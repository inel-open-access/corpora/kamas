<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID63D60F8A-3EB1-A930-ADC7-F1F266BAACBF">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_1964_SU0209.wav" />
         <referenced-file url="PKZ_1964_SU0209.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_1964_SU0209\PKZ_1964_SU0209.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1284</ud-information>
            <ud-information attribute-name="# HIAT:w">851</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">1</ud-information>
            <ud-information attribute-name="# e">839</ud-information>
            <ud-information attribute-name="# HIAT:u">198</ud-information>
            <ud-information attribute-name="# sc">24</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KA">
            <abbreviation>KA</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.092" type="appl" />
         <tli id="T1" time="0.818" type="appl" />
         <tli id="T2" time="1.543" type="appl" />
         <tli id="T3" time="2.268" type="appl" />
         <tli id="T871" time="2.89" type="appl" />
         <tli id="T872" time="4.094" type="appl" />
         <tli id="T4" time="4.546652324095834" />
         <tli id="T5" time="4.904" type="appl" />
         <tli id="T6" time="5.432" type="appl" />
         <tli id="T7" time="5.96" type="appl" />
         <tli id="T8" time="6.489" type="appl" />
         <tli id="T9" time="7.017" type="appl" />
         <tli id="T873" time="7.706" type="appl" />
         <tli id="T874" time="8.369" type="appl" />
         <tli id="T10" time="8.466639958360277" />
         <tli id="T11" time="9.235" type="appl" />
         <tli id="T12" time="9.97" type="appl" />
         <tli id="T13" time="10.704" type="appl" />
         <tli id="T14" time="11.439" type="appl" />
         <tli id="T15" time="12.676" type="appl" />
         <tli id="T16" time="13.517" type="appl" />
         <tli id="T869" time="13.9375" type="intp" />
         <tli id="T17" time="14.358" type="appl" />
         <tli id="T875" time="15.654" type="appl" />
         <tli id="T18" time="16.226615479251116" />
         <tli id="T876" time="16.377" type="appl" />
         <tli id="T19" time="17.158" type="appl" />
         <tli id="T20" time="18.082" type="appl" />
         <tli id="T21" time="18.84" type="appl" />
         <tli id="T22" time="19.526" type="appl" />
         <tli id="T870" time="19.869" type="intp" />
         <tli id="T23" time="20.212" type="appl" />
         <tli id="T877" time="20.952" type="appl" />
         <tli id="T878" time="22.157" type="appl" />
         <tli id="T24" time="22.26659642592388" />
         <tli id="T25" time="23.055" type="appl" />
         <tli id="T26" time="23.874" type="appl" />
         <tli id="T879" time="25.589" type="appl" />
         <tli id="T27" time="32.75323001214492" />
         <tli id="T880" time="34.259" type="appl" />
         <tli id="T28" time="34.45989129508998" />
         <tli id="T29" time="35.415" type="appl" />
         <tli id="T30" time="36.286" type="appl" />
         <tli id="T31" time="37.156" type="appl" />
         <tli id="T881" time="38.172" type="appl" />
         <tli id="T882" time="39.076" type="appl" />
         <tli id="T32" time="39.1398765319159" />
         <tli id="T33" time="39.786" type="appl" />
         <tli id="T34" time="40.526" type="appl" />
         <tli id="T35" time="41.265" type="appl" />
         <tli id="T36" time="42.005" type="appl" />
         <tli id="T37" time="42.744" type="appl" />
         <tli id="T38" time="43.484" type="appl" />
         <tli id="T39" time="44.224" type="appl" />
         <tli id="T883" time="45.157" type="appl" />
         <tli id="T40" time="46.866518824624215" />
         <tli id="T884" time="47.083" type="appl" />
         <tli id="T41" time="48.111" type="appl" />
         <tli id="T42" time="49.044" type="appl" />
         <tli id="T885" time="50.275" type="appl" />
         <tli id="T43" time="50.3665077837889" />
         <tli id="T886" time="50.756" type="appl" />
         <tli id="T44" time="51.543" type="appl" />
         <tli id="T45" time="52.441" type="appl" />
         <tli id="T46" time="53.339" type="appl" />
         <tli id="T47" time="54.171" type="appl" />
         <tli id="T48" time="54.855" type="appl" />
         <tli id="T49" time="55.908" type="appl" />
         <tli id="T50" time="56.642" type="appl" />
         <tli id="T51" time="57.376" type="appl" />
         <tli id="T52" time="58.11" type="appl" />
         <tli id="T53" time="58.844" type="appl" />
         <tli id="T54" time="60.09981041308496" />
         <tli id="T55" time="61.606" type="appl" />
         <tli id="T887" time="63.4" type="appl" />
         <tli id="T56" time="64.63312944590778" />
         <tli id="T888" time="65.146" type="appl" />
         <tli id="T57" time="66.074" type="appl" />
         <tli id="T58" time="67.025" type="appl" />
         <tli id="T59" time="67.976" type="appl" />
         <tli id="T60" time="69.32644797400671" />
         <tli id="T61" time="70.402" type="appl" />
         <tli id="T62" time="71.192" type="appl" />
         <tli id="T63" time="71.982" type="appl" />
         <tli id="T64" time="72.771" type="appl" />
         <tli id="T65" time="73.56" type="appl" />
         <tli id="T66" time="74.59309802684501" />
         <tli id="T67" time="75.161" type="appl" />
         <tli id="T68" time="75.973" type="appl" />
         <tli id="T889" time="76.707" type="appl" />
         <tli id="T890" time="77.369" type="appl" />
         <tli id="T69" time="77.40642248544975" />
         <tli id="T70" time="78.532" type="appl" />
         <tli id="T71" time="79.533" type="appl" />
         <tli id="T891" time="81.102" type="appl" />
         <tli id="T72" time="82.18640740682324" />
         <tli id="T892" time="82.246" type="appl" />
         <tli id="T73" time="82.604" type="appl" />
         <tli id="T74" time="83.225" type="appl" />
         <tli id="T75" time="83.847" type="appl" />
         <tli id="T76" time="84.469" type="appl" />
         <tli id="T77" time="85.09" type="appl" />
         <tli id="T78" time="85.712" type="appl" />
         <tli id="T893" time="85.919" type="appl" />
         <tli id="T894" time="86.34" type="appl" />
         <tli id="T79" time="86.863" type="appl" />
         <tli id="T80" time="87.509" type="appl" />
         <tli id="T81" time="88.156" type="appl" />
         <tli id="T82" time="88.802" type="appl" />
         <tli id="T83" time="89.449" type="appl" />
         <tli id="T84" time="90.095" type="appl" />
         <tli id="T895" time="91.097" type="appl" />
         <tli id="T896" time="93.505" type="appl" />
         <tli id="T85" time="93.6797044841564" />
         <tli id="T86" time="94.838" type="appl" />
         <tli id="T87" time="95.95969729184083" />
         <tli id="T88" time="96.733" type="appl" />
         <tli id="T89" time="97.499" type="appl" />
         <tli id="T90" time="98.266" type="appl" />
         <tli id="T91" time="99.032" type="appl" />
         <tli id="T897" time="103.259" type="appl" />
         <tli id="T92" time="103.95300541000931" />
         <tli id="T898" time="104.102" type="appl" />
         <tli id="T93" time="104.97" type="appl" />
         <tli id="T94" time="105.795" type="appl" />
         <tli id="T95" time="106.62" type="appl" />
         <tli id="T96" time="107.445" type="appl" />
         <tli id="T97" time="108.263" type="appl" />
         <tli id="T98" time="109.081" type="appl" />
         <tli id="T99" time="110.03965287613761" />
         <tli id="T100" time="111.141" type="appl" />
         <tli id="T899" time="112.411" type="appl" />
         <tli id="T101" time="112.89297720856138" />
         <tli id="T900" time="112.953" type="appl" />
         <tli id="T102" time="114.24" type="appl" />
         <tli id="T103" time="115.272" type="appl" />
         <tli id="T104" time="116.13" type="appl" />
         <tli id="T105" time="116.963" type="appl" />
         <tli id="T106" time="117.796" type="appl" />
         <tli id="T107" time="118.629" type="appl" />
         <tli id="T108" time="119.462" type="appl" />
         <tli id="T901" time="120.539" type="appl" />
         <tli id="T109" time="121.1196179240075" />
         <tli id="T902" time="121.141" type="appl" />
         <tli id="T110" time="121.621" type="appl" />
         <tli id="T111" time="122.114" type="appl" />
         <tli id="T112" time="122.607" type="appl" />
         <tli id="T113" time="123.101" type="appl" />
         <tli id="T114" time="123.594" type="appl" />
         <tli id="T115" time="124.087" type="appl" />
         <tli id="T903" time="124.754" type="appl" />
         <tli id="T904" time="125.717" type="appl" />
         <tli id="T116" time="126.0662689862936" />
         <tli id="T117" time="126.758" type="appl" />
         <tli id="T118" time="127.377" type="appl" />
         <tli id="T119" time="127.997" type="appl" />
         <tli id="T120" time="128.616" type="appl" />
         <tli id="T121" time="129.172" type="appl" />
         <tli id="T122" time="129.729" type="appl" />
         <tli id="T123" time="130.285" type="appl" />
         <tli id="T124" time="130.842" type="appl" />
         <tli id="T905" time="131.437" type="appl" />
         <tli id="T906" time="132.1" type="appl" />
         <tli id="T125" time="132.25291613696945" />
         <tli id="T126" time="132.59" type="appl" />
         <tli id="T127" time="133.109" type="appl" />
         <tli id="T128" time="133.629" type="appl" />
         <tli id="T129" time="134.148" type="appl" />
         <tli id="T130" time="134.8995744546616" />
         <tli id="T131" time="135.734" type="appl" />
         <tli id="T132" time="136.635" type="appl" />
         <tli id="T133" time="137.536" type="appl" />
         <tli id="T907" time="138.542" type="appl" />
         <tli id="T134" time="138.66622923928645" />
         <tli id="T908" time="139.144" type="appl" />
         <tli id="T135" time="139.797" type="appl" />
         <tli id="T136" time="140.42" type="appl" />
         <tli id="T137" time="141.042" type="appl" />
         <tli id="T138" time="141.699" type="appl" />
         <tli id="T139" time="142.356" type="appl" />
         <tli id="T909" time="143.299" type="appl" />
         <tli id="T140" time="143.4128809324774" />
         <tli id="T141" time="143.895" type="appl" />
         <tli id="T910" time="143.901" type="appl" />
         <tli id="T142" time="144.529" type="appl" />
         <tli id="T143" time="145.162" type="appl" />
         <tli id="T144" time="145.796" type="appl" />
         <tli id="T145" time="146.43" type="appl" />
         <tli id="T911" time="147.092" type="appl" />
         <tli id="T146" time="147.24620217346728" />
         <tli id="T912" time="147.513" type="appl" />
         <tli id="T147" time="147.828" type="appl" />
         <tli id="T148" time="148.396" type="appl" />
         <tli id="T149" time="148.963" type="appl" />
         <tli id="T150" time="149.53" type="appl" />
         <tli id="T151" time="150.097" type="appl" />
         <tli id="T152" time="150.664" type="appl" />
         <tli id="T153" time="151.232" type="appl" />
         <tli id="T913" time="152.029" type="appl" />
         <tli id="T154" time="152.60618526521662" />
         <tli id="T914" time="152.872" type="appl" />
         <tli id="T155" time="153.369" type="appl" />
         <tli id="T156" time="154.0" type="appl" />
         <tli id="T157" time="154.631" type="appl" />
         <tli id="T158" time="155.262" type="appl" />
         <tli id="T159" time="155.893" type="appl" />
         <tli id="T160" time="156.524" type="appl" />
         <tli id="T915" time="157.207" type="appl" />
         <tli id="T161" time="158.07950133278655" />
         <tli id="T916" time="158.17" type="appl" />
         <tli id="T162" time="159.297" type="appl" />
         <tli id="T163" time="160.268" type="appl" />
         <tli id="T917" time="162.144" type="appl" />
         <tli id="T918" time="162.927" type="appl" />
         <tli id="T164" time="163.09281885143767" />
         <tli id="T165" time="163.921" type="appl" />
         <tli id="T166" time="164.616" type="appl" />
         <tli id="T167" time="165.31" type="appl" />
         <tli id="T168" time="166.005" type="appl" />
         <tli id="T169" time="166.7" type="appl" />
         <tli id="T170" time="168.61946808409962" />
         <tli id="T171" time="169.524" type="appl" />
         <tli id="T172" time="170.053" type="appl" />
         <tli id="T173" time="170.582" type="appl" />
         <tli id="T174" time="171.11" type="appl" />
         <tli id="T175" time="171.639" type="appl" />
         <tli id="T176" time="172.168" type="appl" />
         <tli id="T177" time="172.697" type="appl" />
         <tli id="T178" time="173.567" type="appl" />
         <tli id="T179" time="174.436" type="appl" />
         <tli id="T180" time="175.306" type="appl" />
         <tli id="T181" time="176.176" type="appl" />
         <tli id="T182" time="176.741" type="appl" />
         <tli id="T183" time="177.307" type="appl" />
         <tli id="T184" time="177.872" type="appl" />
         <tli id="T185" time="178.438" type="appl" />
         <tli id="T186" time="179.003" type="appl" />
         <tli id="T187" time="179.568" type="appl" />
         <tli id="T188" time="180.134" type="appl" />
         <tli id="T189" time="180.699" type="appl" />
         <tli id="T190" time="181.265" type="appl" />
         <tli id="T191" time="181.80333274514598" />
         <tli id="T192" time="182.72" type="appl" />
         <tli id="T193" time="183.61" type="appl" />
         <tli id="T194" time="184.5" type="appl" />
         <tli id="T919" time="185.566" type="appl" />
         <tli id="T195" time="186.4260785792022" />
         <tli id="T920" time="186.589" type="appl" />
         <tli id="T196" time="187.377" type="appl" />
         <tli id="T197" time="188.223" type="appl" />
         <tli id="T198" time="189.069" type="appl" />
         <tli id="T199" time="189.915" type="appl" />
         <tli id="T200" time="191.13939704421065" />
         <tli id="T201" time="191.591" type="appl" />
         <tli id="T202" time="192.067" type="appl" />
         <tli id="T203" time="196.29938076686486" />
         <tli id="T204" time="197.33271084052302" />
         <tli id="T205" time="197.896" type="appl" />
         <tli id="T206" time="198.325" type="appl" />
         <tli id="T207" time="200.81269986277817" />
         <tli id="T208" time="201.664" type="appl" />
         <tli id="T921" time="202.244" type="appl" />
         <tli id="T209" time="203.09269267046258" />
         <tli id="T922" time="203.267" type="appl" />
         <tli id="T210" time="203.948" type="appl" />
         <tli id="T211" time="204.606" type="appl" />
         <tli id="T212" time="205.263" type="appl" />
         <tli id="T923" time="205.796" type="appl" />
         <tli id="T924" time="206.639" type="appl" />
         <tli id="T213" time="206.99934701345404" />
         <tli id="T214" time="207.821" type="appl" />
         <tli id="T215" time="208.676" type="appl" />
         <tli id="T925" time="210.372" type="appl" />
         <tli id="T926" time="211.275" type="appl" />
         <tli id="T216" time="211.53266604627686" />
         <tli id="T217" time="212.442" type="appl" />
         <tli id="T218" time="213.151" type="appl" />
         <tli id="T219" time="213.86" type="appl" />
         <tli id="T220" time="214.569" type="appl" />
         <tli id="T221" time="215.516" type="appl" />
         <tli id="T222" time="216.424" type="appl" />
         <tli id="T223" time="217.332" type="appl" />
         <tli id="T224" time="218.24" type="appl" />
         <tli id="T927" time="219.404" type="appl" />
         <tli id="T225" time="220.23930524755127" />
         <tli id="T928" time="220.247" type="appl" />
         <tli id="T226" time="220.888" type="appl" />
         <tli id="T227" time="221.518" type="appl" />
         <tli id="T228" time="222.148" type="appl" />
         <tli id="T229" time="222.779" type="appl" />
         <tli id="T230" time="223.409" type="appl" />
         <tli id="T231" time="224.04" type="appl" />
         <tli id="T232" time="224.67" type="appl" />
         <tli id="T233" time="225.301" type="appl" />
         <tli id="T234" time="225.946" type="appl" />
         <tli id="T235" time="226.591" type="appl" />
         <tli id="T236" time="227.236" type="appl" />
         <tli id="T237" time="227.881" type="appl" />
         <tli id="T238" time="228.526" type="appl" />
         <tli id="T239" time="229.171" type="appl" />
         <tli id="T240" time="229.816" type="appl" />
         <tli id="T241" time="230.461" type="appl" />
         <tli id="T929" time="231.325" type="appl" />
         <tli id="T242" time="231.87260188325106" />
         <tli id="T930" time="232.048" type="appl" />
         <tli id="T243" time="232.785" type="appl" />
         <tli id="T244" time="233.48" type="appl" />
         <tli id="T245" time="234.175" type="appl" />
         <tli id="T246" time="234.871" type="appl" />
         <tli id="T247" time="235.566" type="appl" />
         <tli id="T248" time="236.261" type="appl" />
         <tli id="T931" time="237.226" type="appl" />
         <tli id="T249" time="237.35258459645746" />
         <tli id="T932" time="237.888" type="appl" />
         <tli id="T250" time="238.402" type="appl" />
         <tli id="T251" time="239.173" type="appl" />
         <tli id="T252" time="239.945" type="appl" />
         <tli id="T253" time="240.877" type="appl" />
         <tli id="T254" time="241.781" type="appl" />
         <tli id="T255" time="242.684" type="appl" />
         <tli id="T256" time="243.341" type="appl" />
         <tli id="T257" time="243.963" type="appl" />
         <tli id="T933" time="245.113" type="appl" />
         <tli id="T934" time="246.378" type="appl" />
         <tli id="T258" time="246.42588930773965" />
         <tli id="T259" time="247.737" type="appl" />
         <tli id="T260" time="248.787" type="appl" />
         <tli id="T261" time="250.4325433352786" />
         <tli id="T262" time="251.204" type="appl" />
         <tli id="T263" time="251.782" type="appl" />
         <tli id="T264" time="252.361" type="appl" />
         <tli id="T265" time="253.7125329884387" />
         <tli id="T266" time="254.766" type="appl" />
         <tli id="T267" time="255.509" type="appl" />
         <tli id="T268" time="256.251" type="appl" />
         <tli id="T269" time="256.994" type="appl" />
         <tli id="T270" time="257.736" type="appl" />
         <tli id="T271" time="258.479" type="appl" />
         <tli id="T272" time="259.019" type="appl" />
         <tli id="T273" time="259.559" type="appl" />
         <tli id="T274" time="260.099" type="appl" />
         <tli id="T275" time="262.4191721897131" />
         <tli id="T276" time="263.951" type="appl" />
         <tli id="T277" time="264.968" type="appl" />
         <tli id="T278" time="265.472" type="appl" />
         <tli id="T279" time="265.976" type="appl" />
         <tli id="T280" time="266.481" type="appl" />
         <tli id="T281" time="266.986" type="appl" />
         <tli id="T282" time="267.6366557309821" />
         <tli id="T283" time="268.223" type="appl" />
         <tli id="T284" time="268.956" type="appl" />
         <tli id="T285" time="269.69" type="appl" />
         <tli id="T286" time="270.423" type="appl" />
         <tli id="T287" time="271.156" type="appl" />
         <tli id="T288" time="271.958" type="appl" />
         <tli id="T289" time="272.578" type="appl" />
         <tli id="T290" time="273.199" type="appl" />
         <tli id="T291" time="273.82" type="appl" />
         <tli id="T292" time="274.44" type="appl" />
         <tli id="T935" time="276.061" type="appl" />
         <tli id="T293" time="276.08579574454654" />
         <tli id="T936" time="276.543" type="appl" />
         <tli id="T294" time="276.826" type="appl" />
         <tli id="T295" time="277.8124569644012" />
         <tli id="T296" time="278.685" type="appl" />
         <tli id="T297" time="279.366" type="appl" />
         <tli id="T298" time="280.046" type="appl" />
         <tli id="T299" time="281.1257798457438" />
         <tli id="T300" time="281.917" type="appl" />
         <tli id="T301" time="282.68" type="appl" />
         <tli id="T302" time="283.564" type="appl" />
         <tli id="T303" time="284.293" type="appl" />
         <tli id="T304" time="285.023" type="appl" />
         <tli id="T937" time="286.357" type="appl" />
         <tli id="T938" time="286.718" type="appl" />
         <tli id="T305" time="286.8457618018643" />
         <tli id="T866" time="287.27242712260056" />
         <tli id="T306" time="287.6657592151543" />
         <tli id="T307" time="288.036" type="appl" />
         <tli id="T308" time="288.685" type="appl" />
         <tli id="T309" time="289.334" type="appl" />
         <tli id="T310" time="289.983" type="appl" />
         <tli id="T311" time="290.632" type="appl" />
         <tli id="T312" time="291.281" type="appl" />
         <tli id="T313" time="291.93" type="appl" />
         <tli id="T314" time="292.9124093310831" />
         <tli id="T315" time="293.647" type="appl" />
         <tli id="T316" time="294.22" type="appl" />
         <tli id="T317" time="294.792" type="appl" />
         <tli id="T318" time="295.365" type="appl" />
         <tli id="T319" time="295.937" type="appl" />
         <tli id="T320" time="296.51" type="appl" />
         <tli id="T939" time="297.857" type="appl" />
         <tli id="T321" time="298.29905900537847" />
         <tli id="T940" time="298.76" type="appl" />
         <tli id="T322" time="299.177" type="appl" />
         <tli id="T323" time="299.796" type="appl" />
         <tli id="T324" time="300.415" type="appl" />
         <tli id="T325" time="301.034" type="appl" />
         <tli id="T326" time="301.653" type="appl" />
         <tli id="T327" time="302.272" type="appl" />
         <tli id="T328" time="302.891" type="appl" />
         <tli id="T329" time="303.51" type="appl" />
         <tli id="T330" time="304.129" type="appl" />
         <tli id="T331" time="304.748" type="appl" />
         <tli id="T941" time="306.587" type="appl" />
         <tli id="T332" time="306.68569921610066" />
         <tli id="T942" time="307.069" type="appl" />
         <tli id="T333" time="307.916" type="appl" />
         <tli id="T334" time="308.543" type="appl" />
         <tli id="T335" time="309.171" type="appl" />
         <tli id="T336" time="309.799" type="appl" />
         <tli id="T337" time="310.426" type="appl" />
         <tli id="T338" time="311.33235122474406" />
         <tli id="T339" time="311.99" type="appl" />
         <tli id="T340" time="312.674" type="appl" />
         <tli id="T341" time="313.359" type="appl" />
         <tli id="T342" time="314.043" type="appl" />
         <tli id="T343" time="315.7923371555654" />
         <tli id="T865" time="316.6456677970379" />
         <tli id="T344" time="317.0856664090472" />
         <tli id="T345" time="317.53899831232945" />
         <tli id="T346" time="317.7456643270611" />
         <tli id="T347" time="317.756" type="appl" />
         <tli id="T943" time="318.388" type="appl" />
         <tli id="T944" time="319.713" type="appl" />
         <tli id="T348" time="320.4523224554818" />
         <tli id="T349" time="321.57" type="appl" />
         <tli id="T350" time="322.283" type="appl" />
         <tli id="T351" time="322.997" type="appl" />
         <tli id="T352" time="323.711" type="appl" />
         <tli id="T353" time="324.424" type="appl" />
         <tli id="T354" time="325.138" type="appl" />
         <tli id="T355" time="325.852" type="appl" />
         <tli id="T356" time="326.565" type="appl" />
         <tli id="T357" time="327.5256334758127" />
         <tli id="T358" time="328.14" type="appl" />
         <tli id="T359" time="328.744" type="appl" />
         <tli id="T360" time="329.347" type="appl" />
         <tli id="T361" time="330.489" type="appl" />
         <tli id="T362" time="331.625" type="appl" />
         <tli id="T945" time="332.899" type="appl" />
         <tli id="T363" time="333.0122828346556" />
         <tli id="T946" time="333.561" type="appl" />
         <tli id="T364" time="334.491" type="appl" />
         <tli id="T365" time="335.419" type="appl" />
         <tli id="T947" time="336.511" type="appl" />
         <tli id="T366" time="336.85227072128197" />
         <tli id="T948" time="337.114" type="appl" />
         <tli id="T367" time="337.81" type="appl" />
         <tli id="T368" time="338.653" type="appl" />
         <tli id="T369" time="339.496" type="appl" />
         <tli id="T370" time="340.007" type="appl" />
         <tli id="T371" time="340.518" type="appl" />
         <tli id="T372" time="341.03" type="appl" />
         <tli id="T373" time="341.541" type="appl" />
         <tli id="T374" time="342.65225242504056" />
         <tli id="T375" time="343.606" type="appl" />
         <tli id="T376" time="344.433" type="appl" />
         <tli id="T377" time="345.261" type="appl" />
         <tli id="T378" time="346.089" type="appl" />
         <tli id="T379" time="346.916" type="appl" />
         <tli id="T380" time="347.744" type="appl" />
         <tli id="T381" time="348.571" type="appl" />
         <tli id="T382" time="349.399" type="appl" />
         <tli id="T383" time="350.227" type="appl" />
         <tli id="T384" time="351.054" type="appl" />
         <tli id="T385" time="353.13888601126166" />
         <tli id="T386" time="354.18" type="appl" />
         <tli id="T387" time="354.844" type="appl" />
         <tli id="T388" time="355.507" type="appl" />
         <tli id="T389" time="358.8455346761092" />
         <tli id="T390" time="359.657" type="appl" />
         <tli id="T391" time="360.305" type="appl" />
         <tli id="T949" time="360.896" type="appl" />
         <tli id="T392" time="361.4388598287093" />
         <tli id="T950" time="361.559" type="appl" />
         <tli id="T393" time="362.212" type="appl" />
         <tli id="T394" time="362.944" type="appl" />
         <tli id="T395" time="363.676" type="appl" />
         <tli id="T396" time="364.409" type="appl" />
         <tli id="T397" time="365.141" type="appl" />
         <tli id="T398" time="365.873" type="appl" />
         <tli id="T951" time="367.098" type="appl" />
         <tli id="T399" time="367.98550584375636" />
         <tli id="T952" time="368.061" type="appl" />
         <tli id="T400" time="368.64" type="appl" />
         <tli id="T401" time="369.419" type="appl" />
         <tli id="T402" time="370.197" type="appl" />
         <tli id="T403" time="370.975" type="appl" />
         <tli id="T404" time="371.754" type="appl" />
         <tli id="T405" time="372.8054906389489" />
         <tli id="T406" time="373.607" type="appl" />
         <tli id="T407" time="374.261" type="appl" />
         <tli id="T408" time="374.916" type="appl" />
         <tli id="T409" time="375.57" type="appl" />
         <tli id="T410" time="376.225" type="appl" />
         <tli id="T411" time="376.879" type="appl" />
         <tli id="T953" time="378.839" type="appl" />
         <tli id="T954" time="379.862" type="appl" />
         <tli id="T412" time="380.0121345720099" />
         <tli id="T413" time="380.711" type="appl" />
         <tli id="T414" time="381.235" type="appl" />
         <tli id="T415" time="383.3521240358985" />
         <tli id="T416" time="384.16" type="appl" />
         <tli id="T417" time="386.03211558177316" />
         <tli id="T418" time="386.6" type="appl" />
         <tli id="T955" time="388.472" type="appl" />
         <tli id="T956" time="389.496" type="appl" />
         <tli id="T419" time="389.53877118657437" />
         <tli id="T420" time="390.121" type="appl" />
         <tli id="T421" time="390.76" type="appl" />
         <tli id="T422" time="391.399" type="appl" />
         <tli id="T423" time="392.038" type="appl" />
         <tli id="T424" time="393.0187602088295" />
         <tli id="T425" time="393.658" type="appl" />
         <tli id="T426" time="394.263" type="appl" />
         <tli id="T427" time="394.868" type="appl" />
         <tli id="T957" time="395.938" type="appl" />
         <tli id="T958" time="400.514" type="appl" />
         <tli id="T428" time="400.6054029431712" />
         <tli id="T429" time="401.303" type="appl" />
         <tli id="T430" time="402.008" type="appl" />
         <tli id="T431" time="402.712" type="appl" />
         <tli id="T432" time="403.416" type="appl" />
         <tli id="T433" time="404.12" type="appl" />
         <tli id="T434" time="404.825" type="appl" />
         <tli id="T435" time="406.4587178120219" />
         <tli id="T436" time="407.302" type="appl" />
         <tli id="T437" time="407.995" type="appl" />
         <tli id="T438" time="408.688" type="appl" />
         <tli id="T439" time="409.381" type="appl" />
         <tli id="T959" time="410.268" type="appl" />
         <tli id="T960" time="410.931" type="appl" />
         <tli id="T440" time="410.96537026229873" />
         <tli id="T441" time="412.076" type="appl" />
         <tli id="T442" time="413.207" type="appl" />
         <tli id="T443" time="414.339" type="appl" />
         <tli id="T444" time="415.47" type="appl" />
         <tli id="T445" time="416.601" type="appl" />
         <tli id="T446" time="417.44" type="appl" />
         <tli id="T447" time="418.28" type="appl" />
         <tli id="T448" time="419.12" type="appl" />
         <tli id="T449" time="419.959" type="appl" />
         <tli id="T450" time="420.798" type="appl" />
         <tli id="T451" time="421.638" type="appl" />
         <tli id="T452" time="422.478" type="appl" />
         <tli id="T961" time="423.514" type="appl" />
         <tli id="T962" time="424.237" type="appl" />
         <tli id="T453" time="424.2519950158515" />
         <tli id="T454" time="424.905" type="appl" />
         <tli id="T455" time="425.574" type="appl" />
         <tli id="T456" time="426.244" type="appl" />
         <tli id="T457" time="426.913" type="appl" />
         <tli id="T458" time="427.582" type="appl" />
         <tli id="T459" time="428.251" type="appl" />
         <tli id="T460" time="428.921" type="appl" />
         <tli id="T963" time="429.535" type="appl" />
         <tli id="T461" time="430.8319742590811" />
         <tli id="T964" time="430.98" type="appl" />
         <tli id="T462" time="431.689" type="appl" />
         <tli id="T463" time="432.237" type="appl" />
         <tli id="T464" time="432.784" type="appl" />
         <tli id="T465" time="433.332" type="appl" />
         <tli id="T466" time="433.88" type="appl" />
         <tli id="T467" time="434.428" type="appl" />
         <tli id="T468" time="434.976" type="appl" />
         <tli id="T469" time="435.524" type="appl" />
         <tli id="T470" time="436.072" type="appl" />
         <tli id="T471" time="436.619" type="appl" />
         <tli id="T472" time="437.167" type="appl" />
         <tli id="T473" time="438.05861812905164" />
         <tli id="T474" time="438.781" type="appl" />
         <tli id="T475" time="439.325" type="appl" />
         <tli id="T476" time="439.869" type="appl" />
         <tli id="T477" time="440.412" type="appl" />
         <tli id="T478" time="440.956" type="appl" />
         <tli id="T479" time="441.5" type="appl" />
         <tli id="T480" time="442.044" type="appl" />
         <tli id="T481" time="442.588" type="appl" />
         <tli id="T482" time="443.282" type="appl" />
         <tli id="T965" time="444.106" type="appl" />
         <tli id="T966" time="445.792" type="appl" />
         <tli id="T483" time="445.985259790855" />
         <tli id="T484" time="446.816" type="appl" />
         <tli id="T485" time="447.63" type="appl" />
         <tli id="T486" time="448.443" type="appl" />
         <tli id="T487" time="449.257" type="appl" />
         <tli id="T488" time="450.1919131874891" />
         <tli id="T489" time="450.9" type="appl" />
         <tli id="T490" time="451.472" type="appl" />
         <tli id="T491" time="452.043" type="appl" />
         <tli id="T492" time="452.615" type="appl" />
         <tli id="T493" time="453.5052360688318" />
         <tli id="T494" time="453.967" type="appl" />
         <tli id="T495" time="454.254" type="appl" />
         <tli id="T496" time="454.541" type="appl" />
         <tli id="T497" time="454.828" type="appl" />
         <tli id="T498" time="455.115" type="appl" />
         <tli id="T499" time="455.402" type="appl" />
         <tli id="T967" time="455.606" type="appl" />
         <tli id="T500" time="455.7518956483336" />
         <tli id="T968" time="455.937" type="appl" />
         <tli id="T501" time="456.528" type="appl" />
         <tli id="T502" time="457.339" type="appl" />
         <tli id="T969" time="458.346" type="appl" />
         <tli id="T970" time="458.888" type="appl" />
         <tli id="T503" time="458.9318856169461" />
         <tli id="T504" time="459.634" type="appl" />
         <tli id="T505" time="460.331" type="appl" />
         <tli id="T506" time="461.028" type="appl" />
         <tli id="T507" time="461.725" type="appl" />
         <tli id="T508" time="462.421" type="appl" />
         <tli id="T509" time="463.118" type="appl" />
         <tli id="T510" time="463.815" type="appl" />
         <tli id="T511" time="464.512" type="appl" />
         <tli id="T512" time="465.61186454472323" />
         <tli id="T513" time="466.176" type="appl" />
         <tli id="T514" time="466.724" type="appl" />
         <tli id="T515" time="467.271" type="appl" />
         <tli id="T516" time="468.5118553966026" />
         <tli id="T517" time="469.234" type="appl" />
         <tli id="T518" time="469.833" type="appl" />
         <tli id="T971" time="470.899" type="appl" />
         <tli id="T519" time="475.61849964511606" />
         <tli id="T972" time="475.626" type="appl" />
         <tli id="T520" time="476.356" type="appl" />
         <tli id="T521" time="476.885" type="appl" />
         <tli id="T522" time="477.415" type="appl" />
         <tli id="T523" time="477.944" type="appl" />
         <tli id="T973" time="478.877" type="appl" />
         <tli id="T974" time="480.172" type="appl" />
         <tli id="T524" time="480.7518167852242" />
         <tli id="T525" time="481.702" type="appl" />
         <tli id="T526" time="482.556" type="appl" />
         <tli id="T527" time="483.409" type="appl" />
         <tli id="T528" time="484.263" type="appl" />
         <tli id="T529" time="485.203" type="appl" />
         <tli id="T530" time="485.921" type="appl" />
         <tli id="T531" time="486.639" type="appl" />
         <tli id="T532" time="487.356" type="appl" />
         <tli id="T533" time="488.074" type="appl" />
         <tli id="T534" time="488.792" type="appl" />
         <tli id="T535" time="490.39178637560923" />
         <tli id="T536" time="491.311" type="appl" />
         <tli id="T537" time="492.626" type="appl" />
         <tli id="T538" time="493.942" type="appl" />
         <tli id="T539" time="495.258" type="appl" />
         <tli id="T540" time="496.574" type="appl" />
         <tli id="T541" time="497.889" type="appl" />
         <tli id="T542" time="499.205" type="appl" />
         <tli id="T543" time="500.217" type="appl" />
         <tli id="T544" time="503.1650794151512" />
         <tli id="T545" time="504.184" type="appl" />
         <tli id="T546" time="505.1883542826612" />
         <tli id="T547" time="505.913" type="appl" />
         <tli id="T548" time="506.69" type="appl" />
         <tli id="T549" time="507.468" type="appl" />
         <tli id="T550" time="508.245" type="appl" />
         <tli id="T975" time="509.644" type="appl" />
         <tli id="T976" time="510.818" type="appl" />
         <tli id="T551" time="510.85838847967693" />
         <tli id="T552" time="511.484" type="appl" />
         <tli id="T553" time="512.232" type="appl" />
         <tli id="T554" time="512.98" type="appl" />
         <tli id="T555" time="514.9783754830365" />
         <tli id="T867" time="515.7517063768711" />
         <tli id="T556" time="516.0917053043327" />
         <tli id="T557" time="516.8183696787117" />
         <tli id="T558" time="517.3983678490875" />
         <tli id="T559" time="517.7983665872778" />
         <tli id="T560" time="517.953" type="appl" />
         <tli id="T561" time="518.55" type="appl" />
         <tli id="T562" time="519.146" type="appl" />
         <tli id="T563" time="519.743" type="appl" />
         <tli id="T564" time="520.211" type="appl" />
         <tli id="T565" time="520.678" type="appl" />
         <tli id="T977" time="520.904" type="appl" />
         <tli id="T978" time="522.981" type="appl" />
         <tli id="T566" time="523.2583493635747" />
         <tli id="T567" time="523.955" type="appl" />
         <tli id="T568" time="524.806" type="appl" />
         <tli id="T569" time="525.656" type="appl" />
         <tli id="T570" time="526.507" type="appl" />
         <tli id="T979" time="527.165" type="appl" />
         <tli id="T571" time="527.357" type="appl" />
         <tli id="T980" time="527.677" type="appl" />
         <tli id="T572" time="528.512" type="appl" />
         <tli id="T573" time="529.36" type="appl" />
         <tli id="T574" time="530.209" type="appl" />
         <tli id="T575" time="531.9516552735762" />
         <tli id="T576" time="532.696" type="appl" />
         <tli id="T577" time="533.335" type="appl" />
         <tli id="T578" time="533.974" type="appl" />
         <tli id="T579" time="535.8583096165676" />
         <tli id="T580" time="537.008" type="appl" />
         <tli id="T581" time="537.99" type="appl" />
         <tli id="T582" time="540.9916267566758" />
         <tli id="T583" time="541.794" type="appl" />
         <tli id="T584" time="542.411" type="appl" />
         <tli id="T585" time="543.028" type="appl" />
         <tli id="T586" time="543.644" type="appl" />
         <tli id="T587" time="544.26" type="appl" />
         <tli id="T588" time="544.877" type="appl" />
         <tli id="T589" time="545.494" type="appl" />
         <tli id="T981" time="546.794" type="appl" />
         <tli id="T590" time="547.3449400482642" />
         <tli id="T982" time="547.366" type="appl" />
         <tli id="T591" time="548.11" type="appl" />
         <tli id="T592" time="549.002" type="appl" />
         <tli id="T593" time="549.893" type="appl" />
         <tli id="T594" time="550.784" type="appl" />
         <tli id="T595" time="551.675" type="appl" />
         <tli id="T596" time="552.567" type="appl" />
         <tli id="T597" time="553.458" type="appl" />
         <tli id="T598" time="554.288" type="appl" />
         <tli id="T599" time="554.984" type="appl" />
         <tli id="T600" time="555.681" type="appl" />
         <tli id="T601" time="556.6982438762795" />
         <tli id="T602" time="557.8" type="appl" />
         <tli id="T603" time="558.725" type="appl" />
         <tli id="T604" time="559.65" type="appl" />
         <tli id="T605" time="560.575" type="appl" />
         <tli id="T983" time="562.418" type="appl" />
         <tli id="T984" time="563.382" type="appl" />
         <tli id="T606" time="563.4782224886044" />
         <tli id="T607" time="564.273" type="appl" />
         <tli id="T608" time="565.075" type="appl" />
         <tli id="T609" time="566.2715470102995" />
         <tli id="T610" time="567.349" type="appl" />
         <tli id="T611" time="568.003" type="appl" />
         <tli id="T612" time="568.656" type="appl" />
         <tli id="T613" time="569.31" type="appl" />
         <tli id="T614" time="570.2915343291114" />
         <tli id="T615" time="571.822" type="appl" />
         <tli id="T985" time="573.587" type="appl" />
         <tli id="T616" time="574.3515215217426" />
         <tli id="T986" time="574.37" type="appl" />
         <tli id="T617" time="575.206" type="appl" />
         <tli id="T618" time="576.077" type="appl" />
         <tli id="T619" time="577.4715116796265" />
         <tli id="T868" time="577.9697558398132" type="intp" />
         <tli id="T620" time="578.468" type="appl" />
         <tli id="T621" time="578.966" type="appl" />
         <tli id="T622" time="579.463" type="appl" />
         <tli id="T623" time="579.96" type="appl" />
         <tli id="T624" time="580.457" type="appl" />
         <tli id="T625" time="580.954" type="appl" />
         <tli id="T626" time="581.452" type="appl" />
         <tli id="T987" time="582.016" type="appl" />
         <tli id="T988" time="582.859" type="appl" />
         <tli id="T627" time="582.8914945821044" />
         <tli id="T628" time="583.919" type="appl" />
         <tli id="T629" time="584.805" type="appl" />
         <tli id="T630" time="585.692" type="appl" />
         <tli id="T631" time="588.4914769167679" />
         <tli id="T632" time="589.608" type="appl" />
         <tli id="T633" time="590.458" type="appl" />
         <tli id="T634" time="591.309" type="appl" />
         <tli id="T635" time="592.159" type="appl" />
         <tli id="T636" time="593.1647955079573" />
         <tli id="T637" time="593.934" type="appl" />
         <tli id="T638" time="594.671" type="appl" />
         <tli id="T639" time="595.408" type="appl" />
         <tli id="T640" time="596.3581187678427" />
         <tli id="T641" time="597.175" type="appl" />
         <tli id="T642" time="597.994" type="appl" />
         <tli id="T643" time="598.812" type="appl" />
         <tli id="T989" time="601.344" type="appl" />
         <tli id="T990" time="602.518" type="appl" />
         <tli id="T644" time="602.5580992097916" />
         <tli id="T645" time="603.337" type="appl" />
         <tli id="T646" time="604.066" type="appl" />
         <tli id="T647" time="604.796" type="appl" />
         <tli id="T648" time="605.7980889891327" />
         <tli id="T649" time="606.926" type="appl" />
         <tli id="T650" time="607.943" type="appl" />
         <tli id="T651" time="608.918" type="appl" />
         <tli id="T652" time="609.644" type="appl" />
         <tli id="T653" time="610.37" type="appl" />
         <tli id="T654" time="612.6114008296398" />
         <tli id="T655" time="613.644" type="appl" />
         <tli id="T656" time="614.456" type="appl" />
         <tli id="T657" time="615.268" type="appl" />
         <tli id="T658" time="616.08" type="appl" />
         <tli id="T659" time="616.891" type="appl" />
         <tli id="T660" time="617.703" type="appl" />
         <tli id="T661" time="618.515" type="appl" />
         <tli id="T991" time="619.858" type="appl" />
         <tli id="T992" time="620.37" type="appl" />
         <tli id="T662" time="620.5647090739893" />
         <tli id="T663" time="621.142" type="appl" />
         <tli id="T664" time="621.736" type="appl" />
         <tli id="T665" time="622.33" type="appl" />
         <tli id="T666" time="622.925" type="appl" />
         <tli id="T667" time="623.52" type="appl" />
         <tli id="T668" time="625.5380267188215" />
         <tli id="T669" time="626.507" type="appl" />
         <tli id="T670" time="627.329" type="appl" />
         <tli id="T671" time="628.152" type="appl" />
         <tli id="T672" time="628.975" type="appl" />
         <tli id="T673" time="629.797" type="appl" />
         <tli id="T993" time="631.539" type="appl" />
         <tli id="T994" time="632.051" type="appl" />
         <tli id="T674" time="632.5846711566063" />
         <tli id="T675" time="633.66" type="appl" />
         <tli id="T676" time="634.526" type="appl" />
         <tli id="T677" time="635.391" type="appl" />
         <tli id="T678" time="636.256" type="appl" />
         <tli id="T679" time="637.122" type="appl" />
         <tli id="T680" time="637.987" type="appl" />
         <tli id="T681" time="638.853" type="appl" />
         <tli id="T995" time="640.36" type="appl" />
         <tli id="T996" time="641.052" type="appl" />
         <tli id="T682" time="642.3979735335405" />
         <tli id="T683" time="643.365" type="appl" />
         <tli id="T684" time="644.253" type="appl" />
         <tli id="T685" time="645.141" type="appl" />
         <tli id="T686" time="646.029" type="appl" />
         <tli id="T687" time="647.164625163641" />
         <tli id="T688" time="647.647" type="appl" />
         <tli id="T689" time="648.124" type="appl" />
         <tli id="T690" time="648.602" type="appl" />
         <tli id="T691" time="649.08" type="appl" />
         <tli id="T692" time="649.758" type="appl" />
         <tli id="T693" time="650.435" type="appl" />
         <tli id="T694" time="651.113" type="appl" />
         <tli id="T695" time="651.79" type="appl" />
         <tli id="T696" time="652.8046073721234" />
         <tli id="T697" time="653.886" type="appl" />
         <tli id="T698" time="655.051" type="appl" />
         <tli id="T699" time="656.216" type="appl" />
         <tli id="T700" time="658.4379229349695" />
         <tli id="T701" time="659.334" type="appl" />
         <tli id="T702" time="660.041" type="appl" />
         <tli id="T703" time="660.749" type="appl" />
         <tli id="T704" time="661.471" type="appl" />
         <tli id="T705" time="662.193" type="appl" />
         <tli id="T706" time="663.9845721045409" />
         <tli id="T707" time="664.88" type="appl" />
         <tli id="T997" time="667.033" type="appl" />
         <tli id="T998" time="667.514" type="appl" />
         <tli id="T708" time="667.5845607482531" />
         <tli id="T709" time="668.218" type="appl" />
         <tli id="T710" time="669.6045543761139" />
         <tli id="T711" time="670.646" type="appl" />
         <tli id="T999" time="671.97" type="appl" />
         <tli id="T1000" time="672.692" type="appl" />
         <tli id="T712" time="672.844544155455" />
         <tli id="T713" time="673.629" type="appl" />
         <tli id="T714" time="674.383" type="appl" />
         <tli id="T715" time="675.4045360798725" />
         <tli id="T716" time="676.304" type="appl" />
         <tli id="T717" time="677.128" type="appl" />
         <tli id="T718" time="678.1178608539298" />
         <tli id="T719" time="679.477" type="appl" />
         <tli id="T720" time="680.402" type="appl" />
         <tli id="T721" time="681.5911832305484" />
         <tli id="T722" time="682.325" type="appl" />
         <tli id="T1001" time="682.627" type="appl" />
         <tli id="T723" time="688.8378270374285" />
         <tli id="T1002" time="688.889" type="appl" />
         <tli id="T724" time="689.649" type="appl" />
         <tli id="T725" time="690.376" type="appl" />
         <tli id="T726" time="691.357819088027" />
         <tli id="T727" time="692.127" type="appl" />
         <tli id="T728" time="692.813" type="appl" />
         <tli id="T729" time="693.5" type="appl" />
         <tli id="T730" time="694.327" type="appl" />
         <tli id="T731" time="694.973" type="appl" />
         <tli id="T732" time="695.62" type="appl" />
         <tli id="T733" time="696.382" type="appl" />
         <tli id="T734" time="697.144" type="appl" />
         <tli id="T735" time="697.906" type="appl" />
         <tli id="T736" time="698.942" type="appl" />
         <tli id="T737" time="699.85" type="appl" />
         <tli id="T738" time="700.758" type="appl" />
         <tli id="T739" time="701.667" type="appl" />
         <tli id="T740" time="702.89" type="appl" />
         <tli id="T741" time="703.871" type="appl" />
         <tli id="T742" time="704.852" type="appl" />
         <tli id="T743" time="705.834" type="appl" />
         <tli id="T744" time="706.815" type="appl" />
         <tli id="T745" time="707.796" type="appl" />
         <tli id="T746" time="708.777" type="appl" />
         <tli id="T1003" time="710.504" type="appl" />
         <tli id="T1004" time="711.076" type="appl" />
         <tli id="T747" time="711.3577559975395" />
         <tli id="T748" time="711.772" type="appl" />
         <tli id="T749" time="712.273" type="appl" />
         <tli id="T750" time="712.774" type="appl" />
         <tli id="T751" time="713.276" type="appl" />
         <tli id="T752" time="713.778" type="appl" />
         <tli id="T753" time="714.279" type="appl" />
         <tli id="T754" time="714.781" type="appl" />
         <tli id="T755" time="715.6977423069036" />
         <tli id="T756" time="717.03" type="appl" />
         <tli id="T757" time="718.8177324647877" />
         <tli id="T758" time="719.961" type="appl" />
         <tli id="T759" time="720.969" type="appl" />
         <tli id="T760" time="722.5310540843204" />
         <tli id="T761" time="723.552" type="appl" />
         <tli id="T762" time="724.405" type="appl" />
         <tli id="T763" time="724.883" type="appl" />
         <tli id="T764" time="725.361" type="appl" />
         <tli id="T765" time="725.838" type="appl" />
         <tli id="T766" time="726.316" type="appl" />
         <tli id="T767" time="726.794" type="appl" />
         <tli id="T768" time="727.476" type="appl" />
         <tli id="T769" time="728.304369205533" />
         <tli id="T770" time="729.314" type="appl" />
         <tli id="T1005" time="731.517" type="appl" />
         <tli id="T771" time="732.3176898787086" />
         <tli id="T1006" time="732.45" type="appl" />
         <tli id="T772" time="733.388" type="appl" />
         <tli id="T773" time="734.344" type="appl" />
         <tli id="T774" time="735.3" type="appl" />
         <tli id="T775" time="736.7776758095298" />
         <tli id="T776" time="737.59" type="appl" />
         <tli id="T777" time="738.384" type="appl" />
         <tli id="T778" time="739.177" type="appl" />
         <tli id="T779" time="740.0376655257803" />
         <tli id="T780" time="741.031" type="appl" />
         <tli id="T781" time="741.873" type="appl" />
         <tli id="T782" time="742.7559902840649" />
         <tli id="T783" time="743.52" type="appl" />
         <tli id="T784" time="744.325" type="appl" />
         <tli id="T785" time="745.129" type="appl" />
         <tli id="T786" time="745.934" type="appl" />
         <tli id="T787" time="747.7576411728521" />
         <tli id="T788" time="749.427" type="appl" />
         <tli id="T789" time="750.973" type="appl" />
         <tli id="T790" time="752.518" type="appl" />
         <tli id="T791" time="754.064" type="appl" />
         <tli id="T1007" time="756.233" type="appl" />
         <tli id="T792" time="756.7976126559519" />
         <tli id="T1008" time="756.865" type="appl" />
         <tli id="T793" time="757.6" type="appl" />
         <tli id="T794" time="758.464" type="appl" />
         <tli id="T795" time="759.327" type="appl" />
         <tli id="T796" time="760.19" type="appl" />
         <tli id="T797" time="761.106" type="appl" />
         <tli id="T798" time="761.841" type="appl" />
         <tli id="T799" time="762.575" type="appl" />
         <tli id="T800" time="763.31" type="appl" />
         <tli id="T801" time="764.457" type="appl" />
         <tli id="T802" time="765.7842509739595" />
         <tli id="T803" time="766.568" type="appl" />
         <tli id="T804" time="767.21" type="appl" />
         <tli id="T805" time="767.852" type="appl" />
         <tli id="T806" time="768.495" type="appl" />
         <tli id="T807" time="769.137" type="appl" />
         <tli id="T1009" time="769.63" type="appl" />
         <tli id="T1010" time="770.142" type="appl" />
         <tli id="T808" time="770.3175700067823" />
         <tli id="T809" time="770.795" type="appl" />
         <tli id="T810" time="771.343" type="appl" />
         <tli id="T811" time="771.89" type="appl" />
         <tli id="T812" time="772.438" type="appl" />
         <tli id="T813" time="772.986" type="appl" />
         <tli id="T814" time="773.977558461223" />
         <tli id="T815" time="774.748" type="appl" />
         <tli id="T816" time="775.568" type="appl" />
         <tli id="T817" time="776.388" type="appl" />
         <tli id="T818" time="777.208" type="appl" />
         <tli id="T819" time="779.5975407327961" />
         <tli id="T820" time="781.01" type="appl" />
         <tli id="T821" time="784.1908595763474" />
         <tli id="T822" time="785.003" type="appl" />
         <tli id="T823" time="785.788" type="appl" />
         <tli id="T824" time="786.572" type="appl" />
         <tli id="T825" time="787.357" type="appl" />
         <tli id="T1011" time="788.777" type="appl" />
         <tli id="T1012" time="789.138" type="appl" />
         <tli id="T826" time="789.2908434882731" />
         <tli id="T827" time="789.797" type="appl" />
         <tli id="T828" time="790.496" type="appl" />
         <tli id="T829" time="791.196" type="appl" />
         <tli id="T830" time="791.895" type="appl" />
         <tli id="T831" time="792.594" type="appl" />
         <tli id="T832" time="793.32" type="appl" />
         <tli id="T833" time="793.863" type="appl" />
         <tli id="T834" time="794.406" type="appl" />
         <tli id="T1013" time="795.309" type="appl" />
         <tli id="T1014" time="795.851" type="appl" />
         <tli id="T835" time="796.2508215327834" />
         <tli id="T836" time="797.462" type="appl" />
         <tli id="T837" time="798.521" type="appl" />
         <tli id="T838" time="800.7041408179682" />
         <tli id="T839" time="802.7308010914654" />
         <tli id="T840" time="803.904" type="appl" />
         <tli id="T1015" time="809.669" type="appl" />
         <tli id="T1016" time="810.512" type="appl" />
         <tli id="T841" time="810.6441094619959" />
         <tli id="T842" time="811.75" type="appl" />
         <tli id="T843" time="813.0707684736834" />
         <tli id="T844" time="814.532" type="appl" />
         <tli id="T845" time="816.5774240784846" />
         <tli id="T846" time="817.698" type="appl" />
         <tli id="T1017" time="819.574" type="appl" />
         <tli id="T847" time="820.1907460134697" />
         <tli id="T1018" time="820.206" type="appl" />
         <tli id="T848" time="821.246" type="appl" />
         <tli id="T849" time="823.9840673806407" />
         <tli id="T850" time="825.565" type="appl" />
         <tli id="T851" time="826.773" type="appl" />
         <tli id="T852" time="827.981" type="appl" />
         <tli id="T1019" time="830.291" type="appl" />
         <tli id="T1020" time="831.014" type="appl" />
         <tli id="T853" time="831.4040439740697" />
         <tli id="T854" time="832.93" type="appl" />
         <tli id="T855" time="834.14" type="appl" />
         <tli id="T856" time="836.0173627545306" />
         <tli id="T1021" time="838.1790000000001" type="intp" />
         <tli id="T857" time="838.3573553729436" />
         <tli id="T858" time="840.3773490008044" />
         <tli id="T859" time="841.297346098642" />
         <tli id="T860" time="842.139" type="appl" />
         <tli id="T861" time="842.831" type="appl" />
         <tli id="T862" time="843.5773389063263" />
         <tli id="T863" time="844.831" type="appl" />
         <tli id="T864" time="845.344" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-KA"
                      id="tx-KA"
                      speaker="KA"
                      type="t">
         <timeline-fork end="T4" start="T0">
            <tli id="T0.tx-KA.1" />
            <tli id="T0.tx-KA.2" />
            <tli id="T0.tx-KA.3" />
         </timeline-fork>
         <timeline-fork end="T417" start="T415">
            <tli id="T415.tx-KA.1" />
         </timeline-fork>
         <timeline-fork end="T419" start="T417">
            <tli id="T417.tx-KA.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KA">
            <ts e="T4" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T0.tx-KA.1" id="Seg_4" n="HIAT:w" s="T0">Klaudia</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-KA.2" id="Seg_7" n="HIAT:w" s="T0.tx-KA.1">Plotnikova</ts>
                  <nts id="Seg_8" n="HIAT:ip">,</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0.tx-KA.3" id="Seg_11" n="HIAT:w" s="T0.tx-KA.2">kuues</ts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T0.tx-KA.3">lint</ts>
                  <nts id="Seg_15" n="HIAT:ip">.</nts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T204" id="Seg_17" n="sc" s="T203">
               <ts e="T204" id="Seg_19" n="HIAT:u" s="T203">
                  <ts e="T204" id="Seg_21" n="HIAT:w" s="T203">Играли</ts>
                  <nts id="Seg_22" n="HIAT:ip">.</nts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T419" id="Seg_24" n="sc" s="T415">
               <ts e="T417" id="Seg_26" n="HIAT:u" s="T415">
                  <ts e="T415.tx-KA.1" id="Seg_28" n="HIAT:w" s="T415">Целый</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_31" n="HIAT:w" s="T415.tx-KA.1">день</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T419" id="Seg_35" n="HIAT:u" s="T417">
                  <ts e="T417.tx-KA.1" id="Seg_37" n="HIAT:w" s="T417">Целый</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_40" n="HIAT:w" s="T417.tx-KA.1">день</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T857" id="Seg_43" n="sc" s="T856">
               <ts e="T857" id="Seg_45" n="HIAT:u" s="T856">
                  <ts e="T857" id="Seg_47" n="HIAT:w" s="T856">Грудь</ts>
                  <nts id="Seg_48" n="HIAT:ip">.</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T859" id="Seg_50" n="sc" s="T858">
               <ts e="T859" id="Seg_52" n="HIAT:u" s="T858">
                  <ts e="T859" id="Seg_54" n="HIAT:w" s="T858">Кости</ts>
                  <nts id="Seg_55" n="HIAT:ip">.</nts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T862" id="Seg_57" n="sc" s="T861">
               <ts e="T862" id="Seg_59" n="HIAT:u" s="T861">
                  <ts e="T862" id="Seg_61" n="HIAT:w" s="T861">Кровь</ts>
                  <nts id="Seg_62" n="HIAT:ip">.</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KA">
            <ts e="T4" id="Seg_64" n="sc" s="T0">
               <ts e="T4" id="Seg_66" n="e" s="T0">Klaudia Plotnikova, kuues lint. </ts>
            </ts>
            <ts e="T204" id="Seg_67" n="sc" s="T203">
               <ts e="T204" id="Seg_69" n="e" s="T203">Играли. </ts>
            </ts>
            <ts e="T419" id="Seg_70" n="sc" s="T415">
               <ts e="T417" id="Seg_72" n="e" s="T415">Целый день. </ts>
               <ts e="T419" id="Seg_74" n="e" s="T417">Целый день. </ts>
            </ts>
            <ts e="T857" id="Seg_75" n="sc" s="T856">
               <ts e="T857" id="Seg_77" n="e" s="T856">Грудь. </ts>
            </ts>
            <ts e="T859" id="Seg_78" n="sc" s="T858">
               <ts e="T859" id="Seg_80" n="e" s="T858">Кости. </ts>
            </ts>
            <ts e="T862" id="Seg_81" n="sc" s="T861">
               <ts e="T862" id="Seg_83" n="e" s="T861">Кровь. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KA">
            <ta e="T4" id="Seg_84" s="T0">PKZ_1964_SU0209.KA.001 (001)</ta>
            <ta e="T204" id="Seg_85" s="T203">PKZ_1964_SU0209.KA.002 (047)</ta>
            <ta e="T417" id="Seg_86" s="T415">PKZ_1964_SU0209.KA.003 (091)</ta>
            <ta e="T419" id="Seg_87" s="T417">PKZ_1964_SU0209.KA.004 (092)</ta>
            <ta e="T857" id="Seg_88" s="T856">PKZ_1964_SU0209.KA.005 (194)</ta>
            <ta e="T859" id="Seg_89" s="T858">PKZ_1964_SU0209.KA.006 (196)</ta>
            <ta e="T862" id="Seg_90" s="T861">PKZ_1964_SU0209.KA.007 (198)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-KA">
            <ta e="T4" id="Seg_91" s="T0">Klaudia Plotnikova, kuues lint. </ta>
            <ta e="T204" id="Seg_92" s="T203">Играли. </ta>
            <ta e="T417" id="Seg_93" s="T415">Целый день. </ta>
            <ta e="T419" id="Seg_94" s="T417">Целый день. </ta>
            <ta e="T857" id="Seg_95" s="T856">Грудь. </ta>
            <ta e="T859" id="Seg_96" s="T858">Кости. </ta>
            <ta e="T862" id="Seg_97" s="T861">Кровь. </ta>
         </annotation>
         <annotation name="CS" tierref="CS-KA">
            <ta e="T4" id="Seg_98" s="T0">EST:ext</ta>
            <ta e="T204" id="Seg_99" s="T203">RUS:ext</ta>
            <ta e="T857" id="Seg_100" s="T856">RUS:ext</ta>
            <ta e="T859" id="Seg_101" s="T858">RUS:ext</ta>
            <ta e="T862" id="Seg_102" s="T861">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KA">
            <ta e="T4" id="Seg_103" s="T0">Клавдия Плотникова, шестая пленка.</ta>
         </annotation>
         <annotation name="fe" tierref="fe-KA">
            <ta e="T4" id="Seg_104" s="T0">Klavdiya Plotnikova, sixth tape.</ta>
            <ta e="T204" id="Seg_105" s="T203">We played.</ta>
            <ta e="T857" id="Seg_106" s="T856">Breast.</ta>
            <ta e="T859" id="Seg_107" s="T858">Bones.</ta>
            <ta e="T862" id="Seg_108" s="T861">Blood.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KA">
            <ta e="T4" id="Seg_109" s="T0">Klavdija Plotnikova, sechstes Band.</ta>
            <ta e="T204" id="Seg_110" s="T203">Wir spielten.</ta>
            <ta e="T417" id="Seg_111" s="T415">Den ganzen Tag.</ta>
            <ta e="T419" id="Seg_112" s="T417">Den ganzen Tag.</ta>
            <ta e="T857" id="Seg_113" s="T856">Brust.</ta>
            <ta e="T859" id="Seg_114" s="T858">Knochen.</ta>
            <ta e="T862" id="Seg_115" s="T861">Blut.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-KA">
            <ta e="T4" id="Seg_116" s="T0">[GVY:] The transcription on this tape is in the Ekaterinbirg archive, under Box 5, Recording 6.</ta>
         </annotation>
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-PKZ"
                      id="tx-PKZ"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T459" start="T458">
            <tli id="T458.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T500" start="T493">
            <tli id="T493.tx-PKZ.1" />
            <tli id="T493.tx-PKZ.2" />
            <tli id="T493.tx-PKZ.3" />
            <tli id="T493.tx-PKZ.4" />
         </timeline-fork>
         <timeline-fork end="T566" start="T563">
            <tli id="T563.tx-PKZ.1" />
            <tli id="T563.tx-PKZ.2" />
         </timeline-fork>
         <timeline-fork end="T757" start="T756">
            <tli id="T756.tx-PKZ.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-PKZ">
            <ts e="T203" id="Seg_117" n="sc" s="T4">
               <ts e="T10" id="Seg_119" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_121" n="HIAT:w" s="T4">Döbər</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_124" n="HIAT:w" s="T5">šolal</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_127" n="HIAT:w" s="T6">dăk</ts>
                  <nts id="Seg_128" n="HIAT:ip">,</nts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_131" n="HIAT:w" s="T7">măna</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_134" n="HIAT:w" s="T8">sima</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_137" n="HIAT:w" s="T9">iʔ</ts>
                  <nts id="Seg_138" n="HIAT:ip">.</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_141" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_143" n="HIAT:w" s="T10">Tăn</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_146" n="HIAT:w" s="T11">büzʼe</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_149" n="HIAT:w" s="T12">gibər</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_152" n="HIAT:w" s="T13">kambi</ts>
                  <nts id="Seg_153" n="HIAT:ip">?</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_156" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_158" n="HIAT:w" s="T14">Dʼelamdə</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_160" n="HIAT:ip">(</nts>
                  <ts e="T16" id="Seg_162" n="HIAT:w" s="T15">kalla-</ts>
                  <nts id="Seg_163" n="HIAT:ip">)</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T869" id="Seg_166" n="HIAT:w" s="T16">kalla</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_169" n="HIAT:w" s="T869">dʼürbi</ts>
                  <nts id="Seg_170" n="HIAT:ip">,</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_173" n="HIAT:w" s="T17">ine</ts>
                  <nts id="Seg_174" n="HIAT:ip">.</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_177" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_179" n="HIAT:w" s="T18">Unnʼa</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_182" n="HIAT:w" s="T19">kambi</ts>
                  <nts id="Seg_183" n="HIAT:ip">?</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_186" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_188" n="HIAT:w" s="T20">Dʼok</ts>
                  <nts id="Seg_189" n="HIAT:ip">,</nts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_192" n="HIAT:w" s="T21">šidegöʔ</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T870" id="Seg_195" n="HIAT:w" s="T22">kalla</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_198" n="HIAT:w" s="T870">dʼürbiʔi</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_201" n="HIAT:w" s="T23">bar</ts>
                  <nts id="Seg_202" n="HIAT:ip">.</nts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_205" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_207" n="HIAT:w" s="T24">Kamən</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_209" n="HIAT:ip">(</nts>
                  <ts e="T26" id="Seg_211" n="HIAT:w" s="T25">ka-</ts>
                  <nts id="Seg_212" n="HIAT:ip">)</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_215" n="HIAT:w" s="T26">kambiʔi</ts>
                  <nts id="Seg_216" n="HIAT:ip">?</nts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_219" n="HIAT:u" s="T27">
                  <nts id="Seg_220" n="HIAT:ip">(</nts>
                  <ts e="T28" id="Seg_222" n="HIAT:w" s="T27">Погоди</ts>
                  <nts id="Seg_223" n="HIAT:ip">)</nts>
                  <nts id="Seg_224" n="HIAT:ip">.</nts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_227" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_229" n="HIAT:w" s="T28">Taldʼen</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_232" n="HIAT:w" s="T29">kambiʔi</ts>
                  <nts id="Seg_233" n="HIAT:ip">,</nts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_236" n="HIAT:w" s="T30">ugandə</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_239" n="HIAT:w" s="T31">erten</ts>
                  <nts id="Seg_240" n="HIAT:ip">.</nts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_243" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_245" n="HIAT:w" s="T32">Ĭmbi</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_248" n="HIAT:w" s="T33">šaːmnaʔbəl</ts>
                  <nts id="Seg_249" n="HIAT:ip">,</nts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_252" n="HIAT:w" s="T34">ej</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_254" n="HIAT:ip">(</nts>
                  <ts e="T36" id="Seg_256" n="HIAT:w" s="T35">sĭ-</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_259" n="HIAT:w" s="T36">sĭr-</ts>
                  <nts id="Seg_260" n="HIAT:ip">)</nts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_263" n="HIAT:w" s="T37">ej</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_265" n="HIAT:ip">(</nts>
                  <ts e="T39" id="Seg_267" n="HIAT:w" s="T38">sĭr-</ts>
                  <nts id="Seg_268" n="HIAT:ip">)</nts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_271" n="HIAT:w" s="T39">sĭriel</ts>
                  <nts id="Seg_272" n="HIAT:ip">?</nts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_275" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_277" n="HIAT:w" s="T40">Köžetsnəktə</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_279" n="HIAT:ip">(</nts>
                  <ts e="T42" id="Seg_281" n="HIAT:w" s="T41">nuldə-</ts>
                  <nts id="Seg_282" n="HIAT:ip">)</nts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_285" n="HIAT:w" s="T42">nuldʼit</ts>
                  <nts id="Seg_286" n="HIAT:ip">.</nts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_289" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_291" n="HIAT:w" s="T43">Bar</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_293" n="HIAT:ip">(</nts>
                  <ts e="T45" id="Seg_295" n="HIAT:w" s="T44">kujnekəm</ts>
                  <nts id="Seg_296" n="HIAT:ip">)</nts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_299" n="HIAT:w" s="T45">nĭŋgəluʔpi</ts>
                  <nts id="Seg_300" n="HIAT:ip">.</nts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_303" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_305" n="HIAT:w" s="T46">Ši</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_308" n="HIAT:w" s="T47">molaːmbi</ts>
                  <nts id="Seg_309" n="HIAT:ip">.</nts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_312" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_314" n="HIAT:w" s="T48">Măn</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_317" n="HIAT:w" s="T49">dĭgəttə</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_320" n="HIAT:w" s="T50">dĭm</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_322" n="HIAT:ip">(</nts>
                  <ts e="T52" id="Seg_324" n="HIAT:w" s="T51">sö-</ts>
                  <nts id="Seg_325" n="HIAT:ip">)</nts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_328" n="HIAT:w" s="T52">šödörbiam</ts>
                  <nts id="Seg_329" n="HIAT:ip">,</nts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_332" n="HIAT:w" s="T53">nʼitkaʔizʼiʔ</ts>
                  <nts id="Seg_333" n="HIAT:ip">.</nts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_336" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_338" n="HIAT:w" s="T54">Nʼimizʼiʔ</ts>
                  <nts id="Seg_339" n="HIAT:ip">,</nts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_342" n="HIAT:w" s="T55">ĭndak</ts>
                  <nts id="Seg_343" n="HIAT:ip">.</nts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_346" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_348" n="HIAT:w" s="T56">Tʼegermaʔgən</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_351" n="HIAT:w" s="T57">bar</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_354" n="HIAT:w" s="T58">küzürleʔbə</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_357" n="HIAT:w" s="T59">kunguro</ts>
                  <nts id="Seg_358" n="HIAT:ip">.</nts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_361" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_363" n="HIAT:w" s="T60">Nada</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_366" n="HIAT:w" s="T61">kanzittə</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_369" n="HIAT:w" s="T62">kudajdə</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_371" n="HIAT:ip">(</nts>
                  <ts e="T64" id="Seg_373" n="HIAT:w" s="T63">üzəš-</ts>
                  <nts id="Seg_374" n="HIAT:ip">)</nts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_377" n="HIAT:w" s="T64">üzəsʼtə</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_380" n="HIAT:w" s="T65">dĭgəttə</ts>
                  <nts id="Seg_381" n="HIAT:ip">.</nts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_384" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_386" n="HIAT:w" s="T66">Kăštəliaʔi</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_389" n="HIAT:w" s="T67">dĭzeŋ</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_392" n="HIAT:w" s="T68">bar</ts>
                  <nts id="Seg_393" n="HIAT:ip">.</nts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_396" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_398" n="HIAT:w" s="T69">Edət</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_401" n="HIAT:w" s="T70">inenə</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_404" n="HIAT:w" s="T71">koŋgoro</ts>
                  <nts id="Seg_405" n="HIAT:ip">.</nts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_408" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_410" n="HIAT:w" s="T72">Sagəšdə</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_413" n="HIAT:w" s="T73">naga</ts>
                  <nts id="Seg_414" n="HIAT:ip">,</nts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_417" n="HIAT:w" s="T74">bar</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_420" n="HIAT:w" s="T75">ĭmbidə</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_423" n="HIAT:w" s="T76">ej</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_426" n="HIAT:w" s="T77">tĭmnem</ts>
                  <nts id="Seg_427" n="HIAT:ip">.</nts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_430" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_432" n="HIAT:w" s="T78">Ĭmbidə</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_435" n="HIAT:w" s="T79">ej</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_438" n="HIAT:w" s="T80">moliam</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_441" n="HIAT:w" s="T81">nörbəsʼtə</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_444" n="HIAT:w" s="T82">bar</ts>
                  <nts id="Seg_445" n="HIAT:ip">,</nts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_448" n="HIAT:w" s="T83">sagəšdə</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_451" n="HIAT:w" s="T84">naga</ts>
                  <nts id="Seg_452" n="HIAT:ip">.</nts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_455" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_457" n="HIAT:w" s="T85">Dʼijenə</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_460" n="HIAT:w" s="T86">kambiam</ts>
                  <nts id="Seg_461" n="HIAT:ip">.</nts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_464" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_466" n="HIAT:w" s="T87">Bar</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_468" n="HIAT:ip">(</nts>
                  <ts e="T89" id="Seg_470" n="HIAT:w" s="T88">dʼü-</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_473" n="HIAT:w" s="T89">dʼür-</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_476" n="HIAT:w" s="T90">dʼürdə-</ts>
                  <nts id="Seg_477" n="HIAT:ip">)</nts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_480" n="HIAT:w" s="T91">tʼürleʔbə</ts>
                  <nts id="Seg_481" n="HIAT:ip">.</nts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_484" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_486" n="HIAT:w" s="T92">Dʼijenə</ts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_489" n="HIAT:w" s="T93">kambiam</ts>
                  <nts id="Seg_490" n="HIAT:ip">,</nts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_493" n="HIAT:w" s="T94">bar</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_496" n="HIAT:w" s="T95">dʼürleʔpiem</ts>
                  <nts id="Seg_497" n="HIAT:ip">.</nts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T99" id="Seg_500" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_502" n="HIAT:w" s="T96">Dĭgəttə</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_505" n="HIAT:w" s="T97">kirgarbiam</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_508" n="HIAT:w" s="T98">bar</ts>
                  <nts id="Seg_509" n="HIAT:ip">.</nts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_512" n="HIAT:u" s="T99">
                  <ts e="T100" id="Seg_514" n="HIAT:w" s="T99">Dĭgəttə</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_517" n="HIAT:w" s="T100">šobiam</ts>
                  <nts id="Seg_518" n="HIAT:ip">.</nts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_521" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_523" n="HIAT:w" s="T101">Kondʼo</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_526" n="HIAT:w" s="T102">amnobi</ts>
                  <nts id="Seg_527" n="HIAT:ip">.</nts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_530" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_532" n="HIAT:w" s="T103">Ĭmbidə</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_535" n="HIAT:w" s="T104">bar</ts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_538" n="HIAT:w" s="T106">ĭmbidə</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_541" n="HIAT:w" s="T107">ej</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_544" n="HIAT:w" s="T108">dʼăbaktərbi</ts>
                  <nts id="Seg_545" n="HIAT:ip">.</nts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_548" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_550" n="HIAT:w" s="T109">Dĭ</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_553" n="HIAT:w" s="T110">kandəga</ts>
                  <nts id="Seg_554" n="HIAT:ip">,</nts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_557" n="HIAT:w" s="T111">a</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_560" n="HIAT:w" s="T112">măn</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_563" n="HIAT:w" s="T113">dĭm</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_565" n="HIAT:ip">(</nts>
                  <ts e="T115" id="Seg_567" n="HIAT:w" s="T114">pă-</ts>
                  <nts id="Seg_568" n="HIAT:ip">)</nts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_571" n="HIAT:w" s="T115">bĭdliem</ts>
                  <nts id="Seg_572" n="HIAT:ip">.</nts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_575" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_577" n="HIAT:w" s="T116">Iʔ</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_580" n="HIAT:w" s="T117">kanaʔ</ts>
                  <nts id="Seg_581" n="HIAT:ip">,</nts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_584" n="HIAT:w" s="T118">paraʔ</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_587" n="HIAT:w" s="T119">döbər</ts>
                  <nts id="Seg_588" n="HIAT:ip">!</nts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_591" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_593" n="HIAT:w" s="T120">Măn</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_596" n="HIAT:w" s="T121">tănan</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_599" n="HIAT:w" s="T122">mănliam:</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_602" n="HIAT:w" s="T123">paraʔ</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_605" n="HIAT:w" s="T124">döbər</ts>
                  <nts id="Seg_606" n="HIAT:ip">!</nts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_609" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_611" n="HIAT:w" s="T125">Măn</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_614" n="HIAT:w" s="T126">ugandə</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_617" n="HIAT:w" s="T127">urgo</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_620" n="HIAT:w" s="T128">il</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_623" n="HIAT:w" s="T129">ige</ts>
                  <nts id="Seg_624" n="HIAT:ip">.</nts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_627" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_629" n="HIAT:w" s="T130">Ipek</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_632" n="HIAT:w" s="T131">iʔgö</ts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_634" n="HIAT:ip">(</nts>
                  <ts e="T133" id="Seg_636" n="HIAT:w" s="T132">kereʔnə-</ts>
                  <nts id="Seg_637" n="HIAT:ip">)</nts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_640" n="HIAT:w" s="T133">kereʔ</ts>
                  <nts id="Seg_641" n="HIAT:ip">.</nts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T137" id="Seg_644" n="HIAT:u" s="T134">
                  <ts e="T135" id="Seg_646" n="HIAT:w" s="T134">Iʔ</ts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_649" n="HIAT:w" s="T135">bögəldə</ts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_652" n="HIAT:w" s="T136">dĭm</ts>
                  <nts id="Seg_653" n="HIAT:ip">!</nts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_656" n="HIAT:u" s="T137">
                  <ts e="T138" id="Seg_658" n="HIAT:w" s="T137">Udal</ts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_661" n="HIAT:w" s="T138">ej</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_664" n="HIAT:w" s="T139">endə</ts>
                  <nts id="Seg_665" n="HIAT:ip">!</nts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T146" id="Seg_668" n="HIAT:u" s="T140">
                  <nts id="Seg_669" n="HIAT:ip">(</nts>
                  <ts e="T142" id="Seg_671" n="HIAT:w" s="T140">Ižəmdə</ts>
                  <nts id="Seg_672" n="HIAT:ip">)</nts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_675" n="HIAT:w" s="T142">dĭm</ts>
                  <nts id="Seg_676" n="HIAT:ip">,</nts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_678" n="HIAT:ip">(</nts>
                  <ts e="T144" id="Seg_680" n="HIAT:w" s="T143">ižəmdə</ts>
                  <nts id="Seg_681" n="HIAT:ip">)</nts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_684" n="HIAT:w" s="T144">dĭm</ts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_687" n="HIAT:w" s="T145">udazi</ts>
                  <nts id="Seg_688" n="HIAT:ip">.</nts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T154" id="Seg_691" n="HIAT:u" s="T146">
                  <ts e="T148" id="Seg_693" n="HIAT:w" s="T146">Boskənə</ts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_696" n="HIAT:w" s="T148">iʔgö</ts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_699" n="HIAT:w" s="T149">ibiem</ts>
                  <nts id="Seg_700" n="HIAT:ip">,</nts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_703" n="HIAT:w" s="T150">a</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_706" n="HIAT:w" s="T151">dĭʔnə</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_709" n="HIAT:w" s="T152">amgam</ts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_712" n="HIAT:w" s="T153">ibiem</ts>
                  <nts id="Seg_713" n="HIAT:ip">.</nts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_716" n="HIAT:u" s="T154">
                  <ts e="T155" id="Seg_718" n="HIAT:w" s="T154">Boskəndə</ts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_721" n="HIAT:w" s="T155">iʔgö</ts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_724" n="HIAT:w" s="T156">ilie</ts>
                  <nts id="Seg_725" n="HIAT:ip">,</nts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_728" n="HIAT:w" s="T157">a</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_731" n="HIAT:w" s="T158">dĭʔnə</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_734" n="HIAT:w" s="T159">amgam</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_737" n="HIAT:w" s="T160">ilie</ts>
                  <nts id="Seg_738" n="HIAT:ip">.</nts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T164" id="Seg_741" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_743" n="HIAT:w" s="T161">Muktuʔ</ts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_746" n="HIAT:w" s="T162">bar</ts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_749" n="HIAT:w" s="T163">dʼăbaktərbibaʔ</ts>
                  <nts id="Seg_750" n="HIAT:ip">.</nts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T170" id="Seg_753" n="HIAT:u" s="T164">
                  <ts e="T165" id="Seg_755" n="HIAT:w" s="T164">Miʔ</ts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_758" n="HIAT:w" s="T165">bar</ts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_761" n="HIAT:w" s="T166">üdʼüge</ts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_764" n="HIAT:w" s="T167">ibibeʔ</ts>
                  <nts id="Seg_765" n="HIAT:ip">,</nts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_768" n="HIAT:w" s="T168">bar</ts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_771" n="HIAT:w" s="T169">sʼarlaʔpibaʔ</ts>
                  <nts id="Seg_772" n="HIAT:ip">.</nts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T177" id="Seg_775" n="HIAT:u" s="T170">
                  <ts e="T171" id="Seg_777" n="HIAT:w" s="T170">Paʔi</ts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_780" n="HIAT:w" s="T171">iləj</ts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_783" n="HIAT:w" s="T172">da</ts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_785" n="HIAT:ip">(</nts>
                  <ts e="T174" id="Seg_787" n="HIAT:w" s="T173">bar</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_790" n="HIAT:w" s="T174">oj</ts>
                  <nts id="Seg_791" n="HIAT:ip">)</nts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_794" n="HIAT:w" s="T175">onʼiʔ</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_797" n="HIAT:w" s="T176">nuʔmələj</ts>
                  <nts id="Seg_798" n="HIAT:ip">.</nts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_801" n="HIAT:u" s="T177">
                  <ts e="T178" id="Seg_803" n="HIAT:w" s="T177">A</ts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_806" n="HIAT:w" s="T178">miʔ</ts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_809" n="HIAT:w" s="T179">bar</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_812" n="HIAT:w" s="T180">šalaːmbibeʔ</ts>
                  <nts id="Seg_813" n="HIAT:ip">.</nts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T191" id="Seg_816" n="HIAT:u" s="T181">
                  <ts e="T182" id="Seg_818" n="HIAT:w" s="T181">Dĭgəttə</ts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_821" n="HIAT:w" s="T182">dĭ</ts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_824" n="HIAT:w" s="T183">bar</ts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_827" n="HIAT:w" s="T184">kulia</ts>
                  <nts id="Seg_828" n="HIAT:ip">,</nts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_831" n="HIAT:w" s="T185">kulia</ts>
                  <nts id="Seg_832" n="HIAT:ip">,</nts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_834" n="HIAT:ip">(</nts>
                  <ts e="T187" id="Seg_836" n="HIAT:w" s="T186">ej</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_839" n="HIAT:w" s="T187">mozi-</ts>
                  <nts id="Seg_840" n="HIAT:ip">)</nts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_843" n="HIAT:w" s="T188">ej</ts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_846" n="HIAT:w" s="T189">molia</ts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_849" n="HIAT:w" s="T190">kuzittə</ts>
                  <nts id="Seg_850" n="HIAT:ip">.</nts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T195" id="Seg_853" n="HIAT:u" s="T191">
                  <ts e="T192" id="Seg_855" n="HIAT:w" s="T191">Miʔ</ts>
                  <nts id="Seg_856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_858" n="HIAT:w" s="T192">bar</ts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_861" n="HIAT:w" s="T193">bazoʔ</ts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_864" n="HIAT:w" s="T194">sarlaʔbəbaʔ</ts>
                  <nts id="Seg_865" n="HIAT:ip">.</nts>
                  <nts id="Seg_866" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T200" id="Seg_868" n="HIAT:u" s="T195">
                  <nts id="Seg_869" n="HIAT:ip">(</nts>
                  <ts e="T196" id="Seg_871" n="HIAT:w" s="T195">M-</ts>
                  <nts id="Seg_872" n="HIAT:ip">)</nts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_875" n="HIAT:w" s="T196">Miʔ</ts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_878" n="HIAT:w" s="T197">bar</ts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_881" n="HIAT:w" s="T198">ej</ts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_884" n="HIAT:w" s="T199">kudonzluʔpibaʔ</ts>
                  <nts id="Seg_885" n="HIAT:ip">.</nts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T203" id="Seg_888" n="HIAT:u" s="T200">
                  <ts e="T201" id="Seg_890" n="HIAT:w" s="T200">Jakšə</ts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_893" n="HIAT:w" s="T201">bar</ts>
                  <nts id="Seg_894" n="HIAT:ip">…</nts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T415" id="Seg_896" n="sc" s="T204">
               <ts e="T207" id="Seg_898" n="HIAT:u" s="T204">
                  <ts e="T205" id="Seg_900" n="HIAT:w" s="T204">Jakšə</ts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_903" n="HIAT:w" s="T205">bar</ts>
                  <nts id="Seg_904" n="HIAT:ip">…</nts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T209" id="Seg_907" n="HIAT:u" s="T207">
                  <nts id="Seg_908" n="HIAT:ip">(</nts>
                  <ts e="T208" id="Seg_910" n="HIAT:w" s="T207">Пока</ts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_913" n="HIAT:w" s="T208">закрой</ts>
                  <nts id="Seg_914" n="HIAT:ip">)</nts>
                  <nts id="Seg_915" n="HIAT:ip">.</nts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T213" id="Seg_918" n="HIAT:u" s="T209">
                  <ts e="T210" id="Seg_920" n="HIAT:w" s="T209">Miʔ</ts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_923" n="HIAT:w" s="T210">bar</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_926" n="HIAT:w" s="T211">jakšə</ts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_929" n="HIAT:w" s="T212">sʼarbibaʔ</ts>
                  <nts id="Seg_930" n="HIAT:ip">.</nts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T216" id="Seg_933" n="HIAT:u" s="T213">
                  <ts e="T214" id="Seg_935" n="HIAT:w" s="T213">Miʔ</ts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_938" n="HIAT:w" s="T214">ej</ts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_940" n="HIAT:ip">(</nts>
                  <ts e="T216" id="Seg_942" n="HIAT:w" s="T215">kurollubibaʔ</ts>
                  <nts id="Seg_943" n="HIAT:ip">)</nts>
                  <nts id="Seg_944" n="HIAT:ip">.</nts>
                  <nts id="Seg_945" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T220" id="Seg_947" n="HIAT:u" s="T216">
                  <ts e="T217" id="Seg_949" n="HIAT:w" s="T216">Tibizeŋ</ts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_952" n="HIAT:w" s="T217">i</ts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_955" n="HIAT:w" s="T218">nezeŋ</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_958" n="HIAT:w" s="T219">amnobiʔi</ts>
                  <nts id="Seg_959" n="HIAT:ip">.</nts>
                  <nts id="Seg_960" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T225" id="Seg_962" n="HIAT:u" s="T220">
                  <ts e="T221" id="Seg_964" n="HIAT:w" s="T220">Da</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_967" n="HIAT:w" s="T221">măndolaʔpiʔi</ts>
                  <nts id="Seg_968" n="HIAT:ip">,</nts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_971" n="HIAT:w" s="T222">kăde</ts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_974" n="HIAT:w" s="T223">miʔ</ts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_977" n="HIAT:w" s="T224">sʼarlaʔbəbaʔ</ts>
                  <nts id="Seg_978" n="HIAT:ip">.</nts>
                  <nts id="Seg_979" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T233" id="Seg_981" n="HIAT:u" s="T225">
                  <ts e="T226" id="Seg_983" n="HIAT:w" s="T225">Šindi</ts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_985" n="HIAT:ip">(</nts>
                  <ts e="T227" id="Seg_987" n="HIAT:w" s="T226">n-</ts>
                  <nts id="Seg_988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_990" n="HIAT:w" s="T227">nʼe-</ts>
                  <nts id="Seg_991" n="HIAT:ip">)</nts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_994" n="HIAT:w" s="T228">nʼeʔtəbi</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_997" n="HIAT:w" s="T229">taŋgo</ts>
                  <nts id="Seg_998" n="HIAT:ip">,</nts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_1001" n="HIAT:w" s="T230">šindi</ts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_1004" n="HIAT:w" s="T231">ej</ts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_1007" n="HIAT:w" s="T232">nʼeʔtəbi</ts>
                  <nts id="Seg_1008" n="HIAT:ip">.</nts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T242" id="Seg_1011" n="HIAT:u" s="T233">
                  <ts e="T234" id="Seg_1013" n="HIAT:w" s="T233">Šindi</ts>
                  <nts id="Seg_1014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_1016" n="HIAT:w" s="T234">bar</ts>
                  <nts id="Seg_1017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1018" n="HIAT:ip">(</nts>
                  <ts e="T236" id="Seg_1020" n="HIAT:w" s="T235">kaŋ-</ts>
                  <nts id="Seg_1021" n="HIAT:ip">)</nts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_1024" n="HIAT:w" s="T236">kanzasi</ts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_1027" n="HIAT:w" s="T237">bar</ts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_1030" n="HIAT:w" s="T238">nʼeʔleʔbə</ts>
                  <nts id="Seg_1031" n="HIAT:ip">,</nts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_1034" n="HIAT:w" s="T239">šindi</ts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_1037" n="HIAT:w" s="T240">ej</ts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_1040" n="HIAT:w" s="T241">nʼeʔleʔbə</ts>
                  <nts id="Seg_1041" n="HIAT:ip">.</nts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T249" id="Seg_1044" n="HIAT:u" s="T242">
                  <ts e="T243" id="Seg_1046" n="HIAT:w" s="T242">Ular</ts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_1049" n="HIAT:w" s="T243">ugaːndə</ts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_1052" n="HIAT:w" s="T244">iʔgö</ts>
                  <nts id="Seg_1053" n="HIAT:ip">,</nts>
                  <nts id="Seg_1054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_1056" n="HIAT:w" s="T245">однако</ts>
                  <nts id="Seg_1057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_1059" n="HIAT:w" s="T246">šide</ts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_1062" n="HIAT:w" s="T247">bit</ts>
                  <nts id="Seg_1063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_1065" n="HIAT:w" s="T248">šide</ts>
                  <nts id="Seg_1066" n="HIAT:ip">.</nts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T252" id="Seg_1069" n="HIAT:u" s="T249">
                  <ts e="T250" id="Seg_1071" n="HIAT:w" s="T249">Abam</ts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_1074" n="HIAT:w" s="T250">dʼijenə</ts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_1077" n="HIAT:w" s="T251">kambi</ts>
                  <nts id="Seg_1078" n="HIAT:ip">.</nts>
                  <nts id="Seg_1079" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T255" id="Seg_1081" n="HIAT:u" s="T252">
                  <ts e="T253" id="Seg_1083" n="HIAT:w" s="T252">Iʔgö</ts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_1086" n="HIAT:w" s="T253">kurizəʔi</ts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_1089" n="HIAT:w" s="T254">kuʔpi</ts>
                  <nts id="Seg_1090" n="HIAT:ip">.</nts>
                  <nts id="Seg_1091" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T258" id="Seg_1093" n="HIAT:u" s="T255">
                  <ts e="T256" id="Seg_1095" n="HIAT:w" s="T255">I</ts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_1098" n="HIAT:w" s="T256">maːndə</ts>
                  <nts id="Seg_1099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_1101" n="HIAT:w" s="T257">deʔpi</ts>
                  <nts id="Seg_1102" n="HIAT:ip">.</nts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T261" id="Seg_1105" n="HIAT:u" s="T258">
                  <ts e="T259" id="Seg_1107" n="HIAT:w" s="T258">Măn</ts>
                  <nts id="Seg_1108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_1110" n="HIAT:w" s="T259">šonəgam</ts>
                  <nts id="Seg_1111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_1113" n="HIAT:w" s="T260">Permʼakovo</ts>
                  <nts id="Seg_1114" n="HIAT:ip">.</nts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T265" id="Seg_1117" n="HIAT:u" s="T261">
                  <ts e="T262" id="Seg_1119" n="HIAT:w" s="T261">Kuliom:</ts>
                  <nts id="Seg_1120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1122" n="HIAT:w" s="T262">bar</ts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_1125" n="HIAT:w" s="T263">bulan</ts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_1128" n="HIAT:w" s="T264">nuga</ts>
                  <nts id="Seg_1129" n="HIAT:ip">.</nts>
                  <nts id="Seg_1130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T271" id="Seg_1132" n="HIAT:u" s="T265">
                  <ts e="T266" id="Seg_1134" n="HIAT:w" s="T265">Măn</ts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1137" n="HIAT:w" s="T266">măndobiam</ts>
                  <nts id="Seg_1138" n="HIAT:ip">,</nts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1141" n="HIAT:w" s="T267">măndobiam</ts>
                  <nts id="Seg_1142" n="HIAT:ip">,</nts>
                  <nts id="Seg_1143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1145" n="HIAT:w" s="T268">dĭgəttə</ts>
                  <nts id="Seg_1146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1148" n="HIAT:w" s="T269">bar</ts>
                  <nts id="Seg_1149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1151" n="HIAT:w" s="T270">kirgarluʔpim</ts>
                  <nts id="Seg_1152" n="HIAT:ip">.</nts>
                  <nts id="Seg_1153" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T275" id="Seg_1155" n="HIAT:u" s="T271">
                  <ts e="T272" id="Seg_1157" n="HIAT:w" s="T271">Dĭ</ts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1160" n="HIAT:w" s="T272">bar</ts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1163" n="HIAT:w" s="T273">nuʔməluʔpi</ts>
                  <nts id="Seg_1164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1166" n="HIAT:w" s="T274">bünə</ts>
                  <nts id="Seg_1167" n="HIAT:ip">.</nts>
                  <nts id="Seg_1168" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T276" id="Seg_1170" n="HIAT:u" s="T275">
                  <nts id="Seg_1171" n="HIAT:ip">(</nts>
                  <ts e="T276" id="Seg_1173" n="HIAT:w" s="T275">Penzi</ts>
                  <nts id="Seg_1174" n="HIAT:ip">)</nts>
                  <nts id="Seg_1175" n="HIAT:ip">.</nts>
                  <nts id="Seg_1176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T282" id="Seg_1178" n="HIAT:u" s="T276">
                  <ts e="T277" id="Seg_1180" n="HIAT:w" s="T276">Dĭgəttə</ts>
                  <nts id="Seg_1181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1183" n="HIAT:w" s="T277">dĭ</ts>
                  <nts id="Seg_1184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1185" n="HIAT:ip">(</nts>
                  <ts e="T279" id="Seg_1187" n="HIAT:w" s="T278">bar</ts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1190" n="HIAT:w" s="T279">kaluʔpi</ts>
                  <nts id="Seg_1191" n="HIAT:ip">)</nts>
                  <nts id="Seg_1192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1194" n="HIAT:w" s="T280">i</ts>
                  <nts id="Seg_1195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1197" n="HIAT:w" s="T281">nuʔməluʔpi</ts>
                  <nts id="Seg_1198" n="HIAT:ip">.</nts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T287" id="Seg_1201" n="HIAT:u" s="T282">
                  <ts e="T283" id="Seg_1203" n="HIAT:w" s="T282">Măn</ts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1206" n="HIAT:w" s="T283">šobiam</ts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1209" n="HIAT:w" s="T284">da</ts>
                  <nts id="Seg_1210" n="HIAT:ip">,</nts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1213" n="HIAT:w" s="T285">Vlastə</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1216" n="HIAT:w" s="T286">nörbəbiem</ts>
                  <nts id="Seg_1217" n="HIAT:ip">.</nts>
                  <nts id="Seg_1218" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T293" id="Seg_1220" n="HIAT:u" s="T287">
                  <ts e="T288" id="Seg_1222" n="HIAT:w" s="T287">Dĭ</ts>
                  <nts id="Seg_1223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1225" n="HIAT:w" s="T288">kambi</ts>
                  <nts id="Seg_1226" n="HIAT:ip">,</nts>
                  <nts id="Seg_1227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1229" n="HIAT:w" s="T289">multuksi</ts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1232" n="HIAT:w" s="T290">da</ts>
                  <nts id="Seg_1233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1234" n="HIAT:ip">(</nts>
                  <ts e="T292" id="Seg_1236" n="HIAT:w" s="T291">kutl-</ts>
                  <nts id="Seg_1237" n="HIAT:ip">)</nts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1240" n="HIAT:w" s="T292">kuʔpi</ts>
                  <nts id="Seg_1241" n="HIAT:ip">.</nts>
                  <nts id="Seg_1242" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T295" id="Seg_1244" n="HIAT:u" s="T293">
                  <ts e="T295" id="Seg_1246" n="HIAT:w" s="T293">aspaʔ</ts>
                  <nts id="Seg_1247" n="HIAT:ip">.</nts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T299" id="Seg_1250" n="HIAT:u" s="T295">
                  <ts e="T296" id="Seg_1252" n="HIAT:w" s="T295">Măn</ts>
                  <nts id="Seg_1253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1255" n="HIAT:w" s="T296">dĭgəttə</ts>
                  <nts id="Seg_1256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1258" n="HIAT:w" s="T297">uja</ts>
                  <nts id="Seg_1259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1261" n="HIAT:w" s="T298">băzəbiam</ts>
                  <nts id="Seg_1262" n="HIAT:ip">.</nts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T301" id="Seg_1265" n="HIAT:u" s="T299">
                  <ts e="T300" id="Seg_1267" n="HIAT:w" s="T299">I</ts>
                  <nts id="Seg_1268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1270" n="HIAT:w" s="T300">ambiam</ts>
                  <nts id="Seg_1271" n="HIAT:ip">.</nts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T305" id="Seg_1274" n="HIAT:u" s="T301">
                  <ts e="T302" id="Seg_1276" n="HIAT:w" s="T301">Miʔnʼibeʔ</ts>
                  <nts id="Seg_1277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1279" n="HIAT:w" s="T302">mĭbi</ts>
                  <nts id="Seg_1280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1282" n="HIAT:w" s="T303">onʼiʔ</ts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1285" n="HIAT:w" s="T304">aspaʔ</ts>
                  <nts id="Seg_1286" n="HIAT:ip">.</nts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T314" id="Seg_1289" n="HIAT:u" s="T305">
                  <ts e="T866" id="Seg_1291" n="HIAT:w" s="T305">Финн</ts>
                  <nts id="Seg_1292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1294" n="HIAT:w" s="T866">kuza</ts>
                  <nts id="Seg_1295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1297" n="HIAT:w" s="T306">šobi</ts>
                  <nts id="Seg_1298" n="HIAT:ip">,</nts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1301" n="HIAT:w" s="T307">măn</ts>
                  <nts id="Seg_1302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1304" n="HIAT:w" s="T308">dibər</ts>
                  <nts id="Seg_1305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1307" n="HIAT:w" s="T309">mĭmbiem</ts>
                  <nts id="Seg_1308" n="HIAT:ip">,</nts>
                  <nts id="Seg_1309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1311" n="HIAT:w" s="T310">dĭzeŋ</ts>
                  <nts id="Seg_1312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1314" n="HIAT:w" s="T311">bar</ts>
                  <nts id="Seg_1315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1317" n="HIAT:w" s="T312">dʼăbaktərlaʔbəʔjə</ts>
                  <nts id="Seg_1318" n="HIAT:ip">.</nts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T321" id="Seg_1321" n="HIAT:u" s="T314">
                  <ts e="T315" id="Seg_1323" n="HIAT:w" s="T314">A</ts>
                  <nts id="Seg_1324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1326" n="HIAT:w" s="T315">dĭzeŋ</ts>
                  <nts id="Seg_1327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1329" n="HIAT:w" s="T316">bar</ts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1332" n="HIAT:w" s="T317">măna</ts>
                  <nts id="Seg_1333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1335" n="HIAT:w" s="T318">sürerluʔpiʔjə</ts>
                  <nts id="Seg_1336" n="HIAT:ip">,</nts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1339" n="HIAT:w" s="T319">măn</ts>
                  <nts id="Seg_1340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1342" n="HIAT:w" s="T320">nuʔməluʔpiem</ts>
                  <nts id="Seg_1343" n="HIAT:ip">.</nts>
                  <nts id="Seg_1344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T332" id="Seg_1346" n="HIAT:u" s="T321">
                  <nts id="Seg_1347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1349" n="HIAT:w" s="T321">unnʼa</ts>
                  <nts id="Seg_1350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1352" n="HIAT:w" s="T323">nuzaŋ</ts>
                  <nts id="Seg_1353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1355" n="HIAT:w" s="T324">amnobiʔi</ts>
                  <nts id="Seg_1356" n="HIAT:ip">,</nts>
                  <nts id="Seg_1357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1359" n="HIAT:w" s="T325">jakšə</ts>
                  <nts id="Seg_1360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1362" n="HIAT:w" s="T326">ibi</ts>
                  <nts id="Seg_1363" n="HIAT:ip">,</nts>
                  <nts id="Seg_1364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1366" n="HIAT:w" s="T327">ujat</ts>
                  <nts id="Seg_1367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1369" n="HIAT:w" s="T328">iʔgö</ts>
                  <nts id="Seg_1370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1372" n="HIAT:w" s="T329">ibi</ts>
                  <nts id="Seg_1373" n="HIAT:ip">,</nts>
                  <nts id="Seg_1374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1376" n="HIAT:w" s="T330">bar</ts>
                  <nts id="Seg_1377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1379" n="HIAT:w" s="T331">amnial</ts>
                  <nts id="Seg_1380" n="HIAT:ip">.</nts>
                  <nts id="Seg_1381" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T338" id="Seg_1383" n="HIAT:u" s="T332">
                  <ts e="T333" id="Seg_1385" n="HIAT:w" s="T332">Ălenʼən</ts>
                  <nts id="Seg_1386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1388" n="HIAT:w" s="T333">uja</ts>
                  <nts id="Seg_1389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1391" n="HIAT:w" s="T334">ugandə</ts>
                  <nts id="Seg_1392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1394" n="HIAT:w" s="T335">nömər</ts>
                  <nts id="Seg_1395" n="HIAT:ip">,</nts>
                  <nts id="Seg_1396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1398" n="HIAT:w" s="T336">kak</ts>
                  <nts id="Seg_1399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1401" n="HIAT:w" s="T337">ipek</ts>
                  <nts id="Seg_1402" n="HIAT:ip">.</nts>
                  <nts id="Seg_1403" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T343" id="Seg_1405" n="HIAT:u" s="T338">
                  <ts e="T339" id="Seg_1407" n="HIAT:w" s="T338">Iʔgö</ts>
                  <nts id="Seg_1408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1410" n="HIAT:w" s="T339">amnial</ts>
                  <nts id="Seg_1411" n="HIAT:ip">,</nts>
                  <nts id="Seg_1412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1414" n="HIAT:w" s="T340">dĭgəttə</ts>
                  <nts id="Seg_1415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1417" n="HIAT:w" s="T341">bü</ts>
                  <nts id="Seg_1418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1420" n="HIAT:w" s="T342">bĭtliel</ts>
                  <nts id="Seg_1421" n="HIAT:ip">.</nts>
                  <nts id="Seg_1422" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T348" id="Seg_1424" n="HIAT:u" s="T343">
                  <nts id="Seg_1425" n="HIAT:ip">(</nts>
                  <nts id="Seg_1426" n="HIAT:ip">(</nts>
                  <ats e="T865" id="Seg_1427" n="HIAT:non-pho" s="T343">…</ats>
                  <nts id="Seg_1428" n="HIAT:ip">)</nts>
                  <nts id="Seg_1429" n="HIAT:ip">)</nts>
                  <nts id="Seg_1430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1432" n="HIAT:w" s="T865">bü</ts>
                  <nts id="Seg_1433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1435" n="HIAT:w" s="T344">bĭtliel</ts>
                  <nts id="Seg_1436" n="HIAT:ip">,</nts>
                  <nts id="Seg_1437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1439" n="HIAT:w" s="T345">nu</ts>
                  <nts id="Seg_1440" n="HIAT:ip">…</nts>
                  <nts id="Seg_1441" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T357" id="Seg_1443" n="HIAT:u" s="T348">
                  <ts e="T349" id="Seg_1445" n="HIAT:w" s="T348">Dĭ</ts>
                  <nts id="Seg_1446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1448" n="HIAT:w" s="T349">koŋ</ts>
                  <nts id="Seg_1449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1451" n="HIAT:w" s="T350">ugandə</ts>
                  <nts id="Seg_1452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1454" n="HIAT:w" s="T351">jakšə</ts>
                  <nts id="Seg_1455" n="HIAT:ip">,</nts>
                  <nts id="Seg_1456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1458" n="HIAT:w" s="T352">ugandə</ts>
                  <nts id="Seg_1459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1461" n="HIAT:w" s="T353">kuvas</ts>
                  <nts id="Seg_1462" n="HIAT:ip">,</nts>
                  <nts id="Seg_1463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1464" n="HIAT:ip">(</nts>
                  <ts e="T355" id="Seg_1466" n="HIAT:w" s="T354">sʼimat</ts>
                  <nts id="Seg_1467" n="HIAT:ip">)</nts>
                  <nts id="Seg_1468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1469" n="HIAT:ip">(</nts>
                  <ts e="T356" id="Seg_1471" n="HIAT:w" s="T355">tʼi-</ts>
                  <nts id="Seg_1472" n="HIAT:ip">)</nts>
                  <nts id="Seg_1473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1475" n="HIAT:w" s="T356">sagər</ts>
                  <nts id="Seg_1476" n="HIAT:ip">.</nts>
                  <nts id="Seg_1477" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T360" id="Seg_1479" n="HIAT:u" s="T357">
                  <ts e="T358" id="Seg_1481" n="HIAT:w" s="T357">Püjet</ts>
                  <nts id="Seg_1482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1484" n="HIAT:w" s="T358">ej</ts>
                  <nts id="Seg_1485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1487" n="HIAT:w" s="T359">numo</ts>
                  <nts id="Seg_1488" n="HIAT:ip">.</nts>
                  <nts id="Seg_1489" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T363" id="Seg_1491" n="HIAT:u" s="T360">
                  <ts e="T361" id="Seg_1493" n="HIAT:w" s="T360">Monzaŋdə</ts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1496" n="HIAT:w" s="T361">kuvas</ts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1499" n="HIAT:w" s="T362">bar</ts>
                  <nts id="Seg_1500" n="HIAT:ip">.</nts>
                  <nts id="Seg_1501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T366" id="Seg_1503" n="HIAT:u" s="T363">
                  <ts e="T364" id="Seg_1505" n="HIAT:w" s="T363">Monzaŋdə</ts>
                  <nts id="Seg_1506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1508" n="HIAT:w" s="T364">ej</ts>
                  <nts id="Seg_1509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1511" n="HIAT:w" s="T365">nʼešpək</ts>
                  <nts id="Seg_1512" n="HIAT:ip">.</nts>
                  <nts id="Seg_1513" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T369" id="Seg_1515" n="HIAT:u" s="T366">
                  <ts e="T367" id="Seg_1517" n="HIAT:w" s="T366">Матрена</ts>
                  <nts id="Seg_1518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1520" n="HIAT:w" s="T367">bar</ts>
                  <nts id="Seg_1521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1523" n="HIAT:w" s="T368">ĭzemneʔpi</ts>
                  <nts id="Seg_1524" n="HIAT:ip">.</nts>
                  <nts id="Seg_1525" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T374" id="Seg_1527" n="HIAT:u" s="T369">
                  <ts e="T370" id="Seg_1529" n="HIAT:w" s="T369">Măn</ts>
                  <nts id="Seg_1530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1531" n="HIAT:ip">(</nts>
                  <ts e="T371" id="Seg_1533" n="HIAT:w" s="T370">dĭʔ-</ts>
                  <nts id="Seg_1534" n="HIAT:ip">)</nts>
                  <nts id="Seg_1535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1537" n="HIAT:w" s="T371">dĭʔnə</ts>
                  <nts id="Seg_1538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1539" n="HIAT:ip">(</nts>
                  <ts e="T373" id="Seg_1541" n="HIAT:w" s="T372">kal-</ts>
                  <nts id="Seg_1542" n="HIAT:ip">)</nts>
                  <nts id="Seg_1543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1545" n="HIAT:w" s="T373">kalam</ts>
                  <nts id="Seg_1546" n="HIAT:ip">.</nts>
                  <nts id="Seg_1547" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T385" id="Seg_1549" n="HIAT:u" s="T374">
                  <ts e="T375" id="Seg_1551" n="HIAT:w" s="T374">Dĭ</ts>
                  <nts id="Seg_1552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1554" n="HIAT:w" s="T375">bar</ts>
                  <nts id="Seg_1555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1557" n="HIAT:w" s="T376">măndə:</ts>
                  <nts id="Seg_1558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1560" n="HIAT:w" s="T377">Iʔ</ts>
                  <nts id="Seg_1561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1563" n="HIAT:w" s="T378">dʼăbaktəraʔ</ts>
                  <nts id="Seg_1564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1566" n="HIAT:w" s="T379">kazan</ts>
                  <nts id="Seg_1567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1569" n="HIAT:w" s="T380">šĭkətsi</ts>
                  <nts id="Seg_1570" n="HIAT:ip">,</nts>
                  <nts id="Seg_1571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1573" n="HIAT:w" s="T381">a</ts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1576" n="HIAT:w" s="T382">bostə</ts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1579" n="HIAT:w" s="T383">šĭkətsi</ts>
                  <nts id="Seg_1580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1582" n="HIAT:w" s="T384">dʼăbaktəraʔ</ts>
                  <nts id="Seg_1583" n="HIAT:ip">.</nts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T389" id="Seg_1586" n="HIAT:u" s="T385">
                  <ts e="T386" id="Seg_1588" n="HIAT:w" s="T385">Măn</ts>
                  <nts id="Seg_1589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1591" n="HIAT:w" s="T386">ugandə</ts>
                  <nts id="Seg_1592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1594" n="HIAT:w" s="T387">ĭzemniem</ts>
                  <nts id="Seg_1595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1597" n="HIAT:w" s="T388">bar</ts>
                  <nts id="Seg_1598" n="HIAT:ip">.</nts>
                  <nts id="Seg_1599" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T392" id="Seg_1601" n="HIAT:u" s="T389">
                  <ts e="T390" id="Seg_1603" n="HIAT:w" s="T389">Ugandə</ts>
                  <nts id="Seg_1604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1606" n="HIAT:w" s="T390">ĭzemniem</ts>
                  <nts id="Seg_1607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1609" n="HIAT:w" s="T391">bar</ts>
                  <nts id="Seg_1610" n="HIAT:ip">.</nts>
                  <nts id="Seg_1611" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T399" id="Seg_1613" n="HIAT:u" s="T392">
                  <ts e="T393" id="Seg_1615" n="HIAT:w" s="T392">Măn</ts>
                  <nts id="Seg_1616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1618" n="HIAT:w" s="T393">bar</ts>
                  <nts id="Seg_1619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1621" n="HIAT:w" s="T394">külaːmbiam</ts>
                  <nts id="Seg_1622" n="HIAT:ip">,</nts>
                  <nts id="Seg_1623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1624" n="HIAT:ip">(</nts>
                  <ts e="T396" id="Seg_1626" n="HIAT:w" s="T395">külaːmbiam</ts>
                  <nts id="Seg_1627" n="HIAT:ip">)</nts>
                  <nts id="Seg_1628" n="HIAT:ip">,</nts>
                  <nts id="Seg_1629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1631" n="HIAT:w" s="T396">a</ts>
                  <nts id="Seg_1632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1634" n="HIAT:w" s="T397">tăn</ts>
                  <nts id="Seg_1635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1637" n="HIAT:w" s="T398">maləl</ts>
                  <nts id="Seg_1638" n="HIAT:ip">.</nts>
                  <nts id="Seg_1639" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T405" id="Seg_1641" n="HIAT:u" s="T399">
                  <ts e="T400" id="Seg_1643" n="HIAT:w" s="T399">Dĭ</ts>
                  <nts id="Seg_1644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1646" n="HIAT:w" s="T400">külaːmbi</ts>
                  <nts id="Seg_1647" n="HIAT:ip">,</nts>
                  <nts id="Seg_1648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1650" n="HIAT:w" s="T401">šide</ts>
                  <nts id="Seg_1651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1653" n="HIAT:w" s="T402">koʔbdot</ts>
                  <nts id="Seg_1654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1655" n="HIAT:ip">(</nts>
                  <ts e="T404" id="Seg_1657" n="HIAT:w" s="T403">maː-</ts>
                  <nts id="Seg_1658" n="HIAT:ip">)</nts>
                  <nts id="Seg_1659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1661" n="HIAT:w" s="T404">maluʔpiʔi</ts>
                  <nts id="Seg_1662" n="HIAT:ip">.</nts>
                  <nts id="Seg_1663" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T412" id="Seg_1665" n="HIAT:u" s="T405">
                  <ts e="T406" id="Seg_1667" n="HIAT:w" s="T405">Onʼiʔ</ts>
                  <nts id="Seg_1668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1670" n="HIAT:w" s="T406">dĭzi</ts>
                  <nts id="Seg_1671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1673" n="HIAT:w" s="T407">ibi</ts>
                  <nts id="Seg_1674" n="HIAT:ip">,</nts>
                  <nts id="Seg_1675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1677" n="HIAT:w" s="T408">a</ts>
                  <nts id="Seg_1678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1680" n="HIAT:w" s="T409">onʼiʔ</ts>
                  <nts id="Seg_1681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1683" n="HIAT:w" s="T410">kuŋgə</ts>
                  <nts id="Seg_1684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1686" n="HIAT:w" s="T411">ibi</ts>
                  <nts id="Seg_1687" n="HIAT:ip">.</nts>
                  <nts id="Seg_1688" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T415" id="Seg_1690" n="HIAT:u" s="T412">
                  <ts e="T413" id="Seg_1692" n="HIAT:w" s="T412">Teinen</ts>
                  <nts id="Seg_1693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1695" n="HIAT:w" s="T413">bar</ts>
                  <nts id="Seg_1696" n="HIAT:ip">…</nts>
                  <nts id="Seg_1697" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T856" id="Seg_1698" n="sc" s="T419">
               <ts e="T424" id="Seg_1700" n="HIAT:u" s="T419">
                  <ts e="T420" id="Seg_1702" n="HIAT:w" s="T419">Teinen</ts>
                  <nts id="Seg_1703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1705" n="HIAT:w" s="T420">selɨj</ts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1708" n="HIAT:w" s="T421">dʼalat</ts>
                  <nts id="Seg_1709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1711" n="HIAT:w" s="T422">surno</ts>
                  <nts id="Seg_1712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1714" n="HIAT:w" s="T423">šobi</ts>
                  <nts id="Seg_1715" n="HIAT:ip">.</nts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T428" id="Seg_1718" n="HIAT:u" s="T424">
                  <ts e="T425" id="Seg_1720" n="HIAT:w" s="T424">A</ts>
                  <nts id="Seg_1721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1723" n="HIAT:w" s="T425">tüj</ts>
                  <nts id="Seg_1724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1726" n="HIAT:w" s="T426">kujo</ts>
                  <nts id="Seg_1727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1729" n="HIAT:w" s="T427">măndolaʔbə</ts>
                  <nts id="Seg_1730" n="HIAT:ip">.</nts>
                  <nts id="Seg_1731" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T435" id="Seg_1733" n="HIAT:u" s="T428">
                  <ts e="T429" id="Seg_1735" n="HIAT:w" s="T428">Novik</ts>
                  <nts id="Seg_1736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1738" n="HIAT:w" s="T429">šonəga</ts>
                  <nts id="Seg_1739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1741" n="HIAT:w" s="T430">šoškandə</ts>
                  <nts id="Seg_1742" n="HIAT:ip">,</nts>
                  <nts id="Seg_1743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1745" n="HIAT:w" s="T431">a</ts>
                  <nts id="Seg_1746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1748" n="HIAT:w" s="T432">Sidor</ts>
                  <nts id="Seg_1749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1751" n="HIAT:w" s="T433">mendə</ts>
                  <nts id="Seg_1752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1754" n="HIAT:w" s="T434">amnolaʔbə</ts>
                  <nts id="Seg_1755" n="HIAT:ip">.</nts>
                  <nts id="Seg_1756" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T440" id="Seg_1758" n="HIAT:u" s="T435">
                  <ts e="T436" id="Seg_1760" n="HIAT:w" s="T435">A</ts>
                  <nts id="Seg_1761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1763" n="HIAT:w" s="T436">il</ts>
                  <nts id="Seg_1764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1766" n="HIAT:w" s="T437">kubiʔi</ts>
                  <nts id="Seg_1767" n="HIAT:ip">,</nts>
                  <nts id="Seg_1768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1770" n="HIAT:w" s="T438">tenəbiʔi:</ts>
                  <nts id="Seg_1771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1773" n="HIAT:w" s="T439">казаки</ts>
                  <nts id="Seg_1774" n="HIAT:ip">.</nts>
                  <nts id="Seg_1775" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T445" id="Seg_1777" n="HIAT:u" s="T440">
                  <ts e="T441" id="Seg_1779" n="HIAT:w" s="T440">Predsʼedatelʼ</ts>
                  <nts id="Seg_1780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1782" n="HIAT:w" s="T441">mămbi:</ts>
                  <nts id="Seg_1783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1785" n="HIAT:w" s="T442">Nada</ts>
                  <nts id="Seg_1786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1788" n="HIAT:w" s="T443">tʼerməndə</ts>
                  <nts id="Seg_1789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1790" n="HIAT:ip">(</nts>
                  <ts e="T445" id="Seg_1792" n="HIAT:w" s="T444">adʼazirzittə</ts>
                  <nts id="Seg_1793" n="HIAT:ip">)</nts>
                  <nts id="Seg_1794" n="HIAT:ip">.</nts>
                  <nts id="Seg_1795" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T453" id="Seg_1797" n="HIAT:u" s="T445">
                  <ts e="T446" id="Seg_1799" n="HIAT:w" s="T445">Jakšə</ts>
                  <nts id="Seg_1800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1802" n="HIAT:w" s="T446">il</ts>
                  <nts id="Seg_1803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1805" n="HIAT:w" s="T447">oʔbdəsʼtə</ts>
                  <nts id="Seg_1806" n="HIAT:ip">,</nts>
                  <nts id="Seg_1807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1808" n="HIAT:ip">(</nts>
                  <ts e="T449" id="Seg_1810" n="HIAT:w" s="T448">pusk-</ts>
                  <nts id="Seg_1811" n="HIAT:ip">)</nts>
                  <nts id="Seg_1812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1813" n="HIAT:ip">(</nts>
                  <ts e="T450" id="Seg_1815" n="HIAT:w" s="T449">tog-</ts>
                  <nts id="Seg_1816" n="HIAT:ip">)</nts>
                  <nts id="Seg_1817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1819" n="HIAT:w" s="T450">puskaj</ts>
                  <nts id="Seg_1820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1822" n="HIAT:w" s="T451">dĭn</ts>
                  <nts id="Seg_1823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1825" n="HIAT:w" s="T452">togonorlaʔi</ts>
                  <nts id="Seg_1826" n="HIAT:ip">.</nts>
                  <nts id="Seg_1827" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T461" id="Seg_1829" n="HIAT:u" s="T453">
                  <ts e="T454" id="Seg_1831" n="HIAT:w" s="T453">Nada</ts>
                  <nts id="Seg_1832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1834" n="HIAT:w" s="T454">tʼerməndə</ts>
                  <nts id="Seg_1835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1837" n="HIAT:w" s="T455">kanzittə</ts>
                  <nts id="Seg_1838" n="HIAT:ip">,</nts>
                  <nts id="Seg_1839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1841" n="HIAT:w" s="T456">un</ts>
                  <nts id="Seg_1842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1844" n="HIAT:w" s="T457">nʼeʔsittə</ts>
                  <nts id="Seg_1845" n="HIAT:ip">,</nts>
                  <nts id="Seg_1846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458.tx-PKZ.1" id="Seg_1848" n="HIAT:w" s="T458">a</ts>
                  <nts id="Seg_1849" n="HIAT:ip">_</nts>
                  <ts e="T459" id="Seg_1851" n="HIAT:w" s="T458.tx-PKZ.1">to</ts>
                  <nts id="Seg_1852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1854" n="HIAT:w" s="T459">un</ts>
                  <nts id="Seg_1855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1857" n="HIAT:w" s="T460">naga</ts>
                  <nts id="Seg_1858" n="HIAT:ip">.</nts>
                  <nts id="Seg_1859" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T473" id="Seg_1861" n="HIAT:u" s="T461">
                  <nts id="Seg_1862" n="HIAT:ip">(</nts>
                  <ts e="T462" id="Seg_1864" n="HIAT:w" s="T461">Kob-</ts>
                  <nts id="Seg_1865" n="HIAT:ip">)</nts>
                  <nts id="Seg_1866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1868" n="HIAT:w" s="T462">Măn</ts>
                  <nts id="Seg_1869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1871" n="HIAT:w" s="T463">üdʼüge</ts>
                  <nts id="Seg_1872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1874" n="HIAT:w" s="T464">ibiem</ts>
                  <nts id="Seg_1875" n="HIAT:ip">,</nts>
                  <nts id="Seg_1876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1878" n="HIAT:w" s="T465">a</ts>
                  <nts id="Seg_1879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1881" n="HIAT:w" s="T466">koʔbsam</ts>
                  <nts id="Seg_1882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1884" n="HIAT:w" s="T467">urgo</ts>
                  <nts id="Seg_1885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1887" n="HIAT:w" s="T468">ibiʔi</ts>
                  <nts id="Seg_1888" n="HIAT:ip">,</nts>
                  <nts id="Seg_1889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1891" n="HIAT:w" s="T469">uge</ts>
                  <nts id="Seg_1892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1894" n="HIAT:w" s="T470">măna</ts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1897" n="HIAT:w" s="T471">ibiʔi</ts>
                  <nts id="Seg_1898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1900" n="HIAT:w" s="T472">boskəndə</ts>
                  <nts id="Seg_1901" n="HIAT:ip">.</nts>
                  <nts id="Seg_1902" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T481" id="Seg_1904" n="HIAT:u" s="T473">
                  <ts e="T474" id="Seg_1906" n="HIAT:w" s="T473">Nüjnə</ts>
                  <nts id="Seg_1907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1908" n="HIAT:ip">(</nts>
                  <ts e="T475" id="Seg_1910" n="HIAT:w" s="T474">nüjnəsʼtə</ts>
                  <nts id="Seg_1911" n="HIAT:ip">)</nts>
                  <nts id="Seg_1912" n="HIAT:ip">,</nts>
                  <nts id="Seg_1913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1914" n="HIAT:ip">(</nts>
                  <ts e="T476" id="Seg_1916" n="HIAT:w" s="T475">măna</ts>
                  <nts id="Seg_1917" n="HIAT:ip">)</nts>
                  <nts id="Seg_1918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1920" n="HIAT:w" s="T476">голос</ts>
                  <nts id="Seg_1921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1923" n="HIAT:w" s="T477">-</ts>
                  <nts id="Seg_1924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1926" n="HIAT:w" s="T478">то</ts>
                  <nts id="Seg_1927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1929" n="HIAT:w" s="T479">kuvas</ts>
                  <nts id="Seg_1930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1932" n="HIAT:w" s="T480">ibi</ts>
                  <nts id="Seg_1933" n="HIAT:ip">.</nts>
                  <nts id="Seg_1934" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T483" id="Seg_1936" n="HIAT:u" s="T481">
                  <nts id="Seg_1937" n="HIAT:ip">(</nts>
                  <ts e="T482" id="Seg_1939" n="HIAT:w" s="T481">Nüjnəbiem</ts>
                  <nts id="Seg_1940" n="HIAT:ip">)</nts>
                  <nts id="Seg_1941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1943" n="HIAT:w" s="T482">bar</ts>
                  <nts id="Seg_1944" n="HIAT:ip">.</nts>
                  <nts id="Seg_1945" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T488" id="Seg_1947" n="HIAT:u" s="T483">
                  <ts e="T484" id="Seg_1949" n="HIAT:w" s="T483">Dĭ</ts>
                  <nts id="Seg_1950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1952" n="HIAT:w" s="T484">kuza</ts>
                  <nts id="Seg_1953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1955" n="HIAT:w" s="T485">ugandə</ts>
                  <nts id="Seg_1956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1958" n="HIAT:w" s="T486">jakšə</ts>
                  <nts id="Seg_1959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1961" n="HIAT:w" s="T487">nüjlie</ts>
                  <nts id="Seg_1962" n="HIAT:ip">.</nts>
                  <nts id="Seg_1963" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T493" id="Seg_1965" n="HIAT:u" s="T488">
                  <ts e="T491" id="Seg_1967" n="HIAT:w" s="T488">Голос-то</ts>
                  <nts id="Seg_1968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1970" n="HIAT:w" s="T491">ugandə</ts>
                  <nts id="Seg_1971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1973" n="HIAT:w" s="T492">kuvas</ts>
                  <nts id="Seg_1974" n="HIAT:ip">.</nts>
                  <nts id="Seg_1975" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T493.tx-PKZ.2" id="Seg_1977" n="HIAT:u" s="T493">
                  <ts e="T493.tx-PKZ.1" id="Seg_1979" n="HIAT:w" s="T493">Голос</ts>
                  <nts id="Seg_1980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493.tx-PKZ.2" id="Seg_1982" n="HIAT:w" s="T493.tx-PKZ.1">как</ts>
                  <nts id="Seg_1983" n="HIAT:ip">…</nts>
                  <nts id="Seg_1984" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T500" id="Seg_1986" n="HIAT:u" s="T493.tx-PKZ.2">
                  <ts e="T493.tx-PKZ.3" id="Seg_1988" n="HIAT:w" s="T493.tx-PKZ.2">Нет</ts>
                  <nts id="Seg_1989" n="HIAT:ip">,</nts>
                  <nts id="Seg_1990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493.tx-PKZ.4" id="Seg_1992" n="HIAT:w" s="T493.tx-PKZ.3">не</ts>
                  <nts id="Seg_1993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1995" n="HIAT:w" s="T493.tx-PKZ.4">так</ts>
                  <nts id="Seg_1996" n="HIAT:ip">…</nts>
                  <nts id="Seg_1997" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T503" id="Seg_1999" n="HIAT:u" s="T500">
                  <ts e="T501" id="Seg_2001" n="HIAT:w" s="T500">Ugandə</ts>
                  <nts id="Seg_2002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_2004" n="HIAT:w" s="T501">tăŋ</ts>
                  <nts id="Seg_2005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_2007" n="HIAT:w" s="T502">nüjleʔbə</ts>
                  <nts id="Seg_2008" n="HIAT:ip">.</nts>
                  <nts id="Seg_2009" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T512" id="Seg_2011" n="HIAT:u" s="T503">
                  <ts e="T504" id="Seg_2013" n="HIAT:w" s="T503">Dĭ</ts>
                  <nts id="Seg_2014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_2016" n="HIAT:w" s="T504">kuza</ts>
                  <nts id="Seg_2017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_2019" n="HIAT:w" s="T505">külaːmbi</ts>
                  <nts id="Seg_2020" n="HIAT:ip">,</nts>
                  <nts id="Seg_2021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_2023" n="HIAT:w" s="T506">nada</ts>
                  <nts id="Seg_2024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_2026" n="HIAT:w" s="T507">maʔtə</ts>
                  <nts id="Seg_2027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_2029" n="HIAT:w" s="T508">azittə</ts>
                  <nts id="Seg_2030" n="HIAT:ip">,</nts>
                  <nts id="Seg_2031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2032" n="HIAT:ip">(</nts>
                  <ts e="T510" id="Seg_2034" n="HIAT:w" s="T509">pa=</ts>
                  <nts id="Seg_2035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_2037" n="HIAT:w" s="T510">pazi=</ts>
                  <nts id="Seg_2038" n="HIAT:ip">)</nts>
                  <nts id="Seg_2039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_2041" n="HIAT:w" s="T511">pagə</ts>
                  <nts id="Seg_2042" n="HIAT:ip">.</nts>
                  <nts id="Seg_2043" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T516" id="Seg_2045" n="HIAT:u" s="T512">
                  <ts e="T513" id="Seg_2047" n="HIAT:w" s="T512">I</ts>
                  <nts id="Seg_2048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_2050" n="HIAT:w" s="T513">dibər</ts>
                  <nts id="Seg_2051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_2053" n="HIAT:w" s="T514">dĭm</ts>
                  <nts id="Seg_2054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_2056" n="HIAT:w" s="T515">enzittə</ts>
                  <nts id="Seg_2057" n="HIAT:ip">.</nts>
                  <nts id="Seg_2058" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T519" id="Seg_2060" n="HIAT:u" s="T516">
                  <ts e="T517" id="Seg_2062" n="HIAT:w" s="T516">I</ts>
                  <nts id="Seg_2063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_2065" n="HIAT:w" s="T517">kros</ts>
                  <nts id="Seg_2066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_2068" n="HIAT:w" s="T518">azittə</ts>
                  <nts id="Seg_2069" n="HIAT:ip">.</nts>
                  <nts id="Seg_2070" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T524" id="Seg_2072" n="HIAT:u" s="T519">
                  <ts e="T520" id="Seg_2074" n="HIAT:w" s="T519">Dĭn</ts>
                  <nts id="Seg_2075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_2077" n="HIAT:w" s="T520">tüj</ts>
                  <nts id="Seg_2078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_2080" n="HIAT:w" s="T521">kaləj</ts>
                  <nts id="Seg_2081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2082" n="HIAT:ip">(</nts>
                  <ts e="T523" id="Seg_2084" n="HIAT:w" s="T522">šü</ts>
                  <nts id="Seg_2085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_2087" n="HIAT:w" s="T523">dʼünə</ts>
                  <nts id="Seg_2088" n="HIAT:ip">)</nts>
                  <nts id="Seg_2089" n="HIAT:ip">.</nts>
                  <nts id="Seg_2090" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T528" id="Seg_2092" n="HIAT:u" s="T524">
                  <ts e="T525" id="Seg_2094" n="HIAT:w" s="T524">Ugandə</ts>
                  <nts id="Seg_2095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_2097" n="HIAT:w" s="T525">kuvas</ts>
                  <nts id="Seg_2098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_2100" n="HIAT:w" s="T526">turaʔi</ts>
                  <nts id="Seg_2101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_2103" n="HIAT:w" s="T527">nugaʔi</ts>
                  <nts id="Seg_2104" n="HIAT:ip">.</nts>
                  <nts id="Seg_2105" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T535" id="Seg_2107" n="HIAT:u" s="T528">
                  <nts id="Seg_2108" n="HIAT:ip">(</nts>
                  <ts e="T529" id="Seg_2110" n="HIAT:w" s="T528">Dĭzeŋ=</ts>
                  <nts id="Seg_2111" n="HIAT:ip">)</nts>
                  <nts id="Seg_2112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_2114" n="HIAT:w" s="T529">Dĭzeŋ</ts>
                  <nts id="Seg_2115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2116" n="HIAT:ip">(</nts>
                  <ts e="T531" id="Seg_2118" n="HIAT:w" s="T530">pi-</ts>
                  <nts id="Seg_2119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_2121" n="HIAT:w" s="T531">pi-</ts>
                  <nts id="Seg_2122" n="HIAT:ip">)</nts>
                  <nts id="Seg_2123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_2125" n="HIAT:w" s="T532">piʔi</ts>
                  <nts id="Seg_2126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2127" n="HIAT:ip">(</nts>
                  <ts e="T534" id="Seg_2129" n="HIAT:w" s="T533">am-</ts>
                  <nts id="Seg_2130" n="HIAT:ip">)</nts>
                  <nts id="Seg_2131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_2133" n="HIAT:w" s="T534">embiʔi</ts>
                  <nts id="Seg_2134" n="HIAT:ip">.</nts>
                  <nts id="Seg_2135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T542" id="Seg_2137" n="HIAT:u" s="T535">
                  <nts id="Seg_2138" n="HIAT:ip">(</nts>
                  <ts e="T536" id="Seg_2140" n="HIAT:w" s="T535">Kuznek=</ts>
                  <nts id="Seg_2141" n="HIAT:ip">)</nts>
                  <nts id="Seg_2142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_2144" n="HIAT:w" s="T536">Kuznekən</ts>
                  <nts id="Seg_2145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_2147" n="HIAT:w" s="T537">tondə</ts>
                  <nts id="Seg_2148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_2150" n="HIAT:w" s="T538">bar</ts>
                  <nts id="Seg_2151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_2153" n="HIAT:w" s="T539">paʔi</ts>
                  <nts id="Seg_2154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2155" n="HIAT:ip">(</nts>
                  <ts e="T541" id="Seg_2157" n="HIAT:w" s="T540">nuga-</ts>
                  <nts id="Seg_2158" n="HIAT:ip">)</nts>
                  <nts id="Seg_2159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_2161" n="HIAT:w" s="T541">amnolbiʔi</ts>
                  <nts id="Seg_2162" n="HIAT:ip">.</nts>
                  <nts id="Seg_2163" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T544" id="Seg_2165" n="HIAT:u" s="T542">
                  <ts e="T543" id="Seg_2167" n="HIAT:w" s="T542">Paʔi</ts>
                  <nts id="Seg_2168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_2170" n="HIAT:w" s="T543">amnolbiʔi</ts>
                  <nts id="Seg_2171" n="HIAT:ip">.</nts>
                  <nts id="Seg_2172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T546" id="Seg_2174" n="HIAT:u" s="T544">
                  <ts e="T545" id="Seg_2176" n="HIAT:w" s="T544">Малина</ts>
                  <nts id="Seg_2177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_2179" n="HIAT:w" s="T545">amnolbiʔi</ts>
                  <nts id="Seg_2180" n="HIAT:ip">.</nts>
                  <nts id="Seg_2181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T551" id="Seg_2183" n="HIAT:u" s="T546">
                  <ts e="T547" id="Seg_2185" n="HIAT:w" s="T546">Ugandə</ts>
                  <nts id="Seg_2186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_2188" n="HIAT:w" s="T547">kuvas</ts>
                  <nts id="Seg_2189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_2191" n="HIAT:w" s="T548">tura</ts>
                  <nts id="Seg_2192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2193" n="HIAT:ip">(</nts>
                  <ts e="T550" id="Seg_2195" n="HIAT:w" s="T549">nuga-</ts>
                  <nts id="Seg_2196" n="HIAT:ip">)</nts>
                  <nts id="Seg_2197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_2199" n="HIAT:w" s="T550">nugaʔbəʔjə</ts>
                  <nts id="Seg_2200" n="HIAT:ip">.</nts>
                  <nts id="Seg_2201" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T555" id="Seg_2203" n="HIAT:u" s="T551">
                  <ts e="T552" id="Seg_2205" n="HIAT:w" s="T551">Lem</ts>
                  <nts id="Seg_2206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2208" n="HIAT:w" s="T552">keʔbde</ts>
                  <nts id="Seg_2209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2211" n="HIAT:w" s="T553">bar</ts>
                  <nts id="Seg_2212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_2214" n="HIAT:w" s="T554">amnolbiʔi</ts>
                  <nts id="Seg_2215" n="HIAT:ip">.</nts>
                  <nts id="Seg_2216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T563" id="Seg_2218" n="HIAT:u" s="T555">
                  <ts e="T867" id="Seg_2220" n="HIAT:w" s="T555">Lʼevăj</ts>
                  <nts id="Seg_2221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2222" n="HIAT:ip">(</nts>
                  <ts e="T556" id="Seg_2224" n="HIAT:w" s="T867">uga-</ts>
                  <nts id="Seg_2225" n="HIAT:ip">)</nts>
                  <nts id="Seg_2226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_2228" n="HIAT:w" s="T556">udanə</ts>
                  <nts id="Seg_2229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2231" n="HIAT:w" s="T557">kuvas</ts>
                  <nts id="Seg_2232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2234" n="HIAT:w" s="T558">i</ts>
                  <nts id="Seg_2235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_2237" n="HIAT:w" s="T559">pravăj</ts>
                  <nts id="Seg_2238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2240" n="HIAT:w" s="T560">udanə</ts>
                  <nts id="Seg_2241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2243" n="HIAT:w" s="T561">kuvas</ts>
                  <nts id="Seg_2244" n="HIAT:ip">.</nts>
                  <nts id="Seg_2245" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T566" id="Seg_2247" n="HIAT:u" s="T563">
                  <ts e="T563.tx-PKZ.1" id="Seg_2249" n="HIAT:w" s="T563">По</ts>
                  <nts id="Seg_2250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563.tx-PKZ.2" id="Seg_2252" n="HIAT:w" s="T563.tx-PKZ.1">левую</ts>
                  <nts id="Seg_2253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2255" n="HIAT:w" s="T563.tx-PKZ.2">сторону</ts>
                  <nts id="Seg_2256" n="HIAT:ip">.</nts>
                  <nts id="Seg_2257" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T571" id="Seg_2259" n="HIAT:u" s="T566">
                  <ts e="T567" id="Seg_2261" n="HIAT:w" s="T566">Ugandə</ts>
                  <nts id="Seg_2262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2264" n="HIAT:w" s="T567">kuvas</ts>
                  <nts id="Seg_2265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2266" n="HIAT:ip">(</nts>
                  <ts e="T569" id="Seg_2268" n="HIAT:w" s="T568">nura-</ts>
                  <nts id="Seg_2269" n="HIAT:ip">)</nts>
                  <nts id="Seg_2270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2272" n="HIAT:w" s="T569">turaʔi</ts>
                  <nts id="Seg_2273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2275" n="HIAT:w" s="T570">nugaʔi</ts>
                  <nts id="Seg_2276" n="HIAT:ip">.</nts>
                  <nts id="Seg_2277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T575" id="Seg_2279" n="HIAT:u" s="T571">
                  <ts e="T572" id="Seg_2281" n="HIAT:w" s="T571">Măn</ts>
                  <nts id="Seg_2282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_2284" n="HIAT:w" s="T572">mejəm</ts>
                  <nts id="Seg_2285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2287" n="HIAT:w" s="T573">самогон</ts>
                  <nts id="Seg_2288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_2290" n="HIAT:w" s="T574">mĭnzərbi</ts>
                  <nts id="Seg_2291" n="HIAT:ip">.</nts>
                  <nts id="Seg_2292" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T579" id="Seg_2294" n="HIAT:u" s="T575">
                  <ts e="T576" id="Seg_2296" n="HIAT:w" s="T575">Dĭ</ts>
                  <nts id="Seg_2297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2299" n="HIAT:w" s="T576">bar</ts>
                  <nts id="Seg_2300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2302" n="HIAT:w" s="T577">ej</ts>
                  <nts id="Seg_2303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2305" n="HIAT:w" s="T578">nʼeʔdə</ts>
                  <nts id="Seg_2306" n="HIAT:ip">.</nts>
                  <nts id="Seg_2307" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T590" id="Seg_2309" n="HIAT:u" s="T579">
                  <ts e="T580" id="Seg_2311" n="HIAT:w" s="T579">Kuza</ts>
                  <nts id="Seg_2312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2314" n="HIAT:w" s="T580">bar</ts>
                  <nts id="Seg_2315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2317" n="HIAT:w" s="T581">kudonzlaʔbə:</ts>
                  <nts id="Seg_2318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2320" n="HIAT:w" s="T582">Daška</ts>
                  <nts id="Seg_2321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2323" n="HIAT:w" s="T583">ej</ts>
                  <nts id="Seg_2324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2326" n="HIAT:w" s="T584">iləl</ts>
                  <nts id="Seg_2327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2329" n="HIAT:w" s="T585">mĭnzərzittə</ts>
                  <nts id="Seg_2330" n="HIAT:ip">,</nts>
                  <nts id="Seg_2331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2333" n="HIAT:w" s="T586">ej</ts>
                  <nts id="Seg_2334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2336" n="HIAT:w" s="T587">jakšə</ts>
                  <nts id="Seg_2337" n="HIAT:ip">,</nts>
                  <nts id="Seg_2338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2340" n="HIAT:w" s="T588">ej</ts>
                  <nts id="Seg_2341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2343" n="HIAT:w" s="T589">nʼeʔdə</ts>
                  <nts id="Seg_2344" n="HIAT:ip">.</nts>
                  <nts id="Seg_2345" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T597" id="Seg_2347" n="HIAT:u" s="T590">
                  <ts e="T591" id="Seg_2349" n="HIAT:w" s="T590">Dĭgəttə</ts>
                  <nts id="Seg_2350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2352" n="HIAT:w" s="T591">dĭ</ts>
                  <nts id="Seg_2353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2355" n="HIAT:w" s="T592">mejəm</ts>
                  <nts id="Seg_2356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2357" n="HIAT:ip">(</nts>
                  <ts e="T594" id="Seg_2359" n="HIAT:w" s="T593">mum-</ts>
                  <nts id="Seg_2360" n="HIAT:ip">)</nts>
                  <nts id="Seg_2361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2363" n="HIAT:w" s="T594">büm</ts>
                  <nts id="Seg_2364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2366" n="HIAT:w" s="T595">mĭnzərbi</ts>
                  <nts id="Seg_2367" n="HIAT:ip">,</nts>
                  <nts id="Seg_2368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2370" n="HIAT:w" s="T596">tussi</ts>
                  <nts id="Seg_2371" n="HIAT:ip">.</nts>
                  <nts id="Seg_2372" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T601" id="Seg_2374" n="HIAT:u" s="T597">
                  <ts e="T598" id="Seg_2376" n="HIAT:w" s="T597">Dĭgəttə</ts>
                  <nts id="Seg_2377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2378" n="HIAT:ip">(</nts>
                  <ts e="T599" id="Seg_2380" n="HIAT:w" s="T598">mʼa-</ts>
                  <nts id="Seg_2381" n="HIAT:ip">)</nts>
                  <nts id="Seg_2382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2384" n="HIAT:w" s="T599">uja</ts>
                  <nts id="Seg_2385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2387" n="HIAT:w" s="T600">jaʔpi</ts>
                  <nts id="Seg_2388" n="HIAT:ip">.</nts>
                  <nts id="Seg_2389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T606" id="Seg_2391" n="HIAT:u" s="T601">
                  <ts e="T602" id="Seg_2393" n="HIAT:w" s="T601">I</ts>
                  <nts id="Seg_2394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2396" n="HIAT:w" s="T602">dibər</ts>
                  <nts id="Seg_2397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2399" n="HIAT:w" s="T603">endəge</ts>
                  <nts id="Seg_2400" n="HIAT:ip">,</nts>
                  <nts id="Seg_2401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2403" n="HIAT:w" s="T604">štobɨ</ts>
                  <nts id="Seg_2404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2406" n="HIAT:w" s="T605">tustʼarlaʔbə</ts>
                  <nts id="Seg_2407" n="HIAT:ip">.</nts>
                  <nts id="Seg_2408" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T609" id="Seg_2410" n="HIAT:u" s="T606">
                  <nts id="Seg_2411" n="HIAT:ip">(</nts>
                  <ts e="T607" id="Seg_2413" n="HIAT:w" s="T606">Ejet</ts>
                  <nts id="Seg_2414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2416" n="HIAT:w" s="T607">ejen</ts>
                  <nts id="Seg_2417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2419" n="HIAT:w" s="T608">ne</ts>
                  <nts id="Seg_2420" n="HIAT:ip">)</nts>
                  <nts id="Seg_2421" n="HIAT:ip">.</nts>
                  <nts id="Seg_2422" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T614" id="Seg_2424" n="HIAT:u" s="T609">
                  <ts e="T610" id="Seg_2426" n="HIAT:w" s="T609">Kanzittə</ts>
                  <nts id="Seg_2427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2429" n="HIAT:w" s="T610">никак</ts>
                  <nts id="Seg_2430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2432" n="HIAT:w" s="T611">нельзя</ts>
                  <nts id="Seg_2433" n="HIAT:ip">,</nts>
                  <nts id="Seg_2434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2436" n="HIAT:w" s="T612">ugandə</ts>
                  <nts id="Seg_2437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2439" n="HIAT:w" s="T613">bü</ts>
                  <nts id="Seg_2440" n="HIAT:ip">.</nts>
                  <nts id="Seg_2441" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T616" id="Seg_2443" n="HIAT:u" s="T614">
                  <ts e="T615" id="Seg_2445" n="HIAT:w" s="T614">Nada</ts>
                  <nts id="Seg_2446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2448" n="HIAT:w" s="T615">parluʔsittə</ts>
                  <nts id="Seg_2449" n="HIAT:ip">.</nts>
                  <nts id="Seg_2450" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T619" id="Seg_2452" n="HIAT:u" s="T616">
                  <ts e="T617" id="Seg_2454" n="HIAT:w" s="T616">Ugandə</ts>
                  <nts id="Seg_2455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2457" n="HIAT:w" s="T617">kuŋgə</ts>
                  <nts id="Seg_2458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2460" n="HIAT:w" s="T618">idlia</ts>
                  <nts id="Seg_2461" n="HIAT:ip">.</nts>
                  <nts id="Seg_2462" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T627" id="Seg_2464" n="HIAT:u" s="T619">
                  <ts e="T868" id="Seg_2466" n="HIAT:w" s="T619">Степь</ts>
                  <nts id="Seg_2467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2469" n="HIAT:w" s="T868">bar</ts>
                  <nts id="Seg_2470" n="HIAT:ip">,</nts>
                  <nts id="Seg_2471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2473" n="HIAT:w" s="T620">paʔi</ts>
                  <nts id="Seg_2474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2476" n="HIAT:w" s="T621">naga</ts>
                  <nts id="Seg_2477" n="HIAT:ip">,</nts>
                  <nts id="Seg_2478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2480" n="HIAT:w" s="T622">onʼiʔ</ts>
                  <nts id="Seg_2481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2483" n="HIAT:w" s="T623">da</ts>
                  <nts id="Seg_2484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2486" n="HIAT:w" s="T624">pa</ts>
                  <nts id="Seg_2487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2489" n="HIAT:w" s="T625">naga</ts>
                  <nts id="Seg_2490" n="HIAT:ip">.</nts>
                  <nts id="Seg_2491" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T631" id="Seg_2493" n="HIAT:u" s="T627">
                  <ts e="T628" id="Seg_2495" n="HIAT:w" s="T627">Cтепью</ts>
                  <nts id="Seg_2496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2498" n="HIAT:w" s="T628">šonəgam</ts>
                  <nts id="Seg_2499" n="HIAT:ip">,</nts>
                  <nts id="Seg_2500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2502" n="HIAT:w" s="T629">степь</ts>
                  <nts id="Seg_2503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2505" n="HIAT:w" s="T630">nüjleʔbəm</ts>
                  <nts id="Seg_2506" n="HIAT:ip">.</nts>
                  <nts id="Seg_2507" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T636" id="Seg_2509" n="HIAT:u" s="T631">
                  <ts e="T632" id="Seg_2511" n="HIAT:w" s="T631">Dʼijenə</ts>
                  <nts id="Seg_2512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2514" n="HIAT:w" s="T632">kandəgam</ts>
                  <nts id="Seg_2515" n="HIAT:ip">,</nts>
                  <nts id="Seg_2516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2518" n="HIAT:w" s="T633">dʼije</ts>
                  <nts id="Seg_2519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2520" n="HIAT:ip">(</nts>
                  <ts e="T635" id="Seg_2522" n="HIAT:w" s="T634">nüjle-</ts>
                  <nts id="Seg_2523" n="HIAT:ip">)</nts>
                  <nts id="Seg_2524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2526" n="HIAT:w" s="T635">nüjleʔbəm</ts>
                  <nts id="Seg_2527" n="HIAT:ip">.</nts>
                  <nts id="Seg_2528" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T640" id="Seg_2530" n="HIAT:u" s="T636">
                  <ts e="T637" id="Seg_2532" n="HIAT:w" s="T636">Bünə</ts>
                  <nts id="Seg_2533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2535" n="HIAT:w" s="T637">kandəgam</ts>
                  <nts id="Seg_2536" n="HIAT:ip">,</nts>
                  <nts id="Seg_2537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2539" n="HIAT:w" s="T638">bü</ts>
                  <nts id="Seg_2540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2542" n="HIAT:w" s="T639">nüjleʔbəm</ts>
                  <nts id="Seg_2543" n="HIAT:ip">.</nts>
                  <nts id="Seg_2544" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T644" id="Seg_2546" n="HIAT:u" s="T640">
                  <ts e="T641" id="Seg_2548" n="HIAT:w" s="T640">Pagən</ts>
                  <nts id="Seg_2549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2551" n="HIAT:w" s="T641">kandəgam</ts>
                  <nts id="Seg_2552" n="HIAT:ip">,</nts>
                  <nts id="Seg_2553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2555" n="HIAT:w" s="T642">pa</ts>
                  <nts id="Seg_2556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2558" n="HIAT:w" s="T643">nüjleʔbəm</ts>
                  <nts id="Seg_2559" n="HIAT:ip">.</nts>
                  <nts id="Seg_2560" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T648" id="Seg_2562" n="HIAT:u" s="T644">
                  <ts e="T645" id="Seg_2564" n="HIAT:w" s="T644">Măn</ts>
                  <nts id="Seg_2565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2567" n="HIAT:w" s="T645">abam</ts>
                  <nts id="Seg_2568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2570" n="HIAT:w" s="T646">Kirelʼdə</ts>
                  <nts id="Seg_2571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2573" n="HIAT:w" s="T647">kambi</ts>
                  <nts id="Seg_2574" n="HIAT:ip">.</nts>
                  <nts id="Seg_2575" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T650" id="Seg_2577" n="HIAT:u" s="T648">
                  <ts e="T649" id="Seg_2579" n="HIAT:w" s="T648">Лодка</ts>
                  <nts id="Seg_2580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2582" n="HIAT:w" s="T649">ibi</ts>
                  <nts id="Seg_2583" n="HIAT:ip">.</nts>
                  <nts id="Seg_2584" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T654" id="Seg_2586" n="HIAT:u" s="T650">
                  <ts e="T651" id="Seg_2588" n="HIAT:w" s="T650">Kola</ts>
                  <nts id="Seg_2589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2590" n="HIAT:ip">(</nts>
                  <ts e="T652" id="Seg_2592" n="HIAT:w" s="T651">u-</ts>
                  <nts id="Seg_2593" n="HIAT:ip">)</nts>
                  <nts id="Seg_2594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2596" n="HIAT:w" s="T652">iʔgö</ts>
                  <nts id="Seg_2597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2599" n="HIAT:w" s="T653">dʼaʔpi</ts>
                  <nts id="Seg_2600" n="HIAT:ip">.</nts>
                  <nts id="Seg_2601" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T662" id="Seg_2603" n="HIAT:u" s="T654">
                  <ts e="T655" id="Seg_2605" n="HIAT:w" s="T654">Miʔ</ts>
                  <nts id="Seg_2606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2608" n="HIAT:w" s="T655">arəmdəbibaʔ</ts>
                  <nts id="Seg_2609" n="HIAT:ip">,</nts>
                  <nts id="Seg_2610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2612" n="HIAT:w" s="T656">i</ts>
                  <nts id="Seg_2613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2615" n="HIAT:w" s="T657">tustʼarbibaʔ</ts>
                  <nts id="Seg_2616" n="HIAT:ip">,</nts>
                  <nts id="Seg_2617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2619" n="HIAT:w" s="T658">i</ts>
                  <nts id="Seg_2620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2621" n="HIAT:ip">(</nts>
                  <ts e="T660" id="Seg_2623" n="HIAT:w" s="T659">pürbibeʔ</ts>
                  <nts id="Seg_2624" n="HIAT:ip">)</nts>
                  <nts id="Seg_2625" n="HIAT:ip">,</nts>
                  <nts id="Seg_2626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2628" n="HIAT:w" s="T660">i</ts>
                  <nts id="Seg_2629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2631" n="HIAT:w" s="T661">ambibaʔ</ts>
                  <nts id="Seg_2632" n="HIAT:ip">.</nts>
                  <nts id="Seg_2633" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T668" id="Seg_2635" n="HIAT:u" s="T662">
                  <ts e="T663" id="Seg_2637" n="HIAT:w" s="T662">Miʔ</ts>
                  <nts id="Seg_2638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2639" n="HIAT:ip">(</nts>
                  <ts e="T664" id="Seg_2641" n="HIAT:w" s="T663">mĭmbieʔ</ts>
                  <nts id="Seg_2642" n="HIAT:ip">)</nts>
                  <nts id="Seg_2643" n="HIAT:ip">,</nts>
                  <nts id="Seg_2644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2646" n="HIAT:w" s="T664">i</ts>
                  <nts id="Seg_2647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2649" n="HIAT:w" s="T665">ugandə</ts>
                  <nts id="Seg_2650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2652" n="HIAT:w" s="T666">urgo</ts>
                  <nts id="Seg_2653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2655" n="HIAT:w" s="T667">bü</ts>
                  <nts id="Seg_2656" n="HIAT:ip">.</nts>
                  <nts id="Seg_2657" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T674" id="Seg_2659" n="HIAT:u" s="T668">
                  <ts e="T669" id="Seg_2661" n="HIAT:w" s="T668">Dĭn</ts>
                  <nts id="Seg_2662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2664" n="HIAT:w" s="T669">bar</ts>
                  <nts id="Seg_2665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2667" n="HIAT:w" s="T670">sedʼiʔi</ts>
                  <nts id="Seg_2668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2669" n="HIAT:ip">(</nts>
                  <ts e="T672" id="Seg_2671" n="HIAT:w" s="T671">barəʔpibiaʔ</ts>
                  <nts id="Seg_2672" n="HIAT:ip">)</nts>
                  <nts id="Seg_2673" n="HIAT:ip">,</nts>
                  <nts id="Seg_2674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2676" n="HIAT:w" s="T672">kola</ts>
                  <nts id="Seg_2677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2679" n="HIAT:w" s="T673">dʼaʔpibaʔ</ts>
                  <nts id="Seg_2680" n="HIAT:ip">.</nts>
                  <nts id="Seg_2681" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T682" id="Seg_2683" n="HIAT:u" s="T674">
                  <ts e="T675" id="Seg_2685" n="HIAT:w" s="T674">Kuza</ts>
                  <nts id="Seg_2686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2688" n="HIAT:w" s="T675">bar</ts>
                  <nts id="Seg_2689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2691" n="HIAT:w" s="T676">nem</ts>
                  <nts id="Seg_2692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2694" n="HIAT:w" s="T677">pʼaŋdəbi</ts>
                  <nts id="Seg_2695" n="HIAT:ip">,</nts>
                  <nts id="Seg_2696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2698" n="HIAT:w" s="T678">a</ts>
                  <nts id="Seg_2699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2701" n="HIAT:w" s="T679">bostə</ts>
                  <nts id="Seg_2702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2704" n="HIAT:w" s="T680">panə</ts>
                  <nts id="Seg_2705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2707" n="HIAT:w" s="T681">edəluʔpi</ts>
                  <nts id="Seg_2708" n="HIAT:ip">.</nts>
                  <nts id="Seg_2709" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T687" id="Seg_2711" n="HIAT:u" s="T682">
                  <nts id="Seg_2712" n="HIAT:ip">(</nts>
                  <ts e="T683" id="Seg_2714" n="HIAT:w" s="T682">Urga=</ts>
                  <nts id="Seg_2715" n="HIAT:ip">)</nts>
                  <nts id="Seg_2716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2718" n="HIAT:w" s="T683">Urgaːba</ts>
                  <nts id="Seg_2719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2721" n="HIAT:w" s="T684">ugandə</ts>
                  <nts id="Seg_2722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2724" n="HIAT:w" s="T685">tărdə</ts>
                  <nts id="Seg_2725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2727" n="HIAT:w" s="T686">iʔgö</ts>
                  <nts id="Seg_2728" n="HIAT:ip">.</nts>
                  <nts id="Seg_2729" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T691" id="Seg_2731" n="HIAT:u" s="T687">
                  <ts e="T688" id="Seg_2733" n="HIAT:w" s="T687">Bar</ts>
                  <nts id="Seg_2734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2736" n="HIAT:w" s="T688">uja</ts>
                  <nts id="Seg_2737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2739" n="HIAT:w" s="T689">ej</ts>
                  <nts id="Seg_2740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2742" n="HIAT:w" s="T690">idlia</ts>
                  <nts id="Seg_2743" n="HIAT:ip">.</nts>
                  <nts id="Seg_2744" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T696" id="Seg_2746" n="HIAT:u" s="T691">
                  <ts e="T692" id="Seg_2748" n="HIAT:w" s="T691">Ugandə</ts>
                  <nts id="Seg_2749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2751" n="HIAT:w" s="T692">dĭ</ts>
                  <nts id="Seg_2752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2754" n="HIAT:w" s="T693">nömər</ts>
                  <nts id="Seg_2755" n="HIAT:ip">,</nts>
                  <nts id="Seg_2756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2758" n="HIAT:w" s="T694">ugandə</ts>
                  <nts id="Seg_2759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2761" n="HIAT:w" s="T695">kəbə</ts>
                  <nts id="Seg_2762" n="HIAT:ip">.</nts>
                  <nts id="Seg_2763" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T700" id="Seg_2765" n="HIAT:u" s="T696">
                  <ts e="T697" id="Seg_2767" n="HIAT:w" s="T696">Ujut</ts>
                  <nts id="Seg_2768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2770" n="HIAT:w" s="T697">ugandə</ts>
                  <nts id="Seg_2771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2773" n="HIAT:w" s="T698">nʼeʔdə</ts>
                  <nts id="Seg_2774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2775" n="HIAT:ip">(</nts>
                  <ts e="T700" id="Seg_2777" n="HIAT:w" s="T699">nʼeʔdə=</ts>
                  <nts id="Seg_2778" n="HIAT:ip">)</nts>
                  <nts id="Seg_2779" n="HIAT:ip">.</nts>
                  <nts id="Seg_2780" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T703" id="Seg_2782" n="HIAT:u" s="T700">
                  <ts e="T701" id="Seg_2784" n="HIAT:w" s="T700">Kadaʔi</ts>
                  <nts id="Seg_2785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2787" n="HIAT:w" s="T701">ugandə</ts>
                  <nts id="Seg_2788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2790" n="HIAT:w" s="T702">urgo</ts>
                  <nts id="Seg_2791" n="HIAT:ip">.</nts>
                  <nts id="Seg_2792" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T706" id="Seg_2794" n="HIAT:u" s="T703">
                  <ts e="T704" id="Seg_2796" n="HIAT:w" s="T703">Urgo</ts>
                  <nts id="Seg_2797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2798" n="HIAT:ip">(</nts>
                  <ts e="T705" id="Seg_2800" n="HIAT:w" s="T704">men=</ts>
                  <nts id="Seg_2801" n="HIAT:ip">)</nts>
                  <nts id="Seg_2802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2804" n="HIAT:w" s="T705">menzi</ts>
                  <nts id="Seg_2805" n="HIAT:ip">.</nts>
                  <nts id="Seg_2806" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T708" id="Seg_2808" n="HIAT:u" s="T706">
                  <ts e="T707" id="Seg_2810" n="HIAT:w" s="T706">Urgo</ts>
                  <nts id="Seg_2811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_2813" n="HIAT:w" s="T707">menzi</ts>
                  <nts id="Seg_2814" n="HIAT:ip">.</nts>
                  <nts id="Seg_2815" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T710" id="Seg_2817" n="HIAT:u" s="T708">
                  <ts e="T709" id="Seg_2819" n="HIAT:w" s="T708">Sʼimat</ts>
                  <nts id="Seg_2820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2822" n="HIAT:w" s="T709">üdʼüge</ts>
                  <nts id="Seg_2823" n="HIAT:ip">.</nts>
                  <nts id="Seg_2824" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T712" id="Seg_2826" n="HIAT:u" s="T710">
                  <ts e="T711" id="Seg_2828" n="HIAT:w" s="T710">Kuʔi</ts>
                  <nts id="Seg_2829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2831" n="HIAT:w" s="T711">üdʼüge</ts>
                  <nts id="Seg_2832" n="HIAT:ip">.</nts>
                  <nts id="Seg_2833" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T715" id="Seg_2835" n="HIAT:u" s="T712">
                  <ts e="T713" id="Seg_2837" n="HIAT:w" s="T712">Măn</ts>
                  <nts id="Seg_2838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2840" n="HIAT:w" s="T713">tuganbə</ts>
                  <nts id="Seg_2841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2843" n="HIAT:w" s="T714">šobi</ts>
                  <nts id="Seg_2844" n="HIAT:ip">.</nts>
                  <nts id="Seg_2845" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T718" id="Seg_2847" n="HIAT:u" s="T715">
                  <ts e="T716" id="Seg_2849" n="HIAT:w" s="T715">Nʼi</ts>
                  <nts id="Seg_2850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2852" n="HIAT:w" s="T716">šobi</ts>
                  <nts id="Seg_2853" n="HIAT:ip">,</nts>
                  <nts id="Seg_2854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2856" n="HIAT:w" s="T717">nezi</ts>
                  <nts id="Seg_2857" n="HIAT:ip">.</nts>
                  <nts id="Seg_2858" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T721" id="Seg_2860" n="HIAT:u" s="T718">
                  <ts e="T719" id="Seg_2862" n="HIAT:w" s="T718">Kagam</ts>
                  <nts id="Seg_2863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2865" n="HIAT:w" s="T719">šobi</ts>
                  <nts id="Seg_2866" n="HIAT:ip">,</nts>
                  <nts id="Seg_2867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_2869" n="HIAT:w" s="T720">nezi</ts>
                  <nts id="Seg_2870" n="HIAT:ip">.</nts>
                  <nts id="Seg_2871" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T723" id="Seg_2873" n="HIAT:u" s="T721">
                  <ts e="T723" id="Seg_2875" n="HIAT:w" s="T721">Esseŋ</ts>
                  <nts id="Seg_2876" n="HIAT:ip">…</nts>
                  <nts id="Seg_2877" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T726" id="Seg_2879" n="HIAT:u" s="T723">
                  <ts e="T724" id="Seg_2881" n="HIAT:w" s="T723">Măn</ts>
                  <nts id="Seg_2882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2884" n="HIAT:w" s="T724">tuganbə</ts>
                  <nts id="Seg_2885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_2887" n="HIAT:w" s="T725">šobi</ts>
                  <nts id="Seg_2888" n="HIAT:ip">.</nts>
                  <nts id="Seg_2889" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T729" id="Seg_2891" n="HIAT:u" s="T726">
                  <ts e="T727" id="Seg_2893" n="HIAT:w" s="T726">Nʼim</ts>
                  <nts id="Seg_2894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2896" n="HIAT:w" s="T727">šobi</ts>
                  <nts id="Seg_2897" n="HIAT:ip">,</nts>
                  <nts id="Seg_2898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2900" n="HIAT:w" s="T728">nezi</ts>
                  <nts id="Seg_2901" n="HIAT:ip">.</nts>
                  <nts id="Seg_2902" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T732" id="Seg_2904" n="HIAT:u" s="T729">
                  <ts e="T730" id="Seg_2906" n="HIAT:w" s="T729">Kagam</ts>
                  <nts id="Seg_2907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2909" n="HIAT:w" s="T730">šobi</ts>
                  <nts id="Seg_2910" n="HIAT:ip">,</nts>
                  <nts id="Seg_2911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_2913" n="HIAT:w" s="T731">nezi</ts>
                  <nts id="Seg_2914" n="HIAT:ip">.</nts>
                  <nts id="Seg_2915" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T735" id="Seg_2917" n="HIAT:u" s="T732">
                  <ts e="T733" id="Seg_2919" n="HIAT:w" s="T732">Koʔbdom</ts>
                  <nts id="Seg_2920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2922" n="HIAT:w" s="T733">tibizi</ts>
                  <nts id="Seg_2923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_2925" n="HIAT:w" s="T734">šobi</ts>
                  <nts id="Seg_2926" n="HIAT:ip">.</nts>
                  <nts id="Seg_2927" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T739" id="Seg_2929" n="HIAT:u" s="T735">
                  <nts id="Seg_2930" n="HIAT:ip">(</nts>
                  <ts e="T736" id="Seg_2932" n="HIAT:w" s="T735">Esseŋ=</ts>
                  <nts id="Seg_2933" n="HIAT:ip">)</nts>
                  <nts id="Seg_2934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_2936" n="HIAT:w" s="T736">Esseŋdə</ts>
                  <nts id="Seg_2937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_2939" n="HIAT:w" s="T737">dĭzi</ts>
                  <nts id="Seg_2940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_2942" n="HIAT:w" s="T738">šobiʔi</ts>
                  <nts id="Seg_2943" n="HIAT:ip">.</nts>
                  <nts id="Seg_2944" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T747" id="Seg_2946" n="HIAT:u" s="T739">
                  <ts e="T740" id="Seg_2948" n="HIAT:w" s="T739">Nada</ts>
                  <nts id="Seg_2949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2951" n="HIAT:w" s="T740">dĭzeŋdə</ts>
                  <nts id="Seg_2952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2953" n="HIAT:ip">(</nts>
                  <ts e="T742" id="Seg_2955" n="HIAT:w" s="T741">obsittə-</ts>
                  <nts id="Seg_2956" n="HIAT:ip">)</nts>
                  <nts id="Seg_2957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_2959" n="HIAT:w" s="T742">oʔbdəsʼtə</ts>
                  <nts id="Seg_2960" n="HIAT:ip">,</nts>
                  <nts id="Seg_2961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_2963" n="HIAT:w" s="T743">dĭzeŋ</ts>
                  <nts id="Seg_2964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2965" n="HIAT:ip">(</nts>
                  <ts e="T745" id="Seg_2967" n="HIAT:w" s="T744">amorzi-</ts>
                  <nts id="Seg_2968" n="HIAT:ip">)</nts>
                  <nts id="Seg_2969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_2971" n="HIAT:w" s="T745">amzittə</ts>
                  <nts id="Seg_2972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_2974" n="HIAT:w" s="T746">mĭzittə</ts>
                  <nts id="Seg_2975" n="HIAT:ip">.</nts>
                  <nts id="Seg_2976" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T757" id="Seg_2978" n="HIAT:u" s="T747">
                  <ts e="T748" id="Seg_2980" n="HIAT:w" s="T747">Măn</ts>
                  <nts id="Seg_2981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_2983" n="HIAT:w" s="T748">nʼim</ts>
                  <nts id="Seg_2984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_2986" n="HIAT:w" s="T749">bar</ts>
                  <nts id="Seg_2987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_2989" n="HIAT:w" s="T750">tăŋ</ts>
                  <nts id="Seg_2990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_2992" n="HIAT:w" s="T751">ĭzemnie</ts>
                  <nts id="Seg_2993" n="HIAT:ip">,</nts>
                  <nts id="Seg_2994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_2996" n="HIAT:w" s="T752">măn</ts>
                  <nts id="Seg_2997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_2999" n="HIAT:w" s="T753">dĭʔnə</ts>
                  <nts id="Seg_3000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_3002" n="HIAT:w" s="T754">mămbiam:</ts>
                  <nts id="Seg_3003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_3005" n="HIAT:w" s="T755">Kudajdə</ts>
                  <nts id="Seg_3006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756.tx-PKZ.1" id="Seg_3008" n="HIAT:w" s="T756">numan</ts>
                  <nts id="Seg_3009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_3011" n="HIAT:w" s="T756.tx-PKZ.1">üzeʔ</ts>
                  <nts id="Seg_3012" n="HIAT:ip">.</nts>
                  <nts id="Seg_3013" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T760" id="Seg_3015" n="HIAT:u" s="T757">
                  <ts e="T758" id="Seg_3017" n="HIAT:w" s="T757">Pʼeʔ</ts>
                  <nts id="Seg_3018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_3020" n="HIAT:w" s="T758">dĭʔə</ts>
                  <nts id="Seg_3021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_3023" n="HIAT:w" s="T759">šaldə</ts>
                  <nts id="Seg_3024" n="HIAT:ip">.</nts>
                  <nts id="Seg_3025" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T762" id="Seg_3027" n="HIAT:u" s="T760">
                  <ts e="T761" id="Seg_3029" n="HIAT:w" s="T760">Kăštaʔ</ts>
                  <nts id="Seg_3030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_3032" n="HIAT:w" s="T761">boskənə</ts>
                  <nts id="Seg_3033" n="HIAT:ip">.</nts>
                  <nts id="Seg_3034" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T767" id="Seg_3036" n="HIAT:u" s="T762">
                  <ts e="T763" id="Seg_3038" n="HIAT:w" s="T762">Dĭ</ts>
                  <nts id="Seg_3039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_3041" n="HIAT:w" s="T763">bar</ts>
                  <nts id="Seg_3042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_3044" n="HIAT:w" s="T764">kăštəbi:</ts>
                  <nts id="Seg_3045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_3047" n="HIAT:w" s="T765">Šoʔ</ts>
                  <nts id="Seg_3048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_3050" n="HIAT:w" s="T766">măna</ts>
                  <nts id="Seg_3051" n="HIAT:ip">!</nts>
                  <nts id="Seg_3052" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T769" id="Seg_3054" n="HIAT:u" s="T767">
                  <ts e="T768" id="Seg_3056" n="HIAT:w" s="T767">Šaldə</ts>
                  <nts id="Seg_3057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_3059" n="HIAT:w" s="T768">deʔ</ts>
                  <nts id="Seg_3060" n="HIAT:ip">!</nts>
                  <nts id="Seg_3061" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T771" id="Seg_3063" n="HIAT:u" s="T769">
                  <ts e="T770" id="Seg_3065" n="HIAT:w" s="T769">Dʼazirdə</ts>
                  <nts id="Seg_3066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T771" id="Seg_3068" n="HIAT:w" s="T770">măna</ts>
                  <nts id="Seg_3069" n="HIAT:ip">!</nts>
                  <nts id="Seg_3070" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T775" id="Seg_3072" n="HIAT:u" s="T771">
                  <ts e="T772" id="Seg_3074" n="HIAT:w" s="T771">Măn</ts>
                  <nts id="Seg_3075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_3077" n="HIAT:w" s="T772">taldʼen</ts>
                  <nts id="Seg_3078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_3080" n="HIAT:w" s="T773">tura</ts>
                  <nts id="Seg_3081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_3083" n="HIAT:w" s="T774">băzəbiam</ts>
                  <nts id="Seg_3084" n="HIAT:ip">.</nts>
                  <nts id="Seg_3085" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T779" id="Seg_3087" n="HIAT:u" s="T775">
                  <ts e="T776" id="Seg_3089" n="HIAT:w" s="T775">Kuris</ts>
                  <nts id="Seg_3090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_3092" n="HIAT:w" s="T776">tüʔpi</ts>
                  <nts id="Seg_3093" n="HIAT:ip">,</nts>
                  <nts id="Seg_3094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_3096" n="HIAT:w" s="T777">tože</ts>
                  <nts id="Seg_3097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_3099" n="HIAT:w" s="T778">băzəbiam</ts>
                  <nts id="Seg_3100" n="HIAT:ip">.</nts>
                  <nts id="Seg_3101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T782" id="Seg_3103" n="HIAT:u" s="T779">
                  <ts e="T780" id="Seg_3105" n="HIAT:w" s="T779">Dĭgəttə</ts>
                  <nts id="Seg_3106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_3108" n="HIAT:w" s="T780">крыльцо</ts>
                  <nts id="Seg_3109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_3111" n="HIAT:w" s="T781">băzəbiam</ts>
                  <nts id="Seg_3112" n="HIAT:ip">.</nts>
                  <nts id="Seg_3113" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T787" id="Seg_3115" n="HIAT:u" s="T782">
                  <ts e="T783" id="Seg_3117" n="HIAT:w" s="T782">Dĭgəttə</ts>
                  <nts id="Seg_3118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_3120" n="HIAT:w" s="T783">kambiam</ts>
                  <nts id="Seg_3121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_3123" n="HIAT:w" s="T784">bü</ts>
                  <nts id="Seg_3124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_3126" n="HIAT:w" s="T785">tazirzittə</ts>
                  <nts id="Seg_3127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_3129" n="HIAT:w" s="T786">multʼanə</ts>
                  <nts id="Seg_3130" n="HIAT:ip">.</nts>
                  <nts id="Seg_3131" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T792" id="Seg_3133" n="HIAT:u" s="T787">
                  <ts e="T789" id="Seg_3135" n="HIAT:w" s="T787">Oʔb</ts>
                  <nts id="Seg_3136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_3138" n="HIAT:w" s="T789">muktuʔ</ts>
                  <nts id="Seg_3139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_3141" n="HIAT:w" s="T790">aspaʔ</ts>
                  <nts id="Seg_3142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_3144" n="HIAT:w" s="T791">deʔpiem</ts>
                  <nts id="Seg_3145" n="HIAT:ip">.</nts>
                  <nts id="Seg_3146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T796" id="Seg_3148" n="HIAT:u" s="T792">
                  <ts e="T793" id="Seg_3150" n="HIAT:w" s="T792">Măn</ts>
                  <nts id="Seg_3151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_3153" n="HIAT:w" s="T793">teinen</ts>
                  <nts id="Seg_3154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_3156" n="HIAT:w" s="T794">erten</ts>
                  <nts id="Seg_3157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_3159" n="HIAT:w" s="T795">uʔbdəbiam</ts>
                  <nts id="Seg_3160" n="HIAT:ip">.</nts>
                  <nts id="Seg_3161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T800" id="Seg_3163" n="HIAT:u" s="T796">
                  <ts e="T797" id="Seg_3165" n="HIAT:w" s="T796">Целый</ts>
                  <nts id="Seg_3166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3167" n="HIAT:ip">(</nts>
                  <ts e="T798" id="Seg_3169" n="HIAT:w" s="T797">dʼa-</ts>
                  <nts id="Seg_3170" n="HIAT:ip">)</nts>
                  <nts id="Seg_3171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_3173" n="HIAT:w" s="T798">dʼala</ts>
                  <nts id="Seg_3174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_3176" n="HIAT:w" s="T799">togonorbiam</ts>
                  <nts id="Seg_3177" n="HIAT:ip">.</nts>
                  <nts id="Seg_3178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T802" id="Seg_3180" n="HIAT:u" s="T800">
                  <ts e="T801" id="Seg_3182" n="HIAT:w" s="T800">Bar</ts>
                  <nts id="Seg_3183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_3185" n="HIAT:w" s="T801">tararluʔpiem</ts>
                  <nts id="Seg_3186" n="HIAT:ip">.</nts>
                  <nts id="Seg_3187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T808" id="Seg_3189" n="HIAT:u" s="T802">
                  <nts id="Seg_3190" n="HIAT:ip">(</nts>
                  <ts e="T803" id="Seg_3192" n="HIAT:w" s="T802">Nü-</ts>
                  <nts id="Seg_3193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_3195" n="HIAT:w" s="T803">nüdʼin=</ts>
                  <nts id="Seg_3196" n="HIAT:ip">)</nts>
                  <nts id="Seg_3197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_3199" n="HIAT:w" s="T804">Nüdʼin</ts>
                  <nts id="Seg_3200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_3202" n="HIAT:w" s="T805">šobi</ts>
                  <nts id="Seg_3203" n="HIAT:ip">,</nts>
                  <nts id="Seg_3204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_3206" n="HIAT:w" s="T806">măn</ts>
                  <nts id="Seg_3207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_3209" n="HIAT:w" s="T807">kunolluʔpiam</ts>
                  <nts id="Seg_3210" n="HIAT:ip">.</nts>
                  <nts id="Seg_3211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T814" id="Seg_3213" n="HIAT:u" s="T808">
                  <ts e="T809" id="Seg_3215" n="HIAT:w" s="T808">Măn</ts>
                  <nts id="Seg_3216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_3218" n="HIAT:w" s="T809">dĭzem</ts>
                  <nts id="Seg_3219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_3221" n="HIAT:w" s="T810">kusʼtə</ts>
                  <nts id="Seg_3222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3223" n="HIAT:ip">(</nts>
                  <ts e="T812" id="Seg_3225" n="HIAT:w" s="T811">nʼe-</ts>
                  <nts id="Seg_3226" n="HIAT:ip">)</nts>
                  <nts id="Seg_3227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T813" id="Seg_3229" n="HIAT:w" s="T812">nʼe</ts>
                  <nts id="Seg_3230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T814" id="Seg_3232" n="HIAT:w" s="T813">axota</ts>
                  <nts id="Seg_3233" n="HIAT:ip">.</nts>
                  <nts id="Seg_3234" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T819" id="Seg_3236" n="HIAT:u" s="T814">
                  <ts e="T815" id="Seg_3238" n="HIAT:w" s="T814">Dĭzeŋ</ts>
                  <nts id="Seg_3239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_3241" n="HIAT:w" s="T815">bar</ts>
                  <nts id="Seg_3242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_3244" n="HIAT:w" s="T816">üge</ts>
                  <nts id="Seg_3245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3246" n="HIAT:ip">(</nts>
                  <ts e="T818" id="Seg_3248" n="HIAT:w" s="T817">šam-</ts>
                  <nts id="Seg_3249" n="HIAT:ip">)</nts>
                  <nts id="Seg_3250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_3252" n="HIAT:w" s="T818">šamnaʔbəʔjə</ts>
                  <nts id="Seg_3253" n="HIAT:ip">.</nts>
                  <nts id="Seg_3254" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T821" id="Seg_3256" n="HIAT:u" s="T819">
                  <ts e="T820" id="Seg_3258" n="HIAT:w" s="T819">Šamnaʔbəʔjə</ts>
                  <nts id="Seg_3259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T821" id="Seg_3261" n="HIAT:w" s="T820">dĭzeŋ</ts>
                  <nts id="Seg_3262" n="HIAT:ip">.</nts>
                  <nts id="Seg_3263" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T826" id="Seg_3265" n="HIAT:u" s="T821">
                  <ts e="T822" id="Seg_3267" n="HIAT:w" s="T821">Arda</ts>
                  <nts id="Seg_3268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3269" n="HIAT:ip">(</nts>
                  <ts e="T823" id="Seg_3271" n="HIAT:w" s="T822">nʼe-</ts>
                  <nts id="Seg_3272" n="HIAT:ip">)</nts>
                  <nts id="Seg_3273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T824" id="Seg_3275" n="HIAT:w" s="T823">nʼilgösʼtə</ts>
                  <nts id="Seg_3276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_3278" n="HIAT:w" s="T824">nʼe</ts>
                  <nts id="Seg_3279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_3281" n="HIAT:w" s="T825">axota</ts>
                  <nts id="Seg_3282" n="HIAT:ip">.</nts>
                  <nts id="Seg_3283" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T831" id="Seg_3285" n="HIAT:u" s="T826">
                  <ts e="T827" id="Seg_3287" n="HIAT:w" s="T826">Pušaj</ts>
                  <nts id="Seg_3288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T828" id="Seg_3290" n="HIAT:w" s="T827">măna</ts>
                  <nts id="Seg_3291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T829" id="Seg_3293" n="HIAT:w" s="T828">sʼimandə</ts>
                  <nts id="Seg_3294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_3296" n="HIAT:w" s="T829">ej</ts>
                  <nts id="Seg_3297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T831" id="Seg_3299" n="HIAT:w" s="T830">šoləʔjə</ts>
                  <nts id="Seg_3300" n="HIAT:ip">.</nts>
                  <nts id="Seg_3301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T835" id="Seg_3303" n="HIAT:u" s="T831">
                  <ts e="T832" id="Seg_3305" n="HIAT:w" s="T831">Măna</ts>
                  <nts id="Seg_3306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_3308" n="HIAT:w" s="T832">dĭzem</ts>
                  <nts id="Seg_3309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_3311" n="HIAT:w" s="T833">ej</ts>
                  <nts id="Seg_3312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_3314" n="HIAT:w" s="T834">kereʔ</ts>
                  <nts id="Seg_3315" n="HIAT:ip">.</nts>
                  <nts id="Seg_3316" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T838" id="Seg_3318" n="HIAT:u" s="T835">
                  <ts e="T836" id="Seg_3320" n="HIAT:w" s="T835">Ulum</ts>
                  <nts id="Seg_3321" n="HIAT:ip">,</nts>
                  <nts id="Seg_3322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T837" id="Seg_3324" n="HIAT:w" s="T836">eʔbdəm</ts>
                  <nts id="Seg_3325" n="HIAT:ip">,</nts>
                  <nts id="Seg_3326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_3328" n="HIAT:w" s="T837">kuzaŋbə</ts>
                  <nts id="Seg_3329" n="HIAT:ip">.</nts>
                  <nts id="Seg_3330" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T839" id="Seg_3332" n="HIAT:u" s="T838">
                  <ts e="T839" id="Seg_3334" n="HIAT:w" s="T838">Tĭmem</ts>
                  <nts id="Seg_3335" n="HIAT:ip">.</nts>
                  <nts id="Seg_3336" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T841" id="Seg_3338" n="HIAT:u" s="T839">
                  <ts e="T840" id="Seg_3340" n="HIAT:w" s="T839">Monzaŋdə</ts>
                  <nts id="Seg_3341" n="HIAT:ip">,</nts>
                  <nts id="Seg_3342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T841" id="Seg_3344" n="HIAT:w" s="T840">šĭkem</ts>
                  <nts id="Seg_3345" n="HIAT:ip">.</nts>
                  <nts id="Seg_3346" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T843" id="Seg_3348" n="HIAT:u" s="T841">
                  <ts e="T842" id="Seg_3350" n="HIAT:w" s="T841">Sʼimam</ts>
                  <nts id="Seg_3351" n="HIAT:ip">,</nts>
                  <nts id="Seg_3352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_3354" n="HIAT:w" s="T842">püjem</ts>
                  <nts id="Seg_3355" n="HIAT:ip">.</nts>
                  <nts id="Seg_3356" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T845" id="Seg_3358" n="HIAT:u" s="T843">
                  <ts e="T844" id="Seg_3360" n="HIAT:w" s="T843">Udazaŋbə</ts>
                  <nts id="Seg_3361" n="HIAT:ip">,</nts>
                  <nts id="Seg_3362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_3364" n="HIAT:w" s="T844">bögəlbə</ts>
                  <nts id="Seg_3365" n="HIAT:ip">.</nts>
                  <nts id="Seg_3366" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T847" id="Seg_3368" n="HIAT:u" s="T845">
                  <nts id="Seg_3369" n="HIAT:ip">(</nts>
                  <ts e="T846" id="Seg_3371" n="HIAT:w" s="T845">Sĭj-</ts>
                  <nts id="Seg_3372" n="HIAT:ip">)</nts>
                  <nts id="Seg_3373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_3375" n="HIAT:w" s="T846">Sĭjbə</ts>
                  <nts id="Seg_3376" n="HIAT:ip">.</nts>
                  <nts id="Seg_3377" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T849" id="Seg_3379" n="HIAT:u" s="T847">
                  <ts e="T848" id="Seg_3381" n="HIAT:w" s="T847">Šüjöm</ts>
                  <nts id="Seg_3382" n="HIAT:ip">,</nts>
                  <nts id="Seg_3383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T849" id="Seg_3385" n="HIAT:w" s="T848">nanəm</ts>
                  <nts id="Seg_3386" n="HIAT:ip">.</nts>
                  <nts id="Seg_3387" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T853" id="Seg_3389" n="HIAT:u" s="T849">
                  <nts id="Seg_3390" n="HIAT:ip">(</nts>
                  <ts e="T850" id="Seg_3392" n="HIAT:w" s="T849">Uj-</ts>
                  <nts id="Seg_3393" n="HIAT:ip">)</nts>
                  <nts id="Seg_3394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T851" id="Seg_3396" n="HIAT:w" s="T850">Bögəlbə</ts>
                  <nts id="Seg_3397" n="HIAT:ip">,</nts>
                  <nts id="Seg_3398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_3400" n="HIAT:w" s="T851">kötendə</ts>
                  <nts id="Seg_3401" n="HIAT:ip">,</nts>
                  <nts id="Seg_3402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_3404" n="HIAT:w" s="T852">üjüm</ts>
                  <nts id="Seg_3405" n="HIAT:ip">.</nts>
                  <nts id="Seg_3406" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T856" id="Seg_3408" n="HIAT:u" s="T853">
                  <ts e="T854" id="Seg_3410" n="HIAT:w" s="T853">Aŋdə</ts>
                  <nts id="Seg_3411" n="HIAT:ip">,</nts>
                  <nts id="Seg_3412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3413" n="HIAT:ip">(</nts>
                  <ts e="T855" id="Seg_3415" n="HIAT:w" s="T854">sĭjbə=</ts>
                  <nts id="Seg_3416" n="HIAT:ip">)</nts>
                  <nts id="Seg_3417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856" id="Seg_3419" n="HIAT:w" s="T855">sĭjdə</ts>
                  <nts id="Seg_3420" n="HIAT:ip">.</nts>
                  <nts id="Seg_3421" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T858" id="Seg_3422" n="sc" s="T857">
               <ts e="T858" id="Seg_3424" n="HIAT:u" s="T857">
                  <ts e="T858" id="Seg_3426" n="HIAT:w" s="T857">Nüjüzaŋdə</ts>
                  <nts id="Seg_3427" n="HIAT:ip">.</nts>
                  <nts id="Seg_3428" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T861" id="Seg_3429" n="sc" s="T859">
               <ts e="T861" id="Seg_3431" n="HIAT:u" s="T859">
                  <nts id="Seg_3432" n="HIAT:ip">(</nts>
                  <ts e="T860" id="Seg_3434" n="HIAT:w" s="T859">E-</ts>
                  <nts id="Seg_3435" n="HIAT:ip">)</nts>
                  <nts id="Seg_3436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T861" id="Seg_3438" n="HIAT:w" s="T860">Lezeŋdə</ts>
                  <nts id="Seg_3439" n="HIAT:ip">.</nts>
                  <nts id="Seg_3440" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T863" id="Seg_3441" n="sc" s="T862">
               <ts e="T863" id="Seg_3443" n="HIAT:u" s="T862">
                  <ts e="T863" id="Seg_3445" n="HIAT:w" s="T862">Kem</ts>
                  <nts id="Seg_3446" n="HIAT:ip">.</nts>
                  <nts id="Seg_3447" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-PKZ">
            <ts e="T203" id="Seg_3448" n="sc" s="T4">
               <ts e="T5" id="Seg_3450" n="e" s="T4">Döbər </ts>
               <ts e="T6" id="Seg_3452" n="e" s="T5">šolal </ts>
               <ts e="T7" id="Seg_3454" n="e" s="T6">dăk, </ts>
               <ts e="T8" id="Seg_3456" n="e" s="T7">măna </ts>
               <ts e="T9" id="Seg_3458" n="e" s="T8">sima </ts>
               <ts e="T10" id="Seg_3460" n="e" s="T9">iʔ. </ts>
               <ts e="T11" id="Seg_3462" n="e" s="T10">Tăn </ts>
               <ts e="T12" id="Seg_3464" n="e" s="T11">büzʼe </ts>
               <ts e="T13" id="Seg_3466" n="e" s="T12">gibər </ts>
               <ts e="T14" id="Seg_3468" n="e" s="T13">kambi? </ts>
               <ts e="T15" id="Seg_3470" n="e" s="T14">Dʼelamdə </ts>
               <ts e="T16" id="Seg_3472" n="e" s="T15">(kalla-) </ts>
               <ts e="T869" id="Seg_3474" n="e" s="T16">kalla </ts>
               <ts e="T17" id="Seg_3476" n="e" s="T869">dʼürbi, </ts>
               <ts e="T18" id="Seg_3478" n="e" s="T17">ine. </ts>
               <ts e="T19" id="Seg_3480" n="e" s="T18">Unnʼa </ts>
               <ts e="T20" id="Seg_3482" n="e" s="T19">kambi? </ts>
               <ts e="T21" id="Seg_3484" n="e" s="T20">Dʼok, </ts>
               <ts e="T22" id="Seg_3486" n="e" s="T21">šidegöʔ </ts>
               <ts e="T870" id="Seg_3488" n="e" s="T22">kalla </ts>
               <ts e="T23" id="Seg_3490" n="e" s="T870">dʼürbiʔi </ts>
               <ts e="T24" id="Seg_3492" n="e" s="T23">bar. </ts>
               <ts e="T25" id="Seg_3494" n="e" s="T24">Kamən </ts>
               <ts e="T26" id="Seg_3496" n="e" s="T25">(ka-) </ts>
               <ts e="T27" id="Seg_3498" n="e" s="T26">kambiʔi? </ts>
               <ts e="T28" id="Seg_3500" n="e" s="T27">(Погоди). </ts>
               <ts e="T29" id="Seg_3502" n="e" s="T28">Taldʼen </ts>
               <ts e="T30" id="Seg_3504" n="e" s="T29">kambiʔi, </ts>
               <ts e="T31" id="Seg_3506" n="e" s="T30">ugandə </ts>
               <ts e="T32" id="Seg_3508" n="e" s="T31">erten. </ts>
               <ts e="T33" id="Seg_3510" n="e" s="T32">Ĭmbi </ts>
               <ts e="T34" id="Seg_3512" n="e" s="T33">šaːmnaʔbəl, </ts>
               <ts e="T35" id="Seg_3514" n="e" s="T34">ej </ts>
               <ts e="T36" id="Seg_3516" n="e" s="T35">(sĭ- </ts>
               <ts e="T37" id="Seg_3518" n="e" s="T36">sĭr-) </ts>
               <ts e="T38" id="Seg_3520" n="e" s="T37">ej </ts>
               <ts e="T39" id="Seg_3522" n="e" s="T38">(sĭr-) </ts>
               <ts e="T40" id="Seg_3524" n="e" s="T39">sĭriel? </ts>
               <ts e="T41" id="Seg_3526" n="e" s="T40">Köžetsnəktə </ts>
               <ts e="T42" id="Seg_3528" n="e" s="T41">(nuldə-) </ts>
               <ts e="T43" id="Seg_3530" n="e" s="T42">nuldʼit. </ts>
               <ts e="T44" id="Seg_3532" n="e" s="T43">Bar </ts>
               <ts e="T45" id="Seg_3534" n="e" s="T44">(kujnekəm) </ts>
               <ts e="T46" id="Seg_3536" n="e" s="T45">nĭŋgəluʔpi. </ts>
               <ts e="T47" id="Seg_3538" n="e" s="T46">Ši </ts>
               <ts e="T48" id="Seg_3540" n="e" s="T47">molaːmbi. </ts>
               <ts e="T49" id="Seg_3542" n="e" s="T48">Măn </ts>
               <ts e="T50" id="Seg_3544" n="e" s="T49">dĭgəttə </ts>
               <ts e="T51" id="Seg_3546" n="e" s="T50">dĭm </ts>
               <ts e="T52" id="Seg_3548" n="e" s="T51">(sö-) </ts>
               <ts e="T53" id="Seg_3550" n="e" s="T52">šödörbiam, </ts>
               <ts e="T54" id="Seg_3552" n="e" s="T53">nʼitkaʔizʼiʔ. </ts>
               <ts e="T55" id="Seg_3554" n="e" s="T54">Nʼimizʼiʔ, </ts>
               <ts e="T56" id="Seg_3556" n="e" s="T55">ĭndak. </ts>
               <ts e="T57" id="Seg_3558" n="e" s="T56">Tʼegermaʔgən </ts>
               <ts e="T58" id="Seg_3560" n="e" s="T57">bar </ts>
               <ts e="T59" id="Seg_3562" n="e" s="T58">küzürleʔbə </ts>
               <ts e="T60" id="Seg_3564" n="e" s="T59">kunguro. </ts>
               <ts e="T61" id="Seg_3566" n="e" s="T60">Nada </ts>
               <ts e="T62" id="Seg_3568" n="e" s="T61">kanzittə </ts>
               <ts e="T63" id="Seg_3570" n="e" s="T62">kudajdə </ts>
               <ts e="T64" id="Seg_3572" n="e" s="T63">(üzəš-) </ts>
               <ts e="T65" id="Seg_3574" n="e" s="T64">üzəsʼtə </ts>
               <ts e="T66" id="Seg_3576" n="e" s="T65">dĭgəttə. </ts>
               <ts e="T67" id="Seg_3578" n="e" s="T66">Kăštəliaʔi </ts>
               <ts e="T68" id="Seg_3580" n="e" s="T67">dĭzeŋ </ts>
               <ts e="T69" id="Seg_3582" n="e" s="T68">bar. </ts>
               <ts e="T70" id="Seg_3584" n="e" s="T69">Edət </ts>
               <ts e="T71" id="Seg_3586" n="e" s="T70">inenə </ts>
               <ts e="T72" id="Seg_3588" n="e" s="T71">koŋgoro. </ts>
               <ts e="T73" id="Seg_3590" n="e" s="T72">Sagəšdə </ts>
               <ts e="T74" id="Seg_3592" n="e" s="T73">naga, </ts>
               <ts e="T75" id="Seg_3594" n="e" s="T74">bar </ts>
               <ts e="T76" id="Seg_3596" n="e" s="T75">ĭmbidə </ts>
               <ts e="T77" id="Seg_3598" n="e" s="T76">ej </ts>
               <ts e="T78" id="Seg_3600" n="e" s="T77">tĭmnem. </ts>
               <ts e="T79" id="Seg_3602" n="e" s="T78">Ĭmbidə </ts>
               <ts e="T80" id="Seg_3604" n="e" s="T79">ej </ts>
               <ts e="T81" id="Seg_3606" n="e" s="T80">moliam </ts>
               <ts e="T82" id="Seg_3608" n="e" s="T81">nörbəsʼtə </ts>
               <ts e="T83" id="Seg_3610" n="e" s="T82">bar, </ts>
               <ts e="T84" id="Seg_3612" n="e" s="T83">sagəšdə </ts>
               <ts e="T85" id="Seg_3614" n="e" s="T84">naga. </ts>
               <ts e="T86" id="Seg_3616" n="e" s="T85">Dʼijenə </ts>
               <ts e="T87" id="Seg_3618" n="e" s="T86">kambiam. </ts>
               <ts e="T88" id="Seg_3620" n="e" s="T87">Bar </ts>
               <ts e="T89" id="Seg_3622" n="e" s="T88">(dʼü- </ts>
               <ts e="T90" id="Seg_3624" n="e" s="T89">dʼür- </ts>
               <ts e="T91" id="Seg_3626" n="e" s="T90">dʼürdə-) </ts>
               <ts e="T92" id="Seg_3628" n="e" s="T91">tʼürleʔbə. </ts>
               <ts e="T93" id="Seg_3630" n="e" s="T92">Dʼijenə </ts>
               <ts e="T94" id="Seg_3632" n="e" s="T93">kambiam, </ts>
               <ts e="T95" id="Seg_3634" n="e" s="T94">bar </ts>
               <ts e="T96" id="Seg_3636" n="e" s="T95">dʼürleʔpiem. </ts>
               <ts e="T97" id="Seg_3638" n="e" s="T96">Dĭgəttə </ts>
               <ts e="T98" id="Seg_3640" n="e" s="T97">kirgarbiam </ts>
               <ts e="T99" id="Seg_3642" n="e" s="T98">bar. </ts>
               <ts e="T100" id="Seg_3644" n="e" s="T99">Dĭgəttə </ts>
               <ts e="T101" id="Seg_3646" n="e" s="T100">šobiam. </ts>
               <ts e="T102" id="Seg_3648" n="e" s="T101">Kondʼo </ts>
               <ts e="T103" id="Seg_3650" n="e" s="T102">amnobi. </ts>
               <ts e="T104" id="Seg_3652" n="e" s="T103">Ĭmbidə </ts>
               <ts e="T106" id="Seg_3654" n="e" s="T104">bar </ts>
               <ts e="T107" id="Seg_3656" n="e" s="T106">ĭmbidə </ts>
               <ts e="T108" id="Seg_3658" n="e" s="T107">ej </ts>
               <ts e="T109" id="Seg_3660" n="e" s="T108">dʼăbaktərbi. </ts>
               <ts e="T110" id="Seg_3662" n="e" s="T109">Dĭ </ts>
               <ts e="T111" id="Seg_3664" n="e" s="T110">kandəga, </ts>
               <ts e="T112" id="Seg_3666" n="e" s="T111">a </ts>
               <ts e="T113" id="Seg_3668" n="e" s="T112">măn </ts>
               <ts e="T114" id="Seg_3670" n="e" s="T113">dĭm </ts>
               <ts e="T115" id="Seg_3672" n="e" s="T114">(pă-) </ts>
               <ts e="T116" id="Seg_3674" n="e" s="T115">bĭdliem. </ts>
               <ts e="T117" id="Seg_3676" n="e" s="T116">Iʔ </ts>
               <ts e="T118" id="Seg_3678" n="e" s="T117">kanaʔ, </ts>
               <ts e="T119" id="Seg_3680" n="e" s="T118">paraʔ </ts>
               <ts e="T120" id="Seg_3682" n="e" s="T119">döbər! </ts>
               <ts e="T121" id="Seg_3684" n="e" s="T120">Măn </ts>
               <ts e="T122" id="Seg_3686" n="e" s="T121">tănan </ts>
               <ts e="T123" id="Seg_3688" n="e" s="T122">mănliam: </ts>
               <ts e="T124" id="Seg_3690" n="e" s="T123">paraʔ </ts>
               <ts e="T125" id="Seg_3692" n="e" s="T124">döbər! </ts>
               <ts e="T126" id="Seg_3694" n="e" s="T125">Măn </ts>
               <ts e="T127" id="Seg_3696" n="e" s="T126">ugandə </ts>
               <ts e="T128" id="Seg_3698" n="e" s="T127">urgo </ts>
               <ts e="T129" id="Seg_3700" n="e" s="T128">il </ts>
               <ts e="T130" id="Seg_3702" n="e" s="T129">ige. </ts>
               <ts e="T131" id="Seg_3704" n="e" s="T130">Ipek </ts>
               <ts e="T132" id="Seg_3706" n="e" s="T131">iʔgö </ts>
               <ts e="T133" id="Seg_3708" n="e" s="T132">(kereʔnə-) </ts>
               <ts e="T134" id="Seg_3710" n="e" s="T133">kereʔ. </ts>
               <ts e="T135" id="Seg_3712" n="e" s="T134">Iʔ </ts>
               <ts e="T136" id="Seg_3714" n="e" s="T135">bögəldə </ts>
               <ts e="T137" id="Seg_3716" n="e" s="T136">dĭm! </ts>
               <ts e="T138" id="Seg_3718" n="e" s="T137">Udal </ts>
               <ts e="T139" id="Seg_3720" n="e" s="T138">ej </ts>
               <ts e="T140" id="Seg_3722" n="e" s="T139">endə! </ts>
               <ts e="T142" id="Seg_3724" n="e" s="T140">(Ižəmdə) </ts>
               <ts e="T143" id="Seg_3726" n="e" s="T142">dĭm, </ts>
               <ts e="T144" id="Seg_3728" n="e" s="T143">(ižəmdə) </ts>
               <ts e="T145" id="Seg_3730" n="e" s="T144">dĭm </ts>
               <ts e="T146" id="Seg_3732" n="e" s="T145">udazi. </ts>
               <ts e="T148" id="Seg_3734" n="e" s="T146">Boskənə </ts>
               <ts e="T149" id="Seg_3736" n="e" s="T148">iʔgö </ts>
               <ts e="T150" id="Seg_3738" n="e" s="T149">ibiem, </ts>
               <ts e="T151" id="Seg_3740" n="e" s="T150">a </ts>
               <ts e="T152" id="Seg_3742" n="e" s="T151">dĭʔnə </ts>
               <ts e="T153" id="Seg_3744" n="e" s="T152">amgam </ts>
               <ts e="T154" id="Seg_3746" n="e" s="T153">ibiem. </ts>
               <ts e="T155" id="Seg_3748" n="e" s="T154">Boskəndə </ts>
               <ts e="T156" id="Seg_3750" n="e" s="T155">iʔgö </ts>
               <ts e="T157" id="Seg_3752" n="e" s="T156">ilie, </ts>
               <ts e="T158" id="Seg_3754" n="e" s="T157">a </ts>
               <ts e="T159" id="Seg_3756" n="e" s="T158">dĭʔnə </ts>
               <ts e="T160" id="Seg_3758" n="e" s="T159">amgam </ts>
               <ts e="T161" id="Seg_3760" n="e" s="T160">ilie. </ts>
               <ts e="T162" id="Seg_3762" n="e" s="T161">Muktuʔ </ts>
               <ts e="T163" id="Seg_3764" n="e" s="T162">bar </ts>
               <ts e="T164" id="Seg_3766" n="e" s="T163">dʼăbaktərbibaʔ. </ts>
               <ts e="T165" id="Seg_3768" n="e" s="T164">Miʔ </ts>
               <ts e="T166" id="Seg_3770" n="e" s="T165">bar </ts>
               <ts e="T167" id="Seg_3772" n="e" s="T166">üdʼüge </ts>
               <ts e="T168" id="Seg_3774" n="e" s="T167">ibibeʔ, </ts>
               <ts e="T169" id="Seg_3776" n="e" s="T168">bar </ts>
               <ts e="T170" id="Seg_3778" n="e" s="T169">sʼarlaʔpibaʔ. </ts>
               <ts e="T171" id="Seg_3780" n="e" s="T170">Paʔi </ts>
               <ts e="T172" id="Seg_3782" n="e" s="T171">iləj </ts>
               <ts e="T173" id="Seg_3784" n="e" s="T172">da </ts>
               <ts e="T174" id="Seg_3786" n="e" s="T173">(bar </ts>
               <ts e="T175" id="Seg_3788" n="e" s="T174">oj) </ts>
               <ts e="T176" id="Seg_3790" n="e" s="T175">onʼiʔ </ts>
               <ts e="T177" id="Seg_3792" n="e" s="T176">nuʔmələj. </ts>
               <ts e="T178" id="Seg_3794" n="e" s="T177">A </ts>
               <ts e="T179" id="Seg_3796" n="e" s="T178">miʔ </ts>
               <ts e="T180" id="Seg_3798" n="e" s="T179">bar </ts>
               <ts e="T181" id="Seg_3800" n="e" s="T180">šalaːmbibeʔ. </ts>
               <ts e="T182" id="Seg_3802" n="e" s="T181">Dĭgəttə </ts>
               <ts e="T183" id="Seg_3804" n="e" s="T182">dĭ </ts>
               <ts e="T184" id="Seg_3806" n="e" s="T183">bar </ts>
               <ts e="T185" id="Seg_3808" n="e" s="T184">kulia, </ts>
               <ts e="T186" id="Seg_3810" n="e" s="T185">kulia, </ts>
               <ts e="T187" id="Seg_3812" n="e" s="T186">(ej </ts>
               <ts e="T188" id="Seg_3814" n="e" s="T187">mozi-) </ts>
               <ts e="T189" id="Seg_3816" n="e" s="T188">ej </ts>
               <ts e="T190" id="Seg_3818" n="e" s="T189">molia </ts>
               <ts e="T191" id="Seg_3820" n="e" s="T190">kuzittə. </ts>
               <ts e="T192" id="Seg_3822" n="e" s="T191">Miʔ </ts>
               <ts e="T193" id="Seg_3824" n="e" s="T192">bar </ts>
               <ts e="T194" id="Seg_3826" n="e" s="T193">bazoʔ </ts>
               <ts e="T195" id="Seg_3828" n="e" s="T194">sarlaʔbəbaʔ. </ts>
               <ts e="T196" id="Seg_3830" n="e" s="T195">(M-) </ts>
               <ts e="T197" id="Seg_3832" n="e" s="T196">Miʔ </ts>
               <ts e="T198" id="Seg_3834" n="e" s="T197">bar </ts>
               <ts e="T199" id="Seg_3836" n="e" s="T198">ej </ts>
               <ts e="T200" id="Seg_3838" n="e" s="T199">kudonzluʔpibaʔ. </ts>
               <ts e="T201" id="Seg_3840" n="e" s="T200">Jakšə </ts>
               <ts e="T203" id="Seg_3842" n="e" s="T201">bar… </ts>
            </ts>
            <ts e="T415" id="Seg_3843" n="sc" s="T204">
               <ts e="T205" id="Seg_3845" n="e" s="T204">Jakšə </ts>
               <ts e="T207" id="Seg_3847" n="e" s="T205">bar… </ts>
               <ts e="T208" id="Seg_3849" n="e" s="T207">(Пока </ts>
               <ts e="T209" id="Seg_3851" n="e" s="T208">закрой). </ts>
               <ts e="T210" id="Seg_3853" n="e" s="T209">Miʔ </ts>
               <ts e="T211" id="Seg_3855" n="e" s="T210">bar </ts>
               <ts e="T212" id="Seg_3857" n="e" s="T211">jakšə </ts>
               <ts e="T213" id="Seg_3859" n="e" s="T212">sʼarbibaʔ. </ts>
               <ts e="T214" id="Seg_3861" n="e" s="T213">Miʔ </ts>
               <ts e="T215" id="Seg_3863" n="e" s="T214">ej </ts>
               <ts e="T216" id="Seg_3865" n="e" s="T215">(kurollubibaʔ). </ts>
               <ts e="T217" id="Seg_3867" n="e" s="T216">Tibizeŋ </ts>
               <ts e="T218" id="Seg_3869" n="e" s="T217">i </ts>
               <ts e="T219" id="Seg_3871" n="e" s="T218">nezeŋ </ts>
               <ts e="T220" id="Seg_3873" n="e" s="T219">amnobiʔi. </ts>
               <ts e="T221" id="Seg_3875" n="e" s="T220">Da </ts>
               <ts e="T222" id="Seg_3877" n="e" s="T221">măndolaʔpiʔi, </ts>
               <ts e="T223" id="Seg_3879" n="e" s="T222">kăde </ts>
               <ts e="T224" id="Seg_3881" n="e" s="T223">miʔ </ts>
               <ts e="T225" id="Seg_3883" n="e" s="T224">sʼarlaʔbəbaʔ. </ts>
               <ts e="T226" id="Seg_3885" n="e" s="T225">Šindi </ts>
               <ts e="T227" id="Seg_3887" n="e" s="T226">(n- </ts>
               <ts e="T228" id="Seg_3889" n="e" s="T227">nʼe-) </ts>
               <ts e="T229" id="Seg_3891" n="e" s="T228">nʼeʔtəbi </ts>
               <ts e="T230" id="Seg_3893" n="e" s="T229">taŋgo, </ts>
               <ts e="T231" id="Seg_3895" n="e" s="T230">šindi </ts>
               <ts e="T232" id="Seg_3897" n="e" s="T231">ej </ts>
               <ts e="T233" id="Seg_3899" n="e" s="T232">nʼeʔtəbi. </ts>
               <ts e="T234" id="Seg_3901" n="e" s="T233">Šindi </ts>
               <ts e="T235" id="Seg_3903" n="e" s="T234">bar </ts>
               <ts e="T236" id="Seg_3905" n="e" s="T235">(kaŋ-) </ts>
               <ts e="T237" id="Seg_3907" n="e" s="T236">kanzasi </ts>
               <ts e="T238" id="Seg_3909" n="e" s="T237">bar </ts>
               <ts e="T239" id="Seg_3911" n="e" s="T238">nʼeʔleʔbə, </ts>
               <ts e="T240" id="Seg_3913" n="e" s="T239">šindi </ts>
               <ts e="T241" id="Seg_3915" n="e" s="T240">ej </ts>
               <ts e="T242" id="Seg_3917" n="e" s="T241">nʼeʔleʔbə. </ts>
               <ts e="T243" id="Seg_3919" n="e" s="T242">Ular </ts>
               <ts e="T244" id="Seg_3921" n="e" s="T243">ugaːndə </ts>
               <ts e="T245" id="Seg_3923" n="e" s="T244">iʔgö, </ts>
               <ts e="T246" id="Seg_3925" n="e" s="T245">однако </ts>
               <ts e="T247" id="Seg_3927" n="e" s="T246">šide </ts>
               <ts e="T248" id="Seg_3929" n="e" s="T247">bit </ts>
               <ts e="T249" id="Seg_3931" n="e" s="T248">šide. </ts>
               <ts e="T250" id="Seg_3933" n="e" s="T249">Abam </ts>
               <ts e="T251" id="Seg_3935" n="e" s="T250">dʼijenə </ts>
               <ts e="T252" id="Seg_3937" n="e" s="T251">kambi. </ts>
               <ts e="T253" id="Seg_3939" n="e" s="T252">Iʔgö </ts>
               <ts e="T254" id="Seg_3941" n="e" s="T253">kurizəʔi </ts>
               <ts e="T255" id="Seg_3943" n="e" s="T254">kuʔpi. </ts>
               <ts e="T256" id="Seg_3945" n="e" s="T255">I </ts>
               <ts e="T257" id="Seg_3947" n="e" s="T256">maːndə </ts>
               <ts e="T258" id="Seg_3949" n="e" s="T257">deʔpi. </ts>
               <ts e="T259" id="Seg_3951" n="e" s="T258">Măn </ts>
               <ts e="T260" id="Seg_3953" n="e" s="T259">šonəgam </ts>
               <ts e="T261" id="Seg_3955" n="e" s="T260">Permʼakovo. </ts>
               <ts e="T262" id="Seg_3957" n="e" s="T261">Kuliom: </ts>
               <ts e="T263" id="Seg_3959" n="e" s="T262">bar </ts>
               <ts e="T264" id="Seg_3961" n="e" s="T263">bulan </ts>
               <ts e="T265" id="Seg_3963" n="e" s="T264">nuga. </ts>
               <ts e="T266" id="Seg_3965" n="e" s="T265">Măn </ts>
               <ts e="T267" id="Seg_3967" n="e" s="T266">măndobiam, </ts>
               <ts e="T268" id="Seg_3969" n="e" s="T267">măndobiam, </ts>
               <ts e="T269" id="Seg_3971" n="e" s="T268">dĭgəttə </ts>
               <ts e="T270" id="Seg_3973" n="e" s="T269">bar </ts>
               <ts e="T271" id="Seg_3975" n="e" s="T270">kirgarluʔpim. </ts>
               <ts e="T272" id="Seg_3977" n="e" s="T271">Dĭ </ts>
               <ts e="T273" id="Seg_3979" n="e" s="T272">bar </ts>
               <ts e="T274" id="Seg_3981" n="e" s="T273">nuʔməluʔpi </ts>
               <ts e="T275" id="Seg_3983" n="e" s="T274">bünə. </ts>
               <ts e="T276" id="Seg_3985" n="e" s="T275">(Penzi). </ts>
               <ts e="T277" id="Seg_3987" n="e" s="T276">Dĭgəttə </ts>
               <ts e="T278" id="Seg_3989" n="e" s="T277">dĭ </ts>
               <ts e="T279" id="Seg_3991" n="e" s="T278">(bar </ts>
               <ts e="T280" id="Seg_3993" n="e" s="T279">kaluʔpi) </ts>
               <ts e="T281" id="Seg_3995" n="e" s="T280">i </ts>
               <ts e="T282" id="Seg_3997" n="e" s="T281">nuʔməluʔpi. </ts>
               <ts e="T283" id="Seg_3999" n="e" s="T282">Măn </ts>
               <ts e="T284" id="Seg_4001" n="e" s="T283">šobiam </ts>
               <ts e="T285" id="Seg_4003" n="e" s="T284">da, </ts>
               <ts e="T286" id="Seg_4005" n="e" s="T285">Vlastə </ts>
               <ts e="T287" id="Seg_4007" n="e" s="T286">nörbəbiem. </ts>
               <ts e="T288" id="Seg_4009" n="e" s="T287">Dĭ </ts>
               <ts e="T289" id="Seg_4011" n="e" s="T288">kambi, </ts>
               <ts e="T290" id="Seg_4013" n="e" s="T289">multuksi </ts>
               <ts e="T291" id="Seg_4015" n="e" s="T290">da </ts>
               <ts e="T292" id="Seg_4017" n="e" s="T291">(kutl-) </ts>
               <ts e="T293" id="Seg_4019" n="e" s="T292">kuʔpi. </ts>
               <ts e="T295" id="Seg_4021" n="e" s="T293">aspaʔ. </ts>
               <ts e="T296" id="Seg_4023" n="e" s="T295">Măn </ts>
               <ts e="T297" id="Seg_4025" n="e" s="T296">dĭgəttə </ts>
               <ts e="T298" id="Seg_4027" n="e" s="T297">uja </ts>
               <ts e="T299" id="Seg_4029" n="e" s="T298">băzəbiam. </ts>
               <ts e="T300" id="Seg_4031" n="e" s="T299">I </ts>
               <ts e="T301" id="Seg_4033" n="e" s="T300">ambiam. </ts>
               <ts e="T302" id="Seg_4035" n="e" s="T301">Miʔnʼibeʔ </ts>
               <ts e="T303" id="Seg_4037" n="e" s="T302">mĭbi </ts>
               <ts e="T304" id="Seg_4039" n="e" s="T303">onʼiʔ </ts>
               <ts e="T305" id="Seg_4041" n="e" s="T304">aspaʔ. </ts>
               <ts e="T866" id="Seg_4043" n="e" s="T305">Финн </ts>
               <ts e="T306" id="Seg_4045" n="e" s="T866">kuza </ts>
               <ts e="T307" id="Seg_4047" n="e" s="T306">šobi, </ts>
               <ts e="T308" id="Seg_4049" n="e" s="T307">măn </ts>
               <ts e="T309" id="Seg_4051" n="e" s="T308">dibər </ts>
               <ts e="T310" id="Seg_4053" n="e" s="T309">mĭmbiem, </ts>
               <ts e="T311" id="Seg_4055" n="e" s="T310">dĭzeŋ </ts>
               <ts e="T312" id="Seg_4057" n="e" s="T311">bar </ts>
               <ts e="T314" id="Seg_4059" n="e" s="T312">dʼăbaktərlaʔbəʔjə. </ts>
               <ts e="T315" id="Seg_4061" n="e" s="T314">A </ts>
               <ts e="T316" id="Seg_4063" n="e" s="T315">dĭzeŋ </ts>
               <ts e="T317" id="Seg_4065" n="e" s="T316">bar </ts>
               <ts e="T318" id="Seg_4067" n="e" s="T317">măna </ts>
               <ts e="T319" id="Seg_4069" n="e" s="T318">sürerluʔpiʔjə, </ts>
               <ts e="T320" id="Seg_4071" n="e" s="T319">măn </ts>
               <ts e="T321" id="Seg_4073" n="e" s="T320">nuʔməluʔpiem. </ts>
               <ts e="T323" id="Seg_4075" n="e" s="T321"> unnʼa </ts>
               <ts e="T324" id="Seg_4077" n="e" s="T323">nuzaŋ </ts>
               <ts e="T325" id="Seg_4079" n="e" s="T324">amnobiʔi, </ts>
               <ts e="T326" id="Seg_4081" n="e" s="T325">jakšə </ts>
               <ts e="T327" id="Seg_4083" n="e" s="T326">ibi, </ts>
               <ts e="T328" id="Seg_4085" n="e" s="T327">ujat </ts>
               <ts e="T329" id="Seg_4087" n="e" s="T328">iʔgö </ts>
               <ts e="T330" id="Seg_4089" n="e" s="T329">ibi, </ts>
               <ts e="T331" id="Seg_4091" n="e" s="T330">bar </ts>
               <ts e="T332" id="Seg_4093" n="e" s="T331">amnial. </ts>
               <ts e="T333" id="Seg_4095" n="e" s="T332">Ălenʼən </ts>
               <ts e="T334" id="Seg_4097" n="e" s="T333">uja </ts>
               <ts e="T335" id="Seg_4099" n="e" s="T334">ugandə </ts>
               <ts e="T336" id="Seg_4101" n="e" s="T335">nömər, </ts>
               <ts e="T337" id="Seg_4103" n="e" s="T336">kak </ts>
               <ts e="T338" id="Seg_4105" n="e" s="T337">ipek. </ts>
               <ts e="T339" id="Seg_4107" n="e" s="T338">Iʔgö </ts>
               <ts e="T340" id="Seg_4109" n="e" s="T339">amnial, </ts>
               <ts e="T341" id="Seg_4111" n="e" s="T340">dĭgəttə </ts>
               <ts e="T342" id="Seg_4113" n="e" s="T341">bü </ts>
               <ts e="T343" id="Seg_4115" n="e" s="T342">bĭtliel. </ts>
               <ts e="T865" id="Seg_4117" n="e" s="T343">((…)) </ts>
               <ts e="T344" id="Seg_4119" n="e" s="T865">bü </ts>
               <ts e="T345" id="Seg_4121" n="e" s="T344">bĭtliel, </ts>
               <ts e="T348" id="Seg_4123" n="e" s="T345">nu… </ts>
               <ts e="T349" id="Seg_4125" n="e" s="T348">Dĭ </ts>
               <ts e="T350" id="Seg_4127" n="e" s="T349">koŋ </ts>
               <ts e="T351" id="Seg_4129" n="e" s="T350">ugandə </ts>
               <ts e="T352" id="Seg_4131" n="e" s="T351">jakšə, </ts>
               <ts e="T353" id="Seg_4133" n="e" s="T352">ugandə </ts>
               <ts e="T354" id="Seg_4135" n="e" s="T353">kuvas, </ts>
               <ts e="T355" id="Seg_4137" n="e" s="T354">(sʼimat) </ts>
               <ts e="T356" id="Seg_4139" n="e" s="T355">(tʼi-) </ts>
               <ts e="T357" id="Seg_4141" n="e" s="T356">sagər. </ts>
               <ts e="T358" id="Seg_4143" n="e" s="T357">Püjet </ts>
               <ts e="T359" id="Seg_4145" n="e" s="T358">ej </ts>
               <ts e="T360" id="Seg_4147" n="e" s="T359">numo. </ts>
               <ts e="T361" id="Seg_4149" n="e" s="T360">Monzaŋdə </ts>
               <ts e="T362" id="Seg_4151" n="e" s="T361">kuvas </ts>
               <ts e="T363" id="Seg_4153" n="e" s="T362">bar. </ts>
               <ts e="T364" id="Seg_4155" n="e" s="T363">Monzaŋdə </ts>
               <ts e="T365" id="Seg_4157" n="e" s="T364">ej </ts>
               <ts e="T366" id="Seg_4159" n="e" s="T365">nʼešpək. </ts>
               <ts e="T367" id="Seg_4161" n="e" s="T366">Матрена </ts>
               <ts e="T368" id="Seg_4163" n="e" s="T367">bar </ts>
               <ts e="T369" id="Seg_4165" n="e" s="T368">ĭzemneʔpi. </ts>
               <ts e="T370" id="Seg_4167" n="e" s="T369">Măn </ts>
               <ts e="T371" id="Seg_4169" n="e" s="T370">(dĭʔ-) </ts>
               <ts e="T372" id="Seg_4171" n="e" s="T371">dĭʔnə </ts>
               <ts e="T373" id="Seg_4173" n="e" s="T372">(kal-) </ts>
               <ts e="T374" id="Seg_4175" n="e" s="T373">kalam. </ts>
               <ts e="T375" id="Seg_4177" n="e" s="T374">Dĭ </ts>
               <ts e="T376" id="Seg_4179" n="e" s="T375">bar </ts>
               <ts e="T377" id="Seg_4181" n="e" s="T376">măndə: </ts>
               <ts e="T378" id="Seg_4183" n="e" s="T377">Iʔ </ts>
               <ts e="T379" id="Seg_4185" n="e" s="T378">dʼăbaktəraʔ </ts>
               <ts e="T380" id="Seg_4187" n="e" s="T379">kazan </ts>
               <ts e="T381" id="Seg_4189" n="e" s="T380">šĭkətsi, </ts>
               <ts e="T382" id="Seg_4191" n="e" s="T381">a </ts>
               <ts e="T383" id="Seg_4193" n="e" s="T382">bostə </ts>
               <ts e="T384" id="Seg_4195" n="e" s="T383">šĭkətsi </ts>
               <ts e="T385" id="Seg_4197" n="e" s="T384">dʼăbaktəraʔ. </ts>
               <ts e="T386" id="Seg_4199" n="e" s="T385">Măn </ts>
               <ts e="T387" id="Seg_4201" n="e" s="T386">ugandə </ts>
               <ts e="T388" id="Seg_4203" n="e" s="T387">ĭzemniem </ts>
               <ts e="T389" id="Seg_4205" n="e" s="T388">bar. </ts>
               <ts e="T390" id="Seg_4207" n="e" s="T389">Ugandə </ts>
               <ts e="T391" id="Seg_4209" n="e" s="T390">ĭzemniem </ts>
               <ts e="T392" id="Seg_4211" n="e" s="T391">bar. </ts>
               <ts e="T393" id="Seg_4213" n="e" s="T392">Măn </ts>
               <ts e="T394" id="Seg_4215" n="e" s="T393">bar </ts>
               <ts e="T395" id="Seg_4217" n="e" s="T394">külaːmbiam, </ts>
               <ts e="T396" id="Seg_4219" n="e" s="T395">(külaːmbiam), </ts>
               <ts e="T397" id="Seg_4221" n="e" s="T396">a </ts>
               <ts e="T398" id="Seg_4223" n="e" s="T397">tăn </ts>
               <ts e="T399" id="Seg_4225" n="e" s="T398">maləl. </ts>
               <ts e="T400" id="Seg_4227" n="e" s="T399">Dĭ </ts>
               <ts e="T401" id="Seg_4229" n="e" s="T400">külaːmbi, </ts>
               <ts e="T402" id="Seg_4231" n="e" s="T401">šide </ts>
               <ts e="T403" id="Seg_4233" n="e" s="T402">koʔbdot </ts>
               <ts e="T404" id="Seg_4235" n="e" s="T403">(maː-) </ts>
               <ts e="T405" id="Seg_4237" n="e" s="T404">maluʔpiʔi. </ts>
               <ts e="T406" id="Seg_4239" n="e" s="T405">Onʼiʔ </ts>
               <ts e="T407" id="Seg_4241" n="e" s="T406">dĭzi </ts>
               <ts e="T408" id="Seg_4243" n="e" s="T407">ibi, </ts>
               <ts e="T409" id="Seg_4245" n="e" s="T408">a </ts>
               <ts e="T410" id="Seg_4247" n="e" s="T409">onʼiʔ </ts>
               <ts e="T411" id="Seg_4249" n="e" s="T410">kuŋgə </ts>
               <ts e="T412" id="Seg_4251" n="e" s="T411">ibi. </ts>
               <ts e="T413" id="Seg_4253" n="e" s="T412">Teinen </ts>
               <ts e="T415" id="Seg_4255" n="e" s="T413">bar… </ts>
            </ts>
            <ts e="T856" id="Seg_4256" n="sc" s="T419">
               <ts e="T420" id="Seg_4258" n="e" s="T419">Teinen </ts>
               <ts e="T421" id="Seg_4260" n="e" s="T420">selɨj </ts>
               <ts e="T422" id="Seg_4262" n="e" s="T421">dʼalat </ts>
               <ts e="T423" id="Seg_4264" n="e" s="T422">surno </ts>
               <ts e="T424" id="Seg_4266" n="e" s="T423">šobi. </ts>
               <ts e="T425" id="Seg_4268" n="e" s="T424">A </ts>
               <ts e="T426" id="Seg_4270" n="e" s="T425">tüj </ts>
               <ts e="T427" id="Seg_4272" n="e" s="T426">kujo </ts>
               <ts e="T428" id="Seg_4274" n="e" s="T427">măndolaʔbə. </ts>
               <ts e="T429" id="Seg_4276" n="e" s="T428">Novik </ts>
               <ts e="T430" id="Seg_4278" n="e" s="T429">šonəga </ts>
               <ts e="T431" id="Seg_4280" n="e" s="T430">šoškandə, </ts>
               <ts e="T432" id="Seg_4282" n="e" s="T431">a </ts>
               <ts e="T433" id="Seg_4284" n="e" s="T432">Sidor </ts>
               <ts e="T434" id="Seg_4286" n="e" s="T433">mendə </ts>
               <ts e="T435" id="Seg_4288" n="e" s="T434">amnolaʔbə. </ts>
               <ts e="T436" id="Seg_4290" n="e" s="T435">A </ts>
               <ts e="T437" id="Seg_4292" n="e" s="T436">il </ts>
               <ts e="T438" id="Seg_4294" n="e" s="T437">kubiʔi, </ts>
               <ts e="T439" id="Seg_4296" n="e" s="T438">tenəbiʔi: </ts>
               <ts e="T440" id="Seg_4298" n="e" s="T439">казаки. </ts>
               <ts e="T441" id="Seg_4300" n="e" s="T440">Predsʼedatelʼ </ts>
               <ts e="T442" id="Seg_4302" n="e" s="T441">mămbi: </ts>
               <ts e="T443" id="Seg_4304" n="e" s="T442">Nada </ts>
               <ts e="T444" id="Seg_4306" n="e" s="T443">tʼerməndə </ts>
               <ts e="T445" id="Seg_4308" n="e" s="T444">(adʼazirzittə). </ts>
               <ts e="T446" id="Seg_4310" n="e" s="T445">Jakšə </ts>
               <ts e="T447" id="Seg_4312" n="e" s="T446">il </ts>
               <ts e="T448" id="Seg_4314" n="e" s="T447">oʔbdəsʼtə, </ts>
               <ts e="T449" id="Seg_4316" n="e" s="T448">(pusk-) </ts>
               <ts e="T450" id="Seg_4318" n="e" s="T449">(tog-) </ts>
               <ts e="T451" id="Seg_4320" n="e" s="T450">puskaj </ts>
               <ts e="T452" id="Seg_4322" n="e" s="T451">dĭn </ts>
               <ts e="T453" id="Seg_4324" n="e" s="T452">togonorlaʔi. </ts>
               <ts e="T454" id="Seg_4326" n="e" s="T453">Nada </ts>
               <ts e="T455" id="Seg_4328" n="e" s="T454">tʼerməndə </ts>
               <ts e="T456" id="Seg_4330" n="e" s="T455">kanzittə, </ts>
               <ts e="T457" id="Seg_4332" n="e" s="T456">un </ts>
               <ts e="T458" id="Seg_4334" n="e" s="T457">nʼeʔsittə, </ts>
               <ts e="T459" id="Seg_4336" n="e" s="T458">a_to </ts>
               <ts e="T460" id="Seg_4338" n="e" s="T459">un </ts>
               <ts e="T461" id="Seg_4340" n="e" s="T460">naga. </ts>
               <ts e="T462" id="Seg_4342" n="e" s="T461">(Kob-) </ts>
               <ts e="T463" id="Seg_4344" n="e" s="T462">Măn </ts>
               <ts e="T464" id="Seg_4346" n="e" s="T463">üdʼüge </ts>
               <ts e="T465" id="Seg_4348" n="e" s="T464">ibiem, </ts>
               <ts e="T466" id="Seg_4350" n="e" s="T465">a </ts>
               <ts e="T467" id="Seg_4352" n="e" s="T466">koʔbsam </ts>
               <ts e="T468" id="Seg_4354" n="e" s="T467">urgo </ts>
               <ts e="T469" id="Seg_4356" n="e" s="T468">ibiʔi, </ts>
               <ts e="T470" id="Seg_4358" n="e" s="T469">uge </ts>
               <ts e="T471" id="Seg_4360" n="e" s="T470">măna </ts>
               <ts e="T472" id="Seg_4362" n="e" s="T471">ibiʔi </ts>
               <ts e="T473" id="Seg_4364" n="e" s="T472">boskəndə. </ts>
               <ts e="T474" id="Seg_4366" n="e" s="T473">Nüjnə </ts>
               <ts e="T475" id="Seg_4368" n="e" s="T474">(nüjnəsʼtə), </ts>
               <ts e="T476" id="Seg_4370" n="e" s="T475">(măna) </ts>
               <ts e="T477" id="Seg_4372" n="e" s="T476">голос </ts>
               <ts e="T478" id="Seg_4374" n="e" s="T477">- </ts>
               <ts e="T479" id="Seg_4376" n="e" s="T478">то </ts>
               <ts e="T480" id="Seg_4378" n="e" s="T479">kuvas </ts>
               <ts e="T481" id="Seg_4380" n="e" s="T480">ibi. </ts>
               <ts e="T482" id="Seg_4382" n="e" s="T481">(Nüjnəbiem) </ts>
               <ts e="T483" id="Seg_4384" n="e" s="T482">bar. </ts>
               <ts e="T484" id="Seg_4386" n="e" s="T483">Dĭ </ts>
               <ts e="T485" id="Seg_4388" n="e" s="T484">kuza </ts>
               <ts e="T486" id="Seg_4390" n="e" s="T485">ugandə </ts>
               <ts e="T487" id="Seg_4392" n="e" s="T486">jakšə </ts>
               <ts e="T488" id="Seg_4394" n="e" s="T487">nüjlie. </ts>
               <ts e="T491" id="Seg_4396" n="e" s="T488">Голос-то </ts>
               <ts e="T492" id="Seg_4398" n="e" s="T491">ugandə </ts>
               <ts e="T493" id="Seg_4400" n="e" s="T492">kuvas. </ts>
               <ts e="T500" id="Seg_4402" n="e" s="T493">Голос как… Нет, не так… </ts>
               <ts e="T501" id="Seg_4404" n="e" s="T500">Ugandə </ts>
               <ts e="T502" id="Seg_4406" n="e" s="T501">tăŋ </ts>
               <ts e="T503" id="Seg_4408" n="e" s="T502">nüjleʔbə. </ts>
               <ts e="T504" id="Seg_4410" n="e" s="T503">Dĭ </ts>
               <ts e="T505" id="Seg_4412" n="e" s="T504">kuza </ts>
               <ts e="T506" id="Seg_4414" n="e" s="T505">külaːmbi, </ts>
               <ts e="T507" id="Seg_4416" n="e" s="T506">nada </ts>
               <ts e="T508" id="Seg_4418" n="e" s="T507">maʔtə </ts>
               <ts e="T509" id="Seg_4420" n="e" s="T508">azittə, </ts>
               <ts e="T510" id="Seg_4422" n="e" s="T509">(pa= </ts>
               <ts e="T511" id="Seg_4424" n="e" s="T510">pazi=) </ts>
               <ts e="T512" id="Seg_4426" n="e" s="T511">pagə. </ts>
               <ts e="T513" id="Seg_4428" n="e" s="T512">I </ts>
               <ts e="T514" id="Seg_4430" n="e" s="T513">dibər </ts>
               <ts e="T515" id="Seg_4432" n="e" s="T514">dĭm </ts>
               <ts e="T516" id="Seg_4434" n="e" s="T515">enzittə. </ts>
               <ts e="T517" id="Seg_4436" n="e" s="T516">I </ts>
               <ts e="T518" id="Seg_4438" n="e" s="T517">kros </ts>
               <ts e="T519" id="Seg_4440" n="e" s="T518">azittə. </ts>
               <ts e="T520" id="Seg_4442" n="e" s="T519">Dĭn </ts>
               <ts e="T521" id="Seg_4444" n="e" s="T520">tüj </ts>
               <ts e="T522" id="Seg_4446" n="e" s="T521">kaləj </ts>
               <ts e="T523" id="Seg_4448" n="e" s="T522">(šü </ts>
               <ts e="T524" id="Seg_4450" n="e" s="T523">dʼünə). </ts>
               <ts e="T525" id="Seg_4452" n="e" s="T524">Ugandə </ts>
               <ts e="T526" id="Seg_4454" n="e" s="T525">kuvas </ts>
               <ts e="T527" id="Seg_4456" n="e" s="T526">turaʔi </ts>
               <ts e="T528" id="Seg_4458" n="e" s="T527">nugaʔi. </ts>
               <ts e="T529" id="Seg_4460" n="e" s="T528">(Dĭzeŋ=) </ts>
               <ts e="T530" id="Seg_4462" n="e" s="T529">Dĭzeŋ </ts>
               <ts e="T531" id="Seg_4464" n="e" s="T530">(pi- </ts>
               <ts e="T532" id="Seg_4466" n="e" s="T531">pi-) </ts>
               <ts e="T533" id="Seg_4468" n="e" s="T532">piʔi </ts>
               <ts e="T534" id="Seg_4470" n="e" s="T533">(am-) </ts>
               <ts e="T535" id="Seg_4472" n="e" s="T534">embiʔi. </ts>
               <ts e="T536" id="Seg_4474" n="e" s="T535">(Kuznek=) </ts>
               <ts e="T537" id="Seg_4476" n="e" s="T536">Kuznekən </ts>
               <ts e="T538" id="Seg_4478" n="e" s="T537">tondə </ts>
               <ts e="T539" id="Seg_4480" n="e" s="T538">bar </ts>
               <ts e="T540" id="Seg_4482" n="e" s="T539">paʔi </ts>
               <ts e="T541" id="Seg_4484" n="e" s="T540">(nuga-) </ts>
               <ts e="T542" id="Seg_4486" n="e" s="T541">amnolbiʔi. </ts>
               <ts e="T543" id="Seg_4488" n="e" s="T542">Paʔi </ts>
               <ts e="T544" id="Seg_4490" n="e" s="T543">amnolbiʔi. </ts>
               <ts e="T545" id="Seg_4492" n="e" s="T544">Малина </ts>
               <ts e="T546" id="Seg_4494" n="e" s="T545">amnolbiʔi. </ts>
               <ts e="T547" id="Seg_4496" n="e" s="T546">Ugandə </ts>
               <ts e="T548" id="Seg_4498" n="e" s="T547">kuvas </ts>
               <ts e="T549" id="Seg_4500" n="e" s="T548">tura </ts>
               <ts e="T550" id="Seg_4502" n="e" s="T549">(nuga-) </ts>
               <ts e="T551" id="Seg_4504" n="e" s="T550">nugaʔbəʔjə. </ts>
               <ts e="T552" id="Seg_4506" n="e" s="T551">Lem </ts>
               <ts e="T553" id="Seg_4508" n="e" s="T552">keʔbde </ts>
               <ts e="T554" id="Seg_4510" n="e" s="T553">bar </ts>
               <ts e="T555" id="Seg_4512" n="e" s="T554">amnolbiʔi. </ts>
               <ts e="T867" id="Seg_4514" n="e" s="T555">Lʼevăj </ts>
               <ts e="T556" id="Seg_4516" n="e" s="T867">(uga-) </ts>
               <ts e="T557" id="Seg_4518" n="e" s="T556">udanə </ts>
               <ts e="T558" id="Seg_4520" n="e" s="T557">kuvas </ts>
               <ts e="T559" id="Seg_4522" n="e" s="T558">i </ts>
               <ts e="T560" id="Seg_4524" n="e" s="T559">pravăj </ts>
               <ts e="T561" id="Seg_4526" n="e" s="T560">udanə </ts>
               <ts e="T563" id="Seg_4528" n="e" s="T561">kuvas. </ts>
               <ts e="T566" id="Seg_4530" n="e" s="T563">По левую сторону. </ts>
               <ts e="T567" id="Seg_4532" n="e" s="T566">Ugandə </ts>
               <ts e="T568" id="Seg_4534" n="e" s="T567">kuvas </ts>
               <ts e="T569" id="Seg_4536" n="e" s="T568">(nura-) </ts>
               <ts e="T570" id="Seg_4538" n="e" s="T569">turaʔi </ts>
               <ts e="T571" id="Seg_4540" n="e" s="T570">nugaʔi. </ts>
               <ts e="T572" id="Seg_4542" n="e" s="T571">Măn </ts>
               <ts e="T573" id="Seg_4544" n="e" s="T572">mejəm </ts>
               <ts e="T574" id="Seg_4546" n="e" s="T573">самогон </ts>
               <ts e="T575" id="Seg_4548" n="e" s="T574">mĭnzərbi. </ts>
               <ts e="T576" id="Seg_4550" n="e" s="T575">Dĭ </ts>
               <ts e="T577" id="Seg_4552" n="e" s="T576">bar </ts>
               <ts e="T578" id="Seg_4554" n="e" s="T577">ej </ts>
               <ts e="T579" id="Seg_4556" n="e" s="T578">nʼeʔdə. </ts>
               <ts e="T580" id="Seg_4558" n="e" s="T579">Kuza </ts>
               <ts e="T581" id="Seg_4560" n="e" s="T580">bar </ts>
               <ts e="T582" id="Seg_4562" n="e" s="T581">kudonzlaʔbə: </ts>
               <ts e="T583" id="Seg_4564" n="e" s="T582">Daška </ts>
               <ts e="T584" id="Seg_4566" n="e" s="T583">ej </ts>
               <ts e="T585" id="Seg_4568" n="e" s="T584">iləl </ts>
               <ts e="T586" id="Seg_4570" n="e" s="T585">mĭnzərzittə, </ts>
               <ts e="T587" id="Seg_4572" n="e" s="T586">ej </ts>
               <ts e="T588" id="Seg_4574" n="e" s="T587">jakšə, </ts>
               <ts e="T589" id="Seg_4576" n="e" s="T588">ej </ts>
               <ts e="T590" id="Seg_4578" n="e" s="T589">nʼeʔdə. </ts>
               <ts e="T591" id="Seg_4580" n="e" s="T590">Dĭgəttə </ts>
               <ts e="T592" id="Seg_4582" n="e" s="T591">dĭ </ts>
               <ts e="T593" id="Seg_4584" n="e" s="T592">mejəm </ts>
               <ts e="T594" id="Seg_4586" n="e" s="T593">(mum-) </ts>
               <ts e="T595" id="Seg_4588" n="e" s="T594">büm </ts>
               <ts e="T596" id="Seg_4590" n="e" s="T595">mĭnzərbi, </ts>
               <ts e="T597" id="Seg_4592" n="e" s="T596">tussi. </ts>
               <ts e="T598" id="Seg_4594" n="e" s="T597">Dĭgəttə </ts>
               <ts e="T599" id="Seg_4596" n="e" s="T598">(mʼa-) </ts>
               <ts e="T600" id="Seg_4598" n="e" s="T599">uja </ts>
               <ts e="T601" id="Seg_4600" n="e" s="T600">jaʔpi. </ts>
               <ts e="T602" id="Seg_4602" n="e" s="T601">I </ts>
               <ts e="T603" id="Seg_4604" n="e" s="T602">dibər </ts>
               <ts e="T604" id="Seg_4606" n="e" s="T603">endəge, </ts>
               <ts e="T605" id="Seg_4608" n="e" s="T604">štobɨ </ts>
               <ts e="T606" id="Seg_4610" n="e" s="T605">tustʼarlaʔbə. </ts>
               <ts e="T607" id="Seg_4612" n="e" s="T606">(Ejet </ts>
               <ts e="T608" id="Seg_4614" n="e" s="T607">ejen </ts>
               <ts e="T609" id="Seg_4616" n="e" s="T608">ne). </ts>
               <ts e="T610" id="Seg_4618" n="e" s="T609">Kanzittə </ts>
               <ts e="T611" id="Seg_4620" n="e" s="T610">никак </ts>
               <ts e="T612" id="Seg_4622" n="e" s="T611">нельзя, </ts>
               <ts e="T613" id="Seg_4624" n="e" s="T612">ugandə </ts>
               <ts e="T614" id="Seg_4626" n="e" s="T613">bü. </ts>
               <ts e="T615" id="Seg_4628" n="e" s="T614">Nada </ts>
               <ts e="T616" id="Seg_4630" n="e" s="T615">parluʔsittə. </ts>
               <ts e="T617" id="Seg_4632" n="e" s="T616">Ugandə </ts>
               <ts e="T618" id="Seg_4634" n="e" s="T617">kuŋgə </ts>
               <ts e="T619" id="Seg_4636" n="e" s="T618">idlia. </ts>
               <ts e="T868" id="Seg_4638" n="e" s="T619">Степь </ts>
               <ts e="T620" id="Seg_4640" n="e" s="T868">bar, </ts>
               <ts e="T621" id="Seg_4642" n="e" s="T620">paʔi </ts>
               <ts e="T622" id="Seg_4644" n="e" s="T621">naga, </ts>
               <ts e="T623" id="Seg_4646" n="e" s="T622">onʼiʔ </ts>
               <ts e="T624" id="Seg_4648" n="e" s="T623">da </ts>
               <ts e="T625" id="Seg_4650" n="e" s="T624">pa </ts>
               <ts e="T627" id="Seg_4652" n="e" s="T625">naga. </ts>
               <ts e="T628" id="Seg_4654" n="e" s="T627">Cтепью </ts>
               <ts e="T629" id="Seg_4656" n="e" s="T628">šonəgam, </ts>
               <ts e="T630" id="Seg_4658" n="e" s="T629">степь </ts>
               <ts e="T631" id="Seg_4660" n="e" s="T630">nüjleʔbəm. </ts>
               <ts e="T632" id="Seg_4662" n="e" s="T631">Dʼijenə </ts>
               <ts e="T633" id="Seg_4664" n="e" s="T632">kandəgam, </ts>
               <ts e="T634" id="Seg_4666" n="e" s="T633">dʼije </ts>
               <ts e="T635" id="Seg_4668" n="e" s="T634">(nüjle-) </ts>
               <ts e="T636" id="Seg_4670" n="e" s="T635">nüjleʔbəm. </ts>
               <ts e="T637" id="Seg_4672" n="e" s="T636">Bünə </ts>
               <ts e="T638" id="Seg_4674" n="e" s="T637">kandəgam, </ts>
               <ts e="T639" id="Seg_4676" n="e" s="T638">bü </ts>
               <ts e="T640" id="Seg_4678" n="e" s="T639">nüjleʔbəm. </ts>
               <ts e="T641" id="Seg_4680" n="e" s="T640">Pagən </ts>
               <ts e="T642" id="Seg_4682" n="e" s="T641">kandəgam, </ts>
               <ts e="T643" id="Seg_4684" n="e" s="T642">pa </ts>
               <ts e="T644" id="Seg_4686" n="e" s="T643">nüjleʔbəm. </ts>
               <ts e="T645" id="Seg_4688" n="e" s="T644">Măn </ts>
               <ts e="T646" id="Seg_4690" n="e" s="T645">abam </ts>
               <ts e="T647" id="Seg_4692" n="e" s="T646">Kirelʼdə </ts>
               <ts e="T648" id="Seg_4694" n="e" s="T647">kambi. </ts>
               <ts e="T649" id="Seg_4696" n="e" s="T648">Лодка </ts>
               <ts e="T650" id="Seg_4698" n="e" s="T649">ibi. </ts>
               <ts e="T651" id="Seg_4700" n="e" s="T650">Kola </ts>
               <ts e="T652" id="Seg_4702" n="e" s="T651">(u-) </ts>
               <ts e="T653" id="Seg_4704" n="e" s="T652">iʔgö </ts>
               <ts e="T654" id="Seg_4706" n="e" s="T653">dʼaʔpi. </ts>
               <ts e="T655" id="Seg_4708" n="e" s="T654">Miʔ </ts>
               <ts e="T656" id="Seg_4710" n="e" s="T655">arəmdəbibaʔ, </ts>
               <ts e="T657" id="Seg_4712" n="e" s="T656">i </ts>
               <ts e="T658" id="Seg_4714" n="e" s="T657">tustʼarbibaʔ, </ts>
               <ts e="T659" id="Seg_4716" n="e" s="T658">i </ts>
               <ts e="T660" id="Seg_4718" n="e" s="T659">(pürbibeʔ), </ts>
               <ts e="T661" id="Seg_4720" n="e" s="T660">i </ts>
               <ts e="T662" id="Seg_4722" n="e" s="T661">ambibaʔ. </ts>
               <ts e="T663" id="Seg_4724" n="e" s="T662">Miʔ </ts>
               <ts e="T664" id="Seg_4726" n="e" s="T663">(mĭmbieʔ), </ts>
               <ts e="T665" id="Seg_4728" n="e" s="T664">i </ts>
               <ts e="T666" id="Seg_4730" n="e" s="T665">ugandə </ts>
               <ts e="T667" id="Seg_4732" n="e" s="T666">urgo </ts>
               <ts e="T668" id="Seg_4734" n="e" s="T667">bü. </ts>
               <ts e="T669" id="Seg_4736" n="e" s="T668">Dĭn </ts>
               <ts e="T670" id="Seg_4738" n="e" s="T669">bar </ts>
               <ts e="T671" id="Seg_4740" n="e" s="T670">sedʼiʔi </ts>
               <ts e="T672" id="Seg_4742" n="e" s="T671">(barəʔpibiaʔ), </ts>
               <ts e="T673" id="Seg_4744" n="e" s="T672">kola </ts>
               <ts e="T674" id="Seg_4746" n="e" s="T673">dʼaʔpibaʔ. </ts>
               <ts e="T675" id="Seg_4748" n="e" s="T674">Kuza </ts>
               <ts e="T676" id="Seg_4750" n="e" s="T675">bar </ts>
               <ts e="T677" id="Seg_4752" n="e" s="T676">nem </ts>
               <ts e="T678" id="Seg_4754" n="e" s="T677">pʼaŋdəbi, </ts>
               <ts e="T679" id="Seg_4756" n="e" s="T678">a </ts>
               <ts e="T680" id="Seg_4758" n="e" s="T679">bostə </ts>
               <ts e="T681" id="Seg_4760" n="e" s="T680">panə </ts>
               <ts e="T682" id="Seg_4762" n="e" s="T681">edəluʔpi. </ts>
               <ts e="T683" id="Seg_4764" n="e" s="T682">(Urga=) </ts>
               <ts e="T684" id="Seg_4766" n="e" s="T683">Urgaːba </ts>
               <ts e="T685" id="Seg_4768" n="e" s="T684">ugandə </ts>
               <ts e="T686" id="Seg_4770" n="e" s="T685">tărdə </ts>
               <ts e="T687" id="Seg_4772" n="e" s="T686">iʔgö. </ts>
               <ts e="T688" id="Seg_4774" n="e" s="T687">Bar </ts>
               <ts e="T689" id="Seg_4776" n="e" s="T688">uja </ts>
               <ts e="T690" id="Seg_4778" n="e" s="T689">ej </ts>
               <ts e="T691" id="Seg_4780" n="e" s="T690">idlia. </ts>
               <ts e="T692" id="Seg_4782" n="e" s="T691">Ugandə </ts>
               <ts e="T693" id="Seg_4784" n="e" s="T692">dĭ </ts>
               <ts e="T694" id="Seg_4786" n="e" s="T693">nömər, </ts>
               <ts e="T695" id="Seg_4788" n="e" s="T694">ugandə </ts>
               <ts e="T696" id="Seg_4790" n="e" s="T695">kəbə. </ts>
               <ts e="T697" id="Seg_4792" n="e" s="T696">Ujut </ts>
               <ts e="T698" id="Seg_4794" n="e" s="T697">ugandə </ts>
               <ts e="T699" id="Seg_4796" n="e" s="T698">nʼeʔdə </ts>
               <ts e="T700" id="Seg_4798" n="e" s="T699">(nʼeʔdə=). </ts>
               <ts e="T701" id="Seg_4800" n="e" s="T700">Kadaʔi </ts>
               <ts e="T702" id="Seg_4802" n="e" s="T701">ugandə </ts>
               <ts e="T703" id="Seg_4804" n="e" s="T702">urgo. </ts>
               <ts e="T704" id="Seg_4806" n="e" s="T703">Urgo </ts>
               <ts e="T705" id="Seg_4808" n="e" s="T704">(men=) </ts>
               <ts e="T706" id="Seg_4810" n="e" s="T705">menzi. </ts>
               <ts e="T707" id="Seg_4812" n="e" s="T706">Urgo </ts>
               <ts e="T708" id="Seg_4814" n="e" s="T707">menzi. </ts>
               <ts e="T709" id="Seg_4816" n="e" s="T708">Sʼimat </ts>
               <ts e="T710" id="Seg_4818" n="e" s="T709">üdʼüge. </ts>
               <ts e="T711" id="Seg_4820" n="e" s="T710">Kuʔi </ts>
               <ts e="T712" id="Seg_4822" n="e" s="T711">üdʼüge. </ts>
               <ts e="T713" id="Seg_4824" n="e" s="T712">Măn </ts>
               <ts e="T714" id="Seg_4826" n="e" s="T713">tuganbə </ts>
               <ts e="T715" id="Seg_4828" n="e" s="T714">šobi. </ts>
               <ts e="T716" id="Seg_4830" n="e" s="T715">Nʼi </ts>
               <ts e="T717" id="Seg_4832" n="e" s="T716">šobi, </ts>
               <ts e="T718" id="Seg_4834" n="e" s="T717">nezi. </ts>
               <ts e="T719" id="Seg_4836" n="e" s="T718">Kagam </ts>
               <ts e="T720" id="Seg_4838" n="e" s="T719">šobi, </ts>
               <ts e="T721" id="Seg_4840" n="e" s="T720">nezi. </ts>
               <ts e="T723" id="Seg_4842" n="e" s="T721">Esseŋ… </ts>
               <ts e="T724" id="Seg_4844" n="e" s="T723">Măn </ts>
               <ts e="T725" id="Seg_4846" n="e" s="T724">tuganbə </ts>
               <ts e="T726" id="Seg_4848" n="e" s="T725">šobi. </ts>
               <ts e="T727" id="Seg_4850" n="e" s="T726">Nʼim </ts>
               <ts e="T728" id="Seg_4852" n="e" s="T727">šobi, </ts>
               <ts e="T729" id="Seg_4854" n="e" s="T728">nezi. </ts>
               <ts e="T730" id="Seg_4856" n="e" s="T729">Kagam </ts>
               <ts e="T731" id="Seg_4858" n="e" s="T730">šobi, </ts>
               <ts e="T732" id="Seg_4860" n="e" s="T731">nezi. </ts>
               <ts e="T733" id="Seg_4862" n="e" s="T732">Koʔbdom </ts>
               <ts e="T734" id="Seg_4864" n="e" s="T733">tibizi </ts>
               <ts e="T735" id="Seg_4866" n="e" s="T734">šobi. </ts>
               <ts e="T736" id="Seg_4868" n="e" s="T735">(Esseŋ=) </ts>
               <ts e="T737" id="Seg_4870" n="e" s="T736">Esseŋdə </ts>
               <ts e="T738" id="Seg_4872" n="e" s="T737">dĭzi </ts>
               <ts e="T739" id="Seg_4874" n="e" s="T738">šobiʔi. </ts>
               <ts e="T740" id="Seg_4876" n="e" s="T739">Nada </ts>
               <ts e="T741" id="Seg_4878" n="e" s="T740">dĭzeŋdə </ts>
               <ts e="T742" id="Seg_4880" n="e" s="T741">(obsittə-) </ts>
               <ts e="T743" id="Seg_4882" n="e" s="T742">oʔbdəsʼtə, </ts>
               <ts e="T744" id="Seg_4884" n="e" s="T743">dĭzeŋ </ts>
               <ts e="T745" id="Seg_4886" n="e" s="T744">(amorzi-) </ts>
               <ts e="T746" id="Seg_4888" n="e" s="T745">amzittə </ts>
               <ts e="T747" id="Seg_4890" n="e" s="T746">mĭzittə. </ts>
               <ts e="T748" id="Seg_4892" n="e" s="T747">Măn </ts>
               <ts e="T749" id="Seg_4894" n="e" s="T748">nʼim </ts>
               <ts e="T750" id="Seg_4896" n="e" s="T749">bar </ts>
               <ts e="T751" id="Seg_4898" n="e" s="T750">tăŋ </ts>
               <ts e="T752" id="Seg_4900" n="e" s="T751">ĭzemnie, </ts>
               <ts e="T753" id="Seg_4902" n="e" s="T752">măn </ts>
               <ts e="T754" id="Seg_4904" n="e" s="T753">dĭʔnə </ts>
               <ts e="T755" id="Seg_4906" n="e" s="T754">mămbiam: </ts>
               <ts e="T756" id="Seg_4908" n="e" s="T755">Kudajdə </ts>
               <ts e="T757" id="Seg_4910" n="e" s="T756">numan üzeʔ. </ts>
               <ts e="T758" id="Seg_4912" n="e" s="T757">Pʼeʔ </ts>
               <ts e="T759" id="Seg_4914" n="e" s="T758">dĭʔə </ts>
               <ts e="T760" id="Seg_4916" n="e" s="T759">šaldə. </ts>
               <ts e="T761" id="Seg_4918" n="e" s="T760">Kăštaʔ </ts>
               <ts e="T762" id="Seg_4920" n="e" s="T761">boskənə. </ts>
               <ts e="T763" id="Seg_4922" n="e" s="T762">Dĭ </ts>
               <ts e="T764" id="Seg_4924" n="e" s="T763">bar </ts>
               <ts e="T765" id="Seg_4926" n="e" s="T764">kăštəbi: </ts>
               <ts e="T766" id="Seg_4928" n="e" s="T765">Šoʔ </ts>
               <ts e="T767" id="Seg_4930" n="e" s="T766">măna! </ts>
               <ts e="T768" id="Seg_4932" n="e" s="T767">Šaldə </ts>
               <ts e="T769" id="Seg_4934" n="e" s="T768">deʔ! </ts>
               <ts e="T770" id="Seg_4936" n="e" s="T769">Dʼazirdə </ts>
               <ts e="T771" id="Seg_4938" n="e" s="T770">măna! </ts>
               <ts e="T772" id="Seg_4940" n="e" s="T771">Măn </ts>
               <ts e="T773" id="Seg_4942" n="e" s="T772">taldʼen </ts>
               <ts e="T774" id="Seg_4944" n="e" s="T773">tura </ts>
               <ts e="T775" id="Seg_4946" n="e" s="T774">băzəbiam. </ts>
               <ts e="T776" id="Seg_4948" n="e" s="T775">Kuris </ts>
               <ts e="T777" id="Seg_4950" n="e" s="T776">tüʔpi, </ts>
               <ts e="T778" id="Seg_4952" n="e" s="T777">tože </ts>
               <ts e="T779" id="Seg_4954" n="e" s="T778">băzəbiam. </ts>
               <ts e="T780" id="Seg_4956" n="e" s="T779">Dĭgəttə </ts>
               <ts e="T781" id="Seg_4958" n="e" s="T780">крыльцо </ts>
               <ts e="T782" id="Seg_4960" n="e" s="T781">băzəbiam. </ts>
               <ts e="T783" id="Seg_4962" n="e" s="T782">Dĭgəttə </ts>
               <ts e="T784" id="Seg_4964" n="e" s="T783">kambiam </ts>
               <ts e="T785" id="Seg_4966" n="e" s="T784">bü </ts>
               <ts e="T786" id="Seg_4968" n="e" s="T785">tazirzittə </ts>
               <ts e="T787" id="Seg_4970" n="e" s="T786">multʼanə. </ts>
               <ts e="T789" id="Seg_4972" n="e" s="T787">Oʔb </ts>
               <ts e="T790" id="Seg_4974" n="e" s="T789">muktuʔ </ts>
               <ts e="T791" id="Seg_4976" n="e" s="T790">aspaʔ </ts>
               <ts e="T792" id="Seg_4978" n="e" s="T791">deʔpiem. </ts>
               <ts e="T793" id="Seg_4980" n="e" s="T792">Măn </ts>
               <ts e="T794" id="Seg_4982" n="e" s="T793">teinen </ts>
               <ts e="T795" id="Seg_4984" n="e" s="T794">erten </ts>
               <ts e="T796" id="Seg_4986" n="e" s="T795">uʔbdəbiam. </ts>
               <ts e="T797" id="Seg_4988" n="e" s="T796">Целый </ts>
               <ts e="T798" id="Seg_4990" n="e" s="T797">(dʼa-) </ts>
               <ts e="T799" id="Seg_4992" n="e" s="T798">dʼala </ts>
               <ts e="T800" id="Seg_4994" n="e" s="T799">togonorbiam. </ts>
               <ts e="T801" id="Seg_4996" n="e" s="T800">Bar </ts>
               <ts e="T802" id="Seg_4998" n="e" s="T801">tararluʔpiem. </ts>
               <ts e="T803" id="Seg_5000" n="e" s="T802">(Nü- </ts>
               <ts e="T804" id="Seg_5002" n="e" s="T803">nüdʼin=) </ts>
               <ts e="T805" id="Seg_5004" n="e" s="T804">Nüdʼin </ts>
               <ts e="T806" id="Seg_5006" n="e" s="T805">šobi, </ts>
               <ts e="T807" id="Seg_5008" n="e" s="T806">măn </ts>
               <ts e="T808" id="Seg_5010" n="e" s="T807">kunolluʔpiam. </ts>
               <ts e="T809" id="Seg_5012" n="e" s="T808">Măn </ts>
               <ts e="T810" id="Seg_5014" n="e" s="T809">dĭzem </ts>
               <ts e="T811" id="Seg_5016" n="e" s="T810">kusʼtə </ts>
               <ts e="T812" id="Seg_5018" n="e" s="T811">(nʼe-) </ts>
               <ts e="T813" id="Seg_5020" n="e" s="T812">nʼe </ts>
               <ts e="T814" id="Seg_5022" n="e" s="T813">axota. </ts>
               <ts e="T815" id="Seg_5024" n="e" s="T814">Dĭzeŋ </ts>
               <ts e="T816" id="Seg_5026" n="e" s="T815">bar </ts>
               <ts e="T817" id="Seg_5028" n="e" s="T816">üge </ts>
               <ts e="T818" id="Seg_5030" n="e" s="T817">(šam-) </ts>
               <ts e="T819" id="Seg_5032" n="e" s="T818">šamnaʔbəʔjə. </ts>
               <ts e="T820" id="Seg_5034" n="e" s="T819">Šamnaʔbəʔjə </ts>
               <ts e="T821" id="Seg_5036" n="e" s="T820">dĭzeŋ. </ts>
               <ts e="T822" id="Seg_5038" n="e" s="T821">Arda </ts>
               <ts e="T823" id="Seg_5040" n="e" s="T822">(nʼe-) </ts>
               <ts e="T824" id="Seg_5042" n="e" s="T823">nʼilgösʼtə </ts>
               <ts e="T825" id="Seg_5044" n="e" s="T824">nʼe </ts>
               <ts e="T826" id="Seg_5046" n="e" s="T825">axota. </ts>
               <ts e="T827" id="Seg_5048" n="e" s="T826">Pušaj </ts>
               <ts e="T828" id="Seg_5050" n="e" s="T827">măna </ts>
               <ts e="T829" id="Seg_5052" n="e" s="T828">sʼimandə </ts>
               <ts e="T830" id="Seg_5054" n="e" s="T829">ej </ts>
               <ts e="T831" id="Seg_5056" n="e" s="T830">šoləʔjə. </ts>
               <ts e="T832" id="Seg_5058" n="e" s="T831">Măna </ts>
               <ts e="T833" id="Seg_5060" n="e" s="T832">dĭzem </ts>
               <ts e="T834" id="Seg_5062" n="e" s="T833">ej </ts>
               <ts e="T835" id="Seg_5064" n="e" s="T834">kereʔ. </ts>
               <ts e="T836" id="Seg_5066" n="e" s="T835">Ulum, </ts>
               <ts e="T837" id="Seg_5068" n="e" s="T836">eʔbdəm, </ts>
               <ts e="T838" id="Seg_5070" n="e" s="T837">kuzaŋbə. </ts>
               <ts e="T839" id="Seg_5072" n="e" s="T838">Tĭmem. </ts>
               <ts e="T840" id="Seg_5074" n="e" s="T839">Monzaŋdə, </ts>
               <ts e="T841" id="Seg_5076" n="e" s="T840">šĭkem. </ts>
               <ts e="T842" id="Seg_5078" n="e" s="T841">Sʼimam, </ts>
               <ts e="T843" id="Seg_5080" n="e" s="T842">püjem. </ts>
               <ts e="T844" id="Seg_5082" n="e" s="T843">Udazaŋbə, </ts>
               <ts e="T845" id="Seg_5084" n="e" s="T844">bögəlbə. </ts>
               <ts e="T846" id="Seg_5086" n="e" s="T845">(Sĭj-) </ts>
               <ts e="T847" id="Seg_5088" n="e" s="T846">Sĭjbə. </ts>
               <ts e="T848" id="Seg_5090" n="e" s="T847">Šüjöm, </ts>
               <ts e="T849" id="Seg_5092" n="e" s="T848">nanəm. </ts>
               <ts e="T850" id="Seg_5094" n="e" s="T849">(Uj-) </ts>
               <ts e="T851" id="Seg_5096" n="e" s="T850">Bögəlbə, </ts>
               <ts e="T852" id="Seg_5098" n="e" s="T851">kötendə, </ts>
               <ts e="T853" id="Seg_5100" n="e" s="T852">üjüm. </ts>
               <ts e="T854" id="Seg_5102" n="e" s="T853">Aŋdə, </ts>
               <ts e="T855" id="Seg_5104" n="e" s="T854">(sĭjbə=) </ts>
               <ts e="T856" id="Seg_5106" n="e" s="T855">sĭjdə. </ts>
            </ts>
            <ts e="T858" id="Seg_5107" n="sc" s="T857">
               <ts e="T858" id="Seg_5109" n="e" s="T857">Nüjüzaŋdə. </ts>
            </ts>
            <ts e="T861" id="Seg_5110" n="sc" s="T859">
               <ts e="T860" id="Seg_5112" n="e" s="T859">(E-) </ts>
               <ts e="T861" id="Seg_5114" n="e" s="T860">Lezeŋdə. </ts>
            </ts>
            <ts e="T863" id="Seg_5115" n="sc" s="T862">
               <ts e="T863" id="Seg_5117" n="e" s="T862">Kem. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-PKZ">
            <ta e="T10" id="Seg_5118" s="T4">PKZ_1964_SU0209.PKZ.001 (002)</ta>
            <ta e="T14" id="Seg_5119" s="T10">PKZ_1964_SU0209.PKZ.002 (003)</ta>
            <ta e="T18" id="Seg_5120" s="T14">PKZ_1964_SU0209.PKZ.003 (004)</ta>
            <ta e="T20" id="Seg_5121" s="T18">PKZ_1964_SU0209.PKZ.004 (005)</ta>
            <ta e="T24" id="Seg_5122" s="T20">PKZ_1964_SU0209.PKZ.005 (006)</ta>
            <ta e="T27" id="Seg_5123" s="T24">PKZ_1964_SU0209.PKZ.006 (007)</ta>
            <ta e="T28" id="Seg_5124" s="T27">PKZ_1964_SU0209.PKZ.007 (008)</ta>
            <ta e="T32" id="Seg_5125" s="T28">PKZ_1964_SU0209.PKZ.008 (009)</ta>
            <ta e="T40" id="Seg_5126" s="T32">PKZ_1964_SU0209.PKZ.009 (010)</ta>
            <ta e="T43" id="Seg_5127" s="T40">PKZ_1964_SU0209.PKZ.010 (011)</ta>
            <ta e="T46" id="Seg_5128" s="T43">PKZ_1964_SU0209.PKZ.011 (012)</ta>
            <ta e="T48" id="Seg_5129" s="T46">PKZ_1964_SU0209.PKZ.012 (013)</ta>
            <ta e="T54" id="Seg_5130" s="T48">PKZ_1964_SU0209.PKZ.013 (014)</ta>
            <ta e="T56" id="Seg_5131" s="T54">PKZ_1964_SU0209.PKZ.014 (015)</ta>
            <ta e="T60" id="Seg_5132" s="T56">PKZ_1964_SU0209.PKZ.015 (016)</ta>
            <ta e="T66" id="Seg_5133" s="T60">PKZ_1964_SU0209.PKZ.016 (017)</ta>
            <ta e="T69" id="Seg_5134" s="T66">PKZ_1964_SU0209.PKZ.017 (018)</ta>
            <ta e="T72" id="Seg_5135" s="T69">PKZ_1964_SU0209.PKZ.018 (019)</ta>
            <ta e="T78" id="Seg_5136" s="T72">PKZ_1964_SU0209.PKZ.019 (020)</ta>
            <ta e="T85" id="Seg_5137" s="T78">PKZ_1964_SU0209.PKZ.020 (021)</ta>
            <ta e="T87" id="Seg_5138" s="T85">PKZ_1964_SU0209.PKZ.021 (022)</ta>
            <ta e="T92" id="Seg_5139" s="T87">PKZ_1964_SU0209.PKZ.022 (023)</ta>
            <ta e="T96" id="Seg_5140" s="T92">PKZ_1964_SU0209.PKZ.023 (024)</ta>
            <ta e="T99" id="Seg_5141" s="T96">PKZ_1964_SU0209.PKZ.024 (025)</ta>
            <ta e="T101" id="Seg_5142" s="T99">PKZ_1964_SU0209.PKZ.025 (026)</ta>
            <ta e="T103" id="Seg_5143" s="T101">PKZ_1964_SU0209.PKZ.026 (027)</ta>
            <ta e="T109" id="Seg_5144" s="T103">PKZ_1964_SU0209.PKZ.027 (028)</ta>
            <ta e="T116" id="Seg_5145" s="T109">PKZ_1964_SU0209.PKZ.028 (029)</ta>
            <ta e="T120" id="Seg_5146" s="T116">PKZ_1964_SU0209.PKZ.029 (030)</ta>
            <ta e="T125" id="Seg_5147" s="T120">PKZ_1964_SU0209.PKZ.030 (031)</ta>
            <ta e="T130" id="Seg_5148" s="T125">PKZ_1964_SU0209.PKZ.031 (032)</ta>
            <ta e="T134" id="Seg_5149" s="T130">PKZ_1964_SU0209.PKZ.032 (033)</ta>
            <ta e="T137" id="Seg_5150" s="T134">PKZ_1964_SU0209.PKZ.033 (034)</ta>
            <ta e="T140" id="Seg_5151" s="T137">PKZ_1964_SU0209.PKZ.034 (035)</ta>
            <ta e="T146" id="Seg_5152" s="T140">PKZ_1964_SU0209.PKZ.035 (036)</ta>
            <ta e="T154" id="Seg_5153" s="T146">PKZ_1964_SU0209.PKZ.036 (037)</ta>
            <ta e="T161" id="Seg_5154" s="T154">PKZ_1964_SU0209.PKZ.037 (038)</ta>
            <ta e="T164" id="Seg_5155" s="T161">PKZ_1964_SU0209.PKZ.038 (039)</ta>
            <ta e="T170" id="Seg_5156" s="T164">PKZ_1964_SU0209.PKZ.039 (040)</ta>
            <ta e="T177" id="Seg_5157" s="T170">PKZ_1964_SU0209.PKZ.040 (041)</ta>
            <ta e="T181" id="Seg_5158" s="T177">PKZ_1964_SU0209.PKZ.041 (042)</ta>
            <ta e="T191" id="Seg_5159" s="T181">PKZ_1964_SU0209.PKZ.042 (043)</ta>
            <ta e="T195" id="Seg_5160" s="T191">PKZ_1964_SU0209.PKZ.043 (044)</ta>
            <ta e="T200" id="Seg_5161" s="T195">PKZ_1964_SU0209.PKZ.044 (045)</ta>
            <ta e="T203" id="Seg_5162" s="T200">PKZ_1964_SU0209.PKZ.045 (046)</ta>
            <ta e="T207" id="Seg_5163" s="T204">PKZ_1964_SU0209.PKZ.046 (048)</ta>
            <ta e="T209" id="Seg_5164" s="T207">PKZ_1964_SU0209.PKZ.047 (049)</ta>
            <ta e="T213" id="Seg_5165" s="T209">PKZ_1964_SU0209.PKZ.048 (050)</ta>
            <ta e="T216" id="Seg_5166" s="T213">PKZ_1964_SU0209.PKZ.049 (051)</ta>
            <ta e="T220" id="Seg_5167" s="T216">PKZ_1964_SU0209.PKZ.050 (052)</ta>
            <ta e="T225" id="Seg_5168" s="T220">PKZ_1964_SU0209.PKZ.051 (053)</ta>
            <ta e="T233" id="Seg_5169" s="T225">PKZ_1964_SU0209.PKZ.052 (054)</ta>
            <ta e="T242" id="Seg_5170" s="T233">PKZ_1964_SU0209.PKZ.053 (055)</ta>
            <ta e="T249" id="Seg_5171" s="T242">PKZ_1964_SU0209.PKZ.054 (056)</ta>
            <ta e="T252" id="Seg_5172" s="T249">PKZ_1964_SU0209.PKZ.055 (057)</ta>
            <ta e="T255" id="Seg_5173" s="T252">PKZ_1964_SU0209.PKZ.056 (058)</ta>
            <ta e="T258" id="Seg_5174" s="T255">PKZ_1964_SU0209.PKZ.057 (059)</ta>
            <ta e="T261" id="Seg_5175" s="T258">PKZ_1964_SU0209.PKZ.058 (060)</ta>
            <ta e="T265" id="Seg_5176" s="T261">PKZ_1964_SU0209.PKZ.059 (061)</ta>
            <ta e="T271" id="Seg_5177" s="T265">PKZ_1964_SU0209.PKZ.060 (062)</ta>
            <ta e="T275" id="Seg_5178" s="T271">PKZ_1964_SU0209.PKZ.061 (063)</ta>
            <ta e="T276" id="Seg_5179" s="T275">PKZ_1964_SU0209.PKZ.062 (064)</ta>
            <ta e="T282" id="Seg_5180" s="T276">PKZ_1964_SU0209.PKZ.063 (065)</ta>
            <ta e="T287" id="Seg_5181" s="T282">PKZ_1964_SU0209.PKZ.064 (066)</ta>
            <ta e="T293" id="Seg_5182" s="T287">PKZ_1964_SU0209.PKZ.065 (067)</ta>
            <ta e="T295" id="Seg_5183" s="T293">PKZ_1964_SU0209.PKZ.066 (068)</ta>
            <ta e="T299" id="Seg_5184" s="T295">PKZ_1964_SU0209.PKZ.067 (069)</ta>
            <ta e="T301" id="Seg_5185" s="T299">PKZ_1964_SU0209.PKZ.068 (070)</ta>
            <ta e="T305" id="Seg_5186" s="T301">PKZ_1964_SU0209.PKZ.069 (071)</ta>
            <ta e="T314" id="Seg_5187" s="T305">PKZ_1964_SU0209.PKZ.070 (072)</ta>
            <ta e="T321" id="Seg_5188" s="T314">PKZ_1964_SU0209.PKZ.071 (073)</ta>
            <ta e="T332" id="Seg_5189" s="T321">PKZ_1964_SU0209.PKZ.072 (074)</ta>
            <ta e="T338" id="Seg_5190" s="T332">PKZ_1964_SU0209.PKZ.073 (075)</ta>
            <ta e="T343" id="Seg_5191" s="T338">PKZ_1964_SU0209.PKZ.074 (076)</ta>
            <ta e="T348" id="Seg_5192" s="T343">PKZ_1964_SU0209.PKZ.075 (077)</ta>
            <ta e="T357" id="Seg_5193" s="T348">PKZ_1964_SU0209.PKZ.076 (078)</ta>
            <ta e="T360" id="Seg_5194" s="T357">PKZ_1964_SU0209.PKZ.077 (079)</ta>
            <ta e="T363" id="Seg_5195" s="T360">PKZ_1964_SU0209.PKZ.078 (080)</ta>
            <ta e="T366" id="Seg_5196" s="T363">PKZ_1964_SU0209.PKZ.079 (081)</ta>
            <ta e="T369" id="Seg_5197" s="T366">PKZ_1964_SU0209.PKZ.080 (082)</ta>
            <ta e="T374" id="Seg_5198" s="T369">PKZ_1964_SU0209.PKZ.081 (083)</ta>
            <ta e="T385" id="Seg_5199" s="T374">PKZ_1964_SU0209.PKZ.082 (084)</ta>
            <ta e="T389" id="Seg_5200" s="T385">PKZ_1964_SU0209.PKZ.083 (085)</ta>
            <ta e="T392" id="Seg_5201" s="T389">PKZ_1964_SU0209.PKZ.084 (086)</ta>
            <ta e="T399" id="Seg_5202" s="T392">PKZ_1964_SU0209.PKZ.085 (087)</ta>
            <ta e="T405" id="Seg_5203" s="T399">PKZ_1964_SU0209.PKZ.086 (088)</ta>
            <ta e="T412" id="Seg_5204" s="T405">PKZ_1964_SU0209.PKZ.087 (089)</ta>
            <ta e="T415" id="Seg_5205" s="T412">PKZ_1964_SU0209.PKZ.088 (090)</ta>
            <ta e="T424" id="Seg_5206" s="T419">PKZ_1964_SU0209.PKZ.089 (093)</ta>
            <ta e="T428" id="Seg_5207" s="T424">PKZ_1964_SU0209.PKZ.090 (094)</ta>
            <ta e="T435" id="Seg_5208" s="T428">PKZ_1964_SU0209.PKZ.091 (095)</ta>
            <ta e="T440" id="Seg_5209" s="T435">PKZ_1964_SU0209.PKZ.092 (096)</ta>
            <ta e="T445" id="Seg_5210" s="T440">PKZ_1964_SU0209.PKZ.093 (097)</ta>
            <ta e="T453" id="Seg_5211" s="T445">PKZ_1964_SU0209.PKZ.094 (098)</ta>
            <ta e="T461" id="Seg_5212" s="T453">PKZ_1964_SU0209.PKZ.095 (099)</ta>
            <ta e="T473" id="Seg_5213" s="T461">PKZ_1964_SU0209.PKZ.096 (100)</ta>
            <ta e="T481" id="Seg_5214" s="T473">PKZ_1964_SU0209.PKZ.097 (101)</ta>
            <ta e="T483" id="Seg_5215" s="T481">PKZ_1964_SU0209.PKZ.098 (102)</ta>
            <ta e="T488" id="Seg_5216" s="T483">PKZ_1964_SU0209.PKZ.099 (103)</ta>
            <ta e="T493" id="Seg_5217" s="T488">PKZ_1964_SU0209.PKZ.100 (104)</ta>
            <ta e="T500" id="Seg_5218" s="T493">PKZ_1964_SU0209.PKZ.101 (105)</ta>
            <ta e="T503" id="Seg_5219" s="T500">PKZ_1964_SU0209.PKZ.102 (106)</ta>
            <ta e="T512" id="Seg_5220" s="T503">PKZ_1964_SU0209.PKZ.103 (107)</ta>
            <ta e="T516" id="Seg_5221" s="T512">PKZ_1964_SU0209.PKZ.104 (108)</ta>
            <ta e="T519" id="Seg_5222" s="T516">PKZ_1964_SU0209.PKZ.105 (109)</ta>
            <ta e="T524" id="Seg_5223" s="T519">PKZ_1964_SU0209.PKZ.106 (110)</ta>
            <ta e="T528" id="Seg_5224" s="T524">PKZ_1964_SU0209.PKZ.107 (111)</ta>
            <ta e="T535" id="Seg_5225" s="T528">PKZ_1964_SU0209.PKZ.108 (112)</ta>
            <ta e="T542" id="Seg_5226" s="T535">PKZ_1964_SU0209.PKZ.109 (113)</ta>
            <ta e="T544" id="Seg_5227" s="T542">PKZ_1964_SU0209.PKZ.110 (114)</ta>
            <ta e="T546" id="Seg_5228" s="T544">PKZ_1964_SU0209.PKZ.111 (115)</ta>
            <ta e="T551" id="Seg_5229" s="T546">PKZ_1964_SU0209.PKZ.112 (116)</ta>
            <ta e="T555" id="Seg_5230" s="T551">PKZ_1964_SU0209.PKZ.113 (117)</ta>
            <ta e="T563" id="Seg_5231" s="T555">PKZ_1964_SU0209.PKZ.114 (118)</ta>
            <ta e="T566" id="Seg_5232" s="T563">PKZ_1964_SU0209.PKZ.115 (119)</ta>
            <ta e="T571" id="Seg_5233" s="T566">PKZ_1964_SU0209.PKZ.116 (120)</ta>
            <ta e="T575" id="Seg_5234" s="T571">PKZ_1964_SU0209.PKZ.117 (121)</ta>
            <ta e="T579" id="Seg_5235" s="T575">PKZ_1964_SU0209.PKZ.118 (122)</ta>
            <ta e="T590" id="Seg_5236" s="T579">PKZ_1964_SU0209.PKZ.119 (123) </ta>
            <ta e="T597" id="Seg_5237" s="T590">PKZ_1964_SU0209.PKZ.120 (125)</ta>
            <ta e="T601" id="Seg_5238" s="T597">PKZ_1964_SU0209.PKZ.121 (126)</ta>
            <ta e="T606" id="Seg_5239" s="T601">PKZ_1964_SU0209.PKZ.122 (127)</ta>
            <ta e="T609" id="Seg_5240" s="T606">PKZ_1964_SU0209.PKZ.123 (128)</ta>
            <ta e="T614" id="Seg_5241" s="T609">PKZ_1964_SU0209.PKZ.124 (129)</ta>
            <ta e="T616" id="Seg_5242" s="T614">PKZ_1964_SU0209.PKZ.125 (130)</ta>
            <ta e="T619" id="Seg_5243" s="T616">PKZ_1964_SU0209.PKZ.126 (131)</ta>
            <ta e="T627" id="Seg_5244" s="T619">PKZ_1964_SU0209.PKZ.127 (132)</ta>
            <ta e="T631" id="Seg_5245" s="T627">PKZ_1964_SU0209.PKZ.128 (133)</ta>
            <ta e="T636" id="Seg_5246" s="T631">PKZ_1964_SU0209.PKZ.129 (134)</ta>
            <ta e="T640" id="Seg_5247" s="T636">PKZ_1964_SU0209.PKZ.130 (135)</ta>
            <ta e="T644" id="Seg_5248" s="T640">PKZ_1964_SU0209.PKZ.131 (136)</ta>
            <ta e="T648" id="Seg_5249" s="T644">PKZ_1964_SU0209.PKZ.132 (137)</ta>
            <ta e="T650" id="Seg_5250" s="T648">PKZ_1964_SU0209.PKZ.133 (138)</ta>
            <ta e="T654" id="Seg_5251" s="T650">PKZ_1964_SU0209.PKZ.134 (139)</ta>
            <ta e="T662" id="Seg_5252" s="T654">PKZ_1964_SU0209.PKZ.135 (140)</ta>
            <ta e="T668" id="Seg_5253" s="T662">PKZ_1964_SU0209.PKZ.136 (141)</ta>
            <ta e="T674" id="Seg_5254" s="T668">PKZ_1964_SU0209.PKZ.137 (142)</ta>
            <ta e="T682" id="Seg_5255" s="T674">PKZ_1964_SU0209.PKZ.138 (143)</ta>
            <ta e="T687" id="Seg_5256" s="T682">PKZ_1964_SU0209.PKZ.139 (144)</ta>
            <ta e="T691" id="Seg_5257" s="T687">PKZ_1964_SU0209.PKZ.140 (145)</ta>
            <ta e="T696" id="Seg_5258" s="T691">PKZ_1964_SU0209.PKZ.141 (146)</ta>
            <ta e="T700" id="Seg_5259" s="T696">PKZ_1964_SU0209.PKZ.142 (147)</ta>
            <ta e="T703" id="Seg_5260" s="T700">PKZ_1964_SU0209.PKZ.143 (148)</ta>
            <ta e="T706" id="Seg_5261" s="T703">PKZ_1964_SU0209.PKZ.144 (149)</ta>
            <ta e="T708" id="Seg_5262" s="T706">PKZ_1964_SU0209.PKZ.145 (150)</ta>
            <ta e="T710" id="Seg_5263" s="T708">PKZ_1964_SU0209.PKZ.146 (151)</ta>
            <ta e="T712" id="Seg_5264" s="T710">PKZ_1964_SU0209.PKZ.147 (152)</ta>
            <ta e="T715" id="Seg_5265" s="T712">PKZ_1964_SU0209.PKZ.148 (153)</ta>
            <ta e="T718" id="Seg_5266" s="T715">PKZ_1964_SU0209.PKZ.149 (154)</ta>
            <ta e="T721" id="Seg_5267" s="T718">PKZ_1964_SU0209.PKZ.150 (155)</ta>
            <ta e="T723" id="Seg_5268" s="T721">PKZ_1964_SU0209.PKZ.151 (156)</ta>
            <ta e="T726" id="Seg_5269" s="T723">PKZ_1964_SU0209.PKZ.152 (157)</ta>
            <ta e="T729" id="Seg_5270" s="T726">PKZ_1964_SU0209.PKZ.153 (158)</ta>
            <ta e="T732" id="Seg_5271" s="T729">PKZ_1964_SU0209.PKZ.154 (159)</ta>
            <ta e="T735" id="Seg_5272" s="T732">PKZ_1964_SU0209.PKZ.155 (160)</ta>
            <ta e="T739" id="Seg_5273" s="T735">PKZ_1964_SU0209.PKZ.156 (161)</ta>
            <ta e="T747" id="Seg_5274" s="T739">PKZ_1964_SU0209.PKZ.157 (162)</ta>
            <ta e="T757" id="Seg_5275" s="T747">PKZ_1964_SU0209.PKZ.158 (163) </ta>
            <ta e="T760" id="Seg_5276" s="T757">PKZ_1964_SU0209.PKZ.159 (165)</ta>
            <ta e="T762" id="Seg_5277" s="T760">PKZ_1964_SU0209.PKZ.160 (166)</ta>
            <ta e="T767" id="Seg_5278" s="T762">PKZ_1964_SU0209.PKZ.161 (167)</ta>
            <ta e="T769" id="Seg_5279" s="T767">PKZ_1964_SU0209.PKZ.162 (168)</ta>
            <ta e="T771" id="Seg_5280" s="T769">PKZ_1964_SU0209.PKZ.163 (169)</ta>
            <ta e="T775" id="Seg_5281" s="T771">PKZ_1964_SU0209.PKZ.164 (170)</ta>
            <ta e="T779" id="Seg_5282" s="T775">PKZ_1964_SU0209.PKZ.165 (171)</ta>
            <ta e="T782" id="Seg_5283" s="T779">PKZ_1964_SU0209.PKZ.166 (172)</ta>
            <ta e="T787" id="Seg_5284" s="T782">PKZ_1964_SU0209.PKZ.167 (173)</ta>
            <ta e="T792" id="Seg_5285" s="T787">PKZ_1964_SU0209.PKZ.168 (174)</ta>
            <ta e="T796" id="Seg_5286" s="T792">PKZ_1964_SU0209.PKZ.169 (175)</ta>
            <ta e="T800" id="Seg_5287" s="T796">PKZ_1964_SU0209.PKZ.170 (176)</ta>
            <ta e="T802" id="Seg_5288" s="T800">PKZ_1964_SU0209.PKZ.171 (177)</ta>
            <ta e="T808" id="Seg_5289" s="T802">PKZ_1964_SU0209.PKZ.172 (178)</ta>
            <ta e="T814" id="Seg_5290" s="T808">PKZ_1964_SU0209.PKZ.173 (179)</ta>
            <ta e="T819" id="Seg_5291" s="T814">PKZ_1964_SU0209.PKZ.174 (180)</ta>
            <ta e="T821" id="Seg_5292" s="T819">PKZ_1964_SU0209.PKZ.175 (181)</ta>
            <ta e="T826" id="Seg_5293" s="T821">PKZ_1964_SU0209.PKZ.176 (182)</ta>
            <ta e="T831" id="Seg_5294" s="T826">PKZ_1964_SU0209.PKZ.177 (183)</ta>
            <ta e="T835" id="Seg_5295" s="T831">PKZ_1964_SU0209.PKZ.178 (184)</ta>
            <ta e="T838" id="Seg_5296" s="T835">PKZ_1964_SU0209.PKZ.179 (185)</ta>
            <ta e="T839" id="Seg_5297" s="T838">PKZ_1964_SU0209.PKZ.180 (186)</ta>
            <ta e="T841" id="Seg_5298" s="T839">PKZ_1964_SU0209.PKZ.181 (187)</ta>
            <ta e="T843" id="Seg_5299" s="T841">PKZ_1964_SU0209.PKZ.182 (188)</ta>
            <ta e="T845" id="Seg_5300" s="T843">PKZ_1964_SU0209.PKZ.183 (189)</ta>
            <ta e="T847" id="Seg_5301" s="T845">PKZ_1964_SU0209.PKZ.184 (190)</ta>
            <ta e="T849" id="Seg_5302" s="T847">PKZ_1964_SU0209.PKZ.185 (191)</ta>
            <ta e="T853" id="Seg_5303" s="T849">PKZ_1964_SU0209.PKZ.186 (192)</ta>
            <ta e="T856" id="Seg_5304" s="T853">PKZ_1964_SU0209.PKZ.187 (193)</ta>
            <ta e="T858" id="Seg_5305" s="T857">PKZ_1964_SU0209.PKZ.188 (195)</ta>
            <ta e="T861" id="Seg_5306" s="T859">PKZ_1964_SU0209.PKZ.189 (197)</ta>
            <ta e="T863" id="Seg_5307" s="T862">PKZ_1964_SU0209.PKZ.190 (199)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-PKZ">
            <ta e="T10" id="Seg_5308" s="T4">Döbər šolal dăk, măna sima iʔ. </ta>
            <ta e="T14" id="Seg_5309" s="T10">Tăn büzʼe gibər kambi? </ta>
            <ta e="T18" id="Seg_5310" s="T14">Dʼelamdə (kalla-) kalla dʼürbi, ine. </ta>
            <ta e="T20" id="Seg_5311" s="T18">Unnʼa kambi? </ta>
            <ta e="T24" id="Seg_5312" s="T20">Dʼok, šidegöʔ kalla dʼürbiʔi bar. </ta>
            <ta e="T27" id="Seg_5313" s="T24">Kamən (ka-) kambiʔi? </ta>
            <ta e="T28" id="Seg_5314" s="T27">(Погоди). </ta>
            <ta e="T32" id="Seg_5315" s="T28">Taldʼen kambiʔi, ugandə erten. </ta>
            <ta e="T40" id="Seg_5316" s="T32">Ĭmbi šaːmnaʔbəl, ej (sĭ- sĭr-) ej (sĭr-) sĭriel? </ta>
            <ta e="T43" id="Seg_5317" s="T40">Köžetsnəktə (noldə-) nuldʼit. </ta>
            <ta e="T46" id="Seg_5318" s="T43">((DMG)) Bar (kujnekəm) nĭŋgəluʔpi. </ta>
            <ta e="T48" id="Seg_5319" s="T46">Ši molaːmbi. </ta>
            <ta e="T54" id="Seg_5320" s="T48">Măn dĭgəttə dĭm (sö-) šödörbiam, nʼitkaʔizʼiʔ. </ta>
            <ta e="T56" id="Seg_5321" s="T54">Nʼimizʼiʔ, ĭndak. </ta>
            <ta e="T60" id="Seg_5322" s="T56">Tʼegermaʔgən bar küzürleʔbə kunguro. </ta>
            <ta e="T66" id="Seg_5323" s="T60">Nada kanzittə kudajdə (üzəš-) üzəsʼtə dĭgəttə. </ta>
            <ta e="T69" id="Seg_5324" s="T66">Kăštəliaʔi dĭzeŋ bar. </ta>
            <ta e="T72" id="Seg_5325" s="T69">Edət inenə koŋgoro. </ta>
            <ta e="T78" id="Seg_5326" s="T72">Sagəšdə naga, bar ĭmbidə ej tĭmnem. </ta>
            <ta e="T85" id="Seg_5327" s="T78">((DMG)) Ĭmbidə ej moliam nörbəsʼtə bar, sagəšdə naga. </ta>
            <ta e="T87" id="Seg_5328" s="T85">Dʼijenə kambiam. </ta>
            <ta e="T92" id="Seg_5329" s="T87">Bar (dʼü- dʼür- dʼürdə-) tʼürleʔbə. </ta>
            <ta e="T96" id="Seg_5330" s="T92">Dʼijenə kambiam, bar dʼürleʔpiem. </ta>
            <ta e="T99" id="Seg_5331" s="T96">Dĭgəttə kirgarbiam bar. </ta>
            <ta e="T101" id="Seg_5332" s="T99">Dĭgəttə šobiam. </ta>
            <ta e="T103" id="Seg_5333" s="T101">Kondʼo amnobi. </ta>
            <ta e="T109" id="Seg_5334" s="T103">Ĭmbidə bar ((PAUSE)) ĭmbidə ej dʼăbaktərbi. </ta>
            <ta e="T116" id="Seg_5335" s="T109">Dĭ kandəga, a măn dĭm (pă-) bĭdliem. </ta>
            <ta e="T120" id="Seg_5336" s="T116">"Iʔ kanaʔ, paraʔ döbər! </ta>
            <ta e="T125" id="Seg_5337" s="T120">Măn tănan mănliam: paraʔ döbər!" </ta>
            <ta e="T130" id="Seg_5338" s="T125">Măn ugandə urgo il ige. </ta>
            <ta e="T134" id="Seg_5339" s="T130">Ipek iʔgö (kereʔnə-) kereʔ. </ta>
            <ta e="T137" id="Seg_5340" s="T134">Iʔ bögəldə dĭm! </ta>
            <ta e="T140" id="Seg_5341" s="T137">Udal ej endə! </ta>
            <ta e="T146" id="Seg_5342" s="T140">((DMG)) (Ižəmdə) dĭm, (ižəmdə) dĭm udazi. </ta>
            <ta e="T154" id="Seg_5343" s="T146">((DMG)) Boskənə iʔgö ibiem, a dĭʔnə amgam ibiem. </ta>
            <ta e="T161" id="Seg_5344" s="T154">Boskəndə iʔgö ilie, a dĭʔnə amgam ilie. </ta>
            <ta e="T164" id="Seg_5345" s="T161">Muktuʔ bar dʼăbaktərbibaʔ. </ta>
            <ta e="T170" id="Seg_5346" s="T164">Miʔ bar üdʼüge ibibeʔ, bar sʼarlaʔpibaʔ. </ta>
            <ta e="T177" id="Seg_5347" s="T170">Paʔi iləj da (bar oj) onʼiʔ nuʔmələj. </ta>
            <ta e="T181" id="Seg_5348" s="T177">A miʔ bar šalaːmbibeʔ. </ta>
            <ta e="T191" id="Seg_5349" s="T181">Dĭgəttə dĭ bar kulia, kulia, (ej mozi-) ej molia kuzittə. </ta>
            <ta e="T195" id="Seg_5350" s="T191">Miʔ bar bazoʔ sarlaʔbəbaʔ. </ta>
            <ta e="T200" id="Seg_5351" s="T195">(M-) Miʔ bar ej kudonzluʔpibaʔ. </ta>
            <ta e="T203" id="Seg_5352" s="T200">Jakšə bar … </ta>
            <ta e="T207" id="Seg_5353" s="T204">Jakšə bar … </ta>
            <ta e="T209" id="Seg_5354" s="T207">(Пока закрой).</ta>
            <ta e="T213" id="Seg_5355" s="T209">Miʔ bar jakšə sʼarbibaʔ. </ta>
            <ta e="T216" id="Seg_5356" s="T213">Miʔ ej (kurollubibaʔ). </ta>
            <ta e="T220" id="Seg_5357" s="T216">Tibizeŋ i nezeŋ amnobiʔi. </ta>
            <ta e="T225" id="Seg_5358" s="T220">Da măndolaʔpiʔi, kăde miʔ sʼarlaʔbəbaʔ. </ta>
            <ta e="T233" id="Seg_5359" s="T225">Šindi (n- nʼe-) nʼeʔtəbi taŋgo, šindi ej nʼeʔtəbi. </ta>
            <ta e="T242" id="Seg_5360" s="T233">Šindi bar (kaŋ-) kanzasi bar nʼeʔleʔbə, šindi ej nʼeʔleʔbə. </ta>
            <ta e="T249" id="Seg_5361" s="T242">Ular ugaːndə iʔgö, однако šide bit šide. </ta>
            <ta e="T252" id="Seg_5362" s="T249">Abam dʼijenə kambi. </ta>
            <ta e="T255" id="Seg_5363" s="T252">Iʔgö kurizəʔi kuʔpi. </ta>
            <ta e="T258" id="Seg_5364" s="T255">I maːndə deʔpi. </ta>
            <ta e="T261" id="Seg_5365" s="T258">Măn šonəgam Permʼakovo. </ta>
            <ta e="T265" id="Seg_5366" s="T261">Kuliom: bar bulan nuga. </ta>
            <ta e="T271" id="Seg_5367" s="T265">Măn măndobiam, măndobiam, dĭgəttə bar kirgarluʔpim. </ta>
            <ta e="T275" id="Seg_5368" s="T271">Dĭ bar nuʔməluʔpi bünə. </ta>
            <ta e="T276" id="Seg_5369" s="T275">(Penzi). </ta>
            <ta e="T282" id="Seg_5370" s="T276">Dĭgəttə dĭ (bar kaluʔpi) i nuʔməluʔpi. </ta>
            <ta e="T287" id="Seg_5371" s="T282">Măn šobiam da, Vlastə nörbəbiem. </ta>
            <ta e="T293" id="Seg_5372" s="T287">Dĭ kambi, multuksi da (kutl-) kuʔpi. </ta>
            <ta e="T295" id="Seg_5373" s="T293">((DMG)) aspaʔ. </ta>
            <ta e="T299" id="Seg_5374" s="T295">Măn dĭgəttə uja băzəbiam. </ta>
            <ta e="T301" id="Seg_5375" s="T299">I ambiam. </ta>
            <ta e="T305" id="Seg_5376" s="T301">Miʔnʼibeʔ mĭbi onʼiʔ aspaʔ. </ta>
            <ta e="T314" id="Seg_5377" s="T305">Финн kuza šobi, măn dibər mĭmbiem, dĭzeŋ bar dʼăbaktərlaʔbəʔjə. </ta>
            <ta e="T321" id="Seg_5378" s="T314">A dĭzeŋ bar măna sürerluʔpiʔjə, măn nuʔməluʔpiem. </ta>
            <ta e="T332" id="Seg_5379" s="T321">((DMG)) unnʼa nuzaŋ amnobiʔi, jakšə ibi, ujat iʔgö ibi, bar amnial. </ta>
            <ta e="T338" id="Seg_5380" s="T332">Ălenʼən uja ugandə nömər, kak ipek. </ta>
            <ta e="T343" id="Seg_5381" s="T338">Iʔgö amnial, dĭgəttə bü bĭtliel. </ta>
            <ta e="T348" id="Seg_5382" s="T343">((…)) bü bĭtliel, nu … </ta>
            <ta e="T357" id="Seg_5383" s="T348">Dĭ koŋ ugandə jakšə, ugandə kuvas, (sʼimat) (tʼi-) sagər. </ta>
            <ta e="T360" id="Seg_5384" s="T357">Püjet ej numo. </ta>
            <ta e="T363" id="Seg_5385" s="T360">Monzaŋdə kuvas bar. </ta>
            <ta e="T366" id="Seg_5386" s="T363">Monzaŋdə ej nʼešpək. </ta>
            <ta e="T369" id="Seg_5387" s="T366">Матрена bar ĭzemneʔpi. </ta>
            <ta e="T374" id="Seg_5388" s="T369">Măn (dĭʔ-) dĭʔnə (kal-) kalam. </ta>
            <ta e="T385" id="Seg_5389" s="T374">Dĭ bar măndə:" Iʔ dʼăbaktəraʔ kazan šĭkətsi, a bostə šĭkətsi dʼăbaktəraʔ. </ta>
            <ta e="T389" id="Seg_5390" s="T385">Măn ugandə ĭzemniem bar. </ta>
            <ta e="T392" id="Seg_5391" s="T389">Ugandə ĭzemniem bar. </ta>
            <ta e="T399" id="Seg_5392" s="T392">Măn bar külaːmbiam, (külaːmbiam), a tăn maləl. </ta>
            <ta e="T405" id="Seg_5393" s="T399">Dĭ külaːmbi, šide koʔbdot (maː-) maluʔpiʔi. </ta>
            <ta e="T412" id="Seg_5394" s="T405">Onʼiʔ dĭzi ibi, a onʼiʔ kuŋgə ibi. </ta>
            <ta e="T415" id="Seg_5395" s="T412">Teinen bar … </ta>
            <ta e="T424" id="Seg_5396" s="T419">Teinen selɨj dʼalat surno šobi. </ta>
            <ta e="T428" id="Seg_5397" s="T424">A tüj kujo măndolaʔbə. </ta>
            <ta e="T435" id="Seg_5398" s="T428">Novik šonəga šoškandə, a Sidor mendə amnolaʔbə. </ta>
            <ta e="T440" id="Seg_5399" s="T435">A il kubiʔi, tenəbiʔi: казаки. </ta>
            <ta e="T445" id="Seg_5400" s="T440">Predsʼedatelʼ mămbi:" Nada tʼerməndə (adʼazirzittə). </ta>
            <ta e="T453" id="Seg_5401" s="T445">Jakšə il oʔbdəsʼtə, (pusk-) (tog-) puskaj dĭn togonorlaʔi. </ta>
            <ta e="T461" id="Seg_5402" s="T453">Nada tʼerməndə kanzittə, un nʼeʔsittə, a to un naga. </ta>
            <ta e="T473" id="Seg_5403" s="T461">(Kob-) Măn üdʼüge ibiem, a koʔbsam urgo ibiʔi, uge măna ibiʔi boskəndə. </ta>
            <ta e="T481" id="Seg_5404" s="T473">Nüjnə (nüjnəsʼtə), (măna) голос - то kuvas ibi. </ta>
            <ta e="T483" id="Seg_5405" s="T481">(Nüjnəbiem) bar. </ta>
            <ta e="T488" id="Seg_5406" s="T483">Dĭ kuza ugandə jakšə nüjlie. </ta>
            <ta e="T493" id="Seg_5407" s="T488">Голос-то ugandə kuvas. </ta>
            <ta e="T500" id="Seg_5408" s="T493">Голос как … Нет, не так … </ta>
            <ta e="T503" id="Seg_5409" s="T500">Ugandə tăŋ nüjleʔbə. </ta>
            <ta e="T512" id="Seg_5410" s="T503">Dĭ kuza külaːmbi, nada maʔtə azittə, (pa= pazi=) pagə. </ta>
            <ta e="T516" id="Seg_5411" s="T512">I dibər dĭm enzittə. </ta>
            <ta e="T519" id="Seg_5412" s="T516">I kros azittə. </ta>
            <ta e="T524" id="Seg_5413" s="T519">Dĭn tüj kaləj (šü dʼünə). </ta>
            <ta e="T528" id="Seg_5414" s="T524">Ugandə kuvas turaʔi nugaʔi. </ta>
            <ta e="T535" id="Seg_5415" s="T528">(Dĭzeŋ=) Dĭzeŋ (pi- pi-) piʔi (am-) embiʔi. </ta>
            <ta e="T542" id="Seg_5416" s="T535">(Kuznek=) Kuznekən tondə bar paʔi (nuga-) amnolbiʔi. </ta>
            <ta e="T544" id="Seg_5417" s="T542">Paʔi amnolbiʔi. </ta>
            <ta e="T546" id="Seg_5418" s="T544">Малина amnolbiʔi. </ta>
            <ta e="T551" id="Seg_5419" s="T546">Ugandə kuvas tura (nuga-) nugaʔbəʔjə. </ta>
            <ta e="T555" id="Seg_5420" s="T551">Lem keʔbde bar amnolbiʔi. </ta>
            <ta e="T563" id="Seg_5421" s="T555">Lʼevăj (uga-) udanə kuvas i pravăj udanə kuvas. </ta>
            <ta e="T566" id="Seg_5422" s="T563">По левую сторону. </ta>
            <ta e="T571" id="Seg_5423" s="T566">Ugandə kuvas (nura-) turaʔi nugaʔi. </ta>
            <ta e="T575" id="Seg_5424" s="T571">Măn mejəm самогон mĭnzərbi. </ta>
            <ta e="T579" id="Seg_5425" s="T575">Dĭ bar ej nʼeʔdə. </ta>
            <ta e="T590" id="Seg_5426" s="T579">Kuza bar kudonzlaʔbə: "Daška ej iləl mĭnzərzittə, ej jakšə, ej nʼeʔdə". </ta>
            <ta e="T597" id="Seg_5427" s="T590">Dĭgəttə dĭ mejəm (mum-) büm mĭnzərbi, tussi. </ta>
            <ta e="T601" id="Seg_5428" s="T597">Dĭgəttə (mʼa-) uja jaʔpi. </ta>
            <ta e="T606" id="Seg_5429" s="T601">I dibər endəge, štobɨ tustʼarlaʔbə. </ta>
            <ta e="T609" id="Seg_5430" s="T606">(Ejet ejen ne). </ta>
            <ta e="T614" id="Seg_5431" s="T609">((DMG)) Kanzittə никак нельзя, ugandə bü. </ta>
            <ta e="T616" id="Seg_5432" s="T614">Nada parluʔsittə. </ta>
            <ta e="T619" id="Seg_5433" s="T616">Ugandə kuŋgə idlia. </ta>
            <ta e="T627" id="Seg_5434" s="T619">Степь bar, paʔi naga, onʼiʔ da pa naga. </ta>
            <ta e="T631" id="Seg_5435" s="T627">Cтепью šonəgam, степь nüjleʔbəm. </ta>
            <ta e="T636" id="Seg_5436" s="T631">Dʼijenə kandəgam, dʼije (nüjle-) nüjleʔbəm. </ta>
            <ta e="T640" id="Seg_5437" s="T636">Bünə kandəgam, bü nüjleʔbəm </ta>
            <ta e="T644" id="Seg_5438" s="T640">Pagən kandəgam, pa nüjleʔbəm. </ta>
            <ta e="T648" id="Seg_5439" s="T644">Măn abam Kirelʼdə kambi. </ta>
            <ta e="T650" id="Seg_5440" s="T648">Лодка ibi. </ta>
            <ta e="T654" id="Seg_5441" s="T650">Kola (u-) iʔgö dʼaʔpi. </ta>
            <ta e="T662" id="Seg_5442" s="T654">Miʔ arəmdəbibaʔ, i tustʼarbibaʔ, i (pürbibeʔ), i ambibaʔ. </ta>
            <ta e="T668" id="Seg_5443" s="T662">Miʔ (mĭmbieʔ), i ugandə urgo bü. </ta>
            <ta e="T674" id="Seg_5444" s="T668">Dĭn bar sedʼiʔi (barəʔpibiaʔ), kola dʼaʔpibaʔ. </ta>
            <ta e="T682" id="Seg_5445" s="T674">Kuza bar nem pʼaŋdəbi, a bostə panə edəluʔpi. </ta>
            <ta e="T687" id="Seg_5446" s="T682">(Urga=) Urgaːba ugandə tărdə iʔgö. </ta>
            <ta e="T691" id="Seg_5447" s="T687">Bar uja ej idlia. </ta>
            <ta e="T696" id="Seg_5448" s="T691">Ugandə dĭ nömər, ugandə kəbə. </ta>
            <ta e="T700" id="Seg_5449" s="T696">Ujut ugandə nʼeʔdə (nʼeʔdə=). </ta>
            <ta e="T703" id="Seg_5450" s="T700">Kadaʔi ugandə urgo. </ta>
            <ta e="T706" id="Seg_5451" s="T703">Urgo (men=) menzi. </ta>
            <ta e="T708" id="Seg_5452" s="T706">Urgo menzi. </ta>
            <ta e="T710" id="Seg_5453" s="T708">Sʼimat üdʼüge. </ta>
            <ta e="T712" id="Seg_5454" s="T710">Kuʔi üdʼüge. </ta>
            <ta e="T715" id="Seg_5455" s="T712">Măn tuganbə šobi. </ta>
            <ta e="T718" id="Seg_5456" s="T715">Nʼi šobi, nezi. </ta>
            <ta e="T721" id="Seg_5457" s="T718">Kagam šobi, nezi. </ta>
            <ta e="T723" id="Seg_5458" s="T721">Esseŋ … </ta>
            <ta e="T726" id="Seg_5459" s="T723">Măn tuganbə šobi. </ta>
            <ta e="T729" id="Seg_5460" s="T726">Nʼim šobi, nezi. </ta>
            <ta e="T732" id="Seg_5461" s="T729">Kagam šobi, nezi. </ta>
            <ta e="T735" id="Seg_5462" s="T732">Koʔbdom tibizi šobi. </ta>
            <ta e="T739" id="Seg_5463" s="T735">(Esseŋ=) Esseŋdə dĭzi šobiʔi. </ta>
            <ta e="T747" id="Seg_5464" s="T739">Nada dĭzeŋdə (obsittə-) oʔbdəsʼtə, dĭzeŋ (amorzi-) amzittə mĭzittə. </ta>
            <ta e="T757" id="Seg_5465" s="T747">Măn nʼim bar tăŋ ĭzemnie, măn dĭʔnə mămbiam: "Kudajdə numan üzeʔ. </ta>
            <ta e="T760" id="Seg_5466" s="T757">Pʼeʔ dĭʔə šaldə. </ta>
            <ta e="T762" id="Seg_5467" s="T760">Kăštaʔ boskənə". </ta>
            <ta e="T767" id="Seg_5468" s="T762">Dĭ bar kăštəbi:" Šoʔ măna! </ta>
            <ta e="T769" id="Seg_5469" s="T767">Šaldə deʔ! </ta>
            <ta e="T771" id="Seg_5470" s="T769">Dʼazirdə măna!" </ta>
            <ta e="T775" id="Seg_5471" s="T771">Măn taldʼen tura băzəbiam. </ta>
            <ta e="T779" id="Seg_5472" s="T775">Kuris tüʔpi, tože băzəbiam. </ta>
            <ta e="T782" id="Seg_5473" s="T779">Dĭgəttə крыльцо băzəbiam. </ta>
            <ta e="T787" id="Seg_5474" s="T782">Dĭgəttə kambiam bü tazirzittə multʼanə. </ta>
            <ta e="T792" id="Seg_5475" s="T787">Oʔb ((PAUSE)) muktuʔ aspaʔ deʔpiem. </ta>
            <ta e="T796" id="Seg_5476" s="T792">Măn teinen erten uʔbdəbiam. </ta>
            <ta e="T800" id="Seg_5477" s="T796">Целый (dʼa-) dʼala togonorbiam. </ta>
            <ta e="T802" id="Seg_5478" s="T800">Bar tararluʔpiem. </ta>
            <ta e="T808" id="Seg_5479" s="T802">(Nü- nüdʼin=) Nüdʼin šobi, măn kunolluʔpiam. </ta>
            <ta e="T814" id="Seg_5480" s="T808">Măn dĭzem kusʼtə (nʼe-) nʼe axota. </ta>
            <ta e="T819" id="Seg_5481" s="T814">Dĭzeŋ bar üge (šam-) šamnaʔbəʔjə. </ta>
            <ta e="T821" id="Seg_5482" s="T819">Šamnaʔbəʔjə dĭzeŋ. </ta>
            <ta e="T826" id="Seg_5483" s="T821">Arda (nʼe-) nʼilgösʼtə nʼe axota. </ta>
            <ta e="T831" id="Seg_5484" s="T826">Pušaj măna sʼimandə ej šoləʔjə. </ta>
            <ta e="T835" id="Seg_5485" s="T831">Măna dĭzem ej kereʔ. </ta>
            <ta e="T838" id="Seg_5486" s="T835">Ulum, eʔbdəm, kuzaŋbə. </ta>
            <ta e="T839" id="Seg_5487" s="T838">Tĭmem. </ta>
            <ta e="T841" id="Seg_5488" s="T839">Monzaŋdə, šĭkem. </ta>
            <ta e="T843" id="Seg_5489" s="T841">Sʼimam, püjem. </ta>
            <ta e="T845" id="Seg_5490" s="T843">Udazaŋbə, bögəlbə. </ta>
            <ta e="T847" id="Seg_5491" s="T845">(Sĭj-) Sĭjbə. </ta>
            <ta e="T849" id="Seg_5492" s="T847">Šüjöm, nanəm. </ta>
            <ta e="T853" id="Seg_5493" s="T849">(Uj-) Bögəlbə, kötendə, üjüm. </ta>
            <ta e="T856" id="Seg_5494" s="T853">Aŋdə, (sĭjbə=) sĭjdə. </ta>
            <ta e="T858" id="Seg_5495" s="T857">Nüjüzaŋdə. </ta>
            <ta e="T861" id="Seg_5496" s="T859">(E-) Lezeŋdə. </ta>
            <ta e="T863" id="Seg_5497" s="T862">Kem. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-PKZ">
            <ta e="T5" id="Seg_5498" s="T4">döbər</ta>
            <ta e="T6" id="Seg_5499" s="T5">šo-la-l</ta>
            <ta e="T7" id="Seg_5500" s="T6">dăk</ta>
            <ta e="T8" id="Seg_5501" s="T7">măna</ta>
            <ta e="T9" id="Seg_5502" s="T8">sima</ta>
            <ta e="T10" id="Seg_5503" s="T9">i-ʔ</ta>
            <ta e="T11" id="Seg_5504" s="T10">tăn</ta>
            <ta e="T12" id="Seg_5505" s="T11">büzʼe</ta>
            <ta e="T13" id="Seg_5506" s="T12">gibər</ta>
            <ta e="T14" id="Seg_5507" s="T13">kam-bi</ta>
            <ta e="T15" id="Seg_5508" s="T14">Dʼelam-də</ta>
            <ta e="T869" id="Seg_5509" s="T16">kal-la</ta>
            <ta e="T17" id="Seg_5510" s="T869">dʼür-bi</ta>
            <ta e="T18" id="Seg_5511" s="T17">ine</ta>
            <ta e="T19" id="Seg_5512" s="T18">unnʼa</ta>
            <ta e="T20" id="Seg_5513" s="T19">kam-bi</ta>
            <ta e="T21" id="Seg_5514" s="T20">dʼok</ta>
            <ta e="T22" id="Seg_5515" s="T21">šide-göʔ</ta>
            <ta e="T870" id="Seg_5516" s="T22">kal-la</ta>
            <ta e="T23" id="Seg_5517" s="T870">dʼür-bi-ʔi</ta>
            <ta e="T24" id="Seg_5518" s="T23">bar</ta>
            <ta e="T25" id="Seg_5519" s="T24">kamən</ta>
            <ta e="T27" id="Seg_5520" s="T26">kam-bi-ʔi</ta>
            <ta e="T29" id="Seg_5521" s="T28">taldʼen</ta>
            <ta e="T30" id="Seg_5522" s="T29">kam-bi-ʔi</ta>
            <ta e="T31" id="Seg_5523" s="T30">ugandə</ta>
            <ta e="T32" id="Seg_5524" s="T31">erte-n</ta>
            <ta e="T33" id="Seg_5525" s="T32">ĭmbi</ta>
            <ta e="T34" id="Seg_5526" s="T33">šaːm-naʔbə-l</ta>
            <ta e="T35" id="Seg_5527" s="T34">ej</ta>
            <ta e="T38" id="Seg_5528" s="T37">ej</ta>
            <ta e="T40" id="Seg_5529" s="T39">sĭr-ie-l</ta>
            <ta e="T41" id="Seg_5530" s="T40">köžetsnək-tə</ta>
            <ta e="T43" id="Seg_5531" s="T42">nuldʼ-it</ta>
            <ta e="T44" id="Seg_5532" s="T43">bar</ta>
            <ta e="T45" id="Seg_5533" s="T44">kujnek-əm</ta>
            <ta e="T46" id="Seg_5534" s="T45">nĭŋgə-luʔ-pi</ta>
            <ta e="T47" id="Seg_5535" s="T46">ši</ta>
            <ta e="T48" id="Seg_5536" s="T47">mo-laːm-bi</ta>
            <ta e="T49" id="Seg_5537" s="T48">măn</ta>
            <ta e="T50" id="Seg_5538" s="T49">dĭgəttə</ta>
            <ta e="T51" id="Seg_5539" s="T50">dĭ-m</ta>
            <ta e="T53" id="Seg_5540" s="T52">šödör-bia-m</ta>
            <ta e="T54" id="Seg_5541" s="T53">nʼitka-ʔi-zʼiʔ</ta>
            <ta e="T55" id="Seg_5542" s="T54">nʼimi-zʼiʔ</ta>
            <ta e="T56" id="Seg_5543" s="T55">ĭndak</ta>
            <ta e="T57" id="Seg_5544" s="T56">tʼegermaʔ-gən</ta>
            <ta e="T58" id="Seg_5545" s="T57">bar</ta>
            <ta e="T59" id="Seg_5546" s="T58">küzür-leʔbə</ta>
            <ta e="T60" id="Seg_5547" s="T59">kunguro</ta>
            <ta e="T61" id="Seg_5548" s="T60">nada</ta>
            <ta e="T62" id="Seg_5549" s="T61">kan-zittə</ta>
            <ta e="T63" id="Seg_5550" s="T62">kudaj-də</ta>
            <ta e="T65" id="Seg_5551" s="T64">üzə-sʼtə</ta>
            <ta e="T66" id="Seg_5552" s="T65">dĭgəttə</ta>
            <ta e="T67" id="Seg_5553" s="T66">kăštə-lia-ʔi</ta>
            <ta e="T68" id="Seg_5554" s="T67">dĭ-zeŋ</ta>
            <ta e="T69" id="Seg_5555" s="T68">bar</ta>
            <ta e="T70" id="Seg_5556" s="T69">edə-t</ta>
            <ta e="T71" id="Seg_5557" s="T70">ine-nə</ta>
            <ta e="T72" id="Seg_5558" s="T71">koŋgoro</ta>
            <ta e="T73" id="Seg_5559" s="T72">sagəš-də</ta>
            <ta e="T74" id="Seg_5560" s="T73">naga</ta>
            <ta e="T75" id="Seg_5561" s="T74">bar</ta>
            <ta e="T76" id="Seg_5562" s="T75">ĭmbi=də</ta>
            <ta e="T77" id="Seg_5563" s="T76">ej</ta>
            <ta e="T78" id="Seg_5564" s="T77">tĭmne-m</ta>
            <ta e="T79" id="Seg_5565" s="T78">ĭmbi=də</ta>
            <ta e="T80" id="Seg_5566" s="T79">ej</ta>
            <ta e="T81" id="Seg_5567" s="T80">mo-lia-m</ta>
            <ta e="T82" id="Seg_5568" s="T81">nörbə-sʼtə</ta>
            <ta e="T83" id="Seg_5569" s="T82">bar</ta>
            <ta e="T84" id="Seg_5570" s="T83">sagəš-də</ta>
            <ta e="T85" id="Seg_5571" s="T84">naga</ta>
            <ta e="T86" id="Seg_5572" s="T85">dʼije-nə</ta>
            <ta e="T87" id="Seg_5573" s="T86">kam-bia-m</ta>
            <ta e="T88" id="Seg_5574" s="T87">bar</ta>
            <ta e="T92" id="Seg_5575" s="T91">tʼür-leʔbə</ta>
            <ta e="T93" id="Seg_5576" s="T92">dʼije-nə</ta>
            <ta e="T94" id="Seg_5577" s="T93">kam-bia-m</ta>
            <ta e="T95" id="Seg_5578" s="T94">bar</ta>
            <ta e="T96" id="Seg_5579" s="T95">dʼür-leʔ-pie-m</ta>
            <ta e="T97" id="Seg_5580" s="T96">dĭgəttə</ta>
            <ta e="T98" id="Seg_5581" s="T97">kirgar-bia-m</ta>
            <ta e="T99" id="Seg_5582" s="T98">bar</ta>
            <ta e="T100" id="Seg_5583" s="T99">dĭgəttə</ta>
            <ta e="T101" id="Seg_5584" s="T100">šo-bia-m</ta>
            <ta e="T102" id="Seg_5585" s="T101">kondʼo</ta>
            <ta e="T103" id="Seg_5586" s="T102">amno-bi</ta>
            <ta e="T104" id="Seg_5587" s="T103">ĭmbi=də</ta>
            <ta e="T106" id="Seg_5588" s="T104">bar</ta>
            <ta e="T107" id="Seg_5589" s="T106">ĭmbi=də</ta>
            <ta e="T108" id="Seg_5590" s="T107">ej</ta>
            <ta e="T109" id="Seg_5591" s="T108">dʼăbaktər-bi</ta>
            <ta e="T110" id="Seg_5592" s="T109">dĭ</ta>
            <ta e="T111" id="Seg_5593" s="T110">kandə-ga</ta>
            <ta e="T112" id="Seg_5594" s="T111">a</ta>
            <ta e="T113" id="Seg_5595" s="T112">măn</ta>
            <ta e="T114" id="Seg_5596" s="T113">dĭ-m</ta>
            <ta e="T116" id="Seg_5597" s="T115">bĭd-lie-m</ta>
            <ta e="T117" id="Seg_5598" s="T116">i-ʔ</ta>
            <ta e="T118" id="Seg_5599" s="T117">kan-a-ʔ</ta>
            <ta e="T119" id="Seg_5600" s="T118">par-a-ʔ</ta>
            <ta e="T120" id="Seg_5601" s="T119">döbər</ta>
            <ta e="T121" id="Seg_5602" s="T120">măn</ta>
            <ta e="T122" id="Seg_5603" s="T121">tănan</ta>
            <ta e="T123" id="Seg_5604" s="T122">măn-lia-m</ta>
            <ta e="T124" id="Seg_5605" s="T123">par-a-ʔ</ta>
            <ta e="T125" id="Seg_5606" s="T124">döbər</ta>
            <ta e="T126" id="Seg_5607" s="T125">măn</ta>
            <ta e="T127" id="Seg_5608" s="T126">ugandə</ta>
            <ta e="T128" id="Seg_5609" s="T127">urgo</ta>
            <ta e="T129" id="Seg_5610" s="T128">il</ta>
            <ta e="T130" id="Seg_5611" s="T129">i-ge</ta>
            <ta e="T131" id="Seg_5612" s="T130">ipek</ta>
            <ta e="T132" id="Seg_5613" s="T131">iʔgö</ta>
            <ta e="T134" id="Seg_5614" s="T133">kereʔ</ta>
            <ta e="T135" id="Seg_5615" s="T134">i-ʔ</ta>
            <ta e="T136" id="Seg_5616" s="T135">bögəl-də</ta>
            <ta e="T137" id="Seg_5617" s="T136">dĭ-m</ta>
            <ta e="T138" id="Seg_5618" s="T137">uda-l</ta>
            <ta e="T139" id="Seg_5619" s="T138">ej</ta>
            <ta e="T140" id="Seg_5620" s="T139">en-də</ta>
            <ta e="T142" id="Seg_5621" s="T140">ižəm-də</ta>
            <ta e="T143" id="Seg_5622" s="T142">dĭ-m</ta>
            <ta e="T144" id="Seg_5623" s="T143">ižəm-də</ta>
            <ta e="T145" id="Seg_5624" s="T144">dĭ-m</ta>
            <ta e="T146" id="Seg_5625" s="T145">uda-zi</ta>
            <ta e="T148" id="Seg_5626" s="T146">bos-kənə</ta>
            <ta e="T149" id="Seg_5627" s="T148">iʔgö</ta>
            <ta e="T150" id="Seg_5628" s="T149">i-bie-m</ta>
            <ta e="T151" id="Seg_5629" s="T150">a</ta>
            <ta e="T152" id="Seg_5630" s="T151">dĭʔ-nə</ta>
            <ta e="T153" id="Seg_5631" s="T152">amga-m</ta>
            <ta e="T154" id="Seg_5632" s="T153">i-bie-m</ta>
            <ta e="T155" id="Seg_5633" s="T154">bos-kəndə</ta>
            <ta e="T156" id="Seg_5634" s="T155">iʔgö</ta>
            <ta e="T157" id="Seg_5635" s="T156">i-lie</ta>
            <ta e="T158" id="Seg_5636" s="T157">a</ta>
            <ta e="T159" id="Seg_5637" s="T158">dĭʔ-nə</ta>
            <ta e="T160" id="Seg_5638" s="T159">amga-m</ta>
            <ta e="T161" id="Seg_5639" s="T160">i-lie</ta>
            <ta e="T162" id="Seg_5640" s="T161">muktuʔ</ta>
            <ta e="T163" id="Seg_5641" s="T162">bar</ta>
            <ta e="T164" id="Seg_5642" s="T163">dʼăbaktər-bi-baʔ</ta>
            <ta e="T165" id="Seg_5643" s="T164">miʔ</ta>
            <ta e="T166" id="Seg_5644" s="T165">bar</ta>
            <ta e="T167" id="Seg_5645" s="T166">üdʼüge</ta>
            <ta e="T168" id="Seg_5646" s="T167">i-bi-beʔ</ta>
            <ta e="T169" id="Seg_5647" s="T168">bar</ta>
            <ta e="T170" id="Seg_5648" s="T169">sʼar-laʔ-pi-baʔ</ta>
            <ta e="T171" id="Seg_5649" s="T170">pa-ʔi</ta>
            <ta e="T172" id="Seg_5650" s="T171">i-lə-j</ta>
            <ta e="T173" id="Seg_5651" s="T172">da</ta>
            <ta e="T174" id="Seg_5652" s="T173">bar</ta>
            <ta e="T175" id="Seg_5653" s="T174">oj</ta>
            <ta e="T176" id="Seg_5654" s="T175">onʼiʔ</ta>
            <ta e="T177" id="Seg_5655" s="T176">nuʔmə-lə-j</ta>
            <ta e="T178" id="Seg_5656" s="T177">a</ta>
            <ta e="T179" id="Seg_5657" s="T178">miʔ</ta>
            <ta e="T180" id="Seg_5658" s="T179">bar</ta>
            <ta e="T181" id="Seg_5659" s="T180">ša-laːm-bi-beʔ</ta>
            <ta e="T182" id="Seg_5660" s="T181">dĭgəttə</ta>
            <ta e="T183" id="Seg_5661" s="T182">dĭ</ta>
            <ta e="T184" id="Seg_5662" s="T183">bar</ta>
            <ta e="T185" id="Seg_5663" s="T184">ku-lia</ta>
            <ta e="T186" id="Seg_5664" s="T185">ku-lia</ta>
            <ta e="T187" id="Seg_5665" s="T186">ej</ta>
            <ta e="T189" id="Seg_5666" s="T188">ej</ta>
            <ta e="T190" id="Seg_5667" s="T189">mo-lia</ta>
            <ta e="T191" id="Seg_5668" s="T190">ku-zittə</ta>
            <ta e="T192" id="Seg_5669" s="T191">miʔ</ta>
            <ta e="T193" id="Seg_5670" s="T192">bar</ta>
            <ta e="T194" id="Seg_5671" s="T193">bazoʔ</ta>
            <ta e="T195" id="Seg_5672" s="T194">sar-laʔbə-baʔ</ta>
            <ta e="T197" id="Seg_5673" s="T196">miʔ</ta>
            <ta e="T198" id="Seg_5674" s="T197">bar</ta>
            <ta e="T199" id="Seg_5675" s="T198">ej</ta>
            <ta e="T200" id="Seg_5676" s="T199">kudonz-luʔ-pi-baʔ</ta>
            <ta e="T201" id="Seg_5677" s="T200">jakšə</ta>
            <ta e="T203" id="Seg_5678" s="T201">bar</ta>
            <ta e="T205" id="Seg_5679" s="T204">jakšə</ta>
            <ta e="T207" id="Seg_5680" s="T205">bar</ta>
            <ta e="T210" id="Seg_5681" s="T209">miʔ</ta>
            <ta e="T211" id="Seg_5682" s="T210">bar</ta>
            <ta e="T212" id="Seg_5683" s="T211">jakšə</ta>
            <ta e="T213" id="Seg_5684" s="T212">sʼar-bi-baʔ</ta>
            <ta e="T214" id="Seg_5685" s="T213">miʔ</ta>
            <ta e="T215" id="Seg_5686" s="T214">ej</ta>
            <ta e="T216" id="Seg_5687" s="T215">kurol-lu-bi-baʔ</ta>
            <ta e="T217" id="Seg_5688" s="T216">tibi-zeŋ</ta>
            <ta e="T218" id="Seg_5689" s="T217">i</ta>
            <ta e="T219" id="Seg_5690" s="T218">ne-zeŋ</ta>
            <ta e="T220" id="Seg_5691" s="T219">amno-bi-ʔi</ta>
            <ta e="T221" id="Seg_5692" s="T220">da</ta>
            <ta e="T222" id="Seg_5693" s="T221">măndo-laʔ-pi-ʔi</ta>
            <ta e="T223" id="Seg_5694" s="T222">kăde</ta>
            <ta e="T224" id="Seg_5695" s="T223">miʔ</ta>
            <ta e="T225" id="Seg_5696" s="T224">sʼar-laʔbə-baʔ</ta>
            <ta e="T226" id="Seg_5697" s="T225">šindi</ta>
            <ta e="T229" id="Seg_5698" s="T228">nʼeʔ-tə-bi</ta>
            <ta e="T230" id="Seg_5699" s="T229">taŋgo</ta>
            <ta e="T231" id="Seg_5700" s="T230">šindi</ta>
            <ta e="T232" id="Seg_5701" s="T231">ej</ta>
            <ta e="T233" id="Seg_5702" s="T232">nʼeʔ-tə-bi</ta>
            <ta e="T234" id="Seg_5703" s="T233">šindi</ta>
            <ta e="T235" id="Seg_5704" s="T234">bar</ta>
            <ta e="T237" id="Seg_5705" s="T236">kanza-si</ta>
            <ta e="T238" id="Seg_5706" s="T237">bar</ta>
            <ta e="T239" id="Seg_5707" s="T238">nʼeʔ-leʔbə</ta>
            <ta e="T240" id="Seg_5708" s="T239">šindi</ta>
            <ta e="T241" id="Seg_5709" s="T240">ej</ta>
            <ta e="T242" id="Seg_5710" s="T241">nʼeʔ-leʔbə</ta>
            <ta e="T243" id="Seg_5711" s="T242">ular</ta>
            <ta e="T244" id="Seg_5712" s="T243">ugaːndə</ta>
            <ta e="T245" id="Seg_5713" s="T244">iʔgö</ta>
            <ta e="T247" id="Seg_5714" s="T246">šide</ta>
            <ta e="T248" id="Seg_5715" s="T247">bit</ta>
            <ta e="T249" id="Seg_5716" s="T248">šide</ta>
            <ta e="T250" id="Seg_5717" s="T249">aba-m</ta>
            <ta e="T251" id="Seg_5718" s="T250">dʼije-nə</ta>
            <ta e="T252" id="Seg_5719" s="T251">kam-bi</ta>
            <ta e="T253" id="Seg_5720" s="T252">iʔgö</ta>
            <ta e="T254" id="Seg_5721" s="T253">kurizə-ʔi</ta>
            <ta e="T255" id="Seg_5722" s="T254">kuʔ-pi</ta>
            <ta e="T256" id="Seg_5723" s="T255">i</ta>
            <ta e="T257" id="Seg_5724" s="T256">ma-ndə</ta>
            <ta e="T258" id="Seg_5725" s="T257">deʔ-pi</ta>
            <ta e="T259" id="Seg_5726" s="T258">măn</ta>
            <ta e="T260" id="Seg_5727" s="T259">šonə-ga-m</ta>
            <ta e="T261" id="Seg_5728" s="T260">Permʼakovo</ta>
            <ta e="T262" id="Seg_5729" s="T261">ku-lio-m</ta>
            <ta e="T263" id="Seg_5730" s="T262">bar</ta>
            <ta e="T264" id="Seg_5731" s="T263">bulan</ta>
            <ta e="T265" id="Seg_5732" s="T264">nu-ga</ta>
            <ta e="T266" id="Seg_5733" s="T265">măn</ta>
            <ta e="T267" id="Seg_5734" s="T266">măndo-bia-m</ta>
            <ta e="T268" id="Seg_5735" s="T267">măndo-bia-m</ta>
            <ta e="T269" id="Seg_5736" s="T268">dĭgəttə</ta>
            <ta e="T270" id="Seg_5737" s="T269">bar</ta>
            <ta e="T271" id="Seg_5738" s="T270">kirgar-luʔ-pi-m</ta>
            <ta e="T272" id="Seg_5739" s="T271">dĭ</ta>
            <ta e="T273" id="Seg_5740" s="T272">bar</ta>
            <ta e="T274" id="Seg_5741" s="T273">nuʔmə-luʔ-pi</ta>
            <ta e="T275" id="Seg_5742" s="T274">bü-nə</ta>
            <ta e="T276" id="Seg_5743" s="T275">penzi</ta>
            <ta e="T277" id="Seg_5744" s="T276">dĭgəttə</ta>
            <ta e="T278" id="Seg_5745" s="T277">dĭ</ta>
            <ta e="T279" id="Seg_5746" s="T278">bar</ta>
            <ta e="T280" id="Seg_5747" s="T279">ka-luʔ-pi</ta>
            <ta e="T281" id="Seg_5748" s="T280">i</ta>
            <ta e="T282" id="Seg_5749" s="T281">nuʔmə-luʔ-pi</ta>
            <ta e="T283" id="Seg_5750" s="T282">măn</ta>
            <ta e="T284" id="Seg_5751" s="T283">šo-bia-m</ta>
            <ta e="T285" id="Seg_5752" s="T284">da</ta>
            <ta e="T286" id="Seg_5753" s="T285">Vlas-tə</ta>
            <ta e="T287" id="Seg_5754" s="T286">nörbə-bie-m</ta>
            <ta e="T288" id="Seg_5755" s="T287">dĭ</ta>
            <ta e="T289" id="Seg_5756" s="T288">kam-bi</ta>
            <ta e="T290" id="Seg_5757" s="T289">multuk-si</ta>
            <ta e="T291" id="Seg_5758" s="T290">da</ta>
            <ta e="T293" id="Seg_5759" s="T292">kuʔ-pi</ta>
            <ta e="T295" id="Seg_5760" s="T293">aspaʔ</ta>
            <ta e="T296" id="Seg_5761" s="T295">măn</ta>
            <ta e="T297" id="Seg_5762" s="T296">dĭgəttə</ta>
            <ta e="T298" id="Seg_5763" s="T297">uja</ta>
            <ta e="T299" id="Seg_5764" s="T298">băzə-bia-m</ta>
            <ta e="T300" id="Seg_5765" s="T299">i</ta>
            <ta e="T301" id="Seg_5766" s="T300">am-bia-m</ta>
            <ta e="T302" id="Seg_5767" s="T301">miʔnʼibeʔ</ta>
            <ta e="T303" id="Seg_5768" s="T302">mĭ-bi</ta>
            <ta e="T304" id="Seg_5769" s="T303">onʼiʔ</ta>
            <ta e="T305" id="Seg_5770" s="T304">aspaʔ</ta>
            <ta e="T306" id="Seg_5771" s="T866">kuza</ta>
            <ta e="T307" id="Seg_5772" s="T306">šo-bi</ta>
            <ta e="T308" id="Seg_5773" s="T307">măn</ta>
            <ta e="T309" id="Seg_5774" s="T308">dibər</ta>
            <ta e="T310" id="Seg_5775" s="T309">mĭm-bie-m</ta>
            <ta e="T311" id="Seg_5776" s="T310">dĭ-zeŋ</ta>
            <ta e="T312" id="Seg_5777" s="T311">bar</ta>
            <ta e="T314" id="Seg_5778" s="T312">dʼăbaktər-laʔbə-ʔjə</ta>
            <ta e="T315" id="Seg_5779" s="T314">a</ta>
            <ta e="T316" id="Seg_5780" s="T315">dĭ-zeŋ</ta>
            <ta e="T317" id="Seg_5781" s="T316">bar</ta>
            <ta e="T318" id="Seg_5782" s="T317">măna</ta>
            <ta e="T319" id="Seg_5783" s="T318">sürer-luʔ-pi-ʔjə</ta>
            <ta e="T320" id="Seg_5784" s="T319">măn</ta>
            <ta e="T321" id="Seg_5785" s="T320">nuʔmə-luʔ-pie-m</ta>
            <ta e="T323" id="Seg_5786" s="T321">unnʼa</ta>
            <ta e="T324" id="Seg_5787" s="T323">nu-zaŋ</ta>
            <ta e="T325" id="Seg_5788" s="T324">amno-bi-ʔi</ta>
            <ta e="T326" id="Seg_5789" s="T325">jakšə</ta>
            <ta e="T327" id="Seg_5790" s="T326">i-bi</ta>
            <ta e="T328" id="Seg_5791" s="T327">uja-t</ta>
            <ta e="T329" id="Seg_5792" s="T328">iʔgö</ta>
            <ta e="T330" id="Seg_5793" s="T329">i-bi</ta>
            <ta e="T331" id="Seg_5794" s="T330">bar</ta>
            <ta e="T332" id="Seg_5795" s="T331">am-nia-l</ta>
            <ta e="T333" id="Seg_5796" s="T332">ălenʼ-ə-n</ta>
            <ta e="T334" id="Seg_5797" s="T333">uja</ta>
            <ta e="T335" id="Seg_5798" s="T334">ugandə</ta>
            <ta e="T336" id="Seg_5799" s="T335">nömər</ta>
            <ta e="T337" id="Seg_5800" s="T336">kak</ta>
            <ta e="T338" id="Seg_5801" s="T337">ipek</ta>
            <ta e="T339" id="Seg_5802" s="T338">iʔgö</ta>
            <ta e="T340" id="Seg_5803" s="T339">am-nia-l</ta>
            <ta e="T341" id="Seg_5804" s="T340">dĭgəttə</ta>
            <ta e="T342" id="Seg_5805" s="T341">bü</ta>
            <ta e="T343" id="Seg_5806" s="T342">bĭt-lie-l</ta>
            <ta e="T344" id="Seg_5807" s="T865">bü</ta>
            <ta e="T345" id="Seg_5808" s="T344">bĭt-lie-l</ta>
            <ta e="T348" id="Seg_5809" s="T345">nu</ta>
            <ta e="T349" id="Seg_5810" s="T348">dĭ</ta>
            <ta e="T350" id="Seg_5811" s="T349">koŋ</ta>
            <ta e="T351" id="Seg_5812" s="T350">ugandə</ta>
            <ta e="T352" id="Seg_5813" s="T351">jakšə</ta>
            <ta e="T353" id="Seg_5814" s="T352">ugandə</ta>
            <ta e="T354" id="Seg_5815" s="T353">kuvas</ta>
            <ta e="T355" id="Seg_5816" s="T354">sʼima-t</ta>
            <ta e="T357" id="Seg_5817" s="T356">sagər</ta>
            <ta e="T358" id="Seg_5818" s="T357">püje-t</ta>
            <ta e="T359" id="Seg_5819" s="T358">ej</ta>
            <ta e="T360" id="Seg_5820" s="T359">numo</ta>
            <ta e="T361" id="Seg_5821" s="T360">mon-zaŋ-də</ta>
            <ta e="T362" id="Seg_5822" s="T361">kuvas</ta>
            <ta e="T363" id="Seg_5823" s="T362">bar</ta>
            <ta e="T364" id="Seg_5824" s="T363">mon-zaŋ-də</ta>
            <ta e="T365" id="Seg_5825" s="T364">ej</ta>
            <ta e="T366" id="Seg_5826" s="T365">nʼešpək</ta>
            <ta e="T368" id="Seg_5827" s="T367">bar</ta>
            <ta e="T369" id="Seg_5828" s="T368">ĭzem-neʔ-pi</ta>
            <ta e="T370" id="Seg_5829" s="T369">măn</ta>
            <ta e="T372" id="Seg_5830" s="T371">dĭʔ-nə</ta>
            <ta e="T374" id="Seg_5831" s="T373">ka-la-m</ta>
            <ta e="T375" id="Seg_5832" s="T374">dĭ</ta>
            <ta e="T376" id="Seg_5833" s="T375">bar</ta>
            <ta e="T377" id="Seg_5834" s="T376">măn-də</ta>
            <ta e="T378" id="Seg_5835" s="T377">i-ʔ</ta>
            <ta e="T379" id="Seg_5836" s="T378">dʼăbaktər-a-ʔ</ta>
            <ta e="T380" id="Seg_5837" s="T379">kaza-n</ta>
            <ta e="T381" id="Seg_5838" s="T380">šĭkə-t-si</ta>
            <ta e="T382" id="Seg_5839" s="T381">a</ta>
            <ta e="T383" id="Seg_5840" s="T382">bos-tə</ta>
            <ta e="T384" id="Seg_5841" s="T383">šĭkə-t-si</ta>
            <ta e="T385" id="Seg_5842" s="T384">dʼăbaktər-a-ʔ</ta>
            <ta e="T386" id="Seg_5843" s="T385">măn</ta>
            <ta e="T387" id="Seg_5844" s="T386">ugandə</ta>
            <ta e="T388" id="Seg_5845" s="T387">ĭzem-nie-m</ta>
            <ta e="T389" id="Seg_5846" s="T388">bar</ta>
            <ta e="T390" id="Seg_5847" s="T389">ugandə</ta>
            <ta e="T391" id="Seg_5848" s="T390">ĭzem-nie-m</ta>
            <ta e="T392" id="Seg_5849" s="T391">bar</ta>
            <ta e="T393" id="Seg_5850" s="T392">măn</ta>
            <ta e="T394" id="Seg_5851" s="T393">bar</ta>
            <ta e="T395" id="Seg_5852" s="T394">kü-laːm-bia-m</ta>
            <ta e="T396" id="Seg_5853" s="T395">kü-laːm-bia-m</ta>
            <ta e="T397" id="Seg_5854" s="T396">a</ta>
            <ta e="T398" id="Seg_5855" s="T397">tăn</ta>
            <ta e="T399" id="Seg_5856" s="T398">ma-lə-l</ta>
            <ta e="T400" id="Seg_5857" s="T399">dĭ</ta>
            <ta e="T401" id="Seg_5858" s="T400">kü-laːm-bi</ta>
            <ta e="T402" id="Seg_5859" s="T401">šide</ta>
            <ta e="T403" id="Seg_5860" s="T402">koʔbdo-t</ta>
            <ta e="T405" id="Seg_5861" s="T404">ma-luʔ-pi-ʔi</ta>
            <ta e="T406" id="Seg_5862" s="T405">onʼiʔ</ta>
            <ta e="T407" id="Seg_5863" s="T406">dĭ-zi</ta>
            <ta e="T408" id="Seg_5864" s="T407">i-bi</ta>
            <ta e="T409" id="Seg_5865" s="T408">a</ta>
            <ta e="T410" id="Seg_5866" s="T409">onʼiʔ</ta>
            <ta e="T411" id="Seg_5867" s="T410">kuŋgə</ta>
            <ta e="T412" id="Seg_5868" s="T411">i-bi</ta>
            <ta e="T413" id="Seg_5869" s="T412">teinen</ta>
            <ta e="T415" id="Seg_5870" s="T413">bar</ta>
            <ta e="T420" id="Seg_5871" s="T419">teinen</ta>
            <ta e="T421" id="Seg_5872" s="T420">selɨj</ta>
            <ta e="T422" id="Seg_5873" s="T421">dʼala-t</ta>
            <ta e="T423" id="Seg_5874" s="T422">surno</ta>
            <ta e="T424" id="Seg_5875" s="T423">šo-bi</ta>
            <ta e="T425" id="Seg_5876" s="T424">a</ta>
            <ta e="T426" id="Seg_5877" s="T425">tüj</ta>
            <ta e="T427" id="Seg_5878" s="T426">kujo</ta>
            <ta e="T428" id="Seg_5879" s="T427">măndo-laʔbǝ</ta>
            <ta e="T429" id="Seg_5880" s="T428">Novik</ta>
            <ta e="T430" id="Seg_5881" s="T429">šonə-ga</ta>
            <ta e="T431" id="Seg_5882" s="T430">šoška-ndə</ta>
            <ta e="T432" id="Seg_5883" s="T431">a</ta>
            <ta e="T433" id="Seg_5884" s="T432">Sidor</ta>
            <ta e="T434" id="Seg_5885" s="T433">men-də</ta>
            <ta e="T435" id="Seg_5886" s="T434">amno-laʔbə</ta>
            <ta e="T436" id="Seg_5887" s="T435">a</ta>
            <ta e="T437" id="Seg_5888" s="T436">il</ta>
            <ta e="T438" id="Seg_5889" s="T437">ku-bi-ʔi</ta>
            <ta e="T439" id="Seg_5890" s="T438">tenə-bi-ʔi</ta>
            <ta e="T441" id="Seg_5891" s="T440">predsʼedatelʼ</ta>
            <ta e="T442" id="Seg_5892" s="T441">măm-bi</ta>
            <ta e="T443" id="Seg_5893" s="T442">nada</ta>
            <ta e="T444" id="Seg_5894" s="T443">tʼermən-də</ta>
            <ta e="T445" id="Seg_5895" s="T444">adʼazir-zittə</ta>
            <ta e="T446" id="Seg_5896" s="T445">jakšə</ta>
            <ta e="T447" id="Seg_5897" s="T446">il</ta>
            <ta e="T448" id="Seg_5898" s="T447">oʔbdə-sʼtə</ta>
            <ta e="T451" id="Seg_5899" s="T450">puskaj</ta>
            <ta e="T452" id="Seg_5900" s="T451">dĭn</ta>
            <ta e="T453" id="Seg_5901" s="T452">togonor-la-ʔi</ta>
            <ta e="T454" id="Seg_5902" s="T453">nada</ta>
            <ta e="T455" id="Seg_5903" s="T454">tʼermən-də</ta>
            <ta e="T456" id="Seg_5904" s="T455">kan-zittə</ta>
            <ta e="T457" id="Seg_5905" s="T456">un</ta>
            <ta e="T458" id="Seg_5906" s="T457">nʼeʔ-sittə</ta>
            <ta e="T459" id="Seg_5907" s="T458">ato</ta>
            <ta e="T460" id="Seg_5908" s="T459">un</ta>
            <ta e="T461" id="Seg_5909" s="T460">naga</ta>
            <ta e="T463" id="Seg_5910" s="T462">măn</ta>
            <ta e="T464" id="Seg_5911" s="T463">üdʼüge</ta>
            <ta e="T465" id="Seg_5912" s="T464">i-bie-m</ta>
            <ta e="T466" id="Seg_5913" s="T465">a</ta>
            <ta e="T467" id="Seg_5914" s="T466">koʔb-sa-m</ta>
            <ta e="T468" id="Seg_5915" s="T467">urgo</ta>
            <ta e="T469" id="Seg_5916" s="T468">i-bi-ʔi</ta>
            <ta e="T470" id="Seg_5917" s="T469">uge</ta>
            <ta e="T471" id="Seg_5918" s="T470">măna</ta>
            <ta e="T472" id="Seg_5919" s="T471">i-bi-ʔi</ta>
            <ta e="T473" id="Seg_5920" s="T472">bos-kəndə</ta>
            <ta e="T474" id="Seg_5921" s="T473">nüjnə</ta>
            <ta e="T475" id="Seg_5922" s="T474">nüjnə-sʼtə</ta>
            <ta e="T476" id="Seg_5923" s="T475">măna</ta>
            <ta e="T480" id="Seg_5924" s="T479">kuvas</ta>
            <ta e="T481" id="Seg_5925" s="T480">i-bi</ta>
            <ta e="T482" id="Seg_5926" s="T481">nüjnə-bie-m</ta>
            <ta e="T483" id="Seg_5927" s="T482">bar</ta>
            <ta e="T484" id="Seg_5928" s="T483">dĭ</ta>
            <ta e="T485" id="Seg_5929" s="T484">kuza</ta>
            <ta e="T486" id="Seg_5930" s="T485">ugandə</ta>
            <ta e="T487" id="Seg_5931" s="T486">jakšə</ta>
            <ta e="T488" id="Seg_5932" s="T487">nüj-lie</ta>
            <ta e="T492" id="Seg_5933" s="T491">ugandə</ta>
            <ta e="T493" id="Seg_5934" s="T492">kuvas</ta>
            <ta e="T501" id="Seg_5935" s="T500">ugandə</ta>
            <ta e="T502" id="Seg_5936" s="T501">tăŋ</ta>
            <ta e="T503" id="Seg_5937" s="T502">nüj-leʔbə</ta>
            <ta e="T504" id="Seg_5938" s="T503">dĭ</ta>
            <ta e="T505" id="Seg_5939" s="T504">kuza</ta>
            <ta e="T506" id="Seg_5940" s="T505">kü-laːm-bi</ta>
            <ta e="T507" id="Seg_5941" s="T506">nada</ta>
            <ta e="T508" id="Seg_5942" s="T507">maʔ-tə</ta>
            <ta e="T509" id="Seg_5943" s="T508">a-zittə</ta>
            <ta e="T510" id="Seg_5944" s="T509">pa</ta>
            <ta e="T511" id="Seg_5945" s="T510">pa-zi</ta>
            <ta e="T512" id="Seg_5946" s="T511">pa-gə</ta>
            <ta e="T513" id="Seg_5947" s="T512">i</ta>
            <ta e="T514" id="Seg_5948" s="T513">dibər</ta>
            <ta e="T515" id="Seg_5949" s="T514">dĭ-m</ta>
            <ta e="T516" id="Seg_5950" s="T515">en-zittə</ta>
            <ta e="T517" id="Seg_5951" s="T516">i</ta>
            <ta e="T518" id="Seg_5952" s="T517">kros</ta>
            <ta e="T519" id="Seg_5953" s="T518">a-zittə</ta>
            <ta e="T520" id="Seg_5954" s="T519">dĭn</ta>
            <ta e="T521" id="Seg_5955" s="T520">tüj</ta>
            <ta e="T522" id="Seg_5956" s="T521">ka-lə-j</ta>
            <ta e="T523" id="Seg_5957" s="T522">šü</ta>
            <ta e="T524" id="Seg_5958" s="T523">dʼü-nə</ta>
            <ta e="T525" id="Seg_5959" s="T524">ugandə</ta>
            <ta e="T526" id="Seg_5960" s="T525">kuvas</ta>
            <ta e="T527" id="Seg_5961" s="T526">tura-ʔi</ta>
            <ta e="T528" id="Seg_5962" s="T527">nu-ga-ʔi</ta>
            <ta e="T529" id="Seg_5963" s="T528">dĭ-zeŋ</ta>
            <ta e="T530" id="Seg_5964" s="T529">dĭ-zeŋ</ta>
            <ta e="T533" id="Seg_5965" s="T532">pi-ʔi</ta>
            <ta e="T535" id="Seg_5966" s="T534">em-bi-ʔi</ta>
            <ta e="T536" id="Seg_5967" s="T535">kuznek</ta>
            <ta e="T537" id="Seg_5968" s="T536">kuznek-ə-n</ta>
            <ta e="T538" id="Seg_5969" s="T537">to-ndə</ta>
            <ta e="T539" id="Seg_5970" s="T538">bar</ta>
            <ta e="T540" id="Seg_5971" s="T539">pa-ʔi</ta>
            <ta e="T542" id="Seg_5972" s="T541">amnol-bi-ʔi</ta>
            <ta e="T543" id="Seg_5973" s="T542">pa-ʔi</ta>
            <ta e="T544" id="Seg_5974" s="T543">amnol-bi-ʔi</ta>
            <ta e="T546" id="Seg_5975" s="T545">amnol-bi-ʔi</ta>
            <ta e="T547" id="Seg_5976" s="T546">ugandə</ta>
            <ta e="T548" id="Seg_5977" s="T547">kuvas</ta>
            <ta e="T549" id="Seg_5978" s="T548">tura</ta>
            <ta e="T552" id="Seg_5979" s="T551">lem</ta>
            <ta e="T553" id="Seg_5980" s="T552">keʔbde</ta>
            <ta e="T554" id="Seg_5981" s="T553">bar</ta>
            <ta e="T555" id="Seg_5982" s="T554">amnol-bi-ʔi</ta>
            <ta e="T867" id="Seg_5983" s="T555">lʼevăj</ta>
            <ta e="T557" id="Seg_5984" s="T556">uda-nə</ta>
            <ta e="T558" id="Seg_5985" s="T557">kuvas</ta>
            <ta e="T559" id="Seg_5986" s="T558">i</ta>
            <ta e="T560" id="Seg_5987" s="T559">pravăj</ta>
            <ta e="T561" id="Seg_5988" s="T560">uda-nə</ta>
            <ta e="T563" id="Seg_5989" s="T561">kuvas</ta>
            <ta e="T567" id="Seg_5990" s="T566">ugandə</ta>
            <ta e="T568" id="Seg_5991" s="T567">kuvas</ta>
            <ta e="T570" id="Seg_5992" s="T569">tura-ʔi</ta>
            <ta e="T571" id="Seg_5993" s="T570">nu-ga-ʔi</ta>
            <ta e="T572" id="Seg_5994" s="T571">măn</ta>
            <ta e="T573" id="Seg_5995" s="T572">mej-əm</ta>
            <ta e="T575" id="Seg_5996" s="T574">mĭnzər-bi</ta>
            <ta e="T576" id="Seg_5997" s="T575">dĭ</ta>
            <ta e="T577" id="Seg_5998" s="T576">bar</ta>
            <ta e="T578" id="Seg_5999" s="T577">ej</ta>
            <ta e="T579" id="Seg_6000" s="T578">nʼeʔdə</ta>
            <ta e="T580" id="Seg_6001" s="T579">kuza</ta>
            <ta e="T581" id="Seg_6002" s="T580">bar</ta>
            <ta e="T582" id="Seg_6003" s="T581">kudo-nz-laʔbə</ta>
            <ta e="T583" id="Seg_6004" s="T582">daška</ta>
            <ta e="T584" id="Seg_6005" s="T583">ej</ta>
            <ta e="T585" id="Seg_6006" s="T584">i-lə-l</ta>
            <ta e="T586" id="Seg_6007" s="T585">mĭnzər-zittə</ta>
            <ta e="T587" id="Seg_6008" s="T586">ej</ta>
            <ta e="T588" id="Seg_6009" s="T587">jakšə</ta>
            <ta e="T589" id="Seg_6010" s="T588">ej</ta>
            <ta e="T590" id="Seg_6011" s="T589">nʼeʔdə</ta>
            <ta e="T591" id="Seg_6012" s="T590">dĭgəttə</ta>
            <ta e="T592" id="Seg_6013" s="T591">dĭ</ta>
            <ta e="T593" id="Seg_6014" s="T592">mej-əm</ta>
            <ta e="T595" id="Seg_6015" s="T594">bü-m</ta>
            <ta e="T596" id="Seg_6016" s="T595">mĭnzər-bi</ta>
            <ta e="T597" id="Seg_6017" s="T596">tus-si</ta>
            <ta e="T598" id="Seg_6018" s="T597">dĭgəttə</ta>
            <ta e="T600" id="Seg_6019" s="T599">uja</ta>
            <ta e="T601" id="Seg_6020" s="T600">jaʔ-pi</ta>
            <ta e="T602" id="Seg_6021" s="T601">i</ta>
            <ta e="T603" id="Seg_6022" s="T602">dibər</ta>
            <ta e="T604" id="Seg_6023" s="T603">en-də-ge</ta>
            <ta e="T605" id="Seg_6024" s="T604">štobɨ</ta>
            <ta e="T606" id="Seg_6025" s="T605">tustʼar-laʔbə</ta>
            <ta e="T607" id="Seg_6026" s="T606">eje-t</ta>
            <ta e="T608" id="Seg_6027" s="T607">ej-e-n</ta>
            <ta e="T609" id="Seg_6028" s="T608">ne</ta>
            <ta e="T610" id="Seg_6029" s="T609">kan-zittə</ta>
            <ta e="T613" id="Seg_6030" s="T612">ugandə</ta>
            <ta e="T614" id="Seg_6031" s="T613">bü</ta>
            <ta e="T615" id="Seg_6032" s="T614">nada</ta>
            <ta e="T616" id="Seg_6033" s="T615">par-luʔ-sittə</ta>
            <ta e="T617" id="Seg_6034" s="T616">ugandə</ta>
            <ta e="T618" id="Seg_6035" s="T617">kuŋgə</ta>
            <ta e="T619" id="Seg_6036" s="T618">id-lia</ta>
            <ta e="T620" id="Seg_6037" s="T868">bar</ta>
            <ta e="T621" id="Seg_6038" s="T620">pa-ʔi</ta>
            <ta e="T622" id="Seg_6039" s="T621">naga</ta>
            <ta e="T623" id="Seg_6040" s="T622">onʼiʔ</ta>
            <ta e="T624" id="Seg_6041" s="T623">da</ta>
            <ta e="T625" id="Seg_6042" s="T624">pa</ta>
            <ta e="T627" id="Seg_6043" s="T625">naga</ta>
            <ta e="T629" id="Seg_6044" s="T628">šonə-ga-m</ta>
            <ta e="T631" id="Seg_6045" s="T630">nüj-leʔbə-m</ta>
            <ta e="T632" id="Seg_6046" s="T631">dʼije-nə</ta>
            <ta e="T633" id="Seg_6047" s="T632">kandə-ga-m</ta>
            <ta e="T634" id="Seg_6048" s="T633">dʼije</ta>
            <ta e="T636" id="Seg_6049" s="T635">nüj-leʔbə-m</ta>
            <ta e="T637" id="Seg_6050" s="T636">bü-nə</ta>
            <ta e="T638" id="Seg_6051" s="T637">kandə-ga-m</ta>
            <ta e="T639" id="Seg_6052" s="T638">bü</ta>
            <ta e="T640" id="Seg_6053" s="T639">nüj-leʔbə-m</ta>
            <ta e="T641" id="Seg_6054" s="T640">pa-gən</ta>
            <ta e="T642" id="Seg_6055" s="T641">kandə-ga-m</ta>
            <ta e="T643" id="Seg_6056" s="T642">pa</ta>
            <ta e="T644" id="Seg_6057" s="T643">nüj-leʔbə-m</ta>
            <ta e="T645" id="Seg_6058" s="T644">măn</ta>
            <ta e="T646" id="Seg_6059" s="T645">aba-m</ta>
            <ta e="T647" id="Seg_6060" s="T646">Kirelʼ-də</ta>
            <ta e="T648" id="Seg_6061" s="T647">kam-bi</ta>
            <ta e="T650" id="Seg_6062" s="T649">i-bi</ta>
            <ta e="T651" id="Seg_6063" s="T650">kola</ta>
            <ta e="T653" id="Seg_6064" s="T652">iʔgö</ta>
            <ta e="T654" id="Seg_6065" s="T653">dʼaʔ-pi</ta>
            <ta e="T655" id="Seg_6066" s="T654">miʔ</ta>
            <ta e="T656" id="Seg_6067" s="T655">arəm-də-bi-baʔ</ta>
            <ta e="T657" id="Seg_6068" s="T656">i</ta>
            <ta e="T658" id="Seg_6069" s="T657">tustʼar-bi-baʔ</ta>
            <ta e="T659" id="Seg_6070" s="T658">i</ta>
            <ta e="T660" id="Seg_6071" s="T659">pür-bi-beʔ</ta>
            <ta e="T661" id="Seg_6072" s="T660">i</ta>
            <ta e="T662" id="Seg_6073" s="T661">am-bi-baʔ</ta>
            <ta e="T663" id="Seg_6074" s="T662">miʔ</ta>
            <ta e="T664" id="Seg_6075" s="T663">mĭm-bieʔ</ta>
            <ta e="T665" id="Seg_6076" s="T664">i</ta>
            <ta e="T666" id="Seg_6077" s="T665">ugandə</ta>
            <ta e="T667" id="Seg_6078" s="T666">urgo</ta>
            <ta e="T668" id="Seg_6079" s="T667">bü</ta>
            <ta e="T669" id="Seg_6080" s="T668">dĭn</ta>
            <ta e="T670" id="Seg_6081" s="T669">bar</ta>
            <ta e="T671" id="Seg_6082" s="T670">sedʼi-ʔi</ta>
            <ta e="T672" id="Seg_6083" s="T671">barəʔ-pi-biaʔ</ta>
            <ta e="T673" id="Seg_6084" s="T672">kola</ta>
            <ta e="T674" id="Seg_6085" s="T673">dʼaʔ-pi-baʔ</ta>
            <ta e="T675" id="Seg_6086" s="T674">kuza</ta>
            <ta e="T676" id="Seg_6087" s="T675">bar</ta>
            <ta e="T677" id="Seg_6088" s="T676">ne-m</ta>
            <ta e="T678" id="Seg_6089" s="T677">pʼaŋdə-bi</ta>
            <ta e="T679" id="Seg_6090" s="T678">a</ta>
            <ta e="T680" id="Seg_6091" s="T679">bos-tə</ta>
            <ta e="T681" id="Seg_6092" s="T680">pa-nə</ta>
            <ta e="T682" id="Seg_6093" s="T681">edə-luʔ-pi</ta>
            <ta e="T684" id="Seg_6094" s="T683">urgaːba</ta>
            <ta e="T685" id="Seg_6095" s="T684">ugandə</ta>
            <ta e="T686" id="Seg_6096" s="T685">tăr-də</ta>
            <ta e="T687" id="Seg_6097" s="T686">iʔgö</ta>
            <ta e="T688" id="Seg_6098" s="T687">bar</ta>
            <ta e="T689" id="Seg_6099" s="T688">uja</ta>
            <ta e="T690" id="Seg_6100" s="T689">ej</ta>
            <ta e="T691" id="Seg_6101" s="T690">id-lia</ta>
            <ta e="T692" id="Seg_6102" s="T691">ugandə</ta>
            <ta e="T693" id="Seg_6103" s="T692">dĭ</ta>
            <ta e="T694" id="Seg_6104" s="T693">nömər</ta>
            <ta e="T695" id="Seg_6105" s="T694">ugandə</ta>
            <ta e="T696" id="Seg_6106" s="T695">kəbə</ta>
            <ta e="T697" id="Seg_6107" s="T696">uju-t</ta>
            <ta e="T698" id="Seg_6108" s="T697">ugandə</ta>
            <ta e="T699" id="Seg_6109" s="T698">nʼeʔdə</ta>
            <ta e="T700" id="Seg_6110" s="T699">nʼeʔdə</ta>
            <ta e="T701" id="Seg_6111" s="T700">kada-ʔi</ta>
            <ta e="T702" id="Seg_6112" s="T701">ugandə</ta>
            <ta e="T703" id="Seg_6113" s="T702">urgo</ta>
            <ta e="T704" id="Seg_6114" s="T703">urgo</ta>
            <ta e="T705" id="Seg_6115" s="T704">men</ta>
            <ta e="T706" id="Seg_6116" s="T705">men-zi</ta>
            <ta e="T707" id="Seg_6117" s="T706">urgo</ta>
            <ta e="T708" id="Seg_6118" s="T707">men-zi</ta>
            <ta e="T709" id="Seg_6119" s="T708">sʼima-t</ta>
            <ta e="T710" id="Seg_6120" s="T709">üdʼüge</ta>
            <ta e="T711" id="Seg_6121" s="T710">ku-ʔi</ta>
            <ta e="T712" id="Seg_6122" s="T711">üdʼüge</ta>
            <ta e="T713" id="Seg_6123" s="T712">măn</ta>
            <ta e="T714" id="Seg_6124" s="T713">tugan-bə</ta>
            <ta e="T715" id="Seg_6125" s="T714">šo-bi</ta>
            <ta e="T716" id="Seg_6126" s="T715">nʼi</ta>
            <ta e="T717" id="Seg_6127" s="T716">šo-bi</ta>
            <ta e="T718" id="Seg_6128" s="T717">ne-zi</ta>
            <ta e="T719" id="Seg_6129" s="T718">kaga-m</ta>
            <ta e="T720" id="Seg_6130" s="T719">šo-bi</ta>
            <ta e="T721" id="Seg_6131" s="T720">ne-zi</ta>
            <ta e="T723" id="Seg_6132" s="T721">es-seŋ</ta>
            <ta e="T724" id="Seg_6133" s="T723">măn</ta>
            <ta e="T725" id="Seg_6134" s="T724">tugan-bə</ta>
            <ta e="T726" id="Seg_6135" s="T725">šo-bi</ta>
            <ta e="T727" id="Seg_6136" s="T726">nʼi-m</ta>
            <ta e="T728" id="Seg_6137" s="T727">šo-bi</ta>
            <ta e="T729" id="Seg_6138" s="T728">ne-zi</ta>
            <ta e="T730" id="Seg_6139" s="T729">kaga-m</ta>
            <ta e="T731" id="Seg_6140" s="T730">šo-bi</ta>
            <ta e="T732" id="Seg_6141" s="T731">ne-zi</ta>
            <ta e="T733" id="Seg_6142" s="T732">koʔbdo-m</ta>
            <ta e="T734" id="Seg_6143" s="T733">tibi-zi</ta>
            <ta e="T735" id="Seg_6144" s="T734">šo-bi</ta>
            <ta e="T736" id="Seg_6145" s="T735">es-seŋ</ta>
            <ta e="T737" id="Seg_6146" s="T736">es-seŋ-də</ta>
            <ta e="T738" id="Seg_6147" s="T737">dĭ-zi</ta>
            <ta e="T739" id="Seg_6148" s="T738">šo-bi-ʔi</ta>
            <ta e="T740" id="Seg_6149" s="T739">nada</ta>
            <ta e="T741" id="Seg_6150" s="T740">dĭ-zeŋ-də</ta>
            <ta e="T743" id="Seg_6151" s="T742">oʔbdə-sʼtə</ta>
            <ta e="T744" id="Seg_6152" s="T743">dĭ-zeŋ</ta>
            <ta e="T746" id="Seg_6153" s="T745">am-zittə</ta>
            <ta e="T747" id="Seg_6154" s="T746">mĭ-zittə</ta>
            <ta e="T748" id="Seg_6155" s="T747">măn</ta>
            <ta e="T749" id="Seg_6156" s="T748">nʼi-m</ta>
            <ta e="T750" id="Seg_6157" s="T749">bar</ta>
            <ta e="T751" id="Seg_6158" s="T750">tăŋ</ta>
            <ta e="T752" id="Seg_6159" s="T751">ĭzem-nie</ta>
            <ta e="T753" id="Seg_6160" s="T752">măn</ta>
            <ta e="T754" id="Seg_6161" s="T753">dĭʔ-nə</ta>
            <ta e="T755" id="Seg_6162" s="T754">măm-bia-m</ta>
            <ta e="T756" id="Seg_6163" s="T755">kudaj-də</ta>
            <ta e="T757" id="Seg_6164" s="T756">numan üz-eʔ</ta>
            <ta e="T758" id="Seg_6165" s="T757">pʼe-ʔ</ta>
            <ta e="T759" id="Seg_6166" s="T758">dĭʔə</ta>
            <ta e="T760" id="Seg_6167" s="T759">šal-də</ta>
            <ta e="T761" id="Seg_6168" s="T760">kăšt-aʔ</ta>
            <ta e="T762" id="Seg_6169" s="T761">bos-kənə</ta>
            <ta e="T763" id="Seg_6170" s="T762">dĭ</ta>
            <ta e="T764" id="Seg_6171" s="T763">bar</ta>
            <ta e="T765" id="Seg_6172" s="T764">kăštə-bi</ta>
            <ta e="T766" id="Seg_6173" s="T765">šo-ʔ</ta>
            <ta e="T767" id="Seg_6174" s="T766">măna</ta>
            <ta e="T768" id="Seg_6175" s="T767">šal-də</ta>
            <ta e="T769" id="Seg_6176" s="T768">de-ʔ</ta>
            <ta e="T770" id="Seg_6177" s="T769">dʼazir-də</ta>
            <ta e="T771" id="Seg_6178" s="T770">măna</ta>
            <ta e="T772" id="Seg_6179" s="T771">măn</ta>
            <ta e="T773" id="Seg_6180" s="T772">taldʼen</ta>
            <ta e="T774" id="Seg_6181" s="T773">tura</ta>
            <ta e="T775" id="Seg_6182" s="T774">băzə-bia-m</ta>
            <ta e="T776" id="Seg_6183" s="T775">kuris</ta>
            <ta e="T777" id="Seg_6184" s="T776">tüʔ-pi</ta>
            <ta e="T778" id="Seg_6185" s="T777">tože</ta>
            <ta e="T779" id="Seg_6186" s="T778">băzə-bia-m</ta>
            <ta e="T780" id="Seg_6187" s="T779">dĭgəttə</ta>
            <ta e="T782" id="Seg_6188" s="T781">băzə-bia-m</ta>
            <ta e="T783" id="Seg_6189" s="T782">dĭgəttə</ta>
            <ta e="T784" id="Seg_6190" s="T783">kam-bia-m</ta>
            <ta e="T785" id="Seg_6191" s="T784">bü</ta>
            <ta e="T786" id="Seg_6192" s="T785">tazir-zittə</ta>
            <ta e="T787" id="Seg_6193" s="T786">multʼa-nə</ta>
            <ta e="T789" id="Seg_6194" s="T787">oʔb</ta>
            <ta e="T790" id="Seg_6195" s="T789">muktuʔ</ta>
            <ta e="T791" id="Seg_6196" s="T790">aspaʔ</ta>
            <ta e="T792" id="Seg_6197" s="T791">deʔ-pie-m</ta>
            <ta e="T793" id="Seg_6198" s="T792">măn</ta>
            <ta e="T794" id="Seg_6199" s="T793">teinen</ta>
            <ta e="T795" id="Seg_6200" s="T794">erte-n</ta>
            <ta e="T796" id="Seg_6201" s="T795">uʔbdə-bia-m</ta>
            <ta e="T799" id="Seg_6202" s="T798">dʼala</ta>
            <ta e="T800" id="Seg_6203" s="T799">togonor-bia-m</ta>
            <ta e="T801" id="Seg_6204" s="T800">bar</ta>
            <ta e="T802" id="Seg_6205" s="T801">tarar-luʔ-pie-m</ta>
            <ta e="T804" id="Seg_6206" s="T803">nüdʼi-n</ta>
            <ta e="T805" id="Seg_6207" s="T804">nüdʼi-n</ta>
            <ta e="T806" id="Seg_6208" s="T805">šo-bi</ta>
            <ta e="T807" id="Seg_6209" s="T806">măn</ta>
            <ta e="T808" id="Seg_6210" s="T807">kunol-luʔ-pia-m</ta>
            <ta e="T809" id="Seg_6211" s="T808">măn</ta>
            <ta e="T810" id="Seg_6212" s="T809">dĭ-zem</ta>
            <ta e="T811" id="Seg_6213" s="T810">ku-sʼtə</ta>
            <ta e="T813" id="Seg_6214" s="T812">nʼe</ta>
            <ta e="T814" id="Seg_6215" s="T813">axota</ta>
            <ta e="T815" id="Seg_6216" s="T814">dĭ-zeŋ</ta>
            <ta e="T816" id="Seg_6217" s="T815">bar</ta>
            <ta e="T817" id="Seg_6218" s="T816">üge</ta>
            <ta e="T819" id="Seg_6219" s="T818">šam-naʔbə-ʔjə</ta>
            <ta e="T820" id="Seg_6220" s="T819">šam-naʔbə-ʔjə</ta>
            <ta e="T821" id="Seg_6221" s="T820">dĭ-zeŋ</ta>
            <ta e="T822" id="Seg_6222" s="T821">arda</ta>
            <ta e="T824" id="Seg_6223" s="T823">nʼilgö-sʼtə</ta>
            <ta e="T825" id="Seg_6224" s="T824">nʼe</ta>
            <ta e="T826" id="Seg_6225" s="T825">axota</ta>
            <ta e="T827" id="Seg_6226" s="T826">pušaj</ta>
            <ta e="T828" id="Seg_6227" s="T827">măna</ta>
            <ta e="T829" id="Seg_6228" s="T828">sʼima-ndə</ta>
            <ta e="T830" id="Seg_6229" s="T829">ej</ta>
            <ta e="T831" id="Seg_6230" s="T830">šo-lə-ʔjə</ta>
            <ta e="T832" id="Seg_6231" s="T831">măna</ta>
            <ta e="T833" id="Seg_6232" s="T832">dĭ-zem</ta>
            <ta e="T834" id="Seg_6233" s="T833">ej</ta>
            <ta e="T835" id="Seg_6234" s="T834">kereʔ</ta>
            <ta e="T836" id="Seg_6235" s="T835">ulu-m</ta>
            <ta e="T837" id="Seg_6236" s="T836">eʔbdə-m</ta>
            <ta e="T838" id="Seg_6237" s="T837">ku-zaŋ-bə</ta>
            <ta e="T839" id="Seg_6238" s="T838">tĭme-m</ta>
            <ta e="T840" id="Seg_6239" s="T839">mon-zaŋ-də</ta>
            <ta e="T841" id="Seg_6240" s="T840">šĭke-m</ta>
            <ta e="T842" id="Seg_6241" s="T841">sʼima-m</ta>
            <ta e="T843" id="Seg_6242" s="T842">püje-m</ta>
            <ta e="T844" id="Seg_6243" s="T843">uda-zaŋ-bə</ta>
            <ta e="T845" id="Seg_6244" s="T844">bögəl-bə</ta>
            <ta e="T847" id="Seg_6245" s="T846">sĭj-bə</ta>
            <ta e="T848" id="Seg_6246" s="T847">šüjö-m</ta>
            <ta e="T849" id="Seg_6247" s="T848">nanə-m</ta>
            <ta e="T851" id="Seg_6248" s="T850">bögəl-bə</ta>
            <ta e="T852" id="Seg_6249" s="T851">köten-də</ta>
            <ta e="T853" id="Seg_6250" s="T852">üjü-m</ta>
            <ta e="T854" id="Seg_6251" s="T853">aŋ-də</ta>
            <ta e="T855" id="Seg_6252" s="T854">sĭj-bə</ta>
            <ta e="T856" id="Seg_6253" s="T855">sĭj-də</ta>
            <ta e="T858" id="Seg_6254" s="T857">nüjü-zaŋ-də</ta>
            <ta e="T861" id="Seg_6255" s="T860">le-zeŋ-də</ta>
            <ta e="T863" id="Seg_6256" s="T862">kem</ta>
         </annotation>
         <annotation name="mp" tierref="mp-PKZ">
            <ta e="T5" id="Seg_6257" s="T4">döbər</ta>
            <ta e="T6" id="Seg_6258" s="T5">šo-lV-l</ta>
            <ta e="T7" id="Seg_6259" s="T6">tak</ta>
            <ta e="T8" id="Seg_6260" s="T7">măna</ta>
            <ta e="T9" id="Seg_6261" s="T8">sima</ta>
            <ta e="T10" id="Seg_6262" s="T9">i-ʔ</ta>
            <ta e="T11" id="Seg_6263" s="T10">tăn</ta>
            <ta e="T12" id="Seg_6264" s="T11">büzʼe</ta>
            <ta e="T13" id="Seg_6265" s="T12">gibər</ta>
            <ta e="T14" id="Seg_6266" s="T13">kan-bi</ta>
            <ta e="T15" id="Seg_6267" s="T14">Dʼelam-Tə</ta>
            <ta e="T869" id="Seg_6268" s="T16">kan-lAʔ</ta>
            <ta e="T17" id="Seg_6269" s="T869">tʼür-bi</ta>
            <ta e="T18" id="Seg_6270" s="T17">ine</ta>
            <ta e="T19" id="Seg_6271" s="T18">unʼə</ta>
            <ta e="T20" id="Seg_6272" s="T19">kan-bi</ta>
            <ta e="T21" id="Seg_6273" s="T20">dʼok</ta>
            <ta e="T22" id="Seg_6274" s="T21">šide-göʔ</ta>
            <ta e="T870" id="Seg_6275" s="T22">kan-lAʔ</ta>
            <ta e="T23" id="Seg_6276" s="T870">tʼür-bi-jəʔ</ta>
            <ta e="T24" id="Seg_6277" s="T23">bar</ta>
            <ta e="T25" id="Seg_6278" s="T24">kamən</ta>
            <ta e="T27" id="Seg_6279" s="T26">kan-bi-jəʔ</ta>
            <ta e="T29" id="Seg_6280" s="T28">taldʼen</ta>
            <ta e="T30" id="Seg_6281" s="T29">kan-bi-jəʔ</ta>
            <ta e="T31" id="Seg_6282" s="T30">ugaːndə</ta>
            <ta e="T32" id="Seg_6283" s="T31">ertə-n</ta>
            <ta e="T33" id="Seg_6284" s="T32">ĭmbi</ta>
            <ta e="T34" id="Seg_6285" s="T33">šʼaːm-laʔbə-l</ta>
            <ta e="T35" id="Seg_6286" s="T34">ej</ta>
            <ta e="T38" id="Seg_6287" s="T37">ej</ta>
            <ta e="T40" id="Seg_6288" s="T39">sĭr-liA-l</ta>
            <ta e="T41" id="Seg_6289" s="T40">köžetsnək-Tə</ta>
            <ta e="T43" id="Seg_6290" s="T42">nuldə-t</ta>
            <ta e="T44" id="Seg_6291" s="T43">bar</ta>
            <ta e="T45" id="Seg_6292" s="T44">kujnek-m</ta>
            <ta e="T46" id="Seg_6293" s="T45">nĭŋgə-luʔbdə-bi</ta>
            <ta e="T47" id="Seg_6294" s="T46">ši</ta>
            <ta e="T48" id="Seg_6295" s="T47">mo-laːm-bi</ta>
            <ta e="T49" id="Seg_6296" s="T48">măn</ta>
            <ta e="T50" id="Seg_6297" s="T49">dĭgəttə</ta>
            <ta e="T51" id="Seg_6298" s="T50">dĭ-m</ta>
            <ta e="T53" id="Seg_6299" s="T52">šödör-bi-m</ta>
            <ta e="T54" id="Seg_6300" s="T53">nʼitka-jəʔ-ziʔ</ta>
            <ta e="T55" id="Seg_6301" s="T54">nʼimi-ziʔ</ta>
            <ta e="T56" id="Seg_6302" s="T55">ĭntak</ta>
            <ta e="T57" id="Seg_6303" s="T56">tʼegermaʔ-Kən</ta>
            <ta e="T58" id="Seg_6304" s="T57">bar</ta>
            <ta e="T59" id="Seg_6305" s="T58">kuzur-laʔbə</ta>
            <ta e="T60" id="Seg_6306" s="T59">koŋgoro</ta>
            <ta e="T61" id="Seg_6307" s="T60">nadə</ta>
            <ta e="T62" id="Seg_6308" s="T61">kan-zittə</ta>
            <ta e="T63" id="Seg_6309" s="T62">kudaj-Tə</ta>
            <ta e="T65" id="Seg_6310" s="T64">üzə-zittə</ta>
            <ta e="T66" id="Seg_6311" s="T65">dĭgəttə</ta>
            <ta e="T67" id="Seg_6312" s="T66">kăštə-liA-jəʔ</ta>
            <ta e="T68" id="Seg_6313" s="T67">dĭ-zAŋ</ta>
            <ta e="T69" id="Seg_6314" s="T68">bar</ta>
            <ta e="T70" id="Seg_6315" s="T69">edə-t</ta>
            <ta e="T71" id="Seg_6316" s="T70">ine-Tə</ta>
            <ta e="T72" id="Seg_6317" s="T71">koŋgoro</ta>
            <ta e="T73" id="Seg_6318" s="T72">sagəš-də</ta>
            <ta e="T74" id="Seg_6319" s="T73">naga</ta>
            <ta e="T75" id="Seg_6320" s="T74">bar</ta>
            <ta e="T76" id="Seg_6321" s="T75">ĭmbi=də</ta>
            <ta e="T77" id="Seg_6322" s="T76">ej</ta>
            <ta e="T78" id="Seg_6323" s="T77">tĭmne-m</ta>
            <ta e="T79" id="Seg_6324" s="T78">ĭmbi=də</ta>
            <ta e="T80" id="Seg_6325" s="T79">ej</ta>
            <ta e="T81" id="Seg_6326" s="T80">mo-liA-m</ta>
            <ta e="T82" id="Seg_6327" s="T81">nörbə-zittə</ta>
            <ta e="T83" id="Seg_6328" s="T82">bar</ta>
            <ta e="T84" id="Seg_6329" s="T83">sagəš-də</ta>
            <ta e="T85" id="Seg_6330" s="T84">naga</ta>
            <ta e="T86" id="Seg_6331" s="T85">dʼije-Tə</ta>
            <ta e="T87" id="Seg_6332" s="T86">kan-bi-m</ta>
            <ta e="T88" id="Seg_6333" s="T87">bar</ta>
            <ta e="T92" id="Seg_6334" s="T91">tʼür-laʔbə</ta>
            <ta e="T93" id="Seg_6335" s="T92">dʼije-Tə</ta>
            <ta e="T94" id="Seg_6336" s="T93">kan-bi-m</ta>
            <ta e="T95" id="Seg_6337" s="T94">bar</ta>
            <ta e="T96" id="Seg_6338" s="T95">tʼür-laʔbə-bi-m</ta>
            <ta e="T97" id="Seg_6339" s="T96">dĭgəttə</ta>
            <ta e="T98" id="Seg_6340" s="T97">kirgaːr-bi-m</ta>
            <ta e="T99" id="Seg_6341" s="T98">bar</ta>
            <ta e="T100" id="Seg_6342" s="T99">dĭgəttə</ta>
            <ta e="T101" id="Seg_6343" s="T100">šo-bi-m</ta>
            <ta e="T102" id="Seg_6344" s="T101">kondʼo</ta>
            <ta e="T103" id="Seg_6345" s="T102">amno-bi</ta>
            <ta e="T104" id="Seg_6346" s="T103">ĭmbi=də</ta>
            <ta e="T106" id="Seg_6347" s="T104">bar</ta>
            <ta e="T107" id="Seg_6348" s="T106">ĭmbi=də</ta>
            <ta e="T108" id="Seg_6349" s="T107">ej</ta>
            <ta e="T109" id="Seg_6350" s="T108">tʼăbaktər-bi</ta>
            <ta e="T110" id="Seg_6351" s="T109">dĭ</ta>
            <ta e="T111" id="Seg_6352" s="T110">kandə-gA</ta>
            <ta e="T112" id="Seg_6353" s="T111">a</ta>
            <ta e="T113" id="Seg_6354" s="T112">măn</ta>
            <ta e="T114" id="Seg_6355" s="T113">dĭ-m</ta>
            <ta e="T116" id="Seg_6356" s="T115">bĭdə-liA-m</ta>
            <ta e="T117" id="Seg_6357" s="T116">e-ʔ</ta>
            <ta e="T118" id="Seg_6358" s="T117">kan-ə-ʔ</ta>
            <ta e="T119" id="Seg_6359" s="T118">par-ə-ʔ</ta>
            <ta e="T120" id="Seg_6360" s="T119">döbər</ta>
            <ta e="T121" id="Seg_6361" s="T120">măn</ta>
            <ta e="T122" id="Seg_6362" s="T121">tănan</ta>
            <ta e="T123" id="Seg_6363" s="T122">măn-liA-m</ta>
            <ta e="T124" id="Seg_6364" s="T123">par-ə-ʔ</ta>
            <ta e="T125" id="Seg_6365" s="T124">döbər</ta>
            <ta e="T126" id="Seg_6366" s="T125">măn</ta>
            <ta e="T127" id="Seg_6367" s="T126">ugaːndə</ta>
            <ta e="T128" id="Seg_6368" s="T127">urgo</ta>
            <ta e="T129" id="Seg_6369" s="T128">il</ta>
            <ta e="T130" id="Seg_6370" s="T129">i-gA</ta>
            <ta e="T131" id="Seg_6371" s="T130">ipek</ta>
            <ta e="T132" id="Seg_6372" s="T131">iʔgö</ta>
            <ta e="T134" id="Seg_6373" s="T133">kereʔ</ta>
            <ta e="T135" id="Seg_6374" s="T134">i-ʔ</ta>
            <ta e="T136" id="Seg_6375" s="T135">bögəl-də</ta>
            <ta e="T137" id="Seg_6376" s="T136">dĭ-m</ta>
            <ta e="T138" id="Seg_6377" s="T137">uda-l</ta>
            <ta e="T139" id="Seg_6378" s="T138">ej</ta>
            <ta e="T140" id="Seg_6379" s="T139">hen-t</ta>
            <ta e="T142" id="Seg_6380" s="T140">üžəm-t</ta>
            <ta e="T143" id="Seg_6381" s="T142">dĭ-m</ta>
            <ta e="T144" id="Seg_6382" s="T143">üžəm-t</ta>
            <ta e="T145" id="Seg_6383" s="T144">dĭ-m</ta>
            <ta e="T146" id="Seg_6384" s="T145">uda-ziʔ</ta>
            <ta e="T148" id="Seg_6385" s="T146">bos-kənə</ta>
            <ta e="T149" id="Seg_6386" s="T148">iʔgö</ta>
            <ta e="T150" id="Seg_6387" s="T149">i-bi-m</ta>
            <ta e="T151" id="Seg_6388" s="T150">a</ta>
            <ta e="T152" id="Seg_6389" s="T151">dĭ-Tə</ta>
            <ta e="T153" id="Seg_6390" s="T152">amka-m</ta>
            <ta e="T154" id="Seg_6391" s="T153">i-bi-m</ta>
            <ta e="T155" id="Seg_6392" s="T154">bos-gəndə</ta>
            <ta e="T156" id="Seg_6393" s="T155">iʔgö</ta>
            <ta e="T157" id="Seg_6394" s="T156">i-liA</ta>
            <ta e="T158" id="Seg_6395" s="T157">a</ta>
            <ta e="T159" id="Seg_6396" s="T158">dĭ-Tə</ta>
            <ta e="T160" id="Seg_6397" s="T159">amka-m</ta>
            <ta e="T161" id="Seg_6398" s="T160">i-liA</ta>
            <ta e="T162" id="Seg_6399" s="T161">muktuʔ</ta>
            <ta e="T163" id="Seg_6400" s="T162">bar</ta>
            <ta e="T164" id="Seg_6401" s="T163">tʼăbaktər-bi-bAʔ</ta>
            <ta e="T165" id="Seg_6402" s="T164">miʔ</ta>
            <ta e="T166" id="Seg_6403" s="T165">bar</ta>
            <ta e="T167" id="Seg_6404" s="T166">üdʼüge</ta>
            <ta e="T168" id="Seg_6405" s="T167">i-bi-bAʔ</ta>
            <ta e="T169" id="Seg_6406" s="T168">bar</ta>
            <ta e="T170" id="Seg_6407" s="T169">sʼar-laʔbə-bi-bAʔ</ta>
            <ta e="T171" id="Seg_6408" s="T170">pa-jəʔ</ta>
            <ta e="T172" id="Seg_6409" s="T171">i-lV-j</ta>
            <ta e="T173" id="Seg_6410" s="T172">da</ta>
            <ta e="T174" id="Seg_6411" s="T173">bar</ta>
            <ta e="T175" id="Seg_6412" s="T174">oi</ta>
            <ta e="T176" id="Seg_6413" s="T175">onʼiʔ</ta>
            <ta e="T177" id="Seg_6414" s="T176">nuʔmə-lV-j</ta>
            <ta e="T178" id="Seg_6415" s="T177">a</ta>
            <ta e="T179" id="Seg_6416" s="T178">miʔ</ta>
            <ta e="T180" id="Seg_6417" s="T179">bar</ta>
            <ta e="T181" id="Seg_6418" s="T180">šaʔ-laːm-bi-bAʔ</ta>
            <ta e="T182" id="Seg_6419" s="T181">dĭgəttə</ta>
            <ta e="T183" id="Seg_6420" s="T182">dĭ</ta>
            <ta e="T184" id="Seg_6421" s="T183">bar</ta>
            <ta e="T185" id="Seg_6422" s="T184">ku-liA</ta>
            <ta e="T186" id="Seg_6423" s="T185">ku-liA</ta>
            <ta e="T187" id="Seg_6424" s="T186">ej</ta>
            <ta e="T189" id="Seg_6425" s="T188">ej</ta>
            <ta e="T190" id="Seg_6426" s="T189">mo-liA</ta>
            <ta e="T191" id="Seg_6427" s="T190">ku-zittə</ta>
            <ta e="T192" id="Seg_6428" s="T191">miʔ</ta>
            <ta e="T193" id="Seg_6429" s="T192">bar</ta>
            <ta e="T194" id="Seg_6430" s="T193">bazoʔ</ta>
            <ta e="T195" id="Seg_6431" s="T194">sar-laʔbə-bAʔ</ta>
            <ta e="T197" id="Seg_6432" s="T196">miʔ</ta>
            <ta e="T198" id="Seg_6433" s="T197">bar</ta>
            <ta e="T199" id="Seg_6434" s="T198">ej</ta>
            <ta e="T200" id="Seg_6435" s="T199">kudonz-luʔbdə-bi-bAʔ</ta>
            <ta e="T201" id="Seg_6436" s="T200">jakšə</ta>
            <ta e="T203" id="Seg_6437" s="T201">bar</ta>
            <ta e="T205" id="Seg_6438" s="T204">jakšə</ta>
            <ta e="T207" id="Seg_6439" s="T205">bar</ta>
            <ta e="T210" id="Seg_6440" s="T209">miʔ</ta>
            <ta e="T211" id="Seg_6441" s="T210">bar</ta>
            <ta e="T212" id="Seg_6442" s="T211">jakšə</ta>
            <ta e="T213" id="Seg_6443" s="T212">sʼar-bi-bAʔ</ta>
            <ta e="T214" id="Seg_6444" s="T213">miʔ</ta>
            <ta e="T215" id="Seg_6445" s="T214">ej</ta>
            <ta e="T216" id="Seg_6446" s="T215">kurol-lV-bi-bAʔ</ta>
            <ta e="T217" id="Seg_6447" s="T216">tibi-zAŋ</ta>
            <ta e="T218" id="Seg_6448" s="T217">i</ta>
            <ta e="T219" id="Seg_6449" s="T218">ne-zAŋ</ta>
            <ta e="T220" id="Seg_6450" s="T219">amnə-bi-jəʔ</ta>
            <ta e="T221" id="Seg_6451" s="T220">da</ta>
            <ta e="T222" id="Seg_6452" s="T221">măndo-laʔbə-bi-jəʔ</ta>
            <ta e="T223" id="Seg_6453" s="T222">kădaʔ</ta>
            <ta e="T224" id="Seg_6454" s="T223">miʔ</ta>
            <ta e="T225" id="Seg_6455" s="T224">sʼar-laʔbə-bAʔ</ta>
            <ta e="T226" id="Seg_6456" s="T225">šində</ta>
            <ta e="T229" id="Seg_6457" s="T228">nʼeʔbdə-ntə-bi</ta>
            <ta e="T230" id="Seg_6458" s="T229">taŋgu</ta>
            <ta e="T231" id="Seg_6459" s="T230">šində</ta>
            <ta e="T232" id="Seg_6460" s="T231">ej</ta>
            <ta e="T233" id="Seg_6461" s="T232">nʼeʔbdə-ntə-bi</ta>
            <ta e="T234" id="Seg_6462" s="T233">šində</ta>
            <ta e="T235" id="Seg_6463" s="T234">bar</ta>
            <ta e="T237" id="Seg_6464" s="T236">kanza-ziʔ</ta>
            <ta e="T238" id="Seg_6465" s="T237">bar</ta>
            <ta e="T239" id="Seg_6466" s="T238">nʼeʔbdə-laʔbə</ta>
            <ta e="T240" id="Seg_6467" s="T239">šində</ta>
            <ta e="T241" id="Seg_6468" s="T240">ej</ta>
            <ta e="T242" id="Seg_6469" s="T241">nʼeʔbdə-laʔbə</ta>
            <ta e="T243" id="Seg_6470" s="T242">ular</ta>
            <ta e="T244" id="Seg_6471" s="T243">ugaːndə</ta>
            <ta e="T245" id="Seg_6472" s="T244">iʔgö</ta>
            <ta e="T247" id="Seg_6473" s="T246">šide</ta>
            <ta e="T248" id="Seg_6474" s="T247">biəʔ</ta>
            <ta e="T249" id="Seg_6475" s="T248">šide</ta>
            <ta e="T250" id="Seg_6476" s="T249">aba-m</ta>
            <ta e="T251" id="Seg_6477" s="T250">dʼije-Tə</ta>
            <ta e="T252" id="Seg_6478" s="T251">kan-bi</ta>
            <ta e="T253" id="Seg_6479" s="T252">iʔgö</ta>
            <ta e="T254" id="Seg_6480" s="T253">kuriza-jəʔ</ta>
            <ta e="T255" id="Seg_6481" s="T254">kut-bi</ta>
            <ta e="T256" id="Seg_6482" s="T255">i</ta>
            <ta e="T257" id="Seg_6483" s="T256">maʔ-gəndə</ta>
            <ta e="T258" id="Seg_6484" s="T257">det-bi</ta>
            <ta e="T259" id="Seg_6485" s="T258">măn</ta>
            <ta e="T260" id="Seg_6486" s="T259">šonə-gA-m</ta>
            <ta e="T261" id="Seg_6487" s="T260">Permʼakovo</ta>
            <ta e="T262" id="Seg_6488" s="T261">ku-liA-m</ta>
            <ta e="T263" id="Seg_6489" s="T262">bar</ta>
            <ta e="T264" id="Seg_6490" s="T263">bulan</ta>
            <ta e="T265" id="Seg_6491" s="T264">nu-gA</ta>
            <ta e="T266" id="Seg_6492" s="T265">măn</ta>
            <ta e="T267" id="Seg_6493" s="T266">măndo-bi-m</ta>
            <ta e="T268" id="Seg_6494" s="T267">măndo-bi-m</ta>
            <ta e="T269" id="Seg_6495" s="T268">dĭgəttə</ta>
            <ta e="T270" id="Seg_6496" s="T269">bar</ta>
            <ta e="T271" id="Seg_6497" s="T270">kirgaːr-luʔbdə-bi-m</ta>
            <ta e="T272" id="Seg_6498" s="T271">dĭ</ta>
            <ta e="T273" id="Seg_6499" s="T272">bar</ta>
            <ta e="T274" id="Seg_6500" s="T273">nuʔmə-luʔbdə-bi</ta>
            <ta e="T275" id="Seg_6501" s="T274">bü-Tə</ta>
            <ta e="T276" id="Seg_6502" s="T275">penzi</ta>
            <ta e="T277" id="Seg_6503" s="T276">dĭgəttə</ta>
            <ta e="T278" id="Seg_6504" s="T277">dĭ</ta>
            <ta e="T279" id="Seg_6505" s="T278">bar</ta>
            <ta e="T280" id="Seg_6506" s="T279">kan-luʔbdə-bi</ta>
            <ta e="T281" id="Seg_6507" s="T280">i</ta>
            <ta e="T282" id="Seg_6508" s="T281">nuʔmə-luʔbdə-bi</ta>
            <ta e="T283" id="Seg_6509" s="T282">măn</ta>
            <ta e="T284" id="Seg_6510" s="T283">šo-bi-m</ta>
            <ta e="T285" id="Seg_6511" s="T284">da</ta>
            <ta e="T286" id="Seg_6512" s="T285">Vlas-Tə</ta>
            <ta e="T287" id="Seg_6513" s="T286">nörbə-bi-m</ta>
            <ta e="T288" id="Seg_6514" s="T287">dĭ</ta>
            <ta e="T289" id="Seg_6515" s="T288">kan-bi</ta>
            <ta e="T290" id="Seg_6516" s="T289">multuk-ziʔ</ta>
            <ta e="T291" id="Seg_6517" s="T290">da</ta>
            <ta e="T293" id="Seg_6518" s="T292">kut-bi</ta>
            <ta e="T295" id="Seg_6519" s="T293">aspaʔ</ta>
            <ta e="T296" id="Seg_6520" s="T295">măn</ta>
            <ta e="T297" id="Seg_6521" s="T296">dĭgəttə</ta>
            <ta e="T298" id="Seg_6522" s="T297">uja</ta>
            <ta e="T299" id="Seg_6523" s="T298">bazə-bi-m</ta>
            <ta e="T300" id="Seg_6524" s="T299">i</ta>
            <ta e="T301" id="Seg_6525" s="T300">am-bi-m</ta>
            <ta e="T302" id="Seg_6526" s="T301">miʔnʼibeʔ</ta>
            <ta e="T303" id="Seg_6527" s="T302">mĭ-bi</ta>
            <ta e="T304" id="Seg_6528" s="T303">onʼiʔ</ta>
            <ta e="T305" id="Seg_6529" s="T304">aspaʔ</ta>
            <ta e="T306" id="Seg_6530" s="T866">kuza</ta>
            <ta e="T307" id="Seg_6531" s="T306">šo-bi</ta>
            <ta e="T308" id="Seg_6532" s="T307">măn</ta>
            <ta e="T309" id="Seg_6533" s="T308">dĭbər</ta>
            <ta e="T310" id="Seg_6534" s="T309">mĭn-bi-m</ta>
            <ta e="T311" id="Seg_6535" s="T310">dĭ-zAŋ</ta>
            <ta e="T312" id="Seg_6536" s="T311">bar</ta>
            <ta e="T314" id="Seg_6537" s="T312">tʼăbaktər-laʔbə-jəʔ</ta>
            <ta e="T315" id="Seg_6538" s="T314">a</ta>
            <ta e="T316" id="Seg_6539" s="T315">dĭ-zAŋ</ta>
            <ta e="T317" id="Seg_6540" s="T316">bar</ta>
            <ta e="T318" id="Seg_6541" s="T317">măna</ta>
            <ta e="T319" id="Seg_6542" s="T318">sürer-luʔbdə-bi-jəʔ</ta>
            <ta e="T320" id="Seg_6543" s="T319">măn</ta>
            <ta e="T321" id="Seg_6544" s="T320">nuʔmə-luʔbdə-bi-m</ta>
            <ta e="T323" id="Seg_6545" s="T321">unʼə</ta>
            <ta e="T324" id="Seg_6546" s="T323">nu-zAŋ</ta>
            <ta e="T325" id="Seg_6547" s="T324">amno-bi-jəʔ</ta>
            <ta e="T326" id="Seg_6548" s="T325">jakšə</ta>
            <ta e="T327" id="Seg_6549" s="T326">i-bi</ta>
            <ta e="T328" id="Seg_6550" s="T327">uja-t</ta>
            <ta e="T329" id="Seg_6551" s="T328">iʔgö</ta>
            <ta e="T330" id="Seg_6552" s="T329">i-bi</ta>
            <ta e="T331" id="Seg_6553" s="T330">bar</ta>
            <ta e="T332" id="Seg_6554" s="T331">am-liA-l</ta>
            <ta e="T333" id="Seg_6555" s="T332">olenʼ-ə-n</ta>
            <ta e="T334" id="Seg_6556" s="T333">uja</ta>
            <ta e="T335" id="Seg_6557" s="T334">ugaːndə</ta>
            <ta e="T336" id="Seg_6558" s="T335">nömər</ta>
            <ta e="T337" id="Seg_6559" s="T336">kak</ta>
            <ta e="T338" id="Seg_6560" s="T337">ipek</ta>
            <ta e="T339" id="Seg_6561" s="T338">iʔgö</ta>
            <ta e="T340" id="Seg_6562" s="T339">am-liA-l</ta>
            <ta e="T341" id="Seg_6563" s="T340">dĭgəttə</ta>
            <ta e="T342" id="Seg_6564" s="T341">bü</ta>
            <ta e="T343" id="Seg_6565" s="T342">bĭs-liA-l</ta>
            <ta e="T344" id="Seg_6566" s="T865">bü</ta>
            <ta e="T345" id="Seg_6567" s="T344">bĭs-liA-l</ta>
            <ta e="T348" id="Seg_6568" s="T345">nu</ta>
            <ta e="T349" id="Seg_6569" s="T348">dĭ</ta>
            <ta e="T350" id="Seg_6570" s="T349">koŋ</ta>
            <ta e="T351" id="Seg_6571" s="T350">ugaːndə</ta>
            <ta e="T352" id="Seg_6572" s="T351">jakšə</ta>
            <ta e="T353" id="Seg_6573" s="T352">ugaːndə</ta>
            <ta e="T354" id="Seg_6574" s="T353">kuvas</ta>
            <ta e="T355" id="Seg_6575" s="T354">sima-t</ta>
            <ta e="T357" id="Seg_6576" s="T356">sagər</ta>
            <ta e="T358" id="Seg_6577" s="T357">püje-t</ta>
            <ta e="T359" id="Seg_6578" s="T358">ej</ta>
            <ta e="T360" id="Seg_6579" s="T359">numo</ta>
            <ta e="T361" id="Seg_6580" s="T360">mon-zAŋ-l</ta>
            <ta e="T362" id="Seg_6581" s="T361">kuvas</ta>
            <ta e="T363" id="Seg_6582" s="T362">bar</ta>
            <ta e="T364" id="Seg_6583" s="T363">mon-zAŋ-l</ta>
            <ta e="T365" id="Seg_6584" s="T364">ej</ta>
            <ta e="T366" id="Seg_6585" s="T365">nʼešpək</ta>
            <ta e="T368" id="Seg_6586" s="T367">bar</ta>
            <ta e="T369" id="Seg_6587" s="T368">ĭzem-luʔbdə-bi</ta>
            <ta e="T370" id="Seg_6588" s="T369">măn</ta>
            <ta e="T372" id="Seg_6589" s="T371">dĭ-Tə</ta>
            <ta e="T374" id="Seg_6590" s="T373">kan-lV-m</ta>
            <ta e="T375" id="Seg_6591" s="T374">dĭ</ta>
            <ta e="T376" id="Seg_6592" s="T375">bar</ta>
            <ta e="T377" id="Seg_6593" s="T376">măn-ntə</ta>
            <ta e="T378" id="Seg_6594" s="T377">e-ʔ</ta>
            <ta e="T379" id="Seg_6595" s="T378">tʼăbaktər-ə-ʔ</ta>
            <ta e="T380" id="Seg_6596" s="T379">kazak-n</ta>
            <ta e="T381" id="Seg_6597" s="T380">šĭkə-t-ziʔ</ta>
            <ta e="T382" id="Seg_6598" s="T381">a</ta>
            <ta e="T383" id="Seg_6599" s="T382">bos-də</ta>
            <ta e="T384" id="Seg_6600" s="T383">šĭkə-t-ziʔ</ta>
            <ta e="T385" id="Seg_6601" s="T384">tʼăbaktər-ə-ʔ</ta>
            <ta e="T386" id="Seg_6602" s="T385">măn</ta>
            <ta e="T387" id="Seg_6603" s="T386">ugaːndə</ta>
            <ta e="T388" id="Seg_6604" s="T387">ĭzem-liA-m</ta>
            <ta e="T389" id="Seg_6605" s="T388">bar</ta>
            <ta e="T390" id="Seg_6606" s="T389">ugaːndə</ta>
            <ta e="T391" id="Seg_6607" s="T390">ĭzem-liA-m</ta>
            <ta e="T392" id="Seg_6608" s="T391">bar</ta>
            <ta e="T393" id="Seg_6609" s="T392">măn</ta>
            <ta e="T394" id="Seg_6610" s="T393">bar</ta>
            <ta e="T395" id="Seg_6611" s="T394">kü-laːm-bi-m</ta>
            <ta e="T396" id="Seg_6612" s="T395">kü-laːm-bi-m</ta>
            <ta e="T397" id="Seg_6613" s="T396">a</ta>
            <ta e="T398" id="Seg_6614" s="T397">tăn</ta>
            <ta e="T399" id="Seg_6615" s="T398">ma-lV-l</ta>
            <ta e="T400" id="Seg_6616" s="T399">dĭ</ta>
            <ta e="T401" id="Seg_6617" s="T400">kü-laːm-bi</ta>
            <ta e="T402" id="Seg_6618" s="T401">šide</ta>
            <ta e="T403" id="Seg_6619" s="T402">koʔbdo-t</ta>
            <ta e="T405" id="Seg_6620" s="T404">ma-luʔbdə-bi-jəʔ</ta>
            <ta e="T406" id="Seg_6621" s="T405">onʼiʔ</ta>
            <ta e="T407" id="Seg_6622" s="T406">dĭ-ziʔ</ta>
            <ta e="T408" id="Seg_6623" s="T407">i-bi</ta>
            <ta e="T409" id="Seg_6624" s="T408">a</ta>
            <ta e="T410" id="Seg_6625" s="T409">onʼiʔ</ta>
            <ta e="T411" id="Seg_6626" s="T410">kuŋgə</ta>
            <ta e="T412" id="Seg_6627" s="T411">i-bi</ta>
            <ta e="T413" id="Seg_6628" s="T412">teinen</ta>
            <ta e="T415" id="Seg_6629" s="T413">bar</ta>
            <ta e="T420" id="Seg_6630" s="T419">teinen</ta>
            <ta e="T421" id="Seg_6631" s="T420">celɨj</ta>
            <ta e="T422" id="Seg_6632" s="T421">tʼala-t</ta>
            <ta e="T423" id="Seg_6633" s="T422">surno</ta>
            <ta e="T424" id="Seg_6634" s="T423">šo-bi</ta>
            <ta e="T425" id="Seg_6635" s="T424">a</ta>
            <ta e="T426" id="Seg_6636" s="T425">tüj</ta>
            <ta e="T427" id="Seg_6637" s="T426">kojo</ta>
            <ta e="T428" id="Seg_6638" s="T427">măndo-laʔbə</ta>
            <ta e="T429" id="Seg_6639" s="T428">Novik</ta>
            <ta e="T430" id="Seg_6640" s="T429">šonə-gA</ta>
            <ta e="T431" id="Seg_6641" s="T430">šoška-gəndə</ta>
            <ta e="T432" id="Seg_6642" s="T431">a</ta>
            <ta e="T433" id="Seg_6643" s="T432">Sidor</ta>
            <ta e="T434" id="Seg_6644" s="T433">men-Tə</ta>
            <ta e="T435" id="Seg_6645" s="T434">amnə-laʔbə</ta>
            <ta e="T436" id="Seg_6646" s="T435">a</ta>
            <ta e="T437" id="Seg_6647" s="T436">il</ta>
            <ta e="T438" id="Seg_6648" s="T437">ku-bi-jəʔ</ta>
            <ta e="T439" id="Seg_6649" s="T438">tene-bi-jəʔ</ta>
            <ta e="T441" id="Seg_6650" s="T440">predsʼedatʼelʼ</ta>
            <ta e="T442" id="Seg_6651" s="T441">măn-bi</ta>
            <ta e="T443" id="Seg_6652" s="T442">nadə</ta>
            <ta e="T444" id="Seg_6653" s="T443">tʼermən-də</ta>
            <ta e="T445" id="Seg_6654" s="T444">adʼazir-zittə</ta>
            <ta e="T446" id="Seg_6655" s="T445">jakšə</ta>
            <ta e="T447" id="Seg_6656" s="T446">il</ta>
            <ta e="T448" id="Seg_6657" s="T447">oʔbdə-zittə</ta>
            <ta e="T451" id="Seg_6658" s="T450">puskaj</ta>
            <ta e="T452" id="Seg_6659" s="T451">dĭn</ta>
            <ta e="T453" id="Seg_6660" s="T452">togonər-lV-jəʔ</ta>
            <ta e="T454" id="Seg_6661" s="T453">nadə</ta>
            <ta e="T455" id="Seg_6662" s="T454">tʼermən-də</ta>
            <ta e="T456" id="Seg_6663" s="T455">kan-zittə</ta>
            <ta e="T457" id="Seg_6664" s="T456">un</ta>
            <ta e="T458" id="Seg_6665" s="T457">nʼeʔbdə-zittə</ta>
            <ta e="T459" id="Seg_6666" s="T458">ato</ta>
            <ta e="T460" id="Seg_6667" s="T459">un</ta>
            <ta e="T461" id="Seg_6668" s="T460">naga</ta>
            <ta e="T463" id="Seg_6669" s="T462">măn</ta>
            <ta e="T464" id="Seg_6670" s="T463">üdʼüge</ta>
            <ta e="T465" id="Seg_6671" s="T464">i-bi-m</ta>
            <ta e="T466" id="Seg_6672" s="T465">a</ta>
            <ta e="T467" id="Seg_6673" s="T466">koʔbdo-zAŋ-m</ta>
            <ta e="T468" id="Seg_6674" s="T467">urgo</ta>
            <ta e="T469" id="Seg_6675" s="T468">i-bi-jəʔ</ta>
            <ta e="T470" id="Seg_6676" s="T469">üge</ta>
            <ta e="T471" id="Seg_6677" s="T470">măna</ta>
            <ta e="T472" id="Seg_6678" s="T471">i-bi-jəʔ</ta>
            <ta e="T473" id="Seg_6679" s="T472">bos-gəndə</ta>
            <ta e="T474" id="Seg_6680" s="T473">nüjnə</ta>
            <ta e="T475" id="Seg_6681" s="T474">nüj-zittə</ta>
            <ta e="T476" id="Seg_6682" s="T475">măna</ta>
            <ta e="T480" id="Seg_6683" s="T479">kuvas</ta>
            <ta e="T481" id="Seg_6684" s="T480">i-bi</ta>
            <ta e="T482" id="Seg_6685" s="T481">nüj-bi-m</ta>
            <ta e="T483" id="Seg_6686" s="T482">bar</ta>
            <ta e="T484" id="Seg_6687" s="T483">dĭ</ta>
            <ta e="T485" id="Seg_6688" s="T484">kuza</ta>
            <ta e="T486" id="Seg_6689" s="T485">ugaːndə</ta>
            <ta e="T487" id="Seg_6690" s="T486">jakšə</ta>
            <ta e="T488" id="Seg_6691" s="T487">nüj-liA</ta>
            <ta e="T492" id="Seg_6692" s="T491">ugaːndə</ta>
            <ta e="T493" id="Seg_6693" s="T492">kuvas</ta>
            <ta e="T501" id="Seg_6694" s="T500">ugaːndə</ta>
            <ta e="T502" id="Seg_6695" s="T501">tăŋ</ta>
            <ta e="T503" id="Seg_6696" s="T502">nüj-laʔbə</ta>
            <ta e="T504" id="Seg_6697" s="T503">dĭ</ta>
            <ta e="T505" id="Seg_6698" s="T504">kuza</ta>
            <ta e="T506" id="Seg_6699" s="T505">kü-laːm-bi</ta>
            <ta e="T507" id="Seg_6700" s="T506">nadə</ta>
            <ta e="T508" id="Seg_6701" s="T507">maʔ-də</ta>
            <ta e="T509" id="Seg_6702" s="T508">a-zittə</ta>
            <ta e="T510" id="Seg_6703" s="T509">pa</ta>
            <ta e="T511" id="Seg_6704" s="T510">pa-ziʔ</ta>
            <ta e="T512" id="Seg_6705" s="T511">pa-gəʔ</ta>
            <ta e="T513" id="Seg_6706" s="T512">i</ta>
            <ta e="T514" id="Seg_6707" s="T513">dĭbər</ta>
            <ta e="T515" id="Seg_6708" s="T514">dĭ-m</ta>
            <ta e="T516" id="Seg_6709" s="T515">hen-zittə</ta>
            <ta e="T517" id="Seg_6710" s="T516">i</ta>
            <ta e="T518" id="Seg_6711" s="T517">kroːs</ta>
            <ta e="T519" id="Seg_6712" s="T518">a-zittə</ta>
            <ta e="T520" id="Seg_6713" s="T519">dĭn</ta>
            <ta e="T521" id="Seg_6714" s="T520">tüj</ta>
            <ta e="T522" id="Seg_6715" s="T521">kan-lV-j</ta>
            <ta e="T523" id="Seg_6716" s="T522">šö</ta>
            <ta e="T524" id="Seg_6717" s="T523">tʼo-Tə</ta>
            <ta e="T525" id="Seg_6718" s="T524">ugaːndə</ta>
            <ta e="T526" id="Seg_6719" s="T525">kuvas</ta>
            <ta e="T527" id="Seg_6720" s="T526">tura-jəʔ</ta>
            <ta e="T528" id="Seg_6721" s="T527">nu-gA-jəʔ</ta>
            <ta e="T529" id="Seg_6722" s="T528">dĭ-zAŋ</ta>
            <ta e="T530" id="Seg_6723" s="T529">dĭ-zAŋ</ta>
            <ta e="T533" id="Seg_6724" s="T532">pi-jəʔ</ta>
            <ta e="T535" id="Seg_6725" s="T534">hen-bi-jəʔ</ta>
            <ta e="T536" id="Seg_6726" s="T535">kuznek</ta>
            <ta e="T537" id="Seg_6727" s="T536">kuznek-ə-n</ta>
            <ta e="T538" id="Seg_6728" s="T537">toʔ-gəndə</ta>
            <ta e="T539" id="Seg_6729" s="T538">bar</ta>
            <ta e="T540" id="Seg_6730" s="T539">pa-jəʔ</ta>
            <ta e="T542" id="Seg_6731" s="T541">amnol-bi-jəʔ</ta>
            <ta e="T543" id="Seg_6732" s="T542">pa-jəʔ</ta>
            <ta e="T544" id="Seg_6733" s="T543">amnol-bi-jəʔ</ta>
            <ta e="T546" id="Seg_6734" s="T545">amnol-bi-jəʔ</ta>
            <ta e="T547" id="Seg_6735" s="T546">ugaːndə</ta>
            <ta e="T548" id="Seg_6736" s="T547">kuvas</ta>
            <ta e="T549" id="Seg_6737" s="T548">tura</ta>
            <ta e="T552" id="Seg_6738" s="T551">lem</ta>
            <ta e="T553" id="Seg_6739" s="T552">keʔbde</ta>
            <ta e="T554" id="Seg_6740" s="T553">bar</ta>
            <ta e="T555" id="Seg_6741" s="T554">amnol-bi-jəʔ</ta>
            <ta e="T867" id="Seg_6742" s="T555">lʼevăj</ta>
            <ta e="T557" id="Seg_6743" s="T556">uda-Tə</ta>
            <ta e="T558" id="Seg_6744" s="T557">kuvas</ta>
            <ta e="T559" id="Seg_6745" s="T558">i</ta>
            <ta e="T560" id="Seg_6746" s="T559">pravăj</ta>
            <ta e="T561" id="Seg_6747" s="T560">uda-Tə</ta>
            <ta e="T563" id="Seg_6748" s="T561">kuvas</ta>
            <ta e="T567" id="Seg_6749" s="T566">ugaːndə</ta>
            <ta e="T568" id="Seg_6750" s="T567">kuvas</ta>
            <ta e="T570" id="Seg_6751" s="T569">tura-jəʔ</ta>
            <ta e="T571" id="Seg_6752" s="T570">nu-gA-jəʔ</ta>
            <ta e="T572" id="Seg_6753" s="T571">măn</ta>
            <ta e="T573" id="Seg_6754" s="T572">mej-m</ta>
            <ta e="T575" id="Seg_6755" s="T574">mĭnzər-bi</ta>
            <ta e="T576" id="Seg_6756" s="T575">dĭ</ta>
            <ta e="T577" id="Seg_6757" s="T576">bar</ta>
            <ta e="T578" id="Seg_6758" s="T577">ej</ta>
            <ta e="T579" id="Seg_6759" s="T578">nʼeʔdə</ta>
            <ta e="T580" id="Seg_6760" s="T579">kuza</ta>
            <ta e="T581" id="Seg_6761" s="T580">bar</ta>
            <ta e="T582" id="Seg_6762" s="T581">kudo-nzə-laʔbə</ta>
            <ta e="T583" id="Seg_6763" s="T582">daška</ta>
            <ta e="T584" id="Seg_6764" s="T583">ej</ta>
            <ta e="T585" id="Seg_6765" s="T584">i-lV-l</ta>
            <ta e="T586" id="Seg_6766" s="T585">mĭnzər-zittə</ta>
            <ta e="T587" id="Seg_6767" s="T586">ej</ta>
            <ta e="T588" id="Seg_6768" s="T587">jakšə</ta>
            <ta e="T589" id="Seg_6769" s="T588">ej</ta>
            <ta e="T590" id="Seg_6770" s="T589">nʼeʔdə</ta>
            <ta e="T591" id="Seg_6771" s="T590">dĭgəttə</ta>
            <ta e="T592" id="Seg_6772" s="T591">dĭ</ta>
            <ta e="T593" id="Seg_6773" s="T592">mej-m</ta>
            <ta e="T595" id="Seg_6774" s="T594">bü-m</ta>
            <ta e="T596" id="Seg_6775" s="T595">mĭnzər-bi</ta>
            <ta e="T597" id="Seg_6776" s="T596">tus-ziʔ</ta>
            <ta e="T598" id="Seg_6777" s="T597">dĭgəttə</ta>
            <ta e="T600" id="Seg_6778" s="T599">uja</ta>
            <ta e="T601" id="Seg_6779" s="T600">hʼaʔ-bi</ta>
            <ta e="T602" id="Seg_6780" s="T601">i</ta>
            <ta e="T603" id="Seg_6781" s="T602">dĭbər</ta>
            <ta e="T604" id="Seg_6782" s="T603">hen-ntə-gA</ta>
            <ta e="T605" id="Seg_6783" s="T604">štobɨ</ta>
            <ta e="T606" id="Seg_6784" s="T605">tustʼar-laʔbə</ta>
            <ta e="T607" id="Seg_6785" s="T606">ei-t</ta>
            <ta e="T608" id="Seg_6786" s="T607">ei-ə-n</ta>
            <ta e="T609" id="Seg_6787" s="T608">ne</ta>
            <ta e="T610" id="Seg_6788" s="T609">kan-zittə</ta>
            <ta e="T613" id="Seg_6789" s="T612">ugaːndə</ta>
            <ta e="T614" id="Seg_6790" s="T613">bü</ta>
            <ta e="T615" id="Seg_6791" s="T614">nadə</ta>
            <ta e="T616" id="Seg_6792" s="T615">par-luʔbdə-zittə</ta>
            <ta e="T617" id="Seg_6793" s="T616">ugaːndə</ta>
            <ta e="T618" id="Seg_6794" s="T617">kuŋgə</ta>
            <ta e="T619" id="Seg_6795" s="T618">ed-liA</ta>
            <ta e="T620" id="Seg_6796" s="T868">bar</ta>
            <ta e="T621" id="Seg_6797" s="T620">pa-jəʔ</ta>
            <ta e="T622" id="Seg_6798" s="T621">naga</ta>
            <ta e="T623" id="Seg_6799" s="T622">onʼiʔ</ta>
            <ta e="T624" id="Seg_6800" s="T623">da</ta>
            <ta e="T625" id="Seg_6801" s="T624">pa</ta>
            <ta e="T627" id="Seg_6802" s="T625">naga</ta>
            <ta e="T629" id="Seg_6803" s="T628">šonə-gA-m</ta>
            <ta e="T631" id="Seg_6804" s="T630">nüj-laʔbə-m</ta>
            <ta e="T632" id="Seg_6805" s="T631">dʼije-Tə</ta>
            <ta e="T633" id="Seg_6806" s="T632">kandə-gA-m</ta>
            <ta e="T634" id="Seg_6807" s="T633">dʼije</ta>
            <ta e="T636" id="Seg_6808" s="T635">nüj-laʔbə-m</ta>
            <ta e="T637" id="Seg_6809" s="T636">bü-Tə</ta>
            <ta e="T638" id="Seg_6810" s="T637">kandə-gA-m</ta>
            <ta e="T639" id="Seg_6811" s="T638">bü</ta>
            <ta e="T640" id="Seg_6812" s="T639">nüj-laʔbə-m</ta>
            <ta e="T641" id="Seg_6813" s="T640">pa-Kən</ta>
            <ta e="T642" id="Seg_6814" s="T641">kandə-gA-m</ta>
            <ta e="T643" id="Seg_6815" s="T642">pa</ta>
            <ta e="T644" id="Seg_6816" s="T643">nüj-laʔbə-m</ta>
            <ta e="T645" id="Seg_6817" s="T644">măn</ta>
            <ta e="T646" id="Seg_6818" s="T645">aba-m</ta>
            <ta e="T647" id="Seg_6819" s="T646">Kirelʼ-Tə</ta>
            <ta e="T648" id="Seg_6820" s="T647">kan-bi</ta>
            <ta e="T650" id="Seg_6821" s="T649">i-bi</ta>
            <ta e="T651" id="Seg_6822" s="T650">kola</ta>
            <ta e="T653" id="Seg_6823" s="T652">iʔgö</ta>
            <ta e="T654" id="Seg_6824" s="T653">dʼabə-bi</ta>
            <ta e="T655" id="Seg_6825" s="T654">miʔ</ta>
            <ta e="T656" id="Seg_6826" s="T655">arəm-ntə-bi-bAʔ</ta>
            <ta e="T657" id="Seg_6827" s="T656">i</ta>
            <ta e="T658" id="Seg_6828" s="T657">tustʼar-bi-bAʔ</ta>
            <ta e="T659" id="Seg_6829" s="T658">i</ta>
            <ta e="T660" id="Seg_6830" s="T659">pür-bi-bAʔ</ta>
            <ta e="T661" id="Seg_6831" s="T660">i</ta>
            <ta e="T662" id="Seg_6832" s="T661">am-bi-bAʔ</ta>
            <ta e="T663" id="Seg_6833" s="T662">miʔ</ta>
            <ta e="T664" id="Seg_6834" s="T663">mĭn-bieʔ</ta>
            <ta e="T665" id="Seg_6835" s="T664">i</ta>
            <ta e="T666" id="Seg_6836" s="T665">ugaːndə</ta>
            <ta e="T667" id="Seg_6837" s="T666">urgo</ta>
            <ta e="T668" id="Seg_6838" s="T667">bü</ta>
            <ta e="T669" id="Seg_6839" s="T668">dĭn</ta>
            <ta e="T670" id="Seg_6840" s="T669">bar</ta>
            <ta e="T671" id="Seg_6841" s="T670">sedʼi-jəʔ</ta>
            <ta e="T672" id="Seg_6842" s="T671">barəʔ-bi-bieʔ</ta>
            <ta e="T673" id="Seg_6843" s="T672">kola</ta>
            <ta e="T674" id="Seg_6844" s="T673">dʼabə-bi-bAʔ</ta>
            <ta e="T675" id="Seg_6845" s="T674">kuza</ta>
            <ta e="T676" id="Seg_6846" s="T675">bar</ta>
            <ta e="T677" id="Seg_6847" s="T676">ne-m</ta>
            <ta e="T678" id="Seg_6848" s="T677">pʼaŋdə-bi</ta>
            <ta e="T679" id="Seg_6849" s="T678">a</ta>
            <ta e="T680" id="Seg_6850" s="T679">bos-də</ta>
            <ta e="T681" id="Seg_6851" s="T680">pa-Tə</ta>
            <ta e="T682" id="Seg_6852" s="T681">edə-luʔbdə-bi</ta>
            <ta e="T684" id="Seg_6853" s="T683">urgaːba</ta>
            <ta e="T685" id="Seg_6854" s="T684">ugaːndə</ta>
            <ta e="T686" id="Seg_6855" s="T685">tar-də</ta>
            <ta e="T687" id="Seg_6856" s="T686">iʔgö</ta>
            <ta e="T688" id="Seg_6857" s="T687">bar</ta>
            <ta e="T689" id="Seg_6858" s="T688">uja</ta>
            <ta e="T690" id="Seg_6859" s="T689">ej</ta>
            <ta e="T691" id="Seg_6860" s="T690">ed-liA</ta>
            <ta e="T692" id="Seg_6861" s="T691">ugaːndə</ta>
            <ta e="T693" id="Seg_6862" s="T692">dĭ</ta>
            <ta e="T694" id="Seg_6863" s="T693">nömər</ta>
            <ta e="T695" id="Seg_6864" s="T694">ugaːndə</ta>
            <ta e="T696" id="Seg_6865" s="T695">kəbə</ta>
            <ta e="T697" id="Seg_6866" s="T696">üjü-t</ta>
            <ta e="T698" id="Seg_6867" s="T697">ugaːndə</ta>
            <ta e="T699" id="Seg_6868" s="T698">nʼeʔdə</ta>
            <ta e="T700" id="Seg_6869" s="T699">nʼeʔdə</ta>
            <ta e="T701" id="Seg_6870" s="T700">kada-jəʔ</ta>
            <ta e="T702" id="Seg_6871" s="T701">ugaːndə</ta>
            <ta e="T703" id="Seg_6872" s="T702">urgo</ta>
            <ta e="T704" id="Seg_6873" s="T703">urgo</ta>
            <ta e="T705" id="Seg_6874" s="T704">men</ta>
            <ta e="T706" id="Seg_6875" s="T705">men-ziʔ</ta>
            <ta e="T707" id="Seg_6876" s="T706">urgo</ta>
            <ta e="T708" id="Seg_6877" s="T707">men-ziʔ</ta>
            <ta e="T709" id="Seg_6878" s="T708">sima-t</ta>
            <ta e="T710" id="Seg_6879" s="T709">üdʼüge</ta>
            <ta e="T711" id="Seg_6880" s="T710">ku-jəʔ</ta>
            <ta e="T712" id="Seg_6881" s="T711">üdʼüge</ta>
            <ta e="T713" id="Seg_6882" s="T712">măn</ta>
            <ta e="T714" id="Seg_6883" s="T713">tugan-m</ta>
            <ta e="T715" id="Seg_6884" s="T714">šo-bi</ta>
            <ta e="T716" id="Seg_6885" s="T715">nʼi</ta>
            <ta e="T717" id="Seg_6886" s="T716">šo-bi</ta>
            <ta e="T718" id="Seg_6887" s="T717">ne-ziʔ</ta>
            <ta e="T719" id="Seg_6888" s="T718">kaga-m</ta>
            <ta e="T720" id="Seg_6889" s="T719">šo-bi</ta>
            <ta e="T721" id="Seg_6890" s="T720">ne-ziʔ</ta>
            <ta e="T723" id="Seg_6891" s="T721">ešši-zAŋ</ta>
            <ta e="T724" id="Seg_6892" s="T723">măn</ta>
            <ta e="T725" id="Seg_6893" s="T724">tugan-m</ta>
            <ta e="T726" id="Seg_6894" s="T725">šo-bi</ta>
            <ta e="T727" id="Seg_6895" s="T726">nʼi-m</ta>
            <ta e="T728" id="Seg_6896" s="T727">šo-bi</ta>
            <ta e="T729" id="Seg_6897" s="T728">ne-ziʔ</ta>
            <ta e="T730" id="Seg_6898" s="T729">kaga-m</ta>
            <ta e="T731" id="Seg_6899" s="T730">šo-bi</ta>
            <ta e="T732" id="Seg_6900" s="T731">ne-ziʔ</ta>
            <ta e="T733" id="Seg_6901" s="T732">koʔbdo-m</ta>
            <ta e="T734" id="Seg_6902" s="T733">tibi-ziʔ</ta>
            <ta e="T735" id="Seg_6903" s="T734">šo-bi</ta>
            <ta e="T736" id="Seg_6904" s="T735">ešši-zAŋ</ta>
            <ta e="T737" id="Seg_6905" s="T736">ešši-zAŋ-Tə</ta>
            <ta e="T738" id="Seg_6906" s="T737">dĭ-ziʔ</ta>
            <ta e="T739" id="Seg_6907" s="T738">šo-bi-jəʔ</ta>
            <ta e="T740" id="Seg_6908" s="T739">nadə</ta>
            <ta e="T741" id="Seg_6909" s="T740">dĭ-zAŋ-Tə</ta>
            <ta e="T743" id="Seg_6910" s="T742">oʔbdə-zittə</ta>
            <ta e="T744" id="Seg_6911" s="T743">dĭ-zAŋ</ta>
            <ta e="T746" id="Seg_6912" s="T745">am-zittə</ta>
            <ta e="T747" id="Seg_6913" s="T746">mĭ-zittə</ta>
            <ta e="T748" id="Seg_6914" s="T747">măn</ta>
            <ta e="T749" id="Seg_6915" s="T748">nʼi-m</ta>
            <ta e="T750" id="Seg_6916" s="T749">bar</ta>
            <ta e="T751" id="Seg_6917" s="T750">tăŋ</ta>
            <ta e="T752" id="Seg_6918" s="T751">ĭzem-liA</ta>
            <ta e="T753" id="Seg_6919" s="T752">măn</ta>
            <ta e="T754" id="Seg_6920" s="T753">dĭ-Tə</ta>
            <ta e="T755" id="Seg_6921" s="T754">măn-bi-m</ta>
            <ta e="T756" id="Seg_6922" s="T755">kudaj-Tə</ta>
            <ta e="T757" id="Seg_6923" s="T756">numan üzə-ʔ</ta>
            <ta e="T758" id="Seg_6924" s="T757">pi-ʔ</ta>
            <ta e="T759" id="Seg_6925" s="T758">dĭʔə</ta>
            <ta e="T760" id="Seg_6926" s="T759">šal-də</ta>
            <ta e="T761" id="Seg_6927" s="T760">kăštə-ʔ</ta>
            <ta e="T762" id="Seg_6928" s="T761">bos-kənə</ta>
            <ta e="T763" id="Seg_6929" s="T762">dĭ</ta>
            <ta e="T764" id="Seg_6930" s="T763">bar</ta>
            <ta e="T765" id="Seg_6931" s="T764">kăštə-bi</ta>
            <ta e="T766" id="Seg_6932" s="T765">šo-ʔ</ta>
            <ta e="T767" id="Seg_6933" s="T766">măna</ta>
            <ta e="T768" id="Seg_6934" s="T767">šal-l</ta>
            <ta e="T769" id="Seg_6935" s="T768">det-ʔ</ta>
            <ta e="T770" id="Seg_6936" s="T769">tʼezer-t</ta>
            <ta e="T771" id="Seg_6937" s="T770">măna</ta>
            <ta e="T772" id="Seg_6938" s="T771">măn</ta>
            <ta e="T773" id="Seg_6939" s="T772">taldʼen</ta>
            <ta e="T774" id="Seg_6940" s="T773">tura</ta>
            <ta e="T775" id="Seg_6941" s="T774">bazə-bi-m</ta>
            <ta e="T776" id="Seg_6942" s="T775">kuriza</ta>
            <ta e="T777" id="Seg_6943" s="T776">tüʔ-bi</ta>
            <ta e="T778" id="Seg_6944" s="T777">tože</ta>
            <ta e="T779" id="Seg_6945" s="T778">bazə-bi-m</ta>
            <ta e="T780" id="Seg_6946" s="T779">dĭgəttə</ta>
            <ta e="T782" id="Seg_6947" s="T781">bazə-bi-m</ta>
            <ta e="T783" id="Seg_6948" s="T782">dĭgəttə</ta>
            <ta e="T784" id="Seg_6949" s="T783">kan-bi-m</ta>
            <ta e="T785" id="Seg_6950" s="T784">bü</ta>
            <ta e="T786" id="Seg_6951" s="T785">tažor-zittə</ta>
            <ta e="T787" id="Seg_6952" s="T786">multʼa-Tə</ta>
            <ta e="T789" id="Seg_6953" s="T787">oʔb</ta>
            <ta e="T790" id="Seg_6954" s="T789">muktuʔ</ta>
            <ta e="T791" id="Seg_6955" s="T790">aspaʔ</ta>
            <ta e="T792" id="Seg_6956" s="T791">det-bi-m</ta>
            <ta e="T793" id="Seg_6957" s="T792">măn</ta>
            <ta e="T794" id="Seg_6958" s="T793">teinen</ta>
            <ta e="T795" id="Seg_6959" s="T794">ertə-n</ta>
            <ta e="T796" id="Seg_6960" s="T795">uʔbdə-bi-m</ta>
            <ta e="T799" id="Seg_6961" s="T798">tʼala</ta>
            <ta e="T800" id="Seg_6962" s="T799">togonər-bi-m</ta>
            <ta e="T801" id="Seg_6963" s="T800">bar</ta>
            <ta e="T802" id="Seg_6964" s="T801">tarar-luʔbdə-bi-m</ta>
            <ta e="T804" id="Seg_6965" s="T803">nüdʼi-n</ta>
            <ta e="T805" id="Seg_6966" s="T804">nüdʼi-n</ta>
            <ta e="T806" id="Seg_6967" s="T805">šo-bi</ta>
            <ta e="T807" id="Seg_6968" s="T806">măn</ta>
            <ta e="T808" id="Seg_6969" s="T807">kunol-luʔbdə-bi-m</ta>
            <ta e="T809" id="Seg_6970" s="T808">măn</ta>
            <ta e="T810" id="Seg_6971" s="T809">dĭ-zem</ta>
            <ta e="T811" id="Seg_6972" s="T810">ku-zittə</ta>
            <ta e="T813" id="Seg_6973" s="T812">nʼe</ta>
            <ta e="T814" id="Seg_6974" s="T813">axota</ta>
            <ta e="T815" id="Seg_6975" s="T814">dĭ-zAŋ</ta>
            <ta e="T816" id="Seg_6976" s="T815">bar</ta>
            <ta e="T817" id="Seg_6977" s="T816">üge</ta>
            <ta e="T819" id="Seg_6978" s="T818">šʼaːm-laʔbə-jəʔ</ta>
            <ta e="T820" id="Seg_6979" s="T819">šʼaːm-laʔbə-jəʔ</ta>
            <ta e="T821" id="Seg_6980" s="T820">dĭ-zAŋ</ta>
            <ta e="T822" id="Seg_6981" s="T821">arda</ta>
            <ta e="T824" id="Seg_6982" s="T823">nʼilgö-zittə</ta>
            <ta e="T825" id="Seg_6983" s="T824">nʼe</ta>
            <ta e="T826" id="Seg_6984" s="T825">axota</ta>
            <ta e="T827" id="Seg_6985" s="T826">pušaj</ta>
            <ta e="T828" id="Seg_6986" s="T827">măna</ta>
            <ta e="T829" id="Seg_6987" s="T828">sima-gəndə</ta>
            <ta e="T830" id="Seg_6988" s="T829">ej</ta>
            <ta e="T831" id="Seg_6989" s="T830">šo-lV-jəʔ</ta>
            <ta e="T832" id="Seg_6990" s="T831">măna</ta>
            <ta e="T833" id="Seg_6991" s="T832">dĭ-zem</ta>
            <ta e="T834" id="Seg_6992" s="T833">ej</ta>
            <ta e="T835" id="Seg_6993" s="T834">kereʔ</ta>
            <ta e="T836" id="Seg_6994" s="T835">ulu-m</ta>
            <ta e="T837" id="Seg_6995" s="T836">eʔbdə-m</ta>
            <ta e="T838" id="Seg_6996" s="T837">ku-zAŋ-m</ta>
            <ta e="T839" id="Seg_6997" s="T838">tĭme-m</ta>
            <ta e="T840" id="Seg_6998" s="T839">mon-zAŋ-l</ta>
            <ta e="T841" id="Seg_6999" s="T840">šĭkə-m</ta>
            <ta e="T842" id="Seg_7000" s="T841">sima-m</ta>
            <ta e="T843" id="Seg_7001" s="T842">püje-m</ta>
            <ta e="T844" id="Seg_7002" s="T843">uda-zAŋ-m</ta>
            <ta e="T845" id="Seg_7003" s="T844">bögəl-m</ta>
            <ta e="T847" id="Seg_7004" s="T846">sĭj-m</ta>
            <ta e="T848" id="Seg_7005" s="T847">šüjə-m</ta>
            <ta e="T849" id="Seg_7006" s="T848">nanə-m</ta>
            <ta e="T851" id="Seg_7007" s="T850">bögəl-m</ta>
            <ta e="T852" id="Seg_7008" s="T851">köten-də</ta>
            <ta e="T853" id="Seg_7009" s="T852">üjü-m</ta>
            <ta e="T854" id="Seg_7010" s="T853">aŋ-də</ta>
            <ta e="T855" id="Seg_7011" s="T854">sĭj-m</ta>
            <ta e="T856" id="Seg_7012" s="T855">sĭj-də</ta>
            <ta e="T858" id="Seg_7013" s="T857">nüjü-zAŋ-də</ta>
            <ta e="T861" id="Seg_7014" s="T860">le-zAŋ-də</ta>
            <ta e="T863" id="Seg_7015" s="T862">kem</ta>
         </annotation>
         <annotation name="ge" tierref="ge-PKZ">
            <ta e="T5" id="Seg_7016" s="T4">here</ta>
            <ta e="T6" id="Seg_7017" s="T5">come-FUT-2SG</ta>
            <ta e="T7" id="Seg_7018" s="T6">so</ta>
            <ta e="T8" id="Seg_7019" s="T7">I.ACC</ta>
            <ta e="T9" id="Seg_7020" s="T8">eye.[NOM.SG]</ta>
            <ta e="T10" id="Seg_7021" s="T9">take-IMP.2SG</ta>
            <ta e="T11" id="Seg_7022" s="T10">you.NOM</ta>
            <ta e="T12" id="Seg_7023" s="T11">man.[NOM.SG]</ta>
            <ta e="T13" id="Seg_7024" s="T12">where.to</ta>
            <ta e="T14" id="Seg_7025" s="T13">go-PST.[3SG]</ta>
            <ta e="T15" id="Seg_7026" s="T14">Sayan.mountains-LAT</ta>
            <ta e="T869" id="Seg_7027" s="T16">go-CVB</ta>
            <ta e="T17" id="Seg_7028" s="T869">disappear-PST.[3SG]</ta>
            <ta e="T18" id="Seg_7029" s="T17">horse.[NOM.SG]</ta>
            <ta e="T19" id="Seg_7030" s="T18">alone</ta>
            <ta e="T20" id="Seg_7031" s="T19">go-PST.[3SG]</ta>
            <ta e="T21" id="Seg_7032" s="T20">no</ta>
            <ta e="T22" id="Seg_7033" s="T21">two-COLL</ta>
            <ta e="T870" id="Seg_7034" s="T22">go-CVB</ta>
            <ta e="T23" id="Seg_7035" s="T870">disappear-PST-3PL</ta>
            <ta e="T24" id="Seg_7036" s="T23">PTCL</ta>
            <ta e="T25" id="Seg_7037" s="T24">when</ta>
            <ta e="T27" id="Seg_7038" s="T26">go-PST-3PL</ta>
            <ta e="T29" id="Seg_7039" s="T28">yesterday</ta>
            <ta e="T30" id="Seg_7040" s="T29">go-PST-3PL</ta>
            <ta e="T31" id="Seg_7041" s="T30">very</ta>
            <ta e="T32" id="Seg_7042" s="T31">early-LOC.ADV</ta>
            <ta e="T33" id="Seg_7043" s="T32">what.[NOM.SG]</ta>
            <ta e="T34" id="Seg_7044" s="T33">lie-DUR-2SG</ta>
            <ta e="T35" id="Seg_7045" s="T34">NEG</ta>
            <ta e="T38" id="Seg_7046" s="T37">NEG</ta>
            <ta e="T40" id="Seg_7047" s="T39">%%-PRS-2SG</ta>
            <ta e="T41" id="Seg_7048" s="T40">%%-LAT</ta>
            <ta e="T43" id="Seg_7049" s="T42">place-IMP.2SG.O</ta>
            <ta e="T44" id="Seg_7050" s="T43">PTCL</ta>
            <ta e="T45" id="Seg_7051" s="T44">shirt-NOM/GEN/ACC.1SG</ta>
            <ta e="T46" id="Seg_7052" s="T45">tear-MOM-PST.[3SG]</ta>
            <ta e="T47" id="Seg_7053" s="T46">hole.[NOM.SG]</ta>
            <ta e="T48" id="Seg_7054" s="T47">become-RES-PST.[3SG]</ta>
            <ta e="T49" id="Seg_7055" s="T48">I.NOM</ta>
            <ta e="T50" id="Seg_7056" s="T49">then</ta>
            <ta e="T51" id="Seg_7057" s="T50">this-ACC</ta>
            <ta e="T53" id="Seg_7058" s="T52">sew-PST-1SG</ta>
            <ta e="T54" id="Seg_7059" s="T53">thread-3PL-INS</ta>
            <ta e="T55" id="Seg_7060" s="T54">needle-INS</ta>
            <ta e="T56" id="Seg_7061" s="T55">thimble.[NOM.SG]</ta>
            <ta e="T57" id="Seg_7062" s="T56">bell.tower-LOC</ta>
            <ta e="T58" id="Seg_7063" s="T57">PTCL</ta>
            <ta e="T59" id="Seg_7064" s="T58">rumble-DUR.[3SG]</ta>
            <ta e="T60" id="Seg_7065" s="T59">bell.[NOM.SG]</ta>
            <ta e="T61" id="Seg_7066" s="T60">one.should</ta>
            <ta e="T62" id="Seg_7067" s="T61">go-INF.LAT</ta>
            <ta e="T63" id="Seg_7068" s="T62">God-LAT</ta>
            <ta e="T65" id="Seg_7069" s="T64">fall-INF.LAT</ta>
            <ta e="T66" id="Seg_7070" s="T65">then</ta>
            <ta e="T67" id="Seg_7071" s="T66">call-PRS-3PL</ta>
            <ta e="T68" id="Seg_7072" s="T67">this-PL</ta>
            <ta e="T69" id="Seg_7073" s="T68">PTCL</ta>
            <ta e="T70" id="Seg_7074" s="T69">hang.up-IMP.2SG.O</ta>
            <ta e="T71" id="Seg_7075" s="T70">horse-LAT</ta>
            <ta e="T72" id="Seg_7076" s="T71">bell.[NOM.SG]</ta>
            <ta e="T73" id="Seg_7077" s="T72">mind-NOM/GEN/ACC.3SG</ta>
            <ta e="T74" id="Seg_7078" s="T73">NEG.EX.[3SG]</ta>
            <ta e="T75" id="Seg_7079" s="T74">PTCL</ta>
            <ta e="T76" id="Seg_7080" s="T75">what.[NOM.SG]=INDEF</ta>
            <ta e="T77" id="Seg_7081" s="T76">NEG</ta>
            <ta e="T78" id="Seg_7082" s="T77">know-1SG</ta>
            <ta e="T79" id="Seg_7083" s="T78">what.[NOM.SG]=INDEF</ta>
            <ta e="T80" id="Seg_7084" s="T79">NEG</ta>
            <ta e="T81" id="Seg_7085" s="T80">can-PRS-1SG</ta>
            <ta e="T82" id="Seg_7086" s="T81">tell-INF.LAT</ta>
            <ta e="T83" id="Seg_7087" s="T82">PTCL</ta>
            <ta e="T84" id="Seg_7088" s="T83">mind-NOM/GEN/ACC.3SG</ta>
            <ta e="T85" id="Seg_7089" s="T84">NEG.EX.[3SG]</ta>
            <ta e="T86" id="Seg_7090" s="T85">forest-LAT</ta>
            <ta e="T87" id="Seg_7091" s="T86">go-PST-1SG</ta>
            <ta e="T88" id="Seg_7092" s="T87">PTCL</ta>
            <ta e="T92" id="Seg_7093" s="T91">disappear-DUR.[3SG]</ta>
            <ta e="T93" id="Seg_7094" s="T92">forest-LAT</ta>
            <ta e="T94" id="Seg_7095" s="T93">go-PST-1SG</ta>
            <ta e="T95" id="Seg_7096" s="T94">PTCL</ta>
            <ta e="T96" id="Seg_7097" s="T95">disappear-DUR-PST-1SG</ta>
            <ta e="T97" id="Seg_7098" s="T96">then</ta>
            <ta e="T98" id="Seg_7099" s="T97">shout-PST-1SG</ta>
            <ta e="T99" id="Seg_7100" s="T98">PTCL</ta>
            <ta e="T100" id="Seg_7101" s="T99">then</ta>
            <ta e="T101" id="Seg_7102" s="T100">come-PST-1SG</ta>
            <ta e="T102" id="Seg_7103" s="T101">long.time</ta>
            <ta e="T103" id="Seg_7104" s="T102">sit-PST.[3SG]</ta>
            <ta e="T104" id="Seg_7105" s="T103">what.[NOM.SG]=INDEF</ta>
            <ta e="T106" id="Seg_7106" s="T104">PTCL</ta>
            <ta e="T107" id="Seg_7107" s="T106">what.[NOM.SG]=INDEF</ta>
            <ta e="T108" id="Seg_7108" s="T107">NEG</ta>
            <ta e="T109" id="Seg_7109" s="T108">speak-PST.[3SG]</ta>
            <ta e="T110" id="Seg_7110" s="T109">this.[NOM.SG]</ta>
            <ta e="T111" id="Seg_7111" s="T110">walk-PRS.[3SG]</ta>
            <ta e="T112" id="Seg_7112" s="T111">and</ta>
            <ta e="T113" id="Seg_7113" s="T112">I.NOM</ta>
            <ta e="T114" id="Seg_7114" s="T113">this-ACC</ta>
            <ta e="T116" id="Seg_7115" s="T115">catch.up-PRS-1SG</ta>
            <ta e="T117" id="Seg_7116" s="T116">NEG.AUX-IMP.2SG</ta>
            <ta e="T118" id="Seg_7117" s="T117">go-EP-CNG</ta>
            <ta e="T119" id="Seg_7118" s="T118">return-EP-IMP.2SG</ta>
            <ta e="T120" id="Seg_7119" s="T119">here</ta>
            <ta e="T121" id="Seg_7120" s="T120">I.NOM</ta>
            <ta e="T122" id="Seg_7121" s="T121">you.DAT</ta>
            <ta e="T123" id="Seg_7122" s="T122">say-PRS-1SG</ta>
            <ta e="T124" id="Seg_7123" s="T123">return-EP-IMP.2SG</ta>
            <ta e="T125" id="Seg_7124" s="T124">here</ta>
            <ta e="T126" id="Seg_7125" s="T125">I.NOM</ta>
            <ta e="T127" id="Seg_7126" s="T126">very</ta>
            <ta e="T128" id="Seg_7127" s="T127">big.[NOM.SG]</ta>
            <ta e="T129" id="Seg_7128" s="T128">people.[NOM.SG]</ta>
            <ta e="T130" id="Seg_7129" s="T129">be-PRS.[3SG]</ta>
            <ta e="T131" id="Seg_7130" s="T130">bread.[NOM.SG]</ta>
            <ta e="T132" id="Seg_7131" s="T131">many</ta>
            <ta e="T134" id="Seg_7132" s="T133">one.needs</ta>
            <ta e="T135" id="Seg_7133" s="T134">take-IMP.2SG</ta>
            <ta e="T136" id="Seg_7134" s="T135">back-NOM/GEN/ACC.3SG</ta>
            <ta e="T137" id="Seg_7135" s="T136">this-ACC</ta>
            <ta e="T138" id="Seg_7136" s="T137">hand-NOM/GEN/ACC.2SG</ta>
            <ta e="T139" id="Seg_7137" s="T138">NEG</ta>
            <ta e="T140" id="Seg_7138" s="T139">put-IMP.2SG.O</ta>
            <ta e="T142" id="Seg_7139" s="T140">%touch-IMP.2SG.O</ta>
            <ta e="T143" id="Seg_7140" s="T142">this-ACC</ta>
            <ta e="T144" id="Seg_7141" s="T143">%touch-IMP.2SG.O</ta>
            <ta e="T145" id="Seg_7142" s="T144">this-ACC</ta>
            <ta e="T146" id="Seg_7143" s="T145">hand-INS</ta>
            <ta e="T148" id="Seg_7144" s="T146">self-%LAT/LOC.1/2SG</ta>
            <ta e="T149" id="Seg_7145" s="T148">many</ta>
            <ta e="T150" id="Seg_7146" s="T149">take-PST-1SG</ta>
            <ta e="T151" id="Seg_7147" s="T150">and</ta>
            <ta e="T152" id="Seg_7148" s="T151">this-LAT</ta>
            <ta e="T153" id="Seg_7149" s="T152">few-ACC</ta>
            <ta e="T154" id="Seg_7150" s="T153">take-PST-1SG</ta>
            <ta e="T155" id="Seg_7151" s="T154">self-LAT/LOC.3SG</ta>
            <ta e="T156" id="Seg_7152" s="T155">many</ta>
            <ta e="T157" id="Seg_7153" s="T156">take-PRS.[3SG]</ta>
            <ta e="T158" id="Seg_7154" s="T157">and</ta>
            <ta e="T159" id="Seg_7155" s="T158">this-LAT</ta>
            <ta e="T160" id="Seg_7156" s="T159">few-ACC</ta>
            <ta e="T161" id="Seg_7157" s="T160">take-PRS.[3SG]</ta>
            <ta e="T162" id="Seg_7158" s="T161">six.[NOM.SG]</ta>
            <ta e="T163" id="Seg_7159" s="T162">PTCL</ta>
            <ta e="T164" id="Seg_7160" s="T163">speak-PST-1PL</ta>
            <ta e="T165" id="Seg_7161" s="T164">we.NOM</ta>
            <ta e="T166" id="Seg_7162" s="T165">PTCL</ta>
            <ta e="T167" id="Seg_7163" s="T166">small.[NOM.SG]</ta>
            <ta e="T168" id="Seg_7164" s="T167">be-PST-1PL</ta>
            <ta e="T169" id="Seg_7165" s="T168">PTCL</ta>
            <ta e="T170" id="Seg_7166" s="T169">play-DUR-PST-1PL</ta>
            <ta e="T171" id="Seg_7167" s="T170">tree-PL</ta>
            <ta e="T172" id="Seg_7168" s="T171">take-FUT-3SG</ta>
            <ta e="T173" id="Seg_7169" s="T172">and</ta>
            <ta e="T174" id="Seg_7170" s="T173">PTCL</ta>
            <ta e="T175" id="Seg_7171" s="T174">oh</ta>
            <ta e="T176" id="Seg_7172" s="T175">one.[NOM.SG]</ta>
            <ta e="T177" id="Seg_7173" s="T176">run-FUT-3SG</ta>
            <ta e="T178" id="Seg_7174" s="T177">and</ta>
            <ta e="T179" id="Seg_7175" s="T178">we.NOM</ta>
            <ta e="T180" id="Seg_7176" s="T179">PTCL</ta>
            <ta e="T181" id="Seg_7177" s="T180">hide-RES-PST-1PL</ta>
            <ta e="T182" id="Seg_7178" s="T181">then</ta>
            <ta e="T183" id="Seg_7179" s="T182">this.[NOM.SG]</ta>
            <ta e="T184" id="Seg_7180" s="T183">PTCL</ta>
            <ta e="T185" id="Seg_7181" s="T184">find-PRS.[3SG]</ta>
            <ta e="T186" id="Seg_7182" s="T185">find-PRS.[3SG]</ta>
            <ta e="T187" id="Seg_7183" s="T186">NEG</ta>
            <ta e="T189" id="Seg_7184" s="T188">NEG</ta>
            <ta e="T190" id="Seg_7185" s="T189">can-PRS.[3SG]</ta>
            <ta e="T191" id="Seg_7186" s="T190">find-INF.LAT</ta>
            <ta e="T192" id="Seg_7187" s="T191">we.NOM</ta>
            <ta e="T193" id="Seg_7188" s="T192">PTCL</ta>
            <ta e="T194" id="Seg_7189" s="T193">again</ta>
            <ta e="T195" id="Seg_7190" s="T194">bind-DUR-1PL</ta>
            <ta e="T197" id="Seg_7191" s="T196">we.NOM</ta>
            <ta e="T198" id="Seg_7192" s="T197">PTCL</ta>
            <ta e="T199" id="Seg_7193" s="T198">NEG</ta>
            <ta e="T200" id="Seg_7194" s="T199">scold-MOM-PST-1PL</ta>
            <ta e="T201" id="Seg_7195" s="T200">good.[NOM.SG]</ta>
            <ta e="T203" id="Seg_7196" s="T201">PTCL</ta>
            <ta e="T205" id="Seg_7197" s="T204">good.[NOM.SG]</ta>
            <ta e="T207" id="Seg_7198" s="T205">PTCL</ta>
            <ta e="T210" id="Seg_7199" s="T209">we.NOM</ta>
            <ta e="T211" id="Seg_7200" s="T210">PTCL</ta>
            <ta e="T212" id="Seg_7201" s="T211">good.[NOM.SG]</ta>
            <ta e="T213" id="Seg_7202" s="T212">play-PST-1PL</ta>
            <ta e="T214" id="Seg_7203" s="T213">we.NOM</ta>
            <ta e="T215" id="Seg_7204" s="T214">NEG</ta>
            <ta e="T216" id="Seg_7205" s="T215">get.angry-FUT-PST-1PL</ta>
            <ta e="T217" id="Seg_7206" s="T216">man-PL</ta>
            <ta e="T218" id="Seg_7207" s="T217">and</ta>
            <ta e="T219" id="Seg_7208" s="T218">woman-PL</ta>
            <ta e="T220" id="Seg_7209" s="T219">sit-PST-3PL</ta>
            <ta e="T221" id="Seg_7210" s="T220">and</ta>
            <ta e="T222" id="Seg_7211" s="T221">look-DUR-PST-3PL</ta>
            <ta e="T223" id="Seg_7212" s="T222">how</ta>
            <ta e="T224" id="Seg_7213" s="T223">we.NOM</ta>
            <ta e="T225" id="Seg_7214" s="T224">play-DUR-1PL</ta>
            <ta e="T226" id="Seg_7215" s="T225">who.[NOM.SG]</ta>
            <ta e="T229" id="Seg_7216" s="T228">smoke-IPFVZ-PST.[3SG]</ta>
            <ta e="T230" id="Seg_7217" s="T229">tobacco.[NOM.SG]</ta>
            <ta e="T231" id="Seg_7218" s="T230">who.[NOM.SG]</ta>
            <ta e="T232" id="Seg_7219" s="T231">NEG</ta>
            <ta e="T233" id="Seg_7220" s="T232">smoke-IPFVZ-PST.[3SG]</ta>
            <ta e="T234" id="Seg_7221" s="T233">who.[NOM.SG]</ta>
            <ta e="T235" id="Seg_7222" s="T234">PTCL</ta>
            <ta e="T237" id="Seg_7223" s="T236">pipe-INS</ta>
            <ta e="T238" id="Seg_7224" s="T237">PTCL</ta>
            <ta e="T239" id="Seg_7225" s="T238">smoke-DUR.[3SG]</ta>
            <ta e="T240" id="Seg_7226" s="T239">who.[NOM.SG]</ta>
            <ta e="T241" id="Seg_7227" s="T240">NEG</ta>
            <ta e="T242" id="Seg_7228" s="T241">smoke-DUR.[3SG]</ta>
            <ta e="T243" id="Seg_7229" s="T242">sheep.[NOM.SG]</ta>
            <ta e="T244" id="Seg_7230" s="T243">very</ta>
            <ta e="T245" id="Seg_7231" s="T244">many</ta>
            <ta e="T247" id="Seg_7232" s="T246">two.[NOM.SG]</ta>
            <ta e="T248" id="Seg_7233" s="T247">ten.[NOM.SG]</ta>
            <ta e="T249" id="Seg_7234" s="T248">two.[NOM.SG]</ta>
            <ta e="T250" id="Seg_7235" s="T249">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T251" id="Seg_7236" s="T250">forest-LAT</ta>
            <ta e="T252" id="Seg_7237" s="T251">go-PST.[3SG]</ta>
            <ta e="T253" id="Seg_7238" s="T252">many</ta>
            <ta e="T254" id="Seg_7239" s="T253">hen-PL</ta>
            <ta e="T255" id="Seg_7240" s="T254">kill-PST.[3SG]</ta>
            <ta e="T256" id="Seg_7241" s="T255">and</ta>
            <ta e="T257" id="Seg_7242" s="T256">tent-LAT/LOC.3SG</ta>
            <ta e="T258" id="Seg_7243" s="T257">bring-PST.[3SG]</ta>
            <ta e="T259" id="Seg_7244" s="T258">I.NOM</ta>
            <ta e="T260" id="Seg_7245" s="T259">come-PRS-1SG</ta>
            <ta e="T261" id="Seg_7246" s="T260">Permyakovo.[NOM.SG]</ta>
            <ta e="T262" id="Seg_7247" s="T261">see-PRS-1SG</ta>
            <ta e="T263" id="Seg_7248" s="T262">PTCL</ta>
            <ta e="T264" id="Seg_7249" s="T263">moose.[NOM.SG]</ta>
            <ta e="T265" id="Seg_7250" s="T264">stand-PRS.[3SG]</ta>
            <ta e="T266" id="Seg_7251" s="T265">I.NOM</ta>
            <ta e="T267" id="Seg_7252" s="T266">look-PST-1SG</ta>
            <ta e="T268" id="Seg_7253" s="T267">look-PST-1SG</ta>
            <ta e="T269" id="Seg_7254" s="T268">then</ta>
            <ta e="T270" id="Seg_7255" s="T269">PTCL</ta>
            <ta e="T271" id="Seg_7256" s="T270">shout-MOM-PST-1SG</ta>
            <ta e="T272" id="Seg_7257" s="T271">this.[NOM.SG]</ta>
            <ta e="T273" id="Seg_7258" s="T272">PTCL</ta>
            <ta e="T274" id="Seg_7259" s="T273">run-MOM-PST.[3SG]</ta>
            <ta e="T275" id="Seg_7260" s="T274">water-LAT</ta>
            <ta e="T276" id="Seg_7261" s="T275">%snake.[NOM.SG]</ta>
            <ta e="T277" id="Seg_7262" s="T276">then</ta>
            <ta e="T278" id="Seg_7263" s="T277">this.[NOM.SG]</ta>
            <ta e="T279" id="Seg_7264" s="T278">PTCL</ta>
            <ta e="T280" id="Seg_7265" s="T279">go-MOM-PST.[3SG]</ta>
            <ta e="T281" id="Seg_7266" s="T280">and</ta>
            <ta e="T282" id="Seg_7267" s="T281">run-MOM-PST.[3SG]</ta>
            <ta e="T283" id="Seg_7268" s="T282">I.NOM</ta>
            <ta e="T284" id="Seg_7269" s="T283">come-PST-1SG</ta>
            <ta e="T285" id="Seg_7270" s="T284">and</ta>
            <ta e="T286" id="Seg_7271" s="T285">Vlas-LAT</ta>
            <ta e="T287" id="Seg_7272" s="T286">tell-PST-1SG</ta>
            <ta e="T288" id="Seg_7273" s="T287">this.[NOM.SG]</ta>
            <ta e="T289" id="Seg_7274" s="T288">go-PST.[3SG]</ta>
            <ta e="T290" id="Seg_7275" s="T289">gun-INS</ta>
            <ta e="T291" id="Seg_7276" s="T290">and</ta>
            <ta e="T293" id="Seg_7277" s="T292">kill-PST.[3SG]</ta>
            <ta e="T295" id="Seg_7278" s="T293">cauldron.[NOM.SG]</ta>
            <ta e="T296" id="Seg_7279" s="T295">I.NOM</ta>
            <ta e="T297" id="Seg_7280" s="T296">then</ta>
            <ta e="T298" id="Seg_7281" s="T297">meat.[NOM.SG]</ta>
            <ta e="T299" id="Seg_7282" s="T298">wash-PST-1SG</ta>
            <ta e="T300" id="Seg_7283" s="T299">and</ta>
            <ta e="T301" id="Seg_7284" s="T300">eat-PST-1SG</ta>
            <ta e="T302" id="Seg_7285" s="T301">we.LAT</ta>
            <ta e="T303" id="Seg_7286" s="T302">give-PST.[3SG]</ta>
            <ta e="T304" id="Seg_7287" s="T303">one.[NOM.SG]</ta>
            <ta e="T305" id="Seg_7288" s="T304">cauldron.[NOM.SG]</ta>
            <ta e="T306" id="Seg_7289" s="T866">man.[NOM.SG]</ta>
            <ta e="T307" id="Seg_7290" s="T306">come-PST.[3SG]</ta>
            <ta e="T308" id="Seg_7291" s="T307">I.NOM</ta>
            <ta e="T309" id="Seg_7292" s="T308">there</ta>
            <ta e="T310" id="Seg_7293" s="T309">go-PST-1SG</ta>
            <ta e="T311" id="Seg_7294" s="T310">this-PL</ta>
            <ta e="T312" id="Seg_7295" s="T311">PTCL</ta>
            <ta e="T314" id="Seg_7296" s="T312">speak-DUR-3PL</ta>
            <ta e="T315" id="Seg_7297" s="T314">and</ta>
            <ta e="T316" id="Seg_7298" s="T315">this-PL</ta>
            <ta e="T317" id="Seg_7299" s="T316">PTCL</ta>
            <ta e="T318" id="Seg_7300" s="T317">I.ACC</ta>
            <ta e="T319" id="Seg_7301" s="T318">drive-MOM-PST-3PL</ta>
            <ta e="T320" id="Seg_7302" s="T319">I.NOM</ta>
            <ta e="T321" id="Seg_7303" s="T320">run-MOM-PST-1SG</ta>
            <ta e="T323" id="Seg_7304" s="T321">alone</ta>
            <ta e="T324" id="Seg_7305" s="T323">Kamassian-PL</ta>
            <ta e="T325" id="Seg_7306" s="T324">live-PST-3PL</ta>
            <ta e="T326" id="Seg_7307" s="T325">good.[NOM.SG]</ta>
            <ta e="T327" id="Seg_7308" s="T326">be-PST.[3SG]</ta>
            <ta e="T328" id="Seg_7309" s="T327">meat-NOM/GEN.3SG</ta>
            <ta e="T329" id="Seg_7310" s="T328">many</ta>
            <ta e="T330" id="Seg_7311" s="T329">be-PST.[3SG]</ta>
            <ta e="T331" id="Seg_7312" s="T330">PTCL</ta>
            <ta e="T332" id="Seg_7313" s="T331">eat-PRS-2SG</ta>
            <ta e="T333" id="Seg_7314" s="T332">reindeer-EP-GEN</ta>
            <ta e="T334" id="Seg_7315" s="T333">meat.[NOM.SG]</ta>
            <ta e="T335" id="Seg_7316" s="T334">very</ta>
            <ta e="T336" id="Seg_7317" s="T335">soft.[NOM.SG]</ta>
            <ta e="T337" id="Seg_7318" s="T336">like</ta>
            <ta e="T338" id="Seg_7319" s="T337">bread.[NOM.SG]</ta>
            <ta e="T339" id="Seg_7320" s="T338">many</ta>
            <ta e="T340" id="Seg_7321" s="T339">eat-PRS-2SG</ta>
            <ta e="T341" id="Seg_7322" s="T340">then</ta>
            <ta e="T342" id="Seg_7323" s="T341">water.[NOM.SG]</ta>
            <ta e="T343" id="Seg_7324" s="T342">drink-PRS-2SG</ta>
            <ta e="T344" id="Seg_7325" s="T865">water.[NOM.SG]</ta>
            <ta e="T345" id="Seg_7326" s="T344">drink-PRS-2SG</ta>
            <ta e="T348" id="Seg_7327" s="T345">well</ta>
            <ta e="T349" id="Seg_7328" s="T348">this.[NOM.SG]</ta>
            <ta e="T350" id="Seg_7329" s="T349">chief.[NOM.SG]</ta>
            <ta e="T351" id="Seg_7330" s="T350">very</ta>
            <ta e="T352" id="Seg_7331" s="T351">good.[NOM.SG]</ta>
            <ta e="T353" id="Seg_7332" s="T352">very</ta>
            <ta e="T354" id="Seg_7333" s="T353">beautiful.[NOM.SG]</ta>
            <ta e="T355" id="Seg_7334" s="T354">eye-NOM/GEN.3SG</ta>
            <ta e="T357" id="Seg_7335" s="T356">black.[NOM.SG]</ta>
            <ta e="T358" id="Seg_7336" s="T357">nose-NOM/GEN.3SG</ta>
            <ta e="T359" id="Seg_7337" s="T358">NEG</ta>
            <ta e="T360" id="Seg_7338" s="T359">long.[NOM.SG]</ta>
            <ta e="T361" id="Seg_7339" s="T360">lip-PL-NOM/GEN/ACC.2SG</ta>
            <ta e="T362" id="Seg_7340" s="T361">beautiful.[NOM.SG]</ta>
            <ta e="T363" id="Seg_7341" s="T362">PTCL</ta>
            <ta e="T364" id="Seg_7342" s="T363">lip-PL-NOM/GEN/ACC.2SG</ta>
            <ta e="T365" id="Seg_7343" s="T364">NEG</ta>
            <ta e="T366" id="Seg_7344" s="T365">thick.[NOM.SG]</ta>
            <ta e="T368" id="Seg_7345" s="T367">PTCL</ta>
            <ta e="T369" id="Seg_7346" s="T368">hurt-MOM-PST.[3SG]</ta>
            <ta e="T370" id="Seg_7347" s="T369">I.NOM</ta>
            <ta e="T372" id="Seg_7348" s="T371">this-LAT</ta>
            <ta e="T374" id="Seg_7349" s="T373">go-FUT-1SG</ta>
            <ta e="T375" id="Seg_7350" s="T374">this.[NOM.SG]</ta>
            <ta e="T376" id="Seg_7351" s="T375">PTCL</ta>
            <ta e="T377" id="Seg_7352" s="T376">say-IPFVZ.[3SG]</ta>
            <ta e="T378" id="Seg_7353" s="T377">NEG.AUX-IMP.2SG</ta>
            <ta e="T379" id="Seg_7354" s="T378">speak-EP-CNG</ta>
            <ta e="T380" id="Seg_7355" s="T379">Russian-GEN</ta>
            <ta e="T381" id="Seg_7356" s="T380">language-3SG-INS</ta>
            <ta e="T382" id="Seg_7357" s="T381">and</ta>
            <ta e="T383" id="Seg_7358" s="T382">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T384" id="Seg_7359" s="T383">language-3SG-INS</ta>
            <ta e="T385" id="Seg_7360" s="T384">speak-EP-IMP.2SG</ta>
            <ta e="T386" id="Seg_7361" s="T385">I.NOM</ta>
            <ta e="T387" id="Seg_7362" s="T386">very</ta>
            <ta e="T388" id="Seg_7363" s="T387">hurt-PRS-1SG</ta>
            <ta e="T389" id="Seg_7364" s="T388">PTCL</ta>
            <ta e="T390" id="Seg_7365" s="T389">very</ta>
            <ta e="T391" id="Seg_7366" s="T390">hurt-PRS-1SG</ta>
            <ta e="T392" id="Seg_7367" s="T391">PTCL</ta>
            <ta e="T393" id="Seg_7368" s="T392">I.NOM</ta>
            <ta e="T394" id="Seg_7369" s="T393">PTCL</ta>
            <ta e="T395" id="Seg_7370" s="T394">die-RES-PST-1SG</ta>
            <ta e="T396" id="Seg_7371" s="T395">die-RES-PST-1SG</ta>
            <ta e="T397" id="Seg_7372" s="T396">and</ta>
            <ta e="T398" id="Seg_7373" s="T397">you.NOM</ta>
            <ta e="T399" id="Seg_7374" s="T398">remain-FUT-2SG</ta>
            <ta e="T400" id="Seg_7375" s="T399">this.[NOM.SG]</ta>
            <ta e="T401" id="Seg_7376" s="T400">die-RES-PST.[3SG]</ta>
            <ta e="T402" id="Seg_7377" s="T401">two.[NOM.SG]</ta>
            <ta e="T403" id="Seg_7378" s="T402">daughter-NOM/GEN.3SG</ta>
            <ta e="T405" id="Seg_7379" s="T404">remain-MOM-PST-3PL</ta>
            <ta e="T406" id="Seg_7380" s="T405">one.[NOM.SG]</ta>
            <ta e="T407" id="Seg_7381" s="T406">this-INS</ta>
            <ta e="T408" id="Seg_7382" s="T407">be-PST.[3SG]</ta>
            <ta e="T409" id="Seg_7383" s="T408">and</ta>
            <ta e="T410" id="Seg_7384" s="T409">one.[NOM.SG]</ta>
            <ta e="T411" id="Seg_7385" s="T410">far</ta>
            <ta e="T412" id="Seg_7386" s="T411">be-PST.[3SG]</ta>
            <ta e="T413" id="Seg_7387" s="T412">today</ta>
            <ta e="T415" id="Seg_7388" s="T413">PTCL</ta>
            <ta e="T420" id="Seg_7389" s="T419">today</ta>
            <ta e="T421" id="Seg_7390" s="T420">whole</ta>
            <ta e="T422" id="Seg_7391" s="T421">day-NOM/GEN.3SG</ta>
            <ta e="T423" id="Seg_7392" s="T422">rain.[NOM.SG]</ta>
            <ta e="T424" id="Seg_7393" s="T423">come-PST.[3SG]</ta>
            <ta e="T425" id="Seg_7394" s="T424">and</ta>
            <ta e="T426" id="Seg_7395" s="T425">now</ta>
            <ta e="T427" id="Seg_7396" s="T426">stay</ta>
            <ta e="T428" id="Seg_7397" s="T427">shine-DUR.[3SG]</ta>
            <ta e="T429" id="Seg_7398" s="T428">Novik.[NOM.SG]</ta>
            <ta e="T430" id="Seg_7399" s="T429">come-PRS.[3SG]</ta>
            <ta e="T431" id="Seg_7400" s="T430">pig-LAT/LOC.3SG</ta>
            <ta e="T432" id="Seg_7401" s="T431">and</ta>
            <ta e="T433" id="Seg_7402" s="T432">Sidor.[NOM.SG]</ta>
            <ta e="T434" id="Seg_7403" s="T433">dog-LAT</ta>
            <ta e="T435" id="Seg_7404" s="T434">sit-DUR.[3SG]</ta>
            <ta e="T436" id="Seg_7405" s="T435">and</ta>
            <ta e="T437" id="Seg_7406" s="T436">people.[NOM.SG]</ta>
            <ta e="T438" id="Seg_7407" s="T437">see-PST-3PL</ta>
            <ta e="T439" id="Seg_7408" s="T438">think-PST-3PL</ta>
            <ta e="T441" id="Seg_7409" s="T440">chairman.[NOM.SG]</ta>
            <ta e="T442" id="Seg_7410" s="T441">say-PST.[3SG]</ta>
            <ta e="T443" id="Seg_7411" s="T442">one.should</ta>
            <ta e="T444" id="Seg_7412" s="T443">mill-NOM/GEN/ACC.3SG</ta>
            <ta e="T445" id="Seg_7413" s="T444">%repair-INF.LAT</ta>
            <ta e="T446" id="Seg_7414" s="T445">good.[NOM.SG]</ta>
            <ta e="T447" id="Seg_7415" s="T446">people.[NOM.SG]</ta>
            <ta e="T448" id="Seg_7416" s="T447">collect-INF.LAT</ta>
            <ta e="T451" id="Seg_7417" s="T450">JUSS</ta>
            <ta e="T452" id="Seg_7418" s="T451">there</ta>
            <ta e="T453" id="Seg_7419" s="T452">work-FUT-3PL</ta>
            <ta e="T454" id="Seg_7420" s="T453">one.should</ta>
            <ta e="T455" id="Seg_7421" s="T454">mill-NOM/GEN/ACC.3SG</ta>
            <ta e="T456" id="Seg_7422" s="T455">go-INF.LAT</ta>
            <ta e="T457" id="Seg_7423" s="T456">flour.[NOM.SG]</ta>
            <ta e="T458" id="Seg_7424" s="T457">smoke-INF.LAT</ta>
            <ta e="T459" id="Seg_7425" s="T458">otherwise</ta>
            <ta e="T460" id="Seg_7426" s="T459">flour.[NOM.SG]</ta>
            <ta e="T461" id="Seg_7427" s="T460">NEG.EX.[3SG]</ta>
            <ta e="T463" id="Seg_7428" s="T462">I.NOM</ta>
            <ta e="T464" id="Seg_7429" s="T463">small.[NOM.SG]</ta>
            <ta e="T465" id="Seg_7430" s="T464">be-PST-1SG</ta>
            <ta e="T466" id="Seg_7431" s="T465">and</ta>
            <ta e="T467" id="Seg_7432" s="T466">girl-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T468" id="Seg_7433" s="T467">big.[NOM.SG]</ta>
            <ta e="T469" id="Seg_7434" s="T468">be-PST-3PL</ta>
            <ta e="T470" id="Seg_7435" s="T469">always</ta>
            <ta e="T471" id="Seg_7436" s="T470">I.ACC</ta>
            <ta e="T472" id="Seg_7437" s="T471">take-PST-3PL</ta>
            <ta e="T473" id="Seg_7438" s="T472">self-LAT/LOC.3SG</ta>
            <ta e="T474" id="Seg_7439" s="T473">song.[NOM.SG]</ta>
            <ta e="T475" id="Seg_7440" s="T474">sing-INF.LAT</ta>
            <ta e="T476" id="Seg_7441" s="T475">I.LAT</ta>
            <ta e="T480" id="Seg_7442" s="T479">beautiful.[NOM.SG]</ta>
            <ta e="T481" id="Seg_7443" s="T480">be-PST.[3SG]</ta>
            <ta e="T482" id="Seg_7444" s="T481">sing-PST-1SG</ta>
            <ta e="T483" id="Seg_7445" s="T482">PTCL</ta>
            <ta e="T484" id="Seg_7446" s="T483">this.[NOM.SG]</ta>
            <ta e="T485" id="Seg_7447" s="T484">man.[NOM.SG]</ta>
            <ta e="T486" id="Seg_7448" s="T485">very</ta>
            <ta e="T487" id="Seg_7449" s="T486">good.[NOM.SG]</ta>
            <ta e="T488" id="Seg_7450" s="T487">sing-PRS.[3SG]</ta>
            <ta e="T492" id="Seg_7451" s="T491">very</ta>
            <ta e="T493" id="Seg_7452" s="T492">beautiful.[NOM.SG]</ta>
            <ta e="T501" id="Seg_7453" s="T500">very</ta>
            <ta e="T502" id="Seg_7454" s="T501">strongly</ta>
            <ta e="T503" id="Seg_7455" s="T502">sing-DUR.[3SG]</ta>
            <ta e="T504" id="Seg_7456" s="T503">this.[NOM.SG]</ta>
            <ta e="T505" id="Seg_7457" s="T504">man.[NOM.SG]</ta>
            <ta e="T506" id="Seg_7458" s="T505">die-RES-PST.[3SG]</ta>
            <ta e="T507" id="Seg_7459" s="T506">one.should</ta>
            <ta e="T508" id="Seg_7460" s="T507">tent-NOM/GEN/ACC.3SG</ta>
            <ta e="T509" id="Seg_7461" s="T508">make-INF.LAT</ta>
            <ta e="T510" id="Seg_7462" s="T509">tree.[NOM.SG]</ta>
            <ta e="T511" id="Seg_7463" s="T510">tree-INS</ta>
            <ta e="T512" id="Seg_7464" s="T511">tree-ABL</ta>
            <ta e="T513" id="Seg_7465" s="T512">and</ta>
            <ta e="T514" id="Seg_7466" s="T513">there</ta>
            <ta e="T515" id="Seg_7467" s="T514">this-ACC</ta>
            <ta e="T516" id="Seg_7468" s="T515">put-INF.LAT</ta>
            <ta e="T517" id="Seg_7469" s="T516">and</ta>
            <ta e="T518" id="Seg_7470" s="T517">cross.[NOM.SG]</ta>
            <ta e="T519" id="Seg_7471" s="T518">make-INF.LAT</ta>
            <ta e="T520" id="Seg_7472" s="T519">there</ta>
            <ta e="T521" id="Seg_7473" s="T520">now</ta>
            <ta e="T522" id="Seg_7474" s="T521">go-FUT-3SG</ta>
            <ta e="T523" id="Seg_7475" s="T522">that.[NOM.SG]</ta>
            <ta e="T524" id="Seg_7476" s="T523">place-LAT</ta>
            <ta e="T525" id="Seg_7477" s="T524">very</ta>
            <ta e="T526" id="Seg_7478" s="T525">beautiful.[NOM.SG]</ta>
            <ta e="T527" id="Seg_7479" s="T526">house-PL</ta>
            <ta e="T528" id="Seg_7480" s="T527">stand-PRS-3PL</ta>
            <ta e="T529" id="Seg_7481" s="T528">this-PL</ta>
            <ta e="T530" id="Seg_7482" s="T529">this-PL</ta>
            <ta e="T533" id="Seg_7483" s="T532">stone-PL</ta>
            <ta e="T535" id="Seg_7484" s="T534">put-PST-3PL</ta>
            <ta e="T536" id="Seg_7485" s="T535">window</ta>
            <ta e="T537" id="Seg_7486" s="T536">window-EP-GEN</ta>
            <ta e="T538" id="Seg_7487" s="T537">edge-LAT/LOC.3SG</ta>
            <ta e="T539" id="Seg_7488" s="T538">PTCL</ta>
            <ta e="T540" id="Seg_7489" s="T539">tree-PL</ta>
            <ta e="T542" id="Seg_7490" s="T541">seat-PST-3PL</ta>
            <ta e="T543" id="Seg_7491" s="T542">tree-PL</ta>
            <ta e="T544" id="Seg_7492" s="T543">seat-PST-3PL</ta>
            <ta e="T546" id="Seg_7493" s="T545">seat-PST-3PL</ta>
            <ta e="T547" id="Seg_7494" s="T546">very</ta>
            <ta e="T548" id="Seg_7495" s="T547">beautiful.[NOM.SG]</ta>
            <ta e="T549" id="Seg_7496" s="T548">house.[NOM.SG]</ta>
            <ta e="T552" id="Seg_7497" s="T551">bird.cherry</ta>
            <ta e="T553" id="Seg_7498" s="T552">berry.[NOM.SG]</ta>
            <ta e="T554" id="Seg_7499" s="T553">PTCL</ta>
            <ta e="T555" id="Seg_7500" s="T554">seat-PST-3PL</ta>
            <ta e="T867" id="Seg_7501" s="T555">left</ta>
            <ta e="T557" id="Seg_7502" s="T556">hand-LAT</ta>
            <ta e="T558" id="Seg_7503" s="T557">beautiful.[NOM.SG]</ta>
            <ta e="T559" id="Seg_7504" s="T558">and</ta>
            <ta e="T560" id="Seg_7505" s="T559">right</ta>
            <ta e="T561" id="Seg_7506" s="T560">hand-LAT</ta>
            <ta e="T563" id="Seg_7507" s="T561">beautiful.[NOM.SG]</ta>
            <ta e="T567" id="Seg_7508" s="T566">very</ta>
            <ta e="T568" id="Seg_7509" s="T567">beautiful.[NOM.SG]</ta>
            <ta e="T570" id="Seg_7510" s="T569">house-PL</ta>
            <ta e="T571" id="Seg_7511" s="T570">stand-PRS-3PL</ta>
            <ta e="T572" id="Seg_7512" s="T571">I.NOM</ta>
            <ta e="T573" id="Seg_7513" s="T572">sister_in_law-NOM/GEN/ACC.1SG</ta>
            <ta e="T575" id="Seg_7514" s="T574">boil-PST.[3SG]</ta>
            <ta e="T576" id="Seg_7515" s="T575">this.[NOM.SG]</ta>
            <ta e="T577" id="Seg_7516" s="T576">PTCL</ta>
            <ta e="T578" id="Seg_7517" s="T577">NEG</ta>
            <ta e="T579" id="Seg_7518" s="T578">strong.[NOM.SG]</ta>
            <ta e="T580" id="Seg_7519" s="T579">man.[NOM.SG]</ta>
            <ta e="T581" id="Seg_7520" s="T580">PTCL</ta>
            <ta e="T582" id="Seg_7521" s="T581">scold-DES-DUR.[3SG]</ta>
            <ta e="T583" id="Seg_7522" s="T582">else</ta>
            <ta e="T584" id="Seg_7523" s="T583">NEG</ta>
            <ta e="T585" id="Seg_7524" s="T584">be-FUT-2SG</ta>
            <ta e="T586" id="Seg_7525" s="T585">boil-INF.LAT</ta>
            <ta e="T587" id="Seg_7526" s="T586">NEG</ta>
            <ta e="T588" id="Seg_7527" s="T587">good.[NOM.SG]</ta>
            <ta e="T589" id="Seg_7528" s="T588">NEG</ta>
            <ta e="T590" id="Seg_7529" s="T589">strong.[NOM.SG]</ta>
            <ta e="T591" id="Seg_7530" s="T590">then</ta>
            <ta e="T592" id="Seg_7531" s="T591">this.[NOM.SG]</ta>
            <ta e="T593" id="Seg_7532" s="T592">sister_in_law-NOM/GEN/ACC.1SG</ta>
            <ta e="T595" id="Seg_7533" s="T594">water-ACC</ta>
            <ta e="T596" id="Seg_7534" s="T595">boil-PST.[3SG]</ta>
            <ta e="T597" id="Seg_7535" s="T596">salt-INS</ta>
            <ta e="T598" id="Seg_7536" s="T597">then</ta>
            <ta e="T600" id="Seg_7537" s="T599">meat.[NOM.SG]</ta>
            <ta e="T601" id="Seg_7538" s="T600">cut-PST.[3SG]</ta>
            <ta e="T602" id="Seg_7539" s="T601">and</ta>
            <ta e="T603" id="Seg_7540" s="T602">there</ta>
            <ta e="T604" id="Seg_7541" s="T603">put-IPFVZ-PRS.[3SG]</ta>
            <ta e="T605" id="Seg_7542" s="T604">so.that</ta>
            <ta e="T606" id="Seg_7543" s="T605">salt-DUR.[3SG]</ta>
            <ta e="T607" id="Seg_7544" s="T606">master-NOM/GEN.3SG</ta>
            <ta e="T608" id="Seg_7545" s="T607">master-EP-GEN</ta>
            <ta e="T609" id="Seg_7546" s="T608">woman.[NOM.SG]</ta>
            <ta e="T610" id="Seg_7547" s="T609">go-INF.LAT</ta>
            <ta e="T613" id="Seg_7548" s="T612">very</ta>
            <ta e="T614" id="Seg_7549" s="T613">water.[NOM.SG]</ta>
            <ta e="T615" id="Seg_7550" s="T614">one.should</ta>
            <ta e="T616" id="Seg_7551" s="T615">return-MOM-INF.LAT</ta>
            <ta e="T617" id="Seg_7552" s="T616">very</ta>
            <ta e="T618" id="Seg_7553" s="T617">far</ta>
            <ta e="T619" id="Seg_7554" s="T618">be.visible-PRS.[3SG]</ta>
            <ta e="T620" id="Seg_7555" s="T868">PTCL</ta>
            <ta e="T621" id="Seg_7556" s="T620">tree-PL</ta>
            <ta e="T622" id="Seg_7557" s="T621">NEG.EX.[3SG]</ta>
            <ta e="T623" id="Seg_7558" s="T622">one.[NOM.SG]</ta>
            <ta e="T624" id="Seg_7559" s="T623">and</ta>
            <ta e="T625" id="Seg_7560" s="T624">tree.[NOM.SG]</ta>
            <ta e="T627" id="Seg_7561" s="T625">NEG.EX.[3SG]</ta>
            <ta e="T629" id="Seg_7562" s="T628">come-PRS-1SG</ta>
            <ta e="T631" id="Seg_7563" s="T630">sing-DUR-1SG</ta>
            <ta e="T632" id="Seg_7564" s="T631">forest-LAT</ta>
            <ta e="T633" id="Seg_7565" s="T632">walk-PRS-1SG</ta>
            <ta e="T634" id="Seg_7566" s="T633">forest.[NOM.SG]</ta>
            <ta e="T636" id="Seg_7567" s="T635">sing-DUR-1SG</ta>
            <ta e="T637" id="Seg_7568" s="T636">water-LAT</ta>
            <ta e="T638" id="Seg_7569" s="T637">walk-PRS-1SG</ta>
            <ta e="T639" id="Seg_7570" s="T638">water.[NOM.SG]</ta>
            <ta e="T640" id="Seg_7571" s="T639">sing-DUR-1SG</ta>
            <ta e="T641" id="Seg_7572" s="T640">tree-LOC</ta>
            <ta e="T642" id="Seg_7573" s="T641">walk-PRS-1SG</ta>
            <ta e="T643" id="Seg_7574" s="T642">tree.[NOM.SG]</ta>
            <ta e="T644" id="Seg_7575" s="T643">sing-DUR-1SG</ta>
            <ta e="T645" id="Seg_7576" s="T644">I.NOM</ta>
            <ta e="T646" id="Seg_7577" s="T645">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T647" id="Seg_7578" s="T646">Kirel-LAT</ta>
            <ta e="T648" id="Seg_7579" s="T647">go-PST.[3SG]</ta>
            <ta e="T650" id="Seg_7580" s="T649">take-PST.[3SG]</ta>
            <ta e="T651" id="Seg_7581" s="T650">fish.[NOM.SG]</ta>
            <ta e="T653" id="Seg_7582" s="T652">many</ta>
            <ta e="T654" id="Seg_7583" s="T653">capture-PST.[3SG]</ta>
            <ta e="T655" id="Seg_7584" s="T654">we.NOM</ta>
            <ta e="T656" id="Seg_7585" s="T655">clean-IPFVZ-PST-1PL</ta>
            <ta e="T657" id="Seg_7586" s="T656">and</ta>
            <ta e="T658" id="Seg_7587" s="T657">salt-PST-1PL</ta>
            <ta e="T659" id="Seg_7588" s="T658">and</ta>
            <ta e="T660" id="Seg_7589" s="T659">bake-PST-1PL</ta>
            <ta e="T661" id="Seg_7590" s="T660">and</ta>
            <ta e="T662" id="Seg_7591" s="T661">eat-PST-1PL</ta>
            <ta e="T663" id="Seg_7592" s="T662">we.NOM</ta>
            <ta e="T664" id="Seg_7593" s="T663">go-%1PL.PST</ta>
            <ta e="T665" id="Seg_7594" s="T664">and</ta>
            <ta e="T666" id="Seg_7595" s="T665">very</ta>
            <ta e="T667" id="Seg_7596" s="T666">big.[NOM.SG]</ta>
            <ta e="T668" id="Seg_7597" s="T667">water.[NOM.SG]</ta>
            <ta e="T669" id="Seg_7598" s="T668">there</ta>
            <ta e="T670" id="Seg_7599" s="T669">PTCL</ta>
            <ta e="T671" id="Seg_7600" s="T670">%net-NOM/GEN/ACC.3PL</ta>
            <ta e="T672" id="Seg_7601" s="T671">throw.away-PST-%1PL.PST</ta>
            <ta e="T673" id="Seg_7602" s="T672">fish.[NOM.SG]</ta>
            <ta e="T674" id="Seg_7603" s="T673">capture-PST-1PL</ta>
            <ta e="T675" id="Seg_7604" s="T674">man.[NOM.SG]</ta>
            <ta e="T676" id="Seg_7605" s="T675">PTCL</ta>
            <ta e="T677" id="Seg_7606" s="T676">woman-ACC</ta>
            <ta e="T678" id="Seg_7607" s="T677">press-PST.[3SG]</ta>
            <ta e="T679" id="Seg_7608" s="T678">and</ta>
            <ta e="T680" id="Seg_7609" s="T679">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T681" id="Seg_7610" s="T680">tree-LAT</ta>
            <ta e="T682" id="Seg_7611" s="T681">hang.up-MOM-PST.[3SG]</ta>
            <ta e="T684" id="Seg_7612" s="T683">bear.[NOM.SG]</ta>
            <ta e="T685" id="Seg_7613" s="T684">very</ta>
            <ta e="T686" id="Seg_7614" s="T685">hair-NOM/GEN/ACC.3SG</ta>
            <ta e="T687" id="Seg_7615" s="T686">many</ta>
            <ta e="T688" id="Seg_7616" s="T687">PTCL</ta>
            <ta e="T689" id="Seg_7617" s="T688">meat.[NOM.SG]</ta>
            <ta e="T690" id="Seg_7618" s="T689">NEG</ta>
            <ta e="T691" id="Seg_7619" s="T690">be.visible-PRS.[3SG]</ta>
            <ta e="T692" id="Seg_7620" s="T691">very</ta>
            <ta e="T693" id="Seg_7621" s="T692">this.[NOM.SG]</ta>
            <ta e="T694" id="Seg_7622" s="T693">soft.[NOM.SG]</ta>
            <ta e="T695" id="Seg_7623" s="T694">very</ta>
            <ta e="T696" id="Seg_7624" s="T695">thick.[NOM.SG]</ta>
            <ta e="T697" id="Seg_7625" s="T696">foot-NOM/GEN.3SG</ta>
            <ta e="T698" id="Seg_7626" s="T697">very</ta>
            <ta e="T699" id="Seg_7627" s="T698">strong.[NOM.SG]</ta>
            <ta e="T700" id="Seg_7628" s="T699">strong.[NOM.SG]</ta>
            <ta e="T701" id="Seg_7629" s="T700">claw-PL</ta>
            <ta e="T702" id="Seg_7630" s="T701">very</ta>
            <ta e="T703" id="Seg_7631" s="T702">big.[NOM.SG]</ta>
            <ta e="T704" id="Seg_7632" s="T703">big.[NOM.SG]</ta>
            <ta e="T705" id="Seg_7633" s="T704">dog.[NOM.SG]</ta>
            <ta e="T706" id="Seg_7634" s="T705">dog-INS</ta>
            <ta e="T707" id="Seg_7635" s="T706">big.[NOM.SG]</ta>
            <ta e="T708" id="Seg_7636" s="T707">dog-INS</ta>
            <ta e="T709" id="Seg_7637" s="T708">eye-NOM/GEN.3SG</ta>
            <ta e="T710" id="Seg_7638" s="T709">small.[NOM.SG]</ta>
            <ta e="T711" id="Seg_7639" s="T710">ear-PL</ta>
            <ta e="T712" id="Seg_7640" s="T711">small.[NOM.SG]</ta>
            <ta e="T713" id="Seg_7641" s="T712">I.NOM</ta>
            <ta e="T714" id="Seg_7642" s="T713">relative-NOM/GEN/ACC.1SG</ta>
            <ta e="T715" id="Seg_7643" s="T714">come-PST.[3SG]</ta>
            <ta e="T716" id="Seg_7644" s="T715">boy.[NOM.SG]</ta>
            <ta e="T717" id="Seg_7645" s="T716">come-PST.[3SG]</ta>
            <ta e="T718" id="Seg_7646" s="T717">woman-INS</ta>
            <ta e="T719" id="Seg_7647" s="T718">brother-NOM/GEN/ACC.1SG</ta>
            <ta e="T720" id="Seg_7648" s="T719">come-PST.[3SG]</ta>
            <ta e="T721" id="Seg_7649" s="T720">woman-INS</ta>
            <ta e="T723" id="Seg_7650" s="T721">child-PL</ta>
            <ta e="T724" id="Seg_7651" s="T723">I.NOM</ta>
            <ta e="T725" id="Seg_7652" s="T724">relative-NOM/GEN/ACC.1SG</ta>
            <ta e="T726" id="Seg_7653" s="T725">come-PST.[3SG]</ta>
            <ta e="T727" id="Seg_7654" s="T726">boy-NOM/GEN/ACC.1SG</ta>
            <ta e="T728" id="Seg_7655" s="T727">come-PST.[3SG]</ta>
            <ta e="T729" id="Seg_7656" s="T728">woman-INS</ta>
            <ta e="T730" id="Seg_7657" s="T729">brother-NOM/GEN/ACC.1SG</ta>
            <ta e="T731" id="Seg_7658" s="T730">come-PST.[3SG]</ta>
            <ta e="T732" id="Seg_7659" s="T731">woman-INS</ta>
            <ta e="T733" id="Seg_7660" s="T732">daughter-NOM/GEN/ACC.1SG</ta>
            <ta e="T734" id="Seg_7661" s="T733">man-INS</ta>
            <ta e="T735" id="Seg_7662" s="T734">come-PST.[3SG]</ta>
            <ta e="T736" id="Seg_7663" s="T735">child-PL</ta>
            <ta e="T737" id="Seg_7664" s="T736">child-PL-LAT</ta>
            <ta e="T738" id="Seg_7665" s="T737">this-INS</ta>
            <ta e="T739" id="Seg_7666" s="T738">come-PST-3PL</ta>
            <ta e="T740" id="Seg_7667" s="T739">one.should</ta>
            <ta e="T741" id="Seg_7668" s="T740">this-PL-LAT</ta>
            <ta e="T743" id="Seg_7669" s="T742">collect-INF.LAT</ta>
            <ta e="T744" id="Seg_7670" s="T743">this-PL</ta>
            <ta e="T746" id="Seg_7671" s="T745">eat-INF.LAT</ta>
            <ta e="T747" id="Seg_7672" s="T746">give-INF.LAT</ta>
            <ta e="T748" id="Seg_7673" s="T747">I.NOM</ta>
            <ta e="T749" id="Seg_7674" s="T748">boy-NOM/GEN/ACC.1SG</ta>
            <ta e="T750" id="Seg_7675" s="T749">PTCL</ta>
            <ta e="T751" id="Seg_7676" s="T750">strongly</ta>
            <ta e="T752" id="Seg_7677" s="T751">hurt-PRS.[3SG]</ta>
            <ta e="T753" id="Seg_7678" s="T752">I.NOM</ta>
            <ta e="T754" id="Seg_7679" s="T753">this-LAT</ta>
            <ta e="T755" id="Seg_7680" s="T754">say-PST-1SG</ta>
            <ta e="T756" id="Seg_7681" s="T755">God-LAT</ta>
            <ta e="T757" id="Seg_7682" s="T756">pray-IMP.2SG</ta>
            <ta e="T758" id="Seg_7683" s="T757">ask-IMP.2SG</ta>
            <ta e="T759" id="Seg_7684" s="T758">from.there</ta>
            <ta e="T760" id="Seg_7685" s="T759">strength-NOM/GEN/ACC.3SG</ta>
            <ta e="T761" id="Seg_7686" s="T760">call-IMP.2SG</ta>
            <ta e="T762" id="Seg_7687" s="T761">self-%LAT/LOC.1/2SG</ta>
            <ta e="T763" id="Seg_7688" s="T762">this.[NOM.SG]</ta>
            <ta e="T764" id="Seg_7689" s="T763">PTCL</ta>
            <ta e="T765" id="Seg_7690" s="T764">call-PST.[3SG]</ta>
            <ta e="T766" id="Seg_7691" s="T765">come-IMP.2SG</ta>
            <ta e="T767" id="Seg_7692" s="T766">I.LAT</ta>
            <ta e="T768" id="Seg_7693" s="T767">strength-NOM/GEN/ACC.2SG</ta>
            <ta e="T769" id="Seg_7694" s="T768">bring-IMP.2SG</ta>
            <ta e="T770" id="Seg_7695" s="T769">heal-IMP.2SG.O</ta>
            <ta e="T771" id="Seg_7696" s="T770">I.ACC</ta>
            <ta e="T772" id="Seg_7697" s="T771">I.NOM</ta>
            <ta e="T773" id="Seg_7698" s="T772">yesterday</ta>
            <ta e="T774" id="Seg_7699" s="T773">house.[NOM.SG]</ta>
            <ta e="T775" id="Seg_7700" s="T774">wash-PST-1SG</ta>
            <ta e="T776" id="Seg_7701" s="T775">hen.[NOM.SG]</ta>
            <ta e="T777" id="Seg_7702" s="T776">shit-PST.[3SG]</ta>
            <ta e="T778" id="Seg_7703" s="T777">also</ta>
            <ta e="T779" id="Seg_7704" s="T778">wash-PST-1SG</ta>
            <ta e="T780" id="Seg_7705" s="T779">then</ta>
            <ta e="T782" id="Seg_7706" s="T781">wash-PST-1SG</ta>
            <ta e="T783" id="Seg_7707" s="T782">then</ta>
            <ta e="T784" id="Seg_7708" s="T783">go-PST-1SG</ta>
            <ta e="T785" id="Seg_7709" s="T784">water.[NOM.SG]</ta>
            <ta e="T786" id="Seg_7710" s="T785">carry-INF.LAT</ta>
            <ta e="T787" id="Seg_7711" s="T786">sauna-LAT</ta>
            <ta e="T789" id="Seg_7712" s="T787">one.[NOM.SG]</ta>
            <ta e="T790" id="Seg_7713" s="T789">six.[NOM.SG]</ta>
            <ta e="T791" id="Seg_7714" s="T790">cauldron.[NOM.SG]</ta>
            <ta e="T792" id="Seg_7715" s="T791">bring-PST-1SG</ta>
            <ta e="T793" id="Seg_7716" s="T792">I.NOM</ta>
            <ta e="T794" id="Seg_7717" s="T793">today</ta>
            <ta e="T795" id="Seg_7718" s="T794">early-LOC.ADV</ta>
            <ta e="T796" id="Seg_7719" s="T795">get.up-PST-1SG</ta>
            <ta e="T799" id="Seg_7720" s="T798">day.[NOM.SG]</ta>
            <ta e="T800" id="Seg_7721" s="T799">work-PST-1SG</ta>
            <ta e="T801" id="Seg_7722" s="T800">PTCL</ta>
            <ta e="T802" id="Seg_7723" s="T801">get.tired-MOM-PST-1SG</ta>
            <ta e="T804" id="Seg_7724" s="T803">evening-LOC.ADV</ta>
            <ta e="T805" id="Seg_7725" s="T804">evening-LOC.ADV</ta>
            <ta e="T806" id="Seg_7726" s="T805">come-PST.[3SG]</ta>
            <ta e="T807" id="Seg_7727" s="T806">I.NOM</ta>
            <ta e="T808" id="Seg_7728" s="T807">sleep-MOM-PST-1SG</ta>
            <ta e="T809" id="Seg_7729" s="T808">I.NOM</ta>
            <ta e="T810" id="Seg_7730" s="T809">this-ACC.PL</ta>
            <ta e="T811" id="Seg_7731" s="T810">see-INF.LAT</ta>
            <ta e="T813" id="Seg_7732" s="T812">not</ta>
            <ta e="T814" id="Seg_7733" s="T813">one.wants</ta>
            <ta e="T815" id="Seg_7734" s="T814">this-PL</ta>
            <ta e="T816" id="Seg_7735" s="T815">PTCL</ta>
            <ta e="T817" id="Seg_7736" s="T816">always</ta>
            <ta e="T819" id="Seg_7737" s="T818">lie-DUR-3PL</ta>
            <ta e="T820" id="Seg_7738" s="T819">lie-DUR-3PL</ta>
            <ta e="T821" id="Seg_7739" s="T820">this-PL</ta>
            <ta e="T822" id="Seg_7740" s="T821">right</ta>
            <ta e="T824" id="Seg_7741" s="T823">listen-INF.LAT</ta>
            <ta e="T825" id="Seg_7742" s="T824">not</ta>
            <ta e="T826" id="Seg_7743" s="T825">one.wants</ta>
            <ta e="T827" id="Seg_7744" s="T826">JUSS</ta>
            <ta e="T828" id="Seg_7745" s="T827">I.LAT</ta>
            <ta e="T829" id="Seg_7746" s="T828">eye-LAT/LOC.3SG</ta>
            <ta e="T830" id="Seg_7747" s="T829">NEG</ta>
            <ta e="T831" id="Seg_7748" s="T830">come-FUT-3PL</ta>
            <ta e="T832" id="Seg_7749" s="T831">I.LAT</ta>
            <ta e="T833" id="Seg_7750" s="T832">this-ACC.PL</ta>
            <ta e="T834" id="Seg_7751" s="T833">NEG</ta>
            <ta e="T835" id="Seg_7752" s="T834">one.needs</ta>
            <ta e="T836" id="Seg_7753" s="T835">head-NOM/GEN/ACC.1SG</ta>
            <ta e="T837" id="Seg_7754" s="T836">hair-NOM/GEN/ACC.1SG</ta>
            <ta e="T838" id="Seg_7755" s="T837">ear-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T839" id="Seg_7756" s="T838">tooth-NOM/GEN/ACC.1SG</ta>
            <ta e="T840" id="Seg_7757" s="T839">lip-PL-NOM/GEN/ACC.2SG</ta>
            <ta e="T841" id="Seg_7758" s="T840">language-NOM/GEN/ACC.1SG</ta>
            <ta e="T842" id="Seg_7759" s="T841">eye-NOM/GEN/ACC.1SG</ta>
            <ta e="T843" id="Seg_7760" s="T842">nose-NOM/GEN/ACC.1SG</ta>
            <ta e="T844" id="Seg_7761" s="T843">hand-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T845" id="Seg_7762" s="T844">back-NOM/GEN/ACC.1SG</ta>
            <ta e="T847" id="Seg_7763" s="T846">heart-NOM/GEN/ACC.1SG</ta>
            <ta e="T848" id="Seg_7764" s="T847">inside-NOM/GEN/ACC.1SG</ta>
            <ta e="T849" id="Seg_7765" s="T848">belly-NOM/GEN/ACC.1SG</ta>
            <ta e="T851" id="Seg_7766" s="T850">back-NOM/GEN/ACC.1SG</ta>
            <ta e="T852" id="Seg_7767" s="T851">ass-NOM/GEN/ACC.3SG</ta>
            <ta e="T853" id="Seg_7768" s="T852">foot-NOM/GEN/ACC.1SG</ta>
            <ta e="T854" id="Seg_7769" s="T853">mouth-NOM/GEN/ACC.3SG</ta>
            <ta e="T855" id="Seg_7770" s="T854">heart-NOM/GEN/ACC.1SG</ta>
            <ta e="T856" id="Seg_7771" s="T855">heart-NOM/GEN/ACC.3SG</ta>
            <ta e="T858" id="Seg_7772" s="T857">breast-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T861" id="Seg_7773" s="T860">bone-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T863" id="Seg_7774" s="T862">blood.[NOM.SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr-PKZ">
            <ta e="T5" id="Seg_7775" s="T4">здесь</ta>
            <ta e="T6" id="Seg_7776" s="T5">прийти-FUT-2SG</ta>
            <ta e="T7" id="Seg_7777" s="T6">так</ta>
            <ta e="T8" id="Seg_7778" s="T7">я.ACC</ta>
            <ta e="T9" id="Seg_7779" s="T8">глаз.[NOM.SG]</ta>
            <ta e="T10" id="Seg_7780" s="T9">взять-IMP.2SG</ta>
            <ta e="T11" id="Seg_7781" s="T10">ты.NOM</ta>
            <ta e="T12" id="Seg_7782" s="T11">мужчина.[NOM.SG]</ta>
            <ta e="T13" id="Seg_7783" s="T12">куда</ta>
            <ta e="T14" id="Seg_7784" s="T13">пойти-PST.[3SG]</ta>
            <ta e="T15" id="Seg_7785" s="T14">Саяны-LAT</ta>
            <ta e="T869" id="Seg_7786" s="T16">пойти-CVB</ta>
            <ta e="T17" id="Seg_7787" s="T869">исчезнуть-PST.[3SG]</ta>
            <ta e="T18" id="Seg_7788" s="T17">лошадь.[NOM.SG]</ta>
            <ta e="T19" id="Seg_7789" s="T18">один</ta>
            <ta e="T20" id="Seg_7790" s="T19">пойти-PST.[3SG]</ta>
            <ta e="T21" id="Seg_7791" s="T20">нет</ta>
            <ta e="T22" id="Seg_7792" s="T21">два-COLL</ta>
            <ta e="T870" id="Seg_7793" s="T22">пойти-CVB</ta>
            <ta e="T23" id="Seg_7794" s="T870">исчезнуть-PST-3PL</ta>
            <ta e="T24" id="Seg_7795" s="T23">PTCL</ta>
            <ta e="T25" id="Seg_7796" s="T24">когда</ta>
            <ta e="T27" id="Seg_7797" s="T26">пойти-PST-3PL</ta>
            <ta e="T29" id="Seg_7798" s="T28">вчера</ta>
            <ta e="T30" id="Seg_7799" s="T29">пойти-PST-3PL</ta>
            <ta e="T31" id="Seg_7800" s="T30">очень</ta>
            <ta e="T32" id="Seg_7801" s="T31">рано-LOC.ADV</ta>
            <ta e="T33" id="Seg_7802" s="T32">что.[NOM.SG]</ta>
            <ta e="T34" id="Seg_7803" s="T33">лгать-DUR-2SG</ta>
            <ta e="T35" id="Seg_7804" s="T34">NEG</ta>
            <ta e="T38" id="Seg_7805" s="T37">NEG</ta>
            <ta e="T40" id="Seg_7806" s="T39">%%-PRS-2SG</ta>
            <ta e="T41" id="Seg_7807" s="T40">%%-LAT</ta>
            <ta e="T43" id="Seg_7808" s="T42">поставить-IMP.2SG.O</ta>
            <ta e="T44" id="Seg_7809" s="T43">PTCL</ta>
            <ta e="T45" id="Seg_7810" s="T44">рубашка-NOM/GEN/ACC.1SG</ta>
            <ta e="T46" id="Seg_7811" s="T45">рвать-MOM-PST.[3SG]</ta>
            <ta e="T47" id="Seg_7812" s="T46">отверстие.[NOM.SG]</ta>
            <ta e="T48" id="Seg_7813" s="T47">стать-RES-PST.[3SG]</ta>
            <ta e="T49" id="Seg_7814" s="T48">я.NOM</ta>
            <ta e="T50" id="Seg_7815" s="T49">тогда</ta>
            <ta e="T51" id="Seg_7816" s="T50">этот-ACC</ta>
            <ta e="T53" id="Seg_7817" s="T52">шить-PST-1SG</ta>
            <ta e="T54" id="Seg_7818" s="T53">нитка-3PL-INS</ta>
            <ta e="T55" id="Seg_7819" s="T54">игла-INS</ta>
            <ta e="T56" id="Seg_7820" s="T55">наперсток.[NOM.SG]</ta>
            <ta e="T57" id="Seg_7821" s="T56">колокольня-LOC</ta>
            <ta e="T58" id="Seg_7822" s="T57">PTCL</ta>
            <ta e="T59" id="Seg_7823" s="T58">греметь-DUR.[3SG]</ta>
            <ta e="T60" id="Seg_7824" s="T59">колокол.[NOM.SG]</ta>
            <ta e="T61" id="Seg_7825" s="T60">надо</ta>
            <ta e="T62" id="Seg_7826" s="T61">пойти-INF.LAT</ta>
            <ta e="T63" id="Seg_7827" s="T62">Бог-LAT</ta>
            <ta e="T65" id="Seg_7828" s="T64">упасть-INF.LAT</ta>
            <ta e="T66" id="Seg_7829" s="T65">тогда</ta>
            <ta e="T67" id="Seg_7830" s="T66">позвать-PRS-3PL</ta>
            <ta e="T68" id="Seg_7831" s="T67">этот-PL</ta>
            <ta e="T69" id="Seg_7832" s="T68">PTCL</ta>
            <ta e="T70" id="Seg_7833" s="T69">вешать-IMP.2SG.O</ta>
            <ta e="T71" id="Seg_7834" s="T70">лошадь-LAT</ta>
            <ta e="T72" id="Seg_7835" s="T71">колокол.[NOM.SG]</ta>
            <ta e="T73" id="Seg_7836" s="T72">ум-NOM/GEN/ACC.3SG</ta>
            <ta e="T74" id="Seg_7837" s="T73">NEG.EX.[3SG]</ta>
            <ta e="T75" id="Seg_7838" s="T74">PTCL</ta>
            <ta e="T76" id="Seg_7839" s="T75">что.[NOM.SG]=INDEF</ta>
            <ta e="T77" id="Seg_7840" s="T76">NEG</ta>
            <ta e="T78" id="Seg_7841" s="T77">знать-1SG</ta>
            <ta e="T79" id="Seg_7842" s="T78">что.[NOM.SG]=INDEF</ta>
            <ta e="T80" id="Seg_7843" s="T79">NEG</ta>
            <ta e="T81" id="Seg_7844" s="T80">мочь-PRS-1SG</ta>
            <ta e="T82" id="Seg_7845" s="T81">сказать-INF.LAT</ta>
            <ta e="T83" id="Seg_7846" s="T82">PTCL</ta>
            <ta e="T84" id="Seg_7847" s="T83">ум-NOM/GEN/ACC.3SG</ta>
            <ta e="T85" id="Seg_7848" s="T84">NEG.EX.[3SG]</ta>
            <ta e="T86" id="Seg_7849" s="T85">лес-LAT</ta>
            <ta e="T87" id="Seg_7850" s="T86">пойти-PST-1SG</ta>
            <ta e="T88" id="Seg_7851" s="T87">PTCL</ta>
            <ta e="T92" id="Seg_7852" s="T91">исчезнуть-DUR.[3SG]</ta>
            <ta e="T93" id="Seg_7853" s="T92">лес-LAT</ta>
            <ta e="T94" id="Seg_7854" s="T93">пойти-PST-1SG</ta>
            <ta e="T95" id="Seg_7855" s="T94">PTCL</ta>
            <ta e="T96" id="Seg_7856" s="T95">исчезнуть-DUR-PST-1SG</ta>
            <ta e="T97" id="Seg_7857" s="T96">тогда</ta>
            <ta e="T98" id="Seg_7858" s="T97">кричать-PST-1SG</ta>
            <ta e="T99" id="Seg_7859" s="T98">PTCL</ta>
            <ta e="T100" id="Seg_7860" s="T99">тогда</ta>
            <ta e="T101" id="Seg_7861" s="T100">прийти-PST-1SG</ta>
            <ta e="T102" id="Seg_7862" s="T101">долго</ta>
            <ta e="T103" id="Seg_7863" s="T102">сидеть-PST.[3SG]</ta>
            <ta e="T104" id="Seg_7864" s="T103">что.[NOM.SG]=INDEF</ta>
            <ta e="T106" id="Seg_7865" s="T104">PTCL</ta>
            <ta e="T107" id="Seg_7866" s="T106">что.[NOM.SG]=INDEF</ta>
            <ta e="T108" id="Seg_7867" s="T107">NEG</ta>
            <ta e="T109" id="Seg_7868" s="T108">говорить-PST.[3SG]</ta>
            <ta e="T110" id="Seg_7869" s="T109">этот.[NOM.SG]</ta>
            <ta e="T111" id="Seg_7870" s="T110">идти-PRS.[3SG]</ta>
            <ta e="T112" id="Seg_7871" s="T111">а</ta>
            <ta e="T113" id="Seg_7872" s="T112">я.NOM</ta>
            <ta e="T114" id="Seg_7873" s="T113">этот-ACC</ta>
            <ta e="T116" id="Seg_7874" s="T115">догонять-PRS-1SG</ta>
            <ta e="T117" id="Seg_7875" s="T116">NEG.AUX-IMP.2SG</ta>
            <ta e="T118" id="Seg_7876" s="T117">пойти-EP-CNG</ta>
            <ta e="T119" id="Seg_7877" s="T118">вернуться-EP-IMP.2SG</ta>
            <ta e="T120" id="Seg_7878" s="T119">здесь</ta>
            <ta e="T121" id="Seg_7879" s="T120">я.NOM</ta>
            <ta e="T122" id="Seg_7880" s="T121">ты.DAT</ta>
            <ta e="T123" id="Seg_7881" s="T122">сказать-PRS-1SG</ta>
            <ta e="T124" id="Seg_7882" s="T123">вернуться-EP-IMP.2SG</ta>
            <ta e="T125" id="Seg_7883" s="T124">здесь</ta>
            <ta e="T126" id="Seg_7884" s="T125">я.NOM</ta>
            <ta e="T127" id="Seg_7885" s="T126">очень</ta>
            <ta e="T128" id="Seg_7886" s="T127">большой.[NOM.SG]</ta>
            <ta e="T129" id="Seg_7887" s="T128">люди.[NOM.SG]</ta>
            <ta e="T130" id="Seg_7888" s="T129">быть-PRS.[3SG]</ta>
            <ta e="T131" id="Seg_7889" s="T130">хлеб.[NOM.SG]</ta>
            <ta e="T132" id="Seg_7890" s="T131">много</ta>
            <ta e="T134" id="Seg_7891" s="T133">нужно</ta>
            <ta e="T135" id="Seg_7892" s="T134">взять-IMP.2SG</ta>
            <ta e="T136" id="Seg_7893" s="T135">спина-NOM/GEN/ACC.3SG</ta>
            <ta e="T137" id="Seg_7894" s="T136">этот-ACC</ta>
            <ta e="T138" id="Seg_7895" s="T137">рука-NOM/GEN/ACC.2SG</ta>
            <ta e="T139" id="Seg_7896" s="T138">NEG</ta>
            <ta e="T140" id="Seg_7897" s="T139">класть-IMP.2SG.O</ta>
            <ta e="T142" id="Seg_7898" s="T140">%трогать-IMP.2SG.O</ta>
            <ta e="T143" id="Seg_7899" s="T142">этот-ACC</ta>
            <ta e="T144" id="Seg_7900" s="T143">%трогать-IMP.2SG.O</ta>
            <ta e="T145" id="Seg_7901" s="T144">этот-ACC</ta>
            <ta e="T146" id="Seg_7902" s="T145">рука-INS</ta>
            <ta e="T148" id="Seg_7903" s="T146">сам-%LAT/LOC.1/2SG</ta>
            <ta e="T149" id="Seg_7904" s="T148">много</ta>
            <ta e="T150" id="Seg_7905" s="T149">взять-PST-1SG</ta>
            <ta e="T151" id="Seg_7906" s="T150">а</ta>
            <ta e="T152" id="Seg_7907" s="T151">этот-LAT</ta>
            <ta e="T153" id="Seg_7908" s="T152">мало-ACC</ta>
            <ta e="T154" id="Seg_7909" s="T153">взять-PST-1SG</ta>
            <ta e="T155" id="Seg_7910" s="T154">сам-LAT/LOC.3SG</ta>
            <ta e="T156" id="Seg_7911" s="T155">много</ta>
            <ta e="T157" id="Seg_7912" s="T156">взять-PRS.[3SG]</ta>
            <ta e="T158" id="Seg_7913" s="T157">а</ta>
            <ta e="T159" id="Seg_7914" s="T158">этот-LAT</ta>
            <ta e="T160" id="Seg_7915" s="T159">мало-ACC</ta>
            <ta e="T161" id="Seg_7916" s="T160">взять-PRS.[3SG]</ta>
            <ta e="T162" id="Seg_7917" s="T161">шесть.[NOM.SG]</ta>
            <ta e="T163" id="Seg_7918" s="T162">PTCL</ta>
            <ta e="T164" id="Seg_7919" s="T163">говорить-PST-1PL</ta>
            <ta e="T165" id="Seg_7920" s="T164">мы.NOM</ta>
            <ta e="T166" id="Seg_7921" s="T165">PTCL</ta>
            <ta e="T167" id="Seg_7922" s="T166">маленький.[NOM.SG]</ta>
            <ta e="T168" id="Seg_7923" s="T167">быть-PST-1PL</ta>
            <ta e="T169" id="Seg_7924" s="T168">PTCL</ta>
            <ta e="T170" id="Seg_7925" s="T169">играть-DUR-PST-1PL</ta>
            <ta e="T171" id="Seg_7926" s="T170">дерево-PL</ta>
            <ta e="T172" id="Seg_7927" s="T171">взять-FUT-3SG</ta>
            <ta e="T173" id="Seg_7928" s="T172">и</ta>
            <ta e="T174" id="Seg_7929" s="T173">PTCL</ta>
            <ta e="T175" id="Seg_7930" s="T174">о</ta>
            <ta e="T176" id="Seg_7931" s="T175">один.[NOM.SG]</ta>
            <ta e="T177" id="Seg_7932" s="T176">бежать-FUT-3SG</ta>
            <ta e="T178" id="Seg_7933" s="T177">а</ta>
            <ta e="T179" id="Seg_7934" s="T178">мы.NOM</ta>
            <ta e="T180" id="Seg_7935" s="T179">PTCL</ta>
            <ta e="T181" id="Seg_7936" s="T180">спрятаться-RES-PST-1PL</ta>
            <ta e="T182" id="Seg_7937" s="T181">тогда</ta>
            <ta e="T183" id="Seg_7938" s="T182">этот.[NOM.SG]</ta>
            <ta e="T184" id="Seg_7939" s="T183">PTCL</ta>
            <ta e="T185" id="Seg_7940" s="T184">найти-PRS.[3SG]</ta>
            <ta e="T186" id="Seg_7941" s="T185">найти-PRS.[3SG]</ta>
            <ta e="T187" id="Seg_7942" s="T186">NEG</ta>
            <ta e="T189" id="Seg_7943" s="T188">NEG</ta>
            <ta e="T190" id="Seg_7944" s="T189">мочь-PRS.[3SG]</ta>
            <ta e="T191" id="Seg_7945" s="T190">найти-INF.LAT</ta>
            <ta e="T192" id="Seg_7946" s="T191">мы.NOM</ta>
            <ta e="T193" id="Seg_7947" s="T192">PTCL</ta>
            <ta e="T194" id="Seg_7948" s="T193">опять</ta>
            <ta e="T195" id="Seg_7949" s="T194">завязать-DUR-1PL</ta>
            <ta e="T197" id="Seg_7950" s="T196">мы.NOM</ta>
            <ta e="T198" id="Seg_7951" s="T197">PTCL</ta>
            <ta e="T199" id="Seg_7952" s="T198">NEG</ta>
            <ta e="T200" id="Seg_7953" s="T199">ругать-MOM-PST-1PL</ta>
            <ta e="T201" id="Seg_7954" s="T200">хороший.[NOM.SG]</ta>
            <ta e="T203" id="Seg_7955" s="T201">PTCL</ta>
            <ta e="T205" id="Seg_7956" s="T204">хороший.[NOM.SG]</ta>
            <ta e="T207" id="Seg_7957" s="T205">PTCL</ta>
            <ta e="T210" id="Seg_7958" s="T209">мы.NOM</ta>
            <ta e="T211" id="Seg_7959" s="T210">PTCL</ta>
            <ta e="T212" id="Seg_7960" s="T211">хороший.[NOM.SG]</ta>
            <ta e="T213" id="Seg_7961" s="T212">играть-PST-1PL</ta>
            <ta e="T214" id="Seg_7962" s="T213">мы.NOM</ta>
            <ta e="T215" id="Seg_7963" s="T214">NEG</ta>
            <ta e="T216" id="Seg_7964" s="T215">рассердиться-FUT-PST-1PL</ta>
            <ta e="T217" id="Seg_7965" s="T216">мужчина-PL</ta>
            <ta e="T218" id="Seg_7966" s="T217">и</ta>
            <ta e="T219" id="Seg_7967" s="T218">женщина-PL</ta>
            <ta e="T220" id="Seg_7968" s="T219">сидеть-PST-3PL</ta>
            <ta e="T221" id="Seg_7969" s="T220">и</ta>
            <ta e="T222" id="Seg_7970" s="T221">смотреть-DUR-PST-3PL</ta>
            <ta e="T223" id="Seg_7971" s="T222">как</ta>
            <ta e="T224" id="Seg_7972" s="T223">мы.NOM</ta>
            <ta e="T225" id="Seg_7973" s="T224">играть-DUR-1PL</ta>
            <ta e="T226" id="Seg_7974" s="T225">кто.[NOM.SG]</ta>
            <ta e="T229" id="Seg_7975" s="T228">курить-IPFVZ-PST.[3SG]</ta>
            <ta e="T230" id="Seg_7976" s="T229">табак.[NOM.SG]</ta>
            <ta e="T231" id="Seg_7977" s="T230">кто.[NOM.SG]</ta>
            <ta e="T232" id="Seg_7978" s="T231">NEG</ta>
            <ta e="T233" id="Seg_7979" s="T232">курить-IPFVZ-PST.[3SG]</ta>
            <ta e="T234" id="Seg_7980" s="T233">кто.[NOM.SG]</ta>
            <ta e="T235" id="Seg_7981" s="T234">PTCL</ta>
            <ta e="T237" id="Seg_7982" s="T236">трубка-INS</ta>
            <ta e="T238" id="Seg_7983" s="T237">PTCL</ta>
            <ta e="T239" id="Seg_7984" s="T238">курить-DUR.[3SG]</ta>
            <ta e="T240" id="Seg_7985" s="T239">кто.[NOM.SG]</ta>
            <ta e="T241" id="Seg_7986" s="T240">NEG</ta>
            <ta e="T242" id="Seg_7987" s="T241">курить-DUR.[3SG]</ta>
            <ta e="T243" id="Seg_7988" s="T242">овца.[NOM.SG]</ta>
            <ta e="T244" id="Seg_7989" s="T243">очень</ta>
            <ta e="T245" id="Seg_7990" s="T244">много</ta>
            <ta e="T247" id="Seg_7991" s="T246">два.[NOM.SG]</ta>
            <ta e="T248" id="Seg_7992" s="T247">десять.[NOM.SG]</ta>
            <ta e="T249" id="Seg_7993" s="T248">два.[NOM.SG]</ta>
            <ta e="T250" id="Seg_7994" s="T249">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T251" id="Seg_7995" s="T250">лес-LAT</ta>
            <ta e="T252" id="Seg_7996" s="T251">пойти-PST.[3SG]</ta>
            <ta e="T253" id="Seg_7997" s="T252">много</ta>
            <ta e="T254" id="Seg_7998" s="T253">курица-PL</ta>
            <ta e="T255" id="Seg_7999" s="T254">убить-PST.[3SG]</ta>
            <ta e="T256" id="Seg_8000" s="T255">и</ta>
            <ta e="T257" id="Seg_8001" s="T256">чум-LAT/LOC.3SG</ta>
            <ta e="T258" id="Seg_8002" s="T257">принести-PST.[3SG]</ta>
            <ta e="T259" id="Seg_8003" s="T258">я.NOM</ta>
            <ta e="T260" id="Seg_8004" s="T259">прийти-PRS-1SG</ta>
            <ta e="T261" id="Seg_8005" s="T260">Пермяково.[NOM.SG]</ta>
            <ta e="T262" id="Seg_8006" s="T261">видеть-PRS-1SG</ta>
            <ta e="T263" id="Seg_8007" s="T262">PTCL</ta>
            <ta e="T264" id="Seg_8008" s="T263">лось.[NOM.SG]</ta>
            <ta e="T265" id="Seg_8009" s="T264">стоять-PRS.[3SG]</ta>
            <ta e="T266" id="Seg_8010" s="T265">я.NOM</ta>
            <ta e="T267" id="Seg_8011" s="T266">смотреть-PST-1SG</ta>
            <ta e="T268" id="Seg_8012" s="T267">смотреть-PST-1SG</ta>
            <ta e="T269" id="Seg_8013" s="T268">тогда</ta>
            <ta e="T270" id="Seg_8014" s="T269">PTCL</ta>
            <ta e="T271" id="Seg_8015" s="T270">кричать-MOM-PST-1SG</ta>
            <ta e="T272" id="Seg_8016" s="T271">этот.[NOM.SG]</ta>
            <ta e="T273" id="Seg_8017" s="T272">PTCL</ta>
            <ta e="T274" id="Seg_8018" s="T273">бежать-MOM-PST.[3SG]</ta>
            <ta e="T275" id="Seg_8019" s="T274">вода-LAT</ta>
            <ta e="T276" id="Seg_8020" s="T275">%змея.[NOM.SG]</ta>
            <ta e="T277" id="Seg_8021" s="T276">тогда</ta>
            <ta e="T278" id="Seg_8022" s="T277">этот.[NOM.SG]</ta>
            <ta e="T279" id="Seg_8023" s="T278">PTCL</ta>
            <ta e="T280" id="Seg_8024" s="T279">пойти-MOM-PST.[3SG]</ta>
            <ta e="T281" id="Seg_8025" s="T280">и</ta>
            <ta e="T282" id="Seg_8026" s="T281">бежать-MOM-PST.[3SG]</ta>
            <ta e="T283" id="Seg_8027" s="T282">я.NOM</ta>
            <ta e="T284" id="Seg_8028" s="T283">прийти-PST-1SG</ta>
            <ta e="T285" id="Seg_8029" s="T284">и</ta>
            <ta e="T286" id="Seg_8030" s="T285">Влас-LAT</ta>
            <ta e="T287" id="Seg_8031" s="T286">сказать-PST-1SG</ta>
            <ta e="T288" id="Seg_8032" s="T287">этот.[NOM.SG]</ta>
            <ta e="T289" id="Seg_8033" s="T288">пойти-PST.[3SG]</ta>
            <ta e="T290" id="Seg_8034" s="T289">ружье-INS</ta>
            <ta e="T291" id="Seg_8035" s="T290">и</ta>
            <ta e="T293" id="Seg_8036" s="T292">убить-PST.[3SG]</ta>
            <ta e="T295" id="Seg_8037" s="T293">котел.[NOM.SG]</ta>
            <ta e="T296" id="Seg_8038" s="T295">я.NOM</ta>
            <ta e="T297" id="Seg_8039" s="T296">тогда</ta>
            <ta e="T298" id="Seg_8040" s="T297">мясо.[NOM.SG]</ta>
            <ta e="T299" id="Seg_8041" s="T298">мыть-PST-1SG</ta>
            <ta e="T300" id="Seg_8042" s="T299">и</ta>
            <ta e="T301" id="Seg_8043" s="T300">съесть-PST-1SG</ta>
            <ta e="T302" id="Seg_8044" s="T301">мы.LAT</ta>
            <ta e="T303" id="Seg_8045" s="T302">дать-PST.[3SG]</ta>
            <ta e="T304" id="Seg_8046" s="T303">один.[NOM.SG]</ta>
            <ta e="T305" id="Seg_8047" s="T304">котел.[NOM.SG]</ta>
            <ta e="T306" id="Seg_8048" s="T866">мужчина.[NOM.SG]</ta>
            <ta e="T307" id="Seg_8049" s="T306">прийти-PST.[3SG]</ta>
            <ta e="T308" id="Seg_8050" s="T307">я.NOM</ta>
            <ta e="T309" id="Seg_8051" s="T308">там</ta>
            <ta e="T310" id="Seg_8052" s="T309">идти-PST-1SG</ta>
            <ta e="T311" id="Seg_8053" s="T310">этот-PL</ta>
            <ta e="T312" id="Seg_8054" s="T311">PTCL</ta>
            <ta e="T314" id="Seg_8055" s="T312">говорить-DUR-3PL</ta>
            <ta e="T315" id="Seg_8056" s="T314">а</ta>
            <ta e="T316" id="Seg_8057" s="T315">этот-PL</ta>
            <ta e="T317" id="Seg_8058" s="T316">PTCL</ta>
            <ta e="T318" id="Seg_8059" s="T317">я.ACC</ta>
            <ta e="T319" id="Seg_8060" s="T318">гнать-MOM-PST-3PL</ta>
            <ta e="T320" id="Seg_8061" s="T319">я.NOM</ta>
            <ta e="T321" id="Seg_8062" s="T320">бежать-MOM-PST-1SG</ta>
            <ta e="T323" id="Seg_8063" s="T321">один</ta>
            <ta e="T324" id="Seg_8064" s="T323">камасинец-PL</ta>
            <ta e="T325" id="Seg_8065" s="T324">жить-PST-3PL</ta>
            <ta e="T326" id="Seg_8066" s="T325">хороший.[NOM.SG]</ta>
            <ta e="T327" id="Seg_8067" s="T326">быть-PST.[3SG]</ta>
            <ta e="T328" id="Seg_8068" s="T327">мясо-NOM/GEN.3SG</ta>
            <ta e="T329" id="Seg_8069" s="T328">много</ta>
            <ta e="T330" id="Seg_8070" s="T329">быть-PST.[3SG]</ta>
            <ta e="T331" id="Seg_8071" s="T330">PTCL</ta>
            <ta e="T332" id="Seg_8072" s="T331">съесть-PRS-2SG</ta>
            <ta e="T333" id="Seg_8073" s="T332">олень-EP-GEN</ta>
            <ta e="T334" id="Seg_8074" s="T333">мясо.[NOM.SG]</ta>
            <ta e="T335" id="Seg_8075" s="T334">очень</ta>
            <ta e="T336" id="Seg_8076" s="T335">мягкий.[NOM.SG]</ta>
            <ta e="T337" id="Seg_8077" s="T336">как</ta>
            <ta e="T338" id="Seg_8078" s="T337">хлеб.[NOM.SG]</ta>
            <ta e="T339" id="Seg_8079" s="T338">много</ta>
            <ta e="T340" id="Seg_8080" s="T339">съесть-PRS-2SG</ta>
            <ta e="T341" id="Seg_8081" s="T340">тогда</ta>
            <ta e="T342" id="Seg_8082" s="T341">вода.[NOM.SG]</ta>
            <ta e="T343" id="Seg_8083" s="T342">пить-PRS-2SG</ta>
            <ta e="T344" id="Seg_8084" s="T865">вода.[NOM.SG]</ta>
            <ta e="T345" id="Seg_8085" s="T344">пить-PRS-2SG</ta>
            <ta e="T348" id="Seg_8086" s="T345">ну</ta>
            <ta e="T349" id="Seg_8087" s="T348">этот.[NOM.SG]</ta>
            <ta e="T350" id="Seg_8088" s="T349">вождь.[NOM.SG]</ta>
            <ta e="T351" id="Seg_8089" s="T350">очень</ta>
            <ta e="T352" id="Seg_8090" s="T351">хороший.[NOM.SG]</ta>
            <ta e="T353" id="Seg_8091" s="T352">очень</ta>
            <ta e="T354" id="Seg_8092" s="T353">красивый.[NOM.SG]</ta>
            <ta e="T355" id="Seg_8093" s="T354">глаз-NOM/GEN.3SG</ta>
            <ta e="T357" id="Seg_8094" s="T356">черный.[NOM.SG]</ta>
            <ta e="T358" id="Seg_8095" s="T357">нос-NOM/GEN.3SG</ta>
            <ta e="T359" id="Seg_8096" s="T358">NEG</ta>
            <ta e="T360" id="Seg_8097" s="T359">длинный.[NOM.SG]</ta>
            <ta e="T361" id="Seg_8098" s="T360">губа-PL-NOM/GEN/ACC.2SG</ta>
            <ta e="T362" id="Seg_8099" s="T361">красивый.[NOM.SG]</ta>
            <ta e="T363" id="Seg_8100" s="T362">PTCL</ta>
            <ta e="T364" id="Seg_8101" s="T363">губа-PL-NOM/GEN/ACC.2SG</ta>
            <ta e="T365" id="Seg_8102" s="T364">NEG</ta>
            <ta e="T366" id="Seg_8103" s="T365">толстый.[NOM.SG]</ta>
            <ta e="T368" id="Seg_8104" s="T367">PTCL</ta>
            <ta e="T369" id="Seg_8105" s="T368">болеть-MOM-PST.[3SG]</ta>
            <ta e="T370" id="Seg_8106" s="T369">я.NOM</ta>
            <ta e="T372" id="Seg_8107" s="T371">этот-LAT</ta>
            <ta e="T374" id="Seg_8108" s="T373">пойти-FUT-1SG</ta>
            <ta e="T375" id="Seg_8109" s="T374">этот.[NOM.SG]</ta>
            <ta e="T376" id="Seg_8110" s="T375">PTCL</ta>
            <ta e="T377" id="Seg_8111" s="T376">сказать-IPFVZ.[3SG]</ta>
            <ta e="T378" id="Seg_8112" s="T377">NEG.AUX-IMP.2SG</ta>
            <ta e="T379" id="Seg_8113" s="T378">говорить-EP-CNG</ta>
            <ta e="T380" id="Seg_8114" s="T379">русский-GEN</ta>
            <ta e="T381" id="Seg_8115" s="T380">язык-3SG-INS</ta>
            <ta e="T382" id="Seg_8116" s="T381">а</ta>
            <ta e="T383" id="Seg_8117" s="T382">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T384" id="Seg_8118" s="T383">язык-3SG-INS</ta>
            <ta e="T385" id="Seg_8119" s="T384">говорить-EP-IMP.2SG</ta>
            <ta e="T386" id="Seg_8120" s="T385">я.NOM</ta>
            <ta e="T387" id="Seg_8121" s="T386">очень</ta>
            <ta e="T388" id="Seg_8122" s="T387">болеть-PRS-1SG</ta>
            <ta e="T389" id="Seg_8123" s="T388">PTCL</ta>
            <ta e="T390" id="Seg_8124" s="T389">очень</ta>
            <ta e="T391" id="Seg_8125" s="T390">болеть-PRS-1SG</ta>
            <ta e="T392" id="Seg_8126" s="T391">PTCL</ta>
            <ta e="T393" id="Seg_8127" s="T392">я.NOM</ta>
            <ta e="T394" id="Seg_8128" s="T393">PTCL</ta>
            <ta e="T395" id="Seg_8129" s="T394">умереть-RES-PST-1SG</ta>
            <ta e="T396" id="Seg_8130" s="T395">умереть-RES-PST-1SG</ta>
            <ta e="T397" id="Seg_8131" s="T396">а</ta>
            <ta e="T398" id="Seg_8132" s="T397">ты.NOM</ta>
            <ta e="T399" id="Seg_8133" s="T398">остаться-FUT-2SG</ta>
            <ta e="T400" id="Seg_8134" s="T399">этот.[NOM.SG]</ta>
            <ta e="T401" id="Seg_8135" s="T400">умереть-RES-PST.[3SG]</ta>
            <ta e="T402" id="Seg_8136" s="T401">два.[NOM.SG]</ta>
            <ta e="T403" id="Seg_8137" s="T402">дочь-NOM/GEN.3SG</ta>
            <ta e="T405" id="Seg_8138" s="T404">остаться-MOM-PST-3PL</ta>
            <ta e="T406" id="Seg_8139" s="T405">один.[NOM.SG]</ta>
            <ta e="T407" id="Seg_8140" s="T406">этот-INS</ta>
            <ta e="T408" id="Seg_8141" s="T407">быть-PST.[3SG]</ta>
            <ta e="T409" id="Seg_8142" s="T408">а</ta>
            <ta e="T410" id="Seg_8143" s="T409">один.[NOM.SG]</ta>
            <ta e="T411" id="Seg_8144" s="T410">далеко</ta>
            <ta e="T412" id="Seg_8145" s="T411">быть-PST.[3SG]</ta>
            <ta e="T413" id="Seg_8146" s="T412">сегодня</ta>
            <ta e="T415" id="Seg_8147" s="T413">PTCL</ta>
            <ta e="T420" id="Seg_8148" s="T419">сегодня</ta>
            <ta e="T421" id="Seg_8149" s="T420">целый</ta>
            <ta e="T422" id="Seg_8150" s="T421">день-NOM/GEN.3SG</ta>
            <ta e="T423" id="Seg_8151" s="T422">дождь.[NOM.SG]</ta>
            <ta e="T424" id="Seg_8152" s="T423">прийти-PST.[3SG]</ta>
            <ta e="T425" id="Seg_8153" s="T424">а</ta>
            <ta e="T426" id="Seg_8154" s="T425">сейчас</ta>
            <ta e="T427" id="Seg_8155" s="T426">остаться</ta>
            <ta e="T428" id="Seg_8156" s="T427">сверкать-DUR.[3SG]</ta>
            <ta e="T429" id="Seg_8157" s="T428">Новик.[NOM.SG]</ta>
            <ta e="T430" id="Seg_8158" s="T429">прийти-PRS.[3SG]</ta>
            <ta e="T431" id="Seg_8159" s="T430">свинья-LAT/LOC.3SG</ta>
            <ta e="T432" id="Seg_8160" s="T431">а</ta>
            <ta e="T433" id="Seg_8161" s="T432">Сидор.[NOM.SG]</ta>
            <ta e="T434" id="Seg_8162" s="T433">собака-LAT</ta>
            <ta e="T435" id="Seg_8163" s="T434">сидеть-DUR.[3SG]</ta>
            <ta e="T436" id="Seg_8164" s="T435">а</ta>
            <ta e="T437" id="Seg_8165" s="T436">люди.[NOM.SG]</ta>
            <ta e="T438" id="Seg_8166" s="T437">видеть-PST-3PL</ta>
            <ta e="T439" id="Seg_8167" s="T438">думать-PST-3PL</ta>
            <ta e="T441" id="Seg_8168" s="T440">председатель.[NOM.SG]</ta>
            <ta e="T442" id="Seg_8169" s="T441">сказать-PST.[3SG]</ta>
            <ta e="T443" id="Seg_8170" s="T442">надо</ta>
            <ta e="T444" id="Seg_8171" s="T443">мельница-NOM/GEN/ACC.3SG</ta>
            <ta e="T445" id="Seg_8172" s="T444">%налаживать-INF.LAT</ta>
            <ta e="T446" id="Seg_8173" s="T445">хороший.[NOM.SG]</ta>
            <ta e="T447" id="Seg_8174" s="T446">люди.[NOM.SG]</ta>
            <ta e="T448" id="Seg_8175" s="T447">собирать-INF.LAT</ta>
            <ta e="T451" id="Seg_8176" s="T450">JUSS</ta>
            <ta e="T452" id="Seg_8177" s="T451">там</ta>
            <ta e="T453" id="Seg_8178" s="T452">работать-FUT-3PL</ta>
            <ta e="T454" id="Seg_8179" s="T453">надо</ta>
            <ta e="T455" id="Seg_8180" s="T454">мельница-NOM/GEN/ACC.3SG</ta>
            <ta e="T456" id="Seg_8181" s="T455">пойти-INF.LAT</ta>
            <ta e="T457" id="Seg_8182" s="T456">мука.[NOM.SG]</ta>
            <ta e="T458" id="Seg_8183" s="T457">курить-INF.LAT</ta>
            <ta e="T459" id="Seg_8184" s="T458">а.то</ta>
            <ta e="T460" id="Seg_8185" s="T459">мука.[NOM.SG]</ta>
            <ta e="T461" id="Seg_8186" s="T460">NEG.EX.[3SG]</ta>
            <ta e="T463" id="Seg_8187" s="T462">я.NOM</ta>
            <ta e="T464" id="Seg_8188" s="T463">маленький.[NOM.SG]</ta>
            <ta e="T465" id="Seg_8189" s="T464">быть-PST-1SG</ta>
            <ta e="T466" id="Seg_8190" s="T465">а</ta>
            <ta e="T467" id="Seg_8191" s="T466">девушка-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T468" id="Seg_8192" s="T467">большой.[NOM.SG]</ta>
            <ta e="T469" id="Seg_8193" s="T468">быть-PST-3PL</ta>
            <ta e="T470" id="Seg_8194" s="T469">всегда</ta>
            <ta e="T471" id="Seg_8195" s="T470">я.ACC</ta>
            <ta e="T472" id="Seg_8196" s="T471">взять-PST-3PL</ta>
            <ta e="T473" id="Seg_8197" s="T472">сам-LAT/LOC.3SG</ta>
            <ta e="T474" id="Seg_8198" s="T473">песня.[NOM.SG]</ta>
            <ta e="T475" id="Seg_8199" s="T474">петь-INF.LAT</ta>
            <ta e="T476" id="Seg_8200" s="T475">я.LAT</ta>
            <ta e="T480" id="Seg_8201" s="T479">красивый.[NOM.SG]</ta>
            <ta e="T481" id="Seg_8202" s="T480">быть-PST.[3SG]</ta>
            <ta e="T482" id="Seg_8203" s="T481">петь-PST-1SG</ta>
            <ta e="T483" id="Seg_8204" s="T482">PTCL</ta>
            <ta e="T484" id="Seg_8205" s="T483">этот.[NOM.SG]</ta>
            <ta e="T485" id="Seg_8206" s="T484">мужчина.[NOM.SG]</ta>
            <ta e="T486" id="Seg_8207" s="T485">очень</ta>
            <ta e="T487" id="Seg_8208" s="T486">хороший.[NOM.SG]</ta>
            <ta e="T488" id="Seg_8209" s="T487">петь-PRS.[3SG]</ta>
            <ta e="T492" id="Seg_8210" s="T491">очень</ta>
            <ta e="T493" id="Seg_8211" s="T492">красивый.[NOM.SG]</ta>
            <ta e="T501" id="Seg_8212" s="T500">очень</ta>
            <ta e="T502" id="Seg_8213" s="T501">сильно</ta>
            <ta e="T503" id="Seg_8214" s="T502">петь-DUR.[3SG]</ta>
            <ta e="T504" id="Seg_8215" s="T503">этот.[NOM.SG]</ta>
            <ta e="T505" id="Seg_8216" s="T504">мужчина.[NOM.SG]</ta>
            <ta e="T506" id="Seg_8217" s="T505">умереть-RES-PST.[3SG]</ta>
            <ta e="T507" id="Seg_8218" s="T506">надо</ta>
            <ta e="T508" id="Seg_8219" s="T507">чум-NOM/GEN/ACC.3SG</ta>
            <ta e="T509" id="Seg_8220" s="T508">делать-INF.LAT</ta>
            <ta e="T510" id="Seg_8221" s="T509">дерево.[NOM.SG]</ta>
            <ta e="T511" id="Seg_8222" s="T510">дерево-INS</ta>
            <ta e="T512" id="Seg_8223" s="T511">дерево-ABL</ta>
            <ta e="T513" id="Seg_8224" s="T512">и</ta>
            <ta e="T514" id="Seg_8225" s="T513">там</ta>
            <ta e="T515" id="Seg_8226" s="T514">этот-ACC</ta>
            <ta e="T516" id="Seg_8227" s="T515">класть-INF.LAT</ta>
            <ta e="T517" id="Seg_8228" s="T516">и</ta>
            <ta e="T518" id="Seg_8229" s="T517">крест.[NOM.SG]</ta>
            <ta e="T519" id="Seg_8230" s="T518">делать-INF.LAT</ta>
            <ta e="T520" id="Seg_8231" s="T519">там</ta>
            <ta e="T521" id="Seg_8232" s="T520">сейчас</ta>
            <ta e="T522" id="Seg_8233" s="T521">пойти-FUT-3SG</ta>
            <ta e="T523" id="Seg_8234" s="T522">тот.[NOM.SG]</ta>
            <ta e="T524" id="Seg_8235" s="T523">место-LAT</ta>
            <ta e="T525" id="Seg_8236" s="T524">очень</ta>
            <ta e="T526" id="Seg_8237" s="T525">красивый.[NOM.SG]</ta>
            <ta e="T527" id="Seg_8238" s="T526">дом-PL</ta>
            <ta e="T528" id="Seg_8239" s="T527">стоять-PRS-3PL</ta>
            <ta e="T529" id="Seg_8240" s="T528">этот-PL</ta>
            <ta e="T530" id="Seg_8241" s="T529">этот-PL</ta>
            <ta e="T533" id="Seg_8242" s="T532">камень-PL</ta>
            <ta e="T535" id="Seg_8243" s="T534">класть-PST-3PL</ta>
            <ta e="T536" id="Seg_8244" s="T535">окно</ta>
            <ta e="T537" id="Seg_8245" s="T536">окно-EP-GEN</ta>
            <ta e="T538" id="Seg_8246" s="T537">край-LAT/LOC.3SG</ta>
            <ta e="T539" id="Seg_8247" s="T538">PTCL</ta>
            <ta e="T540" id="Seg_8248" s="T539">дерево-PL</ta>
            <ta e="T542" id="Seg_8249" s="T541">сажать-PST-3PL</ta>
            <ta e="T543" id="Seg_8250" s="T542">дерево-PL</ta>
            <ta e="T544" id="Seg_8251" s="T543">сажать-PST-3PL</ta>
            <ta e="T546" id="Seg_8252" s="T545">сажать-PST-3PL</ta>
            <ta e="T547" id="Seg_8253" s="T546">очень</ta>
            <ta e="T548" id="Seg_8254" s="T547">красивый.[NOM.SG]</ta>
            <ta e="T549" id="Seg_8255" s="T548">дом.[NOM.SG]</ta>
            <ta e="T552" id="Seg_8256" s="T551">черемуха</ta>
            <ta e="T553" id="Seg_8257" s="T552">ягода.[NOM.SG]</ta>
            <ta e="T554" id="Seg_8258" s="T553">PTCL</ta>
            <ta e="T555" id="Seg_8259" s="T554">сажать-PST-3PL</ta>
            <ta e="T867" id="Seg_8260" s="T555">левый</ta>
            <ta e="T557" id="Seg_8261" s="T556">рука-LAT</ta>
            <ta e="T558" id="Seg_8262" s="T557">красивый.[NOM.SG]</ta>
            <ta e="T559" id="Seg_8263" s="T558">и</ta>
            <ta e="T560" id="Seg_8264" s="T559">правый</ta>
            <ta e="T561" id="Seg_8265" s="T560">рука-LAT</ta>
            <ta e="T563" id="Seg_8266" s="T561">красивый.[NOM.SG]</ta>
            <ta e="T567" id="Seg_8267" s="T566">очень</ta>
            <ta e="T568" id="Seg_8268" s="T567">красивый.[NOM.SG]</ta>
            <ta e="T570" id="Seg_8269" s="T569">дом-PL</ta>
            <ta e="T571" id="Seg_8270" s="T570">стоять-PRS-3PL</ta>
            <ta e="T572" id="Seg_8271" s="T571">я.NOM</ta>
            <ta e="T573" id="Seg_8272" s="T572">невестка-NOM/GEN/ACC.1SG</ta>
            <ta e="T575" id="Seg_8273" s="T574">кипятить-PST.[3SG]</ta>
            <ta e="T576" id="Seg_8274" s="T575">этот.[NOM.SG]</ta>
            <ta e="T577" id="Seg_8275" s="T576">PTCL</ta>
            <ta e="T578" id="Seg_8276" s="T577">NEG</ta>
            <ta e="T579" id="Seg_8277" s="T578">крепкий.[NOM.SG]</ta>
            <ta e="T580" id="Seg_8278" s="T579">мужчина.[NOM.SG]</ta>
            <ta e="T581" id="Seg_8279" s="T580">PTCL</ta>
            <ta e="T582" id="Seg_8280" s="T581">ругать-DES-DUR.[3SG]</ta>
            <ta e="T583" id="Seg_8281" s="T582">еще</ta>
            <ta e="T584" id="Seg_8282" s="T583">NEG</ta>
            <ta e="T585" id="Seg_8283" s="T584">быть-FUT-2SG</ta>
            <ta e="T586" id="Seg_8284" s="T585">кипятить-INF.LAT</ta>
            <ta e="T587" id="Seg_8285" s="T586">NEG</ta>
            <ta e="T588" id="Seg_8286" s="T587">хороший.[NOM.SG]</ta>
            <ta e="T589" id="Seg_8287" s="T588">NEG</ta>
            <ta e="T590" id="Seg_8288" s="T589">крепкий.[NOM.SG]</ta>
            <ta e="T591" id="Seg_8289" s="T590">тогда</ta>
            <ta e="T592" id="Seg_8290" s="T591">этот.[NOM.SG]</ta>
            <ta e="T593" id="Seg_8291" s="T592">невестка-NOM/GEN/ACC.1SG</ta>
            <ta e="T595" id="Seg_8292" s="T594">вода-ACC</ta>
            <ta e="T596" id="Seg_8293" s="T595">кипятить-PST.[3SG]</ta>
            <ta e="T597" id="Seg_8294" s="T596">соль-INS</ta>
            <ta e="T598" id="Seg_8295" s="T597">тогда</ta>
            <ta e="T600" id="Seg_8296" s="T599">мясо.[NOM.SG]</ta>
            <ta e="T601" id="Seg_8297" s="T600">резать-PST.[3SG]</ta>
            <ta e="T602" id="Seg_8298" s="T601">и</ta>
            <ta e="T603" id="Seg_8299" s="T602">там</ta>
            <ta e="T604" id="Seg_8300" s="T603">класть-IPFVZ-PRS.[3SG]</ta>
            <ta e="T605" id="Seg_8301" s="T604">чтобы</ta>
            <ta e="T606" id="Seg_8302" s="T605">солить-DUR.[3SG]</ta>
            <ta e="T607" id="Seg_8303" s="T606">хозяин-NOM/GEN.3SG</ta>
            <ta e="T608" id="Seg_8304" s="T607">хозяин-EP-GEN</ta>
            <ta e="T609" id="Seg_8305" s="T608">женщина.[NOM.SG]</ta>
            <ta e="T610" id="Seg_8306" s="T609">пойти-INF.LAT</ta>
            <ta e="T613" id="Seg_8307" s="T612">очень</ta>
            <ta e="T614" id="Seg_8308" s="T613">вода.[NOM.SG]</ta>
            <ta e="T615" id="Seg_8309" s="T614">надо</ta>
            <ta e="T616" id="Seg_8310" s="T615">вернуться-MOM-INF.LAT</ta>
            <ta e="T617" id="Seg_8311" s="T616">очень</ta>
            <ta e="T618" id="Seg_8312" s="T617">далеко</ta>
            <ta e="T619" id="Seg_8313" s="T618">быть.видным-PRS.[3SG]</ta>
            <ta e="T620" id="Seg_8314" s="T868">PTCL</ta>
            <ta e="T621" id="Seg_8315" s="T620">дерево-PL</ta>
            <ta e="T622" id="Seg_8316" s="T621">NEG.EX.[3SG]</ta>
            <ta e="T623" id="Seg_8317" s="T622">один.[NOM.SG]</ta>
            <ta e="T624" id="Seg_8318" s="T623">и</ta>
            <ta e="T625" id="Seg_8319" s="T624">дерево.[NOM.SG]</ta>
            <ta e="T627" id="Seg_8320" s="T625">NEG.EX.[3SG]</ta>
            <ta e="T629" id="Seg_8321" s="T628">прийти-PRS-1SG</ta>
            <ta e="T631" id="Seg_8322" s="T630">петь-DUR-1SG</ta>
            <ta e="T632" id="Seg_8323" s="T631">лес-LAT</ta>
            <ta e="T633" id="Seg_8324" s="T632">идти-PRS-1SG</ta>
            <ta e="T634" id="Seg_8325" s="T633">лес.[NOM.SG]</ta>
            <ta e="T636" id="Seg_8326" s="T635">петь-DUR-1SG</ta>
            <ta e="T637" id="Seg_8327" s="T636">вода-LAT</ta>
            <ta e="T638" id="Seg_8328" s="T637">идти-PRS-1SG</ta>
            <ta e="T639" id="Seg_8329" s="T638">вода.[NOM.SG]</ta>
            <ta e="T640" id="Seg_8330" s="T639">петь-DUR-1SG</ta>
            <ta e="T641" id="Seg_8331" s="T640">дерево-LOC</ta>
            <ta e="T642" id="Seg_8332" s="T641">идти-PRS-1SG</ta>
            <ta e="T643" id="Seg_8333" s="T642">дерево.[NOM.SG]</ta>
            <ta e="T644" id="Seg_8334" s="T643">петь-DUR-1SG</ta>
            <ta e="T645" id="Seg_8335" s="T644">я.NOM</ta>
            <ta e="T646" id="Seg_8336" s="T645">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T647" id="Seg_8337" s="T646">Кирель-LAT</ta>
            <ta e="T648" id="Seg_8338" s="T647">пойти-PST.[3SG]</ta>
            <ta e="T650" id="Seg_8339" s="T649">взять-PST.[3SG]</ta>
            <ta e="T651" id="Seg_8340" s="T650">рыба.[NOM.SG]</ta>
            <ta e="T653" id="Seg_8341" s="T652">много</ta>
            <ta e="T654" id="Seg_8342" s="T653">ловить-PST.[3SG]</ta>
            <ta e="T655" id="Seg_8343" s="T654">мы.NOM</ta>
            <ta e="T656" id="Seg_8344" s="T655">чистить-IPFVZ-PST-1PL</ta>
            <ta e="T657" id="Seg_8345" s="T656">и</ta>
            <ta e="T658" id="Seg_8346" s="T657">солить-PST-1PL</ta>
            <ta e="T659" id="Seg_8347" s="T658">и</ta>
            <ta e="T660" id="Seg_8348" s="T659">печь-PST-1PL</ta>
            <ta e="T661" id="Seg_8349" s="T660">и</ta>
            <ta e="T662" id="Seg_8350" s="T661">съесть-PST-1PL</ta>
            <ta e="T663" id="Seg_8351" s="T662">мы.NOM</ta>
            <ta e="T664" id="Seg_8352" s="T663">идти-%1PL.PST</ta>
            <ta e="T665" id="Seg_8353" s="T664">и</ta>
            <ta e="T666" id="Seg_8354" s="T665">очень</ta>
            <ta e="T667" id="Seg_8355" s="T666">большой.[NOM.SG]</ta>
            <ta e="T668" id="Seg_8356" s="T667">вода.[NOM.SG]</ta>
            <ta e="T669" id="Seg_8357" s="T668">там</ta>
            <ta e="T670" id="Seg_8358" s="T669">PTCL</ta>
            <ta e="T671" id="Seg_8359" s="T670">%сеть-NOM/GEN/ACC.3PL</ta>
            <ta e="T672" id="Seg_8360" s="T671">выбросить-PST-%1PL.PST</ta>
            <ta e="T673" id="Seg_8361" s="T672">рыба.[NOM.SG]</ta>
            <ta e="T674" id="Seg_8362" s="T673">ловить-PST-1PL</ta>
            <ta e="T675" id="Seg_8363" s="T674">мужчина.[NOM.SG]</ta>
            <ta e="T676" id="Seg_8364" s="T675">PTCL</ta>
            <ta e="T677" id="Seg_8365" s="T676">женщина-ACC</ta>
            <ta e="T678" id="Seg_8366" s="T677">нажимать-PST.[3SG]</ta>
            <ta e="T679" id="Seg_8367" s="T678">а</ta>
            <ta e="T680" id="Seg_8368" s="T679">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T681" id="Seg_8369" s="T680">дерево-LAT</ta>
            <ta e="T682" id="Seg_8370" s="T681">вешать-MOM-PST.[3SG]</ta>
            <ta e="T684" id="Seg_8371" s="T683">медведь.[NOM.SG]</ta>
            <ta e="T685" id="Seg_8372" s="T684">очень</ta>
            <ta e="T686" id="Seg_8373" s="T685">шерсть-NOM/GEN/ACC.3SG</ta>
            <ta e="T687" id="Seg_8374" s="T686">много</ta>
            <ta e="T688" id="Seg_8375" s="T687">PTCL</ta>
            <ta e="T689" id="Seg_8376" s="T688">мясо.[NOM.SG]</ta>
            <ta e="T690" id="Seg_8377" s="T689">NEG</ta>
            <ta e="T691" id="Seg_8378" s="T690">быть.видным-PRS.[3SG]</ta>
            <ta e="T692" id="Seg_8379" s="T691">очень</ta>
            <ta e="T693" id="Seg_8380" s="T692">этот.[NOM.SG]</ta>
            <ta e="T694" id="Seg_8381" s="T693">мягкий.[NOM.SG]</ta>
            <ta e="T695" id="Seg_8382" s="T694">очень</ta>
            <ta e="T696" id="Seg_8383" s="T695">толстый.[NOM.SG]</ta>
            <ta e="T697" id="Seg_8384" s="T696">нога-NOM/GEN.3SG</ta>
            <ta e="T698" id="Seg_8385" s="T697">очень</ta>
            <ta e="T699" id="Seg_8386" s="T698">крепкий.[NOM.SG]</ta>
            <ta e="T700" id="Seg_8387" s="T699">крепкий.[NOM.SG]</ta>
            <ta e="T701" id="Seg_8388" s="T700">коготь-PL</ta>
            <ta e="T702" id="Seg_8389" s="T701">очень</ta>
            <ta e="T703" id="Seg_8390" s="T702">большой.[NOM.SG]</ta>
            <ta e="T704" id="Seg_8391" s="T703">большой.[NOM.SG]</ta>
            <ta e="T705" id="Seg_8392" s="T704">собака.[NOM.SG]</ta>
            <ta e="T706" id="Seg_8393" s="T705">собака-INS</ta>
            <ta e="T707" id="Seg_8394" s="T706">большой.[NOM.SG]</ta>
            <ta e="T708" id="Seg_8395" s="T707">собака-INS</ta>
            <ta e="T709" id="Seg_8396" s="T708">глаз-NOM/GEN.3SG</ta>
            <ta e="T710" id="Seg_8397" s="T709">маленький.[NOM.SG]</ta>
            <ta e="T711" id="Seg_8398" s="T710">ухо-PL</ta>
            <ta e="T712" id="Seg_8399" s="T711">маленький.[NOM.SG]</ta>
            <ta e="T713" id="Seg_8400" s="T712">я.NOM</ta>
            <ta e="T714" id="Seg_8401" s="T713">родственник-NOM/GEN/ACC.1SG</ta>
            <ta e="T715" id="Seg_8402" s="T714">прийти-PST.[3SG]</ta>
            <ta e="T716" id="Seg_8403" s="T715">мальчик.[NOM.SG]</ta>
            <ta e="T717" id="Seg_8404" s="T716">прийти-PST.[3SG]</ta>
            <ta e="T718" id="Seg_8405" s="T717">женщина-INS</ta>
            <ta e="T719" id="Seg_8406" s="T718">брат-NOM/GEN/ACC.1SG</ta>
            <ta e="T720" id="Seg_8407" s="T719">прийти-PST.[3SG]</ta>
            <ta e="T721" id="Seg_8408" s="T720">женщина-INS</ta>
            <ta e="T723" id="Seg_8409" s="T721">ребенок-PL</ta>
            <ta e="T724" id="Seg_8410" s="T723">я.NOM</ta>
            <ta e="T725" id="Seg_8411" s="T724">родственник-NOM/GEN/ACC.1SG</ta>
            <ta e="T726" id="Seg_8412" s="T725">прийти-PST.[3SG]</ta>
            <ta e="T727" id="Seg_8413" s="T726">мальчик-NOM/GEN/ACC.1SG</ta>
            <ta e="T728" id="Seg_8414" s="T727">прийти-PST.[3SG]</ta>
            <ta e="T729" id="Seg_8415" s="T728">женщина-INS</ta>
            <ta e="T730" id="Seg_8416" s="T729">брат-NOM/GEN/ACC.1SG</ta>
            <ta e="T731" id="Seg_8417" s="T730">прийти-PST.[3SG]</ta>
            <ta e="T732" id="Seg_8418" s="T731">женщина-INS</ta>
            <ta e="T733" id="Seg_8419" s="T732">дочь-NOM/GEN/ACC.1SG</ta>
            <ta e="T734" id="Seg_8420" s="T733">мужчина-INS</ta>
            <ta e="T735" id="Seg_8421" s="T734">прийти-PST.[3SG]</ta>
            <ta e="T736" id="Seg_8422" s="T735">ребенок-PL</ta>
            <ta e="T737" id="Seg_8423" s="T736">ребенок-PL-LAT</ta>
            <ta e="T738" id="Seg_8424" s="T737">этот-INS</ta>
            <ta e="T739" id="Seg_8425" s="T738">прийти-PST-3PL</ta>
            <ta e="T740" id="Seg_8426" s="T739">надо</ta>
            <ta e="T741" id="Seg_8427" s="T740">этот-PL-LAT</ta>
            <ta e="T743" id="Seg_8428" s="T742">собирать-INF.LAT</ta>
            <ta e="T744" id="Seg_8429" s="T743">этот-PL</ta>
            <ta e="T746" id="Seg_8430" s="T745">съесть-INF.LAT</ta>
            <ta e="T747" id="Seg_8431" s="T746">дать-INF.LAT</ta>
            <ta e="T748" id="Seg_8432" s="T747">я.NOM</ta>
            <ta e="T749" id="Seg_8433" s="T748">мальчик-NOM/GEN/ACC.1SG</ta>
            <ta e="T750" id="Seg_8434" s="T749">PTCL</ta>
            <ta e="T751" id="Seg_8435" s="T750">сильно</ta>
            <ta e="T752" id="Seg_8436" s="T751">болеть-PRS.[3SG]</ta>
            <ta e="T753" id="Seg_8437" s="T752">я.NOM</ta>
            <ta e="T754" id="Seg_8438" s="T753">этот-LAT</ta>
            <ta e="T755" id="Seg_8439" s="T754">сказать-PST-1SG</ta>
            <ta e="T756" id="Seg_8440" s="T755">Бог-LAT</ta>
            <ta e="T757" id="Seg_8441" s="T756">молиться-IMP.2SG</ta>
            <ta e="T758" id="Seg_8442" s="T757">спросить-IMP.2SG</ta>
            <ta e="T759" id="Seg_8443" s="T758">оттуда</ta>
            <ta e="T760" id="Seg_8444" s="T759">сила-NOM/GEN/ACC.3SG</ta>
            <ta e="T761" id="Seg_8445" s="T760">позвать-IMP.2SG</ta>
            <ta e="T762" id="Seg_8446" s="T761">сам-%LAT/LOC.1/2SG</ta>
            <ta e="T763" id="Seg_8447" s="T762">этот.[NOM.SG]</ta>
            <ta e="T764" id="Seg_8448" s="T763">PTCL</ta>
            <ta e="T765" id="Seg_8449" s="T764">позвать-PST.[3SG]</ta>
            <ta e="T766" id="Seg_8450" s="T765">прийти-IMP.2SG</ta>
            <ta e="T767" id="Seg_8451" s="T766">я.LAT</ta>
            <ta e="T768" id="Seg_8452" s="T767">сила-NOM/GEN/ACC.2SG</ta>
            <ta e="T769" id="Seg_8453" s="T768">принести-IMP.2SG</ta>
            <ta e="T770" id="Seg_8454" s="T769">вылечить-IMP.2SG.O</ta>
            <ta e="T771" id="Seg_8455" s="T770">я.ACC</ta>
            <ta e="T772" id="Seg_8456" s="T771">я.NOM</ta>
            <ta e="T773" id="Seg_8457" s="T772">вчера</ta>
            <ta e="T774" id="Seg_8458" s="T773">дом.[NOM.SG]</ta>
            <ta e="T775" id="Seg_8459" s="T774">мыть-PST-1SG</ta>
            <ta e="T776" id="Seg_8460" s="T775">курица.[NOM.SG]</ta>
            <ta e="T777" id="Seg_8461" s="T776">испражняться-PST.[3SG]</ta>
            <ta e="T778" id="Seg_8462" s="T777">тоже</ta>
            <ta e="T779" id="Seg_8463" s="T778">мыть-PST-1SG</ta>
            <ta e="T780" id="Seg_8464" s="T779">тогда</ta>
            <ta e="T782" id="Seg_8465" s="T781">мыть-PST-1SG</ta>
            <ta e="T783" id="Seg_8466" s="T782">тогда</ta>
            <ta e="T784" id="Seg_8467" s="T783">пойти-PST-1SG</ta>
            <ta e="T785" id="Seg_8468" s="T784">вода.[NOM.SG]</ta>
            <ta e="T786" id="Seg_8469" s="T785">носить-INF.LAT</ta>
            <ta e="T787" id="Seg_8470" s="T786">баня-LAT</ta>
            <ta e="T789" id="Seg_8471" s="T787">один.[NOM.SG]</ta>
            <ta e="T790" id="Seg_8472" s="T789">шесть.[NOM.SG]</ta>
            <ta e="T791" id="Seg_8473" s="T790">котел.[NOM.SG]</ta>
            <ta e="T792" id="Seg_8474" s="T791">принести-PST-1SG</ta>
            <ta e="T793" id="Seg_8475" s="T792">я.NOM</ta>
            <ta e="T794" id="Seg_8476" s="T793">сегодня</ta>
            <ta e="T795" id="Seg_8477" s="T794">рано-LOC.ADV</ta>
            <ta e="T796" id="Seg_8478" s="T795">встать-PST-1SG</ta>
            <ta e="T799" id="Seg_8479" s="T798">день.[NOM.SG]</ta>
            <ta e="T800" id="Seg_8480" s="T799">работать-PST-1SG</ta>
            <ta e="T801" id="Seg_8481" s="T800">PTCL</ta>
            <ta e="T802" id="Seg_8482" s="T801">устать-MOM-PST-1SG</ta>
            <ta e="T804" id="Seg_8483" s="T803">вечер-LOC.ADV</ta>
            <ta e="T805" id="Seg_8484" s="T804">вечер-LOC.ADV</ta>
            <ta e="T806" id="Seg_8485" s="T805">прийти-PST.[3SG]</ta>
            <ta e="T807" id="Seg_8486" s="T806">я.NOM</ta>
            <ta e="T808" id="Seg_8487" s="T807">спать-MOM-PST-1SG</ta>
            <ta e="T809" id="Seg_8488" s="T808">я.NOM</ta>
            <ta e="T810" id="Seg_8489" s="T809">этот-ACC.PL</ta>
            <ta e="T811" id="Seg_8490" s="T810">видеть-INF.LAT</ta>
            <ta e="T813" id="Seg_8491" s="T812">не</ta>
            <ta e="T814" id="Seg_8492" s="T813">хочется</ta>
            <ta e="T815" id="Seg_8493" s="T814">этот-PL</ta>
            <ta e="T816" id="Seg_8494" s="T815">PTCL</ta>
            <ta e="T817" id="Seg_8495" s="T816">всегда</ta>
            <ta e="T819" id="Seg_8496" s="T818">лгать-DUR-3PL</ta>
            <ta e="T820" id="Seg_8497" s="T819">лгать-DUR-3PL</ta>
            <ta e="T821" id="Seg_8498" s="T820">этот-PL</ta>
            <ta e="T822" id="Seg_8499" s="T821">верно</ta>
            <ta e="T824" id="Seg_8500" s="T823">слушать-INF.LAT</ta>
            <ta e="T825" id="Seg_8501" s="T824">не</ta>
            <ta e="T826" id="Seg_8502" s="T825">хочется</ta>
            <ta e="T827" id="Seg_8503" s="T826">JUSS</ta>
            <ta e="T828" id="Seg_8504" s="T827">я.LAT</ta>
            <ta e="T829" id="Seg_8505" s="T828">глаз-LAT/LOC.3SG</ta>
            <ta e="T830" id="Seg_8506" s="T829">NEG</ta>
            <ta e="T831" id="Seg_8507" s="T830">прийти-FUT-3PL</ta>
            <ta e="T832" id="Seg_8508" s="T831">я.LAT</ta>
            <ta e="T833" id="Seg_8509" s="T832">этот-ACC.PL</ta>
            <ta e="T834" id="Seg_8510" s="T833">NEG</ta>
            <ta e="T835" id="Seg_8511" s="T834">нужно</ta>
            <ta e="T836" id="Seg_8512" s="T835">голова-NOM/GEN/ACC.1SG</ta>
            <ta e="T837" id="Seg_8513" s="T836">волосы-NOM/GEN/ACC.1SG</ta>
            <ta e="T838" id="Seg_8514" s="T837">ухо-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T839" id="Seg_8515" s="T838">зуб-NOM/GEN/ACC.1SG</ta>
            <ta e="T840" id="Seg_8516" s="T839">губа-PL-NOM/GEN/ACC.2SG</ta>
            <ta e="T841" id="Seg_8517" s="T840">язык-NOM/GEN/ACC.1SG</ta>
            <ta e="T842" id="Seg_8518" s="T841">глаз-NOM/GEN/ACC.1SG</ta>
            <ta e="T843" id="Seg_8519" s="T842">нос-NOM/GEN/ACC.1SG</ta>
            <ta e="T844" id="Seg_8520" s="T843">рука-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T845" id="Seg_8521" s="T844">спина-NOM/GEN/ACC.1SG</ta>
            <ta e="T847" id="Seg_8522" s="T846">сердце-NOM/GEN/ACC.1SG</ta>
            <ta e="T848" id="Seg_8523" s="T847">внутри-NOM/GEN/ACC.1SG</ta>
            <ta e="T849" id="Seg_8524" s="T848">живот-NOM/GEN/ACC.1SG</ta>
            <ta e="T851" id="Seg_8525" s="T850">спина-NOM/GEN/ACC.1SG</ta>
            <ta e="T852" id="Seg_8526" s="T851">зад-NOM/GEN/ACC.3SG</ta>
            <ta e="T853" id="Seg_8527" s="T852">нога-NOM/GEN/ACC.1SG</ta>
            <ta e="T854" id="Seg_8528" s="T853">рот-NOM/GEN/ACC.3SG</ta>
            <ta e="T855" id="Seg_8529" s="T854">сердце-NOM/GEN/ACC.1SG</ta>
            <ta e="T856" id="Seg_8530" s="T855">сердце-NOM/GEN/ACC.3SG</ta>
            <ta e="T858" id="Seg_8531" s="T857">грудь-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T861" id="Seg_8532" s="T860">кость-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T863" id="Seg_8533" s="T862">кровь.[NOM.SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc-PKZ">
            <ta e="T5" id="Seg_8534" s="T4">adv</ta>
            <ta e="T6" id="Seg_8535" s="T5">v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_8536" s="T6">ptcl</ta>
            <ta e="T8" id="Seg_8537" s="T7">pers</ta>
            <ta e="T9" id="Seg_8538" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_8539" s="T9">v-v:mood.pn</ta>
            <ta e="T11" id="Seg_8540" s="T10">pers</ta>
            <ta e="T12" id="Seg_8541" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_8542" s="T12">que</ta>
            <ta e="T14" id="Seg_8543" s="T13">v-v:tense-v:pn</ta>
            <ta e="T15" id="Seg_8544" s="T14">propr-n:case</ta>
            <ta e="T869" id="Seg_8545" s="T16">v-v:n.fin</ta>
            <ta e="T17" id="Seg_8546" s="T869">v-v:tense-v:pn</ta>
            <ta e="T18" id="Seg_8547" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_8548" s="T18">adv</ta>
            <ta e="T20" id="Seg_8549" s="T19">v-v:tense-v:pn</ta>
            <ta e="T21" id="Seg_8550" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_8551" s="T21">num-num&gt;num</ta>
            <ta e="T870" id="Seg_8552" s="T22">v-v:n.fin</ta>
            <ta e="T23" id="Seg_8553" s="T870">v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_8554" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_8555" s="T24">que</ta>
            <ta e="T27" id="Seg_8556" s="T26">v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_8557" s="T28">adv</ta>
            <ta e="T30" id="Seg_8558" s="T29">v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_8559" s="T30">adv</ta>
            <ta e="T32" id="Seg_8560" s="T31">adv-adv:case</ta>
            <ta e="T33" id="Seg_8561" s="T32">que-n:case</ta>
            <ta e="T34" id="Seg_8562" s="T33">v-v&gt;v-v:pn</ta>
            <ta e="T35" id="Seg_8563" s="T34">ptcl</ta>
            <ta e="T38" id="Seg_8564" s="T37">ptcl</ta>
            <ta e="T40" id="Seg_8565" s="T39">v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_8566" s="T40">n-n:case</ta>
            <ta e="T43" id="Seg_8567" s="T42">v</ta>
            <ta e="T44" id="Seg_8568" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_8569" s="T44">n-n:case.poss</ta>
            <ta e="T46" id="Seg_8570" s="T45">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T47" id="Seg_8571" s="T46">n-n:case</ta>
            <ta e="T48" id="Seg_8572" s="T47">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T49" id="Seg_8573" s="T48">pers</ta>
            <ta e="T50" id="Seg_8574" s="T49">adv</ta>
            <ta e="T51" id="Seg_8575" s="T50">dempro-n:case</ta>
            <ta e="T53" id="Seg_8576" s="T52">v-v:tense-v:pn</ta>
            <ta e="T54" id="Seg_8577" s="T53">n-n:case.poss-n:case</ta>
            <ta e="T55" id="Seg_8578" s="T54">n-n:case</ta>
            <ta e="T56" id="Seg_8579" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_8580" s="T56">n-n:case</ta>
            <ta e="T58" id="Seg_8581" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_8582" s="T58">v-v&gt;v-v:pn</ta>
            <ta e="T60" id="Seg_8583" s="T59">n-n:case</ta>
            <ta e="T61" id="Seg_8584" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_8585" s="T61">v-v:n.fin</ta>
            <ta e="T63" id="Seg_8586" s="T62">n-n:case</ta>
            <ta e="T65" id="Seg_8587" s="T64">v-v:n.fin</ta>
            <ta e="T66" id="Seg_8588" s="T65">adv</ta>
            <ta e="T67" id="Seg_8589" s="T66">v-v:tense-v:pn</ta>
            <ta e="T68" id="Seg_8590" s="T67">dempro-n:num</ta>
            <ta e="T69" id="Seg_8591" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_8592" s="T69">v-v:mood.pn</ta>
            <ta e="T71" id="Seg_8593" s="T70">n-n:case</ta>
            <ta e="T72" id="Seg_8594" s="T71">n-n:case</ta>
            <ta e="T73" id="Seg_8595" s="T72">n-n:case.poss</ta>
            <ta e="T74" id="Seg_8596" s="T73">v-v:pn</ta>
            <ta e="T75" id="Seg_8597" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_8598" s="T75">que-n:case=ptcl</ta>
            <ta e="T77" id="Seg_8599" s="T76">ptcl</ta>
            <ta e="T78" id="Seg_8600" s="T77">v-v:pn</ta>
            <ta e="T79" id="Seg_8601" s="T78">que-n:case=ptcl</ta>
            <ta e="T80" id="Seg_8602" s="T79">ptcl</ta>
            <ta e="T81" id="Seg_8603" s="T80">v-v:tense-v:pn</ta>
            <ta e="T82" id="Seg_8604" s="T81">v-v:n.fin</ta>
            <ta e="T83" id="Seg_8605" s="T82">ptcl</ta>
            <ta e="T84" id="Seg_8606" s="T83">n-n:case.poss</ta>
            <ta e="T85" id="Seg_8607" s="T84">v-v:pn</ta>
            <ta e="T86" id="Seg_8608" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_8609" s="T86">v-v:tense-v:pn</ta>
            <ta e="T88" id="Seg_8610" s="T87">ptcl</ta>
            <ta e="T92" id="Seg_8611" s="T91">v-v&gt;v-v:pn</ta>
            <ta e="T93" id="Seg_8612" s="T92">n-n:case</ta>
            <ta e="T94" id="Seg_8613" s="T93">v-v:tense-v:pn</ta>
            <ta e="T95" id="Seg_8614" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_8615" s="T95">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T97" id="Seg_8616" s="T96">adv</ta>
            <ta e="T98" id="Seg_8617" s="T97">v-v:tense-v:pn</ta>
            <ta e="T99" id="Seg_8618" s="T98">ptcl</ta>
            <ta e="T100" id="Seg_8619" s="T99">adv</ta>
            <ta e="T101" id="Seg_8620" s="T100">v-v:tense-v:pn</ta>
            <ta e="T102" id="Seg_8621" s="T101">adv</ta>
            <ta e="T103" id="Seg_8622" s="T102">v-v:tense-v:pn</ta>
            <ta e="T104" id="Seg_8623" s="T103">que-n:case=ptcl</ta>
            <ta e="T106" id="Seg_8624" s="T104">ptcl</ta>
            <ta e="T107" id="Seg_8625" s="T106">que-n:case=ptcl</ta>
            <ta e="T108" id="Seg_8626" s="T107">ptcl</ta>
            <ta e="T109" id="Seg_8627" s="T108">v-v:tense-v:pn</ta>
            <ta e="T110" id="Seg_8628" s="T109">dempro-n:case</ta>
            <ta e="T111" id="Seg_8629" s="T110">v-v:tense-v:pn</ta>
            <ta e="T112" id="Seg_8630" s="T111">conj</ta>
            <ta e="T113" id="Seg_8631" s="T112">pers</ta>
            <ta e="T114" id="Seg_8632" s="T113">dempro-n:case</ta>
            <ta e="T116" id="Seg_8633" s="T115">v-v:tense-v:pn</ta>
            <ta e="T117" id="Seg_8634" s="T116">aux-v:mood.pn</ta>
            <ta e="T118" id="Seg_8635" s="T117">v-v:ins-v:mood.pn</ta>
            <ta e="T119" id="Seg_8636" s="T118">v-v:ins-v:mood.pn</ta>
            <ta e="T120" id="Seg_8637" s="T119">adv</ta>
            <ta e="T121" id="Seg_8638" s="T120">pers</ta>
            <ta e="T122" id="Seg_8639" s="T121">pers</ta>
            <ta e="T123" id="Seg_8640" s="T122">v-v:tense-v:pn</ta>
            <ta e="T124" id="Seg_8641" s="T123">v-v:ins-v:mood.pn</ta>
            <ta e="T125" id="Seg_8642" s="T124">adv</ta>
            <ta e="T126" id="Seg_8643" s="T125">pers</ta>
            <ta e="T127" id="Seg_8644" s="T126">adv</ta>
            <ta e="T128" id="Seg_8645" s="T127">adj-n:case</ta>
            <ta e="T129" id="Seg_8646" s="T128">n-n:case</ta>
            <ta e="T130" id="Seg_8647" s="T129">v-v:tense-v:pn</ta>
            <ta e="T131" id="Seg_8648" s="T130">n-n:case</ta>
            <ta e="T132" id="Seg_8649" s="T131">quant</ta>
            <ta e="T134" id="Seg_8650" s="T133">adv</ta>
            <ta e="T135" id="Seg_8651" s="T134">v-v:mood.pn</ta>
            <ta e="T136" id="Seg_8652" s="T135">n-n:case.poss</ta>
            <ta e="T137" id="Seg_8653" s="T136">dempro-n:case</ta>
            <ta e="T138" id="Seg_8654" s="T137">n-n:case.poss</ta>
            <ta e="T139" id="Seg_8655" s="T138">ptcl</ta>
            <ta e="T140" id="Seg_8656" s="T139">v-v:mood.pn</ta>
            <ta e="T142" id="Seg_8657" s="T140">v-v:mood.pn</ta>
            <ta e="T143" id="Seg_8658" s="T142">dempro-n:case</ta>
            <ta e="T144" id="Seg_8659" s="T143">v-v:mood.pn</ta>
            <ta e="T145" id="Seg_8660" s="T144">dempro-n:case</ta>
            <ta e="T146" id="Seg_8661" s="T145">n-n:case</ta>
            <ta e="T148" id="Seg_8662" s="T146">refl-n:case.poss</ta>
            <ta e="T149" id="Seg_8663" s="T148">quant</ta>
            <ta e="T150" id="Seg_8664" s="T149">v-v:tense-v:pn</ta>
            <ta e="T151" id="Seg_8665" s="T150">conj</ta>
            <ta e="T152" id="Seg_8666" s="T151">dempro-n:case</ta>
            <ta e="T153" id="Seg_8667" s="T152">adv-n:case</ta>
            <ta e="T154" id="Seg_8668" s="T153">v-v:tense-v:pn</ta>
            <ta e="T155" id="Seg_8669" s="T154">refl-n:case.poss</ta>
            <ta e="T156" id="Seg_8670" s="T155">quant</ta>
            <ta e="T157" id="Seg_8671" s="T156">v-v:tense-v:pn</ta>
            <ta e="T158" id="Seg_8672" s="T157">conj</ta>
            <ta e="T159" id="Seg_8673" s="T158">dempro-n:case</ta>
            <ta e="T160" id="Seg_8674" s="T159">adv-n:case</ta>
            <ta e="T161" id="Seg_8675" s="T160">v-v:tense-v:pn</ta>
            <ta e="T162" id="Seg_8676" s="T161">num-n:case</ta>
            <ta e="T163" id="Seg_8677" s="T162">ptcl</ta>
            <ta e="T164" id="Seg_8678" s="T163">v-v:tense-v:pn</ta>
            <ta e="T165" id="Seg_8679" s="T164">pers</ta>
            <ta e="T166" id="Seg_8680" s="T165">ptcl</ta>
            <ta e="T167" id="Seg_8681" s="T166">adj-n:case</ta>
            <ta e="T168" id="Seg_8682" s="T167">v-v:tense-v:pn</ta>
            <ta e="T169" id="Seg_8683" s="T168">ptcl</ta>
            <ta e="T170" id="Seg_8684" s="T169">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T171" id="Seg_8685" s="T170">n-n:num</ta>
            <ta e="T172" id="Seg_8686" s="T171">v-v:tense-v:pn</ta>
            <ta e="T173" id="Seg_8687" s="T172">conj</ta>
            <ta e="T174" id="Seg_8688" s="T173">ptcl</ta>
            <ta e="T175" id="Seg_8689" s="T174">interj</ta>
            <ta e="T176" id="Seg_8690" s="T175">num-n:case</ta>
            <ta e="T177" id="Seg_8691" s="T176">v-v:tense-v:pn</ta>
            <ta e="T178" id="Seg_8692" s="T177">conj</ta>
            <ta e="T179" id="Seg_8693" s="T178">pers</ta>
            <ta e="T180" id="Seg_8694" s="T179">ptcl</ta>
            <ta e="T181" id="Seg_8695" s="T180">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T182" id="Seg_8696" s="T181">adv</ta>
            <ta e="T183" id="Seg_8697" s="T182">dempro-n:case</ta>
            <ta e="T184" id="Seg_8698" s="T183">ptcl</ta>
            <ta e="T185" id="Seg_8699" s="T184">v-v:tense-v:pn</ta>
            <ta e="T186" id="Seg_8700" s="T185">v-v:tense-v:pn</ta>
            <ta e="T187" id="Seg_8701" s="T186">ptcl</ta>
            <ta e="T189" id="Seg_8702" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_8703" s="T189">v-v:tense-v:pn</ta>
            <ta e="T191" id="Seg_8704" s="T190">v-v:n.fin</ta>
            <ta e="T192" id="Seg_8705" s="T191">pers</ta>
            <ta e="T193" id="Seg_8706" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_8707" s="T193">adv</ta>
            <ta e="T195" id="Seg_8708" s="T194">v-v&gt;v-v:pn</ta>
            <ta e="T197" id="Seg_8709" s="T196">pers</ta>
            <ta e="T198" id="Seg_8710" s="T197">ptcl</ta>
            <ta e="T199" id="Seg_8711" s="T198">ptcl</ta>
            <ta e="T200" id="Seg_8712" s="T199">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T201" id="Seg_8713" s="T200">adj-n:case</ta>
            <ta e="T203" id="Seg_8714" s="T201">ptcl</ta>
            <ta e="T205" id="Seg_8715" s="T204">adj-n:case</ta>
            <ta e="T207" id="Seg_8716" s="T205">ptcl</ta>
            <ta e="T210" id="Seg_8717" s="T209">pers</ta>
            <ta e="T211" id="Seg_8718" s="T210">ptcl</ta>
            <ta e="T212" id="Seg_8719" s="T211">adj-n:case</ta>
            <ta e="T213" id="Seg_8720" s="T212">v-v:tense-v:pn</ta>
            <ta e="T214" id="Seg_8721" s="T213">pers</ta>
            <ta e="T215" id="Seg_8722" s="T214">ptcl</ta>
            <ta e="T216" id="Seg_8723" s="T215">v-v:tense-v:tense-v:pn</ta>
            <ta e="T217" id="Seg_8724" s="T216">n-n:num</ta>
            <ta e="T218" id="Seg_8725" s="T217">conj</ta>
            <ta e="T219" id="Seg_8726" s="T218">n-n:num</ta>
            <ta e="T220" id="Seg_8727" s="T219">v-v:tense-v:pn</ta>
            <ta e="T221" id="Seg_8728" s="T220">conj</ta>
            <ta e="T222" id="Seg_8729" s="T221">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T223" id="Seg_8730" s="T222">que</ta>
            <ta e="T224" id="Seg_8731" s="T223">pers</ta>
            <ta e="T225" id="Seg_8732" s="T224">v-v&gt;v-v:pn</ta>
            <ta e="T226" id="Seg_8733" s="T225">que-n:case</ta>
            <ta e="T229" id="Seg_8734" s="T228">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T230" id="Seg_8735" s="T229">n-n:case</ta>
            <ta e="T231" id="Seg_8736" s="T230">que-n:case</ta>
            <ta e="T232" id="Seg_8737" s="T231">ptcl</ta>
            <ta e="T233" id="Seg_8738" s="T232">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T234" id="Seg_8739" s="T233">que-n:case</ta>
            <ta e="T235" id="Seg_8740" s="T234">ptcl</ta>
            <ta e="T237" id="Seg_8741" s="T236">n-n:case</ta>
            <ta e="T238" id="Seg_8742" s="T237">ptcl</ta>
            <ta e="T239" id="Seg_8743" s="T238">v-v&gt;v-v:pn</ta>
            <ta e="T240" id="Seg_8744" s="T239">que-n:case</ta>
            <ta e="T241" id="Seg_8745" s="T240">ptcl</ta>
            <ta e="T242" id="Seg_8746" s="T241">v-v&gt;v-v:pn</ta>
            <ta e="T243" id="Seg_8747" s="T242">n-n:case</ta>
            <ta e="T244" id="Seg_8748" s="T243">adv</ta>
            <ta e="T245" id="Seg_8749" s="T244">quant</ta>
            <ta e="T247" id="Seg_8750" s="T246">num-n:case</ta>
            <ta e="T248" id="Seg_8751" s="T247">num-n:case</ta>
            <ta e="T249" id="Seg_8752" s="T248">num-n:case</ta>
            <ta e="T250" id="Seg_8753" s="T249">n-n:case.poss</ta>
            <ta e="T251" id="Seg_8754" s="T250">n-n:case</ta>
            <ta e="T252" id="Seg_8755" s="T251">v-v:tense-v:pn</ta>
            <ta e="T253" id="Seg_8756" s="T252">quant</ta>
            <ta e="T254" id="Seg_8757" s="T253">n-n:num</ta>
            <ta e="T255" id="Seg_8758" s="T254">v-v:tense-v:pn</ta>
            <ta e="T256" id="Seg_8759" s="T255">conj</ta>
            <ta e="T257" id="Seg_8760" s="T256">n-n:case.poss</ta>
            <ta e="T258" id="Seg_8761" s="T257">v-v:tense-v:pn</ta>
            <ta e="T259" id="Seg_8762" s="T258">pers</ta>
            <ta e="T260" id="Seg_8763" s="T259">v-v:tense-v:pn</ta>
            <ta e="T261" id="Seg_8764" s="T260">propr-n:case</ta>
            <ta e="T262" id="Seg_8765" s="T261">v-v:tense-v:pn</ta>
            <ta e="T263" id="Seg_8766" s="T262">ptcl</ta>
            <ta e="T264" id="Seg_8767" s="T263">n-n:case</ta>
            <ta e="T265" id="Seg_8768" s="T264">v-v:tense-v:pn</ta>
            <ta e="T266" id="Seg_8769" s="T265">pers</ta>
            <ta e="T267" id="Seg_8770" s="T266">v-v:tense-v:pn</ta>
            <ta e="T268" id="Seg_8771" s="T267">v-v:tense-v:pn</ta>
            <ta e="T269" id="Seg_8772" s="T268">adv</ta>
            <ta e="T270" id="Seg_8773" s="T269">ptcl</ta>
            <ta e="T271" id="Seg_8774" s="T270">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T272" id="Seg_8775" s="T271">dempro-n:case</ta>
            <ta e="T273" id="Seg_8776" s="T272">ptcl</ta>
            <ta e="T274" id="Seg_8777" s="T273">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T275" id="Seg_8778" s="T274">n-n:case</ta>
            <ta e="T276" id="Seg_8779" s="T275">n-n:case</ta>
            <ta e="T277" id="Seg_8780" s="T276">adv</ta>
            <ta e="T278" id="Seg_8781" s="T277">dempro-n:case</ta>
            <ta e="T279" id="Seg_8782" s="T278">ptcl</ta>
            <ta e="T280" id="Seg_8783" s="T279">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T281" id="Seg_8784" s="T280">conj</ta>
            <ta e="T282" id="Seg_8785" s="T281">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T283" id="Seg_8786" s="T282">pers</ta>
            <ta e="T284" id="Seg_8787" s="T283">v-v:tense-v:pn</ta>
            <ta e="T285" id="Seg_8788" s="T284">conj</ta>
            <ta e="T286" id="Seg_8789" s="T285">propr-n:case</ta>
            <ta e="T287" id="Seg_8790" s="T286">v-v:tense-v:pn</ta>
            <ta e="T288" id="Seg_8791" s="T287">dempro-n:case</ta>
            <ta e="T289" id="Seg_8792" s="T288">v-v:tense-v:pn</ta>
            <ta e="T290" id="Seg_8793" s="T289">n-n:case</ta>
            <ta e="T291" id="Seg_8794" s="T290">conj</ta>
            <ta e="T293" id="Seg_8795" s="T292">v-v:tense-v:pn</ta>
            <ta e="T295" id="Seg_8796" s="T293">n-n:case</ta>
            <ta e="T296" id="Seg_8797" s="T295">pers</ta>
            <ta e="T297" id="Seg_8798" s="T296">adv</ta>
            <ta e="T298" id="Seg_8799" s="T297">n-n:case</ta>
            <ta e="T299" id="Seg_8800" s="T298">v-v:tense-v:pn</ta>
            <ta e="T300" id="Seg_8801" s="T299">conj</ta>
            <ta e="T301" id="Seg_8802" s="T300">v-v:tense-v:pn</ta>
            <ta e="T302" id="Seg_8803" s="T301">pers</ta>
            <ta e="T303" id="Seg_8804" s="T302">v-v:tense-v:pn</ta>
            <ta e="T304" id="Seg_8805" s="T303">num-n:case</ta>
            <ta e="T305" id="Seg_8806" s="T304">n-n:case</ta>
            <ta e="T306" id="Seg_8807" s="T866">n-n:case</ta>
            <ta e="T307" id="Seg_8808" s="T306">v-v:tense-v:pn</ta>
            <ta e="T308" id="Seg_8809" s="T307">pers</ta>
            <ta e="T309" id="Seg_8810" s="T308">adv</ta>
            <ta e="T310" id="Seg_8811" s="T309">v-v:tense-v:pn</ta>
            <ta e="T311" id="Seg_8812" s="T310">dempro-n:num</ta>
            <ta e="T312" id="Seg_8813" s="T311">ptcl</ta>
            <ta e="T314" id="Seg_8814" s="T312">v-v&gt;v-v:pn</ta>
            <ta e="T315" id="Seg_8815" s="T314">conj</ta>
            <ta e="T316" id="Seg_8816" s="T315">dempro-n:num</ta>
            <ta e="T317" id="Seg_8817" s="T316">ptcl</ta>
            <ta e="T318" id="Seg_8818" s="T317">pers</ta>
            <ta e="T319" id="Seg_8819" s="T318">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T320" id="Seg_8820" s="T319">pers</ta>
            <ta e="T321" id="Seg_8821" s="T320">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T323" id="Seg_8822" s="T321">adv</ta>
            <ta e="T324" id="Seg_8823" s="T323">n-n:num</ta>
            <ta e="T325" id="Seg_8824" s="T324">v-v:tense-v:pn</ta>
            <ta e="T326" id="Seg_8825" s="T325">adj-n:case</ta>
            <ta e="T327" id="Seg_8826" s="T326">v-v:tense-v:pn</ta>
            <ta e="T328" id="Seg_8827" s="T327">n-n:case.poss</ta>
            <ta e="T329" id="Seg_8828" s="T328">quant</ta>
            <ta e="T330" id="Seg_8829" s="T329">v-v:tense-v:pn</ta>
            <ta e="T331" id="Seg_8830" s="T330">ptcl</ta>
            <ta e="T332" id="Seg_8831" s="T331">v-v:tense-v:pn</ta>
            <ta e="T333" id="Seg_8832" s="T332">n-n:ins-n:case</ta>
            <ta e="T334" id="Seg_8833" s="T333">n-n:case</ta>
            <ta e="T335" id="Seg_8834" s="T334">adv</ta>
            <ta e="T336" id="Seg_8835" s="T335">adj-n:case</ta>
            <ta e="T337" id="Seg_8836" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_8837" s="T337">n-n:case</ta>
            <ta e="T339" id="Seg_8838" s="T338">quant</ta>
            <ta e="T340" id="Seg_8839" s="T339">v-v:tense-v:pn</ta>
            <ta e="T341" id="Seg_8840" s="T340">adv</ta>
            <ta e="T342" id="Seg_8841" s="T341">n-n:case</ta>
            <ta e="T343" id="Seg_8842" s="T342">v-v:tense-v:pn</ta>
            <ta e="T344" id="Seg_8843" s="T865">n-n:case</ta>
            <ta e="T345" id="Seg_8844" s="T344">v-v:tense-v:pn</ta>
            <ta e="T348" id="Seg_8845" s="T345">ptcl</ta>
            <ta e="T349" id="Seg_8846" s="T348">dempro-n:case</ta>
            <ta e="T350" id="Seg_8847" s="T349">n-n:case</ta>
            <ta e="T351" id="Seg_8848" s="T350">adv</ta>
            <ta e="T352" id="Seg_8849" s="T351">adj-n:case</ta>
            <ta e="T353" id="Seg_8850" s="T352">adv</ta>
            <ta e="T354" id="Seg_8851" s="T353">adj-n:case</ta>
            <ta e="T355" id="Seg_8852" s="T354">n-n:case.poss</ta>
            <ta e="T357" id="Seg_8853" s="T356">adj-n:case</ta>
            <ta e="T358" id="Seg_8854" s="T357">n-n:case.poss</ta>
            <ta e="T359" id="Seg_8855" s="T358">ptcl</ta>
            <ta e="T360" id="Seg_8856" s="T359">adj-n:case</ta>
            <ta e="T361" id="Seg_8857" s="T360">n-n:num-n:case.poss</ta>
            <ta e="T362" id="Seg_8858" s="T361">adj-n:case</ta>
            <ta e="T363" id="Seg_8859" s="T362">ptcl</ta>
            <ta e="T364" id="Seg_8860" s="T363">n-n:num-n:case.poss</ta>
            <ta e="T365" id="Seg_8861" s="T364">ptcl</ta>
            <ta e="T366" id="Seg_8862" s="T365">adj-n:case</ta>
            <ta e="T368" id="Seg_8863" s="T367">ptcl</ta>
            <ta e="T369" id="Seg_8864" s="T368">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T370" id="Seg_8865" s="T369">pers</ta>
            <ta e="T372" id="Seg_8866" s="T371">dempro-n:case</ta>
            <ta e="T374" id="Seg_8867" s="T373">v-v:tense-v:pn</ta>
            <ta e="T375" id="Seg_8868" s="T374">dempro-n:case</ta>
            <ta e="T376" id="Seg_8869" s="T375">ptcl</ta>
            <ta e="T377" id="Seg_8870" s="T376">v-v&gt;v-v:pn</ta>
            <ta e="T378" id="Seg_8871" s="T377">aux-v:mood.pn</ta>
            <ta e="T379" id="Seg_8872" s="T378">v-v:ins-v:mood.pn</ta>
            <ta e="T380" id="Seg_8873" s="T379">n-n:case</ta>
            <ta e="T381" id="Seg_8874" s="T380">n-n:case.poss-n:case</ta>
            <ta e="T382" id="Seg_8875" s="T381">conj</ta>
            <ta e="T383" id="Seg_8876" s="T382">refl-n:case.poss</ta>
            <ta e="T384" id="Seg_8877" s="T383">n-n:case.poss-n:case</ta>
            <ta e="T385" id="Seg_8878" s="T384">v-v:ins-v:mood.pn</ta>
            <ta e="T386" id="Seg_8879" s="T385">pers</ta>
            <ta e="T387" id="Seg_8880" s="T386">adv</ta>
            <ta e="T388" id="Seg_8881" s="T387">v-v:tense-v:pn</ta>
            <ta e="T389" id="Seg_8882" s="T388">ptcl</ta>
            <ta e="T390" id="Seg_8883" s="T389">adv</ta>
            <ta e="T391" id="Seg_8884" s="T390">v-v:tense-v:pn</ta>
            <ta e="T392" id="Seg_8885" s="T391">ptcl</ta>
            <ta e="T393" id="Seg_8886" s="T392">pers</ta>
            <ta e="T394" id="Seg_8887" s="T393">ptcl</ta>
            <ta e="T395" id="Seg_8888" s="T394">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T396" id="Seg_8889" s="T395">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T397" id="Seg_8890" s="T396">conj</ta>
            <ta e="T398" id="Seg_8891" s="T397">pers</ta>
            <ta e="T399" id="Seg_8892" s="T398">v-v:tense-v:pn</ta>
            <ta e="T400" id="Seg_8893" s="T399">dempro-n:case</ta>
            <ta e="T401" id="Seg_8894" s="T400">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T402" id="Seg_8895" s="T401">num-n:case</ta>
            <ta e="T403" id="Seg_8896" s="T402">n-n:case.poss</ta>
            <ta e="T405" id="Seg_8897" s="T404">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T406" id="Seg_8898" s="T405">num-n:case</ta>
            <ta e="T407" id="Seg_8899" s="T406">dempro-n:case</ta>
            <ta e="T408" id="Seg_8900" s="T407">v-v:tense-v:pn</ta>
            <ta e="T409" id="Seg_8901" s="T408">conj</ta>
            <ta e="T410" id="Seg_8902" s="T409">num-n:case</ta>
            <ta e="T411" id="Seg_8903" s="T410">adv</ta>
            <ta e="T412" id="Seg_8904" s="T411">v-v:tense-v:pn</ta>
            <ta e="T413" id="Seg_8905" s="T412">adv</ta>
            <ta e="T415" id="Seg_8906" s="T413">ptcl</ta>
            <ta e="T420" id="Seg_8907" s="T419">adv</ta>
            <ta e="T421" id="Seg_8908" s="T420">adj</ta>
            <ta e="T422" id="Seg_8909" s="T421">n-n:case.poss</ta>
            <ta e="T423" id="Seg_8910" s="T422">n-n:case</ta>
            <ta e="T424" id="Seg_8911" s="T423">v-v:tense-v:pn</ta>
            <ta e="T425" id="Seg_8912" s="T424">conj</ta>
            <ta e="T426" id="Seg_8913" s="T425">adv</ta>
            <ta e="T427" id="Seg_8914" s="T426">v</ta>
            <ta e="T428" id="Seg_8915" s="T427">v-v&gt;v-v:pn</ta>
            <ta e="T429" id="Seg_8916" s="T428">propr-n:case</ta>
            <ta e="T430" id="Seg_8917" s="T429">v-v:tense-v:pn</ta>
            <ta e="T431" id="Seg_8918" s="T430">n-n:case.poss</ta>
            <ta e="T432" id="Seg_8919" s="T431">conj</ta>
            <ta e="T433" id="Seg_8920" s="T432">propr-n:case</ta>
            <ta e="T434" id="Seg_8921" s="T433">n-n:case</ta>
            <ta e="T435" id="Seg_8922" s="T434">v-v&gt;v-v:pn</ta>
            <ta e="T436" id="Seg_8923" s="T435">conj</ta>
            <ta e="T437" id="Seg_8924" s="T436">n-n:case</ta>
            <ta e="T438" id="Seg_8925" s="T437">v-v:tense-v:pn</ta>
            <ta e="T439" id="Seg_8926" s="T438">v-v:tense-v:pn</ta>
            <ta e="T441" id="Seg_8927" s="T440">n-n:case</ta>
            <ta e="T442" id="Seg_8928" s="T441">v-v:tense-v:pn</ta>
            <ta e="T443" id="Seg_8929" s="T442">ptcl</ta>
            <ta e="T444" id="Seg_8930" s="T443">n-n:case.poss</ta>
            <ta e="T445" id="Seg_8931" s="T444">v-v:n.fin</ta>
            <ta e="T446" id="Seg_8932" s="T445">adj-n:case</ta>
            <ta e="T447" id="Seg_8933" s="T446">n-n:case</ta>
            <ta e="T448" id="Seg_8934" s="T447">v-v:n.fin</ta>
            <ta e="T451" id="Seg_8935" s="T450">ptcl</ta>
            <ta e="T452" id="Seg_8936" s="T451">adv</ta>
            <ta e="T453" id="Seg_8937" s="T452">v-v:tense-v:pn</ta>
            <ta e="T454" id="Seg_8938" s="T453">ptcl</ta>
            <ta e="T455" id="Seg_8939" s="T454">n-n:case.poss</ta>
            <ta e="T456" id="Seg_8940" s="T455">v-v:n.fin</ta>
            <ta e="T457" id="Seg_8941" s="T456">n.[n:case]</ta>
            <ta e="T458" id="Seg_8942" s="T457">v-v:n.fin</ta>
            <ta e="T459" id="Seg_8943" s="T458">ptcl</ta>
            <ta e="T460" id="Seg_8944" s="T459">n.[n:case]</ta>
            <ta e="T461" id="Seg_8945" s="T460">v-v:pn</ta>
            <ta e="T463" id="Seg_8946" s="T462">pers</ta>
            <ta e="T464" id="Seg_8947" s="T463">adj-n:case</ta>
            <ta e="T465" id="Seg_8948" s="T464">v-v:tense-v:pn</ta>
            <ta e="T466" id="Seg_8949" s="T465">conj</ta>
            <ta e="T467" id="Seg_8950" s="T466">n-n:num-n:case.poss</ta>
            <ta e="T468" id="Seg_8951" s="T467">adj-n:case</ta>
            <ta e="T469" id="Seg_8952" s="T468">v-v:tense-v:pn</ta>
            <ta e="T470" id="Seg_8953" s="T469">adv</ta>
            <ta e="T471" id="Seg_8954" s="T470">pers</ta>
            <ta e="T472" id="Seg_8955" s="T471">v-v:tense-v:pn</ta>
            <ta e="T473" id="Seg_8956" s="T472">refl-n:case.poss</ta>
            <ta e="T474" id="Seg_8957" s="T473">n-n:case</ta>
            <ta e="T475" id="Seg_8958" s="T474">v-v:n.fin</ta>
            <ta e="T476" id="Seg_8959" s="T475">pers</ta>
            <ta e="T480" id="Seg_8960" s="T479">adj-n:case</ta>
            <ta e="T481" id="Seg_8961" s="T480">v-v:tense-v:pn</ta>
            <ta e="T482" id="Seg_8962" s="T481">v-v:tense-v:pn</ta>
            <ta e="T483" id="Seg_8963" s="T482">ptcl</ta>
            <ta e="T484" id="Seg_8964" s="T483">dempro-n:case</ta>
            <ta e="T485" id="Seg_8965" s="T484">n-n:case</ta>
            <ta e="T486" id="Seg_8966" s="T485">adv</ta>
            <ta e="T487" id="Seg_8967" s="T486">adj-n:case</ta>
            <ta e="T488" id="Seg_8968" s="T487">v-v:tense-v:pn</ta>
            <ta e="T492" id="Seg_8969" s="T491">adv</ta>
            <ta e="T493" id="Seg_8970" s="T492">adj-n:case</ta>
            <ta e="T501" id="Seg_8971" s="T500">adv</ta>
            <ta e="T502" id="Seg_8972" s="T501">adv</ta>
            <ta e="T503" id="Seg_8973" s="T502">v-v&gt;v-v:pn</ta>
            <ta e="T504" id="Seg_8974" s="T503">dempro-n:case</ta>
            <ta e="T505" id="Seg_8975" s="T504">n-n:case</ta>
            <ta e="T506" id="Seg_8976" s="T505">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T507" id="Seg_8977" s="T506">ptcl</ta>
            <ta e="T508" id="Seg_8978" s="T507">n-n:case.poss</ta>
            <ta e="T509" id="Seg_8979" s="T508">v-v:n.fin</ta>
            <ta e="T510" id="Seg_8980" s="T509">n-n:case</ta>
            <ta e="T511" id="Seg_8981" s="T510">n-n:case</ta>
            <ta e="T512" id="Seg_8982" s="T511">n-n:case</ta>
            <ta e="T513" id="Seg_8983" s="T512">conj</ta>
            <ta e="T514" id="Seg_8984" s="T513">adv</ta>
            <ta e="T515" id="Seg_8985" s="T514">dempro-n:case</ta>
            <ta e="T516" id="Seg_8986" s="T515">v-v:n.fin</ta>
            <ta e="T517" id="Seg_8987" s="T516">conj</ta>
            <ta e="T518" id="Seg_8988" s="T517">n-n:case</ta>
            <ta e="T519" id="Seg_8989" s="T518">v-v:n.fin</ta>
            <ta e="T520" id="Seg_8990" s="T519">adv</ta>
            <ta e="T521" id="Seg_8991" s="T520">adv</ta>
            <ta e="T522" id="Seg_8992" s="T521">v-v:tense-v:pn</ta>
            <ta e="T523" id="Seg_8993" s="T522">dempro-n:case</ta>
            <ta e="T524" id="Seg_8994" s="T523">n-n:case</ta>
            <ta e="T525" id="Seg_8995" s="T524">adv</ta>
            <ta e="T526" id="Seg_8996" s="T525">adj-n:case</ta>
            <ta e="T527" id="Seg_8997" s="T526">n-n:num</ta>
            <ta e="T528" id="Seg_8998" s="T527">v-v:tense-v:pn</ta>
            <ta e="T529" id="Seg_8999" s="T528">dempro-n:num</ta>
            <ta e="T530" id="Seg_9000" s="T529">dempro-n:num</ta>
            <ta e="T533" id="Seg_9001" s="T532">n-n:num</ta>
            <ta e="T535" id="Seg_9002" s="T534">v-v:tense-v:pn</ta>
            <ta e="T536" id="Seg_9003" s="T535">n</ta>
            <ta e="T537" id="Seg_9004" s="T536">n-n:ins-n:case</ta>
            <ta e="T538" id="Seg_9005" s="T537">n-n:case.poss</ta>
            <ta e="T539" id="Seg_9006" s="T538">ptcl</ta>
            <ta e="T540" id="Seg_9007" s="T539">n-n:num</ta>
            <ta e="T542" id="Seg_9008" s="T541">v-v:tense-v:pn</ta>
            <ta e="T543" id="Seg_9009" s="T542">n-n:num</ta>
            <ta e="T544" id="Seg_9010" s="T543">v-v:tense-v:pn</ta>
            <ta e="T546" id="Seg_9011" s="T545">v-v:tense-v:pn</ta>
            <ta e="T547" id="Seg_9012" s="T546">adv</ta>
            <ta e="T548" id="Seg_9013" s="T547">adj-n:case</ta>
            <ta e="T549" id="Seg_9014" s="T548">n-n:case</ta>
            <ta e="T552" id="Seg_9015" s="T551">n</ta>
            <ta e="T553" id="Seg_9016" s="T552">n-n:case</ta>
            <ta e="T554" id="Seg_9017" s="T553">ptcl</ta>
            <ta e="T555" id="Seg_9018" s="T554">v-v:tense-v:pn</ta>
            <ta e="T867" id="Seg_9019" s="T555">adj</ta>
            <ta e="T557" id="Seg_9020" s="T556">n-n:case</ta>
            <ta e="T558" id="Seg_9021" s="T557">adj-n:case</ta>
            <ta e="T559" id="Seg_9022" s="T558">conj</ta>
            <ta e="T560" id="Seg_9023" s="T559">adj</ta>
            <ta e="T561" id="Seg_9024" s="T560">n-n:case</ta>
            <ta e="T563" id="Seg_9025" s="T561">adj-n:case</ta>
            <ta e="T567" id="Seg_9026" s="T566">adv</ta>
            <ta e="T568" id="Seg_9027" s="T567">adj-n:case</ta>
            <ta e="T570" id="Seg_9028" s="T569">n-n:num</ta>
            <ta e="T571" id="Seg_9029" s="T570">v-v:tense-v:pn</ta>
            <ta e="T572" id="Seg_9030" s="T571">pers</ta>
            <ta e="T573" id="Seg_9031" s="T572">n-n:case.poss</ta>
            <ta e="T575" id="Seg_9032" s="T574">v-v:tense-v:pn</ta>
            <ta e="T576" id="Seg_9033" s="T575">dempro-n:case</ta>
            <ta e="T577" id="Seg_9034" s="T576">ptcl</ta>
            <ta e="T578" id="Seg_9035" s="T577">ptcl</ta>
            <ta e="T579" id="Seg_9036" s="T578">adj-n:case</ta>
            <ta e="T580" id="Seg_9037" s="T579">n-n:case</ta>
            <ta e="T581" id="Seg_9038" s="T580">ptcl</ta>
            <ta e="T582" id="Seg_9039" s="T581">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T583" id="Seg_9040" s="T582">adv</ta>
            <ta e="T584" id="Seg_9041" s="T583">ptcl</ta>
            <ta e="T585" id="Seg_9042" s="T584">v-v:tense-v:pn</ta>
            <ta e="T586" id="Seg_9043" s="T585">v-v:n.fin</ta>
            <ta e="T587" id="Seg_9044" s="T586">ptcl</ta>
            <ta e="T588" id="Seg_9045" s="T587">adj-n:case</ta>
            <ta e="T589" id="Seg_9046" s="T588">ptcl</ta>
            <ta e="T590" id="Seg_9047" s="T589">adj-n:case</ta>
            <ta e="T591" id="Seg_9048" s="T590">adv</ta>
            <ta e="T592" id="Seg_9049" s="T591">dempro-n:case</ta>
            <ta e="T593" id="Seg_9050" s="T592">n-n:case.poss</ta>
            <ta e="T595" id="Seg_9051" s="T594">n-n:case</ta>
            <ta e="T596" id="Seg_9052" s="T595">v-v:tense-v:pn</ta>
            <ta e="T597" id="Seg_9053" s="T596">n-n:case</ta>
            <ta e="T598" id="Seg_9054" s="T597">adv</ta>
            <ta e="T600" id="Seg_9055" s="T599">n-n:case</ta>
            <ta e="T601" id="Seg_9056" s="T600">v-v:tense-v:pn</ta>
            <ta e="T602" id="Seg_9057" s="T601">conj</ta>
            <ta e="T603" id="Seg_9058" s="T602">adv</ta>
            <ta e="T604" id="Seg_9059" s="T603">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T605" id="Seg_9060" s="T604">conj</ta>
            <ta e="T606" id="Seg_9061" s="T605">v-v&gt;v-v:pn</ta>
            <ta e="T607" id="Seg_9062" s="T606">n-n:case.poss</ta>
            <ta e="T608" id="Seg_9063" s="T607">n-n:ins-n:case</ta>
            <ta e="T609" id="Seg_9064" s="T608">n-n:case</ta>
            <ta e="T610" id="Seg_9065" s="T609">v-v:n.fin</ta>
            <ta e="T613" id="Seg_9066" s="T612">adv</ta>
            <ta e="T614" id="Seg_9067" s="T613">n-n:case</ta>
            <ta e="T615" id="Seg_9068" s="T614">ptcl</ta>
            <ta e="T616" id="Seg_9069" s="T615">v-v&gt;v-v:n.fin</ta>
            <ta e="T617" id="Seg_9070" s="T616">adv</ta>
            <ta e="T618" id="Seg_9071" s="T617">adv</ta>
            <ta e="T619" id="Seg_9072" s="T618">v-v:tense-v:pn</ta>
            <ta e="T620" id="Seg_9073" s="T868">ptcl</ta>
            <ta e="T621" id="Seg_9074" s="T620">n-n:num</ta>
            <ta e="T622" id="Seg_9075" s="T621">v-v:pn</ta>
            <ta e="T623" id="Seg_9076" s="T622">num-n:case</ta>
            <ta e="T624" id="Seg_9077" s="T623">conj</ta>
            <ta e="T625" id="Seg_9078" s="T624">n-n:case</ta>
            <ta e="T627" id="Seg_9079" s="T625">v-v:pn</ta>
            <ta e="T629" id="Seg_9080" s="T628">v-v:tense-v:pn</ta>
            <ta e="T631" id="Seg_9081" s="T630">v-v&gt;v-v:pn</ta>
            <ta e="T632" id="Seg_9082" s="T631">n-n:case</ta>
            <ta e="T633" id="Seg_9083" s="T632">v-v:tense-v:pn</ta>
            <ta e="T634" id="Seg_9084" s="T633">n-n:case</ta>
            <ta e="T636" id="Seg_9085" s="T635">v-v&gt;v-v:pn</ta>
            <ta e="T637" id="Seg_9086" s="T636">n-n:case</ta>
            <ta e="T638" id="Seg_9087" s="T637">v-v:tense-v:pn</ta>
            <ta e="T639" id="Seg_9088" s="T638">n-n:case</ta>
            <ta e="T640" id="Seg_9089" s="T639">v-v&gt;v-v:pn</ta>
            <ta e="T641" id="Seg_9090" s="T640">n-n:case</ta>
            <ta e="T642" id="Seg_9091" s="T641">v-v:tense-v:pn</ta>
            <ta e="T643" id="Seg_9092" s="T642">n-n:case</ta>
            <ta e="T644" id="Seg_9093" s="T643">v-v&gt;v-v:pn</ta>
            <ta e="T645" id="Seg_9094" s="T644">pers</ta>
            <ta e="T646" id="Seg_9095" s="T645">n-n:case.poss</ta>
            <ta e="T647" id="Seg_9096" s="T646">n-n:case</ta>
            <ta e="T648" id="Seg_9097" s="T647">v-v:tense-v:pn</ta>
            <ta e="T650" id="Seg_9098" s="T649">v-v:tense-v:pn</ta>
            <ta e="T651" id="Seg_9099" s="T650">n-n:case</ta>
            <ta e="T653" id="Seg_9100" s="T652">quant</ta>
            <ta e="T654" id="Seg_9101" s="T653">v-v:tense-v:pn</ta>
            <ta e="T655" id="Seg_9102" s="T654">pers</ta>
            <ta e="T656" id="Seg_9103" s="T655">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T657" id="Seg_9104" s="T656">conj</ta>
            <ta e="T658" id="Seg_9105" s="T657">v-v:tense-v:pn</ta>
            <ta e="T659" id="Seg_9106" s="T658">conj</ta>
            <ta e="T660" id="Seg_9107" s="T659">v-v:tense-v:pn</ta>
            <ta e="T661" id="Seg_9108" s="T660">conj</ta>
            <ta e="T662" id="Seg_9109" s="T661">v-v:tense-v:pn</ta>
            <ta e="T663" id="Seg_9110" s="T662">pers</ta>
            <ta e="T664" id="Seg_9111" s="T663">v-any</ta>
            <ta e="T665" id="Seg_9112" s="T664">conj</ta>
            <ta e="T666" id="Seg_9113" s="T665">adv</ta>
            <ta e="T667" id="Seg_9114" s="T666">adj-n:case</ta>
            <ta e="T668" id="Seg_9115" s="T667">n-n:case</ta>
            <ta e="T669" id="Seg_9116" s="T668">adv</ta>
            <ta e="T670" id="Seg_9117" s="T669">ptcl</ta>
            <ta e="T671" id="Seg_9118" s="T670">n-n:case.poss</ta>
            <ta e="T672" id="Seg_9119" s="T671">v-v:tense-any</ta>
            <ta e="T673" id="Seg_9120" s="T672">n-n:case</ta>
            <ta e="T674" id="Seg_9121" s="T673">v-v:tense-v:pn</ta>
            <ta e="T675" id="Seg_9122" s="T674">n-n:case</ta>
            <ta e="T676" id="Seg_9123" s="T675">ptcl</ta>
            <ta e="T677" id="Seg_9124" s="T676">n-n:case</ta>
            <ta e="T678" id="Seg_9125" s="T677">v-v:tense-v:pn</ta>
            <ta e="T679" id="Seg_9126" s="T678">conj</ta>
            <ta e="T680" id="Seg_9127" s="T679">refl-n:case.poss</ta>
            <ta e="T681" id="Seg_9128" s="T680">n-n:case</ta>
            <ta e="T682" id="Seg_9129" s="T681">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T684" id="Seg_9130" s="T683">n-n:case</ta>
            <ta e="T685" id="Seg_9131" s="T684">adv</ta>
            <ta e="T686" id="Seg_9132" s="T685">n-n:case.poss</ta>
            <ta e="T687" id="Seg_9133" s="T686">quant</ta>
            <ta e="T688" id="Seg_9134" s="T687">ptcl</ta>
            <ta e="T689" id="Seg_9135" s="T688">n-n:case</ta>
            <ta e="T690" id="Seg_9136" s="T689">ptcl</ta>
            <ta e="T691" id="Seg_9137" s="T690">v-v:tense-v:pn</ta>
            <ta e="T692" id="Seg_9138" s="T691">adv</ta>
            <ta e="T693" id="Seg_9139" s="T692">dempro-n:case</ta>
            <ta e="T694" id="Seg_9140" s="T693">adj-n:case</ta>
            <ta e="T695" id="Seg_9141" s="T694">adv</ta>
            <ta e="T696" id="Seg_9142" s="T695">adj-n:case</ta>
            <ta e="T697" id="Seg_9143" s="T696">n-n:case.poss</ta>
            <ta e="T698" id="Seg_9144" s="T697">adv</ta>
            <ta e="T699" id="Seg_9145" s="T698">adj-n:case</ta>
            <ta e="T700" id="Seg_9146" s="T699">adj-n:case</ta>
            <ta e="T701" id="Seg_9147" s="T700">n-n:num</ta>
            <ta e="T702" id="Seg_9148" s="T701">adv</ta>
            <ta e="T703" id="Seg_9149" s="T702">adj-n:case</ta>
            <ta e="T704" id="Seg_9150" s="T703">adj-n:case</ta>
            <ta e="T705" id="Seg_9151" s="T704">n-n:case</ta>
            <ta e="T706" id="Seg_9152" s="T705">n-n:case</ta>
            <ta e="T707" id="Seg_9153" s="T706">adj-n:case</ta>
            <ta e="T708" id="Seg_9154" s="T707">n-n:case</ta>
            <ta e="T709" id="Seg_9155" s="T708">n-n:case.poss</ta>
            <ta e="T710" id="Seg_9156" s="T709">adj-n:case</ta>
            <ta e="T711" id="Seg_9157" s="T710">n-n:num</ta>
            <ta e="T712" id="Seg_9158" s="T711">adj-n:case</ta>
            <ta e="T713" id="Seg_9159" s="T712">pers</ta>
            <ta e="T714" id="Seg_9160" s="T713">n-n:case.poss</ta>
            <ta e="T715" id="Seg_9161" s="T714">v-v:tense-v:pn</ta>
            <ta e="T716" id="Seg_9162" s="T715">n-n:case</ta>
            <ta e="T717" id="Seg_9163" s="T716">v-v:tense-v:pn</ta>
            <ta e="T718" id="Seg_9164" s="T717">n-n:case</ta>
            <ta e="T719" id="Seg_9165" s="T718">n-n:case.poss</ta>
            <ta e="T720" id="Seg_9166" s="T719">v-v:tense-v:pn</ta>
            <ta e="T721" id="Seg_9167" s="T720">n-n:case</ta>
            <ta e="T723" id="Seg_9168" s="T721">n-n:num</ta>
            <ta e="T724" id="Seg_9169" s="T723">pers</ta>
            <ta e="T725" id="Seg_9170" s="T724">n-n:case.poss</ta>
            <ta e="T726" id="Seg_9171" s="T725">v-v:tense-v:pn</ta>
            <ta e="T727" id="Seg_9172" s="T726">n-n:case.poss</ta>
            <ta e="T728" id="Seg_9173" s="T727">v-v:tense-v:pn</ta>
            <ta e="T729" id="Seg_9174" s="T728">n-n:case</ta>
            <ta e="T730" id="Seg_9175" s="T729">n-n:case.poss</ta>
            <ta e="T731" id="Seg_9176" s="T730">v-v:tense-v:pn</ta>
            <ta e="T732" id="Seg_9177" s="T731">n-n:case</ta>
            <ta e="T733" id="Seg_9178" s="T732">n-n:case.poss</ta>
            <ta e="T734" id="Seg_9179" s="T733">n-n:case</ta>
            <ta e="T735" id="Seg_9180" s="T734">v-v:tense-v:pn</ta>
            <ta e="T736" id="Seg_9181" s="T735">n-n:num</ta>
            <ta e="T737" id="Seg_9182" s="T736">n-n:num-n:case</ta>
            <ta e="T738" id="Seg_9183" s="T737">dempro-n:case</ta>
            <ta e="T739" id="Seg_9184" s="T738">v-v:tense-v:pn</ta>
            <ta e="T740" id="Seg_9185" s="T739">ptcl</ta>
            <ta e="T741" id="Seg_9186" s="T740">dempro-n:num-n:case</ta>
            <ta e="T743" id="Seg_9187" s="T742">v-v:n.fin</ta>
            <ta e="T744" id="Seg_9188" s="T743">dempro-n:num</ta>
            <ta e="T746" id="Seg_9189" s="T745">v-v:n.fin</ta>
            <ta e="T747" id="Seg_9190" s="T746">v-v:n.fin</ta>
            <ta e="T748" id="Seg_9191" s="T747">pers</ta>
            <ta e="T749" id="Seg_9192" s="T748">n-n:case.poss</ta>
            <ta e="T750" id="Seg_9193" s="T749">ptcl</ta>
            <ta e="T751" id="Seg_9194" s="T750">adv</ta>
            <ta e="T752" id="Seg_9195" s="T751">v-v:tense-v:pn</ta>
            <ta e="T753" id="Seg_9196" s="T752">pers</ta>
            <ta e="T754" id="Seg_9197" s="T753">dempro-n:case</ta>
            <ta e="T755" id="Seg_9198" s="T754">v-v:tense-v:pn</ta>
            <ta e="T756" id="Seg_9199" s="T755">n-n:case</ta>
            <ta e="T757" id="Seg_9200" s="T756">v-v:mood.pn</ta>
            <ta e="T758" id="Seg_9201" s="T757">v-v:mood.pn</ta>
            <ta e="T759" id="Seg_9202" s="T758">adv</ta>
            <ta e="T760" id="Seg_9203" s="T759">n-n:case.poss</ta>
            <ta e="T761" id="Seg_9204" s="T760">v-v:mood.pn</ta>
            <ta e="T762" id="Seg_9205" s="T761">refl-n:case.poss</ta>
            <ta e="T763" id="Seg_9206" s="T762">dempro-n:case</ta>
            <ta e="T764" id="Seg_9207" s="T763">ptcl</ta>
            <ta e="T765" id="Seg_9208" s="T764">v-v:tense-v:pn</ta>
            <ta e="T766" id="Seg_9209" s="T765">v-v:mood.pn</ta>
            <ta e="T767" id="Seg_9210" s="T766">pers</ta>
            <ta e="T768" id="Seg_9211" s="T767">n-n:case.poss</ta>
            <ta e="T769" id="Seg_9212" s="T768">v-v:mood.pn</ta>
            <ta e="T770" id="Seg_9213" s="T769">v-v:mood.pn</ta>
            <ta e="T771" id="Seg_9214" s="T770">pers</ta>
            <ta e="T772" id="Seg_9215" s="T771">pers</ta>
            <ta e="T773" id="Seg_9216" s="T772">adv</ta>
            <ta e="T774" id="Seg_9217" s="T773">n-n:case</ta>
            <ta e="T775" id="Seg_9218" s="T774">v-v:tense-v:pn</ta>
            <ta e="T776" id="Seg_9219" s="T775">n-n:case</ta>
            <ta e="T777" id="Seg_9220" s="T776">v-v:tense-v:pn</ta>
            <ta e="T778" id="Seg_9221" s="T777">ptcl</ta>
            <ta e="T779" id="Seg_9222" s="T778">v-v:tense-v:pn</ta>
            <ta e="T780" id="Seg_9223" s="T779">adv</ta>
            <ta e="T782" id="Seg_9224" s="T781">v-v:tense-v:pn</ta>
            <ta e="T783" id="Seg_9225" s="T782">adv</ta>
            <ta e="T784" id="Seg_9226" s="T783">v-v:tense-v:pn</ta>
            <ta e="T785" id="Seg_9227" s="T784">n-n:case</ta>
            <ta e="T786" id="Seg_9228" s="T785">v-v:n.fin</ta>
            <ta e="T787" id="Seg_9229" s="T786">n-n:case</ta>
            <ta e="T789" id="Seg_9230" s="T787">num-n:case</ta>
            <ta e="T790" id="Seg_9231" s="T789">num-n:case</ta>
            <ta e="T791" id="Seg_9232" s="T790">n-n:case</ta>
            <ta e="T792" id="Seg_9233" s="T791">v-v:tense-v:pn</ta>
            <ta e="T793" id="Seg_9234" s="T792">pers</ta>
            <ta e="T794" id="Seg_9235" s="T793">adv</ta>
            <ta e="T795" id="Seg_9236" s="T794">adv-adv:case</ta>
            <ta e="T796" id="Seg_9237" s="T795">v-v:tense-v:pn</ta>
            <ta e="T799" id="Seg_9238" s="T798">n-n:case</ta>
            <ta e="T800" id="Seg_9239" s="T799">v-v:tense-v:pn</ta>
            <ta e="T801" id="Seg_9240" s="T800">ptcl</ta>
            <ta e="T802" id="Seg_9241" s="T801">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T804" id="Seg_9242" s="T803">n-n:case</ta>
            <ta e="T805" id="Seg_9243" s="T804">n-n:case</ta>
            <ta e="T806" id="Seg_9244" s="T805">v-v:tense-v:pn</ta>
            <ta e="T807" id="Seg_9245" s="T806">pers</ta>
            <ta e="T808" id="Seg_9246" s="T807">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T809" id="Seg_9247" s="T808">pers</ta>
            <ta e="T810" id="Seg_9248" s="T809">dempro-n:case</ta>
            <ta e="T811" id="Seg_9249" s="T810">v-v:n.fin</ta>
            <ta e="T813" id="Seg_9250" s="T812">ptcl</ta>
            <ta e="T814" id="Seg_9251" s="T813">n</ta>
            <ta e="T815" id="Seg_9252" s="T814">dempro-n:num</ta>
            <ta e="T816" id="Seg_9253" s="T815">ptcl</ta>
            <ta e="T817" id="Seg_9254" s="T816">adv</ta>
            <ta e="T819" id="Seg_9255" s="T818">v-v&gt;v-v:pn</ta>
            <ta e="T820" id="Seg_9256" s="T819">v-v&gt;v-v:pn</ta>
            <ta e="T821" id="Seg_9257" s="T820">dempro-n:num</ta>
            <ta e="T822" id="Seg_9258" s="T821">adv</ta>
            <ta e="T824" id="Seg_9259" s="T823">v-v:n.fin</ta>
            <ta e="T825" id="Seg_9260" s="T824">ptcl</ta>
            <ta e="T826" id="Seg_9261" s="T825">n</ta>
            <ta e="T827" id="Seg_9262" s="T826">ptcl</ta>
            <ta e="T828" id="Seg_9263" s="T827">pers</ta>
            <ta e="T829" id="Seg_9264" s="T828">n-n:case.poss</ta>
            <ta e="T830" id="Seg_9265" s="T829">ptcl</ta>
            <ta e="T831" id="Seg_9266" s="T830">v-v:tense-v:pn</ta>
            <ta e="T832" id="Seg_9267" s="T831">pers</ta>
            <ta e="T833" id="Seg_9268" s="T832">dempro-n:case</ta>
            <ta e="T834" id="Seg_9269" s="T833">ptcl</ta>
            <ta e="T835" id="Seg_9270" s="T834">adv</ta>
            <ta e="T836" id="Seg_9271" s="T835">n-n:case.poss</ta>
            <ta e="T837" id="Seg_9272" s="T836">n-n:case.poss</ta>
            <ta e="T838" id="Seg_9273" s="T837">n-n:num-n:case.poss</ta>
            <ta e="T839" id="Seg_9274" s="T838">n-n:case.poss</ta>
            <ta e="T840" id="Seg_9275" s="T839">n-n:num-n:case.poss</ta>
            <ta e="T841" id="Seg_9276" s="T840">n-n:case.poss</ta>
            <ta e="T842" id="Seg_9277" s="T841">n-n:case.poss</ta>
            <ta e="T843" id="Seg_9278" s="T842">n-n:case.poss</ta>
            <ta e="T844" id="Seg_9279" s="T843">n-n:num-n:case.poss</ta>
            <ta e="T845" id="Seg_9280" s="T844">n-n:case.poss</ta>
            <ta e="T847" id="Seg_9281" s="T846">n-n:case.poss</ta>
            <ta e="T848" id="Seg_9282" s="T847">n-n:case.poss</ta>
            <ta e="T849" id="Seg_9283" s="T848">n-n:case.poss</ta>
            <ta e="T851" id="Seg_9284" s="T850">n-n:case.poss</ta>
            <ta e="T852" id="Seg_9285" s="T851">n-n:case.poss</ta>
            <ta e="T853" id="Seg_9286" s="T852">n-n:case.poss</ta>
            <ta e="T854" id="Seg_9287" s="T853">n-n:case.poss</ta>
            <ta e="T855" id="Seg_9288" s="T854">n-n:case.poss</ta>
            <ta e="T856" id="Seg_9289" s="T855">n-n:case.poss</ta>
            <ta e="T858" id="Seg_9290" s="T857">n-n:num-n:case.poss</ta>
            <ta e="T861" id="Seg_9291" s="T860">n-n:num-n:case.poss</ta>
            <ta e="T863" id="Seg_9292" s="T862">n-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps-PKZ">
            <ta e="T5" id="Seg_9293" s="T4">adv</ta>
            <ta e="T6" id="Seg_9294" s="T5">v</ta>
            <ta e="T7" id="Seg_9295" s="T6">ptcl</ta>
            <ta e="T8" id="Seg_9296" s="T7">pers</ta>
            <ta e="T9" id="Seg_9297" s="T8">n</ta>
            <ta e="T10" id="Seg_9298" s="T9">v</ta>
            <ta e="T11" id="Seg_9299" s="T10">pers</ta>
            <ta e="T12" id="Seg_9300" s="T11">n</ta>
            <ta e="T13" id="Seg_9301" s="T12">que</ta>
            <ta e="T14" id="Seg_9302" s="T13">v</ta>
            <ta e="T15" id="Seg_9303" s="T14">propr</ta>
            <ta e="T869" id="Seg_9304" s="T16">v</ta>
            <ta e="T17" id="Seg_9305" s="T869">v</ta>
            <ta e="T18" id="Seg_9306" s="T17">n</ta>
            <ta e="T19" id="Seg_9307" s="T18">adv</ta>
            <ta e="T20" id="Seg_9308" s="T19">v</ta>
            <ta e="T21" id="Seg_9309" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_9310" s="T21">num</ta>
            <ta e="T870" id="Seg_9311" s="T22">v</ta>
            <ta e="T23" id="Seg_9312" s="T870">v</ta>
            <ta e="T24" id="Seg_9313" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_9314" s="T24">conj</ta>
            <ta e="T27" id="Seg_9315" s="T26">v</ta>
            <ta e="T29" id="Seg_9316" s="T28">adv</ta>
            <ta e="T30" id="Seg_9317" s="T29">v</ta>
            <ta e="T31" id="Seg_9318" s="T30">adv</ta>
            <ta e="T32" id="Seg_9319" s="T31">adv</ta>
            <ta e="T33" id="Seg_9320" s="T32">que</ta>
            <ta e="T34" id="Seg_9321" s="T33">v</ta>
            <ta e="T35" id="Seg_9322" s="T34">ptcl</ta>
            <ta e="T38" id="Seg_9323" s="T37">ptcl</ta>
            <ta e="T40" id="Seg_9324" s="T39">v</ta>
            <ta e="T41" id="Seg_9325" s="T40">n</ta>
            <ta e="T43" id="Seg_9326" s="T42">v</ta>
            <ta e="T44" id="Seg_9327" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_9328" s="T44">n</ta>
            <ta e="T46" id="Seg_9329" s="T45">v</ta>
            <ta e="T47" id="Seg_9330" s="T46">n</ta>
            <ta e="T48" id="Seg_9331" s="T47">v</ta>
            <ta e="T49" id="Seg_9332" s="T48">pers</ta>
            <ta e="T50" id="Seg_9333" s="T49">adv</ta>
            <ta e="T51" id="Seg_9334" s="T50">dempro</ta>
            <ta e="T53" id="Seg_9335" s="T52">v</ta>
            <ta e="T54" id="Seg_9336" s="T53">n</ta>
            <ta e="T55" id="Seg_9337" s="T54">n</ta>
            <ta e="T56" id="Seg_9338" s="T55">n</ta>
            <ta e="T57" id="Seg_9339" s="T56">n</ta>
            <ta e="T58" id="Seg_9340" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_9341" s="T58">v</ta>
            <ta e="T60" id="Seg_9342" s="T59">n</ta>
            <ta e="T61" id="Seg_9343" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_9344" s="T61">v</ta>
            <ta e="T63" id="Seg_9345" s="T62">n</ta>
            <ta e="T65" id="Seg_9346" s="T64">v</ta>
            <ta e="T66" id="Seg_9347" s="T65">adv</ta>
            <ta e="T67" id="Seg_9348" s="T66">v</ta>
            <ta e="T68" id="Seg_9349" s="T67">dempro</ta>
            <ta e="T69" id="Seg_9350" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_9351" s="T69">v</ta>
            <ta e="T71" id="Seg_9352" s="T70">n</ta>
            <ta e="T72" id="Seg_9353" s="T71">n</ta>
            <ta e="T73" id="Seg_9354" s="T72">n</ta>
            <ta e="T74" id="Seg_9355" s="T73">v</ta>
            <ta e="T75" id="Seg_9356" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_9357" s="T75">que</ta>
            <ta e="T77" id="Seg_9358" s="T76">ptcl</ta>
            <ta e="T78" id="Seg_9359" s="T77">v</ta>
            <ta e="T79" id="Seg_9360" s="T78">que</ta>
            <ta e="T80" id="Seg_9361" s="T79">ptcl</ta>
            <ta e="T81" id="Seg_9362" s="T80">v</ta>
            <ta e="T82" id="Seg_9363" s="T81">v</ta>
            <ta e="T83" id="Seg_9364" s="T82">ptcl</ta>
            <ta e="T84" id="Seg_9365" s="T83">n</ta>
            <ta e="T85" id="Seg_9366" s="T84">v</ta>
            <ta e="T86" id="Seg_9367" s="T85">n</ta>
            <ta e="T87" id="Seg_9368" s="T86">v</ta>
            <ta e="T88" id="Seg_9369" s="T87">ptcl</ta>
            <ta e="T92" id="Seg_9370" s="T91">v</ta>
            <ta e="T93" id="Seg_9371" s="T92">n</ta>
            <ta e="T94" id="Seg_9372" s="T93">v</ta>
            <ta e="T95" id="Seg_9373" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_9374" s="T95">v</ta>
            <ta e="T97" id="Seg_9375" s="T96">adv</ta>
            <ta e="T98" id="Seg_9376" s="T97">v</ta>
            <ta e="T99" id="Seg_9377" s="T98">ptcl</ta>
            <ta e="T100" id="Seg_9378" s="T99">adv</ta>
            <ta e="T101" id="Seg_9379" s="T100">v</ta>
            <ta e="T102" id="Seg_9380" s="T101">adv</ta>
            <ta e="T103" id="Seg_9381" s="T102">v</ta>
            <ta e="T104" id="Seg_9382" s="T103">que</ta>
            <ta e="T106" id="Seg_9383" s="T104">ptcl</ta>
            <ta e="T107" id="Seg_9384" s="T106">que</ta>
            <ta e="T108" id="Seg_9385" s="T107">ptcl</ta>
            <ta e="T109" id="Seg_9386" s="T108">v</ta>
            <ta e="T110" id="Seg_9387" s="T109">dempro</ta>
            <ta e="T111" id="Seg_9388" s="T110">v</ta>
            <ta e="T112" id="Seg_9389" s="T111">conj</ta>
            <ta e="T113" id="Seg_9390" s="T112">pers</ta>
            <ta e="T114" id="Seg_9391" s="T113">dempro</ta>
            <ta e="T116" id="Seg_9392" s="T115">v</ta>
            <ta e="T117" id="Seg_9393" s="T116">aux</ta>
            <ta e="T118" id="Seg_9394" s="T117">v</ta>
            <ta e="T119" id="Seg_9395" s="T118">v</ta>
            <ta e="T120" id="Seg_9396" s="T119">adv</ta>
            <ta e="T121" id="Seg_9397" s="T120">pers</ta>
            <ta e="T122" id="Seg_9398" s="T121">pers</ta>
            <ta e="T123" id="Seg_9399" s="T122">v</ta>
            <ta e="T124" id="Seg_9400" s="T123">v</ta>
            <ta e="T125" id="Seg_9401" s="T124">adv</ta>
            <ta e="T126" id="Seg_9402" s="T125">pers</ta>
            <ta e="T127" id="Seg_9403" s="T126">adv</ta>
            <ta e="T128" id="Seg_9404" s="T127">adj</ta>
            <ta e="T129" id="Seg_9405" s="T128">n</ta>
            <ta e="T130" id="Seg_9406" s="T129">v</ta>
            <ta e="T131" id="Seg_9407" s="T130">n</ta>
            <ta e="T132" id="Seg_9408" s="T131">quant</ta>
            <ta e="T134" id="Seg_9409" s="T133">adv</ta>
            <ta e="T135" id="Seg_9410" s="T134">v</ta>
            <ta e="T136" id="Seg_9411" s="T135">n</ta>
            <ta e="T137" id="Seg_9412" s="T136">dempro</ta>
            <ta e="T138" id="Seg_9413" s="T137">n</ta>
            <ta e="T139" id="Seg_9414" s="T138">ptcl</ta>
            <ta e="T140" id="Seg_9415" s="T139">v</ta>
            <ta e="T142" id="Seg_9416" s="T140">v</ta>
            <ta e="T143" id="Seg_9417" s="T142">dempro</ta>
            <ta e="T144" id="Seg_9418" s="T143">v</ta>
            <ta e="T145" id="Seg_9419" s="T144">dempro</ta>
            <ta e="T146" id="Seg_9420" s="T145">n</ta>
            <ta e="T148" id="Seg_9421" s="T146">refl</ta>
            <ta e="T149" id="Seg_9422" s="T148">quant</ta>
            <ta e="T150" id="Seg_9423" s="T149">v</ta>
            <ta e="T151" id="Seg_9424" s="T150">conj</ta>
            <ta e="T152" id="Seg_9425" s="T151">dempro</ta>
            <ta e="T153" id="Seg_9426" s="T152">adv</ta>
            <ta e="T154" id="Seg_9427" s="T153">v</ta>
            <ta e="T155" id="Seg_9428" s="T154">refl</ta>
            <ta e="T156" id="Seg_9429" s="T155">quant</ta>
            <ta e="T157" id="Seg_9430" s="T156">v</ta>
            <ta e="T158" id="Seg_9431" s="T157">conj</ta>
            <ta e="T159" id="Seg_9432" s="T158">dempro</ta>
            <ta e="T160" id="Seg_9433" s="T159">adv</ta>
            <ta e="T161" id="Seg_9434" s="T160">v</ta>
            <ta e="T162" id="Seg_9435" s="T161">num</ta>
            <ta e="T163" id="Seg_9436" s="T162">ptcl</ta>
            <ta e="T164" id="Seg_9437" s="T163">v</ta>
            <ta e="T165" id="Seg_9438" s="T164">pers</ta>
            <ta e="T166" id="Seg_9439" s="T165">ptcl</ta>
            <ta e="T167" id="Seg_9440" s="T166">adj</ta>
            <ta e="T168" id="Seg_9441" s="T167">v</ta>
            <ta e="T169" id="Seg_9442" s="T168">ptcl</ta>
            <ta e="T170" id="Seg_9443" s="T169">v</ta>
            <ta e="T171" id="Seg_9444" s="T170">n</ta>
            <ta e="T172" id="Seg_9445" s="T171">v</ta>
            <ta e="T173" id="Seg_9446" s="T172">conj</ta>
            <ta e="T174" id="Seg_9447" s="T173">ptcl</ta>
            <ta e="T175" id="Seg_9448" s="T174">interj</ta>
            <ta e="T176" id="Seg_9449" s="T175">num</ta>
            <ta e="T177" id="Seg_9450" s="T176">v</ta>
            <ta e="T178" id="Seg_9451" s="T177">conj</ta>
            <ta e="T179" id="Seg_9452" s="T178">pers</ta>
            <ta e="T180" id="Seg_9453" s="T179">ptcl</ta>
            <ta e="T181" id="Seg_9454" s="T180">v</ta>
            <ta e="T182" id="Seg_9455" s="T181">adv</ta>
            <ta e="T183" id="Seg_9456" s="T182">dempro</ta>
            <ta e="T184" id="Seg_9457" s="T183">ptcl</ta>
            <ta e="T185" id="Seg_9458" s="T184">v</ta>
            <ta e="T186" id="Seg_9459" s="T185">v</ta>
            <ta e="T187" id="Seg_9460" s="T186">ptcl</ta>
            <ta e="T189" id="Seg_9461" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_9462" s="T189">v</ta>
            <ta e="T191" id="Seg_9463" s="T190">v</ta>
            <ta e="T192" id="Seg_9464" s="T191">pers</ta>
            <ta e="T193" id="Seg_9465" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_9466" s="T193">adv</ta>
            <ta e="T195" id="Seg_9467" s="T194">v</ta>
            <ta e="T197" id="Seg_9468" s="T196">pers</ta>
            <ta e="T198" id="Seg_9469" s="T197">ptcl</ta>
            <ta e="T199" id="Seg_9470" s="T198">ptcl</ta>
            <ta e="T200" id="Seg_9471" s="T199">v</ta>
            <ta e="T201" id="Seg_9472" s="T200">adj</ta>
            <ta e="T203" id="Seg_9473" s="T201">ptcl</ta>
            <ta e="T205" id="Seg_9474" s="T204">adj</ta>
            <ta e="T207" id="Seg_9475" s="T205">ptcl</ta>
            <ta e="T210" id="Seg_9476" s="T209">pers</ta>
            <ta e="T211" id="Seg_9477" s="T210">ptcl</ta>
            <ta e="T212" id="Seg_9478" s="T211">adj</ta>
            <ta e="T213" id="Seg_9479" s="T212">v</ta>
            <ta e="T214" id="Seg_9480" s="T213">pers</ta>
            <ta e="T215" id="Seg_9481" s="T214">ptcl</ta>
            <ta e="T216" id="Seg_9482" s="T215">v</ta>
            <ta e="T217" id="Seg_9483" s="T216">n</ta>
            <ta e="T218" id="Seg_9484" s="T217">conj</ta>
            <ta e="T219" id="Seg_9485" s="T218">n</ta>
            <ta e="T220" id="Seg_9486" s="T219">v</ta>
            <ta e="T221" id="Seg_9487" s="T220">conj</ta>
            <ta e="T222" id="Seg_9488" s="T221">v</ta>
            <ta e="T223" id="Seg_9489" s="T222">que</ta>
            <ta e="T224" id="Seg_9490" s="T223">pers</ta>
            <ta e="T225" id="Seg_9491" s="T224">v</ta>
            <ta e="T226" id="Seg_9492" s="T225">que</ta>
            <ta e="T229" id="Seg_9493" s="T228">v</ta>
            <ta e="T230" id="Seg_9494" s="T229">n</ta>
            <ta e="T231" id="Seg_9495" s="T230">que</ta>
            <ta e="T232" id="Seg_9496" s="T231">ptcl</ta>
            <ta e="T233" id="Seg_9497" s="T232">v</ta>
            <ta e="T234" id="Seg_9498" s="T233">que</ta>
            <ta e="T235" id="Seg_9499" s="T234">ptcl</ta>
            <ta e="T237" id="Seg_9500" s="T236">n</ta>
            <ta e="T238" id="Seg_9501" s="T237">ptcl</ta>
            <ta e="T239" id="Seg_9502" s="T238">v</ta>
            <ta e="T240" id="Seg_9503" s="T239">que</ta>
            <ta e="T241" id="Seg_9504" s="T240">ptcl</ta>
            <ta e="T242" id="Seg_9505" s="T241">v</ta>
            <ta e="T243" id="Seg_9506" s="T242">n</ta>
            <ta e="T244" id="Seg_9507" s="T243">adv</ta>
            <ta e="T245" id="Seg_9508" s="T244">quant</ta>
            <ta e="T247" id="Seg_9509" s="T246">num</ta>
            <ta e="T248" id="Seg_9510" s="T247">num</ta>
            <ta e="T249" id="Seg_9511" s="T248">num</ta>
            <ta e="T250" id="Seg_9512" s="T249">n</ta>
            <ta e="T251" id="Seg_9513" s="T250">n</ta>
            <ta e="T252" id="Seg_9514" s="T251">v</ta>
            <ta e="T253" id="Seg_9515" s="T252">quant</ta>
            <ta e="T254" id="Seg_9516" s="T253">n</ta>
            <ta e="T255" id="Seg_9517" s="T254">v</ta>
            <ta e="T256" id="Seg_9518" s="T255">conj</ta>
            <ta e="T257" id="Seg_9519" s="T256">n</ta>
            <ta e="T258" id="Seg_9520" s="T257">v</ta>
            <ta e="T259" id="Seg_9521" s="T258">pers</ta>
            <ta e="T260" id="Seg_9522" s="T259">v</ta>
            <ta e="T261" id="Seg_9523" s="T260">propr</ta>
            <ta e="T262" id="Seg_9524" s="T261">v</ta>
            <ta e="T263" id="Seg_9525" s="T262">ptcl</ta>
            <ta e="T264" id="Seg_9526" s="T263">n</ta>
            <ta e="T265" id="Seg_9527" s="T264">v</ta>
            <ta e="T266" id="Seg_9528" s="T265">pers</ta>
            <ta e="T267" id="Seg_9529" s="T266">v</ta>
            <ta e="T268" id="Seg_9530" s="T267">v</ta>
            <ta e="T269" id="Seg_9531" s="T268">adv</ta>
            <ta e="T270" id="Seg_9532" s="T269">ptcl</ta>
            <ta e="T271" id="Seg_9533" s="T270">v</ta>
            <ta e="T272" id="Seg_9534" s="T271">dempro</ta>
            <ta e="T273" id="Seg_9535" s="T272">ptcl</ta>
            <ta e="T274" id="Seg_9536" s="T273">v</ta>
            <ta e="T275" id="Seg_9537" s="T274">n</ta>
            <ta e="T276" id="Seg_9538" s="T275">n</ta>
            <ta e="T277" id="Seg_9539" s="T276">adv</ta>
            <ta e="T278" id="Seg_9540" s="T277">dempro</ta>
            <ta e="T279" id="Seg_9541" s="T278">ptcl</ta>
            <ta e="T280" id="Seg_9542" s="T279">v</ta>
            <ta e="T281" id="Seg_9543" s="T280">conj</ta>
            <ta e="T282" id="Seg_9544" s="T281">v</ta>
            <ta e="T283" id="Seg_9545" s="T282">pers</ta>
            <ta e="T284" id="Seg_9546" s="T283">v</ta>
            <ta e="T285" id="Seg_9547" s="T284">conj</ta>
            <ta e="T286" id="Seg_9548" s="T285">propr</ta>
            <ta e="T287" id="Seg_9549" s="T286">v</ta>
            <ta e="T288" id="Seg_9550" s="T287">dempro</ta>
            <ta e="T289" id="Seg_9551" s="T288">v</ta>
            <ta e="T290" id="Seg_9552" s="T289">n</ta>
            <ta e="T291" id="Seg_9553" s="T290">conj</ta>
            <ta e="T293" id="Seg_9554" s="T292">v</ta>
            <ta e="T295" id="Seg_9555" s="T293">n</ta>
            <ta e="T296" id="Seg_9556" s="T295">pers</ta>
            <ta e="T297" id="Seg_9557" s="T296">adv</ta>
            <ta e="T298" id="Seg_9558" s="T297">n</ta>
            <ta e="T299" id="Seg_9559" s="T298">v</ta>
            <ta e="T300" id="Seg_9560" s="T299">conj</ta>
            <ta e="T301" id="Seg_9561" s="T300">v</ta>
            <ta e="T302" id="Seg_9562" s="T301">pers</ta>
            <ta e="T303" id="Seg_9563" s="T302">v</ta>
            <ta e="T304" id="Seg_9564" s="T303">num</ta>
            <ta e="T305" id="Seg_9565" s="T304">n</ta>
            <ta e="T306" id="Seg_9566" s="T866">n</ta>
            <ta e="T307" id="Seg_9567" s="T306">v</ta>
            <ta e="T308" id="Seg_9568" s="T307">pers</ta>
            <ta e="T309" id="Seg_9569" s="T308">adv</ta>
            <ta e="T310" id="Seg_9570" s="T309">v</ta>
            <ta e="T311" id="Seg_9571" s="T310">dempro</ta>
            <ta e="T312" id="Seg_9572" s="T311">ptcl</ta>
            <ta e="T314" id="Seg_9573" s="T312">v</ta>
            <ta e="T315" id="Seg_9574" s="T314">conj</ta>
            <ta e="T316" id="Seg_9575" s="T315">dempro</ta>
            <ta e="T317" id="Seg_9576" s="T316">ptcl</ta>
            <ta e="T318" id="Seg_9577" s="T317">pers</ta>
            <ta e="T319" id="Seg_9578" s="T318">v</ta>
            <ta e="T320" id="Seg_9579" s="T319">pers</ta>
            <ta e="T321" id="Seg_9580" s="T320">v</ta>
            <ta e="T323" id="Seg_9581" s="T321">adv</ta>
            <ta e="T324" id="Seg_9582" s="T323">n</ta>
            <ta e="T325" id="Seg_9583" s="T324">v</ta>
            <ta e="T326" id="Seg_9584" s="T325">adj</ta>
            <ta e="T327" id="Seg_9585" s="T326">v</ta>
            <ta e="T328" id="Seg_9586" s="T327">n</ta>
            <ta e="T329" id="Seg_9587" s="T328">quant</ta>
            <ta e="T330" id="Seg_9588" s="T329">v</ta>
            <ta e="T331" id="Seg_9589" s="T330">ptcl</ta>
            <ta e="T332" id="Seg_9590" s="T331">v</ta>
            <ta e="T333" id="Seg_9591" s="T332">n</ta>
            <ta e="T334" id="Seg_9592" s="T333">n</ta>
            <ta e="T335" id="Seg_9593" s="T334">adv</ta>
            <ta e="T336" id="Seg_9594" s="T335">adj</ta>
            <ta e="T337" id="Seg_9595" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_9596" s="T337">n</ta>
            <ta e="T339" id="Seg_9597" s="T338">quant</ta>
            <ta e="T340" id="Seg_9598" s="T339">v</ta>
            <ta e="T341" id="Seg_9599" s="T340">adv</ta>
            <ta e="T342" id="Seg_9600" s="T341">n</ta>
            <ta e="T343" id="Seg_9601" s="T342">v</ta>
            <ta e="T344" id="Seg_9602" s="T865">n</ta>
            <ta e="T345" id="Seg_9603" s="T344">v</ta>
            <ta e="T348" id="Seg_9604" s="T345">ptcl</ta>
            <ta e="T349" id="Seg_9605" s="T348">dempro</ta>
            <ta e="T350" id="Seg_9606" s="T349">n</ta>
            <ta e="T351" id="Seg_9607" s="T350">adv</ta>
            <ta e="T352" id="Seg_9608" s="T351">adj</ta>
            <ta e="T353" id="Seg_9609" s="T352">adv</ta>
            <ta e="T354" id="Seg_9610" s="T353">adj</ta>
            <ta e="T355" id="Seg_9611" s="T354">n</ta>
            <ta e="T357" id="Seg_9612" s="T356">adj</ta>
            <ta e="T358" id="Seg_9613" s="T357">n</ta>
            <ta e="T359" id="Seg_9614" s="T358">ptcl</ta>
            <ta e="T360" id="Seg_9615" s="T359">adj</ta>
            <ta e="T361" id="Seg_9616" s="T360">n</ta>
            <ta e="T362" id="Seg_9617" s="T361">adj</ta>
            <ta e="T363" id="Seg_9618" s="T362">ptcl</ta>
            <ta e="T364" id="Seg_9619" s="T363">n</ta>
            <ta e="T365" id="Seg_9620" s="T364">ptcl</ta>
            <ta e="T366" id="Seg_9621" s="T365">adj</ta>
            <ta e="T368" id="Seg_9622" s="T367">ptcl</ta>
            <ta e="T369" id="Seg_9623" s="T368">v</ta>
            <ta e="T370" id="Seg_9624" s="T369">pers</ta>
            <ta e="T372" id="Seg_9625" s="T371">dempro</ta>
            <ta e="T374" id="Seg_9626" s="T373">v</ta>
            <ta e="T375" id="Seg_9627" s="T374">dempro</ta>
            <ta e="T376" id="Seg_9628" s="T375">ptcl</ta>
            <ta e="T377" id="Seg_9629" s="T376">v</ta>
            <ta e="T378" id="Seg_9630" s="T377">aux</ta>
            <ta e="T379" id="Seg_9631" s="T378">v</ta>
            <ta e="T380" id="Seg_9632" s="T379">n</ta>
            <ta e="T381" id="Seg_9633" s="T380">n</ta>
            <ta e="T382" id="Seg_9634" s="T381">conj</ta>
            <ta e="T383" id="Seg_9635" s="T382">refl</ta>
            <ta e="T384" id="Seg_9636" s="T383">n</ta>
            <ta e="T385" id="Seg_9637" s="T384">v</ta>
            <ta e="T386" id="Seg_9638" s="T385">pers</ta>
            <ta e="T387" id="Seg_9639" s="T386">adv</ta>
            <ta e="T388" id="Seg_9640" s="T387">v</ta>
            <ta e="T389" id="Seg_9641" s="T388">ptcl</ta>
            <ta e="T390" id="Seg_9642" s="T389">adv</ta>
            <ta e="T391" id="Seg_9643" s="T390">v</ta>
            <ta e="T392" id="Seg_9644" s="T391">ptcl</ta>
            <ta e="T393" id="Seg_9645" s="T392">pers</ta>
            <ta e="T394" id="Seg_9646" s="T393">ptcl</ta>
            <ta e="T395" id="Seg_9647" s="T394">v</ta>
            <ta e="T396" id="Seg_9648" s="T395">v</ta>
            <ta e="T397" id="Seg_9649" s="T396">conj</ta>
            <ta e="T398" id="Seg_9650" s="T397">pers</ta>
            <ta e="T399" id="Seg_9651" s="T398">v</ta>
            <ta e="T400" id="Seg_9652" s="T399">dempro</ta>
            <ta e="T401" id="Seg_9653" s="T400">v</ta>
            <ta e="T402" id="Seg_9654" s="T401">num</ta>
            <ta e="T403" id="Seg_9655" s="T402">n</ta>
            <ta e="T405" id="Seg_9656" s="T404">v</ta>
            <ta e="T406" id="Seg_9657" s="T405">num</ta>
            <ta e="T407" id="Seg_9658" s="T406">dempro</ta>
            <ta e="T408" id="Seg_9659" s="T407">v</ta>
            <ta e="T409" id="Seg_9660" s="T408">conj</ta>
            <ta e="T410" id="Seg_9661" s="T409">num</ta>
            <ta e="T411" id="Seg_9662" s="T410">adv</ta>
            <ta e="T412" id="Seg_9663" s="T411">v</ta>
            <ta e="T413" id="Seg_9664" s="T412">adv</ta>
            <ta e="T415" id="Seg_9665" s="T413">ptcl</ta>
            <ta e="T420" id="Seg_9666" s="T419">adv</ta>
            <ta e="T421" id="Seg_9667" s="T420">adj</ta>
            <ta e="T422" id="Seg_9668" s="T421">n</ta>
            <ta e="T423" id="Seg_9669" s="T422">n</ta>
            <ta e="T424" id="Seg_9670" s="T423">v</ta>
            <ta e="T425" id="Seg_9671" s="T424">conj</ta>
            <ta e="T426" id="Seg_9672" s="T425">adv</ta>
            <ta e="T427" id="Seg_9673" s="T426">v</ta>
            <ta e="T428" id="Seg_9674" s="T427">v</ta>
            <ta e="T429" id="Seg_9675" s="T428">propr</ta>
            <ta e="T430" id="Seg_9676" s="T429">v</ta>
            <ta e="T431" id="Seg_9677" s="T430">n</ta>
            <ta e="T432" id="Seg_9678" s="T431">conj</ta>
            <ta e="T433" id="Seg_9679" s="T432">propr</ta>
            <ta e="T434" id="Seg_9680" s="T433">n</ta>
            <ta e="T435" id="Seg_9681" s="T434">v</ta>
            <ta e="T436" id="Seg_9682" s="T435">conj</ta>
            <ta e="T437" id="Seg_9683" s="T436">n</ta>
            <ta e="T438" id="Seg_9684" s="T437">v</ta>
            <ta e="T439" id="Seg_9685" s="T438">v</ta>
            <ta e="T441" id="Seg_9686" s="T440">n</ta>
            <ta e="T442" id="Seg_9687" s="T441">v</ta>
            <ta e="T443" id="Seg_9688" s="T442">ptcl</ta>
            <ta e="T444" id="Seg_9689" s="T443">n</ta>
            <ta e="T445" id="Seg_9690" s="T444">v</ta>
            <ta e="T446" id="Seg_9691" s="T445">adj</ta>
            <ta e="T447" id="Seg_9692" s="T446">n</ta>
            <ta e="T448" id="Seg_9693" s="T447">v</ta>
            <ta e="T451" id="Seg_9694" s="T450">ptcl</ta>
            <ta e="T452" id="Seg_9695" s="T451">adv</ta>
            <ta e="T453" id="Seg_9696" s="T452">v</ta>
            <ta e="T454" id="Seg_9697" s="T453">ptcl</ta>
            <ta e="T455" id="Seg_9698" s="T454">n</ta>
            <ta e="T456" id="Seg_9699" s="T455">v</ta>
            <ta e="T457" id="Seg_9700" s="T456">n</ta>
            <ta e="T458" id="Seg_9701" s="T457">v</ta>
            <ta e="T459" id="Seg_9702" s="T458">ptcl</ta>
            <ta e="T460" id="Seg_9703" s="T459">n</ta>
            <ta e="T461" id="Seg_9704" s="T460">v</ta>
            <ta e="T463" id="Seg_9705" s="T462">pers</ta>
            <ta e="T464" id="Seg_9706" s="T463">adj</ta>
            <ta e="T465" id="Seg_9707" s="T464">v</ta>
            <ta e="T466" id="Seg_9708" s="T465">conj</ta>
            <ta e="T467" id="Seg_9709" s="T466">n</ta>
            <ta e="T468" id="Seg_9710" s="T467">adj</ta>
            <ta e="T469" id="Seg_9711" s="T468">v</ta>
            <ta e="T470" id="Seg_9712" s="T469">adv</ta>
            <ta e="T471" id="Seg_9713" s="T470">pers</ta>
            <ta e="T472" id="Seg_9714" s="T471">v</ta>
            <ta e="T473" id="Seg_9715" s="T472">refl</ta>
            <ta e="T474" id="Seg_9716" s="T473">n</ta>
            <ta e="T475" id="Seg_9717" s="T474">v</ta>
            <ta e="T476" id="Seg_9718" s="T475">pers</ta>
            <ta e="T480" id="Seg_9719" s="T479">adj</ta>
            <ta e="T481" id="Seg_9720" s="T480">v</ta>
            <ta e="T482" id="Seg_9721" s="T481">v</ta>
            <ta e="T483" id="Seg_9722" s="T482">ptcl</ta>
            <ta e="T484" id="Seg_9723" s="T483">dempro</ta>
            <ta e="T485" id="Seg_9724" s="T484">n</ta>
            <ta e="T486" id="Seg_9725" s="T485">adv</ta>
            <ta e="T487" id="Seg_9726" s="T486">adj</ta>
            <ta e="T488" id="Seg_9727" s="T487">v</ta>
            <ta e="T492" id="Seg_9728" s="T491">adv</ta>
            <ta e="T493" id="Seg_9729" s="T492">adj</ta>
            <ta e="T501" id="Seg_9730" s="T500">adv</ta>
            <ta e="T502" id="Seg_9731" s="T501">adv</ta>
            <ta e="T503" id="Seg_9732" s="T502">v</ta>
            <ta e="T504" id="Seg_9733" s="T503">dempro</ta>
            <ta e="T505" id="Seg_9734" s="T504">n</ta>
            <ta e="T506" id="Seg_9735" s="T505">v</ta>
            <ta e="T507" id="Seg_9736" s="T506">ptcl</ta>
            <ta e="T508" id="Seg_9737" s="T507">n</ta>
            <ta e="T509" id="Seg_9738" s="T508">v</ta>
            <ta e="T510" id="Seg_9739" s="T509">n</ta>
            <ta e="T511" id="Seg_9740" s="T510">n</ta>
            <ta e="T512" id="Seg_9741" s="T511">n</ta>
            <ta e="T513" id="Seg_9742" s="T512">conj</ta>
            <ta e="T514" id="Seg_9743" s="T513">adv</ta>
            <ta e="T515" id="Seg_9744" s="T514">dempro</ta>
            <ta e="T516" id="Seg_9745" s="T515">v</ta>
            <ta e="T517" id="Seg_9746" s="T516">conj</ta>
            <ta e="T518" id="Seg_9747" s="T517">n</ta>
            <ta e="T519" id="Seg_9748" s="T518">v</ta>
            <ta e="T520" id="Seg_9749" s="T519">adv</ta>
            <ta e="T521" id="Seg_9750" s="T520">adv</ta>
            <ta e="T522" id="Seg_9751" s="T521">v</ta>
            <ta e="T523" id="Seg_9752" s="T522">dempro</ta>
            <ta e="T524" id="Seg_9753" s="T523">n</ta>
            <ta e="T525" id="Seg_9754" s="T524">adv</ta>
            <ta e="T526" id="Seg_9755" s="T525">adj</ta>
            <ta e="T527" id="Seg_9756" s="T526">n</ta>
            <ta e="T528" id="Seg_9757" s="T527">v</ta>
            <ta e="T529" id="Seg_9758" s="T528">dempro</ta>
            <ta e="T530" id="Seg_9759" s="T529">dempro</ta>
            <ta e="T533" id="Seg_9760" s="T532">n</ta>
            <ta e="T535" id="Seg_9761" s="T534">v</ta>
            <ta e="T536" id="Seg_9762" s="T535">n</ta>
            <ta e="T537" id="Seg_9763" s="T536">n</ta>
            <ta e="T538" id="Seg_9764" s="T537">n</ta>
            <ta e="T539" id="Seg_9765" s="T538">ptcl</ta>
            <ta e="T540" id="Seg_9766" s="T539">n</ta>
            <ta e="T542" id="Seg_9767" s="T541">v</ta>
            <ta e="T543" id="Seg_9768" s="T542">n</ta>
            <ta e="T544" id="Seg_9769" s="T543">v</ta>
            <ta e="T546" id="Seg_9770" s="T545">v</ta>
            <ta e="T547" id="Seg_9771" s="T546">adv</ta>
            <ta e="T548" id="Seg_9772" s="T547">adj</ta>
            <ta e="T549" id="Seg_9773" s="T548">n</ta>
            <ta e="T552" id="Seg_9774" s="T551">n</ta>
            <ta e="T553" id="Seg_9775" s="T552">n</ta>
            <ta e="T554" id="Seg_9776" s="T553">ptcl</ta>
            <ta e="T555" id="Seg_9777" s="T554">v</ta>
            <ta e="T867" id="Seg_9778" s="T555">adj</ta>
            <ta e="T557" id="Seg_9779" s="T556">n</ta>
            <ta e="T558" id="Seg_9780" s="T557">adj</ta>
            <ta e="T559" id="Seg_9781" s="T558">conj</ta>
            <ta e="T560" id="Seg_9782" s="T559">adj</ta>
            <ta e="T561" id="Seg_9783" s="T560">n</ta>
            <ta e="T563" id="Seg_9784" s="T561">adj</ta>
            <ta e="T567" id="Seg_9785" s="T566">adv</ta>
            <ta e="T568" id="Seg_9786" s="T567">adj</ta>
            <ta e="T570" id="Seg_9787" s="T569">n</ta>
            <ta e="T571" id="Seg_9788" s="T570">v</ta>
            <ta e="T572" id="Seg_9789" s="T571">pers</ta>
            <ta e="T573" id="Seg_9790" s="T572">n</ta>
            <ta e="T575" id="Seg_9791" s="T574">v</ta>
            <ta e="T576" id="Seg_9792" s="T575">dempro</ta>
            <ta e="T577" id="Seg_9793" s="T576">ptcl</ta>
            <ta e="T578" id="Seg_9794" s="T577">ptcl</ta>
            <ta e="T579" id="Seg_9795" s="T578">adj</ta>
            <ta e="T580" id="Seg_9796" s="T579">n</ta>
            <ta e="T581" id="Seg_9797" s="T580">ptcl</ta>
            <ta e="T582" id="Seg_9798" s="T581">v</ta>
            <ta e="T583" id="Seg_9799" s="T582">adv</ta>
            <ta e="T584" id="Seg_9800" s="T583">ptcl</ta>
            <ta e="T585" id="Seg_9801" s="T584">v</ta>
            <ta e="T586" id="Seg_9802" s="T585">v</ta>
            <ta e="T587" id="Seg_9803" s="T586">ptcl</ta>
            <ta e="T588" id="Seg_9804" s="T587">adj</ta>
            <ta e="T589" id="Seg_9805" s="T588">ptcl</ta>
            <ta e="T590" id="Seg_9806" s="T589">adj</ta>
            <ta e="T591" id="Seg_9807" s="T590">adv</ta>
            <ta e="T592" id="Seg_9808" s="T591">dempro</ta>
            <ta e="T593" id="Seg_9809" s="T592">n</ta>
            <ta e="T595" id="Seg_9810" s="T594">n</ta>
            <ta e="T596" id="Seg_9811" s="T595">v</ta>
            <ta e="T597" id="Seg_9812" s="T596">n</ta>
            <ta e="T598" id="Seg_9813" s="T597">adv</ta>
            <ta e="T600" id="Seg_9814" s="T599">n</ta>
            <ta e="T601" id="Seg_9815" s="T600">v</ta>
            <ta e="T602" id="Seg_9816" s="T601">conj</ta>
            <ta e="T603" id="Seg_9817" s="T602">adv</ta>
            <ta e="T604" id="Seg_9818" s="T603">v</ta>
            <ta e="T605" id="Seg_9819" s="T604">conj</ta>
            <ta e="T606" id="Seg_9820" s="T605">v</ta>
            <ta e="T607" id="Seg_9821" s="T606">n</ta>
            <ta e="T608" id="Seg_9822" s="T607">n</ta>
            <ta e="T609" id="Seg_9823" s="T608">n</ta>
            <ta e="T610" id="Seg_9824" s="T609">v</ta>
            <ta e="T613" id="Seg_9825" s="T612">adv</ta>
            <ta e="T614" id="Seg_9826" s="T613">n</ta>
            <ta e="T615" id="Seg_9827" s="T614">ptcl</ta>
            <ta e="T616" id="Seg_9828" s="T615">v</ta>
            <ta e="T617" id="Seg_9829" s="T616">adv</ta>
            <ta e="T618" id="Seg_9830" s="T617">adv</ta>
            <ta e="T619" id="Seg_9831" s="T618">v</ta>
            <ta e="T620" id="Seg_9832" s="T868">ptcl</ta>
            <ta e="T621" id="Seg_9833" s="T620">n</ta>
            <ta e="T622" id="Seg_9834" s="T621">v</ta>
            <ta e="T623" id="Seg_9835" s="T622">num</ta>
            <ta e="T624" id="Seg_9836" s="T623">conj</ta>
            <ta e="T625" id="Seg_9837" s="T624">n</ta>
            <ta e="T627" id="Seg_9838" s="T625">v</ta>
            <ta e="T629" id="Seg_9839" s="T628">v</ta>
            <ta e="T631" id="Seg_9840" s="T630">v</ta>
            <ta e="T632" id="Seg_9841" s="T631">n</ta>
            <ta e="T633" id="Seg_9842" s="T632">v</ta>
            <ta e="T634" id="Seg_9843" s="T633">n</ta>
            <ta e="T636" id="Seg_9844" s="T635">v</ta>
            <ta e="T637" id="Seg_9845" s="T636">n</ta>
            <ta e="T638" id="Seg_9846" s="T637">v</ta>
            <ta e="T639" id="Seg_9847" s="T638">n</ta>
            <ta e="T640" id="Seg_9848" s="T639">v</ta>
            <ta e="T641" id="Seg_9849" s="T640">n</ta>
            <ta e="T642" id="Seg_9850" s="T641">v</ta>
            <ta e="T643" id="Seg_9851" s="T642">n</ta>
            <ta e="T644" id="Seg_9852" s="T643">v</ta>
            <ta e="T645" id="Seg_9853" s="T644">pers</ta>
            <ta e="T646" id="Seg_9854" s="T645">n</ta>
            <ta e="T647" id="Seg_9855" s="T646">n</ta>
            <ta e="T648" id="Seg_9856" s="T647">v</ta>
            <ta e="T650" id="Seg_9857" s="T649">v</ta>
            <ta e="T651" id="Seg_9858" s="T650">n</ta>
            <ta e="T653" id="Seg_9859" s="T652">quant</ta>
            <ta e="T654" id="Seg_9860" s="T653">v</ta>
            <ta e="T655" id="Seg_9861" s="T654">pers</ta>
            <ta e="T656" id="Seg_9862" s="T655">v</ta>
            <ta e="T657" id="Seg_9863" s="T656">conj</ta>
            <ta e="T658" id="Seg_9864" s="T657">v</ta>
            <ta e="T659" id="Seg_9865" s="T658">conj</ta>
            <ta e="T660" id="Seg_9866" s="T659">v</ta>
            <ta e="T661" id="Seg_9867" s="T660">conj</ta>
            <ta e="T662" id="Seg_9868" s="T661">v</ta>
            <ta e="T663" id="Seg_9869" s="T662">pers</ta>
            <ta e="T664" id="Seg_9870" s="T663">v</ta>
            <ta e="T665" id="Seg_9871" s="T664">conj</ta>
            <ta e="T666" id="Seg_9872" s="T665">adv</ta>
            <ta e="T667" id="Seg_9873" s="T666">adj</ta>
            <ta e="T668" id="Seg_9874" s="T667">n</ta>
            <ta e="T669" id="Seg_9875" s="T668">adv</ta>
            <ta e="T670" id="Seg_9876" s="T669">ptcl</ta>
            <ta e="T671" id="Seg_9877" s="T670">n</ta>
            <ta e="T672" id="Seg_9878" s="T671">v</ta>
            <ta e="T673" id="Seg_9879" s="T672">n</ta>
            <ta e="T674" id="Seg_9880" s="T673">v</ta>
            <ta e="T675" id="Seg_9881" s="T674">n</ta>
            <ta e="T676" id="Seg_9882" s="T675">ptcl</ta>
            <ta e="T677" id="Seg_9883" s="T676">n</ta>
            <ta e="T678" id="Seg_9884" s="T677">v</ta>
            <ta e="T679" id="Seg_9885" s="T678">conj</ta>
            <ta e="T680" id="Seg_9886" s="T679">refl</ta>
            <ta e="T681" id="Seg_9887" s="T680">n</ta>
            <ta e="T682" id="Seg_9888" s="T681">v</ta>
            <ta e="T684" id="Seg_9889" s="T683">n</ta>
            <ta e="T685" id="Seg_9890" s="T684">adv</ta>
            <ta e="T686" id="Seg_9891" s="T685">n</ta>
            <ta e="T687" id="Seg_9892" s="T686">quant</ta>
            <ta e="T688" id="Seg_9893" s="T687">ptcl</ta>
            <ta e="T689" id="Seg_9894" s="T688">n</ta>
            <ta e="T690" id="Seg_9895" s="T689">ptcl</ta>
            <ta e="T691" id="Seg_9896" s="T690">v</ta>
            <ta e="T692" id="Seg_9897" s="T691">adv</ta>
            <ta e="T693" id="Seg_9898" s="T692">dempro</ta>
            <ta e="T694" id="Seg_9899" s="T693">adj</ta>
            <ta e="T695" id="Seg_9900" s="T694">adv</ta>
            <ta e="T696" id="Seg_9901" s="T695">adj</ta>
            <ta e="T697" id="Seg_9902" s="T696">n</ta>
            <ta e="T698" id="Seg_9903" s="T697">adv</ta>
            <ta e="T699" id="Seg_9904" s="T698">adj</ta>
            <ta e="T700" id="Seg_9905" s="T699">adj</ta>
            <ta e="T701" id="Seg_9906" s="T700">n</ta>
            <ta e="T702" id="Seg_9907" s="T701">adv</ta>
            <ta e="T703" id="Seg_9908" s="T702">adj</ta>
            <ta e="T704" id="Seg_9909" s="T703">adj</ta>
            <ta e="T705" id="Seg_9910" s="T704">n</ta>
            <ta e="T706" id="Seg_9911" s="T705">n</ta>
            <ta e="T707" id="Seg_9912" s="T706">adj</ta>
            <ta e="T708" id="Seg_9913" s="T707">n</ta>
            <ta e="T709" id="Seg_9914" s="T708">n</ta>
            <ta e="T710" id="Seg_9915" s="T709">adj</ta>
            <ta e="T711" id="Seg_9916" s="T710">n</ta>
            <ta e="T712" id="Seg_9917" s="T711">adj</ta>
            <ta e="T713" id="Seg_9918" s="T712">pers</ta>
            <ta e="T714" id="Seg_9919" s="T713">n</ta>
            <ta e="T715" id="Seg_9920" s="T714">v</ta>
            <ta e="T716" id="Seg_9921" s="T715">n</ta>
            <ta e="T717" id="Seg_9922" s="T716">v</ta>
            <ta e="T718" id="Seg_9923" s="T717">n</ta>
            <ta e="T719" id="Seg_9924" s="T718">n</ta>
            <ta e="T720" id="Seg_9925" s="T719">v</ta>
            <ta e="T721" id="Seg_9926" s="T720">n</ta>
            <ta e="T723" id="Seg_9927" s="T721">n</ta>
            <ta e="T724" id="Seg_9928" s="T723">pers</ta>
            <ta e="T725" id="Seg_9929" s="T724">n</ta>
            <ta e="T726" id="Seg_9930" s="T725">v</ta>
            <ta e="T727" id="Seg_9931" s="T726">n</ta>
            <ta e="T728" id="Seg_9932" s="T727">v</ta>
            <ta e="T729" id="Seg_9933" s="T728">n</ta>
            <ta e="T730" id="Seg_9934" s="T729">n</ta>
            <ta e="T731" id="Seg_9935" s="T730">v</ta>
            <ta e="T732" id="Seg_9936" s="T731">n</ta>
            <ta e="T733" id="Seg_9937" s="T732">n</ta>
            <ta e="T735" id="Seg_9938" s="T734">v</ta>
            <ta e="T736" id="Seg_9939" s="T735">n</ta>
            <ta e="T737" id="Seg_9940" s="T736">n</ta>
            <ta e="T738" id="Seg_9941" s="T737">dempro</ta>
            <ta e="T739" id="Seg_9942" s="T738">v</ta>
            <ta e="T740" id="Seg_9943" s="T739">ptcl</ta>
            <ta e="T741" id="Seg_9944" s="T740">dempro</ta>
            <ta e="T743" id="Seg_9945" s="T742">v</ta>
            <ta e="T744" id="Seg_9946" s="T743">dempro</ta>
            <ta e="T746" id="Seg_9947" s="T745">v</ta>
            <ta e="T747" id="Seg_9948" s="T746">v</ta>
            <ta e="T748" id="Seg_9949" s="T747">pers</ta>
            <ta e="T749" id="Seg_9950" s="T748">n</ta>
            <ta e="T750" id="Seg_9951" s="T749">ptcl</ta>
            <ta e="T751" id="Seg_9952" s="T750">adv</ta>
            <ta e="T752" id="Seg_9953" s="T751">v</ta>
            <ta e="T753" id="Seg_9954" s="T752">pers</ta>
            <ta e="T754" id="Seg_9955" s="T753">dempro</ta>
            <ta e="T755" id="Seg_9956" s="T754">v</ta>
            <ta e="T756" id="Seg_9957" s="T755">n</ta>
            <ta e="T757" id="Seg_9958" s="T756">v</ta>
            <ta e="T758" id="Seg_9959" s="T757">v</ta>
            <ta e="T759" id="Seg_9960" s="T758">adv</ta>
            <ta e="T760" id="Seg_9961" s="T759">n</ta>
            <ta e="T761" id="Seg_9962" s="T760">v</ta>
            <ta e="T762" id="Seg_9963" s="T761">refl</ta>
            <ta e="T763" id="Seg_9964" s="T762">dempro</ta>
            <ta e="T764" id="Seg_9965" s="T763">ptcl</ta>
            <ta e="T765" id="Seg_9966" s="T764">v</ta>
            <ta e="T766" id="Seg_9967" s="T765">v</ta>
            <ta e="T767" id="Seg_9968" s="T766">pers</ta>
            <ta e="T768" id="Seg_9969" s="T767">n</ta>
            <ta e="T769" id="Seg_9970" s="T768">v</ta>
            <ta e="T770" id="Seg_9971" s="T769">v</ta>
            <ta e="T771" id="Seg_9972" s="T770">pers</ta>
            <ta e="T772" id="Seg_9973" s="T771">pers</ta>
            <ta e="T773" id="Seg_9974" s="T772">adv</ta>
            <ta e="T774" id="Seg_9975" s="T773">n</ta>
            <ta e="T775" id="Seg_9976" s="T774">v</ta>
            <ta e="T776" id="Seg_9977" s="T775">n</ta>
            <ta e="T777" id="Seg_9978" s="T776">v</ta>
            <ta e="T778" id="Seg_9979" s="T777">ptcl</ta>
            <ta e="T779" id="Seg_9980" s="T778">v</ta>
            <ta e="T780" id="Seg_9981" s="T779">adv</ta>
            <ta e="T782" id="Seg_9982" s="T781">v</ta>
            <ta e="T783" id="Seg_9983" s="T782">adv</ta>
            <ta e="T784" id="Seg_9984" s="T783">v</ta>
            <ta e="T785" id="Seg_9985" s="T784">n</ta>
            <ta e="T786" id="Seg_9986" s="T785">v</ta>
            <ta e="T787" id="Seg_9987" s="T786">n</ta>
            <ta e="T789" id="Seg_9988" s="T787">num</ta>
            <ta e="T790" id="Seg_9989" s="T789">num</ta>
            <ta e="T791" id="Seg_9990" s="T790">n</ta>
            <ta e="T792" id="Seg_9991" s="T791">v</ta>
            <ta e="T793" id="Seg_9992" s="T792">pers</ta>
            <ta e="T794" id="Seg_9993" s="T793">adv</ta>
            <ta e="T795" id="Seg_9994" s="T794">adv</ta>
            <ta e="T796" id="Seg_9995" s="T795">v</ta>
            <ta e="T799" id="Seg_9996" s="T798">n</ta>
            <ta e="T800" id="Seg_9997" s="T799">v</ta>
            <ta e="T801" id="Seg_9998" s="T800">ptcl</ta>
            <ta e="T802" id="Seg_9999" s="T801">v</ta>
            <ta e="T804" id="Seg_10000" s="T803">n</ta>
            <ta e="T805" id="Seg_10001" s="T804">n</ta>
            <ta e="T806" id="Seg_10002" s="T805">v</ta>
            <ta e="T807" id="Seg_10003" s="T806">pers</ta>
            <ta e="T808" id="Seg_10004" s="T807">v</ta>
            <ta e="T809" id="Seg_10005" s="T808">pers</ta>
            <ta e="T810" id="Seg_10006" s="T809">dempro</ta>
            <ta e="T811" id="Seg_10007" s="T810">v</ta>
            <ta e="T813" id="Seg_10008" s="T812">ptcl</ta>
            <ta e="T814" id="Seg_10009" s="T813">ptcl</ta>
            <ta e="T815" id="Seg_10010" s="T814">dempro</ta>
            <ta e="T816" id="Seg_10011" s="T815">ptcl</ta>
            <ta e="T817" id="Seg_10012" s="T816">adv</ta>
            <ta e="T819" id="Seg_10013" s="T818">v</ta>
            <ta e="T820" id="Seg_10014" s="T819">v</ta>
            <ta e="T821" id="Seg_10015" s="T820">dempro</ta>
            <ta e="T822" id="Seg_10016" s="T821">adv</ta>
            <ta e="T824" id="Seg_10017" s="T823">v</ta>
            <ta e="T825" id="Seg_10018" s="T824">ptcl</ta>
            <ta e="T826" id="Seg_10019" s="T825">ptcl</ta>
            <ta e="T827" id="Seg_10020" s="T826">ptcl</ta>
            <ta e="T828" id="Seg_10021" s="T827">pers</ta>
            <ta e="T829" id="Seg_10022" s="T828">n</ta>
            <ta e="T830" id="Seg_10023" s="T829">ptcl</ta>
            <ta e="T831" id="Seg_10024" s="T830">v</ta>
            <ta e="T832" id="Seg_10025" s="T831">pers</ta>
            <ta e="T833" id="Seg_10026" s="T832">dempro</ta>
            <ta e="T834" id="Seg_10027" s="T833">ptcl</ta>
            <ta e="T835" id="Seg_10028" s="T834">adv</ta>
            <ta e="T836" id="Seg_10029" s="T835">n</ta>
            <ta e="T837" id="Seg_10030" s="T836">n</ta>
            <ta e="T838" id="Seg_10031" s="T837">n</ta>
            <ta e="T839" id="Seg_10032" s="T838">n</ta>
            <ta e="T840" id="Seg_10033" s="T839">n</ta>
            <ta e="T841" id="Seg_10034" s="T840">n</ta>
            <ta e="T842" id="Seg_10035" s="T841">n</ta>
            <ta e="T843" id="Seg_10036" s="T842">n</ta>
            <ta e="T844" id="Seg_10037" s="T843">n</ta>
            <ta e="T845" id="Seg_10038" s="T844">n</ta>
            <ta e="T847" id="Seg_10039" s="T846">n</ta>
            <ta e="T848" id="Seg_10040" s="T847">n</ta>
            <ta e="T849" id="Seg_10041" s="T848">n</ta>
            <ta e="T851" id="Seg_10042" s="T850">n</ta>
            <ta e="T852" id="Seg_10043" s="T851">n</ta>
            <ta e="T853" id="Seg_10044" s="T852">n</ta>
            <ta e="T854" id="Seg_10045" s="T853">n</ta>
            <ta e="T855" id="Seg_10046" s="T854">n</ta>
            <ta e="T856" id="Seg_10047" s="T855">n</ta>
            <ta e="T858" id="Seg_10048" s="T857">n</ta>
            <ta e="T861" id="Seg_10049" s="T860">n</ta>
            <ta e="T863" id="Seg_10050" s="T862">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-PKZ">
            <ta e="T5" id="Seg_10051" s="T4">adv:L</ta>
            <ta e="T6" id="Seg_10052" s="T5">0.2.h:A</ta>
            <ta e="T8" id="Seg_10053" s="T7">pro.h:B</ta>
            <ta e="T9" id="Seg_10054" s="T8">np:Th</ta>
            <ta e="T10" id="Seg_10055" s="T9">0.2.h:A</ta>
            <ta e="T11" id="Seg_10056" s="T10">pro.h:Poss</ta>
            <ta e="T12" id="Seg_10057" s="T11">np.h:A</ta>
            <ta e="T15" id="Seg_10058" s="T14">np:G</ta>
            <ta e="T17" id="Seg_10059" s="T869">0.3.h:A</ta>
            <ta e="T18" id="Seg_10060" s="T17">np:Ins</ta>
            <ta e="T20" id="Seg_10061" s="T19">0.3.h:A</ta>
            <ta e="T22" id="Seg_10062" s="T21">np.h:A</ta>
            <ta e="T27" id="Seg_10063" s="T26">0.3.h:A</ta>
            <ta e="T29" id="Seg_10064" s="T28">adv:Time</ta>
            <ta e="T30" id="Seg_10065" s="T29">0.3.h:A</ta>
            <ta e="T32" id="Seg_10066" s="T31">adv:Time</ta>
            <ta e="T34" id="Seg_10067" s="T33">0.2.h:A</ta>
            <ta e="T43" id="Seg_10068" s="T42">0.2.h:A</ta>
            <ta e="T45" id="Seg_10069" s="T44">np:P 0.1.h:Poss</ta>
            <ta e="T47" id="Seg_10070" s="T46">np:P</ta>
            <ta e="T49" id="Seg_10071" s="T48">pro.h:A</ta>
            <ta e="T50" id="Seg_10072" s="T49">adv:Time</ta>
            <ta e="T51" id="Seg_10073" s="T50">pro:P</ta>
            <ta e="T54" id="Seg_10074" s="T53">np:Ins</ta>
            <ta e="T55" id="Seg_10075" s="T54">np:Ins</ta>
            <ta e="T56" id="Seg_10076" s="T55">np:Ins</ta>
            <ta e="T57" id="Seg_10077" s="T56">np:L</ta>
            <ta e="T60" id="Seg_10078" s="T59">np:A</ta>
            <ta e="T63" id="Seg_10079" s="T62">np:R</ta>
            <ta e="T66" id="Seg_10080" s="T65">adv:Time</ta>
            <ta e="T68" id="Seg_10081" s="T67">pro.h:A</ta>
            <ta e="T70" id="Seg_10082" s="T69">0.2.h:A</ta>
            <ta e="T71" id="Seg_10083" s="T70">np:G</ta>
            <ta e="T72" id="Seg_10084" s="T71">np:Th</ta>
            <ta e="T73" id="Seg_10085" s="T72">np:Th</ta>
            <ta e="T76" id="Seg_10086" s="T75">pro:Th</ta>
            <ta e="T78" id="Seg_10087" s="T77">0.1.h:E</ta>
            <ta e="T79" id="Seg_10088" s="T78">pro:Th</ta>
            <ta e="T81" id="Seg_10089" s="T80">0.1.h:A</ta>
            <ta e="T84" id="Seg_10090" s="T83">np:Th 0.3.h:Poss</ta>
            <ta e="T86" id="Seg_10091" s="T85">np:G</ta>
            <ta e="T87" id="Seg_10092" s="T86">0.1.h:A</ta>
            <ta e="T92" id="Seg_10093" s="T91">0.1.h:A</ta>
            <ta e="T93" id="Seg_10094" s="T92">np:G</ta>
            <ta e="T94" id="Seg_10095" s="T93">0.1.h:A</ta>
            <ta e="T96" id="Seg_10096" s="T95">0.1.h:A</ta>
            <ta e="T97" id="Seg_10097" s="T96">adv:Time</ta>
            <ta e="T98" id="Seg_10098" s="T97">0.1.h:A</ta>
            <ta e="T100" id="Seg_10099" s="T99">adv:Time</ta>
            <ta e="T101" id="Seg_10100" s="T100">0.1.h:A</ta>
            <ta e="T103" id="Seg_10101" s="T102">0.1.h:E</ta>
            <ta e="T104" id="Seg_10102" s="T103">pro:Th</ta>
            <ta e="T107" id="Seg_10103" s="T106">pro:Th</ta>
            <ta e="T109" id="Seg_10104" s="T108">0.1.h:A</ta>
            <ta e="T110" id="Seg_10105" s="T109">pro.h:A</ta>
            <ta e="T113" id="Seg_10106" s="T112">pro.h:A</ta>
            <ta e="T114" id="Seg_10107" s="T113">pro.h:Th</ta>
            <ta e="T117" id="Seg_10108" s="T116">0.2.h:A</ta>
            <ta e="T119" id="Seg_10109" s="T118">0.2.h:A</ta>
            <ta e="T120" id="Seg_10110" s="T119">adv:L</ta>
            <ta e="T121" id="Seg_10111" s="T120">pro.h:A</ta>
            <ta e="T122" id="Seg_10112" s="T121">pro.h:R</ta>
            <ta e="T124" id="Seg_10113" s="T123">0.2.h:A</ta>
            <ta e="T125" id="Seg_10114" s="T124">adv:L</ta>
            <ta e="T126" id="Seg_10115" s="T125">pro:L</ta>
            <ta e="T129" id="Seg_10116" s="T128">np.h:Th</ta>
            <ta e="T131" id="Seg_10117" s="T130">np:Th</ta>
            <ta e="T135" id="Seg_10118" s="T134">0.2.h:A</ta>
            <ta e="T136" id="Seg_10119" s="T135">np:L</ta>
            <ta e="T137" id="Seg_10120" s="T136">pro:Th</ta>
            <ta e="T138" id="Seg_10121" s="T137">np:Th</ta>
            <ta e="T140" id="Seg_10122" s="T139">0.2.h:A</ta>
            <ta e="T142" id="Seg_10123" s="T140">0.2.h:A</ta>
            <ta e="T143" id="Seg_10124" s="T142">pro:Th</ta>
            <ta e="T144" id="Seg_10125" s="T143">0.2.h:A</ta>
            <ta e="T145" id="Seg_10126" s="T144">pro:Th</ta>
            <ta e="T146" id="Seg_10127" s="T145">np:Ins</ta>
            <ta e="T148" id="Seg_10128" s="T146">pro.h:B</ta>
            <ta e="T149" id="Seg_10129" s="T148">np:Th</ta>
            <ta e="T150" id="Seg_10130" s="T149">0.1.h:A</ta>
            <ta e="T152" id="Seg_10131" s="T151">pro.h:B</ta>
            <ta e="T153" id="Seg_10132" s="T152">np:Th</ta>
            <ta e="T154" id="Seg_10133" s="T153">0.1.h:A</ta>
            <ta e="T155" id="Seg_10134" s="T154">pro.h:B</ta>
            <ta e="T156" id="Seg_10135" s="T155">np:Th</ta>
            <ta e="T157" id="Seg_10136" s="T156">0.3.h:A</ta>
            <ta e="T159" id="Seg_10137" s="T158">pro.h:B</ta>
            <ta e="T160" id="Seg_10138" s="T159">np:Th</ta>
            <ta e="T161" id="Seg_10139" s="T160">0.3.h:A</ta>
            <ta e="T164" id="Seg_10140" s="T163">0.1.h:A</ta>
            <ta e="T165" id="Seg_10141" s="T164">pro.h:Th</ta>
            <ta e="T170" id="Seg_10142" s="T169">0.1.h:A</ta>
            <ta e="T171" id="Seg_10143" s="T170">np:Th</ta>
            <ta e="T172" id="Seg_10144" s="T171">0.3.h:A</ta>
            <ta e="T176" id="Seg_10145" s="T175">np.h:A</ta>
            <ta e="T179" id="Seg_10146" s="T178">pro.h:A</ta>
            <ta e="T182" id="Seg_10147" s="T181">adv:Time</ta>
            <ta e="T183" id="Seg_10148" s="T182">pro.h:A</ta>
            <ta e="T186" id="Seg_10149" s="T185">0.3.h:A</ta>
            <ta e="T190" id="Seg_10150" s="T189">0.3.h:A</ta>
            <ta e="T192" id="Seg_10151" s="T191">pro.h:A</ta>
            <ta e="T197" id="Seg_10152" s="T196">pro.h:A</ta>
            <ta e="T210" id="Seg_10153" s="T209">pro.h:A</ta>
            <ta e="T214" id="Seg_10154" s="T213">pro.h:E</ta>
            <ta e="T217" id="Seg_10155" s="T216">np.h:E</ta>
            <ta e="T219" id="Seg_10156" s="T218">np.h:E</ta>
            <ta e="T222" id="Seg_10157" s="T221">0.3.h:A</ta>
            <ta e="T224" id="Seg_10158" s="T223">pro.h:A</ta>
            <ta e="T226" id="Seg_10159" s="T225">pro:A</ta>
            <ta e="T230" id="Seg_10160" s="T229">np:P</ta>
            <ta e="T231" id="Seg_10161" s="T230">pro:A</ta>
            <ta e="T234" id="Seg_10162" s="T233">pro:A</ta>
            <ta e="T237" id="Seg_10163" s="T236">np:Ins</ta>
            <ta e="T240" id="Seg_10164" s="T239">pro:A</ta>
            <ta e="T250" id="Seg_10165" s="T249">np.h:A</ta>
            <ta e="T251" id="Seg_10166" s="T250">np:G</ta>
            <ta e="T254" id="Seg_10167" s="T253">np:P</ta>
            <ta e="T255" id="Seg_10168" s="T254">0.3.h:A</ta>
            <ta e="T257" id="Seg_10169" s="T256">np:G</ta>
            <ta e="T258" id="Seg_10170" s="T257">0.3.h:A</ta>
            <ta e="T259" id="Seg_10171" s="T258">pro.h:A</ta>
            <ta e="T261" id="Seg_10172" s="T260">np:G</ta>
            <ta e="T262" id="Seg_10173" s="T261">0.1.h:E</ta>
            <ta e="T264" id="Seg_10174" s="T263">np:Th</ta>
            <ta e="T266" id="Seg_10175" s="T265">pro.h:A</ta>
            <ta e="T268" id="Seg_10176" s="T267">0.1.h:A</ta>
            <ta e="T269" id="Seg_10177" s="T268">adv:Time</ta>
            <ta e="T271" id="Seg_10178" s="T270">0.1.h:A</ta>
            <ta e="T272" id="Seg_10179" s="T271">pro:A</ta>
            <ta e="T275" id="Seg_10180" s="T274">np:G</ta>
            <ta e="T277" id="Seg_10181" s="T276">adv:Time</ta>
            <ta e="T278" id="Seg_10182" s="T277">pro:A</ta>
            <ta e="T282" id="Seg_10183" s="T281">0.3:A</ta>
            <ta e="T283" id="Seg_10184" s="T282">pro.h:A</ta>
            <ta e="T286" id="Seg_10185" s="T285">np.h:R</ta>
            <ta e="T287" id="Seg_10186" s="T286">0.1.h:A</ta>
            <ta e="T288" id="Seg_10187" s="T287">pro.h:A</ta>
            <ta e="T290" id="Seg_10188" s="T289">np:Ins</ta>
            <ta e="T293" id="Seg_10189" s="T292">0.3.h:A</ta>
            <ta e="T296" id="Seg_10190" s="T295">pro.h:A</ta>
            <ta e="T297" id="Seg_10191" s="T296">adv:Time</ta>
            <ta e="T298" id="Seg_10192" s="T297">np:P</ta>
            <ta e="T301" id="Seg_10193" s="T300">0.1.h:A</ta>
            <ta e="T302" id="Seg_10194" s="T301">pro.h:R</ta>
            <ta e="T303" id="Seg_10195" s="T302">0.3.h:A</ta>
            <ta e="T305" id="Seg_10196" s="T304">np:Th</ta>
            <ta e="T306" id="Seg_10197" s="T866">np.h:A</ta>
            <ta e="T308" id="Seg_10198" s="T307">pro.h:A</ta>
            <ta e="T309" id="Seg_10199" s="T308">adv:L</ta>
            <ta e="T311" id="Seg_10200" s="T310">pro.h:A</ta>
            <ta e="T316" id="Seg_10201" s="T315">pro.h:A</ta>
            <ta e="T318" id="Seg_10202" s="T317">pro.h:Th</ta>
            <ta e="T320" id="Seg_10203" s="T319">pro.h:A</ta>
            <ta e="T324" id="Seg_10204" s="T323">np.h:A</ta>
            <ta e="T327" id="Seg_10205" s="T326">0.3:Th</ta>
            <ta e="T328" id="Seg_10206" s="T327">np:Th</ta>
            <ta e="T332" id="Seg_10207" s="T331">0.2.h:A</ta>
            <ta e="T333" id="Seg_10208" s="T332">np:Poss</ta>
            <ta e="T334" id="Seg_10209" s="T333">np:Th</ta>
            <ta e="T340" id="Seg_10210" s="T339">0.2.h:A</ta>
            <ta e="T341" id="Seg_10211" s="T340">adv:Time</ta>
            <ta e="T342" id="Seg_10212" s="T341">np:P</ta>
            <ta e="T343" id="Seg_10213" s="T342">0.2.h:A</ta>
            <ta e="T344" id="Seg_10214" s="T865">np:P</ta>
            <ta e="T345" id="Seg_10215" s="T344">0.2.h:A</ta>
            <ta e="T350" id="Seg_10216" s="T349">np.h:Th</ta>
            <ta e="T355" id="Seg_10217" s="T354">np:Th</ta>
            <ta e="T358" id="Seg_10218" s="T357">np:Th</ta>
            <ta e="T361" id="Seg_10219" s="T360">np:Th</ta>
            <ta e="T364" id="Seg_10220" s="T363">np:Th</ta>
            <ta e="T367" id="Seg_10221" s="T366">np.h:Th</ta>
            <ta e="T370" id="Seg_10222" s="T369">pro.h:A</ta>
            <ta e="T372" id="Seg_10223" s="T371">pro:G</ta>
            <ta e="T375" id="Seg_10224" s="T374">pro.h:A</ta>
            <ta e="T378" id="Seg_10225" s="T377">0.2.h:A</ta>
            <ta e="T380" id="Seg_10226" s="T379">np.h:Poss</ta>
            <ta e="T381" id="Seg_10227" s="T380">np:Ins</ta>
            <ta e="T383" id="Seg_10228" s="T382">pro.h:Poss</ta>
            <ta e="T384" id="Seg_10229" s="T383">np:Ins</ta>
            <ta e="T385" id="Seg_10230" s="T384">0.2.h:A</ta>
            <ta e="T386" id="Seg_10231" s="T385">pro.h:Th</ta>
            <ta e="T391" id="Seg_10232" s="T390">0.1.h:Th</ta>
            <ta e="T393" id="Seg_10233" s="T392">pro.h:P</ta>
            <ta e="T398" id="Seg_10234" s="T397">pro.h:Th</ta>
            <ta e="T400" id="Seg_10235" s="T399">pro.h:P</ta>
            <ta e="T403" id="Seg_10236" s="T402">np.h:Th</ta>
            <ta e="T406" id="Seg_10237" s="T405">np.h:Th</ta>
            <ta e="T407" id="Seg_10238" s="T406">pro:L</ta>
            <ta e="T410" id="Seg_10239" s="T409">np.h:Th</ta>
            <ta e="T411" id="Seg_10240" s="T410">adv:L</ta>
            <ta e="T413" id="Seg_10241" s="T412">adv:Time</ta>
            <ta e="T420" id="Seg_10242" s="T419">adv:Time</ta>
            <ta e="T422" id="Seg_10243" s="T421">n:Time</ta>
            <ta e="T423" id="Seg_10244" s="T422">np:Th</ta>
            <ta e="T426" id="Seg_10245" s="T425">adv:Time</ta>
            <ta e="T428" id="Seg_10246" s="T427">0.3:Th</ta>
            <ta e="T429" id="Seg_10247" s="T428">np.h:A</ta>
            <ta e="T431" id="Seg_10248" s="T430">np:L</ta>
            <ta e="T433" id="Seg_10249" s="T432">np.h:A</ta>
            <ta e="T434" id="Seg_10250" s="T433">np:L</ta>
            <ta e="T437" id="Seg_10251" s="T436">np.h:E</ta>
            <ta e="T439" id="Seg_10252" s="T438">0.3.h:E</ta>
            <ta e="T441" id="Seg_10253" s="T440">np.h:A</ta>
            <ta e="T444" id="Seg_10254" s="T443">np:P</ta>
            <ta e="T447" id="Seg_10255" s="T446">np.h:Th</ta>
            <ta e="T452" id="Seg_10256" s="T451">adv:L</ta>
            <ta e="T453" id="Seg_10257" s="T452">0.3.h:A</ta>
            <ta e="T455" id="Seg_10258" s="T454">np:G</ta>
            <ta e="T457" id="Seg_10259" s="T456">np:P</ta>
            <ta e="T460" id="Seg_10260" s="T459">np:Th</ta>
            <ta e="T463" id="Seg_10261" s="T462">pro.h:Th</ta>
            <ta e="T467" id="Seg_10262" s="T466">np.h:Th</ta>
            <ta e="T471" id="Seg_10263" s="T470">pro.h:Th</ta>
            <ta e="T472" id="Seg_10264" s="T471">0.3.h:A</ta>
            <ta e="T473" id="Seg_10265" s="T472">pro:G</ta>
            <ta e="T474" id="Seg_10266" s="T473">np:Th</ta>
            <ta e="T482" id="Seg_10267" s="T481">0.1.h:A</ta>
            <ta e="T485" id="Seg_10268" s="T484">np.h:A</ta>
            <ta e="T503" id="Seg_10269" s="T502">0.3.h:A</ta>
            <ta e="T505" id="Seg_10270" s="T504">np.h:P</ta>
            <ta e="T508" id="Seg_10271" s="T507">np:P</ta>
            <ta e="T512" id="Seg_10272" s="T511">np:Ins</ta>
            <ta e="T514" id="Seg_10273" s="T513">adv:L</ta>
            <ta e="T515" id="Seg_10274" s="T514">pro:Th</ta>
            <ta e="T518" id="Seg_10275" s="T517">np:P</ta>
            <ta e="T520" id="Seg_10276" s="T519">adv:L</ta>
            <ta e="T521" id="Seg_10277" s="T520">adv:Time</ta>
            <ta e="T522" id="Seg_10278" s="T521">0.3:A</ta>
            <ta e="T524" id="Seg_10279" s="T523">np:G</ta>
            <ta e="T527" id="Seg_10280" s="T526">np:Th</ta>
            <ta e="T530" id="Seg_10281" s="T529">pro.h:A</ta>
            <ta e="T533" id="Seg_10282" s="T532">np:Ins</ta>
            <ta e="T537" id="Seg_10283" s="T536">np:Poss</ta>
            <ta e="T538" id="Seg_10284" s="T537">np:L</ta>
            <ta e="T540" id="Seg_10285" s="T539">np:Th</ta>
            <ta e="T542" id="Seg_10286" s="T541">0.3.h:A</ta>
            <ta e="T543" id="Seg_10287" s="T542">np:Th</ta>
            <ta e="T544" id="Seg_10288" s="T543">0.3.h:A</ta>
            <ta e="T546" id="Seg_10289" s="T545">0.3.h:A</ta>
            <ta e="T549" id="Seg_10290" s="T548">np:Th</ta>
            <ta e="T553" id="Seg_10291" s="T552">np:Th</ta>
            <ta e="T555" id="Seg_10292" s="T554">0.3.h:A</ta>
            <ta e="T557" id="Seg_10293" s="T556">np:L</ta>
            <ta e="T561" id="Seg_10294" s="T560">np:L</ta>
            <ta e="T570" id="Seg_10295" s="T569">np:Th</ta>
            <ta e="T572" id="Seg_10296" s="T571">pro.h:Poss</ta>
            <ta e="T573" id="Seg_10297" s="T572">np.h:A</ta>
            <ta e="T576" id="Seg_10298" s="T575">pro:Th</ta>
            <ta e="T580" id="Seg_10299" s="T579">np.h:A</ta>
            <ta e="T585" id="Seg_10300" s="T584">0.2.h:A</ta>
            <ta e="T591" id="Seg_10301" s="T590">adv:Time</ta>
            <ta e="T593" id="Seg_10302" s="T592">np.h:A 0.1.h:Poss</ta>
            <ta e="T595" id="Seg_10303" s="T594">np:P</ta>
            <ta e="T598" id="Seg_10304" s="T597">adv:Time</ta>
            <ta e="T600" id="Seg_10305" s="T599">np:P</ta>
            <ta e="T601" id="Seg_10306" s="T600">0.3.h:A</ta>
            <ta e="T603" id="Seg_10307" s="T602">adv:L</ta>
            <ta e="T604" id="Seg_10308" s="T603">0.3.h:A</ta>
            <ta e="T606" id="Seg_10309" s="T605">0.3.h:A</ta>
            <ta e="T619" id="Seg_10310" s="T618">0.3:Th</ta>
            <ta e="T621" id="Seg_10311" s="T620">np:Th</ta>
            <ta e="T625" id="Seg_10312" s="T624">np:Th</ta>
            <ta e="T629" id="Seg_10313" s="T628">0.1.h:A</ta>
            <ta e="T631" id="Seg_10314" s="T630">0.1.h:A</ta>
            <ta e="T632" id="Seg_10315" s="T631">np:L</ta>
            <ta e="T633" id="Seg_10316" s="T632">0.1.h:A</ta>
            <ta e="T634" id="Seg_10317" s="T633">np:Th</ta>
            <ta e="T636" id="Seg_10318" s="T635">0.1.h:A</ta>
            <ta e="T637" id="Seg_10319" s="T636">np:G</ta>
            <ta e="T638" id="Seg_10320" s="T637">0.1.h:A</ta>
            <ta e="T639" id="Seg_10321" s="T638">np:Th</ta>
            <ta e="T640" id="Seg_10322" s="T639">0.1.h:A</ta>
            <ta e="T641" id="Seg_10323" s="T640">np:L</ta>
            <ta e="T642" id="Seg_10324" s="T641">0.1.h:A</ta>
            <ta e="T643" id="Seg_10325" s="T642">np:Th</ta>
            <ta e="T644" id="Seg_10326" s="T643">0.1.h:A</ta>
            <ta e="T645" id="Seg_10327" s="T644">pro.h:Poss</ta>
            <ta e="T646" id="Seg_10328" s="T645">np.h:A</ta>
            <ta e="T647" id="Seg_10329" s="T646">np:G</ta>
            <ta e="T650" id="Seg_10330" s="T649">0.3.h:A</ta>
            <ta e="T651" id="Seg_10331" s="T650">np:P</ta>
            <ta e="T654" id="Seg_10332" s="T653">0.3.h:A</ta>
            <ta e="T655" id="Seg_10333" s="T654">pro.h:A</ta>
            <ta e="T658" id="Seg_10334" s="T657">0.1.h:A</ta>
            <ta e="T660" id="Seg_10335" s="T659">0.1.h:A</ta>
            <ta e="T662" id="Seg_10336" s="T661">0.1.h:A</ta>
            <ta e="T663" id="Seg_10337" s="T662">pro.h:A</ta>
            <ta e="T668" id="Seg_10338" s="T667">np:Th</ta>
            <ta e="T669" id="Seg_10339" s="T668">adv:L</ta>
            <ta e="T671" id="Seg_10340" s="T670">np:Th</ta>
            <ta e="T672" id="Seg_10341" s="T671">0.1.h:A</ta>
            <ta e="T673" id="Seg_10342" s="T672">np:P</ta>
            <ta e="T674" id="Seg_10343" s="T673">0.1.h:A</ta>
            <ta e="T675" id="Seg_10344" s="T674">np.h:A</ta>
            <ta e="T677" id="Seg_10345" s="T676">np:P</ta>
            <ta e="T680" id="Seg_10346" s="T679">pro.h:P</ta>
            <ta e="T681" id="Seg_10347" s="T680">np:L</ta>
            <ta e="T682" id="Seg_10348" s="T681">0.3.h:A</ta>
            <ta e="T686" id="Seg_10349" s="T685">np:Th 0.3:Poss</ta>
            <ta e="T689" id="Seg_10350" s="T688">np:Th</ta>
            <ta e="T693" id="Seg_10351" s="T692">pro:Th</ta>
            <ta e="T697" id="Seg_10352" s="T696">np:Th 0.3:Poss</ta>
            <ta e="T701" id="Seg_10353" s="T700">np:Th</ta>
            <ta e="T709" id="Seg_10354" s="T708">np:Th</ta>
            <ta e="T711" id="Seg_10355" s="T710">np:Th</ta>
            <ta e="T713" id="Seg_10356" s="T712">pro.h:Poss</ta>
            <ta e="T714" id="Seg_10357" s="T713">np.h:A</ta>
            <ta e="T716" id="Seg_10358" s="T715">np.h:A</ta>
            <ta e="T718" id="Seg_10359" s="T717">np.h:Com</ta>
            <ta e="T719" id="Seg_10360" s="T718">np.h:A 0.1.h:Poss</ta>
            <ta e="T721" id="Seg_10361" s="T720">np.h:Com</ta>
            <ta e="T724" id="Seg_10362" s="T723">pro.h:Poss</ta>
            <ta e="T725" id="Seg_10363" s="T724">np.h:A</ta>
            <ta e="T727" id="Seg_10364" s="T726">np.h:A 0.1.h:Poss</ta>
            <ta e="T729" id="Seg_10365" s="T728">np.h:Com</ta>
            <ta e="T730" id="Seg_10366" s="T729">np.h:A 0.1.h:Poss</ta>
            <ta e="T732" id="Seg_10367" s="T731">np.h:Com</ta>
            <ta e="T733" id="Seg_10368" s="T732">np.h:A 0.1.h:Poss</ta>
            <ta e="T734" id="Seg_10369" s="T733">np.h:Com</ta>
            <ta e="T737" id="Seg_10370" s="T736">np.h:A</ta>
            <ta e="T738" id="Seg_10371" s="T737">pro.h:Com</ta>
            <ta e="T741" id="Seg_10372" s="T740">pro.h:B</ta>
            <ta e="T744" id="Seg_10373" s="T743">pro.h:A</ta>
            <ta e="T748" id="Seg_10374" s="T747">pro.h:Poss</ta>
            <ta e="T749" id="Seg_10375" s="T748">np.h:Th</ta>
            <ta e="T753" id="Seg_10376" s="T752">pro.h:A</ta>
            <ta e="T754" id="Seg_10377" s="T753">pro.h:R</ta>
            <ta e="T756" id="Seg_10378" s="T755">np:R</ta>
            <ta e="T757" id="Seg_10379" s="T756">0.2.h:A</ta>
            <ta e="T758" id="Seg_10380" s="T757">0.2.h:A</ta>
            <ta e="T761" id="Seg_10381" s="T760">0.2.h:A</ta>
            <ta e="T762" id="Seg_10382" s="T761">pro:G</ta>
            <ta e="T763" id="Seg_10383" s="T762">pro.h:A</ta>
            <ta e="T766" id="Seg_10384" s="T765">0.2.h:A</ta>
            <ta e="T767" id="Seg_10385" s="T766">pro:G</ta>
            <ta e="T768" id="Seg_10386" s="T767">np:Th</ta>
            <ta e="T769" id="Seg_10387" s="T768">0.2.h:A</ta>
            <ta e="T770" id="Seg_10388" s="T769">0.2.h:A</ta>
            <ta e="T771" id="Seg_10389" s="T770">pro.h:P</ta>
            <ta e="T772" id="Seg_10390" s="T771">pro.h:A</ta>
            <ta e="T773" id="Seg_10391" s="T772">adv:Time</ta>
            <ta e="T774" id="Seg_10392" s="T773">np:P</ta>
            <ta e="T776" id="Seg_10393" s="T775">np:A</ta>
            <ta e="T779" id="Seg_10394" s="T778">0.1.h:A</ta>
            <ta e="T780" id="Seg_10395" s="T779">adv:Time</ta>
            <ta e="T782" id="Seg_10396" s="T781">0.1.h:A</ta>
            <ta e="T783" id="Seg_10397" s="T782">adv:Time</ta>
            <ta e="T784" id="Seg_10398" s="T783">0.1.h:A</ta>
            <ta e="T785" id="Seg_10399" s="T784">np:Th</ta>
            <ta e="T787" id="Seg_10400" s="T786">np:G</ta>
            <ta e="T791" id="Seg_10401" s="T790">np:Th</ta>
            <ta e="T792" id="Seg_10402" s="T791">0.1.h:A</ta>
            <ta e="T793" id="Seg_10403" s="T792">pro.h:A</ta>
            <ta e="T794" id="Seg_10404" s="T793">adv:Time</ta>
            <ta e="T800" id="Seg_10405" s="T799">0.1.h:A</ta>
            <ta e="T802" id="Seg_10406" s="T801">0.1.h:E</ta>
            <ta e="T805" id="Seg_10407" s="T804">np:Th</ta>
            <ta e="T807" id="Seg_10408" s="T806">pro.h:E</ta>
            <ta e="T809" id="Seg_10409" s="T808">pro.h:E</ta>
            <ta e="T810" id="Seg_10410" s="T809">pro.h:Th</ta>
            <ta e="T815" id="Seg_10411" s="T814">pro.h:A</ta>
            <ta e="T821" id="Seg_10412" s="T820">pro.h:A</ta>
            <ta e="T828" id="Seg_10413" s="T827">pro:G</ta>
            <ta e="T831" id="Seg_10414" s="T830">0.3.h:A</ta>
            <ta e="T832" id="Seg_10415" s="T831">pro.h:B</ta>
            <ta e="T833" id="Seg_10416" s="T832">pro.h:Th</ta>
            <ta e="T836" id="Seg_10417" s="T835">0.1.h:Poss</ta>
            <ta e="T837" id="Seg_10418" s="T836">0.1.h:Poss</ta>
            <ta e="T838" id="Seg_10419" s="T837">0.1.h:Poss</ta>
            <ta e="T839" id="Seg_10420" s="T838">0.1.h:Poss</ta>
            <ta e="T840" id="Seg_10421" s="T839">0.1.h:Poss</ta>
            <ta e="T841" id="Seg_10422" s="T840">0.1.h:Poss</ta>
            <ta e="T842" id="Seg_10423" s="T841">0.1.h:Poss</ta>
            <ta e="T843" id="Seg_10424" s="T842">0.1.h:Poss</ta>
            <ta e="T844" id="Seg_10425" s="T843">0.1.h:Poss</ta>
            <ta e="T845" id="Seg_10426" s="T844">0.1.h:Poss</ta>
            <ta e="T847" id="Seg_10427" s="T846">0.1.h:Poss</ta>
            <ta e="T848" id="Seg_10428" s="T847">0.1.h:Poss</ta>
            <ta e="T849" id="Seg_10429" s="T848">0.1.h:Poss</ta>
            <ta e="T851" id="Seg_10430" s="T850">0.1.h:Poss</ta>
            <ta e="T852" id="Seg_10431" s="T851">0.3.h:Poss</ta>
            <ta e="T853" id="Seg_10432" s="T852">0.1.h:Poss</ta>
            <ta e="T854" id="Seg_10433" s="T853">0.3.h:Poss</ta>
            <ta e="T855" id="Seg_10434" s="T854">0.1.h:Poss</ta>
            <ta e="T856" id="Seg_10435" s="T855">0.3.h:Poss</ta>
            <ta e="T858" id="Seg_10436" s="T857">0.3.h:Poss</ta>
            <ta e="T861" id="Seg_10437" s="T860">0.3.h:Poss</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-PKZ">
            <ta e="T6" id="Seg_10438" s="T5">v:pred 0.2.h:S</ta>
            <ta e="T9" id="Seg_10439" s="T8">np:O</ta>
            <ta e="T10" id="Seg_10440" s="T9">v:pred 0.2.h:S</ta>
            <ta e="T12" id="Seg_10441" s="T11">np.h:S</ta>
            <ta e="T14" id="Seg_10442" s="T13">v:pred</ta>
            <ta e="T869" id="Seg_10443" s="T16">conv:pred</ta>
            <ta e="T17" id="Seg_10444" s="T869">v:pred 0.3.h:S</ta>
            <ta e="T20" id="Seg_10445" s="T19">v:pred 0.3.h:S</ta>
            <ta e="T22" id="Seg_10446" s="T21">np.h:S</ta>
            <ta e="T870" id="Seg_10447" s="T22">conv:pred</ta>
            <ta e="T23" id="Seg_10448" s="T870">v:pred</ta>
            <ta e="T27" id="Seg_10449" s="T26">v:pred 0.3.h:S</ta>
            <ta e="T30" id="Seg_10450" s="T29">v:pred 0.3.h:S</ta>
            <ta e="T34" id="Seg_10451" s="T33">v:pred 0.2.h:S</ta>
            <ta e="T35" id="Seg_10452" s="T34">ptcl.neg</ta>
            <ta e="T38" id="Seg_10453" s="T37">ptcl.neg</ta>
            <ta e="T43" id="Seg_10454" s="T42">v:pred 0.2.h:S</ta>
            <ta e="T45" id="Seg_10455" s="T44">np:S</ta>
            <ta e="T46" id="Seg_10456" s="T45">v:pred</ta>
            <ta e="T47" id="Seg_10457" s="T46">np:S</ta>
            <ta e="T48" id="Seg_10458" s="T47">v:pred</ta>
            <ta e="T49" id="Seg_10459" s="T48">pro.h:S</ta>
            <ta e="T51" id="Seg_10460" s="T50">pro:O</ta>
            <ta e="T53" id="Seg_10461" s="T52">v:pred</ta>
            <ta e="T59" id="Seg_10462" s="T58">v:pred</ta>
            <ta e="T60" id="Seg_10463" s="T59">np:S</ta>
            <ta e="T61" id="Seg_10464" s="T60">ptcl:pred</ta>
            <ta e="T67" id="Seg_10465" s="T66">v:pred</ta>
            <ta e="T68" id="Seg_10466" s="T67">pro.h:S</ta>
            <ta e="T70" id="Seg_10467" s="T69">v:pred 0.2.h:S</ta>
            <ta e="T72" id="Seg_10468" s="T71">np:O</ta>
            <ta e="T73" id="Seg_10469" s="T72">np:S</ta>
            <ta e="T74" id="Seg_10470" s="T73">v:pred</ta>
            <ta e="T76" id="Seg_10471" s="T75">pro:O</ta>
            <ta e="T77" id="Seg_10472" s="T76">ptcl.neg</ta>
            <ta e="T78" id="Seg_10473" s="T77">v:pred 0.1.h:S</ta>
            <ta e="T79" id="Seg_10474" s="T78">pro:O</ta>
            <ta e="T80" id="Seg_10475" s="T79">ptcl.neg</ta>
            <ta e="T81" id="Seg_10476" s="T80">v:pred 0.1.h:S</ta>
            <ta e="T84" id="Seg_10477" s="T83">np:S</ta>
            <ta e="T85" id="Seg_10478" s="T84">v:pred</ta>
            <ta e="T87" id="Seg_10479" s="T86">v:pred 0.1.h:S</ta>
            <ta e="T92" id="Seg_10480" s="T91">v:pred 0.1.h:S</ta>
            <ta e="T94" id="Seg_10481" s="T93">v:pred 0.1.h:S</ta>
            <ta e="T96" id="Seg_10482" s="T95">v:pred 0.1.h:S</ta>
            <ta e="T98" id="Seg_10483" s="T97">v:pred 0.1.h:S</ta>
            <ta e="T101" id="Seg_10484" s="T100">v:pred 0.1.h:S</ta>
            <ta e="T103" id="Seg_10485" s="T102">v:pred 0.1.h:S</ta>
            <ta e="T104" id="Seg_10486" s="T103">pro:O</ta>
            <ta e="T107" id="Seg_10487" s="T106">pro:O</ta>
            <ta e="T108" id="Seg_10488" s="T107">ptcl.neg</ta>
            <ta e="T109" id="Seg_10489" s="T108">v:pred 0.1.h:S</ta>
            <ta e="T110" id="Seg_10490" s="T109">pro.h:S</ta>
            <ta e="T111" id="Seg_10491" s="T110">v:pred</ta>
            <ta e="T113" id="Seg_10492" s="T112">pro.h:S</ta>
            <ta e="T114" id="Seg_10493" s="T113">pro.h:O</ta>
            <ta e="T116" id="Seg_10494" s="T115">v:pred</ta>
            <ta e="T117" id="Seg_10495" s="T116">v:pred 0.2.h:S</ta>
            <ta e="T119" id="Seg_10496" s="T118">v:pred 0.2.h:S</ta>
            <ta e="T121" id="Seg_10497" s="T120">pro.h:S</ta>
            <ta e="T123" id="Seg_10498" s="T122">v:pred</ta>
            <ta e="T124" id="Seg_10499" s="T123">v:pred 0.2.h:S</ta>
            <ta e="T129" id="Seg_10500" s="T128">np.h:S</ta>
            <ta e="T130" id="Seg_10501" s="T129">v:pred</ta>
            <ta e="T131" id="Seg_10502" s="T130">np:S</ta>
            <ta e="T134" id="Seg_10503" s="T133">adj:pred</ta>
            <ta e="T135" id="Seg_10504" s="T134">v:pred 0.2.h:S</ta>
            <ta e="T137" id="Seg_10505" s="T136">pro:O</ta>
            <ta e="T138" id="Seg_10506" s="T137">np:O </ta>
            <ta e="T139" id="Seg_10507" s="T138">ptcl.neg</ta>
            <ta e="T140" id="Seg_10508" s="T139">v:pred 0.2.h:S</ta>
            <ta e="T142" id="Seg_10509" s="T140">v:pred 0.2.h:S</ta>
            <ta e="T143" id="Seg_10510" s="T142">pro:O</ta>
            <ta e="T144" id="Seg_10511" s="T143">v:pred 0.2.h:S</ta>
            <ta e="T145" id="Seg_10512" s="T144">pro:O</ta>
            <ta e="T149" id="Seg_10513" s="T148">np:O</ta>
            <ta e="T150" id="Seg_10514" s="T149">v:pred 0.1.h:S</ta>
            <ta e="T153" id="Seg_10515" s="T152">np:O</ta>
            <ta e="T154" id="Seg_10516" s="T153">v:pred 0.1.h:S</ta>
            <ta e="T156" id="Seg_10517" s="T155">np:O</ta>
            <ta e="T157" id="Seg_10518" s="T156">v:pred 0.3.h:S</ta>
            <ta e="T160" id="Seg_10519" s="T159">np:O</ta>
            <ta e="T161" id="Seg_10520" s="T160">v:pred 0.3.h:S</ta>
            <ta e="T164" id="Seg_10521" s="T163">v:pred 0.1.h:S</ta>
            <ta e="T165" id="Seg_10522" s="T164">pro.h:S</ta>
            <ta e="T167" id="Seg_10523" s="T166">adj:pred</ta>
            <ta e="T168" id="Seg_10524" s="T167">cop</ta>
            <ta e="T170" id="Seg_10525" s="T169">v:pred 0.1.h:S</ta>
            <ta e="T171" id="Seg_10526" s="T170">np:O</ta>
            <ta e="T172" id="Seg_10527" s="T171">v:pred 0.3.h:S</ta>
            <ta e="T176" id="Seg_10528" s="T175">np.h:S</ta>
            <ta e="T177" id="Seg_10529" s="T176">v:pred</ta>
            <ta e="T179" id="Seg_10530" s="T178">pro.h:S</ta>
            <ta e="T181" id="Seg_10531" s="T180">v:pred</ta>
            <ta e="T183" id="Seg_10532" s="T182">pro.h:S</ta>
            <ta e="T185" id="Seg_10533" s="T184">v:pred</ta>
            <ta e="T186" id="Seg_10534" s="T185">v:pred 0.3.h:S</ta>
            <ta e="T189" id="Seg_10535" s="T188">ptcl.neg</ta>
            <ta e="T190" id="Seg_10536" s="T189">v:pred 0.3.h:S</ta>
            <ta e="T192" id="Seg_10537" s="T191">pro.h:S</ta>
            <ta e="T195" id="Seg_10538" s="T194">v:pred</ta>
            <ta e="T197" id="Seg_10539" s="T196">pro.h:S</ta>
            <ta e="T199" id="Seg_10540" s="T198">ptcl.neg</ta>
            <ta e="T200" id="Seg_10541" s="T199">v:pred</ta>
            <ta e="T210" id="Seg_10542" s="T209">pro.h:S</ta>
            <ta e="T213" id="Seg_10543" s="T212">v:pred</ta>
            <ta e="T214" id="Seg_10544" s="T213">pro.h:S</ta>
            <ta e="T215" id="Seg_10545" s="T214">ptcl.neg</ta>
            <ta e="T216" id="Seg_10546" s="T215">v:pred</ta>
            <ta e="T217" id="Seg_10547" s="T216">np.h:S</ta>
            <ta e="T219" id="Seg_10548" s="T218">np.h:S</ta>
            <ta e="T220" id="Seg_10549" s="T219">v:pred</ta>
            <ta e="T222" id="Seg_10550" s="T221">v:pred 0.3.h:S</ta>
            <ta e="T224" id="Seg_10551" s="T223">pro.h:S</ta>
            <ta e="T225" id="Seg_10552" s="T224">v:pred</ta>
            <ta e="T226" id="Seg_10553" s="T225">pro:S</ta>
            <ta e="T229" id="Seg_10554" s="T228">v:pred</ta>
            <ta e="T230" id="Seg_10555" s="T229">np:O</ta>
            <ta e="T231" id="Seg_10556" s="T230">pro:S</ta>
            <ta e="T232" id="Seg_10557" s="T231">ptcl.neg</ta>
            <ta e="T233" id="Seg_10558" s="T232">v:pred</ta>
            <ta e="T234" id="Seg_10559" s="T233">pro:S</ta>
            <ta e="T239" id="Seg_10560" s="T238">v:pred</ta>
            <ta e="T240" id="Seg_10561" s="T239">pro:S</ta>
            <ta e="T241" id="Seg_10562" s="T240">ptcl.neg</ta>
            <ta e="T242" id="Seg_10563" s="T241">v:pred</ta>
            <ta e="T250" id="Seg_10564" s="T249">np.h:S</ta>
            <ta e="T252" id="Seg_10565" s="T251">v:pred</ta>
            <ta e="T254" id="Seg_10566" s="T253">np:O</ta>
            <ta e="T255" id="Seg_10567" s="T254">v:pred 0.3.h:S</ta>
            <ta e="T258" id="Seg_10568" s="T257">v:pred 0.3.h:S</ta>
            <ta e="T259" id="Seg_10569" s="T258">pro.h:S</ta>
            <ta e="T260" id="Seg_10570" s="T259">v:pred</ta>
            <ta e="T262" id="Seg_10571" s="T261">v:pred 0.1.h:S</ta>
            <ta e="T264" id="Seg_10572" s="T263">np:S</ta>
            <ta e="T265" id="Seg_10573" s="T264">v:pred</ta>
            <ta e="T266" id="Seg_10574" s="T265">pro.h:S</ta>
            <ta e="T267" id="Seg_10575" s="T266">v:pred</ta>
            <ta e="T268" id="Seg_10576" s="T267">v:pred 0.1.h:S</ta>
            <ta e="T271" id="Seg_10577" s="T270">v:pred 0.1.h:S</ta>
            <ta e="T272" id="Seg_10578" s="T271">pro:S</ta>
            <ta e="T274" id="Seg_10579" s="T273">v:pred</ta>
            <ta e="T278" id="Seg_10580" s="T277">pro:S</ta>
            <ta e="T280" id="Seg_10581" s="T279">v:pred</ta>
            <ta e="T282" id="Seg_10582" s="T281">v:pred 0.3:S</ta>
            <ta e="T283" id="Seg_10583" s="T282">pro.h:S</ta>
            <ta e="T284" id="Seg_10584" s="T283">v:pred</ta>
            <ta e="T287" id="Seg_10585" s="T286">v:pred 0.1.h:S</ta>
            <ta e="T288" id="Seg_10586" s="T287">pro.h:S</ta>
            <ta e="T289" id="Seg_10587" s="T288">v:pred</ta>
            <ta e="T293" id="Seg_10588" s="T292">v:pred 0.3.h:S</ta>
            <ta e="T296" id="Seg_10589" s="T295">pro.h:S</ta>
            <ta e="T298" id="Seg_10590" s="T297">np:O</ta>
            <ta e="T299" id="Seg_10591" s="T298">v:pred</ta>
            <ta e="T301" id="Seg_10592" s="T300">v:pred 0.1.h:S</ta>
            <ta e="T303" id="Seg_10593" s="T302">v:pred 0.3.h:S</ta>
            <ta e="T305" id="Seg_10594" s="T304">np:O</ta>
            <ta e="T306" id="Seg_10595" s="T866">np.h:S</ta>
            <ta e="T307" id="Seg_10596" s="T306">v:pred</ta>
            <ta e="T308" id="Seg_10597" s="T307">pro.h:S</ta>
            <ta e="T310" id="Seg_10598" s="T309">v:pred</ta>
            <ta e="T311" id="Seg_10599" s="T310">pro.h:S</ta>
            <ta e="T314" id="Seg_10600" s="T312">v:pred</ta>
            <ta e="T316" id="Seg_10601" s="T315">pro.h:S</ta>
            <ta e="T318" id="Seg_10602" s="T317">pro.h:O</ta>
            <ta e="T319" id="Seg_10603" s="T318">v:pred</ta>
            <ta e="T320" id="Seg_10604" s="T319">pro.h:S</ta>
            <ta e="T321" id="Seg_10605" s="T320">v:pred</ta>
            <ta e="T324" id="Seg_10606" s="T323">np.h:S</ta>
            <ta e="T325" id="Seg_10607" s="T324">v:pred</ta>
            <ta e="T327" id="Seg_10608" s="T326">v:pred 0.3:S</ta>
            <ta e="T328" id="Seg_10609" s="T327">np:S</ta>
            <ta e="T330" id="Seg_10610" s="T329">v:pred</ta>
            <ta e="T332" id="Seg_10611" s="T331">v:pred 0.2.h:S</ta>
            <ta e="T334" id="Seg_10612" s="T333">np:S</ta>
            <ta e="T336" id="Seg_10613" s="T335">adj:pred</ta>
            <ta e="T340" id="Seg_10614" s="T339">v:pred 0.2.h:S</ta>
            <ta e="T342" id="Seg_10615" s="T341">np:O</ta>
            <ta e="T343" id="Seg_10616" s="T342">v:pred 0.2.h:S</ta>
            <ta e="T344" id="Seg_10617" s="T865">np:O</ta>
            <ta e="T345" id="Seg_10618" s="T344">v:pred 0.2.h:S</ta>
            <ta e="T350" id="Seg_10619" s="T349">np.h:S</ta>
            <ta e="T352" id="Seg_10620" s="T351">adj:pred</ta>
            <ta e="T354" id="Seg_10621" s="T353">adj:pred</ta>
            <ta e="T355" id="Seg_10622" s="T354">np:S</ta>
            <ta e="T357" id="Seg_10623" s="T356">adj:pred</ta>
            <ta e="T358" id="Seg_10624" s="T357">np:S</ta>
            <ta e="T359" id="Seg_10625" s="T358">ptcl.neg</ta>
            <ta e="T360" id="Seg_10626" s="T359">adj:pred</ta>
            <ta e="T361" id="Seg_10627" s="T360">np:S</ta>
            <ta e="T362" id="Seg_10628" s="T361">adj:pred</ta>
            <ta e="T364" id="Seg_10629" s="T363">np:S</ta>
            <ta e="T365" id="Seg_10630" s="T364">ptcl.neg</ta>
            <ta e="T366" id="Seg_10631" s="T365">adj:pred</ta>
            <ta e="T367" id="Seg_10632" s="T366">np.h:S</ta>
            <ta e="T369" id="Seg_10633" s="T368">v:pred</ta>
            <ta e="T370" id="Seg_10634" s="T369">pro.h:S</ta>
            <ta e="T374" id="Seg_10635" s="T373">v:pred</ta>
            <ta e="T375" id="Seg_10636" s="T374">pro.h:S</ta>
            <ta e="T377" id="Seg_10637" s="T376">v:pred</ta>
            <ta e="T378" id="Seg_10638" s="T377">v:pred 0.2.h:S</ta>
            <ta e="T385" id="Seg_10639" s="T384">v:pred 0.2.h:S</ta>
            <ta e="T386" id="Seg_10640" s="T385">pro.h:S</ta>
            <ta e="T388" id="Seg_10641" s="T387">v:pred</ta>
            <ta e="T391" id="Seg_10642" s="T390">v:pred 0.1.h:S</ta>
            <ta e="T393" id="Seg_10643" s="T392">pro.h:S</ta>
            <ta e="T395" id="Seg_10644" s="T394">v:pred</ta>
            <ta e="T398" id="Seg_10645" s="T397">pro.h:S</ta>
            <ta e="T399" id="Seg_10646" s="T398">v:pred</ta>
            <ta e="T400" id="Seg_10647" s="T399">pro.h:S</ta>
            <ta e="T401" id="Seg_10648" s="T400">v:pred</ta>
            <ta e="T403" id="Seg_10649" s="T402">np.h:S</ta>
            <ta e="T405" id="Seg_10650" s="T404">v:pred</ta>
            <ta e="T406" id="Seg_10651" s="T405">np.h:S</ta>
            <ta e="T408" id="Seg_10652" s="T407">v:pred</ta>
            <ta e="T410" id="Seg_10653" s="T409">np.h:S</ta>
            <ta e="T412" id="Seg_10654" s="T411">v:pred</ta>
            <ta e="T423" id="Seg_10655" s="T422">np:S</ta>
            <ta e="T424" id="Seg_10656" s="T423">v:pred</ta>
            <ta e="T428" id="Seg_10657" s="T427">v:pred 0.3:S</ta>
            <ta e="T429" id="Seg_10658" s="T428">np.h:S</ta>
            <ta e="T430" id="Seg_10659" s="T429">v:pred</ta>
            <ta e="T433" id="Seg_10660" s="T432">np.h:S</ta>
            <ta e="T435" id="Seg_10661" s="T434">v:pred</ta>
            <ta e="T437" id="Seg_10662" s="T436">np.h:S</ta>
            <ta e="T438" id="Seg_10663" s="T437">v:pred</ta>
            <ta e="T439" id="Seg_10664" s="T438">v:pred 0.3.h:S</ta>
            <ta e="T441" id="Seg_10665" s="T440">np.h:S</ta>
            <ta e="T442" id="Seg_10666" s="T441">v:pred</ta>
            <ta e="T443" id="Seg_10667" s="T442">ptcl:pred</ta>
            <ta e="T444" id="Seg_10668" s="T443">np:O</ta>
            <ta e="T447" id="Seg_10669" s="T446">np.h:O</ta>
            <ta e="T448" id="Seg_10670" s="T447">v:pred</ta>
            <ta e="T451" id="Seg_10671" s="T450">ptcl:pred</ta>
            <ta e="T453" id="Seg_10672" s="T452">v:pred 0.3.h:S</ta>
            <ta e="T454" id="Seg_10673" s="T453">ptcl:pred</ta>
            <ta e="T457" id="Seg_10674" s="T456">np:O</ta>
            <ta e="T458" id="Seg_10675" s="T457">v:pred</ta>
            <ta e="T460" id="Seg_10676" s="T459">np:S</ta>
            <ta e="T461" id="Seg_10677" s="T460">v:pred</ta>
            <ta e="T463" id="Seg_10678" s="T462">pro.h:S</ta>
            <ta e="T464" id="Seg_10679" s="T463">adj:pred</ta>
            <ta e="T465" id="Seg_10680" s="T464">cop</ta>
            <ta e="T467" id="Seg_10681" s="T466">np.h:S</ta>
            <ta e="T468" id="Seg_10682" s="T467">adj:pred</ta>
            <ta e="T469" id="Seg_10683" s="T468">cop</ta>
            <ta e="T471" id="Seg_10684" s="T470">pro.h:O</ta>
            <ta e="T472" id="Seg_10685" s="T471">v:pred 0.3.h:S</ta>
            <ta e="T474" id="Seg_10686" s="T473">np:O</ta>
            <ta e="T475" id="Seg_10687" s="T474">v:pred</ta>
            <ta e="T480" id="Seg_10688" s="T479">adj:pred</ta>
            <ta e="T481" id="Seg_10689" s="T480">cop</ta>
            <ta e="T482" id="Seg_10690" s="T481">v:pred 0.1.h:S</ta>
            <ta e="T485" id="Seg_10691" s="T484">np.h:S</ta>
            <ta e="T488" id="Seg_10692" s="T487">v:pred</ta>
            <ta e="T493" id="Seg_10693" s="T492">adj:pred</ta>
            <ta e="T503" id="Seg_10694" s="T502">v:pred 0.3.h:S</ta>
            <ta e="T505" id="Seg_10695" s="T504">np.h:S</ta>
            <ta e="T506" id="Seg_10696" s="T505">v:pred</ta>
            <ta e="T507" id="Seg_10697" s="T506">ptcl:pred</ta>
            <ta e="T508" id="Seg_10698" s="T507">np:O</ta>
            <ta e="T515" id="Seg_10699" s="T514">pro:O</ta>
            <ta e="T516" id="Seg_10700" s="T515">v:pred</ta>
            <ta e="T518" id="Seg_10701" s="T517">np:O</ta>
            <ta e="T519" id="Seg_10702" s="T518">v:pred</ta>
            <ta e="T522" id="Seg_10703" s="T521">v:pred 0.3:S</ta>
            <ta e="T527" id="Seg_10704" s="T526">np:S</ta>
            <ta e="T528" id="Seg_10705" s="T527">v:pred</ta>
            <ta e="T530" id="Seg_10706" s="T529">pro.h:S</ta>
            <ta e="T533" id="Seg_10707" s="T532">np:O</ta>
            <ta e="T535" id="Seg_10708" s="T534">v:pred</ta>
            <ta e="T540" id="Seg_10709" s="T539">np:O</ta>
            <ta e="T542" id="Seg_10710" s="T541">v:pred 0.3.h:S</ta>
            <ta e="T543" id="Seg_10711" s="T542">np:O</ta>
            <ta e="T544" id="Seg_10712" s="T543">v:pred 0.3.h:S</ta>
            <ta e="T546" id="Seg_10713" s="T545">v:pred 0.3.h:S</ta>
            <ta e="T549" id="Seg_10714" s="T548">np:S</ta>
            <ta e="T553" id="Seg_10715" s="T552">np:O</ta>
            <ta e="T555" id="Seg_10716" s="T554">v:pred 0.3.h:S</ta>
            <ta e="T558" id="Seg_10717" s="T557">adj:pred</ta>
            <ta e="T563" id="Seg_10718" s="T561">adj:pred</ta>
            <ta e="T570" id="Seg_10719" s="T569">np:S</ta>
            <ta e="T571" id="Seg_10720" s="T570">v:pred</ta>
            <ta e="T573" id="Seg_10721" s="T572">np.h:S</ta>
            <ta e="T575" id="Seg_10722" s="T574">v:pred</ta>
            <ta e="T576" id="Seg_10723" s="T575">pro:S</ta>
            <ta e="T578" id="Seg_10724" s="T577">ptcl.neg</ta>
            <ta e="T579" id="Seg_10725" s="T578">adj:pred</ta>
            <ta e="T580" id="Seg_10726" s="T579">np.h:S</ta>
            <ta e="T582" id="Seg_10727" s="T581">v:pred</ta>
            <ta e="T584" id="Seg_10728" s="T583">ptcl.neg</ta>
            <ta e="T585" id="Seg_10729" s="T584">v:pred 0.2.h:S</ta>
            <ta e="T587" id="Seg_10730" s="T586">ptcl.neg</ta>
            <ta e="T588" id="Seg_10731" s="T587">adj:pred</ta>
            <ta e="T589" id="Seg_10732" s="T588">ptcl.neg</ta>
            <ta e="T590" id="Seg_10733" s="T589">adj:pred</ta>
            <ta e="T593" id="Seg_10734" s="T592">np.h:S</ta>
            <ta e="T595" id="Seg_10735" s="T594">np:O</ta>
            <ta e="T596" id="Seg_10736" s="T595">v:pred</ta>
            <ta e="T600" id="Seg_10737" s="T599">np:O</ta>
            <ta e="T601" id="Seg_10738" s="T600">v:pred 0.3.h:S</ta>
            <ta e="T604" id="Seg_10739" s="T603">v:pred 0.3.h:S</ta>
            <ta e="T606" id="Seg_10740" s="T605">v:pred 0.3.h:S</ta>
            <ta e="T615" id="Seg_10741" s="T614">ptcl:pred</ta>
            <ta e="T619" id="Seg_10742" s="T618">v:pred 0.3:S</ta>
            <ta e="T621" id="Seg_10743" s="T620">np:S</ta>
            <ta e="T622" id="Seg_10744" s="T621">v:pred</ta>
            <ta e="T625" id="Seg_10745" s="T624">np:S</ta>
            <ta e="T627" id="Seg_10746" s="T625">v:pred</ta>
            <ta e="T629" id="Seg_10747" s="T628">v:pred 0.1.h:S</ta>
            <ta e="T631" id="Seg_10748" s="T630">v:pred 0.1.h:S</ta>
            <ta e="T633" id="Seg_10749" s="T632">v:pred 0.1.h:S</ta>
            <ta e="T636" id="Seg_10750" s="T635">v:pred 0.1.h:S</ta>
            <ta e="T638" id="Seg_10751" s="T637">v:pred 0.1.h:S</ta>
            <ta e="T640" id="Seg_10752" s="T639">v:pred 0.1.h:S</ta>
            <ta e="T642" id="Seg_10753" s="T641">v:pred 0.1.h:S</ta>
            <ta e="T644" id="Seg_10754" s="T643">v:pred 0.1.h:S</ta>
            <ta e="T646" id="Seg_10755" s="T645">np.h:S</ta>
            <ta e="T648" id="Seg_10756" s="T647">v:pred</ta>
            <ta e="T650" id="Seg_10757" s="T649">v:pred 0.3.h:S</ta>
            <ta e="T651" id="Seg_10758" s="T650">np:O</ta>
            <ta e="T654" id="Seg_10759" s="T653">v:pred 0.3.h:S</ta>
            <ta e="T655" id="Seg_10760" s="T654">pro.h:S</ta>
            <ta e="T656" id="Seg_10761" s="T655">v:pred</ta>
            <ta e="T658" id="Seg_10762" s="T657">v:pred 0.1.h:S</ta>
            <ta e="T660" id="Seg_10763" s="T659">v:pred 0.1.h:S</ta>
            <ta e="T662" id="Seg_10764" s="T661">v:pred 0.1.h:S</ta>
            <ta e="T663" id="Seg_10765" s="T662">pro.h:S</ta>
            <ta e="T664" id="Seg_10766" s="T663">v:pred</ta>
            <ta e="T667" id="Seg_10767" s="T666">adj:pred</ta>
            <ta e="T668" id="Seg_10768" s="T667">np:S</ta>
            <ta e="T671" id="Seg_10769" s="T670">np:O</ta>
            <ta e="T672" id="Seg_10770" s="T671">v:pred 0.1.h:S</ta>
            <ta e="T673" id="Seg_10771" s="T672">np:O</ta>
            <ta e="T674" id="Seg_10772" s="T673">v:pred 0.1.h:S</ta>
            <ta e="T675" id="Seg_10773" s="T674">np.h:S</ta>
            <ta e="T677" id="Seg_10774" s="T676">np.h:O</ta>
            <ta e="T678" id="Seg_10775" s="T677">v:pred</ta>
            <ta e="T680" id="Seg_10776" s="T679">pro.h:O</ta>
            <ta e="T682" id="Seg_10777" s="T681">v:pred 0.3.h:S</ta>
            <ta e="T686" id="Seg_10778" s="T685">np:S</ta>
            <ta e="T687" id="Seg_10779" s="T686">adj:pred</ta>
            <ta e="T689" id="Seg_10780" s="T688">np:S</ta>
            <ta e="T690" id="Seg_10781" s="T689">ptcl.neg</ta>
            <ta e="T691" id="Seg_10782" s="T690">v:pred</ta>
            <ta e="T693" id="Seg_10783" s="T692">pro:S</ta>
            <ta e="T694" id="Seg_10784" s="T693">adj:pred</ta>
            <ta e="T696" id="Seg_10785" s="T695">adj:pred</ta>
            <ta e="T697" id="Seg_10786" s="T696">np:S</ta>
            <ta e="T699" id="Seg_10787" s="T698">adj:pred</ta>
            <ta e="T701" id="Seg_10788" s="T700">np:S</ta>
            <ta e="T703" id="Seg_10789" s="T702">adj:pred</ta>
            <ta e="T709" id="Seg_10790" s="T708">np:S</ta>
            <ta e="T710" id="Seg_10791" s="T709">adj:pred</ta>
            <ta e="T711" id="Seg_10792" s="T710">np:S</ta>
            <ta e="T712" id="Seg_10793" s="T711">adj:pred</ta>
            <ta e="T714" id="Seg_10794" s="T713">np.h:S</ta>
            <ta e="T715" id="Seg_10795" s="T714">v:pred</ta>
            <ta e="T716" id="Seg_10796" s="T715">np.h:S</ta>
            <ta e="T717" id="Seg_10797" s="T716">v:pred</ta>
            <ta e="T719" id="Seg_10798" s="T718">np.h:S</ta>
            <ta e="T720" id="Seg_10799" s="T719">v:pred</ta>
            <ta e="T725" id="Seg_10800" s="T724">np.h:S</ta>
            <ta e="T726" id="Seg_10801" s="T725">v:pred</ta>
            <ta e="T727" id="Seg_10802" s="T726">np.h:S</ta>
            <ta e="T728" id="Seg_10803" s="T727">v:pred</ta>
            <ta e="T730" id="Seg_10804" s="T729">np.h:S</ta>
            <ta e="T731" id="Seg_10805" s="T730">v:pred</ta>
            <ta e="T733" id="Seg_10806" s="T732">np.h:S</ta>
            <ta e="T735" id="Seg_10807" s="T734">v:pred</ta>
            <ta e="T737" id="Seg_10808" s="T736">np.h:S</ta>
            <ta e="T739" id="Seg_10809" s="T738">v:pred</ta>
            <ta e="T740" id="Seg_10810" s="T739">ptcl:pred</ta>
            <ta e="T744" id="Seg_10811" s="T743">pro.h:S</ta>
            <ta e="T746" id="Seg_10812" s="T745">s:purp</ta>
            <ta e="T749" id="Seg_10813" s="T748">np.h:S</ta>
            <ta e="T752" id="Seg_10814" s="T751">v:pred</ta>
            <ta e="T753" id="Seg_10815" s="T752">pro.h:S</ta>
            <ta e="T755" id="Seg_10816" s="T754">v:pred</ta>
            <ta e="T757" id="Seg_10817" s="T756">v:pred 0.2.h:S</ta>
            <ta e="T758" id="Seg_10818" s="T757">v:pred 0.2.h:S</ta>
            <ta e="T761" id="Seg_10819" s="T760">v:pred 0.2.h:S</ta>
            <ta e="T763" id="Seg_10820" s="T762">pro.h:S</ta>
            <ta e="T765" id="Seg_10821" s="T764">v:pred</ta>
            <ta e="T766" id="Seg_10822" s="T765">v:pred 0.2.h:S</ta>
            <ta e="T768" id="Seg_10823" s="T767">np:O</ta>
            <ta e="T769" id="Seg_10824" s="T768">v:pred 0.2.h:S</ta>
            <ta e="T770" id="Seg_10825" s="T769">v:pred 0.2.h:S</ta>
            <ta e="T771" id="Seg_10826" s="T770">pro.h:O</ta>
            <ta e="T772" id="Seg_10827" s="T771">pro.h:S</ta>
            <ta e="T774" id="Seg_10828" s="T773">np:O</ta>
            <ta e="T775" id="Seg_10829" s="T774">v:pred</ta>
            <ta e="T776" id="Seg_10830" s="T775">np:S</ta>
            <ta e="T777" id="Seg_10831" s="T776">v:pred</ta>
            <ta e="T779" id="Seg_10832" s="T778">v:pred 0.1.h:S</ta>
            <ta e="T782" id="Seg_10833" s="T781">v:pred 0.1.h:S</ta>
            <ta e="T784" id="Seg_10834" s="T783">v:pred 0.1.h:S</ta>
            <ta e="T785" id="Seg_10835" s="T784">np:O</ta>
            <ta e="T786" id="Seg_10836" s="T785">s:purp</ta>
            <ta e="T791" id="Seg_10837" s="T790">np:O</ta>
            <ta e="T792" id="Seg_10838" s="T791">v:pred 0.1.h:S</ta>
            <ta e="T793" id="Seg_10839" s="T792">pro.h:S</ta>
            <ta e="T796" id="Seg_10840" s="T795">v:pred</ta>
            <ta e="T800" id="Seg_10841" s="T799">v:pred 0.1.h:S</ta>
            <ta e="T802" id="Seg_10842" s="T801">v:pred 0.1.h:S</ta>
            <ta e="T805" id="Seg_10843" s="T804">np:S</ta>
            <ta e="T806" id="Seg_10844" s="T805">v:pred</ta>
            <ta e="T807" id="Seg_10845" s="T806">pro.h:S</ta>
            <ta e="T808" id="Seg_10846" s="T807">v:pred</ta>
            <ta e="T809" id="Seg_10847" s="T808">pro.h:S</ta>
            <ta e="T810" id="Seg_10848" s="T809">pro.h:O</ta>
            <ta e="T811" id="Seg_10849" s="T810">v:pred</ta>
            <ta e="T813" id="Seg_10850" s="T812">ptcl.neg</ta>
            <ta e="T814" id="Seg_10851" s="T813">ptcl:pred</ta>
            <ta e="T815" id="Seg_10852" s="T814">pro.h:S</ta>
            <ta e="T819" id="Seg_10853" s="T818">v:pred</ta>
            <ta e="T820" id="Seg_10854" s="T819">v:pred</ta>
            <ta e="T821" id="Seg_10855" s="T820">pro.h:S</ta>
            <ta e="T825" id="Seg_10856" s="T824">ptcl.neg</ta>
            <ta e="T826" id="Seg_10857" s="T825">ptcl:pred</ta>
            <ta e="T830" id="Seg_10858" s="T829">ptcl.neg</ta>
            <ta e="T831" id="Seg_10859" s="T830">v:pred 0.3.h:S</ta>
            <ta e="T833" id="Seg_10860" s="T832">pro.h:S</ta>
            <ta e="T834" id="Seg_10861" s="T833">ptcl.neg</ta>
            <ta e="T835" id="Seg_10862" s="T834">adj:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST-PKZ" />
         <annotation name="BOR" tierref="BOR-PKZ">
            <ta e="T7" id="Seg_10863" s="T6">RUS:gram</ta>
            <ta e="T21" id="Seg_10864" s="T20">TURK:disc</ta>
            <ta e="T24" id="Seg_10865" s="T23">TURK:disc</ta>
            <ta e="T44" id="Seg_10866" s="T43">TURK:disc</ta>
            <ta e="T54" id="Seg_10867" s="T53">RUS:cult</ta>
            <ta e="T58" id="Seg_10868" s="T57">TURK:disc</ta>
            <ta e="T61" id="Seg_10869" s="T60">RUS:mod</ta>
            <ta e="T63" id="Seg_10870" s="T62">TURK:cult</ta>
            <ta e="T69" id="Seg_10871" s="T68">TURK:disc</ta>
            <ta e="T75" id="Seg_10872" s="T74">TURK:disc</ta>
            <ta e="T76" id="Seg_10873" s="T75">TURK:gram(INDEF)</ta>
            <ta e="T79" id="Seg_10874" s="T78">TURK:gram(INDEF)</ta>
            <ta e="T83" id="Seg_10875" s="T82">TURK:disc</ta>
            <ta e="T88" id="Seg_10876" s="T87">TURK:disc</ta>
            <ta e="T95" id="Seg_10877" s="T94">TURK:disc</ta>
            <ta e="T99" id="Seg_10878" s="T98">TURK:disc</ta>
            <ta e="T104" id="Seg_10879" s="T103">TURK:gram(INDEF)</ta>
            <ta e="T106" id="Seg_10880" s="T104">TURK:disc</ta>
            <ta e="T107" id="Seg_10881" s="T106">TURK:gram(INDEF)</ta>
            <ta e="T109" id="Seg_10882" s="T108">%TURK:core</ta>
            <ta e="T112" id="Seg_10883" s="T111">RUS:gram</ta>
            <ta e="T151" id="Seg_10884" s="T150">RUS:gram</ta>
            <ta e="T158" id="Seg_10885" s="T157">RUS:gram</ta>
            <ta e="T163" id="Seg_10886" s="T162">TURK:disc</ta>
            <ta e="T164" id="Seg_10887" s="T163">%TURK:core</ta>
            <ta e="T166" id="Seg_10888" s="T165">TURK:disc</ta>
            <ta e="T169" id="Seg_10889" s="T168">TURK:disc</ta>
            <ta e="T173" id="Seg_10890" s="T172">RUS:gram</ta>
            <ta e="T174" id="Seg_10891" s="T173">TURK:disc</ta>
            <ta e="T178" id="Seg_10892" s="T177">RUS:gram</ta>
            <ta e="T180" id="Seg_10893" s="T179">TURK:disc</ta>
            <ta e="T184" id="Seg_10894" s="T183">TURK:disc</ta>
            <ta e="T193" id="Seg_10895" s="T192">TURK:disc</ta>
            <ta e="T198" id="Seg_10896" s="T197">TURK:disc</ta>
            <ta e="T201" id="Seg_10897" s="T200">TURK:core</ta>
            <ta e="T203" id="Seg_10898" s="T201">TURK:disc</ta>
            <ta e="T205" id="Seg_10899" s="T204">TURK:core</ta>
            <ta e="T207" id="Seg_10900" s="T205">TURK:disc</ta>
            <ta e="T211" id="Seg_10901" s="T210">TURK:disc</ta>
            <ta e="T212" id="Seg_10902" s="T211">TURK:core</ta>
            <ta e="T218" id="Seg_10903" s="T217">RUS:gram</ta>
            <ta e="T221" id="Seg_10904" s="T220">RUS:gram</ta>
            <ta e="T230" id="Seg_10905" s="T229">TURK:cult</ta>
            <ta e="T235" id="Seg_10906" s="T234">TURK:disc</ta>
            <ta e="T238" id="Seg_10907" s="T237">TURK:disc</ta>
            <ta e="T254" id="Seg_10908" s="T253">RUS:cult</ta>
            <ta e="T256" id="Seg_10909" s="T255">RUS:gram</ta>
            <ta e="T263" id="Seg_10910" s="T262">TURK:disc</ta>
            <ta e="T270" id="Seg_10911" s="T269">TURK:disc</ta>
            <ta e="T273" id="Seg_10912" s="T272">TURK:disc</ta>
            <ta e="T279" id="Seg_10913" s="T278">TURK:disc</ta>
            <ta e="T281" id="Seg_10914" s="T280">RUS:gram</ta>
            <ta e="T285" id="Seg_10915" s="T284">RUS:gram</ta>
            <ta e="T290" id="Seg_10916" s="T289">TURK:cult</ta>
            <ta e="T291" id="Seg_10917" s="T290">RUS:gram</ta>
            <ta e="T300" id="Seg_10918" s="T299">RUS:gram</ta>
            <ta e="T312" id="Seg_10919" s="T311">TURK:disc</ta>
            <ta e="T314" id="Seg_10920" s="T312">%TURK:core</ta>
            <ta e="T315" id="Seg_10921" s="T314">RUS:gram</ta>
            <ta e="T317" id="Seg_10922" s="T316">TURK:disc</ta>
            <ta e="T326" id="Seg_10923" s="T325">TURK:core</ta>
            <ta e="T331" id="Seg_10924" s="T330">TURK:disc</ta>
            <ta e="T333" id="Seg_10925" s="T332">RUS:core</ta>
            <ta e="T337" id="Seg_10926" s="T336">RUS:gram</ta>
            <ta e="T350" id="Seg_10927" s="T349">TURK:cult</ta>
            <ta e="T352" id="Seg_10928" s="T351">TURK:core</ta>
            <ta e="T363" id="Seg_10929" s="T362">TURK:disc</ta>
            <ta e="T368" id="Seg_10930" s="T367">TURK:disc</ta>
            <ta e="T376" id="Seg_10931" s="T375">TURK:disc</ta>
            <ta e="T379" id="Seg_10932" s="T378">%TURK:core</ta>
            <ta e="T380" id="Seg_10933" s="T379">RUS:cult</ta>
            <ta e="T382" id="Seg_10934" s="T381">RUS:gram</ta>
            <ta e="T385" id="Seg_10935" s="T384">%TURK:core</ta>
            <ta e="T389" id="Seg_10936" s="T388">TURK:disc</ta>
            <ta e="T392" id="Seg_10937" s="T391">TURK:disc</ta>
            <ta e="T394" id="Seg_10938" s="T393">TURK:disc</ta>
            <ta e="T397" id="Seg_10939" s="T396">RUS:gram</ta>
            <ta e="T409" id="Seg_10940" s="T408">RUS:gram</ta>
            <ta e="T415" id="Seg_10941" s="T413">TURK:disc</ta>
            <ta e="T421" id="Seg_10942" s="T420">RUS:core</ta>
            <ta e="T425" id="Seg_10943" s="T424">RUS:gram</ta>
            <ta e="T432" id="Seg_10944" s="T431">RUS:gram</ta>
            <ta e="T436" id="Seg_10945" s="T435">RUS:gram</ta>
            <ta e="T441" id="Seg_10946" s="T440">RUS:cult</ta>
            <ta e="T443" id="Seg_10947" s="T442">RUS:mod</ta>
            <ta e="T444" id="Seg_10948" s="T443">TURK:cult</ta>
            <ta e="T446" id="Seg_10949" s="T445">TURK:core</ta>
            <ta e="T451" id="Seg_10950" s="T450">RUS:mod</ta>
            <ta e="T454" id="Seg_10951" s="T453">RUS:mod</ta>
            <ta e="T455" id="Seg_10952" s="T454">TURK:cult</ta>
            <ta e="T459" id="Seg_10953" s="T458">RUS:gram</ta>
            <ta e="T466" id="Seg_10954" s="T465">RUS:gram</ta>
            <ta e="T483" id="Seg_10955" s="T482">TURK:disc</ta>
            <ta e="T487" id="Seg_10956" s="T486">TURK:core</ta>
            <ta e="T507" id="Seg_10957" s="T506">RUS:mod</ta>
            <ta e="T513" id="Seg_10958" s="T512">RUS:gram</ta>
            <ta e="T517" id="Seg_10959" s="T516">RUS:gram</ta>
            <ta e="T518" id="Seg_10960" s="T517">TURK:cult</ta>
            <ta e="T527" id="Seg_10961" s="T526">TAT:cult</ta>
            <ta e="T539" id="Seg_10962" s="T538">TURK:disc</ta>
            <ta e="T549" id="Seg_10963" s="T548">TAT:cult</ta>
            <ta e="T554" id="Seg_10964" s="T553">TURK:disc</ta>
            <ta e="T867" id="Seg_10965" s="T555">RUS:core</ta>
            <ta e="T559" id="Seg_10966" s="T558">RUS:gram</ta>
            <ta e="T560" id="Seg_10967" s="T559">RUS:core</ta>
            <ta e="T570" id="Seg_10968" s="T569">TAT:cult</ta>
            <ta e="T577" id="Seg_10969" s="T576">TURK:disc</ta>
            <ta e="T581" id="Seg_10970" s="T580">TURK:disc</ta>
            <ta e="T588" id="Seg_10971" s="T587">TURK:core</ta>
            <ta e="T597" id="Seg_10972" s="T596">TURK:cult</ta>
            <ta e="T602" id="Seg_10973" s="T601">RUS:gram</ta>
            <ta e="T605" id="Seg_10974" s="T604">RUS:gram</ta>
            <ta e="T615" id="Seg_10975" s="T614">RUS:mod</ta>
            <ta e="T620" id="Seg_10976" s="T868">TURK:disc</ta>
            <ta e="T624" id="Seg_10977" s="T623">RUS:gram</ta>
            <ta e="T657" id="Seg_10978" s="T656">RUS:gram</ta>
            <ta e="T659" id="Seg_10979" s="T658">RUS:gram</ta>
            <ta e="T661" id="Seg_10980" s="T660">RUS:gram</ta>
            <ta e="T665" id="Seg_10981" s="T664">RUS:gram</ta>
            <ta e="T670" id="Seg_10982" s="T669">TURK:disc</ta>
            <ta e="T671" id="Seg_10983" s="T670">%RUS:cult</ta>
            <ta e="T676" id="Seg_10984" s="T675">TURK:disc</ta>
            <ta e="T679" id="Seg_10985" s="T678">RUS:gram</ta>
            <ta e="T688" id="Seg_10986" s="T687">TURK:disc</ta>
            <ta e="T714" id="Seg_10987" s="T713">TURK:core</ta>
            <ta e="T725" id="Seg_10988" s="T724">TURK:core</ta>
            <ta e="T740" id="Seg_10989" s="T739">RUS:mod</ta>
            <ta e="T750" id="Seg_10990" s="T749">TURK:disc</ta>
            <ta e="T756" id="Seg_10991" s="T755">TURK:cult</ta>
            <ta e="T764" id="Seg_10992" s="T763">TURK:disc</ta>
            <ta e="T774" id="Seg_10993" s="T773">TAT:cult</ta>
            <ta e="T776" id="Seg_10994" s="T775">RUS:cult</ta>
            <ta e="T778" id="Seg_10995" s="T777">RUS:mod</ta>
            <ta e="T801" id="Seg_10996" s="T800">TURK:disc</ta>
            <ta e="T813" id="Seg_10997" s="T812">RUS:gram</ta>
            <ta e="T814" id="Seg_10998" s="T813">RUS:mod</ta>
            <ta e="T816" id="Seg_10999" s="T815">TURK:disc</ta>
            <ta e="T825" id="Seg_11000" s="T824">RUS:gram</ta>
            <ta e="T826" id="Seg_11001" s="T825">RUS:mod</ta>
            <ta e="T827" id="Seg_11002" s="T826">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-PKZ" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-PKZ">
            <ta e="T333" id="Seg_11003" s="T332">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS-PKZ">
            <ta e="T28" id="Seg_11004" s="T27">RUS:ext</ta>
            <ta e="T209" id="Seg_11005" s="T207">RUS:ext</ta>
            <ta e="T246" id="Seg_11006" s="T245">RUS:int</ta>
            <ta e="T866" id="Seg_11007" s="T305">RUS:int</ta>
            <ta e="T440" id="Seg_11008" s="T439">RUS:int</ta>
            <ta e="T477" id="Seg_11009" s="T476">RUS:int</ta>
            <ta e="T491" id="Seg_11010" s="T488">RUS:int</ta>
            <ta e="T500" id="Seg_11011" s="T493">RUS:ext</ta>
            <ta e="T545" id="Seg_11012" s="T544">RUS:int</ta>
            <ta e="T566" id="Seg_11013" s="T563">RUS:ext</ta>
            <ta e="T574" id="Seg_11014" s="T573">RUS:int</ta>
            <ta e="T612" id="Seg_11015" s="T610">RUS:int</ta>
            <ta e="T868" id="Seg_11016" s="T619">RUS:int</ta>
            <ta e="T628" id="Seg_11017" s="T627">RUS:int</ta>
            <ta e="T630" id="Seg_11018" s="T629">RUS:int</ta>
            <ta e="T649" id="Seg_11019" s="T648">RUS:int</ta>
            <ta e="T781" id="Seg_11020" s="T780">RUS:int</ta>
            <ta e="T797" id="Seg_11021" s="T796">RUS:int</ta>
         </annotation>
         <annotation name="fr" tierref="fr-PKZ">
            <ta e="T10" id="Seg_11022" s="T4">Когда приедешь сюда, купи мне очки.</ta>
            <ta e="T14" id="Seg_11023" s="T10">Твой муж куда уехал?</ta>
            <ta e="T18" id="Seg_11024" s="T14">На Белогорье уехал, [на] коне.</ta>
            <ta e="T20" id="Seg_11025" s="T18">Один поехал?</ta>
            <ta e="T24" id="Seg_11026" s="T20">Нет, они вдвоем поехали.</ta>
            <ta e="T27" id="Seg_11027" s="T24">Когда они ушли?</ta>
            <ta e="T32" id="Seg_11028" s="T28">Вчера ушли, очень рано.</ta>
            <ta e="T40" id="Seg_11029" s="T32">Зачем ты врешь, ты не (?).</ta>
            <ta e="T43" id="Seg_11030" s="T40">Поставь это на окно. [?]</ta>
            <ta e="T46" id="Seg_11031" s="T43">У меня рубашка порвалась.</ta>
            <ta e="T48" id="Seg_11032" s="T46">Дыра появилась.</ta>
            <ta e="T54" id="Seg_11033" s="T48">Я потом ее зашила нитками.</ta>
            <ta e="T56" id="Seg_11034" s="T54">Иголкой, наперстк[ом].</ta>
            <ta e="T60" id="Seg_11035" s="T56">Колокол звонит в церкви.</ta>
            <ta e="T66" id="Seg_11036" s="T60">Надо пойти Богу поклониться.</ta>
            <ta e="T69" id="Seg_11037" s="T66">Они зовут.</ta>
            <ta e="T72" id="Seg_11038" s="T69">Повесь на лошадь колокольчик.</ta>
            <ta e="T78" id="Seg_11039" s="T72">Ума нет, ничего не знаю.</ta>
            <ta e="T85" id="Seg_11040" s="T78">Ничего не могу сказать, ума нет.</ta>
            <ta e="T87" id="Seg_11041" s="T85">Я пошла в лес.</ta>
            <ta e="T92" id="Seg_11042" s="T87">Потерялась.</ta>
            <ta e="T96" id="Seg_11043" s="T92">Я пошла в лес, потерялась.</ta>
            <ta e="T99" id="Seg_11044" s="T96">Закричала.</ta>
            <ta e="T101" id="Seg_11045" s="T99">Потом пришла.</ta>
            <ta e="T103" id="Seg_11046" s="T101">Долго сидела.</ta>
            <ta e="T109" id="Seg_11047" s="T103">Ничего… Ничего не говорила.</ta>
            <ta e="T116" id="Seg_11048" s="T109">Он идет, а я его догоняю.</ta>
            <ta e="T120" id="Seg_11049" s="T116">"Не уходи, вернись сюда!</ta>
            <ta e="T125" id="Seg_11050" s="T120">Я тебе говорю: вернись сюда!"</ta>
            <ta e="T130" id="Seg_11051" s="T125">У меня народу очень много. [?]</ta>
            <ta e="T134" id="Seg_11052" s="T130">Надо много хлеба.</ta>
            <ta e="T137" id="Seg_11053" s="T134">Возьми его на спину!</ta>
            <ta e="T140" id="Seg_11054" s="T137">Не клади руки. [?]</ta>
            <ta e="T146" id="Seg_11055" s="T140">Потрогай его, потрогай его рукой. [?]</ta>
            <ta e="T154" id="Seg_11056" s="T146">Я взяла много себе, а ему я взяла мало.</ta>
            <ta e="T161" id="Seg_11057" s="T154">Он берёт много себе, а ему/ей он берёт мало.</ta>
            <ta e="T164" id="Seg_11058" s="T161">Мы вшестером говорили.</ta>
            <ta e="T170" id="Seg_11059" s="T164">[Когда] мы были маленькими, мы играли.</ta>
            <ta e="T177" id="Seg_11060" s="T170">[Один] берет палку, и один из нас побежит (?)</ta>
            <ta e="T181" id="Seg_11061" s="T177">А мы спрятались.</ta>
            <ta e="T191" id="Seg_11062" s="T181">Потом он ищет, ищет, не может найти [нас].</ta>
            <ta e="T195" id="Seg_11063" s="T191">[Потом] мы опять играем.</ta>
            <ta e="T200" id="Seg_11064" s="T195">Мы не ссорились.</ta>
            <ta e="T203" id="Seg_11065" s="T200">Хорошо…</ta>
            <ta e="T207" id="Seg_11066" s="T204">Хорошо…</ta>
            <ta e="T213" id="Seg_11067" s="T209">Мы хорошо играли.</ta>
            <ta e="T216" id="Seg_11068" s="T213">Мы не ссорились.</ta>
            <ta e="T220" id="Seg_11069" s="T216">Мужчины и женщины сидели.</ta>
            <ta e="T225" id="Seg_11070" s="T220">И смотрели, как мы играли.</ta>
            <ta e="T233" id="Seg_11071" s="T225">Кто курил, кто не курил.</ta>
            <ta e="T242" id="Seg_11072" s="T233">Кто трубку курил, кто не курил.</ta>
            <ta e="T249" id="Seg_11073" s="T242">Овец очень много, аж двадцать две.</ta>
            <ta e="T252" id="Seg_11074" s="T249">Мой отец в лес пошел.</ta>
            <ta e="T255" id="Seg_11075" s="T252">Убил много кур/глухарей.</ta>
            <ta e="T258" id="Seg_11076" s="T255">И домой принес.</ta>
            <ta e="T261" id="Seg_11077" s="T258">Я иду в Пермяково.</ta>
            <ta e="T265" id="Seg_11078" s="T261">Смотрю: лось стоит.</ta>
            <ta e="T271" id="Seg_11079" s="T265">Я смотрела, смотрела, потом закричала.</ta>
            <ta e="T275" id="Seg_11080" s="T271">Он побежал к реке.</ta>
            <ta e="T276" id="Seg_11081" s="T275">(Змея?).</ta>
            <ta e="T282" id="Seg_11082" s="T276">Потом он (?) и убежал.</ta>
            <ta e="T287" id="Seg_11083" s="T282">Я пришла, Власу сказала.</ta>
            <ta e="T293" id="Seg_11084" s="T287">Он пошел и убил его из ружья.</ta>
            <ta e="T295" id="Seg_11085" s="T293">… котел.</ta>
            <ta e="T299" id="Seg_11086" s="T295">Потом я мясо помыла.</ta>
            <ta e="T301" id="Seg_11087" s="T299">И съела.</ta>
            <ta e="T305" id="Seg_11088" s="T301">Нам дали один котел.</ta>
            <ta e="T314" id="Seg_11089" s="T305">Мужчина-финн пришел, я пошла туда, они разговаривают.</ta>
            <ta e="T321" id="Seg_11090" s="T314">А они меня прогнали, я убежала.</ta>
            <ta e="T332" id="Seg_11091" s="T321">[Когда] камасы жили одни, было хорошо, мяса было много, ты ел [его].</ta>
            <ta e="T338" id="Seg_11092" s="T332">Оленье мясо очень мягкое, как хлеб.</ta>
            <ta e="T343" id="Seg_11093" s="T338">Ты ел много, потом воду пил.</ta>
            <ta e="T348" id="Seg_11094" s="T343">Ну, воду попил…</ta>
            <ta e="T357" id="Seg_11095" s="T348">Этот начальник очень хороший, очень красивый, глаза у него черные.</ta>
            <ta e="T360" id="Seg_11096" s="T357">Нос у него не длинный.</ta>
            <ta e="T363" id="Seg_11097" s="T360">Губы красивые.</ta>
            <ta e="T366" id="Seg_11098" s="T363">Губы не толстые.</ta>
            <ta e="T369" id="Seg_11099" s="T366">Матрена болела.</ta>
            <ta e="T374" id="Seg_11100" s="T369">Я к ней пойду.</ta>
            <ta e="T385" id="Seg_11101" s="T374">Она говорит: "Не говори по-русски, говори на своем языке.</ta>
            <ta e="T389" id="Seg_11102" s="T385">Я очень больна.</ta>
            <ta e="T392" id="Seg_11103" s="T389">Я очень больна.</ta>
            <ta e="T399" id="Seg_11104" s="T392">Я ведь умру, а ты останешься".</ta>
            <ta e="T405" id="Seg_11105" s="T399">Она умерла, две дочери у нее остались.</ta>
            <ta e="T412" id="Seg_11106" s="T405">Одна с ней была, а другая была далеко.</ta>
            <ta e="T415" id="Seg_11107" s="T412">Сегодня…</ta>
            <ta e="T424" id="Seg_11108" s="T419">Сегодня целый день дождь шел.</ta>
            <ta e="T428" id="Seg_11109" s="T424">А сейчас солнце светит.</ta>
            <ta e="T435" id="Seg_11110" s="T428">Новик едет на свинье, а Сидор сидит на собаке.</ta>
            <ta e="T440" id="Seg_11111" s="T435">А люди увидели и подумали: казаки.</ta>
            <ta e="T445" id="Seg_11112" s="T440">Председатель сказал: "Надо мельницу (налаживать?).</ta>
            <ta e="T453" id="Seg_11113" s="T445">Хороших людей собрать, пускай там работают.</ta>
            <ta e="T461" id="Seg_11114" s="T453">Надо на мельницу пойти, муки намолоть, а то муки нет.</ta>
            <ta e="T473" id="Seg_11115" s="T461">Я была маленькой, а девушки большие были, всегда меня брали с собой.</ta>
            <ta e="T481" id="Seg_11116" s="T473">Чтобы петь песни у меня голос-то красивый был.</ta>
            <ta e="T483" id="Seg_11117" s="T481">Я пела.</ta>
            <ta e="T488" id="Seg_11118" s="T483">Этот человек очень хорошо поет.</ta>
            <ta e="T493" id="Seg_11119" s="T488">Голос-то очень красивый.</ta>
            <ta e="T503" id="Seg_11120" s="T500">Очень громко поет.</ta>
            <ta e="T512" id="Seg_11121" s="T503">Этот человек умер, надо гроб сделать, из дерева.</ta>
            <ta e="T516" id="Seg_11122" s="T512">И туда его положить.</ta>
            <ta e="T519" id="Seg_11123" s="T516">И крест сделать.</ta>
            <ta e="T524" id="Seg_11124" s="T519">Он теперь пойдет в ту землю.</ta>
            <ta e="T528" id="Seg_11125" s="T524">Очень красивые дома стоят.</ta>
            <ta e="T535" id="Seg_11126" s="T528">Они сделаны [из] камня.</ta>
            <ta e="T542" id="Seg_11127" s="T535">У окошка деревья посадили.</ta>
            <ta e="T544" id="Seg_11128" s="T542">Деревья посадили.</ta>
            <ta e="T546" id="Seg_11129" s="T544">Малину посадили.</ta>
            <ta e="T551" id="Seg_11130" s="T546">Очень красивые дома стоят.</ta>
            <ta e="T555" id="Seg_11131" s="T551">Черемуху посадили.</ta>
            <ta e="T563" id="Seg_11132" s="T555">С левой стороны красиво, с правой стороны красиво.</ta>
            <ta e="T571" id="Seg_11133" s="T566">Очень красивые дома стоят.</ta>
            <ta e="T575" id="Seg_11134" s="T571">Моя невестка самогон сварила.</ta>
            <ta e="T579" id="Seg_11135" s="T575">Он не крепкий.</ta>
            <ta e="T582" id="Seg_11136" s="T579">Человек ругается:</ta>
            <ta e="T590" id="Seg_11137" s="T582">"Больше не будешь варить, он плохой, не крепкий".</ta>
            <ta e="T597" id="Seg_11138" s="T590">Тогда моя невестка воду сварила, с солью.</ta>
            <ta e="T601" id="Seg_11139" s="T597">Потом мясо порезала.</ta>
            <ta e="T606" id="Seg_11140" s="T601">И туда мясо положила, чтобы посолить [его].</ta>
            <ta e="T609" id="Seg_11141" s="T606">Хозяин, жена хозяина.</ta>
            <ta e="T614" id="Seg_11142" s="T609">Идти никак нельзя, очень много воды.</ta>
            <ta e="T616" id="Seg_11143" s="T614">Надо возвращаться.</ta>
            <ta e="T619" id="Seg_11144" s="T616">Очень далеко видно.</ta>
            <ta e="T627" id="Seg_11145" s="T619">Степь, деревьев нет, ни одного дерева нет.</ta>
            <ta e="T631" id="Seg_11146" s="T627">Степью иду - степь пою.</ta>
            <ta e="T636" id="Seg_11147" s="T631">В лес иду - лес пою.</ta>
            <ta e="T640" id="Seg_11148" s="T636">На реку иду - реку пою.</ta>
            <ta e="T644" id="Seg_11149" s="T640">В лесу иду - лес пою.</ta>
            <ta e="T648" id="Seg_11150" s="T644">Мой отец ушел на Кирель. </ta>
            <ta e="T650" id="Seg_11151" s="T648">Лодку взял.</ta>
            <ta e="T654" id="Seg_11152" s="T650">Рыбы много поймал.</ta>
            <ta e="T662" id="Seg_11153" s="T654">Мы ее почистили, посолили, пожарили и съели.</ta>
            <ta e="T668" id="Seg_11154" s="T662">Мы (ходили?), вода очень высокая.</ta>
            <ta e="T674" id="Seg_11155" s="T668">Там сети (бросили?), рыбу поймали.</ta>
            <ta e="T682" id="Seg_11156" s="T674">Мужчина жену задушил и сам на дереве повесился.</ta>
            <ta e="T687" id="Seg_11157" s="T682">У медведя очень много шерсти.</ta>
            <ta e="T691" id="Seg_11158" s="T687">Мяса не видно.</ta>
            <ta e="T696" id="Seg_11159" s="T691">Она [= шерсть] очень мягкая, очень толстая.</ta>
            <ta e="T700" id="Seg_11160" s="T696">Ноги у него очень сильные.</ta>
            <ta e="T703" id="Seg_11161" s="T700">Когти очень большие.</ta>
            <ta e="T706" id="Seg_11162" s="T703">Больше, чем собака.</ta>
            <ta e="T708" id="Seg_11163" s="T706">Больше, чем собака.</ta>
            <ta e="T710" id="Seg_11164" s="T708">Глаза у него маленькие.</ta>
            <ta e="T712" id="Seg_11165" s="T710">Уши маленькие.</ta>
            <ta e="T715" id="Seg_11166" s="T712">Мои родственники пришли.</ta>
            <ta e="T718" id="Seg_11167" s="T715">Сын пришел с женой.</ta>
            <ta e="T721" id="Seg_11168" s="T718">Брат пришел с женой.</ta>
            <ta e="T723" id="Seg_11169" s="T721">Дети…</ta>
            <ta e="T726" id="Seg_11170" s="T723">Мои родственники пришли.</ta>
            <ta e="T729" id="Seg_11171" s="T726">Сын пришел с женой.</ta>
            <ta e="T732" id="Seg_11172" s="T729">Брать пришел с женой.</ta>
            <ta e="T735" id="Seg_11173" s="T732">Дочь с мужем пришла.</ta>
            <ta e="T739" id="Seg_11174" s="T735">Ее дети с ней пришли.</ta>
            <ta e="T747" id="Seg_11175" s="T739">Надо им собрать [на стои], их покормить.</ta>
            <ta e="T755" id="Seg_11176" s="T747">Мой сын очень болен, я ему говорю:</ta>
            <ta e="T757" id="Seg_11177" s="T755">"Богу помолись.</ta>
            <ta e="T760" id="Seg_11178" s="T757">Попроси у него силу.</ta>
            <ta e="T762" id="Seg_11179" s="T760">Призови его к себе".</ta>
            <ta e="T767" id="Seg_11180" s="T762">Он позвал: "Приди ко мне.</ta>
            <ta e="T769" id="Seg_11181" s="T767">Дай [мне] силы!</ta>
            <ta e="T771" id="Seg_11182" s="T769">Вылечи меня!"</ta>
            <ta e="T775" id="Seg_11183" s="T771">Я вчера дом помыла.</ta>
            <ta e="T779" id="Seg_11184" s="T775">Курица нагадила, [это] тоже помыла.</ta>
            <ta e="T782" id="Seg_11185" s="T779">Потом я крыльцо помыла.</ta>
            <ta e="T787" id="Seg_11186" s="T782">Потом я пошла натаскать воды в баню.</ta>
            <ta e="T792" id="Seg_11187" s="T787">Один… шесть ведер принесла.</ta>
            <ta e="T796" id="Seg_11188" s="T792">Я сегодня рано встала.</ta>
            <ta e="T800" id="Seg_11189" s="T796">Целый день работала.</ta>
            <ta e="T802" id="Seg_11190" s="T800">Устала.</ta>
            <ta e="T808" id="Seg_11191" s="T802">Вечер пришел, я уснула.</ta>
            <ta e="T814" id="Seg_11192" s="T808">Я не хочу их видеть.</ta>
            <ta e="T819" id="Seg_11193" s="T814">Они всегда врут.</ta>
            <ta e="T821" id="Seg_11194" s="T819">Они всегда врут.</ta>
            <ta e="T826" id="Seg_11195" s="T821">Даже слышать их не хочу.</ta>
            <ta e="T831" id="Seg_11196" s="T826">Пусть они ко мне не приходят!</ta>
            <ta e="T835" id="Seg_11197" s="T831">Мне их не надо.</ta>
            <ta e="T838" id="Seg_11198" s="T835">Моя голова, мои волосы, мои уши.</ta>
            <ta e="T839" id="Seg_11199" s="T838">Мои зубы.</ta>
            <ta e="T841" id="Seg_11200" s="T839">Мои губы, мой язык.</ta>
            <ta e="T843" id="Seg_11201" s="T841">Мои глаза, мой нос.</ta>
            <ta e="T845" id="Seg_11202" s="T843">Мои руки, моя спина.</ta>
            <ta e="T847" id="Seg_11203" s="T845">Мое сердце.</ta>
            <ta e="T849" id="Seg_11204" s="T847">Мое нутро, мой желудок.</ta>
            <ta e="T853" id="Seg_11205" s="T849">Моя спина, моя задница, мои ноги.</ta>
            <ta e="T856" id="Seg_11206" s="T853">[Мой] рот, (мое сердце) его сердце.</ta>
            <ta e="T858" id="Seg_11207" s="T857">Ее груди.</ta>
            <ta e="T861" id="Seg_11208" s="T859">Его/ее кости</ta>
            <ta e="T863" id="Seg_11209" s="T862">Кровь.</ta>
         </annotation>
         <annotation name="fe" tierref="fe-PKZ">
            <ta e="T10" id="Seg_11210" s="T4">When you come here, buy me eyeglasses.</ta>
            <ta e="T14" id="Seg_11211" s="T10">Where did your husband go?</ta>
            <ta e="T18" id="Seg_11212" s="T14">He went to the Sayan mountains, [on] a horse.</ta>
            <ta e="T20" id="Seg_11213" s="T18">Did he go alone?</ta>
            <ta e="T24" id="Seg_11214" s="T20">No, two [people] went.</ta>
            <ta e="T27" id="Seg_11215" s="T24">When did they go?</ta>
            <ta e="T28" id="Seg_11216" s="T27">Wait.</ta>
            <ta e="T32" id="Seg_11217" s="T28">They went yesterday, very [early] in the morning.</ta>
            <ta e="T40" id="Seg_11218" s="T32">Why are you lying, you don't (?).</ta>
            <ta e="T43" id="Seg_11219" s="T40">Put it on the window. [?]</ta>
            <ta e="T46" id="Seg_11220" s="T43">My shirt is torn.</ta>
            <ta e="T48" id="Seg_11221" s="T46">A hole appeared.</ta>
            <ta e="T54" id="Seg_11222" s="T48">Then I sewed it with threads.</ta>
            <ta e="T56" id="Seg_11223" s="T54">With a needle, thimble.</ta>
            <ta e="T60" id="Seg_11224" s="T56">The bell is ringing in the church.</ta>
            <ta e="T66" id="Seg_11225" s="T60">[We] should go to pray to God.</ta>
            <ta e="T69" id="Seg_11226" s="T66">They are calling.</ta>
            <ta e="T72" id="Seg_11227" s="T69">Hang the bell on the horse!</ta>
            <ta e="T78" id="Seg_11228" s="T72">[I] do not have my mind, I do not know anything.</ta>
            <ta e="T85" id="Seg_11229" s="T78">I cannot tell anything, I do not have my mind.</ta>
            <ta e="T87" id="Seg_11230" s="T85">I went to the taiga.</ta>
            <ta e="T92" id="Seg_11231" s="T87">[I] got lost.</ta>
            <ta e="T96" id="Seg_11232" s="T92">I went to the taiga, I got lost.</ta>
            <ta e="T99" id="Seg_11233" s="T96">Then I shouted.</ta>
            <ta e="T101" id="Seg_11234" s="T99">Then I came.</ta>
            <ta e="T103" id="Seg_11235" s="T101">[I] was sitting for a long time.</ta>
            <ta e="T109" id="Seg_11236" s="T103">Nothing… [I] did not say anything.</ta>
            <ta e="T116" id="Seg_11237" s="T109">He is going, and I'm catching him up.</ta>
            <ta e="T120" id="Seg_11238" s="T116">"Don’t go, come back here!</ta>
            <ta e="T125" id="Seg_11239" s="T120">I tell you, come back here!"</ta>
            <ta e="T130" id="Seg_11240" s="T125">There are very many people by me. [?]</ta>
            <ta e="T134" id="Seg_11241" s="T130">[We] need much bread.</ta>
            <ta e="T137" id="Seg_11242" s="T134">Take it onto your back!</ta>
            <ta e="T140" id="Seg_11243" s="T137">Don't put your hands. [?]</ta>
            <ta e="T146" id="Seg_11244" s="T140">Touch it, touch it with [your] hand. [?]</ta>
            <ta e="T154" id="Seg_11245" s="T146">I take much for myself, and for him I took few.</ta>
            <ta e="T161" id="Seg_11246" s="T154">He takes much for himself, and for him/her he takes few.</ta>
            <ta e="T164" id="Seg_11247" s="T161">We were talking six persons together.</ta>
            <ta e="T170" id="Seg_11248" s="T164">[When] we were little, we used to play.</ta>
            <ta e="T177" id="Seg_11249" s="T170">[One] takes sticks, and one [of us] will run. [?]</ta>
            <ta e="T181" id="Seg_11250" s="T177">And we hide.</ta>
            <ta e="T191" id="Seg_11251" s="T181">Then s/he looks, looks for [us], cannot find [us].</ta>
            <ta e="T195" id="Seg_11252" s="T191">[Then] we play again.</ta>
            <ta e="T200" id="Seg_11253" s="T195">We didn't quarrel.</ta>
            <ta e="T203" id="Seg_11254" s="T200">Well…</ta>
            <ta e="T207" id="Seg_11255" s="T204">Well…</ta>
            <ta e="T213" id="Seg_11256" s="T209">We played well.</ta>
            <ta e="T216" id="Seg_11257" s="T213">We weren't angry.</ta>
            <ta e="T220" id="Seg_11258" s="T216">Men and women were sitting.</ta>
            <ta e="T225" id="Seg_11259" s="T220">And they were looking how we played.</ta>
            <ta e="T233" id="Seg_11260" s="T225">Some smoked tobacco, some didn't smoke.</ta>
            <ta e="T242" id="Seg_11261" s="T233">Some smoked pipe, some didn't smoke.</ta>
            <ta e="T249" id="Seg_11262" s="T242">Very many sheep, as many as twenty two.</ta>
            <ta e="T252" id="Seg_11263" s="T249">My father went to the forest.</ta>
            <ta e="T255" id="Seg_11264" s="T252">He killed many hen/wood-grouses.</ta>
            <ta e="T258" id="Seg_11265" s="T255">And brought [them] home.</ta>
            <ta e="T261" id="Seg_11266" s="T258">I'm going to Permyakovo.</ta>
            <ta e="T265" id="Seg_11267" s="T261">I see: there stands an elk.</ta>
            <ta e="T271" id="Seg_11268" s="T265">I was looking for a while, then I shouted.</ta>
            <ta e="T275" id="Seg_11269" s="T271">It ran [away] to the river.</ta>
            <ta e="T276" id="Seg_11270" s="T275">(Snake?).</ta>
            <ta e="T282" id="Seg_11271" s="T276">Then it (?) and ran away.</ta>
            <ta e="T287" id="Seg_11272" s="T282">I came, told to Vlas.</ta>
            <ta e="T293" id="Seg_11273" s="T287">He went [and] killed it with the gun.</ta>
            <ta e="T295" id="Seg_11274" s="T293">… a cauldron.</ta>
            <ta e="T299" id="Seg_11275" s="T295">Then I washed the meat.</ta>
            <ta e="T301" id="Seg_11276" s="T299">And ate [it].</ta>
            <ta e="T305" id="Seg_11277" s="T301">We were given one cauldron.</ta>
            <ta e="T314" id="Seg_11278" s="T305">A Finnish man came, I went there, they are talking.</ta>
            <ta e="T321" id="Seg_11279" s="T314">They chased me away, I ran away.</ta>
            <ta e="T332" id="Seg_11280" s="T321">[When] the Kamas lived alone, it was good, there was much meat, you ate [it all].</ta>
            <ta e="T338" id="Seg_11281" s="T332">Reindeer meat is very soft, like bread.</ta>
            <ta e="T343" id="Seg_11282" s="T338">You ate much, then you drank water.</ta>
            <ta e="T348" id="Seg_11283" s="T343">Well, you drank water…</ta>
            <ta e="T357" id="Seg_11284" s="T348">This chief is very good, very beautiful, his eyes are black.</ta>
            <ta e="T360" id="Seg_11285" s="T357">His nose isn't long.</ta>
            <ta e="T363" id="Seg_11286" s="T360">His lips are beautiful.</ta>
            <ta e="T366" id="Seg_11287" s="T363">His lips are not thick.</ta>
            <ta e="T369" id="Seg_11288" s="T366">Matryona was ill.</ta>
            <ta e="T374" id="Seg_11289" s="T369">I'll go to her.</ta>
            <ta e="T385" id="Seg_11290" s="T374">She said: "Don't speak Russian, speak [your] own language.</ta>
            <ta e="T389" id="Seg_11291" s="T385">I am very ill.</ta>
            <ta e="T392" id="Seg_11292" s="T389">I am very ill.</ta>
            <ta e="T399" id="Seg_11293" s="T392">I'll die, and you'll stay."</ta>
            <ta e="T405" id="Seg_11294" s="T399">She died, two daughters of hers left.</ta>
            <ta e="T412" id="Seg_11295" s="T405">One [of them] was with her, and one was far away.</ta>
            <ta e="T415" id="Seg_11296" s="T412">Today…</ta>
            <ta e="T424" id="Seg_11297" s="T419">Today the whole day it rained.</ta>
            <ta e="T428" id="Seg_11298" s="T424">And now sun shines.</ta>
            <ta e="T435" id="Seg_11299" s="T428">Novik is riding a pig, and Sidor is sitting on a dog.</ta>
            <ta e="T440" id="Seg_11300" s="T435">And people saw [them] and thought: Cossacks.</ta>
            <ta e="T445" id="Seg_11301" s="T440">The chairman said: "[We] should (repair?) the mill.</ta>
            <ta e="T453" id="Seg_11302" s="T445">Gather good people, let them work there.</ta>
            <ta e="T461" id="Seg_11303" s="T453">We should go to the mill to grind the flour, because [we] have no flour.</ta>
            <ta e="T473" id="Seg_11304" s="T461">I was little, and the girls were big, they always took me with them.</ta>
            <ta e="T481" id="Seg_11305" s="T473">To sing songs, my voice was good.</ta>
            <ta e="T483" id="Seg_11306" s="T481">I sang.</ta>
            <ta e="T488" id="Seg_11307" s="T483">This man sings very good.</ta>
            <ta e="T493" id="Seg_11308" s="T488">His voice is beautiful.</ta>
            <ta e="T503" id="Seg_11309" s="T500">He sings very loudly.</ta>
            <ta e="T512" id="Seg_11310" s="T503">This man died, [we] should make a coffin, of wood.</ta>
            <ta e="T516" id="Seg_11311" s="T512">And put him there.</ta>
            <ta e="T519" id="Seg_11312" s="T516">And make a cross.</ta>
            <ta e="T524" id="Seg_11313" s="T519">Now he will go here to that land.</ta>
            <ta e="T528" id="Seg_11314" s="T524">[There] stand very beautiful houses.</ta>
            <ta e="T535" id="Seg_11315" s="T528">They are made [from] stones.</ta>
            <ta e="T542" id="Seg_11316" s="T535">They planted trees near the window. </ta>
            <ta e="T544" id="Seg_11317" s="T542">They planted trees.</ta>
            <ta e="T546" id="Seg_11318" s="T544">They planted raspberries.</ta>
            <ta e="T551" id="Seg_11319" s="T546">Very beautiful houses stand [there].</ta>
            <ta e="T555" id="Seg_11320" s="T551">They planted bird cherry tree(s).</ta>
            <ta e="T563" id="Seg_11321" s="T555">On the left [it is] beatiful, on the right [it is] beautiful.</ta>
            <ta e="T571" id="Seg_11322" s="T566">Very beatiful houses are standing.</ta>
            <ta e="T575" id="Seg_11323" s="T571">My (sister-in-law?) made home alcohol.</ta>
            <ta e="T579" id="Seg_11324" s="T575">It isn't strong.</ta>
            <ta e="T582" id="Seg_11325" s="T579">A man is scolding:</ta>
            <ta e="T590" id="Seg_11326" s="T582">"You won't cook it amymore, it is not good, not strong."</ta>
            <ta e="T597" id="Seg_11327" s="T590">Then my sister-in-law boiled water, with salt.</ta>
            <ta e="T601" id="Seg_11328" s="T597">Then she cut meat.</ta>
            <ta e="T606" id="Seg_11329" s="T601">And put it there, to salt [it].</ta>
            <ta e="T609" id="Seg_11330" s="T606">The master, the master's wife.</ta>
            <ta e="T614" id="Seg_11331" s="T609">There is no way to go, [there is] too much water.</ta>
            <ta e="T616" id="Seg_11332" s="T614">One should go back.</ta>
            <ta e="T619" id="Seg_11333" s="T616">One can see very far.</ta>
            <ta e="T627" id="Seg_11334" s="T619">It is the steppe, there are no trees, not a single tree.</ta>
            <ta e="T631" id="Seg_11335" s="T627">When I'm going through the steppe, I sing [about] the steppe.</ta>
            <ta e="T636" id="Seg_11336" s="T631">[When] I go to the taiga, I sing [about] the taiga.</ta>
            <ta e="T640" id="Seg_11337" s="T636">[When] I go to the river, I sing [about] the river.</ta>
            <ta e="T644" id="Seg_11338" s="T640">[When] I go through the forest, I sing [about] the forest.</ta>
            <ta e="T648" id="Seg_11339" s="T644">My father went to Kirel.</ta>
            <ta e="T650" id="Seg_11340" s="T648">[He] took a boat.</ta>
            <ta e="T654" id="Seg_11341" s="T650">He caught plenty of fish.</ta>
            <ta e="T662" id="Seg_11342" s="T654">We cleaned it, and salted, and roasted, and ate.</ta>
            <ta e="T668" id="Seg_11343" s="T662">We (went?), water is very big [= high].</ta>
            <ta e="T674" id="Seg_11344" s="T668">We set a (net?), caught fish.</ta>
            <ta e="T682" id="Seg_11345" s="T674">A man strangled a woman and hung himself on a tree.</ta>
            <ta e="T687" id="Seg_11346" s="T682">The bear has very much hair.</ta>
            <ta e="T691" id="Seg_11347" s="T687">One can't even see the flesh.</ta>
            <ta e="T696" id="Seg_11348" s="T691">It is very soft, very thick.</ta>
            <ta e="T700" id="Seg_11349" s="T696">Its legs are strong.</ta>
            <ta e="T703" id="Seg_11350" s="T700">Its claws are very big.</ta>
            <ta e="T706" id="Seg_11351" s="T703">Bigger than a dog.</ta>
            <ta e="T708" id="Seg_11352" s="T706">Bigger than a dog.</ta>
            <ta e="T710" id="Seg_11353" s="T708">Its eyes are small.</ta>
            <ta e="T712" id="Seg_11354" s="T710">[Its] ears are small.</ta>
            <ta e="T715" id="Seg_11355" s="T712">My relatives came.</ta>
            <ta e="T718" id="Seg_11356" s="T715">[My] son came with [his] wife.</ta>
            <ta e="T721" id="Seg_11357" s="T718">My brother [in-law] came with [his] wife.</ta>
            <ta e="T723" id="Seg_11358" s="T721">Children…</ta>
            <ta e="T726" id="Seg_11359" s="T723">My relatives came.</ta>
            <ta e="T729" id="Seg_11360" s="T726">[My] son came with [his] wife.</ta>
            <ta e="T732" id="Seg_11361" s="T729">My brother [in-law?] came with [his] wife.</ta>
            <ta e="T735" id="Seg_11362" s="T732">My daughter came with her husband.</ta>
            <ta e="T739" id="Seg_11363" s="T735">Her children came with her.</ta>
            <ta e="T747" id="Seg_11364" s="T739">[I] must collect [food] for them, give them to eat.</ta>
            <ta e="T755" id="Seg_11365" s="T747">My son is very ill, I said to him:</ta>
            <ta e="T757" id="Seg_11366" s="T755">"Pray to God.</ta>
            <ta e="T760" id="Seg_11367" s="T757">Ask him for strength.</ta>
            <ta e="T762" id="Seg_11368" s="T760">Call him to you."</ta>
            <ta e="T767" id="Seg_11369" s="T762">He called: "Come to me.</ta>
            <ta e="T769" id="Seg_11370" s="T767">Give [me] force!</ta>
            <ta e="T771" id="Seg_11371" s="T769">Heal me!"</ta>
            <ta e="T775" id="Seg_11372" s="T771">Yesterday, I washed [my] house.</ta>
            <ta e="T779" id="Seg_11373" s="T775">A hen defecated, I washed [this], too.</ta>
            <ta e="T782" id="Seg_11374" s="T779">Then I washed the front steps.</ta>
            <ta e="T787" id="Seg_11375" s="T782">Then I went to bring water to the bath.</ta>
            <ta e="T792" id="Seg_11376" s="T787">I brought one… six buckets.</ta>
            <ta e="T796" id="Seg_11377" s="T792">I got up early today.</ta>
            <ta e="T800" id="Seg_11378" s="T796">I worked the whole day.</ta>
            <ta e="T802" id="Seg_11379" s="T800">I got tired.</ta>
            <ta e="T808" id="Seg_11380" s="T802">The evening came, I fell asleep.</ta>
            <ta e="T814" id="Seg_11381" s="T808">I don't want to see them.</ta>
            <ta e="T819" id="Seg_11382" s="T814">They always lie.</ta>
            <ta e="T821" id="Seg_11383" s="T819">They [always] lie.</ta>
            <ta e="T826" id="Seg_11384" s="T821">I don't want even hear them.</ta>
            <ta e="T831" id="Seg_11385" s="T826">Let them not come to me!</ta>
            <ta e="T835" id="Seg_11386" s="T831">I don't want them.</ta>
            <ta e="T838" id="Seg_11387" s="T835">My head, my hair, my ears.</ta>
            <ta e="T839" id="Seg_11388" s="T838">My teeth.</ta>
            <ta e="T841" id="Seg_11389" s="T839">My lips, my tongue.</ta>
            <ta e="T843" id="Seg_11390" s="T841">My eyes, my nose.</ta>
            <ta e="T845" id="Seg_11391" s="T843">My arms/hands, my back [shoulders].</ta>
            <ta e="T847" id="Seg_11392" s="T845">My heart.</ta>
            <ta e="T849" id="Seg_11393" s="T847">My stomach, my belly.</ta>
            <ta e="T853" id="Seg_11394" s="T849">My shoulders, my (his?) back, my legs.</ta>
            <ta e="T856" id="Seg_11395" s="T853">[My] mouth, (my heart) his heart.</ta>
            <ta e="T858" id="Seg_11396" s="T857">Her breasts.</ta>
            <ta e="T861" id="Seg_11397" s="T859">His/her bones.</ta>
            <ta e="T863" id="Seg_11398" s="T862">Blood.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-PKZ">
            <ta e="T10" id="Seg_11399" s="T4">Wenn du hierher kommst, kaufe mir eine Brille.</ta>
            <ta e="T14" id="Seg_11400" s="T10">Wohin ist dein Mann gegangen?</ta>
            <ta e="T18" id="Seg_11401" s="T14">Er ist in die Sajan-Berge gegangen, [zu] Pferde.</ta>
            <ta e="T20" id="Seg_11402" s="T18">Ist er alleine gegangen?</ta>
            <ta e="T24" id="Seg_11403" s="T20">Nein, zwei [Leute] sind gegangen.</ta>
            <ta e="T27" id="Seg_11404" s="T24">Wann sind sie gegangen?</ta>
            <ta e="T28" id="Seg_11405" s="T27">Warte.</ta>
            <ta e="T32" id="Seg_11406" s="T28">Sie sind gestern gegangen, sehr [früh] am Morgen.</ta>
            <ta e="T40" id="Seg_11407" s="T32">Warum lügst du, du (?) nicht.</ta>
            <ta e="T43" id="Seg_11408" s="T40">Tu es ans Fenster. [?]</ta>
            <ta e="T46" id="Seg_11409" s="T43">Mein Hemd ist zerrissen.</ta>
            <ta e="T48" id="Seg_11410" s="T46">Ein Loch ist entstanden.</ta>
            <ta e="T54" id="Seg_11411" s="T48">Dann nähte ich es mit Fäden.</ta>
            <ta e="T56" id="Seg_11412" s="T54">Mit einer Nadel, einem Fingerhut.</ta>
            <ta e="T60" id="Seg_11413" s="T56">Die Glocke läutet in der Kirche.</ta>
            <ta e="T66" id="Seg_11414" s="T60">[Wir] sollten gehen, um zu Gott zu beten.</ta>
            <ta e="T69" id="Seg_11415" s="T66">Sie rufen.</ta>
            <ta e="T72" id="Seg_11416" s="T69">Häng die Glocke an das Pferd!</ta>
            <ta e="T78" id="Seg_11417" s="T72">[Ich] habe keine Ahnung, ich weiß nichts.</ta>
            <ta e="T85" id="Seg_11418" s="T78">Ich kann nichts erzählen, ich habe keine Ahnung.</ta>
            <ta e="T87" id="Seg_11419" s="T85">Ich ging in die Taiga.</ta>
            <ta e="T92" id="Seg_11420" s="T87">[Ich] verirrte mich.</ta>
            <ta e="T96" id="Seg_11421" s="T92">Ich ging in die Taiga, ich verirrte mich.</ta>
            <ta e="T99" id="Seg_11422" s="T96">Dann schrie ich.</ta>
            <ta e="T101" id="Seg_11423" s="T99">Dann kam ich.</ta>
            <ta e="T103" id="Seg_11424" s="T101">[Ich] saß eine lange Zeit.</ta>
            <ta e="T109" id="Seg_11425" s="T103">Nichts… [Ich] sagte nichts.</ta>
            <ta e="T116" id="Seg_11426" s="T109">Er geht, und ich hole ihn ein.</ta>
            <ta e="T120" id="Seg_11427" s="T116">"Geh nicht, komm hierher zurück!</ta>
            <ta e="T125" id="Seg_11428" s="T120">Ich sage dir, komm hierher zurück!"</ta>
            <ta e="T130" id="Seg_11429" s="T125">Es sind viele Leute bei mir. [?]</ta>
            <ta e="T134" id="Seg_11430" s="T130">[Wir] brauchen viel Brot.</ta>
            <ta e="T137" id="Seg_11431" s="T134">Nimm es auf den Rücken!</ta>
            <ta e="T140" id="Seg_11432" s="T137">Lege deine Hände nicht. [?]</ta>
            <ta e="T146" id="Seg_11433" s="T140">Berühre ihn mit dem Hand. [?]</ta>
            <ta e="T154" id="Seg_11434" s="T146">Ich nehme viel für mich selbst, und für ihn nahm ich wenig.</ta>
            <ta e="T161" id="Seg_11435" s="T154">Er nimmt viel für sich selbst, und für ihn/sie nimmt er wenig.</ta>
            <ta e="T164" id="Seg_11436" s="T161">We sprachen zu sechs.</ta>
            <ta e="T170" id="Seg_11437" s="T164">[Als] wir klein waren, spielten wir immer.</ta>
            <ta e="T177" id="Seg_11438" s="T170">[Eine(r)] nimmt Stöcker, und eine(r) [von uns] rennt. [?]</ta>
            <ta e="T181" id="Seg_11439" s="T177">Und wir verstecken uns.</ta>
            <ta e="T191" id="Seg_11440" s="T181">Dann sucht er/sie, sucht er/sie [uns], er/sie kann [uns] nicht finden.</ta>
            <ta e="T195" id="Seg_11441" s="T191">[Dann] spielen wir wieder.</ta>
            <ta e="T200" id="Seg_11442" s="T195">We stritten uns nicht.</ta>
            <ta e="T203" id="Seg_11443" s="T200">Also…</ta>
            <ta e="T207" id="Seg_11444" s="T204">Gut…</ta>
            <ta e="T213" id="Seg_11445" s="T209">Wir spielten gut.</ta>
            <ta e="T216" id="Seg_11446" s="T213">Wir waren nicht böse.</ta>
            <ta e="T220" id="Seg_11447" s="T216">Männer und Frauen saßen da.</ta>
            <ta e="T225" id="Seg_11448" s="T220">Und sie schauten, wie wir spielten.</ta>
            <ta e="T233" id="Seg_11449" s="T225">Einige rauchten Tabak, einige rauchten nicht.</ta>
            <ta e="T242" id="Seg_11450" s="T233">Einige rauchten Pfeife, einige rauchten nicht.</ta>
            <ta e="T249" id="Seg_11451" s="T242">Sehr viel Schafe, ganze zweiundzwanzig.</ta>
            <ta e="T252" id="Seg_11452" s="T249">Mein Vater ging in den Wald.</ta>
            <ta e="T255" id="Seg_11453" s="T252">Er tötete viele Hühner/Auerhühner.</ta>
            <ta e="T258" id="Seg_11454" s="T255">Und er brachte [sie] nach Hause.</ta>
            <ta e="T261" id="Seg_11455" s="T258">Ich gehe nach Permjakovo.</ta>
            <ta e="T265" id="Seg_11456" s="T261">Ich sehe: dort steht ein Elch.</ta>
            <ta e="T271" id="Seg_11457" s="T265">Ich schaute eine Weile, dann schrie ich.</ta>
            <ta e="T275" id="Seg_11458" s="T271">Es rannte [weg] zum Fluss.</ta>
            <ta e="T276" id="Seg_11459" s="T275">(Schlange?).</ta>
            <ta e="T282" id="Seg_11460" s="T276">Dann (?) es und rannte davon.</ta>
            <ta e="T287" id="Seg_11461" s="T282">Ich kam, sagte [zu] Wlas.</ta>
            <ta e="T293" id="Seg_11462" s="T287">Er ging [und] tötete es mit dem Gewehr.</ta>
            <ta e="T295" id="Seg_11463" s="T293">… ein Kessel.</ta>
            <ta e="T299" id="Seg_11464" s="T295">Dann wusch ich das Fleisch.</ta>
            <ta e="T301" id="Seg_11465" s="T299">Und ich aß [es].</ta>
            <ta e="T305" id="Seg_11466" s="T301">Uns wurde ein Kessel gegeben.</ta>
            <ta e="T314" id="Seg_11467" s="T305">Ein finnische Mann kam, ich ging dorthin, sie unterhielten sich.</ta>
            <ta e="T321" id="Seg_11468" s="T314">Sie jagten mich weg, ich rannte davon.</ta>
            <ta e="T332" id="Seg_11469" s="T321">[Als] die Kamassen alleine lebten, war es gut, da gab es viel Fleisch, du hast [es alles] gegessen.</ta>
            <ta e="T338" id="Seg_11470" s="T332">Rentierfleisch ist sehr weich, wie Brot.</ta>
            <ta e="T343" id="Seg_11471" s="T338">Du hast viel gegessen, dann hast du Wasser getrunken.</ta>
            <ta e="T348" id="Seg_11472" s="T343">Du hast Wasser getrunken…</ta>
            <ta e="T357" id="Seg_11473" s="T348">Dieser Häuptling ist sehr gut, sehr schön, seine Augen sind schwarz.</ta>
            <ta e="T360" id="Seg_11474" s="T357">Seine Nase ist nicht lang.</ta>
            <ta e="T363" id="Seg_11475" s="T360">Seine Lippen sind schön.</ta>
            <ta e="T366" id="Seg_11476" s="T363">Seine Lippen sind nicht dick.</ta>
            <ta e="T369" id="Seg_11477" s="T366">Matrjona war krank.</ta>
            <ta e="T374" id="Seg_11478" s="T369">Ich gehe zu ihr.</ta>
            <ta e="T385" id="Seg_11479" s="T374">Sie sagte: "Sprich kein Russisch, sprich [deine] eigene Sprache.</ta>
            <ta e="T389" id="Seg_11480" s="T385">Ich bin sehr krank.</ta>
            <ta e="T392" id="Seg_11481" s="T389">Ich bin sehr krank.</ta>
            <ta e="T399" id="Seg_11482" s="T392">Ich werde sterben und wirst bleiben."</ta>
            <ta e="T405" id="Seg_11483" s="T399">Sie starb, zwei Töchter von ihr blieben.</ta>
            <ta e="T412" id="Seg_11484" s="T405">Eine [von ihnen] war bei ihr, eine war weit weg.</ta>
            <ta e="T415" id="Seg_11485" s="T412">Heute…</ta>
            <ta e="T424" id="Seg_11486" s="T419">Heute hat es den ganzen Tag geregnet.</ta>
            <ta e="T428" id="Seg_11487" s="T424">Und jetzt scheint die Sonne.</ta>
            <ta e="T435" id="Seg_11488" s="T428">Novik reitet ein Schwein und Sidor sitzt auf einem Hund.</ta>
            <ta e="T440" id="Seg_11489" s="T435">Und Leute sahen [sie] und dachten: Kosaken.</ta>
            <ta e="T445" id="Seg_11490" s="T440">The Vorsitzende sagte: "[Wir] sollten die Mühle (reparieren?).</ta>
            <ta e="T453" id="Seg_11491" s="T445">Sammel gute Leute, lass sie dort arbeiten.</ta>
            <ta e="T461" id="Seg_11492" s="T453">Wir sollten zur Mühle gehen, um das Mehl zu mahlen, weil [wir] kein Mehl haben.</ta>
            <ta e="T473" id="Seg_11493" s="T461">Ich war klein und die Mädchen waren groß, sie nahmen mich immer mit.</ta>
            <ta e="T481" id="Seg_11494" s="T473">Um Lieder zu singen, meine Stimme war gut.</ta>
            <ta e="T483" id="Seg_11495" s="T481">Ich sang.</ta>
            <ta e="T488" id="Seg_11496" s="T483">Dieser Mann singt sehr gut.</ta>
            <ta e="T493" id="Seg_11497" s="T488">Seine Stimme ist schön.</ta>
            <ta e="T500" id="Seg_11498" s="T493">Stimme… Nein, nicht so…</ta>
            <ta e="T503" id="Seg_11499" s="T500">Er singt sehr laut.</ta>
            <ta e="T512" id="Seg_11500" s="T503">Dieser Mann starb, [wir] sollten einen Sarg machen, aus Holz.</ta>
            <ta e="T516" id="Seg_11501" s="T512">Und ihn hineinlegen.</ta>
            <ta e="T519" id="Seg_11502" s="T516">Und ein Kreuz machen.</ta>
            <ta e="T524" id="Seg_11503" s="T519">Jetzt wird er in dieses Land gehen.</ta>
            <ta e="T528" id="Seg_11504" s="T524">[Dort] stehen sehr schöne Häuser.</ta>
            <ta e="T535" id="Seg_11505" s="T528">Sie sind [aus] Steinen gemacht.</ta>
            <ta e="T542" id="Seg_11506" s="T535">Sie haben Bäume neben das Fenster gepflanzt.</ta>
            <ta e="T544" id="Seg_11507" s="T542">Sie haben Bäume gepflanzt.</ta>
            <ta e="T546" id="Seg_11508" s="T544">Sie haben Himbeeren gepflanzt.</ta>
            <ta e="T551" id="Seg_11509" s="T546">Sehr schöne Häuser stehen [dort].</ta>
            <ta e="T555" id="Seg_11510" s="T551">Sie haben Traubenkirschenbäume gepflanzt.</ta>
            <ta e="T563" id="Seg_11511" s="T555">Links [ist es] schön, rechts [ist es] schön.</ta>
            <ta e="T566" id="Seg_11512" s="T563">Auf der linken Seite.</ta>
            <ta e="T571" id="Seg_11513" s="T566">Sehr schöne Häuser stehen dort.</ta>
            <ta e="T575" id="Seg_11514" s="T571">Meine (Schwägerin?) hat Selbstgebrannten gemacht.</ta>
            <ta e="T579" id="Seg_11515" s="T575">Er ist nicht stark.</ta>
            <ta e="T582" id="Seg_11516" s="T579">Ein Mann schimpft:</ta>
            <ta e="T590" id="Seg_11517" s="T582">"Du wirst ihn nicht mehr brennen, er ist nicht gut, nicht stark." </ta>
            <ta e="T597" id="Seg_11518" s="T590">Dann kochte meine Schwägerin Wasser, mit Salz.</ta>
            <ta e="T601" id="Seg_11519" s="T597">Dann schnitt sie Fleisch.</ta>
            <ta e="T606" id="Seg_11520" s="T601">Und tat es dorthin, um [es] zu salzen.</ta>
            <ta e="T609" id="Seg_11521" s="T606">Der Hausherr, die Frau des Hausherrn.</ta>
            <ta e="T614" id="Seg_11522" s="T609">Dort ist kein Weg zum Gehen, [dort ist] zu viel Wasser.</ta>
            <ta e="T616" id="Seg_11523" s="T614">Man sollte zurück gehen.</ta>
            <ta e="T619" id="Seg_11524" s="T616">Man kann sehr weit gucken.</ta>
            <ta e="T627" id="Seg_11525" s="T619">Das ist die Steppe, dort gibt es keine Bäume, nicht einen einzigen Baum.</ta>
            <ta e="T631" id="Seg_11526" s="T627">Wenn ich durch die Steppe gehe, singe ich [über] die Steppe.</ta>
            <ta e="T636" id="Seg_11527" s="T631">[Wenn] ich in die Taiga gehe, singe ich [über] die Taiga.</ta>
            <ta e="T640" id="Seg_11528" s="T636">[Wenn] ich zum Fluss gehe, singe ich [über] den Fluss.</ta>
            <ta e="T644" id="Seg_11529" s="T640">[Wenn] ich durch den Wald gehe, singe ich [über] den Wald.</ta>
            <ta e="T648" id="Seg_11530" s="T644">Mein Vater ging nach Kirel.</ta>
            <ta e="T650" id="Seg_11531" s="T648">[Er] nahm ein Boot.</ta>
            <ta e="T654" id="Seg_11532" s="T650">Er fing viel Fisch.</ta>
            <ta e="T662" id="Seg_11533" s="T654">Wir schuppten ihn ab, salzten, brieten und aßen ihn.</ta>
            <ta e="T668" id="Seg_11534" s="T662">Wir (gingen?), das Wasser ist sehr groß [= hoch].</ta>
            <ta e="T674" id="Seg_11535" s="T668">Wir stellten ein (Netz?), fingen Fisch.</ta>
            <ta e="T682" id="Seg_11536" s="T674">Ein Mann erwürgte eine Frau und hängte sich selbst an einem Baum auf.</ta>
            <ta e="T687" id="Seg_11537" s="T682">Der Bär hat viele Haare.</ta>
            <ta e="T691" id="Seg_11538" s="T687">Man kann nicht einmal das Fleisch sehen.</ta>
            <ta e="T696" id="Seg_11539" s="T691">Sie [= die Haare] sind sehr weich, sehr fett.</ta>
            <ta e="T700" id="Seg_11540" s="T696">Seine Beine sind stark.</ta>
            <ta e="T703" id="Seg_11541" s="T700">Seine Klauen sind sehr groß.</ta>
            <ta e="T706" id="Seg_11542" s="T703">Größer als ein Hund.</ta>
            <ta e="T708" id="Seg_11543" s="T706">Größer als ein Hund.</ta>
            <ta e="T710" id="Seg_11544" s="T708">Seine Augen sind klein.</ta>
            <ta e="T712" id="Seg_11545" s="T710">[Seine] Ohren sind klein.</ta>
            <ta e="T715" id="Seg_11546" s="T712">Meine Verwandten sind gekommen.</ta>
            <ta e="T718" id="Seg_11547" s="T715">[Mein] Sohn kam mit [seiner] Frau.</ta>
            <ta e="T721" id="Seg_11548" s="T718">Mein Bruder [= Schwager] kam mit [seiner] Frau.</ta>
            <ta e="T723" id="Seg_11549" s="T721">Kinder…</ta>
            <ta e="T726" id="Seg_11550" s="T723">Meine Verwandten kamen.</ta>
            <ta e="T729" id="Seg_11551" s="T726">[Mein] Sohn kam mit [seiner] Frau.</ta>
            <ta e="T732" id="Seg_11552" s="T729">Mein Bruder [= Schwager?] kam mit [seiner] Frau.</ta>
            <ta e="T735" id="Seg_11553" s="T732">Meine Tochter kam mit ihrem Mann.</ta>
            <ta e="T739" id="Seg_11554" s="T735">Ihre Kinder kamen mit ihr.</ta>
            <ta e="T747" id="Seg_11555" s="T739">[Ich] muss [Essen] für sie sammeln, ihnen zu essen geben.</ta>
            <ta e="T755" id="Seg_11556" s="T747">Mein Sohn ist sehr krank, ich sagte zu ihm:</ta>
            <ta e="T757" id="Seg_11557" s="T755">"Bete zu Gott.</ta>
            <ta e="T760" id="Seg_11558" s="T757">Bitte ihn um Kraft.</ta>
            <ta e="T762" id="Seg_11559" s="T760">Ruf ihn zu dir."</ta>
            <ta e="T767" id="Seg_11560" s="T762">Er rief: "Komm zu mir.</ta>
            <ta e="T769" id="Seg_11561" s="T767">Gib [mir] Kraft!</ta>
            <ta e="T771" id="Seg_11562" s="T769">Heil mich!"</ta>
            <ta e="T775" id="Seg_11563" s="T771">Gestern putzte ich [mein] Haus.</ta>
            <ta e="T779" id="Seg_11564" s="T775">Ein Huhn hat gekotet, ich putzte [das] auch.</ta>
            <ta e="T782" id="Seg_11565" s="T779">Dann putzte ich die Veranda.</ta>
            <ta e="T787" id="Seg_11566" s="T782">Dann ging ich, um Wasser in das Badehaus zu bringen.</ta>
            <ta e="T792" id="Seg_11567" s="T787">Ich habe einen… sechs Eimer gebracht.</ta>
            <ta e="T796" id="Seg_11568" s="T792">Ich bin heute früh aufgestanden.</ta>
            <ta e="T800" id="Seg_11569" s="T796">Ich habe den ganzen Tag gearbeitet.</ta>
            <ta e="T802" id="Seg_11570" s="T800">Ich bin müde.</ta>
            <ta e="T808" id="Seg_11571" s="T802">Es wurde Abend, ich schlief ein.</ta>
            <ta e="T814" id="Seg_11572" s="T808">Ich möchte sie nicht sehen/rufen.</ta>
            <ta e="T819" id="Seg_11573" s="T814">Sie lügen immer.</ta>
            <ta e="T821" id="Seg_11574" s="T819">Sie lügen [immer].</ta>
            <ta e="T826" id="Seg_11575" s="T821">Ich möchte sie nicht einmal hören.</ta>
            <ta e="T831" id="Seg_11576" s="T826">Lass sie nicht zu mir kommen!</ta>
            <ta e="T835" id="Seg_11577" s="T831">Ich möchte sie nicht.</ta>
            <ta e="T838" id="Seg_11578" s="T835">Mein Kopf, meine Haare, meine Ohren.</ta>
            <ta e="T839" id="Seg_11579" s="T838">Meine Zähne.</ta>
            <ta e="T841" id="Seg_11580" s="T839">Meine Lippen, meine Zunge.</ta>
            <ta e="T843" id="Seg_11581" s="T841">Meine Augen, meine Nase.</ta>
            <ta e="T845" id="Seg_11582" s="T843">Meine Arme/Hände, mein Rücke [Schultern].</ta>
            <ta e="T847" id="Seg_11583" s="T845">Mein Herz.</ta>
            <ta e="T849" id="Seg_11584" s="T847">Mein Magen, mein Bauch.</ta>
            <ta e="T853" id="Seg_11585" s="T849">Meine Schultern, mein (sein?) Rücken, meinе Beinе.</ta>
            <ta e="T856" id="Seg_11586" s="T853">[Mein] Mund, (mein Herz) sein Herz.</ta>
            <ta e="T858" id="Seg_11587" s="T857">Ihre Brüste.</ta>
            <ta e="T861" id="Seg_11588" s="T859">Seine/ihre Knochen.</ta>
            <ta e="T863" id="Seg_11589" s="T862">Blut.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-PKZ">
            <ta e="T10" id="Seg_11590" s="T4">[GVY:] or măna simajiʔ 'take a picture of me' from the Russ. сымать?</ta>
            <ta e="T43" id="Seg_11591" s="T40">[GVY:] KT5:14. [GVY:] Both in KT5 and on a card in the Ekaterinburg archive there is Köžəsnek tăn nuldʼit "Окошко[?] ты вставь".</ta>
            <ta e="T54" id="Seg_11592" s="T48">Нитка Ru. ’thread’</ta>
            <ta e="T60" id="Seg_11593" s="T56">KT5:16.</ta>
            <ta e="T72" id="Seg_11594" s="T69">KT5:17. [GVY:] [kongoro] KT5: Ej detəʔ ine kuŋguro 'The horse didn't bring the bell'.</ta>
            <ta e="T78" id="Seg_11595" s="T72">KT5: 18</ta>
            <ta e="T87" id="Seg_11596" s="T85">KT5:19.</ta>
            <ta e="T92" id="Seg_11597" s="T87">[KlT:] 3SG instead of 1SG / 1SG marker just missing</ta>
            <ta e="T130" id="Seg_11598" s="T125">KT5:20</ta>
            <ta e="T137" id="Seg_11599" s="T134">KT5:21 [GVY:] In KT5: this and the next sentence are together: "И на-спину ему руку не клади".</ta>
            <ta e="T140" id="Seg_11600" s="T137">KT5:21 end</ta>
            <ta e="T154" id="Seg_11601" s="T146">KT5:22. [GVY:] Boskənə = boskənʼi?</ta>
            <ta e="T164" id="Seg_11602" s="T161">KT5:23-KT5:23 end</ta>
            <ta e="T170" id="Seg_11603" s="T164">KT3:19</ta>
            <ta e="T177" id="Seg_11604" s="T170">[GVY:] oj - Ru ой?</ta>
            <ta e="T195" id="Seg_11605" s="T191">[GVY:] sarlaʔbəbaʔ = sʼarlaʔbəbaʔ</ta>
            <ta e="T200" id="Seg_11606" s="T195">[GVY:] Note that b after ʔ pronounced after a pause sounds voiced.</ta>
            <ta e="T216" id="Seg_11607" s="T213">[GVY:] kurollubibaʔ = kurolluʔpibaʔ?</ta>
            <ta e="T242" id="Seg_11608" s="T233">KT3:19-end. [GVY:] kanzaʔsi</ta>
            <ta e="T249" id="Seg_11609" s="T242">KT5:24. [GVY:] KT5: or 'forty'?</ta>
            <ta e="T255" id="Seg_11610" s="T252">[GVY:] KT5: 'wood-grouses' (глухари). </ta>
            <ta e="T258" id="Seg_11611" s="T255">KT5:24-end</ta>
            <ta e="T282" id="Seg_11612" s="T276">[GVY:] or dibər (kaluʔpi)?</ta>
            <ta e="T299" id="Seg_11613" s="T295">KT5:25</ta>
            <ta e="T305" id="Seg_11614" s="T301">KT5:25-end</ta>
            <ta e="T314" id="Seg_11615" s="T305">[GVY:] mĭmbieŋ = mĭmbiem.</ta>
            <ta e="T332" id="Seg_11616" s="T321">KT5:26</ta>
            <ta e="T357" id="Seg_11617" s="T348">KT5:27.</ta>
            <ta e="T363" id="Seg_11618" s="T360">KT5:27-end</ta>
            <ta e="T369" id="Seg_11619" s="T366">[GVY:] KT5:28</ta>
            <ta e="T412" id="Seg_11620" s="T405">[GVY:] kuŋgəŋ?</ta>
            <ta e="T424" id="Seg_11621" s="T419">KT5:29</ta>
            <ta e="T428" id="Seg_11622" s="T424">[GVY:] kujo = kuja 'sun'</ta>
            <ta e="T435" id="Seg_11623" s="T428">KT5:30</ta>
            <ta e="T445" id="Seg_11624" s="T440">TK15: 31. [GVY:] tʼerbəndə</ta>
            <ta e="T473" id="Seg_11625" s="T461">KT5:32</ta>
            <ta e="T512" id="Seg_11626" s="T503">KT5:33 [GVY:] Dĭ here means 'вот'?</ta>
            <ta e="T519" id="Seg_11627" s="T516">[GVY:] KT5:33 end</ta>
            <ta e="T528" id="Seg_11628" s="T524">KT5:34</ta>
            <ta e="T551" id="Seg_11629" s="T546">[GVY:] nugaʔbəʔjə = nulaʔbəʔjə</ta>
            <ta e="T575" id="Seg_11630" s="T571">KT5:35</ta>
            <ta e="T590" id="Seg_11631" s="T582">[GVY:] ej iləl mĭnzərzittə: KT5: "не возьмешь варить" 'you won't take to cook'. An alternative reading would be 'you won't cook' after the Russian pattern "не будешь варить"</ta>
            <ta e="T606" id="Seg_11632" s="T601">KT5:35-end.</ta>
            <ta e="T614" id="Seg_11633" s="T609">KT5:36</ta>
            <ta e="T619" id="Seg_11634" s="T616">[GVY:] KT5: 37.</ta>
            <ta e="T631" id="Seg_11635" s="T627">[GVY:] KT5:38</ta>
            <ta e="T648" id="Seg_11636" s="T644">KT5:39 [GVY:] Kirel is a river vear the village of Kan.</ta>
            <ta e="T650" id="Seg_11637" s="T648">[GVY:] or 'He had a boat.'</ta>
            <ta e="T668" id="Seg_11638" s="T662">[GVY:] KT5:40. Mĭmbieʔ: cf. iʔpəbiaʔ in the next sentence. Or = mĭmbijəʔ (3PL instead of 1PL)?</ta>
            <ta e="T674" id="Seg_11639" s="T668">[GVY:] KT5: "sedʼiʔi bar əʔBabʼaʔ".</ta>
            <ta e="T682" id="Seg_11640" s="T674">[GVY:] KT5:41</ta>
            <ta e="T687" id="Seg_11641" s="T682">KT5:42. [GVY:] Tĭrdə?</ta>
            <ta e="T691" id="Seg_11642" s="T687">[GVY:] or rather 'its body'</ta>
            <ta e="T696" id="Seg_11643" s="T691">[GVY:] KT5: "Очень он мягкий, очень жирный", that is, it is about the bear itself and not about its hair.</ta>
            <ta e="T712" id="Seg_11644" s="T710">K5:42-end</ta>
            <ta e="T715" id="Seg_11645" s="T712">Beginning of a fragment that is repeated after.</ta>
            <ta e="T726" id="Seg_11646" s="T723">KT5:43</ta>
            <ta e="T755" id="Seg_11647" s="T747">KT5:44</ta>
            <ta e="T769" id="Seg_11648" s="T767">[GVY:] šăldi?</ta>
            <ta e="T771" id="Seg_11649" s="T769">[GVY:] KT15: a literal translation "have pity of me" (maybe by analogy with ajir- 'have pity'). </ta>
            <ta e="T775" id="Seg_11650" s="T771">KT5:45</ta>
            <ta e="T796" id="Seg_11651" s="T792">KT5:46</ta>
            <ta e="T814" id="Seg_11652" s="T808">[GVY:] Künnap "to call them".</ta>
            <ta e="T819" id="Seg_11653" s="T814">KT5:47</ta>
            <ta e="T831" id="Seg_11654" s="T826">KT5:48 [GVY:] This seems to be a continuation of the preceding fragment.</ta>
            <ta e="T835" id="Seg_11655" s="T831">[GVY:] KT5:48 end. </ta>
            <ta e="T853" id="Seg_11656" s="T849">[GVY:] the distribution of -bə and -də possessive markers in KP's speech is not yet clear.</ta>
            <ta e="T861" id="Seg_11657" s="T859">[GVY:] The first e- must be accidental.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T869" />
            <conversion-tli id="T17" />
            <conversion-tli id="T875" />
            <conversion-tli id="T18" />
            <conversion-tli id="T876" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T870" />
            <conversion-tli id="T23" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T879" />
            <conversion-tli id="T27" />
            <conversion-tli id="T880" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T883" />
            <conversion-tli id="T40" />
            <conversion-tli id="T884" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T885" />
            <conversion-tli id="T43" />
            <conversion-tli id="T886" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T887" />
            <conversion-tli id="T56" />
            <conversion-tli id="T888" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T891" />
            <conversion-tli id="T72" />
            <conversion-tli id="T892" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T897" />
            <conversion-tli id="T92" />
            <conversion-tli id="T898" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T899" />
            <conversion-tli id="T101" />
            <conversion-tli id="T900" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T901" />
            <conversion-tli id="T109" />
            <conversion-tli id="T902" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T907" />
            <conversion-tli id="T134" />
            <conversion-tli id="T908" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T909" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T910" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T911" />
            <conversion-tli id="T146" />
            <conversion-tli id="T912" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T913" />
            <conversion-tli id="T154" />
            <conversion-tli id="T914" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T915" />
            <conversion-tli id="T161" />
            <conversion-tli id="T916" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T919" />
            <conversion-tli id="T195" />
            <conversion-tli id="T920" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T921" />
            <conversion-tli id="T209" />
            <conversion-tli id="T922" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T927" />
            <conversion-tli id="T225" />
            <conversion-tli id="T928" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T929" />
            <conversion-tli id="T242" />
            <conversion-tli id="T930" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T931" />
            <conversion-tli id="T249" />
            <conversion-tli id="T932" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T935" />
            <conversion-tli id="T293" />
            <conversion-tli id="T936" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T937" />
            <conversion-tli id="T938" />
            <conversion-tli id="T305" />
            <conversion-tli id="T866" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T939" />
            <conversion-tli id="T321" />
            <conversion-tli id="T940" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T941" />
            <conversion-tli id="T332" />
            <conversion-tli id="T942" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T865" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T945" />
            <conversion-tli id="T363" />
            <conversion-tli id="T946" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T947" />
            <conversion-tli id="T366" />
            <conversion-tli id="T948" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T949" />
            <conversion-tli id="T392" />
            <conversion-tli id="T950" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T951" />
            <conversion-tli id="T399" />
            <conversion-tli id="T952" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T953" />
            <conversion-tli id="T954" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T955" />
            <conversion-tli id="T956" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T957" />
            <conversion-tli id="T958" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T959" />
            <conversion-tli id="T960" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T961" />
            <conversion-tli id="T962" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T963" />
            <conversion-tli id="T461" />
            <conversion-tli id="T964" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T965" />
            <conversion-tli id="T966" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T967" />
            <conversion-tli id="T500" />
            <conversion-tli id="T968" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T969" />
            <conversion-tli id="T970" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T971" />
            <conversion-tli id="T519" />
            <conversion-tli id="T972" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T973" />
            <conversion-tli id="T974" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T975" />
            <conversion-tli id="T976" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T867" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T977" />
            <conversion-tli id="T978" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T979" />
            <conversion-tli id="T571" />
            <conversion-tli id="T980" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T981" />
            <conversion-tli id="T590" />
            <conversion-tli id="T982" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T983" />
            <conversion-tli id="T984" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T985" />
            <conversion-tli id="T616" />
            <conversion-tli id="T986" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T868" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T987" />
            <conversion-tli id="T988" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T989" />
            <conversion-tli id="T990" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T991" />
            <conversion-tli id="T992" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T993" />
            <conversion-tli id="T994" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T995" />
            <conversion-tli id="T996" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T997" />
            <conversion-tli id="T998" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T999" />
            <conversion-tli id="T1000" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T1001" />
            <conversion-tli id="T723" />
            <conversion-tli id="T1002" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T1003" />
            <conversion-tli id="T1004" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T1005" />
            <conversion-tli id="T771" />
            <conversion-tli id="T1006" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T1007" />
            <conversion-tli id="T792" />
            <conversion-tli id="T1008" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T1009" />
            <conversion-tli id="T1010" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T1011" />
            <conversion-tli id="T1012" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T1013" />
            <conversion-tli id="T1014" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T1015" />
            <conversion-tli id="T1016" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T1017" />
            <conversion-tli id="T847" />
            <conversion-tli id="T1018" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T1019" />
            <conversion-tli id="T1020" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T1021" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-KA"
                          name="ref"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KA"
                          name="ts"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KA"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KA"
                          type="t" />
         <conversion-tier category="CS"
                          display-name="CS-KA"
                          name="CS"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KA"
                          name="fr"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KA"
                          name="fe"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KA"
                          name="fg"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KA"
                          name="nt"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-PKZ"
                          name="ref"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-PKZ"
                          name="ts"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-PKZ"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-PKZ"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-PKZ"
                          name="mb"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-PKZ"
                          name="mp"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-PKZ"
                          name="ge"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-PKZ"
                          name="gr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-PKZ"
                          name="mc"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-PKZ"
                          name="ps"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-PKZ"
                          name="SeR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-PKZ"
                          name="SyF"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-PKZ"
                          name="IST"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-PKZ"
                          name="BOR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-PKZ"
                          name="BOR-Phon"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-PKZ"
                          name="BOR-Morph"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-PKZ"
                          name="CS"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-PKZ"
                          name="fr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-PKZ"
                          name="fe"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-PKZ"
                          name="fg"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-PKZ"
                          name="nt"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
