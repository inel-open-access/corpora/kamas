<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDCD22D3F6-12FC-B660-E225-4903AEEA4237">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_SU0218.wav" />
         <referenced-file url="PKZ_196X_SU0218.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_196X_SU0218\PKZ_196X_SU0218.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1656</ud-information>
            <ud-information attribute-name="# HIAT:w">945</ud-information>
            <ud-information attribute-name="# e">981</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">38</ud-information>
            <ud-information attribute-name="# HIAT:u">231</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.07" type="appl" />
         <tli id="T1" time="0.923" type="appl" />
         <tli id="T2" time="1.775" type="appl" />
         <tli id="T3" time="2.628" type="appl" />
         <tli id="T4" time="3.646643489537015" />
         <tli id="T5" time="4.713" type="appl" />
         <tli id="T6" time="5.717" type="appl" />
         <tli id="T7" time="6.72" type="appl" />
         <tli id="T8" time="7.723" type="appl" />
         <tli id="T9" time="8.727" type="appl" />
         <tli id="T10" time="9.73" type="appl" />
         <tli id="T11" time="10.22" type="appl" />
         <tli id="T12" time="10.71" type="appl" />
         <tli id="T13" time="11.2" type="appl" />
         <tli id="T14" time="12.047" type="appl" />
         <tli id="T15" time="12.893" type="appl" />
         <tli id="T16" time="13.74" type="appl" />
         <tli id="T17" time="14.62" type="appl" />
         <tli id="T18" time="15.29" type="appl" />
         <tli id="T19" time="15.96" type="appl" />
         <tli id="T20" time="16.63" type="appl" />
         <tli id="T21" time="18.786547264196177" />
         <tli id="T22" time="19.315" type="appl" />
         <tli id="T23" time="19.95" type="appl" />
         <tli id="T24" time="20.585" type="appl" />
         <tli id="T25" time="21.22" type="appl" />
         <tli id="T26" time="21.855" type="appl" />
         <tli id="T27" time="22.49" type="appl" />
         <tli id="T28" time="23.125" type="appl" />
         <tli id="T29" time="23.76" type="appl" />
         <tli id="T30" time="25.045" type="appl" />
         <tli id="T31" time="26.03" type="appl" />
         <tli id="T32" time="27.382" type="appl" />
         <tli id="T33" time="28.585" type="appl" />
         <tli id="T34" time="29.787" type="appl" />
         <tli id="T35" time="30.99" type="appl" />
         <tli id="T36" time="32.298" type="appl" />
         <tli id="T37" time="33.346" type="appl" />
         <tli id="T38" time="34.394" type="appl" />
         <tli id="T39" time="35.442" type="appl" />
         <tli id="T40" time="36.49" type="appl" />
         <tli id="T41" time="37.923" type="appl" />
         <tli id="T42" time="38.872" type="appl" />
         <tli id="T43" time="39.82" type="appl" />
         <tli id="T44" time="41.89306707175612" />
         <tli id="T45" time="42.835" type="appl" />
         <tli id="T46" time="43.795" type="appl" />
         <tli id="T47" time="44.755" type="appl" />
         <tli id="T48" time="45.715" type="appl" />
         <tli id="T49" time="46.813035801698206" />
         <tli id="T50" time="47.651" type="appl" />
         <tli id="T51" time="48.322" type="appl" />
         <tli id="T52" time="48.993" type="appl" />
         <tli id="T53" time="49.664" type="appl" />
         <tli id="T54" time="50.335" type="appl" />
         <tli id="T55" time="51.006" type="appl" />
         <tli id="T56" time="51.677" type="appl" />
         <tli id="T57" time="52.407" type="appl" />
         <tli id="T58" time="53.137" type="appl" />
         <tli id="T59" time="54.002" type="appl" />
         <tli id="T60" time="54.823" type="appl" />
         <tli id="T61" time="55.643" type="appl" />
         <tli id="T62" time="56.463" type="appl" />
         <tli id="T63" time="57.284" type="appl" />
         <tli id="T64" time="58.104" type="appl" />
         <tli id="T65" time="58.966" type="appl" />
         <tli id="T66" time="59.712" type="appl" />
         <tli id="T67" time="60.458" type="appl" />
         <tli id="T68" time="61.204" type="appl" />
         <tli id="T69" time="61.95" type="appl" />
         <tli id="T70" time="63.88626062199856" />
         <tli id="T71" time="64.925" type="appl" />
         <tli id="T72" time="65.845" type="appl" />
         <tli id="T73" time="66.765" type="appl" />
         <tli id="T988" time="67.15928571428572" type="intp" />
         <tli id="T74" time="67.685" type="appl" />
         <tli id="T75" time="68.464" type="appl" />
         <tli id="T76" time="69.128" type="appl" />
         <tli id="T77" time="69.792" type="appl" />
         <tli id="T78" time="70.456" type="appl" />
         <tli id="T79" time="71.12" type="appl" />
         <tli id="T80" time="72.067" type="appl" />
         <tli id="T81" time="72.859" type="appl" />
         <tli id="T82" time="73.651" type="appl" />
         <tli id="T83" time="74.443" type="appl" />
         <tli id="T84" time="75.235" type="appl" />
         <tli id="T85" time="75.728" type="appl" />
         <tli id="T86" time="76.176" type="appl" />
         <tli id="T87" time="76.624" type="appl" />
         <tli id="T88" time="77.072" type="appl" />
         <tli id="T89" time="78.06617049813245" />
         <tli id="T90" time="79.093" type="appl" />
         <tli id="T91" time="79.831" type="appl" />
         <tli id="T92" time="80.569" type="appl" />
         <tli id="T93" time="81.307" type="appl" />
         <tli id="T94" time="82.77280725062445" />
         <tli id="T95" time="83.824" type="appl" />
         <tli id="T96" time="84.808" type="appl" />
         <tli id="T97" time="85.792" type="appl" />
         <tli id="T98" time="86.776" type="appl" />
         <tli id="T99" time="87.76" type="appl" />
         <tli id="T100" time="88.744" type="appl" />
         <tli id="T101" time="89.728" type="appl" />
         <tli id="T102" time="90.712" type="appl" />
         <tli id="T103" time="91.696" type="appl" />
         <tli id="T104" time="91.94608228097717" />
         <tli id="T105" time="93.36" type="appl" />
         <tli id="T106" time="93.89" type="appl" />
         <tli id="T107" time="94.42" type="appl" />
         <tli id="T108" time="95.0060628325265" />
         <tli id="T109" time="95.978" type="appl" />
         <tli id="T110" time="96.902" type="appl" />
         <tli id="T111" time="98.40604122313688" />
         <tli id="T112" time="99.194" type="appl" />
         <tli id="T113" time="100.008" type="appl" />
         <tli id="T114" time="100.822" type="appl" />
         <tli id="T115" time="101.636" type="appl" />
         <tli id="T116" time="102.37000040423017" />
         <tli id="T117" time="103.12" type="appl" />
         <tli id="T118" time="103.79" type="appl" />
         <tli id="T119" time="105.7" type="appl" />
         <tli id="T120" time="106.68" type="appl" />
         <tli id="T121" time="107.525" type="appl" />
         <tli id="T122" time="108.37" type="appl" />
         <tli id="T123" time="109.215" type="appl" />
         <tli id="T124" time="110.123" type="appl" />
         <tli id="T125" time="110.717" type="appl" />
         <tli id="T126" time="112.739283458063" />
         <tli id="T127" time="114.715" type="appl" />
         <tli id="T128" time="115.765" type="appl" />
         <tli id="T129" time="116.59" type="appl" />
         <tli id="T130" time="117.316" type="appl" />
         <tli id="T131" time="117.861" type="appl" />
         <tli id="T132" time="118.407" type="appl" />
         <tli id="T133" time="118.953" type="appl" />
         <tli id="T134" time="119.499" type="appl" />
         <tli id="T135" time="120.044" type="appl" />
         <tli id="T136" time="120.59" type="appl" />
         <tli id="T137" time="121.392" type="appl" />
         <tli id="T138" time="122.21255658170486" />
         <tli id="T139" time="122.612" type="appl" />
         <tli id="T140" time="123.034" type="appl" />
         <tli id="T141" time="123.456" type="appl" />
         <tli id="T142" time="123.878" type="appl" />
         <tli id="T143" time="124.38587610197739" />
         <tli id="T144" time="124.865" type="appl" />
         <tli id="T145" time="125.33" type="appl" />
         <tli id="T146" time="125.795" type="appl" />
         <tli id="T147" time="126.26" type="appl" />
         <tli id="T148" time="126.725" type="appl" />
         <tli id="T149" time="127.19" type="appl" />
         <tli id="T150" time="128.063" type="appl" />
         <tli id="T151" time="128.837" type="appl" />
         <tli id="T152" time="129.8791745213165" />
         <tli id="T153" time="130.93250115997617" />
         <tli id="T154" time="131.838" type="appl" />
         <tli id="T155" time="132.672" type="appl" />
         <tli id="T156" time="133.9191488442771" />
         <tli id="T157" time="135.1" type="appl" />
         <tli id="T158" time="135.95" type="appl" />
         <tli id="T159" time="136.83333605161812" />
         <tli id="T160" time="137.56" type="appl" />
         <tli id="T161" time="138.5457861052253" />
         <tli id="T162" time="139.795" type="appl" />
         <tli id="T163" time="140.89" type="appl" />
         <tli id="T164" time="141.742" type="appl" />
         <tli id="T165" time="142.595" type="appl" />
         <tli id="T166" time="143.448" type="appl" />
         <tli id="T167" time="144.3" type="appl" />
         <tli id="T168" time="144.916" type="appl" />
         <tli id="T169" time="145.501" type="appl" />
         <tli id="T170" time="146.087" type="appl" />
         <tli id="T171" time="146.672" type="appl" />
         <tli id="T172" time="147.258" type="appl" />
         <tli id="T173" time="147.843" type="appl" />
         <tli id="T174" time="148.429" type="appl" />
         <tli id="T175" time="149.014" type="appl" />
         <tli id="T176" time="149.6" type="appl" />
         <tli id="T177" time="150.349" type="appl" />
         <tli id="T178" time="150.978" type="appl" />
         <tli id="T179" time="151.607" type="appl" />
         <tli id="T180" time="152.236" type="appl" />
         <tli id="T181" time="152.865" type="appl" />
         <tli id="T182" time="153.485" type="appl" />
         <tli id="T183" time="154.105" type="appl" />
         <tli id="T184" time="154.725" type="appl" />
         <tli id="T185" time="155.65901067129755" />
         <tli id="T186" time="156.644" type="appl" />
         <tli id="T187" time="157.608" type="appl" />
         <tli id="T188" time="158.572" type="appl" />
         <tli id="T189" time="159.536" type="appl" />
         <tli id="T190" time="160.5" type="appl" />
         <tli id="T191" time="161.805" type="appl" />
         <tli id="T192" time="162.401" type="appl" />
         <tli id="T193" time="162.903" type="appl" />
         <tli id="T194" time="163.404" type="appl" />
         <tli id="T195" time="163.906" type="appl" />
         <tli id="T196" time="164.407" type="appl" />
         <tli id="T197" time="164.909" type="appl" />
         <tli id="T198" time="165.41" type="appl" />
         <tli id="T199" time="166.422" type="appl" />
         <tli id="T200" time="167.378" type="appl" />
         <tli id="T201" time="168.335" type="appl" />
         <tli id="T202" time="169.242" type="appl" />
         <tli id="T203" time="169.94" type="appl" />
         <tli id="T204" time="170.638" type="appl" />
         <tli id="T205" time="171.88557420609297" />
         <tli id="T206" time="172.701" type="appl" />
         <tli id="T207" time="173.437" type="appl" />
         <tli id="T208" time="174.173" type="appl" />
         <tli id="T209" time="174.909" type="appl" />
         <tli id="T210" time="175.646" type="appl" />
         <tli id="T211" time="176.382" type="appl" />
         <tli id="T212" time="177.118" type="appl" />
         <tli id="T213" time="177.854" type="appl" />
         <tli id="T214" time="178.59" type="appl" />
         <tli id="T215" time="179.217" type="appl" />
         <tli id="T216" time="179.844" type="appl" />
         <tli id="T217" time="180.471" type="appl" />
         <tli id="T218" time="181.098" type="appl" />
         <tli id="T219" time="181.725" type="appl" />
         <tli id="T220" time="182.352" type="appl" />
         <tli id="T221" time="182.979" type="appl" />
         <tli id="T222" time="183.606" type="appl" />
         <tli id="T223" time="184.233" type="appl" />
         <tli id="T224" time="184.86" type="appl" />
         <tli id="T225" time="190.4187897470672" />
         <tli id="T226" time="191.841" type="appl" />
         <tli id="T227" time="192.997" type="appl" />
         <tli id="T228" time="194.153" type="appl" />
         <tli id="T229" time="195.309" type="appl" />
         <tli id="T230" time="196.465" type="appl" />
         <tli id="T231" time="197.621" type="appl" />
         <tli id="T232" time="198.777" type="appl" />
         <tli id="T233" time="199.415" type="appl" />
         <tli id="T234" time="200.054" type="appl" />
         <tli id="T235" time="200.692" type="appl" />
         <tli id="T236" time="201.33" type="appl" />
         <tli id="T237" time="201.967" type="appl" />
         <tli id="T238" time="202.604" type="appl" />
         <tli id="T239" time="203.85870432642116" />
         <tli id="T240" time="204.903" type="appl" />
         <tli id="T241" time="205.726" type="appl" />
         <tli id="T242" time="206.549" type="appl" />
         <tli id="T243" time="207.371" type="appl" />
         <tli id="T244" time="208.194" type="appl" />
         <tli id="T245" time="209.017" type="appl" />
         <tli id="T246" time="209.84" type="appl" />
         <tli id="T247" time="210.806" type="appl" />
         <tli id="T248" time="211.642" type="appl" />
         <tli id="T249" time="212.477" type="appl" />
         <tli id="T250" time="213.313" type="appl" />
         <tli id="T251" time="214.149" type="appl" />
         <tli id="T252" time="214.985" type="appl" />
         <tli id="T253" time="215.82" type="appl" />
         <tli id="T254" time="216.656" type="appl" />
         <tli id="T255" time="217.492" type="appl" />
         <tli id="T256" time="219.696" type="appl" />
         <tli id="T257" time="220.942" type="appl" />
         <tli id="T258" time="221.897" type="appl" />
         <tli id="T259" time="222.853" type="appl" />
         <tli id="T260" time="223.809" type="appl" />
         <tli id="T261" time="224.765" type="appl" />
         <tli id="T262" time="225.72" type="appl" />
         <tli id="T263" time="226.676" type="appl" />
         <tli id="T264" time="227.776" type="appl" />
         <tli id="T265" time="228.778" type="appl" />
         <tli id="T266" time="229.705" type="appl" />
         <tli id="T267" time="230.632" type="appl" />
         <tli id="T268" time="231.56" type="appl" />
         <tli id="T269" time="232.325" type="appl" />
         <tli id="T270" time="233.09" type="appl" />
         <tli id="T271" time="233.855" type="appl" />
         <tli id="T272" time="234.62" type="appl" />
         <tli id="T273" time="235.24" type="appl" />
         <tli id="T274" time="235.84" type="appl" />
         <tli id="T275" time="236.44" type="appl" />
         <tli id="T276" time="237.04" type="appl" />
         <tli id="T277" time="237.64" type="appl" />
         <tli id="T278" time="238.24" type="appl" />
         <tli id="T279" time="238.84" type="appl" />
         <tli id="T280" time="239.44" type="appl" />
         <tli id="T281" time="240.04" type="appl" />
         <tli id="T282" time="240.549" type="appl" />
         <tli id="T283" time="241.058" type="appl" />
         <tli id="T284" time="241.568" type="appl" />
         <tli id="T285" time="242.077" type="appl" />
         <tli id="T286" time="242.586" type="appl" />
         <tli id="T287" time="243.221" type="appl" />
         <tli id="T288" time="243.691" type="appl" />
         <tli id="T289" time="244.161" type="appl" />
         <tli id="T290" time="244.84511049401448" />
         <tli id="T291" time="245.763" type="appl" />
         <tli id="T292" time="246.679" type="appl" />
         <tli id="T293" time="247.596" type="appl" />
         <tli id="T294" time="248.417" type="appl" />
         <tli id="T295" time="249.218" type="appl" />
         <tli id="T296" time="250.019" type="appl" />
         <tli id="T297" time="251.4717350435571" />
         <tli id="T298" time="252.283" type="appl" />
         <tli id="T299" time="253.167" type="appl" />
         <tli id="T300" time="255.44504312356455" />
         <tli id="T301" time="256.227" type="appl" />
         <tli id="T302" time="256.785" type="appl" />
         <tli id="T303" time="257.342" type="appl" />
         <tli id="T304" time="257.9" type="appl" />
         <tli id="T305" time="258.458" type="appl" />
         <tli id="T306" time="259.015" type="appl" />
         <tli id="T307" time="259.572" type="appl" />
         <tli id="T308" time="260.13" type="appl" />
         <tli id="T309" time="260.758" type="appl" />
         <tli id="T310" time="261.378" type="appl" />
         <tli id="T311" time="261.998" type="appl" />
         <tli id="T312" time="262.618" type="appl" />
         <tli id="T313" time="264.96498261727356" />
         <tli id="T314" time="265.984" type="appl" />
         <tli id="T315" time="266.609" type="appl" />
         <tli id="T316" time="267.235" type="appl" />
         <tli id="T317" time="267.861" type="appl" />
         <tli id="T318" time="268.487" type="appl" />
         <tli id="T319" time="269.112" type="appl" />
         <tli id="T320" time="269.92495109298756" />
         <tli id="T321" time="270.681" type="appl" />
         <tli id="T322" time="271.359" type="appl" />
         <tli id="T323" time="272.037" type="appl" />
         <tli id="T324" time="272.715" type="appl" />
         <tli id="T325" time="273.393" type="appl" />
         <tli id="T326" time="275.79158047286427" />
         <tli id="T327" time="276.813" type="appl" />
         <tli id="T328" time="277.593" type="appl" />
         <tli id="T329" time="278.373" type="appl" />
         <tli id="T330" time="279.153" type="appl" />
         <tli id="T331" time="282.738" type="appl" />
         <tli id="T332" time="283.653" type="appl" />
         <tli id="T333" time="284.338" type="appl" />
         <tli id="T334" time="285.023" type="appl" />
         <tli id="T335" time="286.09818163342044" />
         <tli id="T336" time="286.793" type="appl" />
         <tli id="T337" time="287.373" type="appl" />
         <tli id="T338" time="287.953" type="appl" />
         <tli id="T339" time="288.533" type="appl" />
         <tli id="T340" time="289.113" type="appl" />
         <tli id="T341" time="290.6114862813484" />
         <tli id="T342" time="291.417" type="appl" />
         <tli id="T343" time="292.176" type="appl" />
         <tli id="T344" time="292.934" type="appl" />
         <tli id="T345" time="293.8847988103477" />
         <tli id="T346" time="294.68" type="appl" />
         <tli id="T347" time="295.39" type="appl" />
         <tli id="T348" time="296.1" type="appl" />
         <tli id="T349" time="296.81" type="appl" />
         <tli id="T350" time="297.52" type="appl" />
         <tli id="T351" time="298.5514358170679" />
         <tli id="T352" time="300.51142335989033" />
         <tli id="T353" time="301.77" type="appl" />
         <tli id="T354" time="302.755" type="appl" />
         <tli id="T355" time="304.135" type="appl" />
         <tli id="T356" time="305.075" type="appl" />
         <tli id="T357" time="306.698" type="appl" />
         <tli id="T358" time="308.037" type="appl" />
         <tli id="T359" time="309.375" type="appl" />
         <tli id="T360" time="310.713" type="appl" />
         <tli id="T361" time="312.052" type="appl" />
         <tli id="T362" time="313.39" type="appl" />
         <tli id="T363" time="315.37799553844155" />
         <tli id="T364" time="316.25" type="appl" />
         <tli id="T365" time="317.14" type="appl" />
         <tli id="T366" time="318.03" type="appl" />
         <tli id="T367" time="318.754" type="appl" />
         <tli id="T368" time="319.478" type="appl" />
         <tli id="T369" time="320.202" type="appl" />
         <tli id="T370" time="320.926" type="appl" />
         <tli id="T371" time="321.9979534636889" />
         <tli id="T372" time="323.014" type="appl" />
         <tli id="T373" time="323.989" type="appl" />
         <tli id="T374" time="324.963" type="appl" />
         <tli id="T375" time="325.938" type="appl" />
         <tli id="T376" time="327.07125455242317" />
         <tli id="T377" time="328.061" type="appl" />
         <tli id="T378" time="329.23124082410504" />
         <tli id="T379" time="329.978" type="appl" />
         <tli id="T380" time="330.6" type="appl" />
         <tli id="T381" time="331.222" type="appl" />
         <tli id="T382" time="331.844" type="appl" />
         <tli id="T383" time="333.98454394666425" />
         <tli id="T384" time="334.838" type="appl" />
         <tli id="T385" time="335.586" type="appl" />
         <tli id="T386" time="336.334" type="appl" />
         <tli id="T387" time="337.082" type="appl" />
         <tli id="T388" time="337.83" type="appl" />
         <tli id="T389" time="338.917845925197" />
         <tli id="T390" time="339.862" type="appl" />
         <tli id="T391" time="340.603" type="appl" />
         <tli id="T392" time="341.345" type="appl" />
         <tli id="T393" time="342.087" type="appl" />
         <tli id="T394" time="342.828" type="appl" />
         <tli id="T395" time="343.57" type="appl" />
         <tli id="T396" time="344.453" type="appl" />
         <tli id="T397" time="345.277" type="appl" />
         <tli id="T398" time="346.1" type="appl" />
         <tli id="T399" time="351.825" type="appl" />
         <tli id="T400" time="352.31" type="appl" />
         <tli id="T401" time="352.795" type="appl" />
         <tli id="T402" time="353.28" type="appl" />
         <tli id="T403" time="353.765" type="appl" />
         <tli id="T404" time="354.25" type="appl" />
         <tli id="T405" time="355.17" type="appl" />
         <tli id="T406" time="355.93" type="appl" />
         <tli id="T407" time="357.0043976383851" />
         <tli id="T408" time="357.744" type="appl" />
         <tli id="T409" time="358.332" type="appl" />
         <tli id="T410" time="358.921" type="appl" />
         <tli id="T411" time="359.509" type="appl" />
         <tli id="T412" time="360.098" type="appl" />
         <tli id="T413" time="360.686" type="appl" />
         <tli id="T414" time="361.49770241342713" />
         <tli id="T415" time="362.163" type="appl" />
         <tli id="T416" time="362.846" type="appl" />
         <tli id="T986" time="363.1188" type="intp" />
         <tli id="T417" time="363.528" type="appl" />
         <tli id="T418" time="364.211" type="appl" />
         <tli id="T419" time="364.894" type="appl" />
         <tli id="T420" time="365.577" type="appl" />
         <tli id="T421" time="366.259" type="appl" />
         <tli id="T422" time="366.942" type="appl" />
         <tli id="T423" time="367.625" type="appl" />
         <tli id="T424" time="368.90432200552146" />
         <tli id="T425" time="369.701" type="appl" />
         <tli id="T426" time="370.415" type="appl" />
         <tli id="T427" time="371.129" type="appl" />
         <tli id="T428" time="371.842" type="appl" />
         <tli id="T429" time="372.556" type="appl" />
         <tli id="T430" time="373.22333101334254" />
         <tli id="T431" time="374.02" type="appl" />
         <tli id="T432" time="374.76" type="appl" />
         <tli id="T433" time="375.5" type="appl" />
         <tli id="T434" time="376.24" type="appl" />
         <tli id="T435" time="376.981" type="appl" />
         <tli id="T436" time="377.721" type="appl" />
         <tli id="T437" time="378.461" type="appl" />
         <tli id="T438" time="379.201" type="appl" />
         <tli id="T439" time="380.127" type="appl" />
         <tli id="T440" time="380.964" type="appl" />
         <tli id="T441" time="381.801" type="appl" />
         <tli id="T442" time="382.639" type="appl" />
         <tli id="T443" time="383.476" type="appl" />
         <tli id="T444" time="384.313" type="appl" />
         <tli id="T445" time="385.15" type="appl" />
         <tli id="T446" time="386.06421294166097" />
         <tli id="T447" time="386.92" type="appl" />
         <tli id="T448" time="387.81" type="appl" />
         <tli id="T449" time="388.7" type="appl" />
         <tli id="T450" time="389.59" type="appl" />
         <tli id="T451" time="390.88418230717326" />
         <tli id="T452" time="392.172" type="appl" />
         <tli id="T453" time="393.253" type="appl" />
         <tli id="T454" time="394.335" type="appl" />
         <tli id="T455" time="395.416" type="appl" />
         <tli id="T456" time="396.498" type="appl" />
         <tli id="T457" time="397.194" type="appl" />
         <tli id="T458" time="397.808" type="appl" />
         <tli id="T459" time="398.422" type="appl" />
         <tli id="T460" time="399.036" type="appl" />
         <tli id="T461" time="399.65" type="appl" />
         <tli id="T462" time="400.264" type="appl" />
         <tli id="T463" time="400.878" type="appl" />
         <tli id="T464" time="401.492" type="appl" />
         <tli id="T465" time="402.106" type="appl" />
         <tli id="T466" time="402.72" type="appl" />
         <tli id="T467" time="403.91" type="appl" />
         <tli id="T468" time="404.7" type="appl" />
         <tli id="T469" time="405.476" type="appl" />
         <tli id="T470" time="406.251" type="appl" />
         <tli id="T471" time="407.027" type="appl" />
         <tli id="T472" time="407.803" type="appl" />
         <tli id="T473" time="408.579" type="appl" />
         <tli id="T474" time="409.354" type="appl" />
         <tli id="T475" time="411.5240511254669" />
         <tli id="T476" time="412.275" type="appl" />
         <tli id="T477" time="413.23" type="appl" />
         <tli id="T478" time="414.185" type="appl" />
         <tli id="T479" time="415.14" type="appl" />
         <tli id="T480" time="416.445" type="appl" />
         <tli id="T481" time="417.44" type="appl" />
         <tli id="T482" time="418.435" type="appl" />
         <tli id="T483" time="419.43" type="appl" />
         <tli id="T484" time="420.425" type="appl" />
         <tli id="T485" time="421.42" type="appl" />
         <tli id="T486" time="422.43" type="appl" />
         <tli id="T487" time="423.32" type="appl" />
         <tli id="T488" time="424.21" type="appl" />
         <tli id="T489" time="424.791" type="appl" />
         <tli id="T490" time="425.212" type="appl" />
         <tli id="T491" time="425.633" type="appl" />
         <tli id="T492" time="426.054" type="appl" />
         <tli id="T493" time="426.475" type="appl" />
         <tli id="T494" time="427.165" type="appl" />
         <tli id="T495" time="427.855" type="appl" />
         <tli id="T496" time="428.545" type="appl" />
         <tli id="T497" time="430.465" type="appl" />
         <tli id="T498" time="431.95" type="appl" />
         <tli id="T499" time="433.435" type="appl" />
         <tli id="T500" time="434.92" type="appl" />
         <tli id="T501" time="436.405" type="appl" />
         <tli id="T502" time="437.312" type="appl" />
         <tli id="T503" time="438.205" type="appl" />
         <tli id="T504" time="439.098" type="appl" />
         <tli id="T505" time="439.99" type="appl" />
         <tli id="T506" time="441.19" type="appl" />
         <tli id="T507" time="441.89" type="appl" />
         <tli id="T508" time="442.59" type="appl" />
         <tli id="T509" time="443.29" type="appl" />
         <tli id="T510" time="443.99" type="appl" />
         <tli id="T511" time="444.87" type="appl" />
         <tli id="T512" time="445.58" type="appl" />
         <tli id="T513" time="446.29" type="appl" />
         <tli id="T514" time="447.28" type="appl" />
         <tli id="T515" time="448.169" type="appl" />
         <tli id="T516" time="449.058" type="appl" />
         <tli id="T517" time="449.948" type="appl" />
         <tli id="T518" time="450.838" type="appl" />
         <tli id="T519" time="451.727" type="appl" />
         <tli id="T520" time="453.005" type="appl" />
         <tli id="T521" time="454.09" type="appl" />
         <tli id="T522" time="455.57" type="appl" />
         <tli id="T523" time="456.72" type="appl" />
         <tli id="T524" time="458.45708616448076" />
         <tli id="T525" time="459.181" type="appl" />
         <tli id="T526" time="459.937" type="appl" />
         <tli id="T527" time="460.693" type="appl" />
         <tli id="T528" time="461.449" type="appl" />
         <tli id="T529" time="462.39706112301155" />
         <tli id="T530" time="463.44" type="appl" />
         <tli id="T531" time="464.59" type="appl" />
         <tli id="T532" time="465.302" type="appl" />
         <tli id="T533" time="465.95" type="appl" />
         <tli id="T534" time="466.597" type="appl" />
         <tli id="T535" time="467.245" type="appl" />
         <tli id="T536" time="468.42" type="appl" />
         <tli id="T537" time="469.435" type="appl" />
         <tli id="T538" time="470.693" type="appl" />
         <tli id="T539" time="471.847" type="appl" />
         <tli id="T540" time="473.66365618209306" />
         <tli id="T541" time="475.575" type="appl" />
         <tli id="T542" time="477.35" type="appl" />
         <tli id="T543" time="477.841" type="appl" />
         <tli id="T544" time="478.313" type="appl" />
         <tli id="T545" time="478.784" type="appl" />
         <tli id="T546" time="479.256" type="appl" />
         <tli id="T547" time="479.727" type="appl" />
         <tli id="T548" time="480.199" type="appl" />
         <tli id="T549" time="480.67" type="appl" />
         <tli id="T550" time="481.6" type="appl" />
         <tli id="T551" time="482.495" type="appl" />
         <tli id="T552" time="483.27" type="appl" />
         <tli id="T553" time="483.96" type="appl" />
         <tli id="T554" time="484.65" type="appl" />
         <tli id="T555" time="485.34" type="appl" />
         <tli id="T556" time="486.03" type="appl" />
         <tli id="T557" time="486.72" type="appl" />
         <tli id="T558" time="487.457" type="appl" />
         <tli id="T559" time="488.019" type="appl" />
         <tli id="T560" time="488.581" type="appl" />
         <tli id="T561" time="489.143" type="appl" />
         <tli id="T562" time="489.705" type="appl" />
         <tli id="T563" time="490.438" type="appl" />
         <tli id="T564" time="491.052" type="appl" />
         <tli id="T565" time="491.665" type="appl" />
         <tli id="T566" time="492.642" type="appl" />
         <tli id="T567" time="493.618" type="appl" />
         <tli id="T568" time="494.595" type="appl" />
         <tli id="T569" time="495.363" type="appl" />
         <tli id="T570" time="496.002" type="appl" />
         <tli id="T571" time="496.64" type="appl" />
         <tli id="T572" time="497.278" type="appl" />
         <tli id="T573" time="497.917" type="appl" />
         <tli id="T574" time="499.0568281229459" />
         <tli id="T575" time="499.805" type="appl" />
         <tli id="T576" time="500.9768159199964" />
         <tli id="T577" time="501.845" type="appl" />
         <tli id="T578" time="505.1167896073867" />
         <tli id="T579" time="505.995" type="appl" />
         <tli id="T580" time="506.67" type="appl" />
         <tli id="T581" time="507.725" type="appl" />
         <tli id="T582" time="508.72" type="appl" />
         <tli id="T583" time="509.715" type="appl" />
         <tli id="T584" time="510.71" type="appl" />
         <tli id="T585" time="511.575" type="appl" />
         <tli id="T586" time="512.215" type="appl" />
         <tli id="T587" time="512.855" type="appl" />
         <tli id="T588" time="515.0567264317006" />
         <tli id="T589" time="515.849" type="appl" />
         <tli id="T590" time="516.569" type="appl" />
         <tli id="T591" time="517.288" type="appl" />
         <tli id="T592" time="518.007" type="appl" />
         <tli id="T593" time="518.727" type="appl" />
         <tli id="T594" time="519.5766977039239" />
         <tli id="T595" time="520.275" type="appl" />
         <tli id="T596" time="520.966" type="appl" />
         <tli id="T597" time="521.658" type="appl" />
         <tli id="T598" time="522.478" type="appl" />
         <tli id="T599" time="523.298" type="appl" />
         <tli id="T600" time="524.118" type="appl" />
         <tli id="T601" time="524.938" type="appl" />
         <tli id="T602" time="525.696" type="appl" />
         <tli id="T603" time="526.454" type="appl" />
         <tli id="T604" time="527.212" type="appl" />
         <tli id="T605" time="527.97" type="appl" />
         <tli id="T606" time="529.28" type="appl" />
         <tli id="T607" time="530.056" type="appl" />
         <tli id="T608" time="530.651" type="appl" />
         <tli id="T609" time="531.247" type="appl" />
         <tli id="T610" time="531.843" type="appl" />
         <tli id="T611" time="532.439" type="appl" />
         <tli id="T612" time="533.034" type="appl" />
         <tli id="T613" time="533.63" type="appl" />
         <tli id="T614" time="534.469" type="appl" />
         <tli id="T615" time="535.158" type="appl" />
         <tli id="T616" time="535.848" type="appl" />
         <tli id="T617" time="536.537" type="appl" />
         <tli id="T618" time="537.226" type="appl" />
         <tli id="T619" time="537.915" type="appl" />
         <tli id="T620" time="538.558" type="appl" />
         <tli id="T621" time="539.2" type="appl" />
         <tli id="T622" time="539.842" type="appl" />
         <tli id="T623" time="540.8098960845003" />
         <tli id="T624" time="541.512" type="appl" />
         <tli id="T625" time="541.989" type="appl" />
         <tli id="T626" time="542.466" type="appl" />
         <tli id="T627" time="542.943" type="appl" />
         <tli id="T628" time="543.42" type="appl" />
         <tli id="T629" time="543.91" type="appl" />
         <tli id="T630" time="544.4" type="appl" />
         <tli id="T631" time="544.89" type="appl" />
         <tli id="T632" time="545.38" type="appl" />
         <tli id="T633" time="546.317" type="appl" />
         <tli id="T634" time="546.919" type="appl" />
         <tli id="T635" time="547.521" type="appl" />
         <tli id="T636" time="548.123" type="appl" />
         <tli id="T637" time="549.2365091937779" />
         <tli id="T638" time="550.013" type="appl" />
         <tli id="T639" time="550.707" type="appl" />
         <tli id="T640" time="551.4" type="appl" />
         <tli id="T641" time="552.093" type="appl" />
         <tli id="T642" time="552.787" type="appl" />
         <tli id="T643" time="553.48" type="appl" />
         <tli id="T644" time="554.233" type="appl" />
         <tli id="T645" time="554.937" type="appl" />
         <tli id="T646" time="555.64" type="appl" />
         <tli id="T647" time="556.343" type="appl" />
         <tli id="T648" time="557.047" type="appl" />
         <tli id="T649" time="557.8964541533913" />
         <tli id="T650" time="558.98" type="appl" />
         <tli id="T651" time="559.89" type="appl" />
         <tli id="T652" time="560.8" type="appl" />
         <tli id="T653" time="561.709" type="appl" />
         <tli id="T654" time="562.619" type="appl" />
         <tli id="T655" time="563.529" type="appl" />
         <tli id="T656" time="564.4897455814574" />
         <tli id="T657" time="565.031" type="appl" />
         <tli id="T658" time="565.604" type="appl" />
         <tli id="T659" time="566.176" type="appl" />
         <tli id="T660" time="566.896396952066" />
         <tli id="T661" time="567.592" type="appl" />
         <tli id="T662" time="568.164" type="appl" />
         <tli id="T663" time="569.134" type="appl" />
         <tli id="T664" time="570.104" type="appl" />
         <tli id="T665" time="570.814" type="appl" />
         <tli id="T666" time="571.479" type="appl" />
         <tli id="T667" time="572.144" type="appl" />
         <tli id="T668" time="572.809" type="appl" />
         <tli id="T669" time="573.448" type="appl" />
         <tli id="T670" time="574.032" type="appl" />
         <tli id="T671" time="574.615" type="appl" />
         <tli id="T672" time="575.199" type="appl" />
         <tli id="T673" time="575.729" type="appl" />
         <tli id="T674" time="576.259" type="appl" />
         <tli id="T675" time="576.789" type="appl" />
         <tli id="T676" time="577.809" type="appl" />
         <tli id="T677" time="578.679" type="appl" />
         <tli id="T678" time="579.224" type="appl" />
         <tli id="T679" time="579.694" type="appl" />
         <tli id="T680" time="580.9496409665888" />
         <tli id="T681" time="581.612" type="appl" />
         <tli id="T682" time="582.24" type="appl" />
         <tli id="T683" time="582.868" type="appl" />
         <tli id="T684" time="583.496" type="appl" />
         <tli id="T685" time="585.636277846195" />
         <tli id="T686" time="586.246" type="appl" />
         <tli id="T687" time="586.908" type="appl" />
         <tli id="T688" time="587.57" type="appl" />
         <tli id="T689" time="588.232" type="appl" />
         <tli id="T690" time="588.894" type="appl" />
         <tli id="T691" time="589.556" type="appl" />
         <tli id="T692" time="590.218" type="appl" />
         <tli id="T693" time="590.88" type="appl" />
         <tli id="T694" time="592.3895682573484" />
         <tli id="T695" time="593.342" type="appl" />
         <tli id="T696" time="594.225" type="appl" />
         <tli id="T697" time="595.107" type="appl" />
         <tli id="T698" time="596.033" type="appl" />
         <tli id="T699" time="596.748" type="appl" />
         <tli id="T700" time="597.463" type="appl" />
         <tli id="T701" time="598.178" type="appl" />
         <tli id="T702" time="598.893" type="appl" />
         <tli id="T703" time="600.4095172846118" />
         <tli id="T704" time="601.398" type="appl" />
         <tli id="T705" time="602.238" type="appl" />
         <tli id="T706" time="604.5628242205927" />
         <tli id="T707" time="605.484" type="appl" />
         <tli id="T708" time="606.108" type="appl" />
         <tli id="T709" time="606.733" type="appl" />
         <tli id="T710" time="607.357" type="appl" />
         <tli id="T711" time="607.981" type="appl" />
         <tli id="T712" time="608.605" type="appl" />
         <tli id="T713" time="609.33" type="appl" />
         <tli id="T714" time="610.055" type="appl" />
         <tli id="T715" time="610.78" type="appl" />
         <tli id="T716" time="611.7361119623511" />
         <tli id="T717" time="612.933" type="appl" />
         <tli id="T718" time="613.857" type="appl" />
         <tli id="T719" time="615.2694228388676" />
         <tli id="T720" time="616.297" type="appl" />
         <tli id="T721" time="617.285" type="appl" />
         <tli id="T722" time="618.272" type="appl" />
         <tli id="T723" time="619.26" type="appl" />
         <tli id="T724" time="620.096" type="appl" />
         <tli id="T725" time="620.912" type="appl" />
         <tli id="T726" time="621.728" type="appl" />
         <tli id="T727" time="622.544" type="appl" />
         <tli id="T728" time="623.36" type="appl" />
         <tli id="T729" time="624.7360293382142" />
         <tli id="T730" time="625.49" type="appl" />
         <tli id="T731" time="626.135" type="appl" />
         <tli id="T732" time="626.78" type="appl" />
         <tli id="T733" time="627.425" type="appl" />
         <tli id="T734" time="628.07" type="appl" />
         <tli id="T735" time="628.715" type="appl" />
         <tli id="T736" time="629.36" type="appl" />
         <tli id="T737" time="630.005" type="appl" />
         <tli id="T738" time="631.01" type="appl" />
         <tli id="T739" time="631.74" type="appl" />
         <tli id="T740" time="632.47" type="appl" />
         <tli id="T741" time="633.47" type="appl" />
         <tli id="T742" time="634.36" type="appl" />
         <tli id="T743" time="635.25" type="appl" />
         <tli id="T744" time="636.14" type="appl" />
         <tli id="T987" time="636.47375" type="intp" />
         <tli id="T745" time="637.03" type="appl" />
         <tli id="T746" time="638.395" type="appl" />
         <tli id="T747" time="639.5" type="appl" />
         <tli id="T748" time="640.237" type="appl" />
         <tli id="T749" time="640.913" type="appl" />
         <tli id="T750" time="641.59" type="appl" />
         <tli id="T751" time="642.267" type="appl" />
         <tli id="T752" time="642.943" type="appl" />
         <tli id="T753" time="643.7825749499277" />
         <tli id="T754" time="645.9158947244283" />
         <tli id="T755" time="646.653" type="appl" />
         <tli id="T756" time="647.197" type="appl" />
         <tli id="T757" time="647.74" type="appl" />
         <tli id="T758" time="648.283" type="appl" />
         <tli id="T759" time="648.827" type="appl" />
         <tli id="T760" time="650.2425338920874" />
         <tli id="T761" time="650.874" type="appl" />
         <tli id="T762" time="651.509" type="appl" />
         <tli id="T763" time="652.143" type="appl" />
         <tli id="T764" time="652.777" type="appl" />
         <tli id="T765" time="653.411" type="appl" />
         <tli id="T766" time="654.046" type="appl" />
         <tli id="T767" time="654.68" type="appl" />
         <tli id="T768" time="655.314" type="appl" />
         <tli id="T769" time="655.949" type="appl" />
         <tli id="T770" time="656.583" type="appl" />
         <tli id="T771" time="657.217" type="appl" />
         <tli id="T772" time="657.851" type="appl" />
         <tli id="T773" time="658.486" type="appl" />
         <tli id="T774" time="659.6624740213668" />
         <tli id="T775" time="660.495802058281" />
         <tli id="T776" time="661.592" type="appl" />
         <tli id="T777" time="662.525" type="appl" />
         <tli id="T778" time="663.458" type="appl" />
         <tli id="T779" time="664.5824427513089" />
         <tli id="T780" time="665.41" type="appl" />
         <tli id="T781" time="666.13" type="appl" />
         <tli id="T782" time="666.85" type="appl" />
         <tli id="T783" time="667.57" type="appl" />
         <tli id="T784" time="668.29" type="appl" />
         <tli id="T785" time="669.01" type="appl" />
         <tli id="T786" time="670.35" type="appl" />
         <tli id="T787" time="671.48" type="appl" />
         <tli id="T788" time="672.61" type="appl" />
         <tli id="T789" time="673.74" type="appl" />
         <tli id="T790" time="674.352" type="appl" />
         <tli id="T791" time="674.918" type="appl" />
         <tli id="T792" time="675.485" type="appl" />
         <tli id="T793" time="676.052" type="appl" />
         <tli id="T794" time="676.618" type="appl" />
         <tli id="T795" time="678.1156900707972" />
         <tli id="T796" time="678.868" type="appl" />
         <tli id="T797" time="679.436" type="appl" />
         <tli id="T798" time="680.004" type="appl" />
         <tli id="T799" time="680.572" type="appl" />
         <tli id="T800" time="681.35566947832" />
         <tli id="T801" time="682.536" type="appl" />
         <tli id="T802" time="683.498" type="appl" />
         <tli id="T803" time="684.459" type="appl" />
         <tli id="T804" time="685.42" type="appl" />
         <tli id="T805" time="686.104" type="appl" />
         <tli id="T806" time="686.718" type="appl" />
         <tli id="T807" time="687.332" type="appl" />
         <tli id="T808" time="687.946" type="appl" />
         <tli id="T809" time="688.4599993250238" />
         <tli id="T810" time="689.35" type="appl" />
         <tli id="T811" time="690.14" type="appl" />
         <tli id="T812" time="690.93" type="appl" />
         <tli id="T813" time="691.765" type="appl" />
         <tli id="T814" time="692.6" type="appl" />
         <tli id="T815" time="693.435" type="appl" />
         <tli id="T816" time="694.27" type="appl" />
         <tli id="T817" time="695.235" type="appl" />
         <tli id="T818" time="696.105" type="appl" />
         <tli id="T819" time="696.975" type="appl" />
         <tli id="T820" time="698.0555633380827" />
         <tli id="T821" time="699.367" type="appl" />
         <tli id="T822" time="700.443" type="appl" />
         <tli id="T823" time="702.8555328307091" />
         <tli id="T824" time="704.32" type="appl" />
         <tli id="T825" time="706.7821745406494" />
         <tli id="T826" time="708.22" type="appl" />
         <tli id="T827" time="709.41" type="appl" />
         <tli id="T828" time="712.5754710532777" />
         <tli id="T829" time="713.418" type="appl" />
         <tli id="T830" time="714.16" type="appl" />
         <tli id="T831" time="714.902" type="appl" />
         <tli id="T832" time="715.645" type="appl" />
         <tli id="T833" time="716.549" type="appl" />
         <tli id="T834" time="717.223" type="appl" />
         <tli id="T835" time="717.897" type="appl" />
         <tli id="T836" time="718.571" type="appl" />
         <tli id="T837" time="719.748758795036" />
         <tli id="T838" time="720.551" type="appl" />
         <tli id="T839" time="721.202" type="appl" />
         <tli id="T840" time="721.853" type="appl" />
         <tli id="T841" time="722.504" type="appl" />
         <tli id="T842" time="723.6954003778621" />
         <tli id="T843" time="724.81" type="appl" />
         <tli id="T844" time="725.6" type="appl" />
         <tli id="T845" time="726.39" type="appl" />
         <tli id="T846" time="727.18" type="appl" />
         <tli id="T847" time="728.2353715229714" />
         <tli id="T848" time="731.4753509304942" />
         <tli id="T849" time="732.455" type="appl" />
         <tli id="T850" time="733.055" type="appl" />
         <tli id="T851" time="733.655" type="appl" />
         <tli id="T852" time="734.6419974707686" />
         <tli id="T853" time="735.386" type="appl" />
         <tli id="T854" time="736.082" type="appl" />
         <tli id="T855" time="736.778" type="appl" />
         <tli id="T856" time="737.474" type="appl" />
         <tli id="T857" time="738.171" type="appl" />
         <tli id="T858" time="738.867" type="appl" />
         <tli id="T859" time="739.563" type="appl" />
         <tli id="T860" time="741.2952885174925" />
         <tli id="T861" time="742.416" type="appl" />
         <tli id="T862" time="743.343" type="appl" />
         <tli id="T863" time="745.4352622048829" />
         <tli id="T864" time="746.481" type="appl" />
         <tli id="T865" time="747.223" type="appl" />
         <tli id="T866" time="747.964" type="appl" />
         <tli id="T867" time="748.706" type="appl" />
         <tli id="T868" time="749.447" type="appl" />
         <tli id="T869" time="750.189" type="appl" />
         <tli id="T870" time="751.588556429458" />
         <tli id="T871" time="752.732" type="appl" />
         <tli id="T872" time="753.584" type="appl" />
         <tli id="T873" time="754.436" type="appl" />
         <tli id="T874" time="755.288" type="appl" />
         <tli id="T875" time="756.2218603147015" />
         <tli id="T876" time="758.1818478575241" />
         <tli id="T877" time="759.42" type="appl" />
         <tli id="T878" time="760.285" type="appl" />
         <tli id="T879" time="761.15" type="appl" />
         <tli id="T880" time="762.015" type="appl" />
         <tli id="T881" time="762.88" type="appl" />
         <tli id="T882" time="764.4684745680055" />
         <tli id="T883" time="765.647" type="appl" />
         <tli id="T884" time="766.569" type="appl" />
         <tli id="T885" time="767.491" type="appl" />
         <tli id="T886" time="768.413" type="appl" />
         <tli id="T887" time="769.335" type="appl" />
         <tli id="T888" time="770.295" type="appl" />
         <tli id="T889" time="770.985" type="appl" />
         <tli id="T890" time="771.8617609115092" />
         <tli id="T891" time="772.585" type="appl" />
         <tli id="T892" time="773.22" type="appl" />
         <tli id="T893" time="773.855" type="appl" />
         <tli id="T894" time="774.49" type="appl" />
         <tli id="T895" time="775.125" type="appl" />
         <tli id="T896" time="775.76" type="appl" />
         <tli id="T897" time="776.395" type="appl" />
         <tli id="T898" time="777.03" type="appl" />
         <tli id="T899" time="778.5150519582331" />
         <tli id="T900" time="779.502" type="appl" />
         <tli id="T901" time="780.295" type="appl" />
         <tli id="T902" time="781.088" type="appl" />
         <tli id="T903" time="782.0483628347497" />
         <tli id="T904" time="783.41" type="appl" />
         <tli id="T905" time="784.48" type="appl" />
         <tli id="T906" time="785.55" type="appl" />
         <tli id="T907" time="786.62" type="appl" />
         <tli id="T908" time="787.69" type="appl" />
         <tli id="T909" time="788.76" type="appl" />
         <tli id="T910" time="789.83" type="appl" />
         <tli id="T911" time="790.9" type="appl" />
         <tli id="T912" time="793.255" type="appl" />
         <tli id="T913" time="794.243" type="appl" />
         <tli id="T914" time="794.98" type="appl" />
         <tli id="T915" time="795.718" type="appl" />
         <tli id="T916" time="799.1549207765266" />
         <tli id="T917" time="800.143" type="appl" />
         <tli id="T918" time="800.957" type="appl" />
         <tli id="T919" time="801.77" type="appl" />
         <tli id="T920" time="803.757" type="appl" />
         <tli id="T921" time="805.608" type="appl" />
         <tli id="T922" time="808.5215279114435" />
         <tli id="T923" time="809.696" type="appl" />
         <tli id="T924" time="810.592" type="appl" />
         <tli id="T925" time="811.488" type="appl" />
         <tli id="T926" time="812.384" type="appl" />
         <tli id="T927" time="813.28" type="appl" />
         <tli id="T928" time="816.085" type="appl" />
         <tli id="T929" time="817.002" type="appl" />
         <tli id="T930" time="817.764" type="appl" />
         <tli id="T931" time="818.526" type="appl" />
         <tli id="T932" time="819.288" type="appl" />
         <tli id="T933" time="820.05" type="appl" />
         <tli id="T934" time="820.971" type="appl" />
         <tli id="T935" time="821.743" type="appl" />
         <tli id="T936" time="822.514" type="appl" />
         <tli id="T937" time="823.285" type="appl" />
         <tli id="T938" time="824.056" type="appl" />
         <tli id="T939" time="824.828" type="appl" />
         <tli id="T940" time="825.599" type="appl" />
         <tli id="T941" time="826.37" type="appl" />
         <tli id="T942" time="828.181402958326" />
         <tli id="T943" time="828.871" type="appl" />
         <tli id="T944" time="829.527" type="appl" />
         <tli id="T945" time="830.183" type="appl" />
         <tli id="T946" time="830.839" type="appl" />
         <tli id="T947" time="832.1280445411521" />
         <tli id="T948" time="833.028" type="appl" />
         <tli id="T949" time="833.767" type="appl" />
         <tli id="T950" time="834.505" type="appl" />
         <tli id="T951" time="835.243" type="appl" />
         <tli id="T952" time="835.982" type="appl" />
         <tli id="T953" time="836.72" type="appl" />
         <tli id="T954" time="837.44" type="appl" />
         <tli id="T955" time="838.4146712516336" />
         <tli id="T956" time="839.7546627349918" />
         <tli id="T957" time="840.561" type="appl" />
         <tli id="T958" time="841.227" type="appl" />
         <tli id="T959" time="841.893" type="appl" />
         <tli id="T960" time="842.559" type="appl" />
         <tli id="T961" time="843.5279720861398" />
         <tli id="T962" time="844.51" type="appl" />
         <tli id="T963" time="845.22" type="appl" />
         <tli id="T964" time="845.93" type="appl" />
         <tli id="T965" time="846.64" type="appl" />
         <tli id="T966" time="847.35" type="appl" />
         <tli id="T967" time="848.06" type="appl" />
         <tli id="T968" time="848.868" type="appl" />
         <tli id="T969" time="849.662" type="appl" />
         <tli id="T970" time="850.455" type="appl" />
         <tli id="T971" time="852.0945843056189" />
         <tli id="T972" time="853.055" type="appl" />
         <tli id="T973" time="853.695" type="appl" />
         <tli id="T974" time="854.335" type="appl" />
         <tli id="T975" time="854.975" type="appl" />
         <tli id="T976" time="855.8412271595857" />
         <tli id="T977" time="856.712" type="appl" />
         <tli id="T978" time="857.514" type="appl" />
         <tli id="T979" time="858.316" type="appl" />
         <tli id="T980" time="859.118" type="appl" />
         <tli id="T981" time="860.0612003385196" />
         <tli id="T982" time="860.527" type="appl" />
         <tli id="T983" time="861.013" type="appl" />
         <tli id="T984" time="861.5" type="appl" />
         <tli id="T985" time="862.081" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T106" start="T104">
            <tli id="T104.tx.1" />
         </timeline-fork>
         <timeline-fork end="T782" start="T781">
            <tli id="T781.tx.1" />
         </timeline-fork>
         <timeline-fork end="T790" start="T789">
            <tli id="T789.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T984" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Kaŋgaʔ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">multʼanə</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_9" n="HIAT:ip">(</nts>
                  <ts e="T3" id="Seg_11" n="HIAT:w" s="T2">obbərej</ts>
                  <nts id="Seg_12" n="HIAT:ip">)</nts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_15" n="HIAT:w" s="T3">šidegöʔ</ts>
                  <nts id="Seg_16" n="HIAT:ip">.</nts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_19" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_21" n="HIAT:w" s="T4">Dĭ</ts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_24" n="HIAT:w" s="T5">tănan</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_27" n="HIAT:w" s="T6">bögəlbə</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_30" n="HIAT:w" s="T7">kĭškələj</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_33" n="HIAT:w" s="T8">sabənzi</ts>
                  <nts id="Seg_34" n="HIAT:ip">,</nts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_36" n="HIAT:ip">(</nts>
                  <ts e="T10" id="Seg_38" n="HIAT:w" s="T9">vixotkazi</ts>
                  <nts id="Seg_39" n="HIAT:ip">)</nts>
                  <nts id="Seg_40" n="HIAT:ip">.</nts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_43" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_45" n="HIAT:w" s="T10">A</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_47" n="HIAT:ip">(</nts>
                  <ts e="T12" id="Seg_49" n="HIAT:w" s="T11">tăn</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_52" n="HIAT:w" s="T12">dĭʔnə</ts>
                  <nts id="Seg_53" n="HIAT:ip">)</nts>
                  <nts id="Seg_54" n="HIAT:ip">.</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_57" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_59" n="HIAT:w" s="T13">Da</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_61" n="HIAT:ip">(</nts>
                  <ts e="T15" id="Seg_63" n="HIAT:w" s="T14">šereʔ-</ts>
                  <nts id="Seg_64" n="HIAT:ip">)</nts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_67" n="HIAT:w" s="T15">šeriem</ts>
                  <nts id="Seg_68" n="HIAT:ip">.</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_71" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_73" n="HIAT:w" s="T16">Nʼe</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_76" n="HIAT:w" s="T17">axota</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_79" n="HIAT:w" s="T18">kanzittə</ts>
                  <nts id="Seg_80" n="HIAT:ip">,</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_83" n="HIAT:w" s="T19">ugandə</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_86" n="HIAT:w" s="T20">šeriem</ts>
                  <nts id="Seg_87" n="HIAT:ip">.</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_90" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_92" n="HIAT:w" s="T21">Kazan</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_95" n="HIAT:w" s="T22">turagən</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_98" n="HIAT:w" s="T23">multʼanə</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_100" n="HIAT:ip">(</nts>
                  <ts e="T25" id="Seg_102" n="HIAT:w" s="T24">ej</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_105" n="HIAT:w" s="T25">kanʼ-</ts>
                  <nts id="Seg_106" n="HIAT:ip">)</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_109" n="HIAT:w" s="T26">ej</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_112" n="HIAT:w" s="T27">kaliaʔi</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_114" n="HIAT:ip">(</nts>
                  <ts e="T29" id="Seg_116" n="HIAT:w" s="T28">obbərej</ts>
                  <nts id="Seg_117" n="HIAT:ip">)</nts>
                  <nts id="Seg_118" n="HIAT:ip">.</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_121" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_123" n="HIAT:w" s="T29">Nezeŋ</ts>
                  <nts id="Seg_124" n="HIAT:ip">,</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_127" n="HIAT:w" s="T30">tibizeŋ</ts>
                  <nts id="Seg_128" n="HIAT:ip">.</nts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_131" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_133" n="HIAT:w" s="T31">Nezeŋ</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_136" n="HIAT:w" s="T32">onʼiʔ</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_138" n="HIAT:ip">(</nts>
                  <ts e="T34" id="Seg_140" n="HIAT:w" s="T33">kaŋg-</ts>
                  <nts id="Seg_141" n="HIAT:ip">)</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_144" n="HIAT:w" s="T34">kandəʔi</ts>
                  <nts id="Seg_145" n="HIAT:ip">.</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_148" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_150" n="HIAT:w" s="T35">A</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_153" n="HIAT:w" s="T36">tibizeŋ</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_156" n="HIAT:w" s="T37">karəldʼan</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_159" n="HIAT:w" s="T38">nendəleʔbəʔjə</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_162" n="HIAT:w" s="T39">bazoʔ</ts>
                  <nts id="Seg_163" n="HIAT:ip">.</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_166" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_168" n="HIAT:w" s="T40">Onʼiʔ</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_171" n="HIAT:w" s="T41">kandəgaʔi</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_174" n="HIAT:w" s="T42">tibizeŋ</ts>
                  <nts id="Seg_175" n="HIAT:ip">.</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_178" n="HIAT:u" s="T43">
                  <nts id="Seg_179" n="HIAT:ip">(</nts>
                  <nts id="Seg_180" n="HIAT:ip">(</nts>
                  <ats e="T44" id="Seg_181" n="HIAT:non-pho" s="T43">BRK</ats>
                  <nts id="Seg_182" n="HIAT:ip">)</nts>
                  <nts id="Seg_183" n="HIAT:ip">)</nts>
                  <nts id="Seg_184" n="HIAT:ip">.</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_187" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_189" n="HIAT:w" s="T44">Miʔ</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_192" n="HIAT:w" s="T45">multʼagən</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_195" n="HIAT:w" s="T46">koʔbsaŋ</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_198" n="HIAT:w" s="T47">šidegöʔ</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_201" n="HIAT:w" s="T48">kandəgaʔi</ts>
                  <nts id="Seg_202" n="HIAT:ip">.</nts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_205" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_207" n="HIAT:w" s="T49">Dĭgəttə</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_210" n="HIAT:w" s="T50">iat</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_213" n="HIAT:w" s="T51">kaləj</ts>
                  <nts id="Seg_214" n="HIAT:ip">,</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_217" n="HIAT:w" s="T52">onʼiʔ</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_220" n="HIAT:w" s="T53">koʔbdo</ts>
                  <nts id="Seg_221" n="HIAT:ip">,</nts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_224" n="HIAT:w" s="T54">onʼiʔ</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_227" n="HIAT:w" s="T55">nʼi</ts>
                  <nts id="Seg_228" n="HIAT:ip">.</nts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_231" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_233" n="HIAT:w" s="T56">Băzləj</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_236" n="HIAT:w" s="T57">dĭzem</ts>
                  <nts id="Seg_237" n="HIAT:ip">.</nts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_240" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_242" n="HIAT:w" s="T58">Dĭzeŋ</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_244" n="HIAT:ip">(</nts>
                  <ts e="T60" id="Seg_246" n="HIAT:w" s="T59">šolə-</ts>
                  <nts id="Seg_247" n="HIAT:ip">)</nts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_250" n="HIAT:w" s="T60">šonəgaʔi</ts>
                  <nts id="Seg_251" n="HIAT:ip">,</nts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_254" n="HIAT:w" s="T61">dĭgəttə</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_257" n="HIAT:w" s="T62">tibi</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_259" n="HIAT:ip">(</nts>
                  <ts e="T64" id="Seg_261" n="HIAT:w" s="T63">kalaʔbə</ts>
                  <nts id="Seg_262" n="HIAT:ip">)</nts>
                  <nts id="Seg_263" n="HIAT:ip">.</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_266" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_268" n="HIAT:w" s="T64">A</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_271" n="HIAT:w" s="T65">dĭgəttə</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_274" n="HIAT:w" s="T66">măn</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_277" n="HIAT:w" s="T67">kalaʔbəm</ts>
                  <nts id="Seg_278" n="HIAT:ip">,</nts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_280" n="HIAT:ip">(</nts>
                  <ts e="T69" id="Seg_282" n="HIAT:w" s="T68">băzəjdliam</ts>
                  <nts id="Seg_283" n="HIAT:ip">)</nts>
                  <nts id="Seg_284" n="HIAT:ip">.</nts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_287" n="HIAT:u" s="T69">
                  <nts id="Seg_288" n="HIAT:ip">(</nts>
                  <nts id="Seg_289" n="HIAT:ip">(</nts>
                  <ats e="T70" id="Seg_290" n="HIAT:non-pho" s="T69">BRK</ats>
                  <nts id="Seg_291" n="HIAT:ip">)</nts>
                  <nts id="Seg_292" n="HIAT:ip">)</nts>
                  <nts id="Seg_293" n="HIAT:ip">.</nts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_296" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_298" n="HIAT:w" s="T70">Măn</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_301" n="HIAT:w" s="T71">tʼotkam</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_304" n="HIAT:w" s="T72">kambi</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T988" id="Seg_307" n="HIAT:w" s="T73">Kazan</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_310" n="HIAT:w" s="T988">turanə</ts>
                  <nts id="Seg_311" n="HIAT:ip">.</nts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_314" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_316" n="HIAT:w" s="T74">Dĭn</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_319" n="HIAT:w" s="T75">kak</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_322" n="HIAT:w" s="T76">raz</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_325" n="HIAT:w" s="T77">multʼa</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_328" n="HIAT:w" s="T78">nendəbiʔi</ts>
                  <nts id="Seg_329" n="HIAT:ip">.</nts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_332" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_334" n="HIAT:w" s="T79">Bar</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_337" n="HIAT:w" s="T80">băzəbiʔi</ts>
                  <nts id="Seg_338" n="HIAT:ip">,</nts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_341" n="HIAT:w" s="T81">măndəʔi:</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_344" n="HIAT:w" s="T82">Kanaʔ</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_347" n="HIAT:w" s="T83">moltʼanə</ts>
                  <nts id="Seg_348" n="HIAT:ip">.</nts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_351" n="HIAT:u" s="T84">
                  <nts id="Seg_352" n="HIAT:ip">(</nts>
                  <ts e="T85" id="Seg_354" n="HIAT:w" s="T84">A</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_357" n="HIAT:w" s="T85">măn=</ts>
                  <nts id="Seg_358" n="HIAT:ip">)</nts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_361" n="HIAT:w" s="T86">A</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_364" n="HIAT:w" s="T87">dĭ</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_367" n="HIAT:w" s="T88">kambi</ts>
                  <nts id="Seg_368" n="HIAT:ip">.</nts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_371" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_373" n="HIAT:w" s="T89">Ej</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_376" n="HIAT:w" s="T90">tĭmnet</ts>
                  <nts id="Seg_377" n="HIAT:ip">,</nts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_380" n="HIAT:w" s="T91">gibər</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_383" n="HIAT:w" s="T92">bü</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_386" n="HIAT:w" s="T93">kămnasʼtə</ts>
                  <nts id="Seg_387" n="HIAT:ip">.</nts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T104" id="Seg_390" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_392" n="HIAT:w" s="T94">Pʼeštə</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_395" n="HIAT:w" s="T95">kămnəbiam</ts>
                  <nts id="Seg_396" n="HIAT:ip">,</nts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_399" n="HIAT:w" s="T96">bar</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_402" n="HIAT:w" s="T97">stʼenaʔi</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_405" n="HIAT:w" s="T98">büzi</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_407" n="HIAT:ip">(</nts>
                  <ts e="T100" id="Seg_409" n="HIAT:w" s="T99">обл</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_412" n="HIAT:w" s="T100">-</ts>
                  <nts id="Seg_413" n="HIAT:ip">)</nts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_416" n="HIAT:w" s="T101">облила</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_419" n="HIAT:w" s="T102">стены</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_422" n="HIAT:w" s="T103">эти</ts>
                  <nts id="Seg_423" n="HIAT:ip">.</nts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_426" n="HIAT:u" s="T104">
                  <ts e="T104.tx.1" id="Seg_428" n="HIAT:w" s="T104">По-</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_431" n="HIAT:w" s="T104.tx.1">русски</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_433" n="HIAT:ip">(</nts>
                  <nts id="Seg_434" n="HIAT:ip">(</nts>
                  <ats e="T108" id="Seg_435" n="HIAT:non-pho" s="T106">BRK</ats>
                  <nts id="Seg_436" n="HIAT:ip">)</nts>
                  <nts id="Seg_437" n="HIAT:ip">)</nts>
                  <nts id="Seg_438" n="HIAT:ip">.</nts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T111" id="Seg_441" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_443" n="HIAT:w" s="T108">Dĭgəttə</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_446" n="HIAT:w" s="T109">băzəjbiam</ts>
                  <nts id="Seg_447" n="HIAT:ip">,</nts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_450" n="HIAT:w" s="T110">šobiam</ts>
                  <nts id="Seg_451" n="HIAT:ip">.</nts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_454" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_456" n="HIAT:w" s="T111">Ĭmbi</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_459" n="HIAT:w" s="T112">băzəjbial</ts>
                  <nts id="Seg_460" n="HIAT:ip">,</nts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_463" n="HIAT:w" s="T113">băzəjbial</ts>
                  <nts id="Seg_464" n="HIAT:ip">,</nts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_467" n="HIAT:w" s="T114">ej</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_469" n="HIAT:ip">(</nts>
                  <ts e="T116" id="Seg_471" n="HIAT:w" s="T115">embiel</ts>
                  <nts id="Seg_472" n="HIAT:ip">)</nts>
                  <nts id="Seg_473" n="HIAT:ip">?</nts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_476" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_478" n="HIAT:w" s="T116">Ej</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_481" n="HIAT:w" s="T117">embiem</ts>
                  <nts id="Seg_482" n="HIAT:ip">!</nts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_485" n="HIAT:u" s="T118">
                  <nts id="Seg_486" n="HIAT:ip">(</nts>
                  <nts id="Seg_487" n="HIAT:ip">(</nts>
                  <ats e="T119" id="Seg_488" n="HIAT:non-pho" s="T118">BRK</ats>
                  <nts id="Seg_489" n="HIAT:ip">)</nts>
                  <nts id="Seg_490" n="HIAT:ip">)</nts>
                  <nts id="Seg_491" n="HIAT:ip">.</nts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_494" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_496" n="HIAT:w" s="T119">Dĭgəttə</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_498" n="HIAT:ip">(</nts>
                  <ts e="T121" id="Seg_500" n="HIAT:w" s="T120">o-</ts>
                  <nts id="Seg_501" n="HIAT:ip">)</nts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_504" n="HIAT:w" s="T121">mănliaʔi:</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_507" n="HIAT:w" s="T122">Kanaʔ</ts>
                  <nts id="Seg_508" n="HIAT:ip">!</nts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_511" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_513" n="HIAT:w" s="T123">Pădvalbə</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_516" n="HIAT:w" s="T124">uja</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_519" n="HIAT:w" s="T125">deʔ</ts>
                  <nts id="Seg_520" n="HIAT:ip">.</nts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_523" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_525" n="HIAT:w" s="T126">Kătletaʔinə</ts>
                  <nts id="Seg_526" n="HIAT:ip">.</nts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T129" id="Seg_529" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_531" n="HIAT:w" s="T127">Măn</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_534" n="HIAT:w" s="T128">kambiam</ts>
                  <nts id="Seg_535" n="HIAT:ip">.</nts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_538" n="HIAT:u" s="T129">
                  <nts id="Seg_539" n="HIAT:ip">(</nts>
                  <ts e="T130" id="Seg_541" n="HIAT:w" s="T129">Nugia-</ts>
                  <nts id="Seg_542" n="HIAT:ip">)</nts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_545" n="HIAT:w" s="T130">Nugam</ts>
                  <nts id="Seg_546" n="HIAT:ip">,</nts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_549" n="HIAT:w" s="T131">uja</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_552" n="HIAT:w" s="T132">iʔgö</ts>
                  <nts id="Seg_553" n="HIAT:ip">,</nts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_556" n="HIAT:w" s="T133">a</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_559" n="HIAT:w" s="T134">aspaʔ</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_562" n="HIAT:w" s="T135">naga</ts>
                  <nts id="Seg_563" n="HIAT:ip">.</nts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T138" id="Seg_566" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_568" n="HIAT:w" s="T136">Dĭgəttə</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_571" n="HIAT:w" s="T137">parbiam</ts>
                  <nts id="Seg_572" n="HIAT:ip">.</nts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_575" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_577" n="HIAT:w" s="T138">Ĭmbi</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_580" n="HIAT:w" s="T139">uja</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_583" n="HIAT:w" s="T140">ej</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_585" n="HIAT:ip">(</nts>
                  <ts e="T142" id="Seg_587" n="HIAT:w" s="T141">det-</ts>
                  <nts id="Seg_588" n="HIAT:ip">)</nts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_591" n="HIAT:w" s="T142">detlel</ts>
                  <nts id="Seg_592" n="HIAT:ip">?</nts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_595" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_597" n="HIAT:w" s="T143">Da</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_600" n="HIAT:w" s="T144">uja</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_603" n="HIAT:w" s="T145">iʔgö</ts>
                  <nts id="Seg_604" n="HIAT:ip">,</nts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_607" n="HIAT:w" s="T146">a</ts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_610" n="HIAT:w" s="T147">aspaʔ</ts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_613" n="HIAT:w" s="T148">naga</ts>
                  <nts id="Seg_614" n="HIAT:ip">.</nts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_617" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_619" n="HIAT:w" s="T149">Dĭzeŋ</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_622" n="HIAT:w" s="T150">davaj</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_625" n="HIAT:w" s="T151">kaknarzittə</ts>
                  <nts id="Seg_626" n="HIAT:ip">!</nts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_629" n="HIAT:u" s="T152">
                  <nts id="Seg_630" n="HIAT:ip">(</nts>
                  <nts id="Seg_631" n="HIAT:ip">(</nts>
                  <ats e="T153" id="Seg_632" n="HIAT:non-pho" s="T152">BRK</ats>
                  <nts id="Seg_633" n="HIAT:ip">)</nts>
                  <nts id="Seg_634" n="HIAT:ip">)</nts>
                  <nts id="Seg_635" n="HIAT:ip">.</nts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_638" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_640" n="HIAT:w" s="T153">Dĭgəttə</ts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_643" n="HIAT:w" s="T154">mănliaʔi:</ts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_646" n="HIAT:w" s="T155">Kanaʔ</ts>
                  <nts id="Seg_647" n="HIAT:ip">!</nts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T159" id="Seg_650" n="HIAT:u" s="T156">
                  <ts e="T157" id="Seg_652" n="HIAT:w" s="T156">Ăgărottə</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_655" n="HIAT:w" s="T157">deʔ</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_658" n="HIAT:w" s="T158">ăgurcɨʔi</ts>
                  <nts id="Seg_659" n="HIAT:ip">.</nts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_662" n="HIAT:u" s="T159">
                  <ts e="T160" id="Seg_664" n="HIAT:w" s="T159">Măn</ts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_666" n="HIAT:ip">(</nts>
                  <ts e="T161" id="Seg_668" n="HIAT:w" s="T160">kambiam</ts>
                  <nts id="Seg_669" n="HIAT:ip">)</nts>
                  <nts id="Seg_670" n="HIAT:ip">.</nts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T163" id="Seg_673" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_675" n="HIAT:w" s="T161">Măndərbiam</ts>
                  <nts id="Seg_676" n="HIAT:ip">,</nts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_679" n="HIAT:w" s="T162">măndərbiam</ts>
                  <nts id="Seg_680" n="HIAT:ip">.</nts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_683" n="HIAT:u" s="T163">
                  <ts e="T164" id="Seg_685" n="HIAT:w" s="T163">Nĭŋgəliem</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_687" n="HIAT:ip">(</nts>
                  <ts e="T165" id="Seg_689" n="HIAT:w" s="T164">toŋtanoʔ</ts>
                  <nts id="Seg_690" n="HIAT:ip">)</nts>
                  <nts id="Seg_691" n="HIAT:ip">,</nts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_694" n="HIAT:w" s="T165">išo</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_697" n="HIAT:w" s="T166">ĭmbi-nʼibudʼ</ts>
                  <nts id="Seg_698" n="HIAT:ip">.</nts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T176" id="Seg_701" n="HIAT:u" s="T167">
                  <ts e="T168" id="Seg_703" n="HIAT:w" s="T167">Dĭgəttə</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_705" n="HIAT:ip">(</nts>
                  <ts e="T169" id="Seg_707" n="HIAT:w" s="T168">napal</ts>
                  <nts id="Seg_708" n="HIAT:ip">)</nts>
                  <nts id="Seg_709" n="HIAT:ip">;</nts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_712" n="HIAT:w" s="T169">šide</ts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_714" n="HIAT:ip">(</nts>
                  <ts e="T171" id="Seg_716" n="HIAT:w" s="T170">dɨ-</ts>
                  <nts id="Seg_717" n="HIAT:ip">)</nts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_720" n="HIAT:w" s="T171">тыква</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_723" n="HIAT:w" s="T172">ibiem</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_726" n="HIAT:w" s="T173">i</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_729" n="HIAT:w" s="T174">šobiam</ts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_732" n="HIAT:w" s="T175">turanə</ts>
                  <nts id="Seg_733" n="HIAT:ip">.</nts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_736" n="HIAT:u" s="T176">
                  <ts e="T177" id="Seg_738" n="HIAT:w" s="T176">Onʼiʔ</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_741" n="HIAT:w" s="T177">ne</ts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_744" n="HIAT:w" s="T178">pătpollʼanə</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_746" n="HIAT:ip">(</nts>
                  <ts e="T180" id="Seg_748" n="HIAT:w" s="T179">š-</ts>
                  <nts id="Seg_749" n="HIAT:ip">)</nts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_752" n="HIAT:w" s="T180">süʔmiluʔpi</ts>
                  <nts id="Seg_753" n="HIAT:ip">.</nts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T185" id="Seg_756" n="HIAT:u" s="T181">
                  <ts e="T182" id="Seg_758" n="HIAT:w" s="T181">Onʼiʔ</ts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_760" n="HIAT:ip">(</nts>
                  <ts e="T183" id="Seg_762" n="HIAT:w" s="T182">n-</ts>
                  <nts id="Seg_763" n="HIAT:ip">)</nts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_766" n="HIAT:w" s="T183">ne</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_769" n="HIAT:w" s="T184">nʼiʔtə</ts>
                  <nts id="Seg_770" n="HIAT:ip">.</nts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T190" id="Seg_773" n="HIAT:u" s="T185">
                  <ts e="T186" id="Seg_775" n="HIAT:w" s="T185">Aʔpi</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_777" n="HIAT:ip">(</nts>
                  <ts e="T187" id="Seg_779" n="HIAT:w" s="T186">min-</ts>
                  <nts id="Seg_780" n="HIAT:ip">)</nts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_783" n="HIAT:w" s="T187">măna</ts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_786" n="HIAT:w" s="T188">ej</ts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_789" n="HIAT:w" s="T189">tonuʔpi</ts>
                  <nts id="Seg_790" n="HIAT:ip">.</nts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T191" id="Seg_793" n="HIAT:u" s="T190">
                  <nts id="Seg_794" n="HIAT:ip">(</nts>
                  <nts id="Seg_795" n="HIAT:ip">(</nts>
                  <ats e="T191" id="Seg_796" n="HIAT:non-pho" s="T190">BRK</ats>
                  <nts id="Seg_797" n="HIAT:ip">)</nts>
                  <nts id="Seg_798" n="HIAT:ip">)</nts>
                  <nts id="Seg_799" n="HIAT:ip">.</nts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T201" id="Seg_802" n="HIAT:u" s="T191">
                  <ts e="T192" id="Seg_804" n="HIAT:w" s="T191">Dĭgəttə</ts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_806" n="HIAT:ip">(</nts>
                  <ts e="T193" id="Seg_808" n="HIAT:w" s="T192">dĭj-</ts>
                  <nts id="Seg_809" n="HIAT:ip">)</nts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_812" n="HIAT:w" s="T193">dĭ</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_815" n="HIAT:w" s="T194">nezen</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_817" n="HIAT:ip">(</nts>
                  <ts e="T196" id="Seg_819" n="HIAT:w" s="T195">aba=</ts>
                  <nts id="Seg_820" n="HIAT:ip">)</nts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_823" n="HIAT:w" s="T196">abat</ts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_826" n="HIAT:w" s="T197">măndə:</ts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_829" n="HIAT:w" s="T198">Gibər</ts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_832" n="HIAT:w" s="T199">šiʔ</ts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_835" n="HIAT:w" s="T200">nuʔməlambilaʔ</ts>
                  <nts id="Seg_836" n="HIAT:ip">?</nts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T205" id="Seg_839" n="HIAT:u" s="T201">
                  <ts e="T202" id="Seg_841" n="HIAT:w" s="T201">Deʔkeʔ</ts>
                  <nts id="Seg_842" n="HIAT:ip">,</nts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_845" n="HIAT:w" s="T202">a</ts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_848" n="HIAT:w" s="T203">kămnəgaʔ</ts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_851" n="HIAT:w" s="T204">mĭj</ts>
                  <nts id="Seg_852" n="HIAT:ip">.</nts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T214" id="Seg_855" n="HIAT:u" s="T205">
                  <ts e="T206" id="Seg_857" n="HIAT:w" s="T205">Abam</ts>
                  <nts id="Seg_858" n="HIAT:ip">,</nts>
                  <nts id="Seg_859" n="HIAT:ip">—</nts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_862" n="HIAT:w" s="T206">măndə</ts>
                  <nts id="Seg_863" n="HIAT:ip">,</nts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_866" n="HIAT:w" s="T207">kuiol</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_869" n="HIAT:w" s="T208">dĭ</ts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_872" n="HIAT:w" s="T209">šide</ts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_875" n="HIAT:w" s="T210">dĭʔə</ts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_877" n="HIAT:ip">(</nts>
                  <ts e="T212" id="Seg_879" n="HIAT:w" s="T211">tɨkla-</ts>
                  <nts id="Seg_880" n="HIAT:ip">)</nts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_883" n="HIAT:w" s="T212">тыква</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_886" n="HIAT:w" s="T213">deʔpi</ts>
                  <nts id="Seg_887" n="HIAT:ip">.</nts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T224" id="Seg_890" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_892" n="HIAT:w" s="T214">A</ts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_895" n="HIAT:w" s="T215">ĭmbi</ts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_898" n="HIAT:w" s="T216">öʔlieʔi</ts>
                  <nts id="Seg_899" n="HIAT:ip">,</nts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_902" n="HIAT:w" s="T217">kuza</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_905" n="HIAT:w" s="T218">ej</ts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_908" n="HIAT:w" s="T219">tĭmnet</ts>
                  <nts id="Seg_909" n="HIAT:ip">,</nts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_912" n="HIAT:w" s="T220">a</ts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_915" n="HIAT:w" s="T221">šiʔ</ts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_917" n="HIAT:ip">(</nts>
                  <ts e="T223" id="Seg_919" n="HIAT:w" s="T222">öʔleʔil-</ts>
                  <nts id="Seg_920" n="HIAT:ip">)</nts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_923" n="HIAT:w" s="T223">öʔleileʔ</ts>
                  <nts id="Seg_924" n="HIAT:ip">"</nts>
                  <nts id="Seg_925" n="HIAT:ip">.</nts>
                  <nts id="Seg_926" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T225" id="Seg_928" n="HIAT:u" s="T224">
                  <nts id="Seg_929" n="HIAT:ip">(</nts>
                  <nts id="Seg_930" n="HIAT:ip">(</nts>
                  <ats e="T225" id="Seg_931" n="HIAT:non-pho" s="T224">BRK</ats>
                  <nts id="Seg_932" n="HIAT:ip">)</nts>
                  <nts id="Seg_933" n="HIAT:ip">)</nts>
                  <nts id="Seg_934" n="HIAT:ip">.</nts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T232" id="Seg_937" n="HIAT:u" s="T225">
                  <ts e="T226" id="Seg_939" n="HIAT:w" s="T225">Onʼiʔ</ts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_942" n="HIAT:w" s="T226">ne</ts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_945" n="HIAT:w" s="T227">Kazan</ts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_948" n="HIAT:w" s="T228">turanə</ts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_951" n="HIAT:w" s="T229">mĭmbi</ts>
                  <nts id="Seg_952" n="HIAT:ip">,</nts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_954" n="HIAT:ip">(</nts>
                  <ts e="T231" id="Seg_956" n="HIAT:w" s="T230">inettə-</ts>
                  <nts id="Seg_957" n="HIAT:ip">)</nts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_960" n="HIAT:w" s="T231">inetsi</ts>
                  <nts id="Seg_961" n="HIAT:ip">.</nts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T235" id="Seg_964" n="HIAT:u" s="T232">
                  <nts id="Seg_965" n="HIAT:ip">(</nts>
                  <ts e="T233" id="Seg_967" n="HIAT:w" s="T232">Šo-</ts>
                  <nts id="Seg_968" n="HIAT:ip">)</nts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_971" n="HIAT:w" s="T233">Šobi</ts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_974" n="HIAT:w" s="T234">maʔndə</ts>
                  <nts id="Seg_975" n="HIAT:ip">.</nts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T239" id="Seg_978" n="HIAT:u" s="T235">
                  <ts e="T236" id="Seg_980" n="HIAT:w" s="T235">I</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_983" n="HIAT:w" s="T236">kubi:</ts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_986" n="HIAT:w" s="T237">pa</ts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_989" n="HIAT:w" s="T238">iʔbolaʔbə</ts>
                  <nts id="Seg_990" n="HIAT:ip">.</nts>
                  <nts id="Seg_991" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T246" id="Seg_993" n="HIAT:u" s="T239">
                  <nts id="Seg_994" n="HIAT:ip">(</nts>
                  <ts e="T240" id="Seg_996" n="HIAT:w" s="T239">Š-</ts>
                  <nts id="Seg_997" n="HIAT:ip">)</nts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_1000" n="HIAT:w" s="T240">Dĭgəttə</ts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_1003" n="HIAT:w" s="T241">măndə:</ts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_1006" n="HIAT:w" s="T242">dĭn</ts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_1009" n="HIAT:w" s="T243">červə</ts>
                  <nts id="Seg_1010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_1012" n="HIAT:w" s="T244">bar</ts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_1015" n="HIAT:w" s="T245">ambi</ts>
                  <nts id="Seg_1016" n="HIAT:ip">.</nts>
                  <nts id="Seg_1017" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T255" id="Seg_1019" n="HIAT:u" s="T246">
                  <nts id="Seg_1020" n="HIAT:ip">(</nts>
                  <ts e="T247" id="Seg_1022" n="HIAT:w" s="T246">Dĭrg-</ts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_1025" n="HIAT:w" s="T247">dĭgət-</ts>
                  <nts id="Seg_1026" n="HIAT:ip">)</nts>
                  <nts id="Seg_1027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_1029" n="HIAT:w" s="T248">Dĭrgit</ts>
                  <nts id="Seg_1030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_1032" n="HIAT:w" s="T249">paʔi</ts>
                  <nts id="Seg_1033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_1035" n="HIAT:w" s="T250">možna</ts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_1038" n="HIAT:w" s="T251">toʔnarzittə</ts>
                  <nts id="Seg_1039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_1041" n="HIAT:w" s="T252">i</ts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1043" n="HIAT:ip">(</nts>
                  <ts e="T254" id="Seg_1045" n="HIAT:w" s="T253">paʔi</ts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_1048" n="HIAT:w" s="T254">boləj</ts>
                  <nts id="Seg_1049" n="HIAT:ip">)</nts>
                  <nts id="Seg_1050" n="HIAT:ip">.</nts>
                  <nts id="Seg_1051" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T256" id="Seg_1053" n="HIAT:u" s="T255">
                  <nts id="Seg_1054" n="HIAT:ip">(</nts>
                  <nts id="Seg_1055" n="HIAT:ip">(</nts>
                  <ats e="T256" id="Seg_1056" n="HIAT:non-pho" s="T255">BRK</ats>
                  <nts id="Seg_1057" n="HIAT:ip">)</nts>
                  <nts id="Seg_1058" n="HIAT:ip">)</nts>
                  <nts id="Seg_1059" n="HIAT:ip">.</nts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T263" id="Seg_1062" n="HIAT:u" s="T256">
                  <ts e="T257" id="Seg_1064" n="HIAT:w" s="T256">Dĭrgit</ts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_1067" n="HIAT:w" s="T257">bar</ts>
                  <nts id="Seg_1068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_1070" n="HIAT:w" s="T258">abi</ts>
                  <nts id="Seg_1071" n="HIAT:ip">,</nts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_1074" n="HIAT:w" s="T259">üdʼüge</ts>
                  <nts id="Seg_1075" n="HIAT:ip">,</nts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_1078" n="HIAT:w" s="T260">pʼeš</ts>
                  <nts id="Seg_1079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1081" n="HIAT:w" s="T261">možna</ts>
                  <nts id="Seg_1082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1084" n="HIAT:w" s="T262">nendəsʼtə</ts>
                  <nts id="Seg_1085" n="HIAT:ip">.</nts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T264" id="Seg_1088" n="HIAT:u" s="T263">
                  <nts id="Seg_1089" n="HIAT:ip">(</nts>
                  <nts id="Seg_1090" n="HIAT:ip">(</nts>
                  <ats e="T264" id="Seg_1091" n="HIAT:non-pho" s="T263">BRK</ats>
                  <nts id="Seg_1092" n="HIAT:ip">)</nts>
                  <nts id="Seg_1093" n="HIAT:ip">)</nts>
                  <nts id="Seg_1094" n="HIAT:ip">.</nts>
                  <nts id="Seg_1095" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T268" id="Seg_1097" n="HIAT:u" s="T264">
                  <ts e="T265" id="Seg_1099" n="HIAT:w" s="T264">Dĭgəttə</ts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1102" n="HIAT:w" s="T265">Varlam</ts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1105" n="HIAT:w" s="T266">deʔpi</ts>
                  <nts id="Seg_1106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1108" n="HIAT:w" s="T267">kujdərgan</ts>
                  <nts id="Seg_1109" n="HIAT:ip">.</nts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T272" id="Seg_1112" n="HIAT:u" s="T268">
                  <ts e="T269" id="Seg_1114" n="HIAT:w" s="T268">Nuldaʔ</ts>
                  <nts id="Seg_1115" n="HIAT:ip">,</nts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1117" n="HIAT:ip">(</nts>
                  <ts e="T270" id="Seg_1119" n="HIAT:w" s="T269">măndə-</ts>
                  <nts id="Seg_1120" n="HIAT:ip">)</nts>
                  <nts id="Seg_1121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1123" n="HIAT:w" s="T270">măndə</ts>
                  <nts id="Seg_1124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1126" n="HIAT:w" s="T271">dĭʔnə</ts>
                  <nts id="Seg_1127" n="HIAT:ip">.</nts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T281" id="Seg_1130" n="HIAT:u" s="T272">
                  <ts e="T273" id="Seg_1132" n="HIAT:w" s="T272">Dĭ</ts>
                  <nts id="Seg_1133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1135" n="HIAT:w" s="T273">kambi</ts>
                  <nts id="Seg_1136" n="HIAT:ip">,</nts>
                  <nts id="Seg_1137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1139" n="HIAT:w" s="T274">bar</ts>
                  <nts id="Seg_1140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1142" n="HIAT:w" s="T275">trubagəndə</ts>
                  <nts id="Seg_1143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1145" n="HIAT:w" s="T276">bü</ts>
                  <nts id="Seg_1146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1148" n="HIAT:w" s="T277">embi</ts>
                  <nts id="Seg_1149" n="HIAT:ip">,</nts>
                  <nts id="Seg_1150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1152" n="HIAT:w" s="T278">embi</ts>
                  <nts id="Seg_1153" n="HIAT:ip">,</nts>
                  <nts id="Seg_1154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1156" n="HIAT:w" s="T279">kămnəbi</ts>
                  <nts id="Seg_1157" n="HIAT:ip">,</nts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1160" n="HIAT:w" s="T280">kămnəbi</ts>
                  <nts id="Seg_1161" n="HIAT:ip">.</nts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T286" id="Seg_1164" n="HIAT:u" s="T281">
                  <ts e="T282" id="Seg_1166" n="HIAT:w" s="T281">I</ts>
                  <nts id="Seg_1167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1169" n="HIAT:w" s="T282">dĭbər</ts>
                  <nts id="Seg_1170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1171" n="HIAT:ip">(</nts>
                  <ts e="T284" id="Seg_1173" n="HIAT:w" s="T283">ko-</ts>
                  <nts id="Seg_1174" n="HIAT:ip">)</nts>
                  <nts id="Seg_1175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1177" n="HIAT:w" s="T284">kös</ts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1180" n="HIAT:w" s="T285">embi</ts>
                  <nts id="Seg_1181" n="HIAT:ip">.</nts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T293" id="Seg_1184" n="HIAT:u" s="T286">
                  <ts e="T287" id="Seg_1186" n="HIAT:w" s="T286">Šobi</ts>
                  <nts id="Seg_1187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1189" n="HIAT:w" s="T287">turanə</ts>
                  <nts id="Seg_1190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1192" n="HIAT:w" s="T288">da</ts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1195" n="HIAT:w" s="T289">măndə:</ts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1198" n="HIAT:w" s="T290">Samovar</ts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1201" n="HIAT:w" s="T291">mʼaŋnaʔbə</ts>
                  <nts id="Seg_1202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1203" n="HIAT:ip">(</nts>
                  <ts e="T293" id="Seg_1205" n="HIAT:w" s="T292">bar</ts>
                  <nts id="Seg_1206" n="HIAT:ip">)</nts>
                  <nts id="Seg_1207" n="HIAT:ip">.</nts>
                  <nts id="Seg_1208" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T297" id="Seg_1210" n="HIAT:u" s="T293">
                  <ts e="T294" id="Seg_1212" n="HIAT:w" s="T293">Kujdərgan</ts>
                  <nts id="Seg_1213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1215" n="HIAT:w" s="T294">mʼaŋnaʔbə</ts>
                  <nts id="Seg_1216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1218" n="HIAT:w" s="T295">bar</ts>
                  <nts id="Seg_1219" n="HIAT:ip">,</nts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1222" n="HIAT:w" s="T296">bü</ts>
                  <nts id="Seg_1223" n="HIAT:ip">.</nts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T300" id="Seg_1226" n="HIAT:u" s="T297">
                  <ts e="T298" id="Seg_1228" n="HIAT:w" s="T297">Dĭ</ts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1231" n="HIAT:w" s="T298">kambi</ts>
                  <nts id="Seg_1232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1234" n="HIAT:w" s="T299">bar</ts>
                  <nts id="Seg_1235" n="HIAT:ip">.</nts>
                  <nts id="Seg_1236" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T308" id="Seg_1238" n="HIAT:u" s="T300">
                  <ts e="T301" id="Seg_1240" n="HIAT:w" s="T300">Döbər</ts>
                  <nts id="Seg_1241" n="HIAT:ip">,</nts>
                  <nts id="Seg_1242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1244" n="HIAT:w" s="T301">măndə</ts>
                  <nts id="Seg_1245" n="HIAT:ip">,</nts>
                  <nts id="Seg_1246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1248" n="HIAT:w" s="T302">bü</ts>
                  <nts id="Seg_1249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1251" n="HIAT:w" s="T303">eneʔ</ts>
                  <nts id="Seg_1252" n="HIAT:ip">,</nts>
                  <nts id="Seg_1253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1255" n="HIAT:w" s="T304">a</ts>
                  <nts id="Seg_1256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1258" n="HIAT:w" s="T305">döbər</ts>
                  <nts id="Seg_1259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1261" n="HIAT:w" s="T306">kös</ts>
                  <nts id="Seg_1262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1264" n="HIAT:w" s="T307">eneʔ</ts>
                  <nts id="Seg_1265" n="HIAT:ip">.</nts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T312" id="Seg_1268" n="HIAT:u" s="T308">
                  <ts e="T309" id="Seg_1270" n="HIAT:w" s="T308">Dĭgəttə</ts>
                  <nts id="Seg_1271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1272" n="HIAT:ip">(</nts>
                  <ts e="T310" id="Seg_1274" n="HIAT:w" s="T309">dĭ</ts>
                  <nts id="Seg_1275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1277" n="HIAT:w" s="T310">didro</ts>
                  <nts id="Seg_1278" n="HIAT:ip">)</nts>
                  <nts id="Seg_1279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1281" n="HIAT:w" s="T311">abi</ts>
                  <nts id="Seg_1282" n="HIAT:ip">.</nts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T313" id="Seg_1285" n="HIAT:u" s="T312">
                  <nts id="Seg_1286" n="HIAT:ip">(</nts>
                  <nts id="Seg_1287" n="HIAT:ip">(</nts>
                  <ats e="T313" id="Seg_1288" n="HIAT:non-pho" s="T312">BRK</ats>
                  <nts id="Seg_1289" n="HIAT:ip">)</nts>
                  <nts id="Seg_1290" n="HIAT:ip">)</nts>
                  <nts id="Seg_1291" n="HIAT:ip">.</nts>
                  <nts id="Seg_1292" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T320" id="Seg_1294" n="HIAT:u" s="T313">
                  <ts e="T314" id="Seg_1296" n="HIAT:w" s="T313">Pa</ts>
                  <nts id="Seg_1297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1299" n="HIAT:w" s="T314">kambiʔi</ts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1302" n="HIAT:w" s="T315">jaʔsittə</ts>
                  <nts id="Seg_1303" n="HIAT:ip">,</nts>
                  <nts id="Seg_1304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1306" n="HIAT:w" s="T316">i</ts>
                  <nts id="Seg_1307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1309" n="HIAT:w" s="T317">nʼamga</ts>
                  <nts id="Seg_1310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1312" n="HIAT:w" s="T318">bü</ts>
                  <nts id="Seg_1313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1315" n="HIAT:w" s="T319">deʔpiʔi</ts>
                  <nts id="Seg_1316" n="HIAT:ip">.</nts>
                  <nts id="Seg_1317" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T325" id="Seg_1319" n="HIAT:u" s="T320">
                  <nts id="Seg_1320" n="HIAT:ip">(</nts>
                  <ts e="T321" id="Seg_1322" n="HIAT:w" s="T320">Pa-</ts>
                  <nts id="Seg_1323" n="HIAT:ip">)</nts>
                  <nts id="Seg_1324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1326" n="HIAT:w" s="T321">Pagən</ts>
                  <nts id="Seg_1327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1329" n="HIAT:w" s="T322">nʼamga</ts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1332" n="HIAT:w" s="T323">bü</ts>
                  <nts id="Seg_1333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1335" n="HIAT:w" s="T324">ibi</ts>
                  <nts id="Seg_1336" n="HIAT:ip">.</nts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T326" id="Seg_1339" n="HIAT:u" s="T325">
                  <nts id="Seg_1340" n="HIAT:ip">(</nts>
                  <nts id="Seg_1341" n="HIAT:ip">(</nts>
                  <ats e="T326" id="Seg_1342" n="HIAT:non-pho" s="T325">BRK</ats>
                  <nts id="Seg_1343" n="HIAT:ip">)</nts>
                  <nts id="Seg_1344" n="HIAT:ip">)</nts>
                  <nts id="Seg_1345" n="HIAT:ip">.</nts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T330" id="Seg_1348" n="HIAT:u" s="T326">
                  <ts e="T327" id="Seg_1350" n="HIAT:w" s="T326">Sĭre</ts>
                  <nts id="Seg_1351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1353" n="HIAT:w" s="T327">pa</ts>
                  <nts id="Seg_1354" n="HIAT:ip">,</nts>
                  <nts id="Seg_1355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1357" n="HIAT:w" s="T328">kojü</ts>
                  <nts id="Seg_1358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1360" n="HIAT:w" s="T329">pa</ts>
                  <nts id="Seg_1361" n="HIAT:ip">.</nts>
                  <nts id="Seg_1362" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T331" id="Seg_1364" n="HIAT:u" s="T330">
                  <nts id="Seg_1365" n="HIAT:ip">(</nts>
                  <nts id="Seg_1366" n="HIAT:ip">(</nts>
                  <ats e="T331" id="Seg_1367" n="HIAT:non-pho" s="T330">BRK</ats>
                  <nts id="Seg_1368" n="HIAT:ip">)</nts>
                  <nts id="Seg_1369" n="HIAT:ip">)</nts>
                  <nts id="Seg_1370" n="HIAT:ip">.</nts>
                  <nts id="Seg_1371" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T335" id="Seg_1373" n="HIAT:u" s="T331">
                  <ts e="T332" id="Seg_1375" n="HIAT:w" s="T331">Šomi</ts>
                  <nts id="Seg_1376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1378" n="HIAT:w" s="T332">pa</ts>
                  <nts id="Seg_1379" n="HIAT:ip">,</nts>
                  <nts id="Seg_1380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1382" n="HIAT:w" s="T333">san</ts>
                  <nts id="Seg_1383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1385" n="HIAT:w" s="T334">pa</ts>
                  <nts id="Seg_1386" n="HIAT:ip">.</nts>
                  <nts id="Seg_1387" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T340" id="Seg_1389" n="HIAT:u" s="T335">
                  <nts id="Seg_1390" n="HIAT:ip">(</nts>
                  <nts id="Seg_1391" n="HIAT:ip">(</nts>
                  <ats e="T336" id="Seg_1392" n="HIAT:non-pho" s="T335">BRK</ats>
                  <nts id="Seg_1393" n="HIAT:ip">)</nts>
                  <nts id="Seg_1394" n="HIAT:ip">)</nts>
                  <nts id="Seg_1395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1397" n="HIAT:w" s="T336">говорила</ts>
                  <nts id="Seg_1398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1399" n="HIAT:ip">(</nts>
                  <ts e="T338" id="Seg_1401" n="HIAT:w" s="T337">lö-</ts>
                  <nts id="Seg_1402" n="HIAT:ip">)</nts>
                  <nts id="Seg_1403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1404" n="HIAT:ip">(</nts>
                  <ts e="T339" id="Seg_1406" n="HIAT:w" s="T338">len</ts>
                  <nts id="Seg_1407" n="HIAT:ip">)</nts>
                  <nts id="Seg_1408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1410" n="HIAT:w" s="T339">pa</ts>
                  <nts id="Seg_1411" n="HIAT:ip">.</nts>
                  <nts id="Seg_1412" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T341" id="Seg_1414" n="HIAT:u" s="T340">
                  <nts id="Seg_1415" n="HIAT:ip">(</nts>
                  <nts id="Seg_1416" n="HIAT:ip">(</nts>
                  <ats e="T341" id="Seg_1417" n="HIAT:non-pho" s="T340">BRK</ats>
                  <nts id="Seg_1418" n="HIAT:ip">)</nts>
                  <nts id="Seg_1419" n="HIAT:ip">)</nts>
                  <nts id="Seg_1420" n="HIAT:ip">.</nts>
                  <nts id="Seg_1421" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T345" id="Seg_1423" n="HIAT:u" s="T341">
                  <ts e="T342" id="Seg_1425" n="HIAT:w" s="T341">Muʔjə</ts>
                  <nts id="Seg_1426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1428" n="HIAT:w" s="T342">ugandə</ts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1431" n="HIAT:w" s="T343">iʔgö</ts>
                  <nts id="Seg_1432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1434" n="HIAT:w" s="T344">dʼijegən</ts>
                  <nts id="Seg_1435" n="HIAT:ip">.</nts>
                  <nts id="Seg_1436" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T351" id="Seg_1438" n="HIAT:u" s="T345">
                  <ts e="T346" id="Seg_1440" n="HIAT:w" s="T345">Kalal</ts>
                  <nts id="Seg_1441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1443" n="HIAT:w" s="T346">dăk</ts>
                  <nts id="Seg_1444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1446" n="HIAT:w" s="T347">bar</ts>
                  <nts id="Seg_1447" n="HIAT:ip">,</nts>
                  <nts id="Seg_1448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1450" n="HIAT:w" s="T348">üge</ts>
                  <nts id="Seg_1451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1453" n="HIAT:w" s="T349">üjüzi</ts>
                  <nts id="Seg_1454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1456" n="HIAT:w" s="T350">tonuʔlal</ts>
                  <nts id="Seg_1457" n="HIAT:ip">.</nts>
                  <nts id="Seg_1458" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T352" id="Seg_1460" n="HIAT:u" s="T351">
                  <nts id="Seg_1461" n="HIAT:ip">(</nts>
                  <nts id="Seg_1462" n="HIAT:ip">(</nts>
                  <ats e="T352" id="Seg_1463" n="HIAT:non-pho" s="T351">BRK</ats>
                  <nts id="Seg_1464" n="HIAT:ip">)</nts>
                  <nts id="Seg_1465" n="HIAT:ip">)</nts>
                  <nts id="Seg_1466" n="HIAT:ip">.</nts>
                  <nts id="Seg_1467" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T354" id="Seg_1469" n="HIAT:u" s="T352">
                  <ts e="T353" id="Seg_1471" n="HIAT:w" s="T352">Dărowă</ts>
                  <nts id="Seg_1472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1474" n="HIAT:w" s="T353">igel</ts>
                  <nts id="Seg_1475" n="HIAT:ip">.</nts>
                  <nts id="Seg_1476" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T356" id="Seg_1478" n="HIAT:u" s="T354">
                  <ts e="T355" id="Seg_1480" n="HIAT:w" s="T354">Jakše</ts>
                  <nts id="Seg_1481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1483" n="HIAT:w" s="T355">erte</ts>
                  <nts id="Seg_1484" n="HIAT:ip">!</nts>
                  <nts id="Seg_1485" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T362" id="Seg_1487" n="HIAT:u" s="T356">
                  <ts e="T357" id="Seg_1489" n="HIAT:w" s="T356">Jakše</ts>
                  <nts id="Seg_1490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1492" n="HIAT:w" s="T357">ertezi</ts>
                  <nts id="Seg_1493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1495" n="HIAT:w" s="T358">i</ts>
                  <nts id="Seg_1496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1498" n="HIAT:w" s="T359">веселый</ts>
                  <nts id="Seg_1499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1501" n="HIAT:w" s="T360">dʼala</ts>
                  <nts id="Seg_1502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1504" n="HIAT:w" s="T361">tănan</ts>
                  <nts id="Seg_1505" n="HIAT:ip">.</nts>
                  <nts id="Seg_1506" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T363" id="Seg_1508" n="HIAT:u" s="T362">
                  <nts id="Seg_1509" n="HIAT:ip">(</nts>
                  <nts id="Seg_1510" n="HIAT:ip">(</nts>
                  <ats e="T363" id="Seg_1511" n="HIAT:non-pho" s="T362">BRK</ats>
                  <nts id="Seg_1512" n="HIAT:ip">)</nts>
                  <nts id="Seg_1513" n="HIAT:ip">)</nts>
                  <nts id="Seg_1514" n="HIAT:ip">.</nts>
                  <nts id="Seg_1515" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T366" id="Seg_1517" n="HIAT:u" s="T363">
                  <ts e="T364" id="Seg_1519" n="HIAT:w" s="T363">Kăde</ts>
                  <nts id="Seg_1520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1522" n="HIAT:w" s="T364">kunolbial</ts>
                  <nts id="Seg_1523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1525" n="HIAT:w" s="T365">teinen</ts>
                  <nts id="Seg_1526" n="HIAT:ip">?</nts>
                  <nts id="Seg_1527" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T371" id="Seg_1529" n="HIAT:u" s="T366">
                  <ts e="T367" id="Seg_1531" n="HIAT:w" s="T366">Moltʼagən</ts>
                  <nts id="Seg_1532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1534" n="HIAT:w" s="T367">băzəbial</ts>
                  <nts id="Seg_1535" n="HIAT:ip">,</nts>
                  <nts id="Seg_1536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1538" n="HIAT:w" s="T368">padʼi</ts>
                  <nts id="Seg_1539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1541" n="HIAT:w" s="T369">tăŋ</ts>
                  <nts id="Seg_1542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1544" n="HIAT:w" s="T370">kunolbial</ts>
                  <nts id="Seg_1545" n="HIAT:ip">.</nts>
                  <nts id="Seg_1546" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T376" id="Seg_1548" n="HIAT:u" s="T371">
                  <ts e="T372" id="Seg_1550" n="HIAT:w" s="T371">Da</ts>
                  <nts id="Seg_1551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1552" n="HIAT:ip">(</nts>
                  <ts e="T373" id="Seg_1554" n="HIAT:w" s="T372">kunolbialəj-</ts>
                  <nts id="Seg_1555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1557" n="HIAT:w" s="T373">kulonolbiam-</ts>
                  <nts id="Seg_1558" n="HIAT:ip">)</nts>
                  <nts id="Seg_1559" n="HIAT:ip">,</nts>
                  <nts id="Seg_1560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1562" n="HIAT:w" s="T374">jakše</ts>
                  <nts id="Seg_1563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1565" n="HIAT:w" s="T375">băzəbiam</ts>
                  <nts id="Seg_1566" n="HIAT:ip">.</nts>
                  <nts id="Seg_1567" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T378" id="Seg_1569" n="HIAT:u" s="T376">
                  <ts e="T377" id="Seg_1571" n="HIAT:w" s="T376">Bü</ts>
                  <nts id="Seg_1572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1574" n="HIAT:w" s="T377">kabarbi</ts>
                  <nts id="Seg_1575" n="HIAT:ip">.</nts>
                  <nts id="Seg_1576" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T383" id="Seg_1578" n="HIAT:u" s="T378">
                  <ts e="T379" id="Seg_1580" n="HIAT:w" s="T378">Dʼibige</ts>
                  <nts id="Seg_1581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1583" n="HIAT:w" s="T379">bübə</ts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1586" n="HIAT:w" s="T380">ibi</ts>
                  <nts id="Seg_1587" n="HIAT:ip">,</nts>
                  <nts id="Seg_1588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1590" n="HIAT:w" s="T381">i</ts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1593" n="HIAT:w" s="T382">šišəge</ts>
                  <nts id="Seg_1594" n="HIAT:ip">.</nts>
                  <nts id="Seg_1595" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T388" id="Seg_1597" n="HIAT:u" s="T383">
                  <ts e="T384" id="Seg_1599" n="HIAT:w" s="T383">Ejü</ts>
                  <nts id="Seg_1600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1602" n="HIAT:w" s="T384">mobi</ts>
                  <nts id="Seg_1603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1605" n="HIAT:w" s="T385">ugandə</ts>
                  <nts id="Seg_1606" n="HIAT:ip">,</nts>
                  <nts id="Seg_1607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1608" n="HIAT:ip">(</nts>
                  <ts e="T387" id="Seg_1610" n="HIAT:w" s="T386">tăŋ</ts>
                  <nts id="Seg_1611" n="HIAT:ip">)</nts>
                  <nts id="Seg_1612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1614" n="HIAT:w" s="T387">ejü</ts>
                  <nts id="Seg_1615" n="HIAT:ip">.</nts>
                  <nts id="Seg_1616" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T389" id="Seg_1618" n="HIAT:u" s="T388">
                  <nts id="Seg_1619" n="HIAT:ip">(</nts>
                  <nts id="Seg_1620" n="HIAT:ip">(</nts>
                  <ats e="T389" id="Seg_1621" n="HIAT:non-pho" s="T388">BRK</ats>
                  <nts id="Seg_1622" n="HIAT:ip">)</nts>
                  <nts id="Seg_1623" n="HIAT:ip">)</nts>
                  <nts id="Seg_1624" n="HIAT:ip">.</nts>
                  <nts id="Seg_1625" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T395" id="Seg_1627" n="HIAT:u" s="T389">
                  <ts e="T390" id="Seg_1629" n="HIAT:w" s="T389">Măn</ts>
                  <nts id="Seg_1630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1632" n="HIAT:w" s="T390">moltʼagən</ts>
                  <nts id="Seg_1633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1635" n="HIAT:w" s="T391">băzəbiam</ts>
                  <nts id="Seg_1636" n="HIAT:ip">,</nts>
                  <nts id="Seg_1637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1639" n="HIAT:w" s="T392">bar</ts>
                  <nts id="Seg_1640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1642" n="HIAT:w" s="T393">bü</ts>
                  <nts id="Seg_1643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1645" n="HIAT:w" s="T394">kămnəbiam</ts>
                  <nts id="Seg_1646" n="HIAT:ip">.</nts>
                  <nts id="Seg_1647" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T398" id="Seg_1649" n="HIAT:u" s="T395">
                  <ts e="T396" id="Seg_1651" n="HIAT:w" s="T395">Dĭgəttə</ts>
                  <nts id="Seg_1652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1654" n="HIAT:w" s="T396">šobi</ts>
                  <nts id="Seg_1655" n="HIAT:ip">…</nts>
                  <nts id="Seg_1656" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T404" id="Seg_1658" n="HIAT:u" s="T398">
                  <nts id="Seg_1659" n="HIAT:ip">(</nts>
                  <nts id="Seg_1660" n="HIAT:ip">(</nts>
                  <ats e="T399" id="Seg_1661" n="HIAT:non-pho" s="T398">BRK</ats>
                  <nts id="Seg_1662" n="HIAT:ip">)</nts>
                  <nts id="Seg_1663" n="HIAT:ip">)</nts>
                  <nts id="Seg_1664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1666" n="HIAT:w" s="T399">накрутил</ts>
                  <nts id="Seg_1667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1669" n="HIAT:w" s="T400">там</ts>
                  <nts id="Seg_1670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1672" n="HIAT:w" s="T401">на</ts>
                  <nts id="Seg_1673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1675" n="HIAT:w" s="T402">это</ts>
                  <nts id="Seg_1676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1677" n="HIAT:ip">(</nts>
                  <nts id="Seg_1678" n="HIAT:ip">(</nts>
                  <ats e="T404" id="Seg_1679" n="HIAT:non-pho" s="T403">BRK</ats>
                  <nts id="Seg_1680" n="HIAT:ip">)</nts>
                  <nts id="Seg_1681" n="HIAT:ip">)</nts>
                  <nts id="Seg_1682" n="HIAT:ip">.</nts>
                  <nts id="Seg_1683" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T407" id="Seg_1685" n="HIAT:u" s="T404">
                  <ts e="T405" id="Seg_1687" n="HIAT:w" s="T404">Dĭgəttə</ts>
                  <nts id="Seg_1688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1690" n="HIAT:w" s="T405">iet</ts>
                  <nts id="Seg_1691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1693" n="HIAT:w" s="T406">šobi</ts>
                  <nts id="Seg_1694" n="HIAT:ip">.</nts>
                  <nts id="Seg_1695" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T414" id="Seg_1697" n="HIAT:u" s="T407">
                  <ts e="T408" id="Seg_1699" n="HIAT:w" s="T407">Bü</ts>
                  <nts id="Seg_1700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1702" n="HIAT:w" s="T408">naga</ts>
                  <nts id="Seg_1703" n="HIAT:ip">,</nts>
                  <nts id="Seg_1704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1706" n="HIAT:w" s="T409">a</ts>
                  <nts id="Seg_1707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1709" n="HIAT:w" s="T410">măn</ts>
                  <nts id="Seg_1710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1712" n="HIAT:w" s="T411">detlem</ts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1715" n="HIAT:w" s="T412">da</ts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1718" n="HIAT:w" s="T413">băzəjdlam</ts>
                  <nts id="Seg_1719" n="HIAT:ip">.</nts>
                  <nts id="Seg_1720" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T423" id="Seg_1722" n="HIAT:u" s="T414">
                  <ts e="T415" id="Seg_1724" n="HIAT:w" s="T414">A</ts>
                  <nts id="Seg_1725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1727" n="HIAT:w" s="T415">măn</ts>
                  <nts id="Seg_1728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T986" id="Seg_1730" n="HIAT:w" s="T416">kalla</ts>
                  <nts id="Seg_1731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1733" n="HIAT:w" s="T986">dʼürbim</ts>
                  <nts id="Seg_1734" n="HIAT:ip">,</nts>
                  <nts id="Seg_1735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1737" n="HIAT:w" s="T417">dĭ</ts>
                  <nts id="Seg_1738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1740" n="HIAT:w" s="T418">bar</ts>
                  <nts id="Seg_1741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1743" n="HIAT:w" s="T419">băzəjdəbi</ts>
                  <nts id="Seg_1744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1746" n="HIAT:w" s="T420">i</ts>
                  <nts id="Seg_1747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1749" n="HIAT:w" s="T421">maʔndə</ts>
                  <nts id="Seg_1750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1752" n="HIAT:w" s="T422">šobi</ts>
                  <nts id="Seg_1753" n="HIAT:ip">.</nts>
                  <nts id="Seg_1754" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T424" id="Seg_1756" n="HIAT:u" s="T423">
                  <nts id="Seg_1757" n="HIAT:ip">(</nts>
                  <nts id="Seg_1758" n="HIAT:ip">(</nts>
                  <ats e="T424" id="Seg_1759" n="HIAT:non-pho" s="T423">BRK</ats>
                  <nts id="Seg_1760" n="HIAT:ip">)</nts>
                  <nts id="Seg_1761" n="HIAT:ip">)</nts>
                  <nts id="Seg_1762" n="HIAT:ip">.</nts>
                  <nts id="Seg_1763" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T430" id="Seg_1765" n="HIAT:u" s="T424">
                  <ts e="T425" id="Seg_1767" n="HIAT:w" s="T424">Măn</ts>
                  <nts id="Seg_1768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1769" n="HIAT:ip">(</nts>
                  <ts e="T426" id="Seg_1771" n="HIAT:w" s="T425">sʼera-</ts>
                  <nts id="Seg_1772" n="HIAT:ip">)</nts>
                  <nts id="Seg_1773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1775" n="HIAT:w" s="T426">sʼestram</ts>
                  <nts id="Seg_1776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1778" n="HIAT:w" s="T427">nʼit</ts>
                  <nts id="Seg_1779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1781" n="HIAT:w" s="T428">mĭmbi</ts>
                  <nts id="Seg_1782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1784" n="HIAT:w" s="T429">gorăttə</ts>
                  <nts id="Seg_1785" n="HIAT:ip">.</nts>
                  <nts id="Seg_1786" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T438" id="Seg_1788" n="HIAT:u" s="T430">
                  <ts e="T431" id="Seg_1790" n="HIAT:w" s="T430">Dĭn</ts>
                  <nts id="Seg_1791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1793" n="HIAT:w" s="T431">mašina</ts>
                  <nts id="Seg_1794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1796" n="HIAT:w" s="T432">ibi</ts>
                  <nts id="Seg_1797" n="HIAT:ip">,</nts>
                  <nts id="Seg_1798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1800" n="HIAT:w" s="T433">kujnegəʔi</ts>
                  <nts id="Seg_1801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1802" n="HIAT:ip">(</nts>
                  <ts e="T435" id="Seg_1804" n="HIAT:w" s="T434">băzəsʼtə</ts>
                  <nts id="Seg_1805" n="HIAT:ip">)</nts>
                  <nts id="Seg_1806" n="HIAT:ip">,</nts>
                  <nts id="Seg_1807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1809" n="HIAT:w" s="T435">dĭgəttə</ts>
                  <nts id="Seg_1810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1812" n="HIAT:w" s="T436">deʔpi</ts>
                  <nts id="Seg_1813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1814" n="HIAT:ip">(</nts>
                  <ts e="T438" id="Seg_1816" n="HIAT:w" s="T437">maʔndə</ts>
                  <nts id="Seg_1817" n="HIAT:ip">)</nts>
                  <nts id="Seg_1818" n="HIAT:ip">.</nts>
                  <nts id="Seg_1819" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T445" id="Seg_1821" n="HIAT:u" s="T438">
                  <ts e="T439" id="Seg_1823" n="HIAT:w" s="T438">A</ts>
                  <nts id="Seg_1824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1826" n="HIAT:w" s="T439">teinen</ts>
                  <nts id="Seg_1827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1829" n="HIAT:w" s="T440">kujnegəʔi</ts>
                  <nts id="Seg_1830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1832" n="HIAT:w" s="T441">bar</ts>
                  <nts id="Seg_1833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1835" n="HIAT:w" s="T442">băzəbiʔi</ts>
                  <nts id="Seg_1836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1838" n="HIAT:w" s="T443">i</ts>
                  <nts id="Seg_1839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1841" n="HIAT:w" s="T444">edəbiʔi</ts>
                  <nts id="Seg_1842" n="HIAT:ip">.</nts>
                  <nts id="Seg_1843" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T446" id="Seg_1845" n="HIAT:u" s="T445">
                  <nts id="Seg_1846" n="HIAT:ip">(</nts>
                  <nts id="Seg_1847" n="HIAT:ip">(</nts>
                  <ats e="T446" id="Seg_1848" n="HIAT:non-pho" s="T445">BRK</ats>
                  <nts id="Seg_1849" n="HIAT:ip">)</nts>
                  <nts id="Seg_1850" n="HIAT:ip">)</nts>
                  <nts id="Seg_1851" n="HIAT:ip">.</nts>
                  <nts id="Seg_1852" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T451" id="Seg_1854" n="HIAT:u" s="T446">
                  <ts e="T447" id="Seg_1856" n="HIAT:w" s="T446">Măn</ts>
                  <nts id="Seg_1857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1859" n="HIAT:w" s="T447">sʼestranə</ts>
                  <nts id="Seg_1860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1862" n="HIAT:w" s="T448">kazak</ts>
                  <nts id="Seg_1863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1865" n="HIAT:w" s="T449">iat</ts>
                  <nts id="Seg_1866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1868" n="HIAT:w" s="T450">šobi</ts>
                  <nts id="Seg_1869" n="HIAT:ip">.</nts>
                  <nts id="Seg_1870" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T456" id="Seg_1872" n="HIAT:u" s="T451">
                  <ts e="T452" id="Seg_1874" n="HIAT:w" s="T451">Ajaʔ</ts>
                  <nts id="Seg_1875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1877" n="HIAT:w" s="T452">deʔpi</ts>
                  <nts id="Seg_1878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1879" n="HIAT:ip">(</nts>
                  <ts e="T454" id="Seg_1881" n="HIAT:w" s="T453">tibi</ts>
                  <nts id="Seg_1882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1884" n="HIAT:w" s="T454">i</ts>
                  <nts id="Seg_1885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1887" n="HIAT:w" s="T455">ne</ts>
                  <nts id="Seg_1888" n="HIAT:ip">)</nts>
                  <nts id="Seg_1889" n="HIAT:ip">.</nts>
                  <nts id="Seg_1890" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T466" id="Seg_1892" n="HIAT:u" s="T456">
                  <ts e="T457" id="Seg_1894" n="HIAT:w" s="T456">Dĭgəttə</ts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1896" n="HIAT:ip">(</nts>
                  <ts e="T458" id="Seg_1898" n="HIAT:w" s="T457">ia=</ts>
                  <nts id="Seg_1899" n="HIAT:ip">)</nts>
                  <nts id="Seg_1900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1902" n="HIAT:w" s="T458">măn</ts>
                  <nts id="Seg_1903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1905" n="HIAT:w" s="T459">ianə</ts>
                  <nts id="Seg_1906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1908" n="HIAT:w" s="T460">măndə:</ts>
                  <nts id="Seg_1909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1911" n="HIAT:w" s="T461">Kangaʔ</ts>
                  <nts id="Seg_1912" n="HIAT:ip">,</nts>
                  <nts id="Seg_1913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1915" n="HIAT:w" s="T462">keʔbde</ts>
                  <nts id="Seg_1916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1918" n="HIAT:w" s="T463">ileʔ</ts>
                  <nts id="Seg_1919" n="HIAT:ip">,</nts>
                  <nts id="Seg_1920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1922" n="HIAT:w" s="T464">малина</ts>
                  <nts id="Seg_1923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1925" n="HIAT:w" s="T465">ileʔ</ts>
                  <nts id="Seg_1926" n="HIAT:ip">.</nts>
                  <nts id="Seg_1927" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T468" id="Seg_1929" n="HIAT:u" s="T466">
                  <ts e="T467" id="Seg_1931" n="HIAT:w" s="T466">Dĭzeŋ</ts>
                  <nts id="Seg_1932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1934" n="HIAT:w" s="T467">kambiʔi</ts>
                  <nts id="Seg_1935" n="HIAT:ip">.</nts>
                  <nts id="Seg_1936" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T475" id="Seg_1938" n="HIAT:u" s="T468">
                  <ts e="T469" id="Seg_1940" n="HIAT:w" s="T468">Dĭ</ts>
                  <nts id="Seg_1941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1943" n="HIAT:w" s="T469">ara</ts>
                  <nts id="Seg_1944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1946" n="HIAT:w" s="T470">mĭbi</ts>
                  <nts id="Seg_1947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1948" n="HIAT:ip">(</nts>
                  <ts e="T472" id="Seg_1950" n="HIAT:w" s="T471">onʼiʔ=</ts>
                  <nts id="Seg_1951" n="HIAT:ip">)</nts>
                  <nts id="Seg_1952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1954" n="HIAT:w" s="T472">onʼiʔ</ts>
                  <nts id="Seg_1955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1957" n="HIAT:w" s="T473">бутылка</ts>
                  <nts id="Seg_1958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1960" n="HIAT:w" s="T474">dĭzeŋ</ts>
                  <nts id="Seg_1961" n="HIAT:ip">.</nts>
                  <nts id="Seg_1962" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T479" id="Seg_1964" n="HIAT:u" s="T475">
                  <ts e="T476" id="Seg_1966" n="HIAT:w" s="T475">Kuʔpi</ts>
                  <nts id="Seg_1967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1968" n="HIAT:ip">(</nts>
                  <ts e="T477" id="Seg_1970" n="HIAT:w" s="T476">ta-</ts>
                  <nts id="Seg_1971" n="HIAT:ip">)</nts>
                  <nts id="Seg_1972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1974" n="HIAT:w" s="T477">dĭn</ts>
                  <nts id="Seg_1975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1977" n="HIAT:w" s="T478">keʔbde</ts>
                  <nts id="Seg_1978" n="HIAT:ip">.</nts>
                  <nts id="Seg_1979" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T485" id="Seg_1981" n="HIAT:u" s="T479">
                  <ts e="T480" id="Seg_1983" n="HIAT:w" s="T479">Dĭzeŋ</ts>
                  <nts id="Seg_1984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1985" n="HIAT:ip">(</nts>
                  <ts e="T481" id="Seg_1987" n="HIAT:w" s="T480">nam-</ts>
                  <nts id="Seg_1988" n="HIAT:ip">)</nts>
                  <nts id="Seg_1989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1991" n="HIAT:w" s="T481">ibiʔi</ts>
                  <nts id="Seg_1992" n="HIAT:ip">,</nts>
                  <nts id="Seg_1993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1995" n="HIAT:w" s="T482">ibiʔi</ts>
                  <nts id="Seg_1996" n="HIAT:ip">,</nts>
                  <nts id="Seg_1997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1999" n="HIAT:w" s="T483">kon</ts>
                  <nts id="Seg_2000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_2002" n="HIAT:w" s="T484">ibiʔi</ts>
                  <nts id="Seg_2003" n="HIAT:ip">.</nts>
                  <nts id="Seg_2004" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T488" id="Seg_2006" n="HIAT:u" s="T485">
                  <ts e="T486" id="Seg_2008" n="HIAT:w" s="T485">Dĭgəttə</ts>
                  <nts id="Seg_2009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_2011" n="HIAT:w" s="T486">amnobiʔi</ts>
                  <nts id="Seg_2012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_2014" n="HIAT:w" s="T487">amorzittə</ts>
                  <nts id="Seg_2015" n="HIAT:ip">.</nts>
                  <nts id="Seg_2016" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T496" id="Seg_2018" n="HIAT:u" s="T488">
                  <ts e="T489" id="Seg_2020" n="HIAT:w" s="T488">A</ts>
                  <nts id="Seg_2021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_2023" n="HIAT:w" s="T489">măn</ts>
                  <nts id="Seg_2024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_2026" n="HIAT:w" s="T490">iam</ts>
                  <nts id="Seg_2027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2028" n="HIAT:ip">(</nts>
                  <ts e="T492" id="Seg_2030" n="HIAT:w" s="T491">bə-</ts>
                  <nts id="Seg_2031" n="HIAT:ip">)</nts>
                  <nts id="Seg_2032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_2034" n="HIAT:w" s="T492">măndə:</ts>
                  <nts id="Seg_2035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_2037" n="HIAT:w" s="T493">Davaj</ts>
                  <nts id="Seg_2038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_2040" n="HIAT:w" s="T494">döʔə</ts>
                  <nts id="Seg_2041" n="HIAT:ip">…</nts>
                  <nts id="Seg_2042" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T501" id="Seg_2044" n="HIAT:u" s="T496">
                  <ts e="T497" id="Seg_2046" n="HIAT:w" s="T496">Nada</ts>
                  <nts id="Seg_2047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_2049" n="HIAT:w" s="T497">keʔbde</ts>
                  <nts id="Seg_2050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2051" n="HIAT:ip">(</nts>
                  <ts e="T499" id="Seg_2053" n="HIAT:w" s="T498">koʔsittə</ts>
                  <nts id="Seg_2054" n="HIAT:ip">)</nts>
                  <nts id="Seg_2055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_2057" n="HIAT:w" s="T499">dĭ</ts>
                  <nts id="Seg_2058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_2060" n="HIAT:w" s="T500">arazi</ts>
                  <nts id="Seg_2061" n="HIAT:ip">.</nts>
                  <nts id="Seg_2062" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T505" id="Seg_2064" n="HIAT:u" s="T501">
                  <ts e="T502" id="Seg_2066" n="HIAT:w" s="T501">Dĭgəttə</ts>
                  <nts id="Seg_2067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_2069" n="HIAT:w" s="T502">kămnəbiʔi</ts>
                  <nts id="Seg_2070" n="HIAT:ip">,</nts>
                  <nts id="Seg_2071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_2073" n="HIAT:w" s="T503">bostə</ts>
                  <nts id="Seg_2074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_2076" n="HIAT:w" s="T504">bĭʔpiʔi</ts>
                  <nts id="Seg_2077" n="HIAT:ip">.</nts>
                  <nts id="Seg_2078" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T510" id="Seg_2080" n="HIAT:u" s="T505">
                  <ts e="T506" id="Seg_2082" n="HIAT:w" s="T505">Dĭgəttə</ts>
                  <nts id="Seg_2083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_2085" n="HIAT:w" s="T506">šo</ts>
                  <nts id="Seg_2086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_2088" n="HIAT:w" s="T507">oʔbdəbiʔi</ts>
                  <nts id="Seg_2089" n="HIAT:ip">,</nts>
                  <nts id="Seg_2090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_2092" n="HIAT:w" s="T508">iʔgö</ts>
                  <nts id="Seg_2093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_2095" n="HIAT:w" s="T509">deʔpiʔi</ts>
                  <nts id="Seg_2096" n="HIAT:ip">.</nts>
                  <nts id="Seg_2097" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T513" id="Seg_2099" n="HIAT:u" s="T510">
                  <ts e="T511" id="Seg_2101" n="HIAT:w" s="T510">Šobiʔi</ts>
                  <nts id="Seg_2102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_2104" n="HIAT:w" s="T511">maʔndə</ts>
                  <nts id="Seg_2105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_2107" n="HIAT:w" s="T512">dĭbər</ts>
                  <nts id="Seg_2108" n="HIAT:ip">.</nts>
                  <nts id="Seg_2109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T519" id="Seg_2111" n="HIAT:u" s="T513">
                  <ts e="T514" id="Seg_2113" n="HIAT:w" s="T513">Döm</ts>
                  <nts id="Seg_2114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_2116" n="HIAT:w" s="T514">nʼilgölia</ts>
                  <nts id="Seg_2117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_2119" n="HIAT:w" s="T515">püjetsi</ts>
                  <nts id="Seg_2120" n="HIAT:ip">,</nts>
                  <nts id="Seg_2121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_2123" n="HIAT:w" s="T516">döm</ts>
                  <nts id="Seg_2124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_2126" n="HIAT:w" s="T517">nʼilgölia</ts>
                  <nts id="Seg_2127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_2129" n="HIAT:w" s="T518">püjetsi</ts>
                  <nts id="Seg_2130" n="HIAT:ip">.</nts>
                  <nts id="Seg_2131" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T521" id="Seg_2133" n="HIAT:u" s="T519">
                  <ts e="T520" id="Seg_2135" n="HIAT:w" s="T519">Putʼəmnia</ts>
                  <nts id="Seg_2136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_2138" n="HIAT:w" s="T520">arazi</ts>
                  <nts id="Seg_2139" n="HIAT:ip">!</nts>
                  <nts id="Seg_2140" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T523" id="Seg_2142" n="HIAT:u" s="T521">
                  <ts e="T522" id="Seg_2144" n="HIAT:w" s="T521">Ej</ts>
                  <nts id="Seg_2145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_2147" n="HIAT:w" s="T522">tĭmnebi</ts>
                  <nts id="Seg_2148" n="HIAT:ip">.</nts>
                  <nts id="Seg_2149" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T524" id="Seg_2151" n="HIAT:u" s="T523">
                  <nts id="Seg_2152" n="HIAT:ip">(</nts>
                  <nts id="Seg_2153" n="HIAT:ip">(</nts>
                  <ats e="T524" id="Seg_2154" n="HIAT:non-pho" s="T523">BRK</ats>
                  <nts id="Seg_2155" n="HIAT:ip">)</nts>
                  <nts id="Seg_2156" n="HIAT:ip">)</nts>
                  <nts id="Seg_2157" n="HIAT:ip">.</nts>
                  <nts id="Seg_2158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T529" id="Seg_2160" n="HIAT:u" s="T524">
                  <ts e="T525" id="Seg_2162" n="HIAT:w" s="T524">Măn</ts>
                  <nts id="Seg_2163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2164" n="HIAT:ip">(</nts>
                  <ts e="T526" id="Seg_2166" n="HIAT:w" s="T525">təni-</ts>
                  <nts id="Seg_2167" n="HIAT:ip">)</nts>
                  <nts id="Seg_2168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_2170" n="HIAT:w" s="T526">teinen</ts>
                  <nts id="Seg_2171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_2173" n="HIAT:w" s="T527">ertə</ts>
                  <nts id="Seg_2174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_2176" n="HIAT:w" s="T528">uʔbdəbiam</ts>
                  <nts id="Seg_2177" n="HIAT:ip">.</nts>
                  <nts id="Seg_2178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T531" id="Seg_2180" n="HIAT:u" s="T529">
                  <ts e="T530" id="Seg_2182" n="HIAT:w" s="T529">Pʼeš</ts>
                  <nts id="Seg_2183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_2185" n="HIAT:w" s="T530">nendəbiem</ts>
                  <nts id="Seg_2186" n="HIAT:ip">.</nts>
                  <nts id="Seg_2187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T535" id="Seg_2189" n="HIAT:u" s="T531">
                  <ts e="T532" id="Seg_2191" n="HIAT:w" s="T531">Dĭgəttə</ts>
                  <nts id="Seg_2192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_2194" n="HIAT:w" s="T532">tüžöjdə</ts>
                  <nts id="Seg_2195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_2197" n="HIAT:w" s="T533">noʔ</ts>
                  <nts id="Seg_2198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_2200" n="HIAT:w" s="T534">mĭbiam</ts>
                  <nts id="Seg_2201" n="HIAT:ip">.</nts>
                  <nts id="Seg_2202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T537" id="Seg_2204" n="HIAT:u" s="T535">
                  <ts e="T536" id="Seg_2206" n="HIAT:w" s="T535">Toltanoʔ</ts>
                  <nts id="Seg_2207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_2209" n="HIAT:w" s="T536">bĭʔpiam</ts>
                  <nts id="Seg_2210" n="HIAT:ip">.</nts>
                  <nts id="Seg_2211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T540" id="Seg_2213" n="HIAT:u" s="T537">
                  <ts e="T538" id="Seg_2215" n="HIAT:w" s="T537">Kambiam</ts>
                  <nts id="Seg_2216" n="HIAT:ip">,</nts>
                  <nts id="Seg_2217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_2219" n="HIAT:w" s="T538">tüžöjdə</ts>
                  <nts id="Seg_2220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_2222" n="HIAT:w" s="T539">surdəbiam</ts>
                  <nts id="Seg_2223" n="HIAT:ip">.</nts>
                  <nts id="Seg_2224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T542" id="Seg_2226" n="HIAT:u" s="T540">
                  <ts e="T541" id="Seg_2228" n="HIAT:w" s="T540">Süttə</ts>
                  <nts id="Seg_2229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_2231" n="HIAT:w" s="T541">kămnəbiam</ts>
                  <nts id="Seg_2232" n="HIAT:ip">.</nts>
                  <nts id="Seg_2233" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T549" id="Seg_2235" n="HIAT:u" s="T542">
                  <ts e="T543" id="Seg_2237" n="HIAT:w" s="T542">A</ts>
                  <nts id="Seg_2238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_2240" n="HIAT:w" s="T543">dĭgəttə</ts>
                  <nts id="Seg_2241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_2243" n="HIAT:w" s="T544">kambiam</ts>
                  <nts id="Seg_2244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_2246" n="HIAT:w" s="T545">nükenə</ts>
                  <nts id="Seg_2247" n="HIAT:ip">,</nts>
                  <nts id="Seg_2248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_2250" n="HIAT:w" s="T546">mĭj</ts>
                  <nts id="Seg_2251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2252" n="HIAT:ip">(</nts>
                  <ts e="T549" id="Seg_2254" n="HIAT:w" s="T547">ka-</ts>
                  <nts id="Seg_2255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2256" n="HIAT:ip">)</nts>
                  <nts id="Seg_2257" n="HIAT:ip">…</nts>
                  <nts id="Seg_2258" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T551" id="Seg_2260" n="HIAT:u" s="T549">
                  <ts e="T550" id="Seg_2262" n="HIAT:w" s="T549">Mĭj</ts>
                  <nts id="Seg_2263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_2265" n="HIAT:w" s="T550">mĭnzerbiem</ts>
                  <nts id="Seg_2266" n="HIAT:ip">.</nts>
                  <nts id="Seg_2267" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T557" id="Seg_2269" n="HIAT:u" s="T551">
                  <ts e="T552" id="Seg_2271" n="HIAT:w" s="T551">Dĭgəttə</ts>
                  <nts id="Seg_2272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2274" n="HIAT:w" s="T552">kambiam</ts>
                  <nts id="Seg_2275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2277" n="HIAT:w" s="T553">nükenə</ts>
                  <nts id="Seg_2278" n="HIAT:ip">,</nts>
                  <nts id="Seg_2279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2280" n="HIAT:ip">(</nts>
                  <ts e="T555" id="Seg_2282" n="HIAT:w" s="T554">mĭj=</ts>
                  <nts id="Seg_2283" n="HIAT:ip">)</nts>
                  <nts id="Seg_2284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2286" n="HIAT:w" s="T555">mĭj</ts>
                  <nts id="Seg_2287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_2289" n="HIAT:w" s="T556">deʔpiem</ts>
                  <nts id="Seg_2290" n="HIAT:ip">.</nts>
                  <nts id="Seg_2291" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T562" id="Seg_2293" n="HIAT:u" s="T557">
                  <ts e="T558" id="Seg_2295" n="HIAT:w" s="T557">Dĭ</ts>
                  <nts id="Seg_2296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2298" n="HIAT:w" s="T558">bar</ts>
                  <nts id="Seg_2299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2300" n="HIAT:ip">(</nts>
                  <ts e="T560" id="Seg_2302" n="HIAT:w" s="T559">mĭn-</ts>
                  <nts id="Seg_2303" n="HIAT:ip">)</nts>
                  <nts id="Seg_2304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2306" n="HIAT:w" s="T560">mĭlleʔbə</ts>
                  <nts id="Seg_2307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2308" n="HIAT:ip">(</nts>
                  <ts e="T562" id="Seg_2310" n="HIAT:w" s="T561">üjügən</ts>
                  <nts id="Seg_2311" n="HIAT:ip">)</nts>
                  <nts id="Seg_2312" n="HIAT:ip">.</nts>
                  <nts id="Seg_2313" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T565" id="Seg_2315" n="HIAT:u" s="T562">
                  <ts e="T563" id="Seg_2317" n="HIAT:w" s="T562">Ĭmbi</ts>
                  <nts id="Seg_2318" n="HIAT:ip">,</nts>
                  <nts id="Seg_2319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2321" n="HIAT:w" s="T563">tural</ts>
                  <nts id="Seg_2322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2324" n="HIAT:w" s="T564">băzəbi</ts>
                  <nts id="Seg_2325" n="HIAT:ip">?</nts>
                  <nts id="Seg_2326" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T568" id="Seg_2328" n="HIAT:u" s="T565">
                  <ts e="T566" id="Seg_2330" n="HIAT:w" s="T565">Dʼok</ts>
                  <nts id="Seg_2331" n="HIAT:ip">,</nts>
                  <nts id="Seg_2332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2334" n="HIAT:w" s="T566">esseŋ</ts>
                  <nts id="Seg_2335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2337" n="HIAT:w" s="T567">băzəbiʔi</ts>
                  <nts id="Seg_2338" n="HIAT:ip">.</nts>
                  <nts id="Seg_2339" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T574" id="Seg_2341" n="HIAT:u" s="T568">
                  <ts e="T569" id="Seg_2343" n="HIAT:w" s="T568">Dĭgəttə</ts>
                  <nts id="Seg_2344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2346" n="HIAT:w" s="T569">šobi</ts>
                  <nts id="Seg_2347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2349" n="HIAT:w" s="T570">döbər</ts>
                  <nts id="Seg_2350" n="HIAT:ip">,</nts>
                  <nts id="Seg_2351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2353" n="HIAT:w" s="T571">krăvatʼtʼə</ts>
                  <nts id="Seg_2354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_2356" n="HIAT:w" s="T572">bar</ts>
                  <nts id="Seg_2357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2358" n="HIAT:ip">(</nts>
                  <ts e="T574" id="Seg_2360" n="HIAT:w" s="T573">saʔməluʔpi</ts>
                  <nts id="Seg_2361" n="HIAT:ip">)</nts>
                  <nts id="Seg_2362" n="HIAT:ip">.</nts>
                  <nts id="Seg_2363" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T578" id="Seg_2365" n="HIAT:u" s="T574">
                  <ts e="T575" id="Seg_2367" n="HIAT:w" s="T574">Măn</ts>
                  <nts id="Seg_2368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2370" n="HIAT:w" s="T575">măndəm:</ts>
                  <nts id="Seg_2371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2373" n="HIAT:w" s="T576">Iʔ</ts>
                  <nts id="Seg_2374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2376" n="HIAT:w" s="T577">saʔmaʔ</ts>
                  <nts id="Seg_2377" n="HIAT:ip">.</nts>
                  <nts id="Seg_2378" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T580" id="Seg_2380" n="HIAT:u" s="T578">
                  <ts e="T580" id="Seg_2382" n="HIAT:w" s="T578">Погоди</ts>
                  <nts id="Seg_2383" n="HIAT:ip">…</nts>
                  <nts id="Seg_2384" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T584" id="Seg_2386" n="HIAT:u" s="T580">
                  <ts e="T581" id="Seg_2388" n="HIAT:w" s="T580">A</ts>
                  <nts id="Seg_2389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2391" n="HIAT:w" s="T581">mĭjdə</ts>
                  <nts id="Seg_2392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2394" n="HIAT:w" s="T582">bar</ts>
                  <nts id="Seg_2395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2396" n="HIAT:ip">(</nts>
                  <ts e="T584" id="Seg_2398" n="HIAT:w" s="T583">săgəluʔpi</ts>
                  <nts id="Seg_2399" n="HIAT:ip">)</nts>
                  <nts id="Seg_2400" n="HIAT:ip">.</nts>
                  <nts id="Seg_2401" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T587" id="Seg_2403" n="HIAT:u" s="T584">
                  <ts e="T585" id="Seg_2405" n="HIAT:w" s="T584">Büžü</ts>
                  <nts id="Seg_2406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2408" n="HIAT:w" s="T585">ej</ts>
                  <nts id="Seg_2409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2411" n="HIAT:w" s="T586">kuləj</ts>
                  <nts id="Seg_2412" n="HIAT:ip">.</nts>
                  <nts id="Seg_2413" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T588" id="Seg_2415" n="HIAT:u" s="T587">
                  <nts id="Seg_2416" n="HIAT:ip">(</nts>
                  <nts id="Seg_2417" n="HIAT:ip">(</nts>
                  <ats e="T588" id="Seg_2418" n="HIAT:non-pho" s="T587">BRK</ats>
                  <nts id="Seg_2419" n="HIAT:ip">)</nts>
                  <nts id="Seg_2420" n="HIAT:ip">)</nts>
                  <nts id="Seg_2421" n="HIAT:ip">.</nts>
                  <nts id="Seg_2422" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T594" id="Seg_2424" n="HIAT:u" s="T588">
                  <ts e="T589" id="Seg_2426" n="HIAT:w" s="T588">Dĭgəttə</ts>
                  <nts id="Seg_2427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2429" n="HIAT:w" s="T589">dĭ</ts>
                  <nts id="Seg_2430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2432" n="HIAT:w" s="T590">nükebə</ts>
                  <nts id="Seg_2433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2434" n="HIAT:ip">(</nts>
                  <ts e="T592" id="Seg_2436" n="HIAT:w" s="T591">nuʔgĭ-</ts>
                  <nts id="Seg_2437" n="HIAT:ip">)</nts>
                  <nts id="Seg_2438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2440" n="HIAT:w" s="T592">kambiam</ts>
                  <nts id="Seg_2441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2443" n="HIAT:w" s="T593">Anissʼanə</ts>
                  <nts id="Seg_2444" n="HIAT:ip">.</nts>
                  <nts id="Seg_2445" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T597" id="Seg_2447" n="HIAT:u" s="T594">
                  <ts e="T595" id="Seg_2449" n="HIAT:w" s="T594">Kundʼo</ts>
                  <nts id="Seg_2450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2452" n="HIAT:w" s="T595">ej</ts>
                  <nts id="Seg_2453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2455" n="HIAT:w" s="T596">mĭmbiem</ts>
                  <nts id="Seg_2456" n="HIAT:ip">.</nts>
                  <nts id="Seg_2457" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T600" id="Seg_2459" n="HIAT:u" s="T597">
                  <ts e="T598" id="Seg_2461" n="HIAT:w" s="T597">Šobiam</ts>
                  <nts id="Seg_2462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2463" n="HIAT:ip">(</nts>
                  <ts e="T599" id="Seg_2465" n="HIAT:w" s="T598">dibər=</ts>
                  <nts id="Seg_2466" n="HIAT:ip">)</nts>
                  <nts id="Seg_2467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2469" n="HIAT:w" s="T599">dĭʔnə</ts>
                  <nts id="Seg_2470" n="HIAT:ip">.</nts>
                  <nts id="Seg_2471" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T605" id="Seg_2473" n="HIAT:u" s="T600">
                  <ts e="T601" id="Seg_2475" n="HIAT:w" s="T600">Dĭzeŋ</ts>
                  <nts id="Seg_2476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2478" n="HIAT:w" s="T601">bar</ts>
                  <nts id="Seg_2479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2481" n="HIAT:w" s="T602">dibər</ts>
                  <nts id="Seg_2482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2484" n="HIAT:w" s="T603">nʼitsi</ts>
                  <nts id="Seg_2485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2487" n="HIAT:w" s="T604">kudonzlaʔbə</ts>
                  <nts id="Seg_2488" n="HIAT:ip">.</nts>
                  <nts id="Seg_2489" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T606" id="Seg_2491" n="HIAT:u" s="T605">
                  <nts id="Seg_2492" n="HIAT:ip">(</nts>
                  <nts id="Seg_2493" n="HIAT:ip">(</nts>
                  <ats e="T606" id="Seg_2494" n="HIAT:non-pho" s="T605">BRK</ats>
                  <nts id="Seg_2495" n="HIAT:ip">)</nts>
                  <nts id="Seg_2496" n="HIAT:ip">)</nts>
                  <nts id="Seg_2497" n="HIAT:ip">.</nts>
                  <nts id="Seg_2498" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T613" id="Seg_2500" n="HIAT:u" s="T606">
                  <ts e="T607" id="Seg_2502" n="HIAT:w" s="T606">Dĭgəttə</ts>
                  <nts id="Seg_2503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2505" n="HIAT:w" s="T607">dĭ</ts>
                  <nts id="Seg_2506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2508" n="HIAT:w" s="T608">Anissʼa</ts>
                  <nts id="Seg_2509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2511" n="HIAT:w" s="T609">bar</ts>
                  <nts id="Seg_2512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2514" n="HIAT:w" s="T610">kudonzəbi</ts>
                  <nts id="Seg_2515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2516" n="HIAT:ip">(</nts>
                  <ts e="T612" id="Seg_2518" n="HIAT:w" s="T611">n-</ts>
                  <nts id="Seg_2519" n="HIAT:ip">)</nts>
                  <nts id="Seg_2520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2522" n="HIAT:w" s="T612">nʼit</ts>
                  <nts id="Seg_2523" n="HIAT:ip">.</nts>
                  <nts id="Seg_2524" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T619" id="Seg_2526" n="HIAT:u" s="T613">
                  <ts e="T614" id="Seg_2528" n="HIAT:w" s="T613">A</ts>
                  <nts id="Seg_2529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2531" n="HIAT:w" s="T614">nʼit</ts>
                  <nts id="Seg_2532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2534" n="HIAT:w" s="T615">bar</ts>
                  <nts id="Seg_2535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2537" n="HIAT:w" s="T616">dĭʔnə</ts>
                  <nts id="Seg_2538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2540" n="HIAT:w" s="T617">ĭmbi-nʼibudʼ</ts>
                  <nts id="Seg_2541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2543" n="HIAT:w" s="T618">dʼăbaktərləj</ts>
                  <nts id="Seg_2544" n="HIAT:ip">.</nts>
                  <nts id="Seg_2545" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T623" id="Seg_2547" n="HIAT:u" s="T619">
                  <ts e="T620" id="Seg_2549" n="HIAT:w" s="T619">Măn</ts>
                  <nts id="Seg_2550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2552" n="HIAT:w" s="T620">măndəm:</ts>
                  <nts id="Seg_2553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2555" n="HIAT:w" s="T621">Iʔ</ts>
                  <nts id="Seg_2556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2558" n="HIAT:w" s="T622">kürümaʔ</ts>
                  <nts id="Seg_2559" n="HIAT:ip">.</nts>
                  <nts id="Seg_2560" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T628" id="Seg_2562" n="HIAT:u" s="T623">
                  <nts id="Seg_2563" n="HIAT:ip">(</nts>
                  <ts e="T624" id="Seg_2565" n="HIAT:w" s="T623">Ia-</ts>
                  <nts id="Seg_2566" n="HIAT:ip">)</nts>
                  <nts id="Seg_2567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2569" n="HIAT:w" s="T624">Ian</ts>
                  <nts id="Seg_2570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2572" n="HIAT:w" s="T625">ĭmbidə</ts>
                  <nts id="Seg_2573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2575" n="HIAT:w" s="T626">ej</ts>
                  <nts id="Seg_2576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2577" n="HIAT:ip">(</nts>
                  <ts e="T628" id="Seg_2579" n="HIAT:w" s="T627">mănaʔ</ts>
                  <nts id="Seg_2580" n="HIAT:ip">)</nts>
                  <nts id="Seg_2581" n="HIAT:ip">.</nts>
                  <nts id="Seg_2582" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T632" id="Seg_2584" n="HIAT:u" s="T628">
                  <ts e="T629" id="Seg_2586" n="HIAT:w" s="T628">A</ts>
                  <nts id="Seg_2587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2589" n="HIAT:w" s="T629">dĭ</ts>
                  <nts id="Seg_2590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2592" n="HIAT:w" s="T630">üge</ts>
                  <nts id="Seg_2593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2595" n="HIAT:w" s="T631">kürümnie</ts>
                  <nts id="Seg_2596" n="HIAT:ip">.</nts>
                  <nts id="Seg_2597" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T637" id="Seg_2599" n="HIAT:u" s="T632">
                  <ts e="T633" id="Seg_2601" n="HIAT:w" s="T632">A</ts>
                  <nts id="Seg_2602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2604" n="HIAT:w" s="T633">iat</ts>
                  <nts id="Seg_2605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2607" n="HIAT:w" s="T634">bar</ts>
                  <nts id="Seg_2608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2610" n="HIAT:w" s="T635">davaj</ts>
                  <nts id="Seg_2611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2613" n="HIAT:w" s="T636">dʼorzittə</ts>
                  <nts id="Seg_2614" n="HIAT:ip">.</nts>
                  <nts id="Seg_2615" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T643" id="Seg_2617" n="HIAT:u" s="T637">
                  <ts e="T638" id="Seg_2619" n="HIAT:w" s="T637">Kuiol</ts>
                  <nts id="Seg_2620" n="HIAT:ip">,</nts>
                  <nts id="Seg_2621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2623" n="HIAT:w" s="T638">kăde</ts>
                  <nts id="Seg_2624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2626" n="HIAT:w" s="T639">dĭzeŋ</ts>
                  <nts id="Seg_2627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2628" n="HIAT:ip">(</nts>
                  <ts e="T641" id="Seg_2630" n="HIAT:w" s="T640">ipeksi</ts>
                  <nts id="Seg_2631" n="HIAT:ip">)</nts>
                  <nts id="Seg_2632" n="HIAT:ip">,</nts>
                  <nts id="Seg_2633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2635" n="HIAT:w" s="T641">măna</ts>
                  <nts id="Seg_2636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2637" n="HIAT:ip">(</nts>
                  <ts e="T643" id="Seg_2639" n="HIAT:w" s="T642">bădləʔi</ts>
                  <nts id="Seg_2640" n="HIAT:ip">)</nts>
                  <nts id="Seg_2641" n="HIAT:ip">.</nts>
                  <nts id="Seg_2642" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T649" id="Seg_2644" n="HIAT:u" s="T643">
                  <ts e="T644" id="Seg_2646" n="HIAT:w" s="T643">Măn</ts>
                  <nts id="Seg_2647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2649" n="HIAT:w" s="T644">ipek</ts>
                  <nts id="Seg_2650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2652" n="HIAT:w" s="T645">amnia</ts>
                  <nts id="Seg_2653" n="HIAT:ip">,</nts>
                  <nts id="Seg_2654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2656" n="HIAT:w" s="T646">i</ts>
                  <nts id="Seg_2657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2659" n="HIAT:w" s="T647">išo</ts>
                  <nts id="Seg_2660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2662" n="HIAT:w" s="T648">kudonzlaʔbə</ts>
                  <nts id="Seg_2663" n="HIAT:ip">.</nts>
                  <nts id="Seg_2664" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T656" id="Seg_2666" n="HIAT:u" s="T649">
                  <ts e="T650" id="Seg_2668" n="HIAT:w" s="T649">Dĭgəttə</ts>
                  <nts id="Seg_2669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2670" n="HIAT:ip">(</nts>
                  <ts e="T651" id="Seg_2672" n="HIAT:w" s="T650">šalaʔ-</ts>
                  <nts id="Seg_2673" n="HIAT:ip">)</nts>
                  <nts id="Seg_2674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2676" n="HIAT:w" s="T651">šala</ts>
                  <nts id="Seg_2677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2679" n="HIAT:w" s="T652">amnobibaʔ</ts>
                  <nts id="Seg_2680" n="HIAT:ip">,</nts>
                  <nts id="Seg_2681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2683" n="HIAT:w" s="T653">dĭgəttə</ts>
                  <nts id="Seg_2684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2686" n="HIAT:w" s="T654">Jelʼa</ts>
                  <nts id="Seg_2687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2689" n="HIAT:w" s="T655">šobi</ts>
                  <nts id="Seg_2690" n="HIAT:ip">.</nts>
                  <nts id="Seg_2691" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T660" id="Seg_2693" n="HIAT:u" s="T656">
                  <ts e="T657" id="Seg_2695" n="HIAT:w" s="T656">Zdărowă</ts>
                  <nts id="Seg_2696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2698" n="HIAT:w" s="T657">igel</ts>
                  <nts id="Seg_2699" n="HIAT:ip">—</nts>
                  <nts id="Seg_2700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2702" n="HIAT:w" s="T658">Zdărowă</ts>
                  <nts id="Seg_2703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2705" n="HIAT:w" s="T659">igel</ts>
                  <nts id="Seg_2706" n="HIAT:ip">.</nts>
                  <nts id="Seg_2707" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T662" id="Seg_2709" n="HIAT:u" s="T660">
                  <ts e="T661" id="Seg_2711" n="HIAT:w" s="T660">Ĭmbi</ts>
                  <nts id="Seg_2712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2714" n="HIAT:w" s="T661">šobial</ts>
                  <nts id="Seg_2715" n="HIAT:ip">?</nts>
                  <nts id="Seg_2716" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T664" id="Seg_2718" n="HIAT:u" s="T662">
                  <ts e="T663" id="Seg_2720" n="HIAT:w" s="T662">Ipek</ts>
                  <nts id="Seg_2721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2723" n="HIAT:w" s="T663">măndərbiam</ts>
                  <nts id="Seg_2724" n="HIAT:ip">.</nts>
                  <nts id="Seg_2725" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T672" id="Seg_2727" n="HIAT:u" s="T664">
                  <ts e="T665" id="Seg_2729" n="HIAT:w" s="T664">A</ts>
                  <nts id="Seg_2730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2732" n="HIAT:w" s="T665">măn</ts>
                  <nts id="Seg_2733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2735" n="HIAT:w" s="T666">Anissʼanə</ts>
                  <nts id="Seg_2736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2738" n="HIAT:w" s="T667">mălliam:</ts>
                  <nts id="Seg_2739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2741" n="HIAT:w" s="T668">Mĭʔ</ts>
                  <nts id="Seg_2742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2744" n="HIAT:w" s="T669">dĭʔnə</ts>
                  <nts id="Seg_2745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2747" n="HIAT:w" s="T670">onʼiʔ</ts>
                  <nts id="Seg_2748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2750" n="HIAT:w" s="T671">ipek</ts>
                  <nts id="Seg_2751" n="HIAT:ip">.</nts>
                  <nts id="Seg_2752" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T675" id="Seg_2754" n="HIAT:u" s="T672">
                  <ts e="T673" id="Seg_2756" n="HIAT:w" s="T672">Dĭ</ts>
                  <nts id="Seg_2757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2759" n="HIAT:w" s="T673">mĭbi</ts>
                  <nts id="Seg_2760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2762" n="HIAT:w" s="T674">dĭʔnə</ts>
                  <nts id="Seg_2763" n="HIAT:ip">.</nts>
                  <nts id="Seg_2764" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T677" id="Seg_2766" n="HIAT:u" s="T675">
                  <ts e="T676" id="Seg_2768" n="HIAT:w" s="T675">Kumen</ts>
                  <nts id="Seg_2769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2771" n="HIAT:w" s="T676">pizittə</ts>
                  <nts id="Seg_2772" n="HIAT:ip">?</nts>
                  <nts id="Seg_2773" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T685" id="Seg_2775" n="HIAT:u" s="T677">
                  <ts e="T678" id="Seg_2777" n="HIAT:w" s="T677">A</ts>
                  <nts id="Seg_2778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2780" n="HIAT:w" s="T678">măn</ts>
                  <nts id="Seg_2781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2783" n="HIAT:w" s="T679">măndəm:</ts>
                  <nts id="Seg_2784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2785" n="HIAT:ip">(</nts>
                  <ts e="T681" id="Seg_2787" n="HIAT:w" s="T680">Šalaʔ</ts>
                  <nts id="Seg_2788" n="HIAT:ip">)</nts>
                  <nts id="Seg_2789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2791" n="HIAT:w" s="T681">ej</ts>
                  <nts id="Seg_2792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2794" n="HIAT:w" s="T682">iʔgö</ts>
                  <nts id="Seg_2795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2797" n="HIAT:w" s="T683">kak</ts>
                  <nts id="Seg_2798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2799" n="HIAT:ip">(</nts>
                  <ts e="T685" id="Seg_2801" n="HIAT:w" s="T684">măgăzingəʔ</ts>
                  <nts id="Seg_2802" n="HIAT:ip">)</nts>
                  <nts id="Seg_2803" n="HIAT:ip">.</nts>
                  <nts id="Seg_2804" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T693" id="Seg_2806" n="HIAT:u" s="T685">
                  <ts e="T686" id="Seg_2808" n="HIAT:w" s="T685">Dĭgəttə</ts>
                  <nts id="Seg_2809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2810" n="HIAT:ip">(</nts>
                  <ts e="T687" id="Seg_2812" n="HIAT:w" s="T686">dĭn=</ts>
                  <nts id="Seg_2813" n="HIAT:ip">)</nts>
                  <nts id="Seg_2814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2816" n="HIAT:w" s="T687">dĭ</ts>
                  <nts id="Seg_2817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2819" n="HIAT:w" s="T688">dĭʔnə</ts>
                  <nts id="Seg_2820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2822" n="HIAT:w" s="T689">mĭbi</ts>
                  <nts id="Seg_2823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2825" n="HIAT:w" s="T690">teʔtə</ts>
                  <nts id="Seg_2826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2828" n="HIAT:w" s="T691">bieʔ</ts>
                  <nts id="Seg_2829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2831" n="HIAT:w" s="T692">kăpʼejkaʔi</ts>
                  <nts id="Seg_2832" n="HIAT:ip">.</nts>
                  <nts id="Seg_2833" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T694" id="Seg_2835" n="HIAT:u" s="T693">
                  <nts id="Seg_2836" n="HIAT:ip">(</nts>
                  <nts id="Seg_2837" n="HIAT:ip">(</nts>
                  <ats e="T694" id="Seg_2838" n="HIAT:non-pho" s="T693">BRK</ats>
                  <nts id="Seg_2839" n="HIAT:ip">)</nts>
                  <nts id="Seg_2840" n="HIAT:ip">)</nts>
                  <nts id="Seg_2841" n="HIAT:ip">.</nts>
                  <nts id="Seg_2842" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T697" id="Seg_2844" n="HIAT:u" s="T694">
                  <ts e="T695" id="Seg_2846" n="HIAT:w" s="T694">Dĭgəttə</ts>
                  <nts id="Seg_2847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2849" n="HIAT:w" s="T695">măn</ts>
                  <nts id="Seg_2850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2852" n="HIAT:w" s="T696">šobiam</ts>
                  <nts id="Seg_2853" n="HIAT:ip">.</nts>
                  <nts id="Seg_2854" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T703" id="Seg_2856" n="HIAT:u" s="T697">
                  <ts e="T698" id="Seg_2858" n="HIAT:w" s="T697">Tüžöjbə</ts>
                  <nts id="Seg_2859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2860" n="HIAT:ip">(</nts>
                  <ts e="T699" id="Seg_2862" n="HIAT:w" s="T698">bĭtəjbiem</ts>
                  <nts id="Seg_2863" n="HIAT:ip">)</nts>
                  <nts id="Seg_2864" n="HIAT:ip">,</nts>
                  <nts id="Seg_2865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2867" n="HIAT:w" s="T699">buzo</ts>
                  <nts id="Seg_2868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2870" n="HIAT:w" s="T700">bĭtəlbiem</ts>
                  <nts id="Seg_2871" n="HIAT:ip">,</nts>
                  <nts id="Seg_2872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2874" n="HIAT:w" s="T701">noʔ</ts>
                  <nts id="Seg_2875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2877" n="HIAT:w" s="T702">mĭbiem</ts>
                  <nts id="Seg_2878" n="HIAT:ip">.</nts>
                  <nts id="Seg_2879" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T706" id="Seg_2881" n="HIAT:u" s="T703">
                  <ts e="T704" id="Seg_2883" n="HIAT:w" s="T703">Dĭgəttə</ts>
                  <nts id="Seg_2884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2886" n="HIAT:w" s="T704">büjle</ts>
                  <nts id="Seg_2887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2889" n="HIAT:w" s="T705">mĭmbiem</ts>
                  <nts id="Seg_2890" n="HIAT:ip">.</nts>
                  <nts id="Seg_2891" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T712" id="Seg_2893" n="HIAT:u" s="T706">
                  <ts e="T707" id="Seg_2895" n="HIAT:w" s="T706">I</ts>
                  <nts id="Seg_2896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2897" n="HIAT:ip">(</nts>
                  <ts e="T708" id="Seg_2899" n="HIAT:w" s="T707">š-</ts>
                  <nts id="Seg_2900" n="HIAT:ip">)</nts>
                  <nts id="Seg_2901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2903" n="HIAT:w" s="T708">šobiam</ts>
                  <nts id="Seg_2904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2906" n="HIAT:w" s="T709">turanə</ts>
                  <nts id="Seg_2907" n="HIAT:ip">,</nts>
                  <nts id="Seg_2908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2910" n="HIAT:w" s="T710">ujum</ts>
                  <nts id="Seg_2911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2912" n="HIAT:ip">(</nts>
                  <ts e="T712" id="Seg_2914" n="HIAT:w" s="T711">kănnambi</ts>
                  <nts id="Seg_2915" n="HIAT:ip">)</nts>
                  <nts id="Seg_2916" n="HIAT:ip">.</nts>
                  <nts id="Seg_2917" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T716" id="Seg_2919" n="HIAT:u" s="T712">
                  <ts e="T713" id="Seg_2921" n="HIAT:w" s="T712">Pʼeštə</ts>
                  <nts id="Seg_2922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2923" n="HIAT:ip">(</nts>
                  <ts e="T714" id="Seg_2925" n="HIAT:w" s="T713">š-</ts>
                  <nts id="Seg_2926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2928" n="HIAT:w" s="T714">s-</ts>
                  <nts id="Seg_2929" n="HIAT:ip">)</nts>
                  <nts id="Seg_2930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2931" n="HIAT:ip">(</nts>
                  <ts e="T716" id="Seg_2933" n="HIAT:w" s="T715">sʼabiam</ts>
                  <nts id="Seg_2934" n="HIAT:ip">)</nts>
                  <nts id="Seg_2935" n="HIAT:ip">.</nts>
                  <nts id="Seg_2936" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T719" id="Seg_2938" n="HIAT:u" s="T716">
                  <ts e="T717" id="Seg_2940" n="HIAT:w" s="T716">Pʼeštə</ts>
                  <nts id="Seg_2941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2942" n="HIAT:ip">(</nts>
                  <ts e="T718" id="Seg_2944" n="HIAT:w" s="T717">s-</ts>
                  <nts id="Seg_2945" n="HIAT:ip">)</nts>
                  <nts id="Seg_2946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2947" n="HIAT:ip">(</nts>
                  <ts e="T719" id="Seg_2949" n="HIAT:w" s="T718">sʼabiam</ts>
                  <nts id="Seg_2950" n="HIAT:ip">)</nts>
                  <nts id="Seg_2951" n="HIAT:ip">.</nts>
                  <nts id="Seg_2952" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T723" id="Seg_2954" n="HIAT:u" s="T719">
                  <ts e="T720" id="Seg_2956" n="HIAT:w" s="T719">Dĭgəttə</ts>
                  <nts id="Seg_2957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_2959" n="HIAT:w" s="T720">ujum</ts>
                  <nts id="Seg_2960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2961" n="HIAT:ip">(</nts>
                  <ts e="T722" id="Seg_2963" n="HIAT:w" s="T721">ejüm-</ts>
                  <nts id="Seg_2964" n="HIAT:ip">)</nts>
                  <nts id="Seg_2965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2966" n="HIAT:ip">(</nts>
                  <ts e="T723" id="Seg_2968" n="HIAT:w" s="T722">ojimulem</ts>
                  <nts id="Seg_2969" n="HIAT:ip">)</nts>
                  <nts id="Seg_2970" n="HIAT:ip">.</nts>
                  <nts id="Seg_2971" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T728" id="Seg_2973" n="HIAT:u" s="T723">
                  <ts e="T724" id="Seg_2975" n="HIAT:w" s="T723">Dĭzeŋ</ts>
                  <nts id="Seg_2976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2977" n="HIAT:ip">(</nts>
                  <ts e="T725" id="Seg_2979" n="HIAT:w" s="T724">so-</ts>
                  <nts id="Seg_2980" n="HIAT:ip">)</nts>
                  <nts id="Seg_2981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_2983" n="HIAT:w" s="T725">šobiʔi</ts>
                  <nts id="Seg_2984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2986" n="HIAT:w" s="T726">i</ts>
                  <nts id="Seg_2987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2989" n="HIAT:w" s="T727">dʼăbaktərzittə</ts>
                  <nts id="Seg_2990" n="HIAT:ip">.</nts>
                  <nts id="Seg_2991" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T729" id="Seg_2993" n="HIAT:u" s="T728">
                  <nts id="Seg_2994" n="HIAT:ip">(</nts>
                  <nts id="Seg_2995" n="HIAT:ip">(</nts>
                  <ats e="T729" id="Seg_2996" n="HIAT:non-pho" s="T728">BRK</ats>
                  <nts id="Seg_2997" n="HIAT:ip">)</nts>
                  <nts id="Seg_2998" n="HIAT:ip">)</nts>
                  <nts id="Seg_2999" n="HIAT:ip">.</nts>
                  <nts id="Seg_3000" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T737" id="Seg_3002" n="HIAT:u" s="T729">
                  <ts e="T730" id="Seg_3004" n="HIAT:w" s="T729">Dĭgəttə</ts>
                  <nts id="Seg_3005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_3007" n="HIAT:w" s="T730">dʼăbaktərzittə</ts>
                  <nts id="Seg_3008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_3010" n="HIAT:w" s="T731">nada</ts>
                  <nts id="Seg_3011" n="HIAT:ip">,</nts>
                  <nts id="Seg_3012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_3014" n="HIAT:w" s="T732">a</ts>
                  <nts id="Seg_3015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_3017" n="HIAT:w" s="T733">dĭn</ts>
                  <nts id="Seg_3018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_3020" n="HIAT:w" s="T734">mašina</ts>
                  <nts id="Seg_3021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_3023" n="HIAT:w" s="T735">bar</ts>
                  <nts id="Seg_3024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_3026" n="HIAT:w" s="T736">kürümnaʔbə</ts>
                  <nts id="Seg_3027" n="HIAT:ip">.</nts>
                  <nts id="Seg_3028" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T740" id="Seg_3030" n="HIAT:u" s="T737">
                  <ts e="T738" id="Seg_3032" n="HIAT:w" s="T737">Ej</ts>
                  <nts id="Seg_3033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_3035" n="HIAT:w" s="T738">mĭlie</ts>
                  <nts id="Seg_3036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_3038" n="HIAT:w" s="T739">dʼăbaktərzittə</ts>
                  <nts id="Seg_3039" n="HIAT:ip">.</nts>
                  <nts id="Seg_3040" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T745" id="Seg_3042" n="HIAT:u" s="T740">
                  <ts e="T741" id="Seg_3044" n="HIAT:w" s="T740">Miʔ</ts>
                  <nts id="Seg_3045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_3047" n="HIAT:w" s="T741">baʔluʔpibaʔ</ts>
                  <nts id="Seg_3048" n="HIAT:ip">,</nts>
                  <nts id="Seg_3049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_3051" n="HIAT:w" s="T742">a</ts>
                  <nts id="Seg_3052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_3054" n="HIAT:w" s="T743">dĭzeŋ</ts>
                  <nts id="Seg_3055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T987" id="Seg_3057" n="HIAT:w" s="T744">kalla</ts>
                  <nts id="Seg_3058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_3060" n="HIAT:w" s="T987">dʼürbiʔi</ts>
                  <nts id="Seg_3061" n="HIAT:ip">.</nts>
                  <nts id="Seg_3062" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T747" id="Seg_3064" n="HIAT:u" s="T745">
                  <ts e="T746" id="Seg_3066" n="HIAT:w" s="T745">Ej</ts>
                  <nts id="Seg_3067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_3069" n="HIAT:w" s="T746">dʼăbaktərbiabaʔ</ts>
                  <nts id="Seg_3070" n="HIAT:ip">.</nts>
                  <nts id="Seg_3071" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T753" id="Seg_3073" n="HIAT:u" s="T747">
                  <ts e="T748" id="Seg_3075" n="HIAT:w" s="T747">A</ts>
                  <nts id="Seg_3076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_3078" n="HIAT:w" s="T748">tüj</ts>
                  <nts id="Seg_3079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_3081" n="HIAT:w" s="T749">šobiʔi</ts>
                  <nts id="Seg_3082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_3084" n="HIAT:w" s="T750">bazoʔ</ts>
                  <nts id="Seg_3085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3086" n="HIAT:ip">(</nts>
                  <ts e="T752" id="Seg_3088" n="HIAT:w" s="T751">s-</ts>
                  <nts id="Seg_3089" n="HIAT:ip">)</nts>
                  <nts id="Seg_3090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_3092" n="HIAT:w" s="T752">dʼăbaktərzittə</ts>
                  <nts id="Seg_3093" n="HIAT:ip">.</nts>
                  <nts id="Seg_3094" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T754" id="Seg_3096" n="HIAT:u" s="T753">
                  <nts id="Seg_3097" n="HIAT:ip">(</nts>
                  <nts id="Seg_3098" n="HIAT:ip">(</nts>
                  <ats e="T754" id="Seg_3099" n="HIAT:non-pho" s="T753">BRK</ats>
                  <nts id="Seg_3100" n="HIAT:ip">)</nts>
                  <nts id="Seg_3101" n="HIAT:ip">)</nts>
                  <nts id="Seg_3102" n="HIAT:ip">.</nts>
                  <nts id="Seg_3103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T760" id="Seg_3105" n="HIAT:u" s="T754">
                  <ts e="T755" id="Seg_3107" n="HIAT:w" s="T754">Šobiʔi</ts>
                  <nts id="Seg_3108" n="HIAT:ip">,</nts>
                  <nts id="Seg_3109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_3111" n="HIAT:w" s="T755">a</ts>
                  <nts id="Seg_3112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_3114" n="HIAT:w" s="T756">măn</ts>
                  <nts id="Seg_3115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_3117" n="HIAT:w" s="T757">măndəm:</ts>
                  <nts id="Seg_3118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_3120" n="HIAT:w" s="T758">Gijen</ts>
                  <nts id="Seg_3121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_3123" n="HIAT:w" s="T759">ibileʔ</ts>
                  <nts id="Seg_3124" n="HIAT:ip">?</nts>
                  <nts id="Seg_3125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T774" id="Seg_3127" n="HIAT:u" s="T760">
                  <ts e="T761" id="Seg_3129" n="HIAT:w" s="T760">Da</ts>
                  <nts id="Seg_3130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_3132" n="HIAT:w" s="T761">pa</ts>
                  <nts id="Seg_3133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_3135" n="HIAT:w" s="T762">kürbibeʔ</ts>
                  <nts id="Seg_3136" n="HIAT:ip">,</nts>
                  <nts id="Seg_3137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_3139" n="HIAT:w" s="T763">bostə</ts>
                  <nts id="Seg_3140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_3142" n="HIAT:w" s="T764">tura</ts>
                  <nts id="Seg_3143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_3145" n="HIAT:w" s="T765">kürbibeʔ</ts>
                  <nts id="Seg_3146" n="HIAT:ip">,</nts>
                  <nts id="Seg_3147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_3149" n="HIAT:w" s="T766">i</ts>
                  <nts id="Seg_3150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T768" id="Seg_3152" n="HIAT:w" s="T767">urgajan</ts>
                  <nts id="Seg_3153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_3155" n="HIAT:w" s="T768">turanə</ts>
                  <nts id="Seg_3156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3157" n="HIAT:ip">(</nts>
                  <ts e="T770" id="Seg_3159" n="HIAT:w" s="T769">kürbi-</ts>
                  <nts id="Seg_3160" n="HIAT:ip">)</nts>
                  <nts id="Seg_3161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T771" id="Seg_3163" n="HIAT:w" s="T770">kürbibeʔ</ts>
                  <nts id="Seg_3164" n="HIAT:ip">,</nts>
                  <nts id="Seg_3165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_3167" n="HIAT:w" s="T771">i</ts>
                  <nts id="Seg_3168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_3170" n="HIAT:w" s="T772">döbər</ts>
                  <nts id="Seg_3171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_3173" n="HIAT:w" s="T773">šobibaʔ</ts>
                  <nts id="Seg_3174" n="HIAT:ip">"</nts>
                  <nts id="Seg_3175" n="HIAT:ip">.</nts>
                  <nts id="Seg_3176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T775" id="Seg_3178" n="HIAT:u" s="T774">
                  <nts id="Seg_3179" n="HIAT:ip">(</nts>
                  <nts id="Seg_3180" n="HIAT:ip">(</nts>
                  <ats e="T775" id="Seg_3181" n="HIAT:non-pho" s="T774">BRK</ats>
                  <nts id="Seg_3182" n="HIAT:ip">)</nts>
                  <nts id="Seg_3183" n="HIAT:ip">)</nts>
                  <nts id="Seg_3184" n="HIAT:ip">.</nts>
                  <nts id="Seg_3185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T779" id="Seg_3187" n="HIAT:u" s="T775">
                  <ts e="T776" id="Seg_3189" n="HIAT:w" s="T775">Dĭgəttə</ts>
                  <nts id="Seg_3190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_3192" n="HIAT:w" s="T776">iat</ts>
                  <nts id="Seg_3193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_3195" n="HIAT:w" s="T777">šobi</ts>
                  <nts id="Seg_3196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_3198" n="HIAT:w" s="T778">bălʼnʼitsagən</ts>
                  <nts id="Seg_3199" n="HIAT:ip">.</nts>
                  <nts id="Seg_3200" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T785" id="Seg_3202" n="HIAT:u" s="T779">
                  <ts e="T780" id="Seg_3204" n="HIAT:w" s="T779">Davaj</ts>
                  <nts id="Seg_3205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_3207" n="HIAT:w" s="T780">essem</ts>
                  <nts id="Seg_3208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3209" n="HIAT:ip">(</nts>
                  <ts e="T781.tx.1" id="Seg_3211" n="HIAT:w" s="T781">kudonz-</ts>
                  <nts id="Seg_3212" n="HIAT:ip">)</nts>
                  <ts e="T782" id="Seg_3214" n="HIAT:w" s="T781.tx.1">:</ts>
                  <nts id="Seg_3215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_3217" n="HIAT:w" s="T782">ĭmbidə</ts>
                  <nts id="Seg_3218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_3220" n="HIAT:w" s="T783">ej</ts>
                  <nts id="Seg_3221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3222" n="HIAT:ip">(</nts>
                  <ts e="T785" id="Seg_3224" n="HIAT:w" s="T784">aliaʔi</ts>
                  <nts id="Seg_3225" n="HIAT:ip">)</nts>
                  <nts id="Seg_3226" n="HIAT:ip">.</nts>
                  <nts id="Seg_3227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T789" id="Seg_3229" n="HIAT:u" s="T785">
                  <nts id="Seg_3230" n="HIAT:ip">(</nts>
                  <ts e="T786" id="Seg_3232" n="HIAT:w" s="T785">Na</ts>
                  <nts id="Seg_3233" n="HIAT:ip">)</nts>
                  <nts id="Seg_3234" n="HIAT:ip">,</nts>
                  <nts id="Seg_3235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_3237" n="HIAT:w" s="T786">paʔi</ts>
                  <nts id="Seg_3238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_3240" n="HIAT:w" s="T787">eneʔ</ts>
                  <nts id="Seg_3241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_3243" n="HIAT:w" s="T788">ăgradagən</ts>
                  <nts id="Seg_3244" n="HIAT:ip">.</nts>
                  <nts id="Seg_3245" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T795" id="Seg_3247" n="HIAT:u" s="T789">
                  <ts e="T789.tx.1" id="Seg_3249" n="HIAT:w" s="T789">A</ts>
                  <nts id="Seg_3250" n="HIAT:ip">_</nts>
                  <ts e="T790" id="Seg_3252" n="HIAT:w" s="T789.tx.1">to</ts>
                  <nts id="Seg_3253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_3255" n="HIAT:w" s="T790">abal</ts>
                  <nts id="Seg_3256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_3258" n="HIAT:w" s="T791">šoləj</ts>
                  <nts id="Seg_3259" n="HIAT:ip">,</nts>
                  <nts id="Seg_3260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T793" id="Seg_3262" n="HIAT:w" s="T792">ĭmbidə</ts>
                  <nts id="Seg_3263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_3265" n="HIAT:w" s="T793">ej</ts>
                  <nts id="Seg_3266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_3268" n="HIAT:w" s="T794">mĭləj</ts>
                  <nts id="Seg_3269" n="HIAT:ip">.</nts>
                  <nts id="Seg_3270" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T800" id="Seg_3272" n="HIAT:u" s="T795">
                  <ts e="T796" id="Seg_3274" n="HIAT:w" s="T795">Dĭ</ts>
                  <nts id="Seg_3275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797" id="Seg_3277" n="HIAT:w" s="T796">kambi</ts>
                  <nts id="Seg_3278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_3280" n="HIAT:w" s="T797">da</ts>
                  <nts id="Seg_3281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_3283" n="HIAT:w" s="T798">davaj</ts>
                  <nts id="Seg_3284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_3286" n="HIAT:w" s="T799">enzittə</ts>
                  <nts id="Seg_3287" n="HIAT:ip">.</nts>
                  <nts id="Seg_3288" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T804" id="Seg_3290" n="HIAT:u" s="T800">
                  <ts e="T801" id="Seg_3292" n="HIAT:w" s="T800">Dĭgəttə</ts>
                  <nts id="Seg_3293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_3295" n="HIAT:w" s="T801">šobi</ts>
                  <nts id="Seg_3296" n="HIAT:ip">,</nts>
                  <nts id="Seg_3297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_3299" n="HIAT:w" s="T802">multʼagən</ts>
                  <nts id="Seg_3300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_3302" n="HIAT:w" s="T803">băzəjdəbiʔi</ts>
                  <nts id="Seg_3303" n="HIAT:ip">.</nts>
                  <nts id="Seg_3304" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T809" id="Seg_3306" n="HIAT:u" s="T804">
                  <ts e="T805" id="Seg_3308" n="HIAT:w" s="T804">Dĭgəttə</ts>
                  <nts id="Seg_3309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_3311" n="HIAT:w" s="T805">măna</ts>
                  <nts id="Seg_3312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_3314" n="HIAT:w" s="T806">măndəʔi:</ts>
                  <nts id="Seg_3315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_3317" n="HIAT:w" s="T807">Kanaʔ</ts>
                  <nts id="Seg_3318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_3320" n="HIAT:w" s="T808">moltʼanə</ts>
                  <nts id="Seg_3321" n="HIAT:ip">.</nts>
                  <nts id="Seg_3322" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T812" id="Seg_3324" n="HIAT:u" s="T809">
                  <ts e="T810" id="Seg_3326" n="HIAT:w" s="T809">Măn</ts>
                  <nts id="Seg_3327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_3329" n="HIAT:w" s="T810">kambiam</ts>
                  <nts id="Seg_3330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_3332" n="HIAT:w" s="T811">băzəjdəsʼtə</ts>
                  <nts id="Seg_3333" n="HIAT:ip">.</nts>
                  <nts id="Seg_3334" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T816" id="Seg_3336" n="HIAT:u" s="T812">
                  <ts e="T813" id="Seg_3338" n="HIAT:w" s="T812">Dĭgəttə</ts>
                  <nts id="Seg_3339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T814" id="Seg_3341" n="HIAT:w" s="T813">Jelʼa</ts>
                  <nts id="Seg_3342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_3344" n="HIAT:w" s="T814">šobi</ts>
                  <nts id="Seg_3345" n="HIAT:ip">,</nts>
                  <nts id="Seg_3346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_3348" n="HIAT:w" s="T815">băzəjdəsʼtə</ts>
                  <nts id="Seg_3349" n="HIAT:ip">.</nts>
                  <nts id="Seg_3350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T820" id="Seg_3352" n="HIAT:u" s="T816">
                  <ts e="T817" id="Seg_3354" n="HIAT:w" s="T816">Dĭgəttə</ts>
                  <nts id="Seg_3355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_3357" n="HIAT:w" s="T817">tibi</ts>
                  <nts id="Seg_3358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_3360" n="HIAT:w" s="T818">šobi</ts>
                  <nts id="Seg_3361" n="HIAT:ip">,</nts>
                  <nts id="Seg_3362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T820" id="Seg_3364" n="HIAT:w" s="T819">băzəjdəbi</ts>
                  <nts id="Seg_3365" n="HIAT:ip">.</nts>
                  <nts id="Seg_3366" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T823" id="Seg_3368" n="HIAT:u" s="T820">
                  <ts e="T821" id="Seg_3370" n="HIAT:w" s="T820">Dĭgəttə</ts>
                  <nts id="Seg_3371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_3373" n="HIAT:w" s="T821">abat</ts>
                  <nts id="Seg_3374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_3376" n="HIAT:w" s="T822">šobi</ts>
                  <nts id="Seg_3377" n="HIAT:ip">.</nts>
                  <nts id="Seg_3378" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T825" id="Seg_3380" n="HIAT:u" s="T823">
                  <ts e="T824" id="Seg_3382" n="HIAT:w" s="T823">Deʔpi</ts>
                  <nts id="Seg_3383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_3385" n="HIAT:w" s="T824">čulkiʔi</ts>
                  <nts id="Seg_3386" n="HIAT:ip">.</nts>
                  <nts id="Seg_3387" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T827" id="Seg_3389" n="HIAT:u" s="T825">
                  <nts id="Seg_3390" n="HIAT:ip">(</nts>
                  <ts e="T826" id="Seg_3392" n="HIAT:w" s="T825">Piʔmeʔi</ts>
                  <nts id="Seg_3393" n="HIAT:ip">)</nts>
                  <nts id="Seg_3394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_3396" n="HIAT:w" s="T826">deʔpi</ts>
                  <nts id="Seg_3397" n="HIAT:ip">.</nts>
                  <nts id="Seg_3398" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T828" id="Seg_3400" n="HIAT:u" s="T827">
                  <nts id="Seg_3401" n="HIAT:ip">(</nts>
                  <nts id="Seg_3402" n="HIAT:ip">(</nts>
                  <ats e="T828" id="Seg_3403" n="HIAT:non-pho" s="T827">BRK</ats>
                  <nts id="Seg_3404" n="HIAT:ip">)</nts>
                  <nts id="Seg_3405" n="HIAT:ip">)</nts>
                  <nts id="Seg_3406" n="HIAT:ip">.</nts>
                  <nts id="Seg_3407" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T832" id="Seg_3409" n="HIAT:u" s="T828">
                  <ts e="T829" id="Seg_3411" n="HIAT:w" s="T828">Dĭgəttə</ts>
                  <nts id="Seg_3412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_3414" n="HIAT:w" s="T829">šobi</ts>
                  <nts id="Seg_3415" n="HIAT:ip">,</nts>
                  <nts id="Seg_3416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T831" id="Seg_3418" n="HIAT:w" s="T830">ara</ts>
                  <nts id="Seg_3419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_3421" n="HIAT:w" s="T831">deʔpi</ts>
                  <nts id="Seg_3422" n="HIAT:ip">.</nts>
                  <nts id="Seg_3423" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T837" id="Seg_3425" n="HIAT:u" s="T832">
                  <ts e="T833" id="Seg_3427" n="HIAT:w" s="T832">Onʼiʔ</ts>
                  <nts id="Seg_3428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_3430" n="HIAT:w" s="T833">tibi</ts>
                  <nts id="Seg_3431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_3433" n="HIAT:w" s="T834">dĭm</ts>
                  <nts id="Seg_3434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_3436" n="HIAT:w" s="T835">deʔpi</ts>
                  <nts id="Seg_3437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T837" id="Seg_3439" n="HIAT:w" s="T836">inetsi</ts>
                  <nts id="Seg_3440" n="HIAT:ip">.</nts>
                  <nts id="Seg_3441" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T842" id="Seg_3443" n="HIAT:u" s="T837">
                  <ts e="T838" id="Seg_3445" n="HIAT:w" s="T837">Dĭgəttə</ts>
                  <nts id="Seg_3446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_3448" n="HIAT:w" s="T838">dĭ</ts>
                  <nts id="Seg_3449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_3451" n="HIAT:w" s="T839">dĭʔnə</ts>
                  <nts id="Seg_3452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T841" id="Seg_3454" n="HIAT:w" s="T840">ara</ts>
                  <nts id="Seg_3455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_3457" n="HIAT:w" s="T841">kămnəbi</ts>
                  <nts id="Seg_3458" n="HIAT:ip">.</nts>
                  <nts id="Seg_3459" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T847" id="Seg_3461" n="HIAT:u" s="T842">
                  <ts e="T843" id="Seg_3463" n="HIAT:w" s="T842">Šalaʔi</ts>
                  <nts id="Seg_3464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T844" id="Seg_3466" n="HIAT:w" s="T843">amorbi</ts>
                  <nts id="Seg_3467" n="HIAT:ip">,</nts>
                  <nts id="Seg_3468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3469" n="HIAT:ip">(</nts>
                  <ts e="T845" id="Seg_3471" n="HIAT:w" s="T844">ka-</ts>
                  <nts id="Seg_3472" n="HIAT:ip">)</nts>
                  <nts id="Seg_3473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_3475" n="HIAT:w" s="T845">kambi</ts>
                  <nts id="Seg_3476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_3478" n="HIAT:w" s="T846">maːʔndə</ts>
                  <nts id="Seg_3479" n="HIAT:ip">.</nts>
                  <nts id="Seg_3480" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T848" id="Seg_3482" n="HIAT:u" s="T847">
                  <nts id="Seg_3483" n="HIAT:ip">(</nts>
                  <nts id="Seg_3484" n="HIAT:ip">(</nts>
                  <ats e="T848" id="Seg_3485" n="HIAT:non-pho" s="T847">BRK</ats>
                  <nts id="Seg_3486" n="HIAT:ip">)</nts>
                  <nts id="Seg_3487" n="HIAT:ip">)</nts>
                  <nts id="Seg_3488" n="HIAT:ip">.</nts>
                  <nts id="Seg_3489" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T852" id="Seg_3491" n="HIAT:u" s="T848">
                  <ts e="T849" id="Seg_3493" n="HIAT:w" s="T848">Dĭ</ts>
                  <nts id="Seg_3494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T850" id="Seg_3496" n="HIAT:w" s="T849">ne</ts>
                  <nts id="Seg_3497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T851" id="Seg_3499" n="HIAT:w" s="T850">ugandə</ts>
                  <nts id="Seg_3500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_3502" n="HIAT:w" s="T851">kurojoʔ</ts>
                  <nts id="Seg_3503" n="HIAT:ip">.</nts>
                  <nts id="Seg_3504" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T860" id="Seg_3506" n="HIAT:u" s="T852">
                  <ts e="T853" id="Seg_3508" n="HIAT:w" s="T852">Pa</ts>
                  <nts id="Seg_3509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_3511" n="HIAT:w" s="T853">ibi</ts>
                  <nts id="Seg_3512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T855" id="Seg_3514" n="HIAT:w" s="T854">da</ts>
                  <nts id="Seg_3515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856" id="Seg_3517" n="HIAT:w" s="T855">büzəm</ts>
                  <nts id="Seg_3518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3519" n="HIAT:ip">(</nts>
                  <ts e="T857" id="Seg_3521" n="HIAT:w" s="T856">pa-</ts>
                  <nts id="Seg_3522" n="HIAT:ip">)</nts>
                  <nts id="Seg_3523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T858" id="Seg_3525" n="HIAT:w" s="T857">pazi</ts>
                  <nts id="Seg_3526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T859" id="Seg_3528" n="HIAT:w" s="T858">toʔnarbi</ts>
                  <nts id="Seg_3529" n="HIAT:ip">,</nts>
                  <nts id="Seg_3530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_3532" n="HIAT:w" s="T859">sʼimattə</ts>
                  <nts id="Seg_3533" n="HIAT:ip">.</nts>
                  <nts id="Seg_3534" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T863" id="Seg_3536" n="HIAT:u" s="T860">
                  <ts e="T861" id="Seg_3538" n="HIAT:w" s="T860">Kăjdə</ts>
                  <nts id="Seg_3539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T862" id="Seg_3541" n="HIAT:w" s="T861">bar</ts>
                  <nts id="Seg_3542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T863" id="Seg_3544" n="HIAT:w" s="T862">mʼaŋnaʔbə</ts>
                  <nts id="Seg_3545" n="HIAT:ip">.</nts>
                  <nts id="Seg_3546" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T870" id="Seg_3548" n="HIAT:u" s="T863">
                  <ts e="T864" id="Seg_3550" n="HIAT:w" s="T863">A</ts>
                  <nts id="Seg_3551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T865" id="Seg_3553" n="HIAT:w" s="T864">măn</ts>
                  <nts id="Seg_3554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_3556" n="HIAT:w" s="T865">bostə</ts>
                  <nts id="Seg_3557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T867" id="Seg_3559" n="HIAT:w" s="T866">büzəm</ts>
                  <nts id="Seg_3560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T868" id="Seg_3562" n="HIAT:w" s="T867">ej</ts>
                  <nts id="Seg_3563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T869" id="Seg_3565" n="HIAT:w" s="T868">münöriam</ts>
                  <nts id="Seg_3566" n="HIAT:ip">,</nts>
                  <nts id="Seg_3567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3568" n="HIAT:ip">(</nts>
                  <ts e="T870" id="Seg_3570" n="HIAT:w" s="T869">udazi</ts>
                  <nts id="Seg_3571" n="HIAT:ip">)</nts>
                  <nts id="Seg_3572" n="HIAT:ip">.</nts>
                  <nts id="Seg_3573" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T875" id="Seg_3575" n="HIAT:u" s="T870">
                  <ts e="T871" id="Seg_3577" n="HIAT:w" s="T870">Udazi</ts>
                  <nts id="Seg_3578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T872" id="Seg_3580" n="HIAT:w" s="T871">toʔlim</ts>
                  <nts id="Seg_3581" n="HIAT:ip">,</nts>
                  <nts id="Seg_3582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T873" id="Seg_3584" n="HIAT:w" s="T872">šalaʔi</ts>
                  <nts id="Seg_3585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T874" id="Seg_3587" n="HIAT:w" s="T873">bar</ts>
                  <nts id="Seg_3588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3589" n="HIAT:ip">(</nts>
                  <ts e="T875" id="Seg_3591" n="HIAT:w" s="T874">bĭtluʔpi</ts>
                  <nts id="Seg_3592" n="HIAT:ip">)</nts>
                  <nts id="Seg_3593" n="HIAT:ip">.</nts>
                  <nts id="Seg_3594" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T876" id="Seg_3596" n="HIAT:u" s="T875">
                  <nts id="Seg_3597" n="HIAT:ip">(</nts>
                  <nts id="Seg_3598" n="HIAT:ip">(</nts>
                  <ats e="T876" id="Seg_3599" n="HIAT:non-pho" s="T875">BRK</ats>
                  <nts id="Seg_3600" n="HIAT:ip">)</nts>
                  <nts id="Seg_3601" n="HIAT:ip">)</nts>
                  <nts id="Seg_3602" n="HIAT:ip">.</nts>
                  <nts id="Seg_3603" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T882" id="Seg_3605" n="HIAT:u" s="T876">
                  <ts e="T877" id="Seg_3607" n="HIAT:w" s="T876">Măn</ts>
                  <nts id="Seg_3608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T878" id="Seg_3610" n="HIAT:w" s="T877">turagən</ts>
                  <nts id="Seg_3611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T879" id="Seg_3613" n="HIAT:w" s="T878">kürbiem</ts>
                  <nts id="Seg_3614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T880" id="Seg_3616" n="HIAT:w" s="T879">mašinam</ts>
                  <nts id="Seg_3617" n="HIAT:ip">,</nts>
                  <nts id="Seg_3618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T882" id="Seg_3620" n="HIAT:w" s="T880">i</ts>
                  <nts id="Seg_3621" n="HIAT:ip">…</nts>
                  <nts id="Seg_3622" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T887" id="Seg_3624" n="HIAT:u" s="T882">
                  <ts e="T883" id="Seg_3626" n="HIAT:w" s="T882">Dĭzeŋ</ts>
                  <nts id="Seg_3627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T884" id="Seg_3629" n="HIAT:w" s="T883">oʔb</ts>
                  <nts id="Seg_3630" n="HIAT:ip">,</nts>
                  <nts id="Seg_3631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T885" id="Seg_3633" n="HIAT:w" s="T884">šide</ts>
                  <nts id="Seg_3634" n="HIAT:ip">,</nts>
                  <nts id="Seg_3635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T886" id="Seg_3637" n="HIAT:w" s="T885">nagur</ts>
                  <nts id="Seg_3638" n="HIAT:ip">,</nts>
                  <nts id="Seg_3639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T887" id="Seg_3641" n="HIAT:w" s="T886">teʔtə</ts>
                  <nts id="Seg_3642" n="HIAT:ip">.</nts>
                  <nts id="Seg_3643" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T898" id="Seg_3645" n="HIAT:u" s="T887">
                  <ts e="T888" id="Seg_3647" n="HIAT:w" s="T887">Dĭgəttə</ts>
                  <nts id="Seg_3648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T889" id="Seg_3650" n="HIAT:w" s="T888">Vanʼa</ts>
                  <nts id="Seg_3651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T890" id="Seg_3653" n="HIAT:w" s="T889">măndə:</ts>
                  <nts id="Seg_3654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T891" id="Seg_3656" n="HIAT:w" s="T890">Büzö</ts>
                  <nts id="Seg_3657" n="HIAT:ip">,</nts>
                  <nts id="Seg_3658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T892" id="Seg_3660" n="HIAT:w" s="T891">kereʔ</ts>
                  <nts id="Seg_3661" n="HIAT:ip">,</nts>
                  <nts id="Seg_3662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T893" id="Seg_3664" n="HIAT:w" s="T892">i</ts>
                  <nts id="Seg_3665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T894" id="Seg_3667" n="HIAT:w" s="T893">šoškaʔi</ts>
                  <nts id="Seg_3668" n="HIAT:ip">,</nts>
                  <nts id="Seg_3669" n="HIAT:ip">—</nts>
                  <nts id="Seg_3670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T895" id="Seg_3672" n="HIAT:w" s="T894">a</ts>
                  <nts id="Seg_3673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T896" id="Seg_3675" n="HIAT:w" s="T895">miʔ</ts>
                  <nts id="Seg_3676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T897" id="Seg_3678" n="HIAT:w" s="T896">bar</ts>
                  <nts id="Seg_3679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898" id="Seg_3681" n="HIAT:w" s="T897">kaknarbibaʔ</ts>
                  <nts id="Seg_3682" n="HIAT:ip">.</nts>
                  <nts id="Seg_3683" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T899" id="Seg_3685" n="HIAT:u" s="T898">
                  <nts id="Seg_3686" n="HIAT:ip">(</nts>
                  <nts id="Seg_3687" n="HIAT:ip">(</nts>
                  <ats e="T899" id="Seg_3688" n="HIAT:non-pho" s="T898">BRK</ats>
                  <nts id="Seg_3689" n="HIAT:ip">)</nts>
                  <nts id="Seg_3690" n="HIAT:ip">)</nts>
                  <nts id="Seg_3691" n="HIAT:ip">.</nts>
                  <nts id="Seg_3692" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T903" id="Seg_3694" n="HIAT:u" s="T899">
                  <ts e="T900" id="Seg_3696" n="HIAT:w" s="T899">Teinen</ts>
                  <nts id="Seg_3697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T901" id="Seg_3699" n="HIAT:w" s="T900">padʼi</ts>
                  <nts id="Seg_3700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T902" id="Seg_3702" n="HIAT:w" s="T901">iʔgö</ts>
                  <nts id="Seg_3703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T903" id="Seg_3705" n="HIAT:w" s="T902">dʼăbaktərbibaʔ</ts>
                  <nts id="Seg_3706" n="HIAT:ip">.</nts>
                  <nts id="Seg_3707" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T911" id="Seg_3709" n="HIAT:u" s="T903">
                  <ts e="T904" id="Seg_3711" n="HIAT:w" s="T903">Tüj</ts>
                  <nts id="Seg_3712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T905" id="Seg_3714" n="HIAT:w" s="T904">nada</ts>
                  <nts id="Seg_3715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T906" id="Seg_3717" n="HIAT:w" s="T905">maʔnʼi</ts>
                  <nts id="Seg_3718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T907" id="Seg_3720" n="HIAT:w" s="T906">kanzittə</ts>
                  <nts id="Seg_3721" n="HIAT:ip">,</nts>
                  <nts id="Seg_3722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T908" id="Seg_3724" n="HIAT:w" s="T907">amorzittə</ts>
                  <nts id="Seg_3725" n="HIAT:ip">,</nts>
                  <nts id="Seg_3726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909" id="Seg_3728" n="HIAT:w" s="T908">iʔbəsʼtə</ts>
                  <nts id="Seg_3729" n="HIAT:ip">,</nts>
                  <nts id="Seg_3730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3731" n="HIAT:ip">(</nts>
                  <ts e="T910" id="Seg_3733" n="HIAT:w" s="T909">šal-</ts>
                  <nts id="Seg_3734" n="HIAT:ip">)</nts>
                  <nts id="Seg_3735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3736" n="HIAT:ip">(</nts>
                  <ts e="T911" id="Seg_3738" n="HIAT:w" s="T910">šalaʔi</ts>
                  <nts id="Seg_3739" n="HIAT:ip">)</nts>
                  <nts id="Seg_3740" n="HIAT:ip">.</nts>
                  <nts id="Seg_3741" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T912" id="Seg_3743" n="HIAT:u" s="T911">
                  <nts id="Seg_3744" n="HIAT:ip">(</nts>
                  <nts id="Seg_3745" n="HIAT:ip">(</nts>
                  <ats e="T912" id="Seg_3746" n="HIAT:non-pho" s="T911">BRK</ats>
                  <nts id="Seg_3747" n="HIAT:ip">)</nts>
                  <nts id="Seg_3748" n="HIAT:ip">)</nts>
                  <nts id="Seg_3749" n="HIAT:ip">.</nts>
                  <nts id="Seg_3750" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T916" id="Seg_3752" n="HIAT:u" s="T912">
                  <ts e="T913" id="Seg_3754" n="HIAT:w" s="T912">Bor</ts>
                  <nts id="Seg_3755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T914" id="Seg_3757" n="HIAT:w" s="T913">bar</ts>
                  <nts id="Seg_3758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T915" id="Seg_3760" n="HIAT:w" s="T914">nʼiʔtə</ts>
                  <nts id="Seg_3761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T916" id="Seg_3763" n="HIAT:w" s="T915">kandəga</ts>
                  <nts id="Seg_3764" n="HIAT:ip">.</nts>
                  <nts id="Seg_3765" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T919" id="Seg_3767" n="HIAT:u" s="T916">
                  <ts e="T917" id="Seg_3769" n="HIAT:w" s="T916">Ugandə</ts>
                  <nts id="Seg_3770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T918" id="Seg_3772" n="HIAT:w" s="T917">šišəge</ts>
                  <nts id="Seg_3773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T919" id="Seg_3775" n="HIAT:w" s="T918">moləj</ts>
                  <nts id="Seg_3776" n="HIAT:ip">.</nts>
                  <nts id="Seg_3777" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T922" id="Seg_3779" n="HIAT:u" s="T919">
                  <ts e="T920" id="Seg_3781" n="HIAT:w" s="T919">Teinen</ts>
                  <nts id="Seg_3782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T921" id="Seg_3784" n="HIAT:w" s="T920">nüdʼin</ts>
                  <nts id="Seg_3785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T922" id="Seg_3787" n="HIAT:w" s="T921">sĭre</ts>
                  <nts id="Seg_3788" n="HIAT:ip">.</nts>
                  <nts id="Seg_3789" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T927" id="Seg_3791" n="HIAT:u" s="T922">
                  <ts e="T923" id="Seg_3793" n="HIAT:w" s="T922">Ertən</ts>
                  <nts id="Seg_3794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T924" id="Seg_3796" n="HIAT:w" s="T923">uʔbdəbiam</ts>
                  <nts id="Seg_3797" n="HIAT:ip">—</nts>
                  <nts id="Seg_3798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T925" id="Seg_3800" n="HIAT:w" s="T924">bar</ts>
                  <nts id="Seg_3801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T926" id="Seg_3803" n="HIAT:w" s="T925">šišəge</ts>
                  <nts id="Seg_3804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T927" id="Seg_3806" n="HIAT:w" s="T926">molambi</ts>
                  <nts id="Seg_3807" n="HIAT:ip">.</nts>
                  <nts id="Seg_3808" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T928" id="Seg_3810" n="HIAT:u" s="T927">
                  <nts id="Seg_3811" n="HIAT:ip">(</nts>
                  <nts id="Seg_3812" n="HIAT:ip">(</nts>
                  <ats e="T928" id="Seg_3813" n="HIAT:non-pho" s="T927">BRK</ats>
                  <nts id="Seg_3814" n="HIAT:ip">)</nts>
                  <nts id="Seg_3815" n="HIAT:ip">)</nts>
                  <nts id="Seg_3816" n="HIAT:ip">.</nts>
                  <nts id="Seg_3817" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T933" id="Seg_3819" n="HIAT:u" s="T928">
                  <ts e="T929" id="Seg_3821" n="HIAT:w" s="T928">Miʔnʼibeʔ</ts>
                  <nts id="Seg_3822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T930" id="Seg_3824" n="HIAT:w" s="T929">teinen</ts>
                  <nts id="Seg_3825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T931" id="Seg_3827" n="HIAT:w" s="T930">nada</ts>
                  <nts id="Seg_3828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T932" id="Seg_3830" n="HIAT:w" s="T931">kanzittə</ts>
                  <nts id="Seg_3831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T933" id="Seg_3833" n="HIAT:w" s="T932">Tanʼanə</ts>
                  <nts id="Seg_3834" n="HIAT:ip">.</nts>
                  <nts id="Seg_3835" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T941" id="Seg_3837" n="HIAT:u" s="T933">
                  <ts e="T934" id="Seg_3839" n="HIAT:w" s="T933">Štobɨ</ts>
                  <nts id="Seg_3840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T935" id="Seg_3842" n="HIAT:w" s="T934">dĭ</ts>
                  <nts id="Seg_3843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T936" id="Seg_3845" n="HIAT:w" s="T935">ipek</ts>
                  <nts id="Seg_3846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T937" id="Seg_3848" n="HIAT:w" s="T936">ibi</ts>
                  <nts id="Seg_3849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3850" n="HIAT:ip">(</nts>
                  <ts e="T938" id="Seg_3852" n="HIAT:w" s="T937">Văznesenkagə</ts>
                  <nts id="Seg_3853" n="HIAT:ip">)</nts>
                  <nts id="Seg_3854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T939" id="Seg_3856" n="HIAT:w" s="T938">i</ts>
                  <nts id="Seg_3857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T940" id="Seg_3859" n="HIAT:w" s="T939">deʔpi</ts>
                  <nts id="Seg_3860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T941" id="Seg_3862" n="HIAT:w" s="T940">miʔnʼibeʔ</ts>
                  <nts id="Seg_3863" n="HIAT:ip">.</nts>
                  <nts id="Seg_3864" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T942" id="Seg_3866" n="HIAT:u" s="T941">
                  <nts id="Seg_3867" n="HIAT:ip">(</nts>
                  <nts id="Seg_3868" n="HIAT:ip">(</nts>
                  <ats e="T942" id="Seg_3869" n="HIAT:non-pho" s="T941">BRK</ats>
                  <nts id="Seg_3870" n="HIAT:ip">)</nts>
                  <nts id="Seg_3871" n="HIAT:ip">)</nts>
                  <nts id="Seg_3872" n="HIAT:ip">.</nts>
                  <nts id="Seg_3873" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T947" id="Seg_3875" n="HIAT:u" s="T942">
                  <nts id="Seg_3876" n="HIAT:ip">(</nts>
                  <nts id="Seg_3877" n="HIAT:ip">(</nts>
                  <ats e="T943" id="Seg_3878" n="HIAT:non-pho" s="T942">…</ats>
                  <nts id="Seg_3879" n="HIAT:ip">)</nts>
                  <nts id="Seg_3880" n="HIAT:ip">)</nts>
                  <nts id="Seg_3881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T944" id="Seg_3883" n="HIAT:w" s="T943">nada</ts>
                  <nts id="Seg_3884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T945" id="Seg_3886" n="HIAT:w" s="T944">aktʼam</ts>
                  <nts id="Seg_3887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T946" id="Seg_3889" n="HIAT:w" s="T945">izittə</ts>
                  <nts id="Seg_3890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T947" id="Seg_3892" n="HIAT:w" s="T946">diʔnə</ts>
                  <nts id="Seg_3893" n="HIAT:ip">.</nts>
                  <nts id="Seg_3894" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T953" id="Seg_3896" n="HIAT:u" s="T947">
                  <ts e="T948" id="Seg_3898" n="HIAT:w" s="T947">Ej</ts>
                  <nts id="Seg_3899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T949" id="Seg_3901" n="HIAT:w" s="T948">boskəndə</ts>
                  <nts id="Seg_3902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T950" id="Seg_3904" n="HIAT:w" s="T949">deʔsittə</ts>
                  <nts id="Seg_3905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T951" id="Seg_3907" n="HIAT:w" s="T950">dĭʔə</ts>
                  <nts id="Seg_3908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T952" id="Seg_3910" n="HIAT:w" s="T951">inetsi</ts>
                  <nts id="Seg_3911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T953" id="Seg_3913" n="HIAT:w" s="T952">mĭlləj</ts>
                  <nts id="Seg_3914" n="HIAT:ip">.</nts>
                  <nts id="Seg_3915" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T955" id="Seg_3917" n="HIAT:u" s="T953">
                  <ts e="T954" id="Seg_3919" n="HIAT:w" s="T953">I</ts>
                  <nts id="Seg_3920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T955" id="Seg_3922" n="HIAT:w" s="T954">detləj</ts>
                  <nts id="Seg_3923" n="HIAT:ip">.</nts>
                  <nts id="Seg_3924" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T956" id="Seg_3926" n="HIAT:u" s="T955">
                  <nts id="Seg_3927" n="HIAT:ip">(</nts>
                  <nts id="Seg_3928" n="HIAT:ip">(</nts>
                  <ats e="T956" id="Seg_3929" n="HIAT:non-pho" s="T955">BRK</ats>
                  <nts id="Seg_3930" n="HIAT:ip">)</nts>
                  <nts id="Seg_3931" n="HIAT:ip">)</nts>
                  <nts id="Seg_3932" n="HIAT:ip">.</nts>
                  <nts id="Seg_3933" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T961" id="Seg_3935" n="HIAT:u" s="T956">
                  <ts e="T957" id="Seg_3937" n="HIAT:w" s="T956">Ej</ts>
                  <nts id="Seg_3938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T958" id="Seg_3940" n="HIAT:w" s="T957">tĭmnem</ts>
                  <nts id="Seg_3941" n="HIAT:ip">,</nts>
                  <nts id="Seg_3942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T959" id="Seg_3944" n="HIAT:w" s="T958">iləj</ts>
                  <nts id="Seg_3945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T960" id="Seg_3947" n="HIAT:w" s="T959">ilʼi</ts>
                  <nts id="Seg_3948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T961" id="Seg_3950" n="HIAT:w" s="T960">ej</ts>
                  <nts id="Seg_3951" n="HIAT:ip">.</nts>
                  <nts id="Seg_3952" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T967" id="Seg_3954" n="HIAT:u" s="T961">
                  <ts e="T962" id="Seg_3956" n="HIAT:w" s="T961">Nada</ts>
                  <nts id="Seg_3957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T963" id="Seg_3959" n="HIAT:w" s="T962">kanzittə</ts>
                  <nts id="Seg_3960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T964" id="Seg_3962" n="HIAT:w" s="T963">dĭzi</ts>
                  <nts id="Seg_3963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T965" id="Seg_3965" n="HIAT:w" s="T964">dʼăbaktərzittə</ts>
                  <nts id="Seg_3966" n="HIAT:ip">,</nts>
                  <nts id="Seg_3967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T966" id="Seg_3969" n="HIAT:w" s="T965">možet</ts>
                  <nts id="Seg_3970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T967" id="Seg_3972" n="HIAT:w" s="T966">iləj</ts>
                  <nts id="Seg_3973" n="HIAT:ip">.</nts>
                  <nts id="Seg_3974" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T970" id="Seg_3976" n="HIAT:u" s="T967">
                  <ts e="T968" id="Seg_3978" n="HIAT:w" s="T967">No</ts>
                  <nts id="Seg_3979" n="HIAT:ip">,</nts>
                  <nts id="Seg_3980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T969" id="Seg_3982" n="HIAT:w" s="T968">kanaʔ</ts>
                  <nts id="Seg_3983" n="HIAT:ip">,</nts>
                  <nts id="Seg_3984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T970" id="Seg_3986" n="HIAT:w" s="T969">dʼăbaktəraʔ</ts>
                  <nts id="Seg_3987" n="HIAT:ip">.</nts>
                  <nts id="Seg_3988" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T971" id="Seg_3990" n="HIAT:u" s="T970">
                  <nts id="Seg_3991" n="HIAT:ip">(</nts>
                  <nts id="Seg_3992" n="HIAT:ip">(</nts>
                  <ats e="T971" id="Seg_3993" n="HIAT:non-pho" s="T970">BRK</ats>
                  <nts id="Seg_3994" n="HIAT:ip">)</nts>
                  <nts id="Seg_3995" n="HIAT:ip">)</nts>
                  <nts id="Seg_3996" n="HIAT:ip">.</nts>
                  <nts id="Seg_3997" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T976" id="Seg_3999" n="HIAT:u" s="T971">
                  <ts e="T972" id="Seg_4001" n="HIAT:w" s="T971">Dĭ</ts>
                  <nts id="Seg_4002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T973" id="Seg_4004" n="HIAT:w" s="T972">možet</ts>
                  <nts id="Seg_4005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T974" id="Seg_4007" n="HIAT:w" s="T973">mĭlləj</ts>
                  <nts id="Seg_4008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T975" id="Seg_4010" n="HIAT:w" s="T974">măgăzingən</ts>
                  <nts id="Seg_4011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T976" id="Seg_4013" n="HIAT:w" s="T975">dĭn</ts>
                  <nts id="Seg_4014" n="HIAT:ip">.</nts>
                  <nts id="Seg_4015" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T981" id="Seg_4017" n="HIAT:u" s="T976">
                  <ts e="T977" id="Seg_4019" n="HIAT:w" s="T976">Очередь</ts>
                  <nts id="Seg_4020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T978" id="Seg_4022" n="HIAT:w" s="T977">nuzittə</ts>
                  <nts id="Seg_4023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T979" id="Seg_4025" n="HIAT:w" s="T978">nada</ts>
                  <nts id="Seg_4026" n="HIAT:ip">,</nts>
                  <nts id="Seg_4027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T980" id="Seg_4029" n="HIAT:w" s="T979">ipek</ts>
                  <nts id="Seg_4030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T981" id="Seg_4032" n="HIAT:w" s="T980">izittə</ts>
                  <nts id="Seg_4033" n="HIAT:ip">.</nts>
                  <nts id="Seg_4034" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T984" id="Seg_4036" n="HIAT:u" s="T981">
                  <ts e="T982" id="Seg_4038" n="HIAT:w" s="T981">Măn</ts>
                  <nts id="Seg_4039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T983" id="Seg_4041" n="HIAT:w" s="T982">ej</ts>
                  <nts id="Seg_4042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T984" id="Seg_4044" n="HIAT:w" s="T983">iləm</ts>
                  <nts id="Seg_4045" n="HIAT:ip">.</nts>
                  <nts id="Seg_4046" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T984" id="Seg_4047" n="sc" s="T0">
               <ts e="T1" id="Seg_4049" n="e" s="T0">Kaŋgaʔ </ts>
               <ts e="T2" id="Seg_4051" n="e" s="T1">multʼanə </ts>
               <ts e="T3" id="Seg_4053" n="e" s="T2">(obbərej) </ts>
               <ts e="T4" id="Seg_4055" n="e" s="T3">šidegöʔ. </ts>
               <ts e="T5" id="Seg_4057" n="e" s="T4">Dĭ </ts>
               <ts e="T6" id="Seg_4059" n="e" s="T5">tănan </ts>
               <ts e="T7" id="Seg_4061" n="e" s="T6">bögəlbə </ts>
               <ts e="T8" id="Seg_4063" n="e" s="T7">kĭškələj </ts>
               <ts e="T9" id="Seg_4065" n="e" s="T8">sabənzi, </ts>
               <ts e="T10" id="Seg_4067" n="e" s="T9">(vixotkazi). </ts>
               <ts e="T11" id="Seg_4069" n="e" s="T10">A </ts>
               <ts e="T12" id="Seg_4071" n="e" s="T11">(tăn </ts>
               <ts e="T13" id="Seg_4073" n="e" s="T12">dĭʔnə). </ts>
               <ts e="T14" id="Seg_4075" n="e" s="T13">Da </ts>
               <ts e="T15" id="Seg_4077" n="e" s="T14">(šereʔ-) </ts>
               <ts e="T16" id="Seg_4079" n="e" s="T15">šeriem. </ts>
               <ts e="T17" id="Seg_4081" n="e" s="T16">Nʼe </ts>
               <ts e="T18" id="Seg_4083" n="e" s="T17">axota </ts>
               <ts e="T19" id="Seg_4085" n="e" s="T18">kanzittə, </ts>
               <ts e="T20" id="Seg_4087" n="e" s="T19">ugandə </ts>
               <ts e="T21" id="Seg_4089" n="e" s="T20">šeriem. </ts>
               <ts e="T22" id="Seg_4091" n="e" s="T21">Kazan </ts>
               <ts e="T23" id="Seg_4093" n="e" s="T22">turagən </ts>
               <ts e="T24" id="Seg_4095" n="e" s="T23">multʼanə </ts>
               <ts e="T25" id="Seg_4097" n="e" s="T24">(ej </ts>
               <ts e="T26" id="Seg_4099" n="e" s="T25">kanʼ-) </ts>
               <ts e="T27" id="Seg_4101" n="e" s="T26">ej </ts>
               <ts e="T28" id="Seg_4103" n="e" s="T27">kaliaʔi </ts>
               <ts e="T29" id="Seg_4105" n="e" s="T28">(obbərej). </ts>
               <ts e="T30" id="Seg_4107" n="e" s="T29">Nezeŋ, </ts>
               <ts e="T31" id="Seg_4109" n="e" s="T30">tibizeŋ. </ts>
               <ts e="T32" id="Seg_4111" n="e" s="T31">Nezeŋ </ts>
               <ts e="T33" id="Seg_4113" n="e" s="T32">onʼiʔ </ts>
               <ts e="T34" id="Seg_4115" n="e" s="T33">(kaŋg-) </ts>
               <ts e="T35" id="Seg_4117" n="e" s="T34">kandəʔi. </ts>
               <ts e="T36" id="Seg_4119" n="e" s="T35">A </ts>
               <ts e="T37" id="Seg_4121" n="e" s="T36">tibizeŋ </ts>
               <ts e="T38" id="Seg_4123" n="e" s="T37">karəldʼan </ts>
               <ts e="T39" id="Seg_4125" n="e" s="T38">nendəleʔbəʔjə </ts>
               <ts e="T40" id="Seg_4127" n="e" s="T39">bazoʔ. </ts>
               <ts e="T41" id="Seg_4129" n="e" s="T40">Onʼiʔ </ts>
               <ts e="T42" id="Seg_4131" n="e" s="T41">kandəgaʔi </ts>
               <ts e="T43" id="Seg_4133" n="e" s="T42">tibizeŋ. </ts>
               <ts e="T44" id="Seg_4135" n="e" s="T43">((BRK)). </ts>
               <ts e="T45" id="Seg_4137" n="e" s="T44">Miʔ </ts>
               <ts e="T46" id="Seg_4139" n="e" s="T45">multʼagən </ts>
               <ts e="T47" id="Seg_4141" n="e" s="T46">koʔbsaŋ </ts>
               <ts e="T48" id="Seg_4143" n="e" s="T47">šidegöʔ </ts>
               <ts e="T49" id="Seg_4145" n="e" s="T48">kandəgaʔi. </ts>
               <ts e="T50" id="Seg_4147" n="e" s="T49">Dĭgəttə </ts>
               <ts e="T51" id="Seg_4149" n="e" s="T50">iat </ts>
               <ts e="T52" id="Seg_4151" n="e" s="T51">kaləj, </ts>
               <ts e="T53" id="Seg_4153" n="e" s="T52">onʼiʔ </ts>
               <ts e="T54" id="Seg_4155" n="e" s="T53">koʔbdo, </ts>
               <ts e="T55" id="Seg_4157" n="e" s="T54">onʼiʔ </ts>
               <ts e="T56" id="Seg_4159" n="e" s="T55">nʼi. </ts>
               <ts e="T57" id="Seg_4161" n="e" s="T56">Băzləj </ts>
               <ts e="T58" id="Seg_4163" n="e" s="T57">dĭzem. </ts>
               <ts e="T59" id="Seg_4165" n="e" s="T58">Dĭzeŋ </ts>
               <ts e="T60" id="Seg_4167" n="e" s="T59">(šolə-) </ts>
               <ts e="T61" id="Seg_4169" n="e" s="T60">šonəgaʔi, </ts>
               <ts e="T62" id="Seg_4171" n="e" s="T61">dĭgəttə </ts>
               <ts e="T63" id="Seg_4173" n="e" s="T62">tibi </ts>
               <ts e="T64" id="Seg_4175" n="e" s="T63">(kalaʔbə). </ts>
               <ts e="T65" id="Seg_4177" n="e" s="T64">A </ts>
               <ts e="T66" id="Seg_4179" n="e" s="T65">dĭgəttə </ts>
               <ts e="T67" id="Seg_4181" n="e" s="T66">măn </ts>
               <ts e="T68" id="Seg_4183" n="e" s="T67">kalaʔbəm, </ts>
               <ts e="T69" id="Seg_4185" n="e" s="T68">(băzəjdliam). </ts>
               <ts e="T70" id="Seg_4187" n="e" s="T69">((BRK)). </ts>
               <ts e="T71" id="Seg_4189" n="e" s="T70">Măn </ts>
               <ts e="T72" id="Seg_4191" n="e" s="T71">tʼotkam </ts>
               <ts e="T73" id="Seg_4193" n="e" s="T72">kambi </ts>
               <ts e="T988" id="Seg_4195" n="e" s="T73">Kazan </ts>
               <ts e="T74" id="Seg_4197" n="e" s="T988">turanə. </ts>
               <ts e="T75" id="Seg_4199" n="e" s="T74">Dĭn </ts>
               <ts e="T76" id="Seg_4201" n="e" s="T75">kak </ts>
               <ts e="T77" id="Seg_4203" n="e" s="T76">raz </ts>
               <ts e="T78" id="Seg_4205" n="e" s="T77">multʼa </ts>
               <ts e="T79" id="Seg_4207" n="e" s="T78">nendəbiʔi. </ts>
               <ts e="T80" id="Seg_4209" n="e" s="T79">Bar </ts>
               <ts e="T81" id="Seg_4211" n="e" s="T80">băzəbiʔi, </ts>
               <ts e="T82" id="Seg_4213" n="e" s="T81">măndəʔi: </ts>
               <ts e="T83" id="Seg_4215" n="e" s="T82">Kanaʔ </ts>
               <ts e="T84" id="Seg_4217" n="e" s="T83">moltʼanə. </ts>
               <ts e="T85" id="Seg_4219" n="e" s="T84">(A </ts>
               <ts e="T86" id="Seg_4221" n="e" s="T85">măn=) </ts>
               <ts e="T87" id="Seg_4223" n="e" s="T86">A </ts>
               <ts e="T88" id="Seg_4225" n="e" s="T87">dĭ </ts>
               <ts e="T89" id="Seg_4227" n="e" s="T88">kambi. </ts>
               <ts e="T90" id="Seg_4229" n="e" s="T89">Ej </ts>
               <ts e="T91" id="Seg_4231" n="e" s="T90">tĭmnet, </ts>
               <ts e="T92" id="Seg_4233" n="e" s="T91">gibər </ts>
               <ts e="T93" id="Seg_4235" n="e" s="T92">bü </ts>
               <ts e="T94" id="Seg_4237" n="e" s="T93">kămnasʼtə. </ts>
               <ts e="T95" id="Seg_4239" n="e" s="T94">Pʼeštə </ts>
               <ts e="T96" id="Seg_4241" n="e" s="T95">kămnəbiam, </ts>
               <ts e="T97" id="Seg_4243" n="e" s="T96">bar </ts>
               <ts e="T98" id="Seg_4245" n="e" s="T97">stʼenaʔi </ts>
               <ts e="T99" id="Seg_4247" n="e" s="T98">büzi </ts>
               <ts e="T100" id="Seg_4249" n="e" s="T99">(обл </ts>
               <ts e="T101" id="Seg_4251" n="e" s="T100">-) </ts>
               <ts e="T102" id="Seg_4253" n="e" s="T101">облила </ts>
               <ts e="T103" id="Seg_4255" n="e" s="T102">стены </ts>
               <ts e="T104" id="Seg_4257" n="e" s="T103">эти. </ts>
               <ts e="T106" id="Seg_4259" n="e" s="T104">По- русски </ts>
               <ts e="T108" id="Seg_4261" n="e" s="T106">((BRK)). </ts>
               <ts e="T109" id="Seg_4263" n="e" s="T108">Dĭgəttə </ts>
               <ts e="T110" id="Seg_4265" n="e" s="T109">băzəjbiam, </ts>
               <ts e="T111" id="Seg_4267" n="e" s="T110">šobiam. </ts>
               <ts e="T112" id="Seg_4269" n="e" s="T111">Ĭmbi </ts>
               <ts e="T113" id="Seg_4271" n="e" s="T112">băzəjbial, </ts>
               <ts e="T114" id="Seg_4273" n="e" s="T113">băzəjbial, </ts>
               <ts e="T115" id="Seg_4275" n="e" s="T114">ej </ts>
               <ts e="T116" id="Seg_4277" n="e" s="T115">(embiel)? </ts>
               <ts e="T117" id="Seg_4279" n="e" s="T116">Ej </ts>
               <ts e="T118" id="Seg_4281" n="e" s="T117">embiem! </ts>
               <ts e="T119" id="Seg_4283" n="e" s="T118">((BRK)). </ts>
               <ts e="T120" id="Seg_4285" n="e" s="T119">Dĭgəttə </ts>
               <ts e="T121" id="Seg_4287" n="e" s="T120">(o-) </ts>
               <ts e="T122" id="Seg_4289" n="e" s="T121">mănliaʔi: </ts>
               <ts e="T123" id="Seg_4291" n="e" s="T122">Kanaʔ! </ts>
               <ts e="T124" id="Seg_4293" n="e" s="T123">Pădvalbə </ts>
               <ts e="T125" id="Seg_4295" n="e" s="T124">uja </ts>
               <ts e="T126" id="Seg_4297" n="e" s="T125">deʔ. </ts>
               <ts e="T127" id="Seg_4299" n="e" s="T126">Kătletaʔinə. </ts>
               <ts e="T128" id="Seg_4301" n="e" s="T127">Măn </ts>
               <ts e="T129" id="Seg_4303" n="e" s="T128">kambiam. </ts>
               <ts e="T130" id="Seg_4305" n="e" s="T129">(Nugia-) </ts>
               <ts e="T131" id="Seg_4307" n="e" s="T130">Nugam, </ts>
               <ts e="T132" id="Seg_4309" n="e" s="T131">uja </ts>
               <ts e="T133" id="Seg_4311" n="e" s="T132">iʔgö, </ts>
               <ts e="T134" id="Seg_4313" n="e" s="T133">a </ts>
               <ts e="T135" id="Seg_4315" n="e" s="T134">aspaʔ </ts>
               <ts e="T136" id="Seg_4317" n="e" s="T135">naga. </ts>
               <ts e="T137" id="Seg_4319" n="e" s="T136">Dĭgəttə </ts>
               <ts e="T138" id="Seg_4321" n="e" s="T137">parbiam. </ts>
               <ts e="T139" id="Seg_4323" n="e" s="T138">Ĭmbi </ts>
               <ts e="T140" id="Seg_4325" n="e" s="T139">uja </ts>
               <ts e="T141" id="Seg_4327" n="e" s="T140">ej </ts>
               <ts e="T142" id="Seg_4329" n="e" s="T141">(det-) </ts>
               <ts e="T143" id="Seg_4331" n="e" s="T142">detlel? </ts>
               <ts e="T144" id="Seg_4333" n="e" s="T143">Da </ts>
               <ts e="T145" id="Seg_4335" n="e" s="T144">uja </ts>
               <ts e="T146" id="Seg_4337" n="e" s="T145">iʔgö, </ts>
               <ts e="T147" id="Seg_4339" n="e" s="T146">a </ts>
               <ts e="T148" id="Seg_4341" n="e" s="T147">aspaʔ </ts>
               <ts e="T149" id="Seg_4343" n="e" s="T148">naga. </ts>
               <ts e="T150" id="Seg_4345" n="e" s="T149">Dĭzeŋ </ts>
               <ts e="T151" id="Seg_4347" n="e" s="T150">davaj </ts>
               <ts e="T152" id="Seg_4349" n="e" s="T151">kaknarzittə! </ts>
               <ts e="T153" id="Seg_4351" n="e" s="T152">((BRK)). </ts>
               <ts e="T154" id="Seg_4353" n="e" s="T153">Dĭgəttə </ts>
               <ts e="T155" id="Seg_4355" n="e" s="T154">mănliaʔi: </ts>
               <ts e="T156" id="Seg_4357" n="e" s="T155">Kanaʔ! </ts>
               <ts e="T157" id="Seg_4359" n="e" s="T156">Ăgărottə </ts>
               <ts e="T158" id="Seg_4361" n="e" s="T157">deʔ </ts>
               <ts e="T159" id="Seg_4363" n="e" s="T158">ăgurcɨʔi. </ts>
               <ts e="T160" id="Seg_4365" n="e" s="T159">Măn </ts>
               <ts e="T161" id="Seg_4367" n="e" s="T160">(kambiam). </ts>
               <ts e="T162" id="Seg_4369" n="e" s="T161">Măndərbiam, </ts>
               <ts e="T163" id="Seg_4371" n="e" s="T162">măndərbiam. </ts>
               <ts e="T164" id="Seg_4373" n="e" s="T163">Nĭŋgəliem </ts>
               <ts e="T165" id="Seg_4375" n="e" s="T164">(toŋtanoʔ), </ts>
               <ts e="T166" id="Seg_4377" n="e" s="T165">išo </ts>
               <ts e="T167" id="Seg_4379" n="e" s="T166">ĭmbi-nʼibudʼ. </ts>
               <ts e="T168" id="Seg_4381" n="e" s="T167">Dĭgəttə </ts>
               <ts e="T169" id="Seg_4383" n="e" s="T168">(napal); </ts>
               <ts e="T170" id="Seg_4385" n="e" s="T169">šide </ts>
               <ts e="T171" id="Seg_4387" n="e" s="T170">(dɨ-) </ts>
               <ts e="T172" id="Seg_4389" n="e" s="T171">тыква </ts>
               <ts e="T173" id="Seg_4391" n="e" s="T172">ibiem </ts>
               <ts e="T174" id="Seg_4393" n="e" s="T173">i </ts>
               <ts e="T175" id="Seg_4395" n="e" s="T174">šobiam </ts>
               <ts e="T176" id="Seg_4397" n="e" s="T175">turanə. </ts>
               <ts e="T177" id="Seg_4399" n="e" s="T176">Onʼiʔ </ts>
               <ts e="T178" id="Seg_4401" n="e" s="T177">ne </ts>
               <ts e="T179" id="Seg_4403" n="e" s="T178">pătpollʼanə </ts>
               <ts e="T180" id="Seg_4405" n="e" s="T179">(š-) </ts>
               <ts e="T181" id="Seg_4407" n="e" s="T180">süʔmiluʔpi. </ts>
               <ts e="T182" id="Seg_4409" n="e" s="T181">Onʼiʔ </ts>
               <ts e="T183" id="Seg_4411" n="e" s="T182">(n-) </ts>
               <ts e="T184" id="Seg_4413" n="e" s="T183">ne </ts>
               <ts e="T185" id="Seg_4415" n="e" s="T184">nʼiʔtə. </ts>
               <ts e="T186" id="Seg_4417" n="e" s="T185">Aʔpi </ts>
               <ts e="T187" id="Seg_4419" n="e" s="T186">(min-) </ts>
               <ts e="T188" id="Seg_4421" n="e" s="T187">măna </ts>
               <ts e="T189" id="Seg_4423" n="e" s="T188">ej </ts>
               <ts e="T190" id="Seg_4425" n="e" s="T189">tonuʔpi. </ts>
               <ts e="T191" id="Seg_4427" n="e" s="T190">((BRK)). </ts>
               <ts e="T192" id="Seg_4429" n="e" s="T191">Dĭgəttə </ts>
               <ts e="T193" id="Seg_4431" n="e" s="T192">(dĭj-) </ts>
               <ts e="T194" id="Seg_4433" n="e" s="T193">dĭ </ts>
               <ts e="T195" id="Seg_4435" n="e" s="T194">nezen </ts>
               <ts e="T196" id="Seg_4437" n="e" s="T195">(aba=) </ts>
               <ts e="T197" id="Seg_4439" n="e" s="T196">abat </ts>
               <ts e="T198" id="Seg_4441" n="e" s="T197">măndə: </ts>
               <ts e="T199" id="Seg_4443" n="e" s="T198">Gibər </ts>
               <ts e="T200" id="Seg_4445" n="e" s="T199">šiʔ </ts>
               <ts e="T201" id="Seg_4447" n="e" s="T200">nuʔməlambilaʔ? </ts>
               <ts e="T202" id="Seg_4449" n="e" s="T201">Deʔkeʔ, </ts>
               <ts e="T203" id="Seg_4451" n="e" s="T202">a </ts>
               <ts e="T204" id="Seg_4453" n="e" s="T203">kămnəgaʔ </ts>
               <ts e="T205" id="Seg_4455" n="e" s="T204">mĭj. </ts>
               <ts e="T206" id="Seg_4457" n="e" s="T205">Abam,— </ts>
               <ts e="T207" id="Seg_4459" n="e" s="T206">măndə, </ts>
               <ts e="T208" id="Seg_4461" n="e" s="T207">kuiol </ts>
               <ts e="T209" id="Seg_4463" n="e" s="T208">dĭ </ts>
               <ts e="T210" id="Seg_4465" n="e" s="T209">šide </ts>
               <ts e="T211" id="Seg_4467" n="e" s="T210">dĭʔə </ts>
               <ts e="T212" id="Seg_4469" n="e" s="T211">(tɨkla-) </ts>
               <ts e="T213" id="Seg_4471" n="e" s="T212">тыква </ts>
               <ts e="T214" id="Seg_4473" n="e" s="T213">deʔpi. </ts>
               <ts e="T215" id="Seg_4475" n="e" s="T214">A </ts>
               <ts e="T216" id="Seg_4477" n="e" s="T215">ĭmbi </ts>
               <ts e="T217" id="Seg_4479" n="e" s="T216">öʔlieʔi, </ts>
               <ts e="T218" id="Seg_4481" n="e" s="T217">kuza </ts>
               <ts e="T219" id="Seg_4483" n="e" s="T218">ej </ts>
               <ts e="T220" id="Seg_4485" n="e" s="T219">tĭmnet, </ts>
               <ts e="T221" id="Seg_4487" n="e" s="T220">a </ts>
               <ts e="T222" id="Seg_4489" n="e" s="T221">šiʔ </ts>
               <ts e="T223" id="Seg_4491" n="e" s="T222">(öʔleʔil-) </ts>
               <ts e="T224" id="Seg_4493" n="e" s="T223">öʔleileʔ". </ts>
               <ts e="T225" id="Seg_4495" n="e" s="T224">((BRK)). </ts>
               <ts e="T226" id="Seg_4497" n="e" s="T225">Onʼiʔ </ts>
               <ts e="T227" id="Seg_4499" n="e" s="T226">ne </ts>
               <ts e="T228" id="Seg_4501" n="e" s="T227">Kazan </ts>
               <ts e="T229" id="Seg_4503" n="e" s="T228">turanə </ts>
               <ts e="T230" id="Seg_4505" n="e" s="T229">mĭmbi, </ts>
               <ts e="T231" id="Seg_4507" n="e" s="T230">(inettə-) </ts>
               <ts e="T232" id="Seg_4509" n="e" s="T231">inetsi. </ts>
               <ts e="T233" id="Seg_4511" n="e" s="T232">(Šo-) </ts>
               <ts e="T234" id="Seg_4513" n="e" s="T233">Šobi </ts>
               <ts e="T235" id="Seg_4515" n="e" s="T234">maʔndə. </ts>
               <ts e="T236" id="Seg_4517" n="e" s="T235">I </ts>
               <ts e="T237" id="Seg_4519" n="e" s="T236">kubi: </ts>
               <ts e="T238" id="Seg_4521" n="e" s="T237">pa </ts>
               <ts e="T239" id="Seg_4523" n="e" s="T238">iʔbolaʔbə. </ts>
               <ts e="T240" id="Seg_4525" n="e" s="T239">(Š-) </ts>
               <ts e="T241" id="Seg_4527" n="e" s="T240">Dĭgəttə </ts>
               <ts e="T242" id="Seg_4529" n="e" s="T241">măndə: </ts>
               <ts e="T243" id="Seg_4531" n="e" s="T242">dĭn </ts>
               <ts e="T244" id="Seg_4533" n="e" s="T243">červə </ts>
               <ts e="T245" id="Seg_4535" n="e" s="T244">bar </ts>
               <ts e="T246" id="Seg_4537" n="e" s="T245">ambi. </ts>
               <ts e="T247" id="Seg_4539" n="e" s="T246">(Dĭrg- </ts>
               <ts e="T248" id="Seg_4541" n="e" s="T247">dĭgət-) </ts>
               <ts e="T249" id="Seg_4543" n="e" s="T248">Dĭrgit </ts>
               <ts e="T250" id="Seg_4545" n="e" s="T249">paʔi </ts>
               <ts e="T251" id="Seg_4547" n="e" s="T250">možna </ts>
               <ts e="T252" id="Seg_4549" n="e" s="T251">toʔnarzittə </ts>
               <ts e="T253" id="Seg_4551" n="e" s="T252">i </ts>
               <ts e="T254" id="Seg_4553" n="e" s="T253">(paʔi </ts>
               <ts e="T255" id="Seg_4555" n="e" s="T254">boləj). </ts>
               <ts e="T256" id="Seg_4557" n="e" s="T255">((BRK)). </ts>
               <ts e="T257" id="Seg_4559" n="e" s="T256">Dĭrgit </ts>
               <ts e="T258" id="Seg_4561" n="e" s="T257">bar </ts>
               <ts e="T259" id="Seg_4563" n="e" s="T258">abi, </ts>
               <ts e="T260" id="Seg_4565" n="e" s="T259">üdʼüge, </ts>
               <ts e="T261" id="Seg_4567" n="e" s="T260">pʼeš </ts>
               <ts e="T262" id="Seg_4569" n="e" s="T261">možna </ts>
               <ts e="T263" id="Seg_4571" n="e" s="T262">nendəsʼtə. </ts>
               <ts e="T264" id="Seg_4573" n="e" s="T263">((BRK)). </ts>
               <ts e="T265" id="Seg_4575" n="e" s="T264">Dĭgəttə </ts>
               <ts e="T266" id="Seg_4577" n="e" s="T265">Varlam </ts>
               <ts e="T267" id="Seg_4579" n="e" s="T266">deʔpi </ts>
               <ts e="T268" id="Seg_4581" n="e" s="T267">kujdərgan. </ts>
               <ts e="T269" id="Seg_4583" n="e" s="T268">Nuldaʔ, </ts>
               <ts e="T270" id="Seg_4585" n="e" s="T269">(măndə-) </ts>
               <ts e="T271" id="Seg_4587" n="e" s="T270">măndə </ts>
               <ts e="T272" id="Seg_4589" n="e" s="T271">dĭʔnə. </ts>
               <ts e="T273" id="Seg_4591" n="e" s="T272">Dĭ </ts>
               <ts e="T274" id="Seg_4593" n="e" s="T273">kambi, </ts>
               <ts e="T275" id="Seg_4595" n="e" s="T274">bar </ts>
               <ts e="T276" id="Seg_4597" n="e" s="T275">trubagəndə </ts>
               <ts e="T277" id="Seg_4599" n="e" s="T276">bü </ts>
               <ts e="T278" id="Seg_4601" n="e" s="T277">embi, </ts>
               <ts e="T279" id="Seg_4603" n="e" s="T278">embi, </ts>
               <ts e="T280" id="Seg_4605" n="e" s="T279">kămnəbi, </ts>
               <ts e="T281" id="Seg_4607" n="e" s="T280">kămnəbi. </ts>
               <ts e="T282" id="Seg_4609" n="e" s="T281">I </ts>
               <ts e="T283" id="Seg_4611" n="e" s="T282">dĭbər </ts>
               <ts e="T284" id="Seg_4613" n="e" s="T283">(ko-) </ts>
               <ts e="T285" id="Seg_4615" n="e" s="T284">kös </ts>
               <ts e="T286" id="Seg_4617" n="e" s="T285">embi. </ts>
               <ts e="T287" id="Seg_4619" n="e" s="T286">Šobi </ts>
               <ts e="T288" id="Seg_4621" n="e" s="T287">turanə </ts>
               <ts e="T289" id="Seg_4623" n="e" s="T288">da </ts>
               <ts e="T290" id="Seg_4625" n="e" s="T289">măndə: </ts>
               <ts e="T291" id="Seg_4627" n="e" s="T290">Samovar </ts>
               <ts e="T292" id="Seg_4629" n="e" s="T291">mʼaŋnaʔbə </ts>
               <ts e="T293" id="Seg_4631" n="e" s="T292">(bar). </ts>
               <ts e="T294" id="Seg_4633" n="e" s="T293">Kujdərgan </ts>
               <ts e="T295" id="Seg_4635" n="e" s="T294">mʼaŋnaʔbə </ts>
               <ts e="T296" id="Seg_4637" n="e" s="T295">bar, </ts>
               <ts e="T297" id="Seg_4639" n="e" s="T296">bü. </ts>
               <ts e="T298" id="Seg_4641" n="e" s="T297">Dĭ </ts>
               <ts e="T299" id="Seg_4643" n="e" s="T298">kambi </ts>
               <ts e="T300" id="Seg_4645" n="e" s="T299">bar. </ts>
               <ts e="T301" id="Seg_4647" n="e" s="T300">Döbər, </ts>
               <ts e="T302" id="Seg_4649" n="e" s="T301">măndə, </ts>
               <ts e="T303" id="Seg_4651" n="e" s="T302">bü </ts>
               <ts e="T304" id="Seg_4653" n="e" s="T303">eneʔ, </ts>
               <ts e="T305" id="Seg_4655" n="e" s="T304">a </ts>
               <ts e="T306" id="Seg_4657" n="e" s="T305">döbər </ts>
               <ts e="T307" id="Seg_4659" n="e" s="T306">kös </ts>
               <ts e="T308" id="Seg_4661" n="e" s="T307">eneʔ. </ts>
               <ts e="T309" id="Seg_4663" n="e" s="T308">Dĭgəttə </ts>
               <ts e="T310" id="Seg_4665" n="e" s="T309">(dĭ </ts>
               <ts e="T311" id="Seg_4667" n="e" s="T310">didro) </ts>
               <ts e="T312" id="Seg_4669" n="e" s="T311">abi. </ts>
               <ts e="T313" id="Seg_4671" n="e" s="T312">((BRK)). </ts>
               <ts e="T314" id="Seg_4673" n="e" s="T313">Pa </ts>
               <ts e="T315" id="Seg_4675" n="e" s="T314">kambiʔi </ts>
               <ts e="T316" id="Seg_4677" n="e" s="T315">jaʔsittə, </ts>
               <ts e="T317" id="Seg_4679" n="e" s="T316">i </ts>
               <ts e="T318" id="Seg_4681" n="e" s="T317">nʼamga </ts>
               <ts e="T319" id="Seg_4683" n="e" s="T318">bü </ts>
               <ts e="T320" id="Seg_4685" n="e" s="T319">deʔpiʔi. </ts>
               <ts e="T321" id="Seg_4687" n="e" s="T320">(Pa-) </ts>
               <ts e="T322" id="Seg_4689" n="e" s="T321">Pagən </ts>
               <ts e="T323" id="Seg_4691" n="e" s="T322">nʼamga </ts>
               <ts e="T324" id="Seg_4693" n="e" s="T323">bü </ts>
               <ts e="T325" id="Seg_4695" n="e" s="T324">ibi. </ts>
               <ts e="T326" id="Seg_4697" n="e" s="T325">((BRK)). </ts>
               <ts e="T327" id="Seg_4699" n="e" s="T326">Sĭre </ts>
               <ts e="T328" id="Seg_4701" n="e" s="T327">pa, </ts>
               <ts e="T329" id="Seg_4703" n="e" s="T328">kojü </ts>
               <ts e="T330" id="Seg_4705" n="e" s="T329">pa. </ts>
               <ts e="T331" id="Seg_4707" n="e" s="T330">((BRK)). </ts>
               <ts e="T332" id="Seg_4709" n="e" s="T331">Šomi </ts>
               <ts e="T333" id="Seg_4711" n="e" s="T332">pa, </ts>
               <ts e="T334" id="Seg_4713" n="e" s="T333">san </ts>
               <ts e="T335" id="Seg_4715" n="e" s="T334">pa. </ts>
               <ts e="T336" id="Seg_4717" n="e" s="T335">((BRK)) </ts>
               <ts e="T337" id="Seg_4719" n="e" s="T336">говорила </ts>
               <ts e="T338" id="Seg_4721" n="e" s="T337">(lö-) </ts>
               <ts e="T339" id="Seg_4723" n="e" s="T338">(len) </ts>
               <ts e="T340" id="Seg_4725" n="e" s="T339">pa. </ts>
               <ts e="T341" id="Seg_4727" n="e" s="T340">((BRK)). </ts>
               <ts e="T342" id="Seg_4729" n="e" s="T341">Muʔjə </ts>
               <ts e="T343" id="Seg_4731" n="e" s="T342">ugandə </ts>
               <ts e="T344" id="Seg_4733" n="e" s="T343">iʔgö </ts>
               <ts e="T345" id="Seg_4735" n="e" s="T344">dʼijegən. </ts>
               <ts e="T346" id="Seg_4737" n="e" s="T345">Kalal </ts>
               <ts e="T347" id="Seg_4739" n="e" s="T346">dăk </ts>
               <ts e="T348" id="Seg_4741" n="e" s="T347">bar, </ts>
               <ts e="T349" id="Seg_4743" n="e" s="T348">üge </ts>
               <ts e="T350" id="Seg_4745" n="e" s="T349">üjüzi </ts>
               <ts e="T351" id="Seg_4747" n="e" s="T350">tonuʔlal. </ts>
               <ts e="T352" id="Seg_4749" n="e" s="T351">((BRK)). </ts>
               <ts e="T353" id="Seg_4751" n="e" s="T352">Dărowă </ts>
               <ts e="T354" id="Seg_4753" n="e" s="T353">igel. </ts>
               <ts e="T355" id="Seg_4755" n="e" s="T354">Jakše </ts>
               <ts e="T356" id="Seg_4757" n="e" s="T355">erte! </ts>
               <ts e="T357" id="Seg_4759" n="e" s="T356">Jakše </ts>
               <ts e="T358" id="Seg_4761" n="e" s="T357">ertezi </ts>
               <ts e="T359" id="Seg_4763" n="e" s="T358">i </ts>
               <ts e="T360" id="Seg_4765" n="e" s="T359">веселый </ts>
               <ts e="T361" id="Seg_4767" n="e" s="T360">dʼala </ts>
               <ts e="T362" id="Seg_4769" n="e" s="T361">tănan. </ts>
               <ts e="T363" id="Seg_4771" n="e" s="T362">((BRK)). </ts>
               <ts e="T364" id="Seg_4773" n="e" s="T363">Kăde </ts>
               <ts e="T365" id="Seg_4775" n="e" s="T364">kunolbial </ts>
               <ts e="T366" id="Seg_4777" n="e" s="T365">teinen? </ts>
               <ts e="T367" id="Seg_4779" n="e" s="T366">Moltʼagən </ts>
               <ts e="T368" id="Seg_4781" n="e" s="T367">băzəbial, </ts>
               <ts e="T369" id="Seg_4783" n="e" s="T368">padʼi </ts>
               <ts e="T370" id="Seg_4785" n="e" s="T369">tăŋ </ts>
               <ts e="T371" id="Seg_4787" n="e" s="T370">kunolbial. </ts>
               <ts e="T372" id="Seg_4789" n="e" s="T371">Da </ts>
               <ts e="T373" id="Seg_4791" n="e" s="T372">(kunolbialəj- </ts>
               <ts e="T374" id="Seg_4793" n="e" s="T373">kulonolbiam-), </ts>
               <ts e="T375" id="Seg_4795" n="e" s="T374">jakše </ts>
               <ts e="T376" id="Seg_4797" n="e" s="T375">băzəbiam. </ts>
               <ts e="T377" id="Seg_4799" n="e" s="T376">Bü </ts>
               <ts e="T378" id="Seg_4801" n="e" s="T377">kabarbi. </ts>
               <ts e="T379" id="Seg_4803" n="e" s="T378">Dʼibige </ts>
               <ts e="T380" id="Seg_4805" n="e" s="T379">bübə </ts>
               <ts e="T381" id="Seg_4807" n="e" s="T380">ibi, </ts>
               <ts e="T382" id="Seg_4809" n="e" s="T381">i </ts>
               <ts e="T383" id="Seg_4811" n="e" s="T382">šišəge. </ts>
               <ts e="T384" id="Seg_4813" n="e" s="T383">Ejü </ts>
               <ts e="T385" id="Seg_4815" n="e" s="T384">mobi </ts>
               <ts e="T386" id="Seg_4817" n="e" s="T385">ugandə, </ts>
               <ts e="T387" id="Seg_4819" n="e" s="T386">(tăŋ) </ts>
               <ts e="T388" id="Seg_4821" n="e" s="T387">ejü. </ts>
               <ts e="T389" id="Seg_4823" n="e" s="T388">((BRK)). </ts>
               <ts e="T390" id="Seg_4825" n="e" s="T389">Măn </ts>
               <ts e="T391" id="Seg_4827" n="e" s="T390">moltʼagən </ts>
               <ts e="T392" id="Seg_4829" n="e" s="T391">băzəbiam, </ts>
               <ts e="T393" id="Seg_4831" n="e" s="T392">bar </ts>
               <ts e="T394" id="Seg_4833" n="e" s="T393">bü </ts>
               <ts e="T395" id="Seg_4835" n="e" s="T394">kămnəbiam. </ts>
               <ts e="T396" id="Seg_4837" n="e" s="T395">Dĭgəttə </ts>
               <ts e="T398" id="Seg_4839" n="e" s="T396">šobi… </ts>
               <ts e="T399" id="Seg_4841" n="e" s="T398">((BRK)) </ts>
               <ts e="T400" id="Seg_4843" n="e" s="T399">накрутил </ts>
               <ts e="T401" id="Seg_4845" n="e" s="T400">там </ts>
               <ts e="T402" id="Seg_4847" n="e" s="T401">на </ts>
               <ts e="T403" id="Seg_4849" n="e" s="T402">это </ts>
               <ts e="T404" id="Seg_4851" n="e" s="T403">((BRK)). </ts>
               <ts e="T405" id="Seg_4853" n="e" s="T404">Dĭgəttə </ts>
               <ts e="T406" id="Seg_4855" n="e" s="T405">iet </ts>
               <ts e="T407" id="Seg_4857" n="e" s="T406">šobi. </ts>
               <ts e="T408" id="Seg_4859" n="e" s="T407">Bü </ts>
               <ts e="T409" id="Seg_4861" n="e" s="T408">naga, </ts>
               <ts e="T410" id="Seg_4863" n="e" s="T409">a </ts>
               <ts e="T411" id="Seg_4865" n="e" s="T410">măn </ts>
               <ts e="T412" id="Seg_4867" n="e" s="T411">detlem </ts>
               <ts e="T413" id="Seg_4869" n="e" s="T412">da </ts>
               <ts e="T414" id="Seg_4871" n="e" s="T413">băzəjdlam. </ts>
               <ts e="T415" id="Seg_4873" n="e" s="T414">A </ts>
               <ts e="T416" id="Seg_4875" n="e" s="T415">măn </ts>
               <ts e="T986" id="Seg_4877" n="e" s="T416">kalla </ts>
               <ts e="T417" id="Seg_4879" n="e" s="T986">dʼürbim, </ts>
               <ts e="T418" id="Seg_4881" n="e" s="T417">dĭ </ts>
               <ts e="T419" id="Seg_4883" n="e" s="T418">bar </ts>
               <ts e="T420" id="Seg_4885" n="e" s="T419">băzəjdəbi </ts>
               <ts e="T421" id="Seg_4887" n="e" s="T420">i </ts>
               <ts e="T422" id="Seg_4889" n="e" s="T421">maʔndə </ts>
               <ts e="T423" id="Seg_4891" n="e" s="T422">šobi. </ts>
               <ts e="T424" id="Seg_4893" n="e" s="T423">((BRK)). </ts>
               <ts e="T425" id="Seg_4895" n="e" s="T424">Măn </ts>
               <ts e="T426" id="Seg_4897" n="e" s="T425">(sʼera-) </ts>
               <ts e="T427" id="Seg_4899" n="e" s="T426">sʼestram </ts>
               <ts e="T428" id="Seg_4901" n="e" s="T427">nʼit </ts>
               <ts e="T429" id="Seg_4903" n="e" s="T428">mĭmbi </ts>
               <ts e="T430" id="Seg_4905" n="e" s="T429">gorăttə. </ts>
               <ts e="T431" id="Seg_4907" n="e" s="T430">Dĭn </ts>
               <ts e="T432" id="Seg_4909" n="e" s="T431">mašina </ts>
               <ts e="T433" id="Seg_4911" n="e" s="T432">ibi, </ts>
               <ts e="T434" id="Seg_4913" n="e" s="T433">kujnegəʔi </ts>
               <ts e="T435" id="Seg_4915" n="e" s="T434">(băzəsʼtə), </ts>
               <ts e="T436" id="Seg_4917" n="e" s="T435">dĭgəttə </ts>
               <ts e="T437" id="Seg_4919" n="e" s="T436">deʔpi </ts>
               <ts e="T438" id="Seg_4921" n="e" s="T437">(maʔndə). </ts>
               <ts e="T439" id="Seg_4923" n="e" s="T438">A </ts>
               <ts e="T440" id="Seg_4925" n="e" s="T439">teinen </ts>
               <ts e="T441" id="Seg_4927" n="e" s="T440">kujnegəʔi </ts>
               <ts e="T442" id="Seg_4929" n="e" s="T441">bar </ts>
               <ts e="T443" id="Seg_4931" n="e" s="T442">băzəbiʔi </ts>
               <ts e="T444" id="Seg_4933" n="e" s="T443">i </ts>
               <ts e="T445" id="Seg_4935" n="e" s="T444">edəbiʔi. </ts>
               <ts e="T446" id="Seg_4937" n="e" s="T445">((BRK)). </ts>
               <ts e="T447" id="Seg_4939" n="e" s="T446">Măn </ts>
               <ts e="T448" id="Seg_4941" n="e" s="T447">sʼestranə </ts>
               <ts e="T449" id="Seg_4943" n="e" s="T448">kazak </ts>
               <ts e="T450" id="Seg_4945" n="e" s="T449">iat </ts>
               <ts e="T451" id="Seg_4947" n="e" s="T450">šobi. </ts>
               <ts e="T452" id="Seg_4949" n="e" s="T451">Ajaʔ </ts>
               <ts e="T453" id="Seg_4951" n="e" s="T452">deʔpi </ts>
               <ts e="T454" id="Seg_4953" n="e" s="T453">(tibi </ts>
               <ts e="T455" id="Seg_4955" n="e" s="T454">i </ts>
               <ts e="T456" id="Seg_4957" n="e" s="T455">ne). </ts>
               <ts e="T457" id="Seg_4959" n="e" s="T456">Dĭgəttə </ts>
               <ts e="T458" id="Seg_4961" n="e" s="T457">(ia=) </ts>
               <ts e="T459" id="Seg_4963" n="e" s="T458">măn </ts>
               <ts e="T460" id="Seg_4965" n="e" s="T459">ianə </ts>
               <ts e="T461" id="Seg_4967" n="e" s="T460">măndə: </ts>
               <ts e="T462" id="Seg_4969" n="e" s="T461">Kangaʔ, </ts>
               <ts e="T463" id="Seg_4971" n="e" s="T462">keʔbde </ts>
               <ts e="T464" id="Seg_4973" n="e" s="T463">ileʔ, </ts>
               <ts e="T465" id="Seg_4975" n="e" s="T464">малина </ts>
               <ts e="T466" id="Seg_4977" n="e" s="T465">ileʔ. </ts>
               <ts e="T467" id="Seg_4979" n="e" s="T466">Dĭzeŋ </ts>
               <ts e="T468" id="Seg_4981" n="e" s="T467">kambiʔi. </ts>
               <ts e="T469" id="Seg_4983" n="e" s="T468">Dĭ </ts>
               <ts e="T470" id="Seg_4985" n="e" s="T469">ara </ts>
               <ts e="T471" id="Seg_4987" n="e" s="T470">mĭbi </ts>
               <ts e="T472" id="Seg_4989" n="e" s="T471">(onʼiʔ=) </ts>
               <ts e="T473" id="Seg_4991" n="e" s="T472">onʼiʔ </ts>
               <ts e="T474" id="Seg_4993" n="e" s="T473">бутылка </ts>
               <ts e="T475" id="Seg_4995" n="e" s="T474">dĭzeŋ. </ts>
               <ts e="T476" id="Seg_4997" n="e" s="T475">Kuʔpi </ts>
               <ts e="T477" id="Seg_4999" n="e" s="T476">(ta-) </ts>
               <ts e="T478" id="Seg_5001" n="e" s="T477">dĭn </ts>
               <ts e="T479" id="Seg_5003" n="e" s="T478">keʔbde. </ts>
               <ts e="T480" id="Seg_5005" n="e" s="T479">Dĭzeŋ </ts>
               <ts e="T481" id="Seg_5007" n="e" s="T480">(nam-) </ts>
               <ts e="T482" id="Seg_5009" n="e" s="T481">ibiʔi, </ts>
               <ts e="T483" id="Seg_5011" n="e" s="T482">ibiʔi, </ts>
               <ts e="T484" id="Seg_5013" n="e" s="T483">kon </ts>
               <ts e="T485" id="Seg_5015" n="e" s="T484">ibiʔi. </ts>
               <ts e="T486" id="Seg_5017" n="e" s="T485">Dĭgəttə </ts>
               <ts e="T487" id="Seg_5019" n="e" s="T486">amnobiʔi </ts>
               <ts e="T488" id="Seg_5021" n="e" s="T487">amorzittə. </ts>
               <ts e="T489" id="Seg_5023" n="e" s="T488">A </ts>
               <ts e="T490" id="Seg_5025" n="e" s="T489">măn </ts>
               <ts e="T491" id="Seg_5027" n="e" s="T490">iam </ts>
               <ts e="T492" id="Seg_5029" n="e" s="T491">(bə-) </ts>
               <ts e="T493" id="Seg_5031" n="e" s="T492">măndə: </ts>
               <ts e="T494" id="Seg_5033" n="e" s="T493">Davaj </ts>
               <ts e="T496" id="Seg_5035" n="e" s="T494">döʔə… </ts>
               <ts e="T497" id="Seg_5037" n="e" s="T496">Nada </ts>
               <ts e="T498" id="Seg_5039" n="e" s="T497">keʔbde </ts>
               <ts e="T499" id="Seg_5041" n="e" s="T498">(koʔsittə) </ts>
               <ts e="T500" id="Seg_5043" n="e" s="T499">dĭ </ts>
               <ts e="T501" id="Seg_5045" n="e" s="T500">arazi. </ts>
               <ts e="T502" id="Seg_5047" n="e" s="T501">Dĭgəttə </ts>
               <ts e="T503" id="Seg_5049" n="e" s="T502">kămnəbiʔi, </ts>
               <ts e="T504" id="Seg_5051" n="e" s="T503">bostə </ts>
               <ts e="T505" id="Seg_5053" n="e" s="T504">bĭʔpiʔi. </ts>
               <ts e="T506" id="Seg_5055" n="e" s="T505">Dĭgəttə </ts>
               <ts e="T507" id="Seg_5057" n="e" s="T506">šo </ts>
               <ts e="T508" id="Seg_5059" n="e" s="T507">oʔbdəbiʔi, </ts>
               <ts e="T509" id="Seg_5061" n="e" s="T508">iʔgö </ts>
               <ts e="T510" id="Seg_5063" n="e" s="T509">deʔpiʔi. </ts>
               <ts e="T511" id="Seg_5065" n="e" s="T510">Šobiʔi </ts>
               <ts e="T512" id="Seg_5067" n="e" s="T511">maʔndə </ts>
               <ts e="T513" id="Seg_5069" n="e" s="T512">dĭbər. </ts>
               <ts e="T514" id="Seg_5071" n="e" s="T513">Döm </ts>
               <ts e="T515" id="Seg_5073" n="e" s="T514">nʼilgölia </ts>
               <ts e="T516" id="Seg_5075" n="e" s="T515">püjetsi, </ts>
               <ts e="T517" id="Seg_5077" n="e" s="T516">döm </ts>
               <ts e="T518" id="Seg_5079" n="e" s="T517">nʼilgölia </ts>
               <ts e="T519" id="Seg_5081" n="e" s="T518">püjetsi. </ts>
               <ts e="T520" id="Seg_5083" n="e" s="T519">Putʼəmnia </ts>
               <ts e="T521" id="Seg_5085" n="e" s="T520">arazi! </ts>
               <ts e="T522" id="Seg_5087" n="e" s="T521">Ej </ts>
               <ts e="T523" id="Seg_5089" n="e" s="T522">tĭmnebi. </ts>
               <ts e="T524" id="Seg_5091" n="e" s="T523">((BRK)). </ts>
               <ts e="T525" id="Seg_5093" n="e" s="T524">Măn </ts>
               <ts e="T526" id="Seg_5095" n="e" s="T525">(təni-) </ts>
               <ts e="T527" id="Seg_5097" n="e" s="T526">teinen </ts>
               <ts e="T528" id="Seg_5099" n="e" s="T527">ertə </ts>
               <ts e="T529" id="Seg_5101" n="e" s="T528">uʔbdəbiam. </ts>
               <ts e="T530" id="Seg_5103" n="e" s="T529">Pʼeš </ts>
               <ts e="T531" id="Seg_5105" n="e" s="T530">nendəbiem. </ts>
               <ts e="T532" id="Seg_5107" n="e" s="T531">Dĭgəttə </ts>
               <ts e="T533" id="Seg_5109" n="e" s="T532">tüžöjdə </ts>
               <ts e="T534" id="Seg_5111" n="e" s="T533">noʔ </ts>
               <ts e="T535" id="Seg_5113" n="e" s="T534">mĭbiam. </ts>
               <ts e="T536" id="Seg_5115" n="e" s="T535">Toltanoʔ </ts>
               <ts e="T537" id="Seg_5117" n="e" s="T536">bĭʔpiam. </ts>
               <ts e="T538" id="Seg_5119" n="e" s="T537">Kambiam, </ts>
               <ts e="T539" id="Seg_5121" n="e" s="T538">tüžöjdə </ts>
               <ts e="T540" id="Seg_5123" n="e" s="T539">surdəbiam. </ts>
               <ts e="T541" id="Seg_5125" n="e" s="T540">Süttə </ts>
               <ts e="T542" id="Seg_5127" n="e" s="T541">kămnəbiam. </ts>
               <ts e="T543" id="Seg_5129" n="e" s="T542">A </ts>
               <ts e="T544" id="Seg_5131" n="e" s="T543">dĭgəttə </ts>
               <ts e="T545" id="Seg_5133" n="e" s="T544">kambiam </ts>
               <ts e="T546" id="Seg_5135" n="e" s="T545">nükenə, </ts>
               <ts e="T547" id="Seg_5137" n="e" s="T546">mĭj </ts>
               <ts e="T548" id="Seg_5139" n="e" s="T547">(ka- </ts>
               <ts e="T549" id="Seg_5141" n="e" s="T548">)… </ts>
               <ts e="T550" id="Seg_5143" n="e" s="T549">Mĭj </ts>
               <ts e="T551" id="Seg_5145" n="e" s="T550">mĭnzerbiem. </ts>
               <ts e="T552" id="Seg_5147" n="e" s="T551">Dĭgəttə </ts>
               <ts e="T553" id="Seg_5149" n="e" s="T552">kambiam </ts>
               <ts e="T554" id="Seg_5151" n="e" s="T553">nükenə, </ts>
               <ts e="T555" id="Seg_5153" n="e" s="T554">(mĭj=) </ts>
               <ts e="T556" id="Seg_5155" n="e" s="T555">mĭj </ts>
               <ts e="T557" id="Seg_5157" n="e" s="T556">deʔpiem. </ts>
               <ts e="T558" id="Seg_5159" n="e" s="T557">Dĭ </ts>
               <ts e="T559" id="Seg_5161" n="e" s="T558">bar </ts>
               <ts e="T560" id="Seg_5163" n="e" s="T559">(mĭn-) </ts>
               <ts e="T561" id="Seg_5165" n="e" s="T560">mĭlleʔbə </ts>
               <ts e="T562" id="Seg_5167" n="e" s="T561">(üjügən). </ts>
               <ts e="T563" id="Seg_5169" n="e" s="T562">Ĭmbi, </ts>
               <ts e="T564" id="Seg_5171" n="e" s="T563">tural </ts>
               <ts e="T565" id="Seg_5173" n="e" s="T564">băzəbi? </ts>
               <ts e="T566" id="Seg_5175" n="e" s="T565">Dʼok, </ts>
               <ts e="T567" id="Seg_5177" n="e" s="T566">esseŋ </ts>
               <ts e="T568" id="Seg_5179" n="e" s="T567">băzəbiʔi. </ts>
               <ts e="T569" id="Seg_5181" n="e" s="T568">Dĭgəttə </ts>
               <ts e="T570" id="Seg_5183" n="e" s="T569">šobi </ts>
               <ts e="T571" id="Seg_5185" n="e" s="T570">döbər, </ts>
               <ts e="T572" id="Seg_5187" n="e" s="T571">krăvatʼtʼə </ts>
               <ts e="T573" id="Seg_5189" n="e" s="T572">bar </ts>
               <ts e="T574" id="Seg_5191" n="e" s="T573">(saʔməluʔpi). </ts>
               <ts e="T575" id="Seg_5193" n="e" s="T574">Măn </ts>
               <ts e="T576" id="Seg_5195" n="e" s="T575">măndəm: </ts>
               <ts e="T577" id="Seg_5197" n="e" s="T576">Iʔ </ts>
               <ts e="T578" id="Seg_5199" n="e" s="T577">saʔmaʔ. </ts>
               <ts e="T580" id="Seg_5201" n="e" s="T578">Погоди… </ts>
               <ts e="T581" id="Seg_5203" n="e" s="T580">A </ts>
               <ts e="T582" id="Seg_5205" n="e" s="T581">mĭjdə </ts>
               <ts e="T583" id="Seg_5207" n="e" s="T582">bar </ts>
               <ts e="T584" id="Seg_5209" n="e" s="T583">(săgəluʔpi). </ts>
               <ts e="T585" id="Seg_5211" n="e" s="T584">Büžü </ts>
               <ts e="T586" id="Seg_5213" n="e" s="T585">ej </ts>
               <ts e="T587" id="Seg_5215" n="e" s="T586">kuləj. </ts>
               <ts e="T588" id="Seg_5217" n="e" s="T587">((BRK)). </ts>
               <ts e="T589" id="Seg_5219" n="e" s="T588">Dĭgəttə </ts>
               <ts e="T590" id="Seg_5221" n="e" s="T589">dĭ </ts>
               <ts e="T591" id="Seg_5223" n="e" s="T590">nükebə </ts>
               <ts e="T592" id="Seg_5225" n="e" s="T591">(nuʔgĭ-) </ts>
               <ts e="T593" id="Seg_5227" n="e" s="T592">kambiam </ts>
               <ts e="T594" id="Seg_5229" n="e" s="T593">Anissʼanə. </ts>
               <ts e="T595" id="Seg_5231" n="e" s="T594">Kundʼo </ts>
               <ts e="T596" id="Seg_5233" n="e" s="T595">ej </ts>
               <ts e="T597" id="Seg_5235" n="e" s="T596">mĭmbiem. </ts>
               <ts e="T598" id="Seg_5237" n="e" s="T597">Šobiam </ts>
               <ts e="T599" id="Seg_5239" n="e" s="T598">(dibər=) </ts>
               <ts e="T600" id="Seg_5241" n="e" s="T599">dĭʔnə. </ts>
               <ts e="T601" id="Seg_5243" n="e" s="T600">Dĭzeŋ </ts>
               <ts e="T602" id="Seg_5245" n="e" s="T601">bar </ts>
               <ts e="T603" id="Seg_5247" n="e" s="T602">dibər </ts>
               <ts e="T604" id="Seg_5249" n="e" s="T603">nʼitsi </ts>
               <ts e="T605" id="Seg_5251" n="e" s="T604">kudonzlaʔbə. </ts>
               <ts e="T606" id="Seg_5253" n="e" s="T605">((BRK)). </ts>
               <ts e="T607" id="Seg_5255" n="e" s="T606">Dĭgəttə </ts>
               <ts e="T608" id="Seg_5257" n="e" s="T607">dĭ </ts>
               <ts e="T609" id="Seg_5259" n="e" s="T608">Anissʼa </ts>
               <ts e="T610" id="Seg_5261" n="e" s="T609">bar </ts>
               <ts e="T611" id="Seg_5263" n="e" s="T610">kudonzəbi </ts>
               <ts e="T612" id="Seg_5265" n="e" s="T611">(n-) </ts>
               <ts e="T613" id="Seg_5267" n="e" s="T612">nʼit. </ts>
               <ts e="T614" id="Seg_5269" n="e" s="T613">A </ts>
               <ts e="T615" id="Seg_5271" n="e" s="T614">nʼit </ts>
               <ts e="T616" id="Seg_5273" n="e" s="T615">bar </ts>
               <ts e="T617" id="Seg_5275" n="e" s="T616">dĭʔnə </ts>
               <ts e="T618" id="Seg_5277" n="e" s="T617">ĭmbi-nʼibudʼ </ts>
               <ts e="T619" id="Seg_5279" n="e" s="T618">dʼăbaktərləj. </ts>
               <ts e="T620" id="Seg_5281" n="e" s="T619">Măn </ts>
               <ts e="T621" id="Seg_5283" n="e" s="T620">măndəm: </ts>
               <ts e="T622" id="Seg_5285" n="e" s="T621">Iʔ </ts>
               <ts e="T623" id="Seg_5287" n="e" s="T622">kürümaʔ. </ts>
               <ts e="T624" id="Seg_5289" n="e" s="T623">(Ia-) </ts>
               <ts e="T625" id="Seg_5291" n="e" s="T624">Ian </ts>
               <ts e="T626" id="Seg_5293" n="e" s="T625">ĭmbidə </ts>
               <ts e="T627" id="Seg_5295" n="e" s="T626">ej </ts>
               <ts e="T628" id="Seg_5297" n="e" s="T627">(mănaʔ). </ts>
               <ts e="T629" id="Seg_5299" n="e" s="T628">A </ts>
               <ts e="T630" id="Seg_5301" n="e" s="T629">dĭ </ts>
               <ts e="T631" id="Seg_5303" n="e" s="T630">üge </ts>
               <ts e="T632" id="Seg_5305" n="e" s="T631">kürümnie. </ts>
               <ts e="T633" id="Seg_5307" n="e" s="T632">A </ts>
               <ts e="T634" id="Seg_5309" n="e" s="T633">iat </ts>
               <ts e="T635" id="Seg_5311" n="e" s="T634">bar </ts>
               <ts e="T636" id="Seg_5313" n="e" s="T635">davaj </ts>
               <ts e="T637" id="Seg_5315" n="e" s="T636">dʼorzittə. </ts>
               <ts e="T638" id="Seg_5317" n="e" s="T637">Kuiol, </ts>
               <ts e="T639" id="Seg_5319" n="e" s="T638">kăde </ts>
               <ts e="T640" id="Seg_5321" n="e" s="T639">dĭzeŋ </ts>
               <ts e="T641" id="Seg_5323" n="e" s="T640">(ipeksi), </ts>
               <ts e="T642" id="Seg_5325" n="e" s="T641">măna </ts>
               <ts e="T643" id="Seg_5327" n="e" s="T642">(bădləʔi). </ts>
               <ts e="T644" id="Seg_5329" n="e" s="T643">Măn </ts>
               <ts e="T645" id="Seg_5331" n="e" s="T644">ipek </ts>
               <ts e="T646" id="Seg_5333" n="e" s="T645">amnia, </ts>
               <ts e="T647" id="Seg_5335" n="e" s="T646">i </ts>
               <ts e="T648" id="Seg_5337" n="e" s="T647">išo </ts>
               <ts e="T649" id="Seg_5339" n="e" s="T648">kudonzlaʔbə. </ts>
               <ts e="T650" id="Seg_5341" n="e" s="T649">Dĭgəttə </ts>
               <ts e="T651" id="Seg_5343" n="e" s="T650">(šalaʔ-) </ts>
               <ts e="T652" id="Seg_5345" n="e" s="T651">šala </ts>
               <ts e="T653" id="Seg_5347" n="e" s="T652">amnobibaʔ, </ts>
               <ts e="T654" id="Seg_5349" n="e" s="T653">dĭgəttə </ts>
               <ts e="T655" id="Seg_5351" n="e" s="T654">Jelʼa </ts>
               <ts e="T656" id="Seg_5353" n="e" s="T655">šobi. </ts>
               <ts e="T657" id="Seg_5355" n="e" s="T656">Zdărowă </ts>
               <ts e="T658" id="Seg_5357" n="e" s="T657">igel— </ts>
               <ts e="T659" id="Seg_5359" n="e" s="T658">Zdărowă </ts>
               <ts e="T660" id="Seg_5361" n="e" s="T659">igel. </ts>
               <ts e="T661" id="Seg_5363" n="e" s="T660">Ĭmbi </ts>
               <ts e="T662" id="Seg_5365" n="e" s="T661">šobial? </ts>
               <ts e="T663" id="Seg_5367" n="e" s="T662">Ipek </ts>
               <ts e="T664" id="Seg_5369" n="e" s="T663">măndərbiam. </ts>
               <ts e="T665" id="Seg_5371" n="e" s="T664">A </ts>
               <ts e="T666" id="Seg_5373" n="e" s="T665">măn </ts>
               <ts e="T667" id="Seg_5375" n="e" s="T666">Anissʼanə </ts>
               <ts e="T668" id="Seg_5377" n="e" s="T667">mălliam: </ts>
               <ts e="T669" id="Seg_5379" n="e" s="T668">Mĭʔ </ts>
               <ts e="T670" id="Seg_5381" n="e" s="T669">dĭʔnə </ts>
               <ts e="T671" id="Seg_5383" n="e" s="T670">onʼiʔ </ts>
               <ts e="T672" id="Seg_5385" n="e" s="T671">ipek. </ts>
               <ts e="T673" id="Seg_5387" n="e" s="T672">Dĭ </ts>
               <ts e="T674" id="Seg_5389" n="e" s="T673">mĭbi </ts>
               <ts e="T675" id="Seg_5391" n="e" s="T674">dĭʔnə. </ts>
               <ts e="T676" id="Seg_5393" n="e" s="T675">Kumen </ts>
               <ts e="T677" id="Seg_5395" n="e" s="T676">pizittə? </ts>
               <ts e="T678" id="Seg_5397" n="e" s="T677">A </ts>
               <ts e="T679" id="Seg_5399" n="e" s="T678">măn </ts>
               <ts e="T680" id="Seg_5401" n="e" s="T679">măndəm: </ts>
               <ts e="T681" id="Seg_5403" n="e" s="T680">(Šalaʔ) </ts>
               <ts e="T682" id="Seg_5405" n="e" s="T681">ej </ts>
               <ts e="T683" id="Seg_5407" n="e" s="T682">iʔgö </ts>
               <ts e="T684" id="Seg_5409" n="e" s="T683">kak </ts>
               <ts e="T685" id="Seg_5411" n="e" s="T684">(măgăzingəʔ). </ts>
               <ts e="T686" id="Seg_5413" n="e" s="T685">Dĭgəttə </ts>
               <ts e="T687" id="Seg_5415" n="e" s="T686">(dĭn=) </ts>
               <ts e="T688" id="Seg_5417" n="e" s="T687">dĭ </ts>
               <ts e="T689" id="Seg_5419" n="e" s="T688">dĭʔnə </ts>
               <ts e="T690" id="Seg_5421" n="e" s="T689">mĭbi </ts>
               <ts e="T691" id="Seg_5423" n="e" s="T690">teʔtə </ts>
               <ts e="T692" id="Seg_5425" n="e" s="T691">bieʔ </ts>
               <ts e="T693" id="Seg_5427" n="e" s="T692">kăpʼejkaʔi. </ts>
               <ts e="T694" id="Seg_5429" n="e" s="T693">((BRK)). </ts>
               <ts e="T695" id="Seg_5431" n="e" s="T694">Dĭgəttə </ts>
               <ts e="T696" id="Seg_5433" n="e" s="T695">măn </ts>
               <ts e="T697" id="Seg_5435" n="e" s="T696">šobiam. </ts>
               <ts e="T698" id="Seg_5437" n="e" s="T697">Tüžöjbə </ts>
               <ts e="T699" id="Seg_5439" n="e" s="T698">(bĭtəjbiem), </ts>
               <ts e="T700" id="Seg_5441" n="e" s="T699">buzo </ts>
               <ts e="T701" id="Seg_5443" n="e" s="T700">bĭtəlbiem, </ts>
               <ts e="T702" id="Seg_5445" n="e" s="T701">noʔ </ts>
               <ts e="T703" id="Seg_5447" n="e" s="T702">mĭbiem. </ts>
               <ts e="T704" id="Seg_5449" n="e" s="T703">Dĭgəttə </ts>
               <ts e="T705" id="Seg_5451" n="e" s="T704">büjle </ts>
               <ts e="T706" id="Seg_5453" n="e" s="T705">mĭmbiem. </ts>
               <ts e="T707" id="Seg_5455" n="e" s="T706">I </ts>
               <ts e="T708" id="Seg_5457" n="e" s="T707">(š-) </ts>
               <ts e="T709" id="Seg_5459" n="e" s="T708">šobiam </ts>
               <ts e="T710" id="Seg_5461" n="e" s="T709">turanə, </ts>
               <ts e="T711" id="Seg_5463" n="e" s="T710">ujum </ts>
               <ts e="T712" id="Seg_5465" n="e" s="T711">(kănnambi). </ts>
               <ts e="T713" id="Seg_5467" n="e" s="T712">Pʼeštə </ts>
               <ts e="T714" id="Seg_5469" n="e" s="T713">(š- </ts>
               <ts e="T715" id="Seg_5471" n="e" s="T714">s-) </ts>
               <ts e="T716" id="Seg_5473" n="e" s="T715">(sʼabiam). </ts>
               <ts e="T717" id="Seg_5475" n="e" s="T716">Pʼeštə </ts>
               <ts e="T718" id="Seg_5477" n="e" s="T717">(s-) </ts>
               <ts e="T719" id="Seg_5479" n="e" s="T718">(sʼabiam). </ts>
               <ts e="T720" id="Seg_5481" n="e" s="T719">Dĭgəttə </ts>
               <ts e="T721" id="Seg_5483" n="e" s="T720">ujum </ts>
               <ts e="T722" id="Seg_5485" n="e" s="T721">(ejüm-) </ts>
               <ts e="T723" id="Seg_5487" n="e" s="T722">(ojimulem). </ts>
               <ts e="T724" id="Seg_5489" n="e" s="T723">Dĭzeŋ </ts>
               <ts e="T725" id="Seg_5491" n="e" s="T724">(so-) </ts>
               <ts e="T726" id="Seg_5493" n="e" s="T725">šobiʔi </ts>
               <ts e="T727" id="Seg_5495" n="e" s="T726">i </ts>
               <ts e="T728" id="Seg_5497" n="e" s="T727">dʼăbaktərzittə. </ts>
               <ts e="T729" id="Seg_5499" n="e" s="T728">((BRK)). </ts>
               <ts e="T730" id="Seg_5501" n="e" s="T729">Dĭgəttə </ts>
               <ts e="T731" id="Seg_5503" n="e" s="T730">dʼăbaktərzittə </ts>
               <ts e="T732" id="Seg_5505" n="e" s="T731">nada, </ts>
               <ts e="T733" id="Seg_5507" n="e" s="T732">a </ts>
               <ts e="T734" id="Seg_5509" n="e" s="T733">dĭn </ts>
               <ts e="T735" id="Seg_5511" n="e" s="T734">mašina </ts>
               <ts e="T736" id="Seg_5513" n="e" s="T735">bar </ts>
               <ts e="T737" id="Seg_5515" n="e" s="T736">kürümnaʔbə. </ts>
               <ts e="T738" id="Seg_5517" n="e" s="T737">Ej </ts>
               <ts e="T739" id="Seg_5519" n="e" s="T738">mĭlie </ts>
               <ts e="T740" id="Seg_5521" n="e" s="T739">dʼăbaktərzittə. </ts>
               <ts e="T741" id="Seg_5523" n="e" s="T740">Miʔ </ts>
               <ts e="T742" id="Seg_5525" n="e" s="T741">baʔluʔpibaʔ, </ts>
               <ts e="T743" id="Seg_5527" n="e" s="T742">a </ts>
               <ts e="T744" id="Seg_5529" n="e" s="T743">dĭzeŋ </ts>
               <ts e="T987" id="Seg_5531" n="e" s="T744">kalla </ts>
               <ts e="T745" id="Seg_5533" n="e" s="T987">dʼürbiʔi. </ts>
               <ts e="T746" id="Seg_5535" n="e" s="T745">Ej </ts>
               <ts e="T747" id="Seg_5537" n="e" s="T746">dʼăbaktərbiabaʔ. </ts>
               <ts e="T748" id="Seg_5539" n="e" s="T747">A </ts>
               <ts e="T749" id="Seg_5541" n="e" s="T748">tüj </ts>
               <ts e="T750" id="Seg_5543" n="e" s="T749">šobiʔi </ts>
               <ts e="T751" id="Seg_5545" n="e" s="T750">bazoʔ </ts>
               <ts e="T752" id="Seg_5547" n="e" s="T751">(s-) </ts>
               <ts e="T753" id="Seg_5549" n="e" s="T752">dʼăbaktərzittə. </ts>
               <ts e="T754" id="Seg_5551" n="e" s="T753">((BRK)). </ts>
               <ts e="T755" id="Seg_5553" n="e" s="T754">Šobiʔi, </ts>
               <ts e="T756" id="Seg_5555" n="e" s="T755">a </ts>
               <ts e="T757" id="Seg_5557" n="e" s="T756">măn </ts>
               <ts e="T758" id="Seg_5559" n="e" s="T757">măndəm: </ts>
               <ts e="T759" id="Seg_5561" n="e" s="T758">Gijen </ts>
               <ts e="T760" id="Seg_5563" n="e" s="T759">ibileʔ? </ts>
               <ts e="T761" id="Seg_5565" n="e" s="T760">Da </ts>
               <ts e="T762" id="Seg_5567" n="e" s="T761">pa </ts>
               <ts e="T763" id="Seg_5569" n="e" s="T762">kürbibeʔ, </ts>
               <ts e="T764" id="Seg_5571" n="e" s="T763">bostə </ts>
               <ts e="T765" id="Seg_5573" n="e" s="T764">tura </ts>
               <ts e="T766" id="Seg_5575" n="e" s="T765">kürbibeʔ, </ts>
               <ts e="T767" id="Seg_5577" n="e" s="T766">i </ts>
               <ts e="T768" id="Seg_5579" n="e" s="T767">urgajan </ts>
               <ts e="T769" id="Seg_5581" n="e" s="T768">turanə </ts>
               <ts e="T770" id="Seg_5583" n="e" s="T769">(kürbi-) </ts>
               <ts e="T771" id="Seg_5585" n="e" s="T770">kürbibeʔ, </ts>
               <ts e="T772" id="Seg_5587" n="e" s="T771">i </ts>
               <ts e="T773" id="Seg_5589" n="e" s="T772">döbər </ts>
               <ts e="T774" id="Seg_5591" n="e" s="T773">šobibaʔ". </ts>
               <ts e="T775" id="Seg_5593" n="e" s="T774">((BRK)). </ts>
               <ts e="T776" id="Seg_5595" n="e" s="T775">Dĭgəttə </ts>
               <ts e="T777" id="Seg_5597" n="e" s="T776">iat </ts>
               <ts e="T778" id="Seg_5599" n="e" s="T777">šobi </ts>
               <ts e="T779" id="Seg_5601" n="e" s="T778">bălʼnʼitsagən. </ts>
               <ts e="T780" id="Seg_5603" n="e" s="T779">Davaj </ts>
               <ts e="T781" id="Seg_5605" n="e" s="T780">essem </ts>
               <ts e="T782" id="Seg_5607" n="e" s="T781">(kudonz-): </ts>
               <ts e="T783" id="Seg_5609" n="e" s="T782">ĭmbidə </ts>
               <ts e="T784" id="Seg_5611" n="e" s="T783">ej </ts>
               <ts e="T785" id="Seg_5613" n="e" s="T784">(aliaʔi). </ts>
               <ts e="T786" id="Seg_5615" n="e" s="T785">(Na), </ts>
               <ts e="T787" id="Seg_5617" n="e" s="T786">paʔi </ts>
               <ts e="T788" id="Seg_5619" n="e" s="T787">eneʔ </ts>
               <ts e="T789" id="Seg_5621" n="e" s="T788">ăgradagən. </ts>
               <ts e="T790" id="Seg_5623" n="e" s="T789">A_to </ts>
               <ts e="T791" id="Seg_5625" n="e" s="T790">abal </ts>
               <ts e="T792" id="Seg_5627" n="e" s="T791">šoləj, </ts>
               <ts e="T793" id="Seg_5629" n="e" s="T792">ĭmbidə </ts>
               <ts e="T794" id="Seg_5631" n="e" s="T793">ej </ts>
               <ts e="T795" id="Seg_5633" n="e" s="T794">mĭləj. </ts>
               <ts e="T796" id="Seg_5635" n="e" s="T795">Dĭ </ts>
               <ts e="T797" id="Seg_5637" n="e" s="T796">kambi </ts>
               <ts e="T798" id="Seg_5639" n="e" s="T797">da </ts>
               <ts e="T799" id="Seg_5641" n="e" s="T798">davaj </ts>
               <ts e="T800" id="Seg_5643" n="e" s="T799">enzittə. </ts>
               <ts e="T801" id="Seg_5645" n="e" s="T800">Dĭgəttə </ts>
               <ts e="T802" id="Seg_5647" n="e" s="T801">šobi, </ts>
               <ts e="T803" id="Seg_5649" n="e" s="T802">multʼagən </ts>
               <ts e="T804" id="Seg_5651" n="e" s="T803">băzəjdəbiʔi. </ts>
               <ts e="T805" id="Seg_5653" n="e" s="T804">Dĭgəttə </ts>
               <ts e="T806" id="Seg_5655" n="e" s="T805">măna </ts>
               <ts e="T807" id="Seg_5657" n="e" s="T806">măndəʔi: </ts>
               <ts e="T808" id="Seg_5659" n="e" s="T807">Kanaʔ </ts>
               <ts e="T809" id="Seg_5661" n="e" s="T808">moltʼanə. </ts>
               <ts e="T810" id="Seg_5663" n="e" s="T809">Măn </ts>
               <ts e="T811" id="Seg_5665" n="e" s="T810">kambiam </ts>
               <ts e="T812" id="Seg_5667" n="e" s="T811">băzəjdəsʼtə. </ts>
               <ts e="T813" id="Seg_5669" n="e" s="T812">Dĭgəttə </ts>
               <ts e="T814" id="Seg_5671" n="e" s="T813">Jelʼa </ts>
               <ts e="T815" id="Seg_5673" n="e" s="T814">šobi, </ts>
               <ts e="T816" id="Seg_5675" n="e" s="T815">băzəjdəsʼtə. </ts>
               <ts e="T817" id="Seg_5677" n="e" s="T816">Dĭgəttə </ts>
               <ts e="T818" id="Seg_5679" n="e" s="T817">tibi </ts>
               <ts e="T819" id="Seg_5681" n="e" s="T818">šobi, </ts>
               <ts e="T820" id="Seg_5683" n="e" s="T819">băzəjdəbi. </ts>
               <ts e="T821" id="Seg_5685" n="e" s="T820">Dĭgəttə </ts>
               <ts e="T822" id="Seg_5687" n="e" s="T821">abat </ts>
               <ts e="T823" id="Seg_5689" n="e" s="T822">šobi. </ts>
               <ts e="T824" id="Seg_5691" n="e" s="T823">Deʔpi </ts>
               <ts e="T825" id="Seg_5693" n="e" s="T824">čulkiʔi. </ts>
               <ts e="T826" id="Seg_5695" n="e" s="T825">(Piʔmeʔi) </ts>
               <ts e="T827" id="Seg_5697" n="e" s="T826">deʔpi. </ts>
               <ts e="T828" id="Seg_5699" n="e" s="T827">((BRK)). </ts>
               <ts e="T829" id="Seg_5701" n="e" s="T828">Dĭgəttə </ts>
               <ts e="T830" id="Seg_5703" n="e" s="T829">šobi, </ts>
               <ts e="T831" id="Seg_5705" n="e" s="T830">ara </ts>
               <ts e="T832" id="Seg_5707" n="e" s="T831">deʔpi. </ts>
               <ts e="T833" id="Seg_5709" n="e" s="T832">Onʼiʔ </ts>
               <ts e="T834" id="Seg_5711" n="e" s="T833">tibi </ts>
               <ts e="T835" id="Seg_5713" n="e" s="T834">dĭm </ts>
               <ts e="T836" id="Seg_5715" n="e" s="T835">deʔpi </ts>
               <ts e="T837" id="Seg_5717" n="e" s="T836">inetsi. </ts>
               <ts e="T838" id="Seg_5719" n="e" s="T837">Dĭgəttə </ts>
               <ts e="T839" id="Seg_5721" n="e" s="T838">dĭ </ts>
               <ts e="T840" id="Seg_5723" n="e" s="T839">dĭʔnə </ts>
               <ts e="T841" id="Seg_5725" n="e" s="T840">ara </ts>
               <ts e="T842" id="Seg_5727" n="e" s="T841">kămnəbi. </ts>
               <ts e="T843" id="Seg_5729" n="e" s="T842">Šalaʔi </ts>
               <ts e="T844" id="Seg_5731" n="e" s="T843">amorbi, </ts>
               <ts e="T845" id="Seg_5733" n="e" s="T844">(ka-) </ts>
               <ts e="T846" id="Seg_5735" n="e" s="T845">kambi </ts>
               <ts e="T847" id="Seg_5737" n="e" s="T846">maːʔndə. </ts>
               <ts e="T848" id="Seg_5739" n="e" s="T847">((BRK)). </ts>
               <ts e="T849" id="Seg_5741" n="e" s="T848">Dĭ </ts>
               <ts e="T850" id="Seg_5743" n="e" s="T849">ne </ts>
               <ts e="T851" id="Seg_5745" n="e" s="T850">ugandə </ts>
               <ts e="T852" id="Seg_5747" n="e" s="T851">kurojoʔ. </ts>
               <ts e="T853" id="Seg_5749" n="e" s="T852">Pa </ts>
               <ts e="T854" id="Seg_5751" n="e" s="T853">ibi </ts>
               <ts e="T855" id="Seg_5753" n="e" s="T854">da </ts>
               <ts e="T856" id="Seg_5755" n="e" s="T855">büzəm </ts>
               <ts e="T857" id="Seg_5757" n="e" s="T856">(pa-) </ts>
               <ts e="T858" id="Seg_5759" n="e" s="T857">pazi </ts>
               <ts e="T859" id="Seg_5761" n="e" s="T858">toʔnarbi, </ts>
               <ts e="T860" id="Seg_5763" n="e" s="T859">sʼimattə. </ts>
               <ts e="T861" id="Seg_5765" n="e" s="T860">Kăjdə </ts>
               <ts e="T862" id="Seg_5767" n="e" s="T861">bar </ts>
               <ts e="T863" id="Seg_5769" n="e" s="T862">mʼaŋnaʔbə. </ts>
               <ts e="T864" id="Seg_5771" n="e" s="T863">A </ts>
               <ts e="T865" id="Seg_5773" n="e" s="T864">măn </ts>
               <ts e="T866" id="Seg_5775" n="e" s="T865">bostə </ts>
               <ts e="T867" id="Seg_5777" n="e" s="T866">büzəm </ts>
               <ts e="T868" id="Seg_5779" n="e" s="T867">ej </ts>
               <ts e="T869" id="Seg_5781" n="e" s="T868">münöriam, </ts>
               <ts e="T870" id="Seg_5783" n="e" s="T869">(udazi). </ts>
               <ts e="T871" id="Seg_5785" n="e" s="T870">Udazi </ts>
               <ts e="T872" id="Seg_5787" n="e" s="T871">toʔlim, </ts>
               <ts e="T873" id="Seg_5789" n="e" s="T872">šalaʔi </ts>
               <ts e="T874" id="Seg_5791" n="e" s="T873">bar </ts>
               <ts e="T875" id="Seg_5793" n="e" s="T874">(bĭtluʔpi). </ts>
               <ts e="T876" id="Seg_5795" n="e" s="T875">((BRK)). </ts>
               <ts e="T877" id="Seg_5797" n="e" s="T876">Măn </ts>
               <ts e="T878" id="Seg_5799" n="e" s="T877">turagən </ts>
               <ts e="T879" id="Seg_5801" n="e" s="T878">kürbiem </ts>
               <ts e="T880" id="Seg_5803" n="e" s="T879">mašinam, </ts>
               <ts e="T882" id="Seg_5805" n="e" s="T880">i… </ts>
               <ts e="T883" id="Seg_5807" n="e" s="T882">Dĭzeŋ </ts>
               <ts e="T884" id="Seg_5809" n="e" s="T883">oʔb, </ts>
               <ts e="T885" id="Seg_5811" n="e" s="T884">šide, </ts>
               <ts e="T886" id="Seg_5813" n="e" s="T885">nagur, </ts>
               <ts e="T887" id="Seg_5815" n="e" s="T886">teʔtə. </ts>
               <ts e="T888" id="Seg_5817" n="e" s="T887">Dĭgəttə </ts>
               <ts e="T889" id="Seg_5819" n="e" s="T888">Vanʼa </ts>
               <ts e="T890" id="Seg_5821" n="e" s="T889">măndə: </ts>
               <ts e="T891" id="Seg_5823" n="e" s="T890">Büzö, </ts>
               <ts e="T892" id="Seg_5825" n="e" s="T891">kereʔ, </ts>
               <ts e="T893" id="Seg_5827" n="e" s="T892">i </ts>
               <ts e="T894" id="Seg_5829" n="e" s="T893">šoškaʔi,— </ts>
               <ts e="T895" id="Seg_5831" n="e" s="T894">a </ts>
               <ts e="T896" id="Seg_5833" n="e" s="T895">miʔ </ts>
               <ts e="T897" id="Seg_5835" n="e" s="T896">bar </ts>
               <ts e="T898" id="Seg_5837" n="e" s="T897">kaknarbibaʔ. </ts>
               <ts e="T899" id="Seg_5839" n="e" s="T898">((BRK)). </ts>
               <ts e="T900" id="Seg_5841" n="e" s="T899">Teinen </ts>
               <ts e="T901" id="Seg_5843" n="e" s="T900">padʼi </ts>
               <ts e="T902" id="Seg_5845" n="e" s="T901">iʔgö </ts>
               <ts e="T903" id="Seg_5847" n="e" s="T902">dʼăbaktərbibaʔ. </ts>
               <ts e="T904" id="Seg_5849" n="e" s="T903">Tüj </ts>
               <ts e="T905" id="Seg_5851" n="e" s="T904">nada </ts>
               <ts e="T906" id="Seg_5853" n="e" s="T905">maʔnʼi </ts>
               <ts e="T907" id="Seg_5855" n="e" s="T906">kanzittə, </ts>
               <ts e="T908" id="Seg_5857" n="e" s="T907">amorzittə, </ts>
               <ts e="T909" id="Seg_5859" n="e" s="T908">iʔbəsʼtə, </ts>
               <ts e="T910" id="Seg_5861" n="e" s="T909">(šal-) </ts>
               <ts e="T911" id="Seg_5863" n="e" s="T910">(šalaʔi). </ts>
               <ts e="T912" id="Seg_5865" n="e" s="T911">((BRK)). </ts>
               <ts e="T913" id="Seg_5867" n="e" s="T912">Bor </ts>
               <ts e="T914" id="Seg_5869" n="e" s="T913">bar </ts>
               <ts e="T915" id="Seg_5871" n="e" s="T914">nʼiʔtə </ts>
               <ts e="T916" id="Seg_5873" n="e" s="T915">kandəga. </ts>
               <ts e="T917" id="Seg_5875" n="e" s="T916">Ugandə </ts>
               <ts e="T918" id="Seg_5877" n="e" s="T917">šišəge </ts>
               <ts e="T919" id="Seg_5879" n="e" s="T918">moləj. </ts>
               <ts e="T920" id="Seg_5881" n="e" s="T919">Teinen </ts>
               <ts e="T921" id="Seg_5883" n="e" s="T920">nüdʼin </ts>
               <ts e="T922" id="Seg_5885" n="e" s="T921">sĭre. </ts>
               <ts e="T923" id="Seg_5887" n="e" s="T922">Ertən </ts>
               <ts e="T924" id="Seg_5889" n="e" s="T923">uʔbdəbiam— </ts>
               <ts e="T925" id="Seg_5891" n="e" s="T924">bar </ts>
               <ts e="T926" id="Seg_5893" n="e" s="T925">šišəge </ts>
               <ts e="T927" id="Seg_5895" n="e" s="T926">molambi. </ts>
               <ts e="T928" id="Seg_5897" n="e" s="T927">((BRK)). </ts>
               <ts e="T929" id="Seg_5899" n="e" s="T928">Miʔnʼibeʔ </ts>
               <ts e="T930" id="Seg_5901" n="e" s="T929">teinen </ts>
               <ts e="T931" id="Seg_5903" n="e" s="T930">nada </ts>
               <ts e="T932" id="Seg_5905" n="e" s="T931">kanzittə </ts>
               <ts e="T933" id="Seg_5907" n="e" s="T932">Tanʼanə. </ts>
               <ts e="T934" id="Seg_5909" n="e" s="T933">Štobɨ </ts>
               <ts e="T935" id="Seg_5911" n="e" s="T934">dĭ </ts>
               <ts e="T936" id="Seg_5913" n="e" s="T935">ipek </ts>
               <ts e="T937" id="Seg_5915" n="e" s="T936">ibi </ts>
               <ts e="T938" id="Seg_5917" n="e" s="T937">(Văznesenkagə) </ts>
               <ts e="T939" id="Seg_5919" n="e" s="T938">i </ts>
               <ts e="T940" id="Seg_5921" n="e" s="T939">deʔpi </ts>
               <ts e="T941" id="Seg_5923" n="e" s="T940">miʔnʼibeʔ. </ts>
               <ts e="T942" id="Seg_5925" n="e" s="T941">((BRK)). </ts>
               <ts e="T943" id="Seg_5927" n="e" s="T942">((…)) </ts>
               <ts e="T944" id="Seg_5929" n="e" s="T943">nada </ts>
               <ts e="T945" id="Seg_5931" n="e" s="T944">aktʼam </ts>
               <ts e="T946" id="Seg_5933" n="e" s="T945">izittə </ts>
               <ts e="T947" id="Seg_5935" n="e" s="T946">diʔnə. </ts>
               <ts e="T948" id="Seg_5937" n="e" s="T947">Ej </ts>
               <ts e="T949" id="Seg_5939" n="e" s="T948">boskəndə </ts>
               <ts e="T950" id="Seg_5941" n="e" s="T949">deʔsittə </ts>
               <ts e="T951" id="Seg_5943" n="e" s="T950">dĭʔə </ts>
               <ts e="T952" id="Seg_5945" n="e" s="T951">inetsi </ts>
               <ts e="T953" id="Seg_5947" n="e" s="T952">mĭlləj. </ts>
               <ts e="T954" id="Seg_5949" n="e" s="T953">I </ts>
               <ts e="T955" id="Seg_5951" n="e" s="T954">detləj. </ts>
               <ts e="T956" id="Seg_5953" n="e" s="T955">((BRK)). </ts>
               <ts e="T957" id="Seg_5955" n="e" s="T956">Ej </ts>
               <ts e="T958" id="Seg_5957" n="e" s="T957">tĭmnem, </ts>
               <ts e="T959" id="Seg_5959" n="e" s="T958">iləj </ts>
               <ts e="T960" id="Seg_5961" n="e" s="T959">ilʼi </ts>
               <ts e="T961" id="Seg_5963" n="e" s="T960">ej. </ts>
               <ts e="T962" id="Seg_5965" n="e" s="T961">Nada </ts>
               <ts e="T963" id="Seg_5967" n="e" s="T962">kanzittə </ts>
               <ts e="T964" id="Seg_5969" n="e" s="T963">dĭzi </ts>
               <ts e="T965" id="Seg_5971" n="e" s="T964">dʼăbaktərzittə, </ts>
               <ts e="T966" id="Seg_5973" n="e" s="T965">možet </ts>
               <ts e="T967" id="Seg_5975" n="e" s="T966">iləj. </ts>
               <ts e="T968" id="Seg_5977" n="e" s="T967">No, </ts>
               <ts e="T969" id="Seg_5979" n="e" s="T968">kanaʔ, </ts>
               <ts e="T970" id="Seg_5981" n="e" s="T969">dʼăbaktəraʔ. </ts>
               <ts e="T971" id="Seg_5983" n="e" s="T970">((BRK)). </ts>
               <ts e="T972" id="Seg_5985" n="e" s="T971">Dĭ </ts>
               <ts e="T973" id="Seg_5987" n="e" s="T972">možet </ts>
               <ts e="T974" id="Seg_5989" n="e" s="T973">mĭlləj </ts>
               <ts e="T975" id="Seg_5991" n="e" s="T974">măgăzingən </ts>
               <ts e="T976" id="Seg_5993" n="e" s="T975">dĭn. </ts>
               <ts e="T977" id="Seg_5995" n="e" s="T976">Очередь </ts>
               <ts e="T978" id="Seg_5997" n="e" s="T977">nuzittə </ts>
               <ts e="T979" id="Seg_5999" n="e" s="T978">nada, </ts>
               <ts e="T980" id="Seg_6001" n="e" s="T979">ipek </ts>
               <ts e="T981" id="Seg_6003" n="e" s="T980">izittə. </ts>
               <ts e="T982" id="Seg_6005" n="e" s="T981">Măn </ts>
               <ts e="T983" id="Seg_6007" n="e" s="T982">ej </ts>
               <ts e="T984" id="Seg_6009" n="e" s="T983">iləm. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_6010" s="T0">PKZ_196X_SU0218.001 (001)</ta>
            <ta e="T10" id="Seg_6011" s="T4">PKZ_196X_SU0218.002 (002)</ta>
            <ta e="T13" id="Seg_6012" s="T10">PKZ_196X_SU0218.003 (003)</ta>
            <ta e="T16" id="Seg_6013" s="T13">PKZ_196X_SU0218.004 (004)</ta>
            <ta e="T21" id="Seg_6014" s="T16">PKZ_196X_SU0218.005 (005)</ta>
            <ta e="T29" id="Seg_6015" s="T21">PKZ_196X_SU0218.006 (006)</ta>
            <ta e="T31" id="Seg_6016" s="T29">PKZ_196X_SU0218.007 (007)</ta>
            <ta e="T35" id="Seg_6017" s="T31">PKZ_196X_SU0218.008 (008)</ta>
            <ta e="T40" id="Seg_6018" s="T35">PKZ_196X_SU0218.009 (009)</ta>
            <ta e="T43" id="Seg_6019" s="T40">PKZ_196X_SU0218.010 (010)</ta>
            <ta e="T44" id="Seg_6020" s="T43">PKZ_196X_SU0218.011 (011)</ta>
            <ta e="T49" id="Seg_6021" s="T44">PKZ_196X_SU0218.012 (012)</ta>
            <ta e="T56" id="Seg_6022" s="T49">PKZ_196X_SU0218.013 (013)</ta>
            <ta e="T58" id="Seg_6023" s="T56">PKZ_196X_SU0218.014 (014)</ta>
            <ta e="T64" id="Seg_6024" s="T58">PKZ_196X_SU0218.015 (015)</ta>
            <ta e="T69" id="Seg_6025" s="T64">PKZ_196X_SU0218.016 (016)</ta>
            <ta e="T70" id="Seg_6026" s="T69">PKZ_196X_SU0218.017 (017)</ta>
            <ta e="T74" id="Seg_6027" s="T70">PKZ_196X_SU0218.018 (018)</ta>
            <ta e="T79" id="Seg_6028" s="T74">PKZ_196X_SU0218.019 (019)</ta>
            <ta e="T84" id="Seg_6029" s="T79">PKZ_196X_SU0218.020 (020)</ta>
            <ta e="T89" id="Seg_6030" s="T84">PKZ_196X_SU0218.021 (021)</ta>
            <ta e="T94" id="Seg_6031" s="T89">PKZ_196X_SU0218.022 (022)</ta>
            <ta e="T104" id="Seg_6032" s="T94">PKZ_196X_SU0218.023 (023)</ta>
            <ta e="T108" id="Seg_6033" s="T104">PKZ_196X_SU0218.024 (024)</ta>
            <ta e="T111" id="Seg_6034" s="T108">PKZ_196X_SU0218.025 (025)</ta>
            <ta e="T116" id="Seg_6035" s="T111">PKZ_196X_SU0218.026 (026)</ta>
            <ta e="T118" id="Seg_6036" s="T116">PKZ_196X_SU0218.027 (027)</ta>
            <ta e="T119" id="Seg_6037" s="T118">PKZ_196X_SU0218.028 (028)</ta>
            <ta e="T123" id="Seg_6038" s="T119">PKZ_196X_SU0218.029 (029)</ta>
            <ta e="T126" id="Seg_6039" s="T123">PKZ_196X_SU0218.030 (030)</ta>
            <ta e="T127" id="Seg_6040" s="T126">PKZ_196X_SU0218.031 (031)</ta>
            <ta e="T129" id="Seg_6041" s="T127">PKZ_196X_SU0218.032 (032)</ta>
            <ta e="T136" id="Seg_6042" s="T129">PKZ_196X_SU0218.033 (033)</ta>
            <ta e="T138" id="Seg_6043" s="T136">PKZ_196X_SU0218.034 (034)</ta>
            <ta e="T143" id="Seg_6044" s="T138">PKZ_196X_SU0218.035 (035)</ta>
            <ta e="T149" id="Seg_6045" s="T143">PKZ_196X_SU0218.036 (036)</ta>
            <ta e="T152" id="Seg_6046" s="T149">PKZ_196X_SU0218.037 (037)</ta>
            <ta e="T153" id="Seg_6047" s="T152">PKZ_196X_SU0218.038 (038)</ta>
            <ta e="T156" id="Seg_6048" s="T153">PKZ_196X_SU0218.039 (039)</ta>
            <ta e="T159" id="Seg_6049" s="T156">PKZ_196X_SU0218.040 (040)</ta>
            <ta e="T161" id="Seg_6050" s="T159">PKZ_196X_SU0218.041 (041)</ta>
            <ta e="T163" id="Seg_6051" s="T161">PKZ_196X_SU0218.042 (042)</ta>
            <ta e="T167" id="Seg_6052" s="T163">PKZ_196X_SU0218.043 (043)</ta>
            <ta e="T176" id="Seg_6053" s="T167">PKZ_196X_SU0218.044 (044)</ta>
            <ta e="T181" id="Seg_6054" s="T176">PKZ_196X_SU0218.045 (045)</ta>
            <ta e="T185" id="Seg_6055" s="T181">PKZ_196X_SU0218.046 (046)</ta>
            <ta e="T190" id="Seg_6056" s="T185">PKZ_196X_SU0218.047 (047)</ta>
            <ta e="T191" id="Seg_6057" s="T190">PKZ_196X_SU0218.048 (048)</ta>
            <ta e="T201" id="Seg_6058" s="T191">PKZ_196X_SU0218.049 (049) </ta>
            <ta e="T205" id="Seg_6059" s="T201">PKZ_196X_SU0218.050 (051)</ta>
            <ta e="T214" id="Seg_6060" s="T205">PKZ_196X_SU0218.051 (052)</ta>
            <ta e="T224" id="Seg_6061" s="T214">PKZ_196X_SU0218.052 (053)</ta>
            <ta e="T225" id="Seg_6062" s="T224">PKZ_196X_SU0218.053 (054)</ta>
            <ta e="T232" id="Seg_6063" s="T225">PKZ_196X_SU0218.054 (055)</ta>
            <ta e="T235" id="Seg_6064" s="T232">PKZ_196X_SU0218.055 (056)</ta>
            <ta e="T239" id="Seg_6065" s="T235">PKZ_196X_SU0218.056 (057)</ta>
            <ta e="T246" id="Seg_6066" s="T239">PKZ_196X_SU0218.057 (058)</ta>
            <ta e="T255" id="Seg_6067" s="T246">PKZ_196X_SU0218.058 (059)</ta>
            <ta e="T256" id="Seg_6068" s="T255">PKZ_196X_SU0218.059 (060)</ta>
            <ta e="T263" id="Seg_6069" s="T256">PKZ_196X_SU0218.060 (061)</ta>
            <ta e="T264" id="Seg_6070" s="T263">PKZ_196X_SU0218.061 (062)</ta>
            <ta e="T268" id="Seg_6071" s="T264">PKZ_196X_SU0218.062 (063)</ta>
            <ta e="T272" id="Seg_6072" s="T268">PKZ_196X_SU0218.063 (064)</ta>
            <ta e="T281" id="Seg_6073" s="T272">PKZ_196X_SU0218.064 (065)</ta>
            <ta e="T286" id="Seg_6074" s="T281">PKZ_196X_SU0218.065 (066)</ta>
            <ta e="T293" id="Seg_6075" s="T286">PKZ_196X_SU0218.066 (067) </ta>
            <ta e="T297" id="Seg_6076" s="T293">PKZ_196X_SU0218.067 (069)</ta>
            <ta e="T300" id="Seg_6077" s="T297">PKZ_196X_SU0218.068 (070)</ta>
            <ta e="T308" id="Seg_6078" s="T300">PKZ_196X_SU0218.069 (071)</ta>
            <ta e="T312" id="Seg_6079" s="T308">PKZ_196X_SU0218.070 (072)</ta>
            <ta e="T313" id="Seg_6080" s="T312">PKZ_196X_SU0218.071 (073)</ta>
            <ta e="T320" id="Seg_6081" s="T313">PKZ_196X_SU0218.072 (074)</ta>
            <ta e="T325" id="Seg_6082" s="T320">PKZ_196X_SU0218.073 (075)</ta>
            <ta e="T326" id="Seg_6083" s="T325">PKZ_196X_SU0218.074 (076)</ta>
            <ta e="T330" id="Seg_6084" s="T326">PKZ_196X_SU0218.075 (077)</ta>
            <ta e="T331" id="Seg_6085" s="T330">PKZ_196X_SU0218.076 (078)</ta>
            <ta e="T335" id="Seg_6086" s="T331">PKZ_196X_SU0218.077 (079)</ta>
            <ta e="T340" id="Seg_6087" s="T335">PKZ_196X_SU0218.078 (080)</ta>
            <ta e="T341" id="Seg_6088" s="T340">PKZ_196X_SU0218.079 (081)</ta>
            <ta e="T345" id="Seg_6089" s="T341">PKZ_196X_SU0218.080 (082)</ta>
            <ta e="T351" id="Seg_6090" s="T345">PKZ_196X_SU0218.081 (083)</ta>
            <ta e="T352" id="Seg_6091" s="T351">PKZ_196X_SU0218.082 (084)</ta>
            <ta e="T354" id="Seg_6092" s="T352">PKZ_196X_SU0218.083 (085)</ta>
            <ta e="T356" id="Seg_6093" s="T354">PKZ_196X_SU0218.084 (086)</ta>
            <ta e="T362" id="Seg_6094" s="T356">PKZ_196X_SU0218.085 (087)</ta>
            <ta e="T363" id="Seg_6095" s="T362">PKZ_196X_SU0218.086 (088)</ta>
            <ta e="T366" id="Seg_6096" s="T363">PKZ_196X_SU0218.087 (089)</ta>
            <ta e="T371" id="Seg_6097" s="T366">PKZ_196X_SU0218.088 (090)</ta>
            <ta e="T376" id="Seg_6098" s="T371">PKZ_196X_SU0218.089 (091)</ta>
            <ta e="T378" id="Seg_6099" s="T376">PKZ_196X_SU0218.090 (092)</ta>
            <ta e="T383" id="Seg_6100" s="T378">PKZ_196X_SU0218.091 (093)</ta>
            <ta e="T388" id="Seg_6101" s="T383">PKZ_196X_SU0218.092 (094)</ta>
            <ta e="T389" id="Seg_6102" s="T388">PKZ_196X_SU0218.093 (095)</ta>
            <ta e="T395" id="Seg_6103" s="T389">PKZ_196X_SU0218.094 (096)</ta>
            <ta e="T398" id="Seg_6104" s="T395">PKZ_196X_SU0218.095 (097)</ta>
            <ta e="T404" id="Seg_6105" s="T398">PKZ_196X_SU0218.096 (098)</ta>
            <ta e="T407" id="Seg_6106" s="T404">PKZ_196X_SU0218.097 (099)</ta>
            <ta e="T414" id="Seg_6107" s="T407">PKZ_196X_SU0218.098 (100)</ta>
            <ta e="T423" id="Seg_6108" s="T414">PKZ_196X_SU0218.099 (101)</ta>
            <ta e="T424" id="Seg_6109" s="T423">PKZ_196X_SU0218.100 (102)</ta>
            <ta e="T430" id="Seg_6110" s="T424">PKZ_196X_SU0218.101 (103)</ta>
            <ta e="T438" id="Seg_6111" s="T430">PKZ_196X_SU0218.102 (104)</ta>
            <ta e="T445" id="Seg_6112" s="T438">PKZ_196X_SU0218.103 (105)</ta>
            <ta e="T446" id="Seg_6113" s="T445">PKZ_196X_SU0218.104 (106)</ta>
            <ta e="T451" id="Seg_6114" s="T446">PKZ_196X_SU0218.105 (107)</ta>
            <ta e="T456" id="Seg_6115" s="T451">PKZ_196X_SU0218.106 (108)</ta>
            <ta e="T466" id="Seg_6116" s="T456">PKZ_196X_SU0218.107 (109)</ta>
            <ta e="T468" id="Seg_6117" s="T466">PKZ_196X_SU0218.108 (110)</ta>
            <ta e="T475" id="Seg_6118" s="T468">PKZ_196X_SU0218.109 (111)</ta>
            <ta e="T479" id="Seg_6119" s="T475">PKZ_196X_SU0218.110 (112)</ta>
            <ta e="T485" id="Seg_6120" s="T479">PKZ_196X_SU0218.111 (113)</ta>
            <ta e="T488" id="Seg_6121" s="T485">PKZ_196X_SU0218.112 (114)</ta>
            <ta e="T496" id="Seg_6122" s="T488">PKZ_196X_SU0218.113 (115) </ta>
            <ta e="T501" id="Seg_6123" s="T496">PKZ_196X_SU0218.114 (117)</ta>
            <ta e="T505" id="Seg_6124" s="T501">PKZ_196X_SU0218.115 (118)</ta>
            <ta e="T510" id="Seg_6125" s="T505">PKZ_196X_SU0218.116 (119)</ta>
            <ta e="T513" id="Seg_6126" s="T510">PKZ_196X_SU0218.117 (120)</ta>
            <ta e="T519" id="Seg_6127" s="T513">PKZ_196X_SU0218.118 (121)</ta>
            <ta e="T521" id="Seg_6128" s="T519">PKZ_196X_SU0218.119 (122)</ta>
            <ta e="T523" id="Seg_6129" s="T521">PKZ_196X_SU0218.120 (123)</ta>
            <ta e="T524" id="Seg_6130" s="T523">PKZ_196X_SU0218.121 (124)</ta>
            <ta e="T529" id="Seg_6131" s="T524">PKZ_196X_SU0218.122 (125)</ta>
            <ta e="T531" id="Seg_6132" s="T529">PKZ_196X_SU0218.123 (126)</ta>
            <ta e="T535" id="Seg_6133" s="T531">PKZ_196X_SU0218.124 (127)</ta>
            <ta e="T537" id="Seg_6134" s="T535">PKZ_196X_SU0218.125 (128)</ta>
            <ta e="T540" id="Seg_6135" s="T537">PKZ_196X_SU0218.126 (129)</ta>
            <ta e="T542" id="Seg_6136" s="T540">PKZ_196X_SU0218.127 (130)</ta>
            <ta e="T549" id="Seg_6137" s="T542">PKZ_196X_SU0218.128 (131)</ta>
            <ta e="T551" id="Seg_6138" s="T549">PKZ_196X_SU0218.129 (132)</ta>
            <ta e="T557" id="Seg_6139" s="T551">PKZ_196X_SU0218.130 (133)</ta>
            <ta e="T562" id="Seg_6140" s="T557">PKZ_196X_SU0218.131 (134)</ta>
            <ta e="T565" id="Seg_6141" s="T562">PKZ_196X_SU0218.132 (135)</ta>
            <ta e="T568" id="Seg_6142" s="T565">PKZ_196X_SU0218.133 (136)</ta>
            <ta e="T574" id="Seg_6143" s="T568">PKZ_196X_SU0218.134 (137)</ta>
            <ta e="T578" id="Seg_6144" s="T574">PKZ_196X_SU0218.135 (138) </ta>
            <ta e="T580" id="Seg_6145" s="T578">PKZ_196X_SU0218.136 (140)</ta>
            <ta e="T584" id="Seg_6146" s="T580">PKZ_196X_SU0218.137 (141)</ta>
            <ta e="T587" id="Seg_6147" s="T584">PKZ_196X_SU0218.138 (142)</ta>
            <ta e="T588" id="Seg_6148" s="T587">PKZ_196X_SU0218.139 (143)</ta>
            <ta e="T594" id="Seg_6149" s="T588">PKZ_196X_SU0218.140 (144)</ta>
            <ta e="T597" id="Seg_6150" s="T594">PKZ_196X_SU0218.141 (145)</ta>
            <ta e="T600" id="Seg_6151" s="T597">PKZ_196X_SU0218.142 (146)</ta>
            <ta e="T605" id="Seg_6152" s="T600">PKZ_196X_SU0218.143 (147)</ta>
            <ta e="T606" id="Seg_6153" s="T605">PKZ_196X_SU0218.144 (148)</ta>
            <ta e="T613" id="Seg_6154" s="T606">PKZ_196X_SU0218.145 (149)</ta>
            <ta e="T619" id="Seg_6155" s="T613">PKZ_196X_SU0218.146 (150)</ta>
            <ta e="T623" id="Seg_6156" s="T619">PKZ_196X_SU0218.147 (151)</ta>
            <ta e="T628" id="Seg_6157" s="T623">PKZ_196X_SU0218.148 (152)</ta>
            <ta e="T632" id="Seg_6158" s="T628">PKZ_196X_SU0218.149 (153)</ta>
            <ta e="T637" id="Seg_6159" s="T632">PKZ_196X_SU0218.150 (154)</ta>
            <ta e="T643" id="Seg_6160" s="T637">PKZ_196X_SU0218.151 (155)</ta>
            <ta e="T649" id="Seg_6161" s="T643">PKZ_196X_SU0218.152 (156)</ta>
            <ta e="T656" id="Seg_6162" s="T649">PKZ_196X_SU0218.153 (157)</ta>
            <ta e="T660" id="Seg_6163" s="T656">PKZ_196X_SU0218.154 (158)</ta>
            <ta e="T662" id="Seg_6164" s="T660">PKZ_196X_SU0218.155 (159)</ta>
            <ta e="T664" id="Seg_6165" s="T662">PKZ_196X_SU0218.156 (160)</ta>
            <ta e="T672" id="Seg_6166" s="T664">PKZ_196X_SU0218.157 (161) </ta>
            <ta e="T675" id="Seg_6167" s="T672">PKZ_196X_SU0218.158 (163)</ta>
            <ta e="T677" id="Seg_6168" s="T675">PKZ_196X_SU0218.159 (164)</ta>
            <ta e="T685" id="Seg_6169" s="T677">PKZ_196X_SU0218.160 (165) </ta>
            <ta e="T693" id="Seg_6170" s="T685">PKZ_196X_SU0218.161 (167)</ta>
            <ta e="T694" id="Seg_6171" s="T693">PKZ_196X_SU0218.162 (168)</ta>
            <ta e="T697" id="Seg_6172" s="T694">PKZ_196X_SU0218.163 (169)</ta>
            <ta e="T703" id="Seg_6173" s="T697">PKZ_196X_SU0218.164 (170)</ta>
            <ta e="T706" id="Seg_6174" s="T703">PKZ_196X_SU0218.165 (171)</ta>
            <ta e="T712" id="Seg_6175" s="T706">PKZ_196X_SU0218.166 (172)</ta>
            <ta e="T716" id="Seg_6176" s="T712">PKZ_196X_SU0218.167 (173)</ta>
            <ta e="T719" id="Seg_6177" s="T716">PKZ_196X_SU0218.168 (174)</ta>
            <ta e="T723" id="Seg_6178" s="T719">PKZ_196X_SU0218.169 (175)</ta>
            <ta e="T728" id="Seg_6179" s="T723">PKZ_196X_SU0218.170 (176)</ta>
            <ta e="T729" id="Seg_6180" s="T728">PKZ_196X_SU0218.171 (177)</ta>
            <ta e="T737" id="Seg_6181" s="T729">PKZ_196X_SU0218.172 (178)</ta>
            <ta e="T740" id="Seg_6182" s="T737">PKZ_196X_SU0218.173 (179)</ta>
            <ta e="T745" id="Seg_6183" s="T740">PKZ_196X_SU0218.174 (180)</ta>
            <ta e="T747" id="Seg_6184" s="T745">PKZ_196X_SU0218.175 (181)</ta>
            <ta e="T753" id="Seg_6185" s="T747">PKZ_196X_SU0218.176 (182)</ta>
            <ta e="T754" id="Seg_6186" s="T753">PKZ_196X_SU0218.177 (183)</ta>
            <ta e="T760" id="Seg_6187" s="T754">PKZ_196X_SU0218.178 (184)</ta>
            <ta e="T774" id="Seg_6188" s="T760">PKZ_196X_SU0218.179 (185)</ta>
            <ta e="T775" id="Seg_6189" s="T774">PKZ_196X_SU0218.180 (186)</ta>
            <ta e="T779" id="Seg_6190" s="T775">PKZ_196X_SU0218.181 (187)</ta>
            <ta e="T785" id="Seg_6191" s="T779">PKZ_196X_SU0218.182 (188)</ta>
            <ta e="T789" id="Seg_6192" s="T785">PKZ_196X_SU0218.183 (189)</ta>
            <ta e="T795" id="Seg_6193" s="T789">PKZ_196X_SU0218.184 (190)</ta>
            <ta e="T800" id="Seg_6194" s="T795">PKZ_196X_SU0218.185 (191)</ta>
            <ta e="T804" id="Seg_6195" s="T800">PKZ_196X_SU0218.186 (192)</ta>
            <ta e="T809" id="Seg_6196" s="T804">PKZ_196X_SU0218.187 (193)</ta>
            <ta e="T812" id="Seg_6197" s="T809">PKZ_196X_SU0218.188 (194)</ta>
            <ta e="T816" id="Seg_6198" s="T812">PKZ_196X_SU0218.189 (195)</ta>
            <ta e="T820" id="Seg_6199" s="T816">PKZ_196X_SU0218.190 (196)</ta>
            <ta e="T823" id="Seg_6200" s="T820">PKZ_196X_SU0218.191 (197)</ta>
            <ta e="T825" id="Seg_6201" s="T823">PKZ_196X_SU0218.192 (198)</ta>
            <ta e="T827" id="Seg_6202" s="T825">PKZ_196X_SU0218.193 (199)</ta>
            <ta e="T828" id="Seg_6203" s="T827">PKZ_196X_SU0218.194 (200)</ta>
            <ta e="T832" id="Seg_6204" s="T828">PKZ_196X_SU0218.195 (201)</ta>
            <ta e="T837" id="Seg_6205" s="T832">PKZ_196X_SU0218.196 (202)</ta>
            <ta e="T842" id="Seg_6206" s="T837">PKZ_196X_SU0218.197 (203)</ta>
            <ta e="T847" id="Seg_6207" s="T842">PKZ_196X_SU0218.198 (204)</ta>
            <ta e="T848" id="Seg_6208" s="T847">PKZ_196X_SU0218.199 (205)</ta>
            <ta e="T852" id="Seg_6209" s="T848">PKZ_196X_SU0218.200 (206)</ta>
            <ta e="T860" id="Seg_6210" s="T852">PKZ_196X_SU0218.201 (207)</ta>
            <ta e="T863" id="Seg_6211" s="T860">PKZ_196X_SU0218.202 (208)</ta>
            <ta e="T870" id="Seg_6212" s="T863">PKZ_196X_SU0218.203 (209)</ta>
            <ta e="T875" id="Seg_6213" s="T870">PKZ_196X_SU0218.204 (210)</ta>
            <ta e="T876" id="Seg_6214" s="T875">PKZ_196X_SU0218.205 (211)</ta>
            <ta e="T882" id="Seg_6215" s="T876">PKZ_196X_SU0218.206 (212)</ta>
            <ta e="T887" id="Seg_6216" s="T882">PKZ_196X_SU0218.207 (213)</ta>
            <ta e="T898" id="Seg_6217" s="T887">PKZ_196X_SU0218.208 (214) </ta>
            <ta e="T899" id="Seg_6218" s="T898">PKZ_196X_SU0218.209 (216)</ta>
            <ta e="T903" id="Seg_6219" s="T899">PKZ_196X_SU0218.210 (217)</ta>
            <ta e="T911" id="Seg_6220" s="T903">PKZ_196X_SU0218.211 (218)</ta>
            <ta e="T912" id="Seg_6221" s="T911">PKZ_196X_SU0218.212 (219)</ta>
            <ta e="T916" id="Seg_6222" s="T912">PKZ_196X_SU0218.213 (220)</ta>
            <ta e="T919" id="Seg_6223" s="T916">PKZ_196X_SU0218.214 (221)</ta>
            <ta e="T922" id="Seg_6224" s="T919">PKZ_196X_SU0218.215 (222)</ta>
            <ta e="T927" id="Seg_6225" s="T922">PKZ_196X_SU0218.216 (223)</ta>
            <ta e="T928" id="Seg_6226" s="T927">PKZ_196X_SU0218.217 (224)</ta>
            <ta e="T933" id="Seg_6227" s="T928">PKZ_196X_SU0218.218 (225)</ta>
            <ta e="T941" id="Seg_6228" s="T933">PKZ_196X_SU0218.219 (226)</ta>
            <ta e="T942" id="Seg_6229" s="T941">PKZ_196X_SU0218.220 (227)</ta>
            <ta e="T947" id="Seg_6230" s="T942">PKZ_196X_SU0218.221 (228)</ta>
            <ta e="T953" id="Seg_6231" s="T947">PKZ_196X_SU0218.222 (229)</ta>
            <ta e="T955" id="Seg_6232" s="T953">PKZ_196X_SU0218.223 (230)</ta>
            <ta e="T956" id="Seg_6233" s="T955">PKZ_196X_SU0218.224 (231)</ta>
            <ta e="T961" id="Seg_6234" s="T956">PKZ_196X_SU0218.225 (232)</ta>
            <ta e="T967" id="Seg_6235" s="T961">PKZ_196X_SU0218.226 (233)</ta>
            <ta e="T970" id="Seg_6236" s="T967">PKZ_196X_SU0218.227 (234)</ta>
            <ta e="T971" id="Seg_6237" s="T970">PKZ_196X_SU0218.228 (235)</ta>
            <ta e="T976" id="Seg_6238" s="T971">PKZ_196X_SU0218.229 (236)</ta>
            <ta e="T981" id="Seg_6239" s="T976">PKZ_196X_SU0218.230 (237)</ta>
            <ta e="T984" id="Seg_6240" s="T981">PKZ_196X_SU0218.231 (238)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_6241" s="T0">Kaŋgaʔ multʼanə (obbərej) šidegöʔ. </ta>
            <ta e="T10" id="Seg_6242" s="T4">Dĭ tănan bögəlbə kĭškələj sabənzi, (vixotkazi). </ta>
            <ta e="T13" id="Seg_6243" s="T10">A (tăn dĭʔnə). </ta>
            <ta e="T16" id="Seg_6244" s="T13">Da (šereʔ-) šeriem. </ta>
            <ta e="T21" id="Seg_6245" s="T16">Nʼe axota kanzittə, ugandə šeriem. </ta>
            <ta e="T29" id="Seg_6246" s="T21">Kazan turagən multʼanə (ej kanʼ-) ej kaliaʔi (obbərej). </ta>
            <ta e="T31" id="Seg_6247" s="T29">Nezeŋ, tibizeŋ. </ta>
            <ta e="T35" id="Seg_6248" s="T31">Nezeŋ onʼiʔ (kaŋg-) kandəʔi. </ta>
            <ta e="T40" id="Seg_6249" s="T35">A tibizeŋ karəldʼan nendəleʔbəʔjə bazoʔ. </ta>
            <ta e="T43" id="Seg_6250" s="T40">Onʼiʔ kandəgaʔi tibizeŋ. </ta>
            <ta e="T44" id="Seg_6251" s="T43">((BRK)). </ta>
            <ta e="T49" id="Seg_6252" s="T44">Miʔ multʼagən koʔbsaŋ šidegöʔ kandəgaʔi. </ta>
            <ta e="T56" id="Seg_6253" s="T49">Dĭgəttə iat kaləj, onʼiʔ koʔbdo, onʼiʔ nʼi. </ta>
            <ta e="T58" id="Seg_6254" s="T56">Băzləj dĭzem. </ta>
            <ta e="T64" id="Seg_6255" s="T58">Dĭzeŋ (šolə-) šonəgaʔi, dĭgəttə tibi (kalaʔbə). </ta>
            <ta e="T69" id="Seg_6256" s="T64">A dĭgəttə măn kalaʔbəm, (băzəjdliam). </ta>
            <ta e="T70" id="Seg_6257" s="T69">((BRK)). </ta>
            <ta e="T74" id="Seg_6258" s="T70">Măn tʼotkam kambi Kazan turanə. </ta>
            <ta e="T79" id="Seg_6259" s="T74">Dĭn kak raz multʼa nendəbiʔi. </ta>
            <ta e="T84" id="Seg_6260" s="T79">Bar băzəbiʔi, măndəʔi: "Kanaʔ moltʼanə". </ta>
            <ta e="T89" id="Seg_6261" s="T84">(A măn=) A dĭ kambi. </ta>
            <ta e="T94" id="Seg_6262" s="T89">Ej tĭmnet, gibər bü kămnasʼtə. </ta>
            <ta e="T104" id="Seg_6263" s="T94">"Pʼeštə kămnəbiam, bar stʼenaʔi büzi (обл -) облила стены эти". </ta>
            <ta e="T108" id="Seg_6264" s="T104">По-русски ((BRK)). </ta>
            <ta e="T111" id="Seg_6265" s="T108">Dĭgəttə băzəjbiam, šobiam. </ta>
            <ta e="T116" id="Seg_6266" s="T111">Ĭmbi băzəjbial, băzəjbial, ej (embiel)? </ta>
            <ta e="T118" id="Seg_6267" s="T116">Ej embiem! </ta>
            <ta e="T119" id="Seg_6268" s="T118">((BRK)). </ta>
            <ta e="T123" id="Seg_6269" s="T119">Dĭgəttə (o-) mănliaʔi:" Kanaʔ! </ta>
            <ta e="T126" id="Seg_6270" s="T123">Pădvalbə uja deʔ. </ta>
            <ta e="T127" id="Seg_6271" s="T126">Kătletaʔinə". </ta>
            <ta e="T129" id="Seg_6272" s="T127">Măn kambiam. </ta>
            <ta e="T136" id="Seg_6273" s="T129">(Nugia-) Nugam, uja iʔgö, a aspaʔ naga. </ta>
            <ta e="T138" id="Seg_6274" s="T136">Dĭgəttə parbiam. </ta>
            <ta e="T143" id="Seg_6275" s="T138">"Ĭmbi uja ej (det-) detlel?" </ta>
            <ta e="T149" id="Seg_6276" s="T143">"Da uja iʔgö, a aspaʔ naga". </ta>
            <ta e="T152" id="Seg_6277" s="T149">Dĭzeŋ davaj kaknarzittə! </ta>
            <ta e="T153" id="Seg_6278" s="T152">((BRK)). </ta>
            <ta e="T156" id="Seg_6279" s="T153">Dĭgəttə mănliaʔi:" Kanaʔ! </ta>
            <ta e="T159" id="Seg_6280" s="T156">Ăgărottə deʔ ăgurcɨʔi". </ta>
            <ta e="T161" id="Seg_6281" s="T159">Măn (kambiam). </ta>
            <ta e="T163" id="Seg_6282" s="T161">Măndərbiam, măndərbiam. </ta>
            <ta e="T167" id="Seg_6283" s="T163">Nĭŋgəliem (toŋtanoʔ), išo ĭmbi-nʼibudʼ. </ta>
            <ta e="T176" id="Seg_6284" s="T167">Dĭgəttə (napal); šide (dɨ-) тыква ibiem i šobiam turanə. </ta>
            <ta e="T181" id="Seg_6285" s="T176">Onʼiʔ ne pătpollʼanə (š-) süʔmiluʔpi. </ta>
            <ta e="T185" id="Seg_6286" s="T181">Onʼiʔ (n-) ne nʼiʔtə. </ta>
            <ta e="T190" id="Seg_6287" s="T185">Aʔpi (min-) măna ej tonuʔpi. </ta>
            <ta e="T191" id="Seg_6288" s="T190">((BRK)). </ta>
            <ta e="T201" id="Seg_6289" s="T191">Dĭgəttə (dĭj-) dĭ nezen (aba=) abat măndə: "Gibər šiʔ nuʔməlambilaʔ? </ta>
            <ta e="T205" id="Seg_6290" s="T201">Deʔkeʔ, a kămnəgaʔ mĭj". </ta>
            <ta e="T214" id="Seg_6291" s="T205">"Abam,— măndə,— kuiol dĭ šide dĭʔə (tɨkla-) тыква deʔpi. </ta>
            <ta e="T224" id="Seg_6292" s="T214">"A ĭmbi öʔlieʔi, kuza ej tĭmnet, a šiʔ (öʔleʔil-) öʔleileʔ". </ta>
            <ta e="T225" id="Seg_6293" s="T224">((BRK)). </ta>
            <ta e="T232" id="Seg_6294" s="T225">Onʼiʔ ne Kazan turanə mĭmbi, (inettə-) inetsi. </ta>
            <ta e="T235" id="Seg_6295" s="T232">(Šo-) Šobi maʔndə. </ta>
            <ta e="T239" id="Seg_6296" s="T235">I kubi: pa iʔbolaʔbə. </ta>
            <ta e="T246" id="Seg_6297" s="T239">(Š-) Dĭgəttə măndə: dĭn červə bar ambi. </ta>
            <ta e="T255" id="Seg_6298" s="T246">(Dĭrg- dĭgət-) Dĭrgit paʔi možna toʔnarzittə i (paʔi boləj). </ta>
            <ta e="T256" id="Seg_6299" s="T255">((BRK)). </ta>
            <ta e="T263" id="Seg_6300" s="T256">Dĭrgit bar abi, üdʼüge, pʼeš možna nendəsʼtə. </ta>
            <ta e="T264" id="Seg_6301" s="T263">((BRK)). </ta>
            <ta e="T268" id="Seg_6302" s="T264">Dĭgəttə Varlam deʔpi kujdərgan. </ta>
            <ta e="T272" id="Seg_6303" s="T268">"Nuldaʔ", (măndə-) măndə dĭʔnə. </ta>
            <ta e="T281" id="Seg_6304" s="T272">Dĭ kambi, bar trubagəndə bü embi, embi, kămnəbi, kămnəbi. </ta>
            <ta e="T286" id="Seg_6305" s="T281">I dĭbər (ko-) kös embi. </ta>
            <ta e="T293" id="Seg_6306" s="T286">Šobi turanə da măndə: "Samovar mʼaŋnaʔbə (bar). </ta>
            <ta e="T297" id="Seg_6307" s="T293">Kujdərgan mʼaŋnaʔbə bar, bü". </ta>
            <ta e="T300" id="Seg_6308" s="T297">Dĭ kambi bar. </ta>
            <ta e="T308" id="Seg_6309" s="T300">"Döbər, măndə, bü eneʔ, a döbər kös eneʔ". </ta>
            <ta e="T312" id="Seg_6310" s="T308">Dĭgəttə (dĭ didro) abi. </ta>
            <ta e="T313" id="Seg_6311" s="T312">((BRK)). </ta>
            <ta e="T320" id="Seg_6312" s="T313">Pa kambiʔi jaʔsittə, i nʼamga bü deʔpiʔi. </ta>
            <ta e="T325" id="Seg_6313" s="T320">(Pa-) Pagən nʼamga bü ibi. </ta>
            <ta e="T326" id="Seg_6314" s="T325">((BRK)). </ta>
            <ta e="T330" id="Seg_6315" s="T326">Sĭre pa, kojü pa. </ta>
            <ta e="T331" id="Seg_6316" s="T330">((BRK)). </ta>
            <ta e="T335" id="Seg_6317" s="T331">Šomi pa, san pa. </ta>
            <ta e="T340" id="Seg_6318" s="T335">((BRK)) говорила (lö-) (len) pa. </ta>
            <ta e="T341" id="Seg_6319" s="T340">((BRK)). </ta>
            <ta e="T345" id="Seg_6320" s="T341">Muʔjə ugandə iʔgö dʼijegən. </ta>
            <ta e="T351" id="Seg_6321" s="T345">Kalal dăk bar, üge üjüzi tonuʔlal. </ta>
            <ta e="T352" id="Seg_6322" s="T351">((BRK)). </ta>
            <ta e="T354" id="Seg_6323" s="T352">Dărowă igel. </ta>
            <ta e="T356" id="Seg_6324" s="T354">Jakše erte! </ta>
            <ta e="T362" id="Seg_6325" s="T356">Jakše ertezi i веселый dʼala tănan. </ta>
            <ta e="T363" id="Seg_6326" s="T362">((BRK)). </ta>
            <ta e="T366" id="Seg_6327" s="T363">Kăde kunolbial teinen? </ta>
            <ta e="T371" id="Seg_6328" s="T366">Moltʼagən băzəbial, padʼi tăŋ kunolbial. </ta>
            <ta e="T376" id="Seg_6329" s="T371">Da (kunolbialəj- kulonolbiam-), jakše băzəbiam. </ta>
            <ta e="T378" id="Seg_6330" s="T376">Bü kabarbi. </ta>
            <ta e="T383" id="Seg_6331" s="T378">Dʼibige bübə ibi, i šišəge. </ta>
            <ta e="T388" id="Seg_6332" s="T383">Ejü mobi ugandə, (tăŋ) ejü. </ta>
            <ta e="T389" id="Seg_6333" s="T388">((BRK)). </ta>
            <ta e="T395" id="Seg_6334" s="T389">Măn moltʼagən băzəbiam, bar bü kămnəbiam. </ta>
            <ta e="T398" id="Seg_6335" s="T395">Dĭgəttə šobi … </ta>
            <ta e="T404" id="Seg_6336" s="T398">((BRK)) накрутил там на это ((BRK)). </ta>
            <ta e="T407" id="Seg_6337" s="T404">Dĭgəttə iet šobi. </ta>
            <ta e="T414" id="Seg_6338" s="T407">Bü naga, a măn detlem da băzəjdlam. </ta>
            <ta e="T423" id="Seg_6339" s="T414">A măn kalla dʼürbim, dĭ bar băzəjdəbi i maʔndə šobi. </ta>
            <ta e="T424" id="Seg_6340" s="T423">((BRK)). </ta>
            <ta e="T430" id="Seg_6341" s="T424">Măn (sʼera-) sʼestram nʼit mĭmbi gorăttə. </ta>
            <ta e="T438" id="Seg_6342" s="T430">Dĭn mašina ibi, kujnegəʔi (băzəsʼtə), dĭgəttə deʔpi (maʔndə). </ta>
            <ta e="T445" id="Seg_6343" s="T438">A teinen kujnegəʔi bar băzəbiʔi i edəbiʔi. </ta>
            <ta e="T446" id="Seg_6344" s="T445">((BRK)). </ta>
            <ta e="T451" id="Seg_6345" s="T446">Măn sʼestranə kazak iat šobi. </ta>
            <ta e="T456" id="Seg_6346" s="T451">Ajaʔ deʔpi (tibi i ne). </ta>
            <ta e="T466" id="Seg_6347" s="T456">Dĭgəttə (ia=) măn ianə măndə:" Kangaʔ, keʔbde ileʔ, малина ileʔ". </ta>
            <ta e="T468" id="Seg_6348" s="T466">Dĭzeŋ kambiʔi. </ta>
            <ta e="T475" id="Seg_6349" s="T468">Dĭ ara mĭbi (onʼiʔ=) onʼiʔ бутылка dĭzeŋ. </ta>
            <ta e="T479" id="Seg_6350" s="T475">Kuʔpi (ta-) dĭn keʔbde. </ta>
            <ta e="T485" id="Seg_6351" s="T479">Dĭzeŋ (nam-) ibiʔi, ibiʔi, kon ibiʔi. </ta>
            <ta e="T488" id="Seg_6352" s="T485">Dĭgəttə amnobiʔi amorzittə. </ta>
            <ta e="T496" id="Seg_6353" s="T488">A măn iam (bə-) măndə: "Davaj döʔə … </ta>
            <ta e="T501" id="Seg_6354" s="T496">Nada keʔbde (koʔsittə) dĭ arazi. </ta>
            <ta e="T505" id="Seg_6355" s="T501">Dĭgəttə kămnəbiʔi, bostə bĭʔpiʔi. </ta>
            <ta e="T510" id="Seg_6356" s="T505">Dĭgəttə šo oʔbdəbiʔi, iʔgö deʔpiʔi. </ta>
            <ta e="T513" id="Seg_6357" s="T510">Šobiʔi maʔndə dĭbər. </ta>
            <ta e="T519" id="Seg_6358" s="T513">Döm nʼilgölia püjetsi, döm nʼilgölia püjetsi. </ta>
            <ta e="T521" id="Seg_6359" s="T519">Putʼəmnia arazi! </ta>
            <ta e="T523" id="Seg_6360" s="T521">Ej tĭmnebi. </ta>
            <ta e="T524" id="Seg_6361" s="T523">((BRK)). </ta>
            <ta e="T529" id="Seg_6362" s="T524">Măn (təni-) teinen ertə uʔbdəbiam. </ta>
            <ta e="T531" id="Seg_6363" s="T529">Pʼeš nendəbiem. </ta>
            <ta e="T535" id="Seg_6364" s="T531">Dĭgəttə tüžöjdə noʔ mĭbiam. </ta>
            <ta e="T537" id="Seg_6365" s="T535">Toltanoʔ bĭʔpiam. </ta>
            <ta e="T540" id="Seg_6366" s="T537">Kambiam, tüžöjdə surdəbiam. </ta>
            <ta e="T542" id="Seg_6367" s="T540">Süttə kămnəbiam. </ta>
            <ta e="T549" id="Seg_6368" s="T542">A dĭgəttə kambiam nükenə, mĭj (ka- )… </ta>
            <ta e="T551" id="Seg_6369" s="T549">Mĭj mĭnzerbiem. </ta>
            <ta e="T557" id="Seg_6370" s="T551">Dĭgəttə kambiam nükenə, (mĭj=) mĭj deʔpiem. </ta>
            <ta e="T562" id="Seg_6371" s="T557">Dĭ bar (mĭn-) mĭlleʔbə (üjügən). </ta>
            <ta e="T565" id="Seg_6372" s="T562">"Ĭmbi, tural băzəbi?" </ta>
            <ta e="T568" id="Seg_6373" s="T565">"Dʼok, esseŋ băzəbiʔi". </ta>
            <ta e="T574" id="Seg_6374" s="T568">Dĭgəttə šobi döbər, krăvatʼtʼə bar (saʔməluʔpi). </ta>
            <ta e="T578" id="Seg_6375" s="T574">Măn măndəm: "Iʔ saʔmaʔ". </ta>
            <ta e="T580" id="Seg_6376" s="T578">Погоди… </ta>
            <ta e="T584" id="Seg_6377" s="T580">A mĭjdə bar (săgəluʔpi). </ta>
            <ta e="T587" id="Seg_6378" s="T584">Büžü ej kuləj. </ta>
            <ta e="T588" id="Seg_6379" s="T587">((BRK)). </ta>
            <ta e="T594" id="Seg_6380" s="T588">Dĭgəttə dĭ nükebə (nuʔgĭ-) kambiam Anissʼanə. </ta>
            <ta e="T597" id="Seg_6381" s="T594">Kundʼo ej mĭmbiem. </ta>
            <ta e="T600" id="Seg_6382" s="T597">Šobiam (dibər=) dĭʔnə. </ta>
            <ta e="T605" id="Seg_6383" s="T600">Dĭzeŋ bar dibər nʼitsi kudonzlaʔbə. </ta>
            <ta e="T606" id="Seg_6384" s="T605">((BRK)). </ta>
            <ta e="T613" id="Seg_6385" s="T606">Dĭgəttə dĭ Anissʼa bar kudonzəbi (n-) nʼit. </ta>
            <ta e="T619" id="Seg_6386" s="T613">A nʼit bar dĭʔnə ĭmbi-nʼibudʼ dʼăbaktərləj. </ta>
            <ta e="T623" id="Seg_6387" s="T619">Măn măndəm:" Iʔ kürümaʔ. </ta>
            <ta e="T628" id="Seg_6388" s="T623">(Ia-) Ian ĭmbidə ej (mănaʔ)". </ta>
            <ta e="T632" id="Seg_6389" s="T628">A dĭ üge kürümnie. </ta>
            <ta e="T637" id="Seg_6390" s="T632">A iat bar davaj dʼorzittə. </ta>
            <ta e="T643" id="Seg_6391" s="T637">"Kuiol, kăde dĭzeŋ (ipeksi), măna (bădləʔi). </ta>
            <ta e="T649" id="Seg_6392" s="T643">Măn ipek amnia, i išo kudonzlaʔbə". </ta>
            <ta e="T656" id="Seg_6393" s="T649">Dĭgəttə (šalaʔ-) šala amnobibaʔ, dĭgəttə Jelʼa šobi. </ta>
            <ta e="T660" id="Seg_6394" s="T656">"Zdărowă igel"—" Zdărowă igel". </ta>
            <ta e="T662" id="Seg_6395" s="T660">"Ĭmbi šobial?" </ta>
            <ta e="T664" id="Seg_6396" s="T662">"Ipek măndərbiam". </ta>
            <ta e="T672" id="Seg_6397" s="T664">A măn Anissʼanə mălliam: "Mĭʔ dĭʔnə onʼiʔ ipek". </ta>
            <ta e="T675" id="Seg_6398" s="T672">Dĭ mĭbi dĭʔnə. </ta>
            <ta e="T677" id="Seg_6399" s="T675">"Kumen pizittə?" </ta>
            <ta e="T685" id="Seg_6400" s="T677">A măn măndəm: "(Šalaʔ) ej iʔgö kak (măgăzingəʔ)". </ta>
            <ta e="T693" id="Seg_6401" s="T685">Dĭgəttə (dĭn=) dĭ dĭʔnə mĭbi teʔtə bieʔ kăpʼejkaʔi. </ta>
            <ta e="T694" id="Seg_6402" s="T693">((BRK)). </ta>
            <ta e="T697" id="Seg_6403" s="T694">Dĭgəttə măn šobiam. </ta>
            <ta e="T703" id="Seg_6404" s="T697">Tüžöjbə (bĭtəjbiem), buzo bĭtəlbiem, noʔ mĭbiem. </ta>
            <ta e="T706" id="Seg_6405" s="T703">Dĭgəttə büjle mĭmbiem. </ta>
            <ta e="T712" id="Seg_6406" s="T706">I (š-) šobiam turanə, ujum (kănnambi). </ta>
            <ta e="T716" id="Seg_6407" s="T712">Pʼeštə (š- s-) (sʼabiam). </ta>
            <ta e="T719" id="Seg_6408" s="T716">Pʼeštə (s-) (sʼabiam). </ta>
            <ta e="T723" id="Seg_6409" s="T719">Dĭgəttə ujum (ejüm-) (ojimulem). </ta>
            <ta e="T728" id="Seg_6410" s="T723">Dĭzeŋ (so-) šobiʔi i dʼăbaktərzittə. </ta>
            <ta e="T729" id="Seg_6411" s="T728">((BRK)). </ta>
            <ta e="T737" id="Seg_6412" s="T729">Dĭgəttə dʼăbaktərzittə nada, a dĭn mašina bar kürümnaʔbə. </ta>
            <ta e="T740" id="Seg_6413" s="T737">Ej mĭlie dʼăbaktərzittə. </ta>
            <ta e="T745" id="Seg_6414" s="T740">Miʔ baʔluʔpibaʔ, a dĭzeŋ kalla dʼürbiʔi. </ta>
            <ta e="T747" id="Seg_6415" s="T745">Ej dʼăbaktərbiabaʔ. </ta>
            <ta e="T753" id="Seg_6416" s="T747">A tüj šobiʔi bazoʔ (s-) dʼăbaktərzittə. </ta>
            <ta e="T754" id="Seg_6417" s="T753">((BRK)). </ta>
            <ta e="T760" id="Seg_6418" s="T754">Šobiʔi, a măn măndəm:" Gijen ibileʔ?" </ta>
            <ta e="T774" id="Seg_6419" s="T760">"Da pa kürbibeʔ, bostə tura kürbibeʔ, i urgajan turanə (kürbi-) kürbibeʔ, i döbər šobibaʔ". </ta>
            <ta e="T775" id="Seg_6420" s="T774">((BRK)). </ta>
            <ta e="T779" id="Seg_6421" s="T775">Dĭgəttə iat šobi bălʼnʼitsagən. </ta>
            <ta e="T785" id="Seg_6422" s="T779">Davaj essem (kudonz-): ĭmbidə ej (aliaʔi). </ta>
            <ta e="T789" id="Seg_6423" s="T785">"(Na), paʔi eneʔ ăgradagən. </ta>
            <ta e="T795" id="Seg_6424" s="T789">A to abal šoləj, ĭmbidə ej mĭləj". </ta>
            <ta e="T800" id="Seg_6425" s="T795">Dĭ kambi da davaj enzittə. </ta>
            <ta e="T804" id="Seg_6426" s="T800">Dĭgəttə šobi, multʼagən băzəjdəbiʔi. </ta>
            <ta e="T809" id="Seg_6427" s="T804">Dĭgəttə măna măndəʔi:" Kanaʔ moltʼanə". </ta>
            <ta e="T812" id="Seg_6428" s="T809">Măn kambiam băzəjdəsʼtə. </ta>
            <ta e="T816" id="Seg_6429" s="T812">Dĭgəttə Jelʼa šobi, băzəjdəsʼtə. </ta>
            <ta e="T820" id="Seg_6430" s="T816">Dĭgəttə tibi šobi, băzəjdəbi. </ta>
            <ta e="T823" id="Seg_6431" s="T820">Dĭgəttə abat šobi. </ta>
            <ta e="T825" id="Seg_6432" s="T823">Deʔpi čulkiʔi. </ta>
            <ta e="T827" id="Seg_6433" s="T825">(Piʔmeʔi) deʔpi. </ta>
            <ta e="T828" id="Seg_6434" s="T827">((BRK)). </ta>
            <ta e="T832" id="Seg_6435" s="T828">Dĭgəttə šobi, ara deʔpi. </ta>
            <ta e="T837" id="Seg_6436" s="T832">Onʼiʔ tibi dĭm deʔpi inetsi. </ta>
            <ta e="T842" id="Seg_6437" s="T837">Dĭgəttə dĭ dĭʔnə ara kămnəbi. </ta>
            <ta e="T847" id="Seg_6438" s="T842">Šalaʔi amorbi, (ka-) kambi maːʔndə. </ta>
            <ta e="T848" id="Seg_6439" s="T847">((BRK)). </ta>
            <ta e="T852" id="Seg_6440" s="T848">Dĭ ne ugandə kurojoʔ. </ta>
            <ta e="T860" id="Seg_6441" s="T852">Pa ibi da büzəm (pa-) pazi toʔnarbi, sʼimattə. </ta>
            <ta e="T863" id="Seg_6442" s="T860">Kăjdə bar mʼaŋnaʔbə. </ta>
            <ta e="T870" id="Seg_6443" s="T863">A măn bostə büzəm ej münöriam, (udazi). </ta>
            <ta e="T875" id="Seg_6444" s="T870">Udazi toʔlim, šalaʔi bar (bĭtluʔpi). </ta>
            <ta e="T876" id="Seg_6445" s="T875">((BRK)). </ta>
            <ta e="T882" id="Seg_6446" s="T876">Măn turagən kürbiem mašinam, i … </ta>
            <ta e="T887" id="Seg_6447" s="T882">Dĭzeŋ oʔb, šide, nagur, teʔtə. </ta>
            <ta e="T898" id="Seg_6448" s="T887">Dĭgəttə Vanʼa măndə: "Büzö, kereʔ, i šoškaʔi",— a miʔ bar kaknarbibaʔ. </ta>
            <ta e="T899" id="Seg_6449" s="T898">((BRK)). </ta>
            <ta e="T903" id="Seg_6450" s="T899">Teinen padʼi iʔgö dʼăbaktərbibaʔ. </ta>
            <ta e="T911" id="Seg_6451" s="T903">Tüj nada maʔnʼi kanzittə, amorzittə, iʔbəsʼtə, (šal-) (šalaʔi). </ta>
            <ta e="T912" id="Seg_6452" s="T911">((BRK)). </ta>
            <ta e="T916" id="Seg_6453" s="T912">Bor bar nʼiʔtə kandəga. </ta>
            <ta e="T919" id="Seg_6454" s="T916">Ugandə šišəge moləj. </ta>
            <ta e="T922" id="Seg_6455" s="T919">Teinen nüdʼin sĭre. </ta>
            <ta e="T927" id="Seg_6456" s="T922">Ertən uʔbdəbiam— bar šišəge molambi. </ta>
            <ta e="T928" id="Seg_6457" s="T927">((BRK)). </ta>
            <ta e="T933" id="Seg_6458" s="T928">Miʔnʼibeʔ teinen nada kanzittə Tanʼanə. </ta>
            <ta e="T941" id="Seg_6459" s="T933">Štobɨ dĭ ipek ibi (Văznesenkagə) i deʔpi miʔnʼibeʔ. </ta>
            <ta e="T942" id="Seg_6460" s="T941">((BRK)). </ta>
            <ta e="T947" id="Seg_6461" s="T942">((…)) nada aktʼam izittə diʔnə. </ta>
            <ta e="T953" id="Seg_6462" s="T947">Ej boskəndə deʔsittə dĭʔə inetsi mĭlləj. </ta>
            <ta e="T955" id="Seg_6463" s="T953">I detləj. </ta>
            <ta e="T956" id="Seg_6464" s="T955">((BRK)). </ta>
            <ta e="T961" id="Seg_6465" s="T956">Ej tĭmnem, iləj ilʼi ej. </ta>
            <ta e="T967" id="Seg_6466" s="T961">Nada kanzittə dĭzi dʼăbaktərzittə, možet iləj. </ta>
            <ta e="T970" id="Seg_6467" s="T967">No, kanaʔ, dʼăbaktəraʔ. </ta>
            <ta e="T971" id="Seg_6468" s="T970">((BRK)). </ta>
            <ta e="T976" id="Seg_6469" s="T971">Dĭ možet mĭlləj măgăzingən dĭn. </ta>
            <ta e="T981" id="Seg_6470" s="T976">Очередь nuzittə nada, ipek izittə. </ta>
            <ta e="T984" id="Seg_6471" s="T981">Măn ej iləm. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_6472" s="T0">kaŋ-gaʔ</ta>
            <ta e="T2" id="Seg_6473" s="T1">multʼa-nə</ta>
            <ta e="T3" id="Seg_6474" s="T2">obbərej</ta>
            <ta e="T4" id="Seg_6475" s="T3">šide-göʔ</ta>
            <ta e="T5" id="Seg_6476" s="T4">dĭ</ta>
            <ta e="T6" id="Seg_6477" s="T5">tănan</ta>
            <ta e="T7" id="Seg_6478" s="T6">bögəl-bə</ta>
            <ta e="T8" id="Seg_6479" s="T7">kĭškə-lə-j</ta>
            <ta e="T9" id="Seg_6480" s="T8">sabən-zi</ta>
            <ta e="T10" id="Seg_6481" s="T9">vixotka-zi</ta>
            <ta e="T11" id="Seg_6482" s="T10">a</ta>
            <ta e="T12" id="Seg_6483" s="T11">tăn</ta>
            <ta e="T13" id="Seg_6484" s="T12">dĭʔ-nə</ta>
            <ta e="T14" id="Seg_6485" s="T13">da</ta>
            <ta e="T16" id="Seg_6486" s="T15">šer-ie-m</ta>
            <ta e="T17" id="Seg_6487" s="T16">nʼe</ta>
            <ta e="T18" id="Seg_6488" s="T17">axota</ta>
            <ta e="T19" id="Seg_6489" s="T18">kan-zittə</ta>
            <ta e="T20" id="Seg_6490" s="T19">ugandə</ta>
            <ta e="T21" id="Seg_6491" s="T20">šer-ie-m</ta>
            <ta e="T22" id="Seg_6492" s="T21">kaza-n</ta>
            <ta e="T23" id="Seg_6493" s="T22">tura-gən</ta>
            <ta e="T24" id="Seg_6494" s="T23">multʼa-nə</ta>
            <ta e="T25" id="Seg_6495" s="T24">ej</ta>
            <ta e="T27" id="Seg_6496" s="T26">ej</ta>
            <ta e="T28" id="Seg_6497" s="T27">ka-lia-ʔi</ta>
            <ta e="T29" id="Seg_6498" s="T28">obbərej</ta>
            <ta e="T30" id="Seg_6499" s="T29">ne-zeŋ</ta>
            <ta e="T31" id="Seg_6500" s="T30">tibi-zeŋ</ta>
            <ta e="T32" id="Seg_6501" s="T31">ne-zeŋ</ta>
            <ta e="T33" id="Seg_6502" s="T32">onʼiʔ</ta>
            <ta e="T35" id="Seg_6503" s="T34">kandə-ʔi</ta>
            <ta e="T36" id="Seg_6504" s="T35">a</ta>
            <ta e="T37" id="Seg_6505" s="T36">tibi-zeŋ</ta>
            <ta e="T38" id="Seg_6506" s="T37">karəldʼan</ta>
            <ta e="T39" id="Seg_6507" s="T38">nendə-leʔbə-ʔjə</ta>
            <ta e="T40" id="Seg_6508" s="T39">bazoʔ</ta>
            <ta e="T41" id="Seg_6509" s="T40">onʼiʔ</ta>
            <ta e="T42" id="Seg_6510" s="T41">kan-də-ga-ʔi</ta>
            <ta e="T43" id="Seg_6511" s="T42">tibi-zeŋ</ta>
            <ta e="T45" id="Seg_6512" s="T44">miʔ</ta>
            <ta e="T46" id="Seg_6513" s="T45">multʼa-gən</ta>
            <ta e="T47" id="Seg_6514" s="T46">koʔb-saŋ</ta>
            <ta e="T48" id="Seg_6515" s="T47">šide-göʔ</ta>
            <ta e="T49" id="Seg_6516" s="T48">kan-də-ga-ʔi</ta>
            <ta e="T50" id="Seg_6517" s="T49">dĭgəttə</ta>
            <ta e="T51" id="Seg_6518" s="T50">ia-t</ta>
            <ta e="T52" id="Seg_6519" s="T51">ka-lə-j</ta>
            <ta e="T53" id="Seg_6520" s="T52">onʼiʔ</ta>
            <ta e="T54" id="Seg_6521" s="T53">koʔbdo</ta>
            <ta e="T55" id="Seg_6522" s="T54">onʼiʔ</ta>
            <ta e="T56" id="Seg_6523" s="T55">nʼi</ta>
            <ta e="T57" id="Seg_6524" s="T56">băz-lə-j</ta>
            <ta e="T58" id="Seg_6525" s="T57">dĭ-zem</ta>
            <ta e="T59" id="Seg_6526" s="T58">dĭ-zeŋ</ta>
            <ta e="T61" id="Seg_6527" s="T60">šonə-ga-ʔi</ta>
            <ta e="T62" id="Seg_6528" s="T61">dĭgəttə</ta>
            <ta e="T63" id="Seg_6529" s="T62">tibi</ta>
            <ta e="T64" id="Seg_6530" s="T63">ka-laʔbə</ta>
            <ta e="T65" id="Seg_6531" s="T64">a</ta>
            <ta e="T66" id="Seg_6532" s="T65">dĭgəttə</ta>
            <ta e="T67" id="Seg_6533" s="T66">măn</ta>
            <ta e="T68" id="Seg_6534" s="T67">ka-laʔbə-m</ta>
            <ta e="T69" id="Seg_6535" s="T68">băzə-jd-lia-m</ta>
            <ta e="T71" id="Seg_6536" s="T70">măn</ta>
            <ta e="T72" id="Seg_6537" s="T71">tʼotka-m</ta>
            <ta e="T73" id="Seg_6538" s="T72">kam-bi</ta>
            <ta e="T988" id="Seg_6539" s="T73">Kazan</ta>
            <ta e="T74" id="Seg_6540" s="T988">tura-nə</ta>
            <ta e="T75" id="Seg_6541" s="T74">dĭn</ta>
            <ta e="T76" id="Seg_6542" s="T75">kak</ta>
            <ta e="T77" id="Seg_6543" s="T76">raz</ta>
            <ta e="T78" id="Seg_6544" s="T77">multʼa</ta>
            <ta e="T79" id="Seg_6545" s="T78">nendə-bi-ʔi</ta>
            <ta e="T80" id="Seg_6546" s="T79">bar</ta>
            <ta e="T81" id="Seg_6547" s="T80">băzə-bi-ʔi</ta>
            <ta e="T82" id="Seg_6548" s="T81">măn-də-ʔi</ta>
            <ta e="T83" id="Seg_6549" s="T82">kan-a-ʔ</ta>
            <ta e="T84" id="Seg_6550" s="T83">moltʼa-nə</ta>
            <ta e="T85" id="Seg_6551" s="T84">a</ta>
            <ta e="T86" id="Seg_6552" s="T85">măn</ta>
            <ta e="T87" id="Seg_6553" s="T86">a</ta>
            <ta e="T88" id="Seg_6554" s="T87">dĭ</ta>
            <ta e="T89" id="Seg_6555" s="T88">kam-bi</ta>
            <ta e="T90" id="Seg_6556" s="T89">ej</ta>
            <ta e="T91" id="Seg_6557" s="T90">tĭmne-t</ta>
            <ta e="T92" id="Seg_6558" s="T91">gibər</ta>
            <ta e="T93" id="Seg_6559" s="T92">bü</ta>
            <ta e="T94" id="Seg_6560" s="T93">kămna-sʼtə</ta>
            <ta e="T95" id="Seg_6561" s="T94">pʼeš-tə</ta>
            <ta e="T96" id="Seg_6562" s="T95">kămnə-bia-m</ta>
            <ta e="T97" id="Seg_6563" s="T96">bar</ta>
            <ta e="T98" id="Seg_6564" s="T97">stʼena-ʔi</ta>
            <ta e="T99" id="Seg_6565" s="T98">bü-zi</ta>
            <ta e="T109" id="Seg_6566" s="T108">dĭgəttə</ta>
            <ta e="T110" id="Seg_6567" s="T109">băzəj-bia-m</ta>
            <ta e="T111" id="Seg_6568" s="T110">šo-bia-m</ta>
            <ta e="T112" id="Seg_6569" s="T111">ĭmbi</ta>
            <ta e="T113" id="Seg_6570" s="T112">băzəj-bia-l</ta>
            <ta e="T114" id="Seg_6571" s="T113">băzəj-bia-l</ta>
            <ta e="T115" id="Seg_6572" s="T114">ej</ta>
            <ta e="T116" id="Seg_6573" s="T115">em-bie-l</ta>
            <ta e="T117" id="Seg_6574" s="T116">ej</ta>
            <ta e="T118" id="Seg_6575" s="T117">em-bie-m</ta>
            <ta e="T120" id="Seg_6576" s="T119">dĭgəttə</ta>
            <ta e="T122" id="Seg_6577" s="T121">măn-lia-ʔi</ta>
            <ta e="T123" id="Seg_6578" s="T122">kan-a-ʔ</ta>
            <ta e="T124" id="Seg_6579" s="T123">pădval-bə</ta>
            <ta e="T125" id="Seg_6580" s="T124">uja</ta>
            <ta e="T126" id="Seg_6581" s="T125">de-ʔ</ta>
            <ta e="T127" id="Seg_6582" s="T126">kătleta-ʔi-nə</ta>
            <ta e="T128" id="Seg_6583" s="T127">măn</ta>
            <ta e="T129" id="Seg_6584" s="T128">kam-bia-m</ta>
            <ta e="T131" id="Seg_6585" s="T130">nu-ga-m</ta>
            <ta e="T132" id="Seg_6586" s="T131">uja</ta>
            <ta e="T133" id="Seg_6587" s="T132">iʔgö</ta>
            <ta e="T134" id="Seg_6588" s="T133">a</ta>
            <ta e="T135" id="Seg_6589" s="T134">aspaʔ</ta>
            <ta e="T136" id="Seg_6590" s="T135">naga</ta>
            <ta e="T137" id="Seg_6591" s="T136">dĭgəttə</ta>
            <ta e="T138" id="Seg_6592" s="T137">par-bia-m</ta>
            <ta e="T139" id="Seg_6593" s="T138">ĭmbi</ta>
            <ta e="T140" id="Seg_6594" s="T139">uja</ta>
            <ta e="T141" id="Seg_6595" s="T140">ej</ta>
            <ta e="T143" id="Seg_6596" s="T142">det-lie-l</ta>
            <ta e="T144" id="Seg_6597" s="T143">da</ta>
            <ta e="T145" id="Seg_6598" s="T144">uja</ta>
            <ta e="T146" id="Seg_6599" s="T145">iʔgö</ta>
            <ta e="T147" id="Seg_6600" s="T146">a</ta>
            <ta e="T148" id="Seg_6601" s="T147">aspaʔ</ta>
            <ta e="T149" id="Seg_6602" s="T148">naga</ta>
            <ta e="T150" id="Seg_6603" s="T149">dĭ-zeŋ</ta>
            <ta e="T151" id="Seg_6604" s="T150">davaj</ta>
            <ta e="T152" id="Seg_6605" s="T151">kaknar-zittə</ta>
            <ta e="T154" id="Seg_6606" s="T153">dĭgəttə</ta>
            <ta e="T155" id="Seg_6607" s="T154">măn-lia-ʔi</ta>
            <ta e="T156" id="Seg_6608" s="T155">kan-a-ʔ</ta>
            <ta e="T157" id="Seg_6609" s="T156">ăgărot-tə</ta>
            <ta e="T158" id="Seg_6610" s="T157">de-ʔ</ta>
            <ta e="T159" id="Seg_6611" s="T158">ăgurcɨ-ʔi</ta>
            <ta e="T160" id="Seg_6612" s="T159">măn</ta>
            <ta e="T161" id="Seg_6613" s="T160">kam-bia-m</ta>
            <ta e="T162" id="Seg_6614" s="T161">măndə-r-bia-m</ta>
            <ta e="T163" id="Seg_6615" s="T162">măndə-r-bia-m</ta>
            <ta e="T164" id="Seg_6616" s="T163">nĭŋgə-lie-m</ta>
            <ta e="T165" id="Seg_6617" s="T164">toŋtanoʔ</ta>
            <ta e="T166" id="Seg_6618" s="T165">išo</ta>
            <ta e="T167" id="Seg_6619" s="T166">ĭmbi=nʼibudʼ</ta>
            <ta e="T168" id="Seg_6620" s="T167">dĭgəttə</ta>
            <ta e="T169" id="Seg_6621" s="T168">napal</ta>
            <ta e="T170" id="Seg_6622" s="T169">šide</ta>
            <ta e="T173" id="Seg_6623" s="T172">i-bie-m</ta>
            <ta e="T174" id="Seg_6624" s="T173">i</ta>
            <ta e="T175" id="Seg_6625" s="T174">šo-bia-m</ta>
            <ta e="T176" id="Seg_6626" s="T175">tura-nə</ta>
            <ta e="T177" id="Seg_6627" s="T176">onʼiʔ</ta>
            <ta e="T178" id="Seg_6628" s="T177">ne</ta>
            <ta e="T179" id="Seg_6629" s="T178">pătpollʼa-nə</ta>
            <ta e="T181" id="Seg_6630" s="T180">süʔmi-luʔ-pi</ta>
            <ta e="T182" id="Seg_6631" s="T181">onʼiʔ</ta>
            <ta e="T184" id="Seg_6632" s="T183">ne</ta>
            <ta e="T185" id="Seg_6633" s="T184">nʼiʔtə</ta>
            <ta e="T186" id="Seg_6634" s="T185">aʔpi</ta>
            <ta e="T188" id="Seg_6635" s="T187">măna</ta>
            <ta e="T189" id="Seg_6636" s="T188">ej</ta>
            <ta e="T190" id="Seg_6637" s="T189">tonuʔ-pi</ta>
            <ta e="T192" id="Seg_6638" s="T191">dĭgəttə</ta>
            <ta e="T194" id="Seg_6639" s="T193">dĭ</ta>
            <ta e="T195" id="Seg_6640" s="T194">ne-zen</ta>
            <ta e="T196" id="Seg_6641" s="T195">aba</ta>
            <ta e="T197" id="Seg_6642" s="T196">aba-t</ta>
            <ta e="T198" id="Seg_6643" s="T197">măn-də</ta>
            <ta e="T199" id="Seg_6644" s="T198">gibər</ta>
            <ta e="T200" id="Seg_6645" s="T199">šiʔ</ta>
            <ta e="T201" id="Seg_6646" s="T200">nuʔmə-lam-bi-laʔ</ta>
            <ta e="T202" id="Seg_6647" s="T201">deʔ-keʔ</ta>
            <ta e="T203" id="Seg_6648" s="T202">a</ta>
            <ta e="T204" id="Seg_6649" s="T203">kămnə-gaʔ</ta>
            <ta e="T205" id="Seg_6650" s="T204">mĭj</ta>
            <ta e="T206" id="Seg_6651" s="T205">aba-m</ta>
            <ta e="T207" id="Seg_6652" s="T206">măn-də</ta>
            <ta e="T208" id="Seg_6653" s="T207">ku-io-l</ta>
            <ta e="T209" id="Seg_6654" s="T208">dĭ</ta>
            <ta e="T210" id="Seg_6655" s="T209">šide</ta>
            <ta e="T211" id="Seg_6656" s="T210">dĭʔə</ta>
            <ta e="T214" id="Seg_6657" s="T213">deʔ-pi</ta>
            <ta e="T215" id="Seg_6658" s="T214">a</ta>
            <ta e="T216" id="Seg_6659" s="T215">ĭmbi</ta>
            <ta e="T217" id="Seg_6660" s="T216">öʔ-lie-ʔi</ta>
            <ta e="T218" id="Seg_6661" s="T217">kuza</ta>
            <ta e="T219" id="Seg_6662" s="T218">ej</ta>
            <ta e="T220" id="Seg_6663" s="T219">tĭmne-t</ta>
            <ta e="T221" id="Seg_6664" s="T220">a</ta>
            <ta e="T222" id="Seg_6665" s="T221">šiʔ</ta>
            <ta e="T226" id="Seg_6666" s="T225">onʼiʔ</ta>
            <ta e="T227" id="Seg_6667" s="T226">ne</ta>
            <ta e="T228" id="Seg_6668" s="T227">Kazan</ta>
            <ta e="T229" id="Seg_6669" s="T228">tura-nə</ta>
            <ta e="T230" id="Seg_6670" s="T229">mĭm-bi</ta>
            <ta e="T232" id="Seg_6671" s="T231">ine-t-si</ta>
            <ta e="T234" id="Seg_6672" s="T233">šo-bi</ta>
            <ta e="T235" id="Seg_6673" s="T234">maʔ-ndə</ta>
            <ta e="T236" id="Seg_6674" s="T235">i</ta>
            <ta e="T237" id="Seg_6675" s="T236">ku-bi</ta>
            <ta e="T238" id="Seg_6676" s="T237">pa</ta>
            <ta e="T239" id="Seg_6677" s="T238">iʔbo-laʔbə</ta>
            <ta e="T241" id="Seg_6678" s="T240">dĭgəttə</ta>
            <ta e="T242" id="Seg_6679" s="T241">măn-də</ta>
            <ta e="T243" id="Seg_6680" s="T242">dĭ-n</ta>
            <ta e="T244" id="Seg_6681" s="T243">červə</ta>
            <ta e="T245" id="Seg_6682" s="T244">bar</ta>
            <ta e="T246" id="Seg_6683" s="T245">am-bi</ta>
            <ta e="T249" id="Seg_6684" s="T248">dĭrgit</ta>
            <ta e="T250" id="Seg_6685" s="T249">pa-ʔi</ta>
            <ta e="T251" id="Seg_6686" s="T250">možna</ta>
            <ta e="T252" id="Seg_6687" s="T251">toʔ-nar-zittə</ta>
            <ta e="T253" id="Seg_6688" s="T252">i</ta>
            <ta e="T254" id="Seg_6689" s="T253">pa-ʔi</ta>
            <ta e="T255" id="Seg_6690" s="T254">boləj</ta>
            <ta e="T257" id="Seg_6691" s="T256">dĭrgit</ta>
            <ta e="T258" id="Seg_6692" s="T257">bar</ta>
            <ta e="T259" id="Seg_6693" s="T258">a-bi</ta>
            <ta e="T260" id="Seg_6694" s="T259">üdʼüge</ta>
            <ta e="T261" id="Seg_6695" s="T260">pʼeš</ta>
            <ta e="T262" id="Seg_6696" s="T261">možna</ta>
            <ta e="T263" id="Seg_6697" s="T262">nendə-sʼtə</ta>
            <ta e="T265" id="Seg_6698" s="T264">dĭgəttə</ta>
            <ta e="T266" id="Seg_6699" s="T265">Varlam</ta>
            <ta e="T267" id="Seg_6700" s="T266">deʔ-pi</ta>
            <ta e="T268" id="Seg_6701" s="T267">kujdərgan</ta>
            <ta e="T269" id="Seg_6702" s="T268">nulda-ʔ</ta>
            <ta e="T271" id="Seg_6703" s="T270">măn-də</ta>
            <ta e="T272" id="Seg_6704" s="T271">dĭʔ-nə</ta>
            <ta e="T273" id="Seg_6705" s="T272">dĭ</ta>
            <ta e="T274" id="Seg_6706" s="T273">kam-bi</ta>
            <ta e="T275" id="Seg_6707" s="T274">bar</ta>
            <ta e="T276" id="Seg_6708" s="T275">truba-gəndə</ta>
            <ta e="T277" id="Seg_6709" s="T276">bü</ta>
            <ta e="T278" id="Seg_6710" s="T277">em-bi</ta>
            <ta e="T279" id="Seg_6711" s="T278">em-bi</ta>
            <ta e="T280" id="Seg_6712" s="T279">kămnə-bi</ta>
            <ta e="T281" id="Seg_6713" s="T280">kămnə-bi</ta>
            <ta e="T282" id="Seg_6714" s="T281">i</ta>
            <ta e="T283" id="Seg_6715" s="T282">dĭbər</ta>
            <ta e="T285" id="Seg_6716" s="T284">kös</ta>
            <ta e="T286" id="Seg_6717" s="T285">em-bi</ta>
            <ta e="T287" id="Seg_6718" s="T286">šo-bi</ta>
            <ta e="T288" id="Seg_6719" s="T287">tura-nə</ta>
            <ta e="T289" id="Seg_6720" s="T288">da</ta>
            <ta e="T290" id="Seg_6721" s="T289">măn-də</ta>
            <ta e="T291" id="Seg_6722" s="T290">samovar</ta>
            <ta e="T292" id="Seg_6723" s="T291">mʼaŋ-naʔbə</ta>
            <ta e="T293" id="Seg_6724" s="T292">bar</ta>
            <ta e="T294" id="Seg_6725" s="T293">kujdərgan</ta>
            <ta e="T295" id="Seg_6726" s="T294">mʼaŋ-naʔbə</ta>
            <ta e="T296" id="Seg_6727" s="T295">bar</ta>
            <ta e="T297" id="Seg_6728" s="T296">bü</ta>
            <ta e="T298" id="Seg_6729" s="T297">dĭ</ta>
            <ta e="T299" id="Seg_6730" s="T298">kam-bi</ta>
            <ta e="T300" id="Seg_6731" s="T299">bar</ta>
            <ta e="T301" id="Seg_6732" s="T300">döbər</ta>
            <ta e="T302" id="Seg_6733" s="T301">măn-də</ta>
            <ta e="T303" id="Seg_6734" s="T302">bü</ta>
            <ta e="T304" id="Seg_6735" s="T303">en-e-ʔ</ta>
            <ta e="T305" id="Seg_6736" s="T304">a</ta>
            <ta e="T306" id="Seg_6737" s="T305">döbər</ta>
            <ta e="T307" id="Seg_6738" s="T306">kös</ta>
            <ta e="T308" id="Seg_6739" s="T307">en-e-ʔ</ta>
            <ta e="T309" id="Seg_6740" s="T308">dĭgəttə</ta>
            <ta e="T310" id="Seg_6741" s="T309">dĭ</ta>
            <ta e="T311" id="Seg_6742" s="T310">didro</ta>
            <ta e="T312" id="Seg_6743" s="T311">a-bi</ta>
            <ta e="T314" id="Seg_6744" s="T313">pa</ta>
            <ta e="T315" id="Seg_6745" s="T314">kam-bi-ʔi</ta>
            <ta e="T316" id="Seg_6746" s="T315">jaʔ-sittə</ta>
            <ta e="T317" id="Seg_6747" s="T316">i</ta>
            <ta e="T318" id="Seg_6748" s="T317">nʼamga</ta>
            <ta e="T319" id="Seg_6749" s="T318">bü</ta>
            <ta e="T320" id="Seg_6750" s="T319">deʔ-pi-ʔi</ta>
            <ta e="T322" id="Seg_6751" s="T321">pa-gən</ta>
            <ta e="T323" id="Seg_6752" s="T322">nʼamga</ta>
            <ta e="T324" id="Seg_6753" s="T323">bü</ta>
            <ta e="T325" id="Seg_6754" s="T324">i-bi</ta>
            <ta e="T327" id="Seg_6755" s="T326">sĭre</ta>
            <ta e="T328" id="Seg_6756" s="T327">pa</ta>
            <ta e="T329" id="Seg_6757" s="T328">kojü</ta>
            <ta e="T330" id="Seg_6758" s="T329">pa</ta>
            <ta e="T332" id="Seg_6759" s="T331">šomi</ta>
            <ta e="T333" id="Seg_6760" s="T332">pa</ta>
            <ta e="T334" id="Seg_6761" s="T333">san</ta>
            <ta e="T335" id="Seg_6762" s="T334">pa</ta>
            <ta e="T340" id="Seg_6763" s="T339">pa</ta>
            <ta e="T342" id="Seg_6764" s="T341">mu-ʔjə</ta>
            <ta e="T343" id="Seg_6765" s="T342">ugandə</ta>
            <ta e="T344" id="Seg_6766" s="T343">iʔgö</ta>
            <ta e="T345" id="Seg_6767" s="T344">dʼije-gən</ta>
            <ta e="T346" id="Seg_6768" s="T345">ka-la-l</ta>
            <ta e="T347" id="Seg_6769" s="T346">dăk</ta>
            <ta e="T348" id="Seg_6770" s="T347">bar</ta>
            <ta e="T349" id="Seg_6771" s="T348">üge</ta>
            <ta e="T350" id="Seg_6772" s="T349">üjü-zi</ta>
            <ta e="T351" id="Seg_6773" s="T350">tonuʔ-la-l</ta>
            <ta e="T353" id="Seg_6774" s="T352">dărowă</ta>
            <ta e="T354" id="Seg_6775" s="T353">i-ge-l</ta>
            <ta e="T355" id="Seg_6776" s="T354">jakše</ta>
            <ta e="T356" id="Seg_6777" s="T355">erte</ta>
            <ta e="T357" id="Seg_6778" s="T356">jakše</ta>
            <ta e="T358" id="Seg_6779" s="T357">erte-zi</ta>
            <ta e="T359" id="Seg_6780" s="T358">i</ta>
            <ta e="T361" id="Seg_6781" s="T360">dʼala</ta>
            <ta e="T362" id="Seg_6782" s="T361">tănan</ta>
            <ta e="T364" id="Seg_6783" s="T363">kăde</ta>
            <ta e="T365" id="Seg_6784" s="T364">kunol-bia-l</ta>
            <ta e="T366" id="Seg_6785" s="T365">teinen</ta>
            <ta e="T367" id="Seg_6786" s="T366">moltʼa-gən</ta>
            <ta e="T368" id="Seg_6787" s="T367">băzə-bia-l</ta>
            <ta e="T369" id="Seg_6788" s="T368">padʼi</ta>
            <ta e="T370" id="Seg_6789" s="T369">tăŋ</ta>
            <ta e="T371" id="Seg_6790" s="T370">kunol-bia-l</ta>
            <ta e="T372" id="Seg_6791" s="T371">da</ta>
            <ta e="T375" id="Seg_6792" s="T374">jakše</ta>
            <ta e="T376" id="Seg_6793" s="T375">băzə-bia-m</ta>
            <ta e="T377" id="Seg_6794" s="T376">bü</ta>
            <ta e="T378" id="Seg_6795" s="T377">kabar-bi</ta>
            <ta e="T379" id="Seg_6796" s="T378">dʼibige</ta>
            <ta e="T380" id="Seg_6797" s="T379">bü-bə</ta>
            <ta e="T381" id="Seg_6798" s="T380">i-bi</ta>
            <ta e="T382" id="Seg_6799" s="T381">i</ta>
            <ta e="T383" id="Seg_6800" s="T382">šišəge</ta>
            <ta e="T384" id="Seg_6801" s="T383">ejü</ta>
            <ta e="T385" id="Seg_6802" s="T384">mo-bi</ta>
            <ta e="T386" id="Seg_6803" s="T385">ugandə</ta>
            <ta e="T387" id="Seg_6804" s="T386">tăŋ</ta>
            <ta e="T388" id="Seg_6805" s="T387">ejü</ta>
            <ta e="T390" id="Seg_6806" s="T389">măn</ta>
            <ta e="T391" id="Seg_6807" s="T390">moltʼa-gən</ta>
            <ta e="T392" id="Seg_6808" s="T391">băzə-bia-m</ta>
            <ta e="T393" id="Seg_6809" s="T392">bar</ta>
            <ta e="T394" id="Seg_6810" s="T393">bü</ta>
            <ta e="T395" id="Seg_6811" s="T394">kămnə-bia-m</ta>
            <ta e="T396" id="Seg_6812" s="T395">dĭgəttə</ta>
            <ta e="T398" id="Seg_6813" s="T396">šo-bi</ta>
            <ta e="T405" id="Seg_6814" s="T404">dĭgəttə</ta>
            <ta e="T406" id="Seg_6815" s="T405">iet</ta>
            <ta e="T407" id="Seg_6816" s="T406">šo-bi</ta>
            <ta e="T408" id="Seg_6817" s="T407">bü</ta>
            <ta e="T409" id="Seg_6818" s="T408">naga</ta>
            <ta e="T410" id="Seg_6819" s="T409">a</ta>
            <ta e="T411" id="Seg_6820" s="T410">măn</ta>
            <ta e="T412" id="Seg_6821" s="T411">det-le-m</ta>
            <ta e="T413" id="Seg_6822" s="T412">da</ta>
            <ta e="T414" id="Seg_6823" s="T413">băzə-jd-la-m</ta>
            <ta e="T415" id="Seg_6824" s="T414">a</ta>
            <ta e="T416" id="Seg_6825" s="T415">măn</ta>
            <ta e="T986" id="Seg_6826" s="T416">kal-la</ta>
            <ta e="T417" id="Seg_6827" s="T986">dʼür-bi-m</ta>
            <ta e="T418" id="Seg_6828" s="T417">dĭ</ta>
            <ta e="T419" id="Seg_6829" s="T418">bar</ta>
            <ta e="T420" id="Seg_6830" s="T419">băzə-jdə-bi</ta>
            <ta e="T421" id="Seg_6831" s="T420">i</ta>
            <ta e="T422" id="Seg_6832" s="T421">maʔ-ndə</ta>
            <ta e="T423" id="Seg_6833" s="T422">šo-bi</ta>
            <ta e="T425" id="Seg_6834" s="T424">măn</ta>
            <ta e="T427" id="Seg_6835" s="T426">sʼestra-m</ta>
            <ta e="T428" id="Seg_6836" s="T427">nʼi-t</ta>
            <ta e="T429" id="Seg_6837" s="T428">mĭm-bi</ta>
            <ta e="T430" id="Seg_6838" s="T429">gorăt-tə</ta>
            <ta e="T431" id="Seg_6839" s="T430">dĭn</ta>
            <ta e="T432" id="Seg_6840" s="T431">mašina</ta>
            <ta e="T433" id="Seg_6841" s="T432">i-bi</ta>
            <ta e="T434" id="Seg_6842" s="T433">kujneg-əʔi</ta>
            <ta e="T435" id="Seg_6843" s="T434">băzə-sʼtə</ta>
            <ta e="T436" id="Seg_6844" s="T435">dĭgəttə</ta>
            <ta e="T437" id="Seg_6845" s="T436">deʔ-pi</ta>
            <ta e="T438" id="Seg_6846" s="T437">maʔ-ndə</ta>
            <ta e="T439" id="Seg_6847" s="T438">a</ta>
            <ta e="T440" id="Seg_6848" s="T439">teinen</ta>
            <ta e="T441" id="Seg_6849" s="T440">kujneg-əʔi</ta>
            <ta e="T442" id="Seg_6850" s="T441">bar</ta>
            <ta e="T443" id="Seg_6851" s="T442">băzə-bi-ʔi</ta>
            <ta e="T444" id="Seg_6852" s="T443">i</ta>
            <ta e="T445" id="Seg_6853" s="T444">edə-bi-ʔi</ta>
            <ta e="T447" id="Seg_6854" s="T446">măn</ta>
            <ta e="T448" id="Seg_6855" s="T447">sʼestra-nə</ta>
            <ta e="T449" id="Seg_6856" s="T448">kazak</ta>
            <ta e="T450" id="Seg_6857" s="T449">ia-t</ta>
            <ta e="T451" id="Seg_6858" s="T450">šo-bi</ta>
            <ta e="T452" id="Seg_6859" s="T451">ajaʔ</ta>
            <ta e="T453" id="Seg_6860" s="T452">deʔ-pi</ta>
            <ta e="T454" id="Seg_6861" s="T453">tibi</ta>
            <ta e="T455" id="Seg_6862" s="T454">i</ta>
            <ta e="T456" id="Seg_6863" s="T455">ne</ta>
            <ta e="T457" id="Seg_6864" s="T456">dĭgəttə</ta>
            <ta e="T458" id="Seg_6865" s="T457">ia</ta>
            <ta e="T459" id="Seg_6866" s="T458">măn</ta>
            <ta e="T460" id="Seg_6867" s="T459">ia-nə</ta>
            <ta e="T461" id="Seg_6868" s="T460">măn-də</ta>
            <ta e="T462" id="Seg_6869" s="T461">kan-gaʔ</ta>
            <ta e="T463" id="Seg_6870" s="T462">keʔbde</ta>
            <ta e="T464" id="Seg_6871" s="T463">i-leʔ</ta>
            <ta e="T466" id="Seg_6872" s="T465">i-leʔ</ta>
            <ta e="T467" id="Seg_6873" s="T466">dĭ-zeŋ</ta>
            <ta e="T468" id="Seg_6874" s="T467">kam-bi-ʔi</ta>
            <ta e="T469" id="Seg_6875" s="T468">dĭ</ta>
            <ta e="T470" id="Seg_6876" s="T469">ara</ta>
            <ta e="T471" id="Seg_6877" s="T470">mĭ-bi</ta>
            <ta e="T472" id="Seg_6878" s="T471">onʼiʔ</ta>
            <ta e="T473" id="Seg_6879" s="T472">onʼiʔ</ta>
            <ta e="T475" id="Seg_6880" s="T474">dĭ-zeŋ</ta>
            <ta e="T476" id="Seg_6881" s="T475">kuʔ-pi</ta>
            <ta e="T478" id="Seg_6882" s="T477">dĭn</ta>
            <ta e="T479" id="Seg_6883" s="T478">keʔbde</ta>
            <ta e="T480" id="Seg_6884" s="T479">dĭ-zeŋ</ta>
            <ta e="T482" id="Seg_6885" s="T481">i-bi-ʔi</ta>
            <ta e="T483" id="Seg_6886" s="T482">i-bi-ʔi</ta>
            <ta e="T484" id="Seg_6887" s="T483">kon</ta>
            <ta e="T485" id="Seg_6888" s="T484">i-bi-ʔi</ta>
            <ta e="T486" id="Seg_6889" s="T485">dĭgəttə</ta>
            <ta e="T487" id="Seg_6890" s="T486">amno-bi-ʔi</ta>
            <ta e="T488" id="Seg_6891" s="T487">amor-zittə</ta>
            <ta e="T489" id="Seg_6892" s="T488">a</ta>
            <ta e="T490" id="Seg_6893" s="T489">măn</ta>
            <ta e="T491" id="Seg_6894" s="T490">ia-m</ta>
            <ta e="T493" id="Seg_6895" s="T492">măn-də</ta>
            <ta e="T494" id="Seg_6896" s="T493">davaj</ta>
            <ta e="T496" id="Seg_6897" s="T494">döʔə</ta>
            <ta e="T497" id="Seg_6898" s="T496">nada</ta>
            <ta e="T498" id="Seg_6899" s="T497">keʔbde</ta>
            <ta e="T499" id="Seg_6900" s="T498">koʔ-sittə</ta>
            <ta e="T500" id="Seg_6901" s="T499">dĭ</ta>
            <ta e="T501" id="Seg_6902" s="T500">ara-zi</ta>
            <ta e="T502" id="Seg_6903" s="T501">dĭgəttə</ta>
            <ta e="T503" id="Seg_6904" s="T502">kămnə-bi-ʔi</ta>
            <ta e="T504" id="Seg_6905" s="T503">bos-tə</ta>
            <ta e="T505" id="Seg_6906" s="T504">bĭʔ-pi-ʔi</ta>
            <ta e="T506" id="Seg_6907" s="T505">dĭgəttə</ta>
            <ta e="T507" id="Seg_6908" s="T506">šo</ta>
            <ta e="T508" id="Seg_6909" s="T507">oʔbdə-bi-ʔi</ta>
            <ta e="T509" id="Seg_6910" s="T508">iʔgö</ta>
            <ta e="T510" id="Seg_6911" s="T509">deʔ-pi-ʔi</ta>
            <ta e="T511" id="Seg_6912" s="T510">šo-bi-ʔi</ta>
            <ta e="T512" id="Seg_6913" s="T511">maʔ-ndə</ta>
            <ta e="T513" id="Seg_6914" s="T512">dĭbər</ta>
            <ta e="T514" id="Seg_6915" s="T513">dö-m</ta>
            <ta e="T515" id="Seg_6916" s="T514">nʼilgö-lia</ta>
            <ta e="T516" id="Seg_6917" s="T515">püje-t-si</ta>
            <ta e="T517" id="Seg_6918" s="T516">dö-m</ta>
            <ta e="T518" id="Seg_6919" s="T517">nʼilgö-lia</ta>
            <ta e="T519" id="Seg_6920" s="T518">püje-t-si</ta>
            <ta e="T520" id="Seg_6921" s="T519">putʼəm-nia</ta>
            <ta e="T521" id="Seg_6922" s="T520">ara-zi</ta>
            <ta e="T522" id="Seg_6923" s="T521">ej</ta>
            <ta e="T523" id="Seg_6924" s="T522">tĭmne-bi</ta>
            <ta e="T525" id="Seg_6925" s="T524">măn</ta>
            <ta e="T527" id="Seg_6926" s="T526">teinen</ta>
            <ta e="T528" id="Seg_6927" s="T527">ertə</ta>
            <ta e="T529" id="Seg_6928" s="T528">uʔbdə-bia-m</ta>
            <ta e="T530" id="Seg_6929" s="T529">pʼeš</ta>
            <ta e="T531" id="Seg_6930" s="T530">nendə-bie-m</ta>
            <ta e="T532" id="Seg_6931" s="T531">dĭgəttə</ta>
            <ta e="T533" id="Seg_6932" s="T532">tüžöj-də</ta>
            <ta e="T534" id="Seg_6933" s="T533">noʔ</ta>
            <ta e="T535" id="Seg_6934" s="T534">mĭ-bia-m</ta>
            <ta e="T536" id="Seg_6935" s="T535">toltanoʔ</ta>
            <ta e="T537" id="Seg_6936" s="T536">bĭʔ-pia-m</ta>
            <ta e="T538" id="Seg_6937" s="T537">kam-bia-m</ta>
            <ta e="T539" id="Seg_6938" s="T538">tüžöj-də</ta>
            <ta e="T540" id="Seg_6939" s="T539">surdə-bia-m</ta>
            <ta e="T541" id="Seg_6940" s="T540">süt-tə</ta>
            <ta e="T542" id="Seg_6941" s="T541">kămnə-bia-m</ta>
            <ta e="T543" id="Seg_6942" s="T542">a</ta>
            <ta e="T544" id="Seg_6943" s="T543">dĭgəttə</ta>
            <ta e="T545" id="Seg_6944" s="T544">kam-bia-m</ta>
            <ta e="T546" id="Seg_6945" s="T545">nüke-nə</ta>
            <ta e="T547" id="Seg_6946" s="T546">mĭj</ta>
            <ta e="T550" id="Seg_6947" s="T549">mĭj</ta>
            <ta e="T551" id="Seg_6948" s="T550">mĭnzer-bie-m</ta>
            <ta e="T552" id="Seg_6949" s="T551">dĭgəttə</ta>
            <ta e="T553" id="Seg_6950" s="T552">kam-bia-m</ta>
            <ta e="T554" id="Seg_6951" s="T553">nüke-nə</ta>
            <ta e="T555" id="Seg_6952" s="T554">mĭj</ta>
            <ta e="T556" id="Seg_6953" s="T555">mĭj</ta>
            <ta e="T557" id="Seg_6954" s="T556">deʔ-pie-m</ta>
            <ta e="T558" id="Seg_6955" s="T557">dĭ</ta>
            <ta e="T559" id="Seg_6956" s="T558">bar</ta>
            <ta e="T561" id="Seg_6957" s="T560">mĭl-leʔbə</ta>
            <ta e="T562" id="Seg_6958" s="T561">üjü-gən</ta>
            <ta e="T563" id="Seg_6959" s="T562">ĭmbi</ta>
            <ta e="T564" id="Seg_6960" s="T563">tura-l</ta>
            <ta e="T565" id="Seg_6961" s="T564">băzə-bi</ta>
            <ta e="T566" id="Seg_6962" s="T565">dʼok</ta>
            <ta e="T567" id="Seg_6963" s="T566">es-seŋ</ta>
            <ta e="T568" id="Seg_6964" s="T567">băzə-bi-ʔi</ta>
            <ta e="T569" id="Seg_6965" s="T568">dĭgəttə</ta>
            <ta e="T570" id="Seg_6966" s="T569">šo-bi</ta>
            <ta e="T571" id="Seg_6967" s="T570">döbər</ta>
            <ta e="T572" id="Seg_6968" s="T571">krăvatʼ-tʼə</ta>
            <ta e="T573" id="Seg_6969" s="T572">bar</ta>
            <ta e="T574" id="Seg_6970" s="T573">saʔmə-luʔ-pi</ta>
            <ta e="T575" id="Seg_6971" s="T574">măn</ta>
            <ta e="T576" id="Seg_6972" s="T575">măn-də-m</ta>
            <ta e="T577" id="Seg_6973" s="T576">i-ʔ</ta>
            <ta e="T578" id="Seg_6974" s="T577">saʔma-ʔ</ta>
            <ta e="T581" id="Seg_6975" s="T580">a</ta>
            <ta e="T582" id="Seg_6976" s="T581">mĭj-də</ta>
            <ta e="T583" id="Seg_6977" s="T582">bar</ta>
            <ta e="T584" id="Seg_6978" s="T583">săgə-luʔ-pi</ta>
            <ta e="T585" id="Seg_6979" s="T584">büžü</ta>
            <ta e="T586" id="Seg_6980" s="T585">ej</ta>
            <ta e="T587" id="Seg_6981" s="T586">ku-lə-j</ta>
            <ta e="T589" id="Seg_6982" s="T588">dĭgəttə</ta>
            <ta e="T590" id="Seg_6983" s="T589">dĭ</ta>
            <ta e="T591" id="Seg_6984" s="T590">nüke-bə</ta>
            <ta e="T593" id="Seg_6985" s="T592">kam-bia-m</ta>
            <ta e="T594" id="Seg_6986" s="T593">Anissʼa-nə</ta>
            <ta e="T595" id="Seg_6987" s="T594">kundʼo</ta>
            <ta e="T596" id="Seg_6988" s="T595">ej</ta>
            <ta e="T597" id="Seg_6989" s="T596">mĭm-bie-m</ta>
            <ta e="T598" id="Seg_6990" s="T597">šo-bia-m</ta>
            <ta e="T599" id="Seg_6991" s="T598">dibər</ta>
            <ta e="T600" id="Seg_6992" s="T599">dĭʔ-nə</ta>
            <ta e="T601" id="Seg_6993" s="T600">dĭ-zeŋ</ta>
            <ta e="T602" id="Seg_6994" s="T601">bar</ta>
            <ta e="T603" id="Seg_6995" s="T602">dibər</ta>
            <ta e="T604" id="Seg_6996" s="T603">nʼi-t-si</ta>
            <ta e="T605" id="Seg_6997" s="T604">kudo-nz-laʔbə</ta>
            <ta e="T607" id="Seg_6998" s="T606">dĭgəttə</ta>
            <ta e="T608" id="Seg_6999" s="T607">dĭ</ta>
            <ta e="T609" id="Seg_7000" s="T608">Anissʼa</ta>
            <ta e="T610" id="Seg_7001" s="T609">bar</ta>
            <ta e="T611" id="Seg_7002" s="T610">kudo-nzə-bi</ta>
            <ta e="T613" id="Seg_7003" s="T612">nʼi-t</ta>
            <ta e="T614" id="Seg_7004" s="T613">a</ta>
            <ta e="T615" id="Seg_7005" s="T614">nʼi-t</ta>
            <ta e="T616" id="Seg_7006" s="T615">bar</ta>
            <ta e="T617" id="Seg_7007" s="T616">dĭʔ-nə</ta>
            <ta e="T618" id="Seg_7008" s="T617">ĭmbi=nʼibudʼ</ta>
            <ta e="T619" id="Seg_7009" s="T618">dʼăbaktər-lə-j</ta>
            <ta e="T620" id="Seg_7010" s="T619">măn</ta>
            <ta e="T621" id="Seg_7011" s="T620">măn-də-m</ta>
            <ta e="T622" id="Seg_7012" s="T621">i-ʔ</ta>
            <ta e="T623" id="Seg_7013" s="T622">kürüm-a-ʔ</ta>
            <ta e="T625" id="Seg_7014" s="T624">ia-n</ta>
            <ta e="T626" id="Seg_7015" s="T625">ĭmbi=də</ta>
            <ta e="T627" id="Seg_7016" s="T626">ej</ta>
            <ta e="T628" id="Seg_7017" s="T627">măn-a-ʔ</ta>
            <ta e="T629" id="Seg_7018" s="T628">a</ta>
            <ta e="T630" id="Seg_7019" s="T629">dĭ</ta>
            <ta e="T631" id="Seg_7020" s="T630">üge</ta>
            <ta e="T632" id="Seg_7021" s="T631">kürüm-nie</ta>
            <ta e="T633" id="Seg_7022" s="T632">a</ta>
            <ta e="T634" id="Seg_7023" s="T633">ia-t</ta>
            <ta e="T635" id="Seg_7024" s="T634">bar</ta>
            <ta e="T636" id="Seg_7025" s="T635">davaj</ta>
            <ta e="T637" id="Seg_7026" s="T636">dʼor-zittə</ta>
            <ta e="T638" id="Seg_7027" s="T637">ku-io-l</ta>
            <ta e="T639" id="Seg_7028" s="T638">kăde</ta>
            <ta e="T640" id="Seg_7029" s="T639">dĭ-zeŋ</ta>
            <ta e="T641" id="Seg_7030" s="T640">ipek-si</ta>
            <ta e="T642" id="Seg_7031" s="T641">măna</ta>
            <ta e="T643" id="Seg_7032" s="T642">băd-lə-ʔi</ta>
            <ta e="T644" id="Seg_7033" s="T643">măn</ta>
            <ta e="T645" id="Seg_7034" s="T644">ipek</ta>
            <ta e="T646" id="Seg_7035" s="T645">am-nia</ta>
            <ta e="T647" id="Seg_7036" s="T646">i</ta>
            <ta e="T648" id="Seg_7037" s="T647">išo</ta>
            <ta e="T649" id="Seg_7038" s="T648">kudo-nz-laʔbə</ta>
            <ta e="T650" id="Seg_7039" s="T649">dĭgəttə</ta>
            <ta e="T652" id="Seg_7040" s="T651">šala</ta>
            <ta e="T653" id="Seg_7041" s="T652">amno-bi-baʔ</ta>
            <ta e="T654" id="Seg_7042" s="T653">dĭgəttə</ta>
            <ta e="T655" id="Seg_7043" s="T654">Jelʼa</ta>
            <ta e="T656" id="Seg_7044" s="T655">šo-bi</ta>
            <ta e="T657" id="Seg_7045" s="T656">zdărowă</ta>
            <ta e="T658" id="Seg_7046" s="T657">i-ge-l</ta>
            <ta e="T659" id="Seg_7047" s="T658">zdărowă</ta>
            <ta e="T660" id="Seg_7048" s="T659">i-ge-l</ta>
            <ta e="T661" id="Seg_7049" s="T660">ĭmbi</ta>
            <ta e="T662" id="Seg_7050" s="T661">šo-bia-l</ta>
            <ta e="T663" id="Seg_7051" s="T662">ipek</ta>
            <ta e="T664" id="Seg_7052" s="T663">măndə-r-bia-m</ta>
            <ta e="T665" id="Seg_7053" s="T664">a</ta>
            <ta e="T666" id="Seg_7054" s="T665">măn</ta>
            <ta e="T667" id="Seg_7055" s="T666">Anissʼa-nə</ta>
            <ta e="T668" id="Seg_7056" s="T667">măl-lia-m</ta>
            <ta e="T669" id="Seg_7057" s="T668">mĭ-ʔ</ta>
            <ta e="T670" id="Seg_7058" s="T669">dĭʔ-nə</ta>
            <ta e="T671" id="Seg_7059" s="T670">onʼiʔ</ta>
            <ta e="T672" id="Seg_7060" s="T671">ipek</ta>
            <ta e="T673" id="Seg_7061" s="T672">dĭ</ta>
            <ta e="T674" id="Seg_7062" s="T673">mĭ-bi</ta>
            <ta e="T675" id="Seg_7063" s="T674">dĭʔ-nə</ta>
            <ta e="T676" id="Seg_7064" s="T675">kumen</ta>
            <ta e="T677" id="Seg_7065" s="T676">pi-zittə</ta>
            <ta e="T678" id="Seg_7066" s="T677">a</ta>
            <ta e="T679" id="Seg_7067" s="T678">măn</ta>
            <ta e="T680" id="Seg_7068" s="T679">măn-də-m</ta>
            <ta e="T681" id="Seg_7069" s="T680">šalaʔ</ta>
            <ta e="T682" id="Seg_7070" s="T681">ej</ta>
            <ta e="T683" id="Seg_7071" s="T682">iʔgö</ta>
            <ta e="T684" id="Seg_7072" s="T683">kak</ta>
            <ta e="T685" id="Seg_7073" s="T684">măgăzin-gəʔ</ta>
            <ta e="T686" id="Seg_7074" s="T685">dĭgəttə</ta>
            <ta e="T687" id="Seg_7075" s="T686">dĭn</ta>
            <ta e="T688" id="Seg_7076" s="T687">dĭ</ta>
            <ta e="T689" id="Seg_7077" s="T688">dĭʔ-nə</ta>
            <ta e="T690" id="Seg_7078" s="T689">mĭ-bi</ta>
            <ta e="T691" id="Seg_7079" s="T690">teʔtə</ta>
            <ta e="T692" id="Seg_7080" s="T691">bieʔ</ta>
            <ta e="T693" id="Seg_7081" s="T692">kăpʼejka-ʔi</ta>
            <ta e="T695" id="Seg_7082" s="T694">dĭgəttə</ta>
            <ta e="T696" id="Seg_7083" s="T695">măn</ta>
            <ta e="T697" id="Seg_7084" s="T696">šo-bia-m</ta>
            <ta e="T698" id="Seg_7085" s="T697">tüžöj-bə</ta>
            <ta e="T699" id="Seg_7086" s="T698">bĭt-əj-bie-m</ta>
            <ta e="T700" id="Seg_7087" s="T699">buzo</ta>
            <ta e="T701" id="Seg_7088" s="T700">bĭt-əl-bie-m</ta>
            <ta e="T702" id="Seg_7089" s="T701">noʔ</ta>
            <ta e="T703" id="Seg_7090" s="T702">mĭ-bie-m</ta>
            <ta e="T704" id="Seg_7091" s="T703">dĭgəttə</ta>
            <ta e="T705" id="Seg_7092" s="T704">bü-j-le</ta>
            <ta e="T706" id="Seg_7093" s="T705">mĭm-bie-m</ta>
            <ta e="T707" id="Seg_7094" s="T706">i</ta>
            <ta e="T709" id="Seg_7095" s="T708">šo-bia-m</ta>
            <ta e="T710" id="Seg_7096" s="T709">tura-nə</ta>
            <ta e="T711" id="Seg_7097" s="T710">uju-m</ta>
            <ta e="T712" id="Seg_7098" s="T711">kăn-nam-bi</ta>
            <ta e="T713" id="Seg_7099" s="T712">pʼeš-tə</ta>
            <ta e="T716" id="Seg_7100" s="T715">sʼa-bia-m</ta>
            <ta e="T717" id="Seg_7101" s="T716">pʼeš-tə</ta>
            <ta e="T719" id="Seg_7102" s="T718">sʼa-bia-m</ta>
            <ta e="T720" id="Seg_7103" s="T719">dĭgəttə</ta>
            <ta e="T721" id="Seg_7104" s="T720">uju-m</ta>
            <ta e="T723" id="Seg_7105" s="T722">ojimulem</ta>
            <ta e="T724" id="Seg_7106" s="T723">dĭ-zeŋ</ta>
            <ta e="T726" id="Seg_7107" s="T725">šo-bi-ʔi</ta>
            <ta e="T727" id="Seg_7108" s="T726">i</ta>
            <ta e="T728" id="Seg_7109" s="T727">dʼăbaktər-zittə</ta>
            <ta e="T730" id="Seg_7110" s="T729">dĭgəttə</ta>
            <ta e="T731" id="Seg_7111" s="T730">dʼăbaktər-zittə</ta>
            <ta e="T732" id="Seg_7112" s="T731">nada</ta>
            <ta e="T733" id="Seg_7113" s="T732">a</ta>
            <ta e="T734" id="Seg_7114" s="T733">dĭn</ta>
            <ta e="T735" id="Seg_7115" s="T734">mašina</ta>
            <ta e="T736" id="Seg_7116" s="T735">bar</ta>
            <ta e="T737" id="Seg_7117" s="T736">kürüm-naʔbə</ta>
            <ta e="T738" id="Seg_7118" s="T737">ej</ta>
            <ta e="T739" id="Seg_7119" s="T738">mĭ-lie</ta>
            <ta e="T740" id="Seg_7120" s="T739">dʼăbaktər-zittə</ta>
            <ta e="T741" id="Seg_7121" s="T740">miʔ</ta>
            <ta e="T742" id="Seg_7122" s="T741">baʔ-luʔ-pi-baʔ</ta>
            <ta e="T743" id="Seg_7123" s="T742">a</ta>
            <ta e="T744" id="Seg_7124" s="T743">dĭ-zeŋ</ta>
            <ta e="T987" id="Seg_7125" s="T744">kal-la</ta>
            <ta e="T745" id="Seg_7126" s="T987">dʼür-bi-ʔi</ta>
            <ta e="T746" id="Seg_7127" s="T745">ej</ta>
            <ta e="T747" id="Seg_7128" s="T746">dʼăbaktər-bia-baʔ</ta>
            <ta e="T748" id="Seg_7129" s="T747">a</ta>
            <ta e="T749" id="Seg_7130" s="T748">tüj</ta>
            <ta e="T750" id="Seg_7131" s="T749">šo-bi-ʔi</ta>
            <ta e="T751" id="Seg_7132" s="T750">bazoʔ</ta>
            <ta e="T753" id="Seg_7133" s="T752">dʼăbaktər-zittə</ta>
            <ta e="T755" id="Seg_7134" s="T754">šo-bi-ʔi</ta>
            <ta e="T756" id="Seg_7135" s="T755">a</ta>
            <ta e="T757" id="Seg_7136" s="T756">măn</ta>
            <ta e="T758" id="Seg_7137" s="T757">măn-də-m</ta>
            <ta e="T759" id="Seg_7138" s="T758">gijen</ta>
            <ta e="T760" id="Seg_7139" s="T759">i-bi-leʔ</ta>
            <ta e="T761" id="Seg_7140" s="T760">da</ta>
            <ta e="T762" id="Seg_7141" s="T761">pa</ta>
            <ta e="T763" id="Seg_7142" s="T762">kür-bi-beʔ</ta>
            <ta e="T764" id="Seg_7143" s="T763">bos-tə</ta>
            <ta e="T765" id="Seg_7144" s="T764">tura</ta>
            <ta e="T766" id="Seg_7145" s="T765">kür-bi-beʔ</ta>
            <ta e="T767" id="Seg_7146" s="T766">i</ta>
            <ta e="T768" id="Seg_7147" s="T767">urgaja-n</ta>
            <ta e="T769" id="Seg_7148" s="T768">tura-nə</ta>
            <ta e="T771" id="Seg_7149" s="T770">kür-bi-beʔ</ta>
            <ta e="T772" id="Seg_7150" s="T771">i</ta>
            <ta e="T773" id="Seg_7151" s="T772">döbər</ta>
            <ta e="T774" id="Seg_7152" s="T773">šo-bi-baʔ</ta>
            <ta e="T776" id="Seg_7153" s="T775">dĭgəttə</ta>
            <ta e="T777" id="Seg_7154" s="T776">ia-t</ta>
            <ta e="T778" id="Seg_7155" s="T777">šo-bi</ta>
            <ta e="T779" id="Seg_7156" s="T778">bălʼnʼitsa-gən</ta>
            <ta e="T780" id="Seg_7157" s="T779">davaj</ta>
            <ta e="T781" id="Seg_7158" s="T780">es-sem</ta>
            <ta e="T783" id="Seg_7159" s="T782">ĭmbi=də</ta>
            <ta e="T784" id="Seg_7160" s="T783">ej</ta>
            <ta e="T785" id="Seg_7161" s="T784">a-lia-ʔi</ta>
            <ta e="T786" id="Seg_7162" s="T785">na</ta>
            <ta e="T787" id="Seg_7163" s="T786">pa-ʔi</ta>
            <ta e="T788" id="Seg_7164" s="T787">en-e-ʔ</ta>
            <ta e="T789" id="Seg_7165" s="T788">ăgrada-gən</ta>
            <ta e="T790" id="Seg_7166" s="T789">ato</ta>
            <ta e="T791" id="Seg_7167" s="T790">aba-l</ta>
            <ta e="T792" id="Seg_7168" s="T791">šo-lə-j</ta>
            <ta e="T793" id="Seg_7169" s="T792">ĭmbi=də</ta>
            <ta e="T794" id="Seg_7170" s="T793">ej</ta>
            <ta e="T795" id="Seg_7171" s="T794">mĭ-lə-j</ta>
            <ta e="T796" id="Seg_7172" s="T795">dĭ</ta>
            <ta e="T797" id="Seg_7173" s="T796">kam-bi</ta>
            <ta e="T798" id="Seg_7174" s="T797">da</ta>
            <ta e="T799" id="Seg_7175" s="T798">davaj</ta>
            <ta e="T800" id="Seg_7176" s="T799">en-zittə</ta>
            <ta e="T801" id="Seg_7177" s="T800">dĭgəttə</ta>
            <ta e="T802" id="Seg_7178" s="T801">šo-bi</ta>
            <ta e="T803" id="Seg_7179" s="T802">multʼa-gən</ta>
            <ta e="T804" id="Seg_7180" s="T803">băzə-jdə-bi-ʔi</ta>
            <ta e="T805" id="Seg_7181" s="T804">dĭgəttə</ta>
            <ta e="T806" id="Seg_7182" s="T805">măna</ta>
            <ta e="T807" id="Seg_7183" s="T806">măn-də-ʔi</ta>
            <ta e="T808" id="Seg_7184" s="T807">kan-a-ʔ</ta>
            <ta e="T809" id="Seg_7185" s="T808">moltʼa-nə</ta>
            <ta e="T810" id="Seg_7186" s="T809">măn</ta>
            <ta e="T811" id="Seg_7187" s="T810">kam-bia-m</ta>
            <ta e="T812" id="Seg_7188" s="T811">băzə-jdə-sʼtə</ta>
            <ta e="T813" id="Seg_7189" s="T812">dĭgəttə</ta>
            <ta e="T814" id="Seg_7190" s="T813">Jelʼa</ta>
            <ta e="T815" id="Seg_7191" s="T814">šo-bi</ta>
            <ta e="T816" id="Seg_7192" s="T815">băzə-jdə-sʼtə</ta>
            <ta e="T817" id="Seg_7193" s="T816">dĭgəttə</ta>
            <ta e="T818" id="Seg_7194" s="T817">tibi</ta>
            <ta e="T819" id="Seg_7195" s="T818">šo-bi</ta>
            <ta e="T820" id="Seg_7196" s="T819">băzə-jdə-bi</ta>
            <ta e="T821" id="Seg_7197" s="T820">dĭgəttə</ta>
            <ta e="T822" id="Seg_7198" s="T821">aba-t</ta>
            <ta e="T823" id="Seg_7199" s="T822">šo-bi</ta>
            <ta e="T824" id="Seg_7200" s="T823">deʔ-pi</ta>
            <ta e="T825" id="Seg_7201" s="T824">čulki-ʔi</ta>
            <ta e="T826" id="Seg_7202" s="T825">piʔme-ʔi</ta>
            <ta e="T827" id="Seg_7203" s="T826">deʔ-pi</ta>
            <ta e="T829" id="Seg_7204" s="T828">dĭgəttə</ta>
            <ta e="T830" id="Seg_7205" s="T829">šo-bi</ta>
            <ta e="T831" id="Seg_7206" s="T830">ara</ta>
            <ta e="T832" id="Seg_7207" s="T831">deʔ-pi</ta>
            <ta e="T833" id="Seg_7208" s="T832">onʼiʔ</ta>
            <ta e="T834" id="Seg_7209" s="T833">tibi</ta>
            <ta e="T835" id="Seg_7210" s="T834">dĭ-m</ta>
            <ta e="T836" id="Seg_7211" s="T835">deʔ-pi</ta>
            <ta e="T837" id="Seg_7212" s="T836">ine-t-si</ta>
            <ta e="T838" id="Seg_7213" s="T837">dĭgəttə</ta>
            <ta e="T839" id="Seg_7214" s="T838">dĭ</ta>
            <ta e="T840" id="Seg_7215" s="T839">dĭʔ-nə</ta>
            <ta e="T841" id="Seg_7216" s="T840">ara</ta>
            <ta e="T842" id="Seg_7217" s="T841">kămnə-bi</ta>
            <ta e="T843" id="Seg_7218" s="T842">šalaʔi</ta>
            <ta e="T844" id="Seg_7219" s="T843">amor-bi</ta>
            <ta e="T846" id="Seg_7220" s="T845">kam-bi</ta>
            <ta e="T847" id="Seg_7221" s="T846">maːʔ-ndə</ta>
            <ta e="T849" id="Seg_7222" s="T848">dĭ</ta>
            <ta e="T850" id="Seg_7223" s="T849">ne</ta>
            <ta e="T851" id="Seg_7224" s="T850">ugandə</ta>
            <ta e="T852" id="Seg_7225" s="T851">kurojoʔ</ta>
            <ta e="T853" id="Seg_7226" s="T852">pa</ta>
            <ta e="T854" id="Seg_7227" s="T853">i-bi</ta>
            <ta e="T855" id="Seg_7228" s="T854">da</ta>
            <ta e="T856" id="Seg_7229" s="T855">büzə-m</ta>
            <ta e="T858" id="Seg_7230" s="T857">pa-zi</ta>
            <ta e="T859" id="Seg_7231" s="T858">toʔ-nar-bi</ta>
            <ta e="T860" id="Seg_7232" s="T859">sʼima-t-tə</ta>
            <ta e="T861" id="Seg_7233" s="T860">kăj-də</ta>
            <ta e="T862" id="Seg_7234" s="T861">bar</ta>
            <ta e="T863" id="Seg_7235" s="T862">mʼaŋ-naʔbə</ta>
            <ta e="T864" id="Seg_7236" s="T863">a</ta>
            <ta e="T865" id="Seg_7237" s="T864">măn</ta>
            <ta e="T866" id="Seg_7238" s="T865">bos-tə</ta>
            <ta e="T867" id="Seg_7239" s="T866">büzə-m</ta>
            <ta e="T868" id="Seg_7240" s="T867">ej</ta>
            <ta e="T869" id="Seg_7241" s="T868">münör-ia-m</ta>
            <ta e="T870" id="Seg_7242" s="T869">uda-zi</ta>
            <ta e="T871" id="Seg_7243" s="T870">uda-zi</ta>
            <ta e="T872" id="Seg_7244" s="T871">toʔ-li-m</ta>
            <ta e="T873" id="Seg_7245" s="T872">šalaʔi</ta>
            <ta e="T874" id="Seg_7246" s="T873">bar</ta>
            <ta e="T875" id="Seg_7247" s="T874">bĭt-luʔ-pi</ta>
            <ta e="T877" id="Seg_7248" s="T876">măn</ta>
            <ta e="T878" id="Seg_7249" s="T877">tura-gən</ta>
            <ta e="T879" id="Seg_7250" s="T878">kür-bie-m</ta>
            <ta e="T880" id="Seg_7251" s="T879">mašina-m</ta>
            <ta e="T882" id="Seg_7252" s="T880">i</ta>
            <ta e="T883" id="Seg_7253" s="T882">dĭ-zeŋ</ta>
            <ta e="T884" id="Seg_7254" s="T883">oʔb</ta>
            <ta e="T885" id="Seg_7255" s="T884">šide</ta>
            <ta e="T886" id="Seg_7256" s="T885">nagur</ta>
            <ta e="T887" id="Seg_7257" s="T886">teʔtə</ta>
            <ta e="T888" id="Seg_7258" s="T887">dĭgəttə</ta>
            <ta e="T889" id="Seg_7259" s="T888">Vanʼa</ta>
            <ta e="T890" id="Seg_7260" s="T889">măn-də</ta>
            <ta e="T891" id="Seg_7261" s="T890">büzö</ta>
            <ta e="T892" id="Seg_7262" s="T891">kereʔ</ta>
            <ta e="T893" id="Seg_7263" s="T892">i</ta>
            <ta e="T894" id="Seg_7264" s="T893">šoška-ʔi</ta>
            <ta e="T895" id="Seg_7265" s="T894">a</ta>
            <ta e="T896" id="Seg_7266" s="T895">miʔ</ta>
            <ta e="T897" id="Seg_7267" s="T896">bar</ta>
            <ta e="T898" id="Seg_7268" s="T897">kaknar-bi-baʔ</ta>
            <ta e="T900" id="Seg_7269" s="T899">teinen</ta>
            <ta e="T901" id="Seg_7270" s="T900">padʼi</ta>
            <ta e="T902" id="Seg_7271" s="T901">iʔgö</ta>
            <ta e="T903" id="Seg_7272" s="T902">dʼăbaktər-bi-baʔ</ta>
            <ta e="T904" id="Seg_7273" s="T903">tüj</ta>
            <ta e="T905" id="Seg_7274" s="T904">nada</ta>
            <ta e="T906" id="Seg_7275" s="T905">maʔ-nʼi</ta>
            <ta e="T907" id="Seg_7276" s="T906">kan-zittə</ta>
            <ta e="T908" id="Seg_7277" s="T907">amor-zittə</ta>
            <ta e="T909" id="Seg_7278" s="T908">iʔbə-sʼtə</ta>
            <ta e="T911" id="Seg_7279" s="T910">šalaʔi</ta>
            <ta e="T913" id="Seg_7280" s="T912">bor</ta>
            <ta e="T914" id="Seg_7281" s="T913">bar</ta>
            <ta e="T915" id="Seg_7282" s="T914">nʼiʔtə</ta>
            <ta e="T916" id="Seg_7283" s="T915">kandə-ga</ta>
            <ta e="T917" id="Seg_7284" s="T916">ugandə</ta>
            <ta e="T918" id="Seg_7285" s="T917">šišəge</ta>
            <ta e="T919" id="Seg_7286" s="T918">mo-lə-j</ta>
            <ta e="T920" id="Seg_7287" s="T919">teinen</ta>
            <ta e="T921" id="Seg_7288" s="T920">nüdʼi-n</ta>
            <ta e="T922" id="Seg_7289" s="T921">sĭre</ta>
            <ta e="T923" id="Seg_7290" s="T922">ertə-n</ta>
            <ta e="T924" id="Seg_7291" s="T923">uʔbdə-bia-m</ta>
            <ta e="T925" id="Seg_7292" s="T924">bar</ta>
            <ta e="T926" id="Seg_7293" s="T925">šišəge</ta>
            <ta e="T927" id="Seg_7294" s="T926">mo-lam-bi</ta>
            <ta e="T929" id="Seg_7295" s="T928">miʔnʼibeʔ</ta>
            <ta e="T930" id="Seg_7296" s="T929">teinen</ta>
            <ta e="T931" id="Seg_7297" s="T930">nada</ta>
            <ta e="T932" id="Seg_7298" s="T931">kan-zittə</ta>
            <ta e="T933" id="Seg_7299" s="T932">Tanʼa-nə</ta>
            <ta e="T934" id="Seg_7300" s="T933">štobɨ</ta>
            <ta e="T935" id="Seg_7301" s="T934">dĭ</ta>
            <ta e="T936" id="Seg_7302" s="T935">ipek</ta>
            <ta e="T937" id="Seg_7303" s="T936">i-bi</ta>
            <ta e="T938" id="Seg_7304" s="T937">Văznesenka-gə</ta>
            <ta e="T939" id="Seg_7305" s="T938">i</ta>
            <ta e="T940" id="Seg_7306" s="T939">deʔ-pi</ta>
            <ta e="T941" id="Seg_7307" s="T940">miʔnʼibeʔ</ta>
            <ta e="T944" id="Seg_7308" s="T943">nada</ta>
            <ta e="T945" id="Seg_7309" s="T944">aktʼa-m</ta>
            <ta e="T946" id="Seg_7310" s="T945">i-zittə</ta>
            <ta e="T947" id="Seg_7311" s="T946">diʔ-nə</ta>
            <ta e="T948" id="Seg_7312" s="T947">ej</ta>
            <ta e="T949" id="Seg_7313" s="T948">bos-kəndə</ta>
            <ta e="T950" id="Seg_7314" s="T949">deʔ-sittə</ta>
            <ta e="T951" id="Seg_7315" s="T950">dĭʔə</ta>
            <ta e="T952" id="Seg_7316" s="T951">ine-t-si</ta>
            <ta e="T953" id="Seg_7317" s="T952">mĭl-lə-j</ta>
            <ta e="T954" id="Seg_7318" s="T953">i</ta>
            <ta e="T955" id="Seg_7319" s="T954">det-lə-j</ta>
            <ta e="T957" id="Seg_7320" s="T956">ej</ta>
            <ta e="T958" id="Seg_7321" s="T957">tĭmne-m</ta>
            <ta e="T959" id="Seg_7322" s="T958">i-lə-j</ta>
            <ta e="T960" id="Seg_7323" s="T959">ilʼi</ta>
            <ta e="T961" id="Seg_7324" s="T960">ej</ta>
            <ta e="T962" id="Seg_7325" s="T961">nada</ta>
            <ta e="T963" id="Seg_7326" s="T962">kan-zittə</ta>
            <ta e="T964" id="Seg_7327" s="T963">dĭ-zi</ta>
            <ta e="T965" id="Seg_7328" s="T964">dʼăbaktər-zittə</ta>
            <ta e="T966" id="Seg_7329" s="T965">možet</ta>
            <ta e="T967" id="Seg_7330" s="T966">i-lə-j</ta>
            <ta e="T968" id="Seg_7331" s="T967">no</ta>
            <ta e="T969" id="Seg_7332" s="T968">kan-a-ʔ</ta>
            <ta e="T970" id="Seg_7333" s="T969">dʼăbaktər-a-ʔ</ta>
            <ta e="T972" id="Seg_7334" s="T971">dĭ</ta>
            <ta e="T973" id="Seg_7335" s="T972">možet</ta>
            <ta e="T974" id="Seg_7336" s="T973">mĭl-lə-j</ta>
            <ta e="T975" id="Seg_7337" s="T974">măgăzin-gən</ta>
            <ta e="T976" id="Seg_7338" s="T975">dĭn</ta>
            <ta e="T978" id="Seg_7339" s="T977">nu-zittə</ta>
            <ta e="T979" id="Seg_7340" s="T978">nada</ta>
            <ta e="T980" id="Seg_7341" s="T979">ipek</ta>
            <ta e="T981" id="Seg_7342" s="T980">i-zittə</ta>
            <ta e="T982" id="Seg_7343" s="T981">măn</ta>
            <ta e="T983" id="Seg_7344" s="T982">ej</ta>
            <ta e="T984" id="Seg_7345" s="T983">i-lə-m</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_7346" s="T0">kan-KAʔ</ta>
            <ta e="T2" id="Seg_7347" s="T1">multʼa-Tə</ta>
            <ta e="T3" id="Seg_7348" s="T2">obbere</ta>
            <ta e="T4" id="Seg_7349" s="T3">šide-göʔ</ta>
            <ta e="T5" id="Seg_7350" s="T4">dĭ</ta>
            <ta e="T6" id="Seg_7351" s="T5">tănan</ta>
            <ta e="T7" id="Seg_7352" s="T6">bögəl-bə</ta>
            <ta e="T8" id="Seg_7353" s="T7">kĭškə-lV-j</ta>
            <ta e="T9" id="Seg_7354" s="T8">sabən-ziʔ</ta>
            <ta e="T10" id="Seg_7355" s="T9">vixotka-ziʔ</ta>
            <ta e="T11" id="Seg_7356" s="T10">a</ta>
            <ta e="T12" id="Seg_7357" s="T11">tăn</ta>
            <ta e="T13" id="Seg_7358" s="T12">dĭ-Tə</ta>
            <ta e="T14" id="Seg_7359" s="T13">da</ta>
            <ta e="T16" id="Seg_7360" s="T15">šer-liA-m</ta>
            <ta e="T17" id="Seg_7361" s="T16">nʼe</ta>
            <ta e="T18" id="Seg_7362" s="T17">axota</ta>
            <ta e="T19" id="Seg_7363" s="T18">kan-zittə</ta>
            <ta e="T20" id="Seg_7364" s="T19">ugaːndə</ta>
            <ta e="T21" id="Seg_7365" s="T20">šer-liA-m</ta>
            <ta e="T22" id="Seg_7366" s="T21">kazak-n</ta>
            <ta e="T23" id="Seg_7367" s="T22">tura-Kən</ta>
            <ta e="T24" id="Seg_7368" s="T23">multʼa-Tə</ta>
            <ta e="T25" id="Seg_7369" s="T24">ej</ta>
            <ta e="T27" id="Seg_7370" s="T26">ej</ta>
            <ta e="T28" id="Seg_7371" s="T27">kan-liA-jəʔ</ta>
            <ta e="T29" id="Seg_7372" s="T28">obbere</ta>
            <ta e="T30" id="Seg_7373" s="T29">ne-zAŋ</ta>
            <ta e="T31" id="Seg_7374" s="T30">tibi-zAŋ</ta>
            <ta e="T32" id="Seg_7375" s="T31">ne-zAŋ</ta>
            <ta e="T33" id="Seg_7376" s="T32">onʼiʔ</ta>
            <ta e="T35" id="Seg_7377" s="T34">kandə-jəʔ</ta>
            <ta e="T36" id="Seg_7378" s="T35">a</ta>
            <ta e="T37" id="Seg_7379" s="T36">tibi-zAŋ</ta>
            <ta e="T38" id="Seg_7380" s="T37">karəldʼaːn</ta>
            <ta e="T39" id="Seg_7381" s="T38">nendə-laʔbə-jəʔ</ta>
            <ta e="T40" id="Seg_7382" s="T39">bazoʔ</ta>
            <ta e="T41" id="Seg_7383" s="T40">onʼiʔ</ta>
            <ta e="T42" id="Seg_7384" s="T41">kan-ntə-gA-jəʔ</ta>
            <ta e="T43" id="Seg_7385" s="T42">tibi-zAŋ</ta>
            <ta e="T45" id="Seg_7386" s="T44">miʔ</ta>
            <ta e="T46" id="Seg_7387" s="T45">multʼa-Kən</ta>
            <ta e="T47" id="Seg_7388" s="T46">koʔbdo-zAŋ</ta>
            <ta e="T48" id="Seg_7389" s="T47">šide-göʔ</ta>
            <ta e="T49" id="Seg_7390" s="T48">kan-ntə-gA-jəʔ</ta>
            <ta e="T50" id="Seg_7391" s="T49">dĭgəttə</ta>
            <ta e="T51" id="Seg_7392" s="T50">ija-t</ta>
            <ta e="T52" id="Seg_7393" s="T51">kan-lV-j</ta>
            <ta e="T53" id="Seg_7394" s="T52">onʼiʔ</ta>
            <ta e="T54" id="Seg_7395" s="T53">koʔbdo</ta>
            <ta e="T55" id="Seg_7396" s="T54">onʼiʔ</ta>
            <ta e="T56" id="Seg_7397" s="T55">nʼi</ta>
            <ta e="T57" id="Seg_7398" s="T56">bazə-lV-j</ta>
            <ta e="T58" id="Seg_7399" s="T57">dĭ-zem</ta>
            <ta e="T59" id="Seg_7400" s="T58">dĭ-zAŋ</ta>
            <ta e="T61" id="Seg_7401" s="T60">šonə-gA-jəʔ</ta>
            <ta e="T62" id="Seg_7402" s="T61">dĭgəttə</ta>
            <ta e="T63" id="Seg_7403" s="T62">tibi</ta>
            <ta e="T64" id="Seg_7404" s="T63">kan-laʔbə</ta>
            <ta e="T65" id="Seg_7405" s="T64">a</ta>
            <ta e="T66" id="Seg_7406" s="T65">dĭgəttə</ta>
            <ta e="T67" id="Seg_7407" s="T66">măn</ta>
            <ta e="T68" id="Seg_7408" s="T67">kan-laʔbə-m</ta>
            <ta e="T69" id="Seg_7409" s="T68">bazə-jd-liA-m</ta>
            <ta e="T71" id="Seg_7410" s="T70">măn</ta>
            <ta e="T72" id="Seg_7411" s="T71">tʼotka-m</ta>
            <ta e="T73" id="Seg_7412" s="T72">kan-bi</ta>
            <ta e="T988" id="Seg_7413" s="T73">Kazan</ta>
            <ta e="T74" id="Seg_7414" s="T988">tura-Tə</ta>
            <ta e="T75" id="Seg_7415" s="T74">dĭn</ta>
            <ta e="T76" id="Seg_7416" s="T75">kak</ta>
            <ta e="T77" id="Seg_7417" s="T76">raz</ta>
            <ta e="T78" id="Seg_7418" s="T77">multʼa</ta>
            <ta e="T79" id="Seg_7419" s="T78">nendə-bi-jəʔ</ta>
            <ta e="T80" id="Seg_7420" s="T79">bar</ta>
            <ta e="T81" id="Seg_7421" s="T80">bazə-bi-jəʔ</ta>
            <ta e="T82" id="Seg_7422" s="T81">măn-ntə-jəʔ</ta>
            <ta e="T83" id="Seg_7423" s="T82">kan-ə-ʔ</ta>
            <ta e="T84" id="Seg_7424" s="T83">multʼa-Tə</ta>
            <ta e="T85" id="Seg_7425" s="T84">a</ta>
            <ta e="T86" id="Seg_7426" s="T85">măn</ta>
            <ta e="T87" id="Seg_7427" s="T86">a</ta>
            <ta e="T88" id="Seg_7428" s="T87">dĭ</ta>
            <ta e="T89" id="Seg_7429" s="T88">kan-bi</ta>
            <ta e="T90" id="Seg_7430" s="T89">ej</ta>
            <ta e="T91" id="Seg_7431" s="T90">tĭmne-t</ta>
            <ta e="T92" id="Seg_7432" s="T91">gibər</ta>
            <ta e="T93" id="Seg_7433" s="T92">bü</ta>
            <ta e="T94" id="Seg_7434" s="T93">kămnə-zittə</ta>
            <ta e="T95" id="Seg_7435" s="T94">pʼeːš-Tə</ta>
            <ta e="T96" id="Seg_7436" s="T95">kămnə-bi-m</ta>
            <ta e="T97" id="Seg_7437" s="T96">bar</ta>
            <ta e="T98" id="Seg_7438" s="T97">stʼena-jəʔ</ta>
            <ta e="T99" id="Seg_7439" s="T98">bü-ziʔ</ta>
            <ta e="T109" id="Seg_7440" s="T108">dĭgəttə</ta>
            <ta e="T110" id="Seg_7441" s="T109">băzəj-bi-m</ta>
            <ta e="T111" id="Seg_7442" s="T110">šo-bi-m</ta>
            <ta e="T112" id="Seg_7443" s="T111">ĭmbi</ta>
            <ta e="T113" id="Seg_7444" s="T112">băzəj-bi-l</ta>
            <ta e="T114" id="Seg_7445" s="T113">băzəj-bi-l</ta>
            <ta e="T115" id="Seg_7446" s="T114">ej</ta>
            <ta e="T116" id="Seg_7447" s="T115">hen-bi-l</ta>
            <ta e="T117" id="Seg_7448" s="T116">ej</ta>
            <ta e="T118" id="Seg_7449" s="T117">hen-bi-m</ta>
            <ta e="T120" id="Seg_7450" s="T119">dĭgəttə</ta>
            <ta e="T122" id="Seg_7451" s="T121">măn-liA-jəʔ</ta>
            <ta e="T123" id="Seg_7452" s="T122">kan-ə-ʔ</ta>
            <ta e="T124" id="Seg_7453" s="T123">pădval-bə</ta>
            <ta e="T125" id="Seg_7454" s="T124">uja</ta>
            <ta e="T126" id="Seg_7455" s="T125">det-ʔ</ta>
            <ta e="T127" id="Seg_7456" s="T126">kătleta-jəʔ-Tə</ta>
            <ta e="T128" id="Seg_7457" s="T127">măn</ta>
            <ta e="T129" id="Seg_7458" s="T128">kan-bi-m</ta>
            <ta e="T131" id="Seg_7459" s="T130">nu-gA-m</ta>
            <ta e="T132" id="Seg_7460" s="T131">uja</ta>
            <ta e="T133" id="Seg_7461" s="T132">iʔgö</ta>
            <ta e="T134" id="Seg_7462" s="T133">a</ta>
            <ta e="T135" id="Seg_7463" s="T134">aspaʔ</ta>
            <ta e="T136" id="Seg_7464" s="T135">naga</ta>
            <ta e="T137" id="Seg_7465" s="T136">dĭgəttə</ta>
            <ta e="T138" id="Seg_7466" s="T137">par-bi-m</ta>
            <ta e="T139" id="Seg_7467" s="T138">ĭmbi</ta>
            <ta e="T140" id="Seg_7468" s="T139">uja</ta>
            <ta e="T141" id="Seg_7469" s="T140">ej</ta>
            <ta e="T143" id="Seg_7470" s="T142">det-liA-l</ta>
            <ta e="T144" id="Seg_7471" s="T143">da</ta>
            <ta e="T145" id="Seg_7472" s="T144">uja</ta>
            <ta e="T146" id="Seg_7473" s="T145">iʔgö</ta>
            <ta e="T147" id="Seg_7474" s="T146">a</ta>
            <ta e="T148" id="Seg_7475" s="T147">aspaʔ</ta>
            <ta e="T149" id="Seg_7476" s="T148">naga</ta>
            <ta e="T150" id="Seg_7477" s="T149">dĭ-zAŋ</ta>
            <ta e="T151" id="Seg_7478" s="T150">davaj</ta>
            <ta e="T152" id="Seg_7479" s="T151">kakənar-zittə</ta>
            <ta e="T154" id="Seg_7480" s="T153">dĭgəttə</ta>
            <ta e="T155" id="Seg_7481" s="T154">măn-liA-jəʔ</ta>
            <ta e="T156" id="Seg_7482" s="T155">kan-ə-ʔ</ta>
            <ta e="T157" id="Seg_7483" s="T156">ăgărot-Tə</ta>
            <ta e="T158" id="Seg_7484" s="T157">det-ʔ</ta>
            <ta e="T159" id="Seg_7485" s="T158">ăgurcɨ-jəʔ</ta>
            <ta e="T160" id="Seg_7486" s="T159">măn</ta>
            <ta e="T161" id="Seg_7487" s="T160">kan-bi-m</ta>
            <ta e="T162" id="Seg_7488" s="T161">măndo-r-bi-m</ta>
            <ta e="T163" id="Seg_7489" s="T162">măndo-r-bi-m</ta>
            <ta e="T164" id="Seg_7490" s="T163">nĭŋgə-liA-m</ta>
            <ta e="T165" id="Seg_7491" s="T164">toŋtanoʔ</ta>
            <ta e="T166" id="Seg_7492" s="T165">ĭššo</ta>
            <ta e="T167" id="Seg_7493" s="T166">ĭmbi=nʼibudʼ</ta>
            <ta e="T168" id="Seg_7494" s="T167">dĭgəttə</ta>
            <ta e="T169" id="Seg_7495" s="T168">napal</ta>
            <ta e="T170" id="Seg_7496" s="T169">šide</ta>
            <ta e="T173" id="Seg_7497" s="T172">i-bi-m</ta>
            <ta e="T174" id="Seg_7498" s="T173">i</ta>
            <ta e="T175" id="Seg_7499" s="T174">šo-bi-m</ta>
            <ta e="T176" id="Seg_7500" s="T175">tura-Tə</ta>
            <ta e="T177" id="Seg_7501" s="T176">onʼiʔ</ta>
            <ta e="T178" id="Seg_7502" s="T177">ne</ta>
            <ta e="T179" id="Seg_7503" s="T178">patpolʼlʼa-Tə</ta>
            <ta e="T181" id="Seg_7504" s="T180">süʔmə-luʔbdə-bi</ta>
            <ta e="T182" id="Seg_7505" s="T181">onʼiʔ</ta>
            <ta e="T184" id="Seg_7506" s="T183">ne</ta>
            <ta e="T185" id="Seg_7507" s="T184">nʼiʔdə</ta>
            <ta e="T186" id="Seg_7508" s="T185">aʔpi</ta>
            <ta e="T188" id="Seg_7509" s="T187">măna</ta>
            <ta e="T189" id="Seg_7510" s="T188">ej</ta>
            <ta e="T190" id="Seg_7511" s="T189">tonuʔ-bi</ta>
            <ta e="T192" id="Seg_7512" s="T191">dĭgəttə</ta>
            <ta e="T194" id="Seg_7513" s="T193">dĭ</ta>
            <ta e="T195" id="Seg_7514" s="T194">ne-zen</ta>
            <ta e="T196" id="Seg_7515" s="T195">aba</ta>
            <ta e="T197" id="Seg_7516" s="T196">aba-t</ta>
            <ta e="T198" id="Seg_7517" s="T197">măn-ntə</ta>
            <ta e="T199" id="Seg_7518" s="T198">gibər</ta>
            <ta e="T200" id="Seg_7519" s="T199">šiʔ</ta>
            <ta e="T201" id="Seg_7520" s="T200">nuʔmə-laːm-bi-lAʔ</ta>
            <ta e="T202" id="Seg_7521" s="T201">det-KAʔ</ta>
            <ta e="T203" id="Seg_7522" s="T202">a</ta>
            <ta e="T204" id="Seg_7523" s="T203">kămnə-KAʔ</ta>
            <ta e="T205" id="Seg_7524" s="T204">mĭje</ta>
            <ta e="T206" id="Seg_7525" s="T205">aba-m</ta>
            <ta e="T207" id="Seg_7526" s="T206">măn-ntə</ta>
            <ta e="T208" id="Seg_7527" s="T207">ku-liA-l</ta>
            <ta e="T209" id="Seg_7528" s="T208">dĭ</ta>
            <ta e="T210" id="Seg_7529" s="T209">šide</ta>
            <ta e="T211" id="Seg_7530" s="T210">dĭʔə</ta>
            <ta e="T214" id="Seg_7531" s="T213">det-bi</ta>
            <ta e="T215" id="Seg_7532" s="T214">a</ta>
            <ta e="T216" id="Seg_7533" s="T215">ĭmbi</ta>
            <ta e="T217" id="Seg_7534" s="T216">öʔ-liA-jəʔ</ta>
            <ta e="T218" id="Seg_7535" s="T217">kuza</ta>
            <ta e="T219" id="Seg_7536" s="T218">ej</ta>
            <ta e="T220" id="Seg_7537" s="T219">tĭmne-t</ta>
            <ta e="T221" id="Seg_7538" s="T220">a</ta>
            <ta e="T222" id="Seg_7539" s="T221">šiʔ</ta>
            <ta e="T226" id="Seg_7540" s="T225">onʼiʔ</ta>
            <ta e="T227" id="Seg_7541" s="T226">ne</ta>
            <ta e="T228" id="Seg_7542" s="T227">Kazan</ta>
            <ta e="T229" id="Seg_7543" s="T228">tura-Tə</ta>
            <ta e="T230" id="Seg_7544" s="T229">mĭn-bi</ta>
            <ta e="T232" id="Seg_7545" s="T231">ine-t-ziʔ</ta>
            <ta e="T234" id="Seg_7546" s="T233">šo-bi</ta>
            <ta e="T235" id="Seg_7547" s="T234">maʔ-gəndə</ta>
            <ta e="T236" id="Seg_7548" s="T235">i</ta>
            <ta e="T237" id="Seg_7549" s="T236">ku-bi</ta>
            <ta e="T238" id="Seg_7550" s="T237">pa</ta>
            <ta e="T239" id="Seg_7551" s="T238">iʔbö-laʔbə</ta>
            <ta e="T241" id="Seg_7552" s="T240">dĭgəttə</ta>
            <ta e="T242" id="Seg_7553" s="T241">măn-ntə</ta>
            <ta e="T243" id="Seg_7554" s="T242">dĭ-n</ta>
            <ta e="T244" id="Seg_7555" s="T243">červə</ta>
            <ta e="T245" id="Seg_7556" s="T244">bar</ta>
            <ta e="T246" id="Seg_7557" s="T245">am-bi</ta>
            <ta e="T249" id="Seg_7558" s="T248">dĭrgit</ta>
            <ta e="T250" id="Seg_7559" s="T249">pa-jəʔ</ta>
            <ta e="T251" id="Seg_7560" s="T250">možna</ta>
            <ta e="T252" id="Seg_7561" s="T251">toʔbdə-nar-zittə</ta>
            <ta e="T253" id="Seg_7562" s="T252">i</ta>
            <ta e="T254" id="Seg_7563" s="T253">pa-jəʔ</ta>
            <ta e="T255" id="Seg_7564" s="T254">boləj</ta>
            <ta e="T257" id="Seg_7565" s="T256">dĭrgit</ta>
            <ta e="T258" id="Seg_7566" s="T257">bar</ta>
            <ta e="T259" id="Seg_7567" s="T258">a-bi</ta>
            <ta e="T260" id="Seg_7568" s="T259">üdʼüge</ta>
            <ta e="T261" id="Seg_7569" s="T260">pʼeːš</ta>
            <ta e="T262" id="Seg_7570" s="T261">možna</ta>
            <ta e="T263" id="Seg_7571" s="T262">nendə-zittə</ta>
            <ta e="T265" id="Seg_7572" s="T264">dĭgəttə</ta>
            <ta e="T266" id="Seg_7573" s="T265">Varlam</ta>
            <ta e="T267" id="Seg_7574" s="T266">det-bi</ta>
            <ta e="T268" id="Seg_7575" s="T267">kujdərgan</ta>
            <ta e="T269" id="Seg_7576" s="T268">nuldə-ʔ</ta>
            <ta e="T271" id="Seg_7577" s="T270">măn-ntə</ta>
            <ta e="T272" id="Seg_7578" s="T271">dĭ-Tə</ta>
            <ta e="T273" id="Seg_7579" s="T272">dĭ</ta>
            <ta e="T274" id="Seg_7580" s="T273">kan-bi</ta>
            <ta e="T275" id="Seg_7581" s="T274">bar</ta>
            <ta e="T276" id="Seg_7582" s="T275">truba-gəndə</ta>
            <ta e="T277" id="Seg_7583" s="T276">bü</ta>
            <ta e="T278" id="Seg_7584" s="T277">hen-bi</ta>
            <ta e="T279" id="Seg_7585" s="T278">hen-bi</ta>
            <ta e="T280" id="Seg_7586" s="T279">kămnə-bi</ta>
            <ta e="T281" id="Seg_7587" s="T280">kămnə-bi</ta>
            <ta e="T282" id="Seg_7588" s="T281">i</ta>
            <ta e="T283" id="Seg_7589" s="T282">dĭbər</ta>
            <ta e="T285" id="Seg_7590" s="T284">kös</ta>
            <ta e="T286" id="Seg_7591" s="T285">hen-bi</ta>
            <ta e="T287" id="Seg_7592" s="T286">šo-bi</ta>
            <ta e="T288" id="Seg_7593" s="T287">tura-Tə</ta>
            <ta e="T289" id="Seg_7594" s="T288">da</ta>
            <ta e="T290" id="Seg_7595" s="T289">măn-ntə</ta>
            <ta e="T291" id="Seg_7596" s="T290">samovar</ta>
            <ta e="T292" id="Seg_7597" s="T291">mʼaŋ-laʔbə</ta>
            <ta e="T293" id="Seg_7598" s="T292">bar</ta>
            <ta e="T294" id="Seg_7599" s="T293">kujdərgan</ta>
            <ta e="T295" id="Seg_7600" s="T294">mʼaŋ-laʔbə</ta>
            <ta e="T296" id="Seg_7601" s="T295">bar</ta>
            <ta e="T297" id="Seg_7602" s="T296">bü</ta>
            <ta e="T298" id="Seg_7603" s="T297">dĭ</ta>
            <ta e="T299" id="Seg_7604" s="T298">kan-bi</ta>
            <ta e="T300" id="Seg_7605" s="T299">bar</ta>
            <ta e="T301" id="Seg_7606" s="T300">döbər</ta>
            <ta e="T302" id="Seg_7607" s="T301">măn-ntə</ta>
            <ta e="T303" id="Seg_7608" s="T302">bü</ta>
            <ta e="T304" id="Seg_7609" s="T303">hen-ə-ʔ</ta>
            <ta e="T305" id="Seg_7610" s="T304">a</ta>
            <ta e="T306" id="Seg_7611" s="T305">döbər</ta>
            <ta e="T307" id="Seg_7612" s="T306">kös</ta>
            <ta e="T308" id="Seg_7613" s="T307">hen-ə-ʔ</ta>
            <ta e="T309" id="Seg_7614" s="T308">dĭgəttə</ta>
            <ta e="T310" id="Seg_7615" s="T309">dĭ</ta>
            <ta e="T311" id="Seg_7616" s="T310">didro</ta>
            <ta e="T312" id="Seg_7617" s="T311">a-bi</ta>
            <ta e="T314" id="Seg_7618" s="T313">pa</ta>
            <ta e="T315" id="Seg_7619" s="T314">kan-bi-jəʔ</ta>
            <ta e="T316" id="Seg_7620" s="T315">hʼaʔ-zittə</ta>
            <ta e="T317" id="Seg_7621" s="T316">i</ta>
            <ta e="T318" id="Seg_7622" s="T317">nʼamga</ta>
            <ta e="T319" id="Seg_7623" s="T318">bü</ta>
            <ta e="T320" id="Seg_7624" s="T319">det-bi-jəʔ</ta>
            <ta e="T322" id="Seg_7625" s="T321">pa-Kən</ta>
            <ta e="T323" id="Seg_7626" s="T322">nʼamga</ta>
            <ta e="T324" id="Seg_7627" s="T323">bü</ta>
            <ta e="T325" id="Seg_7628" s="T324">i-bi</ta>
            <ta e="T327" id="Seg_7629" s="T326">sĭri</ta>
            <ta e="T328" id="Seg_7630" s="T327">pa</ta>
            <ta e="T329" id="Seg_7631" s="T328">kojü</ta>
            <ta e="T330" id="Seg_7632" s="T329">pa</ta>
            <ta e="T332" id="Seg_7633" s="T331">šomi</ta>
            <ta e="T333" id="Seg_7634" s="T332">pa</ta>
            <ta e="T334" id="Seg_7635" s="T333">sanə</ta>
            <ta e="T335" id="Seg_7636" s="T334">pa</ta>
            <ta e="T340" id="Seg_7637" s="T339">pa</ta>
            <ta e="T342" id="Seg_7638" s="T341">mu-jəʔ</ta>
            <ta e="T343" id="Seg_7639" s="T342">ugaːndə</ta>
            <ta e="T344" id="Seg_7640" s="T343">iʔgö</ta>
            <ta e="T345" id="Seg_7641" s="T344">dʼije-Kən</ta>
            <ta e="T346" id="Seg_7642" s="T345">kan-lV-l</ta>
            <ta e="T347" id="Seg_7643" s="T346">tak</ta>
            <ta e="T348" id="Seg_7644" s="T347">bar</ta>
            <ta e="T349" id="Seg_7645" s="T348">üge</ta>
            <ta e="T350" id="Seg_7646" s="T349">üjü-ziʔ</ta>
            <ta e="T351" id="Seg_7647" s="T350">tonuʔ-lV-l</ta>
            <ta e="T353" id="Seg_7648" s="T352">dărowă</ta>
            <ta e="T354" id="Seg_7649" s="T353">i-gA-l</ta>
            <ta e="T355" id="Seg_7650" s="T354">jakšə</ta>
            <ta e="T356" id="Seg_7651" s="T355">ertə</ta>
            <ta e="T357" id="Seg_7652" s="T356">jakšə</ta>
            <ta e="T358" id="Seg_7653" s="T357">ertə-ziʔ</ta>
            <ta e="T359" id="Seg_7654" s="T358">i</ta>
            <ta e="T361" id="Seg_7655" s="T360">tʼala</ta>
            <ta e="T362" id="Seg_7656" s="T361">tănan</ta>
            <ta e="T364" id="Seg_7657" s="T363">kădaʔ</ta>
            <ta e="T365" id="Seg_7658" s="T364">kunol-bi-l</ta>
            <ta e="T366" id="Seg_7659" s="T365">teinen</ta>
            <ta e="T367" id="Seg_7660" s="T366">multʼa-Kən</ta>
            <ta e="T368" id="Seg_7661" s="T367">bazə-bi-l</ta>
            <ta e="T369" id="Seg_7662" s="T368">padʼi</ta>
            <ta e="T370" id="Seg_7663" s="T369">tăŋ</ta>
            <ta e="T371" id="Seg_7664" s="T370">kunol-bi-l</ta>
            <ta e="T372" id="Seg_7665" s="T371">da</ta>
            <ta e="T375" id="Seg_7666" s="T374">jakšə</ta>
            <ta e="T376" id="Seg_7667" s="T375">bazə-bi-m</ta>
            <ta e="T377" id="Seg_7668" s="T376">bü</ta>
            <ta e="T378" id="Seg_7669" s="T377">kabar-bi</ta>
            <ta e="T379" id="Seg_7670" s="T378">dʼibige</ta>
            <ta e="T380" id="Seg_7671" s="T379">bü-m</ta>
            <ta e="T381" id="Seg_7672" s="T380">i-bi</ta>
            <ta e="T382" id="Seg_7673" s="T381">i</ta>
            <ta e="T383" id="Seg_7674" s="T382">šišəge</ta>
            <ta e="T384" id="Seg_7675" s="T383">ejü</ta>
            <ta e="T385" id="Seg_7676" s="T384">mo-bi</ta>
            <ta e="T386" id="Seg_7677" s="T385">ugaːndə</ta>
            <ta e="T387" id="Seg_7678" s="T386">tăŋ</ta>
            <ta e="T388" id="Seg_7679" s="T387">ejü</ta>
            <ta e="T390" id="Seg_7680" s="T389">măn</ta>
            <ta e="T391" id="Seg_7681" s="T390">multʼa-Kən</ta>
            <ta e="T392" id="Seg_7682" s="T391">bazə-bi-m</ta>
            <ta e="T393" id="Seg_7683" s="T392">bar</ta>
            <ta e="T394" id="Seg_7684" s="T393">bü</ta>
            <ta e="T395" id="Seg_7685" s="T394">kămnə-bi-m</ta>
            <ta e="T396" id="Seg_7686" s="T395">dĭgəttə</ta>
            <ta e="T398" id="Seg_7687" s="T396">šo-bi</ta>
            <ta e="T405" id="Seg_7688" s="T404">dĭgəttə</ta>
            <ta e="T406" id="Seg_7689" s="T405">iet</ta>
            <ta e="T407" id="Seg_7690" s="T406">šo-bi</ta>
            <ta e="T408" id="Seg_7691" s="T407">bü</ta>
            <ta e="T409" id="Seg_7692" s="T408">naga</ta>
            <ta e="T410" id="Seg_7693" s="T409">a</ta>
            <ta e="T411" id="Seg_7694" s="T410">măn</ta>
            <ta e="T412" id="Seg_7695" s="T411">det-lV-m</ta>
            <ta e="T413" id="Seg_7696" s="T412">da</ta>
            <ta e="T414" id="Seg_7697" s="T413">bazə-jd-lV-m</ta>
            <ta e="T415" id="Seg_7698" s="T414">a</ta>
            <ta e="T416" id="Seg_7699" s="T415">măn</ta>
            <ta e="T986" id="Seg_7700" s="T416">kan-lAʔ</ta>
            <ta e="T417" id="Seg_7701" s="T986">tʼür-bi-m</ta>
            <ta e="T418" id="Seg_7702" s="T417">dĭ</ta>
            <ta e="T419" id="Seg_7703" s="T418">bar</ta>
            <ta e="T420" id="Seg_7704" s="T419">bazə-jd-bi</ta>
            <ta e="T421" id="Seg_7705" s="T420">i</ta>
            <ta e="T422" id="Seg_7706" s="T421">maʔ-gəndə</ta>
            <ta e="T423" id="Seg_7707" s="T422">šo-bi</ta>
            <ta e="T425" id="Seg_7708" s="T424">măn</ta>
            <ta e="T427" id="Seg_7709" s="T426">sʼestra-m</ta>
            <ta e="T428" id="Seg_7710" s="T427">nʼi-t</ta>
            <ta e="T429" id="Seg_7711" s="T428">mĭn-bi</ta>
            <ta e="T430" id="Seg_7712" s="T429">gorăt-Tə</ta>
            <ta e="T431" id="Seg_7713" s="T430">dĭn</ta>
            <ta e="T432" id="Seg_7714" s="T431">mašina</ta>
            <ta e="T433" id="Seg_7715" s="T432">i-bi</ta>
            <ta e="T434" id="Seg_7716" s="T433">kujnek-jəʔ</ta>
            <ta e="T435" id="Seg_7717" s="T434">bazə-zittə</ta>
            <ta e="T436" id="Seg_7718" s="T435">dĭgəttə</ta>
            <ta e="T437" id="Seg_7719" s="T436">det-bi</ta>
            <ta e="T438" id="Seg_7720" s="T437">maʔ-gəndə</ta>
            <ta e="T439" id="Seg_7721" s="T438">a</ta>
            <ta e="T440" id="Seg_7722" s="T439">teinen</ta>
            <ta e="T441" id="Seg_7723" s="T440">kujnek-jəʔ</ta>
            <ta e="T442" id="Seg_7724" s="T441">bar</ta>
            <ta e="T443" id="Seg_7725" s="T442">bazə-bi-jəʔ</ta>
            <ta e="T444" id="Seg_7726" s="T443">i</ta>
            <ta e="T445" id="Seg_7727" s="T444">edə-bi-jəʔ</ta>
            <ta e="T447" id="Seg_7728" s="T446">măn</ta>
            <ta e="T448" id="Seg_7729" s="T447">sʼestra-Tə</ta>
            <ta e="T449" id="Seg_7730" s="T448">kazak</ta>
            <ta e="T450" id="Seg_7731" s="T449">ija-t</ta>
            <ta e="T451" id="Seg_7732" s="T450">šo-bi</ta>
            <ta e="T452" id="Seg_7733" s="T451">ajaʔ</ta>
            <ta e="T453" id="Seg_7734" s="T452">det-bi</ta>
            <ta e="T454" id="Seg_7735" s="T453">tibi</ta>
            <ta e="T455" id="Seg_7736" s="T454">i</ta>
            <ta e="T456" id="Seg_7737" s="T455">ne</ta>
            <ta e="T457" id="Seg_7738" s="T456">dĭgəttə</ta>
            <ta e="T458" id="Seg_7739" s="T457">ija</ta>
            <ta e="T459" id="Seg_7740" s="T458">măn</ta>
            <ta e="T460" id="Seg_7741" s="T459">ija-Tə</ta>
            <ta e="T461" id="Seg_7742" s="T460">măn-ntə</ta>
            <ta e="T462" id="Seg_7743" s="T461">kan-KAʔ</ta>
            <ta e="T463" id="Seg_7744" s="T462">keʔbde</ta>
            <ta e="T464" id="Seg_7745" s="T463">i-lAʔ</ta>
            <ta e="T466" id="Seg_7746" s="T465">i-lAʔ</ta>
            <ta e="T467" id="Seg_7747" s="T466">dĭ-zAŋ</ta>
            <ta e="T468" id="Seg_7748" s="T467">kan-bi-jəʔ</ta>
            <ta e="T469" id="Seg_7749" s="T468">dĭ</ta>
            <ta e="T470" id="Seg_7750" s="T469">ara</ta>
            <ta e="T471" id="Seg_7751" s="T470">mĭ-bi</ta>
            <ta e="T472" id="Seg_7752" s="T471">onʼiʔ</ta>
            <ta e="T473" id="Seg_7753" s="T472">onʼiʔ</ta>
            <ta e="T475" id="Seg_7754" s="T474">dĭ-zAŋ</ta>
            <ta e="T476" id="Seg_7755" s="T475">kuʔ-bi</ta>
            <ta e="T478" id="Seg_7756" s="T477">dĭn</ta>
            <ta e="T479" id="Seg_7757" s="T478">keʔbde</ta>
            <ta e="T480" id="Seg_7758" s="T479">dĭ-zAŋ</ta>
            <ta e="T482" id="Seg_7759" s="T481">i-bi-jəʔ</ta>
            <ta e="T483" id="Seg_7760" s="T482">i-bi-jəʔ</ta>
            <ta e="T484" id="Seg_7761" s="T483">kon</ta>
            <ta e="T485" id="Seg_7762" s="T484">i-bi-jəʔ</ta>
            <ta e="T486" id="Seg_7763" s="T485">dĭgəttə</ta>
            <ta e="T487" id="Seg_7764" s="T486">amno-bi-jəʔ</ta>
            <ta e="T488" id="Seg_7765" s="T487">amor-zittə</ta>
            <ta e="T489" id="Seg_7766" s="T488">a</ta>
            <ta e="T490" id="Seg_7767" s="T489">măn</ta>
            <ta e="T491" id="Seg_7768" s="T490">ija-m</ta>
            <ta e="T493" id="Seg_7769" s="T492">măn-ntə</ta>
            <ta e="T494" id="Seg_7770" s="T493">davaj</ta>
            <ta e="T496" id="Seg_7771" s="T494">döʔə</ta>
            <ta e="T497" id="Seg_7772" s="T496">nadə</ta>
            <ta e="T498" id="Seg_7773" s="T497">keʔbde</ta>
            <ta e="T499" id="Seg_7774" s="T498">koʔ-zittə</ta>
            <ta e="T500" id="Seg_7775" s="T499">dĭ</ta>
            <ta e="T501" id="Seg_7776" s="T500">ara-ziʔ</ta>
            <ta e="T502" id="Seg_7777" s="T501">dĭgəttə</ta>
            <ta e="T503" id="Seg_7778" s="T502">kămnə-bi-jəʔ</ta>
            <ta e="T504" id="Seg_7779" s="T503">bos-də</ta>
            <ta e="T505" id="Seg_7780" s="T504">bĭs-bi-jəʔ</ta>
            <ta e="T506" id="Seg_7781" s="T505">dĭgəttə</ta>
            <ta e="T507" id="Seg_7782" s="T506">šo</ta>
            <ta e="T508" id="Seg_7783" s="T507">oʔbdə-bi-jəʔ</ta>
            <ta e="T509" id="Seg_7784" s="T508">iʔgö</ta>
            <ta e="T510" id="Seg_7785" s="T509">det-bi-jəʔ</ta>
            <ta e="T511" id="Seg_7786" s="T510">šo-bi-jəʔ</ta>
            <ta e="T512" id="Seg_7787" s="T511">maʔ-gəndə</ta>
            <ta e="T513" id="Seg_7788" s="T512">dĭbər</ta>
            <ta e="T514" id="Seg_7789" s="T513">dö-m</ta>
            <ta e="T515" id="Seg_7790" s="T514">nʼilgö-liA</ta>
            <ta e="T516" id="Seg_7791" s="T515">püje-t-ziʔ</ta>
            <ta e="T517" id="Seg_7792" s="T516">dö-m</ta>
            <ta e="T518" id="Seg_7793" s="T517">nʼilgö-liA</ta>
            <ta e="T519" id="Seg_7794" s="T518">püje-t-ziʔ</ta>
            <ta e="T520" id="Seg_7795" s="T519">putʼtʼəm-liA</ta>
            <ta e="T521" id="Seg_7796" s="T520">ara-ziʔ</ta>
            <ta e="T522" id="Seg_7797" s="T521">ej</ta>
            <ta e="T523" id="Seg_7798" s="T522">tĭmne-bi</ta>
            <ta e="T525" id="Seg_7799" s="T524">măn</ta>
            <ta e="T527" id="Seg_7800" s="T526">teinen</ta>
            <ta e="T528" id="Seg_7801" s="T527">ertə</ta>
            <ta e="T529" id="Seg_7802" s="T528">uʔbdə-bi-m</ta>
            <ta e="T530" id="Seg_7803" s="T529">pʼeːš</ta>
            <ta e="T531" id="Seg_7804" s="T530">nendə-bi-m</ta>
            <ta e="T532" id="Seg_7805" s="T531">dĭgəttə</ta>
            <ta e="T533" id="Seg_7806" s="T532">tüžöj-Tə</ta>
            <ta e="T534" id="Seg_7807" s="T533">noʔ</ta>
            <ta e="T535" id="Seg_7808" s="T534">mĭ-bi-m</ta>
            <ta e="T536" id="Seg_7809" s="T535">toltano</ta>
            <ta e="T537" id="Seg_7810" s="T536">bĭs-bi-m</ta>
            <ta e="T538" id="Seg_7811" s="T537">kan-bi-m</ta>
            <ta e="T539" id="Seg_7812" s="T538">tüžöj-Tə</ta>
            <ta e="T540" id="Seg_7813" s="T539">surdo-bi-m</ta>
            <ta e="T541" id="Seg_7814" s="T540">süt-də</ta>
            <ta e="T542" id="Seg_7815" s="T541">kămnə-bi-m</ta>
            <ta e="T543" id="Seg_7816" s="T542">a</ta>
            <ta e="T544" id="Seg_7817" s="T543">dĭgəttə</ta>
            <ta e="T545" id="Seg_7818" s="T544">kan-bi-m</ta>
            <ta e="T546" id="Seg_7819" s="T545">nüke-Tə</ta>
            <ta e="T547" id="Seg_7820" s="T546">mĭje</ta>
            <ta e="T550" id="Seg_7821" s="T549">mĭje</ta>
            <ta e="T551" id="Seg_7822" s="T550">mĭnzər-bi-m</ta>
            <ta e="T552" id="Seg_7823" s="T551">dĭgəttə</ta>
            <ta e="T553" id="Seg_7824" s="T552">kan-bi-m</ta>
            <ta e="T554" id="Seg_7825" s="T553">nüke-Tə</ta>
            <ta e="T555" id="Seg_7826" s="T554">mĭje</ta>
            <ta e="T556" id="Seg_7827" s="T555">mĭje</ta>
            <ta e="T557" id="Seg_7828" s="T556">det-bi-m</ta>
            <ta e="T558" id="Seg_7829" s="T557">dĭ</ta>
            <ta e="T559" id="Seg_7830" s="T558">bar</ta>
            <ta e="T561" id="Seg_7831" s="T560">mĭn-laʔbə</ta>
            <ta e="T562" id="Seg_7832" s="T561">üjü-Kən</ta>
            <ta e="T563" id="Seg_7833" s="T562">ĭmbi</ta>
            <ta e="T564" id="Seg_7834" s="T563">tura-l</ta>
            <ta e="T565" id="Seg_7835" s="T564">bazə-bi</ta>
            <ta e="T566" id="Seg_7836" s="T565">dʼok</ta>
            <ta e="T567" id="Seg_7837" s="T566">ešši-zAŋ</ta>
            <ta e="T568" id="Seg_7838" s="T567">bazə-bi-jəʔ</ta>
            <ta e="T569" id="Seg_7839" s="T568">dĭgəttə</ta>
            <ta e="T570" id="Seg_7840" s="T569">šo-bi</ta>
            <ta e="T571" id="Seg_7841" s="T570">döbər</ta>
            <ta e="T572" id="Seg_7842" s="T571">krăvatʼ-Tə</ta>
            <ta e="T573" id="Seg_7843" s="T572">bar</ta>
            <ta e="T574" id="Seg_7844" s="T573">saʔmə-luʔbdə-bi</ta>
            <ta e="T575" id="Seg_7845" s="T574">măn</ta>
            <ta e="T576" id="Seg_7846" s="T575">măn-ntə-m</ta>
            <ta e="T577" id="Seg_7847" s="T576">e-ʔ</ta>
            <ta e="T578" id="Seg_7848" s="T577">saʔmə-ʔ</ta>
            <ta e="T581" id="Seg_7849" s="T580">a</ta>
            <ta e="T582" id="Seg_7850" s="T581">mĭje-də</ta>
            <ta e="T583" id="Seg_7851" s="T582">bar</ta>
            <ta e="T584" id="Seg_7852" s="T583">săgə-luʔbdə-bi</ta>
            <ta e="T585" id="Seg_7853" s="T584">büžü</ta>
            <ta e="T586" id="Seg_7854" s="T585">ej</ta>
            <ta e="T587" id="Seg_7855" s="T586">ku-lV-j</ta>
            <ta e="T589" id="Seg_7856" s="T588">dĭgəttə</ta>
            <ta e="T590" id="Seg_7857" s="T589">dĭ</ta>
            <ta e="T591" id="Seg_7858" s="T590">nüke-bə</ta>
            <ta e="T593" id="Seg_7859" s="T592">kan-bi-m</ta>
            <ta e="T594" id="Seg_7860" s="T593">Anissʼa-Tə</ta>
            <ta e="T595" id="Seg_7861" s="T594">kondʼo</ta>
            <ta e="T596" id="Seg_7862" s="T595">ej</ta>
            <ta e="T597" id="Seg_7863" s="T596">mĭn-bi-m</ta>
            <ta e="T598" id="Seg_7864" s="T597">šo-bi-m</ta>
            <ta e="T599" id="Seg_7865" s="T598">dĭbər</ta>
            <ta e="T600" id="Seg_7866" s="T599">dĭ-Tə</ta>
            <ta e="T601" id="Seg_7867" s="T600">dĭ-zAŋ</ta>
            <ta e="T602" id="Seg_7868" s="T601">bar</ta>
            <ta e="T603" id="Seg_7869" s="T602">dĭbər</ta>
            <ta e="T604" id="Seg_7870" s="T603">nʼi-t-ziʔ</ta>
            <ta e="T605" id="Seg_7871" s="T604">kudo-nzə-laʔbə</ta>
            <ta e="T607" id="Seg_7872" s="T606">dĭgəttə</ta>
            <ta e="T608" id="Seg_7873" s="T607">dĭ</ta>
            <ta e="T609" id="Seg_7874" s="T608">Anissʼa</ta>
            <ta e="T610" id="Seg_7875" s="T609">bar</ta>
            <ta e="T611" id="Seg_7876" s="T610">kudo-nzə-bi</ta>
            <ta e="T613" id="Seg_7877" s="T612">nʼi-t</ta>
            <ta e="T614" id="Seg_7878" s="T613">a</ta>
            <ta e="T615" id="Seg_7879" s="T614">nʼi-t</ta>
            <ta e="T616" id="Seg_7880" s="T615">bar</ta>
            <ta e="T617" id="Seg_7881" s="T616">dĭ-Tə</ta>
            <ta e="T618" id="Seg_7882" s="T617">ĭmbi=nʼibudʼ</ta>
            <ta e="T619" id="Seg_7883" s="T618">tʼăbaktər-lV-j</ta>
            <ta e="T620" id="Seg_7884" s="T619">măn</ta>
            <ta e="T621" id="Seg_7885" s="T620">măn-ntə-m</ta>
            <ta e="T622" id="Seg_7886" s="T621">e-ʔ</ta>
            <ta e="T623" id="Seg_7887" s="T622">kürüm-ə-ʔ</ta>
            <ta e="T625" id="Seg_7888" s="T624">ija-n</ta>
            <ta e="T626" id="Seg_7889" s="T625">ĭmbi=də</ta>
            <ta e="T627" id="Seg_7890" s="T626">ej</ta>
            <ta e="T628" id="Seg_7891" s="T627">măn-ə-ʔ</ta>
            <ta e="T629" id="Seg_7892" s="T628">a</ta>
            <ta e="T630" id="Seg_7893" s="T629">dĭ</ta>
            <ta e="T631" id="Seg_7894" s="T630">üge</ta>
            <ta e="T632" id="Seg_7895" s="T631">kürüm-liA</ta>
            <ta e="T633" id="Seg_7896" s="T632">a</ta>
            <ta e="T634" id="Seg_7897" s="T633">ija-t</ta>
            <ta e="T635" id="Seg_7898" s="T634">bar</ta>
            <ta e="T636" id="Seg_7899" s="T635">davaj</ta>
            <ta e="T637" id="Seg_7900" s="T636">tʼor-zittə</ta>
            <ta e="T638" id="Seg_7901" s="T637">ku-liA-l</ta>
            <ta e="T639" id="Seg_7902" s="T638">kădaʔ</ta>
            <ta e="T640" id="Seg_7903" s="T639">dĭ-zAŋ</ta>
            <ta e="T641" id="Seg_7904" s="T640">ipek-ziʔ</ta>
            <ta e="T642" id="Seg_7905" s="T641">măna</ta>
            <ta e="T643" id="Seg_7906" s="T642">bădə-lV-jəʔ</ta>
            <ta e="T644" id="Seg_7907" s="T643">măn</ta>
            <ta e="T645" id="Seg_7908" s="T644">ipek</ta>
            <ta e="T646" id="Seg_7909" s="T645">am-liA</ta>
            <ta e="T647" id="Seg_7910" s="T646">i</ta>
            <ta e="T648" id="Seg_7911" s="T647">ĭššo</ta>
            <ta e="T649" id="Seg_7912" s="T648">kudo-nzə-laʔbə</ta>
            <ta e="T650" id="Seg_7913" s="T649">dĭgəttə</ta>
            <ta e="T652" id="Seg_7914" s="T651">šala</ta>
            <ta e="T653" id="Seg_7915" s="T652">amno-bi-bAʔ</ta>
            <ta e="T654" id="Seg_7916" s="T653">dĭgəttə</ta>
            <ta e="T655" id="Seg_7917" s="T654">Jelʼa</ta>
            <ta e="T656" id="Seg_7918" s="T655">šo-bi</ta>
            <ta e="T657" id="Seg_7919" s="T656">dărowă</ta>
            <ta e="T658" id="Seg_7920" s="T657">i-gA-l</ta>
            <ta e="T659" id="Seg_7921" s="T658">dărowă</ta>
            <ta e="T660" id="Seg_7922" s="T659">i-gA-l</ta>
            <ta e="T661" id="Seg_7923" s="T660">ĭmbi</ta>
            <ta e="T662" id="Seg_7924" s="T661">šo-bi-l</ta>
            <ta e="T663" id="Seg_7925" s="T662">ipek</ta>
            <ta e="T664" id="Seg_7926" s="T663">măndo-r-bi-m</ta>
            <ta e="T665" id="Seg_7927" s="T664">a</ta>
            <ta e="T666" id="Seg_7928" s="T665">măn</ta>
            <ta e="T667" id="Seg_7929" s="T666">Anissʼa-Tə</ta>
            <ta e="T668" id="Seg_7930" s="T667">măn-liA-m</ta>
            <ta e="T669" id="Seg_7931" s="T668">mĭ-ʔ</ta>
            <ta e="T670" id="Seg_7932" s="T669">dĭ-Tə</ta>
            <ta e="T671" id="Seg_7933" s="T670">onʼiʔ</ta>
            <ta e="T672" id="Seg_7934" s="T671">ipek</ta>
            <ta e="T673" id="Seg_7935" s="T672">dĭ</ta>
            <ta e="T674" id="Seg_7936" s="T673">mĭ-bi</ta>
            <ta e="T675" id="Seg_7937" s="T674">dĭ-Tə</ta>
            <ta e="T676" id="Seg_7938" s="T675">kumən</ta>
            <ta e="T677" id="Seg_7939" s="T676">pi-zittə</ta>
            <ta e="T678" id="Seg_7940" s="T677">a</ta>
            <ta e="T679" id="Seg_7941" s="T678">măn</ta>
            <ta e="T680" id="Seg_7942" s="T679">măn-ntə-m</ta>
            <ta e="T681" id="Seg_7943" s="T680">šala</ta>
            <ta e="T682" id="Seg_7944" s="T681">ej</ta>
            <ta e="T683" id="Seg_7945" s="T682">iʔgö</ta>
            <ta e="T684" id="Seg_7946" s="T683">kak</ta>
            <ta e="T685" id="Seg_7947" s="T684">măgăzin-gəʔ</ta>
            <ta e="T686" id="Seg_7948" s="T685">dĭgəttə</ta>
            <ta e="T687" id="Seg_7949" s="T686">dĭn</ta>
            <ta e="T688" id="Seg_7950" s="T687">dĭ</ta>
            <ta e="T689" id="Seg_7951" s="T688">dĭ-Tə</ta>
            <ta e="T690" id="Seg_7952" s="T689">mĭ-bi</ta>
            <ta e="T691" id="Seg_7953" s="T690">teʔdə</ta>
            <ta e="T692" id="Seg_7954" s="T691">biəʔ</ta>
            <ta e="T693" id="Seg_7955" s="T692">kăpʼejka-jəʔ</ta>
            <ta e="T695" id="Seg_7956" s="T694">dĭgəttə</ta>
            <ta e="T696" id="Seg_7957" s="T695">măn</ta>
            <ta e="T697" id="Seg_7958" s="T696">šo-bi-m</ta>
            <ta e="T698" id="Seg_7959" s="T697">tüžöj-m</ta>
            <ta e="T699" id="Seg_7960" s="T698">bĭs-j-bi-m</ta>
            <ta e="T700" id="Seg_7961" s="T699">büzəj</ta>
            <ta e="T701" id="Seg_7962" s="T700">bĭs-lə-bi-m</ta>
            <ta e="T702" id="Seg_7963" s="T701">noʔ</ta>
            <ta e="T703" id="Seg_7964" s="T702">mĭ-bi-m</ta>
            <ta e="T704" id="Seg_7965" s="T703">dĭgəttə</ta>
            <ta e="T705" id="Seg_7966" s="T704">bü-j-lAʔ</ta>
            <ta e="T706" id="Seg_7967" s="T705">mĭn-bi-m</ta>
            <ta e="T707" id="Seg_7968" s="T706">i</ta>
            <ta e="T709" id="Seg_7969" s="T708">šo-bi-m</ta>
            <ta e="T710" id="Seg_7970" s="T709">tura-Tə</ta>
            <ta e="T711" id="Seg_7971" s="T710">üjü-m</ta>
            <ta e="T712" id="Seg_7972" s="T711">kănzə-laːm-bi</ta>
            <ta e="T713" id="Seg_7973" s="T712">pʼeːš-Tə</ta>
            <ta e="T716" id="Seg_7974" s="T715">sʼa-bi-m</ta>
            <ta e="T717" id="Seg_7975" s="T716">pʼeːš-Tə</ta>
            <ta e="T719" id="Seg_7976" s="T718">sʼa-bi-m</ta>
            <ta e="T720" id="Seg_7977" s="T719">dĭgəttə</ta>
            <ta e="T721" id="Seg_7978" s="T720">üjü-m</ta>
            <ta e="T723" id="Seg_7979" s="T722">ojimulem</ta>
            <ta e="T724" id="Seg_7980" s="T723">dĭ-zAŋ</ta>
            <ta e="T726" id="Seg_7981" s="T725">šo-bi-jəʔ</ta>
            <ta e="T727" id="Seg_7982" s="T726">i</ta>
            <ta e="T728" id="Seg_7983" s="T727">tʼăbaktər-zittə</ta>
            <ta e="T730" id="Seg_7984" s="T729">dĭgəttə</ta>
            <ta e="T731" id="Seg_7985" s="T730">tʼăbaktər-zittə</ta>
            <ta e="T732" id="Seg_7986" s="T731">nadə</ta>
            <ta e="T733" id="Seg_7987" s="T732">a</ta>
            <ta e="T734" id="Seg_7988" s="T733">dĭn</ta>
            <ta e="T735" id="Seg_7989" s="T734">mašina</ta>
            <ta e="T736" id="Seg_7990" s="T735">bar</ta>
            <ta e="T737" id="Seg_7991" s="T736">kürüm-laʔbə</ta>
            <ta e="T738" id="Seg_7992" s="T737">ej</ta>
            <ta e="T739" id="Seg_7993" s="T738">mĭ-liA</ta>
            <ta e="T740" id="Seg_7994" s="T739">tʼăbaktər-zittə</ta>
            <ta e="T741" id="Seg_7995" s="T740">miʔ</ta>
            <ta e="T742" id="Seg_7996" s="T741">baʔbdə-luʔbdə-bi-bAʔ</ta>
            <ta e="T743" id="Seg_7997" s="T742">a</ta>
            <ta e="T744" id="Seg_7998" s="T743">dĭ-zAŋ</ta>
            <ta e="T987" id="Seg_7999" s="T744">kan-lAʔ</ta>
            <ta e="T745" id="Seg_8000" s="T987">tʼür-bi-jəʔ</ta>
            <ta e="T746" id="Seg_8001" s="T745">ej</ta>
            <ta e="T747" id="Seg_8002" s="T746">tʼăbaktər-bi-bAʔ</ta>
            <ta e="T748" id="Seg_8003" s="T747">a</ta>
            <ta e="T749" id="Seg_8004" s="T748">tüj</ta>
            <ta e="T750" id="Seg_8005" s="T749">šo-bi-jəʔ</ta>
            <ta e="T751" id="Seg_8006" s="T750">bazoʔ</ta>
            <ta e="T753" id="Seg_8007" s="T752">tʼăbaktər-zittə</ta>
            <ta e="T755" id="Seg_8008" s="T754">šo-bi-jəʔ</ta>
            <ta e="T756" id="Seg_8009" s="T755">a</ta>
            <ta e="T757" id="Seg_8010" s="T756">măn</ta>
            <ta e="T758" id="Seg_8011" s="T757">măn-ntə-m</ta>
            <ta e="T759" id="Seg_8012" s="T758">gijen</ta>
            <ta e="T760" id="Seg_8013" s="T759">i-bi-lAʔ</ta>
            <ta e="T761" id="Seg_8014" s="T760">da</ta>
            <ta e="T762" id="Seg_8015" s="T761">pa</ta>
            <ta e="T763" id="Seg_8016" s="T762">kür-bi-bAʔ</ta>
            <ta e="T764" id="Seg_8017" s="T763">bos-də</ta>
            <ta e="T765" id="Seg_8018" s="T764">tura</ta>
            <ta e="T766" id="Seg_8019" s="T765">kür-bi-bAʔ</ta>
            <ta e="T767" id="Seg_8020" s="T766">i</ta>
            <ta e="T768" id="Seg_8021" s="T767">urgaja-n</ta>
            <ta e="T769" id="Seg_8022" s="T768">tura-Tə</ta>
            <ta e="T771" id="Seg_8023" s="T770">kür-bi-bAʔ</ta>
            <ta e="T772" id="Seg_8024" s="T771">i</ta>
            <ta e="T773" id="Seg_8025" s="T772">döbər</ta>
            <ta e="T774" id="Seg_8026" s="T773">šo-bi-bAʔ</ta>
            <ta e="T776" id="Seg_8027" s="T775">dĭgəttə</ta>
            <ta e="T777" id="Seg_8028" s="T776">ija-t</ta>
            <ta e="T778" id="Seg_8029" s="T777">šo-bi</ta>
            <ta e="T779" id="Seg_8030" s="T778">bălʼnʼitsa-Kən</ta>
            <ta e="T780" id="Seg_8031" s="T779">davaj</ta>
            <ta e="T781" id="Seg_8032" s="T780">ešši-zem</ta>
            <ta e="T783" id="Seg_8033" s="T782">ĭmbi=də</ta>
            <ta e="T784" id="Seg_8034" s="T783">ej</ta>
            <ta e="T785" id="Seg_8035" s="T784">a-liA-jəʔ</ta>
            <ta e="T786" id="Seg_8036" s="T785">na</ta>
            <ta e="T787" id="Seg_8037" s="T786">pa-jəʔ</ta>
            <ta e="T788" id="Seg_8038" s="T787">hen-ə-ʔ</ta>
            <ta e="T789" id="Seg_8039" s="T788">ăgrada-Kən</ta>
            <ta e="T790" id="Seg_8040" s="T789">ato</ta>
            <ta e="T791" id="Seg_8041" s="T790">aba-l</ta>
            <ta e="T792" id="Seg_8042" s="T791">šo-lV-j</ta>
            <ta e="T793" id="Seg_8043" s="T792">ĭmbi=də</ta>
            <ta e="T794" id="Seg_8044" s="T793">ej</ta>
            <ta e="T795" id="Seg_8045" s="T794">mĭ-lV-j</ta>
            <ta e="T796" id="Seg_8046" s="T795">dĭ</ta>
            <ta e="T797" id="Seg_8047" s="T796">kan-bi</ta>
            <ta e="T798" id="Seg_8048" s="T797">da</ta>
            <ta e="T799" id="Seg_8049" s="T798">davaj</ta>
            <ta e="T800" id="Seg_8050" s="T799">hen-zittə</ta>
            <ta e="T801" id="Seg_8051" s="T800">dĭgəttə</ta>
            <ta e="T802" id="Seg_8052" s="T801">šo-bi</ta>
            <ta e="T803" id="Seg_8053" s="T802">multʼa-Kən</ta>
            <ta e="T804" id="Seg_8054" s="T803">bazə-jd-bi-jəʔ</ta>
            <ta e="T805" id="Seg_8055" s="T804">dĭgəttə</ta>
            <ta e="T806" id="Seg_8056" s="T805">măna</ta>
            <ta e="T807" id="Seg_8057" s="T806">măn-ntə-jəʔ</ta>
            <ta e="T808" id="Seg_8058" s="T807">kan-ə-ʔ</ta>
            <ta e="T809" id="Seg_8059" s="T808">multʼa-Tə</ta>
            <ta e="T810" id="Seg_8060" s="T809">măn</ta>
            <ta e="T811" id="Seg_8061" s="T810">kan-bi-m</ta>
            <ta e="T812" id="Seg_8062" s="T811">bazə-jd-zittə</ta>
            <ta e="T813" id="Seg_8063" s="T812">dĭgəttə</ta>
            <ta e="T814" id="Seg_8064" s="T813">Jelʼa</ta>
            <ta e="T815" id="Seg_8065" s="T814">šo-bi</ta>
            <ta e="T816" id="Seg_8066" s="T815">bazə-jd-zittə</ta>
            <ta e="T817" id="Seg_8067" s="T816">dĭgəttə</ta>
            <ta e="T818" id="Seg_8068" s="T817">tibi</ta>
            <ta e="T819" id="Seg_8069" s="T818">šo-bi</ta>
            <ta e="T820" id="Seg_8070" s="T819">bazə-jd-bi</ta>
            <ta e="T821" id="Seg_8071" s="T820">dĭgəttə</ta>
            <ta e="T822" id="Seg_8072" s="T821">aba-t</ta>
            <ta e="T823" id="Seg_8073" s="T822">šo-bi</ta>
            <ta e="T824" id="Seg_8074" s="T823">det-bi</ta>
            <ta e="T825" id="Seg_8075" s="T824">čulki-jəʔ</ta>
            <ta e="T826" id="Seg_8076" s="T825">piʔme-jəʔ</ta>
            <ta e="T827" id="Seg_8077" s="T826">det-bi</ta>
            <ta e="T829" id="Seg_8078" s="T828">dĭgəttə</ta>
            <ta e="T830" id="Seg_8079" s="T829">šo-bi</ta>
            <ta e="T831" id="Seg_8080" s="T830">ara</ta>
            <ta e="T832" id="Seg_8081" s="T831">det-bi</ta>
            <ta e="T833" id="Seg_8082" s="T832">onʼiʔ</ta>
            <ta e="T834" id="Seg_8083" s="T833">tibi</ta>
            <ta e="T835" id="Seg_8084" s="T834">dĭ-m</ta>
            <ta e="T836" id="Seg_8085" s="T835">det-bi</ta>
            <ta e="T837" id="Seg_8086" s="T836">ine-t-ziʔ</ta>
            <ta e="T838" id="Seg_8087" s="T837">dĭgəttə</ta>
            <ta e="T839" id="Seg_8088" s="T838">dĭ</ta>
            <ta e="T840" id="Seg_8089" s="T839">dĭ-Tə</ta>
            <ta e="T841" id="Seg_8090" s="T840">ara</ta>
            <ta e="T842" id="Seg_8091" s="T841">kămnə-bi</ta>
            <ta e="T843" id="Seg_8092" s="T842">šala</ta>
            <ta e="T844" id="Seg_8093" s="T843">amor-bi</ta>
            <ta e="T846" id="Seg_8094" s="T845">kan-bi</ta>
            <ta e="T847" id="Seg_8095" s="T846">maʔ-gəndə</ta>
            <ta e="T849" id="Seg_8096" s="T848">dĭ</ta>
            <ta e="T850" id="Seg_8097" s="T849">ne</ta>
            <ta e="T851" id="Seg_8098" s="T850">ugaːndə</ta>
            <ta e="T852" id="Seg_8099" s="T851">kurojok</ta>
            <ta e="T853" id="Seg_8100" s="T852">pa</ta>
            <ta e="T854" id="Seg_8101" s="T853">i-bi</ta>
            <ta e="T855" id="Seg_8102" s="T854">da</ta>
            <ta e="T856" id="Seg_8103" s="T855">büzəj-m</ta>
            <ta e="T858" id="Seg_8104" s="T857">pa-ziʔ</ta>
            <ta e="T859" id="Seg_8105" s="T858">toʔbdə-nar-bi</ta>
            <ta e="T860" id="Seg_8106" s="T859">sima-t-Tə</ta>
            <ta e="T861" id="Seg_8107" s="T860">kăj-də</ta>
            <ta e="T862" id="Seg_8108" s="T861">bar</ta>
            <ta e="T863" id="Seg_8109" s="T862">mʼaŋ-laʔbə</ta>
            <ta e="T864" id="Seg_8110" s="T863">a</ta>
            <ta e="T865" id="Seg_8111" s="T864">măn</ta>
            <ta e="T866" id="Seg_8112" s="T865">bos-də</ta>
            <ta e="T867" id="Seg_8113" s="T866">büzəj-m</ta>
            <ta e="T868" id="Seg_8114" s="T867">ej</ta>
            <ta e="T869" id="Seg_8115" s="T868">münör-liA-m</ta>
            <ta e="T870" id="Seg_8116" s="T869">uda-ziʔ</ta>
            <ta e="T871" id="Seg_8117" s="T870">uda-ziʔ</ta>
            <ta e="T872" id="Seg_8118" s="T871">toʔbdə-lV-m</ta>
            <ta e="T873" id="Seg_8119" s="T872">šala</ta>
            <ta e="T874" id="Seg_8120" s="T873">bar</ta>
            <ta e="T875" id="Seg_8121" s="T874">bĭs-luʔbdə-bi</ta>
            <ta e="T877" id="Seg_8122" s="T876">măn</ta>
            <ta e="T878" id="Seg_8123" s="T877">tura-Kən</ta>
            <ta e="T879" id="Seg_8124" s="T878">kür-bi-m</ta>
            <ta e="T880" id="Seg_8125" s="T879">mašina-m</ta>
            <ta e="T882" id="Seg_8126" s="T880">i</ta>
            <ta e="T883" id="Seg_8127" s="T882">dĭ-zAŋ</ta>
            <ta e="T884" id="Seg_8128" s="T883">oʔb</ta>
            <ta e="T885" id="Seg_8129" s="T884">šide</ta>
            <ta e="T886" id="Seg_8130" s="T885">nagur</ta>
            <ta e="T887" id="Seg_8131" s="T886">teʔdə</ta>
            <ta e="T888" id="Seg_8132" s="T887">dĭgəttə</ta>
            <ta e="T889" id="Seg_8133" s="T888">Vanʼa</ta>
            <ta e="T890" id="Seg_8134" s="T889">măn-ntə</ta>
            <ta e="T891" id="Seg_8135" s="T890">büzəj</ta>
            <ta e="T892" id="Seg_8136" s="T891">kereʔ</ta>
            <ta e="T893" id="Seg_8137" s="T892">i</ta>
            <ta e="T894" id="Seg_8138" s="T893">šoška-jəʔ</ta>
            <ta e="T895" id="Seg_8139" s="T894">a</ta>
            <ta e="T896" id="Seg_8140" s="T895">miʔ</ta>
            <ta e="T897" id="Seg_8141" s="T896">bar</ta>
            <ta e="T898" id="Seg_8142" s="T897">kakənar-bi-bAʔ</ta>
            <ta e="T900" id="Seg_8143" s="T899">teinen</ta>
            <ta e="T901" id="Seg_8144" s="T900">padʼi</ta>
            <ta e="T902" id="Seg_8145" s="T901">iʔgö</ta>
            <ta e="T903" id="Seg_8146" s="T902">tʼăbaktər-bi-bAʔ</ta>
            <ta e="T904" id="Seg_8147" s="T903">tüj</ta>
            <ta e="T905" id="Seg_8148" s="T904">nadə</ta>
            <ta e="T906" id="Seg_8149" s="T905">maʔ-gənʼi</ta>
            <ta e="T907" id="Seg_8150" s="T906">kan-zittə</ta>
            <ta e="T908" id="Seg_8151" s="T907">amor-zittə</ta>
            <ta e="T909" id="Seg_8152" s="T908">iʔbö-zittə</ta>
            <ta e="T911" id="Seg_8153" s="T910">šala</ta>
            <ta e="T913" id="Seg_8154" s="T912">bor</ta>
            <ta e="T914" id="Seg_8155" s="T913">bar</ta>
            <ta e="T915" id="Seg_8156" s="T914">nʼiʔdə</ta>
            <ta e="T916" id="Seg_8157" s="T915">kandə-gA</ta>
            <ta e="T917" id="Seg_8158" s="T916">ugaːndə</ta>
            <ta e="T918" id="Seg_8159" s="T917">šišəge</ta>
            <ta e="T919" id="Seg_8160" s="T918">mo-lV-j</ta>
            <ta e="T920" id="Seg_8161" s="T919">teinen</ta>
            <ta e="T921" id="Seg_8162" s="T920">nüdʼi-n</ta>
            <ta e="T922" id="Seg_8163" s="T921">sĭri</ta>
            <ta e="T923" id="Seg_8164" s="T922">ertə-n</ta>
            <ta e="T924" id="Seg_8165" s="T923">uʔbdə-bi-m</ta>
            <ta e="T925" id="Seg_8166" s="T924">bar</ta>
            <ta e="T926" id="Seg_8167" s="T925">šišəge</ta>
            <ta e="T927" id="Seg_8168" s="T926">mo-laːm-bi</ta>
            <ta e="T929" id="Seg_8169" s="T928">miʔnʼibeʔ</ta>
            <ta e="T930" id="Seg_8170" s="T929">teinen</ta>
            <ta e="T931" id="Seg_8171" s="T930">nadə</ta>
            <ta e="T932" id="Seg_8172" s="T931">kan-zittə</ta>
            <ta e="T933" id="Seg_8173" s="T932">Tanʼa-Tə</ta>
            <ta e="T934" id="Seg_8174" s="T933">štobɨ</ta>
            <ta e="T935" id="Seg_8175" s="T934">dĭ</ta>
            <ta e="T936" id="Seg_8176" s="T935">ipek</ta>
            <ta e="T937" id="Seg_8177" s="T936">i-bi</ta>
            <ta e="T938" id="Seg_8178" s="T937">Văznesenka-gəʔ</ta>
            <ta e="T939" id="Seg_8179" s="T938">i</ta>
            <ta e="T940" id="Seg_8180" s="T939">det-bi</ta>
            <ta e="T941" id="Seg_8181" s="T940">miʔnʼibeʔ</ta>
            <ta e="T944" id="Seg_8182" s="T943">nadə</ta>
            <ta e="T945" id="Seg_8183" s="T944">aktʼa-m</ta>
            <ta e="T946" id="Seg_8184" s="T945">i-zittə</ta>
            <ta e="T947" id="Seg_8185" s="T946">dĭ-Tə</ta>
            <ta e="T948" id="Seg_8186" s="T947">ej</ta>
            <ta e="T949" id="Seg_8187" s="T948">bos-gəndə</ta>
            <ta e="T950" id="Seg_8188" s="T949">det-zittə</ta>
            <ta e="T951" id="Seg_8189" s="T950">dĭʔə</ta>
            <ta e="T952" id="Seg_8190" s="T951">ine-t-ziʔ</ta>
            <ta e="T953" id="Seg_8191" s="T952">mĭn-lV-j</ta>
            <ta e="T954" id="Seg_8192" s="T953">i</ta>
            <ta e="T955" id="Seg_8193" s="T954">det-lV-j</ta>
            <ta e="T957" id="Seg_8194" s="T956">ej</ta>
            <ta e="T958" id="Seg_8195" s="T957">tĭmne-m</ta>
            <ta e="T959" id="Seg_8196" s="T958">i-lV-j</ta>
            <ta e="T960" id="Seg_8197" s="T959">ilʼi</ta>
            <ta e="T961" id="Seg_8198" s="T960">ej</ta>
            <ta e="T962" id="Seg_8199" s="T961">nadə</ta>
            <ta e="T963" id="Seg_8200" s="T962">kan-zittə</ta>
            <ta e="T964" id="Seg_8201" s="T963">dĭ-ziʔ</ta>
            <ta e="T965" id="Seg_8202" s="T964">tʼăbaktər-zittə</ta>
            <ta e="T966" id="Seg_8203" s="T965">možet</ta>
            <ta e="T967" id="Seg_8204" s="T966">i-lV-j</ta>
            <ta e="T968" id="Seg_8205" s="T967">no</ta>
            <ta e="T969" id="Seg_8206" s="T968">kan-ə-ʔ</ta>
            <ta e="T970" id="Seg_8207" s="T969">tʼăbaktər-ə-ʔ</ta>
            <ta e="T972" id="Seg_8208" s="T971">dĭ</ta>
            <ta e="T973" id="Seg_8209" s="T972">možet</ta>
            <ta e="T974" id="Seg_8210" s="T973">mĭn-lV-j</ta>
            <ta e="T975" id="Seg_8211" s="T974">măgăzin-Kən</ta>
            <ta e="T976" id="Seg_8212" s="T975">dĭn</ta>
            <ta e="T978" id="Seg_8213" s="T977">nu-zittə</ta>
            <ta e="T979" id="Seg_8214" s="T978">nadə</ta>
            <ta e="T980" id="Seg_8215" s="T979">ipek</ta>
            <ta e="T981" id="Seg_8216" s="T980">i-zittə</ta>
            <ta e="T982" id="Seg_8217" s="T981">măn</ta>
            <ta e="T983" id="Seg_8218" s="T982">ej</ta>
            <ta e="T984" id="Seg_8219" s="T983">i-lV-m</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_8220" s="T0">go-IMP.2PL</ta>
            <ta e="T2" id="Seg_8221" s="T1">sauna-LAT</ta>
            <ta e="T3" id="Seg_8222" s="T2">%together</ta>
            <ta e="T4" id="Seg_8223" s="T3">two-COLL</ta>
            <ta e="T5" id="Seg_8224" s="T4">this.[NOM.SG]</ta>
            <ta e="T6" id="Seg_8225" s="T5">you.DAT</ta>
            <ta e="T7" id="Seg_8226" s="T6">back-ACC.3SG</ta>
            <ta e="T8" id="Seg_8227" s="T7">rub-FUT-3SG</ta>
            <ta e="T9" id="Seg_8228" s="T8">soap-INS</ta>
            <ta e="T10" id="Seg_8229" s="T9">body.scrubber-INS</ta>
            <ta e="T11" id="Seg_8230" s="T10">and</ta>
            <ta e="T12" id="Seg_8231" s="T11">you.NOM</ta>
            <ta e="T13" id="Seg_8232" s="T12">this-LAT</ta>
            <ta e="T14" id="Seg_8233" s="T13">well</ta>
            <ta e="T16" id="Seg_8234" s="T15">be.ashamed-PRS-1SG</ta>
            <ta e="T17" id="Seg_8235" s="T16">not</ta>
            <ta e="T18" id="Seg_8236" s="T17">one.wants</ta>
            <ta e="T19" id="Seg_8237" s="T18">go-INF.LAT</ta>
            <ta e="T20" id="Seg_8238" s="T19">very</ta>
            <ta e="T21" id="Seg_8239" s="T20">be.ashamed-PRS-1SG</ta>
            <ta e="T22" id="Seg_8240" s="T21">Russian-GEN</ta>
            <ta e="T23" id="Seg_8241" s="T22">settlement-LOC</ta>
            <ta e="T24" id="Seg_8242" s="T23">sauna-LAT</ta>
            <ta e="T25" id="Seg_8243" s="T24">NEG</ta>
            <ta e="T27" id="Seg_8244" s="T26">NEG</ta>
            <ta e="T28" id="Seg_8245" s="T27">go-PRS-3PL</ta>
            <ta e="T29" id="Seg_8246" s="T28">%together</ta>
            <ta e="T30" id="Seg_8247" s="T29">woman-PL</ta>
            <ta e="T31" id="Seg_8248" s="T30">man-PL</ta>
            <ta e="T32" id="Seg_8249" s="T31">woman-PL</ta>
            <ta e="T33" id="Seg_8250" s="T32">one.[NOM.SG]</ta>
            <ta e="T35" id="Seg_8251" s="T34">walk-3PL</ta>
            <ta e="T36" id="Seg_8252" s="T35">and</ta>
            <ta e="T37" id="Seg_8253" s="T36">man-PL</ta>
            <ta e="T38" id="Seg_8254" s="T37">tomorrow</ta>
            <ta e="T39" id="Seg_8255" s="T38">light-DUR-3PL</ta>
            <ta e="T40" id="Seg_8256" s="T39">again</ta>
            <ta e="T41" id="Seg_8257" s="T40">one.[NOM.SG]</ta>
            <ta e="T42" id="Seg_8258" s="T41">go-IPFVZ-PRS-3PL</ta>
            <ta e="T43" id="Seg_8259" s="T42">man-PL</ta>
            <ta e="T45" id="Seg_8260" s="T44">we.NOM</ta>
            <ta e="T46" id="Seg_8261" s="T45">sauna-LOC</ta>
            <ta e="T47" id="Seg_8262" s="T46">girl-PL</ta>
            <ta e="T48" id="Seg_8263" s="T47">two-COLL</ta>
            <ta e="T49" id="Seg_8264" s="T48">go-IPFVZ-PRS-3PL</ta>
            <ta e="T50" id="Seg_8265" s="T49">then</ta>
            <ta e="T51" id="Seg_8266" s="T50">mother-NOM/GEN.3SG</ta>
            <ta e="T52" id="Seg_8267" s="T51">go-FUT-3SG</ta>
            <ta e="T53" id="Seg_8268" s="T52">one.[NOM.SG]</ta>
            <ta e="T54" id="Seg_8269" s="T53">girl.[NOM.SG]</ta>
            <ta e="T55" id="Seg_8270" s="T54">one.[NOM.SG]</ta>
            <ta e="T56" id="Seg_8271" s="T55">boy.[NOM.SG]</ta>
            <ta e="T57" id="Seg_8272" s="T56">wash-FUT-3SG</ta>
            <ta e="T58" id="Seg_8273" s="T57">this-ACC.PL</ta>
            <ta e="T59" id="Seg_8274" s="T58">this-PL</ta>
            <ta e="T61" id="Seg_8275" s="T60">come-PRS-3PL</ta>
            <ta e="T62" id="Seg_8276" s="T61">then</ta>
            <ta e="T63" id="Seg_8277" s="T62">man.[NOM.SG]</ta>
            <ta e="T64" id="Seg_8278" s="T63">go-DUR.[3SG]</ta>
            <ta e="T65" id="Seg_8279" s="T64">and</ta>
            <ta e="T66" id="Seg_8280" s="T65">then</ta>
            <ta e="T67" id="Seg_8281" s="T66">I.NOM</ta>
            <ta e="T68" id="Seg_8282" s="T67">go-DUR-1SG</ta>
            <ta e="T69" id="Seg_8283" s="T68">wash-DRV-PRS-1SG</ta>
            <ta e="T71" id="Seg_8284" s="T70">I.NOM</ta>
            <ta e="T72" id="Seg_8285" s="T71">aunt-NOM/GEN/ACC.1SG</ta>
            <ta e="T73" id="Seg_8286" s="T72">go-PST.[3SG]</ta>
            <ta e="T988" id="Seg_8287" s="T73">Aginskoe</ta>
            <ta e="T74" id="Seg_8288" s="T988">settlement-LAT</ta>
            <ta e="T75" id="Seg_8289" s="T74">there</ta>
            <ta e="T76" id="Seg_8290" s="T75">like</ta>
            <ta e="T77" id="Seg_8291" s="T76">time.[NOM.SG]</ta>
            <ta e="T78" id="Seg_8292" s="T77">sauna.[NOM.SG]</ta>
            <ta e="T79" id="Seg_8293" s="T78">light-PST-3PL</ta>
            <ta e="T80" id="Seg_8294" s="T79">all</ta>
            <ta e="T81" id="Seg_8295" s="T80">wash-PST-3PL</ta>
            <ta e="T82" id="Seg_8296" s="T81">say-IPFVZ-3PL</ta>
            <ta e="T83" id="Seg_8297" s="T82">go-EP-IMP.2SG</ta>
            <ta e="T84" id="Seg_8298" s="T83">sauna-LAT</ta>
            <ta e="T85" id="Seg_8299" s="T84">and</ta>
            <ta e="T86" id="Seg_8300" s="T85">I.NOM</ta>
            <ta e="T87" id="Seg_8301" s="T86">and</ta>
            <ta e="T88" id="Seg_8302" s="T87">this.[NOM.SG]</ta>
            <ta e="T89" id="Seg_8303" s="T88">go-PST.[3SG]</ta>
            <ta e="T90" id="Seg_8304" s="T89">NEG</ta>
            <ta e="T91" id="Seg_8305" s="T90">know-3SG.O</ta>
            <ta e="T92" id="Seg_8306" s="T91">where.to</ta>
            <ta e="T93" id="Seg_8307" s="T92">water.[NOM.SG]</ta>
            <ta e="T94" id="Seg_8308" s="T93">pour-INF.LAT</ta>
            <ta e="T95" id="Seg_8309" s="T94">stove-LAT</ta>
            <ta e="T96" id="Seg_8310" s="T95">pour-PST-1SG</ta>
            <ta e="T97" id="Seg_8311" s="T96">PTCL</ta>
            <ta e="T98" id="Seg_8312" s="T97">wall-PL</ta>
            <ta e="T99" id="Seg_8313" s="T98">water-INS</ta>
            <ta e="T109" id="Seg_8314" s="T108">then</ta>
            <ta e="T110" id="Seg_8315" s="T109">wash.oneself-PST-1SG</ta>
            <ta e="T111" id="Seg_8316" s="T110">come-PST-1SG</ta>
            <ta e="T112" id="Seg_8317" s="T111">what.[NOM.SG]</ta>
            <ta e="T113" id="Seg_8318" s="T112">wash.oneself-PST-2SG</ta>
            <ta e="T114" id="Seg_8319" s="T113">wash.oneself-PST-2SG</ta>
            <ta e="T115" id="Seg_8320" s="T114">NEG</ta>
            <ta e="T116" id="Seg_8321" s="T115">put-PST-2SG</ta>
            <ta e="T117" id="Seg_8322" s="T116">NEG</ta>
            <ta e="T118" id="Seg_8323" s="T117">put-PST-1SG</ta>
            <ta e="T120" id="Seg_8324" s="T119">then</ta>
            <ta e="T122" id="Seg_8325" s="T121">say-PRS-3PL</ta>
            <ta e="T123" id="Seg_8326" s="T122">go-EP-IMP.2SG</ta>
            <ta e="T124" id="Seg_8327" s="T123">cellar-ACC.3SG</ta>
            <ta e="T125" id="Seg_8328" s="T124">meat.[NOM.SG]</ta>
            <ta e="T126" id="Seg_8329" s="T125">bring-IMP.2SG</ta>
            <ta e="T127" id="Seg_8330" s="T126">meatball-PL-LAT</ta>
            <ta e="T128" id="Seg_8331" s="T127">I.NOM</ta>
            <ta e="T129" id="Seg_8332" s="T128">go-PST-1SG</ta>
            <ta e="T131" id="Seg_8333" s="T130">stand-PRS-1SG</ta>
            <ta e="T132" id="Seg_8334" s="T131">meat.[NOM.SG]</ta>
            <ta e="T133" id="Seg_8335" s="T132">many</ta>
            <ta e="T134" id="Seg_8336" s="T133">and</ta>
            <ta e="T135" id="Seg_8337" s="T134">cauldron.[NOM.SG]</ta>
            <ta e="T136" id="Seg_8338" s="T135">NEG.EX.[3SG]</ta>
            <ta e="T137" id="Seg_8339" s="T136">then</ta>
            <ta e="T138" id="Seg_8340" s="T137">return-PST-1SG</ta>
            <ta e="T139" id="Seg_8341" s="T138">what.[NOM.SG]</ta>
            <ta e="T140" id="Seg_8342" s="T139">meat.[NOM.SG]</ta>
            <ta e="T141" id="Seg_8343" s="T140">NEG</ta>
            <ta e="T143" id="Seg_8344" s="T142">bring-PRS-2SG</ta>
            <ta e="T144" id="Seg_8345" s="T143">well</ta>
            <ta e="T145" id="Seg_8346" s="T144">meat.[NOM.SG]</ta>
            <ta e="T146" id="Seg_8347" s="T145">many</ta>
            <ta e="T147" id="Seg_8348" s="T146">and</ta>
            <ta e="T148" id="Seg_8349" s="T147">cauldron.[NOM.SG]</ta>
            <ta e="T149" id="Seg_8350" s="T148">NEG.EX.[3SG]</ta>
            <ta e="T150" id="Seg_8351" s="T149">this-PL</ta>
            <ta e="T151" id="Seg_8352" s="T150">INCH</ta>
            <ta e="T152" id="Seg_8353" s="T151">laugh-INF.LAT</ta>
            <ta e="T154" id="Seg_8354" s="T153">then</ta>
            <ta e="T155" id="Seg_8355" s="T154">say-PRS-3PL</ta>
            <ta e="T156" id="Seg_8356" s="T155">go-EP-IMP.2SG</ta>
            <ta e="T157" id="Seg_8357" s="T156">plant.garden-LAT</ta>
            <ta e="T158" id="Seg_8358" s="T157">bring-IMP.2SG</ta>
            <ta e="T159" id="Seg_8359" s="T158">cucumber.PL-PL</ta>
            <ta e="T160" id="Seg_8360" s="T159">I.NOM</ta>
            <ta e="T161" id="Seg_8361" s="T160">go-PST-1SG</ta>
            <ta e="T162" id="Seg_8362" s="T161">look-FRQ-PST-1SG</ta>
            <ta e="T163" id="Seg_8363" s="T162">look-FRQ-PST-1SG</ta>
            <ta e="T164" id="Seg_8364" s="T163">tear-PRS-1SG</ta>
            <ta e="T165" id="Seg_8365" s="T164">%%</ta>
            <ta e="T166" id="Seg_8366" s="T165">more</ta>
            <ta e="T167" id="Seg_8367" s="T166">what=INDEF</ta>
            <ta e="T168" id="Seg_8368" s="T167">then</ta>
            <ta e="T169" id="Seg_8369" s="T168">%%</ta>
            <ta e="T170" id="Seg_8370" s="T169">two.[NOM.SG]</ta>
            <ta e="T173" id="Seg_8371" s="T172">take-PST-1SG</ta>
            <ta e="T174" id="Seg_8372" s="T173">and</ta>
            <ta e="T175" id="Seg_8373" s="T174">come-PST-1SG</ta>
            <ta e="T176" id="Seg_8374" s="T175">house-LAT</ta>
            <ta e="T177" id="Seg_8375" s="T176">one.[NOM.SG]</ta>
            <ta e="T178" id="Seg_8376" s="T177">woman.[NOM.SG]</ta>
            <ta e="T179" id="Seg_8377" s="T178">cellar-LAT</ta>
            <ta e="T181" id="Seg_8378" s="T180">jump-MOM-PST.[3SG]</ta>
            <ta e="T182" id="Seg_8379" s="T181">one.[NOM.SG]</ta>
            <ta e="T184" id="Seg_8380" s="T183">woman.[NOM.SG]</ta>
            <ta e="T185" id="Seg_8381" s="T184">outwards</ta>
            <ta e="T186" id="Seg_8382" s="T185">%nearly</ta>
            <ta e="T188" id="Seg_8383" s="T187">I.ACC</ta>
            <ta e="T189" id="Seg_8384" s="T188">NEG</ta>
            <ta e="T190" id="Seg_8385" s="T189">stamp-PST.[3SG]</ta>
            <ta e="T192" id="Seg_8386" s="T191">then</ta>
            <ta e="T194" id="Seg_8387" s="T193">this</ta>
            <ta e="T195" id="Seg_8388" s="T194">woman-GEN.PL</ta>
            <ta e="T196" id="Seg_8389" s="T195">father</ta>
            <ta e="T197" id="Seg_8390" s="T196">father-NOM/GEN.3SG</ta>
            <ta e="T198" id="Seg_8391" s="T197">say-IPFVZ.[3SG]</ta>
            <ta e="T199" id="Seg_8392" s="T198">where.to</ta>
            <ta e="T200" id="Seg_8393" s="T199">you.PL.NOM</ta>
            <ta e="T201" id="Seg_8394" s="T200">run-RES-PST-2PL</ta>
            <ta e="T202" id="Seg_8395" s="T201">bring-IMP.2PL</ta>
            <ta e="T203" id="Seg_8396" s="T202">and</ta>
            <ta e="T204" id="Seg_8397" s="T203">pour-IMP.2PL</ta>
            <ta e="T205" id="Seg_8398" s="T204">soup.[NOM.SG]</ta>
            <ta e="T206" id="Seg_8399" s="T205">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T207" id="Seg_8400" s="T206">say-IPFVZ.[3SG]</ta>
            <ta e="T208" id="Seg_8401" s="T207">see-PRS-2SG</ta>
            <ta e="T209" id="Seg_8402" s="T208">this.[NOM.SG]</ta>
            <ta e="T210" id="Seg_8403" s="T209">two.[NOM.SG]</ta>
            <ta e="T211" id="Seg_8404" s="T210">from.there</ta>
            <ta e="T214" id="Seg_8405" s="T213">bring-PST.[3SG]</ta>
            <ta e="T215" id="Seg_8406" s="T214">and</ta>
            <ta e="T216" id="Seg_8407" s="T215">what.[NOM.SG]</ta>
            <ta e="T217" id="Seg_8408" s="T216">send-PRS-3PL</ta>
            <ta e="T218" id="Seg_8409" s="T217">human.[NOM.SG]</ta>
            <ta e="T219" id="Seg_8410" s="T218">NEG</ta>
            <ta e="T220" id="Seg_8411" s="T219">know-3SG.O</ta>
            <ta e="T221" id="Seg_8412" s="T220">and</ta>
            <ta e="T222" id="Seg_8413" s="T221">you.PL.NOM</ta>
            <ta e="T226" id="Seg_8414" s="T225">one.[NOM.SG]</ta>
            <ta e="T227" id="Seg_8415" s="T226">woman.[NOM.SG]</ta>
            <ta e="T228" id="Seg_8416" s="T227">Aginskoe</ta>
            <ta e="T229" id="Seg_8417" s="T228">house-LAT</ta>
            <ta e="T230" id="Seg_8418" s="T229">go-PST.[3SG]</ta>
            <ta e="T232" id="Seg_8419" s="T231">horse-3SG-INS</ta>
            <ta e="T234" id="Seg_8420" s="T233">come-PST.[3SG]</ta>
            <ta e="T235" id="Seg_8421" s="T234">house-LAT/LOC.3SG</ta>
            <ta e="T236" id="Seg_8422" s="T235">and</ta>
            <ta e="T237" id="Seg_8423" s="T236">see-PST.[3SG]</ta>
            <ta e="T238" id="Seg_8424" s="T237">tree.[NOM.SG]</ta>
            <ta e="T239" id="Seg_8425" s="T238">lie-DUR.[3SG]</ta>
            <ta e="T241" id="Seg_8426" s="T240">then</ta>
            <ta e="T242" id="Seg_8427" s="T241">say-IPFVZ.[3SG]</ta>
            <ta e="T243" id="Seg_8428" s="T242">this-GEN</ta>
            <ta e="T244" id="Seg_8429" s="T243">worm.[NOM.SG]</ta>
            <ta e="T245" id="Seg_8430" s="T244">PTCL</ta>
            <ta e="T246" id="Seg_8431" s="T245">eat-PST.[3SG]</ta>
            <ta e="T249" id="Seg_8432" s="T248">such</ta>
            <ta e="T250" id="Seg_8433" s="T249">tree-PL</ta>
            <ta e="T251" id="Seg_8434" s="T250">one.can</ta>
            <ta e="T252" id="Seg_8435" s="T251">hit-MULT-INF.LAT</ta>
            <ta e="T253" id="Seg_8436" s="T252">and</ta>
            <ta e="T254" id="Seg_8437" s="T253">tree-PL</ta>
            <ta e="T255" id="Seg_8438" s="T254">%%</ta>
            <ta e="T257" id="Seg_8439" s="T256">such.[NOM.SG]</ta>
            <ta e="T258" id="Seg_8440" s="T257">PTCL</ta>
            <ta e="T259" id="Seg_8441" s="T258">make-PST.[3SG]</ta>
            <ta e="T260" id="Seg_8442" s="T259">small.[NOM.SG]</ta>
            <ta e="T261" id="Seg_8443" s="T260">stove.[NOM.SG]</ta>
            <ta e="T262" id="Seg_8444" s="T261">one.can</ta>
            <ta e="T263" id="Seg_8445" s="T262">light-INF.LAT</ta>
            <ta e="T265" id="Seg_8446" s="T264">then</ta>
            <ta e="T266" id="Seg_8447" s="T265">Varlam.[NOM.SG]</ta>
            <ta e="T267" id="Seg_8448" s="T266">bring-PST.[3SG]</ta>
            <ta e="T268" id="Seg_8449" s="T267">%kettle.[NOM.SG]</ta>
            <ta e="T269" id="Seg_8450" s="T268">place-IMP.2SG</ta>
            <ta e="T271" id="Seg_8451" s="T270">say-IPFVZ.[3SG]</ta>
            <ta e="T272" id="Seg_8452" s="T271">this-LAT</ta>
            <ta e="T273" id="Seg_8453" s="T272">this.[NOM.SG]</ta>
            <ta e="T274" id="Seg_8454" s="T273">go-PST.[3SG]</ta>
            <ta e="T275" id="Seg_8455" s="T274">PTCL</ta>
            <ta e="T276" id="Seg_8456" s="T275">pipe-LAT/LOC.3SG</ta>
            <ta e="T277" id="Seg_8457" s="T276">water.[NOM.SG]</ta>
            <ta e="T278" id="Seg_8458" s="T277">put-PST.[3SG]</ta>
            <ta e="T279" id="Seg_8459" s="T278">put-PST.[3SG]</ta>
            <ta e="T280" id="Seg_8460" s="T279">pour-PST.[3SG]</ta>
            <ta e="T281" id="Seg_8461" s="T280">pour-PST.[3SG]</ta>
            <ta e="T282" id="Seg_8462" s="T281">and</ta>
            <ta e="T283" id="Seg_8463" s="T282">there</ta>
            <ta e="T285" id="Seg_8464" s="T284">coal.[NOM.SG]</ta>
            <ta e="T286" id="Seg_8465" s="T285">put-PST.[3SG]</ta>
            <ta e="T287" id="Seg_8466" s="T286">come-PST.[3SG]</ta>
            <ta e="T288" id="Seg_8467" s="T287">house-LAT</ta>
            <ta e="T289" id="Seg_8468" s="T288">and</ta>
            <ta e="T290" id="Seg_8469" s="T289">say-IPFVZ.[3SG]</ta>
            <ta e="T291" id="Seg_8470" s="T290">samovar.[NOM.SG]</ta>
            <ta e="T292" id="Seg_8471" s="T291">flow-DUR.[3SG]</ta>
            <ta e="T293" id="Seg_8472" s="T292">PTCL</ta>
            <ta e="T294" id="Seg_8473" s="T293">%kettle.[NOM.SG]</ta>
            <ta e="T295" id="Seg_8474" s="T294">flow-DUR.[3SG]</ta>
            <ta e="T296" id="Seg_8475" s="T295">PTCL</ta>
            <ta e="T297" id="Seg_8476" s="T296">water.[NOM.SG]</ta>
            <ta e="T298" id="Seg_8477" s="T297">this.[NOM.SG]</ta>
            <ta e="T299" id="Seg_8478" s="T298">go-PST.[3SG]</ta>
            <ta e="T300" id="Seg_8479" s="T299">PTCL</ta>
            <ta e="T301" id="Seg_8480" s="T300">here</ta>
            <ta e="T302" id="Seg_8481" s="T301">say-IPFVZ.[3SG]</ta>
            <ta e="T303" id="Seg_8482" s="T302">water.[NOM.SG]</ta>
            <ta e="T304" id="Seg_8483" s="T303">put-EP-IMP.2SG</ta>
            <ta e="T305" id="Seg_8484" s="T304">and</ta>
            <ta e="T306" id="Seg_8485" s="T305">here</ta>
            <ta e="T307" id="Seg_8486" s="T306">coal.[NOM.SG]</ta>
            <ta e="T308" id="Seg_8487" s="T307">put-EP-IMP.2SG</ta>
            <ta e="T309" id="Seg_8488" s="T308">then</ta>
            <ta e="T310" id="Seg_8489" s="T309">this.[NOM.SG]</ta>
            <ta e="T311" id="Seg_8490" s="T310">%%</ta>
            <ta e="T312" id="Seg_8491" s="T311">make-PST.[3SG]</ta>
            <ta e="T314" id="Seg_8492" s="T313">tree.[NOM.SG]</ta>
            <ta e="T315" id="Seg_8493" s="T314">go-PST-3PL</ta>
            <ta e="T316" id="Seg_8494" s="T315">cut-INF.LAT</ta>
            <ta e="T317" id="Seg_8495" s="T316">and</ta>
            <ta e="T318" id="Seg_8496" s="T317">sweet.[NOM.SG]</ta>
            <ta e="T319" id="Seg_8497" s="T318">water</ta>
            <ta e="T320" id="Seg_8498" s="T319">bring-PST-3PL</ta>
            <ta e="T322" id="Seg_8499" s="T321">tree-LOC</ta>
            <ta e="T323" id="Seg_8500" s="T322">sweet.[NOM.SG]</ta>
            <ta e="T324" id="Seg_8501" s="T323">water.[NOM.SG]</ta>
            <ta e="T325" id="Seg_8502" s="T324">be-PST.[3SG]</ta>
            <ta e="T327" id="Seg_8503" s="T326">snow.[NOM.SG]</ta>
            <ta e="T328" id="Seg_8504" s="T327">tree.[NOM.SG]</ta>
            <ta e="T329" id="Seg_8505" s="T328">birch</ta>
            <ta e="T330" id="Seg_8506" s="T329">tree.[NOM.SG]</ta>
            <ta e="T332" id="Seg_8507" s="T331">larch.[NOM.SG]</ta>
            <ta e="T333" id="Seg_8508" s="T332">tree.[NOM.SG]</ta>
            <ta e="T334" id="Seg_8509" s="T333">pine.nut.[NOM.SG]</ta>
            <ta e="T335" id="Seg_8510" s="T334">tree.[NOM.SG]</ta>
            <ta e="T340" id="Seg_8511" s="T339">tree.[NOM.SG]</ta>
            <ta e="T342" id="Seg_8512" s="T341">branch-PL</ta>
            <ta e="T343" id="Seg_8513" s="T342">very</ta>
            <ta e="T344" id="Seg_8514" s="T343">many</ta>
            <ta e="T345" id="Seg_8515" s="T344">forest-LOC</ta>
            <ta e="T346" id="Seg_8516" s="T345">go-FUT-2SG</ta>
            <ta e="T347" id="Seg_8517" s="T346">so</ta>
            <ta e="T348" id="Seg_8518" s="T347">PTCL</ta>
            <ta e="T349" id="Seg_8519" s="T348">always</ta>
            <ta e="T350" id="Seg_8520" s="T349">foot-INS</ta>
            <ta e="T351" id="Seg_8521" s="T350">stamp-FUT-2SG</ta>
            <ta e="T353" id="Seg_8522" s="T352">hello</ta>
            <ta e="T354" id="Seg_8523" s="T353">be-PRS-2SG</ta>
            <ta e="T355" id="Seg_8524" s="T354">good</ta>
            <ta e="T356" id="Seg_8525" s="T355">morning</ta>
            <ta e="T357" id="Seg_8526" s="T356">good</ta>
            <ta e="T358" id="Seg_8527" s="T357">morning-INS</ta>
            <ta e="T359" id="Seg_8528" s="T358">and</ta>
            <ta e="T361" id="Seg_8529" s="T360">day.[NOM.SG]</ta>
            <ta e="T362" id="Seg_8530" s="T361">you.DAT</ta>
            <ta e="T364" id="Seg_8531" s="T363">how</ta>
            <ta e="T365" id="Seg_8532" s="T364">sleep-PST-2SG</ta>
            <ta e="T366" id="Seg_8533" s="T365">today</ta>
            <ta e="T367" id="Seg_8534" s="T366">sauna-LOC</ta>
            <ta e="T368" id="Seg_8535" s="T367">wash-PST-2SG</ta>
            <ta e="T369" id="Seg_8536" s="T368">probably</ta>
            <ta e="T370" id="Seg_8537" s="T369">strongly</ta>
            <ta e="T371" id="Seg_8538" s="T370">sleep-PST-2SG</ta>
            <ta e="T372" id="Seg_8539" s="T371">well</ta>
            <ta e="T375" id="Seg_8540" s="T374">good</ta>
            <ta e="T376" id="Seg_8541" s="T375">wash-PST-1SG</ta>
            <ta e="T377" id="Seg_8542" s="T376">water.[NOM.SG]</ta>
            <ta e="T378" id="Seg_8543" s="T377">grab-PST.[3SG]</ta>
            <ta e="T379" id="Seg_8544" s="T378">warm.[NOM.SG]</ta>
            <ta e="T380" id="Seg_8545" s="T379">water-NOM/GEN/ACC.1SG</ta>
            <ta e="T381" id="Seg_8546" s="T380">be-PST.[3SG]</ta>
            <ta e="T382" id="Seg_8547" s="T381">and</ta>
            <ta e="T383" id="Seg_8548" s="T382">cold.[NOM.SG]</ta>
            <ta e="T384" id="Seg_8549" s="T383">warm.[NOM.SG]</ta>
            <ta e="T385" id="Seg_8550" s="T384">become-PST.[3SG]</ta>
            <ta e="T386" id="Seg_8551" s="T385">very</ta>
            <ta e="T387" id="Seg_8552" s="T386">strongly</ta>
            <ta e="T388" id="Seg_8553" s="T387">warm.[NOM.SG]</ta>
            <ta e="T390" id="Seg_8554" s="T389">I.NOM</ta>
            <ta e="T391" id="Seg_8555" s="T390">sauna-LOC</ta>
            <ta e="T392" id="Seg_8556" s="T391">wash-PST-1SG</ta>
            <ta e="T393" id="Seg_8557" s="T392">all</ta>
            <ta e="T394" id="Seg_8558" s="T393">water.[NOM.SG]</ta>
            <ta e="T395" id="Seg_8559" s="T394">pour-PST-1SG</ta>
            <ta e="T396" id="Seg_8560" s="T395">then</ta>
            <ta e="T398" id="Seg_8561" s="T396">come-PST.[3SG]</ta>
            <ta e="T405" id="Seg_8562" s="T404">then</ta>
            <ta e="T406" id="Seg_8563" s="T405">%%</ta>
            <ta e="T407" id="Seg_8564" s="T406">come-PST.[3SG]</ta>
            <ta e="T408" id="Seg_8565" s="T407">water.[NOM.SG]</ta>
            <ta e="T409" id="Seg_8566" s="T408">NEG.EX.[3SG]</ta>
            <ta e="T410" id="Seg_8567" s="T409">and</ta>
            <ta e="T411" id="Seg_8568" s="T410">I.NOM</ta>
            <ta e="T412" id="Seg_8569" s="T411">bring-FUT-1SG</ta>
            <ta e="T413" id="Seg_8570" s="T412">and</ta>
            <ta e="T414" id="Seg_8571" s="T413">wash-DRV-FUT-1SG</ta>
            <ta e="T415" id="Seg_8572" s="T414">and</ta>
            <ta e="T416" id="Seg_8573" s="T415">I.NOM</ta>
            <ta e="T986" id="Seg_8574" s="T416">go-CVB</ta>
            <ta e="T417" id="Seg_8575" s="T986">disappear-PST-1SG</ta>
            <ta e="T418" id="Seg_8576" s="T417">this.[NOM.SG]</ta>
            <ta e="T419" id="Seg_8577" s="T418">PTCL</ta>
            <ta e="T420" id="Seg_8578" s="T419">wash-DRV-PST.[3SG]</ta>
            <ta e="T421" id="Seg_8579" s="T420">and</ta>
            <ta e="T422" id="Seg_8580" s="T421">house-LAT/LOC.3SG</ta>
            <ta e="T423" id="Seg_8581" s="T422">come-PST.[3SG]</ta>
            <ta e="T425" id="Seg_8582" s="T424">I.NOM</ta>
            <ta e="T427" id="Seg_8583" s="T426">sister-NOM/GEN/ACC.1SG</ta>
            <ta e="T428" id="Seg_8584" s="T427">son-NOM/GEN.3SG</ta>
            <ta e="T429" id="Seg_8585" s="T428">go-PST.[3SG]</ta>
            <ta e="T430" id="Seg_8586" s="T429">town-LAT</ta>
            <ta e="T431" id="Seg_8587" s="T430">there</ta>
            <ta e="T432" id="Seg_8588" s="T431">machine.[NOM.SG]</ta>
            <ta e="T433" id="Seg_8589" s="T432">be-PST.[3SG]</ta>
            <ta e="T434" id="Seg_8590" s="T433">shirt-PL</ta>
            <ta e="T435" id="Seg_8591" s="T434">wash-INF.LAT</ta>
            <ta e="T436" id="Seg_8592" s="T435">then</ta>
            <ta e="T437" id="Seg_8593" s="T436">bring-PST.[3SG]</ta>
            <ta e="T438" id="Seg_8594" s="T437">house-LAT/LOC.3SG</ta>
            <ta e="T439" id="Seg_8595" s="T438">and</ta>
            <ta e="T440" id="Seg_8596" s="T439">today</ta>
            <ta e="T441" id="Seg_8597" s="T440">shirt-PL</ta>
            <ta e="T442" id="Seg_8598" s="T441">PTCL</ta>
            <ta e="T443" id="Seg_8599" s="T442">wash-PST-3PL</ta>
            <ta e="T444" id="Seg_8600" s="T443">and</ta>
            <ta e="T445" id="Seg_8601" s="T444">hang.up-PST-3PL</ta>
            <ta e="T447" id="Seg_8602" s="T446">I.NOM</ta>
            <ta e="T448" id="Seg_8603" s="T447">sister-LAT</ta>
            <ta e="T449" id="Seg_8604" s="T448">Russian</ta>
            <ta e="T450" id="Seg_8605" s="T449">mother-NOM/GEN.3SG</ta>
            <ta e="T451" id="Seg_8606" s="T450">come-PST.[3SG]</ta>
            <ta e="T452" id="Seg_8607" s="T451">%%</ta>
            <ta e="T453" id="Seg_8608" s="T452">bring-PST.[3SG]</ta>
            <ta e="T454" id="Seg_8609" s="T453">man.[NOM.SG]</ta>
            <ta e="T455" id="Seg_8610" s="T454">and</ta>
            <ta e="T456" id="Seg_8611" s="T455">woman.[NOM.SG]</ta>
            <ta e="T457" id="Seg_8612" s="T456">then</ta>
            <ta e="T458" id="Seg_8613" s="T457">mother</ta>
            <ta e="T459" id="Seg_8614" s="T458">I.NOM</ta>
            <ta e="T460" id="Seg_8615" s="T459">mother-LAT</ta>
            <ta e="T461" id="Seg_8616" s="T460">say-IPFVZ.[3SG]</ta>
            <ta e="T462" id="Seg_8617" s="T461">go-IMP.2PL</ta>
            <ta e="T463" id="Seg_8618" s="T462">berry.[NOM.SG]</ta>
            <ta e="T464" id="Seg_8619" s="T463">take-2PL</ta>
            <ta e="T466" id="Seg_8620" s="T465">take-2PL</ta>
            <ta e="T467" id="Seg_8621" s="T466">this-PL</ta>
            <ta e="T468" id="Seg_8622" s="T467">go-PST-3PL</ta>
            <ta e="T469" id="Seg_8623" s="T468">this.[NOM.SG]</ta>
            <ta e="T470" id="Seg_8624" s="T469">vodka.[NOM.SG]</ta>
            <ta e="T471" id="Seg_8625" s="T470">give-PST.[3SG]</ta>
            <ta e="T472" id="Seg_8626" s="T471">one.[NOM.SG]</ta>
            <ta e="T473" id="Seg_8627" s="T472">one.[NOM.SG]</ta>
            <ta e="T475" id="Seg_8628" s="T474">this-PL</ta>
            <ta e="T476" id="Seg_8629" s="T475">sprinkle-PST.[3SG]</ta>
            <ta e="T478" id="Seg_8630" s="T477">there</ta>
            <ta e="T479" id="Seg_8631" s="T478">berry.[NOM.SG]</ta>
            <ta e="T480" id="Seg_8632" s="T479">this-PL</ta>
            <ta e="T482" id="Seg_8633" s="T481">be-PST-3PL</ta>
            <ta e="T483" id="Seg_8634" s="T482">be-PST-3PL</ta>
            <ta e="T484" id="Seg_8635" s="T483">%%</ta>
            <ta e="T485" id="Seg_8636" s="T484">be-PST-3PL</ta>
            <ta e="T486" id="Seg_8637" s="T485">then</ta>
            <ta e="T487" id="Seg_8638" s="T486">sit-PST-3PL</ta>
            <ta e="T488" id="Seg_8639" s="T487">eat-INF.LAT</ta>
            <ta e="T489" id="Seg_8640" s="T488">and</ta>
            <ta e="T490" id="Seg_8641" s="T489">I.NOM</ta>
            <ta e="T491" id="Seg_8642" s="T490">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T493" id="Seg_8643" s="T492">say-IPFVZ.[3SG]</ta>
            <ta e="T494" id="Seg_8644" s="T493">INCH</ta>
            <ta e="T496" id="Seg_8645" s="T494">hence</ta>
            <ta e="T497" id="Seg_8646" s="T496">one.should</ta>
            <ta e="T498" id="Seg_8647" s="T497">berry.[NOM.SG]</ta>
            <ta e="T499" id="Seg_8648" s="T498">dry-INF.LAT</ta>
            <ta e="T500" id="Seg_8649" s="T499">this</ta>
            <ta e="T501" id="Seg_8650" s="T500">vodka-INS</ta>
            <ta e="T502" id="Seg_8651" s="T501">then</ta>
            <ta e="T503" id="Seg_8652" s="T502">pour-PST-3PL</ta>
            <ta e="T504" id="Seg_8653" s="T503">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T505" id="Seg_8654" s="T504">drink-PST-3PL</ta>
            <ta e="T506" id="Seg_8655" s="T505">then</ta>
            <ta e="T507" id="Seg_8656" s="T506">birchbark.[NOM.SG]</ta>
            <ta e="T508" id="Seg_8657" s="T507">collect-PST-3PL</ta>
            <ta e="T509" id="Seg_8658" s="T508">many</ta>
            <ta e="T510" id="Seg_8659" s="T509">bring-PST-3PL</ta>
            <ta e="T511" id="Seg_8660" s="T510">come-PST-3PL</ta>
            <ta e="T512" id="Seg_8661" s="T511">house-LAT/LOC.3SG</ta>
            <ta e="T513" id="Seg_8662" s="T512">there</ta>
            <ta e="T514" id="Seg_8663" s="T513">that-ACC</ta>
            <ta e="T515" id="Seg_8664" s="T514">listen-PRS.[3SG]</ta>
            <ta e="T516" id="Seg_8665" s="T515">nose-3SG-INS</ta>
            <ta e="T517" id="Seg_8666" s="T516">that-ACC</ta>
            <ta e="T518" id="Seg_8667" s="T517">listen-PRS.[3SG]</ta>
            <ta e="T519" id="Seg_8668" s="T518">nose-3SG-INS</ta>
            <ta e="T520" id="Seg_8669" s="T519">smell-PRS.[3SG]</ta>
            <ta e="T521" id="Seg_8670" s="T520">vodka-INS</ta>
            <ta e="T522" id="Seg_8671" s="T521">NEG</ta>
            <ta e="T523" id="Seg_8672" s="T522">know-PST.[3SG]</ta>
            <ta e="T525" id="Seg_8673" s="T524">I.NOM</ta>
            <ta e="T527" id="Seg_8674" s="T526">today</ta>
            <ta e="T528" id="Seg_8675" s="T527">morning</ta>
            <ta e="T529" id="Seg_8676" s="T528">get.up-PST-1SG</ta>
            <ta e="T530" id="Seg_8677" s="T529">stove.[NOM.SG]</ta>
            <ta e="T531" id="Seg_8678" s="T530">light-PST-1SG</ta>
            <ta e="T532" id="Seg_8679" s="T531">then</ta>
            <ta e="T533" id="Seg_8680" s="T532">cow-LAT</ta>
            <ta e="T534" id="Seg_8681" s="T533">grass.[NOM.SG]</ta>
            <ta e="T535" id="Seg_8682" s="T534">give-PST-1SG</ta>
            <ta e="T536" id="Seg_8683" s="T535">potato.[NOM.SG]</ta>
            <ta e="T537" id="Seg_8684" s="T536">drink-PST-1SG</ta>
            <ta e="T538" id="Seg_8685" s="T537">go-PST-1SG</ta>
            <ta e="T539" id="Seg_8686" s="T538">cow-LAT</ta>
            <ta e="T540" id="Seg_8687" s="T539">milk-PST-1SG</ta>
            <ta e="T541" id="Seg_8688" s="T540">milk-NOM/GEN/ACC.3SG</ta>
            <ta e="T542" id="Seg_8689" s="T541">pour-PST-1SG</ta>
            <ta e="T543" id="Seg_8690" s="T542">and</ta>
            <ta e="T544" id="Seg_8691" s="T543">then</ta>
            <ta e="T545" id="Seg_8692" s="T544">go-PST-1SG</ta>
            <ta e="T546" id="Seg_8693" s="T545">woman-LAT</ta>
            <ta e="T547" id="Seg_8694" s="T546">soup.[NOM.SG]</ta>
            <ta e="T550" id="Seg_8695" s="T549">soup.[NOM.SG]</ta>
            <ta e="T551" id="Seg_8696" s="T550">boil-PST-1SG</ta>
            <ta e="T552" id="Seg_8697" s="T551">then</ta>
            <ta e="T553" id="Seg_8698" s="T552">go-PST-1SG</ta>
            <ta e="T554" id="Seg_8699" s="T553">woman-LAT</ta>
            <ta e="T555" id="Seg_8700" s="T554">soup.[NOM.SG]</ta>
            <ta e="T556" id="Seg_8701" s="T555">soup.[NOM.SG]</ta>
            <ta e="T557" id="Seg_8702" s="T556">bring-PST-1SG</ta>
            <ta e="T558" id="Seg_8703" s="T557">this.[NOM.SG]</ta>
            <ta e="T559" id="Seg_8704" s="T558">PTCL</ta>
            <ta e="T561" id="Seg_8705" s="T560">go-DUR.[3SG]</ta>
            <ta e="T562" id="Seg_8706" s="T561">foot-LOC</ta>
            <ta e="T563" id="Seg_8707" s="T562">what.[NOM.SG]</ta>
            <ta e="T564" id="Seg_8708" s="T563">house-NOM/GEN/ACC.2SG</ta>
            <ta e="T565" id="Seg_8709" s="T564">wash-PST.[3SG]</ta>
            <ta e="T566" id="Seg_8710" s="T565">no</ta>
            <ta e="T567" id="Seg_8711" s="T566">child-PL</ta>
            <ta e="T568" id="Seg_8712" s="T567">wash-PST-3PL</ta>
            <ta e="T569" id="Seg_8713" s="T568">then</ta>
            <ta e="T570" id="Seg_8714" s="T569">come-PST.[3SG]</ta>
            <ta e="T571" id="Seg_8715" s="T570">here</ta>
            <ta e="T572" id="Seg_8716" s="T571">bed-LAT</ta>
            <ta e="T573" id="Seg_8717" s="T572">PTCL</ta>
            <ta e="T574" id="Seg_8718" s="T573">fall-MOM-PST.[3SG]</ta>
            <ta e="T575" id="Seg_8719" s="T574">I.NOM</ta>
            <ta e="T576" id="Seg_8720" s="T575">say-IPFVZ-1SG</ta>
            <ta e="T577" id="Seg_8721" s="T576">NEG.AUX-IMP.2SG</ta>
            <ta e="T578" id="Seg_8722" s="T577">fall-CNG</ta>
            <ta e="T581" id="Seg_8723" s="T580">and</ta>
            <ta e="T582" id="Seg_8724" s="T581">soup-NOM/GEN/ACC.3SG</ta>
            <ta e="T583" id="Seg_8725" s="T582">PTCL</ta>
            <ta e="T584" id="Seg_8726" s="T583">%%-MOM-PST.[3SG]</ta>
            <ta e="T585" id="Seg_8727" s="T584">soon</ta>
            <ta e="T586" id="Seg_8728" s="T585">NEG</ta>
            <ta e="T587" id="Seg_8729" s="T586">see-FUT-3SG</ta>
            <ta e="T589" id="Seg_8730" s="T588">then</ta>
            <ta e="T590" id="Seg_8731" s="T589">this</ta>
            <ta e="T591" id="Seg_8732" s="T590">woman-ACC.3SG</ta>
            <ta e="T593" id="Seg_8733" s="T592">go-PST-1SG</ta>
            <ta e="T594" id="Seg_8734" s="T593">Anisya-LAT</ta>
            <ta e="T595" id="Seg_8735" s="T594">long.time</ta>
            <ta e="T596" id="Seg_8736" s="T595">NEG</ta>
            <ta e="T597" id="Seg_8737" s="T596">go-PST-1SG</ta>
            <ta e="T598" id="Seg_8738" s="T597">come-PST-1SG</ta>
            <ta e="T599" id="Seg_8739" s="T598">there</ta>
            <ta e="T600" id="Seg_8740" s="T599">this-LAT</ta>
            <ta e="T601" id="Seg_8741" s="T600">this-PL</ta>
            <ta e="T602" id="Seg_8742" s="T601">PTCL</ta>
            <ta e="T603" id="Seg_8743" s="T602">there</ta>
            <ta e="T604" id="Seg_8744" s="T603">son-3SG-COM</ta>
            <ta e="T605" id="Seg_8745" s="T604">scold-DES-DUR.[3SG]</ta>
            <ta e="T607" id="Seg_8746" s="T606">then</ta>
            <ta e="T608" id="Seg_8747" s="T607">this</ta>
            <ta e="T609" id="Seg_8748" s="T608">Anisya.[NOM.SG]</ta>
            <ta e="T610" id="Seg_8749" s="T609">PTCL</ta>
            <ta e="T611" id="Seg_8750" s="T610">scold-DES-PST.[3SG]</ta>
            <ta e="T613" id="Seg_8751" s="T612">son-NOM/GEN.3SG</ta>
            <ta e="T614" id="Seg_8752" s="T613">and</ta>
            <ta e="T615" id="Seg_8753" s="T614">son-NOM/GEN.3SG</ta>
            <ta e="T616" id="Seg_8754" s="T615">PTCL</ta>
            <ta e="T617" id="Seg_8755" s="T616">this-LAT</ta>
            <ta e="T618" id="Seg_8756" s="T617">what=INDEF</ta>
            <ta e="T619" id="Seg_8757" s="T618">speak-FUT-3SG</ta>
            <ta e="T620" id="Seg_8758" s="T619">I.NOM</ta>
            <ta e="T621" id="Seg_8759" s="T620">say-IPFVZ-1SG</ta>
            <ta e="T622" id="Seg_8760" s="T621">NEG.AUX-IMP.2SG</ta>
            <ta e="T623" id="Seg_8761" s="T622">shout-EP-CNG</ta>
            <ta e="T625" id="Seg_8762" s="T624">mother-GEN</ta>
            <ta e="T626" id="Seg_8763" s="T625">what.[NOM.SG]=INDEF</ta>
            <ta e="T627" id="Seg_8764" s="T626">NEG</ta>
            <ta e="T628" id="Seg_8765" s="T627">say-EP-CNG</ta>
            <ta e="T629" id="Seg_8766" s="T628">and</ta>
            <ta e="T630" id="Seg_8767" s="T629">this.[NOM.SG]</ta>
            <ta e="T631" id="Seg_8768" s="T630">always</ta>
            <ta e="T632" id="Seg_8769" s="T631">shout-PRS.[3SG]</ta>
            <ta e="T633" id="Seg_8770" s="T632">and</ta>
            <ta e="T634" id="Seg_8771" s="T633">mother-NOM/GEN.3SG</ta>
            <ta e="T635" id="Seg_8772" s="T634">PTCL</ta>
            <ta e="T636" id="Seg_8773" s="T635">INCH</ta>
            <ta e="T637" id="Seg_8774" s="T636">cry-INF.LAT</ta>
            <ta e="T638" id="Seg_8775" s="T637">see-PRS-2SG</ta>
            <ta e="T639" id="Seg_8776" s="T638">how</ta>
            <ta e="T640" id="Seg_8777" s="T639">this-PL</ta>
            <ta e="T641" id="Seg_8778" s="T640">bread-INS</ta>
            <ta e="T642" id="Seg_8779" s="T641">I.ACC</ta>
            <ta e="T643" id="Seg_8780" s="T642">feed-FUT-3PL</ta>
            <ta e="T644" id="Seg_8781" s="T643">I.NOM</ta>
            <ta e="T645" id="Seg_8782" s="T644">bread.[NOM.SG]</ta>
            <ta e="T646" id="Seg_8783" s="T645">eat-PRS.[3SG]</ta>
            <ta e="T647" id="Seg_8784" s="T646">and</ta>
            <ta e="T648" id="Seg_8785" s="T647">more</ta>
            <ta e="T649" id="Seg_8786" s="T648">scold-DES-DUR.[3SG]</ta>
            <ta e="T650" id="Seg_8787" s="T649">then</ta>
            <ta e="T652" id="Seg_8788" s="T651">%%</ta>
            <ta e="T653" id="Seg_8789" s="T652">sit-PST-1PL</ta>
            <ta e="T654" id="Seg_8790" s="T653">then</ta>
            <ta e="T655" id="Seg_8791" s="T654">Elya.[NOM.SG]</ta>
            <ta e="T656" id="Seg_8792" s="T655">come-PST.[3SG]</ta>
            <ta e="T657" id="Seg_8793" s="T656">hello</ta>
            <ta e="T658" id="Seg_8794" s="T657">be-PRS-2SG</ta>
            <ta e="T659" id="Seg_8795" s="T658">hello</ta>
            <ta e="T660" id="Seg_8796" s="T659">be-PRS-2SG</ta>
            <ta e="T661" id="Seg_8797" s="T660">what.[NOM.SG]</ta>
            <ta e="T662" id="Seg_8798" s="T661">come-PST-2SG</ta>
            <ta e="T663" id="Seg_8799" s="T662">bread.[NOM.SG]</ta>
            <ta e="T664" id="Seg_8800" s="T663">look-FRQ-PST-1SG</ta>
            <ta e="T665" id="Seg_8801" s="T664">and</ta>
            <ta e="T666" id="Seg_8802" s="T665">I.NOM</ta>
            <ta e="T667" id="Seg_8803" s="T666">Anisya-LAT</ta>
            <ta e="T668" id="Seg_8804" s="T667">say-PRS-1SG</ta>
            <ta e="T669" id="Seg_8805" s="T668">give-IMP.2SG</ta>
            <ta e="T670" id="Seg_8806" s="T669">this-LAT</ta>
            <ta e="T671" id="Seg_8807" s="T670">one.[NOM.SG]</ta>
            <ta e="T672" id="Seg_8808" s="T671">bread.[NOM.SG]</ta>
            <ta e="T673" id="Seg_8809" s="T672">this.[NOM.SG]</ta>
            <ta e="T674" id="Seg_8810" s="T673">give-PST.[3SG]</ta>
            <ta e="T675" id="Seg_8811" s="T674">this-LAT</ta>
            <ta e="T676" id="Seg_8812" s="T675">how.much</ta>
            <ta e="T677" id="Seg_8813" s="T676">ask-INF.LAT</ta>
            <ta e="T678" id="Seg_8814" s="T677">and</ta>
            <ta e="T679" id="Seg_8815" s="T678">I.NOM</ta>
            <ta e="T680" id="Seg_8816" s="T679">say-IPFVZ-1SG</ta>
            <ta e="T681" id="Seg_8817" s="T680">%%</ta>
            <ta e="T682" id="Seg_8818" s="T681">NEG</ta>
            <ta e="T683" id="Seg_8819" s="T682">many</ta>
            <ta e="T684" id="Seg_8820" s="T683">like</ta>
            <ta e="T685" id="Seg_8821" s="T684">shop-ABL</ta>
            <ta e="T686" id="Seg_8822" s="T685">then</ta>
            <ta e="T687" id="Seg_8823" s="T686">there</ta>
            <ta e="T688" id="Seg_8824" s="T687">this.[NOM.SG]</ta>
            <ta e="T689" id="Seg_8825" s="T688">this-LAT</ta>
            <ta e="T690" id="Seg_8826" s="T689">give-PST.[3SG]</ta>
            <ta e="T691" id="Seg_8827" s="T690">four.[NOM.SG]</ta>
            <ta e="T692" id="Seg_8828" s="T691">ten.[NOM.SG]</ta>
            <ta e="T693" id="Seg_8829" s="T692">copeck-NOM/GEN/ACC.3PL</ta>
            <ta e="T695" id="Seg_8830" s="T694">then</ta>
            <ta e="T696" id="Seg_8831" s="T695">I.NOM</ta>
            <ta e="T697" id="Seg_8832" s="T696">come-PST-1SG</ta>
            <ta e="T698" id="Seg_8833" s="T697">cow-NOM/GEN/ACC.1SG</ta>
            <ta e="T699" id="Seg_8834" s="T698">drink-%%-PST-1SG</ta>
            <ta e="T700" id="Seg_8835" s="T699">calf.[NOM.SG]</ta>
            <ta e="T701" id="Seg_8836" s="T700">drink-TR-PST-1SG</ta>
            <ta e="T702" id="Seg_8837" s="T701">grass.[NOM.SG]</ta>
            <ta e="T703" id="Seg_8838" s="T702">give-PST-1SG</ta>
            <ta e="T704" id="Seg_8839" s="T703">then</ta>
            <ta e="T705" id="Seg_8840" s="T704">water-VBLZ-CVB</ta>
            <ta e="T706" id="Seg_8841" s="T705">go-PST-1SG</ta>
            <ta e="T707" id="Seg_8842" s="T706">and</ta>
            <ta e="T709" id="Seg_8843" s="T708">come-PST-1SG</ta>
            <ta e="T710" id="Seg_8844" s="T709">house-LAT</ta>
            <ta e="T711" id="Seg_8845" s="T710">foot-NOM/GEN/ACC.1SG</ta>
            <ta e="T712" id="Seg_8846" s="T711">freeze-RES-PST.[3SG]</ta>
            <ta e="T713" id="Seg_8847" s="T712">stove-LAT</ta>
            <ta e="T716" id="Seg_8848" s="T715">climb-PST-1SG</ta>
            <ta e="T717" id="Seg_8849" s="T716">stove-LAT</ta>
            <ta e="T719" id="Seg_8850" s="T718">climb-PST-1SG</ta>
            <ta e="T720" id="Seg_8851" s="T719">then</ta>
            <ta e="T721" id="Seg_8852" s="T720">foot-NOM/GEN/ACC.1SG</ta>
            <ta e="T723" id="Seg_8853" s="T722">%%</ta>
            <ta e="T724" id="Seg_8854" s="T723">this-PL</ta>
            <ta e="T726" id="Seg_8855" s="T725">come-PST-3PL</ta>
            <ta e="T727" id="Seg_8856" s="T726">and</ta>
            <ta e="T728" id="Seg_8857" s="T727">speak-INF.LAT</ta>
            <ta e="T730" id="Seg_8858" s="T729">then</ta>
            <ta e="T731" id="Seg_8859" s="T730">speak-INF.LAT</ta>
            <ta e="T732" id="Seg_8860" s="T731">one.should</ta>
            <ta e="T733" id="Seg_8861" s="T732">and</ta>
            <ta e="T734" id="Seg_8862" s="T733">there</ta>
            <ta e="T735" id="Seg_8863" s="T734">machine.[NOM.SG]</ta>
            <ta e="T736" id="Seg_8864" s="T735">PTCL</ta>
            <ta e="T737" id="Seg_8865" s="T736">shout-DUR.[3SG]</ta>
            <ta e="T738" id="Seg_8866" s="T737">NEG</ta>
            <ta e="T739" id="Seg_8867" s="T738">give-PRS.[3SG]</ta>
            <ta e="T740" id="Seg_8868" s="T739">speak-INF.LAT</ta>
            <ta e="T741" id="Seg_8869" s="T740">we.NOM</ta>
            <ta e="T742" id="Seg_8870" s="T741">throw-MOM-PST-1PL</ta>
            <ta e="T743" id="Seg_8871" s="T742">and</ta>
            <ta e="T744" id="Seg_8872" s="T743">this-PL</ta>
            <ta e="T987" id="Seg_8873" s="T744">go-CVB</ta>
            <ta e="T745" id="Seg_8874" s="T987">disappear-PST-3PL</ta>
            <ta e="T746" id="Seg_8875" s="T745">NEG</ta>
            <ta e="T747" id="Seg_8876" s="T746">speak-PST-1PL</ta>
            <ta e="T748" id="Seg_8877" s="T747">and</ta>
            <ta e="T749" id="Seg_8878" s="T748">now</ta>
            <ta e="T750" id="Seg_8879" s="T749">come-PST-3PL</ta>
            <ta e="T751" id="Seg_8880" s="T750">again</ta>
            <ta e="T753" id="Seg_8881" s="T752">speak-INF.LAT</ta>
            <ta e="T755" id="Seg_8882" s="T754">come-PST-3PL</ta>
            <ta e="T756" id="Seg_8883" s="T755">and</ta>
            <ta e="T757" id="Seg_8884" s="T756">I.NOM</ta>
            <ta e="T758" id="Seg_8885" s="T757">say-IPFVZ-1SG</ta>
            <ta e="T759" id="Seg_8886" s="T758">where</ta>
            <ta e="T760" id="Seg_8887" s="T759">be-PST-2PL</ta>
            <ta e="T761" id="Seg_8888" s="T760">well</ta>
            <ta e="T762" id="Seg_8889" s="T761">tree.[NOM.SG]</ta>
            <ta e="T763" id="Seg_8890" s="T762">take.picture-PST-1PL</ta>
            <ta e="T764" id="Seg_8891" s="T763">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T765" id="Seg_8892" s="T764">house.[NOM.SG]</ta>
            <ta e="T766" id="Seg_8893" s="T765">take.picture-PST-1PL</ta>
            <ta e="T767" id="Seg_8894" s="T766">and</ta>
            <ta e="T768" id="Seg_8895" s="T767">grandmother-GEN</ta>
            <ta e="T769" id="Seg_8896" s="T768">house-LAT</ta>
            <ta e="T771" id="Seg_8897" s="T770">take.picture-PST-1PL</ta>
            <ta e="T772" id="Seg_8898" s="T771">and</ta>
            <ta e="T773" id="Seg_8899" s="T772">here</ta>
            <ta e="T774" id="Seg_8900" s="T773">come-PST-1PL</ta>
            <ta e="T776" id="Seg_8901" s="T775">then</ta>
            <ta e="T777" id="Seg_8902" s="T776">mother-NOM/GEN.3SG</ta>
            <ta e="T778" id="Seg_8903" s="T777">come-PST.[3SG]</ta>
            <ta e="T779" id="Seg_8904" s="T778">hospital-LOC</ta>
            <ta e="T780" id="Seg_8905" s="T779">INCH</ta>
            <ta e="T781" id="Seg_8906" s="T780">child-ACC.PL</ta>
            <ta e="T783" id="Seg_8907" s="T782">what.[NOM.SG]=INDEF</ta>
            <ta e="T784" id="Seg_8908" s="T783">NEG</ta>
            <ta e="T785" id="Seg_8909" s="T784">make-PRS-3PL</ta>
            <ta e="T786" id="Seg_8910" s="T785">take.it</ta>
            <ta e="T787" id="Seg_8911" s="T786">tree-PL</ta>
            <ta e="T788" id="Seg_8912" s="T787">put-EP-IMP.2SG</ta>
            <ta e="T789" id="Seg_8913" s="T788">fence-LOC</ta>
            <ta e="T790" id="Seg_8914" s="T789">otherwise</ta>
            <ta e="T791" id="Seg_8915" s="T790">father-NOM/GEN/ACC.2SG</ta>
            <ta e="T792" id="Seg_8916" s="T791">come-FUT-3SG</ta>
            <ta e="T793" id="Seg_8917" s="T792">what.[NOM.SG]=INDEF</ta>
            <ta e="T794" id="Seg_8918" s="T793">NEG</ta>
            <ta e="T795" id="Seg_8919" s="T794">give-FUT-3SG</ta>
            <ta e="T796" id="Seg_8920" s="T795">this.[NOM.SG]</ta>
            <ta e="T797" id="Seg_8921" s="T796">go-PST.[3SG]</ta>
            <ta e="T798" id="Seg_8922" s="T797">and</ta>
            <ta e="T799" id="Seg_8923" s="T798">INCH</ta>
            <ta e="T800" id="Seg_8924" s="T799">put-INF.LAT</ta>
            <ta e="T801" id="Seg_8925" s="T800">then</ta>
            <ta e="T802" id="Seg_8926" s="T801">come-PST.[3SG]</ta>
            <ta e="T803" id="Seg_8927" s="T802">sauna-LOC</ta>
            <ta e="T804" id="Seg_8928" s="T803">wash-DRV-PST-3PL</ta>
            <ta e="T805" id="Seg_8929" s="T804">then</ta>
            <ta e="T806" id="Seg_8930" s="T805">I.LAT</ta>
            <ta e="T807" id="Seg_8931" s="T806">say-IPFVZ-3PL</ta>
            <ta e="T808" id="Seg_8932" s="T807">go-EP-IMP.2SG</ta>
            <ta e="T809" id="Seg_8933" s="T808">sauna-LAT</ta>
            <ta e="T810" id="Seg_8934" s="T809">I.NOM</ta>
            <ta e="T811" id="Seg_8935" s="T810">go-PST-1SG</ta>
            <ta e="T812" id="Seg_8936" s="T811">wash-DRV-INF.LAT</ta>
            <ta e="T813" id="Seg_8937" s="T812">then</ta>
            <ta e="T814" id="Seg_8938" s="T813">Elya.[NOM.SG]</ta>
            <ta e="T815" id="Seg_8939" s="T814">come-PST.[3SG]</ta>
            <ta e="T816" id="Seg_8940" s="T815">wash-DRV-INF.LAT</ta>
            <ta e="T817" id="Seg_8941" s="T816">then</ta>
            <ta e="T818" id="Seg_8942" s="T817">man.[NOM.SG]</ta>
            <ta e="T819" id="Seg_8943" s="T818">come-PST.[3SG]</ta>
            <ta e="T820" id="Seg_8944" s="T819">wash-DRV-PST.[3SG]</ta>
            <ta e="T821" id="Seg_8945" s="T820">then</ta>
            <ta e="T822" id="Seg_8946" s="T821">father-NOM/GEN.3SG</ta>
            <ta e="T823" id="Seg_8947" s="T822">come-PST.[3SG]</ta>
            <ta e="T824" id="Seg_8948" s="T823">bring-PST.[3SG]</ta>
            <ta e="T825" id="Seg_8949" s="T824">hose-NOM/GEN/ACC.3PL</ta>
            <ta e="T826" id="Seg_8950" s="T825">pants-PL</ta>
            <ta e="T827" id="Seg_8951" s="T826">bring-PST.[3SG]</ta>
            <ta e="T829" id="Seg_8952" s="T828">then</ta>
            <ta e="T830" id="Seg_8953" s="T829">come-PST.[3SG]</ta>
            <ta e="T831" id="Seg_8954" s="T830">vodka.[NOM.SG]</ta>
            <ta e="T832" id="Seg_8955" s="T831">bring-PST.[3SG]</ta>
            <ta e="T833" id="Seg_8956" s="T832">one.[NOM.SG]</ta>
            <ta e="T834" id="Seg_8957" s="T833">man.[NOM.SG]</ta>
            <ta e="T835" id="Seg_8958" s="T834">this-ACC</ta>
            <ta e="T836" id="Seg_8959" s="T835">bring-PST.[3SG]</ta>
            <ta e="T837" id="Seg_8960" s="T836">horse-3SG-INS</ta>
            <ta e="T838" id="Seg_8961" s="T837">then</ta>
            <ta e="T839" id="Seg_8962" s="T838">this.[NOM.SG]</ta>
            <ta e="T840" id="Seg_8963" s="T839">this-LAT</ta>
            <ta e="T841" id="Seg_8964" s="T840">vodka.[NOM.SG]</ta>
            <ta e="T842" id="Seg_8965" s="T841">pour-PST.[3SG]</ta>
            <ta e="T843" id="Seg_8966" s="T842">%%</ta>
            <ta e="T844" id="Seg_8967" s="T843">eat-PST.[3SG]</ta>
            <ta e="T846" id="Seg_8968" s="T845">go-PST.[3SG]</ta>
            <ta e="T847" id="Seg_8969" s="T846">house-LAT/LOC.3SG</ta>
            <ta e="T849" id="Seg_8970" s="T848">this.[NOM.SG]</ta>
            <ta e="T850" id="Seg_8971" s="T849">woman.[NOM.SG]</ta>
            <ta e="T851" id="Seg_8972" s="T850">very</ta>
            <ta e="T852" id="Seg_8973" s="T851">angry.[NOM.SG]</ta>
            <ta e="T853" id="Seg_8974" s="T852">tree.[NOM.SG]</ta>
            <ta e="T854" id="Seg_8975" s="T853">take-PST.[3SG]</ta>
            <ta e="T855" id="Seg_8976" s="T854">and</ta>
            <ta e="T856" id="Seg_8977" s="T855">calf-ACC</ta>
            <ta e="T858" id="Seg_8978" s="T857">tree-INS</ta>
            <ta e="T859" id="Seg_8979" s="T858">hit-MULT-PST.[3SG]</ta>
            <ta e="T860" id="Seg_8980" s="T859">eye-3SG-LAT</ta>
            <ta e="T861" id="Seg_8981" s="T860">%tear-NOM/GEN/ACC.3SG</ta>
            <ta e="T862" id="Seg_8982" s="T861">PTCL</ta>
            <ta e="T863" id="Seg_8983" s="T862">flow-DUR.[3SG]</ta>
            <ta e="T864" id="Seg_8984" s="T863">and</ta>
            <ta e="T865" id="Seg_8985" s="T864">I.NOM</ta>
            <ta e="T866" id="Seg_8986" s="T865">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T867" id="Seg_8987" s="T866">calf-NOM/GEN/ACC.1SG</ta>
            <ta e="T868" id="Seg_8988" s="T867">NEG</ta>
            <ta e="T869" id="Seg_8989" s="T868">beat-PRS-1SG</ta>
            <ta e="T870" id="Seg_8990" s="T869">hand-INS</ta>
            <ta e="T871" id="Seg_8991" s="T870">hand-INS</ta>
            <ta e="T872" id="Seg_8992" s="T871">hit-FUT-1SG</ta>
            <ta e="T873" id="Seg_8993" s="T872">%%</ta>
            <ta e="T874" id="Seg_8994" s="T873">PTCL</ta>
            <ta e="T875" id="Seg_8995" s="T874">drink-MOM-PST.[3SG]</ta>
            <ta e="T877" id="Seg_8996" s="T876">I.NOM</ta>
            <ta e="T878" id="Seg_8997" s="T877">house-LOC</ta>
            <ta e="T879" id="Seg_8998" s="T878">take.picture-PST-1SG</ta>
            <ta e="T880" id="Seg_8999" s="T879">machine-ACC</ta>
            <ta e="T882" id="Seg_9000" s="T880">and</ta>
            <ta e="T883" id="Seg_9001" s="T882">this-PL</ta>
            <ta e="T884" id="Seg_9002" s="T883">one.[NOM.SG]</ta>
            <ta e="T885" id="Seg_9003" s="T884">two.[NOM.SG]</ta>
            <ta e="T886" id="Seg_9004" s="T885">three.[NOM.SG]</ta>
            <ta e="T887" id="Seg_9005" s="T886">four.[NOM.SG]</ta>
            <ta e="T888" id="Seg_9006" s="T887">then</ta>
            <ta e="T889" id="Seg_9007" s="T888">Vanya.[NOM.SG]</ta>
            <ta e="T890" id="Seg_9008" s="T889">say-IPFVZ.[3SG]</ta>
            <ta e="T891" id="Seg_9009" s="T890">calf.[NOM.SG]</ta>
            <ta e="T892" id="Seg_9010" s="T891">one.needs</ta>
            <ta e="T893" id="Seg_9011" s="T892">and</ta>
            <ta e="T894" id="Seg_9012" s="T893">pig-PL</ta>
            <ta e="T895" id="Seg_9013" s="T894">and</ta>
            <ta e="T896" id="Seg_9014" s="T895">we.NOM</ta>
            <ta e="T897" id="Seg_9015" s="T896">PTCL</ta>
            <ta e="T898" id="Seg_9016" s="T897">laugh-PST-1PL</ta>
            <ta e="T900" id="Seg_9017" s="T899">today</ta>
            <ta e="T901" id="Seg_9018" s="T900">probably</ta>
            <ta e="T902" id="Seg_9019" s="T901">many</ta>
            <ta e="T903" id="Seg_9020" s="T902">speak-PST-1PL</ta>
            <ta e="T904" id="Seg_9021" s="T903">now</ta>
            <ta e="T905" id="Seg_9022" s="T904">one.should</ta>
            <ta e="T906" id="Seg_9023" s="T905">house-LAT/LOC.1SG</ta>
            <ta e="T907" id="Seg_9024" s="T906">go-INF.LAT</ta>
            <ta e="T908" id="Seg_9025" s="T907">eat-INF.LAT</ta>
            <ta e="T909" id="Seg_9026" s="T908">lie-INF.LAT</ta>
            <ta e="T911" id="Seg_9027" s="T910">%%</ta>
            <ta e="T913" id="Seg_9028" s="T912">smoke.[NOM.SG]</ta>
            <ta e="T914" id="Seg_9029" s="T913">PTCL</ta>
            <ta e="T915" id="Seg_9030" s="T914">outwards</ta>
            <ta e="T916" id="Seg_9031" s="T915">walk-PRS.[3SG]</ta>
            <ta e="T917" id="Seg_9032" s="T916">very</ta>
            <ta e="T918" id="Seg_9033" s="T917">cold.[NOM.SG]</ta>
            <ta e="T919" id="Seg_9034" s="T918">become-FUT-3SG</ta>
            <ta e="T920" id="Seg_9035" s="T919">today</ta>
            <ta e="T921" id="Seg_9036" s="T920">evening-LOC.ADV</ta>
            <ta e="T922" id="Seg_9037" s="T921">snow.[NOM.SG]</ta>
            <ta e="T923" id="Seg_9038" s="T922">morning-LOC.ADV</ta>
            <ta e="T924" id="Seg_9039" s="T923">get.up-PST-1SG</ta>
            <ta e="T925" id="Seg_9040" s="T924">PTCL</ta>
            <ta e="T926" id="Seg_9041" s="T925">cold.[NOM.SG]</ta>
            <ta e="T927" id="Seg_9042" s="T926">become-RES-PST.[3SG]</ta>
            <ta e="T929" id="Seg_9043" s="T928">we.LAT</ta>
            <ta e="T930" id="Seg_9044" s="T929">today</ta>
            <ta e="T931" id="Seg_9045" s="T930">one.should</ta>
            <ta e="T932" id="Seg_9046" s="T931">go-INF.LAT</ta>
            <ta e="T933" id="Seg_9047" s="T932">Tanya-LAT</ta>
            <ta e="T934" id="Seg_9048" s="T933">so.that</ta>
            <ta e="T935" id="Seg_9049" s="T934">this.[NOM.SG]</ta>
            <ta e="T936" id="Seg_9050" s="T935">bread.[NOM.SG]</ta>
            <ta e="T937" id="Seg_9051" s="T936">take-PST.[3SG]</ta>
            <ta e="T938" id="Seg_9052" s="T937">Voznesenskoe-ABL</ta>
            <ta e="T939" id="Seg_9053" s="T938">and</ta>
            <ta e="T940" id="Seg_9054" s="T939">bring-PST.[3SG]</ta>
            <ta e="T941" id="Seg_9055" s="T940">we.LAT</ta>
            <ta e="T944" id="Seg_9056" s="T943">one.should</ta>
            <ta e="T945" id="Seg_9057" s="T944">money-ACC</ta>
            <ta e="T946" id="Seg_9058" s="T945">take-INF.LAT</ta>
            <ta e="T947" id="Seg_9059" s="T946">this-LAT</ta>
            <ta e="T948" id="Seg_9060" s="T947">NEG</ta>
            <ta e="T949" id="Seg_9061" s="T948">self-LAT/LOC.3SG</ta>
            <ta e="T950" id="Seg_9062" s="T949">bring-INF.LAT</ta>
            <ta e="T951" id="Seg_9063" s="T950">from.there</ta>
            <ta e="T952" id="Seg_9064" s="T951">horse-3SG-INS</ta>
            <ta e="T953" id="Seg_9065" s="T952">go-FUT-3SG</ta>
            <ta e="T954" id="Seg_9066" s="T953">and</ta>
            <ta e="T955" id="Seg_9067" s="T954">bring-FUT-3SG</ta>
            <ta e="T957" id="Seg_9068" s="T956">NEG</ta>
            <ta e="T958" id="Seg_9069" s="T957">know-1SG</ta>
            <ta e="T959" id="Seg_9070" s="T958">take-FUT-3SG</ta>
            <ta e="T960" id="Seg_9071" s="T959">or</ta>
            <ta e="T961" id="Seg_9072" s="T960">NEG</ta>
            <ta e="T962" id="Seg_9073" s="T961">one.should</ta>
            <ta e="T963" id="Seg_9074" s="T962">go-INF.LAT</ta>
            <ta e="T964" id="Seg_9075" s="T963">this-COM</ta>
            <ta e="T965" id="Seg_9076" s="T964">speak-INF.LAT</ta>
            <ta e="T966" id="Seg_9077" s="T965">maybe</ta>
            <ta e="T967" id="Seg_9078" s="T966">take-FUT-3SG</ta>
            <ta e="T968" id="Seg_9079" s="T967">well</ta>
            <ta e="T969" id="Seg_9080" s="T968">go-EP-IMP.2SG</ta>
            <ta e="T970" id="Seg_9081" s="T969">speak-EP-IMP.2SG</ta>
            <ta e="T972" id="Seg_9082" s="T971">this.[NOM.SG]</ta>
            <ta e="T973" id="Seg_9083" s="T972">maybe</ta>
            <ta e="T974" id="Seg_9084" s="T973">go-FUT-3SG</ta>
            <ta e="T975" id="Seg_9085" s="T974">shop-LOC</ta>
            <ta e="T976" id="Seg_9086" s="T975">there</ta>
            <ta e="T978" id="Seg_9087" s="T977">stand-INF.LAT</ta>
            <ta e="T979" id="Seg_9088" s="T978">one.should</ta>
            <ta e="T980" id="Seg_9089" s="T979">bread.[NOM.SG]</ta>
            <ta e="T981" id="Seg_9090" s="T980">take-INF.LAT</ta>
            <ta e="T982" id="Seg_9091" s="T981">I.NOM</ta>
            <ta e="T983" id="Seg_9092" s="T982">NEG</ta>
            <ta e="T984" id="Seg_9093" s="T983">be-FUT-1SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_9094" s="T0">пойти-IMP.2PL</ta>
            <ta e="T2" id="Seg_9095" s="T1">баня-LAT</ta>
            <ta e="T3" id="Seg_9096" s="T2">%вместе</ta>
            <ta e="T4" id="Seg_9097" s="T3">два-COLL</ta>
            <ta e="T5" id="Seg_9098" s="T4">этот.[NOM.SG]</ta>
            <ta e="T6" id="Seg_9099" s="T5">ты.DAT</ta>
            <ta e="T7" id="Seg_9100" s="T6">спина-ACC.3SG</ta>
            <ta e="T8" id="Seg_9101" s="T7">тереть-FUT-3SG</ta>
            <ta e="T9" id="Seg_9102" s="T8">мыло-INS</ta>
            <ta e="T10" id="Seg_9103" s="T9">мочалка-INS</ta>
            <ta e="T11" id="Seg_9104" s="T10">а</ta>
            <ta e="T12" id="Seg_9105" s="T11">ты.NOM</ta>
            <ta e="T13" id="Seg_9106" s="T12">этот-LAT</ta>
            <ta e="T14" id="Seg_9107" s="T13">да</ta>
            <ta e="T16" id="Seg_9108" s="T15">стесняться-PRS-1SG</ta>
            <ta e="T17" id="Seg_9109" s="T16">не</ta>
            <ta e="T18" id="Seg_9110" s="T17">хочется</ta>
            <ta e="T19" id="Seg_9111" s="T18">пойти-INF.LAT</ta>
            <ta e="T20" id="Seg_9112" s="T19">очень</ta>
            <ta e="T21" id="Seg_9113" s="T20">стесняться-PRS-1SG</ta>
            <ta e="T22" id="Seg_9114" s="T21">русский-GEN</ta>
            <ta e="T23" id="Seg_9115" s="T22">поселение-LOC</ta>
            <ta e="T24" id="Seg_9116" s="T23">баня-LAT</ta>
            <ta e="T25" id="Seg_9117" s="T24">NEG</ta>
            <ta e="T27" id="Seg_9118" s="T26">NEG</ta>
            <ta e="T28" id="Seg_9119" s="T27">пойти-PRS-3PL</ta>
            <ta e="T29" id="Seg_9120" s="T28">%вместе</ta>
            <ta e="T30" id="Seg_9121" s="T29">женщина-PL</ta>
            <ta e="T31" id="Seg_9122" s="T30">мужчина-PL</ta>
            <ta e="T32" id="Seg_9123" s="T31">женщина-PL</ta>
            <ta e="T33" id="Seg_9124" s="T32">один.[NOM.SG]</ta>
            <ta e="T35" id="Seg_9125" s="T34">идти-3PL</ta>
            <ta e="T36" id="Seg_9126" s="T35">а</ta>
            <ta e="T37" id="Seg_9127" s="T36">мужчина-PL</ta>
            <ta e="T38" id="Seg_9128" s="T37">завтра</ta>
            <ta e="T39" id="Seg_9129" s="T38">светить-DUR-3PL</ta>
            <ta e="T40" id="Seg_9130" s="T39">опять</ta>
            <ta e="T41" id="Seg_9131" s="T40">один.[NOM.SG]</ta>
            <ta e="T42" id="Seg_9132" s="T41">пойти-IPFVZ-PRS-3PL</ta>
            <ta e="T43" id="Seg_9133" s="T42">мужчина-PL</ta>
            <ta e="T45" id="Seg_9134" s="T44">мы.NOM</ta>
            <ta e="T46" id="Seg_9135" s="T45">баня-LOC</ta>
            <ta e="T47" id="Seg_9136" s="T46">девушка-PL</ta>
            <ta e="T48" id="Seg_9137" s="T47">два-COLL</ta>
            <ta e="T49" id="Seg_9138" s="T48">пойти-IPFVZ-PRS-3PL</ta>
            <ta e="T50" id="Seg_9139" s="T49">тогда</ta>
            <ta e="T51" id="Seg_9140" s="T50">мать-NOM/GEN.3SG</ta>
            <ta e="T52" id="Seg_9141" s="T51">пойти-FUT-3SG</ta>
            <ta e="T53" id="Seg_9142" s="T52">один.[NOM.SG]</ta>
            <ta e="T54" id="Seg_9143" s="T53">девушка.[NOM.SG]</ta>
            <ta e="T55" id="Seg_9144" s="T54">один.[NOM.SG]</ta>
            <ta e="T56" id="Seg_9145" s="T55">мальчик.[NOM.SG]</ta>
            <ta e="T57" id="Seg_9146" s="T56">мыть-FUT-3SG</ta>
            <ta e="T58" id="Seg_9147" s="T57">этот-ACC.PL</ta>
            <ta e="T59" id="Seg_9148" s="T58">этот-PL</ta>
            <ta e="T61" id="Seg_9149" s="T60">прийти-PRS-3PL</ta>
            <ta e="T62" id="Seg_9150" s="T61">тогда</ta>
            <ta e="T63" id="Seg_9151" s="T62">мужчина.[NOM.SG]</ta>
            <ta e="T64" id="Seg_9152" s="T63">пойти-DUR.[3SG]</ta>
            <ta e="T65" id="Seg_9153" s="T64">а</ta>
            <ta e="T66" id="Seg_9154" s="T65">тогда</ta>
            <ta e="T67" id="Seg_9155" s="T66">я.NOM</ta>
            <ta e="T68" id="Seg_9156" s="T67">пойти-DUR-1SG</ta>
            <ta e="T69" id="Seg_9157" s="T68">мыть-DRV-PRS-1SG</ta>
            <ta e="T71" id="Seg_9158" s="T70">я.NOM</ta>
            <ta e="T72" id="Seg_9159" s="T71">тетка-NOM/GEN/ACC.1SG</ta>
            <ta e="T73" id="Seg_9160" s="T72">пойти-PST.[3SG]</ta>
            <ta e="T988" id="Seg_9161" s="T73">Агинское</ta>
            <ta e="T74" id="Seg_9162" s="T988">поселение-LAT</ta>
            <ta e="T75" id="Seg_9163" s="T74">там</ta>
            <ta e="T76" id="Seg_9164" s="T75">как</ta>
            <ta e="T77" id="Seg_9165" s="T76">раз.[NOM.SG]</ta>
            <ta e="T78" id="Seg_9166" s="T77">баня.[NOM.SG]</ta>
            <ta e="T79" id="Seg_9167" s="T78">светить-PST-3PL</ta>
            <ta e="T80" id="Seg_9168" s="T79">весь</ta>
            <ta e="T81" id="Seg_9169" s="T80">мыть-PST-3PL</ta>
            <ta e="T82" id="Seg_9170" s="T81">сказать-IPFVZ-3PL</ta>
            <ta e="T83" id="Seg_9171" s="T82">пойти-EP-IMP.2SG</ta>
            <ta e="T84" id="Seg_9172" s="T83">баня-LAT</ta>
            <ta e="T85" id="Seg_9173" s="T84">а</ta>
            <ta e="T86" id="Seg_9174" s="T85">я.NOM</ta>
            <ta e="T87" id="Seg_9175" s="T86">а</ta>
            <ta e="T88" id="Seg_9176" s="T87">этот.[NOM.SG]</ta>
            <ta e="T89" id="Seg_9177" s="T88">пойти-PST.[3SG]</ta>
            <ta e="T90" id="Seg_9178" s="T89">NEG</ta>
            <ta e="T91" id="Seg_9179" s="T90">знать-3SG.O</ta>
            <ta e="T92" id="Seg_9180" s="T91">куда</ta>
            <ta e="T93" id="Seg_9181" s="T92">вода.[NOM.SG]</ta>
            <ta e="T94" id="Seg_9182" s="T93">лить-INF.LAT</ta>
            <ta e="T95" id="Seg_9183" s="T94">печь-LAT</ta>
            <ta e="T96" id="Seg_9184" s="T95">лить-PST-1SG</ta>
            <ta e="T97" id="Seg_9185" s="T96">PTCL</ta>
            <ta e="T98" id="Seg_9186" s="T97">стена-PL</ta>
            <ta e="T99" id="Seg_9187" s="T98">вода-INS</ta>
            <ta e="T109" id="Seg_9188" s="T108">тогда</ta>
            <ta e="T110" id="Seg_9189" s="T109">мыться-PST-1SG</ta>
            <ta e="T111" id="Seg_9190" s="T110">прийти-PST-1SG</ta>
            <ta e="T112" id="Seg_9191" s="T111">что.[NOM.SG]</ta>
            <ta e="T113" id="Seg_9192" s="T112">мыться-PST-2SG</ta>
            <ta e="T114" id="Seg_9193" s="T113">мыться-PST-2SG</ta>
            <ta e="T115" id="Seg_9194" s="T114">NEG</ta>
            <ta e="T116" id="Seg_9195" s="T115">класть-PST-2SG</ta>
            <ta e="T117" id="Seg_9196" s="T116">NEG</ta>
            <ta e="T118" id="Seg_9197" s="T117">класть-PST-1SG</ta>
            <ta e="T120" id="Seg_9198" s="T119">тогда</ta>
            <ta e="T122" id="Seg_9199" s="T121">сказать-PRS-3PL</ta>
            <ta e="T123" id="Seg_9200" s="T122">пойти-EP-IMP.2SG</ta>
            <ta e="T124" id="Seg_9201" s="T123">подвал-ACC.3SG</ta>
            <ta e="T125" id="Seg_9202" s="T124">мясо.[NOM.SG]</ta>
            <ta e="T126" id="Seg_9203" s="T125">принести-IMP.2SG</ta>
            <ta e="T127" id="Seg_9204" s="T126">котлета-PL-LAT</ta>
            <ta e="T128" id="Seg_9205" s="T127">я.NOM</ta>
            <ta e="T129" id="Seg_9206" s="T128">пойти-PST-1SG</ta>
            <ta e="T131" id="Seg_9207" s="T130">стоять-PRS-1SG</ta>
            <ta e="T132" id="Seg_9208" s="T131">мясо.[NOM.SG]</ta>
            <ta e="T133" id="Seg_9209" s="T132">много</ta>
            <ta e="T134" id="Seg_9210" s="T133">а</ta>
            <ta e="T135" id="Seg_9211" s="T134">котел.[NOM.SG]</ta>
            <ta e="T136" id="Seg_9212" s="T135">NEG.EX.[3SG]</ta>
            <ta e="T137" id="Seg_9213" s="T136">тогда</ta>
            <ta e="T138" id="Seg_9214" s="T137">вернуться-PST-1SG</ta>
            <ta e="T139" id="Seg_9215" s="T138">что.[NOM.SG]</ta>
            <ta e="T140" id="Seg_9216" s="T139">мясо.[NOM.SG]</ta>
            <ta e="T141" id="Seg_9217" s="T140">NEG</ta>
            <ta e="T143" id="Seg_9218" s="T142">принести-PRS-2SG</ta>
            <ta e="T144" id="Seg_9219" s="T143">да</ta>
            <ta e="T145" id="Seg_9220" s="T144">мясо.[NOM.SG]</ta>
            <ta e="T146" id="Seg_9221" s="T145">много</ta>
            <ta e="T147" id="Seg_9222" s="T146">а</ta>
            <ta e="T148" id="Seg_9223" s="T147">котел.[NOM.SG]</ta>
            <ta e="T149" id="Seg_9224" s="T148">NEG.EX.[3SG]</ta>
            <ta e="T150" id="Seg_9225" s="T149">этот-PL</ta>
            <ta e="T151" id="Seg_9226" s="T150">INCH</ta>
            <ta e="T152" id="Seg_9227" s="T151">смеяться-INF.LAT</ta>
            <ta e="T154" id="Seg_9228" s="T153">тогда</ta>
            <ta e="T155" id="Seg_9229" s="T154">сказать-PRS-3PL</ta>
            <ta e="T156" id="Seg_9230" s="T155">пойти-EP-IMP.2SG</ta>
            <ta e="T157" id="Seg_9231" s="T156">огород-LAT</ta>
            <ta e="T158" id="Seg_9232" s="T157">принести-IMP.2SG</ta>
            <ta e="T159" id="Seg_9233" s="T158">огурец.PL-PL</ta>
            <ta e="T160" id="Seg_9234" s="T159">я.NOM</ta>
            <ta e="T161" id="Seg_9235" s="T160">пойти-PST-1SG</ta>
            <ta e="T162" id="Seg_9236" s="T161">смотреть-FRQ-PST-1SG</ta>
            <ta e="T163" id="Seg_9237" s="T162">смотреть-FRQ-PST-1SG</ta>
            <ta e="T164" id="Seg_9238" s="T163">рвать-PRS-1SG</ta>
            <ta e="T165" id="Seg_9239" s="T164">%%</ta>
            <ta e="T166" id="Seg_9240" s="T165">еще</ta>
            <ta e="T167" id="Seg_9241" s="T166">что=INDEF</ta>
            <ta e="T168" id="Seg_9242" s="T167">тогда</ta>
            <ta e="T169" id="Seg_9243" s="T168">%%</ta>
            <ta e="T170" id="Seg_9244" s="T169">два.[NOM.SG]</ta>
            <ta e="T173" id="Seg_9245" s="T172">взять-PST-1SG</ta>
            <ta e="T174" id="Seg_9246" s="T173">и</ta>
            <ta e="T175" id="Seg_9247" s="T174">прийти-PST-1SG</ta>
            <ta e="T176" id="Seg_9248" s="T175">дом-LAT</ta>
            <ta e="T177" id="Seg_9249" s="T176">один.[NOM.SG]</ta>
            <ta e="T178" id="Seg_9250" s="T177">женщина.[NOM.SG]</ta>
            <ta e="T179" id="Seg_9251" s="T178">подвал-LAT</ta>
            <ta e="T181" id="Seg_9252" s="T180">прыгнуть-MOM-PST.[3SG]</ta>
            <ta e="T182" id="Seg_9253" s="T181">один.[NOM.SG]</ta>
            <ta e="T184" id="Seg_9254" s="T183">женщина.[NOM.SG]</ta>
            <ta e="T185" id="Seg_9255" s="T184">наружу</ta>
            <ta e="T186" id="Seg_9256" s="T185">%едва</ta>
            <ta e="T188" id="Seg_9257" s="T187">я.ACC</ta>
            <ta e="T189" id="Seg_9258" s="T188">NEG</ta>
            <ta e="T190" id="Seg_9259" s="T189">топнуть-PST.[3SG]</ta>
            <ta e="T192" id="Seg_9260" s="T191">тогда</ta>
            <ta e="T194" id="Seg_9261" s="T193">этот</ta>
            <ta e="T195" id="Seg_9262" s="T194">женщина-GEN.PL</ta>
            <ta e="T196" id="Seg_9263" s="T195">отец</ta>
            <ta e="T197" id="Seg_9264" s="T196">отец-NOM/GEN.3SG</ta>
            <ta e="T198" id="Seg_9265" s="T197">сказать-IPFVZ.[3SG]</ta>
            <ta e="T199" id="Seg_9266" s="T198">куда</ta>
            <ta e="T200" id="Seg_9267" s="T199">вы.NOM</ta>
            <ta e="T201" id="Seg_9268" s="T200">бежать-RES-PST-2PL</ta>
            <ta e="T202" id="Seg_9269" s="T201">принести-IMP.2PL</ta>
            <ta e="T203" id="Seg_9270" s="T202">а</ta>
            <ta e="T204" id="Seg_9271" s="T203">лить-IMP.2PL</ta>
            <ta e="T205" id="Seg_9272" s="T204">суп.[NOM.SG]</ta>
            <ta e="T206" id="Seg_9273" s="T205">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T207" id="Seg_9274" s="T206">сказать-IPFVZ.[3SG]</ta>
            <ta e="T208" id="Seg_9275" s="T207">видеть-PRS-2SG</ta>
            <ta e="T209" id="Seg_9276" s="T208">этот.[NOM.SG]</ta>
            <ta e="T210" id="Seg_9277" s="T209">два.[NOM.SG]</ta>
            <ta e="T211" id="Seg_9278" s="T210">оттуда</ta>
            <ta e="T214" id="Seg_9279" s="T213">принести-PST.[3SG]</ta>
            <ta e="T215" id="Seg_9280" s="T214">а</ta>
            <ta e="T216" id="Seg_9281" s="T215">что.[NOM.SG]</ta>
            <ta e="T217" id="Seg_9282" s="T216">послать-PRS-3PL</ta>
            <ta e="T218" id="Seg_9283" s="T217">человек.[NOM.SG]</ta>
            <ta e="T219" id="Seg_9284" s="T218">NEG</ta>
            <ta e="T220" id="Seg_9285" s="T219">знать-3SG.O</ta>
            <ta e="T221" id="Seg_9286" s="T220">а</ta>
            <ta e="T222" id="Seg_9287" s="T221">вы.NOM</ta>
            <ta e="T226" id="Seg_9288" s="T225">один.[NOM.SG]</ta>
            <ta e="T227" id="Seg_9289" s="T226">женщина.[NOM.SG]</ta>
            <ta e="T228" id="Seg_9290" s="T227">Агинское</ta>
            <ta e="T229" id="Seg_9291" s="T228">дом-LAT</ta>
            <ta e="T230" id="Seg_9292" s="T229">идти-PST.[3SG]</ta>
            <ta e="T232" id="Seg_9293" s="T231">лошадь-3SG-INS</ta>
            <ta e="T234" id="Seg_9294" s="T233">прийти-PST.[3SG]</ta>
            <ta e="T235" id="Seg_9295" s="T234">дом-LAT/LOC.3SG</ta>
            <ta e="T236" id="Seg_9296" s="T235">и</ta>
            <ta e="T237" id="Seg_9297" s="T236">видеть-PST.[3SG]</ta>
            <ta e="T238" id="Seg_9298" s="T237">дерево.[NOM.SG]</ta>
            <ta e="T239" id="Seg_9299" s="T238">лежать-DUR.[3SG]</ta>
            <ta e="T241" id="Seg_9300" s="T240">тогда</ta>
            <ta e="T242" id="Seg_9301" s="T241">сказать-IPFVZ.[3SG]</ta>
            <ta e="T243" id="Seg_9302" s="T242">этот-GEN</ta>
            <ta e="T244" id="Seg_9303" s="T243">червь.[NOM.SG]</ta>
            <ta e="T245" id="Seg_9304" s="T244">PTCL</ta>
            <ta e="T246" id="Seg_9305" s="T245">съесть-PST.[3SG]</ta>
            <ta e="T249" id="Seg_9306" s="T248">такой</ta>
            <ta e="T250" id="Seg_9307" s="T249">дерево-PL</ta>
            <ta e="T251" id="Seg_9308" s="T250">можно</ta>
            <ta e="T252" id="Seg_9309" s="T251">ударить-MULT-INF.LAT</ta>
            <ta e="T253" id="Seg_9310" s="T252">и</ta>
            <ta e="T254" id="Seg_9311" s="T253">дерево-PL</ta>
            <ta e="T255" id="Seg_9312" s="T254">%%</ta>
            <ta e="T257" id="Seg_9313" s="T256">такой.[NOM.SG]</ta>
            <ta e="T258" id="Seg_9314" s="T257">PTCL</ta>
            <ta e="T259" id="Seg_9315" s="T258">делать-PST.[3SG]</ta>
            <ta e="T260" id="Seg_9316" s="T259">маленький.[NOM.SG]</ta>
            <ta e="T261" id="Seg_9317" s="T260">печь.[NOM.SG]</ta>
            <ta e="T262" id="Seg_9318" s="T261">можно</ta>
            <ta e="T263" id="Seg_9319" s="T262">светить-INF.LAT</ta>
            <ta e="T265" id="Seg_9320" s="T264">тогда</ta>
            <ta e="T266" id="Seg_9321" s="T265">Варлам.[NOM.SG]</ta>
            <ta e="T267" id="Seg_9322" s="T266">принести-PST.[3SG]</ta>
            <ta e="T268" id="Seg_9323" s="T267">%самовар.[NOM.SG]</ta>
            <ta e="T269" id="Seg_9324" s="T268">поставить-IMP.2SG</ta>
            <ta e="T271" id="Seg_9325" s="T270">сказать-IPFVZ.[3SG]</ta>
            <ta e="T272" id="Seg_9326" s="T271">этот-LAT</ta>
            <ta e="T273" id="Seg_9327" s="T272">этот.[NOM.SG]</ta>
            <ta e="T274" id="Seg_9328" s="T273">пойти-PST.[3SG]</ta>
            <ta e="T275" id="Seg_9329" s="T274">PTCL</ta>
            <ta e="T276" id="Seg_9330" s="T275">труба-LAT/LOC.3SG</ta>
            <ta e="T277" id="Seg_9331" s="T276">вода.[NOM.SG]</ta>
            <ta e="T278" id="Seg_9332" s="T277">класть-PST.[3SG]</ta>
            <ta e="T279" id="Seg_9333" s="T278">класть-PST.[3SG]</ta>
            <ta e="T280" id="Seg_9334" s="T279">лить-PST.[3SG]</ta>
            <ta e="T281" id="Seg_9335" s="T280">лить-PST.[3SG]</ta>
            <ta e="T282" id="Seg_9336" s="T281">и</ta>
            <ta e="T283" id="Seg_9337" s="T282">там</ta>
            <ta e="T285" id="Seg_9338" s="T284">уголь.[NOM.SG]</ta>
            <ta e="T286" id="Seg_9339" s="T285">класть-PST.[3SG]</ta>
            <ta e="T287" id="Seg_9340" s="T286">прийти-PST.[3SG]</ta>
            <ta e="T288" id="Seg_9341" s="T287">дом-LAT</ta>
            <ta e="T289" id="Seg_9342" s="T288">и</ta>
            <ta e="T290" id="Seg_9343" s="T289">сказать-IPFVZ.[3SG]</ta>
            <ta e="T291" id="Seg_9344" s="T290">самовар.[NOM.SG]</ta>
            <ta e="T292" id="Seg_9345" s="T291">течь-DUR.[3SG]</ta>
            <ta e="T293" id="Seg_9346" s="T292">PTCL</ta>
            <ta e="T294" id="Seg_9347" s="T293">%самовар.[NOM.SG]</ta>
            <ta e="T295" id="Seg_9348" s="T294">течь-DUR.[3SG]</ta>
            <ta e="T296" id="Seg_9349" s="T295">PTCL</ta>
            <ta e="T297" id="Seg_9350" s="T296">вода.[NOM.SG]</ta>
            <ta e="T298" id="Seg_9351" s="T297">этот.[NOM.SG]</ta>
            <ta e="T299" id="Seg_9352" s="T298">пойти-PST.[3SG]</ta>
            <ta e="T300" id="Seg_9353" s="T299">PTCL</ta>
            <ta e="T301" id="Seg_9354" s="T300">здесь</ta>
            <ta e="T302" id="Seg_9355" s="T301">сказать-IPFVZ.[3SG]</ta>
            <ta e="T303" id="Seg_9356" s="T302">вода.[NOM.SG]</ta>
            <ta e="T304" id="Seg_9357" s="T303">класть-EP-IMP.2SG</ta>
            <ta e="T305" id="Seg_9358" s="T304">а</ta>
            <ta e="T306" id="Seg_9359" s="T305">здесь</ta>
            <ta e="T307" id="Seg_9360" s="T306">уголь.[NOM.SG]</ta>
            <ta e="T308" id="Seg_9361" s="T307">класть-EP-IMP.2SG</ta>
            <ta e="T309" id="Seg_9362" s="T308">тогда</ta>
            <ta e="T310" id="Seg_9363" s="T309">этот.[NOM.SG]</ta>
            <ta e="T311" id="Seg_9364" s="T310">%%</ta>
            <ta e="T312" id="Seg_9365" s="T311">делать-PST.[3SG]</ta>
            <ta e="T314" id="Seg_9366" s="T313">дерево.[NOM.SG]</ta>
            <ta e="T315" id="Seg_9367" s="T314">пойти-PST-3PL</ta>
            <ta e="T316" id="Seg_9368" s="T315">резать-INF.LAT</ta>
            <ta e="T317" id="Seg_9369" s="T316">и</ta>
            <ta e="T318" id="Seg_9370" s="T317">сладкий.[NOM.SG]</ta>
            <ta e="T319" id="Seg_9371" s="T318">вода</ta>
            <ta e="T320" id="Seg_9372" s="T319">принести-PST-3PL</ta>
            <ta e="T322" id="Seg_9373" s="T321">дерево-LOC</ta>
            <ta e="T323" id="Seg_9374" s="T322">сладкий.[NOM.SG]</ta>
            <ta e="T324" id="Seg_9375" s="T323">вода.[NOM.SG]</ta>
            <ta e="T325" id="Seg_9376" s="T324">быть-PST.[3SG]</ta>
            <ta e="T327" id="Seg_9377" s="T326">снег.[NOM.SG]</ta>
            <ta e="T328" id="Seg_9378" s="T327">дерево.[NOM.SG]</ta>
            <ta e="T329" id="Seg_9379" s="T328">береза</ta>
            <ta e="T330" id="Seg_9380" s="T329">дерево.[NOM.SG]</ta>
            <ta e="T332" id="Seg_9381" s="T331">лиственница.[NOM.SG]</ta>
            <ta e="T333" id="Seg_9382" s="T332">дерево.[NOM.SG]</ta>
            <ta e="T334" id="Seg_9383" s="T333">кедровый.орех.[NOM.SG]</ta>
            <ta e="T335" id="Seg_9384" s="T334">дерево.[NOM.SG]</ta>
            <ta e="T340" id="Seg_9385" s="T339">дерево.[NOM.SG]</ta>
            <ta e="T342" id="Seg_9386" s="T341">ветка-PL</ta>
            <ta e="T343" id="Seg_9387" s="T342">очень</ta>
            <ta e="T344" id="Seg_9388" s="T343">много</ta>
            <ta e="T345" id="Seg_9389" s="T344">лес-LOC</ta>
            <ta e="T346" id="Seg_9390" s="T345">пойти-FUT-2SG</ta>
            <ta e="T347" id="Seg_9391" s="T346">так</ta>
            <ta e="T348" id="Seg_9392" s="T347">PTCL</ta>
            <ta e="T349" id="Seg_9393" s="T348">всегда</ta>
            <ta e="T350" id="Seg_9394" s="T349">нога-INS</ta>
            <ta e="T351" id="Seg_9395" s="T350">топнуть-FUT-2SG</ta>
            <ta e="T353" id="Seg_9396" s="T352">здорово</ta>
            <ta e="T354" id="Seg_9397" s="T353">быть-PRS-2SG</ta>
            <ta e="T355" id="Seg_9398" s="T354">хороший</ta>
            <ta e="T356" id="Seg_9399" s="T355">утро</ta>
            <ta e="T357" id="Seg_9400" s="T356">хороший</ta>
            <ta e="T358" id="Seg_9401" s="T357">утро-INS</ta>
            <ta e="T359" id="Seg_9402" s="T358">и</ta>
            <ta e="T361" id="Seg_9403" s="T360">день.[NOM.SG]</ta>
            <ta e="T362" id="Seg_9404" s="T361">ты.DAT</ta>
            <ta e="T364" id="Seg_9405" s="T363">как</ta>
            <ta e="T365" id="Seg_9406" s="T364">спать-PST-2SG</ta>
            <ta e="T366" id="Seg_9407" s="T365">сегодня</ta>
            <ta e="T367" id="Seg_9408" s="T366">баня-LOC</ta>
            <ta e="T368" id="Seg_9409" s="T367">мыть-PST-2SG</ta>
            <ta e="T369" id="Seg_9410" s="T368">наверное</ta>
            <ta e="T370" id="Seg_9411" s="T369">сильно</ta>
            <ta e="T371" id="Seg_9412" s="T370">спать-PST-2SG</ta>
            <ta e="T372" id="Seg_9413" s="T371">да</ta>
            <ta e="T375" id="Seg_9414" s="T374">хороший</ta>
            <ta e="T376" id="Seg_9415" s="T375">мыть-PST-1SG</ta>
            <ta e="T377" id="Seg_9416" s="T376">вода.[NOM.SG]</ta>
            <ta e="T378" id="Seg_9417" s="T377">хватать-PST.[3SG]</ta>
            <ta e="T379" id="Seg_9418" s="T378">теплый.[NOM.SG]</ta>
            <ta e="T380" id="Seg_9419" s="T379">вода-NOM/GEN/ACC.1SG</ta>
            <ta e="T381" id="Seg_9420" s="T380">быть-PST.[3SG]</ta>
            <ta e="T382" id="Seg_9421" s="T381">и</ta>
            <ta e="T383" id="Seg_9422" s="T382">холодный.[NOM.SG]</ta>
            <ta e="T384" id="Seg_9423" s="T383">теплый.[NOM.SG]</ta>
            <ta e="T385" id="Seg_9424" s="T384">стать-PST.[3SG]</ta>
            <ta e="T386" id="Seg_9425" s="T385">очень</ta>
            <ta e="T387" id="Seg_9426" s="T386">сильно</ta>
            <ta e="T388" id="Seg_9427" s="T387">теплый.[NOM.SG]</ta>
            <ta e="T390" id="Seg_9428" s="T389">я.NOM</ta>
            <ta e="T391" id="Seg_9429" s="T390">баня-LOC</ta>
            <ta e="T392" id="Seg_9430" s="T391">мыть-PST-1SG</ta>
            <ta e="T393" id="Seg_9431" s="T392">весь</ta>
            <ta e="T394" id="Seg_9432" s="T393">вода.[NOM.SG]</ta>
            <ta e="T395" id="Seg_9433" s="T394">лить-PST-1SG</ta>
            <ta e="T396" id="Seg_9434" s="T395">тогда</ta>
            <ta e="T398" id="Seg_9435" s="T396">прийти-PST.[3SG]</ta>
            <ta e="T405" id="Seg_9436" s="T404">тогда</ta>
            <ta e="T406" id="Seg_9437" s="T405">%%</ta>
            <ta e="T407" id="Seg_9438" s="T406">прийти-PST.[3SG]</ta>
            <ta e="T408" id="Seg_9439" s="T407">вода.[NOM.SG]</ta>
            <ta e="T409" id="Seg_9440" s="T408">NEG.EX.[3SG]</ta>
            <ta e="T410" id="Seg_9441" s="T409">а</ta>
            <ta e="T411" id="Seg_9442" s="T410">я.NOM</ta>
            <ta e="T412" id="Seg_9443" s="T411">принести-FUT-1SG</ta>
            <ta e="T413" id="Seg_9444" s="T412">и</ta>
            <ta e="T414" id="Seg_9445" s="T413">мыть-DRV-FUT-1SG</ta>
            <ta e="T415" id="Seg_9446" s="T414">а</ta>
            <ta e="T416" id="Seg_9447" s="T415">я.NOM</ta>
            <ta e="T986" id="Seg_9448" s="T416">пойти-CVB</ta>
            <ta e="T417" id="Seg_9449" s="T986">исчезнуть-PST-1SG</ta>
            <ta e="T418" id="Seg_9450" s="T417">этот.[NOM.SG]</ta>
            <ta e="T419" id="Seg_9451" s="T418">PTCL</ta>
            <ta e="T420" id="Seg_9452" s="T419">мыть-DRV-PST.[3SG]</ta>
            <ta e="T421" id="Seg_9453" s="T420">и</ta>
            <ta e="T422" id="Seg_9454" s="T421">дом-LAT/LOC.3SG</ta>
            <ta e="T423" id="Seg_9455" s="T422">прийти-PST.[3SG]</ta>
            <ta e="T425" id="Seg_9456" s="T424">я.NOM</ta>
            <ta e="T427" id="Seg_9457" s="T426">сестра-NOM/GEN/ACC.1SG</ta>
            <ta e="T428" id="Seg_9458" s="T427">сын-NOM/GEN.3SG</ta>
            <ta e="T429" id="Seg_9459" s="T428">идти-PST.[3SG]</ta>
            <ta e="T430" id="Seg_9460" s="T429">город-LAT</ta>
            <ta e="T431" id="Seg_9461" s="T430">там</ta>
            <ta e="T432" id="Seg_9462" s="T431">машина.[NOM.SG]</ta>
            <ta e="T433" id="Seg_9463" s="T432">быть-PST.[3SG]</ta>
            <ta e="T434" id="Seg_9464" s="T433">рубашка-PL</ta>
            <ta e="T435" id="Seg_9465" s="T434">мыть-INF.LAT</ta>
            <ta e="T436" id="Seg_9466" s="T435">тогда</ta>
            <ta e="T437" id="Seg_9467" s="T436">принести-PST.[3SG]</ta>
            <ta e="T438" id="Seg_9468" s="T437">дом-LAT/LOC.3SG</ta>
            <ta e="T439" id="Seg_9469" s="T438">а</ta>
            <ta e="T440" id="Seg_9470" s="T439">сегодня</ta>
            <ta e="T441" id="Seg_9471" s="T440">рубашка-PL</ta>
            <ta e="T442" id="Seg_9472" s="T441">PTCL</ta>
            <ta e="T443" id="Seg_9473" s="T442">мыть-PST-3PL</ta>
            <ta e="T444" id="Seg_9474" s="T443">и</ta>
            <ta e="T445" id="Seg_9475" s="T444">вешать-PST-3PL</ta>
            <ta e="T447" id="Seg_9476" s="T446">я.NOM</ta>
            <ta e="T448" id="Seg_9477" s="T447">сестра-LAT</ta>
            <ta e="T449" id="Seg_9478" s="T448">русский</ta>
            <ta e="T450" id="Seg_9479" s="T449">мать-NOM/GEN.3SG</ta>
            <ta e="T451" id="Seg_9480" s="T450">прийти-PST.[3SG]</ta>
            <ta e="T452" id="Seg_9481" s="T451">%%</ta>
            <ta e="T453" id="Seg_9482" s="T452">принести-PST.[3SG]</ta>
            <ta e="T454" id="Seg_9483" s="T453">мужчина.[NOM.SG]</ta>
            <ta e="T455" id="Seg_9484" s="T454">и</ta>
            <ta e="T456" id="Seg_9485" s="T455">женщина.[NOM.SG]</ta>
            <ta e="T457" id="Seg_9486" s="T456">тогда</ta>
            <ta e="T458" id="Seg_9487" s="T457">мать</ta>
            <ta e="T459" id="Seg_9488" s="T458">я.NOM</ta>
            <ta e="T460" id="Seg_9489" s="T459">мать-LAT</ta>
            <ta e="T461" id="Seg_9490" s="T460">сказать-IPFVZ.[3SG]</ta>
            <ta e="T462" id="Seg_9491" s="T461">пойти-IMP.2PL</ta>
            <ta e="T463" id="Seg_9492" s="T462">ягода.[NOM.SG]</ta>
            <ta e="T464" id="Seg_9493" s="T463">взять-2PL</ta>
            <ta e="T466" id="Seg_9494" s="T465">взять-2PL</ta>
            <ta e="T467" id="Seg_9495" s="T466">этот-PL</ta>
            <ta e="T468" id="Seg_9496" s="T467">пойти-PST-3PL</ta>
            <ta e="T469" id="Seg_9497" s="T468">этот.[NOM.SG]</ta>
            <ta e="T470" id="Seg_9498" s="T469">водка.[NOM.SG]</ta>
            <ta e="T471" id="Seg_9499" s="T470">дать-PST.[3SG]</ta>
            <ta e="T472" id="Seg_9500" s="T471">один.[NOM.SG]</ta>
            <ta e="T473" id="Seg_9501" s="T472">один.[NOM.SG]</ta>
            <ta e="T475" id="Seg_9502" s="T474">этот-PL</ta>
            <ta e="T476" id="Seg_9503" s="T475">брызгать-PST.[3SG]</ta>
            <ta e="T478" id="Seg_9504" s="T477">там</ta>
            <ta e="T479" id="Seg_9505" s="T478">ягода.[NOM.SG]</ta>
            <ta e="T480" id="Seg_9506" s="T479">этот-PL</ta>
            <ta e="T482" id="Seg_9507" s="T481">быть-PST-3PL</ta>
            <ta e="T483" id="Seg_9508" s="T482">быть-PST-3PL</ta>
            <ta e="T484" id="Seg_9509" s="T483">%%</ta>
            <ta e="T485" id="Seg_9510" s="T484">быть-PST-3PL</ta>
            <ta e="T486" id="Seg_9511" s="T485">тогда</ta>
            <ta e="T487" id="Seg_9512" s="T486">сидеть-PST-3PL</ta>
            <ta e="T488" id="Seg_9513" s="T487">есть-INF.LAT</ta>
            <ta e="T489" id="Seg_9514" s="T488">а</ta>
            <ta e="T490" id="Seg_9515" s="T489">я.NOM</ta>
            <ta e="T491" id="Seg_9516" s="T490">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T493" id="Seg_9517" s="T492">сказать-IPFVZ.[3SG]</ta>
            <ta e="T494" id="Seg_9518" s="T493">INCH</ta>
            <ta e="T496" id="Seg_9519" s="T494">отсюда</ta>
            <ta e="T497" id="Seg_9520" s="T496">надо</ta>
            <ta e="T498" id="Seg_9521" s="T497">ягода.[NOM.SG]</ta>
            <ta e="T499" id="Seg_9522" s="T498">сушить-INF.LAT</ta>
            <ta e="T500" id="Seg_9523" s="T499">этот</ta>
            <ta e="T501" id="Seg_9524" s="T500">водка-INS</ta>
            <ta e="T502" id="Seg_9525" s="T501">тогда</ta>
            <ta e="T503" id="Seg_9526" s="T502">лить-PST-3PL</ta>
            <ta e="T504" id="Seg_9527" s="T503">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T505" id="Seg_9528" s="T504">пить-PST-3PL</ta>
            <ta e="T506" id="Seg_9529" s="T505">тогда</ta>
            <ta e="T507" id="Seg_9530" s="T506">береста.[NOM.SG]</ta>
            <ta e="T508" id="Seg_9531" s="T507">собирать-PST-3PL</ta>
            <ta e="T509" id="Seg_9532" s="T508">много</ta>
            <ta e="T510" id="Seg_9533" s="T509">принести-PST-3PL</ta>
            <ta e="T511" id="Seg_9534" s="T510">прийти-PST-3PL</ta>
            <ta e="T512" id="Seg_9535" s="T511">дом-LAT/LOC.3SG</ta>
            <ta e="T513" id="Seg_9536" s="T512">там</ta>
            <ta e="T514" id="Seg_9537" s="T513">тот-ACC</ta>
            <ta e="T515" id="Seg_9538" s="T514">слушать-PRS.[3SG]</ta>
            <ta e="T516" id="Seg_9539" s="T515">нос-3SG-INS</ta>
            <ta e="T517" id="Seg_9540" s="T516">тот-ACC</ta>
            <ta e="T518" id="Seg_9541" s="T517">слушать-PRS.[3SG]</ta>
            <ta e="T519" id="Seg_9542" s="T518">нос-3SG-INS</ta>
            <ta e="T520" id="Seg_9543" s="T519">пахнуть-PRS.[3SG]</ta>
            <ta e="T521" id="Seg_9544" s="T520">водка-INS</ta>
            <ta e="T522" id="Seg_9545" s="T521">NEG</ta>
            <ta e="T523" id="Seg_9546" s="T522">знать-PST.[3SG]</ta>
            <ta e="T525" id="Seg_9547" s="T524">я.NOM</ta>
            <ta e="T527" id="Seg_9548" s="T526">сегодня</ta>
            <ta e="T528" id="Seg_9549" s="T527">утро</ta>
            <ta e="T529" id="Seg_9550" s="T528">встать-PST-1SG</ta>
            <ta e="T530" id="Seg_9551" s="T529">печь.[NOM.SG]</ta>
            <ta e="T531" id="Seg_9552" s="T530">светить-PST-1SG</ta>
            <ta e="T532" id="Seg_9553" s="T531">тогда</ta>
            <ta e="T533" id="Seg_9554" s="T532">корова-LAT</ta>
            <ta e="T534" id="Seg_9555" s="T533">трава.[NOM.SG]</ta>
            <ta e="T535" id="Seg_9556" s="T534">дать-PST-1SG</ta>
            <ta e="T536" id="Seg_9557" s="T535">картофель.[NOM.SG]</ta>
            <ta e="T537" id="Seg_9558" s="T536">пить-PST-1SG</ta>
            <ta e="T538" id="Seg_9559" s="T537">пойти-PST-1SG</ta>
            <ta e="T539" id="Seg_9560" s="T538">корова-LAT</ta>
            <ta e="T540" id="Seg_9561" s="T539">доить-PST-1SG</ta>
            <ta e="T541" id="Seg_9562" s="T540">молоко-NOM/GEN/ACC.3SG</ta>
            <ta e="T542" id="Seg_9563" s="T541">лить-PST-1SG</ta>
            <ta e="T543" id="Seg_9564" s="T542">а</ta>
            <ta e="T544" id="Seg_9565" s="T543">тогда</ta>
            <ta e="T545" id="Seg_9566" s="T544">пойти-PST-1SG</ta>
            <ta e="T546" id="Seg_9567" s="T545">женщина-LAT</ta>
            <ta e="T547" id="Seg_9568" s="T546">суп.[NOM.SG]</ta>
            <ta e="T550" id="Seg_9569" s="T549">суп.[NOM.SG]</ta>
            <ta e="T551" id="Seg_9570" s="T550">кипятить-PST-1SG</ta>
            <ta e="T552" id="Seg_9571" s="T551">тогда</ta>
            <ta e="T553" id="Seg_9572" s="T552">пойти-PST-1SG</ta>
            <ta e="T554" id="Seg_9573" s="T553">женщина-LAT</ta>
            <ta e="T555" id="Seg_9574" s="T554">суп.[NOM.SG]</ta>
            <ta e="T556" id="Seg_9575" s="T555">суп.[NOM.SG]</ta>
            <ta e="T557" id="Seg_9576" s="T556">принести-PST-1SG</ta>
            <ta e="T558" id="Seg_9577" s="T557">этот.[NOM.SG]</ta>
            <ta e="T559" id="Seg_9578" s="T558">PTCL</ta>
            <ta e="T561" id="Seg_9579" s="T560">идти-DUR.[3SG]</ta>
            <ta e="T562" id="Seg_9580" s="T561">нога-LOC</ta>
            <ta e="T563" id="Seg_9581" s="T562">что.[NOM.SG]</ta>
            <ta e="T564" id="Seg_9582" s="T563">дом-NOM/GEN/ACC.2SG</ta>
            <ta e="T565" id="Seg_9583" s="T564">мыть-PST.[3SG]</ta>
            <ta e="T566" id="Seg_9584" s="T565">нет</ta>
            <ta e="T567" id="Seg_9585" s="T566">ребенок-PL</ta>
            <ta e="T568" id="Seg_9586" s="T567">мыть-PST-3PL</ta>
            <ta e="T569" id="Seg_9587" s="T568">тогда</ta>
            <ta e="T570" id="Seg_9588" s="T569">прийти-PST.[3SG]</ta>
            <ta e="T571" id="Seg_9589" s="T570">здесь</ta>
            <ta e="T572" id="Seg_9590" s="T571">кровать-LAT</ta>
            <ta e="T573" id="Seg_9591" s="T572">PTCL</ta>
            <ta e="T574" id="Seg_9592" s="T573">упасть-MOM-PST.[3SG]</ta>
            <ta e="T575" id="Seg_9593" s="T574">я.NOM</ta>
            <ta e="T576" id="Seg_9594" s="T575">сказать-IPFVZ-1SG</ta>
            <ta e="T577" id="Seg_9595" s="T576">NEG.AUX-IMP.2SG</ta>
            <ta e="T578" id="Seg_9596" s="T577">упасть-CNG</ta>
            <ta e="T581" id="Seg_9597" s="T580">а</ta>
            <ta e="T582" id="Seg_9598" s="T581">суп-NOM/GEN/ACC.3SG</ta>
            <ta e="T583" id="Seg_9599" s="T582">PTCL</ta>
            <ta e="T584" id="Seg_9600" s="T583">%%-MOM-PST.[3SG]</ta>
            <ta e="T585" id="Seg_9601" s="T584">скоро</ta>
            <ta e="T586" id="Seg_9602" s="T585">NEG</ta>
            <ta e="T587" id="Seg_9603" s="T586">видеть-FUT-3SG</ta>
            <ta e="T589" id="Seg_9604" s="T588">тогда</ta>
            <ta e="T590" id="Seg_9605" s="T589">этот</ta>
            <ta e="T591" id="Seg_9606" s="T590">женщина-ACC.3SG</ta>
            <ta e="T593" id="Seg_9607" s="T592">пойти-PST-1SG</ta>
            <ta e="T594" id="Seg_9608" s="T593">Анисья-LAT</ta>
            <ta e="T595" id="Seg_9609" s="T594">долго</ta>
            <ta e="T596" id="Seg_9610" s="T595">NEG</ta>
            <ta e="T597" id="Seg_9611" s="T596">идти-PST-1SG</ta>
            <ta e="T598" id="Seg_9612" s="T597">прийти-PST-1SG</ta>
            <ta e="T599" id="Seg_9613" s="T598">там</ta>
            <ta e="T600" id="Seg_9614" s="T599">этот-LAT</ta>
            <ta e="T601" id="Seg_9615" s="T600">этот-PL</ta>
            <ta e="T602" id="Seg_9616" s="T601">PTCL</ta>
            <ta e="T603" id="Seg_9617" s="T602">там</ta>
            <ta e="T604" id="Seg_9618" s="T603">сын-3SG-COM</ta>
            <ta e="T605" id="Seg_9619" s="T604">ругать-DES-DUR.[3SG]</ta>
            <ta e="T607" id="Seg_9620" s="T606">тогда</ta>
            <ta e="T608" id="Seg_9621" s="T607">этот</ta>
            <ta e="T609" id="Seg_9622" s="T608">Анисья.[NOM.SG]</ta>
            <ta e="T610" id="Seg_9623" s="T609">PTCL</ta>
            <ta e="T611" id="Seg_9624" s="T610">ругать-DES-PST.[3SG]</ta>
            <ta e="T613" id="Seg_9625" s="T612">сын-NOM/GEN.3SG</ta>
            <ta e="T614" id="Seg_9626" s="T613">а</ta>
            <ta e="T615" id="Seg_9627" s="T614">сын-NOM/GEN.3SG</ta>
            <ta e="T616" id="Seg_9628" s="T615">PTCL</ta>
            <ta e="T617" id="Seg_9629" s="T616">этот-LAT</ta>
            <ta e="T618" id="Seg_9630" s="T617">что=INDEF</ta>
            <ta e="T619" id="Seg_9631" s="T618">говорить-FUT-3SG</ta>
            <ta e="T620" id="Seg_9632" s="T619">я.NOM</ta>
            <ta e="T621" id="Seg_9633" s="T620">сказать-IPFVZ-1SG</ta>
            <ta e="T622" id="Seg_9634" s="T621">NEG.AUX-IMP.2SG</ta>
            <ta e="T623" id="Seg_9635" s="T622">кричать-EP-CNG</ta>
            <ta e="T625" id="Seg_9636" s="T624">мать-GEN</ta>
            <ta e="T626" id="Seg_9637" s="T625">что.[NOM.SG]=INDEF</ta>
            <ta e="T627" id="Seg_9638" s="T626">NEG</ta>
            <ta e="T628" id="Seg_9639" s="T627">сказать-EP-CNG</ta>
            <ta e="T629" id="Seg_9640" s="T628">а</ta>
            <ta e="T630" id="Seg_9641" s="T629">этот.[NOM.SG]</ta>
            <ta e="T631" id="Seg_9642" s="T630">всегда</ta>
            <ta e="T632" id="Seg_9643" s="T631">кричать-PRS.[3SG]</ta>
            <ta e="T633" id="Seg_9644" s="T632">а</ta>
            <ta e="T634" id="Seg_9645" s="T633">мать-NOM/GEN.3SG</ta>
            <ta e="T635" id="Seg_9646" s="T634">PTCL</ta>
            <ta e="T636" id="Seg_9647" s="T635">INCH</ta>
            <ta e="T637" id="Seg_9648" s="T636">плакать-INF.LAT</ta>
            <ta e="T638" id="Seg_9649" s="T637">видеть-PRS-2SG</ta>
            <ta e="T639" id="Seg_9650" s="T638">как</ta>
            <ta e="T640" id="Seg_9651" s="T639">этот-PL</ta>
            <ta e="T641" id="Seg_9652" s="T640">хлеб-INS</ta>
            <ta e="T642" id="Seg_9653" s="T641">я.ACC</ta>
            <ta e="T643" id="Seg_9654" s="T642">кормить-FUT-3PL</ta>
            <ta e="T644" id="Seg_9655" s="T643">я.NOM</ta>
            <ta e="T645" id="Seg_9656" s="T644">хлеб.[NOM.SG]</ta>
            <ta e="T646" id="Seg_9657" s="T645">съесть-PRS.[3SG]</ta>
            <ta e="T647" id="Seg_9658" s="T646">и</ta>
            <ta e="T648" id="Seg_9659" s="T647">еще</ta>
            <ta e="T649" id="Seg_9660" s="T648">ругать-DES-DUR.[3SG]</ta>
            <ta e="T650" id="Seg_9661" s="T649">тогда</ta>
            <ta e="T652" id="Seg_9662" s="T651">%%</ta>
            <ta e="T653" id="Seg_9663" s="T652">сидеть-PST-1PL</ta>
            <ta e="T654" id="Seg_9664" s="T653">тогда</ta>
            <ta e="T655" id="Seg_9665" s="T654">Эля.[NOM.SG]</ta>
            <ta e="T656" id="Seg_9666" s="T655">прийти-PST.[3SG]</ta>
            <ta e="T657" id="Seg_9667" s="T656">здорово</ta>
            <ta e="T658" id="Seg_9668" s="T657">быть-PRS-2SG</ta>
            <ta e="T659" id="Seg_9669" s="T658">здорово</ta>
            <ta e="T660" id="Seg_9670" s="T659">быть-PRS-2SG</ta>
            <ta e="T661" id="Seg_9671" s="T660">что.[NOM.SG]</ta>
            <ta e="T662" id="Seg_9672" s="T661">прийти-PST-2SG</ta>
            <ta e="T663" id="Seg_9673" s="T662">хлеб.[NOM.SG]</ta>
            <ta e="T664" id="Seg_9674" s="T663">смотреть-FRQ-PST-1SG</ta>
            <ta e="T665" id="Seg_9675" s="T664">а</ta>
            <ta e="T666" id="Seg_9676" s="T665">я.NOM</ta>
            <ta e="T667" id="Seg_9677" s="T666">Анисья-LAT</ta>
            <ta e="T668" id="Seg_9678" s="T667">сказать-PRS-1SG</ta>
            <ta e="T669" id="Seg_9679" s="T668">дать-IMP.2SG</ta>
            <ta e="T670" id="Seg_9680" s="T669">этот-LAT</ta>
            <ta e="T671" id="Seg_9681" s="T670">один.[NOM.SG]</ta>
            <ta e="T672" id="Seg_9682" s="T671">хлеб.[NOM.SG]</ta>
            <ta e="T673" id="Seg_9683" s="T672">этот.[NOM.SG]</ta>
            <ta e="T674" id="Seg_9684" s="T673">дать-PST.[3SG]</ta>
            <ta e="T675" id="Seg_9685" s="T674">этот-LAT</ta>
            <ta e="T676" id="Seg_9686" s="T675">сколько</ta>
            <ta e="T677" id="Seg_9687" s="T676">спросить-INF.LAT</ta>
            <ta e="T678" id="Seg_9688" s="T677">а</ta>
            <ta e="T679" id="Seg_9689" s="T678">я.NOM</ta>
            <ta e="T680" id="Seg_9690" s="T679">сказать-IPFVZ-1SG</ta>
            <ta e="T681" id="Seg_9691" s="T680">%%</ta>
            <ta e="T682" id="Seg_9692" s="T681">NEG</ta>
            <ta e="T683" id="Seg_9693" s="T682">много</ta>
            <ta e="T684" id="Seg_9694" s="T683">как</ta>
            <ta e="T685" id="Seg_9695" s="T684">магазин-ABL</ta>
            <ta e="T686" id="Seg_9696" s="T685">тогда</ta>
            <ta e="T687" id="Seg_9697" s="T686">там</ta>
            <ta e="T688" id="Seg_9698" s="T687">этот.[NOM.SG]</ta>
            <ta e="T689" id="Seg_9699" s="T688">этот-LAT</ta>
            <ta e="T690" id="Seg_9700" s="T689">дать-PST.[3SG]</ta>
            <ta e="T691" id="Seg_9701" s="T690">четыре.[NOM.SG]</ta>
            <ta e="T692" id="Seg_9702" s="T691">десять.[NOM.SG]</ta>
            <ta e="T693" id="Seg_9703" s="T692">копейка-NOM/GEN/ACC.3PL</ta>
            <ta e="T695" id="Seg_9704" s="T694">тогда</ta>
            <ta e="T696" id="Seg_9705" s="T695">я.NOM</ta>
            <ta e="T697" id="Seg_9706" s="T696">прийти-PST-1SG</ta>
            <ta e="T698" id="Seg_9707" s="T697">корова-NOM/GEN/ACC.1SG</ta>
            <ta e="T699" id="Seg_9708" s="T698">пить-%%-PST-1SG</ta>
            <ta e="T700" id="Seg_9709" s="T699">теленок.[NOM.SG]</ta>
            <ta e="T701" id="Seg_9710" s="T700">пить-TR-PST-1SG</ta>
            <ta e="T702" id="Seg_9711" s="T701">трава.[NOM.SG]</ta>
            <ta e="T703" id="Seg_9712" s="T702">дать-PST-1SG</ta>
            <ta e="T704" id="Seg_9713" s="T703">тогда</ta>
            <ta e="T705" id="Seg_9714" s="T704">вода-VBLZ-CVB</ta>
            <ta e="T706" id="Seg_9715" s="T705">идти-PST-1SG</ta>
            <ta e="T707" id="Seg_9716" s="T706">и</ta>
            <ta e="T709" id="Seg_9717" s="T708">прийти-PST-1SG</ta>
            <ta e="T710" id="Seg_9718" s="T709">дом-LAT</ta>
            <ta e="T711" id="Seg_9719" s="T710">нога-NOM/GEN/ACC.1SG</ta>
            <ta e="T712" id="Seg_9720" s="T711">замерзнуть-RES-PST.[3SG]</ta>
            <ta e="T713" id="Seg_9721" s="T712">печь-LAT</ta>
            <ta e="T716" id="Seg_9722" s="T715">влезать-PST-1SG</ta>
            <ta e="T717" id="Seg_9723" s="T716">печь-LAT</ta>
            <ta e="T719" id="Seg_9724" s="T718">влезать-PST-1SG</ta>
            <ta e="T720" id="Seg_9725" s="T719">тогда</ta>
            <ta e="T721" id="Seg_9726" s="T720">нога-NOM/GEN/ACC.1SG</ta>
            <ta e="T723" id="Seg_9727" s="T722">%%</ta>
            <ta e="T724" id="Seg_9728" s="T723">этот-PL</ta>
            <ta e="T726" id="Seg_9729" s="T725">прийти-PST-3PL</ta>
            <ta e="T727" id="Seg_9730" s="T726">и</ta>
            <ta e="T728" id="Seg_9731" s="T727">говорить-INF.LAT</ta>
            <ta e="T730" id="Seg_9732" s="T729">тогда</ta>
            <ta e="T731" id="Seg_9733" s="T730">говорить-INF.LAT</ta>
            <ta e="T732" id="Seg_9734" s="T731">надо</ta>
            <ta e="T733" id="Seg_9735" s="T732">а</ta>
            <ta e="T734" id="Seg_9736" s="T733">там</ta>
            <ta e="T735" id="Seg_9737" s="T734">машина.[NOM.SG]</ta>
            <ta e="T736" id="Seg_9738" s="T735">PTCL</ta>
            <ta e="T737" id="Seg_9739" s="T736">кричать-DUR.[3SG]</ta>
            <ta e="T738" id="Seg_9740" s="T737">NEG</ta>
            <ta e="T739" id="Seg_9741" s="T738">дать-PRS.[3SG]</ta>
            <ta e="T740" id="Seg_9742" s="T739">говорить-INF.LAT</ta>
            <ta e="T741" id="Seg_9743" s="T740">мы.NOM</ta>
            <ta e="T742" id="Seg_9744" s="T741">бросить-MOM-PST-1PL</ta>
            <ta e="T743" id="Seg_9745" s="T742">а</ta>
            <ta e="T744" id="Seg_9746" s="T743">этот-PL</ta>
            <ta e="T987" id="Seg_9747" s="T744">пойти-CVB</ta>
            <ta e="T745" id="Seg_9748" s="T987">исчезнуть-PST-3PL</ta>
            <ta e="T746" id="Seg_9749" s="T745">NEG</ta>
            <ta e="T747" id="Seg_9750" s="T746">говорить-PST-1PL</ta>
            <ta e="T748" id="Seg_9751" s="T747">а</ta>
            <ta e="T749" id="Seg_9752" s="T748">сейчас</ta>
            <ta e="T750" id="Seg_9753" s="T749">прийти-PST-3PL</ta>
            <ta e="T751" id="Seg_9754" s="T750">опять</ta>
            <ta e="T753" id="Seg_9755" s="T752">говорить-INF.LAT</ta>
            <ta e="T755" id="Seg_9756" s="T754">прийти-PST-3PL</ta>
            <ta e="T756" id="Seg_9757" s="T755">а</ta>
            <ta e="T757" id="Seg_9758" s="T756">я.NOM</ta>
            <ta e="T758" id="Seg_9759" s="T757">сказать-IPFVZ-1SG</ta>
            <ta e="T759" id="Seg_9760" s="T758">где</ta>
            <ta e="T760" id="Seg_9761" s="T759">быть-PST-2PL</ta>
            <ta e="T761" id="Seg_9762" s="T760">да</ta>
            <ta e="T762" id="Seg_9763" s="T761">дерево.[NOM.SG]</ta>
            <ta e="T763" id="Seg_9764" s="T762">фотографировать-PST-1PL</ta>
            <ta e="T764" id="Seg_9765" s="T763">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T765" id="Seg_9766" s="T764">дом.[NOM.SG]</ta>
            <ta e="T766" id="Seg_9767" s="T765">фотографировать-PST-1PL</ta>
            <ta e="T767" id="Seg_9768" s="T766">и</ta>
            <ta e="T768" id="Seg_9769" s="T767">бабушка-GEN</ta>
            <ta e="T769" id="Seg_9770" s="T768">дом-LAT</ta>
            <ta e="T771" id="Seg_9771" s="T770">фотографировать-PST-1PL</ta>
            <ta e="T772" id="Seg_9772" s="T771">и</ta>
            <ta e="T773" id="Seg_9773" s="T772">здесь</ta>
            <ta e="T774" id="Seg_9774" s="T773">прийти-PST-1PL</ta>
            <ta e="T776" id="Seg_9775" s="T775">тогда</ta>
            <ta e="T777" id="Seg_9776" s="T776">мать-NOM/GEN.3SG</ta>
            <ta e="T778" id="Seg_9777" s="T777">прийти-PST.[3SG]</ta>
            <ta e="T779" id="Seg_9778" s="T778">больница-LOC</ta>
            <ta e="T780" id="Seg_9779" s="T779">INCH</ta>
            <ta e="T781" id="Seg_9780" s="T780">ребенок-ACC.PL</ta>
            <ta e="T783" id="Seg_9781" s="T782">что.[NOM.SG]=INDEF</ta>
            <ta e="T784" id="Seg_9782" s="T783">NEG</ta>
            <ta e="T785" id="Seg_9783" s="T784">делать-PRS-3PL</ta>
            <ta e="T786" id="Seg_9784" s="T785">возьми</ta>
            <ta e="T787" id="Seg_9785" s="T786">дерево-PL</ta>
            <ta e="T788" id="Seg_9786" s="T787">класть-EP-IMP.2SG</ta>
            <ta e="T789" id="Seg_9787" s="T788">ограда-LOC</ta>
            <ta e="T790" id="Seg_9788" s="T789">а.то</ta>
            <ta e="T791" id="Seg_9789" s="T790">отец-NOM/GEN/ACC.2SG</ta>
            <ta e="T792" id="Seg_9790" s="T791">прийти-FUT-3SG</ta>
            <ta e="T793" id="Seg_9791" s="T792">что.[NOM.SG]=INDEF</ta>
            <ta e="T794" id="Seg_9792" s="T793">NEG</ta>
            <ta e="T795" id="Seg_9793" s="T794">дать-FUT-3SG</ta>
            <ta e="T796" id="Seg_9794" s="T795">этот.[NOM.SG]</ta>
            <ta e="T797" id="Seg_9795" s="T796">пойти-PST.[3SG]</ta>
            <ta e="T798" id="Seg_9796" s="T797">и</ta>
            <ta e="T799" id="Seg_9797" s="T798">INCH</ta>
            <ta e="T800" id="Seg_9798" s="T799">класть-INF.LAT</ta>
            <ta e="T801" id="Seg_9799" s="T800">тогда</ta>
            <ta e="T802" id="Seg_9800" s="T801">прийти-PST.[3SG]</ta>
            <ta e="T803" id="Seg_9801" s="T802">баня-LOC</ta>
            <ta e="T804" id="Seg_9802" s="T803">мыть-DRV-PST-3PL</ta>
            <ta e="T805" id="Seg_9803" s="T804">тогда</ta>
            <ta e="T806" id="Seg_9804" s="T805">я.LAT</ta>
            <ta e="T807" id="Seg_9805" s="T806">сказать-IPFVZ-3PL</ta>
            <ta e="T808" id="Seg_9806" s="T807">пойти-EP-IMP.2SG</ta>
            <ta e="T809" id="Seg_9807" s="T808">баня-LAT</ta>
            <ta e="T810" id="Seg_9808" s="T809">я.NOM</ta>
            <ta e="T811" id="Seg_9809" s="T810">пойти-PST-1SG</ta>
            <ta e="T812" id="Seg_9810" s="T811">мыть-DRV-INF.LAT</ta>
            <ta e="T813" id="Seg_9811" s="T812">тогда</ta>
            <ta e="T814" id="Seg_9812" s="T813">Эля.[NOM.SG]</ta>
            <ta e="T815" id="Seg_9813" s="T814">прийти-PST.[3SG]</ta>
            <ta e="T816" id="Seg_9814" s="T815">мыть-DRV-INF.LAT</ta>
            <ta e="T817" id="Seg_9815" s="T816">тогда</ta>
            <ta e="T818" id="Seg_9816" s="T817">мужчина.[NOM.SG]</ta>
            <ta e="T819" id="Seg_9817" s="T818">прийти-PST.[3SG]</ta>
            <ta e="T820" id="Seg_9818" s="T819">мыть-DRV-PST.[3SG]</ta>
            <ta e="T821" id="Seg_9819" s="T820">тогда</ta>
            <ta e="T822" id="Seg_9820" s="T821">отец-NOM/GEN.3SG</ta>
            <ta e="T823" id="Seg_9821" s="T822">прийти-PST.[3SG]</ta>
            <ta e="T824" id="Seg_9822" s="T823">принести-PST.[3SG]</ta>
            <ta e="T825" id="Seg_9823" s="T824">чулки-NOM/GEN/ACC.3PL</ta>
            <ta e="T826" id="Seg_9824" s="T825">штаны-PL</ta>
            <ta e="T827" id="Seg_9825" s="T826">принести-PST.[3SG]</ta>
            <ta e="T829" id="Seg_9826" s="T828">тогда</ta>
            <ta e="T830" id="Seg_9827" s="T829">прийти-PST.[3SG]</ta>
            <ta e="T831" id="Seg_9828" s="T830">водка.[NOM.SG]</ta>
            <ta e="T832" id="Seg_9829" s="T831">принести-PST.[3SG]</ta>
            <ta e="T833" id="Seg_9830" s="T832">один.[NOM.SG]</ta>
            <ta e="T834" id="Seg_9831" s="T833">мужчина.[NOM.SG]</ta>
            <ta e="T835" id="Seg_9832" s="T834">этот-ACC</ta>
            <ta e="T836" id="Seg_9833" s="T835">принести-PST.[3SG]</ta>
            <ta e="T837" id="Seg_9834" s="T836">лошадь-3SG-INS</ta>
            <ta e="T838" id="Seg_9835" s="T837">тогда</ta>
            <ta e="T839" id="Seg_9836" s="T838">этот.[NOM.SG]</ta>
            <ta e="T840" id="Seg_9837" s="T839">этот-LAT</ta>
            <ta e="T841" id="Seg_9838" s="T840">водка.[NOM.SG]</ta>
            <ta e="T842" id="Seg_9839" s="T841">лить-PST.[3SG]</ta>
            <ta e="T843" id="Seg_9840" s="T842">%%</ta>
            <ta e="T844" id="Seg_9841" s="T843">есть-PST.[3SG]</ta>
            <ta e="T846" id="Seg_9842" s="T845">пойти-PST.[3SG]</ta>
            <ta e="T847" id="Seg_9843" s="T846">дом-LAT/LOC.3SG</ta>
            <ta e="T849" id="Seg_9844" s="T848">этот.[NOM.SG]</ta>
            <ta e="T850" id="Seg_9845" s="T849">женщина.[NOM.SG]</ta>
            <ta e="T851" id="Seg_9846" s="T850">очень</ta>
            <ta e="T852" id="Seg_9847" s="T851">сердитый.[NOM.SG]</ta>
            <ta e="T853" id="Seg_9848" s="T852">дерево.[NOM.SG]</ta>
            <ta e="T854" id="Seg_9849" s="T853">взять-PST.[3SG]</ta>
            <ta e="T855" id="Seg_9850" s="T854">и</ta>
            <ta e="T856" id="Seg_9851" s="T855">теленок-ACC</ta>
            <ta e="T858" id="Seg_9852" s="T857">дерево-INS</ta>
            <ta e="T859" id="Seg_9853" s="T858">ударить-MULT-PST.[3SG]</ta>
            <ta e="T860" id="Seg_9854" s="T859">глаз-3SG-LAT</ta>
            <ta e="T861" id="Seg_9855" s="T860">%слеза-NOM/GEN/ACC.3SG</ta>
            <ta e="T862" id="Seg_9856" s="T861">PTCL</ta>
            <ta e="T863" id="Seg_9857" s="T862">течь-DUR.[3SG]</ta>
            <ta e="T864" id="Seg_9858" s="T863">а</ta>
            <ta e="T865" id="Seg_9859" s="T864">я.NOM</ta>
            <ta e="T866" id="Seg_9860" s="T865">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T867" id="Seg_9861" s="T866">теленок-NOM/GEN/ACC.1SG</ta>
            <ta e="T868" id="Seg_9862" s="T867">NEG</ta>
            <ta e="T869" id="Seg_9863" s="T868">бить-PRS-1SG</ta>
            <ta e="T870" id="Seg_9864" s="T869">рука-INS</ta>
            <ta e="T871" id="Seg_9865" s="T870">рука-INS</ta>
            <ta e="T872" id="Seg_9866" s="T871">ударить-FUT-1SG</ta>
            <ta e="T873" id="Seg_9867" s="T872">%%</ta>
            <ta e="T874" id="Seg_9868" s="T873">PTCL</ta>
            <ta e="T875" id="Seg_9869" s="T874">пить-MOM-PST.[3SG]</ta>
            <ta e="T877" id="Seg_9870" s="T876">я.NOM</ta>
            <ta e="T878" id="Seg_9871" s="T877">дом-LOC</ta>
            <ta e="T879" id="Seg_9872" s="T878">фотографировать-PST-1SG</ta>
            <ta e="T880" id="Seg_9873" s="T879">машина-ACC</ta>
            <ta e="T882" id="Seg_9874" s="T880">и</ta>
            <ta e="T883" id="Seg_9875" s="T882">этот-PL</ta>
            <ta e="T884" id="Seg_9876" s="T883">один.[NOM.SG]</ta>
            <ta e="T885" id="Seg_9877" s="T884">два.[NOM.SG]</ta>
            <ta e="T886" id="Seg_9878" s="T885">три.[NOM.SG]</ta>
            <ta e="T887" id="Seg_9879" s="T886">четыре.[NOM.SG]</ta>
            <ta e="T888" id="Seg_9880" s="T887">тогда</ta>
            <ta e="T889" id="Seg_9881" s="T888">Ваня.[NOM.SG]</ta>
            <ta e="T890" id="Seg_9882" s="T889">сказать-IPFVZ.[3SG]</ta>
            <ta e="T891" id="Seg_9883" s="T890">теленок.[NOM.SG]</ta>
            <ta e="T892" id="Seg_9884" s="T891">нужно</ta>
            <ta e="T893" id="Seg_9885" s="T892">и</ta>
            <ta e="T894" id="Seg_9886" s="T893">свинья-PL</ta>
            <ta e="T895" id="Seg_9887" s="T894">а</ta>
            <ta e="T896" id="Seg_9888" s="T895">мы.NOM</ta>
            <ta e="T897" id="Seg_9889" s="T896">PTCL</ta>
            <ta e="T898" id="Seg_9890" s="T897">смеяться-PST-1PL</ta>
            <ta e="T900" id="Seg_9891" s="T899">сегодня</ta>
            <ta e="T901" id="Seg_9892" s="T900">наверное</ta>
            <ta e="T902" id="Seg_9893" s="T901">много</ta>
            <ta e="T903" id="Seg_9894" s="T902">говорить-PST-1PL</ta>
            <ta e="T904" id="Seg_9895" s="T903">сейчас</ta>
            <ta e="T905" id="Seg_9896" s="T904">надо</ta>
            <ta e="T906" id="Seg_9897" s="T905">дом-LAT/LOC.1SG</ta>
            <ta e="T907" id="Seg_9898" s="T906">пойти-INF.LAT</ta>
            <ta e="T908" id="Seg_9899" s="T907">есть-INF.LAT</ta>
            <ta e="T909" id="Seg_9900" s="T908">лежать-INF.LAT</ta>
            <ta e="T911" id="Seg_9901" s="T910">%%</ta>
            <ta e="T913" id="Seg_9902" s="T912">дым.[NOM.SG]</ta>
            <ta e="T914" id="Seg_9903" s="T913">PTCL</ta>
            <ta e="T915" id="Seg_9904" s="T914">наружу</ta>
            <ta e="T916" id="Seg_9905" s="T915">идти-PRS.[3SG]</ta>
            <ta e="T917" id="Seg_9906" s="T916">очень</ta>
            <ta e="T918" id="Seg_9907" s="T917">холодный.[NOM.SG]</ta>
            <ta e="T919" id="Seg_9908" s="T918">стать-FUT-3SG</ta>
            <ta e="T920" id="Seg_9909" s="T919">сегодня</ta>
            <ta e="T921" id="Seg_9910" s="T920">вечер-LOC.ADV</ta>
            <ta e="T922" id="Seg_9911" s="T921">снег.[NOM.SG]</ta>
            <ta e="T923" id="Seg_9912" s="T922">утро-LOC.ADV</ta>
            <ta e="T924" id="Seg_9913" s="T923">встать-PST-1SG</ta>
            <ta e="T925" id="Seg_9914" s="T924">PTCL</ta>
            <ta e="T926" id="Seg_9915" s="T925">холодный.[NOM.SG]</ta>
            <ta e="T927" id="Seg_9916" s="T926">стать-RES-PST.[3SG]</ta>
            <ta e="T929" id="Seg_9917" s="T928">мы.LAT</ta>
            <ta e="T930" id="Seg_9918" s="T929">сегодня</ta>
            <ta e="T931" id="Seg_9919" s="T930">надо</ta>
            <ta e="T932" id="Seg_9920" s="T931">пойти-INF.LAT</ta>
            <ta e="T933" id="Seg_9921" s="T932">Таня-LAT</ta>
            <ta e="T934" id="Seg_9922" s="T933">чтобы</ta>
            <ta e="T935" id="Seg_9923" s="T934">этот.[NOM.SG]</ta>
            <ta e="T936" id="Seg_9924" s="T935">хлеб.[NOM.SG]</ta>
            <ta e="T937" id="Seg_9925" s="T936">взять-PST.[3SG]</ta>
            <ta e="T938" id="Seg_9926" s="T937">Вознесенское-ABL</ta>
            <ta e="T939" id="Seg_9927" s="T938">и</ta>
            <ta e="T940" id="Seg_9928" s="T939">принести-PST.[3SG]</ta>
            <ta e="T941" id="Seg_9929" s="T940">мы.LAT</ta>
            <ta e="T944" id="Seg_9930" s="T943">надо</ta>
            <ta e="T945" id="Seg_9931" s="T944">деньги-ACC</ta>
            <ta e="T946" id="Seg_9932" s="T945">взять-INF.LAT</ta>
            <ta e="T947" id="Seg_9933" s="T946">этот-LAT</ta>
            <ta e="T948" id="Seg_9934" s="T947">NEG</ta>
            <ta e="T949" id="Seg_9935" s="T948">сам-LAT/LOC.3SG</ta>
            <ta e="T950" id="Seg_9936" s="T949">принести-INF.LAT</ta>
            <ta e="T951" id="Seg_9937" s="T950">оттуда</ta>
            <ta e="T952" id="Seg_9938" s="T951">лошадь-3SG-INS</ta>
            <ta e="T953" id="Seg_9939" s="T952">идти-FUT-3SG</ta>
            <ta e="T954" id="Seg_9940" s="T953">и</ta>
            <ta e="T955" id="Seg_9941" s="T954">принести-FUT-3SG</ta>
            <ta e="T957" id="Seg_9942" s="T956">NEG</ta>
            <ta e="T958" id="Seg_9943" s="T957">знать-1SG</ta>
            <ta e="T959" id="Seg_9944" s="T958">взять-FUT-3SG</ta>
            <ta e="T960" id="Seg_9945" s="T959">или</ta>
            <ta e="T961" id="Seg_9946" s="T960">NEG</ta>
            <ta e="T962" id="Seg_9947" s="T961">надо</ta>
            <ta e="T963" id="Seg_9948" s="T962">пойти-INF.LAT</ta>
            <ta e="T964" id="Seg_9949" s="T963">этот-COM</ta>
            <ta e="T965" id="Seg_9950" s="T964">говорить-INF.LAT</ta>
            <ta e="T966" id="Seg_9951" s="T965">может.быть</ta>
            <ta e="T967" id="Seg_9952" s="T966">взять-FUT-3SG</ta>
            <ta e="T968" id="Seg_9953" s="T967">ну</ta>
            <ta e="T969" id="Seg_9954" s="T968">пойти-EP-IMP.2SG</ta>
            <ta e="T970" id="Seg_9955" s="T969">говорить-EP-IMP.2SG</ta>
            <ta e="T972" id="Seg_9956" s="T971">этот.[NOM.SG]</ta>
            <ta e="T973" id="Seg_9957" s="T972">может.быть</ta>
            <ta e="T974" id="Seg_9958" s="T973">идти-FUT-3SG</ta>
            <ta e="T975" id="Seg_9959" s="T974">магазин-LOC</ta>
            <ta e="T976" id="Seg_9960" s="T975">там</ta>
            <ta e="T978" id="Seg_9961" s="T977">стоять-INF.LAT</ta>
            <ta e="T979" id="Seg_9962" s="T978">надо</ta>
            <ta e="T980" id="Seg_9963" s="T979">хлеб.[NOM.SG]</ta>
            <ta e="T981" id="Seg_9964" s="T980">взять-INF.LAT</ta>
            <ta e="T982" id="Seg_9965" s="T981">я.NOM</ta>
            <ta e="T983" id="Seg_9966" s="T982">NEG</ta>
            <ta e="T984" id="Seg_9967" s="T983">быть-FUT-1SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_9968" s="T0">v-v:mood.pn</ta>
            <ta e="T2" id="Seg_9969" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_9970" s="T2">adv</ta>
            <ta e="T4" id="Seg_9971" s="T3">num-num&gt;num</ta>
            <ta e="T5" id="Seg_9972" s="T4">dempro-n:case</ta>
            <ta e="T6" id="Seg_9973" s="T5">pers</ta>
            <ta e="T7" id="Seg_9974" s="T6">n-n:case.poss</ta>
            <ta e="T8" id="Seg_9975" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_9976" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_9977" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_9978" s="T10">conj</ta>
            <ta e="T12" id="Seg_9979" s="T11">pers</ta>
            <ta e="T13" id="Seg_9980" s="T12">dempro-n:case</ta>
            <ta e="T14" id="Seg_9981" s="T13">ptcl</ta>
            <ta e="T16" id="Seg_9982" s="T15">v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_9983" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_9984" s="T17">n</ta>
            <ta e="T19" id="Seg_9985" s="T18">v-v:n.fin</ta>
            <ta e="T20" id="Seg_9986" s="T19">adv</ta>
            <ta e="T21" id="Seg_9987" s="T20">v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_9988" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_9989" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_9990" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_9991" s="T24">ptcl</ta>
            <ta e="T27" id="Seg_9992" s="T26">ptcl</ta>
            <ta e="T28" id="Seg_9993" s="T27">v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_9994" s="T28">adv</ta>
            <ta e="T30" id="Seg_9995" s="T29">n-n:num</ta>
            <ta e="T31" id="Seg_9996" s="T30">n-n:num</ta>
            <ta e="T32" id="Seg_9997" s="T31">n-n:num</ta>
            <ta e="T33" id="Seg_9998" s="T32">num-n:case</ta>
            <ta e="T35" id="Seg_9999" s="T34">v-v:pn</ta>
            <ta e="T36" id="Seg_10000" s="T35">conj</ta>
            <ta e="T37" id="Seg_10001" s="T36">n-n:num</ta>
            <ta e="T38" id="Seg_10002" s="T37">adv</ta>
            <ta e="T39" id="Seg_10003" s="T38">v-v&gt;v-v:pn</ta>
            <ta e="T40" id="Seg_10004" s="T39">adv</ta>
            <ta e="T41" id="Seg_10005" s="T40">num-n:case</ta>
            <ta e="T42" id="Seg_10006" s="T41">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T43" id="Seg_10007" s="T42">n-n:num</ta>
            <ta e="T45" id="Seg_10008" s="T44">pers</ta>
            <ta e="T46" id="Seg_10009" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_10010" s="T46">n-n:num</ta>
            <ta e="T48" id="Seg_10011" s="T47">num-num&gt;num</ta>
            <ta e="T49" id="Seg_10012" s="T48">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T50" id="Seg_10013" s="T49">adv</ta>
            <ta e="T51" id="Seg_10014" s="T50">n-n:case.poss</ta>
            <ta e="T52" id="Seg_10015" s="T51">v-v:tense-v:pn</ta>
            <ta e="T53" id="Seg_10016" s="T52">num-n:case</ta>
            <ta e="T54" id="Seg_10017" s="T53">n-n:case</ta>
            <ta e="T55" id="Seg_10018" s="T54">num-n:case</ta>
            <ta e="T56" id="Seg_10019" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_10020" s="T56">v-v:tense-v:pn</ta>
            <ta e="T58" id="Seg_10021" s="T57">dempro-n:case</ta>
            <ta e="T59" id="Seg_10022" s="T58">dempro-n:num</ta>
            <ta e="T61" id="Seg_10023" s="T60">v-v:tense-v:pn</ta>
            <ta e="T62" id="Seg_10024" s="T61">adv</ta>
            <ta e="T63" id="Seg_10025" s="T62">n-n:case</ta>
            <ta e="T64" id="Seg_10026" s="T63">v-v&gt;v-v:pn</ta>
            <ta e="T65" id="Seg_10027" s="T64">conj</ta>
            <ta e="T66" id="Seg_10028" s="T65">adv</ta>
            <ta e="T67" id="Seg_10029" s="T66">pers</ta>
            <ta e="T68" id="Seg_10030" s="T67">v-v&gt;v-v:pn</ta>
            <ta e="T69" id="Seg_10031" s="T68">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T71" id="Seg_10032" s="T70">pers</ta>
            <ta e="T72" id="Seg_10033" s="T71">n-n:case.poss</ta>
            <ta e="T73" id="Seg_10034" s="T72">v-v:tense-v:pn</ta>
            <ta e="T988" id="Seg_10035" s="T73">propr</ta>
            <ta e="T74" id="Seg_10036" s="T988">n-n:case</ta>
            <ta e="T75" id="Seg_10037" s="T74">adv</ta>
            <ta e="T76" id="Seg_10038" s="T75">ptcl</ta>
            <ta e="T77" id="Seg_10039" s="T76">n-n:case</ta>
            <ta e="T78" id="Seg_10040" s="T77">n-n:case</ta>
            <ta e="T79" id="Seg_10041" s="T78">v-v:tense-v:pn</ta>
            <ta e="T80" id="Seg_10042" s="T79">quant</ta>
            <ta e="T81" id="Seg_10043" s="T80">v-v:tense-v:pn</ta>
            <ta e="T82" id="Seg_10044" s="T81">v-v&gt;v-v:pn</ta>
            <ta e="T83" id="Seg_10045" s="T82">v-v:ins-v:mood.pn</ta>
            <ta e="T84" id="Seg_10046" s="T83">n-n:case</ta>
            <ta e="T85" id="Seg_10047" s="T84">conj</ta>
            <ta e="T86" id="Seg_10048" s="T85">pers</ta>
            <ta e="T87" id="Seg_10049" s="T86">conj</ta>
            <ta e="T88" id="Seg_10050" s="T87">dempro-n:case</ta>
            <ta e="T89" id="Seg_10051" s="T88">v-v:tense-v:pn</ta>
            <ta e="T90" id="Seg_10052" s="T89">ptcl</ta>
            <ta e="T91" id="Seg_10053" s="T90">v-v:pn</ta>
            <ta e="T92" id="Seg_10054" s="T91">que</ta>
            <ta e="T93" id="Seg_10055" s="T92">n-n:case</ta>
            <ta e="T94" id="Seg_10056" s="T93">v-v:n.fin</ta>
            <ta e="T95" id="Seg_10057" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_10058" s="T95">v-v:tense-v:pn</ta>
            <ta e="T97" id="Seg_10059" s="T96">ptcl</ta>
            <ta e="T98" id="Seg_10060" s="T97">n-n:num</ta>
            <ta e="T99" id="Seg_10061" s="T98">n-n:case</ta>
            <ta e="T109" id="Seg_10062" s="T108">adv</ta>
            <ta e="T110" id="Seg_10063" s="T109">v-v:tense-v:pn</ta>
            <ta e="T111" id="Seg_10064" s="T110">v-v:tense-v:pn</ta>
            <ta e="T112" id="Seg_10065" s="T111">que-n:case</ta>
            <ta e="T113" id="Seg_10066" s="T112">v-v:tense-v:pn</ta>
            <ta e="T114" id="Seg_10067" s="T113">v-v:tense-v:pn</ta>
            <ta e="T115" id="Seg_10068" s="T114">ptcl</ta>
            <ta e="T116" id="Seg_10069" s="T115">v-v:tense-v:pn</ta>
            <ta e="T117" id="Seg_10070" s="T116">ptcl</ta>
            <ta e="T118" id="Seg_10071" s="T117">v-v:tense-v:pn</ta>
            <ta e="T120" id="Seg_10072" s="T119">adv</ta>
            <ta e="T122" id="Seg_10073" s="T121">v-v:tense-v:pn</ta>
            <ta e="T123" id="Seg_10074" s="T122">v-v:ins-v:mood.pn</ta>
            <ta e="T124" id="Seg_10075" s="T123">n-n:case.poss</ta>
            <ta e="T125" id="Seg_10076" s="T124">n-n:case</ta>
            <ta e="T126" id="Seg_10077" s="T125">v-v:mood.pn</ta>
            <ta e="T127" id="Seg_10078" s="T126">n-n:num-n:case</ta>
            <ta e="T128" id="Seg_10079" s="T127">pers</ta>
            <ta e="T129" id="Seg_10080" s="T128">v-v:tense-v:pn</ta>
            <ta e="T131" id="Seg_10081" s="T130">v-v:tense-v:pn</ta>
            <ta e="T132" id="Seg_10082" s="T131">n-n:case</ta>
            <ta e="T133" id="Seg_10083" s="T132">quant</ta>
            <ta e="T134" id="Seg_10084" s="T133">conj</ta>
            <ta e="T135" id="Seg_10085" s="T134">n-n:case</ta>
            <ta e="T136" id="Seg_10086" s="T135">v-v:pn</ta>
            <ta e="T137" id="Seg_10087" s="T136">adv</ta>
            <ta e="T138" id="Seg_10088" s="T137">v-v:tense-v:pn</ta>
            <ta e="T139" id="Seg_10089" s="T138">que-n:case</ta>
            <ta e="T140" id="Seg_10090" s="T139">n-n:case</ta>
            <ta e="T141" id="Seg_10091" s="T140">ptcl</ta>
            <ta e="T143" id="Seg_10092" s="T142">v-v:tense-v:pn</ta>
            <ta e="T144" id="Seg_10093" s="T143">ptcl</ta>
            <ta e="T145" id="Seg_10094" s="T144">n-n:case</ta>
            <ta e="T146" id="Seg_10095" s="T145">quant</ta>
            <ta e="T147" id="Seg_10096" s="T146">conj</ta>
            <ta e="T148" id="Seg_10097" s="T147">n-n:case</ta>
            <ta e="T149" id="Seg_10098" s="T148">v-v:pn</ta>
            <ta e="T150" id="Seg_10099" s="T149">dempro-n:num</ta>
            <ta e="T151" id="Seg_10100" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_10101" s="T151">v-v:n.fin</ta>
            <ta e="T154" id="Seg_10102" s="T153">adv</ta>
            <ta e="T155" id="Seg_10103" s="T154">v-v:tense-v:pn</ta>
            <ta e="T156" id="Seg_10104" s="T155">v-v:ins-v:mood.pn</ta>
            <ta e="T157" id="Seg_10105" s="T156">n-n:case</ta>
            <ta e="T158" id="Seg_10106" s="T157">v-v:mood.pn</ta>
            <ta e="T159" id="Seg_10107" s="T158">n-n:num</ta>
            <ta e="T160" id="Seg_10108" s="T159">pers</ta>
            <ta e="T161" id="Seg_10109" s="T160">v-v:tense-v:pn</ta>
            <ta e="T162" id="Seg_10110" s="T161">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T163" id="Seg_10111" s="T162">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T164" id="Seg_10112" s="T163">v-v:tense-v:pn</ta>
            <ta e="T165" id="Seg_10113" s="T164">%%</ta>
            <ta e="T166" id="Seg_10114" s="T165">adv</ta>
            <ta e="T167" id="Seg_10115" s="T166">que=ptcl</ta>
            <ta e="T168" id="Seg_10116" s="T167">adv</ta>
            <ta e="T169" id="Seg_10117" s="T168">%%</ta>
            <ta e="T170" id="Seg_10118" s="T169">num-n:case</ta>
            <ta e="T173" id="Seg_10119" s="T172">v-v:tense-v:pn</ta>
            <ta e="T174" id="Seg_10120" s="T173">conj</ta>
            <ta e="T175" id="Seg_10121" s="T174">v-v:tense-v:pn</ta>
            <ta e="T176" id="Seg_10122" s="T175">n-n:case</ta>
            <ta e="T177" id="Seg_10123" s="T176">num-n:case</ta>
            <ta e="T178" id="Seg_10124" s="T177">n-n:case</ta>
            <ta e="T179" id="Seg_10125" s="T178">n-n:case</ta>
            <ta e="T181" id="Seg_10126" s="T180">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T182" id="Seg_10127" s="T181">num-n:case</ta>
            <ta e="T184" id="Seg_10128" s="T183">n-n:case</ta>
            <ta e="T185" id="Seg_10129" s="T184">adv</ta>
            <ta e="T186" id="Seg_10130" s="T185">adv</ta>
            <ta e="T188" id="Seg_10131" s="T187">pers</ta>
            <ta e="T189" id="Seg_10132" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_10133" s="T189">v-v:tense-v:pn</ta>
            <ta e="T192" id="Seg_10134" s="T191">adv</ta>
            <ta e="T194" id="Seg_10135" s="T193">dempro</ta>
            <ta e="T195" id="Seg_10136" s="T194">n-n:case</ta>
            <ta e="T196" id="Seg_10137" s="T195">n</ta>
            <ta e="T197" id="Seg_10138" s="T196">n-n:case.poss</ta>
            <ta e="T198" id="Seg_10139" s="T197">v-v&gt;v-v:pn</ta>
            <ta e="T199" id="Seg_10140" s="T198">que</ta>
            <ta e="T200" id="Seg_10141" s="T199">pers</ta>
            <ta e="T201" id="Seg_10142" s="T200">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T202" id="Seg_10143" s="T201">v-v:mood.pn</ta>
            <ta e="T203" id="Seg_10144" s="T202">conj</ta>
            <ta e="T204" id="Seg_10145" s="T203">v-v:mood.pn</ta>
            <ta e="T205" id="Seg_10146" s="T204">n-n:case</ta>
            <ta e="T206" id="Seg_10147" s="T205">n-n:case.poss</ta>
            <ta e="T207" id="Seg_10148" s="T206">v-v&gt;v-v:pn</ta>
            <ta e="T208" id="Seg_10149" s="T207">v-v:tense-v:pn</ta>
            <ta e="T209" id="Seg_10150" s="T208">dempro-n:case</ta>
            <ta e="T210" id="Seg_10151" s="T209">num-n:case</ta>
            <ta e="T211" id="Seg_10152" s="T210">adv</ta>
            <ta e="T214" id="Seg_10153" s="T213">v-v:tense-v:pn</ta>
            <ta e="T215" id="Seg_10154" s="T214">conj</ta>
            <ta e="T216" id="Seg_10155" s="T215">que-n:case</ta>
            <ta e="T217" id="Seg_10156" s="T216">v-v:tense-v:pn</ta>
            <ta e="T218" id="Seg_10157" s="T217">n-n:case</ta>
            <ta e="T219" id="Seg_10158" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_10159" s="T219">v-v:pn</ta>
            <ta e="T221" id="Seg_10160" s="T220">conj</ta>
            <ta e="T222" id="Seg_10161" s="T221">pers</ta>
            <ta e="T226" id="Seg_10162" s="T225">num-n:case</ta>
            <ta e="T227" id="Seg_10163" s="T226">n-n:case</ta>
            <ta e="T228" id="Seg_10164" s="T227">propr</ta>
            <ta e="T229" id="Seg_10165" s="T228">n-n:case</ta>
            <ta e="T230" id="Seg_10166" s="T229">v-v:tense-v:pn</ta>
            <ta e="T232" id="Seg_10167" s="T231">n-n:case.poss-n:case</ta>
            <ta e="T234" id="Seg_10168" s="T233">v-v:tense-v:pn</ta>
            <ta e="T235" id="Seg_10169" s="T234">n-n:case.poss</ta>
            <ta e="T236" id="Seg_10170" s="T235">conj</ta>
            <ta e="T237" id="Seg_10171" s="T236">v-v:tense-v:pn</ta>
            <ta e="T238" id="Seg_10172" s="T237">n-n:case</ta>
            <ta e="T239" id="Seg_10173" s="T238">v-v&gt;v-v:pn</ta>
            <ta e="T241" id="Seg_10174" s="T240">adv</ta>
            <ta e="T242" id="Seg_10175" s="T241">v-v&gt;v-v:pn</ta>
            <ta e="T243" id="Seg_10176" s="T242">dempro-n:case</ta>
            <ta e="T244" id="Seg_10177" s="T243">n-n:case</ta>
            <ta e="T245" id="Seg_10178" s="T244">ptcl</ta>
            <ta e="T246" id="Seg_10179" s="T245">v-v:tense-v:pn</ta>
            <ta e="T249" id="Seg_10180" s="T248">adj</ta>
            <ta e="T250" id="Seg_10181" s="T249">n-n:num</ta>
            <ta e="T251" id="Seg_10182" s="T250">ptcl</ta>
            <ta e="T252" id="Seg_10183" s="T251">v-v&gt;v-v:n.fin</ta>
            <ta e="T253" id="Seg_10184" s="T252">conj</ta>
            <ta e="T254" id="Seg_10185" s="T253">n-n:num</ta>
            <ta e="T255" id="Seg_10186" s="T254">%%</ta>
            <ta e="T257" id="Seg_10187" s="T256">adj-n:case</ta>
            <ta e="T258" id="Seg_10188" s="T257">ptcl</ta>
            <ta e="T259" id="Seg_10189" s="T258">v-v:tense-v:pn</ta>
            <ta e="T260" id="Seg_10190" s="T259">adj-n:case</ta>
            <ta e="T261" id="Seg_10191" s="T260">n-n:case</ta>
            <ta e="T262" id="Seg_10192" s="T261">ptcl</ta>
            <ta e="T263" id="Seg_10193" s="T262">v-v:n.fin</ta>
            <ta e="T265" id="Seg_10194" s="T264">adv</ta>
            <ta e="T266" id="Seg_10195" s="T265">propr-n:case</ta>
            <ta e="T267" id="Seg_10196" s="T266">v-v:tense-v:pn</ta>
            <ta e="T268" id="Seg_10197" s="T267">n-n:case</ta>
            <ta e="T269" id="Seg_10198" s="T268">v-v:mood.pn</ta>
            <ta e="T271" id="Seg_10199" s="T270">v-v&gt;v-v:pn</ta>
            <ta e="T272" id="Seg_10200" s="T271">dempro-n:case</ta>
            <ta e="T273" id="Seg_10201" s="T272">dempro-n:case</ta>
            <ta e="T274" id="Seg_10202" s="T273">v-v:tense-v:pn</ta>
            <ta e="T275" id="Seg_10203" s="T274">ptcl</ta>
            <ta e="T276" id="Seg_10204" s="T275">n-n:case.poss</ta>
            <ta e="T277" id="Seg_10205" s="T276">n-n:case</ta>
            <ta e="T278" id="Seg_10206" s="T277">v-v:tense-v:pn</ta>
            <ta e="T279" id="Seg_10207" s="T278">v-v:tense-v:pn</ta>
            <ta e="T280" id="Seg_10208" s="T279">v-v:tense-v:pn</ta>
            <ta e="T281" id="Seg_10209" s="T280">v-v:tense-v:pn</ta>
            <ta e="T282" id="Seg_10210" s="T281">conj</ta>
            <ta e="T283" id="Seg_10211" s="T282">adv</ta>
            <ta e="T285" id="Seg_10212" s="T284">n-n:case</ta>
            <ta e="T286" id="Seg_10213" s="T285">v-v:tense-v:pn</ta>
            <ta e="T287" id="Seg_10214" s="T286">v-v:tense-v:pn</ta>
            <ta e="T288" id="Seg_10215" s="T287">n-n:case</ta>
            <ta e="T289" id="Seg_10216" s="T288">conj</ta>
            <ta e="T290" id="Seg_10217" s="T289">v-v&gt;v-v:pn</ta>
            <ta e="T291" id="Seg_10218" s="T290">n-n:case</ta>
            <ta e="T292" id="Seg_10219" s="T291">v-v&gt;v-v:pn</ta>
            <ta e="T293" id="Seg_10220" s="T292">ptcl</ta>
            <ta e="T294" id="Seg_10221" s="T293">n-n:case</ta>
            <ta e="T295" id="Seg_10222" s="T294">v-v&gt;v-v:pn</ta>
            <ta e="T296" id="Seg_10223" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_10224" s="T296">n-n:case</ta>
            <ta e="T298" id="Seg_10225" s="T297">dempro-n:case</ta>
            <ta e="T299" id="Seg_10226" s="T298">v-v:tense-v:pn</ta>
            <ta e="T300" id="Seg_10227" s="T299">ptcl</ta>
            <ta e="T301" id="Seg_10228" s="T300">adv</ta>
            <ta e="T302" id="Seg_10229" s="T301">v-v&gt;v-v:pn</ta>
            <ta e="T303" id="Seg_10230" s="T302">n-n:case</ta>
            <ta e="T304" id="Seg_10231" s="T303">v-v:ins-v:mood.pn</ta>
            <ta e="T305" id="Seg_10232" s="T304">conj</ta>
            <ta e="T306" id="Seg_10233" s="T305">adv</ta>
            <ta e="T307" id="Seg_10234" s="T306">n-n:case</ta>
            <ta e="T308" id="Seg_10235" s="T307">v-v:ins-v:mood.pn</ta>
            <ta e="T309" id="Seg_10236" s="T308">adv</ta>
            <ta e="T310" id="Seg_10237" s="T309">dempro-n:case</ta>
            <ta e="T311" id="Seg_10238" s="T310">%%</ta>
            <ta e="T312" id="Seg_10239" s="T311">v-v:tense-v:pn</ta>
            <ta e="T314" id="Seg_10240" s="T313">n-n:case</ta>
            <ta e="T315" id="Seg_10241" s="T314">v-v:tense-v:pn</ta>
            <ta e="T316" id="Seg_10242" s="T315">v-v:n.fin</ta>
            <ta e="T317" id="Seg_10243" s="T316">conj</ta>
            <ta e="T318" id="Seg_10244" s="T317">adj-n:case</ta>
            <ta e="T319" id="Seg_10245" s="T318">n</ta>
            <ta e="T320" id="Seg_10246" s="T319">v-v:tense-v:pn</ta>
            <ta e="T322" id="Seg_10247" s="T321">n-n:case</ta>
            <ta e="T323" id="Seg_10248" s="T322">adj-n:case</ta>
            <ta e="T324" id="Seg_10249" s="T323">n-n:case</ta>
            <ta e="T325" id="Seg_10250" s="T324">v-v:tense-v:pn</ta>
            <ta e="T327" id="Seg_10251" s="T326">n-n:case</ta>
            <ta e="T328" id="Seg_10252" s="T327">n-n:case</ta>
            <ta e="T329" id="Seg_10253" s="T328">n</ta>
            <ta e="T330" id="Seg_10254" s="T329">n-n:case</ta>
            <ta e="T332" id="Seg_10255" s="T331">n-n:case</ta>
            <ta e="T333" id="Seg_10256" s="T332">n-n:case</ta>
            <ta e="T334" id="Seg_10257" s="T333">n-n:case</ta>
            <ta e="T335" id="Seg_10258" s="T334">n-n:case</ta>
            <ta e="T340" id="Seg_10259" s="T339">n-n:case</ta>
            <ta e="T342" id="Seg_10260" s="T341">n-n:num</ta>
            <ta e="T343" id="Seg_10261" s="T342">adv</ta>
            <ta e="T344" id="Seg_10262" s="T343">quant</ta>
            <ta e="T345" id="Seg_10263" s="T344">n-n:case</ta>
            <ta e="T346" id="Seg_10264" s="T345">v-v:tense-v:pn</ta>
            <ta e="T347" id="Seg_10265" s="T346">ptcl</ta>
            <ta e="T348" id="Seg_10266" s="T347">ptcl</ta>
            <ta e="T349" id="Seg_10267" s="T348">adv</ta>
            <ta e="T350" id="Seg_10268" s="T349">n-n:case</ta>
            <ta e="T351" id="Seg_10269" s="T350">v-v:tense-v:pn</ta>
            <ta e="T353" id="Seg_10270" s="T352">ptcl</ta>
            <ta e="T354" id="Seg_10271" s="T353">v-v:tense-v:pn</ta>
            <ta e="T355" id="Seg_10272" s="T354">adj</ta>
            <ta e="T356" id="Seg_10273" s="T355">n</ta>
            <ta e="T357" id="Seg_10274" s="T356">adj</ta>
            <ta e="T358" id="Seg_10275" s="T357">n-n:case</ta>
            <ta e="T359" id="Seg_10276" s="T358">conj</ta>
            <ta e="T361" id="Seg_10277" s="T360">n-n:case</ta>
            <ta e="T362" id="Seg_10278" s="T361">pers</ta>
            <ta e="T364" id="Seg_10279" s="T363">que</ta>
            <ta e="T365" id="Seg_10280" s="T364">v-v:tense-v:pn</ta>
            <ta e="T366" id="Seg_10281" s="T365">adv</ta>
            <ta e="T367" id="Seg_10282" s="T366">n-n:case</ta>
            <ta e="T368" id="Seg_10283" s="T367">v-v:tense-v:pn</ta>
            <ta e="T369" id="Seg_10284" s="T368">ptcl</ta>
            <ta e="T370" id="Seg_10285" s="T369">adv</ta>
            <ta e="T371" id="Seg_10286" s="T370">v-v:tense-v:pn</ta>
            <ta e="T372" id="Seg_10287" s="T371">ptcl</ta>
            <ta e="T375" id="Seg_10288" s="T374">adj</ta>
            <ta e="T376" id="Seg_10289" s="T375">v-v:tense-v:pn</ta>
            <ta e="T377" id="Seg_10290" s="T376">n-n:case</ta>
            <ta e="T378" id="Seg_10291" s="T377">v-v:tense-v:pn</ta>
            <ta e="T379" id="Seg_10292" s="T378">adj-n:case</ta>
            <ta e="T380" id="Seg_10293" s="T379">n-n:case.poss</ta>
            <ta e="T381" id="Seg_10294" s="T380">v-v:tense-v:pn</ta>
            <ta e="T382" id="Seg_10295" s="T381">conj</ta>
            <ta e="T383" id="Seg_10296" s="T382">adj-n:case</ta>
            <ta e="T384" id="Seg_10297" s="T383">adj-n:case</ta>
            <ta e="T385" id="Seg_10298" s="T384">v-v:tense-v:pn</ta>
            <ta e="T386" id="Seg_10299" s="T385">adv</ta>
            <ta e="T387" id="Seg_10300" s="T386">adv</ta>
            <ta e="T388" id="Seg_10301" s="T387">adj-n:case</ta>
            <ta e="T390" id="Seg_10302" s="T389">pers</ta>
            <ta e="T391" id="Seg_10303" s="T390">n-n:case</ta>
            <ta e="T392" id="Seg_10304" s="T391">v-v:tense-v:pn</ta>
            <ta e="T393" id="Seg_10305" s="T392">quant</ta>
            <ta e="T394" id="Seg_10306" s="T393">n-n:case</ta>
            <ta e="T395" id="Seg_10307" s="T394">v-v:tense-v:pn</ta>
            <ta e="T396" id="Seg_10308" s="T395">adv</ta>
            <ta e="T398" id="Seg_10309" s="T396">v-v:tense-v:pn</ta>
            <ta e="T405" id="Seg_10310" s="T404">adv</ta>
            <ta e="T406" id="Seg_10311" s="T405">n</ta>
            <ta e="T407" id="Seg_10312" s="T406">v-v:tense-v:pn</ta>
            <ta e="T408" id="Seg_10313" s="T407">n-n:case</ta>
            <ta e="T409" id="Seg_10314" s="T408">v-v:pn</ta>
            <ta e="T410" id="Seg_10315" s="T409">conj</ta>
            <ta e="T411" id="Seg_10316" s="T410">pers</ta>
            <ta e="T412" id="Seg_10317" s="T411">v-v:tense-v:pn</ta>
            <ta e="T413" id="Seg_10318" s="T412">conj</ta>
            <ta e="T414" id="Seg_10319" s="T413">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T415" id="Seg_10320" s="T414">conj</ta>
            <ta e="T416" id="Seg_10321" s="T415">pers</ta>
            <ta e="T986" id="Seg_10322" s="T416">v-v:n-fin</ta>
            <ta e="T417" id="Seg_10323" s="T986">v-v:tense-v:pn</ta>
            <ta e="T418" id="Seg_10324" s="T417">dempro-n:case</ta>
            <ta e="T419" id="Seg_10325" s="T418">ptcl</ta>
            <ta e="T420" id="Seg_10326" s="T419">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T421" id="Seg_10327" s="T420">conj</ta>
            <ta e="T422" id="Seg_10328" s="T421">n-n:case.poss</ta>
            <ta e="T423" id="Seg_10329" s="T422">v-v:tense-v:pn</ta>
            <ta e="T425" id="Seg_10330" s="T424">pers</ta>
            <ta e="T427" id="Seg_10331" s="T426">n-n:case.poss</ta>
            <ta e="T428" id="Seg_10332" s="T427">n-n:case.poss</ta>
            <ta e="T429" id="Seg_10333" s="T428">v-v:tense-v:pn</ta>
            <ta e="T430" id="Seg_10334" s="T429">n-n:case</ta>
            <ta e="T431" id="Seg_10335" s="T430">adv</ta>
            <ta e="T432" id="Seg_10336" s="T431">n-n:case</ta>
            <ta e="T433" id="Seg_10337" s="T432">v-v:tense-v:pn</ta>
            <ta e="T434" id="Seg_10338" s="T433">n-n:num</ta>
            <ta e="T435" id="Seg_10339" s="T434">v-v:n.fin</ta>
            <ta e="T436" id="Seg_10340" s="T435">adv</ta>
            <ta e="T437" id="Seg_10341" s="T436">v-v:tense-v:pn</ta>
            <ta e="T438" id="Seg_10342" s="T437">n-n:case.poss</ta>
            <ta e="T439" id="Seg_10343" s="T438">conj</ta>
            <ta e="T440" id="Seg_10344" s="T439">adv</ta>
            <ta e="T441" id="Seg_10345" s="T440">n-n:num</ta>
            <ta e="T442" id="Seg_10346" s="T441">ptcl</ta>
            <ta e="T443" id="Seg_10347" s="T442">v-v:tense-v:pn</ta>
            <ta e="T444" id="Seg_10348" s="T443">conj</ta>
            <ta e="T445" id="Seg_10349" s="T444">v-v:tense-v:pn</ta>
            <ta e="T447" id="Seg_10350" s="T446">pers</ta>
            <ta e="T448" id="Seg_10351" s="T447">n-n:case</ta>
            <ta e="T449" id="Seg_10352" s="T448">n</ta>
            <ta e="T450" id="Seg_10353" s="T449">n-n:case.poss</ta>
            <ta e="T451" id="Seg_10354" s="T450">v-v:tense-v:pn</ta>
            <ta e="T452" id="Seg_10355" s="T451">%%</ta>
            <ta e="T453" id="Seg_10356" s="T452">v-v:tense-v:pn</ta>
            <ta e="T454" id="Seg_10357" s="T453">n-n:case</ta>
            <ta e="T455" id="Seg_10358" s="T454">conj</ta>
            <ta e="T456" id="Seg_10359" s="T455">n-n:case</ta>
            <ta e="T457" id="Seg_10360" s="T456">adv</ta>
            <ta e="T458" id="Seg_10361" s="T457">n</ta>
            <ta e="T459" id="Seg_10362" s="T458">pers</ta>
            <ta e="T460" id="Seg_10363" s="T459">n-n:case</ta>
            <ta e="T461" id="Seg_10364" s="T460">v-v&gt;v-v:pn</ta>
            <ta e="T462" id="Seg_10365" s="T461">v-v:mood.pn</ta>
            <ta e="T463" id="Seg_10366" s="T462">n-n:case</ta>
            <ta e="T464" id="Seg_10367" s="T463">v-v:pn</ta>
            <ta e="T466" id="Seg_10368" s="T465">v-v:pn</ta>
            <ta e="T467" id="Seg_10369" s="T466">dempro-n:num</ta>
            <ta e="T468" id="Seg_10370" s="T467">v-v:tense-v:pn</ta>
            <ta e="T469" id="Seg_10371" s="T468">dempro-n:case</ta>
            <ta e="T470" id="Seg_10372" s="T469">n-n:case</ta>
            <ta e="T471" id="Seg_10373" s="T470">v-v:tense-v:pn</ta>
            <ta e="T472" id="Seg_10374" s="T471">num-n:case</ta>
            <ta e="T473" id="Seg_10375" s="T472">num-n:case</ta>
            <ta e="T475" id="Seg_10376" s="T474">dempro-n:num</ta>
            <ta e="T476" id="Seg_10377" s="T475">v-v:tense-v:pn</ta>
            <ta e="T478" id="Seg_10378" s="T477">adv</ta>
            <ta e="T479" id="Seg_10379" s="T478">n-n:case</ta>
            <ta e="T480" id="Seg_10380" s="T479">dempro-n:num</ta>
            <ta e="T482" id="Seg_10381" s="T481">v-v:tense-v:pn</ta>
            <ta e="T483" id="Seg_10382" s="T482">v-v:tense-v:pn</ta>
            <ta e="T484" id="Seg_10383" s="T483">%%</ta>
            <ta e="T485" id="Seg_10384" s="T484">v-v:tense-v:pn</ta>
            <ta e="T486" id="Seg_10385" s="T485">adv</ta>
            <ta e="T487" id="Seg_10386" s="T486">v-v:tense-v:pn</ta>
            <ta e="T488" id="Seg_10387" s="T487">v-v:n.fin</ta>
            <ta e="T489" id="Seg_10388" s="T488">conj</ta>
            <ta e="T490" id="Seg_10389" s="T489">pers</ta>
            <ta e="T491" id="Seg_10390" s="T490">n-n:case.poss</ta>
            <ta e="T493" id="Seg_10391" s="T492">v-v&gt;v-v:pn</ta>
            <ta e="T494" id="Seg_10392" s="T493">ptcl</ta>
            <ta e="T496" id="Seg_10393" s="T494">adv</ta>
            <ta e="T497" id="Seg_10394" s="T496">ptcl</ta>
            <ta e="T498" id="Seg_10395" s="T497">n-n:case</ta>
            <ta e="T499" id="Seg_10396" s="T498">v-v:n.fin</ta>
            <ta e="T500" id="Seg_10397" s="T499">dempro</ta>
            <ta e="T501" id="Seg_10398" s="T500">n-n:case</ta>
            <ta e="T502" id="Seg_10399" s="T501">adv</ta>
            <ta e="T503" id="Seg_10400" s="T502">v-v:tense-v:pn</ta>
            <ta e="T504" id="Seg_10401" s="T503">refl-n:case.poss</ta>
            <ta e="T505" id="Seg_10402" s="T504">v-v:tense-v:pn</ta>
            <ta e="T506" id="Seg_10403" s="T505">adv</ta>
            <ta e="T507" id="Seg_10404" s="T506">n-n:case</ta>
            <ta e="T508" id="Seg_10405" s="T507">v-v:tense-v:pn</ta>
            <ta e="T509" id="Seg_10406" s="T508">quant</ta>
            <ta e="T510" id="Seg_10407" s="T509">v-v:tense-v:pn</ta>
            <ta e="T511" id="Seg_10408" s="T510">v-v:tense-v:pn</ta>
            <ta e="T512" id="Seg_10409" s="T511">n-n:case.poss</ta>
            <ta e="T513" id="Seg_10410" s="T512">adv</ta>
            <ta e="T514" id="Seg_10411" s="T513">dempro-n:case</ta>
            <ta e="T515" id="Seg_10412" s="T514">v-v:tense-v:pn</ta>
            <ta e="T516" id="Seg_10413" s="T515">n-n:case.poss-n:case</ta>
            <ta e="T517" id="Seg_10414" s="T516">dempro-n:case</ta>
            <ta e="T518" id="Seg_10415" s="T517">v-v:tense-v:pn</ta>
            <ta e="T519" id="Seg_10416" s="T518">n-n:case.poss-n:case</ta>
            <ta e="T520" id="Seg_10417" s="T519">v-v:tense-v:pn</ta>
            <ta e="T521" id="Seg_10418" s="T520">n-n:case</ta>
            <ta e="T522" id="Seg_10419" s="T521">ptcl</ta>
            <ta e="T523" id="Seg_10420" s="T522">v-v:tense-v:pn</ta>
            <ta e="T525" id="Seg_10421" s="T524">pers</ta>
            <ta e="T527" id="Seg_10422" s="T526">adv</ta>
            <ta e="T528" id="Seg_10423" s="T527">n</ta>
            <ta e="T529" id="Seg_10424" s="T528">v-v:tense-v:pn</ta>
            <ta e="T530" id="Seg_10425" s="T529">n-n:case</ta>
            <ta e="T531" id="Seg_10426" s="T530">v-v:tense-v:pn</ta>
            <ta e="T532" id="Seg_10427" s="T531">adv</ta>
            <ta e="T533" id="Seg_10428" s="T532">n-n:case</ta>
            <ta e="T534" id="Seg_10429" s="T533">n-n:case</ta>
            <ta e="T535" id="Seg_10430" s="T534">v-v:tense-v:pn</ta>
            <ta e="T536" id="Seg_10431" s="T535">n-n:case</ta>
            <ta e="T537" id="Seg_10432" s="T536">v-v:tense-v:pn</ta>
            <ta e="T538" id="Seg_10433" s="T537">v-v:tense-v:pn</ta>
            <ta e="T539" id="Seg_10434" s="T538">n-n:case</ta>
            <ta e="T540" id="Seg_10435" s="T539">v-v:tense-v:pn</ta>
            <ta e="T541" id="Seg_10436" s="T540">n-n:case.poss</ta>
            <ta e="T542" id="Seg_10437" s="T541">v-v:tense-v:pn</ta>
            <ta e="T543" id="Seg_10438" s="T542">conj</ta>
            <ta e="T544" id="Seg_10439" s="T543">adv</ta>
            <ta e="T545" id="Seg_10440" s="T544">v-v:tense-v:pn</ta>
            <ta e="T546" id="Seg_10441" s="T545">n-n:case</ta>
            <ta e="T547" id="Seg_10442" s="T546">n-n:case</ta>
            <ta e="T550" id="Seg_10443" s="T549">n-n:case</ta>
            <ta e="T551" id="Seg_10444" s="T550">v-v:tense-v:pn</ta>
            <ta e="T552" id="Seg_10445" s="T551">adv</ta>
            <ta e="T553" id="Seg_10446" s="T552">v-v:tense-v:pn</ta>
            <ta e="T554" id="Seg_10447" s="T553">n-n:case</ta>
            <ta e="T555" id="Seg_10448" s="T554">n-n:case</ta>
            <ta e="T556" id="Seg_10449" s="T555">n-n:case</ta>
            <ta e="T557" id="Seg_10450" s="T556">v-v:tense-v:pn</ta>
            <ta e="T558" id="Seg_10451" s="T557">dempro-n:case</ta>
            <ta e="T559" id="Seg_10452" s="T558">ptcl</ta>
            <ta e="T561" id="Seg_10453" s="T560">v-v&gt;v-v:pn</ta>
            <ta e="T562" id="Seg_10454" s="T561">n-n:case</ta>
            <ta e="T563" id="Seg_10455" s="T562">que-n:case</ta>
            <ta e="T564" id="Seg_10456" s="T563">n-n:case.poss</ta>
            <ta e="T565" id="Seg_10457" s="T564">v-v:tense-v:pn</ta>
            <ta e="T566" id="Seg_10458" s="T565">ptcl</ta>
            <ta e="T567" id="Seg_10459" s="T566">n-n:num</ta>
            <ta e="T568" id="Seg_10460" s="T567">v-v:tense-v:pn</ta>
            <ta e="T569" id="Seg_10461" s="T568">adv</ta>
            <ta e="T570" id="Seg_10462" s="T569">v-v:tense-v:pn</ta>
            <ta e="T571" id="Seg_10463" s="T570">adv</ta>
            <ta e="T572" id="Seg_10464" s="T571">n-n:case</ta>
            <ta e="T573" id="Seg_10465" s="T572">ptcl</ta>
            <ta e="T574" id="Seg_10466" s="T573">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T575" id="Seg_10467" s="T574">pers</ta>
            <ta e="T576" id="Seg_10468" s="T575">v-v&gt;v-v:pn</ta>
            <ta e="T577" id="Seg_10469" s="T576">aux-v:mood.pn</ta>
            <ta e="T578" id="Seg_10470" s="T577">v-v:mood.pn</ta>
            <ta e="T581" id="Seg_10471" s="T580">conj</ta>
            <ta e="T582" id="Seg_10472" s="T581">n-n:case.poss</ta>
            <ta e="T583" id="Seg_10473" s="T582">ptcl</ta>
            <ta e="T584" id="Seg_10474" s="T583">%%-v&gt;v-v:tense-v:pn</ta>
            <ta e="T585" id="Seg_10475" s="T584">adv</ta>
            <ta e="T586" id="Seg_10476" s="T585">ptcl</ta>
            <ta e="T587" id="Seg_10477" s="T586">v-v:tense-v:pn</ta>
            <ta e="T589" id="Seg_10478" s="T588">adv</ta>
            <ta e="T590" id="Seg_10479" s="T589">dempro</ta>
            <ta e="T591" id="Seg_10480" s="T590">n-n:case.poss</ta>
            <ta e="T593" id="Seg_10481" s="T592">v-v:tense-v:pn</ta>
            <ta e="T594" id="Seg_10482" s="T593">propr-n:case</ta>
            <ta e="T595" id="Seg_10483" s="T594">adv</ta>
            <ta e="T596" id="Seg_10484" s="T595">ptcl</ta>
            <ta e="T597" id="Seg_10485" s="T596">v-v:tense-v:pn</ta>
            <ta e="T598" id="Seg_10486" s="T597">v-v:tense-v:pn</ta>
            <ta e="T599" id="Seg_10487" s="T598">adv</ta>
            <ta e="T600" id="Seg_10488" s="T599">dempro-n:case</ta>
            <ta e="T601" id="Seg_10489" s="T600">dempro-n:num</ta>
            <ta e="T602" id="Seg_10490" s="T601">ptcl</ta>
            <ta e="T603" id="Seg_10491" s="T602">adv</ta>
            <ta e="T604" id="Seg_10492" s="T603">n-n:case.poss-n:case</ta>
            <ta e="T605" id="Seg_10493" s="T604">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T607" id="Seg_10494" s="T606">adv</ta>
            <ta e="T608" id="Seg_10495" s="T607">dempro</ta>
            <ta e="T609" id="Seg_10496" s="T608">propr-n:case</ta>
            <ta e="T610" id="Seg_10497" s="T609">ptcl</ta>
            <ta e="T611" id="Seg_10498" s="T610">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T613" id="Seg_10499" s="T612">n-n:case.poss</ta>
            <ta e="T614" id="Seg_10500" s="T613">conj</ta>
            <ta e="T615" id="Seg_10501" s="T614">n-n:case.poss</ta>
            <ta e="T616" id="Seg_10502" s="T615">ptcl</ta>
            <ta e="T617" id="Seg_10503" s="T616">dempro-n:case</ta>
            <ta e="T618" id="Seg_10504" s="T617">que=ptcl</ta>
            <ta e="T619" id="Seg_10505" s="T618">v-v:tense-v:pn</ta>
            <ta e="T620" id="Seg_10506" s="T619">pers</ta>
            <ta e="T621" id="Seg_10507" s="T620">v-v&gt;v-v:pn</ta>
            <ta e="T622" id="Seg_10508" s="T621">aux-v:mood.pn</ta>
            <ta e="T623" id="Seg_10509" s="T622">v-v:ins-v:mood.pn</ta>
            <ta e="T625" id="Seg_10510" s="T624">n-n:case</ta>
            <ta e="T626" id="Seg_10511" s="T625">que-n:case=ptcl</ta>
            <ta e="T627" id="Seg_10512" s="T626">ptcl</ta>
            <ta e="T628" id="Seg_10513" s="T627">v-v:ins-v:n.fin</ta>
            <ta e="T629" id="Seg_10514" s="T628">conj</ta>
            <ta e="T630" id="Seg_10515" s="T629">dempro-n:case</ta>
            <ta e="T631" id="Seg_10516" s="T630">adv</ta>
            <ta e="T632" id="Seg_10517" s="T631">v-v:tense-v:pn</ta>
            <ta e="T633" id="Seg_10518" s="T632">conj</ta>
            <ta e="T634" id="Seg_10519" s="T633">n-n:case.poss</ta>
            <ta e="T635" id="Seg_10520" s="T634">ptcl</ta>
            <ta e="T636" id="Seg_10521" s="T635">ptcl</ta>
            <ta e="T637" id="Seg_10522" s="T636">v-v:n.fin</ta>
            <ta e="T638" id="Seg_10523" s="T637">v-v:tense-v:pn</ta>
            <ta e="T639" id="Seg_10524" s="T638">que</ta>
            <ta e="T640" id="Seg_10525" s="T639">dempro-n:num</ta>
            <ta e="T641" id="Seg_10526" s="T640">n-n:case</ta>
            <ta e="T642" id="Seg_10527" s="T641">pers</ta>
            <ta e="T643" id="Seg_10528" s="T642">v-v:tense-v:pn</ta>
            <ta e="T644" id="Seg_10529" s="T643">pers</ta>
            <ta e="T645" id="Seg_10530" s="T644">n-n:case</ta>
            <ta e="T646" id="Seg_10531" s="T645">v-v:tense-v:pn</ta>
            <ta e="T647" id="Seg_10532" s="T646">conj</ta>
            <ta e="T648" id="Seg_10533" s="T647">adv</ta>
            <ta e="T649" id="Seg_10534" s="T648">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T650" id="Seg_10535" s="T649">adv</ta>
            <ta e="T652" id="Seg_10536" s="T651">%%</ta>
            <ta e="T653" id="Seg_10537" s="T652">v-v:tense-v:pn</ta>
            <ta e="T654" id="Seg_10538" s="T653">adv</ta>
            <ta e="T655" id="Seg_10539" s="T654">propr-n:case</ta>
            <ta e="T656" id="Seg_10540" s="T655">v-v:tense-v:pn</ta>
            <ta e="T657" id="Seg_10541" s="T656">ptcl</ta>
            <ta e="T658" id="Seg_10542" s="T657">v-v:tense-v:pn</ta>
            <ta e="T659" id="Seg_10543" s="T658">ptcl</ta>
            <ta e="T660" id="Seg_10544" s="T659">v-v:tense-v:pn</ta>
            <ta e="T661" id="Seg_10545" s="T660">que-n:case</ta>
            <ta e="T662" id="Seg_10546" s="T661">v-v:tense-v:pn</ta>
            <ta e="T663" id="Seg_10547" s="T662">n-n:case</ta>
            <ta e="T664" id="Seg_10548" s="T663">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T665" id="Seg_10549" s="T664">conj</ta>
            <ta e="T666" id="Seg_10550" s="T665">pers</ta>
            <ta e="T667" id="Seg_10551" s="T666">propr-n:case</ta>
            <ta e="T668" id="Seg_10552" s="T667">v-v:tense-v:pn</ta>
            <ta e="T669" id="Seg_10553" s="T668">v-v:mood.pn</ta>
            <ta e="T670" id="Seg_10554" s="T669">dempro-n:case</ta>
            <ta e="T671" id="Seg_10555" s="T670">num-n:case</ta>
            <ta e="T672" id="Seg_10556" s="T671">n-n:case</ta>
            <ta e="T673" id="Seg_10557" s="T672">dempro-n:case</ta>
            <ta e="T674" id="Seg_10558" s="T673">v-v:tense-v:pn</ta>
            <ta e="T675" id="Seg_10559" s="T674">dempro-n:case</ta>
            <ta e="T676" id="Seg_10560" s="T675">adv</ta>
            <ta e="T677" id="Seg_10561" s="T676">v-v:n.fin</ta>
            <ta e="T678" id="Seg_10562" s="T677">conj</ta>
            <ta e="T679" id="Seg_10563" s="T678">pers</ta>
            <ta e="T680" id="Seg_10564" s="T679">v-v&gt;v-v:pn</ta>
            <ta e="T681" id="Seg_10565" s="T680">%%</ta>
            <ta e="T682" id="Seg_10566" s="T681">ptcl</ta>
            <ta e="T683" id="Seg_10567" s="T682">quant</ta>
            <ta e="T684" id="Seg_10568" s="T683">ptcl</ta>
            <ta e="T685" id="Seg_10569" s="T684">n-n:case</ta>
            <ta e="T686" id="Seg_10570" s="T685">adv</ta>
            <ta e="T687" id="Seg_10571" s="T686">adv</ta>
            <ta e="T688" id="Seg_10572" s="T687">dempro-n:case</ta>
            <ta e="T689" id="Seg_10573" s="T688">dempro-n:case</ta>
            <ta e="T690" id="Seg_10574" s="T689">v-v:tense-v:pn</ta>
            <ta e="T691" id="Seg_10575" s="T690">num-n:case</ta>
            <ta e="T692" id="Seg_10576" s="T691">num-n:case</ta>
            <ta e="T693" id="Seg_10577" s="T692">n-n:case.poss</ta>
            <ta e="T695" id="Seg_10578" s="T694">adv</ta>
            <ta e="T696" id="Seg_10579" s="T695">pers</ta>
            <ta e="T697" id="Seg_10580" s="T696">v-v:tense-v:pn</ta>
            <ta e="T698" id="Seg_10581" s="T697">n-n:case.poss</ta>
            <ta e="T699" id="Seg_10582" s="T698">v-%%-v:tense-v:pn</ta>
            <ta e="T700" id="Seg_10583" s="T699">n-n:case</ta>
            <ta e="T701" id="Seg_10584" s="T700">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T702" id="Seg_10585" s="T701">n-n:case</ta>
            <ta e="T703" id="Seg_10586" s="T702">v-v:tense-v:pn</ta>
            <ta e="T704" id="Seg_10587" s="T703">adv</ta>
            <ta e="T705" id="Seg_10588" s="T704">n-n&gt;v-v:n.fin</ta>
            <ta e="T706" id="Seg_10589" s="T705">v-v:tense-v:pn</ta>
            <ta e="T707" id="Seg_10590" s="T706">conj</ta>
            <ta e="T709" id="Seg_10591" s="T708">v-v:tense-v:pn</ta>
            <ta e="T710" id="Seg_10592" s="T709">n-n:case</ta>
            <ta e="T711" id="Seg_10593" s="T710">n-n:case.poss</ta>
            <ta e="T712" id="Seg_10594" s="T711">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T713" id="Seg_10595" s="T712">n-n:case</ta>
            <ta e="T716" id="Seg_10596" s="T715">v-v:tense-v:pn</ta>
            <ta e="T717" id="Seg_10597" s="T716">n-n:case</ta>
            <ta e="T719" id="Seg_10598" s="T718">v-v:tense-v:pn</ta>
            <ta e="T720" id="Seg_10599" s="T719">adv</ta>
            <ta e="T721" id="Seg_10600" s="T720">n-n:case.poss</ta>
            <ta e="T723" id="Seg_10601" s="T722">%%</ta>
            <ta e="T724" id="Seg_10602" s="T723">dempro-n:num</ta>
            <ta e="T726" id="Seg_10603" s="T725">v-v:tense-v:pn</ta>
            <ta e="T727" id="Seg_10604" s="T726">conj</ta>
            <ta e="T728" id="Seg_10605" s="T727">v-v:n.fin</ta>
            <ta e="T730" id="Seg_10606" s="T729">adv</ta>
            <ta e="T731" id="Seg_10607" s="T730">v-v:n.fin</ta>
            <ta e="T732" id="Seg_10608" s="T731">ptcl</ta>
            <ta e="T733" id="Seg_10609" s="T732">conj</ta>
            <ta e="T734" id="Seg_10610" s="T733">adv</ta>
            <ta e="T735" id="Seg_10611" s="T734">n-n:case</ta>
            <ta e="T736" id="Seg_10612" s="T735">ptcl</ta>
            <ta e="T737" id="Seg_10613" s="T736">v-v&gt;v-v:pn</ta>
            <ta e="T738" id="Seg_10614" s="T737">ptcl</ta>
            <ta e="T739" id="Seg_10615" s="T738">v-v:tense-v:pn</ta>
            <ta e="T740" id="Seg_10616" s="T739">v-v:n.fin</ta>
            <ta e="T741" id="Seg_10617" s="T740">pers</ta>
            <ta e="T742" id="Seg_10618" s="T741">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T743" id="Seg_10619" s="T742">conj</ta>
            <ta e="T744" id="Seg_10620" s="T743">dempro-n:num</ta>
            <ta e="T987" id="Seg_10621" s="T744">v-v:n-fin</ta>
            <ta e="T745" id="Seg_10622" s="T987">v-v:tense-v:pn</ta>
            <ta e="T746" id="Seg_10623" s="T745">ptcl</ta>
            <ta e="T747" id="Seg_10624" s="T746">v-v:tense-v:pn</ta>
            <ta e="T748" id="Seg_10625" s="T747">conj</ta>
            <ta e="T749" id="Seg_10626" s="T748">adv</ta>
            <ta e="T750" id="Seg_10627" s="T749">v-v:tense-v:pn</ta>
            <ta e="T751" id="Seg_10628" s="T750">adv</ta>
            <ta e="T753" id="Seg_10629" s="T752">v-v:n.fin</ta>
            <ta e="T755" id="Seg_10630" s="T754">v-v:tense-v:pn</ta>
            <ta e="T756" id="Seg_10631" s="T755">conj</ta>
            <ta e="T757" id="Seg_10632" s="T756">pers</ta>
            <ta e="T758" id="Seg_10633" s="T757">v-v&gt;v-v:pn</ta>
            <ta e="T759" id="Seg_10634" s="T758">que</ta>
            <ta e="T760" id="Seg_10635" s="T759">v-v:tense-v:pn</ta>
            <ta e="T761" id="Seg_10636" s="T760">ptcl</ta>
            <ta e="T762" id="Seg_10637" s="T761">n-n:case</ta>
            <ta e="T763" id="Seg_10638" s="T762">v-v:tense-v:pn</ta>
            <ta e="T764" id="Seg_10639" s="T763">refl-n:case.poss</ta>
            <ta e="T765" id="Seg_10640" s="T764">n-n:case</ta>
            <ta e="T766" id="Seg_10641" s="T765">v-v:tense-v:pn</ta>
            <ta e="T767" id="Seg_10642" s="T766">conj</ta>
            <ta e="T768" id="Seg_10643" s="T767">n-n:case</ta>
            <ta e="T769" id="Seg_10644" s="T768">n-n:case</ta>
            <ta e="T771" id="Seg_10645" s="T770">v-v:tense-v:pn</ta>
            <ta e="T772" id="Seg_10646" s="T771">conj</ta>
            <ta e="T773" id="Seg_10647" s="T772">adv</ta>
            <ta e="T774" id="Seg_10648" s="T773">v-v:tense-v:pn</ta>
            <ta e="T776" id="Seg_10649" s="T775">adv</ta>
            <ta e="T777" id="Seg_10650" s="T776">n-n:case.poss</ta>
            <ta e="T778" id="Seg_10651" s="T777">v-v:tense-v:pn</ta>
            <ta e="T779" id="Seg_10652" s="T778">n-n:case</ta>
            <ta e="T780" id="Seg_10653" s="T779">ptcl</ta>
            <ta e="T781" id="Seg_10654" s="T780">n-n:case</ta>
            <ta e="T783" id="Seg_10655" s="T782">que-n:case=ptcl</ta>
            <ta e="T784" id="Seg_10656" s="T783">ptcl</ta>
            <ta e="T785" id="Seg_10657" s="T784">v-v:tense-v:pn</ta>
            <ta e="T786" id="Seg_10658" s="T785">ptcl</ta>
            <ta e="T787" id="Seg_10659" s="T786">n-n:num</ta>
            <ta e="T788" id="Seg_10660" s="T787">v-v:ins-v:mood.pn</ta>
            <ta e="T789" id="Seg_10661" s="T788">n-n:case</ta>
            <ta e="T790" id="Seg_10662" s="T789">ptcl</ta>
            <ta e="T791" id="Seg_10663" s="T790">n-n:case.poss</ta>
            <ta e="T792" id="Seg_10664" s="T791">v-v:tense-v:pn</ta>
            <ta e="T793" id="Seg_10665" s="T792">que-n:case=ptcl</ta>
            <ta e="T794" id="Seg_10666" s="T793">ptcl</ta>
            <ta e="T795" id="Seg_10667" s="T794">v-v:tense-v:pn</ta>
            <ta e="T796" id="Seg_10668" s="T795">dempro-n:case</ta>
            <ta e="T797" id="Seg_10669" s="T796">v-v:tense-v:pn</ta>
            <ta e="T798" id="Seg_10670" s="T797">conj</ta>
            <ta e="T799" id="Seg_10671" s="T798">ptcl</ta>
            <ta e="T800" id="Seg_10672" s="T799">v-v:n.fin</ta>
            <ta e="T801" id="Seg_10673" s="T800">adv</ta>
            <ta e="T802" id="Seg_10674" s="T801">v-v:tense-v:pn</ta>
            <ta e="T803" id="Seg_10675" s="T802">n-n:case</ta>
            <ta e="T804" id="Seg_10676" s="T803">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T805" id="Seg_10677" s="T804">adv</ta>
            <ta e="T806" id="Seg_10678" s="T805">pers</ta>
            <ta e="T807" id="Seg_10679" s="T806">v-v&gt;v-v:pn</ta>
            <ta e="T808" id="Seg_10680" s="T807">v-v:ins-v:mood.pn</ta>
            <ta e="T809" id="Seg_10681" s="T808">n-n:case</ta>
            <ta e="T810" id="Seg_10682" s="T809">pers</ta>
            <ta e="T811" id="Seg_10683" s="T810">v-v:tense-v:pn</ta>
            <ta e="T812" id="Seg_10684" s="T811">v-v&gt;v-v:n.fin</ta>
            <ta e="T813" id="Seg_10685" s="T812">adv</ta>
            <ta e="T814" id="Seg_10686" s="T813">propr-n:case</ta>
            <ta e="T815" id="Seg_10687" s="T814">v-v:tense-v:pn</ta>
            <ta e="T816" id="Seg_10688" s="T815">v-v&gt;v-v:n.fin</ta>
            <ta e="T817" id="Seg_10689" s="T816">adv</ta>
            <ta e="T818" id="Seg_10690" s="T817">n-n:case</ta>
            <ta e="T819" id="Seg_10691" s="T818">v-v:tense-v:pn</ta>
            <ta e="T820" id="Seg_10692" s="T819">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T821" id="Seg_10693" s="T820">adv</ta>
            <ta e="T822" id="Seg_10694" s="T821">n-n:case.poss</ta>
            <ta e="T823" id="Seg_10695" s="T822">v-v:tense-v:pn</ta>
            <ta e="T824" id="Seg_10696" s="T823">v-v:tense-v:pn</ta>
            <ta e="T825" id="Seg_10697" s="T824">n-n:case.poss</ta>
            <ta e="T826" id="Seg_10698" s="T825">n-n:num</ta>
            <ta e="T827" id="Seg_10699" s="T826">v-v:tense-v:pn</ta>
            <ta e="T829" id="Seg_10700" s="T828">adv</ta>
            <ta e="T830" id="Seg_10701" s="T829">v-v:tense-v:pn</ta>
            <ta e="T831" id="Seg_10702" s="T830">n-n:case</ta>
            <ta e="T832" id="Seg_10703" s="T831">v-v:tense-v:pn</ta>
            <ta e="T833" id="Seg_10704" s="T832">num-n:case</ta>
            <ta e="T834" id="Seg_10705" s="T833">n-n:case</ta>
            <ta e="T835" id="Seg_10706" s="T834">dempro-n:case</ta>
            <ta e="T836" id="Seg_10707" s="T835">v-v:tense-v:pn</ta>
            <ta e="T837" id="Seg_10708" s="T836">n-n:case.poss-n:case</ta>
            <ta e="T838" id="Seg_10709" s="T837">adv</ta>
            <ta e="T839" id="Seg_10710" s="T838">dempro-n:case</ta>
            <ta e="T840" id="Seg_10711" s="T839">dempro-n:case</ta>
            <ta e="T841" id="Seg_10712" s="T840">n-n:case</ta>
            <ta e="T842" id="Seg_10713" s="T841">v-v:tense-v:pn</ta>
            <ta e="T843" id="Seg_10714" s="T842">%%</ta>
            <ta e="T844" id="Seg_10715" s="T843">v-v:tense-v:pn</ta>
            <ta e="T846" id="Seg_10716" s="T845">v-v:tense-v:pn</ta>
            <ta e="T847" id="Seg_10717" s="T846">n-n:case.poss</ta>
            <ta e="T849" id="Seg_10718" s="T848">dempro-n:case</ta>
            <ta e="T850" id="Seg_10719" s="T849">n-n:case</ta>
            <ta e="T851" id="Seg_10720" s="T850">adv</ta>
            <ta e="T852" id="Seg_10721" s="T851">adj-n:case</ta>
            <ta e="T853" id="Seg_10722" s="T852">n-n:case</ta>
            <ta e="T854" id="Seg_10723" s="T853">v-v:tense-v:pn</ta>
            <ta e="T855" id="Seg_10724" s="T854">conj</ta>
            <ta e="T856" id="Seg_10725" s="T855">n-n:case</ta>
            <ta e="T858" id="Seg_10726" s="T857">n-n:case</ta>
            <ta e="T859" id="Seg_10727" s="T858">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T860" id="Seg_10728" s="T859">n-n:case.poss-n:case</ta>
            <ta e="T861" id="Seg_10729" s="T860">n-n:case.poss</ta>
            <ta e="T862" id="Seg_10730" s="T861">ptcl</ta>
            <ta e="T863" id="Seg_10731" s="T862">v-v&gt;v-v:pn</ta>
            <ta e="T864" id="Seg_10732" s="T863">conj</ta>
            <ta e="T865" id="Seg_10733" s="T864">pers</ta>
            <ta e="T866" id="Seg_10734" s="T865">refl-n:case.poss</ta>
            <ta e="T867" id="Seg_10735" s="T866">n-n:case.poss</ta>
            <ta e="T868" id="Seg_10736" s="T867">ptcl</ta>
            <ta e="T869" id="Seg_10737" s="T868">v-v:tense-v:pn</ta>
            <ta e="T870" id="Seg_10738" s="T869">n-n:case</ta>
            <ta e="T871" id="Seg_10739" s="T870">n-n:case</ta>
            <ta e="T872" id="Seg_10740" s="T871">v-v:tense-v:pn</ta>
            <ta e="T873" id="Seg_10741" s="T872">%%</ta>
            <ta e="T874" id="Seg_10742" s="T873">ptcl</ta>
            <ta e="T875" id="Seg_10743" s="T874">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T877" id="Seg_10744" s="T876">pers</ta>
            <ta e="T878" id="Seg_10745" s="T877">n-n:case</ta>
            <ta e="T879" id="Seg_10746" s="T878">v-v:tense-v:pn</ta>
            <ta e="T880" id="Seg_10747" s="T879">n-n:case</ta>
            <ta e="T882" id="Seg_10748" s="T880">conj</ta>
            <ta e="T883" id="Seg_10749" s="T882">dempro-n:num</ta>
            <ta e="T884" id="Seg_10750" s="T883">num-n:case</ta>
            <ta e="T885" id="Seg_10751" s="T884">num-n:case</ta>
            <ta e="T886" id="Seg_10752" s="T885">num-n:case</ta>
            <ta e="T887" id="Seg_10753" s="T886">num-n:case</ta>
            <ta e="T888" id="Seg_10754" s="T887">adv</ta>
            <ta e="T889" id="Seg_10755" s="T888">propr.[n:case]</ta>
            <ta e="T890" id="Seg_10756" s="T889">v-v&gt;v-v:pn</ta>
            <ta e="T891" id="Seg_10757" s="T890">n-n:case</ta>
            <ta e="T892" id="Seg_10758" s="T891">adv</ta>
            <ta e="T893" id="Seg_10759" s="T892">conj</ta>
            <ta e="T894" id="Seg_10760" s="T893">n-n:num</ta>
            <ta e="T895" id="Seg_10761" s="T894">conj</ta>
            <ta e="T896" id="Seg_10762" s="T895">pers</ta>
            <ta e="T897" id="Seg_10763" s="T896">ptcl</ta>
            <ta e="T898" id="Seg_10764" s="T897">v-v:tense-v:pn</ta>
            <ta e="T900" id="Seg_10765" s="T899">adv</ta>
            <ta e="T901" id="Seg_10766" s="T900">ptcl</ta>
            <ta e="T902" id="Seg_10767" s="T901">quant</ta>
            <ta e="T903" id="Seg_10768" s="T902">v-v:tense-v:pn</ta>
            <ta e="T904" id="Seg_10769" s="T903">adv</ta>
            <ta e="T905" id="Seg_10770" s="T904">ptcl</ta>
            <ta e="T906" id="Seg_10771" s="T905">n-n:case.poss</ta>
            <ta e="T907" id="Seg_10772" s="T906">v-v:n.fin</ta>
            <ta e="T908" id="Seg_10773" s="T907">v-v:n.fin</ta>
            <ta e="T909" id="Seg_10774" s="T908">v-v:n.fin</ta>
            <ta e="T911" id="Seg_10775" s="T910">%%</ta>
            <ta e="T913" id="Seg_10776" s="T912">n-n:case</ta>
            <ta e="T914" id="Seg_10777" s="T913">ptcl</ta>
            <ta e="T915" id="Seg_10778" s="T914">adv</ta>
            <ta e="T916" id="Seg_10779" s="T915">v-v:tense-v:pn</ta>
            <ta e="T917" id="Seg_10780" s="T916">adv</ta>
            <ta e="T918" id="Seg_10781" s="T917">adj-n:case</ta>
            <ta e="T919" id="Seg_10782" s="T918">v-v:tense-v:pn</ta>
            <ta e="T920" id="Seg_10783" s="T919">adv</ta>
            <ta e="T921" id="Seg_10784" s="T920">n-n:case</ta>
            <ta e="T922" id="Seg_10785" s="T921">n-n:case</ta>
            <ta e="T923" id="Seg_10786" s="T922">n-adv:case</ta>
            <ta e="T924" id="Seg_10787" s="T923">v-v:tense-v:pn</ta>
            <ta e="T925" id="Seg_10788" s="T924">ptcl</ta>
            <ta e="T926" id="Seg_10789" s="T925">adj-n:case</ta>
            <ta e="T927" id="Seg_10790" s="T926">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T929" id="Seg_10791" s="T928">pers</ta>
            <ta e="T930" id="Seg_10792" s="T929">adv</ta>
            <ta e="T931" id="Seg_10793" s="T930">ptcl</ta>
            <ta e="T932" id="Seg_10794" s="T931">v-v:n.fin</ta>
            <ta e="T933" id="Seg_10795" s="T932">propr-n:case</ta>
            <ta e="T934" id="Seg_10796" s="T933">conj</ta>
            <ta e="T935" id="Seg_10797" s="T934">dempro-n:case</ta>
            <ta e="T936" id="Seg_10798" s="T935">n-n:case</ta>
            <ta e="T937" id="Seg_10799" s="T936">v-v:tense-v:pn</ta>
            <ta e="T938" id="Seg_10800" s="T937">propr-n:case</ta>
            <ta e="T939" id="Seg_10801" s="T938">conj</ta>
            <ta e="T940" id="Seg_10802" s="T939">v-v:tense-v:pn</ta>
            <ta e="T941" id="Seg_10803" s="T940">pers</ta>
            <ta e="T944" id="Seg_10804" s="T943">ptcl</ta>
            <ta e="T945" id="Seg_10805" s="T944">n-n:case</ta>
            <ta e="T946" id="Seg_10806" s="T945">v-v:n.fin</ta>
            <ta e="T947" id="Seg_10807" s="T946">dempro-n:case</ta>
            <ta e="T948" id="Seg_10808" s="T947">ptcl</ta>
            <ta e="T949" id="Seg_10809" s="T948">refl-n:case.poss</ta>
            <ta e="T950" id="Seg_10810" s="T949">v-v:n.fin</ta>
            <ta e="T951" id="Seg_10811" s="T950">adv</ta>
            <ta e="T952" id="Seg_10812" s="T951">n-n:case.poss-n:case</ta>
            <ta e="T953" id="Seg_10813" s="T952">v-v:tense-v:pn</ta>
            <ta e="T954" id="Seg_10814" s="T953">conj</ta>
            <ta e="T955" id="Seg_10815" s="T954">v-v:tense-v:pn</ta>
            <ta e="T957" id="Seg_10816" s="T956">ptcl</ta>
            <ta e="T958" id="Seg_10817" s="T957">v-v:pn</ta>
            <ta e="T959" id="Seg_10818" s="T958">v-v:tense-v:pn</ta>
            <ta e="T960" id="Seg_10819" s="T959">conj</ta>
            <ta e="T961" id="Seg_10820" s="T960">ptcl</ta>
            <ta e="T962" id="Seg_10821" s="T961">ptcl</ta>
            <ta e="T963" id="Seg_10822" s="T962">v-v:n.fin</ta>
            <ta e="T964" id="Seg_10823" s="T963">dempro-n:case</ta>
            <ta e="T965" id="Seg_10824" s="T964">v-v:n.fin</ta>
            <ta e="T966" id="Seg_10825" s="T965">ptcl</ta>
            <ta e="T967" id="Seg_10826" s="T966">v-v:tense-v:pn</ta>
            <ta e="T968" id="Seg_10827" s="T967">ptcl</ta>
            <ta e="T969" id="Seg_10828" s="T968">v-v:ins-v:mood.pn</ta>
            <ta e="T970" id="Seg_10829" s="T969">v-v:ins-v:mood.pn</ta>
            <ta e="T972" id="Seg_10830" s="T971">dempro-n:case</ta>
            <ta e="T973" id="Seg_10831" s="T972">ptcl</ta>
            <ta e="T974" id="Seg_10832" s="T973">v-v:tense-v:pn</ta>
            <ta e="T975" id="Seg_10833" s="T974">n-n:case</ta>
            <ta e="T976" id="Seg_10834" s="T975">adv</ta>
            <ta e="T978" id="Seg_10835" s="T977">v-v:n.fin</ta>
            <ta e="T979" id="Seg_10836" s="T978">ptcl</ta>
            <ta e="T980" id="Seg_10837" s="T979">n-n:case</ta>
            <ta e="T981" id="Seg_10838" s="T980">v-v:n.fin</ta>
            <ta e="T982" id="Seg_10839" s="T981">pers</ta>
            <ta e="T983" id="Seg_10840" s="T982">ptcl</ta>
            <ta e="T984" id="Seg_10841" s="T983">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_10842" s="T0">v</ta>
            <ta e="T2" id="Seg_10843" s="T1">n</ta>
            <ta e="T3" id="Seg_10844" s="T2">adv</ta>
            <ta e="T4" id="Seg_10845" s="T3">num</ta>
            <ta e="T5" id="Seg_10846" s="T4">dempro</ta>
            <ta e="T6" id="Seg_10847" s="T5">pers</ta>
            <ta e="T7" id="Seg_10848" s="T6">n</ta>
            <ta e="T8" id="Seg_10849" s="T7">v</ta>
            <ta e="T9" id="Seg_10850" s="T8">n</ta>
            <ta e="T10" id="Seg_10851" s="T9">n</ta>
            <ta e="T11" id="Seg_10852" s="T10">conj</ta>
            <ta e="T12" id="Seg_10853" s="T11">pers</ta>
            <ta e="T13" id="Seg_10854" s="T12">dempro</ta>
            <ta e="T14" id="Seg_10855" s="T13">ptcl</ta>
            <ta e="T16" id="Seg_10856" s="T15">v</ta>
            <ta e="T17" id="Seg_10857" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_10858" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_10859" s="T18">v</ta>
            <ta e="T20" id="Seg_10860" s="T19">adv</ta>
            <ta e="T21" id="Seg_10861" s="T20">v</ta>
            <ta e="T22" id="Seg_10862" s="T21">n</ta>
            <ta e="T23" id="Seg_10863" s="T22">n</ta>
            <ta e="T24" id="Seg_10864" s="T23">n</ta>
            <ta e="T25" id="Seg_10865" s="T24">ptcl</ta>
            <ta e="T27" id="Seg_10866" s="T26">ptcl</ta>
            <ta e="T28" id="Seg_10867" s="T27">v</ta>
            <ta e="T29" id="Seg_10868" s="T28">adv</ta>
            <ta e="T30" id="Seg_10869" s="T29">n</ta>
            <ta e="T31" id="Seg_10870" s="T30">n</ta>
            <ta e="T32" id="Seg_10871" s="T31">n</ta>
            <ta e="T33" id="Seg_10872" s="T32">num</ta>
            <ta e="T35" id="Seg_10873" s="T34">v</ta>
            <ta e="T36" id="Seg_10874" s="T35">conj</ta>
            <ta e="T37" id="Seg_10875" s="T36">n</ta>
            <ta e="T38" id="Seg_10876" s="T37">adv</ta>
            <ta e="T39" id="Seg_10877" s="T38">v</ta>
            <ta e="T40" id="Seg_10878" s="T39">adv</ta>
            <ta e="T41" id="Seg_10879" s="T40">num</ta>
            <ta e="T42" id="Seg_10880" s="T41">v</ta>
            <ta e="T43" id="Seg_10881" s="T42">n</ta>
            <ta e="T45" id="Seg_10882" s="T44">pers</ta>
            <ta e="T46" id="Seg_10883" s="T45">n</ta>
            <ta e="T47" id="Seg_10884" s="T46">n</ta>
            <ta e="T48" id="Seg_10885" s="T47">num</ta>
            <ta e="T49" id="Seg_10886" s="T48">v</ta>
            <ta e="T50" id="Seg_10887" s="T49">adv</ta>
            <ta e="T51" id="Seg_10888" s="T50">n</ta>
            <ta e="T52" id="Seg_10889" s="T51">v</ta>
            <ta e="T53" id="Seg_10890" s="T52">num</ta>
            <ta e="T54" id="Seg_10891" s="T53">n</ta>
            <ta e="T55" id="Seg_10892" s="T54">num</ta>
            <ta e="T56" id="Seg_10893" s="T55">n</ta>
            <ta e="T57" id="Seg_10894" s="T56">n</ta>
            <ta e="T58" id="Seg_10895" s="T57">dempro</ta>
            <ta e="T59" id="Seg_10896" s="T58">dempro</ta>
            <ta e="T61" id="Seg_10897" s="T60">v</ta>
            <ta e="T62" id="Seg_10898" s="T61">adv</ta>
            <ta e="T63" id="Seg_10899" s="T62">n</ta>
            <ta e="T64" id="Seg_10900" s="T63">v</ta>
            <ta e="T65" id="Seg_10901" s="T64">conj</ta>
            <ta e="T66" id="Seg_10902" s="T65">adv</ta>
            <ta e="T67" id="Seg_10903" s="T66">pers</ta>
            <ta e="T68" id="Seg_10904" s="T67">v</ta>
            <ta e="T69" id="Seg_10905" s="T68">v</ta>
            <ta e="T71" id="Seg_10906" s="T70">pers</ta>
            <ta e="T72" id="Seg_10907" s="T71">n</ta>
            <ta e="T73" id="Seg_10908" s="T72">v</ta>
            <ta e="T988" id="Seg_10909" s="T73">propr</ta>
            <ta e="T74" id="Seg_10910" s="T988">n</ta>
            <ta e="T75" id="Seg_10911" s="T74">adv</ta>
            <ta e="T76" id="Seg_10912" s="T75">ptcl</ta>
            <ta e="T77" id="Seg_10913" s="T76">n</ta>
            <ta e="T78" id="Seg_10914" s="T77">n</ta>
            <ta e="T79" id="Seg_10915" s="T78">v</ta>
            <ta e="T80" id="Seg_10916" s="T79">quant</ta>
            <ta e="T81" id="Seg_10917" s="T80">v</ta>
            <ta e="T82" id="Seg_10918" s="T81">v</ta>
            <ta e="T83" id="Seg_10919" s="T82">v</ta>
            <ta e="T84" id="Seg_10920" s="T83">n</ta>
            <ta e="T85" id="Seg_10921" s="T84">conj</ta>
            <ta e="T86" id="Seg_10922" s="T85">pers</ta>
            <ta e="T87" id="Seg_10923" s="T86">conj</ta>
            <ta e="T88" id="Seg_10924" s="T87">dempro</ta>
            <ta e="T89" id="Seg_10925" s="T88">v</ta>
            <ta e="T90" id="Seg_10926" s="T89">ptcl</ta>
            <ta e="T91" id="Seg_10927" s="T90">v</ta>
            <ta e="T92" id="Seg_10928" s="T91">que</ta>
            <ta e="T93" id="Seg_10929" s="T92">n</ta>
            <ta e="T94" id="Seg_10930" s="T93">v</ta>
            <ta e="T95" id="Seg_10931" s="T94">n</ta>
            <ta e="T96" id="Seg_10932" s="T95">v</ta>
            <ta e="T97" id="Seg_10933" s="T96">ptcl</ta>
            <ta e="T98" id="Seg_10934" s="T97">n</ta>
            <ta e="T99" id="Seg_10935" s="T98">n</ta>
            <ta e="T109" id="Seg_10936" s="T108">adv</ta>
            <ta e="T110" id="Seg_10937" s="T109">v</ta>
            <ta e="T111" id="Seg_10938" s="T110">v</ta>
            <ta e="T112" id="Seg_10939" s="T111">que</ta>
            <ta e="T113" id="Seg_10940" s="T112">v</ta>
            <ta e="T114" id="Seg_10941" s="T113">v</ta>
            <ta e="T115" id="Seg_10942" s="T114">ptcl</ta>
            <ta e="T116" id="Seg_10943" s="T115">v</ta>
            <ta e="T117" id="Seg_10944" s="T116">ptcl</ta>
            <ta e="T118" id="Seg_10945" s="T117">v</ta>
            <ta e="T120" id="Seg_10946" s="T119">adv</ta>
            <ta e="T122" id="Seg_10947" s="T121">v</ta>
            <ta e="T123" id="Seg_10948" s="T122">v</ta>
            <ta e="T124" id="Seg_10949" s="T123">n</ta>
            <ta e="T125" id="Seg_10950" s="T124">n</ta>
            <ta e="T126" id="Seg_10951" s="T125">v</ta>
            <ta e="T127" id="Seg_10952" s="T126">n</ta>
            <ta e="T128" id="Seg_10953" s="T127">pers</ta>
            <ta e="T129" id="Seg_10954" s="T128">v</ta>
            <ta e="T131" id="Seg_10955" s="T130">v</ta>
            <ta e="T132" id="Seg_10956" s="T131">n</ta>
            <ta e="T133" id="Seg_10957" s="T132">quant</ta>
            <ta e="T134" id="Seg_10958" s="T133">conj</ta>
            <ta e="T135" id="Seg_10959" s="T134">n</ta>
            <ta e="T136" id="Seg_10960" s="T135">v</ta>
            <ta e="T137" id="Seg_10961" s="T136">adv</ta>
            <ta e="T138" id="Seg_10962" s="T137">v</ta>
            <ta e="T139" id="Seg_10963" s="T138">que</ta>
            <ta e="T140" id="Seg_10964" s="T139">n</ta>
            <ta e="T141" id="Seg_10965" s="T140">ptcl</ta>
            <ta e="T143" id="Seg_10966" s="T142">v</ta>
            <ta e="T144" id="Seg_10967" s="T143">ptcl</ta>
            <ta e="T145" id="Seg_10968" s="T144">n</ta>
            <ta e="T146" id="Seg_10969" s="T145">quant</ta>
            <ta e="T147" id="Seg_10970" s="T146">conj</ta>
            <ta e="T148" id="Seg_10971" s="T147">n</ta>
            <ta e="T149" id="Seg_10972" s="T148">v</ta>
            <ta e="T150" id="Seg_10973" s="T149">dempro</ta>
            <ta e="T151" id="Seg_10974" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_10975" s="T151">v</ta>
            <ta e="T154" id="Seg_10976" s="T153">adv</ta>
            <ta e="T155" id="Seg_10977" s="T154">v</ta>
            <ta e="T156" id="Seg_10978" s="T155">v</ta>
            <ta e="T157" id="Seg_10979" s="T156">n</ta>
            <ta e="T158" id="Seg_10980" s="T157">v</ta>
            <ta e="T159" id="Seg_10981" s="T158">n</ta>
            <ta e="T160" id="Seg_10982" s="T159">pers</ta>
            <ta e="T161" id="Seg_10983" s="T160">v</ta>
            <ta e="T162" id="Seg_10984" s="T161">v</ta>
            <ta e="T163" id="Seg_10985" s="T162">v</ta>
            <ta e="T166" id="Seg_10986" s="T165">adv</ta>
            <ta e="T167" id="Seg_10987" s="T166">que</ta>
            <ta e="T168" id="Seg_10988" s="T167">adv</ta>
            <ta e="T170" id="Seg_10989" s="T169">num</ta>
            <ta e="T173" id="Seg_10990" s="T172">v</ta>
            <ta e="T174" id="Seg_10991" s="T173">conj</ta>
            <ta e="T175" id="Seg_10992" s="T174">v</ta>
            <ta e="T176" id="Seg_10993" s="T175">n</ta>
            <ta e="T177" id="Seg_10994" s="T176">num</ta>
            <ta e="T178" id="Seg_10995" s="T177">n</ta>
            <ta e="T179" id="Seg_10996" s="T178">n</ta>
            <ta e="T181" id="Seg_10997" s="T180">v</ta>
            <ta e="T182" id="Seg_10998" s="T181">num</ta>
            <ta e="T184" id="Seg_10999" s="T183">n</ta>
            <ta e="T185" id="Seg_11000" s="T184">adv</ta>
            <ta e="T186" id="Seg_11001" s="T185">adv</ta>
            <ta e="T188" id="Seg_11002" s="T187">pers</ta>
            <ta e="T189" id="Seg_11003" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_11004" s="T189">v</ta>
            <ta e="T192" id="Seg_11005" s="T191">adv</ta>
            <ta e="T194" id="Seg_11006" s="T193">dempro</ta>
            <ta e="T195" id="Seg_11007" s="T194">n</ta>
            <ta e="T196" id="Seg_11008" s="T195">n</ta>
            <ta e="T197" id="Seg_11009" s="T196">n</ta>
            <ta e="T198" id="Seg_11010" s="T197">v</ta>
            <ta e="T199" id="Seg_11011" s="T198">que</ta>
            <ta e="T200" id="Seg_11012" s="T199">pers</ta>
            <ta e="T201" id="Seg_11013" s="T200">v</ta>
            <ta e="T202" id="Seg_11014" s="T201">v</ta>
            <ta e="T203" id="Seg_11015" s="T202">conj</ta>
            <ta e="T204" id="Seg_11016" s="T203">v</ta>
            <ta e="T205" id="Seg_11017" s="T204">n</ta>
            <ta e="T206" id="Seg_11018" s="T205">n</ta>
            <ta e="T207" id="Seg_11019" s="T206">v</ta>
            <ta e="T208" id="Seg_11020" s="T207">v</ta>
            <ta e="T209" id="Seg_11021" s="T208">dempro</ta>
            <ta e="T210" id="Seg_11022" s="T209">num</ta>
            <ta e="T211" id="Seg_11023" s="T210">adv</ta>
            <ta e="T214" id="Seg_11024" s="T213">v</ta>
            <ta e="T215" id="Seg_11025" s="T214">conj</ta>
            <ta e="T216" id="Seg_11026" s="T215">que</ta>
            <ta e="T217" id="Seg_11027" s="T216">v</ta>
            <ta e="T218" id="Seg_11028" s="T217">n</ta>
            <ta e="T219" id="Seg_11029" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_11030" s="T219">v</ta>
            <ta e="T221" id="Seg_11031" s="T220">conj</ta>
            <ta e="T222" id="Seg_11032" s="T221">pers</ta>
            <ta e="T226" id="Seg_11033" s="T225">num</ta>
            <ta e="T227" id="Seg_11034" s="T226">n</ta>
            <ta e="T228" id="Seg_11035" s="T227">propr</ta>
            <ta e="T229" id="Seg_11036" s="T228">n</ta>
            <ta e="T230" id="Seg_11037" s="T229">v</ta>
            <ta e="T232" id="Seg_11038" s="T231">n</ta>
            <ta e="T234" id="Seg_11039" s="T233">v</ta>
            <ta e="T235" id="Seg_11040" s="T234">n</ta>
            <ta e="T236" id="Seg_11041" s="T235">conj</ta>
            <ta e="T237" id="Seg_11042" s="T236">v</ta>
            <ta e="T238" id="Seg_11043" s="T237">n</ta>
            <ta e="T239" id="Seg_11044" s="T238">v</ta>
            <ta e="T241" id="Seg_11045" s="T240">adv</ta>
            <ta e="T242" id="Seg_11046" s="T241">v</ta>
            <ta e="T243" id="Seg_11047" s="T242">dempro</ta>
            <ta e="T244" id="Seg_11048" s="T243">n</ta>
            <ta e="T245" id="Seg_11049" s="T244">ptcl</ta>
            <ta e="T246" id="Seg_11050" s="T245">v</ta>
            <ta e="T249" id="Seg_11051" s="T248">adj</ta>
            <ta e="T250" id="Seg_11052" s="T249">n</ta>
            <ta e="T251" id="Seg_11053" s="T250">ptcl</ta>
            <ta e="T252" id="Seg_11054" s="T251">v</ta>
            <ta e="T253" id="Seg_11055" s="T252">conj</ta>
            <ta e="T254" id="Seg_11056" s="T253">n</ta>
            <ta e="T257" id="Seg_11057" s="T256">adj</ta>
            <ta e="T258" id="Seg_11058" s="T257">ptcl</ta>
            <ta e="T259" id="Seg_11059" s="T258">v</ta>
            <ta e="T260" id="Seg_11060" s="T259">adj</ta>
            <ta e="T261" id="Seg_11061" s="T260">n</ta>
            <ta e="T262" id="Seg_11062" s="T261">ptcl</ta>
            <ta e="T263" id="Seg_11063" s="T262">v</ta>
            <ta e="T265" id="Seg_11064" s="T264">adv</ta>
            <ta e="T266" id="Seg_11065" s="T265">propr</ta>
            <ta e="T267" id="Seg_11066" s="T266">v</ta>
            <ta e="T268" id="Seg_11067" s="T267">n</ta>
            <ta e="T269" id="Seg_11068" s="T268">v</ta>
            <ta e="T271" id="Seg_11069" s="T270">v</ta>
            <ta e="T272" id="Seg_11070" s="T271">dempro</ta>
            <ta e="T273" id="Seg_11071" s="T272">dempro</ta>
            <ta e="T274" id="Seg_11072" s="T273">v</ta>
            <ta e="T275" id="Seg_11073" s="T274">ptcl</ta>
            <ta e="T276" id="Seg_11074" s="T275">n</ta>
            <ta e="T277" id="Seg_11075" s="T276">n</ta>
            <ta e="T278" id="Seg_11076" s="T277">v</ta>
            <ta e="T279" id="Seg_11077" s="T278">v</ta>
            <ta e="T280" id="Seg_11078" s="T279">v</ta>
            <ta e="T281" id="Seg_11079" s="T280">v</ta>
            <ta e="T282" id="Seg_11080" s="T281">conj</ta>
            <ta e="T283" id="Seg_11081" s="T282">adv</ta>
            <ta e="T285" id="Seg_11082" s="T284">n</ta>
            <ta e="T286" id="Seg_11083" s="T285">v</ta>
            <ta e="T287" id="Seg_11084" s="T286">v</ta>
            <ta e="T288" id="Seg_11085" s="T287">n</ta>
            <ta e="T289" id="Seg_11086" s="T288">conj</ta>
            <ta e="T290" id="Seg_11087" s="T289">v</ta>
            <ta e="T291" id="Seg_11088" s="T290">n</ta>
            <ta e="T292" id="Seg_11089" s="T291">v</ta>
            <ta e="T293" id="Seg_11090" s="T292">ptcl</ta>
            <ta e="T294" id="Seg_11091" s="T293">n</ta>
            <ta e="T295" id="Seg_11092" s="T294">v</ta>
            <ta e="T296" id="Seg_11093" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_11094" s="T296">n</ta>
            <ta e="T298" id="Seg_11095" s="T297">dempro</ta>
            <ta e="T299" id="Seg_11096" s="T298">v</ta>
            <ta e="T300" id="Seg_11097" s="T299">ptcl</ta>
            <ta e="T301" id="Seg_11098" s="T300">adv</ta>
            <ta e="T302" id="Seg_11099" s="T301">v</ta>
            <ta e="T303" id="Seg_11100" s="T302">n</ta>
            <ta e="T304" id="Seg_11101" s="T303">v</ta>
            <ta e="T305" id="Seg_11102" s="T304">conj</ta>
            <ta e="T306" id="Seg_11103" s="T305">adv</ta>
            <ta e="T307" id="Seg_11104" s="T306">n</ta>
            <ta e="T308" id="Seg_11105" s="T307">v</ta>
            <ta e="T309" id="Seg_11106" s="T308">adv</ta>
            <ta e="T310" id="Seg_11107" s="T309">dempro</ta>
            <ta e="T312" id="Seg_11108" s="T311">v</ta>
            <ta e="T314" id="Seg_11109" s="T313">n</ta>
            <ta e="T315" id="Seg_11110" s="T314">v</ta>
            <ta e="T316" id="Seg_11111" s="T315">v</ta>
            <ta e="T317" id="Seg_11112" s="T316">conj</ta>
            <ta e="T318" id="Seg_11113" s="T317">adj</ta>
            <ta e="T319" id="Seg_11114" s="T318">n</ta>
            <ta e="T320" id="Seg_11115" s="T319">v</ta>
            <ta e="T322" id="Seg_11116" s="T321">n</ta>
            <ta e="T323" id="Seg_11117" s="T322">adj</ta>
            <ta e="T324" id="Seg_11118" s="T323">n</ta>
            <ta e="T325" id="Seg_11119" s="T324">v</ta>
            <ta e="T327" id="Seg_11120" s="T326">n</ta>
            <ta e="T328" id="Seg_11121" s="T327">n</ta>
            <ta e="T329" id="Seg_11122" s="T328">n</ta>
            <ta e="T330" id="Seg_11123" s="T329">n</ta>
            <ta e="T332" id="Seg_11124" s="T331">n</ta>
            <ta e="T333" id="Seg_11125" s="T332">n</ta>
            <ta e="T334" id="Seg_11126" s="T333">n</ta>
            <ta e="T335" id="Seg_11127" s="T334">n</ta>
            <ta e="T340" id="Seg_11128" s="T339">n</ta>
            <ta e="T342" id="Seg_11129" s="T341">n</ta>
            <ta e="T343" id="Seg_11130" s="T342">adv</ta>
            <ta e="T344" id="Seg_11131" s="T343">quant</ta>
            <ta e="T345" id="Seg_11132" s="T344">n</ta>
            <ta e="T346" id="Seg_11133" s="T345">v</ta>
            <ta e="T347" id="Seg_11134" s="T346">ptcl</ta>
            <ta e="T348" id="Seg_11135" s="T347">ptcl</ta>
            <ta e="T349" id="Seg_11136" s="T348">adv</ta>
            <ta e="T350" id="Seg_11137" s="T349">n</ta>
            <ta e="T351" id="Seg_11138" s="T350">v</ta>
            <ta e="T353" id="Seg_11139" s="T352">ptcl</ta>
            <ta e="T354" id="Seg_11140" s="T353">v</ta>
            <ta e="T355" id="Seg_11141" s="T354">adj</ta>
            <ta e="T356" id="Seg_11142" s="T355">n</ta>
            <ta e="T357" id="Seg_11143" s="T356">adj</ta>
            <ta e="T358" id="Seg_11144" s="T357">n</ta>
            <ta e="T359" id="Seg_11145" s="T358">conj</ta>
            <ta e="T361" id="Seg_11146" s="T360">n</ta>
            <ta e="T362" id="Seg_11147" s="T361">pers</ta>
            <ta e="T364" id="Seg_11148" s="T363">que</ta>
            <ta e="T365" id="Seg_11149" s="T364">v</ta>
            <ta e="T366" id="Seg_11150" s="T365">adv</ta>
            <ta e="T367" id="Seg_11151" s="T366">n</ta>
            <ta e="T368" id="Seg_11152" s="T367">v</ta>
            <ta e="T369" id="Seg_11153" s="T368">ptcl</ta>
            <ta e="T370" id="Seg_11154" s="T369">adv</ta>
            <ta e="T371" id="Seg_11155" s="T370">v</ta>
            <ta e="T372" id="Seg_11156" s="T371">ptcl</ta>
            <ta e="T375" id="Seg_11157" s="T374">adj</ta>
            <ta e="T376" id="Seg_11158" s="T375">v</ta>
            <ta e="T377" id="Seg_11159" s="T376">n</ta>
            <ta e="T378" id="Seg_11160" s="T377">v</ta>
            <ta e="T379" id="Seg_11161" s="T378">adj</ta>
            <ta e="T380" id="Seg_11162" s="T379">n</ta>
            <ta e="T381" id="Seg_11163" s="T380">v</ta>
            <ta e="T382" id="Seg_11164" s="T381">conj</ta>
            <ta e="T383" id="Seg_11165" s="T382">adj</ta>
            <ta e="T384" id="Seg_11166" s="T383">adj</ta>
            <ta e="T385" id="Seg_11167" s="T384">v</ta>
            <ta e="T386" id="Seg_11168" s="T385">adv</ta>
            <ta e="T387" id="Seg_11169" s="T386">adv</ta>
            <ta e="T388" id="Seg_11170" s="T387">adj</ta>
            <ta e="T390" id="Seg_11171" s="T389">pers</ta>
            <ta e="T391" id="Seg_11172" s="T390">n</ta>
            <ta e="T392" id="Seg_11173" s="T391">v</ta>
            <ta e="T393" id="Seg_11174" s="T392">quant</ta>
            <ta e="T394" id="Seg_11175" s="T393">n</ta>
            <ta e="T395" id="Seg_11176" s="T394">v</ta>
            <ta e="T396" id="Seg_11177" s="T395">adv</ta>
            <ta e="T398" id="Seg_11178" s="T396">v</ta>
            <ta e="T405" id="Seg_11179" s="T404">adv</ta>
            <ta e="T406" id="Seg_11180" s="T405">n</ta>
            <ta e="T407" id="Seg_11181" s="T406">v</ta>
            <ta e="T408" id="Seg_11182" s="T407">n</ta>
            <ta e="T409" id="Seg_11183" s="T408">v</ta>
            <ta e="T410" id="Seg_11184" s="T409">conj</ta>
            <ta e="T411" id="Seg_11185" s="T410">pers</ta>
            <ta e="T412" id="Seg_11186" s="T411">v</ta>
            <ta e="T413" id="Seg_11187" s="T412">conj</ta>
            <ta e="T414" id="Seg_11188" s="T413">v</ta>
            <ta e="T415" id="Seg_11189" s="T414">conj</ta>
            <ta e="T416" id="Seg_11190" s="T415">pers</ta>
            <ta e="T986" id="Seg_11191" s="T416">v</ta>
            <ta e="T417" id="Seg_11192" s="T986">v</ta>
            <ta e="T418" id="Seg_11193" s="T417">dempro</ta>
            <ta e="T419" id="Seg_11194" s="T418">ptcl</ta>
            <ta e="T420" id="Seg_11195" s="T419">v</ta>
            <ta e="T421" id="Seg_11196" s="T420">conj</ta>
            <ta e="T422" id="Seg_11197" s="T421">n</ta>
            <ta e="T423" id="Seg_11198" s="T422">v</ta>
            <ta e="T425" id="Seg_11199" s="T424">pers</ta>
            <ta e="T427" id="Seg_11200" s="T426">n</ta>
            <ta e="T428" id="Seg_11201" s="T427">n</ta>
            <ta e="T429" id="Seg_11202" s="T428">v</ta>
            <ta e="T430" id="Seg_11203" s="T429">n</ta>
            <ta e="T431" id="Seg_11204" s="T430">adv</ta>
            <ta e="T432" id="Seg_11205" s="T431">n</ta>
            <ta e="T433" id="Seg_11206" s="T432">v</ta>
            <ta e="T434" id="Seg_11207" s="T433">n</ta>
            <ta e="T435" id="Seg_11208" s="T434">v</ta>
            <ta e="T436" id="Seg_11209" s="T435">adv</ta>
            <ta e="T437" id="Seg_11210" s="T436">v</ta>
            <ta e="T438" id="Seg_11211" s="T437">n</ta>
            <ta e="T439" id="Seg_11212" s="T438">conj</ta>
            <ta e="T440" id="Seg_11213" s="T439">adv</ta>
            <ta e="T441" id="Seg_11214" s="T440">n</ta>
            <ta e="T442" id="Seg_11215" s="T441">ptcl</ta>
            <ta e="T443" id="Seg_11216" s="T442">v</ta>
            <ta e="T444" id="Seg_11217" s="T443">conj</ta>
            <ta e="T445" id="Seg_11218" s="T444">v</ta>
            <ta e="T447" id="Seg_11219" s="T446">pers</ta>
            <ta e="T448" id="Seg_11220" s="T447">n</ta>
            <ta e="T449" id="Seg_11221" s="T448">n</ta>
            <ta e="T450" id="Seg_11222" s="T449">n</ta>
            <ta e="T451" id="Seg_11223" s="T450">v</ta>
            <ta e="T453" id="Seg_11224" s="T452">v</ta>
            <ta e="T454" id="Seg_11225" s="T453">n</ta>
            <ta e="T455" id="Seg_11226" s="T454">conj</ta>
            <ta e="T456" id="Seg_11227" s="T455">n</ta>
            <ta e="T457" id="Seg_11228" s="T456">adv</ta>
            <ta e="T458" id="Seg_11229" s="T457">n</ta>
            <ta e="T459" id="Seg_11230" s="T458">pers</ta>
            <ta e="T460" id="Seg_11231" s="T459">n</ta>
            <ta e="T461" id="Seg_11232" s="T460">v</ta>
            <ta e="T462" id="Seg_11233" s="T461">v</ta>
            <ta e="T463" id="Seg_11234" s="T462">n</ta>
            <ta e="T464" id="Seg_11235" s="T463">v</ta>
            <ta e="T466" id="Seg_11236" s="T465">v</ta>
            <ta e="T467" id="Seg_11237" s="T466">dempro</ta>
            <ta e="T468" id="Seg_11238" s="T467">v</ta>
            <ta e="T469" id="Seg_11239" s="T468">dempro</ta>
            <ta e="T470" id="Seg_11240" s="T469">n</ta>
            <ta e="T471" id="Seg_11241" s="T470">v</ta>
            <ta e="T472" id="Seg_11242" s="T471">num</ta>
            <ta e="T473" id="Seg_11243" s="T472">num</ta>
            <ta e="T475" id="Seg_11244" s="T474">dempro</ta>
            <ta e="T476" id="Seg_11245" s="T475">v</ta>
            <ta e="T478" id="Seg_11246" s="T477">adv</ta>
            <ta e="T479" id="Seg_11247" s="T478">n</ta>
            <ta e="T480" id="Seg_11248" s="T479">dempro</ta>
            <ta e="T482" id="Seg_11249" s="T481">v</ta>
            <ta e="T483" id="Seg_11250" s="T482">v</ta>
            <ta e="T484" id="Seg_11251" s="T483">v</ta>
            <ta e="T485" id="Seg_11252" s="T484">v</ta>
            <ta e="T486" id="Seg_11253" s="T485">adv</ta>
            <ta e="T487" id="Seg_11254" s="T486">v</ta>
            <ta e="T488" id="Seg_11255" s="T487">v</ta>
            <ta e="T489" id="Seg_11256" s="T488">conj</ta>
            <ta e="T490" id="Seg_11257" s="T489">pers</ta>
            <ta e="T491" id="Seg_11258" s="T490">n</ta>
            <ta e="T493" id="Seg_11259" s="T492">v</ta>
            <ta e="T494" id="Seg_11260" s="T493">ptcl</ta>
            <ta e="T496" id="Seg_11261" s="T494">adv</ta>
            <ta e="T497" id="Seg_11262" s="T496">ptcl</ta>
            <ta e="T498" id="Seg_11263" s="T497">n</ta>
            <ta e="T499" id="Seg_11264" s="T498">v</ta>
            <ta e="T500" id="Seg_11265" s="T499">dempro</ta>
            <ta e="T501" id="Seg_11266" s="T500">n</ta>
            <ta e="T502" id="Seg_11267" s="T501">adv</ta>
            <ta e="T503" id="Seg_11268" s="T502">v</ta>
            <ta e="T504" id="Seg_11269" s="T503">refl</ta>
            <ta e="T505" id="Seg_11270" s="T504">v</ta>
            <ta e="T506" id="Seg_11271" s="T505">adv</ta>
            <ta e="T507" id="Seg_11272" s="T506">n</ta>
            <ta e="T508" id="Seg_11273" s="T507">v</ta>
            <ta e="T509" id="Seg_11274" s="T508">quant</ta>
            <ta e="T510" id="Seg_11275" s="T509">v</ta>
            <ta e="T511" id="Seg_11276" s="T510">v</ta>
            <ta e="T512" id="Seg_11277" s="T511">n</ta>
            <ta e="T513" id="Seg_11278" s="T512">adv</ta>
            <ta e="T514" id="Seg_11279" s="T513">dempro</ta>
            <ta e="T515" id="Seg_11280" s="T514">v</ta>
            <ta e="T516" id="Seg_11281" s="T515">n</ta>
            <ta e="T517" id="Seg_11282" s="T516">dempro</ta>
            <ta e="T518" id="Seg_11283" s="T517">v</ta>
            <ta e="T519" id="Seg_11284" s="T518">n</ta>
            <ta e="T520" id="Seg_11285" s="T519">v</ta>
            <ta e="T521" id="Seg_11286" s="T520">n</ta>
            <ta e="T522" id="Seg_11287" s="T521">ptcl</ta>
            <ta e="T523" id="Seg_11288" s="T522">v</ta>
            <ta e="T525" id="Seg_11289" s="T524">pers</ta>
            <ta e="T527" id="Seg_11290" s="T526">adv</ta>
            <ta e="T528" id="Seg_11291" s="T527">n</ta>
            <ta e="T529" id="Seg_11292" s="T528">v</ta>
            <ta e="T530" id="Seg_11293" s="T529">n</ta>
            <ta e="T531" id="Seg_11294" s="T530">v</ta>
            <ta e="T532" id="Seg_11295" s="T531">adv</ta>
            <ta e="T533" id="Seg_11296" s="T532">n</ta>
            <ta e="T534" id="Seg_11297" s="T533">n</ta>
            <ta e="T535" id="Seg_11298" s="T534">v</ta>
            <ta e="T536" id="Seg_11299" s="T535">n</ta>
            <ta e="T538" id="Seg_11300" s="T537">v</ta>
            <ta e="T539" id="Seg_11301" s="T538">n</ta>
            <ta e="T540" id="Seg_11302" s="T539">v</ta>
            <ta e="T541" id="Seg_11303" s="T540">n</ta>
            <ta e="T542" id="Seg_11304" s="T541">v</ta>
            <ta e="T543" id="Seg_11305" s="T542">conj</ta>
            <ta e="T544" id="Seg_11306" s="T543">adv</ta>
            <ta e="T545" id="Seg_11307" s="T544">v</ta>
            <ta e="T546" id="Seg_11308" s="T545">n</ta>
            <ta e="T547" id="Seg_11309" s="T546">n</ta>
            <ta e="T550" id="Seg_11310" s="T549">n</ta>
            <ta e="T551" id="Seg_11311" s="T550">v</ta>
            <ta e="T552" id="Seg_11312" s="T551">adv</ta>
            <ta e="T553" id="Seg_11313" s="T552">v</ta>
            <ta e="T554" id="Seg_11314" s="T553">n</ta>
            <ta e="T555" id="Seg_11315" s="T554">n</ta>
            <ta e="T556" id="Seg_11316" s="T555">n</ta>
            <ta e="T557" id="Seg_11317" s="T556">v</ta>
            <ta e="T558" id="Seg_11318" s="T557">dempro</ta>
            <ta e="T559" id="Seg_11319" s="T558">ptcl</ta>
            <ta e="T561" id="Seg_11320" s="T560">v</ta>
            <ta e="T563" id="Seg_11321" s="T562">que</ta>
            <ta e="T564" id="Seg_11322" s="T563">n</ta>
            <ta e="T565" id="Seg_11323" s="T564">v</ta>
            <ta e="T566" id="Seg_11324" s="T565">ptcl</ta>
            <ta e="T567" id="Seg_11325" s="T566">n</ta>
            <ta e="T568" id="Seg_11326" s="T567">v</ta>
            <ta e="T569" id="Seg_11327" s="T568">adv</ta>
            <ta e="T570" id="Seg_11328" s="T569">v</ta>
            <ta e="T571" id="Seg_11329" s="T570">adv</ta>
            <ta e="T572" id="Seg_11330" s="T571">n</ta>
            <ta e="T573" id="Seg_11331" s="T572">ptcl</ta>
            <ta e="T574" id="Seg_11332" s="T573">v</ta>
            <ta e="T575" id="Seg_11333" s="T574">pers</ta>
            <ta e="T576" id="Seg_11334" s="T575">v</ta>
            <ta e="T577" id="Seg_11335" s="T576">aux</ta>
            <ta e="T578" id="Seg_11336" s="T577">v</ta>
            <ta e="T581" id="Seg_11337" s="T580">conj</ta>
            <ta e="T582" id="Seg_11338" s="T581">n</ta>
            <ta e="T583" id="Seg_11339" s="T582">ptcl</ta>
            <ta e="T584" id="Seg_11340" s="T583">v</ta>
            <ta e="T585" id="Seg_11341" s="T584">adv</ta>
            <ta e="T586" id="Seg_11342" s="T585">ptcl</ta>
            <ta e="T587" id="Seg_11343" s="T586">v</ta>
            <ta e="T589" id="Seg_11344" s="T588">adv</ta>
            <ta e="T590" id="Seg_11345" s="T589">dempro</ta>
            <ta e="T591" id="Seg_11346" s="T590">n</ta>
            <ta e="T593" id="Seg_11347" s="T592">v</ta>
            <ta e="T594" id="Seg_11348" s="T593">propr</ta>
            <ta e="T595" id="Seg_11349" s="T594">adv</ta>
            <ta e="T596" id="Seg_11350" s="T595">ptcl</ta>
            <ta e="T597" id="Seg_11351" s="T596">v</ta>
            <ta e="T598" id="Seg_11352" s="T597">v</ta>
            <ta e="T599" id="Seg_11353" s="T598">adv</ta>
            <ta e="T600" id="Seg_11354" s="T599">dempro</ta>
            <ta e="T601" id="Seg_11355" s="T600">dempro</ta>
            <ta e="T602" id="Seg_11356" s="T601">ptcl</ta>
            <ta e="T603" id="Seg_11357" s="T602">adv</ta>
            <ta e="T604" id="Seg_11358" s="T603">n</ta>
            <ta e="T605" id="Seg_11359" s="T604">v</ta>
            <ta e="T607" id="Seg_11360" s="T606">adv</ta>
            <ta e="T608" id="Seg_11361" s="T607">dempro</ta>
            <ta e="T609" id="Seg_11362" s="T608">propr</ta>
            <ta e="T610" id="Seg_11363" s="T609">ptcl</ta>
            <ta e="T611" id="Seg_11364" s="T610">v</ta>
            <ta e="T613" id="Seg_11365" s="T612">n</ta>
            <ta e="T614" id="Seg_11366" s="T613">conj</ta>
            <ta e="T615" id="Seg_11367" s="T614">n</ta>
            <ta e="T616" id="Seg_11368" s="T615">ptcl</ta>
            <ta e="T617" id="Seg_11369" s="T616">dempro</ta>
            <ta e="T618" id="Seg_11370" s="T617">que</ta>
            <ta e="T619" id="Seg_11371" s="T618">v</ta>
            <ta e="T620" id="Seg_11372" s="T619">pers</ta>
            <ta e="T621" id="Seg_11373" s="T620">v</ta>
            <ta e="T622" id="Seg_11374" s="T621">aux</ta>
            <ta e="T623" id="Seg_11375" s="T622">v</ta>
            <ta e="T625" id="Seg_11376" s="T624">n</ta>
            <ta e="T626" id="Seg_11377" s="T625">que</ta>
            <ta e="T627" id="Seg_11378" s="T626">ptcl</ta>
            <ta e="T628" id="Seg_11379" s="T627">v</ta>
            <ta e="T629" id="Seg_11380" s="T628">conj</ta>
            <ta e="T630" id="Seg_11381" s="T629">dempro</ta>
            <ta e="T631" id="Seg_11382" s="T630">adv</ta>
            <ta e="T632" id="Seg_11383" s="T631">v</ta>
            <ta e="T633" id="Seg_11384" s="T632">conj</ta>
            <ta e="T634" id="Seg_11385" s="T633">n</ta>
            <ta e="T635" id="Seg_11386" s="T634">ptcl</ta>
            <ta e="T636" id="Seg_11387" s="T635">ptcl</ta>
            <ta e="T637" id="Seg_11388" s="T636">v</ta>
            <ta e="T638" id="Seg_11389" s="T637">v</ta>
            <ta e="T639" id="Seg_11390" s="T638">que</ta>
            <ta e="T640" id="Seg_11391" s="T639">dempro</ta>
            <ta e="T641" id="Seg_11392" s="T640">n</ta>
            <ta e="T642" id="Seg_11393" s="T641">pers</ta>
            <ta e="T643" id="Seg_11394" s="T642">v</ta>
            <ta e="T644" id="Seg_11395" s="T643">pers</ta>
            <ta e="T645" id="Seg_11396" s="T644">n</ta>
            <ta e="T646" id="Seg_11397" s="T645">v</ta>
            <ta e="T647" id="Seg_11398" s="T646">conj</ta>
            <ta e="T648" id="Seg_11399" s="T647">adv</ta>
            <ta e="T649" id="Seg_11400" s="T648">v</ta>
            <ta e="T650" id="Seg_11401" s="T649">adv</ta>
            <ta e="T653" id="Seg_11402" s="T652">v</ta>
            <ta e="T654" id="Seg_11403" s="T653">adv</ta>
            <ta e="T655" id="Seg_11404" s="T654">propr</ta>
            <ta e="T656" id="Seg_11405" s="T655">v</ta>
            <ta e="T657" id="Seg_11406" s="T656">ptcl</ta>
            <ta e="T658" id="Seg_11407" s="T657">v</ta>
            <ta e="T659" id="Seg_11408" s="T658">ptcl</ta>
            <ta e="T660" id="Seg_11409" s="T659">v</ta>
            <ta e="T661" id="Seg_11410" s="T660">que</ta>
            <ta e="T662" id="Seg_11411" s="T661">v</ta>
            <ta e="T663" id="Seg_11412" s="T662">n</ta>
            <ta e="T664" id="Seg_11413" s="T663">v</ta>
            <ta e="T665" id="Seg_11414" s="T664">conj</ta>
            <ta e="T666" id="Seg_11415" s="T665">pers</ta>
            <ta e="T667" id="Seg_11416" s="T666">propr</ta>
            <ta e="T668" id="Seg_11417" s="T667">v</ta>
            <ta e="T669" id="Seg_11418" s="T668">pers</ta>
            <ta e="T670" id="Seg_11419" s="T669">dempro</ta>
            <ta e="T671" id="Seg_11420" s="T670">num</ta>
            <ta e="T672" id="Seg_11421" s="T671">n</ta>
            <ta e="T673" id="Seg_11422" s="T672">dempro</ta>
            <ta e="T674" id="Seg_11423" s="T673">v</ta>
            <ta e="T675" id="Seg_11424" s="T674">dempro</ta>
            <ta e="T676" id="Seg_11425" s="T675">adv</ta>
            <ta e="T677" id="Seg_11426" s="T676">v</ta>
            <ta e="T678" id="Seg_11427" s="T677">conj</ta>
            <ta e="T679" id="Seg_11428" s="T678">pers</ta>
            <ta e="T680" id="Seg_11429" s="T679">v</ta>
            <ta e="T682" id="Seg_11430" s="T681">ptcl</ta>
            <ta e="T683" id="Seg_11431" s="T682">quant</ta>
            <ta e="T684" id="Seg_11432" s="T683">ptcl</ta>
            <ta e="T685" id="Seg_11433" s="T684">n</ta>
            <ta e="T686" id="Seg_11434" s="T685">adv</ta>
            <ta e="T687" id="Seg_11435" s="T686">adv</ta>
            <ta e="T688" id="Seg_11436" s="T687">dempro</ta>
            <ta e="T689" id="Seg_11437" s="T688">dempro</ta>
            <ta e="T690" id="Seg_11438" s="T689">v</ta>
            <ta e="T691" id="Seg_11439" s="T690">num</ta>
            <ta e="T692" id="Seg_11440" s="T691">num</ta>
            <ta e="T693" id="Seg_11441" s="T692">n</ta>
            <ta e="T695" id="Seg_11442" s="T694">adv</ta>
            <ta e="T696" id="Seg_11443" s="T695">pers</ta>
            <ta e="T697" id="Seg_11444" s="T696">v</ta>
            <ta e="T698" id="Seg_11445" s="T697">n</ta>
            <ta e="T700" id="Seg_11446" s="T699">n</ta>
            <ta e="T701" id="Seg_11447" s="T700">v</ta>
            <ta e="T702" id="Seg_11448" s="T701">n</ta>
            <ta e="T703" id="Seg_11449" s="T702">v</ta>
            <ta e="T704" id="Seg_11450" s="T703">adv</ta>
            <ta e="T705" id="Seg_11451" s="T704">v</ta>
            <ta e="T706" id="Seg_11452" s="T705">v</ta>
            <ta e="T707" id="Seg_11453" s="T706">conj</ta>
            <ta e="T709" id="Seg_11454" s="T708">v</ta>
            <ta e="T710" id="Seg_11455" s="T709">n</ta>
            <ta e="T711" id="Seg_11456" s="T710">n</ta>
            <ta e="T712" id="Seg_11457" s="T711">v</ta>
            <ta e="T713" id="Seg_11458" s="T712">n</ta>
            <ta e="T716" id="Seg_11459" s="T715">v</ta>
            <ta e="T717" id="Seg_11460" s="T716">n</ta>
            <ta e="T719" id="Seg_11461" s="T718">v</ta>
            <ta e="T720" id="Seg_11462" s="T719">adv</ta>
            <ta e="T721" id="Seg_11463" s="T720">n</ta>
            <ta e="T724" id="Seg_11464" s="T723">dempro</ta>
            <ta e="T726" id="Seg_11465" s="T725">v</ta>
            <ta e="T727" id="Seg_11466" s="T726">conj</ta>
            <ta e="T728" id="Seg_11467" s="T727">v</ta>
            <ta e="T730" id="Seg_11468" s="T729">adv</ta>
            <ta e="T731" id="Seg_11469" s="T730">v</ta>
            <ta e="T732" id="Seg_11470" s="T731">ptcl</ta>
            <ta e="T733" id="Seg_11471" s="T732">conj</ta>
            <ta e="T734" id="Seg_11472" s="T733">adv</ta>
            <ta e="T735" id="Seg_11473" s="T734">n</ta>
            <ta e="T736" id="Seg_11474" s="T735">ptcl</ta>
            <ta e="T737" id="Seg_11475" s="T736">v</ta>
            <ta e="T738" id="Seg_11476" s="T737">ptcl</ta>
            <ta e="T739" id="Seg_11477" s="T738">v</ta>
            <ta e="T740" id="Seg_11478" s="T739">v</ta>
            <ta e="T741" id="Seg_11479" s="T740">pers</ta>
            <ta e="T742" id="Seg_11480" s="T741">v</ta>
            <ta e="T743" id="Seg_11481" s="T742">conj</ta>
            <ta e="T744" id="Seg_11482" s="T743">dempro</ta>
            <ta e="T987" id="Seg_11483" s="T744">v</ta>
            <ta e="T745" id="Seg_11484" s="T987">v</ta>
            <ta e="T746" id="Seg_11485" s="T745">ptcl</ta>
            <ta e="T747" id="Seg_11486" s="T746">v</ta>
            <ta e="T748" id="Seg_11487" s="T747">conj</ta>
            <ta e="T749" id="Seg_11488" s="T748">adv</ta>
            <ta e="T750" id="Seg_11489" s="T749">v</ta>
            <ta e="T751" id="Seg_11490" s="T750">adv</ta>
            <ta e="T753" id="Seg_11491" s="T752">v</ta>
            <ta e="T755" id="Seg_11492" s="T754">v</ta>
            <ta e="T756" id="Seg_11493" s="T755">conj</ta>
            <ta e="T757" id="Seg_11494" s="T756">pers</ta>
            <ta e="T758" id="Seg_11495" s="T757">v</ta>
            <ta e="T759" id="Seg_11496" s="T758">que</ta>
            <ta e="T760" id="Seg_11497" s="T759">v</ta>
            <ta e="T761" id="Seg_11498" s="T760">conj</ta>
            <ta e="T762" id="Seg_11499" s="T761">n</ta>
            <ta e="T763" id="Seg_11500" s="T762">v</ta>
            <ta e="T764" id="Seg_11501" s="T763">refl</ta>
            <ta e="T765" id="Seg_11502" s="T764">n</ta>
            <ta e="T766" id="Seg_11503" s="T765">v</ta>
            <ta e="T767" id="Seg_11504" s="T766">conj</ta>
            <ta e="T768" id="Seg_11505" s="T767">n</ta>
            <ta e="T769" id="Seg_11506" s="T768">n</ta>
            <ta e="T771" id="Seg_11507" s="T770">v</ta>
            <ta e="T772" id="Seg_11508" s="T771">conj</ta>
            <ta e="T773" id="Seg_11509" s="T772">adv</ta>
            <ta e="T774" id="Seg_11510" s="T773">v</ta>
            <ta e="T776" id="Seg_11511" s="T775">adv</ta>
            <ta e="T777" id="Seg_11512" s="T776">n</ta>
            <ta e="T778" id="Seg_11513" s="T777">v</ta>
            <ta e="T779" id="Seg_11514" s="T778">n</ta>
            <ta e="T780" id="Seg_11515" s="T779">ptcl</ta>
            <ta e="T781" id="Seg_11516" s="T780">n</ta>
            <ta e="T783" id="Seg_11517" s="T782">que</ta>
            <ta e="T784" id="Seg_11518" s="T783">ptcl</ta>
            <ta e="T785" id="Seg_11519" s="T784">v</ta>
            <ta e="T786" id="Seg_11520" s="T785">ptcl</ta>
            <ta e="T787" id="Seg_11521" s="T786">n</ta>
            <ta e="T788" id="Seg_11522" s="T787">v</ta>
            <ta e="T789" id="Seg_11523" s="T788">n</ta>
            <ta e="T790" id="Seg_11524" s="T789">ptcl</ta>
            <ta e="T791" id="Seg_11525" s="T790">n</ta>
            <ta e="T792" id="Seg_11526" s="T791">v</ta>
            <ta e="T793" id="Seg_11527" s="T792">que</ta>
            <ta e="T794" id="Seg_11528" s="T793">ptcl</ta>
            <ta e="T795" id="Seg_11529" s="T794">v</ta>
            <ta e="T796" id="Seg_11530" s="T795">dempro</ta>
            <ta e="T797" id="Seg_11531" s="T796">v</ta>
            <ta e="T798" id="Seg_11532" s="T797">conj</ta>
            <ta e="T799" id="Seg_11533" s="T798">ptcl</ta>
            <ta e="T800" id="Seg_11534" s="T799">v</ta>
            <ta e="T801" id="Seg_11535" s="T800">adv</ta>
            <ta e="T802" id="Seg_11536" s="T801">v</ta>
            <ta e="T803" id="Seg_11537" s="T802">n</ta>
            <ta e="T804" id="Seg_11538" s="T803">v</ta>
            <ta e="T805" id="Seg_11539" s="T804">adv</ta>
            <ta e="T806" id="Seg_11540" s="T805">pers</ta>
            <ta e="T807" id="Seg_11541" s="T806">v</ta>
            <ta e="T808" id="Seg_11542" s="T807">v</ta>
            <ta e="T809" id="Seg_11543" s="T808">n</ta>
            <ta e="T810" id="Seg_11544" s="T809">pers</ta>
            <ta e="T811" id="Seg_11545" s="T810">v</ta>
            <ta e="T812" id="Seg_11546" s="T811">v</ta>
            <ta e="T813" id="Seg_11547" s="T812">adv</ta>
            <ta e="T814" id="Seg_11548" s="T813">propr</ta>
            <ta e="T815" id="Seg_11549" s="T814">v</ta>
            <ta e="T816" id="Seg_11550" s="T815">v</ta>
            <ta e="T817" id="Seg_11551" s="T816">adv</ta>
            <ta e="T818" id="Seg_11552" s="T817">n</ta>
            <ta e="T819" id="Seg_11553" s="T818">v</ta>
            <ta e="T820" id="Seg_11554" s="T819">v</ta>
            <ta e="T821" id="Seg_11555" s="T820">adv</ta>
            <ta e="T822" id="Seg_11556" s="T821">n</ta>
            <ta e="T823" id="Seg_11557" s="T822">v</ta>
            <ta e="T824" id="Seg_11558" s="T823">v</ta>
            <ta e="T825" id="Seg_11559" s="T824">n</ta>
            <ta e="T826" id="Seg_11560" s="T825">n</ta>
            <ta e="T827" id="Seg_11561" s="T826">v</ta>
            <ta e="T829" id="Seg_11562" s="T828">adv</ta>
            <ta e="T830" id="Seg_11563" s="T829">v</ta>
            <ta e="T831" id="Seg_11564" s="T830">n</ta>
            <ta e="T832" id="Seg_11565" s="T831">v</ta>
            <ta e="T833" id="Seg_11566" s="T832">num</ta>
            <ta e="T834" id="Seg_11567" s="T833">n</ta>
            <ta e="T835" id="Seg_11568" s="T834">dempro</ta>
            <ta e="T836" id="Seg_11569" s="T835">v</ta>
            <ta e="T837" id="Seg_11570" s="T836">n</ta>
            <ta e="T838" id="Seg_11571" s="T837">adv</ta>
            <ta e="T839" id="Seg_11572" s="T838">dempro</ta>
            <ta e="T840" id="Seg_11573" s="T839">dempro</ta>
            <ta e="T841" id="Seg_11574" s="T840">n</ta>
            <ta e="T842" id="Seg_11575" s="T841">v</ta>
            <ta e="T844" id="Seg_11576" s="T843">v</ta>
            <ta e="T846" id="Seg_11577" s="T845">v</ta>
            <ta e="T847" id="Seg_11578" s="T846">n</ta>
            <ta e="T849" id="Seg_11579" s="T848">dempro</ta>
            <ta e="T850" id="Seg_11580" s="T849">n</ta>
            <ta e="T851" id="Seg_11581" s="T850">adv</ta>
            <ta e="T852" id="Seg_11582" s="T851">adj</ta>
            <ta e="T853" id="Seg_11583" s="T852">n</ta>
            <ta e="T854" id="Seg_11584" s="T853">v</ta>
            <ta e="T855" id="Seg_11585" s="T854">conj</ta>
            <ta e="T856" id="Seg_11586" s="T855">n</ta>
            <ta e="T858" id="Seg_11587" s="T857">n</ta>
            <ta e="T859" id="Seg_11588" s="T858">v</ta>
            <ta e="T860" id="Seg_11589" s="T859">n</ta>
            <ta e="T861" id="Seg_11590" s="T860">n</ta>
            <ta e="T862" id="Seg_11591" s="T861">ptcl</ta>
            <ta e="T863" id="Seg_11592" s="T862">v</ta>
            <ta e="T864" id="Seg_11593" s="T863">conj</ta>
            <ta e="T865" id="Seg_11594" s="T864">pers</ta>
            <ta e="T866" id="Seg_11595" s="T865">refl</ta>
            <ta e="T867" id="Seg_11596" s="T866">n</ta>
            <ta e="T868" id="Seg_11597" s="T867">ptcl</ta>
            <ta e="T869" id="Seg_11598" s="T868">v</ta>
            <ta e="T870" id="Seg_11599" s="T869">n</ta>
            <ta e="T871" id="Seg_11600" s="T870">n</ta>
            <ta e="T872" id="Seg_11601" s="T871">v</ta>
            <ta e="T874" id="Seg_11602" s="T873">ptcl</ta>
            <ta e="T875" id="Seg_11603" s="T874">v</ta>
            <ta e="T877" id="Seg_11604" s="T876">pers</ta>
            <ta e="T878" id="Seg_11605" s="T877">n</ta>
            <ta e="T879" id="Seg_11606" s="T878">v</ta>
            <ta e="T880" id="Seg_11607" s="T879">n</ta>
            <ta e="T882" id="Seg_11608" s="T880">conj</ta>
            <ta e="T883" id="Seg_11609" s="T882">dempro</ta>
            <ta e="T884" id="Seg_11610" s="T883">num</ta>
            <ta e="T885" id="Seg_11611" s="T884">num</ta>
            <ta e="T886" id="Seg_11612" s="T885">num</ta>
            <ta e="T887" id="Seg_11613" s="T886">num</ta>
            <ta e="T888" id="Seg_11614" s="T887">adv</ta>
            <ta e="T889" id="Seg_11615" s="T888">propr</ta>
            <ta e="T890" id="Seg_11616" s="T889">v</ta>
            <ta e="T891" id="Seg_11617" s="T890">n</ta>
            <ta e="T892" id="Seg_11618" s="T891">adv</ta>
            <ta e="T893" id="Seg_11619" s="T892">conj</ta>
            <ta e="T894" id="Seg_11620" s="T893">n</ta>
            <ta e="T895" id="Seg_11621" s="T894">conj</ta>
            <ta e="T896" id="Seg_11622" s="T895">pers</ta>
            <ta e="T897" id="Seg_11623" s="T896">ptcl</ta>
            <ta e="T898" id="Seg_11624" s="T897">v</ta>
            <ta e="T900" id="Seg_11625" s="T899">adv</ta>
            <ta e="T901" id="Seg_11626" s="T900">ptcl</ta>
            <ta e="T902" id="Seg_11627" s="T901">quant</ta>
            <ta e="T903" id="Seg_11628" s="T902">v</ta>
            <ta e="T904" id="Seg_11629" s="T903">adv</ta>
            <ta e="T905" id="Seg_11630" s="T904">ptcl</ta>
            <ta e="T906" id="Seg_11631" s="T905">n</ta>
            <ta e="T907" id="Seg_11632" s="T906">v</ta>
            <ta e="T908" id="Seg_11633" s="T907">v</ta>
            <ta e="T909" id="Seg_11634" s="T908">v</ta>
            <ta e="T913" id="Seg_11635" s="T912">n</ta>
            <ta e="T914" id="Seg_11636" s="T913">ptcl</ta>
            <ta e="T915" id="Seg_11637" s="T914">adv</ta>
            <ta e="T916" id="Seg_11638" s="T915">v</ta>
            <ta e="T917" id="Seg_11639" s="T916">adv</ta>
            <ta e="T918" id="Seg_11640" s="T917">adj</ta>
            <ta e="T919" id="Seg_11641" s="T918">v</ta>
            <ta e="T920" id="Seg_11642" s="T919">adv</ta>
            <ta e="T921" id="Seg_11643" s="T920">n</ta>
            <ta e="T922" id="Seg_11644" s="T921">n</ta>
            <ta e="T923" id="Seg_11645" s="T922">adv</ta>
            <ta e="T924" id="Seg_11646" s="T923">v</ta>
            <ta e="T925" id="Seg_11647" s="T924">ptcl</ta>
            <ta e="T926" id="Seg_11648" s="T925">adj</ta>
            <ta e="T927" id="Seg_11649" s="T926">v</ta>
            <ta e="T929" id="Seg_11650" s="T928">pers</ta>
            <ta e="T930" id="Seg_11651" s="T929">adv</ta>
            <ta e="T931" id="Seg_11652" s="T930">ptcl</ta>
            <ta e="T932" id="Seg_11653" s="T931">v</ta>
            <ta e="T933" id="Seg_11654" s="T932">propr</ta>
            <ta e="T934" id="Seg_11655" s="T933">conj</ta>
            <ta e="T935" id="Seg_11656" s="T934">dempro</ta>
            <ta e="T936" id="Seg_11657" s="T935">n</ta>
            <ta e="T937" id="Seg_11658" s="T936">v</ta>
            <ta e="T938" id="Seg_11659" s="T937">propr</ta>
            <ta e="T939" id="Seg_11660" s="T938">conj</ta>
            <ta e="T940" id="Seg_11661" s="T939">v</ta>
            <ta e="T941" id="Seg_11662" s="T940">pers</ta>
            <ta e="T944" id="Seg_11663" s="T943">ptcl</ta>
            <ta e="T945" id="Seg_11664" s="T944">n</ta>
            <ta e="T946" id="Seg_11665" s="T945">v</ta>
            <ta e="T947" id="Seg_11666" s="T946">dempro</ta>
            <ta e="T948" id="Seg_11667" s="T947">ptcl</ta>
            <ta e="T949" id="Seg_11668" s="T948">refl</ta>
            <ta e="T950" id="Seg_11669" s="T949">v</ta>
            <ta e="T951" id="Seg_11670" s="T950">adv</ta>
            <ta e="T952" id="Seg_11671" s="T951">n</ta>
            <ta e="T953" id="Seg_11672" s="T952">v</ta>
            <ta e="T954" id="Seg_11673" s="T953">conj</ta>
            <ta e="T955" id="Seg_11674" s="T954">v</ta>
            <ta e="T957" id="Seg_11675" s="T956">ptcl</ta>
            <ta e="T958" id="Seg_11676" s="T957">v</ta>
            <ta e="T959" id="Seg_11677" s="T958">v</ta>
            <ta e="T960" id="Seg_11678" s="T959">conj</ta>
            <ta e="T961" id="Seg_11679" s="T960">ptcl</ta>
            <ta e="T962" id="Seg_11680" s="T961">ptcl</ta>
            <ta e="T963" id="Seg_11681" s="T962">v</ta>
            <ta e="T964" id="Seg_11682" s="T963">dempro</ta>
            <ta e="T965" id="Seg_11683" s="T964">v</ta>
            <ta e="T966" id="Seg_11684" s="T965">ptcl</ta>
            <ta e="T967" id="Seg_11685" s="T966">v</ta>
            <ta e="T968" id="Seg_11686" s="T967">ptcl</ta>
            <ta e="T969" id="Seg_11687" s="T968">v</ta>
            <ta e="T970" id="Seg_11688" s="T969">v</ta>
            <ta e="T972" id="Seg_11689" s="T971">dempro</ta>
            <ta e="T973" id="Seg_11690" s="T972">ptcl</ta>
            <ta e="T974" id="Seg_11691" s="T973">v</ta>
            <ta e="T975" id="Seg_11692" s="T974">n</ta>
            <ta e="T976" id="Seg_11693" s="T975">adv</ta>
            <ta e="T978" id="Seg_11694" s="T977">v</ta>
            <ta e="T979" id="Seg_11695" s="T978">ptcl</ta>
            <ta e="T980" id="Seg_11696" s="T979">n</ta>
            <ta e="T981" id="Seg_11697" s="T980">v</ta>
            <ta e="T982" id="Seg_11698" s="T981">pers</ta>
            <ta e="T983" id="Seg_11699" s="T982">ptcl</ta>
            <ta e="T984" id="Seg_11700" s="T983">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_11701" s="T0">0.2.h:A</ta>
            <ta e="T2" id="Seg_11702" s="T1">np:G</ta>
            <ta e="T5" id="Seg_11703" s="T4">pro.h:A</ta>
            <ta e="T6" id="Seg_11704" s="T5">pro.h:Poss</ta>
            <ta e="T7" id="Seg_11705" s="T6">np:Th</ta>
            <ta e="T9" id="Seg_11706" s="T8">np:Ins</ta>
            <ta e="T10" id="Seg_11707" s="T9">np:Ins</ta>
            <ta e="T12" id="Seg_11708" s="T11">pro.h:A</ta>
            <ta e="T13" id="Seg_11709" s="T12">pro.h:B</ta>
            <ta e="T16" id="Seg_11710" s="T15">0.1.h:E</ta>
            <ta e="T21" id="Seg_11711" s="T20">0.1.h:E</ta>
            <ta e="T22" id="Seg_11712" s="T21">np.h:Poss</ta>
            <ta e="T23" id="Seg_11713" s="T22">np:L</ta>
            <ta e="T24" id="Seg_11714" s="T23">np:G</ta>
            <ta e="T28" id="Seg_11715" s="T27">0.3.h:A</ta>
            <ta e="T32" id="Seg_11716" s="T31">np.h:A</ta>
            <ta e="T37" id="Seg_11717" s="T36">np.h:A</ta>
            <ta e="T38" id="Seg_11718" s="T37">adv:Time</ta>
            <ta e="T43" id="Seg_11719" s="T42">np.h:A</ta>
            <ta e="T45" id="Seg_11720" s="T44">pro.h:A</ta>
            <ta e="T46" id="Seg_11721" s="T45">np:G</ta>
            <ta e="T50" id="Seg_11722" s="T49">adv:Time</ta>
            <ta e="T51" id="Seg_11723" s="T50">np.h:A</ta>
            <ta e="T57" id="Seg_11724" s="T56">0.3.h:A</ta>
            <ta e="T58" id="Seg_11725" s="T57">pro.h:P</ta>
            <ta e="T59" id="Seg_11726" s="T58">pro.h:A</ta>
            <ta e="T62" id="Seg_11727" s="T61">adv:Time</ta>
            <ta e="T63" id="Seg_11728" s="T62">np.h:A</ta>
            <ta e="T66" id="Seg_11729" s="T65">adv:Time</ta>
            <ta e="T67" id="Seg_11730" s="T66">pro.h:A</ta>
            <ta e="T69" id="Seg_11731" s="T68">0.1.h:A</ta>
            <ta e="T71" id="Seg_11732" s="T70">pro.h:Poss</ta>
            <ta e="T72" id="Seg_11733" s="T71">np.h:A</ta>
            <ta e="T74" id="Seg_11734" s="T73">np:G</ta>
            <ta e="T75" id="Seg_11735" s="T74">adv:Time</ta>
            <ta e="T78" id="Seg_11736" s="T77">np:P</ta>
            <ta e="T79" id="Seg_11737" s="T78">0.3.h:A</ta>
            <ta e="T80" id="Seg_11738" s="T79">np.h:A</ta>
            <ta e="T82" id="Seg_11739" s="T81">0.3.h:A</ta>
            <ta e="T83" id="Seg_11740" s="T82">0.2.h:A</ta>
            <ta e="T84" id="Seg_11741" s="T83">np:G</ta>
            <ta e="T88" id="Seg_11742" s="T87">pro.h:A</ta>
            <ta e="T91" id="Seg_11743" s="T90">0.3.h:E</ta>
            <ta e="T92" id="Seg_11744" s="T91">adv:L</ta>
            <ta e="T93" id="Seg_11745" s="T92">np:Th</ta>
            <ta e="T95" id="Seg_11746" s="T94">np:G</ta>
            <ta e="T96" id="Seg_11747" s="T95">0.1.h:A</ta>
            <ta e="T98" id="Seg_11748" s="T97">np:P</ta>
            <ta e="T99" id="Seg_11749" s="T98">np:Ins</ta>
            <ta e="T109" id="Seg_11750" s="T108">adv:Time</ta>
            <ta e="T110" id="Seg_11751" s="T109">0.1.h:A</ta>
            <ta e="T111" id="Seg_11752" s="T110">0.1.h:A</ta>
            <ta e="T113" id="Seg_11753" s="T112">0.2.h:A</ta>
            <ta e="T114" id="Seg_11754" s="T113">0.2.h:A</ta>
            <ta e="T116" id="Seg_11755" s="T115">0.2.h:A</ta>
            <ta e="T118" id="Seg_11756" s="T117">0.1.h:A</ta>
            <ta e="T120" id="Seg_11757" s="T119">adv:Time</ta>
            <ta e="T122" id="Seg_11758" s="T121">0.3.h:A</ta>
            <ta e="T123" id="Seg_11759" s="T122">0.2.h:A</ta>
            <ta e="T124" id="Seg_11760" s="T123">np:So</ta>
            <ta e="T125" id="Seg_11761" s="T124">np:Th</ta>
            <ta e="T126" id="Seg_11762" s="T125">0.2.h:A</ta>
            <ta e="T127" id="Seg_11763" s="T126">np:B</ta>
            <ta e="T128" id="Seg_11764" s="T127">pro.h:A</ta>
            <ta e="T131" id="Seg_11765" s="T130">0.1.h:Th</ta>
            <ta e="T132" id="Seg_11766" s="T131">np:Th</ta>
            <ta e="T135" id="Seg_11767" s="T134">np:Th</ta>
            <ta e="T137" id="Seg_11768" s="T136">adv:Time</ta>
            <ta e="T138" id="Seg_11769" s="T137">0.1.h:A</ta>
            <ta e="T140" id="Seg_11770" s="T139">np:Th</ta>
            <ta e="T143" id="Seg_11771" s="T142">0.2.h:A</ta>
            <ta e="T145" id="Seg_11772" s="T144">np:Th</ta>
            <ta e="T148" id="Seg_11773" s="T147">np:Th</ta>
            <ta e="T150" id="Seg_11774" s="T149">pro.h:A</ta>
            <ta e="T154" id="Seg_11775" s="T153">adv:Time</ta>
            <ta e="T155" id="Seg_11776" s="T154">0.3.h:A</ta>
            <ta e="T156" id="Seg_11777" s="T155">0.2.h:A</ta>
            <ta e="T157" id="Seg_11778" s="T156">np:So</ta>
            <ta e="T158" id="Seg_11779" s="T157">0.2.h:A</ta>
            <ta e="T159" id="Seg_11780" s="T158">np:Th</ta>
            <ta e="T160" id="Seg_11781" s="T159">pro.h:A</ta>
            <ta e="T162" id="Seg_11782" s="T161">0.1.h:A</ta>
            <ta e="T163" id="Seg_11783" s="T162">0.1.h:A</ta>
            <ta e="T164" id="Seg_11784" s="T163">0.1.h:A</ta>
            <ta e="T168" id="Seg_11785" s="T167">adv:Time</ta>
            <ta e="T170" id="Seg_11786" s="T169">np:Th</ta>
            <ta e="T173" id="Seg_11787" s="T172">0.1.h:A</ta>
            <ta e="T175" id="Seg_11788" s="T174">0.1.h:A</ta>
            <ta e="T176" id="Seg_11789" s="T175">np:G</ta>
            <ta e="T178" id="Seg_11790" s="T177">np.h:A</ta>
            <ta e="T179" id="Seg_11791" s="T178">np:G</ta>
            <ta e="T184" id="Seg_11792" s="T183">np.h:A</ta>
            <ta e="T185" id="Seg_11793" s="T184">adv:Pth</ta>
            <ta e="T188" id="Seg_11794" s="T187">pro.h:E</ta>
            <ta e="T190" id="Seg_11795" s="T189">0.3.h:A</ta>
            <ta e="T192" id="Seg_11796" s="T191">adv:Time</ta>
            <ta e="T195" id="Seg_11797" s="T194">np.h:Poss</ta>
            <ta e="T197" id="Seg_11798" s="T196">np.h:A</ta>
            <ta e="T199" id="Seg_11799" s="T198">pro:L</ta>
            <ta e="T200" id="Seg_11800" s="T199">pro.h:A</ta>
            <ta e="T202" id="Seg_11801" s="T201">0.2.h:A</ta>
            <ta e="T204" id="Seg_11802" s="T203">0.2.h:A</ta>
            <ta e="T205" id="Seg_11803" s="T204">np:Th</ta>
            <ta e="T207" id="Seg_11804" s="T206">0.3.h:A</ta>
            <ta e="T208" id="Seg_11805" s="T207">0.2.h:E</ta>
            <ta e="T209" id="Seg_11806" s="T208">pro.h:A</ta>
            <ta e="T210" id="Seg_11807" s="T209">np:Th</ta>
            <ta e="T211" id="Seg_11808" s="T210">adv:So</ta>
            <ta e="T217" id="Seg_11809" s="T216">0.3.h:A</ta>
            <ta e="T218" id="Seg_11810" s="T217">pro.h:E</ta>
            <ta e="T227" id="Seg_11811" s="T226">np.h:A</ta>
            <ta e="T229" id="Seg_11812" s="T228">np:G</ta>
            <ta e="T232" id="Seg_11813" s="T231">np:Ins</ta>
            <ta e="T234" id="Seg_11814" s="T233">0.3.h:A</ta>
            <ta e="T235" id="Seg_11815" s="T234">np:G</ta>
            <ta e="T237" id="Seg_11816" s="T236">0.3.h:E</ta>
            <ta e="T238" id="Seg_11817" s="T237">np:Th</ta>
            <ta e="T241" id="Seg_11818" s="T240">adv:Time</ta>
            <ta e="T242" id="Seg_11819" s="T241">0.3.h:A</ta>
            <ta e="T243" id="Seg_11820" s="T242">pro.h:P</ta>
            <ta e="T244" id="Seg_11821" s="T243">np:A</ta>
            <ta e="T250" id="Seg_11822" s="T249">np:Th</ta>
            <ta e="T259" id="Seg_11823" s="T258">0.3.h:A</ta>
            <ta e="T261" id="Seg_11824" s="T260">np:P</ta>
            <ta e="T265" id="Seg_11825" s="T264">adv:Time</ta>
            <ta e="T266" id="Seg_11826" s="T265">np.h:A</ta>
            <ta e="T268" id="Seg_11827" s="T267">np:Th</ta>
            <ta e="T269" id="Seg_11828" s="T268">0.2.h:A</ta>
            <ta e="T271" id="Seg_11829" s="T270">0.3.h:A</ta>
            <ta e="T272" id="Seg_11830" s="T271">pro.h:R</ta>
            <ta e="T273" id="Seg_11831" s="T272">pro.h:A</ta>
            <ta e="T276" id="Seg_11832" s="T275">np:G</ta>
            <ta e="T277" id="Seg_11833" s="T276">np:Th</ta>
            <ta e="T278" id="Seg_11834" s="T277">0.3.h:A</ta>
            <ta e="T279" id="Seg_11835" s="T278">0.3.h:A</ta>
            <ta e="T280" id="Seg_11836" s="T279">0.3.h:A</ta>
            <ta e="T281" id="Seg_11837" s="T280">0.3.h:A</ta>
            <ta e="T283" id="Seg_11838" s="T282">adv:L</ta>
            <ta e="T285" id="Seg_11839" s="T284">np:Th</ta>
            <ta e="T286" id="Seg_11840" s="T285">0.3.h:A</ta>
            <ta e="T287" id="Seg_11841" s="T286">0.3.h:A</ta>
            <ta e="T288" id="Seg_11842" s="T287">np:G</ta>
            <ta e="T290" id="Seg_11843" s="T289">0.3.h:A</ta>
            <ta e="T291" id="Seg_11844" s="T290">np:Th</ta>
            <ta e="T297" id="Seg_11845" s="T296">np:Th</ta>
            <ta e="T298" id="Seg_11846" s="T297">pro.h:A</ta>
            <ta e="T301" id="Seg_11847" s="T300">adv:L</ta>
            <ta e="T302" id="Seg_11848" s="T301">0.3.h:A</ta>
            <ta e="T303" id="Seg_11849" s="T302">np:Th</ta>
            <ta e="T304" id="Seg_11850" s="T303">0.2.h:A</ta>
            <ta e="T306" id="Seg_11851" s="T305">adv:L</ta>
            <ta e="T307" id="Seg_11852" s="T306">np:Th</ta>
            <ta e="T308" id="Seg_11853" s="T307">0.2.h:A</ta>
            <ta e="T309" id="Seg_11854" s="T308">adv:Time</ta>
            <ta e="T312" id="Seg_11855" s="T311">0.3.h:A</ta>
            <ta e="T314" id="Seg_11856" s="T313">np:P</ta>
            <ta e="T315" id="Seg_11857" s="T314">0.3.h:A</ta>
            <ta e="T319" id="Seg_11858" s="T318">np:Th</ta>
            <ta e="T320" id="Seg_11859" s="T319">0.3.h:A</ta>
            <ta e="T322" id="Seg_11860" s="T321">np:L</ta>
            <ta e="T324" id="Seg_11861" s="T323">np:Th</ta>
            <ta e="T342" id="Seg_11862" s="T341">np:Th</ta>
            <ta e="T345" id="Seg_11863" s="T344">np:L</ta>
            <ta e="T346" id="Seg_11864" s="T345">0.2.h:A</ta>
            <ta e="T350" id="Seg_11865" s="T349">np:Ins</ta>
            <ta e="T351" id="Seg_11866" s="T350">0.2.h:A</ta>
            <ta e="T354" id="Seg_11867" s="T353">0.2.h:Th</ta>
            <ta e="T361" id="Seg_11868" s="T360">np:Th</ta>
            <ta e="T362" id="Seg_11869" s="T361">pro.h:B</ta>
            <ta e="T365" id="Seg_11870" s="T364">0.2.h:E</ta>
            <ta e="T366" id="Seg_11871" s="T365">adv:Time</ta>
            <ta e="T367" id="Seg_11872" s="T366">np:L</ta>
            <ta e="T368" id="Seg_11873" s="T367">0.2.h:A</ta>
            <ta e="T371" id="Seg_11874" s="T370">0.2.h:E</ta>
            <ta e="T376" id="Seg_11875" s="T375">0.1.h:A</ta>
            <ta e="T377" id="Seg_11876" s="T376">np:Th</ta>
            <ta e="T378" id="Seg_11877" s="T377">0.3.h:A</ta>
            <ta e="T380" id="Seg_11878" s="T379">np:Th</ta>
            <ta e="T385" id="Seg_11879" s="T384">0.3:P</ta>
            <ta e="T390" id="Seg_11880" s="T389">pro.h:A</ta>
            <ta e="T391" id="Seg_11881" s="T390">np:L</ta>
            <ta e="T394" id="Seg_11882" s="T393">np:Th</ta>
            <ta e="T395" id="Seg_11883" s="T394">0.1.h:A</ta>
            <ta e="T396" id="Seg_11884" s="T395">adv:Time</ta>
            <ta e="T398" id="Seg_11885" s="T396">0.3.h:A</ta>
            <ta e="T405" id="Seg_11886" s="T404">adv:Time</ta>
            <ta e="T407" id="Seg_11887" s="T406">0.3.h:A</ta>
            <ta e="T408" id="Seg_11888" s="T407">np:Th</ta>
            <ta e="T411" id="Seg_11889" s="T410">pro.h:A</ta>
            <ta e="T414" id="Seg_11890" s="T413">0.1.h:A</ta>
            <ta e="T416" id="Seg_11891" s="T415">pro.h:A</ta>
            <ta e="T418" id="Seg_11892" s="T417">pro.h:A</ta>
            <ta e="T422" id="Seg_11893" s="T421">np:G</ta>
            <ta e="T423" id="Seg_11894" s="T422">0.3.h:A</ta>
            <ta e="T425" id="Seg_11895" s="T424">pro.h:Poss</ta>
            <ta e="T427" id="Seg_11896" s="T426">np.h:Poss</ta>
            <ta e="T428" id="Seg_11897" s="T427">np.h:A</ta>
            <ta e="T430" id="Seg_11898" s="T429">np:G</ta>
            <ta e="T431" id="Seg_11899" s="T430">adv:L</ta>
            <ta e="T432" id="Seg_11900" s="T431">np:Th</ta>
            <ta e="T434" id="Seg_11901" s="T433">np:P</ta>
            <ta e="T436" id="Seg_11902" s="T435">adv:Time</ta>
            <ta e="T437" id="Seg_11903" s="T436">0.3.h:A</ta>
            <ta e="T438" id="Seg_11904" s="T437">np:G</ta>
            <ta e="T440" id="Seg_11905" s="T439">adv:Time</ta>
            <ta e="T441" id="Seg_11906" s="T440">np:P</ta>
            <ta e="T443" id="Seg_11907" s="T442">0.3.h:A</ta>
            <ta e="T445" id="Seg_11908" s="T444">0.3.h:A</ta>
            <ta e="T447" id="Seg_11909" s="T446">pro.h:Poss</ta>
            <ta e="T448" id="Seg_11910" s="T447">np.h:Poss</ta>
            <ta e="T450" id="Seg_11911" s="T449">np.h:A</ta>
            <ta e="T453" id="Seg_11912" s="T452">0.3.h:A</ta>
            <ta e="T454" id="Seg_11913" s="T453">np.h:Th</ta>
            <ta e="T456" id="Seg_11914" s="T455">np.h:Th</ta>
            <ta e="T457" id="Seg_11915" s="T456">adv:Time</ta>
            <ta e="T459" id="Seg_11916" s="T458">pro.h:Poss</ta>
            <ta e="T460" id="Seg_11917" s="T459">np.h:R</ta>
            <ta e="T461" id="Seg_11918" s="T460">0.3.h:A</ta>
            <ta e="T462" id="Seg_11919" s="T461">0.2.h:A</ta>
            <ta e="T463" id="Seg_11920" s="T462">np:Th</ta>
            <ta e="T464" id="Seg_11921" s="T463">0.2.h:A</ta>
            <ta e="T466" id="Seg_11922" s="T465">0.2.h:A</ta>
            <ta e="T467" id="Seg_11923" s="T466">pro.h:A</ta>
            <ta e="T469" id="Seg_11924" s="T468">pro.h:A</ta>
            <ta e="T470" id="Seg_11925" s="T469">np:Th</ta>
            <ta e="T476" id="Seg_11926" s="T475">0.3.h:A</ta>
            <ta e="T478" id="Seg_11927" s="T477">adv:L</ta>
            <ta e="T479" id="Seg_11928" s="T478">np:Th</ta>
            <ta e="T480" id="Seg_11929" s="T479">pro.h:A</ta>
            <ta e="T486" id="Seg_11930" s="T485">adv:Time</ta>
            <ta e="T487" id="Seg_11931" s="T486">0.3.h:Th</ta>
            <ta e="T490" id="Seg_11932" s="T489">pro.h:Poss</ta>
            <ta e="T491" id="Seg_11933" s="T490">np.h:A</ta>
            <ta e="T498" id="Seg_11934" s="T497">np:Th</ta>
            <ta e="T501" id="Seg_11935" s="T500">np:Ins</ta>
            <ta e="T502" id="Seg_11936" s="T501">adv:Time</ta>
            <ta e="T503" id="Seg_11937" s="T502">0.3.h:A</ta>
            <ta e="T505" id="Seg_11938" s="T504">0.3.h:A</ta>
            <ta e="T506" id="Seg_11939" s="T505">adv:Time</ta>
            <ta e="T507" id="Seg_11940" s="T506">np:Th</ta>
            <ta e="T508" id="Seg_11941" s="T507">0.3.h:A</ta>
            <ta e="T509" id="Seg_11942" s="T508">np:Th</ta>
            <ta e="T510" id="Seg_11943" s="T509">0.3.h:A</ta>
            <ta e="T511" id="Seg_11944" s="T510">0.3.h:A</ta>
            <ta e="T512" id="Seg_11945" s="T511">np:G</ta>
            <ta e="T513" id="Seg_11946" s="T512">adv:L</ta>
            <ta e="T514" id="Seg_11947" s="T513">pro:Th</ta>
            <ta e="T515" id="Seg_11948" s="T514">0.3.h:A</ta>
            <ta e="T516" id="Seg_11949" s="T515">np:Ins</ta>
            <ta e="T517" id="Seg_11950" s="T516">pro:Th</ta>
            <ta e="T518" id="Seg_11951" s="T517">0.3.h:A</ta>
            <ta e="T519" id="Seg_11952" s="T518">np:Ins</ta>
            <ta e="T520" id="Seg_11953" s="T519">0.3:Th</ta>
            <ta e="T521" id="Seg_11954" s="T520">np:Ins</ta>
            <ta e="T523" id="Seg_11955" s="T522">0.3.h:E</ta>
            <ta e="T525" id="Seg_11956" s="T524">pro.h:A</ta>
            <ta e="T527" id="Seg_11957" s="T526">adv:Time</ta>
            <ta e="T528" id="Seg_11958" s="T527">n:Time</ta>
            <ta e="T530" id="Seg_11959" s="T529">np:P</ta>
            <ta e="T531" id="Seg_11960" s="T530">0.1.h:A</ta>
            <ta e="T532" id="Seg_11961" s="T531">adv:Time</ta>
            <ta e="T533" id="Seg_11962" s="T532">np:R</ta>
            <ta e="T534" id="Seg_11963" s="T533">np:Th</ta>
            <ta e="T535" id="Seg_11964" s="T534">0.1.h:A</ta>
            <ta e="T536" id="Seg_11965" s="T535">np:Th</ta>
            <ta e="T537" id="Seg_11966" s="T536">0.1.h:A</ta>
            <ta e="T538" id="Seg_11967" s="T537">0.1.h:A</ta>
            <ta e="T539" id="Seg_11968" s="T538">np:R</ta>
            <ta e="T540" id="Seg_11969" s="T539">0.1.h:A</ta>
            <ta e="T541" id="Seg_11970" s="T540">np:Th</ta>
            <ta e="T542" id="Seg_11971" s="T541">0.1.h:A</ta>
            <ta e="T544" id="Seg_11972" s="T543">adv:Time</ta>
            <ta e="T545" id="Seg_11973" s="T544">0.1.h:A</ta>
            <ta e="T546" id="Seg_11974" s="T545">np:G</ta>
            <ta e="T550" id="Seg_11975" s="T549">np:P</ta>
            <ta e="T551" id="Seg_11976" s="T550">0.1.h:A</ta>
            <ta e="T552" id="Seg_11977" s="T551">adv:Time</ta>
            <ta e="T553" id="Seg_11978" s="T552">0.1.h:A</ta>
            <ta e="T554" id="Seg_11979" s="T553">np:G</ta>
            <ta e="T556" id="Seg_11980" s="T555">np:Th</ta>
            <ta e="T557" id="Seg_11981" s="T556">0.1.h:A</ta>
            <ta e="T558" id="Seg_11982" s="T557">pro.h:A</ta>
            <ta e="T562" id="Seg_11983" s="T561">np:L</ta>
            <ta e="T564" id="Seg_11984" s="T563">np:P</ta>
            <ta e="T565" id="Seg_11985" s="T564">0.3.h:A</ta>
            <ta e="T567" id="Seg_11986" s="T566">np.h:A</ta>
            <ta e="T569" id="Seg_11987" s="T568">adv:Time</ta>
            <ta e="T570" id="Seg_11988" s="T569">0.3.h:A</ta>
            <ta e="T571" id="Seg_11989" s="T570">adv:L</ta>
            <ta e="T572" id="Seg_11990" s="T571">np:G</ta>
            <ta e="T574" id="Seg_11991" s="T573">0.3.h:E</ta>
            <ta e="T575" id="Seg_11992" s="T574">pro.h:A</ta>
            <ta e="T577" id="Seg_11993" s="T576">0.2.h:E</ta>
            <ta e="T585" id="Seg_11994" s="T584">adv:Time</ta>
            <ta e="T587" id="Seg_11995" s="T586">0.3.h:E</ta>
            <ta e="T589" id="Seg_11996" s="T588">adv:Time</ta>
            <ta e="T593" id="Seg_11997" s="T592">0.1.h:A</ta>
            <ta e="T594" id="Seg_11998" s="T593">np:G</ta>
            <ta e="T597" id="Seg_11999" s="T596">0.1.h:A</ta>
            <ta e="T598" id="Seg_12000" s="T597">0.1.h:A</ta>
            <ta e="T600" id="Seg_12001" s="T599">pro:G</ta>
            <ta e="T601" id="Seg_12002" s="T600">pro.h:A</ta>
            <ta e="T603" id="Seg_12003" s="T602">adv:L</ta>
            <ta e="T604" id="Seg_12004" s="T603">np.h:R</ta>
            <ta e="T607" id="Seg_12005" s="T606">adv:Time</ta>
            <ta e="T609" id="Seg_12006" s="T608">np.h:A</ta>
            <ta e="T613" id="Seg_12007" s="T612">np.h:R</ta>
            <ta e="T615" id="Seg_12008" s="T614">np.h:A</ta>
            <ta e="T617" id="Seg_12009" s="T616">pro.h:R</ta>
            <ta e="T618" id="Seg_12010" s="T617">pro:Th</ta>
            <ta e="T620" id="Seg_12011" s="T619">pro.h:A</ta>
            <ta e="T622" id="Seg_12012" s="T621">0.2.h:A</ta>
            <ta e="T625" id="Seg_12013" s="T624">np.h:A</ta>
            <ta e="T626" id="Seg_12014" s="T625">pro:Th</ta>
            <ta e="T630" id="Seg_12015" s="T629">pro.h:A</ta>
            <ta e="T634" id="Seg_12016" s="T633">np.h:A</ta>
            <ta e="T638" id="Seg_12017" s="T637">0.2.h:E</ta>
            <ta e="T640" id="Seg_12018" s="T639">pro.h:A</ta>
            <ta e="T641" id="Seg_12019" s="T640">np:Ins</ta>
            <ta e="T642" id="Seg_12020" s="T641">pro.h:R</ta>
            <ta e="T644" id="Seg_12021" s="T643">pro.h:Poss</ta>
            <ta e="T645" id="Seg_12022" s="T644">np:P</ta>
            <ta e="T646" id="Seg_12023" s="T645">0.3.h:A</ta>
            <ta e="T649" id="Seg_12024" s="T648">0.3.h:A</ta>
            <ta e="T650" id="Seg_12025" s="T649">adv:Time</ta>
            <ta e="T653" id="Seg_12026" s="T652">0.1.h:Th</ta>
            <ta e="T654" id="Seg_12027" s="T653">adv:Time</ta>
            <ta e="T655" id="Seg_12028" s="T654">np.h:A</ta>
            <ta e="T658" id="Seg_12029" s="T657">0.2.h:Th</ta>
            <ta e="T660" id="Seg_12030" s="T659">0.2.h:Th</ta>
            <ta e="T662" id="Seg_12031" s="T661">0.2.h:A</ta>
            <ta e="T663" id="Seg_12032" s="T662">np:Th</ta>
            <ta e="T664" id="Seg_12033" s="T663">0.1.h:A</ta>
            <ta e="T666" id="Seg_12034" s="T665">pro.h:A</ta>
            <ta e="T667" id="Seg_12035" s="T666">np.h:R</ta>
            <ta e="T669" id="Seg_12036" s="T668">0.2.h:A</ta>
            <ta e="T670" id="Seg_12037" s="T669">pro.h:R</ta>
            <ta e="T672" id="Seg_12038" s="T671">np:Th</ta>
            <ta e="T673" id="Seg_12039" s="T672">pro.h:A</ta>
            <ta e="T675" id="Seg_12040" s="T674">pro.h:R</ta>
            <ta e="T679" id="Seg_12041" s="T678">pro.h:A</ta>
            <ta e="T686" id="Seg_12042" s="T685">adv:Time</ta>
            <ta e="T688" id="Seg_12043" s="T687">pro.h:A</ta>
            <ta e="T689" id="Seg_12044" s="T688">pro.h:R</ta>
            <ta e="T693" id="Seg_12045" s="T692">np:Th</ta>
            <ta e="T695" id="Seg_12046" s="T694">adv:Time</ta>
            <ta e="T696" id="Seg_12047" s="T695">pro.h:A</ta>
            <ta e="T698" id="Seg_12048" s="T697">np:R</ta>
            <ta e="T700" id="Seg_12049" s="T699">np:R</ta>
            <ta e="T701" id="Seg_12050" s="T700">0.1.h:A</ta>
            <ta e="T702" id="Seg_12051" s="T701">np:Th</ta>
            <ta e="T703" id="Seg_12052" s="T702">0.1.h:A</ta>
            <ta e="T704" id="Seg_12053" s="T703">adv:Time</ta>
            <ta e="T706" id="Seg_12054" s="T705">0.1.h:A</ta>
            <ta e="T709" id="Seg_12055" s="T708">0.1.h:A</ta>
            <ta e="T710" id="Seg_12056" s="T709">np:G</ta>
            <ta e="T711" id="Seg_12057" s="T710">np:E 0.1.h:Poss</ta>
            <ta e="T713" id="Seg_12058" s="T712">np:G</ta>
            <ta e="T716" id="Seg_12059" s="T715">0.1.h:A</ta>
            <ta e="T717" id="Seg_12060" s="T716">np:G</ta>
            <ta e="T719" id="Seg_12061" s="T718">0.1.h:A</ta>
            <ta e="T720" id="Seg_12062" s="T719">adv:Time</ta>
            <ta e="T724" id="Seg_12063" s="T723">pro.h:A</ta>
            <ta e="T730" id="Seg_12064" s="T729">adv:Time</ta>
            <ta e="T734" id="Seg_12065" s="T733">adv:L</ta>
            <ta e="T735" id="Seg_12066" s="T734">np:A</ta>
            <ta e="T739" id="Seg_12067" s="T738">0.3:Th</ta>
            <ta e="T741" id="Seg_12068" s="T740">pro.h:A</ta>
            <ta e="T744" id="Seg_12069" s="T743">pro.h:A</ta>
            <ta e="T747" id="Seg_12070" s="T746">0.1.h:A</ta>
            <ta e="T749" id="Seg_12071" s="T748">adv:Time</ta>
            <ta e="T750" id="Seg_12072" s="T749">0.3.h:A</ta>
            <ta e="T755" id="Seg_12073" s="T754">0.3.h:A</ta>
            <ta e="T757" id="Seg_12074" s="T756">pro.h:A</ta>
            <ta e="T759" id="Seg_12075" s="T758">adv:L</ta>
            <ta e="T760" id="Seg_12076" s="T759">0.2.h:Th</ta>
            <ta e="T762" id="Seg_12077" s="T761">np:Th</ta>
            <ta e="T763" id="Seg_12078" s="T762">0.1.h:A</ta>
            <ta e="T765" id="Seg_12079" s="T764">np:Th</ta>
            <ta e="T766" id="Seg_12080" s="T765">0.1.h:A</ta>
            <ta e="T768" id="Seg_12081" s="T767">np.h:Poss</ta>
            <ta e="T769" id="Seg_12082" s="T768">np:Th</ta>
            <ta e="T771" id="Seg_12083" s="T770">0.1.h:A</ta>
            <ta e="T773" id="Seg_12084" s="T772">adv:L</ta>
            <ta e="T774" id="Seg_12085" s="T773">0.1.h:A</ta>
            <ta e="T776" id="Seg_12086" s="T775">adv:Time</ta>
            <ta e="T777" id="Seg_12087" s="T776">np.h:A</ta>
            <ta e="T779" id="Seg_12088" s="T778">np:So</ta>
            <ta e="T783" id="Seg_12089" s="T782">pro:Th</ta>
            <ta e="T785" id="Seg_12090" s="T784">0.3.h:A</ta>
            <ta e="T787" id="Seg_12091" s="T786">np:Th</ta>
            <ta e="T788" id="Seg_12092" s="T787">0.2.h:A</ta>
            <ta e="T789" id="Seg_12093" s="T788">np:L</ta>
            <ta e="T791" id="Seg_12094" s="T790">np.h:A 0.2.h:Poss</ta>
            <ta e="T793" id="Seg_12095" s="T792">pro:Th</ta>
            <ta e="T795" id="Seg_12096" s="T794">0.3.h:A</ta>
            <ta e="T796" id="Seg_12097" s="T795">pro.h:A</ta>
            <ta e="T801" id="Seg_12098" s="T800">adv:Time</ta>
            <ta e="T802" id="Seg_12099" s="T801">0.3.h:A</ta>
            <ta e="T803" id="Seg_12100" s="T802">np:L</ta>
            <ta e="T804" id="Seg_12101" s="T803">0.3.h:A</ta>
            <ta e="T805" id="Seg_12102" s="T804">adv:Time</ta>
            <ta e="T806" id="Seg_12103" s="T805">pro.h:R</ta>
            <ta e="T807" id="Seg_12104" s="T806">0.3.h:A</ta>
            <ta e="T808" id="Seg_12105" s="T807">0.2.h:A</ta>
            <ta e="T809" id="Seg_12106" s="T808">np:G</ta>
            <ta e="T810" id="Seg_12107" s="T809">pro.h:A</ta>
            <ta e="T813" id="Seg_12108" s="T812">adv:Time</ta>
            <ta e="T814" id="Seg_12109" s="T813">np.h:A</ta>
            <ta e="T817" id="Seg_12110" s="T816">adv:Time</ta>
            <ta e="T818" id="Seg_12111" s="T817">np.h:A</ta>
            <ta e="T820" id="Seg_12112" s="T819">0.3.h:A</ta>
            <ta e="T821" id="Seg_12113" s="T820">adv:Time</ta>
            <ta e="T822" id="Seg_12114" s="T821">np.h:A 0.3.h:Poss</ta>
            <ta e="T824" id="Seg_12115" s="T823">0.3.h:A</ta>
            <ta e="T825" id="Seg_12116" s="T824">np:Th</ta>
            <ta e="T826" id="Seg_12117" s="T825">np:Th</ta>
            <ta e="T827" id="Seg_12118" s="T826">0.3.h:A</ta>
            <ta e="T829" id="Seg_12119" s="T828">adv:Time</ta>
            <ta e="T830" id="Seg_12120" s="T829">0.3.h:A</ta>
            <ta e="T831" id="Seg_12121" s="T830">np:Th</ta>
            <ta e="T832" id="Seg_12122" s="T831">0.3.h:A</ta>
            <ta e="T834" id="Seg_12123" s="T833">np.h:A</ta>
            <ta e="T835" id="Seg_12124" s="T834">pro:Th</ta>
            <ta e="T837" id="Seg_12125" s="T836">np:Ins</ta>
            <ta e="T838" id="Seg_12126" s="T837">adv:Time</ta>
            <ta e="T839" id="Seg_12127" s="T838">pro.h:A</ta>
            <ta e="T840" id="Seg_12128" s="T839">pro.h:B</ta>
            <ta e="T841" id="Seg_12129" s="T840">np:Th</ta>
            <ta e="T844" id="Seg_12130" s="T843">0.3.h:A</ta>
            <ta e="T846" id="Seg_12131" s="T845">0.3.h:A</ta>
            <ta e="T847" id="Seg_12132" s="T846">np:G</ta>
            <ta e="T850" id="Seg_12133" s="T849">np.h:E</ta>
            <ta e="T853" id="Seg_12134" s="T852">np:Th</ta>
            <ta e="T854" id="Seg_12135" s="T853">0.3.h:A</ta>
            <ta e="T856" id="Seg_12136" s="T855">np:E</ta>
            <ta e="T858" id="Seg_12137" s="T857">np:Ins</ta>
            <ta e="T859" id="Seg_12138" s="T858">0.3.h:A</ta>
            <ta e="T860" id="Seg_12139" s="T859">np:G</ta>
            <ta e="T861" id="Seg_12140" s="T860">np:Th</ta>
            <ta e="T865" id="Seg_12141" s="T864">pro.h:A</ta>
            <ta e="T867" id="Seg_12142" s="T866">np:E</ta>
            <ta e="T870" id="Seg_12143" s="T869">np:Ins</ta>
            <ta e="T871" id="Seg_12144" s="T870">np:Ins</ta>
            <ta e="T872" id="Seg_12145" s="T871">0.1.h:A</ta>
            <ta e="T877" id="Seg_12146" s="T876">pro.h:A</ta>
            <ta e="T878" id="Seg_12147" s="T877">np:L</ta>
            <ta e="T880" id="Seg_12148" s="T879">np:Th</ta>
            <ta e="T888" id="Seg_12149" s="T887">adv:Time</ta>
            <ta e="T889" id="Seg_12150" s="T888">np.h:A</ta>
            <ta e="T891" id="Seg_12151" s="T890">np:Th</ta>
            <ta e="T894" id="Seg_12152" s="T893">np:Th</ta>
            <ta e="T896" id="Seg_12153" s="T895">pro.h:A</ta>
            <ta e="T900" id="Seg_12154" s="T899">adv:Time</ta>
            <ta e="T903" id="Seg_12155" s="T902">0.1.h:A</ta>
            <ta e="T904" id="Seg_12156" s="T903">adv:Time</ta>
            <ta e="T906" id="Seg_12157" s="T905">np:G</ta>
            <ta e="T913" id="Seg_12158" s="T912">np:Th</ta>
            <ta e="T915" id="Seg_12159" s="T914">adv:G</ta>
            <ta e="T919" id="Seg_12160" s="T918">0.3:P</ta>
            <ta e="T920" id="Seg_12161" s="T919">adv:Time</ta>
            <ta e="T921" id="Seg_12162" s="T920">n:Time</ta>
            <ta e="T923" id="Seg_12163" s="T922">n:Time</ta>
            <ta e="T924" id="Seg_12164" s="T923">0.1.h:A</ta>
            <ta e="T927" id="Seg_12165" s="T926">0.3:P</ta>
            <ta e="T929" id="Seg_12166" s="T928">pro.h:A</ta>
            <ta e="T930" id="Seg_12167" s="T929">adv:Time</ta>
            <ta e="T933" id="Seg_12168" s="T932">np:G</ta>
            <ta e="T935" id="Seg_12169" s="T934">pro.h:A</ta>
            <ta e="T936" id="Seg_12170" s="T935">np:Th</ta>
            <ta e="T938" id="Seg_12171" s="T937">np:So</ta>
            <ta e="T940" id="Seg_12172" s="T939">0.3.h:A</ta>
            <ta e="T941" id="Seg_12173" s="T940">pro:G</ta>
            <ta e="T945" id="Seg_12174" s="T944">np:Th</ta>
            <ta e="T947" id="Seg_12175" s="T946">pro.h:B</ta>
            <ta e="T951" id="Seg_12176" s="T950">adv:So</ta>
            <ta e="T952" id="Seg_12177" s="T951">np:Ins</ta>
            <ta e="T953" id="Seg_12178" s="T952">0.3.h:A</ta>
            <ta e="T955" id="Seg_12179" s="T954">0.3.h:A</ta>
            <ta e="T958" id="Seg_12180" s="T957">0.1.h:E</ta>
            <ta e="T959" id="Seg_12181" s="T958">0.3.h:A</ta>
            <ta e="T964" id="Seg_12182" s="T963">pro.h:Com</ta>
            <ta e="T967" id="Seg_12183" s="T966">0.3.h:A</ta>
            <ta e="T969" id="Seg_12184" s="T968">0.2.h:A</ta>
            <ta e="T970" id="Seg_12185" s="T969">0.2.h:A</ta>
            <ta e="T972" id="Seg_12186" s="T971">pro.h:A</ta>
            <ta e="T975" id="Seg_12187" s="T974">np:G</ta>
            <ta e="T976" id="Seg_12188" s="T975">adv:L</ta>
            <ta e="T982" id="Seg_12189" s="T981">pro.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_12190" s="T0">v:pred 0.2.h:S</ta>
            <ta e="T5" id="Seg_12191" s="T4">pro.h:S</ta>
            <ta e="T7" id="Seg_12192" s="T6">np:O</ta>
            <ta e="T8" id="Seg_12193" s="T7">v:pred</ta>
            <ta e="T16" id="Seg_12194" s="T15">v:pred 0.1.h:S</ta>
            <ta e="T17" id="Seg_12195" s="T16">ptcl.neg</ta>
            <ta e="T18" id="Seg_12196" s="T17">ptcl:pred</ta>
            <ta e="T21" id="Seg_12197" s="T20">v:pred 0.1.h:S</ta>
            <ta e="T27" id="Seg_12198" s="T26">ptcl.neg</ta>
            <ta e="T28" id="Seg_12199" s="T27">v:pred 0.3.h:S</ta>
            <ta e="T32" id="Seg_12200" s="T31">np.h:S</ta>
            <ta e="T35" id="Seg_12201" s="T34">v:pred</ta>
            <ta e="T37" id="Seg_12202" s="T36">np.h:S</ta>
            <ta e="T39" id="Seg_12203" s="T38">v:pred</ta>
            <ta e="T42" id="Seg_12204" s="T41">v:pred</ta>
            <ta e="T43" id="Seg_12205" s="T42">np.h:S</ta>
            <ta e="T45" id="Seg_12206" s="T44">pro.h:S</ta>
            <ta e="T49" id="Seg_12207" s="T48">v:pred</ta>
            <ta e="T51" id="Seg_12208" s="T50">np.h:S</ta>
            <ta e="T52" id="Seg_12209" s="T51">v:pred</ta>
            <ta e="T57" id="Seg_12210" s="T56">v:pred 0.3.h:S</ta>
            <ta e="T58" id="Seg_12211" s="T57">pro.h:O</ta>
            <ta e="T59" id="Seg_12212" s="T58">pro.h:S</ta>
            <ta e="T61" id="Seg_12213" s="T60">v:pred</ta>
            <ta e="T63" id="Seg_12214" s="T62">np.h:S</ta>
            <ta e="T64" id="Seg_12215" s="T63">v:pred</ta>
            <ta e="T67" id="Seg_12216" s="T66">pro.h:S</ta>
            <ta e="T68" id="Seg_12217" s="T67">v:pred</ta>
            <ta e="T69" id="Seg_12218" s="T68">v:pred 0.1.h:S</ta>
            <ta e="T72" id="Seg_12219" s="T71">np.h:S</ta>
            <ta e="T73" id="Seg_12220" s="T72">v:pred</ta>
            <ta e="T78" id="Seg_12221" s="T77">np:O</ta>
            <ta e="T79" id="Seg_12222" s="T78">v:pred 0.3.h:S</ta>
            <ta e="T80" id="Seg_12223" s="T79">np.h:S</ta>
            <ta e="T81" id="Seg_12224" s="T80">v:pred</ta>
            <ta e="T82" id="Seg_12225" s="T81">v:pred 0.3.h:S</ta>
            <ta e="T83" id="Seg_12226" s="T82">v:pred 0.2.h:S</ta>
            <ta e="T88" id="Seg_12227" s="T87">pro.h:S</ta>
            <ta e="T89" id="Seg_12228" s="T88">v:pred</ta>
            <ta e="T90" id="Seg_12229" s="T89">ptcl.neg</ta>
            <ta e="T91" id="Seg_12230" s="T90">v:pred 0.3.h:S</ta>
            <ta e="T93" id="Seg_12231" s="T92">np:O</ta>
            <ta e="T96" id="Seg_12232" s="T95">v:pred 0.1.h:S</ta>
            <ta e="T110" id="Seg_12233" s="T109">v:pred 0.1.h:S</ta>
            <ta e="T111" id="Seg_12234" s="T110">v:pred 0.1.h:S</ta>
            <ta e="T113" id="Seg_12235" s="T112">v:pred 0.2.h:S</ta>
            <ta e="T114" id="Seg_12236" s="T113">v:pred 0.2.h:S</ta>
            <ta e="T115" id="Seg_12237" s="T114">ptcl.neg</ta>
            <ta e="T116" id="Seg_12238" s="T115">v:pred 0.2.h:S</ta>
            <ta e="T117" id="Seg_12239" s="T116">ptcl.neg</ta>
            <ta e="T118" id="Seg_12240" s="T117">v:pred 0.1.h:S</ta>
            <ta e="T122" id="Seg_12241" s="T121">v:pred 0.3.h:S</ta>
            <ta e="T123" id="Seg_12242" s="T122">v:pred 0.2.h:S</ta>
            <ta e="T125" id="Seg_12243" s="T124">np:O</ta>
            <ta e="T126" id="Seg_12244" s="T125">v:pred 0.2.h:S</ta>
            <ta e="T128" id="Seg_12245" s="T127">pro.h:S</ta>
            <ta e="T129" id="Seg_12246" s="T128">v:pred</ta>
            <ta e="T131" id="Seg_12247" s="T130">v:pred 0.1.h:S</ta>
            <ta e="T132" id="Seg_12248" s="T131">np:S</ta>
            <ta e="T133" id="Seg_12249" s="T132">adj:pred</ta>
            <ta e="T135" id="Seg_12250" s="T134">np:S</ta>
            <ta e="T136" id="Seg_12251" s="T135">v:pred</ta>
            <ta e="T138" id="Seg_12252" s="T137">v:pred 0.1.h:S</ta>
            <ta e="T140" id="Seg_12253" s="T139">np:O</ta>
            <ta e="T141" id="Seg_12254" s="T140">ptcl.neg</ta>
            <ta e="T143" id="Seg_12255" s="T142">v:pred 0.2.h:S</ta>
            <ta e="T145" id="Seg_12256" s="T144">np:S</ta>
            <ta e="T146" id="Seg_12257" s="T145">adj:pred</ta>
            <ta e="T148" id="Seg_12258" s="T147">np:S</ta>
            <ta e="T149" id="Seg_12259" s="T148">v:pred</ta>
            <ta e="T151" id="Seg_12260" s="T150">ptcl:pred</ta>
            <ta e="T155" id="Seg_12261" s="T154">v:pred 0.3.h:S</ta>
            <ta e="T156" id="Seg_12262" s="T155">v:pred 0.2.h:S</ta>
            <ta e="T158" id="Seg_12263" s="T157">v:pred 0.2.h:S</ta>
            <ta e="T159" id="Seg_12264" s="T158">np:O</ta>
            <ta e="T160" id="Seg_12265" s="T159">pro.h:S</ta>
            <ta e="T161" id="Seg_12266" s="T160">v:pred</ta>
            <ta e="T162" id="Seg_12267" s="T161">v:pred 0.1.h:S</ta>
            <ta e="T163" id="Seg_12268" s="T162">v:pred 0.1.h:S</ta>
            <ta e="T164" id="Seg_12269" s="T163">v:pred 0.1.h:S</ta>
            <ta e="T170" id="Seg_12270" s="T169">np:O</ta>
            <ta e="T173" id="Seg_12271" s="T172">v:pred 0.1.h:S</ta>
            <ta e="T175" id="Seg_12272" s="T174">v:pred 0.1.h:S</ta>
            <ta e="T178" id="Seg_12273" s="T177">np.h:S</ta>
            <ta e="T181" id="Seg_12274" s="T180">v:pred</ta>
            <ta e="T184" id="Seg_12275" s="T183">np.h:S</ta>
            <ta e="T188" id="Seg_12276" s="T187">pro.h:O</ta>
            <ta e="T189" id="Seg_12277" s="T188">ptcl.neg</ta>
            <ta e="T190" id="Seg_12278" s="T189">v:pred 0.3.h:S</ta>
            <ta e="T197" id="Seg_12279" s="T196">np.h:S</ta>
            <ta e="T198" id="Seg_12280" s="T197">v:pred</ta>
            <ta e="T200" id="Seg_12281" s="T199">pro.h:S</ta>
            <ta e="T201" id="Seg_12282" s="T200">v:pred</ta>
            <ta e="T202" id="Seg_12283" s="T201">v:pred 0.2.h:S</ta>
            <ta e="T204" id="Seg_12284" s="T203">v:pred 0.2.h:S</ta>
            <ta e="T205" id="Seg_12285" s="T204">np:O</ta>
            <ta e="T207" id="Seg_12286" s="T206">v:pred 0.3.h:S</ta>
            <ta e="T208" id="Seg_12287" s="T207">v:pred 0.2.h:S</ta>
            <ta e="T209" id="Seg_12288" s="T208">pro.h:S</ta>
            <ta e="T210" id="Seg_12289" s="T209">np:O</ta>
            <ta e="T214" id="Seg_12290" s="T213">v:pred</ta>
            <ta e="T217" id="Seg_12291" s="T216">v:pred 0.3.h:S</ta>
            <ta e="T218" id="Seg_12292" s="T217">pro.h:S</ta>
            <ta e="T219" id="Seg_12293" s="T218">ptcl.neg</ta>
            <ta e="T220" id="Seg_12294" s="T219">v:pred</ta>
            <ta e="T227" id="Seg_12295" s="T226">np.h:S</ta>
            <ta e="T230" id="Seg_12296" s="T229">v:pred</ta>
            <ta e="T234" id="Seg_12297" s="T233">v:pred 0.3.h:S</ta>
            <ta e="T237" id="Seg_12298" s="T236">v:pred 0.3.h:S</ta>
            <ta e="T238" id="Seg_12299" s="T237">np:S</ta>
            <ta e="T239" id="Seg_12300" s="T238">v:pred</ta>
            <ta e="T242" id="Seg_12301" s="T241">v:pred 0.3.h:S</ta>
            <ta e="T244" id="Seg_12302" s="T243">np:S</ta>
            <ta e="T246" id="Seg_12303" s="T245">v:pred</ta>
            <ta e="T250" id="Seg_12304" s="T249">np:O</ta>
            <ta e="T251" id="Seg_12305" s="T250">ptcl:pred</ta>
            <ta e="T259" id="Seg_12306" s="T258">v:pred 0.3.h:S</ta>
            <ta e="T261" id="Seg_12307" s="T260">np:O</ta>
            <ta e="T262" id="Seg_12308" s="T261">ptcl:pred</ta>
            <ta e="T266" id="Seg_12309" s="T265">np.h:S</ta>
            <ta e="T267" id="Seg_12310" s="T266">v:pred</ta>
            <ta e="T268" id="Seg_12311" s="T267">np:O</ta>
            <ta e="T269" id="Seg_12312" s="T268">v:pred 0.2.h:S</ta>
            <ta e="T271" id="Seg_12313" s="T270">v:pred 0.3.h:S</ta>
            <ta e="T273" id="Seg_12314" s="T272">pro.h:S</ta>
            <ta e="T274" id="Seg_12315" s="T273">v:pred</ta>
            <ta e="T277" id="Seg_12316" s="T276">np:O</ta>
            <ta e="T278" id="Seg_12317" s="T277">v:pred 0.3.h:S</ta>
            <ta e="T279" id="Seg_12318" s="T278">v:pred 0.3.h:S</ta>
            <ta e="T280" id="Seg_12319" s="T279">v:pred 0.3.h:S</ta>
            <ta e="T281" id="Seg_12320" s="T280">v:pred 0.3.h:S</ta>
            <ta e="T285" id="Seg_12321" s="T284">np:O</ta>
            <ta e="T286" id="Seg_12322" s="T285">v:pred 0.3.h:S</ta>
            <ta e="T287" id="Seg_12323" s="T286">v:pred 0.3.h:S</ta>
            <ta e="T290" id="Seg_12324" s="T289">v:pred 0.3.h:S</ta>
            <ta e="T291" id="Seg_12325" s="T290">np:S</ta>
            <ta e="T292" id="Seg_12326" s="T291">v:pred</ta>
            <ta e="T295" id="Seg_12327" s="T294">v:pred</ta>
            <ta e="T297" id="Seg_12328" s="T296">np:S</ta>
            <ta e="T298" id="Seg_12329" s="T297">pro.h:S</ta>
            <ta e="T299" id="Seg_12330" s="T298">v:pred</ta>
            <ta e="T302" id="Seg_12331" s="T301">v:pred 0.3.h:S</ta>
            <ta e="T303" id="Seg_12332" s="T302">np:O</ta>
            <ta e="T304" id="Seg_12333" s="T303">v:pred 0.2.h:S</ta>
            <ta e="T307" id="Seg_12334" s="T306">np:O</ta>
            <ta e="T308" id="Seg_12335" s="T307">v:pred 0.2.h:S</ta>
            <ta e="T312" id="Seg_12336" s="T311">v:pred 0.3.h:S</ta>
            <ta e="T314" id="Seg_12337" s="T313">np:O</ta>
            <ta e="T315" id="Seg_12338" s="T314">v:pred 0.3.h:S</ta>
            <ta e="T316" id="Seg_12339" s="T315">s:purp</ta>
            <ta e="T319" id="Seg_12340" s="T318">np:O</ta>
            <ta e="T320" id="Seg_12341" s="T319">v:pred 0.3.h:S</ta>
            <ta e="T324" id="Seg_12342" s="T323">np:S</ta>
            <ta e="T325" id="Seg_12343" s="T324">v:pred</ta>
            <ta e="T342" id="Seg_12344" s="T341">np:S</ta>
            <ta e="T344" id="Seg_12345" s="T343">adj:pred</ta>
            <ta e="T346" id="Seg_12346" s="T345">v:pred 0.2.h:S</ta>
            <ta e="T351" id="Seg_12347" s="T350">v:pred 0.2.h:S</ta>
            <ta e="T354" id="Seg_12348" s="T353">v:pred 0.2.h:S</ta>
            <ta e="T365" id="Seg_12349" s="T364">v:pred 0.2.h:S</ta>
            <ta e="T368" id="Seg_12350" s="T367">v:pred 0.2.h:S</ta>
            <ta e="T371" id="Seg_12351" s="T370">v:pred 0.2.h:S</ta>
            <ta e="T376" id="Seg_12352" s="T375">v:pred 0.1.h:S</ta>
            <ta e="T377" id="Seg_12353" s="T376">np:O</ta>
            <ta e="T378" id="Seg_12354" s="T377">v:pred 0.3.h:S</ta>
            <ta e="T379" id="Seg_12355" s="T378">adj:pred</ta>
            <ta e="T380" id="Seg_12356" s="T379">np:S</ta>
            <ta e="T381" id="Seg_12357" s="T380">cop</ta>
            <ta e="T383" id="Seg_12358" s="T382">adj:pred</ta>
            <ta e="T384" id="Seg_12359" s="T383">adj:pred</ta>
            <ta e="T385" id="Seg_12360" s="T384">cop 0.3:S</ta>
            <ta e="T388" id="Seg_12361" s="T387">adj:pred</ta>
            <ta e="T390" id="Seg_12362" s="T389">pro.h:S</ta>
            <ta e="T392" id="Seg_12363" s="T391">v:pred</ta>
            <ta e="T394" id="Seg_12364" s="T393">np:O</ta>
            <ta e="T395" id="Seg_12365" s="T394">v:pred 0.1.h:S</ta>
            <ta e="T398" id="Seg_12366" s="T396">v:pred 0.3.h:S</ta>
            <ta e="T407" id="Seg_12367" s="T406">v:pred 0.3.h:S</ta>
            <ta e="T408" id="Seg_12368" s="T407">np:S</ta>
            <ta e="T409" id="Seg_12369" s="T408">v:pred</ta>
            <ta e="T411" id="Seg_12370" s="T410">pro.h:S</ta>
            <ta e="T412" id="Seg_12371" s="T411">v:pred</ta>
            <ta e="T414" id="Seg_12372" s="T413">v:pred 0.1.h:S</ta>
            <ta e="T416" id="Seg_12373" s="T415">pro.h:S</ta>
            <ta e="T986" id="Seg_12374" s="T416">conv:pred</ta>
            <ta e="T417" id="Seg_12375" s="T986">v:pred</ta>
            <ta e="T418" id="Seg_12376" s="T417">pro.h:S</ta>
            <ta e="T420" id="Seg_12377" s="T419">v:pred</ta>
            <ta e="T423" id="Seg_12378" s="T422">v:pred 0.3.h:S</ta>
            <ta e="T428" id="Seg_12379" s="T427">np.h:S</ta>
            <ta e="T429" id="Seg_12380" s="T428">v:pred</ta>
            <ta e="T432" id="Seg_12381" s="T431">np:S</ta>
            <ta e="T433" id="Seg_12382" s="T432">v:pred</ta>
            <ta e="T434" id="Seg_12383" s="T433">np:O</ta>
            <ta e="T435" id="Seg_12384" s="T434">s:purp</ta>
            <ta e="T437" id="Seg_12385" s="T436">v:pred 0.3.h:S</ta>
            <ta e="T441" id="Seg_12386" s="T440">np:O</ta>
            <ta e="T443" id="Seg_12387" s="T442">v:pred 0.3.h:S</ta>
            <ta e="T445" id="Seg_12388" s="T444">v:pred 0.3.h:S</ta>
            <ta e="T450" id="Seg_12389" s="T449">np.h:S</ta>
            <ta e="T451" id="Seg_12390" s="T450">v:pred</ta>
            <ta e="T453" id="Seg_12391" s="T452">v:pred 0.3.h:S</ta>
            <ta e="T454" id="Seg_12392" s="T453">np.h:O</ta>
            <ta e="T456" id="Seg_12393" s="T455">np.h:O</ta>
            <ta e="T461" id="Seg_12394" s="T460">v:pred 0.3.h:S</ta>
            <ta e="T462" id="Seg_12395" s="T461">v:pred 0.2.h:S</ta>
            <ta e="T463" id="Seg_12396" s="T462">np:O</ta>
            <ta e="T464" id="Seg_12397" s="T463">v:pred 0.2.h:S</ta>
            <ta e="T466" id="Seg_12398" s="T465">v:pred 0.2.h:S</ta>
            <ta e="T467" id="Seg_12399" s="T466">pro.h:S</ta>
            <ta e="T468" id="Seg_12400" s="T467">v:pred</ta>
            <ta e="T469" id="Seg_12401" s="T468">pro.h:S</ta>
            <ta e="T470" id="Seg_12402" s="T469">np:O</ta>
            <ta e="T471" id="Seg_12403" s="T470">v:pred</ta>
            <ta e="T476" id="Seg_12404" s="T475">v:pred 0.3.h:S</ta>
            <ta e="T479" id="Seg_12405" s="T478">np:O</ta>
            <ta e="T480" id="Seg_12406" s="T479">pro.h:S</ta>
            <ta e="T482" id="Seg_12407" s="T481">v:pred</ta>
            <ta e="T487" id="Seg_12408" s="T486">v:pred 0.3.h:S</ta>
            <ta e="T488" id="Seg_12409" s="T487">s:purp</ta>
            <ta e="T491" id="Seg_12410" s="T490">np.h:S</ta>
            <ta e="T493" id="Seg_12411" s="T492">v:pred</ta>
            <ta e="T494" id="Seg_12412" s="T493">ptcl:pred</ta>
            <ta e="T497" id="Seg_12413" s="T496">ptcl:pred</ta>
            <ta e="T498" id="Seg_12414" s="T497">np:O</ta>
            <ta e="T503" id="Seg_12415" s="T502">v:pred 0.3.h:S</ta>
            <ta e="T505" id="Seg_12416" s="T504">v:pred 0.3.h:S</ta>
            <ta e="T507" id="Seg_12417" s="T506">np:O</ta>
            <ta e="T508" id="Seg_12418" s="T507">v:pred 0.3.h:S</ta>
            <ta e="T509" id="Seg_12419" s="T508">np:O</ta>
            <ta e="T510" id="Seg_12420" s="T509">v:pred 0.3.h:S</ta>
            <ta e="T511" id="Seg_12421" s="T510">v:pred 0.3.h:S</ta>
            <ta e="T514" id="Seg_12422" s="T513">pro:O</ta>
            <ta e="T515" id="Seg_12423" s="T514">v:pred 0.3.h:S</ta>
            <ta e="T517" id="Seg_12424" s="T516">pro:O</ta>
            <ta e="T518" id="Seg_12425" s="T517">v:pred 0.3.h:S</ta>
            <ta e="T520" id="Seg_12426" s="T519">v:pred 0.3:S</ta>
            <ta e="T522" id="Seg_12427" s="T521">ptcl.neg</ta>
            <ta e="T523" id="Seg_12428" s="T522">v:pred 0.3.h:S</ta>
            <ta e="T525" id="Seg_12429" s="T524">pro.h:S</ta>
            <ta e="T529" id="Seg_12430" s="T528">v:pred</ta>
            <ta e="T530" id="Seg_12431" s="T529">np:O</ta>
            <ta e="T531" id="Seg_12432" s="T530">v:pred 0.1.h:S</ta>
            <ta e="T534" id="Seg_12433" s="T533">np:O</ta>
            <ta e="T535" id="Seg_12434" s="T534">v:pred 0.1.h:S</ta>
            <ta e="T536" id="Seg_12435" s="T535">np:O</ta>
            <ta e="T537" id="Seg_12436" s="T536">v:pred 0.1.h:S</ta>
            <ta e="T538" id="Seg_12437" s="T537">v:pred 0.1.h:S</ta>
            <ta e="T540" id="Seg_12438" s="T539">v:pred 0.1.h:S</ta>
            <ta e="T541" id="Seg_12439" s="T540">np:O</ta>
            <ta e="T542" id="Seg_12440" s="T541">v:pred 0.1.h:S</ta>
            <ta e="T545" id="Seg_12441" s="T544">v:pred 0.1.h:S</ta>
            <ta e="T550" id="Seg_12442" s="T549">np:O</ta>
            <ta e="T551" id="Seg_12443" s="T550">v:pred 0.1.h:S</ta>
            <ta e="T553" id="Seg_12444" s="T552">v:pred 0.1.h:S</ta>
            <ta e="T556" id="Seg_12445" s="T555">np:O</ta>
            <ta e="T557" id="Seg_12446" s="T556">v:pred 0.1.h:S</ta>
            <ta e="T558" id="Seg_12447" s="T557">pro.h:S</ta>
            <ta e="T561" id="Seg_12448" s="T560">v:pred</ta>
            <ta e="T564" id="Seg_12449" s="T563">np:O</ta>
            <ta e="T565" id="Seg_12450" s="T564">v:pred 0.3.h:S</ta>
            <ta e="T566" id="Seg_12451" s="T565">ptcl.neg</ta>
            <ta e="T567" id="Seg_12452" s="T566">np.h:S</ta>
            <ta e="T568" id="Seg_12453" s="T567">v:pred</ta>
            <ta e="T570" id="Seg_12454" s="T569">v:pred 0.3.h:S</ta>
            <ta e="T574" id="Seg_12455" s="T573">v:pred 0.3.h:S</ta>
            <ta e="T575" id="Seg_12456" s="T574">pro.h:S</ta>
            <ta e="T576" id="Seg_12457" s="T575">v:pred</ta>
            <ta e="T577" id="Seg_12458" s="T576">v:pred 0.2.h:S</ta>
            <ta e="T586" id="Seg_12459" s="T585">ptcl.neg</ta>
            <ta e="T587" id="Seg_12460" s="T586">v:pred 0.3.h:S</ta>
            <ta e="T593" id="Seg_12461" s="T592">v:pred 0.1.h:S</ta>
            <ta e="T596" id="Seg_12462" s="T595">ptcl.neg</ta>
            <ta e="T597" id="Seg_12463" s="T596">v:pred 0.1.h:S</ta>
            <ta e="T598" id="Seg_12464" s="T597">v:pred 0.1.h:S</ta>
            <ta e="T601" id="Seg_12465" s="T600">pro.h:S</ta>
            <ta e="T605" id="Seg_12466" s="T604">v:pred</ta>
            <ta e="T609" id="Seg_12467" s="T608">np.h:S</ta>
            <ta e="T611" id="Seg_12468" s="T610">v:pred</ta>
            <ta e="T615" id="Seg_12469" s="T614">np.h:S</ta>
            <ta e="T618" id="Seg_12470" s="T617">pro:O</ta>
            <ta e="T619" id="Seg_12471" s="T618">v:pred</ta>
            <ta e="T620" id="Seg_12472" s="T619">pro.h:S</ta>
            <ta e="T621" id="Seg_12473" s="T620">v:pred</ta>
            <ta e="T622" id="Seg_12474" s="T621">v:pred 0.2.h:S</ta>
            <ta e="T626" id="Seg_12475" s="T625">pro:O</ta>
            <ta e="T627" id="Seg_12476" s="T626">ptcl.neg</ta>
            <ta e="T628" id="Seg_12477" s="T627">v:pred</ta>
            <ta e="T630" id="Seg_12478" s="T629">pro.h:S</ta>
            <ta e="T632" id="Seg_12479" s="T631">v:pred</ta>
            <ta e="T636" id="Seg_12480" s="T635">ptcl:pred</ta>
            <ta e="T638" id="Seg_12481" s="T637">v:pred 0.2.h:S</ta>
            <ta e="T640" id="Seg_12482" s="T639">pro.h:S</ta>
            <ta e="T642" id="Seg_12483" s="T641">pro.h:O</ta>
            <ta e="T643" id="Seg_12484" s="T642">v:pred</ta>
            <ta e="T645" id="Seg_12485" s="T644">np:O</ta>
            <ta e="T646" id="Seg_12486" s="T645">v:pred 0.3.h:S</ta>
            <ta e="T649" id="Seg_12487" s="T648">v:pred 0.3.h:S</ta>
            <ta e="T653" id="Seg_12488" s="T652">v:pred 0.1.h:S</ta>
            <ta e="T655" id="Seg_12489" s="T654">np.h:S</ta>
            <ta e="T656" id="Seg_12490" s="T655">v:pred</ta>
            <ta e="T658" id="Seg_12491" s="T657">v:pred 0.2.h:S</ta>
            <ta e="T660" id="Seg_12492" s="T659">v:pred 0.2.h:S</ta>
            <ta e="T662" id="Seg_12493" s="T661">v:pred 0.2.h:S</ta>
            <ta e="T663" id="Seg_12494" s="T662">np:O</ta>
            <ta e="T664" id="Seg_12495" s="T663">v:pred 0.1.h:S</ta>
            <ta e="T666" id="Seg_12496" s="T665">pro.h:S</ta>
            <ta e="T668" id="Seg_12497" s="T667">v:pred</ta>
            <ta e="T669" id="Seg_12498" s="T668">v:pred 0.2.h:S</ta>
            <ta e="T672" id="Seg_12499" s="T671">np:O</ta>
            <ta e="T673" id="Seg_12500" s="T672">pro.h:S</ta>
            <ta e="T674" id="Seg_12501" s="T673">v:pred</ta>
            <ta e="T679" id="Seg_12502" s="T678">pro.h:S</ta>
            <ta e="T680" id="Seg_12503" s="T679">v:pred</ta>
            <ta e="T682" id="Seg_12504" s="T681">ptcl.neg</ta>
            <ta e="T688" id="Seg_12505" s="T687">pro.h:S</ta>
            <ta e="T690" id="Seg_12506" s="T689">v:pred</ta>
            <ta e="T693" id="Seg_12507" s="T692">np:O</ta>
            <ta e="T696" id="Seg_12508" s="T695">pro.h:S</ta>
            <ta e="T697" id="Seg_12509" s="T696">v:pred</ta>
            <ta e="T698" id="Seg_12510" s="T697">np:O</ta>
            <ta e="T700" id="Seg_12511" s="T699">np:O</ta>
            <ta e="T701" id="Seg_12512" s="T700">v:pred 0.1.h:S</ta>
            <ta e="T702" id="Seg_12513" s="T701">np:O</ta>
            <ta e="T703" id="Seg_12514" s="T702">v:pred 0.1.h:S</ta>
            <ta e="T705" id="Seg_12515" s="T704">conv:pred</ta>
            <ta e="T706" id="Seg_12516" s="T705">v:pred 0.1.h:S</ta>
            <ta e="T709" id="Seg_12517" s="T708">v:pred 0.1.h:S</ta>
            <ta e="T711" id="Seg_12518" s="T710">np:S</ta>
            <ta e="T712" id="Seg_12519" s="T711">v:pred</ta>
            <ta e="T716" id="Seg_12520" s="T715">v:pred 0.1.h:S</ta>
            <ta e="T719" id="Seg_12521" s="T718">v:pred 0.1.h:S</ta>
            <ta e="T724" id="Seg_12522" s="T723">pro.h:S</ta>
            <ta e="T726" id="Seg_12523" s="T725">v:pred</ta>
            <ta e="T728" id="Seg_12524" s="T727">s:purp</ta>
            <ta e="T732" id="Seg_12525" s="T731">ptcl:pred</ta>
            <ta e="T735" id="Seg_12526" s="T734">np:S</ta>
            <ta e="T737" id="Seg_12527" s="T736">v:pred</ta>
            <ta e="T738" id="Seg_12528" s="T737">ptcl.neg</ta>
            <ta e="T739" id="Seg_12529" s="T738">v:pred 0.3:S</ta>
            <ta e="T741" id="Seg_12530" s="T740">pro.h:S</ta>
            <ta e="T742" id="Seg_12531" s="T741">v:pred</ta>
            <ta e="T744" id="Seg_12532" s="T743">pro.h:S</ta>
            <ta e="T987" id="Seg_12533" s="T744">conv:pred</ta>
            <ta e="T745" id="Seg_12534" s="T987">v:pred</ta>
            <ta e="T746" id="Seg_12535" s="T745">ptcl.neg</ta>
            <ta e="T747" id="Seg_12536" s="T746">v:pred 0.1.h:S</ta>
            <ta e="T750" id="Seg_12537" s="T749">v:pred 0.3.h:S</ta>
            <ta e="T753" id="Seg_12538" s="T752">s:purp</ta>
            <ta e="T755" id="Seg_12539" s="T754">v:pred 0.3.h:S</ta>
            <ta e="T757" id="Seg_12540" s="T756">pro.h:S</ta>
            <ta e="T758" id="Seg_12541" s="T757">v:pred</ta>
            <ta e="T760" id="Seg_12542" s="T759">v:pred 0.2.h:S</ta>
            <ta e="T763" id="Seg_12543" s="T762">v:pred 0.1.h:S</ta>
            <ta e="T766" id="Seg_12544" s="T765">v:pred 0.1.h:S</ta>
            <ta e="T771" id="Seg_12545" s="T770">v:pred 0.1.h:S</ta>
            <ta e="T774" id="Seg_12546" s="T773">v:pred 0.1.h:S</ta>
            <ta e="T777" id="Seg_12547" s="T776">np.h:S</ta>
            <ta e="T778" id="Seg_12548" s="T777">v:pred</ta>
            <ta e="T780" id="Seg_12549" s="T779">ptcl:pred</ta>
            <ta e="T783" id="Seg_12550" s="T782">pro:O</ta>
            <ta e="T784" id="Seg_12551" s="T783">ptcl.neg</ta>
            <ta e="T785" id="Seg_12552" s="T784">v:pred 0.3.h:S</ta>
            <ta e="T786" id="Seg_12553" s="T785">ptcl:pred</ta>
            <ta e="T787" id="Seg_12554" s="T786">np:O</ta>
            <ta e="T788" id="Seg_12555" s="T787">v:pred 0.2.h:S</ta>
            <ta e="T791" id="Seg_12556" s="T790">np.h:S</ta>
            <ta e="T792" id="Seg_12557" s="T791">v:pred</ta>
            <ta e="T793" id="Seg_12558" s="T792">pro:O</ta>
            <ta e="T794" id="Seg_12559" s="T793">ptcl.neg</ta>
            <ta e="T795" id="Seg_12560" s="T794">v:pred 0.3.h:S</ta>
            <ta e="T796" id="Seg_12561" s="T795">pro.h:S</ta>
            <ta e="T797" id="Seg_12562" s="T796">v:pred</ta>
            <ta e="T799" id="Seg_12563" s="T798">ptcl:pred</ta>
            <ta e="T802" id="Seg_12564" s="T801">v:pred 0.3.h:S</ta>
            <ta e="T804" id="Seg_12565" s="T803">v:pred 0.3.h:S</ta>
            <ta e="T807" id="Seg_12566" s="T806">v:pred 0.3.h:S</ta>
            <ta e="T808" id="Seg_12567" s="T807">v:pred 0.2.h:S</ta>
            <ta e="T810" id="Seg_12568" s="T809">pro.h:S</ta>
            <ta e="T811" id="Seg_12569" s="T810">v:pred</ta>
            <ta e="T812" id="Seg_12570" s="T811">s:purp</ta>
            <ta e="T814" id="Seg_12571" s="T813">np.h:S</ta>
            <ta e="T815" id="Seg_12572" s="T814">v:pred</ta>
            <ta e="T816" id="Seg_12573" s="T815">s:purp</ta>
            <ta e="T818" id="Seg_12574" s="T817">np.h:S</ta>
            <ta e="T819" id="Seg_12575" s="T818">v:pred</ta>
            <ta e="T820" id="Seg_12576" s="T819">v:pred 0.3.h:S</ta>
            <ta e="T822" id="Seg_12577" s="T821">np.h:S</ta>
            <ta e="T823" id="Seg_12578" s="T822">v:pred</ta>
            <ta e="T824" id="Seg_12579" s="T823">v:pred 0.3.h:S</ta>
            <ta e="T825" id="Seg_12580" s="T824">np:O</ta>
            <ta e="T826" id="Seg_12581" s="T825">np:O</ta>
            <ta e="T827" id="Seg_12582" s="T826">v:pred 0.3.h:S</ta>
            <ta e="T830" id="Seg_12583" s="T829">v:pred 0.3.h:S</ta>
            <ta e="T831" id="Seg_12584" s="T830">np:O</ta>
            <ta e="T832" id="Seg_12585" s="T831">v:pred 0.3.h:S</ta>
            <ta e="T834" id="Seg_12586" s="T833">np.h:S</ta>
            <ta e="T835" id="Seg_12587" s="T834">pro:O</ta>
            <ta e="T836" id="Seg_12588" s="T835">v:pred</ta>
            <ta e="T839" id="Seg_12589" s="T838">pro.h:S</ta>
            <ta e="T841" id="Seg_12590" s="T840">np:O</ta>
            <ta e="T842" id="Seg_12591" s="T841">v:pred</ta>
            <ta e="T844" id="Seg_12592" s="T843">v:pred 0.3.h:S</ta>
            <ta e="T846" id="Seg_12593" s="T845">v:pred 0.3.h:S</ta>
            <ta e="T850" id="Seg_12594" s="T849">np.h:S</ta>
            <ta e="T852" id="Seg_12595" s="T851">adj:pred</ta>
            <ta e="T853" id="Seg_12596" s="T852">np:O</ta>
            <ta e="T854" id="Seg_12597" s="T853">v:pred 0.3.h:S</ta>
            <ta e="T856" id="Seg_12598" s="T855">np:O</ta>
            <ta e="T859" id="Seg_12599" s="T858">v:pred 0.3.h:S</ta>
            <ta e="T861" id="Seg_12600" s="T860">np:S</ta>
            <ta e="T863" id="Seg_12601" s="T862">v:pred</ta>
            <ta e="T865" id="Seg_12602" s="T864">pro.h:S</ta>
            <ta e="T867" id="Seg_12603" s="T866">np:O</ta>
            <ta e="T868" id="Seg_12604" s="T867">ptcl.neg</ta>
            <ta e="T869" id="Seg_12605" s="T868">v:pred</ta>
            <ta e="T872" id="Seg_12606" s="T871">v:pred 0.1.h:S</ta>
            <ta e="T877" id="Seg_12607" s="T876">pro.h:S</ta>
            <ta e="T879" id="Seg_12608" s="T878">v:pred</ta>
            <ta e="T880" id="Seg_12609" s="T879">np:O</ta>
            <ta e="T889" id="Seg_12610" s="T888">np.h:S</ta>
            <ta e="T890" id="Seg_12611" s="T889">v:pred</ta>
            <ta e="T891" id="Seg_12612" s="T890">np:S</ta>
            <ta e="T892" id="Seg_12613" s="T891">adj:pred</ta>
            <ta e="T894" id="Seg_12614" s="T893">np:S</ta>
            <ta e="T896" id="Seg_12615" s="T895">pro.h:S</ta>
            <ta e="T898" id="Seg_12616" s="T897">v:pred</ta>
            <ta e="T903" id="Seg_12617" s="T902">v:pred 0.1.h:S</ta>
            <ta e="T905" id="Seg_12618" s="T904">ptcl:pred</ta>
            <ta e="T913" id="Seg_12619" s="T912">np:S</ta>
            <ta e="T916" id="Seg_12620" s="T915">v:pred</ta>
            <ta e="T918" id="Seg_12621" s="T917">adj:pred</ta>
            <ta e="T919" id="Seg_12622" s="T918">cop 0.3:S</ta>
            <ta e="T924" id="Seg_12623" s="T923">v:pred 0.1.h:S</ta>
            <ta e="T926" id="Seg_12624" s="T925">adj:pred</ta>
            <ta e="T927" id="Seg_12625" s="T926">cop 0.3:S</ta>
            <ta e="T931" id="Seg_12626" s="T930">ptcl:pred</ta>
            <ta e="T935" id="Seg_12627" s="T934">pro.h:S</ta>
            <ta e="T936" id="Seg_12628" s="T935">np:O</ta>
            <ta e="T937" id="Seg_12629" s="T936">v:pred</ta>
            <ta e="T940" id="Seg_12630" s="T939">v:pred 0.3.h:S</ta>
            <ta e="T944" id="Seg_12631" s="T943">ptcl:pred</ta>
            <ta e="T945" id="Seg_12632" s="T944">np:O</ta>
            <ta e="T948" id="Seg_12633" s="T947">ptcl.neg</ta>
            <ta e="T953" id="Seg_12634" s="T952">v:pred 0.3.h:S</ta>
            <ta e="T955" id="Seg_12635" s="T954">v:pred 0.3.h:S</ta>
            <ta e="T957" id="Seg_12636" s="T956">ptcl.neg</ta>
            <ta e="T958" id="Seg_12637" s="T957">v:pred 0.1.h:S</ta>
            <ta e="T959" id="Seg_12638" s="T958">v:pred 0.3.h:S</ta>
            <ta e="T961" id="Seg_12639" s="T960">ptcl.neg</ta>
            <ta e="T962" id="Seg_12640" s="T961">ptcl:pred</ta>
            <ta e="T967" id="Seg_12641" s="T966">v:pred 0.3.h:S</ta>
            <ta e="T969" id="Seg_12642" s="T968">v:pred 0.2.h:S</ta>
            <ta e="T970" id="Seg_12643" s="T969">v:pred 0.2.h:S</ta>
            <ta e="T972" id="Seg_12644" s="T971">pro.h:S</ta>
            <ta e="T974" id="Seg_12645" s="T973">v:pred</ta>
            <ta e="T979" id="Seg_12646" s="T978">ptcl:pred</ta>
            <ta e="T980" id="Seg_12647" s="T979">v:pred</ta>
            <ta e="T981" id="Seg_12648" s="T980">s:purp</ta>
            <ta e="T982" id="Seg_12649" s="T981">pro.h:S</ta>
            <ta e="T983" id="Seg_12650" s="T982">ptcl.neg</ta>
            <ta e="T984" id="Seg_12651" s="T983">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T3" id="Seg_12652" s="T2">TURK:core</ta>
            <ta e="T10" id="Seg_12653" s="T9">RUS:cult</ta>
            <ta e="T11" id="Seg_12654" s="T10">RUS:gram</ta>
            <ta e="T14" id="Seg_12655" s="T13">RUS:disc</ta>
            <ta e="T17" id="Seg_12656" s="T16">RUS:gram</ta>
            <ta e="T18" id="Seg_12657" s="T17">RUS:mod</ta>
            <ta e="T22" id="Seg_12658" s="T21">RUS:cult</ta>
            <ta e="T23" id="Seg_12659" s="T22">TAT:cult</ta>
            <ta e="T29" id="Seg_12660" s="T28">TURK:core</ta>
            <ta e="T36" id="Seg_12661" s="T35">RUS:gram</ta>
            <ta e="T65" id="Seg_12662" s="T64">RUS:gram</ta>
            <ta e="T72" id="Seg_12663" s="T71">RUS:core</ta>
            <ta e="T74" id="Seg_12664" s="T988">TAT:cult</ta>
            <ta e="T76" id="Seg_12665" s="T75">RUS:gram</ta>
            <ta e="T77" id="Seg_12666" s="T76">RUS:core</ta>
            <ta e="T80" id="Seg_12667" s="T79">TURK:disc</ta>
            <ta e="T85" id="Seg_12668" s="T84">RUS:gram</ta>
            <ta e="T87" id="Seg_12669" s="T86">RUS:gram</ta>
            <ta e="T95" id="Seg_12670" s="T94">RUS:cult</ta>
            <ta e="T97" id="Seg_12671" s="T96">TURK:disc</ta>
            <ta e="T98" id="Seg_12672" s="T97">RUS:cult</ta>
            <ta e="T124" id="Seg_12673" s="T123">RUS:cult</ta>
            <ta e="T127" id="Seg_12674" s="T126">RUS:cult</ta>
            <ta e="T134" id="Seg_12675" s="T133">RUS:gram</ta>
            <ta e="T144" id="Seg_12676" s="T143">RUS:disc</ta>
            <ta e="T147" id="Seg_12677" s="T146">RUS:gram</ta>
            <ta e="T151" id="Seg_12678" s="T150">RUS:gram</ta>
            <ta e="T157" id="Seg_12679" s="T156">RUS:cult</ta>
            <ta e="T159" id="Seg_12680" s="T158">RUS:cult</ta>
            <ta e="T166" id="Seg_12681" s="T165">RUS:mod</ta>
            <ta e="T167" id="Seg_12682" s="T166">RUS:gram(INDEF)</ta>
            <ta e="T174" id="Seg_12683" s="T173">RUS:gram</ta>
            <ta e="T176" id="Seg_12684" s="T175">TAT:cult</ta>
            <ta e="T179" id="Seg_12685" s="T178">RUS:cult</ta>
            <ta e="T203" id="Seg_12686" s="T202">RUS:gram</ta>
            <ta e="T215" id="Seg_12687" s="T214">RUS:gram</ta>
            <ta e="T221" id="Seg_12688" s="T220">RUS:gram</ta>
            <ta e="T229" id="Seg_12689" s="T228">TAT:cult</ta>
            <ta e="T236" id="Seg_12690" s="T235">RUS:gram</ta>
            <ta e="T244" id="Seg_12691" s="T243">RUS:cult</ta>
            <ta e="T245" id="Seg_12692" s="T244">TURK:disc</ta>
            <ta e="T251" id="Seg_12693" s="T250">RUS:mod</ta>
            <ta e="T253" id="Seg_12694" s="T252">RUS:gram</ta>
            <ta e="T258" id="Seg_12695" s="T257">TURK:disc</ta>
            <ta e="T261" id="Seg_12696" s="T260">RUS:cult</ta>
            <ta e="T262" id="Seg_12697" s="T261">RUS:mod</ta>
            <ta e="T268" id="Seg_12698" s="T267">%TURK:cult</ta>
            <ta e="T275" id="Seg_12699" s="T274">TURK:disc</ta>
            <ta e="T276" id="Seg_12700" s="T275">RUS:cult</ta>
            <ta e="T282" id="Seg_12701" s="T281">RUS:gram</ta>
            <ta e="T288" id="Seg_12702" s="T287">TAT:cult</ta>
            <ta e="T289" id="Seg_12703" s="T288">RUS:gram</ta>
            <ta e="T291" id="Seg_12704" s="T290">RUS:cult</ta>
            <ta e="T293" id="Seg_12705" s="T292">TURK:disc</ta>
            <ta e="T294" id="Seg_12706" s="T293">%TURK:cult</ta>
            <ta e="T296" id="Seg_12707" s="T295">TURK:disc</ta>
            <ta e="T300" id="Seg_12708" s="T299">TURK:disc</ta>
            <ta e="T305" id="Seg_12709" s="T304">RUS:gram</ta>
            <ta e="T317" id="Seg_12710" s="T316">RUS:gram</ta>
            <ta e="T347" id="Seg_12711" s="T346">RUS:gram</ta>
            <ta e="T348" id="Seg_12712" s="T347">TURK:disc</ta>
            <ta e="T353" id="Seg_12713" s="T352">RUS:cult</ta>
            <ta e="T355" id="Seg_12714" s="T354">TURK:core</ta>
            <ta e="T357" id="Seg_12715" s="T356">TURK:core</ta>
            <ta e="T359" id="Seg_12716" s="T358">RUS:gram</ta>
            <ta e="T369" id="Seg_12717" s="T368">RUS:mod</ta>
            <ta e="T372" id="Seg_12718" s="T371">RUS:disc</ta>
            <ta e="T375" id="Seg_12719" s="T374">TURK:core</ta>
            <ta e="T382" id="Seg_12720" s="T381">RUS:gram</ta>
            <ta e="T393" id="Seg_12721" s="T392">TURK:disc</ta>
            <ta e="T410" id="Seg_12722" s="T409">RUS:gram</ta>
            <ta e="T413" id="Seg_12723" s="T412">RUS:gram</ta>
            <ta e="T415" id="Seg_12724" s="T414">RUS:gram</ta>
            <ta e="T419" id="Seg_12725" s="T418">TURK:disc</ta>
            <ta e="T421" id="Seg_12726" s="T420">RUS:gram</ta>
            <ta e="T427" id="Seg_12727" s="T426">RUS:core</ta>
            <ta e="T430" id="Seg_12728" s="T429">RUS:cult</ta>
            <ta e="T432" id="Seg_12729" s="T431">RUS:cult</ta>
            <ta e="T439" id="Seg_12730" s="T438">RUS:gram</ta>
            <ta e="T442" id="Seg_12731" s="T441">TURK:disc</ta>
            <ta e="T444" id="Seg_12732" s="T443">RUS:gram</ta>
            <ta e="T448" id="Seg_12733" s="T447">RUS:core</ta>
            <ta e="T449" id="Seg_12734" s="T448">RUS:cult</ta>
            <ta e="T455" id="Seg_12735" s="T454">RUS:gram</ta>
            <ta e="T470" id="Seg_12736" s="T469">TURK:cult</ta>
            <ta e="T489" id="Seg_12737" s="T488">RUS:gram</ta>
            <ta e="T494" id="Seg_12738" s="T493">RUS:gram</ta>
            <ta e="T497" id="Seg_12739" s="T496">RUS:mod</ta>
            <ta e="T501" id="Seg_12740" s="T500">TURK:cult</ta>
            <ta e="T521" id="Seg_12741" s="T520">TURK:cult</ta>
            <ta e="T530" id="Seg_12742" s="T529">RUS:cult</ta>
            <ta e="T533" id="Seg_12743" s="T532">TURK:cult</ta>
            <ta e="T539" id="Seg_12744" s="T538">TURK:cult</ta>
            <ta e="T541" id="Seg_12745" s="T540">TURK:cult</ta>
            <ta e="T543" id="Seg_12746" s="T542">RUS:gram</ta>
            <ta e="T559" id="Seg_12747" s="T558">TURK:disc</ta>
            <ta e="T564" id="Seg_12748" s="T563">TAT:cult</ta>
            <ta e="T566" id="Seg_12749" s="T565">TURK:disc</ta>
            <ta e="T572" id="Seg_12750" s="T571">RUS:cult</ta>
            <ta e="T573" id="Seg_12751" s="T572">TURK:disc</ta>
            <ta e="T581" id="Seg_12752" s="T580">RUS:gram</ta>
            <ta e="T583" id="Seg_12753" s="T582">TURK:disc</ta>
            <ta e="T602" id="Seg_12754" s="T601">TURK:disc</ta>
            <ta e="T610" id="Seg_12755" s="T609">TURK:disc</ta>
            <ta e="T614" id="Seg_12756" s="T613">RUS:gram</ta>
            <ta e="T616" id="Seg_12757" s="T615">TURK:disc</ta>
            <ta e="T618" id="Seg_12758" s="T617">RUS:gram(INDEF)</ta>
            <ta e="T619" id="Seg_12759" s="T618">%TURK:core</ta>
            <ta e="T626" id="Seg_12760" s="T625">TURK:gram(INDEF)</ta>
            <ta e="T629" id="Seg_12761" s="T628">RUS:gram</ta>
            <ta e="T633" id="Seg_12762" s="T632">RUS:gram</ta>
            <ta e="T635" id="Seg_12763" s="T634">TURK:disc</ta>
            <ta e="T636" id="Seg_12764" s="T635">RUS:gram</ta>
            <ta e="T647" id="Seg_12765" s="T646">RUS:gram</ta>
            <ta e="T648" id="Seg_12766" s="T647">RUS:mod</ta>
            <ta e="T657" id="Seg_12767" s="T656">RUS:cult</ta>
            <ta e="T659" id="Seg_12768" s="T658">RUS:cult</ta>
            <ta e="T665" id="Seg_12769" s="T664">RUS:gram</ta>
            <ta e="T678" id="Seg_12770" s="T677">RUS:gram</ta>
            <ta e="T684" id="Seg_12771" s="T683">RUS:gram</ta>
            <ta e="T685" id="Seg_12772" s="T684">RUS:cult</ta>
            <ta e="T693" id="Seg_12773" s="T692">RUS:cult</ta>
            <ta e="T698" id="Seg_12774" s="T697">TURK:cult</ta>
            <ta e="T707" id="Seg_12775" s="T706">RUS:gram</ta>
            <ta e="T710" id="Seg_12776" s="T709">TAT:cult</ta>
            <ta e="T713" id="Seg_12777" s="T712">RUS:cult</ta>
            <ta e="T717" id="Seg_12778" s="T716">RUS:cult</ta>
            <ta e="T727" id="Seg_12779" s="T726">RUS:gram</ta>
            <ta e="T728" id="Seg_12780" s="T727">%TURK:core</ta>
            <ta e="T731" id="Seg_12781" s="T730">%TURK:core</ta>
            <ta e="T732" id="Seg_12782" s="T731">RUS:mod</ta>
            <ta e="T733" id="Seg_12783" s="T732">RUS:gram</ta>
            <ta e="T735" id="Seg_12784" s="T734">RUS:cult</ta>
            <ta e="T736" id="Seg_12785" s="T735">TURK:disc</ta>
            <ta e="T740" id="Seg_12786" s="T739">%TURK:core</ta>
            <ta e="T743" id="Seg_12787" s="T742">RUS:gram</ta>
            <ta e="T747" id="Seg_12788" s="T746">%TURK:core</ta>
            <ta e="T748" id="Seg_12789" s="T747">RUS:gram</ta>
            <ta e="T753" id="Seg_12790" s="T752">%TURK:core</ta>
            <ta e="T756" id="Seg_12791" s="T755">RUS:gram</ta>
            <ta e="T761" id="Seg_12792" s="T760">RUS:disc</ta>
            <ta e="T763" id="Seg_12793" s="T762">RUS:calq</ta>
            <ta e="T765" id="Seg_12794" s="T764">TAT:cult</ta>
            <ta e="T766" id="Seg_12795" s="T765">RUS:calq</ta>
            <ta e="T767" id="Seg_12796" s="T766">RUS:gram</ta>
            <ta e="T769" id="Seg_12797" s="T768">TAT:cult</ta>
            <ta e="T771" id="Seg_12798" s="T770">RUS:calq</ta>
            <ta e="T772" id="Seg_12799" s="T771">RUS:gram</ta>
            <ta e="T779" id="Seg_12800" s="T778">RUS:cult</ta>
            <ta e="T780" id="Seg_12801" s="T779">RUS:gram</ta>
            <ta e="T783" id="Seg_12802" s="T782">TURK:gram(INDEF)</ta>
            <ta e="T786" id="Seg_12803" s="T785">RUS:disc</ta>
            <ta e="T789" id="Seg_12804" s="T788">RUS:cult</ta>
            <ta e="T790" id="Seg_12805" s="T789">RUS:gram</ta>
            <ta e="T793" id="Seg_12806" s="T792">TURK:gram(INDEF)</ta>
            <ta e="T798" id="Seg_12807" s="T797">RUS:gram</ta>
            <ta e="T799" id="Seg_12808" s="T798">RUS:gram</ta>
            <ta e="T825" id="Seg_12809" s="T824">RUS:cult</ta>
            <ta e="T831" id="Seg_12810" s="T830">TURK:cult</ta>
            <ta e="T841" id="Seg_12811" s="T840">TURK:cult</ta>
            <ta e="T855" id="Seg_12812" s="T854">RUS:gram</ta>
            <ta e="T862" id="Seg_12813" s="T861">TURK:disc</ta>
            <ta e="T864" id="Seg_12814" s="T863">RUS:gram</ta>
            <ta e="T874" id="Seg_12815" s="T873">TURK:disc</ta>
            <ta e="T878" id="Seg_12816" s="T877">TAT:cult</ta>
            <ta e="T879" id="Seg_12817" s="T878">RUS:calq</ta>
            <ta e="T880" id="Seg_12818" s="T879">RUS:cult</ta>
            <ta e="T882" id="Seg_12819" s="T880">RUS:gram</ta>
            <ta e="T893" id="Seg_12820" s="T892">RUS:gram</ta>
            <ta e="T895" id="Seg_12821" s="T894">RUS:gram</ta>
            <ta e="T897" id="Seg_12822" s="T896">TURK:disc</ta>
            <ta e="T901" id="Seg_12823" s="T900">RUS:mod</ta>
            <ta e="T903" id="Seg_12824" s="T902">%TURK:core</ta>
            <ta e="T905" id="Seg_12825" s="T904">RUS:mod</ta>
            <ta e="T914" id="Seg_12826" s="T913">TURK:disc</ta>
            <ta e="T925" id="Seg_12827" s="T924">TURK:disc</ta>
            <ta e="T931" id="Seg_12828" s="T930">RUS:mod</ta>
            <ta e="T934" id="Seg_12829" s="T933">RUS:gram</ta>
            <ta e="T939" id="Seg_12830" s="T938">RUS:gram</ta>
            <ta e="T944" id="Seg_12831" s="T943">RUS:mod</ta>
            <ta e="T954" id="Seg_12832" s="T953">RUS:gram</ta>
            <ta e="T960" id="Seg_12833" s="T959">RUS:gram</ta>
            <ta e="T962" id="Seg_12834" s="T961">RUS:mod</ta>
            <ta e="T965" id="Seg_12835" s="T964">%TURK:core</ta>
            <ta e="T966" id="Seg_12836" s="T965">RUS:mod</ta>
            <ta e="T968" id="Seg_12837" s="T967">RUS:disc</ta>
            <ta e="T970" id="Seg_12838" s="T969">TURK:core</ta>
            <ta e="T973" id="Seg_12839" s="T972">RUS:mod</ta>
            <ta e="T975" id="Seg_12840" s="T974">RUS:cult</ta>
            <ta e="T979" id="Seg_12841" s="T978">RUS:mod</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T104" id="Seg_12842" s="T99">RUS:int</ta>
            <ta e="T106" id="Seg_12843" s="T104">RUS:ext</ta>
            <ta e="T172" id="Seg_12844" s="T171">RUS:int</ta>
            <ta e="T337" id="Seg_12845" s="T336">RUS:int</ta>
            <ta e="T360" id="Seg_12846" s="T359">RUS:int</ta>
            <ta e="T404" id="Seg_12847" s="T398">RUS:ext</ta>
            <ta e="T465" id="Seg_12848" s="T464">RUS:int</ta>
            <ta e="T474" id="Seg_12849" s="T473">RUS:int</ta>
            <ta e="T580" id="Seg_12850" s="T578">RUS:ext</ta>
            <ta e="T977" id="Seg_12851" s="T976">RUS:int</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_12852" s="T0">Идите вместе в баню.</ta>
            <ta e="T10" id="Seg_12853" s="T4">Он тебе потрет спину мылом, мочалкой.</ta>
            <ta e="T13" id="Seg_12854" s="T10">А ты ему.</ta>
            <ta e="T16" id="Seg_12855" s="T13">Да я стесняюсь.</ta>
            <ta e="T21" id="Seg_12856" s="T16">Не хочу идти, очень стесняюсь.</ta>
            <ta e="T29" id="Seg_12857" s="T21">В Агинском не ходят вместе в баню.</ta>
            <ta e="T31" id="Seg_12858" s="T29">Женщины и мужчины.</ta>
            <ta e="T35" id="Seg_12859" s="T31">Женщины одни ходят.</ta>
            <ta e="T40" id="Seg_12860" s="T35">А мужчины на следующий день снова топят [баню].</ta>
            <ta e="T43" id="Seg_12861" s="T40">Одни мужчины идут.</ta>
            <ta e="T49" id="Seg_12862" s="T44">Мы девочки в баню вдвоем идем.</ta>
            <ta e="T56" id="Seg_12863" s="T49">Потом мать пойдет, одна девочка, один мальчик.</ta>
            <ta e="T58" id="Seg_12864" s="T56">Она их помоет.</ta>
            <ta e="T64" id="Seg_12865" s="T58">Они идут, потом муж идет.</ta>
            <ta e="T69" id="Seg_12866" s="T64">А потом я иду и моюсь.</ta>
            <ta e="T74" id="Seg_12867" s="T70">Моя тетка поехала в Агинское.</ta>
            <ta e="T79" id="Seg_12868" s="T74">Там как раз затопили баню.</ta>
            <ta e="T84" id="Seg_12869" s="T79">Они все помылись и говорят: "Иди в баню!"</ta>
            <ta e="T89" id="Seg_12870" s="T84">Она пошла.</ta>
            <ta e="T94" id="Seg_12871" s="T89">Не знает, куда воду лить.</ta>
            <ta e="T104" id="Seg_12872" s="T94">"На печь полила, все стены облила…"</ta>
            <ta e="T108" id="Seg_12873" s="T104">По-русски (?).</ta>
            <ta e="T111" id="Seg_12874" s="T108">Потом я помылась, вышла.</ta>
            <ta e="T116" id="Seg_12875" s="T111">Почему ты мылась-мылась и не (положила?)?</ta>
            <ta e="T118" id="Seg_12876" s="T116">Я не (положила?)!</ta>
            <ta e="T123" id="Seg_12877" s="T119">Потом они говорят: "Иди!</ta>
            <ta e="T126" id="Seg_12878" s="T123">[Из] подвала мясо принеси.</ta>
            <ta e="T127" id="Seg_12879" s="T126">На котлеты".</ta>
            <ta e="T129" id="Seg_12880" s="T127">Я пошла.</ta>
            <ta e="T136" id="Seg_12881" s="T129">Стою, мяса много, а таза нет.</ta>
            <ta e="T138" id="Seg_12882" s="T136">Тогда я вернулась.</ta>
            <ta e="T143" id="Seg_12883" s="T138">"Ты что мяса не несешь?"</ta>
            <ta e="T149" id="Seg_12884" s="T143">"Так мяса много, а таза нет".</ta>
            <ta e="T152" id="Seg_12885" s="T149">Они давай хохотать!</ta>
            <ta e="T156" id="Seg_12886" s="T153">Потом говорят: "Иди!</ta>
            <ta e="T159" id="Seg_12887" s="T156">Принеси из огорода огурцы".</ta>
            <ta e="T161" id="Seg_12888" s="T159">Я пошла.</ta>
            <ta e="T163" id="Seg_12889" s="T161">Смотрю, смотрю.</ta>
            <ta e="T167" id="Seg_12890" s="T163">Сорвала (?), еще что-нибудь.</ta>
            <ta e="T176" id="Seg_12891" s="T167">Потом (?): взяла две тыквы и вернулась в дом.</ta>
            <ta e="T181" id="Seg_12892" s="T176">Одна женщина прыгнула в подвал.</ta>
            <ta e="T185" id="Seg_12893" s="T181">Одна женщина наружу.</ta>
            <ta e="T190" id="Seg_12894" s="T185">Чуть не наступила на меня.</ta>
            <ta e="T198" id="Seg_12895" s="T191">Тогда отец этих женщин сказал:</ta>
            <ta e="T201" id="Seg_12896" s="T198">"Куда вы побежали?</ta>
            <ta e="T205" id="Seg_12897" s="T201">Налейте [мне] супа". [?]</ta>
            <ta e="T214" id="Seg_12898" s="T205">"Отец, — говорит, — смотри, она две тыквы оттуда принесла".</ta>
            <ta e="T224" id="Seg_12899" s="T214">"А зачем [вы] ее посылаете, она не знает, а вы посылаете".</ta>
            <ta e="T232" id="Seg_12900" s="T225">Одна женщина поехала в Агинское, на лошади.</ta>
            <ta e="T235" id="Seg_12901" s="T232">Пришла домой.</ta>
            <ta e="T239" id="Seg_12902" s="T235">И видит: дерево лежит.</ta>
            <ta e="T246" id="Seg_12903" s="T239">И говорит: его черви съели.</ta>
            <ta e="T255" id="Seg_12904" s="T246">Такие деревья можно ударить, и они (упадут?).</ta>
            <ta e="T263" id="Seg_12905" s="T256">Она сделала его таким маленьким, печь можно топить.</ta>
            <ta e="T268" id="Seg_12906" s="T264">Потом Варлам принес самовар.</ta>
            <ta e="T272" id="Seg_12907" s="T268">"Поставь", - говорит ей.</ta>
            <ta e="T281" id="Seg_12908" s="T272">Она пошла, в трубу воду лила-лила, лила-лила.</ta>
            <ta e="T286" id="Seg_12909" s="T281">И туда уголь положила.</ta>
            <ta e="T290" id="Seg_12910" s="T286">Пришла в дом и говорит:</ta>
            <ta e="T293" id="Seg_12911" s="T290">"Самовар течет.</ta>
            <ta e="T297" id="Seg_12912" s="T293">Самовар течет, вода…"</ta>
            <ta e="T300" id="Seg_12913" s="T297">Он пошел.</ta>
            <ta e="T308" id="Seg_12914" s="T300">"Сюда, — говорит, — воду наливай, а сюда уголь клади".</ta>
            <ta e="T312" id="Seg_12915" s="T308">Потом это (?) сделала.</ta>
            <ta e="T320" id="Seg_12916" s="T313">Пошли рубить деревья и принесли сок.</ta>
            <ta e="T325" id="Seg_12917" s="T320">На деревьях был сок.</ta>
            <ta e="T330" id="Seg_12918" s="T326">Белое дерево — береза.</ta>
            <ta e="T335" id="Seg_12919" s="T331">Лиственница, кедр.</ta>
            <ta e="T340" id="Seg_12920" s="T335">…говорила, (черемуха?).</ta>
            <ta e="T345" id="Seg_12921" s="T341">Веток очень много в лесу.</ta>
            <ta e="T351" id="Seg_12922" s="T345">Когда идешь, все время наступаешь.</ta>
            <ta e="T354" id="Seg_12923" s="T352">Здравствуй!</ta>
            <ta e="T356" id="Seg_12924" s="T354">Доброе утро!</ta>
            <ta e="T362" id="Seg_12925" s="T356">С добрым утром и хорошего дня тебе!</ta>
            <ta e="T366" id="Seg_12926" s="T363">Как ты сегодня спал?</ta>
            <ta e="T371" id="Seg_12927" s="T366">Ты в бане мылся, наверное, хорошо спал.</ta>
            <ta e="T376" id="Seg_12928" s="T371">Да я хорошо помылся.</ta>
            <ta e="T378" id="Seg_12929" s="T376">Воды хватило.</ta>
            <ta e="T383" id="Seg_12930" s="T378">Теплая вода у меня была и холодная.</ta>
            <ta e="T388" id="Seg_12931" s="T383">Тепло стало, очень тепло.</ta>
            <ta e="T395" id="Seg_12932" s="T389">Я в бане мылась, всю воду вылила.</ta>
            <ta e="T398" id="Seg_12933" s="T395">Потом пришел…</ta>
            <ta e="T407" id="Seg_12934" s="T404">Потом пришла (?).</ta>
            <ta e="T414" id="Seg_12935" s="T407">Воды нет, я принесу и помоюсь.</ta>
            <ta e="T423" id="Seg_12936" s="T414">Я ушла, она помылась и пришла в дом.</ta>
            <ta e="T430" id="Seg_12937" s="T424">Моей сестры сын поехал в город.</ta>
            <ta e="T438" id="Seg_12938" s="T430">Там он взял машину, чтобы постирать рубашки, и привез их домой.</ta>
            <ta e="T445" id="Seg_12939" s="T438">А сегодня они рубашки постирали и повесили. </ta>
            <ta e="T451" id="Seg_12940" s="T446">К моей сестре приехала ее (крестная?).</ta>
            <ta e="T456" id="Seg_12941" s="T451">Она привезла (?) мужчину и женщину. [?]</ta>
            <ta e="T466" id="Seg_12942" s="T456">Потом она моей матери говорит: "Идите возьмите ягод, возьмите малины".</ta>
            <ta e="T468" id="Seg_12943" s="T466">Они пошли.</ta>
            <ta e="T475" id="Seg_12944" s="T468">Она водку дала, одну бутылку (им?).</ta>
            <ta e="T479" id="Seg_12945" s="T475">Она сбрызнула ей ягоды.</ta>
            <ta e="T485" id="Seg_12946" s="T479">Они взяли/были (?).</ta>
            <ta e="T488" id="Seg_12947" s="T485">Потом сели есть.</ta>
            <ta e="T493" id="Seg_12948" s="T488">А моя мать говорит:</ta>
            <ta e="T496" id="Seg_12949" s="T493">"Давай отсюда…</ta>
            <ta e="T501" id="Seg_12950" s="T496">Надо сбрызнуть ягоды этой водкой".</ta>
            <ta e="T505" id="Seg_12951" s="T501">Потом налили, сами выпили.</ta>
            <ta e="T510" id="Seg_12952" s="T505">Потом они собрали бересту, много принесли.</ta>
            <ta e="T513" id="Seg_12953" s="T510">Пришли домой туда.</ta>
            <ta e="T519" id="Seg_12954" s="T513">Тут нюхает, тут нюхает.</ta>
            <ta e="T521" id="Seg_12955" s="T519">Пахнет водкой.</ta>
            <ta e="T523" id="Seg_12956" s="T521">Он(а) не знал(а).</ta>
            <ta e="T529" id="Seg_12957" s="T524">Я сегодня рано встала.</ta>
            <ta e="T531" id="Seg_12958" s="T529">Печь затопила.</ta>
            <ta e="T535" id="Seg_12959" s="T531">Дала корове травы.</ta>
            <ta e="T537" id="Seg_12960" s="T535">Полила картошку.</ta>
            <ta e="T540" id="Seg_12961" s="T537">Пошла подоила корову.</ta>
            <ta e="T542" id="Seg_12962" s="T540">Перелила молоко.</ta>
            <ta e="T549" id="Seg_12963" s="T542">А потом пошла к женщине, суп [отнести].</ta>
            <ta e="T551" id="Seg_12964" s="T549">Я сварила суп.</ta>
            <ta e="T557" id="Seg_12965" s="T551">Потом пошла к женщине, отнесла суп.</ta>
            <ta e="T562" id="Seg_12966" s="T557">Она ходит на ногах.</ta>
            <ta e="T565" id="Seg_12967" s="T562">"Ты что, дом помыла?"</ta>
            <ta e="T568" id="Seg_12968" s="T565">"Нет, дети помыли".</ta>
            <ta e="T574" id="Seg_12969" s="T568">Потом (она?) пришла сюда, упала на кровать.</ta>
            <ta e="T576" id="Seg_12970" s="T574">Я говорю:</ta>
            <ta e="T578" id="Seg_12971" s="T576">"Не падай!"</ta>
            <ta e="T580" id="Seg_12972" s="T578">Погоди.</ta>
            <ta e="T584" id="Seg_12973" s="T580">А суп весь (?).</ta>
            <ta e="T587" id="Seg_12974" s="T584">Нескоро (?).</ta>
            <ta e="T594" id="Seg_12975" s="T588">Тогда я ушла (от?) этой женщины и пошла к Анисье.</ta>
            <ta e="T597" id="Seg_12976" s="T594">Я долго не ходила [к ней?].</ta>
            <ta e="T600" id="Seg_12977" s="T597">Пришла к ней.</ta>
            <ta e="T605" id="Seg_12978" s="T600">Она там ругается с сыном.</ta>
            <ta e="T613" id="Seg_12979" s="T606">Анисья ругается на сына.</ta>
            <ta e="T619" id="Seg_12980" s="T613">А сын ей что-нибудь скажет.</ta>
            <ta e="T623" id="Seg_12981" s="T619">Я говорю: "Не ругайся.</ta>
            <ta e="T628" id="Seg_12982" s="T623">Мать ничего не говорит". [?]</ta>
            <ta e="T632" id="Seg_12983" s="T628">А он все ругается.</ta>
            <ta e="T637" id="Seg_12984" s="T632">А мать давай плакать.</ta>
            <ta e="T643" id="Seg_12985" s="T637">"Видишь, как они кормят меня хлебом. [?]</ta>
            <ta e="T649" id="Seg_12986" s="T643">Мой хлеб ест, и еще ругается".</ta>
            <ta e="T656" id="Seg_12987" s="T649">Потом мы сидели (?), потом Эля пришла.</ta>
            <ta e="T660" id="Seg_12988" s="T656">"Здорово". — "Здорово".</ta>
            <ta e="T662" id="Seg_12989" s="T660">"Зачем пришла?"</ta>
            <ta e="T664" id="Seg_12990" s="T662">"Я хлеб искала". [?]</ta>
            <ta e="T668" id="Seg_12991" s="T664">А я Анисье говорю:</ta>
            <ta e="T672" id="Seg_12992" s="T668">"Дай ей одну [буханку] хлеба".</ta>
            <ta e="T675" id="Seg_12993" s="T672">Она ей дала.</ta>
            <ta e="T677" id="Seg_12994" s="T675">"Сколько спросить [за хлеб]?"</ta>
            <ta e="T680" id="Seg_12995" s="T677">Я говорю:</ta>
            <ta e="T685" id="Seg_12996" s="T680">"Немного(?), как в магазине".</ta>
            <ta e="T693" id="Seg_12997" s="T685">Тогда она дала ей сорок копеек.</ta>
            <ta e="T697" id="Seg_12998" s="T694">Потом я пришла [домой].</ta>
            <ta e="T703" id="Seg_12999" s="T697">Корову напоила, теленка напоила, сена дала.</ta>
            <ta e="T706" id="Seg_13000" s="T703">Потом пошла за водой.</ta>
            <ta e="T712" id="Seg_13001" s="T706">Пришла в дом; ноги у меня замерзли.</ta>
            <ta e="T716" id="Seg_13002" s="T712">На печь залезла.</ta>
            <ta e="T719" id="Seg_13003" s="T716">На печь залезла.</ta>
            <ta e="T723" id="Seg_13004" s="T719">Тогда ноги у меня (согрелись?).</ta>
            <ta e="T728" id="Seg_13005" s="T723">Они пришли разговаривать.</ta>
            <ta e="T737" id="Seg_13006" s="T729">Разговаривать надо, но там машина шумит.</ta>
            <ta e="T740" id="Seg_13007" s="T737">Не дает говорить.</ta>
            <ta e="T745" id="Seg_13008" s="T740">Мы бросили, и они ушли.</ta>
            <ta e="T747" id="Seg_13009" s="T745">Мы не поговорили.</ta>
            <ta e="T753" id="Seg_13010" s="T747">А сейчас они снова пришли разговаривать.</ta>
            <ta e="T760" id="Seg_13011" s="T754">Пришли, я говорю: "Где вы были?"</ta>
            <ta e="T774" id="Seg_13012" s="T760">"Мы деревья фотографировали, наш дом, бабушкин дом, и [потом] пришли сюда".</ta>
            <ta e="T779" id="Seg_13013" s="T775">Потом мать пришла из больницы.</ta>
            <ta e="T785" id="Seg_13014" s="T779">Стала ругать детей: они ничего не делают.</ta>
            <ta e="T789" id="Seg_13015" s="T785">"На, поставь жерди в забор.</ta>
            <ta e="T795" id="Seg_13016" s="T789">А то отец придет, он ничего не даст".</ta>
            <ta e="T800" id="Seg_13017" s="T795">Он пошел и стал ставить [жерди].</ta>
            <ta e="T804" id="Seg_13018" s="T800">Потом он пришел, они помылись в бане.</ta>
            <ta e="T809" id="Seg_13019" s="T804">Потом мне говорят: "Иди в баню".</ta>
            <ta e="T812" id="Seg_13020" s="T809">Я пошла мыться.</ta>
            <ta e="T816" id="Seg_13021" s="T812">Потом Эля пришла помыться.</ta>
            <ta e="T820" id="Seg_13022" s="T816">Потом мужчина пришел и помылся.</ta>
            <ta e="T823" id="Seg_13023" s="T820">Потом [его?] отец пришел.</ta>
            <ta e="T825" id="Seg_13024" s="T823">Принес чулки.</ta>
            <ta e="T827" id="Seg_13025" s="T825">Принес штаны.</ta>
            <ta e="T832" id="Seg_13026" s="T828">Потом пришел, водку принес.</ta>
            <ta e="T837" id="Seg_13027" s="T832">Один мужчина его привез на лошади.</ta>
            <ta e="T842" id="Seg_13028" s="T837">Потом он налил ему водки.</ta>
            <ta e="T847" id="Seg_13029" s="T842">Он (немного?) поел, [потом] пошел домой.</ta>
            <ta e="T852" id="Seg_13030" s="T848">Эта женщина очень злая.</ta>
            <ta e="T860" id="Seg_13031" s="T852">Палку взяла и теленка палкой ударила, по глазам.</ta>
            <ta e="T863" id="Seg_13032" s="T860">Слезы(?) текут.</ta>
            <ta e="T870" id="Seg_13033" s="T863">А я своего теленка не бью (рукой).</ta>
            <ta e="T875" id="Seg_13034" s="T870">(…)</ta>
            <ta e="T882" id="Seg_13035" s="T876">Я в доме фотографировала машину, и…</ta>
            <ta e="T887" id="Seg_13036" s="T882">Они… один, два, три, четыре.</ta>
            <ta e="T890" id="Seg_13037" s="T887">Потом Ваня говорит:</ta>
            <ta e="T898" id="Seg_13038" s="T890">"Нужен теленок и свиньи", — а мы засмеялись.</ta>
            <ta e="T903" id="Seg_13039" s="T899">Сегодня мы, наверное, много поговорили.</ta>
            <ta e="T911" id="Seg_13040" s="T903">Теперь надо мне домой идти, поесть, полежать [немного?].</ta>
            <ta e="T916" id="Seg_13041" s="T912">Дым вверх идет.</ta>
            <ta e="T919" id="Seg_13042" s="T916">Очень холодно будет.</ta>
            <ta e="T922" id="Seg_13043" s="T919">Сегодня вечером снег.</ta>
            <ta e="T927" id="Seg_13044" s="T922">Утром я встала — холодно стало!</ta>
            <ta e="T933" id="Seg_13045" s="T928">Нам сегодня надо пойти к Тане.</ta>
            <ta e="T941" id="Seg_13046" s="T933">Чтобы она хлеб взяла из Вознесенского и привезла нам.</ta>
            <ta e="T947" id="Seg_13047" s="T942">(…) надо ей деньги взять.</ta>
            <ta e="T953" id="Seg_13048" s="T947">(…)</ta>
            <ta e="T955" id="Seg_13049" s="T953">И привезет.</ta>
            <ta e="T961" id="Seg_13050" s="T956">Не знаю, возьмет или нет.</ta>
            <ta e="T967" id="Seg_13051" s="T961">Надо пойти с ней поговорить, может возьмет.</ta>
            <ta e="T970" id="Seg_13052" s="T967">Ну иди, поговори.</ta>
            <ta e="T976" id="Seg_13053" s="T971">Она может пойдет в магазин там.</ta>
            <ta e="T981" id="Seg_13054" s="T976">"Очередь надо стоять, [чтобы] хлеб купить.</ta>
            <ta e="T984" id="Seg_13055" s="T981">Я не буду."</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_13056" s="T0">Go together to the sauna.</ta>
            <ta e="T10" id="Seg_13057" s="T4">He'll rub your back with soap and body scrubber.</ta>
            <ta e="T13" id="Seg_13058" s="T10">And you [will do the same] to him.</ta>
            <ta e="T16" id="Seg_13059" s="T13">But I feel shy.</ta>
            <ta e="T21" id="Seg_13060" s="T16">I don't want to go, I'm feeling very shy.</ta>
            <ta e="T29" id="Seg_13061" s="T21">In Aginskoye, they don't go to the sauna together.</ta>
            <ta e="T31" id="Seg_13062" s="T29">Women, men.</ta>
            <ta e="T35" id="Seg_13063" s="T31">The women go alone [= separately].</ta>
            <ta e="T40" id="Seg_13064" s="T35">And the men heat up [the sauna] the next day.</ta>
            <ta e="T43" id="Seg_13065" s="T40">The men go separately.</ta>
            <ta e="T49" id="Seg_13066" s="T44">We go to the sauna together, two girls.</ta>
            <ta e="T56" id="Seg_13067" s="T49">Then the mother goes, one girl, one boy.</ta>
            <ta e="T58" id="Seg_13068" s="T56">She'll wash them.</ta>
            <ta e="T64" id="Seg_13069" s="T58">They go, then the husband goes.</ta>
            <ta e="T69" id="Seg_13070" s="T64">And then I go and wash.</ta>
            <ta e="T74" id="Seg_13071" s="T70">My aunt went to Aginskoye.</ta>
            <ta e="T79" id="Seg_13072" s="T74">They just heated up the sauna there.</ta>
            <ta e="T84" id="Seg_13073" s="T79">They all washed and said: "Go to the sauna!"</ta>
            <ta e="T89" id="Seg_13074" s="T84">She went.</ta>
            <ta e="T94" id="Seg_13075" s="T89">She doesn't know, where to pour water.</ta>
            <ta e="T104" id="Seg_13076" s="T94">"I poured to the oven, and wetted the walls…"</ta>
            <ta e="T108" id="Seg_13077" s="T104">[I say it] in Russian (?).</ta>
            <ta e="T111" id="Seg_13078" s="T108">Then I washed, I came [out].</ta>
            <ta e="T116" id="Seg_13079" s="T111">Why did you wash and didn't (put?)?</ta>
            <ta e="T118" id="Seg_13080" s="T116">I didn't (put?)!</ta>
            <ta e="T123" id="Seg_13081" s="T119">Then they say: "Go!</ta>
            <ta e="T126" id="Seg_13082" s="T123">Bring the meat [from] the cellar.</ta>
            <ta e="T127" id="Seg_13083" s="T126">For the meatballs."</ta>
            <ta e="T129" id="Seg_13084" s="T127">I went.</ta>
            <ta e="T136" id="Seg_13085" s="T129">I'm staying, there is a lot of meat, but no pan.</ta>
            <ta e="T138" id="Seg_13086" s="T136">Then I came back.</ta>
            <ta e="T143" id="Seg_13087" s="T138">"Why don't you bring the meat?"</ta>
            <ta e="T149" id="Seg_13088" s="T143">"There is a lot of meat, but no pan."</ta>
            <ta e="T152" id="Seg_13089" s="T149">They started to laugh.</ta>
            <ta e="T156" id="Seg_13090" s="T153">Then they say: "Go!</ta>
            <ta e="T159" id="Seg_13091" s="T156">Bring the cucumbers [from] the vegetable garden."</ta>
            <ta e="T161" id="Seg_13092" s="T159">I went.</ta>
            <ta e="T163" id="Seg_13093" s="T161">I looked for a long time.</ta>
            <ta e="T167" id="Seg_13094" s="T163">I picked (?), and something more.</ta>
            <ta e="T176" id="Seg_13095" s="T167">Then (?); I took two pumpkins and came to the house.</ta>
            <ta e="T181" id="Seg_13096" s="T176">One woman jumped to the cellar.</ta>
            <ta e="T185" id="Seg_13097" s="T181">One woman [jumped] outside.</ta>
            <ta e="T190" id="Seg_13098" s="T185">She nearly stepped on my [toe].</ta>
            <ta e="T198" id="Seg_13099" s="T191">Then these women's father said:</ta>
            <ta e="T201" id="Seg_13100" s="T198">"Where did you run?</ta>
            <ta e="T205" id="Seg_13101" s="T201">Pour [me] some soup." [?]</ta>
            <ta e="T214" id="Seg_13102" s="T205">"Father, — [she] says, — look, she brought from there two pumpkins."</ta>
            <ta e="T224" id="Seg_13103" s="T214">"Why do [you] send [her], she doesn't know, and send her."</ta>
            <ta e="T232" id="Seg_13104" s="T225">One woman went to Aginskoye, on a horse.</ta>
            <ta e="T235" id="Seg_13105" s="T232">She came home.</ta>
            <ta e="T239" id="Seg_13106" s="T235">And she sees: a log is lying.</ta>
            <ta e="T246" id="Seg_13107" s="T239">Then she says: it is eaten by worms.</ta>
            <ta e="T255" id="Seg_13108" s="T246">Such trees one can hit, and the trees will fall. [?]</ta>
            <ta e="T263" id="Seg_13109" s="T256">She made it so small, one can heat an oven [with it].</ta>
            <ta e="T268" id="Seg_13110" s="T264">Then Varlam brought a samovar.</ta>
            <ta e="T272" id="Seg_13111" s="T268">"Put [it]", [he?] says to her.</ta>
            <ta e="T281" id="Seg_13112" s="T272">She came, poured water into the pipe, she poured and poured it.</ta>
            <ta e="T286" id="Seg_13113" s="T281">And he put coal there.</ta>
            <ta e="T290" id="Seg_13114" s="T286">Then she comes in the house and says:</ta>
            <ta e="T293" id="Seg_13115" s="T290">"The samovar leaks.</ta>
            <ta e="T297" id="Seg_13116" s="T293">The samovar leaks, water [flows?]."</ta>
            <ta e="T300" id="Seg_13117" s="T297">He went.</ta>
            <ta e="T308" id="Seg_13118" s="T300">He says: "Pour water here, and coal here."</ta>
            <ta e="T312" id="Seg_13119" s="T308">Then s/he made this. [?]</ta>
            <ta e="T320" id="Seg_13120" s="T313">They went to cut trees and brought sap.</ta>
            <ta e="T325" id="Seg_13121" s="T320">There was sap in the trees.</ta>
            <ta e="T330" id="Seg_13122" s="T326">White tree, birch tree.</ta>
            <ta e="T335" id="Seg_13123" s="T331">Larch tree, ceddar tree.</ta>
            <ta e="T340" id="Seg_13124" s="T335">[I said] bird cherry (?) tree.</ta>
            <ta e="T345" id="Seg_13125" s="T341">In the forest, there are very many branches.</ta>
            <ta e="T351" id="Seg_13126" s="T345">When you walk, you always go on (them).</ta>
            <ta e="T354" id="Seg_13127" s="T352">Hello!</ta>
            <ta e="T356" id="Seg_13128" s="T354">Good morning!</ta>
            <ta e="T362" id="Seg_13129" s="T356">Good morning, and I wish you a merry day.</ta>
            <ta e="T366" id="Seg_13130" s="T363">How did you sleep today?</ta>
            <ta e="T371" id="Seg_13131" s="T366">You [had] washed in the sauna, you should have slept well.</ta>
            <ta e="T376" id="Seg_13132" s="T371">Well, I slept, I washed well.</ta>
            <ta e="T378" id="Seg_13133" s="T376">There was enough water.</ta>
            <ta e="T383" id="Seg_13134" s="T378"> I had warm water, and cold [water].</ta>
            <ta e="T388" id="Seg_13135" s="T383">It became very warm, (very?) warm.</ta>
            <ta e="T395" id="Seg_13136" s="T389">I washed myself in the sauna, I used all the water.</ta>
            <ta e="T398" id="Seg_13137" s="T395">Then came…</ta>
            <ta e="T407" id="Seg_13138" s="T404">Then came (?).</ta>
            <ta e="T414" id="Seg_13139" s="T407">There is no water, I'll bring it and wash (myself?).</ta>
            <ta e="T423" id="Seg_13140" s="T414">I went away, and she washed and came in the house.</ta>
            <ta e="T430" id="Seg_13141" s="T424">My sister's son went into town.</ta>
            <ta e="T438" id="Seg_13142" s="T430">There was / he took a machine to wash the shirts, then he brought it home.</ta>
            <ta e="T445" id="Seg_13143" s="T438">And today they washed and hung out the shirts [to dry].</ta>
            <ta e="T451" id="Seg_13144" s="T446">My sister's (godmother?) came to her.</ta>
            <ta e="T456" id="Seg_13145" s="T451">She brought (?) a man and a woman. [?]</ta>
            <ta e="T466" id="Seg_13146" s="T456">Then she says to my mother: "Go take berries, take raspberries.</ta>
            <ta e="T468" id="Seg_13147" s="T466">They went.</ta>
            <ta e="T475" id="Seg_13148" s="T468">She gave (them?) one bottle of vodka.</ta>
            <ta e="T479" id="Seg_13149" s="T475">She (sprinkled?) the berries with it. [?]</ta>
            <ta e="T485" id="Seg_13150" s="T479">Then took/were (?).</ta>
            <ta e="T488" id="Seg_13151" s="T485">Then they sat down to eat.</ta>
            <ta e="T493" id="Seg_13152" s="T488">And my mother says:</ta>
            <ta e="T496" id="Seg_13153" s="T493">"Let's…</ta>
            <ta e="T501" id="Seg_13154" s="T496">We should (sprinkle?) the berries with this vodka."</ta>
            <ta e="T505" id="Seg_13155" s="T501">Then they poured [vodka into glasses?], they drank themselves.</ta>
            <ta e="T510" id="Seg_13156" s="T505">Then they gathered birch bark, they brought much.</ta>
            <ta e="T513" id="Seg_13157" s="T510">They came home.</ta>
            <ta e="T519" id="Seg_13158" s="T513">S/he sniffed here, s/he sniffed there.</ta>
            <ta e="T521" id="Seg_13159" s="T519">It smells of vodka.</ta>
            <ta e="T523" id="Seg_13160" s="T521">S/he didn't know.</ta>
            <ta e="T529" id="Seg_13161" s="T524">I got up early today.</ta>
            <ta e="T531" id="Seg_13162" s="T529">I heated up the oven.</ta>
            <ta e="T535" id="Seg_13163" s="T531">I gave grass to the cow.</ta>
            <ta e="T537" id="Seg_13164" s="T535">I watered potatoes.</ta>
            <ta e="T540" id="Seg_13165" s="T537">I went and milked the cow.</ta>
            <ta e="T542" id="Seg_13166" s="T540">I (poured?) the milk.</ta>
            <ta e="T549" id="Seg_13167" s="T542">Then I went to the woman, to [bring her] the soup.</ta>
            <ta e="T551" id="Seg_13168" s="T549">I cooked soup.</ta>
            <ta e="T557" id="Seg_13169" s="T551">Then I went to the woman and brought [her] the soup.</ta>
            <ta e="T562" id="Seg_13170" s="T557">She's walking on her foots.</ta>
            <ta e="T565" id="Seg_13171" s="T562">"Did you wash your house?"</ta>
            <ta e="T568" id="Seg_13172" s="T565">"No, [my] kids washed [it]."</ta>
            <ta e="T574" id="Seg_13173" s="T568">Then (she?) came here, fell on the bed.</ta>
            <ta e="T576" id="Seg_13174" s="T574">I say:</ta>
            <ta e="T578" id="Seg_13175" s="T576">"Don't fall!"</ta>
            <ta e="T580" id="Seg_13176" s="T578">Wait a bit.</ta>
            <ta e="T584" id="Seg_13177" s="T580">And the soup is all (?).</ta>
            <ta e="T587" id="Seg_13178" s="T584">It won't (?) soon.</ta>
            <ta e="T594" id="Seg_13179" s="T588">Then I left (?) this woman and went to Anisya.</ta>
            <ta e="T597" id="Seg_13180" s="T594">I haven't gone [there] for a long time. [?]</ta>
            <ta e="T600" id="Seg_13181" s="T597">I came to her.</ta>
            <ta e="T605" id="Seg_13182" s="T600">She's scolding there with her son.</ta>
            <ta e="T613" id="Seg_13183" s="T606">Then that Anisya curses [at] her son.</ta>
            <ta e="T619" id="Seg_13184" s="T613">And her son will say something to her.</ta>
            <ta e="T623" id="Seg_13185" s="T619">I say: "Don't quarrel.</ta>
            <ta e="T628" id="Seg_13186" s="T623">The mother doesn't say anything." [?]</ta>
            <ta e="T632" id="Seg_13187" s="T628">But he's still cursing.</ta>
            <ta e="T637" id="Seg_13188" s="T632">And the mother started crying.</ta>
            <ta e="T643" id="Seg_13189" s="T637">"Do you see how they feed me with bread. [?]</ta>
            <ta e="T649" id="Seg_13190" s="T643">He eats my bread and curses [at me]."</ta>
            <ta e="T656" id="Seg_13191" s="T649">Then we sat down to (?), then Elya came.</ta>
            <ta e="T660" id="Seg_13192" s="T656">"Hello." ‒ "Hello."</ta>
            <ta e="T662" id="Seg_13193" s="T660">"What did you come for?"</ta>
            <ta e="T664" id="Seg_13194" s="T662">"I was looking for some bread." [?]</ta>
            <ta e="T668" id="Seg_13195" s="T664">I say to Anisya:</ta>
            <ta e="T672" id="Seg_13196" s="T668">"Give her one piece of bread."</ta>
            <ta e="T675" id="Seg_13197" s="T672">She gave her.</ta>
            <ta e="T677" id="Seg_13198" s="T675">"How much should ask [for this bread]?"</ta>
            <ta e="T680" id="Seg_13199" s="T677">I say:</ta>
            <ta e="T685" id="Seg_13200" s="T680">"Not (much?), like in the shop."</ta>
            <ta e="T693" id="Seg_13201" s="T685">Then she gave her forty copecks.</ta>
            <ta e="T697" id="Seg_13202" s="T694">Then I came [home].</ta>
            <ta e="T703" id="Seg_13203" s="T697">I gave water to the cow, to the calf, gave [them] grass.</ta>
            <ta e="T706" id="Seg_13204" s="T703">Then I went to fetch water.</ta>
            <ta e="T712" id="Seg_13205" s="T706">And I came home; my feet were freezing.</ta>
            <ta e="T716" id="Seg_13206" s="T712">I climbed on the stove.</ta>
            <ta e="T719" id="Seg_13207" s="T716">I climbed on the stove.</ta>
            <ta e="T723" id="Seg_13208" s="T719">Then my feet got warm. [?]</ta>
            <ta e="T728" id="Seg_13209" s="T723">They came to speak.</ta>
            <ta e="T737" id="Seg_13210" s="T729">Then we have to speak, but a car is rattling there.</ta>
            <ta e="T740" id="Seg_13211" s="T737">It doesn't let [us] speak.</ta>
            <ta e="T745" id="Seg_13212" s="T740">We gave up, and they left.</ta>
            <ta e="T747" id="Seg_13213" s="T745">We didn't speak.</ta>
            <ta e="T753" id="Seg_13214" s="T747">And now they came to speak again.</ta>
            <ta e="T760" id="Seg_13215" s="T754">They came, and I say: "Where have you been?"</ta>
            <ta e="T774" id="Seg_13216" s="T760">"We took pictures of the forest, of our house, and of grandmother's house, and [then] we came here."</ta>
            <ta e="T779" id="Seg_13217" s="T775">Then the mother came [from] the hospital.</ta>
            <ta e="T785" id="Seg_13218" s="T779">She started scolding the kids: they don't do anything.</ta>
            <ta e="T789" id="Seg_13219" s="T785">"Take [it], put the stakes in the fence.</ta>
            <ta e="T795" id="Seg_13220" s="T789">Or your father will come, he won't give anything."</ta>
            <ta e="T800" id="Seg_13221" s="T795">He went and started putting [the stakes].</ta>
            <ta e="T804" id="Seg_13222" s="T800">Then he came, they washed in the sauna.</ta>
            <ta e="T809" id="Seg_13223" s="T804">Then they say to me: "Go to the sauna."</ta>
            <ta e="T812" id="Seg_13224" s="T809">I went to wash.</ta>
            <ta e="T816" id="Seg_13225" s="T812">Then Elya came to wash.</ta>
            <ta e="T820" id="Seg_13226" s="T816">Then [a] man came and washed himself.</ta>
            <ta e="T823" id="Seg_13227" s="T820">Then [his?] father came.</ta>
            <ta e="T825" id="Seg_13228" s="T823">He brought a hose.</ta>
            <ta e="T827" id="Seg_13229" s="T825">He brought the pants.</ta>
            <ta e="T832" id="Seg_13230" s="T828">Then he came and brought vodka.</ta>
            <ta e="T837" id="Seg_13231" s="T832">One man [had] brought him/it with a horse.</ta>
            <ta e="T842" id="Seg_13232" s="T837">Then he poured him some vodka.</ta>
            <ta e="T847" id="Seg_13233" s="T842">He ate a (little?), [then] went home.</ta>
            <ta e="T852" id="Seg_13234" s="T848">This woman is very angry.</ta>
            <ta e="T860" id="Seg_13235" s="T852">She took a stick and hit the calf, in the eye.</ta>
            <ta e="T863" id="Seg_13236" s="T860">Tears(?) flow.</ta>
            <ta e="T870" id="Seg_13237" s="T863">And I don't beat my calf (with my hand).</ta>
            <ta e="T875" id="Seg_13238" s="T870">(…)</ta>
            <ta e="T882" id="Seg_13239" s="T876">I took picture of the machine at home, and…</ta>
            <ta e="T887" id="Seg_13240" s="T882">They… one, two, three, four.</ta>
            <ta e="T890" id="Seg_13241" s="T887">Then Vania says:</ta>
            <ta e="T898" id="Seg_13242" s="T890">"We need a calf and pigs", and we laughed.</ta>
            <ta e="T903" id="Seg_13243" s="T899">Today we must have spoken much.</ta>
            <ta e="T911" id="Seg_13244" s="T903">Now I must go home, to eat, to lie down [for a while?].</ta>
            <ta e="T916" id="Seg_13245" s="T912">The smoke rises [high] in the air.</ta>
            <ta e="T919" id="Seg_13246" s="T916">It will be very cold.</ta>
            <ta e="T922" id="Seg_13247" s="T919">Tonight the snow lays [on the ground].</ta>
            <ta e="T927" id="Seg_13248" s="T922">I got up ‎‎in the morning ‒ it was very cold.</ta>
            <ta e="T933" id="Seg_13249" s="T928">We should go to Tanya today.</ta>
            <ta e="T941" id="Seg_13250" s="T933">[To ask her] to take bread from Voznesenskoe and bring it to us.</ta>
            <ta e="T947" id="Seg_13251" s="T942">(…) [we] should take money for her.</ta>
            <ta e="T953" id="Seg_13252" s="T947">(…)</ta>
            <ta e="T955" id="Seg_13253" s="T953">And will bring.</ta>
            <ta e="T961" id="Seg_13254" s="T956">I don't know, if she will take [it] or not.</ta>
            <ta e="T967" id="Seg_13255" s="T961">I should go and speak to her, maybe she'll take.</ta>
            <ta e="T970" id="Seg_13256" s="T967">Well, go, speak.</ta>
            <ta e="T976" id="Seg_13257" s="T971">Maybe she'll go to the shop there.</ta>
            <ta e="T981" id="Seg_13258" s="T976">"One has to stand in a queue, to buy bread.</ta>
            <ta e="T984" id="Seg_13259" s="T981">I won't."</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_13260" s="T0">Geht zusammen in die Sauna.</ta>
            <ta e="T10" id="Seg_13261" s="T4">Er schrubbt deinen Rücken mit Seife und einer Körperbürste.</ta>
            <ta e="T13" id="Seg_13262" s="T10">Und du [machst das Gleiche] bei ihm.</ta>
            <ta e="T16" id="Seg_13263" s="T13">Aber ich schäme mich.</ta>
            <ta e="T21" id="Seg_13264" s="T16">Ich will nicht gehen, ich bin schüchtern.</ta>
            <ta e="T29" id="Seg_13265" s="T21">In Aginskoje geht man nicht zusammen in die Sauna.</ta>
            <ta e="T31" id="Seg_13266" s="T29">Frauen, Männer.</ta>
            <ta e="T35" id="Seg_13267" s="T31">Die Frauen gehen alleine [= getrennt].</ta>
            <ta e="T40" id="Seg_13268" s="T35">Und die Männer heizen [die Sauna] am nächsten Tag.</ta>
            <ta e="T43" id="Seg_13269" s="T40">Die Männer gehen getrennt.</ta>
            <ta e="T49" id="Seg_13270" s="T44">Wir gehen zusammen in die Sauna, zwei Mädchen.</ta>
            <ta e="T56" id="Seg_13271" s="T49">Dann geht meine Mutter, ein Mädchen, ein Junge.</ta>
            <ta e="T58" id="Seg_13272" s="T56">Dann wäscht sie sie.</ta>
            <ta e="T64" id="Seg_13273" s="T58">Sie gehen, dann geht der Mann.</ta>
            <ta e="T69" id="Seg_13274" s="T64">Und dann gehe ich und wasche mich.</ta>
            <ta e="T74" id="Seg_13275" s="T70">Meine Tante ging nach Aginskoje.</ta>
            <ta e="T79" id="Seg_13276" s="T74">Sie hatten dort gerade die Sauna angeheizt.</ta>
            <ta e="T84" id="Seg_13277" s="T79">Sie alle wuschen sich und sagten: "Geh in die Sauna!"</ta>
            <ta e="T89" id="Seg_13278" s="T84">Sie ging.</ta>
            <ta e="T94" id="Seg_13279" s="T89">Sie wusste nicht, wohin sie Wasser gießen soll.</ta>
            <ta e="T104" id="Seg_13280" s="T94">"Ich goss es auf den Ofen und machte die Wände naß…"</ta>
            <ta e="T108" id="Seg_13281" s="T104">[Ich sage es] auf Russisch (?).</ta>
            <ta e="T111" id="Seg_13282" s="T108">Dann wusch ich [mich], ich kam [heraus].</ta>
            <ta e="T116" id="Seg_13283" s="T111">Warum hast du dich gewaschen und hast [es] nicht (hingetan?)?</ta>
            <ta e="T118" id="Seg_13284" s="T116">Ich habe [es] nicht (hingetan?)!</ta>
            <ta e="T123" id="Seg_13285" s="T119">Dann sagen sie: "Geh!</ta>
            <ta e="T126" id="Seg_13286" s="T123">Bring das Fleisch [aus] dem Keller.</ta>
            <ta e="T127" id="Seg_13287" s="T126">Für die Frikadellen."</ta>
            <ta e="T129" id="Seg_13288" s="T127">Ich ging.</ta>
            <ta e="T136" id="Seg_13289" s="T129">Ich bleibe, dort ist viel Fleisch, aber keine Pfanne.</ta>
            <ta e="T138" id="Seg_13290" s="T136">Dann kam ich zurück.</ta>
            <ta e="T143" id="Seg_13291" s="T138">"Warum bringst du das Fleisch nicht?"</ta>
            <ta e="T149" id="Seg_13292" s="T143">"Dort ist viel Fleisch, aber keine Pfanne."</ta>
            <ta e="T152" id="Seg_13293" s="T149">Sie begann zu lachen.</ta>
            <ta e="T156" id="Seg_13294" s="T153">Dann sagen sie: "Geh!</ta>
            <ta e="T159" id="Seg_13295" s="T156">Bring die Gurken [aus] dem Gemüsegarten."</ta>
            <ta e="T161" id="Seg_13296" s="T159">Ich ging.</ta>
            <ta e="T163" id="Seg_13297" s="T161">Ich suchte eine lange Zeit.</ta>
            <ta e="T167" id="Seg_13298" s="T163">Ich pflückte (?), und noch etwas mehr.</ta>
            <ta e="T176" id="Seg_13299" s="T167">Dann (?); ich nahm zwei Kürbisse und kam ins Haus.</ta>
            <ta e="T181" id="Seg_13300" s="T176">Eine Frau sprang in den Keller.</ta>
            <ta e="T185" id="Seg_13301" s="T181">Eine Frau [sprang] heraus.</ta>
            <ta e="T190" id="Seg_13302" s="T185">Sie trat mir fast [auf den Fuß].</ta>
            <ta e="T198" id="Seg_13303" s="T191">Dann sagte der Vater dieser Frauen:</ta>
            <ta e="T201" id="Seg_13304" s="T198">"Wo seid ihr hingelaufen?</ta>
            <ta e="T205" id="Seg_13305" s="T201">Tut [mir] ein Bisschen Suppe auf." [?]</ta>
            <ta e="T214" id="Seg_13306" s="T205">"Vater", sagt [sie], "schau, sie hat von dort zwei Kürbisse gebracht."</ta>
            <ta e="T224" id="Seg_13307" s="T214">"Warum schickt [ihr sie], sie weiß es nicht und ihr schickt sie."</ta>
            <ta e="T232" id="Seg_13308" s="T225">Eine Frau ging nach Aginskoje, auf dem Pferd.</ta>
            <ta e="T235" id="Seg_13309" s="T232">Sie kam nach Hause.</ta>
            <ta e="T239" id="Seg_13310" s="T235">Und sie sieht: Dort liegt ein Holzklotz.</ta>
            <ta e="T246" id="Seg_13311" s="T239">Dann sagt sie: Er ist von Würmern zerfressen.</ta>
            <ta e="T255" id="Seg_13312" s="T246">Solche Bäume kann man schlagen, und die Bäume werden fallen. [?]</ta>
            <ta e="T263" id="Seg_13313" s="T256">Sie machte ihn so kleine, man kann [damit] einen Ofen heizen.</ta>
            <ta e="T268" id="Seg_13314" s="T264">Dann brachte Varlam ein Samowar.</ta>
            <ta e="T272" id="Seg_13315" s="T268">"Tu [es] hin", sagt [er] zu ihr.</ta>
            <ta e="T281" id="Seg_13316" s="T272">Sie kam, goss Wasser in die Pfeife, sie goss und goss es.</ta>
            <ta e="T286" id="Seg_13317" s="T281">Und er tat Kohle dorthin.</ta>
            <ta e="T290" id="Seg_13318" s="T286">Dann kommt sie ins Haus uns sagt:</ta>
            <ta e="T293" id="Seg_13319" s="T290">"Das Samowar leckt.</ta>
            <ta e="T297" id="Seg_13320" s="T293">Das Samowar leckt, Wasser [fließt?]."</ta>
            <ta e="T300" id="Seg_13321" s="T297">Er ging.</ta>
            <ta e="T308" id="Seg_13322" s="T300">Er sagt: "Gieß Wasser hierhin und tu Kohle dahin."</ta>
            <ta e="T312" id="Seg_13323" s="T308">Dann machte er/sie diese(m). [?]</ta>
            <ta e="T320" id="Seg_13324" s="T313">Sie gingen, um Bäume zu schlagen, und brachten Saft.</ta>
            <ta e="T325" id="Seg_13325" s="T320">Es war Saft in den Bäumen.</ta>
            <ta e="T330" id="Seg_13326" s="T326">Weißer Baum, Birkenbaum.</ta>
            <ta e="T335" id="Seg_13327" s="T331">Lärchenbaum, Zedernbaum.</ta>
            <ta e="T340" id="Seg_13328" s="T335">[Ich sagte] (Vogelkirschen?)baum.</ta>
            <ta e="T345" id="Seg_13329" s="T341">Im Wald gibt es viele Äste.</ta>
            <ta e="T351" id="Seg_13330" s="T345">Wenn du gehst, trittst du immer auf (sie).</ta>
            <ta e="T354" id="Seg_13331" s="T352">Hallo!</ta>
            <ta e="T356" id="Seg_13332" s="T354">Guten Morgen!</ta>
            <ta e="T362" id="Seg_13333" s="T356">Guten Morgen und ich wünsche dir einen guten Tag.</ta>
            <ta e="T366" id="Seg_13334" s="T363">Wie hast du heute geschlafen?</ta>
            <ta e="T371" id="Seg_13335" s="T366">Du [hattest] [dich] in der Sauna gewaschen, du solltest gut geschlafen haben.</ta>
            <ta e="T376" id="Seg_13336" s="T371">Also, ich habe gut geschlafen, gewaschen.</ta>
            <ta e="T378" id="Seg_13337" s="T376">Es gab genug Wasser.</ta>
            <ta e="T383" id="Seg_13338" s="T378">Ich hatte warmes Wasser und kaltes [Wasser].</ta>
            <ta e="T388" id="Seg_13339" s="T383">Es wurde sehr warm, (sehr?) warm.</ta>
            <ta e="T395" id="Seg_13340" s="T389">Ich wusch mich in der Sauna, habe alles Wasser ausgegossen.</ta>
            <ta e="T398" id="Seg_13341" s="T395">Dann kam…</ta>
            <ta e="T407" id="Seg_13342" s="T404">Dann kam (?).</ta>
            <ta e="T414" id="Seg_13343" s="T407">Dort gibt es kein Wasser, ich bringe es und wasche mich.</ta>
            <ta e="T423" id="Seg_13344" s="T414">Ich ging weg, und sie wusch [sich] und kam ins Haus.</ta>
            <ta e="T430" id="Seg_13345" s="T424">Der Sohn meiner Schwester fuhr in die Stadt.</ta>
            <ta e="T438" id="Seg_13346" s="T430">Dort gab es / nahm er eine Maschine, um die Hemden zu waschen, dann brachte er sie nach Hause.</ta>
            <ta e="T445" id="Seg_13347" s="T438">Und heute wuschen sie die Hemden und hängten sie [zum trocknen] auf.</ta>
            <ta e="T451" id="Seg_13348" s="T446">Zu meiner Schwester kam ihre (Patin?).</ta>
            <ta e="T456" id="Seg_13349" s="T451">Sie brachte (?) einen Mann un eine Frau. [?]</ta>
            <ta e="T466" id="Seg_13350" s="T456">Dann sagt sie zu meiner Mutter: "Geht, hol Beeren, hol Himbeeren.</ta>
            <ta e="T468" id="Seg_13351" s="T466">Sie gingen.</ta>
            <ta e="T475" id="Seg_13352" s="T468">Sie gab (ihnen?) eine Flasche Wodka.</ta>
            <ta e="T479" id="Seg_13353" s="T475">Sie (bespritzte?) die Beeren damit. [?]</ta>
            <ta e="T485" id="Seg_13354" s="T479">Dann nahmen/waren sie (?).</ta>
            <ta e="T488" id="Seg_13355" s="T485">Dann setzten sie sich um zu essen.</ta>
            <ta e="T493" id="Seg_13356" s="T488">Und meine Mutter sagt:</ta>
            <ta e="T496" id="Seg_13357" s="T493">"Lass uns…</ta>
            <ta e="T501" id="Seg_13358" s="T496">Wir sollten die Beeren mit dem Wodka (bespritzen?)."</ta>
            <ta e="T505" id="Seg_13359" s="T501">Dann gossen sie [Wodka in Gläser?], sie selbst tranken.</ta>
            <ta e="T510" id="Seg_13360" s="T505">Dann sammelten sie Birkenrinde, brachten viel.</ta>
            <ta e="T513" id="Seg_13361" s="T510">Sie kamen nach Hause.</ta>
            <ta e="T519" id="Seg_13362" s="T513">Er/sie roch hier, er/sie roch dort.</ta>
            <ta e="T521" id="Seg_13363" s="T519">Es riecht nach Wodka.</ta>
            <ta e="T523" id="Seg_13364" s="T521">Er/sie wusste es nicht.</ta>
            <ta e="T529" id="Seg_13365" s="T524">Ich bin heute früh aufgestanden.</ta>
            <ta e="T531" id="Seg_13366" s="T529">Ich heizte den Ofen.</ta>
            <ta e="T535" id="Seg_13367" s="T531">Ich gab der Kuh Gras.</ta>
            <ta e="T537" id="Seg_13368" s="T535">Ich goss die Kartoffeln.</ta>
            <ta e="T540" id="Seg_13369" s="T537">Ich ging und molk die Kuh.</ta>
            <ta e="T542" id="Seg_13370" s="T540">Ich (goss?) die Milch.</ta>
            <ta e="T549" id="Seg_13371" s="T542">Dann ging ich zu der Frau, um [ihr] die Suppe [zu bringen].</ta>
            <ta e="T551" id="Seg_13372" s="T549">Ich kochte Suppe.</ta>
            <ta e="T557" id="Seg_13373" s="T551">Dann ging ich zu der Frau und brachte [ihr] die Suppe.</ta>
            <ta e="T562" id="Seg_13374" s="T557">Sie geht auf ihre Füße.</ta>
            <ta e="T565" id="Seg_13375" s="T562">"Hast du dein Haus geputzt?"</ta>
            <ta e="T568" id="Seg_13376" s="T565">"Nein, [meine] Kinder haben [es] geputzt."</ta>
            <ta e="T574" id="Seg_13377" s="T568">Dann kam (sie?) her, fiel aufs Bett.</ta>
            <ta e="T576" id="Seg_13378" s="T574">Ich sage:</ta>
            <ta e="T578" id="Seg_13379" s="T576">"Fall nicht!"</ta>
            <ta e="T580" id="Seg_13380" s="T578">Warte ein Bisschen.</ta>
            <ta e="T584" id="Seg_13381" s="T580">Und alle Suppe ist (?).</ta>
            <ta e="T587" id="Seg_13382" s="T584">Es wird nicht (?) bald.</ta>
            <ta e="T594" id="Seg_13383" s="T588">Dann verließ (?) ich diese Frau und ging zu Anisja.</ta>
            <ta e="T597" id="Seg_13384" s="T594">Ich war lange nicht dort gewesen. [?]</ta>
            <ta e="T600" id="Seg_13385" s="T597">Ich kam zu ihr.</ta>
            <ta e="T605" id="Seg_13386" s="T600">Sie schimpft dort mit ihrem Sohn.</ta>
            <ta e="T613" id="Seg_13387" s="T606">Dann verfluchte diese Anisja ihren Sohn.</ta>
            <ta e="T619" id="Seg_13388" s="T613">Und ihr Sohn sagt irgendetwas zu ihr.</ta>
            <ta e="T623" id="Seg_13389" s="T619">Ich sage: "Streit nicht.</ta>
            <ta e="T628" id="Seg_13390" s="T623">Die Mutter sagt nichts." [?]</ta>
            <ta e="T632" id="Seg_13391" s="T628">Aber er flucht immer noch.</ta>
            <ta e="T637" id="Seg_13392" s="T632">Und die Mutter fing an zu weinen.</ta>
            <ta e="T643" id="Seg_13393" s="T637">"Siehst du wie sie mich mit dem Brot füttern. [?]</ta>
            <ta e="T649" id="Seg_13394" s="T643">Er isst mein Brot und verflucht [mich]."</ta>
            <ta e="T656" id="Seg_13395" s="T649">Denn setzten wir uns um (?), dann Elja kam.</ta>
            <ta e="T660" id="Seg_13396" s="T656">"Hallo." ‒ "Hallo."</ta>
            <ta e="T662" id="Seg_13397" s="T660">"Wozu bist du gekommen?"</ta>
            <ta e="T664" id="Seg_13398" s="T662">"Ich habe Brot gesucht." [?]</ta>
            <ta e="T668" id="Seg_13399" s="T664">Ich sage zu Anisja:</ta>
            <ta e="T672" id="Seg_13400" s="T668">"Gib ihr ein Stück Brot."</ta>
            <ta e="T675" id="Seg_13401" s="T672">Sie gab es ihr.</ta>
            <ta e="T677" id="Seg_13402" s="T675">"Wie viel soll ich [dafür] verlangen?"</ta>
            <ta e="T680" id="Seg_13403" s="T677">Ich sage:</ta>
            <ta e="T685" id="Seg_13404" s="T680">"Nicht (viel?), wie im Laden."</ta>
            <ta e="T693" id="Seg_13405" s="T685">Dann gab sie ihr vierzig Kopeken.</ta>
            <ta e="T697" id="Seg_13406" s="T694">Dann kam ich [nach Hause].</ta>
            <ta e="T703" id="Seg_13407" s="T697">Ich gab der Kuh Wasser, dem Kalb, gab [ihnen] Grass.</ta>
            <ta e="T706" id="Seg_13408" s="T703">Dann ging ich um Wasser zu holen.</ta>
            <ta e="T712" id="Seg_13409" s="T706">Und ich kam nach Hause, meine Füße froren.</ta>
            <ta e="T716" id="Seg_13410" s="T712">Ich kletterte auf den Ofen.</ta>
            <ta e="T719" id="Seg_13411" s="T716">Ich kletterte auf den Ofen.</ta>
            <ta e="T723" id="Seg_13412" s="T719">Dann wurden meine Füße warm. [?]</ta>
            <ta e="T728" id="Seg_13413" s="T723">Sie kamen um zu sprechen.</ta>
            <ta e="T737" id="Seg_13414" s="T729">Dann müssen wir sprechen, aber ein Auto knattert dort.</ta>
            <ta e="T740" id="Seg_13415" s="T737">Es lässt [uns] nicht sprechen.</ta>
            <ta e="T745" id="Seg_13416" s="T740">Wir gaben auf und sie gingen weg.</ta>
            <ta e="T747" id="Seg_13417" s="T745">Wir sprachen nicht.</ta>
            <ta e="T753" id="Seg_13418" s="T747">Und jetzt sind sie wieder gekommen um zu sprechen.</ta>
            <ta e="T760" id="Seg_13419" s="T754">Sie kamen und ich sage: "Wo wart ihr?"</ta>
            <ta e="T774" id="Seg_13420" s="T760">"Wir haben Bilder vom Wald gemacht, von unserem Haus und von Großmutters Haus und [dann] kamen wir hierher."</ta>
            <ta e="T779" id="Seg_13421" s="T775">Dann kam die Mutter [aus] dem Krankenhaus.</ta>
            <ta e="T785" id="Seg_13422" s="T779">Sie fing an ihre Kinder zu schelten: Sie machen nichts.</ta>
            <ta e="T789" id="Seg_13423" s="T785">"Nimm [es], tu die Pflöcke in den Zaun.</ta>
            <ta e="T795" id="Seg_13424" s="T789">Oder dein Vater kommt, er gibt nichts."</ta>
            <ta e="T800" id="Seg_13425" s="T795">Er ging und fing an [die Pflöcke] aufzustellen.</ta>
            <ta e="T804" id="Seg_13426" s="T800">Dann kam er, sie wuschen [sich] in der Sauna.</ta>
            <ta e="T809" id="Seg_13427" s="T804">Dann sagen sie zu mir: "Geh in die Sauna."</ta>
            <ta e="T812" id="Seg_13428" s="T809">Ich ging zum waschen.</ta>
            <ta e="T816" id="Seg_13429" s="T812">Dann Elja kam zum waschen.</ta>
            <ta e="T820" id="Seg_13430" s="T816">Dann kam [ein] Mann und wusch sich.</ta>
            <ta e="T823" id="Seg_13431" s="T820">Dann kam [sein?] Vater.</ta>
            <ta e="T825" id="Seg_13432" s="T823">Er brachte eine Hose.</ta>
            <ta e="T827" id="Seg_13433" s="T825">Er brachte die Hosen.</ta>
            <ta e="T832" id="Seg_13434" s="T828">Dann kam er und brachte Wodka.</ta>
            <ta e="T837" id="Seg_13435" s="T832">Ein Mann [hatte] ihn/es mit einem Pferd gebracht.</ta>
            <ta e="T842" id="Seg_13436" s="T837">Dann goss er im etwas Wodka ein.</ta>
            <ta e="T847" id="Seg_13437" s="T842">Er aß ein (bisschen?), [dann] ging er nach Hause.</ta>
            <ta e="T852" id="Seg_13438" s="T848">Diese Frau ist sehr böse.</ta>
            <ta e="T860" id="Seg_13439" s="T852">Sie nahm einen Stock und schlug das Kalb, ins Auge.</ta>
            <ta e="T863" id="Seg_13440" s="T860">Träne(?) fließen.</ta>
            <ta e="T870" id="Seg_13441" s="T863">Und ich schlage mein Kalb nicht (mit der Hand).</ta>
            <ta e="T875" id="Seg_13442" s="T870">(…)</ta>
            <ta e="T882" id="Seg_13443" s="T876">Ich habe Bilder von der Maschine zuhause gemacht, und…</ta>
            <ta e="T887" id="Seg_13444" s="T882">Sie… eins, zwei, drei, vier.</ta>
            <ta e="T890" id="Seg_13445" s="T887">Dann sagt Vanja:</ta>
            <ta e="T898" id="Seg_13446" s="T890">"Wir brauchen ein Kalb und Schweine", und wir lachten.</ta>
            <ta e="T903" id="Seg_13447" s="T899">Heute müssen wir viel gesprochen haben.</ta>
            <ta e="T911" id="Seg_13448" s="T903">Jetzt muss ich nach Hause gehen, um zu essen, [eine Weile?] zu liegen.</ta>
            <ta e="T916" id="Seg_13449" s="T912">Der Rauch steigt [hoch] in die Luft auf.</ta>
            <ta e="T919" id="Seg_13450" s="T916">Es wird sehr kalt sein.</ta>
            <ta e="T922" id="Seg_13451" s="T919">Heute Nacht liegt der Schnee [auf dem Boden].</ta>
            <ta e="T927" id="Seg_13452" s="T922">Am Morgen stand ich auf ‒ es war sehr kalt.</ta>
            <ta e="T933" id="Seg_13453" s="T928">Wir sollten heute zu Tanja gehen.</ta>
            <ta e="T941" id="Seg_13454" s="T933">[Um sie zu bitten], Brot aus Voznesenskoje zu holen und uns zu bringen.</ta>
            <ta e="T947" id="Seg_13455" s="T942">(…) [wir] müssen Geld für sie nehmen.</ta>
            <ta e="T953" id="Seg_13456" s="T947">(…)</ta>
            <ta e="T955" id="Seg_13457" s="T953">Und wird bringen.</ta>
            <ta e="T961" id="Seg_13458" s="T956">Ich weiß nicht, ob sie [es] holt oder nicht.</ta>
            <ta e="T967" id="Seg_13459" s="T961">Ich sollte gehen und mit ihr sprechen, vielleicht wird sie es holen.</ta>
            <ta e="T970" id="Seg_13460" s="T967">Nun, geh, sprich.</ta>
            <ta e="T976" id="Seg_13461" s="T971">Vielleicht geht sie dort in den Laden.</ta>
            <ta e="T981" id="Seg_13462" s="T976">"Man muss mich anstellen um Brot zu kaufen.</ta>
            <ta e="T984" id="Seg_13463" s="T981">Ich werde nicht."</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T4" id="Seg_13464" s="T0">[GVY:] ob bĭrəj: ob 'one' + Turkic bir- (cf. Khakas pirge etc. 'together')</ta>
            <ta e="T116" id="Seg_13465" s="T111">[GVY:] unclear. Maybe the last stem is not en- 'put', but some other verb. The same in the next sentence.</ta>
            <ta e="T126" id="Seg_13466" s="T123">[GVY:] padvalbə = padvalgə?</ta>
            <ta e="T159" id="Seg_13467" s="T156">[GVY:] agarottə: LAT instead of ABL?</ta>
            <ta e="T167" id="Seg_13468" s="T163">[GVY:] cf. toltano 'potatoes'; but it is strange to 'pick' potatoes.</ta>
            <ta e="T190" id="Seg_13469" s="T185">!!! nastupili</ta>
            <ta e="T205" id="Seg_13470" s="T201">[GVY:] Unclear. Deʔkeʔ 'give-IMP.2PL' as a loan translation of the Russian "давайте" (but "a" is then out of place)?</ta>
            <ta e="T224" id="Seg_13471" s="T214">[GVY:] oʔ-, əʔ (D 19a, 49b) 'let; tell' means here 'make fun'?</ta>
            <ta e="T246" id="Seg_13472" s="T239">[GVY:] Dĭn = dĭm?</ta>
            <ta e="T263" id="Seg_13473" s="T256">[GVY:] she hacked it?</ta>
            <ta e="T340" id="Seg_13474" s="T335">[GVY:] len = lem?</ta>
            <ta e="T362" id="Seg_13475" s="T356">[GVY:] Russ. "С добрым утром и хорошего дня тебе".</ta>
            <ta e="T376" id="Seg_13476" s="T371">[GVY:] presumably she tries to say kunolbiam 'I slept'.</ta>
            <ta e="T407" id="Seg_13477" s="T404">[GVY:] iet = iat [mother-3SG]?</ta>
            <ta e="T414" id="Seg_13478" s="T407">[GVY:] băzajdlam</ta>
            <ta e="T423" id="Seg_13479" s="T414">[GVY:] băzajdəbi</ta>
            <ta e="T438" id="Seg_13480" s="T430">[GVY:] besʼtə = bej- 'cross, carry over ("he took a car to carry the shirts"?; überfahren (D 9b)'? maʔndən?</ta>
            <ta e="T456" id="Seg_13481" s="T451">[GVY:] a ia 'and the mother'?</ta>
            <ta e="T485" id="Seg_13482" s="T479">[GVY:] kon = kondʼo?</ta>
            <ta e="T501" id="Seg_13483" s="T496">[GVY:] Unclear. Koʔsittə 'to dry', i.e. take the berries out of vodka?</ta>
            <ta e="T535" id="Seg_13484" s="T531">[GVY:] should be mĭbiem?</ta>
            <ta e="T587" id="Seg_13485" s="T584">[GVY:] Unclear.</ta>
            <ta e="T628" id="Seg_13486" s="T623">[GVY:] Not sure. Ianə ĭmbidə iʔ manaʔ 'Don't say anything to your mother' or Ial ĭmbidə ej măn[d]ə 'Your mother doesn't say anything'? The ʔ in mănaʔ is rather absent.</ta>
            <ta e="T643" id="Seg_13487" s="T637">[GVY:] Unclear.</ta>
            <ta e="T703" id="Seg_13488" s="T697">[GVY:] bĭtəlbiem = bĭtəjbiem?</ta>
            <ta e="T716" id="Seg_13489" s="T712">[GVY:] sabiam</ta>
            <ta e="T719" id="Seg_13490" s="T716">[GVY:] sajbiam</ta>
            <ta e="T785" id="Seg_13491" s="T779">[GVY:] ej elliaʔi 'don't put' (cf. the next sentence).</ta>
            <ta e="T804" id="Seg_13492" s="T800">[GVY:] măltʼa-?</ta>
            <ta e="T816" id="Seg_13493" s="T812">[GVY:] Here, büzəjdə- is with a rounded vowel in the first syllable (cf. various variants by Donner)</ta>
            <ta e="T827" id="Seg_13494" s="T825">[GVY:] Sounds rather like piʔneʔi, but piʔne- 'behind' seems out of place here.</ta>
            <ta e="T870" id="Seg_13495" s="T863">[GVY:] "Udazi" may be a part of the next sentence.</ta>
            <ta e="T875" id="Seg_13496" s="T870">[GVY:] Unclear</ta>
            <ta e="T882" id="Seg_13497" s="T876">[GVY:] kür- 'take picture'? Unclear</ta>
            <ta e="T898" id="Seg_13498" s="T890">[GVY:] Unclear.</ta>
            <ta e="T911" id="Seg_13499" s="T903">[GVY:] šalaʔiej?</ta>
            <ta e="T953" id="Seg_13500" s="T947">[GVY:] Unclear</ta>
            <ta e="T967" id="Seg_13501" s="T961">[GVY:] možetʼ</ta>
            <ta e="T976" id="Seg_13502" s="T971">если это пойдет, то почему Loc?</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T988" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T986" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T987" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T927" />
            <conversion-tli id="T928" />
            <conversion-tli id="T929" />
            <conversion-tli id="T930" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T935" />
            <conversion-tli id="T936" />
            <conversion-tli id="T937" />
            <conversion-tli id="T938" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T941" />
            <conversion-tli id="T942" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
            <conversion-tli id="T949" />
            <conversion-tli id="T950" />
            <conversion-tli id="T951" />
            <conversion-tli id="T952" />
            <conversion-tli id="T953" />
            <conversion-tli id="T954" />
            <conversion-tli id="T955" />
            <conversion-tli id="T956" />
            <conversion-tli id="T957" />
            <conversion-tli id="T958" />
            <conversion-tli id="T959" />
            <conversion-tli id="T960" />
            <conversion-tli id="T961" />
            <conversion-tli id="T962" />
            <conversion-tli id="T963" />
            <conversion-tli id="T964" />
            <conversion-tli id="T965" />
            <conversion-tli id="T966" />
            <conversion-tli id="T967" />
            <conversion-tli id="T968" />
            <conversion-tli id="T969" />
            <conversion-tli id="T970" />
            <conversion-tli id="T971" />
            <conversion-tli id="T972" />
            <conversion-tli id="T973" />
            <conversion-tli id="T974" />
            <conversion-tli id="T975" />
            <conversion-tli id="T976" />
            <conversion-tli id="T977" />
            <conversion-tli id="T978" />
            <conversion-tli id="T979" />
            <conversion-tli id="T980" />
            <conversion-tli id="T981" />
            <conversion-tli id="T982" />
            <conversion-tli id="T983" />
            <conversion-tli id="T984" />
            <conversion-tli id="T985" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
