<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID91865968-8BBD-B8CD-2A0C-EA4838857CC3">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_SU0217.wav" />
         <referenced-file url="PKZ_196X_SU0217.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\misc\PKZ_196X_SU0217\PKZ_196X_SU0217.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1593</ud-information>
            <ud-information attribute-name="# HIAT:w">993</ud-information>
            <ud-information attribute-name="# e">1019</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">29</ud-information>
            <ud-information attribute-name="# HIAT:u">205</ud-information>
            <ud-information attribute-name="# sc">6</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="KA">
            <abbreviation>KA</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.93" type="appl" />
         <tli id="T1" time="2.0" type="appl" />
         <tli id="T2" time="3.07" type="appl" />
         <tli id="T3" time="4.152" type="appl" />
         <tli id="T4" time="5.025" type="appl" />
         <tli id="T5" time="5.897" type="appl" />
         <tli id="T6" time="6.77" type="appl" />
         <tli id="T7" time="8.733" type="appl" />
         <tli id="T8" time="10.587" type="appl" />
         <tli id="T9" time="12.693238526417392" />
         <tli id="T10" time="13.239" type="appl" />
         <tli id="T11" time="13.918" type="appl" />
         <tli id="T12" time="14.597" type="appl" />
         <tli id="T13" time="15.276" type="appl" />
         <tli id="T14" time="15.955" type="appl" />
         <tli id="T15" time="16.634" type="appl" />
         <tli id="T16" time="17.313" type="appl" />
         <tli id="T776" time="17.82165506695864" type="intp" />
         <tli id="T17" time="18.4998618229035" />
         <tli id="T18" time="19.879851515639007" />
         <tli id="T19" time="22.253167122469144" />
         <tli id="T20" time="23.067" type="appl" />
         <tli id="T21" time="23.834" type="appl" />
         <tli id="T22" time="24.601" type="appl" />
         <tli id="T23" time="25.369" type="appl" />
         <tli id="T24" time="26.136" type="appl" />
         <tli id="T25" time="26.903" type="appl" />
         <tli id="T26" time="28.139789821432675" />
         <tli id="T27" time="28.895" type="appl" />
         <tli id="T28" time="29.849" type="appl" />
         <tli id="T29" time="30.804" type="appl" />
         <tli id="T30" time="31.758" type="appl" />
         <tli id="T31" time="32.713" type="appl" />
         <tli id="T32" time="33.56" type="appl" />
         <tli id="T33" time="34.2" type="appl" />
         <tli id="T34" time="34.84" type="appl" />
         <tli id="T35" time="35.48" type="appl" />
         <tli id="T36" time="36.12" type="appl" />
         <tli id="T37" time="39.01970855836187" />
         <tli id="T38" time="39.768" type="appl" />
         <tli id="T39" time="40.615" type="appl" />
         <tli id="T40" time="41.463" type="appl" />
         <tli id="T41" time="42.31" type="appl" />
         <tli id="T42" time="42.885" type="appl" />
         <tli id="T43" time="43.43" type="appl" />
         <tli id="T44" time="43.975" type="appl" />
         <tli id="T45" time="44.52" type="appl" />
         <tli id="T46" time="45.422" type="appl" />
         <tli id="T47" time="46.17" type="appl" />
         <tli id="T48" time="46.918" type="appl" />
         <tli id="T49" time="47.665" type="appl" />
         <tli id="T50" time="48.82" type="appl" />
         <tli id="T51" time="50.666288235699675" />
         <tli id="T52" time="52.272" type="appl" />
         <tli id="T53" time="53.725" type="appl" />
         <tli id="T54" time="54.707" type="appl" />
         <tli id="T55" time="55.57366694663941" />
         <tli id="T56" time="56.635" type="appl" />
         <tli id="T57" time="57.579" type="appl" />
         <tli id="T58" time="58.524" type="appl" />
         <tli id="T59" time="59.468" type="appl" />
         <tli id="T60" time="60.413" type="appl" />
         <tli id="T61" time="61.357" type="appl" />
         <tli id="T62" time="62.302" type="appl" />
         <tli id="T63" time="63.098" type="appl" />
         <tli id="T64" time="63.847" type="appl" />
         <tli id="T65" time="64.595" type="appl" />
         <tli id="T66" time="65.343" type="appl" />
         <tli id="T67" time="66.092" type="appl" />
         <tli id="T68" time="66.84" type="appl" />
         <tli id="T69" time="67.843" type="appl" />
         <tli id="T70" time="68.757" type="appl" />
         <tli id="T71" time="69.67" type="appl" />
         <tli id="T72" time="71.65279815227633" />
         <tli id="T73" time="72.458" type="appl" />
         <tli id="T74" time="73.105" type="appl" />
         <tli id="T75" time="73.753" type="appl" />
         <tli id="T76" time="74.401" type="appl" />
         <tli id="T77" time="75.048" type="appl" />
         <tli id="T78" time="75.696" type="appl" />
         <tli id="T79" time="76.344" type="appl" />
         <tli id="T80" time="76.992" type="appl" />
         <tli id="T81" time="77.639" type="appl" />
         <tli id="T82" time="78.287" type="appl" />
         <tli id="T83" time="79.793" type="appl" />
         <tli id="T84" time="81.217" type="appl" />
         <tli id="T85" time="82.64" type="appl" />
         <tli id="T86" time="83.555" type="appl" />
         <tli id="T87" time="84.419" type="appl" />
         <tli id="T88" time="85.63936035207868" />
         <tli id="T89" time="86.186" type="appl" />
         <tli id="T90" time="86.932" type="appl" />
         <tli id="T91" time="87.679" type="appl" />
         <tli id="T92" time="88.425" type="appl" />
         <tli id="T93" time="89.171" type="appl" />
         <tli id="T94" time="89.917" type="appl" />
         <tli id="T95" time="90.664" type="appl" />
         <tli id="T96" time="91.41" type="appl" />
         <tli id="T97" time="92.156" type="appl" />
         <tli id="T98" time="93.21263711994116" />
         <tli id="T99" time="93.933" type="appl" />
         <tli id="T100" time="94.543" type="appl" />
         <tli id="T101" time="95.153" type="appl" />
         <tli id="T102" time="95.763" type="appl" />
         <tli id="T103" time="96.373" type="appl" />
         <tli id="T104" time="96.983" type="appl" />
         <tli id="T105" time="97.593" type="appl" />
         <tli id="T106" time="98.85926161147243" />
         <tli id="T107" time="99.938" type="appl" />
         <tli id="T108" time="100.957" type="appl" />
         <tli id="T109" time="101.975" type="appl" />
         <tli id="T110" time="102.802" type="appl" />
         <tli id="T111" time="103.63" type="appl" />
         <tli id="T112" time="104.458" type="appl" />
         <tli id="T113" time="105.285" type="appl" />
         <tli id="T114" time="106.255" type="appl" />
         <tli id="T115" time="106.79" type="appl" />
         <tli id="T116" time="107.31" type="appl" />
         <tli id="T117" time="107.971" type="appl" />
         <tli id="T118" time="108.577" type="appl" />
         <tli id="T119" time="109.183" type="appl" />
         <tli id="T120" time="109.789" type="appl" />
         <tli id="T121" time="110.395" type="appl" />
         <tli id="T122" time="111.225" type="appl" />
         <tli id="T123" time="111.895" type="appl" />
         <tli id="T124" time="112.565" type="appl" />
         <tli id="T125" time="113.235" type="appl" />
         <tli id="T126" time="113.905" type="appl" />
         <tli id="T127" time="114.575" type="appl" />
         <tli id="T128" time="115.178" type="appl" />
         <tli id="T129" time="115.637" type="appl" />
         <tli id="T130" time="116.095" type="appl" />
         <tli id="T131" time="116.553" type="appl" />
         <tli id="T132" time="117.012" type="appl" />
         <tli id="T133" time="117.47" type="appl" />
         <tli id="T134" time="118.61" type="appl" />
         <tli id="T135" time="119.72" type="appl" />
         <tli id="T136" time="120.83" type="appl" />
         <tli id="T137" time="121.94" type="appl" />
         <tli id="T138" time="123.05" type="appl" />
         <tli id="T139" time="124.16" type="appl" />
         <tli id="T140" time="125.27" type="appl" />
         <tli id="T141" time="126.38" type="appl" />
         <tli id="T142" time="127.49" type="appl" />
         <tli id="T143" time="128.6" type="appl" />
         <tli id="T144" time="129.71" type="appl" />
         <tli id="T145" time="130.439" type="appl" />
         <tli id="T146" time="130.897" type="appl" />
         <tli id="T1017" time="131.09371428571427" type="intp" />
         <tli id="T147" time="131.356" type="appl" />
         <tli id="T148" time="131.814" type="appl" />
         <tli id="T149" time="132.273" type="appl" />
         <tli id="T150" time="132.731" type="appl" />
         <tli id="T151" time="133.19" type="appl" />
         <tli id="T152" time="134.26" type="appl" />
         <tli id="T153" time="139.73895627743434" />
         <tli id="T154" time="140.658" type="appl" />
         <tli id="T155" time="141.465" type="appl" />
         <tli id="T156" time="142.272" type="appl" />
         <tli id="T157" time="144.14559003056556" />
         <tli id="T158" time="144.784" type="appl" />
         <tli id="T159" time="145.318" type="appl" />
         <tli id="T160" time="145.852" type="appl" />
         <tli id="T161" time="146.386" type="appl" />
         <tli id="T162" time="146.87999669037487" />
         <tli id="T163" time="147.364" type="appl" />
         <tli id="T164" time="147.808" type="appl" />
         <tli id="T165" time="148.252" type="appl" />
         <tli id="T166" time="148.696" type="appl" />
         <tli id="T167" time="150.1122121320065" />
         <tli id="T168" time="150.876" type="appl" />
         <tli id="T169" time="151.802" type="appl" />
         <tli id="T170" time="152.728" type="appl" />
         <tli id="T171" time="153.655" type="appl" />
         <tli id="T172" time="154.581" type="appl" />
         <tli id="T173" time="156.2721661227679" />
         <tli id="T174" time="156.728" type="appl" />
         <tli id="T175" time="157.327" type="appl" />
         <tli id="T176" time="157.925" type="appl" />
         <tli id="T177" time="158.524" type="appl" />
         <tli id="T178" time="159.122" type="appl" />
         <tli id="T179" time="159.887" type="appl" />
         <tli id="T180" time="160.652" type="appl" />
         <tli id="T181" time="161.417" type="appl" />
         <tli id="T182" time="162.182" type="appl" />
         <tli id="T183" time="163.107" type="appl" />
         <tli id="T184" time="163.85877612437662" />
         <tli id="T185" time="164.774" type="appl" />
         <tli id="T186" time="165.417" type="appl" />
         <tli id="T187" time="166.06" type="appl" />
         <tli id="T188" time="173.92536760278537" />
         <tli id="T189" time="174.842" type="appl" />
         <tli id="T190" time="175.722" type="appl" />
         <tli id="T191" time="176.70200050970308" />
         <tli id="T192" time="177.777" type="appl" />
         <tli id="T193" time="178.937" type="appl" />
         <tli id="T194" time="180.097" type="appl" />
         <tli id="T195" time="182.24530546043712" />
         <tli id="T196" time="182.989" type="appl" />
         <tli id="T197" time="184.077" type="appl" />
         <tli id="T198" time="185.166" type="appl" />
         <tli id="T199" time="186.254" type="appl" />
         <tli id="T200" time="187.343" type="appl" />
         <tli id="T201" time="188.431" type="appl" />
         <tli id="T1018" time="188.8977142857143" type="intp" />
         <tli id="T202" time="189.52" type="appl" />
         <tli id="T203" time="190.23" type="appl" />
         <tli id="T204" time="190.941" type="appl" />
         <tli id="T205" time="191.651" type="appl" />
         <tli id="T206" time="192.362" type="appl" />
         <tli id="T207" time="193.93855145588674" />
         <tli id="T208" time="195.056" type="appl" />
         <tli id="T209" time="195.991" type="appl" />
         <tli id="T210" time="196.85" type="appl" />
         <tli id="T211" time="197.699" type="appl" />
         <tli id="T212" time="198.547" type="appl" />
         <tli id="T213" time="199.396" type="appl" />
         <tli id="T214" time="200.245" type="appl" />
         <tli id="T215" time="201.094" type="appl" />
         <tli id="T216" time="201.942" type="appl" />
         <tli id="T217" time="202.791" type="appl" />
         <tli id="T218" time="203.6066693480298" />
         <tli id="T219" time="204.845" type="appl" />
         <tli id="T220" time="206.05" type="appl" />
         <tli id="T221" time="207.255" type="appl" />
         <tli id="T222" time="208.78510723232137" />
         <tli id="T223" time="209.088" type="appl" />
         <tli id="T224" time="209.49" type="appl" />
         <tli id="T225" time="209.892" type="appl" />
         <tli id="T226" time="215.10506002803757" />
         <tli id="T227" time="216.015" type="appl" />
         <tli id="T228" time="216.63838190885483" />
         <tli id="T229" time="217.428" type="appl" />
         <tli id="T230" time="218.146" type="appl" />
         <tli id="T231" time="218.864" type="appl" />
         <tli id="T232" time="219.581" type="appl" />
         <tli id="T233" time="220.299" type="appl" />
         <tli id="T234" time="221.017" type="appl" />
         <tli id="T235" time="221.89834262174526" />
         <tli id="T236" time="222.807" type="appl" />
         <tli id="T237" time="223.592" type="appl" />
         <tli id="T238" time="224.377" type="appl" />
         <tli id="T239" time="225.162" type="appl" />
         <tli id="T240" time="226.275" type="appl" />
         <tli id="T241" time="227.032" type="appl" />
         <tli id="T242" time="227.91163104129802" />
         <tli id="T243" time="229.095" type="appl" />
         <tli id="T244" time="230.261" type="appl" />
         <tli id="T245" time="231.428" type="appl" />
         <tli id="T246" time="232.061" type="appl" />
         <tli id="T247" time="232.662" type="appl" />
         <tli id="T248" time="233.263" type="appl" />
         <tli id="T249" time="233.864" type="appl" />
         <tli id="T250" time="234.465" type="appl" />
         <tli id="T251" time="235.065" type="appl" />
         <tli id="T252" time="235.666" type="appl" />
         <tli id="T253" time="236.267" type="appl" />
         <tli id="T254" time="236.868" type="appl" />
         <tli id="T255" time="237.469" type="appl" />
         <tli id="T1019" time="237.83866566313515" type="intp" />
         <tli id="T256" time="238.33155321398203" />
         <tli id="T257" time="239.53821086801648" />
         <tli id="T258" time="240.293" type="appl" />
         <tli id="T259" time="240.817" type="appl" />
         <tli id="T260" time="241.34" type="appl" />
         <tli id="T261" time="241.863" type="appl" />
         <tli id="T262" time="242.387" type="appl" />
         <tli id="T263" time="243.1848502976122" />
         <tli id="T264" time="243.851" type="appl" />
         <tli id="T265" time="244.473" type="appl" />
         <tli id="T266" time="245.094" type="appl" />
         <tli id="T267" time="245.715" type="appl" />
         <tli id="T268" time="246.337" type="appl" />
         <tli id="T269" time="246.958" type="appl" />
         <tli id="T270" time="247.579" type="appl" />
         <tli id="T271" time="248.201" type="appl" />
         <tli id="T272" time="248.822" type="appl" />
         <tli id="T273" time="249.443" type="appl" />
         <tli id="T274" time="250.065" type="appl" />
         <tli id="T275" time="250.686" type="appl" />
         <tli id="T276" time="251.307" type="appl" />
         <tli id="T277" time="251.929" type="appl" />
         <tli id="T1020" time="252.30433361950944" type="intp" />
         <tli id="T278" time="252.80477844552203" />
         <tli id="T279" time="255.6914235515823" />
         <tli id="T280" time="256.992" type="appl" />
         <tli id="T281" time="257.811" type="appl" />
         <tli id="T282" time="258.629" type="appl" />
         <tli id="T283" time="259.448" type="appl" />
         <tli id="T284" time="260.266" type="appl" />
         <tli id="T285" time="261.085" type="appl" />
         <tli id="T286" time="261.903" type="appl" />
         <tli id="T287" time="262.722" type="appl" />
         <tli id="T1021" time="262.95051243306597" type="intp" />
         <tli id="T288" time="263.33136648817595" />
         <tli id="T289" time="264.272" type="appl" />
         <tli id="T290" time="264.824" type="appl" />
         <tli id="T291" time="265.377" type="appl" />
         <tli id="T292" time="265.929" type="appl" />
         <tli id="T293" time="266.481" type="appl" />
         <tli id="T294" time="267.033" type="appl" />
         <tli id="T295" time="267.586" type="appl" />
         <tli id="T296" time="268.138" type="appl" />
         <tli id="T297" time="268.69" type="appl" />
         <tli id="T298" time="269.856" type="appl" />
         <tli id="T299" time="270.741" type="appl" />
         <tli id="T1022" time="271.149" type="intp" />
         <tli id="T300" time="271.625" type="appl" />
         <tli id="T301" time="272.509" type="appl" />
         <tli id="T302" time="273.393" type="appl" />
         <tli id="T303" time="274.278" type="appl" />
         <tli id="T304" time="275.2579440741847" />
         <tli id="T305" time="277.2379292855009" />
         <tli id="T306" time="277.869" type="appl" />
         <tli id="T307" time="278.432" type="appl" />
         <tli id="T308" time="278.996" type="appl" />
         <tli id="T309" time="279.56" type="appl" />
         <tli id="T310" time="280.124" type="appl" />
         <tli id="T311" time="280.688" type="appl" />
         <tli id="T312" time="281.251" type="appl" />
         <tli id="T313" time="281.815" type="appl" />
         <tli id="T314" time="285.27120261760854" />
         <tli id="T315" time="286.166" type="appl" />
         <tli id="T316" time="286.823" type="appl" />
         <tli id="T317" time="287.479" type="appl" />
         <tli id="T318" time="288.136" type="appl" />
         <tli id="T319" time="288.792" type="appl" />
         <tli id="T320" time="289.449" type="appl" />
         <tli id="T321" time="290.105" type="appl" />
         <tli id="T322" time="292.71781366488176" />
         <tli id="T323" time="293.819" type="appl" />
         <tli id="T324" time="294.785" type="appl" />
         <tli id="T325" time="296.01778901707536" />
         <tli id="T326" time="297.265" type="appl" />
         <tli id="T327" time="298.234" type="appl" />
         <tli id="T328" time="299.202" type="appl" />
         <tli id="T329" time="300.6510877438926" />
         <tli id="T330" time="301.564" type="appl" />
         <tli id="T331" time="302.468" type="appl" />
         <tli id="T332" time="303.372" type="appl" />
         <tli id="T333" time="304.277" type="appl" />
         <tli id="T334" time="305.181" type="appl" />
         <tli id="T335" time="306.6310430790798" />
         <tli id="T336" time="307.713" type="appl" />
         <tli id="T337" time="308.515" type="appl" />
         <tli id="T338" time="309.318" type="appl" />
         <tli id="T339" time="310.12" type="appl" />
         <tli id="T340" time="310.923" type="appl" />
         <tli id="T341" time="311.726" type="appl" />
         <tli id="T342" time="312.528" type="appl" />
         <tli id="T343" time="313.331" type="appl" />
         <tli id="T344" time="314.133" type="appl" />
         <tli id="T345" time="314.936" type="appl" />
         <tli id="T346" time="315.738" type="appl" />
         <tli id="T347" time="316.541" type="appl" />
         <tli id="T348" time="317.313" type="appl" />
         <tli id="T349" time="318.077" type="appl" />
         <tli id="T350" time="319.0842833980649" />
         <tli id="T351" time="319.85" type="appl" />
         <tli id="T352" time="320.41" type="appl" />
         <tli id="T353" time="320.97" type="appl" />
         <tli id="T354" time="321.53" type="appl" />
         <tli id="T355" time="322.09" type="appl" />
         <tli id="T356" time="322.735" type="appl" />
         <tli id="T357" time="323.35" type="appl" />
         <tli id="T358" time="323.965" type="appl" />
         <tli id="T359" time="324.58" type="appl" />
         <tli id="T360" time="325.195" type="appl" />
         <tli id="T361" time="325.89665439086633" />
         <tli id="T362" time="326.457" type="appl" />
         <tli id="T363" time="327.103" type="appl" />
         <tli id="T364" time="327.8508845862156" />
         <tli id="T365" time="328.784" type="appl" />
         <tli id="T366" time="329.688" type="appl" />
         <tli id="T367" time="330.592" type="appl" />
         <tli id="T368" time="331.496" type="appl" />
         <tli id="T369" time="332.4" type="appl" />
         <tli id="T370" time="333.304" type="appl" />
         <tli id="T371" time="334.208" type="appl" />
         <tli id="T372" time="335.112" type="appl" />
         <tli id="T373" time="336.016" type="appl" />
         <tli id="T374" time="336.92" type="appl" />
         <tli id="T375" time="337.945" type="appl" />
         <tli id="T376" time="340.02412699652973" />
         <tli id="T377" time="340.803" type="appl" />
         <tli id="T378" time="341.416" type="appl" />
         <tli id="T379" time="342.029" type="appl" />
         <tli id="T380" time="342.641" type="appl" />
         <tli id="T381" time="343.254" type="appl" />
         <tli id="T382" time="343.867" type="appl" />
         <tli id="T383" time="344.48" type="appl" />
         <tli id="T384" time="345.093" type="appl" />
         <tli id="T385" time="345.706" type="appl" />
         <tli id="T386" time="346.319" type="appl" />
         <tli id="T387" time="346.931" type="appl" />
         <tli id="T388" time="347.544" type="appl" />
         <tli id="T389" time="348.157" type="appl" />
         <tli id="T390" time="349.88405335187184" />
         <tli id="T391" time="350.8" type="appl" />
         <tli id="T392" time="351.6707066738676" />
         <tli id="T393" time="352.263" type="appl" />
         <tli id="T394" time="352.891" type="appl" />
         <tli id="T395" time="353.519" type="appl" />
         <tli id="T396" time="354.147" type="appl" />
         <tli id="T397" time="354.775" type="appl" />
         <tli id="T398" time="355.3" type="appl" />
         <tli id="T399" time="355.82" type="appl" />
         <tli id="T400" time="356.34" type="appl" />
         <tli id="T401" time="356.86" type="appl" />
         <tli id="T402" time="357.38" type="appl" />
         <tli id="T403" time="357.9" type="appl" />
         <tli id="T404" time="358.42" type="appl" />
         <tli id="T405" time="359.297316376715" />
         <tli id="T406" time="360.371" type="appl" />
         <tli id="T407" time="361.213" type="appl" />
         <tli id="T408" time="362.054" type="appl" />
         <tli id="T409" time="362.896" type="appl" />
         <tli id="T410" time="363.738" type="appl" />
         <tli id="T411" time="365.0839398225817" />
         <tli id="T412" time="366.9372593133087" />
         <tli id="T413" time="367.42" type="appl" />
         <tli id="T414" time="367.97" type="appl" />
         <tli id="T415" time="368.6105801484614" />
         <tli id="T416" time="369.1" type="appl" />
         <tli id="T417" time="369.67" type="appl" />
         <tli id="T418" time="370.24" type="appl" />
         <tli id="T419" time="370.81" type="appl" />
         <tli id="T420" time="371.38" type="appl" />
         <tli id="T421" time="372.199" type="appl" />
         <tli id="T422" time="373.018" type="appl" />
         <tli id="T423" time="373.837" type="appl" />
         <tli id="T424" time="374.656" type="appl" />
         <tli id="T425" time="375.475" type="appl" />
         <tli id="T426" time="376.294" type="appl" />
         <tli id="T1023" time="376.645" type="intp" />
         <tli id="T427" time="377.113" type="appl" />
         <tli id="T428" time="377.932" type="appl" />
         <tli id="T429" time="378.751" type="appl" />
         <tli id="T430" time="379.57" type="appl" />
         <tli id="T431" time="381.257152356767" />
         <tli id="T432" time="382.121" type="appl" />
         <tli id="T433" time="382.736" type="appl" />
         <tli id="T1024" time="383.0" type="intp" />
         <tli id="T434" time="383.352" type="appl" />
         <tli id="T435" time="383.968" type="appl" />
         <tli id="T436" time="384.584" type="appl" />
         <tli id="T437" time="385.199" type="appl" />
         <tli id="T438" time="385.815" type="appl" />
         <tli id="T439" time="386.632" type="appl" />
         <tli id="T440" time="387.338" type="appl" />
         <tli id="T441" time="388.5504312159585" />
         <tli id="T442" time="389.93" type="appl" />
         <tli id="T443" time="390.97" type="appl" />
         <tli id="T444" time="392.01" type="appl" />
         <tli id="T445" time="393.05" type="appl" />
         <tli id="T446" time="394.8170510766897" />
         <tli id="T447" time="395.937" type="appl" />
         <tli id="T448" time="397.053" type="appl" />
         <tli id="T449" time="398.17" type="appl" />
         <tli id="T450" time="399.287" type="appl" />
         <tli id="T451" time="400.403" type="appl" />
         <tli id="T452" time="401.52" type="appl" />
         <tli id="T453" time="402.637" type="appl" />
         <tli id="T454" time="403.753" type="appl" />
         <tli id="T455" time="404.9169756394641" />
         <tli id="T456" time="406.502" type="appl" />
         <tli id="T457" time="407.865" type="appl" />
         <tli id="T458" time="409.227" type="appl" />
         <tli id="T459" time="410.59" type="appl" />
         <tli id="T460" time="411.377" type="appl" />
         <tli id="T461" time="412.163" type="appl" />
         <tli id="T462" time="413.4235787695631" />
         <tli id="T463" time="414.57" type="appl" />
         <tli id="T464" time="415.55" type="appl" />
         <tli id="T465" time="416.69022103739115" />
         <tli id="T466" time="417.409" type="appl" />
         <tli id="T467" time="418.067" type="appl" />
         <tli id="T468" time="418.726" type="appl" />
         <tli id="T469" time="419.385" type="appl" />
         <tli id="T470" time="420.043" type="appl" />
         <tli id="T471" time="420.702" type="appl" />
         <tli id="T472" time="421.361" type="appl" />
         <tli id="T473" time="422.019" type="appl" />
         <tli id="T474" time="422.678" type="appl" />
         <tli id="T475" time="423.287" type="appl" />
         <tli id="T476" time="423.824" type="appl" />
         <tli id="T477" time="424.361" type="appl" />
         <tli id="T478" time="424.899" type="appl" />
         <tli id="T479" time="425.436" type="appl" />
         <tli id="T480" time="425.973" type="appl" />
         <tli id="T481" time="426.51" type="appl" />
         <tli id="T482" time="427.047" type="appl" />
         <tli id="T483" time="427.584" type="appl" />
         <tli id="T484" time="428.121" type="appl" />
         <tli id="T485" time="428.659" type="appl" />
         <tli id="T486" time="429.196" type="appl" />
         <tli id="T487" time="429.733" type="appl" />
         <tli id="T488" time="430.27" type="appl" />
         <tli id="T489" time="432.11677248178705" />
         <tli id="T490" time="433.016" type="appl" />
         <tli id="T491" time="433.732" type="appl" />
         <tli id="T492" time="434.448" type="appl" />
         <tli id="T493" time="435.164" type="appl" />
         <tli id="T494" time="435.88" type="appl" />
         <tli id="T495" time="436.677" type="appl" />
         <tli id="T496" time="437.469" type="appl" />
         <tli id="T497" time="438.261" type="appl" />
         <tli id="T498" time="439.053" type="appl" />
         <tli id="T499" time="439.845" type="appl" />
         <tli id="T500" time="440.746" type="appl" />
         <tli id="T501" time="441.431" type="appl" />
         <tli id="T502" time="442.117" type="appl" />
         <tli id="T503" time="442.803" type="appl" />
         <tli id="T504" time="443.489" type="appl" />
         <tli id="T505" time="444.174" type="appl" />
         <tli id="T506" time="444.86" type="appl" />
         <tli id="T507" time="446.105" type="appl" />
         <tli id="T508" time="446.705" type="appl" />
         <tli id="T509" time="447.27" type="appl" />
         <tli id="T510" time="447.882" type="appl" />
         <tli id="T511" time="448.345" type="appl" />
         <tli id="T512" time="448.808" type="appl" />
         <tli id="T513" time="449.27" type="appl" />
         <tli id="T514" time="450.067" type="appl" />
         <tli id="T515" time="450.863" type="appl" />
         <tli id="T516" time="451.66" type="appl" />
         <tli id="T517" time="452.877" type="appl" />
         <tli id="T518" time="453.863" type="appl" />
         <tli id="T519" time="454.85" type="appl" />
         <tli id="T520" time="455.638" type="appl" />
         <tli id="T521" time="456.426" type="appl" />
         <tli id="T522" time="457.214" type="appl" />
         <tli id="T523" time="458.002" type="appl" />
         <tli id="T524" time="458.79" type="appl" />
         <tli id="T525" time="460.255" type="appl" />
         <tli id="T526" time="461.015" type="appl" />
         <tli id="T527" time="461.7965508009101" />
         <tli id="T528" time="462.45" type="appl" />
         <tli id="T529" time="462.87" type="appl" />
         <tli id="T530" time="463.29" type="appl" />
         <tli id="T531" time="463.71" type="appl" />
         <tli id="T532" time="464.845" type="appl" />
         <tli id="T533" time="465.805" type="appl" />
         <tli id="T534" time="466.765" type="appl" />
         <tli id="T535" time="467.33" type="appl" />
         <tli id="T536" time="467.87" type="appl" />
         <tli id="T537" time="468.41" type="appl" />
         <tli id="T538" time="468.95" type="appl" />
         <tli id="T539" time="469.49" type="appl" />
         <tli id="T540" time="470.05" type="appl" />
         <tli id="T541" time="470.595" type="appl" />
         <tli id="T542" time="471.14" type="appl" />
         <tli id="T543" time="471.685" type="appl" />
         <tli id="T544" time="472.23" type="appl" />
         <tli id="T545" time="472.775" type="appl" />
         <tli id="T546" time="473.515" type="appl" />
         <tli id="T547" time="474.205" type="appl" />
         <tli id="T548" time="474.895" type="appl" />
         <tli id="T549" time="475.585" type="appl" />
         <tli id="T550" time="476.275" type="appl" />
         <tli id="T551" time="477.449767218992" />
         <tli id="T552" time="477.877" type="appl" />
         <tli id="T553" time="478.303" type="appl" />
         <tli id="T554" time="478.73" type="appl" />
         <tli id="T555" time="479.157" type="appl" />
         <tli id="T556" time="479.583" type="appl" />
         <tli id="T557" time="480.01" type="appl" />
         <tli id="T558" time="480.905" type="appl" />
         <tli id="T559" time="481.615" type="appl" />
         <tli id="T560" time="482.325" type="appl" />
         <tli id="T561" time="485.6897056741663" />
         <tli id="T562" time="486.568" type="appl" />
         <tli id="T563" time="487.346" type="appl" />
         <tli id="T564" time="488.125" type="appl" />
         <tli id="T565" time="488.903" type="appl" />
         <tli id="T566" time="489.687" type="appl" />
         <tli id="T567" time="490.344" type="appl" />
         <tli id="T568" time="491.001" type="appl" />
         <tli id="T569" time="491.658" type="appl" />
         <tli id="T570" time="492.314" type="appl" />
         <tli id="T571" time="492.971" type="appl" />
         <tli id="T572" time="493.628" type="appl" />
         <tli id="T573" time="494.285" type="appl" />
         <tli id="T574" time="495.2163011858525" />
         <tli id="T575" time="495.948" type="appl" />
         <tli id="T576" time="496.547" type="appl" />
         <tli id="T577" time="497.145" type="appl" />
         <tli id="T578" time="497.743" type="appl" />
         <tli id="T579" time="498.341" type="appl" />
         <tli id="T580" time="498.94" type="appl" />
         <tli id="T581" time="499.538" type="appl" />
         <tli id="T582" time="500.136" type="appl" />
         <tli id="T583" time="500.734" type="appl" />
         <tli id="T584" time="501.333" type="appl" />
         <tli id="T585" time="501.931" type="appl" />
         <tli id="T586" time="502.627" type="appl" />
         <tli id="T587" time="503.314" type="appl" />
         <tli id="T588" time="504.001" type="appl" />
         <tli id="T589" time="504.688" type="appl" />
         <tli id="T590" time="505.375" type="appl" />
         <tli id="T591" time="506.062" type="appl" />
         <tli id="T592" time="506.749" type="appl" />
         <tli id="T593" time="507.436" type="appl" />
         <tli id="T594" time="508.123" type="appl" />
         <tli id="T595" time="508.81" type="appl" />
         <tli id="T596" time="509.593" type="appl" />
         <tli id="T597" time="510.135" type="appl" />
         <tli id="T598" time="510.678" type="appl" />
         <tli id="T599" time="511.22" type="appl" />
         <tli id="T600" time="511.762" type="appl" />
         <tli id="T601" time="512.305" type="appl" />
         <tli id="T602" time="512.848" type="appl" />
         <tli id="T603" time="514.1628263392549" />
         <tli id="T604" time="514.958" type="appl" />
         <tli id="T605" time="515.616" type="appl" />
         <tli id="T606" time="516.274" type="appl" />
         <tli id="T607" time="516.932" type="appl" />
         <tli id="T608" time="517.591" type="appl" />
         <tli id="T609" time="518.249" type="appl" />
         <tli id="T610" time="518.907" type="appl" />
         <tli id="T611" time="519.565" type="appl" />
         <tli id="T612" time="520.309" type="appl" />
         <tli id="T613" time="521.029" type="appl" />
         <tli id="T614" time="521.748" type="appl" />
         <tli id="T615" time="522.468" type="appl" />
         <tli id="T616" time="523.187" type="appl" />
         <tli id="T617" time="523.907" type="appl" />
         <tli id="T618" time="524.626" type="appl" />
         <tli id="T619" time="525.346" type="appl" />
         <tli id="T620" time="526.065" type="appl" />
         <tli id="T621" time="526.785" type="appl" />
         <tli id="T622" time="528.9093828625928" />
         <tli id="T623" time="529.725" type="appl" />
         <tli id="T624" time="530.34" type="appl" />
         <tli id="T625" time="530.955" type="appl" />
         <tli id="T626" time="531.57" type="appl" />
         <tli id="T627" time="532.185" type="appl" />
         <tli id="T628" time="532.8" type="appl" />
         <tli id="T629" time="533.415" type="appl" />
         <tli id="T630" time="534.03" type="appl" />
         <tli id="T631" time="534.645" type="appl" />
         <tli id="T632" time="535.26" type="appl" />
         <tli id="T633" time="535.93" type="appl" />
         <tli id="T634" time="536.6" type="appl" />
         <tli id="T635" time="537.27" type="appl" />
         <tli id="T636" time="537.94" type="appl" />
         <tli id="T637" time="538.611" type="appl" />
         <tli id="T638" time="539.281" type="appl" />
         <tli id="T639" time="539.951" type="appl" />
         <tli id="T640" time="540.621" type="appl" />
         <tli id="T641" time="541.291" type="appl" />
         <tli id="T642" time="542.03" type="appl" />
         <tli id="T643" time="542.76" type="appl" />
         <tli id="T644" time="543.49" type="appl" />
         <tli id="T645" time="544.22" type="appl" />
         <tli id="T646" time="544.95" type="appl" />
         <tli id="T647" time="545.68" type="appl" />
         <tli id="T648" time="551.002551180107" />
         <tli id="T649" time="551.486" type="appl" />
         <tli id="T650" time="551.971" type="appl" />
         <tli id="T651" time="552.457" type="appl" />
         <tli id="T652" time="552.943" type="appl" />
         <tli id="T653" time="553.429" type="appl" />
         <tli id="T654" time="553.914" type="appl" />
         <tli id="T655" time="554.4" type="appl" />
         <tli id="T656" time="555.092" type="appl" />
         <tli id="T657" time="555.784" type="appl" />
         <tli id="T658" time="556.477" type="appl" />
         <tli id="T659" time="557.169" type="appl" />
         <tli id="T660" time="557.861" type="appl" />
         <tli id="T661" time="558.553" type="appl" />
         <tli id="T662" time="559.246" type="appl" />
         <tli id="T663" time="559.938" type="appl" />
         <tli id="T664" time="560.9958098729115" />
         <tli id="T665" time="561.443" type="appl" />
         <tli id="T666" time="561.956" type="appl" />
         <tli id="T667" time="562.469" type="appl" />
         <tli id="T668" time="562.981" type="appl" />
         <tli id="T669" time="563.494" type="appl" />
         <tli id="T670" time="564.007" type="appl" />
         <tli id="T671" time="564.52" type="appl" />
         <tli id="T672" time="567.8157589341117" />
         <tli id="T673" time="568.75" type="appl" />
         <tli id="T674" time="569.51" type="appl" />
         <tli id="T675" time="570.27" type="appl" />
         <tli id="T676" time="571.5466685676191" />
         <tli id="T677" time="571.941" type="appl" />
         <tli id="T678" time="572.432" type="appl" />
         <tli id="T679" time="572.923" type="appl" />
         <tli id="T680" time="573.414" type="appl" />
         <tli id="T681" time="574.7490404821548" />
         <tli id="T682" time="575.89" type="appl" />
         <tli id="T683" time="576.7" type="appl" />
         <tli id="T684" time="577.51" type="appl" />
         <tli id="T685" time="578.32" type="appl" />
         <tli id="T686" time="579.2556734883829" />
         <tli id="T687" time="580.195" type="appl" />
         <tli id="T688" time="581.18" type="appl" />
         <tli id="T689" time="583.4823085859398" />
         <tli id="T690" time="584.312" type="appl" />
         <tli id="T691" time="585.094" type="appl" />
         <tli id="T692" time="585.876" type="appl" />
         <tli id="T693" time="586.658" type="appl" />
         <tli id="T694" time="587.44" type="appl" />
         <tli id="T695" time="588.072" type="appl" />
         <tli id="T696" time="588.705" type="appl" />
         <tli id="T697" time="589.337" type="appl" />
         <tli id="T698" time="589.969" type="appl" />
         <tli id="T699" time="590.602" type="appl" />
         <tli id="T700" time="591.234" type="appl" />
         <tli id="T701" time="591.866" type="appl" />
         <tli id="T702" time="592.499" type="appl" />
         <tli id="T703" time="593.2555689219313" />
         <tli id="T704" time="593.638" type="appl" />
         <tli id="T705" time="594.045" type="appl" />
         <tli id="T706" time="594.452" type="appl" />
         <tli id="T707" time="594.906650339859" />
         <tli id="T708" time="595.762" type="appl" />
         <tli id="T709" time="596.665" type="appl" />
         <tli id="T710" time="597.568" type="appl" />
         <tli id="T711" time="598.47" type="appl" />
         <tli id="T712" time="599.373" type="appl" />
         <tli id="T713" time="600.275" type="appl" />
         <tli id="T714" time="601.178" type="appl" />
         <tli id="T715" time="602.08" type="appl" />
         <tli id="T716" time="602.913" type="appl" />
         <tli id="T717" time="603.637" type="appl" />
         <tli id="T718" time="604.608817456852" />
         <tli id="T719" time="605.158" type="appl" />
         <tli id="T720" time="605.827" type="appl" />
         <tli id="T721" time="606.495" type="appl" />
         <tli id="T722" time="607.163" type="appl" />
         <tli id="T723" time="607.832" type="appl" />
         <tli id="T724" time="608.5" type="appl" />
         <tli id="T725" time="609.303" type="appl" />
         <tli id="T726" time="610.007" type="appl" />
         <tli id="T727" time="610.71" type="appl" />
         <tli id="T728" time="611.443" type="appl" />
         <tli id="T729" time="612.117" type="appl" />
         <tli id="T730" time="612.79" type="appl" />
         <tli id="T731" time="614.08" type="appl" />
         <tli id="T732" time="615.3554038563188" />
         <tli id="T733" time="616.245" type="appl" />
         <tli id="T734" time="616.845" type="appl" />
         <tli id="T735" time="617.445" type="appl" />
         <tli id="T736" time="618.6220461241468" />
         <tli id="T737" time="618.86" type="appl" />
         <tli id="T738" time="619.58" type="appl" />
         <tli id="T739" time="620.3" type="appl" />
         <tli id="T740" time="621.02" type="appl" />
         <tli id="T741" time="621.74" type="appl" />
         <tli id="T742" time="622.46" type="appl" />
         <tli id="T743" time="623.18" type="appl" />
         <tli id="T744" time="623.9" type="appl" />
         <tli id="T745" time="625.425" type="appl" />
         <tli id="T746" time="626.272" type="appl" />
         <tli id="T747" time="627.064" type="appl" />
         <tli id="T748" time="627.856" type="appl" />
         <tli id="T749" time="628.648" type="appl" />
         <tli id="T750" time="629.44" type="appl" />
         <tli id="T751" time="630.217" type="appl" />
         <tli id="T752" time="630.994" type="appl" />
         <tli id="T753" time="631.772" type="appl" />
         <tli id="T754" time="632.549" type="appl" />
         <tli id="T755" time="633.8819321467148" />
         <tli id="T756" time="634.388" type="appl" />
         <tli id="T757" time="635.098" type="appl" />
         <tli id="T758" time="635.808" type="appl" />
         <tli id="T759" time="636.518" type="appl" />
         <tli id="T760" time="637.5485714269299" />
         <tli id="T761" time="638.714" type="appl" />
         <tli id="T762" time="639.51" type="appl" />
         <tli id="T763" time="640.051" type="appl" />
         <tli id="T764" time="640.563" type="appl" />
         <tli id="T765" time="641.076" type="appl" />
         <tli id="T766" time="641.588" type="appl" />
         <tli id="T767" time="642.1" />
         <tli id="T768" time="643.084" type="appl" />
         <tli id="T769" time="644.043" type="appl" />
         <tli id="T770" time="645.003" type="appl" />
         <tli id="T771" time="645.962" type="appl" />
         <tli id="T772" time="646.922" type="appl" />
         <tli id="T773" time="647.881" type="appl" />
         <tli id="T774" time="648.841" type="appl" />
         <tli id="T775" time="649.8" />
         <tli id="T777" time="650.8418054719278" />
         <tli id="T778" time="652.204" type="appl" />
         <tli id="T779" time="653.297" type="appl" />
         <tli id="T780" time="654.623" type="appl" />
         <tli id="T781" time="655.728" type="appl" />
         <tli id="T782" time="656.534" type="appl" />
         <tli id="T783" time="657.331" type="appl" />
         <tli id="T784" time="658.127" type="appl" />
         <tli id="T785" time="658.924" type="appl" />
         <tli id="T786" time="659.72" type="appl" />
         <tli id="T787" time="660.754" type="appl" />
         <tli id="T788" time="661.644" type="appl" />
         <tli id="T789" time="662.534" type="appl" />
         <tli id="T790" time="664.0150404132098" />
         <tli id="T791" time="664.769" type="appl" />
         <tli id="T792" time="665.449" type="appl" />
         <tli id="T793" time="666.129" type="appl" />
         <tli id="T794" time="666.809" type="appl" />
         <tli id="T795" time="667.489" type="appl" />
         <tli id="T796" time="668.414" type="appl" />
         <tli id="T797" time="669.154" type="appl" />
         <tli id="T798" time="669.596" type="appl" />
         <tli id="T799" time="670.022" type="appl" />
         <tli id="T800" time="670.448" type="appl" />
         <tli id="T801" time="670.874" type="appl" />
         <tli id="T802" time="671.3" type="appl" />
         <tli id="T803" time="676.3349483947326" />
         <tli id="T804" time="676.903" type="appl" />
         <tli id="T805" time="677.497" type="appl" />
         <tli id="T806" time="678.09" type="appl" />
         <tli id="T807" time="678.684" type="appl" />
         <tli id="T808" time="679.277" type="appl" />
         <tli id="T809" time="679.871" type="appl" />
         <tli id="T810" time="680.464" type="appl" />
         <tli id="T811" time="681.058" type="appl" />
         <tli id="T812" time="681.651" type="appl" />
         <tli id="T813" time="682.245" type="appl" />
         <tli id="T814" time="682.838" type="appl" />
         <tli id="T815" time="683.432" type="appl" />
         <tli id="T816" time="684.025" type="appl" />
         <tli id="T817" time="684.799" type="appl" />
         <tli id="T818" time="685.508" type="appl" />
         <tli id="T819" time="686.218" type="appl" />
         <tli id="T820" time="686.927" type="appl" />
         <tli id="T821" time="687.636" type="appl" />
         <tli id="T822" time="688.5415238894124" />
         <tli id="T823" time="689.592" type="appl" />
         <tli id="T824" time="690.465" type="appl" />
         <tli id="T825" time="691.338" type="appl" />
         <tli id="T826" time="692.21" type="appl" />
         <tli id="T827" time="692.795" type="appl" />
         <tli id="T828" time="693.38" type="appl" />
         <tli id="T829" time="693.965" type="appl" />
         <tli id="T830" time="694.55" type="appl" />
         <tli id="T831" time="695.135" type="appl" />
         <tli id="T832" time="695.72" type="appl" />
         <tli id="T833" time="696.305" type="appl" />
         <tli id="T834" time="696.89" type="appl" />
         <tli id="T835" time="697.475" type="appl" />
         <tli id="T836" time="698.06" type="appl" />
         <tli id="T837" time="698.645" type="appl" />
         <tli id="T838" time="699.23" type="appl" />
         <tli id="T839" time="700.106" type="appl" />
         <tli id="T840" time="700.862" type="appl" />
         <tli id="T841" time="701.618" type="appl" />
         <tli id="T842" time="702.374" type="appl" />
         <tli id="T843" time="703.131" type="appl" />
         <tli id="T844" time="703.887" type="appl" />
         <tli id="T845" time="704.643" type="appl" />
         <tli id="T846" time="705.399" type="appl" />
         <tli id="T847" time="707.2947171534944" />
         <tli id="T848" time="708.061" type="appl" />
         <tli id="T849" time="708.612" type="appl" />
         <tli id="T850" time="709.164" type="appl" />
         <tli id="T851" time="709.715" type="appl" />
         <tli id="T852" time="710.266" type="appl" />
         <tli id="T853" time="710.817" type="appl" />
         <tli id="T854" time="711.378" type="appl" />
         <tli id="T855" time="711.938" type="appl" />
         <tli id="T856" time="712.6880102038472" />
         <tli id="T857" time="713.308" type="appl" />
         <tli id="T858" time="713.905" type="appl" />
         <tli id="T859" time="714.502" type="appl" />
         <tli id="T860" time="715.161325063774" />
         <tli id="T861" time="717.98" type="appl" />
         <tli id="T862" time="718.8" type="appl" />
         <tli id="T863" time="719.375" type="appl" />
         <tli id="T864" time="719.95" type="appl" />
         <tli id="T865" time="720.525" type="appl" />
         <tli id="T866" time="721.1" type="appl" />
         <tli id="T867" time="721.675" type="appl" />
         <tli id="T868" time="722.25" type="appl" />
         <tli id="T869" time="722.825" type="appl" />
         <tli id="T870" time="723.4" type="appl" />
         <tli id="T871" time="724.159" type="appl" />
         <tli id="T872" time="724.891" type="appl" />
         <tli id="T873" time="725.623" type="appl" />
         <tli id="T874" time="726.354" type="appl" />
         <tli id="T875" time="727.086" type="appl" />
         <tli id="T876" time="727.818" type="appl" />
         <tli id="T877" time="728.55" type="appl" />
         <tli id="T878" time="732.2078644083377" />
         <tli id="T879" time="733.4878548479765" />
         <tli id="T880" time="734.081183749684" />
         <tli id="T881" time="735.091" type="appl" />
         <tli id="T882" time="735.992" type="appl" />
         <tli id="T883" time="736.893" type="appl" />
         <tli id="T884" time="737.794" type="appl" />
         <tli id="T885" time="738.694" type="appl" />
         <tli id="T886" time="739.595" type="appl" />
         <tli id="T887" time="740.496" type="appl" />
         <tli id="T888" time="741.397" type="appl" />
         <tli id="T889" time="742.298" type="appl" />
         <tli id="T890" time="744.527" type="appl" />
         <tli id="T891" time="746.753" type="appl" />
         <tli id="T892" time="748.98" type="appl" />
         <tli id="T893" time="749.592" type="appl" />
         <tli id="T894" time="750.16" type="appl" />
         <tli id="T895" time="750.728" type="appl" />
         <tli id="T896" time="751.295" type="appl" />
         <tli id="T897" time="752.305" type="appl" />
         <tli id="T898" time="753.175" type="appl" />
         <tli id="T899" time="753.9010357134649" />
         <tli id="T900" time="754.683" type="appl" />
         <tli id="T901" time="755.417" type="appl" />
         <tli id="T902" time="756.15" type="appl" />
         <tli id="T903" time="756.883" type="appl" />
         <tli id="T1025" time="757.2217692307693" type="intp" />
         <tli id="T904" time="757.617" type="appl" />
         <tli id="T905" time="758.35" type="appl" />
         <tli id="T906" time="759.155" type="appl" />
         <tli id="T907" time="759.84" type="appl" />
         <tli id="T908" time="760.526" type="appl" />
         <tli id="T909" time="761.211" type="appl" />
         <tli id="T910" time="761.896" type="appl" />
         <tli id="T911" time="762.581" type="appl" />
         <tli id="T912" time="763.267" type="appl" />
         <tli id="T913" time="763.952" type="appl" />
         <tli id="T914" time="765.1009520603039" />
         <tli id="T915" time="765.83" type="appl" />
         <tli id="T916" time="766.489" type="appl" />
         <tli id="T917" time="767.149" type="appl" />
         <tli id="T918" time="767.809" type="appl" />
         <tli id="T919" time="768.967" type="appl" />
         <tli id="T920" time="770.104" type="appl" />
         <tli id="T921" time="771.242" type="appl" />
         <tli id="T922" time="772.379" type="appl" />
         <tli id="T923" time="774.3808827476846" />
         <tli id="T924" time="775.154" type="appl" />
         <tli id="T925" time="775.938" type="appl" />
         <tli id="T926" time="776.722" type="appl" />
         <tli id="T927" time="777.506" type="appl" />
         <tli id="T928" time="778.29" type="appl" />
         <tli id="T929" time="779.44" type="appl" />
         <tli id="T930" time="780.7475018615127" />
         <tli id="T931" time="781.372" type="appl" />
         <tli id="T932" time="782.058" type="appl" />
         <tli id="T933" time="782.745" type="appl" />
         <tli id="T934" time="783.432" type="appl" />
         <tli id="T935" time="784.118" type="appl" />
         <tli id="T936" time="784.805" type="appl" />
         <tli id="T937" time="785.758" type="appl" />
         <tli id="T938" time="786.655" type="appl" />
         <tli id="T939" time="787.552" type="appl" />
         <tli id="T940" time="788.45" type="appl" />
         <tli id="T941" time="789.86" type="appl" />
         <tli id="T942" time="791.18" type="appl" />
         <tli id="T1026" time="791.7457142857143" type="intp" />
         <tli id="T943" time="792.5" type="appl" />
         <tli id="T944" time="794.2807341139429" />
         <tli id="T945" time="797.1340461356375" />
         <tli id="T946" time="797.7273750373452" />
         <tli id="T947" time="798.58" type="appl" />
         <tli id="T948" time="799.26" type="appl" />
         <tli id="T949" time="799.94" type="appl" />
         <tli id="T950" time="800.766" type="appl" />
         <tli id="T951" time="801.532" type="appl" />
         <tli id="T952" time="802.298" type="appl" />
         <tli id="T953" time="803.064" type="appl" />
         <tli id="T954" time="803.83" type="appl" />
         <tli id="T955" time="804.595" type="appl" />
         <tli id="T956" time="805.361" type="appl" />
         <tli id="T957" time="806.127" type="appl" />
         <tli id="T958" time="806.893" type="appl" />
         <tli id="T959" time="807.659" type="appl" />
         <tli id="T960" time="808.425" type="appl" />
         <tli id="T961" time="809.337" type="appl" />
         <tli id="T962" time="810.093" type="appl" />
         <tli id="T963" time="811.5539384321927" />
         <tli id="T964" time="812.595" type="appl" />
         <tli id="T965" time="813.58" type="appl" />
         <tli id="T966" time="814.565" type="appl" />
         <tli id="T967" time="815.55" type="appl" />
         <tli id="T968" time="816.602" type="appl" />
         <tli id="T969" time="817.624" type="appl" />
         <tli id="T970" time="818.646" type="appl" />
         <tli id="T971" time="819.668" type="appl" />
         <tli id="T972" time="820.69" type="appl" />
         <tli id="T973" time="821.512" type="appl" />
         <tli id="T974" time="822.243" type="appl" />
         <tli id="T975" time="822.975" type="appl" />
         <tli id="T976" time="823.707" type="appl" />
         <tli id="T977" time="824.439" type="appl" />
         <tli id="T978" time="825.17" type="appl" />
         <tli id="T979" time="825.902" type="appl" />
         <tli id="T980" time="826.456" type="appl" />
         <tli id="T981" time="826.951" type="appl" />
         <tli id="T982" time="827.447" type="appl" />
         <tli id="T983" time="827.943" type="appl" />
         <tli id="T984" time="828.439" type="appl" />
         <tli id="T985" time="828.934" type="appl" />
         <tli id="T986" time="829.43" type="appl" />
         <tli id="T987" time="830.605" type="appl" />
         <tli id="T988" time="831.78" type="appl" />
         <tli id="T989" time="832.955" type="appl" />
         <tli id="T990" time="834.13" type="appl" />
         <tli id="T991" time="835.305" type="appl" />
         <tli id="T992" time="836.48" type="appl" />
         <tli id="T993" time="841.9" type="appl" />
         <tli id="T994" time="843.195" type="appl" />
         <tli id="T995" time="844.375" type="appl" />
         <tli id="T996" time="845.555" type="appl" />
         <tli id="T997" time="847.0670065152942" />
         <tli id="T998" time="848.655" type="appl" />
         <tli id="T999" time="852.2136347413417" />
         <tli id="T1000" time="853.573" type="appl" />
         <tli id="T1001" time="854.877" type="appl" />
         <tli id="T1002" time="856.18" type="appl" />
         <tli id="T1003" time="858.933584549445" />
         <tli id="T1004" time="860.08" type="appl" />
         <tli id="T1005" time="861.05" type="appl" />
         <tli id="T1006" time="862.02" type="appl" />
         <tli id="T1007" time="862.99" type="appl" />
         <tli id="T1008" time="863.96" type="appl" />
         <tli id="T1009" time="864.93" type="appl" />
         <tli id="T1010" time="865.9" type="appl" />
         <tli id="T1011" time="866.742" type="appl" />
         <tli id="T1012" time="867.584" type="appl" />
         <tli id="T1013" time="868.426" type="appl" />
         <tli id="T1014" time="869.268" type="appl" />
         <tli id="T1015" time="870.11" type="appl" />
         <tli id="T1016" time="870.253" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-PKZ"
                      id="tx-PKZ"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T664" start="T662">
            <tli id="T662.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T954" start="T953">
            <tli id="T953.tx-PKZ.1" />
         </timeline-fork>
         <timeline-fork end="T1006" start="T1005">
            <tli id="T1005.tx-PKZ.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-PKZ">
            <ts e="T18" id="Seg_0" n="sc" s="T0">
               <ts e="T2" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Măn</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">amnobiam</ts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T6" id="Seg_11" n="HIAT:u" s="T2">
                  <ts e="T3" id="Seg_13" n="HIAT:w" s="T2">Sumna</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">tibizeŋ</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">ibiʔi</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">măn</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_26" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">Dĭgəttə</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">muktuʔ</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">ibiem</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_38" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_40" n="HIAT:w" s="T9">Tĭ</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">muktuʔ</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">tibi</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">ej</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">amnobi</ts>
                  <nts id="Seg_53" n="HIAT:ip">,</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">barəʔpi</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">da</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_62" n="HIAT:w" s="T16">kalla</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_65" n="HIAT:w" s="T776">dʼürbi</ts>
                  <nts id="Seg_66" n="HIAT:ip">.</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_69" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_71" n="HIAT:w" s="T17">Dĭgəttə</ts>
                  <nts id="Seg_72" n="HIAT:ip">…</nts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T1015" id="Seg_74" n="sc" s="T19">
               <ts e="T26" id="Seg_76" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_78" n="HIAT:w" s="T19">Первый</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_81" n="HIAT:w" s="T20">tibim</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_84" n="HIAT:w" s="T21">bügən</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_87" n="HIAT:w" s="T22">külambi</ts>
                  <nts id="Seg_88" n="HIAT:ip">,</nts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_91" n="HIAT:w" s="T23">nʼim</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_94" n="HIAT:w" s="T24">bügən</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_97" n="HIAT:w" s="T25">külambi</ts>
                  <nts id="Seg_98" n="HIAT:ip">.</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_101" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_103" n="HIAT:w" s="T26">Dĭgəttə</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_106" n="HIAT:w" s="T27">iam</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_109" n="HIAT:w" s="T28">kuʔpiʔi</ts>
                  <nts id="Seg_110" n="HIAT:ip">,</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_113" n="HIAT:w" s="T29">sʼestram</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_116" n="HIAT:w" s="T30">kuʔpiʔi</ts>
                  <nts id="Seg_117" n="HIAT:ip">.</nts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_120" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_122" n="HIAT:w" s="T31">Abam</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_125" n="HIAT:w" s="T32">dʼijenə</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_128" n="HIAT:w" s="T33">kambi</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_131" n="HIAT:w" s="T34">i</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_134" n="HIAT:w" s="T35">dĭn</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_137" n="HIAT:w" s="T36">külambi</ts>
                  <nts id="Seg_138" n="HIAT:ip">.</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_141" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_143" n="HIAT:w" s="T37">Măn</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_146" n="HIAT:w" s="T38">sʼestram</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_149" n="HIAT:w" s="T39">tibit</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_152" n="HIAT:w" s="T40">kuʔpi</ts>
                  <nts id="Seg_153" n="HIAT:ip">.</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_156" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_158" n="HIAT:w" s="T41">Šide</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_160" n="HIAT:ip">(</nts>
                  <ts e="T43" id="Seg_162" n="HIAT:w" s="T42">ni-</ts>
                  <nts id="Seg_163" n="HIAT:ip">)</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_166" n="HIAT:w" s="T43">nʼi</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_169" n="HIAT:w" s="T44">maːbi</ts>
                  <nts id="Seg_170" n="HIAT:ip">.</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_173" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_175" n="HIAT:w" s="T45">Măn</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_178" n="HIAT:w" s="T46">dĭzem</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_181" n="HIAT:w" s="T47">bar</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_184" n="HIAT:w" s="T48">özerbiem</ts>
                  <nts id="Seg_185" n="HIAT:ip">.</nts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_188" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_190" n="HIAT:w" s="T49">Oldʼa</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_193" n="HIAT:w" s="T50">ibiem</ts>
                  <nts id="Seg_194" n="HIAT:ip">.</nts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_197" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_199" n="HIAT:w" s="T51">I</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_202" n="HIAT:w" s="T52">tüšəlbiem</ts>
                  <nts id="Seg_203" n="HIAT:ip">.</nts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_206" n="HIAT:u" s="T53">
                  <nts id="Seg_207" n="HIAT:ip">(</nts>
                  <ts e="T54" id="Seg_209" n="HIAT:w" s="T53">Teŋziszɨ</ts>
                  <nts id="Seg_210" n="HIAT:ip">)</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_213" n="HIAT:w" s="T54">sazən</ts>
                  <nts id="Seg_214" n="HIAT:ip">.</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_217" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_219" n="HIAT:w" s="T55">Onʼiʔ</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_222" n="HIAT:w" s="T56">был</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_225" n="HIAT:w" s="T57">булгахтер</ts>
                  <nts id="Seg_226" n="HIAT:ip">,</nts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_228" n="HIAT:ip">(</nts>
                  <ts e="T59" id="Seg_230" n="HIAT:w" s="T58">dĭgət-</ts>
                  <nts id="Seg_231" n="HIAT:ip">)</nts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_234" n="HIAT:w" s="T59">onʼiʔ</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_237" n="HIAT:w" s="T60">măna</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_240" n="HIAT:w" s="T61">amnobi</ts>
                  <nts id="Seg_241" n="HIAT:ip">.</nts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_244" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_246" n="HIAT:w" s="T62">Dĭgəttə</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_249" n="HIAT:w" s="T63">dĭm</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_252" n="HIAT:w" s="T64">ibiʔi</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_255" n="HIAT:w" s="T65">văjnanə</ts>
                  <nts id="Seg_256" n="HIAT:ip">,</nts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_259" n="HIAT:w" s="T66">dĭn</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_262" n="HIAT:w" s="T67">kuʔpiʔi</ts>
                  <nts id="Seg_263" n="HIAT:ip">.</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_266" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_268" n="HIAT:w" s="T68">Dĭn</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_271" n="HIAT:w" s="T69">koʔbdo</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_274" n="HIAT:w" s="T70">maluʔpi</ts>
                  <nts id="Seg_275" n="HIAT:ip">.</nts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_278" n="HIAT:u" s="T71">
                  <nts id="Seg_279" n="HIAT:ip">(</nts>
                  <nts id="Seg_280" n="HIAT:ip">(</nts>
                  <ats e="T72" id="Seg_281" n="HIAT:non-pho" s="T71">BRK</ats>
                  <nts id="Seg_282" n="HIAT:ip">)</nts>
                  <nts id="Seg_283" n="HIAT:ip">)</nts>
                  <nts id="Seg_284" n="HIAT:ip">.</nts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_287" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_289" n="HIAT:w" s="T72">Măn</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_291" n="HIAT:ip">(</nts>
                  <ts e="T74" id="Seg_293" n="HIAT:w" s="T73">pʼešk-</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_296" n="HIAT:w" s="T74">pʼeštə</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_299" n="HIAT:w" s="T75">s-</ts>
                  <nts id="Seg_300" n="HIAT:ip">)</nts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_303" n="HIAT:w" s="T76">kănnambiam</ts>
                  <nts id="Seg_304" n="HIAT:ip">,</nts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_307" n="HIAT:w" s="T77">pʼeštə</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_309" n="HIAT:ip">(</nts>
                  <ts e="T79" id="Seg_311" n="HIAT:w" s="T78">s-</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_314" n="HIAT:w" s="T79">š-</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_317" n="HIAT:w" s="T80">sa-</ts>
                  <nts id="Seg_318" n="HIAT:ip">)</nts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_321" n="HIAT:w" s="T81">sʼabiam</ts>
                  <nts id="Seg_322" n="HIAT:ip">.</nts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_325" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_327" n="HIAT:w" s="T82">Amnobiam</ts>
                  <nts id="Seg_328" n="HIAT:ip">,</nts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_331" n="HIAT:w" s="T83">iʔbobiam</ts>
                  <nts id="Seg_332" n="HIAT:ip">,</nts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_335" n="HIAT:w" s="T84">kunolbiam</ts>
                  <nts id="Seg_336" n="HIAT:ip">.</nts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_339" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_341" n="HIAT:w" s="T85">Sanə</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_343" n="HIAT:ip">(</nts>
                  <ts e="T87" id="Seg_345" n="HIAT:w" s="T86">lei-</ts>
                  <nts id="Seg_346" n="HIAT:ip">)</nts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_349" n="HIAT:w" s="T87">lejlem</ts>
                  <nts id="Seg_350" n="HIAT:ip">.</nts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_353" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_355" n="HIAT:w" s="T88">Dĭgəttə</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_358" n="HIAT:w" s="T89">šiʔ</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_361" n="HIAT:w" s="T90">šobiʔi</ts>
                  <nts id="Seg_362" n="HIAT:ip">,</nts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_365" n="HIAT:w" s="T91">măn</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_368" n="HIAT:w" s="T92">šobiam</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_371" n="HIAT:w" s="T93">šiʔnʼileʔ</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_374" n="HIAT:w" s="T94">i</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_377" n="HIAT:w" s="T95">bar</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_380" n="HIAT:w" s="T96">dʼăbaktərlaʔbəm</ts>
                  <nts id="Seg_381" n="HIAT:ip">.</nts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_384" n="HIAT:u" s="T97">
                  <nts id="Seg_385" n="HIAT:ip">(</nts>
                  <nts id="Seg_386" n="HIAT:ip">(</nts>
                  <ats e="T98" id="Seg_387" n="HIAT:non-pho" s="T97">BRK</ats>
                  <nts id="Seg_388" n="HIAT:ip">)</nts>
                  <nts id="Seg_389" n="HIAT:ip">)</nts>
                  <nts id="Seg_390" n="HIAT:ip">.</nts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_393" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_395" n="HIAT:w" s="T98">Nagur</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_398" n="HIAT:w" s="T99">kaga</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_401" n="HIAT:w" s="T100">amnobiʔi</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_404" n="HIAT:w" s="T101">i</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_407" n="HIAT:w" s="T102">dĭgəttə</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_410" n="HIAT:w" s="T103">kambiʔi</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_413" n="HIAT:w" s="T104">pa</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_416" n="HIAT:w" s="T105">jaʔsʼittə</ts>
                  <nts id="Seg_417" n="HIAT:ip">.</nts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_420" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_422" n="HIAT:w" s="T106">Dĭzeŋgən</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_425" n="HIAT:w" s="T107">šü</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_428" n="HIAT:w" s="T108">nagobi</ts>
                  <nts id="Seg_429" n="HIAT:ip">.</nts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_432" n="HIAT:u" s="T109">
                  <nts id="Seg_433" n="HIAT:ip">(</nts>
                  <ts e="T110" id="Seg_435" n="HIAT:w" s="T109">Tirdə</ts>
                  <nts id="Seg_436" n="HIAT:ip">)</nts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_439" n="HIAT:w" s="T110">kubiʔi</ts>
                  <nts id="Seg_440" n="HIAT:ip">,</nts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_443" n="HIAT:w" s="T111">šü</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_446" n="HIAT:w" s="T112">nendleʔbə</ts>
                  <nts id="Seg_447" n="HIAT:ip">.</nts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_450" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_452" n="HIAT:w" s="T113">Kanaʔ</ts>
                  <nts id="Seg_453" n="HIAT:ip">!</nts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_456" n="HIAT:u" s="T114">
                  <ts e="T115" id="Seg_458" n="HIAT:w" s="T114">Iʔ</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_461" n="HIAT:w" s="T115">šü</ts>
                  <nts id="Seg_462" n="HIAT:ip">!</nts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T121" id="Seg_465" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_467" n="HIAT:w" s="T116">Dĭ</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_470" n="HIAT:w" s="T117">kambi</ts>
                  <nts id="Seg_471" n="HIAT:ip">,</nts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_474" n="HIAT:w" s="T118">dĭn</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_477" n="HIAT:w" s="T119">büzʼe</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_480" n="HIAT:w" s="T120">amnolaʔbə</ts>
                  <nts id="Seg_481" n="HIAT:ip">.</nts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_484" n="HIAT:u" s="T121">
                  <nts id="Seg_485" n="HIAT:ip">(</nts>
                  <ts e="T122" id="Seg_487" n="HIAT:w" s="T121">Nö-</ts>
                  <nts id="Seg_488" n="HIAT:ip">)</nts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_491" n="HIAT:w" s="T122">Nörbaʔ</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_494" n="HIAT:w" s="T123">сказка</ts>
                  <nts id="Seg_495" n="HIAT:ip">,</nts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_498" n="HIAT:w" s="T124">dĭgəttə</ts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_501" n="HIAT:w" s="T125">šü</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_504" n="HIAT:w" s="T126">mĭlem</ts>
                  <nts id="Seg_505" n="HIAT:ip">.</nts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_508" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_510" n="HIAT:w" s="T127">A</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_513" n="HIAT:w" s="T128">dĭ</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_516" n="HIAT:w" s="T129">măndə:</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_519" n="HIAT:w" s="T130">Măn</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_522" n="HIAT:w" s="T131">ej</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_525" n="HIAT:w" s="T132">tĭmnem</ts>
                  <nts id="Seg_526" n="HIAT:ip">.</nts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_529" n="HIAT:u" s="T133">
                  <ts e="T134" id="Seg_531" n="HIAT:w" s="T133">Dĭ</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_534" n="HIAT:w" s="T134">bögəldə</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_536" n="HIAT:ip">(</nts>
                  <ts e="T136" id="Seg_538" n="HIAT:w" s="T135">i-</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_541" n="HIAT:w" s="T136">ba-</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_544" n="HIAT:w" s="T137">bădarbi=</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_547" n="HIAT:w" s="T138">bădaʔ-</ts>
                  <nts id="Seg_548" n="HIAT:ip">)</nts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_551" n="HIAT:w" s="T139">băʔpi</ts>
                  <nts id="Seg_552" n="HIAT:ip">,</nts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_554" n="HIAT:ip">(</nts>
                  <ts e="T141" id="Seg_556" n="HIAT:w" s="T140">remniʔ</ts>
                  <nts id="Seg_557" n="HIAT:ip">)</nts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_560" n="HIAT:w" s="T141">nagur</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_563" n="HIAT:w" s="T142">ремень</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_566" n="HIAT:w" s="T143">băʔpi</ts>
                  <nts id="Seg_567" n="HIAT:ip">.</nts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T151" id="Seg_570" n="HIAT:u" s="T144">
                  <ts e="T145" id="Seg_572" n="HIAT:w" s="T144">Dĭ</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_574" n="HIAT:ip">(</nts>
                  <ts e="T146" id="Seg_576" n="HIAT:w" s="T145">kalal-</ts>
                  <nts id="Seg_577" n="HIAT:ip">)</nts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1017" id="Seg_580" n="HIAT:w" s="T146">kalla</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_583" n="HIAT:w" s="T1017">dʼürbi</ts>
                  <nts id="Seg_584" n="HIAT:ip">,</nts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_587" n="HIAT:w" s="T147">i</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_590" n="HIAT:w" s="T148">šü</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_593" n="HIAT:w" s="T149">ej</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_596" n="HIAT:w" s="T150">mĭbi</ts>
                  <nts id="Seg_597" n="HIAT:ip">.</nts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_600" n="HIAT:u" s="T151">
                  <ts e="T153" id="Seg_602" n="HIAT:w" s="T151">Dĭgəttə</ts>
                  <nts id="Seg_603" n="HIAT:ip">…</nts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_606" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_608" n="HIAT:w" s="T153">Dĭgəttə</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_611" n="HIAT:w" s="T154">onʼiʔ</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_614" n="HIAT:w" s="T155">bazoʔ</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_617" n="HIAT:w" s="T156">kambi</ts>
                  <nts id="Seg_618" n="HIAT:ip">.</nts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T162" id="Seg_621" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_623" n="HIAT:w" s="T157">I</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_626" n="HIAT:w" s="T158">dĭʔnə</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_629" n="HIAT:w" s="T159">mămbi:</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_632" n="HIAT:w" s="T160">Nörbaʔ</ts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_635" n="HIAT:w" s="T161">сказка</ts>
                  <nts id="Seg_636" n="HIAT:ip">.</nts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_639" n="HIAT:u" s="T162">
                  <ts e="T163" id="Seg_641" n="HIAT:w" s="T162">Dĭ</ts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_644" n="HIAT:w" s="T163">măndə:</ts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_647" n="HIAT:w" s="T164">Măn</ts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_650" n="HIAT:w" s="T165">ej</ts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_653" n="HIAT:w" s="T166">tĭmnem</ts>
                  <nts id="Seg_654" n="HIAT:ip">.</nts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T173" id="Seg_657" n="HIAT:u" s="T167">
                  <ts e="T168" id="Seg_659" n="HIAT:w" s="T167">No</ts>
                  <nts id="Seg_660" n="HIAT:ip">,</nts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_663" n="HIAT:w" s="T168">dĭgəttə</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_666" n="HIAT:w" s="T169">dĭn</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_669" n="HIAT:w" s="T170">bar</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_672" n="HIAT:w" s="T171">dʼagarbi</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_675" n="HIAT:w" s="T172">bögəldə</ts>
                  <nts id="Seg_676" n="HIAT:ip">.</nts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T178" id="Seg_679" n="HIAT:u" s="T173">
                  <ts e="T174" id="Seg_681" n="HIAT:w" s="T173">I</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_684" n="HIAT:w" s="T174">kambi</ts>
                  <nts id="Seg_685" n="HIAT:ip">,</nts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_688" n="HIAT:w" s="T175">ej</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_691" n="HIAT:w" s="T176">mĭbi</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_694" n="HIAT:w" s="T177">šü</ts>
                  <nts id="Seg_695" n="HIAT:ip">.</nts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T182" id="Seg_698" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_700" n="HIAT:w" s="T178">Dĭgəttə</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_703" n="HIAT:w" s="T179">onʼiʔ</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_706" n="HIAT:w" s="T180">bazoʔ</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_709" n="HIAT:w" s="T181">šobi</ts>
                  <nts id="Seg_710" n="HIAT:ip">.</nts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_713" n="HIAT:u" s="T182">
                  <ts e="T183" id="Seg_715" n="HIAT:w" s="T182">Nörbaʔ</ts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_718" n="HIAT:w" s="T183">cказка</ts>
                  <nts id="Seg_719" n="HIAT:ip">!</nts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_722" n="HIAT:u" s="T184">
                  <ts e="T185" id="Seg_724" n="HIAT:w" s="T184">No</ts>
                  <nts id="Seg_725" n="HIAT:ip">,</nts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_728" n="HIAT:w" s="T185">dĭ</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_731" n="HIAT:w" s="T186">măndə:</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_734" n="HIAT:w" s="T187">Nörbəlam</ts>
                  <nts id="Seg_735" n="HIAT:ip">.</nts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T191" id="Seg_738" n="HIAT:u" s="T188">
                  <ts e="T189" id="Seg_740" n="HIAT:w" s="T188">Bʼeʔ</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_743" n="HIAT:w" s="T189">ibiʔi</ts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_746" n="HIAT:w" s="T190">kagaʔi</ts>
                  <nts id="Seg_747" n="HIAT:ip">.</nts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T195" id="Seg_750" n="HIAT:u" s="T191">
                  <ts e="T192" id="Seg_752" n="HIAT:w" s="T191">Dĭgəttə</ts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_755" n="HIAT:w" s="T192">kambiʔi</ts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_758" n="HIAT:w" s="T193">pa</ts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_761" n="HIAT:w" s="T194">jaʔsittə</ts>
                  <nts id="Seg_762" n="HIAT:ip">.</nts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T202" id="Seg_765" n="HIAT:u" s="T195">
                  <ts e="T196" id="Seg_767" n="HIAT:w" s="T195">Dĭgəttə</ts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_769" n="HIAT:ip">(</nts>
                  <ts e="T197" id="Seg_771" n="HIAT:w" s="T196">šojmu</ts>
                  <nts id="Seg_772" n="HIAT:ip">)</nts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_775" n="HIAT:w" s="T197">suʔmiluʔpi</ts>
                  <nts id="Seg_776" n="HIAT:ip">,</nts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_779" n="HIAT:w" s="T198">dĭ</ts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_782" n="HIAT:w" s="T199">bar</ts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_785" n="HIAT:w" s="T200">dʼünə</ts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1018" id="Seg_788" n="HIAT:w" s="T201">kalla</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_791" n="HIAT:w" s="T1018">dʼürbi</ts>
                  <nts id="Seg_792" n="HIAT:ip">.</nts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T207" id="Seg_795" n="HIAT:u" s="T202">
                  <ts e="T203" id="Seg_797" n="HIAT:w" s="T202">Dĭgəttə</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_800" n="HIAT:w" s="T203">nav</ts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_803" n="HIAT:w" s="T204">šobi</ts>
                  <nts id="Seg_804" n="HIAT:ip">,</nts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_807" n="HIAT:w" s="T205">maʔ</ts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_810" n="HIAT:w" s="T206">abi</ts>
                  <nts id="Seg_811" n="HIAT:ip">.</nts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T209" id="Seg_814" n="HIAT:u" s="T207">
                  <ts e="T208" id="Seg_816" n="HIAT:w" s="T207">Munəʔi</ts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_819" n="HIAT:w" s="T208">nʼebi</ts>
                  <nts id="Seg_820" n="HIAT:ip">.</nts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T218" id="Seg_823" n="HIAT:u" s="T209">
                  <ts e="T210" id="Seg_825" n="HIAT:w" s="T209">Dĭgəttə</ts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_828" n="HIAT:w" s="T210">šobi</ts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_831" n="HIAT:w" s="T211">dʼijegəʔ</ts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_834" n="HIAT:w" s="T212">men</ts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_837" n="HIAT:w" s="T213">urgo</ts>
                  <nts id="Seg_838" n="HIAT:ip">,</nts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_841" n="HIAT:w" s="T214">i</ts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_844" n="HIAT:w" s="T215">davaj</ts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_847" n="HIAT:w" s="T216">munəʔi</ts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_850" n="HIAT:w" s="T217">amzittə</ts>
                  <nts id="Seg_851" n="HIAT:ip">.</nts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T222" id="Seg_854" n="HIAT:u" s="T218">
                  <ts e="T219" id="Seg_856" n="HIAT:w" s="T218">Dĭ</ts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_858" n="HIAT:ip">(</nts>
                  <ts e="T220" id="Seg_860" n="HIAT:w" s="T219">dʼabə-</ts>
                  <nts id="Seg_861" n="HIAT:ip">)</nts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_864" n="HIAT:w" s="T220">dʼabəluʔpi</ts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_867" n="HIAT:w" s="T221">xvostə</ts>
                  <nts id="Seg_868" n="HIAT:ip">.</nts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T226" id="Seg_871" n="HIAT:u" s="T222">
                  <ts e="T223" id="Seg_873" n="HIAT:w" s="T222">Dĭ</ts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_876" n="HIAT:w" s="T223">bar</ts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_879" n="HIAT:w" s="T224">dĭm</ts>
                  <nts id="Seg_880" n="HIAT:ip">…</nts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T228" id="Seg_883" n="HIAT:u" s="T226">
                  <ts e="T227" id="Seg_885" n="HIAT:w" s="T226">запретил</ts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_887" n="HIAT:ip">(</nts>
                  <nts id="Seg_888" n="HIAT:ip">(</nts>
                  <ats e="T228" id="Seg_889" n="HIAT:non-pho" s="T227">BRK</ats>
                  <nts id="Seg_890" n="HIAT:ip">)</nts>
                  <nts id="Seg_891" n="HIAT:ip">)</nts>
                  <nts id="Seg_892" n="HIAT:ip">.</nts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T235" id="Seg_895" n="HIAT:u" s="T228">
                  <ts e="T229" id="Seg_897" n="HIAT:w" s="T228">Dĭgəttə</ts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_900" n="HIAT:w" s="T229">dĭ</ts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_902" n="HIAT:ip">(</nts>
                  <ts e="T231" id="Seg_904" n="HIAT:w" s="T230">dʼabəluʔpi</ts>
                  <nts id="Seg_905" n="HIAT:ip">)</nts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_908" n="HIAT:w" s="T231">xvostə</ts>
                  <nts id="Seg_909" n="HIAT:ip">,</nts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_912" n="HIAT:w" s="T232">xvostə</ts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_914" n="HIAT:ip">(</nts>
                  <ts e="T234" id="Seg_916" n="HIAT:w" s="T233">dʼ-</ts>
                  <nts id="Seg_917" n="HIAT:ip">)</nts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_920" n="HIAT:w" s="T234">sajnʼeʔpi</ts>
                  <nts id="Seg_921" n="HIAT:ip">.</nts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T239" id="Seg_924" n="HIAT:u" s="T235">
                  <ts e="T236" id="Seg_926" n="HIAT:w" s="T235">Dĭgəttə</ts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_929" n="HIAT:w" s="T236">dagajzi</ts>
                  <nts id="Seg_930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_931" n="HIAT:ip">(</nts>
                  <ts e="T238" id="Seg_933" n="HIAT:w" s="T237">bă-</ts>
                  <nts id="Seg_934" n="HIAT:ip">)</nts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_937" n="HIAT:w" s="T238">băʔpi</ts>
                  <nts id="Seg_938" n="HIAT:ip">.</nts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T242" id="Seg_941" n="HIAT:u" s="T239">
                  <ts e="T240" id="Seg_943" n="HIAT:w" s="T239">Dĭn</ts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_946" n="HIAT:w" s="T240">jašɨk</ts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_949" n="HIAT:w" s="T241">kubi</ts>
                  <nts id="Seg_950" n="HIAT:ip">.</nts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T245" id="Seg_953" n="HIAT:u" s="T242">
                  <ts e="T243" id="Seg_955" n="HIAT:w" s="T242">Jašɨkkən</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_958" n="HIAT:w" s="T243">sazən</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_960" n="HIAT:ip">(</nts>
                  <ts e="T245" id="Seg_962" n="HIAT:w" s="T244">pʼaŋnomə</ts>
                  <nts id="Seg_963" n="HIAT:ip">)</nts>
                  <nts id="Seg_964" n="HIAT:ip">.</nts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T256" id="Seg_967" n="HIAT:u" s="T245">
                  <nts id="Seg_968" n="HIAT:ip">(</nts>
                  <ts e="T246" id="Seg_970" n="HIAT:w" s="T245">Măn</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_973" n="HIAT:w" s="T246">a-</ts>
                  <nts id="Seg_974" n="HIAT:ip">)</nts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_977" n="HIAT:w" s="T247">Măn</ts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_980" n="HIAT:w" s="T248">abam</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_982" n="HIAT:ip">(</nts>
                  <ts e="T250" id="Seg_984" n="HIAT:w" s="T249">tăn</ts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_987" n="HIAT:w" s="T250">aba-</ts>
                  <nts id="Seg_988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_990" n="HIAT:w" s="T251">abatsi=</ts>
                  <nts id="Seg_991" n="HIAT:ip">)</nts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_994" n="HIAT:w" s="T252">tăn</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_997" n="HIAT:w" s="T253">abandə</ts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_1000" n="HIAT:w" s="T254">tüʔsittə</ts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1019" id="Seg_1003" n="HIAT:w" s="T255">kalla</ts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_1006" n="HIAT:w" s="T1019">dʼürbi</ts>
                  <nts id="Seg_1007" n="HIAT:ip">.</nts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T257" id="Seg_1010" n="HIAT:u" s="T256">
                  <nts id="Seg_1011" n="HIAT:ip">(</nts>
                  <nts id="Seg_1012" n="HIAT:ip">(</nts>
                  <ats e="T257" id="Seg_1013" n="HIAT:non-pho" s="T256">BRK</ats>
                  <nts id="Seg_1014" n="HIAT:ip">)</nts>
                  <nts id="Seg_1015" n="HIAT:ip">)</nts>
                  <nts id="Seg_1016" n="HIAT:ip">.</nts>
                  <nts id="Seg_1017" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T263" id="Seg_1019" n="HIAT:u" s="T257">
                  <ts e="T258" id="Seg_1021" n="HIAT:w" s="T257">Dĭgəttə</ts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_1024" n="HIAT:w" s="T258">dĭ</ts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_1027" n="HIAT:w" s="T259">büzʼe</ts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_1030" n="HIAT:w" s="T260">mămbi:</ts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1033" n="HIAT:w" s="T261">Iʔ</ts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1036" n="HIAT:w" s="T262">šamaʔ</ts>
                  <nts id="Seg_1037" n="HIAT:ip">!</nts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T278" id="Seg_1040" n="HIAT:u" s="T263">
                  <ts e="T264" id="Seg_1042" n="HIAT:w" s="T263">A</ts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1044" n="HIAT:ip">(</nts>
                  <ts e="T265" id="Seg_1046" n="HIAT:w" s="T264">dĭ</ts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1049" n="HIAT:w" s="T265">m-</ts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1052" n="HIAT:w" s="T266">dĭ</ts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1055" n="HIAT:w" s="T267">dĭnʼ</ts>
                  <nts id="Seg_1056" n="HIAT:ip">)</nts>
                  <nts id="Seg_1057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1059" n="HIAT:w" s="T268">kuza</ts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1062" n="HIAT:w" s="T269">dĭʔnə</ts>
                  <nts id="Seg_1063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1065" n="HIAT:w" s="T270">bögəldə</ts>
                  <nts id="Seg_1066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1068" n="HIAT:w" s="T271">ремни</ts>
                  <nts id="Seg_1069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1071" n="HIAT:w" s="T272">băʔpi</ts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1074" n="HIAT:w" s="T273">i</ts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1077" n="HIAT:w" s="T274">šü</ts>
                  <nts id="Seg_1078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1080" n="HIAT:w" s="T275">ibi</ts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1083" n="HIAT:w" s="T276">i</ts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1020" id="Seg_1086" n="HIAT:w" s="T277">kalla</ts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1089" n="HIAT:w" s="T1020">dʼürbi</ts>
                  <nts id="Seg_1090" n="HIAT:ip">.</nts>
                  <nts id="Seg_1091" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T279" id="Seg_1093" n="HIAT:u" s="T278">
                  <nts id="Seg_1094" n="HIAT:ip">(</nts>
                  <nts id="Seg_1095" n="HIAT:ip">(</nts>
                  <ats e="T279" id="Seg_1096" n="HIAT:non-pho" s="T278">BRK</ats>
                  <nts id="Seg_1097" n="HIAT:ip">)</nts>
                  <nts id="Seg_1098" n="HIAT:ip">)</nts>
                  <nts id="Seg_1099" n="HIAT:ip">.</nts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T288" id="Seg_1102" n="HIAT:u" s="T279">
                  <ts e="T280" id="Seg_1104" n="HIAT:w" s="T279">Măn</ts>
                  <nts id="Seg_1105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1107" n="HIAT:w" s="T280">meim</ts>
                  <nts id="Seg_1108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1110" n="HIAT:w" s="T281">măn</ts>
                  <nts id="Seg_1111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1113" n="HIAT:w" s="T282">tibi</ts>
                  <nts id="Seg_1114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1115" n="HIAT:ip">(</nts>
                  <ts e="T284" id="Seg_1117" n="HIAT:w" s="T283">ka-</ts>
                  <nts id="Seg_1118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1120" n="HIAT:w" s="T284">i-</ts>
                  <nts id="Seg_1121" n="HIAT:ip">)</nts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1124" n="HIAT:w" s="T285">ileʔpi</ts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1127" n="HIAT:w" s="T286">i</ts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1021" id="Seg_1130" n="HIAT:w" s="T287">kalla</ts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1133" n="HIAT:w" s="T1021">dʼürbiʔi</ts>
                  <nts id="Seg_1134" n="HIAT:ip">.</nts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T297" id="Seg_1137" n="HIAT:u" s="T288">
                  <ts e="T289" id="Seg_1139" n="HIAT:w" s="T288">Miʔ</ts>
                  <nts id="Seg_1140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1142" n="HIAT:w" s="T289">jakše</ts>
                  <nts id="Seg_1143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1145" n="HIAT:w" s="T290">amnobiʔi</ts>
                  <nts id="Seg_1146" n="HIAT:ip">,</nts>
                  <nts id="Seg_1147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1149" n="HIAT:w" s="T291">a</ts>
                  <nts id="Seg_1150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1152" n="HIAT:w" s="T292">dĭ</ts>
                  <nts id="Seg_1153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1155" n="HIAT:w" s="T293">măna</ts>
                  <nts id="Seg_1156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1158" n="HIAT:w" s="T294">ajirbi</ts>
                  <nts id="Seg_1159" n="HIAT:ip">,</nts>
                  <nts id="Seg_1160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1162" n="HIAT:w" s="T295">udanə</ts>
                  <nts id="Seg_1163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1165" n="HIAT:w" s="T296">ibi</ts>
                  <nts id="Seg_1166" n="HIAT:ip">.</nts>
                  <nts id="Seg_1167" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T304" id="Seg_1169" n="HIAT:u" s="T297">
                  <ts e="T298" id="Seg_1171" n="HIAT:w" s="T297">A</ts>
                  <nts id="Seg_1172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1173" n="HIAT:ip">(</nts>
                  <ts e="T299" id="Seg_1175" n="HIAT:w" s="T298">dĭgəttə</ts>
                  <nts id="Seg_1176" n="HIAT:ip">)</nts>
                  <nts id="Seg_1177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1022" id="Seg_1179" n="HIAT:w" s="T299">kalla</ts>
                  <nts id="Seg_1180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1182" n="HIAT:w" s="T1022">dʼürbi</ts>
                  <nts id="Seg_1183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1185" n="HIAT:w" s="T300">Igarkanə</ts>
                  <nts id="Seg_1186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1188" n="HIAT:w" s="T301">i</ts>
                  <nts id="Seg_1189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1191" n="HIAT:w" s="T302">dĭn</ts>
                  <nts id="Seg_1192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1194" n="HIAT:w" s="T303">külambi</ts>
                  <nts id="Seg_1195" n="HIAT:ip">.</nts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T305" id="Seg_1198" n="HIAT:u" s="T304">
                  <nts id="Seg_1199" n="HIAT:ip">(</nts>
                  <nts id="Seg_1200" n="HIAT:ip">(</nts>
                  <ats e="T305" id="Seg_1201" n="HIAT:non-pho" s="T304">BRK</ats>
                  <nts id="Seg_1202" n="HIAT:ip">)</nts>
                  <nts id="Seg_1203" n="HIAT:ip">)</nts>
                  <nts id="Seg_1204" n="HIAT:ip">.</nts>
                  <nts id="Seg_1205" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T313" id="Seg_1207" n="HIAT:u" s="T305">
                  <ts e="T306" id="Seg_1209" n="HIAT:w" s="T305">A</ts>
                  <nts id="Seg_1210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1211" n="HIAT:ip">(</nts>
                  <ts e="T307" id="Seg_1213" n="HIAT:w" s="T306">dĭ</ts>
                  <nts id="Seg_1214" n="HIAT:ip">)</nts>
                  <nts id="Seg_1215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1217" n="HIAT:w" s="T307">ej</ts>
                  <nts id="Seg_1218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1220" n="HIAT:w" s="T308">kuvas</ts>
                  <nts id="Seg_1221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1223" n="HIAT:w" s="T309">ibi</ts>
                  <nts id="Seg_1224" n="HIAT:ip">,</nts>
                  <nts id="Seg_1225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1227" n="HIAT:w" s="T310">aŋdə</ts>
                  <nts id="Seg_1228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1230" n="HIAT:w" s="T311">bar</ts>
                  <nts id="Seg_1231" n="HIAT:ip">…</nts>
                  <nts id="Seg_1232" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T314" id="Seg_1234" n="HIAT:u" s="T313">
                  <nts id="Seg_1235" n="HIAT:ip">(</nts>
                  <nts id="Seg_1236" n="HIAT:ip">(</nts>
                  <ats e="T314" id="Seg_1237" n="HIAT:non-pho" s="T313">BRK</ats>
                  <nts id="Seg_1238" n="HIAT:ip">)</nts>
                  <nts id="Seg_1239" n="HIAT:ip">)</nts>
                  <nts id="Seg_1240" n="HIAT:ip">.</nts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T321" id="Seg_1243" n="HIAT:u" s="T314">
                  <ts e="T315" id="Seg_1245" n="HIAT:w" s="T314">Dĭ</ts>
                  <nts id="Seg_1246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1248" n="HIAT:w" s="T315">ej</ts>
                  <nts id="Seg_1249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1251" n="HIAT:w" s="T316">kuvas</ts>
                  <nts id="Seg_1252" n="HIAT:ip">,</nts>
                  <nts id="Seg_1253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1255" n="HIAT:w" s="T317">aŋdə</ts>
                  <nts id="Seg_1256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1258" n="HIAT:w" s="T318">păjdʼaŋ</ts>
                  <nts id="Seg_1259" n="HIAT:ip">,</nts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1262" n="HIAT:w" s="T319">sʼimat</ts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1265" n="HIAT:w" s="T320">păjdʼaŋ</ts>
                  <nts id="Seg_1266" n="HIAT:ip">.</nts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T322" id="Seg_1269" n="HIAT:u" s="T321">
                  <nts id="Seg_1270" n="HIAT:ip">(</nts>
                  <nts id="Seg_1271" n="HIAT:ip">(</nts>
                  <ats e="T322" id="Seg_1272" n="HIAT:non-pho" s="T321">BRK</ats>
                  <nts id="Seg_1273" n="HIAT:ip">)</nts>
                  <nts id="Seg_1274" n="HIAT:ip">)</nts>
                  <nts id="Seg_1275" n="HIAT:ip">.</nts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T325" id="Seg_1278" n="HIAT:u" s="T322">
                  <ts e="T323" id="Seg_1280" n="HIAT:w" s="T322">Šide</ts>
                  <nts id="Seg_1281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1282" n="HIAT:ip">(</nts>
                  <ts e="T324" id="Seg_1284" n="HIAT:w" s="T323">ne</ts>
                  <nts id="Seg_1285" n="HIAT:ip">)</nts>
                  <nts id="Seg_1286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1288" n="HIAT:w" s="T324">amnobiʔi</ts>
                  <nts id="Seg_1289" n="HIAT:ip">.</nts>
                  <nts id="Seg_1290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T329" id="Seg_1292" n="HIAT:u" s="T325">
                  <ts e="T326" id="Seg_1294" n="HIAT:w" s="T325">Kambiʔi</ts>
                  <nts id="Seg_1295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1297" n="HIAT:w" s="T326">sarankaʔi</ts>
                  <nts id="Seg_1298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1299" n="HIAT:ip">(</nts>
                  <ts e="T328" id="Seg_1301" n="HIAT:w" s="T327">tĭldəz'-</ts>
                  <nts id="Seg_1302" n="HIAT:ip">)</nts>
                  <nts id="Seg_1303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1305" n="HIAT:w" s="T328">tĭlzittə</ts>
                  <nts id="Seg_1306" n="HIAT:ip">.</nts>
                  <nts id="Seg_1307" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T335" id="Seg_1309" n="HIAT:u" s="T329">
                  <ts e="T330" id="Seg_1311" n="HIAT:w" s="T329">Onʼiʔ</ts>
                  <nts id="Seg_1312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1314" n="HIAT:w" s="T330">tĭlbi</ts>
                  <nts id="Seg_1315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1317" n="HIAT:w" s="T331">iʔgö</ts>
                  <nts id="Seg_1318" n="HIAT:ip">,</nts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1321" n="HIAT:w" s="T332">a</ts>
                  <nts id="Seg_1322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1324" n="HIAT:w" s="T333">onʼiʔ</ts>
                  <nts id="Seg_1325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1327" n="HIAT:w" s="T334">amga</ts>
                  <nts id="Seg_1328" n="HIAT:ip">.</nts>
                  <nts id="Seg_1329" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T347" id="Seg_1331" n="HIAT:u" s="T335">
                  <ts e="T336" id="Seg_1333" n="HIAT:w" s="T335">Girgit</ts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1336" n="HIAT:w" s="T336">iʔgö</ts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1339" n="HIAT:w" s="T337">tĭlbi</ts>
                  <nts id="Seg_1340" n="HIAT:ip">,</nts>
                  <nts id="Seg_1341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1343" n="HIAT:w" s="T338">a</ts>
                  <nts id="Seg_1344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1346" n="HIAT:w" s="T339">girgit</ts>
                  <nts id="Seg_1347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1349" n="HIAT:w" s="T340">amga</ts>
                  <nts id="Seg_1350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1352" n="HIAT:w" s="T341">tĭlbi</ts>
                  <nts id="Seg_1353" n="HIAT:ip">,</nts>
                  <nts id="Seg_1354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1356" n="HIAT:w" s="T342">dĭm</ts>
                  <nts id="Seg_1357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1358" n="HIAT:ip">(</nts>
                  <ts e="T344" id="Seg_1360" n="HIAT:w" s="T343">taʔ</ts>
                  <nts id="Seg_1361" n="HIAT:ip">)</nts>
                  <nts id="Seg_1362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1364" n="HIAT:w" s="T344">baltuzi</ts>
                  <nts id="Seg_1365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1367" n="HIAT:w" s="T345">ulut</ts>
                  <nts id="Seg_1368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1370" n="HIAT:w" s="T346">jaʔpi</ts>
                  <nts id="Seg_1371" n="HIAT:ip">.</nts>
                  <nts id="Seg_1372" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T350" id="Seg_1374" n="HIAT:u" s="T347">
                  <ts e="T348" id="Seg_1376" n="HIAT:w" s="T347">Dĭgəttə</ts>
                  <nts id="Seg_1377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1379" n="HIAT:w" s="T348">šobi</ts>
                  <nts id="Seg_1380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1382" n="HIAT:w" s="T349">maʔnə</ts>
                  <nts id="Seg_1383" n="HIAT:ip">.</nts>
                  <nts id="Seg_1384" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T355" id="Seg_1386" n="HIAT:u" s="T350">
                  <ts e="T351" id="Seg_1388" n="HIAT:w" s="T350">Koʔbdo</ts>
                  <nts id="Seg_1389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1391" n="HIAT:w" s="T351">šobi:</ts>
                  <nts id="Seg_1392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1394" n="HIAT:w" s="T352">Gijen</ts>
                  <nts id="Seg_1395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1397" n="HIAT:w" s="T353">măn</ts>
                  <nts id="Seg_1398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1400" n="HIAT:w" s="T354">iam</ts>
                  <nts id="Seg_1401" n="HIAT:ip">?</nts>
                  <nts id="Seg_1402" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T361" id="Seg_1404" n="HIAT:u" s="T355">
                  <ts e="T356" id="Seg_1406" n="HIAT:w" s="T355">Dĭ</ts>
                  <nts id="Seg_1407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1409" n="HIAT:w" s="T356">bar</ts>
                  <nts id="Seg_1410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1412" n="HIAT:w" s="T357">ɨrɨː</ts>
                  <nts id="Seg_1413" n="HIAT:ip">,</nts>
                  <nts id="Seg_1414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1416" n="HIAT:w" s="T358">ej</ts>
                  <nts id="Seg_1417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1419" n="HIAT:w" s="T359">tĭlbi</ts>
                  <nts id="Seg_1420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1422" n="HIAT:w" s="T360">sarankaʔi</ts>
                  <nts id="Seg_1423" n="HIAT:ip">.</nts>
                  <nts id="Seg_1424" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T364" id="Seg_1426" n="HIAT:u" s="T361">
                  <ts e="T362" id="Seg_1428" n="HIAT:w" s="T361">Dĭn</ts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1431" n="HIAT:w" s="T362">bar</ts>
                  <nts id="Seg_1432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1434" n="HIAT:w" s="T363">kunollaʔbə</ts>
                  <nts id="Seg_1435" n="HIAT:ip">.</nts>
                  <nts id="Seg_1436" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T374" id="Seg_1438" n="HIAT:u" s="T364">
                  <ts e="T365" id="Seg_1440" n="HIAT:w" s="T364">A</ts>
                  <nts id="Seg_1441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1443" n="HIAT:w" s="T365">koʔbdot</ts>
                  <nts id="Seg_1444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1445" n="HIAT:ip">(</nts>
                  <ts e="T367" id="Seg_1447" n="HIAT:w" s="T366">kădedə</ts>
                  <nts id="Seg_1448" n="HIAT:ip">)</nts>
                  <nts id="Seg_1449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1451" n="HIAT:w" s="T367">kăde</ts>
                  <nts id="Seg_1452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1454" n="HIAT:w" s="T368">-</ts>
                  <nts id="Seg_1455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1457" n="HIAT:w" s="T369">то</ts>
                  <nts id="Seg_1458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1459" n="HIAT:ip">(</nts>
                  <ts e="T371" id="Seg_1461" n="HIAT:w" s="T370">узн-</ts>
                  <nts id="Seg_1462" n="HIAT:ip">)</nts>
                  <nts id="Seg_1463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1465" n="HIAT:w" s="T371">tĭmnebi</ts>
                  <nts id="Seg_1466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1468" n="HIAT:w" s="T372">i</ts>
                  <nts id="Seg_1469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1471" n="HIAT:w" s="T373">šobi</ts>
                  <nts id="Seg_1472" n="HIAT:ip">.</nts>
                  <nts id="Seg_1473" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T376" id="Seg_1475" n="HIAT:u" s="T374">
                  <ts e="T375" id="Seg_1477" n="HIAT:w" s="T374">Nʼit</ts>
                  <nts id="Seg_1478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1480" n="HIAT:w" s="T375">ibi</ts>
                  <nts id="Seg_1481" n="HIAT:ip">.</nts>
                  <nts id="Seg_1482" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T390" id="Seg_1484" n="HIAT:u" s="T376">
                  <ts e="T377" id="Seg_1486" n="HIAT:w" s="T376">Dĭ</ts>
                  <nts id="Seg_1487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1489" n="HIAT:w" s="T377">dĭm</ts>
                  <nts id="Seg_1490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1491" n="HIAT:ip">(</nts>
                  <ts e="T379" id="Seg_1493" n="HIAT:w" s="T378">šaʔla</ts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1496" n="HIAT:w" s="T379">ibi</ts>
                  <nts id="Seg_1497" n="HIAT:ip">)</nts>
                  <nts id="Seg_1498" n="HIAT:ip">,</nts>
                  <nts id="Seg_1499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1500" n="HIAT:ip">(</nts>
                  <ts e="T381" id="Seg_1502" n="HIAT:w" s="T380">kunol-</ts>
                  <nts id="Seg_1503" n="HIAT:ip">)</nts>
                  <nts id="Seg_1504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1506" n="HIAT:w" s="T381">kunolzittə</ts>
                  <nts id="Seg_1507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1509" n="HIAT:w" s="T382">embi</ts>
                  <nts id="Seg_1510" n="HIAT:ip">,</nts>
                  <nts id="Seg_1511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1513" n="HIAT:w" s="T383">a</ts>
                  <nts id="Seg_1514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1516" n="HIAT:w" s="T384">bostə</ts>
                  <nts id="Seg_1517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1519" n="HIAT:w" s="T385">nʼuʔdə</ts>
                  <nts id="Seg_1520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1521" n="HIAT:ip">(</nts>
                  <ts e="T387" id="Seg_1523" n="HIAT:w" s="T386">sa-</ts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1526" n="HIAT:w" s="T387">š-</ts>
                  <nts id="Seg_1527" n="HIAT:ip">)</nts>
                  <nts id="Seg_1528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1530" n="HIAT:w" s="T388">sʼabi</ts>
                  <nts id="Seg_1531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1533" n="HIAT:w" s="T389">maʔtə</ts>
                  <nts id="Seg_1534" n="HIAT:ip">.</nts>
                  <nts id="Seg_1535" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T392" id="Seg_1537" n="HIAT:u" s="T390">
                  <ts e="T391" id="Seg_1539" n="HIAT:w" s="T390">Зола</ts>
                  <nts id="Seg_1540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1542" n="HIAT:w" s="T391">ibi</ts>
                  <nts id="Seg_1543" n="HIAT:ip">.</nts>
                  <nts id="Seg_1544" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T397" id="Seg_1546" n="HIAT:u" s="T392">
                  <ts e="T393" id="Seg_1548" n="HIAT:w" s="T392">Dĭgəttə</ts>
                  <nts id="Seg_1549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1550" n="HIAT:ip">(</nts>
                  <ts e="T394" id="Seg_1552" n="HIAT:w" s="T393">dĭn-</ts>
                  <nts id="Seg_1553" n="HIAT:ip">)</nts>
                  <nts id="Seg_1554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1556" n="HIAT:w" s="T394">dĭ</ts>
                  <nts id="Seg_1557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1559" n="HIAT:w" s="T395">ne</ts>
                  <nts id="Seg_1560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1562" n="HIAT:w" s="T396">šobi</ts>
                  <nts id="Seg_1563" n="HIAT:ip">.</nts>
                  <nts id="Seg_1564" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T405" id="Seg_1566" n="HIAT:u" s="T397">
                  <ts e="T398" id="Seg_1568" n="HIAT:w" s="T397">Gijen</ts>
                  <nts id="Seg_1569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1571" n="HIAT:w" s="T398">šiʔ</ts>
                  <nts id="Seg_1572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1573" n="HIAT:ip">(</nts>
                  <ts e="T400" id="Seg_1575" n="HIAT:w" s="T399">t-</ts>
                  <nts id="Seg_1576" n="HIAT:ip">)</nts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1579" n="HIAT:w" s="T400">dĭ</ts>
                  <nts id="Seg_1580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1582" n="HIAT:w" s="T401">măndə</ts>
                  <nts id="Seg_1583" n="HIAT:ip">,</nts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1586" n="HIAT:w" s="T402">măn</ts>
                  <nts id="Seg_1587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1589" n="HIAT:w" s="T403">dön</ts>
                  <nts id="Seg_1590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1592" n="HIAT:w" s="T404">amnolaʔbəm</ts>
                  <nts id="Seg_1593" n="HIAT:ip">!</nts>
                  <nts id="Seg_1594" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T411" id="Seg_1596" n="HIAT:u" s="T405">
                  <ts e="T406" id="Seg_1598" n="HIAT:w" s="T405">Dĭ</ts>
                  <nts id="Seg_1599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1600" n="HIAT:ip">(</nts>
                  <ts e="T407" id="Seg_1602" n="HIAT:w" s="T406">za-</ts>
                  <nts id="Seg_1603" n="HIAT:ip">)</nts>
                  <nts id="Seg_1604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1606" n="HIAT:w" s="T407">зола</ts>
                  <nts id="Seg_1607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1608" n="HIAT:ip">(</nts>
                  <ts e="T409" id="Seg_1610" n="HIAT:w" s="T408">s-</ts>
                  <nts id="Seg_1611" n="HIAT:ip">)</nts>
                  <nts id="Seg_1612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1614" n="HIAT:w" s="T409">kămnəbi</ts>
                  <nts id="Seg_1615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1617" n="HIAT:w" s="T410">sʼimandə</ts>
                  <nts id="Seg_1618" n="HIAT:ip">.</nts>
                  <nts id="Seg_1619" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T412" id="Seg_1621" n="HIAT:u" s="T411">
                  <nts id="Seg_1622" n="HIAT:ip">(</nts>
                  <nts id="Seg_1623" n="HIAT:ip">(</nts>
                  <ats e="T412" id="Seg_1624" n="HIAT:non-pho" s="T411">BRK</ats>
                  <nts id="Seg_1625" n="HIAT:ip">)</nts>
                  <nts id="Seg_1626" n="HIAT:ip">)</nts>
                  <nts id="Seg_1627" n="HIAT:ip">.</nts>
                  <nts id="Seg_1628" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T415" id="Seg_1630" n="HIAT:u" s="T412">
                  <ts e="T413" id="Seg_1632" n="HIAT:w" s="T412">Ĭmbidə</ts>
                  <nts id="Seg_1633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1635" n="HIAT:w" s="T413">ej</ts>
                  <nts id="Seg_1636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1638" n="HIAT:w" s="T414">măndəliaʔi</ts>
                  <nts id="Seg_1639" n="HIAT:ip">.</nts>
                  <nts id="Seg_1640" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T420" id="Seg_1642" n="HIAT:u" s="T415">
                  <ts e="T416" id="Seg_1644" n="HIAT:w" s="T415">Dĭgəttə</ts>
                  <nts id="Seg_1645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1647" n="HIAT:w" s="T416">sʼimat</ts>
                  <nts id="Seg_1648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1650" n="HIAT:w" s="T417">ĭmbidə</ts>
                  <nts id="Seg_1651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1653" n="HIAT:w" s="T418">ej</ts>
                  <nts id="Seg_1654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1656" n="HIAT:w" s="T419">moliaʔi</ts>
                  <nts id="Seg_1657" n="HIAT:ip">.</nts>
                  <nts id="Seg_1658" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T430" id="Seg_1660" n="HIAT:u" s="T420">
                  <ts e="T421" id="Seg_1662" n="HIAT:w" s="T420">Dĭ</ts>
                  <nts id="Seg_1663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1665" n="HIAT:w" s="T421">suʔməluʔpi</ts>
                  <nts id="Seg_1666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1668" n="HIAT:w" s="T422">maʔtə</ts>
                  <nts id="Seg_1669" n="HIAT:ip">,</nts>
                  <nts id="Seg_1670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1672" n="HIAT:w" s="T423">kagat</ts>
                  <nts id="Seg_1673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1675" n="HIAT:w" s="T424">ibi</ts>
                  <nts id="Seg_1676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1678" n="HIAT:w" s="T425">i</ts>
                  <nts id="Seg_1679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1023" id="Seg_1681" n="HIAT:w" s="T426">kalla</ts>
                  <nts id="Seg_1682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1684" n="HIAT:w" s="T1023">dʼürbi</ts>
                  <nts id="Seg_1685" n="HIAT:ip">,</nts>
                  <nts id="Seg_1686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1688" n="HIAT:w" s="T427">gijen</ts>
                  <nts id="Seg_1689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1691" n="HIAT:w" s="T428">il</ts>
                  <nts id="Seg_1692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1694" n="HIAT:w" s="T429">iʔgö</ts>
                  <nts id="Seg_1695" n="HIAT:ip">.</nts>
                  <nts id="Seg_1696" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T431" id="Seg_1698" n="HIAT:u" s="T430">
                  <nts id="Seg_1699" n="HIAT:ip">(</nts>
                  <nts id="Seg_1700" n="HIAT:ip">(</nts>
                  <ats e="T431" id="Seg_1701" n="HIAT:non-pho" s="T430">BRK</ats>
                  <nts id="Seg_1702" n="HIAT:ip">)</nts>
                  <nts id="Seg_1703" n="HIAT:ip">)</nts>
                  <nts id="Seg_1704" n="HIAT:ip">.</nts>
                  <nts id="Seg_1705" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T438" id="Seg_1707" n="HIAT:u" s="T431">
                  <ts e="T432" id="Seg_1709" n="HIAT:w" s="T431">Măn</ts>
                  <nts id="Seg_1710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1712" n="HIAT:w" s="T432">tibi</ts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1024" id="Seg_1715" n="HIAT:w" s="T433">kalla</ts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1718" n="HIAT:w" s="T1024">dʼürbi</ts>
                  <nts id="Seg_1719" n="HIAT:ip">,</nts>
                  <nts id="Seg_1720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1722" n="HIAT:w" s="T434">măn</ts>
                  <nts id="Seg_1723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1725" n="HIAT:w" s="T435">ugandə</ts>
                  <nts id="Seg_1726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1728" n="HIAT:w" s="T436">tăŋ</ts>
                  <nts id="Seg_1729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1731" n="HIAT:w" s="T437">dʼorbiam</ts>
                  <nts id="Seg_1732" n="HIAT:ip">.</nts>
                  <nts id="Seg_1733" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T441" id="Seg_1735" n="HIAT:u" s="T438">
                  <ts e="T439" id="Seg_1737" n="HIAT:w" s="T438">Sĭjbə</ts>
                  <nts id="Seg_1738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1740" n="HIAT:w" s="T439">bar</ts>
                  <nts id="Seg_1741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1743" n="HIAT:w" s="T440">ĭzembi</ts>
                  <nts id="Seg_1744" n="HIAT:ip">.</nts>
                  <nts id="Seg_1745" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T445" id="Seg_1747" n="HIAT:u" s="T441">
                  <ts e="T442" id="Seg_1749" n="HIAT:w" s="T441">I</ts>
                  <nts id="Seg_1750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1752" n="HIAT:w" s="T442">ulum</ts>
                  <nts id="Seg_1753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1754" n="HIAT:ip">(</nts>
                  <nts id="Seg_1755" n="HIAT:ip">(</nts>
                  <ats e="T444" id="Seg_1756" n="HIAT:non-pho" s="T443">…</ats>
                  <nts id="Seg_1757" n="HIAT:ip">)</nts>
                  <nts id="Seg_1758" n="HIAT:ip">)</nts>
                  <nts id="Seg_1759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1760" n="HIAT:ip">(</nts>
                  <nts id="Seg_1761" n="HIAT:ip">(</nts>
                  <ats e="T445" id="Seg_1762" n="HIAT:non-pho" s="T444">DMG</ats>
                  <nts id="Seg_1763" n="HIAT:ip">)</nts>
                  <nts id="Seg_1764" n="HIAT:ip">)</nts>
                  <nts id="Seg_1765" n="HIAT:ip">.</nts>
                  <nts id="Seg_1766" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T446" id="Seg_1768" n="HIAT:u" s="T445">
                  <nts id="Seg_1769" n="HIAT:ip">(</nts>
                  <nts id="Seg_1770" n="HIAT:ip">(</nts>
                  <ats e="T446" id="Seg_1771" n="HIAT:non-pho" s="T445">BRK</ats>
                  <nts id="Seg_1772" n="HIAT:ip">)</nts>
                  <nts id="Seg_1773" n="HIAT:ip">)</nts>
                  <nts id="Seg_1774" n="HIAT:ip">.</nts>
                  <nts id="Seg_1775" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T455" id="Seg_1777" n="HIAT:u" s="T446">
                  <ts e="T447" id="Seg_1779" n="HIAT:w" s="T446">Măn</ts>
                  <nts id="Seg_1780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1781" n="HIAT:ip">(</nts>
                  <ts e="T448" id="Seg_1783" n="HIAT:w" s="T447">ser-</ts>
                  <nts id="Seg_1784" n="HIAT:ip">)</nts>
                  <nts id="Seg_1785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1787" n="HIAT:w" s="T448">sʼestram</ts>
                  <nts id="Seg_1788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1789" n="HIAT:ip">(</nts>
                  <ts e="T450" id="Seg_1791" n="HIAT:w" s="T449">nʼi=</ts>
                  <nts id="Seg_1792" n="HIAT:ip">)</nts>
                  <nts id="Seg_1793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1795" n="HIAT:w" s="T450">nʼit</ts>
                  <nts id="Seg_1796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1798" n="HIAT:w" s="T451">tʼerməndə</ts>
                  <nts id="Seg_1799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1801" n="HIAT:w" s="T452">mĭmbi</ts>
                  <nts id="Seg_1802" n="HIAT:ip">,</nts>
                  <nts id="Seg_1803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1805" n="HIAT:w" s="T453">ipek</ts>
                  <nts id="Seg_1806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1807" n="HIAT:ip">(</nts>
                  <nts id="Seg_1808" n="HIAT:ip">(</nts>
                  <ats e="T455" id="Seg_1809" n="HIAT:non-pho" s="T454">DMG</ats>
                  <nts id="Seg_1810" n="HIAT:ip">)</nts>
                  <nts id="Seg_1811" n="HIAT:ip">)</nts>
                  <nts id="Seg_1812" n="HIAT:ip">.</nts>
                  <nts id="Seg_1813" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T459" id="Seg_1815" n="HIAT:u" s="T455">
                  <ts e="T456" id="Seg_1817" n="HIAT:w" s="T455">Tʼerməndə</ts>
                  <nts id="Seg_1818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1820" n="HIAT:w" s="T456">mĭbi</ts>
                  <nts id="Seg_1821" n="HIAT:ip">,</nts>
                  <nts id="Seg_1822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1824" n="HIAT:w" s="T457">un</ts>
                  <nts id="Seg_1825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1827" n="HIAT:w" s="T458">nʼeʔsittə</ts>
                  <nts id="Seg_1828" n="HIAT:ip">.</nts>
                  <nts id="Seg_1829" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T462" id="Seg_1831" n="HIAT:u" s="T459">
                  <ts e="T460" id="Seg_1833" n="HIAT:w" s="T459">Dĭgəttə</ts>
                  <nts id="Seg_1834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1836" n="HIAT:w" s="T460">maʔndə</ts>
                  <nts id="Seg_1837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1839" n="HIAT:w" s="T461">šobi</ts>
                  <nts id="Seg_1840" n="HIAT:ip">.</nts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T474" id="Seg_1843" n="HIAT:u" s="T462">
                  <ts e="T463" id="Seg_1845" n="HIAT:w" s="T462">Bostə</ts>
                  <nts id="Seg_1846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1848" n="HIAT:w" s="T463">nenə</ts>
                  <nts id="Seg_1849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1851" n="HIAT:w" s="T464">mănlia:</ts>
                  <nts id="Seg_1852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1854" n="HIAT:w" s="T465">Štobɨ</ts>
                  <nts id="Seg_1855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1857" n="HIAT:w" s="T466">kondʼo</ts>
                  <nts id="Seg_1858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1859" n="HIAT:ip">(</nts>
                  <ts e="T468" id="Seg_1861" n="HIAT:w" s="T467">kaba-</ts>
                  <nts id="Seg_1862" n="HIAT:ip">)</nts>
                  <nts id="Seg_1863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1865" n="HIAT:w" s="T468">kabarləj</ts>
                  <nts id="Seg_1866" n="HIAT:ip">,</nts>
                  <nts id="Seg_1867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1869" n="HIAT:w" s="T469">a</ts>
                  <nts id="Seg_1870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1872" n="HIAT:w" s="T470">măn</ts>
                  <nts id="Seg_1873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1875" n="HIAT:w" s="T471">amnoliam</ts>
                  <nts id="Seg_1876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1878" n="HIAT:w" s="T472">da</ts>
                  <nts id="Seg_1879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1881" n="HIAT:w" s="T473">tenöliam</ts>
                  <nts id="Seg_1882" n="HIAT:ip">.</nts>
                  <nts id="Seg_1883" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T488" id="Seg_1885" n="HIAT:u" s="T474">
                  <ts e="T475" id="Seg_1887" n="HIAT:w" s="T474">Ладно</ts>
                  <nts id="Seg_1888" n="HIAT:ip">,</nts>
                  <nts id="Seg_1889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1891" n="HIAT:w" s="T475">što</ts>
                  <nts id="Seg_1892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1894" n="HIAT:w" s="T476">măn</ts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1896" n="HIAT:ip">(</nts>
                  <ts e="T478" id="Seg_1898" n="HIAT:w" s="T477">dĭzi-</ts>
                  <nts id="Seg_1899" n="HIAT:ip">)</nts>
                  <nts id="Seg_1900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1902" n="HIAT:w" s="T478">dĭzeŋzi</ts>
                  <nts id="Seg_1903" n="HIAT:ip">,</nts>
                  <nts id="Seg_1904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1906" n="HIAT:w" s="T479">amorzittə</ts>
                  <nts id="Seg_1907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1909" n="HIAT:w" s="T480">не</ts>
                  <nts id="Seg_1910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1912" n="HIAT:w" s="T481">стало</ts>
                  <nts id="Seg_1913" n="HIAT:ip">,</nts>
                  <nts id="Seg_1914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1916" n="HIAT:w" s="T482">to</ts>
                  <nts id="Seg_1917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1918" n="HIAT:ip">(</nts>
                  <ts e="T484" id="Seg_1920" n="HIAT:w" s="T483">mĭmbiʔi</ts>
                  <nts id="Seg_1921" n="HIAT:ip">,</nts>
                  <nts id="Seg_1922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1924" n="HIAT:w" s="T484">boštə</ts>
                  <nts id="Seg_1925" n="HIAT:ip">)</nts>
                  <nts id="Seg_1926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1928" n="HIAT:w" s="T485">măn</ts>
                  <nts id="Seg_1929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1931" n="HIAT:w" s="T486">iʔgö</ts>
                  <nts id="Seg_1932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1934" n="HIAT:w" s="T487">amniam</ts>
                  <nts id="Seg_1935" n="HIAT:ip">"</nts>
                  <nts id="Seg_1936" n="HIAT:ip">.</nts>
                  <nts id="Seg_1937" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T489" id="Seg_1939" n="HIAT:u" s="T488">
                  <nts id="Seg_1940" n="HIAT:ip">(</nts>
                  <nts id="Seg_1941" n="HIAT:ip">(</nts>
                  <ats e="T489" id="Seg_1942" n="HIAT:non-pho" s="T488">BRK</ats>
                  <nts id="Seg_1943" n="HIAT:ip">)</nts>
                  <nts id="Seg_1944" n="HIAT:ip">)</nts>
                  <nts id="Seg_1945" n="HIAT:ip">.</nts>
                  <nts id="Seg_1946" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T494" id="Seg_1948" n="HIAT:u" s="T489">
                  <ts e="T490" id="Seg_1950" n="HIAT:w" s="T489">Onʼiʔ</ts>
                  <nts id="Seg_1951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1953" n="HIAT:w" s="T490">kuza</ts>
                  <nts id="Seg_1954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1956" n="HIAT:w" s="T491">monoʔkosʼtə</ts>
                  <nts id="Seg_1957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1958" n="HIAT:ip">(</nts>
                  <ts e="T493" id="Seg_1960" n="HIAT:w" s="T492">k-</ts>
                  <nts id="Seg_1961" n="HIAT:ip">)</nts>
                  <nts id="Seg_1962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1964" n="HIAT:w" s="T493">kambi</ts>
                  <nts id="Seg_1965" n="HIAT:ip">.</nts>
                  <nts id="Seg_1966" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T499" id="Seg_1968" n="HIAT:u" s="T494">
                  <ts e="T495" id="Seg_1970" n="HIAT:w" s="T494">Kuvas</ts>
                  <nts id="Seg_1971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1973" n="HIAT:w" s="T495">koʔbdo</ts>
                  <nts id="Seg_1974" n="HIAT:ip">,</nts>
                  <nts id="Seg_1975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1977" n="HIAT:w" s="T496">ugandə</ts>
                  <nts id="Seg_1978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1980" n="HIAT:w" s="T497">oldʼat</ts>
                  <nts id="Seg_1981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1983" n="HIAT:w" s="T498">iʔgö</ts>
                  <nts id="Seg_1984" n="HIAT:ip">.</nts>
                  <nts id="Seg_1985" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T506" id="Seg_1987" n="HIAT:u" s="T499">
                  <ts e="T500" id="Seg_1989" n="HIAT:w" s="T499">Kandəga</ts>
                  <nts id="Seg_1990" n="HIAT:ip">,</nts>
                  <nts id="Seg_1991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1993" n="HIAT:w" s="T500">dĭgəttə</ts>
                  <nts id="Seg_1994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1996" n="HIAT:w" s="T501">kuza</ts>
                  <nts id="Seg_1997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1999" n="HIAT:w" s="T502">dĭʔnə</ts>
                  <nts id="Seg_2000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_2002" n="HIAT:w" s="T503">măndə:</ts>
                  <nts id="Seg_2003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_2005" n="HIAT:w" s="T504">Gibər</ts>
                  <nts id="Seg_2006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_2008" n="HIAT:w" s="T505">kandəgal</ts>
                  <nts id="Seg_2009" n="HIAT:ip">?</nts>
                  <nts id="Seg_2010" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T507" id="Seg_2012" n="HIAT:u" s="T506">
                  <ts e="T507" id="Seg_2014" n="HIAT:w" s="T506">Monoʔkosʼtə</ts>
                  <nts id="Seg_2015" n="HIAT:ip">.</nts>
                  <nts id="Seg_2016" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T509" id="Seg_2018" n="HIAT:u" s="T507">
                  <ts e="T508" id="Seg_2020" n="HIAT:w" s="T507">Iʔ</ts>
                  <nts id="Seg_2021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_2023" n="HIAT:w" s="T508">măna</ts>
                  <nts id="Seg_2024" n="HIAT:ip">!</nts>
                  <nts id="Seg_2025" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T513" id="Seg_2027" n="HIAT:u" s="T509">
                  <ts e="T510" id="Seg_2029" n="HIAT:w" s="T509">A</ts>
                  <nts id="Seg_2030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_2032" n="HIAT:w" s="T510">tăn</ts>
                  <nts id="Seg_2033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_2035" n="HIAT:w" s="T511">ĭmbi</ts>
                  <nts id="Seg_2036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_2038" n="HIAT:w" s="T512">alial</ts>
                  <nts id="Seg_2039" n="HIAT:ip">?</nts>
                  <nts id="Seg_2040" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T516" id="Seg_2042" n="HIAT:u" s="T513">
                  <ts e="T514" id="Seg_2044" n="HIAT:w" s="T513">Amorzittə</ts>
                  <nts id="Seg_2045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_2047" n="HIAT:w" s="T514">da</ts>
                  <nts id="Seg_2048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_2050" n="HIAT:w" s="T515">tüʔsittə</ts>
                  <nts id="Seg_2051" n="HIAT:ip">.</nts>
                  <nts id="Seg_2052" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T519" id="Seg_2054" n="HIAT:u" s="T516">
                  <ts e="T517" id="Seg_2056" n="HIAT:w" s="T516">Dĭgəttə</ts>
                  <nts id="Seg_2057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_2059" n="HIAT:w" s="T517">kambiʔi</ts>
                  <nts id="Seg_2060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_2062" n="HIAT:w" s="T518">bazoʔ</ts>
                  <nts id="Seg_2063" n="HIAT:ip">.</nts>
                  <nts id="Seg_2064" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T524" id="Seg_2066" n="HIAT:u" s="T519">
                  <ts e="T520" id="Seg_2068" n="HIAT:w" s="T519">Bazoʔ</ts>
                  <nts id="Seg_2069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_2071" n="HIAT:w" s="T520">kuza</ts>
                  <nts id="Seg_2072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_2074" n="HIAT:w" s="T521">măndə:</ts>
                  <nts id="Seg_2075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_2077" n="HIAT:w" s="T522">Gibər</ts>
                  <nts id="Seg_2078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_2080" n="HIAT:w" s="T523">kandəgalaʔ</ts>
                  <nts id="Seg_2081" n="HIAT:ip">?</nts>
                  <nts id="Seg_2082" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T525" id="Seg_2084" n="HIAT:u" s="T524">
                  <ts e="T525" id="Seg_2086" n="HIAT:w" s="T524">Monoʔkosʼtə</ts>
                  <nts id="Seg_2087" n="HIAT:ip">.</nts>
                  <nts id="Seg_2088" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T527" id="Seg_2090" n="HIAT:u" s="T525">
                  <ts e="T526" id="Seg_2092" n="HIAT:w" s="T525">Măna</ts>
                  <nts id="Seg_2093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_2095" n="HIAT:w" s="T526">it</ts>
                  <nts id="Seg_2096" n="HIAT:ip">!</nts>
                  <nts id="Seg_2097" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T531" id="Seg_2099" n="HIAT:u" s="T527">
                  <ts e="T528" id="Seg_2101" n="HIAT:w" s="T527">A</ts>
                  <nts id="Seg_2102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_2104" n="HIAT:w" s="T528">tăn</ts>
                  <nts id="Seg_2105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_2107" n="HIAT:w" s="T529">ĭmbi</ts>
                  <nts id="Seg_2108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_2110" n="HIAT:w" s="T530">aləl</ts>
                  <nts id="Seg_2111" n="HIAT:ip">?</nts>
                  <nts id="Seg_2112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T534" id="Seg_2114" n="HIAT:u" s="T531">
                  <ts e="T532" id="Seg_2116" n="HIAT:w" s="T531">Bĭʔsittə</ts>
                  <nts id="Seg_2117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_2119" n="HIAT:w" s="T532">da</ts>
                  <nts id="Seg_2120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2121" n="HIAT:ip">(</nts>
                  <ts e="T534" id="Seg_2123" n="HIAT:w" s="T533">kĭnzəsʼtə</ts>
                  <nts id="Seg_2124" n="HIAT:ip">)</nts>
                  <nts id="Seg_2125" n="HIAT:ip">.</nts>
                  <nts id="Seg_2126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T539" id="Seg_2128" n="HIAT:u" s="T534">
                  <ts e="T535" id="Seg_2130" n="HIAT:w" s="T534">Dĭgəttə</ts>
                  <nts id="Seg_2131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_2133" n="HIAT:w" s="T535">šobiʔi</ts>
                  <nts id="Seg_2134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_2136" n="HIAT:w" s="T536">i</ts>
                  <nts id="Seg_2137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_2139" n="HIAT:w" s="T537">davaj</ts>
                  <nts id="Seg_2140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_2142" n="HIAT:w" s="T538">monoʔkosʼtə</ts>
                  <nts id="Seg_2143" n="HIAT:ip">.</nts>
                  <nts id="Seg_2144" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T545" id="Seg_2146" n="HIAT:u" s="T539">
                  <ts e="T540" id="Seg_2148" n="HIAT:w" s="T539">Dĭ</ts>
                  <nts id="Seg_2149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_2151" n="HIAT:w" s="T540">măndə:</ts>
                  <nts id="Seg_2152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_2154" n="HIAT:w" s="T541">Măn</ts>
                  <nts id="Seg_2155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_2157" n="HIAT:w" s="T542">iʔgö</ts>
                  <nts id="Seg_2158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_2160" n="HIAT:w" s="T543">bar</ts>
                  <nts id="Seg_2161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_2163" n="HIAT:w" s="T544">aləm</ts>
                  <nts id="Seg_2164" n="HIAT:ip">.</nts>
                  <nts id="Seg_2165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T551" id="Seg_2167" n="HIAT:u" s="T545">
                  <ts e="T546" id="Seg_2169" n="HIAT:w" s="T545">Ipek</ts>
                  <nts id="Seg_2170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_2172" n="HIAT:w" s="T546">pürlem</ts>
                  <nts id="Seg_2173" n="HIAT:ip">,</nts>
                  <nts id="Seg_2174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_2176" n="HIAT:w" s="T547">uja</ts>
                  <nts id="Seg_2177" n="HIAT:ip">,</nts>
                  <nts id="Seg_2178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_2180" n="HIAT:w" s="T548">bar</ts>
                  <nts id="Seg_2181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_2183" n="HIAT:w" s="T549">amzittə</ts>
                  <nts id="Seg_2184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_2186" n="HIAT:w" s="T550">nada</ts>
                  <nts id="Seg_2187" n="HIAT:ip">.</nts>
                  <nts id="Seg_2188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T557" id="Seg_2190" n="HIAT:u" s="T551">
                  <ts e="T552" id="Seg_2192" n="HIAT:w" s="T551">Dĭ</ts>
                  <nts id="Seg_2193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2195" n="HIAT:w" s="T552">măndə:</ts>
                  <nts id="Seg_2196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2198" n="HIAT:w" s="T553">No</ts>
                  <nts id="Seg_2199" n="HIAT:ip">,</nts>
                  <nts id="Seg_2200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_2202" n="HIAT:w" s="T554">bar</ts>
                  <nts id="Seg_2203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2205" n="HIAT:w" s="T555">măn</ts>
                  <nts id="Seg_2206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_2208" n="HIAT:w" s="T556">amnam</ts>
                  <nts id="Seg_2209" n="HIAT:ip">.</nts>
                  <nts id="Seg_2210" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T561" id="Seg_2212" n="HIAT:u" s="T557">
                  <ts e="T558" id="Seg_2214" n="HIAT:w" s="T557">I</ts>
                  <nts id="Seg_2215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2216" n="HIAT:ip">(</nts>
                  <nts id="Seg_2217" n="HIAT:ip">(</nts>
                  <ats e="T559" id="Seg_2218" n="HIAT:non-pho" s="T558">PAUSE</ats>
                  <nts id="Seg_2219" n="HIAT:ip">)</nts>
                  <nts id="Seg_2220" n="HIAT:ip">)</nts>
                  <nts id="Seg_2221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_2223" n="HIAT:w" s="T559">пива</ts>
                  <nts id="Seg_2224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2226" n="HIAT:w" s="T560">iʔgö</ts>
                  <nts id="Seg_2227" n="HIAT:ip">.</nts>
                  <nts id="Seg_2228" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T565" id="Seg_2230" n="HIAT:u" s="T561">
                  <ts e="T562" id="Seg_2232" n="HIAT:w" s="T561">Bʼeʔ</ts>
                  <nts id="Seg_2233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2235" n="HIAT:w" s="T562">teʔtə</ts>
                  <nts id="Seg_2236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2238" n="HIAT:w" s="T563">ведро</ts>
                  <nts id="Seg_2239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2241" n="HIAT:w" s="T564">alam</ts>
                  <nts id="Seg_2242" n="HIAT:ip">.</nts>
                  <nts id="Seg_2243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T574" id="Seg_2245" n="HIAT:u" s="T565">
                  <ts e="T566" id="Seg_2247" n="HIAT:w" s="T565">No</ts>
                  <nts id="Seg_2248" n="HIAT:ip">,</nts>
                  <nts id="Seg_2249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2251" n="HIAT:w" s="T566">dĭgəttə</ts>
                  <nts id="Seg_2252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2253" n="HIAT:ip">(</nts>
                  <ts e="T568" id="Seg_2255" n="HIAT:w" s="T567">š-</ts>
                  <nts id="Seg_2256" n="HIAT:ip">)</nts>
                  <nts id="Seg_2257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2259" n="HIAT:w" s="T568">stoldə</ts>
                  <nts id="Seg_2260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2262" n="HIAT:w" s="T569">oʔbdəbi</ts>
                  <nts id="Seg_2263" n="HIAT:ip">,</nts>
                  <nts id="Seg_2264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2266" n="HIAT:w" s="T570">dĭgəttə</ts>
                  <nts id="Seg_2267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2269" n="HIAT:w" s="T571">šobi</ts>
                  <nts id="Seg_2270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_2272" n="HIAT:w" s="T572">dĭ</ts>
                  <nts id="Seg_2273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2275" n="HIAT:w" s="T573">kuza</ts>
                  <nts id="Seg_2276" n="HIAT:ip">.</nts>
                  <nts id="Seg_2277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T585" id="Seg_2279" n="HIAT:u" s="T574">
                  <ts e="T575" id="Seg_2281" n="HIAT:w" s="T574">Kajit</ts>
                  <nts id="Seg_2282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2284" n="HIAT:w" s="T575">amorzittə</ts>
                  <nts id="Seg_2285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2287" n="HIAT:w" s="T576">da</ts>
                  <nts id="Seg_2288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2290" n="HIAT:w" s="T577">tüʔsittə</ts>
                  <nts id="Seg_2291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2293" n="HIAT:w" s="T578">bar</ts>
                  <nts id="Seg_2294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2296" n="HIAT:w" s="T579">amnaʔbə</ts>
                  <nts id="Seg_2297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2299" n="HIAT:w" s="T580">da</ts>
                  <nts id="Seg_2300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2302" n="HIAT:w" s="T581">tüʔleʔbə</ts>
                  <nts id="Seg_2303" n="HIAT:ip">,</nts>
                  <nts id="Seg_2304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2306" n="HIAT:w" s="T582">amnaʔbə</ts>
                  <nts id="Seg_2307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2309" n="HIAT:w" s="T583">da</ts>
                  <nts id="Seg_2310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2312" n="HIAT:w" s="T584">tüʔleʔbə</ts>
                  <nts id="Seg_2313" n="HIAT:ip">.</nts>
                  <nts id="Seg_2314" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T595" id="Seg_2316" n="HIAT:u" s="T585">
                  <ts e="T586" id="Seg_2318" n="HIAT:w" s="T585">Bar</ts>
                  <nts id="Seg_2319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2321" n="HIAT:w" s="T586">ĭmbi</ts>
                  <nts id="Seg_2322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2324" n="HIAT:w" s="T587">amnuʔpi:</ts>
                  <nts id="Seg_2325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2327" n="HIAT:w" s="T588">i</ts>
                  <nts id="Seg_2328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2330" n="HIAT:w" s="T589">uja</ts>
                  <nts id="Seg_2331" n="HIAT:ip">,</nts>
                  <nts id="Seg_2332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2334" n="HIAT:w" s="T590">i</ts>
                  <nts id="Seg_2335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2336" n="HIAT:ip">(</nts>
                  <nts id="Seg_2337" n="HIAT:ip">(</nts>
                  <ats e="T592" id="Seg_2338" n="HIAT:non-pho" s="T591">PAUSE</ats>
                  <nts id="Seg_2339" n="HIAT:ip">)</nts>
                  <nts id="Seg_2340" n="HIAT:ip">)</nts>
                  <nts id="Seg_2341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2343" n="HIAT:w" s="T592">ĭmbi</ts>
                  <nts id="Seg_2344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2346" n="HIAT:w" s="T593">ibi</ts>
                  <nts id="Seg_2347" n="HIAT:ip">,</nts>
                  <nts id="Seg_2348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2350" n="HIAT:w" s="T594">bar</ts>
                  <nts id="Seg_2351" n="HIAT:ip">.</nts>
                  <nts id="Seg_2352" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T603" id="Seg_2354" n="HIAT:u" s="T595">
                  <ts e="T596" id="Seg_2356" n="HIAT:w" s="T595">Bostə</ts>
                  <nts id="Seg_2357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2359" n="HIAT:w" s="T596">măndə:</ts>
                  <nts id="Seg_2360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2362" n="HIAT:w" s="T597">Deʔkeʔ</ts>
                  <nts id="Seg_2363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2365" n="HIAT:w" s="T598">išo</ts>
                  <nts id="Seg_2366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2368" n="HIAT:w" s="T599">amga</ts>
                  <nts id="Seg_2369" n="HIAT:ip">,</nts>
                  <nts id="Seg_2370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2372" n="HIAT:w" s="T600">deʔkeʔ</ts>
                  <nts id="Seg_2373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2375" n="HIAT:w" s="T601">išo</ts>
                  <nts id="Seg_2376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2378" n="HIAT:w" s="T602">amga</ts>
                  <nts id="Seg_2379" n="HIAT:ip">.</nts>
                  <nts id="Seg_2380" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T611" id="Seg_2382" n="HIAT:u" s="T603">
                  <ts e="T604" id="Seg_2384" n="HIAT:w" s="T603">Dĭgəttə</ts>
                  <nts id="Seg_2385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2387" n="HIAT:w" s="T604">už</ts>
                  <nts id="Seg_2388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2390" n="HIAT:w" s="T605">amzittə</ts>
                  <nts id="Seg_2391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2392" n="HIAT:ip">(</nts>
                  <ts e="T607" id="Seg_2394" n="HIAT:w" s="T606">нечего</ts>
                  <nts id="Seg_2395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2397" n="HIAT:w" s="T607">ст</ts>
                  <nts id="Seg_2398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2400" n="HIAT:w" s="T608">-</ts>
                  <nts id="Seg_2401" n="HIAT:ip">)</nts>
                  <nts id="Seg_2402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2404" n="HIAT:w" s="T609">нечего</ts>
                  <nts id="Seg_2405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2407" n="HIAT:w" s="T610">стало</ts>
                  <nts id="Seg_2408" n="HIAT:ip">.</nts>
                  <nts id="Seg_2409" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T622" id="Seg_2411" n="HIAT:u" s="T611">
                  <ts e="T612" id="Seg_2413" n="HIAT:w" s="T611">Dĭgəttə</ts>
                  <nts id="Seg_2414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2415" n="HIAT:ip">(</nts>
                  <ts e="T613" id="Seg_2417" n="HIAT:w" s="T612">bü=</ts>
                  <nts id="Seg_2418" n="HIAT:ip">)</nts>
                  <nts id="Seg_2419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2421" n="HIAT:w" s="T613">пиво</ts>
                  <nts id="Seg_2422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2424" n="HIAT:w" s="T614">bĭʔsittə</ts>
                  <nts id="Seg_2425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2426" n="HIAT:ip">(</nts>
                  <ts e="T616" id="Seg_2428" n="HIAT:w" s="T615">bazoʔ=</ts>
                  <nts id="Seg_2429" n="HIAT:ip">)</nts>
                  <nts id="Seg_2430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2432" n="HIAT:w" s="T616">bazoʔ</ts>
                  <nts id="Seg_2433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2435" n="HIAT:w" s="T617">šobi</ts>
                  <nts id="Seg_2436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2437" n="HIAT:ip">(</nts>
                  <ts e="T619" id="Seg_2439" n="HIAT:w" s="T618">dĭ</ts>
                  <nts id="Seg_2440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2442" n="HIAT:w" s="T619">k-</ts>
                  <nts id="Seg_2443" n="HIAT:ip">)</nts>
                  <nts id="Seg_2444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2446" n="HIAT:w" s="T620">dĭ</ts>
                  <nts id="Seg_2447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2449" n="HIAT:w" s="T621">kuza</ts>
                  <nts id="Seg_2450" n="HIAT:ip">.</nts>
                  <nts id="Seg_2451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T632" id="Seg_2453" n="HIAT:u" s="T622">
                  <ts e="T623" id="Seg_2455" n="HIAT:w" s="T622">Dĭgəttə</ts>
                  <nts id="Seg_2456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2458" n="HIAT:w" s="T623">dĭ</ts>
                  <nts id="Seg_2459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2460" n="HIAT:ip">(</nts>
                  <ts e="T625" id="Seg_2462" n="HIAT:w" s="T624">bĭ-</ts>
                  <nts id="Seg_2463" n="HIAT:ip">)</nts>
                  <nts id="Seg_2464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2466" n="HIAT:w" s="T625">bĭtleʔbə</ts>
                  <nts id="Seg_2467" n="HIAT:ip">,</nts>
                  <nts id="Seg_2468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2470" n="HIAT:w" s="T626">bĭtleʔbə</ts>
                  <nts id="Seg_2471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2473" n="HIAT:w" s="T627">da</ts>
                  <nts id="Seg_2474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2476" n="HIAT:w" s="T628">kĭnzleʔbə</ts>
                  <nts id="Seg_2477" n="HIAT:ip">,</nts>
                  <nts id="Seg_2478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2480" n="HIAT:w" s="T629">bĭtleʔbə</ts>
                  <nts id="Seg_2481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2483" n="HIAT:w" s="T630">da</ts>
                  <nts id="Seg_2484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2486" n="HIAT:w" s="T631">kĭnzleʔbə</ts>
                  <nts id="Seg_2487" n="HIAT:ip">.</nts>
                  <nts id="Seg_2488" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T641" id="Seg_2490" n="HIAT:u" s="T632">
                  <ts e="T633" id="Seg_2492" n="HIAT:w" s="T632">Bar</ts>
                  <nts id="Seg_2493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2495" n="HIAT:w" s="T633">bĭtluʔpi</ts>
                  <nts id="Seg_2496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2498" n="HIAT:w" s="T634">da</ts>
                  <nts id="Seg_2499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2501" n="HIAT:w" s="T635">kirgarlaʔbə:</ts>
                  <nts id="Seg_2502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2504" n="HIAT:w" s="T636">Išo</ts>
                  <nts id="Seg_2505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2506" n="HIAT:ip">(</nts>
                  <ts e="T638" id="Seg_2508" n="HIAT:w" s="T637">d-</ts>
                  <nts id="Seg_2509" n="HIAT:ip">)</nts>
                  <nts id="Seg_2510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2512" n="HIAT:w" s="T638">deʔ</ts>
                  <nts id="Seg_2513" n="HIAT:ip">,</nts>
                  <nts id="Seg_2514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2516" n="HIAT:w" s="T639">măna</ts>
                  <nts id="Seg_2517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2519" n="HIAT:w" s="T640">amga</ts>
                  <nts id="Seg_2520" n="HIAT:ip">!</nts>
                  <nts id="Seg_2521" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T647" id="Seg_2523" n="HIAT:u" s="T641">
                  <ts e="T642" id="Seg_2525" n="HIAT:w" s="T641">A</ts>
                  <nts id="Seg_2526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2528" n="HIAT:w" s="T642">ulitsanə</ts>
                  <nts id="Seg_2529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2531" n="HIAT:w" s="T643">bar</ts>
                  <nts id="Seg_2532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2534" n="HIAT:w" s="T644">ugandə</ts>
                  <nts id="Seg_2535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2537" n="HIAT:w" s="T645">bü</ts>
                  <nts id="Seg_2538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2540" n="HIAT:w" s="T646">iʔgö</ts>
                  <nts id="Seg_2541" n="HIAT:ip">.</nts>
                  <nts id="Seg_2542" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T648" id="Seg_2544" n="HIAT:u" s="T647">
                  <nts id="Seg_2545" n="HIAT:ip">(</nts>
                  <nts id="Seg_2546" n="HIAT:ip">(</nts>
                  <ats e="T648" id="Seg_2547" n="HIAT:non-pho" s="T647">BRK</ats>
                  <nts id="Seg_2548" n="HIAT:ip">)</nts>
                  <nts id="Seg_2549" n="HIAT:ip">)</nts>
                  <nts id="Seg_2550" n="HIAT:ip">.</nts>
                  <nts id="Seg_2551" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T655" id="Seg_2553" n="HIAT:u" s="T648">
                  <ts e="T649" id="Seg_2555" n="HIAT:w" s="T648">Dĭgəttə</ts>
                  <nts id="Seg_2556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2558" n="HIAT:w" s="T649">ej</ts>
                  <nts id="Seg_2559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2561" n="HIAT:w" s="T650">ambi</ts>
                  <nts id="Seg_2562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2564" n="HIAT:w" s="T651">da</ts>
                  <nts id="Seg_2565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2567" n="HIAT:w" s="T652">bü</ts>
                  <nts id="Seg_2568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2570" n="HIAT:w" s="T653">ej</ts>
                  <nts id="Seg_2571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2573" n="HIAT:w" s="T654">bĭʔpi</ts>
                  <nts id="Seg_2574" n="HIAT:ip">.</nts>
                  <nts id="Seg_2575" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T664" id="Seg_2577" n="HIAT:u" s="T655">
                  <ts e="T656" id="Seg_2579" n="HIAT:w" s="T655">Dĭʔnə</ts>
                  <nts id="Seg_2580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2582" n="HIAT:w" s="T656">bɨ</ts>
                  <nts id="Seg_2583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2585" n="HIAT:w" s="T657">ulubə</ts>
                  <nts id="Seg_2586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2588" n="HIAT:w" s="T658">sajjaʔpiʔi</ts>
                  <nts id="Seg_2589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2591" n="HIAT:w" s="T659">bɨ</ts>
                  <nts id="Seg_2592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2594" n="HIAT:w" s="T660">i</ts>
                  <nts id="Seg_2595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2597" n="HIAT:w" s="T661">edəbiʔi</ts>
                  <nts id="Seg_2598" n="HIAT:ip">,</nts>
                  <nts id="Seg_2599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662.tx-PKZ.1" id="Seg_2601" n="HIAT:w" s="T662">a</ts>
                  <nts id="Seg_2602" n="HIAT:ip">_</nts>
                  <ts e="T664" id="Seg_2604" n="HIAT:w" s="T662.tx-PKZ.1">to</ts>
                  <nts id="Seg_2605" n="HIAT:ip">…</nts>
                  <nts id="Seg_2606" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T671" id="Seg_2608" n="HIAT:u" s="T664">
                  <ts e="T665" id="Seg_2610" n="HIAT:w" s="T664">Dĭgəttə</ts>
                  <nts id="Seg_2611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2613" n="HIAT:w" s="T665">dĭ</ts>
                  <nts id="Seg_2614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2616" n="HIAT:w" s="T666">koʔbdom</ts>
                  <nts id="Seg_2617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2619" n="HIAT:w" s="T667">ibi</ts>
                  <nts id="Seg_2620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2622" n="HIAT:w" s="T668">i</ts>
                  <nts id="Seg_2623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2625" n="HIAT:w" s="T669">maʔndə</ts>
                  <nts id="Seg_2626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2628" n="HIAT:w" s="T670">kambi</ts>
                  <nts id="Seg_2629" n="HIAT:ip">.</nts>
                  <nts id="Seg_2630" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T672" id="Seg_2632" n="HIAT:u" s="T671">
                  <nts id="Seg_2633" n="HIAT:ip">(</nts>
                  <nts id="Seg_2634" n="HIAT:ip">(</nts>
                  <ats e="T672" id="Seg_2635" n="HIAT:non-pho" s="T671">BRK</ats>
                  <nts id="Seg_2636" n="HIAT:ip">)</nts>
                  <nts id="Seg_2637" n="HIAT:ip">)</nts>
                  <nts id="Seg_2638" n="HIAT:ip">.</nts>
                  <nts id="Seg_2639" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T675" id="Seg_2641" n="HIAT:u" s="T672">
                  <nts id="Seg_2642" n="HIAT:ip">"</nts>
                  <ts e="T673" id="Seg_2644" n="HIAT:w" s="T672">Kaba</ts>
                  <nts id="Seg_2645" n="HIAT:ip">,</nts>
                  <nts id="Seg_2646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2648" n="HIAT:w" s="T673">ĭmbi</ts>
                  <nts id="Seg_2649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2651" n="HIAT:w" s="T674">nüjleʔbəl</ts>
                  <nts id="Seg_2652" n="HIAT:ip">?</nts>
                  <nts id="Seg_2653" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T676" id="Seg_2655" n="HIAT:u" s="T675">
                  <ts e="T676" id="Seg_2657" n="HIAT:w" s="T675">Коробочка</ts>
                  <nts id="Seg_2658" n="HIAT:ip">.</nts>
                  <nts id="Seg_2659" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T681" id="Seg_2661" n="HIAT:u" s="T676">
                  <ts e="T677" id="Seg_2663" n="HIAT:w" s="T676">A</ts>
                  <nts id="Seg_2664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2666" n="HIAT:w" s="T677">ĭmbi</ts>
                  <nts id="Seg_2667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2669" n="HIAT:w" s="T678">ej</ts>
                  <nts id="Seg_2670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2672" n="HIAT:w" s="T679">jakše</ts>
                  <nts id="Seg_2673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2675" n="HIAT:w" s="T680">nüjleʔbəl</ts>
                  <nts id="Seg_2676" n="HIAT:ip">?</nts>
                  <nts id="Seg_2677" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T686" id="Seg_2679" n="HIAT:u" s="T681">
                  <ts e="T682" id="Seg_2681" n="HIAT:w" s="T681">Da</ts>
                  <nts id="Seg_2682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2684" n="HIAT:w" s="T682">нельзя</ts>
                  <nts id="Seg_2685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2687" n="HIAT:w" s="T683">jakše</ts>
                  <nts id="Seg_2688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2689" n="HIAT:ip">(</nts>
                  <ts e="T685" id="Seg_2691" n="HIAT:w" s="T684">nüjn-</ts>
                  <nts id="Seg_2692" n="HIAT:ip">)</nts>
                  <nts id="Seg_2693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2695" n="HIAT:w" s="T685">nüjnəsʼtə</ts>
                  <nts id="Seg_2696" n="HIAT:ip">.</nts>
                  <nts id="Seg_2697" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T688" id="Seg_2699" n="HIAT:u" s="T686">
                  <ts e="T687" id="Seg_2701" n="HIAT:w" s="T686">Kuiol</ts>
                  <nts id="Seg_2702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2703" n="HIAT:ip">(</nts>
                  <ts e="T688" id="Seg_2705" n="HIAT:w" s="T687">barlaʔina</ts>
                  <nts id="Seg_2706" n="HIAT:ip">)</nts>
                  <nts id="Seg_2707" n="HIAT:ip">"</nts>
                  <nts id="Seg_2708" n="HIAT:ip">.</nts>
                  <nts id="Seg_2709" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T689" id="Seg_2711" n="HIAT:u" s="T688">
                  <nts id="Seg_2712" n="HIAT:ip">(</nts>
                  <nts id="Seg_2713" n="HIAT:ip">(</nts>
                  <ats e="T689" id="Seg_2714" n="HIAT:non-pho" s="T688">BRK</ats>
                  <nts id="Seg_2715" n="HIAT:ip">)</nts>
                  <nts id="Seg_2716" n="HIAT:ip">)</nts>
                  <nts id="Seg_2717" n="HIAT:ip">.</nts>
                  <nts id="Seg_2718" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T694" id="Seg_2720" n="HIAT:u" s="T689">
                  <ts e="T690" id="Seg_2722" n="HIAT:w" s="T689">Onʼiʔ</ts>
                  <nts id="Seg_2723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2725" n="HIAT:w" s="T690">kuza</ts>
                  <nts id="Seg_2726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2727" n="HIAT:ip">(</nts>
                  <ts e="T692" id="Seg_2729" n="HIAT:w" s="T691">ibɨ-</ts>
                  <nts id="Seg_2730" n="HIAT:ip">)</nts>
                  <nts id="Seg_2731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2733" n="HIAT:w" s="T692">ibi</ts>
                  <nts id="Seg_2734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2736" n="HIAT:w" s="T693">tʼerməndə</ts>
                  <nts id="Seg_2737" n="HIAT:ip">.</nts>
                  <nts id="Seg_2738" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T703" id="Seg_2740" n="HIAT:u" s="T694">
                  <nts id="Seg_2741" n="HIAT:ip">(</nts>
                  <ts e="T695" id="Seg_2743" n="HIAT:w" s="T694">A=</ts>
                  <nts id="Seg_2744" n="HIAT:ip">)</nts>
                  <nts id="Seg_2745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2747" n="HIAT:w" s="T695">A</ts>
                  <nts id="Seg_2748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2750" n="HIAT:w" s="T696">onʼiʔ</ts>
                  <nts id="Seg_2751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2753" n="HIAT:w" s="T697">kuza</ts>
                  <nts id="Seg_2754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2756" n="HIAT:w" s="T698">ugandə</ts>
                  <nts id="Seg_2757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2759" n="HIAT:w" s="T699">aktʼa</ts>
                  <nts id="Seg_2760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2762" n="HIAT:w" s="T700">iʔgö</ts>
                  <nts id="Seg_2763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2764" n="HIAT:ip">(</nts>
                  <ts e="T702" id="Seg_2766" n="HIAT:w" s="T701">был=</ts>
                  <nts id="Seg_2767" n="HIAT:ip">)</nts>
                  <nts id="Seg_2768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2770" n="HIAT:w" s="T702">ibi</ts>
                  <nts id="Seg_2771" n="HIAT:ip">.</nts>
                  <nts id="Seg_2772" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T707" id="Seg_2774" n="HIAT:u" s="T703">
                  <ts e="T704" id="Seg_2776" n="HIAT:w" s="T703">I</ts>
                  <nts id="Seg_2777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2779" n="HIAT:w" s="T704">oldʼa</ts>
                  <nts id="Seg_2780" n="HIAT:ip">,</nts>
                  <nts id="Seg_2781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2783" n="HIAT:w" s="T705">i</ts>
                  <nts id="Seg_2784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2786" n="HIAT:w" s="T706">ipek</ts>
                  <nts id="Seg_2787" n="HIAT:ip">.</nts>
                  <nts id="Seg_2788" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T715" id="Seg_2790" n="HIAT:u" s="T707">
                  <nts id="Seg_2791" n="HIAT:ip">(</nts>
                  <ts e="T708" id="Seg_2793" n="HIAT:w" s="T707">Dĭ=</ts>
                  <nts id="Seg_2794" n="HIAT:ip">)</nts>
                  <nts id="Seg_2795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2797" n="HIAT:w" s="T708">Dĭn</ts>
                  <nts id="Seg_2798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2800" n="HIAT:w" s="T709">kuzanə</ts>
                  <nts id="Seg_2801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2803" n="HIAT:w" s="T710">ibi</ts>
                  <nts id="Seg_2804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2806" n="HIAT:w" s="T711">tʼerməndə</ts>
                  <nts id="Seg_2807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2809" n="HIAT:w" s="T712">i</ts>
                  <nts id="Seg_2810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2811" n="HIAT:ip">(</nts>
                  <ts e="T714" id="Seg_2813" n="HIAT:w" s="T713">ko-</ts>
                  <nts id="Seg_2814" n="HIAT:ip">)</nts>
                  <nts id="Seg_2815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2817" n="HIAT:w" s="T714">konnambi</ts>
                  <nts id="Seg_2818" n="HIAT:ip">.</nts>
                  <nts id="Seg_2819" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T718" id="Seg_2821" n="HIAT:u" s="T715">
                  <ts e="T716" id="Seg_2823" n="HIAT:w" s="T715">Dĭ</ts>
                  <nts id="Seg_2824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2826" n="HIAT:w" s="T716">bar</ts>
                  <nts id="Seg_2827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2829" n="HIAT:w" s="T717">dʼorlaʔbə</ts>
                  <nts id="Seg_2830" n="HIAT:ip">.</nts>
                  <nts id="Seg_2831" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T724" id="Seg_2833" n="HIAT:u" s="T718">
                  <ts e="T719" id="Seg_2835" n="HIAT:w" s="T718">A</ts>
                  <nts id="Seg_2836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2838" n="HIAT:w" s="T719">pʼetux</ts>
                  <nts id="Seg_2839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_2841" n="HIAT:w" s="T720">mălia:</ts>
                  <nts id="Seg_2842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_2844" n="HIAT:w" s="T721">Ĭmbi</ts>
                  <nts id="Seg_2845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2846" n="HIAT:ip">(</nts>
                  <ts e="T723" id="Seg_2848" n="HIAT:w" s="T722">dʼorlaʔbəl</ts>
                  <nts id="Seg_2849" n="HIAT:ip">)</nts>
                  <nts id="Seg_2850" n="HIAT:ip">,</nts>
                  <nts id="Seg_2851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2853" n="HIAT:w" s="T723">kanžəbəj</ts>
                  <nts id="Seg_2854" n="HIAT:ip">!</nts>
                  <nts id="Seg_2855" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T727" id="Seg_2857" n="HIAT:u" s="T724">
                  <ts e="T725" id="Seg_2859" n="HIAT:w" s="T724">Tüjö</ts>
                  <nts id="Seg_2860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_2862" n="HIAT:w" s="T725">ibəj</ts>
                  <nts id="Seg_2863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2865" n="HIAT:w" s="T726">tʼerməndə</ts>
                  <nts id="Seg_2866" n="HIAT:ip">.</nts>
                  <nts id="Seg_2867" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T730" id="Seg_2869" n="HIAT:u" s="T727">
                  <ts e="T728" id="Seg_2871" n="HIAT:w" s="T727">Dĭ</ts>
                  <nts id="Seg_2872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2874" n="HIAT:w" s="T728">lattɨ</ts>
                  <nts id="Seg_2875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2877" n="HIAT:w" s="T729">kambiʔi</ts>
                  <nts id="Seg_2878" n="HIAT:ip">.</nts>
                  <nts id="Seg_2879" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T736" id="Seg_2881" n="HIAT:u" s="T730">
                  <ts e="T731" id="Seg_2883" n="HIAT:w" s="T730">Pʼetux</ts>
                  <nts id="Seg_2884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_2886" n="HIAT:w" s="T731">kirgarlaʔbə:</ts>
                  <nts id="Seg_2887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2889" n="HIAT:w" s="T732">Deʔ</ts>
                  <nts id="Seg_2890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2892" n="HIAT:w" s="T733">tʼermən</ts>
                  <nts id="Seg_2893" n="HIAT:ip">,</nts>
                  <nts id="Seg_2894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_2896" n="HIAT:w" s="T734">deʔ</ts>
                  <nts id="Seg_2897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_2899" n="HIAT:w" s="T735">tʼermən</ts>
                  <nts id="Seg_2900" n="HIAT:ip">!</nts>
                  <nts id="Seg_2901" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T744" id="Seg_2903" n="HIAT:u" s="T736">
                  <nts id="Seg_2904" n="HIAT:ip">(</nts>
                  <ts e="T737" id="Seg_2906" n="HIAT:w" s="T736">D-</ts>
                  <nts id="Seg_2907" n="HIAT:ip">)</nts>
                  <nts id="Seg_2908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_2910" n="HIAT:w" s="T737">Dĭ</ts>
                  <nts id="Seg_2911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_2913" n="HIAT:w" s="T738">kuza</ts>
                  <nts id="Seg_2914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_2916" n="HIAT:w" s="T739">măndə:</ts>
                  <nts id="Seg_2917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2919" n="HIAT:w" s="T740">Barəʔtə</ts>
                  <nts id="Seg_2920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_2922" n="HIAT:w" s="T741">dĭm</ts>
                  <nts id="Seg_2923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2924" n="HIAT:ip">(</nts>
                  <ts e="T743" id="Seg_2926" n="HIAT:w" s="T742">ineziʔ</ts>
                  <nts id="Seg_2927" n="HIAT:ip">)</nts>
                  <nts id="Seg_2928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_2930" n="HIAT:w" s="T743">ineʔinə</ts>
                  <nts id="Seg_2931" n="HIAT:ip">.</nts>
                  <nts id="Seg_2932" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T745" id="Seg_2934" n="HIAT:u" s="T744">
                  <ts e="T745" id="Seg_2936" n="HIAT:w" s="T744">Barəʔluʔpi</ts>
                  <nts id="Seg_2937" n="HIAT:ip">.</nts>
                  <nts id="Seg_2938" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T755" id="Seg_2940" n="HIAT:u" s="T745">
                  <ts e="T746" id="Seg_2942" n="HIAT:w" s="T745">Dĭ</ts>
                  <nts id="Seg_2943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_2945" n="HIAT:w" s="T746">dĭʔə</ts>
                  <nts id="Seg_2946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_2948" n="HIAT:w" s="T747">šobi</ts>
                  <nts id="Seg_2949" n="HIAT:ip">,</nts>
                  <nts id="Seg_2950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_2952" n="HIAT:w" s="T748">bazoʔ</ts>
                  <nts id="Seg_2953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_2955" n="HIAT:w" s="T749">kirgarlaʔbə:</ts>
                  <nts id="Seg_2956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2957" n="HIAT:ip">(</nts>
                  <ts e="T751" id="Seg_2959" n="HIAT:w" s="T750">Отда-</ts>
                  <nts id="Seg_2960" n="HIAT:ip">)</nts>
                  <nts id="Seg_2961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_2963" n="HIAT:w" s="T751">Deʔtə</ts>
                  <nts id="Seg_2964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_2966" n="HIAT:w" s="T752">tʼerməndə</ts>
                  <nts id="Seg_2967" n="HIAT:ip">,</nts>
                  <nts id="Seg_2968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_2970" n="HIAT:w" s="T753">deʔtə</ts>
                  <nts id="Seg_2971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_2973" n="HIAT:w" s="T754">tʼerməndə</ts>
                  <nts id="Seg_2974" n="HIAT:ip">!</nts>
                  <nts id="Seg_2975" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T760" id="Seg_2977" n="HIAT:u" s="T755">
                  <ts e="T756" id="Seg_2979" n="HIAT:w" s="T755">Dĭgəttə</ts>
                  <nts id="Seg_2980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_2982" n="HIAT:w" s="T756">Barəʔtə</ts>
                  <nts id="Seg_2983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2984" n="HIAT:ip">(</nts>
                  <ts e="T758" id="Seg_2986" n="HIAT:w" s="T757">dĭ-</ts>
                  <nts id="Seg_2987" n="HIAT:ip">)</nts>
                  <nts id="Seg_2988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_2990" n="HIAT:w" s="T758">dĭm</ts>
                  <nts id="Seg_2991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_2993" n="HIAT:w" s="T759">tüžöjdə</ts>
                  <nts id="Seg_2994" n="HIAT:ip">.</nts>
                  <nts id="Seg_2995" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T762" id="Seg_2997" n="HIAT:u" s="T760">
                  <ts e="T761" id="Seg_2999" n="HIAT:w" s="T760">Barəʔluʔpi</ts>
                  <nts id="Seg_3000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_3002" n="HIAT:w" s="T761">tüžöjdə</ts>
                  <nts id="Seg_3003" n="HIAT:ip">.</nts>
                  <nts id="Seg_3004" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T767" id="Seg_3006" n="HIAT:u" s="T762">
                  <ts e="T763" id="Seg_3008" n="HIAT:w" s="T762">Dĭ</ts>
                  <nts id="Seg_3009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_3011" n="HIAT:w" s="T763">tüžöjgən</ts>
                  <nts id="Seg_3012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3013" n="HIAT:ip">(</nts>
                  <ts e="T765" id="Seg_3015" n="HIAT:w" s="T764">tĭ-</ts>
                  <nts id="Seg_3016" n="HIAT:ip">)</nts>
                  <nts id="Seg_3017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_3019" n="HIAT:w" s="T765">bazoʔ</ts>
                  <nts id="Seg_3020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_3022" n="HIAT:w" s="T766">šobi</ts>
                  <nts id="Seg_3023" n="HIAT:ip">.</nts>
                  <nts id="Seg_3024" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T777" id="Seg_3026" n="HIAT:u" s="T767">
                  <ts e="T768" id="Seg_3028" n="HIAT:w" s="T767">Dĭgəttə</ts>
                  <nts id="Seg_3029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_3031" n="HIAT:w" s="T768">bazoʔ</ts>
                  <nts id="Seg_3032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_3034" n="HIAT:w" s="T769">măndə:</ts>
                  <nts id="Seg_3035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3036" n="HIAT:ip">(</nts>
                  <ts e="T771" id="Seg_3038" n="HIAT:w" s="T770">Дай</ts>
                  <nts id="Seg_3039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_3041" n="HIAT:w" s="T771">tʼe-</ts>
                  <nts id="Seg_3042" n="HIAT:ip">)</nts>
                  <nts id="Seg_3043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_3045" n="HIAT:w" s="T772">Deʔtə</ts>
                  <nts id="Seg_3046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_3048" n="HIAT:w" s="T773">tʼerməndə</ts>
                  <nts id="Seg_3049" n="HIAT:ip">,</nts>
                  <nts id="Seg_3050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_3052" n="HIAT:w" s="T774">deʔtə</ts>
                  <nts id="Seg_3053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_3055" n="HIAT:w" s="T775">tʼerməndə</ts>
                  <nts id="Seg_3056" n="HIAT:ip">!</nts>
                  <nts id="Seg_3057" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T779" id="Seg_3059" n="HIAT:u" s="T777">
                  <ts e="T778" id="Seg_3061" n="HIAT:w" s="T777">Barəʔtə</ts>
                  <nts id="Seg_3062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_3064" n="HIAT:w" s="T778">ulardə</ts>
                  <nts id="Seg_3065" n="HIAT:ip">!</nts>
                  <nts id="Seg_3066" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T781" id="Seg_3068" n="HIAT:u" s="T779">
                  <ts e="T780" id="Seg_3070" n="HIAT:w" s="T779">Barəʔluʔpi</ts>
                  <nts id="Seg_3071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_3073" n="HIAT:w" s="T780">ulardə</ts>
                  <nts id="Seg_3074" n="HIAT:ip">.</nts>
                  <nts id="Seg_3075" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T790" id="Seg_3077" n="HIAT:u" s="T781">
                  <ts e="T782" id="Seg_3079" n="HIAT:w" s="T781">Dĭgəttə</ts>
                  <nts id="Seg_3080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_3082" n="HIAT:w" s="T782">bazoʔ</ts>
                  <nts id="Seg_3083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_3085" n="HIAT:w" s="T783">šobi</ts>
                  <nts id="Seg_3086" n="HIAT:ip">,</nts>
                  <nts id="Seg_3087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_3089" n="HIAT:w" s="T784">bazoʔ</ts>
                  <nts id="Seg_3090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_3092" n="HIAT:w" s="T785">kirgarlaʔbə:</ts>
                  <nts id="Seg_3093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_3095" n="HIAT:w" s="T786">Deʔtə</ts>
                  <nts id="Seg_3096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_3098" n="HIAT:w" s="T787">tʼerməndə</ts>
                  <nts id="Seg_3099" n="HIAT:ip">,</nts>
                  <nts id="Seg_3100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_3102" n="HIAT:w" s="T788">deʔtə</ts>
                  <nts id="Seg_3103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_3105" n="HIAT:w" s="T789">tʼerməndə</ts>
                  <nts id="Seg_3106" n="HIAT:ip">!</nts>
                  <nts id="Seg_3107" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T795" id="Seg_3109" n="HIAT:u" s="T790">
                  <ts e="T791" id="Seg_3111" n="HIAT:w" s="T790">Dĭgəttə</ts>
                  <nts id="Seg_3112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_3114" n="HIAT:w" s="T791">măndə:</ts>
                  <nts id="Seg_3115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T793" id="Seg_3117" n="HIAT:w" s="T792">Barəʔtə</ts>
                  <nts id="Seg_3118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_3120" n="HIAT:w" s="T793">dĭm</ts>
                  <nts id="Seg_3121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_3123" n="HIAT:w" s="T794">bünə</ts>
                  <nts id="Seg_3124" n="HIAT:ip">!</nts>
                  <nts id="Seg_3125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T797" id="Seg_3127" n="HIAT:u" s="T795">
                  <ts e="T796" id="Seg_3129" n="HIAT:w" s="T795">Dĭm</ts>
                  <nts id="Seg_3130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797" id="Seg_3132" n="HIAT:w" s="T796">barəʔluʔpi</ts>
                  <nts id="Seg_3133" n="HIAT:ip">.</nts>
                  <nts id="Seg_3134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T802" id="Seg_3136" n="HIAT:u" s="T797">
                  <ts e="T798" id="Seg_3138" n="HIAT:w" s="T797">A</ts>
                  <nts id="Seg_3139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_3141" n="HIAT:w" s="T798">dĭ</ts>
                  <nts id="Seg_3142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_3144" n="HIAT:w" s="T799">măndə:</ts>
                  <nts id="Seg_3145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_3147" n="HIAT:w" s="T800">Kötenbə</ts>
                  <nts id="Seg_3148" n="HIAT:ip">…</nts>
                  <nts id="Seg_3149" n="HIAT:ip">"</nts>
                  <nts id="Seg_3150" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T803" id="Seg_3152" n="HIAT:u" s="T802">
                  <nts id="Seg_3153" n="HIAT:ip">(</nts>
                  <nts id="Seg_3154" n="HIAT:ip">(</nts>
                  <ats e="T803" id="Seg_3155" n="HIAT:non-pho" s="T802">BRK</ats>
                  <nts id="Seg_3156" n="HIAT:ip">)</nts>
                  <nts id="Seg_3157" n="HIAT:ip">)</nts>
                  <nts id="Seg_3158" n="HIAT:ip">.</nts>
                  <nts id="Seg_3159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T816" id="Seg_3161" n="HIAT:u" s="T803">
                  <ts e="T804" id="Seg_3163" n="HIAT:w" s="T803">Barəʔluʔpi</ts>
                  <nts id="Seg_3164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3165" n="HIAT:ip">(</nts>
                  <ts e="T805" id="Seg_3167" n="HIAT:w" s="T804">dĭ=</ts>
                  <nts id="Seg_3168" n="HIAT:ip">)</nts>
                  <nts id="Seg_3169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_3171" n="HIAT:w" s="T805">dĭm</ts>
                  <nts id="Seg_3172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_3174" n="HIAT:w" s="T806">bünə</ts>
                  <nts id="Seg_3175" n="HIAT:ip">,</nts>
                  <nts id="Seg_3176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_3178" n="HIAT:w" s="T807">a</ts>
                  <nts id="Seg_3179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_3181" n="HIAT:w" s="T808">dĭ</ts>
                  <nts id="Seg_3182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_3184" n="HIAT:w" s="T809">kirgarlaʔbə:</ts>
                  <nts id="Seg_3185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_3187" n="HIAT:w" s="T810">Kötenbə</ts>
                  <nts id="Seg_3188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_3190" n="HIAT:w" s="T811">bĭdeʔ</ts>
                  <nts id="Seg_3191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T813" id="Seg_3193" n="HIAT:w" s="T812">bü</ts>
                  <nts id="Seg_3194" n="HIAT:ip">,</nts>
                  <nts id="Seg_3195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T814" id="Seg_3197" n="HIAT:w" s="T813">kötenbə</ts>
                  <nts id="Seg_3198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_3200" n="HIAT:w" s="T814">bĭdeʔ</ts>
                  <nts id="Seg_3201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_3203" n="HIAT:w" s="T815">bü</ts>
                  <nts id="Seg_3204" n="HIAT:ip">.</nts>
                  <nts id="Seg_3205" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T822" id="Seg_3207" n="HIAT:u" s="T816">
                  <ts e="T817" id="Seg_3209" n="HIAT:w" s="T816">Kötenbə</ts>
                  <nts id="Seg_3210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_3212" n="HIAT:w" s="T817">bar</ts>
                  <nts id="Seg_3213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_3215" n="HIAT:w" s="T818">bü</ts>
                  <nts id="Seg_3216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T820" id="Seg_3218" n="HIAT:w" s="T819">bĭtluʔpi</ts>
                  <nts id="Seg_3219" n="HIAT:ip">,</nts>
                  <nts id="Seg_3220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T821" id="Seg_3222" n="HIAT:w" s="T820">bazoʔ</ts>
                  <nts id="Seg_3223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_3225" n="HIAT:w" s="T821">šobi</ts>
                  <nts id="Seg_3226" n="HIAT:ip">.</nts>
                  <nts id="Seg_3227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T826" id="Seg_3229" n="HIAT:u" s="T822">
                  <ts e="T823" id="Seg_3231" n="HIAT:w" s="T822">Deʔtə</ts>
                  <nts id="Seg_3232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T824" id="Seg_3234" n="HIAT:w" s="T823">tʼerməndə</ts>
                  <nts id="Seg_3235" n="HIAT:ip">,</nts>
                  <nts id="Seg_3236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_3238" n="HIAT:w" s="T824">deʔtə</ts>
                  <nts id="Seg_3239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_3241" n="HIAT:w" s="T825">tʼerməndə</ts>
                  <nts id="Seg_3242" n="HIAT:ip">!</nts>
                  <nts id="Seg_3243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T838" id="Seg_3245" n="HIAT:u" s="T826">
                  <ts e="T827" id="Seg_3247" n="HIAT:w" s="T826">Dĭgəttə</ts>
                  <nts id="Seg_3248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T828" id="Seg_3250" n="HIAT:w" s="T827">dĭ</ts>
                  <nts id="Seg_3251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T829" id="Seg_3253" n="HIAT:w" s="T828">kuza</ts>
                  <nts id="Seg_3254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_3256" n="HIAT:w" s="T829">măndə:</ts>
                  <nts id="Seg_3257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T831" id="Seg_3259" n="HIAT:w" s="T830">Šü</ts>
                  <nts id="Seg_3260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3261" n="HIAT:ip">(</nts>
                  <ts e="T832" id="Seg_3263" n="HIAT:w" s="T831">eŋg-</ts>
                  <nts id="Seg_3264" n="HIAT:ip">)</nts>
                  <nts id="Seg_3265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_3267" n="HIAT:w" s="T832">eŋgeʔ</ts>
                  <nts id="Seg_3268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_3270" n="HIAT:w" s="T833">urgo</ts>
                  <nts id="Seg_3271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_3273" n="HIAT:w" s="T834">i</ts>
                  <nts id="Seg_3274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_3276" n="HIAT:w" s="T835">barəʔtə</ts>
                  <nts id="Seg_3277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T837" id="Seg_3279" n="HIAT:w" s="T836">dĭm</ts>
                  <nts id="Seg_3280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_3282" n="HIAT:w" s="T837">šünə</ts>
                  <nts id="Seg_3283" n="HIAT:ip">.</nts>
                  <nts id="Seg_3284" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T853" id="Seg_3286" n="HIAT:u" s="T838">
                  <ts e="T839" id="Seg_3288" n="HIAT:w" s="T838">Dĭgəttə</ts>
                  <nts id="Seg_3289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_3291" n="HIAT:w" s="T839">embiʔi</ts>
                  <nts id="Seg_3292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T841" id="Seg_3294" n="HIAT:w" s="T840">šü</ts>
                  <nts id="Seg_3295" n="HIAT:ip">,</nts>
                  <nts id="Seg_3296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_3298" n="HIAT:w" s="T841">barəʔluʔpi</ts>
                  <nts id="Seg_3299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_3301" n="HIAT:w" s="T842">dĭ</ts>
                  <nts id="Seg_3302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T844" id="Seg_3304" n="HIAT:w" s="T843">pʼetugəm</ts>
                  <nts id="Seg_3305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_3307" n="HIAT:w" s="T844">šünə</ts>
                  <nts id="Seg_3308" n="HIAT:ip">,</nts>
                  <nts id="Seg_3309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_3311" n="HIAT:w" s="T845">dibər</ts>
                  <nts id="Seg_3312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_3314" n="HIAT:w" s="T846">măndə:</ts>
                  <nts id="Seg_3315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T848" id="Seg_3317" n="HIAT:w" s="T847">Kămnaʔ</ts>
                  <nts id="Seg_3318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T849" id="Seg_3320" n="HIAT:w" s="T848">kötenbə</ts>
                  <nts id="Seg_3321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T850" id="Seg_3323" n="HIAT:w" s="T849">bü</ts>
                  <nts id="Seg_3324" n="HIAT:ip">,</nts>
                  <nts id="Seg_3325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T851" id="Seg_3327" n="HIAT:w" s="T850">kămnaʔ</ts>
                  <nts id="Seg_3328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_3330" n="HIAT:w" s="T851">kötenbə</ts>
                  <nts id="Seg_3331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_3333" n="HIAT:w" s="T852">bü</ts>
                  <nts id="Seg_3334" n="HIAT:ip">!</nts>
                  <nts id="Seg_3335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T856" id="Seg_3337" n="HIAT:u" s="T853">
                  <ts e="T854" id="Seg_3339" n="HIAT:w" s="T853">Kak</ts>
                  <nts id="Seg_3340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T855" id="Seg_3342" n="HIAT:w" s="T854">bü</ts>
                  <nts id="Seg_3343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856" id="Seg_3345" n="HIAT:w" s="T855">kambi</ts>
                  <nts id="Seg_3346" n="HIAT:ip">!</nts>
                  <nts id="Seg_3347" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T860" id="Seg_3349" n="HIAT:u" s="T856">
                  <ts e="T857" id="Seg_3351" n="HIAT:w" s="T856">Bar</ts>
                  <nts id="Seg_3352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T858" id="Seg_3354" n="HIAT:w" s="T857">ĭmbi</ts>
                  <nts id="Seg_3355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_3357" n="HIAT:w" s="T858">bar</ts>
                  <nts id="Seg_3358" n="HIAT:ip">…</nts>
                  <nts id="Seg_3359" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T861" id="Seg_3361" n="HIAT:u" s="T860">
                  <nts id="Seg_3362" n="HIAT:ip">(</nts>
                  <nts id="Seg_3363" n="HIAT:ip">(</nts>
                  <ats e="T861" id="Seg_3364" n="HIAT:non-pho" s="T860">BRK</ats>
                  <nts id="Seg_3365" n="HIAT:ip">)</nts>
                  <nts id="Seg_3366" n="HIAT:ip">)</nts>
                  <nts id="Seg_3367" n="HIAT:ip">.</nts>
                  <nts id="Seg_3368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T877" id="Seg_3370" n="HIAT:u" s="T861">
                  <ts e="T862" id="Seg_3372" n="HIAT:w" s="T861">Dĭgəttə</ts>
                  <nts id="Seg_3373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T863" id="Seg_3375" n="HIAT:w" s="T862">bü</ts>
                  <nts id="Seg_3376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864" id="Seg_3378" n="HIAT:w" s="T863">ugandə</ts>
                  <nts id="Seg_3379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T865" id="Seg_3381" n="HIAT:w" s="T864">iʔgö</ts>
                  <nts id="Seg_3382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3383" n="HIAT:ip">(</nts>
                  <ts e="T866" id="Seg_3385" n="HIAT:w" s="T865">mʼaŋi-</ts>
                  <nts id="Seg_3386" n="HIAT:ip">)</nts>
                  <nts id="Seg_3387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T867" id="Seg_3389" n="HIAT:w" s="T866">mʼambi</ts>
                  <nts id="Seg_3390" n="HIAT:ip">,</nts>
                  <nts id="Seg_3391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T868" id="Seg_3393" n="HIAT:w" s="T867">dĭ</ts>
                  <nts id="Seg_3394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T869" id="Seg_3396" n="HIAT:w" s="T868">kuza</ts>
                  <nts id="Seg_3397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T870" id="Seg_3399" n="HIAT:w" s="T869">măndə:</ts>
                  <nts id="Seg_3400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T871" id="Seg_3402" n="HIAT:w" s="T870">Iʔ</ts>
                  <nts id="Seg_3403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T872" id="Seg_3405" n="HIAT:w" s="T871">tʼerməndə</ts>
                  <nts id="Seg_3406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T873" id="Seg_3408" n="HIAT:w" s="T872">i</ts>
                  <nts id="Seg_3409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T874" id="Seg_3411" n="HIAT:w" s="T873">kanaʔ</ts>
                  <nts id="Seg_3412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3413" n="HIAT:ip">(</nts>
                  <ts e="T875" id="Seg_3415" n="HIAT:w" s="T874">döʔə</ts>
                  <nts id="Seg_3416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_3418" n="HIAT:w" s="T875">döʔə=</ts>
                  <nts id="Seg_3419" n="HIAT:ip">)</nts>
                  <nts id="Seg_3420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T877" id="Seg_3422" n="HIAT:w" s="T876">döʔnə</ts>
                  <nts id="Seg_3423" n="HIAT:ip">!</nts>
                  <nts id="Seg_3424" n="HIAT:ip">"</nts>
                  <nts id="Seg_3425" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T878" id="Seg_3427" n="HIAT:u" s="T877">
                  <nts id="Seg_3428" n="HIAT:ip">(</nts>
                  <nts id="Seg_3429" n="HIAT:ip">(</nts>
                  <ats e="T878" id="Seg_3430" n="HIAT:non-pho" s="T877">BRK</ats>
                  <nts id="Seg_3431" n="HIAT:ip">)</nts>
                  <nts id="Seg_3432" n="HIAT:ip">)</nts>
                  <nts id="Seg_3433" n="HIAT:ip">.</nts>
                  <nts id="Seg_3434" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T879" id="Seg_3436" n="HIAT:u" s="T878">
                  <ts e="T879" id="Seg_3438" n="HIAT:w" s="T878">Погоди</ts>
                  <nts id="Seg_3439" n="HIAT:ip">!</nts>
                  <nts id="Seg_3440" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T880" id="Seg_3442" n="HIAT:u" s="T879">
                  <nts id="Seg_3443" n="HIAT:ip">(</nts>
                  <nts id="Seg_3444" n="HIAT:ip">(</nts>
                  <ats e="T880" id="Seg_3445" n="HIAT:non-pho" s="T879">BRK</ats>
                  <nts id="Seg_3446" n="HIAT:ip">)</nts>
                  <nts id="Seg_3447" n="HIAT:ip">)</nts>
                  <nts id="Seg_3448" n="HIAT:ip">.</nts>
                  <nts id="Seg_3449" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T889" id="Seg_3451" n="HIAT:u" s="T880">
                  <nts id="Seg_3452" n="HIAT:ip">(</nts>
                  <ts e="T881" id="Seg_3454" n="HIAT:w" s="T880">Xisəʔi</ts>
                  <nts id="Seg_3455" n="HIAT:ip">)</nts>
                  <nts id="Seg_3456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T882" id="Seg_3458" n="HIAT:w" s="T881">onʼiʔ</ts>
                  <nts id="Seg_3459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3460" n="HIAT:ip">(</nts>
                  <ts e="T883" id="Seg_3462" n="HIAT:w" s="T882">nul-</ts>
                  <nts id="Seg_3463" n="HIAT:ip">)</nts>
                  <nts id="Seg_3464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3465" n="HIAT:ip">(</nts>
                  <ts e="T884" id="Seg_3467" n="HIAT:w" s="T883">nubiʔi</ts>
                  <nts id="Seg_3468" n="HIAT:ip">)</nts>
                  <nts id="Seg_3469" n="HIAT:ip">,</nts>
                  <nts id="Seg_3470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T885" id="Seg_3472" n="HIAT:w" s="T884">măn</ts>
                  <nts id="Seg_3473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T886" id="Seg_3475" n="HIAT:w" s="T885">dĭzeŋ</ts>
                  <nts id="Seg_3476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3477" n="HIAT:ip">(</nts>
                  <ts e="T887" id="Seg_3479" n="HIAT:w" s="T886">oʔlu-</ts>
                  <nts id="Seg_3480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T888" id="Seg_3482" n="HIAT:w" s="T887">ole-</ts>
                  <nts id="Seg_3483" n="HIAT:ip">)</nts>
                  <nts id="Seg_3484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3485" n="HIAT:ip">(</nts>
                  <ts e="T889" id="Seg_3487" n="HIAT:w" s="T888">öʔlubiem</ts>
                  <nts id="Seg_3488" n="HIAT:ip">)</nts>
                  <nts id="Seg_3489" n="HIAT:ip">.</nts>
                  <nts id="Seg_3490" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T892" id="Seg_3492" n="HIAT:u" s="T889">
                  <ts e="T890" id="Seg_3494" n="HIAT:w" s="T889">Dĭgəttə</ts>
                  <nts id="Seg_3495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T891" id="Seg_3497" n="HIAT:w" s="T890">dĭzeŋ</ts>
                  <nts id="Seg_3498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T892" id="Seg_3500" n="HIAT:w" s="T891">nuʔməluʔpiʔi</ts>
                  <nts id="Seg_3501" n="HIAT:ip">.</nts>
                  <nts id="Seg_3502" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T896" id="Seg_3504" n="HIAT:u" s="T892">
                  <ts e="T893" id="Seg_3506" n="HIAT:w" s="T892">A</ts>
                  <nts id="Seg_3507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T894" id="Seg_3509" n="HIAT:w" s="T893">dĭzeŋ</ts>
                  <nts id="Seg_3510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T895" id="Seg_3512" n="HIAT:w" s="T894">ej</ts>
                  <nts id="Seg_3513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T896" id="Seg_3515" n="HIAT:w" s="T895">nuʔməlieʔi</ts>
                  <nts id="Seg_3516" n="HIAT:ip">.</nts>
                  <nts id="Seg_3517" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T898" id="Seg_3519" n="HIAT:u" s="T896">
                  <ts e="T897" id="Seg_3521" n="HIAT:w" s="T896">ɨrɨ</ts>
                  <nts id="Seg_3522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898" id="Seg_3524" n="HIAT:w" s="T897">molambiʔi</ts>
                  <nts id="Seg_3525" n="HIAT:ip">.</nts>
                  <nts id="Seg_3526" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T899" id="Seg_3528" n="HIAT:u" s="T898">
                  <nts id="Seg_3529" n="HIAT:ip">(</nts>
                  <nts id="Seg_3530" n="HIAT:ip">(</nts>
                  <ats e="T899" id="Seg_3531" n="HIAT:non-pho" s="T898">BRK</ats>
                  <nts id="Seg_3532" n="HIAT:ip">)</nts>
                  <nts id="Seg_3533" n="HIAT:ip">)</nts>
                  <nts id="Seg_3534" n="HIAT:ip">.</nts>
                  <nts id="Seg_3535" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T905" id="Seg_3537" n="HIAT:u" s="T899">
                  <ts e="T900" id="Seg_3539" n="HIAT:w" s="T899">Taldʼen</ts>
                  <nts id="Seg_3540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T901" id="Seg_3542" n="HIAT:w" s="T900">măn</ts>
                  <nts id="Seg_3543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T902" id="Seg_3545" n="HIAT:w" s="T901">meim</ts>
                  <nts id="Seg_3546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T903" id="Seg_3548" n="HIAT:w" s="T902">ej</ts>
                  <nts id="Seg_3549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1025" id="Seg_3551" n="HIAT:w" s="T903">kalla</ts>
                  <nts id="Seg_3552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T904" id="Seg_3554" n="HIAT:w" s="T1025">dʼürbi</ts>
                  <nts id="Seg_3555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T905" id="Seg_3557" n="HIAT:w" s="T904">maːʔndə</ts>
                  <nts id="Seg_3558" n="HIAT:ip">.</nts>
                  <nts id="Seg_3559" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T914" id="Seg_3561" n="HIAT:u" s="T905">
                  <ts e="T906" id="Seg_3563" n="HIAT:w" s="T905">Măn</ts>
                  <nts id="Seg_3564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T907" id="Seg_3566" n="HIAT:w" s="T906">mĭmbiem</ts>
                  <nts id="Seg_3567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T908" id="Seg_3569" n="HIAT:w" s="T907">i</ts>
                  <nts id="Seg_3570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909" id="Seg_3572" n="HIAT:w" s="T908">dĭzi</ts>
                  <nts id="Seg_3573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T910" id="Seg_3575" n="HIAT:w" s="T909">ej</ts>
                  <nts id="Seg_3576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T911" id="Seg_3578" n="HIAT:w" s="T910">pănarbiam</ts>
                  <nts id="Seg_3579" n="HIAT:ip">,</nts>
                  <nts id="Seg_3580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T912" id="Seg_3582" n="HIAT:w" s="T911">udam</ts>
                  <nts id="Seg_3583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T913" id="Seg_3585" n="HIAT:w" s="T912">ej</ts>
                  <nts id="Seg_3586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T914" id="Seg_3588" n="HIAT:w" s="T913">mĭbiem</ts>
                  <nts id="Seg_3589" n="HIAT:ip">.</nts>
                  <nts id="Seg_3590" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T918" id="Seg_3592" n="HIAT:u" s="T914">
                  <ts e="T915" id="Seg_3594" n="HIAT:w" s="T914">Dĭgəttə</ts>
                  <nts id="Seg_3595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T916" id="Seg_3597" n="HIAT:w" s="T915">dĭ</ts>
                  <nts id="Seg_3598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T917" id="Seg_3600" n="HIAT:w" s="T916">bazoʔ</ts>
                  <nts id="Seg_3601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T918" id="Seg_3603" n="HIAT:w" s="T917">parluʔpi</ts>
                  <nts id="Seg_3604" n="HIAT:ip">.</nts>
                  <nts id="Seg_3605" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T923" id="Seg_3607" n="HIAT:u" s="T918">
                  <ts e="T919" id="Seg_3609" n="HIAT:w" s="T918">Ugandə</ts>
                  <nts id="Seg_3610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T920" id="Seg_3612" n="HIAT:w" s="T919">beržə</ts>
                  <nts id="Seg_3613" n="HIAT:ip">,</nts>
                  <nts id="Seg_3614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T921" id="Seg_3616" n="HIAT:w" s="T920">sĭre</ts>
                  <nts id="Seg_3617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T922" id="Seg_3619" n="HIAT:w" s="T921">bar</ts>
                  <nts id="Seg_3620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T923" id="Seg_3622" n="HIAT:w" s="T922">kundlaʔbə</ts>
                  <nts id="Seg_3623" n="HIAT:ip">.</nts>
                  <nts id="Seg_3624" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T928" id="Seg_3626" n="HIAT:u" s="T923">
                  <ts e="T924" id="Seg_3628" n="HIAT:w" s="T923">Mašinaʔi</ts>
                  <nts id="Seg_3629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T925" id="Seg_3631" n="HIAT:w" s="T924">ej</ts>
                  <nts id="Seg_3632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T926" id="Seg_3634" n="HIAT:w" s="T925">kambiʔi</ts>
                  <nts id="Seg_3635" n="HIAT:ip">,</nts>
                  <nts id="Seg_3636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T927" id="Seg_3638" n="HIAT:w" s="T926">dĭ</ts>
                  <nts id="Seg_3639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T928" id="Seg_3641" n="HIAT:w" s="T927">parluʔpi</ts>
                  <nts id="Seg_3642" n="HIAT:ip">.</nts>
                  <nts id="Seg_3643" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T930" id="Seg_3645" n="HIAT:u" s="T928">
                  <ts e="T929" id="Seg_3647" n="HIAT:w" s="T928">Dön</ts>
                  <nts id="Seg_3648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T930" id="Seg_3650" n="HIAT:w" s="T929">šaːbi</ts>
                  <nts id="Seg_3651" n="HIAT:ip">.</nts>
                  <nts id="Seg_3652" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T936" id="Seg_3654" n="HIAT:u" s="T930">
                  <ts e="T931" id="Seg_3656" n="HIAT:w" s="T930">A</ts>
                  <nts id="Seg_3657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T932" id="Seg_3659" n="HIAT:w" s="T931">teinen</ts>
                  <nts id="Seg_3660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T933" id="Seg_3662" n="HIAT:w" s="T932">măn</ts>
                  <nts id="Seg_3663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T934" id="Seg_3665" n="HIAT:w" s="T933">uʔpiam</ts>
                  <nts id="Seg_3666" n="HIAT:ip">,</nts>
                  <nts id="Seg_3667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T935" id="Seg_3669" n="HIAT:w" s="T934">mĭnzerbiem</ts>
                  <nts id="Seg_3670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T936" id="Seg_3672" n="HIAT:w" s="T935">bar</ts>
                  <nts id="Seg_3673" n="HIAT:ip">.</nts>
                  <nts id="Seg_3674" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T940" id="Seg_3676" n="HIAT:u" s="T936">
                  <ts e="T937" id="Seg_3678" n="HIAT:w" s="T936">Ipek</ts>
                  <nts id="Seg_3679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T938" id="Seg_3681" n="HIAT:w" s="T937">pürbiem</ts>
                  <nts id="Seg_3682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T939" id="Seg_3684" n="HIAT:w" s="T938">boskəndə</ts>
                  <nts id="Seg_3685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T940" id="Seg_3687" n="HIAT:w" s="T939">aʔtʼinə</ts>
                  <nts id="Seg_3688" n="HIAT:ip">.</nts>
                  <nts id="Seg_3689" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T944" id="Seg_3691" n="HIAT:u" s="T940">
                  <ts e="T941" id="Seg_3693" n="HIAT:w" s="T940">Amorbi</ts>
                  <nts id="Seg_3694" n="HIAT:ip">,</nts>
                  <nts id="Seg_3695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T942" id="Seg_3697" n="HIAT:w" s="T941">dĭgəttə</ts>
                  <nts id="Seg_3698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1026" id="Seg_3700" n="HIAT:w" s="T942">kalla</ts>
                  <nts id="Seg_3701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T943" id="Seg_3703" n="HIAT:w" s="T1026">dʼürbi</ts>
                  <nts id="Seg_3704" n="HIAT:ip">,</nts>
                  <nts id="Seg_3705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T944" id="Seg_3707" n="HIAT:w" s="T943">mašinazi</ts>
                  <nts id="Seg_3708" n="HIAT:ip">.</nts>
                  <nts id="Seg_3709" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T945" id="Seg_3711" n="HIAT:u" s="T944">
                  <ts e="T945" id="Seg_3713" n="HIAT:w" s="T944">Predsedatelʼzi</ts>
                  <nts id="Seg_3714" n="HIAT:ip">.</nts>
                  <nts id="Seg_3715" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T946" id="Seg_3717" n="HIAT:u" s="T945">
                  <nts id="Seg_3718" n="HIAT:ip">(</nts>
                  <nts id="Seg_3719" n="HIAT:ip">(</nts>
                  <ats e="T946" id="Seg_3720" n="HIAT:non-pho" s="T945">BRK</ats>
                  <nts id="Seg_3721" n="HIAT:ip">)</nts>
                  <nts id="Seg_3722" n="HIAT:ip">)</nts>
                  <nts id="Seg_3723" n="HIAT:ip">.</nts>
                  <nts id="Seg_3724" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T949" id="Seg_3726" n="HIAT:u" s="T946">
                  <ts e="T947" id="Seg_3728" n="HIAT:w" s="T946">Dĭgəttə</ts>
                  <nts id="Seg_3729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T948" id="Seg_3731" n="HIAT:w" s="T947">kallam</ts>
                  <nts id="Seg_3732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T949" id="Seg_3734" n="HIAT:w" s="T948">maʔnʼi</ts>
                  <nts id="Seg_3735" n="HIAT:ip">.</nts>
                  <nts id="Seg_3736" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T960" id="Seg_3738" n="HIAT:u" s="T949">
                  <ts e="T950" id="Seg_3740" n="HIAT:w" s="T949">Dĭgəttə</ts>
                  <nts id="Seg_3741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3742" n="HIAT:ip">(</nts>
                  <ts e="T951" id="Seg_3744" n="HIAT:w" s="T950">šobiam=</ts>
                  <nts id="Seg_3745" n="HIAT:ip">)</nts>
                  <nts id="Seg_3746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T952" id="Seg_3748" n="HIAT:w" s="T951">kallam</ts>
                  <nts id="Seg_3749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T953" id="Seg_3751" n="HIAT:w" s="T952">maʔnʼi</ts>
                  <nts id="Seg_3752" n="HIAT:ip">,</nts>
                  <nts id="Seg_3753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T953.tx-PKZ.1" id="Seg_3755" n="HIAT:w" s="T953">a</ts>
                  <nts id="Seg_3756" n="HIAT:ip">_</nts>
                  <ts e="T954" id="Seg_3758" n="HIAT:w" s="T953.tx-PKZ.1">to</ts>
                  <nts id="Seg_3759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T955" id="Seg_3761" n="HIAT:w" s="T954">măn</ts>
                  <nts id="Seg_3762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T956" id="Seg_3764" n="HIAT:w" s="T955">dĭn</ts>
                  <nts id="Seg_3765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T957" id="Seg_3767" n="HIAT:w" s="T956">il</ts>
                  <nts id="Seg_3768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T958" id="Seg_3770" n="HIAT:w" s="T957">amnolaʔbə</ts>
                  <nts id="Seg_3771" n="HIAT:ip">,</nts>
                  <nts id="Seg_3772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T959" id="Seg_3774" n="HIAT:w" s="T958">măna</ts>
                  <nts id="Seg_3775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T960" id="Seg_3777" n="HIAT:w" s="T959">edəʔleʔbəʔjə</ts>
                  <nts id="Seg_3778" n="HIAT:ip">.</nts>
                  <nts id="Seg_3779" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T963" id="Seg_3781" n="HIAT:u" s="T960">
                  <ts e="T961" id="Seg_3783" n="HIAT:w" s="T960">Dĭgəttə</ts>
                  <nts id="Seg_3784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T962" id="Seg_3786" n="HIAT:w" s="T961">šobiam</ts>
                  <nts id="Seg_3787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T963" id="Seg_3789" n="HIAT:w" s="T962">maːʔnʼi</ts>
                  <nts id="Seg_3790" n="HIAT:ip">.</nts>
                  <nts id="Seg_3791" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T967" id="Seg_3793" n="HIAT:u" s="T963">
                  <nts id="Seg_3794" n="HIAT:ip">(</nts>
                  <ts e="T964" id="Seg_3796" n="HIAT:w" s="T963">Dʼaparat</ts>
                  <nts id="Seg_3797" n="HIAT:ip">)</nts>
                  <nts id="Seg_3798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T965" id="Seg_3800" n="HIAT:w" s="T964">băzəbiam</ts>
                  <nts id="Seg_3801" n="HIAT:ip">,</nts>
                  <nts id="Seg_3802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3803" n="HIAT:ip">(</nts>
                  <ts e="T966" id="Seg_3805" n="HIAT:w" s="T965">büzö</ts>
                  <nts id="Seg_3806" n="HIAT:ip">)</nts>
                  <nts id="Seg_3807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T967" id="Seg_3809" n="HIAT:w" s="T966">bĭtəlbiem</ts>
                  <nts id="Seg_3810" n="HIAT:ip">.</nts>
                  <nts id="Seg_3811" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T972" id="Seg_3813" n="HIAT:u" s="T967">
                  <ts e="T968" id="Seg_3815" n="HIAT:w" s="T967">Dĭgəttə</ts>
                  <nts id="Seg_3816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T969" id="Seg_3818" n="HIAT:w" s="T968">tüžöjbə</ts>
                  <nts id="Seg_3819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T970" id="Seg_3821" n="HIAT:w" s="T969">sürerbiam</ts>
                  <nts id="Seg_3822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T971" id="Seg_3824" n="HIAT:w" s="T970">bünə</ts>
                  <nts id="Seg_3825" n="HIAT:ip">,</nts>
                  <nts id="Seg_3826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T972" id="Seg_3828" n="HIAT:w" s="T971">bĭtəlbiem</ts>
                  <nts id="Seg_3829" n="HIAT:ip">.</nts>
                  <nts id="Seg_3830" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T979" id="Seg_3832" n="HIAT:u" s="T972">
                  <ts e="T973" id="Seg_3834" n="HIAT:w" s="T972">Dĭgəttə</ts>
                  <nts id="Seg_3835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3836" n="HIAT:ip">(</nts>
                  <ts e="T974" id="Seg_3838" n="HIAT:w" s="T973">šonəg-</ts>
                  <nts id="Seg_3839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T975" id="Seg_3841" n="HIAT:w" s="T974">šonəga</ts>
                  <nts id="Seg_3842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T976" id="Seg_3844" n="HIAT:w" s="T975">m-</ts>
                  <nts id="Seg_3845" n="HIAT:ip">)</nts>
                  <nts id="Seg_3846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T977" id="Seg_3848" n="HIAT:w" s="T976">koʔbdo</ts>
                  <nts id="Seg_3849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T978" id="Seg_3851" n="HIAT:w" s="T977">šonəga</ts>
                  <nts id="Seg_3852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T979" id="Seg_3854" n="HIAT:w" s="T978">măna</ts>
                  <nts id="Seg_3855" n="HIAT:ip">.</nts>
                  <nts id="Seg_3856" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T986" id="Seg_3858" n="HIAT:u" s="T979">
                  <ts e="T980" id="Seg_3860" n="HIAT:w" s="T979">Davaj</ts>
                  <nts id="Seg_3861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T981" id="Seg_3863" n="HIAT:w" s="T980">kĭrzittə</ts>
                  <nts id="Seg_3864" n="HIAT:ip">,</nts>
                  <nts id="Seg_3865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T982" id="Seg_3867" n="HIAT:w" s="T981">măn</ts>
                  <nts id="Seg_3868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T983" id="Seg_3870" n="HIAT:w" s="T982">nubiam</ts>
                  <nts id="Seg_3871" n="HIAT:ip">,</nts>
                  <nts id="Seg_3872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T984" id="Seg_3874" n="HIAT:w" s="T983">dĭ</ts>
                  <nts id="Seg_3875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T985" id="Seg_3877" n="HIAT:w" s="T984">kĭrbi</ts>
                  <nts id="Seg_3878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T986" id="Seg_3880" n="HIAT:w" s="T985">măna</ts>
                  <nts id="Seg_3881" n="HIAT:ip">.</nts>
                  <nts id="Seg_3882" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T992" id="Seg_3884" n="HIAT:u" s="T986">
                  <ts e="T987" id="Seg_3886" n="HIAT:w" s="T986">Dĭgəttə</ts>
                  <nts id="Seg_3887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3888" n="HIAT:ip">(</nts>
                  <ts e="T988" id="Seg_3890" n="HIAT:w" s="T987">šobi-</ts>
                  <nts id="Seg_3891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T989" id="Seg_3893" n="HIAT:w" s="T988">šobila-</ts>
                  <nts id="Seg_3894" n="HIAT:ip">)</nts>
                  <nts id="Seg_3895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T990" id="Seg_3897" n="HIAT:w" s="T989">šobibaʔ</ts>
                  <nts id="Seg_3898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T991" id="Seg_3900" n="HIAT:w" s="T990">turanə</ts>
                  <nts id="Seg_3901" n="HIAT:ip">,</nts>
                  <nts id="Seg_3902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T992" id="Seg_3904" n="HIAT:w" s="T991">dʼăbaktərzittə</ts>
                  <nts id="Seg_3905" n="HIAT:ip">.</nts>
                  <nts id="Seg_3906" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T993" id="Seg_3908" n="HIAT:u" s="T992">
                  <nts id="Seg_3909" n="HIAT:ip">(</nts>
                  <nts id="Seg_3910" n="HIAT:ip">(</nts>
                  <ats e="T993" id="Seg_3911" n="HIAT:non-pho" s="T992">BRK</ats>
                  <nts id="Seg_3912" n="HIAT:ip">)</nts>
                  <nts id="Seg_3913" n="HIAT:ip">)</nts>
                  <nts id="Seg_3914" n="HIAT:ip">.</nts>
                  <nts id="Seg_3915" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T996" id="Seg_3917" n="HIAT:u" s="T993">
                  <ts e="T994" id="Seg_3919" n="HIAT:w" s="T993">Dĭgəttə</ts>
                  <nts id="Seg_3920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T995" id="Seg_3922" n="HIAT:w" s="T994">dʼăbaktərzittə</ts>
                  <nts id="Seg_3923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T996" id="Seg_3925" n="HIAT:w" s="T995">plʼonkanə</ts>
                  <nts id="Seg_3926" n="HIAT:ip">.</nts>
                  <nts id="Seg_3927" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T997" id="Seg_3929" n="HIAT:u" s="T996">
                  <nts id="Seg_3930" n="HIAT:ip">(</nts>
                  <nts id="Seg_3931" n="HIAT:ip">(</nts>
                  <ats e="T997" id="Seg_3932" n="HIAT:non-pho" s="T996">BRK</ats>
                  <nts id="Seg_3933" n="HIAT:ip">)</nts>
                  <nts id="Seg_3934" n="HIAT:ip">)</nts>
                  <nts id="Seg_3935" n="HIAT:ip">.</nts>
                  <nts id="Seg_3936" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T999" id="Seg_3938" n="HIAT:u" s="T997">
                  <ts e="T998" id="Seg_3940" n="HIAT:w" s="T997">Kurizəʔi</ts>
                  <nts id="Seg_3941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T999" id="Seg_3943" n="HIAT:w" s="T998">bădəbiam</ts>
                  <nts id="Seg_3944" n="HIAT:ip">.</nts>
                  <nts id="Seg_3945" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1002" id="Seg_3947" n="HIAT:u" s="T999">
                  <ts e="T1000" id="Seg_3949" n="HIAT:w" s="T999">Tüžöjn</ts>
                  <nts id="Seg_3950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1001" id="Seg_3952" n="HIAT:w" s="T1000">tüʔ</ts>
                  <nts id="Seg_3953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1002" id="Seg_3955" n="HIAT:w" s="T1001">barəʔpiam</ts>
                  <nts id="Seg_3956" n="HIAT:ip">.</nts>
                  <nts id="Seg_3957" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1003" id="Seg_3959" n="HIAT:u" s="T1002">
                  <nts id="Seg_3960" n="HIAT:ip">(</nts>
                  <nts id="Seg_3961" n="HIAT:ip">(</nts>
                  <ats e="T1003" id="Seg_3962" n="HIAT:non-pho" s="T1002">BRK</ats>
                  <nts id="Seg_3963" n="HIAT:ip">)</nts>
                  <nts id="Seg_3964" n="HIAT:ip">)</nts>
                  <nts id="Seg_3965" n="HIAT:ip">.</nts>
                  <nts id="Seg_3966" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1010" id="Seg_3968" n="HIAT:u" s="T1003">
                  <ts e="T1004" id="Seg_3970" n="HIAT:w" s="T1003">Kabarləj</ts>
                  <nts id="Seg_3971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1005" id="Seg_3973" n="HIAT:w" s="T1004">dʼăbaktərzittə</ts>
                  <nts id="Seg_3974" n="HIAT:ip">,</nts>
                  <nts id="Seg_3975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1005.tx-PKZ.1" id="Seg_3977" n="HIAT:w" s="T1005">a</ts>
                  <nts id="Seg_3978" n="HIAT:ip">_</nts>
                  <ts e="T1006" id="Seg_3980" n="HIAT:w" s="T1005.tx-PKZ.1">to</ts>
                  <nts id="Seg_3981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1007" id="Seg_3983" n="HIAT:w" s="T1006">esseŋ</ts>
                  <nts id="Seg_3984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1008" id="Seg_3986" n="HIAT:w" s="T1007">büžü</ts>
                  <nts id="Seg_3987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1009" id="Seg_3989" n="HIAT:w" s="T1008">šoləʔi</ts>
                  <nts id="Seg_3990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1010" id="Seg_3992" n="HIAT:w" s="T1009">školagəʔ</ts>
                  <nts id="Seg_3993" n="HIAT:ip">.</nts>
                  <nts id="Seg_3994" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1015" id="Seg_3996" n="HIAT:u" s="T1010">
                  <ts e="T1011" id="Seg_3998" n="HIAT:w" s="T1010">Ej</ts>
                  <nts id="Seg_3999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1012" id="Seg_4001" n="HIAT:w" s="T1011">mĭləʔi</ts>
                  <nts id="Seg_4002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1013" id="Seg_4004" n="HIAT:w" s="T1012">dʼăbaktərzittə</ts>
                  <nts id="Seg_4005" n="HIAT:ip">,</nts>
                  <nts id="Seg_4006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1014" id="Seg_4008" n="HIAT:w" s="T1013">bar</ts>
                  <nts id="Seg_4009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1015" id="Seg_4011" n="HIAT:w" s="T1014">kirgarləʔi</ts>
                  <nts id="Seg_4012" n="HIAT:ip">.</nts>
                  <nts id="Seg_4013" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-PKZ">
            <ts e="T18" id="Seg_4014" n="sc" s="T0">
               <ts e="T1" id="Seg_4016" n="e" s="T0">Măn </ts>
               <ts e="T2" id="Seg_4018" n="e" s="T1">amnobiam. </ts>
               <ts e="T3" id="Seg_4020" n="e" s="T2">Sumna </ts>
               <ts e="T4" id="Seg_4022" n="e" s="T3">tibizeŋ </ts>
               <ts e="T5" id="Seg_4024" n="e" s="T4">ibiʔi </ts>
               <ts e="T6" id="Seg_4026" n="e" s="T5">măn. </ts>
               <ts e="T7" id="Seg_4028" n="e" s="T6">Dĭgəttə </ts>
               <ts e="T8" id="Seg_4030" n="e" s="T7">muktuʔ </ts>
               <ts e="T9" id="Seg_4032" n="e" s="T8">ibiem. </ts>
               <ts e="T10" id="Seg_4034" n="e" s="T9">Tĭ </ts>
               <ts e="T11" id="Seg_4036" n="e" s="T10">muktuʔ </ts>
               <ts e="T12" id="Seg_4038" n="e" s="T11">tibi </ts>
               <ts e="T13" id="Seg_4040" n="e" s="T12">ej </ts>
               <ts e="T14" id="Seg_4042" n="e" s="T13">amnobi, </ts>
               <ts e="T15" id="Seg_4044" n="e" s="T14">barəʔpi </ts>
               <ts e="T16" id="Seg_4046" n="e" s="T15">da </ts>
               <ts e="T776" id="Seg_4048" n="e" s="T16">kalla </ts>
               <ts e="T17" id="Seg_4050" n="e" s="T776">dʼürbi. </ts>
               <ts e="T18" id="Seg_4052" n="e" s="T17">Dĭgəttə… </ts>
            </ts>
            <ts e="T1015" id="Seg_4053" n="sc" s="T19">
               <ts e="T20" id="Seg_4055" n="e" s="T19">Первый </ts>
               <ts e="T21" id="Seg_4057" n="e" s="T20">tibim </ts>
               <ts e="T22" id="Seg_4059" n="e" s="T21">bügən </ts>
               <ts e="T23" id="Seg_4061" n="e" s="T22">külambi, </ts>
               <ts e="T24" id="Seg_4063" n="e" s="T23">nʼim </ts>
               <ts e="T25" id="Seg_4065" n="e" s="T24">bügən </ts>
               <ts e="T26" id="Seg_4067" n="e" s="T25">külambi. </ts>
               <ts e="T27" id="Seg_4069" n="e" s="T26">Dĭgəttə </ts>
               <ts e="T28" id="Seg_4071" n="e" s="T27">iam </ts>
               <ts e="T29" id="Seg_4073" n="e" s="T28">kuʔpiʔi, </ts>
               <ts e="T30" id="Seg_4075" n="e" s="T29">sʼestram </ts>
               <ts e="T31" id="Seg_4077" n="e" s="T30">kuʔpiʔi. </ts>
               <ts e="T32" id="Seg_4079" n="e" s="T31">Abam </ts>
               <ts e="T33" id="Seg_4081" n="e" s="T32">dʼijenə </ts>
               <ts e="T34" id="Seg_4083" n="e" s="T33">kambi </ts>
               <ts e="T35" id="Seg_4085" n="e" s="T34">i </ts>
               <ts e="T36" id="Seg_4087" n="e" s="T35">dĭn </ts>
               <ts e="T37" id="Seg_4089" n="e" s="T36">külambi. </ts>
               <ts e="T38" id="Seg_4091" n="e" s="T37">Măn </ts>
               <ts e="T39" id="Seg_4093" n="e" s="T38">sʼestram </ts>
               <ts e="T40" id="Seg_4095" n="e" s="T39">tibit </ts>
               <ts e="T41" id="Seg_4097" n="e" s="T40">kuʔpi. </ts>
               <ts e="T42" id="Seg_4099" n="e" s="T41">Šide </ts>
               <ts e="T43" id="Seg_4101" n="e" s="T42">(ni-) </ts>
               <ts e="T44" id="Seg_4103" n="e" s="T43">nʼi </ts>
               <ts e="T45" id="Seg_4105" n="e" s="T44">maːbi. </ts>
               <ts e="T46" id="Seg_4107" n="e" s="T45">Măn </ts>
               <ts e="T47" id="Seg_4109" n="e" s="T46">dĭzem </ts>
               <ts e="T48" id="Seg_4111" n="e" s="T47">bar </ts>
               <ts e="T49" id="Seg_4113" n="e" s="T48">özerbiem. </ts>
               <ts e="T50" id="Seg_4115" n="e" s="T49">Oldʼa </ts>
               <ts e="T51" id="Seg_4117" n="e" s="T50">ibiem. </ts>
               <ts e="T52" id="Seg_4119" n="e" s="T51">I </ts>
               <ts e="T53" id="Seg_4121" n="e" s="T52">tüšəlbiem. </ts>
               <ts e="T54" id="Seg_4123" n="e" s="T53">(Teŋziszɨ) </ts>
               <ts e="T55" id="Seg_4125" n="e" s="T54">sazən. </ts>
               <ts e="T56" id="Seg_4127" n="e" s="T55">Onʼiʔ </ts>
               <ts e="T57" id="Seg_4129" n="e" s="T56">был </ts>
               <ts e="T58" id="Seg_4131" n="e" s="T57">булгахтер, </ts>
               <ts e="T59" id="Seg_4133" n="e" s="T58">(dĭgət-) </ts>
               <ts e="T60" id="Seg_4135" n="e" s="T59">onʼiʔ </ts>
               <ts e="T61" id="Seg_4137" n="e" s="T60">măna </ts>
               <ts e="T62" id="Seg_4139" n="e" s="T61">amnobi. </ts>
               <ts e="T63" id="Seg_4141" n="e" s="T62">Dĭgəttə </ts>
               <ts e="T64" id="Seg_4143" n="e" s="T63">dĭm </ts>
               <ts e="T65" id="Seg_4145" n="e" s="T64">ibiʔi </ts>
               <ts e="T66" id="Seg_4147" n="e" s="T65">văjnanə, </ts>
               <ts e="T67" id="Seg_4149" n="e" s="T66">dĭn </ts>
               <ts e="T68" id="Seg_4151" n="e" s="T67">kuʔpiʔi. </ts>
               <ts e="T69" id="Seg_4153" n="e" s="T68">Dĭn </ts>
               <ts e="T70" id="Seg_4155" n="e" s="T69">koʔbdo </ts>
               <ts e="T71" id="Seg_4157" n="e" s="T70">maluʔpi. </ts>
               <ts e="T72" id="Seg_4159" n="e" s="T71">((BRK)). </ts>
               <ts e="T73" id="Seg_4161" n="e" s="T72">Măn </ts>
               <ts e="T74" id="Seg_4163" n="e" s="T73">(pʼešk- </ts>
               <ts e="T75" id="Seg_4165" n="e" s="T74">pʼeštə </ts>
               <ts e="T76" id="Seg_4167" n="e" s="T75">s-) </ts>
               <ts e="T77" id="Seg_4169" n="e" s="T76">kănnambiam, </ts>
               <ts e="T78" id="Seg_4171" n="e" s="T77">pʼeštə </ts>
               <ts e="T79" id="Seg_4173" n="e" s="T78">(s- </ts>
               <ts e="T80" id="Seg_4175" n="e" s="T79">š- </ts>
               <ts e="T81" id="Seg_4177" n="e" s="T80">sa-) </ts>
               <ts e="T82" id="Seg_4179" n="e" s="T81">sʼabiam. </ts>
               <ts e="T83" id="Seg_4181" n="e" s="T82">Amnobiam, </ts>
               <ts e="T84" id="Seg_4183" n="e" s="T83">iʔbobiam, </ts>
               <ts e="T85" id="Seg_4185" n="e" s="T84">kunolbiam. </ts>
               <ts e="T86" id="Seg_4187" n="e" s="T85">Sanə </ts>
               <ts e="T87" id="Seg_4189" n="e" s="T86">(lei-) </ts>
               <ts e="T88" id="Seg_4191" n="e" s="T87">lejlem. </ts>
               <ts e="T89" id="Seg_4193" n="e" s="T88">Dĭgəttə </ts>
               <ts e="T90" id="Seg_4195" n="e" s="T89">šiʔ </ts>
               <ts e="T91" id="Seg_4197" n="e" s="T90">šobiʔi, </ts>
               <ts e="T92" id="Seg_4199" n="e" s="T91">măn </ts>
               <ts e="T93" id="Seg_4201" n="e" s="T92">šobiam </ts>
               <ts e="T94" id="Seg_4203" n="e" s="T93">šiʔnʼileʔ </ts>
               <ts e="T95" id="Seg_4205" n="e" s="T94">i </ts>
               <ts e="T96" id="Seg_4207" n="e" s="T95">bar </ts>
               <ts e="T97" id="Seg_4209" n="e" s="T96">dʼăbaktərlaʔbəm. </ts>
               <ts e="T98" id="Seg_4211" n="e" s="T97">((BRK)). </ts>
               <ts e="T99" id="Seg_4213" n="e" s="T98">Nagur </ts>
               <ts e="T100" id="Seg_4215" n="e" s="T99">kaga </ts>
               <ts e="T101" id="Seg_4217" n="e" s="T100">amnobiʔi </ts>
               <ts e="T102" id="Seg_4219" n="e" s="T101">i </ts>
               <ts e="T103" id="Seg_4221" n="e" s="T102">dĭgəttə </ts>
               <ts e="T104" id="Seg_4223" n="e" s="T103">kambiʔi </ts>
               <ts e="T105" id="Seg_4225" n="e" s="T104">pa </ts>
               <ts e="T106" id="Seg_4227" n="e" s="T105">jaʔsʼittə. </ts>
               <ts e="T107" id="Seg_4229" n="e" s="T106">Dĭzeŋgən </ts>
               <ts e="T108" id="Seg_4231" n="e" s="T107">šü </ts>
               <ts e="T109" id="Seg_4233" n="e" s="T108">nagobi. </ts>
               <ts e="T110" id="Seg_4235" n="e" s="T109">(Tirdə) </ts>
               <ts e="T111" id="Seg_4237" n="e" s="T110">kubiʔi, </ts>
               <ts e="T112" id="Seg_4239" n="e" s="T111">šü </ts>
               <ts e="T113" id="Seg_4241" n="e" s="T112">nendleʔbə. </ts>
               <ts e="T114" id="Seg_4243" n="e" s="T113">Kanaʔ! </ts>
               <ts e="T115" id="Seg_4245" n="e" s="T114">Iʔ </ts>
               <ts e="T116" id="Seg_4247" n="e" s="T115">šü! </ts>
               <ts e="T117" id="Seg_4249" n="e" s="T116">Dĭ </ts>
               <ts e="T118" id="Seg_4251" n="e" s="T117">kambi, </ts>
               <ts e="T119" id="Seg_4253" n="e" s="T118">dĭn </ts>
               <ts e="T120" id="Seg_4255" n="e" s="T119">büzʼe </ts>
               <ts e="T121" id="Seg_4257" n="e" s="T120">amnolaʔbə. </ts>
               <ts e="T122" id="Seg_4259" n="e" s="T121">(Nö-) </ts>
               <ts e="T123" id="Seg_4261" n="e" s="T122">Nörbaʔ </ts>
               <ts e="T124" id="Seg_4263" n="e" s="T123">сказка, </ts>
               <ts e="T125" id="Seg_4265" n="e" s="T124">dĭgəttə </ts>
               <ts e="T126" id="Seg_4267" n="e" s="T125">šü </ts>
               <ts e="T127" id="Seg_4269" n="e" s="T126">mĭlem. </ts>
               <ts e="T128" id="Seg_4271" n="e" s="T127">A </ts>
               <ts e="T129" id="Seg_4273" n="e" s="T128">dĭ </ts>
               <ts e="T130" id="Seg_4275" n="e" s="T129">măndə: </ts>
               <ts e="T131" id="Seg_4277" n="e" s="T130">Măn </ts>
               <ts e="T132" id="Seg_4279" n="e" s="T131">ej </ts>
               <ts e="T133" id="Seg_4281" n="e" s="T132">tĭmnem. </ts>
               <ts e="T134" id="Seg_4283" n="e" s="T133">Dĭ </ts>
               <ts e="T135" id="Seg_4285" n="e" s="T134">bögəldə </ts>
               <ts e="T136" id="Seg_4287" n="e" s="T135">(i- </ts>
               <ts e="T137" id="Seg_4289" n="e" s="T136">ba- </ts>
               <ts e="T138" id="Seg_4291" n="e" s="T137">bădarbi= </ts>
               <ts e="T139" id="Seg_4293" n="e" s="T138">bădaʔ-) </ts>
               <ts e="T140" id="Seg_4295" n="e" s="T139">băʔpi, </ts>
               <ts e="T141" id="Seg_4297" n="e" s="T140">(remniʔ) </ts>
               <ts e="T142" id="Seg_4299" n="e" s="T141">nagur </ts>
               <ts e="T143" id="Seg_4301" n="e" s="T142">ремень </ts>
               <ts e="T144" id="Seg_4303" n="e" s="T143">băʔpi. </ts>
               <ts e="T145" id="Seg_4305" n="e" s="T144">Dĭ </ts>
               <ts e="T146" id="Seg_4307" n="e" s="T145">(kalal-) </ts>
               <ts e="T1017" id="Seg_4309" n="e" s="T146">kalla </ts>
               <ts e="T147" id="Seg_4311" n="e" s="T1017">dʼürbi, </ts>
               <ts e="T148" id="Seg_4313" n="e" s="T147">i </ts>
               <ts e="T149" id="Seg_4315" n="e" s="T148">šü </ts>
               <ts e="T150" id="Seg_4317" n="e" s="T149">ej </ts>
               <ts e="T151" id="Seg_4319" n="e" s="T150">mĭbi. </ts>
               <ts e="T153" id="Seg_4321" n="e" s="T151">Dĭgəttə… </ts>
               <ts e="T154" id="Seg_4323" n="e" s="T153">Dĭgəttə </ts>
               <ts e="T155" id="Seg_4325" n="e" s="T154">onʼiʔ </ts>
               <ts e="T156" id="Seg_4327" n="e" s="T155">bazoʔ </ts>
               <ts e="T157" id="Seg_4329" n="e" s="T156">kambi. </ts>
               <ts e="T158" id="Seg_4331" n="e" s="T157">I </ts>
               <ts e="T159" id="Seg_4333" n="e" s="T158">dĭʔnə </ts>
               <ts e="T160" id="Seg_4335" n="e" s="T159">mămbi: </ts>
               <ts e="T161" id="Seg_4337" n="e" s="T160">Nörbaʔ </ts>
               <ts e="T162" id="Seg_4339" n="e" s="T161">сказка. </ts>
               <ts e="T163" id="Seg_4341" n="e" s="T162">Dĭ </ts>
               <ts e="T164" id="Seg_4343" n="e" s="T163">măndə: </ts>
               <ts e="T165" id="Seg_4345" n="e" s="T164">Măn </ts>
               <ts e="T166" id="Seg_4347" n="e" s="T165">ej </ts>
               <ts e="T167" id="Seg_4349" n="e" s="T166">tĭmnem. </ts>
               <ts e="T168" id="Seg_4351" n="e" s="T167">No, </ts>
               <ts e="T169" id="Seg_4353" n="e" s="T168">dĭgəttə </ts>
               <ts e="T170" id="Seg_4355" n="e" s="T169">dĭn </ts>
               <ts e="T171" id="Seg_4357" n="e" s="T170">bar </ts>
               <ts e="T172" id="Seg_4359" n="e" s="T171">dʼagarbi </ts>
               <ts e="T173" id="Seg_4361" n="e" s="T172">bögəldə. </ts>
               <ts e="T174" id="Seg_4363" n="e" s="T173">I </ts>
               <ts e="T175" id="Seg_4365" n="e" s="T174">kambi, </ts>
               <ts e="T176" id="Seg_4367" n="e" s="T175">ej </ts>
               <ts e="T177" id="Seg_4369" n="e" s="T176">mĭbi </ts>
               <ts e="T178" id="Seg_4371" n="e" s="T177">šü. </ts>
               <ts e="T179" id="Seg_4373" n="e" s="T178">Dĭgəttə </ts>
               <ts e="T180" id="Seg_4375" n="e" s="T179">onʼiʔ </ts>
               <ts e="T181" id="Seg_4377" n="e" s="T180">bazoʔ </ts>
               <ts e="T182" id="Seg_4379" n="e" s="T181">šobi. </ts>
               <ts e="T183" id="Seg_4381" n="e" s="T182">Nörbaʔ </ts>
               <ts e="T184" id="Seg_4383" n="e" s="T183">cказка! </ts>
               <ts e="T185" id="Seg_4385" n="e" s="T184">No, </ts>
               <ts e="T186" id="Seg_4387" n="e" s="T185">dĭ </ts>
               <ts e="T187" id="Seg_4389" n="e" s="T186">măndə: </ts>
               <ts e="T188" id="Seg_4391" n="e" s="T187">Nörbəlam. </ts>
               <ts e="T189" id="Seg_4393" n="e" s="T188">Bʼeʔ </ts>
               <ts e="T190" id="Seg_4395" n="e" s="T189">ibiʔi </ts>
               <ts e="T191" id="Seg_4397" n="e" s="T190">kagaʔi. </ts>
               <ts e="T192" id="Seg_4399" n="e" s="T191">Dĭgəttə </ts>
               <ts e="T193" id="Seg_4401" n="e" s="T192">kambiʔi </ts>
               <ts e="T194" id="Seg_4403" n="e" s="T193">pa </ts>
               <ts e="T195" id="Seg_4405" n="e" s="T194">jaʔsittə. </ts>
               <ts e="T196" id="Seg_4407" n="e" s="T195">Dĭgəttə </ts>
               <ts e="T197" id="Seg_4409" n="e" s="T196">(šojmu) </ts>
               <ts e="T198" id="Seg_4411" n="e" s="T197">suʔmiluʔpi, </ts>
               <ts e="T199" id="Seg_4413" n="e" s="T198">dĭ </ts>
               <ts e="T200" id="Seg_4415" n="e" s="T199">bar </ts>
               <ts e="T201" id="Seg_4417" n="e" s="T200">dʼünə </ts>
               <ts e="T1018" id="Seg_4419" n="e" s="T201">kalla </ts>
               <ts e="T202" id="Seg_4421" n="e" s="T1018">dʼürbi. </ts>
               <ts e="T203" id="Seg_4423" n="e" s="T202">Dĭgəttə </ts>
               <ts e="T204" id="Seg_4425" n="e" s="T203">nav </ts>
               <ts e="T205" id="Seg_4427" n="e" s="T204">šobi, </ts>
               <ts e="T206" id="Seg_4429" n="e" s="T205">maʔ </ts>
               <ts e="T207" id="Seg_4431" n="e" s="T206">abi. </ts>
               <ts e="T208" id="Seg_4433" n="e" s="T207">Munəʔi </ts>
               <ts e="T209" id="Seg_4435" n="e" s="T208">nʼebi. </ts>
               <ts e="T210" id="Seg_4437" n="e" s="T209">Dĭgəttə </ts>
               <ts e="T211" id="Seg_4439" n="e" s="T210">šobi </ts>
               <ts e="T212" id="Seg_4441" n="e" s="T211">dʼijegəʔ </ts>
               <ts e="T213" id="Seg_4443" n="e" s="T212">men </ts>
               <ts e="T214" id="Seg_4445" n="e" s="T213">urgo, </ts>
               <ts e="T215" id="Seg_4447" n="e" s="T214">i </ts>
               <ts e="T216" id="Seg_4449" n="e" s="T215">davaj </ts>
               <ts e="T217" id="Seg_4451" n="e" s="T216">munəʔi </ts>
               <ts e="T218" id="Seg_4453" n="e" s="T217">amzittə. </ts>
               <ts e="T219" id="Seg_4455" n="e" s="T218">Dĭ </ts>
               <ts e="T220" id="Seg_4457" n="e" s="T219">(dʼabə-) </ts>
               <ts e="T221" id="Seg_4459" n="e" s="T220">dʼabəluʔpi </ts>
               <ts e="T222" id="Seg_4461" n="e" s="T221">xvostə. </ts>
               <ts e="T223" id="Seg_4463" n="e" s="T222">Dĭ </ts>
               <ts e="T224" id="Seg_4465" n="e" s="T223">bar </ts>
               <ts e="T226" id="Seg_4467" n="e" s="T224">dĭm… </ts>
               <ts e="T227" id="Seg_4469" n="e" s="T226">запретил </ts>
               <ts e="T228" id="Seg_4471" n="e" s="T227">((BRK)). </ts>
               <ts e="T229" id="Seg_4473" n="e" s="T228">Dĭgəttə </ts>
               <ts e="T230" id="Seg_4475" n="e" s="T229">dĭ </ts>
               <ts e="T231" id="Seg_4477" n="e" s="T230">(dʼabəluʔpi) </ts>
               <ts e="T232" id="Seg_4479" n="e" s="T231">xvostə, </ts>
               <ts e="T233" id="Seg_4481" n="e" s="T232">xvostə </ts>
               <ts e="T234" id="Seg_4483" n="e" s="T233">(dʼ-) </ts>
               <ts e="T235" id="Seg_4485" n="e" s="T234">sajnʼeʔpi. </ts>
               <ts e="T236" id="Seg_4487" n="e" s="T235">Dĭgəttə </ts>
               <ts e="T237" id="Seg_4489" n="e" s="T236">dagajzi </ts>
               <ts e="T238" id="Seg_4491" n="e" s="T237">(bă-) </ts>
               <ts e="T239" id="Seg_4493" n="e" s="T238">băʔpi. </ts>
               <ts e="T240" id="Seg_4495" n="e" s="T239">Dĭn </ts>
               <ts e="T241" id="Seg_4497" n="e" s="T240">jašɨk </ts>
               <ts e="T242" id="Seg_4499" n="e" s="T241">kubi. </ts>
               <ts e="T243" id="Seg_4501" n="e" s="T242">Jašɨkkən </ts>
               <ts e="T244" id="Seg_4503" n="e" s="T243">sazən </ts>
               <ts e="T245" id="Seg_4505" n="e" s="T244">(pʼaŋnomə). </ts>
               <ts e="T246" id="Seg_4507" n="e" s="T245">(Măn </ts>
               <ts e="T247" id="Seg_4509" n="e" s="T246">a-) </ts>
               <ts e="T248" id="Seg_4511" n="e" s="T247">Măn </ts>
               <ts e="T249" id="Seg_4513" n="e" s="T248">abam </ts>
               <ts e="T250" id="Seg_4515" n="e" s="T249">(tăn </ts>
               <ts e="T251" id="Seg_4517" n="e" s="T250">aba- </ts>
               <ts e="T252" id="Seg_4519" n="e" s="T251">abatsi=) </ts>
               <ts e="T253" id="Seg_4521" n="e" s="T252">tăn </ts>
               <ts e="T254" id="Seg_4523" n="e" s="T253">abandə </ts>
               <ts e="T255" id="Seg_4525" n="e" s="T254">tüʔsittə </ts>
               <ts e="T1019" id="Seg_4527" n="e" s="T255">kalla </ts>
               <ts e="T256" id="Seg_4529" n="e" s="T1019">dʼürbi. </ts>
               <ts e="T257" id="Seg_4531" n="e" s="T256">((BRK)). </ts>
               <ts e="T258" id="Seg_4533" n="e" s="T257">Dĭgəttə </ts>
               <ts e="T259" id="Seg_4535" n="e" s="T258">dĭ </ts>
               <ts e="T260" id="Seg_4537" n="e" s="T259">büzʼe </ts>
               <ts e="T261" id="Seg_4539" n="e" s="T260">mămbi: </ts>
               <ts e="T262" id="Seg_4541" n="e" s="T261">Iʔ </ts>
               <ts e="T263" id="Seg_4543" n="e" s="T262">šamaʔ! </ts>
               <ts e="T264" id="Seg_4545" n="e" s="T263">A </ts>
               <ts e="T265" id="Seg_4547" n="e" s="T264">(dĭ </ts>
               <ts e="T266" id="Seg_4549" n="e" s="T265">m- </ts>
               <ts e="T267" id="Seg_4551" n="e" s="T266">dĭ </ts>
               <ts e="T268" id="Seg_4553" n="e" s="T267">dĭnʼ) </ts>
               <ts e="T269" id="Seg_4555" n="e" s="T268">kuza </ts>
               <ts e="T270" id="Seg_4557" n="e" s="T269">dĭʔnə </ts>
               <ts e="T271" id="Seg_4559" n="e" s="T270">bögəldə </ts>
               <ts e="T272" id="Seg_4561" n="e" s="T271">ремни </ts>
               <ts e="T273" id="Seg_4563" n="e" s="T272">băʔpi </ts>
               <ts e="T274" id="Seg_4565" n="e" s="T273">i </ts>
               <ts e="T275" id="Seg_4567" n="e" s="T274">šü </ts>
               <ts e="T276" id="Seg_4569" n="e" s="T275">ibi </ts>
               <ts e="T277" id="Seg_4571" n="e" s="T276">i </ts>
               <ts e="T1020" id="Seg_4573" n="e" s="T277">kalla </ts>
               <ts e="T278" id="Seg_4575" n="e" s="T1020">dʼürbi. </ts>
               <ts e="T279" id="Seg_4577" n="e" s="T278">((BRK)). </ts>
               <ts e="T280" id="Seg_4579" n="e" s="T279">Măn </ts>
               <ts e="T281" id="Seg_4581" n="e" s="T280">meim </ts>
               <ts e="T282" id="Seg_4583" n="e" s="T281">măn </ts>
               <ts e="T283" id="Seg_4585" n="e" s="T282">tibi </ts>
               <ts e="T284" id="Seg_4587" n="e" s="T283">(ka- </ts>
               <ts e="T285" id="Seg_4589" n="e" s="T284">i-) </ts>
               <ts e="T286" id="Seg_4591" n="e" s="T285">ileʔpi </ts>
               <ts e="T287" id="Seg_4593" n="e" s="T286">i </ts>
               <ts e="T1021" id="Seg_4595" n="e" s="T287">kalla </ts>
               <ts e="T288" id="Seg_4597" n="e" s="T1021">dʼürbiʔi. </ts>
               <ts e="T289" id="Seg_4599" n="e" s="T288">Miʔ </ts>
               <ts e="T290" id="Seg_4601" n="e" s="T289">jakše </ts>
               <ts e="T291" id="Seg_4603" n="e" s="T290">amnobiʔi, </ts>
               <ts e="T292" id="Seg_4605" n="e" s="T291">a </ts>
               <ts e="T293" id="Seg_4607" n="e" s="T292">dĭ </ts>
               <ts e="T294" id="Seg_4609" n="e" s="T293">măna </ts>
               <ts e="T295" id="Seg_4611" n="e" s="T294">ajirbi, </ts>
               <ts e="T296" id="Seg_4613" n="e" s="T295">udanə </ts>
               <ts e="T297" id="Seg_4615" n="e" s="T296">ibi. </ts>
               <ts e="T298" id="Seg_4617" n="e" s="T297">A </ts>
               <ts e="T299" id="Seg_4619" n="e" s="T298">(dĭgəttə) </ts>
               <ts e="T1022" id="Seg_4621" n="e" s="T299">kalla </ts>
               <ts e="T300" id="Seg_4623" n="e" s="T1022">dʼürbi </ts>
               <ts e="T301" id="Seg_4625" n="e" s="T300">Igarkanə </ts>
               <ts e="T302" id="Seg_4627" n="e" s="T301">i </ts>
               <ts e="T303" id="Seg_4629" n="e" s="T302">dĭn </ts>
               <ts e="T304" id="Seg_4631" n="e" s="T303">külambi. </ts>
               <ts e="T305" id="Seg_4633" n="e" s="T304">((BRK)). </ts>
               <ts e="T306" id="Seg_4635" n="e" s="T305">A </ts>
               <ts e="T307" id="Seg_4637" n="e" s="T306">(dĭ) </ts>
               <ts e="T308" id="Seg_4639" n="e" s="T307">ej </ts>
               <ts e="T309" id="Seg_4641" n="e" s="T308">kuvas </ts>
               <ts e="T310" id="Seg_4643" n="e" s="T309">ibi, </ts>
               <ts e="T311" id="Seg_4645" n="e" s="T310">aŋdə </ts>
               <ts e="T313" id="Seg_4647" n="e" s="T311">bar… </ts>
               <ts e="T314" id="Seg_4649" n="e" s="T313">((BRK)). </ts>
               <ts e="T315" id="Seg_4651" n="e" s="T314">Dĭ </ts>
               <ts e="T316" id="Seg_4653" n="e" s="T315">ej </ts>
               <ts e="T317" id="Seg_4655" n="e" s="T316">kuvas, </ts>
               <ts e="T318" id="Seg_4657" n="e" s="T317">aŋdə </ts>
               <ts e="T319" id="Seg_4659" n="e" s="T318">păjdʼaŋ, </ts>
               <ts e="T320" id="Seg_4661" n="e" s="T319">sʼimat </ts>
               <ts e="T321" id="Seg_4663" n="e" s="T320">păjdʼaŋ. </ts>
               <ts e="T322" id="Seg_4665" n="e" s="T321">((BRK)). </ts>
               <ts e="T323" id="Seg_4667" n="e" s="T322">Šide </ts>
               <ts e="T324" id="Seg_4669" n="e" s="T323">(ne) </ts>
               <ts e="T325" id="Seg_4671" n="e" s="T324">amnobiʔi. </ts>
               <ts e="T326" id="Seg_4673" n="e" s="T325">Kambiʔi </ts>
               <ts e="T327" id="Seg_4675" n="e" s="T326">sarankaʔi </ts>
               <ts e="T328" id="Seg_4677" n="e" s="T327">(tĭldəz'-) </ts>
               <ts e="T329" id="Seg_4679" n="e" s="T328">tĭlzittə. </ts>
               <ts e="T330" id="Seg_4681" n="e" s="T329">Onʼiʔ </ts>
               <ts e="T331" id="Seg_4683" n="e" s="T330">tĭlbi </ts>
               <ts e="T332" id="Seg_4685" n="e" s="T331">iʔgö, </ts>
               <ts e="T333" id="Seg_4687" n="e" s="T332">a </ts>
               <ts e="T334" id="Seg_4689" n="e" s="T333">onʼiʔ </ts>
               <ts e="T335" id="Seg_4691" n="e" s="T334">amga. </ts>
               <ts e="T336" id="Seg_4693" n="e" s="T335">Girgit </ts>
               <ts e="T337" id="Seg_4695" n="e" s="T336">iʔgö </ts>
               <ts e="T338" id="Seg_4697" n="e" s="T337">tĭlbi, </ts>
               <ts e="T339" id="Seg_4699" n="e" s="T338">a </ts>
               <ts e="T340" id="Seg_4701" n="e" s="T339">girgit </ts>
               <ts e="T341" id="Seg_4703" n="e" s="T340">amga </ts>
               <ts e="T342" id="Seg_4705" n="e" s="T341">tĭlbi, </ts>
               <ts e="T343" id="Seg_4707" n="e" s="T342">dĭm </ts>
               <ts e="T344" id="Seg_4709" n="e" s="T343">(taʔ) </ts>
               <ts e="T345" id="Seg_4711" n="e" s="T344">baltuzi </ts>
               <ts e="T346" id="Seg_4713" n="e" s="T345">ulut </ts>
               <ts e="T347" id="Seg_4715" n="e" s="T346">jaʔpi. </ts>
               <ts e="T348" id="Seg_4717" n="e" s="T347">Dĭgəttə </ts>
               <ts e="T349" id="Seg_4719" n="e" s="T348">šobi </ts>
               <ts e="T350" id="Seg_4721" n="e" s="T349">maʔnə. </ts>
               <ts e="T351" id="Seg_4723" n="e" s="T350">Koʔbdo </ts>
               <ts e="T352" id="Seg_4725" n="e" s="T351">šobi: </ts>
               <ts e="T353" id="Seg_4727" n="e" s="T352">Gijen </ts>
               <ts e="T354" id="Seg_4729" n="e" s="T353">măn </ts>
               <ts e="T355" id="Seg_4731" n="e" s="T354">iam? </ts>
               <ts e="T356" id="Seg_4733" n="e" s="T355">Dĭ </ts>
               <ts e="T357" id="Seg_4735" n="e" s="T356">bar </ts>
               <ts e="T358" id="Seg_4737" n="e" s="T357">ɨrɨː, </ts>
               <ts e="T359" id="Seg_4739" n="e" s="T358">ej </ts>
               <ts e="T360" id="Seg_4741" n="e" s="T359">tĭlbi </ts>
               <ts e="T361" id="Seg_4743" n="e" s="T360">sarankaʔi. </ts>
               <ts e="T362" id="Seg_4745" n="e" s="T361">Dĭn </ts>
               <ts e="T363" id="Seg_4747" n="e" s="T362">bar </ts>
               <ts e="T364" id="Seg_4749" n="e" s="T363">kunollaʔbə. </ts>
               <ts e="T365" id="Seg_4751" n="e" s="T364">A </ts>
               <ts e="T366" id="Seg_4753" n="e" s="T365">koʔbdot </ts>
               <ts e="T367" id="Seg_4755" n="e" s="T366">(kădedə) </ts>
               <ts e="T368" id="Seg_4757" n="e" s="T367">kăde </ts>
               <ts e="T369" id="Seg_4759" n="e" s="T368">- </ts>
               <ts e="T370" id="Seg_4761" n="e" s="T369">то </ts>
               <ts e="T371" id="Seg_4763" n="e" s="T370">(узн-) </ts>
               <ts e="T372" id="Seg_4765" n="e" s="T371">tĭmnebi </ts>
               <ts e="T373" id="Seg_4767" n="e" s="T372">i </ts>
               <ts e="T374" id="Seg_4769" n="e" s="T373">šobi. </ts>
               <ts e="T375" id="Seg_4771" n="e" s="T374">Nʼit </ts>
               <ts e="T376" id="Seg_4773" n="e" s="T375">ibi. </ts>
               <ts e="T377" id="Seg_4775" n="e" s="T376">Dĭ </ts>
               <ts e="T378" id="Seg_4777" n="e" s="T377">dĭm </ts>
               <ts e="T379" id="Seg_4779" n="e" s="T378">(šaʔla </ts>
               <ts e="T380" id="Seg_4781" n="e" s="T379">ibi), </ts>
               <ts e="T381" id="Seg_4783" n="e" s="T380">(kunol-) </ts>
               <ts e="T382" id="Seg_4785" n="e" s="T381">kunolzittə </ts>
               <ts e="T383" id="Seg_4787" n="e" s="T382">embi, </ts>
               <ts e="T384" id="Seg_4789" n="e" s="T383">a </ts>
               <ts e="T385" id="Seg_4791" n="e" s="T384">bostə </ts>
               <ts e="T386" id="Seg_4793" n="e" s="T385">nʼuʔdə </ts>
               <ts e="T387" id="Seg_4795" n="e" s="T386">(sa- </ts>
               <ts e="T388" id="Seg_4797" n="e" s="T387">š-) </ts>
               <ts e="T389" id="Seg_4799" n="e" s="T388">sʼabi </ts>
               <ts e="T390" id="Seg_4801" n="e" s="T389">maʔtə. </ts>
               <ts e="T391" id="Seg_4803" n="e" s="T390">Зола </ts>
               <ts e="T392" id="Seg_4805" n="e" s="T391">ibi. </ts>
               <ts e="T393" id="Seg_4807" n="e" s="T392">Dĭgəttə </ts>
               <ts e="T394" id="Seg_4809" n="e" s="T393">(dĭn-) </ts>
               <ts e="T395" id="Seg_4811" n="e" s="T394">dĭ </ts>
               <ts e="T396" id="Seg_4813" n="e" s="T395">ne </ts>
               <ts e="T397" id="Seg_4815" n="e" s="T396">šobi. </ts>
               <ts e="T398" id="Seg_4817" n="e" s="T397">Gijen </ts>
               <ts e="T399" id="Seg_4819" n="e" s="T398">šiʔ </ts>
               <ts e="T400" id="Seg_4821" n="e" s="T399">(t-) </ts>
               <ts e="T401" id="Seg_4823" n="e" s="T400">dĭ </ts>
               <ts e="T402" id="Seg_4825" n="e" s="T401">măndə, </ts>
               <ts e="T403" id="Seg_4827" n="e" s="T402">măn </ts>
               <ts e="T404" id="Seg_4829" n="e" s="T403">dön </ts>
               <ts e="T405" id="Seg_4831" n="e" s="T404">amnolaʔbəm! </ts>
               <ts e="T406" id="Seg_4833" n="e" s="T405">Dĭ </ts>
               <ts e="T407" id="Seg_4835" n="e" s="T406">(za-) </ts>
               <ts e="T408" id="Seg_4837" n="e" s="T407">зола </ts>
               <ts e="T409" id="Seg_4839" n="e" s="T408">(s-) </ts>
               <ts e="T410" id="Seg_4841" n="e" s="T409">kămnəbi </ts>
               <ts e="T411" id="Seg_4843" n="e" s="T410">sʼimandə. </ts>
               <ts e="T412" id="Seg_4845" n="e" s="T411">((BRK)). </ts>
               <ts e="T413" id="Seg_4847" n="e" s="T412">Ĭmbidə </ts>
               <ts e="T414" id="Seg_4849" n="e" s="T413">ej </ts>
               <ts e="T415" id="Seg_4851" n="e" s="T414">măndəliaʔi. </ts>
               <ts e="T416" id="Seg_4853" n="e" s="T415">Dĭgəttə </ts>
               <ts e="T417" id="Seg_4855" n="e" s="T416">sʼimat </ts>
               <ts e="T418" id="Seg_4857" n="e" s="T417">ĭmbidə </ts>
               <ts e="T419" id="Seg_4859" n="e" s="T418">ej </ts>
               <ts e="T420" id="Seg_4861" n="e" s="T419">moliaʔi. </ts>
               <ts e="T421" id="Seg_4863" n="e" s="T420">Dĭ </ts>
               <ts e="T422" id="Seg_4865" n="e" s="T421">suʔməluʔpi </ts>
               <ts e="T423" id="Seg_4867" n="e" s="T422">maʔtə, </ts>
               <ts e="T424" id="Seg_4869" n="e" s="T423">kagat </ts>
               <ts e="T425" id="Seg_4871" n="e" s="T424">ibi </ts>
               <ts e="T426" id="Seg_4873" n="e" s="T425">i </ts>
               <ts e="T1023" id="Seg_4875" n="e" s="T426">kalla </ts>
               <ts e="T427" id="Seg_4877" n="e" s="T1023">dʼürbi, </ts>
               <ts e="T428" id="Seg_4879" n="e" s="T427">gijen </ts>
               <ts e="T429" id="Seg_4881" n="e" s="T428">il </ts>
               <ts e="T430" id="Seg_4883" n="e" s="T429">iʔgö. </ts>
               <ts e="T431" id="Seg_4885" n="e" s="T430">((BRK)). </ts>
               <ts e="T432" id="Seg_4887" n="e" s="T431">Măn </ts>
               <ts e="T433" id="Seg_4889" n="e" s="T432">tibi </ts>
               <ts e="T1024" id="Seg_4891" n="e" s="T433">kalla </ts>
               <ts e="T434" id="Seg_4893" n="e" s="T1024">dʼürbi, </ts>
               <ts e="T435" id="Seg_4895" n="e" s="T434">măn </ts>
               <ts e="T436" id="Seg_4897" n="e" s="T435">ugandə </ts>
               <ts e="T437" id="Seg_4899" n="e" s="T436">tăŋ </ts>
               <ts e="T438" id="Seg_4901" n="e" s="T437">dʼorbiam. </ts>
               <ts e="T439" id="Seg_4903" n="e" s="T438">Sĭjbə </ts>
               <ts e="T440" id="Seg_4905" n="e" s="T439">bar </ts>
               <ts e="T441" id="Seg_4907" n="e" s="T440">ĭzembi. </ts>
               <ts e="T442" id="Seg_4909" n="e" s="T441">I </ts>
               <ts e="T443" id="Seg_4911" n="e" s="T442">ulum </ts>
               <ts e="T444" id="Seg_4913" n="e" s="T443">((…)) </ts>
               <ts e="T445" id="Seg_4915" n="e" s="T444">((DMG)). </ts>
               <ts e="T446" id="Seg_4917" n="e" s="T445">((BRK)). </ts>
               <ts e="T447" id="Seg_4919" n="e" s="T446">Măn </ts>
               <ts e="T448" id="Seg_4921" n="e" s="T447">(ser-) </ts>
               <ts e="T449" id="Seg_4923" n="e" s="T448">sʼestram </ts>
               <ts e="T450" id="Seg_4925" n="e" s="T449">(nʼi=) </ts>
               <ts e="T451" id="Seg_4927" n="e" s="T450">nʼit </ts>
               <ts e="T452" id="Seg_4929" n="e" s="T451">tʼerməndə </ts>
               <ts e="T453" id="Seg_4931" n="e" s="T452">mĭmbi, </ts>
               <ts e="T454" id="Seg_4933" n="e" s="T453">ipek </ts>
               <ts e="T455" id="Seg_4935" n="e" s="T454">((DMG)). </ts>
               <ts e="T456" id="Seg_4937" n="e" s="T455">Tʼerməndə </ts>
               <ts e="T457" id="Seg_4939" n="e" s="T456">mĭbi, </ts>
               <ts e="T458" id="Seg_4941" n="e" s="T457">un </ts>
               <ts e="T459" id="Seg_4943" n="e" s="T458">nʼeʔsittə. </ts>
               <ts e="T460" id="Seg_4945" n="e" s="T459">Dĭgəttə </ts>
               <ts e="T461" id="Seg_4947" n="e" s="T460">maʔndə </ts>
               <ts e="T462" id="Seg_4949" n="e" s="T461">šobi. </ts>
               <ts e="T463" id="Seg_4951" n="e" s="T462">Bostə </ts>
               <ts e="T464" id="Seg_4953" n="e" s="T463">nenə </ts>
               <ts e="T465" id="Seg_4955" n="e" s="T464">mănlia: </ts>
               <ts e="T466" id="Seg_4957" n="e" s="T465">Štobɨ </ts>
               <ts e="T467" id="Seg_4959" n="e" s="T466">kondʼo </ts>
               <ts e="T468" id="Seg_4961" n="e" s="T467">(kaba-) </ts>
               <ts e="T469" id="Seg_4963" n="e" s="T468">kabarləj, </ts>
               <ts e="T470" id="Seg_4965" n="e" s="T469">a </ts>
               <ts e="T471" id="Seg_4967" n="e" s="T470">măn </ts>
               <ts e="T472" id="Seg_4969" n="e" s="T471">amnoliam </ts>
               <ts e="T473" id="Seg_4971" n="e" s="T472">da </ts>
               <ts e="T474" id="Seg_4973" n="e" s="T473">tenöliam. </ts>
               <ts e="T475" id="Seg_4975" n="e" s="T474">Ладно, </ts>
               <ts e="T476" id="Seg_4977" n="e" s="T475">što </ts>
               <ts e="T477" id="Seg_4979" n="e" s="T476">măn </ts>
               <ts e="T478" id="Seg_4981" n="e" s="T477">(dĭzi-) </ts>
               <ts e="T479" id="Seg_4983" n="e" s="T478">dĭzeŋzi, </ts>
               <ts e="T480" id="Seg_4985" n="e" s="T479">amorzittə </ts>
               <ts e="T481" id="Seg_4987" n="e" s="T480">не </ts>
               <ts e="T482" id="Seg_4989" n="e" s="T481">стало, </ts>
               <ts e="T483" id="Seg_4991" n="e" s="T482">to </ts>
               <ts e="T484" id="Seg_4993" n="e" s="T483">(mĭmbiʔi, </ts>
               <ts e="T485" id="Seg_4995" n="e" s="T484">boštə) </ts>
               <ts e="T486" id="Seg_4997" n="e" s="T485">măn </ts>
               <ts e="T487" id="Seg_4999" n="e" s="T486">iʔgö </ts>
               <ts e="T488" id="Seg_5001" n="e" s="T487">amniam". </ts>
               <ts e="T489" id="Seg_5003" n="e" s="T488">((BRK)). </ts>
               <ts e="T490" id="Seg_5005" n="e" s="T489">Onʼiʔ </ts>
               <ts e="T491" id="Seg_5007" n="e" s="T490">kuza </ts>
               <ts e="T492" id="Seg_5009" n="e" s="T491">monoʔkosʼtə </ts>
               <ts e="T493" id="Seg_5011" n="e" s="T492">(k-) </ts>
               <ts e="T494" id="Seg_5013" n="e" s="T493">kambi. </ts>
               <ts e="T495" id="Seg_5015" n="e" s="T494">Kuvas </ts>
               <ts e="T496" id="Seg_5017" n="e" s="T495">koʔbdo, </ts>
               <ts e="T497" id="Seg_5019" n="e" s="T496">ugandə </ts>
               <ts e="T498" id="Seg_5021" n="e" s="T497">oldʼat </ts>
               <ts e="T499" id="Seg_5023" n="e" s="T498">iʔgö. </ts>
               <ts e="T500" id="Seg_5025" n="e" s="T499">Kandəga, </ts>
               <ts e="T501" id="Seg_5027" n="e" s="T500">dĭgəttə </ts>
               <ts e="T502" id="Seg_5029" n="e" s="T501">kuza </ts>
               <ts e="T503" id="Seg_5031" n="e" s="T502">dĭʔnə </ts>
               <ts e="T504" id="Seg_5033" n="e" s="T503">măndə: </ts>
               <ts e="T505" id="Seg_5035" n="e" s="T504">Gibər </ts>
               <ts e="T506" id="Seg_5037" n="e" s="T505">kandəgal? </ts>
               <ts e="T507" id="Seg_5039" n="e" s="T506">Monoʔkosʼtə. </ts>
               <ts e="T508" id="Seg_5041" n="e" s="T507">Iʔ </ts>
               <ts e="T509" id="Seg_5043" n="e" s="T508">măna! </ts>
               <ts e="T510" id="Seg_5045" n="e" s="T509">A </ts>
               <ts e="T511" id="Seg_5047" n="e" s="T510">tăn </ts>
               <ts e="T512" id="Seg_5049" n="e" s="T511">ĭmbi </ts>
               <ts e="T513" id="Seg_5051" n="e" s="T512">alial? </ts>
               <ts e="T514" id="Seg_5053" n="e" s="T513">Amorzittə </ts>
               <ts e="T515" id="Seg_5055" n="e" s="T514">da </ts>
               <ts e="T516" id="Seg_5057" n="e" s="T515">tüʔsittə. </ts>
               <ts e="T517" id="Seg_5059" n="e" s="T516">Dĭgəttə </ts>
               <ts e="T518" id="Seg_5061" n="e" s="T517">kambiʔi </ts>
               <ts e="T519" id="Seg_5063" n="e" s="T518">bazoʔ. </ts>
               <ts e="T520" id="Seg_5065" n="e" s="T519">Bazoʔ </ts>
               <ts e="T521" id="Seg_5067" n="e" s="T520">kuza </ts>
               <ts e="T522" id="Seg_5069" n="e" s="T521">măndə: </ts>
               <ts e="T523" id="Seg_5071" n="e" s="T522">Gibər </ts>
               <ts e="T524" id="Seg_5073" n="e" s="T523">kandəgalaʔ? </ts>
               <ts e="T525" id="Seg_5075" n="e" s="T524">Monoʔkosʼtə. </ts>
               <ts e="T526" id="Seg_5077" n="e" s="T525">Măna </ts>
               <ts e="T527" id="Seg_5079" n="e" s="T526">it! </ts>
               <ts e="T528" id="Seg_5081" n="e" s="T527">A </ts>
               <ts e="T529" id="Seg_5083" n="e" s="T528">tăn </ts>
               <ts e="T530" id="Seg_5085" n="e" s="T529">ĭmbi </ts>
               <ts e="T531" id="Seg_5087" n="e" s="T530">aləl? </ts>
               <ts e="T532" id="Seg_5089" n="e" s="T531">Bĭʔsittə </ts>
               <ts e="T533" id="Seg_5091" n="e" s="T532">da </ts>
               <ts e="T534" id="Seg_5093" n="e" s="T533">(kĭnzəsʼtə). </ts>
               <ts e="T535" id="Seg_5095" n="e" s="T534">Dĭgəttə </ts>
               <ts e="T536" id="Seg_5097" n="e" s="T535">šobiʔi </ts>
               <ts e="T537" id="Seg_5099" n="e" s="T536">i </ts>
               <ts e="T538" id="Seg_5101" n="e" s="T537">davaj </ts>
               <ts e="T539" id="Seg_5103" n="e" s="T538">monoʔkosʼtə. </ts>
               <ts e="T540" id="Seg_5105" n="e" s="T539">Dĭ </ts>
               <ts e="T541" id="Seg_5107" n="e" s="T540">măndə: </ts>
               <ts e="T542" id="Seg_5109" n="e" s="T541">Măn </ts>
               <ts e="T543" id="Seg_5111" n="e" s="T542">iʔgö </ts>
               <ts e="T544" id="Seg_5113" n="e" s="T543">bar </ts>
               <ts e="T545" id="Seg_5115" n="e" s="T544">aləm. </ts>
               <ts e="T546" id="Seg_5117" n="e" s="T545">Ipek </ts>
               <ts e="T547" id="Seg_5119" n="e" s="T546">pürlem, </ts>
               <ts e="T548" id="Seg_5121" n="e" s="T547">uja, </ts>
               <ts e="T549" id="Seg_5123" n="e" s="T548">bar </ts>
               <ts e="T550" id="Seg_5125" n="e" s="T549">amzittə </ts>
               <ts e="T551" id="Seg_5127" n="e" s="T550">nada. </ts>
               <ts e="T552" id="Seg_5129" n="e" s="T551">Dĭ </ts>
               <ts e="T553" id="Seg_5131" n="e" s="T552">măndə: </ts>
               <ts e="T554" id="Seg_5133" n="e" s="T553">No, </ts>
               <ts e="T555" id="Seg_5135" n="e" s="T554">bar </ts>
               <ts e="T556" id="Seg_5137" n="e" s="T555">măn </ts>
               <ts e="T557" id="Seg_5139" n="e" s="T556">amnam. </ts>
               <ts e="T558" id="Seg_5141" n="e" s="T557">I </ts>
               <ts e="T559" id="Seg_5143" n="e" s="T558">((PAUSE)) </ts>
               <ts e="T560" id="Seg_5145" n="e" s="T559">пива </ts>
               <ts e="T561" id="Seg_5147" n="e" s="T560">iʔgö. </ts>
               <ts e="T562" id="Seg_5149" n="e" s="T561">Bʼeʔ </ts>
               <ts e="T563" id="Seg_5151" n="e" s="T562">teʔtə </ts>
               <ts e="T564" id="Seg_5153" n="e" s="T563">ведро </ts>
               <ts e="T565" id="Seg_5155" n="e" s="T564">alam. </ts>
               <ts e="T566" id="Seg_5157" n="e" s="T565">No, </ts>
               <ts e="T567" id="Seg_5159" n="e" s="T566">dĭgəttə </ts>
               <ts e="T568" id="Seg_5161" n="e" s="T567">(š-) </ts>
               <ts e="T569" id="Seg_5163" n="e" s="T568">stoldə </ts>
               <ts e="T570" id="Seg_5165" n="e" s="T569">oʔbdəbi, </ts>
               <ts e="T571" id="Seg_5167" n="e" s="T570">dĭgəttə </ts>
               <ts e="T572" id="Seg_5169" n="e" s="T571">šobi </ts>
               <ts e="T573" id="Seg_5171" n="e" s="T572">dĭ </ts>
               <ts e="T574" id="Seg_5173" n="e" s="T573">kuza. </ts>
               <ts e="T575" id="Seg_5175" n="e" s="T574">Kajit </ts>
               <ts e="T576" id="Seg_5177" n="e" s="T575">amorzittə </ts>
               <ts e="T577" id="Seg_5179" n="e" s="T576">da </ts>
               <ts e="T578" id="Seg_5181" n="e" s="T577">tüʔsittə </ts>
               <ts e="T579" id="Seg_5183" n="e" s="T578">bar </ts>
               <ts e="T580" id="Seg_5185" n="e" s="T579">amnaʔbə </ts>
               <ts e="T581" id="Seg_5187" n="e" s="T580">da </ts>
               <ts e="T582" id="Seg_5189" n="e" s="T581">tüʔleʔbə, </ts>
               <ts e="T583" id="Seg_5191" n="e" s="T582">amnaʔbə </ts>
               <ts e="T584" id="Seg_5193" n="e" s="T583">da </ts>
               <ts e="T585" id="Seg_5195" n="e" s="T584">tüʔleʔbə. </ts>
               <ts e="T586" id="Seg_5197" n="e" s="T585">Bar </ts>
               <ts e="T587" id="Seg_5199" n="e" s="T586">ĭmbi </ts>
               <ts e="T588" id="Seg_5201" n="e" s="T587">amnuʔpi: </ts>
               <ts e="T589" id="Seg_5203" n="e" s="T588">i </ts>
               <ts e="T590" id="Seg_5205" n="e" s="T589">uja, </ts>
               <ts e="T591" id="Seg_5207" n="e" s="T590">i </ts>
               <ts e="T592" id="Seg_5209" n="e" s="T591">((PAUSE)) </ts>
               <ts e="T593" id="Seg_5211" n="e" s="T592">ĭmbi </ts>
               <ts e="T594" id="Seg_5213" n="e" s="T593">ibi, </ts>
               <ts e="T595" id="Seg_5215" n="e" s="T594">bar. </ts>
               <ts e="T596" id="Seg_5217" n="e" s="T595">Bostə </ts>
               <ts e="T597" id="Seg_5219" n="e" s="T596">măndə: </ts>
               <ts e="T598" id="Seg_5221" n="e" s="T597">Deʔkeʔ </ts>
               <ts e="T599" id="Seg_5223" n="e" s="T598">išo </ts>
               <ts e="T600" id="Seg_5225" n="e" s="T599">amga, </ts>
               <ts e="T601" id="Seg_5227" n="e" s="T600">deʔkeʔ </ts>
               <ts e="T602" id="Seg_5229" n="e" s="T601">išo </ts>
               <ts e="T603" id="Seg_5231" n="e" s="T602">amga. </ts>
               <ts e="T604" id="Seg_5233" n="e" s="T603">Dĭgəttə </ts>
               <ts e="T605" id="Seg_5235" n="e" s="T604">už </ts>
               <ts e="T606" id="Seg_5237" n="e" s="T605">amzittə </ts>
               <ts e="T607" id="Seg_5239" n="e" s="T606">(нечего </ts>
               <ts e="T608" id="Seg_5241" n="e" s="T607">ст </ts>
               <ts e="T609" id="Seg_5243" n="e" s="T608">-) </ts>
               <ts e="T610" id="Seg_5245" n="e" s="T609">нечего </ts>
               <ts e="T611" id="Seg_5247" n="e" s="T610">стало. </ts>
               <ts e="T612" id="Seg_5249" n="e" s="T611">Dĭgəttə </ts>
               <ts e="T613" id="Seg_5251" n="e" s="T612">(bü=) </ts>
               <ts e="T614" id="Seg_5253" n="e" s="T613">пиво </ts>
               <ts e="T615" id="Seg_5255" n="e" s="T614">bĭʔsittə </ts>
               <ts e="T616" id="Seg_5257" n="e" s="T615">(bazoʔ=) </ts>
               <ts e="T617" id="Seg_5259" n="e" s="T616">bazoʔ </ts>
               <ts e="T618" id="Seg_5261" n="e" s="T617">šobi </ts>
               <ts e="T619" id="Seg_5263" n="e" s="T618">(dĭ </ts>
               <ts e="T620" id="Seg_5265" n="e" s="T619">k-) </ts>
               <ts e="T621" id="Seg_5267" n="e" s="T620">dĭ </ts>
               <ts e="T622" id="Seg_5269" n="e" s="T621">kuza. </ts>
               <ts e="T623" id="Seg_5271" n="e" s="T622">Dĭgəttə </ts>
               <ts e="T624" id="Seg_5273" n="e" s="T623">dĭ </ts>
               <ts e="T625" id="Seg_5275" n="e" s="T624">(bĭ-) </ts>
               <ts e="T626" id="Seg_5277" n="e" s="T625">bĭtleʔbə, </ts>
               <ts e="T627" id="Seg_5279" n="e" s="T626">bĭtleʔbə </ts>
               <ts e="T628" id="Seg_5281" n="e" s="T627">da </ts>
               <ts e="T629" id="Seg_5283" n="e" s="T628">kĭnzleʔbə, </ts>
               <ts e="T630" id="Seg_5285" n="e" s="T629">bĭtleʔbə </ts>
               <ts e="T631" id="Seg_5287" n="e" s="T630">da </ts>
               <ts e="T632" id="Seg_5289" n="e" s="T631">kĭnzleʔbə. </ts>
               <ts e="T633" id="Seg_5291" n="e" s="T632">Bar </ts>
               <ts e="T634" id="Seg_5293" n="e" s="T633">bĭtluʔpi </ts>
               <ts e="T635" id="Seg_5295" n="e" s="T634">da </ts>
               <ts e="T636" id="Seg_5297" n="e" s="T635">kirgarlaʔbə: </ts>
               <ts e="T637" id="Seg_5299" n="e" s="T636">Išo </ts>
               <ts e="T638" id="Seg_5301" n="e" s="T637">(d-) </ts>
               <ts e="T639" id="Seg_5303" n="e" s="T638">deʔ, </ts>
               <ts e="T640" id="Seg_5305" n="e" s="T639">măna </ts>
               <ts e="T641" id="Seg_5307" n="e" s="T640">amga! </ts>
               <ts e="T642" id="Seg_5309" n="e" s="T641">A </ts>
               <ts e="T643" id="Seg_5311" n="e" s="T642">ulitsanə </ts>
               <ts e="T644" id="Seg_5313" n="e" s="T643">bar </ts>
               <ts e="T645" id="Seg_5315" n="e" s="T644">ugandə </ts>
               <ts e="T646" id="Seg_5317" n="e" s="T645">bü </ts>
               <ts e="T647" id="Seg_5319" n="e" s="T646">iʔgö. </ts>
               <ts e="T648" id="Seg_5321" n="e" s="T647">((BRK)). </ts>
               <ts e="T649" id="Seg_5323" n="e" s="T648">Dĭgəttə </ts>
               <ts e="T650" id="Seg_5325" n="e" s="T649">ej </ts>
               <ts e="T651" id="Seg_5327" n="e" s="T650">ambi </ts>
               <ts e="T652" id="Seg_5329" n="e" s="T651">da </ts>
               <ts e="T653" id="Seg_5331" n="e" s="T652">bü </ts>
               <ts e="T654" id="Seg_5333" n="e" s="T653">ej </ts>
               <ts e="T655" id="Seg_5335" n="e" s="T654">bĭʔpi. </ts>
               <ts e="T656" id="Seg_5337" n="e" s="T655">Dĭʔnə </ts>
               <ts e="T657" id="Seg_5339" n="e" s="T656">bɨ </ts>
               <ts e="T658" id="Seg_5341" n="e" s="T657">ulubə </ts>
               <ts e="T659" id="Seg_5343" n="e" s="T658">sajjaʔpiʔi </ts>
               <ts e="T660" id="Seg_5345" n="e" s="T659">bɨ </ts>
               <ts e="T661" id="Seg_5347" n="e" s="T660">i </ts>
               <ts e="T662" id="Seg_5349" n="e" s="T661">edəbiʔi, </ts>
               <ts e="T664" id="Seg_5351" n="e" s="T662">a_to… </ts>
               <ts e="T665" id="Seg_5353" n="e" s="T664">Dĭgəttə </ts>
               <ts e="T666" id="Seg_5355" n="e" s="T665">dĭ </ts>
               <ts e="T667" id="Seg_5357" n="e" s="T666">koʔbdom </ts>
               <ts e="T668" id="Seg_5359" n="e" s="T667">ibi </ts>
               <ts e="T669" id="Seg_5361" n="e" s="T668">i </ts>
               <ts e="T670" id="Seg_5363" n="e" s="T669">maʔndə </ts>
               <ts e="T671" id="Seg_5365" n="e" s="T670">kambi. </ts>
               <ts e="T672" id="Seg_5367" n="e" s="T671">((BRK)). </ts>
               <ts e="T673" id="Seg_5369" n="e" s="T672">"Kaba, </ts>
               <ts e="T674" id="Seg_5371" n="e" s="T673">ĭmbi </ts>
               <ts e="T675" id="Seg_5373" n="e" s="T674">nüjleʔbəl? </ts>
               <ts e="T676" id="Seg_5375" n="e" s="T675">Коробочка. </ts>
               <ts e="T677" id="Seg_5377" n="e" s="T676">A </ts>
               <ts e="T678" id="Seg_5379" n="e" s="T677">ĭmbi </ts>
               <ts e="T679" id="Seg_5381" n="e" s="T678">ej </ts>
               <ts e="T680" id="Seg_5383" n="e" s="T679">jakše </ts>
               <ts e="T681" id="Seg_5385" n="e" s="T680">nüjleʔbəl? </ts>
               <ts e="T682" id="Seg_5387" n="e" s="T681">Da </ts>
               <ts e="T683" id="Seg_5389" n="e" s="T682">нельзя </ts>
               <ts e="T684" id="Seg_5391" n="e" s="T683">jakše </ts>
               <ts e="T685" id="Seg_5393" n="e" s="T684">(nüjn-) </ts>
               <ts e="T686" id="Seg_5395" n="e" s="T685">nüjnəsʼtə. </ts>
               <ts e="T687" id="Seg_5397" n="e" s="T686">Kuiol </ts>
               <ts e="T688" id="Seg_5399" n="e" s="T687">(barlaʔina)". </ts>
               <ts e="T689" id="Seg_5401" n="e" s="T688">((BRK)). </ts>
               <ts e="T690" id="Seg_5403" n="e" s="T689">Onʼiʔ </ts>
               <ts e="T691" id="Seg_5405" n="e" s="T690">kuza </ts>
               <ts e="T692" id="Seg_5407" n="e" s="T691">(ibɨ-) </ts>
               <ts e="T693" id="Seg_5409" n="e" s="T692">ibi </ts>
               <ts e="T694" id="Seg_5411" n="e" s="T693">tʼerməndə. </ts>
               <ts e="T695" id="Seg_5413" n="e" s="T694">(A=) </ts>
               <ts e="T696" id="Seg_5415" n="e" s="T695">A </ts>
               <ts e="T697" id="Seg_5417" n="e" s="T696">onʼiʔ </ts>
               <ts e="T698" id="Seg_5419" n="e" s="T697">kuza </ts>
               <ts e="T699" id="Seg_5421" n="e" s="T698">ugandə </ts>
               <ts e="T700" id="Seg_5423" n="e" s="T699">aktʼa </ts>
               <ts e="T701" id="Seg_5425" n="e" s="T700">iʔgö </ts>
               <ts e="T702" id="Seg_5427" n="e" s="T701">(был=) </ts>
               <ts e="T703" id="Seg_5429" n="e" s="T702">ibi. </ts>
               <ts e="T704" id="Seg_5431" n="e" s="T703">I </ts>
               <ts e="T705" id="Seg_5433" n="e" s="T704">oldʼa, </ts>
               <ts e="T706" id="Seg_5435" n="e" s="T705">i </ts>
               <ts e="T707" id="Seg_5437" n="e" s="T706">ipek. </ts>
               <ts e="T708" id="Seg_5439" n="e" s="T707">(Dĭ=) </ts>
               <ts e="T709" id="Seg_5441" n="e" s="T708">Dĭn </ts>
               <ts e="T710" id="Seg_5443" n="e" s="T709">kuzanə </ts>
               <ts e="T711" id="Seg_5445" n="e" s="T710">ibi </ts>
               <ts e="T712" id="Seg_5447" n="e" s="T711">tʼerməndə </ts>
               <ts e="T713" id="Seg_5449" n="e" s="T712">i </ts>
               <ts e="T714" id="Seg_5451" n="e" s="T713">(ko-) </ts>
               <ts e="T715" id="Seg_5453" n="e" s="T714">konnambi. </ts>
               <ts e="T716" id="Seg_5455" n="e" s="T715">Dĭ </ts>
               <ts e="T717" id="Seg_5457" n="e" s="T716">bar </ts>
               <ts e="T718" id="Seg_5459" n="e" s="T717">dʼorlaʔbə. </ts>
               <ts e="T719" id="Seg_5461" n="e" s="T718">A </ts>
               <ts e="T720" id="Seg_5463" n="e" s="T719">pʼetux </ts>
               <ts e="T721" id="Seg_5465" n="e" s="T720">mălia: </ts>
               <ts e="T722" id="Seg_5467" n="e" s="T721">Ĭmbi </ts>
               <ts e="T723" id="Seg_5469" n="e" s="T722">(dʼorlaʔbəl), </ts>
               <ts e="T724" id="Seg_5471" n="e" s="T723">kanžəbəj! </ts>
               <ts e="T725" id="Seg_5473" n="e" s="T724">Tüjö </ts>
               <ts e="T726" id="Seg_5475" n="e" s="T725">ibəj </ts>
               <ts e="T727" id="Seg_5477" n="e" s="T726">tʼerməndə. </ts>
               <ts e="T728" id="Seg_5479" n="e" s="T727">Dĭ </ts>
               <ts e="T729" id="Seg_5481" n="e" s="T728">lattɨ </ts>
               <ts e="T730" id="Seg_5483" n="e" s="T729">kambiʔi. </ts>
               <ts e="T731" id="Seg_5485" n="e" s="T730">Pʼetux </ts>
               <ts e="T732" id="Seg_5487" n="e" s="T731">kirgarlaʔbə: </ts>
               <ts e="T733" id="Seg_5489" n="e" s="T732">Deʔ </ts>
               <ts e="T734" id="Seg_5491" n="e" s="T733">tʼermən, </ts>
               <ts e="T735" id="Seg_5493" n="e" s="T734">deʔ </ts>
               <ts e="T736" id="Seg_5495" n="e" s="T735">tʼermən! </ts>
               <ts e="T737" id="Seg_5497" n="e" s="T736">(D-) </ts>
               <ts e="T738" id="Seg_5499" n="e" s="T737">Dĭ </ts>
               <ts e="T739" id="Seg_5501" n="e" s="T738">kuza </ts>
               <ts e="T740" id="Seg_5503" n="e" s="T739">măndə: </ts>
               <ts e="T741" id="Seg_5505" n="e" s="T740">Barəʔtə </ts>
               <ts e="T742" id="Seg_5507" n="e" s="T741">dĭm </ts>
               <ts e="T743" id="Seg_5509" n="e" s="T742">(ineziʔ) </ts>
               <ts e="T744" id="Seg_5511" n="e" s="T743">ineʔinə. </ts>
               <ts e="T745" id="Seg_5513" n="e" s="T744">Barəʔluʔpi. </ts>
               <ts e="T746" id="Seg_5515" n="e" s="T745">Dĭ </ts>
               <ts e="T747" id="Seg_5517" n="e" s="T746">dĭʔə </ts>
               <ts e="T748" id="Seg_5519" n="e" s="T747">šobi, </ts>
               <ts e="T749" id="Seg_5521" n="e" s="T748">bazoʔ </ts>
               <ts e="T750" id="Seg_5523" n="e" s="T749">kirgarlaʔbə: </ts>
               <ts e="T751" id="Seg_5525" n="e" s="T750">(Отда-) </ts>
               <ts e="T752" id="Seg_5527" n="e" s="T751">Deʔtə </ts>
               <ts e="T753" id="Seg_5529" n="e" s="T752">tʼerməndə, </ts>
               <ts e="T754" id="Seg_5531" n="e" s="T753">deʔtə </ts>
               <ts e="T755" id="Seg_5533" n="e" s="T754">tʼerməndə! </ts>
               <ts e="T756" id="Seg_5535" n="e" s="T755">Dĭgəttə </ts>
               <ts e="T757" id="Seg_5537" n="e" s="T756">Barəʔtə </ts>
               <ts e="T758" id="Seg_5539" n="e" s="T757">(dĭ-) </ts>
               <ts e="T759" id="Seg_5541" n="e" s="T758">dĭm </ts>
               <ts e="T760" id="Seg_5543" n="e" s="T759">tüžöjdə. </ts>
               <ts e="T761" id="Seg_5545" n="e" s="T760">Barəʔluʔpi </ts>
               <ts e="T762" id="Seg_5547" n="e" s="T761">tüžöjdə. </ts>
               <ts e="T763" id="Seg_5549" n="e" s="T762">Dĭ </ts>
               <ts e="T764" id="Seg_5551" n="e" s="T763">tüžöjgən </ts>
               <ts e="T765" id="Seg_5553" n="e" s="T764">(tĭ-) </ts>
               <ts e="T766" id="Seg_5555" n="e" s="T765">bazoʔ </ts>
               <ts e="T767" id="Seg_5557" n="e" s="T766">šobi. </ts>
               <ts e="T768" id="Seg_5559" n="e" s="T767">Dĭgəttə </ts>
               <ts e="T769" id="Seg_5561" n="e" s="T768">bazoʔ </ts>
               <ts e="T770" id="Seg_5563" n="e" s="T769">măndə: </ts>
               <ts e="T771" id="Seg_5565" n="e" s="T770">(Дай </ts>
               <ts e="T772" id="Seg_5567" n="e" s="T771">tʼe-) </ts>
               <ts e="T773" id="Seg_5569" n="e" s="T772">Deʔtə </ts>
               <ts e="T774" id="Seg_5571" n="e" s="T773">tʼerməndə, </ts>
               <ts e="T775" id="Seg_5573" n="e" s="T774">deʔtə </ts>
               <ts e="T777" id="Seg_5575" n="e" s="T775">tʼerməndə! </ts>
               <ts e="T778" id="Seg_5577" n="e" s="T777">Barəʔtə </ts>
               <ts e="T779" id="Seg_5579" n="e" s="T778">ulardə! </ts>
               <ts e="T780" id="Seg_5581" n="e" s="T779">Barəʔluʔpi </ts>
               <ts e="T781" id="Seg_5583" n="e" s="T780">ulardə. </ts>
               <ts e="T782" id="Seg_5585" n="e" s="T781">Dĭgəttə </ts>
               <ts e="T783" id="Seg_5587" n="e" s="T782">bazoʔ </ts>
               <ts e="T784" id="Seg_5589" n="e" s="T783">šobi, </ts>
               <ts e="T785" id="Seg_5591" n="e" s="T784">bazoʔ </ts>
               <ts e="T786" id="Seg_5593" n="e" s="T785">kirgarlaʔbə: </ts>
               <ts e="T787" id="Seg_5595" n="e" s="T786">Deʔtə </ts>
               <ts e="T788" id="Seg_5597" n="e" s="T787">tʼerməndə, </ts>
               <ts e="T789" id="Seg_5599" n="e" s="T788">deʔtə </ts>
               <ts e="T790" id="Seg_5601" n="e" s="T789">tʼerməndə! </ts>
               <ts e="T791" id="Seg_5603" n="e" s="T790">Dĭgəttə </ts>
               <ts e="T792" id="Seg_5605" n="e" s="T791">măndə: </ts>
               <ts e="T793" id="Seg_5607" n="e" s="T792">Barəʔtə </ts>
               <ts e="T794" id="Seg_5609" n="e" s="T793">dĭm </ts>
               <ts e="T795" id="Seg_5611" n="e" s="T794">bünə! </ts>
               <ts e="T796" id="Seg_5613" n="e" s="T795">Dĭm </ts>
               <ts e="T797" id="Seg_5615" n="e" s="T796">barəʔluʔpi. </ts>
               <ts e="T798" id="Seg_5617" n="e" s="T797">A </ts>
               <ts e="T799" id="Seg_5619" n="e" s="T798">dĭ </ts>
               <ts e="T800" id="Seg_5621" n="e" s="T799">măndə: </ts>
               <ts e="T802" id="Seg_5623" n="e" s="T800">Kötenbə…" </ts>
               <ts e="T803" id="Seg_5625" n="e" s="T802">((BRK)). </ts>
               <ts e="T804" id="Seg_5627" n="e" s="T803">Barəʔluʔpi </ts>
               <ts e="T805" id="Seg_5629" n="e" s="T804">(dĭ=) </ts>
               <ts e="T806" id="Seg_5631" n="e" s="T805">dĭm </ts>
               <ts e="T807" id="Seg_5633" n="e" s="T806">bünə, </ts>
               <ts e="T808" id="Seg_5635" n="e" s="T807">a </ts>
               <ts e="T809" id="Seg_5637" n="e" s="T808">dĭ </ts>
               <ts e="T810" id="Seg_5639" n="e" s="T809">kirgarlaʔbə: </ts>
               <ts e="T811" id="Seg_5641" n="e" s="T810">Kötenbə </ts>
               <ts e="T812" id="Seg_5643" n="e" s="T811">bĭdeʔ </ts>
               <ts e="T813" id="Seg_5645" n="e" s="T812">bü, </ts>
               <ts e="T814" id="Seg_5647" n="e" s="T813">kötenbə </ts>
               <ts e="T815" id="Seg_5649" n="e" s="T814">bĭdeʔ </ts>
               <ts e="T816" id="Seg_5651" n="e" s="T815">bü. </ts>
               <ts e="T817" id="Seg_5653" n="e" s="T816">Kötenbə </ts>
               <ts e="T818" id="Seg_5655" n="e" s="T817">bar </ts>
               <ts e="T819" id="Seg_5657" n="e" s="T818">bü </ts>
               <ts e="T820" id="Seg_5659" n="e" s="T819">bĭtluʔpi, </ts>
               <ts e="T821" id="Seg_5661" n="e" s="T820">bazoʔ </ts>
               <ts e="T822" id="Seg_5663" n="e" s="T821">šobi. </ts>
               <ts e="T823" id="Seg_5665" n="e" s="T822">Deʔtə </ts>
               <ts e="T824" id="Seg_5667" n="e" s="T823">tʼerməndə, </ts>
               <ts e="T825" id="Seg_5669" n="e" s="T824">deʔtə </ts>
               <ts e="T826" id="Seg_5671" n="e" s="T825">tʼerməndə! </ts>
               <ts e="T827" id="Seg_5673" n="e" s="T826">Dĭgəttə </ts>
               <ts e="T828" id="Seg_5675" n="e" s="T827">dĭ </ts>
               <ts e="T829" id="Seg_5677" n="e" s="T828">kuza </ts>
               <ts e="T830" id="Seg_5679" n="e" s="T829">măndə: </ts>
               <ts e="T831" id="Seg_5681" n="e" s="T830">Šü </ts>
               <ts e="T832" id="Seg_5683" n="e" s="T831">(eŋg-) </ts>
               <ts e="T833" id="Seg_5685" n="e" s="T832">eŋgeʔ </ts>
               <ts e="T834" id="Seg_5687" n="e" s="T833">urgo </ts>
               <ts e="T835" id="Seg_5689" n="e" s="T834">i </ts>
               <ts e="T836" id="Seg_5691" n="e" s="T835">barəʔtə </ts>
               <ts e="T837" id="Seg_5693" n="e" s="T836">dĭm </ts>
               <ts e="T838" id="Seg_5695" n="e" s="T837">šünə. </ts>
               <ts e="T839" id="Seg_5697" n="e" s="T838">Dĭgəttə </ts>
               <ts e="T840" id="Seg_5699" n="e" s="T839">embiʔi </ts>
               <ts e="T841" id="Seg_5701" n="e" s="T840">šü, </ts>
               <ts e="T842" id="Seg_5703" n="e" s="T841">barəʔluʔpi </ts>
               <ts e="T843" id="Seg_5705" n="e" s="T842">dĭ </ts>
               <ts e="T844" id="Seg_5707" n="e" s="T843">pʼetugəm </ts>
               <ts e="T845" id="Seg_5709" n="e" s="T844">šünə, </ts>
               <ts e="T846" id="Seg_5711" n="e" s="T845">dibər </ts>
               <ts e="T847" id="Seg_5713" n="e" s="T846">măndə: </ts>
               <ts e="T848" id="Seg_5715" n="e" s="T847">Kămnaʔ </ts>
               <ts e="T849" id="Seg_5717" n="e" s="T848">kötenbə </ts>
               <ts e="T850" id="Seg_5719" n="e" s="T849">bü, </ts>
               <ts e="T851" id="Seg_5721" n="e" s="T850">kămnaʔ </ts>
               <ts e="T852" id="Seg_5723" n="e" s="T851">kötenbə </ts>
               <ts e="T853" id="Seg_5725" n="e" s="T852">bü! </ts>
               <ts e="T854" id="Seg_5727" n="e" s="T853">Kak </ts>
               <ts e="T855" id="Seg_5729" n="e" s="T854">bü </ts>
               <ts e="T856" id="Seg_5731" n="e" s="T855">kambi! </ts>
               <ts e="T857" id="Seg_5733" n="e" s="T856">Bar </ts>
               <ts e="T858" id="Seg_5735" n="e" s="T857">ĭmbi </ts>
               <ts e="T860" id="Seg_5737" n="e" s="T858">bar… </ts>
               <ts e="T861" id="Seg_5739" n="e" s="T860">((BRK)). </ts>
               <ts e="T862" id="Seg_5741" n="e" s="T861">Dĭgəttə </ts>
               <ts e="T863" id="Seg_5743" n="e" s="T862">bü </ts>
               <ts e="T864" id="Seg_5745" n="e" s="T863">ugandə </ts>
               <ts e="T865" id="Seg_5747" n="e" s="T864">iʔgö </ts>
               <ts e="T866" id="Seg_5749" n="e" s="T865">(mʼaŋi-) </ts>
               <ts e="T867" id="Seg_5751" n="e" s="T866">mʼambi, </ts>
               <ts e="T868" id="Seg_5753" n="e" s="T867">dĭ </ts>
               <ts e="T869" id="Seg_5755" n="e" s="T868">kuza </ts>
               <ts e="T870" id="Seg_5757" n="e" s="T869">măndə: </ts>
               <ts e="T871" id="Seg_5759" n="e" s="T870">Iʔ </ts>
               <ts e="T872" id="Seg_5761" n="e" s="T871">tʼerməndə </ts>
               <ts e="T873" id="Seg_5763" n="e" s="T872">i </ts>
               <ts e="T874" id="Seg_5765" n="e" s="T873">kanaʔ </ts>
               <ts e="T875" id="Seg_5767" n="e" s="T874">(döʔə </ts>
               <ts e="T876" id="Seg_5769" n="e" s="T875">döʔə=) </ts>
               <ts e="T877" id="Seg_5771" n="e" s="T876">döʔnə!" </ts>
               <ts e="T878" id="Seg_5773" n="e" s="T877">((BRK)). </ts>
               <ts e="T879" id="Seg_5775" n="e" s="T878">Погоди! </ts>
               <ts e="T880" id="Seg_5777" n="e" s="T879">((BRK)). </ts>
               <ts e="T881" id="Seg_5779" n="e" s="T880">(Xisəʔi) </ts>
               <ts e="T882" id="Seg_5781" n="e" s="T881">onʼiʔ </ts>
               <ts e="T883" id="Seg_5783" n="e" s="T882">(nul-) </ts>
               <ts e="T884" id="Seg_5785" n="e" s="T883">(nubiʔi), </ts>
               <ts e="T885" id="Seg_5787" n="e" s="T884">măn </ts>
               <ts e="T886" id="Seg_5789" n="e" s="T885">dĭzeŋ </ts>
               <ts e="T887" id="Seg_5791" n="e" s="T886">(oʔlu- </ts>
               <ts e="T888" id="Seg_5793" n="e" s="T887">ole-) </ts>
               <ts e="T889" id="Seg_5795" n="e" s="T888">(öʔlubiem). </ts>
               <ts e="T890" id="Seg_5797" n="e" s="T889">Dĭgəttə </ts>
               <ts e="T891" id="Seg_5799" n="e" s="T890">dĭzeŋ </ts>
               <ts e="T892" id="Seg_5801" n="e" s="T891">nuʔməluʔpiʔi. </ts>
               <ts e="T893" id="Seg_5803" n="e" s="T892">A </ts>
               <ts e="T894" id="Seg_5805" n="e" s="T893">dĭzeŋ </ts>
               <ts e="T895" id="Seg_5807" n="e" s="T894">ej </ts>
               <ts e="T896" id="Seg_5809" n="e" s="T895">nuʔməlieʔi. </ts>
               <ts e="T897" id="Seg_5811" n="e" s="T896">ɨrɨ </ts>
               <ts e="T898" id="Seg_5813" n="e" s="T897">molambiʔi. </ts>
               <ts e="T899" id="Seg_5815" n="e" s="T898">((BRK)). </ts>
               <ts e="T900" id="Seg_5817" n="e" s="T899">Taldʼen </ts>
               <ts e="T901" id="Seg_5819" n="e" s="T900">măn </ts>
               <ts e="T902" id="Seg_5821" n="e" s="T901">meim </ts>
               <ts e="T903" id="Seg_5823" n="e" s="T902">ej </ts>
               <ts e="T1025" id="Seg_5825" n="e" s="T903">kalla </ts>
               <ts e="T904" id="Seg_5827" n="e" s="T1025">dʼürbi </ts>
               <ts e="T905" id="Seg_5829" n="e" s="T904">maːʔndə. </ts>
               <ts e="T906" id="Seg_5831" n="e" s="T905">Măn </ts>
               <ts e="T907" id="Seg_5833" n="e" s="T906">mĭmbiem </ts>
               <ts e="T908" id="Seg_5835" n="e" s="T907">i </ts>
               <ts e="T909" id="Seg_5837" n="e" s="T908">dĭzi </ts>
               <ts e="T910" id="Seg_5839" n="e" s="T909">ej </ts>
               <ts e="T911" id="Seg_5841" n="e" s="T910">pănarbiam, </ts>
               <ts e="T912" id="Seg_5843" n="e" s="T911">udam </ts>
               <ts e="T913" id="Seg_5845" n="e" s="T912">ej </ts>
               <ts e="T914" id="Seg_5847" n="e" s="T913">mĭbiem. </ts>
               <ts e="T915" id="Seg_5849" n="e" s="T914">Dĭgəttə </ts>
               <ts e="T916" id="Seg_5851" n="e" s="T915">dĭ </ts>
               <ts e="T917" id="Seg_5853" n="e" s="T916">bazoʔ </ts>
               <ts e="T918" id="Seg_5855" n="e" s="T917">parluʔpi. </ts>
               <ts e="T919" id="Seg_5857" n="e" s="T918">Ugandə </ts>
               <ts e="T920" id="Seg_5859" n="e" s="T919">beržə, </ts>
               <ts e="T921" id="Seg_5861" n="e" s="T920">sĭre </ts>
               <ts e="T922" id="Seg_5863" n="e" s="T921">bar </ts>
               <ts e="T923" id="Seg_5865" n="e" s="T922">kundlaʔbə. </ts>
               <ts e="T924" id="Seg_5867" n="e" s="T923">Mašinaʔi </ts>
               <ts e="T925" id="Seg_5869" n="e" s="T924">ej </ts>
               <ts e="T926" id="Seg_5871" n="e" s="T925">kambiʔi, </ts>
               <ts e="T927" id="Seg_5873" n="e" s="T926">dĭ </ts>
               <ts e="T928" id="Seg_5875" n="e" s="T927">parluʔpi. </ts>
               <ts e="T929" id="Seg_5877" n="e" s="T928">Dön </ts>
               <ts e="T930" id="Seg_5879" n="e" s="T929">šaːbi. </ts>
               <ts e="T931" id="Seg_5881" n="e" s="T930">A </ts>
               <ts e="T932" id="Seg_5883" n="e" s="T931">teinen </ts>
               <ts e="T933" id="Seg_5885" n="e" s="T932">măn </ts>
               <ts e="T934" id="Seg_5887" n="e" s="T933">uʔpiam, </ts>
               <ts e="T935" id="Seg_5889" n="e" s="T934">mĭnzerbiem </ts>
               <ts e="T936" id="Seg_5891" n="e" s="T935">bar. </ts>
               <ts e="T937" id="Seg_5893" n="e" s="T936">Ipek </ts>
               <ts e="T938" id="Seg_5895" n="e" s="T937">pürbiem </ts>
               <ts e="T939" id="Seg_5897" n="e" s="T938">boskəndə </ts>
               <ts e="T940" id="Seg_5899" n="e" s="T939">aʔtʼinə. </ts>
               <ts e="T941" id="Seg_5901" n="e" s="T940">Amorbi, </ts>
               <ts e="T942" id="Seg_5903" n="e" s="T941">dĭgəttə </ts>
               <ts e="T1026" id="Seg_5905" n="e" s="T942">kalla </ts>
               <ts e="T943" id="Seg_5907" n="e" s="T1026">dʼürbi, </ts>
               <ts e="T944" id="Seg_5909" n="e" s="T943">mašinazi. </ts>
               <ts e="T945" id="Seg_5911" n="e" s="T944">Predsedatelʼzi. </ts>
               <ts e="T946" id="Seg_5913" n="e" s="T945">((BRK)). </ts>
               <ts e="T947" id="Seg_5915" n="e" s="T946">Dĭgəttə </ts>
               <ts e="T948" id="Seg_5917" n="e" s="T947">kallam </ts>
               <ts e="T949" id="Seg_5919" n="e" s="T948">maʔnʼi. </ts>
               <ts e="T950" id="Seg_5921" n="e" s="T949">Dĭgəttə </ts>
               <ts e="T951" id="Seg_5923" n="e" s="T950">(šobiam=) </ts>
               <ts e="T952" id="Seg_5925" n="e" s="T951">kallam </ts>
               <ts e="T953" id="Seg_5927" n="e" s="T952">maʔnʼi, </ts>
               <ts e="T954" id="Seg_5929" n="e" s="T953">a_to </ts>
               <ts e="T955" id="Seg_5931" n="e" s="T954">măn </ts>
               <ts e="T956" id="Seg_5933" n="e" s="T955">dĭn </ts>
               <ts e="T957" id="Seg_5935" n="e" s="T956">il </ts>
               <ts e="T958" id="Seg_5937" n="e" s="T957">amnolaʔbə, </ts>
               <ts e="T959" id="Seg_5939" n="e" s="T958">măna </ts>
               <ts e="T960" id="Seg_5941" n="e" s="T959">edəʔleʔbəʔjə. </ts>
               <ts e="T961" id="Seg_5943" n="e" s="T960">Dĭgəttə </ts>
               <ts e="T962" id="Seg_5945" n="e" s="T961">šobiam </ts>
               <ts e="T963" id="Seg_5947" n="e" s="T962">maːʔnʼi. </ts>
               <ts e="T964" id="Seg_5949" n="e" s="T963">(Dʼaparat) </ts>
               <ts e="T965" id="Seg_5951" n="e" s="T964">băzəbiam, </ts>
               <ts e="T966" id="Seg_5953" n="e" s="T965">(büzö) </ts>
               <ts e="T967" id="Seg_5955" n="e" s="T966">bĭtəlbiem. </ts>
               <ts e="T968" id="Seg_5957" n="e" s="T967">Dĭgəttə </ts>
               <ts e="T969" id="Seg_5959" n="e" s="T968">tüžöjbə </ts>
               <ts e="T970" id="Seg_5961" n="e" s="T969">sürerbiam </ts>
               <ts e="T971" id="Seg_5963" n="e" s="T970">bünə, </ts>
               <ts e="T972" id="Seg_5965" n="e" s="T971">bĭtəlbiem. </ts>
               <ts e="T973" id="Seg_5967" n="e" s="T972">Dĭgəttə </ts>
               <ts e="T974" id="Seg_5969" n="e" s="T973">(šonəg- </ts>
               <ts e="T975" id="Seg_5971" n="e" s="T974">šonəga </ts>
               <ts e="T976" id="Seg_5973" n="e" s="T975">m-) </ts>
               <ts e="T977" id="Seg_5975" n="e" s="T976">koʔbdo </ts>
               <ts e="T978" id="Seg_5977" n="e" s="T977">šonəga </ts>
               <ts e="T979" id="Seg_5979" n="e" s="T978">măna. </ts>
               <ts e="T980" id="Seg_5981" n="e" s="T979">Davaj </ts>
               <ts e="T981" id="Seg_5983" n="e" s="T980">kĭrzittə, </ts>
               <ts e="T982" id="Seg_5985" n="e" s="T981">măn </ts>
               <ts e="T983" id="Seg_5987" n="e" s="T982">nubiam, </ts>
               <ts e="T984" id="Seg_5989" n="e" s="T983">dĭ </ts>
               <ts e="T985" id="Seg_5991" n="e" s="T984">kĭrbi </ts>
               <ts e="T986" id="Seg_5993" n="e" s="T985">măna. </ts>
               <ts e="T987" id="Seg_5995" n="e" s="T986">Dĭgəttə </ts>
               <ts e="T988" id="Seg_5997" n="e" s="T987">(šobi- </ts>
               <ts e="T989" id="Seg_5999" n="e" s="T988">šobila-) </ts>
               <ts e="T990" id="Seg_6001" n="e" s="T989">šobibaʔ </ts>
               <ts e="T991" id="Seg_6003" n="e" s="T990">turanə, </ts>
               <ts e="T992" id="Seg_6005" n="e" s="T991">dʼăbaktərzittə. </ts>
               <ts e="T993" id="Seg_6007" n="e" s="T992">((BRK)). </ts>
               <ts e="T994" id="Seg_6009" n="e" s="T993">Dĭgəttə </ts>
               <ts e="T995" id="Seg_6011" n="e" s="T994">dʼăbaktərzittə </ts>
               <ts e="T996" id="Seg_6013" n="e" s="T995">plʼonkanə. </ts>
               <ts e="T997" id="Seg_6015" n="e" s="T996">((BRK)). </ts>
               <ts e="T998" id="Seg_6017" n="e" s="T997">Kurizəʔi </ts>
               <ts e="T999" id="Seg_6019" n="e" s="T998">bădəbiam. </ts>
               <ts e="T1000" id="Seg_6021" n="e" s="T999">Tüžöjn </ts>
               <ts e="T1001" id="Seg_6023" n="e" s="T1000">tüʔ </ts>
               <ts e="T1002" id="Seg_6025" n="e" s="T1001">barəʔpiam. </ts>
               <ts e="T1003" id="Seg_6027" n="e" s="T1002">((BRK)). </ts>
               <ts e="T1004" id="Seg_6029" n="e" s="T1003">Kabarləj </ts>
               <ts e="T1005" id="Seg_6031" n="e" s="T1004">dʼăbaktərzittə, </ts>
               <ts e="T1006" id="Seg_6033" n="e" s="T1005">a_to </ts>
               <ts e="T1007" id="Seg_6035" n="e" s="T1006">esseŋ </ts>
               <ts e="T1008" id="Seg_6037" n="e" s="T1007">büžü </ts>
               <ts e="T1009" id="Seg_6039" n="e" s="T1008">šoləʔi </ts>
               <ts e="T1010" id="Seg_6041" n="e" s="T1009">školagəʔ. </ts>
               <ts e="T1011" id="Seg_6043" n="e" s="T1010">Ej </ts>
               <ts e="T1012" id="Seg_6045" n="e" s="T1011">mĭləʔi </ts>
               <ts e="T1013" id="Seg_6047" n="e" s="T1012">dʼăbaktərzittə, </ts>
               <ts e="T1014" id="Seg_6049" n="e" s="T1013">bar </ts>
               <ts e="T1015" id="Seg_6051" n="e" s="T1014">kirgarləʔi. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-PKZ">
            <ta e="T2" id="Seg_6052" s="T0">PKZ_196X_SU0217.PKZ.001 (001)</ta>
            <ta e="T6" id="Seg_6053" s="T2">PKZ_196X_SU0217.PKZ.002 (002)</ta>
            <ta e="T9" id="Seg_6054" s="T6">PKZ_196X_SU0217.PKZ.003 (003)</ta>
            <ta e="T17" id="Seg_6055" s="T9">PKZ_196X_SU0217.PKZ.004 (004)</ta>
            <ta e="T18" id="Seg_6056" s="T17">PKZ_196X_SU0217.PKZ.005 (005)</ta>
            <ta e="T26" id="Seg_6057" s="T19">PKZ_196X_SU0217.PKZ.006 (006)</ta>
            <ta e="T31" id="Seg_6058" s="T26">PKZ_196X_SU0217.PKZ.007 (007)</ta>
            <ta e="T37" id="Seg_6059" s="T31">PKZ_196X_SU0217.PKZ.008 (008)</ta>
            <ta e="T41" id="Seg_6060" s="T37">PKZ_196X_SU0217.PKZ.009 (009)</ta>
            <ta e="T45" id="Seg_6061" s="T41">PKZ_196X_SU0217.PKZ.010 (010)</ta>
            <ta e="T49" id="Seg_6062" s="T45">PKZ_196X_SU0217.PKZ.011 (011)</ta>
            <ta e="T51" id="Seg_6063" s="T49">PKZ_196X_SU0217.PKZ.012 (012)</ta>
            <ta e="T53" id="Seg_6064" s="T51">PKZ_196X_SU0217.PKZ.013 (013)</ta>
            <ta e="T55" id="Seg_6065" s="T53">PKZ_196X_SU0217.PKZ.014 (014)</ta>
            <ta e="T62" id="Seg_6066" s="T55">PKZ_196X_SU0217.PKZ.015 (015)</ta>
            <ta e="T68" id="Seg_6067" s="T62">PKZ_196X_SU0217.PKZ.016 (016)</ta>
            <ta e="T71" id="Seg_6068" s="T68">PKZ_196X_SU0217.PKZ.017 (017)</ta>
            <ta e="T72" id="Seg_6069" s="T71">PKZ_196X_SU0217.PKZ.018 (018)</ta>
            <ta e="T82" id="Seg_6070" s="T72">PKZ_196X_SU0217.PKZ.019 (019)</ta>
            <ta e="T85" id="Seg_6071" s="T82">PKZ_196X_SU0217.PKZ.020 (020)</ta>
            <ta e="T88" id="Seg_6072" s="T85">PKZ_196X_SU0217.PKZ.021 (021)</ta>
            <ta e="T97" id="Seg_6073" s="T88">PKZ_196X_SU0217.PKZ.022 (022)</ta>
            <ta e="T98" id="Seg_6074" s="T97">PKZ_196X_SU0217.PKZ.023 (023)</ta>
            <ta e="T106" id="Seg_6075" s="T98">PKZ_196X_SU0217.PKZ.024 (024)</ta>
            <ta e="T109" id="Seg_6076" s="T106">PKZ_196X_SU0217.PKZ.025 (025)</ta>
            <ta e="T113" id="Seg_6077" s="T109">PKZ_196X_SU0217.PKZ.026 (026)</ta>
            <ta e="T114" id="Seg_6078" s="T113">PKZ_196X_SU0217.PKZ.027 (027)</ta>
            <ta e="T116" id="Seg_6079" s="T114">PKZ_196X_SU0217.PKZ.028 (028)</ta>
            <ta e="T121" id="Seg_6080" s="T116">PKZ_196X_SU0217.PKZ.029 (029)</ta>
            <ta e="T127" id="Seg_6081" s="T121">PKZ_196X_SU0217.PKZ.030 (030)</ta>
            <ta e="T133" id="Seg_6082" s="T127">PKZ_196X_SU0217.PKZ.031 (031)</ta>
            <ta e="T144" id="Seg_6083" s="T133">PKZ_196X_SU0217.PKZ.032 (032)</ta>
            <ta e="T151" id="Seg_6084" s="T144">PKZ_196X_SU0217.PKZ.033 (033)</ta>
            <ta e="T153" id="Seg_6085" s="T151">PKZ_196X_SU0217.PKZ.034 (034)</ta>
            <ta e="T157" id="Seg_6086" s="T153">PKZ_196X_SU0217.PKZ.035 (035)</ta>
            <ta e="T162" id="Seg_6087" s="T157">PKZ_196X_SU0217.PKZ.036 (036)</ta>
            <ta e="T167" id="Seg_6088" s="T162">PKZ_196X_SU0217.PKZ.037 (037)</ta>
            <ta e="T173" id="Seg_6089" s="T167">PKZ_196X_SU0217.PKZ.038 (038)</ta>
            <ta e="T178" id="Seg_6090" s="T173">PKZ_196X_SU0217.PKZ.039 (039)</ta>
            <ta e="T182" id="Seg_6091" s="T178">PKZ_196X_SU0217.PKZ.040 (040)</ta>
            <ta e="T184" id="Seg_6092" s="T182">PKZ_196X_SU0217.PKZ.041 (041)</ta>
            <ta e="T188" id="Seg_6093" s="T184">PKZ_196X_SU0217.PKZ.042 (042)</ta>
            <ta e="T191" id="Seg_6094" s="T188">PKZ_196X_SU0217.PKZ.043 (043)</ta>
            <ta e="T195" id="Seg_6095" s="T191">PKZ_196X_SU0217.PKZ.044 (044)</ta>
            <ta e="T202" id="Seg_6096" s="T195">PKZ_196X_SU0217.PKZ.045 (045)</ta>
            <ta e="T207" id="Seg_6097" s="T202">PKZ_196X_SU0217.PKZ.046 (046)</ta>
            <ta e="T209" id="Seg_6098" s="T207">PKZ_196X_SU0217.PKZ.047 (047)</ta>
            <ta e="T218" id="Seg_6099" s="T209">PKZ_196X_SU0217.PKZ.048 (048)</ta>
            <ta e="T222" id="Seg_6100" s="T218">PKZ_196X_SU0217.PKZ.049 (049)</ta>
            <ta e="T226" id="Seg_6101" s="T222">PKZ_196X_SU0217.PKZ.050 (050)</ta>
            <ta e="T228" id="Seg_6102" s="T226">PKZ_196X_SU0217.PKZ.051 (051)</ta>
            <ta e="T235" id="Seg_6103" s="T228">PKZ_196X_SU0217.PKZ.052 (052)</ta>
            <ta e="T239" id="Seg_6104" s="T235">PKZ_196X_SU0217.PKZ.053 (053)</ta>
            <ta e="T242" id="Seg_6105" s="T239">PKZ_196X_SU0217.PKZ.054 (054)</ta>
            <ta e="T245" id="Seg_6106" s="T242">PKZ_196X_SU0217.PKZ.055 (055)</ta>
            <ta e="T256" id="Seg_6107" s="T245">PKZ_196X_SU0217.PKZ.056 (056)</ta>
            <ta e="T257" id="Seg_6108" s="T256">PKZ_196X_SU0217.PKZ.057 (057)</ta>
            <ta e="T263" id="Seg_6109" s="T257">PKZ_196X_SU0217.PKZ.058 (058)</ta>
            <ta e="T278" id="Seg_6110" s="T263">PKZ_196X_SU0217.PKZ.059 (059)</ta>
            <ta e="T279" id="Seg_6111" s="T278">PKZ_196X_SU0217.PKZ.060 (060)</ta>
            <ta e="T288" id="Seg_6112" s="T279">PKZ_196X_SU0217.PKZ.061 (061)</ta>
            <ta e="T297" id="Seg_6113" s="T288">PKZ_196X_SU0217.PKZ.062 (062)</ta>
            <ta e="T304" id="Seg_6114" s="T297">PKZ_196X_SU0217.PKZ.063 (063)</ta>
            <ta e="T305" id="Seg_6115" s="T304">PKZ_196X_SU0217.PKZ.064 (064)</ta>
            <ta e="T313" id="Seg_6116" s="T305">PKZ_196X_SU0217.PKZ.065 (065)</ta>
            <ta e="T314" id="Seg_6117" s="T313">PKZ_196X_SU0217.PKZ.066 (066)</ta>
            <ta e="T321" id="Seg_6118" s="T314">PKZ_196X_SU0217.PKZ.067 (067)</ta>
            <ta e="T322" id="Seg_6119" s="T321">PKZ_196X_SU0217.PKZ.068 (068)</ta>
            <ta e="T325" id="Seg_6120" s="T322">PKZ_196X_SU0217.PKZ.069 (069)</ta>
            <ta e="T329" id="Seg_6121" s="T325">PKZ_196X_SU0217.PKZ.070 (070)</ta>
            <ta e="T335" id="Seg_6122" s="T329">PKZ_196X_SU0217.PKZ.071 (071)</ta>
            <ta e="T347" id="Seg_6123" s="T335">PKZ_196X_SU0217.PKZ.072 (072)</ta>
            <ta e="T350" id="Seg_6124" s="T347">PKZ_196X_SU0217.PKZ.073 (073)</ta>
            <ta e="T355" id="Seg_6125" s="T350">PKZ_196X_SU0217.PKZ.074 (074)</ta>
            <ta e="T361" id="Seg_6126" s="T355">PKZ_196X_SU0217.PKZ.075 (075)</ta>
            <ta e="T364" id="Seg_6127" s="T361">PKZ_196X_SU0217.PKZ.076 (076)</ta>
            <ta e="T374" id="Seg_6128" s="T364">PKZ_196X_SU0217.PKZ.077 (077)</ta>
            <ta e="T376" id="Seg_6129" s="T374">PKZ_196X_SU0217.PKZ.078 (078)</ta>
            <ta e="T390" id="Seg_6130" s="T376">PKZ_196X_SU0217.PKZ.079 (079)</ta>
            <ta e="T392" id="Seg_6131" s="T390">PKZ_196X_SU0217.PKZ.080 (080)</ta>
            <ta e="T397" id="Seg_6132" s="T392">PKZ_196X_SU0217.PKZ.081 (081)</ta>
            <ta e="T405" id="Seg_6133" s="T397">PKZ_196X_SU0217.PKZ.082 (082)</ta>
            <ta e="T411" id="Seg_6134" s="T405">PKZ_196X_SU0217.PKZ.083 (083)</ta>
            <ta e="T412" id="Seg_6135" s="T411">PKZ_196X_SU0217.PKZ.084 (084)</ta>
            <ta e="T415" id="Seg_6136" s="T412">PKZ_196X_SU0217.PKZ.085 (085)</ta>
            <ta e="T420" id="Seg_6137" s="T415">PKZ_196X_SU0217.PKZ.086 (086)</ta>
            <ta e="T430" id="Seg_6138" s="T420">PKZ_196X_SU0217.PKZ.087 (087)</ta>
            <ta e="T431" id="Seg_6139" s="T430">PKZ_196X_SU0217.PKZ.088 (088)</ta>
            <ta e="T438" id="Seg_6140" s="T431">PKZ_196X_SU0217.PKZ.089 (089)</ta>
            <ta e="T441" id="Seg_6141" s="T438">PKZ_196X_SU0217.PKZ.090 (090)</ta>
            <ta e="T445" id="Seg_6142" s="T441">PKZ_196X_SU0217.PKZ.091 (091)</ta>
            <ta e="T446" id="Seg_6143" s="T445">PKZ_196X_SU0217.PKZ.092 (092)</ta>
            <ta e="T455" id="Seg_6144" s="T446">PKZ_196X_SU0217.PKZ.093 (093)</ta>
            <ta e="T459" id="Seg_6145" s="T455">PKZ_196X_SU0217.PKZ.094 (094)</ta>
            <ta e="T462" id="Seg_6146" s="T459">PKZ_196X_SU0217.PKZ.095 (095)</ta>
            <ta e="T474" id="Seg_6147" s="T462">PKZ_196X_SU0217.PKZ.096 (096)</ta>
            <ta e="T488" id="Seg_6148" s="T474">PKZ_196X_SU0217.PKZ.097 (098)</ta>
            <ta e="T489" id="Seg_6149" s="T488">PKZ_196X_SU0217.PKZ.098 (099)</ta>
            <ta e="T494" id="Seg_6150" s="T489">PKZ_196X_SU0217.PKZ.099 (100)</ta>
            <ta e="T499" id="Seg_6151" s="T494">PKZ_196X_SU0217.PKZ.100 (101)</ta>
            <ta e="T506" id="Seg_6152" s="T499">PKZ_196X_SU0217.PKZ.101 (102)</ta>
            <ta e="T507" id="Seg_6153" s="T506">PKZ_196X_SU0217.PKZ.102 (103)</ta>
            <ta e="T509" id="Seg_6154" s="T507">PKZ_196X_SU0217.PKZ.103 (104)</ta>
            <ta e="T513" id="Seg_6155" s="T509">PKZ_196X_SU0217.PKZ.104 (105)</ta>
            <ta e="T516" id="Seg_6156" s="T513">PKZ_196X_SU0217.PKZ.105 (106)</ta>
            <ta e="T519" id="Seg_6157" s="T516">PKZ_196X_SU0217.PKZ.106 (107)</ta>
            <ta e="T524" id="Seg_6158" s="T519">PKZ_196X_SU0217.PKZ.107 (108)</ta>
            <ta e="T525" id="Seg_6159" s="T524">PKZ_196X_SU0217.PKZ.108 (109)</ta>
            <ta e="T527" id="Seg_6160" s="T525">PKZ_196X_SU0217.PKZ.109 (110)</ta>
            <ta e="T531" id="Seg_6161" s="T527">PKZ_196X_SU0217.PKZ.110 (111)</ta>
            <ta e="T534" id="Seg_6162" s="T531">PKZ_196X_SU0217.PKZ.111 (112)</ta>
            <ta e="T539" id="Seg_6163" s="T534">PKZ_196X_SU0217.PKZ.112 (113)</ta>
            <ta e="T545" id="Seg_6164" s="T539">PKZ_196X_SU0217.PKZ.113 (114)</ta>
            <ta e="T551" id="Seg_6165" s="T545">PKZ_196X_SU0217.PKZ.114 (115)</ta>
            <ta e="T557" id="Seg_6166" s="T551">PKZ_196X_SU0217.PKZ.115 (116)</ta>
            <ta e="T561" id="Seg_6167" s="T557">PKZ_196X_SU0217.PKZ.116 (117)</ta>
            <ta e="T565" id="Seg_6168" s="T561">PKZ_196X_SU0217.PKZ.117 (118)</ta>
            <ta e="T574" id="Seg_6169" s="T565">PKZ_196X_SU0217.PKZ.118 (119)</ta>
            <ta e="T585" id="Seg_6170" s="T574">PKZ_196X_SU0217.PKZ.119 (120)</ta>
            <ta e="T595" id="Seg_6171" s="T585">PKZ_196X_SU0217.PKZ.120 (121)</ta>
            <ta e="T603" id="Seg_6172" s="T595">PKZ_196X_SU0217.PKZ.121 (122)</ta>
            <ta e="T611" id="Seg_6173" s="T603">PKZ_196X_SU0217.PKZ.122 (123)</ta>
            <ta e="T622" id="Seg_6174" s="T611">PKZ_196X_SU0217.PKZ.123 (124)</ta>
            <ta e="T632" id="Seg_6175" s="T622">PKZ_196X_SU0217.PKZ.124 (125)</ta>
            <ta e="T641" id="Seg_6176" s="T632">PKZ_196X_SU0217.PKZ.125 (126)</ta>
            <ta e="T647" id="Seg_6177" s="T641">PKZ_196X_SU0217.PKZ.126 (127)</ta>
            <ta e="T648" id="Seg_6178" s="T647">PKZ_196X_SU0217.PKZ.127 (128)</ta>
            <ta e="T655" id="Seg_6179" s="T648">PKZ_196X_SU0217.PKZ.128 (129)</ta>
            <ta e="T664" id="Seg_6180" s="T655">PKZ_196X_SU0217.PKZ.129 (130)</ta>
            <ta e="T671" id="Seg_6181" s="T664">PKZ_196X_SU0217.PKZ.130 (131)</ta>
            <ta e="T672" id="Seg_6182" s="T671">PKZ_196X_SU0217.PKZ.131 (132)</ta>
            <ta e="T675" id="Seg_6183" s="T672">PKZ_196X_SU0217.PKZ.132 (133)</ta>
            <ta e="T676" id="Seg_6184" s="T675">PKZ_196X_SU0217.PKZ.133 (134)</ta>
            <ta e="T681" id="Seg_6185" s="T676">PKZ_196X_SU0217.PKZ.134 (135)</ta>
            <ta e="T686" id="Seg_6186" s="T681">PKZ_196X_SU0217.PKZ.135 (136)</ta>
            <ta e="T688" id="Seg_6187" s="T686">PKZ_196X_SU0217.PKZ.136 (137)</ta>
            <ta e="T689" id="Seg_6188" s="T688">PKZ_196X_SU0217.PKZ.137 (138)</ta>
            <ta e="T694" id="Seg_6189" s="T689">PKZ_196X_SU0217.PKZ.138 (139)</ta>
            <ta e="T703" id="Seg_6190" s="T694">PKZ_196X_SU0217.PKZ.139 (140)</ta>
            <ta e="T707" id="Seg_6191" s="T703">PKZ_196X_SU0217.PKZ.140 (141)</ta>
            <ta e="T715" id="Seg_6192" s="T707">PKZ_196X_SU0217.PKZ.141 (142)</ta>
            <ta e="T718" id="Seg_6193" s="T715">PKZ_196X_SU0217.PKZ.142 (143)</ta>
            <ta e="T724" id="Seg_6194" s="T718">PKZ_196X_SU0217.PKZ.143 (144)</ta>
            <ta e="T727" id="Seg_6195" s="T724">PKZ_196X_SU0217.PKZ.144 (145)</ta>
            <ta e="T730" id="Seg_6196" s="T727">PKZ_196X_SU0217.PKZ.145 (146)</ta>
            <ta e="T736" id="Seg_6197" s="T730">PKZ_196X_SU0217.PKZ.146 (147)</ta>
            <ta e="T744" id="Seg_6198" s="T736">PKZ_196X_SU0217.PKZ.147 (149)</ta>
            <ta e="T745" id="Seg_6199" s="T744">PKZ_196X_SU0217.PKZ.148 (150)</ta>
            <ta e="T755" id="Seg_6200" s="T745">PKZ_196X_SU0217.PKZ.149 (151) </ta>
            <ta e="T760" id="Seg_6201" s="T755">PKZ_196X_SU0217.PKZ.150 (153)</ta>
            <ta e="T762" id="Seg_6202" s="T760">PKZ_196X_SU0217.PKZ.151 (154)</ta>
            <ta e="T767" id="Seg_6203" s="T762">PKZ_196X_SU0217.PKZ.152 (155)</ta>
            <ta e="T777" id="Seg_6204" s="T767">PKZ_196X_SU0217.PKZ.153 (156)</ta>
            <ta e="T779" id="Seg_6205" s="T777">PKZ_196X_SU0217.PKZ.154 (158)</ta>
            <ta e="T781" id="Seg_6206" s="T779">PKZ_196X_SU0217.PKZ.155 (159)</ta>
            <ta e="T790" id="Seg_6207" s="T781">PKZ_196X_SU0217.PKZ.156 (160) </ta>
            <ta e="T795" id="Seg_6208" s="T790">PKZ_196X_SU0217.PKZ.157 (162)</ta>
            <ta e="T797" id="Seg_6209" s="T795">PKZ_196X_SU0217.PKZ.158 (163)</ta>
            <ta e="T802" id="Seg_6210" s="T797">PKZ_196X_SU0217.PKZ.159 (164)</ta>
            <ta e="T803" id="Seg_6211" s="T802">PKZ_196X_SU0217.PKZ.160 (165)</ta>
            <ta e="T816" id="Seg_6212" s="T803">PKZ_196X_SU0217.PKZ.161 (166)</ta>
            <ta e="T822" id="Seg_6213" s="T816">PKZ_196X_SU0217.PKZ.162 (167)</ta>
            <ta e="T826" id="Seg_6214" s="T822">PKZ_196X_SU0217.PKZ.163 (168)</ta>
            <ta e="T838" id="Seg_6215" s="T826">PKZ_196X_SU0217.PKZ.164 (169)</ta>
            <ta e="T853" id="Seg_6216" s="T838">PKZ_196X_SU0217.PKZ.165 (170) </ta>
            <ta e="T856" id="Seg_6217" s="T853">PKZ_196X_SU0217.PKZ.166 (172)</ta>
            <ta e="T860" id="Seg_6218" s="T856">PKZ_196X_SU0217.PKZ.167 (173)</ta>
            <ta e="T861" id="Seg_6219" s="T860">PKZ_196X_SU0217.PKZ.168 (174)</ta>
            <ta e="T877" id="Seg_6220" s="T861">PKZ_196X_SU0217.PKZ.169 (175) </ta>
            <ta e="T878" id="Seg_6221" s="T877">PKZ_196X_SU0217.PKZ.170 (177)</ta>
            <ta e="T879" id="Seg_6222" s="T878">PKZ_196X_SU0217.PKZ.171 (178.001)</ta>
            <ta e="T880" id="Seg_6223" s="T879">PKZ_196X_SU0217.PKZ.172 (178.002)</ta>
            <ta e="T889" id="Seg_6224" s="T880">PKZ_196X_SU0217.PKZ.173 (179)</ta>
            <ta e="T892" id="Seg_6225" s="T889">PKZ_196X_SU0217.PKZ.174 (180)</ta>
            <ta e="T896" id="Seg_6226" s="T892">PKZ_196X_SU0217.PKZ.175 (181)</ta>
            <ta e="T898" id="Seg_6227" s="T896">PKZ_196X_SU0217.PKZ.176 (182)</ta>
            <ta e="T899" id="Seg_6228" s="T898">PKZ_196X_SU0217.PKZ.177 (183)</ta>
            <ta e="T905" id="Seg_6229" s="T899">PKZ_196X_SU0217.PKZ.178 (184)</ta>
            <ta e="T914" id="Seg_6230" s="T905">PKZ_196X_SU0217.PKZ.179 (185)</ta>
            <ta e="T918" id="Seg_6231" s="T914">PKZ_196X_SU0217.PKZ.180 (186)</ta>
            <ta e="T923" id="Seg_6232" s="T918">PKZ_196X_SU0217.PKZ.181 (187)</ta>
            <ta e="T928" id="Seg_6233" s="T923">PKZ_196X_SU0217.PKZ.182 (188)</ta>
            <ta e="T930" id="Seg_6234" s="T928">PKZ_196X_SU0217.PKZ.183 (189)</ta>
            <ta e="T936" id="Seg_6235" s="T930">PKZ_196X_SU0217.PKZ.184 (190)</ta>
            <ta e="T940" id="Seg_6236" s="T936">PKZ_196X_SU0217.PKZ.185 (191)</ta>
            <ta e="T944" id="Seg_6237" s="T940">PKZ_196X_SU0217.PKZ.186 (192)</ta>
            <ta e="T945" id="Seg_6238" s="T944">PKZ_196X_SU0217.PKZ.187 (193)</ta>
            <ta e="T946" id="Seg_6239" s="T945">PKZ_196X_SU0217.PKZ.188 (194)</ta>
            <ta e="T949" id="Seg_6240" s="T946">PKZ_196X_SU0217.PKZ.189 (195)</ta>
            <ta e="T960" id="Seg_6241" s="T949">PKZ_196X_SU0217.PKZ.190 (196)</ta>
            <ta e="T963" id="Seg_6242" s="T960">PKZ_196X_SU0217.PKZ.191 (197)</ta>
            <ta e="T967" id="Seg_6243" s="T963">PKZ_196X_SU0217.PKZ.192 (198)</ta>
            <ta e="T972" id="Seg_6244" s="T967">PKZ_196X_SU0217.PKZ.193 (199)</ta>
            <ta e="T979" id="Seg_6245" s="T972">PKZ_196X_SU0217.PKZ.194 (200)</ta>
            <ta e="T986" id="Seg_6246" s="T979">PKZ_196X_SU0217.PKZ.195 (201)</ta>
            <ta e="T992" id="Seg_6247" s="T986">PKZ_196X_SU0217.PKZ.196 (202)</ta>
            <ta e="T993" id="Seg_6248" s="T992">PKZ_196X_SU0217.PKZ.197 (203)</ta>
            <ta e="T996" id="Seg_6249" s="T993">PKZ_196X_SU0217.PKZ.198 (204)</ta>
            <ta e="T997" id="Seg_6250" s="T996">PKZ_196X_SU0217.PKZ.199 (205)</ta>
            <ta e="T999" id="Seg_6251" s="T997">PKZ_196X_SU0217.PKZ.200 (206)</ta>
            <ta e="T1002" id="Seg_6252" s="T999">PKZ_196X_SU0217.PKZ.201 (207)</ta>
            <ta e="T1003" id="Seg_6253" s="T1002">PKZ_196X_SU0217.PKZ.202 (208)</ta>
            <ta e="T1010" id="Seg_6254" s="T1003">PKZ_196X_SU0217.PKZ.203 (209)</ta>
            <ta e="T1015" id="Seg_6255" s="T1010">PKZ_196X_SU0217.PKZ.204 (210)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-PKZ">
            <ta e="T2" id="Seg_6256" s="T0">Măn amnobiam. </ta>
            <ta e="T6" id="Seg_6257" s="T2">Sumna tibizeŋ ibiʔi măn. </ta>
            <ta e="T9" id="Seg_6258" s="T6">Dĭgəttə muktuʔ ibiem. </ta>
            <ta e="T17" id="Seg_6259" s="T9">Tĭ muktuʔ tibi ej amnobi, barəʔpi da kalla dʼürbi. </ta>
            <ta e="T18" id="Seg_6260" s="T17">Dĭgəttə… </ta>
            <ta e="T26" id="Seg_6261" s="T19">Первый tibim bügən külambi, nʼim bügən külambi. </ta>
            <ta e="T31" id="Seg_6262" s="T26">Dĭgəttə iam kuʔpiʔi, sʼestram kuʔpiʔi. </ta>
            <ta e="T37" id="Seg_6263" s="T31">Abam dʼijenə kambi i dĭn külambi. </ta>
            <ta e="T41" id="Seg_6264" s="T37">Măn sʼestram tibit kuʔpi. </ta>
            <ta e="T45" id="Seg_6265" s="T41">Šide (ni-) nʼi maːbi. </ta>
            <ta e="T49" id="Seg_6266" s="T45">Măn dĭzem bar özerbiem. </ta>
            <ta e="T51" id="Seg_6267" s="T49">Oldʼa ibiem. </ta>
            <ta e="T53" id="Seg_6268" s="T51">I tüšəlbiem. </ta>
            <ta e="T55" id="Seg_6269" s="T53">(Teŋziszɨ) sazən. </ta>
            <ta e="T62" id="Seg_6270" s="T55">Onʼiʔ был булгахтер, (dĭgət-) onʼiʔ măna amnobi. </ta>
            <ta e="T68" id="Seg_6271" s="T62">Dĭgəttə dĭm ibiʔi văjnanə, dĭn kuʔpiʔi. </ta>
            <ta e="T71" id="Seg_6272" s="T68">Dĭn koʔbdo maluʔpi. </ta>
            <ta e="T72" id="Seg_6273" s="T71">((BRK)). </ta>
            <ta e="T82" id="Seg_6274" s="T72">Măn (pʼešk- pʼeštə s-) kănnambiam, pʼeštə (s- š- sa-) sʼabiam. </ta>
            <ta e="T85" id="Seg_6275" s="T82">Amnobiam, iʔbobiam, kunolbiam. </ta>
            <ta e="T88" id="Seg_6276" s="T85">Sanə (lei-) lejlem. </ta>
            <ta e="T97" id="Seg_6277" s="T88">Dĭgəttə šiʔ šobiʔi, măn šobiam šiʔnʼileʔ i bar dʼăbaktərlaʔbəm. </ta>
            <ta e="T98" id="Seg_6278" s="T97">((BRK)). </ta>
            <ta e="T106" id="Seg_6279" s="T98">Nagur kaga amnobiʔi i dĭgəttə kambiʔi pa jaʔsʼittə. </ta>
            <ta e="T109" id="Seg_6280" s="T106">Dĭzeŋgən šü nagobi. </ta>
            <ta e="T113" id="Seg_6281" s="T109">(Tirdə) kubiʔi, šü nendleʔbə. </ta>
            <ta e="T114" id="Seg_6282" s="T113">"Kanaʔ! </ta>
            <ta e="T116" id="Seg_6283" s="T114">Iʔ šü!" </ta>
            <ta e="T121" id="Seg_6284" s="T116">Dĭ kambi, dĭn büzʼe amnolaʔbə. </ta>
            <ta e="T127" id="Seg_6285" s="T121">"(Nö-) Nörbaʔ сказка, dĭgəttə šü mĭlem". </ta>
            <ta e="T133" id="Seg_6286" s="T127">A dĭ măndə: "Măn ej tĭmnem". </ta>
            <ta e="T144" id="Seg_6287" s="T133">Dĭ bögəldə (i- ba- bădarbi= bădaʔ-) băʔpi, (remniʔ) nagur ремень băʔpi. </ta>
            <ta e="T151" id="Seg_6288" s="T144">Dĭ (kalal-) kalla dʼürbi, i šü ej mĭbi. </ta>
            <ta e="T153" id="Seg_6289" s="T151">Dĭgəttə … </ta>
            <ta e="T157" id="Seg_6290" s="T153">Dĭgəttə onʼiʔ bazoʔ kambi. </ta>
            <ta e="T162" id="Seg_6291" s="T157">I dĭʔnə mămbi: "Nörbaʔ сказка". </ta>
            <ta e="T167" id="Seg_6292" s="T162">Dĭ măndə:" Măn ej tĭmnem". </ta>
            <ta e="T173" id="Seg_6293" s="T167">No, dĭgəttə dĭn bar dʼagarbi bögəldə. </ta>
            <ta e="T178" id="Seg_6294" s="T173">I kambi, ej mĭbi šü. </ta>
            <ta e="T182" id="Seg_6295" s="T178">Dĭgəttə onʼiʔ bazoʔ šobi. </ta>
            <ta e="T184" id="Seg_6296" s="T182">"Nörbaʔ cказка!" </ta>
            <ta e="T188" id="Seg_6297" s="T184">No, dĭ măndə:" Nörbəlam". </ta>
            <ta e="T191" id="Seg_6298" s="T188">Bʼeʔ ibiʔi kagaʔi. </ta>
            <ta e="T195" id="Seg_6299" s="T191">Dĭgəttə kambiʔi pa jaʔsittə. </ta>
            <ta e="T202" id="Seg_6300" s="T195">Dĭgəttə (šojmu) suʔmiluʔpi, dĭ bar dʼünə kalla dʼürbi. </ta>
            <ta e="T207" id="Seg_6301" s="T202">Dĭgəttə nav šobi, maʔ abi. </ta>
            <ta e="T209" id="Seg_6302" s="T207">Munəʔi nʼebi. </ta>
            <ta e="T218" id="Seg_6303" s="T209">Dĭgəttə šobi dʼijegəʔ men urgo, i davaj munəʔi amzittə. </ta>
            <ta e="T222" id="Seg_6304" s="T218">Dĭ (dʼabə-) dʼabəluʔpi xvostə. </ta>
            <ta e="T226" id="Seg_6305" s="T222">Dĭ bar dĭm … </ta>
            <ta e="T228" id="Seg_6306" s="T226">запретил ((BRK)). </ta>
            <ta e="T235" id="Seg_6307" s="T228">((DMG)) Dĭgəttə dĭ (dʼabəluʔpi) xvostə, xvostə (dʼ-) sajnʼeʔpi. </ta>
            <ta e="T239" id="Seg_6308" s="T235">Dĭgəttə dagajzi (bă-) băʔpi. </ta>
            <ta e="T242" id="Seg_6309" s="T239">Dĭn jašɨk kubi. </ta>
            <ta e="T245" id="Seg_6310" s="T242">Jašɨkkən sazən (pʼaŋnomə). </ta>
            <ta e="T256" id="Seg_6311" s="T245">(Măn a-) Măn abam (tăn aba- abatsi=) tăn abandə tüʔsittə kalla dʼürbi. </ta>
            <ta e="T257" id="Seg_6312" s="T256">((BRK)). </ta>
            <ta e="T263" id="Seg_6313" s="T257">Dĭgəttə dĭ büzʼe mămbi:" Iʔ šamaʔ!" </ta>
            <ta e="T278" id="Seg_6314" s="T263">A (dĭ m- dĭ dĭnʼ) kuza dĭʔnə bögəldə ремни băʔpi i šü ibi i kalla dʼürbi. </ta>
            <ta e="T279" id="Seg_6315" s="T278">((BRK)). </ta>
            <ta e="T288" id="Seg_6316" s="T279">Măn meim măn tibi (ka- i-) ileʔpi i kalla dʼürbiʔi. </ta>
            <ta e="T297" id="Seg_6317" s="T288">Miʔ jakše amnobiʔi, a dĭ măna ajirbi, udanə ibi. </ta>
            <ta e="T304" id="Seg_6318" s="T297">A (dĭgəttə) kalla dʼürbi Igarkanə i dĭn külambi. </ta>
            <ta e="T305" id="Seg_6319" s="T304">((BRK)). </ta>
            <ta e="T313" id="Seg_6320" s="T305">A (dĭ) ej kuvas ibi, aŋdə bar … </ta>
            <ta e="T314" id="Seg_6321" s="T313">((BRK)). </ta>
            <ta e="T321" id="Seg_6322" s="T314">Dĭ ej kuvas, aŋdə păjdʼaŋ, sʼimat păjdʼaŋ. </ta>
            <ta e="T322" id="Seg_6323" s="T321">((BRK)). </ta>
            <ta e="T325" id="Seg_6324" s="T322">Šide (ne) amnobiʔi. </ta>
            <ta e="T329" id="Seg_6325" s="T325">Kambiʔi sarankaʔi (tĭldəz'-) tĭlzittə. </ta>
            <ta e="T335" id="Seg_6326" s="T329">Onʼiʔ tĭlbi iʔgö, a onʼiʔ amga. </ta>
            <ta e="T347" id="Seg_6327" s="T335">Girgit iʔgö tĭlbi, a girgit amga tĭlbi, dĭm (taʔ) baltuzi ulut jaʔpi. </ta>
            <ta e="T350" id="Seg_6328" s="T347">Dĭgəttə šobi maʔnə. </ta>
            <ta e="T355" id="Seg_6329" s="T350">Koʔbdo šobi:" Gijen măn iam?" </ta>
            <ta e="T361" id="Seg_6330" s="T355">"Dĭ bar ɨrɨː, ej tĭlbi sarankaʔi. </ta>
            <ta e="T364" id="Seg_6331" s="T361">Dĭn bar kunollaʔbə". </ta>
            <ta e="T374" id="Seg_6332" s="T364">A koʔbdot (kădedə) kăde - то (узн-) tĭmnebi i šobi. </ta>
            <ta e="T376" id="Seg_6333" s="T374">Nʼit ibi. </ta>
            <ta e="T390" id="Seg_6334" s="T376">Dĭ dĭm (šaʔla ibi), (kunol-) kunolzittə embi, a bostə nʼuʔdə (sa- š-) sʼabi maʔtə. </ta>
            <ta e="T392" id="Seg_6335" s="T390">Зола ibi. </ta>
            <ta e="T397" id="Seg_6336" s="T392">Dĭgəttə (dĭn-) dĭ ne šobi. </ta>
            <ta e="T405" id="Seg_6337" s="T397">"Gijen šiʔ (t-) dĭ măndə, măn dön amnolaʔbəm!" </ta>
            <ta e="T411" id="Seg_6338" s="T405">Dĭ (za-) зола (s-) kămnəbi sʼimandə. </ta>
            <ta e="T412" id="Seg_6339" s="T411">((BRK)). </ta>
            <ta e="T415" id="Seg_6340" s="T412">Ĭmbidə ej măndəliaʔi. </ta>
            <ta e="T420" id="Seg_6341" s="T415">Dĭgəttə sʼimat ĭmbidə ej moliaʔi. </ta>
            <ta e="T430" id="Seg_6342" s="T420">Dĭ suʔməluʔpi maʔtə, kagat ibi i kalla dʼürbi, gijen il iʔgö. </ta>
            <ta e="T431" id="Seg_6343" s="T430">((BRK)). </ta>
            <ta e="T438" id="Seg_6344" s="T431">Măn tibi kalla dʼürbi, măn ugandə tăŋ dʼorbiam. </ta>
            <ta e="T441" id="Seg_6345" s="T438">Sĭjbə bar ĭzembi. </ta>
            <ta e="T445" id="Seg_6346" s="T441">I ulum ((…)) ((DMG)). </ta>
            <ta e="T446" id="Seg_6347" s="T445">((BRK)). </ta>
            <ta e="T455" id="Seg_6348" s="T446">Măn (ser-) sʼestram (nʼi=) nʼit tʼerməndə mĭmbi, ipek ((DMG)). </ta>
            <ta e="T459" id="Seg_6349" s="T455">Tʼerməndə mĭbi, un nʼeʔsittə. </ta>
            <ta e="T462" id="Seg_6350" s="T459">Dĭgəttə maʔndə šobi. </ta>
            <ta e="T474" id="Seg_6351" s="T462">Bostə nenə mănlia: "Štobɨ kondʼo (kaba-) kabarləj, a măn amnoliam da tenöliam". </ta>
            <ta e="T488" id="Seg_6352" s="T474">"Ладно, što măn (dĭzi-) dĭzeŋzi, amorzittə не стало, to (mĭmbiʔi, boštə) măn iʔgö amniam". </ta>
            <ta e="T489" id="Seg_6353" s="T488">((BRK)). </ta>
            <ta e="T494" id="Seg_6354" s="T489">Onʼiʔ kuza monoʔkosʼtə (k-) kambi. </ta>
            <ta e="T499" id="Seg_6355" s="T494">Kuvas koʔbdo, ugandə oldʼat iʔgö. </ta>
            <ta e="T506" id="Seg_6356" s="T499">Kandəga, dĭgəttə kuza dĭʔnə măndə:" Gibər kandəgal?" </ta>
            <ta e="T507" id="Seg_6357" s="T506">"Monoʔkosʼtə". </ta>
            <ta e="T509" id="Seg_6358" s="T507">"Iʔ măna!" </ta>
            <ta e="T513" id="Seg_6359" s="T509">"A tăn ĭmbi alial?" </ta>
            <ta e="T516" id="Seg_6360" s="T513">"Amorzittə da tüʔsittə". </ta>
            <ta e="T519" id="Seg_6361" s="T516">Dĭgəttə kambiʔi bazoʔ. </ta>
            <ta e="T524" id="Seg_6362" s="T519">Bazoʔ kuza măndə:" Gibər kandəgalaʔ?" </ta>
            <ta e="T525" id="Seg_6363" s="T524">"Monoʔkosʼtə". </ta>
            <ta e="T527" id="Seg_6364" s="T525">"Măna it!" </ta>
            <ta e="T531" id="Seg_6365" s="T527">"A tăn ĭmbi aləl?" </ta>
            <ta e="T534" id="Seg_6366" s="T531">"Bĭʔsittə da (kĭnzəsʼtə)". </ta>
            <ta e="T539" id="Seg_6367" s="T534">Dĭgəttə šobiʔi i davaj monoʔkosʼtə. </ta>
            <ta e="T545" id="Seg_6368" s="T539">Dĭ măndə:" Măn iʔgö bar aləm. </ta>
            <ta e="T551" id="Seg_6369" s="T545">Ipek pürlem, uja, bar amzittə nada". </ta>
            <ta e="T557" id="Seg_6370" s="T551">Dĭ măndə:" No, bar măn amnam". </ta>
            <ta e="T561" id="Seg_6371" s="T557">"I ((PAUSE)) пива iʔgö. </ta>
            <ta e="T565" id="Seg_6372" s="T561">Bʼeʔ teʔtə ведро alam". </ta>
            <ta e="T574" id="Seg_6373" s="T565">No, dĭgəttə (š-) stoldə oʔbdəbi, dĭgəttə šobi dĭ kuza. </ta>
            <ta e="T585" id="Seg_6374" s="T574">Kajit amorzittə da tüʔsittə bar amnaʔbə da tüʔleʔbə, amnaʔbə da tüʔleʔbə. </ta>
            <ta e="T595" id="Seg_6375" s="T585">Bar ĭmbi amnuʔpi: i uja, i ((PAUSE)) ĭmbi ibi, bar. </ta>
            <ta e="T603" id="Seg_6376" s="T595">Bostə măndə:" Deʔkeʔ išo amga, deʔkeʔ išo amga". </ta>
            <ta e="T611" id="Seg_6377" s="T603">Dĭgəttə už amzittə (нечего ст -) нечего стало. </ta>
            <ta e="T622" id="Seg_6378" s="T611">Dĭgəttə (bü=) пиво bĭʔsittə (bazoʔ=) bazoʔ šobi (dĭ k-) dĭ kuza. </ta>
            <ta e="T632" id="Seg_6379" s="T622">Dĭgəttə dĭ (bĭ-) bĭtleʔbə, bĭtleʔbə da kĭnzleʔbə, bĭtleʔbə da kĭnzleʔbə. </ta>
            <ta e="T641" id="Seg_6380" s="T632">Bar bĭtluʔpi da kirgarlaʔbə:" Išo (d-) deʔ, măna amga!" </ta>
            <ta e="T647" id="Seg_6381" s="T641">A ulitsanə bar ugandə bü iʔgö. </ta>
            <ta e="T648" id="Seg_6382" s="T647">((BRK)). </ta>
            <ta e="T655" id="Seg_6383" s="T648">Dĭgəttə ej ambi da bü ej bĭʔpi. </ta>
            <ta e="T664" id="Seg_6384" s="T655">Dĭʔnə bɨ ulubə sajjaʔpiʔi bɨ i edəbiʔi, a to … </ta>
            <ta e="T671" id="Seg_6385" s="T664">Dĭgəttə dĭ koʔbdom ibi i maʔndə kambi. </ta>
            <ta e="T672" id="Seg_6386" s="T671">((BRK)). </ta>
            <ta e="T675" id="Seg_6387" s="T672">"Kaba, ĭmbi nüjleʔbəl?" </ta>
            <ta e="T676" id="Seg_6388" s="T675">"Коробочка". </ta>
            <ta e="T681" id="Seg_6389" s="T676">"A ĭmbi ej jakše nüjleʔbəl?" </ta>
            <ta e="T686" id="Seg_6390" s="T681">"Da нельзя jakše (nüjn-) nüjnəsʼtə". </ta>
            <ta e="T688" id="Seg_6391" s="T686">Kuiol (barlaʔina)". </ta>
            <ta e="T689" id="Seg_6392" s="T688">((BRK)). </ta>
            <ta e="T694" id="Seg_6393" s="T689">Onʼiʔ kuza (ibɨ-) ibi tʼerməndə. </ta>
            <ta e="T703" id="Seg_6394" s="T694">(A=) A onʼiʔ kuza ugandə aktʼa iʔgö (был=) ibi. </ta>
            <ta e="T707" id="Seg_6395" s="T703">I oldʼa, i ipek. </ta>
            <ta e="T715" id="Seg_6396" s="T707">(Dĭ=) Dĭn kuzanə ibi tʼerməndə i (ko-) konnambi. </ta>
            <ta e="T718" id="Seg_6397" s="T715">Dĭ bar dʼorlaʔbə. </ta>
            <ta e="T724" id="Seg_6398" s="T718">A pʼetux mălia:" Ĭmbi (dʼorlaʔbəl), kanžəbəj!" </ta>
            <ta e="T727" id="Seg_6399" s="T724">Tüjö ilbəj tʼerməndə". </ta>
            <ta e="T730" id="Seg_6400" s="T727">Dĭ lattɨ kambiʔi. </ta>
            <ta e="T736" id="Seg_6401" s="T730">Pʼetux kirgarlaʔbə: "Deʔ tʼermən, deʔ tʼermən!" </ta>
            <ta e="T744" id="Seg_6402" s="T736">(D-) Dĭ kuza măndə: "Barəʔtə dĭm (ineziʔ) ineʔinə". </ta>
            <ta e="T745" id="Seg_6403" s="T744">Barəʔluʔpi. </ta>
            <ta e="T755" id="Seg_6404" s="T745">Dĭ dĭʔə šobi, bazoʔ kirgarlaʔbə: "(Отда-) Deʔtə tʼerməndə, deʔtə tʼerməndə!" </ta>
            <ta e="T760" id="Seg_6405" s="T755">Dĭgəttə" Barəʔtə (dĭ-) dĭm tüžöjdə". </ta>
            <ta e="T762" id="Seg_6406" s="T760">Barəʔluʔpi tüžöjdə. </ta>
            <ta e="T767" id="Seg_6407" s="T762">Dĭ tüžöjgən (tĭ-) bazoʔ šobi. </ta>
            <ta e="T777" id="Seg_6408" s="T767">Dĭgəttə bazoʔ măndə: (Дай tʼe-) Deʔtə tʼerməndə, deʔtə tʼerməndə!" </ta>
            <ta e="T779" id="Seg_6409" s="T777">"Barəʔtə ulardə!" </ta>
            <ta e="T781" id="Seg_6410" s="T779">Barəʔluʔpi ulardə. </ta>
            <ta e="T790" id="Seg_6411" s="T781">Dĭgəttə bazoʔ šobi, bazoʔ kirgarlaʔbə: "Deʔtə tʼerməndə, deʔtə tʼerməndə!" </ta>
            <ta e="T795" id="Seg_6412" s="T790">Dĭgəttə măndə:" Barəʔtə dĭm bünə!" </ta>
            <ta e="T797" id="Seg_6413" s="T795">Dĭm barəʔluʔpi. </ta>
            <ta e="T802" id="Seg_6414" s="T797">A dĭ măndə:" Kötenbə …" </ta>
            <ta e="T803" id="Seg_6415" s="T802">((BRK)). </ta>
            <ta e="T816" id="Seg_6416" s="T803">Barəʔluʔpi (dĭ=) dĭm bünə, a dĭ kirgarlaʔbə: "Kötenbə bĭdeʔ bü, kötenbə bĭdeʔ bü". </ta>
            <ta e="T822" id="Seg_6417" s="T816">Kötenbə bar bü bĭtluʔpi, bazoʔ šobi. </ta>
            <ta e="T826" id="Seg_6418" s="T822">"Deʔtə tʼerməndə, deʔtə tʼerməndə!" </ta>
            <ta e="T838" id="Seg_6419" s="T826">Dĭgəttə dĭ kuza măndə:" Šü (eŋg-) eŋgeʔ urgo i barəʔtə dĭm šünə". </ta>
            <ta e="T853" id="Seg_6420" s="T838">Dĭgəttə embiʔi šü, barəʔluʔpi dĭ pʼetugəm šünə, dibər măndə: "Kămnaʔ kötenbə bü, kămnaʔ kötenbə bü!" </ta>
            <ta e="T856" id="Seg_6421" s="T853">Kak bü kambi! </ta>
            <ta e="T860" id="Seg_6422" s="T856">Bar ĭmbi bar … </ta>
            <ta e="T861" id="Seg_6423" s="T860">((BRK)). </ta>
            <ta e="T877" id="Seg_6424" s="T861">Dĭgəttə bü ugandə iʔgö (mʼaŋi-) mʼambi, dĭ kuza măndə: "Iʔ tʼerməndə i kanaʔ (döʔə döʔə=) döʔnə!" </ta>
            <ta e="T878" id="Seg_6425" s="T877">((BRK)). </ta>
            <ta e="T879" id="Seg_6426" s="T878">Погоди! </ta>
            <ta e="T880" id="Seg_6427" s="T879">((BRK)). </ta>
            <ta e="T889" id="Seg_6428" s="T880">(Xisəʔi) onʼiʔ (nul-) (nubiʔi), măn dĭzeŋ (oʔlu- ole-) (öʔlubiem). </ta>
            <ta e="T892" id="Seg_6429" s="T889">Dĭgəttə dĭzeŋ nuʔməluʔpiʔi. </ta>
            <ta e="T896" id="Seg_6430" s="T892">A dĭzeŋ ej nuʔməlieʔi. </ta>
            <ta e="T898" id="Seg_6431" s="T896">ɨrɨ molambiʔi. </ta>
            <ta e="T899" id="Seg_6432" s="T898">((BRK)). </ta>
            <ta e="T905" id="Seg_6433" s="T899">Taldʼen măn meim ej kalla dʼürbi maːʔndə. </ta>
            <ta e="T914" id="Seg_6434" s="T905">Măn mĭmbiem i dĭzi ej pănarbiam, udam ej mĭbiem. </ta>
            <ta e="T918" id="Seg_6435" s="T914">Dĭgəttə dĭ bazoʔ parluʔpi. </ta>
            <ta e="T923" id="Seg_6436" s="T918">Ugandə beržə, sĭre bar kundlaʔbə. </ta>
            <ta e="T928" id="Seg_6437" s="T923">Mašinaʔi ej kambiʔi, dĭ parluʔpi. </ta>
            <ta e="T930" id="Seg_6438" s="T928">Dön šaːbi. </ta>
            <ta e="T936" id="Seg_6439" s="T930">A teinen măn uʔpiam, mĭnzerbiem bar. </ta>
            <ta e="T940" id="Seg_6440" s="T936">Ipek pürbiem boskəndə aʔtʼinə. </ta>
            <ta e="T944" id="Seg_6441" s="T940">Amorbi, dĭgəttə kalla dʼürbi, mašinazi. </ta>
            <ta e="T945" id="Seg_6442" s="T944">Predsedatelʼzi. </ta>
            <ta e="T946" id="Seg_6443" s="T945">((BRK)). </ta>
            <ta e="T949" id="Seg_6444" s="T946">Dĭgəttə kallam maʔnʼi. </ta>
            <ta e="T960" id="Seg_6445" s="T949">Dĭgəttə (šobiam=) kallam maʔnʼi, a to măn dĭn il amnolaʔbə, măna edəʔleʔbəʔjə. </ta>
            <ta e="T963" id="Seg_6446" s="T960">Dĭgəttə šobiam maːʔnʼi. </ta>
            <ta e="T967" id="Seg_6447" s="T963">(Dʼaparat) băzəbiam, (büzö) bĭtəlbiem. </ta>
            <ta e="T972" id="Seg_6448" s="T967">Dĭgəttə tüžöjbə sürerbiam bünə, bĭtəlbiem. </ta>
            <ta e="T979" id="Seg_6449" s="T972">Dĭgəttə (šonəg- šonəga m-) koʔbdo šonəga măna. </ta>
            <ta e="T986" id="Seg_6450" s="T979">Davaj kĭrzittə, măn nubiam, dĭ kĭrbi măna. </ta>
            <ta e="T992" id="Seg_6451" s="T986">Dĭgəttə (šobi- šobila-) šobibaʔ turanə, dʼăbaktərzittə. </ta>
            <ta e="T993" id="Seg_6452" s="T992">((BRK)). </ta>
            <ta e="T996" id="Seg_6453" s="T993">Dĭgəttə dʼăbaktərzittə plʼonkanə. </ta>
            <ta e="T997" id="Seg_6454" s="T996">((BRK)). </ta>
            <ta e="T999" id="Seg_6455" s="T997">Kurizəʔi bădəbiam. </ta>
            <ta e="T1002" id="Seg_6456" s="T999">Tüžöjn tüʔ barəʔpiam. </ta>
            <ta e="T1003" id="Seg_6457" s="T1002">((BRK)). </ta>
            <ta e="T1010" id="Seg_6458" s="T1003">Kabarləj dʼăbaktərzittə, a to esseŋ büžü šoləʔi školagəʔ. </ta>
            <ta e="T1015" id="Seg_6459" s="T1010">Ej mĭləʔi dʼăbaktərzittə, bar kirgarləʔi. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-PKZ">
            <ta e="T1" id="Seg_6460" s="T0">măn</ta>
            <ta e="T2" id="Seg_6461" s="T1">amno-bia-m</ta>
            <ta e="T3" id="Seg_6462" s="T2">sumna</ta>
            <ta e="T4" id="Seg_6463" s="T3">tibi-zeŋ</ta>
            <ta e="T5" id="Seg_6464" s="T4">i-bi-ʔi</ta>
            <ta e="T6" id="Seg_6465" s="T5">măn</ta>
            <ta e="T7" id="Seg_6466" s="T6">dĭgəttə</ta>
            <ta e="T8" id="Seg_6467" s="T7">muktuʔ</ta>
            <ta e="T9" id="Seg_6468" s="T8">i-bie-m</ta>
            <ta e="T10" id="Seg_6469" s="T9">tĭ</ta>
            <ta e="T11" id="Seg_6470" s="T10">muktuʔ</ta>
            <ta e="T12" id="Seg_6471" s="T11">tibi</ta>
            <ta e="T13" id="Seg_6472" s="T12">ej</ta>
            <ta e="T14" id="Seg_6473" s="T13">amno-bi</ta>
            <ta e="T15" id="Seg_6474" s="T14">barəʔ-pi</ta>
            <ta e="T16" id="Seg_6475" s="T15">da</ta>
            <ta e="T776" id="Seg_6476" s="T16">kal-la</ta>
            <ta e="T17" id="Seg_6477" s="T776">dʼür-bi</ta>
            <ta e="T18" id="Seg_6478" s="T17">dĭgəttə</ta>
            <ta e="T21" id="Seg_6479" s="T20">tibi-m</ta>
            <ta e="T22" id="Seg_6480" s="T21">bü-gən</ta>
            <ta e="T23" id="Seg_6481" s="T22">kü-lam-bi</ta>
            <ta e="T24" id="Seg_6482" s="T23">nʼi-m</ta>
            <ta e="T25" id="Seg_6483" s="T24">bü-gən</ta>
            <ta e="T26" id="Seg_6484" s="T25">kü-lam-bi</ta>
            <ta e="T27" id="Seg_6485" s="T26">dĭgəttə</ta>
            <ta e="T28" id="Seg_6486" s="T27">ia-m</ta>
            <ta e="T29" id="Seg_6487" s="T28">kuʔ-pi-ʔi</ta>
            <ta e="T30" id="Seg_6488" s="T29">sʼestra-m</ta>
            <ta e="T31" id="Seg_6489" s="T30">kuʔ-pi-ʔi</ta>
            <ta e="T32" id="Seg_6490" s="T31">aba-m</ta>
            <ta e="T33" id="Seg_6491" s="T32">dʼije-nə</ta>
            <ta e="T34" id="Seg_6492" s="T33">kam-bi</ta>
            <ta e="T35" id="Seg_6493" s="T34">i</ta>
            <ta e="T36" id="Seg_6494" s="T35">dĭn</ta>
            <ta e="T37" id="Seg_6495" s="T36">kü-lam-bi</ta>
            <ta e="T38" id="Seg_6496" s="T37">măn</ta>
            <ta e="T39" id="Seg_6497" s="T38">sʼestra-m</ta>
            <ta e="T40" id="Seg_6498" s="T39">tibi-t</ta>
            <ta e="T41" id="Seg_6499" s="T40">kuʔ-pi</ta>
            <ta e="T42" id="Seg_6500" s="T41">šide</ta>
            <ta e="T44" id="Seg_6501" s="T43">nʼi</ta>
            <ta e="T45" id="Seg_6502" s="T44">ma-bi</ta>
            <ta e="T46" id="Seg_6503" s="T45">măn</ta>
            <ta e="T47" id="Seg_6504" s="T46">dĭ-zem</ta>
            <ta e="T48" id="Seg_6505" s="T47">bar</ta>
            <ta e="T49" id="Seg_6506" s="T48">özer-bie-m</ta>
            <ta e="T50" id="Seg_6507" s="T49">oldʼa</ta>
            <ta e="T51" id="Seg_6508" s="T50">i-bie-m</ta>
            <ta e="T52" id="Seg_6509" s="T51">i</ta>
            <ta e="T53" id="Seg_6510" s="T52">tüšə-l-bie-m</ta>
            <ta e="T55" id="Seg_6511" s="T54">sazən</ta>
            <ta e="T56" id="Seg_6512" s="T55">onʼiʔ</ta>
            <ta e="T60" id="Seg_6513" s="T59">onʼiʔ</ta>
            <ta e="T61" id="Seg_6514" s="T60">măna</ta>
            <ta e="T62" id="Seg_6515" s="T61">amno-bi</ta>
            <ta e="T63" id="Seg_6516" s="T62">dĭgəttə</ta>
            <ta e="T64" id="Seg_6517" s="T63">dĭ-m</ta>
            <ta e="T65" id="Seg_6518" s="T64">i-bi-ʔi</ta>
            <ta e="T66" id="Seg_6519" s="T65">văjna-nə</ta>
            <ta e="T67" id="Seg_6520" s="T66">dĭn</ta>
            <ta e="T68" id="Seg_6521" s="T67">kuʔ-pi-ʔi</ta>
            <ta e="T69" id="Seg_6522" s="T68">dĭ-n</ta>
            <ta e="T70" id="Seg_6523" s="T69">koʔbdo</ta>
            <ta e="T71" id="Seg_6524" s="T70">ma-luʔ-pi</ta>
            <ta e="T73" id="Seg_6525" s="T72">măn</ta>
            <ta e="T75" id="Seg_6526" s="T74">pʼeš-tə</ta>
            <ta e="T77" id="Seg_6527" s="T76">kăn-nam-bia-m</ta>
            <ta e="T78" id="Seg_6528" s="T77">pʼeš-tə</ta>
            <ta e="T82" id="Seg_6529" s="T81">sʼa-bia-m</ta>
            <ta e="T83" id="Seg_6530" s="T82">amno-bia-m</ta>
            <ta e="T84" id="Seg_6531" s="T83">iʔbo-bia-m</ta>
            <ta e="T85" id="Seg_6532" s="T84">kunol-bia-m</ta>
            <ta e="T86" id="Seg_6533" s="T85">sanə</ta>
            <ta e="T88" id="Seg_6534" s="T87">lej-le-m</ta>
            <ta e="T89" id="Seg_6535" s="T88">dĭgəttə</ta>
            <ta e="T90" id="Seg_6536" s="T89">šiʔ</ta>
            <ta e="T91" id="Seg_6537" s="T90">šo-bi-ʔi</ta>
            <ta e="T92" id="Seg_6538" s="T91">măn</ta>
            <ta e="T93" id="Seg_6539" s="T92">šo-bia-m</ta>
            <ta e="T94" id="Seg_6540" s="T93">šiʔnʼileʔ</ta>
            <ta e="T95" id="Seg_6541" s="T94">i</ta>
            <ta e="T96" id="Seg_6542" s="T95">bar</ta>
            <ta e="T97" id="Seg_6543" s="T96">dʼăbaktər-laʔbə-m</ta>
            <ta e="T99" id="Seg_6544" s="T98">nagur</ta>
            <ta e="T100" id="Seg_6545" s="T99">kaga</ta>
            <ta e="T101" id="Seg_6546" s="T100">amno-bi-ʔi</ta>
            <ta e="T102" id="Seg_6547" s="T101">i</ta>
            <ta e="T103" id="Seg_6548" s="T102">dĭgəttə</ta>
            <ta e="T104" id="Seg_6549" s="T103">kam-bi-ʔi</ta>
            <ta e="T105" id="Seg_6550" s="T104">pa</ta>
            <ta e="T106" id="Seg_6551" s="T105">jaʔ-sʼittə</ta>
            <ta e="T107" id="Seg_6552" s="T106">dĭ-zeŋ-gən</ta>
            <ta e="T108" id="Seg_6553" s="T107">šü</ta>
            <ta e="T109" id="Seg_6554" s="T108">nago-bi</ta>
            <ta e="T111" id="Seg_6555" s="T110">ku-bi-ʔi</ta>
            <ta e="T112" id="Seg_6556" s="T111">šü</ta>
            <ta e="T113" id="Seg_6557" s="T112">nend-leʔbə</ta>
            <ta e="T114" id="Seg_6558" s="T113">kan-a-ʔ</ta>
            <ta e="T115" id="Seg_6559" s="T114">i-ʔ</ta>
            <ta e="T116" id="Seg_6560" s="T115">šü</ta>
            <ta e="T117" id="Seg_6561" s="T116">dĭ</ta>
            <ta e="T118" id="Seg_6562" s="T117">kam-bi</ta>
            <ta e="T119" id="Seg_6563" s="T118">dĭn</ta>
            <ta e="T120" id="Seg_6564" s="T119">büzʼe</ta>
            <ta e="T121" id="Seg_6565" s="T120">amno-laʔbə</ta>
            <ta e="T123" id="Seg_6566" s="T122">nörba-ʔ</ta>
            <ta e="T125" id="Seg_6567" s="T124">dĭgəttə</ta>
            <ta e="T126" id="Seg_6568" s="T125">šü</ta>
            <ta e="T127" id="Seg_6569" s="T126">mĭ-le-m</ta>
            <ta e="T128" id="Seg_6570" s="T127">a</ta>
            <ta e="T129" id="Seg_6571" s="T128">dĭ</ta>
            <ta e="T130" id="Seg_6572" s="T129">măn-də</ta>
            <ta e="T131" id="Seg_6573" s="T130">măn</ta>
            <ta e="T132" id="Seg_6574" s="T131">ej</ta>
            <ta e="T133" id="Seg_6575" s="T132">tĭmne-m</ta>
            <ta e="T134" id="Seg_6576" s="T133">dĭ</ta>
            <ta e="T135" id="Seg_6577" s="T134">bögəl-də</ta>
            <ta e="T140" id="Seg_6578" s="T139">băʔ-pi</ta>
            <ta e="T142" id="Seg_6579" s="T141">nagur</ta>
            <ta e="T144" id="Seg_6580" s="T143">băʔ-pi</ta>
            <ta e="T145" id="Seg_6581" s="T144">dĭ</ta>
            <ta e="T1017" id="Seg_6582" s="T146">kal-la</ta>
            <ta e="T147" id="Seg_6583" s="T1017">dʼür-bi</ta>
            <ta e="T148" id="Seg_6584" s="T147">i</ta>
            <ta e="T149" id="Seg_6585" s="T148">šü</ta>
            <ta e="T150" id="Seg_6586" s="T149">ej</ta>
            <ta e="T151" id="Seg_6587" s="T150">mĭ-bi</ta>
            <ta e="T153" id="Seg_6588" s="T151">dĭgəttə</ta>
            <ta e="T154" id="Seg_6589" s="T153">dĭgəttə</ta>
            <ta e="T155" id="Seg_6590" s="T154">onʼiʔ</ta>
            <ta e="T156" id="Seg_6591" s="T155">bazoʔ</ta>
            <ta e="T157" id="Seg_6592" s="T156">kam-bi</ta>
            <ta e="T158" id="Seg_6593" s="T157">i</ta>
            <ta e="T159" id="Seg_6594" s="T158">dĭʔ-nə</ta>
            <ta e="T160" id="Seg_6595" s="T159">măm-bi</ta>
            <ta e="T161" id="Seg_6596" s="T160">nörba-ʔ</ta>
            <ta e="T163" id="Seg_6597" s="T162">dĭ</ta>
            <ta e="T164" id="Seg_6598" s="T163">măn-də</ta>
            <ta e="T165" id="Seg_6599" s="T164">măn</ta>
            <ta e="T166" id="Seg_6600" s="T165">ej</ta>
            <ta e="T167" id="Seg_6601" s="T166">tĭmne-m</ta>
            <ta e="T168" id="Seg_6602" s="T167">no</ta>
            <ta e="T169" id="Seg_6603" s="T168">dĭgəttə</ta>
            <ta e="T170" id="Seg_6604" s="T169">dĭn</ta>
            <ta e="T171" id="Seg_6605" s="T170">bar</ta>
            <ta e="T172" id="Seg_6606" s="T171">dʼagar-bi</ta>
            <ta e="T173" id="Seg_6607" s="T172">bögəl-də</ta>
            <ta e="T174" id="Seg_6608" s="T173">i</ta>
            <ta e="T175" id="Seg_6609" s="T174">kam-bi</ta>
            <ta e="T176" id="Seg_6610" s="T175">ej</ta>
            <ta e="T177" id="Seg_6611" s="T176">mĭ-bi</ta>
            <ta e="T178" id="Seg_6612" s="T177">šü</ta>
            <ta e="T179" id="Seg_6613" s="T178">dĭgəttə</ta>
            <ta e="T180" id="Seg_6614" s="T179">onʼiʔ</ta>
            <ta e="T181" id="Seg_6615" s="T180">bazoʔ</ta>
            <ta e="T182" id="Seg_6616" s="T181">šo-bi</ta>
            <ta e="T183" id="Seg_6617" s="T182">nörba-ʔ</ta>
            <ta e="T185" id="Seg_6618" s="T184">no</ta>
            <ta e="T186" id="Seg_6619" s="T185">dĭ</ta>
            <ta e="T187" id="Seg_6620" s="T186">măn-də</ta>
            <ta e="T188" id="Seg_6621" s="T187">nörbə-la-m</ta>
            <ta e="T189" id="Seg_6622" s="T188">bʼeʔ</ta>
            <ta e="T190" id="Seg_6623" s="T189">i-bi-ʔi</ta>
            <ta e="T191" id="Seg_6624" s="T190">kaga-ʔi</ta>
            <ta e="T192" id="Seg_6625" s="T191">dĭgəttə</ta>
            <ta e="T193" id="Seg_6626" s="T192">kam-bi-ʔi</ta>
            <ta e="T194" id="Seg_6627" s="T193">pa</ta>
            <ta e="T195" id="Seg_6628" s="T194">jaʔ-sittə</ta>
            <ta e="T196" id="Seg_6629" s="T195">dĭgəttə</ta>
            <ta e="T197" id="Seg_6630" s="T196">šojmu</ta>
            <ta e="T198" id="Seg_6631" s="T197">suʔmi-luʔ-pi</ta>
            <ta e="T199" id="Seg_6632" s="T198">dĭ</ta>
            <ta e="T200" id="Seg_6633" s="T199">bar</ta>
            <ta e="T201" id="Seg_6634" s="T200">dʼü-nə</ta>
            <ta e="T1018" id="Seg_6635" s="T201">kal-la</ta>
            <ta e="T202" id="Seg_6636" s="T1018">dʼür-bi</ta>
            <ta e="T203" id="Seg_6637" s="T202">dĭgəttə</ta>
            <ta e="T204" id="Seg_6638" s="T203">nav</ta>
            <ta e="T205" id="Seg_6639" s="T204">šo-bi</ta>
            <ta e="T206" id="Seg_6640" s="T205">maʔ</ta>
            <ta e="T207" id="Seg_6641" s="T206">a-bi</ta>
            <ta e="T208" id="Seg_6642" s="T207">munə-ʔi</ta>
            <ta e="T209" id="Seg_6643" s="T208">nʼe-bi</ta>
            <ta e="T210" id="Seg_6644" s="T209">dĭgəttə</ta>
            <ta e="T211" id="Seg_6645" s="T210">šo-bi</ta>
            <ta e="T212" id="Seg_6646" s="T211">dʼije-gəʔ</ta>
            <ta e="T213" id="Seg_6647" s="T212">men</ta>
            <ta e="T214" id="Seg_6648" s="T213">urgo</ta>
            <ta e="T215" id="Seg_6649" s="T214">i</ta>
            <ta e="T216" id="Seg_6650" s="T215">davaj</ta>
            <ta e="T217" id="Seg_6651" s="T216">munə-ʔi</ta>
            <ta e="T218" id="Seg_6652" s="T217">am-zittə</ta>
            <ta e="T219" id="Seg_6653" s="T218">dĭ</ta>
            <ta e="T221" id="Seg_6654" s="T220">dʼabə-luʔ-pi</ta>
            <ta e="T222" id="Seg_6655" s="T221">xvostə</ta>
            <ta e="T223" id="Seg_6656" s="T222">dĭ</ta>
            <ta e="T224" id="Seg_6657" s="T223">bar</ta>
            <ta e="T226" id="Seg_6658" s="T224">dĭ-m</ta>
            <ta e="T229" id="Seg_6659" s="T228">dĭgəttə</ta>
            <ta e="T230" id="Seg_6660" s="T229">dĭ</ta>
            <ta e="T231" id="Seg_6661" s="T230">dʼabə-luʔ-pi</ta>
            <ta e="T232" id="Seg_6662" s="T231">xvostə</ta>
            <ta e="T233" id="Seg_6663" s="T232">xvostə</ta>
            <ta e="T235" id="Seg_6664" s="T234">saj_nʼeʔ-pi</ta>
            <ta e="T236" id="Seg_6665" s="T235">dĭgəttə</ta>
            <ta e="T237" id="Seg_6666" s="T236">dagaj-zi</ta>
            <ta e="T239" id="Seg_6667" s="T238">băʔ-pi</ta>
            <ta e="T240" id="Seg_6668" s="T239">dĭn</ta>
            <ta e="T241" id="Seg_6669" s="T240">jašɨk</ta>
            <ta e="T242" id="Seg_6670" s="T241">ku-bi</ta>
            <ta e="T243" id="Seg_6671" s="T242">jašɨk-kən</ta>
            <ta e="T244" id="Seg_6672" s="T243">sazən</ta>
            <ta e="T245" id="Seg_6673" s="T244">pʼaŋnomə</ta>
            <ta e="T246" id="Seg_6674" s="T245">măn</ta>
            <ta e="T248" id="Seg_6675" s="T247">măn</ta>
            <ta e="T249" id="Seg_6676" s="T248">aba-m</ta>
            <ta e="T250" id="Seg_6677" s="T249">tăn</ta>
            <ta e="T252" id="Seg_6678" s="T251">aba-t-si</ta>
            <ta e="T253" id="Seg_6679" s="T252">tăn</ta>
            <ta e="T254" id="Seg_6680" s="T253">aba-ndə</ta>
            <ta e="T255" id="Seg_6681" s="T254">tüʔ-sittə</ta>
            <ta e="T1019" id="Seg_6682" s="T255">kal-la</ta>
            <ta e="T256" id="Seg_6683" s="T1019">dʼür-bi</ta>
            <ta e="T258" id="Seg_6684" s="T257">dĭgəttə</ta>
            <ta e="T259" id="Seg_6685" s="T258">dĭ</ta>
            <ta e="T260" id="Seg_6686" s="T259">büzʼe</ta>
            <ta e="T261" id="Seg_6687" s="T260">măm-bi</ta>
            <ta e="T262" id="Seg_6688" s="T261">i-ʔ</ta>
            <ta e="T263" id="Seg_6689" s="T262">šama-ʔ</ta>
            <ta e="T264" id="Seg_6690" s="T263">a</ta>
            <ta e="T265" id="Seg_6691" s="T264">dĭ</ta>
            <ta e="T267" id="Seg_6692" s="T266">dĭ</ta>
            <ta e="T269" id="Seg_6693" s="T268">kuza</ta>
            <ta e="T270" id="Seg_6694" s="T269">dĭʔ-nə</ta>
            <ta e="T271" id="Seg_6695" s="T270">bögəl-də</ta>
            <ta e="T273" id="Seg_6696" s="T272">băʔ-pi</ta>
            <ta e="T274" id="Seg_6697" s="T273">i</ta>
            <ta e="T275" id="Seg_6698" s="T274">šü</ta>
            <ta e="T276" id="Seg_6699" s="T275">i-bi</ta>
            <ta e="T277" id="Seg_6700" s="T276">i</ta>
            <ta e="T1020" id="Seg_6701" s="T277">kal-la</ta>
            <ta e="T278" id="Seg_6702" s="T1020">dʼür-bi</ta>
            <ta e="T280" id="Seg_6703" s="T279">măn</ta>
            <ta e="T281" id="Seg_6704" s="T280">mei-m</ta>
            <ta e="T282" id="Seg_6705" s="T281">măn</ta>
            <ta e="T283" id="Seg_6706" s="T282">tibi</ta>
            <ta e="T286" id="Seg_6707" s="T285">i-leʔ-pi</ta>
            <ta e="T287" id="Seg_6708" s="T286">i</ta>
            <ta e="T1021" id="Seg_6709" s="T287">kal-la</ta>
            <ta e="T288" id="Seg_6710" s="T1021">dʼür-bi-ʔi</ta>
            <ta e="T289" id="Seg_6711" s="T288">miʔ</ta>
            <ta e="T290" id="Seg_6712" s="T289">jakše</ta>
            <ta e="T291" id="Seg_6713" s="T290">amno-bi-ʔi</ta>
            <ta e="T292" id="Seg_6714" s="T291">a</ta>
            <ta e="T293" id="Seg_6715" s="T292">dĭ</ta>
            <ta e="T294" id="Seg_6716" s="T293">măna</ta>
            <ta e="T295" id="Seg_6717" s="T294">ajir-bi</ta>
            <ta e="T296" id="Seg_6718" s="T295">uda-nə</ta>
            <ta e="T297" id="Seg_6719" s="T296">i-bi</ta>
            <ta e="T298" id="Seg_6720" s="T297">a</ta>
            <ta e="T299" id="Seg_6721" s="T298">dĭgəttə</ta>
            <ta e="T1022" id="Seg_6722" s="T299">kal-la</ta>
            <ta e="T300" id="Seg_6723" s="T1022">dʼür-bi</ta>
            <ta e="T301" id="Seg_6724" s="T300">Igarka-nə</ta>
            <ta e="T302" id="Seg_6725" s="T301">i</ta>
            <ta e="T303" id="Seg_6726" s="T302">dĭn</ta>
            <ta e="T304" id="Seg_6727" s="T303">kü-lam-bi</ta>
            <ta e="T306" id="Seg_6728" s="T305">a</ta>
            <ta e="T307" id="Seg_6729" s="T306">dĭ</ta>
            <ta e="T308" id="Seg_6730" s="T307">ej</ta>
            <ta e="T309" id="Seg_6731" s="T308">kuvas</ta>
            <ta e="T310" id="Seg_6732" s="T309">i-bi</ta>
            <ta e="T311" id="Seg_6733" s="T310">aŋ-də</ta>
            <ta e="T313" id="Seg_6734" s="T311">bar</ta>
            <ta e="T315" id="Seg_6735" s="T314">dĭ</ta>
            <ta e="T316" id="Seg_6736" s="T315">ej</ta>
            <ta e="T317" id="Seg_6737" s="T316">kuvas</ta>
            <ta e="T318" id="Seg_6738" s="T317">aŋ-də</ta>
            <ta e="T319" id="Seg_6739" s="T318">păjdʼaŋ</ta>
            <ta e="T320" id="Seg_6740" s="T319">sʼima-t</ta>
            <ta e="T321" id="Seg_6741" s="T320">păjdʼaŋ</ta>
            <ta e="T323" id="Seg_6742" s="T322">šide</ta>
            <ta e="T324" id="Seg_6743" s="T323">ne</ta>
            <ta e="T325" id="Seg_6744" s="T324">amno-bi-ʔi</ta>
            <ta e="T326" id="Seg_6745" s="T325">kam-bi-ʔi</ta>
            <ta e="T327" id="Seg_6746" s="T326">saranka-ʔi</ta>
            <ta e="T329" id="Seg_6747" s="T328">tĭl-zittə</ta>
            <ta e="T330" id="Seg_6748" s="T329">onʼiʔ</ta>
            <ta e="T331" id="Seg_6749" s="T330">tĭl-bi</ta>
            <ta e="T332" id="Seg_6750" s="T331">iʔgö</ta>
            <ta e="T333" id="Seg_6751" s="T332">a</ta>
            <ta e="T334" id="Seg_6752" s="T333">onʼiʔ</ta>
            <ta e="T335" id="Seg_6753" s="T334">amga</ta>
            <ta e="T336" id="Seg_6754" s="T335">girgit</ta>
            <ta e="T337" id="Seg_6755" s="T336">iʔgö</ta>
            <ta e="T338" id="Seg_6756" s="T337">tĭl-bi</ta>
            <ta e="T339" id="Seg_6757" s="T338">a</ta>
            <ta e="T340" id="Seg_6758" s="T339">girgit</ta>
            <ta e="T341" id="Seg_6759" s="T340">amga</ta>
            <ta e="T342" id="Seg_6760" s="T341">tĭl-bi</ta>
            <ta e="T343" id="Seg_6761" s="T342">dĭ-m</ta>
            <ta e="T345" id="Seg_6762" s="T344">baltu-zi</ta>
            <ta e="T346" id="Seg_6763" s="T345">ulu-t</ta>
            <ta e="T347" id="Seg_6764" s="T346">jaʔ-pi</ta>
            <ta e="T348" id="Seg_6765" s="T347">dĭgəttə</ta>
            <ta e="T349" id="Seg_6766" s="T348">šo-bi</ta>
            <ta e="T350" id="Seg_6767" s="T349">maʔ-nə</ta>
            <ta e="T351" id="Seg_6768" s="T350">koʔbdo</ta>
            <ta e="T352" id="Seg_6769" s="T351">šo-bi</ta>
            <ta e="T353" id="Seg_6770" s="T352">gijen</ta>
            <ta e="T354" id="Seg_6771" s="T353">măn</ta>
            <ta e="T355" id="Seg_6772" s="T354">ia-m</ta>
            <ta e="T356" id="Seg_6773" s="T355">dĭ</ta>
            <ta e="T357" id="Seg_6774" s="T356">bar</ta>
            <ta e="T358" id="Seg_6775" s="T357">ɨrɨː</ta>
            <ta e="T359" id="Seg_6776" s="T358">ej</ta>
            <ta e="T360" id="Seg_6777" s="T359">tĭl-bi</ta>
            <ta e="T361" id="Seg_6778" s="T360">saranka-ʔi</ta>
            <ta e="T362" id="Seg_6779" s="T361">dĭn</ta>
            <ta e="T363" id="Seg_6780" s="T362">bar</ta>
            <ta e="T364" id="Seg_6781" s="T363">kunol-laʔbə</ta>
            <ta e="T365" id="Seg_6782" s="T364">a</ta>
            <ta e="T366" id="Seg_6783" s="T365">koʔbdo-t</ta>
            <ta e="T367" id="Seg_6784" s="T366">kăde=də</ta>
            <ta e="T368" id="Seg_6785" s="T367">kăde</ta>
            <ta e="T372" id="Seg_6786" s="T371">tĭmne-bi</ta>
            <ta e="T373" id="Seg_6787" s="T372">i</ta>
            <ta e="T374" id="Seg_6788" s="T373">šo-bi</ta>
            <ta e="T375" id="Seg_6789" s="T374">nʼi-t</ta>
            <ta e="T376" id="Seg_6790" s="T375">i-bi</ta>
            <ta e="T377" id="Seg_6791" s="T376">dĭ</ta>
            <ta e="T378" id="Seg_6792" s="T377">dĭ-m</ta>
            <ta e="T379" id="Seg_6793" s="T378">šaʔ-la</ta>
            <ta e="T380" id="Seg_6794" s="T379">i-bi</ta>
            <ta e="T382" id="Seg_6795" s="T381">kunol-zittə</ta>
            <ta e="T383" id="Seg_6796" s="T382">em-bi</ta>
            <ta e="T384" id="Seg_6797" s="T383">a</ta>
            <ta e="T385" id="Seg_6798" s="T384">bos-tə</ta>
            <ta e="T386" id="Seg_6799" s="T385">nʼuʔdə</ta>
            <ta e="T389" id="Seg_6800" s="T388">sʼa-bi</ta>
            <ta e="T390" id="Seg_6801" s="T389">maʔ-tə</ta>
            <ta e="T392" id="Seg_6802" s="T391">i-bi</ta>
            <ta e="T393" id="Seg_6803" s="T392">dĭgəttə</ta>
            <ta e="T395" id="Seg_6804" s="T394">dĭ</ta>
            <ta e="T396" id="Seg_6805" s="T395">ne</ta>
            <ta e="T397" id="Seg_6806" s="T396">šo-bi</ta>
            <ta e="T398" id="Seg_6807" s="T397">gijen</ta>
            <ta e="T399" id="Seg_6808" s="T398">šiʔ</ta>
            <ta e="T401" id="Seg_6809" s="T400">dĭ</ta>
            <ta e="T402" id="Seg_6810" s="T401">măn-də</ta>
            <ta e="T403" id="Seg_6811" s="T402">măn</ta>
            <ta e="T404" id="Seg_6812" s="T403">dön</ta>
            <ta e="T405" id="Seg_6813" s="T404">amno-laʔbə-m</ta>
            <ta e="T406" id="Seg_6814" s="T405">dĭ</ta>
            <ta e="T410" id="Seg_6815" s="T409">kămnə-bi</ta>
            <ta e="T411" id="Seg_6816" s="T410">sʼima-ndə</ta>
            <ta e="T413" id="Seg_6817" s="T412">ĭmbi=də</ta>
            <ta e="T414" id="Seg_6818" s="T413">ej</ta>
            <ta e="T415" id="Seg_6819" s="T414">măndə-lia-ʔi</ta>
            <ta e="T416" id="Seg_6820" s="T415">dĭgəttə</ta>
            <ta e="T417" id="Seg_6821" s="T416">sʼima-t</ta>
            <ta e="T418" id="Seg_6822" s="T417">ĭmbi=də</ta>
            <ta e="T419" id="Seg_6823" s="T418">ej</ta>
            <ta e="T420" id="Seg_6824" s="T419">mo-lia-ʔi</ta>
            <ta e="T421" id="Seg_6825" s="T420">dĭ</ta>
            <ta e="T422" id="Seg_6826" s="T421">suʔmə-luʔ-pi</ta>
            <ta e="T423" id="Seg_6827" s="T422">maʔ-tə</ta>
            <ta e="T424" id="Seg_6828" s="T423">kaga-t</ta>
            <ta e="T425" id="Seg_6829" s="T424">i-bi</ta>
            <ta e="T426" id="Seg_6830" s="T425">i</ta>
            <ta e="T1023" id="Seg_6831" s="T426">kal-la</ta>
            <ta e="T427" id="Seg_6832" s="T1023">dʼür-bi</ta>
            <ta e="T428" id="Seg_6833" s="T427">gijen</ta>
            <ta e="T429" id="Seg_6834" s="T428">il</ta>
            <ta e="T430" id="Seg_6835" s="T429">iʔgö</ta>
            <ta e="T432" id="Seg_6836" s="T431">măn</ta>
            <ta e="T433" id="Seg_6837" s="T432">tibi</ta>
            <ta e="T1024" id="Seg_6838" s="T433">kal-la</ta>
            <ta e="T434" id="Seg_6839" s="T1024">dʼür-bi</ta>
            <ta e="T435" id="Seg_6840" s="T434">măn</ta>
            <ta e="T436" id="Seg_6841" s="T435">ugandə</ta>
            <ta e="T437" id="Seg_6842" s="T436">tăŋ</ta>
            <ta e="T438" id="Seg_6843" s="T437">dʼor-bia-m</ta>
            <ta e="T439" id="Seg_6844" s="T438">sĭj-bə</ta>
            <ta e="T440" id="Seg_6845" s="T439">bar</ta>
            <ta e="T441" id="Seg_6846" s="T440">ĭzem-bi</ta>
            <ta e="T442" id="Seg_6847" s="T441">i</ta>
            <ta e="T443" id="Seg_6848" s="T442">ulu-m</ta>
            <ta e="T447" id="Seg_6849" s="T446">măn</ta>
            <ta e="T449" id="Seg_6850" s="T448">sʼestra-m</ta>
            <ta e="T450" id="Seg_6851" s="T449">nʼi</ta>
            <ta e="T451" id="Seg_6852" s="T450">nʼi-t</ta>
            <ta e="T452" id="Seg_6853" s="T451">tʼermən-də</ta>
            <ta e="T453" id="Seg_6854" s="T452">mĭm-bi</ta>
            <ta e="T454" id="Seg_6855" s="T453">ipek</ta>
            <ta e="T456" id="Seg_6856" s="T455">tʼermən-də</ta>
            <ta e="T457" id="Seg_6857" s="T456">mĭ-bi</ta>
            <ta e="T458" id="Seg_6858" s="T457">un</ta>
            <ta e="T459" id="Seg_6859" s="T458">nʼeʔ-sittə</ta>
            <ta e="T460" id="Seg_6860" s="T459">dĭgəttə</ta>
            <ta e="T461" id="Seg_6861" s="T460">maʔ-ndə</ta>
            <ta e="T462" id="Seg_6862" s="T461">šo-bi</ta>
            <ta e="T463" id="Seg_6863" s="T462">bos-tə</ta>
            <ta e="T464" id="Seg_6864" s="T463">ne-nə</ta>
            <ta e="T465" id="Seg_6865" s="T464">măn-lia</ta>
            <ta e="T466" id="Seg_6866" s="T465">štobɨ</ta>
            <ta e="T467" id="Seg_6867" s="T466">kondʼo</ta>
            <ta e="T469" id="Seg_6868" s="T468">kabar-lə-j</ta>
            <ta e="T470" id="Seg_6869" s="T469">a</ta>
            <ta e="T471" id="Seg_6870" s="T470">măn</ta>
            <ta e="T472" id="Seg_6871" s="T471">amno-lia-m</ta>
            <ta e="T473" id="Seg_6872" s="T472">da</ta>
            <ta e="T474" id="Seg_6873" s="T473">tenö-lia-m</ta>
            <ta e="T476" id="Seg_6874" s="T475">što</ta>
            <ta e="T477" id="Seg_6875" s="T476">măn</ta>
            <ta e="T479" id="Seg_6876" s="T478">dĭ-zeŋ-zi</ta>
            <ta e="T480" id="Seg_6877" s="T479">amor-zittə</ta>
            <ta e="T483" id="Seg_6878" s="T482">to</ta>
            <ta e="T484" id="Seg_6879" s="T483">mĭm-bi-ʔi</ta>
            <ta e="T485" id="Seg_6880" s="T484">boš-tə</ta>
            <ta e="T486" id="Seg_6881" s="T485">măn</ta>
            <ta e="T487" id="Seg_6882" s="T486">iʔgö</ta>
            <ta e="T488" id="Seg_6883" s="T487">am-nia-m</ta>
            <ta e="T490" id="Seg_6884" s="T489">onʼiʔ</ta>
            <ta e="T491" id="Seg_6885" s="T490">kuza</ta>
            <ta e="T492" id="Seg_6886" s="T491">monoʔko-sʼtə</ta>
            <ta e="T494" id="Seg_6887" s="T493">kam-bi</ta>
            <ta e="T495" id="Seg_6888" s="T494">kuvas</ta>
            <ta e="T496" id="Seg_6889" s="T495">koʔbdo</ta>
            <ta e="T497" id="Seg_6890" s="T496">ugandə</ta>
            <ta e="T498" id="Seg_6891" s="T497">oldʼa-t</ta>
            <ta e="T499" id="Seg_6892" s="T498">iʔgö</ta>
            <ta e="T500" id="Seg_6893" s="T499">kandə-ga</ta>
            <ta e="T501" id="Seg_6894" s="T500">dĭgəttə</ta>
            <ta e="T502" id="Seg_6895" s="T501">kuza</ta>
            <ta e="T503" id="Seg_6896" s="T502">dĭʔ-nə</ta>
            <ta e="T504" id="Seg_6897" s="T503">măn-də</ta>
            <ta e="T505" id="Seg_6898" s="T504">gibər</ta>
            <ta e="T506" id="Seg_6899" s="T505">kandə-ga-l</ta>
            <ta e="T507" id="Seg_6900" s="T506">monoʔko-sʼtə</ta>
            <ta e="T508" id="Seg_6901" s="T507">i-ʔ</ta>
            <ta e="T509" id="Seg_6902" s="T508">măna</ta>
            <ta e="T510" id="Seg_6903" s="T509">a</ta>
            <ta e="T511" id="Seg_6904" s="T510">tăn</ta>
            <ta e="T512" id="Seg_6905" s="T511">ĭmbi</ta>
            <ta e="T513" id="Seg_6906" s="T512">a-lia-l</ta>
            <ta e="T514" id="Seg_6907" s="T513">amor-zittə</ta>
            <ta e="T515" id="Seg_6908" s="T514">da</ta>
            <ta e="T516" id="Seg_6909" s="T515">tüʔ-sittə</ta>
            <ta e="T517" id="Seg_6910" s="T516">dĭgəttə</ta>
            <ta e="T518" id="Seg_6911" s="T517">kam-bi-ʔi</ta>
            <ta e="T519" id="Seg_6912" s="T518">bazoʔ</ta>
            <ta e="T520" id="Seg_6913" s="T519">bazoʔ</ta>
            <ta e="T521" id="Seg_6914" s="T520">kuza</ta>
            <ta e="T522" id="Seg_6915" s="T521">măn-də</ta>
            <ta e="T523" id="Seg_6916" s="T522">gibər</ta>
            <ta e="T524" id="Seg_6917" s="T523">kandə-ga-laʔ</ta>
            <ta e="T525" id="Seg_6918" s="T524">monoʔko-sʼtə</ta>
            <ta e="T526" id="Seg_6919" s="T525">măna</ta>
            <ta e="T527" id="Seg_6920" s="T526">i-t</ta>
            <ta e="T528" id="Seg_6921" s="T527">a</ta>
            <ta e="T529" id="Seg_6922" s="T528">tăn</ta>
            <ta e="T530" id="Seg_6923" s="T529">ĭmbi</ta>
            <ta e="T531" id="Seg_6924" s="T530">a-lə-l</ta>
            <ta e="T532" id="Seg_6925" s="T531">bĭʔ-sittə</ta>
            <ta e="T533" id="Seg_6926" s="T532">da</ta>
            <ta e="T534" id="Seg_6927" s="T533">kĭnzə-sʼtə</ta>
            <ta e="T535" id="Seg_6928" s="T534">dĭgəttə</ta>
            <ta e="T536" id="Seg_6929" s="T535">šo-bi-ʔi</ta>
            <ta e="T537" id="Seg_6930" s="T536">i</ta>
            <ta e="T538" id="Seg_6931" s="T537">davaj</ta>
            <ta e="T539" id="Seg_6932" s="T538">monoʔko-sʼtə</ta>
            <ta e="T540" id="Seg_6933" s="T539">dĭ</ta>
            <ta e="T541" id="Seg_6934" s="T540">măn-də</ta>
            <ta e="T542" id="Seg_6935" s="T541">măn</ta>
            <ta e="T543" id="Seg_6936" s="T542">iʔgö</ta>
            <ta e="T544" id="Seg_6937" s="T543">bar</ta>
            <ta e="T545" id="Seg_6938" s="T544">a-lə-m</ta>
            <ta e="T546" id="Seg_6939" s="T545">ipek</ta>
            <ta e="T547" id="Seg_6940" s="T546">pür-le-m</ta>
            <ta e="T548" id="Seg_6941" s="T547">uja</ta>
            <ta e="T549" id="Seg_6942" s="T548">bar</ta>
            <ta e="T550" id="Seg_6943" s="T549">am-zittə</ta>
            <ta e="T551" id="Seg_6944" s="T550">nada</ta>
            <ta e="T552" id="Seg_6945" s="T551">dĭ</ta>
            <ta e="T553" id="Seg_6946" s="T552">măn-də</ta>
            <ta e="T554" id="Seg_6947" s="T553">no</ta>
            <ta e="T555" id="Seg_6948" s="T554">bar</ta>
            <ta e="T556" id="Seg_6949" s="T555">măn</ta>
            <ta e="T557" id="Seg_6950" s="T556">am-na-m</ta>
            <ta e="T558" id="Seg_6951" s="T557">i</ta>
            <ta e="T561" id="Seg_6952" s="T560">iʔgö</ta>
            <ta e="T562" id="Seg_6953" s="T561">bʼeʔ</ta>
            <ta e="T563" id="Seg_6954" s="T562">teʔtə</ta>
            <ta e="T565" id="Seg_6955" s="T564">a-la-m</ta>
            <ta e="T566" id="Seg_6956" s="T565">no</ta>
            <ta e="T567" id="Seg_6957" s="T566">dĭgəttə</ta>
            <ta e="T569" id="Seg_6958" s="T568">stol-də</ta>
            <ta e="T570" id="Seg_6959" s="T569">oʔbdə-bi</ta>
            <ta e="T571" id="Seg_6960" s="T570">dĭgəttə</ta>
            <ta e="T572" id="Seg_6961" s="T571">šo-bi</ta>
            <ta e="T573" id="Seg_6962" s="T572">dĭ</ta>
            <ta e="T574" id="Seg_6963" s="T573">kuza</ta>
            <ta e="T575" id="Seg_6964" s="T574">kajit</ta>
            <ta e="T576" id="Seg_6965" s="T575">amor-zittə</ta>
            <ta e="T577" id="Seg_6966" s="T576">da</ta>
            <ta e="T578" id="Seg_6967" s="T577">tüʔ-sittə</ta>
            <ta e="T579" id="Seg_6968" s="T578">bar</ta>
            <ta e="T580" id="Seg_6969" s="T579">am-naʔbə</ta>
            <ta e="T581" id="Seg_6970" s="T580">da</ta>
            <ta e="T582" id="Seg_6971" s="T581">tüʔ-leʔbə</ta>
            <ta e="T583" id="Seg_6972" s="T582">am-naʔbə</ta>
            <ta e="T584" id="Seg_6973" s="T583">da</ta>
            <ta e="T585" id="Seg_6974" s="T584">tüʔ-leʔbə</ta>
            <ta e="T586" id="Seg_6975" s="T585">bar</ta>
            <ta e="T587" id="Seg_6976" s="T586">ĭmbi</ta>
            <ta e="T588" id="Seg_6977" s="T587">am-nuʔ-pi</ta>
            <ta e="T589" id="Seg_6978" s="T588">i</ta>
            <ta e="T590" id="Seg_6979" s="T589">uja</ta>
            <ta e="T591" id="Seg_6980" s="T590">i</ta>
            <ta e="T593" id="Seg_6981" s="T592">ĭmbi</ta>
            <ta e="T594" id="Seg_6982" s="T593">i-bi</ta>
            <ta e="T595" id="Seg_6983" s="T594">bar</ta>
            <ta e="T596" id="Seg_6984" s="T595">bos-tə</ta>
            <ta e="T597" id="Seg_6985" s="T596">măn-də</ta>
            <ta e="T598" id="Seg_6986" s="T597">deʔ-keʔ</ta>
            <ta e="T599" id="Seg_6987" s="T598">išo</ta>
            <ta e="T600" id="Seg_6988" s="T599">amga</ta>
            <ta e="T601" id="Seg_6989" s="T600">deʔ-keʔ</ta>
            <ta e="T602" id="Seg_6990" s="T601">išo</ta>
            <ta e="T603" id="Seg_6991" s="T602">amga</ta>
            <ta e="T604" id="Seg_6992" s="T603">dĭgəttə</ta>
            <ta e="T605" id="Seg_6993" s="T604">už</ta>
            <ta e="T606" id="Seg_6994" s="T605">am-zittə</ta>
            <ta e="T612" id="Seg_6995" s="T611">dĭgəttə</ta>
            <ta e="T613" id="Seg_6996" s="T612">bü</ta>
            <ta e="T615" id="Seg_6997" s="T614">bĭʔ-sittə</ta>
            <ta e="T616" id="Seg_6998" s="T615">bazoʔ</ta>
            <ta e="T617" id="Seg_6999" s="T616">bazoʔ</ta>
            <ta e="T618" id="Seg_7000" s="T617">šo-bi</ta>
            <ta e="T619" id="Seg_7001" s="T618">dĭ</ta>
            <ta e="T621" id="Seg_7002" s="T620">dĭ</ta>
            <ta e="T622" id="Seg_7003" s="T621">kuza</ta>
            <ta e="T623" id="Seg_7004" s="T622">dĭgəttə</ta>
            <ta e="T624" id="Seg_7005" s="T623">dĭ</ta>
            <ta e="T626" id="Seg_7006" s="T625">bĭt-leʔbə</ta>
            <ta e="T627" id="Seg_7007" s="T626">bĭt-leʔbə</ta>
            <ta e="T628" id="Seg_7008" s="T627">da</ta>
            <ta e="T629" id="Seg_7009" s="T628">kĭnz-leʔbə</ta>
            <ta e="T630" id="Seg_7010" s="T629">bĭt-leʔbə</ta>
            <ta e="T631" id="Seg_7011" s="T630">da</ta>
            <ta e="T632" id="Seg_7012" s="T631">kĭnz-leʔbə</ta>
            <ta e="T633" id="Seg_7013" s="T632">bar</ta>
            <ta e="T634" id="Seg_7014" s="T633">bĭt-luʔ-pi</ta>
            <ta e="T635" id="Seg_7015" s="T634">da</ta>
            <ta e="T636" id="Seg_7016" s="T635">kirgar-laʔbə</ta>
            <ta e="T637" id="Seg_7017" s="T636">išo</ta>
            <ta e="T639" id="Seg_7018" s="T638">de-ʔ</ta>
            <ta e="T640" id="Seg_7019" s="T639">măna</ta>
            <ta e="T641" id="Seg_7020" s="T640">amga</ta>
            <ta e="T642" id="Seg_7021" s="T641">a</ta>
            <ta e="T643" id="Seg_7022" s="T642">ulitsa-nə</ta>
            <ta e="T644" id="Seg_7023" s="T643">bar</ta>
            <ta e="T645" id="Seg_7024" s="T644">ugandə</ta>
            <ta e="T646" id="Seg_7025" s="T645">bü</ta>
            <ta e="T647" id="Seg_7026" s="T646">iʔgö</ta>
            <ta e="T649" id="Seg_7027" s="T648">dĭgəttə</ta>
            <ta e="T650" id="Seg_7028" s="T649">ej</ta>
            <ta e="T651" id="Seg_7029" s="T650">am-bi</ta>
            <ta e="T652" id="Seg_7030" s="T651">da</ta>
            <ta e="T653" id="Seg_7031" s="T652">bü</ta>
            <ta e="T654" id="Seg_7032" s="T653">ej</ta>
            <ta e="T655" id="Seg_7033" s="T654">bĭʔ-pi</ta>
            <ta e="T656" id="Seg_7034" s="T655">dĭʔ-nə</ta>
            <ta e="T657" id="Seg_7035" s="T656">bɨ</ta>
            <ta e="T658" id="Seg_7036" s="T657">ulu-bə</ta>
            <ta e="T659" id="Seg_7037" s="T658">saj-jaʔ-pi-ʔi</ta>
            <ta e="T660" id="Seg_7038" s="T659">bɨ</ta>
            <ta e="T661" id="Seg_7039" s="T660">i</ta>
            <ta e="T662" id="Seg_7040" s="T661">edə-bi-ʔi</ta>
            <ta e="T664" id="Seg_7041" s="T662">ato</ta>
            <ta e="T665" id="Seg_7042" s="T664">dĭgəttə</ta>
            <ta e="T666" id="Seg_7043" s="T665">dĭ</ta>
            <ta e="T667" id="Seg_7044" s="T666">koʔbdo-m</ta>
            <ta e="T668" id="Seg_7045" s="T667">i-bi</ta>
            <ta e="T669" id="Seg_7046" s="T668">i</ta>
            <ta e="T670" id="Seg_7047" s="T669">maʔ-ndə</ta>
            <ta e="T671" id="Seg_7048" s="T670">kam-bi</ta>
            <ta e="T673" id="Seg_7049" s="T672">kaba</ta>
            <ta e="T674" id="Seg_7050" s="T673">ĭmbi</ta>
            <ta e="T675" id="Seg_7051" s="T674">nüj-leʔbə-l</ta>
            <ta e="T677" id="Seg_7052" s="T676">a</ta>
            <ta e="T678" id="Seg_7053" s="T677">ĭmbi</ta>
            <ta e="T679" id="Seg_7054" s="T678">ej</ta>
            <ta e="T680" id="Seg_7055" s="T679">jakše</ta>
            <ta e="T681" id="Seg_7056" s="T680">nüj-leʔbə-l</ta>
            <ta e="T682" id="Seg_7057" s="T681">da</ta>
            <ta e="T684" id="Seg_7058" s="T683">jakše</ta>
            <ta e="T686" id="Seg_7059" s="T685">nüjnə-sʼtə</ta>
            <ta e="T687" id="Seg_7060" s="T686">ku-io-l</ta>
            <ta e="T688" id="Seg_7061" s="T687">barlaʔina</ta>
            <ta e="T690" id="Seg_7062" s="T689">onʼiʔ</ta>
            <ta e="T691" id="Seg_7063" s="T690">kuza</ta>
            <ta e="T693" id="Seg_7064" s="T692">i-bi</ta>
            <ta e="T694" id="Seg_7065" s="T693">tʼermən-də</ta>
            <ta e="T695" id="Seg_7066" s="T694">a</ta>
            <ta e="T696" id="Seg_7067" s="T695">a</ta>
            <ta e="T697" id="Seg_7068" s="T696">onʼiʔ</ta>
            <ta e="T698" id="Seg_7069" s="T697">kuza</ta>
            <ta e="T699" id="Seg_7070" s="T698">ugandə</ta>
            <ta e="T700" id="Seg_7071" s="T699">aktʼa</ta>
            <ta e="T701" id="Seg_7072" s="T700">iʔgö</ta>
            <ta e="T703" id="Seg_7073" s="T702">i-bi</ta>
            <ta e="T704" id="Seg_7074" s="T703">i</ta>
            <ta e="T705" id="Seg_7075" s="T704">oldʼa</ta>
            <ta e="T706" id="Seg_7076" s="T705">i</ta>
            <ta e="T707" id="Seg_7077" s="T706">ipek</ta>
            <ta e="T708" id="Seg_7078" s="T707">dĭ</ta>
            <ta e="T709" id="Seg_7079" s="T708">dĭ-n</ta>
            <ta e="T710" id="Seg_7080" s="T709">kuza-nə</ta>
            <ta e="T711" id="Seg_7081" s="T710">i-bi</ta>
            <ta e="T712" id="Seg_7082" s="T711">tʼermən-də</ta>
            <ta e="T713" id="Seg_7083" s="T712">i</ta>
            <ta e="T715" id="Seg_7084" s="T714">kon-nam-bi</ta>
            <ta e="T716" id="Seg_7085" s="T715">dĭ</ta>
            <ta e="T717" id="Seg_7086" s="T716">bar</ta>
            <ta e="T718" id="Seg_7087" s="T717">dʼor-laʔbə</ta>
            <ta e="T719" id="Seg_7088" s="T718">a</ta>
            <ta e="T720" id="Seg_7089" s="T719">pʼetux</ta>
            <ta e="T721" id="Seg_7090" s="T720">mă-lia</ta>
            <ta e="T722" id="Seg_7091" s="T721">ĭmbi</ta>
            <ta e="T723" id="Seg_7092" s="T722">dʼor-laʔbə-l</ta>
            <ta e="T724" id="Seg_7093" s="T723">kan-žə-bəj</ta>
            <ta e="T725" id="Seg_7094" s="T724">tüjö</ta>
            <ta e="T726" id="Seg_7095" s="T725">i-bəj</ta>
            <ta e="T727" id="Seg_7096" s="T726">tʼermən-də</ta>
            <ta e="T728" id="Seg_7097" s="T727">dĭ</ta>
            <ta e="T729" id="Seg_7098" s="T728">lattɨ</ta>
            <ta e="T730" id="Seg_7099" s="T729">kam-bi-ʔi</ta>
            <ta e="T731" id="Seg_7100" s="T730">pʼetux</ta>
            <ta e="T732" id="Seg_7101" s="T731">kirgar-laʔbə</ta>
            <ta e="T733" id="Seg_7102" s="T732">de-ʔ</ta>
            <ta e="T734" id="Seg_7103" s="T733">tʼermən</ta>
            <ta e="T735" id="Seg_7104" s="T734">de-ʔ</ta>
            <ta e="T736" id="Seg_7105" s="T735">tʼermən</ta>
            <ta e="T738" id="Seg_7106" s="T737">dĭ</ta>
            <ta e="T739" id="Seg_7107" s="T738">kuza</ta>
            <ta e="T740" id="Seg_7108" s="T739">măn-də</ta>
            <ta e="T741" id="Seg_7109" s="T740">barəʔ-tə</ta>
            <ta e="T742" id="Seg_7110" s="T741">dĭ-m</ta>
            <ta e="T743" id="Seg_7111" s="T742">ine-ziʔ</ta>
            <ta e="T744" id="Seg_7112" s="T743">ine-ʔi-nə</ta>
            <ta e="T745" id="Seg_7113" s="T744">barəʔ-luʔ-pi</ta>
            <ta e="T746" id="Seg_7114" s="T745">dĭ</ta>
            <ta e="T747" id="Seg_7115" s="T746">dĭʔə</ta>
            <ta e="T748" id="Seg_7116" s="T747">šo-bi</ta>
            <ta e="T749" id="Seg_7117" s="T748">bazoʔ</ta>
            <ta e="T750" id="Seg_7118" s="T749">kirgar-laʔbə</ta>
            <ta e="T752" id="Seg_7119" s="T751">deʔ-tə</ta>
            <ta e="T753" id="Seg_7120" s="T752">tʼermən-də</ta>
            <ta e="T754" id="Seg_7121" s="T753">deʔ-tə</ta>
            <ta e="T755" id="Seg_7122" s="T754">tʼermən-də</ta>
            <ta e="T756" id="Seg_7123" s="T755">dĭgəttə</ta>
            <ta e="T757" id="Seg_7124" s="T756">barəʔ-tə</ta>
            <ta e="T759" id="Seg_7125" s="T758">dĭ-m</ta>
            <ta e="T760" id="Seg_7126" s="T759">tüžöj-də</ta>
            <ta e="T761" id="Seg_7127" s="T760">barəʔ-luʔ-pi</ta>
            <ta e="T762" id="Seg_7128" s="T761">tüžöj-də</ta>
            <ta e="T763" id="Seg_7129" s="T762">dĭ</ta>
            <ta e="T764" id="Seg_7130" s="T763">tüžöj-gən</ta>
            <ta e="T766" id="Seg_7131" s="T765">bazoʔ</ta>
            <ta e="T767" id="Seg_7132" s="T766">šo-bi</ta>
            <ta e="T768" id="Seg_7133" s="T767">dĭgəttə</ta>
            <ta e="T769" id="Seg_7134" s="T768">bazoʔ</ta>
            <ta e="T770" id="Seg_7135" s="T769">măn-də</ta>
            <ta e="T773" id="Seg_7136" s="T772">deʔ-tə</ta>
            <ta e="T774" id="Seg_7137" s="T773">tʼermən-də</ta>
            <ta e="T775" id="Seg_7138" s="T774">deʔ-tə</ta>
            <ta e="T777" id="Seg_7139" s="T775">tʼermən-də</ta>
            <ta e="T778" id="Seg_7140" s="T777">barəʔ-tə</ta>
            <ta e="T779" id="Seg_7141" s="T778">ular-də</ta>
            <ta e="T780" id="Seg_7142" s="T779">barəʔ-luʔ-pi</ta>
            <ta e="T781" id="Seg_7143" s="T780">ular-də</ta>
            <ta e="T782" id="Seg_7144" s="T781">dĭgəttə</ta>
            <ta e="T783" id="Seg_7145" s="T782">bazoʔ</ta>
            <ta e="T784" id="Seg_7146" s="T783">šo-bi</ta>
            <ta e="T785" id="Seg_7147" s="T784">bazoʔ</ta>
            <ta e="T786" id="Seg_7148" s="T785">kirgar-laʔbə</ta>
            <ta e="T787" id="Seg_7149" s="T786">deʔ-tə</ta>
            <ta e="T788" id="Seg_7150" s="T787">tʼermən-də</ta>
            <ta e="T789" id="Seg_7151" s="T788">deʔ-tə</ta>
            <ta e="T790" id="Seg_7152" s="T789">tʼermən-də</ta>
            <ta e="T791" id="Seg_7153" s="T790">dĭgəttə</ta>
            <ta e="T792" id="Seg_7154" s="T791">măn-də</ta>
            <ta e="T793" id="Seg_7155" s="T792">barəʔ-tə</ta>
            <ta e="T794" id="Seg_7156" s="T793">dĭ-m</ta>
            <ta e="T795" id="Seg_7157" s="T794">bü-nə</ta>
            <ta e="T796" id="Seg_7158" s="T795">dĭ-m</ta>
            <ta e="T797" id="Seg_7159" s="T796">barəʔ-luʔ-pi</ta>
            <ta e="T798" id="Seg_7160" s="T797">a</ta>
            <ta e="T799" id="Seg_7161" s="T798">dĭ</ta>
            <ta e="T800" id="Seg_7162" s="T799">măn-də</ta>
            <ta e="T802" id="Seg_7163" s="T800">köten-bə</ta>
            <ta e="T804" id="Seg_7164" s="T803">barəʔ-luʔ-pi</ta>
            <ta e="T805" id="Seg_7165" s="T804">dĭ</ta>
            <ta e="T806" id="Seg_7166" s="T805">dĭ-m</ta>
            <ta e="T807" id="Seg_7167" s="T806">bü-nə</ta>
            <ta e="T808" id="Seg_7168" s="T807">a</ta>
            <ta e="T809" id="Seg_7169" s="T808">dĭ</ta>
            <ta e="T810" id="Seg_7170" s="T809">kirgar-laʔbə</ta>
            <ta e="T811" id="Seg_7171" s="T810">köten-bə</ta>
            <ta e="T812" id="Seg_7172" s="T811">bĭd-e-ʔ</ta>
            <ta e="T813" id="Seg_7173" s="T812">bü</ta>
            <ta e="T814" id="Seg_7174" s="T813">köten-bə</ta>
            <ta e="T815" id="Seg_7175" s="T814">bĭd-e-ʔ</ta>
            <ta e="T816" id="Seg_7176" s="T815">bü</ta>
            <ta e="T817" id="Seg_7177" s="T816">köten-bə</ta>
            <ta e="T818" id="Seg_7178" s="T817">bar</ta>
            <ta e="T819" id="Seg_7179" s="T818">bü</ta>
            <ta e="T820" id="Seg_7180" s="T819">bĭt-luʔ-pi</ta>
            <ta e="T821" id="Seg_7181" s="T820">bazoʔ</ta>
            <ta e="T822" id="Seg_7182" s="T821">šo-bi</ta>
            <ta e="T823" id="Seg_7183" s="T822">deʔ-tə</ta>
            <ta e="T824" id="Seg_7184" s="T823">tʼermən-də</ta>
            <ta e="T825" id="Seg_7185" s="T824">deʔ-tə</ta>
            <ta e="T826" id="Seg_7186" s="T825">tʼermən-də</ta>
            <ta e="T827" id="Seg_7187" s="T826">dĭgəttə</ta>
            <ta e="T828" id="Seg_7188" s="T827">dĭ</ta>
            <ta e="T829" id="Seg_7189" s="T828">kuza</ta>
            <ta e="T830" id="Seg_7190" s="T829">măn-də</ta>
            <ta e="T831" id="Seg_7191" s="T830">šü</ta>
            <ta e="T833" id="Seg_7192" s="T832">eŋ-geʔ</ta>
            <ta e="T834" id="Seg_7193" s="T833">urgo</ta>
            <ta e="T835" id="Seg_7194" s="T834">i</ta>
            <ta e="T836" id="Seg_7195" s="T835">barəʔ-tə</ta>
            <ta e="T837" id="Seg_7196" s="T836">dĭ-m</ta>
            <ta e="T838" id="Seg_7197" s="T837">šü-nə</ta>
            <ta e="T839" id="Seg_7198" s="T838">dĭgəttə</ta>
            <ta e="T840" id="Seg_7199" s="T839">em-bi-ʔi</ta>
            <ta e="T841" id="Seg_7200" s="T840">šü</ta>
            <ta e="T842" id="Seg_7201" s="T841">barəʔ-luʔ-pi</ta>
            <ta e="T843" id="Seg_7202" s="T842">dĭ</ta>
            <ta e="T844" id="Seg_7203" s="T843">pʼetug-əm</ta>
            <ta e="T845" id="Seg_7204" s="T844">šü-nə</ta>
            <ta e="T846" id="Seg_7205" s="T845">dibər</ta>
            <ta e="T847" id="Seg_7206" s="T846">măn-də</ta>
            <ta e="T848" id="Seg_7207" s="T847">kămna-ʔ</ta>
            <ta e="T849" id="Seg_7208" s="T848">köten-bə</ta>
            <ta e="T850" id="Seg_7209" s="T849">bü</ta>
            <ta e="T851" id="Seg_7210" s="T850">kămna-ʔ</ta>
            <ta e="T852" id="Seg_7211" s="T851">köten-bə</ta>
            <ta e="T853" id="Seg_7212" s="T852">bü</ta>
            <ta e="T854" id="Seg_7213" s="T853">kak</ta>
            <ta e="T855" id="Seg_7214" s="T854">bü</ta>
            <ta e="T856" id="Seg_7215" s="T855">kam-bi</ta>
            <ta e="T857" id="Seg_7216" s="T856">bar</ta>
            <ta e="T858" id="Seg_7217" s="T857">ĭmbi</ta>
            <ta e="T860" id="Seg_7218" s="T858">bar</ta>
            <ta e="T862" id="Seg_7219" s="T861">dĭgəttə</ta>
            <ta e="T863" id="Seg_7220" s="T862">bü</ta>
            <ta e="T864" id="Seg_7221" s="T863">ugandə</ta>
            <ta e="T865" id="Seg_7222" s="T864">iʔgö</ta>
            <ta e="T867" id="Seg_7223" s="T866">mʼam-bi</ta>
            <ta e="T868" id="Seg_7224" s="T867">dĭ</ta>
            <ta e="T869" id="Seg_7225" s="T868">kuza</ta>
            <ta e="T870" id="Seg_7226" s="T869">măn-də</ta>
            <ta e="T871" id="Seg_7227" s="T870">i-ʔ</ta>
            <ta e="T872" id="Seg_7228" s="T871">tʼermən-də</ta>
            <ta e="T873" id="Seg_7229" s="T872">i</ta>
            <ta e="T874" id="Seg_7230" s="T873">kan-a-ʔ</ta>
            <ta e="T875" id="Seg_7231" s="T874">döʔə</ta>
            <ta e="T876" id="Seg_7232" s="T875">döʔə</ta>
            <ta e="T877" id="Seg_7233" s="T876">döʔnə</ta>
            <ta e="T881" id="Seg_7234" s="T880">xisə-ʔi</ta>
            <ta e="T882" id="Seg_7235" s="T881">onʼiʔ</ta>
            <ta e="T884" id="Seg_7236" s="T883">nu-bi-ʔi</ta>
            <ta e="T885" id="Seg_7237" s="T884">măn</ta>
            <ta e="T886" id="Seg_7238" s="T885">dĭ-zeŋ</ta>
            <ta e="T889" id="Seg_7239" s="T888">öʔlu-bie-m</ta>
            <ta e="T890" id="Seg_7240" s="T889">dĭgəttə</ta>
            <ta e="T891" id="Seg_7241" s="T890">dĭ-zeŋ</ta>
            <ta e="T892" id="Seg_7242" s="T891">nuʔmə-luʔ-pi-ʔi</ta>
            <ta e="T893" id="Seg_7243" s="T892">a</ta>
            <ta e="T894" id="Seg_7244" s="T893">dĭ-zeŋ</ta>
            <ta e="T895" id="Seg_7245" s="T894">ej</ta>
            <ta e="T896" id="Seg_7246" s="T895">nuʔmə-lie-ʔi</ta>
            <ta e="T897" id="Seg_7247" s="T896">ɨrɨ</ta>
            <ta e="T898" id="Seg_7248" s="T897">mo-lam-bi-ʔi</ta>
            <ta e="T900" id="Seg_7249" s="T899">taldʼen</ta>
            <ta e="T901" id="Seg_7250" s="T900">măn</ta>
            <ta e="T902" id="Seg_7251" s="T901">mei-m</ta>
            <ta e="T903" id="Seg_7252" s="T902">ej</ta>
            <ta e="T1025" id="Seg_7253" s="T903">kal-la</ta>
            <ta e="T904" id="Seg_7254" s="T1025">dʼür-bi</ta>
            <ta e="T905" id="Seg_7255" s="T904">maːʔ-ndə</ta>
            <ta e="T906" id="Seg_7256" s="T905">măn</ta>
            <ta e="T907" id="Seg_7257" s="T906">mĭm-bie-m</ta>
            <ta e="T908" id="Seg_7258" s="T907">i</ta>
            <ta e="T909" id="Seg_7259" s="T908">dĭ-zi</ta>
            <ta e="T910" id="Seg_7260" s="T909">ej</ta>
            <ta e="T911" id="Seg_7261" s="T910">pănar-bia-m</ta>
            <ta e="T912" id="Seg_7262" s="T911">uda-m</ta>
            <ta e="T913" id="Seg_7263" s="T912">ej</ta>
            <ta e="T914" id="Seg_7264" s="T913">mĭ-bie-m</ta>
            <ta e="T915" id="Seg_7265" s="T914">dĭgəttə</ta>
            <ta e="T916" id="Seg_7266" s="T915">dĭ</ta>
            <ta e="T917" id="Seg_7267" s="T916">bazoʔ</ta>
            <ta e="T918" id="Seg_7268" s="T917">par-luʔ-pi</ta>
            <ta e="T919" id="Seg_7269" s="T918">ugandə</ta>
            <ta e="T920" id="Seg_7270" s="T919">beržə</ta>
            <ta e="T921" id="Seg_7271" s="T920">sĭre</ta>
            <ta e="T922" id="Seg_7272" s="T921">bar</ta>
            <ta e="T923" id="Seg_7273" s="T922">kun-d-laʔbə</ta>
            <ta e="T924" id="Seg_7274" s="T923">mašina-ʔi</ta>
            <ta e="T925" id="Seg_7275" s="T924">ej</ta>
            <ta e="T926" id="Seg_7276" s="T925">kam-bi-ʔi</ta>
            <ta e="T927" id="Seg_7277" s="T926">dĭ</ta>
            <ta e="T928" id="Seg_7278" s="T927">par-luʔ-pi</ta>
            <ta e="T929" id="Seg_7279" s="T928">dön</ta>
            <ta e="T930" id="Seg_7280" s="T929">šaː-bi</ta>
            <ta e="T931" id="Seg_7281" s="T930">a</ta>
            <ta e="T932" id="Seg_7282" s="T931">teinen</ta>
            <ta e="T933" id="Seg_7283" s="T932">măn</ta>
            <ta e="T934" id="Seg_7284" s="T933">uʔ-pia-m</ta>
            <ta e="T935" id="Seg_7285" s="T934">mĭnzer-bie-m</ta>
            <ta e="T936" id="Seg_7286" s="T935">bar</ta>
            <ta e="T937" id="Seg_7287" s="T936">ipek</ta>
            <ta e="T938" id="Seg_7288" s="T937">pür-bie-m</ta>
            <ta e="T939" id="Seg_7289" s="T938">bos-kəndə</ta>
            <ta e="T940" id="Seg_7290" s="T939">aʔtʼi-nə</ta>
            <ta e="T941" id="Seg_7291" s="T940">amor-bi</ta>
            <ta e="T942" id="Seg_7292" s="T941">dĭgəttə</ta>
            <ta e="T1026" id="Seg_7293" s="T942">kal-la</ta>
            <ta e="T943" id="Seg_7294" s="T1026">dʼür-bi</ta>
            <ta e="T944" id="Seg_7295" s="T943">mašina-zi</ta>
            <ta e="T945" id="Seg_7296" s="T944">predsedatelʼ-zi</ta>
            <ta e="T947" id="Seg_7297" s="T946">dĭgəttə</ta>
            <ta e="T948" id="Seg_7298" s="T947">kal-la-m</ta>
            <ta e="T949" id="Seg_7299" s="T948">maʔ-nʼi</ta>
            <ta e="T950" id="Seg_7300" s="T949">dĭgəttə</ta>
            <ta e="T951" id="Seg_7301" s="T950">šo-bia-m</ta>
            <ta e="T952" id="Seg_7302" s="T951">kal-la-m</ta>
            <ta e="T953" id="Seg_7303" s="T952">maʔ-nʼi</ta>
            <ta e="T954" id="Seg_7304" s="T953">ato</ta>
            <ta e="T955" id="Seg_7305" s="T954">măn</ta>
            <ta e="T956" id="Seg_7306" s="T955">dĭn</ta>
            <ta e="T957" id="Seg_7307" s="T956">il</ta>
            <ta e="T958" id="Seg_7308" s="T957">amno-laʔbə</ta>
            <ta e="T959" id="Seg_7309" s="T958">măna</ta>
            <ta e="T960" id="Seg_7310" s="T959">edəʔ-leʔbə-ʔjə</ta>
            <ta e="T961" id="Seg_7311" s="T960">dĭgəttə</ta>
            <ta e="T962" id="Seg_7312" s="T961">šo-bia-m</ta>
            <ta e="T963" id="Seg_7313" s="T962">maːʔ-nʼi</ta>
            <ta e="T964" id="Seg_7314" s="T963">dʼaparat</ta>
            <ta e="T965" id="Seg_7315" s="T964">băzə-bia-m</ta>
            <ta e="T966" id="Seg_7316" s="T965">büzö</ta>
            <ta e="T967" id="Seg_7317" s="T966">bĭt-əl-bie-m</ta>
            <ta e="T968" id="Seg_7318" s="T967">dĭgəttə</ta>
            <ta e="T969" id="Seg_7319" s="T968">tüžöj-bə</ta>
            <ta e="T970" id="Seg_7320" s="T969">sürer-bia-m</ta>
            <ta e="T971" id="Seg_7321" s="T970">bü-nə</ta>
            <ta e="T972" id="Seg_7322" s="T971">bĭt-əl-bie-m</ta>
            <ta e="T973" id="Seg_7323" s="T972">dĭgəttə</ta>
            <ta e="T975" id="Seg_7324" s="T974">šonə-ga</ta>
            <ta e="T977" id="Seg_7325" s="T976">koʔbdo</ta>
            <ta e="T978" id="Seg_7326" s="T977">šonə-ga</ta>
            <ta e="T979" id="Seg_7327" s="T978">măna</ta>
            <ta e="T980" id="Seg_7328" s="T979">davaj</ta>
            <ta e="T981" id="Seg_7329" s="T980">kĭr-zittə</ta>
            <ta e="T982" id="Seg_7330" s="T981">măn</ta>
            <ta e="T983" id="Seg_7331" s="T982">nu-bia-m</ta>
            <ta e="T984" id="Seg_7332" s="T983">dĭ</ta>
            <ta e="T985" id="Seg_7333" s="T984">kĭr-bi</ta>
            <ta e="T986" id="Seg_7334" s="T985">măna</ta>
            <ta e="T987" id="Seg_7335" s="T986">dĭgəttə</ta>
            <ta e="T990" id="Seg_7336" s="T989">šo-bi-baʔ</ta>
            <ta e="T991" id="Seg_7337" s="T990">tura-nə</ta>
            <ta e="T992" id="Seg_7338" s="T991">dʼăbaktər-zittə</ta>
            <ta e="T994" id="Seg_7339" s="T993">dĭgəttə</ta>
            <ta e="T995" id="Seg_7340" s="T994">dʼăbaktər-zittə</ta>
            <ta e="T996" id="Seg_7341" s="T995">plʼonka-nə</ta>
            <ta e="T998" id="Seg_7342" s="T997">kurizə-ʔi</ta>
            <ta e="T999" id="Seg_7343" s="T998">bădə-bia-m</ta>
            <ta e="T1000" id="Seg_7344" s="T999">tüžöj-n</ta>
            <ta e="T1001" id="Seg_7345" s="T1000">tüʔ</ta>
            <ta e="T1002" id="Seg_7346" s="T1001">barəʔ-pia-m</ta>
            <ta e="T1004" id="Seg_7347" s="T1003">kabarləj</ta>
            <ta e="T1005" id="Seg_7348" s="T1004">dʼăbaktər-zittə</ta>
            <ta e="T1006" id="Seg_7349" s="T1005">ato</ta>
            <ta e="T1007" id="Seg_7350" s="T1006">es-seŋ</ta>
            <ta e="T1008" id="Seg_7351" s="T1007">büžü</ta>
            <ta e="T1009" id="Seg_7352" s="T1008">šo-lə-ʔi</ta>
            <ta e="T1010" id="Seg_7353" s="T1009">škola-gəʔ</ta>
            <ta e="T1011" id="Seg_7354" s="T1010">ej</ta>
            <ta e="T1012" id="Seg_7355" s="T1011">mĭ-lə-ʔi</ta>
            <ta e="T1013" id="Seg_7356" s="T1012">dʼăbaktər-zittə</ta>
            <ta e="T1014" id="Seg_7357" s="T1013">bar</ta>
            <ta e="T1015" id="Seg_7358" s="T1014">kirgar-lə-ʔi</ta>
         </annotation>
         <annotation name="mp" tierref="mp-PKZ">
            <ta e="T1" id="Seg_7359" s="T0">măn</ta>
            <ta e="T2" id="Seg_7360" s="T1">amno-bi-m</ta>
            <ta e="T3" id="Seg_7361" s="T2">sumna</ta>
            <ta e="T4" id="Seg_7362" s="T3">tibi-zAŋ</ta>
            <ta e="T5" id="Seg_7363" s="T4">i-bi-jəʔ</ta>
            <ta e="T6" id="Seg_7364" s="T5">măn</ta>
            <ta e="T7" id="Seg_7365" s="T6">dĭgəttə</ta>
            <ta e="T8" id="Seg_7366" s="T7">muktuʔ</ta>
            <ta e="T9" id="Seg_7367" s="T8">i-bi-m</ta>
            <ta e="T10" id="Seg_7368" s="T9">dĭ</ta>
            <ta e="T11" id="Seg_7369" s="T10">muktuʔ</ta>
            <ta e="T12" id="Seg_7370" s="T11">tibi</ta>
            <ta e="T13" id="Seg_7371" s="T12">ej</ta>
            <ta e="T14" id="Seg_7372" s="T13">amno-bi</ta>
            <ta e="T15" id="Seg_7373" s="T14">barəʔ-bi</ta>
            <ta e="T16" id="Seg_7374" s="T15">da</ta>
            <ta e="T776" id="Seg_7375" s="T16">kan-lAʔ</ta>
            <ta e="T17" id="Seg_7376" s="T776">tʼür-bi</ta>
            <ta e="T18" id="Seg_7377" s="T17">dĭgəttə</ta>
            <ta e="T21" id="Seg_7378" s="T20">tibi-m</ta>
            <ta e="T22" id="Seg_7379" s="T21">bü-Kən</ta>
            <ta e="T23" id="Seg_7380" s="T22">kü-laːm-bi</ta>
            <ta e="T24" id="Seg_7381" s="T23">nʼi-m</ta>
            <ta e="T25" id="Seg_7382" s="T24">bü-Kən</ta>
            <ta e="T26" id="Seg_7383" s="T25">kü-laːm-bi</ta>
            <ta e="T27" id="Seg_7384" s="T26">dĭgəttə</ta>
            <ta e="T28" id="Seg_7385" s="T27">ija-m</ta>
            <ta e="T29" id="Seg_7386" s="T28">kut-bi-jəʔ</ta>
            <ta e="T30" id="Seg_7387" s="T29">sʼestra-m</ta>
            <ta e="T31" id="Seg_7388" s="T30">kut-bi-jəʔ</ta>
            <ta e="T32" id="Seg_7389" s="T31">aba-m</ta>
            <ta e="T33" id="Seg_7390" s="T32">dʼije-Tə</ta>
            <ta e="T34" id="Seg_7391" s="T33">kan-bi</ta>
            <ta e="T35" id="Seg_7392" s="T34">i</ta>
            <ta e="T36" id="Seg_7393" s="T35">dĭn</ta>
            <ta e="T37" id="Seg_7394" s="T36">kü-laːm-bi</ta>
            <ta e="T38" id="Seg_7395" s="T37">măn</ta>
            <ta e="T39" id="Seg_7396" s="T38">sʼestra-m</ta>
            <ta e="T40" id="Seg_7397" s="T39">tibi-t</ta>
            <ta e="T41" id="Seg_7398" s="T40">kut-bi</ta>
            <ta e="T42" id="Seg_7399" s="T41">šide</ta>
            <ta e="T44" id="Seg_7400" s="T43">nʼi</ta>
            <ta e="T45" id="Seg_7401" s="T44">ma-bi</ta>
            <ta e="T46" id="Seg_7402" s="T45">măn</ta>
            <ta e="T47" id="Seg_7403" s="T46">dĭ-zem</ta>
            <ta e="T48" id="Seg_7404" s="T47">bar</ta>
            <ta e="T49" id="Seg_7405" s="T48">özer-bi-m</ta>
            <ta e="T50" id="Seg_7406" s="T49">oldʼa</ta>
            <ta e="T51" id="Seg_7407" s="T50">i-bi-m</ta>
            <ta e="T52" id="Seg_7408" s="T51">i</ta>
            <ta e="T53" id="Seg_7409" s="T52">tüšə-lə-bi-m</ta>
            <ta e="T55" id="Seg_7410" s="T54">sazən</ta>
            <ta e="T56" id="Seg_7411" s="T55">onʼiʔ</ta>
            <ta e="T60" id="Seg_7412" s="T59">onʼiʔ</ta>
            <ta e="T61" id="Seg_7413" s="T60">măna</ta>
            <ta e="T62" id="Seg_7414" s="T61">amno-bi</ta>
            <ta e="T63" id="Seg_7415" s="T62">dĭgəttə</ta>
            <ta e="T64" id="Seg_7416" s="T63">dĭ-m</ta>
            <ta e="T65" id="Seg_7417" s="T64">i-bi-jəʔ</ta>
            <ta e="T66" id="Seg_7418" s="T65">văjna-Tə</ta>
            <ta e="T67" id="Seg_7419" s="T66">dĭn</ta>
            <ta e="T68" id="Seg_7420" s="T67">kut-bi-jəʔ</ta>
            <ta e="T69" id="Seg_7421" s="T68">dĭ-n</ta>
            <ta e="T70" id="Seg_7422" s="T69">koʔbdo</ta>
            <ta e="T71" id="Seg_7423" s="T70">ma-luʔbdə-bi</ta>
            <ta e="T73" id="Seg_7424" s="T72">măn</ta>
            <ta e="T75" id="Seg_7425" s="T74">pʼeːš-də</ta>
            <ta e="T77" id="Seg_7426" s="T76">kănzə-laːm-bi-m</ta>
            <ta e="T78" id="Seg_7427" s="T77">pʼeːš-Tə</ta>
            <ta e="T82" id="Seg_7428" s="T81">sʼa-bi-m</ta>
            <ta e="T83" id="Seg_7429" s="T82">amnə-bi-m</ta>
            <ta e="T84" id="Seg_7430" s="T83">iʔbö-bi-m</ta>
            <ta e="T85" id="Seg_7431" s="T84">kunol-bi-m</ta>
            <ta e="T86" id="Seg_7432" s="T85">sanə</ta>
            <ta e="T88" id="Seg_7433" s="T87">lej-lV-m</ta>
            <ta e="T89" id="Seg_7434" s="T88">dĭgəttə</ta>
            <ta e="T90" id="Seg_7435" s="T89">šiʔ</ta>
            <ta e="T91" id="Seg_7436" s="T90">šo-bi-jəʔ</ta>
            <ta e="T92" id="Seg_7437" s="T91">măn</ta>
            <ta e="T93" id="Seg_7438" s="T92">šo-bi-m</ta>
            <ta e="T94" id="Seg_7439" s="T93">šiʔnʼileʔ</ta>
            <ta e="T95" id="Seg_7440" s="T94">i</ta>
            <ta e="T96" id="Seg_7441" s="T95">bar</ta>
            <ta e="T97" id="Seg_7442" s="T96">tʼăbaktər-laʔbə-m</ta>
            <ta e="T99" id="Seg_7443" s="T98">nagur</ta>
            <ta e="T100" id="Seg_7444" s="T99">kaga</ta>
            <ta e="T101" id="Seg_7445" s="T100">amno-bi-jəʔ</ta>
            <ta e="T102" id="Seg_7446" s="T101">i</ta>
            <ta e="T103" id="Seg_7447" s="T102">dĭgəttə</ta>
            <ta e="T104" id="Seg_7448" s="T103">kan-bi-jəʔ</ta>
            <ta e="T105" id="Seg_7449" s="T104">pa</ta>
            <ta e="T106" id="Seg_7450" s="T105">hʼaʔ-zittə</ta>
            <ta e="T107" id="Seg_7451" s="T106">dĭ-zAŋ-Kən</ta>
            <ta e="T108" id="Seg_7452" s="T107">šü</ta>
            <ta e="T109" id="Seg_7453" s="T108">naga-bi</ta>
            <ta e="T111" id="Seg_7454" s="T110">ku-bi-jəʔ</ta>
            <ta e="T112" id="Seg_7455" s="T111">šü</ta>
            <ta e="T113" id="Seg_7456" s="T112">nend-laʔbə</ta>
            <ta e="T114" id="Seg_7457" s="T113">kan-ə-ʔ</ta>
            <ta e="T115" id="Seg_7458" s="T114">i-ʔ</ta>
            <ta e="T116" id="Seg_7459" s="T115">šü</ta>
            <ta e="T117" id="Seg_7460" s="T116">dĭ</ta>
            <ta e="T118" id="Seg_7461" s="T117">kan-bi</ta>
            <ta e="T119" id="Seg_7462" s="T118">dĭn</ta>
            <ta e="T120" id="Seg_7463" s="T119">büzʼe</ta>
            <ta e="T121" id="Seg_7464" s="T120">amno-laʔbə</ta>
            <ta e="T123" id="Seg_7465" s="T122">nörbə-ʔ</ta>
            <ta e="T125" id="Seg_7466" s="T124">dĭgəttə</ta>
            <ta e="T126" id="Seg_7467" s="T125">šü</ta>
            <ta e="T127" id="Seg_7468" s="T126">mĭ-lV-m</ta>
            <ta e="T128" id="Seg_7469" s="T127">a</ta>
            <ta e="T129" id="Seg_7470" s="T128">dĭ</ta>
            <ta e="T130" id="Seg_7471" s="T129">măn-ntə</ta>
            <ta e="T131" id="Seg_7472" s="T130">măn</ta>
            <ta e="T132" id="Seg_7473" s="T131">ej</ta>
            <ta e="T133" id="Seg_7474" s="T132">tĭmne-m</ta>
            <ta e="T134" id="Seg_7475" s="T133">dĭ</ta>
            <ta e="T135" id="Seg_7476" s="T134">bögəl-də</ta>
            <ta e="T140" id="Seg_7477" s="T139">băt-bi</ta>
            <ta e="T142" id="Seg_7478" s="T141">nagur</ta>
            <ta e="T144" id="Seg_7479" s="T143">băt-bi</ta>
            <ta e="T145" id="Seg_7480" s="T144">dĭ</ta>
            <ta e="T1017" id="Seg_7481" s="T146">kan-lAʔ</ta>
            <ta e="T147" id="Seg_7482" s="T1017">tʼür-bi</ta>
            <ta e="T148" id="Seg_7483" s="T147">i</ta>
            <ta e="T149" id="Seg_7484" s="T148">šü</ta>
            <ta e="T150" id="Seg_7485" s="T149">ej</ta>
            <ta e="T151" id="Seg_7486" s="T150">mĭ-bi</ta>
            <ta e="T153" id="Seg_7487" s="T151">dĭgəttə</ta>
            <ta e="T154" id="Seg_7488" s="T153">dĭgəttə</ta>
            <ta e="T155" id="Seg_7489" s="T154">onʼiʔ</ta>
            <ta e="T156" id="Seg_7490" s="T155">bazoʔ</ta>
            <ta e="T157" id="Seg_7491" s="T156">kan-bi</ta>
            <ta e="T158" id="Seg_7492" s="T157">i</ta>
            <ta e="T159" id="Seg_7493" s="T158">dĭ-Tə</ta>
            <ta e="T160" id="Seg_7494" s="T159">măn-bi</ta>
            <ta e="T161" id="Seg_7495" s="T160">nörbə-ʔ</ta>
            <ta e="T163" id="Seg_7496" s="T162">dĭ</ta>
            <ta e="T164" id="Seg_7497" s="T163">măn-ntə</ta>
            <ta e="T165" id="Seg_7498" s="T164">măn</ta>
            <ta e="T166" id="Seg_7499" s="T165">ej</ta>
            <ta e="T167" id="Seg_7500" s="T166">tĭmne-m</ta>
            <ta e="T168" id="Seg_7501" s="T167">no</ta>
            <ta e="T169" id="Seg_7502" s="T168">dĭgəttə</ta>
            <ta e="T170" id="Seg_7503" s="T169">dĭn</ta>
            <ta e="T171" id="Seg_7504" s="T170">bar</ta>
            <ta e="T172" id="Seg_7505" s="T171">dʼagar-bi</ta>
            <ta e="T173" id="Seg_7506" s="T172">bögəl-də</ta>
            <ta e="T174" id="Seg_7507" s="T173">i</ta>
            <ta e="T175" id="Seg_7508" s="T174">kan-bi</ta>
            <ta e="T176" id="Seg_7509" s="T175">ej</ta>
            <ta e="T177" id="Seg_7510" s="T176">mĭ-bi</ta>
            <ta e="T178" id="Seg_7511" s="T177">šü</ta>
            <ta e="T179" id="Seg_7512" s="T178">dĭgəttə</ta>
            <ta e="T180" id="Seg_7513" s="T179">onʼiʔ</ta>
            <ta e="T181" id="Seg_7514" s="T180">bazoʔ</ta>
            <ta e="T182" id="Seg_7515" s="T181">šo-bi</ta>
            <ta e="T183" id="Seg_7516" s="T182">nörbə-ʔ</ta>
            <ta e="T185" id="Seg_7517" s="T184">no</ta>
            <ta e="T186" id="Seg_7518" s="T185">dĭ</ta>
            <ta e="T187" id="Seg_7519" s="T186">măn-ntə</ta>
            <ta e="T188" id="Seg_7520" s="T187">nörbə-lV-m</ta>
            <ta e="T189" id="Seg_7521" s="T188">biəʔ</ta>
            <ta e="T190" id="Seg_7522" s="T189">i-bi-jəʔ</ta>
            <ta e="T191" id="Seg_7523" s="T190">kaga-jəʔ</ta>
            <ta e="T192" id="Seg_7524" s="T191">dĭgəttə</ta>
            <ta e="T193" id="Seg_7525" s="T192">kan-bi-jəʔ</ta>
            <ta e="T194" id="Seg_7526" s="T193">pa</ta>
            <ta e="T195" id="Seg_7527" s="T194">hʼaʔ-zittə</ta>
            <ta e="T196" id="Seg_7528" s="T195">dĭgəttə</ta>
            <ta e="T197" id="Seg_7529" s="T196">šojmu</ta>
            <ta e="T198" id="Seg_7530" s="T197">süʔmə-luʔbdə-bi</ta>
            <ta e="T199" id="Seg_7531" s="T198">dĭ</ta>
            <ta e="T200" id="Seg_7532" s="T199">bar</ta>
            <ta e="T201" id="Seg_7533" s="T200">tʼo-Tə</ta>
            <ta e="T1018" id="Seg_7534" s="T201">kan-lAʔ</ta>
            <ta e="T202" id="Seg_7535" s="T1018">tʼür-bi</ta>
            <ta e="T203" id="Seg_7536" s="T202">dĭgəttə</ta>
            <ta e="T204" id="Seg_7537" s="T203">nabə</ta>
            <ta e="T205" id="Seg_7538" s="T204">šo-bi</ta>
            <ta e="T206" id="Seg_7539" s="T205">maʔ</ta>
            <ta e="T207" id="Seg_7540" s="T206">a-bi</ta>
            <ta e="T208" id="Seg_7541" s="T207">munəj-jəʔ</ta>
            <ta e="T209" id="Seg_7542" s="T208">nʼe-bi</ta>
            <ta e="T210" id="Seg_7543" s="T209">dĭgəttə</ta>
            <ta e="T211" id="Seg_7544" s="T210">šo-bi</ta>
            <ta e="T212" id="Seg_7545" s="T211">dʼije-gəʔ</ta>
            <ta e="T213" id="Seg_7546" s="T212">men</ta>
            <ta e="T214" id="Seg_7547" s="T213">urgo</ta>
            <ta e="T215" id="Seg_7548" s="T214">i</ta>
            <ta e="T216" id="Seg_7549" s="T215">davaj</ta>
            <ta e="T217" id="Seg_7550" s="T216">munəj-jəʔ</ta>
            <ta e="T218" id="Seg_7551" s="T217">am-zittə</ta>
            <ta e="T219" id="Seg_7552" s="T218">dĭ</ta>
            <ta e="T221" id="Seg_7553" s="T220">dʼabə-luʔbdə-bi</ta>
            <ta e="T222" id="Seg_7554" s="T221">xvostə</ta>
            <ta e="T223" id="Seg_7555" s="T222">dĭ</ta>
            <ta e="T224" id="Seg_7556" s="T223">bar</ta>
            <ta e="T226" id="Seg_7557" s="T224">dĭ-m</ta>
            <ta e="T229" id="Seg_7558" s="T228">dĭgəttə</ta>
            <ta e="T230" id="Seg_7559" s="T229">dĭ</ta>
            <ta e="T231" id="Seg_7560" s="T230">dʼabə-luʔbdə-bi</ta>
            <ta e="T232" id="Seg_7561" s="T231">xvostə</ta>
            <ta e="T233" id="Seg_7562" s="T232">xvostə</ta>
            <ta e="T235" id="Seg_7563" s="T234">săj_nʼeʔbdə-bi</ta>
            <ta e="T236" id="Seg_7564" s="T235">dĭgəttə</ta>
            <ta e="T237" id="Seg_7565" s="T236">tagaj-ziʔ</ta>
            <ta e="T239" id="Seg_7566" s="T238">băt-bi</ta>
            <ta e="T240" id="Seg_7567" s="T239">dĭn</ta>
            <ta e="T241" id="Seg_7568" s="T240">jašɨk</ta>
            <ta e="T242" id="Seg_7569" s="T241">ku-bi</ta>
            <ta e="T243" id="Seg_7570" s="T242">jašɨk-Kən</ta>
            <ta e="T244" id="Seg_7571" s="T243">sazən</ta>
            <ta e="T245" id="Seg_7572" s="T244">pʼaŋnomə</ta>
            <ta e="T246" id="Seg_7573" s="T245">măn</ta>
            <ta e="T248" id="Seg_7574" s="T247">măn</ta>
            <ta e="T249" id="Seg_7575" s="T248">aba-m</ta>
            <ta e="T250" id="Seg_7576" s="T249">tăn</ta>
            <ta e="T252" id="Seg_7577" s="T251">aba-t-ziʔ</ta>
            <ta e="T253" id="Seg_7578" s="T252">tăn</ta>
            <ta e="T254" id="Seg_7579" s="T253">aba-gəndə</ta>
            <ta e="T255" id="Seg_7580" s="T254">tüʔ-zittə</ta>
            <ta e="T1019" id="Seg_7581" s="T255">kan-lAʔ</ta>
            <ta e="T256" id="Seg_7582" s="T1019">tʼür-bi</ta>
            <ta e="T258" id="Seg_7583" s="T257">dĭgəttə</ta>
            <ta e="T259" id="Seg_7584" s="T258">dĭ</ta>
            <ta e="T260" id="Seg_7585" s="T259">büzʼe</ta>
            <ta e="T261" id="Seg_7586" s="T260">măn-bi</ta>
            <ta e="T262" id="Seg_7587" s="T261">e-ʔ</ta>
            <ta e="T263" id="Seg_7588" s="T262">šʼaːm-ʔ</ta>
            <ta e="T264" id="Seg_7589" s="T263">a</ta>
            <ta e="T265" id="Seg_7590" s="T264">dĭ</ta>
            <ta e="T267" id="Seg_7591" s="T266">dĭ</ta>
            <ta e="T269" id="Seg_7592" s="T268">kuza</ta>
            <ta e="T270" id="Seg_7593" s="T269">dĭ-Tə</ta>
            <ta e="T271" id="Seg_7594" s="T270">bögəl-də</ta>
            <ta e="T273" id="Seg_7595" s="T272">băt-bi</ta>
            <ta e="T274" id="Seg_7596" s="T273">i</ta>
            <ta e="T275" id="Seg_7597" s="T274">šü</ta>
            <ta e="T276" id="Seg_7598" s="T275">i-bi</ta>
            <ta e="T277" id="Seg_7599" s="T276">i</ta>
            <ta e="T1020" id="Seg_7600" s="T277">kan-lAʔ</ta>
            <ta e="T278" id="Seg_7601" s="T1020">tʼür-bi</ta>
            <ta e="T280" id="Seg_7602" s="T279">măn</ta>
            <ta e="T281" id="Seg_7603" s="T280">mej-m</ta>
            <ta e="T282" id="Seg_7604" s="T281">măn</ta>
            <ta e="T283" id="Seg_7605" s="T282">tibi</ta>
            <ta e="T286" id="Seg_7606" s="T285">i-lAʔ-bi</ta>
            <ta e="T287" id="Seg_7607" s="T286">i</ta>
            <ta e="T1021" id="Seg_7608" s="T287">kan-lAʔ</ta>
            <ta e="T288" id="Seg_7609" s="T1021">tʼür-bi-jəʔ</ta>
            <ta e="T289" id="Seg_7610" s="T288">miʔ</ta>
            <ta e="T290" id="Seg_7611" s="T289">jakšə</ta>
            <ta e="T291" id="Seg_7612" s="T290">amno-bi-jəʔ</ta>
            <ta e="T292" id="Seg_7613" s="T291">a</ta>
            <ta e="T293" id="Seg_7614" s="T292">dĭ</ta>
            <ta e="T294" id="Seg_7615" s="T293">măna</ta>
            <ta e="T295" id="Seg_7616" s="T294">ajir-bi</ta>
            <ta e="T296" id="Seg_7617" s="T295">uda-Tə</ta>
            <ta e="T297" id="Seg_7618" s="T296">i-bi</ta>
            <ta e="T298" id="Seg_7619" s="T297">a</ta>
            <ta e="T299" id="Seg_7620" s="T298">dĭgəttə</ta>
            <ta e="T1022" id="Seg_7621" s="T299">kan-lAʔ</ta>
            <ta e="T300" id="Seg_7622" s="T1022">tʼür-bi</ta>
            <ta e="T301" id="Seg_7623" s="T300">Igarka-Tə</ta>
            <ta e="T302" id="Seg_7624" s="T301">i</ta>
            <ta e="T303" id="Seg_7625" s="T302">dĭn</ta>
            <ta e="T304" id="Seg_7626" s="T303">kü-laːm-bi</ta>
            <ta e="T306" id="Seg_7627" s="T305">a</ta>
            <ta e="T307" id="Seg_7628" s="T306">dĭ</ta>
            <ta e="T308" id="Seg_7629" s="T307">ej</ta>
            <ta e="T309" id="Seg_7630" s="T308">kuvas</ta>
            <ta e="T310" id="Seg_7631" s="T309">i-bi</ta>
            <ta e="T311" id="Seg_7632" s="T310">aŋ-də</ta>
            <ta e="T313" id="Seg_7633" s="T311">bar</ta>
            <ta e="T315" id="Seg_7634" s="T314">dĭ</ta>
            <ta e="T316" id="Seg_7635" s="T315">ej</ta>
            <ta e="T317" id="Seg_7636" s="T316">kuvas</ta>
            <ta e="T318" id="Seg_7637" s="T317">aŋ-də</ta>
            <ta e="T319" id="Seg_7638" s="T318">păjdʼaŋ</ta>
            <ta e="T320" id="Seg_7639" s="T319">sima-t</ta>
            <ta e="T321" id="Seg_7640" s="T320">păjdʼaŋ</ta>
            <ta e="T323" id="Seg_7641" s="T322">šide</ta>
            <ta e="T324" id="Seg_7642" s="T323">ne</ta>
            <ta e="T325" id="Seg_7643" s="T324">amno-bi-jəʔ</ta>
            <ta e="T326" id="Seg_7644" s="T325">kan-bi-jəʔ</ta>
            <ta e="T327" id="Seg_7645" s="T326">saranka-jəʔ</ta>
            <ta e="T329" id="Seg_7646" s="T328">tĭl-zittə</ta>
            <ta e="T330" id="Seg_7647" s="T329">onʼiʔ</ta>
            <ta e="T331" id="Seg_7648" s="T330">tĭl-bi</ta>
            <ta e="T332" id="Seg_7649" s="T331">iʔgö</ta>
            <ta e="T333" id="Seg_7650" s="T332">a</ta>
            <ta e="T334" id="Seg_7651" s="T333">onʼiʔ</ta>
            <ta e="T335" id="Seg_7652" s="T334">amka</ta>
            <ta e="T336" id="Seg_7653" s="T335">girgit</ta>
            <ta e="T337" id="Seg_7654" s="T336">iʔgö</ta>
            <ta e="T338" id="Seg_7655" s="T337">tĭl-bi</ta>
            <ta e="T339" id="Seg_7656" s="T338">a</ta>
            <ta e="T340" id="Seg_7657" s="T339">girgit</ta>
            <ta e="T341" id="Seg_7658" s="T340">amka</ta>
            <ta e="T342" id="Seg_7659" s="T341">tĭl-bi</ta>
            <ta e="T343" id="Seg_7660" s="T342">dĭ-m</ta>
            <ta e="T345" id="Seg_7661" s="T344">baltu-ziʔ</ta>
            <ta e="T346" id="Seg_7662" s="T345">ulu-t</ta>
            <ta e="T347" id="Seg_7663" s="T346">hʼaʔ-bi</ta>
            <ta e="T348" id="Seg_7664" s="T347">dĭgəttə</ta>
            <ta e="T349" id="Seg_7665" s="T348">šo-bi</ta>
            <ta e="T350" id="Seg_7666" s="T349">maʔ-Tə</ta>
            <ta e="T351" id="Seg_7667" s="T350">koʔbdo</ta>
            <ta e="T352" id="Seg_7668" s="T351">šo-bi</ta>
            <ta e="T353" id="Seg_7669" s="T352">gijen</ta>
            <ta e="T354" id="Seg_7670" s="T353">măn</ta>
            <ta e="T355" id="Seg_7671" s="T354">ija-m</ta>
            <ta e="T356" id="Seg_7672" s="T355">dĭ</ta>
            <ta e="T357" id="Seg_7673" s="T356">bar</ta>
            <ta e="T358" id="Seg_7674" s="T357">ɨrɨː</ta>
            <ta e="T359" id="Seg_7675" s="T358">ej</ta>
            <ta e="T360" id="Seg_7676" s="T359">tĭl-bi</ta>
            <ta e="T361" id="Seg_7677" s="T360">saranka-jəʔ</ta>
            <ta e="T362" id="Seg_7678" s="T361">dĭn</ta>
            <ta e="T363" id="Seg_7679" s="T362">bar</ta>
            <ta e="T364" id="Seg_7680" s="T363">kunol-laʔbə</ta>
            <ta e="T365" id="Seg_7681" s="T364">a</ta>
            <ta e="T366" id="Seg_7682" s="T365">koʔbdo-t</ta>
            <ta e="T367" id="Seg_7683" s="T366">kădaʔ=də</ta>
            <ta e="T368" id="Seg_7684" s="T367">kădaʔ</ta>
            <ta e="T372" id="Seg_7685" s="T371">tĭmne-bi</ta>
            <ta e="T373" id="Seg_7686" s="T372">i</ta>
            <ta e="T374" id="Seg_7687" s="T373">šo-bi</ta>
            <ta e="T375" id="Seg_7688" s="T374">nʼi-t</ta>
            <ta e="T376" id="Seg_7689" s="T375">i-bi</ta>
            <ta e="T377" id="Seg_7690" s="T376">dĭ</ta>
            <ta e="T378" id="Seg_7691" s="T377">dĭ-m</ta>
            <ta e="T379" id="Seg_7692" s="T378">šaʔ-lAʔ</ta>
            <ta e="T380" id="Seg_7693" s="T379">i-bi</ta>
            <ta e="T382" id="Seg_7694" s="T381">kunol-zittə</ta>
            <ta e="T383" id="Seg_7695" s="T382">hen-bi</ta>
            <ta e="T384" id="Seg_7696" s="T383">a</ta>
            <ta e="T385" id="Seg_7697" s="T384">bos-də</ta>
            <ta e="T386" id="Seg_7698" s="T385">nʼuʔdə</ta>
            <ta e="T389" id="Seg_7699" s="T388">sʼa-bi</ta>
            <ta e="T390" id="Seg_7700" s="T389">maʔ-Tə</ta>
            <ta e="T392" id="Seg_7701" s="T391">i-bi</ta>
            <ta e="T393" id="Seg_7702" s="T392">dĭgəttə</ta>
            <ta e="T395" id="Seg_7703" s="T394">dĭ</ta>
            <ta e="T396" id="Seg_7704" s="T395">ne</ta>
            <ta e="T397" id="Seg_7705" s="T396">šo-bi</ta>
            <ta e="T398" id="Seg_7706" s="T397">gijen</ta>
            <ta e="T399" id="Seg_7707" s="T398">šiʔ</ta>
            <ta e="T401" id="Seg_7708" s="T400">dĭ</ta>
            <ta e="T402" id="Seg_7709" s="T401">măn-ntə</ta>
            <ta e="T403" id="Seg_7710" s="T402">măn</ta>
            <ta e="T404" id="Seg_7711" s="T403">dön</ta>
            <ta e="T405" id="Seg_7712" s="T404">amno-laʔbə-m</ta>
            <ta e="T406" id="Seg_7713" s="T405">dĭ</ta>
            <ta e="T410" id="Seg_7714" s="T409">kămnə-bi</ta>
            <ta e="T411" id="Seg_7715" s="T410">sima-gəndə</ta>
            <ta e="T413" id="Seg_7716" s="T412">ĭmbi=də</ta>
            <ta e="T414" id="Seg_7717" s="T413">ej</ta>
            <ta e="T415" id="Seg_7718" s="T414">măndo-liA-jəʔ</ta>
            <ta e="T416" id="Seg_7719" s="T415">dĭgəttə</ta>
            <ta e="T417" id="Seg_7720" s="T416">sima-t</ta>
            <ta e="T418" id="Seg_7721" s="T417">ĭmbi=də</ta>
            <ta e="T419" id="Seg_7722" s="T418">ej</ta>
            <ta e="T420" id="Seg_7723" s="T419">mo-liA-jəʔ</ta>
            <ta e="T421" id="Seg_7724" s="T420">dĭ</ta>
            <ta e="T422" id="Seg_7725" s="T421">süʔmə-luʔbdə-bi</ta>
            <ta e="T423" id="Seg_7726" s="T422">maʔ-Tə</ta>
            <ta e="T424" id="Seg_7727" s="T423">kaga-t</ta>
            <ta e="T425" id="Seg_7728" s="T424">i-bi</ta>
            <ta e="T426" id="Seg_7729" s="T425">i</ta>
            <ta e="T1023" id="Seg_7730" s="T426">kan-lAʔ</ta>
            <ta e="T427" id="Seg_7731" s="T1023">tʼür-bi</ta>
            <ta e="T428" id="Seg_7732" s="T427">gijen</ta>
            <ta e="T429" id="Seg_7733" s="T428">il</ta>
            <ta e="T430" id="Seg_7734" s="T429">iʔgö</ta>
            <ta e="T432" id="Seg_7735" s="T431">măn</ta>
            <ta e="T433" id="Seg_7736" s="T432">tibi</ta>
            <ta e="T1024" id="Seg_7737" s="T433">kan-lAʔ</ta>
            <ta e="T434" id="Seg_7738" s="T1024">tʼür-bi</ta>
            <ta e="T435" id="Seg_7739" s="T434">măn</ta>
            <ta e="T436" id="Seg_7740" s="T435">ugaːndə</ta>
            <ta e="T437" id="Seg_7741" s="T436">tăŋ</ta>
            <ta e="T438" id="Seg_7742" s="T437">tʼor-bi-m</ta>
            <ta e="T439" id="Seg_7743" s="T438">sĭj-m</ta>
            <ta e="T440" id="Seg_7744" s="T439">bar</ta>
            <ta e="T441" id="Seg_7745" s="T440">ĭzem-bi</ta>
            <ta e="T442" id="Seg_7746" s="T441">i</ta>
            <ta e="T443" id="Seg_7747" s="T442">ulu-m</ta>
            <ta e="T447" id="Seg_7748" s="T446">măn</ta>
            <ta e="T449" id="Seg_7749" s="T448">sʼestra-m</ta>
            <ta e="T450" id="Seg_7750" s="T449">nʼi</ta>
            <ta e="T451" id="Seg_7751" s="T450">nʼi-t</ta>
            <ta e="T452" id="Seg_7752" s="T451">tʼermən-də</ta>
            <ta e="T453" id="Seg_7753" s="T452">mĭn-bi</ta>
            <ta e="T454" id="Seg_7754" s="T453">ipek</ta>
            <ta e="T456" id="Seg_7755" s="T455">tʼermən-Tə</ta>
            <ta e="T457" id="Seg_7756" s="T456">mĭ-bi</ta>
            <ta e="T458" id="Seg_7757" s="T457">un</ta>
            <ta e="T459" id="Seg_7758" s="T458">nʼeʔbdə-zittə</ta>
            <ta e="T460" id="Seg_7759" s="T459">dĭgəttə</ta>
            <ta e="T461" id="Seg_7760" s="T460">maʔ-gəndə</ta>
            <ta e="T462" id="Seg_7761" s="T461">šo-bi</ta>
            <ta e="T463" id="Seg_7762" s="T462">bos-də</ta>
            <ta e="T464" id="Seg_7763" s="T463">ne-Tə</ta>
            <ta e="T465" id="Seg_7764" s="T464">măn-liA</ta>
            <ta e="T466" id="Seg_7765" s="T465">štobɨ</ta>
            <ta e="T467" id="Seg_7766" s="T466">kondʼo</ta>
            <ta e="T469" id="Seg_7767" s="T468">kabar-lV-j</ta>
            <ta e="T470" id="Seg_7768" s="T469">a</ta>
            <ta e="T471" id="Seg_7769" s="T470">măn</ta>
            <ta e="T472" id="Seg_7770" s="T471">amno-liA-m</ta>
            <ta e="T473" id="Seg_7771" s="T472">da</ta>
            <ta e="T474" id="Seg_7772" s="T473">tenö-liA-m</ta>
            <ta e="T476" id="Seg_7773" s="T475">što</ta>
            <ta e="T477" id="Seg_7774" s="T476">măn</ta>
            <ta e="T479" id="Seg_7775" s="T478">dĭ-zAŋ-ziʔ</ta>
            <ta e="T480" id="Seg_7776" s="T479">amor-zittə</ta>
            <ta e="T483" id="Seg_7777" s="T482">to</ta>
            <ta e="T484" id="Seg_7778" s="T483">mĭn-bi-jəʔ</ta>
            <ta e="T485" id="Seg_7779" s="T484">boš-də</ta>
            <ta e="T486" id="Seg_7780" s="T485">măn</ta>
            <ta e="T487" id="Seg_7781" s="T486">iʔgö</ta>
            <ta e="T488" id="Seg_7782" s="T487">am-liA-m</ta>
            <ta e="T490" id="Seg_7783" s="T489">onʼiʔ</ta>
            <ta e="T491" id="Seg_7784" s="T490">kuza</ta>
            <ta e="T492" id="Seg_7785" s="T491">monoʔko-zittə</ta>
            <ta e="T494" id="Seg_7786" s="T493">kan-bi</ta>
            <ta e="T495" id="Seg_7787" s="T494">kuvas</ta>
            <ta e="T496" id="Seg_7788" s="T495">koʔbdo</ta>
            <ta e="T497" id="Seg_7789" s="T496">ugaːndə</ta>
            <ta e="T498" id="Seg_7790" s="T497">oldʼa-t</ta>
            <ta e="T499" id="Seg_7791" s="T498">iʔgö</ta>
            <ta e="T500" id="Seg_7792" s="T499">kandə-gA</ta>
            <ta e="T501" id="Seg_7793" s="T500">dĭgəttə</ta>
            <ta e="T502" id="Seg_7794" s="T501">kuza</ta>
            <ta e="T503" id="Seg_7795" s="T502">dĭ-Tə</ta>
            <ta e="T504" id="Seg_7796" s="T503">măn-ntə</ta>
            <ta e="T505" id="Seg_7797" s="T504">gibər</ta>
            <ta e="T506" id="Seg_7798" s="T505">kandə-gA-l</ta>
            <ta e="T507" id="Seg_7799" s="T506">monoʔko-zittə</ta>
            <ta e="T508" id="Seg_7800" s="T507">i-ʔ</ta>
            <ta e="T509" id="Seg_7801" s="T508">măna</ta>
            <ta e="T510" id="Seg_7802" s="T509">a</ta>
            <ta e="T511" id="Seg_7803" s="T510">tăn</ta>
            <ta e="T512" id="Seg_7804" s="T511">ĭmbi</ta>
            <ta e="T513" id="Seg_7805" s="T512">a-liA-l</ta>
            <ta e="T514" id="Seg_7806" s="T513">amor-zittə</ta>
            <ta e="T515" id="Seg_7807" s="T514">da</ta>
            <ta e="T516" id="Seg_7808" s="T515">tüʔ-zittə</ta>
            <ta e="T517" id="Seg_7809" s="T516">dĭgəttə</ta>
            <ta e="T518" id="Seg_7810" s="T517">kan-bi-jəʔ</ta>
            <ta e="T519" id="Seg_7811" s="T518">bazoʔ</ta>
            <ta e="T520" id="Seg_7812" s="T519">bazoʔ</ta>
            <ta e="T521" id="Seg_7813" s="T520">kuza</ta>
            <ta e="T522" id="Seg_7814" s="T521">măn-ntə</ta>
            <ta e="T523" id="Seg_7815" s="T522">gibər</ta>
            <ta e="T524" id="Seg_7816" s="T523">kandə-gA-lAʔ</ta>
            <ta e="T525" id="Seg_7817" s="T524">monoʔko-zittə</ta>
            <ta e="T526" id="Seg_7818" s="T525">măna</ta>
            <ta e="T527" id="Seg_7819" s="T526">i-t</ta>
            <ta e="T528" id="Seg_7820" s="T527">a</ta>
            <ta e="T529" id="Seg_7821" s="T528">tăn</ta>
            <ta e="T530" id="Seg_7822" s="T529">ĭmbi</ta>
            <ta e="T531" id="Seg_7823" s="T530">a-lV-l</ta>
            <ta e="T532" id="Seg_7824" s="T531">bĭs-zittə</ta>
            <ta e="T533" id="Seg_7825" s="T532">da</ta>
            <ta e="T534" id="Seg_7826" s="T533">kĭnzə-zittə</ta>
            <ta e="T535" id="Seg_7827" s="T534">dĭgəttə</ta>
            <ta e="T536" id="Seg_7828" s="T535">šo-bi-jəʔ</ta>
            <ta e="T537" id="Seg_7829" s="T536">i</ta>
            <ta e="T538" id="Seg_7830" s="T537">davaj</ta>
            <ta e="T539" id="Seg_7831" s="T538">monoʔko-zittə</ta>
            <ta e="T540" id="Seg_7832" s="T539">dĭ</ta>
            <ta e="T541" id="Seg_7833" s="T540">măn-ntə</ta>
            <ta e="T542" id="Seg_7834" s="T541">măn</ta>
            <ta e="T543" id="Seg_7835" s="T542">iʔgö</ta>
            <ta e="T544" id="Seg_7836" s="T543">bar</ta>
            <ta e="T545" id="Seg_7837" s="T544">a-lV-m</ta>
            <ta e="T546" id="Seg_7838" s="T545">ipek</ta>
            <ta e="T547" id="Seg_7839" s="T546">pür-lV-m</ta>
            <ta e="T548" id="Seg_7840" s="T547">uja</ta>
            <ta e="T549" id="Seg_7841" s="T548">bar</ta>
            <ta e="T550" id="Seg_7842" s="T549">am-zittə</ta>
            <ta e="T551" id="Seg_7843" s="T550">nadə</ta>
            <ta e="T552" id="Seg_7844" s="T551">dĭ</ta>
            <ta e="T553" id="Seg_7845" s="T552">măn-ntə</ta>
            <ta e="T554" id="Seg_7846" s="T553">no</ta>
            <ta e="T555" id="Seg_7847" s="T554">bar</ta>
            <ta e="T556" id="Seg_7848" s="T555">măn</ta>
            <ta e="T557" id="Seg_7849" s="T556">am-lV-m</ta>
            <ta e="T558" id="Seg_7850" s="T557">i</ta>
            <ta e="T561" id="Seg_7851" s="T560">iʔgö</ta>
            <ta e="T562" id="Seg_7852" s="T561">biəʔ</ta>
            <ta e="T563" id="Seg_7853" s="T562">teʔdə</ta>
            <ta e="T565" id="Seg_7854" s="T564">a-lV-m</ta>
            <ta e="T566" id="Seg_7855" s="T565">no</ta>
            <ta e="T567" id="Seg_7856" s="T566">dĭgəttə</ta>
            <ta e="T569" id="Seg_7857" s="T568">stol-də</ta>
            <ta e="T570" id="Seg_7858" s="T569">oʔbdə-bi</ta>
            <ta e="T571" id="Seg_7859" s="T570">dĭgəttə</ta>
            <ta e="T572" id="Seg_7860" s="T571">šo-bi</ta>
            <ta e="T573" id="Seg_7861" s="T572">dĭ</ta>
            <ta e="T574" id="Seg_7862" s="T573">kuza</ta>
            <ta e="T575" id="Seg_7863" s="T574">kajət</ta>
            <ta e="T576" id="Seg_7864" s="T575">amor-zittə</ta>
            <ta e="T577" id="Seg_7865" s="T576">da</ta>
            <ta e="T578" id="Seg_7866" s="T577">tüʔ-zittə</ta>
            <ta e="T579" id="Seg_7867" s="T578">bar</ta>
            <ta e="T580" id="Seg_7868" s="T579">am-laʔbə</ta>
            <ta e="T581" id="Seg_7869" s="T580">da</ta>
            <ta e="T582" id="Seg_7870" s="T581">tüʔ-laʔbə</ta>
            <ta e="T583" id="Seg_7871" s="T582">am-laʔbə</ta>
            <ta e="T584" id="Seg_7872" s="T583">da</ta>
            <ta e="T585" id="Seg_7873" s="T584">tüʔ-laʔbə</ta>
            <ta e="T586" id="Seg_7874" s="T585">bar</ta>
            <ta e="T587" id="Seg_7875" s="T586">ĭmbi</ta>
            <ta e="T588" id="Seg_7876" s="T587">am-luʔbdə-bi</ta>
            <ta e="T589" id="Seg_7877" s="T588">i</ta>
            <ta e="T590" id="Seg_7878" s="T589">uja</ta>
            <ta e="T591" id="Seg_7879" s="T590">i</ta>
            <ta e="T593" id="Seg_7880" s="T592">ĭmbi</ta>
            <ta e="T594" id="Seg_7881" s="T593">i-bi</ta>
            <ta e="T595" id="Seg_7882" s="T594">bar</ta>
            <ta e="T596" id="Seg_7883" s="T595">bos-də</ta>
            <ta e="T597" id="Seg_7884" s="T596">măn-ntə</ta>
            <ta e="T598" id="Seg_7885" s="T597">det-KAʔ</ta>
            <ta e="T599" id="Seg_7886" s="T598">ĭššo</ta>
            <ta e="T600" id="Seg_7887" s="T599">amka</ta>
            <ta e="T601" id="Seg_7888" s="T600">det-KAʔ</ta>
            <ta e="T602" id="Seg_7889" s="T601">ĭššo</ta>
            <ta e="T603" id="Seg_7890" s="T602">amka</ta>
            <ta e="T604" id="Seg_7891" s="T603">dĭgəttə</ta>
            <ta e="T605" id="Seg_7892" s="T604">už</ta>
            <ta e="T606" id="Seg_7893" s="T605">am-zittə</ta>
            <ta e="T612" id="Seg_7894" s="T611">dĭgəttə</ta>
            <ta e="T613" id="Seg_7895" s="T612">bü</ta>
            <ta e="T615" id="Seg_7896" s="T614">bĭs-zittə</ta>
            <ta e="T616" id="Seg_7897" s="T615">bazoʔ</ta>
            <ta e="T617" id="Seg_7898" s="T616">bazoʔ</ta>
            <ta e="T618" id="Seg_7899" s="T617">šo-bi</ta>
            <ta e="T619" id="Seg_7900" s="T618">dĭ</ta>
            <ta e="T621" id="Seg_7901" s="T620">dĭ</ta>
            <ta e="T622" id="Seg_7902" s="T621">kuza</ta>
            <ta e="T623" id="Seg_7903" s="T622">dĭgəttə</ta>
            <ta e="T624" id="Seg_7904" s="T623">dĭ</ta>
            <ta e="T626" id="Seg_7905" s="T625">bĭs-laʔbə</ta>
            <ta e="T627" id="Seg_7906" s="T626">bĭs-laʔbə</ta>
            <ta e="T628" id="Seg_7907" s="T627">da</ta>
            <ta e="T629" id="Seg_7908" s="T628">kĭnzə-laʔbə</ta>
            <ta e="T630" id="Seg_7909" s="T629">bĭs-laʔbə</ta>
            <ta e="T631" id="Seg_7910" s="T630">da</ta>
            <ta e="T632" id="Seg_7911" s="T631">kĭnzə-laʔbə</ta>
            <ta e="T633" id="Seg_7912" s="T632">bar</ta>
            <ta e="T634" id="Seg_7913" s="T633">bĭs-luʔbdə-bi</ta>
            <ta e="T635" id="Seg_7914" s="T634">da</ta>
            <ta e="T636" id="Seg_7915" s="T635">kirgaːr-laʔbə</ta>
            <ta e="T637" id="Seg_7916" s="T636">ĭššo</ta>
            <ta e="T639" id="Seg_7917" s="T638">det-ʔ</ta>
            <ta e="T640" id="Seg_7918" s="T639">măna</ta>
            <ta e="T641" id="Seg_7919" s="T640">amka</ta>
            <ta e="T642" id="Seg_7920" s="T641">a</ta>
            <ta e="T643" id="Seg_7921" s="T642">ulitsa-Tə</ta>
            <ta e="T644" id="Seg_7922" s="T643">bar</ta>
            <ta e="T645" id="Seg_7923" s="T644">ugaːndə</ta>
            <ta e="T646" id="Seg_7924" s="T645">bü</ta>
            <ta e="T647" id="Seg_7925" s="T646">iʔgö</ta>
            <ta e="T649" id="Seg_7926" s="T648">dĭgəttə</ta>
            <ta e="T650" id="Seg_7927" s="T649">ej</ta>
            <ta e="T651" id="Seg_7928" s="T650">am-bi</ta>
            <ta e="T652" id="Seg_7929" s="T651">da</ta>
            <ta e="T653" id="Seg_7930" s="T652">bü</ta>
            <ta e="T654" id="Seg_7931" s="T653">ej</ta>
            <ta e="T655" id="Seg_7932" s="T654">bĭs-bi</ta>
            <ta e="T656" id="Seg_7933" s="T655">dĭ-Tə</ta>
            <ta e="T657" id="Seg_7934" s="T656">bɨ</ta>
            <ta e="T658" id="Seg_7935" s="T657">ulu-bə</ta>
            <ta e="T659" id="Seg_7936" s="T658">săj-hʼaʔ-bi-jəʔ</ta>
            <ta e="T660" id="Seg_7937" s="T659">bɨ</ta>
            <ta e="T661" id="Seg_7938" s="T660">i</ta>
            <ta e="T662" id="Seg_7939" s="T661">edə-bi-jəʔ</ta>
            <ta e="T664" id="Seg_7940" s="T662">ato</ta>
            <ta e="T665" id="Seg_7941" s="T664">dĭgəttə</ta>
            <ta e="T666" id="Seg_7942" s="T665">dĭ</ta>
            <ta e="T667" id="Seg_7943" s="T666">koʔbdo-m</ta>
            <ta e="T668" id="Seg_7944" s="T667">i-bi</ta>
            <ta e="T669" id="Seg_7945" s="T668">i</ta>
            <ta e="T670" id="Seg_7946" s="T669">maʔ-gəndə</ta>
            <ta e="T671" id="Seg_7947" s="T670">kan-bi</ta>
            <ta e="T673" id="Seg_7948" s="T672">kaba</ta>
            <ta e="T674" id="Seg_7949" s="T673">ĭmbi</ta>
            <ta e="T675" id="Seg_7950" s="T674">nüj-laʔbə-l</ta>
            <ta e="T677" id="Seg_7951" s="T676">a</ta>
            <ta e="T678" id="Seg_7952" s="T677">ĭmbi</ta>
            <ta e="T679" id="Seg_7953" s="T678">ej</ta>
            <ta e="T680" id="Seg_7954" s="T679">jakšə</ta>
            <ta e="T681" id="Seg_7955" s="T680">nüj-laʔbə-l</ta>
            <ta e="T682" id="Seg_7956" s="T681">da</ta>
            <ta e="T684" id="Seg_7957" s="T683">jakšə</ta>
            <ta e="T686" id="Seg_7958" s="T685">nüj-zittə</ta>
            <ta e="T687" id="Seg_7959" s="T686">ku-liA-l</ta>
            <ta e="T688" id="Seg_7960" s="T687">barlaʔina</ta>
            <ta e="T690" id="Seg_7961" s="T689">onʼiʔ</ta>
            <ta e="T691" id="Seg_7962" s="T690">kuza</ta>
            <ta e="T693" id="Seg_7963" s="T692">i-bi</ta>
            <ta e="T694" id="Seg_7964" s="T693">tʼermən-də</ta>
            <ta e="T695" id="Seg_7965" s="T694">a</ta>
            <ta e="T696" id="Seg_7966" s="T695">a</ta>
            <ta e="T697" id="Seg_7967" s="T696">onʼiʔ</ta>
            <ta e="T698" id="Seg_7968" s="T697">kuza</ta>
            <ta e="T699" id="Seg_7969" s="T698">ugaːndə</ta>
            <ta e="T700" id="Seg_7970" s="T699">aktʼa</ta>
            <ta e="T701" id="Seg_7971" s="T700">iʔgö</ta>
            <ta e="T703" id="Seg_7972" s="T702">i-bi</ta>
            <ta e="T704" id="Seg_7973" s="T703">i</ta>
            <ta e="T705" id="Seg_7974" s="T704">oldʼa</ta>
            <ta e="T706" id="Seg_7975" s="T705">i</ta>
            <ta e="T707" id="Seg_7976" s="T706">ipek</ta>
            <ta e="T708" id="Seg_7977" s="T707">dĭ</ta>
            <ta e="T709" id="Seg_7978" s="T708">dĭ-n</ta>
            <ta e="T710" id="Seg_7979" s="T709">kuza-Tə</ta>
            <ta e="T711" id="Seg_7980" s="T710">i-bi</ta>
            <ta e="T712" id="Seg_7981" s="T711">tʼermən-də</ta>
            <ta e="T713" id="Seg_7982" s="T712">i</ta>
            <ta e="T715" id="Seg_7983" s="T714">kon-laːm-bi</ta>
            <ta e="T716" id="Seg_7984" s="T715">dĭ</ta>
            <ta e="T717" id="Seg_7985" s="T716">bar</ta>
            <ta e="T718" id="Seg_7986" s="T717">tʼor-laʔbə</ta>
            <ta e="T719" id="Seg_7987" s="T718">a</ta>
            <ta e="T720" id="Seg_7988" s="T719">pʼetux</ta>
            <ta e="T721" id="Seg_7989" s="T720">măn-liA</ta>
            <ta e="T722" id="Seg_7990" s="T721">ĭmbi</ta>
            <ta e="T723" id="Seg_7991" s="T722">tʼor-laʔbə-l</ta>
            <ta e="T724" id="Seg_7992" s="T723">kan-žə-bəj</ta>
            <ta e="T725" id="Seg_7993" s="T724">tüjö</ta>
            <ta e="T726" id="Seg_7994" s="T725">i-bəj</ta>
            <ta e="T727" id="Seg_7995" s="T726">tʼermən-də</ta>
            <ta e="T728" id="Seg_7996" s="T727">dĭ</ta>
            <ta e="T729" id="Seg_7997" s="T728">lattɨ</ta>
            <ta e="T730" id="Seg_7998" s="T729">kan-bi-jəʔ</ta>
            <ta e="T731" id="Seg_7999" s="T730">pʼetux</ta>
            <ta e="T732" id="Seg_8000" s="T731">kirgaːr-laʔbə</ta>
            <ta e="T733" id="Seg_8001" s="T732">det-ʔ</ta>
            <ta e="T734" id="Seg_8002" s="T733">tʼermən</ta>
            <ta e="T735" id="Seg_8003" s="T734">det-ʔ</ta>
            <ta e="T736" id="Seg_8004" s="T735">tʼermən</ta>
            <ta e="T738" id="Seg_8005" s="T737">dĭ</ta>
            <ta e="T739" id="Seg_8006" s="T738">kuza</ta>
            <ta e="T740" id="Seg_8007" s="T739">măn-ntə</ta>
            <ta e="T741" id="Seg_8008" s="T740">barəʔ-t</ta>
            <ta e="T742" id="Seg_8009" s="T741">dĭ-m</ta>
            <ta e="T743" id="Seg_8010" s="T742">ine-ziʔ</ta>
            <ta e="T744" id="Seg_8011" s="T743">ine-jəʔ-Tə</ta>
            <ta e="T745" id="Seg_8012" s="T744">barəʔ-luʔbdə-bi</ta>
            <ta e="T746" id="Seg_8013" s="T745">dĭ</ta>
            <ta e="T747" id="Seg_8014" s="T746">dĭʔə</ta>
            <ta e="T748" id="Seg_8015" s="T747">šo-bi</ta>
            <ta e="T749" id="Seg_8016" s="T748">bazoʔ</ta>
            <ta e="T750" id="Seg_8017" s="T749">kirgaːr-laʔbə</ta>
            <ta e="T752" id="Seg_8018" s="T751">det-t</ta>
            <ta e="T753" id="Seg_8019" s="T752">tʼermən-də</ta>
            <ta e="T754" id="Seg_8020" s="T753">det-t</ta>
            <ta e="T755" id="Seg_8021" s="T754">tʼermən-də</ta>
            <ta e="T756" id="Seg_8022" s="T755">dĭgəttə</ta>
            <ta e="T757" id="Seg_8023" s="T756">barəʔ-t</ta>
            <ta e="T759" id="Seg_8024" s="T758">dĭ-m</ta>
            <ta e="T760" id="Seg_8025" s="T759">tüžöj-Tə</ta>
            <ta e="T761" id="Seg_8026" s="T760">barəʔ-luʔbdə-bi</ta>
            <ta e="T762" id="Seg_8027" s="T761">tüžöj-Tə</ta>
            <ta e="T763" id="Seg_8028" s="T762">dĭ</ta>
            <ta e="T764" id="Seg_8029" s="T763">tüžöj-Kən</ta>
            <ta e="T766" id="Seg_8030" s="T765">bazoʔ</ta>
            <ta e="T767" id="Seg_8031" s="T766">šo-bi</ta>
            <ta e="T768" id="Seg_8032" s="T767">dĭgəttə</ta>
            <ta e="T769" id="Seg_8033" s="T768">bazoʔ</ta>
            <ta e="T770" id="Seg_8034" s="T769">măn-ntə</ta>
            <ta e="T773" id="Seg_8035" s="T772">det-t</ta>
            <ta e="T774" id="Seg_8036" s="T773">tʼermən-də</ta>
            <ta e="T775" id="Seg_8037" s="T774">det-t</ta>
            <ta e="T777" id="Seg_8038" s="T775">tʼermən-də</ta>
            <ta e="T778" id="Seg_8039" s="T777">barəʔ-t</ta>
            <ta e="T779" id="Seg_8040" s="T778">ular-Tə</ta>
            <ta e="T780" id="Seg_8041" s="T779">barəʔ-luʔbdə-bi</ta>
            <ta e="T781" id="Seg_8042" s="T780">ular-Tə</ta>
            <ta e="T782" id="Seg_8043" s="T781">dĭgəttə</ta>
            <ta e="T783" id="Seg_8044" s="T782">bazoʔ</ta>
            <ta e="T784" id="Seg_8045" s="T783">šo-bi</ta>
            <ta e="T785" id="Seg_8046" s="T784">bazoʔ</ta>
            <ta e="T786" id="Seg_8047" s="T785">kirgaːr-laʔbə</ta>
            <ta e="T787" id="Seg_8048" s="T786">det-t</ta>
            <ta e="T788" id="Seg_8049" s="T787">tʼermən-də</ta>
            <ta e="T789" id="Seg_8050" s="T788">det-t</ta>
            <ta e="T790" id="Seg_8051" s="T789">tʼermən-də</ta>
            <ta e="T791" id="Seg_8052" s="T790">dĭgəttə</ta>
            <ta e="T792" id="Seg_8053" s="T791">măn-ntə</ta>
            <ta e="T793" id="Seg_8054" s="T792">barəʔ-t</ta>
            <ta e="T794" id="Seg_8055" s="T793">dĭ-m</ta>
            <ta e="T795" id="Seg_8056" s="T794">bü-Tə</ta>
            <ta e="T796" id="Seg_8057" s="T795">dĭ-m</ta>
            <ta e="T797" id="Seg_8058" s="T796">barəʔ-luʔbdə-bi</ta>
            <ta e="T798" id="Seg_8059" s="T797">a</ta>
            <ta e="T799" id="Seg_8060" s="T798">dĭ</ta>
            <ta e="T800" id="Seg_8061" s="T799">măn-ntə</ta>
            <ta e="T802" id="Seg_8062" s="T800">köten-m</ta>
            <ta e="T804" id="Seg_8063" s="T803">barəʔ-luʔbdə-bi</ta>
            <ta e="T805" id="Seg_8064" s="T804">dĭ</ta>
            <ta e="T806" id="Seg_8065" s="T805">dĭ-m</ta>
            <ta e="T807" id="Seg_8066" s="T806">bü-Tə</ta>
            <ta e="T808" id="Seg_8067" s="T807">a</ta>
            <ta e="T809" id="Seg_8068" s="T808">dĭ</ta>
            <ta e="T810" id="Seg_8069" s="T809">kirgaːr-laʔbə</ta>
            <ta e="T811" id="Seg_8070" s="T810">köten-m</ta>
            <ta e="T812" id="Seg_8071" s="T811">bĭs-ə-ʔ</ta>
            <ta e="T813" id="Seg_8072" s="T812">bü</ta>
            <ta e="T814" id="Seg_8073" s="T813">köten-m</ta>
            <ta e="T815" id="Seg_8074" s="T814">bĭs-ə-ʔ</ta>
            <ta e="T816" id="Seg_8075" s="T815">bü</ta>
            <ta e="T817" id="Seg_8076" s="T816">köten-m</ta>
            <ta e="T818" id="Seg_8077" s="T817">bar</ta>
            <ta e="T819" id="Seg_8078" s="T818">bü</ta>
            <ta e="T820" id="Seg_8079" s="T819">bĭs-luʔbdə-bi</ta>
            <ta e="T821" id="Seg_8080" s="T820">bazoʔ</ta>
            <ta e="T822" id="Seg_8081" s="T821">šo-bi</ta>
            <ta e="T823" id="Seg_8082" s="T822">det-t</ta>
            <ta e="T824" id="Seg_8083" s="T823">tʼermən-də</ta>
            <ta e="T825" id="Seg_8084" s="T824">det-t</ta>
            <ta e="T826" id="Seg_8085" s="T825">tʼermən-də</ta>
            <ta e="T827" id="Seg_8086" s="T826">dĭgəttə</ta>
            <ta e="T828" id="Seg_8087" s="T827">dĭ</ta>
            <ta e="T829" id="Seg_8088" s="T828">kuza</ta>
            <ta e="T830" id="Seg_8089" s="T829">măn-ntə</ta>
            <ta e="T831" id="Seg_8090" s="T830">šü</ta>
            <ta e="T833" id="Seg_8091" s="T832">hen-KAʔ</ta>
            <ta e="T834" id="Seg_8092" s="T833">urgo</ta>
            <ta e="T835" id="Seg_8093" s="T834">i</ta>
            <ta e="T836" id="Seg_8094" s="T835">barəʔ-t</ta>
            <ta e="T837" id="Seg_8095" s="T836">dĭ-m</ta>
            <ta e="T838" id="Seg_8096" s="T837">šü-Tə</ta>
            <ta e="T839" id="Seg_8097" s="T838">dĭgəttə</ta>
            <ta e="T840" id="Seg_8098" s="T839">hen-bi-jəʔ</ta>
            <ta e="T841" id="Seg_8099" s="T840">šü</ta>
            <ta e="T842" id="Seg_8100" s="T841">barəʔ-luʔbdə-bi</ta>
            <ta e="T843" id="Seg_8101" s="T842">dĭ</ta>
            <ta e="T844" id="Seg_8102" s="T843">pʼetux-m</ta>
            <ta e="T845" id="Seg_8103" s="T844">šü-Tə</ta>
            <ta e="T846" id="Seg_8104" s="T845">dĭbər</ta>
            <ta e="T847" id="Seg_8105" s="T846">măn-ntə</ta>
            <ta e="T848" id="Seg_8106" s="T847">kămnə-ʔ</ta>
            <ta e="T849" id="Seg_8107" s="T848">köten-m</ta>
            <ta e="T850" id="Seg_8108" s="T849">bü</ta>
            <ta e="T851" id="Seg_8109" s="T850">kămnə-ʔ</ta>
            <ta e="T852" id="Seg_8110" s="T851">köten-m</ta>
            <ta e="T853" id="Seg_8111" s="T852">bü</ta>
            <ta e="T854" id="Seg_8112" s="T853">kak</ta>
            <ta e="T855" id="Seg_8113" s="T854">bü</ta>
            <ta e="T856" id="Seg_8114" s="T855">kan-bi</ta>
            <ta e="T857" id="Seg_8115" s="T856">bar</ta>
            <ta e="T858" id="Seg_8116" s="T857">ĭmbi</ta>
            <ta e="T860" id="Seg_8117" s="T858">bar</ta>
            <ta e="T862" id="Seg_8118" s="T861">dĭgəttə</ta>
            <ta e="T863" id="Seg_8119" s="T862">bü</ta>
            <ta e="T864" id="Seg_8120" s="T863">ugaːndə</ta>
            <ta e="T865" id="Seg_8121" s="T864">iʔgö</ta>
            <ta e="T867" id="Seg_8122" s="T866">mʼaŋ-bi</ta>
            <ta e="T868" id="Seg_8123" s="T867">dĭ</ta>
            <ta e="T869" id="Seg_8124" s="T868">kuza</ta>
            <ta e="T870" id="Seg_8125" s="T869">măn-ntə</ta>
            <ta e="T871" id="Seg_8126" s="T870">i-ʔ</ta>
            <ta e="T872" id="Seg_8127" s="T871">tʼermən-də</ta>
            <ta e="T873" id="Seg_8128" s="T872">i</ta>
            <ta e="T874" id="Seg_8129" s="T873">kan-ə-ʔ</ta>
            <ta e="T875" id="Seg_8130" s="T874">döʔə</ta>
            <ta e="T876" id="Seg_8131" s="T875">döʔə</ta>
            <ta e="T877" id="Seg_8132" s="T876">döʔnə</ta>
            <ta e="T881" id="Seg_8133" s="T880">xisə-jəʔ</ta>
            <ta e="T882" id="Seg_8134" s="T881">onʼiʔ</ta>
            <ta e="T884" id="Seg_8135" s="T883">nu-bi-jəʔ</ta>
            <ta e="T885" id="Seg_8136" s="T884">măn</ta>
            <ta e="T886" id="Seg_8137" s="T885">dĭ-zAŋ</ta>
            <ta e="T889" id="Seg_8138" s="T888">öʔlu-bi-m</ta>
            <ta e="T890" id="Seg_8139" s="T889">dĭgəttə</ta>
            <ta e="T891" id="Seg_8140" s="T890">dĭ-zAŋ</ta>
            <ta e="T892" id="Seg_8141" s="T891">nuʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T893" id="Seg_8142" s="T892">a</ta>
            <ta e="T894" id="Seg_8143" s="T893">dĭ-zAŋ</ta>
            <ta e="T895" id="Seg_8144" s="T894">ej</ta>
            <ta e="T896" id="Seg_8145" s="T895">nuʔmə-liA-jəʔ</ta>
            <ta e="T897" id="Seg_8146" s="T896">ɨrɨː</ta>
            <ta e="T898" id="Seg_8147" s="T897">mo-laːm-bi-jəʔ</ta>
            <ta e="T900" id="Seg_8148" s="T899">taldʼen</ta>
            <ta e="T901" id="Seg_8149" s="T900">măn</ta>
            <ta e="T902" id="Seg_8150" s="T901">mej-m</ta>
            <ta e="T903" id="Seg_8151" s="T902">ej</ta>
            <ta e="T1025" id="Seg_8152" s="T903">kan-lAʔ</ta>
            <ta e="T904" id="Seg_8153" s="T1025">tʼür-bi</ta>
            <ta e="T905" id="Seg_8154" s="T904">maʔ-gəndə</ta>
            <ta e="T906" id="Seg_8155" s="T905">măn</ta>
            <ta e="T907" id="Seg_8156" s="T906">mĭn-bi-m</ta>
            <ta e="T908" id="Seg_8157" s="T907">i</ta>
            <ta e="T909" id="Seg_8158" s="T908">dĭ-ziʔ</ta>
            <ta e="T910" id="Seg_8159" s="T909">ej</ta>
            <ta e="T911" id="Seg_8160" s="T910">panar-bi-m</ta>
            <ta e="T912" id="Seg_8161" s="T911">uda-m</ta>
            <ta e="T913" id="Seg_8162" s="T912">ej</ta>
            <ta e="T914" id="Seg_8163" s="T913">mĭ-bi-m</ta>
            <ta e="T915" id="Seg_8164" s="T914">dĭgəttə</ta>
            <ta e="T916" id="Seg_8165" s="T915">dĭ</ta>
            <ta e="T917" id="Seg_8166" s="T916">bazoʔ</ta>
            <ta e="T918" id="Seg_8167" s="T917">par-luʔbdə-bi</ta>
            <ta e="T919" id="Seg_8168" s="T918">ugaːndə</ta>
            <ta e="T920" id="Seg_8169" s="T919">beržə</ta>
            <ta e="T921" id="Seg_8170" s="T920">sĭri</ta>
            <ta e="T922" id="Seg_8171" s="T921">bar</ta>
            <ta e="T923" id="Seg_8172" s="T922">kun-də-laʔbə</ta>
            <ta e="T924" id="Seg_8173" s="T923">mašina-jəʔ</ta>
            <ta e="T925" id="Seg_8174" s="T924">ej</ta>
            <ta e="T926" id="Seg_8175" s="T925">kan-bi-jəʔ</ta>
            <ta e="T927" id="Seg_8176" s="T926">dĭ</ta>
            <ta e="T928" id="Seg_8177" s="T927">par-luʔbdə-bi</ta>
            <ta e="T929" id="Seg_8178" s="T928">dön</ta>
            <ta e="T930" id="Seg_8179" s="T929">šaː-bi</ta>
            <ta e="T931" id="Seg_8180" s="T930">a</ta>
            <ta e="T932" id="Seg_8181" s="T931">teinen</ta>
            <ta e="T933" id="Seg_8182" s="T932">măn</ta>
            <ta e="T934" id="Seg_8183" s="T933">uʔbdə-bi-m</ta>
            <ta e="T935" id="Seg_8184" s="T934">mĭnzər-bi-m</ta>
            <ta e="T936" id="Seg_8185" s="T935">bar</ta>
            <ta e="T937" id="Seg_8186" s="T936">ipek</ta>
            <ta e="T938" id="Seg_8187" s="T937">pür-bi-m</ta>
            <ta e="T939" id="Seg_8188" s="T938">bos-gəndə</ta>
            <ta e="T940" id="Seg_8189" s="T939">aʔtʼi-Tə</ta>
            <ta e="T941" id="Seg_8190" s="T940">amor-bi</ta>
            <ta e="T942" id="Seg_8191" s="T941">dĭgəttə</ta>
            <ta e="T1026" id="Seg_8192" s="T942">kan-lAʔ</ta>
            <ta e="T943" id="Seg_8193" s="T1026">tʼür-bi</ta>
            <ta e="T944" id="Seg_8194" s="T943">mašina-ziʔ</ta>
            <ta e="T945" id="Seg_8195" s="T944">predsʼedatʼelʼ-ziʔ</ta>
            <ta e="T947" id="Seg_8196" s="T946">dĭgəttə</ta>
            <ta e="T948" id="Seg_8197" s="T947">kan-lV-m</ta>
            <ta e="T949" id="Seg_8198" s="T948">maʔ-gənʼi</ta>
            <ta e="T950" id="Seg_8199" s="T949">dĭgəttə</ta>
            <ta e="T951" id="Seg_8200" s="T950">šo-bi-m</ta>
            <ta e="T952" id="Seg_8201" s="T951">kan-lV-m</ta>
            <ta e="T953" id="Seg_8202" s="T952">maʔ-gənʼi</ta>
            <ta e="T954" id="Seg_8203" s="T953">ato</ta>
            <ta e="T955" id="Seg_8204" s="T954">măn</ta>
            <ta e="T956" id="Seg_8205" s="T955">dĭn</ta>
            <ta e="T957" id="Seg_8206" s="T956">il</ta>
            <ta e="T958" id="Seg_8207" s="T957">amno-laʔbə</ta>
            <ta e="T959" id="Seg_8208" s="T958">măna</ta>
            <ta e="T960" id="Seg_8209" s="T959">edəʔ-laʔbə-jəʔ</ta>
            <ta e="T961" id="Seg_8210" s="T960">dĭgəttə</ta>
            <ta e="T962" id="Seg_8211" s="T961">šo-bi-m</ta>
            <ta e="T963" id="Seg_8212" s="T962">maʔ-gənʼi</ta>
            <ta e="T964" id="Seg_8213" s="T963">dʼaparat</ta>
            <ta e="T965" id="Seg_8214" s="T964">bazə-bi-m</ta>
            <ta e="T966" id="Seg_8215" s="T965">büzəj</ta>
            <ta e="T967" id="Seg_8216" s="T966">bĭs-lə-bi-m</ta>
            <ta e="T968" id="Seg_8217" s="T967">dĭgəttə</ta>
            <ta e="T969" id="Seg_8218" s="T968">tüžöj-m</ta>
            <ta e="T970" id="Seg_8219" s="T969">sürer-bi-m</ta>
            <ta e="T971" id="Seg_8220" s="T970">bü-Tə</ta>
            <ta e="T972" id="Seg_8221" s="T971">bĭs-lə-bi-m</ta>
            <ta e="T973" id="Seg_8222" s="T972">dĭgəttə</ta>
            <ta e="T975" id="Seg_8223" s="T974">šonə-gA</ta>
            <ta e="T977" id="Seg_8224" s="T976">koʔbdo</ta>
            <ta e="T978" id="Seg_8225" s="T977">šonə-gA</ta>
            <ta e="T979" id="Seg_8226" s="T978">măna</ta>
            <ta e="T980" id="Seg_8227" s="T979">davaj</ta>
            <ta e="T981" id="Seg_8228" s="T980">kĭr-zittə</ta>
            <ta e="T982" id="Seg_8229" s="T981">măn</ta>
            <ta e="T983" id="Seg_8230" s="T982">nu-bi-m</ta>
            <ta e="T984" id="Seg_8231" s="T983">dĭ</ta>
            <ta e="T985" id="Seg_8232" s="T984">kĭr-bi</ta>
            <ta e="T986" id="Seg_8233" s="T985">măna</ta>
            <ta e="T987" id="Seg_8234" s="T986">dĭgəttə</ta>
            <ta e="T990" id="Seg_8235" s="T989">šo-bi-bAʔ</ta>
            <ta e="T991" id="Seg_8236" s="T990">tura-Tə</ta>
            <ta e="T992" id="Seg_8237" s="T991">tʼăbaktər-zittə</ta>
            <ta e="T994" id="Seg_8238" s="T993">dĭgəttə</ta>
            <ta e="T995" id="Seg_8239" s="T994">tʼăbaktər-zittə</ta>
            <ta e="T996" id="Seg_8240" s="T995">plʼonka-Tə</ta>
            <ta e="T998" id="Seg_8241" s="T997">kuriza-jəʔ</ta>
            <ta e="T999" id="Seg_8242" s="T998">bădə-bi-m</ta>
            <ta e="T1000" id="Seg_8243" s="T999">tüžöj-n</ta>
            <ta e="T1001" id="Seg_8244" s="T1000">tüʔ</ta>
            <ta e="T1002" id="Seg_8245" s="T1001">barəʔ-bi-m</ta>
            <ta e="T1004" id="Seg_8246" s="T1003">kabarləj</ta>
            <ta e="T1005" id="Seg_8247" s="T1004">tʼăbaktər-zittə</ta>
            <ta e="T1006" id="Seg_8248" s="T1005">ato</ta>
            <ta e="T1007" id="Seg_8249" s="T1006">ešši-zAŋ</ta>
            <ta e="T1008" id="Seg_8250" s="T1007">büžü</ta>
            <ta e="T1009" id="Seg_8251" s="T1008">šo-lV-jəʔ</ta>
            <ta e="T1010" id="Seg_8252" s="T1009">škola-gəʔ</ta>
            <ta e="T1011" id="Seg_8253" s="T1010">ej</ta>
            <ta e="T1012" id="Seg_8254" s="T1011">mĭ-lV-jəʔ</ta>
            <ta e="T1013" id="Seg_8255" s="T1012">tʼăbaktər-zittə</ta>
            <ta e="T1014" id="Seg_8256" s="T1013">bar</ta>
            <ta e="T1015" id="Seg_8257" s="T1014">kirgaːr-lV-jəʔ</ta>
         </annotation>
         <annotation name="ge" tierref="ge-PKZ">
            <ta e="T1" id="Seg_8258" s="T0">I.NOM</ta>
            <ta e="T2" id="Seg_8259" s="T1">live-PST-1SG</ta>
            <ta e="T3" id="Seg_8260" s="T2">five.[NOM.SG]</ta>
            <ta e="T4" id="Seg_8261" s="T3">man-PL</ta>
            <ta e="T5" id="Seg_8262" s="T4">be-PST-3PL</ta>
            <ta e="T6" id="Seg_8263" s="T5">I.NOM</ta>
            <ta e="T7" id="Seg_8264" s="T6">then</ta>
            <ta e="T8" id="Seg_8265" s="T7">six.[NOM.SG]</ta>
            <ta e="T9" id="Seg_8266" s="T8">take-PST-1SG</ta>
            <ta e="T10" id="Seg_8267" s="T9">this</ta>
            <ta e="T11" id="Seg_8268" s="T10">six.[NOM.SG]</ta>
            <ta e="T12" id="Seg_8269" s="T11">man.[NOM.SG]</ta>
            <ta e="T13" id="Seg_8270" s="T12">NEG</ta>
            <ta e="T14" id="Seg_8271" s="T13">live-PST.[3SG]</ta>
            <ta e="T15" id="Seg_8272" s="T14">throw.away-PST.[3SG]</ta>
            <ta e="T16" id="Seg_8273" s="T15">and</ta>
            <ta e="T776" id="Seg_8274" s="T16">go-CVB</ta>
            <ta e="T17" id="Seg_8275" s="T776">disappear-PST.[3SG]</ta>
            <ta e="T18" id="Seg_8276" s="T17">then</ta>
            <ta e="T21" id="Seg_8277" s="T20">man-NOM/GEN/ACC.1SG</ta>
            <ta e="T22" id="Seg_8278" s="T21">water-LOC</ta>
            <ta e="T23" id="Seg_8279" s="T22">die-RES-PST.[3SG]</ta>
            <ta e="T24" id="Seg_8280" s="T23">boy-NOM/GEN/ACC.1SG</ta>
            <ta e="T25" id="Seg_8281" s="T24">water-LOC</ta>
            <ta e="T26" id="Seg_8282" s="T25">die-RES-PST.[3SG]</ta>
            <ta e="T27" id="Seg_8283" s="T26">then</ta>
            <ta e="T28" id="Seg_8284" s="T27">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T29" id="Seg_8285" s="T28">kill-PST-3PL</ta>
            <ta e="T30" id="Seg_8286" s="T29">sister-NOM/GEN/ACC.1SG</ta>
            <ta e="T31" id="Seg_8287" s="T30">kill-PST-3PL</ta>
            <ta e="T32" id="Seg_8288" s="T31">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T33" id="Seg_8289" s="T32">forest-LAT</ta>
            <ta e="T34" id="Seg_8290" s="T33">go-PST.[3SG]</ta>
            <ta e="T35" id="Seg_8291" s="T34">and</ta>
            <ta e="T36" id="Seg_8292" s="T35">there</ta>
            <ta e="T37" id="Seg_8293" s="T36">die-RES-PST.[3SG]</ta>
            <ta e="T38" id="Seg_8294" s="T37">I.GEN</ta>
            <ta e="T39" id="Seg_8295" s="T38">sister-NOM/GEN/ACC.1SG</ta>
            <ta e="T40" id="Seg_8296" s="T39">man-NOM/GEN.3SG</ta>
            <ta e="T41" id="Seg_8297" s="T40">kill-PST.[3SG]</ta>
            <ta e="T42" id="Seg_8298" s="T41">two.[NOM.SG]</ta>
            <ta e="T44" id="Seg_8299" s="T43">boy.[NOM.SG]</ta>
            <ta e="T45" id="Seg_8300" s="T44">remain-PST.[3SG]</ta>
            <ta e="T46" id="Seg_8301" s="T45">I.NOM</ta>
            <ta e="T47" id="Seg_8302" s="T46">this-ACC.PL</ta>
            <ta e="T48" id="Seg_8303" s="T47">PTCL</ta>
            <ta e="T49" id="Seg_8304" s="T48">grow-PST-1SG</ta>
            <ta e="T50" id="Seg_8305" s="T49">clothing.[NOM.SG]</ta>
            <ta e="T51" id="Seg_8306" s="T50">take-PST-1SG</ta>
            <ta e="T52" id="Seg_8307" s="T51">and</ta>
            <ta e="T53" id="Seg_8308" s="T52">learn-TR-PST-1SG</ta>
            <ta e="T55" id="Seg_8309" s="T54">paper.[NOM.SG]</ta>
            <ta e="T56" id="Seg_8310" s="T55">one.[NOM.SG]</ta>
            <ta e="T60" id="Seg_8311" s="T59">one.[NOM.SG]</ta>
            <ta e="T61" id="Seg_8312" s="T60">I.LAT</ta>
            <ta e="T62" id="Seg_8313" s="T61">live-PST.[3SG]</ta>
            <ta e="T63" id="Seg_8314" s="T62">then</ta>
            <ta e="T64" id="Seg_8315" s="T63">this-ACC</ta>
            <ta e="T65" id="Seg_8316" s="T64">take-PST-3PL</ta>
            <ta e="T66" id="Seg_8317" s="T65">war-LAT</ta>
            <ta e="T67" id="Seg_8318" s="T66">there</ta>
            <ta e="T68" id="Seg_8319" s="T67">kill-PST-3PL</ta>
            <ta e="T69" id="Seg_8320" s="T68">this-GEN</ta>
            <ta e="T70" id="Seg_8321" s="T69">girl.[NOM.SG]</ta>
            <ta e="T71" id="Seg_8322" s="T70">remain-MOM-PST.[3SG]</ta>
            <ta e="T73" id="Seg_8323" s="T72">I.NOM</ta>
            <ta e="T75" id="Seg_8324" s="T74">stove-NOM/GEN/ACC.3SG</ta>
            <ta e="T77" id="Seg_8325" s="T76">freeze-RES-PST-1SG</ta>
            <ta e="T78" id="Seg_8326" s="T77">stove-LAT</ta>
            <ta e="T82" id="Seg_8327" s="T81">climb-PST-1SG</ta>
            <ta e="T83" id="Seg_8328" s="T82">sit-PST-1SG</ta>
            <ta e="T84" id="Seg_8329" s="T83">lie-PST-1SG</ta>
            <ta e="T85" id="Seg_8330" s="T84">sleep-PST-1SG</ta>
            <ta e="T86" id="Seg_8331" s="T85">pine.nut.[NOM.SG]</ta>
            <ta e="T88" id="Seg_8332" s="T87">crack-FUT-1SG</ta>
            <ta e="T89" id="Seg_8333" s="T88">then</ta>
            <ta e="T90" id="Seg_8334" s="T89">you.PL.NOM</ta>
            <ta e="T91" id="Seg_8335" s="T90">come-PST-3PL</ta>
            <ta e="T92" id="Seg_8336" s="T91">I.NOM</ta>
            <ta e="T93" id="Seg_8337" s="T92">come-PST-1SG</ta>
            <ta e="T94" id="Seg_8338" s="T93">you.PL.ACC</ta>
            <ta e="T95" id="Seg_8339" s="T94">and</ta>
            <ta e="T96" id="Seg_8340" s="T95">PTCL</ta>
            <ta e="T97" id="Seg_8341" s="T96">speak-DUR-1SG</ta>
            <ta e="T99" id="Seg_8342" s="T98">three.[NOM.SG]</ta>
            <ta e="T100" id="Seg_8343" s="T99">brother.[NOM.SG]</ta>
            <ta e="T101" id="Seg_8344" s="T100">live-PST-3PL</ta>
            <ta e="T102" id="Seg_8345" s="T101">and</ta>
            <ta e="T103" id="Seg_8346" s="T102">then</ta>
            <ta e="T104" id="Seg_8347" s="T103">go-PST-3PL</ta>
            <ta e="T105" id="Seg_8348" s="T104">tree.[NOM.SG]</ta>
            <ta e="T106" id="Seg_8349" s="T105">cut-INF.LAT</ta>
            <ta e="T107" id="Seg_8350" s="T106">this-PL-LOC</ta>
            <ta e="T108" id="Seg_8351" s="T107">fire.[NOM.SG]</ta>
            <ta e="T109" id="Seg_8352" s="T108">NEG.EX-PST.[3SG]</ta>
            <ta e="T111" id="Seg_8353" s="T110">find-PST-3PL</ta>
            <ta e="T112" id="Seg_8354" s="T111">fire.[NOM.SG]</ta>
            <ta e="T113" id="Seg_8355" s="T112">burn-DUR.[3SG]</ta>
            <ta e="T114" id="Seg_8356" s="T113">go-EP-IMP.2SG</ta>
            <ta e="T115" id="Seg_8357" s="T114">take-IMP.2SG</ta>
            <ta e="T116" id="Seg_8358" s="T115">fire.[NOM.SG]</ta>
            <ta e="T117" id="Seg_8359" s="T116">this.[NOM.SG]</ta>
            <ta e="T118" id="Seg_8360" s="T117">go-PST.[3SG]</ta>
            <ta e="T119" id="Seg_8361" s="T118">there</ta>
            <ta e="T120" id="Seg_8362" s="T119">man.[NOM.SG]</ta>
            <ta e="T121" id="Seg_8363" s="T120">sit-DUR.[3SG]</ta>
            <ta e="T123" id="Seg_8364" s="T122">tell-IMP.2SG</ta>
            <ta e="T125" id="Seg_8365" s="T124">then</ta>
            <ta e="T126" id="Seg_8366" s="T125">fire.[NOM.SG]</ta>
            <ta e="T127" id="Seg_8367" s="T126">give-FUT-1SG</ta>
            <ta e="T128" id="Seg_8368" s="T127">and</ta>
            <ta e="T129" id="Seg_8369" s="T128">this.[NOM.SG]</ta>
            <ta e="T130" id="Seg_8370" s="T129">say-IPFVZ.[3SG]</ta>
            <ta e="T131" id="Seg_8371" s="T130">I.NOM</ta>
            <ta e="T132" id="Seg_8372" s="T131">NEG</ta>
            <ta e="T133" id="Seg_8373" s="T132">know-1SG</ta>
            <ta e="T134" id="Seg_8374" s="T133">this.[NOM.SG]</ta>
            <ta e="T135" id="Seg_8375" s="T134">back-NOM/GEN/ACC.3SG</ta>
            <ta e="T140" id="Seg_8376" s="T139">cut-PST.[3SG]</ta>
            <ta e="T142" id="Seg_8377" s="T141">three.[NOM.SG]</ta>
            <ta e="T144" id="Seg_8378" s="T143">cut-PST.[3SG]</ta>
            <ta e="T145" id="Seg_8379" s="T144">this.[NOM.SG]</ta>
            <ta e="T1017" id="Seg_8380" s="T146">go-CVB</ta>
            <ta e="T147" id="Seg_8381" s="T1017">disappear-PST.[3SG]</ta>
            <ta e="T148" id="Seg_8382" s="T147">and</ta>
            <ta e="T149" id="Seg_8383" s="T148">fire.[NOM.SG]</ta>
            <ta e="T150" id="Seg_8384" s="T149">NEG</ta>
            <ta e="T151" id="Seg_8385" s="T150">give-PST.[3SG]</ta>
            <ta e="T153" id="Seg_8386" s="T151">then</ta>
            <ta e="T154" id="Seg_8387" s="T153">then</ta>
            <ta e="T155" id="Seg_8388" s="T154">one.[NOM.SG]</ta>
            <ta e="T156" id="Seg_8389" s="T155">again</ta>
            <ta e="T157" id="Seg_8390" s="T156">go-PST.[3SG]</ta>
            <ta e="T158" id="Seg_8391" s="T157">and</ta>
            <ta e="T159" id="Seg_8392" s="T158">this-LAT</ta>
            <ta e="T160" id="Seg_8393" s="T159">say-PST.[3SG]</ta>
            <ta e="T161" id="Seg_8394" s="T160">tell-IMP.2SG</ta>
            <ta e="T163" id="Seg_8395" s="T162">this.[NOM.SG]</ta>
            <ta e="T164" id="Seg_8396" s="T163">say-IPFVZ.[3SG]</ta>
            <ta e="T165" id="Seg_8397" s="T164">I.NOM</ta>
            <ta e="T166" id="Seg_8398" s="T165">NEG</ta>
            <ta e="T167" id="Seg_8399" s="T166">know-1SG</ta>
            <ta e="T168" id="Seg_8400" s="T167">well</ta>
            <ta e="T169" id="Seg_8401" s="T168">then</ta>
            <ta e="T170" id="Seg_8402" s="T169">there</ta>
            <ta e="T171" id="Seg_8403" s="T170">PTCL</ta>
            <ta e="T172" id="Seg_8404" s="T171">stab-PST.[3SG]</ta>
            <ta e="T173" id="Seg_8405" s="T172">back-NOM/GEN/ACC.3SG</ta>
            <ta e="T174" id="Seg_8406" s="T173">and</ta>
            <ta e="T175" id="Seg_8407" s="T174">go-PST.[3SG]</ta>
            <ta e="T176" id="Seg_8408" s="T175">NEG</ta>
            <ta e="T177" id="Seg_8409" s="T176">give-PST.[3SG]</ta>
            <ta e="T178" id="Seg_8410" s="T177">fire.[NOM.SG]</ta>
            <ta e="T179" id="Seg_8411" s="T178">then</ta>
            <ta e="T180" id="Seg_8412" s="T179">one.[NOM.SG]</ta>
            <ta e="T181" id="Seg_8413" s="T180">again</ta>
            <ta e="T182" id="Seg_8414" s="T181">come-PST.[3SG]</ta>
            <ta e="T183" id="Seg_8415" s="T182">tell-IMP.2SG</ta>
            <ta e="T185" id="Seg_8416" s="T184">well</ta>
            <ta e="T186" id="Seg_8417" s="T185">this.[NOM.SG]</ta>
            <ta e="T187" id="Seg_8418" s="T186">say-IPFVZ.[3SG]</ta>
            <ta e="T188" id="Seg_8419" s="T187">tell-FUT-1SG</ta>
            <ta e="T189" id="Seg_8420" s="T188">ten.[NOM.SG]</ta>
            <ta e="T190" id="Seg_8421" s="T189">be-PST-3PL</ta>
            <ta e="T191" id="Seg_8422" s="T190">brother-PL</ta>
            <ta e="T192" id="Seg_8423" s="T191">then</ta>
            <ta e="T193" id="Seg_8424" s="T192">go-PST-3PL</ta>
            <ta e="T194" id="Seg_8425" s="T193">tree.[NOM.SG]</ta>
            <ta e="T195" id="Seg_8426" s="T194">cut-INF.LAT</ta>
            <ta e="T196" id="Seg_8427" s="T195">then</ta>
            <ta e="T197" id="Seg_8428" s="T196">%%</ta>
            <ta e="T198" id="Seg_8429" s="T197">jump-MOM-PST.[3SG]</ta>
            <ta e="T199" id="Seg_8430" s="T198">this.[NOM.SG]</ta>
            <ta e="T200" id="Seg_8431" s="T199">PTCL</ta>
            <ta e="T201" id="Seg_8432" s="T200">place-LAT</ta>
            <ta e="T1018" id="Seg_8433" s="T201">go-CVB</ta>
            <ta e="T202" id="Seg_8434" s="T1018">disappear-PST.[3SG]</ta>
            <ta e="T203" id="Seg_8435" s="T202">then</ta>
            <ta e="T204" id="Seg_8436" s="T203">duck.[NOM.SG]</ta>
            <ta e="T205" id="Seg_8437" s="T204">come-PST.[3SG]</ta>
            <ta e="T206" id="Seg_8438" s="T205">tent.[NOM.SG]</ta>
            <ta e="T207" id="Seg_8439" s="T206">make-PST.[3SG]</ta>
            <ta e="T208" id="Seg_8440" s="T207">egg-PL</ta>
            <ta e="T209" id="Seg_8441" s="T208">give.birth-PST.[3SG]</ta>
            <ta e="T210" id="Seg_8442" s="T209">then</ta>
            <ta e="T211" id="Seg_8443" s="T210">come-PST.[3SG]</ta>
            <ta e="T212" id="Seg_8444" s="T211">forest-ABL</ta>
            <ta e="T213" id="Seg_8445" s="T212">dog.[NOM.SG]</ta>
            <ta e="T214" id="Seg_8446" s="T213">big.[NOM.SG]</ta>
            <ta e="T215" id="Seg_8447" s="T214">and</ta>
            <ta e="T216" id="Seg_8448" s="T215">INCH</ta>
            <ta e="T217" id="Seg_8449" s="T216">egg-PL</ta>
            <ta e="T218" id="Seg_8450" s="T217">eat-INF.LAT</ta>
            <ta e="T219" id="Seg_8451" s="T218">this.[NOM.SG]</ta>
            <ta e="T221" id="Seg_8452" s="T220">capture-MOM-PST.[3SG]</ta>
            <ta e="T222" id="Seg_8453" s="T221">tail.[NOM.SG]</ta>
            <ta e="T223" id="Seg_8454" s="T222">this.[NOM.SG]</ta>
            <ta e="T224" id="Seg_8455" s="T223">PTCL</ta>
            <ta e="T226" id="Seg_8456" s="T224">this-ACC</ta>
            <ta e="T229" id="Seg_8457" s="T228">then</ta>
            <ta e="T230" id="Seg_8458" s="T229">this.[NOM.SG]</ta>
            <ta e="T231" id="Seg_8459" s="T230">capture-MOM-PST.[3SG]</ta>
            <ta e="T232" id="Seg_8460" s="T231">tail.[NOM.SG]</ta>
            <ta e="T233" id="Seg_8461" s="T232">tail.[NOM.SG]</ta>
            <ta e="T235" id="Seg_8462" s="T234">off_pull-PST.[3SG]</ta>
            <ta e="T236" id="Seg_8463" s="T235">then</ta>
            <ta e="T237" id="Seg_8464" s="T236">knife-INS</ta>
            <ta e="T239" id="Seg_8465" s="T238">cut-PST.[3SG]</ta>
            <ta e="T240" id="Seg_8466" s="T239">there</ta>
            <ta e="T241" id="Seg_8467" s="T240">drawer.[NOM.SG]</ta>
            <ta e="T242" id="Seg_8468" s="T241">see-PST.[3SG]</ta>
            <ta e="T243" id="Seg_8469" s="T242">drawer-LOC</ta>
            <ta e="T244" id="Seg_8470" s="T243">paper.[NOM.SG]</ta>
            <ta e="T245" id="Seg_8471" s="T244">written</ta>
            <ta e="T246" id="Seg_8472" s="T245">I.NOM</ta>
            <ta e="T248" id="Seg_8473" s="T247">I.GEN</ta>
            <ta e="T249" id="Seg_8474" s="T248">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T250" id="Seg_8475" s="T249">you.NOM</ta>
            <ta e="T252" id="Seg_8476" s="T251">father-3SG-COM</ta>
            <ta e="T253" id="Seg_8477" s="T252">you.NOM</ta>
            <ta e="T254" id="Seg_8478" s="T253">father-LAT/LOC.3SG</ta>
            <ta e="T255" id="Seg_8479" s="T254">shit-INF.LAT</ta>
            <ta e="T1019" id="Seg_8480" s="T255">go-CVB</ta>
            <ta e="T256" id="Seg_8481" s="T1019">disappear-PST.[3SG]</ta>
            <ta e="T258" id="Seg_8482" s="T257">then</ta>
            <ta e="T259" id="Seg_8483" s="T258">this</ta>
            <ta e="T260" id="Seg_8484" s="T259">man.[NOM.SG]</ta>
            <ta e="T261" id="Seg_8485" s="T260">say-PST.[3SG]</ta>
            <ta e="T262" id="Seg_8486" s="T261">NEG.AUX-IMP.2SG</ta>
            <ta e="T263" id="Seg_8487" s="T262">lie-CNG</ta>
            <ta e="T264" id="Seg_8488" s="T263">and</ta>
            <ta e="T265" id="Seg_8489" s="T264">this.[NOM.SG]</ta>
            <ta e="T267" id="Seg_8490" s="T266">this.[NOM.SG]</ta>
            <ta e="T269" id="Seg_8491" s="T268">man.[NOM.SG]</ta>
            <ta e="T270" id="Seg_8492" s="T269">this-LAT</ta>
            <ta e="T271" id="Seg_8493" s="T270">back-NOM/GEN/ACC.3SG</ta>
            <ta e="T273" id="Seg_8494" s="T272">cut-PST.[3SG]</ta>
            <ta e="T274" id="Seg_8495" s="T273">and</ta>
            <ta e="T275" id="Seg_8496" s="T274">fire.[NOM.SG]</ta>
            <ta e="T276" id="Seg_8497" s="T275">be-PST.[3SG]</ta>
            <ta e="T277" id="Seg_8498" s="T276">and</ta>
            <ta e="T1020" id="Seg_8499" s="T277">go-CVB</ta>
            <ta e="T278" id="Seg_8500" s="T1020">disappear-PST.[3SG]</ta>
            <ta e="T280" id="Seg_8501" s="T279">I.GEN</ta>
            <ta e="T281" id="Seg_8502" s="T280">daughter_in_law-NOM/GEN/ACC.1SG</ta>
            <ta e="T282" id="Seg_8503" s="T281">I.GEN</ta>
            <ta e="T283" id="Seg_8504" s="T282">man.[NOM.SG]</ta>
            <ta e="T286" id="Seg_8505" s="T285">take-CVB-PST.[3SG]</ta>
            <ta e="T287" id="Seg_8506" s="T286">and</ta>
            <ta e="T1021" id="Seg_8507" s="T287">go-CVB</ta>
            <ta e="T288" id="Seg_8508" s="T1021">disappear-PST-3PL</ta>
            <ta e="T289" id="Seg_8509" s="T288">we.NOM</ta>
            <ta e="T290" id="Seg_8510" s="T289">good</ta>
            <ta e="T291" id="Seg_8511" s="T290">live-PST-3PL</ta>
            <ta e="T292" id="Seg_8512" s="T291">and</ta>
            <ta e="T293" id="Seg_8513" s="T292">this.[NOM.SG]</ta>
            <ta e="T294" id="Seg_8514" s="T293">I.ACC</ta>
            <ta e="T295" id="Seg_8515" s="T294">pity-PST.[3SG]</ta>
            <ta e="T296" id="Seg_8516" s="T295">hand-LAT</ta>
            <ta e="T297" id="Seg_8517" s="T296">take-PST.[3SG]</ta>
            <ta e="T298" id="Seg_8518" s="T297">and</ta>
            <ta e="T299" id="Seg_8519" s="T298">then</ta>
            <ta e="T1022" id="Seg_8520" s="T299">go-CVB</ta>
            <ta e="T300" id="Seg_8521" s="T1022">disappear-PST.[3SG]</ta>
            <ta e="T301" id="Seg_8522" s="T300">Igarka-LAT</ta>
            <ta e="T302" id="Seg_8523" s="T301">and</ta>
            <ta e="T303" id="Seg_8524" s="T302">there</ta>
            <ta e="T304" id="Seg_8525" s="T303">die-RES-PST.[3SG]</ta>
            <ta e="T306" id="Seg_8526" s="T305">and</ta>
            <ta e="T307" id="Seg_8527" s="T306">this.[NOM.SG]</ta>
            <ta e="T308" id="Seg_8528" s="T307">NEG</ta>
            <ta e="T309" id="Seg_8529" s="T308">beautiful.[NOM.SG]</ta>
            <ta e="T310" id="Seg_8530" s="T309">be-PST.[3SG]</ta>
            <ta e="T311" id="Seg_8531" s="T310">mouth-NOM/GEN/ACC.3SG</ta>
            <ta e="T313" id="Seg_8532" s="T311">PTCL</ta>
            <ta e="T315" id="Seg_8533" s="T314">this.[NOM.SG]</ta>
            <ta e="T316" id="Seg_8534" s="T315">NEG</ta>
            <ta e="T317" id="Seg_8535" s="T316">beautiful.[NOM.SG]</ta>
            <ta e="T318" id="Seg_8536" s="T317">mouth-NOM/GEN/ACC.3SG</ta>
            <ta e="T319" id="Seg_8537" s="T318">wry.[NOM.SG]</ta>
            <ta e="T320" id="Seg_8538" s="T319">eye-NOM/GEN.3SG</ta>
            <ta e="T321" id="Seg_8539" s="T320">wry.[NOM.SG]</ta>
            <ta e="T323" id="Seg_8540" s="T322">two.[NOM.SG]</ta>
            <ta e="T324" id="Seg_8541" s="T323">woman.[NOM.SG]</ta>
            <ta e="T325" id="Seg_8542" s="T324">live-PST-3PL</ta>
            <ta e="T326" id="Seg_8543" s="T325">go-PST-3PL</ta>
            <ta e="T327" id="Seg_8544" s="T326">Lilium.martagon-NOM/GEN/ACC.3PL</ta>
            <ta e="T329" id="Seg_8545" s="T328">dig-INF.LAT</ta>
            <ta e="T330" id="Seg_8546" s="T329">one.[NOM.SG]</ta>
            <ta e="T331" id="Seg_8547" s="T330">dig-PST.[3SG]</ta>
            <ta e="T332" id="Seg_8548" s="T331">many</ta>
            <ta e="T333" id="Seg_8549" s="T332">and</ta>
            <ta e="T334" id="Seg_8550" s="T333">one.[NOM.SG]</ta>
            <ta e="T335" id="Seg_8551" s="T334">few</ta>
            <ta e="T336" id="Seg_8552" s="T335">what.kind</ta>
            <ta e="T337" id="Seg_8553" s="T336">many</ta>
            <ta e="T338" id="Seg_8554" s="T337">dig-PST.[3SG]</ta>
            <ta e="T339" id="Seg_8555" s="T338">and</ta>
            <ta e="T340" id="Seg_8556" s="T339">what.kind</ta>
            <ta e="T341" id="Seg_8557" s="T340">few</ta>
            <ta e="T342" id="Seg_8558" s="T341">dig-PST.[3SG]</ta>
            <ta e="T343" id="Seg_8559" s="T342">this-ACC</ta>
            <ta e="T345" id="Seg_8560" s="T344">axe-INS</ta>
            <ta e="T346" id="Seg_8561" s="T345">head-NOM/GEN.3SG</ta>
            <ta e="T347" id="Seg_8562" s="T346">cut-PST.[3SG]</ta>
            <ta e="T348" id="Seg_8563" s="T347">then</ta>
            <ta e="T349" id="Seg_8564" s="T348">come-PST.[3SG]</ta>
            <ta e="T350" id="Seg_8565" s="T349">house-LAT</ta>
            <ta e="T351" id="Seg_8566" s="T350">girl.[NOM.SG]</ta>
            <ta e="T352" id="Seg_8567" s="T351">come-PST.[3SG]</ta>
            <ta e="T353" id="Seg_8568" s="T352">where</ta>
            <ta e="T354" id="Seg_8569" s="T353">I.GEN</ta>
            <ta e="T355" id="Seg_8570" s="T354">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T356" id="Seg_8571" s="T355">this.[3SG]</ta>
            <ta e="T357" id="Seg_8572" s="T356">PTCL</ta>
            <ta e="T358" id="Seg_8573" s="T357">lazy.[NOM.SG]</ta>
            <ta e="T359" id="Seg_8574" s="T358">NEG</ta>
            <ta e="T360" id="Seg_8575" s="T359">dig-PST.[3SG]</ta>
            <ta e="T361" id="Seg_8576" s="T360">Lilium.martagon-PL</ta>
            <ta e="T362" id="Seg_8577" s="T361">there</ta>
            <ta e="T363" id="Seg_8578" s="T362">PTCL</ta>
            <ta e="T364" id="Seg_8579" s="T363">sleep-DUR.[3SG]</ta>
            <ta e="T365" id="Seg_8580" s="T364">and</ta>
            <ta e="T366" id="Seg_8581" s="T365">daughter-NOM/GEN.3SG</ta>
            <ta e="T367" id="Seg_8582" s="T366">how=INDEF</ta>
            <ta e="T368" id="Seg_8583" s="T367">how</ta>
            <ta e="T372" id="Seg_8584" s="T371">know-PST.[3SG]</ta>
            <ta e="T373" id="Seg_8585" s="T372">and</ta>
            <ta e="T374" id="Seg_8586" s="T373">come-PST.[3SG]</ta>
            <ta e="T375" id="Seg_8587" s="T374">son-NOM/GEN.3SG</ta>
            <ta e="T376" id="Seg_8588" s="T375">be-PST.[3SG]</ta>
            <ta e="T377" id="Seg_8589" s="T376">this.[3SG]</ta>
            <ta e="T378" id="Seg_8590" s="T377">this-ACC</ta>
            <ta e="T379" id="Seg_8591" s="T378">hide-CVB</ta>
            <ta e="T380" id="Seg_8592" s="T379">be-PST.[3SG]</ta>
            <ta e="T382" id="Seg_8593" s="T381">sleep-INF.LAT</ta>
            <ta e="T383" id="Seg_8594" s="T382">put-PST.[3SG]</ta>
            <ta e="T384" id="Seg_8595" s="T383">and</ta>
            <ta e="T385" id="Seg_8596" s="T384">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T386" id="Seg_8597" s="T385">up</ta>
            <ta e="T389" id="Seg_8598" s="T388">climb-PST.[3SG]</ta>
            <ta e="T390" id="Seg_8599" s="T389">house-LAT</ta>
            <ta e="T392" id="Seg_8600" s="T391">take-PST.[3SG]</ta>
            <ta e="T393" id="Seg_8601" s="T392">then</ta>
            <ta e="T395" id="Seg_8602" s="T394">this</ta>
            <ta e="T396" id="Seg_8603" s="T395">woman.[NOM.SG]</ta>
            <ta e="T397" id="Seg_8604" s="T396">come-PST.[3SG]</ta>
            <ta e="T398" id="Seg_8605" s="T397">where</ta>
            <ta e="T399" id="Seg_8606" s="T398">you.PL.NOM</ta>
            <ta e="T401" id="Seg_8607" s="T400">this.[3SG]</ta>
            <ta e="T402" id="Seg_8608" s="T401">say-IPFVZ.[3SG]</ta>
            <ta e="T403" id="Seg_8609" s="T402">I.NOM</ta>
            <ta e="T404" id="Seg_8610" s="T403">here</ta>
            <ta e="T405" id="Seg_8611" s="T404">live-DUR-1SG</ta>
            <ta e="T406" id="Seg_8612" s="T405">this.[3SG]</ta>
            <ta e="T410" id="Seg_8613" s="T409">pour-PST.[3SG]</ta>
            <ta e="T411" id="Seg_8614" s="T410">eye-LAT/LOC.3SG</ta>
            <ta e="T413" id="Seg_8615" s="T412">what.[NOM.SG]=INDEF</ta>
            <ta e="T414" id="Seg_8616" s="T413">NEG</ta>
            <ta e="T415" id="Seg_8617" s="T414">look-PRS-3PL</ta>
            <ta e="T416" id="Seg_8618" s="T415">then</ta>
            <ta e="T417" id="Seg_8619" s="T416">eye-NOM/GEN.3SG</ta>
            <ta e="T418" id="Seg_8620" s="T417">what.[NOM.SG]=INDEF</ta>
            <ta e="T419" id="Seg_8621" s="T418">NEG</ta>
            <ta e="T420" id="Seg_8622" s="T419">can-PRS-3PL</ta>
            <ta e="T421" id="Seg_8623" s="T420">this.[3SG]</ta>
            <ta e="T422" id="Seg_8624" s="T421">jump-MOM-PST.[3SG]</ta>
            <ta e="T423" id="Seg_8625" s="T422">house-LAT</ta>
            <ta e="T424" id="Seg_8626" s="T423">brother-NOM/GEN.3SG</ta>
            <ta e="T425" id="Seg_8627" s="T424">take-PST.[3SG]</ta>
            <ta e="T426" id="Seg_8628" s="T425">and</ta>
            <ta e="T1023" id="Seg_8629" s="T426">go-CVB</ta>
            <ta e="T427" id="Seg_8630" s="T1023">disappear-PST.[3SG]</ta>
            <ta e="T428" id="Seg_8631" s="T427">where</ta>
            <ta e="T429" id="Seg_8632" s="T428">people.[NOM.SG]</ta>
            <ta e="T430" id="Seg_8633" s="T429">many</ta>
            <ta e="T432" id="Seg_8634" s="T431">I.GEN</ta>
            <ta e="T433" id="Seg_8635" s="T432">man.[NOM.SG]</ta>
            <ta e="T1024" id="Seg_8636" s="T433">go-CVB</ta>
            <ta e="T434" id="Seg_8637" s="T1024">disappear-PST.[3SG]</ta>
            <ta e="T435" id="Seg_8638" s="T434">I.NOM</ta>
            <ta e="T436" id="Seg_8639" s="T435">very</ta>
            <ta e="T437" id="Seg_8640" s="T436">strongly</ta>
            <ta e="T438" id="Seg_8641" s="T437">cry-PST-1SG</ta>
            <ta e="T439" id="Seg_8642" s="T438">heart-NOM/GEN/ACC.1SG</ta>
            <ta e="T440" id="Seg_8643" s="T439">PTCL</ta>
            <ta e="T441" id="Seg_8644" s="T440">hurt-PST.[3SG]</ta>
            <ta e="T442" id="Seg_8645" s="T441">and</ta>
            <ta e="T443" id="Seg_8646" s="T442">head-NOM/GEN/ACC.1SG</ta>
            <ta e="T447" id="Seg_8647" s="T446">I.GEN</ta>
            <ta e="T449" id="Seg_8648" s="T448">sister-NOM/GEN/ACC.1SG</ta>
            <ta e="T450" id="Seg_8649" s="T449">boy.[NOM.SG]</ta>
            <ta e="T451" id="Seg_8650" s="T450">son-NOM/GEN.3SG</ta>
            <ta e="T452" id="Seg_8651" s="T451">mill-NOM/GEN/ACC.3SG</ta>
            <ta e="T453" id="Seg_8652" s="T452">go-PST.[3SG]</ta>
            <ta e="T454" id="Seg_8653" s="T453">bread.[NOM.SG]</ta>
            <ta e="T456" id="Seg_8654" s="T455">mill-LAT</ta>
            <ta e="T457" id="Seg_8655" s="T456">give-PST.[3SG]</ta>
            <ta e="T458" id="Seg_8656" s="T457">flour.[NOM.SG]</ta>
            <ta e="T459" id="Seg_8657" s="T458">grind-INF.LAT</ta>
            <ta e="T460" id="Seg_8658" s="T459">then</ta>
            <ta e="T461" id="Seg_8659" s="T460">house-LAT/LOC.3SG</ta>
            <ta e="T462" id="Seg_8660" s="T461">come-PST.[3SG]</ta>
            <ta e="T463" id="Seg_8661" s="T462">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T464" id="Seg_8662" s="T463">woman-LAT</ta>
            <ta e="T465" id="Seg_8663" s="T464">say-PRS.[3SG]</ta>
            <ta e="T466" id="Seg_8664" s="T465">so.that</ta>
            <ta e="T467" id="Seg_8665" s="T466">long.time</ta>
            <ta e="T469" id="Seg_8666" s="T468">grab-FUT-3SG</ta>
            <ta e="T470" id="Seg_8667" s="T469">and</ta>
            <ta e="T471" id="Seg_8668" s="T470">I.NOM</ta>
            <ta e="T472" id="Seg_8669" s="T471">sit-PRS-1SG</ta>
            <ta e="T473" id="Seg_8670" s="T472">and</ta>
            <ta e="T474" id="Seg_8671" s="T473">think-PRS-1SG</ta>
            <ta e="T476" id="Seg_8672" s="T475">that</ta>
            <ta e="T477" id="Seg_8673" s="T476">I.NOM</ta>
            <ta e="T479" id="Seg_8674" s="T478">this-PL-COM</ta>
            <ta e="T480" id="Seg_8675" s="T479">eat-INF.LAT</ta>
            <ta e="T483" id="Seg_8676" s="T482">then</ta>
            <ta e="T484" id="Seg_8677" s="T483">go-PST-3PL</ta>
            <ta e="T485" id="Seg_8678" s="T484">body-NOM/GEN/ACC.3SG</ta>
            <ta e="T486" id="Seg_8679" s="T485">I.NOM</ta>
            <ta e="T487" id="Seg_8680" s="T486">many</ta>
            <ta e="T488" id="Seg_8681" s="T487">eat-PRS-1SG</ta>
            <ta e="T490" id="Seg_8682" s="T489">one.[NOM.SG]</ta>
            <ta e="T491" id="Seg_8683" s="T490">man.[NOM.SG]</ta>
            <ta e="T492" id="Seg_8684" s="T491">marry-INF.LAT</ta>
            <ta e="T494" id="Seg_8685" s="T493">go-PST.[3SG]</ta>
            <ta e="T495" id="Seg_8686" s="T494">beautiful.[NOM.SG]</ta>
            <ta e="T496" id="Seg_8687" s="T495">girl.[NOM.SG]</ta>
            <ta e="T497" id="Seg_8688" s="T496">very</ta>
            <ta e="T498" id="Seg_8689" s="T497">clothing-NOM/GEN.3SG</ta>
            <ta e="T499" id="Seg_8690" s="T498">many</ta>
            <ta e="T500" id="Seg_8691" s="T499">walk-PRS.[3SG]</ta>
            <ta e="T501" id="Seg_8692" s="T500">then</ta>
            <ta e="T502" id="Seg_8693" s="T501">man.[NOM.SG]</ta>
            <ta e="T503" id="Seg_8694" s="T502">this-LAT</ta>
            <ta e="T504" id="Seg_8695" s="T503">say-IPFVZ.[3SG]</ta>
            <ta e="T505" id="Seg_8696" s="T504">where.to</ta>
            <ta e="T506" id="Seg_8697" s="T505">walk-PRS-2SG</ta>
            <ta e="T507" id="Seg_8698" s="T506">marry-INF.LAT</ta>
            <ta e="T508" id="Seg_8699" s="T507">take-IMP.2SG</ta>
            <ta e="T509" id="Seg_8700" s="T508">I.ACC</ta>
            <ta e="T510" id="Seg_8701" s="T509">and</ta>
            <ta e="T511" id="Seg_8702" s="T510">you.NOM</ta>
            <ta e="T512" id="Seg_8703" s="T511">what.[NOM.SG]</ta>
            <ta e="T513" id="Seg_8704" s="T512">make-PRS-2SG</ta>
            <ta e="T514" id="Seg_8705" s="T513">eat-INF.LAT</ta>
            <ta e="T515" id="Seg_8706" s="T514">and</ta>
            <ta e="T516" id="Seg_8707" s="T515">shit-INF.LAT</ta>
            <ta e="T517" id="Seg_8708" s="T516">then</ta>
            <ta e="T518" id="Seg_8709" s="T517">go-PST-3PL</ta>
            <ta e="T519" id="Seg_8710" s="T518">again</ta>
            <ta e="T520" id="Seg_8711" s="T519">again</ta>
            <ta e="T521" id="Seg_8712" s="T520">man.[NOM.SG]</ta>
            <ta e="T522" id="Seg_8713" s="T521">say-IPFVZ.[3SG]</ta>
            <ta e="T523" id="Seg_8714" s="T522">where.to</ta>
            <ta e="T524" id="Seg_8715" s="T523">walk-PRS-2PL</ta>
            <ta e="T525" id="Seg_8716" s="T524">marry-INF.LAT</ta>
            <ta e="T526" id="Seg_8717" s="T525">I.ACC</ta>
            <ta e="T527" id="Seg_8718" s="T526">take-IMP.2SG.O</ta>
            <ta e="T528" id="Seg_8719" s="T527">and</ta>
            <ta e="T529" id="Seg_8720" s="T528">you.NOM</ta>
            <ta e="T530" id="Seg_8721" s="T529">what.[NOM.SG]</ta>
            <ta e="T531" id="Seg_8722" s="T530">make-FUT-2SG</ta>
            <ta e="T532" id="Seg_8723" s="T531">drink-INF.LAT</ta>
            <ta e="T533" id="Seg_8724" s="T532">and</ta>
            <ta e="T534" id="Seg_8725" s="T533">piss-INF.LAT</ta>
            <ta e="T535" id="Seg_8726" s="T534">then</ta>
            <ta e="T536" id="Seg_8727" s="T535">come-PST-3PL</ta>
            <ta e="T537" id="Seg_8728" s="T536">and</ta>
            <ta e="T538" id="Seg_8729" s="T537">INCH</ta>
            <ta e="T539" id="Seg_8730" s="T538">marry-INF.LAT</ta>
            <ta e="T540" id="Seg_8731" s="T539">this.[NOM.SG]</ta>
            <ta e="T541" id="Seg_8732" s="T540">say-IPFVZ.[3SG]</ta>
            <ta e="T542" id="Seg_8733" s="T541">I.NOM</ta>
            <ta e="T543" id="Seg_8734" s="T542">many</ta>
            <ta e="T544" id="Seg_8735" s="T543">PTCL</ta>
            <ta e="T545" id="Seg_8736" s="T544">make-FUT-1SG</ta>
            <ta e="T546" id="Seg_8737" s="T545">bread.[NOM.SG]</ta>
            <ta e="T547" id="Seg_8738" s="T546">bake-FUT-1SG</ta>
            <ta e="T548" id="Seg_8739" s="T547">meat.[NOM.SG]</ta>
            <ta e="T549" id="Seg_8740" s="T548">all</ta>
            <ta e="T550" id="Seg_8741" s="T549">eat-INF.LAT</ta>
            <ta e="T551" id="Seg_8742" s="T550">one.should</ta>
            <ta e="T552" id="Seg_8743" s="T551">this.[3SG]</ta>
            <ta e="T553" id="Seg_8744" s="T552">say-IPFVZ.[3SG]</ta>
            <ta e="T554" id="Seg_8745" s="T553">well</ta>
            <ta e="T555" id="Seg_8746" s="T554">all</ta>
            <ta e="T556" id="Seg_8747" s="T555">I.NOM</ta>
            <ta e="T557" id="Seg_8748" s="T556">eat-FUT-1SG</ta>
            <ta e="T558" id="Seg_8749" s="T557">and</ta>
            <ta e="T561" id="Seg_8750" s="T560">many</ta>
            <ta e="T562" id="Seg_8751" s="T561">ten.[NOM.SG]</ta>
            <ta e="T563" id="Seg_8752" s="T562">four.[NOM.SG]</ta>
            <ta e="T565" id="Seg_8753" s="T564">make-FUT-1SG</ta>
            <ta e="T566" id="Seg_8754" s="T565">well</ta>
            <ta e="T567" id="Seg_8755" s="T566">then</ta>
            <ta e="T569" id="Seg_8756" s="T568">table-NOM/GEN/ACC.3SG</ta>
            <ta e="T570" id="Seg_8757" s="T569">collect-PST.[3SG]</ta>
            <ta e="T571" id="Seg_8758" s="T570">then</ta>
            <ta e="T572" id="Seg_8759" s="T571">come-PST.[3SG]</ta>
            <ta e="T573" id="Seg_8760" s="T572">this.[NOM.SG]</ta>
            <ta e="T574" id="Seg_8761" s="T573">man.[NOM.SG]</ta>
            <ta e="T575" id="Seg_8762" s="T574">what.kind</ta>
            <ta e="T576" id="Seg_8763" s="T575">eat-INF.LAT</ta>
            <ta e="T577" id="Seg_8764" s="T576">and</ta>
            <ta e="T578" id="Seg_8765" s="T577">shit-INF.LAT</ta>
            <ta e="T579" id="Seg_8766" s="T578">PTCL</ta>
            <ta e="T580" id="Seg_8767" s="T579">eat-DUR.[3SG]</ta>
            <ta e="T581" id="Seg_8768" s="T580">and</ta>
            <ta e="T582" id="Seg_8769" s="T581">shit-DUR.[3SG]</ta>
            <ta e="T583" id="Seg_8770" s="T582">eat-DUR.[3SG]</ta>
            <ta e="T584" id="Seg_8771" s="T583">and</ta>
            <ta e="T585" id="Seg_8772" s="T584">shit-DUR.[3SG]</ta>
            <ta e="T586" id="Seg_8773" s="T585">all</ta>
            <ta e="T587" id="Seg_8774" s="T586">what.[NOM.SG]</ta>
            <ta e="T588" id="Seg_8775" s="T587">eat-MOM-PST.[3SG]</ta>
            <ta e="T589" id="Seg_8776" s="T588">and</ta>
            <ta e="T590" id="Seg_8777" s="T589">meat.[NOM.SG]</ta>
            <ta e="T591" id="Seg_8778" s="T590">and</ta>
            <ta e="T593" id="Seg_8779" s="T592">what.[NOM.SG]</ta>
            <ta e="T594" id="Seg_8780" s="T593">be-PST.[3SG]</ta>
            <ta e="T595" id="Seg_8781" s="T594">all</ta>
            <ta e="T596" id="Seg_8782" s="T595">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T597" id="Seg_8783" s="T596">say-IPFVZ.[3SG]</ta>
            <ta e="T598" id="Seg_8784" s="T597">bring-IMP.2PL</ta>
            <ta e="T599" id="Seg_8785" s="T598">more</ta>
            <ta e="T600" id="Seg_8786" s="T599">few</ta>
            <ta e="T601" id="Seg_8787" s="T600">bring-IMP.2PL</ta>
            <ta e="T602" id="Seg_8788" s="T601">more</ta>
            <ta e="T603" id="Seg_8789" s="T602">few</ta>
            <ta e="T604" id="Seg_8790" s="T603">then</ta>
            <ta e="T605" id="Seg_8791" s="T604">PTCL</ta>
            <ta e="T606" id="Seg_8792" s="T605">eat-INF.LAT</ta>
            <ta e="T612" id="Seg_8793" s="T611">then</ta>
            <ta e="T613" id="Seg_8794" s="T612">water.[NOM.SG]</ta>
            <ta e="T615" id="Seg_8795" s="T614">drink-INF.LAT</ta>
            <ta e="T616" id="Seg_8796" s="T615">again</ta>
            <ta e="T617" id="Seg_8797" s="T616">again</ta>
            <ta e="T618" id="Seg_8798" s="T617">come-PST.[3SG]</ta>
            <ta e="T619" id="Seg_8799" s="T618">this</ta>
            <ta e="T621" id="Seg_8800" s="T620">this</ta>
            <ta e="T622" id="Seg_8801" s="T621">man.[NOM.SG]</ta>
            <ta e="T623" id="Seg_8802" s="T622">then</ta>
            <ta e="T624" id="Seg_8803" s="T623">this.[3SG]</ta>
            <ta e="T626" id="Seg_8804" s="T625">drink-DUR.[3SG]</ta>
            <ta e="T627" id="Seg_8805" s="T626">drink-DUR.[3SG]</ta>
            <ta e="T628" id="Seg_8806" s="T627">and</ta>
            <ta e="T629" id="Seg_8807" s="T628">piss-DUR.[3SG]</ta>
            <ta e="T630" id="Seg_8808" s="T629">drink-DUR.[3SG]</ta>
            <ta e="T631" id="Seg_8809" s="T630">and</ta>
            <ta e="T632" id="Seg_8810" s="T631">piss-DUR.[3SG]</ta>
            <ta e="T633" id="Seg_8811" s="T632">all</ta>
            <ta e="T634" id="Seg_8812" s="T633">drink-MOM-PST.[3SG]</ta>
            <ta e="T635" id="Seg_8813" s="T634">and</ta>
            <ta e="T636" id="Seg_8814" s="T635">shout-DUR.[3SG]</ta>
            <ta e="T637" id="Seg_8815" s="T636">more</ta>
            <ta e="T639" id="Seg_8816" s="T638">bring-IMP.2SG</ta>
            <ta e="T640" id="Seg_8817" s="T639">I.LAT</ta>
            <ta e="T641" id="Seg_8818" s="T640">few</ta>
            <ta e="T642" id="Seg_8819" s="T641">and</ta>
            <ta e="T643" id="Seg_8820" s="T642">outside-LAT</ta>
            <ta e="T644" id="Seg_8821" s="T643">PTCL</ta>
            <ta e="T645" id="Seg_8822" s="T644">very</ta>
            <ta e="T646" id="Seg_8823" s="T645">water.[NOM.SG]</ta>
            <ta e="T647" id="Seg_8824" s="T646">many</ta>
            <ta e="T649" id="Seg_8825" s="T648">then</ta>
            <ta e="T650" id="Seg_8826" s="T649">NEG</ta>
            <ta e="T651" id="Seg_8827" s="T650">eat-PST.[3SG]</ta>
            <ta e="T652" id="Seg_8828" s="T651">and</ta>
            <ta e="T653" id="Seg_8829" s="T652">water.[NOM.SG]</ta>
            <ta e="T654" id="Seg_8830" s="T653">NEG</ta>
            <ta e="T655" id="Seg_8831" s="T654">drink-PST.[3SG]</ta>
            <ta e="T656" id="Seg_8832" s="T655">this-LAT</ta>
            <ta e="T657" id="Seg_8833" s="T656">IRREAL</ta>
            <ta e="T658" id="Seg_8834" s="T657">head-ACC.3SG</ta>
            <ta e="T659" id="Seg_8835" s="T658">off-cut-PST-3PL</ta>
            <ta e="T660" id="Seg_8836" s="T659">IRREAL</ta>
            <ta e="T661" id="Seg_8837" s="T660">and</ta>
            <ta e="T662" id="Seg_8838" s="T661">hang.up-PST-3PL</ta>
            <ta e="T664" id="Seg_8839" s="T662">otherwise</ta>
            <ta e="T665" id="Seg_8840" s="T664">then</ta>
            <ta e="T666" id="Seg_8841" s="T665">this</ta>
            <ta e="T667" id="Seg_8842" s="T666">daughter-ACC</ta>
            <ta e="T668" id="Seg_8843" s="T667">take-PST.[3SG]</ta>
            <ta e="T669" id="Seg_8844" s="T668">and</ta>
            <ta e="T670" id="Seg_8845" s="T669">house-LAT/LOC.3SG</ta>
            <ta e="T671" id="Seg_8846" s="T670">go-PST.[3SG]</ta>
            <ta e="T673" id="Seg_8847" s="T672">%%</ta>
            <ta e="T674" id="Seg_8848" s="T673">what.[NOM.SG]</ta>
            <ta e="T675" id="Seg_8849" s="T674">sing-DUR-2SG</ta>
            <ta e="T677" id="Seg_8850" s="T676">and</ta>
            <ta e="T678" id="Seg_8851" s="T677">what.[NOM.SG]</ta>
            <ta e="T679" id="Seg_8852" s="T678">NEG</ta>
            <ta e="T680" id="Seg_8853" s="T679">good</ta>
            <ta e="T681" id="Seg_8854" s="T680">sing-DUR-2SG</ta>
            <ta e="T682" id="Seg_8855" s="T681">well</ta>
            <ta e="T684" id="Seg_8856" s="T683">good</ta>
            <ta e="T686" id="Seg_8857" s="T685">sing-INF.LAT</ta>
            <ta e="T687" id="Seg_8858" s="T686">see-PRS-2SG</ta>
            <ta e="T688" id="Seg_8859" s="T687">%%</ta>
            <ta e="T690" id="Seg_8860" s="T689">one.[NOM.SG]</ta>
            <ta e="T691" id="Seg_8861" s="T690">man.[NOM.SG]</ta>
            <ta e="T693" id="Seg_8862" s="T692">be-PST.[3SG]</ta>
            <ta e="T694" id="Seg_8863" s="T693">mill-NOM/GEN/ACC.3SG</ta>
            <ta e="T695" id="Seg_8864" s="T694">and</ta>
            <ta e="T696" id="Seg_8865" s="T695">and</ta>
            <ta e="T697" id="Seg_8866" s="T696">one.[NOM.SG]</ta>
            <ta e="T698" id="Seg_8867" s="T697">man.[NOM.SG]</ta>
            <ta e="T699" id="Seg_8868" s="T698">very</ta>
            <ta e="T700" id="Seg_8869" s="T699">money.[NOM.SG]</ta>
            <ta e="T701" id="Seg_8870" s="T700">many</ta>
            <ta e="T703" id="Seg_8871" s="T702">be-PST.[3SG]</ta>
            <ta e="T704" id="Seg_8872" s="T703">and</ta>
            <ta e="T705" id="Seg_8873" s="T704">clothing.[NOM.SG]</ta>
            <ta e="T706" id="Seg_8874" s="T705">and</ta>
            <ta e="T707" id="Seg_8875" s="T706">bread.[NOM.SG]</ta>
            <ta e="T708" id="Seg_8876" s="T707">this</ta>
            <ta e="T709" id="Seg_8877" s="T708">this-GEN</ta>
            <ta e="T710" id="Seg_8878" s="T709">man-LAT</ta>
            <ta e="T711" id="Seg_8879" s="T710">take-PST.[3SG]</ta>
            <ta e="T712" id="Seg_8880" s="T711">mill-NOM/GEN/ACC.3SG</ta>
            <ta e="T713" id="Seg_8881" s="T712">and</ta>
            <ta e="T715" id="Seg_8882" s="T714">take.away-RES-PST.[3SG]</ta>
            <ta e="T716" id="Seg_8883" s="T715">this.[3SG]</ta>
            <ta e="T717" id="Seg_8884" s="T716">PTCL</ta>
            <ta e="T718" id="Seg_8885" s="T717">cry-DUR.[3SG]</ta>
            <ta e="T719" id="Seg_8886" s="T718">and</ta>
            <ta e="T720" id="Seg_8887" s="T719">rooster.[NOM.SG]</ta>
            <ta e="T721" id="Seg_8888" s="T720">say-PRS.[3SG]</ta>
            <ta e="T722" id="Seg_8889" s="T721">what.[NOM.SG]</ta>
            <ta e="T723" id="Seg_8890" s="T722">cry-DUR-2SG</ta>
            <ta e="T724" id="Seg_8891" s="T723">go-OPT.DU/PL-1DU</ta>
            <ta e="T725" id="Seg_8892" s="T724">soon</ta>
            <ta e="T726" id="Seg_8893" s="T725">take-1DU</ta>
            <ta e="T727" id="Seg_8894" s="T726">mill-NOM/GEN/ACC.3SG</ta>
            <ta e="T728" id="Seg_8895" s="T727">this.[NOM.SG]</ta>
            <ta e="T729" id="Seg_8896" s="T728">%%</ta>
            <ta e="T730" id="Seg_8897" s="T729">go-PST-3PL</ta>
            <ta e="T731" id="Seg_8898" s="T730">rooster.[NOM.SG]</ta>
            <ta e="T732" id="Seg_8899" s="T731">shout-DUR.[3SG]</ta>
            <ta e="T733" id="Seg_8900" s="T732">bring-IMP.2SG</ta>
            <ta e="T734" id="Seg_8901" s="T733">mill.[NOM.SG]</ta>
            <ta e="T735" id="Seg_8902" s="T734">bring-IMP.2SG</ta>
            <ta e="T736" id="Seg_8903" s="T735">mill.[NOM.SG]</ta>
            <ta e="T738" id="Seg_8904" s="T737">this</ta>
            <ta e="T739" id="Seg_8905" s="T738">man.[NOM.SG]</ta>
            <ta e="T740" id="Seg_8906" s="T739">say-IPFVZ.[3SG]</ta>
            <ta e="T741" id="Seg_8907" s="T740">throw.away-IMP.2SG.O</ta>
            <ta e="T742" id="Seg_8908" s="T741">this-ACC</ta>
            <ta e="T743" id="Seg_8909" s="T742">horse-INS</ta>
            <ta e="T744" id="Seg_8910" s="T743">horse-3PL-LAT</ta>
            <ta e="T745" id="Seg_8911" s="T744">throw.away-MOM-PST.[3SG]</ta>
            <ta e="T746" id="Seg_8912" s="T745">this.[3SG]</ta>
            <ta e="T747" id="Seg_8913" s="T746">from.there</ta>
            <ta e="T748" id="Seg_8914" s="T747">come-PST.[3SG]</ta>
            <ta e="T749" id="Seg_8915" s="T748">again</ta>
            <ta e="T750" id="Seg_8916" s="T749">shout-DUR.[3SG]</ta>
            <ta e="T752" id="Seg_8917" s="T751">bring-IMP.2SG.O</ta>
            <ta e="T753" id="Seg_8918" s="T752">mill-NOM/GEN/ACC.3SG</ta>
            <ta e="T754" id="Seg_8919" s="T753">bring-IMP.2SG.O</ta>
            <ta e="T755" id="Seg_8920" s="T754">mill-NOM/GEN/ACC.3SG</ta>
            <ta e="T756" id="Seg_8921" s="T755">then</ta>
            <ta e="T757" id="Seg_8922" s="T756">throw.away-IMP.2SG.O</ta>
            <ta e="T759" id="Seg_8923" s="T758">this-ACC</ta>
            <ta e="T760" id="Seg_8924" s="T759">cow-LAT</ta>
            <ta e="T761" id="Seg_8925" s="T760">throw.away-MOM-PST.[3SG]</ta>
            <ta e="T762" id="Seg_8926" s="T761">cow-LAT</ta>
            <ta e="T763" id="Seg_8927" s="T762">this.[3SG]</ta>
            <ta e="T764" id="Seg_8928" s="T763">cow-LOC</ta>
            <ta e="T766" id="Seg_8929" s="T765">again</ta>
            <ta e="T767" id="Seg_8930" s="T766">come-PST.[3SG]</ta>
            <ta e="T768" id="Seg_8931" s="T767">then</ta>
            <ta e="T769" id="Seg_8932" s="T768">again</ta>
            <ta e="T770" id="Seg_8933" s="T769">say-IPFVZ.[3SG]</ta>
            <ta e="T773" id="Seg_8934" s="T772">bring-IMP.2SG.O</ta>
            <ta e="T774" id="Seg_8935" s="T773">mill-NOM/GEN/ACC.3SG</ta>
            <ta e="T775" id="Seg_8936" s="T774">bring-IMP.2SG.O</ta>
            <ta e="T777" id="Seg_8937" s="T775">mill-NOM/GEN/ACC.3SG</ta>
            <ta e="T778" id="Seg_8938" s="T777">throw.away-IMP.2SG.O</ta>
            <ta e="T779" id="Seg_8939" s="T778">sheep-LAT</ta>
            <ta e="T780" id="Seg_8940" s="T779">throw.away-MOM-PST.[3SG]</ta>
            <ta e="T781" id="Seg_8941" s="T780">sheep-LAT</ta>
            <ta e="T782" id="Seg_8942" s="T781">then</ta>
            <ta e="T783" id="Seg_8943" s="T782">again</ta>
            <ta e="T784" id="Seg_8944" s="T783">come-PST.[3SG]</ta>
            <ta e="T785" id="Seg_8945" s="T784">again</ta>
            <ta e="T786" id="Seg_8946" s="T785">shout-DUR.[3SG]</ta>
            <ta e="T787" id="Seg_8947" s="T786">bring-IMP.2SG.O</ta>
            <ta e="T788" id="Seg_8948" s="T787">mill-NOM/GEN/ACC.3SG</ta>
            <ta e="T789" id="Seg_8949" s="T788">bring-IMP.2SG.O</ta>
            <ta e="T790" id="Seg_8950" s="T789">mill-NOM/GEN/ACC.3SG</ta>
            <ta e="T791" id="Seg_8951" s="T790">then</ta>
            <ta e="T792" id="Seg_8952" s="T791">say-IPFVZ.[3SG]</ta>
            <ta e="T793" id="Seg_8953" s="T792">throw.away-IMP.2SG.O</ta>
            <ta e="T794" id="Seg_8954" s="T793">this-ACC</ta>
            <ta e="T795" id="Seg_8955" s="T794">water-LAT</ta>
            <ta e="T796" id="Seg_8956" s="T795">this-ACC</ta>
            <ta e="T797" id="Seg_8957" s="T796">throw.away-MOM-PST.[3SG]</ta>
            <ta e="T798" id="Seg_8958" s="T797">and</ta>
            <ta e="T799" id="Seg_8959" s="T798">this.[3SG]</ta>
            <ta e="T800" id="Seg_8960" s="T799">say-IPFVZ.[3SG]</ta>
            <ta e="T802" id="Seg_8961" s="T800">ass-NOM/GEN/ACC.1SG</ta>
            <ta e="T804" id="Seg_8962" s="T803">throw.away-MOM-PST.[3SG]</ta>
            <ta e="T805" id="Seg_8963" s="T804">this.[3SG]</ta>
            <ta e="T806" id="Seg_8964" s="T805">this-ACC</ta>
            <ta e="T807" id="Seg_8965" s="T806">water-LAT</ta>
            <ta e="T808" id="Seg_8966" s="T807">and</ta>
            <ta e="T809" id="Seg_8967" s="T808">this.[NOM.SG]</ta>
            <ta e="T810" id="Seg_8968" s="T809">shout-DUR.[3SG]</ta>
            <ta e="T811" id="Seg_8969" s="T810">ass-NOM/GEN/ACC.1SG</ta>
            <ta e="T812" id="Seg_8970" s="T811">drink-EP-IMP.2SG</ta>
            <ta e="T813" id="Seg_8971" s="T812">water.[NOM.SG]</ta>
            <ta e="T814" id="Seg_8972" s="T813">ass-NOM/GEN/ACC.1SG</ta>
            <ta e="T815" id="Seg_8973" s="T814">drink-EP-IMP.2SG</ta>
            <ta e="T816" id="Seg_8974" s="T815">water.[NOM.SG]</ta>
            <ta e="T817" id="Seg_8975" s="T816">ass-NOM/GEN/ACC.1SG</ta>
            <ta e="T818" id="Seg_8976" s="T817">all</ta>
            <ta e="T819" id="Seg_8977" s="T818">water.[NOM.SG]</ta>
            <ta e="T820" id="Seg_8978" s="T819">drink-MOM-PST.[3SG]</ta>
            <ta e="T821" id="Seg_8979" s="T820">again</ta>
            <ta e="T822" id="Seg_8980" s="T821">come-PST.[3SG]</ta>
            <ta e="T823" id="Seg_8981" s="T822">bring-IMP.2SG.O</ta>
            <ta e="T824" id="Seg_8982" s="T823">mill-NOM/GEN/ACC.3SG</ta>
            <ta e="T825" id="Seg_8983" s="T824">bring-IMP.2SG.O</ta>
            <ta e="T826" id="Seg_8984" s="T825">mill-NOM/GEN/ACC.3SG</ta>
            <ta e="T827" id="Seg_8985" s="T826">then</ta>
            <ta e="T828" id="Seg_8986" s="T827">this</ta>
            <ta e="T829" id="Seg_8987" s="T828">man.[NOM.SG]</ta>
            <ta e="T830" id="Seg_8988" s="T829">say-IPFVZ.[3SG]</ta>
            <ta e="T831" id="Seg_8989" s="T830">fire.[NOM.SG]</ta>
            <ta e="T833" id="Seg_8990" s="T832">put-IMP.2PL</ta>
            <ta e="T834" id="Seg_8991" s="T833">big.[NOM.SG]</ta>
            <ta e="T835" id="Seg_8992" s="T834">and</ta>
            <ta e="T836" id="Seg_8993" s="T835">throw.away-IMP.2SG.O</ta>
            <ta e="T837" id="Seg_8994" s="T836">this-ACC</ta>
            <ta e="T838" id="Seg_8995" s="T837">fire-LAT</ta>
            <ta e="T839" id="Seg_8996" s="T838">then</ta>
            <ta e="T840" id="Seg_8997" s="T839">put-PST-3PL</ta>
            <ta e="T841" id="Seg_8998" s="T840">fire.[NOM.SG]</ta>
            <ta e="T842" id="Seg_8999" s="T841">throw.away-MOM-PST.[3SG]</ta>
            <ta e="T843" id="Seg_9000" s="T842">this</ta>
            <ta e="T844" id="Seg_9001" s="T843">rooster-ACC</ta>
            <ta e="T845" id="Seg_9002" s="T844">fire-LAT</ta>
            <ta e="T846" id="Seg_9003" s="T845">there</ta>
            <ta e="T847" id="Seg_9004" s="T846">say-IPFVZ.[3SG]</ta>
            <ta e="T848" id="Seg_9005" s="T847">pour-IMP.2SG</ta>
            <ta e="T849" id="Seg_9006" s="T848">ass-NOM/GEN/ACC.1SG</ta>
            <ta e="T850" id="Seg_9007" s="T849">water.[NOM.SG]</ta>
            <ta e="T851" id="Seg_9008" s="T850">pour-IMP.2SG</ta>
            <ta e="T852" id="Seg_9009" s="T851">ass-NOM/GEN/ACC.1SG</ta>
            <ta e="T853" id="Seg_9010" s="T852">water.[NOM.SG]</ta>
            <ta e="T854" id="Seg_9011" s="T853">like</ta>
            <ta e="T855" id="Seg_9012" s="T854">water.[NOM.SG]</ta>
            <ta e="T856" id="Seg_9013" s="T855">go-PST.[3SG]</ta>
            <ta e="T857" id="Seg_9014" s="T856">all</ta>
            <ta e="T858" id="Seg_9015" s="T857">what.[NOM.SG]</ta>
            <ta e="T860" id="Seg_9016" s="T858">PTCL</ta>
            <ta e="T862" id="Seg_9017" s="T861">then</ta>
            <ta e="T863" id="Seg_9018" s="T862">water.[NOM.SG]</ta>
            <ta e="T864" id="Seg_9019" s="T863">very</ta>
            <ta e="T865" id="Seg_9020" s="T864">many</ta>
            <ta e="T867" id="Seg_9021" s="T866">flow-PST.[3SG]</ta>
            <ta e="T868" id="Seg_9022" s="T867">this</ta>
            <ta e="T869" id="Seg_9023" s="T868">man.[NOM.SG]</ta>
            <ta e="T870" id="Seg_9024" s="T869">say-IPFVZ.[3SG]</ta>
            <ta e="T871" id="Seg_9025" s="T870">take-IMP.2SG</ta>
            <ta e="T872" id="Seg_9026" s="T871">mill-NOM/GEN/ACC.3SG</ta>
            <ta e="T873" id="Seg_9027" s="T872">and</ta>
            <ta e="T874" id="Seg_9028" s="T873">go-EP-IMP.2SG</ta>
            <ta e="T875" id="Seg_9029" s="T874">hence</ta>
            <ta e="T876" id="Seg_9030" s="T875">hence</ta>
            <ta e="T877" id="Seg_9031" s="T876">here</ta>
            <ta e="T881" id="Seg_9032" s="T880">%%-PL</ta>
            <ta e="T882" id="Seg_9033" s="T881">one.[NOM.SG]</ta>
            <ta e="T884" id="Seg_9034" s="T883">stand-PST-3PL</ta>
            <ta e="T885" id="Seg_9035" s="T884">I.NOM</ta>
            <ta e="T886" id="Seg_9036" s="T885">this-PL</ta>
            <ta e="T889" id="Seg_9037" s="T888">send-PST-1SG</ta>
            <ta e="T890" id="Seg_9038" s="T889">then</ta>
            <ta e="T891" id="Seg_9039" s="T890">this-PL</ta>
            <ta e="T892" id="Seg_9040" s="T891">run-MOM-PST-3PL</ta>
            <ta e="T893" id="Seg_9041" s="T892">and</ta>
            <ta e="T894" id="Seg_9042" s="T893">this-PL</ta>
            <ta e="T895" id="Seg_9043" s="T894">NEG</ta>
            <ta e="T896" id="Seg_9044" s="T895">run-PRS-3PL</ta>
            <ta e="T897" id="Seg_9045" s="T896">lazy.[NOM.SG]</ta>
            <ta e="T898" id="Seg_9046" s="T897">become-RES-PST-3PL</ta>
            <ta e="T900" id="Seg_9047" s="T899">yesterday</ta>
            <ta e="T901" id="Seg_9048" s="T900">I.NOM</ta>
            <ta e="T902" id="Seg_9049" s="T901">daughter_in_law-NOM/GEN/ACC.1SG</ta>
            <ta e="T903" id="Seg_9050" s="T902">NEG</ta>
            <ta e="T1025" id="Seg_9051" s="T903">go-CVB</ta>
            <ta e="T904" id="Seg_9052" s="T1025">disappear-PST.[3SG]</ta>
            <ta e="T905" id="Seg_9053" s="T904">house-LAT/LOC.3SG</ta>
            <ta e="T906" id="Seg_9054" s="T905">I.NOM</ta>
            <ta e="T907" id="Seg_9055" s="T906">go-PST-1SG</ta>
            <ta e="T908" id="Seg_9056" s="T907">and</ta>
            <ta e="T909" id="Seg_9057" s="T908">this-COM</ta>
            <ta e="T910" id="Seg_9058" s="T909">NEG</ta>
            <ta e="T911" id="Seg_9059" s="T910">kiss-PST-1SG</ta>
            <ta e="T912" id="Seg_9060" s="T911">hand-NOM/GEN/ACC.1SG</ta>
            <ta e="T913" id="Seg_9061" s="T912">NEG</ta>
            <ta e="T914" id="Seg_9062" s="T913">give-PST-1SG</ta>
            <ta e="T915" id="Seg_9063" s="T914">then</ta>
            <ta e="T916" id="Seg_9064" s="T915">this</ta>
            <ta e="T917" id="Seg_9065" s="T916">again</ta>
            <ta e="T918" id="Seg_9066" s="T917">return-MOM-PST.[3SG]</ta>
            <ta e="T919" id="Seg_9067" s="T918">very</ta>
            <ta e="T920" id="Seg_9068" s="T919">wind.[NOM.SG]</ta>
            <ta e="T921" id="Seg_9069" s="T920">snow.[NOM.SG]</ta>
            <ta e="T922" id="Seg_9070" s="T921">PTCL</ta>
            <ta e="T923" id="Seg_9071" s="T922">bring-TR-DUR.[3SG]</ta>
            <ta e="T924" id="Seg_9072" s="T923">machine-PL</ta>
            <ta e="T925" id="Seg_9073" s="T924">NEG</ta>
            <ta e="T926" id="Seg_9074" s="T925">go-PST-3PL</ta>
            <ta e="T927" id="Seg_9075" s="T926">this.[NOM.SG]</ta>
            <ta e="T928" id="Seg_9076" s="T927">return-MOM-PST.[3SG]</ta>
            <ta e="T929" id="Seg_9077" s="T928">here</ta>
            <ta e="T930" id="Seg_9078" s="T929">spend.night-PST.[3SG]</ta>
            <ta e="T931" id="Seg_9079" s="T930">and</ta>
            <ta e="T932" id="Seg_9080" s="T931">today</ta>
            <ta e="T933" id="Seg_9081" s="T932">I.NOM</ta>
            <ta e="T934" id="Seg_9082" s="T933">get.up-PST-1SG</ta>
            <ta e="T935" id="Seg_9083" s="T934">boil-PST-1SG</ta>
            <ta e="T936" id="Seg_9084" s="T935">PTCL</ta>
            <ta e="T937" id="Seg_9085" s="T936">bread.[NOM.SG]</ta>
            <ta e="T938" id="Seg_9086" s="T937">bake-PST-1SG</ta>
            <ta e="T939" id="Seg_9087" s="T938">self-LAT/LOC.3SG</ta>
            <ta e="T940" id="Seg_9088" s="T939">road-LAT</ta>
            <ta e="T941" id="Seg_9089" s="T940">eat-PST.[3SG]</ta>
            <ta e="T942" id="Seg_9090" s="T941">then</ta>
            <ta e="T1026" id="Seg_9091" s="T942">go-CVB</ta>
            <ta e="T943" id="Seg_9092" s="T1026">disappear-PST.[3SG]</ta>
            <ta e="T944" id="Seg_9093" s="T943">machine-INS</ta>
            <ta e="T945" id="Seg_9094" s="T944">chairman-INS</ta>
            <ta e="T947" id="Seg_9095" s="T946">then</ta>
            <ta e="T948" id="Seg_9096" s="T947">go-FUT-1SG</ta>
            <ta e="T949" id="Seg_9097" s="T948">house-LAT/LOC.1SG</ta>
            <ta e="T950" id="Seg_9098" s="T949">then</ta>
            <ta e="T951" id="Seg_9099" s="T950">come-PST-1SG</ta>
            <ta e="T952" id="Seg_9100" s="T951">go-FUT-1SG</ta>
            <ta e="T953" id="Seg_9101" s="T952">house-LAT/LOC.1SG</ta>
            <ta e="T954" id="Seg_9102" s="T953">otherwise</ta>
            <ta e="T955" id="Seg_9103" s="T954">I.NOM</ta>
            <ta e="T956" id="Seg_9104" s="T955">there</ta>
            <ta e="T957" id="Seg_9105" s="T956">people.[NOM.SG]</ta>
            <ta e="T958" id="Seg_9106" s="T957">sit-DUR.[3SG]</ta>
            <ta e="T959" id="Seg_9107" s="T958">I.ACC</ta>
            <ta e="T960" id="Seg_9108" s="T959">wait-DUR-3PL</ta>
            <ta e="T961" id="Seg_9109" s="T960">then</ta>
            <ta e="T962" id="Seg_9110" s="T961">come-PST-1SG</ta>
            <ta e="T963" id="Seg_9111" s="T962">house-LAT/LOC.1SG</ta>
            <ta e="T964" id="Seg_9112" s="T963">%%</ta>
            <ta e="T965" id="Seg_9113" s="T964">wash-PST-1SG</ta>
            <ta e="T966" id="Seg_9114" s="T965">calf.[NOM.SG]</ta>
            <ta e="T967" id="Seg_9115" s="T966">drink-TR-PST-1SG</ta>
            <ta e="T968" id="Seg_9116" s="T967">then</ta>
            <ta e="T969" id="Seg_9117" s="T968">cow-NOM/GEN/ACC.1SG</ta>
            <ta e="T970" id="Seg_9118" s="T969">drive-PST-1SG</ta>
            <ta e="T971" id="Seg_9119" s="T970">river-LAT</ta>
            <ta e="T972" id="Seg_9120" s="T971">drink-TR-PST-1SG</ta>
            <ta e="T973" id="Seg_9121" s="T972">then</ta>
            <ta e="T975" id="Seg_9122" s="T974">come-PRS.[3SG]</ta>
            <ta e="T977" id="Seg_9123" s="T976">girl.[NOM.SG]</ta>
            <ta e="T978" id="Seg_9124" s="T977">come-PRS.[3SG]</ta>
            <ta e="T979" id="Seg_9125" s="T978">I.LAT</ta>
            <ta e="T980" id="Seg_9126" s="T979">INCH</ta>
            <ta e="T981" id="Seg_9127" s="T980">skin-INF.LAT</ta>
            <ta e="T982" id="Seg_9128" s="T981">I.NOM</ta>
            <ta e="T983" id="Seg_9129" s="T982">stand-PST-1SG</ta>
            <ta e="T984" id="Seg_9130" s="T983">this.[NOM.SG]</ta>
            <ta e="T985" id="Seg_9131" s="T984">skin-PST.[3SG]</ta>
            <ta e="T986" id="Seg_9132" s="T985">I.ACC</ta>
            <ta e="T987" id="Seg_9133" s="T986">then</ta>
            <ta e="T990" id="Seg_9134" s="T989">come-PST-1PL</ta>
            <ta e="T991" id="Seg_9135" s="T990">house-LAT</ta>
            <ta e="T992" id="Seg_9136" s="T991">speak-INF.LAT</ta>
            <ta e="T994" id="Seg_9137" s="T993">then</ta>
            <ta e="T995" id="Seg_9138" s="T994">speak-INF.LAT</ta>
            <ta e="T996" id="Seg_9139" s="T995">tape-LAT</ta>
            <ta e="T998" id="Seg_9140" s="T997">hen-PL</ta>
            <ta e="T999" id="Seg_9141" s="T998">feed-PST-1SG</ta>
            <ta e="T1000" id="Seg_9142" s="T999">cow-GEN</ta>
            <ta e="T1001" id="Seg_9143" s="T1000">shit.[NOM.SG]</ta>
            <ta e="T1002" id="Seg_9144" s="T1001">throw.away-PST-1SG</ta>
            <ta e="T1004" id="Seg_9145" s="T1003">enough</ta>
            <ta e="T1005" id="Seg_9146" s="T1004">speak-INF.LAT</ta>
            <ta e="T1006" id="Seg_9147" s="T1005">otherwise</ta>
            <ta e="T1007" id="Seg_9148" s="T1006">child-PL</ta>
            <ta e="T1008" id="Seg_9149" s="T1007">soon</ta>
            <ta e="T1009" id="Seg_9150" s="T1008">come-FUT-3PL</ta>
            <ta e="T1010" id="Seg_9151" s="T1009">school-ABL</ta>
            <ta e="T1011" id="Seg_9152" s="T1010">NEG</ta>
            <ta e="T1012" id="Seg_9153" s="T1011">give-FUT-3PL</ta>
            <ta e="T1013" id="Seg_9154" s="T1012">speak-INF.LAT</ta>
            <ta e="T1014" id="Seg_9155" s="T1013">PTCL</ta>
            <ta e="T1015" id="Seg_9156" s="T1014">shout-FUT-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr-PKZ">
            <ta e="T1" id="Seg_9157" s="T0">я.NOM</ta>
            <ta e="T2" id="Seg_9158" s="T1">жить-PST-1SG</ta>
            <ta e="T3" id="Seg_9159" s="T2">пять.[NOM.SG]</ta>
            <ta e="T4" id="Seg_9160" s="T3">мужчина-PL</ta>
            <ta e="T5" id="Seg_9161" s="T4">быть-PST-3PL</ta>
            <ta e="T6" id="Seg_9162" s="T5">я.NOM</ta>
            <ta e="T7" id="Seg_9163" s="T6">тогда</ta>
            <ta e="T8" id="Seg_9164" s="T7">шесть.[NOM.SG]</ta>
            <ta e="T9" id="Seg_9165" s="T8">взять-PST-1SG</ta>
            <ta e="T10" id="Seg_9166" s="T9">этот</ta>
            <ta e="T11" id="Seg_9167" s="T10">шесть.[NOM.SG]</ta>
            <ta e="T12" id="Seg_9168" s="T11">мужчина.[NOM.SG]</ta>
            <ta e="T13" id="Seg_9169" s="T12">NEG</ta>
            <ta e="T14" id="Seg_9170" s="T13">жить-PST.[3SG]</ta>
            <ta e="T15" id="Seg_9171" s="T14">выбросить-PST.[3SG]</ta>
            <ta e="T16" id="Seg_9172" s="T15">и</ta>
            <ta e="T776" id="Seg_9173" s="T16">пойти-CVB</ta>
            <ta e="T17" id="Seg_9174" s="T776">исчезнуть-PST.[3SG]</ta>
            <ta e="T18" id="Seg_9175" s="T17">тогда</ta>
            <ta e="T21" id="Seg_9176" s="T20">мужчина-NOM/GEN/ACC.1SG</ta>
            <ta e="T22" id="Seg_9177" s="T21">вода-LOC</ta>
            <ta e="T23" id="Seg_9178" s="T22">умереть-RES-PST.[3SG]</ta>
            <ta e="T24" id="Seg_9179" s="T23">мальчик-NOM/GEN/ACC.1SG</ta>
            <ta e="T25" id="Seg_9180" s="T24">вода-LOC</ta>
            <ta e="T26" id="Seg_9181" s="T25">умереть-RES-PST.[3SG]</ta>
            <ta e="T27" id="Seg_9182" s="T26">тогда</ta>
            <ta e="T28" id="Seg_9183" s="T27">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T29" id="Seg_9184" s="T28">убить-PST-3PL</ta>
            <ta e="T30" id="Seg_9185" s="T29">сестра-NOM/GEN/ACC.1SG</ta>
            <ta e="T31" id="Seg_9186" s="T30">убить-PST-3PL</ta>
            <ta e="T32" id="Seg_9187" s="T31">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T33" id="Seg_9188" s="T32">лес-LAT</ta>
            <ta e="T34" id="Seg_9189" s="T33">пойти-PST.[3SG]</ta>
            <ta e="T35" id="Seg_9190" s="T34">и</ta>
            <ta e="T36" id="Seg_9191" s="T35">там</ta>
            <ta e="T37" id="Seg_9192" s="T36">умереть-RES-PST.[3SG]</ta>
            <ta e="T38" id="Seg_9193" s="T37">я.GEN</ta>
            <ta e="T39" id="Seg_9194" s="T38">сестра-NOM/GEN/ACC.1SG</ta>
            <ta e="T40" id="Seg_9195" s="T39">мужчина-NOM/GEN.3SG</ta>
            <ta e="T41" id="Seg_9196" s="T40">убить-PST.[3SG]</ta>
            <ta e="T42" id="Seg_9197" s="T41">два.[NOM.SG]</ta>
            <ta e="T44" id="Seg_9198" s="T43">мальчик.[NOM.SG]</ta>
            <ta e="T45" id="Seg_9199" s="T44">остаться-PST.[3SG]</ta>
            <ta e="T46" id="Seg_9200" s="T45">я.NOM</ta>
            <ta e="T47" id="Seg_9201" s="T46">этот-ACC.PL</ta>
            <ta e="T48" id="Seg_9202" s="T47">PTCL</ta>
            <ta e="T49" id="Seg_9203" s="T48">расти-PST-1SG</ta>
            <ta e="T50" id="Seg_9204" s="T49">одежда.[NOM.SG]</ta>
            <ta e="T51" id="Seg_9205" s="T50">взять-PST-1SG</ta>
            <ta e="T52" id="Seg_9206" s="T51">и</ta>
            <ta e="T53" id="Seg_9207" s="T52">научиться-TR-PST-1SG</ta>
            <ta e="T55" id="Seg_9208" s="T54">бумага.[NOM.SG]</ta>
            <ta e="T56" id="Seg_9209" s="T55">один.[NOM.SG]</ta>
            <ta e="T60" id="Seg_9210" s="T59">один.[NOM.SG]</ta>
            <ta e="T61" id="Seg_9211" s="T60">я.LAT</ta>
            <ta e="T62" id="Seg_9212" s="T61">жить-PST.[3SG]</ta>
            <ta e="T63" id="Seg_9213" s="T62">тогда</ta>
            <ta e="T64" id="Seg_9214" s="T63">этот-ACC</ta>
            <ta e="T65" id="Seg_9215" s="T64">взять-PST-3PL</ta>
            <ta e="T66" id="Seg_9216" s="T65">война-LAT</ta>
            <ta e="T67" id="Seg_9217" s="T66">там</ta>
            <ta e="T68" id="Seg_9218" s="T67">убить-PST-3PL</ta>
            <ta e="T69" id="Seg_9219" s="T68">этот-GEN</ta>
            <ta e="T70" id="Seg_9220" s="T69">девушка.[NOM.SG]</ta>
            <ta e="T71" id="Seg_9221" s="T70">остаться-MOM-PST.[3SG]</ta>
            <ta e="T73" id="Seg_9222" s="T72">я.NOM</ta>
            <ta e="T75" id="Seg_9223" s="T74">печь-NOM/GEN/ACC.3SG</ta>
            <ta e="T77" id="Seg_9224" s="T76">замерзнуть-RES-PST-1SG</ta>
            <ta e="T78" id="Seg_9225" s="T77">печь-LAT</ta>
            <ta e="T82" id="Seg_9226" s="T81">влезать-PST-1SG</ta>
            <ta e="T83" id="Seg_9227" s="T82">сидеть-PST-1SG</ta>
            <ta e="T84" id="Seg_9228" s="T83">лежать-PST-1SG</ta>
            <ta e="T85" id="Seg_9229" s="T84">спать-PST-1SG</ta>
            <ta e="T86" id="Seg_9230" s="T85">кедровый.орех.[NOM.SG]</ta>
            <ta e="T88" id="Seg_9231" s="T87">лущить-FUT-1SG</ta>
            <ta e="T89" id="Seg_9232" s="T88">тогда</ta>
            <ta e="T90" id="Seg_9233" s="T89">вы.NOM</ta>
            <ta e="T91" id="Seg_9234" s="T90">прийти-PST-3PL</ta>
            <ta e="T92" id="Seg_9235" s="T91">я.NOM</ta>
            <ta e="T93" id="Seg_9236" s="T92">прийти-PST-1SG</ta>
            <ta e="T94" id="Seg_9237" s="T93">вы.ACC</ta>
            <ta e="T95" id="Seg_9238" s="T94">и</ta>
            <ta e="T96" id="Seg_9239" s="T95">PTCL</ta>
            <ta e="T97" id="Seg_9240" s="T96">говорить-DUR-1SG</ta>
            <ta e="T99" id="Seg_9241" s="T98">три.[NOM.SG]</ta>
            <ta e="T100" id="Seg_9242" s="T99">брат.[NOM.SG]</ta>
            <ta e="T101" id="Seg_9243" s="T100">жить-PST-3PL</ta>
            <ta e="T102" id="Seg_9244" s="T101">и</ta>
            <ta e="T103" id="Seg_9245" s="T102">тогда</ta>
            <ta e="T104" id="Seg_9246" s="T103">пойти-PST-3PL</ta>
            <ta e="T105" id="Seg_9247" s="T104">дерево.[NOM.SG]</ta>
            <ta e="T106" id="Seg_9248" s="T105">резать-INF.LAT</ta>
            <ta e="T107" id="Seg_9249" s="T106">этот-PL-LOC</ta>
            <ta e="T108" id="Seg_9250" s="T107">огонь.[NOM.SG]</ta>
            <ta e="T109" id="Seg_9251" s="T108">NEG.EX-PST.[3SG]</ta>
            <ta e="T111" id="Seg_9252" s="T110">найти-PST-3PL</ta>
            <ta e="T112" id="Seg_9253" s="T111">огонь.[NOM.SG]</ta>
            <ta e="T113" id="Seg_9254" s="T112">гореть-DUR.[3SG]</ta>
            <ta e="T114" id="Seg_9255" s="T113">пойти-EP-IMP.2SG</ta>
            <ta e="T115" id="Seg_9256" s="T114">взять-IMP.2SG</ta>
            <ta e="T116" id="Seg_9257" s="T115">огонь.[NOM.SG]</ta>
            <ta e="T117" id="Seg_9258" s="T116">этот.[NOM.SG]</ta>
            <ta e="T118" id="Seg_9259" s="T117">пойти-PST.[3SG]</ta>
            <ta e="T119" id="Seg_9260" s="T118">там</ta>
            <ta e="T120" id="Seg_9261" s="T119">мужчина.[NOM.SG]</ta>
            <ta e="T121" id="Seg_9262" s="T120">сидеть-DUR.[3SG]</ta>
            <ta e="T123" id="Seg_9263" s="T122">сказать-IMP.2SG</ta>
            <ta e="T125" id="Seg_9264" s="T124">тогда</ta>
            <ta e="T126" id="Seg_9265" s="T125">огонь.[NOM.SG]</ta>
            <ta e="T127" id="Seg_9266" s="T126">дать-FUT-1SG</ta>
            <ta e="T128" id="Seg_9267" s="T127">а</ta>
            <ta e="T129" id="Seg_9268" s="T128">этот.[NOM.SG]</ta>
            <ta e="T130" id="Seg_9269" s="T129">сказать-IPFVZ.[3SG]</ta>
            <ta e="T131" id="Seg_9270" s="T130">я.NOM</ta>
            <ta e="T132" id="Seg_9271" s="T131">NEG</ta>
            <ta e="T133" id="Seg_9272" s="T132">знать-1SG</ta>
            <ta e="T134" id="Seg_9273" s="T133">этот.[NOM.SG]</ta>
            <ta e="T135" id="Seg_9274" s="T134">спина-NOM/GEN/ACC.3SG</ta>
            <ta e="T140" id="Seg_9275" s="T139">резать-PST.[3SG]</ta>
            <ta e="T142" id="Seg_9276" s="T141">три.[NOM.SG]</ta>
            <ta e="T144" id="Seg_9277" s="T143">резать-PST.[3SG]</ta>
            <ta e="T145" id="Seg_9278" s="T144">этот.[NOM.SG]</ta>
            <ta e="T1017" id="Seg_9279" s="T146">пойти-CVB</ta>
            <ta e="T147" id="Seg_9280" s="T1017">исчезнуть-PST.[3SG]</ta>
            <ta e="T148" id="Seg_9281" s="T147">и</ta>
            <ta e="T149" id="Seg_9282" s="T148">огонь.[NOM.SG]</ta>
            <ta e="T150" id="Seg_9283" s="T149">NEG</ta>
            <ta e="T151" id="Seg_9284" s="T150">дать-PST.[3SG]</ta>
            <ta e="T153" id="Seg_9285" s="T151">тогда</ta>
            <ta e="T154" id="Seg_9286" s="T153">тогда</ta>
            <ta e="T155" id="Seg_9287" s="T154">один.[NOM.SG]</ta>
            <ta e="T156" id="Seg_9288" s="T155">опять</ta>
            <ta e="T157" id="Seg_9289" s="T156">пойти-PST.[3SG]</ta>
            <ta e="T158" id="Seg_9290" s="T157">и</ta>
            <ta e="T159" id="Seg_9291" s="T158">этот-LAT</ta>
            <ta e="T160" id="Seg_9292" s="T159">сказать-PST.[3SG]</ta>
            <ta e="T161" id="Seg_9293" s="T160">сказать-IMP.2SG</ta>
            <ta e="T163" id="Seg_9294" s="T162">этот.[NOM.SG]</ta>
            <ta e="T164" id="Seg_9295" s="T163">сказать-IPFVZ.[3SG]</ta>
            <ta e="T165" id="Seg_9296" s="T164">я.NOM</ta>
            <ta e="T166" id="Seg_9297" s="T165">NEG</ta>
            <ta e="T167" id="Seg_9298" s="T166">знать-1SG</ta>
            <ta e="T168" id="Seg_9299" s="T167">ну</ta>
            <ta e="T169" id="Seg_9300" s="T168">тогда</ta>
            <ta e="T170" id="Seg_9301" s="T169">там</ta>
            <ta e="T171" id="Seg_9302" s="T170">PTCL</ta>
            <ta e="T172" id="Seg_9303" s="T171">зарезать-PST.[3SG]</ta>
            <ta e="T173" id="Seg_9304" s="T172">спина-NOM/GEN/ACC.3SG</ta>
            <ta e="T174" id="Seg_9305" s="T173">и</ta>
            <ta e="T175" id="Seg_9306" s="T174">пойти-PST.[3SG]</ta>
            <ta e="T176" id="Seg_9307" s="T175">NEG</ta>
            <ta e="T177" id="Seg_9308" s="T176">дать-PST.[3SG]</ta>
            <ta e="T178" id="Seg_9309" s="T177">огонь.[NOM.SG]</ta>
            <ta e="T179" id="Seg_9310" s="T178">тогда</ta>
            <ta e="T180" id="Seg_9311" s="T179">один.[NOM.SG]</ta>
            <ta e="T181" id="Seg_9312" s="T180">опять</ta>
            <ta e="T182" id="Seg_9313" s="T181">прийти-PST.[3SG]</ta>
            <ta e="T183" id="Seg_9314" s="T182">сказать-IMP.2SG</ta>
            <ta e="T185" id="Seg_9315" s="T184">ну</ta>
            <ta e="T186" id="Seg_9316" s="T185">этот.[NOM.SG]</ta>
            <ta e="T187" id="Seg_9317" s="T186">сказать-IPFVZ.[3SG]</ta>
            <ta e="T188" id="Seg_9318" s="T187">сказать-FUT-1SG</ta>
            <ta e="T189" id="Seg_9319" s="T188">десять.[NOM.SG]</ta>
            <ta e="T190" id="Seg_9320" s="T189">быть-PST-3PL</ta>
            <ta e="T191" id="Seg_9321" s="T190">брат-PL</ta>
            <ta e="T192" id="Seg_9322" s="T191">тогда</ta>
            <ta e="T193" id="Seg_9323" s="T192">пойти-PST-3PL</ta>
            <ta e="T194" id="Seg_9324" s="T193">дерево.[NOM.SG]</ta>
            <ta e="T195" id="Seg_9325" s="T194">резать-INF.LAT</ta>
            <ta e="T196" id="Seg_9326" s="T195">тогда</ta>
            <ta e="T197" id="Seg_9327" s="T196">%%</ta>
            <ta e="T198" id="Seg_9328" s="T197">прыгнуть-MOM-PST.[3SG]</ta>
            <ta e="T199" id="Seg_9329" s="T198">этот.[NOM.SG]</ta>
            <ta e="T200" id="Seg_9330" s="T199">PTCL</ta>
            <ta e="T201" id="Seg_9331" s="T200">место-LAT</ta>
            <ta e="T1018" id="Seg_9332" s="T201">пойти-CVB</ta>
            <ta e="T202" id="Seg_9333" s="T1018">исчезнуть-PST.[3SG]</ta>
            <ta e="T203" id="Seg_9334" s="T202">тогда</ta>
            <ta e="T204" id="Seg_9335" s="T203">утка.[NOM.SG]</ta>
            <ta e="T205" id="Seg_9336" s="T204">прийти-PST.[3SG]</ta>
            <ta e="T206" id="Seg_9337" s="T205">чум.[NOM.SG]</ta>
            <ta e="T207" id="Seg_9338" s="T206">делать-PST.[3SG]</ta>
            <ta e="T208" id="Seg_9339" s="T207">яйцо-PL</ta>
            <ta e="T209" id="Seg_9340" s="T208">рожать-PST.[3SG]</ta>
            <ta e="T210" id="Seg_9341" s="T209">тогда</ta>
            <ta e="T211" id="Seg_9342" s="T210">прийти-PST.[3SG]</ta>
            <ta e="T212" id="Seg_9343" s="T211">лес-ABL</ta>
            <ta e="T213" id="Seg_9344" s="T212">собака.[NOM.SG]</ta>
            <ta e="T214" id="Seg_9345" s="T213">большой.[NOM.SG]</ta>
            <ta e="T215" id="Seg_9346" s="T214">и</ta>
            <ta e="T216" id="Seg_9347" s="T215">INCH</ta>
            <ta e="T217" id="Seg_9348" s="T216">яйцо-PL</ta>
            <ta e="T218" id="Seg_9349" s="T217">съесть-INF.LAT</ta>
            <ta e="T219" id="Seg_9350" s="T218">этот.[NOM.SG]</ta>
            <ta e="T221" id="Seg_9351" s="T220">ловить-MOM-PST.[3SG]</ta>
            <ta e="T222" id="Seg_9352" s="T221">хвост.[NOM.SG]</ta>
            <ta e="T223" id="Seg_9353" s="T222">этот.[NOM.SG]</ta>
            <ta e="T224" id="Seg_9354" s="T223">PTCL</ta>
            <ta e="T226" id="Seg_9355" s="T224">этот-ACC</ta>
            <ta e="T229" id="Seg_9356" s="T228">тогда</ta>
            <ta e="T230" id="Seg_9357" s="T229">этот.[NOM.SG]</ta>
            <ta e="T231" id="Seg_9358" s="T230">ловить-MOM-PST.[3SG]</ta>
            <ta e="T232" id="Seg_9359" s="T231">хвост.[NOM.SG]</ta>
            <ta e="T233" id="Seg_9360" s="T232">хвост.[NOM.SG]</ta>
            <ta e="T235" id="Seg_9361" s="T234">оттянуть-PST.[3SG]</ta>
            <ta e="T236" id="Seg_9362" s="T235">тогда</ta>
            <ta e="T237" id="Seg_9363" s="T236">нож-INS</ta>
            <ta e="T239" id="Seg_9364" s="T238">резать-PST.[3SG]</ta>
            <ta e="T240" id="Seg_9365" s="T239">там</ta>
            <ta e="T241" id="Seg_9366" s="T240">ящик.[NOM.SG]</ta>
            <ta e="T242" id="Seg_9367" s="T241">видеть-PST.[3SG]</ta>
            <ta e="T243" id="Seg_9368" s="T242">ящик-LOC</ta>
            <ta e="T244" id="Seg_9369" s="T243">бумага.[NOM.SG]</ta>
            <ta e="T245" id="Seg_9370" s="T244">написанный</ta>
            <ta e="T246" id="Seg_9371" s="T245">я.NOM</ta>
            <ta e="T248" id="Seg_9372" s="T247">я.GEN</ta>
            <ta e="T249" id="Seg_9373" s="T248">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T250" id="Seg_9374" s="T249">ты.NOM</ta>
            <ta e="T252" id="Seg_9375" s="T251">отец-3SG-COM</ta>
            <ta e="T253" id="Seg_9376" s="T252">ты.NOM</ta>
            <ta e="T254" id="Seg_9377" s="T253">отец-LAT/LOC.3SG</ta>
            <ta e="T255" id="Seg_9378" s="T254">испражняться-INF.LAT</ta>
            <ta e="T1019" id="Seg_9379" s="T255">пойти-CVB</ta>
            <ta e="T256" id="Seg_9380" s="T1019">исчезнуть-PST.[3SG]</ta>
            <ta e="T258" id="Seg_9381" s="T257">тогда</ta>
            <ta e="T259" id="Seg_9382" s="T258">этот</ta>
            <ta e="T260" id="Seg_9383" s="T259">мужчина.[NOM.SG]</ta>
            <ta e="T261" id="Seg_9384" s="T260">сказать-PST.[3SG]</ta>
            <ta e="T262" id="Seg_9385" s="T261">NEG.AUX-IMP.2SG</ta>
            <ta e="T263" id="Seg_9386" s="T262">лгать-CNG</ta>
            <ta e="T264" id="Seg_9387" s="T263">а</ta>
            <ta e="T265" id="Seg_9388" s="T264">этот.[NOM.SG]</ta>
            <ta e="T267" id="Seg_9389" s="T266">этот.[NOM.SG]</ta>
            <ta e="T269" id="Seg_9390" s="T268">мужчина.[NOM.SG]</ta>
            <ta e="T270" id="Seg_9391" s="T269">этот-LAT</ta>
            <ta e="T271" id="Seg_9392" s="T270">спина-NOM/GEN/ACC.3SG</ta>
            <ta e="T273" id="Seg_9393" s="T272">резать-PST.[3SG]</ta>
            <ta e="T274" id="Seg_9394" s="T273">и</ta>
            <ta e="T275" id="Seg_9395" s="T274">огонь.[NOM.SG]</ta>
            <ta e="T276" id="Seg_9396" s="T275">быть-PST.[3SG]</ta>
            <ta e="T277" id="Seg_9397" s="T276">и</ta>
            <ta e="T1020" id="Seg_9398" s="T277">пойти-CVB</ta>
            <ta e="T278" id="Seg_9399" s="T1020">исчезнуть-PST.[3SG]</ta>
            <ta e="T280" id="Seg_9400" s="T279">я.GEN</ta>
            <ta e="T281" id="Seg_9401" s="T280">невестка-NOM/GEN/ACC.1SG</ta>
            <ta e="T282" id="Seg_9402" s="T281">я.GEN</ta>
            <ta e="T283" id="Seg_9403" s="T282">мужчина.[NOM.SG]</ta>
            <ta e="T286" id="Seg_9404" s="T285">взять-CVB-PST.[3SG]</ta>
            <ta e="T287" id="Seg_9405" s="T286">и</ta>
            <ta e="T1021" id="Seg_9406" s="T287">пойти-CVB</ta>
            <ta e="T288" id="Seg_9407" s="T1021">исчезнуть-PST-3PL</ta>
            <ta e="T289" id="Seg_9408" s="T288">мы.NOM</ta>
            <ta e="T290" id="Seg_9409" s="T289">хороший</ta>
            <ta e="T291" id="Seg_9410" s="T290">жить-PST-3PL</ta>
            <ta e="T292" id="Seg_9411" s="T291">а</ta>
            <ta e="T293" id="Seg_9412" s="T292">этот.[NOM.SG]</ta>
            <ta e="T294" id="Seg_9413" s="T293">я.ACC</ta>
            <ta e="T295" id="Seg_9414" s="T294">жалеть-PST.[3SG]</ta>
            <ta e="T296" id="Seg_9415" s="T295">рука-LAT</ta>
            <ta e="T297" id="Seg_9416" s="T296">взять-PST.[3SG]</ta>
            <ta e="T298" id="Seg_9417" s="T297">а</ta>
            <ta e="T299" id="Seg_9418" s="T298">тогда</ta>
            <ta e="T1022" id="Seg_9419" s="T299">пойти-CVB</ta>
            <ta e="T300" id="Seg_9420" s="T1022">исчезнуть-PST.[3SG]</ta>
            <ta e="T301" id="Seg_9421" s="T300">Игарка-LAT</ta>
            <ta e="T302" id="Seg_9422" s="T301">и</ta>
            <ta e="T303" id="Seg_9423" s="T302">там</ta>
            <ta e="T304" id="Seg_9424" s="T303">умереть-RES-PST.[3SG]</ta>
            <ta e="T306" id="Seg_9425" s="T305">а</ta>
            <ta e="T307" id="Seg_9426" s="T306">этот.[NOM.SG]</ta>
            <ta e="T308" id="Seg_9427" s="T307">NEG</ta>
            <ta e="T309" id="Seg_9428" s="T308">красивый.[NOM.SG]</ta>
            <ta e="T310" id="Seg_9429" s="T309">быть-PST.[3SG]</ta>
            <ta e="T311" id="Seg_9430" s="T310">рот-NOM/GEN/ACC.3SG</ta>
            <ta e="T313" id="Seg_9431" s="T311">PTCL</ta>
            <ta e="T315" id="Seg_9432" s="T314">этот.[NOM.SG]</ta>
            <ta e="T316" id="Seg_9433" s="T315">NEG</ta>
            <ta e="T317" id="Seg_9434" s="T316">красивый.[NOM.SG]</ta>
            <ta e="T318" id="Seg_9435" s="T317">рот-NOM/GEN/ACC.3SG</ta>
            <ta e="T319" id="Seg_9436" s="T318">кривой.[NOM.SG]</ta>
            <ta e="T320" id="Seg_9437" s="T319">глаз-NOM/GEN.3SG</ta>
            <ta e="T321" id="Seg_9438" s="T320">кривой.[NOM.SG]</ta>
            <ta e="T323" id="Seg_9439" s="T322">два.[NOM.SG]</ta>
            <ta e="T324" id="Seg_9440" s="T323">женщина.[NOM.SG]</ta>
            <ta e="T325" id="Seg_9441" s="T324">жить-PST-3PL</ta>
            <ta e="T326" id="Seg_9442" s="T325">пойти-PST-3PL</ta>
            <ta e="T327" id="Seg_9443" s="T326">саранка-NOM/GEN/ACC.3PL</ta>
            <ta e="T329" id="Seg_9444" s="T328">копать-INF.LAT</ta>
            <ta e="T330" id="Seg_9445" s="T329">один.[NOM.SG]</ta>
            <ta e="T331" id="Seg_9446" s="T330">копать-PST.[3SG]</ta>
            <ta e="T332" id="Seg_9447" s="T331">много</ta>
            <ta e="T333" id="Seg_9448" s="T332">а</ta>
            <ta e="T334" id="Seg_9449" s="T333">один.[NOM.SG]</ta>
            <ta e="T335" id="Seg_9450" s="T334">мало</ta>
            <ta e="T336" id="Seg_9451" s="T335">какой</ta>
            <ta e="T337" id="Seg_9452" s="T336">много</ta>
            <ta e="T338" id="Seg_9453" s="T337">копать-PST.[3SG]</ta>
            <ta e="T339" id="Seg_9454" s="T338">а</ta>
            <ta e="T340" id="Seg_9455" s="T339">какой</ta>
            <ta e="T341" id="Seg_9456" s="T340">мало</ta>
            <ta e="T342" id="Seg_9457" s="T341">копать-PST.[3SG]</ta>
            <ta e="T343" id="Seg_9458" s="T342">этот-ACC</ta>
            <ta e="T345" id="Seg_9459" s="T344">топор-INS</ta>
            <ta e="T346" id="Seg_9460" s="T345">голова-NOM/GEN.3SG</ta>
            <ta e="T347" id="Seg_9461" s="T346">резать-PST.[3SG]</ta>
            <ta e="T348" id="Seg_9462" s="T347">тогда</ta>
            <ta e="T349" id="Seg_9463" s="T348">прийти-PST.[3SG]</ta>
            <ta e="T350" id="Seg_9464" s="T349">дом-LAT</ta>
            <ta e="T351" id="Seg_9465" s="T350">девушка.[NOM.SG]</ta>
            <ta e="T352" id="Seg_9466" s="T351">прийти-PST.[3SG]</ta>
            <ta e="T353" id="Seg_9467" s="T352">где</ta>
            <ta e="T354" id="Seg_9468" s="T353">я.GEN</ta>
            <ta e="T355" id="Seg_9469" s="T354">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T356" id="Seg_9470" s="T355">этот.[3SG]</ta>
            <ta e="T357" id="Seg_9471" s="T356">PTCL</ta>
            <ta e="T358" id="Seg_9472" s="T357">ленивый.[NOM.SG]</ta>
            <ta e="T359" id="Seg_9473" s="T358">NEG</ta>
            <ta e="T360" id="Seg_9474" s="T359">копать-PST.[3SG]</ta>
            <ta e="T361" id="Seg_9475" s="T360">саранка-PL</ta>
            <ta e="T362" id="Seg_9476" s="T361">там</ta>
            <ta e="T363" id="Seg_9477" s="T362">PTCL</ta>
            <ta e="T364" id="Seg_9478" s="T363">спать-DUR.[3SG]</ta>
            <ta e="T365" id="Seg_9479" s="T364">а</ta>
            <ta e="T366" id="Seg_9480" s="T365">дочь-NOM/GEN.3SG</ta>
            <ta e="T367" id="Seg_9481" s="T366">как=INDEF</ta>
            <ta e="T368" id="Seg_9482" s="T367">как</ta>
            <ta e="T372" id="Seg_9483" s="T371">знать-PST.[3SG]</ta>
            <ta e="T373" id="Seg_9484" s="T372">и</ta>
            <ta e="T374" id="Seg_9485" s="T373">прийти-PST.[3SG]</ta>
            <ta e="T375" id="Seg_9486" s="T374">сын-NOM/GEN.3SG</ta>
            <ta e="T376" id="Seg_9487" s="T375">быть-PST.[3SG]</ta>
            <ta e="T377" id="Seg_9488" s="T376">этот.[3SG]</ta>
            <ta e="T378" id="Seg_9489" s="T377">этот-ACC</ta>
            <ta e="T379" id="Seg_9490" s="T378">спрятаться-CVB</ta>
            <ta e="T380" id="Seg_9491" s="T379">быть-PST.[3SG]</ta>
            <ta e="T382" id="Seg_9492" s="T381">спать-INF.LAT</ta>
            <ta e="T383" id="Seg_9493" s="T382">класть-PST.[3SG]</ta>
            <ta e="T384" id="Seg_9494" s="T383">а</ta>
            <ta e="T385" id="Seg_9495" s="T384">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T386" id="Seg_9496" s="T385">вверх</ta>
            <ta e="T389" id="Seg_9497" s="T388">влезать-PST.[3SG]</ta>
            <ta e="T390" id="Seg_9498" s="T389">дом-LAT</ta>
            <ta e="T392" id="Seg_9499" s="T391">взять-PST.[3SG]</ta>
            <ta e="T393" id="Seg_9500" s="T392">тогда</ta>
            <ta e="T395" id="Seg_9501" s="T394">этот</ta>
            <ta e="T396" id="Seg_9502" s="T395">женщина.[NOM.SG]</ta>
            <ta e="T397" id="Seg_9503" s="T396">прийти-PST.[3SG]</ta>
            <ta e="T398" id="Seg_9504" s="T397">где</ta>
            <ta e="T399" id="Seg_9505" s="T398">вы.NOM</ta>
            <ta e="T401" id="Seg_9506" s="T400">этот.[3SG]</ta>
            <ta e="T402" id="Seg_9507" s="T401">сказать-IPFVZ.[3SG]</ta>
            <ta e="T403" id="Seg_9508" s="T402">я.NOM</ta>
            <ta e="T404" id="Seg_9509" s="T403">здесь</ta>
            <ta e="T405" id="Seg_9510" s="T404">жить-DUR-1SG</ta>
            <ta e="T406" id="Seg_9511" s="T405">этот.[3SG]</ta>
            <ta e="T410" id="Seg_9512" s="T409">лить-PST.[3SG]</ta>
            <ta e="T411" id="Seg_9513" s="T410">глаз-LAT/LOC.3SG</ta>
            <ta e="T413" id="Seg_9514" s="T412">что.[NOM.SG]=INDEF</ta>
            <ta e="T414" id="Seg_9515" s="T413">NEG</ta>
            <ta e="T415" id="Seg_9516" s="T414">смотреть-PRS-3PL</ta>
            <ta e="T416" id="Seg_9517" s="T415">тогда</ta>
            <ta e="T417" id="Seg_9518" s="T416">глаз-NOM/GEN.3SG</ta>
            <ta e="T418" id="Seg_9519" s="T417">что.[NOM.SG]=INDEF</ta>
            <ta e="T419" id="Seg_9520" s="T418">NEG</ta>
            <ta e="T420" id="Seg_9521" s="T419">мочь-PRS-3PL</ta>
            <ta e="T421" id="Seg_9522" s="T420">этот.[3SG]</ta>
            <ta e="T422" id="Seg_9523" s="T421">прыгнуть-MOM-PST.[3SG]</ta>
            <ta e="T423" id="Seg_9524" s="T422">дом-LAT</ta>
            <ta e="T424" id="Seg_9525" s="T423">брат-NOM/GEN.3SG</ta>
            <ta e="T425" id="Seg_9526" s="T424">взять-PST.[3SG]</ta>
            <ta e="T426" id="Seg_9527" s="T425">и</ta>
            <ta e="T1023" id="Seg_9528" s="T426">пойти-CVB</ta>
            <ta e="T427" id="Seg_9529" s="T1023">исчезнуть-PST.[3SG]</ta>
            <ta e="T428" id="Seg_9530" s="T427">где</ta>
            <ta e="T429" id="Seg_9531" s="T428">люди.[NOM.SG]</ta>
            <ta e="T430" id="Seg_9532" s="T429">много</ta>
            <ta e="T432" id="Seg_9533" s="T431">я.GEN</ta>
            <ta e="T433" id="Seg_9534" s="T432">мужчина.[NOM.SG]</ta>
            <ta e="T1024" id="Seg_9535" s="T433">пойти-CVB</ta>
            <ta e="T434" id="Seg_9536" s="T1024">исчезнуть-PST.[3SG]</ta>
            <ta e="T435" id="Seg_9537" s="T434">я.NOM</ta>
            <ta e="T436" id="Seg_9538" s="T435">очень</ta>
            <ta e="T437" id="Seg_9539" s="T436">сильно</ta>
            <ta e="T438" id="Seg_9540" s="T437">плакать-PST-1SG</ta>
            <ta e="T439" id="Seg_9541" s="T438">сердце-NOM/GEN/ACC.1SG</ta>
            <ta e="T440" id="Seg_9542" s="T439">PTCL</ta>
            <ta e="T441" id="Seg_9543" s="T440">болеть-PST.[3SG]</ta>
            <ta e="T442" id="Seg_9544" s="T441">и</ta>
            <ta e="T443" id="Seg_9545" s="T442">голова-NOM/GEN/ACC.1SG</ta>
            <ta e="T447" id="Seg_9546" s="T446">я.GEN</ta>
            <ta e="T449" id="Seg_9547" s="T448">сестра-NOM/GEN/ACC.1SG</ta>
            <ta e="T450" id="Seg_9548" s="T449">мальчик.[NOM.SG]</ta>
            <ta e="T451" id="Seg_9549" s="T450">сын-NOM/GEN.3SG</ta>
            <ta e="T452" id="Seg_9550" s="T451">мельница-NOM/GEN/ACC.3SG</ta>
            <ta e="T453" id="Seg_9551" s="T452">идти-PST.[3SG]</ta>
            <ta e="T454" id="Seg_9552" s="T453">хлеб.[NOM.SG]</ta>
            <ta e="T456" id="Seg_9553" s="T455">мельница-LAT</ta>
            <ta e="T457" id="Seg_9554" s="T456">дать-PST.[3SG]</ta>
            <ta e="T458" id="Seg_9555" s="T457">мука.[NOM.SG]</ta>
            <ta e="T459" id="Seg_9556" s="T458">молоть-INF.LAT</ta>
            <ta e="T460" id="Seg_9557" s="T459">тогда</ta>
            <ta e="T461" id="Seg_9558" s="T460">дом-LAT/LOC.3SG</ta>
            <ta e="T462" id="Seg_9559" s="T461">прийти-PST.[3SG]</ta>
            <ta e="T463" id="Seg_9560" s="T462">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T464" id="Seg_9561" s="T463">женщина-LAT</ta>
            <ta e="T465" id="Seg_9562" s="T464">сказать-PRS.[3SG]</ta>
            <ta e="T466" id="Seg_9563" s="T465">чтобы</ta>
            <ta e="T467" id="Seg_9564" s="T466">долго</ta>
            <ta e="T469" id="Seg_9565" s="T468">хватать-FUT-3SG</ta>
            <ta e="T470" id="Seg_9566" s="T469">а</ta>
            <ta e="T471" id="Seg_9567" s="T470">я.NOM</ta>
            <ta e="T472" id="Seg_9568" s="T471">сидеть-PRS-1SG</ta>
            <ta e="T473" id="Seg_9569" s="T472">и</ta>
            <ta e="T474" id="Seg_9570" s="T473">думать-PRS-1SG</ta>
            <ta e="T476" id="Seg_9571" s="T475">что</ta>
            <ta e="T477" id="Seg_9572" s="T476">я.NOM</ta>
            <ta e="T479" id="Seg_9573" s="T478">этот-PL-COM</ta>
            <ta e="T480" id="Seg_9574" s="T479">есть-INF.LAT</ta>
            <ta e="T483" id="Seg_9575" s="T482">то</ta>
            <ta e="T484" id="Seg_9576" s="T483">идти-PST-3PL</ta>
            <ta e="T485" id="Seg_9577" s="T484">тело-NOM/GEN/ACC.3SG</ta>
            <ta e="T486" id="Seg_9578" s="T485">я.NOM</ta>
            <ta e="T487" id="Seg_9579" s="T486">много</ta>
            <ta e="T488" id="Seg_9580" s="T487">съесть-PRS-1SG</ta>
            <ta e="T490" id="Seg_9581" s="T489">один.[NOM.SG]</ta>
            <ta e="T491" id="Seg_9582" s="T490">мужчина.[NOM.SG]</ta>
            <ta e="T492" id="Seg_9583" s="T491">жениться-INF.LAT</ta>
            <ta e="T494" id="Seg_9584" s="T493">пойти-PST.[3SG]</ta>
            <ta e="T495" id="Seg_9585" s="T494">красивый.[NOM.SG]</ta>
            <ta e="T496" id="Seg_9586" s="T495">девушка.[NOM.SG]</ta>
            <ta e="T497" id="Seg_9587" s="T496">очень</ta>
            <ta e="T498" id="Seg_9588" s="T497">одежда-NOM/GEN.3SG</ta>
            <ta e="T499" id="Seg_9589" s="T498">много</ta>
            <ta e="T500" id="Seg_9590" s="T499">идти-PRS.[3SG]</ta>
            <ta e="T501" id="Seg_9591" s="T500">тогда</ta>
            <ta e="T502" id="Seg_9592" s="T501">мужчина.[NOM.SG]</ta>
            <ta e="T503" id="Seg_9593" s="T502">этот-LAT</ta>
            <ta e="T504" id="Seg_9594" s="T503">сказать-IPFVZ.[3SG]</ta>
            <ta e="T505" id="Seg_9595" s="T504">куда</ta>
            <ta e="T506" id="Seg_9596" s="T505">идти-PRS-2SG</ta>
            <ta e="T507" id="Seg_9597" s="T506">жениться-INF.LAT</ta>
            <ta e="T508" id="Seg_9598" s="T507">взять-IMP.2SG</ta>
            <ta e="T509" id="Seg_9599" s="T508">я.ACC</ta>
            <ta e="T510" id="Seg_9600" s="T509">а</ta>
            <ta e="T511" id="Seg_9601" s="T510">ты.NOM</ta>
            <ta e="T512" id="Seg_9602" s="T511">что.[NOM.SG]</ta>
            <ta e="T513" id="Seg_9603" s="T512">делать-PRS-2SG</ta>
            <ta e="T514" id="Seg_9604" s="T513">есть-INF.LAT</ta>
            <ta e="T515" id="Seg_9605" s="T514">и</ta>
            <ta e="T516" id="Seg_9606" s="T515">испражняться-INF.LAT</ta>
            <ta e="T517" id="Seg_9607" s="T516">тогда</ta>
            <ta e="T518" id="Seg_9608" s="T517">пойти-PST-3PL</ta>
            <ta e="T519" id="Seg_9609" s="T518">опять</ta>
            <ta e="T520" id="Seg_9610" s="T519">опять</ta>
            <ta e="T521" id="Seg_9611" s="T520">мужчина.[NOM.SG]</ta>
            <ta e="T522" id="Seg_9612" s="T521">сказать-IPFVZ.[3SG]</ta>
            <ta e="T523" id="Seg_9613" s="T522">куда</ta>
            <ta e="T524" id="Seg_9614" s="T523">идти-PRS-2PL</ta>
            <ta e="T525" id="Seg_9615" s="T524">жениться-INF.LAT</ta>
            <ta e="T526" id="Seg_9616" s="T525">я.ACC</ta>
            <ta e="T527" id="Seg_9617" s="T526">взять-IMP.2SG.O</ta>
            <ta e="T528" id="Seg_9618" s="T527">а</ta>
            <ta e="T529" id="Seg_9619" s="T528">ты.NOM</ta>
            <ta e="T530" id="Seg_9620" s="T529">что.[NOM.SG]</ta>
            <ta e="T531" id="Seg_9621" s="T530">делать-FUT-2SG</ta>
            <ta e="T532" id="Seg_9622" s="T531">пить-INF.LAT</ta>
            <ta e="T533" id="Seg_9623" s="T532">и</ta>
            <ta e="T534" id="Seg_9624" s="T533">мочиться-INF.LAT</ta>
            <ta e="T535" id="Seg_9625" s="T534">тогда</ta>
            <ta e="T536" id="Seg_9626" s="T535">прийти-PST-3PL</ta>
            <ta e="T537" id="Seg_9627" s="T536">и</ta>
            <ta e="T538" id="Seg_9628" s="T537">INCH</ta>
            <ta e="T539" id="Seg_9629" s="T538">жениться-INF.LAT</ta>
            <ta e="T540" id="Seg_9630" s="T539">этот.[NOM.SG]</ta>
            <ta e="T541" id="Seg_9631" s="T540">сказать-IPFVZ.[3SG]</ta>
            <ta e="T542" id="Seg_9632" s="T541">я.NOM</ta>
            <ta e="T543" id="Seg_9633" s="T542">много</ta>
            <ta e="T544" id="Seg_9634" s="T543">PTCL</ta>
            <ta e="T545" id="Seg_9635" s="T544">делать-FUT-1SG</ta>
            <ta e="T546" id="Seg_9636" s="T545">хлеб.[NOM.SG]</ta>
            <ta e="T547" id="Seg_9637" s="T546">печь-FUT-1SG</ta>
            <ta e="T548" id="Seg_9638" s="T547">мясо.[NOM.SG]</ta>
            <ta e="T549" id="Seg_9639" s="T548">весь</ta>
            <ta e="T550" id="Seg_9640" s="T549">съесть-INF.LAT</ta>
            <ta e="T551" id="Seg_9641" s="T550">надо</ta>
            <ta e="T552" id="Seg_9642" s="T551">этот.[3SG]</ta>
            <ta e="T553" id="Seg_9643" s="T552">сказать-IPFVZ.[3SG]</ta>
            <ta e="T554" id="Seg_9644" s="T553">ну</ta>
            <ta e="T555" id="Seg_9645" s="T554">весь</ta>
            <ta e="T556" id="Seg_9646" s="T555">я.NOM</ta>
            <ta e="T557" id="Seg_9647" s="T556">съесть-FUT-1SG</ta>
            <ta e="T558" id="Seg_9648" s="T557">и</ta>
            <ta e="T561" id="Seg_9649" s="T560">много</ta>
            <ta e="T562" id="Seg_9650" s="T561">десять.[NOM.SG]</ta>
            <ta e="T563" id="Seg_9651" s="T562">четыре.[NOM.SG]</ta>
            <ta e="T565" id="Seg_9652" s="T564">делать-FUT-1SG</ta>
            <ta e="T566" id="Seg_9653" s="T565">ну</ta>
            <ta e="T567" id="Seg_9654" s="T566">тогда</ta>
            <ta e="T569" id="Seg_9655" s="T568">стол-NOM/GEN/ACC.3SG</ta>
            <ta e="T570" id="Seg_9656" s="T569">собирать-PST.[3SG]</ta>
            <ta e="T571" id="Seg_9657" s="T570">тогда</ta>
            <ta e="T572" id="Seg_9658" s="T571">прийти-PST.[3SG]</ta>
            <ta e="T573" id="Seg_9659" s="T572">этот.[NOM.SG]</ta>
            <ta e="T574" id="Seg_9660" s="T573">мужчина.[NOM.SG]</ta>
            <ta e="T575" id="Seg_9661" s="T574">какой</ta>
            <ta e="T576" id="Seg_9662" s="T575">есть-INF.LAT</ta>
            <ta e="T577" id="Seg_9663" s="T576">и</ta>
            <ta e="T578" id="Seg_9664" s="T577">испражняться-INF.LAT</ta>
            <ta e="T579" id="Seg_9665" s="T578">PTCL</ta>
            <ta e="T580" id="Seg_9666" s="T579">съесть-DUR.[3SG]</ta>
            <ta e="T581" id="Seg_9667" s="T580">и</ta>
            <ta e="T582" id="Seg_9668" s="T581">испражняться-DUR.[3SG]</ta>
            <ta e="T583" id="Seg_9669" s="T582">съесть-DUR.[3SG]</ta>
            <ta e="T584" id="Seg_9670" s="T583">и</ta>
            <ta e="T585" id="Seg_9671" s="T584">испражняться-DUR.[3SG]</ta>
            <ta e="T586" id="Seg_9672" s="T585">весь</ta>
            <ta e="T587" id="Seg_9673" s="T586">что.[NOM.SG]</ta>
            <ta e="T588" id="Seg_9674" s="T587">съесть-MOM-PST.[3SG]</ta>
            <ta e="T589" id="Seg_9675" s="T588">и</ta>
            <ta e="T590" id="Seg_9676" s="T589">мясо.[NOM.SG]</ta>
            <ta e="T591" id="Seg_9677" s="T590">и</ta>
            <ta e="T593" id="Seg_9678" s="T592">что.[NOM.SG]</ta>
            <ta e="T594" id="Seg_9679" s="T593">быть-PST.[3SG]</ta>
            <ta e="T595" id="Seg_9680" s="T594">весь</ta>
            <ta e="T596" id="Seg_9681" s="T595">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T597" id="Seg_9682" s="T596">сказать-IPFVZ.[3SG]</ta>
            <ta e="T598" id="Seg_9683" s="T597">принести-IMP.2PL</ta>
            <ta e="T599" id="Seg_9684" s="T598">еще</ta>
            <ta e="T600" id="Seg_9685" s="T599">мало</ta>
            <ta e="T601" id="Seg_9686" s="T600">принести-IMP.2PL</ta>
            <ta e="T602" id="Seg_9687" s="T601">еще</ta>
            <ta e="T603" id="Seg_9688" s="T602">мало</ta>
            <ta e="T604" id="Seg_9689" s="T603">тогда</ta>
            <ta e="T605" id="Seg_9690" s="T604">PTCL</ta>
            <ta e="T606" id="Seg_9691" s="T605">съесть-INF.LAT</ta>
            <ta e="T612" id="Seg_9692" s="T611">тогда</ta>
            <ta e="T613" id="Seg_9693" s="T612">вода.[NOM.SG]</ta>
            <ta e="T615" id="Seg_9694" s="T614">пить-INF.LAT</ta>
            <ta e="T616" id="Seg_9695" s="T615">опять</ta>
            <ta e="T617" id="Seg_9696" s="T616">опять</ta>
            <ta e="T618" id="Seg_9697" s="T617">прийти-PST.[3SG]</ta>
            <ta e="T619" id="Seg_9698" s="T618">этот</ta>
            <ta e="T621" id="Seg_9699" s="T620">этот</ta>
            <ta e="T622" id="Seg_9700" s="T621">мужчина.[NOM.SG]</ta>
            <ta e="T623" id="Seg_9701" s="T622">тогда</ta>
            <ta e="T624" id="Seg_9702" s="T623">этот.[3SG]</ta>
            <ta e="T626" id="Seg_9703" s="T625">пить-DUR.[3SG]</ta>
            <ta e="T627" id="Seg_9704" s="T626">пить-DUR.[3SG]</ta>
            <ta e="T628" id="Seg_9705" s="T627">и</ta>
            <ta e="T629" id="Seg_9706" s="T628">мочиться-DUR.[3SG]</ta>
            <ta e="T630" id="Seg_9707" s="T629">пить-DUR.[3SG]</ta>
            <ta e="T631" id="Seg_9708" s="T630">и</ta>
            <ta e="T632" id="Seg_9709" s="T631">мочиться-DUR.[3SG]</ta>
            <ta e="T633" id="Seg_9710" s="T632">весь</ta>
            <ta e="T634" id="Seg_9711" s="T633">пить-MOM-PST.[3SG]</ta>
            <ta e="T635" id="Seg_9712" s="T634">и</ta>
            <ta e="T636" id="Seg_9713" s="T635">кричать-DUR.[3SG]</ta>
            <ta e="T637" id="Seg_9714" s="T636">еще</ta>
            <ta e="T639" id="Seg_9715" s="T638">принести-IMP.2SG</ta>
            <ta e="T640" id="Seg_9716" s="T639">я.LAT</ta>
            <ta e="T641" id="Seg_9717" s="T640">мало</ta>
            <ta e="T642" id="Seg_9718" s="T641">а</ta>
            <ta e="T643" id="Seg_9719" s="T642">снаружи-LAT</ta>
            <ta e="T644" id="Seg_9720" s="T643">PTCL</ta>
            <ta e="T645" id="Seg_9721" s="T644">очень</ta>
            <ta e="T646" id="Seg_9722" s="T645">вода.[NOM.SG]</ta>
            <ta e="T647" id="Seg_9723" s="T646">много</ta>
            <ta e="T649" id="Seg_9724" s="T648">тогда</ta>
            <ta e="T650" id="Seg_9725" s="T649">NEG</ta>
            <ta e="T651" id="Seg_9726" s="T650">съесть-PST.[3SG]</ta>
            <ta e="T652" id="Seg_9727" s="T651">и</ta>
            <ta e="T653" id="Seg_9728" s="T652">вода.[NOM.SG]</ta>
            <ta e="T654" id="Seg_9729" s="T653">NEG</ta>
            <ta e="T655" id="Seg_9730" s="T654">пить-PST.[3SG]</ta>
            <ta e="T656" id="Seg_9731" s="T655">этот-LAT</ta>
            <ta e="T657" id="Seg_9732" s="T656">IRREAL</ta>
            <ta e="T658" id="Seg_9733" s="T657">голова-ACC.3SG</ta>
            <ta e="T659" id="Seg_9734" s="T658">от-резать-PST-3PL</ta>
            <ta e="T660" id="Seg_9735" s="T659">IRREAL</ta>
            <ta e="T661" id="Seg_9736" s="T660">и</ta>
            <ta e="T662" id="Seg_9737" s="T661">вешать-PST-3PL</ta>
            <ta e="T664" id="Seg_9738" s="T662">а.то</ta>
            <ta e="T665" id="Seg_9739" s="T664">тогда</ta>
            <ta e="T666" id="Seg_9740" s="T665">этот</ta>
            <ta e="T667" id="Seg_9741" s="T666">дочь-ACC</ta>
            <ta e="T668" id="Seg_9742" s="T667">взять-PST.[3SG]</ta>
            <ta e="T669" id="Seg_9743" s="T668">и</ta>
            <ta e="T670" id="Seg_9744" s="T669">дом-LAT/LOC.3SG</ta>
            <ta e="T671" id="Seg_9745" s="T670">пойти-PST.[3SG]</ta>
            <ta e="T673" id="Seg_9746" s="T672">%%</ta>
            <ta e="T674" id="Seg_9747" s="T673">что.[NOM.SG]</ta>
            <ta e="T675" id="Seg_9748" s="T674">петь-DUR-2SG</ta>
            <ta e="T677" id="Seg_9749" s="T676">а</ta>
            <ta e="T678" id="Seg_9750" s="T677">что.[NOM.SG]</ta>
            <ta e="T679" id="Seg_9751" s="T678">NEG</ta>
            <ta e="T680" id="Seg_9752" s="T679">хороший</ta>
            <ta e="T681" id="Seg_9753" s="T680">петь-DUR-2SG</ta>
            <ta e="T682" id="Seg_9754" s="T681">да</ta>
            <ta e="T684" id="Seg_9755" s="T683">хороший</ta>
            <ta e="T686" id="Seg_9756" s="T685">петь-INF.LAT</ta>
            <ta e="T687" id="Seg_9757" s="T686">видеть-PRS-2SG</ta>
            <ta e="T688" id="Seg_9758" s="T687">%%</ta>
            <ta e="T690" id="Seg_9759" s="T689">один.[NOM.SG]</ta>
            <ta e="T691" id="Seg_9760" s="T690">мужчина.[NOM.SG]</ta>
            <ta e="T693" id="Seg_9761" s="T692">быть-PST.[3SG]</ta>
            <ta e="T694" id="Seg_9762" s="T693">мельница-NOM/GEN/ACC.3SG</ta>
            <ta e="T695" id="Seg_9763" s="T694">а</ta>
            <ta e="T696" id="Seg_9764" s="T695">а</ta>
            <ta e="T697" id="Seg_9765" s="T696">один.[NOM.SG]</ta>
            <ta e="T698" id="Seg_9766" s="T697">мужчина.[NOM.SG]</ta>
            <ta e="T699" id="Seg_9767" s="T698">очень</ta>
            <ta e="T700" id="Seg_9768" s="T699">деньги.[NOM.SG]</ta>
            <ta e="T701" id="Seg_9769" s="T700">много</ta>
            <ta e="T703" id="Seg_9770" s="T702">быть-PST.[3SG]</ta>
            <ta e="T704" id="Seg_9771" s="T703">и</ta>
            <ta e="T705" id="Seg_9772" s="T704">одежда.[NOM.SG]</ta>
            <ta e="T706" id="Seg_9773" s="T705">и</ta>
            <ta e="T707" id="Seg_9774" s="T706">хлеб.[NOM.SG]</ta>
            <ta e="T708" id="Seg_9775" s="T707">этот</ta>
            <ta e="T709" id="Seg_9776" s="T708">этот-GEN</ta>
            <ta e="T710" id="Seg_9777" s="T709">мужчина-LAT</ta>
            <ta e="T711" id="Seg_9778" s="T710">взять-PST.[3SG]</ta>
            <ta e="T712" id="Seg_9779" s="T711">мельница-NOM/GEN/ACC.3SG</ta>
            <ta e="T713" id="Seg_9780" s="T712">и</ta>
            <ta e="T715" id="Seg_9781" s="T714">унести-RES-PST.[3SG]</ta>
            <ta e="T716" id="Seg_9782" s="T715">этот.[3SG]</ta>
            <ta e="T717" id="Seg_9783" s="T716">PTCL</ta>
            <ta e="T718" id="Seg_9784" s="T717">плакать-DUR.[3SG]</ta>
            <ta e="T719" id="Seg_9785" s="T718">а</ta>
            <ta e="T720" id="Seg_9786" s="T719">петух.[NOM.SG]</ta>
            <ta e="T721" id="Seg_9787" s="T720">сказать-PRS.[3SG]</ta>
            <ta e="T722" id="Seg_9788" s="T721">что.[NOM.SG]</ta>
            <ta e="T723" id="Seg_9789" s="T722">плакать-DUR-2SG</ta>
            <ta e="T724" id="Seg_9790" s="T723">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T725" id="Seg_9791" s="T724">скоро</ta>
            <ta e="T726" id="Seg_9792" s="T725">взять-1DU</ta>
            <ta e="T727" id="Seg_9793" s="T726">мельница-NOM/GEN/ACC.3SG</ta>
            <ta e="T728" id="Seg_9794" s="T727">этот.[NOM.SG]</ta>
            <ta e="T729" id="Seg_9795" s="T728">%%</ta>
            <ta e="T730" id="Seg_9796" s="T729">пойти-PST-3PL</ta>
            <ta e="T731" id="Seg_9797" s="T730">петух.[NOM.SG]</ta>
            <ta e="T732" id="Seg_9798" s="T731">кричать-DUR.[3SG]</ta>
            <ta e="T733" id="Seg_9799" s="T732">принести-IMP.2SG</ta>
            <ta e="T734" id="Seg_9800" s="T733">мельница.[NOM.SG]</ta>
            <ta e="T735" id="Seg_9801" s="T734">принести-IMP.2SG</ta>
            <ta e="T736" id="Seg_9802" s="T735">мельница.[NOM.SG]</ta>
            <ta e="T738" id="Seg_9803" s="T737">этот</ta>
            <ta e="T739" id="Seg_9804" s="T738">мужчина.[NOM.SG]</ta>
            <ta e="T740" id="Seg_9805" s="T739">сказать-IPFVZ.[3SG]</ta>
            <ta e="T741" id="Seg_9806" s="T740">выбросить-IMP.2SG.O</ta>
            <ta e="T742" id="Seg_9807" s="T741">этот-ACC</ta>
            <ta e="T743" id="Seg_9808" s="T742">лошадь-INS</ta>
            <ta e="T744" id="Seg_9809" s="T743">лошадь-3PL-LAT</ta>
            <ta e="T745" id="Seg_9810" s="T744">выбросить-MOM-PST.[3SG]</ta>
            <ta e="T746" id="Seg_9811" s="T745">этот.[3SG]</ta>
            <ta e="T747" id="Seg_9812" s="T746">оттуда</ta>
            <ta e="T748" id="Seg_9813" s="T747">прийти-PST.[3SG]</ta>
            <ta e="T749" id="Seg_9814" s="T748">опять</ta>
            <ta e="T750" id="Seg_9815" s="T749">кричать-DUR.[3SG]</ta>
            <ta e="T752" id="Seg_9816" s="T751">принести-IMP.2SG.O</ta>
            <ta e="T753" id="Seg_9817" s="T752">мельница-NOM/GEN/ACC.3SG</ta>
            <ta e="T754" id="Seg_9818" s="T753">принести-IMP.2SG.O</ta>
            <ta e="T755" id="Seg_9819" s="T754">мельница-NOM/GEN/ACC.3SG</ta>
            <ta e="T756" id="Seg_9820" s="T755">тогда</ta>
            <ta e="T757" id="Seg_9821" s="T756">выбросить-IMP.2SG.O</ta>
            <ta e="T759" id="Seg_9822" s="T758">этот-ACC</ta>
            <ta e="T760" id="Seg_9823" s="T759">корова-LAT</ta>
            <ta e="T761" id="Seg_9824" s="T760">выбросить-MOM-PST.[3SG]</ta>
            <ta e="T762" id="Seg_9825" s="T761">корова-LAT</ta>
            <ta e="T763" id="Seg_9826" s="T762">этот.[3SG]</ta>
            <ta e="T764" id="Seg_9827" s="T763">корова-LOC</ta>
            <ta e="T766" id="Seg_9828" s="T765">опять</ta>
            <ta e="T767" id="Seg_9829" s="T766">прийти-PST.[3SG]</ta>
            <ta e="T768" id="Seg_9830" s="T767">тогда</ta>
            <ta e="T769" id="Seg_9831" s="T768">опять</ta>
            <ta e="T770" id="Seg_9832" s="T769">сказать-IPFVZ.[3SG]</ta>
            <ta e="T773" id="Seg_9833" s="T772">принести-IMP.2SG.O</ta>
            <ta e="T774" id="Seg_9834" s="T773">мельница-NOM/GEN/ACC.3SG</ta>
            <ta e="T775" id="Seg_9835" s="T774">принести-IMP.2SG.O</ta>
            <ta e="T777" id="Seg_9836" s="T775">мельница-NOM/GEN/ACC.3SG</ta>
            <ta e="T778" id="Seg_9837" s="T777">выбросить-IMP.2SG.O</ta>
            <ta e="T779" id="Seg_9838" s="T778">овца-LAT</ta>
            <ta e="T780" id="Seg_9839" s="T779">выбросить-MOM-PST.[3SG]</ta>
            <ta e="T781" id="Seg_9840" s="T780">овца-LAT</ta>
            <ta e="T782" id="Seg_9841" s="T781">тогда</ta>
            <ta e="T783" id="Seg_9842" s="T782">опять</ta>
            <ta e="T784" id="Seg_9843" s="T783">прийти-PST.[3SG]</ta>
            <ta e="T785" id="Seg_9844" s="T784">опять</ta>
            <ta e="T786" id="Seg_9845" s="T785">кричать-DUR.[3SG]</ta>
            <ta e="T787" id="Seg_9846" s="T786">принести-IMP.2SG.O</ta>
            <ta e="T788" id="Seg_9847" s="T787">мельница-NOM/GEN/ACC.3SG</ta>
            <ta e="T789" id="Seg_9848" s="T788">принести-IMP.2SG.O</ta>
            <ta e="T790" id="Seg_9849" s="T789">мельница-NOM/GEN/ACC.3SG</ta>
            <ta e="T791" id="Seg_9850" s="T790">тогда</ta>
            <ta e="T792" id="Seg_9851" s="T791">сказать-IPFVZ.[3SG]</ta>
            <ta e="T793" id="Seg_9852" s="T792">выбросить-IMP.2SG.O</ta>
            <ta e="T794" id="Seg_9853" s="T793">этот-ACC</ta>
            <ta e="T795" id="Seg_9854" s="T794">вода-LAT</ta>
            <ta e="T796" id="Seg_9855" s="T795">этот-ACC</ta>
            <ta e="T797" id="Seg_9856" s="T796">выбросить-MOM-PST.[3SG]</ta>
            <ta e="T798" id="Seg_9857" s="T797">а</ta>
            <ta e="T799" id="Seg_9858" s="T798">этот.[3SG]</ta>
            <ta e="T800" id="Seg_9859" s="T799">сказать-IPFVZ.[3SG]</ta>
            <ta e="T802" id="Seg_9860" s="T800">зад-NOM/GEN/ACC.1SG</ta>
            <ta e="T804" id="Seg_9861" s="T803">выбросить-MOM-PST.[3SG]</ta>
            <ta e="T805" id="Seg_9862" s="T804">этот.[3SG]</ta>
            <ta e="T806" id="Seg_9863" s="T805">этот-ACC</ta>
            <ta e="T807" id="Seg_9864" s="T806">вода-LAT</ta>
            <ta e="T808" id="Seg_9865" s="T807">а</ta>
            <ta e="T809" id="Seg_9866" s="T808">этот.[NOM.SG]</ta>
            <ta e="T810" id="Seg_9867" s="T809">кричать-DUR.[3SG]</ta>
            <ta e="T811" id="Seg_9868" s="T810">зад-NOM/GEN/ACC.1SG</ta>
            <ta e="T812" id="Seg_9869" s="T811">пить-EP-IMP.2SG</ta>
            <ta e="T813" id="Seg_9870" s="T812">вода.[NOM.SG]</ta>
            <ta e="T814" id="Seg_9871" s="T813">зад-NOM/GEN/ACC.1SG</ta>
            <ta e="T815" id="Seg_9872" s="T814">пить-EP-IMP.2SG</ta>
            <ta e="T816" id="Seg_9873" s="T815">вода.[NOM.SG]</ta>
            <ta e="T817" id="Seg_9874" s="T816">зад-NOM/GEN/ACC.1SG</ta>
            <ta e="T818" id="Seg_9875" s="T817">весь</ta>
            <ta e="T819" id="Seg_9876" s="T818">вода.[NOM.SG]</ta>
            <ta e="T820" id="Seg_9877" s="T819">пить-MOM-PST.[3SG]</ta>
            <ta e="T821" id="Seg_9878" s="T820">опять</ta>
            <ta e="T822" id="Seg_9879" s="T821">прийти-PST.[3SG]</ta>
            <ta e="T823" id="Seg_9880" s="T822">принести-IMP.2SG.O</ta>
            <ta e="T824" id="Seg_9881" s="T823">мельница-NOM/GEN/ACC.3SG</ta>
            <ta e="T825" id="Seg_9882" s="T824">принести-IMP.2SG.O</ta>
            <ta e="T826" id="Seg_9883" s="T825">мельница-NOM/GEN/ACC.3SG</ta>
            <ta e="T827" id="Seg_9884" s="T826">тогда</ta>
            <ta e="T828" id="Seg_9885" s="T827">этот</ta>
            <ta e="T829" id="Seg_9886" s="T828">мужчина.[NOM.SG]</ta>
            <ta e="T830" id="Seg_9887" s="T829">сказать-IPFVZ.[3SG]</ta>
            <ta e="T831" id="Seg_9888" s="T830">огонь.[NOM.SG]</ta>
            <ta e="T833" id="Seg_9889" s="T832">класть-IMP.2PL</ta>
            <ta e="T834" id="Seg_9890" s="T833">большой.[NOM.SG]</ta>
            <ta e="T835" id="Seg_9891" s="T834">и</ta>
            <ta e="T836" id="Seg_9892" s="T835">выбросить-IMP.2SG.O</ta>
            <ta e="T837" id="Seg_9893" s="T836">этот-ACC</ta>
            <ta e="T838" id="Seg_9894" s="T837">огонь-LAT</ta>
            <ta e="T839" id="Seg_9895" s="T838">тогда</ta>
            <ta e="T840" id="Seg_9896" s="T839">класть-PST-3PL</ta>
            <ta e="T841" id="Seg_9897" s="T840">огонь.[NOM.SG]</ta>
            <ta e="T842" id="Seg_9898" s="T841">выбросить-MOM-PST.[3SG]</ta>
            <ta e="T843" id="Seg_9899" s="T842">этот</ta>
            <ta e="T844" id="Seg_9900" s="T843">петух-ACC</ta>
            <ta e="T845" id="Seg_9901" s="T844">огонь-LAT</ta>
            <ta e="T846" id="Seg_9902" s="T845">там</ta>
            <ta e="T847" id="Seg_9903" s="T846">сказать-IPFVZ.[3SG]</ta>
            <ta e="T848" id="Seg_9904" s="T847">лить-IMP.2SG</ta>
            <ta e="T849" id="Seg_9905" s="T848">зад-NOM/GEN/ACC.1SG</ta>
            <ta e="T850" id="Seg_9906" s="T849">вода.[NOM.SG]</ta>
            <ta e="T851" id="Seg_9907" s="T850">лить-IMP.2SG</ta>
            <ta e="T852" id="Seg_9908" s="T851">зад-NOM/GEN/ACC.1SG</ta>
            <ta e="T853" id="Seg_9909" s="T852">вода.[NOM.SG]</ta>
            <ta e="T854" id="Seg_9910" s="T853">как</ta>
            <ta e="T855" id="Seg_9911" s="T854">вода.[NOM.SG]</ta>
            <ta e="T856" id="Seg_9912" s="T855">пойти-PST.[3SG]</ta>
            <ta e="T857" id="Seg_9913" s="T856">весь</ta>
            <ta e="T858" id="Seg_9914" s="T857">что.[NOM.SG]</ta>
            <ta e="T860" id="Seg_9915" s="T858">PTCL</ta>
            <ta e="T862" id="Seg_9916" s="T861">тогда</ta>
            <ta e="T863" id="Seg_9917" s="T862">вода.[NOM.SG]</ta>
            <ta e="T864" id="Seg_9918" s="T863">очень</ta>
            <ta e="T865" id="Seg_9919" s="T864">много</ta>
            <ta e="T867" id="Seg_9920" s="T866">течь-PST.[3SG]</ta>
            <ta e="T868" id="Seg_9921" s="T867">этот</ta>
            <ta e="T869" id="Seg_9922" s="T868">мужчина.[NOM.SG]</ta>
            <ta e="T870" id="Seg_9923" s="T869">сказать-IPFVZ.[3SG]</ta>
            <ta e="T871" id="Seg_9924" s="T870">взять-IMP.2SG</ta>
            <ta e="T872" id="Seg_9925" s="T871">мельница-NOM/GEN/ACC.3SG</ta>
            <ta e="T873" id="Seg_9926" s="T872">и</ta>
            <ta e="T874" id="Seg_9927" s="T873">пойти-EP-IMP.2SG</ta>
            <ta e="T875" id="Seg_9928" s="T874">отсюда</ta>
            <ta e="T876" id="Seg_9929" s="T875">отсюда</ta>
            <ta e="T877" id="Seg_9930" s="T876">сюда</ta>
            <ta e="T881" id="Seg_9931" s="T880">%%-PL</ta>
            <ta e="T882" id="Seg_9932" s="T881">один.[NOM.SG]</ta>
            <ta e="T884" id="Seg_9933" s="T883">стоять-PST-3PL</ta>
            <ta e="T885" id="Seg_9934" s="T884">я.NOM</ta>
            <ta e="T886" id="Seg_9935" s="T885">этот-PL</ta>
            <ta e="T889" id="Seg_9936" s="T888">посылать-PST-1SG</ta>
            <ta e="T890" id="Seg_9937" s="T889">тогда</ta>
            <ta e="T891" id="Seg_9938" s="T890">этот-PL</ta>
            <ta e="T892" id="Seg_9939" s="T891">бежать-MOM-PST-3PL</ta>
            <ta e="T893" id="Seg_9940" s="T892">а</ta>
            <ta e="T894" id="Seg_9941" s="T893">этот-PL</ta>
            <ta e="T895" id="Seg_9942" s="T894">NEG</ta>
            <ta e="T896" id="Seg_9943" s="T895">бежать-PRS-3PL</ta>
            <ta e="T897" id="Seg_9944" s="T896">ленивый.[NOM.SG]</ta>
            <ta e="T898" id="Seg_9945" s="T897">стать-RES-PST-3PL</ta>
            <ta e="T900" id="Seg_9946" s="T899">вчера</ta>
            <ta e="T901" id="Seg_9947" s="T900">я.NOM</ta>
            <ta e="T902" id="Seg_9948" s="T901">невестка-NOM/GEN/ACC.1SG</ta>
            <ta e="T903" id="Seg_9949" s="T902">NEG</ta>
            <ta e="T1025" id="Seg_9950" s="T903">пойти-CVB</ta>
            <ta e="T904" id="Seg_9951" s="T1025">исчезнуть-PST.[3SG]</ta>
            <ta e="T905" id="Seg_9952" s="T904">дом-LAT/LOC.3SG</ta>
            <ta e="T906" id="Seg_9953" s="T905">я.NOM</ta>
            <ta e="T907" id="Seg_9954" s="T906">идти-PST-1SG</ta>
            <ta e="T908" id="Seg_9955" s="T907">и</ta>
            <ta e="T909" id="Seg_9956" s="T908">этот-COM</ta>
            <ta e="T910" id="Seg_9957" s="T909">NEG</ta>
            <ta e="T911" id="Seg_9958" s="T910">целовать-PST-1SG</ta>
            <ta e="T912" id="Seg_9959" s="T911">рука-NOM/GEN/ACC.1SG</ta>
            <ta e="T913" id="Seg_9960" s="T912">NEG</ta>
            <ta e="T914" id="Seg_9961" s="T913">дать-PST-1SG</ta>
            <ta e="T915" id="Seg_9962" s="T914">тогда</ta>
            <ta e="T916" id="Seg_9963" s="T915">этот</ta>
            <ta e="T917" id="Seg_9964" s="T916">опять</ta>
            <ta e="T918" id="Seg_9965" s="T917">вернуться-MOM-PST.[3SG]</ta>
            <ta e="T919" id="Seg_9966" s="T918">очень</ta>
            <ta e="T920" id="Seg_9967" s="T919">ветер.[NOM.SG]</ta>
            <ta e="T921" id="Seg_9968" s="T920">снег.[NOM.SG]</ta>
            <ta e="T922" id="Seg_9969" s="T921">PTCL</ta>
            <ta e="T923" id="Seg_9970" s="T922">нести-TR-DUR.[3SG]</ta>
            <ta e="T924" id="Seg_9971" s="T923">машина-PL</ta>
            <ta e="T925" id="Seg_9972" s="T924">NEG</ta>
            <ta e="T926" id="Seg_9973" s="T925">пойти-PST-3PL</ta>
            <ta e="T927" id="Seg_9974" s="T926">этот.[NOM.SG]</ta>
            <ta e="T928" id="Seg_9975" s="T927">вернуться-MOM-PST.[3SG]</ta>
            <ta e="T929" id="Seg_9976" s="T928">здесь</ta>
            <ta e="T930" id="Seg_9977" s="T929">ночевать-PST.[3SG]</ta>
            <ta e="T931" id="Seg_9978" s="T930">а</ta>
            <ta e="T932" id="Seg_9979" s="T931">сегодня</ta>
            <ta e="T933" id="Seg_9980" s="T932">я.NOM</ta>
            <ta e="T934" id="Seg_9981" s="T933">встать-PST-1SG</ta>
            <ta e="T935" id="Seg_9982" s="T934">кипятить-PST-1SG</ta>
            <ta e="T936" id="Seg_9983" s="T935">PTCL</ta>
            <ta e="T937" id="Seg_9984" s="T936">хлеб.[NOM.SG]</ta>
            <ta e="T938" id="Seg_9985" s="T937">печь-PST-1SG</ta>
            <ta e="T939" id="Seg_9986" s="T938">сам-LAT/LOC.3SG</ta>
            <ta e="T940" id="Seg_9987" s="T939">дорога-LAT</ta>
            <ta e="T941" id="Seg_9988" s="T940">есть-PST.[3SG]</ta>
            <ta e="T942" id="Seg_9989" s="T941">тогда</ta>
            <ta e="T1026" id="Seg_9990" s="T942">пойти-CVB</ta>
            <ta e="T943" id="Seg_9991" s="T1026">исчезнуть-PST.[3SG]</ta>
            <ta e="T944" id="Seg_9992" s="T943">машина-INS</ta>
            <ta e="T945" id="Seg_9993" s="T944">председатель-INS</ta>
            <ta e="T947" id="Seg_9994" s="T946">тогда</ta>
            <ta e="T948" id="Seg_9995" s="T947">пойти-FUT-1SG</ta>
            <ta e="T949" id="Seg_9996" s="T948">дом-LAT/LOC.1SG</ta>
            <ta e="T950" id="Seg_9997" s="T949">тогда</ta>
            <ta e="T951" id="Seg_9998" s="T950">прийти-PST-1SG</ta>
            <ta e="T952" id="Seg_9999" s="T951">пойти-FUT-1SG</ta>
            <ta e="T953" id="Seg_10000" s="T952">дом-LAT/LOC.1SG</ta>
            <ta e="T954" id="Seg_10001" s="T953">а.то</ta>
            <ta e="T955" id="Seg_10002" s="T954">я.NOM</ta>
            <ta e="T956" id="Seg_10003" s="T955">там</ta>
            <ta e="T957" id="Seg_10004" s="T956">люди.[NOM.SG]</ta>
            <ta e="T958" id="Seg_10005" s="T957">сидеть-DUR.[3SG]</ta>
            <ta e="T959" id="Seg_10006" s="T958">я.ACC</ta>
            <ta e="T960" id="Seg_10007" s="T959">ждать-DUR-3PL</ta>
            <ta e="T961" id="Seg_10008" s="T960">тогда</ta>
            <ta e="T962" id="Seg_10009" s="T961">прийти-PST-1SG</ta>
            <ta e="T963" id="Seg_10010" s="T962">дом-LAT/LOC.1SG</ta>
            <ta e="T964" id="Seg_10011" s="T963">%%</ta>
            <ta e="T965" id="Seg_10012" s="T964">мыть-PST-1SG</ta>
            <ta e="T966" id="Seg_10013" s="T965">теленок.[NOM.SG]</ta>
            <ta e="T967" id="Seg_10014" s="T966">пить-TR-PST-1SG</ta>
            <ta e="T968" id="Seg_10015" s="T967">тогда</ta>
            <ta e="T969" id="Seg_10016" s="T968">корова-NOM/GEN/ACC.1SG</ta>
            <ta e="T970" id="Seg_10017" s="T969">гнать-PST-1SG</ta>
            <ta e="T971" id="Seg_10018" s="T970">река-LAT</ta>
            <ta e="T972" id="Seg_10019" s="T971">пить-TR-PST-1SG</ta>
            <ta e="T973" id="Seg_10020" s="T972">тогда</ta>
            <ta e="T975" id="Seg_10021" s="T974">прийти-PRS.[3SG]</ta>
            <ta e="T977" id="Seg_10022" s="T976">девушка.[NOM.SG]</ta>
            <ta e="T978" id="Seg_10023" s="T977">прийти-PRS.[3SG]</ta>
            <ta e="T979" id="Seg_10024" s="T978">я.LAT</ta>
            <ta e="T980" id="Seg_10025" s="T979">INCH</ta>
            <ta e="T981" id="Seg_10026" s="T980">ободрать-INF.LAT</ta>
            <ta e="T982" id="Seg_10027" s="T981">я.NOM</ta>
            <ta e="T983" id="Seg_10028" s="T982">стоять-PST-1SG</ta>
            <ta e="T984" id="Seg_10029" s="T983">этот.[NOM.SG]</ta>
            <ta e="T985" id="Seg_10030" s="T984">ободрать-PST.[3SG]</ta>
            <ta e="T986" id="Seg_10031" s="T985">я.ACC</ta>
            <ta e="T987" id="Seg_10032" s="T986">тогда</ta>
            <ta e="T990" id="Seg_10033" s="T989">прийти-PST-1PL</ta>
            <ta e="T991" id="Seg_10034" s="T990">дом-LAT</ta>
            <ta e="T992" id="Seg_10035" s="T991">говорить-INF.LAT</ta>
            <ta e="T994" id="Seg_10036" s="T993">тогда</ta>
            <ta e="T995" id="Seg_10037" s="T994">говорить-INF.LAT</ta>
            <ta e="T996" id="Seg_10038" s="T995">пленка-LAT</ta>
            <ta e="T998" id="Seg_10039" s="T997">курица-PL</ta>
            <ta e="T999" id="Seg_10040" s="T998">кормить-PST-1SG</ta>
            <ta e="T1000" id="Seg_10041" s="T999">корова-GEN</ta>
            <ta e="T1001" id="Seg_10042" s="T1000">дерьмо.[NOM.SG]</ta>
            <ta e="T1002" id="Seg_10043" s="T1001">выбросить-PST-1SG</ta>
            <ta e="T1004" id="Seg_10044" s="T1003">хватит</ta>
            <ta e="T1005" id="Seg_10045" s="T1004">говорить-INF.LAT</ta>
            <ta e="T1006" id="Seg_10046" s="T1005">а.то</ta>
            <ta e="T1007" id="Seg_10047" s="T1006">ребенок-PL</ta>
            <ta e="T1008" id="Seg_10048" s="T1007">скоро</ta>
            <ta e="T1009" id="Seg_10049" s="T1008">прийти-FUT-3PL</ta>
            <ta e="T1010" id="Seg_10050" s="T1009">школа-ABL</ta>
            <ta e="T1011" id="Seg_10051" s="T1010">NEG</ta>
            <ta e="T1012" id="Seg_10052" s="T1011">дать-FUT-3PL</ta>
            <ta e="T1013" id="Seg_10053" s="T1012">говорить-INF.LAT</ta>
            <ta e="T1014" id="Seg_10054" s="T1013">PTCL</ta>
            <ta e="T1015" id="Seg_10055" s="T1014">кричать-FUT-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc-PKZ">
            <ta e="T1" id="Seg_10056" s="T0">pers</ta>
            <ta e="T2" id="Seg_10057" s="T1">v-v:tense-v:pn</ta>
            <ta e="T3" id="Seg_10058" s="T2">num-n:case</ta>
            <ta e="T4" id="Seg_10059" s="T3">n-n:num</ta>
            <ta e="T5" id="Seg_10060" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_10061" s="T5">pers</ta>
            <ta e="T7" id="Seg_10062" s="T6">adv</ta>
            <ta e="T8" id="Seg_10063" s="T7">num-n:case</ta>
            <ta e="T9" id="Seg_10064" s="T8">v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_10065" s="T9">dempro</ta>
            <ta e="T11" id="Seg_10066" s="T10">num-n:case</ta>
            <ta e="T12" id="Seg_10067" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_10068" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_10069" s="T13">v-v:tense-v:pn</ta>
            <ta e="T15" id="Seg_10070" s="T14">v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_10071" s="T15">conj</ta>
            <ta e="T776" id="Seg_10072" s="T16">v-v:n-fin</ta>
            <ta e="T17" id="Seg_10073" s="T776">v-v:tense-v:pn</ta>
            <ta e="T18" id="Seg_10074" s="T17">adv</ta>
            <ta e="T21" id="Seg_10075" s="T20">n-n:case.poss</ta>
            <ta e="T22" id="Seg_10076" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_10077" s="T22">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_10078" s="T23">n-n:case.poss</ta>
            <ta e="T25" id="Seg_10079" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_10080" s="T25">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_10081" s="T26">adv</ta>
            <ta e="T28" id="Seg_10082" s="T27">n-n:case.poss</ta>
            <ta e="T29" id="Seg_10083" s="T28">v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_10084" s="T29">n-n:case.poss</ta>
            <ta e="T31" id="Seg_10085" s="T30">v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_10086" s="T31">n-n:case.poss</ta>
            <ta e="T33" id="Seg_10087" s="T32">n-n:case</ta>
            <ta e="T34" id="Seg_10088" s="T33">v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_10089" s="T34">conj</ta>
            <ta e="T36" id="Seg_10090" s="T35">adv</ta>
            <ta e="T37" id="Seg_10091" s="T36">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T38" id="Seg_10092" s="T37">pers</ta>
            <ta e="T39" id="Seg_10093" s="T38">n-n:case.poss</ta>
            <ta e="T40" id="Seg_10094" s="T39">n-n:case.poss</ta>
            <ta e="T41" id="Seg_10095" s="T40">v-v:tense-v:pn</ta>
            <ta e="T42" id="Seg_10096" s="T41">num-n:case</ta>
            <ta e="T44" id="Seg_10097" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_10098" s="T44">v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_10099" s="T45">pers</ta>
            <ta e="T47" id="Seg_10100" s="T46">dempro-n:case</ta>
            <ta e="T48" id="Seg_10101" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_10102" s="T48">v-v:tense-v:pn</ta>
            <ta e="T50" id="Seg_10103" s="T49">n-n:case</ta>
            <ta e="T51" id="Seg_10104" s="T50">v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_10105" s="T51">conj</ta>
            <ta e="T53" id="Seg_10106" s="T52">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T55" id="Seg_10107" s="T54">n-n:case</ta>
            <ta e="T56" id="Seg_10108" s="T55">num-n:case</ta>
            <ta e="T60" id="Seg_10109" s="T59">num-n:case</ta>
            <ta e="T61" id="Seg_10110" s="T60">pers</ta>
            <ta e="T62" id="Seg_10111" s="T61">v-v:tense-v:pn</ta>
            <ta e="T63" id="Seg_10112" s="T62">adv</ta>
            <ta e="T64" id="Seg_10113" s="T63">dempro-n:case</ta>
            <ta e="T65" id="Seg_10114" s="T64">v-v:tense-v:pn</ta>
            <ta e="T66" id="Seg_10115" s="T65">n-n:case</ta>
            <ta e="T67" id="Seg_10116" s="T66">adv</ta>
            <ta e="T68" id="Seg_10117" s="T67">v-v:tense-v:pn</ta>
            <ta e="T69" id="Seg_10118" s="T68">dempro-n:case</ta>
            <ta e="T70" id="Seg_10119" s="T69">n-n:case</ta>
            <ta e="T71" id="Seg_10120" s="T70">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T73" id="Seg_10121" s="T72">pers</ta>
            <ta e="T75" id="Seg_10122" s="T74">n-n:case.poss</ta>
            <ta e="T77" id="Seg_10123" s="T76">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_10124" s="T77">n-n:case</ta>
            <ta e="T82" id="Seg_10125" s="T81">v-v:tense-v:pn</ta>
            <ta e="T83" id="Seg_10126" s="T82">v-v:tense-v:pn</ta>
            <ta e="T84" id="Seg_10127" s="T83">v-v:tense-v:pn</ta>
            <ta e="T85" id="Seg_10128" s="T84">v-v:tense-v:pn</ta>
            <ta e="T86" id="Seg_10129" s="T85">n-n:case</ta>
            <ta e="T88" id="Seg_10130" s="T87">v-v:tense-v:pn</ta>
            <ta e="T89" id="Seg_10131" s="T88">adv</ta>
            <ta e="T90" id="Seg_10132" s="T89">pers</ta>
            <ta e="T91" id="Seg_10133" s="T90">v-v:tense-v:pn</ta>
            <ta e="T92" id="Seg_10134" s="T91">pers</ta>
            <ta e="T93" id="Seg_10135" s="T92">v-v:tense-v:pn</ta>
            <ta e="T94" id="Seg_10136" s="T93">pers</ta>
            <ta e="T95" id="Seg_10137" s="T94">conj</ta>
            <ta e="T96" id="Seg_10138" s="T95">ptcl</ta>
            <ta e="T97" id="Seg_10139" s="T96">v-v&gt;v-v:pn</ta>
            <ta e="T99" id="Seg_10140" s="T98">num-n:case</ta>
            <ta e="T100" id="Seg_10141" s="T99">n-n:case</ta>
            <ta e="T101" id="Seg_10142" s="T100">v-v:tense-v:pn</ta>
            <ta e="T102" id="Seg_10143" s="T101">conj</ta>
            <ta e="T103" id="Seg_10144" s="T102">adv</ta>
            <ta e="T104" id="Seg_10145" s="T103">v-v:tense-v:pn</ta>
            <ta e="T105" id="Seg_10146" s="T104">n-n:case</ta>
            <ta e="T106" id="Seg_10147" s="T105">v-v:n.fin</ta>
            <ta e="T107" id="Seg_10148" s="T106">dempro-n:num-n:case</ta>
            <ta e="T108" id="Seg_10149" s="T107">n-n:case</ta>
            <ta e="T109" id="Seg_10150" s="T108">v-v:tense-v:pn</ta>
            <ta e="T111" id="Seg_10151" s="T110">v-v:tense-v:pn</ta>
            <ta e="T112" id="Seg_10152" s="T111">n-n:case</ta>
            <ta e="T113" id="Seg_10153" s="T112">v-v&gt;v-v:pn</ta>
            <ta e="T114" id="Seg_10154" s="T113">v-v:ins-v:mood.pn</ta>
            <ta e="T115" id="Seg_10155" s="T114">v-v:mood.pn</ta>
            <ta e="T116" id="Seg_10156" s="T115">n-n:case</ta>
            <ta e="T117" id="Seg_10157" s="T116">dempro-n:case</ta>
            <ta e="T118" id="Seg_10158" s="T117">v-v:tense-v:pn</ta>
            <ta e="T119" id="Seg_10159" s="T118">adv</ta>
            <ta e="T120" id="Seg_10160" s="T119">n-n:case</ta>
            <ta e="T121" id="Seg_10161" s="T120">v-v&gt;v-v:pn</ta>
            <ta e="T123" id="Seg_10162" s="T122">v-v:mood.pn</ta>
            <ta e="T125" id="Seg_10163" s="T124">adv</ta>
            <ta e="T126" id="Seg_10164" s="T125">n-n:case</ta>
            <ta e="T127" id="Seg_10165" s="T126">v-v:tense-v:pn</ta>
            <ta e="T128" id="Seg_10166" s="T127">conj</ta>
            <ta e="T129" id="Seg_10167" s="T128">dempro-n:case</ta>
            <ta e="T130" id="Seg_10168" s="T129">v-v&gt;v-v:pn</ta>
            <ta e="T131" id="Seg_10169" s="T130">pers</ta>
            <ta e="T132" id="Seg_10170" s="T131">ptcl</ta>
            <ta e="T133" id="Seg_10171" s="T132">v-v:pn</ta>
            <ta e="T134" id="Seg_10172" s="T133">dempro-n:case</ta>
            <ta e="T135" id="Seg_10173" s="T134">n-n:case.poss</ta>
            <ta e="T140" id="Seg_10174" s="T139">v-v:tense-v:pn</ta>
            <ta e="T142" id="Seg_10175" s="T141">num-n:case</ta>
            <ta e="T144" id="Seg_10176" s="T143">v-v:tense-v:pn</ta>
            <ta e="T145" id="Seg_10177" s="T144">dempro-n:case</ta>
            <ta e="T1017" id="Seg_10178" s="T146">v-v:n-fin</ta>
            <ta e="T147" id="Seg_10179" s="T1017">v-v:tense-v:pn</ta>
            <ta e="T148" id="Seg_10180" s="T147">conj</ta>
            <ta e="T149" id="Seg_10181" s="T148">n-n:case</ta>
            <ta e="T150" id="Seg_10182" s="T149">ptcl</ta>
            <ta e="T151" id="Seg_10183" s="T150">v-v:tense-v:pn</ta>
            <ta e="T153" id="Seg_10184" s="T151">adv</ta>
            <ta e="T154" id="Seg_10185" s="T153">adv</ta>
            <ta e="T155" id="Seg_10186" s="T154">num-n:case</ta>
            <ta e="T156" id="Seg_10187" s="T155">adv</ta>
            <ta e="T157" id="Seg_10188" s="T156">v-v:tense-v:pn</ta>
            <ta e="T158" id="Seg_10189" s="T157">conj</ta>
            <ta e="T159" id="Seg_10190" s="T158">dempro-n:case</ta>
            <ta e="T160" id="Seg_10191" s="T159">v-v:tense-v:pn</ta>
            <ta e="T161" id="Seg_10192" s="T160">v-v:mood.pn</ta>
            <ta e="T163" id="Seg_10193" s="T162">dempro-n:case</ta>
            <ta e="T164" id="Seg_10194" s="T163">v-v&gt;v-v:pn</ta>
            <ta e="T165" id="Seg_10195" s="T164">pers</ta>
            <ta e="T166" id="Seg_10196" s="T165">ptcl</ta>
            <ta e="T167" id="Seg_10197" s="T166">v-v:pn</ta>
            <ta e="T168" id="Seg_10198" s="T167">ptcl</ta>
            <ta e="T169" id="Seg_10199" s="T168">adv</ta>
            <ta e="T170" id="Seg_10200" s="T169">adv</ta>
            <ta e="T171" id="Seg_10201" s="T170">ptcl</ta>
            <ta e="T172" id="Seg_10202" s="T171">v-v:tense-v:pn</ta>
            <ta e="T173" id="Seg_10203" s="T172">n-n:case.poss</ta>
            <ta e="T174" id="Seg_10204" s="T173">conj</ta>
            <ta e="T175" id="Seg_10205" s="T174">v-v:tense-v:pn</ta>
            <ta e="T176" id="Seg_10206" s="T175">ptcl</ta>
            <ta e="T177" id="Seg_10207" s="T176">v-v:tense-v:pn</ta>
            <ta e="T178" id="Seg_10208" s="T177">n-n:case</ta>
            <ta e="T179" id="Seg_10209" s="T178">adv</ta>
            <ta e="T180" id="Seg_10210" s="T179">num-n:case</ta>
            <ta e="T181" id="Seg_10211" s="T180">adv</ta>
            <ta e="T182" id="Seg_10212" s="T181">v-v:tense-v:pn</ta>
            <ta e="T183" id="Seg_10213" s="T182">v-v:mood.pn</ta>
            <ta e="T185" id="Seg_10214" s="T184">ptcl</ta>
            <ta e="T186" id="Seg_10215" s="T185">dempro-n:case</ta>
            <ta e="T187" id="Seg_10216" s="T186">v-v&gt;v-v:pn</ta>
            <ta e="T188" id="Seg_10217" s="T187">v-v:tense-v:pn</ta>
            <ta e="T189" id="Seg_10218" s="T188">num-n:case</ta>
            <ta e="T190" id="Seg_10219" s="T189">v-v:tense-v:pn</ta>
            <ta e="T191" id="Seg_10220" s="T190">n-n:num</ta>
            <ta e="T192" id="Seg_10221" s="T191">adv</ta>
            <ta e="T193" id="Seg_10222" s="T192">v-v:tense-v:pn</ta>
            <ta e="T194" id="Seg_10223" s="T193">n-n:case</ta>
            <ta e="T195" id="Seg_10224" s="T194">v-v:n.fin</ta>
            <ta e="T196" id="Seg_10225" s="T195">adv</ta>
            <ta e="T197" id="Seg_10226" s="T196">n</ta>
            <ta e="T198" id="Seg_10227" s="T197">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T199" id="Seg_10228" s="T198">dempro-n:case</ta>
            <ta e="T200" id="Seg_10229" s="T199">ptcl</ta>
            <ta e="T201" id="Seg_10230" s="T200">n-n:case</ta>
            <ta e="T1018" id="Seg_10231" s="T201">v-v:n-fin</ta>
            <ta e="T202" id="Seg_10232" s="T1018">v-v:tense-v:pn</ta>
            <ta e="T203" id="Seg_10233" s="T202">adv</ta>
            <ta e="T204" id="Seg_10234" s="T203">n-n:case</ta>
            <ta e="T205" id="Seg_10235" s="T204">v-v:tense-v:pn</ta>
            <ta e="T206" id="Seg_10236" s="T205">n-n:case</ta>
            <ta e="T207" id="Seg_10237" s="T206">v-v:tense-v:pn</ta>
            <ta e="T208" id="Seg_10238" s="T207">n-n:num</ta>
            <ta e="T209" id="Seg_10239" s="T208">v-v:tense-v:pn</ta>
            <ta e="T210" id="Seg_10240" s="T209">adv</ta>
            <ta e="T211" id="Seg_10241" s="T210">v-v:tense-v:pn</ta>
            <ta e="T212" id="Seg_10242" s="T211">n-n:case</ta>
            <ta e="T213" id="Seg_10243" s="T212">n-n:case</ta>
            <ta e="T214" id="Seg_10244" s="T213">adj-n:case</ta>
            <ta e="T215" id="Seg_10245" s="T214">conj</ta>
            <ta e="T216" id="Seg_10246" s="T215">ptcl</ta>
            <ta e="T217" id="Seg_10247" s="T216">n-n:num</ta>
            <ta e="T218" id="Seg_10248" s="T217">v-v:n.fin</ta>
            <ta e="T219" id="Seg_10249" s="T218">dempro-n:case</ta>
            <ta e="T221" id="Seg_10250" s="T220">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T222" id="Seg_10251" s="T221">n-n:case</ta>
            <ta e="T223" id="Seg_10252" s="T222">dempro-n:case</ta>
            <ta e="T224" id="Seg_10253" s="T223">ptcl</ta>
            <ta e="T226" id="Seg_10254" s="T224">dempro-n:case</ta>
            <ta e="T229" id="Seg_10255" s="T228">adv</ta>
            <ta e="T230" id="Seg_10256" s="T229">dempro-n:case</ta>
            <ta e="T231" id="Seg_10257" s="T230">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T232" id="Seg_10258" s="T231">n-n:case</ta>
            <ta e="T233" id="Seg_10259" s="T232">n-n:case</ta>
            <ta e="T235" id="Seg_10260" s="T234">v&gt;v-v-v:tense-v:pn</ta>
            <ta e="T236" id="Seg_10261" s="T235">adv</ta>
            <ta e="T237" id="Seg_10262" s="T236">n-n:case</ta>
            <ta e="T239" id="Seg_10263" s="T238">v-v:tense-v:pn</ta>
            <ta e="T240" id="Seg_10264" s="T239">adv</ta>
            <ta e="T241" id="Seg_10265" s="T240">n-n:case</ta>
            <ta e="T242" id="Seg_10266" s="T241">v-v:tense-v:pn</ta>
            <ta e="T243" id="Seg_10267" s="T242">n-n:case</ta>
            <ta e="T244" id="Seg_10268" s="T243">n-n:case</ta>
            <ta e="T245" id="Seg_10269" s="T244">adj</ta>
            <ta e="T246" id="Seg_10270" s="T245">pers</ta>
            <ta e="T248" id="Seg_10271" s="T247">pers</ta>
            <ta e="T249" id="Seg_10272" s="T248">n-n:case.poss</ta>
            <ta e="T250" id="Seg_10273" s="T249">pers</ta>
            <ta e="T252" id="Seg_10274" s="T251">n-n:case.poss-n:case</ta>
            <ta e="T253" id="Seg_10275" s="T252">pers</ta>
            <ta e="T254" id="Seg_10276" s="T253">n-n:case.poss</ta>
            <ta e="T255" id="Seg_10277" s="T254">v-v:n.fin</ta>
            <ta e="T1019" id="Seg_10278" s="T255">v-v:n-fin</ta>
            <ta e="T256" id="Seg_10279" s="T1019">v-v:tense-v:pn</ta>
            <ta e="T258" id="Seg_10280" s="T257">adv</ta>
            <ta e="T259" id="Seg_10281" s="T258">dempro</ta>
            <ta e="T260" id="Seg_10282" s="T259">n-n:case</ta>
            <ta e="T261" id="Seg_10283" s="T260">v-v:tense-v:pn</ta>
            <ta e="T262" id="Seg_10284" s="T261">aux-v:mood.pn</ta>
            <ta e="T263" id="Seg_10285" s="T262">v-v:n.fin</ta>
            <ta e="T264" id="Seg_10286" s="T263">conj</ta>
            <ta e="T265" id="Seg_10287" s="T264">dempro-n:case</ta>
            <ta e="T267" id="Seg_10288" s="T266">dempro-n:case</ta>
            <ta e="T269" id="Seg_10289" s="T268">n-n:case</ta>
            <ta e="T270" id="Seg_10290" s="T269">dempro-n:case</ta>
            <ta e="T271" id="Seg_10291" s="T270">n-n:case.poss</ta>
            <ta e="T273" id="Seg_10292" s="T272">v-v:tense-v:pn</ta>
            <ta e="T274" id="Seg_10293" s="T273">conj</ta>
            <ta e="T275" id="Seg_10294" s="T274">n-n:case</ta>
            <ta e="T276" id="Seg_10295" s="T275">v-v:tense-v:pn</ta>
            <ta e="T277" id="Seg_10296" s="T276">conj</ta>
            <ta e="T1020" id="Seg_10297" s="T277">v-v:n-fin</ta>
            <ta e="T278" id="Seg_10298" s="T1020">v-v:tense-v:pn</ta>
            <ta e="T280" id="Seg_10299" s="T279">pers</ta>
            <ta e="T281" id="Seg_10300" s="T280">n-n:case.poss</ta>
            <ta e="T282" id="Seg_10301" s="T281">pers</ta>
            <ta e="T283" id="Seg_10302" s="T282">n-n:case</ta>
            <ta e="T286" id="Seg_10303" s="T285">v-v:n.fin-v:tense-v:pn</ta>
            <ta e="T287" id="Seg_10304" s="T286">conj</ta>
            <ta e="T1021" id="Seg_10305" s="T287">v-v:n-fin</ta>
            <ta e="T288" id="Seg_10306" s="T1021">v-v:tense-v:pn</ta>
            <ta e="T289" id="Seg_10307" s="T288">pers</ta>
            <ta e="T290" id="Seg_10308" s="T289">adj</ta>
            <ta e="T291" id="Seg_10309" s="T290">v-v:tense-v:pn</ta>
            <ta e="T292" id="Seg_10310" s="T291">conj</ta>
            <ta e="T293" id="Seg_10311" s="T292">dempro-n:case</ta>
            <ta e="T294" id="Seg_10312" s="T293">pers</ta>
            <ta e="T295" id="Seg_10313" s="T294">v-v:tense-v:pn</ta>
            <ta e="T296" id="Seg_10314" s="T295">n-n:case</ta>
            <ta e="T297" id="Seg_10315" s="T296">v-v:tense-v:pn</ta>
            <ta e="T298" id="Seg_10316" s="T297">conj</ta>
            <ta e="T299" id="Seg_10317" s="T298">adv</ta>
            <ta e="T1022" id="Seg_10318" s="T299">v-v:n-fin</ta>
            <ta e="T300" id="Seg_10319" s="T1022">v-v:tense-v:pn</ta>
            <ta e="T301" id="Seg_10320" s="T300">propr-n:case</ta>
            <ta e="T302" id="Seg_10321" s="T301">conj</ta>
            <ta e="T303" id="Seg_10322" s="T302">adv</ta>
            <ta e="T304" id="Seg_10323" s="T303">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T306" id="Seg_10324" s="T305">conj</ta>
            <ta e="T307" id="Seg_10325" s="T306">dempro-n:case</ta>
            <ta e="T308" id="Seg_10326" s="T307">ptcl</ta>
            <ta e="T309" id="Seg_10327" s="T308">adj-n:case</ta>
            <ta e="T310" id="Seg_10328" s="T309">v-v:tense-v:pn</ta>
            <ta e="T311" id="Seg_10329" s="T310">n-n:case.poss</ta>
            <ta e="T313" id="Seg_10330" s="T311">ptcl</ta>
            <ta e="T315" id="Seg_10331" s="T314">dempro-n:case</ta>
            <ta e="T316" id="Seg_10332" s="T315">ptcl</ta>
            <ta e="T317" id="Seg_10333" s="T316">adj-n:case</ta>
            <ta e="T318" id="Seg_10334" s="T317">n-n:case.poss</ta>
            <ta e="T319" id="Seg_10335" s="T318">adj-n:case</ta>
            <ta e="T320" id="Seg_10336" s="T319">n-n:case.poss</ta>
            <ta e="T321" id="Seg_10337" s="T320">adj-n:case</ta>
            <ta e="T323" id="Seg_10338" s="T322">num-n:case</ta>
            <ta e="T324" id="Seg_10339" s="T323">n-n:case</ta>
            <ta e="T325" id="Seg_10340" s="T324">v-v:tense-v:pn</ta>
            <ta e="T326" id="Seg_10341" s="T325">v-v:tense-v:pn</ta>
            <ta e="T327" id="Seg_10342" s="T326">n-n:case.poss</ta>
            <ta e="T329" id="Seg_10343" s="T328">v-v:n.fin</ta>
            <ta e="T330" id="Seg_10344" s="T329">num-n:case</ta>
            <ta e="T331" id="Seg_10345" s="T330">v-v:tense-v:pn</ta>
            <ta e="T332" id="Seg_10346" s="T331">quant</ta>
            <ta e="T333" id="Seg_10347" s="T332">conj</ta>
            <ta e="T334" id="Seg_10348" s="T333">num-n:case</ta>
            <ta e="T335" id="Seg_10349" s="T334">adv</ta>
            <ta e="T336" id="Seg_10350" s="T335">que</ta>
            <ta e="T337" id="Seg_10351" s="T336">quant</ta>
            <ta e="T338" id="Seg_10352" s="T337">v-v:tense-v:pn</ta>
            <ta e="T339" id="Seg_10353" s="T338">conj</ta>
            <ta e="T340" id="Seg_10354" s="T339">que</ta>
            <ta e="T341" id="Seg_10355" s="T340">adv</ta>
            <ta e="T342" id="Seg_10356" s="T341">v-v:tense-v:pn</ta>
            <ta e="T343" id="Seg_10357" s="T342">dempro-n:case</ta>
            <ta e="T345" id="Seg_10358" s="T344">n-n:case</ta>
            <ta e="T346" id="Seg_10359" s="T345">n-n:case.poss</ta>
            <ta e="T347" id="Seg_10360" s="T346">v-v:tense-v:pn</ta>
            <ta e="T348" id="Seg_10361" s="T347">adv</ta>
            <ta e="T349" id="Seg_10362" s="T348">v-v:tense-v:pn</ta>
            <ta e="T350" id="Seg_10363" s="T349">n-n:case</ta>
            <ta e="T351" id="Seg_10364" s="T350">n-n:case</ta>
            <ta e="T352" id="Seg_10365" s="T351">v-v:tense-v:pn</ta>
            <ta e="T353" id="Seg_10366" s="T352">que</ta>
            <ta e="T354" id="Seg_10367" s="T353">pers</ta>
            <ta e="T355" id="Seg_10368" s="T354">n-n:case.poss</ta>
            <ta e="T356" id="Seg_10369" s="T355">dempro-v:pn</ta>
            <ta e="T357" id="Seg_10370" s="T356">ptcl</ta>
            <ta e="T358" id="Seg_10371" s="T357">adj-n:case</ta>
            <ta e="T359" id="Seg_10372" s="T358">ptcl</ta>
            <ta e="T360" id="Seg_10373" s="T359">v-v:tense-v:pn</ta>
            <ta e="T361" id="Seg_10374" s="T360">n-n:num</ta>
            <ta e="T362" id="Seg_10375" s="T361">adv</ta>
            <ta e="T363" id="Seg_10376" s="T362">ptcl</ta>
            <ta e="T364" id="Seg_10377" s="T363">v-v&gt;v-v:pn</ta>
            <ta e="T365" id="Seg_10378" s="T364">conj</ta>
            <ta e="T366" id="Seg_10379" s="T365">n-n:case.poss</ta>
            <ta e="T367" id="Seg_10380" s="T366">que=ptcl</ta>
            <ta e="T368" id="Seg_10381" s="T367">que</ta>
            <ta e="T372" id="Seg_10382" s="T371">v-v:tense-v:pn</ta>
            <ta e="T373" id="Seg_10383" s="T372">conj</ta>
            <ta e="T374" id="Seg_10384" s="T373">v-v:tense-v:pn</ta>
            <ta e="T375" id="Seg_10385" s="T374">n-n:case.poss</ta>
            <ta e="T376" id="Seg_10386" s="T375">v-v:tense-v:pn</ta>
            <ta e="T377" id="Seg_10387" s="T376">dempro-v:pn</ta>
            <ta e="T378" id="Seg_10388" s="T377">dempro-n:case</ta>
            <ta e="T379" id="Seg_10389" s="T378">v-v:n.fin</ta>
            <ta e="T380" id="Seg_10390" s="T379">v-v:tense-v:pn</ta>
            <ta e="T382" id="Seg_10391" s="T381">v-v:n.fin</ta>
            <ta e="T383" id="Seg_10392" s="T382">v-v:tense-v:pn</ta>
            <ta e="T384" id="Seg_10393" s="T383">conj</ta>
            <ta e="T385" id="Seg_10394" s="T384">refl-n:case.poss</ta>
            <ta e="T386" id="Seg_10395" s="T385">adv</ta>
            <ta e="T389" id="Seg_10396" s="T388">v-v:tense-v:pn</ta>
            <ta e="T390" id="Seg_10397" s="T389">n-n:case</ta>
            <ta e="T392" id="Seg_10398" s="T391">v-v:tense-v:pn</ta>
            <ta e="T393" id="Seg_10399" s="T392">adv</ta>
            <ta e="T395" id="Seg_10400" s="T394">dempro</ta>
            <ta e="T396" id="Seg_10401" s="T395">n-n:case</ta>
            <ta e="T397" id="Seg_10402" s="T396">v-v:tense-v:pn</ta>
            <ta e="T398" id="Seg_10403" s="T397">que</ta>
            <ta e="T399" id="Seg_10404" s="T398">pers</ta>
            <ta e="T401" id="Seg_10405" s="T400">dempro-v:pn</ta>
            <ta e="T402" id="Seg_10406" s="T401">v-v&gt;v-v:pn</ta>
            <ta e="T403" id="Seg_10407" s="T402">pers</ta>
            <ta e="T404" id="Seg_10408" s="T403">adv</ta>
            <ta e="T405" id="Seg_10409" s="T404">v-v&gt;v-v:pn</ta>
            <ta e="T406" id="Seg_10410" s="T405">dempro-v:pn</ta>
            <ta e="T410" id="Seg_10411" s="T409">v-v:tense-v:pn</ta>
            <ta e="T411" id="Seg_10412" s="T410">n-n:case.poss</ta>
            <ta e="T413" id="Seg_10413" s="T412">que-n:case=ptcl</ta>
            <ta e="T414" id="Seg_10414" s="T413">ptcl</ta>
            <ta e="T415" id="Seg_10415" s="T414">v-v:tense-v:pn</ta>
            <ta e="T416" id="Seg_10416" s="T415">adv</ta>
            <ta e="T417" id="Seg_10417" s="T416">n-n:case.poss</ta>
            <ta e="T418" id="Seg_10418" s="T417">que-n:case=ptcl</ta>
            <ta e="T419" id="Seg_10419" s="T418">ptcl</ta>
            <ta e="T420" id="Seg_10420" s="T419">v-v:tense-v:pn</ta>
            <ta e="T421" id="Seg_10421" s="T420">dempro-v:pn</ta>
            <ta e="T422" id="Seg_10422" s="T421">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T423" id="Seg_10423" s="T422">n-n:case</ta>
            <ta e="T424" id="Seg_10424" s="T423">n-n:case.poss</ta>
            <ta e="T425" id="Seg_10425" s="T424">v-v:tense-v:pn</ta>
            <ta e="T426" id="Seg_10426" s="T425">conj</ta>
            <ta e="T1023" id="Seg_10427" s="T426">v-v:n-fin</ta>
            <ta e="T427" id="Seg_10428" s="T1023">v-v:tense-v:pn</ta>
            <ta e="T428" id="Seg_10429" s="T427">que</ta>
            <ta e="T429" id="Seg_10430" s="T428">n-n:case</ta>
            <ta e="T430" id="Seg_10431" s="T429">quant</ta>
            <ta e="T432" id="Seg_10432" s="T431">pers</ta>
            <ta e="T433" id="Seg_10433" s="T432">n-n:case</ta>
            <ta e="T1024" id="Seg_10434" s="T433">v-v:n-fin</ta>
            <ta e="T434" id="Seg_10435" s="T1024">v-v:tense-v:pn</ta>
            <ta e="T435" id="Seg_10436" s="T434">pers</ta>
            <ta e="T436" id="Seg_10437" s="T435">adv</ta>
            <ta e="T437" id="Seg_10438" s="T436">adv</ta>
            <ta e="T438" id="Seg_10439" s="T437">v-v:tense-v:pn</ta>
            <ta e="T439" id="Seg_10440" s="T438">n-n:case.poss</ta>
            <ta e="T440" id="Seg_10441" s="T439">ptcl</ta>
            <ta e="T441" id="Seg_10442" s="T440">v-v:tense-v:pn</ta>
            <ta e="T442" id="Seg_10443" s="T441">conj</ta>
            <ta e="T443" id="Seg_10444" s="T442">n-n:case.poss</ta>
            <ta e="T447" id="Seg_10445" s="T446">pers</ta>
            <ta e="T449" id="Seg_10446" s="T448">n-n:case.poss</ta>
            <ta e="T450" id="Seg_10447" s="T449">n-n:case</ta>
            <ta e="T451" id="Seg_10448" s="T450">n-n:case.poss</ta>
            <ta e="T452" id="Seg_10449" s="T451">n-n:case.poss</ta>
            <ta e="T453" id="Seg_10450" s="T452">v-v:tense-v:pn</ta>
            <ta e="T454" id="Seg_10451" s="T453">n-n:case</ta>
            <ta e="T456" id="Seg_10452" s="T455">n-n:case</ta>
            <ta e="T457" id="Seg_10453" s="T456">v-v:tense-v:pn</ta>
            <ta e="T458" id="Seg_10454" s="T457">n.[n:case]</ta>
            <ta e="T459" id="Seg_10455" s="T458">v-v:n.fin</ta>
            <ta e="T460" id="Seg_10456" s="T459">adv</ta>
            <ta e="T461" id="Seg_10457" s="T460">n-n:case.poss</ta>
            <ta e="T462" id="Seg_10458" s="T461">v-v:tense-v:pn</ta>
            <ta e="T463" id="Seg_10459" s="T462">refl-n:case.poss</ta>
            <ta e="T464" id="Seg_10460" s="T463">n-n:case</ta>
            <ta e="T465" id="Seg_10461" s="T464">v-v:tense-v:pn</ta>
            <ta e="T466" id="Seg_10462" s="T465">conj</ta>
            <ta e="T467" id="Seg_10463" s="T466">adv</ta>
            <ta e="T469" id="Seg_10464" s="T468">v-v:tense-v:pn</ta>
            <ta e="T470" id="Seg_10465" s="T469">conj</ta>
            <ta e="T471" id="Seg_10466" s="T470">pers</ta>
            <ta e="T472" id="Seg_10467" s="T471">v-v:tense-v:pn</ta>
            <ta e="T473" id="Seg_10468" s="T472">conj</ta>
            <ta e="T474" id="Seg_10469" s="T473">v-v:tense-v:pn</ta>
            <ta e="T476" id="Seg_10470" s="T475">conj</ta>
            <ta e="T477" id="Seg_10471" s="T476">pers</ta>
            <ta e="T479" id="Seg_10472" s="T478">dempro-n:num-n:case</ta>
            <ta e="T480" id="Seg_10473" s="T479">v-v:n.fin</ta>
            <ta e="T483" id="Seg_10474" s="T482">conj</ta>
            <ta e="T484" id="Seg_10475" s="T483">v-v:tense-v:pn</ta>
            <ta e="T485" id="Seg_10476" s="T484">n-n:case.poss</ta>
            <ta e="T486" id="Seg_10477" s="T485">pers</ta>
            <ta e="T487" id="Seg_10478" s="T486">quant</ta>
            <ta e="T488" id="Seg_10479" s="T487">v-v:tense-v:pn</ta>
            <ta e="T490" id="Seg_10480" s="T489">num-n:case</ta>
            <ta e="T491" id="Seg_10481" s="T490">n-n:case</ta>
            <ta e="T492" id="Seg_10482" s="T491">v-v:n.fin</ta>
            <ta e="T494" id="Seg_10483" s="T493">v-v:tense-v:pn</ta>
            <ta e="T495" id="Seg_10484" s="T494">adj-n:case</ta>
            <ta e="T496" id="Seg_10485" s="T495">n-n:case</ta>
            <ta e="T497" id="Seg_10486" s="T496">adv</ta>
            <ta e="T498" id="Seg_10487" s="T497">n-n:case.poss</ta>
            <ta e="T499" id="Seg_10488" s="T498">quant</ta>
            <ta e="T500" id="Seg_10489" s="T499">v-v:tense-v:pn</ta>
            <ta e="T501" id="Seg_10490" s="T500">adv</ta>
            <ta e="T502" id="Seg_10491" s="T501">n-n:case</ta>
            <ta e="T503" id="Seg_10492" s="T502">dempro-n:case</ta>
            <ta e="T504" id="Seg_10493" s="T503">v-v&gt;v-v:pn</ta>
            <ta e="T505" id="Seg_10494" s="T504">que</ta>
            <ta e="T506" id="Seg_10495" s="T505">v-v:tense-v:pn</ta>
            <ta e="T507" id="Seg_10496" s="T506">v-v:n.fin</ta>
            <ta e="T508" id="Seg_10497" s="T507">v-v:mood.pn</ta>
            <ta e="T509" id="Seg_10498" s="T508">pers</ta>
            <ta e="T510" id="Seg_10499" s="T509">conj</ta>
            <ta e="T511" id="Seg_10500" s="T510">pers</ta>
            <ta e="T512" id="Seg_10501" s="T511">que-n:case</ta>
            <ta e="T513" id="Seg_10502" s="T512">v-v:tense-v:pn</ta>
            <ta e="T514" id="Seg_10503" s="T513">v-v:n.fin</ta>
            <ta e="T515" id="Seg_10504" s="T514">conj</ta>
            <ta e="T516" id="Seg_10505" s="T515">v-v:n.fin</ta>
            <ta e="T517" id="Seg_10506" s="T516">adv</ta>
            <ta e="T518" id="Seg_10507" s="T517">v-v:tense-v:pn</ta>
            <ta e="T519" id="Seg_10508" s="T518">adv</ta>
            <ta e="T520" id="Seg_10509" s="T519">adv</ta>
            <ta e="T521" id="Seg_10510" s="T520">n-n:case</ta>
            <ta e="T522" id="Seg_10511" s="T521">v-v&gt;v-v:pn</ta>
            <ta e="T523" id="Seg_10512" s="T522">que</ta>
            <ta e="T524" id="Seg_10513" s="T523">v-v:tense-v:pn</ta>
            <ta e="T525" id="Seg_10514" s="T524">v-v:n.fin</ta>
            <ta e="T526" id="Seg_10515" s="T525">pers</ta>
            <ta e="T527" id="Seg_10516" s="T526">v-v:mood.pn</ta>
            <ta e="T528" id="Seg_10517" s="T527">conj</ta>
            <ta e="T529" id="Seg_10518" s="T528">pers</ta>
            <ta e="T530" id="Seg_10519" s="T529">que-n:case</ta>
            <ta e="T531" id="Seg_10520" s="T530">v-v:tense-v:pn</ta>
            <ta e="T532" id="Seg_10521" s="T531">v-v:n.fin</ta>
            <ta e="T533" id="Seg_10522" s="T532">conj</ta>
            <ta e="T534" id="Seg_10523" s="T533">v-v:n.fin</ta>
            <ta e="T535" id="Seg_10524" s="T534">adv</ta>
            <ta e="T536" id="Seg_10525" s="T535">v-v:tense-v:pn</ta>
            <ta e="T537" id="Seg_10526" s="T536">conj</ta>
            <ta e="T538" id="Seg_10527" s="T537">ptcl</ta>
            <ta e="T539" id="Seg_10528" s="T538">v-v:n.fin</ta>
            <ta e="T540" id="Seg_10529" s="T539">dempro-n:case</ta>
            <ta e="T541" id="Seg_10530" s="T540">v-v&gt;v-v:pn</ta>
            <ta e="T542" id="Seg_10531" s="T541">pers</ta>
            <ta e="T543" id="Seg_10532" s="T542">quant</ta>
            <ta e="T544" id="Seg_10533" s="T543">ptcl</ta>
            <ta e="T545" id="Seg_10534" s="T544">v-v:tense-v:pn</ta>
            <ta e="T546" id="Seg_10535" s="T545">n-n:case</ta>
            <ta e="T547" id="Seg_10536" s="T546">v-v:tense-v:pn</ta>
            <ta e="T548" id="Seg_10537" s="T547">n-n:case</ta>
            <ta e="T549" id="Seg_10538" s="T548">quant</ta>
            <ta e="T550" id="Seg_10539" s="T549">v-v:n.fin</ta>
            <ta e="T551" id="Seg_10540" s="T550">ptcl</ta>
            <ta e="T552" id="Seg_10541" s="T551">dempro-v:pn</ta>
            <ta e="T553" id="Seg_10542" s="T552">v-v&gt;v-v:pn</ta>
            <ta e="T554" id="Seg_10543" s="T553">ptcl</ta>
            <ta e="T555" id="Seg_10544" s="T554">quant</ta>
            <ta e="T556" id="Seg_10545" s="T555">pers</ta>
            <ta e="T557" id="Seg_10546" s="T556">v-v:tense-v:pn</ta>
            <ta e="T558" id="Seg_10547" s="T557">conj</ta>
            <ta e="T561" id="Seg_10548" s="T560">quant</ta>
            <ta e="T562" id="Seg_10549" s="T561">num-n:case</ta>
            <ta e="T563" id="Seg_10550" s="T562">num-n:case</ta>
            <ta e="T565" id="Seg_10551" s="T564">v-v:tense-v:pn</ta>
            <ta e="T566" id="Seg_10552" s="T565">ptcl</ta>
            <ta e="T567" id="Seg_10553" s="T566">adv</ta>
            <ta e="T569" id="Seg_10554" s="T568">n-n:case.poss</ta>
            <ta e="T570" id="Seg_10555" s="T569">v-v:tense-v:pn</ta>
            <ta e="T571" id="Seg_10556" s="T570">adv</ta>
            <ta e="T572" id="Seg_10557" s="T571">v-v:tense-v:pn</ta>
            <ta e="T573" id="Seg_10558" s="T572">dempro-n:case</ta>
            <ta e="T574" id="Seg_10559" s="T573">n-n:case</ta>
            <ta e="T575" id="Seg_10560" s="T574">que</ta>
            <ta e="T576" id="Seg_10561" s="T575">v-v:n.fin</ta>
            <ta e="T577" id="Seg_10562" s="T576">conj</ta>
            <ta e="T578" id="Seg_10563" s="T577">v-v:n.fin</ta>
            <ta e="T579" id="Seg_10564" s="T578">ptcl</ta>
            <ta e="T580" id="Seg_10565" s="T579">v-v&gt;v-v:pn</ta>
            <ta e="T581" id="Seg_10566" s="T580">conj</ta>
            <ta e="T582" id="Seg_10567" s="T581">v-v&gt;v-v:pn</ta>
            <ta e="T583" id="Seg_10568" s="T582">v-v&gt;v-v:pn</ta>
            <ta e="T584" id="Seg_10569" s="T583">conj</ta>
            <ta e="T585" id="Seg_10570" s="T584">v-v&gt;v-v:pn</ta>
            <ta e="T586" id="Seg_10571" s="T585">quant</ta>
            <ta e="T587" id="Seg_10572" s="T586">que-n:case</ta>
            <ta e="T588" id="Seg_10573" s="T587">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T589" id="Seg_10574" s="T588">conj</ta>
            <ta e="T590" id="Seg_10575" s="T589">n-n:case</ta>
            <ta e="T591" id="Seg_10576" s="T590">conj</ta>
            <ta e="T593" id="Seg_10577" s="T592">que-n:case</ta>
            <ta e="T594" id="Seg_10578" s="T593">v-v:tense-v:pn</ta>
            <ta e="T595" id="Seg_10579" s="T594">quant</ta>
            <ta e="T596" id="Seg_10580" s="T595">refl-n:case.poss</ta>
            <ta e="T597" id="Seg_10581" s="T596">v-v&gt;v-v:pn</ta>
            <ta e="T598" id="Seg_10582" s="T597">v-v:mood.pn</ta>
            <ta e="T599" id="Seg_10583" s="T598">adv</ta>
            <ta e="T600" id="Seg_10584" s="T599">adv</ta>
            <ta e="T601" id="Seg_10585" s="T600">v-v:mood.pn</ta>
            <ta e="T602" id="Seg_10586" s="T601">adv</ta>
            <ta e="T603" id="Seg_10587" s="T602">adv</ta>
            <ta e="T604" id="Seg_10588" s="T603">adv</ta>
            <ta e="T605" id="Seg_10589" s="T604">ptcl</ta>
            <ta e="T606" id="Seg_10590" s="T605">v-v:n.fin</ta>
            <ta e="T612" id="Seg_10591" s="T611">adv</ta>
            <ta e="T613" id="Seg_10592" s="T612">n-n:case</ta>
            <ta e="T615" id="Seg_10593" s="T614">v-v:n.fin</ta>
            <ta e="T616" id="Seg_10594" s="T615">adv</ta>
            <ta e="T617" id="Seg_10595" s="T616">adv</ta>
            <ta e="T618" id="Seg_10596" s="T617">v-v:tense-v:pn</ta>
            <ta e="T619" id="Seg_10597" s="T618">dempro</ta>
            <ta e="T621" id="Seg_10598" s="T620">dempro</ta>
            <ta e="T622" id="Seg_10599" s="T621">n-n:case</ta>
            <ta e="T623" id="Seg_10600" s="T622">adv</ta>
            <ta e="T624" id="Seg_10601" s="T623">dempro-v:pn</ta>
            <ta e="T626" id="Seg_10602" s="T625">v-v&gt;v-v:pn</ta>
            <ta e="T627" id="Seg_10603" s="T626">v-v&gt;v-v:pn</ta>
            <ta e="T628" id="Seg_10604" s="T627">conj</ta>
            <ta e="T629" id="Seg_10605" s="T628">v-v&gt;v-v:pn</ta>
            <ta e="T630" id="Seg_10606" s="T629">v-v&gt;v-v:pn</ta>
            <ta e="T631" id="Seg_10607" s="T630">conj</ta>
            <ta e="T632" id="Seg_10608" s="T631">v-v&gt;v-v:pn</ta>
            <ta e="T633" id="Seg_10609" s="T632">quant</ta>
            <ta e="T634" id="Seg_10610" s="T633">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T635" id="Seg_10611" s="T634">conj</ta>
            <ta e="T636" id="Seg_10612" s="T635">v-v&gt;v-v:pn</ta>
            <ta e="T637" id="Seg_10613" s="T636">adv</ta>
            <ta e="T639" id="Seg_10614" s="T638">v-v:mood.pn</ta>
            <ta e="T640" id="Seg_10615" s="T639">pers</ta>
            <ta e="T641" id="Seg_10616" s="T640">adv</ta>
            <ta e="T642" id="Seg_10617" s="T641">conj</ta>
            <ta e="T643" id="Seg_10618" s="T642">n-n:case</ta>
            <ta e="T644" id="Seg_10619" s="T643">ptcl</ta>
            <ta e="T645" id="Seg_10620" s="T644">adv</ta>
            <ta e="T646" id="Seg_10621" s="T645">n-n:case</ta>
            <ta e="T647" id="Seg_10622" s="T646">quant</ta>
            <ta e="T649" id="Seg_10623" s="T648">adv</ta>
            <ta e="T650" id="Seg_10624" s="T649">ptcl</ta>
            <ta e="T651" id="Seg_10625" s="T650">v-v:tense-v:pn</ta>
            <ta e="T652" id="Seg_10626" s="T651">conj</ta>
            <ta e="T653" id="Seg_10627" s="T652">n-n:case</ta>
            <ta e="T654" id="Seg_10628" s="T653">ptcl</ta>
            <ta e="T655" id="Seg_10629" s="T654">v-v:tense-v:pn</ta>
            <ta e="T656" id="Seg_10630" s="T655">dempro-n:case</ta>
            <ta e="T657" id="Seg_10631" s="T656">ptcl</ta>
            <ta e="T658" id="Seg_10632" s="T657">n-n:case.poss</ta>
            <ta e="T659" id="Seg_10633" s="T658">v&gt;v-v-v:tense-v:pn</ta>
            <ta e="T660" id="Seg_10634" s="T659">ptcl</ta>
            <ta e="T661" id="Seg_10635" s="T660">conj</ta>
            <ta e="T662" id="Seg_10636" s="T661">v-v:tense-v:pn</ta>
            <ta e="T664" id="Seg_10637" s="T662">ptcl</ta>
            <ta e="T665" id="Seg_10638" s="T664">adv</ta>
            <ta e="T666" id="Seg_10639" s="T665">dempro</ta>
            <ta e="T667" id="Seg_10640" s="T666">n-n:case</ta>
            <ta e="T668" id="Seg_10641" s="T667">v-v:tense-v:pn</ta>
            <ta e="T669" id="Seg_10642" s="T668">conj</ta>
            <ta e="T670" id="Seg_10643" s="T669">n-n:case.poss</ta>
            <ta e="T671" id="Seg_10644" s="T670">v-v:tense-v:pn</ta>
            <ta e="T673" id="Seg_10645" s="T672">%%</ta>
            <ta e="T674" id="Seg_10646" s="T673">que-n:case</ta>
            <ta e="T675" id="Seg_10647" s="T674">v-v&gt;v-v:pn</ta>
            <ta e="T677" id="Seg_10648" s="T676">conj</ta>
            <ta e="T678" id="Seg_10649" s="T677">que-n:case</ta>
            <ta e="T679" id="Seg_10650" s="T678">ptcl</ta>
            <ta e="T680" id="Seg_10651" s="T679">adj</ta>
            <ta e="T681" id="Seg_10652" s="T680">v-v&gt;v-v:pn</ta>
            <ta e="T682" id="Seg_10653" s="T681">ptcl</ta>
            <ta e="T684" id="Seg_10654" s="T683">adj</ta>
            <ta e="T686" id="Seg_10655" s="T685">v-v:n.fin</ta>
            <ta e="T687" id="Seg_10656" s="T686">v-v:tense-v:pn</ta>
            <ta e="T688" id="Seg_10657" s="T687">%%</ta>
            <ta e="T690" id="Seg_10658" s="T689">num-n:case</ta>
            <ta e="T691" id="Seg_10659" s="T690">n-n:case</ta>
            <ta e="T693" id="Seg_10660" s="T692">v-v:tense-v:pn</ta>
            <ta e="T694" id="Seg_10661" s="T693">n-n:case.poss</ta>
            <ta e="T695" id="Seg_10662" s="T694">conj</ta>
            <ta e="T696" id="Seg_10663" s="T695">conj</ta>
            <ta e="T697" id="Seg_10664" s="T696">num-n:case</ta>
            <ta e="T698" id="Seg_10665" s="T697">n-n:case</ta>
            <ta e="T699" id="Seg_10666" s="T698">adv</ta>
            <ta e="T700" id="Seg_10667" s="T699">n-n:case</ta>
            <ta e="T701" id="Seg_10668" s="T700">quant</ta>
            <ta e="T703" id="Seg_10669" s="T702">v-v:tense-v:pn</ta>
            <ta e="T704" id="Seg_10670" s="T703">conj</ta>
            <ta e="T705" id="Seg_10671" s="T704">n-n:case</ta>
            <ta e="T706" id="Seg_10672" s="T705">conj</ta>
            <ta e="T707" id="Seg_10673" s="T706">n-n:case</ta>
            <ta e="T708" id="Seg_10674" s="T707">dempro</ta>
            <ta e="T709" id="Seg_10675" s="T708">dempro-n:case</ta>
            <ta e="T710" id="Seg_10676" s="T709">n-n:case</ta>
            <ta e="T711" id="Seg_10677" s="T710">v-v:tense-v:pn</ta>
            <ta e="T712" id="Seg_10678" s="T711">n-n:case.poss</ta>
            <ta e="T713" id="Seg_10679" s="T712">conj</ta>
            <ta e="T715" id="Seg_10680" s="T714">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T716" id="Seg_10681" s="T715">dempro-v:pn</ta>
            <ta e="T717" id="Seg_10682" s="T716">ptcl</ta>
            <ta e="T718" id="Seg_10683" s="T717">v-v&gt;v-v:pn</ta>
            <ta e="T719" id="Seg_10684" s="T718">conj</ta>
            <ta e="T720" id="Seg_10685" s="T719">n</ta>
            <ta e="T721" id="Seg_10686" s="T720">v-v:tense-v:pn</ta>
            <ta e="T722" id="Seg_10687" s="T721">que-n:case</ta>
            <ta e="T723" id="Seg_10688" s="T722">v-v&gt;v-v:pn</ta>
            <ta e="T724" id="Seg_10689" s="T723">v-v:mood-v:pn</ta>
            <ta e="T725" id="Seg_10690" s="T724">adv</ta>
            <ta e="T726" id="Seg_10691" s="T725">v-v:pn</ta>
            <ta e="T727" id="Seg_10692" s="T726">n-n:case.poss</ta>
            <ta e="T728" id="Seg_10693" s="T727">dempro-n:case</ta>
            <ta e="T729" id="Seg_10694" s="T728">%%</ta>
            <ta e="T730" id="Seg_10695" s="T729">v-v:tense-v:pn</ta>
            <ta e="T731" id="Seg_10696" s="T730">n-n:case</ta>
            <ta e="T732" id="Seg_10697" s="T731">v-v&gt;v-v:pn</ta>
            <ta e="T733" id="Seg_10698" s="T732">v-v:mood.pn</ta>
            <ta e="T734" id="Seg_10699" s="T733">n-n:case</ta>
            <ta e="T735" id="Seg_10700" s="T734">v-v:mood.pn</ta>
            <ta e="T736" id="Seg_10701" s="T735">n-n:case</ta>
            <ta e="T738" id="Seg_10702" s="T737">dempro</ta>
            <ta e="T739" id="Seg_10703" s="T738">n-n:case</ta>
            <ta e="T740" id="Seg_10704" s="T739">v-v&gt;v-v:pn</ta>
            <ta e="T741" id="Seg_10705" s="T740">v-v:mood.pn</ta>
            <ta e="T742" id="Seg_10706" s="T741">dempro-n:case</ta>
            <ta e="T743" id="Seg_10707" s="T742">n-n:case</ta>
            <ta e="T744" id="Seg_10708" s="T743">n-n:case.poss-n:case</ta>
            <ta e="T745" id="Seg_10709" s="T744">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T746" id="Seg_10710" s="T745">dempro-v:pn</ta>
            <ta e="T747" id="Seg_10711" s="T746">adv</ta>
            <ta e="T748" id="Seg_10712" s="T747">v-v:tense-v:pn</ta>
            <ta e="T749" id="Seg_10713" s="T748">adv</ta>
            <ta e="T750" id="Seg_10714" s="T749">v-v&gt;v-v:pn</ta>
            <ta e="T752" id="Seg_10715" s="T751">v-v:mood.pn</ta>
            <ta e="T753" id="Seg_10716" s="T752">n-n:case.poss</ta>
            <ta e="T754" id="Seg_10717" s="T753">v-v:mood.pn</ta>
            <ta e="T755" id="Seg_10718" s="T754">n-n:case.poss</ta>
            <ta e="T756" id="Seg_10719" s="T755">adv</ta>
            <ta e="T757" id="Seg_10720" s="T756">v-v:mood.pn</ta>
            <ta e="T759" id="Seg_10721" s="T758">dempro-n:case</ta>
            <ta e="T760" id="Seg_10722" s="T759">n-n:case</ta>
            <ta e="T761" id="Seg_10723" s="T760">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T762" id="Seg_10724" s="T761">n-n:case</ta>
            <ta e="T763" id="Seg_10725" s="T762">dempro-v:pn</ta>
            <ta e="T764" id="Seg_10726" s="T763">n-n:case</ta>
            <ta e="T766" id="Seg_10727" s="T765">adv</ta>
            <ta e="T767" id="Seg_10728" s="T766">v-v:tense-v:pn</ta>
            <ta e="T768" id="Seg_10729" s="T767">adv</ta>
            <ta e="T769" id="Seg_10730" s="T768">adv</ta>
            <ta e="T770" id="Seg_10731" s="T769">v-v&gt;v-v:pn</ta>
            <ta e="T773" id="Seg_10732" s="T772">v-v:mood.pn</ta>
            <ta e="T774" id="Seg_10733" s="T773">n-n:case.poss</ta>
            <ta e="T775" id="Seg_10734" s="T774">v-v:mood.pn</ta>
            <ta e="T777" id="Seg_10735" s="T775">n-n:case.poss</ta>
            <ta e="T778" id="Seg_10736" s="T777">v-v:mood.pn</ta>
            <ta e="T779" id="Seg_10737" s="T778">n-n:case</ta>
            <ta e="T780" id="Seg_10738" s="T779">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T781" id="Seg_10739" s="T780">n-n:case</ta>
            <ta e="T782" id="Seg_10740" s="T781">adv</ta>
            <ta e="T783" id="Seg_10741" s="T782">adv</ta>
            <ta e="T784" id="Seg_10742" s="T783">v-v:tense-v:pn</ta>
            <ta e="T785" id="Seg_10743" s="T784">adv</ta>
            <ta e="T786" id="Seg_10744" s="T785">v-v&gt;v-v:pn</ta>
            <ta e="T787" id="Seg_10745" s="T786">v-v:mood.pn</ta>
            <ta e="T788" id="Seg_10746" s="T787">n-n:case.poss</ta>
            <ta e="T789" id="Seg_10747" s="T788">v-v:mood.pn</ta>
            <ta e="T790" id="Seg_10748" s="T789">n-n:case.poss</ta>
            <ta e="T791" id="Seg_10749" s="T790">adv</ta>
            <ta e="T792" id="Seg_10750" s="T791">v-v&gt;v-v:pn</ta>
            <ta e="T793" id="Seg_10751" s="T792">v-v:mood.pn</ta>
            <ta e="T794" id="Seg_10752" s="T793">dempro-n:case</ta>
            <ta e="T795" id="Seg_10753" s="T794">n-n:case</ta>
            <ta e="T796" id="Seg_10754" s="T795">dempro-n:case</ta>
            <ta e="T797" id="Seg_10755" s="T796">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T798" id="Seg_10756" s="T797">conj</ta>
            <ta e="T799" id="Seg_10757" s="T798">dempro-v:pn</ta>
            <ta e="T800" id="Seg_10758" s="T799">v-v&gt;v-v:pn</ta>
            <ta e="T802" id="Seg_10759" s="T800">n-n:case.poss</ta>
            <ta e="T804" id="Seg_10760" s="T803">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T805" id="Seg_10761" s="T804">dempro-v:pn</ta>
            <ta e="T806" id="Seg_10762" s="T805">dempro-n:case</ta>
            <ta e="T807" id="Seg_10763" s="T806">n-n:case</ta>
            <ta e="T808" id="Seg_10764" s="T807">conj</ta>
            <ta e="T809" id="Seg_10765" s="T808">dempro-n:case</ta>
            <ta e="T810" id="Seg_10766" s="T809">v-v&gt;v-v:pn</ta>
            <ta e="T811" id="Seg_10767" s="T810">n-n:case.poss</ta>
            <ta e="T812" id="Seg_10768" s="T811">v-v:ins-v:mood.pn</ta>
            <ta e="T813" id="Seg_10769" s="T812">n-n:case</ta>
            <ta e="T814" id="Seg_10770" s="T813">n-n:case.poss</ta>
            <ta e="T815" id="Seg_10771" s="T814">v-v:ins-v:mood.pn</ta>
            <ta e="T816" id="Seg_10772" s="T815">n-n:case</ta>
            <ta e="T817" id="Seg_10773" s="T816">n-n:case.poss</ta>
            <ta e="T818" id="Seg_10774" s="T817">quant</ta>
            <ta e="T819" id="Seg_10775" s="T818">n-n:case</ta>
            <ta e="T820" id="Seg_10776" s="T819">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T821" id="Seg_10777" s="T820">adv</ta>
            <ta e="T822" id="Seg_10778" s="T821">v-v:tense-v:pn</ta>
            <ta e="T823" id="Seg_10779" s="T822">v-v:mood.pn</ta>
            <ta e="T824" id="Seg_10780" s="T823">n-n:case.poss</ta>
            <ta e="T825" id="Seg_10781" s="T824">v-v:mood.pn</ta>
            <ta e="T826" id="Seg_10782" s="T825">n-n:case.poss</ta>
            <ta e="T827" id="Seg_10783" s="T826">adv</ta>
            <ta e="T828" id="Seg_10784" s="T827">dempro</ta>
            <ta e="T829" id="Seg_10785" s="T828">n-n:case</ta>
            <ta e="T830" id="Seg_10786" s="T829">v-v&gt;v-v:pn</ta>
            <ta e="T831" id="Seg_10787" s="T830">n-n:case</ta>
            <ta e="T833" id="Seg_10788" s="T832">v-v:mood.pn</ta>
            <ta e="T834" id="Seg_10789" s="T833">adj-n:case</ta>
            <ta e="T835" id="Seg_10790" s="T834">conj</ta>
            <ta e="T836" id="Seg_10791" s="T835">v-v:mood.pn</ta>
            <ta e="T837" id="Seg_10792" s="T836">dempro-n:case</ta>
            <ta e="T838" id="Seg_10793" s="T837">n-n:case</ta>
            <ta e="T839" id="Seg_10794" s="T838">adv</ta>
            <ta e="T840" id="Seg_10795" s="T839">v-v:tense-v:pn</ta>
            <ta e="T841" id="Seg_10796" s="T840">n-n:case</ta>
            <ta e="T842" id="Seg_10797" s="T841">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T843" id="Seg_10798" s="T842">dempro</ta>
            <ta e="T844" id="Seg_10799" s="T843">n-n:case</ta>
            <ta e="T845" id="Seg_10800" s="T844">n-n:case</ta>
            <ta e="T846" id="Seg_10801" s="T845">adv</ta>
            <ta e="T847" id="Seg_10802" s="T846">v-v&gt;v-v:pn</ta>
            <ta e="T848" id="Seg_10803" s="T847">v-v:mood.pn</ta>
            <ta e="T849" id="Seg_10804" s="T848">n-n:case.poss</ta>
            <ta e="T850" id="Seg_10805" s="T849">n-n:case</ta>
            <ta e="T851" id="Seg_10806" s="T850">v-v:mood.pn</ta>
            <ta e="T852" id="Seg_10807" s="T851">n-n:case.poss</ta>
            <ta e="T853" id="Seg_10808" s="T852">n-n:case</ta>
            <ta e="T854" id="Seg_10809" s="T853">ptcl</ta>
            <ta e="T855" id="Seg_10810" s="T854">n-n:case</ta>
            <ta e="T856" id="Seg_10811" s="T855">v-v:tense-v:pn</ta>
            <ta e="T857" id="Seg_10812" s="T856">quant</ta>
            <ta e="T858" id="Seg_10813" s="T857">que-n:case</ta>
            <ta e="T860" id="Seg_10814" s="T858">ptcl</ta>
            <ta e="T862" id="Seg_10815" s="T861">adv</ta>
            <ta e="T863" id="Seg_10816" s="T862">n-n:case</ta>
            <ta e="T864" id="Seg_10817" s="T863">adv</ta>
            <ta e="T865" id="Seg_10818" s="T864">quant</ta>
            <ta e="T867" id="Seg_10819" s="T866">v-v:tense-v:pn</ta>
            <ta e="T868" id="Seg_10820" s="T867">dempro</ta>
            <ta e="T869" id="Seg_10821" s="T868">n-n:case</ta>
            <ta e="T870" id="Seg_10822" s="T869">v-v&gt;v-v:pn</ta>
            <ta e="T871" id="Seg_10823" s="T870">v-v:mood.pn</ta>
            <ta e="T872" id="Seg_10824" s="T871">n-n:case.poss</ta>
            <ta e="T873" id="Seg_10825" s="T872">conj</ta>
            <ta e="T874" id="Seg_10826" s="T873">v-v:ins-v:mood.pn</ta>
            <ta e="T875" id="Seg_10827" s="T874">adv</ta>
            <ta e="T876" id="Seg_10828" s="T875">adv</ta>
            <ta e="T877" id="Seg_10829" s="T876">adv</ta>
            <ta e="T881" id="Seg_10830" s="T880">n-n:num</ta>
            <ta e="T882" id="Seg_10831" s="T881">num-n:case</ta>
            <ta e="T884" id="Seg_10832" s="T883">v-v:tense-v:pn</ta>
            <ta e="T885" id="Seg_10833" s="T884">pers</ta>
            <ta e="T886" id="Seg_10834" s="T885">dempro-n:num</ta>
            <ta e="T889" id="Seg_10835" s="T888">v-v:tense-v:pn</ta>
            <ta e="T890" id="Seg_10836" s="T889">adv</ta>
            <ta e="T891" id="Seg_10837" s="T890">dempro-n:num</ta>
            <ta e="T892" id="Seg_10838" s="T891">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T893" id="Seg_10839" s="T892">conj</ta>
            <ta e="T894" id="Seg_10840" s="T893">dempro-n:num</ta>
            <ta e="T895" id="Seg_10841" s="T894">ptcl</ta>
            <ta e="T896" id="Seg_10842" s="T895">v-v:tense-v:pn</ta>
            <ta e="T897" id="Seg_10843" s="T896">adj-n:case</ta>
            <ta e="T898" id="Seg_10844" s="T897">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T900" id="Seg_10845" s="T899">adv</ta>
            <ta e="T901" id="Seg_10846" s="T900">pers</ta>
            <ta e="T902" id="Seg_10847" s="T901">n-n:case.poss</ta>
            <ta e="T903" id="Seg_10848" s="T902">ptcl</ta>
            <ta e="T1025" id="Seg_10849" s="T903">v-v:n-fin</ta>
            <ta e="T904" id="Seg_10850" s="T1025">v-v:tense-v:pn</ta>
            <ta e="T905" id="Seg_10851" s="T904">n-n:case.poss</ta>
            <ta e="T906" id="Seg_10852" s="T905">pers</ta>
            <ta e="T907" id="Seg_10853" s="T906">v-v:tense-v:pn</ta>
            <ta e="T908" id="Seg_10854" s="T907">conj</ta>
            <ta e="T909" id="Seg_10855" s="T908">dempro-n:case</ta>
            <ta e="T910" id="Seg_10856" s="T909">ptcl</ta>
            <ta e="T911" id="Seg_10857" s="T910">v-v:tense-v:pn</ta>
            <ta e="T912" id="Seg_10858" s="T911">n-n:case.poss</ta>
            <ta e="T913" id="Seg_10859" s="T912">ptcl</ta>
            <ta e="T914" id="Seg_10860" s="T913">v-v:tense-v:pn</ta>
            <ta e="T915" id="Seg_10861" s="T914">adv</ta>
            <ta e="T916" id="Seg_10862" s="T915">dempro</ta>
            <ta e="T917" id="Seg_10863" s="T916">adv</ta>
            <ta e="T918" id="Seg_10864" s="T917">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T919" id="Seg_10865" s="T918">adv</ta>
            <ta e="T920" id="Seg_10866" s="T919">n-n:case</ta>
            <ta e="T921" id="Seg_10867" s="T920">n-n:case</ta>
            <ta e="T922" id="Seg_10868" s="T921">ptcl</ta>
            <ta e="T923" id="Seg_10869" s="T922">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T924" id="Seg_10870" s="T923">n-n:num</ta>
            <ta e="T925" id="Seg_10871" s="T924">ptcl</ta>
            <ta e="T926" id="Seg_10872" s="T925">v-v:tense-v:pn</ta>
            <ta e="T927" id="Seg_10873" s="T926">dempro-n:case</ta>
            <ta e="T928" id="Seg_10874" s="T927">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T929" id="Seg_10875" s="T928">adv</ta>
            <ta e="T930" id="Seg_10876" s="T929">v-v:tense-v:pn</ta>
            <ta e="T931" id="Seg_10877" s="T930">conj</ta>
            <ta e="T932" id="Seg_10878" s="T931">adv</ta>
            <ta e="T933" id="Seg_10879" s="T932">pers</ta>
            <ta e="T934" id="Seg_10880" s="T933">v-v:tense-v:pn</ta>
            <ta e="T935" id="Seg_10881" s="T934">v-v:tense-v:pn</ta>
            <ta e="T936" id="Seg_10882" s="T935">ptcl</ta>
            <ta e="T937" id="Seg_10883" s="T936">n-n:case</ta>
            <ta e="T938" id="Seg_10884" s="T937">v-v:tense-v:pn</ta>
            <ta e="T939" id="Seg_10885" s="T938">refl-n:case.poss</ta>
            <ta e="T940" id="Seg_10886" s="T939">n-n:case</ta>
            <ta e="T941" id="Seg_10887" s="T940">v-v:tense-v:pn</ta>
            <ta e="T942" id="Seg_10888" s="T941">adv</ta>
            <ta e="T1026" id="Seg_10889" s="T942">v-v:n-fin</ta>
            <ta e="T943" id="Seg_10890" s="T1026">v-v:tense-v:pn</ta>
            <ta e="T944" id="Seg_10891" s="T943">n-n:case</ta>
            <ta e="T945" id="Seg_10892" s="T944">n-n:case</ta>
            <ta e="T947" id="Seg_10893" s="T946">adv</ta>
            <ta e="T948" id="Seg_10894" s="T947">v-v:tense-v:pn</ta>
            <ta e="T949" id="Seg_10895" s="T948">n-n:case.poss</ta>
            <ta e="T950" id="Seg_10896" s="T949">adv</ta>
            <ta e="T951" id="Seg_10897" s="T950">v-v:tense-v:pn</ta>
            <ta e="T952" id="Seg_10898" s="T951">v-v:tense-v:pn</ta>
            <ta e="T953" id="Seg_10899" s="T952">n-n:case.poss</ta>
            <ta e="T954" id="Seg_10900" s="T953">ptcl</ta>
            <ta e="T955" id="Seg_10901" s="T954">pers</ta>
            <ta e="T956" id="Seg_10902" s="T955">adv</ta>
            <ta e="T957" id="Seg_10903" s="T956">n-n:case</ta>
            <ta e="T958" id="Seg_10904" s="T957">v-v&gt;v-v:pn</ta>
            <ta e="T959" id="Seg_10905" s="T958">pers</ta>
            <ta e="T960" id="Seg_10906" s="T959">v-v&gt;v-v:pn</ta>
            <ta e="T961" id="Seg_10907" s="T960">adv</ta>
            <ta e="T962" id="Seg_10908" s="T961">v-v:tense-v:pn</ta>
            <ta e="T963" id="Seg_10909" s="T962">n-n:case.poss</ta>
            <ta e="T964" id="Seg_10910" s="T963">%%</ta>
            <ta e="T965" id="Seg_10911" s="T964">v-v:tense-v:pn</ta>
            <ta e="T966" id="Seg_10912" s="T965">n-n:case</ta>
            <ta e="T967" id="Seg_10913" s="T966">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T968" id="Seg_10914" s="T967">adv</ta>
            <ta e="T969" id="Seg_10915" s="T968">n-n:case.poss</ta>
            <ta e="T970" id="Seg_10916" s="T969">v-v:tense-v:pn</ta>
            <ta e="T971" id="Seg_10917" s="T970">n-n:case</ta>
            <ta e="T972" id="Seg_10918" s="T971">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T973" id="Seg_10919" s="T972">adv</ta>
            <ta e="T975" id="Seg_10920" s="T974">v-v:tense-v:pn</ta>
            <ta e="T977" id="Seg_10921" s="T976">n-n:case</ta>
            <ta e="T978" id="Seg_10922" s="T977">v-v:tense-v:pn</ta>
            <ta e="T979" id="Seg_10923" s="T978">pers</ta>
            <ta e="T980" id="Seg_10924" s="T979">ptcl</ta>
            <ta e="T981" id="Seg_10925" s="T980">v-v:n.fin</ta>
            <ta e="T982" id="Seg_10926" s="T981">pers</ta>
            <ta e="T983" id="Seg_10927" s="T982">v-v:tense-v:pn</ta>
            <ta e="T984" id="Seg_10928" s="T983">dempro-n:case</ta>
            <ta e="T985" id="Seg_10929" s="T984">v-v:tense-v:pn</ta>
            <ta e="T986" id="Seg_10930" s="T985">pers</ta>
            <ta e="T987" id="Seg_10931" s="T986">adv</ta>
            <ta e="T990" id="Seg_10932" s="T989">v-v:tense-v:pn</ta>
            <ta e="T991" id="Seg_10933" s="T990">n-n:case</ta>
            <ta e="T992" id="Seg_10934" s="T991">v-v:n.fin</ta>
            <ta e="T994" id="Seg_10935" s="T993">adv</ta>
            <ta e="T995" id="Seg_10936" s="T994">v-v:n.fin</ta>
            <ta e="T996" id="Seg_10937" s="T995">n-n:case</ta>
            <ta e="T998" id="Seg_10938" s="T997">n-n:num</ta>
            <ta e="T999" id="Seg_10939" s="T998">v-v:tense-v:pn</ta>
            <ta e="T1000" id="Seg_10940" s="T999">n-n:case</ta>
            <ta e="T1001" id="Seg_10941" s="T1000">n-n:case</ta>
            <ta e="T1002" id="Seg_10942" s="T1001">v-v:tense-v:pn</ta>
            <ta e="T1004" id="Seg_10943" s="T1003">ptcl</ta>
            <ta e="T1005" id="Seg_10944" s="T1004">v-v:n.fin</ta>
            <ta e="T1006" id="Seg_10945" s="T1005">ptcl</ta>
            <ta e="T1007" id="Seg_10946" s="T1006">n-n:num</ta>
            <ta e="T1008" id="Seg_10947" s="T1007">adv</ta>
            <ta e="T1009" id="Seg_10948" s="T1008">v-v:tense-v:pn</ta>
            <ta e="T1010" id="Seg_10949" s="T1009">n-n:case</ta>
            <ta e="T1011" id="Seg_10950" s="T1010">ptcl</ta>
            <ta e="T1012" id="Seg_10951" s="T1011">v-v:tense-v:pn</ta>
            <ta e="T1013" id="Seg_10952" s="T1012">v-v:n.fin</ta>
            <ta e="T1014" id="Seg_10953" s="T1013">ptcl</ta>
            <ta e="T1015" id="Seg_10954" s="T1014">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps-PKZ">
            <ta e="T1" id="Seg_10955" s="T0">pers</ta>
            <ta e="T2" id="Seg_10956" s="T1">v</ta>
            <ta e="T3" id="Seg_10957" s="T2">num</ta>
            <ta e="T4" id="Seg_10958" s="T3">n</ta>
            <ta e="T5" id="Seg_10959" s="T4">v</ta>
            <ta e="T6" id="Seg_10960" s="T5">pers</ta>
            <ta e="T7" id="Seg_10961" s="T6">adv</ta>
            <ta e="T8" id="Seg_10962" s="T7">num</ta>
            <ta e="T9" id="Seg_10963" s="T8">v</ta>
            <ta e="T10" id="Seg_10964" s="T9">dempro</ta>
            <ta e="T11" id="Seg_10965" s="T10">num</ta>
            <ta e="T12" id="Seg_10966" s="T11">n</ta>
            <ta e="T13" id="Seg_10967" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_10968" s="T13">v</ta>
            <ta e="T15" id="Seg_10969" s="T14">v</ta>
            <ta e="T16" id="Seg_10970" s="T15">conj</ta>
            <ta e="T776" id="Seg_10971" s="T16">v</ta>
            <ta e="T17" id="Seg_10972" s="T776">v</ta>
            <ta e="T18" id="Seg_10973" s="T17">adv</ta>
            <ta e="T21" id="Seg_10974" s="T20">n</ta>
            <ta e="T22" id="Seg_10975" s="T21">n</ta>
            <ta e="T23" id="Seg_10976" s="T22">v</ta>
            <ta e="T24" id="Seg_10977" s="T23">n</ta>
            <ta e="T25" id="Seg_10978" s="T24">n</ta>
            <ta e="T26" id="Seg_10979" s="T25">v</ta>
            <ta e="T27" id="Seg_10980" s="T26">adv</ta>
            <ta e="T28" id="Seg_10981" s="T27">n</ta>
            <ta e="T29" id="Seg_10982" s="T28">v</ta>
            <ta e="T30" id="Seg_10983" s="T29">n</ta>
            <ta e="T31" id="Seg_10984" s="T30">v</ta>
            <ta e="T32" id="Seg_10985" s="T31">n</ta>
            <ta e="T33" id="Seg_10986" s="T32">n</ta>
            <ta e="T34" id="Seg_10987" s="T33">v</ta>
            <ta e="T35" id="Seg_10988" s="T34">conj</ta>
            <ta e="T36" id="Seg_10989" s="T35">adv</ta>
            <ta e="T37" id="Seg_10990" s="T36">v</ta>
            <ta e="T38" id="Seg_10991" s="T37">pers</ta>
            <ta e="T39" id="Seg_10992" s="T38">n</ta>
            <ta e="T40" id="Seg_10993" s="T39">n</ta>
            <ta e="T41" id="Seg_10994" s="T40">v</ta>
            <ta e="T42" id="Seg_10995" s="T41">num</ta>
            <ta e="T44" id="Seg_10996" s="T43">n</ta>
            <ta e="T45" id="Seg_10997" s="T44">v</ta>
            <ta e="T46" id="Seg_10998" s="T45">pers</ta>
            <ta e="T47" id="Seg_10999" s="T46">dempro</ta>
            <ta e="T48" id="Seg_11000" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_11001" s="T48">v</ta>
            <ta e="T50" id="Seg_11002" s="T49">n</ta>
            <ta e="T51" id="Seg_11003" s="T50">v</ta>
            <ta e="T52" id="Seg_11004" s="T51">conj</ta>
            <ta e="T53" id="Seg_11005" s="T52">v</ta>
            <ta e="T55" id="Seg_11006" s="T54">n</ta>
            <ta e="T56" id="Seg_11007" s="T55">num</ta>
            <ta e="T60" id="Seg_11008" s="T59">num</ta>
            <ta e="T61" id="Seg_11009" s="T60">pers</ta>
            <ta e="T62" id="Seg_11010" s="T61">v</ta>
            <ta e="T63" id="Seg_11011" s="T62">adv</ta>
            <ta e="T64" id="Seg_11012" s="T63">dempro</ta>
            <ta e="T65" id="Seg_11013" s="T64">v</ta>
            <ta e="T66" id="Seg_11014" s="T65">n</ta>
            <ta e="T67" id="Seg_11015" s="T66">adv</ta>
            <ta e="T68" id="Seg_11016" s="T67">v</ta>
            <ta e="T69" id="Seg_11017" s="T68">dempro</ta>
            <ta e="T70" id="Seg_11018" s="T69">n</ta>
            <ta e="T71" id="Seg_11019" s="T70">v</ta>
            <ta e="T73" id="Seg_11020" s="T72">pers</ta>
            <ta e="T75" id="Seg_11021" s="T74">n</ta>
            <ta e="T77" id="Seg_11022" s="T76">v</ta>
            <ta e="T78" id="Seg_11023" s="T77">n</ta>
            <ta e="T82" id="Seg_11024" s="T81">v</ta>
            <ta e="T83" id="Seg_11025" s="T82">v</ta>
            <ta e="T84" id="Seg_11026" s="T83">v</ta>
            <ta e="T85" id="Seg_11027" s="T84">v</ta>
            <ta e="T86" id="Seg_11028" s="T85">n</ta>
            <ta e="T88" id="Seg_11029" s="T87">v</ta>
            <ta e="T89" id="Seg_11030" s="T88">adv</ta>
            <ta e="T90" id="Seg_11031" s="T89">pers</ta>
            <ta e="T91" id="Seg_11032" s="T90">v</ta>
            <ta e="T92" id="Seg_11033" s="T91">pers</ta>
            <ta e="T93" id="Seg_11034" s="T92">v</ta>
            <ta e="T94" id="Seg_11035" s="T93">pers</ta>
            <ta e="T95" id="Seg_11036" s="T94">conj</ta>
            <ta e="T96" id="Seg_11037" s="T95">ptcl</ta>
            <ta e="T97" id="Seg_11038" s="T96">v</ta>
            <ta e="T99" id="Seg_11039" s="T98">num</ta>
            <ta e="T100" id="Seg_11040" s="T99">n</ta>
            <ta e="T101" id="Seg_11041" s="T100">v</ta>
            <ta e="T102" id="Seg_11042" s="T101">conj</ta>
            <ta e="T103" id="Seg_11043" s="T102">adv</ta>
            <ta e="T104" id="Seg_11044" s="T103">v</ta>
            <ta e="T105" id="Seg_11045" s="T104">n</ta>
            <ta e="T106" id="Seg_11046" s="T105">v</ta>
            <ta e="T107" id="Seg_11047" s="T106">dempro</ta>
            <ta e="T108" id="Seg_11048" s="T107">n</ta>
            <ta e="T109" id="Seg_11049" s="T108">v</ta>
            <ta e="T111" id="Seg_11050" s="T110">v</ta>
            <ta e="T112" id="Seg_11051" s="T111">n</ta>
            <ta e="T113" id="Seg_11052" s="T112">v</ta>
            <ta e="T114" id="Seg_11053" s="T113">v</ta>
            <ta e="T115" id="Seg_11054" s="T114">v</ta>
            <ta e="T116" id="Seg_11055" s="T115">n</ta>
            <ta e="T117" id="Seg_11056" s="T116">dempro</ta>
            <ta e="T118" id="Seg_11057" s="T117">v</ta>
            <ta e="T119" id="Seg_11058" s="T118">adv</ta>
            <ta e="T120" id="Seg_11059" s="T119">n</ta>
            <ta e="T121" id="Seg_11060" s="T120">v</ta>
            <ta e="T123" id="Seg_11061" s="T122">v</ta>
            <ta e="T125" id="Seg_11062" s="T124">adv</ta>
            <ta e="T126" id="Seg_11063" s="T125">n</ta>
            <ta e="T127" id="Seg_11064" s="T126">v</ta>
            <ta e="T128" id="Seg_11065" s="T127">conj</ta>
            <ta e="T129" id="Seg_11066" s="T128">dempro</ta>
            <ta e="T130" id="Seg_11067" s="T129">v</ta>
            <ta e="T131" id="Seg_11068" s="T130">pers</ta>
            <ta e="T132" id="Seg_11069" s="T131">ptcl</ta>
            <ta e="T133" id="Seg_11070" s="T132">v</ta>
            <ta e="T134" id="Seg_11071" s="T133">dempro</ta>
            <ta e="T135" id="Seg_11072" s="T134">n</ta>
            <ta e="T140" id="Seg_11073" s="T139">v</ta>
            <ta e="T142" id="Seg_11074" s="T141">num</ta>
            <ta e="T144" id="Seg_11075" s="T143">v</ta>
            <ta e="T145" id="Seg_11076" s="T144">dempro</ta>
            <ta e="T1017" id="Seg_11077" s="T146">v</ta>
            <ta e="T147" id="Seg_11078" s="T1017">v</ta>
            <ta e="T148" id="Seg_11079" s="T147">conj</ta>
            <ta e="T149" id="Seg_11080" s="T148">n</ta>
            <ta e="T150" id="Seg_11081" s="T149">ptcl</ta>
            <ta e="T151" id="Seg_11082" s="T150">v</ta>
            <ta e="T153" id="Seg_11083" s="T151">adv</ta>
            <ta e="T154" id="Seg_11084" s="T153">adv</ta>
            <ta e="T155" id="Seg_11085" s="T154">num</ta>
            <ta e="T156" id="Seg_11086" s="T155">adv</ta>
            <ta e="T157" id="Seg_11087" s="T156">v</ta>
            <ta e="T158" id="Seg_11088" s="T157">conj</ta>
            <ta e="T159" id="Seg_11089" s="T158">dempro</ta>
            <ta e="T160" id="Seg_11090" s="T159">v</ta>
            <ta e="T161" id="Seg_11091" s="T160">v</ta>
            <ta e="T163" id="Seg_11092" s="T162">dempro</ta>
            <ta e="T164" id="Seg_11093" s="T163">v</ta>
            <ta e="T165" id="Seg_11094" s="T164">pers</ta>
            <ta e="T166" id="Seg_11095" s="T165">ptcl</ta>
            <ta e="T167" id="Seg_11096" s="T166">v</ta>
            <ta e="T168" id="Seg_11097" s="T167">ptcl</ta>
            <ta e="T169" id="Seg_11098" s="T168">adv</ta>
            <ta e="T170" id="Seg_11099" s="T169">adv</ta>
            <ta e="T171" id="Seg_11100" s="T170">ptcl</ta>
            <ta e="T172" id="Seg_11101" s="T171">v</ta>
            <ta e="T173" id="Seg_11102" s="T172">n</ta>
            <ta e="T174" id="Seg_11103" s="T173">conj</ta>
            <ta e="T175" id="Seg_11104" s="T174">v</ta>
            <ta e="T176" id="Seg_11105" s="T175">ptcl</ta>
            <ta e="T177" id="Seg_11106" s="T176">v</ta>
            <ta e="T178" id="Seg_11107" s="T177">n</ta>
            <ta e="T179" id="Seg_11108" s="T178">adv</ta>
            <ta e="T180" id="Seg_11109" s="T179">num</ta>
            <ta e="T181" id="Seg_11110" s="T180">adv</ta>
            <ta e="T182" id="Seg_11111" s="T181">v</ta>
            <ta e="T183" id="Seg_11112" s="T182">v</ta>
            <ta e="T185" id="Seg_11113" s="T184">ptcl</ta>
            <ta e="T186" id="Seg_11114" s="T185">dempro</ta>
            <ta e="T187" id="Seg_11115" s="T186">v</ta>
            <ta e="T188" id="Seg_11116" s="T187">v</ta>
            <ta e="T189" id="Seg_11117" s="T188">num</ta>
            <ta e="T190" id="Seg_11118" s="T189">v</ta>
            <ta e="T191" id="Seg_11119" s="T190">n</ta>
            <ta e="T192" id="Seg_11120" s="T191">adv</ta>
            <ta e="T193" id="Seg_11121" s="T192">v</ta>
            <ta e="T194" id="Seg_11122" s="T193">n</ta>
            <ta e="T195" id="Seg_11123" s="T194">v</ta>
            <ta e="T196" id="Seg_11124" s="T195">adv</ta>
            <ta e="T197" id="Seg_11125" s="T196">n</ta>
            <ta e="T198" id="Seg_11126" s="T197">v</ta>
            <ta e="T199" id="Seg_11127" s="T198">dempro</ta>
            <ta e="T200" id="Seg_11128" s="T199">ptcl</ta>
            <ta e="T201" id="Seg_11129" s="T200">n</ta>
            <ta e="T1018" id="Seg_11130" s="T201">v</ta>
            <ta e="T202" id="Seg_11131" s="T1018">v</ta>
            <ta e="T203" id="Seg_11132" s="T202">adv</ta>
            <ta e="T204" id="Seg_11133" s="T203">n</ta>
            <ta e="T205" id="Seg_11134" s="T204">v</ta>
            <ta e="T206" id="Seg_11135" s="T205">n</ta>
            <ta e="T207" id="Seg_11136" s="T206">v</ta>
            <ta e="T208" id="Seg_11137" s="T207">n</ta>
            <ta e="T209" id="Seg_11138" s="T208">v</ta>
            <ta e="T210" id="Seg_11139" s="T209">adv</ta>
            <ta e="T211" id="Seg_11140" s="T210">v</ta>
            <ta e="T212" id="Seg_11141" s="T211">n</ta>
            <ta e="T213" id="Seg_11142" s="T212">n</ta>
            <ta e="T214" id="Seg_11143" s="T213">adj</ta>
            <ta e="T215" id="Seg_11144" s="T214">conj</ta>
            <ta e="T216" id="Seg_11145" s="T215">ptcl</ta>
            <ta e="T217" id="Seg_11146" s="T216">n</ta>
            <ta e="T218" id="Seg_11147" s="T217">v</ta>
            <ta e="T219" id="Seg_11148" s="T218">dempro</ta>
            <ta e="T221" id="Seg_11149" s="T220">v</ta>
            <ta e="T222" id="Seg_11150" s="T221">n</ta>
            <ta e="T223" id="Seg_11151" s="T222">dempro</ta>
            <ta e="T224" id="Seg_11152" s="T223">ptcl</ta>
            <ta e="T226" id="Seg_11153" s="T224">dempro</ta>
            <ta e="T229" id="Seg_11154" s="T228">adv</ta>
            <ta e="T230" id="Seg_11155" s="T229">dempro</ta>
            <ta e="T231" id="Seg_11156" s="T230">v</ta>
            <ta e="T232" id="Seg_11157" s="T231">n</ta>
            <ta e="T233" id="Seg_11158" s="T232">n</ta>
            <ta e="T235" id="Seg_11159" s="T234">v</ta>
            <ta e="T236" id="Seg_11160" s="T235">adv</ta>
            <ta e="T237" id="Seg_11161" s="T236">n</ta>
            <ta e="T239" id="Seg_11162" s="T238">v</ta>
            <ta e="T240" id="Seg_11163" s="T239">adv</ta>
            <ta e="T241" id="Seg_11164" s="T240">n</ta>
            <ta e="T242" id="Seg_11165" s="T241">v</ta>
            <ta e="T243" id="Seg_11166" s="T242">n</ta>
            <ta e="T244" id="Seg_11167" s="T243">n</ta>
            <ta e="T245" id="Seg_11168" s="T244">adj</ta>
            <ta e="T246" id="Seg_11169" s="T245">pers</ta>
            <ta e="T248" id="Seg_11170" s="T247">pers</ta>
            <ta e="T249" id="Seg_11171" s="T248">n</ta>
            <ta e="T250" id="Seg_11172" s="T249">pers</ta>
            <ta e="T252" id="Seg_11173" s="T251">n</ta>
            <ta e="T253" id="Seg_11174" s="T252">pers</ta>
            <ta e="T254" id="Seg_11175" s="T253">n</ta>
            <ta e="T255" id="Seg_11176" s="T254">v</ta>
            <ta e="T1019" id="Seg_11177" s="T255">v</ta>
            <ta e="T256" id="Seg_11178" s="T1019">v</ta>
            <ta e="T258" id="Seg_11179" s="T257">adv</ta>
            <ta e="T259" id="Seg_11180" s="T258">dempro</ta>
            <ta e="T260" id="Seg_11181" s="T259">n</ta>
            <ta e="T261" id="Seg_11182" s="T260">v</ta>
            <ta e="T262" id="Seg_11183" s="T261">aux</ta>
            <ta e="T263" id="Seg_11184" s="T262">v</ta>
            <ta e="T264" id="Seg_11185" s="T263">conj</ta>
            <ta e="T265" id="Seg_11186" s="T264">dempro</ta>
            <ta e="T267" id="Seg_11187" s="T266">dempro</ta>
            <ta e="T269" id="Seg_11188" s="T268">n</ta>
            <ta e="T270" id="Seg_11189" s="T269">dempro</ta>
            <ta e="T271" id="Seg_11190" s="T270">n</ta>
            <ta e="T273" id="Seg_11191" s="T272">v</ta>
            <ta e="T274" id="Seg_11192" s="T273">conj</ta>
            <ta e="T275" id="Seg_11193" s="T274">n</ta>
            <ta e="T276" id="Seg_11194" s="T275">v</ta>
            <ta e="T277" id="Seg_11195" s="T276">conj</ta>
            <ta e="T1020" id="Seg_11196" s="T277">v</ta>
            <ta e="T278" id="Seg_11197" s="T1020">v</ta>
            <ta e="T280" id="Seg_11198" s="T279">pers</ta>
            <ta e="T281" id="Seg_11199" s="T280">n</ta>
            <ta e="T282" id="Seg_11200" s="T281">pers</ta>
            <ta e="T283" id="Seg_11201" s="T282">n</ta>
            <ta e="T286" id="Seg_11202" s="T285">v</ta>
            <ta e="T287" id="Seg_11203" s="T286">conj</ta>
            <ta e="T1021" id="Seg_11204" s="T287">v</ta>
            <ta e="T288" id="Seg_11205" s="T1021">v</ta>
            <ta e="T289" id="Seg_11206" s="T288">pers</ta>
            <ta e="T290" id="Seg_11207" s="T289">adj</ta>
            <ta e="T291" id="Seg_11208" s="T290">v</ta>
            <ta e="T292" id="Seg_11209" s="T291">conj</ta>
            <ta e="T293" id="Seg_11210" s="T292">dempro</ta>
            <ta e="T294" id="Seg_11211" s="T293">pers</ta>
            <ta e="T295" id="Seg_11212" s="T294">v</ta>
            <ta e="T296" id="Seg_11213" s="T295">n</ta>
            <ta e="T297" id="Seg_11214" s="T296">v</ta>
            <ta e="T298" id="Seg_11215" s="T297">conj</ta>
            <ta e="T299" id="Seg_11216" s="T298">adv</ta>
            <ta e="T1022" id="Seg_11217" s="T299">v</ta>
            <ta e="T300" id="Seg_11218" s="T1022">v</ta>
            <ta e="T301" id="Seg_11219" s="T300">propr</ta>
            <ta e="T302" id="Seg_11220" s="T301">conj</ta>
            <ta e="T303" id="Seg_11221" s="T302">adv</ta>
            <ta e="T304" id="Seg_11222" s="T303">v</ta>
            <ta e="T306" id="Seg_11223" s="T305">conj</ta>
            <ta e="T307" id="Seg_11224" s="T306">dempro</ta>
            <ta e="T308" id="Seg_11225" s="T307">ptcl</ta>
            <ta e="T309" id="Seg_11226" s="T308">adj</ta>
            <ta e="T310" id="Seg_11227" s="T309">v</ta>
            <ta e="T311" id="Seg_11228" s="T310">n</ta>
            <ta e="T313" id="Seg_11229" s="T311">ptcl</ta>
            <ta e="T315" id="Seg_11230" s="T314">dempro</ta>
            <ta e="T316" id="Seg_11231" s="T315">ptcl</ta>
            <ta e="T317" id="Seg_11232" s="T316">adj</ta>
            <ta e="T318" id="Seg_11233" s="T317">n</ta>
            <ta e="T319" id="Seg_11234" s="T318">adj</ta>
            <ta e="T320" id="Seg_11235" s="T319">n</ta>
            <ta e="T321" id="Seg_11236" s="T320">adj</ta>
            <ta e="T323" id="Seg_11237" s="T322">num</ta>
            <ta e="T324" id="Seg_11238" s="T323">n</ta>
            <ta e="T325" id="Seg_11239" s="T324">v</ta>
            <ta e="T326" id="Seg_11240" s="T325">v</ta>
            <ta e="T327" id="Seg_11241" s="T326">n</ta>
            <ta e="T329" id="Seg_11242" s="T328">v</ta>
            <ta e="T330" id="Seg_11243" s="T329">num</ta>
            <ta e="T331" id="Seg_11244" s="T330">v</ta>
            <ta e="T332" id="Seg_11245" s="T331">quant</ta>
            <ta e="T333" id="Seg_11246" s="T332">conj</ta>
            <ta e="T334" id="Seg_11247" s="T333">num</ta>
            <ta e="T335" id="Seg_11248" s="T334">adv</ta>
            <ta e="T336" id="Seg_11249" s="T335">que</ta>
            <ta e="T337" id="Seg_11250" s="T336">quant</ta>
            <ta e="T338" id="Seg_11251" s="T337">v</ta>
            <ta e="T339" id="Seg_11252" s="T338">conj</ta>
            <ta e="T340" id="Seg_11253" s="T339">que</ta>
            <ta e="T341" id="Seg_11254" s="T340">adv</ta>
            <ta e="T342" id="Seg_11255" s="T341">v</ta>
            <ta e="T343" id="Seg_11256" s="T342">dempro</ta>
            <ta e="T345" id="Seg_11257" s="T344">n</ta>
            <ta e="T346" id="Seg_11258" s="T345">n</ta>
            <ta e="T347" id="Seg_11259" s="T346">v</ta>
            <ta e="T348" id="Seg_11260" s="T347">adv</ta>
            <ta e="T349" id="Seg_11261" s="T348">v</ta>
            <ta e="T350" id="Seg_11262" s="T349">n</ta>
            <ta e="T351" id="Seg_11263" s="T350">n</ta>
            <ta e="T352" id="Seg_11264" s="T351">v</ta>
            <ta e="T353" id="Seg_11265" s="T352">que</ta>
            <ta e="T354" id="Seg_11266" s="T353">pers</ta>
            <ta e="T355" id="Seg_11267" s="T354">n</ta>
            <ta e="T356" id="Seg_11268" s="T355">dempro</ta>
            <ta e="T357" id="Seg_11269" s="T356">ptcl</ta>
            <ta e="T358" id="Seg_11270" s="T357">adj</ta>
            <ta e="T359" id="Seg_11271" s="T358">ptcl</ta>
            <ta e="T360" id="Seg_11272" s="T359">v</ta>
            <ta e="T361" id="Seg_11273" s="T360">n</ta>
            <ta e="T362" id="Seg_11274" s="T361">adv</ta>
            <ta e="T363" id="Seg_11275" s="T362">ptcl</ta>
            <ta e="T364" id="Seg_11276" s="T363">v</ta>
            <ta e="T365" id="Seg_11277" s="T364">conj</ta>
            <ta e="T366" id="Seg_11278" s="T365">n</ta>
            <ta e="T367" id="Seg_11279" s="T366">que</ta>
            <ta e="T368" id="Seg_11280" s="T367">que</ta>
            <ta e="T372" id="Seg_11281" s="T371">v</ta>
            <ta e="T373" id="Seg_11282" s="T372">conj</ta>
            <ta e="T374" id="Seg_11283" s="T373">v</ta>
            <ta e="T375" id="Seg_11284" s="T374">n</ta>
            <ta e="T376" id="Seg_11285" s="T375">v</ta>
            <ta e="T377" id="Seg_11286" s="T376">dempro</ta>
            <ta e="T378" id="Seg_11287" s="T377">dempro</ta>
            <ta e="T379" id="Seg_11288" s="T378">v</ta>
            <ta e="T380" id="Seg_11289" s="T379">v</ta>
            <ta e="T382" id="Seg_11290" s="T381">v</ta>
            <ta e="T383" id="Seg_11291" s="T382">v</ta>
            <ta e="T384" id="Seg_11292" s="T383">conj</ta>
            <ta e="T385" id="Seg_11293" s="T384">refl</ta>
            <ta e="T386" id="Seg_11294" s="T385">adv</ta>
            <ta e="T389" id="Seg_11295" s="T388">v</ta>
            <ta e="T390" id="Seg_11296" s="T389">n</ta>
            <ta e="T392" id="Seg_11297" s="T391">v</ta>
            <ta e="T393" id="Seg_11298" s="T392">adv</ta>
            <ta e="T395" id="Seg_11299" s="T394">dempro</ta>
            <ta e="T396" id="Seg_11300" s="T395">n</ta>
            <ta e="T397" id="Seg_11301" s="T396">v</ta>
            <ta e="T398" id="Seg_11302" s="T397">que</ta>
            <ta e="T399" id="Seg_11303" s="T398">pers</ta>
            <ta e="T401" id="Seg_11304" s="T400">dempro</ta>
            <ta e="T402" id="Seg_11305" s="T401">v</ta>
            <ta e="T403" id="Seg_11306" s="T402">pers</ta>
            <ta e="T404" id="Seg_11307" s="T403">adv</ta>
            <ta e="T405" id="Seg_11308" s="T404">v</ta>
            <ta e="T406" id="Seg_11309" s="T405">dempro</ta>
            <ta e="T410" id="Seg_11310" s="T409">v</ta>
            <ta e="T411" id="Seg_11311" s="T410">n</ta>
            <ta e="T413" id="Seg_11312" s="T412">que</ta>
            <ta e="T414" id="Seg_11313" s="T413">ptcl</ta>
            <ta e="T415" id="Seg_11314" s="T414">v</ta>
            <ta e="T416" id="Seg_11315" s="T415">adv</ta>
            <ta e="T417" id="Seg_11316" s="T416">n</ta>
            <ta e="T418" id="Seg_11317" s="T417">que</ta>
            <ta e="T419" id="Seg_11318" s="T418">ptcl</ta>
            <ta e="T420" id="Seg_11319" s="T419">v</ta>
            <ta e="T421" id="Seg_11320" s="T420">dempro</ta>
            <ta e="T422" id="Seg_11321" s="T421">v</ta>
            <ta e="T423" id="Seg_11322" s="T422">n</ta>
            <ta e="T424" id="Seg_11323" s="T423">n</ta>
            <ta e="T425" id="Seg_11324" s="T424">v</ta>
            <ta e="T426" id="Seg_11325" s="T425">conj</ta>
            <ta e="T1023" id="Seg_11326" s="T426">v</ta>
            <ta e="T427" id="Seg_11327" s="T1023">v</ta>
            <ta e="T428" id="Seg_11328" s="T427">que</ta>
            <ta e="T429" id="Seg_11329" s="T428">n</ta>
            <ta e="T430" id="Seg_11330" s="T429">quant</ta>
            <ta e="T432" id="Seg_11331" s="T431">pers</ta>
            <ta e="T433" id="Seg_11332" s="T432">n</ta>
            <ta e="T1024" id="Seg_11333" s="T433">v</ta>
            <ta e="T434" id="Seg_11334" s="T1024">v</ta>
            <ta e="T435" id="Seg_11335" s="T434">pers</ta>
            <ta e="T436" id="Seg_11336" s="T435">adv</ta>
            <ta e="T437" id="Seg_11337" s="T436">adv</ta>
            <ta e="T438" id="Seg_11338" s="T437">v</ta>
            <ta e="T439" id="Seg_11339" s="T438">n</ta>
            <ta e="T440" id="Seg_11340" s="T439">ptcl</ta>
            <ta e="T441" id="Seg_11341" s="T440">v</ta>
            <ta e="T442" id="Seg_11342" s="T441">conj</ta>
            <ta e="T443" id="Seg_11343" s="T442">n</ta>
            <ta e="T447" id="Seg_11344" s="T446">pers</ta>
            <ta e="T449" id="Seg_11345" s="T448">n</ta>
            <ta e="T450" id="Seg_11346" s="T449">n</ta>
            <ta e="T451" id="Seg_11347" s="T450">n</ta>
            <ta e="T452" id="Seg_11348" s="T451">n</ta>
            <ta e="T453" id="Seg_11349" s="T452">v</ta>
            <ta e="T454" id="Seg_11350" s="T453">n</ta>
            <ta e="T456" id="Seg_11351" s="T455">n</ta>
            <ta e="T457" id="Seg_11352" s="T456">v</ta>
            <ta e="T458" id="Seg_11353" s="T457">n</ta>
            <ta e="T459" id="Seg_11354" s="T458">v</ta>
            <ta e="T460" id="Seg_11355" s="T459">adv</ta>
            <ta e="T461" id="Seg_11356" s="T460">n</ta>
            <ta e="T462" id="Seg_11357" s="T461">v</ta>
            <ta e="T463" id="Seg_11358" s="T462">refl</ta>
            <ta e="T464" id="Seg_11359" s="T463">n</ta>
            <ta e="T465" id="Seg_11360" s="T464">v</ta>
            <ta e="T466" id="Seg_11361" s="T465">conj</ta>
            <ta e="T467" id="Seg_11362" s="T466">adv</ta>
            <ta e="T469" id="Seg_11363" s="T468">v</ta>
            <ta e="T470" id="Seg_11364" s="T469">conj</ta>
            <ta e="T471" id="Seg_11365" s="T470">pers</ta>
            <ta e="T472" id="Seg_11366" s="T471">v</ta>
            <ta e="T473" id="Seg_11367" s="T472">conj</ta>
            <ta e="T474" id="Seg_11368" s="T473">v</ta>
            <ta e="T476" id="Seg_11369" s="T475">conj</ta>
            <ta e="T477" id="Seg_11370" s="T476">pers</ta>
            <ta e="T479" id="Seg_11371" s="T478">dempro</ta>
            <ta e="T480" id="Seg_11372" s="T479">v</ta>
            <ta e="T483" id="Seg_11373" s="T482">conj</ta>
            <ta e="T484" id="Seg_11374" s="T483">v</ta>
            <ta e="T485" id="Seg_11375" s="T484">n</ta>
            <ta e="T486" id="Seg_11376" s="T485">pers</ta>
            <ta e="T487" id="Seg_11377" s="T486">quant</ta>
            <ta e="T488" id="Seg_11378" s="T487">v</ta>
            <ta e="T490" id="Seg_11379" s="T489">num</ta>
            <ta e="T491" id="Seg_11380" s="T490">n</ta>
            <ta e="T492" id="Seg_11381" s="T491">v</ta>
            <ta e="T494" id="Seg_11382" s="T493">v</ta>
            <ta e="T495" id="Seg_11383" s="T494">adj</ta>
            <ta e="T496" id="Seg_11384" s="T495">n</ta>
            <ta e="T497" id="Seg_11385" s="T496">adv</ta>
            <ta e="T498" id="Seg_11386" s="T497">n</ta>
            <ta e="T499" id="Seg_11387" s="T498">quant</ta>
            <ta e="T500" id="Seg_11388" s="T499">v</ta>
            <ta e="T501" id="Seg_11389" s="T500">adv</ta>
            <ta e="T502" id="Seg_11390" s="T501">n</ta>
            <ta e="T503" id="Seg_11391" s="T502">dempro</ta>
            <ta e="T504" id="Seg_11392" s="T503">v</ta>
            <ta e="T505" id="Seg_11393" s="T504">que</ta>
            <ta e="T506" id="Seg_11394" s="T505">v</ta>
            <ta e="T507" id="Seg_11395" s="T506">v</ta>
            <ta e="T508" id="Seg_11396" s="T507">aux</ta>
            <ta e="T509" id="Seg_11397" s="T508">pers</ta>
            <ta e="T510" id="Seg_11398" s="T509">conj</ta>
            <ta e="T511" id="Seg_11399" s="T510">pers</ta>
            <ta e="T512" id="Seg_11400" s="T511">que</ta>
            <ta e="T513" id="Seg_11401" s="T512">v</ta>
            <ta e="T514" id="Seg_11402" s="T513">v</ta>
            <ta e="T515" id="Seg_11403" s="T514">conj</ta>
            <ta e="T516" id="Seg_11404" s="T515">v</ta>
            <ta e="T517" id="Seg_11405" s="T516">adv</ta>
            <ta e="T518" id="Seg_11406" s="T517">v</ta>
            <ta e="T519" id="Seg_11407" s="T518">adv</ta>
            <ta e="T520" id="Seg_11408" s="T519">adv</ta>
            <ta e="T521" id="Seg_11409" s="T520">n</ta>
            <ta e="T522" id="Seg_11410" s="T521">v</ta>
            <ta e="T523" id="Seg_11411" s="T522">que</ta>
            <ta e="T524" id="Seg_11412" s="T523">v</ta>
            <ta e="T525" id="Seg_11413" s="T524">v</ta>
            <ta e="T526" id="Seg_11414" s="T525">pers</ta>
            <ta e="T527" id="Seg_11415" s="T526">v</ta>
            <ta e="T528" id="Seg_11416" s="T527">conj</ta>
            <ta e="T529" id="Seg_11417" s="T528">pers</ta>
            <ta e="T530" id="Seg_11418" s="T529">que</ta>
            <ta e="T531" id="Seg_11419" s="T530">v</ta>
            <ta e="T532" id="Seg_11420" s="T531">v</ta>
            <ta e="T533" id="Seg_11421" s="T532">conj</ta>
            <ta e="T534" id="Seg_11422" s="T533">v</ta>
            <ta e="T535" id="Seg_11423" s="T534">adv</ta>
            <ta e="T536" id="Seg_11424" s="T535">v</ta>
            <ta e="T537" id="Seg_11425" s="T536">conj</ta>
            <ta e="T538" id="Seg_11426" s="T537">ptcl</ta>
            <ta e="T539" id="Seg_11427" s="T538">v</ta>
            <ta e="T540" id="Seg_11428" s="T539">dempro</ta>
            <ta e="T541" id="Seg_11429" s="T540">v</ta>
            <ta e="T542" id="Seg_11430" s="T541">pers</ta>
            <ta e="T543" id="Seg_11431" s="T542">quant</ta>
            <ta e="T544" id="Seg_11432" s="T543">ptcl</ta>
            <ta e="T545" id="Seg_11433" s="T544">v</ta>
            <ta e="T546" id="Seg_11434" s="T545">n</ta>
            <ta e="T547" id="Seg_11435" s="T546">v</ta>
            <ta e="T548" id="Seg_11436" s="T547">n</ta>
            <ta e="T549" id="Seg_11437" s="T548">quant</ta>
            <ta e="T550" id="Seg_11438" s="T549">v</ta>
            <ta e="T551" id="Seg_11439" s="T550">ptcl</ta>
            <ta e="T552" id="Seg_11440" s="T551">dempro</ta>
            <ta e="T553" id="Seg_11441" s="T552">v</ta>
            <ta e="T554" id="Seg_11442" s="T553">ptcl</ta>
            <ta e="T555" id="Seg_11443" s="T554">quant</ta>
            <ta e="T556" id="Seg_11444" s="T555">pers</ta>
            <ta e="T557" id="Seg_11445" s="T556">v</ta>
            <ta e="T558" id="Seg_11446" s="T557">conj</ta>
            <ta e="T561" id="Seg_11447" s="T560">quant</ta>
            <ta e="T562" id="Seg_11448" s="T561">num</ta>
            <ta e="T563" id="Seg_11449" s="T562">num</ta>
            <ta e="T565" id="Seg_11450" s="T564">v</ta>
            <ta e="T566" id="Seg_11451" s="T565">ptcl</ta>
            <ta e="T567" id="Seg_11452" s="T566">adv</ta>
            <ta e="T569" id="Seg_11453" s="T568">n</ta>
            <ta e="T570" id="Seg_11454" s="T569">v</ta>
            <ta e="T571" id="Seg_11455" s="T570">adv</ta>
            <ta e="T572" id="Seg_11456" s="T571">v</ta>
            <ta e="T573" id="Seg_11457" s="T572">dempro</ta>
            <ta e="T574" id="Seg_11458" s="T573">n</ta>
            <ta e="T575" id="Seg_11459" s="T574">que</ta>
            <ta e="T576" id="Seg_11460" s="T575">v</ta>
            <ta e="T577" id="Seg_11461" s="T576">conj</ta>
            <ta e="T578" id="Seg_11462" s="T577">v</ta>
            <ta e="T579" id="Seg_11463" s="T578">ptcl</ta>
            <ta e="T580" id="Seg_11464" s="T579">v</ta>
            <ta e="T581" id="Seg_11465" s="T580">conj</ta>
            <ta e="T582" id="Seg_11466" s="T581">v</ta>
            <ta e="T583" id="Seg_11467" s="T582">v</ta>
            <ta e="T584" id="Seg_11468" s="T583">conj</ta>
            <ta e="T585" id="Seg_11469" s="T584">v</ta>
            <ta e="T586" id="Seg_11470" s="T585">quant</ta>
            <ta e="T587" id="Seg_11471" s="T586">que</ta>
            <ta e="T588" id="Seg_11472" s="T587">v</ta>
            <ta e="T589" id="Seg_11473" s="T588">conj</ta>
            <ta e="T590" id="Seg_11474" s="T589">n</ta>
            <ta e="T591" id="Seg_11475" s="T590">conj</ta>
            <ta e="T593" id="Seg_11476" s="T592">que</ta>
            <ta e="T594" id="Seg_11477" s="T593">v</ta>
            <ta e="T595" id="Seg_11478" s="T594">quant</ta>
            <ta e="T596" id="Seg_11479" s="T595">refl</ta>
            <ta e="T597" id="Seg_11480" s="T596">v</ta>
            <ta e="T598" id="Seg_11481" s="T597">v</ta>
            <ta e="T599" id="Seg_11482" s="T598">adv</ta>
            <ta e="T600" id="Seg_11483" s="T599">adv</ta>
            <ta e="T601" id="Seg_11484" s="T600">v</ta>
            <ta e="T602" id="Seg_11485" s="T601">adv</ta>
            <ta e="T603" id="Seg_11486" s="T602">adv</ta>
            <ta e="T604" id="Seg_11487" s="T603">adv</ta>
            <ta e="T605" id="Seg_11488" s="T604">ptcl</ta>
            <ta e="T606" id="Seg_11489" s="T605">v</ta>
            <ta e="T612" id="Seg_11490" s="T611">adv</ta>
            <ta e="T613" id="Seg_11491" s="T612">n</ta>
            <ta e="T615" id="Seg_11492" s="T614">v</ta>
            <ta e="T616" id="Seg_11493" s="T615">adv</ta>
            <ta e="T617" id="Seg_11494" s="T616">adv</ta>
            <ta e="T618" id="Seg_11495" s="T617">v</ta>
            <ta e="T619" id="Seg_11496" s="T618">dempro</ta>
            <ta e="T621" id="Seg_11497" s="T620">dempro</ta>
            <ta e="T622" id="Seg_11498" s="T621">n</ta>
            <ta e="T623" id="Seg_11499" s="T622">adv</ta>
            <ta e="T624" id="Seg_11500" s="T623">dempro</ta>
            <ta e="T626" id="Seg_11501" s="T625">v</ta>
            <ta e="T627" id="Seg_11502" s="T626">v</ta>
            <ta e="T628" id="Seg_11503" s="T627">conj</ta>
            <ta e="T629" id="Seg_11504" s="T628">v</ta>
            <ta e="T630" id="Seg_11505" s="T629">v</ta>
            <ta e="T631" id="Seg_11506" s="T630">conj</ta>
            <ta e="T632" id="Seg_11507" s="T631">v</ta>
            <ta e="T633" id="Seg_11508" s="T632">quant</ta>
            <ta e="T634" id="Seg_11509" s="T633">v</ta>
            <ta e="T635" id="Seg_11510" s="T634">conj</ta>
            <ta e="T636" id="Seg_11511" s="T635">v</ta>
            <ta e="T637" id="Seg_11512" s="T636">adv</ta>
            <ta e="T639" id="Seg_11513" s="T638">v</ta>
            <ta e="T640" id="Seg_11514" s="T639">pers</ta>
            <ta e="T641" id="Seg_11515" s="T640">adv</ta>
            <ta e="T642" id="Seg_11516" s="T641">conj</ta>
            <ta e="T643" id="Seg_11517" s="T642">n</ta>
            <ta e="T644" id="Seg_11518" s="T643">ptcl</ta>
            <ta e="T645" id="Seg_11519" s="T644">adv</ta>
            <ta e="T646" id="Seg_11520" s="T645">n</ta>
            <ta e="T647" id="Seg_11521" s="T646">quant</ta>
            <ta e="T649" id="Seg_11522" s="T648">adv</ta>
            <ta e="T650" id="Seg_11523" s="T649">ptcl</ta>
            <ta e="T651" id="Seg_11524" s="T650">v</ta>
            <ta e="T652" id="Seg_11525" s="T651">conj</ta>
            <ta e="T653" id="Seg_11526" s="T652">n</ta>
            <ta e="T654" id="Seg_11527" s="T653">ptcl</ta>
            <ta e="T655" id="Seg_11528" s="T654">v</ta>
            <ta e="T656" id="Seg_11529" s="T655">dempro</ta>
            <ta e="T657" id="Seg_11530" s="T656">ptcl</ta>
            <ta e="T658" id="Seg_11531" s="T657">n</ta>
            <ta e="T659" id="Seg_11532" s="T658">v</ta>
            <ta e="T660" id="Seg_11533" s="T659">ptcl</ta>
            <ta e="T661" id="Seg_11534" s="T660">conj</ta>
            <ta e="T662" id="Seg_11535" s="T661">v</ta>
            <ta e="T664" id="Seg_11536" s="T662">ptcl</ta>
            <ta e="T665" id="Seg_11537" s="T664">adv</ta>
            <ta e="T666" id="Seg_11538" s="T665">dempro</ta>
            <ta e="T667" id="Seg_11539" s="T666">n</ta>
            <ta e="T668" id="Seg_11540" s="T667">v</ta>
            <ta e="T669" id="Seg_11541" s="T668">conj</ta>
            <ta e="T670" id="Seg_11542" s="T669">n</ta>
            <ta e="T671" id="Seg_11543" s="T670">v</ta>
            <ta e="T674" id="Seg_11544" s="T673">que</ta>
            <ta e="T675" id="Seg_11545" s="T674">v</ta>
            <ta e="T677" id="Seg_11546" s="T676">conj</ta>
            <ta e="T678" id="Seg_11547" s="T677">que</ta>
            <ta e="T679" id="Seg_11548" s="T678">ptcl</ta>
            <ta e="T680" id="Seg_11549" s="T679">adj</ta>
            <ta e="T681" id="Seg_11550" s="T680">v</ta>
            <ta e="T682" id="Seg_11551" s="T681">ptcl</ta>
            <ta e="T684" id="Seg_11552" s="T683">adj</ta>
            <ta e="T686" id="Seg_11553" s="T685">v</ta>
            <ta e="T687" id="Seg_11554" s="T686">v</ta>
            <ta e="T690" id="Seg_11555" s="T689">num</ta>
            <ta e="T691" id="Seg_11556" s="T690">n</ta>
            <ta e="T693" id="Seg_11557" s="T692">v</ta>
            <ta e="T694" id="Seg_11558" s="T693">n</ta>
            <ta e="T695" id="Seg_11559" s="T694">conj</ta>
            <ta e="T696" id="Seg_11560" s="T695">conj</ta>
            <ta e="T697" id="Seg_11561" s="T696">num</ta>
            <ta e="T698" id="Seg_11562" s="T697">n</ta>
            <ta e="T699" id="Seg_11563" s="T698">adv</ta>
            <ta e="T700" id="Seg_11564" s="T699">n</ta>
            <ta e="T701" id="Seg_11565" s="T700">quant</ta>
            <ta e="T703" id="Seg_11566" s="T702">v</ta>
            <ta e="T704" id="Seg_11567" s="T703">conj</ta>
            <ta e="T705" id="Seg_11568" s="T704">n</ta>
            <ta e="T706" id="Seg_11569" s="T705">conj</ta>
            <ta e="T707" id="Seg_11570" s="T706">n</ta>
            <ta e="T708" id="Seg_11571" s="T707">dempro</ta>
            <ta e="T709" id="Seg_11572" s="T708">dempro</ta>
            <ta e="T710" id="Seg_11573" s="T709">n</ta>
            <ta e="T711" id="Seg_11574" s="T710">v</ta>
            <ta e="T712" id="Seg_11575" s="T711">n</ta>
            <ta e="T713" id="Seg_11576" s="T712">conj</ta>
            <ta e="T715" id="Seg_11577" s="T714">v</ta>
            <ta e="T716" id="Seg_11578" s="T715">dempro</ta>
            <ta e="T717" id="Seg_11579" s="T716">ptcl</ta>
            <ta e="T718" id="Seg_11580" s="T717">v</ta>
            <ta e="T719" id="Seg_11581" s="T718">conj</ta>
            <ta e="T720" id="Seg_11582" s="T719">n</ta>
            <ta e="T721" id="Seg_11583" s="T720">v</ta>
            <ta e="T722" id="Seg_11584" s="T721">que</ta>
            <ta e="T723" id="Seg_11585" s="T722">v</ta>
            <ta e="T724" id="Seg_11586" s="T723">v</ta>
            <ta e="T725" id="Seg_11587" s="T724">adv</ta>
            <ta e="T726" id="Seg_11588" s="T725">v</ta>
            <ta e="T727" id="Seg_11589" s="T726">n</ta>
            <ta e="T728" id="Seg_11590" s="T727">dempro</ta>
            <ta e="T730" id="Seg_11591" s="T729">v</ta>
            <ta e="T731" id="Seg_11592" s="T730">n</ta>
            <ta e="T732" id="Seg_11593" s="T731">v</ta>
            <ta e="T733" id="Seg_11594" s="T732">v</ta>
            <ta e="T734" id="Seg_11595" s="T733">n</ta>
            <ta e="T735" id="Seg_11596" s="T734">v</ta>
            <ta e="T736" id="Seg_11597" s="T735">n</ta>
            <ta e="T738" id="Seg_11598" s="T737">dempro</ta>
            <ta e="T739" id="Seg_11599" s="T738">n</ta>
            <ta e="T740" id="Seg_11600" s="T739">v</ta>
            <ta e="T741" id="Seg_11601" s="T740">v</ta>
            <ta e="T742" id="Seg_11602" s="T741">dempro</ta>
            <ta e="T743" id="Seg_11603" s="T742">n</ta>
            <ta e="T744" id="Seg_11604" s="T743">n</ta>
            <ta e="T745" id="Seg_11605" s="T744">v</ta>
            <ta e="T746" id="Seg_11606" s="T745">dempro</ta>
            <ta e="T747" id="Seg_11607" s="T746">adv</ta>
            <ta e="T748" id="Seg_11608" s="T747">v</ta>
            <ta e="T749" id="Seg_11609" s="T748">adv</ta>
            <ta e="T750" id="Seg_11610" s="T749">v</ta>
            <ta e="T752" id="Seg_11611" s="T751">v</ta>
            <ta e="T753" id="Seg_11612" s="T752">n</ta>
            <ta e="T754" id="Seg_11613" s="T753">v</ta>
            <ta e="T755" id="Seg_11614" s="T754">n</ta>
            <ta e="T756" id="Seg_11615" s="T755">adv</ta>
            <ta e="T757" id="Seg_11616" s="T756">v</ta>
            <ta e="T759" id="Seg_11617" s="T758">dempro</ta>
            <ta e="T760" id="Seg_11618" s="T759">n</ta>
            <ta e="T761" id="Seg_11619" s="T760">v</ta>
            <ta e="T762" id="Seg_11620" s="T761">n</ta>
            <ta e="T763" id="Seg_11621" s="T762">dempro</ta>
            <ta e="T764" id="Seg_11622" s="T763">n</ta>
            <ta e="T766" id="Seg_11623" s="T765">adv</ta>
            <ta e="T767" id="Seg_11624" s="T766">v</ta>
            <ta e="T768" id="Seg_11625" s="T767">adv</ta>
            <ta e="T769" id="Seg_11626" s="T768">adv</ta>
            <ta e="T770" id="Seg_11627" s="T769">v</ta>
            <ta e="T773" id="Seg_11628" s="T772">v</ta>
            <ta e="T774" id="Seg_11629" s="T773">n</ta>
            <ta e="T775" id="Seg_11630" s="T774">v</ta>
            <ta e="T777" id="Seg_11631" s="T775">n</ta>
            <ta e="T778" id="Seg_11632" s="T777">v</ta>
            <ta e="T779" id="Seg_11633" s="T778">n</ta>
            <ta e="T780" id="Seg_11634" s="T779">v</ta>
            <ta e="T781" id="Seg_11635" s="T780">n</ta>
            <ta e="T782" id="Seg_11636" s="T781">adv</ta>
            <ta e="T783" id="Seg_11637" s="T782">adv</ta>
            <ta e="T784" id="Seg_11638" s="T783">v</ta>
            <ta e="T785" id="Seg_11639" s="T784">adv</ta>
            <ta e="T786" id="Seg_11640" s="T785">v</ta>
            <ta e="T787" id="Seg_11641" s="T786">v</ta>
            <ta e="T788" id="Seg_11642" s="T787">n</ta>
            <ta e="T789" id="Seg_11643" s="T788">v</ta>
            <ta e="T790" id="Seg_11644" s="T789">n</ta>
            <ta e="T791" id="Seg_11645" s="T790">adv</ta>
            <ta e="T792" id="Seg_11646" s="T791">v</ta>
            <ta e="T793" id="Seg_11647" s="T792">v</ta>
            <ta e="T794" id="Seg_11648" s="T793">dempro</ta>
            <ta e="T795" id="Seg_11649" s="T794">n</ta>
            <ta e="T796" id="Seg_11650" s="T795">dempro</ta>
            <ta e="T797" id="Seg_11651" s="T796">v</ta>
            <ta e="T798" id="Seg_11652" s="T797">conj</ta>
            <ta e="T799" id="Seg_11653" s="T798">dempro</ta>
            <ta e="T800" id="Seg_11654" s="T799">v</ta>
            <ta e="T802" id="Seg_11655" s="T800">n</ta>
            <ta e="T804" id="Seg_11656" s="T803">v</ta>
            <ta e="T805" id="Seg_11657" s="T804">dempro</ta>
            <ta e="T806" id="Seg_11658" s="T805">dempro</ta>
            <ta e="T807" id="Seg_11659" s="T806">n</ta>
            <ta e="T808" id="Seg_11660" s="T807">conj</ta>
            <ta e="T809" id="Seg_11661" s="T808">dempro</ta>
            <ta e="T810" id="Seg_11662" s="T809">v</ta>
            <ta e="T811" id="Seg_11663" s="T810">n</ta>
            <ta e="T812" id="Seg_11664" s="T811">v</ta>
            <ta e="T813" id="Seg_11665" s="T812">n</ta>
            <ta e="T814" id="Seg_11666" s="T813">n</ta>
            <ta e="T815" id="Seg_11667" s="T814">v</ta>
            <ta e="T816" id="Seg_11668" s="T815">n</ta>
            <ta e="T817" id="Seg_11669" s="T816">n</ta>
            <ta e="T818" id="Seg_11670" s="T817">quant</ta>
            <ta e="T819" id="Seg_11671" s="T818">n</ta>
            <ta e="T820" id="Seg_11672" s="T819">v</ta>
            <ta e="T821" id="Seg_11673" s="T820">adv</ta>
            <ta e="T822" id="Seg_11674" s="T821">v</ta>
            <ta e="T823" id="Seg_11675" s="T822">v</ta>
            <ta e="T824" id="Seg_11676" s="T823">n</ta>
            <ta e="T825" id="Seg_11677" s="T824">v</ta>
            <ta e="T826" id="Seg_11678" s="T825">n</ta>
            <ta e="T827" id="Seg_11679" s="T826">adv</ta>
            <ta e="T828" id="Seg_11680" s="T827">dempro</ta>
            <ta e="T829" id="Seg_11681" s="T828">n</ta>
            <ta e="T830" id="Seg_11682" s="T829">v</ta>
            <ta e="T831" id="Seg_11683" s="T830">n</ta>
            <ta e="T833" id="Seg_11684" s="T832">v</ta>
            <ta e="T834" id="Seg_11685" s="T833">adj</ta>
            <ta e="T835" id="Seg_11686" s="T834">conj</ta>
            <ta e="T836" id="Seg_11687" s="T835">v</ta>
            <ta e="T837" id="Seg_11688" s="T836">dempro</ta>
            <ta e="T838" id="Seg_11689" s="T837">n</ta>
            <ta e="T839" id="Seg_11690" s="T838">adv</ta>
            <ta e="T840" id="Seg_11691" s="T839">v</ta>
            <ta e="T841" id="Seg_11692" s="T840">n</ta>
            <ta e="T842" id="Seg_11693" s="T841">v</ta>
            <ta e="T843" id="Seg_11694" s="T842">dempro</ta>
            <ta e="T844" id="Seg_11695" s="T843">n</ta>
            <ta e="T845" id="Seg_11696" s="T844">n</ta>
            <ta e="T846" id="Seg_11697" s="T845">adv</ta>
            <ta e="T847" id="Seg_11698" s="T846">v</ta>
            <ta e="T848" id="Seg_11699" s="T847">v</ta>
            <ta e="T849" id="Seg_11700" s="T848">n</ta>
            <ta e="T850" id="Seg_11701" s="T849">n</ta>
            <ta e="T851" id="Seg_11702" s="T850">v</ta>
            <ta e="T852" id="Seg_11703" s="T851">n</ta>
            <ta e="T853" id="Seg_11704" s="T852">n</ta>
            <ta e="T854" id="Seg_11705" s="T853">ptcl</ta>
            <ta e="T855" id="Seg_11706" s="T854">n</ta>
            <ta e="T856" id="Seg_11707" s="T855">v</ta>
            <ta e="T857" id="Seg_11708" s="T856">quant</ta>
            <ta e="T858" id="Seg_11709" s="T857">que</ta>
            <ta e="T860" id="Seg_11710" s="T858">ptcl</ta>
            <ta e="T862" id="Seg_11711" s="T861">adv</ta>
            <ta e="T863" id="Seg_11712" s="T862">n</ta>
            <ta e="T864" id="Seg_11713" s="T863">adv</ta>
            <ta e="T865" id="Seg_11714" s="T864">quant</ta>
            <ta e="T867" id="Seg_11715" s="T866">v</ta>
            <ta e="T868" id="Seg_11716" s="T867">dempro</ta>
            <ta e="T869" id="Seg_11717" s="T868">n</ta>
            <ta e="T870" id="Seg_11718" s="T869">v</ta>
            <ta e="T871" id="Seg_11719" s="T870">v</ta>
            <ta e="T872" id="Seg_11720" s="T871">n</ta>
            <ta e="T873" id="Seg_11721" s="T872">conj</ta>
            <ta e="T874" id="Seg_11722" s="T873">v</ta>
            <ta e="T875" id="Seg_11723" s="T874">adv</ta>
            <ta e="T876" id="Seg_11724" s="T875">adv</ta>
            <ta e="T877" id="Seg_11725" s="T876">adv</ta>
            <ta e="T881" id="Seg_11726" s="T880">n</ta>
            <ta e="T882" id="Seg_11727" s="T881">num</ta>
            <ta e="T884" id="Seg_11728" s="T883">v</ta>
            <ta e="T885" id="Seg_11729" s="T884">pers</ta>
            <ta e="T886" id="Seg_11730" s="T885">dempro</ta>
            <ta e="T889" id="Seg_11731" s="T888">v</ta>
            <ta e="T890" id="Seg_11732" s="T889">adv</ta>
            <ta e="T891" id="Seg_11733" s="T890">dempro</ta>
            <ta e="T892" id="Seg_11734" s="T891">v</ta>
            <ta e="T893" id="Seg_11735" s="T892">conj</ta>
            <ta e="T894" id="Seg_11736" s="T893">dempro</ta>
            <ta e="T895" id="Seg_11737" s="T894">ptcl</ta>
            <ta e="T896" id="Seg_11738" s="T895">v</ta>
            <ta e="T897" id="Seg_11739" s="T896">adj</ta>
            <ta e="T898" id="Seg_11740" s="T897">v</ta>
            <ta e="T900" id="Seg_11741" s="T899">adv</ta>
            <ta e="T901" id="Seg_11742" s="T900">pers</ta>
            <ta e="T902" id="Seg_11743" s="T901">n</ta>
            <ta e="T903" id="Seg_11744" s="T902">ptcl</ta>
            <ta e="T1025" id="Seg_11745" s="T903">v</ta>
            <ta e="T904" id="Seg_11746" s="T1025">v</ta>
            <ta e="T905" id="Seg_11747" s="T904">n</ta>
            <ta e="T906" id="Seg_11748" s="T905">pers</ta>
            <ta e="T907" id="Seg_11749" s="T906">v</ta>
            <ta e="T908" id="Seg_11750" s="T907">conj</ta>
            <ta e="T909" id="Seg_11751" s="T908">dempro</ta>
            <ta e="T910" id="Seg_11752" s="T909">ptcl</ta>
            <ta e="T911" id="Seg_11753" s="T910">v</ta>
            <ta e="T912" id="Seg_11754" s="T911">n</ta>
            <ta e="T913" id="Seg_11755" s="T912">ptcl</ta>
            <ta e="T914" id="Seg_11756" s="T913">v</ta>
            <ta e="T915" id="Seg_11757" s="T914">adv</ta>
            <ta e="T916" id="Seg_11758" s="T915">dempro</ta>
            <ta e="T917" id="Seg_11759" s="T916">adv</ta>
            <ta e="T918" id="Seg_11760" s="T917">v</ta>
            <ta e="T919" id="Seg_11761" s="T918">adv</ta>
            <ta e="T920" id="Seg_11762" s="T919">n</ta>
            <ta e="T921" id="Seg_11763" s="T920">n</ta>
            <ta e="T922" id="Seg_11764" s="T921">ptcl</ta>
            <ta e="T923" id="Seg_11765" s="T922">v</ta>
            <ta e="T924" id="Seg_11766" s="T923">n</ta>
            <ta e="T925" id="Seg_11767" s="T924">ptcl</ta>
            <ta e="T926" id="Seg_11768" s="T925">v</ta>
            <ta e="T927" id="Seg_11769" s="T926">dempro</ta>
            <ta e="T928" id="Seg_11770" s="T927">v</ta>
            <ta e="T929" id="Seg_11771" s="T928">adv</ta>
            <ta e="T930" id="Seg_11772" s="T929">v</ta>
            <ta e="T931" id="Seg_11773" s="T930">conj</ta>
            <ta e="T932" id="Seg_11774" s="T931">adv</ta>
            <ta e="T933" id="Seg_11775" s="T932">pers</ta>
            <ta e="T934" id="Seg_11776" s="T933">v</ta>
            <ta e="T935" id="Seg_11777" s="T934">v</ta>
            <ta e="T936" id="Seg_11778" s="T935">ptcl</ta>
            <ta e="T937" id="Seg_11779" s="T936">n</ta>
            <ta e="T938" id="Seg_11780" s="T937">v</ta>
            <ta e="T939" id="Seg_11781" s="T938">refl</ta>
            <ta e="T940" id="Seg_11782" s="T939">n</ta>
            <ta e="T941" id="Seg_11783" s="T940">v</ta>
            <ta e="T942" id="Seg_11784" s="T941">adv</ta>
            <ta e="T1026" id="Seg_11785" s="T942">v</ta>
            <ta e="T943" id="Seg_11786" s="T1026">v</ta>
            <ta e="T944" id="Seg_11787" s="T943">n</ta>
            <ta e="T945" id="Seg_11788" s="T944">n</ta>
            <ta e="T947" id="Seg_11789" s="T946">adv</ta>
            <ta e="T948" id="Seg_11790" s="T947">v</ta>
            <ta e="T949" id="Seg_11791" s="T948">n</ta>
            <ta e="T950" id="Seg_11792" s="T949">adv</ta>
            <ta e="T951" id="Seg_11793" s="T950">v</ta>
            <ta e="T952" id="Seg_11794" s="T951">v</ta>
            <ta e="T953" id="Seg_11795" s="T952">n</ta>
            <ta e="T954" id="Seg_11796" s="T953">ptcl</ta>
            <ta e="T955" id="Seg_11797" s="T954">pers</ta>
            <ta e="T956" id="Seg_11798" s="T955">adv</ta>
            <ta e="T957" id="Seg_11799" s="T956">n</ta>
            <ta e="T958" id="Seg_11800" s="T957">v</ta>
            <ta e="T959" id="Seg_11801" s="T958">pers</ta>
            <ta e="T960" id="Seg_11802" s="T959">v</ta>
            <ta e="T961" id="Seg_11803" s="T960">adv</ta>
            <ta e="T962" id="Seg_11804" s="T961">v</ta>
            <ta e="T963" id="Seg_11805" s="T962">n</ta>
            <ta e="T965" id="Seg_11806" s="T964">v</ta>
            <ta e="T966" id="Seg_11807" s="T965">n</ta>
            <ta e="T968" id="Seg_11808" s="T967">adv</ta>
            <ta e="T969" id="Seg_11809" s="T968">n</ta>
            <ta e="T970" id="Seg_11810" s="T969">v</ta>
            <ta e="T971" id="Seg_11811" s="T970">n</ta>
            <ta e="T972" id="Seg_11812" s="T971">v</ta>
            <ta e="T973" id="Seg_11813" s="T972">adv</ta>
            <ta e="T975" id="Seg_11814" s="T974">v</ta>
            <ta e="T977" id="Seg_11815" s="T976">n</ta>
            <ta e="T978" id="Seg_11816" s="T977">v</ta>
            <ta e="T979" id="Seg_11817" s="T978">pers</ta>
            <ta e="T980" id="Seg_11818" s="T979">ptcl</ta>
            <ta e="T981" id="Seg_11819" s="T980">v</ta>
            <ta e="T982" id="Seg_11820" s="T981">pers</ta>
            <ta e="T983" id="Seg_11821" s="T982">v</ta>
            <ta e="T984" id="Seg_11822" s="T983">dempro</ta>
            <ta e="T985" id="Seg_11823" s="T984">v</ta>
            <ta e="T986" id="Seg_11824" s="T985">pers</ta>
            <ta e="T987" id="Seg_11825" s="T986">adv</ta>
            <ta e="T990" id="Seg_11826" s="T989">v</ta>
            <ta e="T991" id="Seg_11827" s="T990">n</ta>
            <ta e="T992" id="Seg_11828" s="T991">v</ta>
            <ta e="T994" id="Seg_11829" s="T993">adv</ta>
            <ta e="T995" id="Seg_11830" s="T994">v</ta>
            <ta e="T996" id="Seg_11831" s="T995">n</ta>
            <ta e="T998" id="Seg_11832" s="T997">n</ta>
            <ta e="T999" id="Seg_11833" s="T998">v</ta>
            <ta e="T1000" id="Seg_11834" s="T999">n</ta>
            <ta e="T1001" id="Seg_11835" s="T1000">n</ta>
            <ta e="T1002" id="Seg_11836" s="T1001">v</ta>
            <ta e="T1004" id="Seg_11837" s="T1003">ptcl</ta>
            <ta e="T1005" id="Seg_11838" s="T1004">v</ta>
            <ta e="T1006" id="Seg_11839" s="T1005">ptcl</ta>
            <ta e="T1007" id="Seg_11840" s="T1006">n</ta>
            <ta e="T1008" id="Seg_11841" s="T1007">adv</ta>
            <ta e="T1009" id="Seg_11842" s="T1008">v</ta>
            <ta e="T1010" id="Seg_11843" s="T1009">n</ta>
            <ta e="T1011" id="Seg_11844" s="T1010">ptcl</ta>
            <ta e="T1012" id="Seg_11845" s="T1011">v</ta>
            <ta e="T1013" id="Seg_11846" s="T1012">v</ta>
            <ta e="T1014" id="Seg_11847" s="T1013">ptcl</ta>
            <ta e="T1015" id="Seg_11848" s="T1014">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF-PKZ">
            <ta e="T1" id="Seg_11849" s="T0">pro.h:S</ta>
            <ta e="T2" id="Seg_11850" s="T1">v:pred</ta>
            <ta e="T4" id="Seg_11851" s="T3">np.h:S</ta>
            <ta e="T5" id="Seg_11852" s="T4">v:pred</ta>
            <ta e="T8" id="Seg_11853" s="T7">np.h:O</ta>
            <ta e="T9" id="Seg_11854" s="T8">0.1.h:S v:pred</ta>
            <ta e="T12" id="Seg_11855" s="T11">np.h:S</ta>
            <ta e="T13" id="Seg_11856" s="T12">ptcl.neg</ta>
            <ta e="T14" id="Seg_11857" s="T13">v:pred</ta>
            <ta e="T15" id="Seg_11858" s="T14">0.3.h:S v:pred</ta>
            <ta e="T776" id="Seg_11859" s="T16">conv:pred</ta>
            <ta e="T17" id="Seg_11860" s="T776">0.3.h:S v:pred</ta>
            <ta e="T21" id="Seg_11861" s="T20">np.h:S</ta>
            <ta e="T23" id="Seg_11862" s="T22">v:pred</ta>
            <ta e="T24" id="Seg_11863" s="T23">np.h:S</ta>
            <ta e="T26" id="Seg_11864" s="T25">v:pred</ta>
            <ta e="T28" id="Seg_11865" s="T27">np.h:O</ta>
            <ta e="T29" id="Seg_11866" s="T28">0.3.h:S v:pred</ta>
            <ta e="T30" id="Seg_11867" s="T29">np.h:O</ta>
            <ta e="T31" id="Seg_11868" s="T30">0.3.h:S v:pred</ta>
            <ta e="T32" id="Seg_11869" s="T31">np.h:S</ta>
            <ta e="T34" id="Seg_11870" s="T33">v:pred</ta>
            <ta e="T37" id="Seg_11871" s="T36">0.3.h:S v:pred</ta>
            <ta e="T39" id="Seg_11872" s="T38">np.h:O</ta>
            <ta e="T40" id="Seg_11873" s="T39">np.h:S</ta>
            <ta e="T41" id="Seg_11874" s="T40">v:pred</ta>
            <ta e="T44" id="Seg_11875" s="T43">np.h:S</ta>
            <ta e="T45" id="Seg_11876" s="T44">v:pred</ta>
            <ta e="T46" id="Seg_11877" s="T45">pro.h:S</ta>
            <ta e="T47" id="Seg_11878" s="T46">pro.h:O</ta>
            <ta e="T49" id="Seg_11879" s="T48">v:pred</ta>
            <ta e="T50" id="Seg_11880" s="T49">np:O</ta>
            <ta e="T51" id="Seg_11881" s="T50">0.1.h:S v:pred</ta>
            <ta e="T53" id="Seg_11882" s="T52">0.1.h:S v:pred</ta>
            <ta e="T62" id="Seg_11883" s="T61">0.3.h:S v:pred</ta>
            <ta e="T64" id="Seg_11884" s="T63">pro.h:O</ta>
            <ta e="T65" id="Seg_11885" s="T64">0.3.h:S v:pred</ta>
            <ta e="T68" id="Seg_11886" s="T67">0.1.h:S v:pred</ta>
            <ta e="T70" id="Seg_11887" s="T69">np.h:S</ta>
            <ta e="T71" id="Seg_11888" s="T70">v:pred</ta>
            <ta e="T73" id="Seg_11889" s="T72">pro.h:S</ta>
            <ta e="T77" id="Seg_11890" s="T76">v:pred</ta>
            <ta e="T82" id="Seg_11891" s="T81">0.1.h:S v:pred</ta>
            <ta e="T83" id="Seg_11892" s="T82">0.1.h:S v:pred</ta>
            <ta e="T84" id="Seg_11893" s="T83">0.1.h:S v:pred</ta>
            <ta e="T85" id="Seg_11894" s="T84">0.1.h:S v:pred</ta>
            <ta e="T86" id="Seg_11895" s="T85">np:O</ta>
            <ta e="T88" id="Seg_11896" s="T87">0.1.h:S v:pred</ta>
            <ta e="T90" id="Seg_11897" s="T89">pro.h:S</ta>
            <ta e="T91" id="Seg_11898" s="T90">v:pred</ta>
            <ta e="T92" id="Seg_11899" s="T91">pro.h:S</ta>
            <ta e="T93" id="Seg_11900" s="T92">v:pred</ta>
            <ta e="T97" id="Seg_11901" s="T96">0.1.h:S v:pred</ta>
            <ta e="T100" id="Seg_11902" s="T99">np.h:S</ta>
            <ta e="T101" id="Seg_11903" s="T100">v:pred</ta>
            <ta e="T104" id="Seg_11904" s="T103">0.3.h:S v:pred</ta>
            <ta e="T106" id="Seg_11905" s="T104">s:purp</ta>
            <ta e="T108" id="Seg_11906" s="T107">np:S</ta>
            <ta e="T109" id="Seg_11907" s="T108">v:pred</ta>
            <ta e="T111" id="Seg_11908" s="T110">0.3.h:S v:pred</ta>
            <ta e="T112" id="Seg_11909" s="T111">np:S</ta>
            <ta e="T113" id="Seg_11910" s="T112">v:pred</ta>
            <ta e="T114" id="Seg_11911" s="T113">0.2.h:S v:pred</ta>
            <ta e="T115" id="Seg_11912" s="T114">0.2.h:S v:pred</ta>
            <ta e="T116" id="Seg_11913" s="T115">np:O</ta>
            <ta e="T117" id="Seg_11914" s="T116">pro.h:S</ta>
            <ta e="T118" id="Seg_11915" s="T117">v:pred</ta>
            <ta e="T120" id="Seg_11916" s="T119">np.h:S</ta>
            <ta e="T121" id="Seg_11917" s="T120">v:pred</ta>
            <ta e="T123" id="Seg_11918" s="T122">0.2.h:S v:pred</ta>
            <ta e="T126" id="Seg_11919" s="T125">np:O</ta>
            <ta e="T127" id="Seg_11920" s="T126">0.1.h:S v:pred</ta>
            <ta e="T129" id="Seg_11921" s="T128">pro.h:S</ta>
            <ta e="T130" id="Seg_11922" s="T129">v:pred</ta>
            <ta e="T131" id="Seg_11923" s="T130">pro.h:S</ta>
            <ta e="T132" id="Seg_11924" s="T131">ptcl.neg</ta>
            <ta e="T133" id="Seg_11925" s="T132">v:pred</ta>
            <ta e="T140" id="Seg_11926" s="T139">v:pred</ta>
            <ta e="T144" id="Seg_11927" s="T143">0.3.h:S v:pred</ta>
            <ta e="T145" id="Seg_11928" s="T144">pro.h:S</ta>
            <ta e="T1017" id="Seg_11929" s="T146">conv:pred</ta>
            <ta e="T147" id="Seg_11930" s="T1017">v:pred</ta>
            <ta e="T149" id="Seg_11931" s="T148">np:S</ta>
            <ta e="T150" id="Seg_11932" s="T149">ptcl.neg</ta>
            <ta e="T151" id="Seg_11933" s="T150">v:pred</ta>
            <ta e="T157" id="Seg_11934" s="T156">0.3.h:S v:pred</ta>
            <ta e="T160" id="Seg_11935" s="T159">0.3.h:S v:pred</ta>
            <ta e="T161" id="Seg_11936" s="T160">0.2.h:S v:pred</ta>
            <ta e="T163" id="Seg_11937" s="T162">pro.h:S</ta>
            <ta e="T164" id="Seg_11938" s="T163">v:pred</ta>
            <ta e="T165" id="Seg_11939" s="T164">pro.h:S</ta>
            <ta e="T166" id="Seg_11940" s="T165">ptcl.neg</ta>
            <ta e="T167" id="Seg_11941" s="T166">v:pred</ta>
            <ta e="T172" id="Seg_11942" s="T171">0.3.h:S v:pred</ta>
            <ta e="T173" id="Seg_11943" s="T172">np:O</ta>
            <ta e="T175" id="Seg_11944" s="T174">0.3.h:S v:pred</ta>
            <ta e="T176" id="Seg_11945" s="T175">ptcl.neg</ta>
            <ta e="T177" id="Seg_11946" s="T176">0.3.h:S v:pred</ta>
            <ta e="T178" id="Seg_11947" s="T177">np:O</ta>
            <ta e="T180" id="Seg_11948" s="T179">np:S</ta>
            <ta e="T182" id="Seg_11949" s="T181">v:pred</ta>
            <ta e="T183" id="Seg_11950" s="T182">0.2.h:S v:pred</ta>
            <ta e="T186" id="Seg_11951" s="T185">pro.h:S</ta>
            <ta e="T187" id="Seg_11952" s="T186">v:pred</ta>
            <ta e="T188" id="Seg_11953" s="T187">0.1.h:S v:pred</ta>
            <ta e="T190" id="Seg_11954" s="T189">v:pred</ta>
            <ta e="T191" id="Seg_11955" s="T190">np.h:S</ta>
            <ta e="T193" id="Seg_11956" s="T192">0.3.h:S v:pred</ta>
            <ta e="T195" id="Seg_11957" s="T193">s:purp</ta>
            <ta e="T198" id="Seg_11958" s="T197">0.3.h:S v:pred</ta>
            <ta e="T1018" id="Seg_11959" s="T201">conv:pred</ta>
            <ta e="T202" id="Seg_11960" s="T1018">0.3.h:S v:pred</ta>
            <ta e="T204" id="Seg_11961" s="T203">np:S</ta>
            <ta e="T205" id="Seg_11962" s="T204">v:pred</ta>
            <ta e="T206" id="Seg_11963" s="T205">np:O</ta>
            <ta e="T207" id="Seg_11964" s="T206">0.3.h:S v:pred</ta>
            <ta e="T208" id="Seg_11965" s="T207">np:O</ta>
            <ta e="T209" id="Seg_11966" s="T208">0.3.h:S v:pred</ta>
            <ta e="T211" id="Seg_11967" s="T210">v:pred</ta>
            <ta e="T213" id="Seg_11968" s="T212">np:S</ta>
            <ta e="T216" id="Seg_11969" s="T215">ptcl:pred</ta>
            <ta e="T217" id="Seg_11970" s="T216">np:O</ta>
            <ta e="T221" id="Seg_11971" s="T220">0.3.h:S v:pred</ta>
            <ta e="T222" id="Seg_11972" s="T221">np:O</ta>
            <ta e="T230" id="Seg_11973" s="T229">pro.h:O</ta>
            <ta e="T231" id="Seg_11974" s="T230">0.3.h:S v:pred</ta>
            <ta e="T235" id="Seg_11975" s="T234">0.3.h:S v:pred</ta>
            <ta e="T239" id="Seg_11976" s="T238">0.3.h:S v:pred</ta>
            <ta e="T241" id="Seg_11977" s="T240">np:O</ta>
            <ta e="T242" id="Seg_11978" s="T241">0.3.h:S v:pred</ta>
            <ta e="T244" id="Seg_11979" s="T243">np:S</ta>
            <ta e="T245" id="Seg_11980" s="T244">adj:pred</ta>
            <ta e="T249" id="Seg_11981" s="T248">np.h:S</ta>
            <ta e="T255" id="Seg_11982" s="T254">s:purp</ta>
            <ta e="T1019" id="Seg_11983" s="T255">conv:pred</ta>
            <ta e="T256" id="Seg_11984" s="T1019">v:pred</ta>
            <ta e="T260" id="Seg_11985" s="T259">np.h:S</ta>
            <ta e="T261" id="Seg_11986" s="T260">v:pred</ta>
            <ta e="T262" id="Seg_11987" s="T261">0.2.h:S v:pred</ta>
            <ta e="T273" id="Seg_11988" s="T272">0.3.h:S v:pred</ta>
            <ta e="T275" id="Seg_11989" s="T274">np:S</ta>
            <ta e="T276" id="Seg_11990" s="T275">v:pred</ta>
            <ta e="T1020" id="Seg_11991" s="T277">conv:pred</ta>
            <ta e="T278" id="Seg_11992" s="T1020">0.3.h:S v:pred</ta>
            <ta e="T281" id="Seg_11993" s="T280">np.h:S</ta>
            <ta e="T283" id="Seg_11994" s="T282">np.h:O</ta>
            <ta e="T1021" id="Seg_11995" s="T287">conv:pred</ta>
            <ta e="T288" id="Seg_11996" s="T1021">0.3.h:S v:pred</ta>
            <ta e="T289" id="Seg_11997" s="T288">pro.h:S</ta>
            <ta e="T291" id="Seg_11998" s="T290">v:pred</ta>
            <ta e="T293" id="Seg_11999" s="T292">pro.h:S</ta>
            <ta e="T294" id="Seg_12000" s="T293">pro.h:O</ta>
            <ta e="T295" id="Seg_12001" s="T294">v:pred</ta>
            <ta e="T297" id="Seg_12002" s="T296">0.3.h:S v:pred</ta>
            <ta e="T1022" id="Seg_12003" s="T299">conv:pred</ta>
            <ta e="T300" id="Seg_12004" s="T1022">0.3.h:S v:pred</ta>
            <ta e="T304" id="Seg_12005" s="T303">0.3.h:S v:pred</ta>
            <ta e="T307" id="Seg_12006" s="T306">pro.h:S</ta>
            <ta e="T308" id="Seg_12007" s="T307">ptcl.neg</ta>
            <ta e="T309" id="Seg_12008" s="T308">adj:pred</ta>
            <ta e="T310" id="Seg_12009" s="T309">cop</ta>
            <ta e="T315" id="Seg_12010" s="T314">pro.h:S</ta>
            <ta e="T316" id="Seg_12011" s="T315">ptcl.neg</ta>
            <ta e="T317" id="Seg_12012" s="T316">adj:pred</ta>
            <ta e="T318" id="Seg_12013" s="T317">np:S</ta>
            <ta e="T319" id="Seg_12014" s="T318">adj:pred</ta>
            <ta e="T320" id="Seg_12015" s="T319">np:S</ta>
            <ta e="T321" id="Seg_12016" s="T320">adj:pred</ta>
            <ta e="T324" id="Seg_12017" s="T323">np.h:S</ta>
            <ta e="T325" id="Seg_12018" s="T324">v:pred</ta>
            <ta e="T326" id="Seg_12019" s="T325">0.3.h:S v:pred</ta>
            <ta e="T329" id="Seg_12020" s="T326">s:purp</ta>
            <ta e="T330" id="Seg_12021" s="T329">np.h:S</ta>
            <ta e="T331" id="Seg_12022" s="T330">v:pred</ta>
            <ta e="T338" id="Seg_12023" s="T337">0.3.h:S v:pred</ta>
            <ta e="T342" id="Seg_12024" s="T341">0.3.h:S v:pred</ta>
            <ta e="T346" id="Seg_12025" s="T345">np:O</ta>
            <ta e="T347" id="Seg_12026" s="T346">0.3.h:S v:pred</ta>
            <ta e="T349" id="Seg_12027" s="T348">0.3.h:S v:pred</ta>
            <ta e="T351" id="Seg_12028" s="T350">np.h:S</ta>
            <ta e="T352" id="Seg_12029" s="T351">v:pred</ta>
            <ta e="T353" id="Seg_12030" s="T352">adv.pred</ta>
            <ta e="T355" id="Seg_12031" s="T354">np.h:S</ta>
            <ta e="T356" id="Seg_12032" s="T355">pro.h:S</ta>
            <ta e="T358" id="Seg_12033" s="T357">adj:pred</ta>
            <ta e="T359" id="Seg_12034" s="T358">ptcl.neg</ta>
            <ta e="T360" id="Seg_12035" s="T359">0.3.h:S v:pred</ta>
            <ta e="T364" id="Seg_12036" s="T363">0.3.h:S v:pred</ta>
            <ta e="T366" id="Seg_12037" s="T365">np.h:S</ta>
            <ta e="T372" id="Seg_12038" s="T371">v:pred</ta>
            <ta e="T374" id="Seg_12039" s="T373">0.3.h:S v:pred</ta>
            <ta e="T375" id="Seg_12040" s="T374">np.h:S</ta>
            <ta e="T376" id="Seg_12041" s="T375">v:pred</ta>
            <ta e="T377" id="Seg_12042" s="T376">pro.h:S</ta>
            <ta e="T379" id="Seg_12043" s="T378">conv:pred</ta>
            <ta e="T380" id="Seg_12044" s="T379">v:pred</ta>
            <ta e="T383" id="Seg_12045" s="T382">0.3.h:S v:pred</ta>
            <ta e="T385" id="Seg_12046" s="T384">pro.h:S</ta>
            <ta e="T389" id="Seg_12047" s="T388">v:pred</ta>
            <ta e="T392" id="Seg_12048" s="T391">0.3.h:S v:pred</ta>
            <ta e="T396" id="Seg_12049" s="T395">np.h:S</ta>
            <ta e="T397" id="Seg_12050" s="T396">v:pred</ta>
            <ta e="T401" id="Seg_12051" s="T400">pro.h:S</ta>
            <ta e="T402" id="Seg_12052" s="T401">v:pred</ta>
            <ta e="T403" id="Seg_12053" s="T402">pro.h:S</ta>
            <ta e="T405" id="Seg_12054" s="T404">v:pred</ta>
            <ta e="T410" id="Seg_12055" s="T409">0.3.h:S v:pred</ta>
            <ta e="T413" id="Seg_12056" s="T412">pro:O</ta>
            <ta e="T414" id="Seg_12057" s="T413">ptcl.neg</ta>
            <ta e="T415" id="Seg_12058" s="T414">0.3.h:S v:pred</ta>
            <ta e="T417" id="Seg_12059" s="T416">np:S</ta>
            <ta e="T418" id="Seg_12060" s="T417">pro:O</ta>
            <ta e="T419" id="Seg_12061" s="T418">ptcl.neg</ta>
            <ta e="T420" id="Seg_12062" s="T419">v:pred</ta>
            <ta e="T421" id="Seg_12063" s="T420">pro.h:S</ta>
            <ta e="T422" id="Seg_12064" s="T421">v:pred</ta>
            <ta e="T424" id="Seg_12065" s="T423">np.h:O</ta>
            <ta e="T425" id="Seg_12066" s="T424">0.3.h:S v:pred</ta>
            <ta e="T1023" id="Seg_12067" s="T426">conv:pred</ta>
            <ta e="T427" id="Seg_12068" s="T1023">0.3.h:S v:pred</ta>
            <ta e="T429" id="Seg_12069" s="T428">np.h:S</ta>
            <ta e="T430" id="Seg_12070" s="T429">adj:pred</ta>
            <ta e="T434" id="Seg_12071" s="T431">s:temp</ta>
            <ta e="T435" id="Seg_12072" s="T434">pro.h:S</ta>
            <ta e="T438" id="Seg_12073" s="T437">v:pred</ta>
            <ta e="T439" id="Seg_12074" s="T438">np:S</ta>
            <ta e="T441" id="Seg_12075" s="T440">v:pred</ta>
            <ta e="T443" id="Seg_12076" s="T442">np:S</ta>
            <ta e="T450" id="Seg_12077" s="T449">np.h:S</ta>
            <ta e="T453" id="Seg_12078" s="T452">v:pred</ta>
            <ta e="T457" id="Seg_12079" s="T456">0.3.h:S v:pred</ta>
            <ta e="T459" id="Seg_12080" s="T457">s:purp</ta>
            <ta e="T462" id="Seg_12081" s="T461">0.3.h:S v:pred</ta>
            <ta e="T465" id="Seg_12082" s="T464">0.3.h:S v:pred</ta>
            <ta e="T469" id="Seg_12083" s="T468">0.3.h:S v:pred</ta>
            <ta e="T471" id="Seg_12084" s="T470">pro.h:S</ta>
            <ta e="T472" id="Seg_12085" s="T471">v:pred</ta>
            <ta e="T474" id="Seg_12086" s="T473">0.1.h:S v:pred</ta>
            <ta e="T491" id="Seg_12087" s="T490">np.h:S</ta>
            <ta e="T492" id="Seg_12088" s="T491">s:purp</ta>
            <ta e="T494" id="Seg_12089" s="T493">v:pred</ta>
            <ta e="T498" id="Seg_12090" s="T497">np:S</ta>
            <ta e="T499" id="Seg_12091" s="T498">n:pred</ta>
            <ta e="T500" id="Seg_12092" s="T499">0.3.h:S v:pred</ta>
            <ta e="T502" id="Seg_12093" s="T501">np.h:S</ta>
            <ta e="T504" id="Seg_12094" s="T503">v:pred</ta>
            <ta e="T506" id="Seg_12095" s="T505">0.2.h:S v:pred</ta>
            <ta e="T507" id="Seg_12096" s="T506">s:purp</ta>
            <ta e="T508" id="Seg_12097" s="T507">0.2.h:S v:pred</ta>
            <ta e="T509" id="Seg_12098" s="T508">pro.h:O</ta>
            <ta e="T511" id="Seg_12099" s="T510">pro.h:S</ta>
            <ta e="T512" id="Seg_12100" s="T511">pro:O</ta>
            <ta e="T513" id="Seg_12101" s="T512">v:pred</ta>
            <ta e="T514" id="Seg_12102" s="T513">s:compl</ta>
            <ta e="T516" id="Seg_12103" s="T515">s:compl</ta>
            <ta e="T518" id="Seg_12104" s="T517">0.3.h:S v:pred</ta>
            <ta e="T521" id="Seg_12105" s="T520">np.h:S</ta>
            <ta e="T522" id="Seg_12106" s="T521">v:pred</ta>
            <ta e="T524" id="Seg_12107" s="T523">0.2.h:S v:pred</ta>
            <ta e="T525" id="Seg_12108" s="T524">s:purp</ta>
            <ta e="T526" id="Seg_12109" s="T525">pro.h:O</ta>
            <ta e="T527" id="Seg_12110" s="T526">0.2.h:S v:pred</ta>
            <ta e="T529" id="Seg_12111" s="T528">pro.h:S</ta>
            <ta e="T530" id="Seg_12112" s="T529">pro:O</ta>
            <ta e="T531" id="Seg_12113" s="T530">v:pred</ta>
            <ta e="T532" id="Seg_12114" s="T531">s:compl</ta>
            <ta e="T534" id="Seg_12115" s="T533">s:compl</ta>
            <ta e="T536" id="Seg_12116" s="T535">0.3.h:S v:pred</ta>
            <ta e="T538" id="Seg_12117" s="T537">ptcl:pred</ta>
            <ta e="T539" id="Seg_12118" s="T538">s:purp</ta>
            <ta e="T540" id="Seg_12119" s="T539">pro.h:S</ta>
            <ta e="T541" id="Seg_12120" s="T540">v:pred</ta>
            <ta e="T542" id="Seg_12121" s="T541">pro.h:S</ta>
            <ta e="T543" id="Seg_12122" s="T542">pro:O</ta>
            <ta e="T545" id="Seg_12123" s="T544">v:pred</ta>
            <ta e="T546" id="Seg_12124" s="T545">np:O</ta>
            <ta e="T547" id="Seg_12125" s="T546">0.1.h:S v:pred</ta>
            <ta e="T548" id="Seg_12126" s="T547">np:O</ta>
            <ta e="T550" id="Seg_12127" s="T549">np:S</ta>
            <ta e="T551" id="Seg_12128" s="T550">ptcl:pred</ta>
            <ta e="T553" id="Seg_12129" s="T552">0.3.h:S v:pred</ta>
            <ta e="T556" id="Seg_12130" s="T555">pro.h:S</ta>
            <ta e="T557" id="Seg_12131" s="T556">v:pred</ta>
            <ta e="T561" id="Seg_12132" s="T560">adj:pred</ta>
            <ta e="T565" id="Seg_12133" s="T564">0.1.h:S v:pred</ta>
            <ta e="T569" id="Seg_12134" s="T568">np:O</ta>
            <ta e="T570" id="Seg_12135" s="T569">0.3.h:S v:pred</ta>
            <ta e="T572" id="Seg_12136" s="T571">v:pred</ta>
            <ta e="T574" id="Seg_12137" s="T573">np.h:S</ta>
            <ta e="T580" id="Seg_12138" s="T579">0.3.h:S v:pred</ta>
            <ta e="T582" id="Seg_12139" s="T581">0.3.h:S v:pred</ta>
            <ta e="T583" id="Seg_12140" s="T582">0.3.h:S v:pred</ta>
            <ta e="T585" id="Seg_12141" s="T584">0.3.h:S v:pred</ta>
            <ta e="T587" id="Seg_12142" s="T586">pro:O</ta>
            <ta e="T588" id="Seg_12143" s="T587">0.3.h:S v:pred</ta>
            <ta e="T590" id="Seg_12144" s="T589">np:S</ta>
            <ta e="T594" id="Seg_12145" s="T593">v:pred</ta>
            <ta e="T596" id="Seg_12146" s="T595">pro.h:S</ta>
            <ta e="T597" id="Seg_12147" s="T596">v:pred</ta>
            <ta e="T598" id="Seg_12148" s="T597">0.2.h:S v:pred</ta>
            <ta e="T601" id="Seg_12149" s="T600">0.2.h:S v:pred</ta>
            <ta e="T615" id="Seg_12150" s="T611">s:purp</ta>
            <ta e="T618" id="Seg_12151" s="T617">v:pred</ta>
            <ta e="T622" id="Seg_12152" s="T621">np.h:S</ta>
            <ta e="T626" id="Seg_12153" s="T625">0.3.h:S v:pred</ta>
            <ta e="T627" id="Seg_12154" s="T626">0.3.h:S v:pred</ta>
            <ta e="T629" id="Seg_12155" s="T628">0.3.h:S v:pred</ta>
            <ta e="T630" id="Seg_12156" s="T629">0.3.h:S v:pred</ta>
            <ta e="T632" id="Seg_12157" s="T631">0.3.h:S v:pred</ta>
            <ta e="T634" id="Seg_12158" s="T633">0.3.h:S v:pred</ta>
            <ta e="T636" id="Seg_12159" s="T635">0.3.h:S v:pred</ta>
            <ta e="T639" id="Seg_12160" s="T638">0.2.h:S v:pred</ta>
            <ta e="T646" id="Seg_12161" s="T645">np:S</ta>
            <ta e="T647" id="Seg_12162" s="T646">adj:pred</ta>
            <ta e="T650" id="Seg_12163" s="T649">ptcl.neg</ta>
            <ta e="T651" id="Seg_12164" s="T650">0.3.h:S v:pred</ta>
            <ta e="T653" id="Seg_12165" s="T652">np:O</ta>
            <ta e="T654" id="Seg_12166" s="T653">ptcl.neg</ta>
            <ta e="T655" id="Seg_12167" s="T654">0.3.h:S v:pred</ta>
            <ta e="T658" id="Seg_12168" s="T657">np:O</ta>
            <ta e="T659" id="Seg_12169" s="T658">0.3.h:S v:pred</ta>
            <ta e="T662" id="Seg_12170" s="T661">0.3.h:S v:pred</ta>
            <ta e="T667" id="Seg_12171" s="T666">np:O</ta>
            <ta e="T668" id="Seg_12172" s="T667">0.3.h:S v:pred</ta>
            <ta e="T671" id="Seg_12173" s="T670">0.3.h:S v:pred</ta>
            <ta e="T674" id="Seg_12174" s="T673">pro:O</ta>
            <ta e="T675" id="Seg_12175" s="T674">0.2.h:S v:pred</ta>
            <ta e="T679" id="Seg_12176" s="T678">ptcl.neg</ta>
            <ta e="T681" id="Seg_12177" s="T680">0.2.h:S v:pred</ta>
            <ta e="T687" id="Seg_12178" s="T686">0.2.h:S v:pred</ta>
            <ta e="T693" id="Seg_12179" s="T692">v:pred</ta>
            <ta e="T694" id="Seg_12180" s="T693">np:S</ta>
            <ta e="T700" id="Seg_12181" s="T699">np:S</ta>
            <ta e="T703" id="Seg_12182" s="T702">v:pred</ta>
            <ta e="T708" id="Seg_12183" s="T707">pro.h:S</ta>
            <ta e="T711" id="Seg_12184" s="T710">v:pred</ta>
            <ta e="T712" id="Seg_12185" s="T711">np:O</ta>
            <ta e="T715" id="Seg_12186" s="T714">0.3.h:S v:pred</ta>
            <ta e="T716" id="Seg_12187" s="T715">pro.h:S</ta>
            <ta e="T718" id="Seg_12188" s="T717">v:pred</ta>
            <ta e="T720" id="Seg_12189" s="T719">np:S</ta>
            <ta e="T721" id="Seg_12190" s="T720">v:pred</ta>
            <ta e="T723" id="Seg_12191" s="T722">0.2.h:S v:pred</ta>
            <ta e="T724" id="Seg_12192" s="T723">0.1.h:S v:pred</ta>
            <ta e="T727" id="Seg_12193" s="T726">np:O</ta>
            <ta e="T728" id="Seg_12194" s="T727">pro.h:S</ta>
            <ta e="T730" id="Seg_12195" s="T729">v:pred</ta>
            <ta e="T731" id="Seg_12196" s="T730">np:S</ta>
            <ta e="T732" id="Seg_12197" s="T731">v:pred</ta>
            <ta e="T733" id="Seg_12198" s="T732">0.1.h:S v:pred</ta>
            <ta e="T734" id="Seg_12199" s="T733">np:O</ta>
            <ta e="T735" id="Seg_12200" s="T734">0.2.h:S v:pred</ta>
            <ta e="T736" id="Seg_12201" s="T735">np:O</ta>
            <ta e="T739" id="Seg_12202" s="T738">np.h:S</ta>
            <ta e="T740" id="Seg_12203" s="T739">v:pred</ta>
            <ta e="T741" id="Seg_12204" s="T740">0.2.h:S v:pred</ta>
            <ta e="T742" id="Seg_12205" s="T741">pro:O</ta>
            <ta e="T745" id="Seg_12206" s="T744">0.3.h:S v:pred</ta>
            <ta e="T748" id="Seg_12207" s="T747">0.3.h:S v:pred</ta>
            <ta e="T750" id="Seg_12208" s="T749">0.3.h:S v:pred</ta>
            <ta e="T752" id="Seg_12209" s="T751">0.2.h:S v:pred</ta>
            <ta e="T753" id="Seg_12210" s="T752">np:O</ta>
            <ta e="T754" id="Seg_12211" s="T753">0.2.h:S v:pred</ta>
            <ta e="T755" id="Seg_12212" s="T754">np:O</ta>
            <ta e="T757" id="Seg_12213" s="T756">0.2.h:S v:pred</ta>
            <ta e="T759" id="Seg_12214" s="T758">pro:O</ta>
            <ta e="T761" id="Seg_12215" s="T760">0.3.h:S v:pred</ta>
            <ta e="T767" id="Seg_12216" s="T766">0.3.h:S v:pred</ta>
            <ta e="T770" id="Seg_12217" s="T769">0.3.h:S v:pred</ta>
            <ta e="T773" id="Seg_12218" s="T772">0.2.h:S v:pred</ta>
            <ta e="T774" id="Seg_12219" s="T773">np:O</ta>
            <ta e="T775" id="Seg_12220" s="T774">0.2.h:S v:pred</ta>
            <ta e="T777" id="Seg_12221" s="T775">np:O</ta>
            <ta e="T778" id="Seg_12222" s="T777">0.2.h:S v:pred</ta>
            <ta e="T780" id="Seg_12223" s="T779">0.3.h:S v:pred</ta>
            <ta e="T784" id="Seg_12224" s="T783">0.3.h:S v:pred</ta>
            <ta e="T786" id="Seg_12225" s="T785">0.3.h:S v:pred</ta>
            <ta e="T787" id="Seg_12226" s="T786">0.2.h:S v:pred</ta>
            <ta e="T788" id="Seg_12227" s="T787">np:O</ta>
            <ta e="T789" id="Seg_12228" s="T788">0.2.h:S v:pred</ta>
            <ta e="T790" id="Seg_12229" s="T789">np:O</ta>
            <ta e="T792" id="Seg_12230" s="T791">0.3.h:S v:pred</ta>
            <ta e="T793" id="Seg_12231" s="T792">0.2.h:S v:pred</ta>
            <ta e="T794" id="Seg_12232" s="T793">pro:O</ta>
            <ta e="T796" id="Seg_12233" s="T795">pro:O</ta>
            <ta e="T797" id="Seg_12234" s="T796">0.3.h:S v:pred</ta>
            <ta e="T799" id="Seg_12235" s="T798">pro.h:S</ta>
            <ta e="T800" id="Seg_12236" s="T799">v:pred</ta>
            <ta e="T804" id="Seg_12237" s="T803">0.3.h:S v:pred</ta>
            <ta e="T805" id="Seg_12238" s="T804">pro.h:S</ta>
            <ta e="T810" id="Seg_12239" s="T809">v:pred</ta>
            <ta e="T812" id="Seg_12240" s="T811">0.2.h:S v:pred</ta>
            <ta e="T813" id="Seg_12241" s="T812">np:O</ta>
            <ta e="T815" id="Seg_12242" s="T814">0.2.h:S v:pred</ta>
            <ta e="T816" id="Seg_12243" s="T815">np:O</ta>
            <ta e="T817" id="Seg_12244" s="T816">np:S</ta>
            <ta e="T819" id="Seg_12245" s="T818">np:O</ta>
            <ta e="T820" id="Seg_12246" s="T819">v:pred</ta>
            <ta e="T822" id="Seg_12247" s="T821">0.3.h:S v:pred</ta>
            <ta e="T823" id="Seg_12248" s="T822">0.2.h:S v:pred</ta>
            <ta e="T824" id="Seg_12249" s="T823">np:O</ta>
            <ta e="T825" id="Seg_12250" s="T824">0.2.h:S v:pred</ta>
            <ta e="T826" id="Seg_12251" s="T825">np:O</ta>
            <ta e="T829" id="Seg_12252" s="T828">np.h:S</ta>
            <ta e="T830" id="Seg_12253" s="T829">v:pred</ta>
            <ta e="T831" id="Seg_12254" s="T830">np:O</ta>
            <ta e="T833" id="Seg_12255" s="T832">0.2.h:S v:pred</ta>
            <ta e="T836" id="Seg_12256" s="T835">0.2.h:S v:pred</ta>
            <ta e="T840" id="Seg_12257" s="T839">0.3.h:S v:pred</ta>
            <ta e="T841" id="Seg_12258" s="T840">np:O</ta>
            <ta e="T842" id="Seg_12259" s="T841">0.3.h:S v:pred</ta>
            <ta e="T844" id="Seg_12260" s="T843">np:O</ta>
            <ta e="T847" id="Seg_12261" s="T846">0.3.h:S v:pred</ta>
            <ta e="T848" id="Seg_12262" s="T847">v:pred</ta>
            <ta e="T849" id="Seg_12263" s="T848">np:S</ta>
            <ta e="T850" id="Seg_12264" s="T849">np:O</ta>
            <ta e="T851" id="Seg_12265" s="T850">v:pred</ta>
            <ta e="T852" id="Seg_12266" s="T851">np:S</ta>
            <ta e="T853" id="Seg_12267" s="T852">np:O</ta>
            <ta e="T855" id="Seg_12268" s="T854">np:S</ta>
            <ta e="T856" id="Seg_12269" s="T855">v:pred</ta>
            <ta e="T863" id="Seg_12270" s="T862">np:S</ta>
            <ta e="T867" id="Seg_12271" s="T866">v:pred</ta>
            <ta e="T869" id="Seg_12272" s="T868">np.h:S</ta>
            <ta e="T870" id="Seg_12273" s="T869">v:pred</ta>
            <ta e="T871" id="Seg_12274" s="T870">0.2.h:S v:pred</ta>
            <ta e="T872" id="Seg_12275" s="T871">np:O</ta>
            <ta e="T874" id="Seg_12276" s="T873">0.2.h:S v:pred</ta>
            <ta e="T884" id="Seg_12277" s="T883">0.3.h:S v:pred</ta>
            <ta e="T885" id="Seg_12278" s="T884">pro.h:S</ta>
            <ta e="T889" id="Seg_12279" s="T888">v:pred</ta>
            <ta e="T891" id="Seg_12280" s="T890">pro.h:S</ta>
            <ta e="T892" id="Seg_12281" s="T891">v:pred</ta>
            <ta e="T894" id="Seg_12282" s="T893">pro.h:S</ta>
            <ta e="T895" id="Seg_12283" s="T894">ptcl.neg</ta>
            <ta e="T896" id="Seg_12284" s="T895">v:pred</ta>
            <ta e="T898" id="Seg_12285" s="T897">0.3.h:S v:pred</ta>
            <ta e="T901" id="Seg_12286" s="T900">pro.h:S</ta>
            <ta e="T903" id="Seg_12287" s="T902">ptcl.neg</ta>
            <ta e="T1025" id="Seg_12288" s="T903">conv:pred</ta>
            <ta e="T904" id="Seg_12289" s="T1025">v:pred</ta>
            <ta e="T906" id="Seg_12290" s="T905">pro.h:S</ta>
            <ta e="T907" id="Seg_12291" s="T906">v:pred</ta>
            <ta e="T910" id="Seg_12292" s="T909">ptcl.neg</ta>
            <ta e="T911" id="Seg_12293" s="T910">0.1.h:S v:pred</ta>
            <ta e="T912" id="Seg_12294" s="T911">np:O</ta>
            <ta e="T913" id="Seg_12295" s="T912">ptcl.neg</ta>
            <ta e="T914" id="Seg_12296" s="T913">0.3.h:S v:pred</ta>
            <ta e="T916" id="Seg_12297" s="T915">pro.h:S</ta>
            <ta e="T918" id="Seg_12298" s="T917">v:pred</ta>
            <ta e="T920" id="Seg_12299" s="T919">np:S</ta>
            <ta e="T921" id="Seg_12300" s="T920">np:S</ta>
            <ta e="T923" id="Seg_12301" s="T922">v:pred</ta>
            <ta e="T924" id="Seg_12302" s="T923">np:S</ta>
            <ta e="T925" id="Seg_12303" s="T924">ptcl.neg</ta>
            <ta e="T926" id="Seg_12304" s="T925">v:pred</ta>
            <ta e="T927" id="Seg_12305" s="T926">pro.h:S</ta>
            <ta e="T928" id="Seg_12306" s="T927">v:pred</ta>
            <ta e="T930" id="Seg_12307" s="T929">0.3.h:S v:pred</ta>
            <ta e="T933" id="Seg_12308" s="T932">pro.h:S</ta>
            <ta e="T934" id="Seg_12309" s="T933">v:pred</ta>
            <ta e="T935" id="Seg_12310" s="T934">0.1.h:S v:pred</ta>
            <ta e="T937" id="Seg_12311" s="T936">np:O</ta>
            <ta e="T938" id="Seg_12312" s="T937">0.1.h:S v:pred</ta>
            <ta e="T941" id="Seg_12313" s="T940">0.3.h:S v:pred</ta>
            <ta e="T1026" id="Seg_12314" s="T942">conv:pred</ta>
            <ta e="T943" id="Seg_12315" s="T1026">0.3.h:S v:pred</ta>
            <ta e="T948" id="Seg_12316" s="T947">0.1.h:S v:pred</ta>
            <ta e="T951" id="Seg_12317" s="T950">0.1.h:S v:pred</ta>
            <ta e="T952" id="Seg_12318" s="T951">0.1.h:S v:pred</ta>
            <ta e="T955" id="Seg_12319" s="T954">pro.h:O</ta>
            <ta e="T957" id="Seg_12320" s="T956">np:S</ta>
            <ta e="T958" id="Seg_12321" s="T957">v:pred</ta>
            <ta e="T960" id="Seg_12322" s="T959">0.3.h:S v:pred</ta>
            <ta e="T962" id="Seg_12323" s="T961">0.1.h:S v:pred</ta>
            <ta e="T965" id="Seg_12324" s="T964">0.1.h:S v:pred</ta>
            <ta e="T966" id="Seg_12325" s="T965">np:O</ta>
            <ta e="T967" id="Seg_12326" s="T966">0.1.h:S v:pred</ta>
            <ta e="T969" id="Seg_12327" s="T968">np:O</ta>
            <ta e="T970" id="Seg_12328" s="T969">0.1.h:S v:pred</ta>
            <ta e="T972" id="Seg_12329" s="T971">0.1.h:S v:pred</ta>
            <ta e="T975" id="Seg_12330" s="T974">0.3.h:S v:pred</ta>
            <ta e="T977" id="Seg_12331" s="T976">np.h:S</ta>
            <ta e="T978" id="Seg_12332" s="T977">v:pred</ta>
            <ta e="T980" id="Seg_12333" s="T979">ptcl:pred</ta>
            <ta e="T982" id="Seg_12334" s="T981">pro.h:S</ta>
            <ta e="T983" id="Seg_12335" s="T982">v:pred</ta>
            <ta e="T985" id="Seg_12336" s="T984">0.3.h:S v:pred</ta>
            <ta e="T986" id="Seg_12337" s="T985">pro.h:O</ta>
            <ta e="T990" id="Seg_12338" s="T989">0.1.h:S v:pred</ta>
            <ta e="T992" id="Seg_12339" s="T991">s:purp</ta>
            <ta e="T995" id="Seg_12340" s="T994">s:purp</ta>
            <ta e="T998" id="Seg_12341" s="T997">np.h:S</ta>
            <ta e="T999" id="Seg_12342" s="T998">v:pred</ta>
            <ta e="T1001" id="Seg_12343" s="T1000">np:S</ta>
            <ta e="T1002" id="Seg_12344" s="T1001">v:pred</ta>
            <ta e="T1007" id="Seg_12345" s="T1006">np:S</ta>
            <ta e="T1009" id="Seg_12346" s="T1008">v:pred</ta>
            <ta e="T1011" id="Seg_12347" s="T1010">ptcl.neg</ta>
            <ta e="T1012" id="Seg_12348" s="T1011">0.3.h:S v:pred</ta>
            <ta e="T1015" id="Seg_12349" s="T1014">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-PKZ">
            <ta e="T1" id="Seg_12350" s="T0">pro.h:E</ta>
            <ta e="T4" id="Seg_12351" s="T3">np.h:Th</ta>
            <ta e="T6" id="Seg_12352" s="T5">pro.h:Poss</ta>
            <ta e="T7" id="Seg_12353" s="T6">adv:Time</ta>
            <ta e="T8" id="Seg_12354" s="T7">np.h:Th</ta>
            <ta e="T9" id="Seg_12355" s="T8">0.1.h:A</ta>
            <ta e="T12" id="Seg_12356" s="T11">np.h:E</ta>
            <ta e="T15" id="Seg_12357" s="T14">0.3.h:A</ta>
            <ta e="T17" id="Seg_12358" s="T776">0.3.h:A</ta>
            <ta e="T18" id="Seg_12359" s="T17">adv:Time</ta>
            <ta e="T21" id="Seg_12360" s="T20">np.h:P 0.1.h:Poss</ta>
            <ta e="T22" id="Seg_12361" s="T21">np:L</ta>
            <ta e="T24" id="Seg_12362" s="T23">np.h:P 0.1.h:Poss</ta>
            <ta e="T25" id="Seg_12363" s="T24">np:L</ta>
            <ta e="T27" id="Seg_12364" s="T26">adv:Time</ta>
            <ta e="T28" id="Seg_12365" s="T27">np.h:P</ta>
            <ta e="T29" id="Seg_12366" s="T28">0.3.h:A</ta>
            <ta e="T30" id="Seg_12367" s="T29">np.h:P</ta>
            <ta e="T31" id="Seg_12368" s="T30">0.3.h:A</ta>
            <ta e="T32" id="Seg_12369" s="T31">np.h:A 0.1.h:Poss</ta>
            <ta e="T33" id="Seg_12370" s="T32">np:G</ta>
            <ta e="T36" id="Seg_12371" s="T35">adv:L</ta>
            <ta e="T37" id="Seg_12372" s="T36">0.3.h:P</ta>
            <ta e="T38" id="Seg_12373" s="T37">pro.h:Poss</ta>
            <ta e="T39" id="Seg_12374" s="T38">np.h:P</ta>
            <ta e="T40" id="Seg_12375" s="T39">np.h:A</ta>
            <ta e="T44" id="Seg_12376" s="T43">np.h:Th</ta>
            <ta e="T46" id="Seg_12377" s="T45">pro.h:A</ta>
            <ta e="T47" id="Seg_12378" s="T46">pro.h:Th</ta>
            <ta e="T50" id="Seg_12379" s="T49">np:Th</ta>
            <ta e="T51" id="Seg_12380" s="T50">0.1.h:A</ta>
            <ta e="T53" id="Seg_12381" s="T52">0.1.h:A</ta>
            <ta e="T61" id="Seg_12382" s="T60">pro:G</ta>
            <ta e="T62" id="Seg_12383" s="T61">0.3.h:E</ta>
            <ta e="T63" id="Seg_12384" s="T62">adv:Time</ta>
            <ta e="T64" id="Seg_12385" s="T63">pro.h:Th</ta>
            <ta e="T65" id="Seg_12386" s="T64">0.3.h:A</ta>
            <ta e="T66" id="Seg_12387" s="T65">np:G</ta>
            <ta e="T67" id="Seg_12388" s="T66">adv:L</ta>
            <ta e="T68" id="Seg_12389" s="T67">0.3.h:A</ta>
            <ta e="T69" id="Seg_12390" s="T68">pro.h:Poss</ta>
            <ta e="T70" id="Seg_12391" s="T69">np.h:Th</ta>
            <ta e="T73" id="Seg_12392" s="T72">pro.h:E</ta>
            <ta e="T78" id="Seg_12393" s="T77">np:G</ta>
            <ta e="T82" id="Seg_12394" s="T81">0.1.h:A</ta>
            <ta e="T83" id="Seg_12395" s="T82">0.1.h:Th</ta>
            <ta e="T84" id="Seg_12396" s="T83">0.1.h:Th</ta>
            <ta e="T85" id="Seg_12397" s="T84">0.1.h:E</ta>
            <ta e="T86" id="Seg_12398" s="T85">np:P</ta>
            <ta e="T88" id="Seg_12399" s="T87">0.1.h:A</ta>
            <ta e="T89" id="Seg_12400" s="T88">adv:Time</ta>
            <ta e="T90" id="Seg_12401" s="T89">pro.h:A</ta>
            <ta e="T92" id="Seg_12402" s="T91">pro.h:A</ta>
            <ta e="T94" id="Seg_12403" s="T93">pro:G</ta>
            <ta e="T97" id="Seg_12404" s="T96">0.1.h:A</ta>
            <ta e="T100" id="Seg_12405" s="T99">np.h:E</ta>
            <ta e="T103" id="Seg_12406" s="T102">adv:Time</ta>
            <ta e="T104" id="Seg_12407" s="T103">0.3.h:A</ta>
            <ta e="T105" id="Seg_12408" s="T104">np:P</ta>
            <ta e="T107" id="Seg_12409" s="T106">pro.h:Poss</ta>
            <ta e="T108" id="Seg_12410" s="T107">np:Th</ta>
            <ta e="T111" id="Seg_12411" s="T110">0.3.h:E</ta>
            <ta e="T112" id="Seg_12412" s="T111">np:Th</ta>
            <ta e="T114" id="Seg_12413" s="T113">0.2.h:A</ta>
            <ta e="T115" id="Seg_12414" s="T114">0.2.h:A</ta>
            <ta e="T116" id="Seg_12415" s="T115">np:Th</ta>
            <ta e="T117" id="Seg_12416" s="T116">pro.h:A</ta>
            <ta e="T119" id="Seg_12417" s="T118">adv:L</ta>
            <ta e="T120" id="Seg_12418" s="T119">np.h:Th</ta>
            <ta e="T123" id="Seg_12419" s="T122">0.2.h:A</ta>
            <ta e="T125" id="Seg_12420" s="T124">adv:Time</ta>
            <ta e="T126" id="Seg_12421" s="T125">np:Th</ta>
            <ta e="T127" id="Seg_12422" s="T126">0.1.h:A</ta>
            <ta e="T129" id="Seg_12423" s="T128">pro.h:A</ta>
            <ta e="T131" id="Seg_12424" s="T130">pro.h:E</ta>
            <ta e="T134" id="Seg_12425" s="T133">pro.h:Poss</ta>
            <ta e="T142" id="Seg_12426" s="T141">np:P</ta>
            <ta e="T144" id="Seg_12427" s="T143">0.3.h:A</ta>
            <ta e="T145" id="Seg_12428" s="T144">pro.h:A</ta>
            <ta e="T149" id="Seg_12429" s="T148">np:Th</ta>
            <ta e="T153" id="Seg_12430" s="T151">adv:Time</ta>
            <ta e="T154" id="Seg_12431" s="T153">adv:Time</ta>
            <ta e="T155" id="Seg_12432" s="T154">np.h:A</ta>
            <ta e="T159" id="Seg_12433" s="T158">np.h:R</ta>
            <ta e="T160" id="Seg_12434" s="T159">0.3.h:A</ta>
            <ta e="T161" id="Seg_12435" s="T160">0.2.h:A</ta>
            <ta e="T163" id="Seg_12436" s="T162">pro.h:A</ta>
            <ta e="T165" id="Seg_12437" s="T164">pro.h:E</ta>
            <ta e="T169" id="Seg_12438" s="T168">adv:Time</ta>
            <ta e="T170" id="Seg_12439" s="T169">adv:L</ta>
            <ta e="T172" id="Seg_12440" s="T171">0.3.h:A</ta>
            <ta e="T173" id="Seg_12441" s="T172">np:P 0.3.h:Poss</ta>
            <ta e="T175" id="Seg_12442" s="T174">0.3.h:A</ta>
            <ta e="T177" id="Seg_12443" s="T176">0.3.h:A</ta>
            <ta e="T178" id="Seg_12444" s="T177">np:Th</ta>
            <ta e="T179" id="Seg_12445" s="T178">adv:Time</ta>
            <ta e="T180" id="Seg_12446" s="T179">np.h:A</ta>
            <ta e="T183" id="Seg_12447" s="T182">0.2.h:A</ta>
            <ta e="T186" id="Seg_12448" s="T185">pro.h:A</ta>
            <ta e="T188" id="Seg_12449" s="T187">0.1.h:A</ta>
            <ta e="T191" id="Seg_12450" s="T190">np.h:E</ta>
            <ta e="T192" id="Seg_12451" s="T191">adv:Time</ta>
            <ta e="T193" id="Seg_12452" s="T192">0.3.h:A</ta>
            <ta e="T194" id="Seg_12453" s="T193">np:P</ta>
            <ta e="T196" id="Seg_12454" s="T195">adv:Time</ta>
            <ta e="T198" id="Seg_12455" s="T197">0.3.h:A</ta>
            <ta e="T199" id="Seg_12456" s="T198">pro.h:A</ta>
            <ta e="T201" id="Seg_12457" s="T200">np:So</ta>
            <ta e="T203" id="Seg_12458" s="T202">adv:Time</ta>
            <ta e="T204" id="Seg_12459" s="T203">np:A</ta>
            <ta e="T206" id="Seg_12460" s="T205">np:P</ta>
            <ta e="T207" id="Seg_12461" s="T206">0.3:A</ta>
            <ta e="T208" id="Seg_12462" s="T207">np:P</ta>
            <ta e="T209" id="Seg_12463" s="T208">0.3.h:A</ta>
            <ta e="T210" id="Seg_12464" s="T209">adv:Time</ta>
            <ta e="T212" id="Seg_12465" s="T211">np:So</ta>
            <ta e="T213" id="Seg_12466" s="T212">np:A</ta>
            <ta e="T217" id="Seg_12467" s="T216">np:P</ta>
            <ta e="T219" id="Seg_12468" s="T218">pro.h:A</ta>
            <ta e="T229" id="Seg_12469" s="T228">adv:Time</ta>
            <ta e="T230" id="Seg_12470" s="T229">pro.h:A</ta>
            <ta e="T232" id="Seg_12471" s="T231">np:Th</ta>
            <ta e="T233" id="Seg_12472" s="T232">np:Th</ta>
            <ta e="T235" id="Seg_12473" s="T234">0.3.h:A</ta>
            <ta e="T236" id="Seg_12474" s="T235">adv:Time</ta>
            <ta e="T237" id="Seg_12475" s="T236">np:Ins</ta>
            <ta e="T239" id="Seg_12476" s="T238">0.3.h:A</ta>
            <ta e="T240" id="Seg_12477" s="T239">adv:L</ta>
            <ta e="T241" id="Seg_12478" s="T240">np:Th</ta>
            <ta e="T242" id="Seg_12479" s="T241">0.3.h:E</ta>
            <ta e="T243" id="Seg_12480" s="T242">np:L</ta>
            <ta e="T244" id="Seg_12481" s="T243">np:Th</ta>
            <ta e="T248" id="Seg_12482" s="T247">pro.h:Poss</ta>
            <ta e="T249" id="Seg_12483" s="T248">np.h:A</ta>
            <ta e="T253" id="Seg_12484" s="T252">pro.h:Poss</ta>
            <ta e="T254" id="Seg_12485" s="T253">np:G</ta>
            <ta e="T258" id="Seg_12486" s="T257">adv:Time</ta>
            <ta e="T260" id="Seg_12487" s="T259">np.h:A</ta>
            <ta e="T262" id="Seg_12488" s="T261">0.2.h:A</ta>
            <ta e="T269" id="Seg_12489" s="T268">np.h:A</ta>
            <ta e="T270" id="Seg_12490" s="T269">pro.h:Poss</ta>
            <ta e="T271" id="Seg_12491" s="T270">np:G</ta>
            <ta e="T273" id="Seg_12492" s="T272">0.3.h:A</ta>
            <ta e="T275" id="Seg_12493" s="T274">np:Th</ta>
            <ta e="T278" id="Seg_12494" s="T1020">0.3.h:A</ta>
            <ta e="T280" id="Seg_12495" s="T279">pro.h:Poss</ta>
            <ta e="T281" id="Seg_12496" s="T280">np.h:A</ta>
            <ta e="T282" id="Seg_12497" s="T281">pro.h:Poss</ta>
            <ta e="T283" id="Seg_12498" s="T282">np.h:Th</ta>
            <ta e="T288" id="Seg_12499" s="T1021">0.3.h:A</ta>
            <ta e="T289" id="Seg_12500" s="T288">pro.h:E</ta>
            <ta e="T293" id="Seg_12501" s="T292">pro.h:E</ta>
            <ta e="T294" id="Seg_12502" s="T293">pro.h:B</ta>
            <ta e="T296" id="Seg_12503" s="T295">np:G</ta>
            <ta e="T297" id="Seg_12504" s="T296">0.3.h:A</ta>
            <ta e="T299" id="Seg_12505" s="T298">adv:Time</ta>
            <ta e="T300" id="Seg_12506" s="T1022">0.3.h:A</ta>
            <ta e="T301" id="Seg_12507" s="T300">np:G</ta>
            <ta e="T303" id="Seg_12508" s="T302">adv:L</ta>
            <ta e="T304" id="Seg_12509" s="T303">0.3.h:P</ta>
            <ta e="T307" id="Seg_12510" s="T306">pro.h:Th</ta>
            <ta e="T315" id="Seg_12511" s="T314">pro.h:Th</ta>
            <ta e="T318" id="Seg_12512" s="T317">np:Th 0.3.h:Poss</ta>
            <ta e="T320" id="Seg_12513" s="T319">np:Th 0.3.h:Poss</ta>
            <ta e="T324" id="Seg_12514" s="T323">np.h:E</ta>
            <ta e="T326" id="Seg_12515" s="T325">0.3.h:A</ta>
            <ta e="T327" id="Seg_12516" s="T326">np:Th</ta>
            <ta e="T330" id="Seg_12517" s="T329">np.h:A</ta>
            <ta e="T332" id="Seg_12518" s="T331">np:Th</ta>
            <ta e="T334" id="Seg_12519" s="T333">np.h:A</ta>
            <ta e="T335" id="Seg_12520" s="T334">np:Th</ta>
            <ta e="T337" id="Seg_12521" s="T336">np:Th</ta>
            <ta e="T338" id="Seg_12522" s="T337">0.3.h:A</ta>
            <ta e="T341" id="Seg_12523" s="T340">np:Th</ta>
            <ta e="T342" id="Seg_12524" s="T341">0.3.h:A</ta>
            <ta e="T343" id="Seg_12525" s="T342">pro.h:E</ta>
            <ta e="T345" id="Seg_12526" s="T344">np:Ins</ta>
            <ta e="T346" id="Seg_12527" s="T345">np:P 0.3.h:Poss</ta>
            <ta e="T347" id="Seg_12528" s="T346">0.3.h:A</ta>
            <ta e="T348" id="Seg_12529" s="T347">adv:Time</ta>
            <ta e="T349" id="Seg_12530" s="T348">0.3.h:A</ta>
            <ta e="T350" id="Seg_12531" s="T349">np:G</ta>
            <ta e="T351" id="Seg_12532" s="T350">np.h:A</ta>
            <ta e="T353" id="Seg_12533" s="T352">pro:L</ta>
            <ta e="T354" id="Seg_12534" s="T353">pro.h:Poss</ta>
            <ta e="T355" id="Seg_12535" s="T354">np.h:Th</ta>
            <ta e="T356" id="Seg_12536" s="T355">pro.h:Th</ta>
            <ta e="T360" id="Seg_12537" s="T359">0.3.h:A</ta>
            <ta e="T361" id="Seg_12538" s="T360">np:Th</ta>
            <ta e="T362" id="Seg_12539" s="T361">adv:L</ta>
            <ta e="T364" id="Seg_12540" s="T363">0.3.h:E</ta>
            <ta e="T366" id="Seg_12541" s="T365">np.h:E</ta>
            <ta e="T374" id="Seg_12542" s="T373">0.3.h:A</ta>
            <ta e="T375" id="Seg_12543" s="T374">np.h:Th</ta>
            <ta e="T377" id="Seg_12544" s="T376">pro.h:A</ta>
            <ta e="T378" id="Seg_12545" s="T377">pro.h:Th</ta>
            <ta e="T383" id="Seg_12546" s="T382">0.3.h:A</ta>
            <ta e="T385" id="Seg_12547" s="T384">pro.h:A</ta>
            <ta e="T386" id="Seg_12548" s="T385">adv:G</ta>
            <ta e="T390" id="Seg_12549" s="T389">np:G</ta>
            <ta e="T392" id="Seg_12550" s="T391">0.3.h:A</ta>
            <ta e="T393" id="Seg_12551" s="T392">adv:Time</ta>
            <ta e="T396" id="Seg_12552" s="T395">np.h:A</ta>
            <ta e="T398" id="Seg_12553" s="T397">pro:L</ta>
            <ta e="T399" id="Seg_12554" s="T398">pro.h:Th</ta>
            <ta e="T401" id="Seg_12555" s="T400">pro.h:A</ta>
            <ta e="T403" id="Seg_12556" s="T402">pro.h:E</ta>
            <ta e="T404" id="Seg_12557" s="T403">adv:L</ta>
            <ta e="T406" id="Seg_12558" s="T405">pro.h:A</ta>
            <ta e="T411" id="Seg_12559" s="T410">np:G</ta>
            <ta e="T413" id="Seg_12560" s="T412">pro:Th</ta>
            <ta e="T415" id="Seg_12561" s="T414">0.3.h:E</ta>
            <ta e="T416" id="Seg_12562" s="T415">adv:Time</ta>
            <ta e="T417" id="Seg_12563" s="T416">np:E</ta>
            <ta e="T418" id="Seg_12564" s="T417">pro:Th</ta>
            <ta e="T421" id="Seg_12565" s="T420">pro.h:A</ta>
            <ta e="T423" id="Seg_12566" s="T422">np:G</ta>
            <ta e="T424" id="Seg_12567" s="T423">np.h:Th</ta>
            <ta e="T425" id="Seg_12568" s="T424">0.3.h:A</ta>
            <ta e="T427" id="Seg_12569" s="T1023">0.3.h:A</ta>
            <ta e="T428" id="Seg_12570" s="T427">pro:L</ta>
            <ta e="T429" id="Seg_12571" s="T428">np.h:Th</ta>
            <ta e="T432" id="Seg_12572" s="T431">pro.h:Poss</ta>
            <ta e="T433" id="Seg_12573" s="T432">np.h:A</ta>
            <ta e="T435" id="Seg_12574" s="T434">pro.h:E</ta>
            <ta e="T439" id="Seg_12575" s="T438">np:E</ta>
            <ta e="T447" id="Seg_12576" s="T446">pro.h:Poss</ta>
            <ta e="T449" id="Seg_12577" s="T448">np.h:Poss </ta>
            <ta e="T450" id="Seg_12578" s="T449">np.h:A</ta>
            <ta e="T451" id="Seg_12579" s="T450">np.h:A</ta>
            <ta e="T452" id="Seg_12580" s="T451">np:G</ta>
            <ta e="T456" id="Seg_12581" s="T455">np:G</ta>
            <ta e="T457" id="Seg_12582" s="T456">0.3.h:A</ta>
            <ta e="T458" id="Seg_12583" s="T457">np:Th</ta>
            <ta e="T460" id="Seg_12584" s="T459">adv:Time</ta>
            <ta e="T461" id="Seg_12585" s="T460">np:G</ta>
            <ta e="T462" id="Seg_12586" s="T461">0.3.h:A</ta>
            <ta e="T464" id="Seg_12587" s="T463">np.h:R</ta>
            <ta e="T465" id="Seg_12588" s="T464">0.3.h:A</ta>
            <ta e="T469" id="Seg_12589" s="T468">0.3.h:A</ta>
            <ta e="T471" id="Seg_12590" s="T470">pro.h:Th</ta>
            <ta e="T474" id="Seg_12591" s="T473">0.1.h:E</ta>
            <ta e="T477" id="Seg_12592" s="T476">pro.h:A</ta>
            <ta e="T479" id="Seg_12593" s="T478">pro.h:Com</ta>
            <ta e="T483" id="Seg_12594" s="T482">adv:Time</ta>
            <ta e="T486" id="Seg_12595" s="T485">pro.h:A</ta>
            <ta e="T487" id="Seg_12596" s="T486">np:P</ta>
            <ta e="T491" id="Seg_12597" s="T490">np.h:A</ta>
            <ta e="T498" id="Seg_12598" s="T497">np:Th</ta>
            <ta e="T500" id="Seg_12599" s="T499">0.3.h:A</ta>
            <ta e="T501" id="Seg_12600" s="T500">adv:Time</ta>
            <ta e="T502" id="Seg_12601" s="T501">np.h:A</ta>
            <ta e="T503" id="Seg_12602" s="T502">pro.h:R</ta>
            <ta e="T505" id="Seg_12603" s="T504">pro:G</ta>
            <ta e="T506" id="Seg_12604" s="T505">0.2.h:A</ta>
            <ta e="T508" id="Seg_12605" s="T507">0.2.h:A</ta>
            <ta e="T509" id="Seg_12606" s="T508">pro.h:Th</ta>
            <ta e="T511" id="Seg_12607" s="T510">pro.h:A</ta>
            <ta e="T512" id="Seg_12608" s="T511">pro:Th</ta>
            <ta e="T517" id="Seg_12609" s="T516">adv:Time</ta>
            <ta e="T518" id="Seg_12610" s="T517">0.3.h:A</ta>
            <ta e="T521" id="Seg_12611" s="T520">np.h:A</ta>
            <ta e="T523" id="Seg_12612" s="T522">pro:G</ta>
            <ta e="T524" id="Seg_12613" s="T523">0.2.h:A</ta>
            <ta e="T526" id="Seg_12614" s="T525">pro.h:Th</ta>
            <ta e="T527" id="Seg_12615" s="T526">0.2.h:A</ta>
            <ta e="T529" id="Seg_12616" s="T528">pro.h:A</ta>
            <ta e="T530" id="Seg_12617" s="T529">pro:Th</ta>
            <ta e="T535" id="Seg_12618" s="T534">adv:Time</ta>
            <ta e="T536" id="Seg_12619" s="T535">0.3.h:A</ta>
            <ta e="T540" id="Seg_12620" s="T539">pro.h:A</ta>
            <ta e="T542" id="Seg_12621" s="T541">pro.h:A</ta>
            <ta e="T543" id="Seg_12622" s="T542">pro:P</ta>
            <ta e="T546" id="Seg_12623" s="T545">np:P</ta>
            <ta e="T547" id="Seg_12624" s="T546">0.1.h:A</ta>
            <ta e="T548" id="Seg_12625" s="T547">np:P</ta>
            <ta e="T549" id="Seg_12626" s="T548">np:P</ta>
            <ta e="T552" id="Seg_12627" s="T551">pro.h:A</ta>
            <ta e="T555" id="Seg_12628" s="T554">np:P</ta>
            <ta e="T556" id="Seg_12629" s="T555">pro.h:A</ta>
            <ta e="T565" id="Seg_12630" s="T564">0.1.h:A</ta>
            <ta e="T567" id="Seg_12631" s="T566">adv:Time</ta>
            <ta e="T569" id="Seg_12632" s="T568">np:Th</ta>
            <ta e="T570" id="Seg_12633" s="T569">0.3.h:A</ta>
            <ta e="T571" id="Seg_12634" s="T570">adv:Time</ta>
            <ta e="T574" id="Seg_12635" s="T573">np.h:A</ta>
            <ta e="T580" id="Seg_12636" s="T579">0.3.h:A</ta>
            <ta e="T582" id="Seg_12637" s="T581">0.3.h:A</ta>
            <ta e="T583" id="Seg_12638" s="T582">0.3.h:A</ta>
            <ta e="T585" id="Seg_12639" s="T584">0.3.h:A</ta>
            <ta e="T587" id="Seg_12640" s="T586">pro:P</ta>
            <ta e="T588" id="Seg_12641" s="T587">0.3.h:A</ta>
            <ta e="T590" id="Seg_12642" s="T589">np:Th</ta>
            <ta e="T593" id="Seg_12643" s="T592">pro:Th</ta>
            <ta e="T596" id="Seg_12644" s="T595">pro.h:A</ta>
            <ta e="T598" id="Seg_12645" s="T597">0.2.h:A</ta>
            <ta e="T599" id="Seg_12646" s="T598">np:Th</ta>
            <ta e="T600" id="Seg_12647" s="T599">np:Th</ta>
            <ta e="T601" id="Seg_12648" s="T600">0.2.h:A</ta>
            <ta e="T602" id="Seg_12649" s="T601">np:Th</ta>
            <ta e="T604" id="Seg_12650" s="T603">adv:Time</ta>
            <ta e="T612" id="Seg_12651" s="T611">adv:Time</ta>
            <ta e="T622" id="Seg_12652" s="T621">np.h:A</ta>
            <ta e="T623" id="Seg_12653" s="T622">adv:Time</ta>
            <ta e="T624" id="Seg_12654" s="T623">pro.h:A</ta>
            <ta e="T627" id="Seg_12655" s="T626">0.3.h:A</ta>
            <ta e="T629" id="Seg_12656" s="T628">0.3.h:A</ta>
            <ta e="T630" id="Seg_12657" s="T629">0.3.h:A</ta>
            <ta e="T632" id="Seg_12658" s="T631">0.3.h:A</ta>
            <ta e="T633" id="Seg_12659" s="T632">np:P</ta>
            <ta e="T634" id="Seg_12660" s="T633">0.3.h:A</ta>
            <ta e="T636" id="Seg_12661" s="T635">0.3.h:A</ta>
            <ta e="T637" id="Seg_12662" s="T636">np:Th</ta>
            <ta e="T639" id="Seg_12663" s="T638">0.2.h:A</ta>
            <ta e="T640" id="Seg_12664" s="T639">pro.h:R</ta>
            <ta e="T643" id="Seg_12665" s="T642">np:L</ta>
            <ta e="T646" id="Seg_12666" s="T645">np:Th</ta>
            <ta e="T649" id="Seg_12667" s="T648">adv:Time</ta>
            <ta e="T651" id="Seg_12668" s="T650">0.3.h:A</ta>
            <ta e="T653" id="Seg_12669" s="T652">np:P</ta>
            <ta e="T655" id="Seg_12670" s="T654">0.3.h:A</ta>
            <ta e="T656" id="Seg_12671" s="T655">pro.h:B</ta>
            <ta e="T658" id="Seg_12672" s="T657">np:P 0.3.h:Poss</ta>
            <ta e="T659" id="Seg_12673" s="T658">0.3.h:A</ta>
            <ta e="T662" id="Seg_12674" s="T661">0.3.h:A</ta>
            <ta e="T665" id="Seg_12675" s="T664">adv:Time</ta>
            <ta e="T667" id="Seg_12676" s="T666">np:Th</ta>
            <ta e="T668" id="Seg_12677" s="T667">0.3.h:A</ta>
            <ta e="T670" id="Seg_12678" s="T669">np:G</ta>
            <ta e="T671" id="Seg_12679" s="T670">0.3.h:A</ta>
            <ta e="T674" id="Seg_12680" s="T673">pro:Th</ta>
            <ta e="T675" id="Seg_12681" s="T674">0.2.h:A</ta>
            <ta e="T681" id="Seg_12682" s="T680">0.2.h:A</ta>
            <ta e="T687" id="Seg_12683" s="T686">0.2.h:E</ta>
            <ta e="T691" id="Seg_12684" s="T690">np.h:Poss</ta>
            <ta e="T694" id="Seg_12685" s="T693">np:Th</ta>
            <ta e="T698" id="Seg_12686" s="T697">np.h:Poss</ta>
            <ta e="T700" id="Seg_12687" s="T699">np:Th</ta>
            <ta e="T708" id="Seg_12688" s="T707">pro.h:A</ta>
            <ta e="T709" id="Seg_12689" s="T708">pro.h:Poss</ta>
            <ta e="T710" id="Seg_12690" s="T709">np.h:Poss</ta>
            <ta e="T712" id="Seg_12691" s="T711">np:Th</ta>
            <ta e="T715" id="Seg_12692" s="T714">0.3.h:A</ta>
            <ta e="T716" id="Seg_12693" s="T715">pro.h:E</ta>
            <ta e="T720" id="Seg_12694" s="T719">np.h:A</ta>
            <ta e="T723" id="Seg_12695" s="T722">0.2.h:E</ta>
            <ta e="T724" id="Seg_12696" s="T723">0.1.h:A</ta>
            <ta e="T726" id="Seg_12697" s="T725">0.1.h:A</ta>
            <ta e="T727" id="Seg_12698" s="T726">np:Th</ta>
            <ta e="T728" id="Seg_12699" s="T727">pro.h:A</ta>
            <ta e="T731" id="Seg_12700" s="T730">np.h:A</ta>
            <ta e="T733" id="Seg_12701" s="T732">0.2.h:A</ta>
            <ta e="T734" id="Seg_12702" s="T733">np:Th</ta>
            <ta e="T735" id="Seg_12703" s="T734">0.2.h:A</ta>
            <ta e="T736" id="Seg_12704" s="T735">np:Th</ta>
            <ta e="T739" id="Seg_12705" s="T738">np.h:A</ta>
            <ta e="T741" id="Seg_12706" s="T740">0.2.h:A</ta>
            <ta e="T742" id="Seg_12707" s="T741">pro:Th</ta>
            <ta e="T744" id="Seg_12708" s="T743">np:R</ta>
            <ta e="T745" id="Seg_12709" s="T744">0.3.h:A</ta>
            <ta e="T746" id="Seg_12710" s="T745">pro.h:A</ta>
            <ta e="T747" id="Seg_12711" s="T746">adv:So</ta>
            <ta e="T750" id="Seg_12712" s="T749">0.3.h:A</ta>
            <ta e="T752" id="Seg_12713" s="T751">0.2.h:A</ta>
            <ta e="T753" id="Seg_12714" s="T752">np:Th</ta>
            <ta e="T754" id="Seg_12715" s="T753">0.2.h:A</ta>
            <ta e="T755" id="Seg_12716" s="T754">np:Th</ta>
            <ta e="T756" id="Seg_12717" s="T755">adv:Time</ta>
            <ta e="T757" id="Seg_12718" s="T756">0.2.h:A</ta>
            <ta e="T759" id="Seg_12719" s="T758">pro:Th</ta>
            <ta e="T760" id="Seg_12720" s="T759">np:G</ta>
            <ta e="T761" id="Seg_12721" s="T760">0.3.h:A</ta>
            <ta e="T762" id="Seg_12722" s="T761">np:G</ta>
            <ta e="T763" id="Seg_12723" s="T762">pro.h:A</ta>
            <ta e="T764" id="Seg_12724" s="T763">np:L</ta>
            <ta e="T768" id="Seg_12725" s="T767">adv:Time</ta>
            <ta e="T770" id="Seg_12726" s="T769">0.3.h:A</ta>
            <ta e="T773" id="Seg_12727" s="T772">0.2.h:A</ta>
            <ta e="T774" id="Seg_12728" s="T773">np:Th</ta>
            <ta e="T775" id="Seg_12729" s="T774">0.2.h:A</ta>
            <ta e="T777" id="Seg_12730" s="T775">np:Th</ta>
            <ta e="T778" id="Seg_12731" s="T777">0.2.h:A</ta>
            <ta e="T779" id="Seg_12732" s="T778">np:G</ta>
            <ta e="T780" id="Seg_12733" s="T779">0.3.h:A</ta>
            <ta e="T781" id="Seg_12734" s="T780">np:G</ta>
            <ta e="T782" id="Seg_12735" s="T781">adv:Time</ta>
            <ta e="T784" id="Seg_12736" s="T783">0.3.h:A</ta>
            <ta e="T786" id="Seg_12737" s="T785">0.3.h:A</ta>
            <ta e="T787" id="Seg_12738" s="T786">0.2.h:A</ta>
            <ta e="T788" id="Seg_12739" s="T787">np:Th</ta>
            <ta e="T789" id="Seg_12740" s="T788">0.2.h:A</ta>
            <ta e="T790" id="Seg_12741" s="T789">np:Th</ta>
            <ta e="T791" id="Seg_12742" s="T790">adv:Time</ta>
            <ta e="T792" id="Seg_12743" s="T791">0.3.h:A</ta>
            <ta e="T793" id="Seg_12744" s="T792">0.2.h:A</ta>
            <ta e="T794" id="Seg_12745" s="T793">pro:Th</ta>
            <ta e="T795" id="Seg_12746" s="T794">np:G</ta>
            <ta e="T796" id="Seg_12747" s="T795">pro:Th</ta>
            <ta e="T797" id="Seg_12748" s="T796">0.3.h:A</ta>
            <ta e="T799" id="Seg_12749" s="T798">pro.h:A</ta>
            <ta e="T802" id="Seg_12750" s="T800">0.1.h:Poss</ta>
            <ta e="T804" id="Seg_12751" s="T803">0.3.h:A</ta>
            <ta e="T806" id="Seg_12752" s="T805">pro:Th</ta>
            <ta e="T807" id="Seg_12753" s="T806">np:G</ta>
            <ta e="T809" id="Seg_12754" s="T808">pro.h:A</ta>
            <ta e="T811" id="Seg_12755" s="T810">0.1.h:Poss</ta>
            <ta e="T812" id="Seg_12756" s="T811">0.2.h:A</ta>
            <ta e="T813" id="Seg_12757" s="T812">np:P</ta>
            <ta e="T814" id="Seg_12758" s="T813">0.1.h:Poss</ta>
            <ta e="T815" id="Seg_12759" s="T814">0.2.h:A</ta>
            <ta e="T816" id="Seg_12760" s="T815">np:P</ta>
            <ta e="T817" id="Seg_12761" s="T816">np:A</ta>
            <ta e="T819" id="Seg_12762" s="T818">np:P</ta>
            <ta e="T822" id="Seg_12763" s="T821">0.3.h:A</ta>
            <ta e="T823" id="Seg_12764" s="T822">0.2.h:A</ta>
            <ta e="T824" id="Seg_12765" s="T823">np:Th</ta>
            <ta e="T825" id="Seg_12766" s="T824">0.2.h:A</ta>
            <ta e="T826" id="Seg_12767" s="T825">np:Th</ta>
            <ta e="T827" id="Seg_12768" s="T826">adv:Time</ta>
            <ta e="T829" id="Seg_12769" s="T828">np.h:A</ta>
            <ta e="T831" id="Seg_12770" s="T830">np:P</ta>
            <ta e="T833" id="Seg_12771" s="T832">0.2.h:A</ta>
            <ta e="T836" id="Seg_12772" s="T835">0.2.h:A</ta>
            <ta e="T837" id="Seg_12773" s="T836">pro:Th</ta>
            <ta e="T838" id="Seg_12774" s="T837">np:G</ta>
            <ta e="T839" id="Seg_12775" s="T838">adv:Time</ta>
            <ta e="T840" id="Seg_12776" s="T839">0.3.h:A</ta>
            <ta e="T841" id="Seg_12777" s="T840">np:P</ta>
            <ta e="T842" id="Seg_12778" s="T841">0.3.h:A</ta>
            <ta e="T844" id="Seg_12779" s="T843">np:Th</ta>
            <ta e="T845" id="Seg_12780" s="T844">np:G</ta>
            <ta e="T846" id="Seg_12781" s="T845">adv:L</ta>
            <ta e="T847" id="Seg_12782" s="T846">0.3.h:A</ta>
            <ta e="T848" id="Seg_12783" s="T847">0.2.h:A</ta>
            <ta e="T849" id="Seg_12784" s="T848">0.1.h:Poss</ta>
            <ta e="T850" id="Seg_12785" s="T849">np:Th</ta>
            <ta e="T851" id="Seg_12786" s="T850">0.2.h:A</ta>
            <ta e="T852" id="Seg_12787" s="T851">0.1.h:Poss</ta>
            <ta e="T853" id="Seg_12788" s="T852">np:Th</ta>
            <ta e="T855" id="Seg_12789" s="T854">np:Th</ta>
            <ta e="T862" id="Seg_12790" s="T861">adv:Time</ta>
            <ta e="T863" id="Seg_12791" s="T862">np:Th</ta>
            <ta e="T869" id="Seg_12792" s="T868">np.h:A</ta>
            <ta e="T871" id="Seg_12793" s="T870">0.2.h:A</ta>
            <ta e="T872" id="Seg_12794" s="T871">np:Th</ta>
            <ta e="T874" id="Seg_12795" s="T873">0.2.h:A</ta>
            <ta e="T877" id="Seg_12796" s="T876">adv:L</ta>
            <ta e="T884" id="Seg_12797" s="T883">0.3.h:A</ta>
            <ta e="T885" id="Seg_12798" s="T884">pro.h:A</ta>
            <ta e="T886" id="Seg_12799" s="T885">pro:Th</ta>
            <ta e="T890" id="Seg_12800" s="T889">adv:Time</ta>
            <ta e="T891" id="Seg_12801" s="T890">pro.h:A</ta>
            <ta e="T894" id="Seg_12802" s="T893">pro.h:A</ta>
            <ta e="T898" id="Seg_12803" s="T897">0.3.h:P</ta>
            <ta e="T900" id="Seg_12804" s="T899">adv:Time</ta>
            <ta e="T901" id="Seg_12805" s="T900">pro.h:Poss</ta>
            <ta e="T902" id="Seg_12806" s="T901">np.h:A</ta>
            <ta e="T905" id="Seg_12807" s="T904">np:G</ta>
            <ta e="T906" id="Seg_12808" s="T905">pro.h:A</ta>
            <ta e="T909" id="Seg_12809" s="T908">pro.h:Com</ta>
            <ta e="T911" id="Seg_12810" s="T910">0.1.h:A</ta>
            <ta e="T912" id="Seg_12811" s="T911">np:Th</ta>
            <ta e="T914" id="Seg_12812" s="T913">0.3.h:A</ta>
            <ta e="T915" id="Seg_12813" s="T914">adv:Time</ta>
            <ta e="T916" id="Seg_12814" s="T915">pro.h:A</ta>
            <ta e="T920" id="Seg_12815" s="T919">np:Th</ta>
            <ta e="T921" id="Seg_12816" s="T920">np:Th</ta>
            <ta e="T924" id="Seg_12817" s="T923">np:A</ta>
            <ta e="T927" id="Seg_12818" s="T926">pro.h:A</ta>
            <ta e="T929" id="Seg_12819" s="T928">adv:L</ta>
            <ta e="T930" id="Seg_12820" s="T929">0.3.h:A</ta>
            <ta e="T932" id="Seg_12821" s="T931">adv:Time</ta>
            <ta e="T933" id="Seg_12822" s="T932">pro.h:A</ta>
            <ta e="T935" id="Seg_12823" s="T934">0.1.h:A</ta>
            <ta e="T937" id="Seg_12824" s="T936">np:P</ta>
            <ta e="T938" id="Seg_12825" s="T937">0.1.h:A</ta>
            <ta e="T939" id="Seg_12826" s="T938">pro.h:B</ta>
            <ta e="T940" id="Seg_12827" s="T939">np:B</ta>
            <ta e="T941" id="Seg_12828" s="T940">0.3.h:A</ta>
            <ta e="T942" id="Seg_12829" s="T941">adv:Time</ta>
            <ta e="T943" id="Seg_12830" s="T1026">0.3.h:A</ta>
            <ta e="T944" id="Seg_12831" s="T943">np:Ins</ta>
            <ta e="T945" id="Seg_12832" s="T944">np.h:Com</ta>
            <ta e="T947" id="Seg_12833" s="T946">adv:Time</ta>
            <ta e="T948" id="Seg_12834" s="T947">0.1.h:A</ta>
            <ta e="T949" id="Seg_12835" s="T948">np:G</ta>
            <ta e="T950" id="Seg_12836" s="T949">adv:Time</ta>
            <ta e="T951" id="Seg_12837" s="T950">0.1.h:A</ta>
            <ta e="T952" id="Seg_12838" s="T951">0.1.h:A</ta>
            <ta e="T953" id="Seg_12839" s="T952">np:G</ta>
            <ta e="T956" id="Seg_12840" s="T955">adv:L</ta>
            <ta e="T957" id="Seg_12841" s="T956">np.h:E</ta>
            <ta e="T959" id="Seg_12842" s="T958">pro.h:B</ta>
            <ta e="T960" id="Seg_12843" s="T959">0.3.h:A</ta>
            <ta e="T961" id="Seg_12844" s="T960">adv:Time</ta>
            <ta e="T962" id="Seg_12845" s="T961">0.1.h:A</ta>
            <ta e="T963" id="Seg_12846" s="T962">np:G</ta>
            <ta e="T965" id="Seg_12847" s="T964">0.1.h:A</ta>
            <ta e="T966" id="Seg_12848" s="T965">np:R</ta>
            <ta e="T967" id="Seg_12849" s="T966">0.1.h:A</ta>
            <ta e="T968" id="Seg_12850" s="T967">adv:Time</ta>
            <ta e="T969" id="Seg_12851" s="T968">np:Th</ta>
            <ta e="T970" id="Seg_12852" s="T969">0.1.h:A</ta>
            <ta e="T971" id="Seg_12853" s="T970">np:G</ta>
            <ta e="T972" id="Seg_12854" s="T971">0.1.h:A</ta>
            <ta e="T973" id="Seg_12855" s="T972">adv:Time</ta>
            <ta e="T977" id="Seg_12856" s="T976">np.h:A</ta>
            <ta e="T979" id="Seg_12857" s="T978">pro:G</ta>
            <ta e="T982" id="Seg_12858" s="T981">pro.h:Th</ta>
            <ta e="T985" id="Seg_12859" s="T984">0.3.h:A</ta>
            <ta e="T986" id="Seg_12860" s="T985">pro.h:P</ta>
            <ta e="T987" id="Seg_12861" s="T986">adv:Time</ta>
            <ta e="T990" id="Seg_12862" s="T989">0.1.h:A</ta>
            <ta e="T991" id="Seg_12863" s="T990">np:G</ta>
            <ta e="T994" id="Seg_12864" s="T993">adv:Time</ta>
            <ta e="T996" id="Seg_12865" s="T995">np:G</ta>
            <ta e="T998" id="Seg_12866" s="T997">np:R</ta>
            <ta e="T999" id="Seg_12867" s="T998">0.1.h:A</ta>
            <ta e="T1000" id="Seg_12868" s="T999">np:Poss</ta>
            <ta e="T1001" id="Seg_12869" s="T1000">np:Th</ta>
            <ta e="T1002" id="Seg_12870" s="T1001">0.1.h:A</ta>
            <ta e="T1007" id="Seg_12871" s="T1006">np:A</ta>
            <ta e="T1008" id="Seg_12872" s="T1007">adv:Time</ta>
            <ta e="T1010" id="Seg_12873" s="T1009">np:So</ta>
            <ta e="T1012" id="Seg_12874" s="T1011">0.3.h:A</ta>
            <ta e="T1015" id="Seg_12875" s="T1014">0.3.h:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST-PKZ" />
         <annotation name="BOR" tierref="BOR-PKZ">
            <ta e="T16" id="Seg_12876" s="T15">RUS:gram</ta>
            <ta e="T30" id="Seg_12877" s="T29">RUS:core</ta>
            <ta e="T35" id="Seg_12878" s="T34">RUS:gram</ta>
            <ta e="T39" id="Seg_12879" s="T38">RUS:core</ta>
            <ta e="T48" id="Seg_12880" s="T47">TURK:disc</ta>
            <ta e="T52" id="Seg_12881" s="T51">RUS:gram</ta>
            <ta e="T66" id="Seg_12882" s="T65">RUS:cult</ta>
            <ta e="T75" id="Seg_12883" s="T74">RUS:cult</ta>
            <ta e="T78" id="Seg_12884" s="T77">RUS:cult</ta>
            <ta e="T95" id="Seg_12885" s="T94">RUS:gram</ta>
            <ta e="T96" id="Seg_12886" s="T95">TURK:disc</ta>
            <ta e="T97" id="Seg_12887" s="T96">%TURK:core</ta>
            <ta e="T102" id="Seg_12888" s="T101">RUS:gram</ta>
            <ta e="T128" id="Seg_12889" s="T127">RUS:gram</ta>
            <ta e="T148" id="Seg_12890" s="T147">RUS:gram</ta>
            <ta e="T158" id="Seg_12891" s="T157">RUS:gram</ta>
            <ta e="T168" id="Seg_12892" s="T167">RUS:disc</ta>
            <ta e="T171" id="Seg_12893" s="T170">TURK:disc</ta>
            <ta e="T174" id="Seg_12894" s="T173">RUS:gram</ta>
            <ta e="T185" id="Seg_12895" s="T184">RUS:disc</ta>
            <ta e="T200" id="Seg_12896" s="T199">TURK:disc</ta>
            <ta e="T215" id="Seg_12897" s="T214">RUS:gram</ta>
            <ta e="T216" id="Seg_12898" s="T215">RUS:gram</ta>
            <ta e="T222" id="Seg_12899" s="T221">RUS:core</ta>
            <ta e="T224" id="Seg_12900" s="T223">TURK:disc</ta>
            <ta e="T232" id="Seg_12901" s="T231">RUS:core</ta>
            <ta e="T233" id="Seg_12902" s="T232">RUS:core</ta>
            <ta e="T241" id="Seg_12903" s="T240">RUS:cult</ta>
            <ta e="T243" id="Seg_12904" s="T242">RUS:cult</ta>
            <ta e="T264" id="Seg_12905" s="T263">RUS:gram</ta>
            <ta e="T274" id="Seg_12906" s="T273">RUS:gram</ta>
            <ta e="T277" id="Seg_12907" s="T276">RUS:gram</ta>
            <ta e="T287" id="Seg_12908" s="T286">RUS:gram</ta>
            <ta e="T290" id="Seg_12909" s="T289">TURK:core</ta>
            <ta e="T292" id="Seg_12910" s="T291">RUS:gram</ta>
            <ta e="T298" id="Seg_12911" s="T297">RUS:gram</ta>
            <ta e="T302" id="Seg_12912" s="T301">RUS:gram</ta>
            <ta e="T306" id="Seg_12913" s="T305">RUS:gram</ta>
            <ta e="T313" id="Seg_12914" s="T311">TURK:disc</ta>
            <ta e="T327" id="Seg_12915" s="T326">RUS:core</ta>
            <ta e="T333" id="Seg_12916" s="T332">RUS:gram</ta>
            <ta e="T339" id="Seg_12917" s="T338">RUS:gram</ta>
            <ta e="T357" id="Seg_12918" s="T356">TURK:disc</ta>
            <ta e="T361" id="Seg_12919" s="T360">RUS:core</ta>
            <ta e="T363" id="Seg_12920" s="T362">TURK:disc</ta>
            <ta e="T365" id="Seg_12921" s="T364">RUS:gram</ta>
            <ta e="T367" id="Seg_12922" s="T366">TURK:gram(INDEF)</ta>
            <ta e="T373" id="Seg_12923" s="T372">RUS:gram</ta>
            <ta e="T384" id="Seg_12924" s="T383">RUS:gram</ta>
            <ta e="T413" id="Seg_12925" s="T412">TURK:gram(INDEF)</ta>
            <ta e="T418" id="Seg_12926" s="T417">TURK:gram(INDEF)</ta>
            <ta e="T426" id="Seg_12927" s="T425">RUS:gram</ta>
            <ta e="T440" id="Seg_12928" s="T439">TURK:disc</ta>
            <ta e="T442" id="Seg_12929" s="T441">RUS:gram</ta>
            <ta e="T449" id="Seg_12930" s="T448">RUS:core</ta>
            <ta e="T452" id="Seg_12931" s="T451">TURK:cult</ta>
            <ta e="T456" id="Seg_12932" s="T455">TURK:cult</ta>
            <ta e="T466" id="Seg_12933" s="T465">RUS:gram</ta>
            <ta e="T470" id="Seg_12934" s="T469">RUS:gram</ta>
            <ta e="T473" id="Seg_12935" s="T472">RUS:gram</ta>
            <ta e="T476" id="Seg_12936" s="T475">RUS:gram</ta>
            <ta e="T483" id="Seg_12937" s="T482">RUS:gram</ta>
            <ta e="T510" id="Seg_12938" s="T509">RUS:gram</ta>
            <ta e="T515" id="Seg_12939" s="T514">RUS:gram</ta>
            <ta e="T528" id="Seg_12940" s="T527">RUS:gram</ta>
            <ta e="T533" id="Seg_12941" s="T532">RUS:gram</ta>
            <ta e="T537" id="Seg_12942" s="T536">RUS:gram</ta>
            <ta e="T538" id="Seg_12943" s="T537">RUS:gram</ta>
            <ta e="T544" id="Seg_12944" s="T543">TURK:disc</ta>
            <ta e="T549" id="Seg_12945" s="T548">TURK:disc</ta>
            <ta e="T551" id="Seg_12946" s="T550">RUS:mod</ta>
            <ta e="T554" id="Seg_12947" s="T553">RUS:disc</ta>
            <ta e="T555" id="Seg_12948" s="T554">TURK:disc</ta>
            <ta e="T558" id="Seg_12949" s="T557">RUS:gram</ta>
            <ta e="T566" id="Seg_12950" s="T565">RUS:disc</ta>
            <ta e="T569" id="Seg_12951" s="T568">RUS:cult</ta>
            <ta e="T577" id="Seg_12952" s="T576">RUS:gram</ta>
            <ta e="T579" id="Seg_12953" s="T578">TURK:disc</ta>
            <ta e="T581" id="Seg_12954" s="T580">RUS:gram</ta>
            <ta e="T584" id="Seg_12955" s="T583">RUS:gram</ta>
            <ta e="T586" id="Seg_12956" s="T585">TURK:disc</ta>
            <ta e="T589" id="Seg_12957" s="T588">RUS:gram</ta>
            <ta e="T591" id="Seg_12958" s="T590">RUS:gram</ta>
            <ta e="T595" id="Seg_12959" s="T594">TURK:disc</ta>
            <ta e="T599" id="Seg_12960" s="T598">RUS:mod</ta>
            <ta e="T602" id="Seg_12961" s="T601">RUS:mod</ta>
            <ta e="T605" id="Seg_12962" s="T604">RUS:disc</ta>
            <ta e="T628" id="Seg_12963" s="T627">RUS:gram</ta>
            <ta e="T631" id="Seg_12964" s="T630">RUS:gram</ta>
            <ta e="T633" id="Seg_12965" s="T632">TURK:disc</ta>
            <ta e="T635" id="Seg_12966" s="T634">RUS:gram</ta>
            <ta e="T637" id="Seg_12967" s="T636">RUS:mod</ta>
            <ta e="T642" id="Seg_12968" s="T641">RUS:gram</ta>
            <ta e="T643" id="Seg_12969" s="T642">RUS:core</ta>
            <ta e="T644" id="Seg_12970" s="T643">TURK:disc</ta>
            <ta e="T652" id="Seg_12971" s="T651">RUS:gram</ta>
            <ta e="T657" id="Seg_12972" s="T656">RUS:gram</ta>
            <ta e="T660" id="Seg_12973" s="T659">RUS:gram</ta>
            <ta e="T661" id="Seg_12974" s="T660">RUS:gram</ta>
            <ta e="T664" id="Seg_12975" s="T662">RUS:gram</ta>
            <ta e="T669" id="Seg_12976" s="T668">RUS:gram</ta>
            <ta e="T677" id="Seg_12977" s="T676">RUS:gram</ta>
            <ta e="T680" id="Seg_12978" s="T679">TURK:core</ta>
            <ta e="T682" id="Seg_12979" s="T681">RUS:disc</ta>
            <ta e="T684" id="Seg_12980" s="T683">TURK:core</ta>
            <ta e="T694" id="Seg_12981" s="T693">TURK:cult</ta>
            <ta e="T695" id="Seg_12982" s="T694">RUS:gram</ta>
            <ta e="T696" id="Seg_12983" s="T695">RUS:gram</ta>
            <ta e="T704" id="Seg_12984" s="T703">RUS:gram</ta>
            <ta e="T706" id="Seg_12985" s="T705">RUS:gram</ta>
            <ta e="T712" id="Seg_12986" s="T711">TURK:cult</ta>
            <ta e="T713" id="Seg_12987" s="T712">RUS:gram</ta>
            <ta e="T717" id="Seg_12988" s="T716">TURK:disc</ta>
            <ta e="T719" id="Seg_12989" s="T718">RUS:gram</ta>
            <ta e="T720" id="Seg_12990" s="T719">RUS:core</ta>
            <ta e="T727" id="Seg_12991" s="T726">TURK:cult</ta>
            <ta e="T731" id="Seg_12992" s="T730">RUS:core</ta>
            <ta e="T734" id="Seg_12993" s="T733">TURK:cult</ta>
            <ta e="T736" id="Seg_12994" s="T735">TURK:cult</ta>
            <ta e="T753" id="Seg_12995" s="T752">TURK:cult</ta>
            <ta e="T755" id="Seg_12996" s="T754">TURK:cult</ta>
            <ta e="T760" id="Seg_12997" s="T759">TURK:cult</ta>
            <ta e="T762" id="Seg_12998" s="T761">TURK:cult</ta>
            <ta e="T764" id="Seg_12999" s="T763">TURK:cult</ta>
            <ta e="T774" id="Seg_13000" s="T773">TURK:cult</ta>
            <ta e="T777" id="Seg_13001" s="T775">TURK:cult</ta>
            <ta e="T788" id="Seg_13002" s="T787">TURK:cult</ta>
            <ta e="T790" id="Seg_13003" s="T789">TURK:cult</ta>
            <ta e="T798" id="Seg_13004" s="T797">RUS:gram</ta>
            <ta e="T808" id="Seg_13005" s="T807">RUS:gram</ta>
            <ta e="T818" id="Seg_13006" s="T817">TURK:disc</ta>
            <ta e="T824" id="Seg_13007" s="T823">TURK:cult</ta>
            <ta e="T826" id="Seg_13008" s="T825">TURK:cult</ta>
            <ta e="T835" id="Seg_13009" s="T834">RUS:gram</ta>
            <ta e="T844" id="Seg_13010" s="T843">RUS:core</ta>
            <ta e="T854" id="Seg_13011" s="T853">RUS:gram</ta>
            <ta e="T857" id="Seg_13012" s="T856">TURK:disc</ta>
            <ta e="T860" id="Seg_13013" s="T858">TURK:disc</ta>
            <ta e="T872" id="Seg_13014" s="T871">TURK:cult</ta>
            <ta e="T873" id="Seg_13015" s="T872">RUS:gram</ta>
            <ta e="T893" id="Seg_13016" s="T892">RUS:gram</ta>
            <ta e="T908" id="Seg_13017" s="T907">RUS:gram</ta>
            <ta e="T922" id="Seg_13018" s="T921">TURK:disc</ta>
            <ta e="T924" id="Seg_13019" s="T923">RUS:cult</ta>
            <ta e="T931" id="Seg_13020" s="T930">RUS:gram</ta>
            <ta e="T936" id="Seg_13021" s="T935">TURK:disc</ta>
            <ta e="T944" id="Seg_13022" s="T943">RUS:cult</ta>
            <ta e="T945" id="Seg_13023" s="T944">RUS:cult</ta>
            <ta e="T954" id="Seg_13024" s="T953">RUS:gram</ta>
            <ta e="T969" id="Seg_13025" s="T968">TURK:cult</ta>
            <ta e="T980" id="Seg_13026" s="T979">RUS:gram</ta>
            <ta e="T991" id="Seg_13027" s="T990">TAT:cult</ta>
            <ta e="T992" id="Seg_13028" s="T991">%TURK:core</ta>
            <ta e="T995" id="Seg_13029" s="T994">%TURK:core</ta>
            <ta e="T996" id="Seg_13030" s="T995">RUS:cult</ta>
            <ta e="T998" id="Seg_13031" s="T997">RUS:cult</ta>
            <ta e="T1000" id="Seg_13032" s="T999">TURK:cult</ta>
            <ta e="T1005" id="Seg_13033" s="T1004">%TURK:core</ta>
            <ta e="T1006" id="Seg_13034" s="T1005">RUS:gram</ta>
            <ta e="T1010" id="Seg_13035" s="T1009">RUS:cult</ta>
            <ta e="T1013" id="Seg_13036" s="T1012">%TURK:core</ta>
            <ta e="T1014" id="Seg_13037" s="T1013">TURK:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-PKZ" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-PKZ" />
         <annotation name="CS" tierref="CS-PKZ">
            <ta e="T20" id="Seg_13038" s="T19">RUS:int</ta>
            <ta e="T58" id="Seg_13039" s="T56">RUS:int</ta>
            <ta e="T124" id="Seg_13040" s="T123">RUS:int</ta>
            <ta e="T143" id="Seg_13041" s="T142">RUS:int</ta>
            <ta e="T162" id="Seg_13042" s="T161">RUS:int</ta>
            <ta e="T184" id="Seg_13043" s="T183">RUS:int</ta>
            <ta e="T227" id="Seg_13044" s="T226">RUS:ext</ta>
            <ta e="T272" id="Seg_13045" s="T271">RUS:int</ta>
            <ta e="T371" id="Seg_13046" s="T369">RUS:int</ta>
            <ta e="T391" id="Seg_13047" s="T390">RUS:int</ta>
            <ta e="T408" id="Seg_13048" s="T407">RUS:int</ta>
            <ta e="T475" id="Seg_13049" s="T474">RUS:int</ta>
            <ta e="T482" id="Seg_13050" s="T480">RUS:int</ta>
            <ta e="T560" id="Seg_13051" s="T559">RUS:int</ta>
            <ta e="T564" id="Seg_13052" s="T563">RUS:int</ta>
            <ta e="T611" id="Seg_13053" s="T606">RUS:int</ta>
            <ta e="T614" id="Seg_13054" s="T613">RUS:int</ta>
            <ta e="T676" id="Seg_13055" s="T675">RUS:ext</ta>
            <ta e="T683" id="Seg_13056" s="T682">RUS:int</ta>
            <ta e="T702" id="Seg_13057" s="T701">RUS:int</ta>
            <ta e="T751" id="Seg_13058" s="T750">RUS:int</ta>
            <ta e="T771" id="Seg_13059" s="T770">RUS:int</ta>
            <ta e="T1013" id="Seg_13060" s="T1010">RUS:calq</ta>
         </annotation>
         <annotation name="fr" tierref="fr-PKZ">
            <ta e="T2" id="Seg_13061" s="T0">Я жила…</ta>
            <ta e="T6" id="Seg_13062" s="T2">У меня было пять мужей.</ta>
            <ta e="T9" id="Seg_13063" s="T6">Потом я шестого взяла.</ta>
            <ta e="T17" id="Seg_13064" s="T9">Этот шестой муж не жил [со мной?], бросил [меня] и ушел.</ta>
            <ta e="T18" id="Seg_13065" s="T17">Потом…</ta>
            <ta e="T26" id="Seg_13066" s="T19">Мой первый муж утонул, [и] мой сын утонул.</ta>
            <ta e="T31" id="Seg_13067" s="T26">Потом мать убили, сестру убили.</ta>
            <ta e="T37" id="Seg_13068" s="T31">Отец в лес пошел и там умер.</ta>
            <ta e="T41" id="Seg_13069" s="T37">Мою сестру ее муж убил.</ta>
            <ta e="T45" id="Seg_13070" s="T41">Двое сыновей осталось.</ta>
            <ta e="T49" id="Seg_13071" s="T45">Я их вырастила.</ta>
            <ta e="T51" id="Seg_13072" s="T49">Одежду [им] покупала.</ta>
            <ta e="T53" id="Seg_13073" s="T51">И учила [их].</ta>
            <ta e="T55" id="Seg_13074" s="T53">(…) бумага</ta>
            <ta e="T62" id="Seg_13075" s="T55">Один был бухгалтер, другой со мной жил.</ta>
            <ta e="T68" id="Seg_13076" s="T62">Потом его взяли на войну, там убили.</ta>
            <ta e="T71" id="Seg_13077" s="T68">У него дочка осталась.</ta>
            <ta e="T82" id="Seg_13078" s="T72">Я замерзла, на печку залезла.</ta>
            <ta e="T85" id="Seg_13079" s="T82">Я сидела, лежала, уснула.</ta>
            <ta e="T88" id="Seg_13080" s="T85">Буду орехи лущить.</ta>
            <ta e="T97" id="Seg_13081" s="T88">Потом вы пришли, я пришла к вам и говорю.</ta>
            <ta e="T106" id="Seg_13082" s="T98">Три брата жили; они пошли рубить дрова.</ta>
            <ta e="T109" id="Seg_13083" s="T106">У них не было огня.</ta>
            <ta e="T113" id="Seg_13084" s="T109">Они нашли (?), огонь горит.</ta>
            <ta e="T114" id="Seg_13085" s="T113">"Иди!</ta>
            <ta e="T116" id="Seg_13086" s="T114">Возьми огонь!"</ta>
            <ta e="T121" id="Seg_13087" s="T116">Он пошел, там сидит человек.</ta>
            <ta e="T127" id="Seg_13088" s="T121">"Расскажи сказку, тогда дам [тебе] огонь".</ta>
            <ta e="T133" id="Seg_13089" s="T127">А тот говорит: "Я не знаю".</ta>
            <ta e="T144" id="Seg_13090" s="T133">Он у него из спины вырезал три ремня.</ta>
            <ta e="T151" id="Seg_13091" s="T144">Он ушел, [тот человек] не дал ему огня.</ta>
            <ta e="T153" id="Seg_13092" s="T151">Потом…</ta>
            <ta e="T157" id="Seg_13093" s="T153">Потом другой [брат] пошел.</ta>
            <ta e="T162" id="Seg_13094" s="T157">И ему говорит [тот человек]: "Расскажи сказку".</ta>
            <ta e="T167" id="Seg_13095" s="T162">Тот говорит: "Я не знаю".</ta>
            <ta e="T173" id="Seg_13096" s="T167">Ну, он ему разрезал спину.</ta>
            <ta e="T178" id="Seg_13097" s="T173">И тот ушел; он не дал ему огня.</ta>
            <ta e="T182" id="Seg_13098" s="T178">Потом еще один пошел.</ta>
            <ta e="T184" id="Seg_13099" s="T182">"Расскажи сказку!"</ta>
            <ta e="T188" id="Seg_13100" s="T184">Ну, тот говорит: "Я расскажу".</ta>
            <ta e="T191" id="Seg_13101" s="T188">Жило десять братьев.</ta>
            <ta e="T195" id="Seg_13102" s="T191">Они пошли рубить лес.</ta>
            <ta e="T202" id="Seg_13103" s="T195">Тогда [один из них] спрыгнул [с] лиственницы и ушел под землю.</ta>
            <ta e="T207" id="Seg_13104" s="T202">Потом пришла утка, устроили гнездо [у него на голове].</ta>
            <ta e="T209" id="Seg_13105" s="T207">Яйца отложила.</ta>
            <ta e="T218" id="Seg_13106" s="T209">Потом из леса пришла большая собака и стала есть яйца.</ta>
            <ta e="T222" id="Seg_13107" s="T218">[Человек] схватил ее за хвост.</ta>
            <ta e="T226" id="Seg_13108" s="T222">Он ее…</ta>
            <ta e="T235" id="Seg_13109" s="T228">Он ее схватил за хвост, хвост оторвался.</ta>
            <ta e="T239" id="Seg_13110" s="T235">Тогда он его ножом разрезал.</ta>
            <ta e="T242" id="Seg_13111" s="T239">Там ящик нашел.</ta>
            <ta e="T245" id="Seg_13112" s="T242">В ящике лежит письмо.</ta>
            <ta e="T256" id="Seg_13113" s="T245">"Мой отец к твоему отцу по нужде пошел".</ta>
            <ta e="T263" id="Seg_13114" s="T257">Тогда этот человек говорит: "Не ври!"</ta>
            <ta e="T278" id="Seg_13115" s="T263">А тот у него из спины ремны вырезал, огонь взял и ушел.</ta>
            <ta e="T288" id="Seg_13116" s="T279">Моя невестка увела моего мужа, и они уехали.</ta>
            <ta e="T297" id="Seg_13117" s="T288">Мы хорошо жили, он меня любил, (обнимал?) меня.</ta>
            <ta e="T304" id="Seg_13118" s="T297">А (потом?) уехал в Игарку и там умер.</ta>
            <ta e="T313" id="Seg_13119" s="T305">А (она?) была некрасивая, у нее рот…</ta>
            <ta e="T321" id="Seg_13120" s="T314">Она некрасивая, рот кривой, глаза кривые.</ta>
            <ta e="T325" id="Seg_13121" s="T322">Жили две женщины.</ta>
            <ta e="T329" id="Seg_13122" s="T325">Они пошли копать саранку.</ta>
            <ta e="T335" id="Seg_13123" s="T329">Одна выкопала много, а другая мало.</ta>
            <ta e="T347" id="Seg_13124" s="T335">[Одна] накопала много, а та, которая накопала много, разрубила ей голову топором.</ta>
            <ta e="T350" id="Seg_13125" s="T347">Потом пришла домой.</ta>
            <ta e="T355" id="Seg_13126" s="T350">Девочка пришла: "Где моя мама?"</ta>
            <ta e="T361" id="Seg_13127" s="T355">"Она ленивая, не копала саранку.</ta>
            <ta e="T364" id="Seg_13128" s="T361">Она там спит".</ta>
            <ta e="T374" id="Seg_13129" s="T364">А девочка как-то узнала и пошла…</ta>
            <ta e="T376" id="Seg_13130" s="T374">Мальчик был.</ta>
            <ta e="T390" id="Seg_13131" s="T376">Она его спрятала, спать положила, а сама залезла на верх юрты.</ta>
            <ta e="T392" id="Seg_13132" s="T390">Она взяла золу.</ta>
            <ta e="T397" id="Seg_13133" s="T392">Потом та женщина пришла.</ta>
            <ta e="T405" id="Seg_13134" s="T397">"Где вы", она говорит, "тут я живу!"</ta>
            <ta e="T411" id="Seg_13135" s="T405">Та золу высыпала ей в глаза.</ta>
            <ta e="T415" id="Seg_13136" s="T412">Они [=глаза] ничего не видят.</ta>
            <ta e="T420" id="Seg_13137" s="T415">Ее глаза ничего не могут [видеть].</ta>
            <ta e="T430" id="Seg_13138" s="T420">Она прыгнула в чум, взяла брата и ушла туда, где много людей.</ta>
            <ta e="T438" id="Seg_13139" s="T431">[Когда] мой муж ушел, я очень плакала.</ta>
            <ta e="T441" id="Seg_13140" s="T438">У меня сердце болело.</ta>
            <ta e="T445" id="Seg_13141" s="T441">И голова…</ta>
            <ta e="T455" id="Seg_13142" s="T446">Сын моей сестры пошел на мельницу, зерно (?).</ta>
            <ta e="T459" id="Seg_13143" s="T455">На мельницу отдал, чтобы смолоть муку.</ta>
            <ta e="T462" id="Seg_13144" s="T459">Потом домой пришел.</ta>
            <ta e="T465" id="Seg_13145" s="T462">Своей жене говорит:</ta>
            <ta e="T474" id="Seg_13146" s="T465">(…)</ta>
            <ta e="T488" id="Seg_13147" s="T474">(…)</ta>
            <ta e="T494" id="Seg_13148" s="T489">Один мужчина пошел свататься.</ta>
            <ta e="T499" id="Seg_13149" s="T494">Девушка красивая, одежды у нее много.</ta>
            <ta e="T506" id="Seg_13150" s="T499">Идет, [другой] человек у него спрашивает: "Куда ты идешь?"</ta>
            <ta e="T507" id="Seg_13151" s="T506">"Свататься".</ta>
            <ta e="T509" id="Seg_13152" s="T507">"Возьми меня!"</ta>
            <ta e="T513" id="Seg_13153" s="T509">"А ты что [будешь] делать?"</ta>
            <ta e="T516" id="Seg_13154" s="T513">"Есть и испражняться".</ta>
            <ta e="T519" id="Seg_13155" s="T516">Они дальше пошли.</ta>
            <ta e="T524" id="Seg_13156" s="T519">Другой человек говорит: "Куда вы идете?"</ta>
            <ta e="T525" id="Seg_13157" s="T524">"Свататься".</ta>
            <ta e="T527" id="Seg_13158" s="T525">"Возьмите меня!"</ta>
            <ta e="T531" id="Seg_13159" s="T527">"А ты что будешь делать?"</ta>
            <ta e="T534" id="Seg_13160" s="T531">"Пить и мочиться".</ta>
            <ta e="T539" id="Seg_13161" s="T534">Они пришли и стали свататься.</ta>
            <ta e="T545" id="Seg_13162" s="T539">Она говорит: "Я много всего буду делать.</ta>
            <ta e="T551" id="Seg_13163" s="T545">Я буду печь хлеб, [жарить] мясо, все съесть надо".</ta>
            <ta e="T557" id="Seg_13164" s="T551">Он говорит: "Ну, я все съем".</ta>
            <ta e="T561" id="Seg_13165" s="T557">"И… пива много.</ta>
            <ta e="T565" id="Seg_13166" s="T561">Я сделаю четырнадцать ведер".</ta>
            <ta e="T574" id="Seg_13167" s="T565">Ну, она стол накрыла, потом пришел этот человек.</ta>
            <ta e="T585" id="Seg_13168" s="T574">Который [мог] есть и испражняться, ест и испражняется, ест и испражняется.</ta>
            <ta e="T595" id="Seg_13169" s="T585">Все съел: мясо и… что было, все.</ta>
            <ta e="T603" id="Seg_13170" s="T595">И говорит: "Дайте еще немножко, дайте еще немножко".</ta>
            <ta e="T611" id="Seg_13171" s="T603">Потом уже есть нечего стало.</ta>
            <ta e="T622" id="Seg_13172" s="T611">Потом пиво пить снова пришел тот человек.</ta>
            <ta e="T632" id="Seg_13173" s="T622">Он пьет, пьет и мочится, пьет и мочится.</ta>
            <ta e="T641" id="Seg_13174" s="T632">Все выпил и кричит: "Еще дайте, мне мало!"</ta>
            <ta e="T647" id="Seg_13175" s="T641">А снаружи воды очень много.</ta>
            <ta e="T655" id="Seg_13176" s="T648">А он не ел и воду не пил.</ta>
            <ta e="T664" id="Seg_13177" s="T655">Ему бы голову отрубили и повесили бы, а то…</ta>
            <ta e="T671" id="Seg_13178" s="T664">Потом он эту девушку взял и пошел домой.</ta>
            <ta e="T675" id="Seg_13179" s="T672">"(?) Что ты поёшь?"</ta>
            <ta e="T676" id="Seg_13180" s="T675">"Коробочку".</ta>
            <ta e="T681" id="Seg_13181" s="T676">"А почему ты плохо поешь?"</ta>
            <ta e="T686" id="Seg_13182" s="T681">"Да нельзя хорошо петь.</ta>
            <ta e="T688" id="Seg_13183" s="T686">Видишь (?)".</ta>
            <ta e="T694" id="Seg_13184" s="T689">У одного человека была мельница.</ta>
            <ta e="T703" id="Seg_13185" s="T694">А у другого человека было очень много денег.</ta>
            <ta e="T707" id="Seg_13186" s="T703">И одежды, и зерна.</ta>
            <ta e="T715" id="Seg_13187" s="T707">У того человека он взял мельницу и увез ее.</ta>
            <ta e="T718" id="Seg_13188" s="T715">Тот плачет.</ta>
            <ta e="T724" id="Seg_13189" s="T718">А петух говорит: "Что ты плачешь, пойдет".</ta>
            <ta e="T727" id="Seg_13190" s="T724">Отберем мельницу".</ta>
            <ta e="T730" id="Seg_13191" s="T727">(…) они пошли.</ta>
            <ta e="T732" id="Seg_13192" s="T730">Петух кричит:</ta>
            <ta e="T736" id="Seg_13193" s="T732">"Отдай мельницу, отдай мельницу!"</ta>
            <ta e="T744" id="Seg_13194" s="T736">Тот человек говорит: "Брось его лошадям".</ta>
            <ta e="T745" id="Seg_13195" s="T744">[Его] бросили.</ta>
            <ta e="T750" id="Seg_13196" s="T745">Он оттуда пришел и опять кричит:</ta>
            <ta e="T755" id="Seg_13197" s="T750">"Отдай мельницу, отдай мельницу!"</ta>
            <ta e="T760" id="Seg_13198" s="T755">Тогда [тот говорит:] "Брось его коровам".</ta>
            <ta e="T762" id="Seg_13199" s="T760">Его бросили коровам.</ta>
            <ta e="T767" id="Seg_13200" s="T762">[Он] от коров опять пришел.</ta>
            <ta e="T777" id="Seg_13201" s="T767">И опять говорит: "Отдай мельницу, отдай мельницу!"</ta>
            <ta e="T779" id="Seg_13202" s="T777">"Бросьте [его] овцам!"</ta>
            <ta e="T781" id="Seg_13203" s="T779">Его бросили овцам.</ta>
            <ta e="T786" id="Seg_13204" s="T781">Потом он опять пришел, опять кричит:</ta>
            <ta e="T790" id="Seg_13205" s="T786">"Отдай мельницу, отдай мельницу!"</ta>
            <ta e="T795" id="Seg_13206" s="T790">Тогда [тот] говорит: "Брось его в воду!"</ta>
            <ta e="T797" id="Seg_13207" s="T795">Его бросили.</ta>
            <ta e="T802" id="Seg_13208" s="T797">А тот говорит: "Мой зад…"</ta>
            <ta e="T816" id="Seg_13209" s="T803">Его бросили в воду, а он кричит: "Мой зад, пей воду, пей воду!"</ta>
            <ta e="T822" id="Seg_13210" s="T816">Его зад всю воду выпил, он опять пришел.</ta>
            <ta e="T826" id="Seg_13211" s="T822">"Дай мельницу, дай мельницу!"</ta>
            <ta e="T838" id="Seg_13212" s="T826">Тогда тот человек говорит: "Разложите большой огонь и бросьте его в огонь".</ta>
            <ta e="T847" id="Seg_13213" s="T838">Тогда разложили костер, бросили этого петуха в костер, он там говорит:</ta>
            <ta e="T853" id="Seg_13214" s="T847">"Мой зад, лей воду, мой зад, лей воду!"</ta>
            <ta e="T856" id="Seg_13215" s="T853">Как вода полилась!</ta>
            <ta e="T860" id="Seg_13216" s="T856">Все…</ta>
            <ta e="T870" id="Seg_13217" s="T861">Воды очень много течет, этот человек говорит:</ta>
            <ta e="T877" id="Seg_13218" s="T870">"Возьми мельницу и иди отсюда!"</ta>
            <ta e="T880" id="Seg_13219" s="T879">Подожди немного!</ta>
            <ta e="T889" id="Seg_13220" s="T880">(…) поставили, я отпустила их.</ta>
            <ta e="T892" id="Seg_13221" s="T889">Они убежали.</ta>
            <ta e="T896" id="Seg_13222" s="T892">А эти не убегают.</ta>
            <ta e="T898" id="Seg_13223" s="T896">Они стали ленивыми.</ta>
            <ta e="T905" id="Seg_13224" s="T899">Вчера моя невестка не уехаа домой.</ta>
            <ta e="T914" id="Seg_13225" s="T905">Я пошла и не поцеловалась с ней, руку ей не дала.</ta>
            <ta e="T918" id="Seg_13226" s="T914">Тогда она обратно вернулась.</ta>
            <ta e="T923" id="Seg_13227" s="T918">Сильный ветер, дождь (идет?).</ta>
            <ta e="T928" id="Seg_13228" s="T923">Машины не ходили, она вернулась.</ta>
            <ta e="T930" id="Seg_13229" s="T928">Здесь переночевала.</ta>
            <ta e="T936" id="Seg_13230" s="T930">А сегодня я встала, сварила [еду].</ta>
            <ta e="T940" id="Seg_13231" s="T936">Хлеб ей испекла на дорогу.</ta>
            <ta e="T944" id="Seg_13232" s="T940">Она поела, потом уехала на машине.</ta>
            <ta e="T945" id="Seg_13233" s="T944">С председателем.</ta>
            <ta e="T949" id="Seg_13234" s="T946">Потом я пойду домой.</ta>
            <ta e="T960" id="Seg_13235" s="T949">Потом я пойду домой, потому что там народ сидит, меня ждет.</ta>
            <ta e="T963" id="Seg_13236" s="T960">Потом я пришла домой.</ta>
            <ta e="T967" id="Seg_13237" s="T963">Помыла (?), напоила теленка.</ta>
            <ta e="T972" id="Seg_13238" s="T967">Потом отогнала корову к реке, напоила ее.</ta>
            <ta e="T979" id="Seg_13239" s="T972">Потом моя дочь пришла ко мне.</ta>
            <ta e="T986" id="Seg_13240" s="T979">Стала меня стричь, я стою, а она меня стрижет.</ta>
            <ta e="T992" id="Seg_13241" s="T986">Потом мы пришли в дом, говорить.</ta>
            <ta e="T996" id="Seg_13242" s="T993">Потом говорить на пленку.</ta>
            <ta e="T999" id="Seg_13243" s="T997">Я накормила кур.</ta>
            <ta e="T1002" id="Seg_13244" s="T999">Разбросала коровий навоз.</ta>
            <ta e="T1010" id="Seg_13245" s="T1003">Хватит говорить, а то дети скоро придут из школы.</ta>
            <ta e="T1015" id="Seg_13246" s="T1010">Не дадут поговорить, будут кричать.</ta>
         </annotation>
         <annotation name="fe" tierref="fe-PKZ">
            <ta e="T2" id="Seg_13247" s="T0">I lived…</ta>
            <ta e="T6" id="Seg_13248" s="T2">I had five husbands.</ta>
            <ta e="T9" id="Seg_13249" s="T6">Then I took a sixth one.</ta>
            <ta e="T17" id="Seg_13250" s="T9">This sixth husband didn't live [with me?], he abandoned [me] and went away.</ta>
            <ta e="T18" id="Seg_13251" s="T17">Then…</ta>
            <ta e="T26" id="Seg_13252" s="T19">My first husband drowned, [and] my son drowned.</ta>
            <ta e="T31" id="Seg_13253" s="T26">Then my mother was killed, my sister was killed.</ta>
            <ta e="T37" id="Seg_13254" s="T31">My father went to the forest and died there.</ta>
            <ta e="T41" id="Seg_13255" s="T37">My sister was killed by her husband.</ta>
            <ta e="T45" id="Seg_13256" s="T41">Two sons left.</ta>
            <ta e="T49" id="Seg_13257" s="T45">I raised them.</ta>
            <ta e="T51" id="Seg_13258" s="T49">I bought [them] clothes.</ta>
            <ta e="T53" id="Seg_13259" s="T51">And taught [them].</ta>
            <ta e="T55" id="Seg_13260" s="T53">(…) paper. [?]</ta>
            <ta e="T62" id="Seg_13261" s="T55">One was book-keeper, another lived with me.</ta>
            <ta e="T68" id="Seg_13262" s="T62">Then he was sent to war, there he was killed.</ta>
            <ta e="T71" id="Seg_13263" s="T68">His daughter has left.</ta>
            <ta e="T82" id="Seg_13264" s="T72">I felt cold and heated the stove.</ta>
            <ta e="T85" id="Seg_13265" s="T82">I was sitting, lying, I fell asleep.</ta>
            <ta e="T88" id="Seg_13266" s="T85">I'll knack nuts.</ta>
            <ta e="T97" id="Seg_13267" s="T88">Then you came, I went to you and am speaking.</ta>
            <ta e="T106" id="Seg_13268" s="T98">There lived three brothers; then they went to chop wood.</ta>
            <ta e="T109" id="Seg_13269" s="T106">They had no fire.</ta>
            <ta e="T113" id="Seg_13270" s="T109">They found (?), fire is burning.</ta>
            <ta e="T114" id="Seg_13271" s="T113">"Go!</ta>
            <ta e="T116" id="Seg_13272" s="T114">Take the fire!"</ta>
            <ta e="T121" id="Seg_13273" s="T116">He went, there sits a man.</ta>
            <ta e="T127" id="Seg_13274" s="T121">"Tell [me] a tale, then I'll give [you] fire."</ta>
            <ta e="T133" id="Seg_13275" s="T127">He says: "I don't know [tales]."</ta>
            <ta e="T144" id="Seg_13276" s="T133">He cut three straps from his back.</ta>
            <ta e="T151" id="Seg_13277" s="T144">He went away, [that man] didn't give him fire.</ta>
            <ta e="T153" id="Seg_13278" s="T151">Then…</ta>
            <ta e="T157" id="Seg_13279" s="T153">Then another [brother] went.</ta>
            <ta e="T162" id="Seg_13280" s="T157">To him [that man] said, too: "Tell a tale".</ta>
            <ta e="T167" id="Seg_13281" s="T162">He says: "I don't know [tales]."</ta>
            <ta e="T173" id="Seg_13282" s="T167">Well, he cut up his back.</ta>
            <ta e="T178" id="Seg_13283" s="T173">And he went away; he didn't give fire.</ta>
            <ta e="T182" id="Seg_13284" s="T178">Then one more person went.</ta>
            <ta e="T184" id="Seg_13285" s="T182">"Tell [me] a tale."</ta>
            <ta e="T188" id="Seg_13286" s="T184">Well he says: "I'll tell."</ta>
            <ta e="T191" id="Seg_13287" s="T188">There lived ten brothers.</ta>
            <ta e="T195" id="Seg_13288" s="T191">Then they came to chop wood.</ta>
            <ta e="T202" id="Seg_13289" s="T195">Then [one of them] jumped [from] the larch and sank into the ground.</ta>
            <ta e="T207" id="Seg_13290" s="T202">Then came a duck, made a nest [on his head].</ta>
            <ta e="T209" id="Seg_13291" s="T207">It laid eggs.</ta>
            <ta e="T218" id="Seg_13292" s="T209">Then came a big dog from the forest and started eating eggs.</ta>
            <ta e="T222" id="Seg_13293" s="T218">The [man] caught it by the tail.</ta>
            <ta e="T226" id="Seg_13294" s="T222">He … it…</ta>
            <ta e="T235" id="Seg_13295" s="T228">Then he caught it by the tail, the tail tore off.</ta>
            <ta e="T239" id="Seg_13296" s="T235">Then he cut [it] with a knife.</ta>
            <ta e="T242" id="Seg_13297" s="T239">There he saw a box.</ta>
            <ta e="T245" id="Seg_13298" s="T242">In the box there is a letter.</ta>
            <ta e="T256" id="Seg_13299" s="T245">"My father went to your father to defecate."</ta>
            <ta e="T263" id="Seg_13300" s="T257">Then this man said: "Don't lie!"</ta>
            <ta e="T278" id="Seg_13301" s="T263">And this person cut the straps on his back and take the fire and went away.</ta>
            <ta e="T288" id="Seg_13302" s="T279">My daughter-in-law/sister-in-law stole my man, and they left.</ta>
            <ta e="T297" id="Seg_13303" s="T288">We lived well, he loved me, (hugged?) me.</ta>
            <ta e="T304" id="Seg_13304" s="T297">And (then?) he went to Igarka and died there.</ta>
            <ta e="T313" id="Seg_13305" s="T305">She(?) wasn't beautiful, her mouth…</ta>
            <ta e="T321" id="Seg_13306" s="T314">She's not beautiful, her mouth is wry, her eyes are wry.</ta>
            <ta e="T325" id="Seg_13307" s="T322">There lived two (women?).</ta>
            <ta e="T329" id="Seg_13308" s="T325">They went to dig out lilium roots.</ta>
            <ta e="T335" id="Seg_13309" s="T329">One dug many [roots], and another dug few.</ta>
            <ta e="T347" id="Seg_13310" s="T335">[One] dug out many, and [the other], who dug few, cut the other's head with an axe.</ta>
            <ta e="T350" id="Seg_13311" s="T347">Then she came home.</ta>
            <ta e="T355" id="Seg_13312" s="T350">A girl came: "Where is my mother?"</ta>
            <ta e="T361" id="Seg_13313" s="T355">"She's lazy, she didn't dig lilium roots.</ta>
            <ta e="T364" id="Seg_13314" s="T361">She's sleeping there."</ta>
            <ta e="T374" id="Seg_13315" s="T364">But the girl somehow got to know, she came…</ta>
            <ta e="T376" id="Seg_13316" s="T374">There was a boy.</ta>
            <ta e="T390" id="Seg_13317" s="T376">She hid (?) him, put him to bed, and herself climbed to the top of the house.</ta>
            <ta e="T392" id="Seg_13318" s="T390">She took ashes.</ta>
            <ta e="T397" id="Seg_13319" s="T392">Then that woman came.</ta>
            <ta e="T405" id="Seg_13320" s="T397">"Where are you", she says, "I live there!"</ta>
            <ta e="T411" id="Seg_13321" s="T405">She poured ashes into her eyes.</ta>
            <ta e="T415" id="Seg_13322" s="T412">They [=her eyes] don't see anything.</ta>
            <ta e="T420" id="Seg_13323" s="T415">Then her eyes cannot [see] anything.</ta>
            <ta e="T430" id="Seg_13324" s="T420">She jumped inside the house, took her brother and went away, where there are many people.</ta>
            <ta e="T438" id="Seg_13325" s="T431">[When] my man left [me], I cried a lot.</ta>
            <ta e="T441" id="Seg_13326" s="T438">My heart ached.</ta>
            <ta e="T445" id="Seg_13327" s="T441">And my head…</ta>
            <ta e="T455" id="Seg_13328" s="T446">My sisters son went to the mill, to (?) corn.</ta>
            <ta e="T459" id="Seg_13329" s="T455">He gave [it] to the mill to grind flour.</ta>
            <ta e="T462" id="Seg_13330" s="T459">Then he came home.</ta>
            <ta e="T465" id="Seg_13331" s="T462">He says to his wife.</ta>
            <ta e="T474" id="Seg_13332" s="T465">(…)</ta>
            <ta e="T488" id="Seg_13333" s="T474">(…)</ta>
            <ta e="T494" id="Seg_13334" s="T489">One man went to marry.</ta>
            <ta e="T499" id="Seg_13335" s="T494">A beautiful girl, she has very many clothes.</ta>
            <ta e="T506" id="Seg_13336" s="T499">He goes, then [another] man says [to him]: "Where are you going?"</ta>
            <ta e="T507" id="Seg_13337" s="T506">"To marry."</ta>
            <ta e="T509" id="Seg_13338" s="T507">"Take me [with you]!"</ta>
            <ta e="T513" id="Seg_13339" s="T509">"What [will] you do?"</ta>
            <ta e="T516" id="Seg_13340" s="T513">"[I will] eat and defecate."</ta>
            <ta e="T519" id="Seg_13341" s="T516">Then they went on.</ta>
            <ta e="T524" id="Seg_13342" s="T519">Another man asks: "Where are you going?"</ta>
            <ta e="T525" id="Seg_13343" s="T524">"To marry."</ta>
            <ta e="T527" id="Seg_13344" s="T525">"Take me!"</ta>
            <ta e="T531" id="Seg_13345" s="T527">"What will you do?"</ta>
            <ta e="T534" id="Seg_13346" s="T531">"I'll drink and urinate."</ta>
            <ta e="T539" id="Seg_13347" s="T534">Then they came and started asking [the girl] for marriage.</ta>
            <ta e="T545" id="Seg_13348" s="T539">She says: "I'll make many things.</ta>
            <ta e="T551" id="Seg_13349" s="T545">I'll bake ipek, [cook] meat, all has to be eaten."</ta>
            <ta e="T557" id="Seg_13350" s="T551">He says: "Well, I'll eat everything."</ta>
            <ta e="T561" id="Seg_13351" s="T557">"And… there is a lot of beer.</ta>
            <ta e="T565" id="Seg_13352" s="T561">I'll make fourteen buckets."</ta>
            <ta e="T574" id="Seg_13353" s="T565">Then they laid the table, then this man came.</ta>
            <ta e="T585" id="Seg_13354" s="T574">That [man] that [could] eat and defecate, eats and defecates, eats and defecates.</ta>
            <ta e="T595" id="Seg_13355" s="T585">He ate everything: meat, and… what there was, everything.</ta>
            <ta e="T603" id="Seg_13356" s="T595">And says: "Give [me] some more, give [me] some more."</ta>
            <ta e="T611" id="Seg_13357" s="T603">Then there was nothing more to eat.</ta>
            <ta e="T622" id="Seg_13358" s="T611">Then this man came again to drink beer.</ta>
            <ta e="T632" id="Seg_13359" s="T622">Then he drinks and drinks and urinates, drinks and urinates.</ta>
            <ta e="T641" id="Seg_13360" s="T632">He drank everything and shouts: "Give [me] more, I want more!"</ta>
            <ta e="T647" id="Seg_13361" s="T641">And on the street there is very much water.</ta>
            <ta e="T655" id="Seg_13362" s="T648">And he didn't eat and didn't drink water.</ta>
            <ta e="T664" id="Seg_13363" s="T655">One should cut off his head and hang him, otherwise…</ta>
            <ta e="T671" id="Seg_13364" s="T664">Then he took the girl and went home.</ta>
            <ta e="T675" id="Seg_13365" s="T672">"(?) What do you sing?"</ta>
            <ta e="T676" id="Seg_13366" s="T675">"Korobeiniki."</ta>
            <ta e="T681" id="Seg_13367" s="T676">"But why don't you sing well?"</ta>
            <ta e="T686" id="Seg_13368" s="T681">"It's not allowed to sing well.</ta>
            <ta e="T688" id="Seg_13369" s="T686">Do you see (?)."</ta>
            <ta e="T694" id="Seg_13370" s="T689">One person had a mill.</ta>
            <ta e="T703" id="Seg_13371" s="T694">And another man had very much money.</ta>
            <ta e="T707" id="Seg_13372" s="T703">And [many] clothes, [much] grain.</ta>
            <ta e="T715" id="Seg_13373" s="T707">He took that man's mill and carried it away.</ta>
            <ta e="T718" id="Seg_13374" s="T715">He's crying.</ta>
            <ta e="T724" id="Seg_13375" s="T718">But a rooster says: "Why are you crying, let's go."</ta>
            <ta e="T727" id="Seg_13376" s="T724">We'll take the mill [back]."</ta>
            <ta e="T730" id="Seg_13377" s="T727">(…) they went.</ta>
            <ta e="T732" id="Seg_13378" s="T730">The rooster shouts:</ta>
            <ta e="T736" id="Seg_13379" s="T732">"Give the mill [back], give the mill!"</ta>
            <ta e="T744" id="Seg_13380" s="T736">This man says: "Throw it to the horses."</ta>
            <ta e="T745" id="Seg_13381" s="T744">They threw [it].</ta>
            <ta e="T750" id="Seg_13382" s="T745">It came from there, shouts again:</ta>
            <ta e="T755" id="Seg_13383" s="T750">"Give the mill back!"</ta>
            <ta e="T760" id="Seg_13384" s="T755">Then [he says]: "Throw it to the cow[s]."</ta>
            <ta e="T762" id="Seg_13385" s="T760">[They] threw it to the cow[s].</ta>
            <ta e="T767" id="Seg_13386" s="T762">[From] the cow[s] he came, too.</ta>
            <ta e="T777" id="Seg_13387" s="T767">Then he says again: "Give the mill [back], give the mill [back]!"</ta>
            <ta e="T779" id="Seg_13388" s="T777">"Throw [it] to the sheep!"</ta>
            <ta e="T781" id="Seg_13389" s="T779">[They] threw [it] to the sheep.</ta>
            <ta e="T786" id="Seg_13390" s="T781">Then it came again, shouts again:</ta>
            <ta e="T790" id="Seg_13391" s="T786">"Give the mill, give the mill!"</ta>
            <ta e="T795" id="Seg_13392" s="T790">Then he says: "Throw it in the water!"</ta>
            <ta e="T797" id="Seg_13393" s="T795">[They] threw it.</ta>
            <ta e="T802" id="Seg_13394" s="T797">And it says: "My ass…"</ta>
            <ta e="T816" id="Seg_13395" s="T803">[They] threw it in the water, and it shouts: "My ass, drink water, drink water!"</ta>
            <ta e="T822" id="Seg_13396" s="T816">Its ass drank all the water, it [= the rooster] came again.</ta>
            <ta e="T826" id="Seg_13397" s="T822">"Give the mill, give the mill!"</ta>
            <ta e="T838" id="Seg_13398" s="T826">Then this man says: "Make a big fire and throw it into the fire."</ta>
            <ta e="T847" id="Seg_13399" s="T838">Then they made fire, threw this rooster into the fire, it says from there:</ta>
            <ta e="T853" id="Seg_13400" s="T847">"My ass, pour the water, my ass, pour the water!"</ta>
            <ta e="T856" id="Seg_13401" s="T853">The water flew!</ta>
            <ta e="T860" id="Seg_13402" s="T856">Everything…</ta>
            <ta e="T870" id="Seg_13403" s="T861">Then a lot of water flew, this man said:</ta>
            <ta e="T877" id="Seg_13404" s="T870">"Take the mill and go there!"</ta>
            <ta e="T880" id="Seg_13405" s="T879">Wait a bit!</ta>
            <ta e="T889" id="Seg_13406" s="T880">(…) put, I (released?) them.</ta>
            <ta e="T892" id="Seg_13407" s="T889">Then they ran away.</ta>
            <ta e="T896" id="Seg_13408" s="T892">And these don't run.</ta>
            <ta e="T898" id="Seg_13409" s="T896">They became lazy.</ta>
            <ta e="T905" id="Seg_13410" s="T899">Yesterday my daughter-in-law didn't go home.</ta>
            <ta e="T914" id="Seg_13411" s="T905">I went and didn't kiss her, didn't give her a hand.</ta>
            <ta e="T918" id="Seg_13412" s="T914">Then she came back.</ta>
            <ta e="T923" id="Seg_13413" s="T918">There is a strong wind and snow.</ta>
            <ta e="T928" id="Seg_13414" s="T923">The cars didn't go, [and] she came back.</ta>
            <ta e="T930" id="Seg_13415" s="T928">She spent the night here.</ta>
            <ta e="T936" id="Seg_13416" s="T930">Today I got up, I cooked.</ta>
            <ta e="T940" id="Seg_13417" s="T936">I baked her bread for the road.</ta>
            <ta e="T944" id="Seg_13418" s="T940">She ate, then she left with a car.</ta>
            <ta e="T945" id="Seg_13419" s="T944">With the [collective farm] chairman. </ta>
            <ta e="T949" id="Seg_13420" s="T946">Then I'll go home.</ta>
            <ta e="T960" id="Seg_13421" s="T949">Then I'll go home, because people are sitting there and waiting for me.</ta>
            <ta e="T963" id="Seg_13422" s="T960">Then I came home.</ta>
            <ta e="T967" id="Seg_13423" s="T963">I washed (?), I gave water to the calf.</ta>
            <ta e="T972" id="Seg_13424" s="T967">Then I drove the cow to the river, made it drink.</ta>
            <ta e="T979" id="Seg_13425" s="T972">Then my daughter comes to me.</ta>
            <ta e="T986" id="Seg_13426" s="T979">She started cutting [my] hair, I stood and she was cutting my hair.</ta>
            <ta e="T992" id="Seg_13427" s="T986">Then we came home, to speak.</ta>
            <ta e="T996" id="Seg_13428" s="T993">Then to speak on a tape.</ta>
            <ta e="T999" id="Seg_13429" s="T997">I feed hens.</ta>
            <ta e="T1002" id="Seg_13430" s="T999">I spread the cow's manure.</ta>
            <ta e="T1010" id="Seg_13431" s="T1003">That's enough speaking, the children will come soon from the school.</ta>
            <ta e="T1015" id="Seg_13432" s="T1010">They won't let [us] speak, they'll shout.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-PKZ">
            <ta e="T2" id="Seg_13433" s="T0">Ich lebte…</ta>
            <ta e="T6" id="Seg_13434" s="T2">Ich hatte fünf Männer.</ta>
            <ta e="T9" id="Seg_13435" s="T6">Dann nahm ich einen sechsten.</ta>
            <ta e="T17" id="Seg_13436" s="T9">Dieser sechste Mann lebte nicht [mit mir?], er verließ [mich] und ging weg.</ta>
            <ta e="T18" id="Seg_13437" s="T17">Dann…</ta>
            <ta e="T26" id="Seg_13438" s="T19">Mein erster Mann ertrank, [und] mein Sohn ertrank.</ta>
            <ta e="T31" id="Seg_13439" s="T26">Dann wurde meine Mutter umgebracht, meine Schwester wurde umgebracht.</ta>
            <ta e="T37" id="Seg_13440" s="T31">Mein Vater ging in den Wald und starb dort.</ta>
            <ta e="T41" id="Seg_13441" s="T37">Meine Schwester wurde von ihrem Mann umgebracht.</ta>
            <ta e="T45" id="Seg_13442" s="T41">Zwei Söhne sind übrig geblieben.</ta>
            <ta e="T49" id="Seg_13443" s="T45">Ich zog sie auf.</ta>
            <ta e="T51" id="Seg_13444" s="T49">Ich kaufte [ihnen] Kleidung.</ta>
            <ta e="T53" id="Seg_13445" s="T51">Und lehrte [sie].</ta>
            <ta e="T55" id="Seg_13446" s="T53">(…) Papier. [?]</ta>
            <ta e="T62" id="Seg_13447" s="T55">Einer war Buchhalter, der andere lebte bei mir.</ta>
            <ta e="T68" id="Seg_13448" s="T62">Dann wurde er in den Krieg geschickt, dort wurde er getötet.</ta>
            <ta e="T71" id="Seg_13449" s="T68">Seine Tochter ist weggegangen.</ta>
            <ta e="T82" id="Seg_13450" s="T72">Mir war kalt und ich heizte den Ofen.</ta>
            <ta e="T85" id="Seg_13451" s="T82">Ich saß, lag, ich schlief ein.</ta>
            <ta e="T88" id="Seg_13452" s="T85">Ich werde Nüsse knacken.</ta>
            <ta e="T97" id="Seg_13453" s="T88">Dann kamst du, ich ging zu dir und spreche.</ta>
            <ta e="T106" id="Seg_13454" s="T98">Es lebten drei Brüder; dann gingen sie, um Holz zu hacken.</ta>
            <ta e="T109" id="Seg_13455" s="T106">Sie hatten kein Feuer.</ta>
            <ta e="T113" id="Seg_13456" s="T109">Sie finden (?), ein Feuer brennt.</ta>
            <ta e="T114" id="Seg_13457" s="T113">"Geh!</ta>
            <ta e="T116" id="Seg_13458" s="T114">Nimm das Feuer!"</ta>
            <ta e="T121" id="Seg_13459" s="T116">Er ging, dort sitzt ein Mann.</ta>
            <ta e="T127" id="Seg_13460" s="T121">"Erzählt [mir] ein Märchen, dann gebe ich [dir] Feuer."</ta>
            <ta e="T133" id="Seg_13461" s="T127">Er sagt: "Ich kenne keine [Märchen]."</ta>
            <ta e="T144" id="Seg_13462" s="T133">Er schnitt drei Riemen von seinem Rücken.</ta>
            <ta e="T151" id="Seg_13463" s="T144">Er ging weg, [jener Mann] gab ihm kein Feuer.</ta>
            <ta e="T153" id="Seg_13464" s="T151">Dann…</ta>
            <ta e="T157" id="Seg_13465" s="T153">Dann ging ein anderer [Bruder].</ta>
            <ta e="T162" id="Seg_13466" s="T157">Zu diesem sagte [der Mann] auch: "Erzähl ein Märchen."</ta>
            <ta e="T167" id="Seg_13467" s="T162">Er sagt: "Ich kenne keine [Märchen]."</ta>
            <ta e="T173" id="Seg_13468" s="T167">Gut, er schnitt seinen Rücken auf.</ta>
            <ta e="T178" id="Seg_13469" s="T173">Und er ging weg; er gab kein Feuer.</ta>
            <ta e="T182" id="Seg_13470" s="T178">Denn ging noch jemand.</ta>
            <ta e="T184" id="Seg_13471" s="T182">"Erzähl [mir] ein Märchen."</ta>
            <ta e="T188" id="Seg_13472" s="T184">Nun, er sagt: "Ich erzähle."</ta>
            <ta e="T191" id="Seg_13473" s="T188">Es lebten zehn Brüder.</ta>
            <ta e="T195" id="Seg_13474" s="T191">Dann kamen sie, um Holz zu hacken.</ta>
            <ta e="T202" id="Seg_13475" s="T195">Dann sprang [einer von ihnen] [von] der Lärche und versank im Boden.</ta>
            <ta e="T207" id="Seg_13476" s="T202">Dann kam eine Ente, machte ein Nest [auf seinem Kopf].</ta>
            <ta e="T209" id="Seg_13477" s="T207">Sie legte Eier.</ta>
            <ta e="T218" id="Seg_13478" s="T209">Dann kam ein großer Hund aus dem Wald und fing an Eier zu fressen.</ta>
            <ta e="T222" id="Seg_13479" s="T218">Der [Mann] fing ihn am Schwanz.</ta>
            <ta e="T226" id="Seg_13480" s="T222">Er… es…</ta>
            <ta e="T235" id="Seg_13481" s="T228">Dann fing er ihn am Schwanz, der Schwanz riss ab.</ta>
            <ta e="T239" id="Seg_13482" s="T235">Dann schnitt er [es] mit einem Messer.</ta>
            <ta e="T242" id="Seg_13483" s="T239">Dann sah er eine Schachtel.</ta>
            <ta e="T245" id="Seg_13484" s="T242">In der Schachtel ist ein Brief.</ta>
            <ta e="T256" id="Seg_13485" s="T245">"Mein Vater ging zu deinem Vater um zu koten."</ta>
            <ta e="T263" id="Seg_13486" s="T257">Dann sagte dieser Mann: "Lüg nicht!"</ta>
            <ta e="T278" id="Seg_13487" s="T263">Und diese Person schnitt die Riemen auf seinem Rücken und nahm das Feuer und ging weg.</ta>
            <ta e="T288" id="Seg_13488" s="T279">Meine Schwiegertochter/Schwägerin stahl meinen Mann und sie gingen weg.</ta>
            <ta e="T297" id="Seg_13489" s="T288">Wir lebten gut, er liebte mich, (umarmte?) mich.</ta>
            <ta e="T304" id="Seg_13490" s="T297">Und (dann?) ging er nach Igarka und starb dort.</ta>
            <ta e="T313" id="Seg_13491" s="T305">Sie(?) war nicht schön, ihr Mund…</ta>
            <ta e="T321" id="Seg_13492" s="T314">Sie ist nicht schön, ihr Mund ist schief, ihre Augen sind schief.</ta>
            <ta e="T325" id="Seg_13493" s="T322">Es lebten zwei (Frauen?).</ta>
            <ta e="T329" id="Seg_13494" s="T325">Sie gingen, um Lilienwurzeln auszugraben.</ta>
            <ta e="T335" id="Seg_13495" s="T329">Eine grub viele [Wurzeln] aus und eine grub wenige aus.</ta>
            <ta e="T347" id="Seg_13496" s="T335">[Eine] grub viele aus, und [die andere], die wenig ausgrub, schnitt der anderen mit einer Axt den Kopf ab.</ta>
            <ta e="T350" id="Seg_13497" s="T347">Dann kam sie nach Hause.</ta>
            <ta e="T355" id="Seg_13498" s="T350">Ein Mädchen kam: "Wo ist meine Mutter?"</ta>
            <ta e="T361" id="Seg_13499" s="T355">"Sie ist faul, sie hat keine Lilienwurzeln ausgegraben.</ta>
            <ta e="T364" id="Seg_13500" s="T361">Sie schläft dort."</ta>
            <ta e="T374" id="Seg_13501" s="T364">Aber das Mädchen erfuhr irgendwie, sie kam…</ta>
            <ta e="T376" id="Seg_13502" s="T374">Dort wart ein Junge.</ta>
            <ta e="T390" id="Seg_13503" s="T376">Sie versteckte (?) ihn, brachte ihn ins Bett und kletterte selbst auf das Haus.</ta>
            <ta e="T392" id="Seg_13504" s="T390">Sie nahm Asche.</ta>
            <ta e="T397" id="Seg_13505" s="T392">Dann kam jene Frau.</ta>
            <ta e="T405" id="Seg_13506" s="T397">"Wo seid ihr", sagt sie, "ich wohne dort!"</ta>
            <ta e="T411" id="Seg_13507" s="T405">Sie streute Asche in ihre Augen.</ta>
            <ta e="T415" id="Seg_13508" s="T412">Sie [=ihre Augen] sehen nichts.</ta>
            <ta e="T420" id="Seg_13509" s="T415">Dann können ihre Augen nichts [sehen].</ta>
            <ta e="T430" id="Seg_13510" s="T420">Sie sprang ins Haus hinein, nahm ihren Bruder und ging weg, wo viele Leute sind.</ta>
            <ta e="T438" id="Seg_13511" s="T431">[Als] mein Mann [mich] verließ, weinte ich viel.</ta>
            <ta e="T441" id="Seg_13512" s="T438">Mein Herz tat weh.</ta>
            <ta e="T445" id="Seg_13513" s="T441">Und mein Kopf…</ta>
            <ta e="T455" id="Seg_13514" s="T446">Der Sohn meiner Schwester ging zur Mühle, um Korn (?).</ta>
            <ta e="T459" id="Seg_13515" s="T455">Er gab [es] zur Mühle, um Korn zu mahlen.</ta>
            <ta e="T462" id="Seg_13516" s="T459">Dann kam er nach Hause.</ta>
            <ta e="T465" id="Seg_13517" s="T462">Er sagt zu seiner Frau:</ta>
            <ta e="T474" id="Seg_13518" s="T465">(…)</ta>
            <ta e="T488" id="Seg_13519" s="T474">(…)</ta>
            <ta e="T494" id="Seg_13520" s="T489">Ein Mann ging um zu heiraten.</ta>
            <ta e="T499" id="Seg_13521" s="T494">Ein schönes Mädchen, sie hat sehr viele Kleider.</ta>
            <ta e="T506" id="Seg_13522" s="T499">Er geht, dann sagt ein [anderer] Mann [zu ihm]: "Wo gehst du hin?"</ta>
            <ta e="T507" id="Seg_13523" s="T506">"Um zu heiraten."</ta>
            <ta e="T509" id="Seg_13524" s="T507">"Nimm mich [mit]!"</ta>
            <ta e="T513" id="Seg_13525" s="T509">"Was [wirst] du machen?"</ta>
            <ta e="T516" id="Seg_13526" s="T513">"[Ich werde] essen und koten."</ta>
            <ta e="T519" id="Seg_13527" s="T516">Dann gingen sie weiter.</ta>
            <ta e="T524" id="Seg_13528" s="T519">Ein anderer Mann fragt: "Wo gehst du hin?"</ta>
            <ta e="T525" id="Seg_13529" s="T524">"Um zu heiraten."</ta>
            <ta e="T527" id="Seg_13530" s="T525">"Nimm mich mit!"</ta>
            <ta e="T531" id="Seg_13531" s="T527">"Was wirst du machen?"</ta>
            <ta e="T534" id="Seg_13532" s="T531">"Ich werde trinken und urinieren."</ta>
            <ta e="T539" id="Seg_13533" s="T534">Dann kamen sie und fragten [das Mädchen] um ihre Hand.</ta>
            <ta e="T545" id="Seg_13534" s="T539">Sie sagt: "Ich mache viele Sachen.</ta>
            <ta e="T551" id="Seg_13535" s="T545">Ich backe Ipek, [koche] Fleisch, alles muss gegessen werden."</ta>
            <ta e="T557" id="Seg_13536" s="T551">Er sagt: "Also, ich esse alles."</ta>
            <ta e="T561" id="Seg_13537" s="T557">"Und… dort ist viel Bier.</ta>
            <ta e="T565" id="Seg_13538" s="T561">Ich mache vierzehn Eimer."</ta>
            <ta e="T574" id="Seg_13539" s="T565">Dann deckten sie die Tisch, dann kam dieser Mann.</ta>
            <ta e="T585" id="Seg_13540" s="T574">Dieser [Mann], der essen und koten [konnte], isst und kotet, isst und kotet.</ta>
            <ta e="T595" id="Seg_13541" s="T585">Er aß alles: Fleisch und… was dort war, alles.</ta>
            <ta e="T603" id="Seg_13542" s="T595">Und sagt: "Gib [mir] mehr, gib [mir] mehr."</ta>
            <ta e="T611" id="Seg_13543" s="T603">Dann gab es nichts mehr zu essen.</ta>
            <ta e="T622" id="Seg_13544" s="T611">Dann kam dieser Mann wieder, um Bier zu trinken.</ta>
            <ta e="T632" id="Seg_13545" s="T622">Dann trinkt er und trinkt und uriniert, trinkt und uriniert.</ta>
            <ta e="T641" id="Seg_13546" s="T632">Er trank alles und schreit: "Gib [mir] mehr, ich will mehr!"</ta>
            <ta e="T647" id="Seg_13547" s="T641">Und auf der Straße gibt es viel Wasser.</ta>
            <ta e="T655" id="Seg_13548" s="T648">Und er aß nicht und trank kein Wasser.</ta>
            <ta e="T664" id="Seg_13549" s="T655">Man sollte seinen Kopf abschneiden und ihn aufhängen, sonst…</ta>
            <ta e="T671" id="Seg_13550" s="T664">Dann nahm er das Mädchen und ging nach Hause.</ta>
            <ta e="T675" id="Seg_13551" s="T672">"(?) Was singst du?"</ta>
            <ta e="T676" id="Seg_13552" s="T675">"Korobejniki."</ta>
            <ta e="T681" id="Seg_13553" s="T676">"Aber warum singst du nicht gut?"</ta>
            <ta e="T686" id="Seg_13554" s="T681">"Man darf nicht gut singen.</ta>
            <ta e="T688" id="Seg_13555" s="T686">Siehst du (?)."</ta>
            <ta e="T694" id="Seg_13556" s="T689">Ein Mensch hatte eine Mühle.</ta>
            <ta e="T703" id="Seg_13557" s="T694">Und ein anderen Mann hatte viel Geld.</ta>
            <ta e="T707" id="Seg_13558" s="T703">Und [viele] Kleider, [viel] Korn.</ta>
            <ta e="T715" id="Seg_13559" s="T707">Er nahm die Mühle des Mannes und trug sie weg.</ta>
            <ta e="T718" id="Seg_13560" s="T715">Er weint.</ta>
            <ta e="T724" id="Seg_13561" s="T718">Aber ein Hahn sagt: "Warum weinst du, lass uns gehen.</ta>
            <ta e="T727" id="Seg_13562" s="T724">Wir holen die Mühle [zurück]."</ta>
            <ta e="T730" id="Seg_13563" s="T727">(…) sie gingen.</ta>
            <ta e="T732" id="Seg_13564" s="T730">Der Hahn schreit:</ta>
            <ta e="T736" id="Seg_13565" s="T732">"Gib die Mühle [zurück], gib die Mühle!"</ta>
            <ta e="T744" id="Seg_13566" s="T736">Dieser Mann sagt: "Wirf ihn zu den Pferden."</ta>
            <ta e="T745" id="Seg_13567" s="T744">Sie warfen [ihn].</ta>
            <ta e="T750" id="Seg_13568" s="T745">Er kam von dort und schreit wieder:</ta>
            <ta e="T755" id="Seg_13569" s="T750">"Gib die Mühle zurück!"</ta>
            <ta e="T760" id="Seg_13570" s="T755">Dann [sagt er]: "Wirf ihn zur Kuh."</ta>
            <ta e="T762" id="Seg_13571" s="T760">[Sie] warfen ihn zur Kuh.</ta>
            <ta e="T767" id="Seg_13572" s="T762">[Von] der Kuh kam er auch.</ta>
            <ta e="T777" id="Seg_13573" s="T767">Dann sagt er wieder: "Gib die Mühle [zurück], gib die Mühle [zurück]!"</ta>
            <ta e="T779" id="Seg_13574" s="T777">"Wirf [ihn] zu den Schafen!"</ta>
            <ta e="T781" id="Seg_13575" s="T779">[Sie] warfen [ihn] zu den Schafen.</ta>
            <ta e="T786" id="Seg_13576" s="T781">Dann kam er wieder, er schreit wieder:</ta>
            <ta e="T790" id="Seg_13577" s="T786">"Gib die Mühle, gib die Mühle!"</ta>
            <ta e="T795" id="Seg_13578" s="T790">Dann sagt er: "Wirf ihn ins Wasser!"</ta>
            <ta e="T797" id="Seg_13579" s="T795">[Sie] warfen ihn.</ta>
            <ta e="T802" id="Seg_13580" s="T797">Und er sagt: "Mein Arsch…"</ta>
            <ta e="T816" id="Seg_13581" s="T803">[Sie] warfen ihn ins Wasser und er schreit: "Mein Arsch, trink Wasser, trink Wasser!"</ta>
            <ta e="T822" id="Seg_13582" s="T816">Sein Arsch trank alles Wasser, er [= der Hahn] kam wieder.</ta>
            <ta e="T826" id="Seg_13583" s="T822">"Gib die Mühle, gib die Mühle!"</ta>
            <ta e="T838" id="Seg_13584" s="T826">Dann sagt dieser Mann: "Macht ein großes Feuer und werft ihn ins Feuer."</ta>
            <ta e="T847" id="Seg_13585" s="T838">Dann machten sie Feuer, warfen den Hahn ins Feuer, er sagt von dort:</ta>
            <ta e="T853" id="Seg_13586" s="T847">"Mein Arsch, gieß das Wasser aus, mein Arsch, gieß das Wasser aus!"</ta>
            <ta e="T856" id="Seg_13587" s="T853">Das Wasser floss!</ta>
            <ta e="T860" id="Seg_13588" s="T856">Alles…</ta>
            <ta e="T870" id="Seg_13589" s="T861">Dann floss eine Menge Wasser, dieser Mann sagte:</ta>
            <ta e="T877" id="Seg_13590" s="T870">"Nimm die Mühle und gehe dorthin!"</ta>
            <ta e="T880" id="Seg_13591" s="T879">Warte ein Bisschen!</ta>
            <ta e="T889" id="Seg_13592" s="T880">(…) stellte, ich (entliess?) sie.</ta>
            <ta e="T892" id="Seg_13593" s="T889">Sie liefen davon.</ta>
            <ta e="T896" id="Seg_13594" s="T892">Und diese laufen nicht.</ta>
            <ta e="T898" id="Seg_13595" s="T896">Sie sind faul geworden.</ta>
            <ta e="T905" id="Seg_13596" s="T899">Gestern ging meine Schwiegertochter nicht nach Hause.</ta>
            <ta e="T914" id="Seg_13597" s="T905">Ich ging und gab ihr keinen Kuss, gab ihr nicht die Hand.</ta>
            <ta e="T918" id="Seg_13598" s="T914">Dann kam sie zurück.</ta>
            <ta e="T923" id="Seg_13599" s="T918">Dort ist starker Wind und Schnee.</ta>
            <ta e="T928" id="Seg_13600" s="T923">Die Autos fahren nicht, [und] sie kam zurück.</ta>
            <ta e="T930" id="Seg_13601" s="T928">Sie verbrachte die Nacht hier.</ta>
            <ta e="T936" id="Seg_13602" s="T930">Heute stand ich auf, ich kochte.</ta>
            <ta e="T940" id="Seg_13603" s="T936">Ich buk ihr Brot für den Weg.</ta>
            <ta e="T944" id="Seg_13604" s="T940">Sie aß, dann fuhr sie mit einem Auto weg.</ta>
            <ta e="T945" id="Seg_13605" s="T944">Mit dem Vorsitzenden [der Kolchose].</ta>
            <ta e="T949" id="Seg_13606" s="T946">Dann gehe ich nach Hause.</ta>
            <ta e="T960" id="Seg_13607" s="T949">Dann gehe nach Hause, weil dort Leute sitzen und auf mich warten.</ta>
            <ta e="T963" id="Seg_13608" s="T960">Dann kam ich nach Hause.</ta>
            <ta e="T967" id="Seg_13609" s="T963">Ich wusch (?), ich gab dem Kalb Wasser.</ta>
            <ta e="T972" id="Seg_13610" s="T967">Dann jagte ich die Kuh zum Fluss, tränkte sie.</ta>
            <ta e="T979" id="Seg_13611" s="T972">Dann kommt meine Tochter zu mir.</ta>
            <ta e="T986" id="Seg_13612" s="T979">Sie fing an, [meine] Haare zu schneiden, ich stand und sie schnitt mir die Haare.</ta>
            <ta e="T992" id="Seg_13613" s="T986">Dann kamen wir nach Hause, um zu sprechen.</ta>
            <ta e="T996" id="Seg_13614" s="T993">Um dann auf ein Band zu sprechen.</ta>
            <ta e="T999" id="Seg_13615" s="T997">Ich fütterte die Hühner.</ta>
            <ta e="T1002" id="Seg_13616" s="T999">Ich verteile den Kuhmist.</ta>
            <ta e="T1010" id="Seg_13617" s="T1003">Das ist genug gesprochen, die Kinder kommen bald aus der Schule.</ta>
            <ta e="T1015" id="Seg_13618" s="T1010">Sie lassen uns nicht sprechen, sie werden schreien.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-PKZ">
            <ta e="T106" id="Seg_13619" s="T98">[GVY:] A Russian tale, see e.g. https://ru.wikisource.org/wiki/Народные_русские_сказки_(Афанасьев)/Не_любо_—_не_слушай</ta>
            <ta e="T113" id="Seg_13620" s="T109">[GVY:] judging by the original text, one may expect here a word for 'beehouse'.</ta>
            <ta e="T202" id="Seg_13621" s="T195">[GVY:] šojmu = šumi 'larch'?</ta>
            <ta e="T235" id="Seg_13622" s="T228">[GVY:] dʼabəʔluʔpi</ta>
            <ta e="T239" id="Seg_13623" s="T235">[GVY:] bĭʔpi?</ta>
            <ta e="T415" id="Seg_13624" s="T412">[GVY:] măndəliai? Cf. the next sentence.</ta>
            <ta e="T420" id="Seg_13625" s="T415">[GVY:] moliai? Cf. the preceding sentence.</ta>
            <ta e="T474" id="Seg_13626" s="T465">[GVY:] The meaning of this and the following sentence is unclear.</ta>
            <ta e="T565" id="Seg_13627" s="T561">[GVY:] or (rather) forty.</ta>
            <ta e="T655" id="Seg_13628" s="T648">[GVY:] that man who wanted to marry?</ta>
            <ta e="T676" id="Seg_13629" s="T675">[GVY:] A Russian folk song.</ta>
            <ta e="T686" id="Seg_13630" s="T681">[GVY:] Maybe jakše means 'loud' here?</ta>
            <ta e="T694" id="Seg_13631" s="T689">[GVY:] A tale with several variants, see its expose e.g. http://kinderbox.ru/skazka-petuh-i-zhernovki/</ta>
            <ta e="T727" id="Seg_13632" s="T724">[GVY:] ilbəj = ibəj</ta>
            <ta e="T730" id="Seg_13633" s="T727">[GVY:] dĭgəttə?</ta>
            <ta e="T860" id="Seg_13634" s="T856">[GVY:] The sentence is unfinished.</ta>
            <ta e="T889" id="Seg_13635" s="T880">[GVY:] Unclear. [AAV:] Xisəʔi = časəʔi 'clock'?</ta>
            <ta e="T892" id="Seg_13636" s="T889">[GVY:] nuʔməluʔbəʔjə</ta>
            <ta e="T944" id="Seg_13637" s="T940">[GVY:] Or mašinaʔizi? NB that kalladʼürbi here is pronounced with a pause.</ta>
            <ta e="T996" id="Seg_13638" s="T993">[GVY:] The syntax is unclear. [AAV] From here on, the speed is irregular.</ta>
         </annotation>
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-KA"
                      id="tx-KA"
                      speaker="KA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KA">
            <ts e="T773" id="Seg_13639" n="sc" s="T772">
               <ts e="T773" id="Seg_13641" n="HIAT:u" s="T772">
                  <ts e="T773" id="Seg_13643" n="HIAT:w" s="T772">Deʔtə</ts>
                  <nts id="Seg_13644" n="HIAT:ip">.</nts>
                  <nts id="Seg_13645" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KA">
            <ts e="T773" id="Seg_13646" n="sc" s="T772">
               <ts e="T773" id="Seg_13648" n="e" s="T772">Deʔtə. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KA">
            <ta e="T773" id="Seg_13649" s="T772">PKZ_196X_SU0217.KA.001 (157)</ta>
         </annotation>
         <annotation name="ts" tierref="ts-KA">
            <ta e="T773" id="Seg_13650" s="T772">Deʔtə. </ta>
         </annotation>
         <annotation name="CS" tierref="CS-KA" />
         <annotation name="fr" tierref="fr-KA">
            <ta e="T773" id="Seg_13651" s="T772">Отдай!</ta>
         </annotation>
         <annotation name="fe" tierref="fe-KA">
            <ta e="T773" id="Seg_13652" s="T772">Give!</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KA">
            <ta e="T773" id="Seg_13653" s="T772">Gib!</ta>
         </annotation>
         <annotation name="nt" tierref="nt-KA" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T776" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T1017" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T1018" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T1019" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T1020" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T1021" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T1022" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T1023" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T1024" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T1025" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T927" />
            <conversion-tli id="T928" />
            <conversion-tli id="T929" />
            <conversion-tli id="T930" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T935" />
            <conversion-tli id="T936" />
            <conversion-tli id="T937" />
            <conversion-tli id="T938" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T941" />
            <conversion-tli id="T942" />
            <conversion-tli id="T1026" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
            <conversion-tli id="T949" />
            <conversion-tli id="T950" />
            <conversion-tli id="T951" />
            <conversion-tli id="T952" />
            <conversion-tli id="T953" />
            <conversion-tli id="T954" />
            <conversion-tli id="T955" />
            <conversion-tli id="T956" />
            <conversion-tli id="T957" />
            <conversion-tli id="T958" />
            <conversion-tli id="T959" />
            <conversion-tli id="T960" />
            <conversion-tli id="T961" />
            <conversion-tli id="T962" />
            <conversion-tli id="T963" />
            <conversion-tli id="T964" />
            <conversion-tli id="T965" />
            <conversion-tli id="T966" />
            <conversion-tli id="T967" />
            <conversion-tli id="T968" />
            <conversion-tli id="T969" />
            <conversion-tli id="T970" />
            <conversion-tli id="T971" />
            <conversion-tli id="T972" />
            <conversion-tli id="T973" />
            <conversion-tli id="T974" />
            <conversion-tli id="T975" />
            <conversion-tli id="T976" />
            <conversion-tli id="T977" />
            <conversion-tli id="T978" />
            <conversion-tli id="T979" />
            <conversion-tli id="T980" />
            <conversion-tli id="T981" />
            <conversion-tli id="T982" />
            <conversion-tli id="T983" />
            <conversion-tli id="T984" />
            <conversion-tli id="T985" />
            <conversion-tli id="T986" />
            <conversion-tli id="T987" />
            <conversion-tli id="T988" />
            <conversion-tli id="T989" />
            <conversion-tli id="T990" />
            <conversion-tli id="T991" />
            <conversion-tli id="T992" />
            <conversion-tli id="T993" />
            <conversion-tli id="T994" />
            <conversion-tli id="T995" />
            <conversion-tli id="T996" />
            <conversion-tli id="T997" />
            <conversion-tli id="T998" />
            <conversion-tli id="T999" />
            <conversion-tli id="T1000" />
            <conversion-tli id="T1001" />
            <conversion-tli id="T1002" />
            <conversion-tli id="T1003" />
            <conversion-tli id="T1004" />
            <conversion-tli id="T1005" />
            <conversion-tli id="T1006" />
            <conversion-tli id="T1007" />
            <conversion-tli id="T1008" />
            <conversion-tli id="T1009" />
            <conversion-tli id="T1010" />
            <conversion-tli id="T1011" />
            <conversion-tli id="T1012" />
            <conversion-tli id="T1013" />
            <conversion-tli id="T1014" />
            <conversion-tli id="T1015" />
            <conversion-tli id="T1016" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-PKZ"
                          name="ref"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-PKZ"
                          name="ts"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-PKZ"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-PKZ"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-PKZ"
                          name="mb"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-PKZ"
                          name="mp"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-PKZ"
                          name="ge"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-PKZ"
                          name="gr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-PKZ"
                          name="mc"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-PKZ"
                          name="ps"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-PKZ"
                          name="SyF"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-PKZ"
                          name="SeR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-PKZ"
                          name="IST"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-PKZ"
                          name="BOR"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-PKZ"
                          name="BOR-Phon"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-PKZ"
                          name="BOR-Morph"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-PKZ"
                          name="CS"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-PKZ"
                          name="fr"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-PKZ"
                          name="fe"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-PKZ"
                          name="fg"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-PKZ"
                          name="nt"
                          segmented-tier-id="tx-PKZ"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-KA"
                          name="ref"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KA"
                          name="ts"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KA"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KA"
                          type="t" />
         <conversion-tier category="CS"
                          display-name="CS-KA"
                          name="CS"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KA"
                          name="fr"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KA"
                          name="fe"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KA"
                          name="fg"
                          segmented-tier-id="tx-KA"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KA"
                          name="nt"
                          segmented-tier-id="tx-KA"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
