<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID587D42BA-B590-AFE9-7EAD-0D49DF39823F">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_Lutonyushka_flk.wav" />
         <referenced-file url="PKZ_196X_Lutonyushka_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_Lutonyushka_flk\PKZ_196X_Lutonyushka_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">333</ud-information>
            <ud-information attribute-name="# HIAT:w">219</ud-information>
            <ud-information attribute-name="# e">219</ud-information>
            <ud-information attribute-name="# HIAT:u">44</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.005" type="appl" />
         <tli id="T1" time="0.734" type="appl" />
         <tli id="T2" time="1.464" type="appl" />
         <tli id="T3" time="2.193" type="appl" />
         <tli id="T4" time="2.755" type="appl" />
         <tli id="T5" time="3.308" type="appl" />
         <tli id="T6" time="3.86" type="appl" />
         <tli id="T7" time="4.412" type="appl" />
         <tli id="T8" time="5.713" type="appl" />
         <tli id="T9" time="6.757" type="appl" />
         <tli id="T10" time="7.315" type="appl" />
         <tli id="T11" time="7.873" type="appl" />
         <tli id="T12" time="8.431" type="appl" />
         <tli id="T13" time="8.989" type="appl" />
         <tli id="T14" time="10.186342167855928" />
         <tli id="T15" time="10.976" type="appl" />
         <tli id="T16" time="11.515" type="appl" />
         <tli id="T17" time="12.055" type="appl" />
         <tli id="T18" time="12.594" type="appl" />
         <tli id="T19" time="13.134" type="appl" />
         <tli id="T20" time="13.673" type="appl" />
         <tli id="T21" time="14.213" type="appl" />
         <tli id="T22" time="14.928" type="appl" />
         <tli id="T23" time="15.642" type="appl" />
         <tli id="T24" time="16.357" type="appl" />
         <tli id="T25" time="17.071" type="appl" />
         <tli id="T26" time="18.13275569147129" />
         <tli id="T27" time="19.044" type="appl" />
         <tli id="T28" time="19.76" type="appl" />
         <tli id="T29" time="20.477" type="appl" />
         <tli id="T30" time="21.352653117567108" />
         <tli id="T31" time="21.992" type="appl" />
         <tli id="T32" time="22.618" type="appl" />
         <tli id="T33" time="23.244" type="appl" />
         <tli id="T34" time="23.87" type="appl" />
         <tli id="T35" time="24.735" type="appl" />
         <tli id="T36" time="25.334" type="appl" />
         <tli id="T37" time="25.934" type="appl" />
         <tli id="T38" time="26.533" type="appl" />
         <tli id="T39" time="27.132" type="appl" />
         <tli id="T40" time="27.732" type="appl" />
         <tli id="T41" time="28.331" type="appl" />
         <tli id="T42" time="28.93" type="appl" />
         <tli id="T43" time="29.529" type="appl" />
         <tli id="T44" time="30.129" type="appl" />
         <tli id="T45" time="30.728" type="appl" />
         <tli id="T46" time="31.327" type="appl" />
         <tli id="T47" time="31.926" type="appl" />
         <tli id="T48" time="32.526" type="appl" />
         <tli id="T49" time="33.125" type="appl" />
         <tli id="T50" time="33.724" type="appl" />
         <tli id="T51" time="34.323" type="appl" />
         <tli id="T52" time="34.922" type="appl" />
         <tli id="T53" time="35.522" type="appl" />
         <tli id="T54" time="36.121" type="appl" />
         <tli id="T55" time="36.671" type="appl" />
         <tli id="T56" time="37.221" type="appl" />
         <tli id="T57" time="37.772" type="appl" />
         <tli id="T58" time="38.322" type="appl" />
         <tli id="T59" time="38.872" type="appl" />
         <tli id="T60" time="39.505" type="appl" />
         <tli id="T61" time="40.139" type="appl" />
         <tli id="T62" time="40.772" type="appl" />
         <tli id="T63" time="41.406" type="appl" />
         <tli id="T64" time="42.039" type="appl" />
         <tli id="T65" time="43.092" type="appl" />
         <tli id="T66" time="43.759" type="appl" />
         <tli id="T67" time="44.427" type="appl" />
         <tli id="T68" time="45.094" type="appl" />
         <tli id="T69" time="46.04519983860007" />
         <tli id="T70" time="46.599" type="appl" />
         <tli id="T71" time="47.022" type="appl" />
         <tli id="T72" time="47.444" type="appl" />
         <tli id="T73" time="47.867" type="appl" />
         <tli id="T74" time="48.29" type="appl" />
         <tli id="T75" time="48.713" type="appl" />
         <tli id="T76" time="49.135" type="appl" />
         <tli id="T77" time="49.558" type="appl" />
         <tli id="T78" time="49.981" type="appl" />
         <tli id="T79" time="50.404" type="appl" />
         <tli id="T80" time="50.826" type="appl" />
         <tli id="T81" time="53.05830976044852" />
         <tli id="T82" time="53.733" type="appl" />
         <tli id="T83" time="54.243" type="appl" />
         <tli id="T84" time="55.84488765715256" />
         <tli id="T85" time="56.856" type="appl" />
         <tli id="T86" time="57.775" type="appl" />
         <tli id="T87" time="58.396" type="appl" />
         <tli id="T88" time="58.994" type="appl" />
         <tli id="T89" time="59.591" type="appl" />
         <tli id="T220" time="60.56980948012232" type="intp" />
         <tli id="T90" time="62.038023700305814" />
         <tli id="T91" time="62.38" type="appl" />
         <tli id="T92" time="62.954" type="appl" />
         <tli id="T93" time="63.529" type="appl" />
         <tli id="T94" time="64.103" type="appl" />
         <tli id="T95" time="64.678" type="appl" />
         <tli id="T96" time="65.252" type="appl" />
         <tli id="T97" time="65.827" type="appl" />
         <tli id="T98" time="66.402" type="appl" />
         <tli id="T99" time="66.976" type="appl" />
         <tli id="T100" time="67.551" type="appl" />
         <tli id="T101" time="68.125" type="appl" />
         <tli id="T102" time="68.7" type="appl" />
         <tli id="T103" time="69.382" type="appl" />
         <tli id="T104" time="70.064" type="appl" />
         <tli id="T105" time="71.00440473156644" />
         <tli id="T106" time="72.603" type="appl" />
         <tli id="T107" time="74.205" type="appl" />
         <tli id="T108" time="75.807" type="appl" />
         <tli id="T109" time="77.409" type="appl" />
         <tli id="T110" time="79.011" type="appl" />
         <tli id="T111" time="79.66" type="appl" />
         <tli id="T112" time="80.166" type="appl" />
         <tli id="T113" time="80.672" type="appl" />
         <tli id="T114" time="81.65073224600748" />
         <tli id="T115" time="82.169" type="appl" />
         <tli id="T116" time="82.679" type="appl" />
         <tli id="T117" time="83.19" type="appl" />
         <tli id="T118" time="83.7" type="appl" />
         <tli id="T119" time="84.57730568297656" />
         <tli id="T120" time="85.136" type="appl" />
         <tli id="T121" time="85.838" type="appl" />
         <tli id="T122" time="86.54" type="appl" />
         <tli id="T123" time="87.241" type="appl" />
         <tli id="T124" time="87.943" type="appl" />
         <tli id="T125" time="88.645" type="appl" />
         <tli id="T126" time="89.338" type="appl" />
         <tli id="T127" time="90.1437950220863" />
         <tli id="T128" time="90.747" type="appl" />
         <tli id="T129" time="91.314" type="appl" />
         <tli id="T130" time="91.882" type="appl" />
         <tli id="T131" time="92.484" type="appl" />
         <tli id="T132" time="93.086" type="appl" />
         <tli id="T133" time="93.688" type="appl" />
         <tli id="T134" time="94.29" type="appl" />
         <tli id="T135" time="94.892" type="appl" />
         <tli id="T136" time="95.494" type="appl" />
         <tli id="T137" time="96.096" type="appl" />
         <tli id="T138" time="96.698" type="appl" />
         <tli id="T139" time="97.402" type="appl" />
         <tli id="T140" time="98.106" type="appl" />
         <tli id="T141" time="98.81" type="appl" />
         <tli id="T142" time="99.452" type="appl" />
         <tli id="T143" time="100.093" type="appl" />
         <tli id="T144" time="101.79675713557594" />
         <tli id="T145" time="102.476" type="appl" />
         <tli id="T146" time="103.018" type="appl" />
         <tli id="T147" time="103.561" type="appl" />
         <tli id="T148" time="104.104" type="appl" />
         <tli id="T149" time="105.086" type="appl" />
         <tli id="T150" time="106.068" type="appl" />
         <tli id="T151" time="107.05" type="appl" />
         <tli id="T152" time="107.752" type="appl" />
         <tli id="T153" time="108.414" type="appl" />
         <tli id="T154" time="109.149" type="appl" />
         <tli id="T155" time="109.884" type="appl" />
         <tli id="T156" time="110.62" type="appl" />
         <tli id="T157" time="111.62977722562012" />
         <tli id="T158" time="112.526" type="appl" />
         <tli id="T159" time="113.371" type="appl" />
         <tli id="T160" time="114.215" type="appl" />
         <tli id="T161" time="115.06" type="appl" />
         <tli id="T162" time="115.904" type="appl" />
         <tli id="T163" time="116.94960775569147" />
         <tli id="T164" time="117.428" type="appl" />
         <tli id="T165" time="117.93" type="appl" />
         <tli id="T166" time="118.432" type="appl" />
         <tli id="T167" time="118.935" type="appl" />
         <tli id="T168" time="119.3" type="appl" />
         <tli id="T169" time="119.665" type="appl" />
         <tli id="T170" time="120.03" type="appl" />
         <tli id="T171" time="120.893" type="appl" />
         <tli id="T172" time="121.756" type="appl" />
         <tli id="T173" time="122.62" type="appl" />
         <tli id="T174" time="123.6360614169215" />
         <tli id="T175" time="124.511" type="appl" />
         <tli id="T176" time="125.49" type="appl" />
         <tli id="T177" time="126.47" type="appl" />
         <tli id="T178" time="127.449" type="appl" />
         <tli id="T179" time="128.136" type="appl" />
         <tli id="T180" time="128.783" type="appl" />
         <tli id="T181" time="129.429" type="appl" />
         <tli id="T182" time="130.326" type="appl" />
         <tli id="T183" time="131.222" type="appl" />
         <tli id="T184" time="132.119" type="appl" />
         <tli id="T185" time="133.08242715766227" />
         <tli id="T186" time="133.708" type="appl" />
         <tli id="T187" time="134.373" type="appl" />
         <tli id="T188" time="135.038" type="appl" />
         <tli id="T189" time="135.704" type="appl" />
         <tli id="T190" time="136.369" type="appl" />
         <tli id="T191" time="137.32895854570165" />
         <tli id="T192" time="138.034" type="appl" />
         <tli id="T193" time="138.472" type="appl" />
         <tli id="T194" time="138.911" type="appl" />
         <tli id="T195" time="139.349" type="appl" />
         <tli id="T196" time="139.788" type="appl" />
         <tli id="T197" time="140.227" type="appl" />
         <tli id="T198" time="140.665" type="appl" />
         <tli id="T199" time="141.104" type="appl" />
         <tli id="T200" time="141.542" type="appl" />
         <tli id="T201" time="141.981" type="appl" />
         <tli id="T202" time="142.975" type="appl" />
         <tli id="T203" time="143.666" type="appl" />
         <tli id="T204" time="144.358" type="appl" />
         <tli id="T205" time="145.8020219588855" />
         <tli id="T206" time="146.673" type="appl" />
         <tli id="T207" time="147.512" type="appl" />
         <tli id="T208" time="148.35" type="appl" />
         <tli id="T209" time="149.189" type="appl" />
         <tli id="T210" time="150.028" type="appl" />
         <tli id="T211" time="150.867" type="appl" />
         <tli id="T212" time="151.517" type="appl" />
         <tli id="T213" time="152.138" type="appl" />
         <tli id="T214" time="152.76" type="appl" />
         <tli id="T215" time="153.382" type="appl" />
         <tli id="T216" time="154.004" type="appl" />
         <tli id="T217" time="154.625" type="appl" />
         <tli id="T218" time="155.70170659191302" />
         <tli id="T219" time="156.955" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T219" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Amnobiʔi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">nüke</ts>
                  <nts id="Seg_8" n="HIAT:ip">,</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_11" n="HIAT:w" s="T2">büzʼe</ts>
                  <nts id="Seg_12" n="HIAT:ip">.</nts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_15" n="HIAT:u" s="T3">
                  <nts id="Seg_16" n="HIAT:ip">(</nts>
                  <ts e="T4" id="Seg_18" n="HIAT:w" s="T3">Dĭ-</ts>
                  <nts id="Seg_19" n="HIAT:ip">)</nts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_22" n="HIAT:w" s="T4">Dĭzen</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_25" n="HIAT:w" s="T5">nʼit</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">ibi</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_32" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_34" n="HIAT:w" s="T7">Kăštəbiʔi</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_37" n="HIAT:w" s="T8">Lutonʼuška</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_41" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_43" n="HIAT:w" s="T9">Dĭ</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_46" n="HIAT:w" s="T10">nüke</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_49" n="HIAT:w" s="T11">pʼešdə</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_52" n="HIAT:w" s="T12">paʔi</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_55" n="HIAT:w" s="T13">embiʔi</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_59" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_61" n="HIAT:w" s="T14">A</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_64" n="HIAT:w" s="T15">nʼi</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_67" n="HIAT:w" s="T16">da</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_70" n="HIAT:w" s="T17">büzʼe</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_73" n="HIAT:w" s="T18">nʼiʔnen</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_76" n="HIAT:w" s="T19">ĭmbidə</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_79" n="HIAT:w" s="T20">abiʔi</ts>
                  <nts id="Seg_80" n="HIAT:ip">.</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_83" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_85" n="HIAT:w" s="T21">Dĭ</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_88" n="HIAT:w" s="T22">nüke</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_91" n="HIAT:w" s="T23">pa</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_94" n="HIAT:w" s="T24">dʼünə</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_97" n="HIAT:w" s="T25">saʔməluʔpi</ts>
                  <nts id="Seg_98" n="HIAT:ip">.</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_101" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_103" n="HIAT:w" s="T26">Davaj</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_106" n="HIAT:w" s="T27">dʼorzittə</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_109" n="HIAT:w" s="T28">da</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_112" n="HIAT:w" s="T29">kirgarzittə</ts>
                  <nts id="Seg_113" n="HIAT:ip">.</nts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_116" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_118" n="HIAT:w" s="T30">Büzʼet</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_121" n="HIAT:w" s="T31">šobi:</ts>
                  <nts id="Seg_122" n="HIAT:ip">"</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_125" n="HIAT:w" s="T32">Ĭmbi</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_128" n="HIAT:w" s="T33">dʼorlaʔbəl</ts>
                  <nts id="Seg_129" n="HIAT:ip">?</nts>
                  <nts id="Seg_130" n="HIAT:ip">"</nts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_133" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_135" n="HIAT:w" s="T34">Da</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_138" n="HIAT:w" s="T35">miʔ</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_141" n="HIAT:w" s="T36">Lutonʼuška</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_144" n="HIAT:w" s="T37">bɨ</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_147" n="HIAT:w" s="T38">net</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_150" n="HIAT:w" s="T39">ibi</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_153" n="HIAT:w" s="T40">i</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_155" n="HIAT:ip">(</nts>
                  <ts e="T42" id="Seg_157" n="HIAT:w" s="T41">dĭ-</ts>
                  <nts id="Seg_158" n="HIAT:ip">)</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_161" n="HIAT:w" s="T42">dĭzeŋ</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_164" n="HIAT:w" s="T43">ešši</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_167" n="HIAT:w" s="T44">ibi</ts>
                  <nts id="Seg_168" n="HIAT:ip">,</nts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_171" n="HIAT:w" s="T45">i</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_174" n="HIAT:w" s="T46">dön</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_177" n="HIAT:w" s="T47">amnobi</ts>
                  <nts id="Seg_178" n="HIAT:ip">,</nts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_181" n="HIAT:w" s="T48">a</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_184" n="HIAT:w" s="T49">măn</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_187" n="HIAT:w" s="T50">bɨ</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_189" n="HIAT:ip">(</nts>
                  <ts e="T52" id="Seg_191" n="HIAT:w" s="T51">ku-</ts>
                  <nts id="Seg_192" n="HIAT:ip">)</nts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_195" n="HIAT:w" s="T52">kuʔpiom</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_198" n="HIAT:w" s="T53">pazʼiʔ</ts>
                  <nts id="Seg_199" n="HIAT:ip">"</nts>
                  <nts id="Seg_200" n="HIAT:ip">.</nts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_203" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_205" n="HIAT:w" s="T54">Dĭgəttə</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_208" n="HIAT:w" s="T55">i</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_211" n="HIAT:w" s="T56">büzʼe</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_214" n="HIAT:w" s="T57">davaj</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_217" n="HIAT:w" s="T58">kirgarzittə</ts>
                  <nts id="Seg_218" n="HIAT:ip">.</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_221" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_223" n="HIAT:w" s="T59">Dĭgəttə</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_226" n="HIAT:w" s="T60">nʼit</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_229" n="HIAT:w" s="T61">šobi:</ts>
                  <nts id="Seg_230" n="HIAT:ip">"</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_233" n="HIAT:w" s="T62">Ĭmbi</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_236" n="HIAT:w" s="T63">kirgarlaʔbəlaʔ</ts>
                  <nts id="Seg_237" n="HIAT:ip">?</nts>
                  <nts id="Seg_238" n="HIAT:ip">"</nts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_241" n="HIAT:u" s="T64">
                  <nts id="Seg_242" n="HIAT:ip">"</nts>
                  <ts e="T65" id="Seg_244" n="HIAT:w" s="T64">Da</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_247" n="HIAT:w" s="T65">tăn</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_250" n="HIAT:w" s="T66">bɨ</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_253" n="HIAT:w" s="T67">nel</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_256" n="HIAT:w" s="T68">ibi</ts>
                  <nts id="Seg_257" n="HIAT:ip">.</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_260" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_262" n="HIAT:w" s="T69">I</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_265" n="HIAT:w" s="T70">eššil</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_268" n="HIAT:w" s="T71">bɨ</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_271" n="HIAT:w" s="T72">ibi</ts>
                  <nts id="Seg_272" n="HIAT:ip">,</nts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_275" n="HIAT:w" s="T73">dön</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_278" n="HIAT:w" s="T74">amnobi</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_281" n="HIAT:w" s="T75">a</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_284" n="HIAT:w" s="T76">măn</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_287" n="HIAT:w" s="T77">bɨ</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_290" n="HIAT:w" s="T78">kutlambiam</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_293" n="HIAT:w" s="T79">dĭ</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_296" n="HIAT:w" s="T80">pazʼiʔ</ts>
                  <nts id="Seg_297" n="HIAT:ip">"</nts>
                  <nts id="Seg_298" n="HIAT:ip">.</nts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_301" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_303" n="HIAT:w" s="T81">Dĭgəttə</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_306" n="HIAT:w" s="T82">dĭ</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_309" n="HIAT:w" s="T83">măndə</ts>
                  <nts id="Seg_310" n="HIAT:ip">.</nts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_313" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_315" n="HIAT:w" s="T84">Üžübə</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_318" n="HIAT:w" s="T85">šerbi</ts>
                  <nts id="Seg_319" n="HIAT:ip">.</nts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_322" n="HIAT:u" s="T86">
                  <nts id="Seg_323" n="HIAT:ip">"</nts>
                  <ts e="T87" id="Seg_325" n="HIAT:w" s="T86">Amnogaʔ</ts>
                  <nts id="Seg_326" n="HIAT:ip">,</nts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_329" n="HIAT:w" s="T87">a</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_332" n="HIAT:w" s="T88">măn</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_335" n="HIAT:w" s="T89">kalla</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_338" n="HIAT:w" s="T220">dʼürlem</ts>
                  <nts id="Seg_339" n="HIAT:ip">.</nts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_342" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_344" n="HIAT:w" s="T90">Kulam</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_347" n="HIAT:w" s="T91">dĭrgit</ts>
                  <nts id="Seg_348" n="HIAT:ip">,</nts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_351" n="HIAT:w" s="T92">kak</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_354" n="HIAT:w" s="T93">šiʔ</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_357" n="HIAT:w" s="T94">dăk</ts>
                  <nts id="Seg_358" n="HIAT:ip">,</nts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_361" n="HIAT:w" s="T95">parlam</ts>
                  <nts id="Seg_362" n="HIAT:ip">,</nts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_365" n="HIAT:w" s="T96">a</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_368" n="HIAT:w" s="T97">ej</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_371" n="HIAT:w" s="T98">kulam</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_374" n="HIAT:w" s="T99">dăk</ts>
                  <nts id="Seg_375" n="HIAT:ip">,</nts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_378" n="HIAT:w" s="T100">ej</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_381" n="HIAT:w" s="T101">parlam</ts>
                  <nts id="Seg_382" n="HIAT:ip">"</nts>
                  <nts id="Seg_383" n="HIAT:ip">.</nts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_386" n="HIAT:u" s="T102">
                  <ts e="T103" id="Seg_388" n="HIAT:w" s="T102">Dĭgəttə</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_391" n="HIAT:w" s="T103">šonəga</ts>
                  <nts id="Seg_392" n="HIAT:ip">,</nts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_395" n="HIAT:w" s="T104">šonəga</ts>
                  <nts id="Seg_396" n="HIAT:ip">.</nts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_399" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_401" n="HIAT:w" s="T105">Il</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_404" n="HIAT:w" s="T106">tüžöj</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_407" n="HIAT:w" s="T107">multʼanə</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_410" n="HIAT:w" s="T108">bar</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_413" n="HIAT:w" s="T109">amnollaʔbəʔjə</ts>
                  <nts id="Seg_414" n="HIAT:ip">.</nts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_417" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_419" n="HIAT:w" s="T110">Dĭ</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_422" n="HIAT:w" s="T111">măndə:</ts>
                  <nts id="Seg_423" n="HIAT:ip">"</nts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_426" n="HIAT:w" s="T112">Ĭmbi</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_429" n="HIAT:w" s="T113">alaʔbəʔjə</ts>
                  <nts id="Seg_430" n="HIAT:ip">?</nts>
                  <nts id="Seg_431" n="HIAT:ip">"</nts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_434" n="HIAT:u" s="T114">
                  <nts id="Seg_435" n="HIAT:ip">"</nts>
                  <ts e="T115" id="Seg_437" n="HIAT:w" s="T114">A</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_440" n="HIAT:w" s="T115">kuiol</ts>
                  <nts id="Seg_441" n="HIAT:ip">,</nts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_444" n="HIAT:w" s="T116">dĭn</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_447" n="HIAT:w" s="T117">noʔ</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_450" n="HIAT:w" s="T118">amnolaʔbə</ts>
                  <nts id="Seg_451" n="HIAT:ip">"</nts>
                  <nts id="Seg_452" n="HIAT:ip">.</nts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_455" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_457" n="HIAT:w" s="T119">Dĭ</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_460" n="HIAT:w" s="T120">sʼabi</ts>
                  <nts id="Seg_461" n="HIAT:ip">,</nts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_464" n="HIAT:w" s="T121">noʔ</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_467" n="HIAT:w" s="T122">nĭŋgəbi</ts>
                  <nts id="Seg_468" n="HIAT:ip">,</nts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_471" n="HIAT:w" s="T123">tüžöjdə</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_474" n="HIAT:w" s="T124">mĭbi</ts>
                  <nts id="Seg_475" n="HIAT:ip">.</nts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_478" n="HIAT:u" s="T125">
                  <nts id="Seg_479" n="HIAT:ip">"</nts>
                  <ts e="T126" id="Seg_481" n="HIAT:w" s="T125">Amnoʔ</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_484" n="HIAT:w" s="T126">mănzi</ts>
                  <nts id="Seg_485" n="HIAT:ip">!</nts>
                  <nts id="Seg_486" n="HIAT:ip">"</nts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_489" n="HIAT:u" s="T127">
                  <nts id="Seg_490" n="HIAT:ip">"</nts>
                  <ts e="T128" id="Seg_492" n="HIAT:w" s="T127">Dʼok</ts>
                  <nts id="Seg_493" n="HIAT:ip">,</nts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_496" n="HIAT:w" s="T128">ej</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_499" n="HIAT:w" s="T129">amnobaʔ</ts>
                  <nts id="Seg_500" n="HIAT:ip">!</nts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T138" id="Seg_503" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_505" n="HIAT:w" s="T130">Măn</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_508" n="HIAT:w" s="T131">iššo</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_511" n="HIAT:w" s="T132">dĭrgit</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_514" n="HIAT:w" s="T133">il</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_517" n="HIAT:w" s="T134">iʔgö</ts>
                  <nts id="Seg_518" n="HIAT:ip">,</nts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_520" n="HIAT:ip">(</nts>
                  <ts e="T136" id="Seg_522" n="HIAT:w" s="T135">kam-</ts>
                  <nts id="Seg_523" n="HIAT:ip">)</nts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_526" n="HIAT:w" s="T136">kalam</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_529" n="HIAT:w" s="T137">kuŋge</ts>
                  <nts id="Seg_530" n="HIAT:ip">"</nts>
                  <nts id="Seg_531" n="HIAT:ip">.</nts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T141" id="Seg_534" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_536" n="HIAT:w" s="T138">Dĭgəttə</ts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_539" n="HIAT:w" s="T139">šonəga</ts>
                  <nts id="Seg_540" n="HIAT:ip">,</nts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_543" n="HIAT:w" s="T140">šonəga</ts>
                  <nts id="Seg_544" n="HIAT:ip">.</nts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_547" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_549" n="HIAT:w" s="T141">Iʔgö</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_552" n="HIAT:w" s="T142">il</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_555" n="HIAT:w" s="T143">nulaʔbə</ts>
                  <nts id="Seg_556" n="HIAT:ip">.</nts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_559" n="HIAT:u" s="T144">
                  <ts e="T145" id="Seg_561" n="HIAT:w" s="T144">I</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_563" n="HIAT:ip">(</nts>
                  <ts e="T146" id="Seg_565" n="HIAT:w" s="T145">xa-</ts>
                  <nts id="Seg_566" n="HIAT:ip">)</nts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_569" n="HIAT:w" s="T146">xămut</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_572" n="HIAT:w" s="T147">edleʔbə</ts>
                  <nts id="Seg_573" n="HIAT:ip">.</nts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T151" id="Seg_576" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_578" n="HIAT:w" s="T148">Inem</ts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_581" n="HIAT:w" s="T149">xămuttə</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_584" n="HIAT:w" s="T150">sürerlaʔbə</ts>
                  <nts id="Seg_585" n="HIAT:ip">.</nts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_588" n="HIAT:u" s="T151">
                  <nts id="Seg_589" n="HIAT:ip">"</nts>
                  <ts e="T152" id="Seg_591" n="HIAT:w" s="T151">Ĭmbi</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_594" n="HIAT:w" s="T152">alaʔbəlaʔ</ts>
                  <nts id="Seg_595" n="HIAT:ip">?</nts>
                  <nts id="Seg_596" n="HIAT:ip">"</nts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_599" n="HIAT:u" s="T153">
                  <nts id="Seg_600" n="HIAT:ip">"</nts>
                  <nts id="Seg_601" n="HIAT:ip">(</nts>
                  <ts e="T154" id="Seg_603" n="HIAT:w" s="T153">Da</ts>
                  <nts id="Seg_604" n="HIAT:ip">)</nts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_607" n="HIAT:w" s="T154">inem</ts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_610" n="HIAT:w" s="T155">ködərzittə</ts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_613" n="HIAT:w" s="T156">nada</ts>
                  <nts id="Seg_614" n="HIAT:ip">"</nts>
                  <nts id="Seg_615" n="HIAT:ip">.</nts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T163" id="Seg_618" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_620" n="HIAT:w" s="T157">Dĭ</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_623" n="HIAT:w" s="T158">xămuttə</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_626" n="HIAT:w" s="T159">šerbi</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_629" n="HIAT:w" s="T160">inenə</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_632" n="HIAT:w" s="T161">i</ts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_635" n="HIAT:w" s="T162">ködərbi</ts>
                  <nts id="Seg_636" n="HIAT:ip">.</nts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_639" n="HIAT:u" s="T163">
                  <nts id="Seg_640" n="HIAT:ip">"</nts>
                  <ts e="T164" id="Seg_642" n="HIAT:w" s="T163">Amnoʔ</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_644" n="HIAT:ip">(</nts>
                  <ts e="T165" id="Seg_646" n="HIAT:w" s="T164">mɨ-</ts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_649" n="HIAT:w" s="T165">mi-</ts>
                  <nts id="Seg_650" n="HIAT:ip">)</nts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_653" n="HIAT:w" s="T166">mănziʔ</ts>
                  <nts id="Seg_654" n="HIAT:ip">!</nts>
                  <nts id="Seg_655" n="HIAT:ip">"</nts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T170" id="Seg_658" n="HIAT:u" s="T167">
                  <nts id="Seg_659" n="HIAT:ip">"</nts>
                  <ts e="T168" id="Seg_661" n="HIAT:w" s="T167">Dʼok</ts>
                  <nts id="Seg_662" n="HIAT:ip">,</nts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_665" n="HIAT:w" s="T168">ej</ts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_668" n="HIAT:w" s="T169">amnoʔ</ts>
                  <nts id="Seg_669" n="HIAT:ip">!</nts>
                  <nts id="Seg_670" n="HIAT:ip">"</nts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T174" id="Seg_673" n="HIAT:u" s="T170">
                  <ts e="T171" id="Seg_675" n="HIAT:w" s="T170">Kandəga</ts>
                  <nts id="Seg_676" n="HIAT:ip">,</nts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_679" n="HIAT:w" s="T171">kandəga</ts>
                  <nts id="Seg_680" n="HIAT:ip">,</nts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_683" n="HIAT:w" s="T172">šobi</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_686" n="HIAT:w" s="T173">nükenə</ts>
                  <nts id="Seg_687" n="HIAT:ip">.</nts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T178" id="Seg_690" n="HIAT:u" s="T174">
                  <ts e="T175" id="Seg_692" n="HIAT:w" s="T174">Dĭ</ts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_695" n="HIAT:w" s="T175">nüke</ts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_698" n="HIAT:w" s="T176">mĭnzerleʔbə</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_701" n="HIAT:w" s="T177">kisʼelʼdə</ts>
                  <nts id="Seg_702" n="HIAT:ip">.</nts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_705" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_707" n="HIAT:w" s="T178">I</ts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_710" n="HIAT:w" s="T179">stoldə</ts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_713" n="HIAT:w" s="T180">nulbi</ts>
                  <nts id="Seg_714" n="HIAT:ip">.</nts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T185" id="Seg_717" n="HIAT:u" s="T181">
                  <ts e="T182" id="Seg_719" n="HIAT:w" s="T181">I</ts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_722" n="HIAT:w" s="T182">pogreptə</ts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_725" n="HIAT:w" s="T183">mĭlleʔbə</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_728" n="HIAT:w" s="T184">oromatsiʔ</ts>
                  <nts id="Seg_729" n="HIAT:ip">.</nts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T191" id="Seg_732" n="HIAT:u" s="T185">
                  <ts e="T186" id="Seg_734" n="HIAT:w" s="T185">A</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_737" n="HIAT:w" s="T186">dĭ</ts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_740" n="HIAT:w" s="T187">măndə:</ts>
                  <nts id="Seg_741" n="HIAT:ip">"</nts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_744" n="HIAT:w" s="T188">Ĭmbi</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_746" n="HIAT:ip">(</nts>
                  <ts e="T190" id="Seg_748" n="HIAT:w" s="T189">tažerial</ts>
                  <nts id="Seg_749" n="HIAT:ip">)</nts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_752" n="HIAT:w" s="T190">šamnaksiʔ</ts>
                  <nts id="Seg_753" n="HIAT:ip">?</nts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T201" id="Seg_756" n="HIAT:u" s="T191">
                  <ts e="T192" id="Seg_758" n="HIAT:w" s="T191">Kuiol</ts>
                  <nts id="Seg_759" n="HIAT:ip">,</nts>
                  <nts id="Seg_760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_762" n="HIAT:w" s="T192">döbər</ts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_765" n="HIAT:w" s="T193">nada</ts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_768" n="HIAT:w" s="T194">enzittə</ts>
                  <nts id="Seg_769" n="HIAT:ip">,</nts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_772" n="HIAT:w" s="T195">kanaʔ</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_774" n="HIAT:ip">(</nts>
                  <ts e="T197" id="Seg_776" n="HIAT:w" s="T196">deʔ-</ts>
                  <nts id="Seg_777" n="HIAT:ip">)</nts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_780" n="HIAT:w" s="T197">deʔtə</ts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_783" n="HIAT:w" s="T198">da</ts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_786" n="HIAT:w" s="T199">i</ts>
                  <nts id="Seg_787" n="HIAT:ip">…</nts>
                  <nts id="Seg_788" n="HIAT:ip">"</nts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T205" id="Seg_791" n="HIAT:u" s="T201">
                  <ts e="T202" id="Seg_793" n="HIAT:w" s="T201">Dĭ</ts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_796" n="HIAT:w" s="T202">kambi</ts>
                  <nts id="Seg_797" n="HIAT:ip">,</nts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_799" n="HIAT:ip">(</nts>
                  <ts e="T204" id="Seg_801" n="HIAT:w" s="T203">deʔtə</ts>
                  <nts id="Seg_802" n="HIAT:ip">)</nts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_805" n="HIAT:w" s="T204">amnəlbiʔi</ts>
                  <nts id="Seg_806" n="HIAT:ip">.</nts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T211" id="Seg_809" n="HIAT:u" s="T205">
                  <ts e="T206" id="Seg_811" n="HIAT:w" s="T205">Lutonʼuška</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_814" n="HIAT:w" s="T206">ambi</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_817" n="HIAT:w" s="T207">i</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_820" n="HIAT:w" s="T208">pălattə</ts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_823" n="HIAT:w" s="T209">iʔbəbi</ts>
                  <nts id="Seg_824" n="HIAT:ip">,</nts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_827" n="HIAT:w" s="T210">kunolbi</ts>
                  <nts id="Seg_828" n="HIAT:ip">.</nts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T218" id="Seg_831" n="HIAT:u" s="T211">
                  <ts e="T212" id="Seg_833" n="HIAT:w" s="T211">Erten</ts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_836" n="HIAT:w" s="T212">uʔbdəbi</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_839" n="HIAT:w" s="T213">i</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_842" n="HIAT:w" s="T214">parluʔpi</ts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_845" n="HIAT:w" s="T215">abandə</ts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_848" n="HIAT:w" s="T216">da</ts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_851" n="HIAT:w" s="T217">iandə</ts>
                  <nts id="Seg_852" n="HIAT:ip">.</nts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T219" id="Seg_855" n="HIAT:u" s="T218">
                  <ts e="T219" id="Seg_857" n="HIAT:w" s="T218">Kabarləj</ts>
                  <nts id="Seg_858" n="HIAT:ip">.</nts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T219" id="Seg_860" n="sc" s="T0">
               <ts e="T1" id="Seg_862" n="e" s="T0">Amnobiʔi </ts>
               <ts e="T2" id="Seg_864" n="e" s="T1">nüke, </ts>
               <ts e="T3" id="Seg_866" n="e" s="T2">büzʼe. </ts>
               <ts e="T4" id="Seg_868" n="e" s="T3">(Dĭ-) </ts>
               <ts e="T5" id="Seg_870" n="e" s="T4">Dĭzen </ts>
               <ts e="T6" id="Seg_872" n="e" s="T5">nʼit </ts>
               <ts e="T7" id="Seg_874" n="e" s="T6">ibi. </ts>
               <ts e="T8" id="Seg_876" n="e" s="T7">Kăštəbiʔi </ts>
               <ts e="T9" id="Seg_878" n="e" s="T8">Lutonʼuška. </ts>
               <ts e="T10" id="Seg_880" n="e" s="T9">Dĭ </ts>
               <ts e="T11" id="Seg_882" n="e" s="T10">nüke </ts>
               <ts e="T12" id="Seg_884" n="e" s="T11">pʼešdə </ts>
               <ts e="T13" id="Seg_886" n="e" s="T12">paʔi </ts>
               <ts e="T14" id="Seg_888" n="e" s="T13">embiʔi. </ts>
               <ts e="T15" id="Seg_890" n="e" s="T14">A </ts>
               <ts e="T16" id="Seg_892" n="e" s="T15">nʼi </ts>
               <ts e="T17" id="Seg_894" n="e" s="T16">da </ts>
               <ts e="T18" id="Seg_896" n="e" s="T17">büzʼe </ts>
               <ts e="T19" id="Seg_898" n="e" s="T18">nʼiʔnen </ts>
               <ts e="T20" id="Seg_900" n="e" s="T19">ĭmbidə </ts>
               <ts e="T21" id="Seg_902" n="e" s="T20">abiʔi. </ts>
               <ts e="T22" id="Seg_904" n="e" s="T21">Dĭ </ts>
               <ts e="T23" id="Seg_906" n="e" s="T22">nüke </ts>
               <ts e="T24" id="Seg_908" n="e" s="T23">pa </ts>
               <ts e="T25" id="Seg_910" n="e" s="T24">dʼünə </ts>
               <ts e="T26" id="Seg_912" n="e" s="T25">saʔməluʔpi. </ts>
               <ts e="T27" id="Seg_914" n="e" s="T26">Davaj </ts>
               <ts e="T28" id="Seg_916" n="e" s="T27">dʼorzittə </ts>
               <ts e="T29" id="Seg_918" n="e" s="T28">da </ts>
               <ts e="T30" id="Seg_920" n="e" s="T29">kirgarzittə. </ts>
               <ts e="T31" id="Seg_922" n="e" s="T30">Büzʼet </ts>
               <ts e="T32" id="Seg_924" n="e" s="T31">šobi:" </ts>
               <ts e="T33" id="Seg_926" n="e" s="T32">Ĭmbi </ts>
               <ts e="T34" id="Seg_928" n="e" s="T33">dʼorlaʔbəl?" </ts>
               <ts e="T35" id="Seg_930" n="e" s="T34">Da </ts>
               <ts e="T36" id="Seg_932" n="e" s="T35">miʔ </ts>
               <ts e="T37" id="Seg_934" n="e" s="T36">Lutonʼuška </ts>
               <ts e="T38" id="Seg_936" n="e" s="T37">bɨ </ts>
               <ts e="T39" id="Seg_938" n="e" s="T38">net </ts>
               <ts e="T40" id="Seg_940" n="e" s="T39">ibi </ts>
               <ts e="T41" id="Seg_942" n="e" s="T40">i </ts>
               <ts e="T42" id="Seg_944" n="e" s="T41">(dĭ-) </ts>
               <ts e="T43" id="Seg_946" n="e" s="T42">dĭzeŋ </ts>
               <ts e="T44" id="Seg_948" n="e" s="T43">ešši </ts>
               <ts e="T45" id="Seg_950" n="e" s="T44">ibi, </ts>
               <ts e="T46" id="Seg_952" n="e" s="T45">i </ts>
               <ts e="T47" id="Seg_954" n="e" s="T46">dön </ts>
               <ts e="T48" id="Seg_956" n="e" s="T47">amnobi, </ts>
               <ts e="T49" id="Seg_958" n="e" s="T48">a </ts>
               <ts e="T50" id="Seg_960" n="e" s="T49">măn </ts>
               <ts e="T51" id="Seg_962" n="e" s="T50">bɨ </ts>
               <ts e="T52" id="Seg_964" n="e" s="T51">(ku-) </ts>
               <ts e="T53" id="Seg_966" n="e" s="T52">kuʔpiom </ts>
               <ts e="T54" id="Seg_968" n="e" s="T53">pazʼiʔ". </ts>
               <ts e="T55" id="Seg_970" n="e" s="T54">Dĭgəttə </ts>
               <ts e="T56" id="Seg_972" n="e" s="T55">i </ts>
               <ts e="T57" id="Seg_974" n="e" s="T56">büzʼe </ts>
               <ts e="T58" id="Seg_976" n="e" s="T57">davaj </ts>
               <ts e="T59" id="Seg_978" n="e" s="T58">kirgarzittə. </ts>
               <ts e="T60" id="Seg_980" n="e" s="T59">Dĭgəttə </ts>
               <ts e="T61" id="Seg_982" n="e" s="T60">nʼit </ts>
               <ts e="T62" id="Seg_984" n="e" s="T61">šobi:" </ts>
               <ts e="T63" id="Seg_986" n="e" s="T62">Ĭmbi </ts>
               <ts e="T64" id="Seg_988" n="e" s="T63">kirgarlaʔbəlaʔ?" </ts>
               <ts e="T65" id="Seg_990" n="e" s="T64">"Da </ts>
               <ts e="T66" id="Seg_992" n="e" s="T65">tăn </ts>
               <ts e="T67" id="Seg_994" n="e" s="T66">bɨ </ts>
               <ts e="T68" id="Seg_996" n="e" s="T67">nel </ts>
               <ts e="T69" id="Seg_998" n="e" s="T68">ibi. </ts>
               <ts e="T70" id="Seg_1000" n="e" s="T69">I </ts>
               <ts e="T71" id="Seg_1002" n="e" s="T70">eššil </ts>
               <ts e="T72" id="Seg_1004" n="e" s="T71">bɨ </ts>
               <ts e="T73" id="Seg_1006" n="e" s="T72">ibi, </ts>
               <ts e="T74" id="Seg_1008" n="e" s="T73">dön </ts>
               <ts e="T75" id="Seg_1010" n="e" s="T74">amnobi </ts>
               <ts e="T76" id="Seg_1012" n="e" s="T75">a </ts>
               <ts e="T77" id="Seg_1014" n="e" s="T76">măn </ts>
               <ts e="T78" id="Seg_1016" n="e" s="T77">bɨ </ts>
               <ts e="T79" id="Seg_1018" n="e" s="T78">kutlambiam </ts>
               <ts e="T80" id="Seg_1020" n="e" s="T79">dĭ </ts>
               <ts e="T81" id="Seg_1022" n="e" s="T80">pazʼiʔ". </ts>
               <ts e="T82" id="Seg_1024" n="e" s="T81">Dĭgəttə </ts>
               <ts e="T83" id="Seg_1026" n="e" s="T82">dĭ </ts>
               <ts e="T84" id="Seg_1028" n="e" s="T83">măndə. </ts>
               <ts e="T85" id="Seg_1030" n="e" s="T84">Üžübə </ts>
               <ts e="T86" id="Seg_1032" n="e" s="T85">šerbi. </ts>
               <ts e="T87" id="Seg_1034" n="e" s="T86">"Amnogaʔ, </ts>
               <ts e="T88" id="Seg_1036" n="e" s="T87">a </ts>
               <ts e="T89" id="Seg_1038" n="e" s="T88">măn </ts>
               <ts e="T220" id="Seg_1040" n="e" s="T89">kalla </ts>
               <ts e="T90" id="Seg_1042" n="e" s="T220">dʼürlem. </ts>
               <ts e="T91" id="Seg_1044" n="e" s="T90">Kulam </ts>
               <ts e="T92" id="Seg_1046" n="e" s="T91">dĭrgit, </ts>
               <ts e="T93" id="Seg_1048" n="e" s="T92">kak </ts>
               <ts e="T94" id="Seg_1050" n="e" s="T93">šiʔ </ts>
               <ts e="T95" id="Seg_1052" n="e" s="T94">dăk, </ts>
               <ts e="T96" id="Seg_1054" n="e" s="T95">parlam, </ts>
               <ts e="T97" id="Seg_1056" n="e" s="T96">a </ts>
               <ts e="T98" id="Seg_1058" n="e" s="T97">ej </ts>
               <ts e="T99" id="Seg_1060" n="e" s="T98">kulam </ts>
               <ts e="T100" id="Seg_1062" n="e" s="T99">dăk, </ts>
               <ts e="T101" id="Seg_1064" n="e" s="T100">ej </ts>
               <ts e="T102" id="Seg_1066" n="e" s="T101">parlam". </ts>
               <ts e="T103" id="Seg_1068" n="e" s="T102">Dĭgəttə </ts>
               <ts e="T104" id="Seg_1070" n="e" s="T103">šonəga, </ts>
               <ts e="T105" id="Seg_1072" n="e" s="T104">šonəga. </ts>
               <ts e="T106" id="Seg_1074" n="e" s="T105">Il </ts>
               <ts e="T107" id="Seg_1076" n="e" s="T106">tüžöj </ts>
               <ts e="T108" id="Seg_1078" n="e" s="T107">multʼanə </ts>
               <ts e="T109" id="Seg_1080" n="e" s="T108">bar </ts>
               <ts e="T110" id="Seg_1082" n="e" s="T109">amnollaʔbəʔjə. </ts>
               <ts e="T111" id="Seg_1084" n="e" s="T110">Dĭ </ts>
               <ts e="T112" id="Seg_1086" n="e" s="T111">măndə:" </ts>
               <ts e="T113" id="Seg_1088" n="e" s="T112">Ĭmbi </ts>
               <ts e="T114" id="Seg_1090" n="e" s="T113">alaʔbəʔjə?" </ts>
               <ts e="T115" id="Seg_1092" n="e" s="T114">"A </ts>
               <ts e="T116" id="Seg_1094" n="e" s="T115">kuiol, </ts>
               <ts e="T117" id="Seg_1096" n="e" s="T116">dĭn </ts>
               <ts e="T118" id="Seg_1098" n="e" s="T117">noʔ </ts>
               <ts e="T119" id="Seg_1100" n="e" s="T118">amnolaʔbə". </ts>
               <ts e="T120" id="Seg_1102" n="e" s="T119">Dĭ </ts>
               <ts e="T121" id="Seg_1104" n="e" s="T120">sʼabi, </ts>
               <ts e="T122" id="Seg_1106" n="e" s="T121">noʔ </ts>
               <ts e="T123" id="Seg_1108" n="e" s="T122">nĭŋgəbi, </ts>
               <ts e="T124" id="Seg_1110" n="e" s="T123">tüžöjdə </ts>
               <ts e="T125" id="Seg_1112" n="e" s="T124">mĭbi. </ts>
               <ts e="T126" id="Seg_1114" n="e" s="T125">"Amnoʔ </ts>
               <ts e="T127" id="Seg_1116" n="e" s="T126">mănzi!" </ts>
               <ts e="T128" id="Seg_1118" n="e" s="T127">"Dʼok, </ts>
               <ts e="T129" id="Seg_1120" n="e" s="T128">ej </ts>
               <ts e="T130" id="Seg_1122" n="e" s="T129">amnobaʔ! </ts>
               <ts e="T131" id="Seg_1124" n="e" s="T130">Măn </ts>
               <ts e="T132" id="Seg_1126" n="e" s="T131">iššo </ts>
               <ts e="T133" id="Seg_1128" n="e" s="T132">dĭrgit </ts>
               <ts e="T134" id="Seg_1130" n="e" s="T133">il </ts>
               <ts e="T135" id="Seg_1132" n="e" s="T134">iʔgö, </ts>
               <ts e="T136" id="Seg_1134" n="e" s="T135">(kam-) </ts>
               <ts e="T137" id="Seg_1136" n="e" s="T136">kalam </ts>
               <ts e="T138" id="Seg_1138" n="e" s="T137">kuŋge". </ts>
               <ts e="T139" id="Seg_1140" n="e" s="T138">Dĭgəttə </ts>
               <ts e="T140" id="Seg_1142" n="e" s="T139">šonəga, </ts>
               <ts e="T141" id="Seg_1144" n="e" s="T140">šonəga. </ts>
               <ts e="T142" id="Seg_1146" n="e" s="T141">Iʔgö </ts>
               <ts e="T143" id="Seg_1148" n="e" s="T142">il </ts>
               <ts e="T144" id="Seg_1150" n="e" s="T143">nulaʔbə. </ts>
               <ts e="T145" id="Seg_1152" n="e" s="T144">I </ts>
               <ts e="T146" id="Seg_1154" n="e" s="T145">(xa-) </ts>
               <ts e="T147" id="Seg_1156" n="e" s="T146">xămut </ts>
               <ts e="T148" id="Seg_1158" n="e" s="T147">edleʔbə. </ts>
               <ts e="T149" id="Seg_1160" n="e" s="T148">Inem </ts>
               <ts e="T150" id="Seg_1162" n="e" s="T149">xămuttə </ts>
               <ts e="T151" id="Seg_1164" n="e" s="T150">sürerlaʔbə. </ts>
               <ts e="T152" id="Seg_1166" n="e" s="T151">"Ĭmbi </ts>
               <ts e="T153" id="Seg_1168" n="e" s="T152">alaʔbəlaʔ?" </ts>
               <ts e="T154" id="Seg_1170" n="e" s="T153">"(Da) </ts>
               <ts e="T155" id="Seg_1172" n="e" s="T154">inem </ts>
               <ts e="T156" id="Seg_1174" n="e" s="T155">ködərzittə </ts>
               <ts e="T157" id="Seg_1176" n="e" s="T156">nada". </ts>
               <ts e="T158" id="Seg_1178" n="e" s="T157">Dĭ </ts>
               <ts e="T159" id="Seg_1180" n="e" s="T158">xămuttə </ts>
               <ts e="T160" id="Seg_1182" n="e" s="T159">šerbi </ts>
               <ts e="T161" id="Seg_1184" n="e" s="T160">inenə </ts>
               <ts e="T162" id="Seg_1186" n="e" s="T161">i </ts>
               <ts e="T163" id="Seg_1188" n="e" s="T162">ködərbi. </ts>
               <ts e="T164" id="Seg_1190" n="e" s="T163">"Amnoʔ </ts>
               <ts e="T165" id="Seg_1192" n="e" s="T164">(mɨ- </ts>
               <ts e="T166" id="Seg_1194" n="e" s="T165">mi-) </ts>
               <ts e="T167" id="Seg_1196" n="e" s="T166">mănziʔ!" </ts>
               <ts e="T168" id="Seg_1198" n="e" s="T167">"Dʼok, </ts>
               <ts e="T169" id="Seg_1200" n="e" s="T168">ej </ts>
               <ts e="T170" id="Seg_1202" n="e" s="T169">amnoʔ!" </ts>
               <ts e="T171" id="Seg_1204" n="e" s="T170">Kandəga, </ts>
               <ts e="T172" id="Seg_1206" n="e" s="T171">kandəga, </ts>
               <ts e="T173" id="Seg_1208" n="e" s="T172">šobi </ts>
               <ts e="T174" id="Seg_1210" n="e" s="T173">nükenə. </ts>
               <ts e="T175" id="Seg_1212" n="e" s="T174">Dĭ </ts>
               <ts e="T176" id="Seg_1214" n="e" s="T175">nüke </ts>
               <ts e="T177" id="Seg_1216" n="e" s="T176">mĭnzerleʔbə </ts>
               <ts e="T178" id="Seg_1218" n="e" s="T177">kisʼelʼdə. </ts>
               <ts e="T179" id="Seg_1220" n="e" s="T178">I </ts>
               <ts e="T180" id="Seg_1222" n="e" s="T179">stoldə </ts>
               <ts e="T181" id="Seg_1224" n="e" s="T180">nulbi. </ts>
               <ts e="T182" id="Seg_1226" n="e" s="T181">I </ts>
               <ts e="T183" id="Seg_1228" n="e" s="T182">pogreptə </ts>
               <ts e="T184" id="Seg_1230" n="e" s="T183">mĭlleʔbə </ts>
               <ts e="T185" id="Seg_1232" n="e" s="T184">oromatsiʔ. </ts>
               <ts e="T186" id="Seg_1234" n="e" s="T185">A </ts>
               <ts e="T187" id="Seg_1236" n="e" s="T186">dĭ </ts>
               <ts e="T188" id="Seg_1238" n="e" s="T187">măndə:" </ts>
               <ts e="T189" id="Seg_1240" n="e" s="T188">Ĭmbi </ts>
               <ts e="T190" id="Seg_1242" n="e" s="T189">(tažerial) </ts>
               <ts e="T191" id="Seg_1244" n="e" s="T190">šamnaksiʔ? </ts>
               <ts e="T192" id="Seg_1246" n="e" s="T191">Kuiol, </ts>
               <ts e="T193" id="Seg_1248" n="e" s="T192">döbər </ts>
               <ts e="T194" id="Seg_1250" n="e" s="T193">nada </ts>
               <ts e="T195" id="Seg_1252" n="e" s="T194">enzittə, </ts>
               <ts e="T196" id="Seg_1254" n="e" s="T195">kanaʔ </ts>
               <ts e="T197" id="Seg_1256" n="e" s="T196">(deʔ-) </ts>
               <ts e="T198" id="Seg_1258" n="e" s="T197">deʔtə </ts>
               <ts e="T199" id="Seg_1260" n="e" s="T198">da </ts>
               <ts e="T201" id="Seg_1262" n="e" s="T199">i…" </ts>
               <ts e="T202" id="Seg_1264" n="e" s="T201">Dĭ </ts>
               <ts e="T203" id="Seg_1266" n="e" s="T202">kambi, </ts>
               <ts e="T204" id="Seg_1268" n="e" s="T203">(deʔtə) </ts>
               <ts e="T205" id="Seg_1270" n="e" s="T204">amnəlbiʔi. </ts>
               <ts e="T206" id="Seg_1272" n="e" s="T205">Lutonʼuška </ts>
               <ts e="T207" id="Seg_1274" n="e" s="T206">ambi </ts>
               <ts e="T208" id="Seg_1276" n="e" s="T207">i </ts>
               <ts e="T209" id="Seg_1278" n="e" s="T208">pălattə </ts>
               <ts e="T210" id="Seg_1280" n="e" s="T209">iʔbəbi, </ts>
               <ts e="T211" id="Seg_1282" n="e" s="T210">kunolbi. </ts>
               <ts e="T212" id="Seg_1284" n="e" s="T211">Erten </ts>
               <ts e="T213" id="Seg_1286" n="e" s="T212">uʔbdəbi </ts>
               <ts e="T214" id="Seg_1288" n="e" s="T213">i </ts>
               <ts e="T215" id="Seg_1290" n="e" s="T214">parluʔpi </ts>
               <ts e="T216" id="Seg_1292" n="e" s="T215">abandə </ts>
               <ts e="T217" id="Seg_1294" n="e" s="T216">da </ts>
               <ts e="T218" id="Seg_1296" n="e" s="T217">iandə. </ts>
               <ts e="T219" id="Seg_1298" n="e" s="T218">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_1299" s="T0">PKZ_196X_Lutonyushka_flk.001 (001)</ta>
            <ta e="T7" id="Seg_1300" s="T3">PKZ_196X_Lutonyushka_flk.002 (002)</ta>
            <ta e="T9" id="Seg_1301" s="T7">PKZ_196X_Lutonyushka_flk.003 (003)</ta>
            <ta e="T14" id="Seg_1302" s="T9">PKZ_196X_Lutonyushka_flk.004 (004)</ta>
            <ta e="T21" id="Seg_1303" s="T14">PKZ_196X_Lutonyushka_flk.005 (005)</ta>
            <ta e="T26" id="Seg_1304" s="T21">PKZ_196X_Lutonyushka_flk.006 (006)</ta>
            <ta e="T30" id="Seg_1305" s="T26">PKZ_196X_Lutonyushka_flk.007 (007)</ta>
            <ta e="T34" id="Seg_1306" s="T30">PKZ_196X_Lutonyushka_flk.008 (008)</ta>
            <ta e="T54" id="Seg_1307" s="T34">PKZ_196X_Lutonyushka_flk.009 (009)</ta>
            <ta e="T59" id="Seg_1308" s="T54">PKZ_196X_Lutonyushka_flk.010 (010)</ta>
            <ta e="T64" id="Seg_1309" s="T59">PKZ_196X_Lutonyushka_flk.011 (011)</ta>
            <ta e="T69" id="Seg_1310" s="T64">PKZ_196X_Lutonyushka_flk.012 (012)</ta>
            <ta e="T81" id="Seg_1311" s="T69">PKZ_196X_Lutonyushka_flk.013 (013)</ta>
            <ta e="T84" id="Seg_1312" s="T81">PKZ_196X_Lutonyushka_flk.014 (014)</ta>
            <ta e="T86" id="Seg_1313" s="T84">PKZ_196X_Lutonyushka_flk.015 (015)</ta>
            <ta e="T90" id="Seg_1314" s="T86">PKZ_196X_Lutonyushka_flk.016 (016)</ta>
            <ta e="T102" id="Seg_1315" s="T90">PKZ_196X_Lutonyushka_flk.017 (017)</ta>
            <ta e="T105" id="Seg_1316" s="T102">PKZ_196X_Lutonyushka_flk.018 (018)</ta>
            <ta e="T110" id="Seg_1317" s="T105">PKZ_196X_Lutonyushka_flk.019 (019)</ta>
            <ta e="T114" id="Seg_1318" s="T110">PKZ_196X_Lutonyushka_flk.020 (020)</ta>
            <ta e="T119" id="Seg_1319" s="T114">PKZ_196X_Lutonyushka_flk.021 (021)</ta>
            <ta e="T125" id="Seg_1320" s="T119">PKZ_196X_Lutonyushka_flk.022 (022)</ta>
            <ta e="T127" id="Seg_1321" s="T125">PKZ_196X_Lutonyushka_flk.023 (023)</ta>
            <ta e="T130" id="Seg_1322" s="T127">PKZ_196X_Lutonyushka_flk.024 (024)</ta>
            <ta e="T138" id="Seg_1323" s="T130">PKZ_196X_Lutonyushka_flk.025 (025)</ta>
            <ta e="T141" id="Seg_1324" s="T138">PKZ_196X_Lutonyushka_flk.026 (026)</ta>
            <ta e="T144" id="Seg_1325" s="T141">PKZ_196X_Lutonyushka_flk.027 (027)</ta>
            <ta e="T148" id="Seg_1326" s="T144">PKZ_196X_Lutonyushka_flk.028 (028)</ta>
            <ta e="T151" id="Seg_1327" s="T148">PKZ_196X_Lutonyushka_flk.029 (029)</ta>
            <ta e="T153" id="Seg_1328" s="T151">PKZ_196X_Lutonyushka_flk.030 (030)</ta>
            <ta e="T157" id="Seg_1329" s="T153">PKZ_196X_Lutonyushka_flk.031 (031)</ta>
            <ta e="T163" id="Seg_1330" s="T157">PKZ_196X_Lutonyushka_flk.032 (032)</ta>
            <ta e="T167" id="Seg_1331" s="T163">PKZ_196X_Lutonyushka_flk.033 (033)</ta>
            <ta e="T170" id="Seg_1332" s="T167">PKZ_196X_Lutonyushka_flk.034 (034)</ta>
            <ta e="T174" id="Seg_1333" s="T170">PKZ_196X_Lutonyushka_flk.035 (035)</ta>
            <ta e="T178" id="Seg_1334" s="T174">PKZ_196X_Lutonyushka_flk.036 (036)</ta>
            <ta e="T181" id="Seg_1335" s="T178">PKZ_196X_Lutonyushka_flk.037 (037)</ta>
            <ta e="T185" id="Seg_1336" s="T181">PKZ_196X_Lutonyushka_flk.038 (038)</ta>
            <ta e="T191" id="Seg_1337" s="T185">PKZ_196X_Lutonyushka_flk.039 (039)</ta>
            <ta e="T201" id="Seg_1338" s="T191">PKZ_196X_Lutonyushka_flk.040 (040)</ta>
            <ta e="T205" id="Seg_1339" s="T201">PKZ_196X_Lutonyushka_flk.041 (041)</ta>
            <ta e="T211" id="Seg_1340" s="T205">PKZ_196X_Lutonyushka_flk.042 (042)</ta>
            <ta e="T218" id="Seg_1341" s="T211">PKZ_196X_Lutonyushka_flk.043 (043)</ta>
            <ta e="T219" id="Seg_1342" s="T218">PKZ_196X_Lutonyushka_flk.044 (044)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_1343" s="T0">Amnobiʔi nüke, büzʼe. </ta>
            <ta e="T7" id="Seg_1344" s="T3">(Dĭ-) Dĭzen nʼit ibi. </ta>
            <ta e="T9" id="Seg_1345" s="T7">Kăštəbiʔi Lutonʼuška. </ta>
            <ta e="T14" id="Seg_1346" s="T9">Dĭ nüke pʼešdə paʔi embiʔi. </ta>
            <ta e="T21" id="Seg_1347" s="T14">A nʼi da büzʼe nʼiʔnen ĭmbidə abiʔi. </ta>
            <ta e="T26" id="Seg_1348" s="T21">Dĭ nüke pa dʼünə saʔməluʔpi. </ta>
            <ta e="T30" id="Seg_1349" s="T26">Davaj dʼorzittə da kirgarzittə. </ta>
            <ta e="T34" id="Seg_1350" s="T30">Büzʼet šobi:" Ĭmbi dʼorlaʔbəl?" </ta>
            <ta e="T54" id="Seg_1351" s="T34">Da miʔ Lutonʼuška bɨ net ibi i (dĭ-) dĭzeŋ ešši ibi, i dön amnobi, a măn bɨ (ku-) kuʔpiom pazʼiʔ". </ta>
            <ta e="T59" id="Seg_1352" s="T54">Dĭgəttə i büzʼe davaj kirgarzittə. </ta>
            <ta e="T64" id="Seg_1353" s="T59">Dĭgəttə nʼit šobi:" Ĭmbi kirgarlaʔbəlaʔ?" </ta>
            <ta e="T69" id="Seg_1354" s="T64">"Da tăn bɨ ((COUGH)) nel ibi. </ta>
            <ta e="T81" id="Seg_1355" s="T69">I eššil bɨ ibi, dön amnobi a măn bɨ kutlambiam dĭ pazʼiʔ". </ta>
            <ta e="T84" id="Seg_1356" s="T81">Dĭgəttə dĭ măndə. </ta>
            <ta e="T86" id="Seg_1357" s="T84">Üžübə šerbi. </ta>
            <ta e="T90" id="Seg_1358" s="T86">"Amnogaʔ, a măn kalla dʼürlem. </ta>
            <ta e="T102" id="Seg_1359" s="T90">Kulam dĭrgit, kak šiʔ dăk, parlam, a ej kulam dăk, ej parlam". </ta>
            <ta e="T105" id="Seg_1360" s="T102">Dĭgəttə šonəga, šonəga. </ta>
            <ta e="T110" id="Seg_1361" s="T105">Il tüžöj multʼanə bar amnollaʔbəʔjə. </ta>
            <ta e="T114" id="Seg_1362" s="T110">Dĭ măndə:" Ĭmbi alaʔbəʔjə?" </ta>
            <ta e="T119" id="Seg_1363" s="T114">"A kuiol, dĭn noʔ amnolaʔbə". </ta>
            <ta e="T125" id="Seg_1364" s="T119">Dĭ sʼabi, noʔ nĭŋgəbi, tüžöjdə mĭbi. </ta>
            <ta e="T127" id="Seg_1365" s="T125">"Amnoʔ mănzi!" </ta>
            <ta e="T130" id="Seg_1366" s="T127">"Dʼok, ej amnobaʔ! </ta>
            <ta e="T138" id="Seg_1367" s="T130">Măn iššo dĭrgit il iʔgö, (kam-) kalam kuŋge". </ta>
            <ta e="T141" id="Seg_1368" s="T138">Dĭgəttə šonəga, šonəga. </ta>
            <ta e="T144" id="Seg_1369" s="T141">Iʔgö il nulaʔbə. </ta>
            <ta e="T148" id="Seg_1370" s="T144">I (xa-) xămut edleʔbə. </ta>
            <ta e="T151" id="Seg_1371" s="T148">Inem xămuttə sürerlaʔbə. </ta>
            <ta e="T153" id="Seg_1372" s="T151">"Ĭmbi alaʔbəlaʔ?" </ta>
            <ta e="T157" id="Seg_1373" s="T153">"(Da) inem ködərzittə nada". </ta>
            <ta e="T163" id="Seg_1374" s="T157">Dĭ xămuttə šerbi inenə i ködərbi. </ta>
            <ta e="T167" id="Seg_1375" s="T163">"Amnoʔ (mɨ- mi-) mănziʔ!" </ta>
            <ta e="T170" id="Seg_1376" s="T167">"Dʼok, ej amnoʔ!" </ta>
            <ta e="T174" id="Seg_1377" s="T170">Kandəga, kandəga, šobi nükenə. </ta>
            <ta e="T178" id="Seg_1378" s="T174">Dĭ nüke mĭnzerleʔbə kisʼelʼdə. </ta>
            <ta e="T181" id="Seg_1379" s="T178">I stoldə nulbi. </ta>
            <ta e="T185" id="Seg_1380" s="T181">I pogreptə mĭlleʔbə oromatsiʔ. </ta>
            <ta e="T191" id="Seg_1381" s="T185">A dĭ măndə:" Ĭmbi (tažerial) šamnaksiʔ? </ta>
            <ta e="T201" id="Seg_1382" s="T191">Kuiol, döbər nada enzittə, kanaʔ (deʔ-) deʔtə da i…" </ta>
            <ta e="T205" id="Seg_1383" s="T201">Dĭ kambi, (deʔtə) amnəlbiʔi. </ta>
            <ta e="T211" id="Seg_1384" s="T205">Lutonʼuška ambi i pălattə iʔbəbi, kunolbi. </ta>
            <ta e="T218" id="Seg_1385" s="T211">Erten uʔbdəbi i parluʔpi abandə da iandə. </ta>
            <ta e="T219" id="Seg_1386" s="T218">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1387" s="T0">amno-bi-ʔi</ta>
            <ta e="T2" id="Seg_1388" s="T1">nüke</ta>
            <ta e="T3" id="Seg_1389" s="T2">büzʼe</ta>
            <ta e="T5" id="Seg_1390" s="T4">dĭ-zen</ta>
            <ta e="T6" id="Seg_1391" s="T5">nʼi-t</ta>
            <ta e="T7" id="Seg_1392" s="T6">i-bi</ta>
            <ta e="T8" id="Seg_1393" s="T7">kăštə-bi-ʔi</ta>
            <ta e="T9" id="Seg_1394" s="T8">Lutonʼuška</ta>
            <ta e="T10" id="Seg_1395" s="T9">dĭ</ta>
            <ta e="T11" id="Seg_1396" s="T10">nüke</ta>
            <ta e="T12" id="Seg_1397" s="T11">pʼeš-də</ta>
            <ta e="T13" id="Seg_1398" s="T12">pa-ʔi</ta>
            <ta e="T14" id="Seg_1399" s="T13">em-bi-ʔi</ta>
            <ta e="T15" id="Seg_1400" s="T14">a</ta>
            <ta e="T16" id="Seg_1401" s="T15">nʼi</ta>
            <ta e="T17" id="Seg_1402" s="T16">da</ta>
            <ta e="T18" id="Seg_1403" s="T17">büzʼe</ta>
            <ta e="T19" id="Seg_1404" s="T18">nʼiʔnen</ta>
            <ta e="T20" id="Seg_1405" s="T19">ĭmbi=də</ta>
            <ta e="T21" id="Seg_1406" s="T20">a-bi-ʔi</ta>
            <ta e="T22" id="Seg_1407" s="T21">dĭ</ta>
            <ta e="T23" id="Seg_1408" s="T22">nüke</ta>
            <ta e="T24" id="Seg_1409" s="T23">pa</ta>
            <ta e="T25" id="Seg_1410" s="T24">dʼü-nə</ta>
            <ta e="T26" id="Seg_1411" s="T25">saʔmə-luʔ-pi</ta>
            <ta e="T27" id="Seg_1412" s="T26">davaj</ta>
            <ta e="T28" id="Seg_1413" s="T27">dʼor-zittə</ta>
            <ta e="T29" id="Seg_1414" s="T28">da</ta>
            <ta e="T30" id="Seg_1415" s="T29">kirgar-zittə</ta>
            <ta e="T31" id="Seg_1416" s="T30">büzʼe-t</ta>
            <ta e="T32" id="Seg_1417" s="T31">šo-bi</ta>
            <ta e="T33" id="Seg_1418" s="T32">ĭmbi</ta>
            <ta e="T34" id="Seg_1419" s="T33">dʼor-laʔbə-l</ta>
            <ta e="T35" id="Seg_1420" s="T34">da</ta>
            <ta e="T36" id="Seg_1421" s="T35">miʔ</ta>
            <ta e="T37" id="Seg_1422" s="T36">Lutonʼuška</ta>
            <ta e="T38" id="Seg_1423" s="T37">bɨ</ta>
            <ta e="T39" id="Seg_1424" s="T38">ne-t</ta>
            <ta e="T40" id="Seg_1425" s="T39">i-bi</ta>
            <ta e="T41" id="Seg_1426" s="T40">i</ta>
            <ta e="T43" id="Seg_1427" s="T42">dĭ-zeŋ</ta>
            <ta e="T44" id="Seg_1428" s="T43">ešši</ta>
            <ta e="T45" id="Seg_1429" s="T44">i-bi</ta>
            <ta e="T46" id="Seg_1430" s="T45">i</ta>
            <ta e="T47" id="Seg_1431" s="T46">dön</ta>
            <ta e="T48" id="Seg_1432" s="T47">amno-bi</ta>
            <ta e="T49" id="Seg_1433" s="T48">a</ta>
            <ta e="T50" id="Seg_1434" s="T49">măn</ta>
            <ta e="T51" id="Seg_1435" s="T50">bɨ</ta>
            <ta e="T53" id="Seg_1436" s="T52">kuʔ-pio-m</ta>
            <ta e="T54" id="Seg_1437" s="T53">pa-zʼiʔ</ta>
            <ta e="T55" id="Seg_1438" s="T54">dĭgəttə</ta>
            <ta e="T56" id="Seg_1439" s="T55">i</ta>
            <ta e="T57" id="Seg_1440" s="T56">büzʼe</ta>
            <ta e="T58" id="Seg_1441" s="T57">davaj</ta>
            <ta e="T59" id="Seg_1442" s="T58">kirgar-zittə</ta>
            <ta e="T60" id="Seg_1443" s="T59">dĭgəttə</ta>
            <ta e="T61" id="Seg_1444" s="T60">nʼi-t</ta>
            <ta e="T62" id="Seg_1445" s="T61">šo-bi</ta>
            <ta e="T63" id="Seg_1446" s="T62">ĭmbi</ta>
            <ta e="T64" id="Seg_1447" s="T63">kirgar-laʔbə-laʔ</ta>
            <ta e="T65" id="Seg_1448" s="T64">da</ta>
            <ta e="T66" id="Seg_1449" s="T65">tăn</ta>
            <ta e="T67" id="Seg_1450" s="T66">bɨ</ta>
            <ta e="T68" id="Seg_1451" s="T67">ne-l</ta>
            <ta e="T69" id="Seg_1452" s="T68">i-bi</ta>
            <ta e="T70" id="Seg_1453" s="T69">i</ta>
            <ta e="T71" id="Seg_1454" s="T70">ešši-l</ta>
            <ta e="T72" id="Seg_1455" s="T71">bɨ</ta>
            <ta e="T73" id="Seg_1456" s="T72">i-bi</ta>
            <ta e="T74" id="Seg_1457" s="T73">dön</ta>
            <ta e="T75" id="Seg_1458" s="T74">amno-bi</ta>
            <ta e="T76" id="Seg_1459" s="T75">a</ta>
            <ta e="T77" id="Seg_1460" s="T76">măn</ta>
            <ta e="T78" id="Seg_1461" s="T77">bɨ</ta>
            <ta e="T79" id="Seg_1462" s="T78">kut-lam-bia-m</ta>
            <ta e="T80" id="Seg_1463" s="T79">dĭ</ta>
            <ta e="T81" id="Seg_1464" s="T80">pa-zʼiʔ</ta>
            <ta e="T82" id="Seg_1465" s="T81">dĭgəttə</ta>
            <ta e="T83" id="Seg_1466" s="T82">dĭ</ta>
            <ta e="T84" id="Seg_1467" s="T83">măn-də</ta>
            <ta e="T85" id="Seg_1468" s="T84">üžü-bə</ta>
            <ta e="T86" id="Seg_1469" s="T85">šer-bi</ta>
            <ta e="T87" id="Seg_1470" s="T86">amno-gaʔ</ta>
            <ta e="T88" id="Seg_1471" s="T87">a</ta>
            <ta e="T89" id="Seg_1472" s="T88">măn</ta>
            <ta e="T220" id="Seg_1473" s="T89">kal-la</ta>
            <ta e="T90" id="Seg_1474" s="T220">dʼür-le-m</ta>
            <ta e="T91" id="Seg_1475" s="T90">ku-la-m</ta>
            <ta e="T92" id="Seg_1476" s="T91">dĭrgit</ta>
            <ta e="T93" id="Seg_1477" s="T92">kak</ta>
            <ta e="T94" id="Seg_1478" s="T93">šiʔ</ta>
            <ta e="T95" id="Seg_1479" s="T94">dăk</ta>
            <ta e="T96" id="Seg_1480" s="T95">par-la-m</ta>
            <ta e="T97" id="Seg_1481" s="T96">a</ta>
            <ta e="T98" id="Seg_1482" s="T97">ej</ta>
            <ta e="T99" id="Seg_1483" s="T98">ku-la-m</ta>
            <ta e="T100" id="Seg_1484" s="T99">dăk</ta>
            <ta e="T101" id="Seg_1485" s="T100">ej</ta>
            <ta e="T102" id="Seg_1486" s="T101">par-la-m</ta>
            <ta e="T103" id="Seg_1487" s="T102">dĭgəttə</ta>
            <ta e="T104" id="Seg_1488" s="T103">šonə-ga</ta>
            <ta e="T105" id="Seg_1489" s="T104">šonə-ga</ta>
            <ta e="T106" id="Seg_1490" s="T105">il</ta>
            <ta e="T107" id="Seg_1491" s="T106">tüžöj</ta>
            <ta e="T108" id="Seg_1492" s="T107">multʼa-nə</ta>
            <ta e="T109" id="Seg_1493" s="T108">bar</ta>
            <ta e="T110" id="Seg_1494" s="T109">amnol-laʔbə-ʔjə</ta>
            <ta e="T111" id="Seg_1495" s="T110">dĭ</ta>
            <ta e="T112" id="Seg_1496" s="T111">măn-də</ta>
            <ta e="T113" id="Seg_1497" s="T112">ĭmbi</ta>
            <ta e="T114" id="Seg_1498" s="T113">a-laʔbə-ʔjə</ta>
            <ta e="T115" id="Seg_1499" s="T114">a</ta>
            <ta e="T116" id="Seg_1500" s="T115">ku-io-l</ta>
            <ta e="T117" id="Seg_1501" s="T116">dĭn</ta>
            <ta e="T118" id="Seg_1502" s="T117">noʔ</ta>
            <ta e="T119" id="Seg_1503" s="T118">amno-laʔbə</ta>
            <ta e="T120" id="Seg_1504" s="T119">dĭ</ta>
            <ta e="T121" id="Seg_1505" s="T120">sʼa-bi</ta>
            <ta e="T122" id="Seg_1506" s="T121">noʔ</ta>
            <ta e="T123" id="Seg_1507" s="T122">nĭŋgə-bi</ta>
            <ta e="T124" id="Seg_1508" s="T123">tüžöj-də</ta>
            <ta e="T125" id="Seg_1509" s="T124">mĭ-bi</ta>
            <ta e="T126" id="Seg_1510" s="T125">amno-ʔ</ta>
            <ta e="T127" id="Seg_1511" s="T126">măn-zi</ta>
            <ta e="T128" id="Seg_1512" s="T127">dʼok</ta>
            <ta e="T129" id="Seg_1513" s="T128">ej</ta>
            <ta e="T130" id="Seg_1514" s="T129">amno-baʔ</ta>
            <ta e="T131" id="Seg_1515" s="T130">măn</ta>
            <ta e="T132" id="Seg_1516" s="T131">iššo</ta>
            <ta e="T133" id="Seg_1517" s="T132">dĭrgit</ta>
            <ta e="T134" id="Seg_1518" s="T133">il</ta>
            <ta e="T135" id="Seg_1519" s="T134">iʔgö</ta>
            <ta e="T137" id="Seg_1520" s="T136">ka-la-m</ta>
            <ta e="T138" id="Seg_1521" s="T137">kuŋge</ta>
            <ta e="T139" id="Seg_1522" s="T138">dĭgəttə</ta>
            <ta e="T140" id="Seg_1523" s="T139">šonə-ga</ta>
            <ta e="T141" id="Seg_1524" s="T140">šonə-ga</ta>
            <ta e="T142" id="Seg_1525" s="T141">iʔgö</ta>
            <ta e="T143" id="Seg_1526" s="T142">il</ta>
            <ta e="T144" id="Seg_1527" s="T143">nu-laʔbə</ta>
            <ta e="T145" id="Seg_1528" s="T144">i</ta>
            <ta e="T147" id="Seg_1529" s="T146">xămut</ta>
            <ta e="T148" id="Seg_1530" s="T147">ed-leʔbə</ta>
            <ta e="T149" id="Seg_1531" s="T148">ine-m</ta>
            <ta e="T150" id="Seg_1532" s="T149">xămut-tə</ta>
            <ta e="T151" id="Seg_1533" s="T150">sürer-laʔbə</ta>
            <ta e="T152" id="Seg_1534" s="T151">ĭmbi</ta>
            <ta e="T153" id="Seg_1535" s="T152">a-laʔbə-laʔ</ta>
            <ta e="T154" id="Seg_1536" s="T153">da</ta>
            <ta e="T155" id="Seg_1537" s="T154">ine-m</ta>
            <ta e="T156" id="Seg_1538" s="T155">ködər-zittə</ta>
            <ta e="T157" id="Seg_1539" s="T156">nada</ta>
            <ta e="T158" id="Seg_1540" s="T157">dĭ</ta>
            <ta e="T159" id="Seg_1541" s="T158">xămut-tə</ta>
            <ta e="T160" id="Seg_1542" s="T159">šer-bi</ta>
            <ta e="T161" id="Seg_1543" s="T160">ine-nə</ta>
            <ta e="T162" id="Seg_1544" s="T161">i</ta>
            <ta e="T163" id="Seg_1545" s="T162">ködər-bi</ta>
            <ta e="T164" id="Seg_1546" s="T163">amno-ʔ</ta>
            <ta e="T167" id="Seg_1547" s="T166">măn-ziʔ</ta>
            <ta e="T168" id="Seg_1548" s="T167">dʼok</ta>
            <ta e="T169" id="Seg_1549" s="T168">ej</ta>
            <ta e="T170" id="Seg_1550" s="T169">amno-ʔ</ta>
            <ta e="T171" id="Seg_1551" s="T170">kandə-ga</ta>
            <ta e="T172" id="Seg_1552" s="T171">kandə-ga</ta>
            <ta e="T173" id="Seg_1553" s="T172">šo-bi</ta>
            <ta e="T174" id="Seg_1554" s="T173">nüke-nə</ta>
            <ta e="T175" id="Seg_1555" s="T174">dĭ</ta>
            <ta e="T176" id="Seg_1556" s="T175">nüke</ta>
            <ta e="T177" id="Seg_1557" s="T176">mĭnzer-leʔbə</ta>
            <ta e="T178" id="Seg_1558" s="T177">kisʼelʼ-də</ta>
            <ta e="T179" id="Seg_1559" s="T178">i</ta>
            <ta e="T180" id="Seg_1560" s="T179">stol-də</ta>
            <ta e="T181" id="Seg_1561" s="T180">nu-l-bi</ta>
            <ta e="T182" id="Seg_1562" s="T181">i</ta>
            <ta e="T183" id="Seg_1563" s="T182">pogrep-tə</ta>
            <ta e="T184" id="Seg_1564" s="T183">mĭl-leʔbə</ta>
            <ta e="T185" id="Seg_1565" s="T184">oroma-t-siʔ</ta>
            <ta e="T186" id="Seg_1566" s="T185">a</ta>
            <ta e="T187" id="Seg_1567" s="T186">dĭ</ta>
            <ta e="T188" id="Seg_1568" s="T187">măn-də</ta>
            <ta e="T189" id="Seg_1569" s="T188">ĭmbi</ta>
            <ta e="T190" id="Seg_1570" s="T189">tažer-ia-l</ta>
            <ta e="T191" id="Seg_1571" s="T190">šamnak-siʔ</ta>
            <ta e="T192" id="Seg_1572" s="T191">ku-io-l</ta>
            <ta e="T193" id="Seg_1573" s="T192">döbər</ta>
            <ta e="T194" id="Seg_1574" s="T193">nada</ta>
            <ta e="T195" id="Seg_1575" s="T194">en-zittə</ta>
            <ta e="T196" id="Seg_1576" s="T195">kan-a-ʔ</ta>
            <ta e="T198" id="Seg_1577" s="T197">deʔ-tə</ta>
            <ta e="T199" id="Seg_1578" s="T198">da</ta>
            <ta e="T201" id="Seg_1579" s="T199">i</ta>
            <ta e="T202" id="Seg_1580" s="T201">dĭ</ta>
            <ta e="T203" id="Seg_1581" s="T202">kam-bi</ta>
            <ta e="T204" id="Seg_1582" s="T203">deʔ-tə</ta>
            <ta e="T205" id="Seg_1583" s="T204">amnəl-bi-ʔi</ta>
            <ta e="T206" id="Seg_1584" s="T205">Lutonʼuška</ta>
            <ta e="T207" id="Seg_1585" s="T206">am-bi</ta>
            <ta e="T208" id="Seg_1586" s="T207">i</ta>
            <ta e="T209" id="Seg_1587" s="T208">pălat-tə</ta>
            <ta e="T210" id="Seg_1588" s="T209">iʔbə-bi</ta>
            <ta e="T211" id="Seg_1589" s="T210">kunol-bi</ta>
            <ta e="T212" id="Seg_1590" s="T211">erte-n</ta>
            <ta e="T213" id="Seg_1591" s="T212">uʔbdə-bi</ta>
            <ta e="T214" id="Seg_1592" s="T213">i</ta>
            <ta e="T215" id="Seg_1593" s="T214">par-luʔ-pi</ta>
            <ta e="T216" id="Seg_1594" s="T215">aba-ndə</ta>
            <ta e="T217" id="Seg_1595" s="T216">da</ta>
            <ta e="T218" id="Seg_1596" s="T217">ia-ndə</ta>
            <ta e="T219" id="Seg_1597" s="T218">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1598" s="T0">amno-bi-jəʔ</ta>
            <ta e="T2" id="Seg_1599" s="T1">nüke</ta>
            <ta e="T3" id="Seg_1600" s="T2">büzʼe</ta>
            <ta e="T5" id="Seg_1601" s="T4">dĭ-zen</ta>
            <ta e="T6" id="Seg_1602" s="T5">nʼi-t</ta>
            <ta e="T7" id="Seg_1603" s="T6">i-bi</ta>
            <ta e="T8" id="Seg_1604" s="T7">kăštə-bi-jəʔ</ta>
            <ta e="T9" id="Seg_1605" s="T8">Lutonʼuška</ta>
            <ta e="T10" id="Seg_1606" s="T9">dĭ</ta>
            <ta e="T11" id="Seg_1607" s="T10">nüke</ta>
            <ta e="T12" id="Seg_1608" s="T11">pʼeːš-Tə</ta>
            <ta e="T13" id="Seg_1609" s="T12">pa-jəʔ</ta>
            <ta e="T14" id="Seg_1610" s="T13">hen-bi-jəʔ</ta>
            <ta e="T15" id="Seg_1611" s="T14">a</ta>
            <ta e="T16" id="Seg_1612" s="T15">nʼi</ta>
            <ta e="T17" id="Seg_1613" s="T16">da</ta>
            <ta e="T18" id="Seg_1614" s="T17">büzʼe</ta>
            <ta e="T19" id="Seg_1615" s="T18">nʼiʔnen</ta>
            <ta e="T20" id="Seg_1616" s="T19">ĭmbi=də</ta>
            <ta e="T21" id="Seg_1617" s="T20">a-bi-jəʔ</ta>
            <ta e="T22" id="Seg_1618" s="T21">dĭ</ta>
            <ta e="T23" id="Seg_1619" s="T22">nüke</ta>
            <ta e="T24" id="Seg_1620" s="T23">pa</ta>
            <ta e="T25" id="Seg_1621" s="T24">tʼo-Tə</ta>
            <ta e="T26" id="Seg_1622" s="T25">saʔmə-luʔbdə-bi</ta>
            <ta e="T27" id="Seg_1623" s="T26">davaj</ta>
            <ta e="T28" id="Seg_1624" s="T27">tʼor-zittə</ta>
            <ta e="T29" id="Seg_1625" s="T28">da</ta>
            <ta e="T30" id="Seg_1626" s="T29">kirgaːr-zittə</ta>
            <ta e="T31" id="Seg_1627" s="T30">büzʼe-t</ta>
            <ta e="T32" id="Seg_1628" s="T31">šo-bi</ta>
            <ta e="T33" id="Seg_1629" s="T32">ĭmbi</ta>
            <ta e="T34" id="Seg_1630" s="T33">tʼor-laʔbə-l</ta>
            <ta e="T35" id="Seg_1631" s="T34">da</ta>
            <ta e="T36" id="Seg_1632" s="T35">miʔ</ta>
            <ta e="T37" id="Seg_1633" s="T36">Lutonʼuška</ta>
            <ta e="T38" id="Seg_1634" s="T37">bɨ</ta>
            <ta e="T39" id="Seg_1635" s="T38">ne-t</ta>
            <ta e="T40" id="Seg_1636" s="T39">i-bi</ta>
            <ta e="T41" id="Seg_1637" s="T40">i</ta>
            <ta e="T43" id="Seg_1638" s="T42">dĭ-zAŋ</ta>
            <ta e="T44" id="Seg_1639" s="T43">ešši</ta>
            <ta e="T45" id="Seg_1640" s="T44">i-bi</ta>
            <ta e="T46" id="Seg_1641" s="T45">i</ta>
            <ta e="T47" id="Seg_1642" s="T46">dön</ta>
            <ta e="T48" id="Seg_1643" s="T47">amno-bi</ta>
            <ta e="T49" id="Seg_1644" s="T48">a</ta>
            <ta e="T50" id="Seg_1645" s="T49">măn</ta>
            <ta e="T51" id="Seg_1646" s="T50">bɨ</ta>
            <ta e="T53" id="Seg_1647" s="T52">kut-bi-m</ta>
            <ta e="T54" id="Seg_1648" s="T53">pa-ziʔ</ta>
            <ta e="T55" id="Seg_1649" s="T54">dĭgəttə</ta>
            <ta e="T56" id="Seg_1650" s="T55">i</ta>
            <ta e="T57" id="Seg_1651" s="T56">büzʼe</ta>
            <ta e="T58" id="Seg_1652" s="T57">davaj</ta>
            <ta e="T59" id="Seg_1653" s="T58">kirgaːr-zittə</ta>
            <ta e="T60" id="Seg_1654" s="T59">dĭgəttə</ta>
            <ta e="T61" id="Seg_1655" s="T60">nʼi-t</ta>
            <ta e="T62" id="Seg_1656" s="T61">šo-bi</ta>
            <ta e="T63" id="Seg_1657" s="T62">ĭmbi</ta>
            <ta e="T64" id="Seg_1658" s="T63">kirgaːr-laʔbə-lAʔ</ta>
            <ta e="T65" id="Seg_1659" s="T64">da</ta>
            <ta e="T66" id="Seg_1660" s="T65">tăn</ta>
            <ta e="T67" id="Seg_1661" s="T66">bɨ</ta>
            <ta e="T68" id="Seg_1662" s="T67">ne-l</ta>
            <ta e="T69" id="Seg_1663" s="T68">i-bi</ta>
            <ta e="T70" id="Seg_1664" s="T69">i</ta>
            <ta e="T71" id="Seg_1665" s="T70">ešši-l</ta>
            <ta e="T72" id="Seg_1666" s="T71">bɨ</ta>
            <ta e="T73" id="Seg_1667" s="T72">i-bi</ta>
            <ta e="T74" id="Seg_1668" s="T73">dön</ta>
            <ta e="T75" id="Seg_1669" s="T74">amno-bi</ta>
            <ta e="T76" id="Seg_1670" s="T75">a</ta>
            <ta e="T77" id="Seg_1671" s="T76">măn</ta>
            <ta e="T78" id="Seg_1672" s="T77">bɨ</ta>
            <ta e="T79" id="Seg_1673" s="T78">kut-laːm-bi-m</ta>
            <ta e="T80" id="Seg_1674" s="T79">dĭ</ta>
            <ta e="T81" id="Seg_1675" s="T80">pa-ziʔ</ta>
            <ta e="T82" id="Seg_1676" s="T81">dĭgəttə</ta>
            <ta e="T83" id="Seg_1677" s="T82">dĭ</ta>
            <ta e="T84" id="Seg_1678" s="T83">măn-ntə</ta>
            <ta e="T85" id="Seg_1679" s="T84">üžü-bə</ta>
            <ta e="T86" id="Seg_1680" s="T85">šer-bi</ta>
            <ta e="T87" id="Seg_1681" s="T86">amno-KAʔ</ta>
            <ta e="T88" id="Seg_1682" s="T87">a</ta>
            <ta e="T89" id="Seg_1683" s="T88">măn</ta>
            <ta e="T220" id="Seg_1684" s="T89">kan-lAʔ</ta>
            <ta e="T90" id="Seg_1685" s="T220">tʼür-lV-m</ta>
            <ta e="T91" id="Seg_1686" s="T90">ku-lV-m</ta>
            <ta e="T92" id="Seg_1687" s="T91">dĭrgit</ta>
            <ta e="T93" id="Seg_1688" s="T92">kak</ta>
            <ta e="T94" id="Seg_1689" s="T93">šiʔ</ta>
            <ta e="T95" id="Seg_1690" s="T94">tak</ta>
            <ta e="T96" id="Seg_1691" s="T95">par-lV-m</ta>
            <ta e="T97" id="Seg_1692" s="T96">a</ta>
            <ta e="T98" id="Seg_1693" s="T97">ej</ta>
            <ta e="T99" id="Seg_1694" s="T98">ku-lV-m</ta>
            <ta e="T100" id="Seg_1695" s="T99">tak</ta>
            <ta e="T101" id="Seg_1696" s="T100">ej</ta>
            <ta e="T102" id="Seg_1697" s="T101">par-lV-m</ta>
            <ta e="T103" id="Seg_1698" s="T102">dĭgəttə</ta>
            <ta e="T104" id="Seg_1699" s="T103">šonə-gA</ta>
            <ta e="T105" id="Seg_1700" s="T104">šonə-gA</ta>
            <ta e="T106" id="Seg_1701" s="T105">il</ta>
            <ta e="T107" id="Seg_1702" s="T106">tüžöj</ta>
            <ta e="T108" id="Seg_1703" s="T107">multʼa-Tə</ta>
            <ta e="T109" id="Seg_1704" s="T108">bar</ta>
            <ta e="T110" id="Seg_1705" s="T109">amnol-laʔbə-jəʔ</ta>
            <ta e="T111" id="Seg_1706" s="T110">dĭ</ta>
            <ta e="T112" id="Seg_1707" s="T111">măn-ntə</ta>
            <ta e="T113" id="Seg_1708" s="T112">ĭmbi</ta>
            <ta e="T114" id="Seg_1709" s="T113">a-laʔbə-jəʔ</ta>
            <ta e="T115" id="Seg_1710" s="T114">a</ta>
            <ta e="T116" id="Seg_1711" s="T115">ku-liA-l</ta>
            <ta e="T117" id="Seg_1712" s="T116">dĭn</ta>
            <ta e="T118" id="Seg_1713" s="T117">noʔ</ta>
            <ta e="T119" id="Seg_1714" s="T118">amno-laʔbə</ta>
            <ta e="T120" id="Seg_1715" s="T119">dĭ</ta>
            <ta e="T121" id="Seg_1716" s="T120">sʼa-bi</ta>
            <ta e="T122" id="Seg_1717" s="T121">noʔ</ta>
            <ta e="T123" id="Seg_1718" s="T122">nĭŋgə-bi</ta>
            <ta e="T124" id="Seg_1719" s="T123">tüžöj-Tə</ta>
            <ta e="T125" id="Seg_1720" s="T124">mĭ-bi</ta>
            <ta e="T126" id="Seg_1721" s="T125">amno-ʔ</ta>
            <ta e="T127" id="Seg_1722" s="T126">măn-ziʔ</ta>
            <ta e="T128" id="Seg_1723" s="T127">dʼok</ta>
            <ta e="T129" id="Seg_1724" s="T128">ej</ta>
            <ta e="T130" id="Seg_1725" s="T129">amno-bAʔ</ta>
            <ta e="T131" id="Seg_1726" s="T130">măn</ta>
            <ta e="T132" id="Seg_1727" s="T131">ĭššo</ta>
            <ta e="T133" id="Seg_1728" s="T132">dĭrgit</ta>
            <ta e="T134" id="Seg_1729" s="T133">il</ta>
            <ta e="T135" id="Seg_1730" s="T134">iʔgö</ta>
            <ta e="T137" id="Seg_1731" s="T136">kan-lV-m</ta>
            <ta e="T138" id="Seg_1732" s="T137">kuŋgə</ta>
            <ta e="T139" id="Seg_1733" s="T138">dĭgəttə</ta>
            <ta e="T140" id="Seg_1734" s="T139">šonə-gA</ta>
            <ta e="T141" id="Seg_1735" s="T140">šonə-gA</ta>
            <ta e="T142" id="Seg_1736" s="T141">iʔgö</ta>
            <ta e="T143" id="Seg_1737" s="T142">il</ta>
            <ta e="T144" id="Seg_1738" s="T143">nu-laʔbə</ta>
            <ta e="T145" id="Seg_1739" s="T144">i</ta>
            <ta e="T147" id="Seg_1740" s="T146">xămut</ta>
            <ta e="T148" id="Seg_1741" s="T147">edə-laʔbə</ta>
            <ta e="T149" id="Seg_1742" s="T148">ine-m</ta>
            <ta e="T150" id="Seg_1743" s="T149">xămut-Tə</ta>
            <ta e="T151" id="Seg_1744" s="T150">sürer-laʔbə</ta>
            <ta e="T152" id="Seg_1745" s="T151">ĭmbi</ta>
            <ta e="T153" id="Seg_1746" s="T152">a-laʔbə-lAʔ</ta>
            <ta e="T154" id="Seg_1747" s="T153">da</ta>
            <ta e="T155" id="Seg_1748" s="T154">ine-m</ta>
            <ta e="T156" id="Seg_1749" s="T155">ködər-zittə</ta>
            <ta e="T157" id="Seg_1750" s="T156">nadə</ta>
            <ta e="T158" id="Seg_1751" s="T157">dĭ</ta>
            <ta e="T159" id="Seg_1752" s="T158">xămut-də</ta>
            <ta e="T160" id="Seg_1753" s="T159">šer-bi</ta>
            <ta e="T161" id="Seg_1754" s="T160">ine-Tə</ta>
            <ta e="T162" id="Seg_1755" s="T161">i</ta>
            <ta e="T163" id="Seg_1756" s="T162">ködər-bi</ta>
            <ta e="T164" id="Seg_1757" s="T163">amno-ʔ</ta>
            <ta e="T167" id="Seg_1758" s="T166">măn-ziʔ</ta>
            <ta e="T168" id="Seg_1759" s="T167">dʼok</ta>
            <ta e="T169" id="Seg_1760" s="T168">ej</ta>
            <ta e="T170" id="Seg_1761" s="T169">amno-ʔ</ta>
            <ta e="T171" id="Seg_1762" s="T170">kandə-gA</ta>
            <ta e="T172" id="Seg_1763" s="T171">kandə-gA</ta>
            <ta e="T173" id="Seg_1764" s="T172">šo-bi</ta>
            <ta e="T174" id="Seg_1765" s="T173">nüke-Tə</ta>
            <ta e="T175" id="Seg_1766" s="T174">dĭ</ta>
            <ta e="T176" id="Seg_1767" s="T175">nüke</ta>
            <ta e="T177" id="Seg_1768" s="T176">mĭnzər-laʔbə</ta>
            <ta e="T178" id="Seg_1769" s="T177">kiselʼ-də</ta>
            <ta e="T179" id="Seg_1770" s="T178">i</ta>
            <ta e="T180" id="Seg_1771" s="T179">stol-də</ta>
            <ta e="T181" id="Seg_1772" s="T180">nu-lə-bi</ta>
            <ta e="T182" id="Seg_1773" s="T181">i</ta>
            <ta e="T183" id="Seg_1774" s="T182">pogrep-Tə</ta>
            <ta e="T184" id="Seg_1775" s="T183">mĭn-laʔbə</ta>
            <ta e="T185" id="Seg_1776" s="T184">oroma-t-ziʔ</ta>
            <ta e="T186" id="Seg_1777" s="T185">a</ta>
            <ta e="T187" id="Seg_1778" s="T186">dĭ</ta>
            <ta e="T188" id="Seg_1779" s="T187">măn-ntə</ta>
            <ta e="T189" id="Seg_1780" s="T188">ĭmbi</ta>
            <ta e="T190" id="Seg_1781" s="T189">tažor-liA-l</ta>
            <ta e="T191" id="Seg_1782" s="T190">šamnak-ziʔ</ta>
            <ta e="T192" id="Seg_1783" s="T191">ku-liA-l</ta>
            <ta e="T193" id="Seg_1784" s="T192">döbər</ta>
            <ta e="T194" id="Seg_1785" s="T193">nadə</ta>
            <ta e="T195" id="Seg_1786" s="T194">hen-zittə</ta>
            <ta e="T196" id="Seg_1787" s="T195">kan-ə-ʔ</ta>
            <ta e="T198" id="Seg_1788" s="T197">det-t</ta>
            <ta e="T199" id="Seg_1789" s="T198">da</ta>
            <ta e="T201" id="Seg_1790" s="T199">i</ta>
            <ta e="T202" id="Seg_1791" s="T201">dĭ</ta>
            <ta e="T203" id="Seg_1792" s="T202">kan-bi</ta>
            <ta e="T204" id="Seg_1793" s="T203">det-t</ta>
            <ta e="T205" id="Seg_1794" s="T204">amnəl-bi-jəʔ</ta>
            <ta e="T206" id="Seg_1795" s="T205">Lutonʼuška</ta>
            <ta e="T207" id="Seg_1796" s="T206">am-bi</ta>
            <ta e="T208" id="Seg_1797" s="T207">i</ta>
            <ta e="T209" id="Seg_1798" s="T208">pălat-Tə</ta>
            <ta e="T210" id="Seg_1799" s="T209">iʔbə-bi</ta>
            <ta e="T211" id="Seg_1800" s="T210">kunol-bi</ta>
            <ta e="T212" id="Seg_1801" s="T211">ertə-n</ta>
            <ta e="T213" id="Seg_1802" s="T212">uʔbdə-bi</ta>
            <ta e="T214" id="Seg_1803" s="T213">i</ta>
            <ta e="T215" id="Seg_1804" s="T214">par-luʔbdə-bi</ta>
            <ta e="T216" id="Seg_1805" s="T215">aba-gəndə</ta>
            <ta e="T217" id="Seg_1806" s="T216">da</ta>
            <ta e="T218" id="Seg_1807" s="T217">ija-gəndə</ta>
            <ta e="T219" id="Seg_1808" s="T218">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1809" s="T0">live-PST-3PL</ta>
            <ta e="T2" id="Seg_1810" s="T1">woman.[NOM.SG]</ta>
            <ta e="T3" id="Seg_1811" s="T2">man.[NOM.SG]</ta>
            <ta e="T5" id="Seg_1812" s="T4">this-GEN.PL</ta>
            <ta e="T6" id="Seg_1813" s="T5">son-NOM/GEN.3SG</ta>
            <ta e="T7" id="Seg_1814" s="T6">be-PST.[3SG]</ta>
            <ta e="T8" id="Seg_1815" s="T7">call-PST-3PL</ta>
            <ta e="T9" id="Seg_1816" s="T8">Lutonyushka.[NOM.SG]</ta>
            <ta e="T10" id="Seg_1817" s="T9">this.[NOM.SG]</ta>
            <ta e="T11" id="Seg_1818" s="T10">woman.[NOM.SG]</ta>
            <ta e="T12" id="Seg_1819" s="T11">stove-LAT</ta>
            <ta e="T13" id="Seg_1820" s="T12">tree-PL</ta>
            <ta e="T14" id="Seg_1821" s="T13">put-PST-3PL</ta>
            <ta e="T15" id="Seg_1822" s="T14">and</ta>
            <ta e="T16" id="Seg_1823" s="T15">boy.[NOM.SG]</ta>
            <ta e="T17" id="Seg_1824" s="T16">and</ta>
            <ta e="T18" id="Seg_1825" s="T17">man.[NOM.SG]</ta>
            <ta e="T19" id="Seg_1826" s="T18">outside</ta>
            <ta e="T20" id="Seg_1827" s="T19">what.[NOM.SG]=INDEF</ta>
            <ta e="T21" id="Seg_1828" s="T20">make-PST-3PL</ta>
            <ta e="T22" id="Seg_1829" s="T21">this.[NOM.SG]</ta>
            <ta e="T23" id="Seg_1830" s="T22">woman.[NOM.SG]</ta>
            <ta e="T24" id="Seg_1831" s="T23">tree.[NOM.SG]</ta>
            <ta e="T25" id="Seg_1832" s="T24">place-LAT</ta>
            <ta e="T26" id="Seg_1833" s="T25">fall-MOM-PST.[3SG]</ta>
            <ta e="T27" id="Seg_1834" s="T26">INCH</ta>
            <ta e="T28" id="Seg_1835" s="T27">cry-INF.LAT</ta>
            <ta e="T29" id="Seg_1836" s="T28">and</ta>
            <ta e="T30" id="Seg_1837" s="T29">shout-INF.LAT</ta>
            <ta e="T31" id="Seg_1838" s="T30">man-NOM/GEN.3SG</ta>
            <ta e="T32" id="Seg_1839" s="T31">come-PST.[3SG]</ta>
            <ta e="T33" id="Seg_1840" s="T32">what.[NOM.SG]</ta>
            <ta e="T34" id="Seg_1841" s="T33">cry-DUR-2SG</ta>
            <ta e="T35" id="Seg_1842" s="T34">because</ta>
            <ta e="T36" id="Seg_1843" s="T35">we.NOM</ta>
            <ta e="T37" id="Seg_1844" s="T36">Lutonyushka.[NOM.SG]</ta>
            <ta e="T38" id="Seg_1845" s="T37">IRREAL</ta>
            <ta e="T39" id="Seg_1846" s="T38">woman-NOM/GEN.3SG</ta>
            <ta e="T40" id="Seg_1847" s="T39">be-PST.[3SG]</ta>
            <ta e="T41" id="Seg_1848" s="T40">and</ta>
            <ta e="T43" id="Seg_1849" s="T42">this-PL</ta>
            <ta e="T44" id="Seg_1850" s="T43">child.[NOM.SG]</ta>
            <ta e="T45" id="Seg_1851" s="T44">be-PST.[3SG]</ta>
            <ta e="T46" id="Seg_1852" s="T45">and</ta>
            <ta e="T47" id="Seg_1853" s="T46">here</ta>
            <ta e="T48" id="Seg_1854" s="T47">sit-PST.[3SG]</ta>
            <ta e="T49" id="Seg_1855" s="T48">and</ta>
            <ta e="T50" id="Seg_1856" s="T49">I.NOM</ta>
            <ta e="T51" id="Seg_1857" s="T50">IRREAL</ta>
            <ta e="T53" id="Seg_1858" s="T52">kill-PST-1SG</ta>
            <ta e="T54" id="Seg_1859" s="T53">tree-INS</ta>
            <ta e="T55" id="Seg_1860" s="T54">then</ta>
            <ta e="T56" id="Seg_1861" s="T55">and</ta>
            <ta e="T57" id="Seg_1862" s="T56">man.[NOM.SG]</ta>
            <ta e="T58" id="Seg_1863" s="T57">INCH</ta>
            <ta e="T59" id="Seg_1864" s="T58">shout-INF.LAT</ta>
            <ta e="T60" id="Seg_1865" s="T59">then</ta>
            <ta e="T61" id="Seg_1866" s="T60">son-NOM/GEN.3SG</ta>
            <ta e="T62" id="Seg_1867" s="T61">come-PST.[3SG]</ta>
            <ta e="T63" id="Seg_1868" s="T62">what.[NOM.SG]</ta>
            <ta e="T64" id="Seg_1869" s="T63">shout-DUR-2PL</ta>
            <ta e="T65" id="Seg_1870" s="T64">because</ta>
            <ta e="T66" id="Seg_1871" s="T65">you.NOM</ta>
            <ta e="T67" id="Seg_1872" s="T66">IRREAL</ta>
            <ta e="T68" id="Seg_1873" s="T67">woman-NOM/GEN/ACC.2SG</ta>
            <ta e="T69" id="Seg_1874" s="T68">be-PST.[3SG]</ta>
            <ta e="T70" id="Seg_1875" s="T69">and</ta>
            <ta e="T71" id="Seg_1876" s="T70">child-NOM/GEN/ACC.2SG</ta>
            <ta e="T72" id="Seg_1877" s="T71">IRREAL</ta>
            <ta e="T73" id="Seg_1878" s="T72">be-PST.[3SG]</ta>
            <ta e="T74" id="Seg_1879" s="T73">here</ta>
            <ta e="T75" id="Seg_1880" s="T74">sit-PST.[3SG]</ta>
            <ta e="T76" id="Seg_1881" s="T75">and</ta>
            <ta e="T77" id="Seg_1882" s="T76">I.NOM</ta>
            <ta e="T78" id="Seg_1883" s="T77">IRREAL</ta>
            <ta e="T79" id="Seg_1884" s="T78">kill-RES-PST-1SG</ta>
            <ta e="T80" id="Seg_1885" s="T79">this.[NOM.SG]</ta>
            <ta e="T81" id="Seg_1886" s="T80">tree-INS</ta>
            <ta e="T82" id="Seg_1887" s="T81">then</ta>
            <ta e="T83" id="Seg_1888" s="T82">this.[NOM.SG]</ta>
            <ta e="T84" id="Seg_1889" s="T83">say-IPFVZ.[3SG]</ta>
            <ta e="T85" id="Seg_1890" s="T84">cap-ACC.3SG</ta>
            <ta e="T86" id="Seg_1891" s="T85">dress-PST.[3SG]</ta>
            <ta e="T87" id="Seg_1892" s="T86">live-IMP.2PL</ta>
            <ta e="T88" id="Seg_1893" s="T87">and</ta>
            <ta e="T89" id="Seg_1894" s="T88">I.NOM</ta>
            <ta e="T220" id="Seg_1895" s="T89">go-CVB</ta>
            <ta e="T90" id="Seg_1896" s="T220">disappear-FUT-1SG</ta>
            <ta e="T91" id="Seg_1897" s="T90">find-FUT-1SG</ta>
            <ta e="T92" id="Seg_1898" s="T91">such.[NOM.SG]</ta>
            <ta e="T93" id="Seg_1899" s="T92">like</ta>
            <ta e="T94" id="Seg_1900" s="T93">you.PL.NOM</ta>
            <ta e="T95" id="Seg_1901" s="T94">so</ta>
            <ta e="T96" id="Seg_1902" s="T95">return-FUT-1SG</ta>
            <ta e="T97" id="Seg_1903" s="T96">and</ta>
            <ta e="T98" id="Seg_1904" s="T97">NEG</ta>
            <ta e="T99" id="Seg_1905" s="T98">find-FUT-1SG</ta>
            <ta e="T100" id="Seg_1906" s="T99">so</ta>
            <ta e="T101" id="Seg_1907" s="T100">NEG</ta>
            <ta e="T102" id="Seg_1908" s="T101">return-FUT-1SG</ta>
            <ta e="T103" id="Seg_1909" s="T102">then</ta>
            <ta e="T104" id="Seg_1910" s="T103">come-PRS.[3SG]</ta>
            <ta e="T105" id="Seg_1911" s="T104">come-PRS.[3SG]</ta>
            <ta e="T106" id="Seg_1912" s="T105">people.[NOM.SG]</ta>
            <ta e="T107" id="Seg_1913" s="T106">cow.[NOM.SG]</ta>
            <ta e="T108" id="Seg_1914" s="T107">sauna-LAT</ta>
            <ta e="T109" id="Seg_1915" s="T108">PTCL</ta>
            <ta e="T110" id="Seg_1916" s="T109">seat-DUR-3PL</ta>
            <ta e="T111" id="Seg_1917" s="T110">this.[NOM.SG]</ta>
            <ta e="T112" id="Seg_1918" s="T111">say-IPFVZ.[3SG]</ta>
            <ta e="T113" id="Seg_1919" s="T112">what.[NOM.SG]</ta>
            <ta e="T114" id="Seg_1920" s="T113">make-DUR-3PL</ta>
            <ta e="T115" id="Seg_1921" s="T114">and</ta>
            <ta e="T116" id="Seg_1922" s="T115">see-PRS-2SG</ta>
            <ta e="T117" id="Seg_1923" s="T116">there</ta>
            <ta e="T118" id="Seg_1924" s="T117">grass.[NOM.SG]</ta>
            <ta e="T119" id="Seg_1925" s="T118">live-DUR.[3SG]</ta>
            <ta e="T120" id="Seg_1926" s="T119">this.[NOM.SG]</ta>
            <ta e="T121" id="Seg_1927" s="T120">climb-PST.[3SG]</ta>
            <ta e="T122" id="Seg_1928" s="T121">grass.[NOM.SG]</ta>
            <ta e="T123" id="Seg_1929" s="T122">tear-PST.[3SG]</ta>
            <ta e="T124" id="Seg_1930" s="T123">cow-LAT</ta>
            <ta e="T125" id="Seg_1931" s="T124">give-PST.[3SG]</ta>
            <ta e="T126" id="Seg_1932" s="T125">live-IMP.2SG</ta>
            <ta e="T127" id="Seg_1933" s="T126">I.NOM-COM</ta>
            <ta e="T128" id="Seg_1934" s="T127">no</ta>
            <ta e="T129" id="Seg_1935" s="T128">NEG</ta>
            <ta e="T130" id="Seg_1936" s="T129">live-1PL</ta>
            <ta e="T131" id="Seg_1937" s="T130">I.NOM</ta>
            <ta e="T132" id="Seg_1938" s="T131">more</ta>
            <ta e="T133" id="Seg_1939" s="T132">such.[NOM.SG]</ta>
            <ta e="T134" id="Seg_1940" s="T133">people.[NOM.SG]</ta>
            <ta e="T135" id="Seg_1941" s="T134">many</ta>
            <ta e="T137" id="Seg_1942" s="T136">go-FUT-1SG</ta>
            <ta e="T138" id="Seg_1943" s="T137">far</ta>
            <ta e="T139" id="Seg_1944" s="T138">then</ta>
            <ta e="T140" id="Seg_1945" s="T139">come-PRS.[3SG]</ta>
            <ta e="T141" id="Seg_1946" s="T140">come-PRS.[3SG]</ta>
            <ta e="T142" id="Seg_1947" s="T141">many</ta>
            <ta e="T143" id="Seg_1948" s="T142">people.[NOM.SG]</ta>
            <ta e="T144" id="Seg_1949" s="T143">stand-DUR.[3SG]</ta>
            <ta e="T145" id="Seg_1950" s="T144">and</ta>
            <ta e="T147" id="Seg_1951" s="T146">horse.collar.[NOM.SG]</ta>
            <ta e="T148" id="Seg_1952" s="T147">hang.up-DUR.[3SG]</ta>
            <ta e="T149" id="Seg_1953" s="T148">horse-ACC</ta>
            <ta e="T150" id="Seg_1954" s="T149">horse.collar-LAT</ta>
            <ta e="T151" id="Seg_1955" s="T150">drive-DUR.[3SG]</ta>
            <ta e="T152" id="Seg_1956" s="T151">what.[NOM.SG]</ta>
            <ta e="T153" id="Seg_1957" s="T152">make-DUR-2PL</ta>
            <ta e="T154" id="Seg_1958" s="T153">and</ta>
            <ta e="T155" id="Seg_1959" s="T154">horse-ACC</ta>
            <ta e="T156" id="Seg_1960" s="T155">untie-INF.LAT</ta>
            <ta e="T157" id="Seg_1961" s="T156">one.should</ta>
            <ta e="T158" id="Seg_1962" s="T157">this.[NOM.SG]</ta>
            <ta e="T159" id="Seg_1963" s="T158">horse.collar-NOM/GEN/ACC.3SG</ta>
            <ta e="T160" id="Seg_1964" s="T159">dress-PST.[3SG]</ta>
            <ta e="T161" id="Seg_1965" s="T160">horse-LAT</ta>
            <ta e="T162" id="Seg_1966" s="T161">and</ta>
            <ta e="T163" id="Seg_1967" s="T162">untie-PST.[3SG]</ta>
            <ta e="T164" id="Seg_1968" s="T163">live-IMP.2SG</ta>
            <ta e="T167" id="Seg_1969" s="T166">I.NOM-COM</ta>
            <ta e="T168" id="Seg_1970" s="T167">no</ta>
            <ta e="T169" id="Seg_1971" s="T168">NEG</ta>
            <ta e="T170" id="Seg_1972" s="T169">live-CNG</ta>
            <ta e="T171" id="Seg_1973" s="T170">walk-PRS.[3SG]</ta>
            <ta e="T172" id="Seg_1974" s="T171">walk-PRS.[3SG]</ta>
            <ta e="T173" id="Seg_1975" s="T172">come-PST.[3SG]</ta>
            <ta e="T174" id="Seg_1976" s="T173">woman-LAT</ta>
            <ta e="T175" id="Seg_1977" s="T174">this.[NOM.SG]</ta>
            <ta e="T176" id="Seg_1978" s="T175">woman.[NOM.SG]</ta>
            <ta e="T177" id="Seg_1979" s="T176">boil-DUR.[3SG]</ta>
            <ta e="T178" id="Seg_1980" s="T177">kissel-NOM/GEN/ACC.3SG</ta>
            <ta e="T179" id="Seg_1981" s="T178">and</ta>
            <ta e="T180" id="Seg_1982" s="T179">table-NOM/GEN/ACC.3SG</ta>
            <ta e="T181" id="Seg_1983" s="T180">stand-TR-PST.[3SG]</ta>
            <ta e="T182" id="Seg_1984" s="T181">and</ta>
            <ta e="T183" id="Seg_1985" s="T182">cellar-LAT</ta>
            <ta e="T184" id="Seg_1986" s="T183">go-DUR.[3SG]</ta>
            <ta e="T185" id="Seg_1987" s="T184">cream-3SG-INS</ta>
            <ta e="T186" id="Seg_1988" s="T185">and</ta>
            <ta e="T187" id="Seg_1989" s="T186">this.[NOM.SG]</ta>
            <ta e="T188" id="Seg_1990" s="T187">say-IPFVZ.[3SG]</ta>
            <ta e="T189" id="Seg_1991" s="T188">what.[NOM.SG]</ta>
            <ta e="T190" id="Seg_1992" s="T189">carry-PRS-2SG</ta>
            <ta e="T191" id="Seg_1993" s="T190">spoon-INS</ta>
            <ta e="T192" id="Seg_1994" s="T191">see-PRS-2SG</ta>
            <ta e="T193" id="Seg_1995" s="T192">here</ta>
            <ta e="T194" id="Seg_1996" s="T193">one.should</ta>
            <ta e="T195" id="Seg_1997" s="T194">put-INF.LAT</ta>
            <ta e="T196" id="Seg_1998" s="T195">go-EP-IMP.2SG</ta>
            <ta e="T198" id="Seg_1999" s="T197">bring-IMP.2SG.O</ta>
            <ta e="T199" id="Seg_2000" s="T198">and</ta>
            <ta e="T201" id="Seg_2001" s="T199">and</ta>
            <ta e="T202" id="Seg_2002" s="T201">this.[NOM.SG]</ta>
            <ta e="T203" id="Seg_2003" s="T202">go-PST.[3SG]</ta>
            <ta e="T204" id="Seg_2004" s="T203">bring-IMP.2SG.O</ta>
            <ta e="T205" id="Seg_2005" s="T204">sit.down-PST-3PL</ta>
            <ta e="T206" id="Seg_2006" s="T205">Lutonyushka.[NOM.SG]</ta>
            <ta e="T207" id="Seg_2007" s="T206">eat-PST.[3SG]</ta>
            <ta e="T208" id="Seg_2008" s="T207">and</ta>
            <ta e="T209" id="Seg_2009" s="T208">bed-LAT</ta>
            <ta e="T210" id="Seg_2010" s="T209">lie.down-PST.[3SG]</ta>
            <ta e="T211" id="Seg_2011" s="T210">sleep-PST.[3SG]</ta>
            <ta e="T212" id="Seg_2012" s="T211">morning-LOC.ADV</ta>
            <ta e="T213" id="Seg_2013" s="T212">get.up-PST.[3SG]</ta>
            <ta e="T214" id="Seg_2014" s="T213">and</ta>
            <ta e="T215" id="Seg_2015" s="T214">return-MOM-PST.[3SG]</ta>
            <ta e="T216" id="Seg_2016" s="T215">father-LAT/LOC.3SG</ta>
            <ta e="T217" id="Seg_2017" s="T216">and</ta>
            <ta e="T218" id="Seg_2018" s="T217">mother-LAT/LOC.3SG</ta>
            <ta e="T219" id="Seg_2019" s="T218">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_2020" s="T0">жить-PST-3PL</ta>
            <ta e="T2" id="Seg_2021" s="T1">женщина.[NOM.SG]</ta>
            <ta e="T3" id="Seg_2022" s="T2">мужчина.[NOM.SG]</ta>
            <ta e="T5" id="Seg_2023" s="T4">этот-GEN.PL</ta>
            <ta e="T6" id="Seg_2024" s="T5">сын-NOM/GEN.3SG</ta>
            <ta e="T7" id="Seg_2025" s="T6">быть-PST.[3SG]</ta>
            <ta e="T8" id="Seg_2026" s="T7">позвать-PST-3PL</ta>
            <ta e="T9" id="Seg_2027" s="T8">Лутонюшка.[NOM.SG]</ta>
            <ta e="T10" id="Seg_2028" s="T9">этот.[NOM.SG]</ta>
            <ta e="T11" id="Seg_2029" s="T10">женщина.[NOM.SG]</ta>
            <ta e="T12" id="Seg_2030" s="T11">печь-LAT</ta>
            <ta e="T13" id="Seg_2031" s="T12">дерево-PL</ta>
            <ta e="T14" id="Seg_2032" s="T13">класть-PST-3PL</ta>
            <ta e="T15" id="Seg_2033" s="T14">а</ta>
            <ta e="T16" id="Seg_2034" s="T15">мальчик.[NOM.SG]</ta>
            <ta e="T17" id="Seg_2035" s="T16">и</ta>
            <ta e="T18" id="Seg_2036" s="T17">мужчина.[NOM.SG]</ta>
            <ta e="T19" id="Seg_2037" s="T18">снаружи</ta>
            <ta e="T20" id="Seg_2038" s="T19">что.[NOM.SG]=INDEF</ta>
            <ta e="T21" id="Seg_2039" s="T20">делать-PST-3PL</ta>
            <ta e="T22" id="Seg_2040" s="T21">этот.[NOM.SG]</ta>
            <ta e="T23" id="Seg_2041" s="T22">женщина.[NOM.SG]</ta>
            <ta e="T24" id="Seg_2042" s="T23">дерево.[NOM.SG]</ta>
            <ta e="T25" id="Seg_2043" s="T24">место-LAT</ta>
            <ta e="T26" id="Seg_2044" s="T25">упасть-MOM-PST.[3SG]</ta>
            <ta e="T27" id="Seg_2045" s="T26">INCH</ta>
            <ta e="T28" id="Seg_2046" s="T27">плакать-INF.LAT</ta>
            <ta e="T29" id="Seg_2047" s="T28">и</ta>
            <ta e="T30" id="Seg_2048" s="T29">кричать-INF.LAT</ta>
            <ta e="T31" id="Seg_2049" s="T30">мужчина-NOM/GEN.3SG</ta>
            <ta e="T32" id="Seg_2050" s="T31">прийти-PST.[3SG]</ta>
            <ta e="T33" id="Seg_2051" s="T32">что.[NOM.SG]</ta>
            <ta e="T34" id="Seg_2052" s="T33">плакать-DUR-2SG</ta>
            <ta e="T35" id="Seg_2053" s="T34">да</ta>
            <ta e="T36" id="Seg_2054" s="T35">мы.NOM</ta>
            <ta e="T37" id="Seg_2055" s="T36">Лутонюшка.[NOM.SG]</ta>
            <ta e="T38" id="Seg_2056" s="T37">IRREAL</ta>
            <ta e="T39" id="Seg_2057" s="T38">женщина-NOM/GEN.3SG</ta>
            <ta e="T40" id="Seg_2058" s="T39">быть-PST.[3SG]</ta>
            <ta e="T41" id="Seg_2059" s="T40">и</ta>
            <ta e="T43" id="Seg_2060" s="T42">этот-PL</ta>
            <ta e="T44" id="Seg_2061" s="T43">ребенок.[NOM.SG]</ta>
            <ta e="T45" id="Seg_2062" s="T44">быть-PST.[3SG]</ta>
            <ta e="T46" id="Seg_2063" s="T45">и</ta>
            <ta e="T47" id="Seg_2064" s="T46">здесь</ta>
            <ta e="T48" id="Seg_2065" s="T47">сидеть-PST.[3SG]</ta>
            <ta e="T49" id="Seg_2066" s="T48">а</ta>
            <ta e="T50" id="Seg_2067" s="T49">я.NOM</ta>
            <ta e="T51" id="Seg_2068" s="T50">IRREAL</ta>
            <ta e="T53" id="Seg_2069" s="T52">убить-PST-1SG</ta>
            <ta e="T54" id="Seg_2070" s="T53">дерево-INS</ta>
            <ta e="T55" id="Seg_2071" s="T54">тогда</ta>
            <ta e="T56" id="Seg_2072" s="T55">и</ta>
            <ta e="T57" id="Seg_2073" s="T56">мужчина.[NOM.SG]</ta>
            <ta e="T58" id="Seg_2074" s="T57">INCH</ta>
            <ta e="T59" id="Seg_2075" s="T58">кричать-INF.LAT</ta>
            <ta e="T60" id="Seg_2076" s="T59">тогда</ta>
            <ta e="T61" id="Seg_2077" s="T60">сын-NOM/GEN.3SG</ta>
            <ta e="T62" id="Seg_2078" s="T61">прийти-PST.[3SG]</ta>
            <ta e="T63" id="Seg_2079" s="T62">что.[NOM.SG]</ta>
            <ta e="T64" id="Seg_2080" s="T63">кричать-DUR-2PL</ta>
            <ta e="T65" id="Seg_2081" s="T64">да</ta>
            <ta e="T66" id="Seg_2082" s="T65">ты.NOM</ta>
            <ta e="T67" id="Seg_2083" s="T66">IRREAL</ta>
            <ta e="T68" id="Seg_2084" s="T67">женщина-NOM/GEN/ACC.2SG</ta>
            <ta e="T69" id="Seg_2085" s="T68">быть-PST.[3SG]</ta>
            <ta e="T70" id="Seg_2086" s="T69">и</ta>
            <ta e="T71" id="Seg_2087" s="T70">ребенок-NOM/GEN/ACC.2SG</ta>
            <ta e="T72" id="Seg_2088" s="T71">IRREAL</ta>
            <ta e="T73" id="Seg_2089" s="T72">быть-PST.[3SG]</ta>
            <ta e="T74" id="Seg_2090" s="T73">здесь</ta>
            <ta e="T75" id="Seg_2091" s="T74">сидеть-PST.[3SG]</ta>
            <ta e="T76" id="Seg_2092" s="T75">а</ta>
            <ta e="T77" id="Seg_2093" s="T76">я.NOM</ta>
            <ta e="T78" id="Seg_2094" s="T77">IRREAL</ta>
            <ta e="T79" id="Seg_2095" s="T78">убить-RES-PST-1SG</ta>
            <ta e="T80" id="Seg_2096" s="T79">этот.[NOM.SG]</ta>
            <ta e="T81" id="Seg_2097" s="T80">дерево-INS</ta>
            <ta e="T82" id="Seg_2098" s="T81">тогда</ta>
            <ta e="T83" id="Seg_2099" s="T82">этот.[NOM.SG]</ta>
            <ta e="T84" id="Seg_2100" s="T83">сказать-IPFVZ.[3SG]</ta>
            <ta e="T85" id="Seg_2101" s="T84">шапка-ACC.3SG</ta>
            <ta e="T86" id="Seg_2102" s="T85">надеть-PST.[3SG]</ta>
            <ta e="T87" id="Seg_2103" s="T86">жить-IMP.2PL</ta>
            <ta e="T88" id="Seg_2104" s="T87">а</ta>
            <ta e="T89" id="Seg_2105" s="T88">я.NOM</ta>
            <ta e="T220" id="Seg_2106" s="T89">пойти-CVB</ta>
            <ta e="T90" id="Seg_2107" s="T220">исчезнуть-FUT-1SG</ta>
            <ta e="T91" id="Seg_2108" s="T90">найти-FUT-1SG</ta>
            <ta e="T92" id="Seg_2109" s="T91">такой.[NOM.SG]</ta>
            <ta e="T93" id="Seg_2110" s="T92">как</ta>
            <ta e="T94" id="Seg_2111" s="T93">вы.NOM</ta>
            <ta e="T95" id="Seg_2112" s="T94">так</ta>
            <ta e="T96" id="Seg_2113" s="T95">вернуться-FUT-1SG</ta>
            <ta e="T97" id="Seg_2114" s="T96">а</ta>
            <ta e="T98" id="Seg_2115" s="T97">NEG</ta>
            <ta e="T99" id="Seg_2116" s="T98">найти-FUT-1SG</ta>
            <ta e="T100" id="Seg_2117" s="T99">так</ta>
            <ta e="T101" id="Seg_2118" s="T100">NEG</ta>
            <ta e="T102" id="Seg_2119" s="T101">вернуться-FUT-1SG</ta>
            <ta e="T103" id="Seg_2120" s="T102">тогда</ta>
            <ta e="T104" id="Seg_2121" s="T103">прийти-PRS.[3SG]</ta>
            <ta e="T105" id="Seg_2122" s="T104">прийти-PRS.[3SG]</ta>
            <ta e="T106" id="Seg_2123" s="T105">люди.[NOM.SG]</ta>
            <ta e="T107" id="Seg_2124" s="T106">корова.[NOM.SG]</ta>
            <ta e="T108" id="Seg_2125" s="T107">баня-LAT</ta>
            <ta e="T109" id="Seg_2126" s="T108">PTCL</ta>
            <ta e="T110" id="Seg_2127" s="T109">сажать-DUR-3PL</ta>
            <ta e="T111" id="Seg_2128" s="T110">этот.[NOM.SG]</ta>
            <ta e="T112" id="Seg_2129" s="T111">сказать-IPFVZ.[3SG]</ta>
            <ta e="T113" id="Seg_2130" s="T112">что.[NOM.SG]</ta>
            <ta e="T114" id="Seg_2131" s="T113">делать-DUR-3PL</ta>
            <ta e="T115" id="Seg_2132" s="T114">а</ta>
            <ta e="T116" id="Seg_2133" s="T115">видеть-PRS-2SG</ta>
            <ta e="T117" id="Seg_2134" s="T116">там</ta>
            <ta e="T118" id="Seg_2135" s="T117">трава.[NOM.SG]</ta>
            <ta e="T119" id="Seg_2136" s="T118">жить-DUR.[3SG]</ta>
            <ta e="T120" id="Seg_2137" s="T119">этот.[NOM.SG]</ta>
            <ta e="T121" id="Seg_2138" s="T120">влезать-PST.[3SG]</ta>
            <ta e="T122" id="Seg_2139" s="T121">трава.[NOM.SG]</ta>
            <ta e="T123" id="Seg_2140" s="T122">рвать-PST.[3SG]</ta>
            <ta e="T124" id="Seg_2141" s="T123">корова-LAT</ta>
            <ta e="T125" id="Seg_2142" s="T124">дать-PST.[3SG]</ta>
            <ta e="T126" id="Seg_2143" s="T125">жить-IMP.2SG</ta>
            <ta e="T127" id="Seg_2144" s="T126">я.NOM-COM</ta>
            <ta e="T128" id="Seg_2145" s="T127">нет</ta>
            <ta e="T129" id="Seg_2146" s="T128">NEG</ta>
            <ta e="T130" id="Seg_2147" s="T129">жить-1PL</ta>
            <ta e="T131" id="Seg_2148" s="T130">я.NOM</ta>
            <ta e="T132" id="Seg_2149" s="T131">еще</ta>
            <ta e="T133" id="Seg_2150" s="T132">такой.[NOM.SG]</ta>
            <ta e="T134" id="Seg_2151" s="T133">люди.[NOM.SG]</ta>
            <ta e="T135" id="Seg_2152" s="T134">много</ta>
            <ta e="T137" id="Seg_2153" s="T136">пойти-FUT-1SG</ta>
            <ta e="T138" id="Seg_2154" s="T137">далеко</ta>
            <ta e="T139" id="Seg_2155" s="T138">тогда</ta>
            <ta e="T140" id="Seg_2156" s="T139">прийти-PRS.[3SG]</ta>
            <ta e="T141" id="Seg_2157" s="T140">прийти-PRS.[3SG]</ta>
            <ta e="T142" id="Seg_2158" s="T141">много</ta>
            <ta e="T143" id="Seg_2159" s="T142">люди.[NOM.SG]</ta>
            <ta e="T144" id="Seg_2160" s="T143">стоять-DUR.[3SG]</ta>
            <ta e="T145" id="Seg_2161" s="T144">и</ta>
            <ta e="T147" id="Seg_2162" s="T146">хомут.[NOM.SG]</ta>
            <ta e="T148" id="Seg_2163" s="T147">вешать-DUR.[3SG]</ta>
            <ta e="T149" id="Seg_2164" s="T148">лошадь-ACC</ta>
            <ta e="T150" id="Seg_2165" s="T149">хомут-LAT</ta>
            <ta e="T151" id="Seg_2166" s="T150">гнать-DUR.[3SG]</ta>
            <ta e="T152" id="Seg_2167" s="T151">что.[NOM.SG]</ta>
            <ta e="T153" id="Seg_2168" s="T152">делать-DUR-2PL</ta>
            <ta e="T154" id="Seg_2169" s="T153">и</ta>
            <ta e="T155" id="Seg_2170" s="T154">лошадь-ACC</ta>
            <ta e="T156" id="Seg_2171" s="T155">развязать-INF.LAT</ta>
            <ta e="T157" id="Seg_2172" s="T156">надо</ta>
            <ta e="T158" id="Seg_2173" s="T157">этот.[NOM.SG]</ta>
            <ta e="T159" id="Seg_2174" s="T158">хомут-NOM/GEN/ACC.3SG</ta>
            <ta e="T160" id="Seg_2175" s="T159">надеть-PST.[3SG]</ta>
            <ta e="T161" id="Seg_2176" s="T160">лошадь-LAT</ta>
            <ta e="T162" id="Seg_2177" s="T161">и</ta>
            <ta e="T163" id="Seg_2178" s="T162">развязать-PST.[3SG]</ta>
            <ta e="T164" id="Seg_2179" s="T163">жить-IMP.2SG</ta>
            <ta e="T167" id="Seg_2180" s="T166">я.NOM-COM</ta>
            <ta e="T168" id="Seg_2181" s="T167">нет</ta>
            <ta e="T169" id="Seg_2182" s="T168">NEG</ta>
            <ta e="T170" id="Seg_2183" s="T169">жить-CNG</ta>
            <ta e="T171" id="Seg_2184" s="T170">идти-PRS.[3SG]</ta>
            <ta e="T172" id="Seg_2185" s="T171">идти-PRS.[3SG]</ta>
            <ta e="T173" id="Seg_2186" s="T172">прийти-PST.[3SG]</ta>
            <ta e="T174" id="Seg_2187" s="T173">женщина-LAT</ta>
            <ta e="T175" id="Seg_2188" s="T174">этот.[NOM.SG]</ta>
            <ta e="T176" id="Seg_2189" s="T175">женщина.[NOM.SG]</ta>
            <ta e="T177" id="Seg_2190" s="T176">кипятить-DUR.[3SG]</ta>
            <ta e="T178" id="Seg_2191" s="T177">кисель-NOM/GEN/ACC.3SG</ta>
            <ta e="T179" id="Seg_2192" s="T178">и</ta>
            <ta e="T180" id="Seg_2193" s="T179">стол-NOM/GEN/ACC.3SG</ta>
            <ta e="T181" id="Seg_2194" s="T180">стоять-TR-PST.[3SG]</ta>
            <ta e="T182" id="Seg_2195" s="T181">и</ta>
            <ta e="T183" id="Seg_2196" s="T182">погреб-LAT</ta>
            <ta e="T184" id="Seg_2197" s="T183">идти-DUR.[3SG]</ta>
            <ta e="T185" id="Seg_2198" s="T184">сметана-3SG-INS</ta>
            <ta e="T186" id="Seg_2199" s="T185">а</ta>
            <ta e="T187" id="Seg_2200" s="T186">этот.[NOM.SG]</ta>
            <ta e="T188" id="Seg_2201" s="T187">сказать-IPFVZ.[3SG]</ta>
            <ta e="T189" id="Seg_2202" s="T188">что.[NOM.SG]</ta>
            <ta e="T190" id="Seg_2203" s="T189">носить-PRS-2SG</ta>
            <ta e="T191" id="Seg_2204" s="T190">ложка-INS</ta>
            <ta e="T192" id="Seg_2205" s="T191">видеть-PRS-2SG</ta>
            <ta e="T193" id="Seg_2206" s="T192">здесь</ta>
            <ta e="T194" id="Seg_2207" s="T193">надо</ta>
            <ta e="T195" id="Seg_2208" s="T194">класть-INF.LAT</ta>
            <ta e="T196" id="Seg_2209" s="T195">пойти-EP-IMP.2SG</ta>
            <ta e="T198" id="Seg_2210" s="T197">принести-IMP.2SG.O</ta>
            <ta e="T199" id="Seg_2211" s="T198">и</ta>
            <ta e="T201" id="Seg_2212" s="T199">и</ta>
            <ta e="T202" id="Seg_2213" s="T201">этот.[NOM.SG]</ta>
            <ta e="T203" id="Seg_2214" s="T202">пойти-PST.[3SG]</ta>
            <ta e="T204" id="Seg_2215" s="T203">принести-IMP.2SG.O</ta>
            <ta e="T205" id="Seg_2216" s="T204">сесть-PST-3PL</ta>
            <ta e="T206" id="Seg_2217" s="T205">Лутонюшка.[NOM.SG]</ta>
            <ta e="T207" id="Seg_2218" s="T206">съесть-PST.[3SG]</ta>
            <ta e="T208" id="Seg_2219" s="T207">и</ta>
            <ta e="T209" id="Seg_2220" s="T208">полати-LAT</ta>
            <ta e="T210" id="Seg_2221" s="T209">ложиться-PST.[3SG]</ta>
            <ta e="T211" id="Seg_2222" s="T210">спать-PST.[3SG]</ta>
            <ta e="T212" id="Seg_2223" s="T211">утро-LOC.ADV</ta>
            <ta e="T213" id="Seg_2224" s="T212">встать-PST.[3SG]</ta>
            <ta e="T214" id="Seg_2225" s="T213">и</ta>
            <ta e="T215" id="Seg_2226" s="T214">вернуться-MOM-PST.[3SG]</ta>
            <ta e="T216" id="Seg_2227" s="T215">отец-LAT/LOC.3SG</ta>
            <ta e="T217" id="Seg_2228" s="T216">и</ta>
            <ta e="T218" id="Seg_2229" s="T217">мать-LAT/LOC.3SG</ta>
            <ta e="T219" id="Seg_2230" s="T218">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_2231" s="T0">v-v:tense-v:pn</ta>
            <ta e="T2" id="Seg_2232" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_2233" s="T2">n-n:case</ta>
            <ta e="T5" id="Seg_2234" s="T4">dempro-n:case</ta>
            <ta e="T6" id="Seg_2235" s="T5">n-n:case.poss</ta>
            <ta e="T7" id="Seg_2236" s="T6">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_2237" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_2238" s="T8">propr-n:case</ta>
            <ta e="T10" id="Seg_2239" s="T9">dempro-n:case</ta>
            <ta e="T11" id="Seg_2240" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_2241" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_2242" s="T12">n-n:num</ta>
            <ta e="T14" id="Seg_2243" s="T13">v-v:tense-v:pn</ta>
            <ta e="T15" id="Seg_2244" s="T14">conj</ta>
            <ta e="T16" id="Seg_2245" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_2246" s="T16">conj</ta>
            <ta e="T18" id="Seg_2247" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_2248" s="T18">adv</ta>
            <ta e="T20" id="Seg_2249" s="T19">que-n:case=ptcl</ta>
            <ta e="T21" id="Seg_2250" s="T20">v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_2251" s="T21">dempro-n:case</ta>
            <ta e="T23" id="Seg_2252" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_2253" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_2254" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_2255" s="T25">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_2256" s="T26">ptcl</ta>
            <ta e="T28" id="Seg_2257" s="T27">v-v:n.fin</ta>
            <ta e="T29" id="Seg_2258" s="T28">conj</ta>
            <ta e="T30" id="Seg_2259" s="T29">v-v:n.fin</ta>
            <ta e="T31" id="Seg_2260" s="T30">n-n:case.poss</ta>
            <ta e="T32" id="Seg_2261" s="T31">v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_2262" s="T32">que-n:case</ta>
            <ta e="T34" id="Seg_2263" s="T33">v-v&gt;v-v:pn</ta>
            <ta e="T35" id="Seg_2264" s="T34">ptcl</ta>
            <ta e="T36" id="Seg_2265" s="T35">pers</ta>
            <ta e="T37" id="Seg_2266" s="T36">propr-n:case</ta>
            <ta e="T38" id="Seg_2267" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_2268" s="T38">n-n:case.poss</ta>
            <ta e="T40" id="Seg_2269" s="T39">v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_2270" s="T40">conj</ta>
            <ta e="T43" id="Seg_2271" s="T42">dempro-n:num</ta>
            <ta e="T44" id="Seg_2272" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_2273" s="T44">v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_2274" s="T45">conj</ta>
            <ta e="T47" id="Seg_2275" s="T46">adv</ta>
            <ta e="T48" id="Seg_2276" s="T47">v-v:tense-v:pn</ta>
            <ta e="T49" id="Seg_2277" s="T48">conj</ta>
            <ta e="T50" id="Seg_2278" s="T49">pers</ta>
            <ta e="T51" id="Seg_2279" s="T50">ptcl</ta>
            <ta e="T53" id="Seg_2280" s="T52">v-v:tense-v:pn</ta>
            <ta e="T54" id="Seg_2281" s="T53">n-n:case</ta>
            <ta e="T55" id="Seg_2282" s="T54">adv</ta>
            <ta e="T56" id="Seg_2283" s="T55">conj</ta>
            <ta e="T57" id="Seg_2284" s="T56">n-n:case</ta>
            <ta e="T58" id="Seg_2285" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_2286" s="T58">v-v:n.fin</ta>
            <ta e="T60" id="Seg_2287" s="T59">adv</ta>
            <ta e="T61" id="Seg_2288" s="T60">n-n:case.poss</ta>
            <ta e="T62" id="Seg_2289" s="T61">v-v:tense-v:pn</ta>
            <ta e="T63" id="Seg_2290" s="T62">que-n:case</ta>
            <ta e="T64" id="Seg_2291" s="T63">v-v&gt;v-v:pn</ta>
            <ta e="T65" id="Seg_2292" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_2293" s="T65">pers</ta>
            <ta e="T67" id="Seg_2294" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_2295" s="T67">n-n:case.poss</ta>
            <ta e="T69" id="Seg_2296" s="T68">v-v:tense-v:pn</ta>
            <ta e="T70" id="Seg_2297" s="T69">conj</ta>
            <ta e="T71" id="Seg_2298" s="T70">n-n:case.poss</ta>
            <ta e="T72" id="Seg_2299" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_2300" s="T72">v-v:tense-v:pn</ta>
            <ta e="T74" id="Seg_2301" s="T73">adv</ta>
            <ta e="T75" id="Seg_2302" s="T74">v-v:tense-v:pn</ta>
            <ta e="T76" id="Seg_2303" s="T75">conj</ta>
            <ta e="T77" id="Seg_2304" s="T76">pers</ta>
            <ta e="T78" id="Seg_2305" s="T77">ptcl</ta>
            <ta e="T79" id="Seg_2306" s="T78">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T80" id="Seg_2307" s="T79">dempro-n:case</ta>
            <ta e="T81" id="Seg_2308" s="T80">n-n:case</ta>
            <ta e="T82" id="Seg_2309" s="T81">adv</ta>
            <ta e="T83" id="Seg_2310" s="T82">dempro-n:case</ta>
            <ta e="T84" id="Seg_2311" s="T83">v-v&gt;v-v:pn</ta>
            <ta e="T85" id="Seg_2312" s="T84">n-n:case.poss</ta>
            <ta e="T86" id="Seg_2313" s="T85">v-v:tense-v:pn</ta>
            <ta e="T87" id="Seg_2314" s="T86">v-v:mood.pn</ta>
            <ta e="T88" id="Seg_2315" s="T87">conj</ta>
            <ta e="T89" id="Seg_2316" s="T88">pers</ta>
            <ta e="T220" id="Seg_2317" s="T89">v-v:n-fin</ta>
            <ta e="T90" id="Seg_2318" s="T220">v-v:tense-v:pn</ta>
            <ta e="T91" id="Seg_2319" s="T90">v-v:tense-v:pn</ta>
            <ta e="T92" id="Seg_2320" s="T91">adj-n:case</ta>
            <ta e="T93" id="Seg_2321" s="T92">ptcl</ta>
            <ta e="T94" id="Seg_2322" s="T93">pers</ta>
            <ta e="T95" id="Seg_2323" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_2324" s="T95">v-v:tense-v:pn</ta>
            <ta e="T97" id="Seg_2325" s="T96">conj</ta>
            <ta e="T98" id="Seg_2326" s="T97">ptcl</ta>
            <ta e="T99" id="Seg_2327" s="T98">v-v:tense-v:pn</ta>
            <ta e="T100" id="Seg_2328" s="T99">ptcl</ta>
            <ta e="T101" id="Seg_2329" s="T100">ptcl</ta>
            <ta e="T102" id="Seg_2330" s="T101">v-v:tense-v:pn</ta>
            <ta e="T103" id="Seg_2331" s="T102">adv</ta>
            <ta e="T104" id="Seg_2332" s="T103">v-v:tense-v:pn</ta>
            <ta e="T105" id="Seg_2333" s="T104">v-v:tense-v:pn</ta>
            <ta e="T106" id="Seg_2334" s="T105">n-n:case</ta>
            <ta e="T107" id="Seg_2335" s="T106">n-n:case</ta>
            <ta e="T108" id="Seg_2336" s="T107">n-n:case</ta>
            <ta e="T109" id="Seg_2337" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_2338" s="T109">v-v&gt;v-v:pn</ta>
            <ta e="T111" id="Seg_2339" s="T110">dempro-n:case</ta>
            <ta e="T112" id="Seg_2340" s="T111">v-v&gt;v-v:pn</ta>
            <ta e="T113" id="Seg_2341" s="T112">que-n:case</ta>
            <ta e="T114" id="Seg_2342" s="T113">v-v&gt;v-v:pn</ta>
            <ta e="T115" id="Seg_2343" s="T114">conj</ta>
            <ta e="T116" id="Seg_2344" s="T115">v-v:tense-v:pn</ta>
            <ta e="T117" id="Seg_2345" s="T116">adv</ta>
            <ta e="T118" id="Seg_2346" s="T117">n-n:case</ta>
            <ta e="T119" id="Seg_2347" s="T118">v-v&gt;v-v:pn</ta>
            <ta e="T120" id="Seg_2348" s="T119">dempro-n:case</ta>
            <ta e="T121" id="Seg_2349" s="T120">v-v:tense-v:pn</ta>
            <ta e="T122" id="Seg_2350" s="T121">n-n:case</ta>
            <ta e="T123" id="Seg_2351" s="T122">v-v:tense-v:pn</ta>
            <ta e="T124" id="Seg_2352" s="T123">n-n:case</ta>
            <ta e="T125" id="Seg_2353" s="T124">v-v:tense-v:pn</ta>
            <ta e="T126" id="Seg_2354" s="T125">v-v:mood.pn</ta>
            <ta e="T127" id="Seg_2355" s="T126">pers-n:case</ta>
            <ta e="T128" id="Seg_2356" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_2357" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_2358" s="T129">v-v:pn</ta>
            <ta e="T131" id="Seg_2359" s="T130">pers</ta>
            <ta e="T132" id="Seg_2360" s="T131">adv</ta>
            <ta e="T133" id="Seg_2361" s="T132">adj-n:case</ta>
            <ta e="T134" id="Seg_2362" s="T133">n-n:case</ta>
            <ta e="T135" id="Seg_2363" s="T134">quant</ta>
            <ta e="T137" id="Seg_2364" s="T136">v-v:tense-v:pn</ta>
            <ta e="T138" id="Seg_2365" s="T137">adv</ta>
            <ta e="T139" id="Seg_2366" s="T138">adv</ta>
            <ta e="T140" id="Seg_2367" s="T139">v-v:tense-v:pn</ta>
            <ta e="T141" id="Seg_2368" s="T140">v-v:tense-v:pn</ta>
            <ta e="T142" id="Seg_2369" s="T141">quant</ta>
            <ta e="T143" id="Seg_2370" s="T142">n-n:case</ta>
            <ta e="T144" id="Seg_2371" s="T143">v-v&gt;v-v:pn</ta>
            <ta e="T145" id="Seg_2372" s="T144">conj</ta>
            <ta e="T147" id="Seg_2373" s="T146">n-n:case</ta>
            <ta e="T148" id="Seg_2374" s="T147">v-v&gt;v-v:pn</ta>
            <ta e="T149" id="Seg_2375" s="T148">n-n:case</ta>
            <ta e="T150" id="Seg_2376" s="T149">n-n:case</ta>
            <ta e="T151" id="Seg_2377" s="T150">v-v&gt;v-v:pn</ta>
            <ta e="T152" id="Seg_2378" s="T151">que-n:case</ta>
            <ta e="T153" id="Seg_2379" s="T152">v-v&gt;v-v:pn</ta>
            <ta e="T154" id="Seg_2380" s="T153">conj</ta>
            <ta e="T155" id="Seg_2381" s="T154">n-n:case</ta>
            <ta e="T156" id="Seg_2382" s="T155">v-v:n.fin</ta>
            <ta e="T157" id="Seg_2383" s="T156">ptcl</ta>
            <ta e="T158" id="Seg_2384" s="T157">dempro-n:case</ta>
            <ta e="T159" id="Seg_2385" s="T158">n-n:case.poss</ta>
            <ta e="T160" id="Seg_2386" s="T159">v-v:tense-v:pn</ta>
            <ta e="T161" id="Seg_2387" s="T160">n-n:case</ta>
            <ta e="T162" id="Seg_2388" s="T161">conj</ta>
            <ta e="T163" id="Seg_2389" s="T162">v-v:tense-v:pn</ta>
            <ta e="T164" id="Seg_2390" s="T163">v-v:mood.pn</ta>
            <ta e="T167" id="Seg_2391" s="T166">pers</ta>
            <ta e="T168" id="Seg_2392" s="T167">ptcl</ta>
            <ta e="T169" id="Seg_2393" s="T168">ptcl</ta>
            <ta e="T170" id="Seg_2394" s="T169">v-v:n.fin</ta>
            <ta e="T171" id="Seg_2395" s="T170">v-v:tense-v:pn</ta>
            <ta e="T172" id="Seg_2396" s="T171">v-v:tense-v:pn</ta>
            <ta e="T173" id="Seg_2397" s="T172">v-v:tense-v:pn</ta>
            <ta e="T174" id="Seg_2398" s="T173">n-n:case</ta>
            <ta e="T175" id="Seg_2399" s="T174">dempro-n:case</ta>
            <ta e="T176" id="Seg_2400" s="T175">n-n:case</ta>
            <ta e="T177" id="Seg_2401" s="T176">v-v&gt;v-v:pn</ta>
            <ta e="T178" id="Seg_2402" s="T177">n-n:case.poss</ta>
            <ta e="T179" id="Seg_2403" s="T178">conj</ta>
            <ta e="T180" id="Seg_2404" s="T179">n-n:case.poss</ta>
            <ta e="T181" id="Seg_2405" s="T180">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T182" id="Seg_2406" s="T181">conj</ta>
            <ta e="T183" id="Seg_2407" s="T182">n-n:case</ta>
            <ta e="T184" id="Seg_2408" s="T183">v-v&gt;v-v:pn</ta>
            <ta e="T185" id="Seg_2409" s="T184">n-n:case.poss-n:case</ta>
            <ta e="T186" id="Seg_2410" s="T185">conj</ta>
            <ta e="T187" id="Seg_2411" s="T186">dempro-n:case</ta>
            <ta e="T188" id="Seg_2412" s="T187">v-v&gt;v-v:pn</ta>
            <ta e="T189" id="Seg_2413" s="T188">que-n:case</ta>
            <ta e="T190" id="Seg_2414" s="T189">v-v:tense-v:pn</ta>
            <ta e="T191" id="Seg_2415" s="T190">n-n:case</ta>
            <ta e="T192" id="Seg_2416" s="T191">v-v:tense-v:pn</ta>
            <ta e="T193" id="Seg_2417" s="T192">adv</ta>
            <ta e="T194" id="Seg_2418" s="T193">ptcl</ta>
            <ta e="T195" id="Seg_2419" s="T194">v-v:n.fin</ta>
            <ta e="T196" id="Seg_2420" s="T195">v-v:ins-v:mood.pn</ta>
            <ta e="T198" id="Seg_2421" s="T197">v-v:mood.pn</ta>
            <ta e="T199" id="Seg_2422" s="T198">conj</ta>
            <ta e="T201" id="Seg_2423" s="T199">conj</ta>
            <ta e="T202" id="Seg_2424" s="T201">dempro-n:case</ta>
            <ta e="T203" id="Seg_2425" s="T202">v-v:tense-v:pn</ta>
            <ta e="T204" id="Seg_2426" s="T203">v-v:mood.pn</ta>
            <ta e="T205" id="Seg_2427" s="T204">v-v:tense-v:pn</ta>
            <ta e="T206" id="Seg_2428" s="T205">propr-n:case</ta>
            <ta e="T207" id="Seg_2429" s="T206">v-v:tense-v:pn</ta>
            <ta e="T208" id="Seg_2430" s="T207">conj</ta>
            <ta e="T209" id="Seg_2431" s="T208">n-n:case</ta>
            <ta e="T210" id="Seg_2432" s="T209">v-v:tense-v:pn</ta>
            <ta e="T211" id="Seg_2433" s="T210">v-v:tense-v:pn</ta>
            <ta e="T212" id="Seg_2434" s="T211">n-adv:case</ta>
            <ta e="T213" id="Seg_2435" s="T212">v-v:tense-v:pn</ta>
            <ta e="T214" id="Seg_2436" s="T213">conj</ta>
            <ta e="T215" id="Seg_2437" s="T214">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T216" id="Seg_2438" s="T215">n-n:case.poss</ta>
            <ta e="T217" id="Seg_2439" s="T216">conj</ta>
            <ta e="T218" id="Seg_2440" s="T217">n-n:case.poss</ta>
            <ta e="T219" id="Seg_2441" s="T218">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_2442" s="T0">v</ta>
            <ta e="T2" id="Seg_2443" s="T1">n</ta>
            <ta e="T3" id="Seg_2444" s="T2">n</ta>
            <ta e="T5" id="Seg_2445" s="T4">dempro</ta>
            <ta e="T6" id="Seg_2446" s="T5">n</ta>
            <ta e="T7" id="Seg_2447" s="T6">v</ta>
            <ta e="T8" id="Seg_2448" s="T7">v</ta>
            <ta e="T9" id="Seg_2449" s="T8">propr</ta>
            <ta e="T10" id="Seg_2450" s="T9">dempro</ta>
            <ta e="T11" id="Seg_2451" s="T10">n</ta>
            <ta e="T12" id="Seg_2452" s="T11">n</ta>
            <ta e="T13" id="Seg_2453" s="T12">n</ta>
            <ta e="T14" id="Seg_2454" s="T13">v</ta>
            <ta e="T15" id="Seg_2455" s="T14">conj</ta>
            <ta e="T16" id="Seg_2456" s="T15">n</ta>
            <ta e="T17" id="Seg_2457" s="T16">conj</ta>
            <ta e="T18" id="Seg_2458" s="T17">n</ta>
            <ta e="T19" id="Seg_2459" s="T18">adv</ta>
            <ta e="T20" id="Seg_2460" s="T19">que</ta>
            <ta e="T21" id="Seg_2461" s="T20">v</ta>
            <ta e="T22" id="Seg_2462" s="T21">dempro</ta>
            <ta e="T23" id="Seg_2463" s="T22">n</ta>
            <ta e="T24" id="Seg_2464" s="T23">n</ta>
            <ta e="T25" id="Seg_2465" s="T24">n</ta>
            <ta e="T26" id="Seg_2466" s="T25">v</ta>
            <ta e="T27" id="Seg_2467" s="T26">ptcl</ta>
            <ta e="T28" id="Seg_2468" s="T27">v</ta>
            <ta e="T29" id="Seg_2469" s="T28">conj</ta>
            <ta e="T30" id="Seg_2470" s="T29">v</ta>
            <ta e="T31" id="Seg_2471" s="T30">n</ta>
            <ta e="T32" id="Seg_2472" s="T31">v</ta>
            <ta e="T33" id="Seg_2473" s="T32">que</ta>
            <ta e="T34" id="Seg_2474" s="T33">v</ta>
            <ta e="T35" id="Seg_2475" s="T34">ptcl</ta>
            <ta e="T36" id="Seg_2476" s="T35">pers</ta>
            <ta e="T37" id="Seg_2477" s="T36">propr</ta>
            <ta e="T38" id="Seg_2478" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_2479" s="T38">n</ta>
            <ta e="T40" id="Seg_2480" s="T39">v</ta>
            <ta e="T41" id="Seg_2481" s="T40">conj</ta>
            <ta e="T43" id="Seg_2482" s="T42">dempro</ta>
            <ta e="T44" id="Seg_2483" s="T43">n</ta>
            <ta e="T45" id="Seg_2484" s="T44">v</ta>
            <ta e="T46" id="Seg_2485" s="T45">conj</ta>
            <ta e="T47" id="Seg_2486" s="T46">adv</ta>
            <ta e="T48" id="Seg_2487" s="T47">v</ta>
            <ta e="T49" id="Seg_2488" s="T48">conj</ta>
            <ta e="T50" id="Seg_2489" s="T49">pers</ta>
            <ta e="T51" id="Seg_2490" s="T50">ptcl</ta>
            <ta e="T53" id="Seg_2491" s="T52">v</ta>
            <ta e="T54" id="Seg_2492" s="T53">n</ta>
            <ta e="T55" id="Seg_2493" s="T54">adv</ta>
            <ta e="T56" id="Seg_2494" s="T55">conj</ta>
            <ta e="T57" id="Seg_2495" s="T56">n</ta>
            <ta e="T58" id="Seg_2496" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_2497" s="T58">v</ta>
            <ta e="T60" id="Seg_2498" s="T59">adv</ta>
            <ta e="T61" id="Seg_2499" s="T60">n</ta>
            <ta e="T62" id="Seg_2500" s="T61">v</ta>
            <ta e="T63" id="Seg_2501" s="T62">que</ta>
            <ta e="T64" id="Seg_2502" s="T63">v</ta>
            <ta e="T65" id="Seg_2503" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_2504" s="T65">pers</ta>
            <ta e="T67" id="Seg_2505" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_2506" s="T67">n</ta>
            <ta e="T69" id="Seg_2507" s="T68">v</ta>
            <ta e="T70" id="Seg_2508" s="T69">conj</ta>
            <ta e="T71" id="Seg_2509" s="T70">n</ta>
            <ta e="T72" id="Seg_2510" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_2511" s="T72">v</ta>
            <ta e="T74" id="Seg_2512" s="T73">adv</ta>
            <ta e="T75" id="Seg_2513" s="T74">v</ta>
            <ta e="T76" id="Seg_2514" s="T75">conj</ta>
            <ta e="T77" id="Seg_2515" s="T76">pers</ta>
            <ta e="T78" id="Seg_2516" s="T77">ptcl</ta>
            <ta e="T79" id="Seg_2517" s="T78">v</ta>
            <ta e="T80" id="Seg_2518" s="T79">dempro</ta>
            <ta e="T81" id="Seg_2519" s="T80">n</ta>
            <ta e="T82" id="Seg_2520" s="T81">adv</ta>
            <ta e="T83" id="Seg_2521" s="T82">dempro</ta>
            <ta e="T84" id="Seg_2522" s="T83">v</ta>
            <ta e="T85" id="Seg_2523" s="T84">n</ta>
            <ta e="T86" id="Seg_2524" s="T85">v</ta>
            <ta e="T87" id="Seg_2525" s="T86">v</ta>
            <ta e="T88" id="Seg_2526" s="T87">conj</ta>
            <ta e="T89" id="Seg_2527" s="T88">pers</ta>
            <ta e="T220" id="Seg_2528" s="T89">v</ta>
            <ta e="T90" id="Seg_2529" s="T220">v</ta>
            <ta e="T91" id="Seg_2530" s="T90">v</ta>
            <ta e="T92" id="Seg_2531" s="T91">adj</ta>
            <ta e="T93" id="Seg_2532" s="T92">ptcl</ta>
            <ta e="T94" id="Seg_2533" s="T93">pers</ta>
            <ta e="T95" id="Seg_2534" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_2535" s="T95">v</ta>
            <ta e="T97" id="Seg_2536" s="T96">conj</ta>
            <ta e="T98" id="Seg_2537" s="T97">ptcl</ta>
            <ta e="T99" id="Seg_2538" s="T98">v</ta>
            <ta e="T100" id="Seg_2539" s="T99">ptcl</ta>
            <ta e="T101" id="Seg_2540" s="T100">ptcl</ta>
            <ta e="T102" id="Seg_2541" s="T101">v</ta>
            <ta e="T103" id="Seg_2542" s="T102">adv</ta>
            <ta e="T104" id="Seg_2543" s="T103">v</ta>
            <ta e="T105" id="Seg_2544" s="T104">v</ta>
            <ta e="T106" id="Seg_2545" s="T105">n</ta>
            <ta e="T107" id="Seg_2546" s="T106">n</ta>
            <ta e="T108" id="Seg_2547" s="T107">n</ta>
            <ta e="T109" id="Seg_2548" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_2549" s="T109">v</ta>
            <ta e="T111" id="Seg_2550" s="T110">dempro</ta>
            <ta e="T112" id="Seg_2551" s="T111">v</ta>
            <ta e="T113" id="Seg_2552" s="T112">que</ta>
            <ta e="T114" id="Seg_2553" s="T113">v</ta>
            <ta e="T115" id="Seg_2554" s="T114">conj</ta>
            <ta e="T116" id="Seg_2555" s="T115">v</ta>
            <ta e="T117" id="Seg_2556" s="T116">adv</ta>
            <ta e="T118" id="Seg_2557" s="T117">n</ta>
            <ta e="T119" id="Seg_2558" s="T118">v</ta>
            <ta e="T120" id="Seg_2559" s="T119">dempro</ta>
            <ta e="T121" id="Seg_2560" s="T120">v</ta>
            <ta e="T122" id="Seg_2561" s="T121">n</ta>
            <ta e="T123" id="Seg_2562" s="T122">v</ta>
            <ta e="T124" id="Seg_2563" s="T123">n</ta>
            <ta e="T125" id="Seg_2564" s="T124">v</ta>
            <ta e="T126" id="Seg_2565" s="T125">v</ta>
            <ta e="T127" id="Seg_2566" s="T126">pers</ta>
            <ta e="T128" id="Seg_2567" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_2568" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_2569" s="T129">v</ta>
            <ta e="T131" id="Seg_2570" s="T130">pers</ta>
            <ta e="T132" id="Seg_2571" s="T131">adv</ta>
            <ta e="T133" id="Seg_2572" s="T132">adj</ta>
            <ta e="T134" id="Seg_2573" s="T133">n</ta>
            <ta e="T135" id="Seg_2574" s="T134">quant</ta>
            <ta e="T137" id="Seg_2575" s="T136">v</ta>
            <ta e="T138" id="Seg_2576" s="T137">adv</ta>
            <ta e="T139" id="Seg_2577" s="T138">adv</ta>
            <ta e="T140" id="Seg_2578" s="T139">v</ta>
            <ta e="T141" id="Seg_2579" s="T140">v</ta>
            <ta e="T142" id="Seg_2580" s="T141">quant</ta>
            <ta e="T143" id="Seg_2581" s="T142">n</ta>
            <ta e="T144" id="Seg_2582" s="T143">v</ta>
            <ta e="T145" id="Seg_2583" s="T144">conj</ta>
            <ta e="T147" id="Seg_2584" s="T146">n</ta>
            <ta e="T148" id="Seg_2585" s="T147">v</ta>
            <ta e="T149" id="Seg_2586" s="T148">n</ta>
            <ta e="T150" id="Seg_2587" s="T149">n</ta>
            <ta e="T151" id="Seg_2588" s="T150">v</ta>
            <ta e="T152" id="Seg_2589" s="T151">que</ta>
            <ta e="T153" id="Seg_2590" s="T152">v</ta>
            <ta e="T154" id="Seg_2591" s="T153">conj</ta>
            <ta e="T155" id="Seg_2592" s="T154">n</ta>
            <ta e="T156" id="Seg_2593" s="T155">v</ta>
            <ta e="T157" id="Seg_2594" s="T156">ptcl</ta>
            <ta e="T158" id="Seg_2595" s="T157">dempro</ta>
            <ta e="T159" id="Seg_2596" s="T158">n</ta>
            <ta e="T160" id="Seg_2597" s="T159">v</ta>
            <ta e="T161" id="Seg_2598" s="T160">n</ta>
            <ta e="T162" id="Seg_2599" s="T161">conj</ta>
            <ta e="T163" id="Seg_2600" s="T162">v</ta>
            <ta e="T164" id="Seg_2601" s="T163">v</ta>
            <ta e="T167" id="Seg_2602" s="T166">pers</ta>
            <ta e="T168" id="Seg_2603" s="T167">ptcl</ta>
            <ta e="T169" id="Seg_2604" s="T168">ptcl</ta>
            <ta e="T170" id="Seg_2605" s="T169">v</ta>
            <ta e="T171" id="Seg_2606" s="T170">v</ta>
            <ta e="T172" id="Seg_2607" s="T171">v</ta>
            <ta e="T173" id="Seg_2608" s="T172">v</ta>
            <ta e="T174" id="Seg_2609" s="T173">n</ta>
            <ta e="T175" id="Seg_2610" s="T174">dempro</ta>
            <ta e="T176" id="Seg_2611" s="T175">n</ta>
            <ta e="T177" id="Seg_2612" s="T176">v</ta>
            <ta e="T178" id="Seg_2613" s="T177">n</ta>
            <ta e="T179" id="Seg_2614" s="T178">conj</ta>
            <ta e="T180" id="Seg_2615" s="T179">n</ta>
            <ta e="T181" id="Seg_2616" s="T180">v</ta>
            <ta e="T182" id="Seg_2617" s="T181">conj</ta>
            <ta e="T183" id="Seg_2618" s="T182">n</ta>
            <ta e="T184" id="Seg_2619" s="T183">v</ta>
            <ta e="T185" id="Seg_2620" s="T184">n</ta>
            <ta e="T186" id="Seg_2621" s="T185">conj</ta>
            <ta e="T187" id="Seg_2622" s="T186">dempro</ta>
            <ta e="T188" id="Seg_2623" s="T187">v</ta>
            <ta e="T189" id="Seg_2624" s="T188">que</ta>
            <ta e="T191" id="Seg_2625" s="T190">n</ta>
            <ta e="T192" id="Seg_2626" s="T191">v</ta>
            <ta e="T193" id="Seg_2627" s="T192">adv</ta>
            <ta e="T194" id="Seg_2628" s="T193">ptcl</ta>
            <ta e="T195" id="Seg_2629" s="T194">v</ta>
            <ta e="T196" id="Seg_2630" s="T195">v</ta>
            <ta e="T198" id="Seg_2631" s="T197">v</ta>
            <ta e="T199" id="Seg_2632" s="T198">conj</ta>
            <ta e="T201" id="Seg_2633" s="T199">conj</ta>
            <ta e="T202" id="Seg_2634" s="T201">dempro</ta>
            <ta e="T203" id="Seg_2635" s="T202">v</ta>
            <ta e="T204" id="Seg_2636" s="T203">v</ta>
            <ta e="T205" id="Seg_2637" s="T204">v</ta>
            <ta e="T206" id="Seg_2638" s="T205">propr</ta>
            <ta e="T207" id="Seg_2639" s="T206">v</ta>
            <ta e="T208" id="Seg_2640" s="T207">conj</ta>
            <ta e="T209" id="Seg_2641" s="T208">n</ta>
            <ta e="T210" id="Seg_2642" s="T209">v</ta>
            <ta e="T211" id="Seg_2643" s="T210">v</ta>
            <ta e="T212" id="Seg_2644" s="T211">adv</ta>
            <ta e="T213" id="Seg_2645" s="T212">v</ta>
            <ta e="T214" id="Seg_2646" s="T213">conj</ta>
            <ta e="T215" id="Seg_2647" s="T214">v</ta>
            <ta e="T216" id="Seg_2648" s="T215">n</ta>
            <ta e="T217" id="Seg_2649" s="T216">conj</ta>
            <ta e="T218" id="Seg_2650" s="T217">n</ta>
            <ta e="T219" id="Seg_2651" s="T218">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_2652" s="T1">np.h:E</ta>
            <ta e="T3" id="Seg_2653" s="T2">np.h:E</ta>
            <ta e="T5" id="Seg_2654" s="T4">pro.h:Poss</ta>
            <ta e="T6" id="Seg_2655" s="T5">np.h:Th</ta>
            <ta e="T8" id="Seg_2656" s="T7">0.3.h:Th</ta>
            <ta e="T11" id="Seg_2657" s="T10">np.h:A</ta>
            <ta e="T12" id="Seg_2658" s="T11">np:G</ta>
            <ta e="T13" id="Seg_2659" s="T12">np:Th</ta>
            <ta e="T16" id="Seg_2660" s="T15">np.h:A</ta>
            <ta e="T18" id="Seg_2661" s="T17">np.h:A</ta>
            <ta e="T19" id="Seg_2662" s="T18">adv:L</ta>
            <ta e="T20" id="Seg_2663" s="T19">pro:P</ta>
            <ta e="T23" id="Seg_2664" s="T22">np.h:A</ta>
            <ta e="T24" id="Seg_2665" s="T23">np:Th</ta>
            <ta e="T25" id="Seg_2666" s="T24">np:G</ta>
            <ta e="T31" id="Seg_2667" s="T30">np.h:A</ta>
            <ta e="T34" id="Seg_2668" s="T33">0.2.h:E</ta>
            <ta e="T36" id="Seg_2669" s="T35">pro.h:Poss</ta>
            <ta e="T37" id="Seg_2670" s="T36">np.h:Poss</ta>
            <ta e="T39" id="Seg_2671" s="T38">np.h:Th</ta>
            <ta e="T43" id="Seg_2672" s="T42">pro.h:Poss</ta>
            <ta e="T44" id="Seg_2673" s="T43">np.h:Th</ta>
            <ta e="T48" id="Seg_2674" s="T47">0.3.h:E</ta>
            <ta e="T50" id="Seg_2675" s="T49">pro.h:A</ta>
            <ta e="T54" id="Seg_2676" s="T53">np:Ins</ta>
            <ta e="T55" id="Seg_2677" s="T54">adv:Time</ta>
            <ta e="T57" id="Seg_2678" s="T56">np.h:A</ta>
            <ta e="T60" id="Seg_2679" s="T59">adv:Time</ta>
            <ta e="T61" id="Seg_2680" s="T60">np.h:A</ta>
            <ta e="T64" id="Seg_2681" s="T63">0.2.h:A</ta>
            <ta e="T66" id="Seg_2682" s="T65">pro.h:Poss</ta>
            <ta e="T68" id="Seg_2683" s="T67">np.h:Th</ta>
            <ta e="T71" id="Seg_2684" s="T70">np.h:Th</ta>
            <ta e="T74" id="Seg_2685" s="T73">adv:L</ta>
            <ta e="T75" id="Seg_2686" s="T74">0.3.h:E</ta>
            <ta e="T77" id="Seg_2687" s="T76">pro.h:A</ta>
            <ta e="T80" id="Seg_2688" s="T79">pro.h:P</ta>
            <ta e="T81" id="Seg_2689" s="T80">np:Ins</ta>
            <ta e="T82" id="Seg_2690" s="T81">adv:Time</ta>
            <ta e="T83" id="Seg_2691" s="T82">pro.h:A</ta>
            <ta e="T85" id="Seg_2692" s="T84">np:Th</ta>
            <ta e="T86" id="Seg_2693" s="T85">0.3.h:A</ta>
            <ta e="T87" id="Seg_2694" s="T86">0.2.h:E</ta>
            <ta e="T89" id="Seg_2695" s="T88">pro.h:A</ta>
            <ta e="T91" id="Seg_2696" s="T90">0.1.h:A</ta>
            <ta e="T96" id="Seg_2697" s="T95">0.1.h:A</ta>
            <ta e="T99" id="Seg_2698" s="T98">0.1.h:A</ta>
            <ta e="T102" id="Seg_2699" s="T101">0.1.h:A</ta>
            <ta e="T103" id="Seg_2700" s="T102">adv:Time</ta>
            <ta e="T104" id="Seg_2701" s="T103">0.3.h:A</ta>
            <ta e="T105" id="Seg_2702" s="T104">0.3.h:A</ta>
            <ta e="T106" id="Seg_2703" s="T105">np.h:A</ta>
            <ta e="T107" id="Seg_2704" s="T106">np:Th</ta>
            <ta e="T108" id="Seg_2705" s="T107">np:G</ta>
            <ta e="T111" id="Seg_2706" s="T110">pro.h:A</ta>
            <ta e="T113" id="Seg_2707" s="T112">pro:P</ta>
            <ta e="T114" id="Seg_2708" s="T113">0.3.h:A</ta>
            <ta e="T116" id="Seg_2709" s="T115">0.2.h:E</ta>
            <ta e="T117" id="Seg_2710" s="T116">adv:L</ta>
            <ta e="T118" id="Seg_2711" s="T117">np:Th</ta>
            <ta e="T120" id="Seg_2712" s="T119">pro.h:A</ta>
            <ta e="T122" id="Seg_2713" s="T121">np:P</ta>
            <ta e="T124" id="Seg_2714" s="T123">np:G</ta>
            <ta e="T125" id="Seg_2715" s="T124">0.3.h:A</ta>
            <ta e="T126" id="Seg_2716" s="T125">0.2.h:E</ta>
            <ta e="T127" id="Seg_2717" s="T126">pro.h:Com</ta>
            <ta e="T130" id="Seg_2718" s="T129">0.1.h:E</ta>
            <ta e="T131" id="Seg_2719" s="T130">pro.h:Th</ta>
            <ta e="T134" id="Seg_2720" s="T133">np.h:Th</ta>
            <ta e="T137" id="Seg_2721" s="T136">0.1.h:A</ta>
            <ta e="T139" id="Seg_2722" s="T138">adv:Time</ta>
            <ta e="T140" id="Seg_2723" s="T139">0.3.h:A</ta>
            <ta e="T141" id="Seg_2724" s="T140">0.3.h:A</ta>
            <ta e="T143" id="Seg_2725" s="T142">np.h:E</ta>
            <ta e="T147" id="Seg_2726" s="T146">np:Th</ta>
            <ta e="T149" id="Seg_2727" s="T148">np:Th</ta>
            <ta e="T150" id="Seg_2728" s="T149">np:G</ta>
            <ta e="T151" id="Seg_2729" s="T150">0.3.h:A</ta>
            <ta e="T152" id="Seg_2730" s="T151">pro:P</ta>
            <ta e="T153" id="Seg_2731" s="T152">0.2.h:A</ta>
            <ta e="T155" id="Seg_2732" s="T154">np:Th</ta>
            <ta e="T158" id="Seg_2733" s="T157">pro.h:A</ta>
            <ta e="T159" id="Seg_2734" s="T158">np:Th</ta>
            <ta e="T161" id="Seg_2735" s="T160">np:G</ta>
            <ta e="T163" id="Seg_2736" s="T162">0.3.h:A</ta>
            <ta e="T164" id="Seg_2737" s="T163">0.2.h:E</ta>
            <ta e="T167" id="Seg_2738" s="T166">pro.h:Com</ta>
            <ta e="T171" id="Seg_2739" s="T170">0.3.h:A</ta>
            <ta e="T172" id="Seg_2740" s="T171">0.3.h:A</ta>
            <ta e="T173" id="Seg_2741" s="T172">0.3.h:A</ta>
            <ta e="T174" id="Seg_2742" s="T173">np:G</ta>
            <ta e="T176" id="Seg_2743" s="T175">np.h:A</ta>
            <ta e="T178" id="Seg_2744" s="T177">np:P</ta>
            <ta e="T180" id="Seg_2745" s="T179">np:G</ta>
            <ta e="T181" id="Seg_2746" s="T180">0.3:Th</ta>
            <ta e="T183" id="Seg_2747" s="T182">np:G</ta>
            <ta e="T184" id="Seg_2748" s="T183">0.3.h:A</ta>
            <ta e="T187" id="Seg_2749" s="T186">pro.h:A</ta>
            <ta e="T190" id="Seg_2750" s="T189">0.2.h:A</ta>
            <ta e="T191" id="Seg_2751" s="T190">np:Ins</ta>
            <ta e="T192" id="Seg_2752" s="T191">0.2.h:E</ta>
            <ta e="T193" id="Seg_2753" s="T192">adv:L</ta>
            <ta e="T196" id="Seg_2754" s="T195">0.2.h:A</ta>
            <ta e="T198" id="Seg_2755" s="T197">0.2.h:A</ta>
            <ta e="T202" id="Seg_2756" s="T201">pro.h:A</ta>
            <ta e="T204" id="Seg_2757" s="T203">0.2.h:A</ta>
            <ta e="T205" id="Seg_2758" s="T204">0.3.h:A</ta>
            <ta e="T206" id="Seg_2759" s="T205">np.h:A</ta>
            <ta e="T209" id="Seg_2760" s="T208">np:G</ta>
            <ta e="T210" id="Seg_2761" s="T209">0.3.h:A</ta>
            <ta e="T211" id="Seg_2762" s="T210">0.3.h:E</ta>
            <ta e="T212" id="Seg_2763" s="T211">n:Time</ta>
            <ta e="T213" id="Seg_2764" s="T212">0.3.h:A</ta>
            <ta e="T215" id="Seg_2765" s="T214">0.3.h:A</ta>
            <ta e="T216" id="Seg_2766" s="T215">np:G</ta>
            <ta e="T218" id="Seg_2767" s="T217">np:G</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_2768" s="T0">v:pred</ta>
            <ta e="T2" id="Seg_2769" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_2770" s="T2">np.h:S</ta>
            <ta e="T6" id="Seg_2771" s="T5">np.h:S</ta>
            <ta e="T7" id="Seg_2772" s="T6">v:pred</ta>
            <ta e="T8" id="Seg_2773" s="T7">v:pred 0.3.h:S</ta>
            <ta e="T11" id="Seg_2774" s="T10">np.h:S</ta>
            <ta e="T13" id="Seg_2775" s="T12">np:O</ta>
            <ta e="T14" id="Seg_2776" s="T13">v:pred</ta>
            <ta e="T16" id="Seg_2777" s="T15">np.h:S</ta>
            <ta e="T18" id="Seg_2778" s="T17">np.h:S</ta>
            <ta e="T20" id="Seg_2779" s="T19">pro:O</ta>
            <ta e="T21" id="Seg_2780" s="T20">v:pred</ta>
            <ta e="T23" id="Seg_2781" s="T22">np.h:S</ta>
            <ta e="T24" id="Seg_2782" s="T23">np:O</ta>
            <ta e="T26" id="Seg_2783" s="T25">v:pred</ta>
            <ta e="T27" id="Seg_2784" s="T26">ptcl:pred</ta>
            <ta e="T31" id="Seg_2785" s="T30">np.h:S</ta>
            <ta e="T32" id="Seg_2786" s="T31">v:pred</ta>
            <ta e="T34" id="Seg_2787" s="T33">v:pred 0.2.h:S</ta>
            <ta e="T39" id="Seg_2788" s="T38">np.h:S</ta>
            <ta e="T40" id="Seg_2789" s="T39">v:pred</ta>
            <ta e="T44" id="Seg_2790" s="T43">np.h:S</ta>
            <ta e="T45" id="Seg_2791" s="T44">v:pred</ta>
            <ta e="T48" id="Seg_2792" s="T47">v:pred 0.3.h:S</ta>
            <ta e="T50" id="Seg_2793" s="T49">pro.h:S</ta>
            <ta e="T53" id="Seg_2794" s="T52">v:pred</ta>
            <ta e="T58" id="Seg_2795" s="T57">ptcl:pred</ta>
            <ta e="T61" id="Seg_2796" s="T60">np.h:S</ta>
            <ta e="T62" id="Seg_2797" s="T61">v:pred</ta>
            <ta e="T64" id="Seg_2798" s="T63">v:pred 0.2.h:S</ta>
            <ta e="T68" id="Seg_2799" s="T67">np.h:S</ta>
            <ta e="T69" id="Seg_2800" s="T68">v:pred</ta>
            <ta e="T71" id="Seg_2801" s="T70">np.h:S</ta>
            <ta e="T73" id="Seg_2802" s="T72">v:pred</ta>
            <ta e="T75" id="Seg_2803" s="T74">v:pred 0.3.h:S</ta>
            <ta e="T77" id="Seg_2804" s="T76">pro.h:S</ta>
            <ta e="T79" id="Seg_2805" s="T78">v:pred</ta>
            <ta e="T80" id="Seg_2806" s="T79">pro.h:O</ta>
            <ta e="T83" id="Seg_2807" s="T82">pro.h:S</ta>
            <ta e="T84" id="Seg_2808" s="T83">v:pred</ta>
            <ta e="T85" id="Seg_2809" s="T84">np:O</ta>
            <ta e="T86" id="Seg_2810" s="T85">v:pred 0.3.h:S</ta>
            <ta e="T87" id="Seg_2811" s="T86">v:pred 0.2.h:S</ta>
            <ta e="T89" id="Seg_2812" s="T88">pro.h:S</ta>
            <ta e="T220" id="Seg_2813" s="T89">conv:pred</ta>
            <ta e="T90" id="Seg_2814" s="T220">v:pred</ta>
            <ta e="T91" id="Seg_2815" s="T90">v:pred 0.1.h:S</ta>
            <ta e="T96" id="Seg_2816" s="T95">v:pred 0.1.h:S</ta>
            <ta e="T98" id="Seg_2817" s="T97">ptcl.neg</ta>
            <ta e="T99" id="Seg_2818" s="T98">v:pred 0.1.h:S</ta>
            <ta e="T101" id="Seg_2819" s="T100">ptcl.neg</ta>
            <ta e="T102" id="Seg_2820" s="T101">v:pred 0.1.h:S</ta>
            <ta e="T104" id="Seg_2821" s="T103">v:pred 0.3.h:S</ta>
            <ta e="T105" id="Seg_2822" s="T104">v:pred 0.3.h:S</ta>
            <ta e="T106" id="Seg_2823" s="T105">np.h:S</ta>
            <ta e="T107" id="Seg_2824" s="T106">np:O</ta>
            <ta e="T110" id="Seg_2825" s="T109">v:pred</ta>
            <ta e="T111" id="Seg_2826" s="T110">pro.h:S</ta>
            <ta e="T112" id="Seg_2827" s="T111">v:pred</ta>
            <ta e="T113" id="Seg_2828" s="T112">pro:O</ta>
            <ta e="T114" id="Seg_2829" s="T113">v:pred 0.3.h:S</ta>
            <ta e="T116" id="Seg_2830" s="T115">v:pred 0.2.h:S</ta>
            <ta e="T118" id="Seg_2831" s="T117">np:S</ta>
            <ta e="T119" id="Seg_2832" s="T118">v:pred</ta>
            <ta e="T120" id="Seg_2833" s="T119">pro.h:S</ta>
            <ta e="T121" id="Seg_2834" s="T120">v:pred</ta>
            <ta e="T122" id="Seg_2835" s="T121">np:O</ta>
            <ta e="T123" id="Seg_2836" s="T122">v:pred 0.3.h:S</ta>
            <ta e="T125" id="Seg_2837" s="T124">v:pred 0.3.h:S</ta>
            <ta e="T126" id="Seg_2838" s="T125">v:pred 0.2.h:S</ta>
            <ta e="T129" id="Seg_2839" s="T128">ptcl.neg</ta>
            <ta e="T130" id="Seg_2840" s="T129">v:pred 0.1.h:S</ta>
            <ta e="T131" id="Seg_2841" s="T130">pro.h:S</ta>
            <ta e="T137" id="Seg_2842" s="T136">v:pred 0.1.h:S</ta>
            <ta e="T140" id="Seg_2843" s="T139">v:pred 0.3.h:S</ta>
            <ta e="T141" id="Seg_2844" s="T140">v:pred 0.3.h:S</ta>
            <ta e="T143" id="Seg_2845" s="T142">np.h:S</ta>
            <ta e="T144" id="Seg_2846" s="T143">v:pred</ta>
            <ta e="T147" id="Seg_2847" s="T146">np:S</ta>
            <ta e="T148" id="Seg_2848" s="T147">v:pred</ta>
            <ta e="T149" id="Seg_2849" s="T148">np:O</ta>
            <ta e="T151" id="Seg_2850" s="T150">v:pred 0.3.h:S</ta>
            <ta e="T152" id="Seg_2851" s="T151">pro:O</ta>
            <ta e="T153" id="Seg_2852" s="T152">v:pred 0.2.h:S</ta>
            <ta e="T155" id="Seg_2853" s="T154">np:O</ta>
            <ta e="T157" id="Seg_2854" s="T156">ptcl:pred</ta>
            <ta e="T158" id="Seg_2855" s="T157">pro.h:S</ta>
            <ta e="T159" id="Seg_2856" s="T158">np:O</ta>
            <ta e="T160" id="Seg_2857" s="T159">v:pred</ta>
            <ta e="T163" id="Seg_2858" s="T162">v:pred 0.3.h:S</ta>
            <ta e="T164" id="Seg_2859" s="T163">v:pred 0.2.h:S</ta>
            <ta e="T169" id="Seg_2860" s="T168">ptcl.neg</ta>
            <ta e="T170" id="Seg_2861" s="T169">v:pred</ta>
            <ta e="T171" id="Seg_2862" s="T170">v:pred 0.3.h:S</ta>
            <ta e="T172" id="Seg_2863" s="T171">v:pred 0.3.h:S</ta>
            <ta e="T173" id="Seg_2864" s="T172">v:pred 0.3.h:S</ta>
            <ta e="T176" id="Seg_2865" s="T175">np.h:S</ta>
            <ta e="T177" id="Seg_2866" s="T176">v:pred</ta>
            <ta e="T178" id="Seg_2867" s="T177">np:O</ta>
            <ta e="T181" id="Seg_2868" s="T180">v:pred 0.3:S</ta>
            <ta e="T184" id="Seg_2869" s="T183">v:pred 0.3.h:S</ta>
            <ta e="T187" id="Seg_2870" s="T186">pro.h:S</ta>
            <ta e="T188" id="Seg_2871" s="T187">v:pred</ta>
            <ta e="T190" id="Seg_2872" s="T189">v:pred 0.2.h:S</ta>
            <ta e="T192" id="Seg_2873" s="T191">v:pred 0.2.h:S</ta>
            <ta e="T194" id="Seg_2874" s="T193">ptcl:pred</ta>
            <ta e="T196" id="Seg_2875" s="T195">v:pred 0.2.h:S</ta>
            <ta e="T198" id="Seg_2876" s="T197">v:pred 0.2.h:S</ta>
            <ta e="T202" id="Seg_2877" s="T201">pro.h:S</ta>
            <ta e="T203" id="Seg_2878" s="T202">v:pred</ta>
            <ta e="T204" id="Seg_2879" s="T203">v:pred 0.2.h:S</ta>
            <ta e="T205" id="Seg_2880" s="T204">v:pred 0.3.h:S</ta>
            <ta e="T206" id="Seg_2881" s="T205">np.h:S</ta>
            <ta e="T207" id="Seg_2882" s="T206">v:pred</ta>
            <ta e="T210" id="Seg_2883" s="T209">v:pred 0.3.h:S</ta>
            <ta e="T211" id="Seg_2884" s="T210">v:pred 0.3.h:S</ta>
            <ta e="T213" id="Seg_2885" s="T212">v:pred 0.3.h:S</ta>
            <ta e="T215" id="Seg_2886" s="T214">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T12" id="Seg_2887" s="T11">RUS:cult</ta>
            <ta e="T15" id="Seg_2888" s="T14">RUS:gram</ta>
            <ta e="T17" id="Seg_2889" s="T16">RUS:gram</ta>
            <ta e="T20" id="Seg_2890" s="T19">TURK:gram(INDEF)</ta>
            <ta e="T27" id="Seg_2891" s="T26">RUS:gram</ta>
            <ta e="T29" id="Seg_2892" s="T28">RUS:gram</ta>
            <ta e="T38" id="Seg_2893" s="T37">RUS:gram</ta>
            <ta e="T41" id="Seg_2894" s="T40">RUS:gram</ta>
            <ta e="T46" id="Seg_2895" s="T45">RUS:gram</ta>
            <ta e="T49" id="Seg_2896" s="T48">RUS:gram</ta>
            <ta e="T51" id="Seg_2897" s="T50">RUS:gram</ta>
            <ta e="T56" id="Seg_2898" s="T55">RUS:gram</ta>
            <ta e="T58" id="Seg_2899" s="T57">RUS:gram</ta>
            <ta e="T67" id="Seg_2900" s="T66">RUS:gram</ta>
            <ta e="T70" id="Seg_2901" s="T69">RUS:gram</ta>
            <ta e="T72" id="Seg_2902" s="T71">RUS:gram</ta>
            <ta e="T76" id="Seg_2903" s="T75">RUS:gram</ta>
            <ta e="T78" id="Seg_2904" s="T77">RUS:gram</ta>
            <ta e="T88" id="Seg_2905" s="T87">RUS:gram</ta>
            <ta e="T93" id="Seg_2906" s="T92">RUS:gram</ta>
            <ta e="T95" id="Seg_2907" s="T94">RUS:gram</ta>
            <ta e="T97" id="Seg_2908" s="T96">RUS:gram</ta>
            <ta e="T100" id="Seg_2909" s="T99">RUS:gram</ta>
            <ta e="T107" id="Seg_2910" s="T106">TURK:cult</ta>
            <ta e="T109" id="Seg_2911" s="T108">TURK:disc</ta>
            <ta e="T115" id="Seg_2912" s="T114">RUS:gram</ta>
            <ta e="T124" id="Seg_2913" s="T123">TURK:cult</ta>
            <ta e="T128" id="Seg_2914" s="T127">TURK:disc</ta>
            <ta e="T132" id="Seg_2915" s="T131">RUS:mod</ta>
            <ta e="T145" id="Seg_2916" s="T144">RUS:gram</ta>
            <ta e="T147" id="Seg_2917" s="T146">RUS:cult</ta>
            <ta e="T150" id="Seg_2918" s="T149">RUS:cult</ta>
            <ta e="T154" id="Seg_2919" s="T153">RUS:gram</ta>
            <ta e="T157" id="Seg_2920" s="T156">RUS:mod</ta>
            <ta e="T159" id="Seg_2921" s="T158">RUS:cult</ta>
            <ta e="T162" id="Seg_2922" s="T161">RUS:gram</ta>
            <ta e="T168" id="Seg_2923" s="T167">TURK:disc</ta>
            <ta e="T178" id="Seg_2924" s="T177">RUS:cult</ta>
            <ta e="T179" id="Seg_2925" s="T178">RUS:gram</ta>
            <ta e="T180" id="Seg_2926" s="T179">RUS:cult</ta>
            <ta e="T182" id="Seg_2927" s="T181">RUS:gram</ta>
            <ta e="T183" id="Seg_2928" s="T182">RUS:cult</ta>
            <ta e="T185" id="Seg_2929" s="T184">TURK:cult</ta>
            <ta e="T186" id="Seg_2930" s="T185">RUS:gram</ta>
            <ta e="T194" id="Seg_2931" s="T193">RUS:mod</ta>
            <ta e="T199" id="Seg_2932" s="T198">RUS:gram</ta>
            <ta e="T201" id="Seg_2933" s="T199">RUS:gram</ta>
            <ta e="T208" id="Seg_2934" s="T207">RUS:gram</ta>
            <ta e="T209" id="Seg_2935" s="T208">RUS:cult</ta>
            <ta e="T214" id="Seg_2936" s="T213">RUS:gram</ta>
            <ta e="T217" id="Seg_2937" s="T216">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_2938" s="T0">Жили женщина и мужчина.</ta>
            <ta e="T7" id="Seg_2939" s="T3">У них был сын.</ta>
            <ta e="T9" id="Seg_2940" s="T7">Его звали Лутонюшка.</ta>
            <ta e="T14" id="Seg_2941" s="T9">Женщина клала дрова в печь.</ta>
            <ta e="T21" id="Seg_2942" s="T14">А сын и муж что-то делали на улице.</ta>
            <ta e="T26" id="Seg_2943" s="T21">Женщина уронила полено на пол.</ta>
            <ta e="T30" id="Seg_2944" s="T26">Стала плакать и кричать.</ta>
            <ta e="T34" id="Seg_2945" s="T30">Муж пришел: "Почему ты плачешь?"</ta>
            <ta e="T54" id="Seg_2946" s="T34">"Да если бы у Лутонюшки была жена и у них был ребенок, и он бы сидел здесь, я бы его убила этим поленом!"</ta>
            <ta e="T59" id="Seg_2947" s="T54">Тогда и муж стал кричать.</ta>
            <ta e="T64" id="Seg_2948" s="T59">Тогда их сын пришел: "Почему вы кричите?"</ta>
            <ta e="T69" id="Seg_2949" s="T64">Если бы у тебя была жена.</ta>
            <ta e="T81" id="Seg_2950" s="T69">И если бы у тебя был ребенок и сидел бы здесь, я убила бы его поленом".</ta>
            <ta e="T84" id="Seg_2951" s="T81">Потом он говорит… </ta>
            <ta e="T86" id="Seg_2952" s="T84">Шапку надел [и говорит]: </ta>
            <ta e="T90" id="Seg_2953" s="T86">"Живите, а я уйду.</ta>
            <ta e="T102" id="Seg_2954" s="T90">Если я найду таких [людей], как вы, то вернусь, а если не найду, не вернусь".</ta>
            <ta e="T105" id="Seg_2955" s="T102">Потом он идет, идет.</ta>
            <ta e="T110" id="Seg_2956" s="T105">Люди сажают корову на крышу бани.</ta>
            <ta e="T114" id="Seg_2957" s="T110">Он говорит: "Что [вы] делаете?"</ta>
            <ta e="T119" id="Seg_2958" s="T114">"Видишь, там трава растет".</ta>
            <ta e="T125" id="Seg_2959" s="T119">Он залез, сорвал траву, дал ее корове.</ta>
            <ta e="T127" id="Seg_2960" s="T125">"Живи со мной [= с нами]!"</ta>
            <ta e="T130" id="Seg_2961" s="T127">"Нет, мы не будем жить!</ta>
            <ta e="T138" id="Seg_2962" s="T130">У меня еще таких людей много, я пойду далеко [чтобы помогать им]". </ta>
            <ta e="T141" id="Seg_2963" s="T138">Потом он идет, идет.</ta>
            <ta e="T144" id="Seg_2964" s="T141">Много людей стоит.</ta>
            <ta e="T148" id="Seg_2965" s="T144">И хомут висит.</ta>
            <ta e="T151" id="Seg_2966" s="T148">[Они] загоняют лошадь в хомут.</ta>
            <ta e="T153" id="Seg_2967" s="T151">"Что вы делаете?"</ta>
            <ta e="T157" id="Seg_2968" s="T153">"Да лошадь надо запрячь".</ta>
            <ta e="T163" id="Seg_2969" s="T157">Он надел хомут на лошадь и (запряг?) ее.</ta>
            <ta e="T167" id="Seg_2970" s="T163">"Живи со мной [= с нами]!"</ta>
            <ta e="T170" id="Seg_2971" s="T167">"Нет, [я] не буду жить!"</ta>
            <ta e="T174" id="Seg_2972" s="T170">Шел-шел, пришел к женщине.</ta>
            <ta e="T178" id="Seg_2973" s="T174">Женщина варит кисель.</ta>
            <ta e="T181" id="Seg_2974" s="T178">И поставила его на стол.</ta>
            <ta e="T185" id="Seg_2975" s="T181">И ходит в погреб за сметаной.</ta>
            <ta e="T191" id="Seg_2976" s="T185">Он говорит: "Зачем ты носишь ее ложкой?</ta>
            <ta e="T201" id="Seg_2977" s="T191">Смотри, надо поставить ее сюда, принеси ее и…"</ta>
            <ta e="T205" id="Seg_2978" s="T201">Она пошла, принесла, они сели. [?]</ta>
            <ta e="T211" id="Seg_2979" s="T205">Лутонюшка поел, лег на полати и заснул.</ta>
            <ta e="T218" id="Seg_2980" s="T211">Утром встал и вернулся к отцу и матери.</ta>
            <ta e="T219" id="Seg_2981" s="T218">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_2982" s="T0">There lived a woman, a man.</ta>
            <ta e="T7" id="Seg_2983" s="T3">They had a son.</ta>
            <ta e="T9" id="Seg_2984" s="T7">His name was Lutonyushka.</ta>
            <ta e="T14" id="Seg_2985" s="T9">The woman put wood into the stove.</ta>
            <ta e="T21" id="Seg_2986" s="T14">And the son and the husband did something outside.</ta>
            <ta e="T26" id="Seg_2987" s="T21">The woman dropped a [piece of] wood on the ground.</ta>
            <ta e="T30" id="Seg_2988" s="T26">She started to cry and shout.</ta>
            <ta e="T34" id="Seg_2989" s="T30">Her husband came: "Why are you crying?".</ta>
            <ta e="T54" id="Seg_2990" s="T34">"Well if our Lutonyushka had a wife and they had a child, and s/he would sit there, I would kill it with the piece of wood!".</ta>
            <ta e="T59" id="Seg_2991" s="T54">Then the husband started to shout.</ta>
            <ta e="T64" id="Seg_2992" s="T59">Then his son came: "Why are you shouting?".</ta>
            <ta e="T69" id="Seg_2993" s="T64">"If you had a wife.</ta>
            <ta e="T81" id="Seg_2994" s="T69">And if you had a child, s/he would sit there but I would have killed them with this piece of wood".</ta>
            <ta e="T84" id="Seg_2995" s="T81">Then he says… </ta>
            <ta e="T86" id="Seg_2996" s="T84">He put on his hat [and says]: </ta>
            <ta e="T90" id="Seg_2997" s="T86">"Live, and I will leave.</ta>
            <ta e="T102" id="Seg_2998" s="T90">If I find such [people] as you, then I wil return, and if I do not find, I will not return."</ta>
            <ta e="T105" id="Seg_2999" s="T102">Then he comes, he comes.</ta>
            <ta e="T110" id="Seg_3000" s="T105">People are setting a cow on top of a sauna.</ta>
            <ta e="T114" id="Seg_3001" s="T110">He says: "What are you doing?".</ta>
            <ta e="T119" id="Seg_3002" s="T114">"You see, there is grass there."</ta>
            <ta e="T125" id="Seg_3003" s="T119">He climbed, cut the grass, gave it to the cow.</ta>
            <ta e="T127" id="Seg_3004" s="T125">"Live with me [=us]!"</ta>
            <ta e="T130" id="Seg_3005" s="T127">"No, we will not live!</ta>
            <ta e="T138" id="Seg_3006" s="T130">I [have] many people like you, I will go far [to help them]." </ta>
            <ta e="T141" id="Seg_3007" s="T138">Then he goes, he goes.</ta>
            <ta e="T144" id="Seg_3008" s="T141">A lot of people are standing.</ta>
            <ta e="T148" id="Seg_3009" s="T144">A horse collar is hanging.</ta>
            <ta e="T151" id="Seg_3010" s="T148">[They] are driving a horse into the horse collar.</ta>
            <ta e="T153" id="Seg_3011" s="T151">"What are you doing?"</ta>
            <ta e="T157" id="Seg_3012" s="T153">"We need to harness the horse."</ta>
            <ta e="T163" id="Seg_3013" s="T157">He put the collar on the horse and (harnessed?) it.</ta>
            <ta e="T167" id="Seg_3014" s="T163">"Live with me [us]!".</ta>
            <ta e="T170" id="Seg_3015" s="T167">"No, [I] won't live!"</ta>
            <ta e="T174" id="Seg_3016" s="T170">He goes, he goes, he came to a woman.</ta>
            <ta e="T178" id="Seg_3017" s="T174">The woman is cooking her kissel.</ta>
            <ta e="T181" id="Seg_3018" s="T178">And she put it on the table.</ta>
            <ta e="T185" id="Seg_3019" s="T181">And she is going to the cellar with cream.</ta>
            <ta e="T191" id="Seg_3020" s="T185">And he says: "Why do you carry it with a spoon.</ta>
            <ta e="T201" id="Seg_3021" s="T191">You see, you need to put [it] here, go bring it and…"</ta>
            <ta e="T205" id="Seg_3022" s="T201">He went, brought it, they sit down. [?]</ta>
            <ta e="T211" id="Seg_3023" s="T205">Lutonjushka ate, lied down on the bed and fell asleep.</ta>
            <ta e="T218" id="Seg_3024" s="T211">In the morning he got up and returned to his father and mother.</ta>
            <ta e="T219" id="Seg_3025" s="T218">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_3026" s="T0">Es lebten eine Frau und ein Mann.</ta>
            <ta e="T7" id="Seg_3027" s="T3">Sie hatten einen Sohn.</ta>
            <ta e="T9" id="Seg_3028" s="T7">Er hieß Lutonjuschka.</ta>
            <ta e="T14" id="Seg_3029" s="T9">Die Frau legte Holz in den Ofen.</ta>
            <ta e="T21" id="Seg_3030" s="T14">Und der Sohn und der Mann machten etwas draußen.</ta>
            <ta e="T26" id="Seg_3031" s="T21">Die Frau ließ ein Stück Holz auf den Boden fallen.</ta>
            <ta e="T30" id="Seg_3032" s="T26">Sie fing an zu weinen und zu schreien.</ta>
            <ta e="T34" id="Seg_3033" s="T30">Ihr Mann kam: "Warum weinst du?"</ta>
            <ta e="T54" id="Seg_3034" s="T34">"Nun, wenn unser Lutonjuschka eine Frau hätte und sie ein Kind hätten und es hier sitzen würde, dann würde ich es mit dem Stück Holz töten!"</ta>
            <ta e="T59" id="Seg_3035" s="T54">Dann fing der Mann an zu schreien.</ta>
            <ta e="T64" id="Seg_3036" s="T59">Dann kam sein Sohn: "Warum schreist du?"</ta>
            <ta e="T69" id="Seg_3037" s="T64">"Wenn du eine Frau hättest.</ta>
            <ta e="T81" id="Seg_3038" s="T69">Und wenn du ein Kind hättest, dann würde es hier sitzen und ich hätte es mit diesem Stück Holz getötet."</ta>
            <ta e="T84" id="Seg_3039" s="T81">Dann sagt er… </ta>
            <ta e="T86" id="Seg_3040" s="T84">Er setzte seinen Hut auf [und sagt]: </ta>
            <ta e="T90" id="Seg_3041" s="T86">"Lebt und ich werde gehen.</ta>
            <ta e="T102" id="Seg_3042" s="T90">Wenn ich solche [Leute] wie ihr finde, dann komme ich zurück, wenn ich sie nicht finde, komme ich nicht zurück."</ta>
            <ta e="T105" id="Seg_3043" s="T102">Dann kommt er, er kommt.</ta>
            <ta e="T110" id="Seg_3044" s="T105">Leute setzen eine Kuh auf ein Badehaus.</ta>
            <ta e="T114" id="Seg_3045" s="T110">Er sagt: "Was macht ihr?"</ta>
            <ta e="T119" id="Seg_3046" s="T114">"Du siehst, dort ist Gras."</ta>
            <ta e="T125" id="Seg_3047" s="T119">Er kletterte, schnitt das Gras, gab es der Kuh.</ta>
            <ta e="T127" id="Seg_3048" s="T125">"Lebe mit mir [=uns]!"</ta>
            <ta e="T130" id="Seg_3049" s="T127">"Nein, werden nicht leben!</ta>
            <ta e="T138" id="Seg_3050" s="T130">Ich [habe] viele Leute wie du, ich gehe weit weg [um ihnen zu helfen]." </ta>
            <ta e="T141" id="Seg_3051" s="T138">Dann geht er, er geht.</ta>
            <ta e="T144" id="Seg_3052" s="T141">Viele Leute stehen [da].</ta>
            <ta e="T148" id="Seg_3053" s="T144">Ein Pferdegeschirr hängt [da].</ta>
            <ta e="T151" id="Seg_3054" s="T148">[Sie] treiben ein Pferd in das Pferdegeschirr.</ta>
            <ta e="T153" id="Seg_3055" s="T151">"Was macht ihr?"</ta>
            <ta e="T157" id="Seg_3056" s="T153">"Wir müssen das Pferd anspannen."</ta>
            <ta e="T163" id="Seg_3057" s="T157">Er legt dem Pferd das Geschirr an und spannte es (an?).</ta>
            <ta e="T167" id="Seg_3058" s="T163">"Leb mit mir [=uns]!"</ta>
            <ta e="T170" id="Seg_3059" s="T167">"Nein, [ich] werde nicht leben!"</ta>
            <ta e="T174" id="Seg_3060" s="T170">Er geht, er geht, er kommt zu einer Frau.</ta>
            <ta e="T178" id="Seg_3061" s="T174">Die Frau kocht ihren Kissel.</ta>
            <ta e="T181" id="Seg_3062" s="T178">Und sie stellte ihn auf den Tisch.</ta>
            <ta e="T185" id="Seg_3063" s="T181">Und sie geht mit Sahne in den Keller.</ta>
            <ta e="T191" id="Seg_3064" s="T185">Und er sagt: "Warum trägst du sie mit einem Löffel.</ta>
            <ta e="T201" id="Seg_3065" s="T191">Siehst du, du musst [sie] hier hinlegen, bring sie und…"</ta>
            <ta e="T205" id="Seg_3066" s="T201">Er ging, brachte sie, sie setzten sich. [?]</ta>
            <ta e="T211" id="Seg_3067" s="T205">Lutonjuschka aß, legte sich auf das Bett und schlief ein.</ta>
            <ta e="T218" id="Seg_3068" s="T211">Am Morgen stand er auf und kehrte zu seinem Vater und seiner Mutter zurück.</ta>
            <ta e="T219" id="Seg_3069" s="T218">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T3" id="Seg_3070" s="T0">[GVY:] http://hyaenidae.narod.ru/story2/088.html</ta>
            <ta e="T114" id="Seg_3071" s="T110">[GVY:] allaʔbəʔjə?</ta>
            <ta e="T138" id="Seg_3072" s="T130">[GVY:] kuŋgeʔ?</ta>
            <ta e="T148" id="Seg_3073" s="T144">Ru. хомут 'horse collar'.</ta>
            <ta e="T163" id="Seg_3074" s="T157">[GVY:] ködərbi = körerbi?</ta>
            <ta e="T170" id="Seg_3075" s="T167">[AAV] ej instead of em (NEG.AUX)?</ta>
            <ta e="T205" id="Seg_3076" s="T201">[GVY:] The forms of deʔtə and amnolbiʔi are unclear.</ta>
            <ta e="T211" id="Seg_3077" s="T205">[GVY:] Полати - bed behind the oven</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T220" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
