<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID3051181B-075A-002D-6627-A9F3DE7EB3D1">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_SixFools_flk.wav" />
         <referenced-file url="PKZ_196X_SixFools_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_SixFools_flk\PKZ_196X_SixFools_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">138</ud-information>
            <ud-information attribute-name="# HIAT:w">92</ud-information>
            <ud-information attribute-name="# e">92</ud-information>
            <ud-information attribute-name="# HIAT:u">22</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T215" time="0.0" type="appl" />
         <tli id="T216" time="1.062" type="appl" />
         <tli id="T217" time="2.124" type="appl" />
         <tli id="T218" time="3.186" type="appl" />
         <tli id="T219" time="5.8731498425858035" />
         <tli id="T220" time="7.041" type="appl" />
         <tli id="T221" time="7.975" type="appl" />
         <tli id="T222" time="8.909" type="appl" />
         <tli id="T223" time="9.843" type="appl" />
         <tli id="T224" time="10.697" type="appl" />
         <tli id="T225" time="11.551" type="appl" />
         <tli id="T226" time="12.405" type="appl" />
         <tli id="T227" time="13.259" type="appl" />
         <tli id="T228" time="14.141" type="appl" />
         <tli id="T229" time="15.023" type="appl" />
         <tli id="T230" time="15.905" type="appl" />
         <tli id="T231" time="16.787" type="appl" />
         <tli id="T232" time="18.00610411444297" />
         <tli id="T233" time="18.829" type="appl" />
         <tli id="T234" time="19.351" type="appl" />
         <tli id="T235" time="19.95199709629967" />
         <tli id="T236" time="20.697" type="appl" />
         <tli id="T237" time="21.522" type="appl" />
         <tli id="T238" time="21.962" type="appl" />
         <tli id="T239" time="22.402" type="appl" />
         <tli id="T240" time="22.842" type="appl" />
         <tli id="T241" time="23.282" type="appl" />
         <tli id="T242" time="23.721" type="appl" />
         <tli id="T243" time="24.161" type="appl" />
         <tli id="T244" time="24.601" type="appl" />
         <tli id="T245" time="25.041" type="appl" />
         <tli id="T246" time="25.478" type="appl" />
         <tli id="T247" time="25.915" type="appl" />
         <tli id="T248" time="26.351" type="appl" />
         <tli id="T249" time="26.788" type="appl" />
         <tli id="T250" time="27.225" type="appl" />
         <tli id="T251" time="28.167" type="appl" />
         <tli id="T252" time="29.109" type="appl" />
         <tli id="T253" time="30.051" type="appl" />
         <tli id="T254" time="30.994" type="appl" />
         <tli id="T255" time="31.936" type="appl" />
         <tli id="T256" time="32.878" type="appl" />
         <tli id="T257" time="33.82" type="appl" />
         <tli id="T258" time="34.762" type="appl" />
         <tli id="T259" time="35.365" type="appl" />
         <tli id="T260" time="35.967" type="appl" />
         <tli id="T261" time="36.57" type="appl" />
         <tli id="T262" time="37.172" type="appl" />
         <tli id="T263" time="37.775" type="appl" />
         <tli id="T264" time="38.377" type="appl" />
         <tli id="T265" time="39.05999712351447" />
         <tli id="T266" time="39.705" type="appl" />
         <tli id="T267" time="40.43" type="appl" />
         <tli id="T268" time="41.154" type="appl" />
         <tli id="T269" time="41.879" type="appl" />
         <tli id="T270" time="42.604" type="appl" />
         <tli id="T271" time="43.329" type="appl" />
         <tli id="T272" time="44.667" type="appl" />
         <tli id="T273" time="46.006" type="appl" />
         <tli id="T274" time="47.344" type="appl" />
         <tli id="T275" time="48.682" type="appl" />
         <tli id="T276" time="49.999" type="appl" />
         <tli id="T277" time="51.074" type="appl" />
         <tli id="T278" time="52.15" type="appl" />
         <tli id="T279" time="53.225" type="appl" />
         <tli id="T280" time="54.301" type="appl" />
         <tli id="T281" time="55.079" type="appl" />
         <tli id="T282" time="55.857" type="appl" />
         <tli id="T283" time="56.86489007634155" />
         <tli id="T284" time="58.662" type="appl" />
         <tli id="T285" time="60.323" type="appl" />
         <tli id="T286" time="61.985" type="appl" />
         <tli id="T287" time="63.103" type="appl" />
         <tli id="T288" time="64.221" type="appl" />
         <tli id="T289" time="65.34" type="appl" />
         <tli id="T290" time="66.458" type="appl" />
         <tli id="T291" time="67.576" type="appl" />
         <tli id="T292" time="68.351" type="appl" />
         <tli id="T293" time="69.126" type="appl" />
         <tli id="T294" time="69.902" type="appl" />
         <tli id="T295" time="70.677" type="appl" />
         <tli id="T296" time="71.452" type="appl" />
         <tli id="T297" time="72.279" type="appl" />
         <tli id="T298" time="72.96" type="appl" />
         <tli id="T299" time="73.642" type="appl" />
         <tli id="T300" time="74.81099606526435" />
         <tli id="T301" time="76.538" type="appl" />
         <tli id="T302" time="78.25088859508759" />
         <tli id="T303" time="79.101" type="appl" />
         <tli id="T304" time="79.923" type="appl" />
         <tli id="T305" time="81.44412216443902" />
         <tli id="T306" time="82.818" type="appl" />
         <tli id="T307" time="84.104" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T307" id="Seg_0" n="sc" s="T215">
               <ts e="T219" id="Seg_2" n="HIAT:u" s="T215">
                  <ts e="T216" id="Seg_4" n="HIAT:w" s="T215">Muktuʔ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_7" n="HIAT:w" s="T216">kuzaʔi</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_10" n="HIAT:w" s="T217">sagəšsəbi</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_13" n="HIAT:w" s="T218">kambiʔi</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T223" id="Seg_17" n="HIAT:u" s="T219">
                  <ts e="T220" id="Seg_19" n="HIAT:w" s="T219">Tĭlzittə</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_22" n="HIAT:w" s="T220">dʼü</ts>
                  <nts id="Seg_23" n="HIAT:ip">,</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_26" n="HIAT:w" s="T221">kös</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_29" n="HIAT:w" s="T222">nendəsʼtə</ts>
                  <nts id="Seg_30" n="HIAT:ip">.</nts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T227" id="Seg_33" n="HIAT:u" s="T223">
                  <ts e="T224" id="Seg_35" n="HIAT:w" s="T223">Dĭgəttə</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_38" n="HIAT:w" s="T224">üjüzaŋdə</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_41" n="HIAT:w" s="T225">dibər</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_44" n="HIAT:w" s="T226">păʔpiʔi</ts>
                  <nts id="Seg_45" n="HIAT:ip">.</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T232" id="Seg_48" n="HIAT:u" s="T227">
                  <ts e="T228" id="Seg_50" n="HIAT:w" s="T227">Măndəʔi:</ts>
                  <nts id="Seg_51" n="HIAT:ip">"</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_54" n="HIAT:w" s="T228">Šindən</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_57" n="HIAT:w" s="T229">ujut</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_60" n="HIAT:w" s="T230">ej</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_63" n="HIAT:w" s="T231">mobiʔi</ts>
                  <nts id="Seg_64" n="HIAT:ip">"</nts>
                  <nts id="Seg_65" n="HIAT:ip">.</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T235" id="Seg_68" n="HIAT:u" s="T232">
                  <ts e="T233" id="Seg_70" n="HIAT:w" s="T232">Dĭgəttə</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_73" n="HIAT:w" s="T233">šobi</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_76" n="HIAT:w" s="T234">kuza</ts>
                  <nts id="Seg_77" n="HIAT:ip">.</nts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T237" id="Seg_80" n="HIAT:u" s="T235">
                  <ts e="T236" id="Seg_82" n="HIAT:w" s="T235">Ĭmbi</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_85" n="HIAT:w" s="T236">amnolaʔbəʔjə</ts>
                  <nts id="Seg_86" n="HIAT:ip">.</nts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T245" id="Seg_89" n="HIAT:u" s="T237">
                  <ts e="T238" id="Seg_91" n="HIAT:w" s="T237">Dĭ</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_93" n="HIAT:ip">(</nts>
                  <ts e="T239" id="Seg_95" n="HIAT:w" s="T238">m-</ts>
                  <nts id="Seg_96" n="HIAT:ip">)</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_99" n="HIAT:w" s="T239">miʔ</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_102" n="HIAT:w" s="T240">ujuʔi</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_105" n="HIAT:w" s="T241">ej</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_107" n="HIAT:ip">(</nts>
                  <ts e="T243" id="Seg_109" n="HIAT:w" s="T242">tĭmniem</ts>
                  <nts id="Seg_110" n="HIAT:ip">)</nts>
                  <nts id="Seg_111" n="HIAT:ip">,</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_114" n="HIAT:w" s="T243">šindin</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_117" n="HIAT:w" s="T244">girgit</ts>
                  <nts id="Seg_118" n="HIAT:ip">.</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T258" id="Seg_121" n="HIAT:u" s="T245">
                  <nts id="Seg_122" n="HIAT:ip">(</nts>
                  <ts e="T246" id="Seg_124" n="HIAT:w" s="T245">Dĭ</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_127" n="HIAT:w" s="T246">pa</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_130" n="HIAT:w" s="T247">ib-</ts>
                  <nts id="Seg_131" n="HIAT:ip">)</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_134" n="HIAT:w" s="T248">Dĭ</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_137" n="HIAT:w" s="T249">măndə:</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_140" n="HIAT:w" s="T250">Baltuʔi</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_143" n="HIAT:w" s="T251">mĭbileʔ</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_146" n="HIAT:w" s="T252">dak</ts>
                  <nts id="Seg_147" n="HIAT:ip">,</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_150" n="HIAT:w" s="T253">dĭgəttə</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_153" n="HIAT:w" s="T254">măn</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_156" n="HIAT:w" s="T255">šiʔnʼileʔ</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_159" n="HIAT:w" s="T256">alam</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_162" n="HIAT:w" s="T257">jakšə</ts>
                  <nts id="Seg_163" n="HIAT:ip">.</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T265" id="Seg_166" n="HIAT:u" s="T258">
                  <ts e="T259" id="Seg_168" n="HIAT:w" s="T258">Dĭgəttə</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_171" n="HIAT:w" s="T259">ibi</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_174" n="HIAT:w" s="T260">pa</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_177" n="HIAT:w" s="T261">da</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_180" n="HIAT:w" s="T262">davaj</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_183" n="HIAT:w" s="T263">dĭzeŋ</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_186" n="HIAT:w" s="T264">münörzittə</ts>
                  <nts id="Seg_187" n="HIAT:ip">.</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T271" id="Seg_190" n="HIAT:u" s="T265">
                  <ts e="T266" id="Seg_192" n="HIAT:w" s="T265">Dĭzeŋ</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_195" n="HIAT:w" s="T266">bar</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_198" n="HIAT:w" s="T267">suʔmiluʔpiʔi</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_201" n="HIAT:w" s="T268">i</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_203" n="HIAT:ip">(</nts>
                  <ts e="T270" id="Seg_205" n="HIAT:w" s="T269">nu-</ts>
                  <nts id="Seg_206" n="HIAT:ip">)</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_209" n="HIAT:w" s="T270">nuʔməluʔpiʔi</ts>
                  <nts id="Seg_210" n="HIAT:ip">.</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T275" id="Seg_213" n="HIAT:u" s="T271">
                  <ts e="T272" id="Seg_215" n="HIAT:w" s="T271">Dĭgəttə</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_218" n="HIAT:w" s="T272">măndəʔi:</ts>
                  <nts id="Seg_219" n="HIAT:ip">"</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_222" n="HIAT:w" s="T273">Nada</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_225" n="HIAT:w" s="T274">pa</ts>
                  <nts id="Seg_226" n="HIAT:ip">"</nts>
                  <nts id="Seg_227" n="HIAT:ip">.</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T280" id="Seg_230" n="HIAT:u" s="T275">
                  <ts e="T276" id="Seg_232" n="HIAT:w" s="T275">Onʼiʔ</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_235" n="HIAT:w" s="T276">măndə:</ts>
                  <nts id="Seg_236" n="HIAT:ip">"</nts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_239" n="HIAT:w" s="T277">Măn</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_242" n="HIAT:w" s="T278">mu</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_245" n="HIAT:w" s="T279">băldəlim</ts>
                  <nts id="Seg_246" n="HIAT:ip">"</nts>
                  <nts id="Seg_247" n="HIAT:ip">.</nts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T283" id="Seg_250" n="HIAT:u" s="T280">
                  <ts e="T281" id="Seg_252" n="HIAT:w" s="T280">Dĭgəttə</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_255" n="HIAT:w" s="T281">băldəbi</ts>
                  <nts id="Seg_256" n="HIAT:ip">,</nts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_259" n="HIAT:w" s="T282">băldəbi</ts>
                  <nts id="Seg_260" n="HIAT:ip">.</nts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T286" id="Seg_263" n="HIAT:u" s="T283">
                  <ts e="T284" id="Seg_265" n="HIAT:w" s="T283">Onʼiʔ</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_268" n="HIAT:w" s="T284">ibi</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_271" n="HIAT:w" s="T285">ujutsiʔ</ts>
                  <nts id="Seg_272" n="HIAT:ip">.</nts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T291" id="Seg_275" n="HIAT:u" s="T286">
                  <ts e="T287" id="Seg_277" n="HIAT:w" s="T286">Dĭgəttə</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_280" n="HIAT:w" s="T287">bar</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_282" n="HIAT:ip">(</nts>
                  <ts e="T289" id="Seg_284" n="HIAT:w" s="T288">te-</ts>
                  <nts id="Seg_285" n="HIAT:ip">)</nts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_288" n="HIAT:w" s="T289">teʔtəguʔ</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_291" n="HIAT:w" s="T290">ibiʔi</ts>
                  <nts id="Seg_292" n="HIAT:ip">.</nts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T296" id="Seg_295" n="HIAT:u" s="T291">
                  <ts e="T292" id="Seg_297" n="HIAT:w" s="T291">Dĭgəttə</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_300" n="HIAT:w" s="T292">dĭ</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_303" n="HIAT:w" s="T293">măndə:</ts>
                  <nts id="Seg_304" n="HIAT:ip">"</nts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_307" n="HIAT:w" s="T294">Udam</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_310" n="HIAT:w" s="T295">tʼüʔpi</ts>
                  <nts id="Seg_311" n="HIAT:ip">.</nts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T297" id="Seg_314" n="HIAT:u" s="T296">
                  <ts e="T297" id="Seg_316" n="HIAT:w" s="T296">Kĭškəlim</ts>
                  <nts id="Seg_317" n="HIAT:ip">"</nts>
                  <nts id="Seg_318" n="HIAT:ip">.</nts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T300" id="Seg_321" n="HIAT:u" s="T297">
                  <ts e="T298" id="Seg_323" n="HIAT:w" s="T297">Dĭgəttə</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_326" n="HIAT:w" s="T298">öʔlüʔbi</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_328" n="HIAT:ip">(</nts>
                  <ts e="T300" id="Seg_330" n="HIAT:w" s="T299">bargo</ts>
                  <nts id="Seg_331" n="HIAT:ip">)</nts>
                  <nts id="Seg_332" n="HIAT:ip">.</nts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T301" id="Seg_335" n="HIAT:u" s="T300">
                  <ts e="T301" id="Seg_337" n="HIAT:w" s="T300">Saʔməluʔpiʔi</ts>
                  <nts id="Seg_338" n="HIAT:ip">.</nts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T302" id="Seg_341" n="HIAT:u" s="T301">
                  <ts e="T302" id="Seg_343" n="HIAT:w" s="T301">Dʼabərobiʔi</ts>
                  <nts id="Seg_344" n="HIAT:ip">.</nts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T305" id="Seg_347" n="HIAT:u" s="T302">
                  <ts e="T303" id="Seg_349" n="HIAT:w" s="T302">Ĭmbidə</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_352" n="HIAT:w" s="T303">ej</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_355" n="HIAT:w" s="T304">tĭmnebiʔi</ts>
                  <nts id="Seg_356" n="HIAT:ip">.</nts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T307" id="Seg_359" n="HIAT:u" s="T305">
                  <ts e="T306" id="Seg_361" n="HIAT:w" s="T305">I</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_364" n="HIAT:w" s="T306">kabarləj</ts>
                  <nts id="Seg_365" n="HIAT:ip">.</nts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T307" id="Seg_367" n="sc" s="T215">
               <ts e="T216" id="Seg_369" n="e" s="T215">Muktuʔ </ts>
               <ts e="T217" id="Seg_371" n="e" s="T216">kuzaʔi </ts>
               <ts e="T218" id="Seg_373" n="e" s="T217">sagəšsəbi </ts>
               <ts e="T219" id="Seg_375" n="e" s="T218">kambiʔi. </ts>
               <ts e="T220" id="Seg_377" n="e" s="T219">Tĭlzittə </ts>
               <ts e="T221" id="Seg_379" n="e" s="T220">dʼü, </ts>
               <ts e="T222" id="Seg_381" n="e" s="T221">kös </ts>
               <ts e="T223" id="Seg_383" n="e" s="T222">nendəsʼtə. </ts>
               <ts e="T224" id="Seg_385" n="e" s="T223">Dĭgəttə </ts>
               <ts e="T225" id="Seg_387" n="e" s="T224">üjüzaŋdə </ts>
               <ts e="T226" id="Seg_389" n="e" s="T225">dibər </ts>
               <ts e="T227" id="Seg_391" n="e" s="T226">păʔpiʔi. </ts>
               <ts e="T228" id="Seg_393" n="e" s="T227">Măndəʔi:" </ts>
               <ts e="T229" id="Seg_395" n="e" s="T228">Šindən </ts>
               <ts e="T230" id="Seg_397" n="e" s="T229">ujut </ts>
               <ts e="T231" id="Seg_399" n="e" s="T230">ej </ts>
               <ts e="T232" id="Seg_401" n="e" s="T231">mobiʔi". </ts>
               <ts e="T233" id="Seg_403" n="e" s="T232">Dĭgəttə </ts>
               <ts e="T234" id="Seg_405" n="e" s="T233">šobi </ts>
               <ts e="T235" id="Seg_407" n="e" s="T234">kuza. </ts>
               <ts e="T236" id="Seg_409" n="e" s="T235">Ĭmbi </ts>
               <ts e="T237" id="Seg_411" n="e" s="T236">amnolaʔbəʔjə. </ts>
               <ts e="T238" id="Seg_413" n="e" s="T237">Dĭ </ts>
               <ts e="T239" id="Seg_415" n="e" s="T238">(m-) </ts>
               <ts e="T240" id="Seg_417" n="e" s="T239">miʔ </ts>
               <ts e="T241" id="Seg_419" n="e" s="T240">ujuʔi </ts>
               <ts e="T242" id="Seg_421" n="e" s="T241">ej </ts>
               <ts e="T243" id="Seg_423" n="e" s="T242">(tĭmniem), </ts>
               <ts e="T244" id="Seg_425" n="e" s="T243">šindin </ts>
               <ts e="T245" id="Seg_427" n="e" s="T244">girgit. </ts>
               <ts e="T246" id="Seg_429" n="e" s="T245">(Dĭ </ts>
               <ts e="T247" id="Seg_431" n="e" s="T246">pa </ts>
               <ts e="T248" id="Seg_433" n="e" s="T247">ib-) </ts>
               <ts e="T249" id="Seg_435" n="e" s="T248">Dĭ </ts>
               <ts e="T250" id="Seg_437" n="e" s="T249">măndə: </ts>
               <ts e="T251" id="Seg_439" n="e" s="T250">Baltuʔi </ts>
               <ts e="T252" id="Seg_441" n="e" s="T251">mĭbileʔ </ts>
               <ts e="T253" id="Seg_443" n="e" s="T252">dak, </ts>
               <ts e="T254" id="Seg_445" n="e" s="T253">dĭgəttə </ts>
               <ts e="T255" id="Seg_447" n="e" s="T254">măn </ts>
               <ts e="T256" id="Seg_449" n="e" s="T255">šiʔnʼileʔ </ts>
               <ts e="T257" id="Seg_451" n="e" s="T256">alam </ts>
               <ts e="T258" id="Seg_453" n="e" s="T257">jakšə. </ts>
               <ts e="T259" id="Seg_455" n="e" s="T258">Dĭgəttə </ts>
               <ts e="T260" id="Seg_457" n="e" s="T259">ibi </ts>
               <ts e="T261" id="Seg_459" n="e" s="T260">pa </ts>
               <ts e="T262" id="Seg_461" n="e" s="T261">da </ts>
               <ts e="T263" id="Seg_463" n="e" s="T262">davaj </ts>
               <ts e="T264" id="Seg_465" n="e" s="T263">dĭzeŋ </ts>
               <ts e="T265" id="Seg_467" n="e" s="T264">münörzittə. </ts>
               <ts e="T266" id="Seg_469" n="e" s="T265">Dĭzeŋ </ts>
               <ts e="T267" id="Seg_471" n="e" s="T266">bar </ts>
               <ts e="T268" id="Seg_473" n="e" s="T267">suʔmiluʔpiʔi </ts>
               <ts e="T269" id="Seg_475" n="e" s="T268">i </ts>
               <ts e="T270" id="Seg_477" n="e" s="T269">(nu-) </ts>
               <ts e="T271" id="Seg_479" n="e" s="T270">nuʔməluʔpiʔi. </ts>
               <ts e="T272" id="Seg_481" n="e" s="T271">Dĭgəttə </ts>
               <ts e="T273" id="Seg_483" n="e" s="T272">măndəʔi:" </ts>
               <ts e="T274" id="Seg_485" n="e" s="T273">Nada </ts>
               <ts e="T275" id="Seg_487" n="e" s="T274">pa". </ts>
               <ts e="T276" id="Seg_489" n="e" s="T275">Onʼiʔ </ts>
               <ts e="T277" id="Seg_491" n="e" s="T276">măndə:" </ts>
               <ts e="T278" id="Seg_493" n="e" s="T277">Măn </ts>
               <ts e="T279" id="Seg_495" n="e" s="T278">mu </ts>
               <ts e="T280" id="Seg_497" n="e" s="T279">băldəlim". </ts>
               <ts e="T281" id="Seg_499" n="e" s="T280">Dĭgəttə </ts>
               <ts e="T282" id="Seg_501" n="e" s="T281">băldəbi, </ts>
               <ts e="T283" id="Seg_503" n="e" s="T282">băldəbi. </ts>
               <ts e="T284" id="Seg_505" n="e" s="T283">Onʼiʔ </ts>
               <ts e="T285" id="Seg_507" n="e" s="T284">ibi </ts>
               <ts e="T286" id="Seg_509" n="e" s="T285">ujutsiʔ. </ts>
               <ts e="T287" id="Seg_511" n="e" s="T286">Dĭgəttə </ts>
               <ts e="T288" id="Seg_513" n="e" s="T287">bar </ts>
               <ts e="T289" id="Seg_515" n="e" s="T288">(te-) </ts>
               <ts e="T290" id="Seg_517" n="e" s="T289">teʔtəguʔ </ts>
               <ts e="T291" id="Seg_519" n="e" s="T290">ibiʔi. </ts>
               <ts e="T292" id="Seg_521" n="e" s="T291">Dĭgəttə </ts>
               <ts e="T293" id="Seg_523" n="e" s="T292">dĭ </ts>
               <ts e="T294" id="Seg_525" n="e" s="T293">măndə:" </ts>
               <ts e="T295" id="Seg_527" n="e" s="T294">Udam </ts>
               <ts e="T296" id="Seg_529" n="e" s="T295">tʼüʔpi. </ts>
               <ts e="T297" id="Seg_531" n="e" s="T296">Kĭškəlim". </ts>
               <ts e="T298" id="Seg_533" n="e" s="T297">Dĭgəttə </ts>
               <ts e="T299" id="Seg_535" n="e" s="T298">öʔlüʔbi </ts>
               <ts e="T300" id="Seg_537" n="e" s="T299">(bargo). </ts>
               <ts e="T301" id="Seg_539" n="e" s="T300">Saʔməluʔpiʔi. </ts>
               <ts e="T302" id="Seg_541" n="e" s="T301">Dʼabərobiʔi. </ts>
               <ts e="T303" id="Seg_543" n="e" s="T302">Ĭmbidə </ts>
               <ts e="T304" id="Seg_545" n="e" s="T303">ej </ts>
               <ts e="T305" id="Seg_547" n="e" s="T304">tĭmnebiʔi. </ts>
               <ts e="T306" id="Seg_549" n="e" s="T305">I </ts>
               <ts e="T307" id="Seg_551" n="e" s="T306">kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T219" id="Seg_552" s="T215">PKZ_196X_SixFools_flk.001 (001)</ta>
            <ta e="T223" id="Seg_553" s="T219">PKZ_196X_SixFools_flk.002 (002)</ta>
            <ta e="T227" id="Seg_554" s="T223">PKZ_196X_SixFools_flk.003 (003)</ta>
            <ta e="T232" id="Seg_555" s="T227">PKZ_196X_SixFools_flk.004 (004)</ta>
            <ta e="T235" id="Seg_556" s="T232">PKZ_196X_SixFools_flk.005 (005)</ta>
            <ta e="T237" id="Seg_557" s="T235">PKZ_196X_SixFools_flk.006 (006)</ta>
            <ta e="T245" id="Seg_558" s="T237">PKZ_196X_SixFools_flk.007 (007)</ta>
            <ta e="T258" id="Seg_559" s="T245">PKZ_196X_SixFools_flk.008 (008) </ta>
            <ta e="T265" id="Seg_560" s="T258">PKZ_196X_SixFools_flk.009 (010)</ta>
            <ta e="T271" id="Seg_561" s="T265">PKZ_196X_SixFools_flk.010 (011)</ta>
            <ta e="T275" id="Seg_562" s="T271">PKZ_196X_SixFools_flk.011 (012)</ta>
            <ta e="T280" id="Seg_563" s="T275">PKZ_196X_SixFools_flk.012 (013)</ta>
            <ta e="T283" id="Seg_564" s="T280">PKZ_196X_SixFools_flk.013 (014)</ta>
            <ta e="T286" id="Seg_565" s="T283">PKZ_196X_SixFools_flk.014 (015)</ta>
            <ta e="T291" id="Seg_566" s="T286">PKZ_196X_SixFools_flk.015 (016)</ta>
            <ta e="T296" id="Seg_567" s="T291">PKZ_196X_SixFools_flk.016 (017)</ta>
            <ta e="T297" id="Seg_568" s="T296">PKZ_196X_SixFools_flk.017 (018)</ta>
            <ta e="T300" id="Seg_569" s="T297">PKZ_196X_SixFools_flk.018 (019)</ta>
            <ta e="T301" id="Seg_570" s="T300">PKZ_196X_SixFools_flk.019 (020)</ta>
            <ta e="T302" id="Seg_571" s="T301">PKZ_196X_SixFools_flk.020 (021)</ta>
            <ta e="T305" id="Seg_572" s="T302">PKZ_196X_SixFools_flk.021 (022)</ta>
            <ta e="T307" id="Seg_573" s="T305">PKZ_196X_SixFools_flk.022 (023)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T219" id="Seg_574" s="T215">Muktuʔ kuzaʔi sagəšsəbi kambiʔi. </ta>
            <ta e="T223" id="Seg_575" s="T219">Tĭlzittə dʼü, kös nendəsʼtə. </ta>
            <ta e="T227" id="Seg_576" s="T223">Dĭgəttə üjüzaŋdə dibər păʔpiʔi. </ta>
            <ta e="T232" id="Seg_577" s="T227">Măndəʔi:" Šindən ujut ej mobiʔi". </ta>
            <ta e="T235" id="Seg_578" s="T232">Dĭgəttə šobi kuza. </ta>
            <ta e="T237" id="Seg_579" s="T235">Ĭmbi amnolaʔbəʔjə. </ta>
            <ta e="T245" id="Seg_580" s="T237">Dĭ (m-) miʔ ujuʔi ej (tĭmniem), šindin girgit. </ta>
            <ta e="T258" id="Seg_581" s="T245">(Dĭ pa ib-) Dĭ măndə: Baltuʔi mĭbileʔ dak, dĭgəttə măn šiʔnʼileʔ alam jakšə. </ta>
            <ta e="T265" id="Seg_582" s="T258">Dĭgəttə ibi pa da davaj dĭzeŋ münörzittə. </ta>
            <ta e="T271" id="Seg_583" s="T265">Dĭzeŋ bar suʔmiluʔpiʔi i (nu-) nuʔməluʔpiʔi. </ta>
            <ta e="T275" id="Seg_584" s="T271">Dĭgəttə măndəʔi:" Nada pa". </ta>
            <ta e="T280" id="Seg_585" s="T275">Onʼiʔ măndə:" Măn mu băldəlim". </ta>
            <ta e="T283" id="Seg_586" s="T280">Dĭgəttə băldəbi, băldəbi. </ta>
            <ta e="T286" id="Seg_587" s="T283">Onʼiʔ ibi ujutsiʔ. </ta>
            <ta e="T291" id="Seg_588" s="T286">Dĭgəttə bar (te-) teʔtəguʔ ibiʔi. </ta>
            <ta e="T296" id="Seg_589" s="T291">Dĭgəttə dĭ măndə:" Udam tʼüʔpi. </ta>
            <ta e="T297" id="Seg_590" s="T296">Kĭškəlim". </ta>
            <ta e="T300" id="Seg_591" s="T297">Dĭgəttə öʔlüʔbi (bargo). </ta>
            <ta e="T301" id="Seg_592" s="T300">Saʔməluʔpiʔi. </ta>
            <ta e="T302" id="Seg_593" s="T301">Dʼabərobiʔi. </ta>
            <ta e="T305" id="Seg_594" s="T302">Ĭmbidə ej tĭmnebiʔi. </ta>
            <ta e="T307" id="Seg_595" s="T305">I kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T216" id="Seg_596" s="T215">muktuʔ</ta>
            <ta e="T217" id="Seg_597" s="T216">kuza-ʔi</ta>
            <ta e="T218" id="Seg_598" s="T217">sagəš-səbi</ta>
            <ta e="T219" id="Seg_599" s="T218">kam-bi-ʔi</ta>
            <ta e="T220" id="Seg_600" s="T219">tĭl-zittə</ta>
            <ta e="T221" id="Seg_601" s="T220">dʼü</ta>
            <ta e="T222" id="Seg_602" s="T221">kös</ta>
            <ta e="T223" id="Seg_603" s="T222">nendə-sʼtə</ta>
            <ta e="T224" id="Seg_604" s="T223">dĭgəttə</ta>
            <ta e="T225" id="Seg_605" s="T224">üjü-zaŋ-də</ta>
            <ta e="T226" id="Seg_606" s="T225">dibər</ta>
            <ta e="T227" id="Seg_607" s="T226">păʔ-pi-ʔi</ta>
            <ta e="T228" id="Seg_608" s="T227">măn-də-ʔi</ta>
            <ta e="T229" id="Seg_609" s="T228">šində-n</ta>
            <ta e="T230" id="Seg_610" s="T229">uju-t</ta>
            <ta e="T231" id="Seg_611" s="T230">ej</ta>
            <ta e="T232" id="Seg_612" s="T231">mo-bi-ʔi</ta>
            <ta e="T233" id="Seg_613" s="T232">dĭgəttə</ta>
            <ta e="T234" id="Seg_614" s="T233">šo-bi</ta>
            <ta e="T235" id="Seg_615" s="T234">kuza</ta>
            <ta e="T236" id="Seg_616" s="T235">ĭmbi</ta>
            <ta e="T237" id="Seg_617" s="T236">amno-laʔbə-ʔjə</ta>
            <ta e="T238" id="Seg_618" s="T237">dĭ</ta>
            <ta e="T240" id="Seg_619" s="T239">miʔ</ta>
            <ta e="T241" id="Seg_620" s="T240">uju-ʔi</ta>
            <ta e="T242" id="Seg_621" s="T241">ej</ta>
            <ta e="T243" id="Seg_622" s="T242">tĭm-nie-m</ta>
            <ta e="T244" id="Seg_623" s="T243">šindi-n</ta>
            <ta e="T245" id="Seg_624" s="T244">girgit</ta>
            <ta e="T246" id="Seg_625" s="T245">dĭ</ta>
            <ta e="T247" id="Seg_626" s="T246">pa</ta>
            <ta e="T249" id="Seg_627" s="T248">dĭ</ta>
            <ta e="T250" id="Seg_628" s="T249">măn-də</ta>
            <ta e="T251" id="Seg_629" s="T250">baltu-ʔi</ta>
            <ta e="T252" id="Seg_630" s="T251">mĭ-bi-leʔ</ta>
            <ta e="T253" id="Seg_631" s="T252">dak</ta>
            <ta e="T254" id="Seg_632" s="T253">dĭgəttə</ta>
            <ta e="T255" id="Seg_633" s="T254">măn</ta>
            <ta e="T256" id="Seg_634" s="T255">šiʔnʼileʔ</ta>
            <ta e="T257" id="Seg_635" s="T256">a-la-m</ta>
            <ta e="T258" id="Seg_636" s="T257">jakšə</ta>
            <ta e="T259" id="Seg_637" s="T258">dĭgəttə</ta>
            <ta e="T260" id="Seg_638" s="T259">i-bi</ta>
            <ta e="T261" id="Seg_639" s="T260">pa</ta>
            <ta e="T262" id="Seg_640" s="T261">da</ta>
            <ta e="T263" id="Seg_641" s="T262">davaj</ta>
            <ta e="T264" id="Seg_642" s="T263">dĭ-zeŋ</ta>
            <ta e="T265" id="Seg_643" s="T264">münör-zittə</ta>
            <ta e="T266" id="Seg_644" s="T265">dĭ-zeŋ</ta>
            <ta e="T267" id="Seg_645" s="T266">bar</ta>
            <ta e="T268" id="Seg_646" s="T267">suʔmi-luʔ-pi-ʔi</ta>
            <ta e="T269" id="Seg_647" s="T268">i</ta>
            <ta e="T271" id="Seg_648" s="T270">nuʔmə-luʔ-pi-ʔi</ta>
            <ta e="T272" id="Seg_649" s="T271">dĭgəttə</ta>
            <ta e="T273" id="Seg_650" s="T272">măn-də-ʔi</ta>
            <ta e="T274" id="Seg_651" s="T273">nada</ta>
            <ta e="T275" id="Seg_652" s="T274">pa</ta>
            <ta e="T276" id="Seg_653" s="T275">onʼiʔ</ta>
            <ta e="T277" id="Seg_654" s="T276">măn-də</ta>
            <ta e="T278" id="Seg_655" s="T277">măn</ta>
            <ta e="T279" id="Seg_656" s="T278">mu</ta>
            <ta e="T280" id="Seg_657" s="T279">băldə-li-m</ta>
            <ta e="T281" id="Seg_658" s="T280">dĭgəttə</ta>
            <ta e="T282" id="Seg_659" s="T281">băldə-bi</ta>
            <ta e="T283" id="Seg_660" s="T282">băldə-bi</ta>
            <ta e="T284" id="Seg_661" s="T283">onʼiʔ</ta>
            <ta e="T285" id="Seg_662" s="T284">i-bi</ta>
            <ta e="T286" id="Seg_663" s="T285">uju-t-siʔ</ta>
            <ta e="T287" id="Seg_664" s="T286">dĭgəttə</ta>
            <ta e="T288" id="Seg_665" s="T287">bar</ta>
            <ta e="T290" id="Seg_666" s="T289">teʔtə-guʔ</ta>
            <ta e="T291" id="Seg_667" s="T290">i-bi-ʔi</ta>
            <ta e="T292" id="Seg_668" s="T291">dĭgəttə</ta>
            <ta e="T293" id="Seg_669" s="T292">dĭ</ta>
            <ta e="T294" id="Seg_670" s="T293">măn-də</ta>
            <ta e="T295" id="Seg_671" s="T294">uda-m</ta>
            <ta e="T296" id="Seg_672" s="T295">tʼüʔpi</ta>
            <ta e="T297" id="Seg_673" s="T296">kĭškə-li-m</ta>
            <ta e="T298" id="Seg_674" s="T297">dĭgəttə</ta>
            <ta e="T299" id="Seg_675" s="T298">öʔ-lüʔ-bi</ta>
            <ta e="T300" id="Seg_676" s="T299">bar-go</ta>
            <ta e="T301" id="Seg_677" s="T300">saʔmə-luʔ-pi-ʔi</ta>
            <ta e="T302" id="Seg_678" s="T301">dʼabəro-bi-ʔi</ta>
            <ta e="T303" id="Seg_679" s="T302">ĭmbi=də</ta>
            <ta e="T304" id="Seg_680" s="T303">ej</ta>
            <ta e="T305" id="Seg_681" s="T304">tĭmne-bi-ʔi</ta>
            <ta e="T306" id="Seg_682" s="T305">i</ta>
            <ta e="T307" id="Seg_683" s="T306">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T216" id="Seg_684" s="T215">muktuʔ</ta>
            <ta e="T217" id="Seg_685" s="T216">kuza-jəʔ</ta>
            <ta e="T218" id="Seg_686" s="T217">sagəš-zəbi</ta>
            <ta e="T219" id="Seg_687" s="T218">kan-bi-jəʔ</ta>
            <ta e="T220" id="Seg_688" s="T219">tĭl-zittə</ta>
            <ta e="T221" id="Seg_689" s="T220">tʼo</ta>
            <ta e="T222" id="Seg_690" s="T221">kös</ta>
            <ta e="T223" id="Seg_691" s="T222">nendə-zittə</ta>
            <ta e="T224" id="Seg_692" s="T223">dĭgəttə</ta>
            <ta e="T225" id="Seg_693" s="T224">üjü-zAŋ-də</ta>
            <ta e="T226" id="Seg_694" s="T225">dĭbər</ta>
            <ta e="T227" id="Seg_695" s="T226">păda-bi-jəʔ</ta>
            <ta e="T228" id="Seg_696" s="T227">măn-ntə-jəʔ</ta>
            <ta e="T229" id="Seg_697" s="T228">šində-n</ta>
            <ta e="T230" id="Seg_698" s="T229">üjü-t</ta>
            <ta e="T231" id="Seg_699" s="T230">ej</ta>
            <ta e="T232" id="Seg_700" s="T231">mo-bi-jəʔ</ta>
            <ta e="T233" id="Seg_701" s="T232">dĭgəttə</ta>
            <ta e="T234" id="Seg_702" s="T233">šo-bi</ta>
            <ta e="T235" id="Seg_703" s="T234">kuza</ta>
            <ta e="T236" id="Seg_704" s="T235">ĭmbi</ta>
            <ta e="T237" id="Seg_705" s="T236">amno-laʔbə-jəʔ</ta>
            <ta e="T238" id="Seg_706" s="T237">dĭ</ta>
            <ta e="T240" id="Seg_707" s="T239">miʔ</ta>
            <ta e="T241" id="Seg_708" s="T240">üjü-jəʔ</ta>
            <ta e="T242" id="Seg_709" s="T241">ej</ta>
            <ta e="T243" id="Seg_710" s="T242">tĭm-liA-m</ta>
            <ta e="T244" id="Seg_711" s="T243">šində-n</ta>
            <ta e="T245" id="Seg_712" s="T244">girgit</ta>
            <ta e="T246" id="Seg_713" s="T245">dĭ</ta>
            <ta e="T247" id="Seg_714" s="T246">pa</ta>
            <ta e="T249" id="Seg_715" s="T248">dĭ</ta>
            <ta e="T250" id="Seg_716" s="T249">măn-ntə</ta>
            <ta e="T251" id="Seg_717" s="T250">baltu-jəʔ</ta>
            <ta e="T252" id="Seg_718" s="T251">mĭ-bi-lAʔ</ta>
            <ta e="T253" id="Seg_719" s="T252">tak</ta>
            <ta e="T254" id="Seg_720" s="T253">dĭgəttə</ta>
            <ta e="T255" id="Seg_721" s="T254">măn</ta>
            <ta e="T256" id="Seg_722" s="T255">šiʔnʼileʔ</ta>
            <ta e="T257" id="Seg_723" s="T256">a-lV-m</ta>
            <ta e="T258" id="Seg_724" s="T257">jakšə</ta>
            <ta e="T259" id="Seg_725" s="T258">dĭgəttə</ta>
            <ta e="T260" id="Seg_726" s="T259">i-bi</ta>
            <ta e="T261" id="Seg_727" s="T260">pa</ta>
            <ta e="T262" id="Seg_728" s="T261">da</ta>
            <ta e="T263" id="Seg_729" s="T262">davaj</ta>
            <ta e="T264" id="Seg_730" s="T263">dĭ-zAŋ</ta>
            <ta e="T265" id="Seg_731" s="T264">münör-zittə</ta>
            <ta e="T266" id="Seg_732" s="T265">dĭ-zAŋ</ta>
            <ta e="T267" id="Seg_733" s="T266">bar</ta>
            <ta e="T268" id="Seg_734" s="T267">süʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T269" id="Seg_735" s="T268">i</ta>
            <ta e="T271" id="Seg_736" s="T270">nuʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T272" id="Seg_737" s="T271">dĭgəttə</ta>
            <ta e="T273" id="Seg_738" s="T272">măn-ntə-jəʔ</ta>
            <ta e="T274" id="Seg_739" s="T273">nadə</ta>
            <ta e="T275" id="Seg_740" s="T274">pa</ta>
            <ta e="T276" id="Seg_741" s="T275">onʼiʔ</ta>
            <ta e="T277" id="Seg_742" s="T276">măn-ntə</ta>
            <ta e="T278" id="Seg_743" s="T277">măn</ta>
            <ta e="T279" id="Seg_744" s="T278">mu</ta>
            <ta e="T280" id="Seg_745" s="T279">băldə-lV-m</ta>
            <ta e="T281" id="Seg_746" s="T280">dĭgəttə</ta>
            <ta e="T282" id="Seg_747" s="T281">băldə-bi</ta>
            <ta e="T283" id="Seg_748" s="T282">băldə-bi</ta>
            <ta e="T284" id="Seg_749" s="T283">onʼiʔ</ta>
            <ta e="T285" id="Seg_750" s="T284">i-bi</ta>
            <ta e="T286" id="Seg_751" s="T285">üjü-t-ziʔ</ta>
            <ta e="T287" id="Seg_752" s="T286">dĭgəttə</ta>
            <ta e="T288" id="Seg_753" s="T287">bar</ta>
            <ta e="T290" id="Seg_754" s="T289">teʔdə-göʔ</ta>
            <ta e="T291" id="Seg_755" s="T290">i-bi-jəʔ</ta>
            <ta e="T292" id="Seg_756" s="T291">dĭgəttə</ta>
            <ta e="T293" id="Seg_757" s="T292">dĭ</ta>
            <ta e="T294" id="Seg_758" s="T293">măn-ntə</ta>
            <ta e="T295" id="Seg_759" s="T294">uda-m</ta>
            <ta e="T296" id="Seg_760" s="T295">dʼüʔpi</ta>
            <ta e="T297" id="Seg_761" s="T296">kĭškə-lV-m</ta>
            <ta e="T298" id="Seg_762" s="T297">dĭgəttə</ta>
            <ta e="T299" id="Seg_763" s="T298">öʔ-luʔbdə-bi</ta>
            <ta e="T300" id="Seg_764" s="T299">bar-göʔ</ta>
            <ta e="T301" id="Seg_765" s="T300">saʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T302" id="Seg_766" s="T301">tʼabəro-bi-jəʔ</ta>
            <ta e="T303" id="Seg_767" s="T302">ĭmbi=də</ta>
            <ta e="T304" id="Seg_768" s="T303">ej</ta>
            <ta e="T305" id="Seg_769" s="T304">tĭmne-bi-jəʔ</ta>
            <ta e="T306" id="Seg_770" s="T305">i</ta>
            <ta e="T307" id="Seg_771" s="T306">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T216" id="Seg_772" s="T215">six.[NOM.SG]</ta>
            <ta e="T217" id="Seg_773" s="T216">man-PL</ta>
            <ta e="T218" id="Seg_774" s="T217">mind-ADJZ.[NOM.SG]</ta>
            <ta e="T219" id="Seg_775" s="T218">go-PST-3PL</ta>
            <ta e="T220" id="Seg_776" s="T219">dig-INF.LAT</ta>
            <ta e="T221" id="Seg_777" s="T220">earth.[NOM.SG]</ta>
            <ta e="T222" id="Seg_778" s="T221">coal.[NOM.SG]</ta>
            <ta e="T223" id="Seg_779" s="T222">light-INF.LAT</ta>
            <ta e="T224" id="Seg_780" s="T223">then</ta>
            <ta e="T225" id="Seg_781" s="T224">foot-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T226" id="Seg_782" s="T225">there</ta>
            <ta e="T227" id="Seg_783" s="T226">creep.into-PST-3PL</ta>
            <ta e="T228" id="Seg_784" s="T227">say-IPFVZ-3PL</ta>
            <ta e="T229" id="Seg_785" s="T228">who-GEN</ta>
            <ta e="T230" id="Seg_786" s="T229">foot-NOM/GEN.3SG</ta>
            <ta e="T231" id="Seg_787" s="T230">NEG</ta>
            <ta e="T232" id="Seg_788" s="T231">become-PST-3PL</ta>
            <ta e="T233" id="Seg_789" s="T232">then</ta>
            <ta e="T234" id="Seg_790" s="T233">come-PST.[3SG]</ta>
            <ta e="T235" id="Seg_791" s="T234">man.[NOM.SG]</ta>
            <ta e="T236" id="Seg_792" s="T235">what.[NOM.SG]</ta>
            <ta e="T237" id="Seg_793" s="T236">sit-DUR-3PL</ta>
            <ta e="T238" id="Seg_794" s="T237">this.[NOM.SG]</ta>
            <ta e="T240" id="Seg_795" s="T239">we.NOM</ta>
            <ta e="T241" id="Seg_796" s="T240">foot-PL</ta>
            <ta e="T242" id="Seg_797" s="T241">NEG</ta>
            <ta e="T243" id="Seg_798" s="T242">know-PRS-1SG</ta>
            <ta e="T244" id="Seg_799" s="T243">who-GEN</ta>
            <ta e="T245" id="Seg_800" s="T244">what.kind</ta>
            <ta e="T246" id="Seg_801" s="T245">this</ta>
            <ta e="T247" id="Seg_802" s="T246">tree.[NOM.SG]</ta>
            <ta e="T249" id="Seg_803" s="T248">this</ta>
            <ta e="T250" id="Seg_804" s="T249">say-IPFVZ.[3SG]</ta>
            <ta e="T251" id="Seg_805" s="T250">axe-NOM/GEN/ACC.3PL</ta>
            <ta e="T252" id="Seg_806" s="T251">give-PST-2PL</ta>
            <ta e="T253" id="Seg_807" s="T252">so</ta>
            <ta e="T254" id="Seg_808" s="T253">then</ta>
            <ta e="T255" id="Seg_809" s="T254">I.NOM</ta>
            <ta e="T256" id="Seg_810" s="T255">you.PL.ACC</ta>
            <ta e="T257" id="Seg_811" s="T256">make-FUT-1SG</ta>
            <ta e="T258" id="Seg_812" s="T257">good.[NOM.SG]</ta>
            <ta e="T259" id="Seg_813" s="T258">then</ta>
            <ta e="T260" id="Seg_814" s="T259">take-PST.[3SG]</ta>
            <ta e="T261" id="Seg_815" s="T260">tree.[NOM.SG]</ta>
            <ta e="T262" id="Seg_816" s="T261">and</ta>
            <ta e="T263" id="Seg_817" s="T262">INCH</ta>
            <ta e="T264" id="Seg_818" s="T263">this-PL</ta>
            <ta e="T265" id="Seg_819" s="T264">beat-INF.LAT</ta>
            <ta e="T266" id="Seg_820" s="T265">this-PL</ta>
            <ta e="T267" id="Seg_821" s="T266">PTCL</ta>
            <ta e="T268" id="Seg_822" s="T267">jump-MOM-PST-3PL</ta>
            <ta e="T269" id="Seg_823" s="T268">and</ta>
            <ta e="T271" id="Seg_824" s="T270">run-MOM-PST-3PL</ta>
            <ta e="T272" id="Seg_825" s="T271">then</ta>
            <ta e="T273" id="Seg_826" s="T272">say-IPFVZ-3PL</ta>
            <ta e="T274" id="Seg_827" s="T273">one.should</ta>
            <ta e="T275" id="Seg_828" s="T274">tree.[NOM.SG]</ta>
            <ta e="T276" id="Seg_829" s="T275">one.[NOM.SG]</ta>
            <ta e="T277" id="Seg_830" s="T276">say-IPFVZ.[3SG]</ta>
            <ta e="T278" id="Seg_831" s="T277">I.NOM</ta>
            <ta e="T279" id="Seg_832" s="T278">branch.[NOM.SG]</ta>
            <ta e="T280" id="Seg_833" s="T279">break-FUT-1SG</ta>
            <ta e="T281" id="Seg_834" s="T280">then</ta>
            <ta e="T282" id="Seg_835" s="T281">break-PST.[3SG]</ta>
            <ta e="T283" id="Seg_836" s="T282">break-PST.[3SG]</ta>
            <ta e="T284" id="Seg_837" s="T283">one.[NOM.SG]</ta>
            <ta e="T285" id="Seg_838" s="T284">take-PST.[3SG]</ta>
            <ta e="T286" id="Seg_839" s="T285">foot-3SG-INS</ta>
            <ta e="T287" id="Seg_840" s="T286">then</ta>
            <ta e="T288" id="Seg_841" s="T287">PTCL</ta>
            <ta e="T290" id="Seg_842" s="T289">four-COLL</ta>
            <ta e="T291" id="Seg_843" s="T290">take-PST-3PL</ta>
            <ta e="T292" id="Seg_844" s="T291">then</ta>
            <ta e="T293" id="Seg_845" s="T292">this.[NOM.SG]</ta>
            <ta e="T294" id="Seg_846" s="T293">say-IPFVZ.[3SG]</ta>
            <ta e="T295" id="Seg_847" s="T294">hand-NOM/GEN/ACC.1SG</ta>
            <ta e="T296" id="Seg_848" s="T295">wet</ta>
            <ta e="T297" id="Seg_849" s="T296">rub-FUT-1SG</ta>
            <ta e="T298" id="Seg_850" s="T297">then</ta>
            <ta e="T299" id="Seg_851" s="T298">send-MOM-PST.[3SG]</ta>
            <ta e="T300" id="Seg_852" s="T299">PTCL-COLL</ta>
            <ta e="T301" id="Seg_853" s="T300">fall-MOM-PST-3PL</ta>
            <ta e="T302" id="Seg_854" s="T301">fight-PST-3PL</ta>
            <ta e="T303" id="Seg_855" s="T302">what.[NOM.SG]=INDEF</ta>
            <ta e="T304" id="Seg_856" s="T303">NEG</ta>
            <ta e="T305" id="Seg_857" s="T304">know-PST-3PL</ta>
            <ta e="T306" id="Seg_858" s="T305">and</ta>
            <ta e="T307" id="Seg_859" s="T306">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T216" id="Seg_860" s="T215">шесть.[NOM.SG]</ta>
            <ta e="T217" id="Seg_861" s="T216">мужчина-PL</ta>
            <ta e="T218" id="Seg_862" s="T217">ум-ADJZ.[NOM.SG]</ta>
            <ta e="T219" id="Seg_863" s="T218">пойти-PST-3PL</ta>
            <ta e="T220" id="Seg_864" s="T219">копать-INF.LAT</ta>
            <ta e="T221" id="Seg_865" s="T220">земля.[NOM.SG]</ta>
            <ta e="T222" id="Seg_866" s="T221">уголь.[NOM.SG]</ta>
            <ta e="T223" id="Seg_867" s="T222">светить-INF.LAT</ta>
            <ta e="T224" id="Seg_868" s="T223">тогда</ta>
            <ta e="T225" id="Seg_869" s="T224">нога-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T226" id="Seg_870" s="T225">там</ta>
            <ta e="T227" id="Seg_871" s="T226">ползти-PST-3PL</ta>
            <ta e="T228" id="Seg_872" s="T227">сказать-IPFVZ-3PL</ta>
            <ta e="T229" id="Seg_873" s="T228">кто-GEN</ta>
            <ta e="T230" id="Seg_874" s="T229">нога-NOM/GEN.3SG</ta>
            <ta e="T231" id="Seg_875" s="T230">NEG</ta>
            <ta e="T232" id="Seg_876" s="T231">стать-PST-3PL</ta>
            <ta e="T233" id="Seg_877" s="T232">тогда</ta>
            <ta e="T234" id="Seg_878" s="T233">прийти-PST.[3SG]</ta>
            <ta e="T235" id="Seg_879" s="T234">мужчина.[NOM.SG]</ta>
            <ta e="T236" id="Seg_880" s="T235">что.[NOM.SG]</ta>
            <ta e="T237" id="Seg_881" s="T236">сидеть-DUR-3PL</ta>
            <ta e="T238" id="Seg_882" s="T237">этот.[NOM.SG]</ta>
            <ta e="T240" id="Seg_883" s="T239">мы.NOM</ta>
            <ta e="T241" id="Seg_884" s="T240">нога-PL</ta>
            <ta e="T242" id="Seg_885" s="T241">NEG</ta>
            <ta e="T243" id="Seg_886" s="T242">знать-PRS-1SG</ta>
            <ta e="T244" id="Seg_887" s="T243">кто-GEN</ta>
            <ta e="T245" id="Seg_888" s="T244">какой</ta>
            <ta e="T246" id="Seg_889" s="T245">этот</ta>
            <ta e="T247" id="Seg_890" s="T246">дерево.[NOM.SG]</ta>
            <ta e="T249" id="Seg_891" s="T248">этот</ta>
            <ta e="T250" id="Seg_892" s="T249">сказать-IPFVZ.[3SG]</ta>
            <ta e="T251" id="Seg_893" s="T250">топор-NOM/GEN/ACC.3PL</ta>
            <ta e="T252" id="Seg_894" s="T251">дать-PST-2PL</ta>
            <ta e="T253" id="Seg_895" s="T252">так</ta>
            <ta e="T254" id="Seg_896" s="T253">тогда</ta>
            <ta e="T255" id="Seg_897" s="T254">я.NOM</ta>
            <ta e="T256" id="Seg_898" s="T255">вы.ACC</ta>
            <ta e="T257" id="Seg_899" s="T256">делать-FUT-1SG</ta>
            <ta e="T258" id="Seg_900" s="T257">хороший.[NOM.SG]</ta>
            <ta e="T259" id="Seg_901" s="T258">тогда</ta>
            <ta e="T260" id="Seg_902" s="T259">взять-PST.[3SG]</ta>
            <ta e="T261" id="Seg_903" s="T260">дерево.[NOM.SG]</ta>
            <ta e="T262" id="Seg_904" s="T261">и</ta>
            <ta e="T263" id="Seg_905" s="T262">INCH</ta>
            <ta e="T264" id="Seg_906" s="T263">этот-PL</ta>
            <ta e="T265" id="Seg_907" s="T264">бить-INF.LAT</ta>
            <ta e="T266" id="Seg_908" s="T265">этот-PL</ta>
            <ta e="T267" id="Seg_909" s="T266">PTCL</ta>
            <ta e="T268" id="Seg_910" s="T267">прыгнуть-MOM-PST-3PL</ta>
            <ta e="T269" id="Seg_911" s="T268">и</ta>
            <ta e="T271" id="Seg_912" s="T270">бежать-MOM-PST-3PL</ta>
            <ta e="T272" id="Seg_913" s="T271">тогда</ta>
            <ta e="T273" id="Seg_914" s="T272">сказать-IPFVZ-3PL</ta>
            <ta e="T274" id="Seg_915" s="T273">надо</ta>
            <ta e="T275" id="Seg_916" s="T274">дерево.[NOM.SG]</ta>
            <ta e="T276" id="Seg_917" s="T275">один.[NOM.SG]</ta>
            <ta e="T277" id="Seg_918" s="T276">сказать-IPFVZ.[3SG]</ta>
            <ta e="T278" id="Seg_919" s="T277">я.NOM</ta>
            <ta e="T279" id="Seg_920" s="T278">ветка.[NOM.SG]</ta>
            <ta e="T280" id="Seg_921" s="T279">сломать-FUT-1SG</ta>
            <ta e="T281" id="Seg_922" s="T280">тогда</ta>
            <ta e="T282" id="Seg_923" s="T281">сломать-PST.[3SG]</ta>
            <ta e="T283" id="Seg_924" s="T282">сломать-PST.[3SG]</ta>
            <ta e="T284" id="Seg_925" s="T283">один.[NOM.SG]</ta>
            <ta e="T285" id="Seg_926" s="T284">взять-PST.[3SG]</ta>
            <ta e="T286" id="Seg_927" s="T285">нога-3SG-INS</ta>
            <ta e="T287" id="Seg_928" s="T286">тогда</ta>
            <ta e="T288" id="Seg_929" s="T287">PTCL</ta>
            <ta e="T290" id="Seg_930" s="T289">четыре-COLL</ta>
            <ta e="T291" id="Seg_931" s="T290">взять-PST-3PL</ta>
            <ta e="T292" id="Seg_932" s="T291">тогда</ta>
            <ta e="T293" id="Seg_933" s="T292">этот.[NOM.SG]</ta>
            <ta e="T294" id="Seg_934" s="T293">сказать-IPFVZ.[3SG]</ta>
            <ta e="T295" id="Seg_935" s="T294">рука-NOM/GEN/ACC.1SG</ta>
            <ta e="T296" id="Seg_936" s="T295">влажный</ta>
            <ta e="T297" id="Seg_937" s="T296">тереть-FUT-1SG</ta>
            <ta e="T298" id="Seg_938" s="T297">тогда</ta>
            <ta e="T299" id="Seg_939" s="T298">послать-MOM-PST.[3SG]</ta>
            <ta e="T300" id="Seg_940" s="T299">PTCL-COLL</ta>
            <ta e="T301" id="Seg_941" s="T300">упасть-MOM-PST-3PL</ta>
            <ta e="T302" id="Seg_942" s="T301">бороться-PST-3PL</ta>
            <ta e="T303" id="Seg_943" s="T302">что.[NOM.SG]=INDEF</ta>
            <ta e="T304" id="Seg_944" s="T303">NEG</ta>
            <ta e="T305" id="Seg_945" s="T304">знать-PST-3PL</ta>
            <ta e="T306" id="Seg_946" s="T305">и</ta>
            <ta e="T307" id="Seg_947" s="T306">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T216" id="Seg_948" s="T215">num-n:case</ta>
            <ta e="T217" id="Seg_949" s="T216">n-n:num</ta>
            <ta e="T218" id="Seg_950" s="T217">n-n&gt;adj-n:case</ta>
            <ta e="T219" id="Seg_951" s="T218">v-v:tense-v:pn</ta>
            <ta e="T220" id="Seg_952" s="T219">v-v:n.fin</ta>
            <ta e="T221" id="Seg_953" s="T220">n-n:case</ta>
            <ta e="T222" id="Seg_954" s="T221">n-n:case</ta>
            <ta e="T223" id="Seg_955" s="T222">v-v:n.fin</ta>
            <ta e="T224" id="Seg_956" s="T223">adv</ta>
            <ta e="T225" id="Seg_957" s="T224">n-n:num-n:case.poss</ta>
            <ta e="T226" id="Seg_958" s="T225">adv</ta>
            <ta e="T227" id="Seg_959" s="T226">v-v:tense-v:pn</ta>
            <ta e="T228" id="Seg_960" s="T227">v-v&gt;v-v:pn</ta>
            <ta e="T229" id="Seg_961" s="T228">que-n:case</ta>
            <ta e="T230" id="Seg_962" s="T229">n-n:case.poss</ta>
            <ta e="T231" id="Seg_963" s="T230">ptcl</ta>
            <ta e="T232" id="Seg_964" s="T231">v-v:tense-v:pn</ta>
            <ta e="T233" id="Seg_965" s="T232">adv</ta>
            <ta e="T234" id="Seg_966" s="T233">v-v:tense-v:pn</ta>
            <ta e="T235" id="Seg_967" s="T234">n-n:case</ta>
            <ta e="T236" id="Seg_968" s="T235">que-n:case</ta>
            <ta e="T237" id="Seg_969" s="T236">v-v&gt;v-v:pn</ta>
            <ta e="T238" id="Seg_970" s="T237">dempro-n:case</ta>
            <ta e="T240" id="Seg_971" s="T239">pers</ta>
            <ta e="T241" id="Seg_972" s="T240">n-n:num</ta>
            <ta e="T242" id="Seg_973" s="T241">ptcl</ta>
            <ta e="T243" id="Seg_974" s="T242">v-v:tense-v:pn</ta>
            <ta e="T244" id="Seg_975" s="T243">que-n:case</ta>
            <ta e="T245" id="Seg_976" s="T244">que</ta>
            <ta e="T246" id="Seg_977" s="T245">dempro</ta>
            <ta e="T247" id="Seg_978" s="T246">n-n:case</ta>
            <ta e="T249" id="Seg_979" s="T248">dempro</ta>
            <ta e="T250" id="Seg_980" s="T249">v-v&gt;v-v:pn</ta>
            <ta e="T251" id="Seg_981" s="T250">n-n:case.poss</ta>
            <ta e="T252" id="Seg_982" s="T251">v-v:tense-v:pn</ta>
            <ta e="T253" id="Seg_983" s="T252">ptcl</ta>
            <ta e="T254" id="Seg_984" s="T253">adv</ta>
            <ta e="T255" id="Seg_985" s="T254">pers</ta>
            <ta e="T256" id="Seg_986" s="T255">pers</ta>
            <ta e="T257" id="Seg_987" s="T256">v-v:tense-v:pn</ta>
            <ta e="T258" id="Seg_988" s="T257">adj-n:case</ta>
            <ta e="T259" id="Seg_989" s="T258">adv</ta>
            <ta e="T260" id="Seg_990" s="T259">v-v:tense-v:pn</ta>
            <ta e="T261" id="Seg_991" s="T260">n-n:case</ta>
            <ta e="T262" id="Seg_992" s="T261">conj</ta>
            <ta e="T263" id="Seg_993" s="T262">ptcl</ta>
            <ta e="T264" id="Seg_994" s="T263">dempro-n:num</ta>
            <ta e="T265" id="Seg_995" s="T264">v-v:n.fin</ta>
            <ta e="T266" id="Seg_996" s="T265">dempro-n:num</ta>
            <ta e="T267" id="Seg_997" s="T266">ptcl</ta>
            <ta e="T268" id="Seg_998" s="T267">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T269" id="Seg_999" s="T268">conj</ta>
            <ta e="T271" id="Seg_1000" s="T270">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T272" id="Seg_1001" s="T271">adv</ta>
            <ta e="T273" id="Seg_1002" s="T272">v-v&gt;v-v:pn</ta>
            <ta e="T274" id="Seg_1003" s="T273">ptcl</ta>
            <ta e="T275" id="Seg_1004" s="T274">n-n:case</ta>
            <ta e="T276" id="Seg_1005" s="T275">num-n:case</ta>
            <ta e="T277" id="Seg_1006" s="T276">v-v&gt;v-v:pn</ta>
            <ta e="T278" id="Seg_1007" s="T277">pers</ta>
            <ta e="T279" id="Seg_1008" s="T278">n-n:case</ta>
            <ta e="T280" id="Seg_1009" s="T279">v-v:tense-v:pn</ta>
            <ta e="T281" id="Seg_1010" s="T280">adv</ta>
            <ta e="T282" id="Seg_1011" s="T281">v-v:tense-v:pn</ta>
            <ta e="T283" id="Seg_1012" s="T282">v-v:tense-v:pn</ta>
            <ta e="T284" id="Seg_1013" s="T283">num-n:case</ta>
            <ta e="T285" id="Seg_1014" s="T284">v-v:tense-v:pn</ta>
            <ta e="T286" id="Seg_1015" s="T285">n-n:case.poss-n:case</ta>
            <ta e="T287" id="Seg_1016" s="T286">adv</ta>
            <ta e="T288" id="Seg_1017" s="T287">ptcl</ta>
            <ta e="T290" id="Seg_1018" s="T289">num-num&gt;num</ta>
            <ta e="T291" id="Seg_1019" s="T290">v-v:tense-v:pn</ta>
            <ta e="T292" id="Seg_1020" s="T291">adv</ta>
            <ta e="T293" id="Seg_1021" s="T292">dempro-n:case</ta>
            <ta e="T294" id="Seg_1022" s="T293">v-v&gt;v-v:pn</ta>
            <ta e="T295" id="Seg_1023" s="T294">n-n:case.poss</ta>
            <ta e="T296" id="Seg_1024" s="T295">adj</ta>
            <ta e="T297" id="Seg_1025" s="T296">v-v:tense-v:pn</ta>
            <ta e="T298" id="Seg_1026" s="T297">adv</ta>
            <ta e="T299" id="Seg_1027" s="T298">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T300" id="Seg_1028" s="T299">ptcl-num&gt;num</ta>
            <ta e="T301" id="Seg_1029" s="T300">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T302" id="Seg_1030" s="T301">v-v:tense-v:pn</ta>
            <ta e="T303" id="Seg_1031" s="T302">que-n:case=ptcl</ta>
            <ta e="T304" id="Seg_1032" s="T303">ptcl</ta>
            <ta e="T305" id="Seg_1033" s="T304">v-v:tense-v:pn</ta>
            <ta e="T306" id="Seg_1034" s="T305">conj</ta>
            <ta e="T307" id="Seg_1035" s="T306">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T216" id="Seg_1036" s="T215">num</ta>
            <ta e="T217" id="Seg_1037" s="T216">n</ta>
            <ta e="T218" id="Seg_1038" s="T217">adj</ta>
            <ta e="T219" id="Seg_1039" s="T218">v</ta>
            <ta e="T220" id="Seg_1040" s="T219">v</ta>
            <ta e="T221" id="Seg_1041" s="T220">n</ta>
            <ta e="T222" id="Seg_1042" s="T221">n</ta>
            <ta e="T223" id="Seg_1043" s="T222">v</ta>
            <ta e="T224" id="Seg_1044" s="T223">adv</ta>
            <ta e="T225" id="Seg_1045" s="T224">n</ta>
            <ta e="T226" id="Seg_1046" s="T225">adv</ta>
            <ta e="T228" id="Seg_1047" s="T227">v</ta>
            <ta e="T229" id="Seg_1048" s="T228">que</ta>
            <ta e="T230" id="Seg_1049" s="T229">n</ta>
            <ta e="T231" id="Seg_1050" s="T230">ptcl</ta>
            <ta e="T232" id="Seg_1051" s="T231">v</ta>
            <ta e="T233" id="Seg_1052" s="T232">adv</ta>
            <ta e="T234" id="Seg_1053" s="T233">v</ta>
            <ta e="T235" id="Seg_1054" s="T234">n</ta>
            <ta e="T236" id="Seg_1055" s="T235">que</ta>
            <ta e="T237" id="Seg_1056" s="T236">v</ta>
            <ta e="T238" id="Seg_1057" s="T237">dempro</ta>
            <ta e="T240" id="Seg_1058" s="T239">pers</ta>
            <ta e="T241" id="Seg_1059" s="T240">n</ta>
            <ta e="T242" id="Seg_1060" s="T241">ptcl</ta>
            <ta e="T243" id="Seg_1061" s="T242">v</ta>
            <ta e="T244" id="Seg_1062" s="T243">que</ta>
            <ta e="T245" id="Seg_1063" s="T244">que</ta>
            <ta e="T246" id="Seg_1064" s="T245">dempro</ta>
            <ta e="T247" id="Seg_1065" s="T246">n</ta>
            <ta e="T249" id="Seg_1066" s="T248">dempro</ta>
            <ta e="T250" id="Seg_1067" s="T249">v</ta>
            <ta e="T252" id="Seg_1068" s="T251">v</ta>
            <ta e="T253" id="Seg_1069" s="T252">ptcl</ta>
            <ta e="T254" id="Seg_1070" s="T253">adv</ta>
            <ta e="T255" id="Seg_1071" s="T254">pers</ta>
            <ta e="T256" id="Seg_1072" s="T255">pers</ta>
            <ta e="T257" id="Seg_1073" s="T256">v</ta>
            <ta e="T258" id="Seg_1074" s="T257">adj</ta>
            <ta e="T259" id="Seg_1075" s="T258">adv</ta>
            <ta e="T260" id="Seg_1076" s="T259">v</ta>
            <ta e="T261" id="Seg_1077" s="T260">n</ta>
            <ta e="T262" id="Seg_1078" s="T261">conj</ta>
            <ta e="T263" id="Seg_1079" s="T262">ptcl</ta>
            <ta e="T264" id="Seg_1080" s="T263">dempro</ta>
            <ta e="T265" id="Seg_1081" s="T264">v</ta>
            <ta e="T266" id="Seg_1082" s="T265">dempro</ta>
            <ta e="T267" id="Seg_1083" s="T266">ptcl</ta>
            <ta e="T268" id="Seg_1084" s="T267">v</ta>
            <ta e="T269" id="Seg_1085" s="T268">conj</ta>
            <ta e="T271" id="Seg_1086" s="T270">v</ta>
            <ta e="T272" id="Seg_1087" s="T271">adv</ta>
            <ta e="T273" id="Seg_1088" s="T272">v</ta>
            <ta e="T274" id="Seg_1089" s="T273">ptcl</ta>
            <ta e="T275" id="Seg_1090" s="T274">n</ta>
            <ta e="T276" id="Seg_1091" s="T275">num</ta>
            <ta e="T277" id="Seg_1092" s="T276">v</ta>
            <ta e="T278" id="Seg_1093" s="T277">pers</ta>
            <ta e="T279" id="Seg_1094" s="T278">n</ta>
            <ta e="T280" id="Seg_1095" s="T279">v</ta>
            <ta e="T281" id="Seg_1096" s="T280">adv</ta>
            <ta e="T282" id="Seg_1097" s="T281">v</ta>
            <ta e="T283" id="Seg_1098" s="T282">v</ta>
            <ta e="T284" id="Seg_1099" s="T283">num</ta>
            <ta e="T285" id="Seg_1100" s="T284">v</ta>
            <ta e="T286" id="Seg_1101" s="T285">n</ta>
            <ta e="T287" id="Seg_1102" s="T286">adv</ta>
            <ta e="T288" id="Seg_1103" s="T287">ptcl</ta>
            <ta e="T290" id="Seg_1104" s="T289">num</ta>
            <ta e="T291" id="Seg_1105" s="T290">v</ta>
            <ta e="T292" id="Seg_1106" s="T291">adv</ta>
            <ta e="T293" id="Seg_1107" s="T292">dempro</ta>
            <ta e="T294" id="Seg_1108" s="T293">v</ta>
            <ta e="T295" id="Seg_1109" s="T294">n</ta>
            <ta e="T296" id="Seg_1110" s="T295">adj</ta>
            <ta e="T298" id="Seg_1111" s="T297">adv</ta>
            <ta e="T299" id="Seg_1112" s="T298">v</ta>
            <ta e="T300" id="Seg_1113" s="T299">num</ta>
            <ta e="T301" id="Seg_1114" s="T300">v</ta>
            <ta e="T302" id="Seg_1115" s="T301">v</ta>
            <ta e="T303" id="Seg_1116" s="T302">que</ta>
            <ta e="T304" id="Seg_1117" s="T303">ptcl</ta>
            <ta e="T305" id="Seg_1118" s="T304">v</ta>
            <ta e="T306" id="Seg_1119" s="T305">conj</ta>
            <ta e="T307" id="Seg_1120" s="T306">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T217" id="Seg_1121" s="T216">np.h:A</ta>
            <ta e="T221" id="Seg_1122" s="T220">np:P</ta>
            <ta e="T222" id="Seg_1123" s="T221">np:P</ta>
            <ta e="T224" id="Seg_1124" s="T223">adv:Time</ta>
            <ta e="T225" id="Seg_1125" s="T224">np:Th</ta>
            <ta e="T226" id="Seg_1126" s="T225">adv:L</ta>
            <ta e="T228" id="Seg_1127" s="T227">0.3.h:A</ta>
            <ta e="T229" id="Seg_1128" s="T228">pro.h:Poss</ta>
            <ta e="T230" id="Seg_1129" s="T229">np:Th</ta>
            <ta e="T233" id="Seg_1130" s="T232">adv:Time</ta>
            <ta e="T235" id="Seg_1131" s="T234">np.h:A</ta>
            <ta e="T237" id="Seg_1132" s="T236">0.3.h:E</ta>
            <ta e="T240" id="Seg_1133" s="T239">pro.h:E</ta>
            <ta e="T241" id="Seg_1134" s="T240">np:Th</ta>
            <ta e="T244" id="Seg_1135" s="T243">pro.h:Poss</ta>
            <ta e="T249" id="Seg_1136" s="T248">pro.h:A</ta>
            <ta e="T251" id="Seg_1137" s="T250">np:Th</ta>
            <ta e="T252" id="Seg_1138" s="T251">0.2.h:A</ta>
            <ta e="T254" id="Seg_1139" s="T253">adv:Time</ta>
            <ta e="T255" id="Seg_1140" s="T254">pro.h:A</ta>
            <ta e="T256" id="Seg_1141" s="T255">pro.h:P</ta>
            <ta e="T259" id="Seg_1142" s="T258">adv:Time</ta>
            <ta e="T260" id="Seg_1143" s="T259">0.3.h:A</ta>
            <ta e="T261" id="Seg_1144" s="T260">np:Th</ta>
            <ta e="T264" id="Seg_1145" s="T263">pro.h:E</ta>
            <ta e="T266" id="Seg_1146" s="T265">pro.h:A</ta>
            <ta e="T271" id="Seg_1147" s="T270">0.3.h:A</ta>
            <ta e="T272" id="Seg_1148" s="T271">adv:Time</ta>
            <ta e="T273" id="Seg_1149" s="T272">0.3.h:A</ta>
            <ta e="T275" id="Seg_1150" s="T274">np:Th</ta>
            <ta e="T276" id="Seg_1151" s="T275">np.h:A</ta>
            <ta e="T278" id="Seg_1152" s="T277">pro.h:A</ta>
            <ta e="T279" id="Seg_1153" s="T278">np:P</ta>
            <ta e="T281" id="Seg_1154" s="T280">adv:Time</ta>
            <ta e="T282" id="Seg_1155" s="T281">0.3.h:A</ta>
            <ta e="T283" id="Seg_1156" s="T282">0.3.h:A</ta>
            <ta e="T284" id="Seg_1157" s="T283">np.h:A</ta>
            <ta e="T286" id="Seg_1158" s="T285">np:Ins</ta>
            <ta e="T287" id="Seg_1159" s="T286">adv:Time</ta>
            <ta e="T290" id="Seg_1160" s="T289">np.h:A</ta>
            <ta e="T292" id="Seg_1161" s="T291">adv:Time</ta>
            <ta e="T293" id="Seg_1162" s="T292">pro.h:A</ta>
            <ta e="T295" id="Seg_1163" s="T294">np:Th 0.1.h:Poss</ta>
            <ta e="T297" id="Seg_1164" s="T296">0.1.h:A</ta>
            <ta e="T298" id="Seg_1165" s="T297">adv:Time</ta>
            <ta e="T299" id="Seg_1166" s="T298">0.3.h:A</ta>
            <ta e="T301" id="Seg_1167" s="T300">0.3.h:E</ta>
            <ta e="T302" id="Seg_1168" s="T301">0.3.h:A</ta>
            <ta e="T303" id="Seg_1169" s="T302">pro:Th</ta>
            <ta e="T305" id="Seg_1170" s="T304">0.3.h:E</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T217" id="Seg_1171" s="T216">np.h:S</ta>
            <ta e="T219" id="Seg_1172" s="T218">v:pred</ta>
            <ta e="T220" id="Seg_1173" s="T219">v:pred</ta>
            <ta e="T222" id="Seg_1174" s="T221">np:O</ta>
            <ta e="T223" id="Seg_1175" s="T222">s:purp</ta>
            <ta e="T225" id="Seg_1176" s="T224">np:S</ta>
            <ta e="T227" id="Seg_1177" s="T226">v:pred</ta>
            <ta e="T228" id="Seg_1178" s="T227">v:pred 0.3.h:S</ta>
            <ta e="T230" id="Seg_1179" s="T229">np:S</ta>
            <ta e="T231" id="Seg_1180" s="T230">ptcl.neg</ta>
            <ta e="T232" id="Seg_1181" s="T231">v:pred</ta>
            <ta e="T234" id="Seg_1182" s="T233">v:pred</ta>
            <ta e="T235" id="Seg_1183" s="T234">np.h:S</ta>
            <ta e="T237" id="Seg_1184" s="T236">v:pred 0.3.h:S</ta>
            <ta e="T240" id="Seg_1185" s="T239">pro.h:S</ta>
            <ta e="T241" id="Seg_1186" s="T240">np:O</ta>
            <ta e="T242" id="Seg_1187" s="T241">ptcl.neg</ta>
            <ta e="T243" id="Seg_1188" s="T242">v:pred</ta>
            <ta e="T249" id="Seg_1189" s="T248">pro.h:S</ta>
            <ta e="T250" id="Seg_1190" s="T249">v:pred</ta>
            <ta e="T251" id="Seg_1191" s="T250">np:O</ta>
            <ta e="T252" id="Seg_1192" s="T251">v:pred 0.2.h:S</ta>
            <ta e="T255" id="Seg_1193" s="T254">pro.h:S</ta>
            <ta e="T256" id="Seg_1194" s="T255">pro.h:O</ta>
            <ta e="T257" id="Seg_1195" s="T256">v:pred</ta>
            <ta e="T260" id="Seg_1196" s="T259">v:pred 0.3.h:S</ta>
            <ta e="T261" id="Seg_1197" s="T260">np:O</ta>
            <ta e="T263" id="Seg_1198" s="T262">ptcl:pred</ta>
            <ta e="T264" id="Seg_1199" s="T263">pro.h:O</ta>
            <ta e="T266" id="Seg_1200" s="T265">pro.h:S</ta>
            <ta e="T268" id="Seg_1201" s="T267">v:pred</ta>
            <ta e="T271" id="Seg_1202" s="T270">v:pred 0.3.h:S</ta>
            <ta e="T273" id="Seg_1203" s="T272">v:pred 0.3.h:S</ta>
            <ta e="T274" id="Seg_1204" s="T273">ptcl:pred</ta>
            <ta e="T275" id="Seg_1205" s="T274">np:O</ta>
            <ta e="T276" id="Seg_1206" s="T275">np.h:S</ta>
            <ta e="T277" id="Seg_1207" s="T276">v:pred</ta>
            <ta e="T278" id="Seg_1208" s="T277">pro.h:S</ta>
            <ta e="T279" id="Seg_1209" s="T278">np:O</ta>
            <ta e="T280" id="Seg_1210" s="T279">v:pred</ta>
            <ta e="T282" id="Seg_1211" s="T281">v:pred 0.3.h:S</ta>
            <ta e="T283" id="Seg_1212" s="T282">v:pred 0.3.h:S</ta>
            <ta e="T284" id="Seg_1213" s="T283">np.h:S</ta>
            <ta e="T285" id="Seg_1214" s="T284">v:pred</ta>
            <ta e="T290" id="Seg_1215" s="T289">np.h:S</ta>
            <ta e="T291" id="Seg_1216" s="T290">v:pred</ta>
            <ta e="T293" id="Seg_1217" s="T292">pro.h:S</ta>
            <ta e="T294" id="Seg_1218" s="T293">v:pred</ta>
            <ta e="T295" id="Seg_1219" s="T294">np:S</ta>
            <ta e="T296" id="Seg_1220" s="T295">adj:pred</ta>
            <ta e="T297" id="Seg_1221" s="T296">v:pred 0.1.h:S</ta>
            <ta e="T299" id="Seg_1222" s="T298">v:pred 0.3.h:S</ta>
            <ta e="T301" id="Seg_1223" s="T300">v:pred 0.3.h:S</ta>
            <ta e="T302" id="Seg_1224" s="T301">v:pred 0.3.h:S</ta>
            <ta e="T303" id="Seg_1225" s="T302">pro:O</ta>
            <ta e="T304" id="Seg_1226" s="T303">ptcl.neg</ta>
            <ta e="T305" id="Seg_1227" s="T304">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T253" id="Seg_1228" s="T252">RUS:gram</ta>
            <ta e="T258" id="Seg_1229" s="T257">TURK:core</ta>
            <ta e="T262" id="Seg_1230" s="T261">RUS:gram</ta>
            <ta e="T263" id="Seg_1231" s="T262">RUS:gram</ta>
            <ta e="T267" id="Seg_1232" s="T266">TURK:disc</ta>
            <ta e="T269" id="Seg_1233" s="T268">RUS:gram</ta>
            <ta e="T274" id="Seg_1234" s="T273">RUS:mod</ta>
            <ta e="T288" id="Seg_1235" s="T287">TURK:disc</ta>
            <ta e="T303" id="Seg_1236" s="T302">TURK:gram(INDEF)</ta>
            <ta e="T306" id="Seg_1237" s="T305">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T236" id="Seg_1238" s="T235">RUS:calq</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T219" id="Seg_1239" s="T215">Пошли шесть мудрецов.</ta>
            <ta e="T223" id="Seg_1240" s="T219">Землю копать, уголь жечь. [?]</ta>
            <ta e="T227" id="Seg_1241" s="T223">Потом их ноги утонули там.</ta>
            <ta e="T232" id="Seg_1242" s="T227">Они говорят: «Ни у кого ног нет».</ta>
            <ta e="T235" id="Seg_1243" s="T232">Потом пришёл человек.</ta>
            <ta e="T237" id="Seg_1244" s="T235">Почему они тут сидят?</ta>
            <ta e="T245" id="Seg_1245" s="T237">«Мы не знаем, где чьи ноги».</ta>
            <ta e="T250" id="Seg_1246" s="T245">Он говорит:</ta>
            <ta e="T258" id="Seg_1247" s="T250">«Если дадите мне ваши топоры, то я вам сделаю хорошо».</ta>
            <ta e="T265" id="Seg_1248" s="T258">Потом он взял палку и давай их бить.</ta>
            <ta e="T271" id="Seg_1249" s="T265">Они выскочили и побежали.</ta>
            <ta e="T275" id="Seg_1250" s="T271">Потом они говорят: «Нам нужны дрова».</ta>
            <ta e="T280" id="Seg_1251" s="T275">Один говорит: «Я ветку сломаю».</ta>
            <ta e="T283" id="Seg_1252" s="T280">Потом ломал её, ломал.</ta>
            <ta e="T286" id="Seg_1253" s="T283">Один взял [его] за ногу.</ta>
            <ta e="T291" id="Seg_1254" s="T286">Потом стали вчетвером [держать].</ta>
            <ta e="T296" id="Seg_1255" s="T291">Потом он говорит: «У меня рука взмокла.</ta>
            <ta e="T297" id="Seg_1256" s="T296">Я [её] вытру».</ta>
            <ta e="T300" id="Seg_1257" s="T297">Потом он отпустил (всех?).</ta>
            <ta e="T301" id="Seg_1258" s="T300">Они упали.</ta>
            <ta e="T302" id="Seg_1259" s="T301">Стали драться.</ta>
            <ta e="T305" id="Seg_1260" s="T302">Они ничего не знали.</ta>
            <ta e="T307" id="Seg_1261" s="T305">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T219" id="Seg_1262" s="T215">Six wizards went.</ta>
            <ta e="T223" id="Seg_1263" s="T219">To dig the ground, to burn coal. [?]</ta>
            <ta e="T227" id="Seg_1264" s="T223">Then their feet sank there.</ta>
            <ta e="T232" id="Seg_1265" s="T227">They say: "Nobody has feet."</ta>
            <ta e="T235" id="Seg_1266" s="T232">Then a man came.</ta>
            <ta e="T237" id="Seg_1267" s="T235">What are they sitting for?</ta>
            <ta e="T245" id="Seg_1268" s="T237">“We do not know [about] feet, whose are which.”</ta>
            <ta e="T250" id="Seg_1269" s="T245">He says:</ta>
            <ta e="T258" id="Seg_1270" s="T250">“If you give me [your] axes, then I will make you good.”</ta>
            <ta e="T265" id="Seg_1271" s="T258">Then he took [a piece of] wood and started to beat them.</ta>
            <ta e="T271" id="Seg_1272" s="T265">They jumped up and ran.</ta>
            <ta e="T275" id="Seg_1273" s="T271">Then they say: "[We] need wood."</ta>
            <ta e="T280" id="Seg_1274" s="T275">One says: "I will break a branch."</ta>
            <ta e="T283" id="Seg_1275" s="T280">Then he [tried and tried to] break [it].</ta>
            <ta e="T286" id="Seg_1276" s="T283">One took [him] by his foot.</ta>
            <ta e="T291" id="Seg_1277" s="T286">Then four [of them] hold [him].</ta>
            <ta e="T296" id="Seg_1278" s="T291">Then he says: "My hand is wet.</ta>
            <ta e="T297" id="Seg_1279" s="T296">I will wipe [it]."</ta>
            <ta e="T300" id="Seg_1280" s="T297">Then he released them (all?).</ta>
            <ta e="T301" id="Seg_1281" s="T300">They fell down.</ta>
            <ta e="T302" id="Seg_1282" s="T301">They [started to] fight.</ta>
            <ta e="T305" id="Seg_1283" s="T302">They knew nothing.</ta>
            <ta e="T307" id="Seg_1284" s="T305">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T219" id="Seg_1285" s="T215">Sechs Zauberer gingen.</ta>
            <ta e="T223" id="Seg_1286" s="T219">Die Erde zu buddeln, um Kohle zu brennen. [?]</ta>
            <ta e="T227" id="Seg_1287" s="T223">Dann sanken dort ihre Füße ein. </ta>
            <ta e="T232" id="Seg_1288" s="T227">Sie sagten: „Niemand hat Füße.“</ta>
            <ta e="T235" id="Seg_1289" s="T232">Dann kam ein Mann.</ta>
            <ta e="T237" id="Seg_1290" s="T235">Weshalb sitzen sie?</ta>
            <ta e="T245" id="Seg_1291" s="T237">„Wir wissen nichts [über] Füße, wessen sind welche.“</ta>
            <ta e="T250" id="Seg_1292" s="T245">Er sagte:</ta>
            <ta e="T258" id="Seg_1293" s="T250">„Wenn Sie mir [Ihre] Axte schenkt, dann werde ich Sie heil machen.“</ta>
            <ta e="T265" id="Seg_1294" s="T258">Dann nahm er [ein Stück] Holz und fing an sie zu schlagen.</ta>
            <ta e="T271" id="Seg_1295" s="T265">Sie sprangen auf und liefen.</ta>
            <ta e="T275" id="Seg_1296" s="T271">Dann sagten sie: „[Wir] brauchen Holz.“</ta>
            <ta e="T280" id="Seg_1297" s="T275">Einer sagt: „Ich werde einen Ast abbrechen.“</ta>
            <ta e="T283" id="Seg_1298" s="T280">Dann [versuchte und versuchte] er [es] zu brechen.</ta>
            <ta e="T286" id="Seg_1299" s="T283">Einer nahm [ihm] am Fuß.</ta>
            <ta e="T291" id="Seg_1300" s="T286">Dann wurden sie vier.</ta>
            <ta e="T296" id="Seg_1301" s="T291">Dann sagte er: „Meine Hand ist nass.</ta>
            <ta e="T297" id="Seg_1302" s="T296">Ich werde [sie] wischen.“</ta>
            <ta e="T300" id="Seg_1303" s="T297">Dann ließ er sie alle (los?).</ta>
            <ta e="T301" id="Seg_1304" s="T300">Sie fielen hinunter.</ta>
            <ta e="T302" id="Seg_1305" s="T301">Sie [fingen an zu] kämpfen.</ta>
            <ta e="T305" id="Seg_1306" s="T302">Sie wußten nichts.</ta>
            <ta e="T307" id="Seg_1307" s="T305">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T227" id="Seg_1308" s="T223">Păʔ- instead of păʔdə-.</ta>
            <ta e="T271" id="Seg_1309" s="T265">[GVY:] numəluʔpiʔi pronounced with -i- as in suʔmiluʔpiʔi </ta>
            <ta e="T280" id="Seg_1310" s="T275">Mu(ː) ’branch’.</ta>
            <ta e="T283" id="Seg_1311" s="T280">[GVY:] A hypothetical reconstruction of the Russian sentence "ломал, ломал"</ta>
            <ta e="T286" id="Seg_1312" s="T283">[AAV:] Not "with his hand" but "by the hand"?</ta>
            <ta e="T296" id="Seg_1313" s="T291">[GVY:] Donner (15b et al.): tʼü(p)pi</ta>
            <ta e="T307" id="Seg_1314" s="T305">[GVY:] A very tentative interpretation: One of the fools tried to break a branch and gripped it with his feet; three others took his hand; the first decided to wipe off his hand and released them, and everybody fell down on the ground.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
