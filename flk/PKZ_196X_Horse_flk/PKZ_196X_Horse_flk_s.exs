<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID93D40247-2AEA-4FFA-EEDC-9872F0D151BB">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_Horse_flk.wav" />
         <referenced-file url="PKZ_196X_Horse_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_Horse_flk\PKZ_196X_Horse_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">134</ud-information>
            <ud-information attribute-name="# HIAT:w">89</ud-information>
            <ud-information attribute-name="# e">89</ud-information>
            <ud-information attribute-name="# HIAT:u">21</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.032" type="appl" />
         <tli id="T1" time="0.45" type="appl" />
         <tli id="T2" time="0.867" type="appl" />
         <tli id="T3" time="1.285" type="appl" />
         <tli id="T4" time="1.702" type="appl" />
         <tli id="T5" time="2.67977595821099" />
         <tli id="T6" time="3.359" type="appl" />
         <tli id="T7" time="4.005" type="appl" />
         <tli id="T8" time="4.65" type="appl" />
         <tli id="T9" time="5.296" type="appl" />
         <tli id="T10" time="5.942" type="appl" />
         <tli id="T11" time="6.588" type="appl" />
         <tli id="T12" time="7.698" type="appl" />
         <tli id="T13" time="8.552" type="appl" />
         <tli id="T14" time="9.405" type="appl" />
         <tli id="T15" time="10.259" type="appl" />
         <tli id="T16" time="11.719020235161494" />
         <tli id="T17" time="12.733" type="appl" />
         <tli id="T18" time="13.368" type="appl" />
         <tli id="T19" time="14.004" type="appl" />
         <tli id="T20" time="14.639" type="appl" />
         <tli id="T21" time="15.275" type="appl" />
         <tli id="T22" time="15.91" type="appl" />
         <tli id="T23" time="16.738600574795512" />
         <tli id="T24" time="17.561" type="appl" />
         <tli id="T25" time="18.576" type="appl" />
         <tli id="T26" time="19.592" type="appl" />
         <tli id="T27" time="21.078237760853604" />
         <tli id="T28" time="22.587" type="appl" />
         <tli id="T29" time="23.872" type="appl" />
         <tli id="T30" time="25.931165366768038" />
         <tli id="T31" time="26.425" type="appl" />
         <tli id="T32" time="27.692" type="appl" />
         <tli id="T33" time="29.884168210596687" />
         <tli id="T34" time="31.333" type="appl" />
         <tli id="T35" time="32.616" type="appl" />
         <tli id="T36" time="33.9" type="appl" />
         <tli id="T37" time="35.183" type="appl" />
         <tli id="T38" time="36.466" type="appl" />
         <tli id="T39" time="37.144" type="appl" />
         <tli id="T40" time="37.699" type="appl" />
         <tli id="T41" time="38.253" type="appl" />
         <tli id="T42" time="38.808" type="appl" />
         <tli id="T43" time="39.67001673461095" />
         <tli id="T44" time="40.356" type="appl" />
         <tli id="T45" time="41.048" type="appl" />
         <tli id="T46" time="41.74" type="appl" />
         <tli id="T47" time="42.432" type="appl" />
         <tli id="T48" time="43.124" type="appl" />
         <tli id="T49" time="43.816" type="appl" />
         <tli id="T50" time="44.64" type="appl" />
         <tli id="T51" time="45.448" type="appl" />
         <tli id="T52" time="46.255" type="appl" />
         <tli id="T53" time="47.063" type="appl" />
         <tli id="T54" time="48.71592712091024" />
         <tli id="T55" time="49.95" type="appl" />
         <tli id="T56" time="51.239" type="appl" />
         <tli id="T57" time="52.528" type="appl" />
         <tli id="T58" time="53.224" type="appl" />
         <tli id="T59" time="53.919" type="appl" />
         <tli id="T60" time="54.614" type="appl" />
         <tli id="T61" time="55.31" type="appl" />
         <tli id="T62" time="55.782" type="appl" />
         <tli id="T63" time="56.254" type="appl" />
         <tli id="T64" time="56.725" type="appl" />
         <tli id="T65" time="57.197" type="appl" />
         <tli id="T66" time="58.025" type="appl" />
         <tli id="T67" time="58.853" type="appl" />
         <tli id="T68" time="60.37495237193268" />
         <tli id="T69" time="61.425" type="appl" />
         <tli id="T70" time="63.96798531092701" />
         <tli id="T71" time="65.203" type="appl" />
         <tli id="T72" time="66.396" type="appl" />
         <tli id="T73" time="67.59" type="appl" />
         <tli id="T74" time="68.584" type="appl" />
         <tli id="T75" time="69.579" type="appl" />
         <tli id="T76" time="70.574" type="appl" />
         <tli id="T77" time="71.568" type="appl" />
         <tli id="T78" time="72.112" type="appl" />
         <tli id="T79" time="72.657" type="appl" />
         <tli id="T80" time="73.201" type="appl" />
         <tli id="T81" time="73.562" type="appl" />
         <tli id="T82" time="73.924" type="appl" />
         <tli id="T83" time="74.285" type="appl" />
         <tli id="T84" time="75.15" type="appl" />
         <tli id="T85" time="76.015" type="appl" />
         <tli id="T86" time="77.20687847761116" />
         <tli id="T87" time="78.072" type="appl" />
         <tli id="T88" time="78.771" type="appl" />
         <tli id="T89" time="79.459" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T89" id="Seg_0" n="sc" s="T0">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Onʼiʔ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">kuza</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">i</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">ne</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">ibi</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_20" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">Aktʼat</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">naga</ts>
                  <nts id="Seg_26" n="HIAT:ip">,</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">nada</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">ine</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_34" n="HIAT:ip">(</nts>
                  <ts e="T10" id="Seg_36" n="HIAT:w" s="T9">sa-</ts>
                  <nts id="Seg_37" n="HIAT:ip">)</nts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">sadarzittə</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_44" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">Dĭgəttə</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">băzəbi</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">dĭ</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">inem</ts>
                  <nts id="Seg_56" n="HIAT:ip">,</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">sabənziʔ</ts>
                  <nts id="Seg_60" n="HIAT:ip">.</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_63" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_65" n="HIAT:w" s="T16">Ertən</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_68" n="HIAT:w" s="T17">uʔbdəbi</ts>
                  <nts id="Seg_69" n="HIAT:ip">,</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_72" n="HIAT:w" s="T18">bar</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_75" n="HIAT:w" s="T19">balgaš</ts>
                  <nts id="Seg_76" n="HIAT:ip">,</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_78" n="HIAT:ip">(</nts>
                  <ts e="T21" id="Seg_80" n="HIAT:w" s="T20">šo-</ts>
                  <nts id="Seg_81" n="HIAT:ip">)</nts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_84" n="HIAT:w" s="T21">surno</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_87" n="HIAT:w" s="T22">šonəga</ts>
                  <nts id="Seg_88" n="HIAT:ip">.</nts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_91" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_93" n="HIAT:w" s="T23">Kăda</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_96" n="HIAT:w" s="T24">kunzittə</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_99" n="HIAT:w" s="T25">inem</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_102" n="HIAT:w" s="T26">bazardə</ts>
                  <nts id="Seg_103" n="HIAT:ip">?</nts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_106" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_108" n="HIAT:w" s="T27">Kambi</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_111" n="HIAT:w" s="T28">kuzanə</ts>
                  <nts id="Seg_112" n="HIAT:ip">,</nts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_115" n="HIAT:w" s="T29">măndə:</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_117" n="HIAT:ip">"</nts>
                  <ts e="T31" id="Seg_119" n="HIAT:w" s="T30">Inem</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_122" n="HIAT:w" s="T31">ugaːndə</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_125" n="HIAT:w" s="T32">balgaš</ts>
                  <nts id="Seg_126" n="HIAT:ip">.</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_129" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_131" n="HIAT:w" s="T33">Inegən</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_134" n="HIAT:w" s="T34">bar</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_137" n="HIAT:w" s="T35">xvos</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_140" n="HIAT:w" s="T36">balgaštə</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_143" n="HIAT:w" s="T37">moləj</ts>
                  <nts id="Seg_144" n="HIAT:ip">.</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_147" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_149" n="HIAT:w" s="T38">Ĭmbi</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_152" n="HIAT:w" s="T39">ej</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_155" n="HIAT:w" s="T40">tĭmniel</ts>
                  <nts id="Seg_156" n="HIAT:ip">,</nts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_159" n="HIAT:w" s="T41">ĭmbi</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_162" n="HIAT:w" s="T42">azittə</ts>
                  <nts id="Seg_163" n="HIAT:ip">"</nts>
                  <nts id="Seg_164" n="HIAT:ip">.</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_167" n="HIAT:u" s="T43">
                  <nts id="Seg_168" n="HIAT:ip">"</nts>
                  <ts e="T44" id="Seg_170" n="HIAT:w" s="T43">Sajjaʔtə</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_173" n="HIAT:w" s="T44">da</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_176" n="HIAT:w" s="T45">băranə</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_179" n="HIAT:w" s="T46">enneʔ</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_182" n="HIAT:w" s="T47">da</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_185" n="HIAT:w" s="T48">kanaʔ</ts>
                  <nts id="Seg_186" n="HIAT:ip">"</nts>
                  <nts id="Seg_187" n="HIAT:ip">.</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_190" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_192" n="HIAT:w" s="T49">Dĭgəttə</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_195" n="HIAT:w" s="T50">kandəga</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_198" n="HIAT:w" s="T51">maːndə</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_201" n="HIAT:w" s="T52">dĭ</ts>
                  <nts id="Seg_202" n="HIAT:ip">,</nts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_205" n="HIAT:w" s="T53">kudonzlaʔbə</ts>
                  <nts id="Seg_206" n="HIAT:ip">.</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_209" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_211" n="HIAT:w" s="T54">Dĭgəttə</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_214" n="HIAT:w" s="T55">xvostə</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_217" n="HIAT:w" s="T56">sajjaʔpi</ts>
                  <nts id="Seg_218" n="HIAT:ip">.</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_221" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_223" n="HIAT:w" s="T57">Băranə</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_226" n="HIAT:w" s="T58">embi</ts>
                  <nts id="Seg_227" n="HIAT:ip">,</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_230" n="HIAT:w" s="T59">kambi</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_233" n="HIAT:w" s="T60">bazardə</ts>
                  <nts id="Seg_234" n="HIAT:ip">.</nts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_237" n="HIAT:u" s="T61">
                  <nts id="Seg_238" n="HIAT:ip">(</nts>
                  <ts e="T62" id="Seg_240" n="HIAT:w" s="T61">Dĭ=</ts>
                  <nts id="Seg_241" n="HIAT:ip">)</nts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_244" n="HIAT:w" s="T62">Dĭn</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_247" n="HIAT:w" s="T63">il</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_250" n="HIAT:w" s="T64">šobiʔi</ts>
                  <nts id="Seg_251" n="HIAT:ip">.</nts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_254" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_256" n="HIAT:w" s="T65">Dĭ</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_259" n="HIAT:w" s="T66">inem</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_262" n="HIAT:w" s="T67">măndərbiʔi</ts>
                  <nts id="Seg_263" n="HIAT:ip">.</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_266" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_268" n="HIAT:w" s="T68">Amnəbiʔi</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_271" n="HIAT:w" s="T69">dĭʔnə</ts>
                  <nts id="Seg_272" n="HIAT:ip">.</nts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_275" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_277" n="HIAT:w" s="T70">Ujut</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_280" n="HIAT:w" s="T71">bar</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_283" n="HIAT:w" s="T72">kubiʔi</ts>
                  <nts id="Seg_284" n="HIAT:ip">.</nts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_287" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_289" n="HIAT:w" s="T73">Dĭgəttə:</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_291" n="HIAT:ip">"</nts>
                  <nts id="Seg_292" n="HIAT:ip">(</nts>
                  <ts e="T75" id="Seg_294" n="HIAT:w" s="T74">In-</ts>
                  <nts id="Seg_295" n="HIAT:ip">)</nts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_298" n="HIAT:w" s="T75">Ine</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_301" n="HIAT:w" s="T76">jakšə</ts>
                  <nts id="Seg_302" n="HIAT:ip">.</nts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_305" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_307" n="HIAT:w" s="T77">A</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_310" n="HIAT:w" s="T78">xvostə</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_313" n="HIAT:w" s="T79">naga</ts>
                  <nts id="Seg_314" n="HIAT:ip">"</nts>
                  <nts id="Seg_315" n="HIAT:ip">.</nts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_318" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_320" n="HIAT:w" s="T80">A</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_323" n="HIAT:w" s="T81">dĭ</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_326" n="HIAT:w" s="T82">măndə:</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_328" n="HIAT:ip">"</nts>
                  <ts e="T84" id="Seg_330" n="HIAT:w" s="T83">Xvos</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_333" n="HIAT:w" s="T84">băragən</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_336" n="HIAT:w" s="T85">iʔbəlaʔbə</ts>
                  <nts id="Seg_337" n="HIAT:ip">.</nts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_340" n="HIAT:u" s="T86">
                  <ts e="T87" id="Seg_342" n="HIAT:w" s="T86">I</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_345" n="HIAT:w" s="T87">ige</ts>
                  <nts id="Seg_346" n="HIAT:ip">"</nts>
                  <nts id="Seg_347" n="HIAT:ip">.</nts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_350" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_352" n="HIAT:w" s="T88">Kabarləj</ts>
                  <nts id="Seg_353" n="HIAT:ip">.</nts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T89" id="Seg_355" n="sc" s="T0">
               <ts e="T1" id="Seg_357" n="e" s="T0">Onʼiʔ </ts>
               <ts e="T2" id="Seg_359" n="e" s="T1">kuza </ts>
               <ts e="T3" id="Seg_361" n="e" s="T2">i </ts>
               <ts e="T4" id="Seg_363" n="e" s="T3">ne </ts>
               <ts e="T5" id="Seg_365" n="e" s="T4">ibi. </ts>
               <ts e="T6" id="Seg_367" n="e" s="T5">Aktʼat </ts>
               <ts e="T7" id="Seg_369" n="e" s="T6">naga, </ts>
               <ts e="T8" id="Seg_371" n="e" s="T7">nada </ts>
               <ts e="T9" id="Seg_373" n="e" s="T8">ine </ts>
               <ts e="T10" id="Seg_375" n="e" s="T9">(sa-) </ts>
               <ts e="T11" id="Seg_377" n="e" s="T10">sadarzittə. </ts>
               <ts e="T12" id="Seg_379" n="e" s="T11">Dĭgəttə </ts>
               <ts e="T13" id="Seg_381" n="e" s="T12">băzəbi </ts>
               <ts e="T14" id="Seg_383" n="e" s="T13">dĭ </ts>
               <ts e="T15" id="Seg_385" n="e" s="T14">inem, </ts>
               <ts e="T16" id="Seg_387" n="e" s="T15">sabənziʔ. </ts>
               <ts e="T17" id="Seg_389" n="e" s="T16">Ertən </ts>
               <ts e="T18" id="Seg_391" n="e" s="T17">uʔbdəbi, </ts>
               <ts e="T19" id="Seg_393" n="e" s="T18">bar </ts>
               <ts e="T20" id="Seg_395" n="e" s="T19">balgaš, </ts>
               <ts e="T21" id="Seg_397" n="e" s="T20">(šo-) </ts>
               <ts e="T22" id="Seg_399" n="e" s="T21">surno </ts>
               <ts e="T23" id="Seg_401" n="e" s="T22">šonəga. </ts>
               <ts e="T24" id="Seg_403" n="e" s="T23">Kăda </ts>
               <ts e="T25" id="Seg_405" n="e" s="T24">kunzittə </ts>
               <ts e="T26" id="Seg_407" n="e" s="T25">inem </ts>
               <ts e="T27" id="Seg_409" n="e" s="T26">bazardə? </ts>
               <ts e="T28" id="Seg_411" n="e" s="T27">Kambi </ts>
               <ts e="T29" id="Seg_413" n="e" s="T28">kuzanə, </ts>
               <ts e="T30" id="Seg_415" n="e" s="T29">măndə: </ts>
               <ts e="T31" id="Seg_417" n="e" s="T30">"Inem </ts>
               <ts e="T32" id="Seg_419" n="e" s="T31">ugaːndə </ts>
               <ts e="T33" id="Seg_421" n="e" s="T32">balgaš. </ts>
               <ts e="T34" id="Seg_423" n="e" s="T33">Inegən </ts>
               <ts e="T35" id="Seg_425" n="e" s="T34">bar </ts>
               <ts e="T36" id="Seg_427" n="e" s="T35">xvos </ts>
               <ts e="T37" id="Seg_429" n="e" s="T36">balgaštə </ts>
               <ts e="T38" id="Seg_431" n="e" s="T37">moləj. </ts>
               <ts e="T39" id="Seg_433" n="e" s="T38">Ĭmbi </ts>
               <ts e="T40" id="Seg_435" n="e" s="T39">ej </ts>
               <ts e="T41" id="Seg_437" n="e" s="T40">tĭmniel, </ts>
               <ts e="T42" id="Seg_439" n="e" s="T41">ĭmbi </ts>
               <ts e="T43" id="Seg_441" n="e" s="T42">azittə". </ts>
               <ts e="T44" id="Seg_443" n="e" s="T43">"Sajjaʔtə </ts>
               <ts e="T45" id="Seg_445" n="e" s="T44">da </ts>
               <ts e="T46" id="Seg_447" n="e" s="T45">băranə </ts>
               <ts e="T47" id="Seg_449" n="e" s="T46">enneʔ </ts>
               <ts e="T48" id="Seg_451" n="e" s="T47">da </ts>
               <ts e="T49" id="Seg_453" n="e" s="T48">kanaʔ". </ts>
               <ts e="T50" id="Seg_455" n="e" s="T49">Dĭgəttə </ts>
               <ts e="T51" id="Seg_457" n="e" s="T50">kandəga </ts>
               <ts e="T52" id="Seg_459" n="e" s="T51">maːndə </ts>
               <ts e="T53" id="Seg_461" n="e" s="T52">dĭ, </ts>
               <ts e="T54" id="Seg_463" n="e" s="T53">kudonzlaʔbə. </ts>
               <ts e="T55" id="Seg_465" n="e" s="T54">Dĭgəttə </ts>
               <ts e="T56" id="Seg_467" n="e" s="T55">xvostə </ts>
               <ts e="T57" id="Seg_469" n="e" s="T56">sajjaʔpi. </ts>
               <ts e="T58" id="Seg_471" n="e" s="T57">Băranə </ts>
               <ts e="T59" id="Seg_473" n="e" s="T58">embi, </ts>
               <ts e="T60" id="Seg_475" n="e" s="T59">kambi </ts>
               <ts e="T61" id="Seg_477" n="e" s="T60">bazardə. </ts>
               <ts e="T62" id="Seg_479" n="e" s="T61">(Dĭ=) </ts>
               <ts e="T63" id="Seg_481" n="e" s="T62">Dĭn </ts>
               <ts e="T64" id="Seg_483" n="e" s="T63">il </ts>
               <ts e="T65" id="Seg_485" n="e" s="T64">šobiʔi. </ts>
               <ts e="T66" id="Seg_487" n="e" s="T65">Dĭ </ts>
               <ts e="T67" id="Seg_489" n="e" s="T66">inem </ts>
               <ts e="T68" id="Seg_491" n="e" s="T67">măndərbiʔi. </ts>
               <ts e="T69" id="Seg_493" n="e" s="T68">Amnəbiʔi </ts>
               <ts e="T70" id="Seg_495" n="e" s="T69">dĭʔnə. </ts>
               <ts e="T71" id="Seg_497" n="e" s="T70">Ujut </ts>
               <ts e="T72" id="Seg_499" n="e" s="T71">bar </ts>
               <ts e="T73" id="Seg_501" n="e" s="T72">kubiʔi. </ts>
               <ts e="T74" id="Seg_503" n="e" s="T73">Dĭgəttə: </ts>
               <ts e="T75" id="Seg_505" n="e" s="T74">"(In-) </ts>
               <ts e="T76" id="Seg_507" n="e" s="T75">Ine </ts>
               <ts e="T77" id="Seg_509" n="e" s="T76">jakšə. </ts>
               <ts e="T78" id="Seg_511" n="e" s="T77">A </ts>
               <ts e="T79" id="Seg_513" n="e" s="T78">xvostə </ts>
               <ts e="T80" id="Seg_515" n="e" s="T79">naga". </ts>
               <ts e="T81" id="Seg_517" n="e" s="T80">A </ts>
               <ts e="T82" id="Seg_519" n="e" s="T81">dĭ </ts>
               <ts e="T83" id="Seg_521" n="e" s="T82">măndə: </ts>
               <ts e="T84" id="Seg_523" n="e" s="T83">"Xvos </ts>
               <ts e="T85" id="Seg_525" n="e" s="T84">băragən </ts>
               <ts e="T86" id="Seg_527" n="e" s="T85">iʔbəlaʔbə. </ts>
               <ts e="T87" id="Seg_529" n="e" s="T86">I </ts>
               <ts e="T88" id="Seg_531" n="e" s="T87">ige". </ts>
               <ts e="T89" id="Seg_533" n="e" s="T88">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_534" s="T0">PKZ_196X_Horse_flk.001 (001)</ta>
            <ta e="T11" id="Seg_535" s="T5">PKZ_196X_Horse_flk.002 (002)</ta>
            <ta e="T16" id="Seg_536" s="T11">PKZ_196X_Horse_flk.003 (003)</ta>
            <ta e="T23" id="Seg_537" s="T16">PKZ_196X_Horse_flk.004 (004)</ta>
            <ta e="T27" id="Seg_538" s="T23">PKZ_196X_Horse_flk.005 (005)</ta>
            <ta e="T33" id="Seg_539" s="T27">PKZ_196X_Horse_flk.006 (006) </ta>
            <ta e="T38" id="Seg_540" s="T33">PKZ_196X_Horse_flk.007 (008)</ta>
            <ta e="T43" id="Seg_541" s="T38">PKZ_196X_Horse_flk.008 (009)</ta>
            <ta e="T49" id="Seg_542" s="T43">PKZ_196X_Horse_flk.009 (010)</ta>
            <ta e="T54" id="Seg_543" s="T49">PKZ_196X_Horse_flk.010 (011)</ta>
            <ta e="T57" id="Seg_544" s="T54">PKZ_196X_Horse_flk.011 (012)</ta>
            <ta e="T61" id="Seg_545" s="T57">PKZ_196X_Horse_flk.012 (013)</ta>
            <ta e="T65" id="Seg_546" s="T61">PKZ_196X_Horse_flk.013 (014)</ta>
            <ta e="T68" id="Seg_547" s="T65">PKZ_196X_Horse_flk.014 (015)</ta>
            <ta e="T70" id="Seg_548" s="T68">PKZ_196X_Horse_flk.015 (016)</ta>
            <ta e="T73" id="Seg_549" s="T70">PKZ_196X_Horse_flk.016 (017)</ta>
            <ta e="T77" id="Seg_550" s="T73">PKZ_196X_Horse_flk.017 (018)</ta>
            <ta e="T80" id="Seg_551" s="T77">PKZ_196X_Horse_flk.018 (019)</ta>
            <ta e="T86" id="Seg_552" s="T80">PKZ_196X_Horse_flk.019 (020) </ta>
            <ta e="T88" id="Seg_553" s="T86">PKZ_196X_Horse_flk.020 (022)</ta>
            <ta e="T89" id="Seg_554" s="T88">PKZ_196X_Horse_flk.021 (023)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_555" s="T0">Onʼiʔ kuza i ne ibi. </ta>
            <ta e="T11" id="Seg_556" s="T5">Aktʼat naga, nada ine (sa-) sadarzittə. </ta>
            <ta e="T16" id="Seg_557" s="T11">Dĭgəttə băzəbi dĭ inem, sabənziʔ. </ta>
            <ta e="T23" id="Seg_558" s="T16">Ertən uʔbdəbi, bar balgaš, (šo-) surno šonəga. </ta>
            <ta e="T27" id="Seg_559" s="T23">Kăda kunzittə inem bazardə? </ta>
            <ta e="T33" id="Seg_560" s="T27">Kambi kuzanə, măndə: "Inem ugaːndə balgaš. </ta>
            <ta e="T38" id="Seg_561" s="T33">Inegən bar xvos balgaštə moləj. </ta>
            <ta e="T43" id="Seg_562" s="T38">Ĭmbi ej tĭmniel, ĭmbi azittə". </ta>
            <ta e="T49" id="Seg_563" s="T43">"Sajjaʔtə da băranə enneʔ da kanaʔ". </ta>
            <ta e="T54" id="Seg_564" s="T49">Dĭgəttə kandəga maːndə dĭ, kudonzlaʔbə. </ta>
            <ta e="T57" id="Seg_565" s="T54">Dĭgəttə xvostə sajjaʔpi. </ta>
            <ta e="T61" id="Seg_566" s="T57">Băranə embi, kambi bazardə. </ta>
            <ta e="T65" id="Seg_567" s="T61">(Dĭ=) Dĭn il šobiʔi. </ta>
            <ta e="T68" id="Seg_568" s="T65">Dĭ inem măndərbiʔi. </ta>
            <ta e="T70" id="Seg_569" s="T68">Amnəbiʔi dĭʔnə. </ta>
            <ta e="T73" id="Seg_570" s="T70">Ujut bar kubiʔi. </ta>
            <ta e="T77" id="Seg_571" s="T73">Dĭgəttə: "(In-) Ine jakšə. </ta>
            <ta e="T80" id="Seg_572" s="T77">A xvostə naga." </ta>
            <ta e="T86" id="Seg_573" s="T80">A dĭ măndə: Xvos băragən iʔbəlaʔbə. </ta>
            <ta e="T88" id="Seg_574" s="T86">I ige". </ta>
            <ta e="T89" id="Seg_575" s="T88">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_576" s="T0">onʼiʔ</ta>
            <ta e="T2" id="Seg_577" s="T1">kuza</ta>
            <ta e="T3" id="Seg_578" s="T2">i</ta>
            <ta e="T4" id="Seg_579" s="T3">ne</ta>
            <ta e="T5" id="Seg_580" s="T4">i-bi</ta>
            <ta e="T6" id="Seg_581" s="T5">aktʼa-t</ta>
            <ta e="T7" id="Seg_582" s="T6">naga</ta>
            <ta e="T8" id="Seg_583" s="T7">nada</ta>
            <ta e="T9" id="Seg_584" s="T8">ine</ta>
            <ta e="T11" id="Seg_585" s="T10">sadar-zittə</ta>
            <ta e="T12" id="Seg_586" s="T11">dĭgəttə</ta>
            <ta e="T13" id="Seg_587" s="T12">băzə-bi</ta>
            <ta e="T14" id="Seg_588" s="T13">dĭ</ta>
            <ta e="T15" id="Seg_589" s="T14">ine-m</ta>
            <ta e="T16" id="Seg_590" s="T15">sabən-ziʔ</ta>
            <ta e="T17" id="Seg_591" s="T16">ertə-n</ta>
            <ta e="T18" id="Seg_592" s="T17">uʔbdə-bi</ta>
            <ta e="T19" id="Seg_593" s="T18">bar</ta>
            <ta e="T20" id="Seg_594" s="T19">balgaš</ta>
            <ta e="T22" id="Seg_595" s="T21">surno</ta>
            <ta e="T23" id="Seg_596" s="T22">šonə-ga</ta>
            <ta e="T24" id="Seg_597" s="T23">kăda</ta>
            <ta e="T25" id="Seg_598" s="T24">kun-zittə</ta>
            <ta e="T26" id="Seg_599" s="T25">ine-m</ta>
            <ta e="T27" id="Seg_600" s="T26">bazar-də</ta>
            <ta e="T28" id="Seg_601" s="T27">kam-bi</ta>
            <ta e="T29" id="Seg_602" s="T28">kuza-nə</ta>
            <ta e="T30" id="Seg_603" s="T29">măn-də</ta>
            <ta e="T31" id="Seg_604" s="T30">ine-m</ta>
            <ta e="T32" id="Seg_605" s="T31">ugaːndə</ta>
            <ta e="T33" id="Seg_606" s="T32">balgaš</ta>
            <ta e="T34" id="Seg_607" s="T33">ine-gən</ta>
            <ta e="T35" id="Seg_608" s="T34">bar</ta>
            <ta e="T36" id="Seg_609" s="T35">xvos</ta>
            <ta e="T37" id="Seg_610" s="T36">balgaš-tə</ta>
            <ta e="T38" id="Seg_611" s="T37">mo-lə-j</ta>
            <ta e="T39" id="Seg_612" s="T38">ĭmbi</ta>
            <ta e="T40" id="Seg_613" s="T39">ej</ta>
            <ta e="T41" id="Seg_614" s="T40">tĭm-nie-l</ta>
            <ta e="T42" id="Seg_615" s="T41">ĭmbi</ta>
            <ta e="T43" id="Seg_616" s="T42">a-zittə</ta>
            <ta e="T44" id="Seg_617" s="T43">saj-jaʔ-tə</ta>
            <ta e="T45" id="Seg_618" s="T44">da</ta>
            <ta e="T46" id="Seg_619" s="T45">băra-nə</ta>
            <ta e="T47" id="Seg_620" s="T46">enn-e-ʔ</ta>
            <ta e="T48" id="Seg_621" s="T47">da</ta>
            <ta e="T49" id="Seg_622" s="T48">kan-a-ʔ</ta>
            <ta e="T50" id="Seg_623" s="T49">dĭgəttə</ta>
            <ta e="T51" id="Seg_624" s="T50">kandə-ga</ta>
            <ta e="T52" id="Seg_625" s="T51">ma-ndə</ta>
            <ta e="T53" id="Seg_626" s="T52">dĭ</ta>
            <ta e="T54" id="Seg_627" s="T53">kudo-nz-laʔbə</ta>
            <ta e="T55" id="Seg_628" s="T54">dĭgəttə</ta>
            <ta e="T56" id="Seg_629" s="T55">xvostə</ta>
            <ta e="T57" id="Seg_630" s="T56">saj-jaʔ-pi</ta>
            <ta e="T58" id="Seg_631" s="T57">băra-nə</ta>
            <ta e="T59" id="Seg_632" s="T58">em-bi</ta>
            <ta e="T60" id="Seg_633" s="T59">kam-bi</ta>
            <ta e="T61" id="Seg_634" s="T60">bazar-də</ta>
            <ta e="T62" id="Seg_635" s="T61">dĭ</ta>
            <ta e="T63" id="Seg_636" s="T62">dĭn</ta>
            <ta e="T64" id="Seg_637" s="T63">il</ta>
            <ta e="T65" id="Seg_638" s="T64">šo-bi-ʔi</ta>
            <ta e="T66" id="Seg_639" s="T65">dĭ</ta>
            <ta e="T67" id="Seg_640" s="T66">ine-m</ta>
            <ta e="T68" id="Seg_641" s="T67">măndə-r-bi-ʔi</ta>
            <ta e="T69" id="Seg_642" s="T68">amnə-bi-ʔi</ta>
            <ta e="T70" id="Seg_643" s="T69">dĭʔ-nə</ta>
            <ta e="T71" id="Seg_644" s="T70">uju-t</ta>
            <ta e="T72" id="Seg_645" s="T71">bar</ta>
            <ta e="T73" id="Seg_646" s="T72">ku-bi-ʔi</ta>
            <ta e="T74" id="Seg_647" s="T73">dĭgəttə</ta>
            <ta e="T76" id="Seg_648" s="T75">ine</ta>
            <ta e="T77" id="Seg_649" s="T76">jakšə</ta>
            <ta e="T78" id="Seg_650" s="T77">a</ta>
            <ta e="T79" id="Seg_651" s="T78">xvostə</ta>
            <ta e="T80" id="Seg_652" s="T79">naga</ta>
            <ta e="T81" id="Seg_653" s="T80">a</ta>
            <ta e="T82" id="Seg_654" s="T81">dĭ</ta>
            <ta e="T83" id="Seg_655" s="T82">măn-də</ta>
            <ta e="T84" id="Seg_656" s="T83">xvos</ta>
            <ta e="T85" id="Seg_657" s="T84">băra-gən</ta>
            <ta e="T86" id="Seg_658" s="T85">iʔbə-laʔbə</ta>
            <ta e="T87" id="Seg_659" s="T86">i</ta>
            <ta e="T88" id="Seg_660" s="T87">i-ge</ta>
            <ta e="T89" id="Seg_661" s="T88">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_662" s="T0">onʼiʔ</ta>
            <ta e="T2" id="Seg_663" s="T1">kuza</ta>
            <ta e="T3" id="Seg_664" s="T2">i</ta>
            <ta e="T4" id="Seg_665" s="T3">ne</ta>
            <ta e="T5" id="Seg_666" s="T4">i-bi</ta>
            <ta e="T6" id="Seg_667" s="T5">aktʼa-t</ta>
            <ta e="T7" id="Seg_668" s="T6">naga</ta>
            <ta e="T8" id="Seg_669" s="T7">nadə</ta>
            <ta e="T9" id="Seg_670" s="T8">ine</ta>
            <ta e="T11" id="Seg_671" s="T10">sădar-zittə</ta>
            <ta e="T12" id="Seg_672" s="T11">dĭgəttə</ta>
            <ta e="T13" id="Seg_673" s="T12">bazə-bi</ta>
            <ta e="T14" id="Seg_674" s="T13">dĭ</ta>
            <ta e="T15" id="Seg_675" s="T14">ine-m</ta>
            <ta e="T16" id="Seg_676" s="T15">sabən-ziʔ</ta>
            <ta e="T17" id="Seg_677" s="T16">ertə-n</ta>
            <ta e="T18" id="Seg_678" s="T17">uʔbdə-bi</ta>
            <ta e="T19" id="Seg_679" s="T18">bar</ta>
            <ta e="T20" id="Seg_680" s="T19">balgaš</ta>
            <ta e="T22" id="Seg_681" s="T21">surno</ta>
            <ta e="T23" id="Seg_682" s="T22">šonə-gA</ta>
            <ta e="T24" id="Seg_683" s="T23">kădaʔ</ta>
            <ta e="T25" id="Seg_684" s="T24">kun-zittə</ta>
            <ta e="T26" id="Seg_685" s="T25">ine-m</ta>
            <ta e="T27" id="Seg_686" s="T26">bazar-Tə</ta>
            <ta e="T28" id="Seg_687" s="T27">kan-bi</ta>
            <ta e="T29" id="Seg_688" s="T28">kuza-Tə</ta>
            <ta e="T30" id="Seg_689" s="T29">măn-ntə</ta>
            <ta e="T31" id="Seg_690" s="T30">ine-m</ta>
            <ta e="T32" id="Seg_691" s="T31">ugaːndə</ta>
            <ta e="T33" id="Seg_692" s="T32">balgaš</ta>
            <ta e="T34" id="Seg_693" s="T33">ine-Kən</ta>
            <ta e="T35" id="Seg_694" s="T34">bar</ta>
            <ta e="T36" id="Seg_695" s="T35">xvostə</ta>
            <ta e="T37" id="Seg_696" s="T36">balgaš-Tə</ta>
            <ta e="T38" id="Seg_697" s="T37">mo-lV-j</ta>
            <ta e="T39" id="Seg_698" s="T38">ĭmbi</ta>
            <ta e="T40" id="Seg_699" s="T39">ej</ta>
            <ta e="T41" id="Seg_700" s="T40">tĭm-liA-l</ta>
            <ta e="T42" id="Seg_701" s="T41">ĭmbi</ta>
            <ta e="T43" id="Seg_702" s="T42">a-zittə</ta>
            <ta e="T44" id="Seg_703" s="T43">săj-hʼaʔ-t</ta>
            <ta e="T45" id="Seg_704" s="T44">da</ta>
            <ta e="T46" id="Seg_705" s="T45">băra-Tə</ta>
            <ta e="T47" id="Seg_706" s="T46">hen-ə-ʔ</ta>
            <ta e="T48" id="Seg_707" s="T47">da</ta>
            <ta e="T49" id="Seg_708" s="T48">kan-ə-ʔ</ta>
            <ta e="T50" id="Seg_709" s="T49">dĭgəttə</ta>
            <ta e="T51" id="Seg_710" s="T50">kandə-gA</ta>
            <ta e="T52" id="Seg_711" s="T51">maʔ-gəndə</ta>
            <ta e="T53" id="Seg_712" s="T52">dĭ</ta>
            <ta e="T54" id="Seg_713" s="T53">kudo-nzə-laʔbə</ta>
            <ta e="T55" id="Seg_714" s="T54">dĭgəttə</ta>
            <ta e="T56" id="Seg_715" s="T55">xvostə</ta>
            <ta e="T57" id="Seg_716" s="T56">săj-hʼaʔ-bi</ta>
            <ta e="T58" id="Seg_717" s="T57">băra-Tə</ta>
            <ta e="T59" id="Seg_718" s="T58">hen-bi</ta>
            <ta e="T60" id="Seg_719" s="T59">kan-bi</ta>
            <ta e="T61" id="Seg_720" s="T60">bazar-Tə</ta>
            <ta e="T62" id="Seg_721" s="T61">dĭ</ta>
            <ta e="T63" id="Seg_722" s="T62">dĭn</ta>
            <ta e="T64" id="Seg_723" s="T63">il</ta>
            <ta e="T65" id="Seg_724" s="T64">šo-bi-jəʔ</ta>
            <ta e="T66" id="Seg_725" s="T65">dĭ</ta>
            <ta e="T67" id="Seg_726" s="T66">ine-m</ta>
            <ta e="T68" id="Seg_727" s="T67">măndo-r-bi-jəʔ</ta>
            <ta e="T69" id="Seg_728" s="T68">amnə-bi-jəʔ</ta>
            <ta e="T70" id="Seg_729" s="T69">dĭ-Tə</ta>
            <ta e="T71" id="Seg_730" s="T70">üjü-t</ta>
            <ta e="T72" id="Seg_731" s="T71">bar</ta>
            <ta e="T73" id="Seg_732" s="T72">ku-bi-jəʔ</ta>
            <ta e="T74" id="Seg_733" s="T73">dĭgəttə</ta>
            <ta e="T76" id="Seg_734" s="T75">ine</ta>
            <ta e="T77" id="Seg_735" s="T76">jakšə</ta>
            <ta e="T78" id="Seg_736" s="T77">a</ta>
            <ta e="T79" id="Seg_737" s="T78">xvostə</ta>
            <ta e="T80" id="Seg_738" s="T79">naga</ta>
            <ta e="T81" id="Seg_739" s="T80">a</ta>
            <ta e="T82" id="Seg_740" s="T81">dĭ</ta>
            <ta e="T83" id="Seg_741" s="T82">măn-ntə</ta>
            <ta e="T84" id="Seg_742" s="T83">xvostə</ta>
            <ta e="T85" id="Seg_743" s="T84">băra-Kən</ta>
            <ta e="T86" id="Seg_744" s="T85">iʔbö-laʔbə</ta>
            <ta e="T87" id="Seg_745" s="T86">i</ta>
            <ta e="T88" id="Seg_746" s="T87">i-gA</ta>
            <ta e="T89" id="Seg_747" s="T88">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_748" s="T0">one.[NOM.SG]</ta>
            <ta e="T2" id="Seg_749" s="T1">man.[NOM.SG]</ta>
            <ta e="T3" id="Seg_750" s="T2">and</ta>
            <ta e="T4" id="Seg_751" s="T3">woman.[NOM.SG]</ta>
            <ta e="T5" id="Seg_752" s="T4">be-PST.[3SG]</ta>
            <ta e="T6" id="Seg_753" s="T5">money-NOM/GEN.3SG</ta>
            <ta e="T7" id="Seg_754" s="T6">NEG.EX.[3SG]</ta>
            <ta e="T8" id="Seg_755" s="T7">one.should</ta>
            <ta e="T9" id="Seg_756" s="T8">horse.[NOM.SG]</ta>
            <ta e="T11" id="Seg_757" s="T10">sell-INF.LAT</ta>
            <ta e="T12" id="Seg_758" s="T11">then</ta>
            <ta e="T13" id="Seg_759" s="T12">wash-PST.[3SG]</ta>
            <ta e="T14" id="Seg_760" s="T13">this.[NOM.SG]</ta>
            <ta e="T15" id="Seg_761" s="T14">horse-ACC</ta>
            <ta e="T16" id="Seg_762" s="T15">soap-INS</ta>
            <ta e="T17" id="Seg_763" s="T16">morning-LOC.ADV</ta>
            <ta e="T18" id="Seg_764" s="T17">get.up-PST.[3SG]</ta>
            <ta e="T19" id="Seg_765" s="T18">all</ta>
            <ta e="T20" id="Seg_766" s="T19">dirty.[NOM.SG]</ta>
            <ta e="T22" id="Seg_767" s="T21">rain.[NOM.SG]</ta>
            <ta e="T23" id="Seg_768" s="T22">come-PRS.[3SG]</ta>
            <ta e="T24" id="Seg_769" s="T23">how</ta>
            <ta e="T25" id="Seg_770" s="T24">bring-INF.LAT</ta>
            <ta e="T26" id="Seg_771" s="T25">horse-ACC</ta>
            <ta e="T27" id="Seg_772" s="T26">market-LAT</ta>
            <ta e="T28" id="Seg_773" s="T27">go-PST.[3SG]</ta>
            <ta e="T29" id="Seg_774" s="T28">man-LAT</ta>
            <ta e="T30" id="Seg_775" s="T29">say-IPFVZ.[3SG]</ta>
            <ta e="T31" id="Seg_776" s="T30">horse-NOM/GEN/ACC.1SG</ta>
            <ta e="T32" id="Seg_777" s="T31">very</ta>
            <ta e="T33" id="Seg_778" s="T32">dirty.[NOM.SG]</ta>
            <ta e="T34" id="Seg_779" s="T33">horse-LOC</ta>
            <ta e="T35" id="Seg_780" s="T34">all</ta>
            <ta e="T36" id="Seg_781" s="T35">tail.[NOM.SG]</ta>
            <ta e="T37" id="Seg_782" s="T36">dirty-LAT</ta>
            <ta e="T38" id="Seg_783" s="T37">become-FUT-3SG</ta>
            <ta e="T39" id="Seg_784" s="T38">what.[NOM.SG]</ta>
            <ta e="T40" id="Seg_785" s="T39">NEG</ta>
            <ta e="T41" id="Seg_786" s="T40">know-PRS-2SG</ta>
            <ta e="T42" id="Seg_787" s="T41">what.[NOM.SG]</ta>
            <ta e="T43" id="Seg_788" s="T42">make-INF.LAT</ta>
            <ta e="T44" id="Seg_789" s="T43">off-cut-IMP.2SG.O</ta>
            <ta e="T45" id="Seg_790" s="T44">and</ta>
            <ta e="T46" id="Seg_791" s="T45">sack-LAT</ta>
            <ta e="T47" id="Seg_792" s="T46">put-EP-IMP.2SG</ta>
            <ta e="T48" id="Seg_793" s="T47">and</ta>
            <ta e="T49" id="Seg_794" s="T48">go-EP-IMP.2SG</ta>
            <ta e="T50" id="Seg_795" s="T49">then</ta>
            <ta e="T51" id="Seg_796" s="T50">walk-PRS.[3SG]</ta>
            <ta e="T52" id="Seg_797" s="T51">tent-LAT/LOC.3SG</ta>
            <ta e="T53" id="Seg_798" s="T52">this.[NOM.SG]</ta>
            <ta e="T54" id="Seg_799" s="T53">scold-DES-DUR.[3SG]</ta>
            <ta e="T55" id="Seg_800" s="T54">then</ta>
            <ta e="T56" id="Seg_801" s="T55">tail.[NOM.SG]</ta>
            <ta e="T57" id="Seg_802" s="T56">off-cut-PST.[3SG]</ta>
            <ta e="T58" id="Seg_803" s="T57">sack-LAT</ta>
            <ta e="T59" id="Seg_804" s="T58">put-PST.[3SG]</ta>
            <ta e="T60" id="Seg_805" s="T59">go-PST.[3SG]</ta>
            <ta e="T61" id="Seg_806" s="T60">market-LAT</ta>
            <ta e="T62" id="Seg_807" s="T61">this.[NOM.SG]</ta>
            <ta e="T63" id="Seg_808" s="T62">there</ta>
            <ta e="T64" id="Seg_809" s="T63">people.[NOM.SG]</ta>
            <ta e="T65" id="Seg_810" s="T64">come-PST-3PL</ta>
            <ta e="T66" id="Seg_811" s="T65">this.[NOM.SG]</ta>
            <ta e="T67" id="Seg_812" s="T66">horse-ACC</ta>
            <ta e="T68" id="Seg_813" s="T67">look-FRQ-PST-3PL</ta>
            <ta e="T69" id="Seg_814" s="T68">sit-PST-3PL</ta>
            <ta e="T70" id="Seg_815" s="T69">this-LAT</ta>
            <ta e="T71" id="Seg_816" s="T70">foot-NOM/GEN.3SG</ta>
            <ta e="T72" id="Seg_817" s="T71">all</ta>
            <ta e="T73" id="Seg_818" s="T72">see-PST-3PL</ta>
            <ta e="T74" id="Seg_819" s="T73">then</ta>
            <ta e="T76" id="Seg_820" s="T75">horse.[NOM.SG]</ta>
            <ta e="T77" id="Seg_821" s="T76">good.[NOM.SG]</ta>
            <ta e="T78" id="Seg_822" s="T77">and</ta>
            <ta e="T79" id="Seg_823" s="T78">tail.[NOM.SG]</ta>
            <ta e="T80" id="Seg_824" s="T79">NEG.EX.[3SG]</ta>
            <ta e="T81" id="Seg_825" s="T80">and</ta>
            <ta e="T82" id="Seg_826" s="T81">this.[NOM.SG]</ta>
            <ta e="T83" id="Seg_827" s="T82">say-IPFVZ.[3SG]</ta>
            <ta e="T84" id="Seg_828" s="T83">tail.[NOM.SG]</ta>
            <ta e="T85" id="Seg_829" s="T84">sack-LOC</ta>
            <ta e="T86" id="Seg_830" s="T85">lie-DUR.[3SG]</ta>
            <ta e="T87" id="Seg_831" s="T86">and</ta>
            <ta e="T88" id="Seg_832" s="T87">be-PRS.[3SG]</ta>
            <ta e="T89" id="Seg_833" s="T88">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_834" s="T0">один.[NOM.SG]</ta>
            <ta e="T2" id="Seg_835" s="T1">мужчина.[NOM.SG]</ta>
            <ta e="T3" id="Seg_836" s="T2">и</ta>
            <ta e="T4" id="Seg_837" s="T3">женщина.[NOM.SG]</ta>
            <ta e="T5" id="Seg_838" s="T4">быть-PST.[3SG]</ta>
            <ta e="T6" id="Seg_839" s="T5">деньги-NOM/GEN.3SG</ta>
            <ta e="T7" id="Seg_840" s="T6">NEG.EX.[3SG]</ta>
            <ta e="T8" id="Seg_841" s="T7">надо</ta>
            <ta e="T9" id="Seg_842" s="T8">лошадь.[NOM.SG]</ta>
            <ta e="T11" id="Seg_843" s="T10">продавать-INF.LAT</ta>
            <ta e="T12" id="Seg_844" s="T11">тогда</ta>
            <ta e="T13" id="Seg_845" s="T12">мыть-PST.[3SG]</ta>
            <ta e="T14" id="Seg_846" s="T13">этот.[NOM.SG]</ta>
            <ta e="T15" id="Seg_847" s="T14">лошадь-ACC</ta>
            <ta e="T16" id="Seg_848" s="T15">мыло-INS</ta>
            <ta e="T17" id="Seg_849" s="T16">утро-LOC.ADV</ta>
            <ta e="T18" id="Seg_850" s="T17">встать-PST.[3SG]</ta>
            <ta e="T19" id="Seg_851" s="T18">весь</ta>
            <ta e="T20" id="Seg_852" s="T19">грязный.[NOM.SG]</ta>
            <ta e="T22" id="Seg_853" s="T21">дождь.[NOM.SG]</ta>
            <ta e="T23" id="Seg_854" s="T22">прийти-PRS.[3SG]</ta>
            <ta e="T24" id="Seg_855" s="T23">как</ta>
            <ta e="T25" id="Seg_856" s="T24">нести-INF.LAT</ta>
            <ta e="T26" id="Seg_857" s="T25">лошадь-ACC</ta>
            <ta e="T27" id="Seg_858" s="T26">базар-LAT</ta>
            <ta e="T28" id="Seg_859" s="T27">пойти-PST.[3SG]</ta>
            <ta e="T29" id="Seg_860" s="T28">мужчина-LAT</ta>
            <ta e="T30" id="Seg_861" s="T29">сказать-IPFVZ.[3SG]</ta>
            <ta e="T31" id="Seg_862" s="T30">лошадь-NOM/GEN/ACC.1SG</ta>
            <ta e="T32" id="Seg_863" s="T31">очень</ta>
            <ta e="T33" id="Seg_864" s="T32">грязный.[NOM.SG]</ta>
            <ta e="T34" id="Seg_865" s="T33">лошадь-LOC</ta>
            <ta e="T35" id="Seg_866" s="T34">весь</ta>
            <ta e="T36" id="Seg_867" s="T35">хвост.[NOM.SG]</ta>
            <ta e="T37" id="Seg_868" s="T36">грязный-LAT</ta>
            <ta e="T38" id="Seg_869" s="T37">стать-FUT-3SG</ta>
            <ta e="T39" id="Seg_870" s="T38">что.[NOM.SG]</ta>
            <ta e="T40" id="Seg_871" s="T39">NEG</ta>
            <ta e="T41" id="Seg_872" s="T40">знать-PRS-2SG</ta>
            <ta e="T42" id="Seg_873" s="T41">что.[NOM.SG]</ta>
            <ta e="T43" id="Seg_874" s="T42">делать-INF.LAT</ta>
            <ta e="T44" id="Seg_875" s="T43">от-резать-IMP.2SG.O</ta>
            <ta e="T45" id="Seg_876" s="T44">и</ta>
            <ta e="T46" id="Seg_877" s="T45">мешок-LAT</ta>
            <ta e="T47" id="Seg_878" s="T46">класть-EP-IMP.2SG</ta>
            <ta e="T48" id="Seg_879" s="T47">и</ta>
            <ta e="T49" id="Seg_880" s="T48">пойти-EP-IMP.2SG</ta>
            <ta e="T50" id="Seg_881" s="T49">тогда</ta>
            <ta e="T51" id="Seg_882" s="T50">идти-PRS.[3SG]</ta>
            <ta e="T52" id="Seg_883" s="T51">чум-LAT/LOC.3SG</ta>
            <ta e="T53" id="Seg_884" s="T52">этот.[NOM.SG]</ta>
            <ta e="T54" id="Seg_885" s="T53">ругать-DES-DUR.[3SG]</ta>
            <ta e="T55" id="Seg_886" s="T54">тогда</ta>
            <ta e="T56" id="Seg_887" s="T55">хвост.[NOM.SG]</ta>
            <ta e="T57" id="Seg_888" s="T56">от-резать-PST.[3SG]</ta>
            <ta e="T58" id="Seg_889" s="T57">мешок-LAT</ta>
            <ta e="T59" id="Seg_890" s="T58">класть-PST.[3SG]</ta>
            <ta e="T60" id="Seg_891" s="T59">пойти-PST.[3SG]</ta>
            <ta e="T61" id="Seg_892" s="T60">базар-LAT</ta>
            <ta e="T62" id="Seg_893" s="T61">этот.[NOM.SG]</ta>
            <ta e="T63" id="Seg_894" s="T62">там</ta>
            <ta e="T64" id="Seg_895" s="T63">люди.[NOM.SG]</ta>
            <ta e="T65" id="Seg_896" s="T64">прийти-PST-3PL</ta>
            <ta e="T66" id="Seg_897" s="T65">этот.[NOM.SG]</ta>
            <ta e="T67" id="Seg_898" s="T66">лошадь-ACC</ta>
            <ta e="T68" id="Seg_899" s="T67">смотреть-FRQ-PST-3PL</ta>
            <ta e="T69" id="Seg_900" s="T68">сидеть-PST-3PL</ta>
            <ta e="T70" id="Seg_901" s="T69">этот-LAT</ta>
            <ta e="T71" id="Seg_902" s="T70">нога-NOM/GEN.3SG</ta>
            <ta e="T72" id="Seg_903" s="T71">весь</ta>
            <ta e="T73" id="Seg_904" s="T72">видеть-PST-3PL</ta>
            <ta e="T74" id="Seg_905" s="T73">тогда</ta>
            <ta e="T76" id="Seg_906" s="T75">лошадь.[NOM.SG]</ta>
            <ta e="T77" id="Seg_907" s="T76">хороший.[NOM.SG]</ta>
            <ta e="T78" id="Seg_908" s="T77">а</ta>
            <ta e="T79" id="Seg_909" s="T78">хвост.[NOM.SG]</ta>
            <ta e="T80" id="Seg_910" s="T79">NEG.EX.[3SG]</ta>
            <ta e="T81" id="Seg_911" s="T80">а</ta>
            <ta e="T82" id="Seg_912" s="T81">этот.[NOM.SG]</ta>
            <ta e="T83" id="Seg_913" s="T82">сказать-IPFVZ.[3SG]</ta>
            <ta e="T84" id="Seg_914" s="T83">хвост.[NOM.SG]</ta>
            <ta e="T85" id="Seg_915" s="T84">мешок-LOC</ta>
            <ta e="T86" id="Seg_916" s="T85">лежать-DUR.[3SG]</ta>
            <ta e="T87" id="Seg_917" s="T86">и</ta>
            <ta e="T88" id="Seg_918" s="T87">быть-PRS.[3SG]</ta>
            <ta e="T89" id="Seg_919" s="T88">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_920" s="T0">num-n:case</ta>
            <ta e="T2" id="Seg_921" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_922" s="T2">conj</ta>
            <ta e="T4" id="Seg_923" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_924" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_925" s="T5">n-n:case.poss</ta>
            <ta e="T7" id="Seg_926" s="T6">v-v:pn</ta>
            <ta e="T8" id="Seg_927" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_928" s="T8">n-n:case</ta>
            <ta e="T11" id="Seg_929" s="T10">v-v:n.fin</ta>
            <ta e="T12" id="Seg_930" s="T11">adv</ta>
            <ta e="T13" id="Seg_931" s="T12">v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_932" s="T13">dempro-n:case</ta>
            <ta e="T15" id="Seg_933" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_934" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_935" s="T16">n-adv:case</ta>
            <ta e="T18" id="Seg_936" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_937" s="T18">quant</ta>
            <ta e="T20" id="Seg_938" s="T19">adj-n:case</ta>
            <ta e="T22" id="Seg_939" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_940" s="T22">v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_941" s="T23">que</ta>
            <ta e="T25" id="Seg_942" s="T24">v-v:n.fin</ta>
            <ta e="T26" id="Seg_943" s="T25">n-n:case</ta>
            <ta e="T27" id="Seg_944" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_945" s="T27">v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_946" s="T28">n-n:case</ta>
            <ta e="T30" id="Seg_947" s="T29">v-v&gt;v-v:pn</ta>
            <ta e="T31" id="Seg_948" s="T30">n-n:case.poss</ta>
            <ta e="T32" id="Seg_949" s="T31">adv</ta>
            <ta e="T33" id="Seg_950" s="T32">adj-n:case</ta>
            <ta e="T34" id="Seg_951" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_952" s="T34">quant</ta>
            <ta e="T36" id="Seg_953" s="T35">n-n:case</ta>
            <ta e="T37" id="Seg_954" s="T36">adj-n:case</ta>
            <ta e="T38" id="Seg_955" s="T37">v-v:tense-v:pn</ta>
            <ta e="T39" id="Seg_956" s="T38">que-n:case</ta>
            <ta e="T40" id="Seg_957" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_958" s="T40">v-v:tense-v:pn</ta>
            <ta e="T42" id="Seg_959" s="T41">que-n:case</ta>
            <ta e="T43" id="Seg_960" s="T42">v-v:n.fin</ta>
            <ta e="T44" id="Seg_961" s="T43">v&gt;v-v-v:mood.pn</ta>
            <ta e="T45" id="Seg_962" s="T44">conj</ta>
            <ta e="T46" id="Seg_963" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_964" s="T46">v-v:ins-v:mood.pn</ta>
            <ta e="T48" id="Seg_965" s="T47">conj</ta>
            <ta e="T49" id="Seg_966" s="T48">v-v:ins-v:mood.pn</ta>
            <ta e="T50" id="Seg_967" s="T49">adv</ta>
            <ta e="T51" id="Seg_968" s="T50">v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_969" s="T51">n-n:case.poss</ta>
            <ta e="T53" id="Seg_970" s="T52">dempro-n:case</ta>
            <ta e="T54" id="Seg_971" s="T53">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T55" id="Seg_972" s="T54">adv</ta>
            <ta e="T56" id="Seg_973" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_974" s="T56">v&gt;v-v-v:tense-v:pn</ta>
            <ta e="T58" id="Seg_975" s="T57">n-n:case</ta>
            <ta e="T59" id="Seg_976" s="T58">v-v:tense-v:pn</ta>
            <ta e="T60" id="Seg_977" s="T59">v-v:tense-v:pn</ta>
            <ta e="T61" id="Seg_978" s="T60">n-n:case</ta>
            <ta e="T62" id="Seg_979" s="T61">dempro-n:case</ta>
            <ta e="T63" id="Seg_980" s="T62">adv</ta>
            <ta e="T64" id="Seg_981" s="T63">n-n:case</ta>
            <ta e="T65" id="Seg_982" s="T64">v-v:tense-v:pn</ta>
            <ta e="T66" id="Seg_983" s="T65">dempro-n:case</ta>
            <ta e="T67" id="Seg_984" s="T66">n-n:case</ta>
            <ta e="T68" id="Seg_985" s="T67">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T69" id="Seg_986" s="T68">v-v:tense-v:pn</ta>
            <ta e="T70" id="Seg_987" s="T69">dempro-n:case</ta>
            <ta e="T71" id="Seg_988" s="T70">n-n:case.poss</ta>
            <ta e="T72" id="Seg_989" s="T71">quant</ta>
            <ta e="T73" id="Seg_990" s="T72">v-v:tense-v:pn</ta>
            <ta e="T74" id="Seg_991" s="T73">adv</ta>
            <ta e="T76" id="Seg_992" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_993" s="T76">adj-n:case</ta>
            <ta e="T78" id="Seg_994" s="T77">conj</ta>
            <ta e="T79" id="Seg_995" s="T78">n-n:case</ta>
            <ta e="T80" id="Seg_996" s="T79">v</ta>
            <ta e="T81" id="Seg_997" s="T80">conj</ta>
            <ta e="T82" id="Seg_998" s="T81">dempro-n:case</ta>
            <ta e="T83" id="Seg_999" s="T82">v-v&gt;v-v:pn</ta>
            <ta e="T84" id="Seg_1000" s="T83">n-n:case</ta>
            <ta e="T85" id="Seg_1001" s="T84">n-n:case</ta>
            <ta e="T86" id="Seg_1002" s="T85">v-v&gt;v-v:pn</ta>
            <ta e="T87" id="Seg_1003" s="T86">conj</ta>
            <ta e="T88" id="Seg_1004" s="T87">v-v:tense-v:pn</ta>
            <ta e="T89" id="Seg_1005" s="T88">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1006" s="T0">num</ta>
            <ta e="T2" id="Seg_1007" s="T1">n</ta>
            <ta e="T3" id="Seg_1008" s="T2">conj</ta>
            <ta e="T4" id="Seg_1009" s="T3">n</ta>
            <ta e="T5" id="Seg_1010" s="T4">v</ta>
            <ta e="T6" id="Seg_1011" s="T5">n</ta>
            <ta e="T7" id="Seg_1012" s="T6">v</ta>
            <ta e="T8" id="Seg_1013" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_1014" s="T8">n</ta>
            <ta e="T11" id="Seg_1015" s="T10">v</ta>
            <ta e="T12" id="Seg_1016" s="T11">adv</ta>
            <ta e="T13" id="Seg_1017" s="T12">v</ta>
            <ta e="T14" id="Seg_1018" s="T13">dempro</ta>
            <ta e="T15" id="Seg_1019" s="T14">n</ta>
            <ta e="T16" id="Seg_1020" s="T15">n</ta>
            <ta e="T17" id="Seg_1021" s="T16">adv</ta>
            <ta e="T18" id="Seg_1022" s="T17">v</ta>
            <ta e="T19" id="Seg_1023" s="T18">quant</ta>
            <ta e="T20" id="Seg_1024" s="T19">adj</ta>
            <ta e="T22" id="Seg_1025" s="T21">n</ta>
            <ta e="T23" id="Seg_1026" s="T22">v</ta>
            <ta e="T24" id="Seg_1027" s="T23">que</ta>
            <ta e="T25" id="Seg_1028" s="T24">v</ta>
            <ta e="T26" id="Seg_1029" s="T25">n</ta>
            <ta e="T27" id="Seg_1030" s="T26">n</ta>
            <ta e="T28" id="Seg_1031" s="T27">v</ta>
            <ta e="T29" id="Seg_1032" s="T28">n</ta>
            <ta e="T30" id="Seg_1033" s="T29">v</ta>
            <ta e="T31" id="Seg_1034" s="T30">n</ta>
            <ta e="T32" id="Seg_1035" s="T31">adv</ta>
            <ta e="T33" id="Seg_1036" s="T32">adj</ta>
            <ta e="T34" id="Seg_1037" s="T33">n</ta>
            <ta e="T35" id="Seg_1038" s="T34">quant</ta>
            <ta e="T36" id="Seg_1039" s="T35">n</ta>
            <ta e="T37" id="Seg_1040" s="T36">adj</ta>
            <ta e="T38" id="Seg_1041" s="T37">v</ta>
            <ta e="T39" id="Seg_1042" s="T38">que</ta>
            <ta e="T40" id="Seg_1043" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_1044" s="T40">v</ta>
            <ta e="T42" id="Seg_1045" s="T41">que</ta>
            <ta e="T43" id="Seg_1046" s="T42">v</ta>
            <ta e="T44" id="Seg_1047" s="T43">v</ta>
            <ta e="T45" id="Seg_1048" s="T44">conj</ta>
            <ta e="T46" id="Seg_1049" s="T45">n</ta>
            <ta e="T47" id="Seg_1050" s="T46">v</ta>
            <ta e="T48" id="Seg_1051" s="T47">conj</ta>
            <ta e="T49" id="Seg_1052" s="T48">v</ta>
            <ta e="T50" id="Seg_1053" s="T49">adv</ta>
            <ta e="T51" id="Seg_1054" s="T50">v</ta>
            <ta e="T52" id="Seg_1055" s="T51">n</ta>
            <ta e="T53" id="Seg_1056" s="T52">dempro</ta>
            <ta e="T54" id="Seg_1057" s="T53">v</ta>
            <ta e="T55" id="Seg_1058" s="T54">adv</ta>
            <ta e="T56" id="Seg_1059" s="T55">n</ta>
            <ta e="T57" id="Seg_1060" s="T56">v</ta>
            <ta e="T58" id="Seg_1061" s="T57">n</ta>
            <ta e="T59" id="Seg_1062" s="T58">v</ta>
            <ta e="T60" id="Seg_1063" s="T59">v</ta>
            <ta e="T61" id="Seg_1064" s="T60">n</ta>
            <ta e="T62" id="Seg_1065" s="T61">dempro</ta>
            <ta e="T63" id="Seg_1066" s="T62">adv</ta>
            <ta e="T64" id="Seg_1067" s="T63">n</ta>
            <ta e="T65" id="Seg_1068" s="T64">v</ta>
            <ta e="T66" id="Seg_1069" s="T65">dempro</ta>
            <ta e="T67" id="Seg_1070" s="T66">n</ta>
            <ta e="T68" id="Seg_1071" s="T67">v</ta>
            <ta e="T69" id="Seg_1072" s="T68">v</ta>
            <ta e="T70" id="Seg_1073" s="T69">dempro</ta>
            <ta e="T71" id="Seg_1074" s="T70">n</ta>
            <ta e="T72" id="Seg_1075" s="T71">quant</ta>
            <ta e="T73" id="Seg_1076" s="T72">v</ta>
            <ta e="T74" id="Seg_1077" s="T73">adv</ta>
            <ta e="T76" id="Seg_1078" s="T75">n</ta>
            <ta e="T77" id="Seg_1079" s="T76">adj</ta>
            <ta e="T78" id="Seg_1080" s="T77">conj</ta>
            <ta e="T79" id="Seg_1081" s="T78">n</ta>
            <ta e="T80" id="Seg_1082" s="T79">v</ta>
            <ta e="T81" id="Seg_1083" s="T80">conj</ta>
            <ta e="T82" id="Seg_1084" s="T81">dempro</ta>
            <ta e="T83" id="Seg_1085" s="T82">v</ta>
            <ta e="T84" id="Seg_1086" s="T83">n</ta>
            <ta e="T85" id="Seg_1087" s="T84">n</ta>
            <ta e="T86" id="Seg_1088" s="T85">v</ta>
            <ta e="T87" id="Seg_1089" s="T86">conj</ta>
            <ta e="T88" id="Seg_1090" s="T87">v</ta>
            <ta e="T89" id="Seg_1091" s="T88">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_1092" s="T1">np.h:E</ta>
            <ta e="T4" id="Seg_1093" s="T3">np.h:E</ta>
            <ta e="T6" id="Seg_1094" s="T5">np:Th</ta>
            <ta e="T9" id="Seg_1095" s="T8">np:Th</ta>
            <ta e="T12" id="Seg_1096" s="T11">adv:Time</ta>
            <ta e="T14" id="Seg_1097" s="T13">pro.h:A</ta>
            <ta e="T15" id="Seg_1098" s="T14">np:Th</ta>
            <ta e="T16" id="Seg_1099" s="T15">np:Ins</ta>
            <ta e="T17" id="Seg_1100" s="T16">n:Time</ta>
            <ta e="T18" id="Seg_1101" s="T17">0.3.h:A</ta>
            <ta e="T19" id="Seg_1102" s="T18">np:Th</ta>
            <ta e="T22" id="Seg_1103" s="T21">np:Th</ta>
            <ta e="T26" id="Seg_1104" s="T25">np:Th</ta>
            <ta e="T27" id="Seg_1105" s="T26">np:G</ta>
            <ta e="T28" id="Seg_1106" s="T27">0.3.h:A</ta>
            <ta e="T29" id="Seg_1107" s="T28">np:G</ta>
            <ta e="T30" id="Seg_1108" s="T29">0.3.h:A</ta>
            <ta e="T31" id="Seg_1109" s="T30">np:Th</ta>
            <ta e="T34" id="Seg_1110" s="T33">np:Poss</ta>
            <ta e="T36" id="Seg_1111" s="T35">np:Th</ta>
            <ta e="T41" id="Seg_1112" s="T40">0.2.h:E</ta>
            <ta e="T44" id="Seg_1113" s="T43">0.2.h:A</ta>
            <ta e="T46" id="Seg_1114" s="T45">np:G</ta>
            <ta e="T47" id="Seg_1115" s="T46">0.2.h:A</ta>
            <ta e="T49" id="Seg_1116" s="T48">0.2.h:A</ta>
            <ta e="T50" id="Seg_1117" s="T49">adv:Time</ta>
            <ta e="T51" id="Seg_1118" s="T50">0.3.h:A</ta>
            <ta e="T52" id="Seg_1119" s="T51">np:G</ta>
            <ta e="T53" id="Seg_1120" s="T52">pro.h:A</ta>
            <ta e="T55" id="Seg_1121" s="T54">adv:Time</ta>
            <ta e="T56" id="Seg_1122" s="T55">np:P</ta>
            <ta e="T57" id="Seg_1123" s="T56">0.3.h:A</ta>
            <ta e="T58" id="Seg_1124" s="T57">np:G</ta>
            <ta e="T59" id="Seg_1125" s="T58">0.3.h:A</ta>
            <ta e="T60" id="Seg_1126" s="T59">0.3.h:A</ta>
            <ta e="T61" id="Seg_1127" s="T60">np:G</ta>
            <ta e="T63" id="Seg_1128" s="T62">adv:L</ta>
            <ta e="T64" id="Seg_1129" s="T63">np.h:A</ta>
            <ta e="T66" id="Seg_1130" s="T65">pro.h:A</ta>
            <ta e="T67" id="Seg_1131" s="T66">np:Th</ta>
            <ta e="T69" id="Seg_1132" s="T68">0.3.h:A</ta>
            <ta e="T70" id="Seg_1133" s="T69">pro:L</ta>
            <ta e="T71" id="Seg_1134" s="T70">np:Th</ta>
            <ta e="T73" id="Seg_1135" s="T72">0.3.h:A</ta>
            <ta e="T74" id="Seg_1136" s="T73">adv:Time</ta>
            <ta e="T76" id="Seg_1137" s="T75">np:Th</ta>
            <ta e="T79" id="Seg_1138" s="T78">np:Th</ta>
            <ta e="T82" id="Seg_1139" s="T81">pro.h:A</ta>
            <ta e="T84" id="Seg_1140" s="T83">np:Th</ta>
            <ta e="T85" id="Seg_1141" s="T84">np:L</ta>
            <ta e="T88" id="Seg_1142" s="T87">0.3:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_1143" s="T1">np.h:S</ta>
            <ta e="T4" id="Seg_1144" s="T3">np.h:S</ta>
            <ta e="T5" id="Seg_1145" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_1146" s="T5">np:S</ta>
            <ta e="T7" id="Seg_1147" s="T6">v:pred</ta>
            <ta e="T8" id="Seg_1148" s="T7">ptcl:pred</ta>
            <ta e="T9" id="Seg_1149" s="T8">np:O</ta>
            <ta e="T13" id="Seg_1150" s="T12">v:pred</ta>
            <ta e="T14" id="Seg_1151" s="T13">pro.h:S</ta>
            <ta e="T15" id="Seg_1152" s="T14">np:O</ta>
            <ta e="T18" id="Seg_1153" s="T17">v:pred 0.3.h:S</ta>
            <ta e="T19" id="Seg_1154" s="T18">np:S</ta>
            <ta e="T20" id="Seg_1155" s="T19">adj:pred</ta>
            <ta e="T22" id="Seg_1156" s="T21">np:S</ta>
            <ta e="T23" id="Seg_1157" s="T22">v:pred</ta>
            <ta e="T26" id="Seg_1158" s="T25">np:O</ta>
            <ta e="T28" id="Seg_1159" s="T27">v:pred 0.3.h:S</ta>
            <ta e="T30" id="Seg_1160" s="T29">v:pred 0.3.h:S</ta>
            <ta e="T31" id="Seg_1161" s="T30">np:S</ta>
            <ta e="T33" id="Seg_1162" s="T32">adj:pred</ta>
            <ta e="T36" id="Seg_1163" s="T35">np:S</ta>
            <ta e="T37" id="Seg_1164" s="T36">adj:pred</ta>
            <ta e="T38" id="Seg_1165" s="T37">cop</ta>
            <ta e="T40" id="Seg_1166" s="T39">ptcl.neg</ta>
            <ta e="T41" id="Seg_1167" s="T40">v:pred 0.2.h:S</ta>
            <ta e="T43" id="Seg_1168" s="T42">v:pred</ta>
            <ta e="T44" id="Seg_1169" s="T43">v:pred 0.2.h:S</ta>
            <ta e="T47" id="Seg_1170" s="T46">v:pred 0.2.h:S</ta>
            <ta e="T49" id="Seg_1171" s="T48">v:pred 0.2.h:S</ta>
            <ta e="T51" id="Seg_1172" s="T50">v:pred 0.3.h:S</ta>
            <ta e="T53" id="Seg_1173" s="T52">pro.h:S</ta>
            <ta e="T54" id="Seg_1174" s="T53">v:pred</ta>
            <ta e="T56" id="Seg_1175" s="T55">np:O</ta>
            <ta e="T57" id="Seg_1176" s="T56">v:pred 0.3.h:S</ta>
            <ta e="T59" id="Seg_1177" s="T58">v:pred 0.3.h:S</ta>
            <ta e="T60" id="Seg_1178" s="T59">v:pred 0.3.h:S</ta>
            <ta e="T64" id="Seg_1179" s="T63">np.h:S</ta>
            <ta e="T65" id="Seg_1180" s="T64">v:pred</ta>
            <ta e="T66" id="Seg_1181" s="T65">pro.h:S</ta>
            <ta e="T67" id="Seg_1182" s="T66">np:O</ta>
            <ta e="T68" id="Seg_1183" s="T67">v:pred</ta>
            <ta e="T69" id="Seg_1184" s="T68">v:pred 0.3.h:S</ta>
            <ta e="T71" id="Seg_1185" s="T70">np:O</ta>
            <ta e="T73" id="Seg_1186" s="T72">v:pred 0.3.h:S</ta>
            <ta e="T76" id="Seg_1187" s="T75">np:S</ta>
            <ta e="T77" id="Seg_1188" s="T76">adj:pred</ta>
            <ta e="T79" id="Seg_1189" s="T78">np:S</ta>
            <ta e="T80" id="Seg_1190" s="T79">v:pred</ta>
            <ta e="T82" id="Seg_1191" s="T81">pro.h:S</ta>
            <ta e="T83" id="Seg_1192" s="T82">v:pred</ta>
            <ta e="T84" id="Seg_1193" s="T83">np:S</ta>
            <ta e="T86" id="Seg_1194" s="T85">v:pred</ta>
            <ta e="T88" id="Seg_1195" s="T87">v:pred 0.3:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T3" id="Seg_1196" s="T2">RUS:gram</ta>
            <ta e="T8" id="Seg_1197" s="T7">RUS:mod</ta>
            <ta e="T11" id="Seg_1198" s="T10">TURK:cult</ta>
            <ta e="T19" id="Seg_1199" s="T18">TURK:disc</ta>
            <ta e="T27" id="Seg_1200" s="T26">RUS:cult</ta>
            <ta e="T35" id="Seg_1201" s="T34">TURK:disc</ta>
            <ta e="T36" id="Seg_1202" s="T35">RUS:core</ta>
            <ta e="T45" id="Seg_1203" s="T44">RUS:gram</ta>
            <ta e="T48" id="Seg_1204" s="T47">RUS:gram</ta>
            <ta e="T56" id="Seg_1205" s="T55">RUS:core</ta>
            <ta e="T61" id="Seg_1206" s="T60">RUS:cult</ta>
            <ta e="T72" id="Seg_1207" s="T71">TURK:disc</ta>
            <ta e="T77" id="Seg_1208" s="T76">TURK:core</ta>
            <ta e="T78" id="Seg_1209" s="T77">RUS:gram</ta>
            <ta e="T79" id="Seg_1210" s="T78">RUS:core</ta>
            <ta e="T81" id="Seg_1211" s="T80">RUS:gram</ta>
            <ta e="T84" id="Seg_1212" s="T83">RUS:core</ta>
            <ta e="T87" id="Seg_1213" s="T86">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T84" id="Seg_1214" s="T83">medCdel finVdel</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T79" id="Seg_1215" s="T78">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_1216" s="T0">Жили муж и жена.</ta>
            <ta e="T11" id="Seg_1217" s="T5">Денег у них не было, пришлось им лошадь продавать.</ta>
            <ta e="T16" id="Seg_1218" s="T11">Тогда они вымыли эту лошадь мылом.</ta>
            <ta e="T23" id="Seg_1219" s="T16">Утром встали, всё грязное, дождь идёт.</ta>
            <ta e="T27" id="Seg_1220" s="T23">Как лошадь на базар вести?</ta>
            <ta e="T30" id="Seg_1221" s="T27">[Муж] пошёл к одному человеку, говорит:</ta>
            <ta e="T33" id="Seg_1222" s="T30">«Моя лошадь очень грязная.</ta>
            <ta e="T38" id="Seg_1223" s="T33">У лошади весь хвост грязным будет. [?]</ta>
            <ta e="T43" id="Seg_1224" s="T38">Ты не знаешь, что делать?»</ta>
            <ta e="T49" id="Seg_1225" s="T43">«Отрежь его, в мешок положи и иди».</ta>
            <ta e="T54" id="Seg_1226" s="T49">Потом он идёт домой, кричит.</ta>
            <ta e="T57" id="Seg_1227" s="T54">Потом отрезал хвост.</ta>
            <ta e="T61" id="Seg_1228" s="T57">Положил в мешок, пошёл на базар.</ta>
            <ta e="T65" id="Seg_1229" s="T61">Туда люди пришли.</ta>
            <ta e="T68" id="Seg_1230" s="T65">Они смотрят на лошадь.</ta>
            <ta e="T70" id="Seg_1231" s="T68">Садятся на неё.</ta>
            <ta e="T73" id="Seg_1232" s="T70">На ноги все смотрят.</ta>
            <ta e="T77" id="Seg_1233" s="T73">Потом [говорят]: «Лошадь хорошая.</ta>
            <ta e="T80" id="Seg_1234" s="T77">А хвоста нет».</ta>
            <ta e="T83" id="Seg_1235" s="T80">А он говорит:</ta>
            <ta e="T86" id="Seg_1236" s="T83">«Хвост в мешке лежит.</ta>
            <ta e="T88" id="Seg_1237" s="T86">Есть [хвост]».</ta>
            <ta e="T89" id="Seg_1238" s="T88">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_1239" s="T0">There were a man and a woman.</ta>
            <ta e="T11" id="Seg_1240" s="T5">They had no money, they had to sell their horse.</ta>
            <ta e="T16" id="Seg_1241" s="T11">Then he washed the horse with soap.</ta>
            <ta e="T23" id="Seg_1242" s="T16">They got up in the morning, everything is dirty, rain is coming [down].</ta>
            <ta e="T27" id="Seg_1243" s="T23">How to take the horse to the market?</ta>
            <ta e="T30" id="Seg_1244" s="T27">He went to a man, he said:</ta>
            <ta e="T33" id="Seg_1245" s="T30">"My horse is very dirty.</ta>
            <ta e="T38" id="Seg_1246" s="T33">The horse's tail will become dirty. [?]</ta>
            <ta e="T43" id="Seg_1247" s="T38">Don't you know something, what to do?"</ta>
            <ta e="T49" id="Seg_1248" s="T43">"Cut it off and put it in a sack and go."</ta>
            <ta e="T54" id="Seg_1249" s="T49">Then he goes home, he shouts.</ta>
            <ta e="T57" id="Seg_1250" s="T54">Then he cut off the tail.</ta>
            <ta e="T61" id="Seg_1251" s="T57">Put it into a sack, went to the market.</ta>
            <ta e="T65" id="Seg_1252" s="T61">People came there.</ta>
            <ta e="T68" id="Seg_1253" s="T65">They are looking at the horse.</ta>
            <ta e="T70" id="Seg_1254" s="T68">They sat on it.</ta>
            <ta e="T73" id="Seg_1255" s="T70">They are looking at its legs.</ta>
            <ta e="T77" id="Seg_1256" s="T73">Then [they say]: "The horse is good.</ta>
            <ta e="T80" id="Seg_1257" s="T77">But it has no tail."</ta>
            <ta e="T83" id="Seg_1258" s="T80">And he says:</ta>
            <ta e="T86" id="Seg_1259" s="T83">"The tail is lying in the sack.</ta>
            <ta e="T88" id="Seg_1260" s="T86">[There] is [a tail]."</ta>
            <ta e="T89" id="Seg_1261" s="T88">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_1262" s="T0">Es war ein Mann und eine Frau.</ta>
            <ta e="T11" id="Seg_1263" s="T5">Sie hatten kein Geld, sie mussten ihr Pferd verkaufen.</ta>
            <ta e="T16" id="Seg_1264" s="T11">Dann wusch er das Pferd mit Seife.</ta>
            <ta e="T23" id="Seg_1265" s="T16">Sie standen morgens auf, alles ist schmutzig, Regen kommt [herunter].</ta>
            <ta e="T27" id="Seg_1266" s="T23">Wie das Pferd zum Markt bringen?</ta>
            <ta e="T30" id="Seg_1267" s="T27">Er ging zu einem Mann, er sagte:</ta>
            <ta e="T33" id="Seg_1268" s="T30">„Mein Pferd ist sehr schmutzig.</ta>
            <ta e="T38" id="Seg_1269" s="T33">Dem Pferd sein Schwanz wird schmutzig werden. [?]</ta>
            <ta e="T43" id="Seg_1270" s="T38">Weißt du nicht etwas, was zu tun?“</ta>
            <ta e="T49" id="Seg_1271" s="T43">“Schneide ihn ab und stecke ihn in einen Sack und geh.“</ta>
            <ta e="T54" id="Seg_1272" s="T49">Dann geht er nach Hause, er ruft.</ta>
            <ta e="T57" id="Seg_1273" s="T54">Dann schnitt er den Schwanz ab.</ta>
            <ta e="T61" id="Seg_1274" s="T57">Steckte ihn in einem Sack, ging zum Markt.</ta>
            <ta e="T65" id="Seg_1275" s="T61">Leute kamen dort hin.</ta>
            <ta e="T68" id="Seg_1276" s="T65">Sie schauen sich das Pferd an.</ta>
            <ta e="T70" id="Seg_1277" s="T68">Sie saßen drauf.</ta>
            <ta e="T73" id="Seg_1278" s="T70">Sie schauen sich die Beine an.</ta>
            <ta e="T77" id="Seg_1279" s="T73">Dann [sagen sie]: „Das Pferd ist gut.</ta>
            <ta e="T80" id="Seg_1280" s="T77">Aber es hat keinen Schwanz.“</ta>
            <ta e="T83" id="Seg_1281" s="T80">Und er sagt:</ta>
            <ta e="T86" id="Seg_1282" s="T83">„Der Schwanz liegt im Sack.</ta>
            <ta e="T88" id="Seg_1283" s="T86">[Da] ist [ein Schwanz].“</ta>
            <ta e="T89" id="Seg_1284" s="T88">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T11" id="Seg_1285" s="T5">[KlT:] 3SG.POSS instead of 3PL.POSS. </ta>
            <ta e="T30" id="Seg_1286" s="T27">[KlT:] Unclear referents.</ta>
            <ta e="T33" id="Seg_1287" s="T30">[KlT:] Balgaš 'dirt' ('dirty' would be balgaššəbi).</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
