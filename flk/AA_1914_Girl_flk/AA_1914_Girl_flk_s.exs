<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID18962740-EC77-D06B-613F-7B0CC029846A">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\AA_1914_Girl_flk\AA_1914_Girl_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">336</ud-information>
            <ud-information attribute-name="# HIAT:w">231</ud-information>
            <ud-information attribute-name="# e">231</ud-information>
            <ud-information attribute-name="# HIAT:u">57</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="AA">
            <abbreviation>AA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T207" />
         <tli id="T208" />
         <tli id="T209" />
         <tli id="T210" />
         <tli id="T211" />
         <tli id="T212" />
         <tli id="T213" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T216" />
         <tli id="T217" />
         <tli id="T218" />
         <tli id="T219" />
         <tli id="T220" />
         <tli id="T221" />
         <tli id="T222" />
         <tli id="T223" />
         <tli id="T224" />
         <tli id="T225" />
         <tli id="T226" />
         <tli id="T227" />
         <tli id="T228" />
         <tli id="T229" />
         <tli id="T230" />
         <tli id="T231" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="AA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T231" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Büzʼen</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">nagur</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">koʔbdot</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">ibi</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">Dĭ</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">büzʼem</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">turanə</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">služit</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">mosʼtə</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_35" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">Urgo</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">koʔbdot:</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_42" n="HIAT:ip">"</nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">Măn</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">kallam</ts>
                  <nts id="Seg_48" n="HIAT:ip">.</nts>
                  <nts id="Seg_49" n="HIAT:ip">"</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_52" n="HIAT:u" s="T13">
                  <nts id="Seg_53" n="HIAT:ip">"</nts>
                  <ts e="T14" id="Seg_55" n="HIAT:w" s="T13">No</ts>
                  <nts id="Seg_56" n="HIAT:ip">,</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_59" n="HIAT:w" s="T14">kalla</ts>
                  <nts id="Seg_60" n="HIAT:ip">"</nts>
                  <nts id="Seg_61" n="HIAT:ip">,</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_64" n="HIAT:w" s="T15">büzʼe</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_67" n="HIAT:w" s="T16">măndə</ts>
                  <nts id="Seg_68" n="HIAT:ip">.</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_71" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_73" n="HIAT:w" s="T17">Eʔbdəbə</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_76" n="HIAT:w" s="T18">saj</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_79" n="HIAT:w" s="T19">băppi</ts>
                  <nts id="Seg_80" n="HIAT:ip">,</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_83" n="HIAT:w" s="T20">tibij</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_86" n="HIAT:w" s="T21">oldʼa</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_89" n="HIAT:w" s="T22">šerbi</ts>
                  <nts id="Seg_90" n="HIAT:ip">.</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_93" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_95" n="HIAT:w" s="T23">Mĭlloʔbdəbi</ts>
                  <nts id="Seg_96" n="HIAT:ip">.</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_99" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_101" n="HIAT:w" s="T24">Büzʼe</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_104" n="HIAT:w" s="T25">tunobi</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_107" n="HIAT:w" s="T26">urgaːba</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_110" n="HIAT:w" s="T27">molaʔ</ts>
                  <nts id="Seg_111" n="HIAT:ip">.</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_114" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_116" n="HIAT:w" s="T28">Aʔtʼəgən</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_119" n="HIAT:w" s="T29">kallaʔ</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_122" n="HIAT:w" s="T30">nobi</ts>
                  <nts id="Seg_123" n="HIAT:ip">.</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_126" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_128" n="HIAT:w" s="T31">Dĭ</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_131" n="HIAT:w" s="T32">koʔbdo</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_134" n="HIAT:w" s="T33">parluʔbi</ts>
                  <nts id="Seg_135" n="HIAT:ip">,</nts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_138" n="HIAT:w" s="T34">parlaʔ</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_141" n="HIAT:w" s="T35">šobi</ts>
                  <nts id="Seg_142" n="HIAT:ip">.</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_145" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_147" n="HIAT:w" s="T36">Mălia:</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_149" n="HIAT:ip">"</nts>
                  <ts e="T38" id="Seg_151" n="HIAT:w" s="T37">Moʔ</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_154" n="HIAT:w" s="T38">parbial</ts>
                  <nts id="Seg_155" n="HIAT:ip">?</nts>
                  <nts id="Seg_156" n="HIAT:ip">"</nts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_159" n="HIAT:u" s="T39">
                  <nts id="Seg_160" n="HIAT:ip">"</nts>
                  <ts e="T40" id="Seg_162" n="HIAT:w" s="T39">Pimbiem</ts>
                  <nts id="Seg_163" n="HIAT:ip">.</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_166" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_168" n="HIAT:w" s="T40">Urgaːba</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_171" n="HIAT:w" s="T41">aʔtʼəgən</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_174" n="HIAT:w" s="T42">mĭŋge</ts>
                  <nts id="Seg_175" n="HIAT:ip">.</nts>
                  <nts id="Seg_176" n="HIAT:ip">"</nts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_179" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_181" n="HIAT:w" s="T43">Tʼergə</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_184" n="HIAT:w" s="T44">koʔbdot</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_187" n="HIAT:w" s="T45">mălia:</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_189" n="HIAT:ip">"</nts>
                  <ts e="T47" id="Seg_191" n="HIAT:w" s="T46">Măn</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_194" n="HIAT:w" s="T47">kallam</ts>
                  <nts id="Seg_195" n="HIAT:ip">.</nts>
                  <nts id="Seg_196" n="HIAT:ip">"</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_199" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_201" n="HIAT:w" s="T48">Bazoʔ</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_204" n="HIAT:w" s="T49">dĭ</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_207" n="HIAT:w" s="T50">büzʼe</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_210" n="HIAT:w" s="T51">urgaːba</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_213" n="HIAT:w" s="T52">mobiːza</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_216" n="HIAT:w" s="T53">bazoʔ</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_219" n="HIAT:w" s="T54">aʔtʼəgən</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_222" n="HIAT:w" s="T55">nulaʔbə</ts>
                  <nts id="Seg_223" n="HIAT:ip">.</nts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_226" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_228" n="HIAT:w" s="T56">Bazoʔ</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_231" n="HIAT:w" s="T57">parlaʔ</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_234" n="HIAT:w" s="T58">šobi</ts>
                  <nts id="Seg_235" n="HIAT:ip">.</nts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_238" n="HIAT:u" s="T59">
                  <nts id="Seg_239" n="HIAT:ip">"</nts>
                  <ts e="T60" id="Seg_241" n="HIAT:w" s="T59">Moʔ</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_244" n="HIAT:w" s="T60">parbial</ts>
                  <nts id="Seg_245" n="HIAT:ip">?</nts>
                  <nts id="Seg_246" n="HIAT:ip">"</nts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_249" n="HIAT:u" s="T61">
                  <nts id="Seg_250" n="HIAT:ip">"</nts>
                  <ts e="T62" id="Seg_252" n="HIAT:w" s="T61">Urgaːba</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_255" n="HIAT:w" s="T62">kubiam</ts>
                  <nts id="Seg_256" n="HIAT:ip">,</nts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_259" n="HIAT:w" s="T63">tĭgəttə</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_262" n="HIAT:w" s="T64">parlaʔ</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_265" n="HIAT:w" s="T65">šobiam</ts>
                  <nts id="Seg_266" n="HIAT:ip">.</nts>
                  <nts id="Seg_267" n="HIAT:ip">"</nts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_270" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_272" n="HIAT:w" s="T66">Üdʼüge</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_275" n="HIAT:w" s="T67">koʔbdot</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_278" n="HIAT:w" s="T68">mĭllüʔbi</ts>
                  <nts id="Seg_279" n="HIAT:ip">.</nts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_282" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_284" n="HIAT:w" s="T69">Tibij</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_287" n="HIAT:w" s="T70">oldʼa</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_290" n="HIAT:w" s="T71">šerbi</ts>
                  <nts id="Seg_291" n="HIAT:ip">.</nts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_294" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_296" n="HIAT:w" s="T72">Dĭ</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_299" n="HIAT:w" s="T73">eʔbdəbə</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_302" n="HIAT:w" s="T74">saj</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_305" n="HIAT:w" s="T75">băppi</ts>
                  <nts id="Seg_306" n="HIAT:ip">.</nts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_309" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_311" n="HIAT:w" s="T76">Mĭlloʔbdəbi</ts>
                  <nts id="Seg_312" n="HIAT:ip">.</nts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_315" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_317" n="HIAT:w" s="T77">Büzʼe</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_320" n="HIAT:w" s="T78">bazoʔ</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_323" n="HIAT:w" s="T79">urgaːba</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_326" n="HIAT:w" s="T80">molaʔ</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_329" n="HIAT:w" s="T81">tunolbi</ts>
                  <nts id="Seg_330" n="HIAT:ip">.</nts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_333" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_335" n="HIAT:w" s="T82">Koʔbdot</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_338" n="HIAT:w" s="T83">möːzʼəʔ</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_341" n="HIAT:w" s="T84">tʼippi</ts>
                  <nts id="Seg_342" n="HIAT:ip">,</nts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_345" n="HIAT:w" s="T85">simabə</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_348" n="HIAT:w" s="T86">păktəj</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_351" n="HIAT:w" s="T87">tʼitlüʔbi</ts>
                  <nts id="Seg_352" n="HIAT:ip">.</nts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_355" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_357" n="HIAT:w" s="T88">Bostə</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_360" n="HIAT:w" s="T89">mĭllüʔbi</ts>
                  <nts id="Seg_361" n="HIAT:ip">.</nts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_364" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_366" n="HIAT:w" s="T90">Kambi</ts>
                  <nts id="Seg_367" n="HIAT:ip">,</nts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_370" n="HIAT:w" s="T91">oʔb</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_373" n="HIAT:w" s="T92">nükegən</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_376" n="HIAT:w" s="T93">üzəbi</ts>
                  <nts id="Seg_377" n="HIAT:ip">.</nts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_380" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_382" n="HIAT:w" s="T94">Dĭ</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_385" n="HIAT:w" s="T95">nüken</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_388" n="HIAT:w" s="T96">nʼit</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_391" n="HIAT:w" s="T97">ibi</ts>
                  <nts id="Seg_392" n="HIAT:ip">.</nts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_395" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_397" n="HIAT:w" s="T98">Dĭzʼəʔ</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_400" n="HIAT:w" s="T99">dĭzeŋ</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_403" n="HIAT:w" s="T100">mĭlleʔbəiʔ</ts>
                  <nts id="Seg_404" n="HIAT:ip">.</nts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_407" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_409" n="HIAT:w" s="T101">Dĭ</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_412" n="HIAT:w" s="T102">nüke</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_415" n="HIAT:w" s="T103">nʼigəndə</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_418" n="HIAT:w" s="T104">mălia:</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_420" n="HIAT:ip">"</nts>
                  <ts e="T106" id="Seg_422" n="HIAT:w" s="T105">Koʔbdo</ts>
                  <nts id="Seg_423" n="HIAT:ip">!</nts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_426" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_428" n="HIAT:w" s="T106">Dĭzʼəʔ</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_431" n="HIAT:w" s="T107">kaŋgələj</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_434" n="HIAT:w" s="T108">sadandəna</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_437" n="HIAT:w" s="T109">maʔdə</ts>
                  <nts id="Seg_438" n="HIAT:ip">!</nts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_441" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_443" n="HIAT:w" s="T110">Nʼi</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_446" n="HIAT:w" s="T111">ibində</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_449" n="HIAT:w" s="T112">inen</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_452" n="HIAT:w" s="T113">oldʼa</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_455" n="HIAT:w" s="T114">kuləj</ts>
                  <nts id="Seg_456" n="HIAT:ip">,</nts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_459" n="HIAT:w" s="T115">koʔbdo</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_462" n="HIAT:w" s="T116">ibində</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_465" n="HIAT:w" s="T117">nen</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_468" n="HIAT:w" s="T118">oldʼa</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_471" n="HIAT:w" s="T119">kuləj</ts>
                  <nts id="Seg_472" n="HIAT:ip">.</nts>
                  <nts id="Seg_473" n="HIAT:ip">"</nts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_476" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_478" n="HIAT:w" s="T120">Kambijəʔ</ts>
                  <nts id="Seg_479" n="HIAT:ip">,</nts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_482" n="HIAT:w" s="T121">šobiiʔ</ts>
                  <nts id="Seg_483" n="HIAT:ip">.</nts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_486" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_488" n="HIAT:w" s="T122">Dĭ</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_491" n="HIAT:w" s="T123">koʔbdo</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_494" n="HIAT:w" s="T124">inen</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_497" n="HIAT:w" s="T125">oldʼa</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_500" n="HIAT:w" s="T126">kulia</ts>
                  <nts id="Seg_501" n="HIAT:ip">,</nts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_504" n="HIAT:w" s="T127">a</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_507" n="HIAT:w" s="T128">dĭ</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_510" n="HIAT:w" s="T129">nʼi</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_513" n="HIAT:w" s="T130">nen</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_516" n="HIAT:w" s="T131">oldʼa</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_519" n="HIAT:w" s="T132">kulia</ts>
                  <nts id="Seg_520" n="HIAT:ip">.</nts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T137" id="Seg_523" n="HIAT:u" s="T133">
                  <ts e="T134" id="Seg_525" n="HIAT:w" s="T133">Maʔgəndə</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_528" n="HIAT:w" s="T134">šobiːza</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_531" n="HIAT:w" s="T135">ijaandə</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_534" n="HIAT:w" s="T136">nörbəlie</ts>
                  <nts id="Seg_535" n="HIAT:ip">.</nts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_538" n="HIAT:u" s="T137">
                  <ts e="T138" id="Seg_540" n="HIAT:w" s="T137">Mălia:</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_542" n="HIAT:ip">"</nts>
                  <ts e="T139" id="Seg_544" n="HIAT:w" s="T138">Dĭ</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_547" n="HIAT:w" s="T139">nʼi</ts>
                  <nts id="Seg_548" n="HIAT:ip">,</nts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_551" n="HIAT:w" s="T140">inen</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_554" n="HIAT:w" s="T141">oldʼa</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_557" n="HIAT:w" s="T142">kulia</ts>
                  <nts id="Seg_558" n="HIAT:ip">.</nts>
                  <nts id="Seg_559" n="HIAT:ip">"</nts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T147" id="Seg_562" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_564" n="HIAT:w" s="T143">Dĭ</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_567" n="HIAT:w" s="T144">nüke</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_570" n="HIAT:w" s="T145">mălia:</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_572" n="HIAT:ip">"</nts>
                  <ts e="T147" id="Seg_574" n="HIAT:w" s="T146">Koʔbdo</ts>
                  <nts id="Seg_575" n="HIAT:ip">!</nts>
                  <nts id="Seg_576" n="HIAT:ip">"</nts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T151" id="Seg_579" n="HIAT:u" s="T147">
                  <ts e="T148" id="Seg_581" n="HIAT:w" s="T147">Mălia:</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_583" n="HIAT:ip">"</nts>
                  <ts e="T149" id="Seg_585" n="HIAT:w" s="T148">Kanaʔ</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_588" n="HIAT:w" s="T149">multʼanə</ts>
                  <nts id="Seg_589" n="HIAT:ip">,</nts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_592" n="HIAT:w" s="T150">kaŋgaʔ</ts>
                  <nts id="Seg_593" n="HIAT:ip">!</nts>
                  <nts id="Seg_594" n="HIAT:ip">"</nts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_597" n="HIAT:u" s="T151">
                  <ts e="T152" id="Seg_599" n="HIAT:w" s="T151">Kambiiʔ</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_602" n="HIAT:w" s="T152">multʼanə</ts>
                  <nts id="Seg_603" n="HIAT:ip">.</nts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T155" id="Seg_606" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_608" n="HIAT:w" s="T153">Sabəndə</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_611" n="HIAT:w" s="T154">nöməllabaʔbi</ts>
                  <nts id="Seg_612" n="HIAT:ip">.</nts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_615" n="HIAT:u" s="T155">
                  <ts e="T156" id="Seg_617" n="HIAT:w" s="T155">Parbi</ts>
                  <nts id="Seg_618" n="HIAT:ip">.</nts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T163" id="Seg_621" n="HIAT:u" s="T156">
                  <ts e="T157" id="Seg_623" n="HIAT:w" s="T156">Dĭ</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_626" n="HIAT:w" s="T157">koʔbdo</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_629" n="HIAT:w" s="T158">ulubə</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_632" n="HIAT:w" s="T159">bazəbi</ts>
                  <nts id="Seg_633" n="HIAT:ip">,</nts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_636" n="HIAT:w" s="T160">ej</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_639" n="HIAT:w" s="T161">săbəjʔ</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_642" n="HIAT:w" s="T162">ködəbi</ts>
                  <nts id="Seg_643" n="HIAT:ip">.</nts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_646" n="HIAT:u" s="T163">
                  <ts e="T164" id="Seg_648" n="HIAT:w" s="T163">Da</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_651" n="HIAT:w" s="T164">šobi</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_654" n="HIAT:w" s="T165">nʼi</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_657" n="HIAT:w" s="T166">dĭ</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_660" n="HIAT:w" s="T167">piʔdölabaʔbi</ts>
                  <nts id="Seg_661" n="HIAT:ip">.</nts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T173" id="Seg_664" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_666" n="HIAT:w" s="T168">Dĭ</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_669" n="HIAT:w" s="T169">nʼi</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_672" n="HIAT:w" s="T170">šobiːza</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_675" n="HIAT:w" s="T171">ijaandə</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_678" n="HIAT:w" s="T172">nörbəlie</ts>
                  <nts id="Seg_679" n="HIAT:ip">.</nts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T177" id="Seg_682" n="HIAT:u" s="T173">
                  <nts id="Seg_683" n="HIAT:ip">"</nts>
                  <ts e="T174" id="Seg_685" n="HIAT:w" s="T173">Nʼi</ts>
                  <nts id="Seg_686" n="HIAT:ip">"</nts>
                  <nts id="Seg_687" n="HIAT:ip">,</nts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_690" n="HIAT:w" s="T174">mălia</ts>
                  <nts id="Seg_691" n="HIAT:ip">,</nts>
                  <nts id="Seg_692" n="HIAT:ip">"</nts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_695" n="HIAT:w" s="T175">ej</ts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_698" n="HIAT:w" s="T176">koʔbdo</ts>
                  <nts id="Seg_699" n="HIAT:ip">"</nts>
                  <nts id="Seg_700" n="HIAT:ip">.</nts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T182" id="Seg_703" n="HIAT:u" s="T177">
                  <ts e="T178" id="Seg_705" n="HIAT:w" s="T177">Dĭgəttə</ts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_708" n="HIAT:w" s="T178">dĭ</ts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_711" n="HIAT:w" s="T179">koʔbdo</ts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_714" n="HIAT:w" s="T180">maʔgəndə</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_717" n="HIAT:w" s="T181">mĭllaːndəga</ts>
                  <nts id="Seg_718" n="HIAT:ip">.</nts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_721" n="HIAT:u" s="T182">
                  <ts e="T183" id="Seg_723" n="HIAT:w" s="T182">Büm</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_726" n="HIAT:w" s="T183">bejbi</ts>
                  <nts id="Seg_727" n="HIAT:ip">.</nts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T192" id="Seg_730" n="HIAT:u" s="T184">
                  <ts e="T185" id="Seg_732" n="HIAT:w" s="T184">Nüjübə</ts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_735" n="HIAT:w" s="T185">pʼeriet:</ts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_737" n="HIAT:ip">"</nts>
                  <ts e="T187" id="Seg_739" n="HIAT:w" s="T186">Šĭʔ</ts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_742" n="HIAT:w" s="T187">ipekətsʼəʔleʔ</ts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_745" n="HIAT:w" s="T188">kajət</ts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_748" n="HIAT:w" s="T189">măn</ts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_751" n="HIAT:w" s="T190">nüjüm</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_754" n="HIAT:w" s="T191">özerlüʔbi</ts>
                  <nts id="Seg_755" n="HIAT:ip">.</nts>
                  <nts id="Seg_756" n="HIAT:ip">"</nts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T199" id="Seg_759" n="HIAT:u" s="T192">
                  <ts e="T193" id="Seg_761" n="HIAT:w" s="T192">Dĭ</ts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_764" n="HIAT:w" s="T193">nüke</ts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_767" n="HIAT:w" s="T194">nʼibə</ts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_770" n="HIAT:w" s="T195">kudoliat:</ts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_772" n="HIAT:ip">"</nts>
                  <ts e="T197" id="Seg_774" n="HIAT:w" s="T196">Tăn</ts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_777" n="HIAT:w" s="T197">muʔ</ts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_780" n="HIAT:w" s="T198">šʼaːmbial</ts>
                  <nts id="Seg_781" n="HIAT:ip">?</nts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T203" id="Seg_784" n="HIAT:u" s="T199">
                  <ts e="T200" id="Seg_786" n="HIAT:w" s="T199">Dĭ</ts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_789" n="HIAT:w" s="T200">koʔbdo</ts>
                  <nts id="Seg_790" n="HIAT:ip">,</nts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_793" n="HIAT:w" s="T201">nʼi</ts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_796" n="HIAT:w" s="T202">mămbial</ts>
                  <nts id="Seg_797" n="HIAT:ip">.</nts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T205" id="Seg_800" n="HIAT:u" s="T203">
                  <ts e="T204" id="Seg_802" n="HIAT:w" s="T203">Mĭllüʔbi</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_805" n="HIAT:w" s="T204">maʔgəndə</ts>
                  <nts id="Seg_806" n="HIAT:ip">.</nts>
                  <nts id="Seg_807" n="HIAT:ip">"</nts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T209" id="Seg_810" n="HIAT:u" s="T205">
                  <ts e="T206" id="Seg_812" n="HIAT:w" s="T205">Dĭ</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_815" n="HIAT:w" s="T206">nüke</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_818" n="HIAT:w" s="T207">öʔlüʔbi</ts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_821" n="HIAT:w" s="T208">enəidənəidə</ts>
                  <nts id="Seg_822" n="HIAT:ip">.</nts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T212" id="Seg_825" n="HIAT:u" s="T209">
                  <nts id="Seg_826" n="HIAT:ip">"</nts>
                  <ts e="T210" id="Seg_828" n="HIAT:w" s="T209">Kut</ts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_831" n="HIAT:w" s="T210">dĭm</ts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_834" n="HIAT:w" s="T211">kallaʔ</ts>
                  <nts id="Seg_835" n="HIAT:ip">!</nts>
                  <nts id="Seg_836" n="HIAT:ip">"</nts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T215" id="Seg_839" n="HIAT:u" s="T212">
                  <ts e="T213" id="Seg_841" n="HIAT:w" s="T212">Šaːškan</ts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_844" n="HIAT:w" s="T213">molaʔ</ts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_847" n="HIAT:w" s="T214">nʼergölüʔbi</ts>
                  <nts id="Seg_848" n="HIAT:ip">.</nts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T217" id="Seg_851" n="HIAT:u" s="T215">
                  <ts e="T216" id="Seg_853" n="HIAT:w" s="T215">Kunolamna</ts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_856" n="HIAT:w" s="T216">bapondə</ts>
                  <nts id="Seg_857" n="HIAT:ip">.</nts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T220" id="Seg_860" n="HIAT:u" s="T217">
                  <ts e="T218" id="Seg_862" n="HIAT:w" s="T217">Bapotsʼəʔ</ts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_865" n="HIAT:w" s="T218">mĭnžəleʔ</ts>
                  <nts id="Seg_866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_868" n="HIAT:w" s="T219">oʔbdulbiiʔ</ts>
                  <nts id="Seg_869" n="HIAT:ip">.</nts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T225" id="Seg_872" n="HIAT:u" s="T220">
                  <ts e="T221" id="Seg_874" n="HIAT:w" s="T220">Dĭ</ts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_877" n="HIAT:w" s="T221">šüʔbdöbi</ts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_880" n="HIAT:w" s="T222">gijen</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_883" n="HIAT:w" s="T223">bü</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_886" n="HIAT:w" s="T224">bejlemne</ts>
                  <nts id="Seg_887" n="HIAT:ip">.</nts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T228" id="Seg_890" n="HIAT:u" s="T225">
                  <ts e="T226" id="Seg_892" n="HIAT:w" s="T225">Dĭ</ts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_895" n="HIAT:w" s="T226">tʼügəndə</ts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_898" n="HIAT:w" s="T227">teppiiʔ</ts>
                  <nts id="Seg_899" n="HIAT:ip">.</nts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T231" id="Seg_902" n="HIAT:u" s="T228">
                  <ts e="T229" id="Seg_904" n="HIAT:w" s="T228">Nʼigəndə</ts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_907" n="HIAT:w" s="T229">ileʔ</ts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_910" n="HIAT:w" s="T230">baʔbiiʔ</ts>
                  <nts id="Seg_911" n="HIAT:ip">.</nts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T231" id="Seg_913" n="sc" s="T0">
               <ts e="T1" id="Seg_915" n="e" s="T0">Büzʼen </ts>
               <ts e="T2" id="Seg_917" n="e" s="T1">nagur </ts>
               <ts e="T3" id="Seg_919" n="e" s="T2">koʔbdot </ts>
               <ts e="T4" id="Seg_921" n="e" s="T3">ibi. </ts>
               <ts e="T5" id="Seg_923" n="e" s="T4">Dĭ </ts>
               <ts e="T6" id="Seg_925" n="e" s="T5">büzʼem </ts>
               <ts e="T7" id="Seg_927" n="e" s="T6">turanə </ts>
               <ts e="T8" id="Seg_929" n="e" s="T7">služit </ts>
               <ts e="T9" id="Seg_931" n="e" s="T8">mosʼtə. </ts>
               <ts e="T10" id="Seg_933" n="e" s="T9">Urgo </ts>
               <ts e="T11" id="Seg_935" n="e" s="T10">koʔbdot: </ts>
               <ts e="T12" id="Seg_937" n="e" s="T11">"Măn </ts>
               <ts e="T13" id="Seg_939" n="e" s="T12">kallam." </ts>
               <ts e="T14" id="Seg_941" n="e" s="T13">"No, </ts>
               <ts e="T15" id="Seg_943" n="e" s="T14">kalla", </ts>
               <ts e="T16" id="Seg_945" n="e" s="T15">büzʼe </ts>
               <ts e="T17" id="Seg_947" n="e" s="T16">măndə. </ts>
               <ts e="T18" id="Seg_949" n="e" s="T17">Eʔbdəbə </ts>
               <ts e="T19" id="Seg_951" n="e" s="T18">saj </ts>
               <ts e="T20" id="Seg_953" n="e" s="T19">băppi, </ts>
               <ts e="T21" id="Seg_955" n="e" s="T20">tibij </ts>
               <ts e="T22" id="Seg_957" n="e" s="T21">oldʼa </ts>
               <ts e="T23" id="Seg_959" n="e" s="T22">šerbi. </ts>
               <ts e="T24" id="Seg_961" n="e" s="T23">Mĭlloʔbdəbi. </ts>
               <ts e="T25" id="Seg_963" n="e" s="T24">Büzʼe </ts>
               <ts e="T26" id="Seg_965" n="e" s="T25">tunobi </ts>
               <ts e="T27" id="Seg_967" n="e" s="T26">urgaːba </ts>
               <ts e="T28" id="Seg_969" n="e" s="T27">molaʔ. </ts>
               <ts e="T29" id="Seg_971" n="e" s="T28">Aʔtʼəgən </ts>
               <ts e="T30" id="Seg_973" n="e" s="T29">kallaʔ </ts>
               <ts e="T31" id="Seg_975" n="e" s="T30">nobi. </ts>
               <ts e="T32" id="Seg_977" n="e" s="T31">Dĭ </ts>
               <ts e="T33" id="Seg_979" n="e" s="T32">koʔbdo </ts>
               <ts e="T34" id="Seg_981" n="e" s="T33">parluʔbi, </ts>
               <ts e="T35" id="Seg_983" n="e" s="T34">parlaʔ </ts>
               <ts e="T36" id="Seg_985" n="e" s="T35">šobi. </ts>
               <ts e="T37" id="Seg_987" n="e" s="T36">Mălia: </ts>
               <ts e="T38" id="Seg_989" n="e" s="T37">"Moʔ </ts>
               <ts e="T39" id="Seg_991" n="e" s="T38">parbial?" </ts>
               <ts e="T40" id="Seg_993" n="e" s="T39">"Pimbiem. </ts>
               <ts e="T41" id="Seg_995" n="e" s="T40">Urgaːba </ts>
               <ts e="T42" id="Seg_997" n="e" s="T41">aʔtʼəgən </ts>
               <ts e="T43" id="Seg_999" n="e" s="T42">mĭŋge." </ts>
               <ts e="T44" id="Seg_1001" n="e" s="T43">Tʼergə </ts>
               <ts e="T45" id="Seg_1003" n="e" s="T44">koʔbdot </ts>
               <ts e="T46" id="Seg_1005" n="e" s="T45">mălia: </ts>
               <ts e="T47" id="Seg_1007" n="e" s="T46">"Măn </ts>
               <ts e="T48" id="Seg_1009" n="e" s="T47">kallam." </ts>
               <ts e="T49" id="Seg_1011" n="e" s="T48">Bazoʔ </ts>
               <ts e="T50" id="Seg_1013" n="e" s="T49">dĭ </ts>
               <ts e="T51" id="Seg_1015" n="e" s="T50">büzʼe </ts>
               <ts e="T52" id="Seg_1017" n="e" s="T51">urgaːba </ts>
               <ts e="T53" id="Seg_1019" n="e" s="T52">mobiːza </ts>
               <ts e="T54" id="Seg_1021" n="e" s="T53">bazoʔ </ts>
               <ts e="T55" id="Seg_1023" n="e" s="T54">aʔtʼəgən </ts>
               <ts e="T56" id="Seg_1025" n="e" s="T55">nulaʔbə. </ts>
               <ts e="T57" id="Seg_1027" n="e" s="T56">Bazoʔ </ts>
               <ts e="T58" id="Seg_1029" n="e" s="T57">parlaʔ </ts>
               <ts e="T59" id="Seg_1031" n="e" s="T58">šobi. </ts>
               <ts e="T60" id="Seg_1033" n="e" s="T59">"Moʔ </ts>
               <ts e="T61" id="Seg_1035" n="e" s="T60">parbial?" </ts>
               <ts e="T62" id="Seg_1037" n="e" s="T61">"Urgaːba </ts>
               <ts e="T63" id="Seg_1039" n="e" s="T62">kubiam, </ts>
               <ts e="T64" id="Seg_1041" n="e" s="T63">tĭgəttə </ts>
               <ts e="T65" id="Seg_1043" n="e" s="T64">parlaʔ </ts>
               <ts e="T66" id="Seg_1045" n="e" s="T65">šobiam." </ts>
               <ts e="T67" id="Seg_1047" n="e" s="T66">Üdʼüge </ts>
               <ts e="T68" id="Seg_1049" n="e" s="T67">koʔbdot </ts>
               <ts e="T69" id="Seg_1051" n="e" s="T68">mĭllüʔbi. </ts>
               <ts e="T70" id="Seg_1053" n="e" s="T69">Tibij </ts>
               <ts e="T71" id="Seg_1055" n="e" s="T70">oldʼa </ts>
               <ts e="T72" id="Seg_1057" n="e" s="T71">šerbi. </ts>
               <ts e="T73" id="Seg_1059" n="e" s="T72">Dĭ </ts>
               <ts e="T74" id="Seg_1061" n="e" s="T73">eʔbdəbə </ts>
               <ts e="T75" id="Seg_1063" n="e" s="T74">saj </ts>
               <ts e="T76" id="Seg_1065" n="e" s="T75">băppi. </ts>
               <ts e="T77" id="Seg_1067" n="e" s="T76">Mĭlloʔbdəbi. </ts>
               <ts e="T78" id="Seg_1069" n="e" s="T77">Büzʼe </ts>
               <ts e="T79" id="Seg_1071" n="e" s="T78">bazoʔ </ts>
               <ts e="T80" id="Seg_1073" n="e" s="T79">urgaːba </ts>
               <ts e="T81" id="Seg_1075" n="e" s="T80">molaʔ </ts>
               <ts e="T82" id="Seg_1077" n="e" s="T81">tunolbi. </ts>
               <ts e="T83" id="Seg_1079" n="e" s="T82">Koʔbdot </ts>
               <ts e="T84" id="Seg_1081" n="e" s="T83">möːzʼəʔ </ts>
               <ts e="T85" id="Seg_1083" n="e" s="T84">tʼippi, </ts>
               <ts e="T86" id="Seg_1085" n="e" s="T85">simabə </ts>
               <ts e="T87" id="Seg_1087" n="e" s="T86">păktəj </ts>
               <ts e="T88" id="Seg_1089" n="e" s="T87">tʼitlüʔbi. </ts>
               <ts e="T89" id="Seg_1091" n="e" s="T88">Bostə </ts>
               <ts e="T90" id="Seg_1093" n="e" s="T89">mĭllüʔbi. </ts>
               <ts e="T91" id="Seg_1095" n="e" s="T90">Kambi, </ts>
               <ts e="T92" id="Seg_1097" n="e" s="T91">oʔb </ts>
               <ts e="T93" id="Seg_1099" n="e" s="T92">nükegən </ts>
               <ts e="T94" id="Seg_1101" n="e" s="T93">üzəbi. </ts>
               <ts e="T95" id="Seg_1103" n="e" s="T94">Dĭ </ts>
               <ts e="T96" id="Seg_1105" n="e" s="T95">nüken </ts>
               <ts e="T97" id="Seg_1107" n="e" s="T96">nʼit </ts>
               <ts e="T98" id="Seg_1109" n="e" s="T97">ibi. </ts>
               <ts e="T99" id="Seg_1111" n="e" s="T98">Dĭzʼəʔ </ts>
               <ts e="T100" id="Seg_1113" n="e" s="T99">dĭzeŋ </ts>
               <ts e="T101" id="Seg_1115" n="e" s="T100">mĭlleʔbəiʔ. </ts>
               <ts e="T102" id="Seg_1117" n="e" s="T101">Dĭ </ts>
               <ts e="T103" id="Seg_1119" n="e" s="T102">nüke </ts>
               <ts e="T104" id="Seg_1121" n="e" s="T103">nʼigəndə </ts>
               <ts e="T105" id="Seg_1123" n="e" s="T104">mălia: </ts>
               <ts e="T106" id="Seg_1125" n="e" s="T105">"Koʔbdo! </ts>
               <ts e="T107" id="Seg_1127" n="e" s="T106">Dĭzʼəʔ </ts>
               <ts e="T108" id="Seg_1129" n="e" s="T107">kaŋgələj </ts>
               <ts e="T109" id="Seg_1131" n="e" s="T108">sadandəna </ts>
               <ts e="T110" id="Seg_1133" n="e" s="T109">maʔdə! </ts>
               <ts e="T111" id="Seg_1135" n="e" s="T110">Nʼi </ts>
               <ts e="T112" id="Seg_1137" n="e" s="T111">ibində </ts>
               <ts e="T113" id="Seg_1139" n="e" s="T112">inen </ts>
               <ts e="T114" id="Seg_1141" n="e" s="T113">oldʼa </ts>
               <ts e="T115" id="Seg_1143" n="e" s="T114">kuləj, </ts>
               <ts e="T116" id="Seg_1145" n="e" s="T115">koʔbdo </ts>
               <ts e="T117" id="Seg_1147" n="e" s="T116">ibində </ts>
               <ts e="T118" id="Seg_1149" n="e" s="T117">nen </ts>
               <ts e="T119" id="Seg_1151" n="e" s="T118">oldʼa </ts>
               <ts e="T120" id="Seg_1153" n="e" s="T119">kuləj." </ts>
               <ts e="T121" id="Seg_1155" n="e" s="T120">Kambijəʔ, </ts>
               <ts e="T122" id="Seg_1157" n="e" s="T121">šobiiʔ. </ts>
               <ts e="T123" id="Seg_1159" n="e" s="T122">Dĭ </ts>
               <ts e="T124" id="Seg_1161" n="e" s="T123">koʔbdo </ts>
               <ts e="T125" id="Seg_1163" n="e" s="T124">inen </ts>
               <ts e="T126" id="Seg_1165" n="e" s="T125">oldʼa </ts>
               <ts e="T127" id="Seg_1167" n="e" s="T126">kulia, </ts>
               <ts e="T128" id="Seg_1169" n="e" s="T127">a </ts>
               <ts e="T129" id="Seg_1171" n="e" s="T128">dĭ </ts>
               <ts e="T130" id="Seg_1173" n="e" s="T129">nʼi </ts>
               <ts e="T131" id="Seg_1175" n="e" s="T130">nen </ts>
               <ts e="T132" id="Seg_1177" n="e" s="T131">oldʼa </ts>
               <ts e="T133" id="Seg_1179" n="e" s="T132">kulia. </ts>
               <ts e="T134" id="Seg_1181" n="e" s="T133">Maʔgəndə </ts>
               <ts e="T135" id="Seg_1183" n="e" s="T134">šobiːza </ts>
               <ts e="T136" id="Seg_1185" n="e" s="T135">ijaandə </ts>
               <ts e="T137" id="Seg_1187" n="e" s="T136">nörbəlie. </ts>
               <ts e="T138" id="Seg_1189" n="e" s="T137">Mălia: </ts>
               <ts e="T139" id="Seg_1191" n="e" s="T138">"Dĭ </ts>
               <ts e="T140" id="Seg_1193" n="e" s="T139">nʼi, </ts>
               <ts e="T141" id="Seg_1195" n="e" s="T140">inen </ts>
               <ts e="T142" id="Seg_1197" n="e" s="T141">oldʼa </ts>
               <ts e="T143" id="Seg_1199" n="e" s="T142">kulia." </ts>
               <ts e="T144" id="Seg_1201" n="e" s="T143">Dĭ </ts>
               <ts e="T145" id="Seg_1203" n="e" s="T144">nüke </ts>
               <ts e="T146" id="Seg_1205" n="e" s="T145">mălia: </ts>
               <ts e="T147" id="Seg_1207" n="e" s="T146">"Koʔbdo!" </ts>
               <ts e="T148" id="Seg_1209" n="e" s="T147">Mălia: </ts>
               <ts e="T149" id="Seg_1211" n="e" s="T148">"Kanaʔ </ts>
               <ts e="T150" id="Seg_1213" n="e" s="T149">multʼanə, </ts>
               <ts e="T151" id="Seg_1215" n="e" s="T150">kaŋgaʔ!" </ts>
               <ts e="T152" id="Seg_1217" n="e" s="T151">Kambiiʔ </ts>
               <ts e="T153" id="Seg_1219" n="e" s="T152">multʼanə. </ts>
               <ts e="T154" id="Seg_1221" n="e" s="T153">Sabəndə </ts>
               <ts e="T155" id="Seg_1223" n="e" s="T154">nöməllabaʔbi. </ts>
               <ts e="T156" id="Seg_1225" n="e" s="T155">Parbi. </ts>
               <ts e="T157" id="Seg_1227" n="e" s="T156">Dĭ </ts>
               <ts e="T158" id="Seg_1229" n="e" s="T157">koʔbdo </ts>
               <ts e="T159" id="Seg_1231" n="e" s="T158">ulubə </ts>
               <ts e="T160" id="Seg_1233" n="e" s="T159">bazəbi, </ts>
               <ts e="T161" id="Seg_1235" n="e" s="T160">ej </ts>
               <ts e="T162" id="Seg_1237" n="e" s="T161">săbəjʔ </ts>
               <ts e="T163" id="Seg_1239" n="e" s="T162">ködəbi. </ts>
               <ts e="T164" id="Seg_1241" n="e" s="T163">Da </ts>
               <ts e="T165" id="Seg_1243" n="e" s="T164">šobi </ts>
               <ts e="T166" id="Seg_1245" n="e" s="T165">nʼi </ts>
               <ts e="T167" id="Seg_1247" n="e" s="T166">dĭ </ts>
               <ts e="T168" id="Seg_1249" n="e" s="T167">piʔdölabaʔbi. </ts>
               <ts e="T169" id="Seg_1251" n="e" s="T168">Dĭ </ts>
               <ts e="T170" id="Seg_1253" n="e" s="T169">nʼi </ts>
               <ts e="T171" id="Seg_1255" n="e" s="T170">šobiːza </ts>
               <ts e="T172" id="Seg_1257" n="e" s="T171">ijaandə </ts>
               <ts e="T173" id="Seg_1259" n="e" s="T172">nörbəlie. </ts>
               <ts e="T174" id="Seg_1261" n="e" s="T173">"Nʼi", </ts>
               <ts e="T175" id="Seg_1263" n="e" s="T174">mălia," </ts>
               <ts e="T176" id="Seg_1265" n="e" s="T175">ej </ts>
               <ts e="T177" id="Seg_1267" n="e" s="T176">koʔbdo". </ts>
               <ts e="T178" id="Seg_1269" n="e" s="T177">Dĭgəttə </ts>
               <ts e="T179" id="Seg_1271" n="e" s="T178">dĭ </ts>
               <ts e="T180" id="Seg_1273" n="e" s="T179">koʔbdo </ts>
               <ts e="T181" id="Seg_1275" n="e" s="T180">maʔgəndə </ts>
               <ts e="T182" id="Seg_1277" n="e" s="T181">mĭllaːndəga. </ts>
               <ts e="T183" id="Seg_1279" n="e" s="T182">Büm </ts>
               <ts e="T184" id="Seg_1281" n="e" s="T183">bejbi. </ts>
               <ts e="T185" id="Seg_1283" n="e" s="T184">Nüjübə </ts>
               <ts e="T186" id="Seg_1285" n="e" s="T185">pʼeriet: </ts>
               <ts e="T187" id="Seg_1287" n="e" s="T186">"Šĭʔ </ts>
               <ts e="T188" id="Seg_1289" n="e" s="T187">ipekətsʼəʔleʔ </ts>
               <ts e="T189" id="Seg_1291" n="e" s="T188">kajət </ts>
               <ts e="T190" id="Seg_1293" n="e" s="T189">măn </ts>
               <ts e="T191" id="Seg_1295" n="e" s="T190">nüjüm </ts>
               <ts e="T192" id="Seg_1297" n="e" s="T191">özerlüʔbi." </ts>
               <ts e="T193" id="Seg_1299" n="e" s="T192">Dĭ </ts>
               <ts e="T194" id="Seg_1301" n="e" s="T193">nüke </ts>
               <ts e="T195" id="Seg_1303" n="e" s="T194">nʼibə </ts>
               <ts e="T196" id="Seg_1305" n="e" s="T195">kudoliat: </ts>
               <ts e="T197" id="Seg_1307" n="e" s="T196">"Tăn </ts>
               <ts e="T198" id="Seg_1309" n="e" s="T197">muʔ </ts>
               <ts e="T199" id="Seg_1311" n="e" s="T198">šʼaːmbial? </ts>
               <ts e="T200" id="Seg_1313" n="e" s="T199">Dĭ </ts>
               <ts e="T201" id="Seg_1315" n="e" s="T200">koʔbdo, </ts>
               <ts e="T202" id="Seg_1317" n="e" s="T201">nʼi </ts>
               <ts e="T203" id="Seg_1319" n="e" s="T202">mămbial. </ts>
               <ts e="T204" id="Seg_1321" n="e" s="T203">Mĭllüʔbi </ts>
               <ts e="T205" id="Seg_1323" n="e" s="T204">maʔgəndə." </ts>
               <ts e="T206" id="Seg_1325" n="e" s="T205">Dĭ </ts>
               <ts e="T207" id="Seg_1327" n="e" s="T206">nüke </ts>
               <ts e="T208" id="Seg_1329" n="e" s="T207">öʔlüʔbi </ts>
               <ts e="T209" id="Seg_1331" n="e" s="T208">enəidənəidə. </ts>
               <ts e="T210" id="Seg_1333" n="e" s="T209">"Kut </ts>
               <ts e="T211" id="Seg_1335" n="e" s="T210">dĭm </ts>
               <ts e="T212" id="Seg_1337" n="e" s="T211">kallaʔ!" </ts>
               <ts e="T213" id="Seg_1339" n="e" s="T212">Šaːškan </ts>
               <ts e="T214" id="Seg_1341" n="e" s="T213">molaʔ </ts>
               <ts e="T215" id="Seg_1343" n="e" s="T214">nʼergölüʔbi. </ts>
               <ts e="T216" id="Seg_1345" n="e" s="T215">Kunolamna </ts>
               <ts e="T217" id="Seg_1347" n="e" s="T216">bapondə. </ts>
               <ts e="T218" id="Seg_1349" n="e" s="T217">Bapotsʼəʔ </ts>
               <ts e="T219" id="Seg_1351" n="e" s="T218">mĭnžəleʔ </ts>
               <ts e="T220" id="Seg_1353" n="e" s="T219">oʔbdulbiiʔ. </ts>
               <ts e="T221" id="Seg_1355" n="e" s="T220">Dĭ </ts>
               <ts e="T222" id="Seg_1357" n="e" s="T221">šüʔbdöbi </ts>
               <ts e="T223" id="Seg_1359" n="e" s="T222">gijen </ts>
               <ts e="T224" id="Seg_1361" n="e" s="T223">bü </ts>
               <ts e="T225" id="Seg_1363" n="e" s="T224">bejlemne. </ts>
               <ts e="T226" id="Seg_1365" n="e" s="T225">Dĭ </ts>
               <ts e="T227" id="Seg_1367" n="e" s="T226">tʼügəndə </ts>
               <ts e="T228" id="Seg_1369" n="e" s="T227">teppiiʔ. </ts>
               <ts e="T229" id="Seg_1371" n="e" s="T228">Nʼigəndə </ts>
               <ts e="T230" id="Seg_1373" n="e" s="T229">ileʔ </ts>
               <ts e="T231" id="Seg_1375" n="e" s="T230">baʔbiiʔ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_1376" s="T0">AA_1914_Girl_flk.001 (001.001)</ta>
            <ta e="T9" id="Seg_1377" s="T4">AA_1914_Girl_flk.002 (001.002)</ta>
            <ta e="T13" id="Seg_1378" s="T9">AA_1914_Girl_flk.003 (001.003)</ta>
            <ta e="T17" id="Seg_1379" s="T13">AA_1914_Girl_flk.004 (001.005)</ta>
            <ta e="T23" id="Seg_1380" s="T17">AA_1914_Girl_flk.005 (001.006)</ta>
            <ta e="T24" id="Seg_1381" s="T23">AA_1914_Girl_flk.006 (001.007)</ta>
            <ta e="T28" id="Seg_1382" s="T24">AA_1914_Girl_flk.007 (001.008)</ta>
            <ta e="T31" id="Seg_1383" s="T28">AA_1914_Girl_flk.008 (001.009)</ta>
            <ta e="T36" id="Seg_1384" s="T31">AA_1914_Girl_flk.009 (001.010)</ta>
            <ta e="T39" id="Seg_1385" s="T36">AA_1914_Girl_flk.010 (001.011)</ta>
            <ta e="T40" id="Seg_1386" s="T39">AA_1914_Girl_flk.011 (001.013)</ta>
            <ta e="T43" id="Seg_1387" s="T40">AA_1914_Girl_flk.012 (001.014)</ta>
            <ta e="T48" id="Seg_1388" s="T43">AA_1914_Girl_flk.013 (001.015)</ta>
            <ta e="T56" id="Seg_1389" s="T48">AA_1914_Girl_flk.014 (001.017)</ta>
            <ta e="T59" id="Seg_1390" s="T56">AA_1914_Girl_flk.015 (001.018)</ta>
            <ta e="T61" id="Seg_1391" s="T59">AA_1914_Girl_flk.016 (001.019)</ta>
            <ta e="T66" id="Seg_1392" s="T61">AA_1914_Girl_flk.017 (001.020)</ta>
            <ta e="T69" id="Seg_1393" s="T66">AA_1914_Girl_flk.018 (001.021)</ta>
            <ta e="T72" id="Seg_1394" s="T69">AA_1914_Girl_flk.019 (001.022)</ta>
            <ta e="T76" id="Seg_1395" s="T72">AA_1914_Girl_flk.020 (001.023)</ta>
            <ta e="T77" id="Seg_1396" s="T76">AA_1914_Girl_flk.021 (001.024)</ta>
            <ta e="T82" id="Seg_1397" s="T77">AA_1914_Girl_flk.022 (001.025)</ta>
            <ta e="T88" id="Seg_1398" s="T82">AA_1914_Girl_flk.023 (001.026)</ta>
            <ta e="T90" id="Seg_1399" s="T88">AA_1914_Girl_flk.024 (001.027)</ta>
            <ta e="T94" id="Seg_1400" s="T90">AA_1914_Girl_flk.025 (002.001)</ta>
            <ta e="T98" id="Seg_1401" s="T94">AA_1914_Girl_flk.026 (002.002)</ta>
            <ta e="T101" id="Seg_1402" s="T98">AA_1914_Girl_flk.027 (002.003)</ta>
            <ta e="T106" id="Seg_1403" s="T101">AA_1914_Girl_flk.028 (002.004)</ta>
            <ta e="T110" id="Seg_1404" s="T106">AA_1914_Girl_flk.029 (002.006)</ta>
            <ta e="T120" id="Seg_1405" s="T110">AA_1914_Girl_flk.030 (002.007)</ta>
            <ta e="T122" id="Seg_1406" s="T120">AA_1914_Girl_flk.031 (002.008)</ta>
            <ta e="T133" id="Seg_1407" s="T122">AA_1914_Girl_flk.032 (002.009)</ta>
            <ta e="T137" id="Seg_1408" s="T133">AA_1914_Girl_flk.033 (002.010)</ta>
            <ta e="T143" id="Seg_1409" s="T137">AA_1914_Girl_flk.034 (002.011)</ta>
            <ta e="T147" id="Seg_1410" s="T143">AA_1914_Girl_flk.035 (002.013)</ta>
            <ta e="T151" id="Seg_1411" s="T147">AA_1914_Girl_flk.036 (002.015)</ta>
            <ta e="T153" id="Seg_1412" s="T151">AA_1914_Girl_flk.037 (002.017)</ta>
            <ta e="T155" id="Seg_1413" s="T153">AA_1914_Girl_flk.038 (002.018)</ta>
            <ta e="T156" id="Seg_1414" s="T155">AA_1914_Girl_flk.039 (002.019)</ta>
            <ta e="T163" id="Seg_1415" s="T156">AA_1914_Girl_flk.040 (002.020)</ta>
            <ta e="T168" id="Seg_1416" s="T163">AA_1914_Girl_flk.041 (002.021)</ta>
            <ta e="T173" id="Seg_1417" s="T168">AA_1914_Girl_flk.042 (002.022)</ta>
            <ta e="T177" id="Seg_1418" s="T173">AA_1914_Girl_flk.043 (002.023)</ta>
            <ta e="T182" id="Seg_1419" s="T177">AA_1914_Girl_flk.044 (003.001)</ta>
            <ta e="T184" id="Seg_1420" s="T182">AA_1914_Girl_flk.045 (003.002)</ta>
            <ta e="T192" id="Seg_1421" s="T184">AA_1914_Girl_flk.046 (003.003)</ta>
            <ta e="T199" id="Seg_1422" s="T192">AA_1914_Girl_flk.047 (003.005) </ta>
            <ta e="T203" id="Seg_1423" s="T199">AA_1914_Girl_flk.048 (003.007)</ta>
            <ta e="T205" id="Seg_1424" s="T203">AA_1914_Girl_flk.049 (003.008)</ta>
            <ta e="T209" id="Seg_1425" s="T205">AA_1914_Girl_flk.050 (003.009)</ta>
            <ta e="T212" id="Seg_1426" s="T209">AA_1914_Girl_flk.051 (003.010)</ta>
            <ta e="T215" id="Seg_1427" s="T212">AA_1914_Girl_flk.052 (003.011)</ta>
            <ta e="T217" id="Seg_1428" s="T215">AA_1914_Girl_flk.053 (003.012)</ta>
            <ta e="T220" id="Seg_1429" s="T217">AA_1914_Girl_flk.054 (003.013)</ta>
            <ta e="T225" id="Seg_1430" s="T220">AA_1914_Girl_flk.055 (003.014)</ta>
            <ta e="T228" id="Seg_1431" s="T225">AA_1914_Girl_flk.056 (003.015)</ta>
            <ta e="T231" id="Seg_1432" s="T228">AA_1914_Girl_flk.057 (003.016)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_1433" s="T0">Büzʼen nagur koʔbdot ibi. </ta>
            <ta e="T9" id="Seg_1434" s="T4">Dĭ büzʼem turanə služit mosʼtə. </ta>
            <ta e="T13" id="Seg_1435" s="T9">Urgo koʔbdot: "Măn kallam." </ta>
            <ta e="T17" id="Seg_1436" s="T13">"No, kalla", büzʼe măndə. </ta>
            <ta e="T23" id="Seg_1437" s="T17">Eʔbdəbə saj băppi, tibij oldʼa šerbi. </ta>
            <ta e="T24" id="Seg_1438" s="T23">Mĭlloʔbdəbi. </ta>
            <ta e="T28" id="Seg_1439" s="T24">Büzʼe tunobi urgaːba molaʔ. </ta>
            <ta e="T31" id="Seg_1440" s="T28">Aʔtʼəgən kallaʔ nobi. </ta>
            <ta e="T36" id="Seg_1441" s="T31">Dĭ koʔbdo parluʔbi, parlaʔ šobi. </ta>
            <ta e="T39" id="Seg_1442" s="T36">Mălia: "Moʔ parbial?" </ta>
            <ta e="T40" id="Seg_1443" s="T39">"Pimbiem. </ta>
            <ta e="T43" id="Seg_1444" s="T40">Urgaːba aʔtʼəgən mĭŋge." </ta>
            <ta e="T48" id="Seg_1445" s="T43">Tʼergə koʔbdot mălia: "Măn kallam." </ta>
            <ta e="T56" id="Seg_1446" s="T48">Bazoʔ dĭ büzʼe urgaːba mobiːza bazoʔ aʔtʼəgən nulaʔbə. </ta>
            <ta e="T59" id="Seg_1447" s="T56">Bazoʔ parlaʔ šobi. </ta>
            <ta e="T61" id="Seg_1448" s="T59">"Moʔ parbial?" </ta>
            <ta e="T66" id="Seg_1449" s="T61">"Urgaːba kubiam, tĭgəttə parlaʔ šobiam." </ta>
            <ta e="T69" id="Seg_1450" s="T66">Üdʼüge koʔbdot mĭllüʔbi. </ta>
            <ta e="T72" id="Seg_1451" s="T69">Tibij oldʼa šerbi. </ta>
            <ta e="T76" id="Seg_1452" s="T72">Dĭ eʔbdəbə saj băppi. </ta>
            <ta e="T77" id="Seg_1453" s="T76">Mĭlloʔbdəbi. </ta>
            <ta e="T82" id="Seg_1454" s="T77">Büzʼe bazoʔ urgaːba molaʔ tunolbi. </ta>
            <ta e="T88" id="Seg_1455" s="T82">Koʔbdot möːzʼəʔ tʼippi, simabə păktəj tʼitlüʔbi. </ta>
            <ta e="T90" id="Seg_1456" s="T88">Bostə mĭllüʔbi. </ta>
            <ta e="T94" id="Seg_1457" s="T90">Kambi, oʔb nükegən üzəbi. </ta>
            <ta e="T98" id="Seg_1458" s="T94">Dĭ nüken nʼit ibi. </ta>
            <ta e="T101" id="Seg_1459" s="T98">Dĭzʼəʔ dĭzeŋ mĭlleʔbəiʔ. </ta>
            <ta e="T106" id="Seg_1460" s="T101">Dĭ nüke nʼigəndə mălia: "Koʔbdo! </ta>
            <ta e="T110" id="Seg_1461" s="T106">Dĭzʼəʔ kaŋgələj sadandəna maʔdə! </ta>
            <ta e="T120" id="Seg_1462" s="T110">Nʼi ibində inen oldʼa kuləj, koʔbdo ibində nen oldʼa kuləj." </ta>
            <ta e="T122" id="Seg_1463" s="T120">Kambijəʔ, šobiiʔ. </ta>
            <ta e="T133" id="Seg_1464" s="T122">Dĭ koʔbdo inen oldʼa kulia, a dĭ nʼi nen oldʼa kulia. </ta>
            <ta e="T137" id="Seg_1465" s="T133">Maʔgəndə šobiːza ijaandə nörbəlie. </ta>
            <ta e="T143" id="Seg_1466" s="T137">Mălia: "Dĭ nʼi, inen oldʼa kulia." </ta>
            <ta e="T147" id="Seg_1467" s="T143">Dĭ nüke mălia: "Koʔbdo!" </ta>
            <ta e="T151" id="Seg_1468" s="T147">Mălia:"Kanaʔ multʼanə, kaŋgaʔ!" </ta>
            <ta e="T153" id="Seg_1469" s="T151">Kambiiʔ multʼanə. </ta>
            <ta e="T155" id="Seg_1470" s="T153">Sabəndə nöməllabaʔbi. </ta>
            <ta e="T156" id="Seg_1471" s="T155">Parbi. </ta>
            <ta e="T163" id="Seg_1472" s="T156">Dĭ koʔbdo ulubə bazəbi, ej săbəjʔ ködəbi. </ta>
            <ta e="T168" id="Seg_1473" s="T163">Da šobi nʼi dĭ piʔdölabaʔbi. </ta>
            <ta e="T173" id="Seg_1474" s="T168">Dĭ nʼi šobiːza ijaandə nörbəlie. </ta>
            <ta e="T177" id="Seg_1475" s="T173">"Nʼi", mălia," ej koʔbdo". </ta>
            <ta e="T182" id="Seg_1476" s="T177">Dĭgəttə dĭ koʔbdo maʔgəndə mĭllaːndəga. </ta>
            <ta e="T184" id="Seg_1477" s="T182">Büm bejbi. </ta>
            <ta e="T192" id="Seg_1478" s="T184">Nüjübə pʼeriet: "Šĭʔ ipekətsʼəʔleʔ kajət măn nüjüm özerlüʔbi." </ta>
            <ta e="T199" id="Seg_1479" s="T192">Dĭ nüke nʼibə kudoliat: "Tăn muʔ šʼaːmbial? </ta>
            <ta e="T203" id="Seg_1480" s="T199">Dĭ koʔbdo, nʼi mămbial. </ta>
            <ta e="T205" id="Seg_1481" s="T203">Mĭllüʔbi maʔgəndə." </ta>
            <ta e="T209" id="Seg_1482" s="T205">Dĭ nüke öʔlüʔbi enəidənəidə. </ta>
            <ta e="T212" id="Seg_1483" s="T209">"Kut dĭm kallaʔ!" </ta>
            <ta e="T215" id="Seg_1484" s="T212">Šaːškan molaʔ nʼergölüʔbi. </ta>
            <ta e="T217" id="Seg_1485" s="T215">Kunolamna bapondə. </ta>
            <ta e="T220" id="Seg_1486" s="T217">Bapotsʼəʔ mĭnžəleʔ oʔbdulbiiʔ. </ta>
            <ta e="T225" id="Seg_1487" s="T220">Dĭ šüʔbdöbi gijen bü bejlemne. </ta>
            <ta e="T228" id="Seg_1488" s="T225">Dĭ tʼügəndə teppiiʔ. </ta>
            <ta e="T231" id="Seg_1489" s="T228">Nʼigəndə ileʔ baʔbiiʔ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1490" s="T0">büzʼe-n</ta>
            <ta e="T2" id="Seg_1491" s="T1">nagur</ta>
            <ta e="T3" id="Seg_1492" s="T2">koʔbdo-t</ta>
            <ta e="T4" id="Seg_1493" s="T3">i-bi</ta>
            <ta e="T5" id="Seg_1494" s="T4">dĭ</ta>
            <ta e="T6" id="Seg_1495" s="T5">büzʼe-m</ta>
            <ta e="T7" id="Seg_1496" s="T6">tura-nə</ta>
            <ta e="T8" id="Seg_1497" s="T7">služit</ta>
            <ta e="T9" id="Seg_1498" s="T8">mo-sʼtə</ta>
            <ta e="T10" id="Seg_1499" s="T9">urgo</ta>
            <ta e="T11" id="Seg_1500" s="T10">koʔbdo-t</ta>
            <ta e="T12" id="Seg_1501" s="T11">măn</ta>
            <ta e="T13" id="Seg_1502" s="T12">kal-la-m</ta>
            <ta e="T14" id="Seg_1503" s="T13">no</ta>
            <ta e="T15" id="Seg_1504" s="T14">kal-la</ta>
            <ta e="T16" id="Seg_1505" s="T15">büzʼe</ta>
            <ta e="T17" id="Seg_1506" s="T16">măn-də</ta>
            <ta e="T18" id="Seg_1507" s="T17">eʔbdə-bə</ta>
            <ta e="T19" id="Seg_1508" s="T18">saj</ta>
            <ta e="T20" id="Seg_1509" s="T19">băp-pi</ta>
            <ta e="T21" id="Seg_1510" s="T20">tibi-j</ta>
            <ta e="T22" id="Seg_1511" s="T21">oldʼa</ta>
            <ta e="T23" id="Seg_1512" s="T22">šer-bi</ta>
            <ta e="T24" id="Seg_1513" s="T23">mĭl-loʔbdə-bi</ta>
            <ta e="T25" id="Seg_1514" s="T24">büzʼe</ta>
            <ta e="T26" id="Seg_1515" s="T25">tuno-bi</ta>
            <ta e="T27" id="Seg_1516" s="T26">urgaːba</ta>
            <ta e="T28" id="Seg_1517" s="T27">mo-laʔ</ta>
            <ta e="T29" id="Seg_1518" s="T28">aʔtʼə-gən</ta>
            <ta e="T30" id="Seg_1519" s="T29">kal-laʔ</ta>
            <ta e="T31" id="Seg_1520" s="T30">no-bi</ta>
            <ta e="T32" id="Seg_1521" s="T31">dĭ</ta>
            <ta e="T33" id="Seg_1522" s="T32">koʔbdo</ta>
            <ta e="T34" id="Seg_1523" s="T33">par-luʔ-bi</ta>
            <ta e="T35" id="Seg_1524" s="T34">par-laʔ</ta>
            <ta e="T36" id="Seg_1525" s="T35">šo-bi</ta>
            <ta e="T37" id="Seg_1526" s="T36">mă-lia</ta>
            <ta e="T38" id="Seg_1527" s="T37">moʔ</ta>
            <ta e="T39" id="Seg_1528" s="T38">par-bia-l</ta>
            <ta e="T40" id="Seg_1529" s="T39">pim-bie-m</ta>
            <ta e="T41" id="Seg_1530" s="T40">urgaːba</ta>
            <ta e="T42" id="Seg_1531" s="T41">aʔtʼə-gən</ta>
            <ta e="T43" id="Seg_1532" s="T42">mĭŋ-ge</ta>
            <ta e="T44" id="Seg_1533" s="T43">tʼergə</ta>
            <ta e="T45" id="Seg_1534" s="T44">koʔbdo-t</ta>
            <ta e="T46" id="Seg_1535" s="T45">mă-lia</ta>
            <ta e="T47" id="Seg_1536" s="T46">măn</ta>
            <ta e="T48" id="Seg_1537" s="T47">kal-la-m</ta>
            <ta e="T49" id="Seg_1538" s="T48">bazoʔ</ta>
            <ta e="T50" id="Seg_1539" s="T49">dĭ</ta>
            <ta e="T51" id="Seg_1540" s="T50">büzʼe</ta>
            <ta e="T52" id="Seg_1541" s="T51">urgaːba</ta>
            <ta e="T53" id="Seg_1542" s="T52">mo-biːza</ta>
            <ta e="T54" id="Seg_1543" s="T53">bazoʔ</ta>
            <ta e="T55" id="Seg_1544" s="T54">aʔtʼə-gən</ta>
            <ta e="T56" id="Seg_1545" s="T55">nu-laʔbə</ta>
            <ta e="T57" id="Seg_1546" s="T56">bazoʔ</ta>
            <ta e="T58" id="Seg_1547" s="T57">par-laʔ</ta>
            <ta e="T59" id="Seg_1548" s="T58">šo-bi</ta>
            <ta e="T60" id="Seg_1549" s="T59">moʔ</ta>
            <ta e="T61" id="Seg_1550" s="T60">par-bia-l</ta>
            <ta e="T62" id="Seg_1551" s="T61">urgaːba</ta>
            <ta e="T63" id="Seg_1552" s="T62">ku-bia-m</ta>
            <ta e="T64" id="Seg_1553" s="T63">tĭgəttə</ta>
            <ta e="T65" id="Seg_1554" s="T64">par-laʔ</ta>
            <ta e="T66" id="Seg_1555" s="T65">šo-bia-m</ta>
            <ta e="T67" id="Seg_1556" s="T66">üdʼüge</ta>
            <ta e="T68" id="Seg_1557" s="T67">koʔbdo-t</ta>
            <ta e="T69" id="Seg_1558" s="T68">mĭl-lüʔ-bi</ta>
            <ta e="T70" id="Seg_1559" s="T69">tibi-j</ta>
            <ta e="T71" id="Seg_1560" s="T70">oldʼa</ta>
            <ta e="T72" id="Seg_1561" s="T71">šer-bi</ta>
            <ta e="T73" id="Seg_1562" s="T72">dĭ</ta>
            <ta e="T74" id="Seg_1563" s="T73">eʔbdə-bə</ta>
            <ta e="T75" id="Seg_1564" s="T74">saj</ta>
            <ta e="T76" id="Seg_1565" s="T75">băp-pi</ta>
            <ta e="T77" id="Seg_1566" s="T76">mĭl-loʔbdə-bi</ta>
            <ta e="T78" id="Seg_1567" s="T77">büzʼe</ta>
            <ta e="T79" id="Seg_1568" s="T78">bazoʔ</ta>
            <ta e="T80" id="Seg_1569" s="T79">urgaːba</ta>
            <ta e="T81" id="Seg_1570" s="T80">mo-laʔ</ta>
            <ta e="T82" id="Seg_1571" s="T81">tuno-l-bi</ta>
            <ta e="T83" id="Seg_1572" s="T82">koʔbdo-t</ta>
            <ta e="T84" id="Seg_1573" s="T83">möː-zʼəʔ</ta>
            <ta e="T85" id="Seg_1574" s="T84">tʼip-pi</ta>
            <ta e="T86" id="Seg_1575" s="T85">sima-bə</ta>
            <ta e="T87" id="Seg_1576" s="T86">păktə-j</ta>
            <ta e="T88" id="Seg_1577" s="T87">tʼit-lüʔ-bi</ta>
            <ta e="T89" id="Seg_1578" s="T88">bos-tə</ta>
            <ta e="T90" id="Seg_1579" s="T89">mĭl-lüʔ-bi</ta>
            <ta e="T91" id="Seg_1580" s="T90">kam-bi</ta>
            <ta e="T92" id="Seg_1581" s="T91">oʔb</ta>
            <ta e="T93" id="Seg_1582" s="T92">nüke-gən</ta>
            <ta e="T94" id="Seg_1583" s="T93">üzə-bi</ta>
            <ta e="T95" id="Seg_1584" s="T94">dĭ</ta>
            <ta e="T96" id="Seg_1585" s="T95">nüke-n</ta>
            <ta e="T97" id="Seg_1586" s="T96">nʼi-t</ta>
            <ta e="T98" id="Seg_1587" s="T97">i-bi</ta>
            <ta e="T99" id="Seg_1588" s="T98">dĭ-zʼəʔ</ta>
            <ta e="T100" id="Seg_1589" s="T99">dĭ-zeŋ</ta>
            <ta e="T101" id="Seg_1590" s="T100">mĭl-leʔbə-iʔ</ta>
            <ta e="T102" id="Seg_1591" s="T101">dĭ</ta>
            <ta e="T103" id="Seg_1592" s="T102">nüke</ta>
            <ta e="T104" id="Seg_1593" s="T103">nʼi-gəndə</ta>
            <ta e="T105" id="Seg_1594" s="T104">mă-lia</ta>
            <ta e="T106" id="Seg_1595" s="T105">koʔbdo</ta>
            <ta e="T107" id="Seg_1596" s="T106">dĭ-zʼəʔ</ta>
            <ta e="T108" id="Seg_1597" s="T107">kaŋ-gə-ləj</ta>
            <ta e="T109" id="Seg_1598" s="T108">sadandə-na</ta>
            <ta e="T110" id="Seg_1599" s="T109">maʔ-də</ta>
            <ta e="T111" id="Seg_1600" s="T110">nʼi</ta>
            <ta e="T112" id="Seg_1601" s="T111">i-bi-ndə</ta>
            <ta e="T113" id="Seg_1602" s="T112">ine-n</ta>
            <ta e="T114" id="Seg_1603" s="T113">oldʼa</ta>
            <ta e="T115" id="Seg_1604" s="T114">ku-lə-j</ta>
            <ta e="T116" id="Seg_1605" s="T115">koʔbdo</ta>
            <ta e="T117" id="Seg_1606" s="T116">i-bi-ndə</ta>
            <ta e="T118" id="Seg_1607" s="T117">ne-n</ta>
            <ta e="T119" id="Seg_1608" s="T118">oldʼa</ta>
            <ta e="T120" id="Seg_1609" s="T119">ku-lə-j</ta>
            <ta e="T121" id="Seg_1610" s="T120">kam-bi-jəʔ</ta>
            <ta e="T122" id="Seg_1611" s="T121">šo-bi-iʔ</ta>
            <ta e="T123" id="Seg_1612" s="T122">dĭ</ta>
            <ta e="T124" id="Seg_1613" s="T123">koʔbdo</ta>
            <ta e="T125" id="Seg_1614" s="T124">ine-n</ta>
            <ta e="T126" id="Seg_1615" s="T125">oldʼa</ta>
            <ta e="T127" id="Seg_1616" s="T126">ku-lia</ta>
            <ta e="T128" id="Seg_1617" s="T127">a</ta>
            <ta e="T129" id="Seg_1618" s="T128">dĭ</ta>
            <ta e="T130" id="Seg_1619" s="T129">nʼi</ta>
            <ta e="T131" id="Seg_1620" s="T130">ne-n</ta>
            <ta e="T132" id="Seg_1621" s="T131">oldʼa</ta>
            <ta e="T133" id="Seg_1622" s="T132">ku-lia</ta>
            <ta e="T134" id="Seg_1623" s="T133">maʔ-gəndə</ta>
            <ta e="T135" id="Seg_1624" s="T134">šo-biːza</ta>
            <ta e="T136" id="Seg_1625" s="T135">ija-andə</ta>
            <ta e="T137" id="Seg_1626" s="T136">nörbə-lie</ta>
            <ta e="T138" id="Seg_1627" s="T137">mă-lia</ta>
            <ta e="T139" id="Seg_1628" s="T138">dĭ</ta>
            <ta e="T140" id="Seg_1629" s="T139">nʼi</ta>
            <ta e="T141" id="Seg_1630" s="T140">ine-n</ta>
            <ta e="T142" id="Seg_1631" s="T141">oldʼa</ta>
            <ta e="T143" id="Seg_1632" s="T142">ku-lia</ta>
            <ta e="T144" id="Seg_1633" s="T143">dĭ</ta>
            <ta e="T145" id="Seg_1634" s="T144">nüke</ta>
            <ta e="T146" id="Seg_1635" s="T145">mă-lia</ta>
            <ta e="T147" id="Seg_1636" s="T146">koʔbdo</ta>
            <ta e="T148" id="Seg_1637" s="T147">mă-lia</ta>
            <ta e="T149" id="Seg_1638" s="T148">kan-a-ʔ</ta>
            <ta e="T150" id="Seg_1639" s="T149">multʼa-nə</ta>
            <ta e="T151" id="Seg_1640" s="T150">kaŋ-gaʔ</ta>
            <ta e="T152" id="Seg_1641" s="T151">kam-bi-iʔ</ta>
            <ta e="T153" id="Seg_1642" s="T152">multʼa-nə</ta>
            <ta e="T154" id="Seg_1643" s="T153">sabən-də</ta>
            <ta e="T155" id="Seg_1644" s="T154">nöməl-labaʔ-bi</ta>
            <ta e="T156" id="Seg_1645" s="T155">par-bi</ta>
            <ta e="T157" id="Seg_1646" s="T156">dĭ</ta>
            <ta e="T158" id="Seg_1647" s="T157">koʔbdo</ta>
            <ta e="T159" id="Seg_1648" s="T158">ulu-bə</ta>
            <ta e="T160" id="Seg_1649" s="T159">bazə-bi</ta>
            <ta e="T161" id="Seg_1650" s="T160">ej</ta>
            <ta e="T162" id="Seg_1651" s="T161">săbəjʔ</ta>
            <ta e="T163" id="Seg_1652" s="T162">ködə-bi</ta>
            <ta e="T164" id="Seg_1653" s="T163">da</ta>
            <ta e="T165" id="Seg_1654" s="T164">šo-bi</ta>
            <ta e="T166" id="Seg_1655" s="T165">nʼi</ta>
            <ta e="T167" id="Seg_1656" s="T166">dĭ</ta>
            <ta e="T168" id="Seg_1657" s="T167">piʔdö-labaʔ-bi</ta>
            <ta e="T169" id="Seg_1658" s="T168">dĭ</ta>
            <ta e="T170" id="Seg_1659" s="T169">nʼi</ta>
            <ta e="T171" id="Seg_1660" s="T170">šo-biːza</ta>
            <ta e="T172" id="Seg_1661" s="T171">ija-andə</ta>
            <ta e="T173" id="Seg_1662" s="T172">nörbə-lie</ta>
            <ta e="T174" id="Seg_1663" s="T173">nʼi</ta>
            <ta e="T175" id="Seg_1664" s="T174">mă-lia</ta>
            <ta e="T176" id="Seg_1665" s="T175">ej</ta>
            <ta e="T177" id="Seg_1666" s="T176">koʔbdo</ta>
            <ta e="T178" id="Seg_1667" s="T177">dĭgəttə</ta>
            <ta e="T179" id="Seg_1668" s="T178">dĭ</ta>
            <ta e="T180" id="Seg_1669" s="T179">koʔbdo</ta>
            <ta e="T181" id="Seg_1670" s="T180">maʔ-gəndə</ta>
            <ta e="T182" id="Seg_1671" s="T181">mĭl-laːndə-ga</ta>
            <ta e="T183" id="Seg_1672" s="T182">bü-m</ta>
            <ta e="T184" id="Seg_1673" s="T183">bej-bi</ta>
            <ta e="T185" id="Seg_1674" s="T184">nüjü-bə</ta>
            <ta e="T186" id="Seg_1675" s="T185">pʼer-ie-t</ta>
            <ta e="T187" id="Seg_1676" s="T186">šĭʔ</ta>
            <ta e="T188" id="Seg_1677" s="T187">ipek-ət-sʼəʔ-leʔ</ta>
            <ta e="T189" id="Seg_1678" s="T188">kajət</ta>
            <ta e="T190" id="Seg_1679" s="T189">măn</ta>
            <ta e="T191" id="Seg_1680" s="T190">nüjü-m</ta>
            <ta e="T192" id="Seg_1681" s="T191">özer-lüʔ-bi</ta>
            <ta e="T193" id="Seg_1682" s="T192">dĭ</ta>
            <ta e="T194" id="Seg_1683" s="T193">nüke</ta>
            <ta e="T195" id="Seg_1684" s="T194">nʼi-bə</ta>
            <ta e="T196" id="Seg_1685" s="T195">kudo-lia-t</ta>
            <ta e="T197" id="Seg_1686" s="T196">tăn</ta>
            <ta e="T198" id="Seg_1687" s="T197">muʔ</ta>
            <ta e="T199" id="Seg_1688" s="T198">šʼaːm-bia-l</ta>
            <ta e="T200" id="Seg_1689" s="T199">dĭ</ta>
            <ta e="T201" id="Seg_1690" s="T200">koʔbdo</ta>
            <ta e="T202" id="Seg_1691" s="T201">nʼi</ta>
            <ta e="T203" id="Seg_1692" s="T202">măm-bia-l</ta>
            <ta e="T204" id="Seg_1693" s="T203">mĭl-lüʔ-bi</ta>
            <ta e="T205" id="Seg_1694" s="T204">maʔ-gəndə</ta>
            <ta e="T206" id="Seg_1695" s="T205">dĭ</ta>
            <ta e="T207" id="Seg_1696" s="T206">nüke</ta>
            <ta e="T208" id="Seg_1697" s="T207">öʔ-lüʔ-bi</ta>
            <ta e="T209" id="Seg_1698" s="T208">enəidənəi-də</ta>
            <ta e="T210" id="Seg_1699" s="T209">ku-t</ta>
            <ta e="T211" id="Seg_1700" s="T210">dĭ-m</ta>
            <ta e="T212" id="Seg_1701" s="T211">kal-laʔ</ta>
            <ta e="T213" id="Seg_1702" s="T212">šaːškan</ta>
            <ta e="T214" id="Seg_1703" s="T213">mo-laʔ</ta>
            <ta e="T215" id="Seg_1704" s="T214">nʼergö-lüʔ-bi</ta>
            <ta e="T216" id="Seg_1705" s="T215">kuno-lamna</ta>
            <ta e="T217" id="Seg_1706" s="T216">bapo-ndə</ta>
            <ta e="T218" id="Seg_1707" s="T217">bapo-t-sʼəʔ</ta>
            <ta e="T219" id="Seg_1708" s="T218">mĭnžə-leʔ</ta>
            <ta e="T220" id="Seg_1709" s="T219">oʔbdul-bi-iʔ</ta>
            <ta e="T221" id="Seg_1710" s="T220">dĭ</ta>
            <ta e="T222" id="Seg_1711" s="T221">šüʔbdö-bi</ta>
            <ta e="T223" id="Seg_1712" s="T222">gijen</ta>
            <ta e="T224" id="Seg_1713" s="T223">bü</ta>
            <ta e="T225" id="Seg_1714" s="T224">bej-lemne</ta>
            <ta e="T226" id="Seg_1715" s="T225">dĭ</ta>
            <ta e="T227" id="Seg_1716" s="T226">tʼü-gəndə</ta>
            <ta e="T228" id="Seg_1717" s="T227">tep-pi-iʔ</ta>
            <ta e="T229" id="Seg_1718" s="T228">nʼi-gəndə</ta>
            <ta e="T230" id="Seg_1719" s="T229">i-leʔ</ta>
            <ta e="T231" id="Seg_1720" s="T230">baʔ-bi-iʔ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1721" s="T0">büzʼe-n</ta>
            <ta e="T2" id="Seg_1722" s="T1">nagur</ta>
            <ta e="T3" id="Seg_1723" s="T2">koʔbdo-t</ta>
            <ta e="T4" id="Seg_1724" s="T3">i-bi</ta>
            <ta e="T5" id="Seg_1725" s="T4">dĭ</ta>
            <ta e="T6" id="Seg_1726" s="T5">büzʼe-m</ta>
            <ta e="T7" id="Seg_1727" s="T6">tura-Tə</ta>
            <ta e="T8" id="Seg_1728" s="T7">služit</ta>
            <ta e="T9" id="Seg_1729" s="T8">mo-zittə</ta>
            <ta e="T10" id="Seg_1730" s="T9">urgo</ta>
            <ta e="T11" id="Seg_1731" s="T10">koʔbdo-t</ta>
            <ta e="T12" id="Seg_1732" s="T11">măn</ta>
            <ta e="T13" id="Seg_1733" s="T12">kan-lV-m</ta>
            <ta e="T14" id="Seg_1734" s="T13">no</ta>
            <ta e="T15" id="Seg_1735" s="T14">kan-lAʔ</ta>
            <ta e="T16" id="Seg_1736" s="T15">büzʼe</ta>
            <ta e="T17" id="Seg_1737" s="T16">măn-ntə</ta>
            <ta e="T18" id="Seg_1738" s="T17">eʔbdə-bə</ta>
            <ta e="T19" id="Seg_1739" s="T18">saj</ta>
            <ta e="T20" id="Seg_1740" s="T19">băt-bi</ta>
            <ta e="T21" id="Seg_1741" s="T20">tibi-j</ta>
            <ta e="T22" id="Seg_1742" s="T21">oldʼa</ta>
            <ta e="T23" id="Seg_1743" s="T22">šer-bi</ta>
            <ta e="T24" id="Seg_1744" s="T23">mĭn-loʔbdə-bi</ta>
            <ta e="T25" id="Seg_1745" s="T24">büzʼe</ta>
            <ta e="T26" id="Seg_1746" s="T25">tuno-bi</ta>
            <ta e="T27" id="Seg_1747" s="T26">urgaːba</ta>
            <ta e="T28" id="Seg_1748" s="T27">mo-lAʔ</ta>
            <ta e="T29" id="Seg_1749" s="T28">aʔtʼi-Kən</ta>
            <ta e="T30" id="Seg_1750" s="T29">kan-lAʔ</ta>
            <ta e="T31" id="Seg_1751" s="T30">nu-bi</ta>
            <ta e="T32" id="Seg_1752" s="T31">dĭ</ta>
            <ta e="T33" id="Seg_1753" s="T32">koʔbdo</ta>
            <ta e="T34" id="Seg_1754" s="T33">par-luʔbdə-bi</ta>
            <ta e="T35" id="Seg_1755" s="T34">par-lAʔ</ta>
            <ta e="T36" id="Seg_1756" s="T35">šo-bi</ta>
            <ta e="T37" id="Seg_1757" s="T36">măn-liA</ta>
            <ta e="T38" id="Seg_1758" s="T37">moʔ</ta>
            <ta e="T39" id="Seg_1759" s="T38">par-bi-l</ta>
            <ta e="T40" id="Seg_1760" s="T39">pim-bi-m</ta>
            <ta e="T41" id="Seg_1761" s="T40">urgaːba</ta>
            <ta e="T42" id="Seg_1762" s="T41">aʔtʼi-Kən</ta>
            <ta e="T43" id="Seg_1763" s="T42">mĭn-gA</ta>
            <ta e="T44" id="Seg_1764" s="T43">tʼergə</ta>
            <ta e="T45" id="Seg_1765" s="T44">koʔbdo-t</ta>
            <ta e="T46" id="Seg_1766" s="T45">măn-liA</ta>
            <ta e="T47" id="Seg_1767" s="T46">măn</ta>
            <ta e="T48" id="Seg_1768" s="T47">kan-lV-m</ta>
            <ta e="T49" id="Seg_1769" s="T48">bazoʔ</ta>
            <ta e="T50" id="Seg_1770" s="T49">dĭ</ta>
            <ta e="T51" id="Seg_1771" s="T50">büzʼe</ta>
            <ta e="T52" id="Seg_1772" s="T51">urgaːba</ta>
            <ta e="T53" id="Seg_1773" s="T52">mo-bizA</ta>
            <ta e="T54" id="Seg_1774" s="T53">bazoʔ</ta>
            <ta e="T55" id="Seg_1775" s="T54">aʔtʼi-Kən</ta>
            <ta e="T56" id="Seg_1776" s="T55">nu-laʔbə</ta>
            <ta e="T57" id="Seg_1777" s="T56">bazoʔ</ta>
            <ta e="T58" id="Seg_1778" s="T57">par-lAʔ</ta>
            <ta e="T59" id="Seg_1779" s="T58">šo-bi</ta>
            <ta e="T60" id="Seg_1780" s="T59">moʔ</ta>
            <ta e="T61" id="Seg_1781" s="T60">par-bi-l</ta>
            <ta e="T62" id="Seg_1782" s="T61">urgaːba</ta>
            <ta e="T63" id="Seg_1783" s="T62">ku-bi-m</ta>
            <ta e="T64" id="Seg_1784" s="T63">dĭgəttə</ta>
            <ta e="T65" id="Seg_1785" s="T64">par-lAʔ</ta>
            <ta e="T66" id="Seg_1786" s="T65">šo-bi-m</ta>
            <ta e="T67" id="Seg_1787" s="T66">üdʼüge</ta>
            <ta e="T68" id="Seg_1788" s="T67">koʔbdo-t</ta>
            <ta e="T69" id="Seg_1789" s="T68">mĭn-luʔbdə-bi</ta>
            <ta e="T70" id="Seg_1790" s="T69">tibi-j</ta>
            <ta e="T71" id="Seg_1791" s="T70">oldʼa</ta>
            <ta e="T72" id="Seg_1792" s="T71">šer-bi</ta>
            <ta e="T73" id="Seg_1793" s="T72">dĭ</ta>
            <ta e="T74" id="Seg_1794" s="T73">eʔbdə-bə</ta>
            <ta e="T75" id="Seg_1795" s="T74">saj</ta>
            <ta e="T76" id="Seg_1796" s="T75">băt-bi</ta>
            <ta e="T77" id="Seg_1797" s="T76">mĭn-loʔbdə-bi</ta>
            <ta e="T78" id="Seg_1798" s="T77">büzʼe</ta>
            <ta e="T79" id="Seg_1799" s="T78">bazoʔ</ta>
            <ta e="T80" id="Seg_1800" s="T79">urgaːba</ta>
            <ta e="T81" id="Seg_1801" s="T80">mo-lAʔ</ta>
            <ta e="T82" id="Seg_1802" s="T81">tuno-l-bi</ta>
            <ta e="T83" id="Seg_1803" s="T82">koʔbdo-t</ta>
            <ta e="T84" id="Seg_1804" s="T83">möː-ziʔ</ta>
            <ta e="T85" id="Seg_1805" s="T84">tʼit-bi</ta>
            <ta e="T86" id="Seg_1806" s="T85">sima-bə</ta>
            <ta e="T87" id="Seg_1807" s="T86">păktə-j</ta>
            <ta e="T88" id="Seg_1808" s="T87">tʼit-luʔbdə-bi</ta>
            <ta e="T89" id="Seg_1809" s="T88">bos-də</ta>
            <ta e="T90" id="Seg_1810" s="T89">mĭn-luʔbdə-bi</ta>
            <ta e="T91" id="Seg_1811" s="T90">kan-bi</ta>
            <ta e="T92" id="Seg_1812" s="T91">oʔb</ta>
            <ta e="T93" id="Seg_1813" s="T92">nüke-Kən</ta>
            <ta e="T94" id="Seg_1814" s="T93">üzə-bi</ta>
            <ta e="T95" id="Seg_1815" s="T94">dĭ</ta>
            <ta e="T96" id="Seg_1816" s="T95">nüke-n</ta>
            <ta e="T97" id="Seg_1817" s="T96">nʼi-t</ta>
            <ta e="T98" id="Seg_1818" s="T97">i-bi</ta>
            <ta e="T99" id="Seg_1819" s="T98">dĭ-ziʔ</ta>
            <ta e="T100" id="Seg_1820" s="T99">dĭ-zAŋ</ta>
            <ta e="T101" id="Seg_1821" s="T100">mĭn-laʔbə-jəʔ</ta>
            <ta e="T102" id="Seg_1822" s="T101">dĭ</ta>
            <ta e="T103" id="Seg_1823" s="T102">nüke</ta>
            <ta e="T104" id="Seg_1824" s="T103">nʼi-gəndə</ta>
            <ta e="T105" id="Seg_1825" s="T104">măn-liA</ta>
            <ta e="T106" id="Seg_1826" s="T105">koʔbdo</ta>
            <ta e="T107" id="Seg_1827" s="T106">dĭ-ziʔ</ta>
            <ta e="T108" id="Seg_1828" s="T107">kan-KV-ləj</ta>
            <ta e="T109" id="Seg_1829" s="T108">sadandə-NTA</ta>
            <ta e="T110" id="Seg_1830" s="T109">maʔ-Tə</ta>
            <ta e="T111" id="Seg_1831" s="T110">nʼi</ta>
            <ta e="T112" id="Seg_1832" s="T111">i-bi-gəndə</ta>
            <ta e="T113" id="Seg_1833" s="T112">ine-n</ta>
            <ta e="T114" id="Seg_1834" s="T113">oldʼa</ta>
            <ta e="T115" id="Seg_1835" s="T114">ku-lV-j</ta>
            <ta e="T116" id="Seg_1836" s="T115">koʔbdo</ta>
            <ta e="T117" id="Seg_1837" s="T116">i-bi-gəndə</ta>
            <ta e="T118" id="Seg_1838" s="T117">ne-n</ta>
            <ta e="T119" id="Seg_1839" s="T118">oldʼa</ta>
            <ta e="T120" id="Seg_1840" s="T119">ku-lV-j</ta>
            <ta e="T121" id="Seg_1841" s="T120">kan-bi-jəʔ</ta>
            <ta e="T122" id="Seg_1842" s="T121">šo-bi-jəʔ</ta>
            <ta e="T123" id="Seg_1843" s="T122">dĭ</ta>
            <ta e="T124" id="Seg_1844" s="T123">koʔbdo</ta>
            <ta e="T125" id="Seg_1845" s="T124">ine-n</ta>
            <ta e="T126" id="Seg_1846" s="T125">oldʼa</ta>
            <ta e="T127" id="Seg_1847" s="T126">ku-liA</ta>
            <ta e="T128" id="Seg_1848" s="T127">a</ta>
            <ta e="T129" id="Seg_1849" s="T128">dĭ</ta>
            <ta e="T130" id="Seg_1850" s="T129">nʼi</ta>
            <ta e="T131" id="Seg_1851" s="T130">ne-n</ta>
            <ta e="T132" id="Seg_1852" s="T131">oldʼa</ta>
            <ta e="T133" id="Seg_1853" s="T132">ku-liA</ta>
            <ta e="T134" id="Seg_1854" s="T133">maʔ-gəndə</ta>
            <ta e="T135" id="Seg_1855" s="T134">šo-bizA</ta>
            <ta e="T136" id="Seg_1856" s="T135">ija-gəndə</ta>
            <ta e="T137" id="Seg_1857" s="T136">nörbə-liA</ta>
            <ta e="T138" id="Seg_1858" s="T137">măn-liA</ta>
            <ta e="T139" id="Seg_1859" s="T138">dĭ</ta>
            <ta e="T140" id="Seg_1860" s="T139">nʼi</ta>
            <ta e="T141" id="Seg_1861" s="T140">ine-n</ta>
            <ta e="T142" id="Seg_1862" s="T141">oldʼa</ta>
            <ta e="T143" id="Seg_1863" s="T142">ku-liA</ta>
            <ta e="T144" id="Seg_1864" s="T143">dĭ</ta>
            <ta e="T145" id="Seg_1865" s="T144">nüke</ta>
            <ta e="T146" id="Seg_1866" s="T145">măn-liA</ta>
            <ta e="T147" id="Seg_1867" s="T146">koʔbdo</ta>
            <ta e="T148" id="Seg_1868" s="T147">măn-liA</ta>
            <ta e="T149" id="Seg_1869" s="T148">kan-ə-ʔ</ta>
            <ta e="T150" id="Seg_1870" s="T149">multʼa-Tə</ta>
            <ta e="T151" id="Seg_1871" s="T150">kan-KAʔ</ta>
            <ta e="T152" id="Seg_1872" s="T151">kan-bi-jəʔ</ta>
            <ta e="T153" id="Seg_1873" s="T152">multʼa-Tə</ta>
            <ta e="T154" id="Seg_1874" s="T153">sabən-də</ta>
            <ta e="T155" id="Seg_1875" s="T154">nöməl-labaʔ-bi</ta>
            <ta e="T156" id="Seg_1876" s="T155">par-bi</ta>
            <ta e="T157" id="Seg_1877" s="T156">dĭ</ta>
            <ta e="T158" id="Seg_1878" s="T157">koʔbdo</ta>
            <ta e="T159" id="Seg_1879" s="T158">ulu-bə</ta>
            <ta e="T160" id="Seg_1880" s="T159">bazə-bi</ta>
            <ta e="T161" id="Seg_1881" s="T160">ej</ta>
            <ta e="T163" id="Seg_1882" s="T162">ködər-bi</ta>
            <ta e="T164" id="Seg_1883" s="T163">da</ta>
            <ta e="T165" id="Seg_1884" s="T164">šo-bi</ta>
            <ta e="T166" id="Seg_1885" s="T165">nʼi</ta>
            <ta e="T167" id="Seg_1886" s="T166">dĭ</ta>
            <ta e="T168" id="Seg_1887" s="T167">piʔdö-labaʔ-bi</ta>
            <ta e="T169" id="Seg_1888" s="T168">dĭ</ta>
            <ta e="T170" id="Seg_1889" s="T169">nʼi</ta>
            <ta e="T171" id="Seg_1890" s="T170">šo-bizA</ta>
            <ta e="T172" id="Seg_1891" s="T171">ija-gəndə</ta>
            <ta e="T173" id="Seg_1892" s="T172">nörbə-liA</ta>
            <ta e="T174" id="Seg_1893" s="T173">nʼi</ta>
            <ta e="T175" id="Seg_1894" s="T174">măn-liA</ta>
            <ta e="T176" id="Seg_1895" s="T175">ej</ta>
            <ta e="T177" id="Seg_1896" s="T176">koʔbdo</ta>
            <ta e="T178" id="Seg_1897" s="T177">dĭgəttə</ta>
            <ta e="T179" id="Seg_1898" s="T178">dĭ</ta>
            <ta e="T180" id="Seg_1899" s="T179">koʔbdo</ta>
            <ta e="T181" id="Seg_1900" s="T180">maʔ-gəndə</ta>
            <ta e="T182" id="Seg_1901" s="T181">mĭn-laːndə-gA</ta>
            <ta e="T183" id="Seg_1902" s="T182">bü-m</ta>
            <ta e="T184" id="Seg_1903" s="T183">bej-bi</ta>
            <ta e="T185" id="Seg_1904" s="T184">nüjü-bə</ta>
            <ta e="T186" id="Seg_1905" s="T185">pʼer-liA-t</ta>
            <ta e="T187" id="Seg_1906" s="T186">šiʔ</ta>
            <ta e="T188" id="Seg_1907" s="T187">ipek-t-ziʔ-lAʔ</ta>
            <ta e="T189" id="Seg_1908" s="T188">kajət</ta>
            <ta e="T190" id="Seg_1909" s="T189">măn</ta>
            <ta e="T191" id="Seg_1910" s="T190">nüjü-m</ta>
            <ta e="T192" id="Seg_1911" s="T191">özer-luʔbdə-bi</ta>
            <ta e="T193" id="Seg_1912" s="T192">dĭ</ta>
            <ta e="T194" id="Seg_1913" s="T193">nüke</ta>
            <ta e="T195" id="Seg_1914" s="T194">nʼi-bə</ta>
            <ta e="T196" id="Seg_1915" s="T195">kudo-liA-t</ta>
            <ta e="T197" id="Seg_1916" s="T196">tăn</ta>
            <ta e="T198" id="Seg_1917" s="T197">moʔ</ta>
            <ta e="T199" id="Seg_1918" s="T198">šʼaːm-bi-l</ta>
            <ta e="T200" id="Seg_1919" s="T199">dĭ</ta>
            <ta e="T201" id="Seg_1920" s="T200">koʔbdo</ta>
            <ta e="T202" id="Seg_1921" s="T201">nʼi</ta>
            <ta e="T203" id="Seg_1922" s="T202">măn-bi-l</ta>
            <ta e="T204" id="Seg_1923" s="T203">mĭn-luʔbdə-bi</ta>
            <ta e="T205" id="Seg_1924" s="T204">maʔ-gəndə</ta>
            <ta e="T206" id="Seg_1925" s="T205">dĭ</ta>
            <ta e="T207" id="Seg_1926" s="T206">nüke</ta>
            <ta e="T208" id="Seg_1927" s="T207">öʔ-luʔbdə-bi</ta>
            <ta e="T209" id="Seg_1928" s="T208">eneidəne-də</ta>
            <ta e="T210" id="Seg_1929" s="T209">ku-t</ta>
            <ta e="T211" id="Seg_1930" s="T210">dĭ-m</ta>
            <ta e="T212" id="Seg_1931" s="T211">kan-lAʔ</ta>
            <ta e="T213" id="Seg_1932" s="T212">šaːškən</ta>
            <ta e="T214" id="Seg_1933" s="T213">mo-lAʔ</ta>
            <ta e="T215" id="Seg_1934" s="T214">nʼergö-luʔbdə-bi</ta>
            <ta e="T216" id="Seg_1935" s="T215">kunol-lamnə</ta>
            <ta e="T217" id="Seg_1936" s="T216">bapo-gəndə</ta>
            <ta e="T218" id="Seg_1937" s="T217">bapo-t-ziʔ</ta>
            <ta e="T219" id="Seg_1938" s="T218">mĭnžə-lAʔ</ta>
            <ta e="T220" id="Seg_1939" s="T219">oʔbdul-bi-jəʔ</ta>
            <ta e="T221" id="Seg_1940" s="T220">dĭ</ta>
            <ta e="T222" id="Seg_1941" s="T221">šüʔbdö-bi</ta>
            <ta e="T223" id="Seg_1942" s="T222">gijen</ta>
            <ta e="T224" id="Seg_1943" s="T223">bü</ta>
            <ta e="T225" id="Seg_1944" s="T224">bej-lamnə</ta>
            <ta e="T226" id="Seg_1945" s="T225">dĭ</ta>
            <ta e="T227" id="Seg_1946" s="T226">tʼü-gəndə</ta>
            <ta e="T228" id="Seg_1947" s="T227">det-bi-jəʔ</ta>
            <ta e="T229" id="Seg_1948" s="T228">nʼi-gəndə</ta>
            <ta e="T230" id="Seg_1949" s="T229">i-lAʔ</ta>
            <ta e="T231" id="Seg_1950" s="T230">baʔbdə-bi-jəʔ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1951" s="T0">man-GEN</ta>
            <ta e="T2" id="Seg_1952" s="T1">three.[NOM.SG]</ta>
            <ta e="T3" id="Seg_1953" s="T2">daughter-NOM/GEN.3SG</ta>
            <ta e="T4" id="Seg_1954" s="T3">be-PST.[3SG]</ta>
            <ta e="T5" id="Seg_1955" s="T4">this.[NOM.SG]</ta>
            <ta e="T6" id="Seg_1956" s="T5">man-ACC</ta>
            <ta e="T7" id="Seg_1957" s="T6">settlement-LAT</ta>
            <ta e="T8" id="Seg_1958" s="T7">serve</ta>
            <ta e="T9" id="Seg_1959" s="T8">become-INF.LAT</ta>
            <ta e="T10" id="Seg_1960" s="T9">big.[NOM.SG]</ta>
            <ta e="T11" id="Seg_1961" s="T10">daughter-NOM/GEN.3SG</ta>
            <ta e="T12" id="Seg_1962" s="T11">I.NOM</ta>
            <ta e="T13" id="Seg_1963" s="T12">go-FUT-1SG</ta>
            <ta e="T14" id="Seg_1964" s="T13">well</ta>
            <ta e="T15" id="Seg_1965" s="T14">go-CVB</ta>
            <ta e="T16" id="Seg_1966" s="T15">man.[NOM.SG]</ta>
            <ta e="T17" id="Seg_1967" s="T16">say-IPFVZ.[3SG]</ta>
            <ta e="T18" id="Seg_1968" s="T17">hair-ACC.3SG</ta>
            <ta e="T19" id="Seg_1969" s="T18">off</ta>
            <ta e="T20" id="Seg_1970" s="T19">cut-PST.[3SG]</ta>
            <ta e="T21" id="Seg_1971" s="T20">man-ADJZ.[NOM.SG]</ta>
            <ta e="T22" id="Seg_1972" s="T21">clothing.[NOM.SG]</ta>
            <ta e="T23" id="Seg_1973" s="T22">dress-PST.[3SG]</ta>
            <ta e="T24" id="Seg_1974" s="T23">go-INCH-PST.[3SG]</ta>
            <ta e="T25" id="Seg_1975" s="T24">man.[NOM.SG]</ta>
            <ta e="T26" id="Seg_1976" s="T25">gallop-PST.[3SG]</ta>
            <ta e="T27" id="Seg_1977" s="T26">bear.[NOM.SG]</ta>
            <ta e="T28" id="Seg_1978" s="T27">become-CVB</ta>
            <ta e="T29" id="Seg_1979" s="T28">road-LOC</ta>
            <ta e="T30" id="Seg_1980" s="T29">go-CVB</ta>
            <ta e="T31" id="Seg_1981" s="T30">stand-PST.[3SG]</ta>
            <ta e="T32" id="Seg_1982" s="T31">this.[NOM.SG]</ta>
            <ta e="T33" id="Seg_1983" s="T32">daughter.[NOM.SG]</ta>
            <ta e="T34" id="Seg_1984" s="T33">return-MOM-PST.[3SG]</ta>
            <ta e="T35" id="Seg_1985" s="T34">return-CVB</ta>
            <ta e="T36" id="Seg_1986" s="T35">come-PST.[3SG]</ta>
            <ta e="T37" id="Seg_1987" s="T36">say-PRS.[3SG]</ta>
            <ta e="T38" id="Seg_1988" s="T37">why</ta>
            <ta e="T39" id="Seg_1989" s="T38">return-PST-2SG</ta>
            <ta e="T40" id="Seg_1990" s="T39">fear-PST-1SG</ta>
            <ta e="T41" id="Seg_1991" s="T40">bear.[NOM.SG]</ta>
            <ta e="T42" id="Seg_1992" s="T41">road-LOC</ta>
            <ta e="T43" id="Seg_1993" s="T42">go-PRS.[3SG]</ta>
            <ta e="T44" id="Seg_1994" s="T43">middle.[NOM.SG]</ta>
            <ta e="T45" id="Seg_1995" s="T44">daughter-NOM/GEN.3SG</ta>
            <ta e="T46" id="Seg_1996" s="T45">say-PRS.[3SG]</ta>
            <ta e="T47" id="Seg_1997" s="T46">I.NOM</ta>
            <ta e="T48" id="Seg_1998" s="T47">go-FUT-1SG</ta>
            <ta e="T49" id="Seg_1999" s="T48">again</ta>
            <ta e="T50" id="Seg_2000" s="T49">this.[NOM.SG]</ta>
            <ta e="T51" id="Seg_2001" s="T50">man.[NOM.SG]</ta>
            <ta e="T52" id="Seg_2002" s="T51">bear.[NOM.SG]</ta>
            <ta e="T53" id="Seg_2003" s="T52">become-CVB.ANT</ta>
            <ta e="T54" id="Seg_2004" s="T53">again</ta>
            <ta e="T55" id="Seg_2005" s="T54">road-LOC</ta>
            <ta e="T56" id="Seg_2006" s="T55">stand-DUR.[3SG]</ta>
            <ta e="T57" id="Seg_2007" s="T56">again</ta>
            <ta e="T58" id="Seg_2008" s="T57">return-CVB</ta>
            <ta e="T59" id="Seg_2009" s="T58">come-PST.[3SG]</ta>
            <ta e="T60" id="Seg_2010" s="T59">why</ta>
            <ta e="T61" id="Seg_2011" s="T60">return-PST-2SG</ta>
            <ta e="T62" id="Seg_2012" s="T61">bear.[NOM.SG]</ta>
            <ta e="T63" id="Seg_2013" s="T62">see-PST-1SG</ta>
            <ta e="T64" id="Seg_2014" s="T63">then</ta>
            <ta e="T65" id="Seg_2015" s="T64">return-CVB</ta>
            <ta e="T66" id="Seg_2016" s="T65">come-PST-1SG</ta>
            <ta e="T67" id="Seg_2017" s="T66">small.[NOM.SG]</ta>
            <ta e="T68" id="Seg_2018" s="T67">daughter-NOM/GEN.3SG</ta>
            <ta e="T69" id="Seg_2019" s="T68">go-MOM-PST.[3SG]</ta>
            <ta e="T70" id="Seg_2020" s="T69">man-ADJZ.[NOM.SG]</ta>
            <ta e="T71" id="Seg_2021" s="T70">clothing.[NOM.SG]</ta>
            <ta e="T72" id="Seg_2022" s="T71">dress-PST.[3SG]</ta>
            <ta e="T73" id="Seg_2023" s="T72">this.[NOM.SG]</ta>
            <ta e="T74" id="Seg_2024" s="T73">hair-ACC.3SG</ta>
            <ta e="T75" id="Seg_2025" s="T74">off</ta>
            <ta e="T76" id="Seg_2026" s="T75">cut-PST.[3SG]</ta>
            <ta e="T77" id="Seg_2027" s="T76">go-INCH-PST.[3SG]</ta>
            <ta e="T78" id="Seg_2028" s="T77">man.[NOM.SG]</ta>
            <ta e="T79" id="Seg_2029" s="T78">again</ta>
            <ta e="T80" id="Seg_2030" s="T79">bear.[NOM.SG]</ta>
            <ta e="T81" id="Seg_2031" s="T80">become-CVB</ta>
            <ta e="T82" id="Seg_2032" s="T81">gallop-FRQ-PST.[3SG]</ta>
            <ta e="T83" id="Seg_2033" s="T82">daughter-NOM/GEN.3SG</ta>
            <ta e="T84" id="Seg_2034" s="T83">arrow-INS</ta>
            <ta e="T85" id="Seg_2035" s="T84">shoot-PST.[3SG]</ta>
            <ta e="T86" id="Seg_2036" s="T85">eye-ACC.3SG</ta>
            <ta e="T87" id="Seg_2037" s="T86">burst-CVB</ta>
            <ta e="T88" id="Seg_2038" s="T87">shoot-MOM-PST.[3SG]</ta>
            <ta e="T89" id="Seg_2039" s="T88">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T90" id="Seg_2040" s="T89">go-MOM-PST.[3SG]</ta>
            <ta e="T91" id="Seg_2041" s="T90">go-PST.[3SG]</ta>
            <ta e="T92" id="Seg_2042" s="T91">one.[NOM.SG]</ta>
            <ta e="T93" id="Seg_2043" s="T92">woman-LOC</ta>
            <ta e="T94" id="Seg_2044" s="T93">descend-PST.[3SG]</ta>
            <ta e="T95" id="Seg_2045" s="T94">this.[NOM.SG]</ta>
            <ta e="T96" id="Seg_2046" s="T95">woman-GEN</ta>
            <ta e="T97" id="Seg_2047" s="T96">son-NOM/GEN.3SG</ta>
            <ta e="T98" id="Seg_2048" s="T97">be-PST.[3SG]</ta>
            <ta e="T99" id="Seg_2049" s="T98">this-INS</ta>
            <ta e="T100" id="Seg_2050" s="T99">this-PL</ta>
            <ta e="T101" id="Seg_2051" s="T100">nomadize-DUR-3PL</ta>
            <ta e="T102" id="Seg_2052" s="T101">this.[NOM.SG]</ta>
            <ta e="T103" id="Seg_2053" s="T102">woman.[NOM.SG]</ta>
            <ta e="T104" id="Seg_2054" s="T103">son-LAT/LOC.3SG</ta>
            <ta e="T105" id="Seg_2055" s="T104">say-PRS.[3SG]</ta>
            <ta e="T106" id="Seg_2056" s="T105">girl.[NOM.SG]</ta>
            <ta e="T107" id="Seg_2057" s="T106">this-INS</ta>
            <ta e="T108" id="Seg_2058" s="T107">go-IMP-2DU</ta>
            <ta e="T109" id="Seg_2059" s="T108">trade-PTCP</ta>
            <ta e="T110" id="Seg_2060" s="T109">tent-LAT</ta>
            <ta e="T111" id="Seg_2061" s="T110">boy.[NOM.SG]</ta>
            <ta e="T112" id="Seg_2062" s="T111">be-CVB.TEMP-LAT/LOC.3SG</ta>
            <ta e="T113" id="Seg_2063" s="T112">horse-GEN</ta>
            <ta e="T114" id="Seg_2064" s="T113">clothing.[NOM.SG]</ta>
            <ta e="T115" id="Seg_2065" s="T114">see-FUT-3SG</ta>
            <ta e="T116" id="Seg_2066" s="T115">girl.[NOM.SG]</ta>
            <ta e="T117" id="Seg_2067" s="T116">be-CVB.TEMP-LAT/LOC.3SG</ta>
            <ta e="T118" id="Seg_2068" s="T117">woman-GEN</ta>
            <ta e="T119" id="Seg_2069" s="T118">clothing.[NOM.SG]</ta>
            <ta e="T120" id="Seg_2070" s="T119">see-FUT-3SG</ta>
            <ta e="T121" id="Seg_2071" s="T120">go-PST-3PL</ta>
            <ta e="T122" id="Seg_2072" s="T121">come-PST-3PL</ta>
            <ta e="T123" id="Seg_2073" s="T122">this.[NOM.SG]</ta>
            <ta e="T124" id="Seg_2074" s="T123">girl.[NOM.SG]</ta>
            <ta e="T125" id="Seg_2075" s="T124">horse-GEN</ta>
            <ta e="T126" id="Seg_2076" s="T125">clothing.[NOM.SG]</ta>
            <ta e="T127" id="Seg_2077" s="T126">see-PRS.[3SG]</ta>
            <ta e="T128" id="Seg_2078" s="T127">and</ta>
            <ta e="T129" id="Seg_2079" s="T128">this.[NOM.SG]</ta>
            <ta e="T130" id="Seg_2080" s="T129">boy.[NOM.SG]</ta>
            <ta e="T131" id="Seg_2081" s="T130">woman-GEN</ta>
            <ta e="T132" id="Seg_2082" s="T131">clothing.[NOM.SG]</ta>
            <ta e="T133" id="Seg_2083" s="T132">see-PRS.[3SG]</ta>
            <ta e="T134" id="Seg_2084" s="T133">tent-LAT/LOC.3SG</ta>
            <ta e="T135" id="Seg_2085" s="T134">come-CVB.ANT</ta>
            <ta e="T136" id="Seg_2086" s="T135">mother-LAT/LOC.3SG</ta>
            <ta e="T137" id="Seg_2087" s="T136">tell-PRS.[3SG]</ta>
            <ta e="T138" id="Seg_2088" s="T137">say-PRS.[3SG]</ta>
            <ta e="T139" id="Seg_2089" s="T138">this.[NOM.SG]</ta>
            <ta e="T140" id="Seg_2090" s="T139">boy.[NOM.SG]</ta>
            <ta e="T141" id="Seg_2091" s="T140">horse-GEN</ta>
            <ta e="T142" id="Seg_2092" s="T141">clothing.[NOM.SG]</ta>
            <ta e="T143" id="Seg_2093" s="T142">see-PRS.[3SG]</ta>
            <ta e="T144" id="Seg_2094" s="T143">this.[NOM.SG]</ta>
            <ta e="T145" id="Seg_2095" s="T144">woman.[NOM.SG]</ta>
            <ta e="T146" id="Seg_2096" s="T145">say-PRS.[3SG]</ta>
            <ta e="T147" id="Seg_2097" s="T146">girl.[NOM.SG]</ta>
            <ta e="T148" id="Seg_2098" s="T147">say-PRS.[3SG]</ta>
            <ta e="T149" id="Seg_2099" s="T148">go-EP-IMP.2SG</ta>
            <ta e="T150" id="Seg_2100" s="T149">sauna-LAT</ta>
            <ta e="T151" id="Seg_2101" s="T150">go-IMP.2PL</ta>
            <ta e="T152" id="Seg_2102" s="T151">go-PST-3PL</ta>
            <ta e="T153" id="Seg_2103" s="T152">sauna-LAT</ta>
            <ta e="T154" id="Seg_2104" s="T153">soap-NOM/GEN/ACC.3SG</ta>
            <ta e="T155" id="Seg_2105" s="T154">forget-RES-PST.[3SG]</ta>
            <ta e="T156" id="Seg_2106" s="T155">return-PST.[3SG]</ta>
            <ta e="T157" id="Seg_2107" s="T156">this.[NOM.SG]</ta>
            <ta e="T158" id="Seg_2108" s="T157">girl.[NOM.SG]</ta>
            <ta e="T159" id="Seg_2109" s="T158">head-ACC.3SG</ta>
            <ta e="T160" id="Seg_2110" s="T159">wash-PST.[3SG]</ta>
            <ta e="T161" id="Seg_2111" s="T160">NEG</ta>
            <ta e="T163" id="Seg_2112" s="T162">untie-PST.[3SG]</ta>
            <ta e="T164" id="Seg_2113" s="T163">and</ta>
            <ta e="T165" id="Seg_2114" s="T164">come-PST.[3SG]</ta>
            <ta e="T166" id="Seg_2115" s="T165">boy.[NOM.SG]</ta>
            <ta e="T167" id="Seg_2116" s="T166">this.[NOM.SG]</ta>
            <ta e="T168" id="Seg_2117" s="T167">wash.oneself-RES-PST.[3SG]</ta>
            <ta e="T169" id="Seg_2118" s="T168">this.[NOM.SG]</ta>
            <ta e="T170" id="Seg_2119" s="T169">boy.[NOM.SG]</ta>
            <ta e="T171" id="Seg_2120" s="T170">come-CVB.ANT</ta>
            <ta e="T172" id="Seg_2121" s="T171">mother-LAT/LOC.3SG</ta>
            <ta e="T173" id="Seg_2122" s="T172">tell-PRS.[3SG]</ta>
            <ta e="T174" id="Seg_2123" s="T173">boy.[NOM.SG]</ta>
            <ta e="T175" id="Seg_2124" s="T174">say-PRS.[3SG]</ta>
            <ta e="T176" id="Seg_2125" s="T175">NEG</ta>
            <ta e="T177" id="Seg_2126" s="T176">girl.[NOM.SG]</ta>
            <ta e="T178" id="Seg_2127" s="T177">then</ta>
            <ta e="T179" id="Seg_2128" s="T178">this.[NOM.SG]</ta>
            <ta e="T180" id="Seg_2129" s="T179">girl.[NOM.SG]</ta>
            <ta e="T181" id="Seg_2130" s="T180">tent-LAT/LOC.3SG</ta>
            <ta e="T182" id="Seg_2131" s="T181">go-DUR-PRS.[3SG]</ta>
            <ta e="T183" id="Seg_2132" s="T182">water-ACC</ta>
            <ta e="T184" id="Seg_2133" s="T183">cross-PST.[3SG]</ta>
            <ta e="T185" id="Seg_2134" s="T184">breast-ACC.3SG</ta>
            <ta e="T186" id="Seg_2135" s="T185">show-PRS-3SG.O</ta>
            <ta e="T187" id="Seg_2136" s="T186">you.PL.GEN</ta>
            <ta e="T188" id="Seg_2137" s="T187">bread-3SG-INS-2PL</ta>
            <ta e="T189" id="Seg_2138" s="T188">how</ta>
            <ta e="T190" id="Seg_2139" s="T189">I.GEN</ta>
            <ta e="T191" id="Seg_2140" s="T190">breast-NOM/GEN/ACC.1SG</ta>
            <ta e="T192" id="Seg_2141" s="T191">grow-MOM-PST.[3SG]</ta>
            <ta e="T193" id="Seg_2142" s="T192">this.[NOM.SG]</ta>
            <ta e="T194" id="Seg_2143" s="T193">woman.[NOM.SG]</ta>
            <ta e="T195" id="Seg_2144" s="T194">son-ACC.3SG</ta>
            <ta e="T196" id="Seg_2145" s="T195">scold-PRS-3SG.O</ta>
            <ta e="T197" id="Seg_2146" s="T196">you.NOM</ta>
            <ta e="T198" id="Seg_2147" s="T197">why</ta>
            <ta e="T199" id="Seg_2148" s="T198">lie-PST-2SG</ta>
            <ta e="T200" id="Seg_2149" s="T199">this.[NOM.SG]</ta>
            <ta e="T201" id="Seg_2150" s="T200">girl.[NOM.SG]</ta>
            <ta e="T202" id="Seg_2151" s="T201">boy.[NOM.SG]</ta>
            <ta e="T203" id="Seg_2152" s="T202">say-PST-2SG</ta>
            <ta e="T204" id="Seg_2153" s="T203">go-MOM-PST.[3SG]</ta>
            <ta e="T205" id="Seg_2154" s="T204">tent-LAT/LOC.3SG</ta>
            <ta e="T206" id="Seg_2155" s="T205">this.[NOM.SG]</ta>
            <ta e="T207" id="Seg_2156" s="T206">woman.[NOM.SG]</ta>
            <ta e="T208" id="Seg_2157" s="T207">send-MOM-PST.[3SG]</ta>
            <ta e="T209" id="Seg_2158" s="T208">Eneidene-NOM/GEN/ACC.3SG</ta>
            <ta e="T210" id="Seg_2159" s="T209">find-IMP.2SG.O</ta>
            <ta e="T211" id="Seg_2160" s="T210">this-ACC</ta>
            <ta e="T212" id="Seg_2161" s="T211">go-CVB</ta>
            <ta e="T213" id="Seg_2162" s="T212">magpie</ta>
            <ta e="T214" id="Seg_2163" s="T213">become-CVB</ta>
            <ta e="T215" id="Seg_2164" s="T214">fly-MOM-PST.[3SG]</ta>
            <ta e="T216" id="Seg_2165" s="T215">sleep-DUR.[3SG]</ta>
            <ta e="T217" id="Seg_2166" s="T216">bed-LAT/LOC.3SG</ta>
            <ta e="T218" id="Seg_2167" s="T217">bed-3SG-INS</ta>
            <ta e="T219" id="Seg_2168" s="T218">lift-CVB</ta>
            <ta e="T220" id="Seg_2169" s="T219">pick.up-PST-3PL</ta>
            <ta e="T221" id="Seg_2170" s="T220">this.[NOM.SG]</ta>
            <ta e="T222" id="Seg_2171" s="T221">wake.up-PST.[3SG]</ta>
            <ta e="T223" id="Seg_2172" s="T222">where</ta>
            <ta e="T224" id="Seg_2173" s="T223">river.[NOM.SG]</ta>
            <ta e="T225" id="Seg_2174" s="T224">cross-DUR.[3SG]</ta>
            <ta e="T226" id="Seg_2175" s="T225">this.[NOM.SG]</ta>
            <ta e="T227" id="Seg_2176" s="T226">land-LAT/LOC.3SG</ta>
            <ta e="T228" id="Seg_2177" s="T227">bring-PST-3PL</ta>
            <ta e="T229" id="Seg_2178" s="T228">son-LAT/LOC.3SG</ta>
            <ta e="T230" id="Seg_2179" s="T229">take-CVB</ta>
            <ta e="T231" id="Seg_2180" s="T230">throw-PST-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_2181" s="T0">мужчина-GEN</ta>
            <ta e="T2" id="Seg_2182" s="T1">три.[NOM.SG]</ta>
            <ta e="T3" id="Seg_2183" s="T2">дочь-NOM/GEN.3SG</ta>
            <ta e="T4" id="Seg_2184" s="T3">быть-PST.[3SG]</ta>
            <ta e="T5" id="Seg_2185" s="T4">этот.[NOM.SG]</ta>
            <ta e="T6" id="Seg_2186" s="T5">мужчина-ACC</ta>
            <ta e="T7" id="Seg_2187" s="T6">поселение-LAT</ta>
            <ta e="T8" id="Seg_2188" s="T7">служить</ta>
            <ta e="T9" id="Seg_2189" s="T8">стать-INF.LAT</ta>
            <ta e="T10" id="Seg_2190" s="T9">большой.[NOM.SG]</ta>
            <ta e="T11" id="Seg_2191" s="T10">дочь-NOM/GEN.3SG</ta>
            <ta e="T12" id="Seg_2192" s="T11">я.NOM</ta>
            <ta e="T13" id="Seg_2193" s="T12">пойти-FUT-1SG</ta>
            <ta e="T14" id="Seg_2194" s="T13">ну</ta>
            <ta e="T15" id="Seg_2195" s="T14">пойти-CVB</ta>
            <ta e="T16" id="Seg_2196" s="T15">мужчина.[NOM.SG]</ta>
            <ta e="T17" id="Seg_2197" s="T16">сказать-IPFVZ.[3SG]</ta>
            <ta e="T18" id="Seg_2198" s="T17">волосы-ACC.3SG</ta>
            <ta e="T19" id="Seg_2199" s="T18">от</ta>
            <ta e="T20" id="Seg_2200" s="T19">резать-PST.[3SG]</ta>
            <ta e="T21" id="Seg_2201" s="T20">мужчина-ADJZ.[NOM.SG]</ta>
            <ta e="T22" id="Seg_2202" s="T21">одежда.[NOM.SG]</ta>
            <ta e="T23" id="Seg_2203" s="T22">надеть-PST.[3SG]</ta>
            <ta e="T24" id="Seg_2204" s="T23">идти-INCH-PST.[3SG]</ta>
            <ta e="T25" id="Seg_2205" s="T24">мужчина.[NOM.SG]</ta>
            <ta e="T26" id="Seg_2206" s="T25">скакать-PST.[3SG]</ta>
            <ta e="T27" id="Seg_2207" s="T26">медведь.[NOM.SG]</ta>
            <ta e="T28" id="Seg_2208" s="T27">стать-CVB</ta>
            <ta e="T29" id="Seg_2209" s="T28">дорога-LOC</ta>
            <ta e="T30" id="Seg_2210" s="T29">пойти-CVB</ta>
            <ta e="T31" id="Seg_2211" s="T30">стоять-PST.[3SG]</ta>
            <ta e="T32" id="Seg_2212" s="T31">этот.[NOM.SG]</ta>
            <ta e="T33" id="Seg_2213" s="T32">дочь.[NOM.SG]</ta>
            <ta e="T34" id="Seg_2214" s="T33">вернуться-MOM-PST.[3SG]</ta>
            <ta e="T35" id="Seg_2215" s="T34">вернуться-CVB</ta>
            <ta e="T36" id="Seg_2216" s="T35">прийти-PST.[3SG]</ta>
            <ta e="T37" id="Seg_2217" s="T36">сказать-PRS.[3SG]</ta>
            <ta e="T38" id="Seg_2218" s="T37">почему</ta>
            <ta e="T39" id="Seg_2219" s="T38">вернуться-PST-2SG</ta>
            <ta e="T40" id="Seg_2220" s="T39">бояться-PST-1SG</ta>
            <ta e="T41" id="Seg_2221" s="T40">медведь.[NOM.SG]</ta>
            <ta e="T42" id="Seg_2222" s="T41">дорога-LOC</ta>
            <ta e="T43" id="Seg_2223" s="T42">идти-PRS.[3SG]</ta>
            <ta e="T44" id="Seg_2224" s="T43">середина.[NOM.SG]</ta>
            <ta e="T45" id="Seg_2225" s="T44">дочь-NOM/GEN.3SG</ta>
            <ta e="T46" id="Seg_2226" s="T45">сказать-PRS.[3SG]</ta>
            <ta e="T47" id="Seg_2227" s="T46">я.NOM</ta>
            <ta e="T48" id="Seg_2228" s="T47">пойти-FUT-1SG</ta>
            <ta e="T49" id="Seg_2229" s="T48">опять</ta>
            <ta e="T50" id="Seg_2230" s="T49">этот.[NOM.SG]</ta>
            <ta e="T51" id="Seg_2231" s="T50">мужчина.[NOM.SG]</ta>
            <ta e="T52" id="Seg_2232" s="T51">медведь.[NOM.SG]</ta>
            <ta e="T53" id="Seg_2233" s="T52">стать-CVB.ANT</ta>
            <ta e="T54" id="Seg_2234" s="T53">опять</ta>
            <ta e="T55" id="Seg_2235" s="T54">дорога-LOC</ta>
            <ta e="T56" id="Seg_2236" s="T55">стоять-DUR.[3SG]</ta>
            <ta e="T57" id="Seg_2237" s="T56">опять</ta>
            <ta e="T58" id="Seg_2238" s="T57">вернуться-CVB</ta>
            <ta e="T59" id="Seg_2239" s="T58">прийти-PST.[3SG]</ta>
            <ta e="T60" id="Seg_2240" s="T59">почему</ta>
            <ta e="T61" id="Seg_2241" s="T60">вернуться-PST-2SG</ta>
            <ta e="T62" id="Seg_2242" s="T61">медведь.[NOM.SG]</ta>
            <ta e="T63" id="Seg_2243" s="T62">видеть-PST-1SG</ta>
            <ta e="T64" id="Seg_2244" s="T63">тогда</ta>
            <ta e="T65" id="Seg_2245" s="T64">вернуться-CVB</ta>
            <ta e="T66" id="Seg_2246" s="T65">прийти-PST-1SG</ta>
            <ta e="T67" id="Seg_2247" s="T66">маленький.[NOM.SG]</ta>
            <ta e="T68" id="Seg_2248" s="T67">дочь-NOM/GEN.3SG</ta>
            <ta e="T69" id="Seg_2249" s="T68">идти-MOM-PST.[3SG]</ta>
            <ta e="T70" id="Seg_2250" s="T69">мужчина-ADJZ.[NOM.SG]</ta>
            <ta e="T71" id="Seg_2251" s="T70">одежда.[NOM.SG]</ta>
            <ta e="T72" id="Seg_2252" s="T71">надеть-PST.[3SG]</ta>
            <ta e="T73" id="Seg_2253" s="T72">этот.[NOM.SG]</ta>
            <ta e="T74" id="Seg_2254" s="T73">волосы-ACC.3SG</ta>
            <ta e="T75" id="Seg_2255" s="T74">от</ta>
            <ta e="T76" id="Seg_2256" s="T75">резать-PST.[3SG]</ta>
            <ta e="T77" id="Seg_2257" s="T76">идти-INCH-PST.[3SG]</ta>
            <ta e="T78" id="Seg_2258" s="T77">мужчина.[NOM.SG]</ta>
            <ta e="T79" id="Seg_2259" s="T78">опять</ta>
            <ta e="T80" id="Seg_2260" s="T79">медведь.[NOM.SG]</ta>
            <ta e="T81" id="Seg_2261" s="T80">стать-CVB</ta>
            <ta e="T82" id="Seg_2262" s="T81">скакать-FRQ-PST.[3SG]</ta>
            <ta e="T83" id="Seg_2263" s="T82">дочь-NOM/GEN.3SG</ta>
            <ta e="T84" id="Seg_2264" s="T83">стрела-INS</ta>
            <ta e="T85" id="Seg_2265" s="T84">стрелять-PST.[3SG]</ta>
            <ta e="T86" id="Seg_2266" s="T85">глаз-ACC.3SG</ta>
            <ta e="T87" id="Seg_2267" s="T86">лопнуть-CVB</ta>
            <ta e="T88" id="Seg_2268" s="T87">стрелять-MOM-PST.[3SG]</ta>
            <ta e="T89" id="Seg_2269" s="T88">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T90" id="Seg_2270" s="T89">идти-MOM-PST.[3SG]</ta>
            <ta e="T91" id="Seg_2271" s="T90">пойти-PST.[3SG]</ta>
            <ta e="T92" id="Seg_2272" s="T91">один.[NOM.SG]</ta>
            <ta e="T93" id="Seg_2273" s="T92">женщина-LOC</ta>
            <ta e="T94" id="Seg_2274" s="T93">спуститься-PST.[3SG]</ta>
            <ta e="T95" id="Seg_2275" s="T94">этот.[NOM.SG]</ta>
            <ta e="T96" id="Seg_2276" s="T95">женщина-GEN</ta>
            <ta e="T97" id="Seg_2277" s="T96">сын-NOM/GEN.3SG</ta>
            <ta e="T98" id="Seg_2278" s="T97">быть-PST.[3SG]</ta>
            <ta e="T99" id="Seg_2279" s="T98">этот-INS</ta>
            <ta e="T100" id="Seg_2280" s="T99">этот-PL</ta>
            <ta e="T101" id="Seg_2281" s="T100">кочевать-DUR-3PL</ta>
            <ta e="T102" id="Seg_2282" s="T101">этот.[NOM.SG]</ta>
            <ta e="T103" id="Seg_2283" s="T102">женщина.[NOM.SG]</ta>
            <ta e="T104" id="Seg_2284" s="T103">сын-LAT/LOC.3SG</ta>
            <ta e="T105" id="Seg_2285" s="T104">сказать-PRS.[3SG]</ta>
            <ta e="T106" id="Seg_2286" s="T105">девушка.[NOM.SG]</ta>
            <ta e="T107" id="Seg_2287" s="T106">этот-INS</ta>
            <ta e="T108" id="Seg_2288" s="T107">пойти-IMP-2DU</ta>
            <ta e="T109" id="Seg_2289" s="T108">торговать-PTCP</ta>
            <ta e="T110" id="Seg_2290" s="T109">чум-LAT</ta>
            <ta e="T111" id="Seg_2291" s="T110">мальчик.[NOM.SG]</ta>
            <ta e="T112" id="Seg_2292" s="T111">быть-CVB.TEMP-LAT/LOC.3SG</ta>
            <ta e="T113" id="Seg_2293" s="T112">лошадь-GEN</ta>
            <ta e="T114" id="Seg_2294" s="T113">одежда.[NOM.SG]</ta>
            <ta e="T115" id="Seg_2295" s="T114">проверить-FUT-3SG</ta>
            <ta e="T116" id="Seg_2296" s="T115">девушка.[NOM.SG]</ta>
            <ta e="T117" id="Seg_2297" s="T116">быть-CVB.TEMP-LAT/LOC.3SG</ta>
            <ta e="T118" id="Seg_2298" s="T117">женщина-GEN</ta>
            <ta e="T119" id="Seg_2299" s="T118">одежда.[NOM.SG]</ta>
            <ta e="T120" id="Seg_2300" s="T119">проверить-FUT-3SG</ta>
            <ta e="T121" id="Seg_2301" s="T120">пойти-PST-3PL</ta>
            <ta e="T122" id="Seg_2302" s="T121">прийти-PST-3PL</ta>
            <ta e="T123" id="Seg_2303" s="T122">этот.[NOM.SG]</ta>
            <ta e="T124" id="Seg_2304" s="T123">девушка.[NOM.SG]</ta>
            <ta e="T125" id="Seg_2305" s="T124">лошадь-GEN</ta>
            <ta e="T126" id="Seg_2306" s="T125">одежда.[NOM.SG]</ta>
            <ta e="T127" id="Seg_2307" s="T126">проверить-PRS.[3SG]</ta>
            <ta e="T128" id="Seg_2308" s="T127">а</ta>
            <ta e="T129" id="Seg_2309" s="T128">этот.[NOM.SG]</ta>
            <ta e="T130" id="Seg_2310" s="T129">мальчик.[NOM.SG]</ta>
            <ta e="T131" id="Seg_2311" s="T130">женщина-GEN</ta>
            <ta e="T132" id="Seg_2312" s="T131">одежда.[NOM.SG]</ta>
            <ta e="T133" id="Seg_2313" s="T132">проверить-PRS.[3SG]</ta>
            <ta e="T134" id="Seg_2314" s="T133">чум-LAT/LOC.3SG</ta>
            <ta e="T135" id="Seg_2315" s="T134">прийти-CVB.ANT</ta>
            <ta e="T136" id="Seg_2316" s="T135">мать-LAT/LOC.3SG</ta>
            <ta e="T137" id="Seg_2317" s="T136">сказать-PRS.[3SG]</ta>
            <ta e="T138" id="Seg_2318" s="T137">сказать-PRS.[3SG]</ta>
            <ta e="T139" id="Seg_2319" s="T138">этот.[NOM.SG]</ta>
            <ta e="T140" id="Seg_2320" s="T139">мальчик.[NOM.SG]</ta>
            <ta e="T141" id="Seg_2321" s="T140">лошадь-GEN</ta>
            <ta e="T142" id="Seg_2322" s="T141">одежда.[NOM.SG]</ta>
            <ta e="T143" id="Seg_2323" s="T142">проверить-PRS.[3SG]</ta>
            <ta e="T144" id="Seg_2324" s="T143">этот.[NOM.SG]</ta>
            <ta e="T145" id="Seg_2325" s="T144">женщина.[NOM.SG]</ta>
            <ta e="T146" id="Seg_2326" s="T145">сказать-PRS.[3SG]</ta>
            <ta e="T147" id="Seg_2327" s="T146">девушка.[NOM.SG]</ta>
            <ta e="T148" id="Seg_2328" s="T147">сказать-PRS.[3SG]</ta>
            <ta e="T149" id="Seg_2329" s="T148">пойти-EP-IMP.2SG</ta>
            <ta e="T150" id="Seg_2330" s="T149">баня-LAT</ta>
            <ta e="T151" id="Seg_2331" s="T150">пойти-IMP.2PL</ta>
            <ta e="T152" id="Seg_2332" s="T151">пойти-PST-3PL</ta>
            <ta e="T153" id="Seg_2333" s="T152">баня-LAT</ta>
            <ta e="T154" id="Seg_2334" s="T153">мыло-NOM/GEN/ACC.3SG</ta>
            <ta e="T155" id="Seg_2335" s="T154">забыть-RES-PST.[3SG]</ta>
            <ta e="T156" id="Seg_2336" s="T155">вернуться-PST.[3SG]</ta>
            <ta e="T157" id="Seg_2337" s="T156">этот.[NOM.SG]</ta>
            <ta e="T158" id="Seg_2338" s="T157">девушка.[NOM.SG]</ta>
            <ta e="T159" id="Seg_2339" s="T158">голова-ACC.3SG</ta>
            <ta e="T160" id="Seg_2340" s="T159">мыть-PST.[3SG]</ta>
            <ta e="T161" id="Seg_2341" s="T160">NEG</ta>
            <ta e="T163" id="Seg_2342" s="T162">развязать-PST.[3SG]</ta>
            <ta e="T164" id="Seg_2343" s="T163">и</ta>
            <ta e="T165" id="Seg_2344" s="T164">прийти-PST.[3SG]</ta>
            <ta e="T166" id="Seg_2345" s="T165">мальчик.[NOM.SG]</ta>
            <ta e="T167" id="Seg_2346" s="T166">этот.[NOM.SG]</ta>
            <ta e="T168" id="Seg_2347" s="T167">мыться-RES-PST.[3SG]</ta>
            <ta e="T169" id="Seg_2348" s="T168">этот.[NOM.SG]</ta>
            <ta e="T170" id="Seg_2349" s="T169">мальчик.[NOM.SG]</ta>
            <ta e="T171" id="Seg_2350" s="T170">прийти-CVB.ANT</ta>
            <ta e="T172" id="Seg_2351" s="T171">мать-LAT/LOC.3SG</ta>
            <ta e="T173" id="Seg_2352" s="T172">сказать-PRS.[3SG]</ta>
            <ta e="T174" id="Seg_2353" s="T173">мальчик.[NOM.SG]</ta>
            <ta e="T175" id="Seg_2354" s="T174">сказать-PRS.[3SG]</ta>
            <ta e="T176" id="Seg_2355" s="T175">NEG</ta>
            <ta e="T177" id="Seg_2356" s="T176">девушка.[NOM.SG]</ta>
            <ta e="T178" id="Seg_2357" s="T177">тогда</ta>
            <ta e="T179" id="Seg_2358" s="T178">этот.[NOM.SG]</ta>
            <ta e="T180" id="Seg_2359" s="T179">девушка.[NOM.SG]</ta>
            <ta e="T181" id="Seg_2360" s="T180">чум-LAT/LOC.3SG</ta>
            <ta e="T182" id="Seg_2361" s="T181">идти-DUR-PRS.[3SG]</ta>
            <ta e="T183" id="Seg_2362" s="T182">вода-ACC</ta>
            <ta e="T184" id="Seg_2363" s="T183">пересечь-PST.[3SG]</ta>
            <ta e="T185" id="Seg_2364" s="T184">грудь-ACC.3SG</ta>
            <ta e="T186" id="Seg_2365" s="T185">показать-PRS-3SG.O</ta>
            <ta e="T187" id="Seg_2366" s="T186">вы.GEN</ta>
            <ta e="T188" id="Seg_2367" s="T187">хлеб-3SG-INS-2PL</ta>
            <ta e="T189" id="Seg_2368" s="T188">как</ta>
            <ta e="T190" id="Seg_2369" s="T189">я.GEN</ta>
            <ta e="T191" id="Seg_2370" s="T190">грудь-NOM/GEN/ACC.1SG</ta>
            <ta e="T192" id="Seg_2371" s="T191">расти-MOM-PST.[3SG]</ta>
            <ta e="T193" id="Seg_2372" s="T192">этот.[NOM.SG]</ta>
            <ta e="T194" id="Seg_2373" s="T193">женщина.[NOM.SG]</ta>
            <ta e="T195" id="Seg_2374" s="T194">сын-ACC.3SG</ta>
            <ta e="T196" id="Seg_2375" s="T195">ругать-PRS-3SG.O</ta>
            <ta e="T197" id="Seg_2376" s="T196">ты.NOM</ta>
            <ta e="T198" id="Seg_2377" s="T197">почему</ta>
            <ta e="T199" id="Seg_2378" s="T198">лгать-PST-2SG</ta>
            <ta e="T200" id="Seg_2379" s="T199">этот.[NOM.SG]</ta>
            <ta e="T201" id="Seg_2380" s="T200">девушка.[NOM.SG]</ta>
            <ta e="T202" id="Seg_2381" s="T201">мальчик.[NOM.SG]</ta>
            <ta e="T203" id="Seg_2382" s="T202">сказать-PST-2SG</ta>
            <ta e="T204" id="Seg_2383" s="T203">идти-MOM-PST.[3SG]</ta>
            <ta e="T205" id="Seg_2384" s="T204">чум-LAT/LOC.3SG</ta>
            <ta e="T206" id="Seg_2385" s="T205">этот.[NOM.SG]</ta>
            <ta e="T207" id="Seg_2386" s="T206">женщина.[NOM.SG]</ta>
            <ta e="T208" id="Seg_2387" s="T207">послать-MOM-PST.[3SG]</ta>
            <ta e="T209" id="Seg_2388" s="T208">Энейдене-NOM/GEN/ACC.3SG</ta>
            <ta e="T210" id="Seg_2389" s="T209">найти-IMP.2SG.O</ta>
            <ta e="T211" id="Seg_2390" s="T210">этот-ACC</ta>
            <ta e="T212" id="Seg_2391" s="T211">пойти-CVB</ta>
            <ta e="T213" id="Seg_2392" s="T212">сорока</ta>
            <ta e="T214" id="Seg_2393" s="T213">стать-CVB</ta>
            <ta e="T215" id="Seg_2394" s="T214">лететь-MOM-PST.[3SG]</ta>
            <ta e="T216" id="Seg_2395" s="T215">спать-DUR.[3SG]</ta>
            <ta e="T217" id="Seg_2396" s="T216">постель-LAT/LOC.3SG</ta>
            <ta e="T218" id="Seg_2397" s="T217">постель-3SG-INS</ta>
            <ta e="T219" id="Seg_2398" s="T218">поднять-CVB</ta>
            <ta e="T220" id="Seg_2399" s="T219">поднять-PST-3PL</ta>
            <ta e="T221" id="Seg_2400" s="T220">этот.[NOM.SG]</ta>
            <ta e="T222" id="Seg_2401" s="T221">проснуться-PST.[3SG]</ta>
            <ta e="T223" id="Seg_2402" s="T222">где</ta>
            <ta e="T224" id="Seg_2403" s="T223">река.[NOM.SG]</ta>
            <ta e="T225" id="Seg_2404" s="T224">пересечь-DUR.[3SG]</ta>
            <ta e="T226" id="Seg_2405" s="T225">этот.[NOM.SG]</ta>
            <ta e="T227" id="Seg_2406" s="T226">земля-LAT/LOC.3SG</ta>
            <ta e="T228" id="Seg_2407" s="T227">принести-PST-3PL</ta>
            <ta e="T229" id="Seg_2408" s="T228">сын-LAT/LOC.3SG</ta>
            <ta e="T230" id="Seg_2409" s="T229">взять-CVB</ta>
            <ta e="T231" id="Seg_2410" s="T230">бросить-PST-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_2411" s="T0">n-n:case</ta>
            <ta e="T2" id="Seg_2412" s="T1">num-n:case</ta>
            <ta e="T3" id="Seg_2413" s="T2">n-n:case.poss</ta>
            <ta e="T4" id="Seg_2414" s="T3">v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_2415" s="T4">dempro-n:case</ta>
            <ta e="T6" id="Seg_2416" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_2417" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_2418" s="T7">v</ta>
            <ta e="T9" id="Seg_2419" s="T8">v-v:n.fin</ta>
            <ta e="T10" id="Seg_2420" s="T9">adj-n:case</ta>
            <ta e="T11" id="Seg_2421" s="T10">n-n:case.poss</ta>
            <ta e="T12" id="Seg_2422" s="T11">pers</ta>
            <ta e="T13" id="Seg_2423" s="T12">v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_2424" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_2425" s="T14">v-v:n.fin</ta>
            <ta e="T16" id="Seg_2426" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_2427" s="T16">v-v&gt;v-v:pn</ta>
            <ta e="T18" id="Seg_2428" s="T17">n-n:case.poss</ta>
            <ta e="T19" id="Seg_2429" s="T18">adv</ta>
            <ta e="T20" id="Seg_2430" s="T19">v-v:tense-v:pn</ta>
            <ta e="T21" id="Seg_2431" s="T20">n-n&gt;adj-n:case</ta>
            <ta e="T22" id="Seg_2432" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_2433" s="T22">v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_2434" s="T23">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_2435" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_2436" s="T25">v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_2437" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_2438" s="T27">v-v:n.fin</ta>
            <ta e="T29" id="Seg_2439" s="T28">n-n:case</ta>
            <ta e="T30" id="Seg_2440" s="T29">v-v:n.fin</ta>
            <ta e="T31" id="Seg_2441" s="T30">v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_2442" s="T31">dempro-n:case</ta>
            <ta e="T33" id="Seg_2443" s="T32">n-n:case</ta>
            <ta e="T34" id="Seg_2444" s="T33">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_2445" s="T34">v-v:n.fin</ta>
            <ta e="T36" id="Seg_2446" s="T35">v-v:tense-v:pn</ta>
            <ta e="T37" id="Seg_2447" s="T36">v-v:tense-v:pn</ta>
            <ta e="T38" id="Seg_2448" s="T37">que</ta>
            <ta e="T39" id="Seg_2449" s="T38">v-v:tense-v:pn</ta>
            <ta e="T40" id="Seg_2450" s="T39">v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_2451" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_2452" s="T41">n-n:case</ta>
            <ta e="T43" id="Seg_2453" s="T42">v-v:tense-v:pn</ta>
            <ta e="T44" id="Seg_2454" s="T43">adj-n:case</ta>
            <ta e="T45" id="Seg_2455" s="T44">n-n:case.poss</ta>
            <ta e="T46" id="Seg_2456" s="T45">v-v:tense-v:pn</ta>
            <ta e="T47" id="Seg_2457" s="T46">pers</ta>
            <ta e="T48" id="Seg_2458" s="T47">v-v:tense-v:pn</ta>
            <ta e="T49" id="Seg_2459" s="T48">adv</ta>
            <ta e="T50" id="Seg_2460" s="T49">dempro-n:case</ta>
            <ta e="T51" id="Seg_2461" s="T50">n-n:case</ta>
            <ta e="T52" id="Seg_2462" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_2463" s="T52">v-v:n.fin</ta>
            <ta e="T54" id="Seg_2464" s="T53">adv</ta>
            <ta e="T55" id="Seg_2465" s="T54">n-n:case</ta>
            <ta e="T56" id="Seg_2466" s="T55">v-v&gt;v-v:pn</ta>
            <ta e="T57" id="Seg_2467" s="T56">adv</ta>
            <ta e="T58" id="Seg_2468" s="T57">v-v:n.fin</ta>
            <ta e="T59" id="Seg_2469" s="T58">v-v:tense-v:pn</ta>
            <ta e="T60" id="Seg_2470" s="T59">que</ta>
            <ta e="T61" id="Seg_2471" s="T60">v-v:tense-v:pn</ta>
            <ta e="T62" id="Seg_2472" s="T61">n-n:case</ta>
            <ta e="T63" id="Seg_2473" s="T62">v-v:tense-v:pn</ta>
            <ta e="T64" id="Seg_2474" s="T63">adv</ta>
            <ta e="T65" id="Seg_2475" s="T64">v-v:n.fin</ta>
            <ta e="T66" id="Seg_2476" s="T65">v-v:tense-v:pn</ta>
            <ta e="T67" id="Seg_2477" s="T66">adj-n:case</ta>
            <ta e="T68" id="Seg_2478" s="T67">n-n:case.poss</ta>
            <ta e="T69" id="Seg_2479" s="T68">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T70" id="Seg_2480" s="T69">n-n&gt;adj-n:case</ta>
            <ta e="T71" id="Seg_2481" s="T70">n-n:case</ta>
            <ta e="T72" id="Seg_2482" s="T71">v-v:tense-v:pn</ta>
            <ta e="T73" id="Seg_2483" s="T72">dempro-n:case</ta>
            <ta e="T74" id="Seg_2484" s="T73">n-n:case.poss</ta>
            <ta e="T75" id="Seg_2485" s="T74">adv</ta>
            <ta e="T76" id="Seg_2486" s="T75">v-v:tense-v:pn</ta>
            <ta e="T77" id="Seg_2487" s="T76">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_2488" s="T77">n-n:case</ta>
            <ta e="T79" id="Seg_2489" s="T78">adv</ta>
            <ta e="T80" id="Seg_2490" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_2491" s="T80">v-v:n.fin</ta>
            <ta e="T82" id="Seg_2492" s="T81">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T83" id="Seg_2493" s="T82">n-n:case.poss</ta>
            <ta e="T84" id="Seg_2494" s="T83">n-n:case</ta>
            <ta e="T85" id="Seg_2495" s="T84">v-v:tense-v:pn</ta>
            <ta e="T86" id="Seg_2496" s="T85">n-n:case.poss</ta>
            <ta e="T87" id="Seg_2497" s="T86">v-v:n.fin</ta>
            <ta e="T88" id="Seg_2498" s="T87">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T89" id="Seg_2499" s="T88">refl-n:case.poss</ta>
            <ta e="T90" id="Seg_2500" s="T89">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T91" id="Seg_2501" s="T90">v-v:tense-v:pn</ta>
            <ta e="T92" id="Seg_2502" s="T91">num-n:case</ta>
            <ta e="T93" id="Seg_2503" s="T92">n-n:case</ta>
            <ta e="T94" id="Seg_2504" s="T93">v-v:tense-v:pn</ta>
            <ta e="T95" id="Seg_2505" s="T94">dempro-n:case</ta>
            <ta e="T96" id="Seg_2506" s="T95">n-n:case</ta>
            <ta e="T97" id="Seg_2507" s="T96">n-n:case.poss</ta>
            <ta e="T98" id="Seg_2508" s="T97">v-v:tense-v:pn</ta>
            <ta e="T99" id="Seg_2509" s="T98">dempro-n:case</ta>
            <ta e="T100" id="Seg_2510" s="T99">dempro-n:num</ta>
            <ta e="T101" id="Seg_2511" s="T100">v-v&gt;v-v:pn</ta>
            <ta e="T102" id="Seg_2512" s="T101">dempro-n:case</ta>
            <ta e="T103" id="Seg_2513" s="T102">n-n:case</ta>
            <ta e="T104" id="Seg_2514" s="T103">n-n:case.poss</ta>
            <ta e="T105" id="Seg_2515" s="T104">v-v:tense-v:pn</ta>
            <ta e="T106" id="Seg_2516" s="T105">n-n:case</ta>
            <ta e="T107" id="Seg_2517" s="T106">dempro-n:case</ta>
            <ta e="T108" id="Seg_2518" s="T107">v-v:mood-v:pn</ta>
            <ta e="T109" id="Seg_2519" s="T108">v-v:n.fin</ta>
            <ta e="T110" id="Seg_2520" s="T109">n-n:case</ta>
            <ta e="T111" id="Seg_2521" s="T110">n-n:case</ta>
            <ta e="T112" id="Seg_2522" s="T111">v-v:n.fin-v:(case.poss)</ta>
            <ta e="T113" id="Seg_2523" s="T112">n-n:case</ta>
            <ta e="T114" id="Seg_2524" s="T113">n-n:case</ta>
            <ta e="T115" id="Seg_2525" s="T114">v-v:tense-v:pn</ta>
            <ta e="T116" id="Seg_2526" s="T115">n-n:case</ta>
            <ta e="T117" id="Seg_2527" s="T116">v-v:n.fin-v:(case.poss)</ta>
            <ta e="T118" id="Seg_2528" s="T117">n-n:case</ta>
            <ta e="T119" id="Seg_2529" s="T118">n-n:case</ta>
            <ta e="T120" id="Seg_2530" s="T119">v-v:tense-v:pn</ta>
            <ta e="T121" id="Seg_2531" s="T120">v-v:tense-v:pn</ta>
            <ta e="T122" id="Seg_2532" s="T121">v-v:tense-v:pn</ta>
            <ta e="T123" id="Seg_2533" s="T122">dempro-n:case</ta>
            <ta e="T124" id="Seg_2534" s="T123">n-n:case</ta>
            <ta e="T125" id="Seg_2535" s="T124">n-n:case</ta>
            <ta e="T126" id="Seg_2536" s="T125">n-n:case</ta>
            <ta e="T127" id="Seg_2537" s="T126">v-v:tense-v:pn</ta>
            <ta e="T128" id="Seg_2538" s="T127">conj</ta>
            <ta e="T129" id="Seg_2539" s="T128">dempro-n:case</ta>
            <ta e="T130" id="Seg_2540" s="T129">n-n:case</ta>
            <ta e="T131" id="Seg_2541" s="T130">n-n:case</ta>
            <ta e="T132" id="Seg_2542" s="T131">n-n:case</ta>
            <ta e="T133" id="Seg_2543" s="T132">v-v:tense-v:pn</ta>
            <ta e="T134" id="Seg_2544" s="T133">n-n:case.poss</ta>
            <ta e="T135" id="Seg_2545" s="T134">v-v:n.fin</ta>
            <ta e="T136" id="Seg_2546" s="T135">n-n:case.poss</ta>
            <ta e="T137" id="Seg_2547" s="T136">v-v:tense-v:pn</ta>
            <ta e="T138" id="Seg_2548" s="T137">v-v:tense-v:pn</ta>
            <ta e="T139" id="Seg_2549" s="T138">dempro-n:case</ta>
            <ta e="T140" id="Seg_2550" s="T139">n-n:case</ta>
            <ta e="T141" id="Seg_2551" s="T140">n-n:case</ta>
            <ta e="T142" id="Seg_2552" s="T141">n-n:case</ta>
            <ta e="T143" id="Seg_2553" s="T142">v-v:tense-v:pn</ta>
            <ta e="T144" id="Seg_2554" s="T143">dempro-n:case</ta>
            <ta e="T145" id="Seg_2555" s="T144">n-n:case</ta>
            <ta e="T146" id="Seg_2556" s="T145">v-v:tense-v:pn</ta>
            <ta e="T147" id="Seg_2557" s="T146">n-n:case</ta>
            <ta e="T148" id="Seg_2558" s="T147">v-v:tense-v:pn</ta>
            <ta e="T149" id="Seg_2559" s="T148">v-v:ins-v:mood.pn</ta>
            <ta e="T150" id="Seg_2560" s="T149">n-n:case</ta>
            <ta e="T151" id="Seg_2561" s="T150">v-v:mood.pn</ta>
            <ta e="T152" id="Seg_2562" s="T151">v-v:tense-v:pn</ta>
            <ta e="T153" id="Seg_2563" s="T152">n-n:case</ta>
            <ta e="T154" id="Seg_2564" s="T153">n-n:case.poss</ta>
            <ta e="T155" id="Seg_2565" s="T154">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T156" id="Seg_2566" s="T155">v-v:tense-v:pn</ta>
            <ta e="T157" id="Seg_2567" s="T156">dempro-n:case</ta>
            <ta e="T158" id="Seg_2568" s="T157">n-n:case</ta>
            <ta e="T159" id="Seg_2569" s="T158">n-n:case.poss</ta>
            <ta e="T160" id="Seg_2570" s="T159">v-v:tense-v:pn</ta>
            <ta e="T161" id="Seg_2571" s="T160">ptcl</ta>
            <ta e="T163" id="Seg_2572" s="T162">v-v:tense-v:pn</ta>
            <ta e="T164" id="Seg_2573" s="T163">conj</ta>
            <ta e="T165" id="Seg_2574" s="T164">v-v:tense-v:pn</ta>
            <ta e="T166" id="Seg_2575" s="T165">n-n:case</ta>
            <ta e="T167" id="Seg_2576" s="T166">dempro-n:case</ta>
            <ta e="T168" id="Seg_2577" s="T167">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T169" id="Seg_2578" s="T168">dempro-n:case</ta>
            <ta e="T170" id="Seg_2579" s="T169">n-n:case</ta>
            <ta e="T171" id="Seg_2580" s="T170">v-v:n.fin</ta>
            <ta e="T172" id="Seg_2581" s="T171">n-n:case.poss</ta>
            <ta e="T173" id="Seg_2582" s="T172">v-v:tense-v:pn</ta>
            <ta e="T174" id="Seg_2583" s="T173">n-n:case</ta>
            <ta e="T175" id="Seg_2584" s="T174">v-v:tense-v:pn</ta>
            <ta e="T176" id="Seg_2585" s="T175">ptcl</ta>
            <ta e="T177" id="Seg_2586" s="T176">n-n:case</ta>
            <ta e="T178" id="Seg_2587" s="T177">adv</ta>
            <ta e="T179" id="Seg_2588" s="T178">dempro-n:case</ta>
            <ta e="T180" id="Seg_2589" s="T179">n-n:case</ta>
            <ta e="T181" id="Seg_2590" s="T180">n-n:case.poss</ta>
            <ta e="T182" id="Seg_2591" s="T181">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T183" id="Seg_2592" s="T182">n-n:case</ta>
            <ta e="T184" id="Seg_2593" s="T183">v-v:tense-v:pn</ta>
            <ta e="T185" id="Seg_2594" s="T184">n-n:case.poss</ta>
            <ta e="T186" id="Seg_2595" s="T185">v-v:tense-v:pn</ta>
            <ta e="T187" id="Seg_2596" s="T186">pers</ta>
            <ta e="T188" id="Seg_2597" s="T187">n-n:case.poss-n:case-n:case.poss</ta>
            <ta e="T189" id="Seg_2598" s="T188">que</ta>
            <ta e="T190" id="Seg_2599" s="T189">pers</ta>
            <ta e="T191" id="Seg_2600" s="T190">n-n:case.poss</ta>
            <ta e="T192" id="Seg_2601" s="T191">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T193" id="Seg_2602" s="T192">dempro-n:case</ta>
            <ta e="T194" id="Seg_2603" s="T193">n-n:case</ta>
            <ta e="T195" id="Seg_2604" s="T194">n-n:case.poss</ta>
            <ta e="T196" id="Seg_2605" s="T195">v-v:tense-v:pn</ta>
            <ta e="T197" id="Seg_2606" s="T196">pers</ta>
            <ta e="T198" id="Seg_2607" s="T197">que</ta>
            <ta e="T199" id="Seg_2608" s="T198">v-v:tense-v:pn</ta>
            <ta e="T200" id="Seg_2609" s="T199">dempro-n:case</ta>
            <ta e="T201" id="Seg_2610" s="T200">n-n:case</ta>
            <ta e="T202" id="Seg_2611" s="T201">n-n:case</ta>
            <ta e="T203" id="Seg_2612" s="T202">v-v:tense-v:pn</ta>
            <ta e="T204" id="Seg_2613" s="T203">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T205" id="Seg_2614" s="T204">n-n:case.poss</ta>
            <ta e="T206" id="Seg_2615" s="T205">dempro-n:case</ta>
            <ta e="T207" id="Seg_2616" s="T206">n-n:case</ta>
            <ta e="T208" id="Seg_2617" s="T207">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T209" id="Seg_2618" s="T208">propr-n:case.poss</ta>
            <ta e="T210" id="Seg_2619" s="T209">v-v:mood.pn</ta>
            <ta e="T211" id="Seg_2620" s="T210">dempro-n:case</ta>
            <ta e="T212" id="Seg_2621" s="T211">v-v:n.fin</ta>
            <ta e="T213" id="Seg_2622" s="T212">n</ta>
            <ta e="T214" id="Seg_2623" s="T213">v-v:n.fin</ta>
            <ta e="T215" id="Seg_2624" s="T214">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T216" id="Seg_2625" s="T215">v-v&gt;v-v:pn</ta>
            <ta e="T217" id="Seg_2626" s="T216">n-n:case.poss</ta>
            <ta e="T218" id="Seg_2627" s="T217">n-n:case.poss-n:case</ta>
            <ta e="T219" id="Seg_2628" s="T218">v-v:n.fin</ta>
            <ta e="T220" id="Seg_2629" s="T219">v-v:tense-v:pn</ta>
            <ta e="T221" id="Seg_2630" s="T220">dempro-n:case</ta>
            <ta e="T222" id="Seg_2631" s="T221">v-v:tense-v:pn</ta>
            <ta e="T223" id="Seg_2632" s="T222">que</ta>
            <ta e="T224" id="Seg_2633" s="T223">n-n:case</ta>
            <ta e="T225" id="Seg_2634" s="T224">v-v&gt;v-v:pn</ta>
            <ta e="T226" id="Seg_2635" s="T225">dempro-n:case</ta>
            <ta e="T227" id="Seg_2636" s="T226">n-n:case.poss</ta>
            <ta e="T228" id="Seg_2637" s="T227">v-v:tense-v:pn</ta>
            <ta e="T229" id="Seg_2638" s="T228">n-n:case.poss</ta>
            <ta e="T230" id="Seg_2639" s="T229">v-v:n.fin</ta>
            <ta e="T231" id="Seg_2640" s="T230">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_2641" s="T0">n</ta>
            <ta e="T2" id="Seg_2642" s="T1">num</ta>
            <ta e="T3" id="Seg_2643" s="T2">n</ta>
            <ta e="T4" id="Seg_2644" s="T3">v</ta>
            <ta e="T5" id="Seg_2645" s="T4">dempro</ta>
            <ta e="T6" id="Seg_2646" s="T5">n</ta>
            <ta e="T7" id="Seg_2647" s="T6">n</ta>
            <ta e="T8" id="Seg_2648" s="T7">v</ta>
            <ta e="T9" id="Seg_2649" s="T8">v</ta>
            <ta e="T10" id="Seg_2650" s="T9">adj</ta>
            <ta e="T11" id="Seg_2651" s="T10">n</ta>
            <ta e="T12" id="Seg_2652" s="T11">pers</ta>
            <ta e="T13" id="Seg_2653" s="T12">v</ta>
            <ta e="T14" id="Seg_2654" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_2655" s="T14">v</ta>
            <ta e="T16" id="Seg_2656" s="T15">n</ta>
            <ta e="T17" id="Seg_2657" s="T16">v</ta>
            <ta e="T18" id="Seg_2658" s="T17">n</ta>
            <ta e="T19" id="Seg_2659" s="T18">adv</ta>
            <ta e="T20" id="Seg_2660" s="T19">v</ta>
            <ta e="T21" id="Seg_2661" s="T20">adj</ta>
            <ta e="T22" id="Seg_2662" s="T21">n</ta>
            <ta e="T23" id="Seg_2663" s="T22">v</ta>
            <ta e="T24" id="Seg_2664" s="T23">v</ta>
            <ta e="T25" id="Seg_2665" s="T24">n</ta>
            <ta e="T26" id="Seg_2666" s="T25">v</ta>
            <ta e="T27" id="Seg_2667" s="T26">n</ta>
            <ta e="T28" id="Seg_2668" s="T27">v</ta>
            <ta e="T29" id="Seg_2669" s="T28">n</ta>
            <ta e="T30" id="Seg_2670" s="T29">v</ta>
            <ta e="T31" id="Seg_2671" s="T30">v</ta>
            <ta e="T32" id="Seg_2672" s="T31">dempro</ta>
            <ta e="T33" id="Seg_2673" s="T32">n</ta>
            <ta e="T34" id="Seg_2674" s="T33">v</ta>
            <ta e="T35" id="Seg_2675" s="T34">v</ta>
            <ta e="T36" id="Seg_2676" s="T35">v</ta>
            <ta e="T37" id="Seg_2677" s="T36">v</ta>
            <ta e="T38" id="Seg_2678" s="T37">que</ta>
            <ta e="T39" id="Seg_2679" s="T38">v</ta>
            <ta e="T40" id="Seg_2680" s="T39">v</ta>
            <ta e="T41" id="Seg_2681" s="T40">n</ta>
            <ta e="T42" id="Seg_2682" s="T41">n</ta>
            <ta e="T43" id="Seg_2683" s="T42">v</ta>
            <ta e="T44" id="Seg_2684" s="T43">adj</ta>
            <ta e="T45" id="Seg_2685" s="T44">n</ta>
            <ta e="T46" id="Seg_2686" s="T45">v</ta>
            <ta e="T47" id="Seg_2687" s="T46">pers</ta>
            <ta e="T48" id="Seg_2688" s="T47">v</ta>
            <ta e="T49" id="Seg_2689" s="T48">adv</ta>
            <ta e="T50" id="Seg_2690" s="T49">dempro</ta>
            <ta e="T51" id="Seg_2691" s="T50">n</ta>
            <ta e="T52" id="Seg_2692" s="T51">n</ta>
            <ta e="T53" id="Seg_2693" s="T52">v</ta>
            <ta e="T54" id="Seg_2694" s="T53">adv</ta>
            <ta e="T55" id="Seg_2695" s="T54">n</ta>
            <ta e="T56" id="Seg_2696" s="T55">v</ta>
            <ta e="T57" id="Seg_2697" s="T56">adv</ta>
            <ta e="T58" id="Seg_2698" s="T57">v</ta>
            <ta e="T59" id="Seg_2699" s="T58">v</ta>
            <ta e="T60" id="Seg_2700" s="T59">que</ta>
            <ta e="T61" id="Seg_2701" s="T60">v</ta>
            <ta e="T62" id="Seg_2702" s="T61">n</ta>
            <ta e="T63" id="Seg_2703" s="T62">v</ta>
            <ta e="T64" id="Seg_2704" s="T63">adv</ta>
            <ta e="T65" id="Seg_2705" s="T64">v</ta>
            <ta e="T66" id="Seg_2706" s="T65">v</ta>
            <ta e="T67" id="Seg_2707" s="T66">adj</ta>
            <ta e="T68" id="Seg_2708" s="T67">n</ta>
            <ta e="T69" id="Seg_2709" s="T68">v</ta>
            <ta e="T70" id="Seg_2710" s="T69">adj</ta>
            <ta e="T71" id="Seg_2711" s="T70">n</ta>
            <ta e="T72" id="Seg_2712" s="T71">v</ta>
            <ta e="T73" id="Seg_2713" s="T72">dempro</ta>
            <ta e="T74" id="Seg_2714" s="T73">n</ta>
            <ta e="T75" id="Seg_2715" s="T74">adv</ta>
            <ta e="T76" id="Seg_2716" s="T75">v</ta>
            <ta e="T77" id="Seg_2717" s="T76">v</ta>
            <ta e="T78" id="Seg_2718" s="T77">n</ta>
            <ta e="T79" id="Seg_2719" s="T78">adv</ta>
            <ta e="T80" id="Seg_2720" s="T79">n</ta>
            <ta e="T81" id="Seg_2721" s="T80">v</ta>
            <ta e="T82" id="Seg_2722" s="T81">v</ta>
            <ta e="T83" id="Seg_2723" s="T82">n</ta>
            <ta e="T84" id="Seg_2724" s="T83">n</ta>
            <ta e="T85" id="Seg_2725" s="T84">v</ta>
            <ta e="T86" id="Seg_2726" s="T85">n</ta>
            <ta e="T87" id="Seg_2727" s="T86">v</ta>
            <ta e="T88" id="Seg_2728" s="T87">v</ta>
            <ta e="T89" id="Seg_2729" s="T88">refl</ta>
            <ta e="T90" id="Seg_2730" s="T89">v</ta>
            <ta e="T91" id="Seg_2731" s="T90">v</ta>
            <ta e="T92" id="Seg_2732" s="T91">num</ta>
            <ta e="T93" id="Seg_2733" s="T92">n</ta>
            <ta e="T94" id="Seg_2734" s="T93">v</ta>
            <ta e="T95" id="Seg_2735" s="T94">dempro</ta>
            <ta e="T96" id="Seg_2736" s="T95">n</ta>
            <ta e="T97" id="Seg_2737" s="T96">n</ta>
            <ta e="T98" id="Seg_2738" s="T97">v</ta>
            <ta e="T99" id="Seg_2739" s="T98">dempro</ta>
            <ta e="T100" id="Seg_2740" s="T99">dempro</ta>
            <ta e="T101" id="Seg_2741" s="T100">v</ta>
            <ta e="T102" id="Seg_2742" s="T101">dempro</ta>
            <ta e="T103" id="Seg_2743" s="T102">n</ta>
            <ta e="T104" id="Seg_2744" s="T103">n</ta>
            <ta e="T105" id="Seg_2745" s="T104">v</ta>
            <ta e="T106" id="Seg_2746" s="T105">n</ta>
            <ta e="T107" id="Seg_2747" s="T106">dempro</ta>
            <ta e="T108" id="Seg_2748" s="T107">v</ta>
            <ta e="T109" id="Seg_2749" s="T108">adj</ta>
            <ta e="T110" id="Seg_2750" s="T109">n</ta>
            <ta e="T111" id="Seg_2751" s="T110">n</ta>
            <ta e="T112" id="Seg_2752" s="T111">v</ta>
            <ta e="T113" id="Seg_2753" s="T112">n</ta>
            <ta e="T114" id="Seg_2754" s="T113">n</ta>
            <ta e="T115" id="Seg_2755" s="T114">v</ta>
            <ta e="T116" id="Seg_2756" s="T115">n</ta>
            <ta e="T117" id="Seg_2757" s="T116">v</ta>
            <ta e="T118" id="Seg_2758" s="T117">n</ta>
            <ta e="T119" id="Seg_2759" s="T118">n</ta>
            <ta e="T120" id="Seg_2760" s="T119">v</ta>
            <ta e="T121" id="Seg_2761" s="T120">v</ta>
            <ta e="T122" id="Seg_2762" s="T121">v</ta>
            <ta e="T123" id="Seg_2763" s="T122">dempro</ta>
            <ta e="T124" id="Seg_2764" s="T123">n</ta>
            <ta e="T125" id="Seg_2765" s="T124">n</ta>
            <ta e="T126" id="Seg_2766" s="T125">n</ta>
            <ta e="T127" id="Seg_2767" s="T126">v</ta>
            <ta e="T128" id="Seg_2768" s="T127">conj</ta>
            <ta e="T129" id="Seg_2769" s="T128">dempro</ta>
            <ta e="T130" id="Seg_2770" s="T129">n</ta>
            <ta e="T131" id="Seg_2771" s="T130">n</ta>
            <ta e="T132" id="Seg_2772" s="T131">n</ta>
            <ta e="T133" id="Seg_2773" s="T132">v</ta>
            <ta e="T134" id="Seg_2774" s="T133">n</ta>
            <ta e="T135" id="Seg_2775" s="T134">v</ta>
            <ta e="T136" id="Seg_2776" s="T135">n</ta>
            <ta e="T137" id="Seg_2777" s="T136">v</ta>
            <ta e="T138" id="Seg_2778" s="T137">v</ta>
            <ta e="T139" id="Seg_2779" s="T138">dempro</ta>
            <ta e="T140" id="Seg_2780" s="T139">n</ta>
            <ta e="T141" id="Seg_2781" s="T140">n</ta>
            <ta e="T142" id="Seg_2782" s="T141">n</ta>
            <ta e="T143" id="Seg_2783" s="T142">v</ta>
            <ta e="T144" id="Seg_2784" s="T143">dempro</ta>
            <ta e="T145" id="Seg_2785" s="T144">n</ta>
            <ta e="T146" id="Seg_2786" s="T145">v</ta>
            <ta e="T147" id="Seg_2787" s="T146">n</ta>
            <ta e="T148" id="Seg_2788" s="T147">v</ta>
            <ta e="T149" id="Seg_2789" s="T148">v</ta>
            <ta e="T150" id="Seg_2790" s="T149">n</ta>
            <ta e="T151" id="Seg_2791" s="T150">v</ta>
            <ta e="T152" id="Seg_2792" s="T151">v</ta>
            <ta e="T153" id="Seg_2793" s="T152">n</ta>
            <ta e="T154" id="Seg_2794" s="T153">n</ta>
            <ta e="T155" id="Seg_2795" s="T154">v</ta>
            <ta e="T156" id="Seg_2796" s="T155">v</ta>
            <ta e="T157" id="Seg_2797" s="T156">dempro</ta>
            <ta e="T158" id="Seg_2798" s="T157">n</ta>
            <ta e="T159" id="Seg_2799" s="T158">n</ta>
            <ta e="T160" id="Seg_2800" s="T159">v</ta>
            <ta e="T161" id="Seg_2801" s="T160">ptcl</ta>
            <ta e="T163" id="Seg_2802" s="T162">v</ta>
            <ta e="T164" id="Seg_2803" s="T163">conj</ta>
            <ta e="T165" id="Seg_2804" s="T164">v</ta>
            <ta e="T166" id="Seg_2805" s="T165">n</ta>
            <ta e="T167" id="Seg_2806" s="T166">dempro</ta>
            <ta e="T168" id="Seg_2807" s="T167">v</ta>
            <ta e="T169" id="Seg_2808" s="T168">dempro</ta>
            <ta e="T170" id="Seg_2809" s="T169">n</ta>
            <ta e="T171" id="Seg_2810" s="T170">v</ta>
            <ta e="T172" id="Seg_2811" s="T171">n</ta>
            <ta e="T173" id="Seg_2812" s="T172">v</ta>
            <ta e="T174" id="Seg_2813" s="T173">n</ta>
            <ta e="T175" id="Seg_2814" s="T174">v</ta>
            <ta e="T176" id="Seg_2815" s="T175">ptcl</ta>
            <ta e="T177" id="Seg_2816" s="T176">n</ta>
            <ta e="T178" id="Seg_2817" s="T177">adv</ta>
            <ta e="T179" id="Seg_2818" s="T178">dempro</ta>
            <ta e="T180" id="Seg_2819" s="T179">n</ta>
            <ta e="T181" id="Seg_2820" s="T180">n</ta>
            <ta e="T182" id="Seg_2821" s="T181">v</ta>
            <ta e="T183" id="Seg_2822" s="T182">n</ta>
            <ta e="T184" id="Seg_2823" s="T183">v</ta>
            <ta e="T185" id="Seg_2824" s="T184">n</ta>
            <ta e="T186" id="Seg_2825" s="T185">v</ta>
            <ta e="T187" id="Seg_2826" s="T186">pers</ta>
            <ta e="T188" id="Seg_2827" s="T187">n</ta>
            <ta e="T189" id="Seg_2828" s="T188">que</ta>
            <ta e="T190" id="Seg_2829" s="T189">pers</ta>
            <ta e="T191" id="Seg_2830" s="T190">n</ta>
            <ta e="T192" id="Seg_2831" s="T191">v</ta>
            <ta e="T193" id="Seg_2832" s="T192">dempro</ta>
            <ta e="T194" id="Seg_2833" s="T193">n</ta>
            <ta e="T195" id="Seg_2834" s="T194">n</ta>
            <ta e="T196" id="Seg_2835" s="T195">v</ta>
            <ta e="T197" id="Seg_2836" s="T196">pers</ta>
            <ta e="T198" id="Seg_2837" s="T197">que</ta>
            <ta e="T199" id="Seg_2838" s="T198">v</ta>
            <ta e="T200" id="Seg_2839" s="T199">dempro</ta>
            <ta e="T201" id="Seg_2840" s="T200">n</ta>
            <ta e="T202" id="Seg_2841" s="T201">n</ta>
            <ta e="T203" id="Seg_2842" s="T202">v</ta>
            <ta e="T204" id="Seg_2843" s="T203">v</ta>
            <ta e="T205" id="Seg_2844" s="T204">n</ta>
            <ta e="T206" id="Seg_2845" s="T205">dempro</ta>
            <ta e="T207" id="Seg_2846" s="T206">n</ta>
            <ta e="T208" id="Seg_2847" s="T207">v</ta>
            <ta e="T209" id="Seg_2848" s="T208">propr</ta>
            <ta e="T210" id="Seg_2849" s="T209">v</ta>
            <ta e="T211" id="Seg_2850" s="T210">dempro</ta>
            <ta e="T212" id="Seg_2851" s="T211">v</ta>
            <ta e="T213" id="Seg_2852" s="T212">n</ta>
            <ta e="T214" id="Seg_2853" s="T213">v</ta>
            <ta e="T215" id="Seg_2854" s="T214">v</ta>
            <ta e="T216" id="Seg_2855" s="T215">v</ta>
            <ta e="T217" id="Seg_2856" s="T216">n</ta>
            <ta e="T218" id="Seg_2857" s="T217">n</ta>
            <ta e="T219" id="Seg_2858" s="T218">v</ta>
            <ta e="T220" id="Seg_2859" s="T219">v</ta>
            <ta e="T221" id="Seg_2860" s="T220">dempro</ta>
            <ta e="T222" id="Seg_2861" s="T221">v</ta>
            <ta e="T223" id="Seg_2862" s="T222">que</ta>
            <ta e="T224" id="Seg_2863" s="T223">n</ta>
            <ta e="T225" id="Seg_2864" s="T224">v</ta>
            <ta e="T226" id="Seg_2865" s="T225">dempro</ta>
            <ta e="T227" id="Seg_2866" s="T226">n</ta>
            <ta e="T228" id="Seg_2867" s="T227">v</ta>
            <ta e="T229" id="Seg_2868" s="T228">n</ta>
            <ta e="T230" id="Seg_2869" s="T229">v</ta>
            <ta e="T231" id="Seg_2870" s="T230">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_2871" s="T0">np.h:Poss</ta>
            <ta e="T3" id="Seg_2872" s="T2">np.h:Th</ta>
            <ta e="T6" id="Seg_2873" s="T5">np.h:Th</ta>
            <ta e="T11" id="Seg_2874" s="T10">np.h:Th 0.3.h:Poss</ta>
            <ta e="T12" id="Seg_2875" s="T11">pro.h:A</ta>
            <ta e="T15" id="Seg_2876" s="T14">0.2.h:A</ta>
            <ta e="T16" id="Seg_2877" s="T15">np.h:A</ta>
            <ta e="T18" id="Seg_2878" s="T17">np:P 0.3.h:Poss</ta>
            <ta e="T20" id="Seg_2879" s="T19">0.3.h:A</ta>
            <ta e="T22" id="Seg_2880" s="T21">np:Th</ta>
            <ta e="T23" id="Seg_2881" s="T22">0.3.h:A</ta>
            <ta e="T24" id="Seg_2882" s="T23">0.3.h:A</ta>
            <ta e="T25" id="Seg_2883" s="T24">np.h:A</ta>
            <ta e="T27" id="Seg_2884" s="T26">np:Th</ta>
            <ta e="T28" id="Seg_2885" s="T27">0.3.h:A</ta>
            <ta e="T29" id="Seg_2886" s="T28">np:L</ta>
            <ta e="T31" id="Seg_2887" s="T30">0.3.h:A</ta>
            <ta e="T33" id="Seg_2888" s="T32">np.h:A</ta>
            <ta e="T36" id="Seg_2889" s="T35">0.3.h:A</ta>
            <ta e="T37" id="Seg_2890" s="T36">0.3.h:A</ta>
            <ta e="T39" id="Seg_2891" s="T38">0.2.h:A</ta>
            <ta e="T40" id="Seg_2892" s="T39">0.1.h:E</ta>
            <ta e="T41" id="Seg_2893" s="T40">np:A</ta>
            <ta e="T42" id="Seg_2894" s="T41">np:L</ta>
            <ta e="T45" id="Seg_2895" s="T44">np.h:A 0.3.h:Poss</ta>
            <ta e="T47" id="Seg_2896" s="T46">pro.h:A</ta>
            <ta e="T49" id="Seg_2897" s="T48">adv:Time</ta>
            <ta e="T51" id="Seg_2898" s="T50">np.h:A</ta>
            <ta e="T52" id="Seg_2899" s="T51">np:Th</ta>
            <ta e="T53" id="Seg_2900" s="T52">0.3.h:A</ta>
            <ta e="T54" id="Seg_2901" s="T53">adv:Time</ta>
            <ta e="T55" id="Seg_2902" s="T54">np:L</ta>
            <ta e="T57" id="Seg_2903" s="T56">adv:Time</ta>
            <ta e="T59" id="Seg_2904" s="T58">0.3.h:A</ta>
            <ta e="T61" id="Seg_2905" s="T60">0.2.h:A</ta>
            <ta e="T62" id="Seg_2906" s="T61">np:Th</ta>
            <ta e="T63" id="Seg_2907" s="T62">0.1.h:E</ta>
            <ta e="T64" id="Seg_2908" s="T63">adv:Time</ta>
            <ta e="T66" id="Seg_2909" s="T65">0.1.h:A</ta>
            <ta e="T68" id="Seg_2910" s="T67">np.h:A 0.3.h:Poss</ta>
            <ta e="T71" id="Seg_2911" s="T70">np:Th</ta>
            <ta e="T72" id="Seg_2912" s="T71">0.3.h:A</ta>
            <ta e="T73" id="Seg_2913" s="T72">pro.h:A</ta>
            <ta e="T74" id="Seg_2914" s="T73">np:P 0.3.h:Poss</ta>
            <ta e="T77" id="Seg_2915" s="T76">0.3.h:A</ta>
            <ta e="T78" id="Seg_2916" s="T77">np.h:A</ta>
            <ta e="T79" id="Seg_2917" s="T78">adv:Time</ta>
            <ta e="T80" id="Seg_2918" s="T79">np:Th</ta>
            <ta e="T81" id="Seg_2919" s="T80">0.3.h:A</ta>
            <ta e="T83" id="Seg_2920" s="T82">np.h:A 0.3.h:Poss</ta>
            <ta e="T84" id="Seg_2921" s="T83">np:Ins</ta>
            <ta e="T86" id="Seg_2922" s="T85">np:P 0.3.h:Poss</ta>
            <ta e="T88" id="Seg_2923" s="T87">0.3.h:A</ta>
            <ta e="T89" id="Seg_2924" s="T88">pro.h:A</ta>
            <ta e="T91" id="Seg_2925" s="T90">0.3.h:A</ta>
            <ta e="T93" id="Seg_2926" s="T92">np:L</ta>
            <ta e="T94" id="Seg_2927" s="T93">0.3.h:A</ta>
            <ta e="T96" id="Seg_2928" s="T95">np.h:Poss</ta>
            <ta e="T97" id="Seg_2929" s="T96">np.h:Th</ta>
            <ta e="T99" id="Seg_2930" s="T98">pro.h:Com</ta>
            <ta e="T100" id="Seg_2931" s="T99">pro.h:A</ta>
            <ta e="T103" id="Seg_2932" s="T102">np.h:A</ta>
            <ta e="T104" id="Seg_2933" s="T103">np.h:R 0.3.h:Poss</ta>
            <ta e="T106" id="Seg_2934" s="T105">np.h:Th</ta>
            <ta e="T107" id="Seg_2935" s="T106">pro.h:Com</ta>
            <ta e="T108" id="Seg_2936" s="T107">0.2.h:A</ta>
            <ta e="T110" id="Seg_2937" s="T109">np:G</ta>
            <ta e="T111" id="Seg_2938" s="T110">np.h:Th</ta>
            <ta e="T112" id="Seg_2939" s="T111">0.3.h:Th</ta>
            <ta e="T114" id="Seg_2940" s="T113">np:Th</ta>
            <ta e="T115" id="Seg_2941" s="T114">0.3.h:A</ta>
            <ta e="T116" id="Seg_2942" s="T115">np.h:Th</ta>
            <ta e="T117" id="Seg_2943" s="T116">0.3.h:Th</ta>
            <ta e="T119" id="Seg_2944" s="T118">np:Th</ta>
            <ta e="T120" id="Seg_2945" s="T119">0.3.h:A</ta>
            <ta e="T121" id="Seg_2946" s="T120">0.3.h:A</ta>
            <ta e="T122" id="Seg_2947" s="T121">0.3.h:A</ta>
            <ta e="T124" id="Seg_2948" s="T123">np.h:A</ta>
            <ta e="T126" id="Seg_2949" s="T125">np:Th</ta>
            <ta e="T130" id="Seg_2950" s="T129">np.h:A</ta>
            <ta e="T132" id="Seg_2951" s="T131">np:Th</ta>
            <ta e="T134" id="Seg_2952" s="T133">np:G 0.3.h:Poss</ta>
            <ta e="T135" id="Seg_2953" s="T134">0.3.h:A</ta>
            <ta e="T136" id="Seg_2954" s="T135">np.h:R 0.3.h:Poss</ta>
            <ta e="T137" id="Seg_2955" s="T136">0.3.h:A</ta>
            <ta e="T138" id="Seg_2956" s="T137">0.3.h:A</ta>
            <ta e="T139" id="Seg_2957" s="T138">pro.h:Th</ta>
            <ta e="T140" id="Seg_2958" s="T139">np.h:Th</ta>
            <ta e="T142" id="Seg_2959" s="T141">np:Th</ta>
            <ta e="T143" id="Seg_2960" s="T142">0.3.h:A</ta>
            <ta e="T145" id="Seg_2961" s="T144">np.h:A</ta>
            <ta e="T147" id="Seg_2962" s="T146">np.h:Th</ta>
            <ta e="T148" id="Seg_2963" s="T147">0.3.h:A</ta>
            <ta e="T149" id="Seg_2964" s="T148">0.2.h:A</ta>
            <ta e="T150" id="Seg_2965" s="T149">np:G</ta>
            <ta e="T151" id="Seg_2966" s="T150">0.2.h:A</ta>
            <ta e="T152" id="Seg_2967" s="T151">0.3.h:A</ta>
            <ta e="T153" id="Seg_2968" s="T152">np:G</ta>
            <ta e="T154" id="Seg_2969" s="T153">np:Th 0.3.h:Poss</ta>
            <ta e="T155" id="Seg_2970" s="T154">0.3.h:E</ta>
            <ta e="T156" id="Seg_2971" s="T155">0.3.h:A</ta>
            <ta e="T158" id="Seg_2972" s="T157">np.h:A</ta>
            <ta e="T159" id="Seg_2973" s="T158">np:P 0.3.h:Poss</ta>
            <ta e="T163" id="Seg_2974" s="T162">0.3.h:A</ta>
            <ta e="T166" id="Seg_2975" s="T165">np.h:A</ta>
            <ta e="T167" id="Seg_2976" s="T166">pro.h:A</ta>
            <ta e="T170" id="Seg_2977" s="T169">np.h:A</ta>
            <ta e="T171" id="Seg_2978" s="T170">0.3.h:A</ta>
            <ta e="T172" id="Seg_2979" s="T171">np.h:R 0.3.h:Poss</ta>
            <ta e="T174" id="Seg_2980" s="T173">np.h:Th</ta>
            <ta e="T175" id="Seg_2981" s="T174">0.3.h:A</ta>
            <ta e="T177" id="Seg_2982" s="T176">np.h:Th</ta>
            <ta e="T178" id="Seg_2983" s="T177">adv:Time</ta>
            <ta e="T180" id="Seg_2984" s="T179">np.h:A</ta>
            <ta e="T181" id="Seg_2985" s="T180">np:G 0.3.h:Poss</ta>
            <ta e="T183" id="Seg_2986" s="T182">np:Pth</ta>
            <ta e="T184" id="Seg_2987" s="T183">0.3.h:A</ta>
            <ta e="T185" id="Seg_2988" s="T184">np:Th 0.3.h:Poss</ta>
            <ta e="T186" id="Seg_2989" s="T185">0.3.h:A</ta>
            <ta e="T187" id="Seg_2990" s="T186">pro.h:Poss</ta>
            <ta e="T188" id="Seg_2991" s="T187">np:Ins</ta>
            <ta e="T190" id="Seg_2992" s="T189">pro.h:Poss</ta>
            <ta e="T191" id="Seg_2993" s="T190">np:Th</ta>
            <ta e="T194" id="Seg_2994" s="T193">np.h:A</ta>
            <ta e="T195" id="Seg_2995" s="T194">np.h:E 0.3.h:Poss</ta>
            <ta e="T197" id="Seg_2996" s="T196">pro.h:A</ta>
            <ta e="T200" id="Seg_2997" s="T199">np.h:Th</ta>
            <ta e="T201" id="Seg_2998" s="T200">np.h:Th</ta>
            <ta e="T202" id="Seg_2999" s="T201">np.h:Th</ta>
            <ta e="T203" id="Seg_3000" s="T202">0.2.h:A</ta>
            <ta e="T204" id="Seg_3001" s="T203">0.3.h:A</ta>
            <ta e="T205" id="Seg_3002" s="T204">np:G 0.3.h:Poss</ta>
            <ta e="T207" id="Seg_3003" s="T206">np.h:A</ta>
            <ta e="T209" id="Seg_3004" s="T208">np.h:E 0.3.h:Poss</ta>
            <ta e="T210" id="Seg_3005" s="T209">0.2.h:A</ta>
            <ta e="T211" id="Seg_3006" s="T210">np.h:Th</ta>
            <ta e="T213" id="Seg_3007" s="T212">np:Th</ta>
            <ta e="T214" id="Seg_3008" s="T213">0.3.h:A</ta>
            <ta e="T215" id="Seg_3009" s="T214">0.3.h:A</ta>
            <ta e="T216" id="Seg_3010" s="T215">0.3.h:Th</ta>
            <ta e="T217" id="Seg_3011" s="T216">np:L 0.3.h:Poss</ta>
            <ta e="T218" id="Seg_3012" s="T217">np:Com 0.3.h:Poss</ta>
            <ta e="T220" id="Seg_3013" s="T219">0.3.h:A</ta>
            <ta e="T221" id="Seg_3014" s="T220">pro.h:P</ta>
            <ta e="T224" id="Seg_3015" s="T223">np:Th</ta>
            <ta e="T227" id="Seg_3016" s="T226">np:L 0.3.h:Poss</ta>
            <ta e="T228" id="Seg_3017" s="T227">0.3.h:A</ta>
            <ta e="T229" id="Seg_3018" s="T228">np.h:R 0.3.h:Poss</ta>
            <ta e="T231" id="Seg_3019" s="T230">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_3020" s="T2">np.h:S</ta>
            <ta e="T4" id="Seg_3021" s="T3">v:pred</ta>
            <ta e="T6" id="Seg_3022" s="T5">np.h:O</ta>
            <ta e="T11" id="Seg_3023" s="T10">np.h:S</ta>
            <ta e="T12" id="Seg_3024" s="T11">pro.h:S</ta>
            <ta e="T13" id="Seg_3025" s="T12">v:pred</ta>
            <ta e="T15" id="Seg_3026" s="T14">v:pred 0.2.h:S</ta>
            <ta e="T16" id="Seg_3027" s="T15">np.h:S</ta>
            <ta e="T17" id="Seg_3028" s="T16">v:pred</ta>
            <ta e="T18" id="Seg_3029" s="T17">np:O</ta>
            <ta e="T20" id="Seg_3030" s="T19">v:pred 0.3.h:S</ta>
            <ta e="T22" id="Seg_3031" s="T21">np:O</ta>
            <ta e="T23" id="Seg_3032" s="T22">v:pred 0.3.h:S</ta>
            <ta e="T24" id="Seg_3033" s="T23">v:pred 0.3.h:S</ta>
            <ta e="T25" id="Seg_3034" s="T24">np.h:S</ta>
            <ta e="T26" id="Seg_3035" s="T25">v:pred</ta>
            <ta e="T28" id="Seg_3036" s="T26">s:adv</ta>
            <ta e="T30" id="Seg_3037" s="T29">s:adv</ta>
            <ta e="T31" id="Seg_3038" s="T30">v:pred 0.3.h:S</ta>
            <ta e="T33" id="Seg_3039" s="T32">np.h:S</ta>
            <ta e="T34" id="Seg_3040" s="T33">v:pred</ta>
            <ta e="T36" id="Seg_3041" s="T35">v:pred 0.3.h:S</ta>
            <ta e="T37" id="Seg_3042" s="T36">v:pred 0.3.h:S</ta>
            <ta e="T39" id="Seg_3043" s="T38">v:pred 0.2.h:S</ta>
            <ta e="T40" id="Seg_3044" s="T39">v:pred 0.1.h:S</ta>
            <ta e="T41" id="Seg_3045" s="T40">np:S</ta>
            <ta e="T43" id="Seg_3046" s="T42">v:pred</ta>
            <ta e="T45" id="Seg_3047" s="T44">np.h:S</ta>
            <ta e="T46" id="Seg_3048" s="T45">v:pred</ta>
            <ta e="T47" id="Seg_3049" s="T46">pro.h:S</ta>
            <ta e="T48" id="Seg_3050" s="T47">v:pred</ta>
            <ta e="T51" id="Seg_3051" s="T50">np.h:S</ta>
            <ta e="T53" id="Seg_3052" s="T51">s:temp</ta>
            <ta e="T56" id="Seg_3053" s="T55">v:pred</ta>
            <ta e="T59" id="Seg_3054" s="T58">v:pred 0.3.h:S</ta>
            <ta e="T61" id="Seg_3055" s="T60">v:pred 0.2.h:S</ta>
            <ta e="T62" id="Seg_3056" s="T61">np:O</ta>
            <ta e="T63" id="Seg_3057" s="T62">v:pred 0.1.h:S</ta>
            <ta e="T66" id="Seg_3058" s="T65">v:pred 0.1.h:S</ta>
            <ta e="T68" id="Seg_3059" s="T67">np.h:S</ta>
            <ta e="T69" id="Seg_3060" s="T68">v:pred</ta>
            <ta e="T71" id="Seg_3061" s="T70">np:O</ta>
            <ta e="T72" id="Seg_3062" s="T71">v:pred 0.3.h:S</ta>
            <ta e="T73" id="Seg_3063" s="T72">pro.h:S</ta>
            <ta e="T74" id="Seg_3064" s="T73">np:O</ta>
            <ta e="T76" id="Seg_3065" s="T75">v:pred</ta>
            <ta e="T77" id="Seg_3066" s="T76">v:pred 0.3.h:S</ta>
            <ta e="T78" id="Seg_3067" s="T77">np.h:S</ta>
            <ta e="T81" id="Seg_3068" s="T79">s:adv</ta>
            <ta e="T82" id="Seg_3069" s="T81">v:pred</ta>
            <ta e="T83" id="Seg_3070" s="T82">np.h:S</ta>
            <ta e="T85" id="Seg_3071" s="T84">v:pred</ta>
            <ta e="T86" id="Seg_3072" s="T85">np:O</ta>
            <ta e="T88" id="Seg_3073" s="T87">v:pred 0.3.h:S</ta>
            <ta e="T89" id="Seg_3074" s="T88">pro.h:S</ta>
            <ta e="T90" id="Seg_3075" s="T89">v:pred</ta>
            <ta e="T91" id="Seg_3076" s="T90">v:pred 0.3.h:S</ta>
            <ta e="T94" id="Seg_3077" s="T93">v:pred 0.3.h:S</ta>
            <ta e="T97" id="Seg_3078" s="T96">np.h:S</ta>
            <ta e="T98" id="Seg_3079" s="T97">v:pred</ta>
            <ta e="T100" id="Seg_3080" s="T99">pro.h:S</ta>
            <ta e="T101" id="Seg_3081" s="T100">v:pred</ta>
            <ta e="T103" id="Seg_3082" s="T102">np.h:S</ta>
            <ta e="T105" id="Seg_3083" s="T104">v:pred</ta>
            <ta e="T106" id="Seg_3084" s="T105">n:pred 0.3.h:S</ta>
            <ta e="T108" id="Seg_3085" s="T107">v:pred 0.2.h:S</ta>
            <ta e="T112" id="Seg_3086" s="T110">s:cond</ta>
            <ta e="T114" id="Seg_3087" s="T113">np:O</ta>
            <ta e="T115" id="Seg_3088" s="T114">v:pred 0.3.h:S</ta>
            <ta e="T117" id="Seg_3089" s="T115">s:cond</ta>
            <ta e="T119" id="Seg_3090" s="T118">np:O</ta>
            <ta e="T120" id="Seg_3091" s="T119">v:pred 0.3.h:S</ta>
            <ta e="T121" id="Seg_3092" s="T120">v:pred 0.3.h:S</ta>
            <ta e="T122" id="Seg_3093" s="T121">v:pred 0.3.h:S</ta>
            <ta e="T124" id="Seg_3094" s="T123">np.h:S</ta>
            <ta e="T126" id="Seg_3095" s="T125">np:O</ta>
            <ta e="T127" id="Seg_3096" s="T126">v:pred</ta>
            <ta e="T130" id="Seg_3097" s="T129">np.h:S</ta>
            <ta e="T132" id="Seg_3098" s="T131">np:O</ta>
            <ta e="T133" id="Seg_3099" s="T132">v:pred</ta>
            <ta e="T135" id="Seg_3100" s="T133">s:temp</ta>
            <ta e="T137" id="Seg_3101" s="T136">v:pred 0.3.h:S</ta>
            <ta e="T138" id="Seg_3102" s="T137">v:pred 0.3.h:S</ta>
            <ta e="T139" id="Seg_3103" s="T138">pro.h:S</ta>
            <ta e="T140" id="Seg_3104" s="T139">n:pred</ta>
            <ta e="T142" id="Seg_3105" s="T141">np:O</ta>
            <ta e="T143" id="Seg_3106" s="T142">v:pred 0.3.h:S</ta>
            <ta e="T145" id="Seg_3107" s="T144">np.h:S</ta>
            <ta e="T146" id="Seg_3108" s="T145">v:pred</ta>
            <ta e="T147" id="Seg_3109" s="T146">n:pred 0.3.h:S</ta>
            <ta e="T148" id="Seg_3110" s="T147">v:pred 0.3.h:S</ta>
            <ta e="T149" id="Seg_3111" s="T148">v:pred 0.2.h:S</ta>
            <ta e="T151" id="Seg_3112" s="T150">v:pred 0.2.h:S</ta>
            <ta e="T152" id="Seg_3113" s="T151">v:pred 0.3.h:S</ta>
            <ta e="T154" id="Seg_3114" s="T153">np:O</ta>
            <ta e="T155" id="Seg_3115" s="T154">v:pred 0.3.h:S</ta>
            <ta e="T156" id="Seg_3116" s="T155">v:pred 0.3.h:S</ta>
            <ta e="T158" id="Seg_3117" s="T157">np.h:S</ta>
            <ta e="T159" id="Seg_3118" s="T158">np:O</ta>
            <ta e="T160" id="Seg_3119" s="T159">v:pred</ta>
            <ta e="T161" id="Seg_3120" s="T160">ptcl.neg</ta>
            <ta e="T163" id="Seg_3121" s="T162">v:pred 0.3.h:S</ta>
            <ta e="T165" id="Seg_3122" s="T164">v:pred</ta>
            <ta e="T166" id="Seg_3123" s="T165">np.h:S</ta>
            <ta e="T167" id="Seg_3124" s="T166">pro.h:S</ta>
            <ta e="T168" id="Seg_3125" s="T167">v:pred</ta>
            <ta e="T170" id="Seg_3126" s="T169">np.h:S</ta>
            <ta e="T171" id="Seg_3127" s="T170">s:temp</ta>
            <ta e="T173" id="Seg_3128" s="T172">v:pred</ta>
            <ta e="T174" id="Seg_3129" s="T173">n:pred 0.3.h:S</ta>
            <ta e="T175" id="Seg_3130" s="T174">v:pred 0.3.h:S</ta>
            <ta e="T176" id="Seg_3131" s="T175">ptcl.neg</ta>
            <ta e="T177" id="Seg_3132" s="T176">n:pred 0.3.h:S</ta>
            <ta e="T180" id="Seg_3133" s="T179">np.h:S</ta>
            <ta e="T182" id="Seg_3134" s="T181">v:pred</ta>
            <ta e="T183" id="Seg_3135" s="T182">np:O</ta>
            <ta e="T184" id="Seg_3136" s="T183">v:pred 0.3.h:S</ta>
            <ta e="T185" id="Seg_3137" s="T184">np:O</ta>
            <ta e="T186" id="Seg_3138" s="T185">v:pred 0.3.h:S</ta>
            <ta e="T191" id="Seg_3139" s="T190">np:S</ta>
            <ta e="T192" id="Seg_3140" s="T191">v:pred</ta>
            <ta e="T194" id="Seg_3141" s="T193">np.h:S</ta>
            <ta e="T195" id="Seg_3142" s="T194">np.h:O</ta>
            <ta e="T196" id="Seg_3143" s="T195">v:pred</ta>
            <ta e="T197" id="Seg_3144" s="T196">pro.h:S</ta>
            <ta e="T199" id="Seg_3145" s="T198">v:pred</ta>
            <ta e="T200" id="Seg_3146" s="T199">pro.h:S</ta>
            <ta e="T201" id="Seg_3147" s="T200">n:pred</ta>
            <ta e="T203" id="Seg_3148" s="T202">v:pred 0.2.h:S</ta>
            <ta e="T204" id="Seg_3149" s="T203">v:pred 0.3.h:S</ta>
            <ta e="T207" id="Seg_3150" s="T206">np.h:S</ta>
            <ta e="T208" id="Seg_3151" s="T207">v:pred</ta>
            <ta e="T209" id="Seg_3152" s="T208">np.h:O</ta>
            <ta e="T210" id="Seg_3153" s="T209">v:pred</ta>
            <ta e="T211" id="Seg_3154" s="T210">pro.h:O</ta>
            <ta e="T214" id="Seg_3155" s="T212">s:adv</ta>
            <ta e="T215" id="Seg_3156" s="T214">v:pred 0.3.h:S</ta>
            <ta e="T216" id="Seg_3157" s="T215">v:pred 0.3.h:S</ta>
            <ta e="T220" id="Seg_3158" s="T219">v:pred 0.3.h:S</ta>
            <ta e="T221" id="Seg_3159" s="T220">pro.h:S</ta>
            <ta e="T222" id="Seg_3160" s="T221">v:pred</ta>
            <ta e="T228" id="Seg_3161" s="T227">v:pred 0.3.h:S</ta>
            <ta e="T231" id="Seg_3162" s="T230">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T1" id="Seg_3163" s="T0">new</ta>
            <ta e="T3" id="Seg_3164" s="T2">new</ta>
            <ta e="T6" id="Seg_3165" s="T5">giv-active</ta>
            <ta e="T7" id="Seg_3166" s="T6">new</ta>
            <ta e="T11" id="Seg_3167" s="T10">accs-inf</ta>
            <ta e="T12" id="Seg_3168" s="T11">giv-active-Q</ta>
            <ta e="T15" id="Seg_3169" s="T14">0.giv-active-Q</ta>
            <ta e="T16" id="Seg_3170" s="T15">giv-inactive</ta>
            <ta e="T17" id="Seg_3171" s="T16">quot-sp</ta>
            <ta e="T18" id="Seg_3172" s="T17">accs-gen</ta>
            <ta e="T20" id="Seg_3173" s="T19">0.giv-active</ta>
            <ta e="T22" id="Seg_3174" s="T21">new</ta>
            <ta e="T24" id="Seg_3175" s="T23">0.giv-active</ta>
            <ta e="T25" id="Seg_3176" s="T24">giv-inactive</ta>
            <ta e="T27" id="Seg_3177" s="T26">new</ta>
            <ta e="T29" id="Seg_3178" s="T28">accs-gen</ta>
            <ta e="T31" id="Seg_3179" s="T30">0.giv-active</ta>
            <ta e="T33" id="Seg_3180" s="T32">giv-inactive</ta>
            <ta e="T36" id="Seg_3181" s="T35">0.giv-active</ta>
            <ta e="T37" id="Seg_3182" s="T36">quot-sp</ta>
            <ta e="T39" id="Seg_3183" s="T38">0.giv-inactive-Q</ta>
            <ta e="T40" id="Seg_3184" s="T39">0.giv-active-Q</ta>
            <ta e="T41" id="Seg_3185" s="T40">giv-inactive-Q</ta>
            <ta e="T42" id="Seg_3186" s="T41">giv-inactive-Q</ta>
            <ta e="T45" id="Seg_3187" s="T44">giv-inactive</ta>
            <ta e="T46" id="Seg_3188" s="T45">quot-sp</ta>
            <ta e="T47" id="Seg_3189" s="T46">giv-active-Q</ta>
            <ta e="T51" id="Seg_3190" s="T50">giv-inactive</ta>
            <ta e="T52" id="Seg_3191" s="T51">giv-inactive</ta>
            <ta e="T55" id="Seg_3192" s="T54">giv-inactive</ta>
            <ta e="T59" id="Seg_3193" s="T58">0.giv-inactive</ta>
            <ta e="T61" id="Seg_3194" s="T60">0.giv-active-Q</ta>
            <ta e="T62" id="Seg_3195" s="T61">giv-inactive-Q</ta>
            <ta e="T63" id="Seg_3196" s="T62">0.giv-active-Q</ta>
            <ta e="T66" id="Seg_3197" s="T65">0.giv-active-Q</ta>
            <ta e="T68" id="Seg_3198" s="T67">giv-inactive</ta>
            <ta e="T71" id="Seg_3199" s="T70">new</ta>
            <ta e="T73" id="Seg_3200" s="T72">giv-inactive</ta>
            <ta e="T74" id="Seg_3201" s="T73">accs-gen</ta>
            <ta e="T77" id="Seg_3202" s="T76">0.giv-active</ta>
            <ta e="T78" id="Seg_3203" s="T77">giv-inactive</ta>
            <ta e="T80" id="Seg_3204" s="T79">giv-inactive</ta>
            <ta e="T83" id="Seg_3205" s="T82">giv-inactive</ta>
            <ta e="T84" id="Seg_3206" s="T83">new</ta>
            <ta e="T86" id="Seg_3207" s="T85">accs-inf</ta>
            <ta e="T88" id="Seg_3208" s="T87">0.giv-active</ta>
            <ta e="T89" id="Seg_3209" s="T88">giv-active</ta>
            <ta e="T91" id="Seg_3210" s="T90">0.giv-active</ta>
            <ta e="T93" id="Seg_3211" s="T92">new</ta>
            <ta e="T94" id="Seg_3212" s="T93">0.giv-active</ta>
            <ta e="T97" id="Seg_3213" s="T96">new</ta>
            <ta e="T99" id="Seg_3214" s="T98">giv-inactive</ta>
            <ta e="T100" id="Seg_3215" s="T99">accs-aggr</ta>
            <ta e="T103" id="Seg_3216" s="T102">giv-active</ta>
            <ta e="T104" id="Seg_3217" s="T103">giv-active</ta>
            <ta e="T105" id="Seg_3218" s="T104">quot-sp</ta>
            <ta e="T106" id="Seg_3219" s="T105">accs-gen-Q</ta>
            <ta e="T107" id="Seg_3220" s="T106">giv-active-Q</ta>
            <ta e="T108" id="Seg_3221" s="T107">0.giv-inactive</ta>
            <ta e="T110" id="Seg_3222" s="T109">new</ta>
            <ta e="T111" id="Seg_3223" s="T110">accs-gen-Q</ta>
            <ta e="T112" id="Seg_3224" s="T111">0.giv-active-Q</ta>
            <ta e="T114" id="Seg_3225" s="T112">new-Q</ta>
            <ta e="T115" id="Seg_3226" s="T114">0.giv-active-Q</ta>
            <ta e="T116" id="Seg_3227" s="T115">accs-gen-Q</ta>
            <ta e="T117" id="Seg_3228" s="T116">0.giv-active-Q</ta>
            <ta e="T119" id="Seg_3229" s="T117">new-Q</ta>
            <ta e="T120" id="Seg_3230" s="T119">0.giv-active-Q</ta>
            <ta e="T121" id="Seg_3231" s="T120">0.accs-aggr</ta>
            <ta e="T122" id="Seg_3232" s="T121">0.giv-active</ta>
            <ta e="T124" id="Seg_3233" s="T123">giv-active</ta>
            <ta e="T126" id="Seg_3234" s="T124">giv-inactive</ta>
            <ta e="T130" id="Seg_3235" s="T129">giv-active</ta>
            <ta e="T132" id="Seg_3236" s="T130">giv-inactive</ta>
            <ta e="T134" id="Seg_3237" s="T133">giv-inactive</ta>
            <ta e="T136" id="Seg_3238" s="T135">giv-inactive</ta>
            <ta e="T137" id="Seg_3239" s="T136">0.giv-active quot-sp</ta>
            <ta e="T138" id="Seg_3240" s="T137">quot-sp 0.giv-active</ta>
            <ta e="T139" id="Seg_3241" s="T138">giv-inactive-Q</ta>
            <ta e="T140" id="Seg_3242" s="T139">accs-gen-Q</ta>
            <ta e="T142" id="Seg_3243" s="T140">giv-inactive-Q</ta>
            <ta e="T143" id="Seg_3244" s="T142">0.giv-active</ta>
            <ta e="T145" id="Seg_3245" s="T144">giv-inactive</ta>
            <ta e="T146" id="Seg_3246" s="T145">quot-sp</ta>
            <ta e="T147" id="Seg_3247" s="T146">accs-gen-Q</ta>
            <ta e="T148" id="Seg_3248" s="T147">0.giv-active quot-sp</ta>
            <ta e="T149" id="Seg_3249" s="T148">0.giv-inactive-Q</ta>
            <ta e="T150" id="Seg_3250" s="T149">new-Q</ta>
            <ta e="T151" id="Seg_3251" s="T150">0.giv-active-Q</ta>
            <ta e="T152" id="Seg_3252" s="T151">accs-aggr</ta>
            <ta e="T153" id="Seg_3253" s="T152">giv-active</ta>
            <ta e="T154" id="Seg_3254" s="T153">new</ta>
            <ta e="T155" id="Seg_3255" s="T154">0.giv-active</ta>
            <ta e="T156" id="Seg_3256" s="T155">0.giv-active</ta>
            <ta e="T158" id="Seg_3257" s="T157">giv-inactive</ta>
            <ta e="T159" id="Seg_3258" s="T158">accs-inf</ta>
            <ta e="T163" id="Seg_3259" s="T162">0.giv-active</ta>
            <ta e="T166" id="Seg_3260" s="T165">giv-inactive</ta>
            <ta e="T167" id="Seg_3261" s="T166">giv-active</ta>
            <ta e="T170" id="Seg_3262" s="T169">giv-active</ta>
            <ta e="T172" id="Seg_3263" s="T171">giv-inactive</ta>
            <ta e="T173" id="Seg_3264" s="T172">quot-sp</ta>
            <ta e="T174" id="Seg_3265" s="T173">accs-gen-Q</ta>
            <ta e="T175" id="Seg_3266" s="T174">0.giv-active quot-sp</ta>
            <ta e="T177" id="Seg_3267" s="T176">accs-gen-Q</ta>
            <ta e="T180" id="Seg_3268" s="T179">giv-inactive</ta>
            <ta e="T181" id="Seg_3269" s="T180">giv-inactive</ta>
            <ta e="T183" id="Seg_3270" s="T182">accs-gen</ta>
            <ta e="T184" id="Seg_3271" s="T183">0.giv-active</ta>
            <ta e="T185" id="Seg_3272" s="T184">accs-inf</ta>
            <ta e="T186" id="Seg_3273" s="T185">0.giv-active</ta>
            <ta e="T188" id="Seg_3274" s="T187">new-Q</ta>
            <ta e="T191" id="Seg_3275" s="T190">giv-active-Q</ta>
            <ta e="T194" id="Seg_3276" s="T193">giv-inactive</ta>
            <ta e="T195" id="Seg_3277" s="T194">giv-inactive</ta>
            <ta e="T196" id="Seg_3278" s="T195">quot-sp</ta>
            <ta e="T197" id="Seg_3279" s="T196">giv-active-Q</ta>
            <ta e="T200" id="Seg_3280" s="T199">giv-inactive-Q</ta>
            <ta e="T201" id="Seg_3281" s="T200">accs-gen-Q</ta>
            <ta e="T202" id="Seg_3282" s="T201">accs-gen-Q</ta>
            <ta e="T203" id="Seg_3283" s="T202">quot-sp-Q</ta>
            <ta e="T204" id="Seg_3284" s="T203">0.giv-active-Q</ta>
            <ta e="T205" id="Seg_3285" s="T204">giv-inactive-Q</ta>
            <ta e="T207" id="Seg_3286" s="T206">giv-active</ta>
            <ta e="T209" id="Seg_3287" s="T208">accs-gen</ta>
            <ta e="T210" id="Seg_3288" s="T209">0.giv-active-Q</ta>
            <ta e="T211" id="Seg_3289" s="T210">giv-inactive-Q</ta>
            <ta e="T213" id="Seg_3290" s="T212">new</ta>
            <ta e="T215" id="Seg_3291" s="T214">0.giv-active</ta>
            <ta e="T216" id="Seg_3292" s="T215">0.giv-inactive</ta>
            <ta e="T217" id="Seg_3293" s="T216">new</ta>
            <ta e="T218" id="Seg_3294" s="T217">giv-active</ta>
            <ta e="T221" id="Seg_3295" s="T220">giv-active</ta>
            <ta e="T224" id="Seg_3296" s="T223">giv-inactive</ta>
            <ta e="T225" id="Seg_3297" s="T224">0.giv-active</ta>
            <ta e="T227" id="Seg_3298" s="T226">accs-gen</ta>
            <ta e="T229" id="Seg_3299" s="T228">giv-inactive</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T7" id="Seg_3300" s="T6">TAT:cult</ta>
            <ta e="T8" id="Seg_3301" s="T7">RUS:cult</ta>
            <ta e="T14" id="Seg_3302" s="T13">RUS:disc</ta>
            <ta e="T128" id="Seg_3303" s="T127">RUS:gram</ta>
            <ta e="T164" id="Seg_3304" s="T163">RUS:gram</ta>
            <ta e="T213" id="Seg_3305" s="T212">TURK:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_3306" s="T0">У одного человека было три дочери.</ta>
            <ta e="T9" id="Seg_3307" s="T4">Этот человек должен был в город служить идти.</ta>
            <ta e="T11" id="Seg_3308" s="T9">Старшая его дочь: </ta>
            <ta e="T13" id="Seg_3309" s="T11">"Я пойду."</ta>
            <ta e="T17" id="Seg_3310" s="T13">"Ну иди!" — сказал старик.</ta>
            <ta e="T23" id="Seg_3311" s="T17">Обрезала она свои волосы и в мужскую одежду переоделась.</ta>
            <ta e="T24" id="Seg_3312" s="T23">Пошла.</ta>
            <ta e="T28" id="Seg_3313" s="T24">Старик побежал и медведем обернулся.</ta>
            <ta e="T31" id="Seg_3314" s="T28">На дорогу вышел и встал.</ta>
            <ta e="T36" id="Seg_3315" s="T31">Девушка развернулась и обратно пошла.</ta>
            <ta e="T37" id="Seg_3316" s="T36">Он говорит: </ta>
            <ta e="T39" id="Seg_3317" s="T37">"Отчего обратно пришла?"</ta>
            <ta e="T40" id="Seg_3318" s="T39">"Испугалась я.</ta>
            <ta e="T43" id="Seg_3319" s="T40">Медведь по той дороге ходит."</ta>
            <ta e="T46" id="Seg_3320" s="T43">Средняя дочь и говорит: </ta>
            <ta e="T48" id="Seg_3321" s="T46">"Я пойду."</ta>
            <ta e="T56" id="Seg_3322" s="T48">Снова этот старик, обернувшись медведем, на дороге стоит.</ta>
            <ta e="T59" id="Seg_3323" s="T56">[И она] тоже назад вернулась.</ta>
            <ta e="T61" id="Seg_3324" s="T59">"Отчего ты вернулась?"</ta>
            <ta e="T66" id="Seg_3325" s="T61">"Я медведя увидела и обратно пришла."</ta>
            <ta e="T69" id="Seg_3326" s="T66">Младшая дочь пошла.</ta>
            <ta e="T72" id="Seg_3327" s="T69">В мужскую одежду переоделась.</ta>
            <ta e="T76" id="Seg_3328" s="T72">Волосы себе обрезала.</ta>
            <ta e="T77" id="Seg_3329" s="T76">Пошла.</ta>
            <ta e="T82" id="Seg_3330" s="T77">Старик вновь побежал, в медведя превратившись.</ta>
            <ta e="T88" id="Seg_3331" s="T82">Дочь его стрелой выстрелила, глаз ему прострелила.</ta>
            <ta e="T90" id="Seg_3332" s="T88">А сама ушла.</ta>
            <ta e="T94" id="Seg_3333" s="T90">Шла, к одной женщине пришла.</ta>
            <ta e="T98" id="Seg_3334" s="T94">У той женщины был сын.</ta>
            <ta e="T101" id="Seg_3335" s="T98">И отправились они с ней в путь.</ta>
            <ta e="T105" id="Seg_3336" s="T101">Эта женщина говорит сыну:</ta>
            <ta e="T106" id="Seg_3337" s="T105">"Девушка это!</ta>
            <ta e="T110" id="Seg_3338" s="T106">Пойди с ней (/с ним) в лавку!</ta>
            <ta e="T120" id="Seg_3339" s="T110">Если это парень, на конскую упряжь будет смотреть, а если девица, на женские платья будет смотреть."</ta>
            <ta e="T122" id="Seg_3340" s="T120">Пошли они, пришли [в лавку].</ta>
            <ta e="T133" id="Seg_3341" s="T122">Девушка упряжь разглядывает, а парень на женские платья смотрит.</ta>
            <ta e="T137" id="Seg_3342" s="T133">Домой пришли когда, рассказывает он матери своей.</ta>
            <ta e="T138" id="Seg_3343" s="T137">Говорит:</ta>
            <ta e="T143" id="Seg_3344" s="T138">"Парень это, упряжь разглядывает".</ta>
            <ta e="T146" id="Seg_3345" s="T143">Женщина говорит:</ta>
            <ta e="T147" id="Seg_3346" s="T146">"Девица!"</ta>
            <ta e="T148" id="Seg_3347" s="T147">Говорит:</ta>
            <ta e="T151" id="Seg_3348" s="T148">"Пойди [с ней] в баню! Идите!"</ta>
            <ta e="T153" id="Seg_3349" s="T151">Пошли они в баню.</ta>
            <ta e="T155" id="Seg_3350" s="T153">Он мыло свое забыл.</ta>
            <ta e="T156" id="Seg_3351" s="T155">Вернулся.</ta>
            <ta e="T163" id="Seg_3352" s="T156">Девушка голову моет, не раздеваясь.</ta>
            <ta e="T168" id="Seg_3353" s="T163">Когда парень обратно пришел, девушка уже помылась.</ta>
            <ta e="T173" id="Seg_3354" s="T168">Парень, вернувшись, матери говорит:</ta>
            <ta e="T177" id="Seg_3355" s="T173">"Парень это", говорит, "не девица."</ta>
            <ta e="T182" id="Seg_3356" s="T177">Затем та девушка домой отправляется.</ta>
            <ta e="T184" id="Seg_3357" s="T182">Реку перешла,</ta>
            <ta e="T186" id="Seg_3358" s="T184">грудь показывает: </ta>
            <ta e="T192" id="Seg_3359" s="T186">"От хлеба вашего грудь у меня как выросла."</ta>
            <ta e="T196" id="Seg_3360" s="T192">Женщина сына ругает:</ta>
            <ta e="T199" id="Seg_3361" s="T196">"Ты почему наврал?</ta>
            <ta e="T203" id="Seg_3362" s="T199">Девица это, а ты сказал, что парень.</ta>
            <ta e="T205" id="Seg_3363" s="T203">[Теперь] она домой ушла!"</ta>
            <ta e="T209" id="Seg_3364" s="T205">Женщина своего духа послала:</ta>
            <ta e="T212" id="Seg_3365" s="T209">"Пойди и найди ее!"</ta>
            <ta e="T215" id="Seg_3366" s="T212">Обернулся тот сорокой и улетел.</ta>
            <ta e="T217" id="Seg_3367" s="T215">[Девушка] в своей кровати спит.</ta>
            <ta e="T220" id="Seg_3368" s="T217">[Сорока] девушку с кроватью вместе подняв, унесла прочь.</ta>
            <ta e="T225" id="Seg_3369" s="T220">Она проснулась, когда реку пересекала.</ta>
            <ta e="T228" id="Seg_3370" s="T225">Принесли её в это место.</ta>
            <ta e="T231" id="Seg_3371" s="T228">Выдали замуж за сына.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_3372" s="T0">A man had three daughters. </ta>
            <ta e="T9" id="Seg_3373" s="T4">This man had to go to town to serve.</ta>
            <ta e="T11" id="Seg_3374" s="T9">His elder daughter:</ta>
            <ta e="T13" id="Seg_3375" s="T11">"I will go."</ta>
            <ta e="T17" id="Seg_3376" s="T13">"Well, go!" said the man.</ta>
            <ta e="T23" id="Seg_3377" s="T17">She cut off her hair and put a men’s dress on.</ta>
            <ta e="T24" id="Seg_3378" s="T23">She started to go.</ta>
            <ta e="T28" id="Seg_3379" s="T24">The man ran becoming a bear.</ta>
            <ta e="T31" id="Seg_3380" s="T28">He went and stood on the road.</ta>
            <ta e="T36" id="Seg_3381" s="T31">The girl turned back and returned.</ta>
            <ta e="T37" id="Seg_3382" s="T36">He says:</ta>
            <ta e="T39" id="Seg_3383" s="T37">"​​Why did you come back?"</ta>
            <ta e="T40" id="Seg_3384" s="T39">"I was afraid.</ta>
            <ta e="T43" id="Seg_3385" s="T40">A bear is strolling on the road.‎‎"</ta>
            <ta e="T46" id="Seg_3386" s="T43">His middle daughter says:</ta>
            <ta e="T48" id="Seg_3387" s="T46">"I will go."</ta>
            <ta e="T56" id="Seg_3388" s="T48">After the man had again turned into a bear he was again standing on the road.</ta>
            <ta e="T59" id="Seg_3389" s="T56">Again she came back.</ta>
            <ta e="T61" id="Seg_3390" s="T59">"Why did you come back?"</ta>
            <ta e="T66" id="Seg_3391" s="T61">"I saw a bear, then I returned."</ta>
            <ta e="T69" id="Seg_3392" s="T66">The youngest daughter went off.</ta>
            <ta e="T72" id="Seg_3393" s="T69">She put on men’s clothes.</ta>
            <ta e="T76" id="Seg_3394" s="T72">She cut off her hair.</ta>
            <ta e="T77" id="Seg_3395" s="T76">She got going.</ta>
            <ta e="T82" id="Seg_3396" s="T77">The man ran again becoming a bear.</ta>
            <ta e="T88" id="Seg_3397" s="T82">His daughter shot with an arrow, shot bursting his eye.</ta>
            <ta e="T90" id="Seg_3398" s="T88">She herself went on.</ta>
            <ta e="T94" id="Seg_3399" s="T90">She went and got to one woman.</ta>
            <ta e="T98" id="Seg_3400" s="T94">This woman had a son.</ta>
            <ta e="T101" id="Seg_3401" s="T98">With her they wander.</ta>
            <ta e="T105" id="Seg_3402" s="T101">The woman says to her son:</ta>
            <ta e="T106" id="Seg_3403" s="T105">"[She is] a girl!</ta>
            <ta e="T110" id="Seg_3404" s="T106">Go with her to the store!</ta>
            <ta e="T120" id="Seg_3405" s="T110">If she’s a boy she will look at horse gear, if she’s a girl she will look at women’s dresses."</ta>
            <ta e="T122" id="Seg_3406" s="T120">They departed, arrived [to the store].</ta>
            <ta e="T133" id="Seg_3407" s="T122">The girl is looking at the horse gear, but the boy is looking at women’s dresses.</ta>
            <ta e="T137" id="Seg_3408" s="T133">After they came home he tells his mother.</ta>
            <ta e="T138" id="Seg_3409" s="T137">He says:</ta>
            <ta e="T143" id="Seg_3410" s="T138">"He’s a boy, he looks at horse gear."</ta>
            <ta e="T146" id="Seg_3411" s="T143">The woman says:</ta>
            <ta e="T147" id="Seg_3412" s="T146">‎"She is a girl!"</ta>
            <ta e="T148" id="Seg_3413" s="T147">She says:</ta>
            <ta e="T151" id="Seg_3414" s="T148">"Go to the sauna [with her], go!"</ta>
            <ta e="T153" id="Seg_3415" s="T151">They went to the sauna.</ta>
            <ta e="T155" id="Seg_3416" s="T153">He had forgotten his soap.</ta>
            <ta e="T156" id="Seg_3417" s="T155">He went back.</ta>
            <ta e="T163" id="Seg_3418" s="T156">The girl washed her head, she did not undress.</ta>
            <ta e="T168" id="Seg_3419" s="T163">And when the boy came back she had already washed herself.</ta>
            <ta e="T173" id="Seg_3420" s="T168">After the boy had come back he told his mother:</ta>
            <ta e="T177" id="Seg_3421" s="T173">"[It is] a boy," he says, "not a girl."</ta>
            <ta e="T182" id="Seg_3422" s="T177">Then the girl is going home.</ta>
            <ta e="T184" id="Seg_3423" s="T182">She crossed the river.</ta>
            <ta e="T186" id="Seg_3424" s="T184">She shows her breast:</ta>
            <ta e="T192" id="Seg_3425" s="T186">"How my breast has grown from your bread."</ta>
            <ta e="T196" id="Seg_3426" s="T192">The woman scolded her son:</ta>
            <ta e="T199" id="Seg_3427" s="T196">"Why did you lie?</ta>
            <ta e="T203" id="Seg_3428" s="T199">She’s a girl, and you said a boy.</ta>
            <ta e="T205" id="Seg_3429" s="T203">She went home!‎‎"</ta>
            <ta e="T209" id="Seg_3430" s="T205">The woman sent her puck:</ta>
            <ta e="T212" id="Seg_3431" s="T209">"Go and find her!"</ta>
            <ta e="T215" id="Seg_3432" s="T212">He turned into a magpie and flew off.</ta>
            <ta e="T217" id="Seg_3433" s="T215">[The girl] is sleeping in her bed.</ta>
            <ta e="T220" id="Seg_3434" s="T217">With her bed she was lifted up and taken away.</ta>
            <ta e="T225" id="Seg_3435" s="T220">She woke up where she was crossing the river.</ta>
            <ta e="T228" id="Seg_3436" s="T225">They had brought her to this place.</ta>
            <ta e="T231" id="Seg_3437" s="T228">They gave her [as a wife] to the son.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_3438" s="T0">Ein Mann hatte drei Töchter.</ta>
            <ta e="T9" id="Seg_3439" s="T4">Dieser Mann musste in die Siedlung um zu dienen.</ta>
            <ta e="T11" id="Seg_3440" s="T9">Seine älteste Tochter:</ta>
            <ta e="T13" id="Seg_3441" s="T11">„Ich werde gehen."</ta>
            <ta e="T17" id="Seg_3442" s="T13">"Na gut, geh!", sagt der Mann.</ta>
            <ta e="T23" id="Seg_3443" s="T17">Sie schnitt ihre Haare ab und zog sich Männerkleidung an.</ta>
            <ta e="T24" id="Seg_3444" s="T23">Sie ging los.</ta>
            <ta e="T28" id="Seg_3445" s="T24">Der Mann lief hinterher und verwandelte sich in einen Bären.</ta>
            <ta e="T31" id="Seg_3446" s="T28">Er ging und stand auf dem Weg.</ta>
            <ta e="T36" id="Seg_3447" s="T31">Die Tochter kehrte um und kam zurück. </ta>
            <ta e="T37" id="Seg_3448" s="T36">Er sagt: </ta>
            <ta e="T39" id="Seg_3449" s="T37">"Warum bist du zurückgekommen?"</ta>
            <ta e="T40" id="Seg_3450" s="T39">"Ich hatte Angst.</ta>
            <ta e="T43" id="Seg_3451" s="T40">Ein Bär läuft auf dem Weg herum."</ta>
            <ta e="T46" id="Seg_3452" s="T43">Seine mittlere Tochter sagt:</ta>
            <ta e="T48" id="Seg_3453" s="T46">"Ich werde gehen."</ta>
            <ta e="T56" id="Seg_3454" s="T48">Nachdem der Mann sich erneut in einen Bären verwandelt hatte, stand er wieder auf dem Weg.</ta>
            <ta e="T59" id="Seg_3455" s="T56">Auch sie kam zurück.</ta>
            <ta e="T61" id="Seg_3456" s="T59">"Warum bist du zurückgekommen?"</ta>
            <ta e="T66" id="Seg_3457" s="T61">"Ich sah einen Bären, dann bin ich zurückgekommen."</ta>
            <ta e="T69" id="Seg_3458" s="T66">Seine jüngste Tochter ging.</ta>
            <ta e="T72" id="Seg_3459" s="T69">Sie zog sich Männerkleidung an.</ta>
            <ta e="T76" id="Seg_3460" s="T72">Sie schnitt ihre Haare ab.</ta>
            <ta e="T77" id="Seg_3461" s="T76">Sie ging los.</ta>
            <ta e="T82" id="Seg_3462" s="T77">Der Mann lief wieder hinterher und verwandelte sich in einen Bären.</ta>
            <ta e="T88" id="Seg_3463" s="T82">Das Mädchen schoss einen Pfeil ab und zerschoss sein Auge.</ta>
            <ta e="T90" id="Seg_3464" s="T88">Sie selbst ging fort.</ta>
            <ta e="T94" id="Seg_3465" s="T90">Sie wanderte und kehrte bei einer Frau ein.</ta>
            <ta e="T98" id="Seg_3466" s="T94">Diese Frau hatte einen Sohn.</ta>
            <ta e="T101" id="Seg_3467" s="T98">Sie wandern mit ihr.</ta>
            <ta e="T105" id="Seg_3468" s="T101">Die Frau sagt zu ihrem Sohn:</ta>
            <ta e="T106" id="Seg_3469" s="T105">„[Sie ist] ein Mädchen!</ta>
            <ta e="T110" id="Seg_3470" s="T106">Geh mit ihr zum Kaufladen!</ta>
            <ta e="T120" id="Seg_3471" s="T110">Ist es ein Junge, wird er sich die Pferdegeschirre ansehen, ist es ein Mädchen, wird sie sich die Frauenkleider ansehen."</ta>
            <ta e="T122" id="Seg_3472" s="T120">Sie gingen und kamen an.</ta>
            <ta e="T133" id="Seg_3473" s="T122">Das Mädchen sieht sich die Pferdegeschirre an, doch der Junge sieht sich die Frauenkleider an.</ta>
            <ta e="T137" id="Seg_3474" s="T133">Nachdem er nach Hause gekommen ist, berichtet er seiner Mutter.</ta>
            <ta e="T138" id="Seg_3475" s="T137">Er sagt:</ta>
            <ta e="T143" id="Seg_3476" s="T138">„Er ist ein Junge, er sieht sich Pferdegeschirre an.“</ta>
            <ta e="T146" id="Seg_3477" s="T143">Die Frau sagt:</ta>
            <ta e="T147" id="Seg_3478" s="T146">"Sie ist ein Mädchen!"</ta>
            <ta e="T148" id="Seg_3479" s="T147">Sie sagt:</ta>
            <ta e="T151" id="Seg_3480" s="T148">"Geh in die Sauna. Geht!"</ta>
            <ta e="T153" id="Seg_3481" s="T151">Sie gingen in die Sauna.</ta>
            <ta e="T155" id="Seg_3482" s="T153">Er hatte seine Seife vergessen.</ta>
            <ta e="T156" id="Seg_3483" s="T155">Er ging zurück.</ta>
            <ta e="T163" id="Seg_3484" s="T156">Das Mädchen wusch ihren Kopf und zog sich nicht aus.</ta>
            <ta e="T168" id="Seg_3485" s="T163">Als der Junge kam, war sie fertig gewaschen.</ta>
            <ta e="T173" id="Seg_3486" s="T168">Nachdem der Junge nach Hause gekommen ist, berichtet er seiner Mutter:</ta>
            <ta e="T177" id="Seg_3487" s="T173">"[Er ist] ein Junge“, sagt er, „kein Mädchen."</ta>
            <ta e="T182" id="Seg_3488" s="T177">Dann geht das Mädchen nach Hause.</ta>
            <ta e="T184" id="Seg_3489" s="T182">Sie überquerte den Fluss.</ta>
            <ta e="T186" id="Seg_3490" s="T184">Sie zeigt ihren Busen:</ta>
            <ta e="T192" id="Seg_3491" s="T186">„Wie groß mein Busen von eurem Brot geworden ist!“</ta>
            <ta e="T196" id="Seg_3492" s="T192">Die Frau schimpft mit ihrem Sohn:</ta>
            <ta e="T199" id="Seg_3493" s="T196">"Du, warum hast du gelogen?</ta>
            <ta e="T203" id="Seg_3494" s="T199">Sie ist ein Mädchen, du sagtest, ein Junge''.</ta>
            <ta e="T205" id="Seg_3495" s="T203">Nun ging sie nach Hause!"</ta>
            <ta e="T209" id="Seg_3496" s="T205">Die Frau schickte ihren Hausgeist los:</ta>
            <ta e="T212" id="Seg_3497" s="T209">"Geh und finde sie!"</ta>
            <ta e="T215" id="Seg_3498" s="T212">Er verwandelte sich in eine Elster und flog los.</ta>
            <ta e="T217" id="Seg_3499" s="T215">[Das Mädchen] schläft in ihrem Bett.</ta>
            <ta e="T220" id="Seg_3500" s="T217">Mit dem Bett wurde sie angehoben und entführt.</ta>
            <ta e="T225" id="Seg_3501" s="T220">Sie erwachte, wo sie den Fluss überquert hatte.</ta>
            <ta e="T228" id="Seg_3502" s="T225">An diese Stelle wurde sie gebracht.</ta>
            <ta e="T231" id="Seg_3503" s="T228">[Das Mädchen] wurde für den Sohn geholt.</ta>
         </annotation>
         <annotation name="ltg" tierref="ltg">
            <ta e="T4" id="Seg_3504" s="T0">Ein Alter hatte ⌈seine⌉ drei Töchter.</ta>
            <ta e="T9" id="Seg_3505" s="T4">Diesen Alten zum Dienen auf dem Gehöft [kommt man] aufzufordern.</ta>
            <ta e="T11" id="Seg_3506" s="T9">Seine älteste Tochter:</ta>
            <ta e="T13" id="Seg_3507" s="T11">»Ich gehe.»</ta>
            <ta e="T17" id="Seg_3508" s="T13">»Nun, gehe!» sagt der Alte.</ta>
            <ta e="T23" id="Seg_3509" s="T17">Ihre Haare aber schnitt sie ab, kleidete sich in Männerkleider,</ta>
            <ta e="T24" id="Seg_3510" s="T23">ging.</ta>
            <ta e="T28" id="Seg_3511" s="T24">Der Alte lief, stellt sich als Bär.</ta>
            <ta e="T31" id="Seg_3512" s="T28">Auf dem Wege geht er, stand.</ta>
            <ta e="T36" id="Seg_3513" s="T31">Die Tochter kehrte zurück, zurückkehrend kam sie.</ta>
            <ta e="T37" id="Seg_3514" s="T36">Er sagt</ta>
            <ta e="T39" id="Seg_3515" s="T37">»Warum kamst du zurück?»</ta>
            <ta e="T40" id="Seg_3516" s="T39">»Ich fürchtete mich,</ta>
            <ta e="T43" id="Seg_3517" s="T40">ein Bär geht auf dem Wege.»</ta>
            <ta e="T46" id="Seg_3518" s="T43">Seine mittlere Tochter sagt:</ta>
            <ta e="T48" id="Seg_3519" s="T46">»Ich gehe.»</ta>
            <ta e="T56" id="Seg_3520" s="T48">Nachdem dieser Alte wieder sich in einen Bären verwandelt hatte, auf dem Wege steht er wieder.</ta>
            <ta e="T59" id="Seg_3521" s="T56">Wiederum zurückkehrend kam sie.</ta>
            <ta e="T61" id="Seg_3522" s="T59">»Warum kamst du zurück?»</ta>
            <ta e="T66" id="Seg_3523" s="T61">»Ich sah einen Bären, dann zurückkehrend ich kam.»</ta>
            <ta e="T69" id="Seg_3524" s="T66">Seine kleinste Tochter ging,</ta>
            <ta e="T72" id="Seg_3525" s="T69">kleidete sich in Männerkleider.</ta>
            <ta e="T76" id="Seg_3526" s="T72">Die Haare schnitt sie ab,</ta>
            <ta e="T77" id="Seg_3527" s="T76">ging.</ta>
            <ta e="T82" id="Seg_3528" s="T77">Der Alte sich wieder als Bär stellend lief.</ta>
            <ta e="T88" id="Seg_3529" s="T82">Das Mädchen mit dem Pfeil schoss, das Auge aber schoss sie entzwei.</ta>
            <ta e="T90" id="Seg_3530" s="T88">Selbst ging sie,</ta>
            <ta e="T94" id="Seg_3531" s="T90">wanderte, bei einem alten Weibe liess sie sich nieder.</ta>
            <ta e="T98" id="Seg_3532" s="T94">Dieses alte Weib hatte ⌈ihren⌉ Sohn.</ta>
            <ta e="T101" id="Seg_3533" s="T98">Mit ihm gehen sie.</ta>
            <ta e="T105" id="Seg_3534" s="T101">Dieses alte Weib sagt zu ihrem Sohn:</ta>
            <ta e="T110" id="Seg_3535" s="T106">»Mit ihm geht in den Kaufladen!</ta>
            <ta e="T120" id="Seg_3536" s="T110">Wenn es ein Bursche ist, betrachtet er die Pferdegeschirre, wenn es ein Mädchen ist, schaut sie auf die Frauenkleider.»</ta>
            <ta e="T122" id="Seg_3537" s="T120">Sie gingen, kamen.</ta>
            <ta e="T133" id="Seg_3538" s="T122">Das Mädchen betrachtet die Pferdegeschirre, aber der Sohn betrachtet die Frauenkleider.</ta>
            <ta e="T137" id="Seg_3539" s="T133">Nachdem er nach Hause gekommen war, erzählt er seiner Mutter,</ta>
            <ta e="T138" id="Seg_3540" s="T137">sagt:</ta>
            <ta e="T143" id="Seg_3541" s="T138">»Er ist ein Knabe, betrachtet Pferdegeschirre».</ta>
            <ta e="T146" id="Seg_3542" s="T143">Das alte Weib sagt:</ta>
            <ta e="T147" id="Seg_3543" s="T146">»Ein Mädchen [ist sie]»,</ta>
            <ta e="T148" id="Seg_3544" s="T147">sagt sie,</ta>
            <ta e="T151" id="Seg_3545" s="T148">»gehe in die Badestube. Geht!»</ta>
            <ta e="T153" id="Seg_3546" s="T151">Sie gingen in die Badestube.</ta>
            <ta e="T155" id="Seg_3547" s="T153">Er vergass seine Seife,</ta>
            <ta e="T156" id="Seg_3548" s="T155">kam zurück.</ta>
            <ta e="T163" id="Seg_3549" s="T156">Das Mädchen wusch sich den Kopf, sich ausziehend flocht sie nicht auf.</ta>
            <ta e="T168" id="Seg_3550" s="T163">Er kam. Der Bursche wusch sich.</ta>
            <ta e="T173" id="Seg_3551" s="T168">Der Sohn, nachdem er gekommen war, seiner Mutter erzählt:</ta>
            <ta e="T177" id="Seg_3552" s="T173">»Knabe», sagt er, »kein Mädchen.»</ta>
            <ta e="T182" id="Seg_3553" s="T177">Dann das Mädchen nach Hause sich begebend geht.</ta>
            <ta e="T184" id="Seg_3554" s="T182">Ging über den Fluss,</ta>
            <ta e="T186" id="Seg_3555" s="T184">zeigt die Brust:</ta>
            <ta e="T192" id="Seg_3556" s="T186">»Von eurem Brote lebend wie wuchs meine Brust.»</ta>
            <ta e="T196" id="Seg_3557" s="T192">Das alte Weib schimpft den Sohn:</ta>
            <ta e="T199" id="Seg_3558" s="T196">»Du, warum logst du?</ta>
            <ta e="T203" id="Seg_3559" s="T199">Sie ist ein Mädchen, einen Knaben nanntest du [sie].»</ta>
            <ta e="T205" id="Seg_3560" s="T203">Sie ging nach Hause.</ta>
            <ta e="T209" id="Seg_3561" s="T205">Das alte Weib schickte den Hausgeist:</ta>
            <ta e="T212" id="Seg_3562" s="T209">»Suche sie, gehe sofort!»</ta>
            <ta e="T215" id="Seg_3563" s="T212">Sich als Elster stellend liess er sich vom Fluge herab.</ta>
            <ta e="T217" id="Seg_3564" s="T215">[Das Mädchen] schläft in ihrem Bett.</ta>
            <ta e="T220" id="Seg_3565" s="T217">Hervorziehend hob er [sie] auf.</ta>
            <ta e="T225" id="Seg_3566" s="T220">Sie erwachte. Wo sie über den Fluss geht,</ta>
            <ta e="T228" id="Seg_3567" s="T225">an diese Stelle brachte er [sie].</ta>
            <ta e="T231" id="Seg_3568" s="T228">Sie nahm für ihren Sohn (zum Weibe).</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltg"
                          display-name="ltg"
                          name="ltg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
