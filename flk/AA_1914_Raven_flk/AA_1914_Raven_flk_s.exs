<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDB9EFF3E9-DB0F-75A9-E41A-229264696BA9">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\AA_1914_Raven_flk\AA_1914_Raven_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">118</ud-information>
            <ud-information attribute-name="# HIAT:w">84</ud-information>
            <ud-information attribute-name="# e">84</ud-information>
            <ud-information attribute-name="# HIAT:u">23</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="AA">
            <abbreviation>AA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="AA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T84" id="Seg_0" n="sc" s="T0">
               <ts e="T2" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Uražə</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">amna</ts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T6" id="Seg_11" n="HIAT:u" s="T2">
                  <ts e="T3" id="Seg_13" n="HIAT:w" s="T2">Uražən</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">šide</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">nüket</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">ibi</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_26" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">Oʔb</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">nüket</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">baška</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">tibizʼəʔ</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">amnobi</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_44" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">Dĭ</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">kuza</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">kăštəliat:</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_54" n="HIAT:ip">"</nts>
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">Kanžəbəj</ts>
                  <nts id="Seg_57" n="HIAT:ip">!</nts>
                  <nts id="Seg_58" n="HIAT:ip">"</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_61" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_63" n="HIAT:w" s="T15">Nunanə</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_66" n="HIAT:w" s="T16">külin</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_69" n="HIAT:w" s="T17">moːt</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_72" n="HIAT:w" s="T18">nʼekezeŋdə</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_75" n="HIAT:w" s="T19">kusʼtə</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_78" n="HIAT:w" s="T20">kambiiʔ</ts>
                  <nts id="Seg_79" n="HIAT:ip">.</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_82" n="HIAT:u" s="T21">
                  <nts id="Seg_83" n="HIAT:ip">"</nts>
                  <ts e="T22" id="Seg_85" n="HIAT:w" s="T21">Sʼaʔ</ts>
                  <nts id="Seg_86" n="HIAT:ip">!</nts>
                  <nts id="Seg_87" n="HIAT:ip">"</nts>
               </ts>
               <ts e="T24" id="Seg_89" n="HIAT:u" s="T22">
                  <nts id="Seg_90" n="HIAT:ip">,</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_93" n="HIAT:w" s="T22">măndə</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_96" n="HIAT:w" s="T23">Uražəndə</ts>
                  <nts id="Seg_97" n="HIAT:ip">.</nts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_100" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_102" n="HIAT:w" s="T24">Uražə</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_105" n="HIAT:w" s="T25">sʼabi</ts>
                  <nts id="Seg_106" n="HIAT:ip">,</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_109" n="HIAT:w" s="T26">üštöbi</ts>
                  <nts id="Seg_110" n="HIAT:ip">.</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_113" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_115" n="HIAT:w" s="T27">Teʔmezʼəʔ</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_118" n="HIAT:w" s="T28">üštöbi</ts>
                  <nts id="Seg_119" n="HIAT:ip">.</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_122" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_124" n="HIAT:w" s="T29">Teʔmebə</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_127" n="HIAT:w" s="T30">saj</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_130" n="HIAT:w" s="T31">băppi</ts>
                  <nts id="Seg_131" n="HIAT:ip">.</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_134" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_136" n="HIAT:w" s="T32">Uražə</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_139" n="HIAT:w" s="T33">ibəleʔ</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_142" n="HIAT:w" s="T34">kojobi</ts>
                  <nts id="Seg_143" n="HIAT:ip">.</nts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_146" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_148" n="HIAT:w" s="T35">Kambi</ts>
                  <nts id="Seg_149" n="HIAT:ip">,</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_152" n="HIAT:w" s="T36">uražən</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_155" n="HIAT:w" s="T37">šide</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_158" n="HIAT:w" s="T38">nükebə</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_161" n="HIAT:w" s="T39">ileʔ</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_164" n="HIAT:w" s="T40">kumbi</ts>
                  <nts id="Seg_165" n="HIAT:ip">.</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_168" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_170" n="HIAT:w" s="T41">Dĭgəttə</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_173" n="HIAT:w" s="T42">sʼüʔleʔ</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_176" n="HIAT:w" s="T43">kandəgaiʔ</ts>
                  <nts id="Seg_177" n="HIAT:ip">.</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_180" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_182" n="HIAT:w" s="T44">Uražən</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_185" n="HIAT:w" s="T45">tʼaktə</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_188" n="HIAT:w" s="T46">nüket</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_191" n="HIAT:w" s="T47">tʼorlaʔ</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_194" n="HIAT:w" s="T48">kandəga</ts>
                  <nts id="Seg_195" n="HIAT:ip">.</nts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_198" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_200" n="HIAT:w" s="T49">Uražə</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_203" n="HIAT:w" s="T50">măndolamnə</ts>
                  <nts id="Seg_204" n="HIAT:ip">.</nts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_207" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_209" n="HIAT:w" s="T51">Külin</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_212" n="HIAT:w" s="T52">nʼekezeŋdətsʼəʔ</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_215" n="HIAT:w" s="T53">amoria</ts>
                  <nts id="Seg_216" n="HIAT:ip">.</nts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_219" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_221" n="HIAT:w" s="T54">Külin</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_224" n="HIAT:w" s="T55">nʼekezeŋdə</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_227" n="HIAT:w" s="T56">dĭnə</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_230" n="HIAT:w" s="T57">tüšəlaːmbi</ts>
                  <nts id="Seg_231" n="HIAT:ip">.</nts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_234" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_236" n="HIAT:w" s="T58">Kaləŋgəndə</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_239" n="HIAT:w" s="T59">šide</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_242" n="HIAT:w" s="T60">nʼekebə</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_245" n="HIAT:w" s="T61">sarbi</ts>
                  <nts id="Seg_246" n="HIAT:ip">,</nts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_249" n="HIAT:w" s="T62">tʼünə</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_252" n="HIAT:w" s="T63">nʼergöleʔ</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_255" n="HIAT:w" s="T64">üzəbi</ts>
                  <nts id="Seg_256" n="HIAT:ip">,</nts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_259" n="HIAT:w" s="T65">kambi</ts>
                  <nts id="Seg_260" n="HIAT:ip">.</nts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_263" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_265" n="HIAT:w" s="T66">Büjzengən</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_268" n="HIAT:w" s="T67">bügən</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_271" n="HIAT:w" s="T68">amnə</ts>
                  <nts id="Seg_272" n="HIAT:ip">.</nts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_275" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_277" n="HIAT:w" s="T69">Tʼaktə</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_280" n="HIAT:w" s="T70">nüket</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_283" n="HIAT:w" s="T71">büjleʔ</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_286" n="HIAT:w" s="T72">šobi</ts>
                  <nts id="Seg_287" n="HIAT:ip">.</nts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_290" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_292" n="HIAT:w" s="T73">Nörbəlia:</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_294" n="HIAT:ip">"</nts>
                  <ts e="T75" id="Seg_296" n="HIAT:w" s="T74">Inəm</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_299" n="HIAT:w" s="T75">möːm</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_302" n="HIAT:w" s="T76">tettə</ts>
                  <nts id="Seg_303" n="HIAT:ip">!</nts>
                  <nts id="Seg_304" n="HIAT:ip">"</nts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_307" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_309" n="HIAT:w" s="T77">Dĭ</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_312" n="HIAT:w" s="T78">inəbə</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_315" n="HIAT:w" s="T79">teppi</ts>
                  <nts id="Seg_316" n="HIAT:ip">.</nts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_319" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_321" n="HIAT:w" s="T80">Dĭ</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_324" n="HIAT:w" s="T81">dĭm</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_327" n="HIAT:w" s="T82">tʼuga</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_330" n="HIAT:w" s="T83">tʼippi</ts>
                  <nts id="Seg_331" n="HIAT:ip">.</nts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T84" id="Seg_333" n="sc" s="T0">
               <ts e="T1" id="Seg_335" n="e" s="T0">Uražə </ts>
               <ts e="T2" id="Seg_337" n="e" s="T1">amna. </ts>
               <ts e="T3" id="Seg_339" n="e" s="T2">Uražən </ts>
               <ts e="T4" id="Seg_341" n="e" s="T3">šide </ts>
               <ts e="T5" id="Seg_343" n="e" s="T4">nüket </ts>
               <ts e="T6" id="Seg_345" n="e" s="T5">ibi. </ts>
               <ts e="T7" id="Seg_347" n="e" s="T6">Oʔb </ts>
               <ts e="T8" id="Seg_349" n="e" s="T7">nüket </ts>
               <ts e="T9" id="Seg_351" n="e" s="T8">baška </ts>
               <ts e="T10" id="Seg_353" n="e" s="T9">tibizʼəʔ </ts>
               <ts e="T11" id="Seg_355" n="e" s="T10">amnobi. </ts>
               <ts e="T12" id="Seg_357" n="e" s="T11">Dĭ </ts>
               <ts e="T13" id="Seg_359" n="e" s="T12">kuza </ts>
               <ts e="T14" id="Seg_361" n="e" s="T13">kăštəliat: </ts>
               <ts e="T15" id="Seg_363" n="e" s="T14">"Kanžəbəj!" </ts>
               <ts e="T16" id="Seg_365" n="e" s="T15">Nunanə </ts>
               <ts e="T17" id="Seg_367" n="e" s="T16">külin </ts>
               <ts e="T18" id="Seg_369" n="e" s="T17">moːt </ts>
               <ts e="T19" id="Seg_371" n="e" s="T18">nʼekezeŋdə </ts>
               <ts e="T20" id="Seg_373" n="e" s="T19">kusʼtə </ts>
               <ts e="T21" id="Seg_375" n="e" s="T20">kambiiʔ. </ts>
               <ts e="T22" id="Seg_377" n="e" s="T21">"Sʼaʔ!", </ts>
               <ts e="T23" id="Seg_379" n="e" s="T22">măndə </ts>
               <ts e="T24" id="Seg_381" n="e" s="T23">Uražəndə. </ts>
               <ts e="T25" id="Seg_383" n="e" s="T24">Uražə </ts>
               <ts e="T26" id="Seg_385" n="e" s="T25">sʼabi, </ts>
               <ts e="T27" id="Seg_387" n="e" s="T26">üštöbi. </ts>
               <ts e="T28" id="Seg_389" n="e" s="T27">Teʔmezʼəʔ </ts>
               <ts e="T29" id="Seg_391" n="e" s="T28">üštöbi. </ts>
               <ts e="T30" id="Seg_393" n="e" s="T29">Teʔmebə </ts>
               <ts e="T31" id="Seg_395" n="e" s="T30">saj </ts>
               <ts e="T32" id="Seg_397" n="e" s="T31">băppi. </ts>
               <ts e="T33" id="Seg_399" n="e" s="T32">Uražə </ts>
               <ts e="T34" id="Seg_401" n="e" s="T33">ibəleʔ </ts>
               <ts e="T35" id="Seg_403" n="e" s="T34">kojobi. </ts>
               <ts e="T36" id="Seg_405" n="e" s="T35">Kambi, </ts>
               <ts e="T37" id="Seg_407" n="e" s="T36">uražən </ts>
               <ts e="T38" id="Seg_409" n="e" s="T37">šide </ts>
               <ts e="T39" id="Seg_411" n="e" s="T38">nükebə </ts>
               <ts e="T40" id="Seg_413" n="e" s="T39">ileʔ </ts>
               <ts e="T41" id="Seg_415" n="e" s="T40">kumbi. </ts>
               <ts e="T42" id="Seg_417" n="e" s="T41">Dĭgəttə </ts>
               <ts e="T43" id="Seg_419" n="e" s="T42">sʼüʔleʔ </ts>
               <ts e="T44" id="Seg_421" n="e" s="T43">kandəgaiʔ. </ts>
               <ts e="T45" id="Seg_423" n="e" s="T44">Uražən </ts>
               <ts e="T46" id="Seg_425" n="e" s="T45">tʼaktə </ts>
               <ts e="T47" id="Seg_427" n="e" s="T46">nüket </ts>
               <ts e="T48" id="Seg_429" n="e" s="T47">tʼorlaʔ </ts>
               <ts e="T49" id="Seg_431" n="e" s="T48">kandəga. </ts>
               <ts e="T50" id="Seg_433" n="e" s="T49">Uražə </ts>
               <ts e="T51" id="Seg_435" n="e" s="T50">măndolamnə. </ts>
               <ts e="T52" id="Seg_437" n="e" s="T51">Külin </ts>
               <ts e="T53" id="Seg_439" n="e" s="T52">nʼekezeŋdətsʼəʔ </ts>
               <ts e="T54" id="Seg_441" n="e" s="T53">amoria. </ts>
               <ts e="T55" id="Seg_443" n="e" s="T54">Külin </ts>
               <ts e="T56" id="Seg_445" n="e" s="T55">nʼekezeŋdə </ts>
               <ts e="T57" id="Seg_447" n="e" s="T56">dĭnə </ts>
               <ts e="T58" id="Seg_449" n="e" s="T57">tüšəlaːmbi. </ts>
               <ts e="T59" id="Seg_451" n="e" s="T58">Kaləŋgəndə </ts>
               <ts e="T60" id="Seg_453" n="e" s="T59">šide </ts>
               <ts e="T61" id="Seg_455" n="e" s="T60">nʼekebə </ts>
               <ts e="T62" id="Seg_457" n="e" s="T61">sarbi, </ts>
               <ts e="T63" id="Seg_459" n="e" s="T62">tʼünə </ts>
               <ts e="T64" id="Seg_461" n="e" s="T63">nʼergöleʔ </ts>
               <ts e="T65" id="Seg_463" n="e" s="T64">üzəbi, </ts>
               <ts e="T66" id="Seg_465" n="e" s="T65">kambi. </ts>
               <ts e="T67" id="Seg_467" n="e" s="T66">Büjzengən </ts>
               <ts e="T68" id="Seg_469" n="e" s="T67">bügən </ts>
               <ts e="T69" id="Seg_471" n="e" s="T68">amnə. </ts>
               <ts e="T70" id="Seg_473" n="e" s="T69">Tʼaktə </ts>
               <ts e="T71" id="Seg_475" n="e" s="T70">nüket </ts>
               <ts e="T72" id="Seg_477" n="e" s="T71">büjleʔ </ts>
               <ts e="T73" id="Seg_479" n="e" s="T72">šobi. </ts>
               <ts e="T74" id="Seg_481" n="e" s="T73">Nörbəlia: </ts>
               <ts e="T75" id="Seg_483" n="e" s="T74">"Inəm </ts>
               <ts e="T76" id="Seg_485" n="e" s="T75">möːm </ts>
               <ts e="T77" id="Seg_487" n="e" s="T76">tettə!" </ts>
               <ts e="T78" id="Seg_489" n="e" s="T77">Dĭ </ts>
               <ts e="T79" id="Seg_491" n="e" s="T78">inəbə </ts>
               <ts e="T80" id="Seg_493" n="e" s="T79">teppi. </ts>
               <ts e="T81" id="Seg_495" n="e" s="T80">Dĭ </ts>
               <ts e="T82" id="Seg_497" n="e" s="T81">dĭm </ts>
               <ts e="T83" id="Seg_499" n="e" s="T82">tʼuga </ts>
               <ts e="T84" id="Seg_501" n="e" s="T83">tʼippi. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T2" id="Seg_502" s="T0">AA_1914_Raven_flk.001 (001.001)</ta>
            <ta e="T6" id="Seg_503" s="T2">AA_1914_Raven_flk.002 (001.002)</ta>
            <ta e="T11" id="Seg_504" s="T6">AA_1914_Raven_flk.003 (001.003)</ta>
            <ta e="T15" id="Seg_505" s="T11">AA_1914_Raven_flk.004 (001.004)</ta>
            <ta e="T21" id="Seg_506" s="T15">AA_1914_Raven_flk.005 (001.006)</ta>
            <ta e="T22" id="Seg_507" s="T21">AA_1914_Raven_flk.006 (001.007)</ta>
            <ta e="T24" id="Seg_508" s="T22">AA_1914_Raven_flk.007 (001.008)</ta>
            <ta e="T27" id="Seg_509" s="T24">AA_1914_Raven_flk.008 (001.009)</ta>
            <ta e="T29" id="Seg_510" s="T27">AA_1914_Raven_flk.009 (001.010)</ta>
            <ta e="T32" id="Seg_511" s="T29">AA_1914_Raven_flk.010 (001.011)</ta>
            <ta e="T35" id="Seg_512" s="T32">AA_1914_Raven_flk.011 (001.012)</ta>
            <ta e="T41" id="Seg_513" s="T35">AA_1914_Raven_flk.012 (001.013)</ta>
            <ta e="T44" id="Seg_514" s="T41">AA_1914_Raven_flk.013 (001.014)</ta>
            <ta e="T49" id="Seg_515" s="T44">AA_1914_Raven_flk.014 (001.015)</ta>
            <ta e="T51" id="Seg_516" s="T49">AA_1914_Raven_flk.015 (001.016)</ta>
            <ta e="T54" id="Seg_517" s="T51">AA_1914_Raven_flk.016 (001.017)</ta>
            <ta e="T58" id="Seg_518" s="T54">AA_1914_Raven_flk.017 (001.018)</ta>
            <ta e="T66" id="Seg_519" s="T58">AA_1914_Raven_flk.018 (001.019)</ta>
            <ta e="T69" id="Seg_520" s="T66">AA_1914_Raven_flk.019 (001.020)</ta>
            <ta e="T73" id="Seg_521" s="T69">AA_1914_Raven_flk.020 (001.021)</ta>
            <ta e="T77" id="Seg_522" s="T73">AA_1914_Raven_flk.021 (001.022)</ta>
            <ta e="T80" id="Seg_523" s="T77">AA_1914_Raven_flk.022 (001.024)</ta>
            <ta e="T84" id="Seg_524" s="T80">AA_1914_Raven_flk.023 (001.025)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T2" id="Seg_525" s="T0">Uražə amna. </ta>
            <ta e="T6" id="Seg_526" s="T2">Uražən šide nüket ibi. </ta>
            <ta e="T11" id="Seg_527" s="T6">Oʔb nüket baška tibizʼəʔ amnobi. </ta>
            <ta e="T15" id="Seg_528" s="T11">Dĭ kuza kăštəliat: "Kanžəbəj!" </ta>
            <ta e="T21" id="Seg_529" s="T15">Nunanə külin moːt nʼekezeŋdə kusʼtə kambiiʔ. </ta>
            <ta e="T22" id="Seg_530" s="T21">"Sʼaʔ!", </ta>
            <ta e="T24" id="Seg_531" s="T22">măndə Uražəndə. </ta>
            <ta e="T27" id="Seg_532" s="T24">Uražə sʼabi, üštöbi. </ta>
            <ta e="T29" id="Seg_533" s="T27">Teʔmezʼəʔ üštöbi. </ta>
            <ta e="T32" id="Seg_534" s="T29">Teʔmebə saj băppi.</ta>
            <ta e="T35" id="Seg_535" s="T32"> Uražə ibəleʔ kojobi. </ta>
            <ta e="T41" id="Seg_536" s="T35">Kambi, uražən šide nükebə ileʔ kumbi. </ta>
            <ta e="T44" id="Seg_537" s="T41">Dĭgəttə sʼüʔleʔ kandəgaiʔ. </ta>
            <ta e="T49" id="Seg_538" s="T44">Uražən tʼaktə nüket tʼorlaʔ kandəga. </ta>
            <ta e="T51" id="Seg_539" s="T49">Uražə măndolamnə. </ta>
            <ta e="T54" id="Seg_540" s="T51">Külin nʼekezeŋdətsʼəʔ amoria. </ta>
            <ta e="T58" id="Seg_541" s="T54">Külin nʼekezeŋdə dĭnə tüšəlaːmbi. </ta>
            <ta e="T66" id="Seg_542" s="T58">Kaləŋgəndə šide nʼekebə sarbi, tʼünə nʼergöleʔ üzəbi, kambi. </ta>
            <ta e="T69" id="Seg_543" s="T66">Büjzengən bügən amnə. </ta>
            <ta e="T73" id="Seg_544" s="T69">Tʼaktə nüket büjleʔ šobi. </ta>
            <ta e="T77" id="Seg_545" s="T73">Nörbəlia: "Inəm möːm tettə!" </ta>
            <ta e="T80" id="Seg_546" s="T77">Dĭ inəbə teppi. </ta>
            <ta e="T84" id="Seg_547" s="T80">Dĭ dĭm tʼuga tʼippi. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_548" s="T0">uražə</ta>
            <ta e="T2" id="Seg_549" s="T1">amna</ta>
            <ta e="T3" id="Seg_550" s="T2">uražə-n</ta>
            <ta e="T4" id="Seg_551" s="T3">šide</ta>
            <ta e="T5" id="Seg_552" s="T4">nüke-t</ta>
            <ta e="T6" id="Seg_553" s="T5">i-bi</ta>
            <ta e="T7" id="Seg_554" s="T6">oʔb</ta>
            <ta e="T8" id="Seg_555" s="T7">nüke-t</ta>
            <ta e="T9" id="Seg_556" s="T8">baška</ta>
            <ta e="T10" id="Seg_557" s="T9">tibi-zʼəʔ</ta>
            <ta e="T11" id="Seg_558" s="T10">amno-bi</ta>
            <ta e="T12" id="Seg_559" s="T11">dĭ</ta>
            <ta e="T13" id="Seg_560" s="T12">kuza</ta>
            <ta e="T14" id="Seg_561" s="T13">kăštə-lia-t</ta>
            <ta e="T15" id="Seg_562" s="T14">kan-žə-bəj</ta>
            <ta e="T16" id="Seg_563" s="T15">nuna-nə</ta>
            <ta e="T17" id="Seg_564" s="T16">küli-n</ta>
            <ta e="T18" id="Seg_565" s="T17">moː-t</ta>
            <ta e="T19" id="Seg_566" s="T18">nʼeke-zeŋ-də</ta>
            <ta e="T20" id="Seg_567" s="T19">ku-sʼtə</ta>
            <ta e="T21" id="Seg_568" s="T20">kam-bi-iʔ</ta>
            <ta e="T22" id="Seg_569" s="T21">sʼa-ʔ</ta>
            <ta e="T23" id="Seg_570" s="T22">măn-də</ta>
            <ta e="T24" id="Seg_571" s="T23">uražə-ndə</ta>
            <ta e="T25" id="Seg_572" s="T24">uražə</ta>
            <ta e="T26" id="Seg_573" s="T25">sʼa-bi</ta>
            <ta e="T27" id="Seg_574" s="T26">üšt-ö-bi</ta>
            <ta e="T28" id="Seg_575" s="T27">teʔme-zʼəʔ</ta>
            <ta e="T29" id="Seg_576" s="T28">üšt-ö-bi</ta>
            <ta e="T30" id="Seg_577" s="T29">teʔme-bə</ta>
            <ta e="T31" id="Seg_578" s="T30">saj</ta>
            <ta e="T32" id="Seg_579" s="T31">băp-pi</ta>
            <ta e="T33" id="Seg_580" s="T32">uražə</ta>
            <ta e="T34" id="Seg_581" s="T33">ibə-leʔ</ta>
            <ta e="T35" id="Seg_582" s="T34">kojo-bi</ta>
            <ta e="T36" id="Seg_583" s="T35">kam-bi</ta>
            <ta e="T37" id="Seg_584" s="T36">uražə-n</ta>
            <ta e="T38" id="Seg_585" s="T37">šide</ta>
            <ta e="T39" id="Seg_586" s="T38">nüke-bə</ta>
            <ta e="T40" id="Seg_587" s="T39">i-leʔ</ta>
            <ta e="T41" id="Seg_588" s="T40">kum-bi</ta>
            <ta e="T42" id="Seg_589" s="T41">dĭgəttə</ta>
            <ta e="T43" id="Seg_590" s="T42">sʼüʔ-leʔ</ta>
            <ta e="T44" id="Seg_591" s="T43">kandə-ga-iʔ</ta>
            <ta e="T45" id="Seg_592" s="T44">uražə-n</ta>
            <ta e="T46" id="Seg_593" s="T45">tʼaktə</ta>
            <ta e="T47" id="Seg_594" s="T46">nüke-t</ta>
            <ta e="T48" id="Seg_595" s="T47">tʼor-laʔ</ta>
            <ta e="T49" id="Seg_596" s="T48">kandə-ga</ta>
            <ta e="T50" id="Seg_597" s="T49">uražə</ta>
            <ta e="T51" id="Seg_598" s="T50">măndo-lamnə</ta>
            <ta e="T52" id="Seg_599" s="T51">küli-n</ta>
            <ta e="T53" id="Seg_600" s="T52">nʼeke-zeŋ-də-t-sʼəʔ</ta>
            <ta e="T54" id="Seg_601" s="T53">amor-ia</ta>
            <ta e="T55" id="Seg_602" s="T54">küli-n</ta>
            <ta e="T56" id="Seg_603" s="T55">nʼeke-zeŋ-də</ta>
            <ta e="T57" id="Seg_604" s="T56">dĭ-nə</ta>
            <ta e="T58" id="Seg_605" s="T57">tüšə-laːm-bi</ta>
            <ta e="T59" id="Seg_606" s="T58">kaləŋ-gəndə</ta>
            <ta e="T60" id="Seg_607" s="T59">šide</ta>
            <ta e="T61" id="Seg_608" s="T60">nʼeke-bə</ta>
            <ta e="T62" id="Seg_609" s="T61">sar-bi</ta>
            <ta e="T63" id="Seg_610" s="T62">tʼü-nə</ta>
            <ta e="T64" id="Seg_611" s="T63">nʼergö-leʔ</ta>
            <ta e="T65" id="Seg_612" s="T64">üzə-bi</ta>
            <ta e="T66" id="Seg_613" s="T65">kam-bi</ta>
            <ta e="T67" id="Seg_614" s="T66">bü-j-zen-gən</ta>
            <ta e="T68" id="Seg_615" s="T67">bü-gən</ta>
            <ta e="T69" id="Seg_616" s="T68">amnə</ta>
            <ta e="T70" id="Seg_617" s="T69">tʼaktə</ta>
            <ta e="T71" id="Seg_618" s="T70">nüke-t</ta>
            <ta e="T72" id="Seg_619" s="T71">bü-j-leʔ</ta>
            <ta e="T73" id="Seg_620" s="T72">šo-bi</ta>
            <ta e="T74" id="Seg_621" s="T73">nörbə-lia</ta>
            <ta e="T75" id="Seg_622" s="T74">inə-m</ta>
            <ta e="T76" id="Seg_623" s="T75">möː-m</ta>
            <ta e="T77" id="Seg_624" s="T76">tet-tə</ta>
            <ta e="T78" id="Seg_625" s="T77">dĭ</ta>
            <ta e="T79" id="Seg_626" s="T78">inə-bə</ta>
            <ta e="T80" id="Seg_627" s="T79">tep-pi</ta>
            <ta e="T81" id="Seg_628" s="T80">dĭ</ta>
            <ta e="T82" id="Seg_629" s="T81">dĭ-m</ta>
            <ta e="T83" id="Seg_630" s="T82">tʼuga</ta>
            <ta e="T84" id="Seg_631" s="T83">tʼip-pi</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_632" s="T0">uražə</ta>
            <ta e="T2" id="Seg_633" s="T1">amnə</ta>
            <ta e="T3" id="Seg_634" s="T2">uražə-n</ta>
            <ta e="T4" id="Seg_635" s="T3">šide</ta>
            <ta e="T5" id="Seg_636" s="T4">nüke-t</ta>
            <ta e="T6" id="Seg_637" s="T5">i-bi</ta>
            <ta e="T7" id="Seg_638" s="T6">oʔb</ta>
            <ta e="T8" id="Seg_639" s="T7">nüke-t</ta>
            <ta e="T9" id="Seg_640" s="T8">baška</ta>
            <ta e="T10" id="Seg_641" s="T9">tibi-ziʔ</ta>
            <ta e="T11" id="Seg_642" s="T10">amno-bi</ta>
            <ta e="T12" id="Seg_643" s="T11">dĭ</ta>
            <ta e="T13" id="Seg_644" s="T12">kuza</ta>
            <ta e="T14" id="Seg_645" s="T13">kăštə-liA-t</ta>
            <ta e="T15" id="Seg_646" s="T14">kan-žə-bəj</ta>
            <ta e="T16" id="Seg_647" s="T15">nuna-Tə</ta>
            <ta e="T17" id="Seg_648" s="T16">küli-n</ta>
            <ta e="T18" id="Seg_649" s="T17">moː-t</ta>
            <ta e="T19" id="Seg_650" s="T18">nʼekə-zAŋ-də</ta>
            <ta e="T20" id="Seg_651" s="T19">ku-zittə</ta>
            <ta e="T21" id="Seg_652" s="T20">kan-bi-jəʔ</ta>
            <ta e="T22" id="Seg_653" s="T21">sʼa-ʔ</ta>
            <ta e="T23" id="Seg_654" s="T22">măn-ntə</ta>
            <ta e="T24" id="Seg_655" s="T23">uražə-gəndə</ta>
            <ta e="T25" id="Seg_656" s="T24">uražə</ta>
            <ta e="T26" id="Seg_657" s="T25">sʼa-bi</ta>
            <ta e="T27" id="Seg_658" s="T26">üštə-o-bi</ta>
            <ta e="T28" id="Seg_659" s="T27">teʔme-ziʔ</ta>
            <ta e="T29" id="Seg_660" s="T28">üštə-o-bi</ta>
            <ta e="T30" id="Seg_661" s="T29">teʔme-bə</ta>
            <ta e="T31" id="Seg_662" s="T30">saj</ta>
            <ta e="T32" id="Seg_663" s="T31">băt-bi</ta>
            <ta e="T33" id="Seg_664" s="T32">uražə</ta>
            <ta e="T34" id="Seg_665" s="T33">iʔbə-lAʔ</ta>
            <ta e="T35" id="Seg_666" s="T34">kojo-bi</ta>
            <ta e="T36" id="Seg_667" s="T35">kan-bi</ta>
            <ta e="T37" id="Seg_668" s="T36">uražə-n</ta>
            <ta e="T38" id="Seg_669" s="T37">šide</ta>
            <ta e="T39" id="Seg_670" s="T38">nüke-bə</ta>
            <ta e="T40" id="Seg_671" s="T39">i-lAʔ</ta>
            <ta e="T41" id="Seg_672" s="T40">kun-bi</ta>
            <ta e="T42" id="Seg_673" s="T41">dĭgəttə</ta>
            <ta e="T43" id="Seg_674" s="T42">sʼüʔ-lAʔ</ta>
            <ta e="T44" id="Seg_675" s="T43">kandə-gA-jəʔ</ta>
            <ta e="T45" id="Seg_676" s="T44">uražə-n</ta>
            <ta e="T46" id="Seg_677" s="T45">tʼaktə</ta>
            <ta e="T47" id="Seg_678" s="T46">nüke-t</ta>
            <ta e="T48" id="Seg_679" s="T47">tʼor-lAʔ</ta>
            <ta e="T49" id="Seg_680" s="T48">kandə-gA</ta>
            <ta e="T50" id="Seg_681" s="T49">uražə</ta>
            <ta e="T51" id="Seg_682" s="T50">măndo-lamnə</ta>
            <ta e="T52" id="Seg_683" s="T51">küli-n</ta>
            <ta e="T53" id="Seg_684" s="T52">nʼekə-zAŋ-də-t-ziʔ</ta>
            <ta e="T54" id="Seg_685" s="T53">amor-liA</ta>
            <ta e="T55" id="Seg_686" s="T54">küli-n</ta>
            <ta e="T56" id="Seg_687" s="T55">nʼekə-zAŋ-də</ta>
            <ta e="T57" id="Seg_688" s="T56">dĭ-Tə</ta>
            <ta e="T58" id="Seg_689" s="T57">tüšə-laːm-bi</ta>
            <ta e="T59" id="Seg_690" s="T58">kaləŋ-gəndə</ta>
            <ta e="T60" id="Seg_691" s="T59">šide</ta>
            <ta e="T61" id="Seg_692" s="T60">nʼekə-bə</ta>
            <ta e="T62" id="Seg_693" s="T61">sar-bi</ta>
            <ta e="T63" id="Seg_694" s="T62">tʼü-Tə</ta>
            <ta e="T64" id="Seg_695" s="T63">nʼergö-lAʔ</ta>
            <ta e="T65" id="Seg_696" s="T64">üzə-bi</ta>
            <ta e="T66" id="Seg_697" s="T65">kan-bi</ta>
            <ta e="T67" id="Seg_698" s="T66">bü-j-zen-Kən</ta>
            <ta e="T68" id="Seg_699" s="T67">bü-Kən</ta>
            <ta e="T69" id="Seg_700" s="T68">amnə</ta>
            <ta e="T70" id="Seg_701" s="T69">tʼaktə</ta>
            <ta e="T71" id="Seg_702" s="T70">nüke-t</ta>
            <ta e="T72" id="Seg_703" s="T71">bü-j-lAʔ</ta>
            <ta e="T73" id="Seg_704" s="T72">šo-bi</ta>
            <ta e="T74" id="Seg_705" s="T73">nörbə-liA</ta>
            <ta e="T75" id="Seg_706" s="T74">inə-m</ta>
            <ta e="T76" id="Seg_707" s="T75">möː-m</ta>
            <ta e="T77" id="Seg_708" s="T76">det-t</ta>
            <ta e="T78" id="Seg_709" s="T77">dĭ</ta>
            <ta e="T79" id="Seg_710" s="T78">inə-bə</ta>
            <ta e="T80" id="Seg_711" s="T79">det-bi</ta>
            <ta e="T81" id="Seg_712" s="T80">dĭ</ta>
            <ta e="T82" id="Seg_713" s="T81">dĭ-m</ta>
            <ta e="T83" id="Seg_714" s="T82">tʼuga</ta>
            <ta e="T84" id="Seg_715" s="T83">tʼit-bi</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_716" s="T0">Urazhe.[NOM.SG]</ta>
            <ta e="T2" id="Seg_717" s="T1">live.[3SG]</ta>
            <ta e="T3" id="Seg_718" s="T2">Urazhe-GEN</ta>
            <ta e="T4" id="Seg_719" s="T3">two.[NOM.SG]</ta>
            <ta e="T5" id="Seg_720" s="T4">woman-NOM/GEN.3SG</ta>
            <ta e="T6" id="Seg_721" s="T5">be-PST.[3SG]</ta>
            <ta e="T7" id="Seg_722" s="T6">one.[NOM.SG]</ta>
            <ta e="T8" id="Seg_723" s="T7">woman-NOM/GEN.3SG</ta>
            <ta e="T9" id="Seg_724" s="T8">another.[NOM.SG]</ta>
            <ta e="T10" id="Seg_725" s="T9">man-INS</ta>
            <ta e="T11" id="Seg_726" s="T10">live-PST.[3SG]</ta>
            <ta e="T12" id="Seg_727" s="T11">this.[NOM.SG]</ta>
            <ta e="T13" id="Seg_728" s="T12">man.[NOM.SG]</ta>
            <ta e="T14" id="Seg_729" s="T13">invite-PRS-3SG.O</ta>
            <ta e="T15" id="Seg_730" s="T14">go-OPT.DU/PL-1DU</ta>
            <ta e="T16" id="Seg_731" s="T15">cliff-LAT</ta>
            <ta e="T17" id="Seg_732" s="T16">raven-GEN</ta>
            <ta e="T18" id="Seg_733" s="T17">nest-NOM/GEN.3SG</ta>
            <ta e="T19" id="Seg_734" s="T18">youngling-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T20" id="Seg_735" s="T19">see-INF.LAT</ta>
            <ta e="T21" id="Seg_736" s="T20">go-PST-3PL</ta>
            <ta e="T22" id="Seg_737" s="T21">climb-IMP.2SG</ta>
            <ta e="T23" id="Seg_738" s="T22">say-IPFVZ.[3SG]</ta>
            <ta e="T24" id="Seg_739" s="T23">Urazhe-LAT/LOC.3SG</ta>
            <ta e="T25" id="Seg_740" s="T24">Urazhe.[NOM.SG]</ta>
            <ta e="T26" id="Seg_741" s="T25">climb-PST.[3SG]</ta>
            <ta e="T27" id="Seg_742" s="T26">bring.down-RFL-PST.[3SG]</ta>
            <ta e="T28" id="Seg_743" s="T27">rope-INS</ta>
            <ta e="T29" id="Seg_744" s="T28">bring.down-RFL-PST.[3SG]</ta>
            <ta e="T30" id="Seg_745" s="T29">rope-ACC.3SG</ta>
            <ta e="T31" id="Seg_746" s="T30">off</ta>
            <ta e="T32" id="Seg_747" s="T31">cut-PST.[3SG]</ta>
            <ta e="T33" id="Seg_748" s="T32">Urazhe.[NOM.SG]</ta>
            <ta e="T34" id="Seg_749" s="T33">lie.down-CVB</ta>
            <ta e="T35" id="Seg_750" s="T34">stay-PST.[3SG]</ta>
            <ta e="T36" id="Seg_751" s="T35">go-PST.[3SG]</ta>
            <ta e="T37" id="Seg_752" s="T36">Urazhe-GEN</ta>
            <ta e="T38" id="Seg_753" s="T37">two.[NOM.SG]</ta>
            <ta e="T39" id="Seg_754" s="T38">woman-ACC.3SG</ta>
            <ta e="T40" id="Seg_755" s="T39">take-CVB</ta>
            <ta e="T41" id="Seg_756" s="T40">bring-PST.[3SG]</ta>
            <ta e="T42" id="Seg_757" s="T41">then</ta>
            <ta e="T43" id="Seg_758" s="T42">move-CVB</ta>
            <ta e="T44" id="Seg_759" s="T43">walk-PRS-3PL</ta>
            <ta e="T45" id="Seg_760" s="T44">Urazhe-GEN</ta>
            <ta e="T46" id="Seg_761" s="T45">old.[NOM.SG]</ta>
            <ta e="T47" id="Seg_762" s="T46">woman-NOM/GEN.3SG</ta>
            <ta e="T48" id="Seg_763" s="T47">cry-CVB</ta>
            <ta e="T49" id="Seg_764" s="T48">walk-PRS.[3SG]</ta>
            <ta e="T50" id="Seg_765" s="T49">Urazhe.[NOM.SG]</ta>
            <ta e="T51" id="Seg_766" s="T50">look-DUR.[3SG]</ta>
            <ta e="T52" id="Seg_767" s="T51">raven-GEN</ta>
            <ta e="T53" id="Seg_768" s="T52">youngling-PL-NOM/GEN/ACC.3SG-3SG-INS</ta>
            <ta e="T54" id="Seg_769" s="T53">eat-PRS.[3SG]</ta>
            <ta e="T55" id="Seg_770" s="T54">raven-GEN</ta>
            <ta e="T56" id="Seg_771" s="T55">youngling-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T57" id="Seg_772" s="T56">this-LAT</ta>
            <ta e="T58" id="Seg_773" s="T57">learn-RES-PST.[3SG]</ta>
            <ta e="T59" id="Seg_774" s="T58">armpit-LAT/LOC.3SG</ta>
            <ta e="T60" id="Seg_775" s="T59">two.[NOM.SG]</ta>
            <ta e="T61" id="Seg_776" s="T60">youngling-ACC.3SG</ta>
            <ta e="T62" id="Seg_777" s="T61">bind-PST.[3SG]</ta>
            <ta e="T63" id="Seg_778" s="T62">land-LAT</ta>
            <ta e="T64" id="Seg_779" s="T63">fly-CVB</ta>
            <ta e="T65" id="Seg_780" s="T64">descend-PST.[3SG]</ta>
            <ta e="T66" id="Seg_781" s="T65">go-PST.[3SG]</ta>
            <ta e="T67" id="Seg_782" s="T66">water-VBLZ-INS.N-LOC</ta>
            <ta e="T68" id="Seg_783" s="T67">water-LOC</ta>
            <ta e="T69" id="Seg_784" s="T68">sit.down.[3SG]</ta>
            <ta e="T70" id="Seg_785" s="T69">old.[NOM.SG]</ta>
            <ta e="T71" id="Seg_786" s="T70">woman-NOM/GEN.3SG</ta>
            <ta e="T72" id="Seg_787" s="T71">water-VBLZ-CVB</ta>
            <ta e="T73" id="Seg_788" s="T72">come-PST.[3SG]</ta>
            <ta e="T74" id="Seg_789" s="T73">tell-PRS.[3SG]</ta>
            <ta e="T75" id="Seg_790" s="T74">bow-NOM/GEN/ACC.1SG</ta>
            <ta e="T76" id="Seg_791" s="T75">arrow-NOM/GEN/ACC.1SG</ta>
            <ta e="T77" id="Seg_792" s="T76">bring-IMP.2SG.O</ta>
            <ta e="T78" id="Seg_793" s="T77">this.[NOM.SG]</ta>
            <ta e="T79" id="Seg_794" s="T78">bow-ACC.3SG</ta>
            <ta e="T80" id="Seg_795" s="T79">bring-PST.[3SG]</ta>
            <ta e="T81" id="Seg_796" s="T80">this.[NOM.SG]</ta>
            <ta e="T82" id="Seg_797" s="T81">this-ACC</ta>
            <ta e="T83" id="Seg_798" s="T82">dead.[NOM.SG]</ta>
            <ta e="T84" id="Seg_799" s="T83">shoot-PST.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_800" s="T0">Ураже.[NOM.SG]</ta>
            <ta e="T2" id="Seg_801" s="T1">жить.[3SG]</ta>
            <ta e="T3" id="Seg_802" s="T2">Ураже-GEN</ta>
            <ta e="T4" id="Seg_803" s="T3">два.[NOM.SG]</ta>
            <ta e="T5" id="Seg_804" s="T4">женщина-NOM/GEN.3SG</ta>
            <ta e="T6" id="Seg_805" s="T5">быть-PST.[3SG]</ta>
            <ta e="T7" id="Seg_806" s="T6">один.[NOM.SG]</ta>
            <ta e="T8" id="Seg_807" s="T7">женщина-NOM/GEN.3SG</ta>
            <ta e="T9" id="Seg_808" s="T8">другой.[NOM.SG]</ta>
            <ta e="T10" id="Seg_809" s="T9">мужчина-INS</ta>
            <ta e="T11" id="Seg_810" s="T10">жить-PST.[3SG]</ta>
            <ta e="T12" id="Seg_811" s="T11">этот.[NOM.SG]</ta>
            <ta e="T13" id="Seg_812" s="T12">мужчина.[NOM.SG]</ta>
            <ta e="T14" id="Seg_813" s="T13">пригласить-PRS-3SG.O</ta>
            <ta e="T15" id="Seg_814" s="T14">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T16" id="Seg_815" s="T15">утес-LAT</ta>
            <ta e="T17" id="Seg_816" s="T16">ворон-GEN</ta>
            <ta e="T18" id="Seg_817" s="T17">гнездо-NOM/GEN.3SG</ta>
            <ta e="T19" id="Seg_818" s="T18">юноша-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T20" id="Seg_819" s="T19">видеть-INF.LAT</ta>
            <ta e="T21" id="Seg_820" s="T20">пойти-PST-3PL</ta>
            <ta e="T22" id="Seg_821" s="T21">влезать-IMP.2SG</ta>
            <ta e="T23" id="Seg_822" s="T22">сказать-IPFVZ.[3SG]</ta>
            <ta e="T24" id="Seg_823" s="T23">Ураже-LAT/LOC.3SG</ta>
            <ta e="T25" id="Seg_824" s="T24">Ураже.[NOM.SG]</ta>
            <ta e="T26" id="Seg_825" s="T25">влезать-PST.[3SG]</ta>
            <ta e="T27" id="Seg_826" s="T26">спустить-RFL-PST.[3SG]</ta>
            <ta e="T28" id="Seg_827" s="T27">веревка-INS</ta>
            <ta e="T29" id="Seg_828" s="T28">спустить-RFL-PST.[3SG]</ta>
            <ta e="T30" id="Seg_829" s="T29">веревка-ACC.3SG</ta>
            <ta e="T31" id="Seg_830" s="T30">от</ta>
            <ta e="T32" id="Seg_831" s="T31">резать-PST.[3SG]</ta>
            <ta e="T33" id="Seg_832" s="T32">Ураже.[NOM.SG]</ta>
            <ta e="T34" id="Seg_833" s="T33">ложиться-CVB</ta>
            <ta e="T35" id="Seg_834" s="T34">остаться-PST.[3SG]</ta>
            <ta e="T36" id="Seg_835" s="T35">пойти-PST.[3SG]</ta>
            <ta e="T37" id="Seg_836" s="T36">Ураже-GEN</ta>
            <ta e="T38" id="Seg_837" s="T37">два.[NOM.SG]</ta>
            <ta e="T39" id="Seg_838" s="T38">женщина-ACC.3SG</ta>
            <ta e="T40" id="Seg_839" s="T39">взять-CVB</ta>
            <ta e="T41" id="Seg_840" s="T40">нести-PST.[3SG]</ta>
            <ta e="T42" id="Seg_841" s="T41">тогда</ta>
            <ta e="T43" id="Seg_842" s="T42">двигаться-CVB</ta>
            <ta e="T44" id="Seg_843" s="T43">идти-PRS-3PL</ta>
            <ta e="T45" id="Seg_844" s="T44">Ураже-GEN</ta>
            <ta e="T46" id="Seg_845" s="T45">старый.[NOM.SG]</ta>
            <ta e="T47" id="Seg_846" s="T46">женщина-NOM/GEN.3SG</ta>
            <ta e="T48" id="Seg_847" s="T47">плакать-CVB</ta>
            <ta e="T49" id="Seg_848" s="T48">идти-PRS.[3SG]</ta>
            <ta e="T50" id="Seg_849" s="T49">Ураже.[NOM.SG]</ta>
            <ta e="T51" id="Seg_850" s="T50">смотреть-DUR.[3SG]</ta>
            <ta e="T52" id="Seg_851" s="T51">ворон-GEN</ta>
            <ta e="T53" id="Seg_852" s="T52">юноша-PL-NOM/GEN/ACC.3SG-3SG-INS</ta>
            <ta e="T54" id="Seg_853" s="T53">есть-PRS.[3SG]</ta>
            <ta e="T55" id="Seg_854" s="T54">ворон-GEN</ta>
            <ta e="T56" id="Seg_855" s="T55">юноша-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T57" id="Seg_856" s="T56">этот-LAT</ta>
            <ta e="T58" id="Seg_857" s="T57">научиться-RES-PST.[3SG]</ta>
            <ta e="T59" id="Seg_858" s="T58">подмышка-LAT/LOC.3SG</ta>
            <ta e="T60" id="Seg_859" s="T59">два.[NOM.SG]</ta>
            <ta e="T61" id="Seg_860" s="T60">юноша-ACC.3SG</ta>
            <ta e="T62" id="Seg_861" s="T61">завязать-PST.[3SG]</ta>
            <ta e="T63" id="Seg_862" s="T62">земля-LAT</ta>
            <ta e="T64" id="Seg_863" s="T63">лететь-CVB</ta>
            <ta e="T65" id="Seg_864" s="T64">спуститься-PST.[3SG]</ta>
            <ta e="T66" id="Seg_865" s="T65">пойти-PST.[3SG]</ta>
            <ta e="T67" id="Seg_866" s="T66">вода-VBLZ-INS.N-LOC</ta>
            <ta e="T68" id="Seg_867" s="T67">вода-LOC</ta>
            <ta e="T69" id="Seg_868" s="T68">сесть.[3SG]</ta>
            <ta e="T70" id="Seg_869" s="T69">старый.[NOM.SG]</ta>
            <ta e="T71" id="Seg_870" s="T70">женщина-NOM/GEN.3SG</ta>
            <ta e="T72" id="Seg_871" s="T71">вода-VBLZ-CVB</ta>
            <ta e="T73" id="Seg_872" s="T72">прийти-PST.[3SG]</ta>
            <ta e="T74" id="Seg_873" s="T73">сказать-PRS.[3SG]</ta>
            <ta e="T75" id="Seg_874" s="T74">лук-NOM/GEN/ACC.1SG</ta>
            <ta e="T76" id="Seg_875" s="T75">стрела-NOM/GEN/ACC.1SG</ta>
            <ta e="T77" id="Seg_876" s="T76">принести-IMP.2SG.O</ta>
            <ta e="T78" id="Seg_877" s="T77">этот.[NOM.SG]</ta>
            <ta e="T79" id="Seg_878" s="T78">лук-ACC.3SG</ta>
            <ta e="T80" id="Seg_879" s="T79">принести-PST.[3SG]</ta>
            <ta e="T81" id="Seg_880" s="T80">этот.[NOM.SG]</ta>
            <ta e="T82" id="Seg_881" s="T81">этот-ACC</ta>
            <ta e="T83" id="Seg_882" s="T82">мертвый.[NOM.SG]</ta>
            <ta e="T84" id="Seg_883" s="T83">стрелять-PST.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_884" s="T0">propr-n:case</ta>
            <ta e="T2" id="Seg_885" s="T1">v-v:pn</ta>
            <ta e="T3" id="Seg_886" s="T2">propr-n:case</ta>
            <ta e="T4" id="Seg_887" s="T3">num-n:case</ta>
            <ta e="T5" id="Seg_888" s="T4">n-n:case.poss</ta>
            <ta e="T6" id="Seg_889" s="T5">v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_890" s="T6">num-n:case</ta>
            <ta e="T8" id="Seg_891" s="T7">n-n:case.poss</ta>
            <ta e="T9" id="Seg_892" s="T8">adj-n:case</ta>
            <ta e="T10" id="Seg_893" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_894" s="T10">v-v:tense-v:pn</ta>
            <ta e="T12" id="Seg_895" s="T11">dempro-n:case</ta>
            <ta e="T13" id="Seg_896" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_897" s="T13">v-v:tense-v:pn</ta>
            <ta e="T15" id="Seg_898" s="T14">v-v:mood-v:pn</ta>
            <ta e="T16" id="Seg_899" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_900" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_901" s="T17">n-n:case.poss</ta>
            <ta e="T19" id="Seg_902" s="T18">n-n:num-n:case.poss</ta>
            <ta e="T20" id="Seg_903" s="T19">v-v:n.fin</ta>
            <ta e="T21" id="Seg_904" s="T20">v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_905" s="T21">v-v:mood.pn</ta>
            <ta e="T23" id="Seg_906" s="T22">v-v&gt;v-v:pn</ta>
            <ta e="T24" id="Seg_907" s="T23">propr-n:case.poss</ta>
            <ta e="T25" id="Seg_908" s="T24">propr-n:case</ta>
            <ta e="T26" id="Seg_909" s="T25">v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_910" s="T26">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_911" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_912" s="T28">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_913" s="T29">n-n:case.poss</ta>
            <ta e="T31" id="Seg_914" s="T30">adv</ta>
            <ta e="T32" id="Seg_915" s="T31">v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_916" s="T32">propr-n:case</ta>
            <ta e="T34" id="Seg_917" s="T33">v-v:n.fin</ta>
            <ta e="T35" id="Seg_918" s="T34">v-v:tense-v:pn</ta>
            <ta e="T36" id="Seg_919" s="T35">v-v:tense-v:pn</ta>
            <ta e="T37" id="Seg_920" s="T36">propr-n:case</ta>
            <ta e="T38" id="Seg_921" s="T37">num-n:case</ta>
            <ta e="T39" id="Seg_922" s="T38">n-n:case.poss</ta>
            <ta e="T40" id="Seg_923" s="T39">v-v:n.fin</ta>
            <ta e="T41" id="Seg_924" s="T40">v-v:tense-v:pn</ta>
            <ta e="T42" id="Seg_925" s="T41">adv</ta>
            <ta e="T43" id="Seg_926" s="T42">v-v:n.fin</ta>
            <ta e="T44" id="Seg_927" s="T43">v-v:tense-v:pn</ta>
            <ta e="T45" id="Seg_928" s="T44">propr-n:case</ta>
            <ta e="T46" id="Seg_929" s="T45">adj-n:case</ta>
            <ta e="T47" id="Seg_930" s="T46">n-n:case.poss</ta>
            <ta e="T48" id="Seg_931" s="T47">v-v:n.fin</ta>
            <ta e="T49" id="Seg_932" s="T48">v-v:tense-v:pn</ta>
            <ta e="T50" id="Seg_933" s="T49">propr-n:case</ta>
            <ta e="T51" id="Seg_934" s="T50">v-v&gt;v-v:pn</ta>
            <ta e="T52" id="Seg_935" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_936" s="T52">n-n:num-n:case.poss-n:case.poss-n:case</ta>
            <ta e="T54" id="Seg_937" s="T53">v-v:tense-v:pn</ta>
            <ta e="T55" id="Seg_938" s="T54">n-n:case</ta>
            <ta e="T56" id="Seg_939" s="T55">n-n:num-n:case.poss</ta>
            <ta e="T57" id="Seg_940" s="T56">dempro-n:case</ta>
            <ta e="T58" id="Seg_941" s="T57">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T59" id="Seg_942" s="T58">n-n:case.poss</ta>
            <ta e="T60" id="Seg_943" s="T59">num-n:case</ta>
            <ta e="T61" id="Seg_944" s="T60">n-n:case.poss</ta>
            <ta e="T62" id="Seg_945" s="T61">v-v:tense-v:pn</ta>
            <ta e="T63" id="Seg_946" s="T62">n-n:case</ta>
            <ta e="T64" id="Seg_947" s="T63">v-v:n.fin</ta>
            <ta e="T65" id="Seg_948" s="T64">v-v:tense-v:pn</ta>
            <ta e="T66" id="Seg_949" s="T65">v-v:tense-v:pn</ta>
            <ta e="T67" id="Seg_950" s="T66">n-n&gt;v-v&gt;n-n:case</ta>
            <ta e="T68" id="Seg_951" s="T67">n-n:case</ta>
            <ta e="T69" id="Seg_952" s="T68">v-v:pn</ta>
            <ta e="T70" id="Seg_953" s="T69">adj-n:case</ta>
            <ta e="T71" id="Seg_954" s="T70">n-n:case.poss</ta>
            <ta e="T72" id="Seg_955" s="T71">n-n&gt;v-v:n.fin</ta>
            <ta e="T73" id="Seg_956" s="T72">v-v:tense-v:pn</ta>
            <ta e="T74" id="Seg_957" s="T73">v-v:tense-v:pn</ta>
            <ta e="T75" id="Seg_958" s="T74">n-n:case.poss</ta>
            <ta e="T76" id="Seg_959" s="T75">n-n:case.poss</ta>
            <ta e="T77" id="Seg_960" s="T76">v-v:mood.pn</ta>
            <ta e="T78" id="Seg_961" s="T77">dempro-n:case</ta>
            <ta e="T79" id="Seg_962" s="T78">n-n:case.poss</ta>
            <ta e="T80" id="Seg_963" s="T79">v-v:tense-v:pn</ta>
            <ta e="T81" id="Seg_964" s="T80">dempro-n:case</ta>
            <ta e="T82" id="Seg_965" s="T81">dempro-n:case</ta>
            <ta e="T83" id="Seg_966" s="T82">adj-n:case</ta>
            <ta e="T84" id="Seg_967" s="T83">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_968" s="T0">propr</ta>
            <ta e="T2" id="Seg_969" s="T1">v</ta>
            <ta e="T3" id="Seg_970" s="T2">propr</ta>
            <ta e="T4" id="Seg_971" s="T3">num</ta>
            <ta e="T5" id="Seg_972" s="T4">n</ta>
            <ta e="T6" id="Seg_973" s="T5">v</ta>
            <ta e="T7" id="Seg_974" s="T6">num</ta>
            <ta e="T8" id="Seg_975" s="T7">n</ta>
            <ta e="T9" id="Seg_976" s="T8">adj</ta>
            <ta e="T10" id="Seg_977" s="T9">n</ta>
            <ta e="T11" id="Seg_978" s="T10">v</ta>
            <ta e="T12" id="Seg_979" s="T11">dempro</ta>
            <ta e="T13" id="Seg_980" s="T12">n</ta>
            <ta e="T14" id="Seg_981" s="T13">v</ta>
            <ta e="T15" id="Seg_982" s="T14">v</ta>
            <ta e="T16" id="Seg_983" s="T15">n</ta>
            <ta e="T17" id="Seg_984" s="T16">n</ta>
            <ta e="T18" id="Seg_985" s="T17">n</ta>
            <ta e="T19" id="Seg_986" s="T18">n</ta>
            <ta e="T20" id="Seg_987" s="T19">v</ta>
            <ta e="T21" id="Seg_988" s="T20">v</ta>
            <ta e="T22" id="Seg_989" s="T21">v</ta>
            <ta e="T23" id="Seg_990" s="T22">v</ta>
            <ta e="T24" id="Seg_991" s="T23">propr</ta>
            <ta e="T25" id="Seg_992" s="T24">propr</ta>
            <ta e="T26" id="Seg_993" s="T25">v</ta>
            <ta e="T27" id="Seg_994" s="T26">v</ta>
            <ta e="T28" id="Seg_995" s="T27">n</ta>
            <ta e="T29" id="Seg_996" s="T28">v</ta>
            <ta e="T30" id="Seg_997" s="T29">n</ta>
            <ta e="T31" id="Seg_998" s="T30">adv</ta>
            <ta e="T32" id="Seg_999" s="T31">v</ta>
            <ta e="T33" id="Seg_1000" s="T32">propr</ta>
            <ta e="T34" id="Seg_1001" s="T33">v</ta>
            <ta e="T35" id="Seg_1002" s="T34">v</ta>
            <ta e="T36" id="Seg_1003" s="T35">v</ta>
            <ta e="T37" id="Seg_1004" s="T36">propr</ta>
            <ta e="T38" id="Seg_1005" s="T37">num</ta>
            <ta e="T39" id="Seg_1006" s="T38">n</ta>
            <ta e="T40" id="Seg_1007" s="T39">v</ta>
            <ta e="T41" id="Seg_1008" s="T40">v</ta>
            <ta e="T42" id="Seg_1009" s="T41">adv</ta>
            <ta e="T43" id="Seg_1010" s="T42">v</ta>
            <ta e="T44" id="Seg_1011" s="T43">v</ta>
            <ta e="T45" id="Seg_1012" s="T44">propr</ta>
            <ta e="T46" id="Seg_1013" s="T45">adj</ta>
            <ta e="T47" id="Seg_1014" s="T46">n</ta>
            <ta e="T48" id="Seg_1015" s="T47">v</ta>
            <ta e="T49" id="Seg_1016" s="T48">v</ta>
            <ta e="T50" id="Seg_1017" s="T49">propr</ta>
            <ta e="T51" id="Seg_1018" s="T50">v</ta>
            <ta e="T52" id="Seg_1019" s="T51">n</ta>
            <ta e="T53" id="Seg_1020" s="T52">n</ta>
            <ta e="T54" id="Seg_1021" s="T53">v</ta>
            <ta e="T55" id="Seg_1022" s="T54">n</ta>
            <ta e="T56" id="Seg_1023" s="T55">n</ta>
            <ta e="T57" id="Seg_1024" s="T56">dempro</ta>
            <ta e="T58" id="Seg_1025" s="T57">v</ta>
            <ta e="T59" id="Seg_1026" s="T58">n</ta>
            <ta e="T60" id="Seg_1027" s="T59">num</ta>
            <ta e="T61" id="Seg_1028" s="T60">n</ta>
            <ta e="T62" id="Seg_1029" s="T61">v</ta>
            <ta e="T63" id="Seg_1030" s="T62">n</ta>
            <ta e="T64" id="Seg_1031" s="T63">v</ta>
            <ta e="T65" id="Seg_1032" s="T64">v</ta>
            <ta e="T66" id="Seg_1033" s="T65">v</ta>
            <ta e="T67" id="Seg_1034" s="T66">v</ta>
            <ta e="T68" id="Seg_1035" s="T67">n</ta>
            <ta e="T69" id="Seg_1036" s="T68">v</ta>
            <ta e="T70" id="Seg_1037" s="T69">adj</ta>
            <ta e="T71" id="Seg_1038" s="T70">n</ta>
            <ta e="T72" id="Seg_1039" s="T71">v</ta>
            <ta e="T73" id="Seg_1040" s="T72">v</ta>
            <ta e="T74" id="Seg_1041" s="T73">v</ta>
            <ta e="T75" id="Seg_1042" s="T74">n</ta>
            <ta e="T76" id="Seg_1043" s="T75">n</ta>
            <ta e="T77" id="Seg_1044" s="T76">v</ta>
            <ta e="T78" id="Seg_1045" s="T77">dempro</ta>
            <ta e="T79" id="Seg_1046" s="T78">n</ta>
            <ta e="T80" id="Seg_1047" s="T79">v</ta>
            <ta e="T81" id="Seg_1048" s="T80">dempro</ta>
            <ta e="T82" id="Seg_1049" s="T81">dempro</ta>
            <ta e="T83" id="Seg_1050" s="T82">adj</ta>
            <ta e="T84" id="Seg_1051" s="T83">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_1052" s="T0">np.h:Th</ta>
            <ta e="T3" id="Seg_1053" s="T2">np.h:Poss</ta>
            <ta e="T5" id="Seg_1054" s="T4">np.h:Th</ta>
            <ta e="T8" id="Seg_1055" s="T7">np.h:Th 0.3.h:Poss</ta>
            <ta e="T10" id="Seg_1056" s="T9">np:Com</ta>
            <ta e="T13" id="Seg_1057" s="T12">np.h:A</ta>
            <ta e="T14" id="Seg_1058" s="T13">0.3.h:E</ta>
            <ta e="T15" id="Seg_1059" s="T14">0.1.h:A</ta>
            <ta e="T16" id="Seg_1060" s="T15">np:G</ta>
            <ta e="T17" id="Seg_1061" s="T16">np:Poss</ta>
            <ta e="T18" id="Seg_1062" s="T17">np:Poss</ta>
            <ta e="T19" id="Seg_1063" s="T18">np:Th</ta>
            <ta e="T21" id="Seg_1064" s="T20">0.3.h:A</ta>
            <ta e="T22" id="Seg_1065" s="T21">0.2.h:A</ta>
            <ta e="T24" id="Seg_1066" s="T23">np.h:R</ta>
            <ta e="T25" id="Seg_1067" s="T24">np.h:A</ta>
            <ta e="T27" id="Seg_1068" s="T26">0.3.h:A</ta>
            <ta e="T28" id="Seg_1069" s="T27">np:Ins</ta>
            <ta e="T29" id="Seg_1070" s="T28">0.3.h:A</ta>
            <ta e="T30" id="Seg_1071" s="T29">np:P 0.3.h:Poss</ta>
            <ta e="T32" id="Seg_1072" s="T31">0.3.h:A</ta>
            <ta e="T33" id="Seg_1073" s="T32">np.h:Th</ta>
            <ta e="T36" id="Seg_1074" s="T35">0.3.h:A</ta>
            <ta e="T37" id="Seg_1075" s="T36">np.h:Poss</ta>
            <ta e="T39" id="Seg_1076" s="T38">np.h:Th</ta>
            <ta e="T41" id="Seg_1077" s="T40">0.3.h:A</ta>
            <ta e="T42" id="Seg_1078" s="T41">adv:Time</ta>
            <ta e="T44" id="Seg_1079" s="T43">0.3.h:A</ta>
            <ta e="T45" id="Seg_1080" s="T44">np.h:Poss</ta>
            <ta e="T47" id="Seg_1081" s="T46">np.h:A</ta>
            <ta e="T49" id="Seg_1082" s="T48">0.3.h:A</ta>
            <ta e="T50" id="Seg_1083" s="T49">np.h:A</ta>
            <ta e="T52" id="Seg_1084" s="T51">np:Poss</ta>
            <ta e="T53" id="Seg_1085" s="T52">np:Com</ta>
            <ta e="T54" id="Seg_1086" s="T53">0.3.h:A</ta>
            <ta e="T55" id="Seg_1087" s="T54">np:Poss</ta>
            <ta e="T56" id="Seg_1088" s="T55">np:E</ta>
            <ta e="T57" id="Seg_1089" s="T56">pro.h:Th</ta>
            <ta e="T59" id="Seg_1090" s="T58">np:G 0.3.h:Poss</ta>
            <ta e="T61" id="Seg_1091" s="T60">np:P 0.3.h:Poss</ta>
            <ta e="T62" id="Seg_1092" s="T61">0.3.h:A</ta>
            <ta e="T63" id="Seg_1093" s="T62">np:G</ta>
            <ta e="T65" id="Seg_1094" s="T64">0.3.h:A</ta>
            <ta e="T66" id="Seg_1095" s="T65">0.3.h:A</ta>
            <ta e="T67" id="Seg_1096" s="T66">np:L</ta>
            <ta e="T68" id="Seg_1097" s="T67">np:L</ta>
            <ta e="T69" id="Seg_1098" s="T68">0.3.h:Th</ta>
            <ta e="T71" id="Seg_1099" s="T70">np.h:A 0.3.h:Poss</ta>
            <ta e="T74" id="Seg_1100" s="T73">0.3.h:A</ta>
            <ta e="T75" id="Seg_1101" s="T74">np:Th 0.1.h:Poss</ta>
            <ta e="T76" id="Seg_1102" s="T75">np:Th 0.1.h:Poss</ta>
            <ta e="T77" id="Seg_1103" s="T76">0.2.h:A</ta>
            <ta e="T78" id="Seg_1104" s="T77">pro.h:A</ta>
            <ta e="T79" id="Seg_1105" s="T78">np:Th 0.3.h:Poss</ta>
            <ta e="T81" id="Seg_1106" s="T80">pro.h:A</ta>
            <ta e="T82" id="Seg_1107" s="T81">pro.h:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_1108" s="T0">np.h:S</ta>
            <ta e="T2" id="Seg_1109" s="T1">v:pred</ta>
            <ta e="T5" id="Seg_1110" s="T4">np.h:S</ta>
            <ta e="T6" id="Seg_1111" s="T5">v:pred</ta>
            <ta e="T8" id="Seg_1112" s="T7">np.h:S</ta>
            <ta e="T11" id="Seg_1113" s="T10">v:pred</ta>
            <ta e="T13" id="Seg_1114" s="T12">np.h:S</ta>
            <ta e="T14" id="Seg_1115" s="T13">v:pred 0.3.h:O</ta>
            <ta e="T15" id="Seg_1116" s="T14">v:pred 0.1.h:S</ta>
            <ta e="T20" id="Seg_1117" s="T16">s:purp</ta>
            <ta e="T21" id="Seg_1118" s="T20">v:pred 0.3.h:S</ta>
            <ta e="T22" id="Seg_1119" s="T21">v:pred 0.2.h:S</ta>
            <ta e="T23" id="Seg_1120" s="T22">v:pred 0.3.h:S</ta>
            <ta e="T25" id="Seg_1121" s="T24">np.h:S</ta>
            <ta e="T26" id="Seg_1122" s="T25">v:pred</ta>
            <ta e="T27" id="Seg_1123" s="T26">v:pred 0.3.h:S</ta>
            <ta e="T29" id="Seg_1124" s="T28">v:pred 0.3.h:S</ta>
            <ta e="T30" id="Seg_1125" s="T29">np:O</ta>
            <ta e="T32" id="Seg_1126" s="T31">v:pred 0.3.h:S</ta>
            <ta e="T33" id="Seg_1127" s="T32">np.h:S</ta>
            <ta e="T35" id="Seg_1128" s="T34">v:pred 0.3.h:S</ta>
            <ta e="T36" id="Seg_1129" s="T35">v:pred 0.3.h:S</ta>
            <ta e="T39" id="Seg_1130" s="T38">np.h:O</ta>
            <ta e="T41" id="Seg_1131" s="T40">v:pred 0.3.h:S</ta>
            <ta e="T44" id="Seg_1132" s="T43">v:pred 0.3.h:S</ta>
            <ta e="T47" id="Seg_1133" s="T46">np.h:S</ta>
            <ta e="T48" id="Seg_1134" s="T47">s:adv</ta>
            <ta e="T49" id="Seg_1135" s="T48">v:pred</ta>
            <ta e="T50" id="Seg_1136" s="T49">np.h:S</ta>
            <ta e="T51" id="Seg_1137" s="T50">v:pred</ta>
            <ta e="T54" id="Seg_1138" s="T53">v:pred 0.3.h:S</ta>
            <ta e="T56" id="Seg_1139" s="T55">np:S</ta>
            <ta e="T58" id="Seg_1140" s="T57">v:pred</ta>
            <ta e="T61" id="Seg_1141" s="T60">np:O</ta>
            <ta e="T62" id="Seg_1142" s="T61">v:pred 0.3.h:S</ta>
            <ta e="T65" id="Seg_1143" s="T64">v:pred 0.3.h:S</ta>
            <ta e="T66" id="Seg_1144" s="T65">v:pred 0.3.h:S</ta>
            <ta e="T69" id="Seg_1145" s="T68">v:pred 0.3.h:S</ta>
            <ta e="T71" id="Seg_1146" s="T70">np.h:S</ta>
            <ta e="T72" id="Seg_1147" s="T71">s:purp</ta>
            <ta e="T73" id="Seg_1148" s="T72">v:pred 0.3.h:S</ta>
            <ta e="T74" id="Seg_1149" s="T73">v:pred 0.3.h:S</ta>
            <ta e="T75" id="Seg_1150" s="T74">np:O</ta>
            <ta e="T76" id="Seg_1151" s="T75">np:O</ta>
            <ta e="T77" id="Seg_1152" s="T76">v:pred 0.2.h:S</ta>
            <ta e="T78" id="Seg_1153" s="T77">pro.h:S</ta>
            <ta e="T79" id="Seg_1154" s="T78">np:O</ta>
            <ta e="T80" id="Seg_1155" s="T79">v:pred</ta>
            <ta e="T81" id="Seg_1156" s="T80">pro.h:S</ta>
            <ta e="T82" id="Seg_1157" s="T81">pro.h:O</ta>
            <ta e="T84" id="Seg_1158" s="T83">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T1" id="Seg_1159" s="T0">new </ta>
            <ta e="T3" id="Seg_1160" s="T2">giv-active </ta>
            <ta e="T5" id="Seg_1161" s="T4">new </ta>
            <ta e="T8" id="Seg_1162" s="T7">accs-inf </ta>
            <ta e="T10" id="Seg_1163" s="T9">new </ta>
            <ta e="T13" id="Seg_1164" s="T12">giv-active </ta>
            <ta e="T14" id="Seg_1165" s="T13">0.giv-inactive</ta>
            <ta e="T15" id="Seg_1166" s="T14">accs-aggr-Q</ta>
            <ta e="T16" id="Seg_1167" s="T15">new </ta>
            <ta e="T19" id="Seg_1168" s="T18">new </ta>
            <ta e="T21" id="Seg_1169" s="T20">0.giv-active</ta>
            <ta e="T22" id="Seg_1170" s="T21">0.giv-active-Q</ta>
            <ta e="T23" id="Seg_1171" s="T22">0.giv-active quot-sp</ta>
            <ta e="T24" id="Seg_1172" s="T23">giv-active</ta>
            <ta e="T25" id="Seg_1173" s="T24">giv-active </ta>
            <ta e="T27" id="Seg_1174" s="T26">0.giv-active</ta>
            <ta e="T28" id="Seg_1175" s="T27">new </ta>
            <ta e="T29" id="Seg_1176" s="T28">0.giv-active</ta>
            <ta e="T30" id="Seg_1177" s="T29">giv-active </ta>
            <ta e="T32" id="Seg_1178" s="T31">0.giv-inactive</ta>
            <ta e="T33" id="Seg_1179" s="T32">giv-inactive </ta>
            <ta e="T36" id="Seg_1180" s="T35">0.giv-inactive</ta>
            <ta e="T39" id="Seg_1181" s="T38">giv-inactive </ta>
            <ta e="T44" id="Seg_1182" s="T43">accs-aggr</ta>
            <ta e="T47" id="Seg_1183" s="T46">giv-active </ta>
            <ta e="T50" id="Seg_1184" s="T49">giv-inactive </ta>
            <ta e="T53" id="Seg_1185" s="T52">giv-inactive </ta>
            <ta e="T54" id="Seg_1186" s="T53">0.giv-active</ta>
            <ta e="T56" id="Seg_1187" s="T55">giv-active </ta>
            <ta e="T57" id="Seg_1188" s="T56">giv-active </ta>
            <ta e="T59" id="Seg_1189" s="T58">accs-inf </ta>
            <ta e="T61" id="Seg_1190" s="T60">giv-active </ta>
            <ta e="T63" id="Seg_1191" s="T62">accs-gen </ta>
            <ta e="T65" id="Seg_1192" s="T64">0.giv-active</ta>
            <ta e="T66" id="Seg_1193" s="T65">0.giv-active</ta>
            <ta e="T67" id="Seg_1194" s="T66">new </ta>
            <ta e="T68" id="Seg_1195" s="T67">accs-sit </ta>
            <ta e="T69" id="Seg_1196" s="T68">0.giv-active</ta>
            <ta e="T71" id="Seg_1197" s="T70">giv-inactive </ta>
            <ta e="T72" id="Seg_1198" s="T71">new </ta>
            <ta e="T74" id="Seg_1199" s="T73">0.giv-inactive quot-sp</ta>
            <ta e="T75" id="Seg_1200" s="T74">new </ta>
            <ta e="T76" id="Seg_1201" s="T75">new </ta>
            <ta e="T77" id="Seg_1202" s="T76">0.giv-active-Q</ta>
            <ta e="T78" id="Seg_1203" s="T77">giv-active </ta>
            <ta e="T79" id="Seg_1204" s="T78">giv-active </ta>
            <ta e="T81" id="Seg_1205" s="T80">giv-inactive </ta>
            <ta e="T82" id="Seg_1206" s="T81">giv-inactive </ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T9" id="Seg_1207" s="T8">TURK:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="fr" tierref="fr">
            <ta e="T2" id="Seg_1208" s="T0">Жил-был Ураже.</ta>
            <ta e="T6" id="Seg_1209" s="T2">Было у Ураже две жены.</ta>
            <ta e="T11" id="Seg_1210" s="T6">Одна из них жила с другим человеком.</ta>
            <ta e="T14" id="Seg_1211" s="T11">Этот человек зовет Ураже:</ta>
            <ta e="T15" id="Seg_1212" s="T14">"Пойдём".</ta>
            <ta e="T21" id="Seg_1213" s="T15">Пошли они к утёсу посмотреть на птенцов в гнезде ворона.</ta>
            <ta e="T22" id="Seg_1214" s="T21">"Забирайся [в гнездо]!" — </ta>
            <ta e="T24" id="Seg_1215" s="T22">говорит он Ураже.</ta>
            <ta e="T27" id="Seg_1216" s="T24">Ураже полез и [в гнездо] залез.</ta>
            <ta e="T29" id="Seg_1217" s="T27">По верёвке вниз спустился. </ta>
            <ta e="T32" id="Seg_1218" s="T29">[Другой] верёвку обрезал.</ta>
            <ta e="T35" id="Seg_1219" s="T32">Ураже лежать остался.</ta>
            <ta e="T41" id="Seg_1220" s="T35">Тот ушёл и увёл обеих жён Ураже с собой.</ta>
            <ta e="T44" id="Seg_1221" s="T41">Затем они [в другое место] отправились. </ta>
            <ta e="T49" id="Seg_1222" s="T44">Прежняя жена Ураже идёт и плачет.</ta>
            <ta e="T51" id="Seg_1223" s="T49">Ураже смотрит.</ta>
            <ta e="T54" id="Seg_1224" s="T51">Вместе с воронятами ест. </ta>
            <ta e="T58" id="Seg_1225" s="T54">Воронята к нему привыкли.</ta>
            <ta e="T66" id="Seg_1226" s="T58">Привязал он двоих птенцов себе под мышки, на землю слетел и пошёл.</ta>
            <ta e="T69" id="Seg_1227" s="T66">Сел у реки, где воду набирают.</ta>
            <ta e="T73" id="Seg_1228" s="T69">Жена его прежняя за водой пришла.</ta>
            <ta e="T74" id="Seg_1229" s="T73">Он говорит:</ta>
            <ta e="T77" id="Seg_1230" s="T74">"Принеси мне мой лук и стрелу!"</ta>
            <ta e="T80" id="Seg_1231" s="T77">Она принесла лук.</ta>
            <ta e="T84" id="Seg_1232" s="T80">Застрелил он [того человека].</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T2" id="Seg_1233" s="T0">There lived Uraže.</ta>
            <ta e="T6" id="Seg_1234" s="T2">Uraže had two wives.</ta>
            <ta e="T11" id="Seg_1235" s="T6">One wife lived with another man.</ta>
            <ta e="T14" id="Seg_1236" s="T11">This man invites him:</ta>
            <ta e="T15" id="Seg_1237" s="T14">"Let’s go!"</ta>
            <ta e="T21" id="Seg_1238" s="T15">They went to the cliff to see the raven’s nest’s nestlings.</ta>
            <ta e="T22" id="Seg_1239" s="T21">"Climb [up]!", </ta>
            <ta e="T24" id="Seg_1240" s="T22">he says to Uraže.</ta>
            <ta e="T27" id="Seg_1241" s="T24">Uraže climbed up and let himself down [into the nest].</ta>
            <ta e="T29" id="Seg_1242" s="T27">With a rope he let himself down.</ta>
            <ta e="T32" id="Seg_1243" s="T29">[The other] cut the rope off.</ta>
            <ta e="T35" id="Seg_1244" s="T32">Uraže stayed there.</ta>
            <ta e="T41" id="Seg_1245" s="T35">He went and took Uraže’s two wives away.</ta>
            <ta e="T44" id="Seg_1246" s="T41">Then they move away.</ta>
            <ta e="T49" id="Seg_1247" s="T44">Uraže's old wife walks crying.</ta>
            <ta e="T51" id="Seg_1248" s="T49">Uraže is looking.</ta>
            <ta e="T54" id="Seg_1249" s="T51">He eats together with the raven’s nestlings.</ta>
            <ta e="T58" id="Seg_1250" s="T54">The raven’s nestlings got accustomed to him.</ta>
            <ta e="T66" id="Seg_1251" s="T58">He tied two nestlings to his armpits, flew down to the ground and went off.</ta>
            <ta e="T69" id="Seg_1252" s="T66">He sits at the water hole by the river.</ta>
            <ta e="T73" id="Seg_1253" s="T69">His old wife came to fetch water.</ta>
            <ta e="T74" id="Seg_1254" s="T73">He says:</ta>
            <ta e="T77" id="Seg_1255" s="T74">"Bring my bow and arrow!"</ta>
            <ta e="T80" id="Seg_1256" s="T77">She brought his bow.</ta>
            <ta e="T84" id="Seg_1257" s="T80">He shot [that man] dead.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T2" id="Seg_1258" s="T0">Uraže lebt.</ta>
            <ta e="T6" id="Seg_1259" s="T2">Uraže hatte zwei Frauen.</ta>
            <ta e="T11" id="Seg_1260" s="T6">Eine seiner Frauen lebte mit einem anderen Mann.</ta>
            <ta e="T14" id="Seg_1261" s="T11">Dieser Mann lädt ihn ein:</ta>
            <ta e="T15" id="Seg_1262" s="T14">"Gehen wir!"</ta>
            <ta e="T21" id="Seg_1263" s="T15">Sie gingen zur Felswand, die Küken im Rabennest zu beobachten.</ta>
            <ta e="T22" id="Seg_1264" s="T21">"Klettere hinauf!",</ta>
            <ta e="T24" id="Seg_1265" s="T22">sagt er zu Uraže.</ta>
            <ta e="T27" id="Seg_1266" s="T24">Uraže kletterte hinauf und ließ sich [in das Nest] hinunter.</ta>
            <ta e="T29" id="Seg_1267" s="T27">Mit einem Seil ließ er sich hinunter.</ta>
            <ta e="T32" id="Seg_1268" s="T29">[Der Mann] schnitt das Seil ab. </ta>
            <ta e="T35" id="Seg_1269" s="T32">Uraže blieb zurück.</ta>
            <ta e="T41" id="Seg_1270" s="T35">Er ging davon und entführte Uražes zwei Frauen.</ta>
            <ta e="T44" id="Seg_1271" s="T41">Dann zog er davon.</ta>
            <ta e="T49" id="Seg_1272" s="T44">Uražes alte Frau weint auf der Wanderung.</ta>
            <ta e="T51" id="Seg_1273" s="T49">Uraže beobachtet [das?]. </ta>
            <ta e="T54" id="Seg_1274" s="T51">Er isst mit den Rabenküken.</ta>
            <ta e="T58" id="Seg_1275" s="T54">Die Rabenküken gewöhnten sich an ihn.</ta>
            <ta e="T66" id="Seg_1276" s="T58">Er band zwei Küken unter seine Achseln, flog hinunter auf die Erde und ging fort.</ta>
            <ta e="T69" id="Seg_1277" s="T66">Er sitzt an der Wasserentnahmestelle am Fluss.</ta>
            <ta e="T73" id="Seg_1278" s="T69">Seine alte Frau kam Wasser holen.</ta>
            <ta e="T74" id="Seg_1279" s="T73">Er sagt:</ta>
            <ta e="T77" id="Seg_1280" s="T74">"Bring mir meinen Bogen und meinen Pfeil!"</ta>
            <ta e="T80" id="Seg_1281" s="T77">Sie brachte ihm den Bogen.</ta>
            <ta e="T84" id="Seg_1282" s="T80">Er schoss [den Mann] tot.</ta>
         </annotation>
         <annotation name="ltg" tierref="ltg">
            <ta e="T2" id="Seg_1283" s="T0">Urāžᵊ lebt.</ta>
            <ta e="T6" id="Seg_1284" s="T2">Urāžᵊ hatte ⌈seine⌉ zwei Frauen.</ta>
            <ta e="T11" id="Seg_1285" s="T6">Eine seiner Frauen mit einem anderen Manne lebte.</ta>
            <ta e="T14" id="Seg_1286" s="T11">Dieser Mann bittet:</ta>
            <ta e="T21" id="Seg_1287" s="T15">»Lasst uns an die steile Felsen wand gehen, um die Jungen des Rabennestes zu beschauen!» Sie gingen.</ta>
            <ta e="T24" id="Seg_1288" s="T22">Als er zu Urāzᵊ kletterte[?],</ta>
            <ta e="T27" id="Seg_1289" s="T24">Urāzᵊ kletterte, zu der schroffen Wand liess er ihn an einem Strick herab.</ta>
            <ta e="T32" id="Seg_1290" s="T29">Dann sah er, den Strick aber schnitt er durch.</ta>
            <ta e="T35" id="Seg_1291" s="T32">Urāzᵊ ⌈zu liegen fiel⌉ fiel hin</ta>
            <ta e="T41" id="Seg_1292" s="T35">Er ging, Urāžᵊs zwei Weiber aber nehmend führte weg.</ta>
            <ta e="T44" id="Seg_1293" s="T41">Dann siedelt er über.</ta>
            <ta e="T49" id="Seg_1294" s="T44">Das Weib des Urāžᵊ weinend wandert.</ta>
            <ta e="T51" id="Seg_1295" s="T49">Urāžᵊ schaut,</ta>
            <ta e="T54" id="Seg_1296" s="T51">isst junge Raben.</ta>
            <ta e="T58" id="Seg_1297" s="T54">An ihn gewöhnte[n] [sie] sich.</ta>
            <ta e="T66" id="Seg_1298" s="T58">Band [er] aber in seine Armhöhle zwei Jungen, liess sich auf die Erde herab, begab sich fort.</ta>
            <ta e="T69" id="Seg_1299" s="T66">Sitzt an der Wassernehmstelle bei dem Fluss.</ta>
            <ta e="T73" id="Seg_1300" s="T69">Sein Weib Wasser holen kam.</ta>
            <ta e="T74" id="Seg_1301" s="T73">Er sagt:</ta>
            <ta e="T77" id="Seg_1302" s="T74">»Meinen Bogen, meinen Pfeil bringe!»</ta>
            <ta e="T80" id="Seg_1303" s="T77">Sie brachte wohl den Bogen.</ta>
            <ta e="T84" id="Seg_1304" s="T80">Er schoss sie tot.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltg"
                          display-name="ltg"
                          name="ltg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
