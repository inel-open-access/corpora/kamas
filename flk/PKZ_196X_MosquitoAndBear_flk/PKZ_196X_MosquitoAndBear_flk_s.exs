<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDD1CD61A6-0080-1154-2B19-B64C1513538C">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_MosquitoAndBear_flk.wav" />
         <referenced-file url="PKZ_196X_MosquitoAndBear_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_MosquitoAndBear_flk\PKZ_196X_MosquitoAndBear_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">213</ud-information>
            <ud-information attribute-name="# HIAT:w">132</ud-information>
            <ud-information attribute-name="# e">133</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">1</ud-information>
            <ud-information attribute-name="# HIAT:u">32</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1765" time="0.076" type="appl" />
         <tli id="T1766" time="0.827" type="appl" />
         <tli id="T1767" time="1.578" type="appl" />
         <tli id="T1768" time="2.328" type="appl" />
         <tli id="T1769" time="3.2997845792048803" />
         <tli id="T1770" time="4.199" type="appl" />
         <tli id="T1771" time="5.319" type="appl" />
         <tli id="T1772" time="6.223" type="appl" />
         <tli id="T1773" time="7.128" type="appl" />
         <tli id="T1774" time="8.686099609502948" />
         <tli id="T1775" time="9.5" type="appl" />
         <tli id="T1776" time="10.22" type="appl" />
         <tli id="T1777" time="10.941" type="appl" />
         <tli id="T1778" time="11.662" type="appl" />
         <tli id="T1779" time="12.383" type="appl" />
         <tli id="T1780" time="13.103" type="appl" />
         <tli id="T1781" time="13.824" type="appl" />
         <tli id="T1782" time="14.465" type="appl" />
         <tli id="T1783" time="15.945625683753684" />
         <tli id="T1784" time="16.88" type="appl" />
         <tli id="T1785" time="17.8" type="appl" />
         <tli id="T1786" time="18.72" type="appl" />
         <tli id="T1787" time="19.272" type="appl" />
         <tli id="T1788" time="19.823" type="appl" />
         <tli id="T1789" time="20.409" type="appl" />
         <tli id="T1790" time="20.851" type="appl" />
         <tli id="T1791" time="21.294" type="appl" />
         <tli id="T1792" time="21.736" type="appl" />
         <tli id="T1793" time="22.178" type="appl" />
         <tli id="T1794" time="22.885" type="appl" />
         <tli id="T1795" time="23.592" type="appl" />
         <tli id="T1796" time="24.298" type="appl" />
         <tli id="T1797" time="25.005" type="appl" />
         <tli id="T1798" time="25.666" type="appl" />
         <tli id="T1799" time="26.326" type="appl" />
         <tli id="T1800" time="26.987" type="appl" />
         <tli id="T1801" time="27.648" type="appl" />
         <tli id="T1802" time="28.523" type="appl" />
         <tli id="T1803" time="29.15" type="appl" />
         <tli id="T1804" time="29.778" type="appl" />
         <tli id="T1805" time="30.197" type="appl" />
         <tli id="T1806" time="30.784656942965935" />
         <tli id="T1807" time="31.274" type="appl" />
         <tli id="T1808" time="31.673" type="appl" />
         <tli id="T1809" time="32.073" type="appl" />
         <tli id="T1810" time="32.473" type="appl" />
         <tli id="T1811" time="32.873" type="appl" />
         <tli id="T1812" time="33.272" type="appl" />
         <tli id="T1813" time="33.672" type="appl" />
         <tli id="T1814" time="34.408" type="appl" />
         <tli id="T1815" time="35.144" type="appl" />
         <tli id="T1816" time="35.881" type="appl" />
         <tli id="T1817" time="37.74420260092532" />
         <tli id="T1818" time="38.439" type="appl" />
         <tli id="T1819" time="39.257" type="appl" />
         <tli id="T1820" time="40.074" type="appl" />
         <tli id="T1821" time="40.414" type="appl" />
         <tli id="T1822" time="40.754" type="appl" />
         <tli id="T1823" time="41.093" type="appl" />
         <tli id="T1824" time="41.433" type="appl" />
         <tli id="T1825" time="41.935" type="appl" />
         <tli id="T1826" time="42.436" type="appl" />
         <tli id="T1827" time="43.46382920488045" />
         <tli id="T1828" time="44.171" type="appl" />
         <tli id="T1829" time="44.94" type="appl" />
         <tli id="T1830" time="45.708" type="appl" />
         <tli id="T1831" time="46.477" type="appl" />
         <tli id="T1832" time="47.245" type="appl" />
         <tli id="T1833" time="48.013" type="appl" />
         <tli id="T1834" time="48.782" type="appl" />
         <tli id="T1835" time="49.55" type="appl" />
         <tli id="T1836" time="50.319" type="appl" />
         <tli id="T1837" time="51.087" type="appl" />
         <tli id="T1838" time="51.56" type="appl" />
         <tli id="T1839" time="52.032" type="appl" />
         <tli id="T1840" time="52.878" type="appl" />
         <tli id="T1841" time="53.724" type="appl" />
         <tli id="T1842" time="54.57" type="appl" />
         <tli id="T1843" time="55.415" type="appl" />
         <tli id="T1844" time="56.261" type="appl" />
         <tli id="T1845" time="57.107" type="appl" />
         <tli id="T1846" time="57.953" type="appl" />
         <tli id="T1847" time="58.468" type="appl" />
         <tli id="T1848" time="58.982" type="appl" />
         <tli id="T1849" time="59.497" type="appl" />
         <tli id="T1850" time="60.011" type="appl" />
         <tli id="T1851" time="60.517" type="appl" />
         <tli id="T1852" time="61.022" type="appl" />
         <tli id="T1853" time="62.029283857578605" />
         <tli id="T1854" time="62.844" type="appl" />
         <tli id="T1855" time="63.662" type="appl" />
         <tli id="T1856" time="64.48" type="appl" />
         <tli id="T1857" time="65.299" type="appl" />
         <tli id="T1858" time="66.118" type="appl" />
         <tli id="T1859" time="67.00895876801508" />
         <tli id="T1860" time="67.477" type="appl" />
         <tli id="T1861" time="68.019" type="appl" />
         <tli id="T1862" time="68.56" type="appl" />
         <tli id="T1863" time="69.097" type="appl" />
         <tli id="T1864" time="69.634" type="appl" />
         <tli id="T1865" time="70.338" type="appl" />
         <tli id="T1866" time="71.042" type="appl" />
         <tli id="T1867" time="71.745" type="appl" />
         <tli id="T1868" time="72.449" type="appl" />
         <tli id="T1869" time="73.153" type="appl" />
         <tli id="T1870" time="74.4618055549869" />
         <tli id="T1871" time="75.298" type="appl" />
         <tli id="T1872" time="76.215" type="appl" />
         <tli id="T1873" time="77.133" type="appl" />
         <tli id="T1874" time="78.621533994227" />
         <tli id="T1875" time="79.542" type="appl" />
         <tli id="T1876" time="80.563" type="appl" />
         <tli id="T1877" time="81.585" type="appl" />
         <tli id="T1878" time="82.607" type="appl" />
         <tli id="T1879" time="83.628" type="appl" />
         <tli id="T1880" time="84.65" type="appl" />
         <tli id="T1881" time="85.784" type="appl" />
         <tli id="T1882" time="86.757" type="appl" />
         <tli id="T1883" time="87.731" type="appl" />
         <tli id="T1884" time="88.704" type="appl" />
         <tli id="T1885" time="89.678" type="appl" />
         <tli id="T1886" time="90.349" type="appl" />
         <tli id="T1887" time="90.957" type="appl" />
         <tli id="T1888" time="91.565" type="appl" />
         <tli id="T1889" time="92.173" type="appl" />
         <tli id="T1890" time="92.781" type="appl" />
         <tli id="T1891" time="95.51376454716672" />
         <tli id="T1892" time="96.723" type="appl" />
         <tli id="T1893" time="97.546" type="appl" />
         <tli id="T1894" time="98.37" type="appl" />
         <tli id="T1895" time="99.193" type="appl" />
         <tli id="T1896" time="99.9801396341713" />
         <tli id="T0" time="100.30733333333333" type="intp" />
         <tli id="T1897" time="101.66669619687602" />
         <tli id="T1898" time="101.76" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T1897" id="Seg_0" n="sc" s="T1765">
               <ts e="T1769" id="Seg_2" n="HIAT:u" s="T1765">
                  <ts e="T1766" id="Seg_4" n="HIAT:w" s="T1765">Urgaːba</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1767" id="Seg_7" n="HIAT:w" s="T1766">i</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1768" id="Seg_10" n="HIAT:w" s="T1767">kamar</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1769" id="Seg_13" n="HIAT:w" s="T1768">dʼăbaktərlaʔbə</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1774" id="Seg_17" n="HIAT:u" s="T1769">
                  <ts e="T1770" id="Seg_19" n="HIAT:w" s="T1769">Urgaːba</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1771" id="Seg_22" n="HIAT:w" s="T1770">surarlaʔbə:</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_24" n="HIAT:ip">"</nts>
                  <ts e="T1772" id="Seg_26" n="HIAT:w" s="T1771">Šindən</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_28" n="HIAT:ip">(</nts>
                  <ts e="T1773" id="Seg_30" n="HIAT:w" s="T1772">kemdə</ts>
                  <nts id="Seg_31" n="HIAT:ip">)</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1774" id="Seg_34" n="HIAT:w" s="T1773">nʼamga</ts>
                  <nts id="Seg_35" n="HIAT:ip">?</nts>
                  <nts id="Seg_36" n="HIAT:ip">"</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1781" id="Seg_39" n="HIAT:u" s="T1774">
                  <ts e="T1775" id="Seg_41" n="HIAT:w" s="T1774">A</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1776" id="Seg_44" n="HIAT:w" s="T1775">kamar</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1777" id="Seg_47" n="HIAT:w" s="T1776">măndə:</ts>
                  <nts id="Seg_48" n="HIAT:ip">"</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1778" id="Seg_51" n="HIAT:w" s="T1777">Kuzan</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1779" id="Seg_54" n="HIAT:w" s="T1778">kemdə</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1780" id="Seg_57" n="HIAT:w" s="T1779">ugaːndə</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1781" id="Seg_60" n="HIAT:w" s="T1780">nʼamga</ts>
                  <nts id="Seg_61" n="HIAT:ip">"</nts>
                  <nts id="Seg_62" n="HIAT:ip">.</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1783" id="Seg_65" n="HIAT:u" s="T1781">
                  <ts e="T1782" id="Seg_67" n="HIAT:w" s="T1781">Dĭ</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1783" id="Seg_70" n="HIAT:w" s="T1782">kandəga</ts>
                  <nts id="Seg_71" n="HIAT:ip">.</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1786" id="Seg_74" n="HIAT:u" s="T1783">
                  <ts e="T1784" id="Seg_76" n="HIAT:w" s="T1783">Šonəga</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1785" id="Seg_79" n="HIAT:w" s="T1784">üdʼüge</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1786" id="Seg_82" n="HIAT:w" s="T1785">nʼi</ts>
                  <nts id="Seg_83" n="HIAT:ip">.</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1788" id="Seg_86" n="HIAT:u" s="T1786">
                  <nts id="Seg_87" n="HIAT:ip">"</nts>
                  <ts e="T1787" id="Seg_89" n="HIAT:w" s="T1786">Tăn</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1788" id="Seg_92" n="HIAT:w" s="T1787">kuza</ts>
                  <nts id="Seg_93" n="HIAT:ip">?</nts>
                  <nts id="Seg_94" n="HIAT:ip">"</nts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1793" id="Seg_97" n="HIAT:u" s="T1788">
                  <nts id="Seg_98" n="HIAT:ip">"</nts>
                  <ts e="T1789" id="Seg_100" n="HIAT:w" s="T1788">Dʼok</ts>
                  <nts id="Seg_101" n="HIAT:ip">,</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1790" id="Seg_104" n="HIAT:w" s="T1789">măn</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1791" id="Seg_107" n="HIAT:w" s="T1790">ej</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1792" id="Seg_110" n="HIAT:w" s="T1791">kuza</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1793" id="Seg_113" n="HIAT:w" s="T1792">iššo</ts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1797" id="Seg_117" n="HIAT:u" s="T1793">
                  <ts e="T1794" id="Seg_119" n="HIAT:w" s="T1793">Kamən-nʼibudʼ</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1795" id="Seg_122" n="HIAT:w" s="T1794">özerləm</ts>
                  <nts id="Seg_123" n="HIAT:ip">,</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1796" id="Seg_126" n="HIAT:w" s="T1795">molam</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1797" id="Seg_129" n="HIAT:w" s="T1796">kuza</ts>
                  <nts id="Seg_130" n="HIAT:ip">"</nts>
                  <nts id="Seg_131" n="HIAT:ip">.</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1801" id="Seg_134" n="HIAT:u" s="T1797">
                  <ts e="T1798" id="Seg_136" n="HIAT:w" s="T1797">Dĭ</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_138" n="HIAT:ip">(</nts>
                  <ts e="T1799" id="Seg_140" n="HIAT:w" s="T1798">baruʔluʔpi</ts>
                  <nts id="Seg_141" n="HIAT:ip">)</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1800" id="Seg_144" n="HIAT:w" s="T1799">i</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1801" id="Seg_147" n="HIAT:w" s="T1800">kandəga</ts>
                  <nts id="Seg_148" n="HIAT:ip">.</nts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1804" id="Seg_151" n="HIAT:u" s="T1801">
                  <ts e="T1802" id="Seg_153" n="HIAT:w" s="T1801">Dĭgəttə</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1803" id="Seg_156" n="HIAT:w" s="T1802">šonəga</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1804" id="Seg_159" n="HIAT:w" s="T1803">büzʼe</ts>
                  <nts id="Seg_160" n="HIAT:ip">.</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1806" id="Seg_163" n="HIAT:u" s="T1804">
                  <nts id="Seg_164" n="HIAT:ip">"</nts>
                  <ts e="T1805" id="Seg_166" n="HIAT:w" s="T1804">Tăn</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1806" id="Seg_169" n="HIAT:w" s="T1805">kuza</ts>
                  <nts id="Seg_170" n="HIAT:ip">?</nts>
                  <nts id="Seg_171" n="HIAT:ip">"</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1813" id="Seg_174" n="HIAT:u" s="T1806">
                  <nts id="Seg_175" n="HIAT:ip">"</nts>
                  <ts e="T1807" id="Seg_177" n="HIAT:w" s="T1806">Da</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1808" id="Seg_180" n="HIAT:w" s="T1807">ibiem</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1809" id="Seg_183" n="HIAT:w" s="T1808">kuza</ts>
                  <nts id="Seg_184" n="HIAT:ip">,</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1810" id="Seg_187" n="HIAT:w" s="T1809">a</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1811" id="Seg_190" n="HIAT:w" s="T1810">tüj</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1812" id="Seg_193" n="HIAT:w" s="T1811">ej</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1813" id="Seg_196" n="HIAT:w" s="T1812">kuza</ts>
                  <nts id="Seg_197" n="HIAT:ip">"</nts>
                  <nts id="Seg_198" n="HIAT:ip">.</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1817" id="Seg_201" n="HIAT:u" s="T1813">
                  <ts e="T1814" id="Seg_203" n="HIAT:w" s="T1813">Dĭgəttə</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1815" id="Seg_206" n="HIAT:w" s="T1814">bazoʔ</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1816" id="Seg_209" n="HIAT:w" s="T1815">kandəga</ts>
                  <nts id="Seg_210" n="HIAT:ip">,</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1817" id="Seg_213" n="HIAT:w" s="T1816">baruʔluʔpi</ts>
                  <nts id="Seg_214" n="HIAT:ip">.</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1820" id="Seg_217" n="HIAT:u" s="T1817">
                  <ts e="T1818" id="Seg_219" n="HIAT:w" s="T1817">Šonəga</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1819" id="Seg_222" n="HIAT:w" s="T1818">onʼiʔ</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1820" id="Seg_225" n="HIAT:w" s="T1819">kuza</ts>
                  <nts id="Seg_226" n="HIAT:ip">.</nts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1824" id="Seg_229" n="HIAT:u" s="T1820">
                  <nts id="Seg_230" n="HIAT:ip">"</nts>
                  <ts e="T1821" id="Seg_232" n="HIAT:w" s="T1820">Tăn</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1822" id="Seg_235" n="HIAT:w" s="T1821">kuza</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_237" n="HIAT:ip">(</nts>
                  <ts e="T1823" id="Seg_239" n="HIAT:w" s="T1822">măn-</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1824" id="Seg_242" n="HIAT:w" s="T1823">măn-</ts>
                  <nts id="Seg_243" n="HIAT:ip">)</nts>
                  <nts id="Seg_244" n="HIAT:ip">?</nts>
                  <nts id="Seg_245" n="HIAT:ip">"</nts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1827" id="Seg_248" n="HIAT:u" s="T1824">
                  <ts e="T1825" id="Seg_250" n="HIAT:w" s="T1824">Dĭ</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1826" id="Seg_253" n="HIAT:w" s="T1825">măndə:</ts>
                  <nts id="Seg_254" n="HIAT:ip">"</nts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1827" id="Seg_257" n="HIAT:w" s="T1826">Kuza</ts>
                  <nts id="Seg_258" n="HIAT:ip">"</nts>
                  <nts id="Seg_259" n="HIAT:ip">.</nts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1837" id="Seg_262" n="HIAT:u" s="T1827">
                  <ts e="T1828" id="Seg_264" n="HIAT:w" s="T1827">Dĭgəttə</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1829" id="Seg_267" n="HIAT:w" s="T1828">dĭ</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1830" id="Seg_270" n="HIAT:w" s="T1829">kandəga</ts>
                  <nts id="Seg_271" n="HIAT:ip">,</nts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_273" n="HIAT:ip">(</nts>
                  <ts e="T1831" id="Seg_275" n="HIAT:w" s="T1830">a</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1832" id="Seg_278" n="HIAT:w" s="T1831">dĭ=</ts>
                  <nts id="Seg_279" n="HIAT:ip">)</nts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_281" n="HIAT:ip">(</nts>
                  <ts e="T1833" id="Seg_283" n="HIAT:w" s="T1832">dĭ=</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1834" id="Seg_286" n="HIAT:w" s="T1833">dĭʔnə=</ts>
                  <nts id="Seg_287" n="HIAT:ip">)</nts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1835" id="Seg_290" n="HIAT:w" s="T1834">diʔgəʔ</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1836" id="Seg_293" n="HIAT:w" s="T1835">ej</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1837" id="Seg_296" n="HIAT:w" s="T1836">malia</ts>
                  <nts id="Seg_297" n="HIAT:ip">.</nts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1839" id="Seg_300" n="HIAT:u" s="T1837">
                  <ts e="T1838" id="Seg_302" n="HIAT:w" s="T1837">Tože</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1839" id="Seg_305" n="HIAT:w" s="T1838">šolia</ts>
                  <nts id="Seg_306" n="HIAT:ip">.</nts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1846" id="Seg_309" n="HIAT:u" s="T1839">
                  <nts id="Seg_310" n="HIAT:ip">(</nts>
                  <ts e="T1840" id="Seg_312" n="HIAT:w" s="T1839">Dĭ=</ts>
                  <nts id="Seg_313" n="HIAT:ip">)</nts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_315" n="HIAT:ip">(</nts>
                  <ts e="T1841" id="Seg_317" n="HIAT:w" s="T1840">Dĭ</ts>
                  <nts id="Seg_318" n="HIAT:ip">)</nts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1842" id="Seg_321" n="HIAT:w" s="T1841">ibi</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1843" id="Seg_324" n="HIAT:w" s="T1842">sablʼabə</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1844" id="Seg_327" n="HIAT:w" s="T1843">i</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1845" id="Seg_330" n="HIAT:w" s="T1844">dĭm</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1846" id="Seg_333" n="HIAT:w" s="T1845">toʔnarluʔpi</ts>
                  <nts id="Seg_334" n="HIAT:ip">.</nts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1850" id="Seg_337" n="HIAT:u" s="T1846">
                  <ts e="T1847" id="Seg_339" n="HIAT:w" s="T1846">Dĭn</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1848" id="Seg_342" n="HIAT:w" s="T1847">bar</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1849" id="Seg_345" n="HIAT:w" s="T1848">kemdə</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1850" id="Seg_348" n="HIAT:w" s="T1849">mʼaŋŋaʔbə</ts>
                  <nts id="Seg_349" n="HIAT:ip">.</nts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1853" id="Seg_352" n="HIAT:u" s="T1850">
                  <ts e="T1851" id="Seg_354" n="HIAT:w" s="T1850">Dĭ</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1852" id="Seg_357" n="HIAT:w" s="T1851">bar</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1853" id="Seg_360" n="HIAT:w" s="T1852">nuʔməluʔpi</ts>
                  <nts id="Seg_361" n="HIAT:ip">.</nts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1859" id="Seg_364" n="HIAT:u" s="T1853">
                  <ts e="T1854" id="Seg_366" n="HIAT:w" s="T1853">A</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1855" id="Seg_369" n="HIAT:w" s="T1854">dĭ</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_371" n="HIAT:ip">(</nts>
                  <ts e="T1856" id="Seg_373" n="HIAT:w" s="T1855">multus-</ts>
                  <nts id="Seg_374" n="HIAT:ip">)</nts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1857" id="Seg_377" n="HIAT:w" s="T1856">multuksiʔ</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1858" id="Seg_380" n="HIAT:w" s="T1857">tʼitluʔpi</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1859" id="Seg_383" n="HIAT:w" s="T1858">dĭ</ts>
                  <nts id="Seg_384" n="HIAT:ip">.</nts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1862" id="Seg_387" n="HIAT:u" s="T1859">
                  <ts e="T1860" id="Seg_389" n="HIAT:w" s="T1859">Dĭ</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1861" id="Seg_392" n="HIAT:w" s="T1860">bar</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1862" id="Seg_395" n="HIAT:w" s="T1861">nuʔməluʔpi</ts>
                  <nts id="Seg_396" n="HIAT:ip">.</nts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1864" id="Seg_399" n="HIAT:u" s="T1862">
                  <ts e="T1863" id="Seg_401" n="HIAT:w" s="T1862">Dĭgəttə</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1864" id="Seg_404" n="HIAT:w" s="T1863">šobi</ts>
                  <nts id="Seg_405" n="HIAT:ip">.</nts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1870" id="Seg_408" n="HIAT:u" s="T1864">
                  <nts id="Seg_409" n="HIAT:ip">"</nts>
                  <ts e="T1865" id="Seg_411" n="HIAT:w" s="T1864">Tăn</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1866" id="Seg_414" n="HIAT:w" s="T1865">măna</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1867" id="Seg_417" n="HIAT:w" s="T1866">măndəl</ts>
                  <nts id="Seg_418" n="HIAT:ip">,</nts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1868" id="Seg_421" n="HIAT:w" s="T1867">nʼamga</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1869" id="Seg_424" n="HIAT:w" s="T1868">kem</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1870" id="Seg_427" n="HIAT:w" s="T1869">kuzan</ts>
                  <nts id="Seg_428" n="HIAT:ip">.</nts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1874" id="Seg_431" n="HIAT:u" s="T1870">
                  <ts e="T1871" id="Seg_433" n="HIAT:w" s="T1870">Dĭn</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1872" id="Seg_436" n="HIAT:w" s="T1871">ugaːndə</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1873" id="Seg_439" n="HIAT:w" s="T1872">šĭkət</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1874" id="Seg_442" n="HIAT:w" s="T1873">numo</ts>
                  <nts id="Seg_443" n="HIAT:ip">.</nts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1880" id="Seg_446" n="HIAT:u" s="T1874">
                  <ts e="T1875" id="Seg_448" n="HIAT:w" s="T1874">Šĭketsiʔ</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1876" id="Seg_451" n="HIAT:w" s="T1875">măna</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1877" id="Seg_454" n="HIAT:w" s="T1876">toʔnarbi</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1878" id="Seg_457" n="HIAT:w" s="T1877">bar</ts>
                  <nts id="Seg_458" n="HIAT:ip">,</nts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1879" id="Seg_461" n="HIAT:w" s="T1878">kem</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1880" id="Seg_464" n="HIAT:w" s="T1879">mʼaŋŋaʔbə</ts>
                  <nts id="Seg_465" n="HIAT:ip">.</nts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1885" id="Seg_468" n="HIAT:u" s="T1880">
                  <ts e="T1881" id="Seg_470" n="HIAT:w" s="T1880">A</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1882" id="Seg_473" n="HIAT:w" s="T1881">dĭgəttə</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1883" id="Seg_476" n="HIAT:w" s="T1882">multuksiʔ</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1884" id="Seg_479" n="HIAT:w" s="T1883">tʼitluʔpi</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_481" n="HIAT:ip">(</nts>
                  <ts e="T1885" id="Seg_483" n="HIAT:w" s="T1884">m-</ts>
                  <nts id="Seg_484" n="HIAT:ip">)</nts>
                  <nts id="Seg_485" n="HIAT:ip">.</nts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1890" id="Seg_488" n="HIAT:u" s="T1885">
                  <ts e="T1886" id="Seg_490" n="HIAT:w" s="T1885">Tüj</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1887" id="Seg_493" n="HIAT:w" s="T1886">da</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1888" id="Seg_496" n="HIAT:w" s="T1887">ĭzemneʔbə</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1889" id="Seg_499" n="HIAT:w" s="T1888">bar</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1890" id="Seg_502" n="HIAT:w" s="T1889">ujam</ts>
                  <nts id="Seg_503" n="HIAT:ip">"</nts>
                  <nts id="Seg_504" n="HIAT:ip">.</nts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1891" id="Seg_507" n="HIAT:u" s="T1890">
                  <ts e="T1891" id="Seg_509" n="HIAT:w" s="T1890">Kabarləj</ts>
                  <nts id="Seg_510" n="HIAT:ip">.</nts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1896" id="Seg_513" n="HIAT:u" s="T1891">
                  <ts e="T1892" id="Seg_515" n="HIAT:w" s="T1891">Dĭgəttə</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1893" id="Seg_518" n="HIAT:w" s="T1892">kamar</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1894" id="Seg_521" n="HIAT:w" s="T1893">bar</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1895" id="Seg_524" n="HIAT:w" s="T1894">kakənarbi</ts>
                  <nts id="Seg_525" n="HIAT:ip">,</nts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1896" id="Seg_528" n="HIAT:w" s="T1895">kakənarbi</ts>
                  <nts id="Seg_529" n="HIAT:ip">.</nts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1897" id="Seg_532" n="HIAT:u" s="T1896">
                  <nts id="Seg_533" n="HIAT:ip">(</nts>
                  <nts id="Seg_534" n="HIAT:ip">(</nts>
                  <ats e="T0" id="Seg_535" n="HIAT:non-pho" s="T1896">…</ats>
                  <nts id="Seg_536" n="HIAT:ip">)</nts>
                  <nts id="Seg_537" n="HIAT:ip">)</nts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1897" id="Seg_540" n="HIAT:w" s="T0">külaːmbi</ts>
                  <nts id="Seg_541" n="HIAT:ip">.</nts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T1897" id="Seg_543" n="sc" s="T1765">
               <ts e="T1766" id="Seg_545" n="e" s="T1765">Urgaːba </ts>
               <ts e="T1767" id="Seg_547" n="e" s="T1766">i </ts>
               <ts e="T1768" id="Seg_549" n="e" s="T1767">kamar </ts>
               <ts e="T1769" id="Seg_551" n="e" s="T1768">dʼăbaktərlaʔbə. </ts>
               <ts e="T1770" id="Seg_553" n="e" s="T1769">Urgaːba </ts>
               <ts e="T1771" id="Seg_555" n="e" s="T1770">surarlaʔbə: </ts>
               <ts e="T1772" id="Seg_557" n="e" s="T1771">"Šindən </ts>
               <ts e="T1773" id="Seg_559" n="e" s="T1772">(kemdə) </ts>
               <ts e="T1774" id="Seg_561" n="e" s="T1773">nʼamga?" </ts>
               <ts e="T1775" id="Seg_563" n="e" s="T1774">A </ts>
               <ts e="T1776" id="Seg_565" n="e" s="T1775">kamar </ts>
               <ts e="T1777" id="Seg_567" n="e" s="T1776">măndə:" </ts>
               <ts e="T1778" id="Seg_569" n="e" s="T1777">Kuzan </ts>
               <ts e="T1779" id="Seg_571" n="e" s="T1778">kemdə </ts>
               <ts e="T1780" id="Seg_573" n="e" s="T1779">ugaːndə </ts>
               <ts e="T1781" id="Seg_575" n="e" s="T1780">nʼamga". </ts>
               <ts e="T1782" id="Seg_577" n="e" s="T1781">Dĭ </ts>
               <ts e="T1783" id="Seg_579" n="e" s="T1782">kandəga. </ts>
               <ts e="T1784" id="Seg_581" n="e" s="T1783">Šonəga </ts>
               <ts e="T1785" id="Seg_583" n="e" s="T1784">üdʼüge </ts>
               <ts e="T1786" id="Seg_585" n="e" s="T1785">nʼi. </ts>
               <ts e="T1787" id="Seg_587" n="e" s="T1786">"Tăn </ts>
               <ts e="T1788" id="Seg_589" n="e" s="T1787">kuza?" </ts>
               <ts e="T1789" id="Seg_591" n="e" s="T1788">"Dʼok, </ts>
               <ts e="T1790" id="Seg_593" n="e" s="T1789">măn </ts>
               <ts e="T1791" id="Seg_595" n="e" s="T1790">ej </ts>
               <ts e="T1792" id="Seg_597" n="e" s="T1791">kuza </ts>
               <ts e="T1793" id="Seg_599" n="e" s="T1792">iššo. </ts>
               <ts e="T1794" id="Seg_601" n="e" s="T1793">Kamən-nʼibudʼ </ts>
               <ts e="T1795" id="Seg_603" n="e" s="T1794">özerləm, </ts>
               <ts e="T1796" id="Seg_605" n="e" s="T1795">molam </ts>
               <ts e="T1797" id="Seg_607" n="e" s="T1796">kuza". </ts>
               <ts e="T1798" id="Seg_609" n="e" s="T1797">Dĭ </ts>
               <ts e="T1799" id="Seg_611" n="e" s="T1798">(baruʔluʔpi) </ts>
               <ts e="T1800" id="Seg_613" n="e" s="T1799">i </ts>
               <ts e="T1801" id="Seg_615" n="e" s="T1800">kandəga. </ts>
               <ts e="T1802" id="Seg_617" n="e" s="T1801">Dĭgəttə </ts>
               <ts e="T1803" id="Seg_619" n="e" s="T1802">šonəga </ts>
               <ts e="T1804" id="Seg_621" n="e" s="T1803">büzʼe. </ts>
               <ts e="T1805" id="Seg_623" n="e" s="T1804">"Tăn </ts>
               <ts e="T1806" id="Seg_625" n="e" s="T1805">kuza?" </ts>
               <ts e="T1807" id="Seg_627" n="e" s="T1806">"Da </ts>
               <ts e="T1808" id="Seg_629" n="e" s="T1807">ibiem </ts>
               <ts e="T1809" id="Seg_631" n="e" s="T1808">kuza, </ts>
               <ts e="T1810" id="Seg_633" n="e" s="T1809">a </ts>
               <ts e="T1811" id="Seg_635" n="e" s="T1810">tüj </ts>
               <ts e="T1812" id="Seg_637" n="e" s="T1811">ej </ts>
               <ts e="T1813" id="Seg_639" n="e" s="T1812">kuza". </ts>
               <ts e="T1814" id="Seg_641" n="e" s="T1813">Dĭgəttə </ts>
               <ts e="T1815" id="Seg_643" n="e" s="T1814">bazoʔ </ts>
               <ts e="T1816" id="Seg_645" n="e" s="T1815">kandəga, </ts>
               <ts e="T1817" id="Seg_647" n="e" s="T1816">baruʔluʔpi. </ts>
               <ts e="T1818" id="Seg_649" n="e" s="T1817">Šonəga </ts>
               <ts e="T1819" id="Seg_651" n="e" s="T1818">onʼiʔ </ts>
               <ts e="T1820" id="Seg_653" n="e" s="T1819">kuza. </ts>
               <ts e="T1821" id="Seg_655" n="e" s="T1820">"Tăn </ts>
               <ts e="T1822" id="Seg_657" n="e" s="T1821">kuza </ts>
               <ts e="T1823" id="Seg_659" n="e" s="T1822">(măn- </ts>
               <ts e="T1824" id="Seg_661" n="e" s="T1823">măn-)?" </ts>
               <ts e="T1825" id="Seg_663" n="e" s="T1824">Dĭ </ts>
               <ts e="T1826" id="Seg_665" n="e" s="T1825">măndə:" </ts>
               <ts e="T1827" id="Seg_667" n="e" s="T1826">Kuza". </ts>
               <ts e="T1828" id="Seg_669" n="e" s="T1827">Dĭgəttə </ts>
               <ts e="T1829" id="Seg_671" n="e" s="T1828">dĭ </ts>
               <ts e="T1830" id="Seg_673" n="e" s="T1829">kandəga, </ts>
               <ts e="T1831" id="Seg_675" n="e" s="T1830">(a </ts>
               <ts e="T1832" id="Seg_677" n="e" s="T1831">dĭ=) </ts>
               <ts e="T1833" id="Seg_679" n="e" s="T1832">(dĭ= </ts>
               <ts e="T1834" id="Seg_681" n="e" s="T1833">dĭʔnə=) </ts>
               <ts e="T1835" id="Seg_683" n="e" s="T1834">diʔgəʔ </ts>
               <ts e="T1836" id="Seg_685" n="e" s="T1835">ej </ts>
               <ts e="T1837" id="Seg_687" n="e" s="T1836">malia. </ts>
               <ts e="T1838" id="Seg_689" n="e" s="T1837">Tože </ts>
               <ts e="T1839" id="Seg_691" n="e" s="T1838">šolia. </ts>
               <ts e="T1840" id="Seg_693" n="e" s="T1839">(Dĭ=) </ts>
               <ts e="T1841" id="Seg_695" n="e" s="T1840">(Dĭ) </ts>
               <ts e="T1842" id="Seg_697" n="e" s="T1841">ibi </ts>
               <ts e="T1843" id="Seg_699" n="e" s="T1842">sablʼabə </ts>
               <ts e="T1844" id="Seg_701" n="e" s="T1843">i </ts>
               <ts e="T1845" id="Seg_703" n="e" s="T1844">dĭm </ts>
               <ts e="T1846" id="Seg_705" n="e" s="T1845">toʔnarluʔpi. </ts>
               <ts e="T1847" id="Seg_707" n="e" s="T1846">Dĭn </ts>
               <ts e="T1848" id="Seg_709" n="e" s="T1847">bar </ts>
               <ts e="T1849" id="Seg_711" n="e" s="T1848">kemdə </ts>
               <ts e="T1850" id="Seg_713" n="e" s="T1849">mʼaŋŋaʔbə. </ts>
               <ts e="T1851" id="Seg_715" n="e" s="T1850">Dĭ </ts>
               <ts e="T1852" id="Seg_717" n="e" s="T1851">bar </ts>
               <ts e="T1853" id="Seg_719" n="e" s="T1852">nuʔməluʔpi. </ts>
               <ts e="T1854" id="Seg_721" n="e" s="T1853">A </ts>
               <ts e="T1855" id="Seg_723" n="e" s="T1854">dĭ </ts>
               <ts e="T1856" id="Seg_725" n="e" s="T1855">(multus-) </ts>
               <ts e="T1857" id="Seg_727" n="e" s="T1856">multuksiʔ </ts>
               <ts e="T1858" id="Seg_729" n="e" s="T1857">tʼitluʔpi </ts>
               <ts e="T1859" id="Seg_731" n="e" s="T1858">dĭ. </ts>
               <ts e="T1860" id="Seg_733" n="e" s="T1859">Dĭ </ts>
               <ts e="T1861" id="Seg_735" n="e" s="T1860">bar </ts>
               <ts e="T1862" id="Seg_737" n="e" s="T1861">nuʔməluʔpi. </ts>
               <ts e="T1863" id="Seg_739" n="e" s="T1862">Dĭgəttə </ts>
               <ts e="T1864" id="Seg_741" n="e" s="T1863">šobi. </ts>
               <ts e="T1865" id="Seg_743" n="e" s="T1864">"Tăn </ts>
               <ts e="T1866" id="Seg_745" n="e" s="T1865">măna </ts>
               <ts e="T1867" id="Seg_747" n="e" s="T1866">măndəl, </ts>
               <ts e="T1868" id="Seg_749" n="e" s="T1867">nʼamga </ts>
               <ts e="T1869" id="Seg_751" n="e" s="T1868">kem </ts>
               <ts e="T1870" id="Seg_753" n="e" s="T1869">kuzan. </ts>
               <ts e="T1871" id="Seg_755" n="e" s="T1870">Dĭn </ts>
               <ts e="T1872" id="Seg_757" n="e" s="T1871">ugaːndə </ts>
               <ts e="T1873" id="Seg_759" n="e" s="T1872">šĭkət </ts>
               <ts e="T1874" id="Seg_761" n="e" s="T1873">numo. </ts>
               <ts e="T1875" id="Seg_763" n="e" s="T1874">Šĭketsiʔ </ts>
               <ts e="T1876" id="Seg_765" n="e" s="T1875">măna </ts>
               <ts e="T1877" id="Seg_767" n="e" s="T1876">toʔnarbi </ts>
               <ts e="T1878" id="Seg_769" n="e" s="T1877">bar, </ts>
               <ts e="T1879" id="Seg_771" n="e" s="T1878">kem </ts>
               <ts e="T1880" id="Seg_773" n="e" s="T1879">mʼaŋŋaʔbə. </ts>
               <ts e="T1881" id="Seg_775" n="e" s="T1880">A </ts>
               <ts e="T1882" id="Seg_777" n="e" s="T1881">dĭgəttə </ts>
               <ts e="T1883" id="Seg_779" n="e" s="T1882">multuksiʔ </ts>
               <ts e="T1884" id="Seg_781" n="e" s="T1883">tʼitluʔpi </ts>
               <ts e="T1885" id="Seg_783" n="e" s="T1884">(m-). </ts>
               <ts e="T1886" id="Seg_785" n="e" s="T1885">Tüj </ts>
               <ts e="T1887" id="Seg_787" n="e" s="T1886">da </ts>
               <ts e="T1888" id="Seg_789" n="e" s="T1887">ĭzemneʔbə </ts>
               <ts e="T1889" id="Seg_791" n="e" s="T1888">bar </ts>
               <ts e="T1890" id="Seg_793" n="e" s="T1889">ujam". </ts>
               <ts e="T1891" id="Seg_795" n="e" s="T1890">Kabarləj. </ts>
               <ts e="T1892" id="Seg_797" n="e" s="T1891">Dĭgəttə </ts>
               <ts e="T1893" id="Seg_799" n="e" s="T1892">kamar </ts>
               <ts e="T1894" id="Seg_801" n="e" s="T1893">bar </ts>
               <ts e="T1895" id="Seg_803" n="e" s="T1894">kakənarbi, </ts>
               <ts e="T1896" id="Seg_805" n="e" s="T1895">kakənarbi. </ts>
               <ts e="T0" id="Seg_807" n="e" s="T1896">((…)) </ts>
               <ts e="T1897" id="Seg_809" n="e" s="T0">külaːmbi. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T1769" id="Seg_810" s="T1765">PKZ_196X_MosquitoAndBear_flk.001 (001)</ta>
            <ta e="T1774" id="Seg_811" s="T1769">PKZ_196X_MosquitoAndBear_flk.002 (002) </ta>
            <ta e="T1781" id="Seg_812" s="T1774">PKZ_196X_MosquitoAndBear_flk.003 (004)</ta>
            <ta e="T1783" id="Seg_813" s="T1781">PKZ_196X_MosquitoAndBear_flk.004 (005)</ta>
            <ta e="T1786" id="Seg_814" s="T1783">PKZ_196X_MosquitoAndBear_flk.005 (006)</ta>
            <ta e="T1788" id="Seg_815" s="T1786">PKZ_196X_MosquitoAndBear_flk.006 (007)</ta>
            <ta e="T1793" id="Seg_816" s="T1788">PKZ_196X_MosquitoAndBear_flk.007 (008)</ta>
            <ta e="T1797" id="Seg_817" s="T1793">PKZ_196X_MosquitoAndBear_flk.008 (009)</ta>
            <ta e="T1801" id="Seg_818" s="T1797">PKZ_196X_MosquitoAndBear_flk.009 (010)</ta>
            <ta e="T1804" id="Seg_819" s="T1801">PKZ_196X_MosquitoAndBear_flk.010 (011)</ta>
            <ta e="T1806" id="Seg_820" s="T1804">PKZ_196X_MosquitoAndBear_flk.011 (012)</ta>
            <ta e="T1813" id="Seg_821" s="T1806">PKZ_196X_MosquitoAndBear_flk.012 (013)</ta>
            <ta e="T1817" id="Seg_822" s="T1813">PKZ_196X_MosquitoAndBear_flk.013 (014)</ta>
            <ta e="T1820" id="Seg_823" s="T1817">PKZ_196X_MosquitoAndBear_flk.014 (015)</ta>
            <ta e="T1824" id="Seg_824" s="T1820">PKZ_196X_MosquitoAndBear_flk.015 (016)</ta>
            <ta e="T1827" id="Seg_825" s="T1824">PKZ_196X_MosquitoAndBear_flk.016 (017)</ta>
            <ta e="T1837" id="Seg_826" s="T1827">PKZ_196X_MosquitoAndBear_flk.017 (018)</ta>
            <ta e="T1839" id="Seg_827" s="T1837">PKZ_196X_MosquitoAndBear_flk.018 (019)</ta>
            <ta e="T1846" id="Seg_828" s="T1839">PKZ_196X_MosquitoAndBear_flk.019 (020)</ta>
            <ta e="T1850" id="Seg_829" s="T1846">PKZ_196X_MosquitoAndBear_flk.020 (021)</ta>
            <ta e="T1853" id="Seg_830" s="T1850">PKZ_196X_MosquitoAndBear_flk.021 (022)</ta>
            <ta e="T1859" id="Seg_831" s="T1853">PKZ_196X_MosquitoAndBear_flk.022 (023)</ta>
            <ta e="T1862" id="Seg_832" s="T1859">PKZ_196X_MosquitoAndBear_flk.023 (024)</ta>
            <ta e="T1864" id="Seg_833" s="T1862">PKZ_196X_MosquitoAndBear_flk.024 (025)</ta>
            <ta e="T1870" id="Seg_834" s="T1864">PKZ_196X_MosquitoAndBear_flk.025 (026)</ta>
            <ta e="T1874" id="Seg_835" s="T1870">PKZ_196X_MosquitoAndBear_flk.026 (027)</ta>
            <ta e="T1880" id="Seg_836" s="T1874">PKZ_196X_MosquitoAndBear_flk.027 (028)</ta>
            <ta e="T1885" id="Seg_837" s="T1880">PKZ_196X_MosquitoAndBear_flk.028 (029)</ta>
            <ta e="T1890" id="Seg_838" s="T1885">PKZ_196X_MosquitoAndBear_flk.029 (030)</ta>
            <ta e="T1891" id="Seg_839" s="T1890">PKZ_196X_MosquitoAndBear_flk.030 (031)</ta>
            <ta e="T1896" id="Seg_840" s="T1891">PKZ_196X_MosquitoAndBear_flk.031 (032)</ta>
            <ta e="T1897" id="Seg_841" s="T1896">PKZ_196X_MosquitoAndBear_flk.032 (033)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T1769" id="Seg_842" s="T1765">Urgaːba i kamar dʼăbaktərlaʔbə. </ta>
            <ta e="T1774" id="Seg_843" s="T1769">Urgaːba surarlaʔbə: "Šindən (kemdə) nʼamga?" </ta>
            <ta e="T1781" id="Seg_844" s="T1774">A kamar măndə:" Kuzan kemdə ugaːndə nʼamga". </ta>
            <ta e="T1783" id="Seg_845" s="T1781">Dĭ kandəga. </ta>
            <ta e="T1786" id="Seg_846" s="T1783">Šonəga üdʼüge nʼi. </ta>
            <ta e="T1788" id="Seg_847" s="T1786">"Tăn kuza?" </ta>
            <ta e="T1793" id="Seg_848" s="T1788">"Dʼok, măn ej kuza iššo. </ta>
            <ta e="T1797" id="Seg_849" s="T1793">Kamən-nʼibudʼ özerləm, molam kuza". </ta>
            <ta e="T1801" id="Seg_850" s="T1797">Dĭ (baruʔluʔpi) i kandəga. </ta>
            <ta e="T1804" id="Seg_851" s="T1801">Dĭgəttə šonəga büzʼe. </ta>
            <ta e="T1806" id="Seg_852" s="T1804">"Tăn kuza?" </ta>
            <ta e="T1813" id="Seg_853" s="T1806">"Da ibiem kuza, a tüj ej kuza". </ta>
            <ta e="T1817" id="Seg_854" s="T1813">Dĭgəttə bazoʔ kandəga, baruʔluʔpi. </ta>
            <ta e="T1820" id="Seg_855" s="T1817">Šonəga onʼiʔ kuza. </ta>
            <ta e="T1824" id="Seg_856" s="T1820">"Tăn kuza (măn- măn-)?" </ta>
            <ta e="T1827" id="Seg_857" s="T1824">Dĭ măndə:" Kuza". </ta>
            <ta e="T1837" id="Seg_858" s="T1827">Dĭgəttə dĭ kandəga, (a dĭ=) (dĭ= dĭʔnə=) diʔgəʔ ej malia. </ta>
            <ta e="T1839" id="Seg_859" s="T1837">Tože šolia. </ta>
            <ta e="T1846" id="Seg_860" s="T1839">(Dĭ=) (Dĭ) ibi sablʼabə i dĭm toʔnarluʔpi. </ta>
            <ta e="T1850" id="Seg_861" s="T1846">Dĭn bar kemdə mʼaŋŋaʔbə. </ta>
            <ta e="T1853" id="Seg_862" s="T1850">Dĭ bar nuʔməluʔpi. </ta>
            <ta e="T1859" id="Seg_863" s="T1853">A dĭ (multus-) multuksiʔ tʼitluʔpi dĭ. </ta>
            <ta e="T1862" id="Seg_864" s="T1859">Dĭ bar nuʔməluʔpi. </ta>
            <ta e="T1864" id="Seg_865" s="T1862">Dĭgəttə šobi. </ta>
            <ta e="T1870" id="Seg_866" s="T1864">"Tăn măna măndəl, nʼamga kem kuzan. </ta>
            <ta e="T1874" id="Seg_867" s="T1870">Dĭn ugaːndə šĭkət numo. </ta>
            <ta e="T1880" id="Seg_868" s="T1874">Šĭketsiʔ măna toʔnarbi bar, kem mʼaŋŋaʔbə. </ta>
            <ta e="T1885" id="Seg_869" s="T1880">A dĭgəttə multuksiʔ tʼitluʔpi (m-). </ta>
            <ta e="T1890" id="Seg_870" s="T1885">Tüj da ĭzemneʔbə bar ujam". </ta>
            <ta e="T1891" id="Seg_871" s="T1890">Kabarləj. </ta>
            <ta e="T1896" id="Seg_872" s="T1891">Dĭgəttə kamar bar kakənarbi, kakənarbi. </ta>
            <ta e="T1897" id="Seg_873" s="T1896">((…)) külaːmbi. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1766" id="Seg_874" s="T1765">urgaːba</ta>
            <ta e="T1767" id="Seg_875" s="T1766">i</ta>
            <ta e="T1768" id="Seg_876" s="T1767">kamar</ta>
            <ta e="T1769" id="Seg_877" s="T1768">dʼăbaktər-laʔbə</ta>
            <ta e="T1770" id="Seg_878" s="T1769">urgaːba</ta>
            <ta e="T1771" id="Seg_879" s="T1770">surar-laʔbə</ta>
            <ta e="T1772" id="Seg_880" s="T1771">šində-n</ta>
            <ta e="T1773" id="Seg_881" s="T1772">kem-də</ta>
            <ta e="T1774" id="Seg_882" s="T1773">nʼamga</ta>
            <ta e="T1775" id="Seg_883" s="T1774">a</ta>
            <ta e="T1776" id="Seg_884" s="T1775">kamar</ta>
            <ta e="T1777" id="Seg_885" s="T1776">măn-də</ta>
            <ta e="T1778" id="Seg_886" s="T1777">kuza-n</ta>
            <ta e="T1779" id="Seg_887" s="T1778">kem-də</ta>
            <ta e="T1780" id="Seg_888" s="T1779">ugaːndə</ta>
            <ta e="T1781" id="Seg_889" s="T1780">nʼamga</ta>
            <ta e="T1782" id="Seg_890" s="T1781">dĭ</ta>
            <ta e="T1783" id="Seg_891" s="T1782">kandə-ga</ta>
            <ta e="T1784" id="Seg_892" s="T1783">šonə-ga</ta>
            <ta e="T1785" id="Seg_893" s="T1784">üdʼüge</ta>
            <ta e="T1786" id="Seg_894" s="T1785">nʼi</ta>
            <ta e="T1787" id="Seg_895" s="T1786">tăn</ta>
            <ta e="T1788" id="Seg_896" s="T1787">kuza</ta>
            <ta e="T1789" id="Seg_897" s="T1788">dʼok</ta>
            <ta e="T1790" id="Seg_898" s="T1789">măn</ta>
            <ta e="T1791" id="Seg_899" s="T1790">ej</ta>
            <ta e="T1792" id="Seg_900" s="T1791">kuza</ta>
            <ta e="T1793" id="Seg_901" s="T1792">iššo</ta>
            <ta e="T1794" id="Seg_902" s="T1793">kamən=nʼibudʼ</ta>
            <ta e="T1795" id="Seg_903" s="T1794">özer-lə-m</ta>
            <ta e="T1796" id="Seg_904" s="T1795">mo-la-m</ta>
            <ta e="T1797" id="Seg_905" s="T1796">kuza</ta>
            <ta e="T1798" id="Seg_906" s="T1797">dĭ</ta>
            <ta e="T1799" id="Seg_907" s="T1798">baruʔ-luʔ-pi</ta>
            <ta e="T1800" id="Seg_908" s="T1799">i</ta>
            <ta e="T1801" id="Seg_909" s="T1800">kandə-ga</ta>
            <ta e="T1802" id="Seg_910" s="T1801">dĭgəttə</ta>
            <ta e="T1803" id="Seg_911" s="T1802">šonə-ga</ta>
            <ta e="T1804" id="Seg_912" s="T1803">büzʼe</ta>
            <ta e="T1805" id="Seg_913" s="T1804">tăn</ta>
            <ta e="T1806" id="Seg_914" s="T1805">kuza</ta>
            <ta e="T1807" id="Seg_915" s="T1806">da</ta>
            <ta e="T1808" id="Seg_916" s="T1807">i-bie-m</ta>
            <ta e="T1809" id="Seg_917" s="T1808">kuza</ta>
            <ta e="T1810" id="Seg_918" s="T1809">a</ta>
            <ta e="T1811" id="Seg_919" s="T1810">tüj</ta>
            <ta e="T1812" id="Seg_920" s="T1811">ej</ta>
            <ta e="T1813" id="Seg_921" s="T1812">kuza</ta>
            <ta e="T1814" id="Seg_922" s="T1813">dĭgəttə</ta>
            <ta e="T1815" id="Seg_923" s="T1814">bazoʔ</ta>
            <ta e="T1816" id="Seg_924" s="T1815">kandə-ga</ta>
            <ta e="T1817" id="Seg_925" s="T1816">baruʔ-luʔ-pi</ta>
            <ta e="T1818" id="Seg_926" s="T1817">šonə-ga</ta>
            <ta e="T1819" id="Seg_927" s="T1818">onʼiʔ</ta>
            <ta e="T1820" id="Seg_928" s="T1819">kuza</ta>
            <ta e="T1821" id="Seg_929" s="T1820">tăn</ta>
            <ta e="T1822" id="Seg_930" s="T1821">kuza</ta>
            <ta e="T1825" id="Seg_931" s="T1824">dĭ</ta>
            <ta e="T1826" id="Seg_932" s="T1825">măn-də</ta>
            <ta e="T1827" id="Seg_933" s="T1826">kuza</ta>
            <ta e="T1828" id="Seg_934" s="T1827">dĭgəttə</ta>
            <ta e="T1829" id="Seg_935" s="T1828">dĭ</ta>
            <ta e="T1830" id="Seg_936" s="T1829">kandə-ga</ta>
            <ta e="T1831" id="Seg_937" s="T1830">a</ta>
            <ta e="T1832" id="Seg_938" s="T1831">dĭ</ta>
            <ta e="T1833" id="Seg_939" s="T1832">dĭ</ta>
            <ta e="T1834" id="Seg_940" s="T1833">dĭʔ-nə</ta>
            <ta e="T1835" id="Seg_941" s="T1834">diʔ-gəʔ</ta>
            <ta e="T1836" id="Seg_942" s="T1835">ej</ta>
            <ta e="T1837" id="Seg_943" s="T1836">ma-lia</ta>
            <ta e="T1838" id="Seg_944" s="T1837">tože</ta>
            <ta e="T1839" id="Seg_945" s="T1838">šo-lia</ta>
            <ta e="T1840" id="Seg_946" s="T1839">dĭ</ta>
            <ta e="T1841" id="Seg_947" s="T1840">dĭ</ta>
            <ta e="T1842" id="Seg_948" s="T1841">i-bi</ta>
            <ta e="T1843" id="Seg_949" s="T1842">sablʼa-bə</ta>
            <ta e="T1844" id="Seg_950" s="T1843">i</ta>
            <ta e="T1845" id="Seg_951" s="T1844">dĭ-m</ta>
            <ta e="T1846" id="Seg_952" s="T1845">toʔ-nar-luʔ-pi</ta>
            <ta e="T1847" id="Seg_953" s="T1846">dĭ-n</ta>
            <ta e="T1848" id="Seg_954" s="T1847">bar</ta>
            <ta e="T1849" id="Seg_955" s="T1848">kem-də</ta>
            <ta e="T1850" id="Seg_956" s="T1849">mʼaŋ-ŋaʔbə</ta>
            <ta e="T1851" id="Seg_957" s="T1850">dĭ</ta>
            <ta e="T1852" id="Seg_958" s="T1851">bar</ta>
            <ta e="T1853" id="Seg_959" s="T1852">nuʔmə-luʔ-pi</ta>
            <ta e="T1854" id="Seg_960" s="T1853">a</ta>
            <ta e="T1855" id="Seg_961" s="T1854">dĭ</ta>
            <ta e="T1857" id="Seg_962" s="T1856">multuk-siʔ</ta>
            <ta e="T1858" id="Seg_963" s="T1857">tʼit-luʔ-pi</ta>
            <ta e="T1859" id="Seg_964" s="T1858">dĭ</ta>
            <ta e="T1860" id="Seg_965" s="T1859">dĭ</ta>
            <ta e="T1861" id="Seg_966" s="T1860">bar</ta>
            <ta e="T1862" id="Seg_967" s="T1861">nuʔmə-luʔ-pi</ta>
            <ta e="T1863" id="Seg_968" s="T1862">dĭgəttə</ta>
            <ta e="T1864" id="Seg_969" s="T1863">šo-bi</ta>
            <ta e="T1865" id="Seg_970" s="T1864">tăn</ta>
            <ta e="T1866" id="Seg_971" s="T1865">măna</ta>
            <ta e="T1867" id="Seg_972" s="T1866">măn-də-l</ta>
            <ta e="T1868" id="Seg_973" s="T1867">nʼamga</ta>
            <ta e="T1869" id="Seg_974" s="T1868">kem</ta>
            <ta e="T1870" id="Seg_975" s="T1869">kuza-n</ta>
            <ta e="T1871" id="Seg_976" s="T1870">dĭ-n</ta>
            <ta e="T1872" id="Seg_977" s="T1871">ugaːndə</ta>
            <ta e="T1873" id="Seg_978" s="T1872">šĭkə-t</ta>
            <ta e="T1874" id="Seg_979" s="T1873">numo</ta>
            <ta e="T1875" id="Seg_980" s="T1874">šĭke-t-siʔ</ta>
            <ta e="T1876" id="Seg_981" s="T1875">măna</ta>
            <ta e="T1877" id="Seg_982" s="T1876">toʔ-nar-bi</ta>
            <ta e="T1878" id="Seg_983" s="T1877">bar</ta>
            <ta e="T1879" id="Seg_984" s="T1878">kem</ta>
            <ta e="T1880" id="Seg_985" s="T1879">mʼaŋ-ŋaʔbə</ta>
            <ta e="T1881" id="Seg_986" s="T1880">a</ta>
            <ta e="T1882" id="Seg_987" s="T1881">dĭgəttə</ta>
            <ta e="T1883" id="Seg_988" s="T1882">multuk-siʔ</ta>
            <ta e="T1884" id="Seg_989" s="T1883">tʼit-luʔ-pi</ta>
            <ta e="T1886" id="Seg_990" s="T1885">tüj</ta>
            <ta e="T1887" id="Seg_991" s="T1886">da</ta>
            <ta e="T1888" id="Seg_992" s="T1887">ĭzem-neʔbə</ta>
            <ta e="T1889" id="Seg_993" s="T1888">bar</ta>
            <ta e="T1890" id="Seg_994" s="T1889">uja-m</ta>
            <ta e="T1891" id="Seg_995" s="T1890">kabarləj</ta>
            <ta e="T1892" id="Seg_996" s="T1891">dĭgəttə</ta>
            <ta e="T1893" id="Seg_997" s="T1892">kamar</ta>
            <ta e="T1894" id="Seg_998" s="T1893">bar</ta>
            <ta e="T1895" id="Seg_999" s="T1894">kakənar-bi</ta>
            <ta e="T1896" id="Seg_1000" s="T1895">kakənar-bi</ta>
            <ta e="T1897" id="Seg_1001" s="T0">kü-laːm-bi</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1766" id="Seg_1002" s="T1765">urgaːba</ta>
            <ta e="T1767" id="Seg_1003" s="T1766">i</ta>
            <ta e="T1768" id="Seg_1004" s="T1767">kamar</ta>
            <ta e="T1769" id="Seg_1005" s="T1768">tʼăbaktər-laʔbə</ta>
            <ta e="T1770" id="Seg_1006" s="T1769">urgaːba</ta>
            <ta e="T1771" id="Seg_1007" s="T1770">surar-laʔbə</ta>
            <ta e="T1772" id="Seg_1008" s="T1771">šində-n</ta>
            <ta e="T1773" id="Seg_1009" s="T1772">kem-də</ta>
            <ta e="T1774" id="Seg_1010" s="T1773">nʼamga</ta>
            <ta e="T1775" id="Seg_1011" s="T1774">a</ta>
            <ta e="T1776" id="Seg_1012" s="T1775">kamar</ta>
            <ta e="T1777" id="Seg_1013" s="T1776">măn-ntə</ta>
            <ta e="T1778" id="Seg_1014" s="T1777">kuza-n</ta>
            <ta e="T1779" id="Seg_1015" s="T1778">kem-də</ta>
            <ta e="T1780" id="Seg_1016" s="T1779">ugaːndə</ta>
            <ta e="T1781" id="Seg_1017" s="T1780">nʼamga</ta>
            <ta e="T1782" id="Seg_1018" s="T1781">dĭ</ta>
            <ta e="T1783" id="Seg_1019" s="T1782">kandə-gA</ta>
            <ta e="T1784" id="Seg_1020" s="T1783">šonə-gA</ta>
            <ta e="T1785" id="Seg_1021" s="T1784">üdʼüge</ta>
            <ta e="T1786" id="Seg_1022" s="T1785">nʼi</ta>
            <ta e="T1787" id="Seg_1023" s="T1786">tăn</ta>
            <ta e="T1788" id="Seg_1024" s="T1787">kuza</ta>
            <ta e="T1789" id="Seg_1025" s="T1788">dʼok</ta>
            <ta e="T1790" id="Seg_1026" s="T1789">măn</ta>
            <ta e="T1791" id="Seg_1027" s="T1790">ej</ta>
            <ta e="T1792" id="Seg_1028" s="T1791">kuza</ta>
            <ta e="T1793" id="Seg_1029" s="T1792">ĭššo</ta>
            <ta e="T1794" id="Seg_1030" s="T1793">kamən=nʼibudʼ</ta>
            <ta e="T1795" id="Seg_1031" s="T1794">özer-lV-m</ta>
            <ta e="T1796" id="Seg_1032" s="T1795">mo-lV-m</ta>
            <ta e="T1797" id="Seg_1033" s="T1796">kuza</ta>
            <ta e="T1798" id="Seg_1034" s="T1797">dĭ</ta>
            <ta e="T1799" id="Seg_1035" s="T1798">barəʔ-luʔbdə-bi</ta>
            <ta e="T1800" id="Seg_1036" s="T1799">i</ta>
            <ta e="T1801" id="Seg_1037" s="T1800">kandə-gA</ta>
            <ta e="T1802" id="Seg_1038" s="T1801">dĭgəttə</ta>
            <ta e="T1803" id="Seg_1039" s="T1802">šonə-gA</ta>
            <ta e="T1804" id="Seg_1040" s="T1803">büzʼe</ta>
            <ta e="T1805" id="Seg_1041" s="T1804">tăn</ta>
            <ta e="T1806" id="Seg_1042" s="T1805">kuza</ta>
            <ta e="T1807" id="Seg_1043" s="T1806">da</ta>
            <ta e="T1808" id="Seg_1044" s="T1807">i-bi-m</ta>
            <ta e="T1809" id="Seg_1045" s="T1808">kuza</ta>
            <ta e="T1810" id="Seg_1046" s="T1809">a</ta>
            <ta e="T1811" id="Seg_1047" s="T1810">tüj</ta>
            <ta e="T1812" id="Seg_1048" s="T1811">ej</ta>
            <ta e="T1813" id="Seg_1049" s="T1812">kuza</ta>
            <ta e="T1814" id="Seg_1050" s="T1813">dĭgəttə</ta>
            <ta e="T1815" id="Seg_1051" s="T1814">bazoʔ</ta>
            <ta e="T1816" id="Seg_1052" s="T1815">kandə-gA</ta>
            <ta e="T1817" id="Seg_1053" s="T1816">barəʔ-luʔbdə-bi</ta>
            <ta e="T1818" id="Seg_1054" s="T1817">šonə-gA</ta>
            <ta e="T1819" id="Seg_1055" s="T1818">onʼiʔ</ta>
            <ta e="T1820" id="Seg_1056" s="T1819">kuza</ta>
            <ta e="T1821" id="Seg_1057" s="T1820">tăn</ta>
            <ta e="T1822" id="Seg_1058" s="T1821">kuza</ta>
            <ta e="T1825" id="Seg_1059" s="T1824">dĭ</ta>
            <ta e="T1826" id="Seg_1060" s="T1825">măn-ntə</ta>
            <ta e="T1827" id="Seg_1061" s="T1826">kuza</ta>
            <ta e="T1828" id="Seg_1062" s="T1827">dĭgəttə</ta>
            <ta e="T1829" id="Seg_1063" s="T1828">dĭ</ta>
            <ta e="T1830" id="Seg_1064" s="T1829">kandə-gA</ta>
            <ta e="T1831" id="Seg_1065" s="T1830">a</ta>
            <ta e="T1832" id="Seg_1066" s="T1831">dĭ</ta>
            <ta e="T1833" id="Seg_1067" s="T1832">dĭ</ta>
            <ta e="T1834" id="Seg_1068" s="T1833">dĭ-Tə</ta>
            <ta e="T1835" id="Seg_1069" s="T1834">dĭ-gəʔ</ta>
            <ta e="T1836" id="Seg_1070" s="T1835">ej</ta>
            <ta e="T1837" id="Seg_1071" s="T1836">ma-liA</ta>
            <ta e="T1838" id="Seg_1072" s="T1837">tože</ta>
            <ta e="T1839" id="Seg_1073" s="T1838">šo-liA</ta>
            <ta e="T1840" id="Seg_1074" s="T1839">dĭ</ta>
            <ta e="T1841" id="Seg_1075" s="T1840">dĭ</ta>
            <ta e="T1842" id="Seg_1076" s="T1841">i-bi</ta>
            <ta e="T1843" id="Seg_1077" s="T1842">sablʼa-bə</ta>
            <ta e="T1844" id="Seg_1078" s="T1843">i</ta>
            <ta e="T1845" id="Seg_1079" s="T1844">dĭ-m</ta>
            <ta e="T1846" id="Seg_1080" s="T1845">toʔbdə-nar-luʔbdə-bi</ta>
            <ta e="T1847" id="Seg_1081" s="T1846">dĭ-n</ta>
            <ta e="T1848" id="Seg_1082" s="T1847">bar</ta>
            <ta e="T1849" id="Seg_1083" s="T1848">kem-də</ta>
            <ta e="T1850" id="Seg_1084" s="T1849">mʼaŋ-laʔbə</ta>
            <ta e="T1851" id="Seg_1085" s="T1850">dĭ</ta>
            <ta e="T1852" id="Seg_1086" s="T1851">bar</ta>
            <ta e="T1853" id="Seg_1087" s="T1852">nuʔmə-luʔbdə-bi</ta>
            <ta e="T1854" id="Seg_1088" s="T1853">a</ta>
            <ta e="T1855" id="Seg_1089" s="T1854">dĭ</ta>
            <ta e="T1857" id="Seg_1090" s="T1856">multuk-ziʔ</ta>
            <ta e="T1858" id="Seg_1091" s="T1857">tʼit-luʔbdə-bi</ta>
            <ta e="T1859" id="Seg_1092" s="T1858">dĭ</ta>
            <ta e="T1860" id="Seg_1093" s="T1859">dĭ</ta>
            <ta e="T1861" id="Seg_1094" s="T1860">bar</ta>
            <ta e="T1862" id="Seg_1095" s="T1861">nuʔmə-luʔbdə-bi</ta>
            <ta e="T1863" id="Seg_1096" s="T1862">dĭgəttə</ta>
            <ta e="T1864" id="Seg_1097" s="T1863">šo-bi</ta>
            <ta e="T1865" id="Seg_1098" s="T1864">tăn</ta>
            <ta e="T1866" id="Seg_1099" s="T1865">măna</ta>
            <ta e="T1867" id="Seg_1100" s="T1866">măn-ntə-l</ta>
            <ta e="T1868" id="Seg_1101" s="T1867">nʼamga</ta>
            <ta e="T1869" id="Seg_1102" s="T1868">kem</ta>
            <ta e="T1870" id="Seg_1103" s="T1869">kuza-n</ta>
            <ta e="T1871" id="Seg_1104" s="T1870">dĭ-n</ta>
            <ta e="T1872" id="Seg_1105" s="T1871">ugaːndə</ta>
            <ta e="T1873" id="Seg_1106" s="T1872">šĭkə-t</ta>
            <ta e="T1874" id="Seg_1107" s="T1873">numo</ta>
            <ta e="T1875" id="Seg_1108" s="T1874">šĭkə-t-ziʔ</ta>
            <ta e="T1876" id="Seg_1109" s="T1875">măna</ta>
            <ta e="T1877" id="Seg_1110" s="T1876">toʔbdə-nar-bi</ta>
            <ta e="T1878" id="Seg_1111" s="T1877">bar</ta>
            <ta e="T1879" id="Seg_1112" s="T1878">kem</ta>
            <ta e="T1880" id="Seg_1113" s="T1879">mʼaŋ-laʔbə</ta>
            <ta e="T1881" id="Seg_1114" s="T1880">a</ta>
            <ta e="T1882" id="Seg_1115" s="T1881">dĭgəttə</ta>
            <ta e="T1883" id="Seg_1116" s="T1882">multuk-ziʔ</ta>
            <ta e="T1884" id="Seg_1117" s="T1883">tʼit-luʔbdə-bi</ta>
            <ta e="T1886" id="Seg_1118" s="T1885">tüj</ta>
            <ta e="T1887" id="Seg_1119" s="T1886">da</ta>
            <ta e="T1888" id="Seg_1120" s="T1887">ĭzem-laʔbə</ta>
            <ta e="T1889" id="Seg_1121" s="T1888">bar</ta>
            <ta e="T1890" id="Seg_1122" s="T1889">uja-m</ta>
            <ta e="T1891" id="Seg_1123" s="T1890">kabarləj</ta>
            <ta e="T1892" id="Seg_1124" s="T1891">dĭgəttə</ta>
            <ta e="T1893" id="Seg_1125" s="T1892">kamar</ta>
            <ta e="T1894" id="Seg_1126" s="T1893">bar</ta>
            <ta e="T1895" id="Seg_1127" s="T1894">kakənar-bi</ta>
            <ta e="T1896" id="Seg_1128" s="T1895">kakənar-bi</ta>
            <ta e="T1897" id="Seg_1129" s="T0">kü-laːm-bi</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1766" id="Seg_1130" s="T1765">bear.[NOM.SG]</ta>
            <ta e="T1767" id="Seg_1131" s="T1766">and</ta>
            <ta e="T1768" id="Seg_1132" s="T1767">mosquito.[NOM.SG]</ta>
            <ta e="T1769" id="Seg_1133" s="T1768">speak-DUR.[3SG]</ta>
            <ta e="T1770" id="Seg_1134" s="T1769">bear.[NOM.SG]</ta>
            <ta e="T1771" id="Seg_1135" s="T1770">ask-DUR.[3SG]</ta>
            <ta e="T1772" id="Seg_1136" s="T1771">who-GEN</ta>
            <ta e="T1773" id="Seg_1137" s="T1772">blood-NOM/GEN/ACC.3SG</ta>
            <ta e="T1774" id="Seg_1138" s="T1773">sweet.[NOM.SG]</ta>
            <ta e="T1775" id="Seg_1139" s="T1774">and</ta>
            <ta e="T1776" id="Seg_1140" s="T1775">mosquito.[NOM.SG]</ta>
            <ta e="T1777" id="Seg_1141" s="T1776">say-IPFVZ.[3SG]</ta>
            <ta e="T1778" id="Seg_1142" s="T1777">man-GEN</ta>
            <ta e="T1779" id="Seg_1143" s="T1778">blood-NOM/GEN/ACC.3SG</ta>
            <ta e="T1780" id="Seg_1144" s="T1779">very</ta>
            <ta e="T1781" id="Seg_1145" s="T1780">sweet.[NOM.SG]</ta>
            <ta e="T1782" id="Seg_1146" s="T1781">this.[NOM.SG]</ta>
            <ta e="T1783" id="Seg_1147" s="T1782">walk-PRS.[3SG]</ta>
            <ta e="T1784" id="Seg_1148" s="T1783">come-PRS.[3SG]</ta>
            <ta e="T1785" id="Seg_1149" s="T1784">small.[NOM.SG]</ta>
            <ta e="T1786" id="Seg_1150" s="T1785">boy.[NOM.SG]</ta>
            <ta e="T1787" id="Seg_1151" s="T1786">you.NOM</ta>
            <ta e="T1788" id="Seg_1152" s="T1787">man.[NOM.SG]</ta>
            <ta e="T1789" id="Seg_1153" s="T1788">no</ta>
            <ta e="T1790" id="Seg_1154" s="T1789">I.NOM</ta>
            <ta e="T1791" id="Seg_1155" s="T1790">NEG</ta>
            <ta e="T1792" id="Seg_1156" s="T1791">man.[NOM.SG]</ta>
            <ta e="T1793" id="Seg_1157" s="T1792">more</ta>
            <ta e="T1794" id="Seg_1158" s="T1793">when=INDEF</ta>
            <ta e="T1795" id="Seg_1159" s="T1794">grow-FUT-1SG</ta>
            <ta e="T1796" id="Seg_1160" s="T1795">become-FUT-1SG</ta>
            <ta e="T1797" id="Seg_1161" s="T1796">man.[NOM.SG]</ta>
            <ta e="T1798" id="Seg_1162" s="T1797">this.[NOM.SG]</ta>
            <ta e="T1799" id="Seg_1163" s="T1798">leave-MOM-PST.[3SG]</ta>
            <ta e="T1800" id="Seg_1164" s="T1799">and</ta>
            <ta e="T1801" id="Seg_1165" s="T1800">walk-PRS.[3SG]</ta>
            <ta e="T1802" id="Seg_1166" s="T1801">then</ta>
            <ta e="T1803" id="Seg_1167" s="T1802">come-PRS.[3SG]</ta>
            <ta e="T1804" id="Seg_1168" s="T1803">man.[NOM.SG]</ta>
            <ta e="T1805" id="Seg_1169" s="T1804">you.NOM</ta>
            <ta e="T1806" id="Seg_1170" s="T1805">man.[NOM.SG]</ta>
            <ta e="T1807" id="Seg_1171" s="T1806">and</ta>
            <ta e="T1808" id="Seg_1172" s="T1807">be-PST-1SG</ta>
            <ta e="T1809" id="Seg_1173" s="T1808">man.[NOM.SG]</ta>
            <ta e="T1810" id="Seg_1174" s="T1809">and</ta>
            <ta e="T1811" id="Seg_1175" s="T1810">now</ta>
            <ta e="T1812" id="Seg_1176" s="T1811">NEG</ta>
            <ta e="T1813" id="Seg_1177" s="T1812">man.[NOM.SG]</ta>
            <ta e="T1814" id="Seg_1178" s="T1813">then</ta>
            <ta e="T1815" id="Seg_1179" s="T1814">again</ta>
            <ta e="T1816" id="Seg_1180" s="T1815">walk-PRS.[3SG]</ta>
            <ta e="T1817" id="Seg_1181" s="T1816">leave-MOM-PST.[3SG]</ta>
            <ta e="T1818" id="Seg_1182" s="T1817">come-PRS.[3SG]</ta>
            <ta e="T1819" id="Seg_1183" s="T1818">one.[NOM.SG]</ta>
            <ta e="T1820" id="Seg_1184" s="T1819">man.[NOM.SG]</ta>
            <ta e="T1821" id="Seg_1185" s="T1820">you.NOM</ta>
            <ta e="T1822" id="Seg_1186" s="T1821">man.[NOM.SG]</ta>
            <ta e="T1825" id="Seg_1187" s="T1824">this.[NOM.SG]</ta>
            <ta e="T1826" id="Seg_1188" s="T1825">say-IPFVZ.[3SG]</ta>
            <ta e="T1827" id="Seg_1189" s="T1826">man.[NOM.SG]</ta>
            <ta e="T1828" id="Seg_1190" s="T1827">then</ta>
            <ta e="T1829" id="Seg_1191" s="T1828">this.[NOM.SG]</ta>
            <ta e="T1830" id="Seg_1192" s="T1829">walk-PRS.[3SG]</ta>
            <ta e="T1831" id="Seg_1193" s="T1830">and</ta>
            <ta e="T1832" id="Seg_1194" s="T1831">this.[NOM.SG]</ta>
            <ta e="T1833" id="Seg_1195" s="T1832">this.[NOM.SG]</ta>
            <ta e="T1834" id="Seg_1196" s="T1833">this-LAT</ta>
            <ta e="T1835" id="Seg_1197" s="T1834">this-ABL</ta>
            <ta e="T1836" id="Seg_1198" s="T1835">NEG</ta>
            <ta e="T1837" id="Seg_1199" s="T1836">remain-PRS.[3SG]</ta>
            <ta e="T1838" id="Seg_1200" s="T1837">also</ta>
            <ta e="T1839" id="Seg_1201" s="T1838">come-PRS.[3SG]</ta>
            <ta e="T1840" id="Seg_1202" s="T1839">this</ta>
            <ta e="T1841" id="Seg_1203" s="T1840">this</ta>
            <ta e="T1842" id="Seg_1204" s="T1841">take-PST.[3SG]</ta>
            <ta e="T1843" id="Seg_1205" s="T1842">saber-ACC.3SG</ta>
            <ta e="T1844" id="Seg_1206" s="T1843">and</ta>
            <ta e="T1845" id="Seg_1207" s="T1844">this-ACC</ta>
            <ta e="T1846" id="Seg_1208" s="T1845">hit-MULT-MOM-PST.[3SG]</ta>
            <ta e="T1847" id="Seg_1209" s="T1846">this-GEN</ta>
            <ta e="T1848" id="Seg_1210" s="T1847">PTCL</ta>
            <ta e="T1849" id="Seg_1211" s="T1848">blood-NOM/GEN/ACC.3SG</ta>
            <ta e="T1850" id="Seg_1212" s="T1849">flow-DUR.[3SG]</ta>
            <ta e="T1851" id="Seg_1213" s="T1850">this.[NOM.SG]</ta>
            <ta e="T1852" id="Seg_1214" s="T1851">PTCL</ta>
            <ta e="T1853" id="Seg_1215" s="T1852">run-MOM-PST.[3SG]</ta>
            <ta e="T1854" id="Seg_1216" s="T1853">and</ta>
            <ta e="T1855" id="Seg_1217" s="T1854">this.[NOM.SG]</ta>
            <ta e="T1857" id="Seg_1218" s="T1856">gun-INS</ta>
            <ta e="T1858" id="Seg_1219" s="T1857">shoot-MOM-PST.[3SG]</ta>
            <ta e="T1859" id="Seg_1220" s="T1858">this.[NOM.SG]</ta>
            <ta e="T1860" id="Seg_1221" s="T1859">this.[NOM.SG]</ta>
            <ta e="T1861" id="Seg_1222" s="T1860">PTCL</ta>
            <ta e="T1862" id="Seg_1223" s="T1861">run-MOM-PST.[3SG]</ta>
            <ta e="T1863" id="Seg_1224" s="T1862">then</ta>
            <ta e="T1864" id="Seg_1225" s="T1863">come-PST.[3SG]</ta>
            <ta e="T1865" id="Seg_1226" s="T1864">you.NOM</ta>
            <ta e="T1866" id="Seg_1227" s="T1865">I.LAT</ta>
            <ta e="T1867" id="Seg_1228" s="T1866">say-IPFVZ-2SG</ta>
            <ta e="T1868" id="Seg_1229" s="T1867">sweet.[NOM.SG]</ta>
            <ta e="T1869" id="Seg_1230" s="T1868">blood.[NOM.SG]</ta>
            <ta e="T1870" id="Seg_1231" s="T1869">man-GEN</ta>
            <ta e="T1871" id="Seg_1232" s="T1870">this-GEN</ta>
            <ta e="T1872" id="Seg_1233" s="T1871">very</ta>
            <ta e="T1873" id="Seg_1234" s="T1872">language-NOM/GEN.3SG</ta>
            <ta e="T1874" id="Seg_1235" s="T1873">long.[NOM.SG]</ta>
            <ta e="T1875" id="Seg_1236" s="T1874">language-3SG-INS</ta>
            <ta e="T1876" id="Seg_1237" s="T1875">I.ACC</ta>
            <ta e="T1877" id="Seg_1238" s="T1876">hit-MULT-PST.[3SG]</ta>
            <ta e="T1878" id="Seg_1239" s="T1877">PTCL</ta>
            <ta e="T1879" id="Seg_1240" s="T1878">blood.[NOM.SG]</ta>
            <ta e="T1880" id="Seg_1241" s="T1879">flow-DUR.[3SG]</ta>
            <ta e="T1881" id="Seg_1242" s="T1880">and</ta>
            <ta e="T1882" id="Seg_1243" s="T1881">then</ta>
            <ta e="T1883" id="Seg_1244" s="T1882">gun-INS</ta>
            <ta e="T1884" id="Seg_1245" s="T1883">shoot-MOM-PST.[3SG]</ta>
            <ta e="T1886" id="Seg_1246" s="T1885">now</ta>
            <ta e="T1887" id="Seg_1247" s="T1886">and</ta>
            <ta e="T1888" id="Seg_1248" s="T1887">hurt-DUR.[3SG]</ta>
            <ta e="T1889" id="Seg_1249" s="T1888">PTCL</ta>
            <ta e="T1890" id="Seg_1250" s="T1889">meat-NOM/GEN/ACC.1SG</ta>
            <ta e="T1891" id="Seg_1251" s="T1890">enough</ta>
            <ta e="T1892" id="Seg_1252" s="T1891">then</ta>
            <ta e="T1893" id="Seg_1253" s="T1892">mosquito.[NOM.SG]</ta>
            <ta e="T1894" id="Seg_1254" s="T1893">PTCL</ta>
            <ta e="T1895" id="Seg_1255" s="T1894">laugh-PST.[3SG]</ta>
            <ta e="T1896" id="Seg_1256" s="T1895">laugh-PST.[3SG]</ta>
            <ta e="T1897" id="Seg_1257" s="T0">die-RES-PST.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1766" id="Seg_1258" s="T1765">медведь.[NOM.SG]</ta>
            <ta e="T1767" id="Seg_1259" s="T1766">и</ta>
            <ta e="T1768" id="Seg_1260" s="T1767">комар.[NOM.SG]</ta>
            <ta e="T1769" id="Seg_1261" s="T1768">говорить-DUR.[3SG]</ta>
            <ta e="T1770" id="Seg_1262" s="T1769">медведь.[NOM.SG]</ta>
            <ta e="T1771" id="Seg_1263" s="T1770">спросить-DUR.[3SG]</ta>
            <ta e="T1772" id="Seg_1264" s="T1771">кто-GEN</ta>
            <ta e="T1773" id="Seg_1265" s="T1772">кровь-NOM/GEN/ACC.3SG</ta>
            <ta e="T1774" id="Seg_1266" s="T1773">сладкий.[NOM.SG]</ta>
            <ta e="T1775" id="Seg_1267" s="T1774">а</ta>
            <ta e="T1776" id="Seg_1268" s="T1775">комар.[NOM.SG]</ta>
            <ta e="T1777" id="Seg_1269" s="T1776">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1778" id="Seg_1270" s="T1777">мужчина-GEN</ta>
            <ta e="T1779" id="Seg_1271" s="T1778">кровь-NOM/GEN/ACC.3SG</ta>
            <ta e="T1780" id="Seg_1272" s="T1779">очень</ta>
            <ta e="T1781" id="Seg_1273" s="T1780">сладкий.[NOM.SG]</ta>
            <ta e="T1782" id="Seg_1274" s="T1781">этот.[NOM.SG]</ta>
            <ta e="T1783" id="Seg_1275" s="T1782">идти-PRS.[3SG]</ta>
            <ta e="T1784" id="Seg_1276" s="T1783">прийти-PRS.[3SG]</ta>
            <ta e="T1785" id="Seg_1277" s="T1784">маленький.[NOM.SG]</ta>
            <ta e="T1786" id="Seg_1278" s="T1785">мальчик.[NOM.SG]</ta>
            <ta e="T1787" id="Seg_1279" s="T1786">ты.NOM</ta>
            <ta e="T1788" id="Seg_1280" s="T1787">мужчина.[NOM.SG]</ta>
            <ta e="T1789" id="Seg_1281" s="T1788">нет</ta>
            <ta e="T1790" id="Seg_1282" s="T1789">я.NOM</ta>
            <ta e="T1791" id="Seg_1283" s="T1790">NEG</ta>
            <ta e="T1792" id="Seg_1284" s="T1791">мужчина.[NOM.SG]</ta>
            <ta e="T1793" id="Seg_1285" s="T1792">еще</ta>
            <ta e="T1794" id="Seg_1286" s="T1793">когда=INDEF</ta>
            <ta e="T1795" id="Seg_1287" s="T1794">расти-FUT-1SG</ta>
            <ta e="T1796" id="Seg_1288" s="T1795">мочь-FUT-1SG</ta>
            <ta e="T1797" id="Seg_1289" s="T1796">мужчина.[NOM.SG]</ta>
            <ta e="T1798" id="Seg_1290" s="T1797">этот.[NOM.SG]</ta>
            <ta e="T1799" id="Seg_1291" s="T1798">оставить-MOM-PST.[3SG]</ta>
            <ta e="T1800" id="Seg_1292" s="T1799">и</ta>
            <ta e="T1801" id="Seg_1293" s="T1800">идти-PRS.[3SG]</ta>
            <ta e="T1802" id="Seg_1294" s="T1801">тогда</ta>
            <ta e="T1803" id="Seg_1295" s="T1802">прийти-PRS.[3SG]</ta>
            <ta e="T1804" id="Seg_1296" s="T1803">мужчина.[NOM.SG]</ta>
            <ta e="T1805" id="Seg_1297" s="T1804">ты.NOM</ta>
            <ta e="T1806" id="Seg_1298" s="T1805">мужчина.[NOM.SG]</ta>
            <ta e="T1807" id="Seg_1299" s="T1806">и</ta>
            <ta e="T1808" id="Seg_1300" s="T1807">быть-PST-1SG</ta>
            <ta e="T1809" id="Seg_1301" s="T1808">мужчина.[NOM.SG]</ta>
            <ta e="T1810" id="Seg_1302" s="T1809">а</ta>
            <ta e="T1811" id="Seg_1303" s="T1810">сейчас</ta>
            <ta e="T1812" id="Seg_1304" s="T1811">NEG</ta>
            <ta e="T1813" id="Seg_1305" s="T1812">мужчина.[NOM.SG]</ta>
            <ta e="T1814" id="Seg_1306" s="T1813">тогда</ta>
            <ta e="T1815" id="Seg_1307" s="T1814">опять</ta>
            <ta e="T1816" id="Seg_1308" s="T1815">идти-PRS.[3SG]</ta>
            <ta e="T1817" id="Seg_1309" s="T1816">оставить-MOM-PST.[3SG]</ta>
            <ta e="T1818" id="Seg_1310" s="T1817">прийти-PRS.[3SG]</ta>
            <ta e="T1819" id="Seg_1311" s="T1818">один.[NOM.SG]</ta>
            <ta e="T1820" id="Seg_1312" s="T1819">мужчина.[NOM.SG]</ta>
            <ta e="T1821" id="Seg_1313" s="T1820">ты.NOM</ta>
            <ta e="T1822" id="Seg_1314" s="T1821">мужчина.[NOM.SG]</ta>
            <ta e="T1825" id="Seg_1315" s="T1824">этот.[NOM.SG]</ta>
            <ta e="T1826" id="Seg_1316" s="T1825">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1827" id="Seg_1317" s="T1826">мужчина.[NOM.SG]</ta>
            <ta e="T1828" id="Seg_1318" s="T1827">тогда</ta>
            <ta e="T1829" id="Seg_1319" s="T1828">этот.[NOM.SG]</ta>
            <ta e="T1830" id="Seg_1320" s="T1829">идти-PRS.[3SG]</ta>
            <ta e="T1831" id="Seg_1321" s="T1830">а</ta>
            <ta e="T1832" id="Seg_1322" s="T1831">этот.[NOM.SG]</ta>
            <ta e="T1833" id="Seg_1323" s="T1832">этот.[NOM.SG]</ta>
            <ta e="T1834" id="Seg_1324" s="T1833">этот-LAT</ta>
            <ta e="T1835" id="Seg_1325" s="T1834">этот-ABL</ta>
            <ta e="T1836" id="Seg_1326" s="T1835">NEG</ta>
            <ta e="T1837" id="Seg_1327" s="T1836">остаться-PRS.[3SG]</ta>
            <ta e="T1838" id="Seg_1328" s="T1837">тоже</ta>
            <ta e="T1839" id="Seg_1329" s="T1838">прийти-PRS.[3SG]</ta>
            <ta e="T1840" id="Seg_1330" s="T1839">этот</ta>
            <ta e="T1841" id="Seg_1331" s="T1840">этот</ta>
            <ta e="T1842" id="Seg_1332" s="T1841">взять-PST.[3SG]</ta>
            <ta e="T1843" id="Seg_1333" s="T1842">сабля-ACC.3SG</ta>
            <ta e="T1844" id="Seg_1334" s="T1843">и</ta>
            <ta e="T1845" id="Seg_1335" s="T1844">этот-ACC</ta>
            <ta e="T1846" id="Seg_1336" s="T1845">ударить-MULT-MOM-PST.[3SG]</ta>
            <ta e="T1847" id="Seg_1337" s="T1846">этот-GEN</ta>
            <ta e="T1848" id="Seg_1338" s="T1847">PTCL</ta>
            <ta e="T1849" id="Seg_1339" s="T1848">кровь-NOM/GEN/ACC.3SG</ta>
            <ta e="T1850" id="Seg_1340" s="T1849">течь-DUR.[3SG]</ta>
            <ta e="T1851" id="Seg_1341" s="T1850">этот.[NOM.SG]</ta>
            <ta e="T1852" id="Seg_1342" s="T1851">PTCL</ta>
            <ta e="T1853" id="Seg_1343" s="T1852">бежать-MOM-PST.[3SG]</ta>
            <ta e="T1854" id="Seg_1344" s="T1853">а</ta>
            <ta e="T1855" id="Seg_1345" s="T1854">этот.[NOM.SG]</ta>
            <ta e="T1857" id="Seg_1346" s="T1856">ружье-INS</ta>
            <ta e="T1858" id="Seg_1347" s="T1857">стрелять-MOM-PST.[3SG]</ta>
            <ta e="T1859" id="Seg_1348" s="T1858">этот.[NOM.SG]</ta>
            <ta e="T1860" id="Seg_1349" s="T1859">этот.[NOM.SG]</ta>
            <ta e="T1861" id="Seg_1350" s="T1860">PTCL</ta>
            <ta e="T1862" id="Seg_1351" s="T1861">бежать-MOM-PST.[3SG]</ta>
            <ta e="T1863" id="Seg_1352" s="T1862">тогда</ta>
            <ta e="T1864" id="Seg_1353" s="T1863">прийти-PST.[3SG]</ta>
            <ta e="T1865" id="Seg_1354" s="T1864">ты.NOM</ta>
            <ta e="T1866" id="Seg_1355" s="T1865">я.LAT</ta>
            <ta e="T1867" id="Seg_1356" s="T1866">сказать-IPFVZ-2SG</ta>
            <ta e="T1868" id="Seg_1357" s="T1867">сладкий.[NOM.SG]</ta>
            <ta e="T1869" id="Seg_1358" s="T1868">кровь.[NOM.SG]</ta>
            <ta e="T1870" id="Seg_1359" s="T1869">мужчина-GEN</ta>
            <ta e="T1871" id="Seg_1360" s="T1870">этот-GEN</ta>
            <ta e="T1872" id="Seg_1361" s="T1871">очень</ta>
            <ta e="T1873" id="Seg_1362" s="T1872">язык-NOM/GEN.3SG</ta>
            <ta e="T1874" id="Seg_1363" s="T1873">длинный.[NOM.SG]</ta>
            <ta e="T1875" id="Seg_1364" s="T1874">язык-3SG-INS</ta>
            <ta e="T1876" id="Seg_1365" s="T1875">я.ACC</ta>
            <ta e="T1877" id="Seg_1366" s="T1876">ударить-MULT-PST.[3SG]</ta>
            <ta e="T1878" id="Seg_1367" s="T1877">PTCL</ta>
            <ta e="T1879" id="Seg_1368" s="T1878">кровь.[NOM.SG]</ta>
            <ta e="T1880" id="Seg_1369" s="T1879">течь-DUR.[3SG]</ta>
            <ta e="T1881" id="Seg_1370" s="T1880">а</ta>
            <ta e="T1882" id="Seg_1371" s="T1881">тогда</ta>
            <ta e="T1883" id="Seg_1372" s="T1882">ружье-INS</ta>
            <ta e="T1884" id="Seg_1373" s="T1883">стрелять-MOM-PST.[3SG]</ta>
            <ta e="T1886" id="Seg_1374" s="T1885">сейчас</ta>
            <ta e="T1887" id="Seg_1375" s="T1886">и</ta>
            <ta e="T1888" id="Seg_1376" s="T1887">болеть-DUR.[3SG]</ta>
            <ta e="T1889" id="Seg_1377" s="T1888">PTCL</ta>
            <ta e="T1890" id="Seg_1378" s="T1889">мясо-NOM/GEN/ACC.1SG</ta>
            <ta e="T1891" id="Seg_1379" s="T1890">хватит</ta>
            <ta e="T1892" id="Seg_1380" s="T1891">тогда</ta>
            <ta e="T1893" id="Seg_1381" s="T1892">комар.[NOM.SG]</ta>
            <ta e="T1894" id="Seg_1382" s="T1893">PTCL</ta>
            <ta e="T1895" id="Seg_1383" s="T1894">смеяться-PST.[3SG]</ta>
            <ta e="T1896" id="Seg_1384" s="T1895">смеяться-PST.[3SG]</ta>
            <ta e="T1897" id="Seg_1385" s="T0">умереть-RES-PST.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1766" id="Seg_1386" s="T1765">n-n:case</ta>
            <ta e="T1767" id="Seg_1387" s="T1766">conj</ta>
            <ta e="T1768" id="Seg_1388" s="T1767">n-n:case</ta>
            <ta e="T1769" id="Seg_1389" s="T1768">v-v&gt;v-v:pn</ta>
            <ta e="T1770" id="Seg_1390" s="T1769">n-n:case</ta>
            <ta e="T1771" id="Seg_1391" s="T1770">v-v&gt;v-v:pn</ta>
            <ta e="T1772" id="Seg_1392" s="T1771">que-n:case</ta>
            <ta e="T1773" id="Seg_1393" s="T1772">n-n:case.poss</ta>
            <ta e="T1774" id="Seg_1394" s="T1773">adj-n:case</ta>
            <ta e="T1775" id="Seg_1395" s="T1774">conj</ta>
            <ta e="T1776" id="Seg_1396" s="T1775">n-n:case</ta>
            <ta e="T1777" id="Seg_1397" s="T1776">v-v&gt;v-v:pn</ta>
            <ta e="T1778" id="Seg_1398" s="T1777">n-n:case</ta>
            <ta e="T1779" id="Seg_1399" s="T1778">n-n:case.poss</ta>
            <ta e="T1780" id="Seg_1400" s="T1779">adv</ta>
            <ta e="T1781" id="Seg_1401" s="T1780">adj-n:case</ta>
            <ta e="T1782" id="Seg_1402" s="T1781">dempro-n:case</ta>
            <ta e="T1783" id="Seg_1403" s="T1782">v-v:tense-v:pn</ta>
            <ta e="T1784" id="Seg_1404" s="T1783">v-v:tense-v:pn</ta>
            <ta e="T1785" id="Seg_1405" s="T1784">adj-n:case</ta>
            <ta e="T1786" id="Seg_1406" s="T1785">n-n:case</ta>
            <ta e="T1787" id="Seg_1407" s="T1786">pers</ta>
            <ta e="T1788" id="Seg_1408" s="T1787">n-n:case</ta>
            <ta e="T1789" id="Seg_1409" s="T1788">ptcl</ta>
            <ta e="T1790" id="Seg_1410" s="T1789">pers</ta>
            <ta e="T1791" id="Seg_1411" s="T1790">ptcl</ta>
            <ta e="T1792" id="Seg_1412" s="T1791">n-n:case</ta>
            <ta e="T1793" id="Seg_1413" s="T1792">adv</ta>
            <ta e="T1794" id="Seg_1414" s="T1793">que=ptcl</ta>
            <ta e="T1795" id="Seg_1415" s="T1794">v-v:tense-v:pn</ta>
            <ta e="T1796" id="Seg_1416" s="T1795">v-v:tense-v:pn</ta>
            <ta e="T1797" id="Seg_1417" s="T1796">n-n:case</ta>
            <ta e="T1798" id="Seg_1418" s="T1797">dempro-n:case</ta>
            <ta e="T1799" id="Seg_1419" s="T1798">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1800" id="Seg_1420" s="T1799">conj</ta>
            <ta e="T1801" id="Seg_1421" s="T1800">v-v:tense-v:pn</ta>
            <ta e="T1802" id="Seg_1422" s="T1801">adv</ta>
            <ta e="T1803" id="Seg_1423" s="T1802">v-v:tense-v:pn</ta>
            <ta e="T1804" id="Seg_1424" s="T1803">n-n:case</ta>
            <ta e="T1805" id="Seg_1425" s="T1804">pers</ta>
            <ta e="T1806" id="Seg_1426" s="T1805">n-n:case</ta>
            <ta e="T1807" id="Seg_1427" s="T1806">conj</ta>
            <ta e="T1808" id="Seg_1428" s="T1807">v-v:tense-v:pn</ta>
            <ta e="T1809" id="Seg_1429" s="T1808">n-n:case</ta>
            <ta e="T1810" id="Seg_1430" s="T1809">conj</ta>
            <ta e="T1811" id="Seg_1431" s="T1810">adv</ta>
            <ta e="T1812" id="Seg_1432" s="T1811">ptcl</ta>
            <ta e="T1813" id="Seg_1433" s="T1812">n-n:case</ta>
            <ta e="T1814" id="Seg_1434" s="T1813">adv</ta>
            <ta e="T1815" id="Seg_1435" s="T1814">adv</ta>
            <ta e="T1816" id="Seg_1436" s="T1815">v-v:tense-v:pn</ta>
            <ta e="T1817" id="Seg_1437" s="T1816">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1818" id="Seg_1438" s="T1817">v-v:tense-v:pn</ta>
            <ta e="T1819" id="Seg_1439" s="T1818">num-n:case</ta>
            <ta e="T1820" id="Seg_1440" s="T1819">n-n:case</ta>
            <ta e="T1821" id="Seg_1441" s="T1820">pers</ta>
            <ta e="T1822" id="Seg_1442" s="T1821">n-n:case</ta>
            <ta e="T1825" id="Seg_1443" s="T1824">dempro-n:case</ta>
            <ta e="T1826" id="Seg_1444" s="T1825">v-v&gt;v-v:pn</ta>
            <ta e="T1827" id="Seg_1445" s="T1826">n-n:case</ta>
            <ta e="T1828" id="Seg_1446" s="T1827">adv</ta>
            <ta e="T1829" id="Seg_1447" s="T1828">dempro-n:case</ta>
            <ta e="T1830" id="Seg_1448" s="T1829">v-v:tense-v:pn</ta>
            <ta e="T1831" id="Seg_1449" s="T1830">conj</ta>
            <ta e="T1832" id="Seg_1450" s="T1831">dempro-n:case</ta>
            <ta e="T1833" id="Seg_1451" s="T1832">dempro-n:case</ta>
            <ta e="T1834" id="Seg_1452" s="T1833">dempro-n:case</ta>
            <ta e="T1835" id="Seg_1453" s="T1834">dempro-n:case</ta>
            <ta e="T1836" id="Seg_1454" s="T1835">ptcl</ta>
            <ta e="T1837" id="Seg_1455" s="T1836">v-v:tense-v:pn</ta>
            <ta e="T1838" id="Seg_1456" s="T1837">ptcl</ta>
            <ta e="T1839" id="Seg_1457" s="T1838">v-v:tense-v:pn</ta>
            <ta e="T1840" id="Seg_1458" s="T1839">dempro</ta>
            <ta e="T1841" id="Seg_1459" s="T1840">dempro</ta>
            <ta e="T1842" id="Seg_1460" s="T1841">v-v:tense-v:pn</ta>
            <ta e="T1843" id="Seg_1461" s="T1842">n-n:case.poss</ta>
            <ta e="T1844" id="Seg_1462" s="T1843">conj</ta>
            <ta e="T1845" id="Seg_1463" s="T1844">dempro-n:case</ta>
            <ta e="T1846" id="Seg_1464" s="T1845">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1847" id="Seg_1465" s="T1846">dempro-n:case</ta>
            <ta e="T1848" id="Seg_1466" s="T1847">ptcl</ta>
            <ta e="T1849" id="Seg_1467" s="T1848">n-n:case.poss</ta>
            <ta e="T1850" id="Seg_1468" s="T1849">v-v&gt;v-v:pn</ta>
            <ta e="T1851" id="Seg_1469" s="T1850">dempro-n:case</ta>
            <ta e="T1852" id="Seg_1470" s="T1851">ptcl</ta>
            <ta e="T1853" id="Seg_1471" s="T1852">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1854" id="Seg_1472" s="T1853">conj</ta>
            <ta e="T1855" id="Seg_1473" s="T1854">dempro-n:case</ta>
            <ta e="T1857" id="Seg_1474" s="T1856">n-n:case</ta>
            <ta e="T1858" id="Seg_1475" s="T1857">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1859" id="Seg_1476" s="T1858">dempro-n:case</ta>
            <ta e="T1860" id="Seg_1477" s="T1859">dempro-n:case</ta>
            <ta e="T1861" id="Seg_1478" s="T1860">ptcl</ta>
            <ta e="T1862" id="Seg_1479" s="T1861">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1863" id="Seg_1480" s="T1862">adv</ta>
            <ta e="T1864" id="Seg_1481" s="T1863">v-v:tense-v:pn</ta>
            <ta e="T1865" id="Seg_1482" s="T1864">pers</ta>
            <ta e="T1866" id="Seg_1483" s="T1865">pers</ta>
            <ta e="T1867" id="Seg_1484" s="T1866">v-v&gt;v-v:pn</ta>
            <ta e="T1868" id="Seg_1485" s="T1867">adj-n:case</ta>
            <ta e="T1869" id="Seg_1486" s="T1868">n-n:case</ta>
            <ta e="T1870" id="Seg_1487" s="T1869">n-n:case</ta>
            <ta e="T1871" id="Seg_1488" s="T1870">dempro-n:case</ta>
            <ta e="T1872" id="Seg_1489" s="T1871">adv</ta>
            <ta e="T1873" id="Seg_1490" s="T1872">n-n:case.poss</ta>
            <ta e="T1874" id="Seg_1491" s="T1873">adj-n:case</ta>
            <ta e="T1875" id="Seg_1492" s="T1874">n-n:case.poss-n:case</ta>
            <ta e="T1876" id="Seg_1493" s="T1875">pers</ta>
            <ta e="T1877" id="Seg_1494" s="T1876">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1878" id="Seg_1495" s="T1877">ptcl</ta>
            <ta e="T1879" id="Seg_1496" s="T1878">n-n:case</ta>
            <ta e="T1880" id="Seg_1497" s="T1879">v-v&gt;v-v:pn</ta>
            <ta e="T1881" id="Seg_1498" s="T1880">conj</ta>
            <ta e="T1882" id="Seg_1499" s="T1881">adv</ta>
            <ta e="T1883" id="Seg_1500" s="T1882">n-n:case</ta>
            <ta e="T1884" id="Seg_1501" s="T1883">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1886" id="Seg_1502" s="T1885">adv</ta>
            <ta e="T1887" id="Seg_1503" s="T1886">conj</ta>
            <ta e="T1888" id="Seg_1504" s="T1887">v-v&gt;v-v:pn</ta>
            <ta e="T1889" id="Seg_1505" s="T1888">ptcl</ta>
            <ta e="T1890" id="Seg_1506" s="T1889">n-n:case.poss</ta>
            <ta e="T1891" id="Seg_1507" s="T1890">ptcl</ta>
            <ta e="T1892" id="Seg_1508" s="T1891">adv</ta>
            <ta e="T1893" id="Seg_1509" s="T1892">n-n:case</ta>
            <ta e="T1894" id="Seg_1510" s="T1893">ptcl</ta>
            <ta e="T1895" id="Seg_1511" s="T1894">v-v:tense-v:pn</ta>
            <ta e="T1896" id="Seg_1512" s="T1895">v-v:tense-v:pn</ta>
            <ta e="T1897" id="Seg_1513" s="T0">v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1766" id="Seg_1514" s="T1765">n</ta>
            <ta e="T1767" id="Seg_1515" s="T1766">conj</ta>
            <ta e="T1768" id="Seg_1516" s="T1767">n</ta>
            <ta e="T1769" id="Seg_1517" s="T1768">v</ta>
            <ta e="T1770" id="Seg_1518" s="T1769">n</ta>
            <ta e="T1771" id="Seg_1519" s="T1770">v</ta>
            <ta e="T1772" id="Seg_1520" s="T1771">que</ta>
            <ta e="T1773" id="Seg_1521" s="T1772">n</ta>
            <ta e="T1774" id="Seg_1522" s="T1773">adj</ta>
            <ta e="T1775" id="Seg_1523" s="T1774">conj</ta>
            <ta e="T1776" id="Seg_1524" s="T1775">n</ta>
            <ta e="T1777" id="Seg_1525" s="T1776">v</ta>
            <ta e="T1778" id="Seg_1526" s="T1777">n</ta>
            <ta e="T1779" id="Seg_1527" s="T1778">n</ta>
            <ta e="T1780" id="Seg_1528" s="T1779">adv</ta>
            <ta e="T1781" id="Seg_1529" s="T1780">adj</ta>
            <ta e="T1782" id="Seg_1530" s="T1781">dempro</ta>
            <ta e="T1783" id="Seg_1531" s="T1782">v</ta>
            <ta e="T1784" id="Seg_1532" s="T1783">v</ta>
            <ta e="T1785" id="Seg_1533" s="T1784">adj</ta>
            <ta e="T1786" id="Seg_1534" s="T1785">n</ta>
            <ta e="T1787" id="Seg_1535" s="T1786">pers</ta>
            <ta e="T1788" id="Seg_1536" s="T1787">n</ta>
            <ta e="T1789" id="Seg_1537" s="T1788">ptcl</ta>
            <ta e="T1790" id="Seg_1538" s="T1789">pers</ta>
            <ta e="T1791" id="Seg_1539" s="T1790">ptcl</ta>
            <ta e="T1792" id="Seg_1540" s="T1791">n</ta>
            <ta e="T1793" id="Seg_1541" s="T1792">adv</ta>
            <ta e="T1794" id="Seg_1542" s="T1793">que</ta>
            <ta e="T1795" id="Seg_1543" s="T1794">v</ta>
            <ta e="T1796" id="Seg_1544" s="T1795">v</ta>
            <ta e="T1797" id="Seg_1545" s="T1796">n</ta>
            <ta e="T1798" id="Seg_1546" s="T1797">dempro</ta>
            <ta e="T1799" id="Seg_1547" s="T1798">v</ta>
            <ta e="T1800" id="Seg_1548" s="T1799">conj</ta>
            <ta e="T1801" id="Seg_1549" s="T1800">v</ta>
            <ta e="T1802" id="Seg_1550" s="T1801">adv</ta>
            <ta e="T1803" id="Seg_1551" s="T1802">v</ta>
            <ta e="T1804" id="Seg_1552" s="T1803">n</ta>
            <ta e="T1805" id="Seg_1553" s="T1804">pers</ta>
            <ta e="T1806" id="Seg_1554" s="T1805">n</ta>
            <ta e="T1807" id="Seg_1555" s="T1806">conj</ta>
            <ta e="T1808" id="Seg_1556" s="T1807">v</ta>
            <ta e="T1809" id="Seg_1557" s="T1808">n</ta>
            <ta e="T1810" id="Seg_1558" s="T1809">conj</ta>
            <ta e="T1811" id="Seg_1559" s="T1810">adv</ta>
            <ta e="T1812" id="Seg_1560" s="T1811">ptcl</ta>
            <ta e="T1813" id="Seg_1561" s="T1812">n</ta>
            <ta e="T1814" id="Seg_1562" s="T1813">adv</ta>
            <ta e="T1815" id="Seg_1563" s="T1814">adv</ta>
            <ta e="T1816" id="Seg_1564" s="T1815">v</ta>
            <ta e="T1817" id="Seg_1565" s="T1816">v</ta>
            <ta e="T1818" id="Seg_1566" s="T1817">v</ta>
            <ta e="T1819" id="Seg_1567" s="T1818">num</ta>
            <ta e="T1820" id="Seg_1568" s="T1819">n</ta>
            <ta e="T1821" id="Seg_1569" s="T1820">pers</ta>
            <ta e="T1822" id="Seg_1570" s="T1821">n</ta>
            <ta e="T1825" id="Seg_1571" s="T1824">dempro</ta>
            <ta e="T1826" id="Seg_1572" s="T1825">v</ta>
            <ta e="T1827" id="Seg_1573" s="T1826">n</ta>
            <ta e="T1828" id="Seg_1574" s="T1827">adv</ta>
            <ta e="T1829" id="Seg_1575" s="T1828">dempro</ta>
            <ta e="T1830" id="Seg_1576" s="T1829">v</ta>
            <ta e="T1831" id="Seg_1577" s="T1830">conj</ta>
            <ta e="T1832" id="Seg_1578" s="T1831">dempro</ta>
            <ta e="T1833" id="Seg_1579" s="T1832">dempro</ta>
            <ta e="T1834" id="Seg_1580" s="T1833">dempro</ta>
            <ta e="T1835" id="Seg_1581" s="T1834">dempro</ta>
            <ta e="T1836" id="Seg_1582" s="T1835">ptcl</ta>
            <ta e="T1837" id="Seg_1583" s="T1836">v</ta>
            <ta e="T1838" id="Seg_1584" s="T1837">ptcl</ta>
            <ta e="T1839" id="Seg_1585" s="T1838">v</ta>
            <ta e="T1840" id="Seg_1586" s="T1839">dempro</ta>
            <ta e="T1841" id="Seg_1587" s="T1840">dempro</ta>
            <ta e="T1842" id="Seg_1588" s="T1841">v</ta>
            <ta e="T1843" id="Seg_1589" s="T1842">n</ta>
            <ta e="T1844" id="Seg_1590" s="T1843">conj</ta>
            <ta e="T1845" id="Seg_1591" s="T1844">dempro</ta>
            <ta e="T1846" id="Seg_1592" s="T1845">v</ta>
            <ta e="T1847" id="Seg_1593" s="T1846">dempro</ta>
            <ta e="T1848" id="Seg_1594" s="T1847">ptcl</ta>
            <ta e="T1849" id="Seg_1595" s="T1848">n</ta>
            <ta e="T1850" id="Seg_1596" s="T1849">v</ta>
            <ta e="T1851" id="Seg_1597" s="T1850">dempro</ta>
            <ta e="T1852" id="Seg_1598" s="T1851">ptcl</ta>
            <ta e="T1853" id="Seg_1599" s="T1852">v</ta>
            <ta e="T1854" id="Seg_1600" s="T1853">conj</ta>
            <ta e="T1855" id="Seg_1601" s="T1854">dempro</ta>
            <ta e="T1857" id="Seg_1602" s="T1856">n</ta>
            <ta e="T1858" id="Seg_1603" s="T1857">v</ta>
            <ta e="T1859" id="Seg_1604" s="T1858">dempro</ta>
            <ta e="T1860" id="Seg_1605" s="T1859">dempro</ta>
            <ta e="T1861" id="Seg_1606" s="T1860">ptcl</ta>
            <ta e="T1862" id="Seg_1607" s="T1861">v</ta>
            <ta e="T1863" id="Seg_1608" s="T1862">adv</ta>
            <ta e="T1864" id="Seg_1609" s="T1863">v</ta>
            <ta e="T1865" id="Seg_1610" s="T1864">pers</ta>
            <ta e="T1866" id="Seg_1611" s="T1865">pers</ta>
            <ta e="T1867" id="Seg_1612" s="T1866">v</ta>
            <ta e="T1868" id="Seg_1613" s="T1867">adj</ta>
            <ta e="T1869" id="Seg_1614" s="T1868">n</ta>
            <ta e="T1870" id="Seg_1615" s="T1869">n</ta>
            <ta e="T1871" id="Seg_1616" s="T1870">dempro</ta>
            <ta e="T1872" id="Seg_1617" s="T1871">adv</ta>
            <ta e="T1873" id="Seg_1618" s="T1872">n</ta>
            <ta e="T1874" id="Seg_1619" s="T1873">adj</ta>
            <ta e="T1875" id="Seg_1620" s="T1874">n</ta>
            <ta e="T1876" id="Seg_1621" s="T1875">pers</ta>
            <ta e="T1877" id="Seg_1622" s="T1876">v</ta>
            <ta e="T1878" id="Seg_1623" s="T1877">ptcl</ta>
            <ta e="T1879" id="Seg_1624" s="T1878">n</ta>
            <ta e="T1880" id="Seg_1625" s="T1879">v</ta>
            <ta e="T1881" id="Seg_1626" s="T1880">conj</ta>
            <ta e="T1882" id="Seg_1627" s="T1881">adv</ta>
            <ta e="T1883" id="Seg_1628" s="T1882">n</ta>
            <ta e="T1884" id="Seg_1629" s="T1883">v</ta>
            <ta e="T1886" id="Seg_1630" s="T1885">adv</ta>
            <ta e="T1887" id="Seg_1631" s="T1886">conj</ta>
            <ta e="T1888" id="Seg_1632" s="T1887">v</ta>
            <ta e="T1889" id="Seg_1633" s="T1888">ptcl</ta>
            <ta e="T1890" id="Seg_1634" s="T1889">n</ta>
            <ta e="T1891" id="Seg_1635" s="T1890">ptcl</ta>
            <ta e="T1892" id="Seg_1636" s="T1891">adv</ta>
            <ta e="T1893" id="Seg_1637" s="T1892">n</ta>
            <ta e="T1894" id="Seg_1638" s="T1893">ptcl</ta>
            <ta e="T1895" id="Seg_1639" s="T1894">v</ta>
            <ta e="T1896" id="Seg_1640" s="T1895">v</ta>
            <ta e="T1897" id="Seg_1641" s="T0">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1766" id="Seg_1642" s="T1765">np.h:A</ta>
            <ta e="T1768" id="Seg_1643" s="T1767">np.h:A</ta>
            <ta e="T1770" id="Seg_1644" s="T1769">np.h:A</ta>
            <ta e="T1772" id="Seg_1645" s="T1771">pro.h:Poss</ta>
            <ta e="T1773" id="Seg_1646" s="T1772">np:Th</ta>
            <ta e="T1776" id="Seg_1647" s="T1775">np.h:A</ta>
            <ta e="T1778" id="Seg_1648" s="T1777">np.h:Poss</ta>
            <ta e="T1779" id="Seg_1649" s="T1778">np:Th</ta>
            <ta e="T1782" id="Seg_1650" s="T1781">pro.h:A</ta>
            <ta e="T1786" id="Seg_1651" s="T1785">np.h:A</ta>
            <ta e="T1787" id="Seg_1652" s="T1786">pro.h:Th</ta>
            <ta e="T1788" id="Seg_1653" s="T1787">np.h:Th</ta>
            <ta e="T1790" id="Seg_1654" s="T1789">pro.h:Th</ta>
            <ta e="T1792" id="Seg_1655" s="T1791">np.h:Th</ta>
            <ta e="T1794" id="Seg_1656" s="T1793">n:Time</ta>
            <ta e="T1795" id="Seg_1657" s="T1794">0.1.h:P</ta>
            <ta e="T1796" id="Seg_1658" s="T1795">0.1.h:P</ta>
            <ta e="T1798" id="Seg_1659" s="T1797">pro.h:A</ta>
            <ta e="T1801" id="Seg_1660" s="T1800">0.3.h:A</ta>
            <ta e="T1802" id="Seg_1661" s="T1801">adv:Time</ta>
            <ta e="T1804" id="Seg_1662" s="T1803">np.h:A</ta>
            <ta e="T1805" id="Seg_1663" s="T1804">pro.h:Th</ta>
            <ta e="T1806" id="Seg_1664" s="T1805">np.h:Th</ta>
            <ta e="T1808" id="Seg_1665" s="T1807">0.1.h:Th</ta>
            <ta e="T1809" id="Seg_1666" s="T1808">np.h:Th</ta>
            <ta e="T1811" id="Seg_1667" s="T1810">adv:Time</ta>
            <ta e="T1813" id="Seg_1668" s="T1812">np.h:Th</ta>
            <ta e="T1814" id="Seg_1669" s="T1813">adv:Time</ta>
            <ta e="T1816" id="Seg_1670" s="T1815">0.3.h:A</ta>
            <ta e="T1817" id="Seg_1671" s="T1816">0.3.h:A</ta>
            <ta e="T1820" id="Seg_1672" s="T1819">np.h:A</ta>
            <ta e="T1821" id="Seg_1673" s="T1820">pro.h:Th</ta>
            <ta e="T1822" id="Seg_1674" s="T1821">np.h:Th</ta>
            <ta e="T1825" id="Seg_1675" s="T1824">pro.h:A</ta>
            <ta e="T1827" id="Seg_1676" s="T1826">np.h:Th</ta>
            <ta e="T1828" id="Seg_1677" s="T1827">adv:Time</ta>
            <ta e="T1829" id="Seg_1678" s="T1828">pro.h:A</ta>
            <ta e="T1835" id="Seg_1679" s="T1834">pro:So</ta>
            <ta e="T1837" id="Seg_1680" s="T1836">0.3.h:A</ta>
            <ta e="T1839" id="Seg_1681" s="T1838">0.3.h:A</ta>
            <ta e="T1842" id="Seg_1682" s="T1841">0.3.h:A</ta>
            <ta e="T1843" id="Seg_1683" s="T1842">np:Th</ta>
            <ta e="T1845" id="Seg_1684" s="T1844">pro.h:E</ta>
            <ta e="T1846" id="Seg_1685" s="T1845">0.3.h:A</ta>
            <ta e="T1847" id="Seg_1686" s="T1846">pro.h:Poss</ta>
            <ta e="T1849" id="Seg_1687" s="T1848">np:Th</ta>
            <ta e="T1851" id="Seg_1688" s="T1850">pro.h:A</ta>
            <ta e="T1855" id="Seg_1689" s="T1854">pro.h:A</ta>
            <ta e="T1857" id="Seg_1690" s="T1856">np:Ins</ta>
            <ta e="T1859" id="Seg_1691" s="T1858">pro.h:P</ta>
            <ta e="T1860" id="Seg_1692" s="T1859">pro.h:A</ta>
            <ta e="T1863" id="Seg_1693" s="T1862">adv:Time</ta>
            <ta e="T1864" id="Seg_1694" s="T1863">0.3.h:A</ta>
            <ta e="T1865" id="Seg_1695" s="T1864">pro.h:A</ta>
            <ta e="T1866" id="Seg_1696" s="T1865">pro.h:R</ta>
            <ta e="T1869" id="Seg_1697" s="T1868">np:Th</ta>
            <ta e="T1870" id="Seg_1698" s="T1869">np.h:Poss</ta>
            <ta e="T1871" id="Seg_1699" s="T1870">pro.h:Poss</ta>
            <ta e="T1873" id="Seg_1700" s="T1872">np:Th</ta>
            <ta e="T1875" id="Seg_1701" s="T1874">np:Ins</ta>
            <ta e="T1876" id="Seg_1702" s="T1875">pro.h:P</ta>
            <ta e="T1877" id="Seg_1703" s="T1876">0.3.h:A</ta>
            <ta e="T1879" id="Seg_1704" s="T1878">np:Th</ta>
            <ta e="T1882" id="Seg_1705" s="T1881">adv:Time</ta>
            <ta e="T1883" id="Seg_1706" s="T1882">np:Ins</ta>
            <ta e="T1884" id="Seg_1707" s="T1883">0.3.h:A</ta>
            <ta e="T1886" id="Seg_1708" s="T1885">adv:Time</ta>
            <ta e="T1890" id="Seg_1709" s="T1889">np:E</ta>
            <ta e="T1892" id="Seg_1710" s="T1891">adv:Time</ta>
            <ta e="T1893" id="Seg_1711" s="T1892">np.h:A</ta>
            <ta e="T1896" id="Seg_1712" s="T1895">0.3.h:A</ta>
            <ta e="T1897" id="Seg_1713" s="T0">0.3.h:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1766" id="Seg_1714" s="T1765">np.h:S</ta>
            <ta e="T1768" id="Seg_1715" s="T1767">np.h:S</ta>
            <ta e="T1769" id="Seg_1716" s="T1768">v:pred</ta>
            <ta e="T1770" id="Seg_1717" s="T1769">np.h:S</ta>
            <ta e="T1771" id="Seg_1718" s="T1770">v:pred</ta>
            <ta e="T1773" id="Seg_1719" s="T1772">np:S</ta>
            <ta e="T1774" id="Seg_1720" s="T1773">adj:pred</ta>
            <ta e="T1776" id="Seg_1721" s="T1775">np.h:S</ta>
            <ta e="T1777" id="Seg_1722" s="T1776">v:pred</ta>
            <ta e="T1779" id="Seg_1723" s="T1778">np:S</ta>
            <ta e="T1781" id="Seg_1724" s="T1780">adj:pred</ta>
            <ta e="T1782" id="Seg_1725" s="T1781">pro.h:S</ta>
            <ta e="T1783" id="Seg_1726" s="T1782">v:pred</ta>
            <ta e="T1784" id="Seg_1727" s="T1783">v:pred</ta>
            <ta e="T1786" id="Seg_1728" s="T1785">np.h:S</ta>
            <ta e="T1787" id="Seg_1729" s="T1786">pro.h:S</ta>
            <ta e="T1788" id="Seg_1730" s="T1787">n:pred</ta>
            <ta e="T1789" id="Seg_1731" s="T1788">ptcl.neg</ta>
            <ta e="T1790" id="Seg_1732" s="T1789">pro.h:S</ta>
            <ta e="T1791" id="Seg_1733" s="T1790">ptcl.neg</ta>
            <ta e="T1792" id="Seg_1734" s="T1791">n:pred</ta>
            <ta e="T1795" id="Seg_1735" s="T1794">v:pred 0.1.h:S</ta>
            <ta e="T1796" id="Seg_1736" s="T1795">v:pred 0.1.h:S</ta>
            <ta e="T1797" id="Seg_1737" s="T1796">n:pred</ta>
            <ta e="T1798" id="Seg_1738" s="T1797">pro.h:S</ta>
            <ta e="T1799" id="Seg_1739" s="T1798">v:pred</ta>
            <ta e="T1801" id="Seg_1740" s="T1800">v:pred 0.3.h:S</ta>
            <ta e="T1803" id="Seg_1741" s="T1802">v:pred </ta>
            <ta e="T1804" id="Seg_1742" s="T1803">np.h:S</ta>
            <ta e="T1805" id="Seg_1743" s="T1804">pro.h:S</ta>
            <ta e="T1806" id="Seg_1744" s="T1805">n:pred</ta>
            <ta e="T1808" id="Seg_1745" s="T1807">cop 0.1.h:S</ta>
            <ta e="T1809" id="Seg_1746" s="T1808">n:pred</ta>
            <ta e="T1812" id="Seg_1747" s="T1811">ptcl.neg</ta>
            <ta e="T1813" id="Seg_1748" s="T1812">n:pred</ta>
            <ta e="T1816" id="Seg_1749" s="T1815">v:pred 0.3.h:S</ta>
            <ta e="T1817" id="Seg_1750" s="T1816">v:pred 0.3.h:S</ta>
            <ta e="T1818" id="Seg_1751" s="T1817">v:pred</ta>
            <ta e="T1820" id="Seg_1752" s="T1819">np.h:S</ta>
            <ta e="T1821" id="Seg_1753" s="T1820">pro.h:S</ta>
            <ta e="T1822" id="Seg_1754" s="T1821">n:pred</ta>
            <ta e="T1825" id="Seg_1755" s="T1824">pro.h:S</ta>
            <ta e="T1826" id="Seg_1756" s="T1825">v:pred</ta>
            <ta e="T1827" id="Seg_1757" s="T1826">n:pred</ta>
            <ta e="T1829" id="Seg_1758" s="T1828">pro.h:S</ta>
            <ta e="T1830" id="Seg_1759" s="T1829">v:pred</ta>
            <ta e="T1836" id="Seg_1760" s="T1835">ptcl.neg</ta>
            <ta e="T1837" id="Seg_1761" s="T1836">v:pred 0.3.h:S</ta>
            <ta e="T1839" id="Seg_1762" s="T1838">v:pred 0.3.h:S</ta>
            <ta e="T1842" id="Seg_1763" s="T1841">v:pred 0.3.h:S</ta>
            <ta e="T1843" id="Seg_1764" s="T1842">np:O</ta>
            <ta e="T1845" id="Seg_1765" s="T1844">pro.h:O</ta>
            <ta e="T1846" id="Seg_1766" s="T1845">v:pred 0.3.h:S</ta>
            <ta e="T1849" id="Seg_1767" s="T1848">np:S</ta>
            <ta e="T1850" id="Seg_1768" s="T1849">v:pred</ta>
            <ta e="T1851" id="Seg_1769" s="T1850">pro.h:S</ta>
            <ta e="T1853" id="Seg_1770" s="T1852">v:pred</ta>
            <ta e="T1855" id="Seg_1771" s="T1854">pro.h:S</ta>
            <ta e="T1858" id="Seg_1772" s="T1857">v:pred</ta>
            <ta e="T1859" id="Seg_1773" s="T1858">pro.h:O</ta>
            <ta e="T1860" id="Seg_1774" s="T1859">pro.h:S</ta>
            <ta e="T1862" id="Seg_1775" s="T1861">v:pred</ta>
            <ta e="T1864" id="Seg_1776" s="T1863">v:pred 0.3.h:S</ta>
            <ta e="T1865" id="Seg_1777" s="T1864">pro.h:S</ta>
            <ta e="T1867" id="Seg_1778" s="T1866">v:pred</ta>
            <ta e="T1868" id="Seg_1779" s="T1867">adj:pred</ta>
            <ta e="T1869" id="Seg_1780" s="T1868">np:S</ta>
            <ta e="T1873" id="Seg_1781" s="T1872">np:S</ta>
            <ta e="T1874" id="Seg_1782" s="T1873">adj:pred</ta>
            <ta e="T1876" id="Seg_1783" s="T1875">pro.h:O</ta>
            <ta e="T1877" id="Seg_1784" s="T1876">v:pred 0.3.h:S</ta>
            <ta e="T1879" id="Seg_1785" s="T1878">np:S</ta>
            <ta e="T1880" id="Seg_1786" s="T1879">v:pred</ta>
            <ta e="T1884" id="Seg_1787" s="T1883">v:pred 0.3.h:S</ta>
            <ta e="T1888" id="Seg_1788" s="T1887">v:pred</ta>
            <ta e="T1890" id="Seg_1789" s="T1889">np:S</ta>
            <ta e="T1893" id="Seg_1790" s="T1892">np.h:S</ta>
            <ta e="T1895" id="Seg_1791" s="T1894">v:pred</ta>
            <ta e="T1896" id="Seg_1792" s="T1895">v:pred 0.3.h:S</ta>
            <ta e="T1897" id="Seg_1793" s="T0">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T1767" id="Seg_1794" s="T1766">RUS:gram</ta>
            <ta e="T1768" id="Seg_1795" s="T1767">RUS:core</ta>
            <ta e="T1769" id="Seg_1796" s="T1768">%TURK:core</ta>
            <ta e="T1775" id="Seg_1797" s="T1774">RUS:gram</ta>
            <ta e="T1776" id="Seg_1798" s="T1775">RUS:core</ta>
            <ta e="T1789" id="Seg_1799" s="T1788">TURK:disc</ta>
            <ta e="T1793" id="Seg_1800" s="T1792">RUS:mod</ta>
            <ta e="T1794" id="Seg_1801" s="T1793">RUS:gram(INDEF)</ta>
            <ta e="T1800" id="Seg_1802" s="T1799">RUS:gram</ta>
            <ta e="T1807" id="Seg_1803" s="T1806">RUS:gram</ta>
            <ta e="T1810" id="Seg_1804" s="T1809">RUS:gram</ta>
            <ta e="T1831" id="Seg_1805" s="T1830">RUS:gram</ta>
            <ta e="T1838" id="Seg_1806" s="T1837">RUS:mod</ta>
            <ta e="T1843" id="Seg_1807" s="T1842">RUS:cult</ta>
            <ta e="T1844" id="Seg_1808" s="T1843">RUS:gram</ta>
            <ta e="T1848" id="Seg_1809" s="T1847">TURK:disc</ta>
            <ta e="T1852" id="Seg_1810" s="T1851">TURK:disc</ta>
            <ta e="T1854" id="Seg_1811" s="T1853">RUS:gram</ta>
            <ta e="T1857" id="Seg_1812" s="T1856">TURK:cult</ta>
            <ta e="T1861" id="Seg_1813" s="T1860">TURK:disc</ta>
            <ta e="T1878" id="Seg_1814" s="T1877">TURK:disc</ta>
            <ta e="T1881" id="Seg_1815" s="T1880">RUS:gram</ta>
            <ta e="T1883" id="Seg_1816" s="T1882">TURK:cult</ta>
            <ta e="T1887" id="Seg_1817" s="T1886">RUS:gram</ta>
            <ta e="T1889" id="Seg_1818" s="T1888">TURK:disc</ta>
            <ta e="T1893" id="Seg_1819" s="T1892">RUS:core</ta>
            <ta e="T1894" id="Seg_1820" s="T1893">TURK:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T1769" id="Seg_1821" s="T1765">Разговаривают медведь и комар.</ta>
            <ta e="T1771" id="Seg_1822" s="T1769">Медведь спрашивает:</ta>
            <ta e="T1774" id="Seg_1823" s="T1771">«Чья кровь [самая?] вкусная?»</ta>
            <ta e="T1781" id="Seg_1824" s="T1774">А комар говорит: «Человеческая кровь очень вкусная».</ta>
            <ta e="T1783" id="Seg_1825" s="T1781">Он пошёл.</ta>
            <ta e="T1786" id="Seg_1826" s="T1783">Идёт маленький мальчик.</ta>
            <ta e="T1788" id="Seg_1827" s="T1786">«Ты человек?»</ta>
            <ta e="T1793" id="Seg_1828" s="T1788">«Нет, я ещё не человек.</ta>
            <ta e="T1797" id="Seg_1829" s="T1793">Когда-нибудь я вырасту, стану человеком».</ta>
            <ta e="T1801" id="Seg_1830" s="T1797">Он [его] оставил и пошёл [дальше].</ta>
            <ta e="T1804" id="Seg_1831" s="T1801">Идёт старик.</ta>
            <ta e="T1806" id="Seg_1832" s="T1804">«Ты человек?»</ta>
            <ta e="T1813" id="Seg_1833" s="T1806">«Да был человеком, а теперь уже не человек».</ta>
            <ta e="T1817" id="Seg_1834" s="T1813">Тогда он снова пошёл, оставил [его].</ta>
            <ta e="T1820" id="Seg_1835" s="T1817">Идёт человек [=мужчина].</ta>
            <ta e="T1824" id="Seg_1836" s="T1820">«Ты человек?»</ta>
            <ta e="T1827" id="Seg_1837" s="T1824">Тот говорит: «Человек».</ta>
            <ta e="T1837" id="Seg_1838" s="T1827">Тогда он [медведь?] идёт к нему, а тот не стоит (на месте?).</ta>
            <ta e="T1839" id="Seg_1839" s="T1837">Тоже идёт [=подходит?].</ta>
            <ta e="T1846" id="Seg_1840" s="T1839">Он взял свою саблю и ударил его.</ta>
            <ta e="T1850" id="Seg_1841" s="T1846">У него кровь течёт.</ta>
            <ta e="T1853" id="Seg_1842" s="T1850">Он убежал.</ta>
            <ta e="T1859" id="Seg_1843" s="T1853">А тот [мужчина] выстрелил в него из ружья.</ta>
            <ta e="T1862" id="Seg_1844" s="T1859">Он убежал.</ta>
            <ta e="T1864" id="Seg_1845" s="T1862">Потом пришёл [к комару].</ta>
            <ta e="T1870" id="Seg_1846" s="T1864">«Ты сказал мне, что у человека кровь вкусная.</ta>
            <ta e="T1874" id="Seg_1847" s="T1870">У него очень длинный язык.</ta>
            <ta e="T1880" id="Seg_1848" s="T1874">Он меня ударил своим языком, кровь течёт.</ta>
            <ta e="T1885" id="Seg_1849" s="T1880">А потом он выстрелил в меня из ружья.</ta>
            <ta e="T1890" id="Seg_1850" s="T1885">Теперь и тело болит».</ta>
            <ta e="T1891" id="Seg_1851" s="T1890">Хватит.</ta>
            <ta e="T1896" id="Seg_1852" s="T1891">Потом комар смеялся, смеялся.</ta>
            <ta e="T1897" id="Seg_1853" s="T1896">[Лопнул и] умер.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T1769" id="Seg_1854" s="T1765">A bear and a mosquito are talking.</ta>
            <ta e="T1771" id="Seg_1855" s="T1769">The bear asks:</ta>
            <ta e="T1774" id="Seg_1856" s="T1771">"Whose blood is sweet (/the sweetest?)?"</ta>
            <ta e="T1781" id="Seg_1857" s="T1774">And the mosquito says: "Man's blood is very sweet."</ta>
            <ta e="T1783" id="Seg_1858" s="T1781">He goes.</ta>
            <ta e="T1786" id="Seg_1859" s="T1783">A small boy comes.</ta>
            <ta e="T1788" id="Seg_1860" s="T1786">"Are you a man?"</ta>
            <ta e="T1793" id="Seg_1861" s="T1788">"No, I am not yet a man.</ta>
            <ta e="T1797" id="Seg_1862" s="T1793">Some day I will grow up, I will become a man."</ta>
            <ta e="T1801" id="Seg_1863" s="T1797">He left [him] and goes [farther].</ta>
            <ta e="T1804" id="Seg_1864" s="T1801">Then an old man comes.</ta>
            <ta e="T1806" id="Seg_1865" s="T1804">"Are you a man?"</ta>
            <ta e="T1813" id="Seg_1866" s="T1806">“Well I was a man, but now I am not a man.”</ta>
            <ta e="T1817" id="Seg_1867" s="T1813">Then again he goes, he left [him].</ta>
            <ta e="T1820" id="Seg_1868" s="T1817">A man comes.</ta>
            <ta e="T1824" id="Seg_1869" s="T1820">"Are you a man?"</ta>
            <ta e="T1827" id="Seg_1870" s="T1824">He says: "A man."</ta>
            <ta e="T1837" id="Seg_1871" s="T1827">Then it [the bear?] goes to him, and he does not stay (there?).</ta>
            <ta e="T1839" id="Seg_1872" s="T1837">He also comes [closer?].</ta>
            <ta e="T1846" id="Seg_1873" s="T1839">He took his saber and hit him [the bear].</ta>
            <ta e="T1850" id="Seg_1874" s="T1846">His blood is running.</ta>
            <ta e="T1853" id="Seg_1875" s="T1850">He ran away.</ta>
            <ta e="T1859" id="Seg_1876" s="T1853">But he [the man] shot at him with a gun.</ta>
            <ta e="T1862" id="Seg_1877" s="T1859">He ran.</ta>
            <ta e="T1864" id="Seg_1878" s="T1862">Then he came [to the mosquito].</ta>
            <ta e="T1870" id="Seg_1879" s="T1864">"You told me, man has sweet blood.</ta>
            <ta e="T1874" id="Seg_1880" s="T1870">He has a very long tongue.</ta>
            <ta e="T1880" id="Seg_1881" s="T1874">With his tongue he stabbed me, my blood is flowing.</ta>
            <ta e="T1885" id="Seg_1882" s="T1880">And then he shot [at me] with a gun.</ta>
            <ta e="T1890" id="Seg_1883" s="T1885">Now my flesh hurts."</ta>
            <ta e="T1891" id="Seg_1884" s="T1890">Enough.</ta>
            <ta e="T1896" id="Seg_1885" s="T1891">Then the mosquito laughed, laughed.</ta>
            <ta e="T1897" id="Seg_1886" s="T1896">It [burst and] died.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T1769" id="Seg_1887" s="T1765">Ein Bär und eine Mücke reden.</ta>
            <ta e="T1771" id="Seg_1888" s="T1769">Der Bär fragt:</ta>
            <ta e="T1774" id="Seg_1889" s="T1771">„Wessen Blut ist süß (/am süßesten?)?“</ta>
            <ta e="T1781" id="Seg_1890" s="T1774">Und die Mücke sagt: „Mannsblut ist sehr süß.“</ta>
            <ta e="T1783" id="Seg_1891" s="T1781">Er geht.</ta>
            <ta e="T1786" id="Seg_1892" s="T1783">Ein kleiner Junge kommt.</ta>
            <ta e="T1788" id="Seg_1893" s="T1786">„Bist du ein Mann?“</ta>
            <ta e="T1793" id="Seg_1894" s="T1788">„Nein, ich bin noch kein Mann.</ta>
            <ta e="T1797" id="Seg_1895" s="T1793">Eines Tages werde ich aufwachsen, und werde zum Mann werden.“</ta>
            <ta e="T1801" id="Seg_1896" s="T1797">Er verlies [ihn] und geht [weiter].</ta>
            <ta e="T1804" id="Seg_1897" s="T1801">Dann kommt ein alter Mann.</ta>
            <ta e="T1806" id="Seg_1898" s="T1804">„Bist du ein Mann?“</ta>
            <ta e="T1813" id="Seg_1899" s="T1806">„Ich war ein Mann, doch jetzt bin ich keinen Mann.“</ta>
            <ta e="T1817" id="Seg_1900" s="T1813">Dann geht er wieder, er verlies [ihn].</ta>
            <ta e="T1820" id="Seg_1901" s="T1817">Ein Mann kommt.</ta>
            <ta e="T1824" id="Seg_1902" s="T1820">„Bist du ein Mann?“</ta>
            <ta e="T1827" id="Seg_1903" s="T1824">Er sagt: „Ein Mann.“</ta>
            <ta e="T1837" id="Seg_1904" s="T1827">Dann ging er [der Bär?] auf ihn zu, und er bleibt nicht (dort?).</ta>
            <ta e="T1839" id="Seg_1905" s="T1837">Er kommt auch [näher].</ta>
            <ta e="T1846" id="Seg_1906" s="T1839">Er nahm seinen Säbel und schlug ihn.</ta>
            <ta e="T1850" id="Seg_1907" s="T1846">Sein Blut läuft.</ta>
            <ta e="T1853" id="Seg_1908" s="T1850">Er rannte fort.</ta>
            <ta e="T1859" id="Seg_1909" s="T1853">Aber er schoss ihn mit einem Gewehr.</ta>
            <ta e="T1862" id="Seg_1910" s="T1859">Er rannte.</ta>
            <ta e="T1864" id="Seg_1911" s="T1862">Dann kam er [zur Mücke].</ta>
            <ta e="T1870" id="Seg_1912" s="T1864">„Du erzähltest mir, der Mann hat süßes Blut.</ta>
            <ta e="T1874" id="Seg_1913" s="T1870">Er hat einen sehr lange Zunge.</ta>
            <ta e="T1880" id="Seg_1914" s="T1874">Mit seiner Zunge stach er mich, mein Blut fließt.</ta>
            <ta e="T1885" id="Seg_1915" s="T1880">Und dann schoss er [nach mir] mit einem Gewehr.</ta>
            <ta e="T1890" id="Seg_1916" s="T1885">Nun schmerzt mein Fleisch.“</ta>
            <ta e="T1891" id="Seg_1917" s="T1890">Genug.</ta>
            <ta e="T1896" id="Seg_1918" s="T1891">Dann lachte die Mücke, lachte.</ta>
            <ta e="T1897" id="Seg_1919" s="T1896">Sie [platzte und] starb.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T1769" id="Seg_1920" s="T1765">[GVY:] A slovak tale, see http://www.planetaskazok.ru/slavyanskieskz/komarimedvedslovtskz</ta>
            <ta e="T1783" id="Seg_1921" s="T1781">[AAV] The bear went to find a man.</ta>
            <ta e="T1846" id="Seg_1922" s="T1839">[KlT:] Сабля ’saber’.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1765" />
            <conversion-tli id="T1766" />
            <conversion-tli id="T1767" />
            <conversion-tli id="T1768" />
            <conversion-tli id="T1769" />
            <conversion-tli id="T1770" />
            <conversion-tli id="T1771" />
            <conversion-tli id="T1772" />
            <conversion-tli id="T1773" />
            <conversion-tli id="T1774" />
            <conversion-tli id="T1775" />
            <conversion-tli id="T1776" />
            <conversion-tli id="T1777" />
            <conversion-tli id="T1778" />
            <conversion-tli id="T1779" />
            <conversion-tli id="T1780" />
            <conversion-tli id="T1781" />
            <conversion-tli id="T1782" />
            <conversion-tli id="T1783" />
            <conversion-tli id="T1784" />
            <conversion-tli id="T1785" />
            <conversion-tli id="T1786" />
            <conversion-tli id="T1787" />
            <conversion-tli id="T1788" />
            <conversion-tli id="T1789" />
            <conversion-tli id="T1790" />
            <conversion-tli id="T1791" />
            <conversion-tli id="T1792" />
            <conversion-tli id="T1793" />
            <conversion-tli id="T1794" />
            <conversion-tli id="T1795" />
            <conversion-tli id="T1796" />
            <conversion-tli id="T1797" />
            <conversion-tli id="T1798" />
            <conversion-tli id="T1799" />
            <conversion-tli id="T1800" />
            <conversion-tli id="T1801" />
            <conversion-tli id="T1802" />
            <conversion-tli id="T1803" />
            <conversion-tli id="T1804" />
            <conversion-tli id="T1805" />
            <conversion-tli id="T1806" />
            <conversion-tli id="T1807" />
            <conversion-tli id="T1808" />
            <conversion-tli id="T1809" />
            <conversion-tli id="T1810" />
            <conversion-tli id="T1811" />
            <conversion-tli id="T1812" />
            <conversion-tli id="T1813" />
            <conversion-tli id="T1814" />
            <conversion-tli id="T1815" />
            <conversion-tli id="T1816" />
            <conversion-tli id="T1817" />
            <conversion-tli id="T1818" />
            <conversion-tli id="T1819" />
            <conversion-tli id="T1820" />
            <conversion-tli id="T1821" />
            <conversion-tli id="T1822" />
            <conversion-tli id="T1823" />
            <conversion-tli id="T1824" />
            <conversion-tli id="T1825" />
            <conversion-tli id="T1826" />
            <conversion-tli id="T1827" />
            <conversion-tli id="T1828" />
            <conversion-tli id="T1829" />
            <conversion-tli id="T1830" />
            <conversion-tli id="T1831" />
            <conversion-tli id="T1832" />
            <conversion-tli id="T1833" />
            <conversion-tli id="T1834" />
            <conversion-tli id="T1835" />
            <conversion-tli id="T1836" />
            <conversion-tli id="T1837" />
            <conversion-tli id="T1838" />
            <conversion-tli id="T1839" />
            <conversion-tli id="T1840" />
            <conversion-tli id="T1841" />
            <conversion-tli id="T1842" />
            <conversion-tli id="T1843" />
            <conversion-tli id="T1844" />
            <conversion-tli id="T1845" />
            <conversion-tli id="T1846" />
            <conversion-tli id="T1847" />
            <conversion-tli id="T1848" />
            <conversion-tli id="T1849" />
            <conversion-tli id="T1850" />
            <conversion-tli id="T1851" />
            <conversion-tli id="T1852" />
            <conversion-tli id="T1853" />
            <conversion-tli id="T1854" />
            <conversion-tli id="T1855" />
            <conversion-tli id="T1856" />
            <conversion-tli id="T1857" />
            <conversion-tli id="T1858" />
            <conversion-tli id="T1859" />
            <conversion-tli id="T1860" />
            <conversion-tli id="T1861" />
            <conversion-tli id="T1862" />
            <conversion-tli id="T1863" />
            <conversion-tli id="T1864" />
            <conversion-tli id="T1865" />
            <conversion-tli id="T1866" />
            <conversion-tli id="T1867" />
            <conversion-tli id="T1868" />
            <conversion-tli id="T1869" />
            <conversion-tli id="T1870" />
            <conversion-tli id="T1871" />
            <conversion-tli id="T1872" />
            <conversion-tli id="T1873" />
            <conversion-tli id="T1874" />
            <conversion-tli id="T1875" />
            <conversion-tli id="T1876" />
            <conversion-tli id="T1877" />
            <conversion-tli id="T1878" />
            <conversion-tli id="T1879" />
            <conversion-tli id="T1880" />
            <conversion-tli id="T1881" />
            <conversion-tli id="T1882" />
            <conversion-tli id="T1883" />
            <conversion-tli id="T1884" />
            <conversion-tli id="T1885" />
            <conversion-tli id="T1886" />
            <conversion-tli id="T1887" />
            <conversion-tli id="T1888" />
            <conversion-tli id="T1889" />
            <conversion-tli id="T1890" />
            <conversion-tli id="T1891" />
            <conversion-tli id="T1892" />
            <conversion-tli id="T1893" />
            <conversion-tli id="T1894" />
            <conversion-tli id="T1895" />
            <conversion-tli id="T1896" />
            <conversion-tli id="T0" />
            <conversion-tli id="T1897" />
            <conversion-tli id="T1898" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
