<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID6E3EC7D9-E425-10AF-A11F-CF94AD6E4B71">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\AA_1914_Hare_flk\AA_1914_Hare_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">160</ud-information>
            <ud-information attribute-name="# HIAT:w">97</ud-information>
            <ud-information attribute-name="# e">97</ud-information>
            <ud-information attribute-name="# HIAT:u">22</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="AA">
            <abbreviation>AA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="AA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T97" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Kozan</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">kandəbi</ts>
                  <nts id="Seg_8" n="HIAT:ip">,</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_11" n="HIAT:w" s="T2">kandəbi</ts>
                  <nts id="Seg_12" n="HIAT:ip">.</nts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_15" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_17" n="HIAT:w" s="T3">Nugurbi</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_20" n="HIAT:w" s="T4">toʔbdobi</ts>
                  <nts id="Seg_21" n="HIAT:ip">,</nts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_24" n="HIAT:w" s="T5">nugurbinə</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_27" n="HIAT:w" s="T6">püjebə</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_30" n="HIAT:w" s="T7">băppi</ts>
                  <nts id="Seg_31" n="HIAT:ip">.</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_34" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_36" n="HIAT:w" s="T8">Dĭgəttə</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_39" n="HIAT:w" s="T9">kambi</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_42" n="HIAT:w" s="T10">neni</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_45" n="HIAT:w" s="T11">šünə</ts>
                  <nts id="Seg_46" n="HIAT:ip">.</nts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_49" n="HIAT:u" s="T12">
                  <nts id="Seg_50" n="HIAT:ip">"</nts>
                  <ts e="T13" id="Seg_52" n="HIAT:w" s="T12">Neni</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_55" n="HIAT:w" s="T13">šü</ts>
                  <nts id="Seg_56" n="HIAT:ip">,</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_59" n="HIAT:w" s="T14">amdə</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_62" n="HIAT:w" s="T15">dĭ</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_65" n="HIAT:w" s="T16">nugurbin</ts>
                  <nts id="Seg_66" n="HIAT:ip">!</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_69" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_71" n="HIAT:w" s="T17">Nendit</ts>
                  <nts id="Seg_72" n="HIAT:ip">,</nts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_75" n="HIAT:w" s="T18">bar</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_78" n="HIAT:w" s="T19">amdə</ts>
                  <nts id="Seg_79" n="HIAT:ip">!</nts>
                  <nts id="Seg_80" n="HIAT:ip">"</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_83" n="HIAT:u" s="T20">
                  <nts id="Seg_84" n="HIAT:ip">"</nts>
                  <ts e="T21" id="Seg_86" n="HIAT:w" s="T20">Măn</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_89" n="HIAT:w" s="T21">dĭžəttə</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_92" n="HIAT:w" s="T22">amozʼət</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_95" n="HIAT:w" s="T23">tʼüm</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_98" n="HIAT:w" s="T24">iʔgö</ts>
                  <nts id="Seg_99" n="HIAT:ip">.</nts>
                  <nts id="Seg_100" n="HIAT:ip">"</nts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_103" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_105" n="HIAT:w" s="T25">Dĭgəttə</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_108" n="HIAT:w" s="T26">kambi</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_111" n="HIAT:w" s="T27">bünə</ts>
                  <nts id="Seg_112" n="HIAT:ip">.</nts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_115" n="HIAT:u" s="T28">
                  <nts id="Seg_116" n="HIAT:ip">"</nts>
                  <ts e="T29" id="Seg_118" n="HIAT:w" s="T28">Bü</ts>
                  <nts id="Seg_119" n="HIAT:ip">,</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_122" n="HIAT:w" s="T29">bü</ts>
                  <nts id="Seg_123" n="HIAT:ip">,</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_126" n="HIAT:w" s="T30">küʔbdərdə</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_129" n="HIAT:w" s="T31">neni</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_132" n="HIAT:w" s="T32">šü</ts>
                  <nts id="Seg_133" n="HIAT:ip">!</nts>
                  <nts id="Seg_134" n="HIAT:ip">"</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_137" n="HIAT:u" s="T33">
                  <nts id="Seg_138" n="HIAT:ip">"</nts>
                  <ts e="T34" id="Seg_140" n="HIAT:w" s="T33">Măn</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_143" n="HIAT:w" s="T34">dărgoda</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_146" n="HIAT:w" s="T35">mʼaŋzʼət</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_149" n="HIAT:w" s="T36">tʼüm</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_152" n="HIAT:w" s="T37">iʔgö</ts>
                  <nts id="Seg_153" n="HIAT:ip">.</nts>
                  <nts id="Seg_154" n="HIAT:ip">"</nts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_157" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_159" n="HIAT:w" s="T38">Bulandə</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_162" n="HIAT:w" s="T39">kambi</ts>
                  <nts id="Seg_163" n="HIAT:ip">.</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_166" n="HIAT:u" s="T40">
                  <nts id="Seg_167" n="HIAT:ip">"</nts>
                  <ts e="T41" id="Seg_169" n="HIAT:w" s="T40">Bulan</ts>
                  <nts id="Seg_170" n="HIAT:ip">,</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_173" n="HIAT:w" s="T41">bulan</ts>
                  <nts id="Seg_174" n="HIAT:ip">,</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_177" n="HIAT:w" s="T42">büʔjəm</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_180" n="HIAT:w" s="T43">bar</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_183" n="HIAT:w" s="T44">bĭttə</ts>
                  <nts id="Seg_184" n="HIAT:ip">!</nts>
                  <nts id="Seg_185" n="HIAT:ip">"</nts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_188" n="HIAT:u" s="T45">
                  <nts id="Seg_189" n="HIAT:ip">"</nts>
                  <ts e="T46" id="Seg_191" n="HIAT:w" s="T45">Măn</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_194" n="HIAT:w" s="T46">üjünə</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_197" n="HIAT:w" s="T47">jilgəndə</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_200" n="HIAT:w" s="T48">büjəʔ</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_203" n="HIAT:w" s="T49">bĭtleʔbəliem</ts>
                  <nts id="Seg_204" n="HIAT:ip">.</nts>
                  <nts id="Seg_205" n="HIAT:ip">"</nts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_208" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_210" n="HIAT:w" s="T50">Dĭgəttə</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_213" n="HIAT:w" s="T51">kambi</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_216" n="HIAT:w" s="T52">tʼaktə</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_219" n="HIAT:w" s="T53">nükeinə</ts>
                  <nts id="Seg_220" n="HIAT:ip">.</nts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_223" n="HIAT:u" s="T54">
                  <nts id="Seg_224" n="HIAT:ip">"</nts>
                  <ts e="T55" id="Seg_226" n="HIAT:w" s="T54">Tʼaktə</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_229" n="HIAT:w" s="T55">nükeiʔ</ts>
                  <nts id="Seg_230" n="HIAT:ip">,</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_233" n="HIAT:w" s="T56">kallaʔ</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_236" n="HIAT:w" s="T57">bulanən</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_239" n="HIAT:w" s="T58">tenzeŋdə</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_242" n="HIAT:w" s="T59">săbəjgut</ts>
                  <nts id="Seg_243" n="HIAT:ip">!</nts>
                  <nts id="Seg_244" n="HIAT:ip">"</nts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_247" n="HIAT:u" s="T60">
                  <nts id="Seg_248" n="HIAT:ip">"</nts>
                  <ts e="T61" id="Seg_250" n="HIAT:w" s="T60">Mĭʔnʼibeʔ</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_253" n="HIAT:w" s="T61">ej</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_256" n="HIAT:w" s="T62">kereʔ</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_259" n="HIAT:w" s="T63">bulanən</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_262" n="HIAT:w" s="T64">tendə</ts>
                  <nts id="Seg_263" n="HIAT:ip">,</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_266" n="HIAT:w" s="T65">kašpoʔ</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_269" n="HIAT:w" s="T66">mĭʔ</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_272" n="HIAT:w" s="T67">bosta</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_275" n="HIAT:w" s="T68">tendə</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_278" n="HIAT:w" s="T69">iʔgö</ts>
                  <nts id="Seg_279" n="HIAT:ip">.</nts>
                  <nts id="Seg_280" n="HIAT:ip">"</nts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_283" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_285" n="HIAT:w" s="T70">Tumoiʔnə</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_288" n="HIAT:w" s="T71">kambi</ts>
                  <nts id="Seg_289" n="HIAT:ip">.</nts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_292" n="HIAT:u" s="T72">
                  <nts id="Seg_293" n="HIAT:ip">"</nts>
                  <ts e="T73" id="Seg_295" n="HIAT:w" s="T72">Tumoiʔ</ts>
                  <nts id="Seg_296" n="HIAT:ip">"</nts>
                  <nts id="Seg_297" n="HIAT:ip">,</nts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_300" n="HIAT:w" s="T73">mălia</ts>
                  <nts id="Seg_301" n="HIAT:ip">.</nts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_304" n="HIAT:u" s="T74">
                  <nts id="Seg_305" n="HIAT:ip">"</nts>
                  <ts e="T75" id="Seg_307" n="HIAT:w" s="T74">Tumoiʔ</ts>
                  <nts id="Seg_308" n="HIAT:ip">,</nts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_311" n="HIAT:w" s="T75">kaŋgaʔ</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_314" n="HIAT:w" s="T76">tʼaktə</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_317" n="HIAT:w" s="T77">nükeiʔnə</ts>
                  <nts id="Seg_318" n="HIAT:ip">,</nts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_321" n="HIAT:w" s="T78">tendən</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_324" n="HIAT:w" s="T79">bar</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_327" n="HIAT:w" s="T80">amgut</ts>
                  <nts id="Seg_328" n="HIAT:ip">!</nts>
                  <nts id="Seg_329" n="HIAT:ip">"</nts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_332" n="HIAT:u" s="T81">
                  <nts id="Seg_333" n="HIAT:ip">"</nts>
                  <ts e="T82" id="Seg_335" n="HIAT:w" s="T81">Miʔ</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_338" n="HIAT:w" s="T82">bostadən</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_341" n="HIAT:w" s="T83">nodən</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_344" n="HIAT:w" s="T84">tamər</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_347" n="HIAT:w" s="T85">amniabaʔ</ts>
                  <nts id="Seg_348" n="HIAT:ip">.</nts>
                  <nts id="Seg_349" n="HIAT:ip">"</nts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_352" n="HIAT:u" s="T86">
                  <ts e="T87" id="Seg_354" n="HIAT:w" s="T86">Kambi</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_357" n="HIAT:w" s="T87">nʼizeŋdə</ts>
                  <nts id="Seg_358" n="HIAT:ip">.</nts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_361" n="HIAT:u" s="T88">
                  <nts id="Seg_362" n="HIAT:ip">"</nts>
                  <ts e="T89" id="Seg_364" n="HIAT:w" s="T88">Nʼizeŋ</ts>
                  <nts id="Seg_365" n="HIAT:ip">,</nts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_368" n="HIAT:w" s="T89">nʼizeŋ</ts>
                  <nts id="Seg_369" n="HIAT:ip">,</nts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_372" n="HIAT:w" s="T90">kallaʔ</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_375" n="HIAT:w" s="T91">kukkut</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_378" n="HIAT:w" s="T92">tumoim</ts>
                  <nts id="Seg_379" n="HIAT:ip">!</nts>
                  <nts id="Seg_380" n="HIAT:ip">"</nts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_383" n="HIAT:u" s="T93">
                  <nts id="Seg_384" n="HIAT:ip">"</nts>
                  <ts e="T94" id="Seg_386" n="HIAT:w" s="T93">Me</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_389" n="HIAT:w" s="T94">bospaʔ</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_392" n="HIAT:w" s="T95">kadʼəksʼəʔ</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_395" n="HIAT:w" s="T96">sʼarlaʔbəbeʔ</ts>
                  <nts id="Seg_396" n="HIAT:ip">.</nts>
                  <nts id="Seg_397" n="HIAT:ip">"</nts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T97" id="Seg_399" n="sc" s="T0">
               <ts e="T1" id="Seg_401" n="e" s="T0">Kozan </ts>
               <ts e="T2" id="Seg_403" n="e" s="T1">kandəbi, </ts>
               <ts e="T3" id="Seg_405" n="e" s="T2">kandəbi. </ts>
               <ts e="T4" id="Seg_407" n="e" s="T3">Nugurbi </ts>
               <ts e="T5" id="Seg_409" n="e" s="T4">toʔbdobi, </ts>
               <ts e="T6" id="Seg_411" n="e" s="T5">nugurbinə </ts>
               <ts e="T7" id="Seg_413" n="e" s="T6">püjebə </ts>
               <ts e="T8" id="Seg_415" n="e" s="T7">băppi. </ts>
               <ts e="T9" id="Seg_417" n="e" s="T8">Dĭgəttə </ts>
               <ts e="T10" id="Seg_419" n="e" s="T9">kambi </ts>
               <ts e="T11" id="Seg_421" n="e" s="T10">neni </ts>
               <ts e="T12" id="Seg_423" n="e" s="T11">šünə. </ts>
               <ts e="T13" id="Seg_425" n="e" s="T12">"Neni </ts>
               <ts e="T14" id="Seg_427" n="e" s="T13">šü, </ts>
               <ts e="T15" id="Seg_429" n="e" s="T14">amdə </ts>
               <ts e="T16" id="Seg_431" n="e" s="T15">dĭ </ts>
               <ts e="T17" id="Seg_433" n="e" s="T16">nugurbin! </ts>
               <ts e="T18" id="Seg_435" n="e" s="T17">Nendit, </ts>
               <ts e="T19" id="Seg_437" n="e" s="T18">bar </ts>
               <ts e="T20" id="Seg_439" n="e" s="T19">amdə!" </ts>
               <ts e="T21" id="Seg_441" n="e" s="T20">"Măn </ts>
               <ts e="T22" id="Seg_443" n="e" s="T21">dĭžəttə </ts>
               <ts e="T23" id="Seg_445" n="e" s="T22">amozʼət </ts>
               <ts e="T24" id="Seg_447" n="e" s="T23">tʼüm </ts>
               <ts e="T25" id="Seg_449" n="e" s="T24">iʔgö." </ts>
               <ts e="T26" id="Seg_451" n="e" s="T25">Dĭgəttə </ts>
               <ts e="T27" id="Seg_453" n="e" s="T26">kambi </ts>
               <ts e="T28" id="Seg_455" n="e" s="T27">bünə. </ts>
               <ts e="T29" id="Seg_457" n="e" s="T28">"Bü, </ts>
               <ts e="T30" id="Seg_459" n="e" s="T29">bü, </ts>
               <ts e="T31" id="Seg_461" n="e" s="T30">küʔbdərdə </ts>
               <ts e="T32" id="Seg_463" n="e" s="T31">neni </ts>
               <ts e="T33" id="Seg_465" n="e" s="T32">šü!" </ts>
               <ts e="T34" id="Seg_467" n="e" s="T33">"Măn </ts>
               <ts e="T35" id="Seg_469" n="e" s="T34">dărgoda </ts>
               <ts e="T36" id="Seg_471" n="e" s="T35">mʼaŋzʼət </ts>
               <ts e="T37" id="Seg_473" n="e" s="T36">tʼüm </ts>
               <ts e="T38" id="Seg_475" n="e" s="T37">iʔgö." </ts>
               <ts e="T39" id="Seg_477" n="e" s="T38">Bulandə </ts>
               <ts e="T40" id="Seg_479" n="e" s="T39">kambi. </ts>
               <ts e="T41" id="Seg_481" n="e" s="T40">"Bulan, </ts>
               <ts e="T42" id="Seg_483" n="e" s="T41">bulan, </ts>
               <ts e="T43" id="Seg_485" n="e" s="T42">büʔjəm </ts>
               <ts e="T44" id="Seg_487" n="e" s="T43">bar </ts>
               <ts e="T45" id="Seg_489" n="e" s="T44">bĭttə!" </ts>
               <ts e="T46" id="Seg_491" n="e" s="T45">"Măn </ts>
               <ts e="T47" id="Seg_493" n="e" s="T46">üjünə </ts>
               <ts e="T48" id="Seg_495" n="e" s="T47">jilgəndə </ts>
               <ts e="T49" id="Seg_497" n="e" s="T48">büjəʔ </ts>
               <ts e="T50" id="Seg_499" n="e" s="T49">bĭtleʔbəliem." </ts>
               <ts e="T51" id="Seg_501" n="e" s="T50">Dĭgəttə </ts>
               <ts e="T52" id="Seg_503" n="e" s="T51">kambi </ts>
               <ts e="T53" id="Seg_505" n="e" s="T52">tʼaktə </ts>
               <ts e="T54" id="Seg_507" n="e" s="T53">nükeinə. </ts>
               <ts e="T55" id="Seg_509" n="e" s="T54">"Tʼaktə </ts>
               <ts e="T56" id="Seg_511" n="e" s="T55">nükeiʔ, </ts>
               <ts e="T57" id="Seg_513" n="e" s="T56">kallaʔ </ts>
               <ts e="T58" id="Seg_515" n="e" s="T57">bulanən </ts>
               <ts e="T59" id="Seg_517" n="e" s="T58">tenzeŋdə </ts>
               <ts e="T60" id="Seg_519" n="e" s="T59">săbəjgut!" </ts>
               <ts e="T61" id="Seg_521" n="e" s="T60">"Mĭʔnʼibeʔ </ts>
               <ts e="T62" id="Seg_523" n="e" s="T61">ej </ts>
               <ts e="T63" id="Seg_525" n="e" s="T62">kereʔ </ts>
               <ts e="T64" id="Seg_527" n="e" s="T63">bulanən </ts>
               <ts e="T65" id="Seg_529" n="e" s="T64">tendə, </ts>
               <ts e="T66" id="Seg_531" n="e" s="T65">kašpoʔ </ts>
               <ts e="T67" id="Seg_533" n="e" s="T66">mĭʔ </ts>
               <ts e="T68" id="Seg_535" n="e" s="T67">bosta </ts>
               <ts e="T69" id="Seg_537" n="e" s="T68">tendə </ts>
               <ts e="T70" id="Seg_539" n="e" s="T69">iʔgö." </ts>
               <ts e="T71" id="Seg_541" n="e" s="T70">Tumoiʔnə </ts>
               <ts e="T72" id="Seg_543" n="e" s="T71">kambi. </ts>
               <ts e="T73" id="Seg_545" n="e" s="T72">"Tumoiʔ", </ts>
               <ts e="T74" id="Seg_547" n="e" s="T73">mălia. </ts>
               <ts e="T75" id="Seg_549" n="e" s="T74">"Tumoiʔ, </ts>
               <ts e="T76" id="Seg_551" n="e" s="T75">kaŋgaʔ </ts>
               <ts e="T77" id="Seg_553" n="e" s="T76">tʼaktə </ts>
               <ts e="T78" id="Seg_555" n="e" s="T77">nükeiʔnə, </ts>
               <ts e="T79" id="Seg_557" n="e" s="T78">tendən </ts>
               <ts e="T80" id="Seg_559" n="e" s="T79">bar </ts>
               <ts e="T81" id="Seg_561" n="e" s="T80">amgut!" </ts>
               <ts e="T82" id="Seg_563" n="e" s="T81">"Miʔ </ts>
               <ts e="T83" id="Seg_565" n="e" s="T82">bostadən </ts>
               <ts e="T84" id="Seg_567" n="e" s="T83">nodən </ts>
               <ts e="T85" id="Seg_569" n="e" s="T84">tamər </ts>
               <ts e="T86" id="Seg_571" n="e" s="T85">amniabaʔ." </ts>
               <ts e="T87" id="Seg_573" n="e" s="T86">Kambi </ts>
               <ts e="T88" id="Seg_575" n="e" s="T87">nʼizeŋdə. </ts>
               <ts e="T89" id="Seg_577" n="e" s="T88">"Nʼizeŋ, </ts>
               <ts e="T90" id="Seg_579" n="e" s="T89">nʼizeŋ, </ts>
               <ts e="T91" id="Seg_581" n="e" s="T90">kallaʔ </ts>
               <ts e="T92" id="Seg_583" n="e" s="T91">kukkut </ts>
               <ts e="T93" id="Seg_585" n="e" s="T92">tumoim!" </ts>
               <ts e="T94" id="Seg_587" n="e" s="T93">"Me </ts>
               <ts e="T95" id="Seg_589" n="e" s="T94">bospaʔ </ts>
               <ts e="T96" id="Seg_591" n="e" s="T95">kadʼəksʼəʔ </ts>
               <ts e="T97" id="Seg_593" n="e" s="T96">sʼarlaʔbəbeʔ." </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_594" s="T0">AA_1914_Hare_flk.001 (001.001)</ta>
            <ta e="T8" id="Seg_595" s="T3">AA_1914_Hare_flk.002 (001.002)</ta>
            <ta e="T12" id="Seg_596" s="T8">AA_1914_Hare_flk.003 (001.003)</ta>
            <ta e="T17" id="Seg_597" s="T12">AA_1914_Hare_flk.004 (001.004)</ta>
            <ta e="T20" id="Seg_598" s="T17">AA_1914_Hare_flk.005 (001.005)</ta>
            <ta e="T25" id="Seg_599" s="T20">AA_1914_Hare_flk.006 (001.006)</ta>
            <ta e="T28" id="Seg_600" s="T25">AA_1914_Hare_flk.007 (001.007)</ta>
            <ta e="T33" id="Seg_601" s="T28">AA_1914_Hare_flk.008 (001.008)</ta>
            <ta e="T38" id="Seg_602" s="T33">AA_1914_Hare_flk.009 (001.009)</ta>
            <ta e="T40" id="Seg_603" s="T38">AA_1914_Hare_flk.010 (001.010)</ta>
            <ta e="T45" id="Seg_604" s="T40">AA_1914_Hare_flk.011 (001.011)</ta>
            <ta e="T50" id="Seg_605" s="T45">AA_1914_Hare_flk.012 (001.012)</ta>
            <ta e="T54" id="Seg_606" s="T50">AA_1914_Hare_flk.013 (001.013)</ta>
            <ta e="T60" id="Seg_607" s="T54">AA_1914_Hare_flk.014 (001.014)</ta>
            <ta e="T70" id="Seg_608" s="T60">AA_1914_Hare_flk.015 (001.015)</ta>
            <ta e="T72" id="Seg_609" s="T70">AA_1914_Hare_flk.016 (001.016)</ta>
            <ta e="T74" id="Seg_610" s="T72">AA_1914_Hare_flk.017 (001.017)</ta>
            <ta e="T81" id="Seg_611" s="T74">AA_1914_Hare_flk.018 (001.018)</ta>
            <ta e="T86" id="Seg_612" s="T81">AA_1914_Hare_flk.019 (001.019)</ta>
            <ta e="T88" id="Seg_613" s="T86">AA_1914_Hare_flk.020 (001.020)</ta>
            <ta e="T93" id="Seg_614" s="T88">AA_1914_Hare_flk.021 (001.021)</ta>
            <ta e="T97" id="Seg_615" s="T93">AA_1914_Hare_flk.022 (001.022)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_616" s="T0">Kozan kandəbi, kandəbi. </ta>
            <ta e="T8" id="Seg_617" s="T3">Nugurbi toʔbdobi, nugurbinə püjebə băppi. </ta>
            <ta e="T12" id="Seg_618" s="T8">Dĭgəttə kambi neni šünə. </ta>
            <ta e="T17" id="Seg_619" s="T12">"Neni šü, amdə dĭ nugurbin! </ta>
            <ta e="T20" id="Seg_620" s="T17">Nendit, bar amdə!" </ta>
            <ta e="T25" id="Seg_621" s="T20">"Măn dĭžəttə amozʼət tʼüm iʔgö." </ta>
            <ta e="T28" id="Seg_622" s="T25">Dĭgəttə kambi bünə. </ta>
            <ta e="T33" id="Seg_623" s="T28">"Bü, bü, küʔbdərdə neni šü!" </ta>
            <ta e="T38" id="Seg_624" s="T33">"Măn dărgoda mʼaŋzʼət tʼüm iʔgö." </ta>
            <ta e="T40" id="Seg_625" s="T38">Bulandə kambi. </ta>
            <ta e="T45" id="Seg_626" s="T40">"Bulan, bulan, büʔjəm bar bĭttə!" </ta>
            <ta e="T50" id="Seg_627" s="T45">"Măn üjünə jilgəndə büjəʔ bĭtleʔbəliem." </ta>
            <ta e="T54" id="Seg_628" s="T50">Dĭgəttə kambi tʼaktə nükeinə. </ta>
            <ta e="T60" id="Seg_629" s="T54">"Tʼaktə nükeiʔ, kallaʔ bulanən tenzeŋdə săbəjgut!" </ta>
            <ta e="T70" id="Seg_630" s="T60">"Mĭʔnʼibeʔ ej kereʔ bulanən tendə, kašpoʔ mĭʔ bosta tendə iʔgö." </ta>
            <ta e="T72" id="Seg_631" s="T70">Tumoiʔnə kambi. </ta>
            <ta e="T74" id="Seg_632" s="T72">"Tumoiʔ", mălia. </ta>
            <ta e="T81" id="Seg_633" s="T74">"Tumoiʔ, kaŋgaʔ tʼaktə nükeiʔnə, tendən bar amgut!" </ta>
            <ta e="T86" id="Seg_634" s="T81">"Miʔ bostadən nodən tamər amniabaʔ." </ta>
            <ta e="T88" id="Seg_635" s="T86">Kambi nʼizeŋdə. </ta>
            <ta e="T93" id="Seg_636" s="T88">"Nʼizeŋ, nʼizeŋ, kallaʔ kukkut tumoim!" </ta>
            <ta e="T97" id="Seg_637" s="T93">"Me bospaʔ kadʼəksʼəʔ sʼarlaʔbəbeʔ." </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_638" s="T0">kozan</ta>
            <ta e="T2" id="Seg_639" s="T1">kandə-bi</ta>
            <ta e="T3" id="Seg_640" s="T2">kandə-bi</ta>
            <ta e="T4" id="Seg_641" s="T3">nugurbi</ta>
            <ta e="T5" id="Seg_642" s="T4">toʔbdo-bi</ta>
            <ta e="T6" id="Seg_643" s="T5">nugurbi-nə</ta>
            <ta e="T7" id="Seg_644" s="T6">püje-bə</ta>
            <ta e="T8" id="Seg_645" s="T7">băp-pi</ta>
            <ta e="T9" id="Seg_646" s="T8">dĭgəttə</ta>
            <ta e="T10" id="Seg_647" s="T9">kam-bi</ta>
            <ta e="T11" id="Seg_648" s="T10">nen-i</ta>
            <ta e="T12" id="Seg_649" s="T11">šü-nə</ta>
            <ta e="T13" id="Seg_650" s="T12">nen-i</ta>
            <ta e="T14" id="Seg_651" s="T13">šü</ta>
            <ta e="T15" id="Seg_652" s="T14">am-də</ta>
            <ta e="T16" id="Seg_653" s="T15">dĭ</ta>
            <ta e="T17" id="Seg_654" s="T16">nugurbi-n</ta>
            <ta e="T18" id="Seg_655" s="T17">nen-di-t</ta>
            <ta e="T19" id="Seg_656" s="T18">bar</ta>
            <ta e="T20" id="Seg_657" s="T19">am-də</ta>
            <ta e="T21" id="Seg_658" s="T20">măn</ta>
            <ta e="T22" id="Seg_659" s="T21">dĭ-žət-tə</ta>
            <ta e="T23" id="Seg_660" s="T22">amo-zʼət</ta>
            <ta e="T24" id="Seg_661" s="T23">tʼü-m</ta>
            <ta e="T25" id="Seg_662" s="T24">iʔgö</ta>
            <ta e="T26" id="Seg_663" s="T25">dĭgəttə</ta>
            <ta e="T27" id="Seg_664" s="T26">kam-bi</ta>
            <ta e="T28" id="Seg_665" s="T27">bü-nə</ta>
            <ta e="T29" id="Seg_666" s="T28">bü</ta>
            <ta e="T30" id="Seg_667" s="T29">bü</ta>
            <ta e="T31" id="Seg_668" s="T30">küʔbdər-də</ta>
            <ta e="T32" id="Seg_669" s="T31">nen-i</ta>
            <ta e="T33" id="Seg_670" s="T32">šü</ta>
            <ta e="T34" id="Seg_671" s="T33">măn</ta>
            <ta e="T35" id="Seg_672" s="T34">dărgo=da</ta>
            <ta e="T36" id="Seg_673" s="T35">mʼaŋ-zʼət</ta>
            <ta e="T37" id="Seg_674" s="T36">tʼü-m</ta>
            <ta e="T38" id="Seg_675" s="T37">iʔgö</ta>
            <ta e="T39" id="Seg_676" s="T38">bulan-də</ta>
            <ta e="T40" id="Seg_677" s="T39">kam-bi</ta>
            <ta e="T41" id="Seg_678" s="T40">bulan</ta>
            <ta e="T42" id="Seg_679" s="T41">bulan</ta>
            <ta e="T43" id="Seg_680" s="T42">bü-ʔjə-m</ta>
            <ta e="T44" id="Seg_681" s="T43">bar</ta>
            <ta e="T45" id="Seg_682" s="T44">bĭt-tə</ta>
            <ta e="T46" id="Seg_683" s="T45">măn</ta>
            <ta e="T47" id="Seg_684" s="T46">üjü-nə</ta>
            <ta e="T48" id="Seg_685" s="T47">jil-gəndə</ta>
            <ta e="T49" id="Seg_686" s="T48">bü-jəʔ</ta>
            <ta e="T50" id="Seg_687" s="T49">bĭt-leʔbə-lie-m</ta>
            <ta e="T51" id="Seg_688" s="T50">dĭgəttə</ta>
            <ta e="T52" id="Seg_689" s="T51">kam-bi</ta>
            <ta e="T53" id="Seg_690" s="T52">tʼaktə</ta>
            <ta e="T54" id="Seg_691" s="T53">nüke-i-nə</ta>
            <ta e="T55" id="Seg_692" s="T54">tʼaktə</ta>
            <ta e="T56" id="Seg_693" s="T55">nüke-iʔ</ta>
            <ta e="T57" id="Seg_694" s="T56">kal-laʔ</ta>
            <ta e="T58" id="Seg_695" s="T57">bulan-ə-n</ta>
            <ta e="T59" id="Seg_696" s="T58">ten-zeŋ-də</ta>
            <ta e="T60" id="Seg_697" s="T59">săbəj-gut</ta>
            <ta e="T61" id="Seg_698" s="T60">mĭʔnʼibeʔ</ta>
            <ta e="T62" id="Seg_699" s="T61">ej</ta>
            <ta e="T63" id="Seg_700" s="T62">kereʔ</ta>
            <ta e="T64" id="Seg_701" s="T63">bulan-ə-n</ta>
            <ta e="T65" id="Seg_702" s="T64">ten-də</ta>
            <ta e="T66" id="Seg_703" s="T65">kašpoʔ</ta>
            <ta e="T67" id="Seg_704" s="T66">mĭʔ</ta>
            <ta e="T68" id="Seg_705" s="T67">bosta</ta>
            <ta e="T69" id="Seg_706" s="T68">ten-də</ta>
            <ta e="T70" id="Seg_707" s="T69">iʔgö</ta>
            <ta e="T71" id="Seg_708" s="T70">tumo-iʔ-nə</ta>
            <ta e="T72" id="Seg_709" s="T71">kam-bi</ta>
            <ta e="T73" id="Seg_710" s="T72">tumo-iʔ</ta>
            <ta e="T74" id="Seg_711" s="T73">mă-lia</ta>
            <ta e="T75" id="Seg_712" s="T74">tumo-iʔ</ta>
            <ta e="T76" id="Seg_713" s="T75">kaŋ-gaʔ</ta>
            <ta e="T77" id="Seg_714" s="T76">tʼaktə</ta>
            <ta e="T78" id="Seg_715" s="T77">nüke-iʔ-nə</ta>
            <ta e="T79" id="Seg_716" s="T78">ten-dən</ta>
            <ta e="T80" id="Seg_717" s="T79">bar</ta>
            <ta e="T81" id="Seg_718" s="T80">am-gut</ta>
            <ta e="T82" id="Seg_719" s="T81">miʔ</ta>
            <ta e="T83" id="Seg_720" s="T82">bosta-dən</ta>
            <ta e="T84" id="Seg_721" s="T83">nod-ə-n</ta>
            <ta e="T85" id="Seg_722" s="T84">tamər</ta>
            <ta e="T86" id="Seg_723" s="T85">am-nia-baʔ</ta>
            <ta e="T87" id="Seg_724" s="T86">kam-bi</ta>
            <ta e="T88" id="Seg_725" s="T87">nʼi-zeŋ-də</ta>
            <ta e="T89" id="Seg_726" s="T88">nʼi-zeŋ</ta>
            <ta e="T90" id="Seg_727" s="T89">nʼi-zeŋ</ta>
            <ta e="T91" id="Seg_728" s="T90">kal-laʔ</ta>
            <ta e="T92" id="Seg_729" s="T91">kuk-kut</ta>
            <ta e="T93" id="Seg_730" s="T92">tumo-i-m</ta>
            <ta e="T94" id="Seg_731" s="T93">me</ta>
            <ta e="T95" id="Seg_732" s="T94">bos-paʔ</ta>
            <ta e="T96" id="Seg_733" s="T95">kadʼək-sʼəʔ</ta>
            <ta e="T97" id="Seg_734" s="T96">sʼar-laʔbə-beʔ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_735" s="T0">kozan</ta>
            <ta e="T2" id="Seg_736" s="T1">kandə-bi</ta>
            <ta e="T3" id="Seg_737" s="T2">kandə-bi</ta>
            <ta e="T4" id="Seg_738" s="T3">nugurbi</ta>
            <ta e="T5" id="Seg_739" s="T4">toʔbdo-bi</ta>
            <ta e="T6" id="Seg_740" s="T5">nugurbi-Tə</ta>
            <ta e="T7" id="Seg_741" s="T6">püje-bə</ta>
            <ta e="T8" id="Seg_742" s="T7">băt-bi</ta>
            <ta e="T9" id="Seg_743" s="T8">dĭgəttə</ta>
            <ta e="T10" id="Seg_744" s="T9">kan-bi</ta>
            <ta e="T11" id="Seg_745" s="T10">nen-j</ta>
            <ta e="T12" id="Seg_746" s="T11">šü-Tə</ta>
            <ta e="T13" id="Seg_747" s="T12">nen-j</ta>
            <ta e="T14" id="Seg_748" s="T13">šü</ta>
            <ta e="T15" id="Seg_749" s="T14">am-t</ta>
            <ta e="T16" id="Seg_750" s="T15">dĭ</ta>
            <ta e="T17" id="Seg_751" s="T16">nugurbi-n</ta>
            <ta e="T18" id="Seg_752" s="T17">nen-də-t</ta>
            <ta e="T19" id="Seg_753" s="T18">bar</ta>
            <ta e="T20" id="Seg_754" s="T19">am-t</ta>
            <ta e="T21" id="Seg_755" s="T20">măn</ta>
            <ta e="T22" id="Seg_756" s="T21">dĭ-žət-də</ta>
            <ta e="T23" id="Seg_757" s="T22">amo-zit</ta>
            <ta e="T24" id="Seg_758" s="T23">tʼü-m</ta>
            <ta e="T25" id="Seg_759" s="T24">iʔgö</ta>
            <ta e="T26" id="Seg_760" s="T25">dĭgəttə</ta>
            <ta e="T27" id="Seg_761" s="T26">kan-bi</ta>
            <ta e="T28" id="Seg_762" s="T27">bü-Tə</ta>
            <ta e="T29" id="Seg_763" s="T28">bü</ta>
            <ta e="T30" id="Seg_764" s="T29">bü</ta>
            <ta e="T31" id="Seg_765" s="T30">küʔbdər-t</ta>
            <ta e="T32" id="Seg_766" s="T31">nen-j</ta>
            <ta e="T33" id="Seg_767" s="T32">šü</ta>
            <ta e="T34" id="Seg_768" s="T33">măn</ta>
            <ta e="T35" id="Seg_769" s="T34">dărgo=da</ta>
            <ta e="T36" id="Seg_770" s="T35">mʼaŋ-zit</ta>
            <ta e="T37" id="Seg_771" s="T36">tʼü-m</ta>
            <ta e="T38" id="Seg_772" s="T37">iʔgö</ta>
            <ta e="T39" id="Seg_773" s="T38">bulan-Tə</ta>
            <ta e="T40" id="Seg_774" s="T39">kan-bi</ta>
            <ta e="T41" id="Seg_775" s="T40">bulan</ta>
            <ta e="T42" id="Seg_776" s="T41">bulan</ta>
            <ta e="T43" id="Seg_777" s="T42">bü-jəʔ-m</ta>
            <ta e="T44" id="Seg_778" s="T43">bar</ta>
            <ta e="T45" id="Seg_779" s="T44">bĭs-t</ta>
            <ta e="T46" id="Seg_780" s="T45">măn</ta>
            <ta e="T47" id="Seg_781" s="T46">üjü-nə</ta>
            <ta e="T48" id="Seg_782" s="T47">il-gəndə</ta>
            <ta e="T49" id="Seg_783" s="T48">bü-jəʔ</ta>
            <ta e="T50" id="Seg_784" s="T49">bĭs-laʔbə-liA-m</ta>
            <ta e="T51" id="Seg_785" s="T50">dĭgəttə</ta>
            <ta e="T52" id="Seg_786" s="T51">kan-bi</ta>
            <ta e="T53" id="Seg_787" s="T52">tʼaktə</ta>
            <ta e="T54" id="Seg_788" s="T53">nüke-jəʔ-Tə</ta>
            <ta e="T55" id="Seg_789" s="T54">tʼaktə</ta>
            <ta e="T56" id="Seg_790" s="T55">nüke-jəʔ</ta>
            <ta e="T57" id="Seg_791" s="T56">kan-lAʔ</ta>
            <ta e="T58" id="Seg_792" s="T57">bulan-ə-n</ta>
            <ta e="T59" id="Seg_793" s="T58">ten-zAŋ-də</ta>
            <ta e="T60" id="Seg_794" s="T59">săbəj-Kut</ta>
            <ta e="T61" id="Seg_795" s="T60">miʔnʼibeʔ</ta>
            <ta e="T62" id="Seg_796" s="T61">ej</ta>
            <ta e="T63" id="Seg_797" s="T62">kereʔ</ta>
            <ta e="T64" id="Seg_798" s="T63">bulan-ə-n</ta>
            <ta e="T65" id="Seg_799" s="T64">ten-də</ta>
            <ta e="T66" id="Seg_800" s="T65">kašpoʔ</ta>
            <ta e="T67" id="Seg_801" s="T66">miʔ</ta>
            <ta e="T68" id="Seg_802" s="T67">bostə</ta>
            <ta e="T69" id="Seg_803" s="T68">ten-də</ta>
            <ta e="T70" id="Seg_804" s="T69">iʔgö</ta>
            <ta e="T71" id="Seg_805" s="T70">tumo-jəʔ-Tə</ta>
            <ta e="T72" id="Seg_806" s="T71">kan-bi</ta>
            <ta e="T73" id="Seg_807" s="T72">tumo-jəʔ</ta>
            <ta e="T74" id="Seg_808" s="T73">măn-liA</ta>
            <ta e="T75" id="Seg_809" s="T74">tumo-jəʔ</ta>
            <ta e="T76" id="Seg_810" s="T75">kan-KAʔ</ta>
            <ta e="T77" id="Seg_811" s="T76">tʼaktə</ta>
            <ta e="T78" id="Seg_812" s="T77">nüke-jəʔ-Tə</ta>
            <ta e="T79" id="Seg_813" s="T78">ten-dən</ta>
            <ta e="T80" id="Seg_814" s="T79">bar</ta>
            <ta e="T81" id="Seg_815" s="T80">am-Kut</ta>
            <ta e="T82" id="Seg_816" s="T81">miʔ</ta>
            <ta e="T83" id="Seg_817" s="T82">bostə-dən</ta>
            <ta e="T84" id="Seg_818" s="T83">noʔ-ə-n</ta>
            <ta e="T85" id="Seg_819" s="T84">tamər</ta>
            <ta e="T86" id="Seg_820" s="T85">am-liA-bAʔ</ta>
            <ta e="T87" id="Seg_821" s="T86">kan-bi</ta>
            <ta e="T88" id="Seg_822" s="T87">nʼi-zAŋ-Tə</ta>
            <ta e="T89" id="Seg_823" s="T88">nʼi-zAŋ</ta>
            <ta e="T90" id="Seg_824" s="T89">nʼi-zAŋ</ta>
            <ta e="T91" id="Seg_825" s="T90">kan-lAʔ</ta>
            <ta e="T92" id="Seg_826" s="T91">kut-Kut</ta>
            <ta e="T93" id="Seg_827" s="T92">tumo-jəʔ-m</ta>
            <ta e="T94" id="Seg_828" s="T93">miʔ</ta>
            <ta e="T95" id="Seg_829" s="T94">bos-bAʔ</ta>
            <ta e="T96" id="Seg_830" s="T95">kadʼək-ziʔ</ta>
            <ta e="T97" id="Seg_831" s="T96">sʼar-laʔbə-bAʔ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_832" s="T0">hare.[NOM.SG]</ta>
            <ta e="T2" id="Seg_833" s="T1">walk-PST.[3SG]</ta>
            <ta e="T3" id="Seg_834" s="T2">walk-PST.[3SG]</ta>
            <ta e="T4" id="Seg_835" s="T3">phleum.[NOM.SG]</ta>
            <ta e="T5" id="Seg_836" s="T4">come.up-PST.[3SG]</ta>
            <ta e="T6" id="Seg_837" s="T5">phleum-LAT</ta>
            <ta e="T7" id="Seg_838" s="T6">nose-ACC.3SG</ta>
            <ta e="T8" id="Seg_839" s="T7">cut-PST.[3SG]</ta>
            <ta e="T9" id="Seg_840" s="T8">then</ta>
            <ta e="T10" id="Seg_841" s="T9">go-PST.[3SG]</ta>
            <ta e="T11" id="Seg_842" s="T10">burn-ADJZ</ta>
            <ta e="T12" id="Seg_843" s="T11">fire-LAT</ta>
            <ta e="T13" id="Seg_844" s="T12">burn-ADJZ</ta>
            <ta e="T14" id="Seg_845" s="T13">fire.[NOM.SG]</ta>
            <ta e="T15" id="Seg_846" s="T14">eat-IMP.2SG.O</ta>
            <ta e="T16" id="Seg_847" s="T15">this.[NOM.SG]</ta>
            <ta e="T17" id="Seg_848" s="T16">phleum-GEN</ta>
            <ta e="T18" id="Seg_849" s="T17">burn-TR-IMP.2SG.O</ta>
            <ta e="T19" id="Seg_850" s="T18">all</ta>
            <ta e="T20" id="Seg_851" s="T19">eat-IMP.2SG.O</ta>
            <ta e="T21" id="Seg_852" s="T20">I.GEN</ta>
            <ta e="T22" id="Seg_853" s="T21">this-CAR.ADJ-NOM/GEN/ACC.3SG</ta>
            <ta e="T23" id="Seg_854" s="T22">burn-INF</ta>
            <ta e="T24" id="Seg_855" s="T23">land-NOM/GEN/ACC.1SG</ta>
            <ta e="T25" id="Seg_856" s="T24">many</ta>
            <ta e="T26" id="Seg_857" s="T25">then</ta>
            <ta e="T27" id="Seg_858" s="T26">go-PST.[3SG]</ta>
            <ta e="T28" id="Seg_859" s="T27">water-LAT</ta>
            <ta e="T29" id="Seg_860" s="T28">water.[NOM.SG]</ta>
            <ta e="T30" id="Seg_861" s="T29">water.[NOM.SG]</ta>
            <ta e="T31" id="Seg_862" s="T30">extinguish-IMP.2SG.O</ta>
            <ta e="T32" id="Seg_863" s="T31">burn-ADJZ</ta>
            <ta e="T33" id="Seg_864" s="T32">fire.[NOM.SG]</ta>
            <ta e="T34" id="Seg_865" s="T33">I.GEN</ta>
            <ta e="T35" id="Seg_866" s="T34">for.free=PTCL</ta>
            <ta e="T36" id="Seg_867" s="T35">flow-INF</ta>
            <ta e="T37" id="Seg_868" s="T36">land-NOM/GEN/ACC.1SG</ta>
            <ta e="T38" id="Seg_869" s="T37">many</ta>
            <ta e="T39" id="Seg_870" s="T38">moose-LAT</ta>
            <ta e="T40" id="Seg_871" s="T39">go-PST.[3SG]</ta>
            <ta e="T41" id="Seg_872" s="T40">moose.[NOM.SG]</ta>
            <ta e="T42" id="Seg_873" s="T41">moose.[NOM.SG]</ta>
            <ta e="T43" id="Seg_874" s="T42">river-PL-ACC</ta>
            <ta e="T44" id="Seg_875" s="T43">all</ta>
            <ta e="T45" id="Seg_876" s="T44">drink-IMP.2SG.O</ta>
            <ta e="T46" id="Seg_877" s="T45">I.NOM</ta>
            <ta e="T47" id="Seg_878" s="T46">foot-GEN.1SG</ta>
            <ta e="T48" id="Seg_879" s="T47">underpart-LAT/LOC.3SG</ta>
            <ta e="T49" id="Seg_880" s="T48">water-PL</ta>
            <ta e="T50" id="Seg_881" s="T49">drink-DUR-PRS-1SG</ta>
            <ta e="T51" id="Seg_882" s="T50">then</ta>
            <ta e="T52" id="Seg_883" s="T51">go-PST.[3SG]</ta>
            <ta e="T53" id="Seg_884" s="T52">old.[NOM.SG]</ta>
            <ta e="T54" id="Seg_885" s="T53">woman-PL-LAT</ta>
            <ta e="T55" id="Seg_886" s="T54">old.[NOM.SG]</ta>
            <ta e="T56" id="Seg_887" s="T55">woman-PL</ta>
            <ta e="T57" id="Seg_888" s="T56">go-CVB</ta>
            <ta e="T58" id="Seg_889" s="T57">moose-EP-GEN</ta>
            <ta e="T59" id="Seg_890" s="T58">sinew-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T60" id="Seg_891" s="T59">pull.out-IMP.2PL.O</ta>
            <ta e="T61" id="Seg_892" s="T60">we.LAT</ta>
            <ta e="T62" id="Seg_893" s="T61">NEG</ta>
            <ta e="T63" id="Seg_894" s="T62">one.needs</ta>
            <ta e="T64" id="Seg_895" s="T63">moose-EP-GEN</ta>
            <ta e="T65" id="Seg_896" s="T64">sinew-NOM/GEN/ACC.3SG</ta>
            <ta e="T66" id="Seg_897" s="T65">robust.[NOM.SG]</ta>
            <ta e="T67" id="Seg_898" s="T66">we.GEN</ta>
            <ta e="T68" id="Seg_899" s="T67">self</ta>
            <ta e="T69" id="Seg_900" s="T68">sinew-NOM/GEN/ACC.3SG</ta>
            <ta e="T70" id="Seg_901" s="T69">many</ta>
            <ta e="T71" id="Seg_902" s="T70">mouse-PL-LAT</ta>
            <ta e="T72" id="Seg_903" s="T71">go-PST.[3SG]</ta>
            <ta e="T73" id="Seg_904" s="T72">mouse-PL</ta>
            <ta e="T74" id="Seg_905" s="T73">say-PRS.[3SG]</ta>
            <ta e="T75" id="Seg_906" s="T74">mouse-PL</ta>
            <ta e="T76" id="Seg_907" s="T75">go-IMP.2PL</ta>
            <ta e="T77" id="Seg_908" s="T76">old.[NOM.SG]</ta>
            <ta e="T78" id="Seg_909" s="T77">woman-PL-LAT</ta>
            <ta e="T79" id="Seg_910" s="T78">sinew-NOM/GEN/ACC.3PL</ta>
            <ta e="T80" id="Seg_911" s="T79">all</ta>
            <ta e="T81" id="Seg_912" s="T80">eat-IMP.2PL.O</ta>
            <ta e="T82" id="Seg_913" s="T81">we.NOM</ta>
            <ta e="T83" id="Seg_914" s="T82">self-NOM/GEN/ACC.3PL</ta>
            <ta e="T84" id="Seg_915" s="T83">grass-EP-GEN</ta>
            <ta e="T85" id="Seg_916" s="T84">root.[NOM.SG]</ta>
            <ta e="T86" id="Seg_917" s="T85">eat-PRS-1PL</ta>
            <ta e="T87" id="Seg_918" s="T86">go-PST.[3SG]</ta>
            <ta e="T88" id="Seg_919" s="T87">boy-PL-LAT</ta>
            <ta e="T89" id="Seg_920" s="T88">boy-PL</ta>
            <ta e="T90" id="Seg_921" s="T89">boy-PL</ta>
            <ta e="T91" id="Seg_922" s="T90">go-CVB</ta>
            <ta e="T92" id="Seg_923" s="T91">kill-IMP.2PL.O</ta>
            <ta e="T93" id="Seg_924" s="T92">mouse-PL-ACC</ta>
            <ta e="T94" id="Seg_925" s="T93">we.NOM</ta>
            <ta e="T95" id="Seg_926" s="T94">self-NOM/GEN/ACC.1PL</ta>
            <ta e="T96" id="Seg_927" s="T95">knuckle-INS</ta>
            <ta e="T97" id="Seg_928" s="T96">play-DUR-1PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_929" s="T0">заяц.[NOM.SG]</ta>
            <ta e="T2" id="Seg_930" s="T1">идти-PST.[3SG]</ta>
            <ta e="T3" id="Seg_931" s="T2">идти-PST.[3SG]</ta>
            <ta e="T4" id="Seg_932" s="T3">тимофеевка.[NOM.SG]</ta>
            <ta e="T5" id="Seg_933" s="T4">попасть-PST.[3SG]</ta>
            <ta e="T6" id="Seg_934" s="T5">тимофеевка-LAT</ta>
            <ta e="T7" id="Seg_935" s="T6">нос-ACC.3SG</ta>
            <ta e="T8" id="Seg_936" s="T7">резать-PST.[3SG]</ta>
            <ta e="T9" id="Seg_937" s="T8">тогда</ta>
            <ta e="T10" id="Seg_938" s="T9">пойти-PST.[3SG]</ta>
            <ta e="T11" id="Seg_939" s="T10">гореть-ADJZ</ta>
            <ta e="T12" id="Seg_940" s="T11">огонь-LAT</ta>
            <ta e="T13" id="Seg_941" s="T12">гореть-ADJZ</ta>
            <ta e="T14" id="Seg_942" s="T13">огонь.[NOM.SG]</ta>
            <ta e="T15" id="Seg_943" s="T14">съесть-IMP.2SG.O</ta>
            <ta e="T16" id="Seg_944" s="T15">этот.[NOM.SG]</ta>
            <ta e="T17" id="Seg_945" s="T16">тимофеевка-GEN</ta>
            <ta e="T18" id="Seg_946" s="T17">гореть-TR-IMP.2SG.O</ta>
            <ta e="T19" id="Seg_947" s="T18">весь</ta>
            <ta e="T20" id="Seg_948" s="T19">съесть-IMP.2SG.O</ta>
            <ta e="T21" id="Seg_949" s="T20">я.GEN</ta>
            <ta e="T22" id="Seg_950" s="T21">этот-CAR.ADJ-NOM/GEN/ACC.3SG</ta>
            <ta e="T23" id="Seg_951" s="T22">гореть-INF</ta>
            <ta e="T24" id="Seg_952" s="T23">земля-NOM/GEN/ACC.1SG</ta>
            <ta e="T25" id="Seg_953" s="T24">много</ta>
            <ta e="T26" id="Seg_954" s="T25">тогда</ta>
            <ta e="T27" id="Seg_955" s="T26">пойти-PST.[3SG]</ta>
            <ta e="T28" id="Seg_956" s="T27">вода-LAT</ta>
            <ta e="T29" id="Seg_957" s="T28">вода.[NOM.SG]</ta>
            <ta e="T30" id="Seg_958" s="T29">вода.[NOM.SG]</ta>
            <ta e="T31" id="Seg_959" s="T30">потушить-IMP.2SG.O</ta>
            <ta e="T32" id="Seg_960" s="T31">гореть-ADJZ</ta>
            <ta e="T33" id="Seg_961" s="T32">огонь.[NOM.SG]</ta>
            <ta e="T34" id="Seg_962" s="T33">я.GEN</ta>
            <ta e="T35" id="Seg_963" s="T34">даром=PTCL</ta>
            <ta e="T36" id="Seg_964" s="T35">течь-INF</ta>
            <ta e="T37" id="Seg_965" s="T36">земля-NOM/GEN/ACC.1SG</ta>
            <ta e="T38" id="Seg_966" s="T37">много</ta>
            <ta e="T39" id="Seg_967" s="T38">лось-LAT</ta>
            <ta e="T40" id="Seg_968" s="T39">пойти-PST.[3SG]</ta>
            <ta e="T41" id="Seg_969" s="T40">лось.[NOM.SG]</ta>
            <ta e="T42" id="Seg_970" s="T41">лось.[NOM.SG]</ta>
            <ta e="T43" id="Seg_971" s="T42">река-PL-ACC</ta>
            <ta e="T44" id="Seg_972" s="T43">весь</ta>
            <ta e="T45" id="Seg_973" s="T44">пить-IMP.2SG.O</ta>
            <ta e="T46" id="Seg_974" s="T45">я.NOM</ta>
            <ta e="T47" id="Seg_975" s="T46">нога-GEN.1SG</ta>
            <ta e="T48" id="Seg_976" s="T47">низ-LAT/LOC.3SG</ta>
            <ta e="T49" id="Seg_977" s="T48">вода-PL</ta>
            <ta e="T50" id="Seg_978" s="T49">пить-DUR-PRS-1SG</ta>
            <ta e="T51" id="Seg_979" s="T50">тогда</ta>
            <ta e="T52" id="Seg_980" s="T51">пойти-PST.[3SG]</ta>
            <ta e="T53" id="Seg_981" s="T52">старый.[NOM.SG]</ta>
            <ta e="T54" id="Seg_982" s="T53">женщина-PL-LAT</ta>
            <ta e="T55" id="Seg_983" s="T54">старый.[NOM.SG]</ta>
            <ta e="T56" id="Seg_984" s="T55">женщина-PL</ta>
            <ta e="T57" id="Seg_985" s="T56">пойти-CVB</ta>
            <ta e="T58" id="Seg_986" s="T57">лось-EP-GEN</ta>
            <ta e="T59" id="Seg_987" s="T58">жила-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T60" id="Seg_988" s="T59">вынимать-IMP.2PL.O</ta>
            <ta e="T61" id="Seg_989" s="T60">мы.LAT</ta>
            <ta e="T62" id="Seg_990" s="T61">NEG</ta>
            <ta e="T63" id="Seg_991" s="T62">нужно</ta>
            <ta e="T64" id="Seg_992" s="T63">лось-EP-GEN</ta>
            <ta e="T65" id="Seg_993" s="T64">жила-NOM/GEN/ACC.3SG</ta>
            <ta e="T66" id="Seg_994" s="T65">крепкий.[NOM.SG]</ta>
            <ta e="T67" id="Seg_995" s="T66">мы.GEN</ta>
            <ta e="T68" id="Seg_996" s="T67">сам</ta>
            <ta e="T69" id="Seg_997" s="T68">жила-NOM/GEN/ACC.3SG</ta>
            <ta e="T70" id="Seg_998" s="T69">много</ta>
            <ta e="T71" id="Seg_999" s="T70">мышь-PL-LAT</ta>
            <ta e="T72" id="Seg_1000" s="T71">пойти-PST.[3SG]</ta>
            <ta e="T73" id="Seg_1001" s="T72">мышь-PL</ta>
            <ta e="T74" id="Seg_1002" s="T73">сказать-PRS.[3SG]</ta>
            <ta e="T75" id="Seg_1003" s="T74">мышь-PL</ta>
            <ta e="T76" id="Seg_1004" s="T75">пойти-IMP.2PL</ta>
            <ta e="T77" id="Seg_1005" s="T76">старый.[NOM.SG]</ta>
            <ta e="T78" id="Seg_1006" s="T77">женщина-PL-LAT</ta>
            <ta e="T79" id="Seg_1007" s="T78">жила-NOM/GEN/ACC.3PL</ta>
            <ta e="T80" id="Seg_1008" s="T79">весь</ta>
            <ta e="T81" id="Seg_1009" s="T80">съесть-IMP.2PL.O</ta>
            <ta e="T82" id="Seg_1010" s="T81">мы.NOM</ta>
            <ta e="T83" id="Seg_1011" s="T82">сам-NOM/GEN/ACC.3PL</ta>
            <ta e="T84" id="Seg_1012" s="T83">трава-EP-GEN</ta>
            <ta e="T85" id="Seg_1013" s="T84">корень.[NOM.SG]</ta>
            <ta e="T86" id="Seg_1014" s="T85">съесть-PRS-1PL</ta>
            <ta e="T87" id="Seg_1015" s="T86">пойти-PST.[3SG]</ta>
            <ta e="T88" id="Seg_1016" s="T87">мальчик-PL-LAT</ta>
            <ta e="T89" id="Seg_1017" s="T88">мальчик-PL</ta>
            <ta e="T90" id="Seg_1018" s="T89">мальчик-PL</ta>
            <ta e="T91" id="Seg_1019" s="T90">пойти-CVB</ta>
            <ta e="T92" id="Seg_1020" s="T91">убить-IMP.2PL.O</ta>
            <ta e="T93" id="Seg_1021" s="T92">мышь-PL-ACC</ta>
            <ta e="T94" id="Seg_1022" s="T93">мы.NOM</ta>
            <ta e="T95" id="Seg_1023" s="T94">сам-NOM/GEN/ACC.1PL</ta>
            <ta e="T96" id="Seg_1024" s="T95">сустав-INS</ta>
            <ta e="T97" id="Seg_1025" s="T96">играть-DUR-1PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1026" s="T0">n-n:case</ta>
            <ta e="T2" id="Seg_1027" s="T1">v-v:tense-v:pn</ta>
            <ta e="T3" id="Seg_1028" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_1029" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_1030" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_1031" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_1032" s="T6">n-n:case.poss</ta>
            <ta e="T8" id="Seg_1033" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_1034" s="T8">adv</ta>
            <ta e="T10" id="Seg_1035" s="T9">v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_1036" s="T10">v-v&gt;adj</ta>
            <ta e="T12" id="Seg_1037" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_1038" s="T12">v-v&gt;adj</ta>
            <ta e="T14" id="Seg_1039" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_1040" s="T14">v-v:mood.pn</ta>
            <ta e="T16" id="Seg_1041" s="T15">dempro-n:case</ta>
            <ta e="T17" id="Seg_1042" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_1043" s="T17">v-v&gt;v-v:mood.pn</ta>
            <ta e="T19" id="Seg_1044" s="T18">quant</ta>
            <ta e="T20" id="Seg_1045" s="T19">v-v:mood.pn</ta>
            <ta e="T21" id="Seg_1046" s="T20">pers</ta>
            <ta e="T22" id="Seg_1047" s="T21">dempro-n&gt;adj-n:case.poss</ta>
            <ta e="T23" id="Seg_1048" s="T22">v-v:n.fin</ta>
            <ta e="T24" id="Seg_1049" s="T23">n-n:case.poss</ta>
            <ta e="T25" id="Seg_1050" s="T24">quant</ta>
            <ta e="T26" id="Seg_1051" s="T25">adv</ta>
            <ta e="T27" id="Seg_1052" s="T26">v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_1053" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_1054" s="T28">n-n:case</ta>
            <ta e="T30" id="Seg_1055" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_1056" s="T30">v-v:mood.pn</ta>
            <ta e="T32" id="Seg_1057" s="T31">v-v&gt;adj</ta>
            <ta e="T33" id="Seg_1058" s="T32">n-n:case</ta>
            <ta e="T34" id="Seg_1059" s="T33">pers</ta>
            <ta e="T35" id="Seg_1060" s="T34">adv=ptcl</ta>
            <ta e="T36" id="Seg_1061" s="T35">v-v:n.fin</ta>
            <ta e="T37" id="Seg_1062" s="T36">n-n:case.poss</ta>
            <ta e="T38" id="Seg_1063" s="T37">quant</ta>
            <ta e="T39" id="Seg_1064" s="T38">n-n:case</ta>
            <ta e="T40" id="Seg_1065" s="T39">v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_1066" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_1067" s="T41">n-n:case</ta>
            <ta e="T43" id="Seg_1068" s="T42">n-n:num-n:case</ta>
            <ta e="T44" id="Seg_1069" s="T43">quant</ta>
            <ta e="T45" id="Seg_1070" s="T44">v-v:mood.pn</ta>
            <ta e="T46" id="Seg_1071" s="T45">pers</ta>
            <ta e="T47" id="Seg_1072" s="T46">n-n:case.poss</ta>
            <ta e="T48" id="Seg_1073" s="T47">n-n:case.poss</ta>
            <ta e="T49" id="Seg_1074" s="T48">n-n:num</ta>
            <ta e="T50" id="Seg_1075" s="T49">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T51" id="Seg_1076" s="T50">adv</ta>
            <ta e="T52" id="Seg_1077" s="T51">v-v:tense-v:pn</ta>
            <ta e="T53" id="Seg_1078" s="T52">adj-n:case</ta>
            <ta e="T54" id="Seg_1079" s="T53">n-n:num-n:case</ta>
            <ta e="T55" id="Seg_1080" s="T54">adj-n:case</ta>
            <ta e="T56" id="Seg_1081" s="T55">n-n:num</ta>
            <ta e="T57" id="Seg_1082" s="T56">v-v:n.fin</ta>
            <ta e="T58" id="Seg_1083" s="T57">n-n:ins-n:case</ta>
            <ta e="T59" id="Seg_1084" s="T58">n-n:num-n:case.poss</ta>
            <ta e="T60" id="Seg_1085" s="T59">v-v:mood.pn</ta>
            <ta e="T61" id="Seg_1086" s="T60">pers</ta>
            <ta e="T62" id="Seg_1087" s="T61">ptcl</ta>
            <ta e="T63" id="Seg_1088" s="T62">adv</ta>
            <ta e="T64" id="Seg_1089" s="T63">n-n:ins-n:case</ta>
            <ta e="T65" id="Seg_1090" s="T64">n-n:case.poss</ta>
            <ta e="T66" id="Seg_1091" s="T65">adj-n:case</ta>
            <ta e="T67" id="Seg_1092" s="T66">pers</ta>
            <ta e="T68" id="Seg_1093" s="T67">refl</ta>
            <ta e="T69" id="Seg_1094" s="T68">n-n:case.poss</ta>
            <ta e="T70" id="Seg_1095" s="T69">quant</ta>
            <ta e="T71" id="Seg_1096" s="T70">n-n:num-n:case</ta>
            <ta e="T72" id="Seg_1097" s="T71">v-v:tense-v:pn</ta>
            <ta e="T73" id="Seg_1098" s="T72">n-n:num</ta>
            <ta e="T74" id="Seg_1099" s="T73">v-v:tense-v:pn</ta>
            <ta e="T75" id="Seg_1100" s="T74">n-n:num</ta>
            <ta e="T76" id="Seg_1101" s="T75">v-v:mood.pn</ta>
            <ta e="T77" id="Seg_1102" s="T76">adj-n:case</ta>
            <ta e="T78" id="Seg_1103" s="T77">n-n:num-n:case</ta>
            <ta e="T79" id="Seg_1104" s="T78">n-n:case.poss</ta>
            <ta e="T80" id="Seg_1105" s="T79">quant</ta>
            <ta e="T81" id="Seg_1106" s="T80">v-v:mood.pn</ta>
            <ta e="T82" id="Seg_1107" s="T81">pers</ta>
            <ta e="T83" id="Seg_1108" s="T82">refl-n:case.poss</ta>
            <ta e="T84" id="Seg_1109" s="T83">n-n:ins-n:case</ta>
            <ta e="T85" id="Seg_1110" s="T84">n-n:case</ta>
            <ta e="T86" id="Seg_1111" s="T85">v-v:tense-v:pn</ta>
            <ta e="T87" id="Seg_1112" s="T86">v-v:tense-v:pn</ta>
            <ta e="T88" id="Seg_1113" s="T87">n-n:num-n:case</ta>
            <ta e="T89" id="Seg_1114" s="T88">n-n:num</ta>
            <ta e="T90" id="Seg_1115" s="T89">n-n:num</ta>
            <ta e="T91" id="Seg_1116" s="T90">v-v:n.fin</ta>
            <ta e="T92" id="Seg_1117" s="T91">v-v:mood.pn</ta>
            <ta e="T93" id="Seg_1118" s="T92">n-n:num-n:case</ta>
            <ta e="T94" id="Seg_1119" s="T93">pers</ta>
            <ta e="T95" id="Seg_1120" s="T94">refl-n:case.poss</ta>
            <ta e="T96" id="Seg_1121" s="T95">n-n:case</ta>
            <ta e="T97" id="Seg_1122" s="T96">v-v&gt;v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1123" s="T0">n</ta>
            <ta e="T2" id="Seg_1124" s="T1">v</ta>
            <ta e="T3" id="Seg_1125" s="T2">v</ta>
            <ta e="T4" id="Seg_1126" s="T3">n</ta>
            <ta e="T5" id="Seg_1127" s="T4">v</ta>
            <ta e="T6" id="Seg_1128" s="T5">n</ta>
            <ta e="T7" id="Seg_1129" s="T6">n</ta>
            <ta e="T8" id="Seg_1130" s="T7">v</ta>
            <ta e="T9" id="Seg_1131" s="T8">adv</ta>
            <ta e="T10" id="Seg_1132" s="T9">v</ta>
            <ta e="T11" id="Seg_1133" s="T10">v</ta>
            <ta e="T12" id="Seg_1134" s="T11">n</ta>
            <ta e="T13" id="Seg_1135" s="T12">v</ta>
            <ta e="T14" id="Seg_1136" s="T13">n</ta>
            <ta e="T15" id="Seg_1137" s="T14">v</ta>
            <ta e="T16" id="Seg_1138" s="T15">dempro</ta>
            <ta e="T17" id="Seg_1139" s="T16">n</ta>
            <ta e="T18" id="Seg_1140" s="T17">v</ta>
            <ta e="T19" id="Seg_1141" s="T18">quant</ta>
            <ta e="T20" id="Seg_1142" s="T19">v</ta>
            <ta e="T21" id="Seg_1143" s="T20">pers</ta>
            <ta e="T22" id="Seg_1144" s="T21">dempro</ta>
            <ta e="T23" id="Seg_1145" s="T22">v</ta>
            <ta e="T24" id="Seg_1146" s="T23">n</ta>
            <ta e="T25" id="Seg_1147" s="T24">quant</ta>
            <ta e="T26" id="Seg_1148" s="T25">adv</ta>
            <ta e="T27" id="Seg_1149" s="T26">v</ta>
            <ta e="T28" id="Seg_1150" s="T27">n</ta>
            <ta e="T29" id="Seg_1151" s="T28">n</ta>
            <ta e="T30" id="Seg_1152" s="T29">n</ta>
            <ta e="T31" id="Seg_1153" s="T30">v</ta>
            <ta e="T32" id="Seg_1154" s="T31">v</ta>
            <ta e="T33" id="Seg_1155" s="T32">n</ta>
            <ta e="T34" id="Seg_1156" s="T33">pers</ta>
            <ta e="T35" id="Seg_1157" s="T34">adv</ta>
            <ta e="T36" id="Seg_1158" s="T35">v</ta>
            <ta e="T37" id="Seg_1159" s="T36">n</ta>
            <ta e="T38" id="Seg_1160" s="T37">quant</ta>
            <ta e="T39" id="Seg_1161" s="T38">n</ta>
            <ta e="T40" id="Seg_1162" s="T39">v</ta>
            <ta e="T41" id="Seg_1163" s="T40">n</ta>
            <ta e="T42" id="Seg_1164" s="T41">n</ta>
            <ta e="T43" id="Seg_1165" s="T42">n</ta>
            <ta e="T44" id="Seg_1166" s="T43">quant</ta>
            <ta e="T45" id="Seg_1167" s="T44">v</ta>
            <ta e="T46" id="Seg_1168" s="T45">pers</ta>
            <ta e="T47" id="Seg_1169" s="T46">n</ta>
            <ta e="T48" id="Seg_1170" s="T47">n</ta>
            <ta e="T49" id="Seg_1171" s="T48">n</ta>
            <ta e="T50" id="Seg_1172" s="T49">v</ta>
            <ta e="T51" id="Seg_1173" s="T50">adv</ta>
            <ta e="T52" id="Seg_1174" s="T51">v</ta>
            <ta e="T53" id="Seg_1175" s="T52">adj</ta>
            <ta e="T54" id="Seg_1176" s="T53">n</ta>
            <ta e="T55" id="Seg_1177" s="T54">adj</ta>
            <ta e="T56" id="Seg_1178" s="T55">n</ta>
            <ta e="T57" id="Seg_1179" s="T56">v</ta>
            <ta e="T58" id="Seg_1180" s="T57">n</ta>
            <ta e="T59" id="Seg_1181" s="T58">n</ta>
            <ta e="T60" id="Seg_1182" s="T59">v</ta>
            <ta e="T61" id="Seg_1183" s="T60">pers</ta>
            <ta e="T62" id="Seg_1184" s="T61">ptcl</ta>
            <ta e="T63" id="Seg_1185" s="T62">adv</ta>
            <ta e="T64" id="Seg_1186" s="T63">n</ta>
            <ta e="T65" id="Seg_1187" s="T64">n</ta>
            <ta e="T66" id="Seg_1188" s="T65">adj</ta>
            <ta e="T67" id="Seg_1189" s="T66">pers</ta>
            <ta e="T68" id="Seg_1190" s="T67">refl</ta>
            <ta e="T69" id="Seg_1191" s="T68">n</ta>
            <ta e="T70" id="Seg_1192" s="T69">quant</ta>
            <ta e="T71" id="Seg_1193" s="T70">n</ta>
            <ta e="T72" id="Seg_1194" s="T71">v</ta>
            <ta e="T73" id="Seg_1195" s="T72">n</ta>
            <ta e="T74" id="Seg_1196" s="T73">v</ta>
            <ta e="T75" id="Seg_1197" s="T74">n</ta>
            <ta e="T76" id="Seg_1198" s="T75">v</ta>
            <ta e="T77" id="Seg_1199" s="T76">adj</ta>
            <ta e="T78" id="Seg_1200" s="T77">n</ta>
            <ta e="T79" id="Seg_1201" s="T78">n</ta>
            <ta e="T80" id="Seg_1202" s="T79">quant</ta>
            <ta e="T81" id="Seg_1203" s="T80">v</ta>
            <ta e="T82" id="Seg_1204" s="T81">pers</ta>
            <ta e="T83" id="Seg_1205" s="T82">refl</ta>
            <ta e="T84" id="Seg_1206" s="T83">n</ta>
            <ta e="T85" id="Seg_1207" s="T84">n</ta>
            <ta e="T86" id="Seg_1208" s="T85">v</ta>
            <ta e="T87" id="Seg_1209" s="T86">v</ta>
            <ta e="T88" id="Seg_1210" s="T87">n</ta>
            <ta e="T89" id="Seg_1211" s="T88">n</ta>
            <ta e="T90" id="Seg_1212" s="T89">n</ta>
            <ta e="T91" id="Seg_1213" s="T90">v</ta>
            <ta e="T92" id="Seg_1214" s="T91">v</ta>
            <ta e="T93" id="Seg_1215" s="T92">n</ta>
            <ta e="T94" id="Seg_1216" s="T93">pers</ta>
            <ta e="T95" id="Seg_1217" s="T94">refl</ta>
            <ta e="T96" id="Seg_1218" s="T95">n</ta>
            <ta e="T97" id="Seg_1219" s="T96">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_1220" s="T0">np.h:Th</ta>
            <ta e="T4" id="Seg_1221" s="T3">np:Th</ta>
            <ta e="T6" id="Seg_1222" s="T5">np:Ins</ta>
            <ta e="T7" id="Seg_1223" s="T6">np:P 0.3.h:Poss</ta>
            <ta e="T8" id="Seg_1224" s="T7">0.3.h:P</ta>
            <ta e="T9" id="Seg_1225" s="T8">adv:Time</ta>
            <ta e="T10" id="Seg_1226" s="T9">0.3.h:Th</ta>
            <ta e="T12" id="Seg_1227" s="T11">np:G</ta>
            <ta e="T15" id="Seg_1228" s="T14">0.2.h:A</ta>
            <ta e="T17" id="Seg_1229" s="T16">np:P</ta>
            <ta e="T18" id="Seg_1230" s="T17">0.2.h:A </ta>
            <ta e="T19" id="Seg_1231" s="T18">pro:P</ta>
            <ta e="T20" id="Seg_1232" s="T19">0.2.h:A</ta>
            <ta e="T21" id="Seg_1233" s="T20">pro.h:Poss</ta>
            <ta e="T24" id="Seg_1234" s="T23">np:Th 0.1.h:Poss</ta>
            <ta e="T26" id="Seg_1235" s="T25">adv:Time</ta>
            <ta e="T27" id="Seg_1236" s="T26">0.3.h:Th</ta>
            <ta e="T28" id="Seg_1237" s="T27">np:G</ta>
            <ta e="T31" id="Seg_1238" s="T30">0.2.h:A</ta>
            <ta e="T33" id="Seg_1239" s="T32">np:P</ta>
            <ta e="T34" id="Seg_1240" s="T33">pro.h:Poss</ta>
            <ta e="T37" id="Seg_1241" s="T36">np:Th 0.1.h:Poss</ta>
            <ta e="T39" id="Seg_1242" s="T38">np:G</ta>
            <ta e="T40" id="Seg_1243" s="T39">0.3.h:Th</ta>
            <ta e="T43" id="Seg_1244" s="T42">np:P</ta>
            <ta e="T45" id="Seg_1245" s="T44">0.2.h:A</ta>
            <ta e="T46" id="Seg_1246" s="T45">pro.h:A</ta>
            <ta e="T48" id="Seg_1247" s="T46">pp:L</ta>
            <ta e="T49" id="Seg_1248" s="T48">np:P</ta>
            <ta e="T52" id="Seg_1249" s="T51">0.3.h:Th</ta>
            <ta e="T54" id="Seg_1250" s="T53">np:G</ta>
            <ta e="T58" id="Seg_1251" s="T57">np:Poss</ta>
            <ta e="T59" id="Seg_1252" s="T58">np:P</ta>
            <ta e="T60" id="Seg_1253" s="T59">0.2.h:A</ta>
            <ta e="T61" id="Seg_1254" s="T60">pro.h:Th</ta>
            <ta e="T64" id="Seg_1255" s="T63">np:Poss</ta>
            <ta e="T65" id="Seg_1256" s="T64">np:Th</ta>
            <ta e="T67" id="Seg_1257" s="T66">pro.h:Poss</ta>
            <ta e="T69" id="Seg_1258" s="T68">np:Th</ta>
            <ta e="T71" id="Seg_1259" s="T70">np:G</ta>
            <ta e="T72" id="Seg_1260" s="T71">0.3.h:Th</ta>
            <ta e="T74" id="Seg_1261" s="T73">0.3.h:A</ta>
            <ta e="T76" id="Seg_1262" s="T75">0.2.h:Th</ta>
            <ta e="T78" id="Seg_1263" s="T77">np:G</ta>
            <ta e="T79" id="Seg_1264" s="T78">np:P 0.3.h:Poss</ta>
            <ta e="T81" id="Seg_1265" s="T80">0.2.h:A</ta>
            <ta e="T82" id="Seg_1266" s="T81">pro.h:A</ta>
            <ta e="T83" id="Seg_1267" s="T82">pro.h:Poss</ta>
            <ta e="T84" id="Seg_1268" s="T83">np:Poss</ta>
            <ta e="T85" id="Seg_1269" s="T84">np:P</ta>
            <ta e="T87" id="Seg_1270" s="T86">0.3.h:Th</ta>
            <ta e="T88" id="Seg_1271" s="T87">np:G</ta>
            <ta e="T92" id="Seg_1272" s="T91">0.2.h:A</ta>
            <ta e="T93" id="Seg_1273" s="T92">np:P</ta>
            <ta e="T94" id="Seg_1274" s="T93">pro.h:A</ta>
            <ta e="T96" id="Seg_1275" s="T95">np:Ins</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_1276" s="T0">np.h:S</ta>
            <ta e="T2" id="Seg_1277" s="T1">v:pred</ta>
            <ta e="T3" id="Seg_1278" s="T2">v:pred</ta>
            <ta e="T4" id="Seg_1279" s="T3">np:S</ta>
            <ta e="T5" id="Seg_1280" s="T4">v:pred</ta>
            <ta e="T7" id="Seg_1281" s="T6">np:O</ta>
            <ta e="T8" id="Seg_1282" s="T7">v:pred 0.3.h:S</ta>
            <ta e="T10" id="Seg_1283" s="T9">v:pred 0.3.h:S</ta>
            <ta e="T15" id="Seg_1284" s="T14">v:pred 0.2.h:S</ta>
            <ta e="T17" id="Seg_1285" s="T16">np:O</ta>
            <ta e="T18" id="Seg_1286" s="T17">v:pred 0.2.h:S 0.3:S</ta>
            <ta e="T19" id="Seg_1287" s="T18">np:O</ta>
            <ta e="T20" id="Seg_1288" s="T19">v:pred 0.2.h:S</ta>
            <ta e="T23" id="Seg_1289" s="T22">s:purp</ta>
            <ta e="T24" id="Seg_1290" s="T23">np:S</ta>
            <ta e="T25" id="Seg_1291" s="T24">adj:pred</ta>
            <ta e="T27" id="Seg_1292" s="T26">v:pred 0.3.h:S</ta>
            <ta e="T31" id="Seg_1293" s="T30">v:pred 0.2.h:S</ta>
            <ta e="T33" id="Seg_1294" s="T32">np:O</ta>
            <ta e="T37" id="Seg_1295" s="T36">np:S</ta>
            <ta e="T38" id="Seg_1296" s="T37">adj:pred</ta>
            <ta e="T40" id="Seg_1297" s="T39">v:pred 0.3.h:S</ta>
            <ta e="T43" id="Seg_1298" s="T42">np:O</ta>
            <ta e="T45" id="Seg_1299" s="T44">v:pred 0.2.h:S</ta>
            <ta e="T46" id="Seg_1300" s="T45">pro.h:S</ta>
            <ta e="T49" id="Seg_1301" s="T48">np:O</ta>
            <ta e="T50" id="Seg_1302" s="T49">v:pred</ta>
            <ta e="T52" id="Seg_1303" s="T51">v:pred 0.3.h:S</ta>
            <ta e="T59" id="Seg_1304" s="T58">np:O</ta>
            <ta e="T60" id="Seg_1305" s="T59">v:pred 0.2.h:S</ta>
            <ta e="T62" id="Seg_1306" s="T61">ptcl.neg</ta>
            <ta e="T63" id="Seg_1307" s="T62">ptcl:pred</ta>
            <ta e="T65" id="Seg_1308" s="T64">np:O</ta>
            <ta e="T69" id="Seg_1309" s="T68">np:S</ta>
            <ta e="T70" id="Seg_1310" s="T69">adj:pred</ta>
            <ta e="T72" id="Seg_1311" s="T71">v:pred 0.3.h:S</ta>
            <ta e="T74" id="Seg_1312" s="T73">v:pred 0.3.h:S</ta>
            <ta e="T76" id="Seg_1313" s="T75">v:pred 0.2.h:S</ta>
            <ta e="T79" id="Seg_1314" s="T78">np:O</ta>
            <ta e="T81" id="Seg_1315" s="T80">v:pred 0.2.h:S</ta>
            <ta e="T82" id="Seg_1316" s="T81">pro.h:S</ta>
            <ta e="T85" id="Seg_1317" s="T84">np:O</ta>
            <ta e="T86" id="Seg_1318" s="T85">v:pred </ta>
            <ta e="T87" id="Seg_1319" s="T86">v:pred 0.3.h:S</ta>
            <ta e="T92" id="Seg_1320" s="T91">v:pred 0.2.h:S</ta>
            <ta e="T93" id="Seg_1321" s="T92">np:O</ta>
            <ta e="T94" id="Seg_1322" s="T93">pro.h:S</ta>
            <ta e="T97" id="Seg_1323" s="T96">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T1" id="Seg_1324" s="T0">new</ta>
            <ta e="T4" id="Seg_1325" s="T3">new</ta>
            <ta e="T6" id="Seg_1326" s="T5">giv-active</ta>
            <ta e="T7" id="Seg_1327" s="T6">accs-gen</ta>
            <ta e="T8" id="Seg_1328" s="T7">0.giv-active</ta>
            <ta e="T10" id="Seg_1329" s="T9">0.giv-active</ta>
            <ta e="T12" id="Seg_1330" s="T11">new</ta>
            <ta e="T14" id="Seg_1331" s="T13">giv-active-Q</ta>
            <ta e="T17" id="Seg_1332" s="T16">giv-inactive-Q</ta>
            <ta e="T18" id="Seg_1333" s="T17">0.giv-active-Q</ta>
            <ta e="T20" id="Seg_1334" s="T19">0.giv-active-Q</ta>
            <ta e="T21" id="Seg_1335" s="T20">giv-inactive-Q</ta>
            <ta e="T24" id="Seg_1336" s="T23">accs-Q</ta>
            <ta e="T27" id="Seg_1337" s="T26">0.giv-inactive</ta>
            <ta e="T28" id="Seg_1338" s="T27">new</ta>
            <ta e="T29" id="Seg_1339" s="T28">giv-active-Q</ta>
            <ta e="T30" id="Seg_1340" s="T29">giv-active-Q</ta>
            <ta e="T33" id="Seg_1341" s="T32">giv-inactive-Q</ta>
            <ta e="T34" id="Seg_1342" s="T33">giv-active-Q</ta>
            <ta e="T37" id="Seg_1343" s="T36">accs-gen-Q</ta>
            <ta e="T39" id="Seg_1344" s="T38">new</ta>
            <ta e="T40" id="Seg_1345" s="T39">0.giv-inactive</ta>
            <ta e="T41" id="Seg_1346" s="T40">giv-active-Q</ta>
            <ta e="T42" id="Seg_1347" s="T41">giv-active-Q</ta>
            <ta e="T43" id="Seg_1348" s="T42">accs-gen-Q</ta>
            <ta e="T46" id="Seg_1349" s="T45">giv-active-Q</ta>
            <ta e="T48" id="Seg_1350" s="T47">accs-sit-Q</ta>
            <ta e="T49" id="Seg_1351" s="T48">accs-sit-Q</ta>
            <ta e="T52" id="Seg_1352" s="T51">0.giv-inactive</ta>
            <ta e="T54" id="Seg_1353" s="T53">new</ta>
            <ta e="T56" id="Seg_1354" s="T55">giv-active-Q</ta>
            <ta e="T59" id="Seg_1355" s="T58">new-Q</ta>
            <ta e="T61" id="Seg_1356" s="T60">giv-active-Q</ta>
            <ta e="T65" id="Seg_1357" s="T64">giv-active-Q</ta>
            <ta e="T69" id="Seg_1358" s="T68">accs-sit-Q</ta>
            <ta e="T71" id="Seg_1359" s="T70">new</ta>
            <ta e="T72" id="Seg_1360" s="T71">0.giv-inactive</ta>
            <ta e="T73" id="Seg_1361" s="T72">giv-active-Q</ta>
            <ta e="T74" id="Seg_1362" s="T73">quot-sp</ta>
            <ta e="T75" id="Seg_1363" s="T74">giv-active-Q</ta>
            <ta e="T78" id="Seg_1364" s="T77">giv-inactive-Q</ta>
            <ta e="T79" id="Seg_1365" s="T78">giv-inactive-Q</ta>
            <ta e="T81" id="Seg_1366" s="T80">0.giv-active-Q</ta>
            <ta e="T82" id="Seg_1367" s="T81">giv-active-Q</ta>
            <ta e="T85" id="Seg_1368" s="T84">new-Q</ta>
            <ta e="T87" id="Seg_1369" s="T86">0.giv-inactive</ta>
            <ta e="T88" id="Seg_1370" s="T87">new</ta>
            <ta e="T89" id="Seg_1371" s="T88">giv-active-Q</ta>
            <ta e="T90" id="Seg_1372" s="T89">giv-active-Q</ta>
            <ta e="T93" id="Seg_1373" s="T92">giv-inactive-Q</ta>
            <ta e="T94" id="Seg_1374" s="T93">giv-active-Q</ta>
            <ta e="T96" id="Seg_1375" s="T95">accs-inf-Q</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T19" id="Seg_1376" s="T18">TURK:disc</ta>
            <ta e="T35" id="Seg_1377" s="T34">TURK:mod(PTCL)</ta>
            <ta e="T44" id="Seg_1378" s="T43">TURK:disc</ta>
            <ta e="T80" id="Seg_1379" s="T79">TURK:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_1380" s="T0">Заяц ходил, ходил.</ta>
            <ta e="T8" id="Seg_1381" s="T3">Пришел к степной траве, о степную траву нос себе порезал. </ta>
            <ta e="T12" id="Seg_1382" s="T8">Тогда пошел он к пожару:</ta>
            <ta e="T17" id="Seg_1383" s="T12">"Пожар, сожги степную траву!</ta>
            <ta e="T20" id="Seg_1384" s="T17">Сожги её, всю выжги!"</ta>
            <ta e="T25" id="Seg_1385" s="T20">"У меня и без того полно земли, чтобы гореть."</ta>
            <ta e="T28" id="Seg_1386" s="T25">Пошел он тогда к воде.</ta>
            <ta e="T33" id="Seg_1387" s="T28">"Вода, вода, потуши пожар!"</ta>
            <ta e="T38" id="Seg_1388" s="T33">"У меня и без того полно земли, чтобы течь."</ta>
            <ta e="T40" id="Seg_1389" s="T38">К лосю пошел:</ta>
            <ta e="T45" id="Seg_1390" s="T40">"Лось, лось, выпей всю воду!"</ta>
            <ta e="T50" id="Seg_1391" s="T45">"Я ту воду пью, которая у меня под ногами."</ta>
            <ta e="T54" id="Seg_1392" s="T50">Пошел он тогда к старухам:</ta>
            <ta e="T60" id="Seg_1393" s="T54">"Старухи, идите и вырежьте у лося сухожилия!"</ta>
            <ta e="T70" id="Seg_1394" s="T60">"Не нужны нам лосиные сухожилия, у нас и так много крепких сухожилий."</ta>
            <ta e="T72" id="Seg_1395" s="T70">Пошел он к мышам.</ta>
            <ta e="T74" id="Seg_1396" s="T72">"Мыши!", говорит,</ta>
            <ta e="T81" id="Seg_1397" s="T74">"Мыши, пойдите к старухам, съешьте у них все сухожилия!"</ta>
            <ta e="T86" id="Seg_1398" s="T81">"Мы корни своей травы едим."</ta>
            <ta e="T88" id="Seg_1399" s="T86">Пошел он к мальчикам:</ta>
            <ta e="T93" id="Seg_1400" s="T88">"Мальчики, мальчики, пойдите убейте мышей!"</ta>
            <ta e="T97" id="Seg_1401" s="T93">"Мы в свои бабки играем."</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_1402" s="T0">A hare walked and walked.</ta>
            <ta e="T8" id="Seg_1403" s="T3">He met steppe grass, on the steppe grass he cut his nose.</ta>
            <ta e="T12" id="Seg_1404" s="T8">Then he went to the wildfire.</ta>
            <ta e="T17" id="Seg_1405" s="T12">"Wildfire, burn the steppe grass!</ta>
            <ta e="T20" id="Seg_1406" s="T17">Make it burn, eat it all up!"</ta>
            <ta e="T25" id="Seg_1407" s="T20">"I have a lot of land to burn without this."</ta>
            <ta e="T28" id="Seg_1408" s="T25">Then he went to the river.</ta>
            <ta e="T33" id="Seg_1409" s="T28">"Water, water extinguish the wildfire!"</ta>
            <ta e="T38" id="Seg_1410" s="T33">I have a lot of land to flow for free, haven’t I?</ta>
            <ta e="T40" id="Seg_1411" s="T38">He went to the moose.</ta>
            <ta e="T45" id="Seg_1412" s="T40">"Moose, moose, drink up all the waters!"</ta>
            <ta e="T50" id="Seg_1413" s="T45">"I’m drinking waters down at my feet."</ta>
            <ta e="T54" id="Seg_1414" s="T50">Then he went to the old women.</ta>
            <ta e="T60" id="Seg_1415" s="T54">"Old women, go and pull the moose’s sinew out!"</ta>
            <ta e="T70" id="Seg_1416" s="T60">"We don’t need the moose’s sinews, we have a lot of our own robust sinews."</ta>
            <ta e="T72" id="Seg_1417" s="T70">He went to the mice.</ta>
            <ta e="T74" id="Seg_1418" s="T72">"Mice", he says.</ta>
            <ta e="T81" id="Seg_1419" s="T74">"Mice, go to the old women and eat up all their sinews!"</ta>
            <ta e="T86" id="Seg_1420" s="T81">"We eat our own grass roots."</ta>
            <ta e="T88" id="Seg_1421" s="T86">He went to the boys.</ta>
            <ta e="T93" id="Seg_1422" s="T88">"Boys, boys, go and kill the mice!"</ta>
            <ta e="T97" id="Seg_1423" s="T93">"We ourselves are playing with knuckles."</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_1424" s="T0">Ein Hase wanderte und wanderte.</ta>
            <ta e="T8" id="Seg_1425" s="T3">Er kam zum Steppengras. An dem Steppengras schnitt er sich die Nase.</ta>
            <ta e="T12" id="Seg_1426" s="T8">Dann ging er zum Lauffeuer:</ta>
            <ta e="T17" id="Seg_1427" s="T12">"Lauffeuer, friss dieses Steppengras!</ta>
            <ta e="T20" id="Seg_1428" s="T17">Steck es in Brand! Friss es ganz auf!"</ta>
            <ta e="T25" id="Seg_1429" s="T20">"Ich habe auch so genug Land zum Verbrennen."</ta>
            <ta e="T28" id="Seg_1430" s="T25">Dann ging er zum Wasser.</ta>
            <ta e="T33" id="Seg_1431" s="T28">"Wasser, Wasser, lösche das Lauffeuer!"</ta>
            <ta e="T38" id="Seg_1432" s="T33">Ich habe genug Land zum Umherfließen."</ta>
            <ta e="T40" id="Seg_1433" s="T38">Er ging zum Elch:</ta>
            <ta e="T45" id="Seg_1434" s="T40">"Elch, Elch, trink alle Gewässer leer!"</ta>
            <ta e="T50" id="Seg_1435" s="T45">"Ich trinke die Wasser zu meinen Füßen."</ta>
            <ta e="T54" id="Seg_1436" s="T50">Dann ging er zu den alten Frauen:</ta>
            <ta e="T60" id="Seg_1437" s="T54">"Geht, alte Frauen! Reißt dem Elch die Sehnen aus!"</ta>
            <ta e="T70" id="Seg_1438" s="T60">"Wir brauchen die Sehne des Elches nicht. Wir haben genügend eigene, robuste Sehnen."</ta>
            <ta e="T72" id="Seg_1439" s="T70">Er ging zu den Mäusen.</ta>
            <ta e="T74" id="Seg_1440" s="T72">"Mäuse", sagt er,</ta>
            <ta e="T81" id="Seg_1441" s="T74">"Mäuse, geht zu den alten Frauen und fresst alle ihre Sehnen auf!"</ta>
            <ta e="T86" id="Seg_1442" s="T81">"Wir fressen unsere eigenen Graswurzeln."</ta>
            <ta e="T88" id="Seg_1443" s="T86">Er ging zu den Jungs:</ta>
            <ta e="T93" id="Seg_1444" s="T88">"Jungs, Jungs, geht und tötet die Mäuse!"</ta>
            <ta e="T97" id="Seg_1445" s="T93">"Wir spielen mit unseren eigenen Mäuseknöchelchen."</ta>
         </annotation>
         <annotation name="ltg" tierref="ltg">
            <ta e="T3" id="Seg_1446" s="T0">Ein Hase wanderte, wanderte.</ta>
            <ta e="T8" id="Seg_1447" s="T3">Gras kam [ihm] entgegen, an dem Gras schnitt er sich aber die Nase ab.</ta>
            <ta e="T12" id="Seg_1448" s="T8">Dann begab er sich zum Lauffeuer:</ta>
            <ta e="T17" id="Seg_1449" s="T12">»Heidebrand; friss das Gras!</ta>
            <ta e="T20" id="Seg_1450" s="T17">Zünde es an! Friss alles!»</ta>
            <ta e="T25" id="Seg_1451" s="T20">»Ich habe viel Land, um es brennen zu lassen.»</ta>
            <ta e="T28" id="Seg_1452" s="T25">Darauf begab er sich zum Wasser.</ta>
            <ta e="T33" id="Seg_1453" s="T28">»Wasser, Wasser, lösche das Lauffeuer!»</ta>
            <ta e="T38" id="Seg_1454" s="T33">»Ich habe viel Land, um das Gras entlang zu laufen.»</ta>
            <ta e="T40" id="Seg_1455" s="T38">Zum Elch begab er sich:</ta>
            <ta e="T45" id="Seg_1456" s="T40">»Elch, Elch, trinke alle Gewässer!»</ta>
            <ta e="T50" id="Seg_1457" s="T45">»Ich trinke unter meinem Fuss Wasser trinkend.»</ta>
            <ta e="T54" id="Seg_1458" s="T50">Dann begab er sich zu den alten Weibern:</ta>
            <ta e="T60" id="Seg_1459" s="T54">»Alte, gehe! Ziehet dem Elch die Sehnen weg!»</ta>
            <ta e="T70" id="Seg_1460" s="T60">»Wir brauchen nicht die Sehne des Elches. Von seiner harten Sehne haben wir viel.»</ta>
            <ta e="T72" id="Seg_1461" s="T70">Zu den Mäusen begab er sich;</ta>
            <ta e="T74" id="Seg_1462" s="T72">die Maus geht nicht.</ta>
            <ta e="T81" id="Seg_1463" s="T74">»Ihr Mäuse, gehend die Sehnen der alten Weiber esset alle!»</ta>
            <ta e="T86" id="Seg_1464" s="T81">Wir essen an der Wurzel unseres eigenen Grases.»</ta>
            <ta e="T88" id="Seg_1465" s="T86">Er begab sich zu den Kindern:</ta>
            <ta e="T93" id="Seg_1466" s="T88">»Kinder, Kinder, gehend tötet die Mäuse!»</ta>
            <ta e="T97" id="Seg_1467" s="T93">»Wir spielen selbst mit dem Würfel.»</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltg"
                          display-name="ltg"
                          name="ltg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
