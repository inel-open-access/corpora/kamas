<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDA2FD0E92-1D88-E68C-0D30-29700578E8DD">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_Cauldron_flk.wav" />
         <referenced-file url="PKZ_196X_Cauldron_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_Cauldron_flk\PKZ_196X_Cauldron_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">99</ud-information>
            <ud-information attribute-name="# HIAT:w">60</ud-information>
            <ud-information attribute-name="# e">60</ud-information>
            <ud-information attribute-name="# HIAT:u">16</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.009" type="appl" />
         <tli id="T1" time="0.631" type="appl" />
         <tli id="T2" time="1.253" type="appl" />
         <tli id="T3" time="1.8949993094520126" />
         <tli id="T4" time="2.446" type="appl" />
         <tli id="T5" time="3.016" type="appl" />
         <tli id="T6" time="3.586" type="appl" />
         <tli id="T7" time="4.156" type="appl" />
         <tli id="T8" time="4.726" type="appl" />
         <tli id="T9" time="5.296" type="appl" />
         <tli id="T10" time="5.866" type="appl" />
         <tli id="T11" time="6.42" type="appl" />
         <tli id="T12" time="6.974" type="appl" />
         <tli id="T13" time="7.527" type="appl" />
         <tli id="T14" time="8.313075939586605" />
         <tli id="T15" time="9.11" type="appl" />
         <tli id="T16" time="9.91969286135114" />
         <tli id="T17" time="10.705" type="appl" />
         <tli id="T18" time="11.311" type="appl" />
         <tli id="T19" time="11.916" type="appl" />
         <tli id="T20" time="12.522" type="appl" />
         <tli id="T21" time="13.127" type="appl" />
         <tli id="T22" time="14.492" type="appl" />
         <tli id="T23" time="15.485" type="appl" />
         <tli id="T24" time="16.478" type="appl" />
         <tli id="T25" time="17.47" type="appl" />
         <tli id="T26" time="18.463" type="appl" />
         <tli id="T27" time="19.456" type="appl" />
         <tli id="T28" time="20.089" type="appl" />
         <tli id="T29" time="20.651" type="appl" />
         <tli id="T30" time="21.212" type="appl" />
         <tli id="T31" time="21.846" type="appl" />
         <tli id="T32" time="22.48" type="appl" />
         <tli id="T33" time="23.114" type="appl" />
         <tli id="T34" time="23.724" type="appl" />
         <tli id="T35" time="24.585905425176747" />
         <tli id="T36" time="25.42" type="appl" />
         <tli id="T37" time="26.33918447257954" />
         <tli id="T38" time="26.976" type="appl" />
         <tli id="T39" time="27.596" type="appl" />
         <tli id="T40" time="28.216" type="appl" />
         <tli id="T41" time="28.836" type="appl" />
         <tli id="T42" time="29.456" type="appl" />
         <tli id="T43" time="30.75904762249607" />
         <tli id="T44" time="31.408" type="appl" />
         <tli id="T45" time="31.989" type="appl" />
         <tli id="T46" time="32.6" type="appl" />
         <tli id="T47" time="33.211" type="appl" />
         <tli id="T48" time="33.822" type="appl" />
         <tli id="T49" time="35.50556732497054" />
         <tli id="T50" time="36.222" type="appl" />
         <tli id="T51" time="36.803" type="appl" />
         <tli id="T52" time="37.385" type="appl" />
         <tli id="T53" time="37.966" type="appl" />
         <tli id="T54" time="38.548" type="appl" />
         <tli id="T55" time="39.13" type="appl" />
         <tli id="T56" time="39.711" type="appl" />
         <tli id="T57" time="40.378749772314414" />
         <tli id="T58" time="41.096" type="appl" />
         <tli id="T59" time="41.764" type="appl" />
         <tli id="T60" time="42.432" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T60" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Šide</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">kuza</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">amnobiʔi</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T5" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Onʼiʔ</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">šobi</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_23" n="HIAT:u" s="T5">
                  <nts id="Seg_24" n="HIAT:ip">"</nts>
                  <ts e="T6" id="Seg_26" n="HIAT:w" s="T5">Deʔ</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_29" n="HIAT:w" s="T6">măna</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_32" n="HIAT:w" s="T7">aspaʔ</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_35" n="HIAT:w" s="T8">uja</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_38" n="HIAT:w" s="T9">mĭnzərzittə</ts>
                  <nts id="Seg_39" n="HIAT:ip">!</nts>
                  <nts id="Seg_40" n="HIAT:ip">"</nts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_43" n="HIAT:u" s="T10">
                  <nts id="Seg_44" n="HIAT:ip">(</nts>
                  <ts e="T11" id="Seg_46" n="HIAT:w" s="T10">Dĭ-</ts>
                  <nts id="Seg_47" n="HIAT:ip">)</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_50" n="HIAT:w" s="T11">Dĭʔnə</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_53" n="HIAT:w" s="T12">mĭbi</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_56" n="HIAT:w" s="T13">aspaʔ</ts>
                  <nts id="Seg_57" n="HIAT:ip">.</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_60" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_62" n="HIAT:w" s="T14">Dĭ</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_65" n="HIAT:w" s="T15">mĭnzərbi</ts>
                  <nts id="Seg_66" n="HIAT:ip">.</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_69" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_71" n="HIAT:w" s="T16">I</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_74" n="HIAT:w" s="T17">deʔpi</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_77" n="HIAT:w" s="T18">üdʼüge</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_80" n="HIAT:w" s="T19">aspaʔ</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_83" n="HIAT:w" s="T20">diʔnə</ts>
                  <nts id="Seg_84" n="HIAT:ip">.</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_87" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_89" n="HIAT:w" s="T21">Dĭ</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_91" n="HIAT:ip">(</nts>
                  <ts e="T23" id="Seg_93" n="HIAT:w" s="T22">de-</ts>
                  <nts id="Seg_94" n="HIAT:ip">)</nts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_97" n="HIAT:w" s="T23">deʔpi</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_100" n="HIAT:w" s="T24">iššo</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_103" n="HIAT:w" s="T25">onʼiʔ</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_106" n="HIAT:w" s="T26">aspaʔ</ts>
                  <nts id="Seg_107" n="HIAT:ip">.</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_110" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_112" n="HIAT:w" s="T27">Dĭgəttə</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_115" n="HIAT:w" s="T28">bazoʔ</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_118" n="HIAT:w" s="T29">šonəga</ts>
                  <nts id="Seg_119" n="HIAT:ip">.</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_122" n="HIAT:u" s="T30">
                  <nts id="Seg_123" n="HIAT:ip">"</nts>
                  <ts e="T31" id="Seg_125" n="HIAT:w" s="T30">Deʔ</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_128" n="HIAT:w" s="T31">aspaʔ</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_131" n="HIAT:w" s="T32">măna</ts>
                  <nts id="Seg_132" n="HIAT:ip">!</nts>
                  <nts id="Seg_133" n="HIAT:ip">"</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_136" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_138" n="HIAT:w" s="T33">Dĭ</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_141" n="HIAT:w" s="T34">mĭbi</ts>
                  <nts id="Seg_142" n="HIAT:ip">.</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_145" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_147" n="HIAT:w" s="T35">Naga</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_150" n="HIAT:w" s="T36">aspaʔ</ts>
                  <nts id="Seg_151" n="HIAT:ip">!</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_154" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_156" n="HIAT:w" s="T37">Ej</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_159" n="HIAT:w" s="T38">šolia</ts>
                  <nts id="Seg_160" n="HIAT:ip">,</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_162" n="HIAT:ip">(</nts>
                  <ts e="T40" id="Seg_164" n="HIAT:w" s="T39">nagur=</ts>
                  <nts id="Seg_165" n="HIAT:ip">)</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_168" n="HIAT:w" s="T40">nagur</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_171" n="HIAT:w" s="T41">dʼala</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_174" n="HIAT:w" s="T42">naga</ts>
                  <nts id="Seg_175" n="HIAT:ip">.</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_178" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_180" n="HIAT:w" s="T43">No</ts>
                  <nts id="Seg_181" n="HIAT:ip">,</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_184" n="HIAT:w" s="T44">măndə</ts>
                  <nts id="Seg_185" n="HIAT:ip">.</nts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_188" n="HIAT:u" s="T45">
                  <nts id="Seg_189" n="HIAT:ip">"</nts>
                  <ts e="T46" id="Seg_191" n="HIAT:w" s="T45">Urgo</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_194" n="HIAT:w" s="T46">aspaʔ</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_197" n="HIAT:w" s="T47">detlie</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_200" n="HIAT:w" s="T48">naverna</ts>
                  <nts id="Seg_201" n="HIAT:ip">"</nts>
                  <nts id="Seg_202" n="HIAT:ip">.</nts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_205" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_207" n="HIAT:w" s="T49">Edəʔpi</ts>
                  <nts id="Seg_208" n="HIAT:ip">,</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_211" n="HIAT:w" s="T50">edəʔpi</ts>
                  <nts id="Seg_212" n="HIAT:ip">,</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_215" n="HIAT:w" s="T51">dĭgəttə</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_218" n="HIAT:w" s="T52">kandəga:</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_220" n="HIAT:ip">"</nts>
                  <ts e="T54" id="Seg_222" n="HIAT:w" s="T53">Gijen</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_224" n="HIAT:ip">(</nts>
                  <ts e="T55" id="Seg_226" n="HIAT:w" s="T54">măn</ts>
                  <nts id="Seg_227" n="HIAT:ip">)</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_230" n="HIAT:w" s="T55">măn</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_233" n="HIAT:w" s="T56">aspaʔ</ts>
                  <nts id="Seg_234" n="HIAT:ip">?</nts>
                  <nts id="Seg_235" n="HIAT:ip">"</nts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_238" n="HIAT:u" s="T57">
                  <nts id="Seg_239" n="HIAT:ip">"</nts>
                  <ts e="T58" id="Seg_241" n="HIAT:w" s="T57">Dĭ</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_244" n="HIAT:w" s="T58">külaːmbi</ts>
                  <nts id="Seg_245" n="HIAT:ip">,</nts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_248" n="HIAT:w" s="T59">naga</ts>
                  <nts id="Seg_249" n="HIAT:ip">"</nts>
                  <nts id="Seg_250" n="HIAT:ip">.</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T60" id="Seg_252" n="sc" s="T0">
               <ts e="T1" id="Seg_254" n="e" s="T0">Šide </ts>
               <ts e="T2" id="Seg_256" n="e" s="T1">kuza </ts>
               <ts e="T3" id="Seg_258" n="e" s="T2">amnobiʔi. </ts>
               <ts e="T4" id="Seg_260" n="e" s="T3">Onʼiʔ </ts>
               <ts e="T5" id="Seg_262" n="e" s="T4">šobi. </ts>
               <ts e="T6" id="Seg_264" n="e" s="T5">"Deʔ </ts>
               <ts e="T7" id="Seg_266" n="e" s="T6">măna </ts>
               <ts e="T8" id="Seg_268" n="e" s="T7">aspaʔ </ts>
               <ts e="T9" id="Seg_270" n="e" s="T8">uja </ts>
               <ts e="T10" id="Seg_272" n="e" s="T9">mĭnzərzittə!" </ts>
               <ts e="T11" id="Seg_274" n="e" s="T10">(Dĭ-) </ts>
               <ts e="T12" id="Seg_276" n="e" s="T11">Dĭʔnə </ts>
               <ts e="T13" id="Seg_278" n="e" s="T12">mĭbi </ts>
               <ts e="T14" id="Seg_280" n="e" s="T13">aspaʔ. </ts>
               <ts e="T15" id="Seg_282" n="e" s="T14">Dĭ </ts>
               <ts e="T16" id="Seg_284" n="e" s="T15">mĭnzərbi. </ts>
               <ts e="T17" id="Seg_286" n="e" s="T16">I </ts>
               <ts e="T18" id="Seg_288" n="e" s="T17">deʔpi </ts>
               <ts e="T19" id="Seg_290" n="e" s="T18">üdʼüge </ts>
               <ts e="T20" id="Seg_292" n="e" s="T19">aspaʔ </ts>
               <ts e="T21" id="Seg_294" n="e" s="T20">diʔnə. </ts>
               <ts e="T22" id="Seg_296" n="e" s="T21">Dĭ </ts>
               <ts e="T23" id="Seg_298" n="e" s="T22">(de-) </ts>
               <ts e="T24" id="Seg_300" n="e" s="T23">deʔpi </ts>
               <ts e="T25" id="Seg_302" n="e" s="T24">iššo </ts>
               <ts e="T26" id="Seg_304" n="e" s="T25">onʼiʔ </ts>
               <ts e="T27" id="Seg_306" n="e" s="T26">aspaʔ. </ts>
               <ts e="T28" id="Seg_308" n="e" s="T27">Dĭgəttə </ts>
               <ts e="T29" id="Seg_310" n="e" s="T28">bazoʔ </ts>
               <ts e="T30" id="Seg_312" n="e" s="T29">šonəga. </ts>
               <ts e="T31" id="Seg_314" n="e" s="T30">"Deʔ </ts>
               <ts e="T32" id="Seg_316" n="e" s="T31">aspaʔ </ts>
               <ts e="T33" id="Seg_318" n="e" s="T32">măna!" </ts>
               <ts e="T34" id="Seg_320" n="e" s="T33">Dĭ </ts>
               <ts e="T35" id="Seg_322" n="e" s="T34">mĭbi. </ts>
               <ts e="T36" id="Seg_324" n="e" s="T35">Naga </ts>
               <ts e="T37" id="Seg_326" n="e" s="T36">aspaʔ! </ts>
               <ts e="T38" id="Seg_328" n="e" s="T37">Ej </ts>
               <ts e="T39" id="Seg_330" n="e" s="T38">šolia, </ts>
               <ts e="T40" id="Seg_332" n="e" s="T39">(nagur=) </ts>
               <ts e="T41" id="Seg_334" n="e" s="T40">nagur </ts>
               <ts e="T42" id="Seg_336" n="e" s="T41">dʼala </ts>
               <ts e="T43" id="Seg_338" n="e" s="T42">naga. </ts>
               <ts e="T44" id="Seg_340" n="e" s="T43">No, </ts>
               <ts e="T45" id="Seg_342" n="e" s="T44">măndə. </ts>
               <ts e="T46" id="Seg_344" n="e" s="T45">"Urgo </ts>
               <ts e="T47" id="Seg_346" n="e" s="T46">aspaʔ </ts>
               <ts e="T48" id="Seg_348" n="e" s="T47">detlie </ts>
               <ts e="T49" id="Seg_350" n="e" s="T48">naverna". </ts>
               <ts e="T50" id="Seg_352" n="e" s="T49">Edəʔpi, </ts>
               <ts e="T51" id="Seg_354" n="e" s="T50">edəʔpi, </ts>
               <ts e="T52" id="Seg_356" n="e" s="T51">dĭgəttə </ts>
               <ts e="T53" id="Seg_358" n="e" s="T52">kandəga: </ts>
               <ts e="T54" id="Seg_360" n="e" s="T53">"Gijen </ts>
               <ts e="T55" id="Seg_362" n="e" s="T54">(măn) </ts>
               <ts e="T56" id="Seg_364" n="e" s="T55">măn </ts>
               <ts e="T57" id="Seg_366" n="e" s="T56">aspaʔ?" </ts>
               <ts e="T58" id="Seg_368" n="e" s="T57">"Dĭ </ts>
               <ts e="T59" id="Seg_370" n="e" s="T58">külaːmbi, </ts>
               <ts e="T60" id="Seg_372" n="e" s="T59">naga". </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_373" s="T0">PKZ_196X_Cauldron_flk.001 (001)</ta>
            <ta e="T5" id="Seg_374" s="T3">PKZ_196X_Cauldron_flk.002 (002)</ta>
            <ta e="T10" id="Seg_375" s="T5">PKZ_196X_Cauldron_flk.003 (003)</ta>
            <ta e="T14" id="Seg_376" s="T10">PKZ_196X_Cauldron_flk.004 (004)</ta>
            <ta e="T16" id="Seg_377" s="T14">PKZ_196X_Cauldron_flk.005 (005)</ta>
            <ta e="T21" id="Seg_378" s="T16">PKZ_196X_Cauldron_flk.006 (006)</ta>
            <ta e="T27" id="Seg_379" s="T21">PKZ_196X_Cauldron_flk.007 (007)</ta>
            <ta e="T30" id="Seg_380" s="T27">PKZ_196X_Cauldron_flk.008 (008)</ta>
            <ta e="T33" id="Seg_381" s="T30">PKZ_196X_Cauldron_flk.009 (009)</ta>
            <ta e="T35" id="Seg_382" s="T33">PKZ_196X_Cauldron_flk.010 (010)</ta>
            <ta e="T37" id="Seg_383" s="T35">PKZ_196X_Cauldron_flk.011 (011)</ta>
            <ta e="T43" id="Seg_384" s="T37">PKZ_196X_Cauldron_flk.012 (012)</ta>
            <ta e="T45" id="Seg_385" s="T43">PKZ_196X_Cauldron_flk.013 (013)</ta>
            <ta e="T49" id="Seg_386" s="T45">PKZ_196X_Cauldron_flk.014 (014)</ta>
            <ta e="T57" id="Seg_387" s="T49">PKZ_196X_Cauldron_flk.015 (015)</ta>
            <ta e="T60" id="Seg_388" s="T57">PKZ_196X_Cauldron_flk.016 (016)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_389" s="T0">Šide kuza amnobiʔi. </ta>
            <ta e="T5" id="Seg_390" s="T3">Onʼiʔ šobi. </ta>
            <ta e="T10" id="Seg_391" s="T5">"Deʔ măna aspaʔ uja mĭnzərzittə!" </ta>
            <ta e="T14" id="Seg_392" s="T10">(Dĭ-) Dĭʔnə mĭbi aspaʔ. </ta>
            <ta e="T16" id="Seg_393" s="T14">Dĭ mĭnzərbi. </ta>
            <ta e="T21" id="Seg_394" s="T16">I deʔpi üdʼüge aspaʔ diʔnə. </ta>
            <ta e="T27" id="Seg_395" s="T21">Dĭ (de-) deʔpi iššo onʼiʔ aspaʔ. </ta>
            <ta e="T30" id="Seg_396" s="T27">Dĭgəttə bazoʔ šonəga. </ta>
            <ta e="T33" id="Seg_397" s="T30">"Deʔ aspaʔ măna!" </ta>
            <ta e="T35" id="Seg_398" s="T33">Dĭ mĭbi. </ta>
            <ta e="T37" id="Seg_399" s="T35">Naga aspaʔ! </ta>
            <ta e="T43" id="Seg_400" s="T37">Ej šolia, (nagur=) nagur dʼala naga. </ta>
            <ta e="T45" id="Seg_401" s="T43">No, măndə. </ta>
            <ta e="T49" id="Seg_402" s="T45">"Urgo aspaʔ detlie naverna". </ta>
            <ta e="T57" id="Seg_403" s="T49">Edəʔpi, edəʔpi, dĭgəttə kandəga:" Gijen (măn) măn aspaʔ?" </ta>
            <ta e="T60" id="Seg_404" s="T57">"Dĭ külaːmbi, naga". </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_405" s="T0">šide</ta>
            <ta e="T2" id="Seg_406" s="T1">kuza</ta>
            <ta e="T3" id="Seg_407" s="T2">amno-bi-ʔi</ta>
            <ta e="T4" id="Seg_408" s="T3">onʼiʔ</ta>
            <ta e="T5" id="Seg_409" s="T4">šo-bi</ta>
            <ta e="T6" id="Seg_410" s="T5">de-ʔ</ta>
            <ta e="T7" id="Seg_411" s="T6">măna</ta>
            <ta e="T8" id="Seg_412" s="T7">aspaʔ</ta>
            <ta e="T9" id="Seg_413" s="T8">uja</ta>
            <ta e="T10" id="Seg_414" s="T9">mĭnzər-zittə</ta>
            <ta e="T12" id="Seg_415" s="T11">dĭʔ-nə</ta>
            <ta e="T13" id="Seg_416" s="T12">mĭ-bi</ta>
            <ta e="T14" id="Seg_417" s="T13">aspaʔ</ta>
            <ta e="T15" id="Seg_418" s="T14">dĭ</ta>
            <ta e="T16" id="Seg_419" s="T15">mĭnzər-bi</ta>
            <ta e="T17" id="Seg_420" s="T16">i</ta>
            <ta e="T18" id="Seg_421" s="T17">deʔ-pi</ta>
            <ta e="T19" id="Seg_422" s="T18">üdʼüge</ta>
            <ta e="T20" id="Seg_423" s="T19">aspaʔ</ta>
            <ta e="T21" id="Seg_424" s="T20">diʔ-nə</ta>
            <ta e="T22" id="Seg_425" s="T21">dĭ</ta>
            <ta e="T24" id="Seg_426" s="T23">deʔ-pi</ta>
            <ta e="T25" id="Seg_427" s="T24">iššo</ta>
            <ta e="T26" id="Seg_428" s="T25">onʼiʔ</ta>
            <ta e="T27" id="Seg_429" s="T26">aspaʔ</ta>
            <ta e="T28" id="Seg_430" s="T27">dĭgəttə</ta>
            <ta e="T29" id="Seg_431" s="T28">bazoʔ</ta>
            <ta e="T30" id="Seg_432" s="T29">šonə-ga</ta>
            <ta e="T31" id="Seg_433" s="T30">de-ʔ</ta>
            <ta e="T32" id="Seg_434" s="T31">aspaʔ</ta>
            <ta e="T33" id="Seg_435" s="T32">măna</ta>
            <ta e="T34" id="Seg_436" s="T33">dĭ</ta>
            <ta e="T35" id="Seg_437" s="T34">mĭ-bi</ta>
            <ta e="T36" id="Seg_438" s="T35">naga</ta>
            <ta e="T37" id="Seg_439" s="T36">aspaʔ</ta>
            <ta e="T38" id="Seg_440" s="T37">ej</ta>
            <ta e="T39" id="Seg_441" s="T38">šo-lia</ta>
            <ta e="T40" id="Seg_442" s="T39">nagur</ta>
            <ta e="T41" id="Seg_443" s="T40">nagur</ta>
            <ta e="T42" id="Seg_444" s="T41">dʼala</ta>
            <ta e="T43" id="Seg_445" s="T42">naga</ta>
            <ta e="T44" id="Seg_446" s="T43">no</ta>
            <ta e="T45" id="Seg_447" s="T44">măn-də</ta>
            <ta e="T46" id="Seg_448" s="T45">urgo</ta>
            <ta e="T47" id="Seg_449" s="T46">aspaʔ</ta>
            <ta e="T48" id="Seg_450" s="T47">det-lie</ta>
            <ta e="T49" id="Seg_451" s="T48">naverna</ta>
            <ta e="T50" id="Seg_452" s="T49">edəʔ-pi</ta>
            <ta e="T51" id="Seg_453" s="T50">edəʔ-pi</ta>
            <ta e="T52" id="Seg_454" s="T51">dĭgəttə</ta>
            <ta e="T53" id="Seg_455" s="T52">kandə-ga</ta>
            <ta e="T54" id="Seg_456" s="T53">gijen</ta>
            <ta e="T55" id="Seg_457" s="T54">măn</ta>
            <ta e="T56" id="Seg_458" s="T55">măn</ta>
            <ta e="T57" id="Seg_459" s="T56">aspaʔ</ta>
            <ta e="T58" id="Seg_460" s="T57">dĭ</ta>
            <ta e="T59" id="Seg_461" s="T58">kü-laːm-bi</ta>
            <ta e="T60" id="Seg_462" s="T59">naga</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_463" s="T0">šide</ta>
            <ta e="T2" id="Seg_464" s="T1">kuza</ta>
            <ta e="T3" id="Seg_465" s="T2">amno-bi-jəʔ</ta>
            <ta e="T4" id="Seg_466" s="T3">onʼiʔ</ta>
            <ta e="T5" id="Seg_467" s="T4">šo-bi</ta>
            <ta e="T6" id="Seg_468" s="T5">det-ʔ</ta>
            <ta e="T7" id="Seg_469" s="T6">măna</ta>
            <ta e="T8" id="Seg_470" s="T7">aspaʔ</ta>
            <ta e="T9" id="Seg_471" s="T8">uja</ta>
            <ta e="T10" id="Seg_472" s="T9">mĭnzər-zittə</ta>
            <ta e="T12" id="Seg_473" s="T11">dĭ-Tə</ta>
            <ta e="T13" id="Seg_474" s="T12">mĭ-bi</ta>
            <ta e="T14" id="Seg_475" s="T13">aspaʔ</ta>
            <ta e="T15" id="Seg_476" s="T14">dĭ</ta>
            <ta e="T16" id="Seg_477" s="T15">mĭnzər-bi</ta>
            <ta e="T17" id="Seg_478" s="T16">i</ta>
            <ta e="T18" id="Seg_479" s="T17">det-bi</ta>
            <ta e="T19" id="Seg_480" s="T18">üdʼüge</ta>
            <ta e="T20" id="Seg_481" s="T19">aspaʔ</ta>
            <ta e="T21" id="Seg_482" s="T20">dĭ-Tə</ta>
            <ta e="T22" id="Seg_483" s="T21">dĭ</ta>
            <ta e="T24" id="Seg_484" s="T23">det-bi</ta>
            <ta e="T25" id="Seg_485" s="T24">ĭššo</ta>
            <ta e="T26" id="Seg_486" s="T25">onʼiʔ</ta>
            <ta e="T27" id="Seg_487" s="T26">aspaʔ</ta>
            <ta e="T28" id="Seg_488" s="T27">dĭgəttə</ta>
            <ta e="T29" id="Seg_489" s="T28">bazoʔ</ta>
            <ta e="T30" id="Seg_490" s="T29">šonə-gA</ta>
            <ta e="T31" id="Seg_491" s="T30">det-ʔ</ta>
            <ta e="T32" id="Seg_492" s="T31">aspaʔ</ta>
            <ta e="T33" id="Seg_493" s="T32">măna</ta>
            <ta e="T34" id="Seg_494" s="T33">dĭ</ta>
            <ta e="T35" id="Seg_495" s="T34">mĭ-bi</ta>
            <ta e="T36" id="Seg_496" s="T35">naga</ta>
            <ta e="T37" id="Seg_497" s="T36">aspaʔ</ta>
            <ta e="T38" id="Seg_498" s="T37">ej</ta>
            <ta e="T39" id="Seg_499" s="T38">šo-liA</ta>
            <ta e="T40" id="Seg_500" s="T39">nagur</ta>
            <ta e="T41" id="Seg_501" s="T40">nagur</ta>
            <ta e="T42" id="Seg_502" s="T41">tʼala</ta>
            <ta e="T43" id="Seg_503" s="T42">naga</ta>
            <ta e="T44" id="Seg_504" s="T43">no</ta>
            <ta e="T45" id="Seg_505" s="T44">măn-ntə</ta>
            <ta e="T46" id="Seg_506" s="T45">urgo</ta>
            <ta e="T47" id="Seg_507" s="T46">aspaʔ</ta>
            <ta e="T48" id="Seg_508" s="T47">det-liA</ta>
            <ta e="T49" id="Seg_509" s="T48">naverna</ta>
            <ta e="T50" id="Seg_510" s="T49">edəʔ-bi</ta>
            <ta e="T51" id="Seg_511" s="T50">edəʔ-bi</ta>
            <ta e="T52" id="Seg_512" s="T51">dĭgəttə</ta>
            <ta e="T53" id="Seg_513" s="T52">kandə-gA</ta>
            <ta e="T54" id="Seg_514" s="T53">gijen</ta>
            <ta e="T55" id="Seg_515" s="T54">măn</ta>
            <ta e="T56" id="Seg_516" s="T55">măn</ta>
            <ta e="T57" id="Seg_517" s="T56">aspaʔ</ta>
            <ta e="T58" id="Seg_518" s="T57">dĭ</ta>
            <ta e="T59" id="Seg_519" s="T58">kü-laːm-bi</ta>
            <ta e="T60" id="Seg_520" s="T59">naga</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_521" s="T0">two.[NOM.SG]</ta>
            <ta e="T2" id="Seg_522" s="T1">man.[NOM.SG]</ta>
            <ta e="T3" id="Seg_523" s="T2">live-PST-3PL</ta>
            <ta e="T4" id="Seg_524" s="T3">one.[NOM.SG]</ta>
            <ta e="T5" id="Seg_525" s="T4">come-PST.[3SG]</ta>
            <ta e="T6" id="Seg_526" s="T5">bring-IMP.2SG</ta>
            <ta e="T7" id="Seg_527" s="T6">I.LAT</ta>
            <ta e="T8" id="Seg_528" s="T7">cauldron.[NOM.SG]</ta>
            <ta e="T9" id="Seg_529" s="T8">meat.[NOM.SG]</ta>
            <ta e="T10" id="Seg_530" s="T9">boil-INF.LAT</ta>
            <ta e="T12" id="Seg_531" s="T11">this-LAT</ta>
            <ta e="T13" id="Seg_532" s="T12">give-PST.[3SG]</ta>
            <ta e="T14" id="Seg_533" s="T13">cauldron.[NOM.SG]</ta>
            <ta e="T15" id="Seg_534" s="T14">this.[NOM.SG]</ta>
            <ta e="T16" id="Seg_535" s="T15">boil-PST.[3SG]</ta>
            <ta e="T17" id="Seg_536" s="T16">and</ta>
            <ta e="T18" id="Seg_537" s="T17">bring-PST.[3SG]</ta>
            <ta e="T19" id="Seg_538" s="T18">small.[NOM.SG]</ta>
            <ta e="T20" id="Seg_539" s="T19">cauldron.[NOM.SG]</ta>
            <ta e="T21" id="Seg_540" s="T20">this-LAT</ta>
            <ta e="T22" id="Seg_541" s="T21">this.[NOM.SG]</ta>
            <ta e="T24" id="Seg_542" s="T23">bring-PST.[3SG]</ta>
            <ta e="T25" id="Seg_543" s="T24">more</ta>
            <ta e="T26" id="Seg_544" s="T25">one.[NOM.SG]</ta>
            <ta e="T27" id="Seg_545" s="T26">cauldron.[NOM.SG]</ta>
            <ta e="T28" id="Seg_546" s="T27">then</ta>
            <ta e="T29" id="Seg_547" s="T28">again</ta>
            <ta e="T30" id="Seg_548" s="T29">come-PRS.[3SG]</ta>
            <ta e="T31" id="Seg_549" s="T30">bring-IMP.2SG</ta>
            <ta e="T32" id="Seg_550" s="T31">cauldron.[NOM.SG]</ta>
            <ta e="T33" id="Seg_551" s="T32">I.LAT</ta>
            <ta e="T34" id="Seg_552" s="T33">this.[NOM.SG]</ta>
            <ta e="T35" id="Seg_553" s="T34">give-PST.[3SG]</ta>
            <ta e="T36" id="Seg_554" s="T35">NEG.EX.[3SG]</ta>
            <ta e="T37" id="Seg_555" s="T36">cauldron.[NOM.SG]</ta>
            <ta e="T38" id="Seg_556" s="T37">NEG</ta>
            <ta e="T39" id="Seg_557" s="T38">come-PRS.[3SG]</ta>
            <ta e="T40" id="Seg_558" s="T39">three.[NOM.SG]</ta>
            <ta e="T41" id="Seg_559" s="T40">three.[NOM.SG]</ta>
            <ta e="T42" id="Seg_560" s="T41">day.[NOM.SG]</ta>
            <ta e="T43" id="Seg_561" s="T42">NEG.EX.[3SG]</ta>
            <ta e="T44" id="Seg_562" s="T43">well</ta>
            <ta e="T45" id="Seg_563" s="T44">say-IPFVZ.[3SG]</ta>
            <ta e="T46" id="Seg_564" s="T45">big.[NOM.SG]</ta>
            <ta e="T47" id="Seg_565" s="T46">cauldron.[NOM.SG]</ta>
            <ta e="T48" id="Seg_566" s="T47">bring-PRS.[3SG]</ta>
            <ta e="T49" id="Seg_567" s="T48">probably</ta>
            <ta e="T50" id="Seg_568" s="T49">wait-PST.[3SG]</ta>
            <ta e="T51" id="Seg_569" s="T50">wait-PST.[3SG]</ta>
            <ta e="T52" id="Seg_570" s="T51">then</ta>
            <ta e="T53" id="Seg_571" s="T52">walk-PRS.[3SG]</ta>
            <ta e="T54" id="Seg_572" s="T53">where</ta>
            <ta e="T55" id="Seg_573" s="T54">I.GEN</ta>
            <ta e="T56" id="Seg_574" s="T55">I.GEN</ta>
            <ta e="T57" id="Seg_575" s="T56">cauldron.[NOM.SG]</ta>
            <ta e="T58" id="Seg_576" s="T57">this</ta>
            <ta e="T59" id="Seg_577" s="T58">die-RES-PST.[3SG]</ta>
            <ta e="T60" id="Seg_578" s="T59">NEG.EX.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_579" s="T0">два.[NOM.SG]</ta>
            <ta e="T2" id="Seg_580" s="T1">мужчина.[NOM.SG]</ta>
            <ta e="T3" id="Seg_581" s="T2">жить-PST-3PL</ta>
            <ta e="T4" id="Seg_582" s="T3">один.[NOM.SG]</ta>
            <ta e="T5" id="Seg_583" s="T4">прийти-PST.[3SG]</ta>
            <ta e="T6" id="Seg_584" s="T5">принести-IMP.2SG</ta>
            <ta e="T7" id="Seg_585" s="T6">я.LAT</ta>
            <ta e="T8" id="Seg_586" s="T7">котел.[NOM.SG]</ta>
            <ta e="T9" id="Seg_587" s="T8">мясо.[NOM.SG]</ta>
            <ta e="T10" id="Seg_588" s="T9">кипятить-INF.LAT</ta>
            <ta e="T12" id="Seg_589" s="T11">этот-LAT</ta>
            <ta e="T13" id="Seg_590" s="T12">дать-PST.[3SG]</ta>
            <ta e="T14" id="Seg_591" s="T13">котел.[NOM.SG]</ta>
            <ta e="T15" id="Seg_592" s="T14">этот.[NOM.SG]</ta>
            <ta e="T16" id="Seg_593" s="T15">кипятить-PST.[3SG]</ta>
            <ta e="T17" id="Seg_594" s="T16">и</ta>
            <ta e="T18" id="Seg_595" s="T17">принести-PST.[3SG]</ta>
            <ta e="T19" id="Seg_596" s="T18">маленький.[NOM.SG]</ta>
            <ta e="T20" id="Seg_597" s="T19">котел.[NOM.SG]</ta>
            <ta e="T21" id="Seg_598" s="T20">этот-LAT</ta>
            <ta e="T22" id="Seg_599" s="T21">этот.[NOM.SG]</ta>
            <ta e="T24" id="Seg_600" s="T23">принести-PST.[3SG]</ta>
            <ta e="T25" id="Seg_601" s="T24">еще</ta>
            <ta e="T26" id="Seg_602" s="T25">один.[NOM.SG]</ta>
            <ta e="T27" id="Seg_603" s="T26">котел.[NOM.SG]</ta>
            <ta e="T28" id="Seg_604" s="T27">тогда</ta>
            <ta e="T29" id="Seg_605" s="T28">опять</ta>
            <ta e="T30" id="Seg_606" s="T29">прийти-PRS.[3SG]</ta>
            <ta e="T31" id="Seg_607" s="T30">принести-IMP.2SG</ta>
            <ta e="T32" id="Seg_608" s="T31">котел.[NOM.SG]</ta>
            <ta e="T33" id="Seg_609" s="T32">я.LAT</ta>
            <ta e="T34" id="Seg_610" s="T33">этот.[NOM.SG]</ta>
            <ta e="T35" id="Seg_611" s="T34">дать-PST.[3SG]</ta>
            <ta e="T36" id="Seg_612" s="T35">NEG.EX.[3SG]</ta>
            <ta e="T37" id="Seg_613" s="T36">котел.[NOM.SG]</ta>
            <ta e="T38" id="Seg_614" s="T37">NEG</ta>
            <ta e="T39" id="Seg_615" s="T38">прийти-PRS.[3SG]</ta>
            <ta e="T40" id="Seg_616" s="T39">три.[NOM.SG]</ta>
            <ta e="T41" id="Seg_617" s="T40">три.[NOM.SG]</ta>
            <ta e="T42" id="Seg_618" s="T41">день.[NOM.SG]</ta>
            <ta e="T43" id="Seg_619" s="T42">NEG.EX.[3SG]</ta>
            <ta e="T44" id="Seg_620" s="T43">ну</ta>
            <ta e="T45" id="Seg_621" s="T44">сказать-IPFVZ.[3SG]</ta>
            <ta e="T46" id="Seg_622" s="T45">большой.[NOM.SG]</ta>
            <ta e="T47" id="Seg_623" s="T46">котел.[NOM.SG]</ta>
            <ta e="T48" id="Seg_624" s="T47">принести-PRS.[3SG]</ta>
            <ta e="T49" id="Seg_625" s="T48">наверное</ta>
            <ta e="T50" id="Seg_626" s="T49">ждать-PST.[3SG]</ta>
            <ta e="T51" id="Seg_627" s="T50">ждать-PST.[3SG]</ta>
            <ta e="T52" id="Seg_628" s="T51">тогда</ta>
            <ta e="T53" id="Seg_629" s="T52">идти-PRS.[3SG]</ta>
            <ta e="T54" id="Seg_630" s="T53">где</ta>
            <ta e="T55" id="Seg_631" s="T54">я.GEN</ta>
            <ta e="T56" id="Seg_632" s="T55">я.GEN</ta>
            <ta e="T57" id="Seg_633" s="T56">котел.[NOM.SG]</ta>
            <ta e="T58" id="Seg_634" s="T57">этот</ta>
            <ta e="T59" id="Seg_635" s="T58">умереть-RES-PST.[3SG]</ta>
            <ta e="T60" id="Seg_636" s="T59">NEG.EX.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_637" s="T0">num-n:case</ta>
            <ta e="T2" id="Seg_638" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_639" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_640" s="T3">num-n:case</ta>
            <ta e="T5" id="Seg_641" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_642" s="T5">v-v:mood.pn</ta>
            <ta e="T7" id="Seg_643" s="T6">pers</ta>
            <ta e="T8" id="Seg_644" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_645" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_646" s="T9">v-v:n.fin</ta>
            <ta e="T12" id="Seg_647" s="T11">dempro-n:case</ta>
            <ta e="T13" id="Seg_648" s="T12">v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_649" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_650" s="T14">dempro-n:case</ta>
            <ta e="T16" id="Seg_651" s="T15">v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_652" s="T16">conj</ta>
            <ta e="T18" id="Seg_653" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_654" s="T18">adj-n:case</ta>
            <ta e="T20" id="Seg_655" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_656" s="T20">dempro-n:case</ta>
            <ta e="T22" id="Seg_657" s="T21">dempro-n:case</ta>
            <ta e="T24" id="Seg_658" s="T23">v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_659" s="T24">adv</ta>
            <ta e="T26" id="Seg_660" s="T25">num-n:case</ta>
            <ta e="T27" id="Seg_661" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_662" s="T27">adv</ta>
            <ta e="T29" id="Seg_663" s="T28">adv</ta>
            <ta e="T30" id="Seg_664" s="T29">v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_665" s="T30">v-v:mood.pn</ta>
            <ta e="T32" id="Seg_666" s="T31">n-n:case</ta>
            <ta e="T33" id="Seg_667" s="T32">pers</ta>
            <ta e="T34" id="Seg_668" s="T33">dempro-n:case</ta>
            <ta e="T35" id="Seg_669" s="T34">v-v:tense-v:pn</ta>
            <ta e="T36" id="Seg_670" s="T35">v-v:pn</ta>
            <ta e="T37" id="Seg_671" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_672" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_673" s="T38">v-v:tense-v:pn</ta>
            <ta e="T40" id="Seg_674" s="T39">num-n:case</ta>
            <ta e="T41" id="Seg_675" s="T40">num-n:case</ta>
            <ta e="T42" id="Seg_676" s="T41">n-n:case</ta>
            <ta e="T43" id="Seg_677" s="T42">v-v:pn</ta>
            <ta e="T44" id="Seg_678" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_679" s="T44">v-v&gt;v-v:pn</ta>
            <ta e="T46" id="Seg_680" s="T45">adj-n:case</ta>
            <ta e="T47" id="Seg_681" s="T46">n-n:case</ta>
            <ta e="T48" id="Seg_682" s="T47">v-v:tense-v:pn</ta>
            <ta e="T49" id="Seg_683" s="T48">adv</ta>
            <ta e="T50" id="Seg_684" s="T49">v-v:tense-v:pn</ta>
            <ta e="T51" id="Seg_685" s="T50">v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_686" s="T51">adv</ta>
            <ta e="T53" id="Seg_687" s="T52">v-v:tense-v:pn</ta>
            <ta e="T54" id="Seg_688" s="T53">que</ta>
            <ta e="T55" id="Seg_689" s="T54">pers</ta>
            <ta e="T56" id="Seg_690" s="T55">pers</ta>
            <ta e="T57" id="Seg_691" s="T56">n-n:case</ta>
            <ta e="T58" id="Seg_692" s="T57">dempro</ta>
            <ta e="T59" id="Seg_693" s="T58">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T60" id="Seg_694" s="T59">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_695" s="T0">num</ta>
            <ta e="T2" id="Seg_696" s="T1">n</ta>
            <ta e="T3" id="Seg_697" s="T2">v</ta>
            <ta e="T4" id="Seg_698" s="T3">num</ta>
            <ta e="T5" id="Seg_699" s="T4">v</ta>
            <ta e="T6" id="Seg_700" s="T5">v</ta>
            <ta e="T7" id="Seg_701" s="T6">pers</ta>
            <ta e="T8" id="Seg_702" s="T7">n</ta>
            <ta e="T9" id="Seg_703" s="T8">n</ta>
            <ta e="T10" id="Seg_704" s="T9">v</ta>
            <ta e="T12" id="Seg_705" s="T11">dempro</ta>
            <ta e="T13" id="Seg_706" s="T12">v</ta>
            <ta e="T14" id="Seg_707" s="T13">n</ta>
            <ta e="T15" id="Seg_708" s="T14">dempro</ta>
            <ta e="T16" id="Seg_709" s="T15">v</ta>
            <ta e="T17" id="Seg_710" s="T16">conj</ta>
            <ta e="T18" id="Seg_711" s="T17">v</ta>
            <ta e="T19" id="Seg_712" s="T18">adj</ta>
            <ta e="T20" id="Seg_713" s="T19">n</ta>
            <ta e="T21" id="Seg_714" s="T20">dempro</ta>
            <ta e="T22" id="Seg_715" s="T21">dempro</ta>
            <ta e="T24" id="Seg_716" s="T23">v</ta>
            <ta e="T25" id="Seg_717" s="T24">adv</ta>
            <ta e="T26" id="Seg_718" s="T25">num</ta>
            <ta e="T27" id="Seg_719" s="T26">n</ta>
            <ta e="T28" id="Seg_720" s="T27">adv</ta>
            <ta e="T29" id="Seg_721" s="T28">adv</ta>
            <ta e="T30" id="Seg_722" s="T29">v</ta>
            <ta e="T31" id="Seg_723" s="T30">v</ta>
            <ta e="T32" id="Seg_724" s="T31">n</ta>
            <ta e="T33" id="Seg_725" s="T32">pers</ta>
            <ta e="T34" id="Seg_726" s="T33">dempro</ta>
            <ta e="T35" id="Seg_727" s="T34">v</ta>
            <ta e="T36" id="Seg_728" s="T35">v</ta>
            <ta e="T37" id="Seg_729" s="T36">n</ta>
            <ta e="T38" id="Seg_730" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_731" s="T38">v</ta>
            <ta e="T40" id="Seg_732" s="T39">num</ta>
            <ta e="T41" id="Seg_733" s="T40">num</ta>
            <ta e="T42" id="Seg_734" s="T41">n</ta>
            <ta e="T43" id="Seg_735" s="T42">v</ta>
            <ta e="T44" id="Seg_736" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_737" s="T44">v</ta>
            <ta e="T46" id="Seg_738" s="T45">adj</ta>
            <ta e="T47" id="Seg_739" s="T46">n</ta>
            <ta e="T48" id="Seg_740" s="T47">v</ta>
            <ta e="T49" id="Seg_741" s="T48">adv</ta>
            <ta e="T50" id="Seg_742" s="T49">v</ta>
            <ta e="T51" id="Seg_743" s="T50">v</ta>
            <ta e="T52" id="Seg_744" s="T51">adv</ta>
            <ta e="T53" id="Seg_745" s="T52">v</ta>
            <ta e="T54" id="Seg_746" s="T53">que</ta>
            <ta e="T55" id="Seg_747" s="T54">pers</ta>
            <ta e="T56" id="Seg_748" s="T55">pers</ta>
            <ta e="T57" id="Seg_749" s="T56">n</ta>
            <ta e="T58" id="Seg_750" s="T57">dempro</ta>
            <ta e="T59" id="Seg_751" s="T58">v</ta>
            <ta e="T60" id="Seg_752" s="T59">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_753" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_754" s="T2">v:pred</ta>
            <ta e="T4" id="Seg_755" s="T3">np:S</ta>
            <ta e="T5" id="Seg_756" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_757" s="T5">0.2.h:S v:pred</ta>
            <ta e="T8" id="Seg_758" s="T7">np:O</ta>
            <ta e="T10" id="Seg_759" s="T8">s:purp</ta>
            <ta e="T12" id="Seg_760" s="T11">pro.h:S</ta>
            <ta e="T13" id="Seg_761" s="T12">v:pred</ta>
            <ta e="T14" id="Seg_762" s="T13">np:O</ta>
            <ta e="T15" id="Seg_763" s="T14">pro.h:S</ta>
            <ta e="T16" id="Seg_764" s="T15">v:pred</ta>
            <ta e="T18" id="Seg_765" s="T17">0.3.h:S v:pred</ta>
            <ta e="T20" id="Seg_766" s="T19">np:O</ta>
            <ta e="T22" id="Seg_767" s="T21">pro.h:S</ta>
            <ta e="T24" id="Seg_768" s="T23">v:pred</ta>
            <ta e="T27" id="Seg_769" s="T26">np:O</ta>
            <ta e="T30" id="Seg_770" s="T29">0.3.h:S v:pred</ta>
            <ta e="T31" id="Seg_771" s="T30">0.2.h:S v:pred</ta>
            <ta e="T32" id="Seg_772" s="T31">np:O</ta>
            <ta e="T34" id="Seg_773" s="T33">pro.h:S</ta>
            <ta e="T35" id="Seg_774" s="T34">v:pred</ta>
            <ta e="T36" id="Seg_775" s="T35">v:pred</ta>
            <ta e="T37" id="Seg_776" s="T36">np:S</ta>
            <ta e="T39" id="Seg_777" s="T38">0.3.h:S v:pred</ta>
            <ta e="T43" id="Seg_778" s="T42">0.3:S v:pred</ta>
            <ta e="T45" id="Seg_779" s="T44">0.3.h:S v:pred</ta>
            <ta e="T47" id="Seg_780" s="T46">np:O</ta>
            <ta e="T48" id="Seg_781" s="T47">0.3.h:S v:pred</ta>
            <ta e="T50" id="Seg_782" s="T49">0.3.h:S v:pred</ta>
            <ta e="T51" id="Seg_783" s="T50">0.3.h:S v:pred</ta>
            <ta e="T53" id="Seg_784" s="T52">0.3.h:S v:pred</ta>
            <ta e="T57" id="Seg_785" s="T56">np:S</ta>
            <ta e="T58" id="Seg_786" s="T57">pro:S</ta>
            <ta e="T59" id="Seg_787" s="T58">v:pred</ta>
            <ta e="T60" id="Seg_788" s="T59">0.3:S v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_789" s="T1">np.h:Th</ta>
            <ta e="T4" id="Seg_790" s="T3">np:A</ta>
            <ta e="T6" id="Seg_791" s="T5">0.2.h:A</ta>
            <ta e="T7" id="Seg_792" s="T6">pro.h:R</ta>
            <ta e="T8" id="Seg_793" s="T7">np:Th</ta>
            <ta e="T9" id="Seg_794" s="T8">np:Th</ta>
            <ta e="T12" id="Seg_795" s="T11">pro.h:A</ta>
            <ta e="T14" id="Seg_796" s="T13">np:Th</ta>
            <ta e="T15" id="Seg_797" s="T14">pro.h:A</ta>
            <ta e="T18" id="Seg_798" s="T17">0.3.h:A</ta>
            <ta e="T20" id="Seg_799" s="T19">np:Th</ta>
            <ta e="T21" id="Seg_800" s="T20">pro.h:R</ta>
            <ta e="T22" id="Seg_801" s="T21">pro.h:A</ta>
            <ta e="T27" id="Seg_802" s="T26">np:Th</ta>
            <ta e="T30" id="Seg_803" s="T29">0.3.h:A</ta>
            <ta e="T31" id="Seg_804" s="T30">0.2.h:A</ta>
            <ta e="T32" id="Seg_805" s="T31">np:Th</ta>
            <ta e="T33" id="Seg_806" s="T32">pro.h:R</ta>
            <ta e="T34" id="Seg_807" s="T33">pro:A</ta>
            <ta e="T37" id="Seg_808" s="T36">np:Th</ta>
            <ta e="T39" id="Seg_809" s="T38">0.3.h:A</ta>
            <ta e="T42" id="Seg_810" s="T41">np:Time</ta>
            <ta e="T43" id="Seg_811" s="T42">0.3:Th</ta>
            <ta e="T45" id="Seg_812" s="T44">0.3.h:A</ta>
            <ta e="T47" id="Seg_813" s="T46">np:Th</ta>
            <ta e="T48" id="Seg_814" s="T47">0.3.h:A</ta>
            <ta e="T50" id="Seg_815" s="T49">0.3.h:A</ta>
            <ta e="T51" id="Seg_816" s="T50">0.3.h:A</ta>
            <ta e="T53" id="Seg_817" s="T52">0.3.h:A</ta>
            <ta e="T56" id="Seg_818" s="T55">pro.h:Poss</ta>
            <ta e="T57" id="Seg_819" s="T56">np:Th</ta>
            <ta e="T58" id="Seg_820" s="T57">pro:Th</ta>
            <ta e="T60" id="Seg_821" s="T59">pro:Th</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T17" id="Seg_822" s="T16">RUS:gram</ta>
            <ta e="T25" id="Seg_823" s="T24">RUS:mod</ta>
            <ta e="T44" id="Seg_824" s="T43">RUS:disc</ta>
            <ta e="T49" id="Seg_825" s="T48">RUS:mod</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_826" s="T0">Жили два человека.</ta>
            <ta e="T5" id="Seg_827" s="T3">Один пришёл.</ta>
            <ta e="T10" id="Seg_828" s="T5">«Принеси мне котёл, мясо сварить!»</ta>
            <ta e="T14" id="Seg_829" s="T10">Тот дал ему котёл.</ta>
            <ta e="T16" id="Seg_830" s="T14">Он сварил [мясо].</ta>
            <ta e="T21" id="Seg_831" s="T16">И принёс ему маленький котёл.</ta>
            <ta e="T27" id="Seg_832" s="T21">Он принёс ещё один котёл.</ta>
            <ta e="T30" id="Seg_833" s="T27">Потом снова приходит.</ta>
            <ta e="T33" id="Seg_834" s="T30">«Принеси мне котёл!»</ta>
            <ta e="T35" id="Seg_835" s="T33">Он дал.</ta>
            <ta e="T37" id="Seg_836" s="T35">Нет котла.</ta>
            <ta e="T43" id="Seg_837" s="T37">Он не приходит, три дня ничего [не происходит].</ta>
            <ta e="T45" id="Seg_838" s="T43">Ну, он говорит.</ta>
            <ta e="T49" id="Seg_839" s="T45">«Наверное, он принесёт большой котёл».</ta>
            <ta e="T57" id="Seg_840" s="T49">Он ждал, ждал, потом идёт [к нему]: «Где мой котёл?»</ta>
            <ta e="T60" id="Seg_841" s="T57">«Он умер, [его больше] нет».</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_842" s="T0">Two men lived.</ta>
            <ta e="T5" id="Seg_843" s="T3">One came.</ta>
            <ta e="T10" id="Seg_844" s="T5">"Bring me a cauldron to cook meat!"</ta>
            <ta e="T14" id="Seg_845" s="T10">He gave a cauldron to him.</ta>
            <ta e="T16" id="Seg_846" s="T14">He cooked.</ta>
            <ta e="T21" id="Seg_847" s="T16">And brought a small cauldron to him.</ta>
            <ta e="T27" id="Seg_848" s="T21">He brought one more cauldron.</ta>
            <ta e="T30" id="Seg_849" s="T27">Then again he comes.</ta>
            <ta e="T33" id="Seg_850" s="T30">"Give me a cauldron!"</ta>
            <ta e="T35" id="Seg_851" s="T33">He gave.</ta>
            <ta e="T37" id="Seg_852" s="T35">There is no cauldron.</ta>
            <ta e="T43" id="Seg_853" s="T37">He does not come, three days nothing [happens].</ta>
            <ta e="T45" id="Seg_854" s="T43">Well, he says.</ta>
            <ta e="T49" id="Seg_855" s="T45">Probably he'll bring a big cauldron."</ta>
            <ta e="T57" id="Seg_856" s="T49">He waited, he waited, then he goes [there]: "Where is my cauldron?"</ta>
            <ta e="T60" id="Seg_857" s="T57">"It died, it is no [more]."</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_858" s="T0">Es lebten zwei Männer.</ta>
            <ta e="T5" id="Seg_859" s="T3">Einer kam.</ta>
            <ta e="T10" id="Seg_860" s="T5">„Bring mir einen Kessel zum Fleischkochen!“</ta>
            <ta e="T14" id="Seg_861" s="T10">Er gab ihm einen Kessel.</ta>
            <ta e="T16" id="Seg_862" s="T14">Er kochte.</ta>
            <ta e="T21" id="Seg_863" s="T16">Und brachte ihm einen kleinen Kessel.</ta>
            <ta e="T27" id="Seg_864" s="T21">Er brauchte noch einen Kessel.</ta>
            <ta e="T30" id="Seg_865" s="T27">Dann kommt er wieder.</ta>
            <ta e="T33" id="Seg_866" s="T30">„Gib mir einen Kessel!“</ta>
            <ta e="T35" id="Seg_867" s="T33">Er gab.</ta>
            <ta e="T37" id="Seg_868" s="T35">Es ist kein Kessel.</ta>
            <ta e="T43" id="Seg_869" s="T37">Er kommt nicht, drei Tage [passiert] nichts.</ta>
            <ta e="T45" id="Seg_870" s="T43">Also, sagt er.</ta>
            <ta e="T49" id="Seg_871" s="T45">Wahrscheinlich bringt er einen großen Kessel.“</ta>
            <ta e="T57" id="Seg_872" s="T49">Er wartete, er wartete, dann geht er [dahin]: „Wo ist mein Kessel?“</ta>
            <ta e="T60" id="Seg_873" s="T57">Er starb, er ist nicht [mehr].“</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T3" id="Seg_874" s="T0">[GVY:] A well-known story about a cauldron that first 'gives birth to a child' and then 'dies'. </ta>
            <ta e="T57" id="Seg_875" s="T49">[KlT:] edəʔ ’to wait’.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
