<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID647BC5F4-C212-3E67-BD8D-5DCF5E2FE5A1">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_GreedyWife_flk.wav" />
         <referenced-file url="PKZ_196X_GreedyWife_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_GreedyWife_flk\PKZ_196X_GreedyWife_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">356</ud-information>
            <ud-information attribute-name="# HIAT:w">249</ud-information>
            <ud-information attribute-name="# e">247</ud-information>
            <ud-information attribute-name="# HIAT:u">57</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1057" time="0.0" type="appl" />
         <tli id="T1058" time="0.726" type="appl" />
         <tli id="T1059" time="1.453" type="appl" />
         <tli id="T1060" time="2.179" type="appl" />
         <tli id="T1061" time="2.887" type="appl" />
         <tli id="T1062" time="3.595" type="appl" />
         <tli id="T1063" time="4.303" type="appl" />
         <tli id="T1064" time="5.011" type="appl" />
         <tli id="T1065" time="5.719" type="appl" />
         <tli id="T1066" time="6.472" type="appl" />
         <tli id="T1067" time="7.225" type="appl" />
         <tli id="T1068" time="7.83" type="appl" />
         <tli id="T1069" time="8.436" type="appl" />
         <tli id="T1070" time="9.042" type="appl" />
         <tli id="T1071" time="9.647" type="appl" />
         <tli id="T1072" time="10.775" type="appl" />
         <tli id="T1073" time="11.904" type="appl" />
         <tli id="T1074" time="13.032" type="appl" />
         <tli id="T1075" time="13.519" type="appl" />
         <tli id="T1076" time="14.613153055249992" />
         <tli id="T1077" time="15.27" type="appl" />
         <tli id="T1078" time="16.096" type="appl" />
         <tli id="T1079" time="16.922" type="appl" />
         <tli id="T1080" time="17.749" type="appl" />
         <tli id="T1081" time="18.575" type="appl" />
         <tli id="T1082" time="20.473080763080624" />
         <tli id="T1083" time="21.108" type="appl" />
         <tli id="T1084" time="21.67" type="appl" />
         <tli id="T1085" time="22.231" type="appl" />
         <tli id="T1086" time="22.793" type="appl" />
         <tli id="T1087" time="23.354" type="appl" />
         <tli id="T1088" time="23.836" type="appl" />
         <tli id="T1089" time="24.317" type="appl" />
         <tli id="T1090" time="24.798" type="appl" />
         <tli id="T1091" time="25.28" type="appl" />
         <tli id="T1092" time="26.1" type="appl" />
         <tli id="T1093" time="26.648" type="appl" />
         <tli id="T1094" time="27.196" type="appl" />
         <tli id="T1095" time="27.743" type="appl" />
         <tli id="T1096" time="28.29" type="appl" />
         <tli id="T1097" time="28.838" type="appl" />
         <tli id="T1098" time="29.357" type="appl" />
         <tli id="T1099" time="29.767" type="appl" />
         <tli id="T1100" time="30.177" type="appl" />
         <tli id="T1101" time="30.751" type="appl" />
         <tli id="T1102" time="31.325" type="appl" />
         <tli id="T1103" time="32.264" type="appl" />
         <tli id="T1104" time="33.064" type="appl" />
         <tli id="T1105" time="33.865" type="appl" />
         <tli id="T1106" time="34.318" type="appl" />
         <tli id="T1107" time="34.77" type="appl" />
         <tli id="T1108" time="35.223" type="appl" />
         <tli id="T1109" time="35.676" type="appl" />
         <tli id="T1110" time="36.128" type="appl" />
         <tli id="T1111" time="36.581" type="appl" />
         <tli id="T1112" time="37.033" type="appl" />
         <tli id="T1113" time="37.946198535804264" />
         <tli id="T1114" time="38.58" type="appl" />
         <tli id="T1115" time="39.232" type="appl" />
         <tli id="T1116" time="39.884" type="appl" />
         <tli id="T1117" time="41.4861548644255" />
         <tli id="T1118" time="42.557" type="appl" />
         <tli id="T1119" time="43.614" type="appl" />
         <tli id="T1120" time="44.671" type="appl" />
         <tli id="T1121" time="45.305" type="appl" />
         <tli id="T1122" time="46.67275754553157" />
         <tli id="T1123" time="48.385" type="appl" />
         <tli id="T1124" time="50.044" type="appl" />
         <tli id="T1125" time="51.704" type="appl" />
         <tli id="T1126" time="52.695" type="appl" />
         <tli id="T1127" time="53.432" type="appl" />
         <tli id="T1128" time="54.168" type="appl" />
         <tli id="T1129" time="54.905" type="appl" />
         <tli id="T1130" time="55.606" type="appl" />
         <tli id="T1131" time="56.286" type="appl" />
         <tli id="T1132" time="56.966" type="appl" />
         <tli id="T1133" time="57.87261937619762" />
         <tli id="T1134" time="58.938" type="appl" />
         <tli id="T1135" time="59.864" type="appl" />
         <tli id="T1136" time="60.81" type="appl" />
         <tli id="T1137" time="61.755" type="appl" />
         <tli id="T1138" time="62.286" type="appl" />
         <tli id="T1139" time="62.816" type="appl" />
         <tli id="T1140" time="63.347" type="appl" />
         <tli id="T1141" time="64.426" type="appl" />
         <tli id="T1142" time="65.506" type="appl" />
         <tli id="T1143" time="66.586" type="appl" />
         <tli id="T1144" time="68.13249280321848" />
         <tli id="T1145" time="68.872" type="appl" />
         <tli id="T1146" time="70.99912410511514" />
         <tli id="T1147" time="71.926" type="appl" />
         <tli id="T1148" time="72.872" type="appl" />
         <tli id="T1149" time="73.818" type="appl" />
         <tli id="T1150" time="74.763" type="appl" />
         <tli id="T1151" time="75.496" type="appl" />
         <tli id="T1152" time="76.1" type="appl" />
         <tli id="T1153" time="76.703" type="appl" />
         <tli id="T1154" time="77.306" type="appl" />
         <tli id="T1155" time="77.947" type="appl" />
         <tli id="T1156" time="78.583" type="appl" />
         <tli id="T1157" time="79.22" type="appl" />
         <tli id="T1158" time="79.856" type="appl" />
         <tli id="T1159" time="80.492" type="appl" />
         <tli id="T1160" time="81.128" type="appl" />
         <tli id="T1161" time="81.764" type="appl" />
         <tli id="T1162" time="82.4" type="appl" />
         <tli id="T1163" time="83.037" type="appl" />
         <tli id="T1164" time="83.673" type="appl" />
         <tli id="T1165" time="84.309" type="appl" />
         <tli id="T1166" time="85.124" type="appl" />
         <tli id="T1167" time="85.899" type="appl" />
         <tli id="T1168" time="86.674" type="appl" />
         <tli id="T1169" time="87.449" type="appl" />
         <tli id="T1170" time="89.08556764475622" />
         <tli id="T1171" time="89.974" type="appl" />
         <tli id="T1172" time="90.789" type="appl" />
         <tli id="T1173" time="91.269" type="appl" />
         <tli id="T1174" time="91.748" type="appl" />
         <tli id="T1175" time="92.228" type="appl" />
         <tli id="T1176" time="92.9855195322203" />
         <tli id="T1177" time="93.646" type="appl" />
         <tli id="T1178" time="94.287" type="appl" />
         <tli id="T1179" time="94.927" type="appl" />
         <tli id="T1180" time="95.567" type="appl" />
         <tli id="T1181" time="96.208" type="appl" />
         <tli id="T1182" time="96.848" type="appl" />
         <tli id="T1183" time="97.488" type="appl" />
         <tli id="T1184" time="98.128" type="appl" />
         <tli id="T1185" time="98.769" type="appl" />
         <tli id="T1186" time="99.409" type="appl" />
         <tli id="T1187" time="100.049" type="appl" />
         <tli id="T1188" time="100.69" type="appl" />
         <tli id="T1189" time="101.33" type="appl" />
         <tli id="T1190" time="102.49873550386341" />
         <tli id="T1191" time="103.152" type="appl" />
         <tli id="T1192" time="104.23204745384744" />
         <tli id="T1193" time="105.233" type="appl" />
         <tli id="T1194" time="106.79868245670842" />
         <tli id="T1195" time="107.823" type="appl" />
         <tli id="T1196" time="108.689" type="appl" />
         <tli id="T1197" time="109.556" type="appl" />
         <tli id="T1198" time="110.21" type="appl" />
         <tli id="T1199" time="110.865" type="appl" />
         <tli id="T1200" time="111.519" type="appl" />
         <tli id="T1201" time="112.174" type="appl" />
         <tli id="T1202" time="112.828" type="appl" />
         <tli id="T1203" time="113.482" type="appl" />
         <tli id="T1204" time="114.137" type="appl" />
         <tli id="T1205" time="114.791" type="appl" />
         <tli id="T1206" time="115.446" type="appl" />
         <tli id="T1207" time="116.1" type="appl" />
         <tli id="T1208" time="118.04521037833558" />
         <tli id="T1209" time="119.087" type="appl" />
         <tli id="T1210" time="119.923" type="appl" />
         <tli id="T1211" time="120.76" type="appl" />
         <tli id="T1212" time="121.596" type="appl" />
         <tli id="T1213" time="122.432" type="appl" />
         <tli id="T1214" time="123.268" type="appl" />
         <tli id="T1215" time="124.065" type="appl" />
         <tli id="T1216" time="124.862" type="appl" />
         <tli id="T1217" time="125.405" type="appl" />
         <tli id="T1218" time="125.948" type="appl" />
         <tli id="T1219" time="126.491" type="appl" />
         <tli id="T1220" time="127.034" type="appl" />
         <tli id="T1221" time="127.812" type="appl" />
         <tli id="T1222" time="128.382" type="appl" />
         <tli id="T1223" time="128.952" type="appl" />
         <tli id="T1224" time="129.522" type="appl" />
         <tli id="T1225" time="130.093" type="appl" />
         <tli id="T1226" time="130.663" type="appl" />
         <tli id="T1227" time="131.233" type="appl" />
         <tli id="T1228" time="131.803" type="appl" />
         <tli id="T1229" time="132.373" type="appl" />
         <tli id="T1230" time="133.42502064223234" />
         <tli id="T1231" time="134.219" type="appl" />
         <tli id="T1232" time="134.952" type="appl" />
         <tli id="T1233" time="135.686" type="appl" />
         <tli id="T1234" time="136.419" type="appl" />
         <tli id="T1235" time="137.153" type="appl" />
         <tli id="T1236" time="137.887" type="appl" />
         <tli id="T1237" time="138.62" type="appl" />
         <tli id="T1238" time="139.354" type="appl" />
         <tli id="T1239" time="140.087" type="appl" />
         <tli id="T1240" time="140.821" type="appl" />
         <tli id="T1241" time="141.727" type="appl" />
         <tli id="T1242" time="142.634" type="appl" />
         <tli id="T1243" time="143.54" type="appl" />
         <tli id="T1244" time="145.07821021366345" />
         <tli id="T1245" time="145.806" type="appl" />
         <tli id="T1246" time="146.53" type="appl" />
         <tli id="T1247" time="147.253" type="appl" />
         <tli id="T1248" time="147.976" type="appl" />
         <tli id="T1249" time="148.7" type="appl" />
         <tli id="T1250" time="149.423" type="appl" />
         <tli id="T1251" time="150.27147947919255" />
         <tli id="T1252" time="150.82" type="appl" />
         <tli id="T1253" time="151.247" type="appl" />
         <tli id="T1254" time="151.674" type="appl" />
         <tli id="T1255" time="152.101" type="appl" />
         <tli id="T1256" time="152.528" type="appl" />
         <tli id="T1257" time="152.955" type="appl" />
         <tli id="T1258" time="153.382" type="appl" />
         <tli id="T1259" time="153.809" type="appl" />
         <tli id="T1260" time="154.837" type="appl" />
         <tli id="T1261" time="155.65" type="appl" />
         <tli id="T1262" time="156.464" type="appl" />
         <tli id="T1263" time="157.278" type="appl" />
         <tli id="T1264" time="158.091" type="appl" />
         <tli id="T1265" time="158.905" type="appl" />
         <tli id="T1266" time="159.718" type="appl" />
         <tli id="T1267" time="160.532" type="appl" />
         <tli id="T1268" time="161.692" type="appl" />
         <tli id="T1269" time="162.65" type="appl" />
         <tli id="T1270" time="163.608" type="appl" />
         <tli id="T1271" time="164.51267097738727" />
         <tli id="T1272" time="164.981" type="appl" />
         <tli id="T1273" time="165.397" type="appl" />
         <tli id="T1274" time="165.812" type="appl" />
         <tli id="T1275" time="166.792" type="appl" />
         <tli id="T1276" time="167.351" type="appl" />
         <tli id="T1277" time="167.91" type="appl" />
         <tli id="T1278" time="168.469" type="appl" />
         <tli id="T1279" time="169.028" type="appl" />
         <tli id="T1280" time="169.588" type="appl" />
         <tli id="T1281" time="170.147" type="appl" />
         <tli id="T1282" time="170.706" type="appl" />
         <tli id="T1283" time="171.265" type="appl" />
         <tli id="T1284" time="171.824" type="appl" />
         <tli id="T1285" time="173.053" type="appl" />
         <tli id="T1286" time="173.628" type="appl" />
         <tli id="T1287" time="174.202" type="appl" />
         <tli id="T1288" time="174.777" type="appl" />
         <tli id="T1289" time="175.352" type="appl" />
         <tli id="T1290" time="175.927" type="appl" />
         <tli id="T1291" time="176.502" type="appl" />
         <tli id="T1292" time="177.076" type="appl" />
         <tli id="T1293" time="177.77780680855454" />
         <tli id="T1294" time="178.655" type="appl" />
         <tli id="T1295" time="180.29110913603137" />
         <tli id="T1296" time="181.108" type="appl" />
         <tli id="T1297" time="181.674" type="appl" />
         <tli id="T1298" time="182.239" type="appl" />
         <tli id="T1299" time="182.804" type="appl" />
         <tli id="T1300" time="183.37" type="appl" />
         <tli id="T1301" time="184.27772662099466" />
         <tli id="T1302" time="185.11" type="appl" />
         <tli id="T1303" time="186.089" type="appl" />
         <tli id="T1304" time="187.068" type="appl" />
         <tli id="T1305" time="187.871" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T1098" start="T1097">
            <tli id="T1097.tx.1" />
         </timeline-fork>
         <timeline-fork end="T1197" start="T1196">
            <tli id="T1196.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T1305" id="Seg_0" n="sc" s="T1057">
               <ts e="T1060" id="Seg_2" n="HIAT:u" s="T1057">
                  <ts e="T1058" id="Seg_4" n="HIAT:w" s="T1057">Amnobiʔi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1059" id="Seg_7" n="HIAT:w" s="T1058">nüke</ts>
                  <nts id="Seg_8" n="HIAT:ip">,</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1060" id="Seg_11" n="HIAT:w" s="T1059">büzʼe</ts>
                  <nts id="Seg_12" n="HIAT:ip">.</nts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1065" id="Seg_15" n="HIAT:u" s="T1060">
                  <nts id="Seg_16" n="HIAT:ip">(</nts>
                  <ts e="T1061" id="Seg_18" n="HIAT:w" s="T1060">Dĭn=</ts>
                  <nts id="Seg_19" n="HIAT:ip">)</nts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1062" id="Seg_22" n="HIAT:w" s="T1061">Dĭzeŋgən</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1063" id="Seg_25" n="HIAT:w" s="T1062">ĭmbidə</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1064" id="Seg_28" n="HIAT:w" s="T1063">ej</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1065" id="Seg_31" n="HIAT:w" s="T1064">nagobi</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1067" id="Seg_35" n="HIAT:u" s="T1065">
                  <ts e="T1066" id="Seg_37" n="HIAT:w" s="T1065">Ipek</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1067" id="Seg_40" n="HIAT:w" s="T1066">naga</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1071" id="Seg_44" n="HIAT:u" s="T1067">
                  <ts e="T1068" id="Seg_46" n="HIAT:w" s="T1067">Paʔi</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1069" id="Seg_49" n="HIAT:w" s="T1068">naga</ts>
                  <nts id="Seg_50" n="HIAT:ip">,</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1070" id="Seg_53" n="HIAT:w" s="T1069">lučinaʔi</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1071" id="Seg_56" n="HIAT:w" s="T1070">naga</ts>
                  <nts id="Seg_57" n="HIAT:ip">.</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1074" id="Seg_60" n="HIAT:u" s="T1071">
                  <ts e="T1072" id="Seg_62" n="HIAT:w" s="T1071">Kanaʔ</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1073" id="Seg_65" n="HIAT:w" s="T1072">dʼijenə</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1074" id="Seg_68" n="HIAT:w" s="T1073">pajlaʔ</ts>
                  <nts id="Seg_69" n="HIAT:ip">.</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1076" id="Seg_72" n="HIAT:u" s="T1074">
                  <ts e="T1075" id="Seg_74" n="HIAT:w" s="T1074">Dĭ</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1076" id="Seg_77" n="HIAT:w" s="T1075">kambi</ts>
                  <nts id="Seg_78" n="HIAT:ip">.</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1082" id="Seg_81" n="HIAT:u" s="T1076">
                  <ts e="T1077" id="Seg_83" n="HIAT:w" s="T1076">Dĭn</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1078" id="Seg_86" n="HIAT:w" s="T1077">davaj</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1079" id="Seg_89" n="HIAT:w" s="T1078">onʼiʔ</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1080" id="Seg_92" n="HIAT:w" s="T1079">pa</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1081" id="Seg_95" n="HIAT:w" s="T1080">baltuziʔ</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1082" id="Seg_98" n="HIAT:w" s="T1081">jaʔsittə</ts>
                  <nts id="Seg_99" n="HIAT:ip">.</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1087" id="Seg_102" n="HIAT:u" s="T1082">
                  <ts e="T1083" id="Seg_104" n="HIAT:w" s="T1082">Dĭgəttə</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_106" n="HIAT:ip">(</nts>
                  <ts e="T1084" id="Seg_108" n="HIAT:w" s="T1083">sü-</ts>
                  <nts id="Seg_109" n="HIAT:ip">)</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1085" id="Seg_112" n="HIAT:w" s="T1084">süjö</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1086" id="Seg_115" n="HIAT:w" s="T1085">nʼergöleʔ</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1087" id="Seg_118" n="HIAT:w" s="T1086">šobi</ts>
                  <nts id="Seg_119" n="HIAT:ip">.</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1091" id="Seg_122" n="HIAT:u" s="T1087">
                  <ts e="T1088" id="Seg_124" n="HIAT:w" s="T1087">Ĭmbi</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1089" id="Seg_127" n="HIAT:w" s="T1088">tănan</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1090" id="Seg_130" n="HIAT:w" s="T1089">kereʔ</ts>
                  <nts id="Seg_131" n="HIAT:ip">,</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1091" id="Seg_134" n="HIAT:w" s="T1090">büzʼe</ts>
                  <nts id="Seg_135" n="HIAT:ip">?</nts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1097" id="Seg_138" n="HIAT:u" s="T1091">
                  <ts e="T1092" id="Seg_140" n="HIAT:w" s="T1091">Dĭ</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1093" id="Seg_143" n="HIAT:w" s="T1092">măndə:</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1094" id="Seg_146" n="HIAT:w" s="T1093">Măna</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_148" n="HIAT:ip">(</nts>
                  <ts e="T1095" id="Seg_150" n="HIAT:w" s="T1094">paʔi=</ts>
                  <nts id="Seg_151" n="HIAT:ip">)</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1096" id="Seg_154" n="HIAT:w" s="T1095">paʔi</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1097" id="Seg_157" n="HIAT:w" s="T1096">kereʔ</ts>
                  <nts id="Seg_158" n="HIAT:ip">.</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1100" id="Seg_161" n="HIAT:u" s="T1097">
                  <ts e="T1097.tx.1" id="Seg_163" n="HIAT:w" s="T1097">A</ts>
                  <nts id="Seg_164" n="HIAT:ip">_</nts>
                  <ts e="T1098" id="Seg_166" n="HIAT:w" s="T1097.tx.1">to</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1099" id="Seg_169" n="HIAT:w" s="T1098">naga</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1100" id="Seg_172" n="HIAT:w" s="T1099">paʔi</ts>
                  <nts id="Seg_173" n="HIAT:ip">.</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1102" id="Seg_176" n="HIAT:u" s="T1100">
                  <ts e="T1101" id="Seg_178" n="HIAT:w" s="T1100">Kanaʔ</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1102" id="Seg_181" n="HIAT:w" s="T1101">maːndə</ts>
                  <nts id="Seg_182" n="HIAT:ip">!</nts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1105" id="Seg_185" n="HIAT:u" s="T1102">
                  <ts e="T1103" id="Seg_187" n="HIAT:w" s="T1102">Dĭn</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1104" id="Seg_190" n="HIAT:w" s="T1103">paʔi</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1105" id="Seg_193" n="HIAT:w" s="T1104">iʔgö</ts>
                  <nts id="Seg_194" n="HIAT:ip">!</nts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1113" id="Seg_197" n="HIAT:u" s="T1105">
                  <ts e="T1106" id="Seg_199" n="HIAT:w" s="T1105">Dĭ</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1107" id="Seg_202" n="HIAT:w" s="T1106">šobi</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1108" id="Seg_205" n="HIAT:w" s="T1107">maːndə</ts>
                  <nts id="Seg_206" n="HIAT:ip">,</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1109" id="Seg_209" n="HIAT:w" s="T1108">i</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_211" n="HIAT:ip">(</nts>
                  <ts e="T1110" id="Seg_213" n="HIAT:w" s="T1109">pu-</ts>
                  <nts id="Seg_214" n="HIAT:ip">)</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1111" id="Seg_217" n="HIAT:w" s="T1110">ugaːndə</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1112" id="Seg_220" n="HIAT:w" s="T1111">paʔi</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1113" id="Seg_223" n="HIAT:w" s="T1112">iʔgö</ts>
                  <nts id="Seg_224" n="HIAT:ip">.</nts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1117" id="Seg_227" n="HIAT:u" s="T1113">
                  <ts e="T1114" id="Seg_229" n="HIAT:w" s="T1113">Dĭgəttə</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1115" id="Seg_232" n="HIAT:w" s="T1114">nüket</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1116" id="Seg_235" n="HIAT:w" s="T1115">măndə:</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1117" id="Seg_238" n="HIAT:w" s="T1116">Kanaʔ</ts>
                  <nts id="Seg_239" n="HIAT:ip">!</nts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1120" id="Seg_242" n="HIAT:u" s="T1117">
                  <ts e="T1118" id="Seg_244" n="HIAT:w" s="T1117">Pušaj</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1119" id="Seg_247" n="HIAT:w" s="T1118">mĭləj</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1120" id="Seg_250" n="HIAT:w" s="T1119">ipek</ts>
                  <nts id="Seg_251" n="HIAT:ip">!</nts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1122" id="Seg_254" n="HIAT:u" s="T1120">
                  <ts e="T1121" id="Seg_256" n="HIAT:w" s="T1120">Dĭgəttə</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1122" id="Seg_259" n="HIAT:w" s="T1121">šobi</ts>
                  <nts id="Seg_260" n="HIAT:ip">.</nts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1125" id="Seg_263" n="HIAT:u" s="T1122">
                  <ts e="T1123" id="Seg_265" n="HIAT:w" s="T1122">Bazoʔ</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1124" id="Seg_268" n="HIAT:w" s="T1123">baltuziʔ</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1125" id="Seg_271" n="HIAT:w" s="T1124">jaʔpi</ts>
                  <nts id="Seg_272" n="HIAT:ip">.</nts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1129" id="Seg_275" n="HIAT:u" s="T1125">
                  <ts e="T1126" id="Seg_277" n="HIAT:w" s="T1125">Bazoʔ</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_279" n="HIAT:ip">(</nts>
                  <ts e="T1127" id="Seg_281" n="HIAT:w" s="T1126">šüjö=</ts>
                  <nts id="Seg_282" n="HIAT:ip">)</nts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1128" id="Seg_285" n="HIAT:w" s="T1127">süjö</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1129" id="Seg_288" n="HIAT:w" s="T1128">nʼergölaʔpi</ts>
                  <nts id="Seg_289" n="HIAT:ip">.</nts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1133" id="Seg_292" n="HIAT:u" s="T1129">
                  <ts e="T1130" id="Seg_294" n="HIAT:w" s="T1129">Ĭmbi</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1131" id="Seg_297" n="HIAT:w" s="T1130">tănan</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1132" id="Seg_300" n="HIAT:w" s="T1131">kereʔ</ts>
                  <nts id="Seg_301" n="HIAT:ip">,</nts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1133" id="Seg_304" n="HIAT:w" s="T1132">büzʼe</ts>
                  <nts id="Seg_305" n="HIAT:ip">?</nts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1135" id="Seg_308" n="HIAT:u" s="T1133">
                  <ts e="T1134" id="Seg_310" n="HIAT:w" s="T1133">Ipek</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1135" id="Seg_313" n="HIAT:w" s="T1134">naga</ts>
                  <nts id="Seg_314" n="HIAT:ip">.</nts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1137" id="Seg_317" n="HIAT:u" s="T1135">
                  <ts e="T1136" id="Seg_319" n="HIAT:w" s="T1135">Kanaʔ</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1137" id="Seg_322" n="HIAT:w" s="T1136">maːndə</ts>
                  <nts id="Seg_323" n="HIAT:ip">!</nts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1140" id="Seg_326" n="HIAT:u" s="T1137">
                  <ts e="T1138" id="Seg_328" n="HIAT:w" s="T1137">Dĭ</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1139" id="Seg_331" n="HIAT:w" s="T1138">šobi</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1140" id="Seg_334" n="HIAT:w" s="T1139">bar</ts>
                  <nts id="Seg_335" n="HIAT:ip">.</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1144" id="Seg_338" n="HIAT:u" s="T1140">
                  <ts e="T1141" id="Seg_340" n="HIAT:w" s="T1140">Tüžöjəʔi</ts>
                  <nts id="Seg_341" n="HIAT:ip">,</nts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1142" id="Seg_344" n="HIAT:w" s="T1141">ularəʔi</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1143" id="Seg_347" n="HIAT:w" s="T1142">bar</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1144" id="Seg_350" n="HIAT:w" s="T1143">iʔgö</ts>
                  <nts id="Seg_351" n="HIAT:ip">.</nts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1146" id="Seg_354" n="HIAT:u" s="T1144">
                  <ts e="T1145" id="Seg_356" n="HIAT:w" s="T1144">Albəj</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1146" id="Seg_359" n="HIAT:w" s="T1145">šeden</ts>
                  <nts id="Seg_360" n="HIAT:ip">.</nts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1150" id="Seg_363" n="HIAT:u" s="T1146">
                  <ts e="T1147" id="Seg_365" n="HIAT:w" s="T1146">I</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1148" id="Seg_368" n="HIAT:w" s="T1147">amzittə</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1149" id="Seg_371" n="HIAT:w" s="T1148">iʔgö</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1150" id="Seg_374" n="HIAT:w" s="T1149">ige</ts>
                  <nts id="Seg_375" n="HIAT:ip">.</nts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1154" id="Seg_378" n="HIAT:u" s="T1150">
                  <ts e="T1151" id="Seg_380" n="HIAT:w" s="T1150">Nüke</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1152" id="Seg_383" n="HIAT:w" s="T1151">ambi</ts>
                  <nts id="Seg_384" n="HIAT:ip">,</nts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1153" id="Seg_387" n="HIAT:w" s="T1152">ambi:</ts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1154" id="Seg_390" n="HIAT:w" s="T1153">Kanaʔ</ts>
                  <nts id="Seg_391" n="HIAT:ip">.</nts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1165" id="Seg_394" n="HIAT:u" s="T1154">
                  <ts e="T1155" id="Seg_396" n="HIAT:w" s="T1154">Nörbəʔ</ts>
                  <nts id="Seg_397" n="HIAT:ip">,</nts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1156" id="Seg_400" n="HIAT:w" s="T1155">štobɨ</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_402" n="HIAT:ip">(</nts>
                  <ts e="T1157" id="Seg_404" n="HIAT:w" s="T1156">tăn</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1158" id="Seg_407" n="HIAT:w" s="T1157">bɨl-</ts>
                  <nts id="Seg_408" n="HIAT:ip">)</nts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1159" id="Seg_411" n="HIAT:w" s="T1158">tăn</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1160" id="Seg_414" n="HIAT:w" s="T1159">ibiel</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1161" id="Seg_417" n="HIAT:w" s="T1160">koŋ</ts>
                  <nts id="Seg_418" n="HIAT:ip">,</nts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1162" id="Seg_421" n="HIAT:w" s="T1161">a</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1163" id="Seg_424" n="HIAT:w" s="T1162">măn</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1164" id="Seg_427" n="HIAT:w" s="T1163">koŋgən</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1165" id="Seg_430" n="HIAT:w" s="T1164">ne</ts>
                  <nts id="Seg_431" n="HIAT:ip">.</nts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1170" id="Seg_434" n="HIAT:u" s="T1165">
                  <ts e="T1166" id="Seg_436" n="HIAT:w" s="T1165">Dĭ</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1167" id="Seg_439" n="HIAT:w" s="T1166">kambi</ts>
                  <nts id="Seg_440" n="HIAT:ip">,</nts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1168" id="Seg_443" n="HIAT:w" s="T1167">bazoʔ</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1169" id="Seg_446" n="HIAT:w" s="T1168">baltuzʼiʔ</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1170" id="Seg_449" n="HIAT:w" s="T1169">jaʔpi</ts>
                  <nts id="Seg_450" n="HIAT:ip">.</nts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1172" id="Seg_453" n="HIAT:u" s="T1170">
                  <ts e="T1171" id="Seg_455" n="HIAT:w" s="T1170">Süjö</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1172" id="Seg_458" n="HIAT:w" s="T1171">nʼergölaʔpi</ts>
                  <nts id="Seg_459" n="HIAT:ip">.</nts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1176" id="Seg_462" n="HIAT:u" s="T1172">
                  <ts e="T1173" id="Seg_464" n="HIAT:w" s="T1172">Ĭmbi</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_466" n="HIAT:ip">(</nts>
                  <ts e="T1174" id="Seg_468" n="HIAT:w" s="T1173">tăna-</ts>
                  <nts id="Seg_469" n="HIAT:ip">)</nts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1175" id="Seg_472" n="HIAT:w" s="T1174">tănan</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1176" id="Seg_475" n="HIAT:w" s="T1175">kereʔ</ts>
                  <nts id="Seg_476" n="HIAT:ip">?</nts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1189" id="Seg_479" n="HIAT:u" s="T1176">
                  <ts e="T1177" id="Seg_481" n="HIAT:w" s="T1176">Da</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1178" id="Seg_484" n="HIAT:w" s="T1177">măn</ts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_486" n="HIAT:ip">(</nts>
                  <ts e="T1179" id="Seg_488" n="HIAT:w" s="T1178">nük-</ts>
                  <nts id="Seg_489" n="HIAT:ip">)</nts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1180" id="Seg_492" n="HIAT:w" s="T1179">măn</ts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1181" id="Seg_495" n="HIAT:w" s="T1180">koŋ</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1182" id="Seg_498" n="HIAT:w" s="T1181">ilem</ts>
                  <nts id="Seg_499" n="HIAT:ip">,</nts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1183" id="Seg_502" n="HIAT:w" s="T1182">a</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1184" id="Seg_505" n="HIAT:w" s="T1183">măn</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1185" id="Seg_508" n="HIAT:w" s="T1184">nükem</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1186" id="Seg_511" n="HIAT:w" s="T1185">tože</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1187" id="Seg_514" n="HIAT:w" s="T1186">măn</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1188" id="Seg_517" n="HIAT:w" s="T1187">koŋ</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1189" id="Seg_520" n="HIAT:w" s="T1188">moləj</ts>
                  <nts id="Seg_521" n="HIAT:ip">.</nts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1190" id="Seg_524" n="HIAT:u" s="T1189">
                  <ts e="T1190" id="Seg_526" n="HIAT:w" s="T1189">Kanaʔ</ts>
                  <nts id="Seg_527" n="HIAT:ip">!</nts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1192" id="Seg_530" n="HIAT:u" s="T1190">
                  <ts e="T1191" id="Seg_532" n="HIAT:w" s="T1190">Dĭgəttə</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1192" id="Seg_535" n="HIAT:w" s="T1191">šobi</ts>
                  <nts id="Seg_536" n="HIAT:ip">.</nts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1194" id="Seg_539" n="HIAT:u" s="T1192">
                  <ts e="T1193" id="Seg_541" n="HIAT:w" s="T1192">Amnobiʔi</ts>
                  <nts id="Seg_542" n="HIAT:ip">,</nts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1194" id="Seg_545" n="HIAT:w" s="T1193">amnobiʔi</ts>
                  <nts id="Seg_546" n="HIAT:ip">.</nts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1207" id="Seg_549" n="HIAT:u" s="T1194">
                  <ts e="T1195" id="Seg_551" n="HIAT:w" s="T1194">Nükendə</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_553" n="HIAT:ip">(</nts>
                  <ts e="T1196" id="Seg_555" n="HIAT:w" s="T1195">a</ts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1196.tx.1" id="Seg_558" n="HIAT:w" s="T1196">bazoʔ</ts>
                  <nts id="Seg_559" n="HIAT:ip">)</nts>
                  <ts e="T1197" id="Seg_561" n="HIAT:w" s="T1196.tx.1">:</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1198" id="Seg_564" n="HIAT:w" s="T1197">Kanaʔ</ts>
                  <nts id="Seg_565" n="HIAT:ip">,</nts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1199" id="Seg_568" n="HIAT:w" s="T1198">nörbəʔ</ts>
                  <nts id="Seg_569" n="HIAT:ip">,</nts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1200" id="Seg_572" n="HIAT:w" s="T1199">štobɨ</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1201" id="Seg_575" n="HIAT:w" s="T1200">tăn</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1202" id="Seg_578" n="HIAT:w" s="T1201">ibiel</ts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1203" id="Seg_581" n="HIAT:w" s="T1202">bɨ</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1204" id="Seg_584" n="HIAT:w" s="T1203">tsar</ts>
                  <nts id="Seg_585" n="HIAT:ip">,</nts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1205" id="Seg_588" n="HIAT:w" s="T1204">a</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1206" id="Seg_591" n="HIAT:w" s="T1205">măn</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1207" id="Seg_594" n="HIAT:w" s="T1206">tsaritsazʼiʔ</ts>
                  <nts id="Seg_595" n="HIAT:ip">.</nts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1208" id="Seg_598" n="HIAT:u" s="T1207">
                  <ts e="T1208" id="Seg_600" n="HIAT:w" s="T1207">Kambi</ts>
                  <nts id="Seg_601" n="HIAT:ip">.</nts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1214" id="Seg_604" n="HIAT:u" s="T1208">
                  <nts id="Seg_605" n="HIAT:ip">(</nts>
                  <ts e="T1209" id="Seg_607" n="HIAT:w" s="T1208">Süjö</ts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1210" id="Seg_610" n="HIAT:w" s="T1209">bazoʔ=</ts>
                  <nts id="Seg_611" n="HIAT:ip">)</nts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1211" id="Seg_614" n="HIAT:w" s="T1210">Davaj</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1212" id="Seg_617" n="HIAT:w" s="T1211">baltuzʼiʔ</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1213" id="Seg_620" n="HIAT:w" s="T1212">jaʔsittə</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1214" id="Seg_623" n="HIAT:w" s="T1213">pam</ts>
                  <nts id="Seg_624" n="HIAT:ip">.</nts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1216" id="Seg_627" n="HIAT:u" s="T1214">
                  <ts e="T1215" id="Seg_629" n="HIAT:w" s="T1214">Süjö</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1216" id="Seg_632" n="HIAT:w" s="T1215">nʼergölüʔpi</ts>
                  <nts id="Seg_633" n="HIAT:ip">.</nts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1220" id="Seg_636" n="HIAT:u" s="T1216">
                  <ts e="T1217" id="Seg_638" n="HIAT:w" s="T1216">Ĭmbi</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1218" id="Seg_641" n="HIAT:w" s="T1217">tănan</ts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1219" id="Seg_644" n="HIAT:w" s="T1218">kereʔ</ts>
                  <nts id="Seg_645" n="HIAT:ip">,</nts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1220" id="Seg_648" n="HIAT:w" s="T1219">büzʼe</ts>
                  <nts id="Seg_649" n="HIAT:ip">?</nts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1229" id="Seg_652" n="HIAT:u" s="T1220">
                  <ts e="T1221" id="Seg_654" n="HIAT:w" s="T1220">Da</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1222" id="Seg_657" n="HIAT:w" s="T1221">măn</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1223" id="Seg_660" n="HIAT:w" s="T1222">štobɨ</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1224" id="Seg_663" n="HIAT:w" s="T1223">ibiem</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1225" id="Seg_666" n="HIAT:w" s="T1224">tsar</ts>
                  <nts id="Seg_667" n="HIAT:ip">,</nts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1226" id="Seg_670" n="HIAT:w" s="T1225">a</ts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1227" id="Seg_673" n="HIAT:w" s="T1226">măn</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1228" id="Seg_676" n="HIAT:w" s="T1227">nükem</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1229" id="Seg_679" n="HIAT:w" s="T1228">tsaritsa</ts>
                  <nts id="Seg_680" n="HIAT:ip">.</nts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1230" id="Seg_683" n="HIAT:u" s="T1229">
                  <ts e="T1230" id="Seg_685" n="HIAT:w" s="T1229">Kanaʔ</ts>
                  <nts id="Seg_686" n="HIAT:ip">!</nts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1240" id="Seg_689" n="HIAT:u" s="T1230">
                  <ts e="T1231" id="Seg_691" n="HIAT:w" s="T1230">Dĭ</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_693" n="HIAT:ip">(</nts>
                  <ts e="T1232" id="Seg_695" n="HIAT:w" s="T1231">šo-</ts>
                  <nts id="Seg_696" n="HIAT:ip">)</nts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1233" id="Seg_699" n="HIAT:w" s="T1232">šonəga</ts>
                  <nts id="Seg_700" n="HIAT:ip">,</nts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1234" id="Seg_703" n="HIAT:w" s="T1233">il</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1235" id="Seg_706" n="HIAT:w" s="T1234">bar</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1237" id="Seg_709" n="HIAT:w" s="T1235">üžüʔi</ts>
                  <nts id="Seg_710" n="HIAT:ip">,</nts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1238" id="Seg_713" n="HIAT:w" s="T1237">bar</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1239" id="Seg_716" n="HIAT:w" s="T1238">saʔməlaʔbəjəʔ</ts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1240" id="Seg_719" n="HIAT:w" s="T1239">dĭʔnə</ts>
                  <nts id="Seg_720" n="HIAT:ip">.</nts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1244" id="Seg_723" n="HIAT:u" s="T1240">
                  <ts e="T1241" id="Seg_725" n="HIAT:w" s="T1240">Dĭgəttə</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1242" id="Seg_728" n="HIAT:w" s="T1241">šobi</ts>
                  <nts id="Seg_729" n="HIAT:ip">,</nts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1243" id="Seg_732" n="HIAT:w" s="T1242">amnobiʔi</ts>
                  <nts id="Seg_733" n="HIAT:ip">,</nts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1244" id="Seg_736" n="HIAT:w" s="T1243">amnobiʔi</ts>
                  <nts id="Seg_737" n="HIAT:ip">.</nts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1250" id="Seg_740" n="HIAT:u" s="T1244">
                  <ts e="T1245" id="Seg_742" n="HIAT:w" s="T1244">Dĭgəttə</ts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1246" id="Seg_745" n="HIAT:w" s="T1245">nüke</ts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1247" id="Seg_748" n="HIAT:w" s="T1246">bazoʔ</ts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1248" id="Seg_751" n="HIAT:w" s="T1247">sürerleʔbə</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1249" id="Seg_754" n="HIAT:w" s="T1248">dĭ</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1250" id="Seg_757" n="HIAT:w" s="T1249">büzʼem</ts>
                  <nts id="Seg_758" n="HIAT:ip">.</nts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1251" id="Seg_761" n="HIAT:u" s="T1250">
                  <ts e="T1251" id="Seg_763" n="HIAT:w" s="T1250">Kanaʔ</ts>
                  <nts id="Seg_764" n="HIAT:ip">!</nts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1259" id="Seg_767" n="HIAT:u" s="T1251">
                  <ts e="T1252" id="Seg_769" n="HIAT:w" s="T1251">Štobɨ</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1253" id="Seg_772" n="HIAT:w" s="T1252">tăn</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1254" id="Seg_775" n="HIAT:w" s="T1253">kudaj</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1255" id="Seg_778" n="HIAT:w" s="T1254">ibiel</ts>
                  <nts id="Seg_779" n="HIAT:ip">,</nts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1256" id="Seg_782" n="HIAT:w" s="T1255">a</ts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1257" id="Seg_785" n="HIAT:w" s="T1256">măn</ts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1258" id="Seg_788" n="HIAT:w" s="T1257">kudajən</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1259" id="Seg_791" n="HIAT:w" s="T1258">ijat</ts>
                  <nts id="Seg_792" n="HIAT:ip">.</nts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1267" id="Seg_795" n="HIAT:u" s="T1259">
                  <ts e="T1260" id="Seg_797" n="HIAT:w" s="T1259">Dĭ</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1261" id="Seg_800" n="HIAT:w" s="T1260">kambi</ts>
                  <nts id="Seg_801" n="HIAT:ip">,</nts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1262" id="Seg_804" n="HIAT:w" s="T1261">davaj</ts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1263" id="Seg_807" n="HIAT:w" s="T1262">bazoʔ</ts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1264" id="Seg_810" n="HIAT:w" s="T1263">baltuzʼiʔ</ts>
                  <nts id="Seg_811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1265" id="Seg_813" n="HIAT:w" s="T1264">jaʔsittə</ts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1266" id="Seg_816" n="HIAT:w" s="T1265">dĭ</ts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1267" id="Seg_819" n="HIAT:w" s="T1266">pam</ts>
                  <nts id="Seg_820" n="HIAT:ip">.</nts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1271" id="Seg_823" n="HIAT:u" s="T1267">
                  <ts e="T1268" id="Seg_825" n="HIAT:w" s="T1267">Dĭ</ts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1269" id="Seg_828" n="HIAT:w" s="T1268">süjö</ts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1270" id="Seg_831" n="HIAT:w" s="T1269">nʼergöleʔ</ts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1271" id="Seg_834" n="HIAT:w" s="T1270">šobi</ts>
                  <nts id="Seg_835" n="HIAT:ip">,</nts>
                  <nts id="Seg_836" n="HIAT:ip">.</nts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1274" id="Seg_839" n="HIAT:u" s="T1271">
                  <ts e="T1272" id="Seg_841" n="HIAT:w" s="T1271">Ĭmbi</ts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1273" id="Seg_844" n="HIAT:w" s="T1272">tănan</ts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1274" id="Seg_847" n="HIAT:w" s="T1273">kereʔ</ts>
                  <nts id="Seg_848" n="HIAT:ip">?</nts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1284" id="Seg_851" n="HIAT:u" s="T1274">
                  <ts e="T1275" id="Seg_853" n="HIAT:w" s="T1274">Da</ts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1276" id="Seg_856" n="HIAT:w" s="T1275">măn</ts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_858" n="HIAT:ip">(</nts>
                  <ts e="T1277" id="Seg_860" n="HIAT:w" s="T1276">kudaj=</ts>
                  <nts id="Seg_861" n="HIAT:ip">)</nts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1278" id="Seg_864" n="HIAT:w" s="T1277">kudaj</ts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1279" id="Seg_867" n="HIAT:w" s="T1278">molam</ts>
                  <nts id="Seg_868" n="HIAT:ip">,</nts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1280" id="Seg_871" n="HIAT:w" s="T1279">a</ts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1281" id="Seg_874" n="HIAT:w" s="T1280">măn</ts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1282" id="Seg_877" n="HIAT:w" s="T1281">nükem</ts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1283" id="Seg_880" n="HIAT:w" s="T1282">kudajən</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1284" id="Seg_883" n="HIAT:w" s="T1283">ijat</ts>
                  <nts id="Seg_884" n="HIAT:ip">.</nts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1285" id="Seg_887" n="HIAT:u" s="T1284">
                  <ts e="T1285" id="Seg_889" n="HIAT:w" s="T1284">Kanaʔ</ts>
                  <nts id="Seg_890" n="HIAT:ip">!</nts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1293" id="Seg_893" n="HIAT:u" s="T1285">
                  <ts e="T1286" id="Seg_895" n="HIAT:w" s="T1285">Tăn</ts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1287" id="Seg_898" n="HIAT:w" s="T1286">buga</ts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1288" id="Seg_901" n="HIAT:w" s="T1287">moləj</ts>
                  <nts id="Seg_902" n="HIAT:ip">,</nts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1289" id="Seg_905" n="HIAT:w" s="T1288">a</ts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1290" id="Seg_908" n="HIAT:w" s="T1289">tăn</ts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1291" id="Seg_911" n="HIAT:w" s="T1290">nüke</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1292" id="Seg_914" n="HIAT:w" s="T1291">šoška</ts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1293" id="Seg_917" n="HIAT:w" s="T1292">moləj</ts>
                  <nts id="Seg_918" n="HIAT:ip">.</nts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1295" id="Seg_921" n="HIAT:u" s="T1293">
                  <ts e="T1294" id="Seg_923" n="HIAT:w" s="T1293">Dĭgəttə</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1295" id="Seg_926" n="HIAT:w" s="T1294">šobi</ts>
                  <nts id="Seg_927" n="HIAT:ip">.</nts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1301" id="Seg_930" n="HIAT:u" s="T1295">
                  <ts e="T1296" id="Seg_932" n="HIAT:w" s="T1295">Dĭ</ts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1297" id="Seg_935" n="HIAT:w" s="T1296">büzʼe</ts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1298" id="Seg_938" n="HIAT:w" s="T1297">buga</ts>
                  <nts id="Seg_939" n="HIAT:ip">,</nts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1299" id="Seg_942" n="HIAT:w" s="T1298">a</ts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1300" id="Seg_945" n="HIAT:w" s="T1299">nüket</ts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1301" id="Seg_948" n="HIAT:w" s="T1300">šoška</ts>
                  <nts id="Seg_949" n="HIAT:ip">.</nts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1304" id="Seg_952" n="HIAT:u" s="T1301">
                  <ts e="T1302" id="Seg_954" n="HIAT:w" s="T1301">I</ts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1303" id="Seg_957" n="HIAT:w" s="T1302">dărəʔ</ts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1304" id="Seg_960" n="HIAT:w" s="T1303">maːlaʔbəʔi</ts>
                  <nts id="Seg_961" n="HIAT:ip">.</nts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1305" id="Seg_964" n="HIAT:u" s="T1304">
                  <ts e="T1305" id="Seg_966" n="HIAT:w" s="T1304">Kabarləj</ts>
                  <nts id="Seg_967" n="HIAT:ip">.</nts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T1305" id="Seg_969" n="sc" s="T1057">
               <ts e="T1058" id="Seg_971" n="e" s="T1057">Amnobiʔi </ts>
               <ts e="T1059" id="Seg_973" n="e" s="T1058">nüke, </ts>
               <ts e="T1060" id="Seg_975" n="e" s="T1059">büzʼe. </ts>
               <ts e="T1061" id="Seg_977" n="e" s="T1060">(Dĭn=) </ts>
               <ts e="T1062" id="Seg_979" n="e" s="T1061">Dĭzeŋgən </ts>
               <ts e="T1063" id="Seg_981" n="e" s="T1062">ĭmbidə </ts>
               <ts e="T1064" id="Seg_983" n="e" s="T1063">ej </ts>
               <ts e="T1065" id="Seg_985" n="e" s="T1064">nagobi. </ts>
               <ts e="T1066" id="Seg_987" n="e" s="T1065">Ipek </ts>
               <ts e="T1067" id="Seg_989" n="e" s="T1066">naga. </ts>
               <ts e="T1068" id="Seg_991" n="e" s="T1067">Paʔi </ts>
               <ts e="T1069" id="Seg_993" n="e" s="T1068">naga, </ts>
               <ts e="T1070" id="Seg_995" n="e" s="T1069">lučinaʔi </ts>
               <ts e="T1071" id="Seg_997" n="e" s="T1070">naga. </ts>
               <ts e="T1072" id="Seg_999" n="e" s="T1071">Kanaʔ </ts>
               <ts e="T1073" id="Seg_1001" n="e" s="T1072">dʼijenə </ts>
               <ts e="T1074" id="Seg_1003" n="e" s="T1073">pajlaʔ. </ts>
               <ts e="T1075" id="Seg_1005" n="e" s="T1074">Dĭ </ts>
               <ts e="T1076" id="Seg_1007" n="e" s="T1075">kambi. </ts>
               <ts e="T1077" id="Seg_1009" n="e" s="T1076">Dĭn </ts>
               <ts e="T1078" id="Seg_1011" n="e" s="T1077">davaj </ts>
               <ts e="T1079" id="Seg_1013" n="e" s="T1078">onʼiʔ </ts>
               <ts e="T1080" id="Seg_1015" n="e" s="T1079">pa </ts>
               <ts e="T1081" id="Seg_1017" n="e" s="T1080">baltuziʔ </ts>
               <ts e="T1082" id="Seg_1019" n="e" s="T1081">jaʔsittə. </ts>
               <ts e="T1083" id="Seg_1021" n="e" s="T1082">Dĭgəttə </ts>
               <ts e="T1084" id="Seg_1023" n="e" s="T1083">(sü-) </ts>
               <ts e="T1085" id="Seg_1025" n="e" s="T1084">süjö </ts>
               <ts e="T1086" id="Seg_1027" n="e" s="T1085">nʼergöleʔ </ts>
               <ts e="T1087" id="Seg_1029" n="e" s="T1086">šobi. </ts>
               <ts e="T1088" id="Seg_1031" n="e" s="T1087">Ĭmbi </ts>
               <ts e="T1089" id="Seg_1033" n="e" s="T1088">tănan </ts>
               <ts e="T1090" id="Seg_1035" n="e" s="T1089">kereʔ, </ts>
               <ts e="T1091" id="Seg_1037" n="e" s="T1090">büzʼe? </ts>
               <ts e="T1092" id="Seg_1039" n="e" s="T1091">Dĭ </ts>
               <ts e="T1093" id="Seg_1041" n="e" s="T1092">măndə: </ts>
               <ts e="T1094" id="Seg_1043" n="e" s="T1093">Măna </ts>
               <ts e="T1095" id="Seg_1045" n="e" s="T1094">(paʔi=) </ts>
               <ts e="T1096" id="Seg_1047" n="e" s="T1095">paʔi </ts>
               <ts e="T1097" id="Seg_1049" n="e" s="T1096">kereʔ. </ts>
               <ts e="T1098" id="Seg_1051" n="e" s="T1097">A_to </ts>
               <ts e="T1099" id="Seg_1053" n="e" s="T1098">naga </ts>
               <ts e="T1100" id="Seg_1055" n="e" s="T1099">paʔi. </ts>
               <ts e="T1101" id="Seg_1057" n="e" s="T1100">Kanaʔ </ts>
               <ts e="T1102" id="Seg_1059" n="e" s="T1101">maːndə! </ts>
               <ts e="T1103" id="Seg_1061" n="e" s="T1102">Dĭn </ts>
               <ts e="T1104" id="Seg_1063" n="e" s="T1103">paʔi </ts>
               <ts e="T1105" id="Seg_1065" n="e" s="T1104">iʔgö! </ts>
               <ts e="T1106" id="Seg_1067" n="e" s="T1105">Dĭ </ts>
               <ts e="T1107" id="Seg_1069" n="e" s="T1106">šobi </ts>
               <ts e="T1108" id="Seg_1071" n="e" s="T1107">maːndə, </ts>
               <ts e="T1109" id="Seg_1073" n="e" s="T1108">i </ts>
               <ts e="T1110" id="Seg_1075" n="e" s="T1109">(pu-) </ts>
               <ts e="T1111" id="Seg_1077" n="e" s="T1110">ugaːndə </ts>
               <ts e="T1112" id="Seg_1079" n="e" s="T1111">paʔi </ts>
               <ts e="T1113" id="Seg_1081" n="e" s="T1112">iʔgö. </ts>
               <ts e="T1114" id="Seg_1083" n="e" s="T1113">Dĭgəttə </ts>
               <ts e="T1115" id="Seg_1085" n="e" s="T1114">nüket </ts>
               <ts e="T1116" id="Seg_1087" n="e" s="T1115">măndə: </ts>
               <ts e="T1117" id="Seg_1089" n="e" s="T1116">Kanaʔ! </ts>
               <ts e="T1118" id="Seg_1091" n="e" s="T1117">Pušaj </ts>
               <ts e="T1119" id="Seg_1093" n="e" s="T1118">mĭləj </ts>
               <ts e="T1120" id="Seg_1095" n="e" s="T1119">ipek! </ts>
               <ts e="T1121" id="Seg_1097" n="e" s="T1120">Dĭgəttə </ts>
               <ts e="T1122" id="Seg_1099" n="e" s="T1121">šobi. </ts>
               <ts e="T1123" id="Seg_1101" n="e" s="T1122">Bazoʔ </ts>
               <ts e="T1124" id="Seg_1103" n="e" s="T1123">baltuziʔ </ts>
               <ts e="T1125" id="Seg_1105" n="e" s="T1124">jaʔpi. </ts>
               <ts e="T1126" id="Seg_1107" n="e" s="T1125">Bazoʔ </ts>
               <ts e="T1127" id="Seg_1109" n="e" s="T1126">(šüjö=) </ts>
               <ts e="T1128" id="Seg_1111" n="e" s="T1127">süjö </ts>
               <ts e="T1129" id="Seg_1113" n="e" s="T1128">nʼergölaʔpi. </ts>
               <ts e="T1130" id="Seg_1115" n="e" s="T1129">Ĭmbi </ts>
               <ts e="T1131" id="Seg_1117" n="e" s="T1130">tănan </ts>
               <ts e="T1132" id="Seg_1119" n="e" s="T1131">kereʔ, </ts>
               <ts e="T1133" id="Seg_1121" n="e" s="T1132">büzʼe? </ts>
               <ts e="T1134" id="Seg_1123" n="e" s="T1133">Ipek </ts>
               <ts e="T1135" id="Seg_1125" n="e" s="T1134">naga. </ts>
               <ts e="T1136" id="Seg_1127" n="e" s="T1135">Kanaʔ </ts>
               <ts e="T1137" id="Seg_1129" n="e" s="T1136">maːndə! </ts>
               <ts e="T1138" id="Seg_1131" n="e" s="T1137">Dĭ </ts>
               <ts e="T1139" id="Seg_1133" n="e" s="T1138">šobi </ts>
               <ts e="T1140" id="Seg_1135" n="e" s="T1139">bar. </ts>
               <ts e="T1141" id="Seg_1137" n="e" s="T1140">Tüžöjəʔi, </ts>
               <ts e="T1142" id="Seg_1139" n="e" s="T1141">ularəʔi </ts>
               <ts e="T1143" id="Seg_1141" n="e" s="T1142">bar </ts>
               <ts e="T1144" id="Seg_1143" n="e" s="T1143">iʔgö. </ts>
               <ts e="T1145" id="Seg_1145" n="e" s="T1144">Albəj </ts>
               <ts e="T1146" id="Seg_1147" n="e" s="T1145">šeden. </ts>
               <ts e="T1147" id="Seg_1149" n="e" s="T1146">I </ts>
               <ts e="T1148" id="Seg_1151" n="e" s="T1147">amzittə </ts>
               <ts e="T1149" id="Seg_1153" n="e" s="T1148">iʔgö </ts>
               <ts e="T1150" id="Seg_1155" n="e" s="T1149">ige. </ts>
               <ts e="T1151" id="Seg_1157" n="e" s="T1150">Nüke </ts>
               <ts e="T1152" id="Seg_1159" n="e" s="T1151">ambi, </ts>
               <ts e="T1153" id="Seg_1161" n="e" s="T1152">ambi: </ts>
               <ts e="T1154" id="Seg_1163" n="e" s="T1153">Kanaʔ. </ts>
               <ts e="T1155" id="Seg_1165" n="e" s="T1154">Nörbəʔ, </ts>
               <ts e="T1156" id="Seg_1167" n="e" s="T1155">štobɨ </ts>
               <ts e="T1157" id="Seg_1169" n="e" s="T1156">(tăn </ts>
               <ts e="T1158" id="Seg_1171" n="e" s="T1157">bɨl-) </ts>
               <ts e="T1159" id="Seg_1173" n="e" s="T1158">tăn </ts>
               <ts e="T1160" id="Seg_1175" n="e" s="T1159">ibiel </ts>
               <ts e="T1161" id="Seg_1177" n="e" s="T1160">koŋ, </ts>
               <ts e="T1162" id="Seg_1179" n="e" s="T1161">a </ts>
               <ts e="T1163" id="Seg_1181" n="e" s="T1162">măn </ts>
               <ts e="T1164" id="Seg_1183" n="e" s="T1163">koŋgən </ts>
               <ts e="T1165" id="Seg_1185" n="e" s="T1164">ne. </ts>
               <ts e="T1166" id="Seg_1187" n="e" s="T1165">Dĭ </ts>
               <ts e="T1167" id="Seg_1189" n="e" s="T1166">kambi, </ts>
               <ts e="T1168" id="Seg_1191" n="e" s="T1167">bazoʔ </ts>
               <ts e="T1169" id="Seg_1193" n="e" s="T1168">baltuzʼiʔ </ts>
               <ts e="T1170" id="Seg_1195" n="e" s="T1169">jaʔpi. </ts>
               <ts e="T1171" id="Seg_1197" n="e" s="T1170">Süjö </ts>
               <ts e="T1172" id="Seg_1199" n="e" s="T1171">nʼergölaʔpi. </ts>
               <ts e="T1173" id="Seg_1201" n="e" s="T1172">Ĭmbi </ts>
               <ts e="T1174" id="Seg_1203" n="e" s="T1173">(tăna-) </ts>
               <ts e="T1175" id="Seg_1205" n="e" s="T1174">tănan </ts>
               <ts e="T1176" id="Seg_1207" n="e" s="T1175">kereʔ? </ts>
               <ts e="T1177" id="Seg_1209" n="e" s="T1176">Da </ts>
               <ts e="T1178" id="Seg_1211" n="e" s="T1177">măn </ts>
               <ts e="T1179" id="Seg_1213" n="e" s="T1178">(nük-) </ts>
               <ts e="T1180" id="Seg_1215" n="e" s="T1179">măn </ts>
               <ts e="T1181" id="Seg_1217" n="e" s="T1180">koŋ </ts>
               <ts e="T1182" id="Seg_1219" n="e" s="T1181">ilem, </ts>
               <ts e="T1183" id="Seg_1221" n="e" s="T1182">a </ts>
               <ts e="T1184" id="Seg_1223" n="e" s="T1183">măn </ts>
               <ts e="T1185" id="Seg_1225" n="e" s="T1184">nükem </ts>
               <ts e="T1186" id="Seg_1227" n="e" s="T1185">tože </ts>
               <ts e="T1187" id="Seg_1229" n="e" s="T1186">măn </ts>
               <ts e="T1188" id="Seg_1231" n="e" s="T1187">koŋ </ts>
               <ts e="T1189" id="Seg_1233" n="e" s="T1188">moləj. </ts>
               <ts e="T1190" id="Seg_1235" n="e" s="T1189">Kanaʔ! </ts>
               <ts e="T1191" id="Seg_1237" n="e" s="T1190">Dĭgəttə </ts>
               <ts e="T1192" id="Seg_1239" n="e" s="T1191">šobi. </ts>
               <ts e="T1193" id="Seg_1241" n="e" s="T1192">Amnobiʔi, </ts>
               <ts e="T1194" id="Seg_1243" n="e" s="T1193">amnobiʔi. </ts>
               <ts e="T1195" id="Seg_1245" n="e" s="T1194">Nükendə </ts>
               <ts e="T1196" id="Seg_1247" n="e" s="T1195">(a </ts>
               <ts e="T1197" id="Seg_1249" n="e" s="T1196">bazoʔ): </ts>
               <ts e="T1198" id="Seg_1251" n="e" s="T1197">Kanaʔ, </ts>
               <ts e="T1199" id="Seg_1253" n="e" s="T1198">nörbəʔ, </ts>
               <ts e="T1200" id="Seg_1255" n="e" s="T1199">štobɨ </ts>
               <ts e="T1201" id="Seg_1257" n="e" s="T1200">tăn </ts>
               <ts e="T1202" id="Seg_1259" n="e" s="T1201">ibiel </ts>
               <ts e="T1203" id="Seg_1261" n="e" s="T1202">bɨ </ts>
               <ts e="T1204" id="Seg_1263" n="e" s="T1203">tsar, </ts>
               <ts e="T1205" id="Seg_1265" n="e" s="T1204">a </ts>
               <ts e="T1206" id="Seg_1267" n="e" s="T1205">măn </ts>
               <ts e="T1207" id="Seg_1269" n="e" s="T1206">tsaritsazʼiʔ. </ts>
               <ts e="T1208" id="Seg_1271" n="e" s="T1207">Kambi. </ts>
               <ts e="T1209" id="Seg_1273" n="e" s="T1208">(Süjö </ts>
               <ts e="T1210" id="Seg_1275" n="e" s="T1209">bazoʔ=) </ts>
               <ts e="T1211" id="Seg_1277" n="e" s="T1210">Davaj </ts>
               <ts e="T1212" id="Seg_1279" n="e" s="T1211">baltuzʼiʔ </ts>
               <ts e="T1213" id="Seg_1281" n="e" s="T1212">jaʔsittə </ts>
               <ts e="T1214" id="Seg_1283" n="e" s="T1213">pam. </ts>
               <ts e="T1215" id="Seg_1285" n="e" s="T1214">Süjö </ts>
               <ts e="T1216" id="Seg_1287" n="e" s="T1215">nʼergölüʔpi. </ts>
               <ts e="T1217" id="Seg_1289" n="e" s="T1216">Ĭmbi </ts>
               <ts e="T1218" id="Seg_1291" n="e" s="T1217">tănan </ts>
               <ts e="T1219" id="Seg_1293" n="e" s="T1218">kereʔ, </ts>
               <ts e="T1220" id="Seg_1295" n="e" s="T1219">büzʼe? </ts>
               <ts e="T1221" id="Seg_1297" n="e" s="T1220">Da </ts>
               <ts e="T1222" id="Seg_1299" n="e" s="T1221">măn </ts>
               <ts e="T1223" id="Seg_1301" n="e" s="T1222">štobɨ </ts>
               <ts e="T1224" id="Seg_1303" n="e" s="T1223">ibiem </ts>
               <ts e="T1225" id="Seg_1305" n="e" s="T1224">tsar, </ts>
               <ts e="T1226" id="Seg_1307" n="e" s="T1225">a </ts>
               <ts e="T1227" id="Seg_1309" n="e" s="T1226">măn </ts>
               <ts e="T1228" id="Seg_1311" n="e" s="T1227">nükem </ts>
               <ts e="T1229" id="Seg_1313" n="e" s="T1228">tsaritsa. </ts>
               <ts e="T1230" id="Seg_1315" n="e" s="T1229">Kanaʔ! </ts>
               <ts e="T1231" id="Seg_1317" n="e" s="T1230">Dĭ </ts>
               <ts e="T1232" id="Seg_1319" n="e" s="T1231">(šo-) </ts>
               <ts e="T1233" id="Seg_1321" n="e" s="T1232">šonəga, </ts>
               <ts e="T1234" id="Seg_1323" n="e" s="T1233">il </ts>
               <ts e="T1235" id="Seg_1325" n="e" s="T1234">bar </ts>
               <ts e="T1237" id="Seg_1327" n="e" s="T1235">üžüʔi, </ts>
               <ts e="T1238" id="Seg_1329" n="e" s="T1237">bar </ts>
               <ts e="T1239" id="Seg_1331" n="e" s="T1238">saʔməlaʔbəjəʔ </ts>
               <ts e="T1240" id="Seg_1333" n="e" s="T1239">dĭʔnə. </ts>
               <ts e="T1241" id="Seg_1335" n="e" s="T1240">Dĭgəttə </ts>
               <ts e="T1242" id="Seg_1337" n="e" s="T1241">šobi, </ts>
               <ts e="T1243" id="Seg_1339" n="e" s="T1242">amnobiʔi, </ts>
               <ts e="T1244" id="Seg_1341" n="e" s="T1243">amnobiʔi. </ts>
               <ts e="T1245" id="Seg_1343" n="e" s="T1244">Dĭgəttə </ts>
               <ts e="T1246" id="Seg_1345" n="e" s="T1245">nüke </ts>
               <ts e="T1247" id="Seg_1347" n="e" s="T1246">bazoʔ </ts>
               <ts e="T1248" id="Seg_1349" n="e" s="T1247">sürerleʔbə </ts>
               <ts e="T1249" id="Seg_1351" n="e" s="T1248">dĭ </ts>
               <ts e="T1250" id="Seg_1353" n="e" s="T1249">büzʼem. </ts>
               <ts e="T1251" id="Seg_1355" n="e" s="T1250">Kanaʔ! </ts>
               <ts e="T1252" id="Seg_1357" n="e" s="T1251">Štobɨ </ts>
               <ts e="T1253" id="Seg_1359" n="e" s="T1252">tăn </ts>
               <ts e="T1254" id="Seg_1361" n="e" s="T1253">kudaj </ts>
               <ts e="T1255" id="Seg_1363" n="e" s="T1254">ibiel, </ts>
               <ts e="T1256" id="Seg_1365" n="e" s="T1255">a </ts>
               <ts e="T1257" id="Seg_1367" n="e" s="T1256">măn </ts>
               <ts e="T1258" id="Seg_1369" n="e" s="T1257">kudajən </ts>
               <ts e="T1259" id="Seg_1371" n="e" s="T1258">ijat. </ts>
               <ts e="T1260" id="Seg_1373" n="e" s="T1259">Dĭ </ts>
               <ts e="T1261" id="Seg_1375" n="e" s="T1260">kambi, </ts>
               <ts e="T1262" id="Seg_1377" n="e" s="T1261">davaj </ts>
               <ts e="T1263" id="Seg_1379" n="e" s="T1262">bazoʔ </ts>
               <ts e="T1264" id="Seg_1381" n="e" s="T1263">baltuzʼiʔ </ts>
               <ts e="T1265" id="Seg_1383" n="e" s="T1264">jaʔsittə </ts>
               <ts e="T1266" id="Seg_1385" n="e" s="T1265">dĭ </ts>
               <ts e="T1267" id="Seg_1387" n="e" s="T1266">pam. </ts>
               <ts e="T1268" id="Seg_1389" n="e" s="T1267">Dĭ </ts>
               <ts e="T1269" id="Seg_1391" n="e" s="T1268">süjö </ts>
               <ts e="T1270" id="Seg_1393" n="e" s="T1269">nʼergöleʔ </ts>
               <ts e="T1271" id="Seg_1395" n="e" s="T1270">šobi,. </ts>
               <ts e="T1272" id="Seg_1397" n="e" s="T1271">Ĭmbi </ts>
               <ts e="T1273" id="Seg_1399" n="e" s="T1272">tănan </ts>
               <ts e="T1274" id="Seg_1401" n="e" s="T1273">kereʔ? </ts>
               <ts e="T1275" id="Seg_1403" n="e" s="T1274">Da </ts>
               <ts e="T1276" id="Seg_1405" n="e" s="T1275">măn </ts>
               <ts e="T1277" id="Seg_1407" n="e" s="T1276">(kudaj=) </ts>
               <ts e="T1278" id="Seg_1409" n="e" s="T1277">kudaj </ts>
               <ts e="T1279" id="Seg_1411" n="e" s="T1278">molam, </ts>
               <ts e="T1280" id="Seg_1413" n="e" s="T1279">a </ts>
               <ts e="T1281" id="Seg_1415" n="e" s="T1280">măn </ts>
               <ts e="T1282" id="Seg_1417" n="e" s="T1281">nükem </ts>
               <ts e="T1283" id="Seg_1419" n="e" s="T1282">kudajən </ts>
               <ts e="T1284" id="Seg_1421" n="e" s="T1283">ijat. </ts>
               <ts e="T1285" id="Seg_1423" n="e" s="T1284">Kanaʔ! </ts>
               <ts e="T1286" id="Seg_1425" n="e" s="T1285">Tăn </ts>
               <ts e="T1287" id="Seg_1427" n="e" s="T1286">buga </ts>
               <ts e="T1288" id="Seg_1429" n="e" s="T1287">moləj, </ts>
               <ts e="T1289" id="Seg_1431" n="e" s="T1288">a </ts>
               <ts e="T1290" id="Seg_1433" n="e" s="T1289">tăn </ts>
               <ts e="T1291" id="Seg_1435" n="e" s="T1290">nüke </ts>
               <ts e="T1292" id="Seg_1437" n="e" s="T1291">šoška </ts>
               <ts e="T1293" id="Seg_1439" n="e" s="T1292">moləj. </ts>
               <ts e="T1294" id="Seg_1441" n="e" s="T1293">Dĭgəttə </ts>
               <ts e="T1295" id="Seg_1443" n="e" s="T1294">šobi. </ts>
               <ts e="T1296" id="Seg_1445" n="e" s="T1295">Dĭ </ts>
               <ts e="T1297" id="Seg_1447" n="e" s="T1296">büzʼe </ts>
               <ts e="T1298" id="Seg_1449" n="e" s="T1297">buga, </ts>
               <ts e="T1299" id="Seg_1451" n="e" s="T1298">a </ts>
               <ts e="T1300" id="Seg_1453" n="e" s="T1299">nüket </ts>
               <ts e="T1301" id="Seg_1455" n="e" s="T1300">šoška. </ts>
               <ts e="T1302" id="Seg_1457" n="e" s="T1301">I </ts>
               <ts e="T1303" id="Seg_1459" n="e" s="T1302">dărəʔ </ts>
               <ts e="T1304" id="Seg_1461" n="e" s="T1303">maːlaʔbəʔi. </ts>
               <ts e="T1305" id="Seg_1463" n="e" s="T1304">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T1060" id="Seg_1464" s="T1057">PKZ_196X_GreedyWife_flk.001 (001)</ta>
            <ta e="T1065" id="Seg_1465" s="T1060">PKZ_196X_GreedyWife_flk.002 (002)</ta>
            <ta e="T1067" id="Seg_1466" s="T1065">PKZ_196X_GreedyWife_flk.003 (003)</ta>
            <ta e="T1071" id="Seg_1467" s="T1067">PKZ_196X_GreedyWife_flk.004 (004)</ta>
            <ta e="T1074" id="Seg_1468" s="T1071">PKZ_196X_GreedyWife_flk.005 (005)</ta>
            <ta e="T1076" id="Seg_1469" s="T1074">PKZ_196X_GreedyWife_flk.006 (006)</ta>
            <ta e="T1082" id="Seg_1470" s="T1076">PKZ_196X_GreedyWife_flk.007 (007)</ta>
            <ta e="T1087" id="Seg_1471" s="T1082">PKZ_196X_GreedyWife_flk.008 (008)</ta>
            <ta e="T1091" id="Seg_1472" s="T1087">PKZ_196X_GreedyWife_flk.009 (009)</ta>
            <ta e="T1097" id="Seg_1473" s="T1091">PKZ_196X_GreedyWife_flk.010 (010)</ta>
            <ta e="T1100" id="Seg_1474" s="T1097">PKZ_196X_GreedyWife_flk.011 (011)</ta>
            <ta e="T1102" id="Seg_1475" s="T1100">PKZ_196X_GreedyWife_flk.012 (012)</ta>
            <ta e="T1105" id="Seg_1476" s="T1102">PKZ_196X_GreedyWife_flk.013 (013)</ta>
            <ta e="T1113" id="Seg_1477" s="T1105">PKZ_196X_GreedyWife_flk.014 (014)</ta>
            <ta e="T1117" id="Seg_1478" s="T1113">PKZ_196X_GreedyWife_flk.015 (015)</ta>
            <ta e="T1120" id="Seg_1479" s="T1117">PKZ_196X_GreedyWife_flk.016 (016)</ta>
            <ta e="T1122" id="Seg_1480" s="T1120">PKZ_196X_GreedyWife_flk.017 (017)</ta>
            <ta e="T1125" id="Seg_1481" s="T1122">PKZ_196X_GreedyWife_flk.018 (018)</ta>
            <ta e="T1129" id="Seg_1482" s="T1125">PKZ_196X_GreedyWife_flk.019 (019)</ta>
            <ta e="T1133" id="Seg_1483" s="T1129">PKZ_196X_GreedyWife_flk.020 (020)</ta>
            <ta e="T1135" id="Seg_1484" s="T1133">PKZ_196X_GreedyWife_flk.021 (021)</ta>
            <ta e="T1137" id="Seg_1485" s="T1135">PKZ_196X_GreedyWife_flk.022 (022)</ta>
            <ta e="T1140" id="Seg_1486" s="T1137">PKZ_196X_GreedyWife_flk.023 (023)</ta>
            <ta e="T1144" id="Seg_1487" s="T1140">PKZ_196X_GreedyWife_flk.024 (024)</ta>
            <ta e="T1146" id="Seg_1488" s="T1144">PKZ_196X_GreedyWife_flk.025 (025)</ta>
            <ta e="T1150" id="Seg_1489" s="T1146">PKZ_196X_GreedyWife_flk.026 (026)</ta>
            <ta e="T1154" id="Seg_1490" s="T1150">PKZ_196X_GreedyWife_flk.027 (027)</ta>
            <ta e="T1165" id="Seg_1491" s="T1154">PKZ_196X_GreedyWife_flk.028 (028)</ta>
            <ta e="T1170" id="Seg_1492" s="T1165">PKZ_196X_GreedyWife_flk.029 (029)</ta>
            <ta e="T1172" id="Seg_1493" s="T1170">PKZ_196X_GreedyWife_flk.030 (030)</ta>
            <ta e="T1176" id="Seg_1494" s="T1172">PKZ_196X_GreedyWife_flk.031 (031)</ta>
            <ta e="T1189" id="Seg_1495" s="T1176">PKZ_196X_GreedyWife_flk.032 (032)</ta>
            <ta e="T1190" id="Seg_1496" s="T1189">PKZ_196X_GreedyWife_flk.033 (033)</ta>
            <ta e="T1192" id="Seg_1497" s="T1190">PKZ_196X_GreedyWife_flk.034 (034)</ta>
            <ta e="T1194" id="Seg_1498" s="T1192">PKZ_196X_GreedyWife_flk.035 (035)</ta>
            <ta e="T1207" id="Seg_1499" s="T1194">PKZ_196X_GreedyWife_flk.036 (036)</ta>
            <ta e="T1208" id="Seg_1500" s="T1207">PKZ_196X_GreedyWife_flk.037 (038)</ta>
            <ta e="T1214" id="Seg_1501" s="T1208">PKZ_196X_GreedyWife_flk.038 (039)</ta>
            <ta e="T1216" id="Seg_1502" s="T1214">PKZ_196X_GreedyWife_flk.039 (040)</ta>
            <ta e="T1220" id="Seg_1503" s="T1216">PKZ_196X_GreedyWife_flk.040 (041)</ta>
            <ta e="T1229" id="Seg_1504" s="T1220">PKZ_196X_GreedyWife_flk.041 (042)</ta>
            <ta e="T1230" id="Seg_1505" s="T1229">PKZ_196X_GreedyWife_flk.042 (043)</ta>
            <ta e="T1240" id="Seg_1506" s="T1230">PKZ_196X_GreedyWife_flk.043 (044)</ta>
            <ta e="T1244" id="Seg_1507" s="T1240">PKZ_196X_GreedyWife_flk.044 (045)</ta>
            <ta e="T1250" id="Seg_1508" s="T1244">PKZ_196X_GreedyWife_flk.045 (046)</ta>
            <ta e="T1251" id="Seg_1509" s="T1250">PKZ_196X_GreedyWife_flk.046 (047)</ta>
            <ta e="T1259" id="Seg_1510" s="T1251">PKZ_196X_GreedyWife_flk.047 (048)</ta>
            <ta e="T1267" id="Seg_1511" s="T1259">PKZ_196X_GreedyWife_flk.048 (049)</ta>
            <ta e="T1271" id="Seg_1512" s="T1267">PKZ_196X_GreedyWife_flk.049 (050)</ta>
            <ta e="T1274" id="Seg_1513" s="T1271">PKZ_196X_GreedyWife_flk.050 (051)</ta>
            <ta e="T1284" id="Seg_1514" s="T1274">PKZ_196X_GreedyWife_flk.051 (052)</ta>
            <ta e="T1285" id="Seg_1515" s="T1284">PKZ_196X_GreedyWife_flk.052 (053)</ta>
            <ta e="T1293" id="Seg_1516" s="T1285">PKZ_196X_GreedyWife_flk.053 (054)</ta>
            <ta e="T1295" id="Seg_1517" s="T1293">PKZ_196X_GreedyWife_flk.054 (055)</ta>
            <ta e="T1301" id="Seg_1518" s="T1295">PKZ_196X_GreedyWife_flk.055 (056)</ta>
            <ta e="T1304" id="Seg_1519" s="T1301">PKZ_196X_GreedyWife_flk.056 (057)</ta>
            <ta e="T1305" id="Seg_1520" s="T1304">PKZ_196X_GreedyWife_flk.057 (058)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T1060" id="Seg_1521" s="T1057">Amnobiʔi nüke, büzʼe. </ta>
            <ta e="T1065" id="Seg_1522" s="T1060">(Dĭn=) Dĭzeŋgən ĭmbidə ej nagobi. </ta>
            <ta e="T1067" id="Seg_1523" s="T1065">Ipek naga. </ta>
            <ta e="T1071" id="Seg_1524" s="T1067">Paʔi naga, lučinaʔi naga. </ta>
            <ta e="T1074" id="Seg_1525" s="T1071">"Kanaʔ dʼijenə pajlaʔ". </ta>
            <ta e="T1076" id="Seg_1526" s="T1074">Dĭ kambi. </ta>
            <ta e="T1082" id="Seg_1527" s="T1076">Dĭn davaj onʼiʔ pa baltuziʔ jaʔsittə. </ta>
            <ta e="T1087" id="Seg_1528" s="T1082">Dĭgəttə (sü-) süjö nʼergöleʔ šobi. </ta>
            <ta e="T1091" id="Seg_1529" s="T1087">"Ĭmbi tănan kereʔ, büzʼe?" </ta>
            <ta e="T1097" id="Seg_1530" s="T1091">Dĭ măndə: "Măna (paʔi=) paʔi kereʔ. </ta>
            <ta e="T1100" id="Seg_1531" s="T1097">A to naga paʔi". </ta>
            <ta e="T1102" id="Seg_1532" s="T1100">"Kanaʔ maːndə! </ta>
            <ta e="T1105" id="Seg_1533" s="T1102">Dĭn paʔi iʔgö!" </ta>
            <ta e="T1113" id="Seg_1534" s="T1105">Dĭ šobi maːndə, i (pu-) ugaːndə paʔi iʔgö. </ta>
            <ta e="T1117" id="Seg_1535" s="T1113">Dĭgəttə nüket măndə:" Kanaʔ! </ta>
            <ta e="T1120" id="Seg_1536" s="T1117">Pušaj mĭləj ipek!" </ta>
            <ta e="T1122" id="Seg_1537" s="T1120">Dĭgəttə šobi. </ta>
            <ta e="T1125" id="Seg_1538" s="T1122">Bazoʔ baltuziʔ jaʔpi. </ta>
            <ta e="T1129" id="Seg_1539" s="T1125">Bazoʔ (šüjö=) süjö nʼergölaʔpi. </ta>
            <ta e="T1133" id="Seg_1540" s="T1129">"Ĭmbi tănan kereʔ, büzʼe?" </ta>
            <ta e="T1135" id="Seg_1541" s="T1133">"Ipek naga". </ta>
            <ta e="T1137" id="Seg_1542" s="T1135">"Kanaʔ maːndə!" </ta>
            <ta e="T1140" id="Seg_1543" s="T1137">Dĭ šobi bar. </ta>
            <ta e="T1144" id="Seg_1544" s="T1140">Tüžöjəʔi, ularəʔi bar iʔgö. </ta>
            <ta e="T1146" id="Seg_1545" s="T1144">Albəj šeden. </ta>
            <ta e="T1150" id="Seg_1546" s="T1146">I amzittə iʔgö ige. </ta>
            <ta e="T1154" id="Seg_1547" s="T1150">Nüke ambi, ambi:" Kanaʔ. </ta>
            <ta e="T1165" id="Seg_1548" s="T1154">Nörbəʔ, štobɨ (tăn bɨl-) tăn ibiel koŋ, a măn koŋgən ne". </ta>
            <ta e="T1170" id="Seg_1549" s="T1165">Dĭ kambi, bazoʔ baltuzʼiʔ jaʔpi. </ta>
            <ta e="T1172" id="Seg_1550" s="T1170">Süjö nʼergölaʔpi. </ta>
            <ta e="T1176" id="Seg_1551" s="T1172">"Ĭmbi (tăna-) tănan kereʔ?" </ta>
            <ta e="T1189" id="Seg_1552" s="T1176">"Da măn (nük-) măn koŋ ilem, a măn nükem tože măn koŋ moləj". </ta>
            <ta e="T1190" id="Seg_1553" s="T1189">"Kanaʔ!" </ta>
            <ta e="T1192" id="Seg_1554" s="T1190">Dĭgəttə šobi. </ta>
            <ta e="T1194" id="Seg_1555" s="T1192">Amnobiʔi, amnobiʔi. </ta>
            <ta e="T1207" id="Seg_1556" s="T1194">Nükendə (a bazoʔ): "Kanaʔ, nörbəʔ, štobɨ tăn ibiel bɨ tsar, a măn tsaritsazʼiʔ". </ta>
            <ta e="T1208" id="Seg_1557" s="T1207">Kambi. </ta>
            <ta e="T1214" id="Seg_1558" s="T1208">(Süjö bazoʔ=) Davaj baltuzʼiʔ jaʔsittə pam. </ta>
            <ta e="T1216" id="Seg_1559" s="T1214">Süjö nʼergölüʔpi. </ta>
            <ta e="T1220" id="Seg_1560" s="T1216">"Ĭmbi tănan kereʔ, büzʼe?" </ta>
            <ta e="T1229" id="Seg_1561" s="T1220">"Da măn štobɨ ibiem tsar, a măn nükem tsaritsa". </ta>
            <ta e="T1230" id="Seg_1562" s="T1229">"Kanaʔ!" </ta>
            <ta e="T1240" id="Seg_1563" s="T1230">Dĭ (šo-) šonəga, il bar üžüʔi, ((PAUSE)) bar saʔməlaʔbəjəʔ dĭʔnə. </ta>
            <ta e="T1244" id="Seg_1564" s="T1240">Dĭgəttə šobi, amnobiʔi, amnobiʔi. </ta>
            <ta e="T1250" id="Seg_1565" s="T1244">Dĭgəttə nüke bazoʔ sürerleʔbə dĭ büzʼem. </ta>
            <ta e="T1251" id="Seg_1566" s="T1250">"Kanaʔ! </ta>
            <ta e="T1259" id="Seg_1567" s="T1251">Štobɨ tăn kudaj ibiel, a măn kudajən ijat. </ta>
            <ta e="T1267" id="Seg_1568" s="T1259">Dĭ kambi, davaj bazoʔ baltuzʼiʔ jaʔsittə dĭ pam. </ta>
            <ta e="T1271" id="Seg_1569" s="T1267">Dĭ süjö nʼergöleʔ šobi,. </ta>
            <ta e="T1274" id="Seg_1570" s="T1271">"Ĭmbi tănan kereʔ?" </ta>
            <ta e="T1284" id="Seg_1571" s="T1274">"Da măn (kudaj=) kudaj molam, a măn nükem kudajən ijat". </ta>
            <ta e="T1285" id="Seg_1572" s="T1284">"Kanaʔ! </ta>
            <ta e="T1293" id="Seg_1573" s="T1285">Tăn buga moləj, a tăn nüke šoška moləj". </ta>
            <ta e="T1295" id="Seg_1574" s="T1293">Dĭgəttə šobi. </ta>
            <ta e="T1301" id="Seg_1575" s="T1295">Dĭ büzʼe buga, a nüket šoška. </ta>
            <ta e="T1304" id="Seg_1576" s="T1301">I dărəʔ maːlaʔbəʔi. </ta>
            <ta e="T1305" id="Seg_1577" s="T1304">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1058" id="Seg_1578" s="T1057">amno-bi-ʔi</ta>
            <ta e="T1059" id="Seg_1579" s="T1058">nüke</ta>
            <ta e="T1060" id="Seg_1580" s="T1059">büzʼe</ta>
            <ta e="T1061" id="Seg_1581" s="T1060">dĭn</ta>
            <ta e="T1062" id="Seg_1582" s="T1061">dĭ-zeŋ-gən</ta>
            <ta e="T1063" id="Seg_1583" s="T1062">ĭmbi=də</ta>
            <ta e="T1064" id="Seg_1584" s="T1063">ej</ta>
            <ta e="T1065" id="Seg_1585" s="T1064">nago-bi</ta>
            <ta e="T1066" id="Seg_1586" s="T1065">ipek</ta>
            <ta e="T1067" id="Seg_1587" s="T1066">naga</ta>
            <ta e="T1068" id="Seg_1588" s="T1067">pa-ʔi</ta>
            <ta e="T1069" id="Seg_1589" s="T1068">naga</ta>
            <ta e="T1070" id="Seg_1590" s="T1069">lučina-ʔi</ta>
            <ta e="T1071" id="Seg_1591" s="T1070">naga</ta>
            <ta e="T1072" id="Seg_1592" s="T1071">kan-a-ʔ</ta>
            <ta e="T1073" id="Seg_1593" s="T1072">dʼije-nə</ta>
            <ta e="T1074" id="Seg_1594" s="T1073">pa-j-laʔ</ta>
            <ta e="T1075" id="Seg_1595" s="T1074">dĭ</ta>
            <ta e="T1076" id="Seg_1596" s="T1075">kam-bi</ta>
            <ta e="T1077" id="Seg_1597" s="T1076">dĭn</ta>
            <ta e="T1078" id="Seg_1598" s="T1077">davaj</ta>
            <ta e="T1079" id="Seg_1599" s="T1078">onʼiʔ</ta>
            <ta e="T1080" id="Seg_1600" s="T1079">pa</ta>
            <ta e="T1081" id="Seg_1601" s="T1080">baltu-ziʔ</ta>
            <ta e="T1082" id="Seg_1602" s="T1081">jaʔ-sittə</ta>
            <ta e="T1083" id="Seg_1603" s="T1082">dĭgəttə</ta>
            <ta e="T1085" id="Seg_1604" s="T1084">süjö</ta>
            <ta e="T1086" id="Seg_1605" s="T1085">nʼergö-leʔ</ta>
            <ta e="T1087" id="Seg_1606" s="T1086">šo-bi</ta>
            <ta e="T1088" id="Seg_1607" s="T1087">ĭmbi</ta>
            <ta e="T1089" id="Seg_1608" s="T1088">tănan</ta>
            <ta e="T1090" id="Seg_1609" s="T1089">kereʔ</ta>
            <ta e="T1091" id="Seg_1610" s="T1090">büzʼe</ta>
            <ta e="T1092" id="Seg_1611" s="T1091">dĭ</ta>
            <ta e="T1093" id="Seg_1612" s="T1092">măn-də</ta>
            <ta e="T1094" id="Seg_1613" s="T1093">măna</ta>
            <ta e="T1095" id="Seg_1614" s="T1094">pa-ʔi</ta>
            <ta e="T1096" id="Seg_1615" s="T1095">pa-ʔi</ta>
            <ta e="T1097" id="Seg_1616" s="T1096">kereʔ</ta>
            <ta e="T1098" id="Seg_1617" s="T1097">ato</ta>
            <ta e="T1099" id="Seg_1618" s="T1098">naga</ta>
            <ta e="T1100" id="Seg_1619" s="T1099">pa-ʔi</ta>
            <ta e="T1101" id="Seg_1620" s="T1100">kan-a-ʔ</ta>
            <ta e="T1102" id="Seg_1621" s="T1101">ma-ndə</ta>
            <ta e="T1103" id="Seg_1622" s="T1102">dĭn</ta>
            <ta e="T1104" id="Seg_1623" s="T1103">pa-ʔi</ta>
            <ta e="T1105" id="Seg_1624" s="T1104">iʔgö</ta>
            <ta e="T1106" id="Seg_1625" s="T1105">dĭ</ta>
            <ta e="T1107" id="Seg_1626" s="T1106">šo-bi</ta>
            <ta e="T1108" id="Seg_1627" s="T1107">ma-ndə</ta>
            <ta e="T1109" id="Seg_1628" s="T1108">i</ta>
            <ta e="T1111" id="Seg_1629" s="T1110">ugaːndə</ta>
            <ta e="T1112" id="Seg_1630" s="T1111">pa-ʔi</ta>
            <ta e="T1113" id="Seg_1631" s="T1112">iʔgö</ta>
            <ta e="T1114" id="Seg_1632" s="T1113">dĭgəttə</ta>
            <ta e="T1115" id="Seg_1633" s="T1114">nüke-t</ta>
            <ta e="T1116" id="Seg_1634" s="T1115">măn-də</ta>
            <ta e="T1117" id="Seg_1635" s="T1116">kan-a-ʔ</ta>
            <ta e="T1118" id="Seg_1636" s="T1117">pušaj</ta>
            <ta e="T1119" id="Seg_1637" s="T1118">mĭ-lə-j</ta>
            <ta e="T1120" id="Seg_1638" s="T1119">ipek</ta>
            <ta e="T1121" id="Seg_1639" s="T1120">dĭgəttə</ta>
            <ta e="T1122" id="Seg_1640" s="T1121">šo-bi</ta>
            <ta e="T1123" id="Seg_1641" s="T1122">bazoʔ</ta>
            <ta e="T1124" id="Seg_1642" s="T1123">baltu-ziʔ</ta>
            <ta e="T1125" id="Seg_1643" s="T1124">jaʔ-pi</ta>
            <ta e="T1126" id="Seg_1644" s="T1125">bazoʔ</ta>
            <ta e="T1127" id="Seg_1645" s="T1126">šüjö</ta>
            <ta e="T1128" id="Seg_1646" s="T1127">süjö</ta>
            <ta e="T1129" id="Seg_1647" s="T1128">nʼergö-laʔ-pi</ta>
            <ta e="T1130" id="Seg_1648" s="T1129">ĭmbi</ta>
            <ta e="T1131" id="Seg_1649" s="T1130">tănan</ta>
            <ta e="T1132" id="Seg_1650" s="T1131">kereʔ</ta>
            <ta e="T1133" id="Seg_1651" s="T1132">büzʼe</ta>
            <ta e="T1134" id="Seg_1652" s="T1133">ipek</ta>
            <ta e="T1135" id="Seg_1653" s="T1134">naga</ta>
            <ta e="T1136" id="Seg_1654" s="T1135">kan-a-ʔ</ta>
            <ta e="T1137" id="Seg_1655" s="T1136">ma-ndə</ta>
            <ta e="T1138" id="Seg_1656" s="T1137">dĭ</ta>
            <ta e="T1139" id="Seg_1657" s="T1138">šo-bi</ta>
            <ta e="T1140" id="Seg_1658" s="T1139">bar</ta>
            <ta e="T1141" id="Seg_1659" s="T1140">tüžöj-əʔi</ta>
            <ta e="T1142" id="Seg_1660" s="T1141">ular-əʔi</ta>
            <ta e="T1143" id="Seg_1661" s="T1142">bar</ta>
            <ta e="T1144" id="Seg_1662" s="T1143">iʔgö</ta>
            <ta e="T1145" id="Seg_1663" s="T1144">albə-j</ta>
            <ta e="T1146" id="Seg_1664" s="T1145">šeden</ta>
            <ta e="T1147" id="Seg_1665" s="T1146">i</ta>
            <ta e="T1148" id="Seg_1666" s="T1147">am-zittə</ta>
            <ta e="T1149" id="Seg_1667" s="T1148">iʔgö</ta>
            <ta e="T1150" id="Seg_1668" s="T1149">i-ge</ta>
            <ta e="T1151" id="Seg_1669" s="T1150">nüke</ta>
            <ta e="T1152" id="Seg_1670" s="T1151">am-bi</ta>
            <ta e="T1153" id="Seg_1671" s="T1152">am-bi</ta>
            <ta e="T1154" id="Seg_1672" s="T1153">kan-a-ʔ</ta>
            <ta e="T1155" id="Seg_1673" s="T1154">nörbə-ʔ</ta>
            <ta e="T1156" id="Seg_1674" s="T1155">štobɨ</ta>
            <ta e="T1157" id="Seg_1675" s="T1156">tăn</ta>
            <ta e="T1159" id="Seg_1676" s="T1158">tăn</ta>
            <ta e="T1160" id="Seg_1677" s="T1159">i-bie-l</ta>
            <ta e="T1161" id="Seg_1678" s="T1160">koŋ</ta>
            <ta e="T1162" id="Seg_1679" s="T1161">a</ta>
            <ta e="T1163" id="Seg_1680" s="T1162">măn</ta>
            <ta e="T1164" id="Seg_1681" s="T1163">koŋ-gən</ta>
            <ta e="T1165" id="Seg_1682" s="T1164">ne</ta>
            <ta e="T1166" id="Seg_1683" s="T1165">dĭ</ta>
            <ta e="T1167" id="Seg_1684" s="T1166">kam-bi</ta>
            <ta e="T1168" id="Seg_1685" s="T1167">bazoʔ</ta>
            <ta e="T1169" id="Seg_1686" s="T1168">baltu-zʼiʔ</ta>
            <ta e="T1170" id="Seg_1687" s="T1169">jaʔ-pi</ta>
            <ta e="T1171" id="Seg_1688" s="T1170">süjö</ta>
            <ta e="T1172" id="Seg_1689" s="T1171">nʼergö-laʔ-pi</ta>
            <ta e="T1173" id="Seg_1690" s="T1172">ĭmbi</ta>
            <ta e="T1175" id="Seg_1691" s="T1174">tănan</ta>
            <ta e="T1176" id="Seg_1692" s="T1175">kereʔ</ta>
            <ta e="T1177" id="Seg_1693" s="T1176">da</ta>
            <ta e="T1178" id="Seg_1694" s="T1177">măn</ta>
            <ta e="T1180" id="Seg_1695" s="T1179">măn</ta>
            <ta e="T1181" id="Seg_1696" s="T1180">koŋ</ta>
            <ta e="T1182" id="Seg_1697" s="T1181">i-le-m</ta>
            <ta e="T1183" id="Seg_1698" s="T1182">a</ta>
            <ta e="T1184" id="Seg_1699" s="T1183">măn</ta>
            <ta e="T1185" id="Seg_1700" s="T1184">nüke-m</ta>
            <ta e="T1186" id="Seg_1701" s="T1185">tože</ta>
            <ta e="T1187" id="Seg_1702" s="T1186">măn</ta>
            <ta e="T1188" id="Seg_1703" s="T1187">koŋ</ta>
            <ta e="T1189" id="Seg_1704" s="T1188">mo-lə-j</ta>
            <ta e="T1190" id="Seg_1705" s="T1189">kan-a-ʔ</ta>
            <ta e="T1191" id="Seg_1706" s="T1190">dĭgəttə</ta>
            <ta e="T1192" id="Seg_1707" s="T1191">šo-bi</ta>
            <ta e="T1193" id="Seg_1708" s="T1192">amno-bi-ʔi</ta>
            <ta e="T1194" id="Seg_1709" s="T1193">amno-bi-ʔi</ta>
            <ta e="T1195" id="Seg_1710" s="T1194">nüke-ndə</ta>
            <ta e="T1196" id="Seg_1711" s="T1195">a</ta>
            <ta e="T1197" id="Seg_1712" s="T1196">bazoʔ</ta>
            <ta e="T1198" id="Seg_1713" s="T1197">kan-a-ʔ</ta>
            <ta e="T1199" id="Seg_1714" s="T1198">nörbə-ʔ</ta>
            <ta e="T1200" id="Seg_1715" s="T1199">štobɨ</ta>
            <ta e="T1201" id="Seg_1716" s="T1200">tăn</ta>
            <ta e="T1202" id="Seg_1717" s="T1201">i-bie-l</ta>
            <ta e="T1203" id="Seg_1718" s="T1202">bɨ</ta>
            <ta e="T1204" id="Seg_1719" s="T1203">tsar</ta>
            <ta e="T1205" id="Seg_1720" s="T1204">a</ta>
            <ta e="T1206" id="Seg_1721" s="T1205">măn</ta>
            <ta e="T1207" id="Seg_1722" s="T1206">tsaritsa-zʼiʔ</ta>
            <ta e="T1208" id="Seg_1723" s="T1207">kam-bi</ta>
            <ta e="T1209" id="Seg_1724" s="T1208">süjö</ta>
            <ta e="T1210" id="Seg_1725" s="T1209">bazoʔ</ta>
            <ta e="T1211" id="Seg_1726" s="T1210">davaj</ta>
            <ta e="T1212" id="Seg_1727" s="T1211">baltu-zʼiʔ</ta>
            <ta e="T1213" id="Seg_1728" s="T1212">jaʔ-sittə</ta>
            <ta e="T1214" id="Seg_1729" s="T1213">pa-m</ta>
            <ta e="T1215" id="Seg_1730" s="T1214">süjö</ta>
            <ta e="T1216" id="Seg_1731" s="T1215">nʼergö-lüʔ-pi</ta>
            <ta e="T1217" id="Seg_1732" s="T1216">ĭmbi</ta>
            <ta e="T1218" id="Seg_1733" s="T1217">tănan</ta>
            <ta e="T1219" id="Seg_1734" s="T1218">kereʔ</ta>
            <ta e="T1220" id="Seg_1735" s="T1219">büzʼe</ta>
            <ta e="T1221" id="Seg_1736" s="T1220">da</ta>
            <ta e="T1222" id="Seg_1737" s="T1221">măn</ta>
            <ta e="T1223" id="Seg_1738" s="T1222">štobɨ</ta>
            <ta e="T1224" id="Seg_1739" s="T1223">i-bie-m</ta>
            <ta e="T1225" id="Seg_1740" s="T1224">tsar</ta>
            <ta e="T1226" id="Seg_1741" s="T1225">a</ta>
            <ta e="T1227" id="Seg_1742" s="T1226">măn</ta>
            <ta e="T1228" id="Seg_1743" s="T1227">nüke-m</ta>
            <ta e="T1229" id="Seg_1744" s="T1228">tsaritsa</ta>
            <ta e="T1230" id="Seg_1745" s="T1229">kan-a-ʔ</ta>
            <ta e="T1231" id="Seg_1746" s="T1230">dĭ</ta>
            <ta e="T1233" id="Seg_1747" s="T1232">šonə-ga</ta>
            <ta e="T1234" id="Seg_1748" s="T1233">il</ta>
            <ta e="T1235" id="Seg_1749" s="T1234">bar</ta>
            <ta e="T1237" id="Seg_1750" s="T1235">üžü-ʔi</ta>
            <ta e="T1238" id="Seg_1751" s="T1237">bar</ta>
            <ta e="T1239" id="Seg_1752" s="T1238">saʔmə-laʔbə-jəʔ</ta>
            <ta e="T1240" id="Seg_1753" s="T1239">dĭʔ-nə</ta>
            <ta e="T1241" id="Seg_1754" s="T1240">dĭgəttə</ta>
            <ta e="T1242" id="Seg_1755" s="T1241">šo-bi</ta>
            <ta e="T1243" id="Seg_1756" s="T1242">amno-bi-ʔi</ta>
            <ta e="T1244" id="Seg_1757" s="T1243">amno-bi-ʔi</ta>
            <ta e="T1245" id="Seg_1758" s="T1244">dĭgəttə</ta>
            <ta e="T1246" id="Seg_1759" s="T1245">nüke</ta>
            <ta e="T1247" id="Seg_1760" s="T1246">bazoʔ</ta>
            <ta e="T1248" id="Seg_1761" s="T1247">sürer-leʔbə</ta>
            <ta e="T1249" id="Seg_1762" s="T1248">dĭ</ta>
            <ta e="T1250" id="Seg_1763" s="T1249">büzʼe-m</ta>
            <ta e="T1251" id="Seg_1764" s="T1250">kan-a-ʔ</ta>
            <ta e="T1252" id="Seg_1765" s="T1251">štobɨ</ta>
            <ta e="T1253" id="Seg_1766" s="T1252">tăn</ta>
            <ta e="T1254" id="Seg_1767" s="T1253">kudaj</ta>
            <ta e="T1255" id="Seg_1768" s="T1254">i-bie-l</ta>
            <ta e="T1256" id="Seg_1769" s="T1255">a</ta>
            <ta e="T1257" id="Seg_1770" s="T1256">măn</ta>
            <ta e="T1258" id="Seg_1771" s="T1257">kudaj-ə-n</ta>
            <ta e="T1259" id="Seg_1772" s="T1258">ija-t</ta>
            <ta e="T1260" id="Seg_1773" s="T1259">dĭ</ta>
            <ta e="T1261" id="Seg_1774" s="T1260">kam-bi</ta>
            <ta e="T1262" id="Seg_1775" s="T1261">davaj</ta>
            <ta e="T1263" id="Seg_1776" s="T1262">bazoʔ</ta>
            <ta e="T1264" id="Seg_1777" s="T1263">baltu-zʼiʔ</ta>
            <ta e="T1265" id="Seg_1778" s="T1264">jaʔ-sittə</ta>
            <ta e="T1266" id="Seg_1779" s="T1265">dĭ</ta>
            <ta e="T1267" id="Seg_1780" s="T1266">pa-m</ta>
            <ta e="T1268" id="Seg_1781" s="T1267">dĭ</ta>
            <ta e="T1269" id="Seg_1782" s="T1268">süjö</ta>
            <ta e="T1270" id="Seg_1783" s="T1269">nʼergö-leʔ</ta>
            <ta e="T1271" id="Seg_1784" s="T1270">šo-bi</ta>
            <ta e="T1272" id="Seg_1785" s="T1271">ĭmbi</ta>
            <ta e="T1273" id="Seg_1786" s="T1272">tănan</ta>
            <ta e="T1274" id="Seg_1787" s="T1273">kereʔ</ta>
            <ta e="T1275" id="Seg_1788" s="T1274">da</ta>
            <ta e="T1276" id="Seg_1789" s="T1275">măn</ta>
            <ta e="T1277" id="Seg_1790" s="T1276">kudaj</ta>
            <ta e="T1278" id="Seg_1791" s="T1277">kudaj</ta>
            <ta e="T1279" id="Seg_1792" s="T1278">mo-la-m</ta>
            <ta e="T1280" id="Seg_1793" s="T1279">a</ta>
            <ta e="T1281" id="Seg_1794" s="T1280">măn</ta>
            <ta e="T1282" id="Seg_1795" s="T1281">nüke-m</ta>
            <ta e="T1283" id="Seg_1796" s="T1282">kudaj-ə-n</ta>
            <ta e="T1284" id="Seg_1797" s="T1283">ija-t</ta>
            <ta e="T1285" id="Seg_1798" s="T1284">kan-a-ʔ</ta>
            <ta e="T1286" id="Seg_1799" s="T1285">tăn</ta>
            <ta e="T1287" id="Seg_1800" s="T1286">buga</ta>
            <ta e="T1288" id="Seg_1801" s="T1287">mo-lə-j</ta>
            <ta e="T1289" id="Seg_1802" s="T1288">a</ta>
            <ta e="T1290" id="Seg_1803" s="T1289">tăn</ta>
            <ta e="T1291" id="Seg_1804" s="T1290">nüke</ta>
            <ta e="T1292" id="Seg_1805" s="T1291">šoška</ta>
            <ta e="T1293" id="Seg_1806" s="T1292">mo-lə-j</ta>
            <ta e="T1294" id="Seg_1807" s="T1293">dĭgəttə</ta>
            <ta e="T1295" id="Seg_1808" s="T1294">šo-bi</ta>
            <ta e="T1296" id="Seg_1809" s="T1295">dĭ</ta>
            <ta e="T1297" id="Seg_1810" s="T1296">büzʼe</ta>
            <ta e="T1298" id="Seg_1811" s="T1297">buga</ta>
            <ta e="T1299" id="Seg_1812" s="T1298">a</ta>
            <ta e="T1300" id="Seg_1813" s="T1299">nüke-t</ta>
            <ta e="T1301" id="Seg_1814" s="T1300">šoška</ta>
            <ta e="T1302" id="Seg_1815" s="T1301">i</ta>
            <ta e="T1303" id="Seg_1816" s="T1302">dărəʔ</ta>
            <ta e="T1304" id="Seg_1817" s="T1303">ma-laʔbə-ʔi</ta>
            <ta e="T1305" id="Seg_1818" s="T1304">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1058" id="Seg_1819" s="T1057">amno-bi-jəʔ</ta>
            <ta e="T1059" id="Seg_1820" s="T1058">nüke</ta>
            <ta e="T1060" id="Seg_1821" s="T1059">büzʼe</ta>
            <ta e="T1061" id="Seg_1822" s="T1060">dĭn</ta>
            <ta e="T1062" id="Seg_1823" s="T1061">dĭ-zAŋ-Kən</ta>
            <ta e="T1063" id="Seg_1824" s="T1062">ĭmbi=də</ta>
            <ta e="T1064" id="Seg_1825" s="T1063">ej</ta>
            <ta e="T1065" id="Seg_1826" s="T1064">naga-bi</ta>
            <ta e="T1066" id="Seg_1827" s="T1065">ipek</ta>
            <ta e="T1067" id="Seg_1828" s="T1066">naga</ta>
            <ta e="T1068" id="Seg_1829" s="T1067">pa-jəʔ</ta>
            <ta e="T1069" id="Seg_1830" s="T1068">naga</ta>
            <ta e="T1070" id="Seg_1831" s="T1069">lučina-jəʔ</ta>
            <ta e="T1071" id="Seg_1832" s="T1070">naga</ta>
            <ta e="T1072" id="Seg_1833" s="T1071">kan-ə-ʔ</ta>
            <ta e="T1073" id="Seg_1834" s="T1072">dʼije-Tə</ta>
            <ta e="T1074" id="Seg_1835" s="T1073">pa-j-lAʔ</ta>
            <ta e="T1075" id="Seg_1836" s="T1074">dĭ</ta>
            <ta e="T1076" id="Seg_1837" s="T1075">kan-bi</ta>
            <ta e="T1077" id="Seg_1838" s="T1076">dĭn</ta>
            <ta e="T1078" id="Seg_1839" s="T1077">davaj</ta>
            <ta e="T1079" id="Seg_1840" s="T1078">onʼiʔ</ta>
            <ta e="T1080" id="Seg_1841" s="T1079">pa</ta>
            <ta e="T1081" id="Seg_1842" s="T1080">baltu-ziʔ</ta>
            <ta e="T1082" id="Seg_1843" s="T1081">hʼaʔ-zittə</ta>
            <ta e="T1083" id="Seg_1844" s="T1082">dĭgəttə</ta>
            <ta e="T1085" id="Seg_1845" s="T1084">süjö</ta>
            <ta e="T1086" id="Seg_1846" s="T1085">nʼergö-lAʔ</ta>
            <ta e="T1087" id="Seg_1847" s="T1086">šo-bi</ta>
            <ta e="T1088" id="Seg_1848" s="T1087">ĭmbi</ta>
            <ta e="T1089" id="Seg_1849" s="T1088">tănan</ta>
            <ta e="T1090" id="Seg_1850" s="T1089">kereʔ</ta>
            <ta e="T1091" id="Seg_1851" s="T1090">büzʼe</ta>
            <ta e="T1092" id="Seg_1852" s="T1091">dĭ</ta>
            <ta e="T1093" id="Seg_1853" s="T1092">măn-ntə</ta>
            <ta e="T1094" id="Seg_1854" s="T1093">măna</ta>
            <ta e="T1095" id="Seg_1855" s="T1094">pa-jəʔ</ta>
            <ta e="T1096" id="Seg_1856" s="T1095">pa-jəʔ</ta>
            <ta e="T1097" id="Seg_1857" s="T1096">kereʔ</ta>
            <ta e="T1098" id="Seg_1858" s="T1097">ato</ta>
            <ta e="T1099" id="Seg_1859" s="T1098">naga</ta>
            <ta e="T1100" id="Seg_1860" s="T1099">pa-jəʔ</ta>
            <ta e="T1101" id="Seg_1861" s="T1100">kan-ə-ʔ</ta>
            <ta e="T1102" id="Seg_1862" s="T1101">maʔ-gəndə</ta>
            <ta e="T1103" id="Seg_1863" s="T1102">dĭn</ta>
            <ta e="T1104" id="Seg_1864" s="T1103">pa-jəʔ</ta>
            <ta e="T1105" id="Seg_1865" s="T1104">iʔgö</ta>
            <ta e="T1106" id="Seg_1866" s="T1105">dĭ</ta>
            <ta e="T1107" id="Seg_1867" s="T1106">šo-bi</ta>
            <ta e="T1108" id="Seg_1868" s="T1107">maʔ-gəndə</ta>
            <ta e="T1109" id="Seg_1869" s="T1108">i</ta>
            <ta e="T1111" id="Seg_1870" s="T1110">ugaːndə</ta>
            <ta e="T1112" id="Seg_1871" s="T1111">pa-jəʔ</ta>
            <ta e="T1113" id="Seg_1872" s="T1112">iʔgö</ta>
            <ta e="T1114" id="Seg_1873" s="T1113">dĭgəttə</ta>
            <ta e="T1115" id="Seg_1874" s="T1114">nüke-t</ta>
            <ta e="T1116" id="Seg_1875" s="T1115">măn-ntə</ta>
            <ta e="T1117" id="Seg_1876" s="T1116">kan-ə-ʔ</ta>
            <ta e="T1118" id="Seg_1877" s="T1117">pušaj</ta>
            <ta e="T1119" id="Seg_1878" s="T1118">mĭ-lV-j</ta>
            <ta e="T1120" id="Seg_1879" s="T1119">ipek</ta>
            <ta e="T1121" id="Seg_1880" s="T1120">dĭgəttə</ta>
            <ta e="T1122" id="Seg_1881" s="T1121">šo-bi</ta>
            <ta e="T1123" id="Seg_1882" s="T1122">bazoʔ</ta>
            <ta e="T1124" id="Seg_1883" s="T1123">baltu-ziʔ</ta>
            <ta e="T1125" id="Seg_1884" s="T1124">hʼaʔ-bi</ta>
            <ta e="T1126" id="Seg_1885" s="T1125">bazoʔ</ta>
            <ta e="T1127" id="Seg_1886" s="T1126">šüjə</ta>
            <ta e="T1128" id="Seg_1887" s="T1127">süjö</ta>
            <ta e="T1129" id="Seg_1888" s="T1128">nʼergö-laʔbə-bi</ta>
            <ta e="T1130" id="Seg_1889" s="T1129">ĭmbi</ta>
            <ta e="T1131" id="Seg_1890" s="T1130">tănan</ta>
            <ta e="T1132" id="Seg_1891" s="T1131">kereʔ</ta>
            <ta e="T1133" id="Seg_1892" s="T1132">büzʼe</ta>
            <ta e="T1134" id="Seg_1893" s="T1133">ipek</ta>
            <ta e="T1135" id="Seg_1894" s="T1134">naga</ta>
            <ta e="T1136" id="Seg_1895" s="T1135">kan-ə-ʔ</ta>
            <ta e="T1137" id="Seg_1896" s="T1136">maʔ-gəndə</ta>
            <ta e="T1138" id="Seg_1897" s="T1137">dĭ</ta>
            <ta e="T1139" id="Seg_1898" s="T1138">šo-bi</ta>
            <ta e="T1140" id="Seg_1899" s="T1139">bar</ta>
            <ta e="T1141" id="Seg_1900" s="T1140">tüžöj-jəʔ</ta>
            <ta e="T1142" id="Seg_1901" s="T1141">ular-jəʔ</ta>
            <ta e="T1143" id="Seg_1902" s="T1142">bar</ta>
            <ta e="T1144" id="Seg_1903" s="T1143">iʔgö</ta>
            <ta e="T1145" id="Seg_1904" s="T1144">albə-j</ta>
            <ta e="T1146" id="Seg_1905" s="T1145">šeden</ta>
            <ta e="T1147" id="Seg_1906" s="T1146">i</ta>
            <ta e="T1148" id="Seg_1907" s="T1147">am-zittə</ta>
            <ta e="T1149" id="Seg_1908" s="T1148">iʔgö</ta>
            <ta e="T1150" id="Seg_1909" s="T1149">i-gA</ta>
            <ta e="T1151" id="Seg_1910" s="T1150">nüke</ta>
            <ta e="T1152" id="Seg_1911" s="T1151">am-bi</ta>
            <ta e="T1153" id="Seg_1912" s="T1152">am-bi</ta>
            <ta e="T1154" id="Seg_1913" s="T1153">kan-ə-ʔ</ta>
            <ta e="T1155" id="Seg_1914" s="T1154">nörbə-ʔ</ta>
            <ta e="T1156" id="Seg_1915" s="T1155">štobɨ</ta>
            <ta e="T1157" id="Seg_1916" s="T1156">tăn</ta>
            <ta e="T1159" id="Seg_1917" s="T1158">tăn</ta>
            <ta e="T1160" id="Seg_1918" s="T1159">i-bi-l</ta>
            <ta e="T1161" id="Seg_1919" s="T1160">koŋ</ta>
            <ta e="T1162" id="Seg_1920" s="T1161">a</ta>
            <ta e="T1163" id="Seg_1921" s="T1162">măn</ta>
            <ta e="T1164" id="Seg_1922" s="T1163">koŋ-Kən</ta>
            <ta e="T1165" id="Seg_1923" s="T1164">ne</ta>
            <ta e="T1166" id="Seg_1924" s="T1165">dĭ</ta>
            <ta e="T1167" id="Seg_1925" s="T1166">kan-bi</ta>
            <ta e="T1168" id="Seg_1926" s="T1167">bazoʔ</ta>
            <ta e="T1169" id="Seg_1927" s="T1168">baltu-ziʔ</ta>
            <ta e="T1170" id="Seg_1928" s="T1169">hʼaʔ-bi</ta>
            <ta e="T1171" id="Seg_1929" s="T1170">süjö</ta>
            <ta e="T1172" id="Seg_1930" s="T1171">nʼergö-laʔbə-bi</ta>
            <ta e="T1173" id="Seg_1931" s="T1172">ĭmbi</ta>
            <ta e="T1175" id="Seg_1932" s="T1174">tănan</ta>
            <ta e="T1176" id="Seg_1933" s="T1175">kereʔ</ta>
            <ta e="T1177" id="Seg_1934" s="T1176">da</ta>
            <ta e="T1178" id="Seg_1935" s="T1177">măn</ta>
            <ta e="T1180" id="Seg_1936" s="T1179">măn</ta>
            <ta e="T1181" id="Seg_1937" s="T1180">koŋ</ta>
            <ta e="T1182" id="Seg_1938" s="T1181">i-lV-m</ta>
            <ta e="T1183" id="Seg_1939" s="T1182">a</ta>
            <ta e="T1184" id="Seg_1940" s="T1183">măn</ta>
            <ta e="T1185" id="Seg_1941" s="T1184">nüke-m</ta>
            <ta e="T1186" id="Seg_1942" s="T1185">tože</ta>
            <ta e="T1187" id="Seg_1943" s="T1186">măn</ta>
            <ta e="T1188" id="Seg_1944" s="T1187">koŋ</ta>
            <ta e="T1189" id="Seg_1945" s="T1188">mo-lV-j</ta>
            <ta e="T1190" id="Seg_1946" s="T1189">kan-ə-ʔ</ta>
            <ta e="T1191" id="Seg_1947" s="T1190">dĭgəttə</ta>
            <ta e="T1192" id="Seg_1948" s="T1191">šo-bi</ta>
            <ta e="T1193" id="Seg_1949" s="T1192">amno-bi-jəʔ</ta>
            <ta e="T1194" id="Seg_1950" s="T1193">amno-bi-jəʔ</ta>
            <ta e="T1195" id="Seg_1951" s="T1194">nüke-gəndə</ta>
            <ta e="T1196" id="Seg_1952" s="T1195">a</ta>
            <ta e="T1197" id="Seg_1953" s="T1196">bazoʔ</ta>
            <ta e="T1198" id="Seg_1954" s="T1197">kan-ə-ʔ</ta>
            <ta e="T1199" id="Seg_1955" s="T1198">nörbə-ʔ</ta>
            <ta e="T1200" id="Seg_1956" s="T1199">štobɨ</ta>
            <ta e="T1201" id="Seg_1957" s="T1200">tăn</ta>
            <ta e="T1202" id="Seg_1958" s="T1201">i-bi-l</ta>
            <ta e="T1203" id="Seg_1959" s="T1202">bɨ</ta>
            <ta e="T1204" id="Seg_1960" s="T1203">tsar</ta>
            <ta e="T1205" id="Seg_1961" s="T1204">a</ta>
            <ta e="T1206" id="Seg_1962" s="T1205">măn</ta>
            <ta e="T1207" id="Seg_1963" s="T1206">tsaritsa-ziʔ</ta>
            <ta e="T1208" id="Seg_1964" s="T1207">kan-bi</ta>
            <ta e="T1209" id="Seg_1965" s="T1208">süjö</ta>
            <ta e="T1210" id="Seg_1966" s="T1209">bazoʔ</ta>
            <ta e="T1211" id="Seg_1967" s="T1210">davaj</ta>
            <ta e="T1212" id="Seg_1968" s="T1211">baltu-ziʔ</ta>
            <ta e="T1213" id="Seg_1969" s="T1212">hʼaʔ-zittə</ta>
            <ta e="T1214" id="Seg_1970" s="T1213">pa-m</ta>
            <ta e="T1215" id="Seg_1971" s="T1214">süjö</ta>
            <ta e="T1216" id="Seg_1972" s="T1215">nʼergö-luʔbdə-bi</ta>
            <ta e="T1217" id="Seg_1973" s="T1216">ĭmbi</ta>
            <ta e="T1218" id="Seg_1974" s="T1217">tănan</ta>
            <ta e="T1219" id="Seg_1975" s="T1218">kereʔ</ta>
            <ta e="T1220" id="Seg_1976" s="T1219">büzʼe</ta>
            <ta e="T1221" id="Seg_1977" s="T1220">da</ta>
            <ta e="T1222" id="Seg_1978" s="T1221">măn</ta>
            <ta e="T1223" id="Seg_1979" s="T1222">štobɨ</ta>
            <ta e="T1224" id="Seg_1980" s="T1223">i-bi-m</ta>
            <ta e="T1225" id="Seg_1981" s="T1224">tsar</ta>
            <ta e="T1226" id="Seg_1982" s="T1225">a</ta>
            <ta e="T1227" id="Seg_1983" s="T1226">măn</ta>
            <ta e="T1228" id="Seg_1984" s="T1227">nüke-m</ta>
            <ta e="T1229" id="Seg_1985" s="T1228">tsaritsa</ta>
            <ta e="T1230" id="Seg_1986" s="T1229">kan-ə-ʔ</ta>
            <ta e="T1231" id="Seg_1987" s="T1230">dĭ</ta>
            <ta e="T1233" id="Seg_1988" s="T1232">šonə-gA</ta>
            <ta e="T1234" id="Seg_1989" s="T1233">il</ta>
            <ta e="T1235" id="Seg_1990" s="T1234">bar</ta>
            <ta e="T1237" id="Seg_1991" s="T1235">üžü-jəʔ</ta>
            <ta e="T1238" id="Seg_1992" s="T1237">bar</ta>
            <ta e="T1239" id="Seg_1993" s="T1238">saʔmə-laʔbə-jəʔ</ta>
            <ta e="T1240" id="Seg_1994" s="T1239">dĭ-Tə</ta>
            <ta e="T1241" id="Seg_1995" s="T1240">dĭgəttə</ta>
            <ta e="T1242" id="Seg_1996" s="T1241">šo-bi</ta>
            <ta e="T1243" id="Seg_1997" s="T1242">amno-bi-jəʔ</ta>
            <ta e="T1244" id="Seg_1998" s="T1243">amno-bi-jəʔ</ta>
            <ta e="T1245" id="Seg_1999" s="T1244">dĭgəttə</ta>
            <ta e="T1246" id="Seg_2000" s="T1245">nüke</ta>
            <ta e="T1247" id="Seg_2001" s="T1246">bazoʔ</ta>
            <ta e="T1248" id="Seg_2002" s="T1247">sürer-laʔbə</ta>
            <ta e="T1249" id="Seg_2003" s="T1248">dĭ</ta>
            <ta e="T1250" id="Seg_2004" s="T1249">büzʼe-m</ta>
            <ta e="T1251" id="Seg_2005" s="T1250">kan-ə-ʔ</ta>
            <ta e="T1252" id="Seg_2006" s="T1251">štobɨ</ta>
            <ta e="T1253" id="Seg_2007" s="T1252">tăn</ta>
            <ta e="T1254" id="Seg_2008" s="T1253">kudaj</ta>
            <ta e="T1255" id="Seg_2009" s="T1254">i-bi-l</ta>
            <ta e="T1256" id="Seg_2010" s="T1255">a</ta>
            <ta e="T1257" id="Seg_2011" s="T1256">măn</ta>
            <ta e="T1258" id="Seg_2012" s="T1257">kudaj-ə-n</ta>
            <ta e="T1259" id="Seg_2013" s="T1258">ija-t</ta>
            <ta e="T1260" id="Seg_2014" s="T1259">dĭ</ta>
            <ta e="T1261" id="Seg_2015" s="T1260">kan-bi</ta>
            <ta e="T1262" id="Seg_2016" s="T1261">davaj</ta>
            <ta e="T1263" id="Seg_2017" s="T1262">bazoʔ</ta>
            <ta e="T1264" id="Seg_2018" s="T1263">baltu-ziʔ</ta>
            <ta e="T1265" id="Seg_2019" s="T1264">hʼaʔ-zittə</ta>
            <ta e="T1266" id="Seg_2020" s="T1265">dĭ</ta>
            <ta e="T1267" id="Seg_2021" s="T1266">pa-m</ta>
            <ta e="T1268" id="Seg_2022" s="T1267">dĭ</ta>
            <ta e="T1269" id="Seg_2023" s="T1268">süjö</ta>
            <ta e="T1270" id="Seg_2024" s="T1269">nʼergö-lAʔ</ta>
            <ta e="T1271" id="Seg_2025" s="T1270">šo-bi</ta>
            <ta e="T1272" id="Seg_2026" s="T1271">ĭmbi</ta>
            <ta e="T1273" id="Seg_2027" s="T1272">tănan</ta>
            <ta e="T1274" id="Seg_2028" s="T1273">kereʔ</ta>
            <ta e="T1275" id="Seg_2029" s="T1274">da</ta>
            <ta e="T1276" id="Seg_2030" s="T1275">măn</ta>
            <ta e="T1277" id="Seg_2031" s="T1276">kudaj</ta>
            <ta e="T1278" id="Seg_2032" s="T1277">kudaj</ta>
            <ta e="T1279" id="Seg_2033" s="T1278">mo-lV-m</ta>
            <ta e="T1280" id="Seg_2034" s="T1279">a</ta>
            <ta e="T1281" id="Seg_2035" s="T1280">măn</ta>
            <ta e="T1282" id="Seg_2036" s="T1281">nüke-m</ta>
            <ta e="T1283" id="Seg_2037" s="T1282">kudaj-ə-n</ta>
            <ta e="T1284" id="Seg_2038" s="T1283">ija-t</ta>
            <ta e="T1285" id="Seg_2039" s="T1284">kan-ə-ʔ</ta>
            <ta e="T1286" id="Seg_2040" s="T1285">tăn</ta>
            <ta e="T1287" id="Seg_2041" s="T1286">buga</ta>
            <ta e="T1288" id="Seg_2042" s="T1287">mo-lV-j</ta>
            <ta e="T1289" id="Seg_2043" s="T1288">a</ta>
            <ta e="T1290" id="Seg_2044" s="T1289">tăn</ta>
            <ta e="T1291" id="Seg_2045" s="T1290">nüke</ta>
            <ta e="T1292" id="Seg_2046" s="T1291">šoška</ta>
            <ta e="T1293" id="Seg_2047" s="T1292">mo-lV-j</ta>
            <ta e="T1294" id="Seg_2048" s="T1293">dĭgəttə</ta>
            <ta e="T1295" id="Seg_2049" s="T1294">šo-bi</ta>
            <ta e="T1296" id="Seg_2050" s="T1295">dĭ</ta>
            <ta e="T1297" id="Seg_2051" s="T1296">büzʼe</ta>
            <ta e="T1298" id="Seg_2052" s="T1297">buga</ta>
            <ta e="T1299" id="Seg_2053" s="T1298">a</ta>
            <ta e="T1300" id="Seg_2054" s="T1299">nüke-t</ta>
            <ta e="T1301" id="Seg_2055" s="T1300">šoška</ta>
            <ta e="T1302" id="Seg_2056" s="T1301">i</ta>
            <ta e="T1303" id="Seg_2057" s="T1302">dărəʔ</ta>
            <ta e="T1304" id="Seg_2058" s="T1303">ma-laʔbə-jəʔ</ta>
            <ta e="T1305" id="Seg_2059" s="T1304">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1058" id="Seg_2060" s="T1057">live-PST-3PL</ta>
            <ta e="T1059" id="Seg_2061" s="T1058">woman.[NOM.SG]</ta>
            <ta e="T1060" id="Seg_2062" s="T1059">man.[NOM.SG]</ta>
            <ta e="T1061" id="Seg_2063" s="T1060">there</ta>
            <ta e="T1062" id="Seg_2064" s="T1061">this-PL-LOC</ta>
            <ta e="T1063" id="Seg_2065" s="T1062">what.[NOM.SG]=INDEF</ta>
            <ta e="T1064" id="Seg_2066" s="T1063">NEG</ta>
            <ta e="T1065" id="Seg_2067" s="T1064">NEG.EX-PST.[3SG]</ta>
            <ta e="T1066" id="Seg_2068" s="T1065">bread.[NOM.SG]</ta>
            <ta e="T1067" id="Seg_2069" s="T1066">NEG.EX</ta>
            <ta e="T1068" id="Seg_2070" s="T1067">tree-PL</ta>
            <ta e="T1069" id="Seg_2071" s="T1068">NEG.EX</ta>
            <ta e="T1070" id="Seg_2072" s="T1069">splinter-PL</ta>
            <ta e="T1071" id="Seg_2073" s="T1070">NEG.EX</ta>
            <ta e="T1072" id="Seg_2074" s="T1071">go-EP-IMP.2SG</ta>
            <ta e="T1073" id="Seg_2075" s="T1072">forest-LAT</ta>
            <ta e="T1074" id="Seg_2076" s="T1073">tree-VBLZ-CVB</ta>
            <ta e="T1075" id="Seg_2077" s="T1074">this.[NOM.SG]</ta>
            <ta e="T1076" id="Seg_2078" s="T1075">go-PST.[3SG]</ta>
            <ta e="T1077" id="Seg_2079" s="T1076">there</ta>
            <ta e="T1078" id="Seg_2080" s="T1077">INCH</ta>
            <ta e="T1079" id="Seg_2081" s="T1078">one.[NOM.SG]</ta>
            <ta e="T1080" id="Seg_2082" s="T1079">tree.[NOM.SG]</ta>
            <ta e="T1081" id="Seg_2083" s="T1080">axe-INS</ta>
            <ta e="T1082" id="Seg_2084" s="T1081">cut-INF.LAT</ta>
            <ta e="T1083" id="Seg_2085" s="T1082">then</ta>
            <ta e="T1085" id="Seg_2086" s="T1084">bird.[NOM.SG]</ta>
            <ta e="T1086" id="Seg_2087" s="T1085">fly-CVB</ta>
            <ta e="T1087" id="Seg_2088" s="T1086">come-PST.[3SG]</ta>
            <ta e="T1088" id="Seg_2089" s="T1087">what.[NOM.SG]</ta>
            <ta e="T1089" id="Seg_2090" s="T1088">you.DAT</ta>
            <ta e="T1090" id="Seg_2091" s="T1089">one.needs</ta>
            <ta e="T1091" id="Seg_2092" s="T1090">man.[NOM.SG]</ta>
            <ta e="T1092" id="Seg_2093" s="T1091">this.[NOM.SG]</ta>
            <ta e="T1093" id="Seg_2094" s="T1092">say-IPFVZ.[3SG]</ta>
            <ta e="T1094" id="Seg_2095" s="T1093">I.LAT</ta>
            <ta e="T1095" id="Seg_2096" s="T1094">tree-PL</ta>
            <ta e="T1096" id="Seg_2097" s="T1095">tree-PL</ta>
            <ta e="T1097" id="Seg_2098" s="T1096">one.needs</ta>
            <ta e="T1098" id="Seg_2099" s="T1097">otherwise</ta>
            <ta e="T1099" id="Seg_2100" s="T1098">NEG.EX</ta>
            <ta e="T1100" id="Seg_2101" s="T1099">tree-PL</ta>
            <ta e="T1101" id="Seg_2102" s="T1100">go-EP-IMP.2SG</ta>
            <ta e="T1102" id="Seg_2103" s="T1101">tent-LAT/LOC.3SG</ta>
            <ta e="T1103" id="Seg_2104" s="T1102">there</ta>
            <ta e="T1104" id="Seg_2105" s="T1103">tree-PL</ta>
            <ta e="T1105" id="Seg_2106" s="T1104">many</ta>
            <ta e="T1106" id="Seg_2107" s="T1105">this.[NOM.SG]</ta>
            <ta e="T1107" id="Seg_2108" s="T1106">come-PST.[3SG]</ta>
            <ta e="T1108" id="Seg_2109" s="T1107">tent-LAT/LOC.3SG</ta>
            <ta e="T1109" id="Seg_2110" s="T1108">and</ta>
            <ta e="T1111" id="Seg_2111" s="T1110">very</ta>
            <ta e="T1112" id="Seg_2112" s="T1111">tree-PL</ta>
            <ta e="T1113" id="Seg_2113" s="T1112">many</ta>
            <ta e="T1114" id="Seg_2114" s="T1113">then</ta>
            <ta e="T1115" id="Seg_2115" s="T1114">woman-NOM/GEN.3SG</ta>
            <ta e="T1116" id="Seg_2116" s="T1115">say-IPFVZ.[3SG]</ta>
            <ta e="T1117" id="Seg_2117" s="T1116">go-EP-IMP.2SG</ta>
            <ta e="T1118" id="Seg_2118" s="T1117">JUSS</ta>
            <ta e="T1119" id="Seg_2119" s="T1118">give-FUT-3SG</ta>
            <ta e="T1120" id="Seg_2120" s="T1119">bread.[NOM.SG]</ta>
            <ta e="T1121" id="Seg_2121" s="T1120">then</ta>
            <ta e="T1122" id="Seg_2122" s="T1121">come-PST.[3SG]</ta>
            <ta e="T1123" id="Seg_2123" s="T1122">again</ta>
            <ta e="T1124" id="Seg_2124" s="T1123">axe-INS</ta>
            <ta e="T1125" id="Seg_2125" s="T1124">cut-PST.[3SG]</ta>
            <ta e="T1126" id="Seg_2126" s="T1125">again</ta>
            <ta e="T1127" id="Seg_2127" s="T1126">inside</ta>
            <ta e="T1128" id="Seg_2128" s="T1127">bird.[NOM.SG]</ta>
            <ta e="T1129" id="Seg_2129" s="T1128">fly-DUR-PST.[3SG]</ta>
            <ta e="T1130" id="Seg_2130" s="T1129">what.[NOM.SG]</ta>
            <ta e="T1131" id="Seg_2131" s="T1130">you.DAT</ta>
            <ta e="T1132" id="Seg_2132" s="T1131">one.needs</ta>
            <ta e="T1133" id="Seg_2133" s="T1132">man.[NOM.SG]</ta>
            <ta e="T1134" id="Seg_2134" s="T1133">bread.[NOM.SG]</ta>
            <ta e="T1135" id="Seg_2135" s="T1134">NEG.EX</ta>
            <ta e="T1136" id="Seg_2136" s="T1135">go-EP-IMP.2SG</ta>
            <ta e="T1137" id="Seg_2137" s="T1136">tent-LAT/LOC.3SG</ta>
            <ta e="T1138" id="Seg_2138" s="T1137">this.[NOM.SG]</ta>
            <ta e="T1139" id="Seg_2139" s="T1138">come-PST.[3SG]</ta>
            <ta e="T1140" id="Seg_2140" s="T1139">PTCL</ta>
            <ta e="T1141" id="Seg_2141" s="T1140">cow-PL</ta>
            <ta e="T1142" id="Seg_2142" s="T1141">sheep-PL</ta>
            <ta e="T1143" id="Seg_2143" s="T1142">PTCL</ta>
            <ta e="T1144" id="Seg_2144" s="T1143">many</ta>
            <ta e="T1145" id="Seg_2145" s="T1144">be.full-ADJZ</ta>
            <ta e="T1146" id="Seg_2146" s="T1145">corral.[NOM.SG]</ta>
            <ta e="T1147" id="Seg_2147" s="T1146">and</ta>
            <ta e="T1148" id="Seg_2148" s="T1147">eat-INF.LAT</ta>
            <ta e="T1149" id="Seg_2149" s="T1148">many</ta>
            <ta e="T1150" id="Seg_2150" s="T1149">be-PRS.[3SG]</ta>
            <ta e="T1151" id="Seg_2151" s="T1150">woman.[NOM.SG]</ta>
            <ta e="T1152" id="Seg_2152" s="T1151">eat-PST.[3SG]</ta>
            <ta e="T1153" id="Seg_2153" s="T1152">eat-PST.[3SG]</ta>
            <ta e="T1154" id="Seg_2154" s="T1153">go-EP-IMP.2SG</ta>
            <ta e="T1155" id="Seg_2155" s="T1154">tell-IMP.2SG</ta>
            <ta e="T1156" id="Seg_2156" s="T1155">so.that</ta>
            <ta e="T1157" id="Seg_2157" s="T1156">you.NOM</ta>
            <ta e="T1159" id="Seg_2158" s="T1158">you.NOM</ta>
            <ta e="T1160" id="Seg_2159" s="T1159">be-PST-2SG</ta>
            <ta e="T1161" id="Seg_2160" s="T1160">chief.[NOM.SG]</ta>
            <ta e="T1162" id="Seg_2161" s="T1161">and</ta>
            <ta e="T1163" id="Seg_2162" s="T1162">I.NOM</ta>
            <ta e="T1164" id="Seg_2163" s="T1163">chief-LOC</ta>
            <ta e="T1165" id="Seg_2164" s="T1164">woman.[NOM.SG]</ta>
            <ta e="T1166" id="Seg_2165" s="T1165">this.[NOM.SG]</ta>
            <ta e="T1167" id="Seg_2166" s="T1166">go-PST.[3SG]</ta>
            <ta e="T1168" id="Seg_2167" s="T1167">again</ta>
            <ta e="T1169" id="Seg_2168" s="T1168">axe-INS</ta>
            <ta e="T1170" id="Seg_2169" s="T1169">cut-PST.[3SG]</ta>
            <ta e="T1171" id="Seg_2170" s="T1170">bird.[NOM.SG]</ta>
            <ta e="T1172" id="Seg_2171" s="T1171">fly-DUR-PST.[3SG]</ta>
            <ta e="T1173" id="Seg_2172" s="T1172">what.[NOM.SG]</ta>
            <ta e="T1175" id="Seg_2173" s="T1174">you.DAT</ta>
            <ta e="T1176" id="Seg_2174" s="T1175">one.needs</ta>
            <ta e="T1177" id="Seg_2175" s="T1176">and</ta>
            <ta e="T1178" id="Seg_2176" s="T1177">I.NOM</ta>
            <ta e="T1180" id="Seg_2177" s="T1179">I.NOM</ta>
            <ta e="T1181" id="Seg_2178" s="T1180">chief.[NOM.SG]</ta>
            <ta e="T1182" id="Seg_2179" s="T1181">be-FUT-1SG</ta>
            <ta e="T1183" id="Seg_2180" s="T1182">and</ta>
            <ta e="T1184" id="Seg_2181" s="T1183">I.GEN</ta>
            <ta e="T1185" id="Seg_2182" s="T1184">woman-NOM/GEN/ACC.1SG</ta>
            <ta e="T1186" id="Seg_2183" s="T1185">also</ta>
            <ta e="T1187" id="Seg_2184" s="T1186">I.GEN</ta>
            <ta e="T1188" id="Seg_2185" s="T1187">chief.[NOM.SG]</ta>
            <ta e="T1189" id="Seg_2186" s="T1188">become-FUT-3SG</ta>
            <ta e="T1190" id="Seg_2187" s="T1189">go-EP-IMP.2SG</ta>
            <ta e="T1191" id="Seg_2188" s="T1190">then</ta>
            <ta e="T1192" id="Seg_2189" s="T1191">come-PST.[3SG]</ta>
            <ta e="T1193" id="Seg_2190" s="T1192">live-PST-3PL</ta>
            <ta e="T1194" id="Seg_2191" s="T1193">live-PST-3PL</ta>
            <ta e="T1195" id="Seg_2192" s="T1194">woman-LAT/LOC.3SG</ta>
            <ta e="T1196" id="Seg_2193" s="T1195">and</ta>
            <ta e="T1197" id="Seg_2194" s="T1196">again</ta>
            <ta e="T1198" id="Seg_2195" s="T1197">go-EP-IMP.2SG</ta>
            <ta e="T1199" id="Seg_2196" s="T1198">tell-IMP.2SG</ta>
            <ta e="T1200" id="Seg_2197" s="T1199">so.that</ta>
            <ta e="T1201" id="Seg_2198" s="T1200">you.NOM</ta>
            <ta e="T1202" id="Seg_2199" s="T1201">be-PST-2SG</ta>
            <ta e="T1203" id="Seg_2200" s="T1202">IRREAL</ta>
            <ta e="T1204" id="Seg_2201" s="T1203">tsar.[NOM.SG]</ta>
            <ta e="T1205" id="Seg_2202" s="T1204">and</ta>
            <ta e="T1206" id="Seg_2203" s="T1205">I.NOM</ta>
            <ta e="T1207" id="Seg_2204" s="T1206">tsarina-INS</ta>
            <ta e="T1208" id="Seg_2205" s="T1207">go-PST.[3SG]</ta>
            <ta e="T1209" id="Seg_2206" s="T1208">bird.[NOM.SG]</ta>
            <ta e="T1210" id="Seg_2207" s="T1209">again</ta>
            <ta e="T1211" id="Seg_2208" s="T1210">INCH</ta>
            <ta e="T1212" id="Seg_2209" s="T1211">axe-INS</ta>
            <ta e="T1213" id="Seg_2210" s="T1212">cut-INF.LAT</ta>
            <ta e="T1214" id="Seg_2211" s="T1213">tree-ACC</ta>
            <ta e="T1215" id="Seg_2212" s="T1214">bird.[NOM.SG]</ta>
            <ta e="T1216" id="Seg_2213" s="T1215">fly-MOM-PST.[3SG]</ta>
            <ta e="T1217" id="Seg_2214" s="T1216">what.[NOM.SG]</ta>
            <ta e="T1218" id="Seg_2215" s="T1217">you.DAT</ta>
            <ta e="T1219" id="Seg_2216" s="T1218">one.needs</ta>
            <ta e="T1220" id="Seg_2217" s="T1219">man.[NOM.SG]</ta>
            <ta e="T1221" id="Seg_2218" s="T1220">and</ta>
            <ta e="T1222" id="Seg_2219" s="T1221">I.NOM</ta>
            <ta e="T1223" id="Seg_2220" s="T1222">so.that</ta>
            <ta e="T1224" id="Seg_2221" s="T1223">be-PST-1SG</ta>
            <ta e="T1225" id="Seg_2222" s="T1224">tsar.[NOM.SG]</ta>
            <ta e="T1226" id="Seg_2223" s="T1225">and</ta>
            <ta e="T1227" id="Seg_2224" s="T1226">I.GEN</ta>
            <ta e="T1228" id="Seg_2225" s="T1227">woman-NOM/GEN/ACC.1SG</ta>
            <ta e="T1229" id="Seg_2226" s="T1228">tsarina</ta>
            <ta e="T1230" id="Seg_2227" s="T1229">go-EP-IMP.2SG</ta>
            <ta e="T1231" id="Seg_2228" s="T1230">this.[NOM.SG]</ta>
            <ta e="T1233" id="Seg_2229" s="T1232">come-PRS.[3SG]</ta>
            <ta e="T1234" id="Seg_2230" s="T1233">people.[NOM.SG]</ta>
            <ta e="T1235" id="Seg_2231" s="T1234">all</ta>
            <ta e="T1237" id="Seg_2232" s="T1235">cap-PL</ta>
            <ta e="T1238" id="Seg_2233" s="T1237">all</ta>
            <ta e="T1239" id="Seg_2234" s="T1238">fall-DUR-3PL</ta>
            <ta e="T1240" id="Seg_2235" s="T1239">this-LAT</ta>
            <ta e="T1241" id="Seg_2236" s="T1240">then</ta>
            <ta e="T1242" id="Seg_2237" s="T1241">come-PST.[3SG]</ta>
            <ta e="T1243" id="Seg_2238" s="T1242">live-PST-3PL</ta>
            <ta e="T1244" id="Seg_2239" s="T1243">live-PST-3PL</ta>
            <ta e="T1245" id="Seg_2240" s="T1244">then</ta>
            <ta e="T1246" id="Seg_2241" s="T1245">woman.[NOM.SG]</ta>
            <ta e="T1247" id="Seg_2242" s="T1246">again</ta>
            <ta e="T1248" id="Seg_2243" s="T1247">drive-DUR.[3SG]</ta>
            <ta e="T1249" id="Seg_2244" s="T1248">this.[NOM.SG]</ta>
            <ta e="T1250" id="Seg_2245" s="T1249">man-ACC</ta>
            <ta e="T1251" id="Seg_2246" s="T1250">go-EP-IMP.2SG</ta>
            <ta e="T1252" id="Seg_2247" s="T1251">so.that</ta>
            <ta e="T1253" id="Seg_2248" s="T1252">you.NOM</ta>
            <ta e="T1254" id="Seg_2249" s="T1253">God.[NOM.SG]</ta>
            <ta e="T1255" id="Seg_2250" s="T1254">be-PST-2SG</ta>
            <ta e="T1256" id="Seg_2251" s="T1255">and</ta>
            <ta e="T1257" id="Seg_2252" s="T1256">I.NOM</ta>
            <ta e="T1258" id="Seg_2253" s="T1257">God-EP-GEN</ta>
            <ta e="T1259" id="Seg_2254" s="T1258">mother-NOM/GEN.3SG</ta>
            <ta e="T1260" id="Seg_2255" s="T1259">this.[NOM.SG]</ta>
            <ta e="T1261" id="Seg_2256" s="T1260">go-PST.[3SG]</ta>
            <ta e="T1262" id="Seg_2257" s="T1261">INCH</ta>
            <ta e="T1263" id="Seg_2258" s="T1262">again</ta>
            <ta e="T1264" id="Seg_2259" s="T1263">axe-INS</ta>
            <ta e="T1265" id="Seg_2260" s="T1264">cut-INF.LAT</ta>
            <ta e="T1266" id="Seg_2261" s="T1265">this.[NOM.SG]</ta>
            <ta e="T1267" id="Seg_2262" s="T1266">tree-ACC</ta>
            <ta e="T1268" id="Seg_2263" s="T1267">this.[NOM.SG]</ta>
            <ta e="T1269" id="Seg_2264" s="T1268">bird.[NOM.SG]</ta>
            <ta e="T1270" id="Seg_2265" s="T1269">fly-CVB</ta>
            <ta e="T1271" id="Seg_2266" s="T1270">come-PST.[3SG]</ta>
            <ta e="T1272" id="Seg_2267" s="T1271">what.[NOM.SG]</ta>
            <ta e="T1273" id="Seg_2268" s="T1272">you.DAT</ta>
            <ta e="T1274" id="Seg_2269" s="T1273">one.needs</ta>
            <ta e="T1275" id="Seg_2270" s="T1274">and</ta>
            <ta e="T1276" id="Seg_2271" s="T1275">I.NOM</ta>
            <ta e="T1277" id="Seg_2272" s="T1276">God.[NOM.SG]</ta>
            <ta e="T1278" id="Seg_2273" s="T1277">God.[NOM.SG]</ta>
            <ta e="T1279" id="Seg_2274" s="T1278">become-FUT-1SG</ta>
            <ta e="T1280" id="Seg_2275" s="T1279">and</ta>
            <ta e="T1281" id="Seg_2276" s="T1280">I.GEN</ta>
            <ta e="T1282" id="Seg_2277" s="T1281">woman-NOM/GEN/ACC.1SG</ta>
            <ta e="T1283" id="Seg_2278" s="T1282">God-EP-GEN</ta>
            <ta e="T1284" id="Seg_2279" s="T1283">mother-NOM/GEN.3SG</ta>
            <ta e="T1285" id="Seg_2280" s="T1284">go-EP-IMP.2SG</ta>
            <ta e="T1286" id="Seg_2281" s="T1285">you.NOM</ta>
            <ta e="T1287" id="Seg_2282" s="T1286">bull.[NOM.SG]</ta>
            <ta e="T1288" id="Seg_2283" s="T1287">become-FUT-3SG</ta>
            <ta e="T1289" id="Seg_2284" s="T1288">and</ta>
            <ta e="T1290" id="Seg_2285" s="T1289">you.GEN</ta>
            <ta e="T1291" id="Seg_2286" s="T1290">woman.[NOM.SG]</ta>
            <ta e="T1292" id="Seg_2287" s="T1291">pig.[NOM.SG]</ta>
            <ta e="T1293" id="Seg_2288" s="T1292">become-FUT-3SG</ta>
            <ta e="T1294" id="Seg_2289" s="T1293">then</ta>
            <ta e="T1295" id="Seg_2290" s="T1294">come-PST.[3SG]</ta>
            <ta e="T1296" id="Seg_2291" s="T1295">this.[NOM.SG]</ta>
            <ta e="T1297" id="Seg_2292" s="T1296">man.[NOM.SG]</ta>
            <ta e="T1298" id="Seg_2293" s="T1297">bull.[NOM.SG]</ta>
            <ta e="T1299" id="Seg_2294" s="T1298">and</ta>
            <ta e="T1300" id="Seg_2295" s="T1299">woman-NOM/GEN.3SG</ta>
            <ta e="T1301" id="Seg_2296" s="T1300">pig.[NOM.SG]</ta>
            <ta e="T1302" id="Seg_2297" s="T1301">and</ta>
            <ta e="T1303" id="Seg_2298" s="T1302">so</ta>
            <ta e="T1304" id="Seg_2299" s="T1303">leave-DUR-3PL</ta>
            <ta e="T1305" id="Seg_2300" s="T1304">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1058" id="Seg_2301" s="T1057">жить-PST-3PL</ta>
            <ta e="T1059" id="Seg_2302" s="T1058">женщина.[NOM.SG]</ta>
            <ta e="T1060" id="Seg_2303" s="T1059">мужчина.[NOM.SG]</ta>
            <ta e="T1061" id="Seg_2304" s="T1060">там</ta>
            <ta e="T1062" id="Seg_2305" s="T1061">этот-PL-LOC</ta>
            <ta e="T1063" id="Seg_2306" s="T1062">что.[NOM.SG]=INDEF</ta>
            <ta e="T1064" id="Seg_2307" s="T1063">NEG</ta>
            <ta e="T1065" id="Seg_2308" s="T1064">NEG.EX-PST.[3SG]</ta>
            <ta e="T1066" id="Seg_2309" s="T1065">хлеб.[NOM.SG]</ta>
            <ta e="T1067" id="Seg_2310" s="T1066">NEG.EX</ta>
            <ta e="T1068" id="Seg_2311" s="T1067">дерево-PL</ta>
            <ta e="T1069" id="Seg_2312" s="T1068">NEG.EX</ta>
            <ta e="T1070" id="Seg_2313" s="T1069">лучина-PL</ta>
            <ta e="T1071" id="Seg_2314" s="T1070">NEG.EX</ta>
            <ta e="T1072" id="Seg_2315" s="T1071">пойти-EP-IMP.2SG</ta>
            <ta e="T1073" id="Seg_2316" s="T1072">лес-LAT</ta>
            <ta e="T1074" id="Seg_2317" s="T1073">дерево-VBLZ-CVB</ta>
            <ta e="T1075" id="Seg_2318" s="T1074">этот.[NOM.SG]</ta>
            <ta e="T1076" id="Seg_2319" s="T1075">пойти-PST.[3SG]</ta>
            <ta e="T1077" id="Seg_2320" s="T1076">там</ta>
            <ta e="T1078" id="Seg_2321" s="T1077">INCH</ta>
            <ta e="T1079" id="Seg_2322" s="T1078">один.[NOM.SG]</ta>
            <ta e="T1080" id="Seg_2323" s="T1079">дерево.[NOM.SG]</ta>
            <ta e="T1081" id="Seg_2324" s="T1080">топор-INS</ta>
            <ta e="T1082" id="Seg_2325" s="T1081">резать-INF.LAT</ta>
            <ta e="T1083" id="Seg_2326" s="T1082">тогда</ta>
            <ta e="T1085" id="Seg_2327" s="T1084">птица.[NOM.SG]</ta>
            <ta e="T1086" id="Seg_2328" s="T1085">лететь-CVB</ta>
            <ta e="T1087" id="Seg_2329" s="T1086">прийти-PST.[3SG]</ta>
            <ta e="T1088" id="Seg_2330" s="T1087">что.[NOM.SG]</ta>
            <ta e="T1089" id="Seg_2331" s="T1088">ты.DAT</ta>
            <ta e="T1090" id="Seg_2332" s="T1089">нужно</ta>
            <ta e="T1091" id="Seg_2333" s="T1090">мужчина.[NOM.SG]</ta>
            <ta e="T1092" id="Seg_2334" s="T1091">этот.[NOM.SG]</ta>
            <ta e="T1093" id="Seg_2335" s="T1092">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1094" id="Seg_2336" s="T1093">я.LAT</ta>
            <ta e="T1095" id="Seg_2337" s="T1094">дерево-PL</ta>
            <ta e="T1096" id="Seg_2338" s="T1095">дерево-PL</ta>
            <ta e="T1097" id="Seg_2339" s="T1096">нужно</ta>
            <ta e="T1098" id="Seg_2340" s="T1097">а.то</ta>
            <ta e="T1099" id="Seg_2341" s="T1098">NEG.EX</ta>
            <ta e="T1100" id="Seg_2342" s="T1099">дерево-PL</ta>
            <ta e="T1101" id="Seg_2343" s="T1100">пойти-EP-IMP.2SG</ta>
            <ta e="T1102" id="Seg_2344" s="T1101">чум-LAT/LOC.3SG</ta>
            <ta e="T1103" id="Seg_2345" s="T1102">там</ta>
            <ta e="T1104" id="Seg_2346" s="T1103">дерево-PL</ta>
            <ta e="T1105" id="Seg_2347" s="T1104">много</ta>
            <ta e="T1106" id="Seg_2348" s="T1105">этот.[NOM.SG]</ta>
            <ta e="T1107" id="Seg_2349" s="T1106">прийти-PST.[3SG]</ta>
            <ta e="T1108" id="Seg_2350" s="T1107">чум-LAT/LOC.3SG</ta>
            <ta e="T1109" id="Seg_2351" s="T1108">и</ta>
            <ta e="T1111" id="Seg_2352" s="T1110">очень</ta>
            <ta e="T1112" id="Seg_2353" s="T1111">дерево-PL</ta>
            <ta e="T1113" id="Seg_2354" s="T1112">много</ta>
            <ta e="T1114" id="Seg_2355" s="T1113">тогда</ta>
            <ta e="T1115" id="Seg_2356" s="T1114">женщина-NOM/GEN.3SG</ta>
            <ta e="T1116" id="Seg_2357" s="T1115">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1117" id="Seg_2358" s="T1116">пойти-EP-IMP.2SG</ta>
            <ta e="T1118" id="Seg_2359" s="T1117">JUSS</ta>
            <ta e="T1119" id="Seg_2360" s="T1118">дать-FUT-3SG</ta>
            <ta e="T1120" id="Seg_2361" s="T1119">хлеб.[NOM.SG]</ta>
            <ta e="T1121" id="Seg_2362" s="T1120">тогда</ta>
            <ta e="T1122" id="Seg_2363" s="T1121">прийти-PST.[3SG]</ta>
            <ta e="T1123" id="Seg_2364" s="T1122">опять</ta>
            <ta e="T1124" id="Seg_2365" s="T1123">топор-INS</ta>
            <ta e="T1125" id="Seg_2366" s="T1124">резать-PST.[3SG]</ta>
            <ta e="T1126" id="Seg_2367" s="T1125">опять</ta>
            <ta e="T1127" id="Seg_2368" s="T1126">внутри</ta>
            <ta e="T1128" id="Seg_2369" s="T1127">птица.[NOM.SG]</ta>
            <ta e="T1129" id="Seg_2370" s="T1128">лететь-DUR-PST.[3SG]</ta>
            <ta e="T1130" id="Seg_2371" s="T1129">что.[NOM.SG]</ta>
            <ta e="T1131" id="Seg_2372" s="T1130">ты.DAT</ta>
            <ta e="T1132" id="Seg_2373" s="T1131">нужно</ta>
            <ta e="T1133" id="Seg_2374" s="T1132">мужчина.[NOM.SG]</ta>
            <ta e="T1134" id="Seg_2375" s="T1133">хлеб.[NOM.SG]</ta>
            <ta e="T1135" id="Seg_2376" s="T1134">NEG.EX</ta>
            <ta e="T1136" id="Seg_2377" s="T1135">пойти-EP-IMP.2SG</ta>
            <ta e="T1137" id="Seg_2378" s="T1136">чум-LAT/LOC.3SG</ta>
            <ta e="T1138" id="Seg_2379" s="T1137">этот.[NOM.SG]</ta>
            <ta e="T1139" id="Seg_2380" s="T1138">прийти-PST.[3SG]</ta>
            <ta e="T1140" id="Seg_2381" s="T1139">PTCL</ta>
            <ta e="T1141" id="Seg_2382" s="T1140">корова-PL</ta>
            <ta e="T1142" id="Seg_2383" s="T1141">овца-PL</ta>
            <ta e="T1143" id="Seg_2384" s="T1142">PTCL</ta>
            <ta e="T1144" id="Seg_2385" s="T1143">много</ta>
            <ta e="T1145" id="Seg_2386" s="T1144">быть.полным-ADJZ</ta>
            <ta e="T1146" id="Seg_2387" s="T1145">кораль.[NOM.SG]</ta>
            <ta e="T1147" id="Seg_2388" s="T1146">и</ta>
            <ta e="T1148" id="Seg_2389" s="T1147">съесть-INF.LAT</ta>
            <ta e="T1149" id="Seg_2390" s="T1148">много</ta>
            <ta e="T1150" id="Seg_2391" s="T1149">быть-PRS.[3SG]</ta>
            <ta e="T1151" id="Seg_2392" s="T1150">женщина.[NOM.SG]</ta>
            <ta e="T1152" id="Seg_2393" s="T1151">съесть-PST.[3SG]</ta>
            <ta e="T1153" id="Seg_2394" s="T1152">съесть-PST.[3SG]</ta>
            <ta e="T1154" id="Seg_2395" s="T1153">пойти-EP-IMP.2SG</ta>
            <ta e="T1155" id="Seg_2396" s="T1154">сказать-IMP.2SG</ta>
            <ta e="T1156" id="Seg_2397" s="T1155">чтобы</ta>
            <ta e="T1157" id="Seg_2398" s="T1156">ты.NOM</ta>
            <ta e="T1159" id="Seg_2399" s="T1158">ты.NOM</ta>
            <ta e="T1160" id="Seg_2400" s="T1159">быть-PST-2SG</ta>
            <ta e="T1161" id="Seg_2401" s="T1160">вождь.[NOM.SG]</ta>
            <ta e="T1162" id="Seg_2402" s="T1161">а</ta>
            <ta e="T1163" id="Seg_2403" s="T1162">я.NOM</ta>
            <ta e="T1164" id="Seg_2404" s="T1163">вождь-LOC</ta>
            <ta e="T1165" id="Seg_2405" s="T1164">женщина.[NOM.SG]</ta>
            <ta e="T1166" id="Seg_2406" s="T1165">этот.[NOM.SG]</ta>
            <ta e="T1167" id="Seg_2407" s="T1166">пойти-PST.[3SG]</ta>
            <ta e="T1168" id="Seg_2408" s="T1167">опять</ta>
            <ta e="T1169" id="Seg_2409" s="T1168">топор-INS</ta>
            <ta e="T1170" id="Seg_2410" s="T1169">резать-PST.[3SG]</ta>
            <ta e="T1171" id="Seg_2411" s="T1170">птица.[NOM.SG]</ta>
            <ta e="T1172" id="Seg_2412" s="T1171">лететь-DUR-PST.[3SG]</ta>
            <ta e="T1173" id="Seg_2413" s="T1172">что.[NOM.SG]</ta>
            <ta e="T1175" id="Seg_2414" s="T1174">ты.DAT</ta>
            <ta e="T1176" id="Seg_2415" s="T1175">нужно</ta>
            <ta e="T1177" id="Seg_2416" s="T1176">и</ta>
            <ta e="T1178" id="Seg_2417" s="T1177">я.NOM</ta>
            <ta e="T1180" id="Seg_2418" s="T1179">я.NOM</ta>
            <ta e="T1181" id="Seg_2419" s="T1180">вождь.[NOM.SG]</ta>
            <ta e="T1182" id="Seg_2420" s="T1181">быть-FUT-1SG</ta>
            <ta e="T1183" id="Seg_2421" s="T1182">а</ta>
            <ta e="T1184" id="Seg_2422" s="T1183">я.GEN</ta>
            <ta e="T1185" id="Seg_2423" s="T1184">женщина-NOM/GEN/ACC.1SG</ta>
            <ta e="T1186" id="Seg_2424" s="T1185">тоже</ta>
            <ta e="T1187" id="Seg_2425" s="T1186">я.GEN</ta>
            <ta e="T1188" id="Seg_2426" s="T1187">вождь.[NOM.SG]</ta>
            <ta e="T1189" id="Seg_2427" s="T1188">стать-FUT-3SG</ta>
            <ta e="T1190" id="Seg_2428" s="T1189">пойти-EP-IMP.2SG</ta>
            <ta e="T1191" id="Seg_2429" s="T1190">тогда</ta>
            <ta e="T1192" id="Seg_2430" s="T1191">прийти-PST.[3SG]</ta>
            <ta e="T1193" id="Seg_2431" s="T1192">жить-PST-3PL</ta>
            <ta e="T1194" id="Seg_2432" s="T1193">жить-PST-3PL</ta>
            <ta e="T1195" id="Seg_2433" s="T1194">женщина-LAT/LOC.3SG</ta>
            <ta e="T1196" id="Seg_2434" s="T1195">а</ta>
            <ta e="T1197" id="Seg_2435" s="T1196">опять</ta>
            <ta e="T1198" id="Seg_2436" s="T1197">пойти-EP-IMP.2SG</ta>
            <ta e="T1199" id="Seg_2437" s="T1198">сказать-IMP.2SG</ta>
            <ta e="T1200" id="Seg_2438" s="T1199">чтобы</ta>
            <ta e="T1201" id="Seg_2439" s="T1200">ты.NOM</ta>
            <ta e="T1202" id="Seg_2440" s="T1201">быть-PST-2SG</ta>
            <ta e="T1203" id="Seg_2441" s="T1202">IRREAL</ta>
            <ta e="T1204" id="Seg_2442" s="T1203">царь.[NOM.SG]</ta>
            <ta e="T1205" id="Seg_2443" s="T1204">а</ta>
            <ta e="T1206" id="Seg_2444" s="T1205">я.NOM</ta>
            <ta e="T1207" id="Seg_2445" s="T1206">царица-INS</ta>
            <ta e="T1208" id="Seg_2446" s="T1207">пойти-PST.[3SG]</ta>
            <ta e="T1209" id="Seg_2447" s="T1208">птица.[NOM.SG]</ta>
            <ta e="T1210" id="Seg_2448" s="T1209">опять</ta>
            <ta e="T1211" id="Seg_2449" s="T1210">INCH</ta>
            <ta e="T1212" id="Seg_2450" s="T1211">топор-INS</ta>
            <ta e="T1213" id="Seg_2451" s="T1212">резать-INF.LAT</ta>
            <ta e="T1214" id="Seg_2452" s="T1213">дерево-ACC</ta>
            <ta e="T1215" id="Seg_2453" s="T1214">птица.[NOM.SG]</ta>
            <ta e="T1216" id="Seg_2454" s="T1215">лететь-MOM-PST.[3SG]</ta>
            <ta e="T1217" id="Seg_2455" s="T1216">что.[NOM.SG]</ta>
            <ta e="T1218" id="Seg_2456" s="T1217">ты.DAT</ta>
            <ta e="T1219" id="Seg_2457" s="T1218">нужно</ta>
            <ta e="T1220" id="Seg_2458" s="T1219">мужчина.[NOM.SG]</ta>
            <ta e="T1221" id="Seg_2459" s="T1220">и</ta>
            <ta e="T1222" id="Seg_2460" s="T1221">я.NOM</ta>
            <ta e="T1223" id="Seg_2461" s="T1222">чтобы</ta>
            <ta e="T1224" id="Seg_2462" s="T1223">быть-PST-1SG</ta>
            <ta e="T1225" id="Seg_2463" s="T1224">царь.[NOM.SG]</ta>
            <ta e="T1226" id="Seg_2464" s="T1225">а</ta>
            <ta e="T1227" id="Seg_2465" s="T1226">я.GEN</ta>
            <ta e="T1228" id="Seg_2466" s="T1227">женщина-NOM/GEN/ACC.1SG</ta>
            <ta e="T1229" id="Seg_2467" s="T1228">царица</ta>
            <ta e="T1230" id="Seg_2468" s="T1229">пойти-EP-IMP.2SG</ta>
            <ta e="T1231" id="Seg_2469" s="T1230">этот.[NOM.SG]</ta>
            <ta e="T1233" id="Seg_2470" s="T1232">прийти-PRS.[3SG]</ta>
            <ta e="T1234" id="Seg_2471" s="T1233">люди.[NOM.SG]</ta>
            <ta e="T1235" id="Seg_2472" s="T1234">весь</ta>
            <ta e="T1237" id="Seg_2473" s="T1235">шапка-PL</ta>
            <ta e="T1238" id="Seg_2474" s="T1237">весь</ta>
            <ta e="T1239" id="Seg_2475" s="T1238">упасть-DUR-3PL</ta>
            <ta e="T1240" id="Seg_2476" s="T1239">этот-LAT</ta>
            <ta e="T1241" id="Seg_2477" s="T1240">тогда</ta>
            <ta e="T1242" id="Seg_2478" s="T1241">прийти-PST.[3SG]</ta>
            <ta e="T1243" id="Seg_2479" s="T1242">жить-PST-3PL</ta>
            <ta e="T1244" id="Seg_2480" s="T1243">жить-PST-3PL</ta>
            <ta e="T1245" id="Seg_2481" s="T1244">тогда</ta>
            <ta e="T1246" id="Seg_2482" s="T1245">женщина.[NOM.SG]</ta>
            <ta e="T1247" id="Seg_2483" s="T1246">опять</ta>
            <ta e="T1248" id="Seg_2484" s="T1247">гнать-DUR.[3SG]</ta>
            <ta e="T1249" id="Seg_2485" s="T1248">этот.[NOM.SG]</ta>
            <ta e="T1250" id="Seg_2486" s="T1249">мужчина-ACC</ta>
            <ta e="T1251" id="Seg_2487" s="T1250">пойти-EP-IMP.2SG</ta>
            <ta e="T1252" id="Seg_2488" s="T1251">чтобы</ta>
            <ta e="T1253" id="Seg_2489" s="T1252">ты.NOM</ta>
            <ta e="T1254" id="Seg_2490" s="T1253">бог.[NOM.SG]</ta>
            <ta e="T1255" id="Seg_2491" s="T1254">быть-PST-2SG</ta>
            <ta e="T1256" id="Seg_2492" s="T1255">а</ta>
            <ta e="T1257" id="Seg_2493" s="T1256">я.NOM</ta>
            <ta e="T1258" id="Seg_2494" s="T1257">бог-EP-GEN</ta>
            <ta e="T1259" id="Seg_2495" s="T1258">мать-NOM/GEN.3SG</ta>
            <ta e="T1260" id="Seg_2496" s="T1259">этот.[NOM.SG]</ta>
            <ta e="T1261" id="Seg_2497" s="T1260">пойти-PST.[3SG]</ta>
            <ta e="T1262" id="Seg_2498" s="T1261">INCH</ta>
            <ta e="T1263" id="Seg_2499" s="T1262">опять</ta>
            <ta e="T1264" id="Seg_2500" s="T1263">топор-INS</ta>
            <ta e="T1265" id="Seg_2501" s="T1264">резать-INF.LAT</ta>
            <ta e="T1266" id="Seg_2502" s="T1265">этот.[NOM.SG]</ta>
            <ta e="T1267" id="Seg_2503" s="T1266">дерево-ACC</ta>
            <ta e="T1268" id="Seg_2504" s="T1267">этот.[NOM.SG]</ta>
            <ta e="T1269" id="Seg_2505" s="T1268">птица.[NOM.SG]</ta>
            <ta e="T1270" id="Seg_2506" s="T1269">лететь-CVB</ta>
            <ta e="T1271" id="Seg_2507" s="T1270">прийти-PST.[3SG]</ta>
            <ta e="T1272" id="Seg_2508" s="T1271">что.[NOM.SG]</ta>
            <ta e="T1273" id="Seg_2509" s="T1272">ты.DAT</ta>
            <ta e="T1274" id="Seg_2510" s="T1273">нужно</ta>
            <ta e="T1275" id="Seg_2511" s="T1274">и</ta>
            <ta e="T1276" id="Seg_2512" s="T1275">я.NOM</ta>
            <ta e="T1277" id="Seg_2513" s="T1276">бог.[NOM.SG]</ta>
            <ta e="T1278" id="Seg_2514" s="T1277">бог.[NOM.SG]</ta>
            <ta e="T1279" id="Seg_2515" s="T1278">мочь-FUT-1SG</ta>
            <ta e="T1280" id="Seg_2516" s="T1279">а</ta>
            <ta e="T1281" id="Seg_2517" s="T1280">я.GEN</ta>
            <ta e="T1282" id="Seg_2518" s="T1281">женщина-NOM/GEN/ACC.1SG</ta>
            <ta e="T1283" id="Seg_2519" s="T1282">бог-EP-GEN</ta>
            <ta e="T1284" id="Seg_2520" s="T1283">мать-NOM/GEN.3SG</ta>
            <ta e="T1285" id="Seg_2521" s="T1284">пойти-EP-IMP.2SG</ta>
            <ta e="T1286" id="Seg_2522" s="T1285">ты.NOM</ta>
            <ta e="T1287" id="Seg_2523" s="T1286">бык.[NOM.SG]</ta>
            <ta e="T1288" id="Seg_2524" s="T1287">стать-FUT-3SG</ta>
            <ta e="T1289" id="Seg_2525" s="T1288">а</ta>
            <ta e="T1290" id="Seg_2526" s="T1289">ты.GEN</ta>
            <ta e="T1291" id="Seg_2527" s="T1290">женщина.[NOM.SG]</ta>
            <ta e="T1292" id="Seg_2528" s="T1291">свинья.[NOM.SG]</ta>
            <ta e="T1293" id="Seg_2529" s="T1292">стать-FUT-3SG</ta>
            <ta e="T1294" id="Seg_2530" s="T1293">тогда</ta>
            <ta e="T1295" id="Seg_2531" s="T1294">прийти-PST.[3SG]</ta>
            <ta e="T1296" id="Seg_2532" s="T1295">этот.[NOM.SG]</ta>
            <ta e="T1297" id="Seg_2533" s="T1296">мужчина.[NOM.SG]</ta>
            <ta e="T1298" id="Seg_2534" s="T1297">бык.[NOM.SG]</ta>
            <ta e="T1299" id="Seg_2535" s="T1298">а</ta>
            <ta e="T1300" id="Seg_2536" s="T1299">женщина-NOM/GEN.3SG</ta>
            <ta e="T1301" id="Seg_2537" s="T1300">свинья.[NOM.SG]</ta>
            <ta e="T1302" id="Seg_2538" s="T1301">и</ta>
            <ta e="T1303" id="Seg_2539" s="T1302">так</ta>
            <ta e="T1304" id="Seg_2540" s="T1303">оставить-DUR-3PL</ta>
            <ta e="T1305" id="Seg_2541" s="T1304">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1058" id="Seg_2542" s="T1057">v-v:tense-v:pn</ta>
            <ta e="T1059" id="Seg_2543" s="T1058">n-n:case</ta>
            <ta e="T1060" id="Seg_2544" s="T1059">n-n:case</ta>
            <ta e="T1061" id="Seg_2545" s="T1060">adv</ta>
            <ta e="T1062" id="Seg_2546" s="T1061">dempro-n:num-n:case</ta>
            <ta e="T1063" id="Seg_2547" s="T1062">que-n:case=ptcl</ta>
            <ta e="T1064" id="Seg_2548" s="T1063">ptcl</ta>
            <ta e="T1065" id="Seg_2549" s="T1064">v-v:tense-v:pn</ta>
            <ta e="T1066" id="Seg_2550" s="T1065">n-n:case</ta>
            <ta e="T1067" id="Seg_2551" s="T1066">v</ta>
            <ta e="T1068" id="Seg_2552" s="T1067">n-n:num</ta>
            <ta e="T1069" id="Seg_2553" s="T1068">v</ta>
            <ta e="T1070" id="Seg_2554" s="T1069">n-n:num</ta>
            <ta e="T1071" id="Seg_2555" s="T1070">v</ta>
            <ta e="T1072" id="Seg_2556" s="T1071">v-v:ins-v:mood.pn</ta>
            <ta e="T1073" id="Seg_2557" s="T1072">n-n:case</ta>
            <ta e="T1074" id="Seg_2558" s="T1073">n-n&gt;v-v:n.fin</ta>
            <ta e="T1075" id="Seg_2559" s="T1074">dempro-n:case</ta>
            <ta e="T1076" id="Seg_2560" s="T1075">v-v:tense-v:pn</ta>
            <ta e="T1077" id="Seg_2561" s="T1076">adv</ta>
            <ta e="T1078" id="Seg_2562" s="T1077">ptcl</ta>
            <ta e="T1079" id="Seg_2563" s="T1078">num-n:case</ta>
            <ta e="T1080" id="Seg_2564" s="T1079">n-n:case</ta>
            <ta e="T1081" id="Seg_2565" s="T1080">n-n:case</ta>
            <ta e="T1082" id="Seg_2566" s="T1081">v-v:n.fin</ta>
            <ta e="T1083" id="Seg_2567" s="T1082">adv</ta>
            <ta e="T1085" id="Seg_2568" s="T1084">n-n:case</ta>
            <ta e="T1086" id="Seg_2569" s="T1085">v-v:n.fin</ta>
            <ta e="T1087" id="Seg_2570" s="T1086">v-v:tense-v:pn</ta>
            <ta e="T1088" id="Seg_2571" s="T1087">que-n:case</ta>
            <ta e="T1089" id="Seg_2572" s="T1088">pers</ta>
            <ta e="T1090" id="Seg_2573" s="T1089">adv</ta>
            <ta e="T1091" id="Seg_2574" s="T1090">n-n:case</ta>
            <ta e="T1092" id="Seg_2575" s="T1091">dempro-n:case</ta>
            <ta e="T1093" id="Seg_2576" s="T1092">v-v&gt;v-v:pn</ta>
            <ta e="T1094" id="Seg_2577" s="T1093">pers</ta>
            <ta e="T1095" id="Seg_2578" s="T1094">n-n:num</ta>
            <ta e="T1096" id="Seg_2579" s="T1095">n-n:num</ta>
            <ta e="T1097" id="Seg_2580" s="T1096">adv</ta>
            <ta e="T1098" id="Seg_2581" s="T1097">ptcl</ta>
            <ta e="T1099" id="Seg_2582" s="T1098">v</ta>
            <ta e="T1100" id="Seg_2583" s="T1099">n-n:num</ta>
            <ta e="T1101" id="Seg_2584" s="T1100">v-v:ins-v:mood.pn</ta>
            <ta e="T1102" id="Seg_2585" s="T1101">n-n:case.poss</ta>
            <ta e="T1103" id="Seg_2586" s="T1102">adv</ta>
            <ta e="T1104" id="Seg_2587" s="T1103">n-n:num</ta>
            <ta e="T1105" id="Seg_2588" s="T1104">quant</ta>
            <ta e="T1106" id="Seg_2589" s="T1105">dempro-n:case</ta>
            <ta e="T1107" id="Seg_2590" s="T1106">v-v:tense-v:pn</ta>
            <ta e="T1108" id="Seg_2591" s="T1107">n-n:case.poss</ta>
            <ta e="T1109" id="Seg_2592" s="T1108">conj</ta>
            <ta e="T1111" id="Seg_2593" s="T1110">adv</ta>
            <ta e="T1112" id="Seg_2594" s="T1111">n-n:num</ta>
            <ta e="T1113" id="Seg_2595" s="T1112">quant</ta>
            <ta e="T1114" id="Seg_2596" s="T1113">adv</ta>
            <ta e="T1115" id="Seg_2597" s="T1114">n-n:case.poss</ta>
            <ta e="T1116" id="Seg_2598" s="T1115">v-v&gt;v-v:pn</ta>
            <ta e="T1117" id="Seg_2599" s="T1116">v-v:ins-v:mood.pn</ta>
            <ta e="T1118" id="Seg_2600" s="T1117">ptcl</ta>
            <ta e="T1119" id="Seg_2601" s="T1118">v-v:tense-v:pn</ta>
            <ta e="T1120" id="Seg_2602" s="T1119">n-n:case</ta>
            <ta e="T1121" id="Seg_2603" s="T1120">adv</ta>
            <ta e="T1122" id="Seg_2604" s="T1121">v-v:tense-v:pn</ta>
            <ta e="T1123" id="Seg_2605" s="T1122">adv</ta>
            <ta e="T1124" id="Seg_2606" s="T1123">n-n:case</ta>
            <ta e="T1125" id="Seg_2607" s="T1124">v-v:tense-v:pn</ta>
            <ta e="T1126" id="Seg_2608" s="T1125">adv</ta>
            <ta e="T1127" id="Seg_2609" s="T1126">n</ta>
            <ta e="T1128" id="Seg_2610" s="T1127">n-n:case</ta>
            <ta e="T1129" id="Seg_2611" s="T1128">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1130" id="Seg_2612" s="T1129">que-n:case</ta>
            <ta e="T1131" id="Seg_2613" s="T1130">pers</ta>
            <ta e="T1132" id="Seg_2614" s="T1131">adv</ta>
            <ta e="T1133" id="Seg_2615" s="T1132">n-n:case</ta>
            <ta e="T1134" id="Seg_2616" s="T1133">n-n:case</ta>
            <ta e="T1135" id="Seg_2617" s="T1134">v</ta>
            <ta e="T1136" id="Seg_2618" s="T1135">v-v:ins-v:mood.pn</ta>
            <ta e="T1137" id="Seg_2619" s="T1136">n-n:case.poss</ta>
            <ta e="T1138" id="Seg_2620" s="T1137">dempro-n:case</ta>
            <ta e="T1139" id="Seg_2621" s="T1138">v-v:tense-v:pn</ta>
            <ta e="T1140" id="Seg_2622" s="T1139">ptcl</ta>
            <ta e="T1141" id="Seg_2623" s="T1140">n-n:num</ta>
            <ta e="T1142" id="Seg_2624" s="T1141">n-n:num</ta>
            <ta e="T1143" id="Seg_2625" s="T1142">ptcl</ta>
            <ta e="T1144" id="Seg_2626" s="T1143">quant</ta>
            <ta e="T1145" id="Seg_2627" s="T1144">v-v&gt;adj</ta>
            <ta e="T1146" id="Seg_2628" s="T1145">n-n:case</ta>
            <ta e="T1147" id="Seg_2629" s="T1146">conj</ta>
            <ta e="T1148" id="Seg_2630" s="T1147">v-v:n.fin</ta>
            <ta e="T1149" id="Seg_2631" s="T1148">quant</ta>
            <ta e="T1150" id="Seg_2632" s="T1149">v-v:tense-v:pn</ta>
            <ta e="T1151" id="Seg_2633" s="T1150">n-n:case</ta>
            <ta e="T1152" id="Seg_2634" s="T1151">v-v:tense-v:pn</ta>
            <ta e="T1153" id="Seg_2635" s="T1152">v-v:tense-v:pn</ta>
            <ta e="T1154" id="Seg_2636" s="T1153">v-v:ins-v:mood.pn</ta>
            <ta e="T1155" id="Seg_2637" s="T1154">v-v:mood.pn</ta>
            <ta e="T1156" id="Seg_2638" s="T1155">conj</ta>
            <ta e="T1157" id="Seg_2639" s="T1156">pers</ta>
            <ta e="T1159" id="Seg_2640" s="T1158">pers</ta>
            <ta e="T1160" id="Seg_2641" s="T1159">v-v:tense-v:pn</ta>
            <ta e="T1161" id="Seg_2642" s="T1160">n-n:case</ta>
            <ta e="T1162" id="Seg_2643" s="T1161">conj</ta>
            <ta e="T1163" id="Seg_2644" s="T1162">pers</ta>
            <ta e="T1164" id="Seg_2645" s="T1163">n-n:case</ta>
            <ta e="T1165" id="Seg_2646" s="T1164">n-n:case</ta>
            <ta e="T1166" id="Seg_2647" s="T1165">dempro-n:case</ta>
            <ta e="T1167" id="Seg_2648" s="T1166">v-v:tense-v:pn</ta>
            <ta e="T1168" id="Seg_2649" s="T1167">adv</ta>
            <ta e="T1169" id="Seg_2650" s="T1168">n-n:case</ta>
            <ta e="T1170" id="Seg_2651" s="T1169">v-v:tense-v:pn</ta>
            <ta e="T1171" id="Seg_2652" s="T1170">n-n:case</ta>
            <ta e="T1172" id="Seg_2653" s="T1171">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1173" id="Seg_2654" s="T1172">que-n:case</ta>
            <ta e="T1175" id="Seg_2655" s="T1174">pers</ta>
            <ta e="T1176" id="Seg_2656" s="T1175">adv</ta>
            <ta e="T1177" id="Seg_2657" s="T1176">conj</ta>
            <ta e="T1178" id="Seg_2658" s="T1177">pers</ta>
            <ta e="T1180" id="Seg_2659" s="T1179">pers</ta>
            <ta e="T1181" id="Seg_2660" s="T1180">n-n:case</ta>
            <ta e="T1182" id="Seg_2661" s="T1181">v-v:tense-v:pn</ta>
            <ta e="T1183" id="Seg_2662" s="T1182">conj</ta>
            <ta e="T1184" id="Seg_2663" s="T1183">pers</ta>
            <ta e="T1185" id="Seg_2664" s="T1184">n-n:case.poss</ta>
            <ta e="T1186" id="Seg_2665" s="T1185">ptcl</ta>
            <ta e="T1187" id="Seg_2666" s="T1186">pers</ta>
            <ta e="T1188" id="Seg_2667" s="T1187">n-n:case</ta>
            <ta e="T1189" id="Seg_2668" s="T1188">v-v:tense-v:pn</ta>
            <ta e="T1190" id="Seg_2669" s="T1189">v-v:ins-v:mood.pn</ta>
            <ta e="T1191" id="Seg_2670" s="T1190">adv</ta>
            <ta e="T1192" id="Seg_2671" s="T1191">v-v:tense-v:pn</ta>
            <ta e="T1193" id="Seg_2672" s="T1192">v-v:tense-v:pn</ta>
            <ta e="T1194" id="Seg_2673" s="T1193">v-v:tense-v:pn</ta>
            <ta e="T1195" id="Seg_2674" s="T1194">n-n:case.poss</ta>
            <ta e="T1196" id="Seg_2675" s="T1195">conj</ta>
            <ta e="T1197" id="Seg_2676" s="T1196">adv</ta>
            <ta e="T1198" id="Seg_2677" s="T1197">v-v:ins-v:mood.pn</ta>
            <ta e="T1199" id="Seg_2678" s="T1198">v-v:mood.pn</ta>
            <ta e="T1200" id="Seg_2679" s="T1199">conj</ta>
            <ta e="T1201" id="Seg_2680" s="T1200">pers</ta>
            <ta e="T1202" id="Seg_2681" s="T1201">v-v:tense-v:pn</ta>
            <ta e="T1203" id="Seg_2682" s="T1202">ptcl</ta>
            <ta e="T1204" id="Seg_2683" s="T1203">n-n:case</ta>
            <ta e="T1205" id="Seg_2684" s="T1204">conj</ta>
            <ta e="T1206" id="Seg_2685" s="T1205">pers</ta>
            <ta e="T1207" id="Seg_2686" s="T1206">n-n:case</ta>
            <ta e="T1208" id="Seg_2687" s="T1207">v-v:tense-v:pn</ta>
            <ta e="T1209" id="Seg_2688" s="T1208">n-n:case</ta>
            <ta e="T1210" id="Seg_2689" s="T1209">adv</ta>
            <ta e="T1211" id="Seg_2690" s="T1210">ptcl</ta>
            <ta e="T1212" id="Seg_2691" s="T1211">n-n:case</ta>
            <ta e="T1213" id="Seg_2692" s="T1212">v-v:n.fin</ta>
            <ta e="T1214" id="Seg_2693" s="T1213">n-n:case</ta>
            <ta e="T1215" id="Seg_2694" s="T1214">n-n:case</ta>
            <ta e="T1216" id="Seg_2695" s="T1215">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1217" id="Seg_2696" s="T1216">que-n:case</ta>
            <ta e="T1218" id="Seg_2697" s="T1217">pers</ta>
            <ta e="T1219" id="Seg_2698" s="T1218">adv</ta>
            <ta e="T1220" id="Seg_2699" s="T1219">n-n:case</ta>
            <ta e="T1221" id="Seg_2700" s="T1220">conj</ta>
            <ta e="T1222" id="Seg_2701" s="T1221">pers</ta>
            <ta e="T1223" id="Seg_2702" s="T1222">conj</ta>
            <ta e="T1224" id="Seg_2703" s="T1223">v-v:tense-v:pn</ta>
            <ta e="T1225" id="Seg_2704" s="T1224">n-n:case</ta>
            <ta e="T1226" id="Seg_2705" s="T1225">conj</ta>
            <ta e="T1227" id="Seg_2706" s="T1226">pers</ta>
            <ta e="T1228" id="Seg_2707" s="T1227">n-n:case.poss</ta>
            <ta e="T1229" id="Seg_2708" s="T1228">n</ta>
            <ta e="T1230" id="Seg_2709" s="T1229">v-v:ins-v:mood.pn</ta>
            <ta e="T1231" id="Seg_2710" s="T1230">dempro-n:case</ta>
            <ta e="T1233" id="Seg_2711" s="T1232">v-v:tense-v:pn</ta>
            <ta e="T1234" id="Seg_2712" s="T1233">n-n:case</ta>
            <ta e="T1235" id="Seg_2713" s="T1234">quant</ta>
            <ta e="T1237" id="Seg_2714" s="T1235">n-n:num</ta>
            <ta e="T1238" id="Seg_2715" s="T1237">quant</ta>
            <ta e="T1239" id="Seg_2716" s="T1238">v-v&gt;v-v:pn</ta>
            <ta e="T1240" id="Seg_2717" s="T1239">dempro-n:case</ta>
            <ta e="T1241" id="Seg_2718" s="T1240">adv</ta>
            <ta e="T1242" id="Seg_2719" s="T1241">v-v:tense-v:pn</ta>
            <ta e="T1243" id="Seg_2720" s="T1242">v-v:tense-v:pn</ta>
            <ta e="T1244" id="Seg_2721" s="T1243">v-v:tense-v:pn</ta>
            <ta e="T1245" id="Seg_2722" s="T1244">adv</ta>
            <ta e="T1246" id="Seg_2723" s="T1245">n-n:case</ta>
            <ta e="T1247" id="Seg_2724" s="T1246">adv</ta>
            <ta e="T1248" id="Seg_2725" s="T1247">v-v&gt;v-v:pn</ta>
            <ta e="T1249" id="Seg_2726" s="T1248">dempro-n:case</ta>
            <ta e="T1250" id="Seg_2727" s="T1249">n-n:case</ta>
            <ta e="T1251" id="Seg_2728" s="T1250">v-v:ins-v:mood.pn</ta>
            <ta e="T1252" id="Seg_2729" s="T1251">conj</ta>
            <ta e="T1253" id="Seg_2730" s="T1252">pers</ta>
            <ta e="T1254" id="Seg_2731" s="T1253">n-n:case</ta>
            <ta e="T1255" id="Seg_2732" s="T1254">v-v:tense-v:pn</ta>
            <ta e="T1256" id="Seg_2733" s="T1255">conj</ta>
            <ta e="T1257" id="Seg_2734" s="T1256">pers</ta>
            <ta e="T1258" id="Seg_2735" s="T1257">n-n:ins-n:case</ta>
            <ta e="T1259" id="Seg_2736" s="T1258">n-n:case.poss</ta>
            <ta e="T1260" id="Seg_2737" s="T1259">dempro-n:case</ta>
            <ta e="T1261" id="Seg_2738" s="T1260">v-v:tense-v:pn</ta>
            <ta e="T1262" id="Seg_2739" s="T1261">ptcl</ta>
            <ta e="T1263" id="Seg_2740" s="T1262">adv</ta>
            <ta e="T1264" id="Seg_2741" s="T1263">n-n:case</ta>
            <ta e="T1265" id="Seg_2742" s="T1264">v-v:n.fin</ta>
            <ta e="T1266" id="Seg_2743" s="T1265">dempro-n:case</ta>
            <ta e="T1267" id="Seg_2744" s="T1266">n-n:case</ta>
            <ta e="T1268" id="Seg_2745" s="T1267">dempro-n:case</ta>
            <ta e="T1269" id="Seg_2746" s="T1268">n-n:case</ta>
            <ta e="T1270" id="Seg_2747" s="T1269">v-v:n.fin</ta>
            <ta e="T1271" id="Seg_2748" s="T1270">v-v:tense-v:pn</ta>
            <ta e="T1272" id="Seg_2749" s="T1271">que-n:case</ta>
            <ta e="T1273" id="Seg_2750" s="T1272">pers</ta>
            <ta e="T1274" id="Seg_2751" s="T1273">adv</ta>
            <ta e="T1275" id="Seg_2752" s="T1274">conj</ta>
            <ta e="T1276" id="Seg_2753" s="T1275">pers</ta>
            <ta e="T1277" id="Seg_2754" s="T1276">n-n:case</ta>
            <ta e="T1278" id="Seg_2755" s="T1277">n-n:case</ta>
            <ta e="T1279" id="Seg_2756" s="T1278">v-v:tense-v:pn</ta>
            <ta e="T1280" id="Seg_2757" s="T1279">conj</ta>
            <ta e="T1281" id="Seg_2758" s="T1280">pers</ta>
            <ta e="T1282" id="Seg_2759" s="T1281">n-n:case.poss</ta>
            <ta e="T1283" id="Seg_2760" s="T1282">n-n:ins-n:case</ta>
            <ta e="T1284" id="Seg_2761" s="T1283">n-n:case.poss</ta>
            <ta e="T1285" id="Seg_2762" s="T1284">v-v:ins-v:mood.pn</ta>
            <ta e="T1286" id="Seg_2763" s="T1285">pers</ta>
            <ta e="T1287" id="Seg_2764" s="T1286">n-n:case</ta>
            <ta e="T1288" id="Seg_2765" s="T1287">v-v:tense-v:pn</ta>
            <ta e="T1289" id="Seg_2766" s="T1288">conj</ta>
            <ta e="T1290" id="Seg_2767" s="T1289">pers</ta>
            <ta e="T1291" id="Seg_2768" s="T1290">n-n:case</ta>
            <ta e="T1292" id="Seg_2769" s="T1291">n-n:case</ta>
            <ta e="T1293" id="Seg_2770" s="T1292">v-v:tense-v:pn</ta>
            <ta e="T1294" id="Seg_2771" s="T1293">adv</ta>
            <ta e="T1295" id="Seg_2772" s="T1294">v-v:tense-v:pn</ta>
            <ta e="T1296" id="Seg_2773" s="T1295">dempro-n:case</ta>
            <ta e="T1297" id="Seg_2774" s="T1296">n-n:case</ta>
            <ta e="T1298" id="Seg_2775" s="T1297">n-n:case</ta>
            <ta e="T1299" id="Seg_2776" s="T1298">conj</ta>
            <ta e="T1300" id="Seg_2777" s="T1299">n-n:case.poss</ta>
            <ta e="T1301" id="Seg_2778" s="T1300">n-n:case</ta>
            <ta e="T1302" id="Seg_2779" s="T1301">conj</ta>
            <ta e="T1303" id="Seg_2780" s="T1302">ptcl</ta>
            <ta e="T1304" id="Seg_2781" s="T1303">v-v&gt;v-v:pn</ta>
            <ta e="T1305" id="Seg_2782" s="T1304">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1058" id="Seg_2783" s="T1057">v</ta>
            <ta e="T1059" id="Seg_2784" s="T1058">n</ta>
            <ta e="T1060" id="Seg_2785" s="T1059">n</ta>
            <ta e="T1061" id="Seg_2786" s="T1060">adv</ta>
            <ta e="T1062" id="Seg_2787" s="T1061">dempro</ta>
            <ta e="T1063" id="Seg_2788" s="T1062">que</ta>
            <ta e="T1064" id="Seg_2789" s="T1063">ptcl</ta>
            <ta e="T1065" id="Seg_2790" s="T1064">v</ta>
            <ta e="T1066" id="Seg_2791" s="T1065">n</ta>
            <ta e="T1067" id="Seg_2792" s="T1066">v</ta>
            <ta e="T1068" id="Seg_2793" s="T1067">n</ta>
            <ta e="T1069" id="Seg_2794" s="T1068">v</ta>
            <ta e="T1070" id="Seg_2795" s="T1069">n</ta>
            <ta e="T1071" id="Seg_2796" s="T1070">v</ta>
            <ta e="T1072" id="Seg_2797" s="T1071">v</ta>
            <ta e="T1073" id="Seg_2798" s="T1072">n</ta>
            <ta e="T1074" id="Seg_2799" s="T1073">v</ta>
            <ta e="T1075" id="Seg_2800" s="T1074">dempro</ta>
            <ta e="T1076" id="Seg_2801" s="T1075">v</ta>
            <ta e="T1077" id="Seg_2802" s="T1076">adv</ta>
            <ta e="T1078" id="Seg_2803" s="T1077">ptcl</ta>
            <ta e="T1079" id="Seg_2804" s="T1078">num</ta>
            <ta e="T1080" id="Seg_2805" s="T1079">n</ta>
            <ta e="T1081" id="Seg_2806" s="T1080">n</ta>
            <ta e="T1082" id="Seg_2807" s="T1081">v</ta>
            <ta e="T1083" id="Seg_2808" s="T1082">adv</ta>
            <ta e="T1085" id="Seg_2809" s="T1084">n</ta>
            <ta e="T1086" id="Seg_2810" s="T1085">v</ta>
            <ta e="T1087" id="Seg_2811" s="T1086">v</ta>
            <ta e="T1088" id="Seg_2812" s="T1087">que</ta>
            <ta e="T1089" id="Seg_2813" s="T1088">pers</ta>
            <ta e="T1090" id="Seg_2814" s="T1089">adv</ta>
            <ta e="T1091" id="Seg_2815" s="T1090">n</ta>
            <ta e="T1092" id="Seg_2816" s="T1091">dempro</ta>
            <ta e="T1093" id="Seg_2817" s="T1092">v</ta>
            <ta e="T1094" id="Seg_2818" s="T1093">pers</ta>
            <ta e="T1095" id="Seg_2819" s="T1094">n</ta>
            <ta e="T1096" id="Seg_2820" s="T1095">n</ta>
            <ta e="T1097" id="Seg_2821" s="T1096">adv</ta>
            <ta e="T1098" id="Seg_2822" s="T1097">ptcl</ta>
            <ta e="T1099" id="Seg_2823" s="T1098">v</ta>
            <ta e="T1100" id="Seg_2824" s="T1099">n</ta>
            <ta e="T1101" id="Seg_2825" s="T1100">v</ta>
            <ta e="T1102" id="Seg_2826" s="T1101">n</ta>
            <ta e="T1103" id="Seg_2827" s="T1102">adv</ta>
            <ta e="T1104" id="Seg_2828" s="T1103">n</ta>
            <ta e="T1105" id="Seg_2829" s="T1104">quant</ta>
            <ta e="T1106" id="Seg_2830" s="T1105">dempro</ta>
            <ta e="T1107" id="Seg_2831" s="T1106">v</ta>
            <ta e="T1108" id="Seg_2832" s="T1107">n</ta>
            <ta e="T1109" id="Seg_2833" s="T1108">conj</ta>
            <ta e="T1111" id="Seg_2834" s="T1110">adv</ta>
            <ta e="T1112" id="Seg_2835" s="T1111">n</ta>
            <ta e="T1113" id="Seg_2836" s="T1112">quant</ta>
            <ta e="T1114" id="Seg_2837" s="T1113">adv</ta>
            <ta e="T1115" id="Seg_2838" s="T1114">n</ta>
            <ta e="T1116" id="Seg_2839" s="T1115">v</ta>
            <ta e="T1117" id="Seg_2840" s="T1116">v</ta>
            <ta e="T1118" id="Seg_2841" s="T1117">ptcl</ta>
            <ta e="T1119" id="Seg_2842" s="T1118">v</ta>
            <ta e="T1120" id="Seg_2843" s="T1119">n</ta>
            <ta e="T1121" id="Seg_2844" s="T1120">adv</ta>
            <ta e="T1122" id="Seg_2845" s="T1121">v</ta>
            <ta e="T1123" id="Seg_2846" s="T1122">adv</ta>
            <ta e="T1124" id="Seg_2847" s="T1123">n</ta>
            <ta e="T1125" id="Seg_2848" s="T1124">v</ta>
            <ta e="T1126" id="Seg_2849" s="T1125">adv</ta>
            <ta e="T1127" id="Seg_2850" s="T1126">n</ta>
            <ta e="T1128" id="Seg_2851" s="T1127">n</ta>
            <ta e="T1129" id="Seg_2852" s="T1128">v</ta>
            <ta e="T1130" id="Seg_2853" s="T1129">que</ta>
            <ta e="T1131" id="Seg_2854" s="T1130">pers</ta>
            <ta e="T1132" id="Seg_2855" s="T1131">adv</ta>
            <ta e="T1133" id="Seg_2856" s="T1132">n</ta>
            <ta e="T1134" id="Seg_2857" s="T1133">n</ta>
            <ta e="T1135" id="Seg_2858" s="T1134">v</ta>
            <ta e="T1136" id="Seg_2859" s="T1135">v</ta>
            <ta e="T1137" id="Seg_2860" s="T1136">n</ta>
            <ta e="T1138" id="Seg_2861" s="T1137">dempro</ta>
            <ta e="T1139" id="Seg_2862" s="T1138">v</ta>
            <ta e="T1140" id="Seg_2863" s="T1139">ptcl</ta>
            <ta e="T1141" id="Seg_2864" s="T1140">n</ta>
            <ta e="T1142" id="Seg_2865" s="T1141">n</ta>
            <ta e="T1143" id="Seg_2866" s="T1142">ptcl</ta>
            <ta e="T1144" id="Seg_2867" s="T1143">quant</ta>
            <ta e="T1145" id="Seg_2868" s="T1144">adj</ta>
            <ta e="T1146" id="Seg_2869" s="T1145">n</ta>
            <ta e="T1147" id="Seg_2870" s="T1146">conj</ta>
            <ta e="T1148" id="Seg_2871" s="T1147">v</ta>
            <ta e="T1149" id="Seg_2872" s="T1148">quant</ta>
            <ta e="T1150" id="Seg_2873" s="T1149">v</ta>
            <ta e="T1151" id="Seg_2874" s="T1150">n</ta>
            <ta e="T1152" id="Seg_2875" s="T1151">v</ta>
            <ta e="T1153" id="Seg_2876" s="T1152">v</ta>
            <ta e="T1154" id="Seg_2877" s="T1153">v</ta>
            <ta e="T1155" id="Seg_2878" s="T1154">v</ta>
            <ta e="T1156" id="Seg_2879" s="T1155">conj</ta>
            <ta e="T1157" id="Seg_2880" s="T1156">pers</ta>
            <ta e="T1159" id="Seg_2881" s="T1158">pers</ta>
            <ta e="T1160" id="Seg_2882" s="T1159">v</ta>
            <ta e="T1161" id="Seg_2883" s="T1160">n</ta>
            <ta e="T1162" id="Seg_2884" s="T1161">conj</ta>
            <ta e="T1163" id="Seg_2885" s="T1162">pers</ta>
            <ta e="T1164" id="Seg_2886" s="T1163">n</ta>
            <ta e="T1165" id="Seg_2887" s="T1164">n</ta>
            <ta e="T1166" id="Seg_2888" s="T1165">dempro</ta>
            <ta e="T1167" id="Seg_2889" s="T1166">v</ta>
            <ta e="T1168" id="Seg_2890" s="T1167">adv</ta>
            <ta e="T1169" id="Seg_2891" s="T1168">n</ta>
            <ta e="T1170" id="Seg_2892" s="T1169">v</ta>
            <ta e="T1171" id="Seg_2893" s="T1170">n</ta>
            <ta e="T1172" id="Seg_2894" s="T1171">v</ta>
            <ta e="T1173" id="Seg_2895" s="T1172">que</ta>
            <ta e="T1175" id="Seg_2896" s="T1174">pers</ta>
            <ta e="T1176" id="Seg_2897" s="T1175">adv</ta>
            <ta e="T1177" id="Seg_2898" s="T1176">conj</ta>
            <ta e="T1178" id="Seg_2899" s="T1177">pers</ta>
            <ta e="T1180" id="Seg_2900" s="T1179">pers</ta>
            <ta e="T1181" id="Seg_2901" s="T1180">n</ta>
            <ta e="T1182" id="Seg_2902" s="T1181">v</ta>
            <ta e="T1183" id="Seg_2903" s="T1182">conj</ta>
            <ta e="T1184" id="Seg_2904" s="T1183">pers</ta>
            <ta e="T1185" id="Seg_2905" s="T1184">n</ta>
            <ta e="T1186" id="Seg_2906" s="T1185">ptcl</ta>
            <ta e="T1187" id="Seg_2907" s="T1186">pers</ta>
            <ta e="T1188" id="Seg_2908" s="T1187">n</ta>
            <ta e="T1189" id="Seg_2909" s="T1188">v</ta>
            <ta e="T1190" id="Seg_2910" s="T1189">v</ta>
            <ta e="T1191" id="Seg_2911" s="T1190">adv</ta>
            <ta e="T1192" id="Seg_2912" s="T1191">v</ta>
            <ta e="T1193" id="Seg_2913" s="T1192">v</ta>
            <ta e="T1194" id="Seg_2914" s="T1193">v</ta>
            <ta e="T1195" id="Seg_2915" s="T1194">n</ta>
            <ta e="T1196" id="Seg_2916" s="T1195">conj</ta>
            <ta e="T1197" id="Seg_2917" s="T1196">adv</ta>
            <ta e="T1198" id="Seg_2918" s="T1197">v</ta>
            <ta e="T1199" id="Seg_2919" s="T1198">v</ta>
            <ta e="T1200" id="Seg_2920" s="T1199">conj</ta>
            <ta e="T1201" id="Seg_2921" s="T1200">pers</ta>
            <ta e="T1202" id="Seg_2922" s="T1201">v</ta>
            <ta e="T1203" id="Seg_2923" s="T1202">ptcl</ta>
            <ta e="T1204" id="Seg_2924" s="T1203">n</ta>
            <ta e="T1205" id="Seg_2925" s="T1204">conj</ta>
            <ta e="T1206" id="Seg_2926" s="T1205">pers</ta>
            <ta e="T1207" id="Seg_2927" s="T1206">n</ta>
            <ta e="T1208" id="Seg_2928" s="T1207">v</ta>
            <ta e="T1209" id="Seg_2929" s="T1208">n</ta>
            <ta e="T1210" id="Seg_2930" s="T1209">adv</ta>
            <ta e="T1211" id="Seg_2931" s="T1210">ptcl</ta>
            <ta e="T1212" id="Seg_2932" s="T1211">n</ta>
            <ta e="T1213" id="Seg_2933" s="T1212">v</ta>
            <ta e="T1214" id="Seg_2934" s="T1213">n</ta>
            <ta e="T1215" id="Seg_2935" s="T1214">n</ta>
            <ta e="T1216" id="Seg_2936" s="T1215">v</ta>
            <ta e="T1217" id="Seg_2937" s="T1216">que</ta>
            <ta e="T1218" id="Seg_2938" s="T1217">pers</ta>
            <ta e="T1219" id="Seg_2939" s="T1218">adv</ta>
            <ta e="T1220" id="Seg_2940" s="T1219">n</ta>
            <ta e="T1221" id="Seg_2941" s="T1220">conj</ta>
            <ta e="T1222" id="Seg_2942" s="T1221">pers</ta>
            <ta e="T1223" id="Seg_2943" s="T1222">conj</ta>
            <ta e="T1224" id="Seg_2944" s="T1223">v</ta>
            <ta e="T1225" id="Seg_2945" s="T1224">n</ta>
            <ta e="T1226" id="Seg_2946" s="T1225">conj</ta>
            <ta e="T1227" id="Seg_2947" s="T1226">pers</ta>
            <ta e="T1228" id="Seg_2948" s="T1227">n</ta>
            <ta e="T1229" id="Seg_2949" s="T1228">n</ta>
            <ta e="T1230" id="Seg_2950" s="T1229">v</ta>
            <ta e="T1231" id="Seg_2951" s="T1230">dempro</ta>
            <ta e="T1233" id="Seg_2952" s="T1232">v</ta>
            <ta e="T1234" id="Seg_2953" s="T1233">n</ta>
            <ta e="T1235" id="Seg_2954" s="T1234">quant</ta>
            <ta e="T1237" id="Seg_2955" s="T1235">n</ta>
            <ta e="T1238" id="Seg_2956" s="T1237">quant</ta>
            <ta e="T1239" id="Seg_2957" s="T1238">v</ta>
            <ta e="T1240" id="Seg_2958" s="T1239">dempro</ta>
            <ta e="T1241" id="Seg_2959" s="T1240">adv</ta>
            <ta e="T1242" id="Seg_2960" s="T1241">v</ta>
            <ta e="T1243" id="Seg_2961" s="T1242">v</ta>
            <ta e="T1244" id="Seg_2962" s="T1243">v</ta>
            <ta e="T1245" id="Seg_2963" s="T1244">adv</ta>
            <ta e="T1246" id="Seg_2964" s="T1245">n</ta>
            <ta e="T1247" id="Seg_2965" s="T1246">adv</ta>
            <ta e="T1248" id="Seg_2966" s="T1247">v</ta>
            <ta e="T1249" id="Seg_2967" s="T1248">dempro</ta>
            <ta e="T1250" id="Seg_2968" s="T1249">n</ta>
            <ta e="T1251" id="Seg_2969" s="T1250">v</ta>
            <ta e="T1252" id="Seg_2970" s="T1251">conj</ta>
            <ta e="T1253" id="Seg_2971" s="T1252">pers</ta>
            <ta e="T1254" id="Seg_2972" s="T1253">n</ta>
            <ta e="T1255" id="Seg_2973" s="T1254">v</ta>
            <ta e="T1256" id="Seg_2974" s="T1255">conj</ta>
            <ta e="T1257" id="Seg_2975" s="T1256">pers</ta>
            <ta e="T1258" id="Seg_2976" s="T1257">n</ta>
            <ta e="T1259" id="Seg_2977" s="T1258">n</ta>
            <ta e="T1260" id="Seg_2978" s="T1259">dempro</ta>
            <ta e="T1261" id="Seg_2979" s="T1260">v</ta>
            <ta e="T1262" id="Seg_2980" s="T1261">ptcl</ta>
            <ta e="T1263" id="Seg_2981" s="T1262">adv</ta>
            <ta e="T1264" id="Seg_2982" s="T1263">n</ta>
            <ta e="T1265" id="Seg_2983" s="T1264">v</ta>
            <ta e="T1266" id="Seg_2984" s="T1265">dempro</ta>
            <ta e="T1267" id="Seg_2985" s="T1266">n</ta>
            <ta e="T1268" id="Seg_2986" s="T1267">dempro</ta>
            <ta e="T1269" id="Seg_2987" s="T1268">n</ta>
            <ta e="T1270" id="Seg_2988" s="T1269">v</ta>
            <ta e="T1271" id="Seg_2989" s="T1270">v</ta>
            <ta e="T1272" id="Seg_2990" s="T1271">que</ta>
            <ta e="T1273" id="Seg_2991" s="T1272">pers</ta>
            <ta e="T1274" id="Seg_2992" s="T1273">adv</ta>
            <ta e="T1275" id="Seg_2993" s="T1274">conj</ta>
            <ta e="T1276" id="Seg_2994" s="T1275">pers</ta>
            <ta e="T1277" id="Seg_2995" s="T1276">n</ta>
            <ta e="T1278" id="Seg_2996" s="T1277">n</ta>
            <ta e="T1279" id="Seg_2997" s="T1278">v</ta>
            <ta e="T1280" id="Seg_2998" s="T1279">conj</ta>
            <ta e="T1281" id="Seg_2999" s="T1280">pers</ta>
            <ta e="T1282" id="Seg_3000" s="T1281">n</ta>
            <ta e="T1283" id="Seg_3001" s="T1282">n</ta>
            <ta e="T1284" id="Seg_3002" s="T1283">n</ta>
            <ta e="T1285" id="Seg_3003" s="T1284">v</ta>
            <ta e="T1286" id="Seg_3004" s="T1285">pers</ta>
            <ta e="T1287" id="Seg_3005" s="T1286">n</ta>
            <ta e="T1288" id="Seg_3006" s="T1287">v</ta>
            <ta e="T1289" id="Seg_3007" s="T1288">conj</ta>
            <ta e="T1290" id="Seg_3008" s="T1289">pers</ta>
            <ta e="T1291" id="Seg_3009" s="T1290">n</ta>
            <ta e="T1292" id="Seg_3010" s="T1291">n</ta>
            <ta e="T1293" id="Seg_3011" s="T1292">v</ta>
            <ta e="T1294" id="Seg_3012" s="T1293">adv</ta>
            <ta e="T1295" id="Seg_3013" s="T1294">v</ta>
            <ta e="T1296" id="Seg_3014" s="T1295">dempro</ta>
            <ta e="T1297" id="Seg_3015" s="T1296">n</ta>
            <ta e="T1298" id="Seg_3016" s="T1297">n</ta>
            <ta e="T1299" id="Seg_3017" s="T1298">conj</ta>
            <ta e="T1300" id="Seg_3018" s="T1299">n</ta>
            <ta e="T1301" id="Seg_3019" s="T1300">n</ta>
            <ta e="T1302" id="Seg_3020" s="T1301">conj</ta>
            <ta e="T1303" id="Seg_3021" s="T1302">ptcl</ta>
            <ta e="T1304" id="Seg_3022" s="T1303">v</ta>
            <ta e="T1305" id="Seg_3023" s="T1304">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1059" id="Seg_3024" s="T1058">np.h:E</ta>
            <ta e="T1060" id="Seg_3025" s="T1059">np.h:E</ta>
            <ta e="T1061" id="Seg_3026" s="T1060">adv:L</ta>
            <ta e="T1062" id="Seg_3027" s="T1061">pro.h:Poss</ta>
            <ta e="T1063" id="Seg_3028" s="T1062">pro:Th</ta>
            <ta e="T1066" id="Seg_3029" s="T1065">np:Th</ta>
            <ta e="T1068" id="Seg_3030" s="T1067">np:Th</ta>
            <ta e="T1070" id="Seg_3031" s="T1069">np:Th</ta>
            <ta e="T1072" id="Seg_3032" s="T1071">0.2.h:A</ta>
            <ta e="T1073" id="Seg_3033" s="T1072">np:G</ta>
            <ta e="T1075" id="Seg_3034" s="T1074">pro.h:A</ta>
            <ta e="T1077" id="Seg_3035" s="T1076">adv:L</ta>
            <ta e="T1080" id="Seg_3036" s="T1079">np:P</ta>
            <ta e="T1081" id="Seg_3037" s="T1080">np:Ins</ta>
            <ta e="T1083" id="Seg_3038" s="T1082">adv:Time</ta>
            <ta e="T1085" id="Seg_3039" s="T1084">np.h:A</ta>
            <ta e="T1088" id="Seg_3040" s="T1087">pro:Th</ta>
            <ta e="T1089" id="Seg_3041" s="T1088">pro.h:B</ta>
            <ta e="T1092" id="Seg_3042" s="T1091">pro.h:A</ta>
            <ta e="T1094" id="Seg_3043" s="T1093">pro.h:B</ta>
            <ta e="T1095" id="Seg_3044" s="T1094">np:Th</ta>
            <ta e="T1096" id="Seg_3045" s="T1095">np:Th</ta>
            <ta e="T1100" id="Seg_3046" s="T1099">np:Th</ta>
            <ta e="T1101" id="Seg_3047" s="T1100">0.2.h:A</ta>
            <ta e="T1102" id="Seg_3048" s="T1101">np:G</ta>
            <ta e="T1103" id="Seg_3049" s="T1102">adv:L</ta>
            <ta e="T1104" id="Seg_3050" s="T1103">np:Th</ta>
            <ta e="T1106" id="Seg_3051" s="T1105">pro.h:A</ta>
            <ta e="T1108" id="Seg_3052" s="T1107">np:G</ta>
            <ta e="T1112" id="Seg_3053" s="T1111">np:Th</ta>
            <ta e="T1114" id="Seg_3054" s="T1113">adv:Time</ta>
            <ta e="T1115" id="Seg_3055" s="T1114">np.h:A</ta>
            <ta e="T1117" id="Seg_3056" s="T1116">0.2.h:A</ta>
            <ta e="T1119" id="Seg_3057" s="T1118">0.3.h:A</ta>
            <ta e="T1120" id="Seg_3058" s="T1119">np:Th</ta>
            <ta e="T1121" id="Seg_3059" s="T1120">adv:Time</ta>
            <ta e="T1122" id="Seg_3060" s="T1121">0.3.h:A</ta>
            <ta e="T1124" id="Seg_3061" s="T1123">np:Ins</ta>
            <ta e="T1125" id="Seg_3062" s="T1124">0.3.h:A</ta>
            <ta e="T1128" id="Seg_3063" s="T1127">np.h:A</ta>
            <ta e="T1130" id="Seg_3064" s="T1129">pro:Th</ta>
            <ta e="T1131" id="Seg_3065" s="T1130">pro.h:B</ta>
            <ta e="T1134" id="Seg_3066" s="T1133">np:Th</ta>
            <ta e="T1136" id="Seg_3067" s="T1135">0.2.h:A</ta>
            <ta e="T1137" id="Seg_3068" s="T1136">np:G</ta>
            <ta e="T1138" id="Seg_3069" s="T1137">pro.h:A</ta>
            <ta e="T1141" id="Seg_3070" s="T1140">np:Th</ta>
            <ta e="T1142" id="Seg_3071" s="T1141">np:Th</ta>
            <ta e="T1150" id="Seg_3072" s="T1149">0.3:Th</ta>
            <ta e="T1151" id="Seg_3073" s="T1150">np.h:A</ta>
            <ta e="T1154" id="Seg_3074" s="T1153">0.2.h:A</ta>
            <ta e="T1155" id="Seg_3075" s="T1154">0.2.h:A</ta>
            <ta e="T1159" id="Seg_3076" s="T1158">pro.h:Th</ta>
            <ta e="T1161" id="Seg_3077" s="T1160">np:Th</ta>
            <ta e="T1163" id="Seg_3078" s="T1162">pro.h:Th</ta>
            <ta e="T1164" id="Seg_3079" s="T1163">np.h:Poss</ta>
            <ta e="T1165" id="Seg_3080" s="T1164">np:Th</ta>
            <ta e="T1166" id="Seg_3081" s="T1165">pro.h:A</ta>
            <ta e="T1169" id="Seg_3082" s="T1168">np:Ins</ta>
            <ta e="T1170" id="Seg_3083" s="T1169">0.3.h:A</ta>
            <ta e="T1171" id="Seg_3084" s="T1170">np.h:A</ta>
            <ta e="T1173" id="Seg_3085" s="T1172">pro:Th</ta>
            <ta e="T1175" id="Seg_3086" s="T1174">pro.h:B</ta>
            <ta e="T1178" id="Seg_3087" s="T1177">pro.h:Th</ta>
            <ta e="T1180" id="Seg_3088" s="T1179">pro.h:Th</ta>
            <ta e="T1181" id="Seg_3089" s="T1180">np:Th</ta>
            <ta e="T1184" id="Seg_3090" s="T1183">pro.h:Poss</ta>
            <ta e="T1185" id="Seg_3091" s="T1184">np.h:Th</ta>
            <ta e="T1187" id="Seg_3092" s="T1186">pro.h:Poss</ta>
            <ta e="T1188" id="Seg_3093" s="T1187">np:Th</ta>
            <ta e="T1190" id="Seg_3094" s="T1189">0.2.h:A</ta>
            <ta e="T1191" id="Seg_3095" s="T1190">adv:Time</ta>
            <ta e="T1192" id="Seg_3096" s="T1191">0.3.h:A</ta>
            <ta e="T1193" id="Seg_3097" s="T1192">0.3.h:A</ta>
            <ta e="T1194" id="Seg_3098" s="T1193">0.3.h:A</ta>
            <ta e="T1195" id="Seg_3099" s="T1194">np.h:A</ta>
            <ta e="T1198" id="Seg_3100" s="T1197">0.2.h:A</ta>
            <ta e="T1199" id="Seg_3101" s="T1198">0.2.h:A</ta>
            <ta e="T1201" id="Seg_3102" s="T1200">pro.h:Th</ta>
            <ta e="T1204" id="Seg_3103" s="T1203">np:Th</ta>
            <ta e="T1206" id="Seg_3104" s="T1205">pro.h:Th</ta>
            <ta e="T1207" id="Seg_3105" s="T1206">np:Th</ta>
            <ta e="T1208" id="Seg_3106" s="T1207">0.3.h:A</ta>
            <ta e="T1212" id="Seg_3107" s="T1211">np:Ins</ta>
            <ta e="T1214" id="Seg_3108" s="T1213">np:P</ta>
            <ta e="T1215" id="Seg_3109" s="T1214">np.h:A</ta>
            <ta e="T1217" id="Seg_3110" s="T1216">pro:Th</ta>
            <ta e="T1218" id="Seg_3111" s="T1217">pro.h:B</ta>
            <ta e="T1222" id="Seg_3112" s="T1221">pro.h:Th</ta>
            <ta e="T1225" id="Seg_3113" s="T1224">np:Th</ta>
            <ta e="T1227" id="Seg_3114" s="T1226">pro.h:Poss</ta>
            <ta e="T1228" id="Seg_3115" s="T1227">np.h:Th</ta>
            <ta e="T1229" id="Seg_3116" s="T1228">np:Th</ta>
            <ta e="T1230" id="Seg_3117" s="T1229">0.2.h:A</ta>
            <ta e="T1231" id="Seg_3118" s="T1230">pro.h:A</ta>
            <ta e="T1234" id="Seg_3119" s="T1233">np.h:A</ta>
            <ta e="T1237" id="Seg_3120" s="T1235">np:Th</ta>
            <ta e="T1239" id="Seg_3121" s="T1238">0.3.h:A</ta>
            <ta e="T1240" id="Seg_3122" s="T1239">pro.h:B</ta>
            <ta e="T1241" id="Seg_3123" s="T1240">adv:Time</ta>
            <ta e="T1242" id="Seg_3124" s="T1241">0.3.h:A</ta>
            <ta e="T1243" id="Seg_3125" s="T1242">0.3.h:E</ta>
            <ta e="T1244" id="Seg_3126" s="T1243">0.3.h:E</ta>
            <ta e="T1245" id="Seg_3127" s="T1244">adv:Time</ta>
            <ta e="T1246" id="Seg_3128" s="T1245">np.h:A</ta>
            <ta e="T1250" id="Seg_3129" s="T1249">np.h:Th</ta>
            <ta e="T1251" id="Seg_3130" s="T1250">0.2.h:A</ta>
            <ta e="T1253" id="Seg_3131" s="T1252">pro.h:Th</ta>
            <ta e="T1254" id="Seg_3132" s="T1253">np:Th</ta>
            <ta e="T1257" id="Seg_3133" s="T1256">pro.h:Th</ta>
            <ta e="T1258" id="Seg_3134" s="T1257">np:Poss</ta>
            <ta e="T1259" id="Seg_3135" s="T1258">np:Th</ta>
            <ta e="T1260" id="Seg_3136" s="T1259">pro.h:A</ta>
            <ta e="T1264" id="Seg_3137" s="T1263">np:Ins</ta>
            <ta e="T1267" id="Seg_3138" s="T1266">np:P</ta>
            <ta e="T1269" id="Seg_3139" s="T1268">np.h:A</ta>
            <ta e="T1272" id="Seg_3140" s="T1271">pro:Th</ta>
            <ta e="T1273" id="Seg_3141" s="T1272">pro.h:B</ta>
            <ta e="T1276" id="Seg_3142" s="T1275">pro.h:Th</ta>
            <ta e="T1278" id="Seg_3143" s="T1277">np:Th</ta>
            <ta e="T1281" id="Seg_3144" s="T1280">pro.h:Poss</ta>
            <ta e="T1282" id="Seg_3145" s="T1281">np.h:Th</ta>
            <ta e="T1283" id="Seg_3146" s="T1282">np:Poss</ta>
            <ta e="T1284" id="Seg_3147" s="T1283">np:Th</ta>
            <ta e="T1285" id="Seg_3148" s="T1284">0.2.h:A</ta>
            <ta e="T1286" id="Seg_3149" s="T1285">pro.h:Th</ta>
            <ta e="T1287" id="Seg_3150" s="T1286">np:Th</ta>
            <ta e="T1290" id="Seg_3151" s="T1289">pro.h:Poss</ta>
            <ta e="T1291" id="Seg_3152" s="T1290">np.h:Th</ta>
            <ta e="T1292" id="Seg_3153" s="T1291">np:Th</ta>
            <ta e="T1294" id="Seg_3154" s="T1293">adv:Time</ta>
            <ta e="T1295" id="Seg_3155" s="T1294">0.3.h:A</ta>
            <ta e="T1297" id="Seg_3156" s="T1296">np.h:Th</ta>
            <ta e="T1298" id="Seg_3157" s="T1297">np:Th</ta>
            <ta e="T1300" id="Seg_3158" s="T1299">np.h:Th</ta>
            <ta e="T1301" id="Seg_3159" s="T1300">np:Th</ta>
            <ta e="T1304" id="Seg_3160" s="T1303">0.3.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1058" id="Seg_3161" s="T1057">v:pred</ta>
            <ta e="T1059" id="Seg_3162" s="T1058">np.h:S</ta>
            <ta e="T1060" id="Seg_3163" s="T1059">np.h:S</ta>
            <ta e="T1063" id="Seg_3164" s="T1062">pro:S</ta>
            <ta e="T1064" id="Seg_3165" s="T1063">ptcl.neg</ta>
            <ta e="T1065" id="Seg_3166" s="T1064">v:pred</ta>
            <ta e="T1066" id="Seg_3167" s="T1065">np:S</ta>
            <ta e="T1067" id="Seg_3168" s="T1066">v:pred</ta>
            <ta e="T1068" id="Seg_3169" s="T1067">np:S</ta>
            <ta e="T1069" id="Seg_3170" s="T1068">v:pred</ta>
            <ta e="T1070" id="Seg_3171" s="T1069">np:S</ta>
            <ta e="T1071" id="Seg_3172" s="T1070">v:pred</ta>
            <ta e="T1072" id="Seg_3173" s="T1071">v:pred 0.2.h:S</ta>
            <ta e="T1074" id="Seg_3174" s="T1073">conv:pred</ta>
            <ta e="T1075" id="Seg_3175" s="T1074">pro.h:S</ta>
            <ta e="T1076" id="Seg_3176" s="T1075">v:pred</ta>
            <ta e="T1078" id="Seg_3177" s="T1077">ptcl:pred</ta>
            <ta e="T1080" id="Seg_3178" s="T1079">np:O</ta>
            <ta e="T1085" id="Seg_3179" s="T1084">np.h:S</ta>
            <ta e="T1086" id="Seg_3180" s="T1085">conv:pred</ta>
            <ta e="T1087" id="Seg_3181" s="T1086">v:pred</ta>
            <ta e="T1088" id="Seg_3182" s="T1087">pro:S</ta>
            <ta e="T1090" id="Seg_3183" s="T1089">adj:pred</ta>
            <ta e="T1092" id="Seg_3184" s="T1091">pro.h:S</ta>
            <ta e="T1093" id="Seg_3185" s="T1092">v:pred</ta>
            <ta e="T1095" id="Seg_3186" s="T1094">np:S</ta>
            <ta e="T1096" id="Seg_3187" s="T1095">np:S</ta>
            <ta e="T1097" id="Seg_3188" s="T1096">adj:pred</ta>
            <ta e="T1099" id="Seg_3189" s="T1098">v:pred</ta>
            <ta e="T1100" id="Seg_3190" s="T1099">np:S</ta>
            <ta e="T1101" id="Seg_3191" s="T1100">v:pred 0.2.h:S</ta>
            <ta e="T1104" id="Seg_3192" s="T1103">np:S</ta>
            <ta e="T1106" id="Seg_3193" s="T1105">pro.h:S</ta>
            <ta e="T1107" id="Seg_3194" s="T1106">v:pred</ta>
            <ta e="T1112" id="Seg_3195" s="T1111">np:S</ta>
            <ta e="T1115" id="Seg_3196" s="T1114">np.h:S</ta>
            <ta e="T1116" id="Seg_3197" s="T1115">v:pred</ta>
            <ta e="T1117" id="Seg_3198" s="T1116">v:pred 0.2.h:S</ta>
            <ta e="T1119" id="Seg_3199" s="T1118">v:pred 0.3.h:S</ta>
            <ta e="T1120" id="Seg_3200" s="T1119">np:O</ta>
            <ta e="T1122" id="Seg_3201" s="T1121">v:pred 0.3.h:S</ta>
            <ta e="T1125" id="Seg_3202" s="T1124">v:pred 0.3.h:S</ta>
            <ta e="T1128" id="Seg_3203" s="T1127">np.h:S</ta>
            <ta e="T1129" id="Seg_3204" s="T1128">v:pred</ta>
            <ta e="T1130" id="Seg_3205" s="T1129">pro:S</ta>
            <ta e="T1132" id="Seg_3206" s="T1131">adj:pred</ta>
            <ta e="T1134" id="Seg_3207" s="T1133">np:S</ta>
            <ta e="T1135" id="Seg_3208" s="T1134">v:pred</ta>
            <ta e="T1136" id="Seg_3209" s="T1135">v:pred 0.2.h:S</ta>
            <ta e="T1138" id="Seg_3210" s="T1137">pro.h:S</ta>
            <ta e="T1139" id="Seg_3211" s="T1138">v:pred</ta>
            <ta e="T1141" id="Seg_3212" s="T1140">np:S</ta>
            <ta e="T1142" id="Seg_3213" s="T1141">np:S</ta>
            <ta e="T1148" id="Seg_3214" s="T1147">s:purp</ta>
            <ta e="T1150" id="Seg_3215" s="T1149">v:pred 0.3:S</ta>
            <ta e="T1151" id="Seg_3216" s="T1150">np.h:S</ta>
            <ta e="T1152" id="Seg_3217" s="T1151">v:pred</ta>
            <ta e="T1153" id="Seg_3218" s="T1152">v:pred</ta>
            <ta e="T1154" id="Seg_3219" s="T1153">v:pred 0.2.h:S</ta>
            <ta e="T1155" id="Seg_3220" s="T1154">v:pred 0.2.h:S</ta>
            <ta e="T1159" id="Seg_3221" s="T1158">pro.h:S</ta>
            <ta e="T1160" id="Seg_3222" s="T1159">cop</ta>
            <ta e="T1161" id="Seg_3223" s="T1160">n:pred</ta>
            <ta e="T1163" id="Seg_3224" s="T1162">pro.h:S</ta>
            <ta e="T1165" id="Seg_3225" s="T1164">n:pred</ta>
            <ta e="T1166" id="Seg_3226" s="T1165">pro.h:S</ta>
            <ta e="T1167" id="Seg_3227" s="T1166">v:pred</ta>
            <ta e="T1170" id="Seg_3228" s="T1169">v:pred 0.3.h:S</ta>
            <ta e="T1171" id="Seg_3229" s="T1170">np.h:S</ta>
            <ta e="T1172" id="Seg_3230" s="T1171">v:pred</ta>
            <ta e="T1173" id="Seg_3231" s="T1172">pro:S</ta>
            <ta e="T1176" id="Seg_3232" s="T1175">adj:pred</ta>
            <ta e="T1178" id="Seg_3233" s="T1177">pro.h:S</ta>
            <ta e="T1180" id="Seg_3234" s="T1179">pro.h:S</ta>
            <ta e="T1181" id="Seg_3235" s="T1180">n:pred</ta>
            <ta e="T1182" id="Seg_3236" s="T1181">cop</ta>
            <ta e="T1185" id="Seg_3237" s="T1184">np.h:S</ta>
            <ta e="T1188" id="Seg_3238" s="T1187">n:pred</ta>
            <ta e="T1189" id="Seg_3239" s="T1188">cop</ta>
            <ta e="T1190" id="Seg_3240" s="T1189">v:pred 0.2.h:S</ta>
            <ta e="T1192" id="Seg_3241" s="T1191">v:pred 0.3.h:S</ta>
            <ta e="T1193" id="Seg_3242" s="T1192">v:pred 0.3.h:S</ta>
            <ta e="T1194" id="Seg_3243" s="T1193">v:pred 0.3.h:S</ta>
            <ta e="T1198" id="Seg_3244" s="T1197">v:pred 0.2.h:S</ta>
            <ta e="T1199" id="Seg_3245" s="T1198">v:pred 0.2.h:S</ta>
            <ta e="T1201" id="Seg_3246" s="T1200">pro.h:S</ta>
            <ta e="T1202" id="Seg_3247" s="T1201">cop</ta>
            <ta e="T1204" id="Seg_3248" s="T1203">n:pred</ta>
            <ta e="T1206" id="Seg_3249" s="T1205">pro.h:S</ta>
            <ta e="T1207" id="Seg_3250" s="T1206">n:pred</ta>
            <ta e="T1208" id="Seg_3251" s="T1207">v:pred 0.3.h:S</ta>
            <ta e="T1211" id="Seg_3252" s="T1210">ptcl:pred</ta>
            <ta e="T1214" id="Seg_3253" s="T1213">np:O</ta>
            <ta e="T1215" id="Seg_3254" s="T1214">np.h:S</ta>
            <ta e="T1216" id="Seg_3255" s="T1215">v:pred</ta>
            <ta e="T1217" id="Seg_3256" s="T1216">pro:S</ta>
            <ta e="T1219" id="Seg_3257" s="T1218">adj:pred</ta>
            <ta e="T1222" id="Seg_3258" s="T1221">pro.h:S</ta>
            <ta e="T1224" id="Seg_3259" s="T1223">cop</ta>
            <ta e="T1225" id="Seg_3260" s="T1224">n:pred</ta>
            <ta e="T1228" id="Seg_3261" s="T1227">np.h:S</ta>
            <ta e="T1229" id="Seg_3262" s="T1228">n:pred</ta>
            <ta e="T1230" id="Seg_3263" s="T1229">v:pred 0.2.h:S</ta>
            <ta e="T1231" id="Seg_3264" s="T1230">pro.h:S</ta>
            <ta e="T1233" id="Seg_3265" s="T1232">v:pred</ta>
            <ta e="T1234" id="Seg_3266" s="T1233">np.h:S</ta>
            <ta e="T1237" id="Seg_3267" s="T1235">np:O</ta>
            <ta e="T1239" id="Seg_3268" s="T1238">v:pred 0.3.h:S</ta>
            <ta e="T1242" id="Seg_3269" s="T1241">v:pred 0.3.h:S</ta>
            <ta e="T1243" id="Seg_3270" s="T1242">v:pred 0.3.h:S</ta>
            <ta e="T1244" id="Seg_3271" s="T1243">v:pred 0.3.h:S</ta>
            <ta e="T1246" id="Seg_3272" s="T1245">np.h:S</ta>
            <ta e="T1248" id="Seg_3273" s="T1247">v:pred</ta>
            <ta e="T1250" id="Seg_3274" s="T1249">np.h:O</ta>
            <ta e="T1251" id="Seg_3275" s="T1250">v:pred 0.2.h:S</ta>
            <ta e="T1253" id="Seg_3276" s="T1252">pro.h:S</ta>
            <ta e="T1254" id="Seg_3277" s="T1253">n:pred</ta>
            <ta e="T1255" id="Seg_3278" s="T1254">cop</ta>
            <ta e="T1257" id="Seg_3279" s="T1256">pro.h:S</ta>
            <ta e="T1259" id="Seg_3280" s="T1258">n:pred</ta>
            <ta e="T1260" id="Seg_3281" s="T1259">pro.h:S</ta>
            <ta e="T1261" id="Seg_3282" s="T1260">v:pred</ta>
            <ta e="T1262" id="Seg_3283" s="T1261">ptcl:pred</ta>
            <ta e="T1267" id="Seg_3284" s="T1266">np:O</ta>
            <ta e="T1269" id="Seg_3285" s="T1268">np.h:S</ta>
            <ta e="T1270" id="Seg_3286" s="T1269">conv:pred</ta>
            <ta e="T1271" id="Seg_3287" s="T1270">v:pred</ta>
            <ta e="T1272" id="Seg_3288" s="T1271">pro:S</ta>
            <ta e="T1274" id="Seg_3289" s="T1273">adj:pred</ta>
            <ta e="T1276" id="Seg_3290" s="T1275">pro.h:S</ta>
            <ta e="T1278" id="Seg_3291" s="T1277">n:pred</ta>
            <ta e="T1279" id="Seg_3292" s="T1278">cop</ta>
            <ta e="T1282" id="Seg_3293" s="T1281">np.h:S</ta>
            <ta e="T1284" id="Seg_3294" s="T1283">n:pred</ta>
            <ta e="T1285" id="Seg_3295" s="T1284">v:pred 0.2.h:S</ta>
            <ta e="T1286" id="Seg_3296" s="T1285">pro.h:S</ta>
            <ta e="T1287" id="Seg_3297" s="T1286">n:pred</ta>
            <ta e="T1288" id="Seg_3298" s="T1287">cop</ta>
            <ta e="T1291" id="Seg_3299" s="T1290">np.h:S</ta>
            <ta e="T1292" id="Seg_3300" s="T1291">n:pred</ta>
            <ta e="T1293" id="Seg_3301" s="T1292">cop</ta>
            <ta e="T1295" id="Seg_3302" s="T1294">v:pred 0.3.h:S</ta>
            <ta e="T1297" id="Seg_3303" s="T1296">np.h:S</ta>
            <ta e="T1298" id="Seg_3304" s="T1297">n:pred</ta>
            <ta e="T1300" id="Seg_3305" s="T1299">np.h:S</ta>
            <ta e="T1301" id="Seg_3306" s="T1300">n:pred</ta>
            <ta e="T1304" id="Seg_3307" s="T1303">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T1063" id="Seg_3308" s="T1062">TURK:gram(INDEF)</ta>
            <ta e="T1070" id="Seg_3309" s="T1069">RUS:cult</ta>
            <ta e="T1078" id="Seg_3310" s="T1077">RUS:gram</ta>
            <ta e="T1098" id="Seg_3311" s="T1097">RUS:gram</ta>
            <ta e="T1109" id="Seg_3312" s="T1108">RUS:gram</ta>
            <ta e="T1118" id="Seg_3313" s="T1117">RUS:gram</ta>
            <ta e="T1120" id="Seg_3314" s="T1119">TURK:cult </ta>
            <ta e="T1140" id="Seg_3315" s="T1139">TURK:disc</ta>
            <ta e="T1141" id="Seg_3316" s="T1140">TURK:cult</ta>
            <ta e="T1143" id="Seg_3317" s="T1142">TURK:disc</ta>
            <ta e="T1147" id="Seg_3318" s="T1146">RUS:gram</ta>
            <ta e="T1156" id="Seg_3319" s="T1155">RUS:gram</ta>
            <ta e="T1161" id="Seg_3320" s="T1160">TURK:cult</ta>
            <ta e="T1162" id="Seg_3321" s="T1161">RUS:gram</ta>
            <ta e="T1164" id="Seg_3322" s="T1163">TURK:cult</ta>
            <ta e="T1177" id="Seg_3323" s="T1176">RUS:gram</ta>
            <ta e="T1181" id="Seg_3324" s="T1180">TURK:cult</ta>
            <ta e="T1183" id="Seg_3325" s="T1182">RUS:gram</ta>
            <ta e="T1186" id="Seg_3326" s="T1185">RUS:mod</ta>
            <ta e="T1188" id="Seg_3327" s="T1187">TURK:cult</ta>
            <ta e="T1196" id="Seg_3328" s="T1195">RUS:gram</ta>
            <ta e="T1200" id="Seg_3329" s="T1199">RUS:gram</ta>
            <ta e="T1203" id="Seg_3330" s="T1202">RUS:gram</ta>
            <ta e="T1204" id="Seg_3331" s="T1203">RUS:cult</ta>
            <ta e="T1205" id="Seg_3332" s="T1204">RUS:gram</ta>
            <ta e="T1207" id="Seg_3333" s="T1206">RUS:cult</ta>
            <ta e="T1211" id="Seg_3334" s="T1210">RUS:gram</ta>
            <ta e="T1221" id="Seg_3335" s="T1220">RUS:gram</ta>
            <ta e="T1223" id="Seg_3336" s="T1222">RUS:gram</ta>
            <ta e="T1225" id="Seg_3337" s="T1224">RUS:cult</ta>
            <ta e="T1226" id="Seg_3338" s="T1225">RUS:gram</ta>
            <ta e="T1229" id="Seg_3339" s="T1228">RUS:cult</ta>
            <ta e="T1235" id="Seg_3340" s="T1234">TURK:disc</ta>
            <ta e="T1238" id="Seg_3341" s="T1237">TURK:disc</ta>
            <ta e="T1252" id="Seg_3342" s="T1251">RUS:gram</ta>
            <ta e="T1256" id="Seg_3343" s="T1255">RUS:gram</ta>
            <ta e="T1262" id="Seg_3344" s="T1261">RUS:gram</ta>
            <ta e="T1275" id="Seg_3345" s="T1274">RUS:gram</ta>
            <ta e="T1280" id="Seg_3346" s="T1279">RUS:gram</ta>
            <ta e="T1289" id="Seg_3347" s="T1288">RUS:gram</ta>
            <ta e="T1299" id="Seg_3348" s="T1298">RUS:gram</ta>
            <ta e="T1302" id="Seg_3349" s="T1301">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T1160" id="Seg_3350" s="T1155">RUS:calq</ta>
            <ta e="T1203" id="Seg_3351" s="T1199">RUS:calq</ta>
            <ta e="T1207" id="Seg_3352" s="T1206">RUS:calq</ta>
            <ta e="T1224" id="Seg_3353" s="T1222">RUS:calq</ta>
            <ta e="T1255" id="Seg_3354" s="T1254">RUS:calq</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T1060" id="Seg_3355" s="T1057">Жили жена и муж.</ta>
            <ta e="T1065" id="Seg_3356" s="T1060">У них ничего не было.</ta>
            <ta e="T1067" id="Seg_3357" s="T1065">Хлеба нет.</ta>
            <ta e="T1071" id="Seg_3358" s="T1067">Дров нет, лучин нет.</ta>
            <ta e="T1074" id="Seg_3359" s="T1071">"Пойди в тайгу за дровами!"</ta>
            <ta e="T1076" id="Seg_3360" s="T1074">Он пошёл.</ta>
            <ta e="T1082" id="Seg_3361" s="T1076">Там начал одно дерево топором рубить.</ta>
            <ta e="T1087" id="Seg_3362" s="T1082">Тогда [оттуда] вылетает птица.</ta>
            <ta e="T1091" id="Seg_3363" s="T1087">"Что тебе нужно, человек?"</ta>
            <ta e="T1097" id="Seg_3364" s="T1091">Он говорит: "Мне дрова нужны.</ta>
            <ta e="T1100" id="Seg_3365" s="T1097">А то дров нет".</ta>
            <ta e="T1102" id="Seg_3366" s="T1100">"Иди домой!</ta>
            <ta e="T1105" id="Seg_3367" s="T1102">Там много дров!"</ta>
            <ta e="T1113" id="Seg_3368" s="T1105">Он пришёл домой, а там дров очень много.</ta>
            <ta e="T1117" id="Seg_3369" s="T1113">Потом его жена говорит: "Иди!</ta>
            <ta e="T1120" id="Seg_3370" s="T1117">Пусть хлеба даст!"</ta>
            <ta e="T1122" id="Seg_3371" s="T1120">Потом он пришёл.</ta>
            <ta e="T1125" id="Seg_3372" s="T1122">Опять топором стал рубить.</ta>
            <ta e="T1129" id="Seg_3373" s="T1125">Опять оттуда птица вылетела.</ta>
            <ta e="T1133" id="Seg_3374" s="T1129">"Что тебе нужно, человек?"</ta>
            <ta e="T1135" id="Seg_3375" s="T1133">"Хлеба нет".</ta>
            <ta e="T1137" id="Seg_3376" s="T1135">"Иди домой!"</ta>
            <ta e="T1140" id="Seg_3377" s="T1137">Он пришёл.</ta>
            <ta e="T1144" id="Seg_3378" s="T1140">[Дома] много коров, овец.</ta>
            <ta e="T1146" id="Seg_3379" s="T1144">Полный загон.</ta>
            <ta e="T1150" id="Seg_3380" s="T1146">И еды много.</ta>
            <ta e="T1154" id="Seg_3381" s="T1150">Жена ела, ела, [потом говорит]: "Иди!</ta>
            <ta e="T1165" id="Seg_3382" s="T1154">Скажи [ей], чтобы ты был начальником, а я начальника женой".</ta>
            <ta e="T1170" id="Seg_3383" s="T1165">Он пошёл, снова топором рубит.</ta>
            <ta e="T1172" id="Seg_3384" s="T1170">Птица вылетела.</ta>
            <ta e="T1176" id="Seg_3385" s="T1172">"Что тебе нужно?"</ta>
            <ta e="T1189" id="Seg_3386" s="T1176">"Да я [хочу] начальником быть, а моя жена тоже (начальником?) [пусть] станет".</ta>
            <ta e="T1190" id="Seg_3387" s="T1189">"Иди!"</ta>
            <ta e="T1192" id="Seg_3388" s="T1190">Потом он пришёл.</ta>
            <ta e="T1194" id="Seg_3389" s="T1192">Они жили, жили.</ta>
            <ta e="T1197" id="Seg_3390" s="T1194">Жене(?) снова [говорит]:</ta>
            <ta e="T1207" id="Seg_3391" s="T1197">"Иди, скажи, чтобы ты был царём, а я царицей".</ta>
            <ta e="T1208" id="Seg_3392" s="T1207">Он пошёл.</ta>
            <ta e="T1214" id="Seg_3393" s="T1208">(Птица снова…) Начал рубить дерево топором.</ta>
            <ta e="T1216" id="Seg_3394" s="T1214">Прилетела птица.</ta>
            <ta e="T1220" id="Seg_3395" s="T1216">"Что тебе нужно, человек?"</ta>
            <ta e="T1229" id="Seg_3396" s="T1220">"Да чтобы я был царём, а моя жена царицей".</ta>
            <ta e="T1230" id="Seg_3397" s="T1229">"Иди!"</ta>
            <ta e="T1240" id="Seg_3398" s="T1230">Он идёт, все люди шапки [перед ним сниимают], все ему кланяются.</ta>
            <ta e="T1244" id="Seg_3399" s="T1240">Потом он пришёл, жили они, жили.</ta>
            <ta e="T1250" id="Seg_3400" s="T1244">Потом жена опять мужа гонит.</ta>
            <ta e="T1251" id="Seg_3401" s="T1250">"Иди!</ta>
            <ta e="T1259" id="Seg_3402" s="T1251">[Скажи,] чтобы ты был богом, а я бога матерью".</ta>
            <ta e="T1267" id="Seg_3403" s="T1259">Он пошёл, стал рубить то дерево топором.</ta>
            <ta e="T1271" id="Seg_3404" s="T1267">Эта птица прилетает.</ta>
            <ta e="T1274" id="Seg_3405" s="T1271">"Что тебе нужно?"</ta>
            <ta e="T1284" id="Seg_3406" s="T1274">"Чтобы я стал богом, а моя жена матерью бога".</ta>
            <ta e="T1285" id="Seg_3407" s="T1284">"Иди!</ta>
            <ta e="T1293" id="Seg_3408" s="T1285">Ты станешь быком, а твоя жена станет свиньёй!"</ta>
            <ta e="T1295" id="Seg_3409" s="T1293">Потом он пришёл.</ta>
            <ta e="T1301" id="Seg_3410" s="T1295">Этот человек стал быком, а его жена свиньёй.</ta>
            <ta e="T1304" id="Seg_3411" s="T1301">И так остались.</ta>
            <ta e="T1305" id="Seg_3412" s="T1304">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T1060" id="Seg_3413" s="T1057">[Once] there lived a woman and a man.</ta>
            <ta e="T1065" id="Seg_3414" s="T1060">They did not have anything.</ta>
            <ta e="T1067" id="Seg_3415" s="T1065">No bread.</ta>
            <ta e="T1071" id="Seg_3416" s="T1067">No wood, no torches.</ta>
            <ta e="T1074" id="Seg_3417" s="T1071">"Go to the taiga to fetch wood!"</ta>
            <ta e="T1076" id="Seg_3418" s="T1074">He went.</ta>
            <ta e="T1082" id="Seg_3419" s="T1076">There he started to cut a tree with an axe.</ta>
            <ta e="T1087" id="Seg_3420" s="T1082">Then a bird came flying [from there].</ta>
            <ta e="T1091" id="Seg_3421" s="T1087">"What do you need, man?"</ta>
            <ta e="T1097" id="Seg_3422" s="T1091">He says: "I need wood.</ta>
            <ta e="T1100" id="Seg_3423" s="T1097">Because [we] have no wood.</ta>
            <ta e="T1102" id="Seg_3424" s="T1100">"Go home!</ta>
            <ta e="T1105" id="Seg_3425" s="T1102">There is a lot of wood!"</ta>
            <ta e="T1113" id="Seg_3426" s="T1105">He came home, and there is very much wood.</ta>
            <ta e="T1117" id="Seg_3427" s="T1113">Then his wife says: "Go!</ta>
            <ta e="T1120" id="Seg_3428" s="T1117">Let it give bread!"</ta>
            <ta e="T1122" id="Seg_3429" s="T1120">Then he came.</ta>
            <ta e="T1125" id="Seg_3430" s="T1122">Again he chopped with an axe.</ta>
            <ta e="T1129" id="Seg_3431" s="T1125">Again the bird was flying around.</ta>
            <ta e="T1133" id="Seg_3432" s="T1129">"What do you need, man?"</ta>
            <ta e="T1135" id="Seg_3433" s="T1133">"There is no bread."</ta>
            <ta e="T1137" id="Seg_3434" s="T1135">"Go home!"</ta>
            <ta e="T1140" id="Seg_3435" s="T1137">He came.</ta>
            <ta e="T1144" id="Seg_3436" s="T1140">[There are] a lot of cows, of sheep.</ta>
            <ta e="T1146" id="Seg_3437" s="T1144">A full corral.</ta>
            <ta e="T1150" id="Seg_3438" s="T1146">And there is a lot to eat.</ta>
            <ta e="T1154" id="Seg_3439" s="T1150">The woman ate, ate, [then says]: "Go!</ta>
            <ta e="T1165" id="Seg_3440" s="T1154">Tell [it], that you would be the chief, and me the chief's wife."</ta>
            <ta e="T1170" id="Seg_3441" s="T1165">He went, again chopped with an axe.</ta>
            <ta e="T1172" id="Seg_3442" s="T1170">The bird flew around.</ta>
            <ta e="T1176" id="Seg_3443" s="T1172">"What do you want?"</ta>
            <ta e="T1189" id="Seg_3444" s="T1176">"I [want to] be the chief, and my wife will be the (chief?), too."</ta>
            <ta e="T1190" id="Seg_3445" s="T1189">"Go!"</ta>
            <ta e="T1192" id="Seg_3446" s="T1190">Then he came.</ta>
            <ta e="T1194" id="Seg_3447" s="T1192">They lived, they lived.</ta>
            <ta e="T1197" id="Seg_3448" s="T1194">(To?) his wife again:.</ta>
            <ta e="T1207" id="Seg_3449" s="T1197">"Go, tell that you would be the tsar, and me the tsarina."</ta>
            <ta e="T1208" id="Seg_3450" s="T1207">He went.</ta>
            <ta e="T1214" id="Seg_3451" s="T1208">(The bird again…) He started to chop a tree with the axe.</ta>
            <ta e="T1216" id="Seg_3452" s="T1214">The bird flew [there].</ta>
            <ta e="T1220" id="Seg_3453" s="T1216">"What do you need, man?"</ta>
            <ta e="T1229" id="Seg_3454" s="T1220">That I would be a tsar, and my wife the tsarina.</ta>
            <ta e="T1230" id="Seg_3455" s="T1229">"Go!"</ta>
            <ta e="T1240" id="Seg_3456" s="T1230">He goes, many people [take off] hats [before him], everybody bow to him.</ta>
            <ta e="T1244" id="Seg_3457" s="T1240">Then he came, [so] they lived, they lived.</ta>
            <ta e="T1250" id="Seg_3458" s="T1244">Then the wife again is driving out the man.</ta>
            <ta e="T1251" id="Seg_3459" s="T1250">"Go!</ta>
            <ta e="T1259" id="Seg_3460" s="T1251">[Tell her] that you would be God, and me God’s mother.</ta>
            <ta e="T1267" id="Seg_3461" s="T1259">He went, he started to chop this tree again with the axe.</ta>
            <ta e="T1271" id="Seg_3462" s="T1267">The bird came flying.</ta>
            <ta e="T1274" id="Seg_3463" s="T1271">"What do you need?"</ta>
            <ta e="T1284" id="Seg_3464" s="T1274">"That I become God, and my wife God's mother."</ta>
            <ta e="T1285" id="Seg_3465" s="T1284">"Go!</ta>
            <ta e="T1293" id="Seg_3466" s="T1285">You will become a bull, and your wife will become a pig!"</ta>
            <ta e="T1295" id="Seg_3467" s="T1293">Then he came.</ta>
            <ta e="T1301" id="Seg_3468" s="T1295">The man was a bull, and his wife a pig.</ta>
            <ta e="T1304" id="Seg_3469" s="T1301">And so they remained.</ta>
            <ta e="T1305" id="Seg_3470" s="T1304">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T1060" id="Seg_3471" s="T1057">Es lebte [einmal] eine Frau und einen Mann.</ta>
            <ta e="T1065" id="Seg_3472" s="T1060">Sie hatten nichts.</ta>
            <ta e="T1067" id="Seg_3473" s="T1065">Kein Brot.</ta>
            <ta e="T1071" id="Seg_3474" s="T1067">Kein Holz, keine Fackel.</ta>
            <ta e="T1074" id="Seg_3475" s="T1071">„Gehe auf die Taiga und hole Holz!“</ta>
            <ta e="T1076" id="Seg_3476" s="T1074">Er ging.</ta>
            <ta e="T1082" id="Seg_3477" s="T1076">Dort fing er an einen Baum mit einer Axt zu schlagen.</ta>
            <ta e="T1087" id="Seg_3478" s="T1082">Dann kam ein Vogel angeflogen.</ta>
            <ta e="T1091" id="Seg_3479" s="T1087">„Was brauchst du, Mann?“</ta>
            <ta e="T1097" id="Seg_3480" s="T1091">Er sagt: „Ich brauche Holz.</ta>
            <ta e="T1100" id="Seg_3481" s="T1097">Weil [wir] kein Holz haben.“</ta>
            <ta e="T1102" id="Seg_3482" s="T1100">„Geh nach Hause!</ta>
            <ta e="T1105" id="Seg_3483" s="T1102">Dort ist viel Holz!“</ta>
            <ta e="T1113" id="Seg_3484" s="T1105">Er kam nach Hause, und es war sehr viel Holz.</ta>
            <ta e="T1117" id="Seg_3485" s="T1113">Dann sagt seine Frau: „Geh!</ta>
            <ta e="T1120" id="Seg_3486" s="T1117">Lass es Brot geben!“</ta>
            <ta e="T1122" id="Seg_3487" s="T1120">Dann kam er.</ta>
            <ta e="T1125" id="Seg_3488" s="T1122">Wieder hackte er mit einer Axt.</ta>
            <ta e="T1129" id="Seg_3489" s="T1125">Wieder flog der Vogel herum.</ta>
            <ta e="T1133" id="Seg_3490" s="T1129">„Was brauchst du, Mann?“</ta>
            <ta e="T1135" id="Seg_3491" s="T1133">Es gibt kein Brot.“</ta>
            <ta e="T1137" id="Seg_3492" s="T1135">„Geh nach Hause!"</ta>
            <ta e="T1140" id="Seg_3493" s="T1137">Er kam.</ta>
            <ta e="T1144" id="Seg_3494" s="T1140">[Es waren] viele Kühe, Schafe.</ta>
            <ta e="T1146" id="Seg_3495" s="T1144">Ein ganzer Korral.</ta>
            <ta e="T1150" id="Seg_3496" s="T1146">Und es ist viel zu essen.</ta>
            <ta e="T1154" id="Seg_3497" s="T1150">Die Frau aß, aß, [sagt dann]: „Geh!</ta>
            <ta e="T1165" id="Seg_3498" s="T1154">Erzähl [ihn], dass du der Chef sein möchtest, und ich die Frau des Chefs.“</ta>
            <ta e="T1170" id="Seg_3499" s="T1165">Er ging, wieder hackte er mit einer Axt.</ta>
            <ta e="T1172" id="Seg_3500" s="T1170">Der Vogel flog herum.</ta>
            <ta e="T1176" id="Seg_3501" s="T1172">„Was willst du?“</ta>
            <ta e="T1189" id="Seg_3502" s="T1176">„Ich [will] der Chef sein, und meine Frau [will] auch die (Chefin?) sein.“</ta>
            <ta e="T1190" id="Seg_3503" s="T1189">„Geh!“</ta>
            <ta e="T1192" id="Seg_3504" s="T1190">Dann kam er.</ta>
            <ta e="T1194" id="Seg_3505" s="T1192">Sie lebten, sie lebten.</ta>
            <ta e="T1197" id="Seg_3506" s="T1194">(Zu?) seiner Frau nochmals:</ta>
            <ta e="T1207" id="Seg_3507" s="T1197">„Geh, erzähl, dass du Zar sein willst, und ich die Zarina.“</ta>
            <ta e="T1208" id="Seg_3508" s="T1207">Er ging.</ta>
            <ta e="T1214" id="Seg_3509" s="T1208">(Der Vogel wieder…) Er fing an, mit einer Axt einen Baum zu schlagen.</ta>
            <ta e="T1216" id="Seg_3510" s="T1214">Der Vogel flog [dort].</ta>
            <ta e="T1220" id="Seg_3511" s="T1216">„Was brauchst du, Mann?“</ta>
            <ta e="T1229" id="Seg_3512" s="T1220">Dass ich Zar sein will, und meine Frau die Zarina.</ta>
            <ta e="T1230" id="Seg_3513" s="T1229">„Geh!“</ta>
            <ta e="T1240" id="Seg_3514" s="T1230">Er geht, viele Leute [nehmen ab] Hüter [vor ihm], jeder verbeugt sich vor ihm.</ta>
            <ta e="T1244" id="Seg_3515" s="T1240">Dann kam er, [so] lebten sie, lebten sie.</ta>
            <ta e="T1250" id="Seg_3516" s="T1244">Dann treibt die Frau den Mann wieder hinaus.</ta>
            <ta e="T1251" id="Seg_3517" s="T1250">„Geh!“</ta>
            <ta e="T1259" id="Seg_3518" s="T1251">[Erzähl] dass du Gott sein willst, und ich die Mutter Gottes.</ta>
            <ta e="T1267" id="Seg_3519" s="T1259">Er ging, er fing wieder an, diesen Baum mit der Axt zu schlagen.</ta>
            <ta e="T1271" id="Seg_3520" s="T1267">Der Vogel kam geflogen.</ta>
            <ta e="T1274" id="Seg_3521" s="T1271">„Was brauchst du?“</ta>
            <ta e="T1284" id="Seg_3522" s="T1274">Dass ich Gott werde, und mein Frau die Mutter Gottes.“</ta>
            <ta e="T1285" id="Seg_3523" s="T1284">"Geh!</ta>
            <ta e="T1293" id="Seg_3524" s="T1285">Du werdest ein Bulle werden, und deine Frau wird zu einem Schwein!“</ta>
            <ta e="T1295" id="Seg_3525" s="T1293">Dann kam er.</ta>
            <ta e="T1301" id="Seg_3526" s="T1295">Der Mann war ein Bulle, und seine Frau ein Scwein.</ta>
            <ta e="T1304" id="Seg_3527" s="T1301">Und so blieben sie.</ta>
            <ta e="T1305" id="Seg_3528" s="T1304">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T1060" id="Seg_3529" s="T1057">[GVY:] A tale by Alexei Tolstoy "Чивы, чивы, чивычок…", see e.g. http://narodstory.net/skazki-tolstoy.php?id=109</ta>
            <ta e="T1146" id="Seg_3530" s="T1144">[KlT:] Albə- ’to be full’.</ta>
            <ta e="T1165" id="Seg_3531" s="T1154">[GVY:] Locative koŋgən is strange. From Russian "у царя жена"?</ta>
            <ta e="T1189" id="Seg_3532" s="T1176">The second măn is unclear. 'My' (referring to the wife) or should be măndə 'she (or he?) says'?</ta>
            <ta e="T1197" id="Seg_3533" s="T1194">[KlT:] LAT/LOC instead of GEN. [GVY:] bazoʔ is often pronounced as bažoʔ</ta>
            <ta e="T1207" id="Seg_3534" s="T1197">[KlT:] INS following the Russian model, most likely. [GVY:] The last form is heard like tsaritsa(ʔ)izʼiʔ. The -(ʔ)i- is not clear (was she going to say "царицею"?).</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1057" />
            <conversion-tli id="T1058" />
            <conversion-tli id="T1059" />
            <conversion-tli id="T1060" />
            <conversion-tli id="T1061" />
            <conversion-tli id="T1062" />
            <conversion-tli id="T1063" />
            <conversion-tli id="T1064" />
            <conversion-tli id="T1065" />
            <conversion-tli id="T1066" />
            <conversion-tli id="T1067" />
            <conversion-tli id="T1068" />
            <conversion-tli id="T1069" />
            <conversion-tli id="T1070" />
            <conversion-tli id="T1071" />
            <conversion-tli id="T1072" />
            <conversion-tli id="T1073" />
            <conversion-tli id="T1074" />
            <conversion-tli id="T1075" />
            <conversion-tli id="T1076" />
            <conversion-tli id="T1077" />
            <conversion-tli id="T1078" />
            <conversion-tli id="T1079" />
            <conversion-tli id="T1080" />
            <conversion-tli id="T1081" />
            <conversion-tli id="T1082" />
            <conversion-tli id="T1083" />
            <conversion-tli id="T1084" />
            <conversion-tli id="T1085" />
            <conversion-tli id="T1086" />
            <conversion-tli id="T1087" />
            <conversion-tli id="T1088" />
            <conversion-tli id="T1089" />
            <conversion-tli id="T1090" />
            <conversion-tli id="T1091" />
            <conversion-tli id="T1092" />
            <conversion-tli id="T1093" />
            <conversion-tli id="T1094" />
            <conversion-tli id="T1095" />
            <conversion-tli id="T1096" />
            <conversion-tli id="T1097" />
            <conversion-tli id="T1098" />
            <conversion-tli id="T1099" />
            <conversion-tli id="T1100" />
            <conversion-tli id="T1101" />
            <conversion-tli id="T1102" />
            <conversion-tli id="T1103" />
            <conversion-tli id="T1104" />
            <conversion-tli id="T1105" />
            <conversion-tli id="T1106" />
            <conversion-tli id="T1107" />
            <conversion-tli id="T1108" />
            <conversion-tli id="T1109" />
            <conversion-tli id="T1110" />
            <conversion-tli id="T1111" />
            <conversion-tli id="T1112" />
            <conversion-tli id="T1113" />
            <conversion-tli id="T1114" />
            <conversion-tli id="T1115" />
            <conversion-tli id="T1116" />
            <conversion-tli id="T1117" />
            <conversion-tli id="T1118" />
            <conversion-tli id="T1119" />
            <conversion-tli id="T1120" />
            <conversion-tli id="T1121" />
            <conversion-tli id="T1122" />
            <conversion-tli id="T1123" />
            <conversion-tli id="T1124" />
            <conversion-tli id="T1125" />
            <conversion-tli id="T1126" />
            <conversion-tli id="T1127" />
            <conversion-tli id="T1128" />
            <conversion-tli id="T1129" />
            <conversion-tli id="T1130" />
            <conversion-tli id="T1131" />
            <conversion-tli id="T1132" />
            <conversion-tli id="T1133" />
            <conversion-tli id="T1134" />
            <conversion-tli id="T1135" />
            <conversion-tli id="T1136" />
            <conversion-tli id="T1137" />
            <conversion-tli id="T1138" />
            <conversion-tli id="T1139" />
            <conversion-tli id="T1140" />
            <conversion-tli id="T1141" />
            <conversion-tli id="T1142" />
            <conversion-tli id="T1143" />
            <conversion-tli id="T1144" />
            <conversion-tli id="T1145" />
            <conversion-tli id="T1146" />
            <conversion-tli id="T1147" />
            <conversion-tli id="T1148" />
            <conversion-tli id="T1149" />
            <conversion-tli id="T1150" />
            <conversion-tli id="T1151" />
            <conversion-tli id="T1152" />
            <conversion-tli id="T1153" />
            <conversion-tli id="T1154" />
            <conversion-tli id="T1155" />
            <conversion-tli id="T1156" />
            <conversion-tli id="T1157" />
            <conversion-tli id="T1158" />
            <conversion-tli id="T1159" />
            <conversion-tli id="T1160" />
            <conversion-tli id="T1161" />
            <conversion-tli id="T1162" />
            <conversion-tli id="T1163" />
            <conversion-tli id="T1164" />
            <conversion-tli id="T1165" />
            <conversion-tli id="T1166" />
            <conversion-tli id="T1167" />
            <conversion-tli id="T1168" />
            <conversion-tli id="T1169" />
            <conversion-tli id="T1170" />
            <conversion-tli id="T1171" />
            <conversion-tli id="T1172" />
            <conversion-tli id="T1173" />
            <conversion-tli id="T1174" />
            <conversion-tli id="T1175" />
            <conversion-tli id="T1176" />
            <conversion-tli id="T1177" />
            <conversion-tli id="T1178" />
            <conversion-tli id="T1179" />
            <conversion-tli id="T1180" />
            <conversion-tli id="T1181" />
            <conversion-tli id="T1182" />
            <conversion-tli id="T1183" />
            <conversion-tli id="T1184" />
            <conversion-tli id="T1185" />
            <conversion-tli id="T1186" />
            <conversion-tli id="T1187" />
            <conversion-tli id="T1188" />
            <conversion-tli id="T1189" />
            <conversion-tli id="T1190" />
            <conversion-tli id="T1191" />
            <conversion-tli id="T1192" />
            <conversion-tli id="T1193" />
            <conversion-tli id="T1194" />
            <conversion-tli id="T1195" />
            <conversion-tli id="T1196" />
            <conversion-tli id="T1197" />
            <conversion-tli id="T1198" />
            <conversion-tli id="T1199" />
            <conversion-tli id="T1200" />
            <conversion-tli id="T1201" />
            <conversion-tli id="T1202" />
            <conversion-tli id="T1203" />
            <conversion-tli id="T1204" />
            <conversion-tli id="T1205" />
            <conversion-tli id="T1206" />
            <conversion-tli id="T1207" />
            <conversion-tli id="T1208" />
            <conversion-tli id="T1209" />
            <conversion-tli id="T1210" />
            <conversion-tli id="T1211" />
            <conversion-tli id="T1212" />
            <conversion-tli id="T1213" />
            <conversion-tli id="T1214" />
            <conversion-tli id="T1215" />
            <conversion-tli id="T1216" />
            <conversion-tli id="T1217" />
            <conversion-tli id="T1218" />
            <conversion-tli id="T1219" />
            <conversion-tli id="T1220" />
            <conversion-tli id="T1221" />
            <conversion-tli id="T1222" />
            <conversion-tli id="T1223" />
            <conversion-tli id="T1224" />
            <conversion-tli id="T1225" />
            <conversion-tli id="T1226" />
            <conversion-tli id="T1227" />
            <conversion-tli id="T1228" />
            <conversion-tli id="T1229" />
            <conversion-tli id="T1230" />
            <conversion-tli id="T1231" />
            <conversion-tli id="T1232" />
            <conversion-tli id="T1233" />
            <conversion-tli id="T1234" />
            <conversion-tli id="T1235" />
            <conversion-tli id="T1236" />
            <conversion-tli id="T1237" />
            <conversion-tli id="T1238" />
            <conversion-tli id="T1239" />
            <conversion-tli id="T1240" />
            <conversion-tli id="T1241" />
            <conversion-tli id="T1242" />
            <conversion-tli id="T1243" />
            <conversion-tli id="T1244" />
            <conversion-tli id="T1245" />
            <conversion-tli id="T1246" />
            <conversion-tli id="T1247" />
            <conversion-tli id="T1248" />
            <conversion-tli id="T1249" />
            <conversion-tli id="T1250" />
            <conversion-tli id="T1251" />
            <conversion-tli id="T1252" />
            <conversion-tli id="T1253" />
            <conversion-tli id="T1254" />
            <conversion-tli id="T1255" />
            <conversion-tli id="T1256" />
            <conversion-tli id="T1257" />
            <conversion-tli id="T1258" />
            <conversion-tli id="T1259" />
            <conversion-tli id="T1260" />
            <conversion-tli id="T1261" />
            <conversion-tli id="T1262" />
            <conversion-tli id="T1263" />
            <conversion-tli id="T1264" />
            <conversion-tli id="T1265" />
            <conversion-tli id="T1266" />
            <conversion-tli id="T1267" />
            <conversion-tli id="T1268" />
            <conversion-tli id="T1269" />
            <conversion-tli id="T1270" />
            <conversion-tli id="T1271" />
            <conversion-tli id="T1272" />
            <conversion-tli id="T1273" />
            <conversion-tli id="T1274" />
            <conversion-tli id="T1275" />
            <conversion-tli id="T1276" />
            <conversion-tli id="T1277" />
            <conversion-tli id="T1278" />
            <conversion-tli id="T1279" />
            <conversion-tli id="T1280" />
            <conversion-tli id="T1281" />
            <conversion-tli id="T1282" />
            <conversion-tli id="T1283" />
            <conversion-tli id="T1284" />
            <conversion-tli id="T1285" />
            <conversion-tli id="T1286" />
            <conversion-tli id="T1287" />
            <conversion-tli id="T1288" />
            <conversion-tli id="T1289" />
            <conversion-tli id="T1290" />
            <conversion-tli id="T1291" />
            <conversion-tli id="T1292" />
            <conversion-tli id="T1293" />
            <conversion-tli id="T1294" />
            <conversion-tli id="T1295" />
            <conversion-tli id="T1296" />
            <conversion-tli id="T1297" />
            <conversion-tli id="T1298" />
            <conversion-tli id="T1299" />
            <conversion-tli id="T1300" />
            <conversion-tli id="T1301" />
            <conversion-tli id="T1302" />
            <conversion-tli id="T1303" />
            <conversion-tli id="T1304" />
            <conversion-tli id="T1305" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
