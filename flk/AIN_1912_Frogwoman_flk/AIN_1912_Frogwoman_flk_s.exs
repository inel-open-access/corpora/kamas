<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDF3CBBE72-C44D-D418-1165-A3B2414DAF6D">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\AIN_1912_Frogwoman_flk\AIN_1912_Frogwoman_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">320</ud-information>
            <ud-information attribute-name="# HIAT:w">236</ud-information>
            <ud-information attribute-name="# e">236</ud-information>
            <ud-information attribute-name="# HIAT:u">46</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="AIN">
            <abbreviation>AIN</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T207" />
         <tli id="T208" />
         <tli id="T209" />
         <tli id="T210" />
         <tli id="T211" />
         <tli id="T212" />
         <tli id="T213" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T216" />
         <tli id="T217" />
         <tli id="T218" />
         <tli id="T219" />
         <tli id="T220" />
         <tli id="T221" />
         <tli id="T222" />
         <tli id="T223" />
         <tli id="T224" />
         <tli id="T225" />
         <tli id="T226" />
         <tli id="T227" />
         <tli id="T228" />
         <tli id="T229" />
         <tli id="T230" />
         <tli id="T231" />
         <tli id="T232" />
         <tli id="T233" />
         <tli id="T234" />
         <tli id="T235" />
         <tli id="T236" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="AIN"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T236" id="Seg_0" n="sc" s="T0">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Šide</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">tʼaktə</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">nükezeŋ</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">amnobi</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">tʼejegən</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_20" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">Dĭzeŋ</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">obəlar</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">ešši</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">teppi</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_35" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">Oʔb</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">nüken</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">koʔbdo</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">ibi</ts>
                  <nts id="Seg_47" n="HIAT:ip">,</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">oʔb</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_53" n="HIAT:w" s="T14">nüken</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_56" n="HIAT:w" s="T15">nʼi</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">ibi</ts>
                  <nts id="Seg_60" n="HIAT:ip">.</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_63" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">Oʔb</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">nüke</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_71" n="HIAT:w" s="T19">nʼibə</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_74" n="HIAT:w" s="T20">tojarbi</ts>
                  <nts id="Seg_75" n="HIAT:ip">,</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_78" n="HIAT:w" s="T21">bostə</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_81" n="HIAT:w" s="T22">koʔbtə</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_84" n="HIAT:w" s="T23">tʼepsində</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_87" n="HIAT:w" s="T24">hembi</ts>
                  <nts id="Seg_88" n="HIAT:ip">.</nts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_91" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_93" n="HIAT:w" s="T25">Dĭgəttə</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_96" n="HIAT:w" s="T26">šobi</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_99" n="HIAT:w" s="T27">di</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_102" n="HIAT:w" s="T28">nüke</ts>
                  <nts id="Seg_103" n="HIAT:ip">,</nts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_106" n="HIAT:w" s="T29">pajlaʔ</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_109" n="HIAT:w" s="T30">mĭmbi</ts>
                  <nts id="Seg_110" n="HIAT:ip">,</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_113" n="HIAT:w" s="T31">šobi</ts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_117" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_119" n="HIAT:w" s="T32">Nʼit</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_122" n="HIAT:w" s="T33">naga</ts>
                  <nts id="Seg_123" n="HIAT:ip">,</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_126" n="HIAT:w" s="T34">koʔbdo</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_129" n="HIAT:w" s="T35">tʼepsində</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_132" n="HIAT:w" s="T36">iʔbə</ts>
                  <nts id="Seg_133" n="HIAT:ip">.</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_136" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_138" n="HIAT:w" s="T37">Di</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_141" n="HIAT:w" s="T38">nüke</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_144" n="HIAT:w" s="T39">korolaʔ</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_147" n="HIAT:w" s="T40">saʔməbi</ts>
                  <nts id="Seg_148" n="HIAT:ip">,</nts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_151" n="HIAT:w" s="T41">eššibə</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_154" n="HIAT:w" s="T42">ibi</ts>
                  <nts id="Seg_155" n="HIAT:ip">,</nts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_158" n="HIAT:w" s="T43">korobi</ts>
                  <nts id="Seg_159" n="HIAT:ip">,</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_162" n="HIAT:w" s="T44">baška</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_165" n="HIAT:w" s="T45">măjanə</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_168" n="HIAT:w" s="T46">kallaʔ</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_171" n="HIAT:w" s="T47">tʼürbi</ts>
                  <nts id="Seg_172" n="HIAT:ip">.</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_175" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_177" n="HIAT:w" s="T48">Tamnugən</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_180" n="HIAT:w" s="T49">nʼe</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_183" n="HIAT:w" s="T50">di</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_186" n="HIAT:w" s="T51">nüke</ts>
                  <nts id="Seg_187" n="HIAT:ip">,</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_190" n="HIAT:w" s="T52">tamnuʔgən</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_193" n="HIAT:w" s="T53">di</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_196" n="HIAT:w" s="T54">nʼekə</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_199" n="HIAT:w" s="T55">iʔbəleʔ</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_202" n="HIAT:w" s="T56">kojobi</ts>
                  <nts id="Seg_203" n="HIAT:ip">.</nts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_206" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_208" n="HIAT:w" s="T57">Nʼit</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_211" n="HIAT:w" s="T58">saməjlaʔ</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_214" n="HIAT:w" s="T59">tüšəleibi</ts>
                  <nts id="Seg_215" n="HIAT:ip">,</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_218" n="HIAT:w" s="T60">poʔto</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_221" n="HIAT:w" s="T61">tʼitleibi</ts>
                  <nts id="Seg_222" n="HIAT:ip">,</nts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_225" n="HIAT:w" s="T62">saməjlaʔ</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_228" n="HIAT:w" s="T63">mĭlleibi</ts>
                  <nts id="Seg_229" n="HIAT:ip">,</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_232" n="HIAT:w" s="T64">poʔto</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_235" n="HIAT:w" s="T65">tʼitleibi</ts>
                  <nts id="Seg_236" n="HIAT:ip">.</nts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_239" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_241" n="HIAT:w" s="T66">Ti</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_244" n="HIAT:w" s="T67">nüke</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_247" n="HIAT:w" s="T68">uja</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_250" n="HIAT:w" s="T69">sil</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_253" n="HIAT:w" s="T70">amneibi</ts>
                  <nts id="Seg_254" n="HIAT:ip">.</nts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_257" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_259" n="HIAT:w" s="T71">Di</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_262" n="HIAT:w" s="T72">nʼi</ts>
                  <nts id="Seg_263" n="HIAT:ip">,</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_266" n="HIAT:w" s="T73">tamnugən</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_269" n="HIAT:w" s="T74">nʼi</ts>
                  <nts id="Seg_270" n="HIAT:ip">,</nts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_273" n="HIAT:w" s="T75">ijabə</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_276" n="HIAT:w" s="T76">sorarbi:</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_278" n="HIAT:ip">"</nts>
                  <ts e="T78" id="Seg_280" n="HIAT:w" s="T77">Tăn</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_283" n="HIAT:w" s="T78">moʔ</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_286" n="HIAT:w" s="T79">müjəl</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_289" n="HIAT:w" s="T80">nagur</ts>
                  <nts id="Seg_290" n="HIAT:ip">?</nts>
                  <nts id="Seg_291" n="HIAT:ip">"</nts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_294" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_296" n="HIAT:w" s="T81">Mălie:</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_298" n="HIAT:ip">"</nts>
                  <ts e="T83" id="Seg_300" n="HIAT:w" s="T82">Tăn</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_303" n="HIAT:w" s="T83">ej</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_306" n="HIAT:w" s="T84">măn</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_309" n="HIAT:w" s="T85">nʼim</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_312" n="HIAT:w" s="T86">igel</ts>
                  <nts id="Seg_313" n="HIAT:ip">,</nts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_316" n="HIAT:w" s="T87">baška</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_319" n="HIAT:w" s="T88">nüken</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_322" n="HIAT:w" s="T89">nʼit</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_325" n="HIAT:w" s="T90">ibiel</ts>
                  <nts id="Seg_326" n="HIAT:ip">.</nts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_329" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_331" n="HIAT:w" s="T91">Măn</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_334" n="HIAT:w" s="T92">koʔbdom</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_337" n="HIAT:w" s="T93">ibi</ts>
                  <nts id="Seg_338" n="HIAT:ip">.</nts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_341" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_343" n="HIAT:w" s="T94">Măn</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_346" n="HIAT:w" s="T95">tojarbiom</ts>
                  <nts id="Seg_347" n="HIAT:ip">,</nts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_350" n="HIAT:w" s="T96">tʼepsində</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_353" n="HIAT:w" s="T97">embiöm</ts>
                  <nts id="Seg_354" n="HIAT:ip">.</nts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T104" id="Seg_357" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_359" n="HIAT:w" s="T98">Tĭgəttə</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_362" n="HIAT:w" s="T99">korolaʔ</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_365" n="HIAT:w" s="T100">kallaʔ</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_368" n="HIAT:w" s="T101">tʼürbi</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_371" n="HIAT:w" s="T102">baškanə</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_374" n="HIAT:w" s="T103">măjanə</ts>
                  <nts id="Seg_375" n="HIAT:ip">.</nts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_378" n="HIAT:u" s="T104">
                  <ts e="T105" id="Seg_380" n="HIAT:w" s="T104">Šü</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_383" n="HIAT:w" s="T105">sagər</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_386" n="HIAT:w" s="T106">măjagən</ts>
                  <nts id="Seg_387" n="HIAT:ip">,</nts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_390" n="HIAT:w" s="T107">dĭgən</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_393" n="HIAT:w" s="T108">amna</ts>
                  <nts id="Seg_394" n="HIAT:ip">.</nts>
                  <nts id="Seg_395" n="HIAT:ip">"</nts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_398" n="HIAT:u" s="T109">
                  <nts id="Seg_399" n="HIAT:ip">"</nts>
                  <ts e="T110" id="Seg_401" n="HIAT:w" s="T109">Teinen</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_404" n="HIAT:w" s="T110">pa</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_407" n="HIAT:w" s="T111">iʔ</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_410" n="HIAT:w" s="T112">pajəʔ</ts>
                  <nts id="Seg_411" n="HIAT:ip">!</nts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_414" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_416" n="HIAT:w" s="T113">Teinen</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_419" n="HIAT:w" s="T114">kaza</ts>
                  <nts id="Seg_420" n="HIAT:ip">,</nts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_423" n="HIAT:w" s="T115">pan</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_426" n="HIAT:w" s="T116">šomin</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_429" n="HIAT:w" s="T117">kazabə</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_432" n="HIAT:w" s="T118">oʔbdʼət</ts>
                  <nts id="Seg_433" n="HIAT:ip">!</nts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_436" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_438" n="HIAT:w" s="T119">Šaːləl</ts>
                  <nts id="Seg_439" n="HIAT:ip">.</nts>
                  <nts id="Seg_440" n="HIAT:ip">"</nts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_443" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_445" n="HIAT:w" s="T120">Kaza</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_448" n="HIAT:w" s="T121">oʔbdəbi</ts>
                  <nts id="Seg_449" n="HIAT:ip">,</nts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_452" n="HIAT:w" s="T122">šü</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_455" n="HIAT:w" s="T123">hembi</ts>
                  <nts id="Seg_456" n="HIAT:ip">.</nts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_459" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_461" n="HIAT:w" s="T124">Šüt</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_464" n="HIAT:w" s="T125">kuʔbdolaːmbi</ts>
                  <nts id="Seg_465" n="HIAT:ip">.</nts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_468" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_470" n="HIAT:w" s="T126">Uja</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_473" n="HIAT:w" s="T127">amnaʔ</ts>
                  <nts id="Seg_474" n="HIAT:ip">.</nts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_477" n="HIAT:u" s="T128">
                  <nts id="Seg_478" n="HIAT:ip">"</nts>
                  <ts e="T129" id="Seg_480" n="HIAT:w" s="T128">Kazatsʼəʔ</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_483" n="HIAT:w" s="T129">hendə</ts>
                  <nts id="Seg_484" n="HIAT:ip">!</nts>
                  <nts id="Seg_485" n="HIAT:ip">"</nts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_488" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_490" n="HIAT:w" s="T130">Šü</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_493" n="HIAT:w" s="T131">kazatsʼəʔ</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_496" n="HIAT:w" s="T132">hembi</ts>
                  <nts id="Seg_497" n="HIAT:ip">.</nts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_500" n="HIAT:u" s="T133">
                  <ts e="T134" id="Seg_502" n="HIAT:w" s="T133">Dĭgəttə</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_505" n="HIAT:w" s="T134">šüt</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_508" n="HIAT:w" s="T135">kuʔbdolaga</ts>
                  <nts id="Seg_509" n="HIAT:ip">.</nts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_512" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_514" n="HIAT:w" s="T136">Kambi</ts>
                  <nts id="Seg_515" n="HIAT:ip">,</nts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_518" n="HIAT:w" s="T137">šübə</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_521" n="HIAT:w" s="T138">püʔleʔbi</ts>
                  <nts id="Seg_522" n="HIAT:ip">.</nts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_525" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_527" n="HIAT:w" s="T139">Dĭgəttə</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_530" n="HIAT:w" s="T140">simat</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_533" n="HIAT:w" s="T141">păktəlaʔ</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_536" n="HIAT:w" s="T142">saʔməbi</ts>
                  <nts id="Seg_537" n="HIAT:ip">.</nts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_540" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_542" n="HIAT:w" s="T143">Dĭgəttə</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_545" n="HIAT:w" s="T144">püʔleʔbi</ts>
                  <nts id="Seg_546" n="HIAT:ip">,</nts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_549" n="HIAT:w" s="T145">ami</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_552" n="HIAT:w" s="T146">simat</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_555" n="HIAT:w" s="T147">păktəlaʔ</ts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_558" n="HIAT:w" s="T148">saʔməbi</ts>
                  <nts id="Seg_559" n="HIAT:ip">.</nts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_562" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_564" n="HIAT:w" s="T149">Ami</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_567" n="HIAT:w" s="T150">simat</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_570" n="HIAT:w" s="T151">păktəlaʔ</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_573" n="HIAT:w" s="T152">saʔməbi</ts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_576" n="HIAT:w" s="T153">külaːmbi</ts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_579" n="HIAT:w" s="T154">dĭ</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_582" n="HIAT:w" s="T155">nüke</ts>
                  <nts id="Seg_583" n="HIAT:ip">.</nts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_586" n="HIAT:u" s="T156">
                  <ts e="T157" id="Seg_588" n="HIAT:w" s="T156">Nʼi</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_591" n="HIAT:w" s="T157">uja</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_594" n="HIAT:w" s="T158">sil</ts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_597" n="HIAT:w" s="T159">ibi</ts>
                  <nts id="Seg_598" n="HIAT:ip">.</nts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T163" id="Seg_601" n="HIAT:u" s="T160">
                  <ts e="T161" id="Seg_603" n="HIAT:w" s="T160">Dĭgəttə</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_606" n="HIAT:w" s="T161">kallaʔ</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_609" n="HIAT:w" s="T162">tʼürbi</ts>
                  <nts id="Seg_610" n="HIAT:ip">.</nts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T169" id="Seg_613" n="HIAT:u" s="T163">
                  <ts e="T164" id="Seg_615" n="HIAT:w" s="T163">Ijabə</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_618" n="HIAT:w" s="T164">pʼeleʔ</ts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_621" n="HIAT:w" s="T165">kambi</ts>
                  <nts id="Seg_622" n="HIAT:ip">,</nts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_625" n="HIAT:w" s="T166">urgo</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_628" n="HIAT:w" s="T167">măjanə</ts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_631" n="HIAT:w" s="T168">kambi</ts>
                  <nts id="Seg_632" n="HIAT:ip">.</nts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T175" id="Seg_635" n="HIAT:u" s="T169">
                  <ts e="T170" id="Seg_637" n="HIAT:w" s="T169">Urgo</ts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_640" n="HIAT:w" s="T170">sagər</ts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_643" n="HIAT:w" s="T171">măjagən</ts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_646" n="HIAT:w" s="T172">kubi:</ts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_649" n="HIAT:w" s="T173">Maʔ</ts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_652" n="HIAT:w" s="T174">nuga</ts>
                  <nts id="Seg_653" n="HIAT:ip">.</nts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T182" id="Seg_656" n="HIAT:u" s="T175">
                  <ts e="T176" id="Seg_658" n="HIAT:w" s="T175">Dĭ</ts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_661" n="HIAT:w" s="T176">maʔgən</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_664" n="HIAT:w" s="T177">šü</ts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_667" n="HIAT:w" s="T178">neileʔbə</ts>
                  <nts id="Seg_668" n="HIAT:ip">,</nts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_671" n="HIAT:w" s="T179">bor</ts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_674" n="HIAT:w" s="T180">mazərogəʔ</ts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_677" n="HIAT:w" s="T181">uʔlaʔbə</ts>
                  <nts id="Seg_678" n="HIAT:ip">.</nts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_681" n="HIAT:u" s="T182">
                  <ts e="T183" id="Seg_683" n="HIAT:w" s="T182">Dĭgəttə</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_686" n="HIAT:w" s="T183">maan</ts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_689" n="HIAT:w" s="T184">nĭgəndə</ts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_692" n="HIAT:w" s="T185">sʼalaːmbi</ts>
                  <nts id="Seg_693" n="HIAT:ip">.</nts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T190" id="Seg_696" n="HIAT:u" s="T186">
                  <ts e="T187" id="Seg_698" n="HIAT:w" s="T186">Dĭgəʔ</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_701" n="HIAT:w" s="T187">mazərogəʔ</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_704" n="HIAT:w" s="T188">tʼüdə</ts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_707" n="HIAT:w" s="T189">măndəbi</ts>
                  <nts id="Seg_708" n="HIAT:ip">.</nts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T196" id="Seg_711" n="HIAT:u" s="T190">
                  <ts e="T191" id="Seg_713" n="HIAT:w" s="T190">Ijat</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_716" n="HIAT:w" s="T191">amna</ts>
                  <nts id="Seg_717" n="HIAT:ip">,</nts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_720" n="HIAT:w" s="T192">tamnugən</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_723" n="HIAT:w" s="T193">nʼe</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_726" n="HIAT:w" s="T194">koʔbdo</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_729" n="HIAT:w" s="T195">amna</ts>
                  <nts id="Seg_730" n="HIAT:ip">.</nts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T201" id="Seg_733" n="HIAT:u" s="T196">
                  <ts e="T197" id="Seg_735" n="HIAT:w" s="T196">Dĭ</ts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_738" n="HIAT:w" s="T197">mazərogəʔ</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_741" n="HIAT:w" s="T198">aspaʔdə</ts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_744" n="HIAT:w" s="T199">sil</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_747" n="HIAT:w" s="T200">püdəldəbi</ts>
                  <nts id="Seg_748" n="HIAT:ip">.</nts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T206" id="Seg_751" n="HIAT:u" s="T201">
                  <ts e="T202" id="Seg_753" n="HIAT:w" s="T201">Koʔbdo</ts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_756" n="HIAT:w" s="T202">mălia:</ts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_758" n="HIAT:ip">"</nts>
                  <ts e="T204" id="Seg_760" n="HIAT:w" s="T203">Aspaʔnʼibaʔ</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_763" n="HIAT:w" s="T204">sil</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_766" n="HIAT:w" s="T205">mĭlleʔbə</ts>
                  <nts id="Seg_767" n="HIAT:ip">.</nts>
                  <nts id="Seg_768" n="HIAT:ip">"</nts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T213" id="Seg_771" n="HIAT:u" s="T206">
                  <ts e="T207" id="Seg_773" n="HIAT:w" s="T206">Tʼaktə</ts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_776" n="HIAT:w" s="T207">nüke</ts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_779" n="HIAT:w" s="T208">pa</ts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_782" n="HIAT:w" s="T209">kabarbi</ts>
                  <nts id="Seg_783" n="HIAT:ip">,</nts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_786" n="HIAT:w" s="T210">koʔbdobə</ts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_789" n="HIAT:w" s="T211">uluttə</ts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_792" n="HIAT:w" s="T212">toʔluʔbi</ts>
                  <nts id="Seg_793" n="HIAT:ip">.</nts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T215" id="Seg_796" n="HIAT:u" s="T213">
                  <nts id="Seg_797" n="HIAT:ip">"</nts>
                  <ts e="T214" id="Seg_799" n="HIAT:w" s="T213">Iʔ</ts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_802" n="HIAT:w" s="T214">šʼaːmaʔ</ts>
                  <nts id="Seg_803" n="HIAT:ip">!</nts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T220" id="Seg_806" n="HIAT:u" s="T215">
                  <ts e="T216" id="Seg_808" n="HIAT:w" s="T215">Tăn</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_811" n="HIAT:w" s="T216">ijal</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_814" n="HIAT:w" s="T217">sil</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_817" n="HIAT:w" s="T218">uja</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_820" n="HIAT:w" s="T219">amnaʔbə</ts>
                  <nts id="Seg_821" n="HIAT:ip">.</nts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T223" id="Seg_824" n="HIAT:u" s="T220">
                  <ts e="T221" id="Seg_826" n="HIAT:w" s="T220">Măn</ts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_829" n="HIAT:w" s="T221">togul</ts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_832" n="HIAT:w" s="T222">amnam</ts>
                  <nts id="Seg_833" n="HIAT:ip">.</nts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T227" id="Seg_836" n="HIAT:u" s="T223">
                  <ts e="T224" id="Seg_838" n="HIAT:w" s="T223">Tăn</ts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_841" n="HIAT:w" s="T224">ersə</ts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_844" n="HIAT:w" s="T225">ijalzʼəʔ</ts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_847" n="HIAT:w" s="T226">kudʼər</ts>
                  <nts id="Seg_848" n="HIAT:ip">.</nts>
                  <nts id="Seg_849" n="HIAT:ip">"</nts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T229" id="Seg_852" n="HIAT:u" s="T227">
                  <ts e="T228" id="Seg_854" n="HIAT:w" s="T227">Örerleʔ</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_857" n="HIAT:w" s="T228">ibi</ts>
                  <nts id="Seg_858" n="HIAT:ip">.</nts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T236" id="Seg_861" n="HIAT:u" s="T229">
                  <ts e="T230" id="Seg_863" n="HIAT:w" s="T229">Dĭgəttə</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_866" n="HIAT:w" s="T230">di</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_869" n="HIAT:w" s="T231">nʼi</ts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_872" n="HIAT:w" s="T232">mazərogəʔ</ts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_875" n="HIAT:w" s="T233">üzəbi</ts>
                  <nts id="Seg_876" n="HIAT:ip">,</nts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_879" n="HIAT:w" s="T234">maʔdə</ts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_882" n="HIAT:w" s="T235">šübi</ts>
                  <nts id="Seg_883" n="HIAT:ip">.</nts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T236" id="Seg_885" n="sc" s="T0">
               <ts e="T1" id="Seg_887" n="e" s="T0">Šide </ts>
               <ts e="T2" id="Seg_889" n="e" s="T1">tʼaktə </ts>
               <ts e="T3" id="Seg_891" n="e" s="T2">nükezeŋ </ts>
               <ts e="T4" id="Seg_893" n="e" s="T3">amnobi </ts>
               <ts e="T5" id="Seg_895" n="e" s="T4">tʼejegən. </ts>
               <ts e="T6" id="Seg_897" n="e" s="T5">Dĭzeŋ </ts>
               <ts e="T7" id="Seg_899" n="e" s="T6">obəlar </ts>
               <ts e="T8" id="Seg_901" n="e" s="T7">ešši </ts>
               <ts e="T9" id="Seg_903" n="e" s="T8">teppi. </ts>
               <ts e="T10" id="Seg_905" n="e" s="T9">Oʔb </ts>
               <ts e="T11" id="Seg_907" n="e" s="T10">nüken </ts>
               <ts e="T12" id="Seg_909" n="e" s="T11">koʔbdo </ts>
               <ts e="T13" id="Seg_911" n="e" s="T12">ibi, </ts>
               <ts e="T14" id="Seg_913" n="e" s="T13">oʔb </ts>
               <ts e="T15" id="Seg_915" n="e" s="T14">nüken </ts>
               <ts e="T16" id="Seg_917" n="e" s="T15">nʼi </ts>
               <ts e="T17" id="Seg_919" n="e" s="T16">ibi. </ts>
               <ts e="T18" id="Seg_921" n="e" s="T17">Oʔb </ts>
               <ts e="T19" id="Seg_923" n="e" s="T18">nüke </ts>
               <ts e="T20" id="Seg_925" n="e" s="T19">nʼibə </ts>
               <ts e="T21" id="Seg_927" n="e" s="T20">tojarbi, </ts>
               <ts e="T22" id="Seg_929" n="e" s="T21">bostə </ts>
               <ts e="T23" id="Seg_931" n="e" s="T22">koʔbtə </ts>
               <ts e="T24" id="Seg_933" n="e" s="T23">tʼepsində </ts>
               <ts e="T25" id="Seg_935" n="e" s="T24">hembi. </ts>
               <ts e="T26" id="Seg_937" n="e" s="T25">Dĭgəttə </ts>
               <ts e="T27" id="Seg_939" n="e" s="T26">šobi </ts>
               <ts e="T28" id="Seg_941" n="e" s="T27">di </ts>
               <ts e="T29" id="Seg_943" n="e" s="T28">nüke, </ts>
               <ts e="T30" id="Seg_945" n="e" s="T29">pajlaʔ </ts>
               <ts e="T31" id="Seg_947" n="e" s="T30">mĭmbi, </ts>
               <ts e="T32" id="Seg_949" n="e" s="T31">šobi. </ts>
               <ts e="T33" id="Seg_951" n="e" s="T32">Nʼit </ts>
               <ts e="T34" id="Seg_953" n="e" s="T33">naga, </ts>
               <ts e="T35" id="Seg_955" n="e" s="T34">koʔbdo </ts>
               <ts e="T36" id="Seg_957" n="e" s="T35">tʼepsində </ts>
               <ts e="T37" id="Seg_959" n="e" s="T36">iʔbə. </ts>
               <ts e="T38" id="Seg_961" n="e" s="T37">Di </ts>
               <ts e="T39" id="Seg_963" n="e" s="T38">nüke </ts>
               <ts e="T40" id="Seg_965" n="e" s="T39">korolaʔ </ts>
               <ts e="T41" id="Seg_967" n="e" s="T40">saʔməbi, </ts>
               <ts e="T42" id="Seg_969" n="e" s="T41">eššibə </ts>
               <ts e="T43" id="Seg_971" n="e" s="T42">ibi, </ts>
               <ts e="T44" id="Seg_973" n="e" s="T43">korobi, </ts>
               <ts e="T45" id="Seg_975" n="e" s="T44">baška </ts>
               <ts e="T46" id="Seg_977" n="e" s="T45">măjanə </ts>
               <ts e="T47" id="Seg_979" n="e" s="T46">kallaʔ </ts>
               <ts e="T48" id="Seg_981" n="e" s="T47">tʼürbi. </ts>
               <ts e="T49" id="Seg_983" n="e" s="T48">Tamnugən </ts>
               <ts e="T50" id="Seg_985" n="e" s="T49">nʼe </ts>
               <ts e="T51" id="Seg_987" n="e" s="T50">di </ts>
               <ts e="T52" id="Seg_989" n="e" s="T51">nüke, </ts>
               <ts e="T53" id="Seg_991" n="e" s="T52">tamnuʔgən </ts>
               <ts e="T54" id="Seg_993" n="e" s="T53">di </ts>
               <ts e="T55" id="Seg_995" n="e" s="T54">nʼekə </ts>
               <ts e="T56" id="Seg_997" n="e" s="T55">iʔbəleʔ </ts>
               <ts e="T57" id="Seg_999" n="e" s="T56">kojobi. </ts>
               <ts e="T58" id="Seg_1001" n="e" s="T57">Nʼit </ts>
               <ts e="T59" id="Seg_1003" n="e" s="T58">saməjlaʔ </ts>
               <ts e="T60" id="Seg_1005" n="e" s="T59">tüšəleibi, </ts>
               <ts e="T61" id="Seg_1007" n="e" s="T60">poʔto </ts>
               <ts e="T62" id="Seg_1009" n="e" s="T61">tʼitleibi, </ts>
               <ts e="T63" id="Seg_1011" n="e" s="T62">saməjlaʔ </ts>
               <ts e="T64" id="Seg_1013" n="e" s="T63">mĭlleibi, </ts>
               <ts e="T65" id="Seg_1015" n="e" s="T64">poʔto </ts>
               <ts e="T66" id="Seg_1017" n="e" s="T65">tʼitleibi. </ts>
               <ts e="T67" id="Seg_1019" n="e" s="T66">Ti </ts>
               <ts e="T68" id="Seg_1021" n="e" s="T67">nüke </ts>
               <ts e="T69" id="Seg_1023" n="e" s="T68">uja </ts>
               <ts e="T70" id="Seg_1025" n="e" s="T69">sil </ts>
               <ts e="T71" id="Seg_1027" n="e" s="T70">amneibi. </ts>
               <ts e="T72" id="Seg_1029" n="e" s="T71">Di </ts>
               <ts e="T73" id="Seg_1031" n="e" s="T72">nʼi, </ts>
               <ts e="T74" id="Seg_1033" n="e" s="T73">tamnugən </ts>
               <ts e="T75" id="Seg_1035" n="e" s="T74">nʼi, </ts>
               <ts e="T76" id="Seg_1037" n="e" s="T75">ijabə </ts>
               <ts e="T77" id="Seg_1039" n="e" s="T76">sorarbi: </ts>
               <ts e="T78" id="Seg_1041" n="e" s="T77">"Tăn </ts>
               <ts e="T79" id="Seg_1043" n="e" s="T78">moʔ </ts>
               <ts e="T80" id="Seg_1045" n="e" s="T79">müjəl </ts>
               <ts e="T81" id="Seg_1047" n="e" s="T80">nagur?" </ts>
               <ts e="T82" id="Seg_1049" n="e" s="T81">Mălie: </ts>
               <ts e="T83" id="Seg_1051" n="e" s="T82">"Tăn </ts>
               <ts e="T84" id="Seg_1053" n="e" s="T83">ej </ts>
               <ts e="T85" id="Seg_1055" n="e" s="T84">măn </ts>
               <ts e="T86" id="Seg_1057" n="e" s="T85">nʼim </ts>
               <ts e="T87" id="Seg_1059" n="e" s="T86">igel, </ts>
               <ts e="T88" id="Seg_1061" n="e" s="T87">baška </ts>
               <ts e="T89" id="Seg_1063" n="e" s="T88">nüken </ts>
               <ts e="T90" id="Seg_1065" n="e" s="T89">nʼit </ts>
               <ts e="T91" id="Seg_1067" n="e" s="T90">ibiel. </ts>
               <ts e="T92" id="Seg_1069" n="e" s="T91">Măn </ts>
               <ts e="T93" id="Seg_1071" n="e" s="T92">koʔbdom </ts>
               <ts e="T94" id="Seg_1073" n="e" s="T93">ibi. </ts>
               <ts e="T95" id="Seg_1075" n="e" s="T94">Măn </ts>
               <ts e="T96" id="Seg_1077" n="e" s="T95">tojarbiom, </ts>
               <ts e="T97" id="Seg_1079" n="e" s="T96">tʼepsində </ts>
               <ts e="T98" id="Seg_1081" n="e" s="T97">embiöm. </ts>
               <ts e="T99" id="Seg_1083" n="e" s="T98">Tĭgəttə </ts>
               <ts e="T100" id="Seg_1085" n="e" s="T99">korolaʔ </ts>
               <ts e="T101" id="Seg_1087" n="e" s="T100">kallaʔ </ts>
               <ts e="T102" id="Seg_1089" n="e" s="T101">tʼürbi </ts>
               <ts e="T103" id="Seg_1091" n="e" s="T102">baškanə </ts>
               <ts e="T104" id="Seg_1093" n="e" s="T103">măjanə. </ts>
               <ts e="T105" id="Seg_1095" n="e" s="T104">Šü </ts>
               <ts e="T106" id="Seg_1097" n="e" s="T105">sagər </ts>
               <ts e="T107" id="Seg_1099" n="e" s="T106">măjagən, </ts>
               <ts e="T108" id="Seg_1101" n="e" s="T107">dĭgən </ts>
               <ts e="T109" id="Seg_1103" n="e" s="T108">amna." </ts>
               <ts e="T110" id="Seg_1105" n="e" s="T109">"Teinen </ts>
               <ts e="T111" id="Seg_1107" n="e" s="T110">pa </ts>
               <ts e="T112" id="Seg_1109" n="e" s="T111">iʔ </ts>
               <ts e="T113" id="Seg_1111" n="e" s="T112">pajəʔ! </ts>
               <ts e="T114" id="Seg_1113" n="e" s="T113">Teinen </ts>
               <ts e="T115" id="Seg_1115" n="e" s="T114">kaza, </ts>
               <ts e="T116" id="Seg_1117" n="e" s="T115">pan </ts>
               <ts e="T117" id="Seg_1119" n="e" s="T116">šomin </ts>
               <ts e="T118" id="Seg_1121" n="e" s="T117">kazabə </ts>
               <ts e="T119" id="Seg_1123" n="e" s="T118">oʔbdʼət! </ts>
               <ts e="T120" id="Seg_1125" n="e" s="T119">Šaːləl." </ts>
               <ts e="T121" id="Seg_1127" n="e" s="T120">Kaza </ts>
               <ts e="T122" id="Seg_1129" n="e" s="T121">oʔbdəbi, </ts>
               <ts e="T123" id="Seg_1131" n="e" s="T122">šü </ts>
               <ts e="T124" id="Seg_1133" n="e" s="T123">hembi. </ts>
               <ts e="T125" id="Seg_1135" n="e" s="T124">Šüt </ts>
               <ts e="T126" id="Seg_1137" n="e" s="T125">kuʔbdolaːmbi. </ts>
               <ts e="T127" id="Seg_1139" n="e" s="T126">Uja </ts>
               <ts e="T128" id="Seg_1141" n="e" s="T127">amnaʔ. </ts>
               <ts e="T129" id="Seg_1143" n="e" s="T128">"Kazatsʼəʔ </ts>
               <ts e="T130" id="Seg_1145" n="e" s="T129">hendə!" </ts>
               <ts e="T131" id="Seg_1147" n="e" s="T130">Šü </ts>
               <ts e="T132" id="Seg_1149" n="e" s="T131">kazatsʼəʔ </ts>
               <ts e="T133" id="Seg_1151" n="e" s="T132">hembi. </ts>
               <ts e="T134" id="Seg_1153" n="e" s="T133">Dĭgəttə </ts>
               <ts e="T135" id="Seg_1155" n="e" s="T134">šüt </ts>
               <ts e="T136" id="Seg_1157" n="e" s="T135">kuʔbdolaga. </ts>
               <ts e="T137" id="Seg_1159" n="e" s="T136">Kambi, </ts>
               <ts e="T138" id="Seg_1161" n="e" s="T137">šübə </ts>
               <ts e="T139" id="Seg_1163" n="e" s="T138">püʔleʔbi. </ts>
               <ts e="T140" id="Seg_1165" n="e" s="T139">Dĭgəttə </ts>
               <ts e="T141" id="Seg_1167" n="e" s="T140">simat </ts>
               <ts e="T142" id="Seg_1169" n="e" s="T141">păktəlaʔ </ts>
               <ts e="T143" id="Seg_1171" n="e" s="T142">saʔməbi. </ts>
               <ts e="T144" id="Seg_1173" n="e" s="T143">Dĭgəttə </ts>
               <ts e="T145" id="Seg_1175" n="e" s="T144">püʔleʔbi, </ts>
               <ts e="T146" id="Seg_1177" n="e" s="T145">ami </ts>
               <ts e="T147" id="Seg_1179" n="e" s="T146">simat </ts>
               <ts e="T148" id="Seg_1181" n="e" s="T147">păktəlaʔ </ts>
               <ts e="T149" id="Seg_1183" n="e" s="T148">saʔməbi. </ts>
               <ts e="T150" id="Seg_1185" n="e" s="T149">Ami </ts>
               <ts e="T151" id="Seg_1187" n="e" s="T150">simat </ts>
               <ts e="T152" id="Seg_1189" n="e" s="T151">păktəlaʔ </ts>
               <ts e="T153" id="Seg_1191" n="e" s="T152">saʔməbi </ts>
               <ts e="T154" id="Seg_1193" n="e" s="T153">külaːmbi </ts>
               <ts e="T155" id="Seg_1195" n="e" s="T154">dĭ </ts>
               <ts e="T156" id="Seg_1197" n="e" s="T155">nüke. </ts>
               <ts e="T157" id="Seg_1199" n="e" s="T156">Nʼi </ts>
               <ts e="T158" id="Seg_1201" n="e" s="T157">uja </ts>
               <ts e="T159" id="Seg_1203" n="e" s="T158">sil </ts>
               <ts e="T160" id="Seg_1205" n="e" s="T159">ibi. </ts>
               <ts e="T161" id="Seg_1207" n="e" s="T160">Dĭgəttə </ts>
               <ts e="T162" id="Seg_1209" n="e" s="T161">kallaʔ </ts>
               <ts e="T163" id="Seg_1211" n="e" s="T162">tʼürbi. </ts>
               <ts e="T164" id="Seg_1213" n="e" s="T163">Ijabə </ts>
               <ts e="T165" id="Seg_1215" n="e" s="T164">pʼeleʔ </ts>
               <ts e="T166" id="Seg_1217" n="e" s="T165">kambi, </ts>
               <ts e="T167" id="Seg_1219" n="e" s="T166">urgo </ts>
               <ts e="T168" id="Seg_1221" n="e" s="T167">măjanə </ts>
               <ts e="T169" id="Seg_1223" n="e" s="T168">kambi. </ts>
               <ts e="T170" id="Seg_1225" n="e" s="T169">Urgo </ts>
               <ts e="T171" id="Seg_1227" n="e" s="T170">sagər </ts>
               <ts e="T172" id="Seg_1229" n="e" s="T171">măjagən </ts>
               <ts e="T173" id="Seg_1231" n="e" s="T172">kubi: </ts>
               <ts e="T174" id="Seg_1233" n="e" s="T173">Maʔ </ts>
               <ts e="T175" id="Seg_1235" n="e" s="T174">nuga. </ts>
               <ts e="T176" id="Seg_1237" n="e" s="T175">Dĭ </ts>
               <ts e="T177" id="Seg_1239" n="e" s="T176">maʔgən </ts>
               <ts e="T178" id="Seg_1241" n="e" s="T177">šü </ts>
               <ts e="T179" id="Seg_1243" n="e" s="T178">neileʔbə, </ts>
               <ts e="T180" id="Seg_1245" n="e" s="T179">bor </ts>
               <ts e="T181" id="Seg_1247" n="e" s="T180">mazərogəʔ </ts>
               <ts e="T182" id="Seg_1249" n="e" s="T181">uʔlaʔbə. </ts>
               <ts e="T183" id="Seg_1251" n="e" s="T182">Dĭgəttə </ts>
               <ts e="T184" id="Seg_1253" n="e" s="T183">maan </ts>
               <ts e="T185" id="Seg_1255" n="e" s="T184">nĭgəndə </ts>
               <ts e="T186" id="Seg_1257" n="e" s="T185">sʼalaːmbi. </ts>
               <ts e="T187" id="Seg_1259" n="e" s="T186">Dĭgəʔ </ts>
               <ts e="T188" id="Seg_1261" n="e" s="T187">mazərogəʔ </ts>
               <ts e="T189" id="Seg_1263" n="e" s="T188">tʼüdə </ts>
               <ts e="T190" id="Seg_1265" n="e" s="T189">măndəbi. </ts>
               <ts e="T191" id="Seg_1267" n="e" s="T190">Ijat </ts>
               <ts e="T192" id="Seg_1269" n="e" s="T191">amna, </ts>
               <ts e="T193" id="Seg_1271" n="e" s="T192">tamnugən </ts>
               <ts e="T194" id="Seg_1273" n="e" s="T193">nʼe </ts>
               <ts e="T195" id="Seg_1275" n="e" s="T194">koʔbdo </ts>
               <ts e="T196" id="Seg_1277" n="e" s="T195">amna. </ts>
               <ts e="T197" id="Seg_1279" n="e" s="T196">Dĭ </ts>
               <ts e="T198" id="Seg_1281" n="e" s="T197">mazərogəʔ </ts>
               <ts e="T199" id="Seg_1283" n="e" s="T198">aspaʔdə </ts>
               <ts e="T200" id="Seg_1285" n="e" s="T199">sil </ts>
               <ts e="T201" id="Seg_1287" n="e" s="T200">püdəldəbi. </ts>
               <ts e="T202" id="Seg_1289" n="e" s="T201">Koʔbdo </ts>
               <ts e="T203" id="Seg_1291" n="e" s="T202">mălia: </ts>
               <ts e="T204" id="Seg_1293" n="e" s="T203">"Aspaʔnʼibaʔ </ts>
               <ts e="T205" id="Seg_1295" n="e" s="T204">sil </ts>
               <ts e="T206" id="Seg_1297" n="e" s="T205">mĭlleʔbə." </ts>
               <ts e="T207" id="Seg_1299" n="e" s="T206">Tʼaktə </ts>
               <ts e="T208" id="Seg_1301" n="e" s="T207">nüke </ts>
               <ts e="T209" id="Seg_1303" n="e" s="T208">pa </ts>
               <ts e="T210" id="Seg_1305" n="e" s="T209">kabarbi, </ts>
               <ts e="T211" id="Seg_1307" n="e" s="T210">koʔbdobə </ts>
               <ts e="T212" id="Seg_1309" n="e" s="T211">uluttə </ts>
               <ts e="T213" id="Seg_1311" n="e" s="T212">toʔluʔbi. </ts>
               <ts e="T214" id="Seg_1313" n="e" s="T213">"Iʔ </ts>
               <ts e="T215" id="Seg_1315" n="e" s="T214">šʼaːmaʔ! </ts>
               <ts e="T216" id="Seg_1317" n="e" s="T215">Tăn </ts>
               <ts e="T217" id="Seg_1319" n="e" s="T216">ijal </ts>
               <ts e="T218" id="Seg_1321" n="e" s="T217">sil </ts>
               <ts e="T219" id="Seg_1323" n="e" s="T218">uja </ts>
               <ts e="T220" id="Seg_1325" n="e" s="T219">amnaʔbə. </ts>
               <ts e="T221" id="Seg_1327" n="e" s="T220">Măn </ts>
               <ts e="T222" id="Seg_1329" n="e" s="T221">togul </ts>
               <ts e="T223" id="Seg_1331" n="e" s="T222">amnam. </ts>
               <ts e="T224" id="Seg_1333" n="e" s="T223">Tăn </ts>
               <ts e="T225" id="Seg_1335" n="e" s="T224">ersə </ts>
               <ts e="T226" id="Seg_1337" n="e" s="T225">ijalzʼəʔ </ts>
               <ts e="T227" id="Seg_1339" n="e" s="T226">kudʼər." </ts>
               <ts e="T228" id="Seg_1341" n="e" s="T227">Örerleʔ </ts>
               <ts e="T229" id="Seg_1343" n="e" s="T228">ibi. </ts>
               <ts e="T230" id="Seg_1345" n="e" s="T229">Dĭgəttə </ts>
               <ts e="T231" id="Seg_1347" n="e" s="T230">di </ts>
               <ts e="T232" id="Seg_1349" n="e" s="T231">nʼi </ts>
               <ts e="T233" id="Seg_1351" n="e" s="T232">mazərogəʔ </ts>
               <ts e="T234" id="Seg_1353" n="e" s="T233">üzəbi, </ts>
               <ts e="T235" id="Seg_1355" n="e" s="T234">maʔdə </ts>
               <ts e="T236" id="Seg_1357" n="e" s="T235">šübi. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_1358" s="T0">AIN_1912_Frogwoman_flk.001 (001.001)</ta>
            <ta e="T9" id="Seg_1359" s="T5">AIN_1912_Frogwoman_flk.002 (001.002)</ta>
            <ta e="T17" id="Seg_1360" s="T9">AIN_1912_Frogwoman_flk.003 (001.003)</ta>
            <ta e="T25" id="Seg_1361" s="T17">AIN_1912_Frogwoman_flk.004 (001.004)</ta>
            <ta e="T32" id="Seg_1362" s="T25">AIN_1912_Frogwoman_flk.005 (001.005)</ta>
            <ta e="T37" id="Seg_1363" s="T32">AIN_1912_Frogwoman_flk.006 (001.006)</ta>
            <ta e="T48" id="Seg_1364" s="T37">AIN_1912_Frogwoman_flk.007 (001.007)</ta>
            <ta e="T57" id="Seg_1365" s="T48">AIN_1912_Frogwoman_flk.008 (001.008)</ta>
            <ta e="T66" id="Seg_1366" s="T57">AIN_1912_Frogwoman_flk.009 (002.001)</ta>
            <ta e="T71" id="Seg_1367" s="T66">AIN_1912_Frogwoman_flk.010 (002.002)</ta>
            <ta e="T81" id="Seg_1368" s="T71">AIN_1912_Frogwoman_flk.011 (002.003)</ta>
            <ta e="T91" id="Seg_1369" s="T81">AIN_1912_Frogwoman_flk.012 (002.005)</ta>
            <ta e="T94" id="Seg_1370" s="T91">AIN_1912_Frogwoman_flk.013 (002.007)</ta>
            <ta e="T98" id="Seg_1371" s="T94">AIN_1912_Frogwoman_flk.014 (002.008)</ta>
            <ta e="T104" id="Seg_1372" s="T98">AIN_1912_Frogwoman_flk.015 (002.009)</ta>
            <ta e="T109" id="Seg_1373" s="T104">AIN_1912_Frogwoman_flk.016 (002.010)</ta>
            <ta e="T113" id="Seg_1374" s="T109">AIN_1912_Frogwoman_flk.017 (002.011)</ta>
            <ta e="T119" id="Seg_1375" s="T113">AIN_1912_Frogwoman_flk.018 (002.012)</ta>
            <ta e="T120" id="Seg_1376" s="T119">AIN_1912_Frogwoman_flk.019 (002.013)</ta>
            <ta e="T124" id="Seg_1377" s="T120">AIN_1912_Frogwoman_flk.020 (002.014)</ta>
            <ta e="T126" id="Seg_1378" s="T124">AIN_1912_Frogwoman_flk.021 (002.015)</ta>
            <ta e="T128" id="Seg_1379" s="T126">AIN_1912_Frogwoman_flk.022 (002.016)</ta>
            <ta e="T130" id="Seg_1380" s="T128">AIN_1912_Frogwoman_flk.023 (002.017)</ta>
            <ta e="T133" id="Seg_1381" s="T130">AIN_1912_Frogwoman_flk.024 (002.018)</ta>
            <ta e="T136" id="Seg_1382" s="T133">AIN_1912_Frogwoman_flk.025 (002.019)</ta>
            <ta e="T139" id="Seg_1383" s="T136">AIN_1912_Frogwoman_flk.026 (002.020)</ta>
            <ta e="T143" id="Seg_1384" s="T139">AIN_1912_Frogwoman_flk.027 (002.021)</ta>
            <ta e="T149" id="Seg_1385" s="T143">AIN_1912_Frogwoman_flk.028 (002.022)</ta>
            <ta e="T156" id="Seg_1386" s="T149">AIN_1912_Frogwoman_flk.029 (002.023)</ta>
            <ta e="T160" id="Seg_1387" s="T156">AIN_1912_Frogwoman_flk.030 (003.001)</ta>
            <ta e="T163" id="Seg_1388" s="T160">AIN_1912_Frogwoman_flk.031 (003.002)</ta>
            <ta e="T169" id="Seg_1389" s="T163">AIN_1912_Frogwoman_flk.032 (003.003)</ta>
            <ta e="T175" id="Seg_1390" s="T169">AIN_1912_Frogwoman_flk.033 (003.004)</ta>
            <ta e="T182" id="Seg_1391" s="T175">AIN_1912_Frogwoman_flk.034 (003.005)</ta>
            <ta e="T186" id="Seg_1392" s="T182">AIN_1912_Frogwoman_flk.035 (003.006)</ta>
            <ta e="T190" id="Seg_1393" s="T186">AIN_1912_Frogwoman_flk.036 (003.007)</ta>
            <ta e="T196" id="Seg_1394" s="T190">AIN_1912_Frogwoman_flk.037 (003.008)</ta>
            <ta e="T201" id="Seg_1395" s="T196">AIN_1912_Frogwoman_flk.038 (003.009)</ta>
            <ta e="T206" id="Seg_1396" s="T201">AIN_1912_Frogwoman_flk.039 (003.010)</ta>
            <ta e="T213" id="Seg_1397" s="T206">AIN_1912_Frogwoman_flk.040 (003.012)</ta>
            <ta e="T215" id="Seg_1398" s="T213">AIN_1912_Frogwoman_flk.041 (003.013)</ta>
            <ta e="T220" id="Seg_1399" s="T215">AIN_1912_Frogwoman_flk.042 (003.014)</ta>
            <ta e="T223" id="Seg_1400" s="T220">AIN_1912_Frogwoman_flk.043 (003.015)</ta>
            <ta e="T227" id="Seg_1401" s="T223">AIN_1912_Frogwoman_flk.044 (003.016)</ta>
            <ta e="T229" id="Seg_1402" s="T227">AIN_1912_Frogwoman_flk.045 (003.017)</ta>
            <ta e="T236" id="Seg_1403" s="T229">AIN_1912_Frogwoman_flk.046 (003.018)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_1404" s="T0">Šide tʼaktə nükezeŋ amnobi tʼejegən. </ta>
            <ta e="T9" id="Seg_1405" s="T5">Dĭzeŋ obəlar ešši teppi. </ta>
            <ta e="T17" id="Seg_1406" s="T9">Oʔb nüken koʔbdo ibi, oʔb nüken nʼi ibi. </ta>
            <ta e="T25" id="Seg_1407" s="T17">Oʔb nüke nʼibə tojarbi, bostə koʔbtə tʼepsində hembi. </ta>
            <ta e="T32" id="Seg_1408" s="T25">Dĭgəttə šobi di nüke, pajlaʔ mĭmbi, šobi. </ta>
            <ta e="T37" id="Seg_1409" s="T32">Nʼit naga, koʔbdo tʼepsində iʔbə. </ta>
            <ta e="T48" id="Seg_1410" s="T37">Di nüke korolaʔ saʔməbi, eššibə ibi, korobi, baška măjanə kallaʔ tʼürbi. </ta>
            <ta e="T57" id="Seg_1411" s="T48">Tamnugən nʼe di nüke, tamnuʔgən di nʼekə iʔbəleʔ kojobi. </ta>
            <ta e="T66" id="Seg_1412" s="T57">Nʼit saməjlaʔ tüšəleibi, poʔto tʼitleibi, saməjlaʔ mĭlleibi, poʔto tʼitleibi. </ta>
            <ta e="T71" id="Seg_1413" s="T66">Ti nüke uja sil amneibi. </ta>
            <ta e="T81" id="Seg_1414" s="T71">Di nʼi, tamnugən nʼi, ijabə sorarbi: "Tăn moʔ müjəl nagur?" </ta>
            <ta e="T91" id="Seg_1415" s="T81">Mălie: "Tăn ej măn nʼim igel, baška nüken nʼit ibiel. </ta>
            <ta e="T94" id="Seg_1416" s="T91">Măn koʔbdom ibi. </ta>
            <ta e="T98" id="Seg_1417" s="T94">Măn tojarbiom, tʼepsində embiöm. </ta>
            <ta e="T104" id="Seg_1418" s="T98">Tĭgəttə korolaʔ kallaʔ tʼürbi baškanə măjanə. </ta>
            <ta e="T109" id="Seg_1419" s="T104">Šü sagər măjagən, dĭgən amna." </ta>
            <ta e="T113" id="Seg_1420" s="T109">"Teinen pa iʔ pajəʔ! </ta>
            <ta e="T119" id="Seg_1421" s="T113">Teinen kaza, pan šomin kazabə oʔbdʼət! </ta>
            <ta e="T120" id="Seg_1422" s="T119">Šaːləl." </ta>
            <ta e="T124" id="Seg_1423" s="T120">Kaza oʔbdəbi, šü hembi. </ta>
            <ta e="T126" id="Seg_1424" s="T124">Šüt kuʔbdolaːmbi. </ta>
            <ta e="T128" id="Seg_1425" s="T126">Uja amnaʔ. </ta>
            <ta e="T130" id="Seg_1426" s="T128">"Kazatsʼəʔ hendə!" </ta>
            <ta e="T133" id="Seg_1427" s="T130">Šü kazatsʼəʔ hembi. </ta>
            <ta e="T136" id="Seg_1428" s="T133">Dĭgəttə šüt kuʔbdolaga. </ta>
            <ta e="T139" id="Seg_1429" s="T136">Kambi, šübə püʔleʔbi. </ta>
            <ta e="T143" id="Seg_1430" s="T139">Dĭgəttə simat păktəlaʔ saʔməbi. </ta>
            <ta e="T149" id="Seg_1431" s="T143">Dĭgəttə püʔleʔbi, ami simat păktəlaʔ saʔməbi. </ta>
            <ta e="T156" id="Seg_1432" s="T149">Ami simat păktəlaʔ saʔməbi külaːmbi dĭ nüke. </ta>
            <ta e="T160" id="Seg_1433" s="T156">Nʼi uja sil ibi. </ta>
            <ta e="T163" id="Seg_1434" s="T160">Dĭgəttə kallaʔ tʼürbi. </ta>
            <ta e="T169" id="Seg_1435" s="T163">Ijabə pʼeleʔ kambi, urgo măjanə kambi. </ta>
            <ta e="T175" id="Seg_1436" s="T169">Urgo sagər măjagən kubi: Maʔ nuga. </ta>
            <ta e="T182" id="Seg_1437" s="T175">Dĭ maʔgən šü neileʔbə, bor mazərogəʔ uʔlaʔbə. </ta>
            <ta e="T186" id="Seg_1438" s="T182">Dĭgəttə maan nĭgəndə sʼalaːmbi. </ta>
            <ta e="T190" id="Seg_1439" s="T186">Dĭgəʔ mazərogəʔ tʼüdə măndəbi. </ta>
            <ta e="T196" id="Seg_1440" s="T190">Ijat amna, tamnugən nʼe koʔbdo amna. </ta>
            <ta e="T201" id="Seg_1441" s="T196">Dĭ mazərogəʔ aspaʔdə sil püdəldəbi. </ta>
            <ta e="T206" id="Seg_1442" s="T201">Koʔbdo mălia: "Aspaʔnʼibaʔ sil mĭlleʔbə." </ta>
            <ta e="T213" id="Seg_1443" s="T206">Tʼaktə nüke pa kabarbi, koʔbdobə uluttə toʔluʔbi. </ta>
            <ta e="T215" id="Seg_1444" s="T213">"Iʔ šʼaːmaʔ! </ta>
            <ta e="T220" id="Seg_1445" s="T215">Tăn ijal sil uja amnaʔbə. </ta>
            <ta e="T223" id="Seg_1446" s="T220">Măn togul amnam. </ta>
            <ta e="T227" id="Seg_1447" s="T223">Tăn ersə ijalzʼəʔ kudʼər." </ta>
            <ta e="T229" id="Seg_1448" s="T227">Örerleʔ ibi. </ta>
            <ta e="T236" id="Seg_1449" s="T229">Dĭgəttə di nʼi mazərogəʔ üzəbi, maʔdə šübi. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1450" s="T0">šide</ta>
            <ta e="T2" id="Seg_1451" s="T1">tʼaktə</ta>
            <ta e="T3" id="Seg_1452" s="T2">nüke-zeŋ</ta>
            <ta e="T4" id="Seg_1453" s="T3">amno-bi</ta>
            <ta e="T5" id="Seg_1454" s="T4">tʼeje-gən</ta>
            <ta e="T6" id="Seg_1455" s="T5">dĭ-zeŋ</ta>
            <ta e="T7" id="Seg_1456" s="T6">obə-lar</ta>
            <ta e="T8" id="Seg_1457" s="T7">ešši</ta>
            <ta e="T9" id="Seg_1458" s="T8">tep-pi</ta>
            <ta e="T10" id="Seg_1459" s="T9">oʔb</ta>
            <ta e="T11" id="Seg_1460" s="T10">nüke-n</ta>
            <ta e="T12" id="Seg_1461" s="T11">koʔbdo</ta>
            <ta e="T13" id="Seg_1462" s="T12">i-bi</ta>
            <ta e="T14" id="Seg_1463" s="T13">oʔb</ta>
            <ta e="T15" id="Seg_1464" s="T14">nüke-n</ta>
            <ta e="T16" id="Seg_1465" s="T15">nʼi</ta>
            <ta e="T17" id="Seg_1466" s="T16">i-bi</ta>
            <ta e="T18" id="Seg_1467" s="T17">oʔb</ta>
            <ta e="T19" id="Seg_1468" s="T18">nüke</ta>
            <ta e="T20" id="Seg_1469" s="T19">nʼi-bə</ta>
            <ta e="T21" id="Seg_1470" s="T20">tojar-bi</ta>
            <ta e="T22" id="Seg_1471" s="T21">bos-tə</ta>
            <ta e="T23" id="Seg_1472" s="T22">koʔbtə</ta>
            <ta e="T24" id="Seg_1473" s="T23">tʼepsi-ndə</ta>
            <ta e="T25" id="Seg_1474" s="T24">hem-bi</ta>
            <ta e="T26" id="Seg_1475" s="T25">dĭgəttə</ta>
            <ta e="T27" id="Seg_1476" s="T26">šo-bi</ta>
            <ta e="T28" id="Seg_1477" s="T27">di</ta>
            <ta e="T29" id="Seg_1478" s="T28">nüke</ta>
            <ta e="T30" id="Seg_1479" s="T29">pa-j-laʔ</ta>
            <ta e="T31" id="Seg_1480" s="T30">mĭm-bi</ta>
            <ta e="T32" id="Seg_1481" s="T31">šo-bi</ta>
            <ta e="T33" id="Seg_1482" s="T32">nʼi-t</ta>
            <ta e="T34" id="Seg_1483" s="T33">naga</ta>
            <ta e="T35" id="Seg_1484" s="T34">koʔbdo</ta>
            <ta e="T36" id="Seg_1485" s="T35">tʼepsi-ndə</ta>
            <ta e="T37" id="Seg_1486" s="T36">iʔbə</ta>
            <ta e="T38" id="Seg_1487" s="T37">di</ta>
            <ta e="T39" id="Seg_1488" s="T38">nüke</ta>
            <ta e="T40" id="Seg_1489" s="T39">koro-laʔ</ta>
            <ta e="T41" id="Seg_1490" s="T40">saʔmə-bi</ta>
            <ta e="T42" id="Seg_1491" s="T41">ešši-bə</ta>
            <ta e="T43" id="Seg_1492" s="T42">i-bi</ta>
            <ta e="T44" id="Seg_1493" s="T43">koro-bi</ta>
            <ta e="T45" id="Seg_1494" s="T44">baška</ta>
            <ta e="T46" id="Seg_1495" s="T45">măja-nə</ta>
            <ta e="T47" id="Seg_1496" s="T46">kal-laʔ</ta>
            <ta e="T48" id="Seg_1497" s="T47">tʼür-bi</ta>
            <ta e="T49" id="Seg_1498" s="T48">tamnug-ən</ta>
            <ta e="T50" id="Seg_1499" s="T49">nʼe</ta>
            <ta e="T51" id="Seg_1500" s="T50">di</ta>
            <ta e="T52" id="Seg_1501" s="T51">nüke</ta>
            <ta e="T53" id="Seg_1502" s="T52">tamnuʔ-gən</ta>
            <ta e="T54" id="Seg_1503" s="T53">di</ta>
            <ta e="T55" id="Seg_1504" s="T54">nʼekə</ta>
            <ta e="T56" id="Seg_1505" s="T55">iʔbə-leʔ</ta>
            <ta e="T57" id="Seg_1506" s="T56">kojo-bi</ta>
            <ta e="T58" id="Seg_1507" s="T57">nʼi-t</ta>
            <ta e="T59" id="Seg_1508" s="T58">saməj-laʔ</ta>
            <ta e="T60" id="Seg_1509" s="T59">tüšə-lei-bi</ta>
            <ta e="T61" id="Seg_1510" s="T60">poʔto</ta>
            <ta e="T62" id="Seg_1511" s="T61">tʼit-lei-bi</ta>
            <ta e="T63" id="Seg_1512" s="T62">saməj-laʔ</ta>
            <ta e="T64" id="Seg_1513" s="T63">mĭl-lei-bi</ta>
            <ta e="T65" id="Seg_1514" s="T64">poʔto</ta>
            <ta e="T66" id="Seg_1515" s="T65">tʼit-lei-bi</ta>
            <ta e="T67" id="Seg_1516" s="T66">ti</ta>
            <ta e="T68" id="Seg_1517" s="T67">nüke</ta>
            <ta e="T69" id="Seg_1518" s="T68">uja</ta>
            <ta e="T70" id="Seg_1519" s="T69">sil</ta>
            <ta e="T71" id="Seg_1520" s="T70">am-nei-bi</ta>
            <ta e="T72" id="Seg_1521" s="T71">di</ta>
            <ta e="T73" id="Seg_1522" s="T72">nʼi</ta>
            <ta e="T74" id="Seg_1523" s="T73">tamnug-ən</ta>
            <ta e="T75" id="Seg_1524" s="T74">nʼi</ta>
            <ta e="T76" id="Seg_1525" s="T75">ija-bə</ta>
            <ta e="T77" id="Seg_1526" s="T76">sorar-bi</ta>
            <ta e="T78" id="Seg_1527" s="T77">tăn</ta>
            <ta e="T79" id="Seg_1528" s="T78">moʔ</ta>
            <ta e="T80" id="Seg_1529" s="T79">müjə-l</ta>
            <ta e="T81" id="Seg_1530" s="T80">nagur</ta>
            <ta e="T82" id="Seg_1531" s="T81">mă-lie</ta>
            <ta e="T83" id="Seg_1532" s="T82">tăn</ta>
            <ta e="T84" id="Seg_1533" s="T83">ej</ta>
            <ta e="T85" id="Seg_1534" s="T84">măn</ta>
            <ta e="T86" id="Seg_1535" s="T85">nʼi-m</ta>
            <ta e="T87" id="Seg_1536" s="T86">i-ge-l</ta>
            <ta e="T88" id="Seg_1537" s="T87">baška</ta>
            <ta e="T89" id="Seg_1538" s="T88">nüke-n</ta>
            <ta e="T90" id="Seg_1539" s="T89">nʼi-t</ta>
            <ta e="T91" id="Seg_1540" s="T90">i-bie-l</ta>
            <ta e="T92" id="Seg_1541" s="T91">măn</ta>
            <ta e="T93" id="Seg_1542" s="T92">koʔbdo-m</ta>
            <ta e="T94" id="Seg_1543" s="T93">i-bi</ta>
            <ta e="T95" id="Seg_1544" s="T94">măn</ta>
            <ta e="T96" id="Seg_1545" s="T95">tojar-bio-m</ta>
            <ta e="T97" id="Seg_1546" s="T96">tʼepsi-ndə</ta>
            <ta e="T98" id="Seg_1547" s="T97">em-biö-m</ta>
            <ta e="T99" id="Seg_1548" s="T98">tĭgəttə</ta>
            <ta e="T100" id="Seg_1549" s="T99">koro-laʔ</ta>
            <ta e="T101" id="Seg_1550" s="T100">kal-laʔ</ta>
            <ta e="T102" id="Seg_1551" s="T101">tʼür-bi</ta>
            <ta e="T103" id="Seg_1552" s="T102">baška-nə</ta>
            <ta e="T104" id="Seg_1553" s="T103">măja-nə</ta>
            <ta e="T105" id="Seg_1554" s="T104">šü</ta>
            <ta e="T106" id="Seg_1555" s="T105">sagər</ta>
            <ta e="T107" id="Seg_1556" s="T106">măja-gən</ta>
            <ta e="T108" id="Seg_1557" s="T107">dĭgən</ta>
            <ta e="T109" id="Seg_1558" s="T108">amna</ta>
            <ta e="T110" id="Seg_1559" s="T109">teinen</ta>
            <ta e="T111" id="Seg_1560" s="T110">pa</ta>
            <ta e="T112" id="Seg_1561" s="T111">i-ʔ</ta>
            <ta e="T113" id="Seg_1562" s="T112">pa-j-ə-ʔ</ta>
            <ta e="T114" id="Seg_1563" s="T113">teinen</ta>
            <ta e="T115" id="Seg_1564" s="T114">kaza</ta>
            <ta e="T116" id="Seg_1565" s="T115">pa-n</ta>
            <ta e="T117" id="Seg_1566" s="T116">šomi-n</ta>
            <ta e="T118" id="Seg_1567" s="T117">kaza-bə</ta>
            <ta e="T119" id="Seg_1568" s="T118">oʔbdʼə-t</ta>
            <ta e="T120" id="Seg_1569" s="T119">šaː-lə-l</ta>
            <ta e="T121" id="Seg_1570" s="T120">kaza</ta>
            <ta e="T122" id="Seg_1571" s="T121">oʔbdə-bi</ta>
            <ta e="T123" id="Seg_1572" s="T122">šü</ta>
            <ta e="T124" id="Seg_1573" s="T123">hem-bi</ta>
            <ta e="T125" id="Seg_1574" s="T124">šü-t</ta>
            <ta e="T126" id="Seg_1575" s="T125">kuʔbd-o-laːm-bi</ta>
            <ta e="T127" id="Seg_1576" s="T126">uja</ta>
            <ta e="T128" id="Seg_1577" s="T127">am-naʔ</ta>
            <ta e="T129" id="Seg_1578" s="T128">kaza-t-sʼəʔ</ta>
            <ta e="T130" id="Seg_1579" s="T129">hen-də</ta>
            <ta e="T131" id="Seg_1580" s="T130">šü</ta>
            <ta e="T132" id="Seg_1581" s="T131">kaza-t-sʼəʔ</ta>
            <ta e="T133" id="Seg_1582" s="T132">hem-bi</ta>
            <ta e="T134" id="Seg_1583" s="T133">dĭgəttə</ta>
            <ta e="T135" id="Seg_1584" s="T134">šü-t</ta>
            <ta e="T136" id="Seg_1585" s="T135">kuʔbd-o-laga</ta>
            <ta e="T137" id="Seg_1586" s="T136">kam-bi</ta>
            <ta e="T138" id="Seg_1587" s="T137">šü-bə</ta>
            <ta e="T139" id="Seg_1588" s="T138">püʔ-leʔ-bi</ta>
            <ta e="T140" id="Seg_1589" s="T139">dĭgəttə</ta>
            <ta e="T141" id="Seg_1590" s="T140">sima-t</ta>
            <ta e="T142" id="Seg_1591" s="T141">păktə-laʔ</ta>
            <ta e="T143" id="Seg_1592" s="T142">saʔmə-bi</ta>
            <ta e="T144" id="Seg_1593" s="T143">dĭgəttə</ta>
            <ta e="T145" id="Seg_1594" s="T144">püʔ-leʔ-bi</ta>
            <ta e="T146" id="Seg_1595" s="T145">ami</ta>
            <ta e="T147" id="Seg_1596" s="T146">sima-t</ta>
            <ta e="T148" id="Seg_1597" s="T147">păktə-laʔ</ta>
            <ta e="T149" id="Seg_1598" s="T148">saʔmə-bi</ta>
            <ta e="T150" id="Seg_1599" s="T149">ami</ta>
            <ta e="T151" id="Seg_1600" s="T150">sima-t</ta>
            <ta e="T152" id="Seg_1601" s="T151">păktə-laʔ</ta>
            <ta e="T153" id="Seg_1602" s="T152">saʔmə-bi</ta>
            <ta e="T154" id="Seg_1603" s="T153">kü-laːm-bi</ta>
            <ta e="T155" id="Seg_1604" s="T154">dĭ</ta>
            <ta e="T156" id="Seg_1605" s="T155">nüke</ta>
            <ta e="T157" id="Seg_1606" s="T156">nʼi</ta>
            <ta e="T158" id="Seg_1607" s="T157">uja</ta>
            <ta e="T159" id="Seg_1608" s="T158">sil</ta>
            <ta e="T160" id="Seg_1609" s="T159">i-bi</ta>
            <ta e="T161" id="Seg_1610" s="T160">dĭgəttə</ta>
            <ta e="T162" id="Seg_1611" s="T161">kal-laʔ</ta>
            <ta e="T163" id="Seg_1612" s="T162">tʼür-bi</ta>
            <ta e="T164" id="Seg_1613" s="T163">ija-bə</ta>
            <ta e="T165" id="Seg_1614" s="T164">pʼe-leʔ</ta>
            <ta e="T166" id="Seg_1615" s="T165">kam-bi</ta>
            <ta e="T167" id="Seg_1616" s="T166">urgo</ta>
            <ta e="T168" id="Seg_1617" s="T167">măja-nə</ta>
            <ta e="T169" id="Seg_1618" s="T168">kam-bi</ta>
            <ta e="T170" id="Seg_1619" s="T169">urgo</ta>
            <ta e="T171" id="Seg_1620" s="T170">sagər</ta>
            <ta e="T172" id="Seg_1621" s="T171">măja-gən</ta>
            <ta e="T173" id="Seg_1622" s="T172">ku-bi</ta>
            <ta e="T174" id="Seg_1623" s="T173">maʔ</ta>
            <ta e="T175" id="Seg_1624" s="T174">nu-ga</ta>
            <ta e="T176" id="Seg_1625" s="T175">dĭ</ta>
            <ta e="T177" id="Seg_1626" s="T176">maʔ-gən</ta>
            <ta e="T178" id="Seg_1627" s="T177">šü</ta>
            <ta e="T179" id="Seg_1628" s="T178">nei-leʔbə</ta>
            <ta e="T180" id="Seg_1629" s="T179">bor</ta>
            <ta e="T181" id="Seg_1630" s="T180">mazəro-gəʔ</ta>
            <ta e="T182" id="Seg_1631" s="T181">uʔ-laʔbə</ta>
            <ta e="T183" id="Seg_1632" s="T182">dĭgəttə</ta>
            <ta e="T184" id="Seg_1633" s="T183">ma-an</ta>
            <ta e="T185" id="Seg_1634" s="T184">nĭ-gəndə</ta>
            <ta e="T186" id="Seg_1635" s="T185">sʼa-laːm-bi</ta>
            <ta e="T187" id="Seg_1636" s="T186">dĭ-gəʔ</ta>
            <ta e="T188" id="Seg_1637" s="T187">mazəro-gəʔ</ta>
            <ta e="T189" id="Seg_1638" s="T188">tʼüdə</ta>
            <ta e="T190" id="Seg_1639" s="T189">măndə-bi</ta>
            <ta e="T191" id="Seg_1640" s="T190">ija-t</ta>
            <ta e="T192" id="Seg_1641" s="T191">amna</ta>
            <ta e="T193" id="Seg_1642" s="T192">tamnug-ən</ta>
            <ta e="T194" id="Seg_1643" s="T193">nʼe</ta>
            <ta e="T195" id="Seg_1644" s="T194">koʔbdo</ta>
            <ta e="T196" id="Seg_1645" s="T195">amna</ta>
            <ta e="T197" id="Seg_1646" s="T196">dĭ</ta>
            <ta e="T198" id="Seg_1647" s="T197">mazəro-gəʔ</ta>
            <ta e="T199" id="Seg_1648" s="T198">aspaʔ-də</ta>
            <ta e="T200" id="Seg_1649" s="T199">sil</ta>
            <ta e="T201" id="Seg_1650" s="T200">püdəldə-bi</ta>
            <ta e="T202" id="Seg_1651" s="T201">koʔbdo</ta>
            <ta e="T203" id="Seg_1652" s="T202">mă-lia</ta>
            <ta e="T204" id="Seg_1653" s="T203">aspaʔ-nʼibaʔ</ta>
            <ta e="T205" id="Seg_1654" s="T204">sil</ta>
            <ta e="T206" id="Seg_1655" s="T205">mĭl-leʔbə</ta>
            <ta e="T207" id="Seg_1656" s="T206">tʼaktə</ta>
            <ta e="T208" id="Seg_1657" s="T207">nüke</ta>
            <ta e="T209" id="Seg_1658" s="T208">pa</ta>
            <ta e="T210" id="Seg_1659" s="T209">kabar-bi</ta>
            <ta e="T211" id="Seg_1660" s="T210">koʔbdo-bə</ta>
            <ta e="T212" id="Seg_1661" s="T211">ulu-ttə</ta>
            <ta e="T213" id="Seg_1662" s="T212">toʔ-luʔ-bi</ta>
            <ta e="T214" id="Seg_1663" s="T213">i-ʔ</ta>
            <ta e="T215" id="Seg_1664" s="T214">šʼaːm-a-ʔ</ta>
            <ta e="T216" id="Seg_1665" s="T215">tăn</ta>
            <ta e="T217" id="Seg_1666" s="T216">ija-l</ta>
            <ta e="T218" id="Seg_1667" s="T217">sil</ta>
            <ta e="T219" id="Seg_1668" s="T218">uja</ta>
            <ta e="T220" id="Seg_1669" s="T219">am-naʔbə</ta>
            <ta e="T221" id="Seg_1670" s="T220">măn</ta>
            <ta e="T222" id="Seg_1671" s="T221">togul</ta>
            <ta e="T223" id="Seg_1672" s="T222">am-na-m</ta>
            <ta e="T224" id="Seg_1673" s="T223">tăn</ta>
            <ta e="T225" id="Seg_1674" s="T224">ersə</ta>
            <ta e="T226" id="Seg_1675" s="T225">ija-l-zʼəʔ</ta>
            <ta e="T227" id="Seg_1676" s="T226">kudʼər</ta>
            <ta e="T228" id="Seg_1677" s="T227">örer-leʔ</ta>
            <ta e="T229" id="Seg_1678" s="T228">i-bi</ta>
            <ta e="T230" id="Seg_1679" s="T229">dĭgəttə</ta>
            <ta e="T231" id="Seg_1680" s="T230">di</ta>
            <ta e="T232" id="Seg_1681" s="T231">nʼi</ta>
            <ta e="T233" id="Seg_1682" s="T232">mazəro-gəʔ</ta>
            <ta e="T234" id="Seg_1683" s="T233">üzə-bi</ta>
            <ta e="T235" id="Seg_1684" s="T234">maʔ-də</ta>
            <ta e="T236" id="Seg_1685" s="T235">šü-bi</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1686" s="T0">šide</ta>
            <ta e="T2" id="Seg_1687" s="T1">tʼaktə</ta>
            <ta e="T3" id="Seg_1688" s="T2">nüke-zAŋ</ta>
            <ta e="T4" id="Seg_1689" s="T3">amno-bi</ta>
            <ta e="T5" id="Seg_1690" s="T4">dʼije-Kən</ta>
            <ta e="T6" id="Seg_1691" s="T5">dĭ-zAŋ</ta>
            <ta e="T7" id="Seg_1692" s="T6">oʔb-lar</ta>
            <ta e="T8" id="Seg_1693" s="T7">ešši</ta>
            <ta e="T9" id="Seg_1694" s="T8">det-bi</ta>
            <ta e="T10" id="Seg_1695" s="T9">oʔb</ta>
            <ta e="T11" id="Seg_1696" s="T10">nüke-n</ta>
            <ta e="T12" id="Seg_1697" s="T11">koʔbdo</ta>
            <ta e="T13" id="Seg_1698" s="T12">i-bi</ta>
            <ta e="T14" id="Seg_1699" s="T13">oʔb</ta>
            <ta e="T15" id="Seg_1700" s="T14">nüke-n</ta>
            <ta e="T16" id="Seg_1701" s="T15">nʼi</ta>
            <ta e="T17" id="Seg_1702" s="T16">i-bi</ta>
            <ta e="T18" id="Seg_1703" s="T17">oʔb</ta>
            <ta e="T19" id="Seg_1704" s="T18">nüke</ta>
            <ta e="T20" id="Seg_1705" s="T19">nʼi-bə</ta>
            <ta e="T21" id="Seg_1706" s="T20">tojar-bi</ta>
            <ta e="T22" id="Seg_1707" s="T21">bos-tə</ta>
            <ta e="T23" id="Seg_1708" s="T22">koʔbdo</ta>
            <ta e="T24" id="Seg_1709" s="T23">tʼepsi-gəndə</ta>
            <ta e="T25" id="Seg_1710" s="T24">hen-bi</ta>
            <ta e="T26" id="Seg_1711" s="T25">dĭgəttə</ta>
            <ta e="T27" id="Seg_1712" s="T26">šo-bi</ta>
            <ta e="T28" id="Seg_1713" s="T27">dĭ</ta>
            <ta e="T29" id="Seg_1714" s="T28">nüke</ta>
            <ta e="T30" id="Seg_1715" s="T29">pa-j-lAʔ</ta>
            <ta e="T31" id="Seg_1716" s="T30">mĭn-bi</ta>
            <ta e="T32" id="Seg_1717" s="T31">šo-bi</ta>
            <ta e="T33" id="Seg_1718" s="T32">nʼi-t</ta>
            <ta e="T34" id="Seg_1719" s="T33">naga</ta>
            <ta e="T35" id="Seg_1720" s="T34">koʔbdo</ta>
            <ta e="T36" id="Seg_1721" s="T35">tʼepsi-gəndə</ta>
            <ta e="T37" id="Seg_1722" s="T36">iʔbə</ta>
            <ta e="T38" id="Seg_1723" s="T37">dĭ</ta>
            <ta e="T39" id="Seg_1724" s="T38">nüke</ta>
            <ta e="T40" id="Seg_1725" s="T39">koro-lAʔ</ta>
            <ta e="T41" id="Seg_1726" s="T40">saʔmə-bi</ta>
            <ta e="T42" id="Seg_1727" s="T41">ešši-bə</ta>
            <ta e="T43" id="Seg_1728" s="T42">i-bi</ta>
            <ta e="T44" id="Seg_1729" s="T43">koro-bi</ta>
            <ta e="T45" id="Seg_1730" s="T44">baška</ta>
            <ta e="T46" id="Seg_1731" s="T45">măja-Tə</ta>
            <ta e="T47" id="Seg_1732" s="T46">kan-lAʔ</ta>
            <ta e="T48" id="Seg_1733" s="T47">tʼür-bi</ta>
            <ta e="T49" id="Seg_1734" s="T48">tamnuʔ-n</ta>
            <ta e="T50" id="Seg_1735" s="T49">ne</ta>
            <ta e="T51" id="Seg_1736" s="T50">dĭ</ta>
            <ta e="T52" id="Seg_1737" s="T51">nüke</ta>
            <ta e="T53" id="Seg_1738" s="T52">tamnuʔ-Kən</ta>
            <ta e="T54" id="Seg_1739" s="T53">dĭ</ta>
            <ta e="T55" id="Seg_1740" s="T54">nʼekə</ta>
            <ta e="T56" id="Seg_1741" s="T55">iʔbə-lAʔ</ta>
            <ta e="T57" id="Seg_1742" s="T56">kojo-bi</ta>
            <ta e="T58" id="Seg_1743" s="T57">nʼi-t</ta>
            <ta e="T59" id="Seg_1744" s="T58">saməj-lAʔ</ta>
            <ta e="T60" id="Seg_1745" s="T59">tüšə-lei-bi</ta>
            <ta e="T61" id="Seg_1746" s="T60">poʔto</ta>
            <ta e="T62" id="Seg_1747" s="T61">tʼit-lei-bi</ta>
            <ta e="T63" id="Seg_1748" s="T62">saməj-lAʔ</ta>
            <ta e="T64" id="Seg_1749" s="T63">mĭn-lei-bi</ta>
            <ta e="T65" id="Seg_1750" s="T64">poʔto</ta>
            <ta e="T66" id="Seg_1751" s="T65">tʼit-lei-bi</ta>
            <ta e="T67" id="Seg_1752" s="T66">dĭ</ta>
            <ta e="T68" id="Seg_1753" s="T67">nüke</ta>
            <ta e="T69" id="Seg_1754" s="T68">uja</ta>
            <ta e="T70" id="Seg_1755" s="T69">sil</ta>
            <ta e="T71" id="Seg_1756" s="T70">am-lei-bi</ta>
            <ta e="T72" id="Seg_1757" s="T71">dĭ</ta>
            <ta e="T73" id="Seg_1758" s="T72">nʼi</ta>
            <ta e="T74" id="Seg_1759" s="T73">tamnuʔ-n</ta>
            <ta e="T75" id="Seg_1760" s="T74">nʼi</ta>
            <ta e="T76" id="Seg_1761" s="T75">ija-bə</ta>
            <ta e="T77" id="Seg_1762" s="T76">surar-bi</ta>
            <ta e="T78" id="Seg_1763" s="T77">tăn</ta>
            <ta e="T79" id="Seg_1764" s="T78">moʔ</ta>
            <ta e="T80" id="Seg_1765" s="T79">müjə-l</ta>
            <ta e="T81" id="Seg_1766" s="T80">nagur</ta>
            <ta e="T82" id="Seg_1767" s="T81">măn-liA</ta>
            <ta e="T83" id="Seg_1768" s="T82">tăn</ta>
            <ta e="T84" id="Seg_1769" s="T83">ej</ta>
            <ta e="T85" id="Seg_1770" s="T84">măn</ta>
            <ta e="T86" id="Seg_1771" s="T85">nʼi-m</ta>
            <ta e="T87" id="Seg_1772" s="T86">i-gA-l</ta>
            <ta e="T88" id="Seg_1773" s="T87">baška</ta>
            <ta e="T89" id="Seg_1774" s="T88">nüke-n</ta>
            <ta e="T90" id="Seg_1775" s="T89">nʼi-t</ta>
            <ta e="T91" id="Seg_1776" s="T90">i-bi-l</ta>
            <ta e="T92" id="Seg_1777" s="T91">măn</ta>
            <ta e="T93" id="Seg_1778" s="T92">koʔbdo-m</ta>
            <ta e="T94" id="Seg_1779" s="T93">i-bi</ta>
            <ta e="T95" id="Seg_1780" s="T94">măn</ta>
            <ta e="T96" id="Seg_1781" s="T95">tojar-bi-m</ta>
            <ta e="T97" id="Seg_1782" s="T96">tʼepsi-gəndə</ta>
            <ta e="T98" id="Seg_1783" s="T97">hen-bi-m</ta>
            <ta e="T99" id="Seg_1784" s="T98">dĭgəttə</ta>
            <ta e="T100" id="Seg_1785" s="T99">koro-lAʔ</ta>
            <ta e="T101" id="Seg_1786" s="T100">kan-lAʔ</ta>
            <ta e="T102" id="Seg_1787" s="T101">tʼür-bi</ta>
            <ta e="T103" id="Seg_1788" s="T102">baška-Tə</ta>
            <ta e="T104" id="Seg_1789" s="T103">măja-Tə</ta>
            <ta e="T105" id="Seg_1790" s="T104">šö</ta>
            <ta e="T106" id="Seg_1791" s="T105">sagər</ta>
            <ta e="T107" id="Seg_1792" s="T106">măja-Kən</ta>
            <ta e="T108" id="Seg_1793" s="T107">dĭgən</ta>
            <ta e="T109" id="Seg_1794" s="T108">amnə</ta>
            <ta e="T110" id="Seg_1795" s="T109">teinen</ta>
            <ta e="T111" id="Seg_1796" s="T110">pa</ta>
            <ta e="T112" id="Seg_1797" s="T111">e-ʔ</ta>
            <ta e="T113" id="Seg_1798" s="T112">pa-j-ə-ʔ</ta>
            <ta e="T114" id="Seg_1799" s="T113">teinen</ta>
            <ta e="T115" id="Seg_1800" s="T114">kaza</ta>
            <ta e="T116" id="Seg_1801" s="T115">pa-n</ta>
            <ta e="T117" id="Seg_1802" s="T116">šomi-n</ta>
            <ta e="T118" id="Seg_1803" s="T117">kaza-bə</ta>
            <ta e="T119" id="Seg_1804" s="T118">oʔbdə-t</ta>
            <ta e="T120" id="Seg_1805" s="T119">šaː-lV-l</ta>
            <ta e="T121" id="Seg_1806" s="T120">kaza</ta>
            <ta e="T122" id="Seg_1807" s="T121">oʔbdə-bi</ta>
            <ta e="T123" id="Seg_1808" s="T122">šü</ta>
            <ta e="T124" id="Seg_1809" s="T123">hen-bi</ta>
            <ta e="T125" id="Seg_1810" s="T124">šü-t</ta>
            <ta e="T126" id="Seg_1811" s="T125">küʔbdə-o-laːm-bi</ta>
            <ta e="T127" id="Seg_1812" s="T126">uja</ta>
            <ta e="T128" id="Seg_1813" s="T127">am-lAʔ</ta>
            <ta e="T129" id="Seg_1814" s="T128">kaza-t-ziʔ</ta>
            <ta e="T130" id="Seg_1815" s="T129">hen-t</ta>
            <ta e="T131" id="Seg_1816" s="T130">šü</ta>
            <ta e="T132" id="Seg_1817" s="T131">kaza-t-ziʔ</ta>
            <ta e="T133" id="Seg_1818" s="T132">hen-bi</ta>
            <ta e="T134" id="Seg_1819" s="T133">dĭgəttə</ta>
            <ta e="T135" id="Seg_1820" s="T134">šü-t</ta>
            <ta e="T136" id="Seg_1821" s="T135">küʔbdə-o-laga</ta>
            <ta e="T137" id="Seg_1822" s="T136">kan-bi</ta>
            <ta e="T138" id="Seg_1823" s="T137">šü-bə</ta>
            <ta e="T139" id="Seg_1824" s="T138">püʔbdə-laʔbə-bi</ta>
            <ta e="T140" id="Seg_1825" s="T139">dĭgəttə</ta>
            <ta e="T141" id="Seg_1826" s="T140">sima-t</ta>
            <ta e="T142" id="Seg_1827" s="T141">păktə-lAʔ</ta>
            <ta e="T143" id="Seg_1828" s="T142">saʔmə-bi</ta>
            <ta e="T144" id="Seg_1829" s="T143">dĭgəttə</ta>
            <ta e="T145" id="Seg_1830" s="T144">püʔbdə-laʔbə-bi</ta>
            <ta e="T146" id="Seg_1831" s="T145">ami</ta>
            <ta e="T147" id="Seg_1832" s="T146">sima-t</ta>
            <ta e="T148" id="Seg_1833" s="T147">păktə-lAʔ</ta>
            <ta e="T149" id="Seg_1834" s="T148">saʔmə-bi</ta>
            <ta e="T150" id="Seg_1835" s="T149">ami</ta>
            <ta e="T151" id="Seg_1836" s="T150">sima-t</ta>
            <ta e="T152" id="Seg_1837" s="T151">păktə-lAʔ</ta>
            <ta e="T153" id="Seg_1838" s="T152">saʔmə-bi</ta>
            <ta e="T154" id="Seg_1839" s="T153">kü-laːm-bi</ta>
            <ta e="T155" id="Seg_1840" s="T154">dĭ</ta>
            <ta e="T156" id="Seg_1841" s="T155">nüke</ta>
            <ta e="T157" id="Seg_1842" s="T156">nʼi</ta>
            <ta e="T158" id="Seg_1843" s="T157">uja</ta>
            <ta e="T159" id="Seg_1844" s="T158">sil</ta>
            <ta e="T160" id="Seg_1845" s="T159">i-bi</ta>
            <ta e="T161" id="Seg_1846" s="T160">dĭgəttə</ta>
            <ta e="T162" id="Seg_1847" s="T161">kan-lAʔ</ta>
            <ta e="T163" id="Seg_1848" s="T162">tʼür-bi</ta>
            <ta e="T164" id="Seg_1849" s="T163">ija-bə</ta>
            <ta e="T165" id="Seg_1850" s="T164">pi-lAʔ</ta>
            <ta e="T166" id="Seg_1851" s="T165">kan-bi</ta>
            <ta e="T167" id="Seg_1852" s="T166">urgo</ta>
            <ta e="T168" id="Seg_1853" s="T167">măja-Tə</ta>
            <ta e="T169" id="Seg_1854" s="T168">kan-bi</ta>
            <ta e="T170" id="Seg_1855" s="T169">urgo</ta>
            <ta e="T171" id="Seg_1856" s="T170">sagər</ta>
            <ta e="T172" id="Seg_1857" s="T171">măja-Kən</ta>
            <ta e="T173" id="Seg_1858" s="T172">ku-bi</ta>
            <ta e="T174" id="Seg_1859" s="T173">maʔ</ta>
            <ta e="T175" id="Seg_1860" s="T174">nu-gA</ta>
            <ta e="T176" id="Seg_1861" s="T175">dĭ</ta>
            <ta e="T177" id="Seg_1862" s="T176">maʔ-Kən</ta>
            <ta e="T178" id="Seg_1863" s="T177">šü</ta>
            <ta e="T179" id="Seg_1864" s="T178">nei-laʔbə</ta>
            <ta e="T180" id="Seg_1865" s="T179">bor</ta>
            <ta e="T181" id="Seg_1866" s="T180">mazəro-gəʔ</ta>
            <ta e="T182" id="Seg_1867" s="T181">uʔbdə-laʔbə</ta>
            <ta e="T183" id="Seg_1868" s="T182">dĭgəttə</ta>
            <ta e="T184" id="Seg_1869" s="T183">maʔ-n</ta>
            <ta e="T185" id="Seg_1870" s="T184">nĭ-gəndə</ta>
            <ta e="T186" id="Seg_1871" s="T185">sʼa-laːm-bi</ta>
            <ta e="T187" id="Seg_1872" s="T186">dĭ-gəʔ</ta>
            <ta e="T188" id="Seg_1873" s="T187">mazəro-gəʔ</ta>
            <ta e="T189" id="Seg_1874" s="T188">tʼüdə</ta>
            <ta e="T190" id="Seg_1875" s="T189">măndo-bi</ta>
            <ta e="T191" id="Seg_1876" s="T190">ija-t</ta>
            <ta e="T192" id="Seg_1877" s="T191">amnə</ta>
            <ta e="T193" id="Seg_1878" s="T192">tamnuʔ-n</ta>
            <ta e="T194" id="Seg_1879" s="T193">ne</ta>
            <ta e="T195" id="Seg_1880" s="T194">koʔbdo</ta>
            <ta e="T196" id="Seg_1881" s="T195">amnə</ta>
            <ta e="T197" id="Seg_1882" s="T196">dĭ</ta>
            <ta e="T198" id="Seg_1883" s="T197">mazəro-gəʔ</ta>
            <ta e="T199" id="Seg_1884" s="T198">aspaʔ-Tə</ta>
            <ta e="T200" id="Seg_1885" s="T199">sil</ta>
            <ta e="T201" id="Seg_1886" s="T200">püdəldə-bi</ta>
            <ta e="T202" id="Seg_1887" s="T201">koʔbdo</ta>
            <ta e="T203" id="Seg_1888" s="T202">măn-liA</ta>
            <ta e="T204" id="Seg_1889" s="T203">aspaʔ-gənʼibAʔ</ta>
            <ta e="T205" id="Seg_1890" s="T204">sil</ta>
            <ta e="T206" id="Seg_1891" s="T205">mĭn-laʔbə</ta>
            <ta e="T207" id="Seg_1892" s="T206">tʼaktə</ta>
            <ta e="T208" id="Seg_1893" s="T207">nüke</ta>
            <ta e="T209" id="Seg_1894" s="T208">pa</ta>
            <ta e="T210" id="Seg_1895" s="T209">kabar-bi</ta>
            <ta e="T211" id="Seg_1896" s="T210">koʔbdo-bə</ta>
            <ta e="T212" id="Seg_1897" s="T211">ulu-ttə</ta>
            <ta e="T213" id="Seg_1898" s="T212">toʔbdə-luʔbdə-bi</ta>
            <ta e="T214" id="Seg_1899" s="T213">e-ʔ</ta>
            <ta e="T215" id="Seg_1900" s="T214">šʼaːm-ə-ʔ</ta>
            <ta e="T216" id="Seg_1901" s="T215">tăn</ta>
            <ta e="T217" id="Seg_1902" s="T216">ija-l</ta>
            <ta e="T218" id="Seg_1903" s="T217">sil</ta>
            <ta e="T219" id="Seg_1904" s="T218">uja</ta>
            <ta e="T220" id="Seg_1905" s="T219">am-laʔbə</ta>
            <ta e="T221" id="Seg_1906" s="T220">măn</ta>
            <ta e="T222" id="Seg_1907" s="T221">tugul</ta>
            <ta e="T223" id="Seg_1908" s="T222">am-lV-m</ta>
            <ta e="T224" id="Seg_1909" s="T223">tăn</ta>
            <ta e="T225" id="Seg_1910" s="T224">ersə</ta>
            <ta e="T226" id="Seg_1911" s="T225">ija-l-ziʔ</ta>
            <ta e="T227" id="Seg_1912" s="T226">kudʼər</ta>
            <ta e="T228" id="Seg_1913" s="T227">örer-lAʔ</ta>
            <ta e="T229" id="Seg_1914" s="T228">i-bi</ta>
            <ta e="T230" id="Seg_1915" s="T229">dĭgəttə</ta>
            <ta e="T231" id="Seg_1916" s="T230">dĭ</ta>
            <ta e="T232" id="Seg_1917" s="T231">nʼi</ta>
            <ta e="T233" id="Seg_1918" s="T232">mazəro-gəʔ</ta>
            <ta e="T234" id="Seg_1919" s="T233">üzə-bi</ta>
            <ta e="T235" id="Seg_1920" s="T234">maʔ-Tə</ta>
            <ta e="T236" id="Seg_1921" s="T235">šü-bi</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1922" s="T0">two.[NOM.SG]</ta>
            <ta e="T2" id="Seg_1923" s="T1">old.[NOM.SG]</ta>
            <ta e="T3" id="Seg_1924" s="T2">woman-PL</ta>
            <ta e="T4" id="Seg_1925" s="T3">live-PST.[3SG]</ta>
            <ta e="T5" id="Seg_1926" s="T4">forest-LOC</ta>
            <ta e="T6" id="Seg_1927" s="T5">this-PL</ta>
            <ta e="T7" id="Seg_1928" s="T6">one-DISTR</ta>
            <ta e="T8" id="Seg_1929" s="T7">child.[NOM.SG]</ta>
            <ta e="T9" id="Seg_1930" s="T8">bring-PST.[3SG]</ta>
            <ta e="T10" id="Seg_1931" s="T9">one.[NOM.SG]</ta>
            <ta e="T11" id="Seg_1932" s="T10">woman-GEN</ta>
            <ta e="T12" id="Seg_1933" s="T11">daughter.[NOM.SG]</ta>
            <ta e="T13" id="Seg_1934" s="T12">be-PST.[3SG]</ta>
            <ta e="T14" id="Seg_1935" s="T13">one.[NOM.SG]</ta>
            <ta e="T15" id="Seg_1936" s="T14">woman-GEN</ta>
            <ta e="T16" id="Seg_1937" s="T15">boy.[NOM.SG]</ta>
            <ta e="T17" id="Seg_1938" s="T16">be-PST.[3SG]</ta>
            <ta e="T18" id="Seg_1939" s="T17">one.[NOM.SG]</ta>
            <ta e="T19" id="Seg_1940" s="T18">woman.[NOM.SG]</ta>
            <ta e="T20" id="Seg_1941" s="T19">son-ACC.3SG</ta>
            <ta e="T21" id="Seg_1942" s="T20">steal-PST.[3SG]</ta>
            <ta e="T22" id="Seg_1943" s="T21">self-NOM/GEN/ACC.3PL</ta>
            <ta e="T23" id="Seg_1944" s="T22">daughter.[NOM.SG]</ta>
            <ta e="T24" id="Seg_1945" s="T23">cradle-LAT/LOC.3SG</ta>
            <ta e="T25" id="Seg_1946" s="T24">put-PST.[3SG]</ta>
            <ta e="T26" id="Seg_1947" s="T25">then</ta>
            <ta e="T27" id="Seg_1948" s="T26">come-PST.[3SG]</ta>
            <ta e="T28" id="Seg_1949" s="T27">this.[NOM.SG]</ta>
            <ta e="T29" id="Seg_1950" s="T28">woman.[NOM.SG]</ta>
            <ta e="T30" id="Seg_1951" s="T29">tree-VBLZ-CVB</ta>
            <ta e="T31" id="Seg_1952" s="T30">go-PST.[3SG]</ta>
            <ta e="T32" id="Seg_1953" s="T31">come-PST.[3SG]</ta>
            <ta e="T33" id="Seg_1954" s="T32">son-NOM/GEN.3SG</ta>
            <ta e="T34" id="Seg_1955" s="T33">NEG.EX.[3SG]</ta>
            <ta e="T35" id="Seg_1956" s="T34">girl.[NOM.SG]</ta>
            <ta e="T36" id="Seg_1957" s="T35">cradle-LAT/LOC.3SG</ta>
            <ta e="T37" id="Seg_1958" s="T36">lie.down.[3SG]</ta>
            <ta e="T38" id="Seg_1959" s="T37">this.[NOM.SG]</ta>
            <ta e="T39" id="Seg_1960" s="T38">woman.[NOM.SG]</ta>
            <ta e="T40" id="Seg_1961" s="T39">be.angry-CVB</ta>
            <ta e="T41" id="Seg_1962" s="T40">collapse-PST.[3SG]</ta>
            <ta e="T42" id="Seg_1963" s="T41">child-ACC.3SG</ta>
            <ta e="T43" id="Seg_1964" s="T42">take-PST.[3SG]</ta>
            <ta e="T44" id="Seg_1965" s="T43">be.angry-PST.[3SG]</ta>
            <ta e="T45" id="Seg_1966" s="T44">another.[NOM.SG]</ta>
            <ta e="T46" id="Seg_1967" s="T45">mountain-LAT</ta>
            <ta e="T47" id="Seg_1968" s="T46">go-CVB</ta>
            <ta e="T48" id="Seg_1969" s="T47">disappear-PST.[3SG]</ta>
            <ta e="T49" id="Seg_1970" s="T48">frog-GEN</ta>
            <ta e="T50" id="Seg_1971" s="T49">woman.[NOM.SG]</ta>
            <ta e="T51" id="Seg_1972" s="T50">this.[NOM.SG]</ta>
            <ta e="T52" id="Seg_1973" s="T51">woman.[NOM.SG]</ta>
            <ta e="T53" id="Seg_1974" s="T52">frog-LOC</ta>
            <ta e="T54" id="Seg_1975" s="T53">this.[NOM.SG]</ta>
            <ta e="T55" id="Seg_1976" s="T54">boy</ta>
            <ta e="T56" id="Seg_1977" s="T55">lie.down-CVB</ta>
            <ta e="T57" id="Seg_1978" s="T56">stay-PST.[3SG]</ta>
            <ta e="T58" id="Seg_1979" s="T57">son-NOM/GEN.3SG</ta>
            <ta e="T59" id="Seg_1980" s="T58">hunt-CVB</ta>
            <ta e="T60" id="Seg_1981" s="T59">learn-HAB-PST.[3SG]</ta>
            <ta e="T61" id="Seg_1982" s="T60">goat.[NOM.SG]</ta>
            <ta e="T62" id="Seg_1983" s="T61">shoot-HAB-PST.[3SG]</ta>
            <ta e="T63" id="Seg_1984" s="T62">hunt-CVB</ta>
            <ta e="T64" id="Seg_1985" s="T63">go-HAB-PST.[3SG]</ta>
            <ta e="T65" id="Seg_1986" s="T64">goat.[NOM.SG]</ta>
            <ta e="T66" id="Seg_1987" s="T65">shoot-HAB-PST.[3SG]</ta>
            <ta e="T67" id="Seg_1988" s="T66">this.[NOM.SG]</ta>
            <ta e="T68" id="Seg_1989" s="T67">woman.[NOM.SG]</ta>
            <ta e="T69" id="Seg_1990" s="T68">meat.[NOM.SG]</ta>
            <ta e="T70" id="Seg_1991" s="T69">fat.[NOM.SG]</ta>
            <ta e="T71" id="Seg_1992" s="T70">eat-HAB-PST.[3SG]</ta>
            <ta e="T72" id="Seg_1993" s="T71">this.[NOM.SG]</ta>
            <ta e="T73" id="Seg_1994" s="T72">boy.[NOM.SG]</ta>
            <ta e="T74" id="Seg_1995" s="T73">frog-GEN</ta>
            <ta e="T75" id="Seg_1996" s="T74">boy.[NOM.SG]</ta>
            <ta e="T76" id="Seg_1997" s="T75">mother-ACC.3SG</ta>
            <ta e="T77" id="Seg_1998" s="T76">ask-PST.[3SG]</ta>
            <ta e="T78" id="Seg_1999" s="T77">you.GEN</ta>
            <ta e="T79" id="Seg_2000" s="T78">why</ta>
            <ta e="T80" id="Seg_2001" s="T79">finger-NOM/GEN/ACC.2SG</ta>
            <ta e="T81" id="Seg_2002" s="T80">three.[NOM.SG]</ta>
            <ta e="T82" id="Seg_2003" s="T81">say-PRS.[3SG]</ta>
            <ta e="T83" id="Seg_2004" s="T82">you.NOM</ta>
            <ta e="T84" id="Seg_2005" s="T83">NEG</ta>
            <ta e="T85" id="Seg_2006" s="T84">I.GEN</ta>
            <ta e="T86" id="Seg_2007" s="T85">boy-NOM/GEN/ACC.1SG</ta>
            <ta e="T87" id="Seg_2008" s="T86">be-PRS-2SG</ta>
            <ta e="T88" id="Seg_2009" s="T87">another</ta>
            <ta e="T89" id="Seg_2010" s="T88">woman-GEN</ta>
            <ta e="T90" id="Seg_2011" s="T89">son-NOM/GEN.3SG</ta>
            <ta e="T91" id="Seg_2012" s="T90">be-PST-2SG</ta>
            <ta e="T92" id="Seg_2013" s="T91">I.GEN</ta>
            <ta e="T93" id="Seg_2014" s="T92">daughter-NOM/GEN/ACC.1SG</ta>
            <ta e="T94" id="Seg_2015" s="T93">be-PST.[3SG]</ta>
            <ta e="T95" id="Seg_2016" s="T94">I.NOM</ta>
            <ta e="T96" id="Seg_2017" s="T95">steal-PST-1SG</ta>
            <ta e="T97" id="Seg_2018" s="T96">cradle-LAT/LOC.3SG</ta>
            <ta e="T98" id="Seg_2019" s="T97">put-PST-1SG</ta>
            <ta e="T99" id="Seg_2020" s="T98">then</ta>
            <ta e="T100" id="Seg_2021" s="T99">be.angry-CVB</ta>
            <ta e="T101" id="Seg_2022" s="T100">go-CVB</ta>
            <ta e="T102" id="Seg_2023" s="T101">disappear-PST.[3SG]</ta>
            <ta e="T103" id="Seg_2024" s="T102">another-LAT</ta>
            <ta e="T104" id="Seg_2025" s="T103">mountain-LAT</ta>
            <ta e="T105" id="Seg_2026" s="T104">that.[NOM.SG]</ta>
            <ta e="T106" id="Seg_2027" s="T105">black.[NOM.SG]</ta>
            <ta e="T107" id="Seg_2028" s="T106">mountain-LOC</ta>
            <ta e="T108" id="Seg_2029" s="T107">there</ta>
            <ta e="T109" id="Seg_2030" s="T108">live.[3SG]</ta>
            <ta e="T110" id="Seg_2031" s="T109">today</ta>
            <ta e="T111" id="Seg_2032" s="T110">tree.[NOM.SG]</ta>
            <ta e="T112" id="Seg_2033" s="T111">NEG.AUX-IMP.2SG</ta>
            <ta e="T113" id="Seg_2034" s="T112">tree-VBLZ-EP-CNG</ta>
            <ta e="T114" id="Seg_2035" s="T113">today</ta>
            <ta e="T115" id="Seg_2036" s="T114">bark.[NOM.SG]</ta>
            <ta e="T116" id="Seg_2037" s="T115">tree-GEN</ta>
            <ta e="T117" id="Seg_2038" s="T116">larch-GEN</ta>
            <ta e="T118" id="Seg_2039" s="T117">bark-ACC.3SG</ta>
            <ta e="T119" id="Seg_2040" s="T118">collect-IMP.2SG.O</ta>
            <ta e="T120" id="Seg_2041" s="T119">spend.night-FUT-2SG</ta>
            <ta e="T121" id="Seg_2042" s="T120">bark.[NOM.SG]</ta>
            <ta e="T122" id="Seg_2043" s="T121">collect-PST.[3SG]</ta>
            <ta e="T123" id="Seg_2044" s="T122">fire.[NOM.SG]</ta>
            <ta e="T124" id="Seg_2045" s="T123">put-PST.[3SG]</ta>
            <ta e="T125" id="Seg_2046" s="T124">fire-NOM/GEN.3SG</ta>
            <ta e="T126" id="Seg_2047" s="T125">stop.burning-DETR-RES-PST.[3SG]</ta>
            <ta e="T127" id="Seg_2048" s="T126">meat.[NOM.SG]</ta>
            <ta e="T128" id="Seg_2049" s="T127">eat-CVB</ta>
            <ta e="T129" id="Seg_2050" s="T128">bark-3SG-INS</ta>
            <ta e="T130" id="Seg_2051" s="T129">put-IMP.2SG.O</ta>
            <ta e="T131" id="Seg_2052" s="T130">fire.[NOM.SG]</ta>
            <ta e="T132" id="Seg_2053" s="T131">bark-3SG-INS</ta>
            <ta e="T133" id="Seg_2054" s="T132">put-PST.[3SG]</ta>
            <ta e="T134" id="Seg_2055" s="T133">then</ta>
            <ta e="T135" id="Seg_2056" s="T134">fire-NOM/GEN.3SG</ta>
            <ta e="T136" id="Seg_2057" s="T135">stop.burning-DETR-DUR.PRS.[3SG]</ta>
            <ta e="T137" id="Seg_2058" s="T136">go-PST.[3SG]</ta>
            <ta e="T138" id="Seg_2059" s="T137">fire-ACC.3SG</ta>
            <ta e="T139" id="Seg_2060" s="T138">blow-DUR-PST.[3SG]</ta>
            <ta e="T140" id="Seg_2061" s="T139">then</ta>
            <ta e="T141" id="Seg_2062" s="T140">eye-NOM/GEN.3SG</ta>
            <ta e="T142" id="Seg_2063" s="T141">burst-CVB</ta>
            <ta e="T143" id="Seg_2064" s="T142">collapse-PST.[3SG]</ta>
            <ta e="T144" id="Seg_2065" s="T143">then</ta>
            <ta e="T145" id="Seg_2066" s="T144">blow-DUR-PST.[3SG]</ta>
            <ta e="T146" id="Seg_2067" s="T145">other.[NOM.SG]</ta>
            <ta e="T147" id="Seg_2068" s="T146">eye-NOM/GEN.3SG</ta>
            <ta e="T148" id="Seg_2069" s="T147">burst-CVB</ta>
            <ta e="T149" id="Seg_2070" s="T148">collapse-PST.[3SG]</ta>
            <ta e="T150" id="Seg_2071" s="T149">other.[NOM.SG]</ta>
            <ta e="T151" id="Seg_2072" s="T150">eye-NOM/GEN.3SG</ta>
            <ta e="T152" id="Seg_2073" s="T151">burst-CVB</ta>
            <ta e="T153" id="Seg_2074" s="T152">collapse-PST.[3SG]</ta>
            <ta e="T154" id="Seg_2075" s="T153">die-RES-PST.[3SG]</ta>
            <ta e="T155" id="Seg_2076" s="T154">this.[NOM.SG]</ta>
            <ta e="T156" id="Seg_2077" s="T155">woman.[NOM.SG]</ta>
            <ta e="T157" id="Seg_2078" s="T156">boy.[NOM.SG]</ta>
            <ta e="T158" id="Seg_2079" s="T157">meat.[NOM.SG]</ta>
            <ta e="T159" id="Seg_2080" s="T158">fat.[NOM.SG]</ta>
            <ta e="T160" id="Seg_2081" s="T159">take-PST.[3SG]</ta>
            <ta e="T161" id="Seg_2082" s="T160">then</ta>
            <ta e="T162" id="Seg_2083" s="T161">go-CVB</ta>
            <ta e="T163" id="Seg_2084" s="T162">disappear-PST.[3SG]</ta>
            <ta e="T164" id="Seg_2085" s="T163">mother-ACC.3SG</ta>
            <ta e="T165" id="Seg_2086" s="T164">look.for-CVB</ta>
            <ta e="T166" id="Seg_2087" s="T165">go-PST.[3SG]</ta>
            <ta e="T167" id="Seg_2088" s="T166">big.[NOM.SG]</ta>
            <ta e="T168" id="Seg_2089" s="T167">mountain-LAT</ta>
            <ta e="T169" id="Seg_2090" s="T168">go-PST.[3SG]</ta>
            <ta e="T170" id="Seg_2091" s="T169">big.[NOM.SG]</ta>
            <ta e="T171" id="Seg_2092" s="T170">black.[NOM.SG]</ta>
            <ta e="T172" id="Seg_2093" s="T171">mountain-LOC</ta>
            <ta e="T173" id="Seg_2094" s="T172">see-PST.[3SG]</ta>
            <ta e="T174" id="Seg_2095" s="T173">tent.[NOM.SG]</ta>
            <ta e="T175" id="Seg_2096" s="T174">stand-PRS.[3SG]</ta>
            <ta e="T176" id="Seg_2097" s="T175">this.[NOM.SG]</ta>
            <ta e="T177" id="Seg_2098" s="T176">tent-LOC</ta>
            <ta e="T178" id="Seg_2099" s="T177">fire.[NOM.SG]</ta>
            <ta e="T179" id="Seg_2100" s="T178">burn-DUR.[3SG]</ta>
            <ta e="T180" id="Seg_2101" s="T179">smoke.[NOM.SG]</ta>
            <ta e="T181" id="Seg_2102" s="T180">smoke.hole-ABL</ta>
            <ta e="T182" id="Seg_2103" s="T181">get.up-DUR.[3SG]</ta>
            <ta e="T183" id="Seg_2104" s="T182">then</ta>
            <ta e="T184" id="Seg_2105" s="T183">tent-GEN</ta>
            <ta e="T185" id="Seg_2106" s="T184">top-LAT/LOC.3SG</ta>
            <ta e="T186" id="Seg_2107" s="T185">climb-RES-PST.[3SG]</ta>
            <ta e="T187" id="Seg_2108" s="T186">this-ABL</ta>
            <ta e="T188" id="Seg_2109" s="T187">smoke.hole-ABL</ta>
            <ta e="T189" id="Seg_2110" s="T188">down</ta>
            <ta e="T190" id="Seg_2111" s="T189">look-PST.[3SG]</ta>
            <ta e="T191" id="Seg_2112" s="T190">mother-NOM/GEN.3SG</ta>
            <ta e="T192" id="Seg_2113" s="T191">sit.[3SG]</ta>
            <ta e="T193" id="Seg_2114" s="T192">frog-GEN</ta>
            <ta e="T194" id="Seg_2115" s="T193">woman</ta>
            <ta e="T195" id="Seg_2116" s="T194">girl.[NOM.SG]</ta>
            <ta e="T196" id="Seg_2117" s="T195">sit.[3SG]</ta>
            <ta e="T197" id="Seg_2118" s="T196">this.[NOM.SG]</ta>
            <ta e="T198" id="Seg_2119" s="T197">smoke.hole-ABL</ta>
            <ta e="T199" id="Seg_2120" s="T198">cauldron-LAT</ta>
            <ta e="T200" id="Seg_2121" s="T199">fat.[NOM.SG]</ta>
            <ta e="T201" id="Seg_2122" s="T200">crumble-PST.[3SG]</ta>
            <ta e="T202" id="Seg_2123" s="T201">girl.[NOM.SG]</ta>
            <ta e="T203" id="Seg_2124" s="T202">say-PRS.[3SG]</ta>
            <ta e="T204" id="Seg_2125" s="T203">cauldron-LAT/LOC.1PL</ta>
            <ta e="T205" id="Seg_2126" s="T204">fat.[NOM.SG]</ta>
            <ta e="T206" id="Seg_2127" s="T205">go-DUR.[3SG]</ta>
            <ta e="T207" id="Seg_2128" s="T206">old.[NOM.SG]</ta>
            <ta e="T208" id="Seg_2129" s="T207">woman.[NOM.SG]</ta>
            <ta e="T209" id="Seg_2130" s="T208">tree.[NOM.SG]</ta>
            <ta e="T210" id="Seg_2131" s="T209">grab-PST.[3SG]</ta>
            <ta e="T211" id="Seg_2132" s="T210">girl-ACC.3SG</ta>
            <ta e="T212" id="Seg_2133" s="T211">head-ABL.3SG</ta>
            <ta e="T213" id="Seg_2134" s="T212">hit-MOM-PST.[3SG]</ta>
            <ta e="T214" id="Seg_2135" s="T213">NEG.AUX-IMP.2SG</ta>
            <ta e="T215" id="Seg_2136" s="T214">lie-EP-CNG</ta>
            <ta e="T216" id="Seg_2137" s="T215">you.GEN</ta>
            <ta e="T217" id="Seg_2138" s="T216">mother-NOM/GEN/ACC.2SG</ta>
            <ta e="T218" id="Seg_2139" s="T217">fat.[NOM.SG]</ta>
            <ta e="T219" id="Seg_2140" s="T218">meat.[NOM.SG]</ta>
            <ta e="T220" id="Seg_2141" s="T219">eat-DUR.[3SG]</ta>
            <ta e="T221" id="Seg_2142" s="T220">I.NOM</ta>
            <ta e="T222" id="Seg_2143" s="T221">martagon.lily</ta>
            <ta e="T223" id="Seg_2144" s="T222">eat-FUT-1SG</ta>
            <ta e="T224" id="Seg_2145" s="T223">you.NOM</ta>
            <ta e="T225" id="Seg_2146" s="T224">whore.[NOM.SG]</ta>
            <ta e="T226" id="Seg_2147" s="T225">mother-NOM/GEN/ACC.2SG-INS</ta>
            <ta e="T227" id="Seg_2148" s="T226">sneaky.[NOM.SG]</ta>
            <ta e="T228" id="Seg_2149" s="T227">die-CVB</ta>
            <ta e="T229" id="Seg_2150" s="T228">take-PST.[3SG]</ta>
            <ta e="T230" id="Seg_2151" s="T229">then</ta>
            <ta e="T231" id="Seg_2152" s="T230">this.[NOM.SG]</ta>
            <ta e="T232" id="Seg_2153" s="T231">boy.[NOM.SG]</ta>
            <ta e="T233" id="Seg_2154" s="T232">smoke.hole-ABL</ta>
            <ta e="T234" id="Seg_2155" s="T233">descend-PST.[3SG]</ta>
            <ta e="T235" id="Seg_2156" s="T234">tent-LAT</ta>
            <ta e="T236" id="Seg_2157" s="T235">enter-PST.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_2158" s="T0">два.[NOM.SG]</ta>
            <ta e="T2" id="Seg_2159" s="T1">старый.[NOM.SG]</ta>
            <ta e="T3" id="Seg_2160" s="T2">женщина-PL</ta>
            <ta e="T4" id="Seg_2161" s="T3">жить-PST.[3SG]</ta>
            <ta e="T5" id="Seg_2162" s="T4">лес-LOC</ta>
            <ta e="T6" id="Seg_2163" s="T5">этот-PL</ta>
            <ta e="T7" id="Seg_2164" s="T6">один-DISTR</ta>
            <ta e="T8" id="Seg_2165" s="T7">ребенок.[NOM.SG]</ta>
            <ta e="T9" id="Seg_2166" s="T8">принести-PST.[3SG]</ta>
            <ta e="T10" id="Seg_2167" s="T9">один.[NOM.SG]</ta>
            <ta e="T11" id="Seg_2168" s="T10">женщина-GEN</ta>
            <ta e="T12" id="Seg_2169" s="T11">дочь.[NOM.SG]</ta>
            <ta e="T13" id="Seg_2170" s="T12">быть-PST.[3SG]</ta>
            <ta e="T14" id="Seg_2171" s="T13">один.[NOM.SG]</ta>
            <ta e="T15" id="Seg_2172" s="T14">женщина-GEN</ta>
            <ta e="T16" id="Seg_2173" s="T15">мальчик.[NOM.SG]</ta>
            <ta e="T17" id="Seg_2174" s="T16">быть-PST.[3SG]</ta>
            <ta e="T18" id="Seg_2175" s="T17">один.[NOM.SG]</ta>
            <ta e="T19" id="Seg_2176" s="T18">женщина.[NOM.SG]</ta>
            <ta e="T20" id="Seg_2177" s="T19">сын-ACC.3SG</ta>
            <ta e="T21" id="Seg_2178" s="T20">украсть-PST.[3SG]</ta>
            <ta e="T22" id="Seg_2179" s="T21">сам-NOM/GEN/ACC.3PL</ta>
            <ta e="T23" id="Seg_2180" s="T22">дочь.[NOM.SG]</ta>
            <ta e="T24" id="Seg_2181" s="T23">люлька-LAT/LOC.3SG</ta>
            <ta e="T25" id="Seg_2182" s="T24">класть-PST.[3SG]</ta>
            <ta e="T26" id="Seg_2183" s="T25">тогда</ta>
            <ta e="T27" id="Seg_2184" s="T26">прийти-PST.[3SG]</ta>
            <ta e="T28" id="Seg_2185" s="T27">этот.[NOM.SG]</ta>
            <ta e="T29" id="Seg_2186" s="T28">женщина.[NOM.SG]</ta>
            <ta e="T30" id="Seg_2187" s="T29">дерево-VBLZ-CVB</ta>
            <ta e="T31" id="Seg_2188" s="T30">идти-PST.[3SG]</ta>
            <ta e="T32" id="Seg_2189" s="T31">прийти-PST.[3SG]</ta>
            <ta e="T33" id="Seg_2190" s="T32">сын-NOM/GEN.3SG</ta>
            <ta e="T34" id="Seg_2191" s="T33">NEG.EX.[3SG]</ta>
            <ta e="T35" id="Seg_2192" s="T34">девушка.[NOM.SG]</ta>
            <ta e="T36" id="Seg_2193" s="T35">люлька-LAT/LOC.3SG</ta>
            <ta e="T37" id="Seg_2194" s="T36">ложиться.[3SG]</ta>
            <ta e="T38" id="Seg_2195" s="T37">этот.[NOM.SG]</ta>
            <ta e="T39" id="Seg_2196" s="T38">женщина.[NOM.SG]</ta>
            <ta e="T40" id="Seg_2197" s="T39">сердиться-CVB</ta>
            <ta e="T41" id="Seg_2198" s="T40">обрушиться-PST.[3SG]</ta>
            <ta e="T42" id="Seg_2199" s="T41">ребенок-ACC.3SG</ta>
            <ta e="T43" id="Seg_2200" s="T42">взять-PST.[3SG]</ta>
            <ta e="T44" id="Seg_2201" s="T43">сердиться-PST.[3SG]</ta>
            <ta e="T45" id="Seg_2202" s="T44">другой.[NOM.SG]</ta>
            <ta e="T46" id="Seg_2203" s="T45">гора-LAT</ta>
            <ta e="T47" id="Seg_2204" s="T46">пойти-CVB</ta>
            <ta e="T48" id="Seg_2205" s="T47">исчезнуть-PST.[3SG]</ta>
            <ta e="T49" id="Seg_2206" s="T48">лягушка-GEN</ta>
            <ta e="T50" id="Seg_2207" s="T49">женщина.[NOM.SG]</ta>
            <ta e="T51" id="Seg_2208" s="T50">этот.[NOM.SG]</ta>
            <ta e="T52" id="Seg_2209" s="T51">женщина.[NOM.SG]</ta>
            <ta e="T53" id="Seg_2210" s="T52">лягушка-LOC</ta>
            <ta e="T54" id="Seg_2211" s="T53">этот.[NOM.SG]</ta>
            <ta e="T55" id="Seg_2212" s="T54">мальчик</ta>
            <ta e="T56" id="Seg_2213" s="T55">ложиться-CVB</ta>
            <ta e="T57" id="Seg_2214" s="T56">остаться-PST.[3SG]</ta>
            <ta e="T58" id="Seg_2215" s="T57">сын-NOM/GEN.3SG</ta>
            <ta e="T59" id="Seg_2216" s="T58">охотиться-CVB</ta>
            <ta e="T60" id="Seg_2217" s="T59">научиться-HAB-PST.[3SG]</ta>
            <ta e="T61" id="Seg_2218" s="T60">коза.[NOM.SG]</ta>
            <ta e="T62" id="Seg_2219" s="T61">стрелять-HAB-PST.[3SG]</ta>
            <ta e="T63" id="Seg_2220" s="T62">охотиться-CVB</ta>
            <ta e="T64" id="Seg_2221" s="T63">идти-HAB-PST.[3SG]</ta>
            <ta e="T65" id="Seg_2222" s="T64">коза.[NOM.SG]</ta>
            <ta e="T66" id="Seg_2223" s="T65">стрелять-HAB-PST.[3SG]</ta>
            <ta e="T67" id="Seg_2224" s="T66">этот.[NOM.SG]</ta>
            <ta e="T68" id="Seg_2225" s="T67">женщина.[NOM.SG]</ta>
            <ta e="T69" id="Seg_2226" s="T68">мясо.[NOM.SG]</ta>
            <ta e="T70" id="Seg_2227" s="T69">жир.[NOM.SG]</ta>
            <ta e="T71" id="Seg_2228" s="T70">съесть-HAB-PST.[3SG]</ta>
            <ta e="T72" id="Seg_2229" s="T71">этот.[NOM.SG]</ta>
            <ta e="T73" id="Seg_2230" s="T72">мальчик.[NOM.SG]</ta>
            <ta e="T74" id="Seg_2231" s="T73">лягушка-GEN</ta>
            <ta e="T75" id="Seg_2232" s="T74">мальчик.[NOM.SG]</ta>
            <ta e="T76" id="Seg_2233" s="T75">мать-ACC.3SG</ta>
            <ta e="T77" id="Seg_2234" s="T76">спросить-PST.[3SG]</ta>
            <ta e="T78" id="Seg_2235" s="T77">ты.GEN</ta>
            <ta e="T79" id="Seg_2236" s="T78">почему</ta>
            <ta e="T80" id="Seg_2237" s="T79">палец-NOM/GEN/ACC.2SG</ta>
            <ta e="T81" id="Seg_2238" s="T80">три.[NOM.SG]</ta>
            <ta e="T82" id="Seg_2239" s="T81">сказать-PRS.[3SG]</ta>
            <ta e="T83" id="Seg_2240" s="T82">ты.NOM</ta>
            <ta e="T84" id="Seg_2241" s="T83">NEG</ta>
            <ta e="T85" id="Seg_2242" s="T84">я.GEN</ta>
            <ta e="T86" id="Seg_2243" s="T85">мальчик-NOM/GEN/ACC.1SG</ta>
            <ta e="T87" id="Seg_2244" s="T86">быть-PRS-2SG</ta>
            <ta e="T88" id="Seg_2245" s="T87">другой</ta>
            <ta e="T89" id="Seg_2246" s="T88">женщина-GEN</ta>
            <ta e="T90" id="Seg_2247" s="T89">сын-NOM/GEN.3SG</ta>
            <ta e="T91" id="Seg_2248" s="T90">быть-PST-2SG</ta>
            <ta e="T92" id="Seg_2249" s="T91">я.GEN</ta>
            <ta e="T93" id="Seg_2250" s="T92">дочь-NOM/GEN/ACC.1SG</ta>
            <ta e="T94" id="Seg_2251" s="T93">быть-PST.[3SG]</ta>
            <ta e="T95" id="Seg_2252" s="T94">я.NOM</ta>
            <ta e="T96" id="Seg_2253" s="T95">украсть-PST-1SG</ta>
            <ta e="T97" id="Seg_2254" s="T96">люлька-LAT/LOC.3SG</ta>
            <ta e="T98" id="Seg_2255" s="T97">класть-PST-1SG</ta>
            <ta e="T99" id="Seg_2256" s="T98">тогда</ta>
            <ta e="T100" id="Seg_2257" s="T99">сердиться-CVB</ta>
            <ta e="T101" id="Seg_2258" s="T100">пойти-CVB</ta>
            <ta e="T102" id="Seg_2259" s="T101">исчезнуть-PST.[3SG]</ta>
            <ta e="T103" id="Seg_2260" s="T102">другой-LAT</ta>
            <ta e="T104" id="Seg_2261" s="T103">гора-LAT</ta>
            <ta e="T105" id="Seg_2262" s="T104">тот.[NOM.SG]</ta>
            <ta e="T106" id="Seg_2263" s="T105">черный.[NOM.SG]</ta>
            <ta e="T107" id="Seg_2264" s="T106">гора-LOC</ta>
            <ta e="T108" id="Seg_2265" s="T107">там</ta>
            <ta e="T109" id="Seg_2266" s="T108">жить.[3SG]</ta>
            <ta e="T110" id="Seg_2267" s="T109">сегодня</ta>
            <ta e="T111" id="Seg_2268" s="T110">дерево.[NOM.SG]</ta>
            <ta e="T112" id="Seg_2269" s="T111">NEG.AUX-IMP.2SG</ta>
            <ta e="T113" id="Seg_2270" s="T112">дерево-VBLZ-EP-CNG</ta>
            <ta e="T114" id="Seg_2271" s="T113">сегодня</ta>
            <ta e="T115" id="Seg_2272" s="T114">кора.[NOM.SG]</ta>
            <ta e="T116" id="Seg_2273" s="T115">дерево-GEN</ta>
            <ta e="T117" id="Seg_2274" s="T116">лиственница-GEN</ta>
            <ta e="T118" id="Seg_2275" s="T117">кора-ACC.3SG</ta>
            <ta e="T119" id="Seg_2276" s="T118">собирать-IMP.2SG.O</ta>
            <ta e="T120" id="Seg_2277" s="T119">ночевать-FUT-2SG</ta>
            <ta e="T121" id="Seg_2278" s="T120">кора.[NOM.SG]</ta>
            <ta e="T122" id="Seg_2279" s="T121">собирать-PST.[3SG]</ta>
            <ta e="T123" id="Seg_2280" s="T122">огонь.[NOM.SG]</ta>
            <ta e="T124" id="Seg_2281" s="T123">класть-PST.[3SG]</ta>
            <ta e="T125" id="Seg_2282" s="T124">огонь-NOM/GEN.3SG</ta>
            <ta e="T126" id="Seg_2283" s="T125">потухнуть-DETR-RES-PST.[3SG]</ta>
            <ta e="T127" id="Seg_2284" s="T126">мясо.[NOM.SG]</ta>
            <ta e="T128" id="Seg_2285" s="T127">съесть-CVB</ta>
            <ta e="T129" id="Seg_2286" s="T128">кора-3SG-INS</ta>
            <ta e="T130" id="Seg_2287" s="T129">класть-IMP.2SG.O</ta>
            <ta e="T131" id="Seg_2288" s="T130">огонь.[NOM.SG]</ta>
            <ta e="T132" id="Seg_2289" s="T131">кора-3SG-INS</ta>
            <ta e="T133" id="Seg_2290" s="T132">класть-PST.[3SG]</ta>
            <ta e="T134" id="Seg_2291" s="T133">тогда</ta>
            <ta e="T135" id="Seg_2292" s="T134">огонь-NOM/GEN.3SG</ta>
            <ta e="T136" id="Seg_2293" s="T135">потухнуть-DETR-DUR.PRS.[3SG]</ta>
            <ta e="T137" id="Seg_2294" s="T136">пойти-PST.[3SG]</ta>
            <ta e="T138" id="Seg_2295" s="T137">огонь-ACC.3SG</ta>
            <ta e="T139" id="Seg_2296" s="T138">дуть-DUR-PST.[3SG]</ta>
            <ta e="T140" id="Seg_2297" s="T139">тогда</ta>
            <ta e="T141" id="Seg_2298" s="T140">глаз-NOM/GEN.3SG</ta>
            <ta e="T142" id="Seg_2299" s="T141">лопнуть-CVB</ta>
            <ta e="T143" id="Seg_2300" s="T142">обрушиться-PST.[3SG]</ta>
            <ta e="T144" id="Seg_2301" s="T143">тогда</ta>
            <ta e="T145" id="Seg_2302" s="T144">дуть-DUR-PST.[3SG]</ta>
            <ta e="T146" id="Seg_2303" s="T145">другой.[NOM.SG]</ta>
            <ta e="T147" id="Seg_2304" s="T146">глаз-NOM/GEN.3SG</ta>
            <ta e="T148" id="Seg_2305" s="T147">лопнуть-CVB</ta>
            <ta e="T149" id="Seg_2306" s="T148">обрушиться-PST.[3SG]</ta>
            <ta e="T150" id="Seg_2307" s="T149">другой.[NOM.SG]</ta>
            <ta e="T151" id="Seg_2308" s="T150">глаз-NOM/GEN.3SG</ta>
            <ta e="T152" id="Seg_2309" s="T151">лопнуть-CVB</ta>
            <ta e="T153" id="Seg_2310" s="T152">обрушиться-PST.[3SG]</ta>
            <ta e="T154" id="Seg_2311" s="T153">умереть-RES-PST.[3SG]</ta>
            <ta e="T155" id="Seg_2312" s="T154">этот.[NOM.SG]</ta>
            <ta e="T156" id="Seg_2313" s="T155">женщина.[NOM.SG]</ta>
            <ta e="T157" id="Seg_2314" s="T156">мальчик.[NOM.SG]</ta>
            <ta e="T158" id="Seg_2315" s="T157">мясо.[NOM.SG]</ta>
            <ta e="T159" id="Seg_2316" s="T158">жир.[NOM.SG]</ta>
            <ta e="T160" id="Seg_2317" s="T159">взять-PST.[3SG]</ta>
            <ta e="T161" id="Seg_2318" s="T160">тогда</ta>
            <ta e="T162" id="Seg_2319" s="T161">пойти-CVB</ta>
            <ta e="T163" id="Seg_2320" s="T162">исчезнуть-PST.[3SG]</ta>
            <ta e="T164" id="Seg_2321" s="T163">мать-ACC.3SG</ta>
            <ta e="T165" id="Seg_2322" s="T164">искать-CVB</ta>
            <ta e="T166" id="Seg_2323" s="T165">пойти-PST.[3SG]</ta>
            <ta e="T167" id="Seg_2324" s="T166">большой.[NOM.SG]</ta>
            <ta e="T168" id="Seg_2325" s="T167">гора-LAT</ta>
            <ta e="T169" id="Seg_2326" s="T168">пойти-PST.[3SG]</ta>
            <ta e="T170" id="Seg_2327" s="T169">большой.[NOM.SG]</ta>
            <ta e="T171" id="Seg_2328" s="T170">черный.[NOM.SG]</ta>
            <ta e="T172" id="Seg_2329" s="T171">гора-LOC</ta>
            <ta e="T173" id="Seg_2330" s="T172">видеть-PST.[3SG]</ta>
            <ta e="T174" id="Seg_2331" s="T173">чум.[NOM.SG]</ta>
            <ta e="T175" id="Seg_2332" s="T174">стоять-PRS.[3SG]</ta>
            <ta e="T176" id="Seg_2333" s="T175">этот.[NOM.SG]</ta>
            <ta e="T177" id="Seg_2334" s="T176">чум-LOC</ta>
            <ta e="T178" id="Seg_2335" s="T177">огонь.[NOM.SG]</ta>
            <ta e="T179" id="Seg_2336" s="T178">гореть-DUR.[3SG]</ta>
            <ta e="T180" id="Seg_2337" s="T179">дым.[NOM.SG]</ta>
            <ta e="T181" id="Seg_2338" s="T180">дымовое.отверстие-ABL</ta>
            <ta e="T182" id="Seg_2339" s="T181">встать-DUR.[3SG]</ta>
            <ta e="T183" id="Seg_2340" s="T182">тогда</ta>
            <ta e="T184" id="Seg_2341" s="T183">чум-GEN</ta>
            <ta e="T185" id="Seg_2342" s="T184">верх-LAT/LOC.3SG</ta>
            <ta e="T186" id="Seg_2343" s="T185">влезать-RES-PST.[3SG]</ta>
            <ta e="T187" id="Seg_2344" s="T186">этот-ABL</ta>
            <ta e="T188" id="Seg_2345" s="T187">дымовое.отверстие-ABL</ta>
            <ta e="T189" id="Seg_2346" s="T188">вниз</ta>
            <ta e="T190" id="Seg_2347" s="T189">смотреть-PST.[3SG]</ta>
            <ta e="T191" id="Seg_2348" s="T190">мать-NOM/GEN.3SG</ta>
            <ta e="T192" id="Seg_2349" s="T191">сидеть.[3SG]</ta>
            <ta e="T193" id="Seg_2350" s="T192">лягушка-GEN</ta>
            <ta e="T194" id="Seg_2351" s="T193">женщина</ta>
            <ta e="T195" id="Seg_2352" s="T194">девушка.[NOM.SG]</ta>
            <ta e="T196" id="Seg_2353" s="T195">сидеть.[3SG]</ta>
            <ta e="T197" id="Seg_2354" s="T196">этот.[NOM.SG]</ta>
            <ta e="T198" id="Seg_2355" s="T197">дымовое.отверстие-ABL</ta>
            <ta e="T199" id="Seg_2356" s="T198">котел-LAT</ta>
            <ta e="T200" id="Seg_2357" s="T199">жир.[NOM.SG]</ta>
            <ta e="T201" id="Seg_2358" s="T200">крошить-PST.[3SG]</ta>
            <ta e="T202" id="Seg_2359" s="T201">девушка.[NOM.SG]</ta>
            <ta e="T203" id="Seg_2360" s="T202">сказать-PRS.[3SG]</ta>
            <ta e="T204" id="Seg_2361" s="T203">котел-LAT/LOC.1PL</ta>
            <ta e="T205" id="Seg_2362" s="T204">жир.[NOM.SG]</ta>
            <ta e="T206" id="Seg_2363" s="T205">идти-DUR.[3SG]</ta>
            <ta e="T207" id="Seg_2364" s="T206">старый.[NOM.SG]</ta>
            <ta e="T208" id="Seg_2365" s="T207">женщина.[NOM.SG]</ta>
            <ta e="T209" id="Seg_2366" s="T208">дерево.[NOM.SG]</ta>
            <ta e="T210" id="Seg_2367" s="T209">хватать-PST.[3SG]</ta>
            <ta e="T211" id="Seg_2368" s="T210">девушка-ACC.3SG</ta>
            <ta e="T212" id="Seg_2369" s="T211">голова-ABL.3SG</ta>
            <ta e="T213" id="Seg_2370" s="T212">ударить-MOM-PST.[3SG]</ta>
            <ta e="T214" id="Seg_2371" s="T213">NEG.AUX-IMP.2SG</ta>
            <ta e="T215" id="Seg_2372" s="T214">лгать-EP-CNG</ta>
            <ta e="T216" id="Seg_2373" s="T215">ты.GEN</ta>
            <ta e="T217" id="Seg_2374" s="T216">мать-NOM/GEN/ACC.2SG</ta>
            <ta e="T218" id="Seg_2375" s="T217">жир.[NOM.SG]</ta>
            <ta e="T219" id="Seg_2376" s="T218">мясо.[NOM.SG]</ta>
            <ta e="T220" id="Seg_2377" s="T219">съесть-DUR.[3SG]</ta>
            <ta e="T221" id="Seg_2378" s="T220">я.NOM</ta>
            <ta e="T222" id="Seg_2379" s="T221">лилия.саранка</ta>
            <ta e="T223" id="Seg_2380" s="T222">съесть-FUT-1SG</ta>
            <ta e="T224" id="Seg_2381" s="T223">ты.NOM</ta>
            <ta e="T225" id="Seg_2382" s="T224">блудница.[NOM.SG]</ta>
            <ta e="T226" id="Seg_2383" s="T225">мать-NOM/GEN/ACC.2SG-INS</ta>
            <ta e="T227" id="Seg_2384" s="T226">подлый.[NOM.SG]</ta>
            <ta e="T228" id="Seg_2385" s="T227">умереть-CVB</ta>
            <ta e="T229" id="Seg_2386" s="T228">взять-PST.[3SG]</ta>
            <ta e="T230" id="Seg_2387" s="T229">тогда</ta>
            <ta e="T231" id="Seg_2388" s="T230">этот.[NOM.SG]</ta>
            <ta e="T232" id="Seg_2389" s="T231">мальчик.[NOM.SG]</ta>
            <ta e="T233" id="Seg_2390" s="T232">дымовое.отверстие-ABL</ta>
            <ta e="T234" id="Seg_2391" s="T233">спуститься-PST.[3SG]</ta>
            <ta e="T235" id="Seg_2392" s="T234">чум-LAT</ta>
            <ta e="T236" id="Seg_2393" s="T235">войти-PST.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_2394" s="T0">num-n:case</ta>
            <ta e="T2" id="Seg_2395" s="T1">adj-n:case</ta>
            <ta e="T3" id="Seg_2396" s="T2">n-n:num</ta>
            <ta e="T4" id="Seg_2397" s="T3">v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_2398" s="T4">n-n:case</ta>
            <ta e="T6" id="Seg_2399" s="T5">dempro-n:num</ta>
            <ta e="T7" id="Seg_2400" s="T6">num-num&gt;num</ta>
            <ta e="T8" id="Seg_2401" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_2402" s="T8">v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_2403" s="T9">num-n:case</ta>
            <ta e="T11" id="Seg_2404" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_2405" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_2406" s="T12">v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_2407" s="T13">num-n:case</ta>
            <ta e="T15" id="Seg_2408" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_2409" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_2410" s="T16">v-v:tense-v:pn</ta>
            <ta e="T18" id="Seg_2411" s="T17">num-n:case</ta>
            <ta e="T19" id="Seg_2412" s="T18">n-n:case</ta>
            <ta e="T20" id="Seg_2413" s="T19">n-n:case.poss</ta>
            <ta e="T21" id="Seg_2414" s="T20">v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_2415" s="T21">refl-n:case.poss</ta>
            <ta e="T23" id="Seg_2416" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_2417" s="T23">n-n:case.poss</ta>
            <ta e="T25" id="Seg_2418" s="T24">v-v:tense-v:pn</ta>
            <ta e="T26" id="Seg_2419" s="T25">adv</ta>
            <ta e="T27" id="Seg_2420" s="T26">v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_2421" s="T27">dempro-n:case</ta>
            <ta e="T29" id="Seg_2422" s="T28">n-n:case</ta>
            <ta e="T30" id="Seg_2423" s="T29">n-n&gt;v-v:n.fin</ta>
            <ta e="T31" id="Seg_2424" s="T30">v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_2425" s="T31">v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_2426" s="T32">n-n:case.poss</ta>
            <ta e="T34" id="Seg_2427" s="T33">v-v:pn</ta>
            <ta e="T35" id="Seg_2428" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_2429" s="T35">n-n:case.poss</ta>
            <ta e="T37" id="Seg_2430" s="T36">v-v:pn</ta>
            <ta e="T38" id="Seg_2431" s="T37">dempro-n:case</ta>
            <ta e="T39" id="Seg_2432" s="T38">n-n:case</ta>
            <ta e="T40" id="Seg_2433" s="T39">v-v:n.fin</ta>
            <ta e="T41" id="Seg_2434" s="T40">v-v:tense-v:pn</ta>
            <ta e="T42" id="Seg_2435" s="T41">n-n:case.poss</ta>
            <ta e="T43" id="Seg_2436" s="T42">v-v:tense-v:pn</ta>
            <ta e="T44" id="Seg_2437" s="T43">v-v:tense-v:pn</ta>
            <ta e="T45" id="Seg_2438" s="T44">adj-n:case</ta>
            <ta e="T46" id="Seg_2439" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_2440" s="T46">v-v:n.fin</ta>
            <ta e="T48" id="Seg_2441" s="T47">v-v:tense-v:pn</ta>
            <ta e="T49" id="Seg_2442" s="T48">n-n:case</ta>
            <ta e="T50" id="Seg_2443" s="T49">n-n:case</ta>
            <ta e="T51" id="Seg_2444" s="T50">dempro-n:case</ta>
            <ta e="T52" id="Seg_2445" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_2446" s="T52">n-n:case</ta>
            <ta e="T54" id="Seg_2447" s="T53">dempro-n:case</ta>
            <ta e="T55" id="Seg_2448" s="T54">n</ta>
            <ta e="T56" id="Seg_2449" s="T55">v-v:n.fin</ta>
            <ta e="T57" id="Seg_2450" s="T56">v-v:tense-v:pn</ta>
            <ta e="T58" id="Seg_2451" s="T57">n-n:case.poss</ta>
            <ta e="T59" id="Seg_2452" s="T58">v-v:n.fin</ta>
            <ta e="T60" id="Seg_2453" s="T59">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T61" id="Seg_2454" s="T60">n-n:case</ta>
            <ta e="T62" id="Seg_2455" s="T61">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T63" id="Seg_2456" s="T62">v-v:n.fin</ta>
            <ta e="T64" id="Seg_2457" s="T63">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T65" id="Seg_2458" s="T64">n-n:case</ta>
            <ta e="T66" id="Seg_2459" s="T65">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T67" id="Seg_2460" s="T66">dempro-n:case</ta>
            <ta e="T68" id="Seg_2461" s="T67">n-n:case</ta>
            <ta e="T69" id="Seg_2462" s="T68">n-n:case</ta>
            <ta e="T70" id="Seg_2463" s="T69">n-n:case</ta>
            <ta e="T71" id="Seg_2464" s="T70">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T72" id="Seg_2465" s="T71">dempro-n:case</ta>
            <ta e="T73" id="Seg_2466" s="T72">n-n:case</ta>
            <ta e="T74" id="Seg_2467" s="T73">n-n:case</ta>
            <ta e="T75" id="Seg_2468" s="T74">n-n:case</ta>
            <ta e="T76" id="Seg_2469" s="T75">n-n:case.poss</ta>
            <ta e="T77" id="Seg_2470" s="T76">v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_2471" s="T77">pers</ta>
            <ta e="T79" id="Seg_2472" s="T78">que</ta>
            <ta e="T80" id="Seg_2473" s="T79">n-n:case.poss</ta>
            <ta e="T81" id="Seg_2474" s="T80">num-n:case</ta>
            <ta e="T82" id="Seg_2475" s="T81">v-v:tense-v:pn</ta>
            <ta e="T83" id="Seg_2476" s="T82">pers</ta>
            <ta e="T84" id="Seg_2477" s="T83">ptcl</ta>
            <ta e="T85" id="Seg_2478" s="T84">pers</ta>
            <ta e="T86" id="Seg_2479" s="T85">n-n:case.poss</ta>
            <ta e="T87" id="Seg_2480" s="T86">v-v:tense-v:pn</ta>
            <ta e="T88" id="Seg_2481" s="T87">adj</ta>
            <ta e="T89" id="Seg_2482" s="T88">n-n:case</ta>
            <ta e="T90" id="Seg_2483" s="T89">n-n:case.poss</ta>
            <ta e="T91" id="Seg_2484" s="T90">v-v:tense-v:pn</ta>
            <ta e="T92" id="Seg_2485" s="T91">pers</ta>
            <ta e="T93" id="Seg_2486" s="T92">n-n:case.poss</ta>
            <ta e="T94" id="Seg_2487" s="T93">v-v:tense-v:pn</ta>
            <ta e="T95" id="Seg_2488" s="T94">pers</ta>
            <ta e="T96" id="Seg_2489" s="T95">v-v:tense-v:pn</ta>
            <ta e="T97" id="Seg_2490" s="T96">n-n:case.poss</ta>
            <ta e="T98" id="Seg_2491" s="T97">v-v:tense-v:pn</ta>
            <ta e="T99" id="Seg_2492" s="T98">adv</ta>
            <ta e="T100" id="Seg_2493" s="T99">v-v:n.fin</ta>
            <ta e="T101" id="Seg_2494" s="T100">v-v:n.fin</ta>
            <ta e="T102" id="Seg_2495" s="T101">v-v:tense-v:pn</ta>
            <ta e="T103" id="Seg_2496" s="T102">adj-n:case</ta>
            <ta e="T104" id="Seg_2497" s="T103">n-n:case</ta>
            <ta e="T105" id="Seg_2498" s="T104">dempro-n:case</ta>
            <ta e="T106" id="Seg_2499" s="T105">adj-n:case</ta>
            <ta e="T107" id="Seg_2500" s="T106">n-n:case</ta>
            <ta e="T108" id="Seg_2501" s="T107">adv</ta>
            <ta e="T109" id="Seg_2502" s="T108">v-v:pn</ta>
            <ta e="T110" id="Seg_2503" s="T109">adv</ta>
            <ta e="T111" id="Seg_2504" s="T110">n-n:case</ta>
            <ta e="T112" id="Seg_2505" s="T111">aux-v:mood.pn</ta>
            <ta e="T113" id="Seg_2506" s="T112">n-n&gt;v-v:ins-v:n.fin</ta>
            <ta e="T114" id="Seg_2507" s="T113">adv</ta>
            <ta e="T115" id="Seg_2508" s="T114">n-n:case</ta>
            <ta e="T116" id="Seg_2509" s="T115">n-n:case</ta>
            <ta e="T117" id="Seg_2510" s="T116">n-n:case</ta>
            <ta e="T118" id="Seg_2511" s="T117">n-n:case.poss</ta>
            <ta e="T119" id="Seg_2512" s="T118">v-v:mood.pn</ta>
            <ta e="T120" id="Seg_2513" s="T119">v-v:tense-v:pn</ta>
            <ta e="T121" id="Seg_2514" s="T120">n-n:case</ta>
            <ta e="T122" id="Seg_2515" s="T121">v-v:tense-v:pn</ta>
            <ta e="T123" id="Seg_2516" s="T122">n-n:case</ta>
            <ta e="T124" id="Seg_2517" s="T123">v-v:tense-v:pn</ta>
            <ta e="T125" id="Seg_2518" s="T124">n-n:case.poss</ta>
            <ta e="T126" id="Seg_2519" s="T125">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T127" id="Seg_2520" s="T126">n-n:case</ta>
            <ta e="T128" id="Seg_2521" s="T127">v-v:n.fin</ta>
            <ta e="T129" id="Seg_2522" s="T128">n-n:case.poss-n:case</ta>
            <ta e="T130" id="Seg_2523" s="T129">v-v:mood.pn</ta>
            <ta e="T131" id="Seg_2524" s="T130">n-n:case</ta>
            <ta e="T132" id="Seg_2525" s="T131">n-n:case.poss-n:case</ta>
            <ta e="T133" id="Seg_2526" s="T132">v-v:tense-v:pn</ta>
            <ta e="T134" id="Seg_2527" s="T133">adv</ta>
            <ta e="T135" id="Seg_2528" s="T134">n-n:case.poss</ta>
            <ta e="T136" id="Seg_2529" s="T135">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T137" id="Seg_2530" s="T136">v-v:tense-v:pn</ta>
            <ta e="T138" id="Seg_2531" s="T137">n-n:case.poss</ta>
            <ta e="T139" id="Seg_2532" s="T138">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T140" id="Seg_2533" s="T139">adv</ta>
            <ta e="T141" id="Seg_2534" s="T140">n-n:case.poss</ta>
            <ta e="T142" id="Seg_2535" s="T141">v-v:n.fin</ta>
            <ta e="T143" id="Seg_2536" s="T142">v-v:tense-v:pn</ta>
            <ta e="T144" id="Seg_2537" s="T143">adv</ta>
            <ta e="T145" id="Seg_2538" s="T144">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T146" id="Seg_2539" s="T145">adj-n:case</ta>
            <ta e="T147" id="Seg_2540" s="T146">n-n:case.poss</ta>
            <ta e="T148" id="Seg_2541" s="T147">v-v:n.fin</ta>
            <ta e="T149" id="Seg_2542" s="T148">v-v:tense-v:pn</ta>
            <ta e="T150" id="Seg_2543" s="T149">adj-n:case</ta>
            <ta e="T151" id="Seg_2544" s="T150">n-n:case.poss</ta>
            <ta e="T152" id="Seg_2545" s="T151">v-v:n.fin</ta>
            <ta e="T153" id="Seg_2546" s="T152">v-v:tense-v:pn</ta>
            <ta e="T154" id="Seg_2547" s="T153">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T155" id="Seg_2548" s="T154">dempro-n:case</ta>
            <ta e="T156" id="Seg_2549" s="T155">n-n:case</ta>
            <ta e="T157" id="Seg_2550" s="T156">n-n:case</ta>
            <ta e="T158" id="Seg_2551" s="T157">n-n:case</ta>
            <ta e="T159" id="Seg_2552" s="T158">n-n:case</ta>
            <ta e="T160" id="Seg_2553" s="T159">v-v:tense-v:pn</ta>
            <ta e="T161" id="Seg_2554" s="T160">adv</ta>
            <ta e="T162" id="Seg_2555" s="T161">v-v:n.fin</ta>
            <ta e="T163" id="Seg_2556" s="T162">v-v:tense-v:pn</ta>
            <ta e="T164" id="Seg_2557" s="T163">n-n:case.poss</ta>
            <ta e="T165" id="Seg_2558" s="T164">v-v:n.fin</ta>
            <ta e="T166" id="Seg_2559" s="T165">v-v:tense-v:pn</ta>
            <ta e="T167" id="Seg_2560" s="T166">adj-n:case</ta>
            <ta e="T168" id="Seg_2561" s="T167">n-n:case</ta>
            <ta e="T169" id="Seg_2562" s="T168">v-v:tense-v:pn</ta>
            <ta e="T170" id="Seg_2563" s="T169">adj-n:case</ta>
            <ta e="T171" id="Seg_2564" s="T170">adj-n:case</ta>
            <ta e="T172" id="Seg_2565" s="T171">n-n:case</ta>
            <ta e="T173" id="Seg_2566" s="T172">v-v:tense-v:pn</ta>
            <ta e="T174" id="Seg_2567" s="T173">n-n:case</ta>
            <ta e="T175" id="Seg_2568" s="T174">v-v:tense-v:pn</ta>
            <ta e="T176" id="Seg_2569" s="T175">dempro-n:case</ta>
            <ta e="T177" id="Seg_2570" s="T176">n-n:case</ta>
            <ta e="T178" id="Seg_2571" s="T177">n-n:case</ta>
            <ta e="T179" id="Seg_2572" s="T178">v-v&gt;v-v:pn</ta>
            <ta e="T180" id="Seg_2573" s="T179">n-n:case</ta>
            <ta e="T181" id="Seg_2574" s="T180">n-n:case</ta>
            <ta e="T182" id="Seg_2575" s="T181">v-v&gt;v-v:pn</ta>
            <ta e="T183" id="Seg_2576" s="T182">adv</ta>
            <ta e="T184" id="Seg_2577" s="T183">n-n:case</ta>
            <ta e="T185" id="Seg_2578" s="T184">n-n:case.poss</ta>
            <ta e="T186" id="Seg_2579" s="T185">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T187" id="Seg_2580" s="T186">dempro-n:case</ta>
            <ta e="T188" id="Seg_2581" s="T187">n-n:case</ta>
            <ta e="T189" id="Seg_2582" s="T188">adv</ta>
            <ta e="T190" id="Seg_2583" s="T189">v-v:tense-v:pn</ta>
            <ta e="T191" id="Seg_2584" s="T190">n-n:case.poss</ta>
            <ta e="T192" id="Seg_2585" s="T191">v-v:pn</ta>
            <ta e="T193" id="Seg_2586" s="T192">n-n:case</ta>
            <ta e="T194" id="Seg_2587" s="T193">n</ta>
            <ta e="T195" id="Seg_2588" s="T194">n-n:case</ta>
            <ta e="T196" id="Seg_2589" s="T195">v-v:pn</ta>
            <ta e="T197" id="Seg_2590" s="T196">dempro-n:case</ta>
            <ta e="T198" id="Seg_2591" s="T197">n-n:case</ta>
            <ta e="T199" id="Seg_2592" s="T198">n-n:case</ta>
            <ta e="T200" id="Seg_2593" s="T199">n-n:case</ta>
            <ta e="T201" id="Seg_2594" s="T200">v-v:tense-v:pn</ta>
            <ta e="T202" id="Seg_2595" s="T201">n-n:case</ta>
            <ta e="T203" id="Seg_2596" s="T202">v-v:tense-v:pn</ta>
            <ta e="T204" id="Seg_2597" s="T203">n-n:case.poss</ta>
            <ta e="T205" id="Seg_2598" s="T204">n-n:case</ta>
            <ta e="T206" id="Seg_2599" s="T205">v-v&gt;v-v:pn</ta>
            <ta e="T207" id="Seg_2600" s="T206">adj-n:case</ta>
            <ta e="T208" id="Seg_2601" s="T207">n-n:case</ta>
            <ta e="T209" id="Seg_2602" s="T208">n-n:case</ta>
            <ta e="T210" id="Seg_2603" s="T209">v-v:tense-v:pn</ta>
            <ta e="T211" id="Seg_2604" s="T210">n-n:case.poss</ta>
            <ta e="T212" id="Seg_2605" s="T211">n-n:case.poss</ta>
            <ta e="T213" id="Seg_2606" s="T212">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T214" id="Seg_2607" s="T213">aux-v:mood.pn</ta>
            <ta e="T215" id="Seg_2608" s="T214">v-v:ins-v:n.fin</ta>
            <ta e="T216" id="Seg_2609" s="T215">pers</ta>
            <ta e="T217" id="Seg_2610" s="T216">n-n:case.poss</ta>
            <ta e="T218" id="Seg_2611" s="T217">n-n:case</ta>
            <ta e="T219" id="Seg_2612" s="T218">n-n:case</ta>
            <ta e="T220" id="Seg_2613" s="T219">v-v&gt;v-v:pn</ta>
            <ta e="T221" id="Seg_2614" s="T220">pers</ta>
            <ta e="T222" id="Seg_2615" s="T221">n</ta>
            <ta e="T223" id="Seg_2616" s="T222">v-v:tense-v:pn</ta>
            <ta e="T224" id="Seg_2617" s="T223">pers</ta>
            <ta e="T225" id="Seg_2618" s="T224">n-n:case</ta>
            <ta e="T226" id="Seg_2619" s="T225">n-n:case.poss-n:case</ta>
            <ta e="T227" id="Seg_2620" s="T226">adj-n:case</ta>
            <ta e="T228" id="Seg_2621" s="T227">v-v:n.fin</ta>
            <ta e="T229" id="Seg_2622" s="T228">v-v:tense-v:pn</ta>
            <ta e="T230" id="Seg_2623" s="T229">adv</ta>
            <ta e="T231" id="Seg_2624" s="T230">dempro-n:case</ta>
            <ta e="T232" id="Seg_2625" s="T231">n-n:case</ta>
            <ta e="T233" id="Seg_2626" s="T232">n-n:case</ta>
            <ta e="T234" id="Seg_2627" s="T233">v-v:tense-v:pn</ta>
            <ta e="T235" id="Seg_2628" s="T234">n-n:case</ta>
            <ta e="T236" id="Seg_2629" s="T235">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_2630" s="T0">num</ta>
            <ta e="T2" id="Seg_2631" s="T1">adj</ta>
            <ta e="T3" id="Seg_2632" s="T2">n</ta>
            <ta e="T4" id="Seg_2633" s="T3">v</ta>
            <ta e="T5" id="Seg_2634" s="T4">n</ta>
            <ta e="T6" id="Seg_2635" s="T5">dempro</ta>
            <ta e="T7" id="Seg_2636" s="T6">num</ta>
            <ta e="T8" id="Seg_2637" s="T7">n</ta>
            <ta e="T9" id="Seg_2638" s="T8">v</ta>
            <ta e="T10" id="Seg_2639" s="T9">num</ta>
            <ta e="T11" id="Seg_2640" s="T10">n</ta>
            <ta e="T12" id="Seg_2641" s="T11">n</ta>
            <ta e="T13" id="Seg_2642" s="T12">v</ta>
            <ta e="T14" id="Seg_2643" s="T13">num</ta>
            <ta e="T15" id="Seg_2644" s="T14">n</ta>
            <ta e="T16" id="Seg_2645" s="T15">n</ta>
            <ta e="T17" id="Seg_2646" s="T16">v</ta>
            <ta e="T18" id="Seg_2647" s="T17">num</ta>
            <ta e="T19" id="Seg_2648" s="T18">n</ta>
            <ta e="T20" id="Seg_2649" s="T19">n</ta>
            <ta e="T21" id="Seg_2650" s="T20">v</ta>
            <ta e="T22" id="Seg_2651" s="T21">refl</ta>
            <ta e="T23" id="Seg_2652" s="T22">n</ta>
            <ta e="T24" id="Seg_2653" s="T23">n</ta>
            <ta e="T25" id="Seg_2654" s="T24">v</ta>
            <ta e="T26" id="Seg_2655" s="T25">adv</ta>
            <ta e="T27" id="Seg_2656" s="T26">v</ta>
            <ta e="T28" id="Seg_2657" s="T27">dempro</ta>
            <ta e="T29" id="Seg_2658" s="T28">n</ta>
            <ta e="T30" id="Seg_2659" s="T29">v</ta>
            <ta e="T31" id="Seg_2660" s="T30">v</ta>
            <ta e="T32" id="Seg_2661" s="T31">v</ta>
            <ta e="T33" id="Seg_2662" s="T32">n</ta>
            <ta e="T34" id="Seg_2663" s="T33">v</ta>
            <ta e="T35" id="Seg_2664" s="T34">n</ta>
            <ta e="T36" id="Seg_2665" s="T35">n</ta>
            <ta e="T37" id="Seg_2666" s="T36">v</ta>
            <ta e="T38" id="Seg_2667" s="T37">dempro</ta>
            <ta e="T39" id="Seg_2668" s="T38">n</ta>
            <ta e="T40" id="Seg_2669" s="T39">v</ta>
            <ta e="T41" id="Seg_2670" s="T40">v</ta>
            <ta e="T42" id="Seg_2671" s="T41">n</ta>
            <ta e="T43" id="Seg_2672" s="T42">v</ta>
            <ta e="T44" id="Seg_2673" s="T43">v</ta>
            <ta e="T45" id="Seg_2674" s="T44">adj</ta>
            <ta e="T46" id="Seg_2675" s="T45">n</ta>
            <ta e="T47" id="Seg_2676" s="T46">v</ta>
            <ta e="T48" id="Seg_2677" s="T47">v</ta>
            <ta e="T49" id="Seg_2678" s="T48">n</ta>
            <ta e="T50" id="Seg_2679" s="T49">n</ta>
            <ta e="T51" id="Seg_2680" s="T50">dempro</ta>
            <ta e="T52" id="Seg_2681" s="T51">n</ta>
            <ta e="T53" id="Seg_2682" s="T52">n</ta>
            <ta e="T54" id="Seg_2683" s="T53">dempro</ta>
            <ta e="T55" id="Seg_2684" s="T54">n</ta>
            <ta e="T56" id="Seg_2685" s="T55">v</ta>
            <ta e="T57" id="Seg_2686" s="T56">v</ta>
            <ta e="T58" id="Seg_2687" s="T57">n</ta>
            <ta e="T59" id="Seg_2688" s="T58">v</ta>
            <ta e="T60" id="Seg_2689" s="T59">v</ta>
            <ta e="T61" id="Seg_2690" s="T60">n</ta>
            <ta e="T62" id="Seg_2691" s="T61">v</ta>
            <ta e="T63" id="Seg_2692" s="T62">v</ta>
            <ta e="T64" id="Seg_2693" s="T63">v</ta>
            <ta e="T65" id="Seg_2694" s="T64">n</ta>
            <ta e="T66" id="Seg_2695" s="T65">v</ta>
            <ta e="T67" id="Seg_2696" s="T66">dempro</ta>
            <ta e="T68" id="Seg_2697" s="T67">n</ta>
            <ta e="T69" id="Seg_2698" s="T68">n</ta>
            <ta e="T70" id="Seg_2699" s="T69">n</ta>
            <ta e="T71" id="Seg_2700" s="T70">v</ta>
            <ta e="T72" id="Seg_2701" s="T71">dempro</ta>
            <ta e="T73" id="Seg_2702" s="T72">n</ta>
            <ta e="T74" id="Seg_2703" s="T73">n</ta>
            <ta e="T75" id="Seg_2704" s="T74">n</ta>
            <ta e="T76" id="Seg_2705" s="T75">n</ta>
            <ta e="T77" id="Seg_2706" s="T76">v</ta>
            <ta e="T78" id="Seg_2707" s="T77">pers</ta>
            <ta e="T79" id="Seg_2708" s="T78">que</ta>
            <ta e="T80" id="Seg_2709" s="T79">n</ta>
            <ta e="T81" id="Seg_2710" s="T80">num</ta>
            <ta e="T82" id="Seg_2711" s="T81">v</ta>
            <ta e="T83" id="Seg_2712" s="T82">pers</ta>
            <ta e="T84" id="Seg_2713" s="T83">ptcl</ta>
            <ta e="T85" id="Seg_2714" s="T84">pers</ta>
            <ta e="T86" id="Seg_2715" s="T85">n</ta>
            <ta e="T87" id="Seg_2716" s="T86">v</ta>
            <ta e="T88" id="Seg_2717" s="T87">adj</ta>
            <ta e="T89" id="Seg_2718" s="T88">n</ta>
            <ta e="T90" id="Seg_2719" s="T89">n</ta>
            <ta e="T91" id="Seg_2720" s="T90">v</ta>
            <ta e="T92" id="Seg_2721" s="T91">pers</ta>
            <ta e="T93" id="Seg_2722" s="T92">n</ta>
            <ta e="T94" id="Seg_2723" s="T93">v</ta>
            <ta e="T95" id="Seg_2724" s="T94">pers</ta>
            <ta e="T96" id="Seg_2725" s="T95">v</ta>
            <ta e="T97" id="Seg_2726" s="T96">n</ta>
            <ta e="T98" id="Seg_2727" s="T97">v</ta>
            <ta e="T99" id="Seg_2728" s="T98">adv</ta>
            <ta e="T100" id="Seg_2729" s="T99">v</ta>
            <ta e="T101" id="Seg_2730" s="T100">v</ta>
            <ta e="T102" id="Seg_2731" s="T101">v</ta>
            <ta e="T103" id="Seg_2732" s="T102">adj</ta>
            <ta e="T104" id="Seg_2733" s="T103">n</ta>
            <ta e="T105" id="Seg_2734" s="T104">n</ta>
            <ta e="T106" id="Seg_2735" s="T105">adj</ta>
            <ta e="T107" id="Seg_2736" s="T106">n</ta>
            <ta e="T108" id="Seg_2737" s="T107">adv</ta>
            <ta e="T109" id="Seg_2738" s="T108">v</ta>
            <ta e="T110" id="Seg_2739" s="T109">adv</ta>
            <ta e="T111" id="Seg_2740" s="T110">n</ta>
            <ta e="T112" id="Seg_2741" s="T111">aux</ta>
            <ta e="T113" id="Seg_2742" s="T112">v</ta>
            <ta e="T114" id="Seg_2743" s="T113">adv</ta>
            <ta e="T115" id="Seg_2744" s="T114">n</ta>
            <ta e="T116" id="Seg_2745" s="T115">n</ta>
            <ta e="T117" id="Seg_2746" s="T116">n</ta>
            <ta e="T118" id="Seg_2747" s="T117">n</ta>
            <ta e="T119" id="Seg_2748" s="T118">v</ta>
            <ta e="T120" id="Seg_2749" s="T119">v</ta>
            <ta e="T121" id="Seg_2750" s="T120">n</ta>
            <ta e="T122" id="Seg_2751" s="T121">v</ta>
            <ta e="T123" id="Seg_2752" s="T122">n</ta>
            <ta e="T124" id="Seg_2753" s="T123">v</ta>
            <ta e="T125" id="Seg_2754" s="T124">n</ta>
            <ta e="T126" id="Seg_2755" s="T125">v</ta>
            <ta e="T127" id="Seg_2756" s="T126">n</ta>
            <ta e="T128" id="Seg_2757" s="T127">v</ta>
            <ta e="T129" id="Seg_2758" s="T128">n</ta>
            <ta e="T130" id="Seg_2759" s="T129">v</ta>
            <ta e="T131" id="Seg_2760" s="T130">n</ta>
            <ta e="T132" id="Seg_2761" s="T131">n</ta>
            <ta e="T133" id="Seg_2762" s="T132">v</ta>
            <ta e="T134" id="Seg_2763" s="T133">adv</ta>
            <ta e="T135" id="Seg_2764" s="T134">n</ta>
            <ta e="T136" id="Seg_2765" s="T135">v</ta>
            <ta e="T137" id="Seg_2766" s="T136">v</ta>
            <ta e="T138" id="Seg_2767" s="T137">n</ta>
            <ta e="T139" id="Seg_2768" s="T138">v</ta>
            <ta e="T140" id="Seg_2769" s="T139">adv</ta>
            <ta e="T141" id="Seg_2770" s="T140">n</ta>
            <ta e="T142" id="Seg_2771" s="T141">v</ta>
            <ta e="T143" id="Seg_2772" s="T142">v</ta>
            <ta e="T144" id="Seg_2773" s="T143">adv</ta>
            <ta e="T145" id="Seg_2774" s="T144">v</ta>
            <ta e="T146" id="Seg_2775" s="T145">adj</ta>
            <ta e="T147" id="Seg_2776" s="T146">n</ta>
            <ta e="T148" id="Seg_2777" s="T147">v</ta>
            <ta e="T149" id="Seg_2778" s="T148">v</ta>
            <ta e="T150" id="Seg_2779" s="T149">adj</ta>
            <ta e="T151" id="Seg_2780" s="T150">n</ta>
            <ta e="T152" id="Seg_2781" s="T151">v</ta>
            <ta e="T153" id="Seg_2782" s="T152">v</ta>
            <ta e="T154" id="Seg_2783" s="T153">v</ta>
            <ta e="T155" id="Seg_2784" s="T154">dempro</ta>
            <ta e="T156" id="Seg_2785" s="T155">n</ta>
            <ta e="T157" id="Seg_2786" s="T156">n</ta>
            <ta e="T158" id="Seg_2787" s="T157">n</ta>
            <ta e="T159" id="Seg_2788" s="T158">n</ta>
            <ta e="T160" id="Seg_2789" s="T159">v</ta>
            <ta e="T161" id="Seg_2790" s="T160">adv</ta>
            <ta e="T162" id="Seg_2791" s="T161">v</ta>
            <ta e="T163" id="Seg_2792" s="T162">v</ta>
            <ta e="T164" id="Seg_2793" s="T163">n</ta>
            <ta e="T165" id="Seg_2794" s="T164">v</ta>
            <ta e="T166" id="Seg_2795" s="T165">v</ta>
            <ta e="T167" id="Seg_2796" s="T166">adj</ta>
            <ta e="T168" id="Seg_2797" s="T167">n</ta>
            <ta e="T169" id="Seg_2798" s="T168">v</ta>
            <ta e="T170" id="Seg_2799" s="T169">adj</ta>
            <ta e="T171" id="Seg_2800" s="T170">adj</ta>
            <ta e="T172" id="Seg_2801" s="T171">n</ta>
            <ta e="T173" id="Seg_2802" s="T172">v</ta>
            <ta e="T174" id="Seg_2803" s="T173">n</ta>
            <ta e="T175" id="Seg_2804" s="T174">v</ta>
            <ta e="T176" id="Seg_2805" s="T175">dempro</ta>
            <ta e="T177" id="Seg_2806" s="T176">n</ta>
            <ta e="T178" id="Seg_2807" s="T177">n</ta>
            <ta e="T179" id="Seg_2808" s="T178">v</ta>
            <ta e="T180" id="Seg_2809" s="T179">n</ta>
            <ta e="T181" id="Seg_2810" s="T180">n</ta>
            <ta e="T182" id="Seg_2811" s="T181">v</ta>
            <ta e="T183" id="Seg_2812" s="T182">adv</ta>
            <ta e="T184" id="Seg_2813" s="T183">n</ta>
            <ta e="T185" id="Seg_2814" s="T184">n</ta>
            <ta e="T186" id="Seg_2815" s="T185">v</ta>
            <ta e="T187" id="Seg_2816" s="T186">dempro</ta>
            <ta e="T188" id="Seg_2817" s="T187">n</ta>
            <ta e="T189" id="Seg_2818" s="T188">adv</ta>
            <ta e="T190" id="Seg_2819" s="T189">v</ta>
            <ta e="T191" id="Seg_2820" s="T190">n</ta>
            <ta e="T192" id="Seg_2821" s="T191">v</ta>
            <ta e="T193" id="Seg_2822" s="T192">n</ta>
            <ta e="T194" id="Seg_2823" s="T193">n</ta>
            <ta e="T195" id="Seg_2824" s="T194">n</ta>
            <ta e="T196" id="Seg_2825" s="T195">v</ta>
            <ta e="T197" id="Seg_2826" s="T196">dempro</ta>
            <ta e="T198" id="Seg_2827" s="T197">n</ta>
            <ta e="T199" id="Seg_2828" s="T198">n</ta>
            <ta e="T200" id="Seg_2829" s="T199">n</ta>
            <ta e="T201" id="Seg_2830" s="T200">v</ta>
            <ta e="T202" id="Seg_2831" s="T201">n</ta>
            <ta e="T203" id="Seg_2832" s="T202">v</ta>
            <ta e="T204" id="Seg_2833" s="T203">n</ta>
            <ta e="T205" id="Seg_2834" s="T204">n</ta>
            <ta e="T206" id="Seg_2835" s="T205">v</ta>
            <ta e="T207" id="Seg_2836" s="T206">adj</ta>
            <ta e="T208" id="Seg_2837" s="T207">n</ta>
            <ta e="T209" id="Seg_2838" s="T208">n</ta>
            <ta e="T210" id="Seg_2839" s="T209">v</ta>
            <ta e="T211" id="Seg_2840" s="T210">n</ta>
            <ta e="T212" id="Seg_2841" s="T211">n</ta>
            <ta e="T213" id="Seg_2842" s="T212">v</ta>
            <ta e="T214" id="Seg_2843" s="T213">aux</ta>
            <ta e="T215" id="Seg_2844" s="T214">v</ta>
            <ta e="T216" id="Seg_2845" s="T215">pers</ta>
            <ta e="T217" id="Seg_2846" s="T216">n</ta>
            <ta e="T218" id="Seg_2847" s="T217">n</ta>
            <ta e="T219" id="Seg_2848" s="T218">n</ta>
            <ta e="T220" id="Seg_2849" s="T219">v</ta>
            <ta e="T221" id="Seg_2850" s="T220">pers</ta>
            <ta e="T222" id="Seg_2851" s="T221">n</ta>
            <ta e="T223" id="Seg_2852" s="T222">v</ta>
            <ta e="T224" id="Seg_2853" s="T223">pers</ta>
            <ta e="T225" id="Seg_2854" s="T224">n</ta>
            <ta e="T226" id="Seg_2855" s="T225">n</ta>
            <ta e="T227" id="Seg_2856" s="T226">adj</ta>
            <ta e="T228" id="Seg_2857" s="T227">v</ta>
            <ta e="T229" id="Seg_2858" s="T228">v</ta>
            <ta e="T230" id="Seg_2859" s="T229">adv</ta>
            <ta e="T231" id="Seg_2860" s="T230">dempro</ta>
            <ta e="T232" id="Seg_2861" s="T231">n</ta>
            <ta e="T233" id="Seg_2862" s="T232">n</ta>
            <ta e="T234" id="Seg_2863" s="T233">v</ta>
            <ta e="T235" id="Seg_2864" s="T234">n</ta>
            <ta e="T236" id="Seg_2865" s="T235">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_2866" s="T2">np.h:A</ta>
            <ta e="T5" id="Seg_2867" s="T4">np:L</ta>
            <ta e="T6" id="Seg_2868" s="T5">pro.h:A</ta>
            <ta e="T8" id="Seg_2869" s="T7">np.h:Th</ta>
            <ta e="T11" id="Seg_2870" s="T10">np.h:Poss</ta>
            <ta e="T12" id="Seg_2871" s="T11">np.h:Th</ta>
            <ta e="T15" id="Seg_2872" s="T14">np.h:Poss</ta>
            <ta e="T16" id="Seg_2873" s="T15">np.h:Th</ta>
            <ta e="T19" id="Seg_2874" s="T18">np.h:A</ta>
            <ta e="T20" id="Seg_2875" s="T19">np.h:Th 0.3.h:Poss</ta>
            <ta e="T22" id="Seg_2876" s="T21">pro.h:Poss</ta>
            <ta e="T23" id="Seg_2877" s="T22">np.h:Th</ta>
            <ta e="T24" id="Seg_2878" s="T23">np:G 0.3.h:Poss</ta>
            <ta e="T25" id="Seg_2879" s="T24">0.3.h:A</ta>
            <ta e="T26" id="Seg_2880" s="T25">adv:Time</ta>
            <ta e="T29" id="Seg_2881" s="T28">np.h:A</ta>
            <ta e="T31" id="Seg_2882" s="T30">0.3.h:A</ta>
            <ta e="T32" id="Seg_2883" s="T31">0.3.h:A</ta>
            <ta e="T33" id="Seg_2884" s="T32">np.h:Th 0.3.h:Poss</ta>
            <ta e="T35" id="Seg_2885" s="T34">np.h:Th</ta>
            <ta e="T36" id="Seg_2886" s="T35">np:L 0.3.h:Poss</ta>
            <ta e="T39" id="Seg_2887" s="T38">np.h:E</ta>
            <ta e="T42" id="Seg_2888" s="T41">np.h:Th 0.3.h:Poss</ta>
            <ta e="T44" id="Seg_2889" s="T43">0.3.h:E</ta>
            <ta e="T46" id="Seg_2890" s="T45">np:G</ta>
            <ta e="T48" id="Seg_2891" s="T47">0.3.h:A</ta>
            <ta e="T52" id="Seg_2892" s="T51">np.h:Th</ta>
            <ta e="T53" id="Seg_2893" s="T52">np:L</ta>
            <ta e="T55" id="Seg_2894" s="T54">np.h:Th</ta>
            <ta e="T58" id="Seg_2895" s="T57">np.h:A 0.3.h:Poss</ta>
            <ta e="T61" id="Seg_2896" s="T60">np:P</ta>
            <ta e="T62" id="Seg_2897" s="T61">0.3.h:A</ta>
            <ta e="T64" id="Seg_2898" s="T63">0.3.h:A</ta>
            <ta e="T65" id="Seg_2899" s="T64">np:P</ta>
            <ta e="T66" id="Seg_2900" s="T65">0.3.h:A</ta>
            <ta e="T68" id="Seg_2901" s="T67">np.h:A</ta>
            <ta e="T69" id="Seg_2902" s="T68">np:P</ta>
            <ta e="T70" id="Seg_2903" s="T69">np:P</ta>
            <ta e="T73" id="Seg_2904" s="T72">np.h:Th</ta>
            <ta e="T74" id="Seg_2905" s="T73">np.h:P</ta>
            <ta e="T75" id="Seg_2906" s="T74">np.h:P</ta>
            <ta e="T76" id="Seg_2907" s="T75">np.h:E 0.3.h:Poss</ta>
            <ta e="T78" id="Seg_2908" s="T77">pro.h:Poss</ta>
            <ta e="T80" id="Seg_2909" s="T79">np:Th 0.2.h:Poss</ta>
            <ta e="T82" id="Seg_2910" s="T81">0.3.h:A</ta>
            <ta e="T83" id="Seg_2911" s="T82">pro.h:Th</ta>
            <ta e="T85" id="Seg_2912" s="T84">pro.h:Poss</ta>
            <ta e="T89" id="Seg_2913" s="T88">np.h:Poss</ta>
            <ta e="T91" id="Seg_2914" s="T90">0.2.h:Th</ta>
            <ta e="T92" id="Seg_2915" s="T91">pro.h:Poss</ta>
            <ta e="T93" id="Seg_2916" s="T92">np.h:Th</ta>
            <ta e="T95" id="Seg_2917" s="T94">pro.h:A</ta>
            <ta e="T97" id="Seg_2918" s="T96">np:G 0.3.h:Poss</ta>
            <ta e="T98" id="Seg_2919" s="T97">0.1.h:A</ta>
            <ta e="T99" id="Seg_2920" s="T98">adv:Time</ta>
            <ta e="T100" id="Seg_2921" s="T99">0.3.h:E</ta>
            <ta e="T102" id="Seg_2922" s="T101">0.3.h:A</ta>
            <ta e="T104" id="Seg_2923" s="T103">np:G</ta>
            <ta e="T107" id="Seg_2924" s="T106">np:L</ta>
            <ta e="T108" id="Seg_2925" s="T107">adv:L</ta>
            <ta e="T109" id="Seg_2926" s="T108">0.3.h:Th</ta>
            <ta e="T110" id="Seg_2927" s="T109">adv:Time</ta>
            <ta e="T111" id="Seg_2928" s="T110">np:P</ta>
            <ta e="T112" id="Seg_2929" s="T111">0.2.h:A</ta>
            <ta e="T114" id="Seg_2930" s="T113">adv:Time</ta>
            <ta e="T115" id="Seg_2931" s="T114">np:P</ta>
            <ta e="T116" id="Seg_2932" s="T115">np:Poss</ta>
            <ta e="T117" id="Seg_2933" s="T116">np:Poss</ta>
            <ta e="T118" id="Seg_2934" s="T117">np:P</ta>
            <ta e="T119" id="Seg_2935" s="T118">0.2.h:A</ta>
            <ta e="T120" id="Seg_2936" s="T119">0.2.h:A</ta>
            <ta e="T121" id="Seg_2937" s="T120">np:P</ta>
            <ta e="T122" id="Seg_2938" s="T121">0.3.h:A</ta>
            <ta e="T123" id="Seg_2939" s="T122">np:P</ta>
            <ta e="T124" id="Seg_2940" s="T123">0.3.h:A</ta>
            <ta e="T125" id="Seg_2941" s="T124">np:Th 0.3.h:Poss</ta>
            <ta e="T127" id="Seg_2942" s="T126">np:P</ta>
            <ta e="T128" id="Seg_2943" s="T127">0.3.h:A</ta>
            <ta e="T129" id="Seg_2944" s="T128">np:Ins 0.3.h:Poss</ta>
            <ta e="T130" id="Seg_2945" s="T129">0.3.h:A 0.3.h:P</ta>
            <ta e="T131" id="Seg_2946" s="T130">np:P</ta>
            <ta e="T132" id="Seg_2947" s="T131">np:Ins 0.3.h:Poss</ta>
            <ta e="T133" id="Seg_2948" s="T132">0.3.h:A</ta>
            <ta e="T134" id="Seg_2949" s="T133">adv:Time</ta>
            <ta e="T135" id="Seg_2950" s="T134">np:Th 0.3.h:Poss</ta>
            <ta e="T137" id="Seg_2951" s="T136">0.3.h:A</ta>
            <ta e="T138" id="Seg_2952" s="T137">np:Th 0.3.h:Poss</ta>
            <ta e="T139" id="Seg_2953" s="T138">0.3.h:A</ta>
            <ta e="T140" id="Seg_2954" s="T139">adv:Time</ta>
            <ta e="T141" id="Seg_2955" s="T140">np:Th 0.3.h:Poss</ta>
            <ta e="T144" id="Seg_2956" s="T143">adv:Time</ta>
            <ta e="T145" id="Seg_2957" s="T144">0.3.h:A</ta>
            <ta e="T147" id="Seg_2958" s="T146">np:Th 0.3.h:Poss</ta>
            <ta e="T151" id="Seg_2959" s="T150">np:Th 0.3.h:Poss</ta>
            <ta e="T156" id="Seg_2960" s="T155">np.h:E</ta>
            <ta e="T157" id="Seg_2961" s="T156">np.h:A</ta>
            <ta e="T158" id="Seg_2962" s="T157">np:Th</ta>
            <ta e="T159" id="Seg_2963" s="T158">np:Th</ta>
            <ta e="T161" id="Seg_2964" s="T160">adv:Time</ta>
            <ta e="T163" id="Seg_2965" s="T162">0.3.h:A</ta>
            <ta e="T164" id="Seg_2966" s="T163">np.h:Th 0.3.h:Poss</ta>
            <ta e="T166" id="Seg_2967" s="T165">0.3.h:A</ta>
            <ta e="T168" id="Seg_2968" s="T167">np:G</ta>
            <ta e="T169" id="Seg_2969" s="T168">0.3.h:A</ta>
            <ta e="T172" id="Seg_2970" s="T171">np:L</ta>
            <ta e="T173" id="Seg_2971" s="T172">0.3.h:E</ta>
            <ta e="T174" id="Seg_2972" s="T173">np:Th</ta>
            <ta e="T177" id="Seg_2973" s="T176">np:L</ta>
            <ta e="T178" id="Seg_2974" s="T177">np:Th</ta>
            <ta e="T180" id="Seg_2975" s="T179">np:Th</ta>
            <ta e="T181" id="Seg_2976" s="T180">np:So</ta>
            <ta e="T183" id="Seg_2977" s="T182">adv:Time</ta>
            <ta e="T184" id="Seg_2978" s="T183">np:Poss</ta>
            <ta e="T185" id="Seg_2979" s="T184">np:G</ta>
            <ta e="T186" id="Seg_2980" s="T185">0.3.h:A</ta>
            <ta e="T187" id="Seg_2981" s="T186">adv:So</ta>
            <ta e="T188" id="Seg_2982" s="T187">np:So</ta>
            <ta e="T189" id="Seg_2983" s="T188">adv:G</ta>
            <ta e="T190" id="Seg_2984" s="T189">0.3.h:A</ta>
            <ta e="T191" id="Seg_2985" s="T190">np.h:Th 0.3.h:Poss</ta>
            <ta e="T194" id="Seg_2986" s="T192">np.h:Poss</ta>
            <ta e="T195" id="Seg_2987" s="T194">np.h:Th</ta>
            <ta e="T197" id="Seg_2988" s="T196">pro.h:A</ta>
            <ta e="T198" id="Seg_2989" s="T197">np:So</ta>
            <ta e="T199" id="Seg_2990" s="T198">np:G</ta>
            <ta e="T200" id="Seg_2991" s="T199">np:P</ta>
            <ta e="T202" id="Seg_2992" s="T201">np.h:A</ta>
            <ta e="T204" id="Seg_2993" s="T203">np:G 0.1.h:Poss</ta>
            <ta e="T205" id="Seg_2994" s="T204">np:Th</ta>
            <ta e="T208" id="Seg_2995" s="T207">np.h:A</ta>
            <ta e="T209" id="Seg_2996" s="T208">np:P</ta>
            <ta e="T211" id="Seg_2997" s="T210">np.h:P 0.3.h:Poss</ta>
            <ta e="T212" id="Seg_2998" s="T211">np:G 0.3.h:Poss</ta>
            <ta e="T213" id="Seg_2999" s="T212">0.3.h:A</ta>
            <ta e="T214" id="Seg_3000" s="T213">0.2.h:A</ta>
            <ta e="T216" id="Seg_3001" s="T215">pro.h:Poss</ta>
            <ta e="T218" id="Seg_3002" s="T217">np:P</ta>
            <ta e="T219" id="Seg_3003" s="T218">np:P</ta>
            <ta e="T221" id="Seg_3004" s="T220">pro.h:A</ta>
            <ta e="T222" id="Seg_3005" s="T221">np:P</ta>
            <ta e="T224" id="Seg_3006" s="T223">pro.h:Th</ta>
            <ta e="T226" id="Seg_3007" s="T225">np.h:Com 0.2.h:Poss</ta>
            <ta e="T229" id="Seg_3008" s="T228">0.3.h:P</ta>
            <ta e="T230" id="Seg_3009" s="T229">adv:Time</ta>
            <ta e="T232" id="Seg_3010" s="T231">np.h:A</ta>
            <ta e="T233" id="Seg_3011" s="T232">np:So</ta>
            <ta e="T235" id="Seg_3012" s="T234">np:G</ta>
            <ta e="T236" id="Seg_3013" s="T235">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_3014" s="T2">np:S</ta>
            <ta e="T4" id="Seg_3015" s="T3">v:pred</ta>
            <ta e="T6" id="Seg_3016" s="T5">pro.h:S</ta>
            <ta e="T8" id="Seg_3017" s="T7">np.h:O</ta>
            <ta e="T9" id="Seg_3018" s="T8">v:pred</ta>
            <ta e="T12" id="Seg_3019" s="T11">np.h:S</ta>
            <ta e="T13" id="Seg_3020" s="T12">v:pred</ta>
            <ta e="T16" id="Seg_3021" s="T15">np.h:S</ta>
            <ta e="T17" id="Seg_3022" s="T16">v:pred</ta>
            <ta e="T19" id="Seg_3023" s="T18">np.h:S</ta>
            <ta e="T20" id="Seg_3024" s="T19">np.h:O</ta>
            <ta e="T21" id="Seg_3025" s="T20">v:pred</ta>
            <ta e="T23" id="Seg_3026" s="T22">np.h:O</ta>
            <ta e="T25" id="Seg_3027" s="T24">v:pred 0.3.h:S</ta>
            <ta e="T27" id="Seg_3028" s="T26">v:pred</ta>
            <ta e="T29" id="Seg_3029" s="T28">np.h:S</ta>
            <ta e="T30" id="Seg_3030" s="T29">s:purp</ta>
            <ta e="T31" id="Seg_3031" s="T30">v:pred 0.3.h:S</ta>
            <ta e="T32" id="Seg_3032" s="T31">v:pred 0.3.h:S</ta>
            <ta e="T33" id="Seg_3033" s="T32">np.h:S</ta>
            <ta e="T34" id="Seg_3034" s="T33">v:pred</ta>
            <ta e="T35" id="Seg_3035" s="T34">np.h:S</ta>
            <ta e="T37" id="Seg_3036" s="T36">v:pred</ta>
            <ta e="T39" id="Seg_3037" s="T38">np.h:S</ta>
            <ta e="T41" id="Seg_3038" s="T40">v:pred</ta>
            <ta e="T42" id="Seg_3039" s="T41">np.h:O</ta>
            <ta e="T43" id="Seg_3040" s="T42">v:pred 0.3.h:S</ta>
            <ta e="T44" id="Seg_3041" s="T43">v:pred 0.3.h:S</ta>
            <ta e="T48" id="Seg_3042" s="T47">v:pred 0.3.h:S</ta>
            <ta e="T50" id="Seg_3043" s="T49">n:pred</ta>
            <ta e="T52" id="Seg_3044" s="T51">np.h:S</ta>
            <ta e="T55" id="Seg_3045" s="T54">np.h:S</ta>
            <ta e="T57" id="Seg_3046" s="T56">v:pred</ta>
            <ta e="T58" id="Seg_3047" s="T57">np.h:S</ta>
            <ta e="T59" id="Seg_3048" s="T58">s:purp</ta>
            <ta e="T60" id="Seg_3049" s="T59">v:pred</ta>
            <ta e="T61" id="Seg_3050" s="T60">np:O</ta>
            <ta e="T62" id="Seg_3051" s="T61">v:pred 0.3.h:S</ta>
            <ta e="T63" id="Seg_3052" s="T62">s:purp</ta>
            <ta e="T64" id="Seg_3053" s="T63">v:pred 0.3.h:S</ta>
            <ta e="T65" id="Seg_3054" s="T64">np:O</ta>
            <ta e="T66" id="Seg_3055" s="T65">v:pred 0.3.h:S</ta>
            <ta e="T68" id="Seg_3056" s="T67">np.h:S</ta>
            <ta e="T69" id="Seg_3057" s="T68">np:O</ta>
            <ta e="T70" id="Seg_3058" s="T69">np:O</ta>
            <ta e="T71" id="Seg_3059" s="T70">v:pred</ta>
            <ta e="T73" id="Seg_3060" s="T72">np.h:S</ta>
            <ta e="T75" id="Seg_3061" s="T74">np.h:S</ta>
            <ta e="T76" id="Seg_3062" s="T75">np.h:O</ta>
            <ta e="T77" id="Seg_3063" s="T76">v:pred</ta>
            <ta e="T80" id="Seg_3064" s="T79">np:S</ta>
            <ta e="T81" id="Seg_3065" s="T80">n:pred</ta>
            <ta e="T82" id="Seg_3066" s="T81">v:pred 0.3.h:S</ta>
            <ta e="T83" id="Seg_3067" s="T82">pro.h:S</ta>
            <ta e="T86" id="Seg_3068" s="T85">n:pred</ta>
            <ta e="T87" id="Seg_3069" s="T86">cop</ta>
            <ta e="T90" id="Seg_3070" s="T89">n:pred</ta>
            <ta e="T91" id="Seg_3071" s="T90">0.2.h:S cop</ta>
            <ta e="T93" id="Seg_3072" s="T92">np.h:S</ta>
            <ta e="T94" id="Seg_3073" s="T93">v:pred</ta>
            <ta e="T95" id="Seg_3074" s="T94">pro.h:S</ta>
            <ta e="T96" id="Seg_3075" s="T95">v:pred</ta>
            <ta e="T98" id="Seg_3076" s="T97">v:pred 0.1.h:S</ta>
            <ta e="T100" id="Seg_3077" s="T99">s:adv</ta>
            <ta e="T102" id="Seg_3078" s="T101">v:pred 0.3.h:S</ta>
            <ta e="T109" id="Seg_3079" s="T108">v:pred 0.3.h:S</ta>
            <ta e="T111" id="Seg_3080" s="T110">np:O</ta>
            <ta e="T112" id="Seg_3081" s="T111">v:pred 0.2.h:S</ta>
            <ta e="T115" id="Seg_3082" s="T114">np:O</ta>
            <ta e="T118" id="Seg_3083" s="T117">np:O</ta>
            <ta e="T119" id="Seg_3084" s="T118">v:pred 0.2.h:S</ta>
            <ta e="T120" id="Seg_3085" s="T119">v:pred 0.2.h:S</ta>
            <ta e="T121" id="Seg_3086" s="T120">np:O</ta>
            <ta e="T122" id="Seg_3087" s="T121">v:pred 0.3.h:S</ta>
            <ta e="T123" id="Seg_3088" s="T122">np:O</ta>
            <ta e="T124" id="Seg_3089" s="T123">v:pred 0.3.h:S</ta>
            <ta e="T125" id="Seg_3090" s="T124">np:S</ta>
            <ta e="T126" id="Seg_3091" s="T125">v:pred</ta>
            <ta e="T127" id="Seg_3092" s="T126">np:O</ta>
            <ta e="T128" id="Seg_3093" s="T127">conv:pred</ta>
            <ta e="T130" id="Seg_3094" s="T129">v:pred 0.2.h:S 0.3:O</ta>
            <ta e="T131" id="Seg_3095" s="T130">np:O</ta>
            <ta e="T133" id="Seg_3096" s="T132">v:pred 0.3.h:S</ta>
            <ta e="T135" id="Seg_3097" s="T134">np:S</ta>
            <ta e="T136" id="Seg_3098" s="T135">v:pred</ta>
            <ta e="T137" id="Seg_3099" s="T136">v:pred 0.3.h:S</ta>
            <ta e="T138" id="Seg_3100" s="T137">np:O</ta>
            <ta e="T139" id="Seg_3101" s="T138">v:pred 0.3.h:S</ta>
            <ta e="T141" id="Seg_3102" s="T140">np:S</ta>
            <ta e="T143" id="Seg_3103" s="T142">v:pred</ta>
            <ta e="T145" id="Seg_3104" s="T144">v:pred 0.3.h:S</ta>
            <ta e="T147" id="Seg_3105" s="T146">np:S</ta>
            <ta e="T149" id="Seg_3106" s="T148">v:pred</ta>
            <ta e="T153" id="Seg_3107" s="T149">s:temp</ta>
            <ta e="T154" id="Seg_3108" s="T153">v:pred</ta>
            <ta e="T156" id="Seg_3109" s="T155">np.h:S</ta>
            <ta e="T157" id="Seg_3110" s="T156">np.h:S</ta>
            <ta e="T158" id="Seg_3111" s="T157">np:O</ta>
            <ta e="T159" id="Seg_3112" s="T158">np:O</ta>
            <ta e="T160" id="Seg_3113" s="T159">v:pred</ta>
            <ta e="T163" id="Seg_3114" s="T162">v:pred 0.3.h:S</ta>
            <ta e="T164" id="Seg_3115" s="T163">np.h:O</ta>
            <ta e="T165" id="Seg_3116" s="T164">s:purp</ta>
            <ta e="T166" id="Seg_3117" s="T165">v:pred 0.3.h:S</ta>
            <ta e="T169" id="Seg_3118" s="T168">v:pred 0.3.h:S</ta>
            <ta e="T173" id="Seg_3119" s="T172">v:pred 0.3.h:S</ta>
            <ta e="T174" id="Seg_3120" s="T173">np:S</ta>
            <ta e="T175" id="Seg_3121" s="T174">v:pred</ta>
            <ta e="T178" id="Seg_3122" s="T177">np:S</ta>
            <ta e="T179" id="Seg_3123" s="T178">v:pred</ta>
            <ta e="T180" id="Seg_3124" s="T179">np:S</ta>
            <ta e="T182" id="Seg_3125" s="T181">v:pred</ta>
            <ta e="T186" id="Seg_3126" s="T185">v:pred 0.3.h:S</ta>
            <ta e="T190" id="Seg_3127" s="T189">v:pred 0.3.h:S</ta>
            <ta e="T191" id="Seg_3128" s="T190">np.h:S</ta>
            <ta e="T192" id="Seg_3129" s="T191">v:pred</ta>
            <ta e="T195" id="Seg_3130" s="T194">np.h:S</ta>
            <ta e="T196" id="Seg_3131" s="T195">v:pred</ta>
            <ta e="T197" id="Seg_3132" s="T196">pro.h:S</ta>
            <ta e="T200" id="Seg_3133" s="T199">np:O</ta>
            <ta e="T201" id="Seg_3134" s="T200">v:pred</ta>
            <ta e="T202" id="Seg_3135" s="T201">np.h:S</ta>
            <ta e="T203" id="Seg_3136" s="T202">v:pred</ta>
            <ta e="T205" id="Seg_3137" s="T204">np:S</ta>
            <ta e="T206" id="Seg_3138" s="T205">v:pred</ta>
            <ta e="T208" id="Seg_3139" s="T207">np.h:S</ta>
            <ta e="T209" id="Seg_3140" s="T208">np:O</ta>
            <ta e="T210" id="Seg_3141" s="T209">v:pred</ta>
            <ta e="T211" id="Seg_3142" s="T210">np.h:O</ta>
            <ta e="T213" id="Seg_3143" s="T212">v:pred</ta>
            <ta e="T214" id="Seg_3144" s="T213">v:pred 0.2.h:S</ta>
            <ta e="T217" id="Seg_3145" s="T216">np.h:S</ta>
            <ta e="T218" id="Seg_3146" s="T217">n:O</ta>
            <ta e="T219" id="Seg_3147" s="T218">np:O</ta>
            <ta e="T220" id="Seg_3148" s="T219">v:pred</ta>
            <ta e="T221" id="Seg_3149" s="T220">pro.h:S</ta>
            <ta e="T222" id="Seg_3150" s="T221">np:O</ta>
            <ta e="T223" id="Seg_3151" s="T222">v:pred</ta>
            <ta e="T224" id="Seg_3152" s="T223">pro.h:S</ta>
            <ta e="T227" id="Seg_3153" s="T226">adj:pred</ta>
            <ta e="T229" id="Seg_3154" s="T228">v:pred 0.3.h:S</ta>
            <ta e="T232" id="Seg_3155" s="T231">np.h:S</ta>
            <ta e="T234" id="Seg_3156" s="T233">v:pred</ta>
            <ta e="T236" id="Seg_3157" s="T235">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T3" id="Seg_3158" s="T2">new </ta>
            <ta e="T5" id="Seg_3159" s="T4">accs-gen </ta>
            <ta e="T6" id="Seg_3160" s="T5">giv-active </ta>
            <ta e="T8" id="Seg_3161" s="T7">new </ta>
            <ta e="T12" id="Seg_3162" s="T11">accs-sit </ta>
            <ta e="T16" id="Seg_3163" s="T15">accs-sit </ta>
            <ta e="T19" id="Seg_3164" s="T18">giv-inactive </ta>
            <ta e="T20" id="Seg_3165" s="T19">giv-active </ta>
            <ta e="T23" id="Seg_3166" s="T22">giv-active</ta>
            <ta e="T24" id="Seg_3167" s="T23">new </ta>
            <ta e="T25" id="Seg_3168" s="T24">0.giv-active</ta>
            <ta e="T29" id="Seg_3169" s="T28">giv-inactive </ta>
            <ta e="T31" id="Seg_3170" s="T30">0.giv-active</ta>
            <ta e="T32" id="Seg_3171" s="T31">0.giv-active</ta>
            <ta e="T33" id="Seg_3172" s="T32">giv-inactive </ta>
            <ta e="T35" id="Seg_3173" s="T34">giv-inactive </ta>
            <ta e="T36" id="Seg_3174" s="T35">giv-inactive</ta>
            <ta e="T39" id="Seg_3175" s="T38">giv-active </ta>
            <ta e="T42" id="Seg_3176" s="T41">giv-active </ta>
            <ta e="T43" id="Seg_3177" s="T42">0.giv-active</ta>
            <ta e="T44" id="Seg_3178" s="T43">0.giv-active</ta>
            <ta e="T46" id="Seg_3179" s="T45">new </ta>
            <ta e="T48" id="Seg_3180" s="T47">0.giv-active</ta>
            <ta e="T50" id="Seg_3181" s="T48">new</ta>
            <ta e="T52" id="Seg_3182" s="T51">giv-active</ta>
            <ta e="T53" id="Seg_3183" s="T52">giv-active </ta>
            <ta e="T55" id="Seg_3184" s="T54">giv-inactive </ta>
            <ta e="T58" id="Seg_3185" s="T57">giv-active </ta>
            <ta e="T61" id="Seg_3186" s="T60">new </ta>
            <ta e="T62" id="Seg_3187" s="T61">0.giv-active</ta>
            <ta e="T64" id="Seg_3188" s="T63">0.giv-active</ta>
            <ta e="T65" id="Seg_3189" s="T64">giv-active </ta>
            <ta e="T66" id="Seg_3190" s="T65">0.giv-active</ta>
            <ta e="T68" id="Seg_3191" s="T67">giv-inactive </ta>
            <ta e="T69" id="Seg_3192" s="T68">new </ta>
            <ta e="T70" id="Seg_3193" s="T69">new </ta>
            <ta e="T73" id="Seg_3194" s="T72">giv-inactive </ta>
            <ta e="T75" id="Seg_3195" s="T74">giv-active </ta>
            <ta e="T76" id="Seg_3196" s="T75">giv-inactive </ta>
            <ta e="T77" id="Seg_3197" s="T76">quot-sp</ta>
            <ta e="T78" id="Seg_3198" s="T77">giv-active-Q </ta>
            <ta e="T80" id="Seg_3199" s="T79">accs-inf-Q </ta>
            <ta e="T82" id="Seg_3200" s="T81">0.giv-active quot-sp</ta>
            <ta e="T83" id="Seg_3201" s="T82">giv-inactive-Q </ta>
            <ta e="T86" id="Seg_3202" s="T85">accs-gen-Q </ta>
            <ta e="T90" id="Seg_3203" s="T89">giv-inactive-Q </ta>
            <ta e="T91" id="Seg_3204" s="T90">0.giv-active-Q</ta>
            <ta e="T93" id="Seg_3205" s="T92">giv-inactive-Q </ta>
            <ta e="T95" id="Seg_3206" s="T94">giv-active-Q </ta>
            <ta e="T97" id="Seg_3207" s="T96">giv-inactive-Q </ta>
            <ta e="T98" id="Seg_3208" s="T97">0.giv-active-Q</ta>
            <ta e="T102" id="Seg_3209" s="T101">0.giv-inactive-Q</ta>
            <ta e="T104" id="Seg_3210" s="T103">giv-inactive-Q </ta>
            <ta e="T107" id="Seg_3211" s="T106">giv-active-Q </ta>
            <ta e="T109" id="Seg_3212" s="T108">0.giv-active-Q</ta>
            <ta e="T111" id="Seg_3213" s="T110">new-Q </ta>
            <ta e="T112" id="Seg_3214" s="T111">0.giv-inactive-Q</ta>
            <ta e="T115" id="Seg_3215" s="T114">new-Q </ta>
            <ta e="T118" id="Seg_3216" s="T117">new-Q </ta>
            <ta e="T119" id="Seg_3217" s="T118">0.giv-active-Q</ta>
            <ta e="T120" id="Seg_3218" s="T119">0.giv-active-Q</ta>
            <ta e="T121" id="Seg_3219" s="T120">giv-inactive </ta>
            <ta e="T122" id="Seg_3220" s="T121">0.giv-active</ta>
            <ta e="T123" id="Seg_3221" s="T122">new </ta>
            <ta e="T125" id="Seg_3222" s="T124">giv-active </ta>
            <ta e="T127" id="Seg_3223" s="T126">new </ta>
            <ta e="T129" id="Seg_3224" s="T128">giv-inactive-Q </ta>
            <ta e="T130" id="Seg_3225" s="T129">0.giv-inactive-Q</ta>
            <ta e="T131" id="Seg_3226" s="T130">giv-inactive </ta>
            <ta e="T132" id="Seg_3227" s="T131">giv-active </ta>
            <ta e="T133" id="Seg_3228" s="T132">0.giv-active</ta>
            <ta e="T135" id="Seg_3229" s="T134">giv-active </ta>
            <ta e="T137" id="Seg_3230" s="T136">0.giv-active</ta>
            <ta e="T138" id="Seg_3231" s="T137">giv-active </ta>
            <ta e="T139" id="Seg_3232" s="T138">0.giv-active</ta>
            <ta e="T141" id="Seg_3233" s="T140">accs-inf </ta>
            <ta e="T145" id="Seg_3234" s="T144">0.giv-active</ta>
            <ta e="T147" id="Seg_3235" s="T146">accs-inf </ta>
            <ta e="T151" id="Seg_3236" s="T150">giv-active </ta>
            <ta e="T156" id="Seg_3237" s="T155">giv-active </ta>
            <ta e="T157" id="Seg_3238" s="T156">giv-inactive </ta>
            <ta e="T158" id="Seg_3239" s="T157">giv-inactive </ta>
            <ta e="T159" id="Seg_3240" s="T158">giv-inactive </ta>
            <ta e="T163" id="Seg_3241" s="T162">0.giv-active</ta>
            <ta e="T164" id="Seg_3242" s="T163">giv-inactive </ta>
            <ta e="T166" id="Seg_3243" s="T165">0.giv-active</ta>
            <ta e="T168" id="Seg_3244" s="T167">giv-inactive </ta>
            <ta e="T169" id="Seg_3245" s="T168">0.giv-active</ta>
            <ta e="T172" id="Seg_3246" s="T171">giv-active </ta>
            <ta e="T173" id="Seg_3247" s="T172">0.giv-active</ta>
            <ta e="T174" id="Seg_3248" s="T173">new </ta>
            <ta e="T177" id="Seg_3249" s="T176">giv-active </ta>
            <ta e="T178" id="Seg_3250" s="T177">new </ta>
            <ta e="T180" id="Seg_3251" s="T179">accs-sit </ta>
            <ta e="T181" id="Seg_3252" s="T180">accs-inf </ta>
            <ta e="T185" id="Seg_3253" s="T184">accs-inf </ta>
            <ta e="T186" id="Seg_3254" s="T185">0.giv-inactive</ta>
            <ta e="T188" id="Seg_3255" s="T187">giv-inactive </ta>
            <ta e="T190" id="Seg_3256" s="T189">0.giv-active</ta>
            <ta e="T191" id="Seg_3257" s="T190">giv-inactive </ta>
            <ta e="T195" id="Seg_3258" s="T194">giv-inactive </ta>
            <ta e="T197" id="Seg_3259" s="T196">giv-inactive </ta>
            <ta e="T198" id="Seg_3260" s="T197">giv-inactive </ta>
            <ta e="T199" id="Seg_3261" s="T198">new</ta>
            <ta e="T200" id="Seg_3262" s="T199">giv-inactive </ta>
            <ta e="T202" id="Seg_3263" s="T201">giv-inactive </ta>
            <ta e="T203" id="Seg_3264" s="T202">quot-sp</ta>
            <ta e="T204" id="Seg_3265" s="T203">giv-inactive-Q </ta>
            <ta e="T205" id="Seg_3266" s="T204">giv-inactive-Q </ta>
            <ta e="T208" id="Seg_3267" s="T207">giv-inactive </ta>
            <ta e="T209" id="Seg_3268" s="T208">new </ta>
            <ta e="T211" id="Seg_3269" s="T210">giv-active </ta>
            <ta e="T212" id="Seg_3270" s="T211">accs-inf </ta>
            <ta e="T213" id="Seg_3271" s="T212">0.giv-active</ta>
            <ta e="T214" id="Seg_3272" s="T213">0.giv-active-Q</ta>
            <ta e="T217" id="Seg_3273" s="T216">giv-inactive-Q </ta>
            <ta e="T218" id="Seg_3274" s="T217">accs-gen-Q </ta>
            <ta e="T219" id="Seg_3275" s="T218">accs-gen-Q </ta>
            <ta e="T221" id="Seg_3276" s="T220">giv-inactive-Q </ta>
            <ta e="T222" id="Seg_3277" s="T221">new-Q </ta>
            <ta e="T224" id="Seg_3278" s="T223">giv-inactive-Q </ta>
            <ta e="T226" id="Seg_3279" s="T225">giv-inactive-Q </ta>
            <ta e="T229" id="Seg_3280" s="T228">0.giv-inactive</ta>
            <ta e="T232" id="Seg_3281" s="T231">giv-inactive </ta>
            <ta e="T233" id="Seg_3282" s="T232">giv-inactive </ta>
            <ta e="T235" id="Seg_3283" s="T234">giv-inactive </ta>
            <ta e="T236" id="Seg_3284" s="T235">0.giv-active</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T45" id="Seg_3285" s="T44">TURK:core</ta>
            <ta e="T88" id="Seg_3286" s="T87">TURK:core</ta>
            <ta e="T103" id="Seg_3287" s="T102">TURK:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_3288" s="T0">Жили две старухи в тайге.</ta>
            <ta e="T9" id="Seg_3289" s="T5">Они родили каждая по ребёнку. </ta>
            <ta e="T17" id="Seg_3290" s="T9">У одной была девочка, у другой мальчик.</ta>
            <ta e="T25" id="Seg_3291" s="T17">Одна женщина украла у другой мальчика, а в колыбель свою дочь положила.</ta>
            <ta e="T32" id="Seg_3292" s="T25">Потом пришла та женщина; она за дровами ходила, вернулась.</ta>
            <ta e="T37" id="Seg_3293" s="T32">Её сына нет, в его колыбели девочка лежит.</ta>
            <ta e="T48" id="Seg_3294" s="T37">Эта женщина разозлилсь, взяла ребенка, разозлилась и к другой горе пошла.</ta>
            <ta e="T57" id="Seg_3295" s="T48">[Другая] женщина была лягушкой, мальчик остался у лягушки жить.</ta>
            <ta e="T66" id="Seg_3296" s="T57">Мальчик в лесу охотиться научился, коз стрелял, охотиться ходил, коз стрелял.</ta>
            <ta e="T71" id="Seg_3297" s="T66">Та женщина мясо и сало ела.</ta>
            <ta e="T77" id="Seg_3298" s="T71">Мальчик тот, лягушачий сын, спросил мать:</ta>
            <ta e="T81" id="Seg_3299" s="T77">"Почему у тебя три пальца?"</ta>
            <ta e="T82" id="Seg_3300" s="T81">Она отвечает:</ta>
            <ta e="T91" id="Seg_3301" s="T82">"Ты не мой сын, ты другой женщины сын.</ta>
            <ta e="T94" id="Seg_3302" s="T91">У меня дочь была.</ta>
            <ta e="T98" id="Seg_3303" s="T94">Я тебя украла, [её] в колыбель положила.</ta>
            <ta e="T104" id="Seg_3304" s="T98">Та [женщина] разозлилась, к другой горе пошла.</ta>
            <ta e="T109" id="Seg_3305" s="T104">На той черной горе она живет."</ta>
            <ta e="T113" id="Seg_3306" s="T109">"Сегодня не руби дров!</ta>
            <ta e="T119" id="Seg_3307" s="T113">Сегодня кору, собери кору лиственницы!</ta>
            <ta e="T120" id="Seg_3308" s="T119">Ночь проведёшь [без сна]."</ta>
            <ta e="T124" id="Seg_3309" s="T120">Набрала коры, огонь развела.</ta>
            <ta e="T126" id="Seg_3310" s="T124">Огонь её потух.</ta>
            <ta e="T128" id="Seg_3311" s="T126">Мясо поедая:</ta>
            <ta e="T130" id="Seg_3312" s="T128">"Брось кору [в огонь]!"</ta>
            <ta e="T133" id="Seg_3313" s="T130">Положила кору в огонь.</ta>
            <ta e="T136" id="Seg_3314" s="T133">Потом огонь потух.</ta>
            <ta e="T139" id="Seg_3315" s="T136">Пошла раздуть огонь.</ta>
            <ta e="T143" id="Seg_3316" s="T139">Тут у неё глаз лопнул.</ta>
            <ta e="T149" id="Seg_3317" s="T143">Потом подула, другой глаз лопнул.</ta>
            <ta e="T156" id="Seg_3318" s="T149">Как второй глаз лопнул, умерла та женщина.</ta>
            <ta e="T160" id="Seg_3319" s="T156">Мальчик мясо и сало взял.</ta>
            <ta e="T163" id="Seg_3320" s="T160">И пошел.</ta>
            <ta e="T169" id="Seg_3321" s="T163">Отправился мать свою искать, к большой горе пошёл.</ta>
            <ta e="T175" id="Seg_3322" s="T169">На большой чёрной горе увидел: чум стоит.</ta>
            <ta e="T182" id="Seg_3323" s="T175">В том чуме огонь горит, дым из дымохода поднимается.</ta>
            <ta e="T186" id="Seg_3324" s="T182">Забрался на чум сверху.</ta>
            <ta e="T190" id="Seg_3325" s="T186">Глядит оттуда вниз через дымоход.</ta>
            <ta e="T196" id="Seg_3326" s="T190">Сидит его мать, дочь женщины-лягушки сидит.</ta>
            <ta e="T201" id="Seg_3327" s="T196">Сбросил сало в котел через дымоход.</ta>
            <ta e="T203" id="Seg_3328" s="T201">Девочка говорит:</ta>
            <ta e="T206" id="Seg_3329" s="T203">"В котел наш сало падает."</ta>
            <ta e="T213" id="Seg_3330" s="T206">Старуха палку взяла, по голове девочку ударила:</ta>
            <ta e="T215" id="Seg_3331" s="T213">"Не ври!</ta>
            <ta e="T220" id="Seg_3332" s="T215">Твоя мать сало да мясо ест.</ta>
            <ta e="T223" id="Seg_3333" s="T220">А я луковицы лилий ем.</ta>
            <ta e="T227" id="Seg_3334" s="T223">Ты такая же хитрая как твоя мать блудница."</ta>
            <ta e="T229" id="Seg_3335" s="T227">[Девочка] умерла.</ta>
            <ta e="T236" id="Seg_3336" s="T229">Тогда мальчик через дымоход спустился и в чум вошёл.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_3337" s="T0">Two old women lived in the taiga.</ta>
            <ta e="T9" id="Seg_3338" s="T5">They each gave birth to a child.</ta>
            <ta e="T17" id="Seg_3339" s="T9">One woman had a girl, one woman had a boy.</ta>
            <ta e="T25" id="Seg_3340" s="T17">One woman stole the boy and left her own daughter in his cradle.</ta>
            <ta e="T32" id="Seg_3341" s="T25">Then came this woman; she had been going for firewood, came back.</ta>
            <ta e="T37" id="Seg_3342" s="T32">Her son is not there, a girl lies in his cradle.</ta>
            <ta e="T48" id="Seg_3343" s="T37">This woman becoming angry took the child, was angry and went away to another mountain.</ta>
            <ta e="T57" id="Seg_3344" s="T48">This [other] woman was a frog, and this boy stayed with the frog.</ta>
            <ta e="T66" id="Seg_3345" s="T57">The boy learned hunting, he went shooting goats, used to go hunting and shoot goats.</ta>
            <ta e="T71" id="Seg_3346" s="T66">This woman used to eat meat and fat.</ta>
            <ta e="T77" id="Seg_3347" s="T71">This boy, the frog’s boy, asked his mother:</ta>
            <ta e="T81" id="Seg_3348" s="T77">"Why do you have three fingers?"</ta>
            <ta e="T82" id="Seg_3349" s="T81">She says:</ta>
            <ta e="T91" id="Seg_3350" s="T82">"You are not my son, you were the son of another woman.</ta>
            <ta e="T94" id="Seg_3351" s="T91">I had a daughter.</ta>
            <ta e="T98" id="Seg_3352" s="T94">I stole [you], put [her] in the cradle.</ta>
            <ta e="T104" id="Seg_3353" s="T98">Then [that woman] got angry and went away to another mountain.</ta>
            <ta e="T109" id="Seg_3354" s="T104">On that black mountain, there she lives."</ta>
            <ta e="T113" id="Seg_3355" s="T109">"Today, don’t fetch wood!</ta>
            <ta e="T119" id="Seg_3356" s="T113">Today the bark, collect the bark of larch tree!</ta>
            <ta e="T120" id="Seg_3357" s="T119">You will spend the night."</ta>
            <ta e="T124" id="Seg_3358" s="T120">She collected bark, made a fire.</ta>
            <ta e="T126" id="Seg_3359" s="T124">Her fire went out.</ta>
            <ta e="T128" id="Seg_3360" s="T126">Eating meat:</ta>
            <ta e="T130" id="Seg_3361" s="T128">‎‎"Put bark [into the fire]!"</ta>
            <ta e="T133" id="Seg_3362" s="T130">She made the fire with her bark.</ta>
            <ta e="T136" id="Seg_3363" s="T133">Then her fire is going out.</ta>
            <ta e="T139" id="Seg_3364" s="T136">She went and blew her fire.</ta>
            <ta e="T143" id="Seg_3365" s="T139">Then her eye burst.</ta>
            <ta e="T149" id="Seg_3366" s="T143">Then she blew and her other eye burst.</ta>
            <ta e="T156" id="Seg_3367" s="T149">When her other eye burst, this woman died.</ta>
            <ta e="T160" id="Seg_3368" s="T156">The boy took meat and fat.</ta>
            <ta e="T163" id="Seg_3369" s="T160">Then he went off.</ta>
            <ta e="T169" id="Seg_3370" s="T163">He went searching his mother, went to the big mountain.</ta>
            <ta e="T175" id="Seg_3371" s="T169">On the big black mountain he saw: A tent is standing.</ta>
            <ta e="T182" id="Seg_3372" s="T175">In this tent a fire is burning, smoke is rising from the smoke hole.</ta>
            <ta e="T186" id="Seg_3373" s="T182">Then he climbed up on the tent top.</ta>
            <ta e="T190" id="Seg_3374" s="T186">From there he looked down through the smokehole.</ta>
            <ta e="T196" id="Seg_3375" s="T190">His mother and the frog woman’s daughter are sitting there.</ta>
            <ta e="T201" id="Seg_3376" s="T196">Through the smokehole, he crumbled fat into the cauldron.</ta>
            <ta e="T203" id="Seg_3377" s="T201">The girl says:</ta>
            <ta e="T206" id="Seg_3378" s="T203">"Fat is going in our cauldron."</ta>
            <ta e="T213" id="Seg_3379" s="T206">The old woman took a piece of wood and hit her daughter on her head.</ta>
            <ta e="T215" id="Seg_3380" s="T213">"Don’t lie!</ta>
            <ta e="T220" id="Seg_3381" s="T215">Your mother is eating fat and meat.</ta>
            <ta e="T223" id="Seg_3382" s="T220">I eat lily bulbs.</ta>
            <ta e="T227" id="Seg_3383" s="T223">You’re as sneaky as your whore mother."</ta>
            <ta e="T229" id="Seg_3384" s="T227">[The girl] died.</ta>
            <ta e="T236" id="Seg_3385" s="T229">Then the boy descended from the smoke hole and came into the tent.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_3386" s="T0">Zwei alte Frauen lebten in der Taiga.</ta>
            <ta e="T9" id="Seg_3387" s="T5">Beide brachten ein Kind zur Welt.</ta>
            <ta e="T17" id="Seg_3388" s="T9">Die eine Frau hatte eine Tochter, die andere Frau hatte einen Sohn.</ta>
            <ta e="T25" id="Seg_3389" s="T17">Die eine Frau stahl den Sohn [der anderen Frau] und legte [stattdessen] ihre eigene Tochter in die Wiege.</ta>
            <ta e="T32" id="Seg_3390" s="T25">Dann kam die andere Frau vom Holzholen zurück.</ta>
            <ta e="T37" id="Seg_3391" s="T32">Ihr Sohn ist nicht da, ein Mädchen liegt in seiner Wiege.</ta>
            <ta e="T48" id="Seg_3392" s="T37">Diese Frau brach in Wut aus, nahm das Kind, war wütend und zog auf einen anderen Berg.</ta>
            <ta e="T57" id="Seg_3393" s="T48">Die [andere] Frau war ein Frosch, und der Junge blieb bei dem Frosch.</ta>
            <ta e="T66" id="Seg_3394" s="T57">Ihr Sohn lernte jagen und Ziegen schießen, [dann] ging er regelmäßig jagen und Ziegen schießen.</ta>
            <ta e="T71" id="Seg_3395" s="T66">Die Frau aß immer Fleisch und Fett.</ta>
            <ta e="T77" id="Seg_3396" s="T71">Der Junge, der Sohn des Froschs, fragte seine Mutter:</ta>
            <ta e="T81" id="Seg_3397" s="T77">"Warum hast du drei Finger?"</ta>
            <ta e="T82" id="Seg_3398" s="T81">Sie antwortet:</ta>
            <ta e="T91" id="Seg_3399" s="T82">"Du bist nicht mein Sohn, du bist der Sohn einer anderen Frau.</ta>
            <ta e="T94" id="Seg_3400" s="T91">Ich hatte eine Tochter.</ta>
            <ta e="T98" id="Seg_3401" s="T94">Ich stahl dich und legte [meine Tochter] in die Wiege.</ta>
            <ta e="T104" id="Seg_3402" s="T98">Dann zog [die Mutter] wütend auf einen anderen Berg.</ta>
            <ta e="T109" id="Seg_3403" s="T104">Auf diesem schwarzen Berg, da wohnt sie."</ta>
            <ta e="T113" id="Seg_3404" s="T109">"Hol heute kein Holz.</ta>
            <ta e="T119" id="Seg_3405" s="T113">Heute Rinde, sammel die Rinde des Lärchenbaums!</ta>
            <ta e="T120" id="Seg_3406" s="T119">Du wirst die Nacht durchwachen."</ta>
            <ta e="T124" id="Seg_3407" s="T120">Sie sammelt Rinde und machte ein Feuer.</ta>
            <ta e="T126" id="Seg_3408" s="T124">Ihr Feuer ging aus.</ta>
            <ta e="T128" id="Seg_3409" s="T126">Sie/Er isst Fleisch.</ta>
            <ta e="T130" id="Seg_3410" s="T128">„Mach mit der Rinde [Feuer]!“</ta>
            <ta e="T133" id="Seg_3411" s="T130">Sie machte Feuer mit der Rinde.</ta>
            <ta e="T136" id="Seg_3412" s="T133">Ihr Feuer ging ständig aus.</ta>
            <ta e="T139" id="Seg_3413" s="T136">Sie ging das Feuer anblasen.</ta>
            <ta e="T143" id="Seg_3414" s="T139">Dann platzte ihr Auge.</ta>
            <ta e="T149" id="Seg_3415" s="T143">Dann blies sie und ihr anderes Auge platzte.</ta>
            <ta e="T156" id="Seg_3416" s="T149">Als ihr anderes Auge platzte, starb die Frau.</ta>
            <ta e="T160" id="Seg_3417" s="T156">Der Junge nahm Fleisch und Fett.</ta>
            <ta e="T163" id="Seg_3418" s="T160">Dann ging er fort.</ta>
            <ta e="T169" id="Seg_3419" s="T163">Er ging nach seiner Mutter suchen und stieg auf einen großen Berg.</ta>
            <ta e="T175" id="Seg_3420" s="T169">Auf dem großen schwarzen Berg sah er: Da steht ein Zelt.</ta>
            <ta e="T182" id="Seg_3421" s="T175">In diesem Zelt brennt ein Feuer, Rauch steigt aus der Rauchöffnung auf.</ta>
            <ta e="T186" id="Seg_3422" s="T182">Dann kletterte er auf das Zelt hinauf.</ta>
            <ta e="T190" id="Seg_3423" s="T186">Durch die Rauchöffnung schaute er hinab.</ta>
            <ta e="T196" id="Seg_3424" s="T190">Seine Mutter sitzt da, die Tochter der Froschfrau sitzt da.</ta>
            <ta e="T201" id="Seg_3425" s="T196">Durch die Rauchöffnung bröselt er Fett in den Kessel.</ta>
            <ta e="T203" id="Seg_3426" s="T201">Das Mädchen sagt:</ta>
            <ta e="T206" id="Seg_3427" s="T203">„Fett fällt in unseren Kessel."</ta>
            <ta e="T213" id="Seg_3428" s="T206">Das alte Weib nahm [ein Stück] Holz und schlug dem Mädchen auf den Kopf:</ta>
            <ta e="T215" id="Seg_3429" s="T213">"Lüge nicht!</ta>
            <ta e="T220" id="Seg_3430" s="T215">Deine Mutter isst Fett und Fleisch.</ta>
            <ta e="T223" id="Seg_3431" s="T220">Ich esse Lilienzwiebeln.</ta>
            <ta e="T227" id="Seg_3432" s="T223">Du bist so hinterlistig wie deine Hure von Mutter."</ta>
            <ta e="T229" id="Seg_3433" s="T227">[Das Mädchen] starb.</ta>
            <ta e="T236" id="Seg_3434" s="T229">Danach stieg der Knabe durch die Rauchöffnung herab in das Zelt.</ta>
         </annotation>
         <annotation name="ltg" tierref="ltg">
            <ta e="T5" id="Seg_3435" s="T0">Zwei alte Weiber lebten im Walde,</ta>
            <ta e="T9" id="Seg_3436" s="T5">sie gebaren zu gleicher Zeit,</ta>
            <ta e="T17" id="Seg_3437" s="T9">das eine Weib hatte eine Tochter, das andere Weib hatte einen Sohn.</ta>
            <ta e="T25" id="Seg_3438" s="T17">Das eine Weib aber stahl den Knaben [des anderen Weibes], ihre eigene Tochter tat sie in die Wiege [des Knaben].</ta>
            <ta e="T32" id="Seg_3439" s="T25">Darauf kam jenes Weib, ging Holz hauen, kam,</ta>
            <ta e="T37" id="Seg_3440" s="T32">ihr Sohn ist nicht da, das Mädchen liegt in seiner Wiege.</ta>
            <ta e="T48" id="Seg_3441" s="T37">Jenes Weib erzürnte, das Kind aber nahm sie, erzürnte, auf einen anderen Berg ging sie.</ta>
            <ta e="T57" id="Seg_3442" s="T48">Den Frosch-Jungen jenes Weib, den Frosch jenes Weib hätschelte.</ta>
            <ta e="T66" id="Seg_3443" s="T57">Ihr Sohn erlernte das Jagen, eine Ziege schoss er, jagen ging er, eine Ziege schoss er.</ta>
            <ta e="T71" id="Seg_3444" s="T66">Jenes Weib Fleisch und Fett ass.</ta>
            <ta e="T77" id="Seg_3445" s="T71">Jener Junge, der Frosch-Junge, aber fragte die Mutter:</ta>
            <ta e="T81" id="Seg_3446" s="T77">»Warum hast du drei Finger?»</ta>
            <ta e="T91" id="Seg_3447" s="T82">»Du bist nicht mein Sohn, du bist der Sohn des anderen Weibes.</ta>
            <ta e="T94" id="Seg_3448" s="T91">Ich hatte eine Tochter,</ta>
            <ta e="T98" id="Seg_3449" s="T94">ich stahl [den Knaben], in die Wiege tat ich [meine Tochter].</ta>
            <ta e="T104" id="Seg_3450" s="T98">Danach erzürnend, ging sie auf einen anderen Berg,</ta>
            <ta e="T109" id="Seg_3451" s="T104">kam auf einen schwarzen Berg, da wohnt sie.</ta>
            <ta e="T113" id="Seg_3452" s="T109">Heute haue nicht Holz;</ta>
            <ta e="T119" id="Seg_3453" s="T113">heute Baumrinde, die Rinde des Baumes, [die Rinde] der Lärche zu sammeln [gehe]!</ta>
            <ta e="T120" id="Seg_3454" s="T119">Da wirst du übernachten.»</ta>
            <ta e="T124" id="Seg_3455" s="T120">Er sammelte Rinde, machte Feuer.</ta>
            <ta e="T126" id="Seg_3456" s="T124">Sein Feuer erlosch.</ta>
            <ta e="T128" id="Seg_3457" s="T126">Er isst Fleisch.</ta>
            <ta e="T130" id="Seg_3458" s="T128">Rinde legt er [ins Feuer],</ta>
            <ta e="T133" id="Seg_3459" s="T130">das Feuer mit der Rinde machte er.</ta>
            <ta e="T136" id="Seg_3460" s="T133">Danach erlischt sein Feuer.</ta>
            <ta e="T139" id="Seg_3461" s="T136">Sie ging, blies ja das Feuer an.</ta>
            <ta e="T143" id="Seg_3462" s="T139">Dann platzte ihr Auge entzwei.</ta>
            <ta e="T149" id="Seg_3463" s="T143">Danach blies sie, ihr anderes Auge platzte,</ta>
            <ta e="T156" id="Seg_3464" s="T149">ihr anderes Auge platzte. Dieses Weib starb.</ta>
            <ta e="T160" id="Seg_3465" s="T156">Der Knabe nahm Fleisch und Fett.</ta>
            <ta e="T163" id="Seg_3466" s="T160">Dann ging er,</ta>
            <ta e="T169" id="Seg_3467" s="T163">die Mutter zu suchen ging er, auf einen grossen Berg ging er.</ta>
            <ta e="T175" id="Seg_3468" s="T169">Auf dem grossen schwarzen Berg sah er: [da] steht ein Zelt.</ta>
            <ta e="T182" id="Seg_3469" s="T175">In diesem Zelt brennt das Feuer, der Rauch steigt aus dem Rauchloch empor.</ta>
            <ta e="T186" id="Seg_3470" s="T182">Dann kletterte er auf das Zelt,</ta>
            <ta e="T190" id="Seg_3471" s="T186">durch jenes Rauchloch hinab schaute er.</ta>
            <ta e="T196" id="Seg_3472" s="T190">Seine Mutter sitzt, die Frosch-Jungen-Tochter sitzt [da].</ta>
            <ta e="T201" id="Seg_3473" s="T196">Er liess durch das Rauchloch Fett in den Kessel hinab.</ta>
            <ta e="T203" id="Seg_3474" s="T201">Das Mädchen sagt:</ta>
            <ta e="T206" id="Seg_3475" s="T203">»In den Kessel gibt der Knabe ja Fett.»</ta>
            <ta e="T213" id="Seg_3476" s="T206">Das alte Weib nahm das Holz, schlug dem Mädchen auf den Kopf:</ta>
            <ta e="T215" id="Seg_3477" s="T213">»Lüge nicht!</ta>
            <ta e="T220" id="Seg_3478" s="T215">Deine Mutter Fett und Fleisch isst;</ta>
            <ta e="T223" id="Seg_3479" s="T220">ich Lilienzwiebel esse.</ta>
            <ta e="T227" id="Seg_3480" s="T223">Du mit seiner Mutter, der Hure, bist schlau.»</ta>
            <ta e="T229" id="Seg_3481" s="T227">Sie starb.</ta>
            <ta e="T236" id="Seg_3482" s="T229">Danach stieg der Knabe durch das Rauchloch nieder, kam in das Zelt.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltg"
                          display-name="ltg"
                          name="ltg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
