<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID1DFC695E-3437-A2A0-12EE-88D38B980B83">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_Gold_flk.wav" />
         <referenced-file url="PKZ_196X_Gold_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_Gold_flk\PKZ_196X_Gold_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">283</ud-information>
            <ud-information attribute-name="# HIAT:w">182</ud-information>
            <ud-information attribute-name="# e">182</ud-information>
            <ud-information attribute-name="# HIAT:u">47</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.012" type="appl" />
         <tli id="T1" time="0.686" type="appl" />
         <tli id="T2" time="1.36" type="appl" />
         <tli id="T3" time="2.033" type="appl" />
         <tli id="T4" time="3.0333333333333337" />
         <tli id="T5" time="3.715" type="appl" />
         <tli id="T6" time="4.476" type="appl" />
         <tli id="T7" time="5.236" type="appl" />
         <tli id="T8" time="5.997" type="appl" />
         <tli id="T9" time="6.609" type="appl" />
         <tli id="T10" time="7.221" type="appl" />
         <tli id="T11" time="7.832" type="appl" />
         <tli id="T12" time="8.444" type="appl" />
         <tli id="T13" time="9.056" type="appl" />
         <tli id="T14" time="10.253333333333334" />
         <tli id="T15" time="11.347" type="appl" />
         <tli id="T16" time="12.428" type="appl" />
         <tli id="T17" time="13.508" type="appl" />
         <tli id="T18" time="14.589" type="appl" />
         <tli id="T19" time="15.503" type="appl" />
         <tli id="T20" time="16.342" type="appl" />
         <tli id="T21" time="17.181" type="appl" />
         <tli id="T22" time="18.019" type="appl" />
         <tli id="T23" time="18.858" type="appl" />
         <tli id="T24" time="19.697" type="appl" />
         <tli id="T25" time="20.536" type="appl" />
         <tli id="T26" time="21.264" type="appl" />
         <tli id="T27" time="21.992" type="appl" />
         <tli id="T28" time="22.721" type="appl" />
         <tli id="T29" time="23.449" type="appl" />
         <tli id="T30" time="24.346666666666664" />
         <tli id="T31" time="25.035" type="appl" />
         <tli id="T32" time="25.786" type="appl" />
         <tli id="T33" time="26.538" type="appl" />
         <tli id="T34" time="27.289" type="appl" />
         <tli id="T35" time="28.04" type="appl" />
         <tli id="T36" time="28.711" type="appl" />
         <tli id="T37" time="29.381" type="appl" />
         <tli id="T38" time="30.052" type="appl" />
         <tli id="T39" time="30.435" type="appl" />
         <tli id="T40" time="30.817" type="appl" />
         <tli id="T41" time="31.2" type="appl" />
         <tli id="T42" time="31.892" type="appl" />
         <tli id="T43" time="32.584" type="appl" />
         <tli id="T44" time="33.275" type="appl" />
         <tli id="T45" time="33.967" type="appl" />
         <tli id="T46" time="34.659" type="appl" />
         <tli id="T47" time="35.294" type="appl" />
         <tli id="T48" time="35.93" type="appl" />
         <tli id="T49" time="36.565" type="appl" />
         <tli id="T50" time="37.2" type="appl" />
         <tli id="T51" time="37.906" type="appl" />
         <tli id="T52" time="38.613" type="appl" />
         <tli id="T53" time="39.72" type="appl" />
         <tli id="T54" time="40.827" type="appl" />
         <tli id="T55" time="41.933" type="appl" />
         <tli id="T56" time="43.04" type="appl" />
         <tli id="T57" time="44.147" type="appl" />
         <tli id="T58" time="45.11" type="appl" />
         <tli id="T59" time="45.768" type="appl" />
         <tli id="T60" time="46.766666666666666" />
         <tli id="T61" time="47.291" type="appl" />
         <tli id="T62" time="47.748" type="appl" />
         <tli id="T63" time="48.204" type="appl" />
         <tli id="T64" time="48.661" type="appl" />
         <tli id="T65" time="49.262" type="appl" />
         <tli id="T66" time="49.864" type="appl" />
         <tli id="T67" time="50.465" type="appl" />
         <tli id="T68" time="51.452" type="appl" />
         <tli id="T69" time="52.164" type="appl" />
         <tli id="T70" time="52.876" type="appl" />
         <tli id="T71" time="53.652" type="appl" />
         <tli id="T72" time="54.427" type="appl" />
         <tli id="T73" time="55.203" type="appl" />
         <tli id="T74" time="55.978" type="appl" />
         <tli id="T75" time="56.86" />
         <tli id="T76" time="57.833" type="appl" />
         <tli id="T77" time="58.891" type="appl" />
         <tli id="T78" time="59.949" type="appl" />
         <tli id="T79" time="61.007" type="appl" />
         <tli id="T80" time="61.838" type="appl" />
         <tli id="T81" time="62.67" type="appl" />
         <tli id="T82" time="63.236" type="appl" />
         <tli id="T83" time="63.792" type="appl" />
         <tli id="T84" time="64.348" type="appl" />
         <tli id="T85" time="64.904" type="appl" />
         <tli id="T86" time="65.585" type="appl" />
         <tli id="T87" time="66.266" type="appl" />
         <tli id="T88" time="67.072" type="appl" />
         <tli id="T89" time="67.878" type="appl" />
         <tli id="T90" time="68.685" type="appl" />
         <tli id="T91" time="69.491" type="appl" />
         <tli id="T92" time="70.1" type="appl" />
         <tli id="T93" time="70.555" type="appl" />
         <tli id="T94" time="71.009" type="appl" />
         <tli id="T95" time="71.464" type="appl" />
         <tli id="T96" time="71.918" type="appl" />
         <tli id="T97" time="72.428" type="appl" />
         <tli id="T98" time="72.939" type="appl" />
         <tli id="T99" time="73.45" type="appl" />
         <tli id="T100" time="73.96" type="appl" />
         <tli id="T101" time="75.842" type="appl" />
         <tli id="T102" time="77.666" type="appl" />
         <tli id="T103" time="78.259" type="appl" />
         <tli id="T104" time="78.852" type="appl" />
         <tli id="T105" time="79.444" type="appl" />
         <tli id="T106" time="80.037" type="appl" />
         <tli id="T107" time="80.484" type="appl" />
         <tli id="T108" time="80.815" type="appl" />
         <tli id="T109" time="81.145" type="appl" />
         <tli id="T110" time="81.476" type="appl" />
         <tli id="T111" time="81.806" type="appl" />
         <tli id="T112" time="82.482" type="appl" />
         <tli id="T113" time="83.159" type="appl" />
         <tli id="T114" time="84.094" type="appl" />
         <tli id="T115" time="85.029" type="appl" />
         <tli id="T116" time="85.964" type="appl" />
         <tli id="T117" time="86.899" type="appl" />
         <tli id="T118" time="87.553" type="appl" />
         <tli id="T119" time="88.208" type="appl" />
         <tli id="T120" time="88.862" type="appl" />
         <tli id="T121" time="89.517" type="appl" />
         <tli id="T122" time="90.171" type="appl" />
         <tli id="T123" time="91.11" type="appl" />
         <tli id="T124" time="91.919" type="appl" />
         <tli id="T125" time="92.728" type="appl" />
         <tli id="T126" time="93.538" type="appl" />
         <tli id="T127" time="94.217" type="appl" />
         <tli id="T128" time="94.896" type="appl" />
         <tli id="T129" time="95.839" type="appl" />
         <tli id="T130" time="96.782" type="appl" />
         <tli id="T131" time="98.1" />
         <tli id="T132" time="98.673" type="appl" />
         <tli id="T133" time="99.214" type="appl" />
         <tli id="T134" time="99.754" type="appl" />
         <tli id="T135" time="100.295" type="appl" />
         <tli id="T136" time="100.836" type="appl" />
         <tli id="T137" time="101.376" type="appl" />
         <tli id="T138" time="101.916" type="appl" />
         <tli id="T139" time="103.24666666666667" />
         <tli id="T140" time="104.966" type="appl" />
         <tli id="T141" time="106.561" type="appl" />
         <tli id="T142" time="108.66" />
         <tli id="T143" time="109.358" type="appl" />
         <tli id="T144" time="110.079" type="appl" />
         <tli id="T145" time="110.801" type="appl" />
         <tli id="T146" time="111.522" type="appl" />
         <tli id="T147" time="112.244" type="appl" />
         <tli id="T148" time="112.8" type="appl" />
         <tli id="T149" time="113.86" />
         <tli id="T150" time="114.628" type="appl" />
         <tli id="T151" time="115.256" type="appl" />
         <tli id="T152" time="115.883" type="appl" />
         <tli id="T153" time="116.511" type="appl" />
         <tli id="T154" time="117.139" type="appl" />
         <tli id="T155" time="117.987" type="appl" />
         <tli id="T156" time="118.835" type="appl" />
         <tli id="T157" time="119.684" type="appl" />
         <tli id="T158" time="120.532" type="appl" />
         <tli id="T159" time="121.38" type="appl" />
         <tli id="T160" time="122.124" type="appl" />
         <tli id="T161" time="122.867" type="appl" />
         <tli id="T162" time="123.611" type="appl" />
         <tli id="T163" time="124.354" type="appl" />
         <tli id="T164" time="125.098" type="appl" />
         <tli id="T165" time="126.027" type="appl" />
         <tli id="T166" time="126.956" type="appl" />
         <tli id="T167" time="127.884" type="appl" />
         <tli id="T168" time="128.813" type="appl" />
         <tli id="T169" time="129.457" type="appl" />
         <tli id="T170" time="130.101" type="appl" />
         <tli id="T171" time="130.745" type="appl" />
         <tli id="T172" time="131.389" type="appl" />
         <tli id="T173" time="132.168" type="appl" />
         <tli id="T174" time="132.947" type="appl" />
         <tli id="T175" time="133.726" type="appl" />
         <tli id="T176" time="134.504" type="appl" />
         <tli id="T177" time="135.283" type="appl" />
         <tli id="T178" time="136.062" type="appl" />
         <tli id="T179" time="136.841" type="appl" />
         <tli id="T182" time="137.46457142857142" type="intp" />
         <tli id="T180" time="138.296" type="appl" />
         <tli id="T181" time="140.0" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T181" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Onʼiʔ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">kuza</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">tĭlbi</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">dʼü</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">I</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">dĭn</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">kubi</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">zolota</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_32" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">Dĭgəttə</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">măndə:</ts>
                  <nts id="Seg_38" n="HIAT:ip">"</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">Gibər</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">dĭm</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">măna</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">azittə</ts>
                  <nts id="Seg_51" n="HIAT:ip">?</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_54" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">Măna</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">kabarlaʔ</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_62" n="HIAT:w" s="T16">iləʔi</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">il</ts>
                  <nts id="Seg_66" n="HIAT:ip">.</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_69" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_71" n="HIAT:w" s="T18">Dĭgəttə</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_74" n="HIAT:w" s="T19">măn</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_77" n="HIAT:w" s="T20">kalam</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_80" n="HIAT:w" s="T21">koŋdə</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_83" n="HIAT:w" s="T22">saddə</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_86" n="HIAT:w" s="T23">i</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_89" n="HIAT:w" s="T24">mĭluʔləm</ts>
                  <nts id="Seg_90" n="HIAT:ip">"</nts>
                  <nts id="Seg_91" n="HIAT:ip">.</nts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_94" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_96" n="HIAT:w" s="T25">Dĭgəttə</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_99" n="HIAT:w" s="T26">kambi</ts>
                  <nts id="Seg_100" n="HIAT:ip">,</nts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_103" n="HIAT:w" s="T27">kambi</ts>
                  <nts id="Seg_104" n="HIAT:ip">,</nts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_107" n="HIAT:w" s="T28">kandəga</ts>
                  <nts id="Seg_108" n="HIAT:ip">,</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_111" n="HIAT:w" s="T29">kandəga</ts>
                  <nts id="Seg_112" n="HIAT:ip">.</nts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_115" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_117" n="HIAT:w" s="T30">Šobi</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_120" n="HIAT:w" s="T31">dĭn</ts>
                  <nts id="Seg_121" n="HIAT:ip">,</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_124" n="HIAT:w" s="T32">dĭm</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_127" n="HIAT:w" s="T33">ej</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_130" n="HIAT:w" s="T34">öʔlieʔi</ts>
                  <nts id="Seg_131" n="HIAT:ip">.</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_134" n="HIAT:u" s="T35">
                  <nts id="Seg_135" n="HIAT:ip">"</nts>
                  <ts e="T36" id="Seg_137" n="HIAT:w" s="T35">Ĭmbileʔ</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_140" n="HIAT:w" s="T36">tăn</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_142" n="HIAT:ip">(</nts>
                  <ts e="T38" id="Seg_144" n="HIAT:w" s="T37">šonəgan</ts>
                  <nts id="Seg_145" n="HIAT:ip">)</nts>
                  <nts id="Seg_146" n="HIAT:ip">?</nts>
                  <nts id="Seg_147" n="HIAT:ip">"</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_150" n="HIAT:u" s="T38">
                  <nts id="Seg_151" n="HIAT:ip">"</nts>
                  <ts e="T39" id="Seg_153" n="HIAT:w" s="T38">Da</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_156" n="HIAT:w" s="T39">kereʔ</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_159" n="HIAT:w" s="T40">măna</ts>
                  <nts id="Seg_160" n="HIAT:ip">!</nts>
                  <nts id="Seg_161" n="HIAT:ip">"</nts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_164" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_166" n="HIAT:w" s="T41">Dĭgəttə</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_169" n="HIAT:w" s="T42">öʔlüʔbi</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_172" n="HIAT:w" s="T43">onʼiʔ</ts>
                  <nts id="Seg_173" n="HIAT:ip">,</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_176" n="HIAT:w" s="T44">teʔtə</ts>
                  <nts id="Seg_177" n="HIAT:ip">,</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_180" n="HIAT:w" s="T45">nagur</ts>
                  <nts id="Seg_181" n="HIAT:ip">.</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_184" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_186" n="HIAT:w" s="T46">Dĭgəttə</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_189" n="HIAT:w" s="T47">onʼiʔ</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_192" n="HIAT:w" s="T48">nuga</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_195" n="HIAT:w" s="T49">ajində</ts>
                  <nts id="Seg_196" n="HIAT:ip">.</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_199" n="HIAT:u" s="T50">
                  <nts id="Seg_200" n="HIAT:ip">"</nts>
                  <ts e="T51" id="Seg_202" n="HIAT:w" s="T50">Ĭmbi</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_205" n="HIAT:w" s="T51">šonəgal</ts>
                  <nts id="Seg_206" n="HIAT:ip">?</nts>
                  <nts id="Seg_207" n="HIAT:ip">"</nts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_210" n="HIAT:u" s="T52">
                  <nts id="Seg_211" n="HIAT:ip">"</nts>
                  <ts e="T53" id="Seg_213" n="HIAT:w" s="T52">Da</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_216" n="HIAT:w" s="T53">măn</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_219" n="HIAT:w" s="T54">kundlaʔbəm</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_222" n="HIAT:w" s="T55">koŋdə</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_225" n="HIAT:w" s="T56">zolota</ts>
                  <nts id="Seg_226" n="HIAT:ip">"</nts>
                  <nts id="Seg_227" n="HIAT:ip">.</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_230" n="HIAT:u" s="T57">
                  <nts id="Seg_231" n="HIAT:ip">"</nts>
                  <ts e="T58" id="Seg_233" n="HIAT:w" s="T57">Pʼerdə</ts>
                  <nts id="Seg_234" n="HIAT:ip">!</nts>
                  <nts id="Seg_235" n="HIAT:ip">"</nts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_238" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_240" n="HIAT:w" s="T58">Dĭ</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_243" n="HIAT:w" s="T59">pʼerbi</ts>
                  <nts id="Seg_244" n="HIAT:ip">.</nts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_247" n="HIAT:u" s="T60">
                  <nts id="Seg_248" n="HIAT:ip">"</nts>
                  <ts e="T61" id="Seg_250" n="HIAT:w" s="T60">No</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_253" n="HIAT:w" s="T61">ĭmbi</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_256" n="HIAT:w" s="T62">tănan</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_259" n="HIAT:w" s="T63">mĭləj</ts>
                  <nts id="Seg_260" n="HIAT:ip">?</nts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_263" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_265" n="HIAT:w" s="T64">Pʼeldə</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_268" n="HIAT:w" s="T65">măna</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_271" n="HIAT:w" s="T66">mĭleʔ</ts>
                  <nts id="Seg_272" n="HIAT:ip">!</nts>
                  <nts id="Seg_273" n="HIAT:ip">"</nts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_276" n="HIAT:u" s="T67">
                  <nts id="Seg_277" n="HIAT:ip">"</nts>
                  <ts e="T68" id="Seg_279" n="HIAT:w" s="T67">Mĭlem</ts>
                  <nts id="Seg_280" n="HIAT:ip">!</nts>
                  <nts id="Seg_281" n="HIAT:ip">"</nts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_284" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_286" n="HIAT:w" s="T68">Dĭgəttə</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_289" n="HIAT:w" s="T69">šobi</ts>
                  <nts id="Seg_290" n="HIAT:ip">.</nts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_293" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_295" n="HIAT:w" s="T70">Măndə:</ts>
                  <nts id="Seg_296" n="HIAT:ip">"</nts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_299" n="HIAT:w" s="T71">Măn</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_302" n="HIAT:w" s="T72">tănan</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_305" n="HIAT:w" s="T73">ĭmbileʔ</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_307" n="HIAT:ip">(</nts>
                  <ts e="T75" id="Seg_309" n="HIAT:w" s="T74">šobial</ts>
                  <nts id="Seg_310" n="HIAT:ip">)</nts>
                  <nts id="Seg_311" n="HIAT:ip">.</nts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_314" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_316" n="HIAT:w" s="T75">Măn</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_319" n="HIAT:w" s="T76">tănan</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_322" n="HIAT:w" s="T77">deʔpiem</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_325" n="HIAT:w" s="T78">zolota</ts>
                  <nts id="Seg_326" n="HIAT:ip">"</nts>
                  <nts id="Seg_327" n="HIAT:ip">.</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_330" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_332" n="HIAT:w" s="T79">Dĭʔnə</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_335" n="HIAT:w" s="T80">mĭbi</ts>
                  <nts id="Seg_336" n="HIAT:ip">.</nts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_339" n="HIAT:u" s="T81">
                  <nts id="Seg_340" n="HIAT:ip">"</nts>
                  <ts e="T82" id="Seg_342" n="HIAT:w" s="T81">Ĭmbi</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_345" n="HIAT:w" s="T82">tănan</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_347" n="HIAT:ip">(</nts>
                  <ts e="T84" id="Seg_349" n="HIAT:w" s="T83">măn=</ts>
                  <nts id="Seg_350" n="HIAT:ip">)</nts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_353" n="HIAT:w" s="T84">mĭzittə</ts>
                  <nts id="Seg_354" n="HIAT:ip">?</nts>
                  <nts id="Seg_355" n="HIAT:ip">"</nts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_358" n="HIAT:u" s="T85">
                  <nts id="Seg_359" n="HIAT:ip">"</nts>
                  <ts e="T86" id="Seg_361" n="HIAT:w" s="T85">Deʔ</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_364" n="HIAT:w" s="T86">ipek</ts>
                  <nts id="Seg_365" n="HIAT:ip">!</nts>
                  <nts id="Seg_366" n="HIAT:ip">"</nts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_369" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_371" n="HIAT:w" s="T87">Dĭʔnə</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_374" n="HIAT:w" s="T88">deʔpiʔi</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_377" n="HIAT:w" s="T89">sĭre</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_380" n="HIAT:w" s="T90">ipek</ts>
                  <nts id="Seg_381" n="HIAT:ip">.</nts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_384" n="HIAT:u" s="T91">
                  <nts id="Seg_385" n="HIAT:ip">"</nts>
                  <ts e="T92" id="Seg_387" n="HIAT:w" s="T91">Măn</ts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_390" n="HIAT:w" s="T92">dĭrgit</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_393" n="HIAT:w" s="T93">ipek</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_396" n="HIAT:w" s="T94">ej</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_399" n="HIAT:w" s="T95">amniam</ts>
                  <nts id="Seg_400" n="HIAT:ip">.</nts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_403" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_405" n="HIAT:w" s="T96">Măna</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_408" n="HIAT:w" s="T97">nada</ts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_411" n="HIAT:w" s="T98">sagər</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_414" n="HIAT:w" s="T99">ipek</ts>
                  <nts id="Seg_415" n="HIAT:ip">"</nts>
                  <nts id="Seg_416" n="HIAT:ip">.</nts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_419" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_421" n="HIAT:w" s="T100">Kambiʔi</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_424" n="HIAT:w" s="T101">nʼišən</ts>
                  <nts id="Seg_425" n="HIAT:ip">.</nts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_428" n="HIAT:u" s="T102">
                  <ts e="T103" id="Seg_430" n="HIAT:w" s="T102">Ibiʔi</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_433" n="HIAT:w" s="T103">ipek</ts>
                  <nts id="Seg_434" n="HIAT:ip">,</nts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_437" n="HIAT:w" s="T104">dĭ</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_440" n="HIAT:w" s="T105">ambi</ts>
                  <nts id="Seg_441" n="HIAT:ip">.</nts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T111" id="Seg_444" n="HIAT:u" s="T106">
                  <nts id="Seg_445" n="HIAT:ip">"</nts>
                  <ts e="T107" id="Seg_447" n="HIAT:w" s="T106">A</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_450" n="HIAT:w" s="T107">tüj</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_453" n="HIAT:w" s="T108">ĭmbi</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_456" n="HIAT:w" s="T109">tănan</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_459" n="HIAT:w" s="T110">kereʔ</ts>
                  <nts id="Seg_460" n="HIAT:ip">?</nts>
                  <nts id="Seg_461" n="HIAT:ip">"</nts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_464" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_466" n="HIAT:w" s="T111">Da</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_469" n="HIAT:w" s="T112">kunolzittə</ts>
                  <nts id="Seg_470" n="HIAT:ip">.</nts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T117" id="Seg_473" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_475" n="HIAT:w" s="T113">Dĭgəttə</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_478" n="HIAT:w" s="T114">dĭʔnə</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_481" n="HIAT:w" s="T115">deʔpiʔi</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_484" n="HIAT:w" s="T116">pʼerinăʔi</ts>
                  <nts id="Seg_485" n="HIAT:ip">.</nts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_488" n="HIAT:u" s="T117">
                  <ts e="T118" id="Seg_490" n="HIAT:w" s="T117">Măndə:</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_492" n="HIAT:ip">"</nts>
                  <ts e="T119" id="Seg_494" n="HIAT:w" s="T118">Dĭn</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_497" n="HIAT:w" s="T119">ej</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_500" n="HIAT:w" s="T120">moliam</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_503" n="HIAT:w" s="T121">kunolzittə</ts>
                  <nts id="Seg_504" n="HIAT:ip">.</nts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_507" n="HIAT:u" s="T122">
                  <nts id="Seg_508" n="HIAT:ip">(</nts>
                  <ts e="T123" id="Seg_510" n="HIAT:w" s="T122">Deʔ-</ts>
                  <nts id="Seg_511" n="HIAT:ip">)</nts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_513" n="HIAT:ip">(</nts>
                  <ts e="T124" id="Seg_515" n="HIAT:w" s="T123">Detəgeʔ</ts>
                  <nts id="Seg_516" n="HIAT:ip">)</nts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_519" n="HIAT:w" s="T124">măna</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_522" n="HIAT:w" s="T125">săloma</ts>
                  <nts id="Seg_523" n="HIAT:ip">"</nts>
                  <nts id="Seg_524" n="HIAT:ip">.</nts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_527" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_529" n="HIAT:w" s="T126">Dĭʔnə</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_532" n="HIAT:w" s="T127">deʔpiʔi</ts>
                  <nts id="Seg_533" n="HIAT:ip">.</nts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_536" n="HIAT:u" s="T128">
                  <ts e="T129" id="Seg_538" n="HIAT:w" s="T128">Dĭ</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_541" n="HIAT:w" s="T129">iʔbolaʔpi</ts>
                  <nts id="Seg_542" n="HIAT:ip">,</nts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_545" n="HIAT:w" s="T130">kunollaʔpi</ts>
                  <nts id="Seg_546" n="HIAT:ip">.</nts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_549" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_551" n="HIAT:w" s="T131">Dĭgəttə</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_554" n="HIAT:w" s="T132">măndə:</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_556" n="HIAT:ip">"</nts>
                  <ts e="T134" id="Seg_558" n="HIAT:w" s="T133">Măn</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_561" n="HIAT:w" s="T134">dĭn</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_564" n="HIAT:w" s="T135">bar</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_567" n="HIAT:w" s="T136">ej</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_570" n="HIAT:w" s="T137">jakšə</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_573" n="HIAT:w" s="T138">abiam</ts>
                  <nts id="Seg_574" n="HIAT:ip">.</nts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T142" id="Seg_577" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_579" n="HIAT:w" s="T139">Deʔkeʔ</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_582" n="HIAT:w" s="T140">măna</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_585" n="HIAT:w" s="T141">rozgaʔi</ts>
                  <nts id="Seg_586" n="HIAT:ip">"</nts>
                  <nts id="Seg_587" n="HIAT:ip">.</nts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T147" id="Seg_590" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_592" n="HIAT:w" s="T142">Dĭgəttə</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_595" n="HIAT:w" s="T143">deʔpiʔi</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_598" n="HIAT:w" s="T144">rozgaʔi</ts>
                  <nts id="Seg_599" n="HIAT:ip">,</nts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_602" n="HIAT:w" s="T145">münörzittə</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_605" n="HIAT:w" s="T146">dĭm</ts>
                  <nts id="Seg_606" n="HIAT:ip">.</nts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T154" id="Seg_609" n="HIAT:u" s="T147">
                  <ts e="T148" id="Seg_611" n="HIAT:w" s="T147">Dĭ</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_614" n="HIAT:w" s="T148">măndə:</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_616" n="HIAT:ip">"</nts>
                  <ts e="T150" id="Seg_618" n="HIAT:w" s="T149">Dĭn</ts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_621" n="HIAT:w" s="T150">ajində</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_624" n="HIAT:w" s="T151">tăn</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_627" n="HIAT:w" s="T152">kuza</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_630" n="HIAT:w" s="T153">nulaʔpi</ts>
                  <nts id="Seg_631" n="HIAT:ip">.</nts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T159" id="Seg_634" n="HIAT:u" s="T154">
                  <ts e="T155" id="Seg_636" n="HIAT:w" s="T154">Dĭ</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_639" n="HIAT:w" s="T155">măndə:</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_642" n="HIAT:w" s="T156">măna</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_645" n="HIAT:w" s="T157">pʼeldə</ts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_648" n="HIAT:w" s="T158">dettə</ts>
                  <nts id="Seg_649" n="HIAT:ip">"</nts>
                  <nts id="Seg_650" n="HIAT:ip">.</nts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T164" id="Seg_653" n="HIAT:u" s="T159">
                  <ts e="T160" id="Seg_655" n="HIAT:w" s="T159">Dĭgəttə</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_658" n="HIAT:w" s="T160">dĭm</ts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_661" n="HIAT:w" s="T161">münörbiʔi</ts>
                  <nts id="Seg_662" n="HIAT:ip">,</nts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_665" n="HIAT:w" s="T162">pʼeldə</ts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_668" n="HIAT:w" s="T163">mĭbiʔi</ts>
                  <nts id="Seg_669" n="HIAT:ip">.</nts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_672" n="HIAT:u" s="T164">
                  <ts e="T165" id="Seg_674" n="HIAT:w" s="T164">Da</ts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_677" n="HIAT:w" s="T165">bar</ts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_680" n="HIAT:w" s="T166">dĭʔnə</ts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_683" n="HIAT:w" s="T167">mĭgeʔi</ts>
                  <nts id="Seg_684" n="HIAT:ip">.</nts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T172" id="Seg_687" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_689" n="HIAT:w" s="T168">Dĭgəttə</ts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_692" n="HIAT:w" s="T169">bar</ts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_695" n="HIAT:w" s="T170">dĭm</ts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_698" n="HIAT:w" s="T171">münörluʔpi</ts>
                  <nts id="Seg_699" n="HIAT:ip">.</nts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T179" id="Seg_702" n="HIAT:u" s="T172">
                  <ts e="T173" id="Seg_704" n="HIAT:w" s="T172">A</ts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_707" n="HIAT:w" s="T173">dĭ</ts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_710" n="HIAT:w" s="T174">ibi</ts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_713" n="HIAT:w" s="T175">üžübə</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_716" n="HIAT:w" s="T176">da</ts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_718" n="HIAT:ip">(</nts>
                  <ts e="T178" id="Seg_720" n="HIAT:w" s="T177">nuʔməla-</ts>
                  <nts id="Seg_721" n="HIAT:ip">)</nts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_724" n="HIAT:w" s="T178">nuʔməluʔpi</ts>
                  <nts id="Seg_725" n="HIAT:ip">.</nts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T180" id="Seg_728" n="HIAT:u" s="T179">
                  <ts e="T182" id="Seg_730" n="HIAT:w" s="T179">Kalla</ts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_733" n="HIAT:w" s="T182">dʼürbi</ts>
                  <nts id="Seg_734" n="HIAT:ip">.</nts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_737" n="HIAT:u" s="T180">
                  <ts e="T181" id="Seg_739" n="HIAT:w" s="T180">Kabarləj</ts>
                  <nts id="Seg_740" n="HIAT:ip">.</nts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T181" id="Seg_742" n="sc" s="T0">
               <ts e="T1" id="Seg_744" n="e" s="T0">Onʼiʔ </ts>
               <ts e="T2" id="Seg_746" n="e" s="T1">kuza </ts>
               <ts e="T3" id="Seg_748" n="e" s="T2">tĭlbi </ts>
               <ts e="T4" id="Seg_750" n="e" s="T3">dʼü. </ts>
               <ts e="T5" id="Seg_752" n="e" s="T4">I </ts>
               <ts e="T6" id="Seg_754" n="e" s="T5">dĭn </ts>
               <ts e="T7" id="Seg_756" n="e" s="T6">kubi </ts>
               <ts e="T8" id="Seg_758" n="e" s="T7">zolota. </ts>
               <ts e="T9" id="Seg_760" n="e" s="T8">Dĭgəttə </ts>
               <ts e="T10" id="Seg_762" n="e" s="T9">măndə:" </ts>
               <ts e="T11" id="Seg_764" n="e" s="T10">Gibər </ts>
               <ts e="T12" id="Seg_766" n="e" s="T11">dĭm </ts>
               <ts e="T13" id="Seg_768" n="e" s="T12">măna </ts>
               <ts e="T14" id="Seg_770" n="e" s="T13">azittə? </ts>
               <ts e="T15" id="Seg_772" n="e" s="T14">Măna </ts>
               <ts e="T16" id="Seg_774" n="e" s="T15">kabarlaʔ </ts>
               <ts e="T17" id="Seg_776" n="e" s="T16">iləʔi </ts>
               <ts e="T18" id="Seg_778" n="e" s="T17">il. </ts>
               <ts e="T19" id="Seg_780" n="e" s="T18">Dĭgəttə </ts>
               <ts e="T20" id="Seg_782" n="e" s="T19">măn </ts>
               <ts e="T21" id="Seg_784" n="e" s="T20">kalam </ts>
               <ts e="T22" id="Seg_786" n="e" s="T21">koŋdə </ts>
               <ts e="T23" id="Seg_788" n="e" s="T22">saddə </ts>
               <ts e="T24" id="Seg_790" n="e" s="T23">i </ts>
               <ts e="T25" id="Seg_792" n="e" s="T24">mĭluʔləm". </ts>
               <ts e="T26" id="Seg_794" n="e" s="T25">Dĭgəttə </ts>
               <ts e="T27" id="Seg_796" n="e" s="T26">kambi, </ts>
               <ts e="T28" id="Seg_798" n="e" s="T27">kambi, </ts>
               <ts e="T29" id="Seg_800" n="e" s="T28">kandəga, </ts>
               <ts e="T30" id="Seg_802" n="e" s="T29">kandəga. </ts>
               <ts e="T31" id="Seg_804" n="e" s="T30">Šobi </ts>
               <ts e="T32" id="Seg_806" n="e" s="T31">dĭn, </ts>
               <ts e="T33" id="Seg_808" n="e" s="T32">dĭm </ts>
               <ts e="T34" id="Seg_810" n="e" s="T33">ej </ts>
               <ts e="T35" id="Seg_812" n="e" s="T34">öʔlieʔi. </ts>
               <ts e="T36" id="Seg_814" n="e" s="T35">"Ĭmbileʔ </ts>
               <ts e="T37" id="Seg_816" n="e" s="T36">tăn </ts>
               <ts e="T38" id="Seg_818" n="e" s="T37">(šonəgan)?" </ts>
               <ts e="T39" id="Seg_820" n="e" s="T38">"Da </ts>
               <ts e="T40" id="Seg_822" n="e" s="T39">kereʔ </ts>
               <ts e="T41" id="Seg_824" n="e" s="T40">măna!" </ts>
               <ts e="T42" id="Seg_826" n="e" s="T41">Dĭgəttə </ts>
               <ts e="T43" id="Seg_828" n="e" s="T42">öʔlüʔbi </ts>
               <ts e="T44" id="Seg_830" n="e" s="T43">onʼiʔ, </ts>
               <ts e="T45" id="Seg_832" n="e" s="T44">teʔtə, </ts>
               <ts e="T46" id="Seg_834" n="e" s="T45">nagur. </ts>
               <ts e="T47" id="Seg_836" n="e" s="T46">Dĭgəttə </ts>
               <ts e="T48" id="Seg_838" n="e" s="T47">onʼiʔ </ts>
               <ts e="T49" id="Seg_840" n="e" s="T48">nuga </ts>
               <ts e="T50" id="Seg_842" n="e" s="T49">ajində. </ts>
               <ts e="T51" id="Seg_844" n="e" s="T50">"Ĭmbi </ts>
               <ts e="T52" id="Seg_846" n="e" s="T51">šonəgal?" </ts>
               <ts e="T53" id="Seg_848" n="e" s="T52">"Da </ts>
               <ts e="T54" id="Seg_850" n="e" s="T53">măn </ts>
               <ts e="T55" id="Seg_852" n="e" s="T54">kundlaʔbəm </ts>
               <ts e="T56" id="Seg_854" n="e" s="T55">koŋdə </ts>
               <ts e="T57" id="Seg_856" n="e" s="T56">zolota". </ts>
               <ts e="T58" id="Seg_858" n="e" s="T57">"Pʼerdə!" </ts>
               <ts e="T59" id="Seg_860" n="e" s="T58">Dĭ </ts>
               <ts e="T60" id="Seg_862" n="e" s="T59">pʼerbi. </ts>
               <ts e="T61" id="Seg_864" n="e" s="T60">"No </ts>
               <ts e="T62" id="Seg_866" n="e" s="T61">ĭmbi </ts>
               <ts e="T63" id="Seg_868" n="e" s="T62">tănan </ts>
               <ts e="T64" id="Seg_870" n="e" s="T63">mĭləj? </ts>
               <ts e="T65" id="Seg_872" n="e" s="T64">Pʼeldə </ts>
               <ts e="T66" id="Seg_874" n="e" s="T65">măna </ts>
               <ts e="T67" id="Seg_876" n="e" s="T66">mĭleʔ!" </ts>
               <ts e="T68" id="Seg_878" n="e" s="T67">"Mĭlem!" </ts>
               <ts e="T69" id="Seg_880" n="e" s="T68">Dĭgəttə </ts>
               <ts e="T70" id="Seg_882" n="e" s="T69">šobi. </ts>
               <ts e="T71" id="Seg_884" n="e" s="T70">Măndə:" </ts>
               <ts e="T72" id="Seg_886" n="e" s="T71">Măn </ts>
               <ts e="T73" id="Seg_888" n="e" s="T72">tănan </ts>
               <ts e="T74" id="Seg_890" n="e" s="T73">ĭmbileʔ </ts>
               <ts e="T75" id="Seg_892" n="e" s="T74">(šobial). </ts>
               <ts e="T76" id="Seg_894" n="e" s="T75">Măn </ts>
               <ts e="T77" id="Seg_896" n="e" s="T76">tănan </ts>
               <ts e="T78" id="Seg_898" n="e" s="T77">deʔpiem </ts>
               <ts e="T79" id="Seg_900" n="e" s="T78">zolota". </ts>
               <ts e="T80" id="Seg_902" n="e" s="T79">Dĭʔnə </ts>
               <ts e="T81" id="Seg_904" n="e" s="T80">mĭbi. </ts>
               <ts e="T82" id="Seg_906" n="e" s="T81">"Ĭmbi </ts>
               <ts e="T83" id="Seg_908" n="e" s="T82">tănan </ts>
               <ts e="T84" id="Seg_910" n="e" s="T83">(măn=) </ts>
               <ts e="T85" id="Seg_912" n="e" s="T84">mĭzittə?" </ts>
               <ts e="T86" id="Seg_914" n="e" s="T85">"Deʔ </ts>
               <ts e="T87" id="Seg_916" n="e" s="T86">ipek!" </ts>
               <ts e="T88" id="Seg_918" n="e" s="T87">Dĭʔnə </ts>
               <ts e="T89" id="Seg_920" n="e" s="T88">deʔpiʔi </ts>
               <ts e="T90" id="Seg_922" n="e" s="T89">sĭre </ts>
               <ts e="T91" id="Seg_924" n="e" s="T90">ipek. </ts>
               <ts e="T92" id="Seg_926" n="e" s="T91">"Măn </ts>
               <ts e="T93" id="Seg_928" n="e" s="T92">dĭrgit </ts>
               <ts e="T94" id="Seg_930" n="e" s="T93">ipek </ts>
               <ts e="T95" id="Seg_932" n="e" s="T94">ej </ts>
               <ts e="T96" id="Seg_934" n="e" s="T95">amniam. </ts>
               <ts e="T97" id="Seg_936" n="e" s="T96">Măna </ts>
               <ts e="T98" id="Seg_938" n="e" s="T97">nada </ts>
               <ts e="T99" id="Seg_940" n="e" s="T98">sagər </ts>
               <ts e="T100" id="Seg_942" n="e" s="T99">ipek". </ts>
               <ts e="T101" id="Seg_944" n="e" s="T100">Kambiʔi </ts>
               <ts e="T102" id="Seg_946" n="e" s="T101">nʼišən. </ts>
               <ts e="T103" id="Seg_948" n="e" s="T102">Ibiʔi </ts>
               <ts e="T104" id="Seg_950" n="e" s="T103">ipek, </ts>
               <ts e="T105" id="Seg_952" n="e" s="T104">dĭ </ts>
               <ts e="T106" id="Seg_954" n="e" s="T105">ambi. </ts>
               <ts e="T107" id="Seg_956" n="e" s="T106">"A </ts>
               <ts e="T108" id="Seg_958" n="e" s="T107">tüj </ts>
               <ts e="T109" id="Seg_960" n="e" s="T108">ĭmbi </ts>
               <ts e="T110" id="Seg_962" n="e" s="T109">tănan </ts>
               <ts e="T111" id="Seg_964" n="e" s="T110">kereʔ?" </ts>
               <ts e="T112" id="Seg_966" n="e" s="T111">Da </ts>
               <ts e="T113" id="Seg_968" n="e" s="T112">kunolzittə. </ts>
               <ts e="T114" id="Seg_970" n="e" s="T113">Dĭgəttə </ts>
               <ts e="T115" id="Seg_972" n="e" s="T114">dĭʔnə </ts>
               <ts e="T116" id="Seg_974" n="e" s="T115">deʔpiʔi </ts>
               <ts e="T117" id="Seg_976" n="e" s="T116">pʼerinăʔi. </ts>
               <ts e="T118" id="Seg_978" n="e" s="T117">Măndə: </ts>
               <ts e="T119" id="Seg_980" n="e" s="T118">"Dĭn </ts>
               <ts e="T120" id="Seg_982" n="e" s="T119">ej </ts>
               <ts e="T121" id="Seg_984" n="e" s="T120">moliam </ts>
               <ts e="T122" id="Seg_986" n="e" s="T121">kunolzittə. </ts>
               <ts e="T123" id="Seg_988" n="e" s="T122">(Deʔ-) </ts>
               <ts e="T124" id="Seg_990" n="e" s="T123">(Detəgeʔ) </ts>
               <ts e="T125" id="Seg_992" n="e" s="T124">măna </ts>
               <ts e="T126" id="Seg_994" n="e" s="T125">săloma". </ts>
               <ts e="T127" id="Seg_996" n="e" s="T126">Dĭʔnə </ts>
               <ts e="T128" id="Seg_998" n="e" s="T127">deʔpiʔi. </ts>
               <ts e="T129" id="Seg_1000" n="e" s="T128">Dĭ </ts>
               <ts e="T130" id="Seg_1002" n="e" s="T129">iʔbolaʔpi, </ts>
               <ts e="T131" id="Seg_1004" n="e" s="T130">kunollaʔpi. </ts>
               <ts e="T132" id="Seg_1006" n="e" s="T131">Dĭgəttə </ts>
               <ts e="T133" id="Seg_1008" n="e" s="T132">măndə: </ts>
               <ts e="T134" id="Seg_1010" n="e" s="T133">"Măn </ts>
               <ts e="T135" id="Seg_1012" n="e" s="T134">dĭn </ts>
               <ts e="T136" id="Seg_1014" n="e" s="T135">bar </ts>
               <ts e="T137" id="Seg_1016" n="e" s="T136">ej </ts>
               <ts e="T138" id="Seg_1018" n="e" s="T137">jakšə </ts>
               <ts e="T139" id="Seg_1020" n="e" s="T138">abiam. </ts>
               <ts e="T140" id="Seg_1022" n="e" s="T139">Deʔkeʔ </ts>
               <ts e="T141" id="Seg_1024" n="e" s="T140">măna </ts>
               <ts e="T142" id="Seg_1026" n="e" s="T141">rozgaʔi". </ts>
               <ts e="T143" id="Seg_1028" n="e" s="T142">Dĭgəttə </ts>
               <ts e="T144" id="Seg_1030" n="e" s="T143">deʔpiʔi </ts>
               <ts e="T145" id="Seg_1032" n="e" s="T144">rozgaʔi, </ts>
               <ts e="T146" id="Seg_1034" n="e" s="T145">münörzittə </ts>
               <ts e="T147" id="Seg_1036" n="e" s="T146">dĭm. </ts>
               <ts e="T148" id="Seg_1038" n="e" s="T147">Dĭ </ts>
               <ts e="T149" id="Seg_1040" n="e" s="T148">măndə: </ts>
               <ts e="T150" id="Seg_1042" n="e" s="T149">"Dĭn </ts>
               <ts e="T151" id="Seg_1044" n="e" s="T150">ajində </ts>
               <ts e="T152" id="Seg_1046" n="e" s="T151">tăn </ts>
               <ts e="T153" id="Seg_1048" n="e" s="T152">kuza </ts>
               <ts e="T154" id="Seg_1050" n="e" s="T153">nulaʔpi. </ts>
               <ts e="T155" id="Seg_1052" n="e" s="T154">Dĭ </ts>
               <ts e="T156" id="Seg_1054" n="e" s="T155">măndə: </ts>
               <ts e="T157" id="Seg_1056" n="e" s="T156">măna </ts>
               <ts e="T158" id="Seg_1058" n="e" s="T157">pʼeldə </ts>
               <ts e="T159" id="Seg_1060" n="e" s="T158">dettə". </ts>
               <ts e="T160" id="Seg_1062" n="e" s="T159">Dĭgəttə </ts>
               <ts e="T161" id="Seg_1064" n="e" s="T160">dĭm </ts>
               <ts e="T162" id="Seg_1066" n="e" s="T161">münörbiʔi, </ts>
               <ts e="T163" id="Seg_1068" n="e" s="T162">pʼeldə </ts>
               <ts e="T164" id="Seg_1070" n="e" s="T163">mĭbiʔi. </ts>
               <ts e="T165" id="Seg_1072" n="e" s="T164">Da </ts>
               <ts e="T166" id="Seg_1074" n="e" s="T165">bar </ts>
               <ts e="T167" id="Seg_1076" n="e" s="T166">dĭʔnə </ts>
               <ts e="T168" id="Seg_1078" n="e" s="T167">mĭgeʔi. </ts>
               <ts e="T169" id="Seg_1080" n="e" s="T168">Dĭgəttə </ts>
               <ts e="T170" id="Seg_1082" n="e" s="T169">bar </ts>
               <ts e="T171" id="Seg_1084" n="e" s="T170">dĭm </ts>
               <ts e="T172" id="Seg_1086" n="e" s="T171">münörluʔpi. </ts>
               <ts e="T173" id="Seg_1088" n="e" s="T172">A </ts>
               <ts e="T174" id="Seg_1090" n="e" s="T173">dĭ </ts>
               <ts e="T175" id="Seg_1092" n="e" s="T174">ibi </ts>
               <ts e="T176" id="Seg_1094" n="e" s="T175">üžübə </ts>
               <ts e="T177" id="Seg_1096" n="e" s="T176">da </ts>
               <ts e="T178" id="Seg_1098" n="e" s="T177">(nuʔməla-) </ts>
               <ts e="T179" id="Seg_1100" n="e" s="T178">nuʔməluʔpi. </ts>
               <ts e="T182" id="Seg_1102" n="e" s="T179">Kalla </ts>
               <ts e="T180" id="Seg_1104" n="e" s="T182">dʼürbi. </ts>
               <ts e="T181" id="Seg_1106" n="e" s="T180">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_1107" s="T0">PKZ_196X_Gold_flk.001 (002)</ta>
            <ta e="T8" id="Seg_1108" s="T4">PKZ_196X_Gold_flk.002 (003)</ta>
            <ta e="T14" id="Seg_1109" s="T8">PKZ_196X_Gold_flk.003 (004)</ta>
            <ta e="T18" id="Seg_1110" s="T14">PKZ_196X_Gold_flk.004 (005)</ta>
            <ta e="T25" id="Seg_1111" s="T18">PKZ_196X_Gold_flk.005 (006)</ta>
            <ta e="T30" id="Seg_1112" s="T25">PKZ_196X_Gold_flk.006 (007)</ta>
            <ta e="T35" id="Seg_1113" s="T30">PKZ_196X_Gold_flk.007 (008)</ta>
            <ta e="T38" id="Seg_1114" s="T35">PKZ_196X_Gold_flk.008 (009)</ta>
            <ta e="T41" id="Seg_1115" s="T38">PKZ_196X_Gold_flk.009 (010)</ta>
            <ta e="T46" id="Seg_1116" s="T41">PKZ_196X_Gold_flk.010 (011)</ta>
            <ta e="T50" id="Seg_1117" s="T46">PKZ_196X_Gold_flk.011 (012)</ta>
            <ta e="T52" id="Seg_1118" s="T50">PKZ_196X_Gold_flk.012 (013)</ta>
            <ta e="T57" id="Seg_1119" s="T52">PKZ_196X_Gold_flk.013 (014)</ta>
            <ta e="T58" id="Seg_1120" s="T57">PKZ_196X_Gold_flk.014 (015)</ta>
            <ta e="T60" id="Seg_1121" s="T58">PKZ_196X_Gold_flk.015 (016)</ta>
            <ta e="T64" id="Seg_1122" s="T60">PKZ_196X_Gold_flk.016 (017)</ta>
            <ta e="T67" id="Seg_1123" s="T64">PKZ_196X_Gold_flk.017 (018)</ta>
            <ta e="T68" id="Seg_1124" s="T67">PKZ_196X_Gold_flk.018 (019)</ta>
            <ta e="T70" id="Seg_1125" s="T68">PKZ_196X_Gold_flk.019 (020)</ta>
            <ta e="T75" id="Seg_1126" s="T70">PKZ_196X_Gold_flk.020 (021)</ta>
            <ta e="T79" id="Seg_1127" s="T75">PKZ_196X_Gold_flk.021 (022)</ta>
            <ta e="T81" id="Seg_1128" s="T79">PKZ_196X_Gold_flk.022 (023)</ta>
            <ta e="T85" id="Seg_1129" s="T81">PKZ_196X_Gold_flk.023 (024)</ta>
            <ta e="T87" id="Seg_1130" s="T85">PKZ_196X_Gold_flk.024 (025)</ta>
            <ta e="T91" id="Seg_1131" s="T87">PKZ_196X_Gold_flk.025 (026)</ta>
            <ta e="T96" id="Seg_1132" s="T91">PKZ_196X_Gold_flk.026 (027)</ta>
            <ta e="T100" id="Seg_1133" s="T96">PKZ_196X_Gold_flk.027 (028)</ta>
            <ta e="T102" id="Seg_1134" s="T100">PKZ_196X_Gold_flk.028 (029)</ta>
            <ta e="T106" id="Seg_1135" s="T102">PKZ_196X_Gold_flk.029 (030)</ta>
            <ta e="T111" id="Seg_1136" s="T106">PKZ_196X_Gold_flk.030 (031)</ta>
            <ta e="T113" id="Seg_1137" s="T111">PKZ_196X_Gold_flk.031 (032)</ta>
            <ta e="T117" id="Seg_1138" s="T113">PKZ_196X_Gold_flk.032 (033)</ta>
            <ta e="T122" id="Seg_1139" s="T117">PKZ_196X_Gold_flk.033 (034)</ta>
            <ta e="T126" id="Seg_1140" s="T122">PKZ_196X_Gold_flk.034 (035)</ta>
            <ta e="T128" id="Seg_1141" s="T126">PKZ_196X_Gold_flk.035 (036)</ta>
            <ta e="T131" id="Seg_1142" s="T128">PKZ_196X_Gold_flk.036 (037)</ta>
            <ta e="T139" id="Seg_1143" s="T131">PKZ_196X_Gold_flk.037 (038)</ta>
            <ta e="T142" id="Seg_1144" s="T139">PKZ_196X_Gold_flk.038 (039)</ta>
            <ta e="T147" id="Seg_1145" s="T142">PKZ_196X_Gold_flk.039 (040)</ta>
            <ta e="T154" id="Seg_1146" s="T147">PKZ_196X_Gold_flk.040 (041)</ta>
            <ta e="T159" id="Seg_1147" s="T154">PKZ_196X_Gold_flk.041 (043)</ta>
            <ta e="T164" id="Seg_1148" s="T159">PKZ_196X_Gold_flk.042 (044)</ta>
            <ta e="T168" id="Seg_1149" s="T164">PKZ_196X_Gold_flk.043 (045)</ta>
            <ta e="T172" id="Seg_1150" s="T168">PKZ_196X_Gold_flk.044 (046)</ta>
            <ta e="T179" id="Seg_1151" s="T172">PKZ_196X_Gold_flk.045 (047)</ta>
            <ta e="T180" id="Seg_1152" s="T179">PKZ_196X_Gold_flk.046 (048)</ta>
            <ta e="T181" id="Seg_1153" s="T180">PKZ_196X_Gold_flk.047 (049)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_1154" s="T0">Onʼiʔ kuza tĭlbi dʼü. </ta>
            <ta e="T8" id="Seg_1155" s="T4">I dĭn kubi zolota. </ta>
            <ta e="T14" id="Seg_1156" s="T8">Dĭgəttə măndə:" Gibər dĭm măna azittə? </ta>
            <ta e="T18" id="Seg_1157" s="T14">Măna kabarlaʔ iləʔi il. </ta>
            <ta e="T25" id="Seg_1158" s="T18">Dĭgəttə măn kalam koŋdə saddə i mĭluʔləm". </ta>
            <ta e="T30" id="Seg_1159" s="T25">Dĭgəttə kambi, kambi, kandəga, kandəga. </ta>
            <ta e="T35" id="Seg_1160" s="T30">Šobi dĭn, dĭm ej öʔlieʔi. </ta>
            <ta e="T38" id="Seg_1161" s="T35">"Ĭmbileʔ tăn (šonəgan)?" </ta>
            <ta e="T41" id="Seg_1162" s="T38">"Da kereʔ măna!" </ta>
            <ta e="T46" id="Seg_1163" s="T41">Dĭgəttə öʔlüʔbi onʼiʔ, teʔtə, nagur. </ta>
            <ta e="T50" id="Seg_1164" s="T46">Dĭgəttə onʼiʔ nuga ajində. </ta>
            <ta e="T52" id="Seg_1165" s="T50">"Ĭmbi šonəgal?" </ta>
            <ta e="T57" id="Seg_1166" s="T52">"Da măn kundlaʔbəm koŋdə zolota". </ta>
            <ta e="T58" id="Seg_1167" s="T57">"Pʼerdə!" </ta>
            <ta e="T60" id="Seg_1168" s="T58">Dĭ pʼerbi. </ta>
            <ta e="T64" id="Seg_1169" s="T60">"No ĭmbi tănan mĭləj? </ta>
            <ta e="T67" id="Seg_1170" s="T64">Pʼeldə măna mĭleʔ!" </ta>
            <ta e="T68" id="Seg_1171" s="T67">"Mĭlem!" </ta>
            <ta e="T70" id="Seg_1172" s="T68">Dĭgəttə šobi. </ta>
            <ta e="T75" id="Seg_1173" s="T70">Măndə:" Măn tănan ĭmbileʔ (šobial). </ta>
            <ta e="T79" id="Seg_1174" s="T75">Măn tănan deʔpiem zolota". </ta>
            <ta e="T81" id="Seg_1175" s="T79">Dĭʔnə mĭbi. </ta>
            <ta e="T85" id="Seg_1176" s="T81">"Ĭmbi tănan (măn=) mĭzittə?" </ta>
            <ta e="T87" id="Seg_1177" s="T85">"Deʔ ipek!" </ta>
            <ta e="T91" id="Seg_1178" s="T87">Dĭʔnə deʔpiʔi sĭre ipek. </ta>
            <ta e="T96" id="Seg_1179" s="T91">"Măn dĭrgit ipek ej amniam. </ta>
            <ta e="T100" id="Seg_1180" s="T96">Măna nada sagər ipek". </ta>
            <ta e="T102" id="Seg_1181" s="T100">Kambiʔi nʼišən. </ta>
            <ta e="T106" id="Seg_1182" s="T102">Ibiʔi ipek, dĭ ambi. </ta>
            <ta e="T111" id="Seg_1183" s="T106">"A tüj ĭmbi tănan kereʔ?" </ta>
            <ta e="T113" id="Seg_1184" s="T111">Da kunolzittə. </ta>
            <ta e="T117" id="Seg_1185" s="T113">Dĭgəttə dĭʔnə deʔpiʔi pʼerinăʔi. </ta>
            <ta e="T122" id="Seg_1186" s="T117">Măndə: "Dĭn ej moliam kunolzittə. </ta>
            <ta e="T126" id="Seg_1187" s="T122">(Deʔ-) (Detəgeʔ) măna săloma". </ta>
            <ta e="T128" id="Seg_1188" s="T126">Dĭʔnə deʔpiʔi. </ta>
            <ta e="T131" id="Seg_1189" s="T128">Dĭ iʔbolaʔpi, kunollaʔpi. </ta>
            <ta e="T139" id="Seg_1190" s="T131">Dĭgəttə măndə:" Măn dĭn bar ej jakšə abiam. </ta>
            <ta e="T142" id="Seg_1191" s="T139">Deʔkeʔ măna rozgaʔi". </ta>
            <ta e="T147" id="Seg_1192" s="T142">Dĭgəttə deʔpiʔi rozgaʔi, münörzittə dĭm. </ta>
            <ta e="T154" id="Seg_1193" s="T147">Dĭ măndə: "Dĭn ajində tăn kuza nulaʔpi. </ta>
            <ta e="T159" id="Seg_1194" s="T154">Dĭ măndə: măna pʼeldə dettə". </ta>
            <ta e="T164" id="Seg_1195" s="T159">Dĭgəttə dĭm münörbiʔi, pʼeldə mĭbiʔi. </ta>
            <ta e="T168" id="Seg_1196" s="T164">Da bar dĭʔnə mĭgeʔi. </ta>
            <ta e="T172" id="Seg_1197" s="T168">Dĭgəttə bar dĭm münörluʔpi. </ta>
            <ta e="T179" id="Seg_1198" s="T172">A dĭ ibi üžübə da (nuʔməla-) nuʔməluʔpi. </ta>
            <ta e="T180" id="Seg_1199" s="T179">Kalla dʼürbi. </ta>
            <ta e="T181" id="Seg_1200" s="T180">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1201" s="T0">onʼiʔ</ta>
            <ta e="T2" id="Seg_1202" s="T1">kuza</ta>
            <ta e="T3" id="Seg_1203" s="T2">tĭl-bi</ta>
            <ta e="T4" id="Seg_1204" s="T3">dʼü</ta>
            <ta e="T5" id="Seg_1205" s="T4">i</ta>
            <ta e="T6" id="Seg_1206" s="T5">dĭn</ta>
            <ta e="T7" id="Seg_1207" s="T6">ku-bi</ta>
            <ta e="T8" id="Seg_1208" s="T7">zolota</ta>
            <ta e="T9" id="Seg_1209" s="T8">dĭgəttə</ta>
            <ta e="T10" id="Seg_1210" s="T9">măn-də</ta>
            <ta e="T11" id="Seg_1211" s="T10">gibər</ta>
            <ta e="T12" id="Seg_1212" s="T11">dĭ-m</ta>
            <ta e="T13" id="Seg_1213" s="T12">măna</ta>
            <ta e="T14" id="Seg_1214" s="T13">a-zittə</ta>
            <ta e="T15" id="Seg_1215" s="T14">măna</ta>
            <ta e="T16" id="Seg_1216" s="T15">kabar-laʔ</ta>
            <ta e="T17" id="Seg_1217" s="T16">i-lə-ʔi</ta>
            <ta e="T18" id="Seg_1218" s="T17">il</ta>
            <ta e="T19" id="Seg_1219" s="T18">dĭgəttə</ta>
            <ta e="T20" id="Seg_1220" s="T19">măn</ta>
            <ta e="T21" id="Seg_1221" s="T20">ka-la-m</ta>
            <ta e="T22" id="Seg_1222" s="T21">koŋ-də</ta>
            <ta e="T23" id="Seg_1223" s="T22">sad-də</ta>
            <ta e="T24" id="Seg_1224" s="T23">i</ta>
            <ta e="T25" id="Seg_1225" s="T24">mĭ-luʔ-lə-m</ta>
            <ta e="T26" id="Seg_1226" s="T25">dĭgəttə</ta>
            <ta e="T27" id="Seg_1227" s="T26">kam-bi</ta>
            <ta e="T28" id="Seg_1228" s="T27">kam-bi</ta>
            <ta e="T29" id="Seg_1229" s="T28">kandə-ga</ta>
            <ta e="T30" id="Seg_1230" s="T29">kandə-ga</ta>
            <ta e="T31" id="Seg_1231" s="T30">šo-bi</ta>
            <ta e="T32" id="Seg_1232" s="T31">dĭn</ta>
            <ta e="T33" id="Seg_1233" s="T32">dĭ-m</ta>
            <ta e="T34" id="Seg_1234" s="T33">ej</ta>
            <ta e="T35" id="Seg_1235" s="T34">öʔ-lie-ʔi</ta>
            <ta e="T36" id="Seg_1236" s="T35">ĭmbi-leʔ</ta>
            <ta e="T37" id="Seg_1237" s="T36">tăn</ta>
            <ta e="T38" id="Seg_1238" s="T37">šonəgan</ta>
            <ta e="T39" id="Seg_1239" s="T38">da</ta>
            <ta e="T40" id="Seg_1240" s="T39">kereʔ</ta>
            <ta e="T41" id="Seg_1241" s="T40">măna</ta>
            <ta e="T42" id="Seg_1242" s="T41">dĭgəttə</ta>
            <ta e="T43" id="Seg_1243" s="T42">öʔlüʔ-bi</ta>
            <ta e="T44" id="Seg_1244" s="T43">onʼiʔ</ta>
            <ta e="T45" id="Seg_1245" s="T44">teʔtə</ta>
            <ta e="T46" id="Seg_1246" s="T45">nagur</ta>
            <ta e="T47" id="Seg_1247" s="T46">dĭgəttə</ta>
            <ta e="T48" id="Seg_1248" s="T47">onʼiʔ</ta>
            <ta e="T49" id="Seg_1249" s="T48">nu-ga</ta>
            <ta e="T50" id="Seg_1250" s="T49">aji-ndə</ta>
            <ta e="T51" id="Seg_1251" s="T50">ĭmbi</ta>
            <ta e="T52" id="Seg_1252" s="T51">šonə-ga-l</ta>
            <ta e="T53" id="Seg_1253" s="T52">da</ta>
            <ta e="T54" id="Seg_1254" s="T53">măn</ta>
            <ta e="T55" id="Seg_1255" s="T54">kun-d-laʔbə-m</ta>
            <ta e="T56" id="Seg_1256" s="T55">koŋ-də</ta>
            <ta e="T57" id="Seg_1257" s="T56">zolota</ta>
            <ta e="T58" id="Seg_1258" s="T57">pʼer-də</ta>
            <ta e="T59" id="Seg_1259" s="T58">dĭ</ta>
            <ta e="T60" id="Seg_1260" s="T59">pʼer-bi</ta>
            <ta e="T61" id="Seg_1261" s="T60">no</ta>
            <ta e="T62" id="Seg_1262" s="T61">ĭmbi</ta>
            <ta e="T63" id="Seg_1263" s="T62">tănan</ta>
            <ta e="T64" id="Seg_1264" s="T63">mĭ-lə-j</ta>
            <ta e="T65" id="Seg_1265" s="T64">pʼel-də</ta>
            <ta e="T66" id="Seg_1266" s="T65">măna</ta>
            <ta e="T67" id="Seg_1267" s="T66">mĭ-leʔ</ta>
            <ta e="T68" id="Seg_1268" s="T67">mĭ-le-m</ta>
            <ta e="T69" id="Seg_1269" s="T68">dĭgəttə</ta>
            <ta e="T70" id="Seg_1270" s="T69">šo-bi</ta>
            <ta e="T71" id="Seg_1271" s="T70">măn-də</ta>
            <ta e="T72" id="Seg_1272" s="T71">măn</ta>
            <ta e="T73" id="Seg_1273" s="T72">tănan</ta>
            <ta e="T74" id="Seg_1274" s="T73">ĭmbi-leʔ</ta>
            <ta e="T75" id="Seg_1275" s="T74">šo-bia-l</ta>
            <ta e="T76" id="Seg_1276" s="T75">măn</ta>
            <ta e="T77" id="Seg_1277" s="T76">tănan</ta>
            <ta e="T78" id="Seg_1278" s="T77">deʔ-pie-m</ta>
            <ta e="T79" id="Seg_1279" s="T78">zolota</ta>
            <ta e="T80" id="Seg_1280" s="T79">dĭʔ-nə</ta>
            <ta e="T81" id="Seg_1281" s="T80">mĭ-bi</ta>
            <ta e="T82" id="Seg_1282" s="T81">ĭmbi</ta>
            <ta e="T83" id="Seg_1283" s="T82">tănan</ta>
            <ta e="T84" id="Seg_1284" s="T83">măn</ta>
            <ta e="T85" id="Seg_1285" s="T84">mĭ-zittə</ta>
            <ta e="T86" id="Seg_1286" s="T85">de-ʔ</ta>
            <ta e="T87" id="Seg_1287" s="T86">ipek</ta>
            <ta e="T88" id="Seg_1288" s="T87">dĭʔ-nə</ta>
            <ta e="T89" id="Seg_1289" s="T88">deʔ-pi-ʔi</ta>
            <ta e="T90" id="Seg_1290" s="T89">sĭre</ta>
            <ta e="T91" id="Seg_1291" s="T90">ipek</ta>
            <ta e="T92" id="Seg_1292" s="T91">măn</ta>
            <ta e="T93" id="Seg_1293" s="T92">dĭrgit</ta>
            <ta e="T94" id="Seg_1294" s="T93">ipek</ta>
            <ta e="T95" id="Seg_1295" s="T94">ej</ta>
            <ta e="T96" id="Seg_1296" s="T95">am-nia-m</ta>
            <ta e="T97" id="Seg_1297" s="T96">măna</ta>
            <ta e="T98" id="Seg_1298" s="T97">nada</ta>
            <ta e="T99" id="Seg_1299" s="T98">sagər</ta>
            <ta e="T100" id="Seg_1300" s="T99">ipek</ta>
            <ta e="T101" id="Seg_1301" s="T100">kam-bi-ʔi</ta>
            <ta e="T102" id="Seg_1302" s="T101">nʼišə-n</ta>
            <ta e="T103" id="Seg_1303" s="T102">i-bi-ʔi</ta>
            <ta e="T104" id="Seg_1304" s="T103">ipek</ta>
            <ta e="T105" id="Seg_1305" s="T104">dĭ</ta>
            <ta e="T106" id="Seg_1306" s="T105">am-bi</ta>
            <ta e="T107" id="Seg_1307" s="T106">a</ta>
            <ta e="T108" id="Seg_1308" s="T107">tüj</ta>
            <ta e="T109" id="Seg_1309" s="T108">ĭmbi</ta>
            <ta e="T110" id="Seg_1310" s="T109">tănan</ta>
            <ta e="T111" id="Seg_1311" s="T110">kereʔ</ta>
            <ta e="T112" id="Seg_1312" s="T111">da</ta>
            <ta e="T113" id="Seg_1313" s="T112">kunol-zittə</ta>
            <ta e="T114" id="Seg_1314" s="T113">dĭgəttə</ta>
            <ta e="T115" id="Seg_1315" s="T114">dĭʔ-nə</ta>
            <ta e="T116" id="Seg_1316" s="T115">deʔ-pi-ʔi</ta>
            <ta e="T117" id="Seg_1317" s="T116">pʼerină-ʔi</ta>
            <ta e="T118" id="Seg_1318" s="T117">măn-də</ta>
            <ta e="T119" id="Seg_1319" s="T118">dĭn</ta>
            <ta e="T120" id="Seg_1320" s="T119">ej</ta>
            <ta e="T121" id="Seg_1321" s="T120">mo-lia-m</ta>
            <ta e="T122" id="Seg_1322" s="T121">kunol-zittə</ta>
            <ta e="T124" id="Seg_1323" s="T123">detəgeʔ</ta>
            <ta e="T125" id="Seg_1324" s="T124">măna</ta>
            <ta e="T126" id="Seg_1325" s="T125">săloma</ta>
            <ta e="T127" id="Seg_1326" s="T126">dĭʔ-nə</ta>
            <ta e="T128" id="Seg_1327" s="T127">deʔ-pi-ʔi</ta>
            <ta e="T129" id="Seg_1328" s="T128">dĭ</ta>
            <ta e="T130" id="Seg_1329" s="T129">iʔbo-laʔ-pi</ta>
            <ta e="T131" id="Seg_1330" s="T130">kunol-laʔ-pi</ta>
            <ta e="T132" id="Seg_1331" s="T131">dĭgəttə</ta>
            <ta e="T133" id="Seg_1332" s="T132">măn-də</ta>
            <ta e="T134" id="Seg_1333" s="T133">măn</ta>
            <ta e="T135" id="Seg_1334" s="T134">dĭn</ta>
            <ta e="T136" id="Seg_1335" s="T135">bar</ta>
            <ta e="T137" id="Seg_1336" s="T136">ej</ta>
            <ta e="T138" id="Seg_1337" s="T137">jakšə</ta>
            <ta e="T139" id="Seg_1338" s="T138">a-bia-m</ta>
            <ta e="T140" id="Seg_1339" s="T139">deʔ-keʔ</ta>
            <ta e="T141" id="Seg_1340" s="T140">măna</ta>
            <ta e="T142" id="Seg_1341" s="T141">rozga-ʔi</ta>
            <ta e="T143" id="Seg_1342" s="T142">dĭgəttə</ta>
            <ta e="T144" id="Seg_1343" s="T143">deʔ-pi-ʔi</ta>
            <ta e="T145" id="Seg_1344" s="T144">rozga-ʔi</ta>
            <ta e="T146" id="Seg_1345" s="T145">münör-zittə</ta>
            <ta e="T147" id="Seg_1346" s="T146">dĭ-m</ta>
            <ta e="T148" id="Seg_1347" s="T147">dĭ</ta>
            <ta e="T149" id="Seg_1348" s="T148">măn-də</ta>
            <ta e="T150" id="Seg_1349" s="T149">dĭn</ta>
            <ta e="T151" id="Seg_1350" s="T150">aji-ndə</ta>
            <ta e="T152" id="Seg_1351" s="T151">tăn</ta>
            <ta e="T153" id="Seg_1352" s="T152">kuza</ta>
            <ta e="T154" id="Seg_1353" s="T153">nu-laʔ-pi</ta>
            <ta e="T155" id="Seg_1354" s="T154">dĭ</ta>
            <ta e="T156" id="Seg_1355" s="T155">măn-də</ta>
            <ta e="T157" id="Seg_1356" s="T156">măna</ta>
            <ta e="T158" id="Seg_1357" s="T157">pʼel-də</ta>
            <ta e="T159" id="Seg_1358" s="T158">det-tə</ta>
            <ta e="T160" id="Seg_1359" s="T159">dĭgəttə</ta>
            <ta e="T161" id="Seg_1360" s="T160">dĭ-m</ta>
            <ta e="T162" id="Seg_1361" s="T161">münör-bi-ʔi</ta>
            <ta e="T163" id="Seg_1362" s="T162">pʼel-də</ta>
            <ta e="T164" id="Seg_1363" s="T163">mĭ-bi-ʔi</ta>
            <ta e="T165" id="Seg_1364" s="T164">da</ta>
            <ta e="T166" id="Seg_1365" s="T165">bar</ta>
            <ta e="T167" id="Seg_1366" s="T166">dĭʔ-nə</ta>
            <ta e="T168" id="Seg_1367" s="T167">mĭ-ge-ʔi</ta>
            <ta e="T169" id="Seg_1368" s="T168">dĭgəttə</ta>
            <ta e="T170" id="Seg_1369" s="T169">bar</ta>
            <ta e="T171" id="Seg_1370" s="T170">dĭ-m</ta>
            <ta e="T172" id="Seg_1371" s="T171">münör-luʔ-pi</ta>
            <ta e="T173" id="Seg_1372" s="T172">a</ta>
            <ta e="T174" id="Seg_1373" s="T173">dĭ</ta>
            <ta e="T175" id="Seg_1374" s="T174">i-bi</ta>
            <ta e="T176" id="Seg_1375" s="T175">üžü-bə</ta>
            <ta e="T177" id="Seg_1376" s="T176">da</ta>
            <ta e="T179" id="Seg_1377" s="T178">nuʔmə-luʔ-pi</ta>
            <ta e="T182" id="Seg_1378" s="T179">kal-la</ta>
            <ta e="T180" id="Seg_1379" s="T182">dʼür-bi</ta>
            <ta e="T181" id="Seg_1380" s="T180">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1381" s="T0">onʼiʔ</ta>
            <ta e="T2" id="Seg_1382" s="T1">kuza</ta>
            <ta e="T3" id="Seg_1383" s="T2">tĭl-bi</ta>
            <ta e="T4" id="Seg_1384" s="T3">tʼo</ta>
            <ta e="T5" id="Seg_1385" s="T4">i</ta>
            <ta e="T6" id="Seg_1386" s="T5">dĭn</ta>
            <ta e="T7" id="Seg_1387" s="T6">ku-bi</ta>
            <ta e="T8" id="Seg_1388" s="T7">zolota</ta>
            <ta e="T9" id="Seg_1389" s="T8">dĭgəttə</ta>
            <ta e="T10" id="Seg_1390" s="T9">măn-ntə</ta>
            <ta e="T11" id="Seg_1391" s="T10">gibər</ta>
            <ta e="T12" id="Seg_1392" s="T11">dĭ-m</ta>
            <ta e="T13" id="Seg_1393" s="T12">măna</ta>
            <ta e="T14" id="Seg_1394" s="T13">a-zittə</ta>
            <ta e="T15" id="Seg_1395" s="T14">măna</ta>
            <ta e="T16" id="Seg_1396" s="T15">kabar-lAʔ</ta>
            <ta e="T17" id="Seg_1397" s="T16">i-lV-jəʔ</ta>
            <ta e="T18" id="Seg_1398" s="T17">il</ta>
            <ta e="T19" id="Seg_1399" s="T18">dĭgəttə</ta>
            <ta e="T20" id="Seg_1400" s="T19">măn</ta>
            <ta e="T21" id="Seg_1401" s="T20">kan-lV-m</ta>
            <ta e="T22" id="Seg_1402" s="T21">koŋ-Tə</ta>
            <ta e="T23" id="Seg_1403" s="T22">sad-Tə</ta>
            <ta e="T24" id="Seg_1404" s="T23">i</ta>
            <ta e="T25" id="Seg_1405" s="T24">mĭ-luʔbdə-lV-m</ta>
            <ta e="T26" id="Seg_1406" s="T25">dĭgəttə</ta>
            <ta e="T27" id="Seg_1407" s="T26">kan-bi</ta>
            <ta e="T28" id="Seg_1408" s="T27">kan-bi</ta>
            <ta e="T29" id="Seg_1409" s="T28">kandə-gA</ta>
            <ta e="T30" id="Seg_1410" s="T29">kandə-gA</ta>
            <ta e="T31" id="Seg_1411" s="T30">šo-bi</ta>
            <ta e="T32" id="Seg_1412" s="T31">dĭn</ta>
            <ta e="T33" id="Seg_1413" s="T32">dĭ-m</ta>
            <ta e="T34" id="Seg_1414" s="T33">ej</ta>
            <ta e="T35" id="Seg_1415" s="T34">öʔ-liA-jəʔ</ta>
            <ta e="T36" id="Seg_1416" s="T35">ĭmbi-%%</ta>
            <ta e="T37" id="Seg_1417" s="T36">tăn</ta>
            <ta e="T39" id="Seg_1418" s="T38">da</ta>
            <ta e="T40" id="Seg_1419" s="T39">kereʔ</ta>
            <ta e="T41" id="Seg_1420" s="T40">măna</ta>
            <ta e="T42" id="Seg_1421" s="T41">dĭgəttə</ta>
            <ta e="T43" id="Seg_1422" s="T42">öʔlüʔ-bi</ta>
            <ta e="T44" id="Seg_1423" s="T43">onʼiʔ</ta>
            <ta e="T45" id="Seg_1424" s="T44">teʔdə</ta>
            <ta e="T46" id="Seg_1425" s="T45">nagur</ta>
            <ta e="T47" id="Seg_1426" s="T46">dĭgəttə</ta>
            <ta e="T48" id="Seg_1427" s="T47">onʼiʔ</ta>
            <ta e="T49" id="Seg_1428" s="T48">nu-gA</ta>
            <ta e="T50" id="Seg_1429" s="T49">ajə-gəndə</ta>
            <ta e="T51" id="Seg_1430" s="T50">ĭmbi</ta>
            <ta e="T52" id="Seg_1431" s="T51">šonə-gA-l</ta>
            <ta e="T53" id="Seg_1432" s="T52">da</ta>
            <ta e="T54" id="Seg_1433" s="T53">măn</ta>
            <ta e="T55" id="Seg_1434" s="T54">kun-ntə-laʔbə-m</ta>
            <ta e="T56" id="Seg_1435" s="T55">koŋ-Tə</ta>
            <ta e="T57" id="Seg_1436" s="T56">zolota</ta>
            <ta e="T58" id="Seg_1437" s="T57">pʼer-t</ta>
            <ta e="T59" id="Seg_1438" s="T58">dĭ</ta>
            <ta e="T60" id="Seg_1439" s="T59">pʼer-bi</ta>
            <ta e="T61" id="Seg_1440" s="T60">no</ta>
            <ta e="T62" id="Seg_1441" s="T61">ĭmbi</ta>
            <ta e="T63" id="Seg_1442" s="T62">tănan</ta>
            <ta e="T64" id="Seg_1443" s="T63">mĭ-lV-j</ta>
            <ta e="T65" id="Seg_1444" s="T64">pʼel-də</ta>
            <ta e="T66" id="Seg_1445" s="T65">măna</ta>
            <ta e="T67" id="Seg_1446" s="T66">mĭ-lAʔ</ta>
            <ta e="T68" id="Seg_1447" s="T67">mĭ-lV-m</ta>
            <ta e="T69" id="Seg_1448" s="T68">dĭgəttə</ta>
            <ta e="T70" id="Seg_1449" s="T69">šo-bi</ta>
            <ta e="T71" id="Seg_1450" s="T70">măn-ntə</ta>
            <ta e="T72" id="Seg_1451" s="T71">măn</ta>
            <ta e="T73" id="Seg_1452" s="T72">tănan</ta>
            <ta e="T74" id="Seg_1453" s="T73">ĭmbi-lAʔ</ta>
            <ta e="T75" id="Seg_1454" s="T74">šo-bi-l</ta>
            <ta e="T76" id="Seg_1455" s="T75">măn</ta>
            <ta e="T77" id="Seg_1456" s="T76">tănan</ta>
            <ta e="T78" id="Seg_1457" s="T77">det-bi-m</ta>
            <ta e="T79" id="Seg_1458" s="T78">zolota</ta>
            <ta e="T80" id="Seg_1459" s="T79">dĭ-Tə</ta>
            <ta e="T81" id="Seg_1460" s="T80">mĭ-bi</ta>
            <ta e="T82" id="Seg_1461" s="T81">ĭmbi</ta>
            <ta e="T83" id="Seg_1462" s="T82">tănan</ta>
            <ta e="T84" id="Seg_1463" s="T83">măn</ta>
            <ta e="T85" id="Seg_1464" s="T84">mĭ-zittə</ta>
            <ta e="T86" id="Seg_1465" s="T85">det-ʔ</ta>
            <ta e="T87" id="Seg_1466" s="T86">ipek</ta>
            <ta e="T88" id="Seg_1467" s="T87">dĭ-Tə</ta>
            <ta e="T89" id="Seg_1468" s="T88">det-bi-jəʔ</ta>
            <ta e="T90" id="Seg_1469" s="T89">sĭri</ta>
            <ta e="T91" id="Seg_1470" s="T90">ipek</ta>
            <ta e="T92" id="Seg_1471" s="T91">măn</ta>
            <ta e="T93" id="Seg_1472" s="T92">dĭrgit</ta>
            <ta e="T94" id="Seg_1473" s="T93">ipek</ta>
            <ta e="T95" id="Seg_1474" s="T94">ej</ta>
            <ta e="T96" id="Seg_1475" s="T95">am-liA-m</ta>
            <ta e="T97" id="Seg_1476" s="T96">măna</ta>
            <ta e="T98" id="Seg_1477" s="T97">nadə</ta>
            <ta e="T99" id="Seg_1478" s="T98">sagər</ta>
            <ta e="T100" id="Seg_1479" s="T99">ipek</ta>
            <ta e="T101" id="Seg_1480" s="T100">kan-bi-jəʔ</ta>
            <ta e="T102" id="Seg_1481" s="T101">nʼišə-n</ta>
            <ta e="T103" id="Seg_1482" s="T102">i-bi-jəʔ</ta>
            <ta e="T104" id="Seg_1483" s="T103">ipek</ta>
            <ta e="T105" id="Seg_1484" s="T104">dĭ</ta>
            <ta e="T106" id="Seg_1485" s="T105">am-bi</ta>
            <ta e="T107" id="Seg_1486" s="T106">a</ta>
            <ta e="T108" id="Seg_1487" s="T107">tüj</ta>
            <ta e="T109" id="Seg_1488" s="T108">ĭmbi</ta>
            <ta e="T110" id="Seg_1489" s="T109">tănan</ta>
            <ta e="T111" id="Seg_1490" s="T110">kereʔ</ta>
            <ta e="T112" id="Seg_1491" s="T111">da</ta>
            <ta e="T113" id="Seg_1492" s="T112">kunol-zittə</ta>
            <ta e="T114" id="Seg_1493" s="T113">dĭgəttə</ta>
            <ta e="T115" id="Seg_1494" s="T114">dĭ-Tə</ta>
            <ta e="T116" id="Seg_1495" s="T115">det-bi-jəʔ</ta>
            <ta e="T117" id="Seg_1496" s="T116">pʼerină-jəʔ</ta>
            <ta e="T118" id="Seg_1497" s="T117">măn-ntə</ta>
            <ta e="T119" id="Seg_1498" s="T118">dĭn</ta>
            <ta e="T120" id="Seg_1499" s="T119">ej</ta>
            <ta e="T121" id="Seg_1500" s="T120">mo-liA-m</ta>
            <ta e="T122" id="Seg_1501" s="T121">kunol-zittə</ta>
            <ta e="T125" id="Seg_1502" s="T124">măna</ta>
            <ta e="T126" id="Seg_1503" s="T125">săloma</ta>
            <ta e="T127" id="Seg_1504" s="T126">dĭ-Tə</ta>
            <ta e="T128" id="Seg_1505" s="T127">det-bi-jəʔ</ta>
            <ta e="T129" id="Seg_1506" s="T128">dĭ</ta>
            <ta e="T130" id="Seg_1507" s="T129">iʔbö-laʔbə-bi</ta>
            <ta e="T131" id="Seg_1508" s="T130">kunol-laʔbə-bi</ta>
            <ta e="T132" id="Seg_1509" s="T131">dĭgəttə</ta>
            <ta e="T133" id="Seg_1510" s="T132">măn-ntə</ta>
            <ta e="T134" id="Seg_1511" s="T133">măn</ta>
            <ta e="T135" id="Seg_1512" s="T134">dĭn</ta>
            <ta e="T136" id="Seg_1513" s="T135">bar</ta>
            <ta e="T137" id="Seg_1514" s="T136">ej</ta>
            <ta e="T138" id="Seg_1515" s="T137">jakšə</ta>
            <ta e="T139" id="Seg_1516" s="T138">a-bi-m</ta>
            <ta e="T140" id="Seg_1517" s="T139">det-KAʔ</ta>
            <ta e="T141" id="Seg_1518" s="T140">măna</ta>
            <ta e="T142" id="Seg_1519" s="T141">rozga-jəʔ</ta>
            <ta e="T143" id="Seg_1520" s="T142">dĭgəttə</ta>
            <ta e="T144" id="Seg_1521" s="T143">det-bi-jəʔ</ta>
            <ta e="T145" id="Seg_1522" s="T144">rozga-jəʔ</ta>
            <ta e="T146" id="Seg_1523" s="T145">münör-zittə</ta>
            <ta e="T147" id="Seg_1524" s="T146">dĭ-m</ta>
            <ta e="T148" id="Seg_1525" s="T147">dĭ</ta>
            <ta e="T149" id="Seg_1526" s="T148">măn-ntə</ta>
            <ta e="T150" id="Seg_1527" s="T149">dĭn</ta>
            <ta e="T151" id="Seg_1528" s="T150">ajə-gəndə</ta>
            <ta e="T152" id="Seg_1529" s="T151">tăn</ta>
            <ta e="T153" id="Seg_1530" s="T152">kuza</ta>
            <ta e="T154" id="Seg_1531" s="T153">nu-lAʔ-bi</ta>
            <ta e="T155" id="Seg_1532" s="T154">dĭ</ta>
            <ta e="T156" id="Seg_1533" s="T155">măn-ntə</ta>
            <ta e="T157" id="Seg_1534" s="T156">măna</ta>
            <ta e="T158" id="Seg_1535" s="T157">pʼel-də</ta>
            <ta e="T159" id="Seg_1536" s="T158">det-t</ta>
            <ta e="T160" id="Seg_1537" s="T159">dĭgəttə</ta>
            <ta e="T161" id="Seg_1538" s="T160">dĭ-m</ta>
            <ta e="T162" id="Seg_1539" s="T161">münör-bi-jəʔ</ta>
            <ta e="T163" id="Seg_1540" s="T162">pʼel-də</ta>
            <ta e="T164" id="Seg_1541" s="T163">mĭ-bi-jəʔ</ta>
            <ta e="T165" id="Seg_1542" s="T164">da</ta>
            <ta e="T166" id="Seg_1543" s="T165">bar</ta>
            <ta e="T167" id="Seg_1544" s="T166">dĭ-Tə</ta>
            <ta e="T168" id="Seg_1545" s="T167">mĭ-gA-jəʔ</ta>
            <ta e="T169" id="Seg_1546" s="T168">dĭgəttə</ta>
            <ta e="T170" id="Seg_1547" s="T169">bar</ta>
            <ta e="T171" id="Seg_1548" s="T170">dĭ-m</ta>
            <ta e="T172" id="Seg_1549" s="T171">münör-luʔbdə-bi</ta>
            <ta e="T173" id="Seg_1550" s="T172">a</ta>
            <ta e="T174" id="Seg_1551" s="T173">dĭ</ta>
            <ta e="T175" id="Seg_1552" s="T174">i-bi</ta>
            <ta e="T176" id="Seg_1553" s="T175">üžü-bə</ta>
            <ta e="T177" id="Seg_1554" s="T176">da</ta>
            <ta e="T179" id="Seg_1555" s="T178">nuʔmə-luʔbdə-bi</ta>
            <ta e="T182" id="Seg_1556" s="T179">kan-lAʔ</ta>
            <ta e="T180" id="Seg_1557" s="T182">tʼür-bi</ta>
            <ta e="T181" id="Seg_1558" s="T180">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1559" s="T0">one.[NOM.SG]</ta>
            <ta e="T2" id="Seg_1560" s="T1">man.[NOM.SG]</ta>
            <ta e="T3" id="Seg_1561" s="T2">dig-PST.[3SG]</ta>
            <ta e="T4" id="Seg_1562" s="T3">earth.[NOM.SG]</ta>
            <ta e="T5" id="Seg_1563" s="T4">and</ta>
            <ta e="T6" id="Seg_1564" s="T5">there</ta>
            <ta e="T7" id="Seg_1565" s="T6">find-PST.[3SG]</ta>
            <ta e="T8" id="Seg_1566" s="T7">gold.[NOM.SG]</ta>
            <ta e="T9" id="Seg_1567" s="T8">then</ta>
            <ta e="T10" id="Seg_1568" s="T9">say-IPFVZ.[3SG]</ta>
            <ta e="T11" id="Seg_1569" s="T10">where.to</ta>
            <ta e="T12" id="Seg_1570" s="T11">this-ACC</ta>
            <ta e="T13" id="Seg_1571" s="T12">I.LAT</ta>
            <ta e="T14" id="Seg_1572" s="T13">make-INF.LAT</ta>
            <ta e="T15" id="Seg_1573" s="T14">I.LAT</ta>
            <ta e="T16" id="Seg_1574" s="T15">grab-CVB</ta>
            <ta e="T17" id="Seg_1575" s="T16">take-FUT-3PL</ta>
            <ta e="T18" id="Seg_1576" s="T17">people.[NOM.SG]</ta>
            <ta e="T19" id="Seg_1577" s="T18">then</ta>
            <ta e="T20" id="Seg_1578" s="T19">I.NOM</ta>
            <ta e="T21" id="Seg_1579" s="T20">go-FUT-1SG</ta>
            <ta e="T22" id="Seg_1580" s="T21">chief-LAT</ta>
            <ta e="T23" id="Seg_1581" s="T22">garder-LAT</ta>
            <ta e="T24" id="Seg_1582" s="T23">and</ta>
            <ta e="T25" id="Seg_1583" s="T24">give-MOM-FUT-1SG</ta>
            <ta e="T26" id="Seg_1584" s="T25">then</ta>
            <ta e="T27" id="Seg_1585" s="T26">go-PST.[3SG]</ta>
            <ta e="T28" id="Seg_1586" s="T27">go-PST.[3SG]</ta>
            <ta e="T29" id="Seg_1587" s="T28">walk-PRS.[3SG]</ta>
            <ta e="T30" id="Seg_1588" s="T29">walk-PRS.[3SG]</ta>
            <ta e="T31" id="Seg_1589" s="T30">come-PST.[3SG]</ta>
            <ta e="T32" id="Seg_1590" s="T31">there</ta>
            <ta e="T33" id="Seg_1591" s="T32">this-ACC</ta>
            <ta e="T34" id="Seg_1592" s="T33">NEG</ta>
            <ta e="T35" id="Seg_1593" s="T34">let-PRS-3PL</ta>
            <ta e="T36" id="Seg_1594" s="T35">what-%%</ta>
            <ta e="T37" id="Seg_1595" s="T36">you.NOM</ta>
            <ta e="T39" id="Seg_1596" s="T38">because</ta>
            <ta e="T40" id="Seg_1597" s="T39">one.needs</ta>
            <ta e="T41" id="Seg_1598" s="T40">I.LAT</ta>
            <ta e="T42" id="Seg_1599" s="T41">then</ta>
            <ta e="T43" id="Seg_1600" s="T42">%%-PST.[3SG]</ta>
            <ta e="T44" id="Seg_1601" s="T43">one.[NOM.SG]</ta>
            <ta e="T45" id="Seg_1602" s="T44">four.[NOM.SG]</ta>
            <ta e="T46" id="Seg_1603" s="T45">three.[NOM.SG]</ta>
            <ta e="T47" id="Seg_1604" s="T46">then</ta>
            <ta e="T48" id="Seg_1605" s="T47">one.[NOM.SG]</ta>
            <ta e="T49" id="Seg_1606" s="T48">stand-PRS.[3SG]</ta>
            <ta e="T50" id="Seg_1607" s="T49">door-LAT/LOC.3SG</ta>
            <ta e="T51" id="Seg_1608" s="T50">what.[NOM.SG]</ta>
            <ta e="T52" id="Seg_1609" s="T51">come-PRS-2SG</ta>
            <ta e="T53" id="Seg_1610" s="T52">because</ta>
            <ta e="T54" id="Seg_1611" s="T53">I.NOM</ta>
            <ta e="T55" id="Seg_1612" s="T54">bring-IPFVZ-DUR-1SG</ta>
            <ta e="T56" id="Seg_1613" s="T55">chief-LAT</ta>
            <ta e="T57" id="Seg_1614" s="T56">gold.[NOM.SG]</ta>
            <ta e="T58" id="Seg_1615" s="T57">show-IMP.2SG.O</ta>
            <ta e="T59" id="Seg_1616" s="T58">this.[NOM.SG]</ta>
            <ta e="T60" id="Seg_1617" s="T59">show-PST.[3SG]</ta>
            <ta e="T61" id="Seg_1618" s="T60">well</ta>
            <ta e="T62" id="Seg_1619" s="T61">what.[NOM.SG]</ta>
            <ta e="T63" id="Seg_1620" s="T62">you.DAT</ta>
            <ta e="T64" id="Seg_1621" s="T63">give-FUT-3SG</ta>
            <ta e="T65" id="Seg_1622" s="T64">half-NOM/GEN/ACC.3SG</ta>
            <ta e="T66" id="Seg_1623" s="T65">I.LAT</ta>
            <ta e="T67" id="Seg_1624" s="T66">give-2PL</ta>
            <ta e="T68" id="Seg_1625" s="T67">give-FUT-1SG</ta>
            <ta e="T69" id="Seg_1626" s="T68">then</ta>
            <ta e="T70" id="Seg_1627" s="T69">come-PST.[3SG]</ta>
            <ta e="T71" id="Seg_1628" s="T70">say-IPFVZ.[3SG]</ta>
            <ta e="T72" id="Seg_1629" s="T71">I.NOM</ta>
            <ta e="T73" id="Seg_1630" s="T72">you.DAT</ta>
            <ta e="T74" id="Seg_1631" s="T73">what-NOM/GEN/ACC.2PL</ta>
            <ta e="T75" id="Seg_1632" s="T74">come-PST-2SG</ta>
            <ta e="T76" id="Seg_1633" s="T75">I.NOM</ta>
            <ta e="T77" id="Seg_1634" s="T76">you.DAT</ta>
            <ta e="T78" id="Seg_1635" s="T77">bring-PST-1SG</ta>
            <ta e="T79" id="Seg_1636" s="T78">gold.[NOM.SG]</ta>
            <ta e="T80" id="Seg_1637" s="T79">this-LAT</ta>
            <ta e="T81" id="Seg_1638" s="T80">give-PST.[3SG]</ta>
            <ta e="T82" id="Seg_1639" s="T81">what.[NOM.SG]</ta>
            <ta e="T83" id="Seg_1640" s="T82">you.DAT</ta>
            <ta e="T84" id="Seg_1641" s="T83">I.NOM</ta>
            <ta e="T85" id="Seg_1642" s="T84">give-INF.LAT</ta>
            <ta e="T86" id="Seg_1643" s="T85">bring-IMP.2SG</ta>
            <ta e="T87" id="Seg_1644" s="T86">bread.[NOM.SG]</ta>
            <ta e="T88" id="Seg_1645" s="T87">this-LAT</ta>
            <ta e="T89" id="Seg_1646" s="T88">bring-PST-3PL</ta>
            <ta e="T90" id="Seg_1647" s="T89">snow.[NOM.SG]</ta>
            <ta e="T91" id="Seg_1648" s="T90">bread.[NOM.SG]</ta>
            <ta e="T92" id="Seg_1649" s="T91">I.NOM</ta>
            <ta e="T93" id="Seg_1650" s="T92">such.[NOM.SG]</ta>
            <ta e="T94" id="Seg_1651" s="T93">bread.[NOM.SG]</ta>
            <ta e="T95" id="Seg_1652" s="T94">NEG</ta>
            <ta e="T96" id="Seg_1653" s="T95">eat-PRS-1SG</ta>
            <ta e="T97" id="Seg_1654" s="T96">I.LAT</ta>
            <ta e="T98" id="Seg_1655" s="T97">one.should</ta>
            <ta e="T99" id="Seg_1656" s="T98">black.[NOM.SG]</ta>
            <ta e="T100" id="Seg_1657" s="T99">bread.[NOM.SG]</ta>
            <ta e="T101" id="Seg_1658" s="T100">go-PST-3PL</ta>
            <ta e="T102" id="Seg_1659" s="T101">beggar-GEN</ta>
            <ta e="T103" id="Seg_1660" s="T102">take-PST-3PL</ta>
            <ta e="T104" id="Seg_1661" s="T103">bread.[NOM.SG]</ta>
            <ta e="T105" id="Seg_1662" s="T104">this.[NOM.SG]</ta>
            <ta e="T106" id="Seg_1663" s="T105">eat-PST.[3SG]</ta>
            <ta e="T107" id="Seg_1664" s="T106">and</ta>
            <ta e="T108" id="Seg_1665" s="T107">now</ta>
            <ta e="T109" id="Seg_1666" s="T108">what.[NOM.SG]</ta>
            <ta e="T110" id="Seg_1667" s="T109">you.DAT</ta>
            <ta e="T111" id="Seg_1668" s="T110">one.needs</ta>
            <ta e="T112" id="Seg_1669" s="T111">PTCL</ta>
            <ta e="T113" id="Seg_1670" s="T112">sleep-INF.LAT</ta>
            <ta e="T114" id="Seg_1671" s="T113">then</ta>
            <ta e="T115" id="Seg_1672" s="T114">this-LAT</ta>
            <ta e="T116" id="Seg_1673" s="T115">bring-PST-3PL</ta>
            <ta e="T117" id="Seg_1674" s="T116">feather.bed-PL</ta>
            <ta e="T118" id="Seg_1675" s="T117">say-IPFVZ.[3SG]</ta>
            <ta e="T119" id="Seg_1676" s="T118">there</ta>
            <ta e="T120" id="Seg_1677" s="T119">NEG</ta>
            <ta e="T121" id="Seg_1678" s="T120">can-PRS-1SG</ta>
            <ta e="T122" id="Seg_1679" s="T121">sleep-INF.LAT</ta>
            <ta e="T125" id="Seg_1680" s="T124">I.LAT</ta>
            <ta e="T126" id="Seg_1681" s="T125">straw.[NOM.SG]</ta>
            <ta e="T127" id="Seg_1682" s="T126">this-LAT</ta>
            <ta e="T128" id="Seg_1683" s="T127">bring-PST-3PL</ta>
            <ta e="T129" id="Seg_1684" s="T128">this.[NOM.SG]</ta>
            <ta e="T130" id="Seg_1685" s="T129">lie-DUR-PST.[3SG]</ta>
            <ta e="T131" id="Seg_1686" s="T130">sleep-DUR-PST.[3SG]</ta>
            <ta e="T132" id="Seg_1687" s="T131">then</ta>
            <ta e="T133" id="Seg_1688" s="T132">say-IPFVZ.[3SG]</ta>
            <ta e="T134" id="Seg_1689" s="T133">I.NOM</ta>
            <ta e="T135" id="Seg_1690" s="T134">there</ta>
            <ta e="T136" id="Seg_1691" s="T135">all</ta>
            <ta e="T137" id="Seg_1692" s="T136">NEG</ta>
            <ta e="T138" id="Seg_1693" s="T137">good.[NOM.SG]</ta>
            <ta e="T139" id="Seg_1694" s="T138">make-PST-1SG</ta>
            <ta e="T140" id="Seg_1695" s="T139">bring-IMP.2PL</ta>
            <ta e="T141" id="Seg_1696" s="T140">I.LAT</ta>
            <ta e="T142" id="Seg_1697" s="T141">scourge-NOM/GEN/ACC.3PL</ta>
            <ta e="T143" id="Seg_1698" s="T142">then</ta>
            <ta e="T144" id="Seg_1699" s="T143">bring-PST-3PL</ta>
            <ta e="T145" id="Seg_1700" s="T144">scourge-NOM/GEN/ACC.3PL</ta>
            <ta e="T146" id="Seg_1701" s="T145">beat-INF.LAT</ta>
            <ta e="T147" id="Seg_1702" s="T146">this-ACC</ta>
            <ta e="T148" id="Seg_1703" s="T147">this.[NOM.SG]</ta>
            <ta e="T149" id="Seg_1704" s="T148">say-IPFVZ.[3SG]</ta>
            <ta e="T150" id="Seg_1705" s="T149">there</ta>
            <ta e="T151" id="Seg_1706" s="T150">door-LAT/LOC.3SG</ta>
            <ta e="T152" id="Seg_1707" s="T151">you.NOM</ta>
            <ta e="T153" id="Seg_1708" s="T152">man.[NOM.SG]</ta>
            <ta e="T154" id="Seg_1709" s="T153">stand-CVB-PST.[3SG]</ta>
            <ta e="T155" id="Seg_1710" s="T154">this.[NOM.SG]</ta>
            <ta e="T156" id="Seg_1711" s="T155">say-IPFVZ.[3SG]</ta>
            <ta e="T157" id="Seg_1712" s="T156">I.LAT</ta>
            <ta e="T158" id="Seg_1713" s="T157">half-NOM/GEN/ACC.3SG</ta>
            <ta e="T159" id="Seg_1714" s="T158">bring-IMP.2SG.O</ta>
            <ta e="T160" id="Seg_1715" s="T159">then</ta>
            <ta e="T161" id="Seg_1716" s="T160">this-ACC</ta>
            <ta e="T162" id="Seg_1717" s="T161">beat-PST-3PL</ta>
            <ta e="T163" id="Seg_1718" s="T162">half-NOM/GEN/ACC.3SG</ta>
            <ta e="T164" id="Seg_1719" s="T163">give-PST-3PL</ta>
            <ta e="T165" id="Seg_1720" s="T164">and</ta>
            <ta e="T166" id="Seg_1721" s="T165">all</ta>
            <ta e="T167" id="Seg_1722" s="T166">this-LAT</ta>
            <ta e="T168" id="Seg_1723" s="T167">give-PRS-3PL</ta>
            <ta e="T169" id="Seg_1724" s="T168">then</ta>
            <ta e="T170" id="Seg_1725" s="T169">PTCL</ta>
            <ta e="T171" id="Seg_1726" s="T170">this-ACC</ta>
            <ta e="T172" id="Seg_1727" s="T171">beat-MOM-PST.[3SG]</ta>
            <ta e="T173" id="Seg_1728" s="T172">and</ta>
            <ta e="T174" id="Seg_1729" s="T173">this.[NOM.SG]</ta>
            <ta e="T175" id="Seg_1730" s="T174">take-PST.[3SG]</ta>
            <ta e="T176" id="Seg_1731" s="T175">cap-ACC.3SG</ta>
            <ta e="T177" id="Seg_1732" s="T176">and</ta>
            <ta e="T179" id="Seg_1733" s="T178">run-MOM-PST.[3SG]</ta>
            <ta e="T182" id="Seg_1734" s="T179">go-CVB</ta>
            <ta e="T180" id="Seg_1735" s="T182">disappear-PST.[3SG]</ta>
            <ta e="T181" id="Seg_1736" s="T180">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1737" s="T0">один.[NOM.SG]</ta>
            <ta e="T2" id="Seg_1738" s="T1">мужчина.[NOM.SG]</ta>
            <ta e="T3" id="Seg_1739" s="T2">копать-PST.[3SG]</ta>
            <ta e="T4" id="Seg_1740" s="T3">земля.[NOM.SG]</ta>
            <ta e="T5" id="Seg_1741" s="T4">и</ta>
            <ta e="T6" id="Seg_1742" s="T5">там</ta>
            <ta e="T7" id="Seg_1743" s="T6">найти-PST.[3SG]</ta>
            <ta e="T8" id="Seg_1744" s="T7">золото.[NOM.SG]</ta>
            <ta e="T9" id="Seg_1745" s="T8">тогда</ta>
            <ta e="T10" id="Seg_1746" s="T9">сказать-IPFVZ.[3SG]</ta>
            <ta e="T11" id="Seg_1747" s="T10">куда</ta>
            <ta e="T12" id="Seg_1748" s="T11">этот-ACC</ta>
            <ta e="T13" id="Seg_1749" s="T12">я.LAT</ta>
            <ta e="T14" id="Seg_1750" s="T13">делать-INF.LAT</ta>
            <ta e="T15" id="Seg_1751" s="T14">я.LAT</ta>
            <ta e="T16" id="Seg_1752" s="T15">хватать-CVB</ta>
            <ta e="T17" id="Seg_1753" s="T16">взять-FUT-3PL</ta>
            <ta e="T18" id="Seg_1754" s="T17">люди.[NOM.SG]</ta>
            <ta e="T19" id="Seg_1755" s="T18">тогда</ta>
            <ta e="T20" id="Seg_1756" s="T19">я.NOM</ta>
            <ta e="T21" id="Seg_1757" s="T20">пойти-FUT-1SG</ta>
            <ta e="T22" id="Seg_1758" s="T21">вождь-LAT</ta>
            <ta e="T23" id="Seg_1759" s="T22">сад-LAT</ta>
            <ta e="T24" id="Seg_1760" s="T23">и</ta>
            <ta e="T25" id="Seg_1761" s="T24">дать-MOM-FUT-1SG</ta>
            <ta e="T26" id="Seg_1762" s="T25">тогда</ta>
            <ta e="T27" id="Seg_1763" s="T26">пойти-PST.[3SG]</ta>
            <ta e="T28" id="Seg_1764" s="T27">пойти-PST.[3SG]</ta>
            <ta e="T29" id="Seg_1765" s="T28">идти-PRS.[3SG]</ta>
            <ta e="T30" id="Seg_1766" s="T29">идти-PRS.[3SG]</ta>
            <ta e="T31" id="Seg_1767" s="T30">прийти-PST.[3SG]</ta>
            <ta e="T32" id="Seg_1768" s="T31">там</ta>
            <ta e="T33" id="Seg_1769" s="T32">этот-ACC</ta>
            <ta e="T34" id="Seg_1770" s="T33">NEG</ta>
            <ta e="T35" id="Seg_1771" s="T34">пускать-PRS-3PL</ta>
            <ta e="T36" id="Seg_1772" s="T35">что-%%</ta>
            <ta e="T37" id="Seg_1773" s="T36">ты.NOM</ta>
            <ta e="T39" id="Seg_1774" s="T38">да</ta>
            <ta e="T40" id="Seg_1775" s="T39">нужно</ta>
            <ta e="T41" id="Seg_1776" s="T40">я.LAT</ta>
            <ta e="T42" id="Seg_1777" s="T41">тогда</ta>
            <ta e="T43" id="Seg_1778" s="T42">%%-PST.[3SG]</ta>
            <ta e="T44" id="Seg_1779" s="T43">один.[NOM.SG]</ta>
            <ta e="T45" id="Seg_1780" s="T44">четыре.[NOM.SG]</ta>
            <ta e="T46" id="Seg_1781" s="T45">три.[NOM.SG]</ta>
            <ta e="T47" id="Seg_1782" s="T46">тогда</ta>
            <ta e="T48" id="Seg_1783" s="T47">один.[NOM.SG]</ta>
            <ta e="T49" id="Seg_1784" s="T48">стоять-PRS.[3SG]</ta>
            <ta e="T50" id="Seg_1785" s="T49">дверь-LAT/LOC.3SG</ta>
            <ta e="T51" id="Seg_1786" s="T50">что.[NOM.SG]</ta>
            <ta e="T52" id="Seg_1787" s="T51">прийти-PRS-2SG</ta>
            <ta e="T53" id="Seg_1788" s="T52">да</ta>
            <ta e="T54" id="Seg_1789" s="T53">я.NOM</ta>
            <ta e="T55" id="Seg_1790" s="T54">нести-IPFVZ-DUR-1SG</ta>
            <ta e="T56" id="Seg_1791" s="T55">вождь-LAT</ta>
            <ta e="T57" id="Seg_1792" s="T56">золото.[NOM.SG]</ta>
            <ta e="T58" id="Seg_1793" s="T57">показать-IMP.2SG.O</ta>
            <ta e="T59" id="Seg_1794" s="T58">этот.[NOM.SG]</ta>
            <ta e="T60" id="Seg_1795" s="T59">показать-PST.[3SG]</ta>
            <ta e="T61" id="Seg_1796" s="T60">ну</ta>
            <ta e="T62" id="Seg_1797" s="T61">что.[NOM.SG]</ta>
            <ta e="T63" id="Seg_1798" s="T62">ты.DAT</ta>
            <ta e="T64" id="Seg_1799" s="T63">дать-FUT-3SG</ta>
            <ta e="T65" id="Seg_1800" s="T64">половина-NOM/GEN/ACC.3SG</ta>
            <ta e="T66" id="Seg_1801" s="T65">я.LAT</ta>
            <ta e="T67" id="Seg_1802" s="T66">дать-2PL</ta>
            <ta e="T68" id="Seg_1803" s="T67">дать-FUT-1SG</ta>
            <ta e="T69" id="Seg_1804" s="T68">тогда</ta>
            <ta e="T70" id="Seg_1805" s="T69">прийти-PST.[3SG]</ta>
            <ta e="T71" id="Seg_1806" s="T70">сказать-IPFVZ.[3SG]</ta>
            <ta e="T72" id="Seg_1807" s="T71">я.NOM</ta>
            <ta e="T73" id="Seg_1808" s="T72">ты.DAT</ta>
            <ta e="T74" id="Seg_1809" s="T73">что-NOM/GEN/ACC.2PL</ta>
            <ta e="T75" id="Seg_1810" s="T74">прийти-PST-2SG</ta>
            <ta e="T76" id="Seg_1811" s="T75">я.NOM</ta>
            <ta e="T77" id="Seg_1812" s="T76">ты.DAT</ta>
            <ta e="T78" id="Seg_1813" s="T77">принести-PST-1SG</ta>
            <ta e="T79" id="Seg_1814" s="T78">золото.[NOM.SG]</ta>
            <ta e="T80" id="Seg_1815" s="T79">этот-LAT</ta>
            <ta e="T81" id="Seg_1816" s="T80">дать-PST.[3SG]</ta>
            <ta e="T82" id="Seg_1817" s="T81">что.[NOM.SG]</ta>
            <ta e="T83" id="Seg_1818" s="T82">ты.DAT</ta>
            <ta e="T84" id="Seg_1819" s="T83">я.NOM</ta>
            <ta e="T85" id="Seg_1820" s="T84">дать-INF.LAT</ta>
            <ta e="T86" id="Seg_1821" s="T85">принести-IMP.2SG</ta>
            <ta e="T87" id="Seg_1822" s="T86">хлеб.[NOM.SG]</ta>
            <ta e="T88" id="Seg_1823" s="T87">этот-LAT</ta>
            <ta e="T89" id="Seg_1824" s="T88">принести-PST-3PL</ta>
            <ta e="T90" id="Seg_1825" s="T89">снег.[NOM.SG]</ta>
            <ta e="T91" id="Seg_1826" s="T90">хлеб.[NOM.SG]</ta>
            <ta e="T92" id="Seg_1827" s="T91">я.NOM</ta>
            <ta e="T93" id="Seg_1828" s="T92">такой.[NOM.SG]</ta>
            <ta e="T94" id="Seg_1829" s="T93">хлеб.[NOM.SG]</ta>
            <ta e="T95" id="Seg_1830" s="T94">NEG</ta>
            <ta e="T96" id="Seg_1831" s="T95">съесть-PRS-1SG</ta>
            <ta e="T97" id="Seg_1832" s="T96">я.LAT</ta>
            <ta e="T98" id="Seg_1833" s="T97">надо</ta>
            <ta e="T99" id="Seg_1834" s="T98">черный.[NOM.SG]</ta>
            <ta e="T100" id="Seg_1835" s="T99">хлеб.[NOM.SG]</ta>
            <ta e="T101" id="Seg_1836" s="T100">пойти-PST-3PL</ta>
            <ta e="T102" id="Seg_1837" s="T101">нищий-GEN</ta>
            <ta e="T103" id="Seg_1838" s="T102">взять-PST-3PL</ta>
            <ta e="T104" id="Seg_1839" s="T103">хлеб.[NOM.SG]</ta>
            <ta e="T105" id="Seg_1840" s="T104">этот.[NOM.SG]</ta>
            <ta e="T106" id="Seg_1841" s="T105">съесть-PST.[3SG]</ta>
            <ta e="T107" id="Seg_1842" s="T106">а</ta>
            <ta e="T108" id="Seg_1843" s="T107">сейчас</ta>
            <ta e="T109" id="Seg_1844" s="T108">что.[NOM.SG]</ta>
            <ta e="T110" id="Seg_1845" s="T109">ты.DAT</ta>
            <ta e="T111" id="Seg_1846" s="T110">нужно</ta>
            <ta e="T112" id="Seg_1847" s="T111">PTCL</ta>
            <ta e="T113" id="Seg_1848" s="T112">спать-INF.LAT</ta>
            <ta e="T114" id="Seg_1849" s="T113">тогда</ta>
            <ta e="T115" id="Seg_1850" s="T114">этот-LAT</ta>
            <ta e="T116" id="Seg_1851" s="T115">принести-PST-3PL</ta>
            <ta e="T117" id="Seg_1852" s="T116">перина-PL</ta>
            <ta e="T118" id="Seg_1853" s="T117">сказать-IPFVZ.[3SG]</ta>
            <ta e="T119" id="Seg_1854" s="T118">там</ta>
            <ta e="T120" id="Seg_1855" s="T119">NEG</ta>
            <ta e="T121" id="Seg_1856" s="T120">мочь-PRS-1SG</ta>
            <ta e="T122" id="Seg_1857" s="T121">спать-INF.LAT</ta>
            <ta e="T125" id="Seg_1858" s="T124">я.LAT</ta>
            <ta e="T126" id="Seg_1859" s="T125">солома.[NOM.SG]</ta>
            <ta e="T127" id="Seg_1860" s="T126">этот-LAT</ta>
            <ta e="T128" id="Seg_1861" s="T127">принести-PST-3PL</ta>
            <ta e="T129" id="Seg_1862" s="T128">этот.[NOM.SG]</ta>
            <ta e="T130" id="Seg_1863" s="T129">лежать-DUR-PST.[3SG]</ta>
            <ta e="T131" id="Seg_1864" s="T130">спать-DUR-PST.[3SG]</ta>
            <ta e="T132" id="Seg_1865" s="T131">тогда</ta>
            <ta e="T133" id="Seg_1866" s="T132">сказать-IPFVZ.[3SG]</ta>
            <ta e="T134" id="Seg_1867" s="T133">я.NOM</ta>
            <ta e="T135" id="Seg_1868" s="T134">там</ta>
            <ta e="T136" id="Seg_1869" s="T135">весь</ta>
            <ta e="T137" id="Seg_1870" s="T136">NEG</ta>
            <ta e="T138" id="Seg_1871" s="T137">хороший.[NOM.SG]</ta>
            <ta e="T139" id="Seg_1872" s="T138">делать-PST-1SG</ta>
            <ta e="T140" id="Seg_1873" s="T139">принести-IMP.2PL</ta>
            <ta e="T141" id="Seg_1874" s="T140">я.LAT</ta>
            <ta e="T142" id="Seg_1875" s="T141">розга-NOM/GEN/ACC.3PL</ta>
            <ta e="T143" id="Seg_1876" s="T142">тогда</ta>
            <ta e="T144" id="Seg_1877" s="T143">принести-PST-3PL</ta>
            <ta e="T145" id="Seg_1878" s="T144">розга-NOM/GEN/ACC.3PL</ta>
            <ta e="T146" id="Seg_1879" s="T145">бить-INF.LAT</ta>
            <ta e="T147" id="Seg_1880" s="T146">этот-ACC</ta>
            <ta e="T148" id="Seg_1881" s="T147">этот.[NOM.SG]</ta>
            <ta e="T149" id="Seg_1882" s="T148">сказать-IPFVZ.[3SG]</ta>
            <ta e="T150" id="Seg_1883" s="T149">там</ta>
            <ta e="T151" id="Seg_1884" s="T150">дверь-LAT/LOC.3SG</ta>
            <ta e="T152" id="Seg_1885" s="T151">ты.NOM</ta>
            <ta e="T153" id="Seg_1886" s="T152">мужчина.[NOM.SG]</ta>
            <ta e="T154" id="Seg_1887" s="T153">стоять-CVB-PST.[3SG]</ta>
            <ta e="T155" id="Seg_1888" s="T154">этот.[NOM.SG]</ta>
            <ta e="T156" id="Seg_1889" s="T155">сказать-IPFVZ.[3SG]</ta>
            <ta e="T157" id="Seg_1890" s="T156">я.LAT</ta>
            <ta e="T158" id="Seg_1891" s="T157">половина-NOM/GEN/ACC.3SG</ta>
            <ta e="T159" id="Seg_1892" s="T158">принести-IMP.2SG.O</ta>
            <ta e="T160" id="Seg_1893" s="T159">тогда</ta>
            <ta e="T161" id="Seg_1894" s="T160">этот-ACC</ta>
            <ta e="T162" id="Seg_1895" s="T161">бить-PST-3PL</ta>
            <ta e="T163" id="Seg_1896" s="T162">половина-NOM/GEN/ACC.3SG</ta>
            <ta e="T164" id="Seg_1897" s="T163">дать-PST-3PL</ta>
            <ta e="T165" id="Seg_1898" s="T164">и</ta>
            <ta e="T166" id="Seg_1899" s="T165">весь</ta>
            <ta e="T167" id="Seg_1900" s="T166">этот-LAT</ta>
            <ta e="T168" id="Seg_1901" s="T167">дать-PRS-3PL</ta>
            <ta e="T169" id="Seg_1902" s="T168">тогда</ta>
            <ta e="T170" id="Seg_1903" s="T169">PTCL</ta>
            <ta e="T171" id="Seg_1904" s="T170">этот-ACC</ta>
            <ta e="T172" id="Seg_1905" s="T171">бить-MOM-PST.[3SG]</ta>
            <ta e="T173" id="Seg_1906" s="T172">а</ta>
            <ta e="T174" id="Seg_1907" s="T173">этот.[NOM.SG]</ta>
            <ta e="T175" id="Seg_1908" s="T174">взять-PST.[3SG]</ta>
            <ta e="T176" id="Seg_1909" s="T175">шапка-ACC.3SG</ta>
            <ta e="T177" id="Seg_1910" s="T176">и</ta>
            <ta e="T179" id="Seg_1911" s="T178">бежать-MOM-PST.[3SG]</ta>
            <ta e="T182" id="Seg_1912" s="T179">пойти-CVB</ta>
            <ta e="T180" id="Seg_1913" s="T182">исчезнуть-PST.[3SG]</ta>
            <ta e="T181" id="Seg_1914" s="T180">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1915" s="T0">num-n:case</ta>
            <ta e="T2" id="Seg_1916" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_1917" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_1918" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_1919" s="T4">conj</ta>
            <ta e="T6" id="Seg_1920" s="T5">adv</ta>
            <ta e="T7" id="Seg_1921" s="T6">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_1922" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_1923" s="T8">adv</ta>
            <ta e="T10" id="Seg_1924" s="T9">v-v&gt;v-v:pn</ta>
            <ta e="T11" id="Seg_1925" s="T10">que</ta>
            <ta e="T12" id="Seg_1926" s="T11">dempro-n:case</ta>
            <ta e="T13" id="Seg_1927" s="T12">pers</ta>
            <ta e="T14" id="Seg_1928" s="T13">v-v:n.fin</ta>
            <ta e="T15" id="Seg_1929" s="T14">pers</ta>
            <ta e="T16" id="Seg_1930" s="T15">v-v:n.fin</ta>
            <ta e="T17" id="Seg_1931" s="T16">v-v:tense-v:pn</ta>
            <ta e="T18" id="Seg_1932" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_1933" s="T18">adv</ta>
            <ta e="T20" id="Seg_1934" s="T19">pers</ta>
            <ta e="T21" id="Seg_1935" s="T20">v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_1936" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_1937" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_1938" s="T23">conj</ta>
            <ta e="T25" id="Seg_1939" s="T24">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T26" id="Seg_1940" s="T25">adv</ta>
            <ta e="T27" id="Seg_1941" s="T26">v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_1942" s="T27">v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_1943" s="T28">v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_1944" s="T29">v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_1945" s="T30">v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_1946" s="T31">adv</ta>
            <ta e="T33" id="Seg_1947" s="T32">dempro-n:case</ta>
            <ta e="T34" id="Seg_1948" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_1949" s="T34">v-v:tense-v:pn</ta>
            <ta e="T36" id="Seg_1950" s="T35">que-%%</ta>
            <ta e="T37" id="Seg_1951" s="T36">pers</ta>
            <ta e="T39" id="Seg_1952" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_1953" s="T39">adv</ta>
            <ta e="T41" id="Seg_1954" s="T40">pers</ta>
            <ta e="T42" id="Seg_1955" s="T41">adv</ta>
            <ta e="T43" id="Seg_1956" s="T42">v-v:tense-v:pn</ta>
            <ta e="T44" id="Seg_1957" s="T43">num-n:case</ta>
            <ta e="T45" id="Seg_1958" s="T44">num-n:case</ta>
            <ta e="T46" id="Seg_1959" s="T45">num-n:case</ta>
            <ta e="T47" id="Seg_1960" s="T46">adv</ta>
            <ta e="T48" id="Seg_1961" s="T47">num-n:case</ta>
            <ta e="T49" id="Seg_1962" s="T48">v-v:tense-v:pn</ta>
            <ta e="T50" id="Seg_1963" s="T49">n-n:case.poss</ta>
            <ta e="T51" id="Seg_1964" s="T50">que-n:case</ta>
            <ta e="T52" id="Seg_1965" s="T51">v-v:tense-v:pn</ta>
            <ta e="T53" id="Seg_1966" s="T52">ptcl</ta>
            <ta e="T54" id="Seg_1967" s="T53">pers</ta>
            <ta e="T55" id="Seg_1968" s="T54">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T56" id="Seg_1969" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_1970" s="T56">n-n:case</ta>
            <ta e="T58" id="Seg_1971" s="T57">v-v:mood.pn</ta>
            <ta e="T59" id="Seg_1972" s="T58">dempro-n:case</ta>
            <ta e="T60" id="Seg_1973" s="T59">v-v:tense-v:pn</ta>
            <ta e="T61" id="Seg_1974" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_1975" s="T61">que-n:case</ta>
            <ta e="T63" id="Seg_1976" s="T62">pers</ta>
            <ta e="T64" id="Seg_1977" s="T63">v-v:tense-v:pn</ta>
            <ta e="T65" id="Seg_1978" s="T64">n-n:case.poss</ta>
            <ta e="T66" id="Seg_1979" s="T65">pers</ta>
            <ta e="T67" id="Seg_1980" s="T66">v-v:pn</ta>
            <ta e="T68" id="Seg_1981" s="T67">v-v:tense-v:pn</ta>
            <ta e="T69" id="Seg_1982" s="T68">adv</ta>
            <ta e="T70" id="Seg_1983" s="T69">v-v:tense-v:pn</ta>
            <ta e="T71" id="Seg_1984" s="T70">v-v&gt;v-v:pn</ta>
            <ta e="T72" id="Seg_1985" s="T71">pers</ta>
            <ta e="T73" id="Seg_1986" s="T72">pers</ta>
            <ta e="T74" id="Seg_1987" s="T73">que-n:case.poss</ta>
            <ta e="T75" id="Seg_1988" s="T74">v-v:tense-v:pn</ta>
            <ta e="T76" id="Seg_1989" s="T75">pers</ta>
            <ta e="T77" id="Seg_1990" s="T76">pers</ta>
            <ta e="T78" id="Seg_1991" s="T77">v-v:tense-v:pn</ta>
            <ta e="T79" id="Seg_1992" s="T78">n-n:case</ta>
            <ta e="T80" id="Seg_1993" s="T79">dempro-n:case</ta>
            <ta e="T81" id="Seg_1994" s="T80">v-v:tense-v:pn</ta>
            <ta e="T82" id="Seg_1995" s="T81">que-n:case</ta>
            <ta e="T83" id="Seg_1996" s="T82">pers</ta>
            <ta e="T84" id="Seg_1997" s="T83">pers</ta>
            <ta e="T85" id="Seg_1998" s="T84">v-v:n.fin</ta>
            <ta e="T86" id="Seg_1999" s="T85">v-v:mood.pn</ta>
            <ta e="T87" id="Seg_2000" s="T86">n-n:case</ta>
            <ta e="T88" id="Seg_2001" s="T87">dempro-n:case</ta>
            <ta e="T89" id="Seg_2002" s="T88">v-v:tense-v:pn</ta>
            <ta e="T90" id="Seg_2003" s="T89">n-n:case</ta>
            <ta e="T91" id="Seg_2004" s="T90">n-n:case</ta>
            <ta e="T92" id="Seg_2005" s="T91">pers</ta>
            <ta e="T93" id="Seg_2006" s="T92">adj-n:case</ta>
            <ta e="T94" id="Seg_2007" s="T93">n-n:case</ta>
            <ta e="T95" id="Seg_2008" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_2009" s="T95">v-v:tense-v:pn</ta>
            <ta e="T97" id="Seg_2010" s="T96">pers</ta>
            <ta e="T98" id="Seg_2011" s="T97">ptcl</ta>
            <ta e="T99" id="Seg_2012" s="T98">adj-n:case</ta>
            <ta e="T100" id="Seg_2013" s="T99">n-n:case</ta>
            <ta e="T101" id="Seg_2014" s="T100">v-v:tense-v:pn</ta>
            <ta e="T102" id="Seg_2015" s="T101">n-n:case</ta>
            <ta e="T103" id="Seg_2016" s="T102">v-v:tense-v:pn</ta>
            <ta e="T104" id="Seg_2017" s="T103">n-n:case</ta>
            <ta e="T105" id="Seg_2018" s="T104">dempro-n:case</ta>
            <ta e="T106" id="Seg_2019" s="T105">v-v:tense-v:pn</ta>
            <ta e="T107" id="Seg_2020" s="T106">conj</ta>
            <ta e="T108" id="Seg_2021" s="T107">adv</ta>
            <ta e="T109" id="Seg_2022" s="T108">que-n:case</ta>
            <ta e="T110" id="Seg_2023" s="T109">pers</ta>
            <ta e="T111" id="Seg_2024" s="T110">adv</ta>
            <ta e="T112" id="Seg_2025" s="T111">ptcl</ta>
            <ta e="T113" id="Seg_2026" s="T112">v-v:n.fin</ta>
            <ta e="T114" id="Seg_2027" s="T113">adv</ta>
            <ta e="T115" id="Seg_2028" s="T114">dempro-n:case</ta>
            <ta e="T116" id="Seg_2029" s="T115">v-v:tense-v:pn</ta>
            <ta e="T117" id="Seg_2030" s="T116">n-n:num</ta>
            <ta e="T118" id="Seg_2031" s="T117">v-v&gt;v-v:pn</ta>
            <ta e="T119" id="Seg_2032" s="T118">adv</ta>
            <ta e="T120" id="Seg_2033" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_2034" s="T120">v-v:tense-v:pn</ta>
            <ta e="T122" id="Seg_2035" s="T121">v-v:n.fin</ta>
            <ta e="T125" id="Seg_2036" s="T124">pers</ta>
            <ta e="T126" id="Seg_2037" s="T125">n-n:case</ta>
            <ta e="T127" id="Seg_2038" s="T126">dempro-n:case</ta>
            <ta e="T128" id="Seg_2039" s="T127">v-v:tense-v:pn</ta>
            <ta e="T129" id="Seg_2040" s="T128">dempro-n:case</ta>
            <ta e="T130" id="Seg_2041" s="T129">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T131" id="Seg_2042" s="T130">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T132" id="Seg_2043" s="T131">adv</ta>
            <ta e="T133" id="Seg_2044" s="T132">v-v&gt;v-v:pn</ta>
            <ta e="T134" id="Seg_2045" s="T133">pers</ta>
            <ta e="T135" id="Seg_2046" s="T134">adv</ta>
            <ta e="T136" id="Seg_2047" s="T135">quant</ta>
            <ta e="T137" id="Seg_2048" s="T136">ptcl</ta>
            <ta e="T138" id="Seg_2049" s="T137">adj-n:case</ta>
            <ta e="T139" id="Seg_2050" s="T138">v-v:tense-v:pn</ta>
            <ta e="T140" id="Seg_2051" s="T139">v-v:mood.pn</ta>
            <ta e="T141" id="Seg_2052" s="T140">pers</ta>
            <ta e="T142" id="Seg_2053" s="T141">n-n:case.poss</ta>
            <ta e="T143" id="Seg_2054" s="T142">adv</ta>
            <ta e="T144" id="Seg_2055" s="T143">v-v:tense-v:pn</ta>
            <ta e="T145" id="Seg_2056" s="T144">n-n:case.poss</ta>
            <ta e="T146" id="Seg_2057" s="T145">v-v:n.fin</ta>
            <ta e="T147" id="Seg_2058" s="T146">dempro-n:case</ta>
            <ta e="T148" id="Seg_2059" s="T147">dempro-n:case</ta>
            <ta e="T149" id="Seg_2060" s="T148">v-v&gt;v-v:pn</ta>
            <ta e="T150" id="Seg_2061" s="T149">adv</ta>
            <ta e="T151" id="Seg_2062" s="T150">n-n:case.poss</ta>
            <ta e="T152" id="Seg_2063" s="T151">pers</ta>
            <ta e="T153" id="Seg_2064" s="T152">n-n:case</ta>
            <ta e="T154" id="Seg_2065" s="T153">v-v:n.fin-v:tense-v:pn</ta>
            <ta e="T155" id="Seg_2066" s="T154">dempro-n:case</ta>
            <ta e="T156" id="Seg_2067" s="T155">v-v&gt;v-v:pn</ta>
            <ta e="T157" id="Seg_2068" s="T156">pers</ta>
            <ta e="T158" id="Seg_2069" s="T157">n-n:case.poss</ta>
            <ta e="T159" id="Seg_2070" s="T158">v-v:mood.pn</ta>
            <ta e="T160" id="Seg_2071" s="T159">adv</ta>
            <ta e="T161" id="Seg_2072" s="T160">dempro-n:case</ta>
            <ta e="T162" id="Seg_2073" s="T161">v-v:tense-v:pn</ta>
            <ta e="T163" id="Seg_2074" s="T162">n-n:case.poss</ta>
            <ta e="T164" id="Seg_2075" s="T163">v-v:tense-v:pn</ta>
            <ta e="T165" id="Seg_2076" s="T164">conj</ta>
            <ta e="T166" id="Seg_2077" s="T165">quant</ta>
            <ta e="T167" id="Seg_2078" s="T166">dempro-n:case</ta>
            <ta e="T168" id="Seg_2079" s="T167">v-v:tense-v:pn</ta>
            <ta e="T169" id="Seg_2080" s="T168">adv</ta>
            <ta e="T170" id="Seg_2081" s="T169">ptcl</ta>
            <ta e="T171" id="Seg_2082" s="T170">dempro-n:case</ta>
            <ta e="T172" id="Seg_2083" s="T171">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T173" id="Seg_2084" s="T172">conj</ta>
            <ta e="T174" id="Seg_2085" s="T173">dempro-n:case</ta>
            <ta e="T175" id="Seg_2086" s="T174">v-v:tense-v:pn</ta>
            <ta e="T176" id="Seg_2087" s="T175">n-n:case.poss</ta>
            <ta e="T177" id="Seg_2088" s="T176">conj</ta>
            <ta e="T179" id="Seg_2089" s="T178">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T182" id="Seg_2090" s="T179">v-v:n-fin</ta>
            <ta e="T180" id="Seg_2091" s="T182">v-v:tense-v:pn</ta>
            <ta e="T181" id="Seg_2092" s="T180">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_2093" s="T0">num</ta>
            <ta e="T2" id="Seg_2094" s="T1">n</ta>
            <ta e="T3" id="Seg_2095" s="T2">v</ta>
            <ta e="T4" id="Seg_2096" s="T3">n</ta>
            <ta e="T5" id="Seg_2097" s="T4">conj</ta>
            <ta e="T6" id="Seg_2098" s="T5">adv</ta>
            <ta e="T7" id="Seg_2099" s="T6">v</ta>
            <ta e="T8" id="Seg_2100" s="T7">n</ta>
            <ta e="T9" id="Seg_2101" s="T8">adv</ta>
            <ta e="T10" id="Seg_2102" s="T9">v</ta>
            <ta e="T11" id="Seg_2103" s="T10">que</ta>
            <ta e="T12" id="Seg_2104" s="T11">dempro</ta>
            <ta e="T13" id="Seg_2105" s="T12">pers</ta>
            <ta e="T14" id="Seg_2106" s="T13">v</ta>
            <ta e="T15" id="Seg_2107" s="T14">pers</ta>
            <ta e="T16" id="Seg_2108" s="T15">v</ta>
            <ta e="T17" id="Seg_2109" s="T16">v</ta>
            <ta e="T18" id="Seg_2110" s="T17">n</ta>
            <ta e="T19" id="Seg_2111" s="T18">adv</ta>
            <ta e="T20" id="Seg_2112" s="T19">pers</ta>
            <ta e="T21" id="Seg_2113" s="T20">v</ta>
            <ta e="T22" id="Seg_2114" s="T21">n</ta>
            <ta e="T23" id="Seg_2115" s="T22">n</ta>
            <ta e="T24" id="Seg_2116" s="T23">conj</ta>
            <ta e="T25" id="Seg_2117" s="T24">v</ta>
            <ta e="T26" id="Seg_2118" s="T25">adv</ta>
            <ta e="T27" id="Seg_2119" s="T26">v</ta>
            <ta e="T28" id="Seg_2120" s="T27">v</ta>
            <ta e="T29" id="Seg_2121" s="T28">v</ta>
            <ta e="T30" id="Seg_2122" s="T29">v</ta>
            <ta e="T31" id="Seg_2123" s="T30">v</ta>
            <ta e="T32" id="Seg_2124" s="T31">adv</ta>
            <ta e="T33" id="Seg_2125" s="T32">dempro</ta>
            <ta e="T34" id="Seg_2126" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_2127" s="T34">v</ta>
            <ta e="T36" id="Seg_2128" s="T35">que</ta>
            <ta e="T37" id="Seg_2129" s="T36">pers</ta>
            <ta e="T39" id="Seg_2130" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_2131" s="T39">adv</ta>
            <ta e="T41" id="Seg_2132" s="T40">pers</ta>
            <ta e="T42" id="Seg_2133" s="T41">adv</ta>
            <ta e="T43" id="Seg_2134" s="T42">v</ta>
            <ta e="T44" id="Seg_2135" s="T43">num</ta>
            <ta e="T45" id="Seg_2136" s="T44">num</ta>
            <ta e="T46" id="Seg_2137" s="T45">num</ta>
            <ta e="T47" id="Seg_2138" s="T46">adv</ta>
            <ta e="T48" id="Seg_2139" s="T47">num</ta>
            <ta e="T49" id="Seg_2140" s="T48">v</ta>
            <ta e="T50" id="Seg_2141" s="T49">n</ta>
            <ta e="T51" id="Seg_2142" s="T50">que</ta>
            <ta e="T52" id="Seg_2143" s="T51">v</ta>
            <ta e="T53" id="Seg_2144" s="T52">ptcl</ta>
            <ta e="T54" id="Seg_2145" s="T53">pers</ta>
            <ta e="T55" id="Seg_2146" s="T54">v</ta>
            <ta e="T56" id="Seg_2147" s="T55">n</ta>
            <ta e="T57" id="Seg_2148" s="T56">n</ta>
            <ta e="T58" id="Seg_2149" s="T57">v</ta>
            <ta e="T59" id="Seg_2150" s="T58">dempro</ta>
            <ta e="T60" id="Seg_2151" s="T59">v</ta>
            <ta e="T61" id="Seg_2152" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_2153" s="T61">que</ta>
            <ta e="T63" id="Seg_2154" s="T62">pers</ta>
            <ta e="T64" id="Seg_2155" s="T63">v</ta>
            <ta e="T65" id="Seg_2156" s="T64">n</ta>
            <ta e="T66" id="Seg_2157" s="T65">pers</ta>
            <ta e="T67" id="Seg_2158" s="T66">v</ta>
            <ta e="T68" id="Seg_2159" s="T67">v</ta>
            <ta e="T69" id="Seg_2160" s="T68">adv</ta>
            <ta e="T70" id="Seg_2161" s="T69">v</ta>
            <ta e="T71" id="Seg_2162" s="T70">v</ta>
            <ta e="T72" id="Seg_2163" s="T71">pers</ta>
            <ta e="T73" id="Seg_2164" s="T72">pers</ta>
            <ta e="T74" id="Seg_2165" s="T73">que</ta>
            <ta e="T75" id="Seg_2166" s="T74">v</ta>
            <ta e="T76" id="Seg_2167" s="T75">pers</ta>
            <ta e="T77" id="Seg_2168" s="T76">pers</ta>
            <ta e="T78" id="Seg_2169" s="T77">v</ta>
            <ta e="T79" id="Seg_2170" s="T78">n</ta>
            <ta e="T80" id="Seg_2171" s="T79">dempro</ta>
            <ta e="T81" id="Seg_2172" s="T80">v</ta>
            <ta e="T82" id="Seg_2173" s="T81">que</ta>
            <ta e="T83" id="Seg_2174" s="T82">pers</ta>
            <ta e="T84" id="Seg_2175" s="T83">pers</ta>
            <ta e="T85" id="Seg_2176" s="T84">v</ta>
            <ta e="T86" id="Seg_2177" s="T85">v</ta>
            <ta e="T87" id="Seg_2178" s="T86">n</ta>
            <ta e="T88" id="Seg_2179" s="T87">dempro</ta>
            <ta e="T89" id="Seg_2180" s="T88">v</ta>
            <ta e="T90" id="Seg_2181" s="T89">n</ta>
            <ta e="T91" id="Seg_2182" s="T90">n</ta>
            <ta e="T92" id="Seg_2183" s="T91">pers</ta>
            <ta e="T93" id="Seg_2184" s="T92">adj</ta>
            <ta e="T94" id="Seg_2185" s="T93">n</ta>
            <ta e="T95" id="Seg_2186" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_2187" s="T95">v</ta>
            <ta e="T97" id="Seg_2188" s="T96">pers</ta>
            <ta e="T98" id="Seg_2189" s="T97">ptcl</ta>
            <ta e="T99" id="Seg_2190" s="T98">adj</ta>
            <ta e="T100" id="Seg_2191" s="T99">n</ta>
            <ta e="T101" id="Seg_2192" s="T100">v</ta>
            <ta e="T102" id="Seg_2193" s="T101">n</ta>
            <ta e="T103" id="Seg_2194" s="T102">v</ta>
            <ta e="T104" id="Seg_2195" s="T103">n</ta>
            <ta e="T105" id="Seg_2196" s="T104">dempro</ta>
            <ta e="T106" id="Seg_2197" s="T105">v</ta>
            <ta e="T107" id="Seg_2198" s="T106">conj</ta>
            <ta e="T108" id="Seg_2199" s="T107">adv</ta>
            <ta e="T109" id="Seg_2200" s="T108">que</ta>
            <ta e="T110" id="Seg_2201" s="T109">pers</ta>
            <ta e="T111" id="Seg_2202" s="T110">adv</ta>
            <ta e="T112" id="Seg_2203" s="T111">ptcl</ta>
            <ta e="T113" id="Seg_2204" s="T112">v</ta>
            <ta e="T114" id="Seg_2205" s="T113">adv</ta>
            <ta e="T115" id="Seg_2206" s="T114">dempro</ta>
            <ta e="T116" id="Seg_2207" s="T115">v</ta>
            <ta e="T117" id="Seg_2208" s="T116">n</ta>
            <ta e="T118" id="Seg_2209" s="T117">v</ta>
            <ta e="T119" id="Seg_2210" s="T118">adv</ta>
            <ta e="T120" id="Seg_2211" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_2212" s="T120">v</ta>
            <ta e="T122" id="Seg_2213" s="T121">v</ta>
            <ta e="T125" id="Seg_2214" s="T124">pers</ta>
            <ta e="T126" id="Seg_2215" s="T125">n</ta>
            <ta e="T127" id="Seg_2216" s="T126">dempro</ta>
            <ta e="T128" id="Seg_2217" s="T127">v</ta>
            <ta e="T129" id="Seg_2218" s="T128">dempro</ta>
            <ta e="T130" id="Seg_2219" s="T129">v</ta>
            <ta e="T131" id="Seg_2220" s="T130">v</ta>
            <ta e="T132" id="Seg_2221" s="T131">adv</ta>
            <ta e="T133" id="Seg_2222" s="T132">v</ta>
            <ta e="T134" id="Seg_2223" s="T133">pers</ta>
            <ta e="T135" id="Seg_2224" s="T134">adv</ta>
            <ta e="T136" id="Seg_2225" s="T135">quant</ta>
            <ta e="T137" id="Seg_2226" s="T136">ptcl</ta>
            <ta e="T138" id="Seg_2227" s="T137">adj</ta>
            <ta e="T139" id="Seg_2228" s="T138">v</ta>
            <ta e="T140" id="Seg_2229" s="T139">v</ta>
            <ta e="T141" id="Seg_2230" s="T140">pers</ta>
            <ta e="T142" id="Seg_2231" s="T141">n</ta>
            <ta e="T143" id="Seg_2232" s="T142">adv</ta>
            <ta e="T144" id="Seg_2233" s="T143">v</ta>
            <ta e="T145" id="Seg_2234" s="T144">n</ta>
            <ta e="T146" id="Seg_2235" s="T145">v</ta>
            <ta e="T147" id="Seg_2236" s="T146">dempro</ta>
            <ta e="T148" id="Seg_2237" s="T147">dempro</ta>
            <ta e="T149" id="Seg_2238" s="T148">v</ta>
            <ta e="T150" id="Seg_2239" s="T149">adv</ta>
            <ta e="T151" id="Seg_2240" s="T150">n</ta>
            <ta e="T152" id="Seg_2241" s="T151">pers</ta>
            <ta e="T153" id="Seg_2242" s="T152">n</ta>
            <ta e="T154" id="Seg_2243" s="T153">v</ta>
            <ta e="T155" id="Seg_2244" s="T154">dempro</ta>
            <ta e="T156" id="Seg_2245" s="T155">v</ta>
            <ta e="T157" id="Seg_2246" s="T156">pers</ta>
            <ta e="T158" id="Seg_2247" s="T157">n</ta>
            <ta e="T159" id="Seg_2248" s="T158">v</ta>
            <ta e="T160" id="Seg_2249" s="T159">adv</ta>
            <ta e="T161" id="Seg_2250" s="T160">dempro</ta>
            <ta e="T162" id="Seg_2251" s="T161">v</ta>
            <ta e="T163" id="Seg_2252" s="T162">n</ta>
            <ta e="T164" id="Seg_2253" s="T163">v</ta>
            <ta e="T165" id="Seg_2254" s="T164">conj</ta>
            <ta e="T166" id="Seg_2255" s="T165">quant</ta>
            <ta e="T167" id="Seg_2256" s="T166">dempro</ta>
            <ta e="T168" id="Seg_2257" s="T167">v</ta>
            <ta e="T169" id="Seg_2258" s="T168">adv</ta>
            <ta e="T170" id="Seg_2259" s="T169">ptcl</ta>
            <ta e="T171" id="Seg_2260" s="T170">dempro</ta>
            <ta e="T172" id="Seg_2261" s="T171">v</ta>
            <ta e="T173" id="Seg_2262" s="T172">conj</ta>
            <ta e="T174" id="Seg_2263" s="T173">dempro</ta>
            <ta e="T175" id="Seg_2264" s="T174">v</ta>
            <ta e="T176" id="Seg_2265" s="T175">n</ta>
            <ta e="T177" id="Seg_2266" s="T176">conj</ta>
            <ta e="T179" id="Seg_2267" s="T178">v</ta>
            <ta e="T182" id="Seg_2268" s="T179">v</ta>
            <ta e="T180" id="Seg_2269" s="T182">v</ta>
            <ta e="T181" id="Seg_2270" s="T180">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_2271" s="T1">np.h:A</ta>
            <ta e="T4" id="Seg_2272" s="T3">np:P</ta>
            <ta e="T6" id="Seg_2273" s="T5">adv:L</ta>
            <ta e="T7" id="Seg_2274" s="T6">0.3.h:A</ta>
            <ta e="T8" id="Seg_2275" s="T7">np:Th</ta>
            <ta e="T9" id="Seg_2276" s="T8">adv:Time</ta>
            <ta e="T10" id="Seg_2277" s="T9">0.3.h:E</ta>
            <ta e="T12" id="Seg_2278" s="T11">pro:Th</ta>
            <ta e="T13" id="Seg_2279" s="T12">pro.h:A</ta>
            <ta e="T15" id="Seg_2280" s="T14">pro:So</ta>
            <ta e="T18" id="Seg_2281" s="T17">np.h:A</ta>
            <ta e="T19" id="Seg_2282" s="T18">adv:Time</ta>
            <ta e="T20" id="Seg_2283" s="T19">pro.h:A</ta>
            <ta e="T22" id="Seg_2284" s="T21">np.h:Poss</ta>
            <ta e="T23" id="Seg_2285" s="T22">np:G</ta>
            <ta e="T25" id="Seg_2286" s="T24">0.1.h:A</ta>
            <ta e="T26" id="Seg_2287" s="T25">adv:Time</ta>
            <ta e="T27" id="Seg_2288" s="T26">0.3.h:A</ta>
            <ta e="T28" id="Seg_2289" s="T27">0.3.h:A</ta>
            <ta e="T29" id="Seg_2290" s="T28">0.3.h:A</ta>
            <ta e="T30" id="Seg_2291" s="T29">0.3.h:A</ta>
            <ta e="T31" id="Seg_2292" s="T30">0.3.h:A</ta>
            <ta e="T32" id="Seg_2293" s="T31">adv:L</ta>
            <ta e="T33" id="Seg_2294" s="T32">pro.h:Th</ta>
            <ta e="T35" id="Seg_2295" s="T34">0.3.h:A</ta>
            <ta e="T37" id="Seg_2296" s="T36">pro.h:A</ta>
            <ta e="T41" id="Seg_2297" s="T40">pro.h:B</ta>
            <ta e="T42" id="Seg_2298" s="T41">adv:Time</ta>
            <ta e="T43" id="Seg_2299" s="T42">0.3.h:A</ta>
            <ta e="T47" id="Seg_2300" s="T46">adv:Time</ta>
            <ta e="T48" id="Seg_2301" s="T47">np.h:E</ta>
            <ta e="T50" id="Seg_2302" s="T49">np:L</ta>
            <ta e="T52" id="Seg_2303" s="T51">0.2.h:A</ta>
            <ta e="T54" id="Seg_2304" s="T53">pro.h:A</ta>
            <ta e="T56" id="Seg_2305" s="T55">np.h:R</ta>
            <ta e="T57" id="Seg_2306" s="T56">np:Th</ta>
            <ta e="T58" id="Seg_2307" s="T57">0.2.h:A</ta>
            <ta e="T59" id="Seg_2308" s="T58">pro.h:A</ta>
            <ta e="T62" id="Seg_2309" s="T61">pro:Th</ta>
            <ta e="T63" id="Seg_2310" s="T62">pro.h:R</ta>
            <ta e="T64" id="Seg_2311" s="T63">0.3.h:A</ta>
            <ta e="T65" id="Seg_2312" s="T64">np:Th</ta>
            <ta e="T66" id="Seg_2313" s="T65">pro.h:R</ta>
            <ta e="T67" id="Seg_2314" s="T66">0.2.h:A</ta>
            <ta e="T68" id="Seg_2315" s="T67">0.1.h:A</ta>
            <ta e="T69" id="Seg_2316" s="T68">adv:Time</ta>
            <ta e="T70" id="Seg_2317" s="T69">0.3.h:A</ta>
            <ta e="T71" id="Seg_2318" s="T70">0.3.h:A</ta>
            <ta e="T72" id="Seg_2319" s="T71">pro:G</ta>
            <ta e="T73" id="Seg_2320" s="T72">pro.h:A</ta>
            <ta e="T76" id="Seg_2321" s="T75">pro.h:A</ta>
            <ta e="T77" id="Seg_2322" s="T76">pro.h:R</ta>
            <ta e="T79" id="Seg_2323" s="T78">np:Th</ta>
            <ta e="T80" id="Seg_2324" s="T79">pro.h:R</ta>
            <ta e="T81" id="Seg_2325" s="T80">0.3.h:A</ta>
            <ta e="T83" id="Seg_2326" s="T82">pro.h:R</ta>
            <ta e="T84" id="Seg_2327" s="T83">pro.h:A</ta>
            <ta e="T86" id="Seg_2328" s="T85">0.2.h:A</ta>
            <ta e="T87" id="Seg_2329" s="T86">np:Th</ta>
            <ta e="T88" id="Seg_2330" s="T87">pro.h:R</ta>
            <ta e="T89" id="Seg_2331" s="T88">0.3.h:A</ta>
            <ta e="T91" id="Seg_2332" s="T90">np:Th</ta>
            <ta e="T92" id="Seg_2333" s="T91">pro.h:A</ta>
            <ta e="T94" id="Seg_2334" s="T93">np:P</ta>
            <ta e="T97" id="Seg_2335" s="T96">pro.h:B</ta>
            <ta e="T100" id="Seg_2336" s="T99">np:Th</ta>
            <ta e="T101" id="Seg_2337" s="T100">0.3.h:A</ta>
            <ta e="T102" id="Seg_2338" s="T101">np:G</ta>
            <ta e="T103" id="Seg_2339" s="T102">0.3.h:A</ta>
            <ta e="T104" id="Seg_2340" s="T103">np:Th</ta>
            <ta e="T105" id="Seg_2341" s="T104">pro.h:A</ta>
            <ta e="T108" id="Seg_2342" s="T107">adv:Time</ta>
            <ta e="T109" id="Seg_2343" s="T108">pro:Th</ta>
            <ta e="T110" id="Seg_2344" s="T109">pro.h:B</ta>
            <ta e="T114" id="Seg_2345" s="T113">adv:Time</ta>
            <ta e="T115" id="Seg_2346" s="T114">pro.h:R</ta>
            <ta e="T116" id="Seg_2347" s="T115">0.3.h:A</ta>
            <ta e="T117" id="Seg_2348" s="T116">np:Th</ta>
            <ta e="T118" id="Seg_2349" s="T117">0.3.h:A</ta>
            <ta e="T119" id="Seg_2350" s="T118">adv:L</ta>
            <ta e="T121" id="Seg_2351" s="T120">0.1.h:E</ta>
            <ta e="T125" id="Seg_2352" s="T124">pro.h:R</ta>
            <ta e="T126" id="Seg_2353" s="T125">np:Th</ta>
            <ta e="T127" id="Seg_2354" s="T126">pro.h:R</ta>
            <ta e="T128" id="Seg_2355" s="T127">0.3.h:A</ta>
            <ta e="T129" id="Seg_2356" s="T128">pro.h:E</ta>
            <ta e="T131" id="Seg_2357" s="T130">0.3.h:E</ta>
            <ta e="T132" id="Seg_2358" s="T131">adv:Time</ta>
            <ta e="T133" id="Seg_2359" s="T132">0.3.h:A</ta>
            <ta e="T134" id="Seg_2360" s="T133">pro.h:A</ta>
            <ta e="T135" id="Seg_2361" s="T134">adv:L</ta>
            <ta e="T136" id="Seg_2362" s="T135">np:P</ta>
            <ta e="T140" id="Seg_2363" s="T139">0.2.h:A</ta>
            <ta e="T141" id="Seg_2364" s="T140">pro.h:R</ta>
            <ta e="T142" id="Seg_2365" s="T141">np:Th</ta>
            <ta e="T143" id="Seg_2366" s="T142">adv:Time</ta>
            <ta e="T144" id="Seg_2367" s="T143">0.3.h:A</ta>
            <ta e="T145" id="Seg_2368" s="T144">np:Th</ta>
            <ta e="T147" id="Seg_2369" s="T146">pro.h:E</ta>
            <ta e="T148" id="Seg_2370" s="T147">pro.h:A</ta>
            <ta e="T150" id="Seg_2371" s="T149">adv:L</ta>
            <ta e="T151" id="Seg_2372" s="T150">np:L</ta>
            <ta e="T152" id="Seg_2373" s="T151">pro.h:Poss</ta>
            <ta e="T153" id="Seg_2374" s="T152">np.h:Th</ta>
            <ta e="T155" id="Seg_2375" s="T154">pro.h:A</ta>
            <ta e="T157" id="Seg_2376" s="T156">pro.h:R</ta>
            <ta e="T158" id="Seg_2377" s="T157">np:Th</ta>
            <ta e="T159" id="Seg_2378" s="T158">0.2.h:A</ta>
            <ta e="T160" id="Seg_2379" s="T159">adv:Time</ta>
            <ta e="T161" id="Seg_2380" s="T160">pro.h:E</ta>
            <ta e="T162" id="Seg_2381" s="T161">0.3.h:A</ta>
            <ta e="T163" id="Seg_2382" s="T162">np:Th</ta>
            <ta e="T164" id="Seg_2383" s="T163">0.3.h:A</ta>
            <ta e="T166" id="Seg_2384" s="T165">np:Th</ta>
            <ta e="T167" id="Seg_2385" s="T166">pro.h:R</ta>
            <ta e="T168" id="Seg_2386" s="T167">0.3.h:A</ta>
            <ta e="T169" id="Seg_2387" s="T168">adv:Time</ta>
            <ta e="T171" id="Seg_2388" s="T170">pro.h:E</ta>
            <ta e="T172" id="Seg_2389" s="T171">0.3.h:A</ta>
            <ta e="T174" id="Seg_2390" s="T173">pro.h:A</ta>
            <ta e="T176" id="Seg_2391" s="T175">np:Th</ta>
            <ta e="T179" id="Seg_2392" s="T178">0.3.h:A</ta>
            <ta e="T180" id="Seg_2393" s="T182">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_2394" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_2395" s="T2">v:pred</ta>
            <ta e="T4" id="Seg_2396" s="T3">np:O</ta>
            <ta e="T7" id="Seg_2397" s="T6">v:pred 0.3.h:S</ta>
            <ta e="T8" id="Seg_2398" s="T7">np:O</ta>
            <ta e="T10" id="Seg_2399" s="T9">v:pred 0.3.h:S</ta>
            <ta e="T12" id="Seg_2400" s="T11">pro:O</ta>
            <ta e="T14" id="Seg_2401" s="T13">v:pred</ta>
            <ta e="T16" id="Seg_2402" s="T15">conv:pred</ta>
            <ta e="T17" id="Seg_2403" s="T16">v:pred</ta>
            <ta e="T18" id="Seg_2404" s="T17">np.h:S</ta>
            <ta e="T20" id="Seg_2405" s="T19">pro.h:S</ta>
            <ta e="T21" id="Seg_2406" s="T20">v:pred</ta>
            <ta e="T25" id="Seg_2407" s="T24">v:pred 0.1.h:S</ta>
            <ta e="T27" id="Seg_2408" s="T26">v:pred 0.3.h:S</ta>
            <ta e="T28" id="Seg_2409" s="T27">v:pred 0.3.h:S</ta>
            <ta e="T29" id="Seg_2410" s="T28">v:pred 0.3.h:S</ta>
            <ta e="T30" id="Seg_2411" s="T29">v:pred 0.3.h:S</ta>
            <ta e="T31" id="Seg_2412" s="T30">v:pred 0.3.h:S</ta>
            <ta e="T33" id="Seg_2413" s="T32">pro.h:O</ta>
            <ta e="T34" id="Seg_2414" s="T33">ptcl.neg</ta>
            <ta e="T35" id="Seg_2415" s="T34">v:pred 0.3.h:S</ta>
            <ta e="T37" id="Seg_2416" s="T36">pro.h:S</ta>
            <ta e="T40" id="Seg_2417" s="T39">adj:pred</ta>
            <ta e="T43" id="Seg_2418" s="T42">v:pred 0.3.h:S</ta>
            <ta e="T48" id="Seg_2419" s="T47">np.h:S</ta>
            <ta e="T49" id="Seg_2420" s="T48">v:pred</ta>
            <ta e="T52" id="Seg_2421" s="T51">v:pred 0.2.h:S</ta>
            <ta e="T54" id="Seg_2422" s="T53">pro.h:S</ta>
            <ta e="T55" id="Seg_2423" s="T54">v:pred</ta>
            <ta e="T57" id="Seg_2424" s="T56">np:O</ta>
            <ta e="T58" id="Seg_2425" s="T57">v:pred 0.2.h:S</ta>
            <ta e="T59" id="Seg_2426" s="T58">pro.h:S</ta>
            <ta e="T60" id="Seg_2427" s="T59">v:pred</ta>
            <ta e="T62" id="Seg_2428" s="T61">pro:O</ta>
            <ta e="T64" id="Seg_2429" s="T63">v:pred 0.3.h:S</ta>
            <ta e="T65" id="Seg_2430" s="T64">np:O</ta>
            <ta e="T67" id="Seg_2431" s="T66">v:pred 0.2.h:S</ta>
            <ta e="T68" id="Seg_2432" s="T67">v:pred 0.1.h:S</ta>
            <ta e="T70" id="Seg_2433" s="T69">v:pred 0.3.h:S</ta>
            <ta e="T71" id="Seg_2434" s="T70">v:pred 0.3.h:S</ta>
            <ta e="T73" id="Seg_2435" s="T72">pro.h:S</ta>
            <ta e="T75" id="Seg_2436" s="T74">v:pred</ta>
            <ta e="T76" id="Seg_2437" s="T75">pro.h:S</ta>
            <ta e="T78" id="Seg_2438" s="T77">v:pred</ta>
            <ta e="T79" id="Seg_2439" s="T78">np:O</ta>
            <ta e="T81" id="Seg_2440" s="T80">v:pred 0.3.h:S</ta>
            <ta e="T84" id="Seg_2441" s="T83">pro.h:S</ta>
            <ta e="T85" id="Seg_2442" s="T84">v:pred</ta>
            <ta e="T86" id="Seg_2443" s="T85">v:pred 0.2.h:S</ta>
            <ta e="T87" id="Seg_2444" s="T86">np:O</ta>
            <ta e="T89" id="Seg_2445" s="T88">v:pred 0.3.h:S</ta>
            <ta e="T91" id="Seg_2446" s="T90">np:O</ta>
            <ta e="T92" id="Seg_2447" s="T91">pro.h:S</ta>
            <ta e="T94" id="Seg_2448" s="T93">np:O</ta>
            <ta e="T95" id="Seg_2449" s="T94">ptcl.neg</ta>
            <ta e="T96" id="Seg_2450" s="T95">v:pred</ta>
            <ta e="T98" id="Seg_2451" s="T97">ptcl:pred</ta>
            <ta e="T100" id="Seg_2452" s="T99">np:S</ta>
            <ta e="T101" id="Seg_2453" s="T100">v:pred 0.3.h:S</ta>
            <ta e="T103" id="Seg_2454" s="T102">v:pred 0.3.h:S</ta>
            <ta e="T104" id="Seg_2455" s="T103">np:O</ta>
            <ta e="T105" id="Seg_2456" s="T104">pro.h:S</ta>
            <ta e="T106" id="Seg_2457" s="T105">v:pred</ta>
            <ta e="T109" id="Seg_2458" s="T108">pro:S</ta>
            <ta e="T111" id="Seg_2459" s="T110">adj:pred</ta>
            <ta e="T116" id="Seg_2460" s="T115">v:pred 0.3.h:S</ta>
            <ta e="T117" id="Seg_2461" s="T116">np:O</ta>
            <ta e="T118" id="Seg_2462" s="T117">v:pred 0.3.h:S</ta>
            <ta e="T120" id="Seg_2463" s="T119">ptcl.neg</ta>
            <ta e="T121" id="Seg_2464" s="T120">v:pred 0.1.h:S</ta>
            <ta e="T126" id="Seg_2465" s="T125">np:O</ta>
            <ta e="T128" id="Seg_2466" s="T127">v:pred 0.3.h:S</ta>
            <ta e="T129" id="Seg_2467" s="T128">pro.h:S</ta>
            <ta e="T130" id="Seg_2468" s="T129">v:pred</ta>
            <ta e="T131" id="Seg_2469" s="T130">v:pred 0.3.h:S</ta>
            <ta e="T133" id="Seg_2470" s="T132">v:pred 0.3.h:S</ta>
            <ta e="T134" id="Seg_2471" s="T133">pro.h:S</ta>
            <ta e="T136" id="Seg_2472" s="T135">np:O</ta>
            <ta e="T137" id="Seg_2473" s="T136">ptcl.neg</ta>
            <ta e="T139" id="Seg_2474" s="T138">v:pred</ta>
            <ta e="T140" id="Seg_2475" s="T139">v:pred 0.2.h:S</ta>
            <ta e="T142" id="Seg_2476" s="T141">np:O</ta>
            <ta e="T144" id="Seg_2477" s="T143">v:pred 0.3.h:S</ta>
            <ta e="T145" id="Seg_2478" s="T144">np:O</ta>
            <ta e="T146" id="Seg_2479" s="T145">s:purp</ta>
            <ta e="T147" id="Seg_2480" s="T146">pro.h:O</ta>
            <ta e="T148" id="Seg_2481" s="T147">pro.h:S</ta>
            <ta e="T149" id="Seg_2482" s="T148">v:pred</ta>
            <ta e="T153" id="Seg_2483" s="T152">np.h:S</ta>
            <ta e="T154" id="Seg_2484" s="T153">conv:pred</ta>
            <ta e="T155" id="Seg_2485" s="T154">pro.h:S</ta>
            <ta e="T156" id="Seg_2486" s="T155">v:pred</ta>
            <ta e="T158" id="Seg_2487" s="T157">np:O</ta>
            <ta e="T159" id="Seg_2488" s="T158">v:pred 0.2.h:S</ta>
            <ta e="T161" id="Seg_2489" s="T160">pro.h:O</ta>
            <ta e="T162" id="Seg_2490" s="T161">v:pred 0.3.h:S</ta>
            <ta e="T163" id="Seg_2491" s="T162">np:O</ta>
            <ta e="T164" id="Seg_2492" s="T163">v:pred 0.3.h:S</ta>
            <ta e="T166" id="Seg_2493" s="T165">np:O</ta>
            <ta e="T168" id="Seg_2494" s="T167">v:pred 0.3.h:S</ta>
            <ta e="T171" id="Seg_2495" s="T170">pro.h:O</ta>
            <ta e="T172" id="Seg_2496" s="T171">v:pred 0.3.h:S</ta>
            <ta e="T174" id="Seg_2497" s="T173">pro.h:S</ta>
            <ta e="T175" id="Seg_2498" s="T174">v:pred</ta>
            <ta e="T176" id="Seg_2499" s="T175">np:O</ta>
            <ta e="T179" id="Seg_2500" s="T178">v:pred 0.3.h:S</ta>
            <ta e="T182" id="Seg_2501" s="T179">conv:pred</ta>
            <ta e="T180" id="Seg_2502" s="T182">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T5" id="Seg_2503" s="T4">RUS:gram</ta>
            <ta e="T8" id="Seg_2504" s="T7">RUS:cult</ta>
            <ta e="T22" id="Seg_2505" s="T21">TURK:cult</ta>
            <ta e="T23" id="Seg_2506" s="T22">RUS:cult</ta>
            <ta e="T24" id="Seg_2507" s="T23">RUS:gram</ta>
            <ta e="T56" id="Seg_2508" s="T55">TURK:cult</ta>
            <ta e="T57" id="Seg_2509" s="T56">RUS:cult</ta>
            <ta e="T61" id="Seg_2510" s="T60">RUS:disc</ta>
            <ta e="T79" id="Seg_2511" s="T78">RUS:cult</ta>
            <ta e="T98" id="Seg_2512" s="T97">RUS:mod</ta>
            <ta e="T102" id="Seg_2513" s="T101">RUS:cult</ta>
            <ta e="T107" id="Seg_2514" s="T106">RUS:gram</ta>
            <ta e="T112" id="Seg_2515" s="T111">RUS:disc</ta>
            <ta e="T117" id="Seg_2516" s="T116">RUS:cult</ta>
            <ta e="T136" id="Seg_2517" s="T135">TURK:disc</ta>
            <ta e="T138" id="Seg_2518" s="T137">TURK:core</ta>
            <ta e="T142" id="Seg_2519" s="T141">RUS:cult</ta>
            <ta e="T145" id="Seg_2520" s="T144">RUS:cult</ta>
            <ta e="T165" id="Seg_2521" s="T164">RUS:gram</ta>
            <ta e="T166" id="Seg_2522" s="T165">TURK:disc</ta>
            <ta e="T170" id="Seg_2523" s="T169">TURK:disc</ta>
            <ta e="T173" id="Seg_2524" s="T172">RUS:gram</ta>
            <ta e="T177" id="Seg_2525" s="T176">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_2526" s="T0">Один человек копал землю.</ta>
            <ta e="T8" id="Seg_2527" s="T4">И там нашел золото.</ta>
            <ta e="T14" id="Seg_2528" s="T8">Тогда он думает: "Куда мне его деть?</ta>
            <ta e="T18" id="Seg_2529" s="T14">Люди его у меня отберут.</ta>
            <ta e="T25" id="Seg_2530" s="T18">Тогда я пойду в сад к царю и отдам [его]".</ta>
            <ta e="T30" id="Seg_2531" s="T25">Он шел-шел, идет-идет.</ta>
            <ta e="T35" id="Seg_2532" s="T30">Пришел туда, его не пускают.</ta>
            <ta e="T38" id="Seg_2533" s="T35">"Зачем ты пришел?"</ta>
            <ta e="T41" id="Seg_2534" s="T38">"Да надо мне!"</ta>
            <ta e="T46" id="Seg_2535" s="T41">Тогда пустил его один, третий, четвертый [сторож].</ta>
            <ta e="T50" id="Seg_2536" s="T46">А один стоит в дверях.</ta>
            <ta e="T52" id="Seg_2537" s="T50">"Зачем ты пришел?"</ta>
            <ta e="T57" id="Seg_2538" s="T52">"Я несу царю золото".</ta>
            <ta e="T58" id="Seg_2539" s="T57">"Покажи!"</ta>
            <ta e="T60" id="Seg_2540" s="T58">Он показал.</ta>
            <ta e="T64" id="Seg_2541" s="T60">"Ну, что он тебе даст?</ta>
            <ta e="T67" id="Seg_2542" s="T64">Дай мне половину!"</ta>
            <ta e="T68" id="Seg_2543" s="T67">"Я дам!"</ta>
            <ta e="T70" id="Seg_2544" s="T68">Потом он пошел.</ta>
            <ta e="T75" id="Seg_2545" s="T70">Говорит: "Зачем ты ко (мне?) пришел?"</ta>
            <ta e="T79" id="Seg_2546" s="T75">"Я тебе принес золото".</ta>
            <ta e="T81" id="Seg_2547" s="T79">Дал ему.</ta>
            <ta e="T85" id="Seg_2548" s="T81">"Что тебе дать?"</ta>
            <ta e="T87" id="Seg_2549" s="T85">"Дай хлеб!"</ta>
            <ta e="T91" id="Seg_2550" s="T87">Ему принесли белый хлеб.</ta>
            <ta e="T96" id="Seg_2551" s="T91">"Я такой хлеб не ем.</ta>
            <ta e="T100" id="Seg_2552" s="T96">Мне надо черный хлеб".</ta>
            <ta e="T102" id="Seg_2553" s="T100">Они пошли к нищему.</ta>
            <ta e="T106" id="Seg_2554" s="T102">Взяли хлеб, он поел.</ta>
            <ta e="T111" id="Seg_2555" s="T106">"А теперь что тебе надо?"</ta>
            <ta e="T113" id="Seg_2556" s="T111">"Поспать".</ta>
            <ta e="T117" id="Seg_2557" s="T113">Тогда ему принесли перины.</ta>
            <ta e="T122" id="Seg_2558" s="T117">Он говорит: "Я не могу на этом спать.</ta>
            <ta e="T126" id="Seg_2559" s="T122">Принесите мне соломы".</ta>
            <ta e="T128" id="Seg_2560" s="T126">Ему принесли.</ta>
            <ta e="T131" id="Seg_2561" s="T128">Он лег, поспал.</ta>
            <ta e="T139" id="Seg_2562" s="T131">Потом говорит: "Я все здесь сделал плохо.</ta>
            <ta e="T142" id="Seg_2563" s="T139">Принесите мне розог".</ta>
            <ta e="T147" id="Seg_2564" s="T142">Тогда они принесли розги, чтобы побить его.</ta>
            <ta e="T149" id="Seg_2565" s="T147">Он говорит:</ta>
            <ta e="T154" id="Seg_2566" s="T149">"Там у двери стоит твой человек.</ta>
            <ta e="T159" id="Seg_2567" s="T154">Он говорит [=сказал]: дай мне половину [от того, что царь тебе даст]".</ta>
            <ta e="T164" id="Seg_2568" s="T159">Тогда они его побили, дали ему половину.</ta>
            <ta e="T168" id="Seg_2569" s="T164">И все ему дали.</ta>
            <ta e="T172" id="Seg_2570" s="T168">Он [=они?] его побил.</ta>
            <ta e="T179" id="Seg_2571" s="T172">А он взял свою шапку и убежал.</ta>
            <ta e="T180" id="Seg_2572" s="T179">Ушел.</ta>
            <ta e="T181" id="Seg_2573" s="T180">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_2574" s="T0">One man dug the ground.</ta>
            <ta e="T8" id="Seg_2575" s="T4">And found gold there.</ta>
            <ta e="T14" id="Seg_2576" s="T8">Then he thinks: "Where to make [=put] it?</ta>
            <ta e="T18" id="Seg_2577" s="T14">People will take [them from] me (grabbing).</ta>
            <ta e="T25" id="Seg_2578" s="T18">Then I will go to the chief's garden and I will give [it]."</ta>
            <ta e="T30" id="Seg_2579" s="T25">Then he went, he went, he goes, he goes.</ta>
            <ta e="T35" id="Seg_2580" s="T30">He came there, they did not let him.</ta>
            <ta e="T38" id="Seg_2581" s="T35">"What did you come for?"</ta>
            <ta e="T41" id="Seg_2582" s="T38">"I need to!"</ta>
            <ta e="T46" id="Seg_2583" s="T41">Then one sentinel let [him in], and the [second,] thirth and forth sentinels [allowed him in].</ta>
            <ta e="T50" id="Seg_2584" s="T46">Then one stands on the door.</ta>
            <ta e="T52" id="Seg_2585" s="T50">"Why did you come?"</ta>
            <ta e="T57" id="Seg_2586" s="T52">"I am bringing gold to the chief."</ta>
            <ta e="T58" id="Seg_2587" s="T57">"Show [it]!"</ta>
            <ta e="T60" id="Seg_2588" s="T58">He showed.</ta>
            <ta e="T64" id="Seg_2589" s="T60">"Well, what will he give to you?</ta>
            <ta e="T67" id="Seg_2590" s="T64">Give me half of it!"</ta>
            <ta e="T68" id="Seg_2591" s="T67">"I will give!"</ta>
            <ta e="T70" id="Seg_2592" s="T68">Then he came.</ta>
            <ta e="T75" id="Seg_2593" s="T70">He says: "Why did you come to (me?)?"</ta>
            <ta e="T79" id="Seg_2594" s="T75">"I brought you gold."</ta>
            <ta e="T81" id="Seg_2595" s="T79">He gave [it] to him.</ta>
            <ta e="T85" id="Seg_2596" s="T81">"What should I to give to you?"</ta>
            <ta e="T87" id="Seg_2597" s="T85">"Give [me] bread!"</ta>
            <ta e="T91" id="Seg_2598" s="T87">They brought him white bread.</ta>
            <ta e="T96" id="Seg_2599" s="T91">"I do not eat such bread.</ta>
            <ta e="T100" id="Seg_2600" s="T96">I need black bread."</ta>
            <ta e="T102" id="Seg_2601" s="T100">They went to a beggar.</ta>
            <ta e="T106" id="Seg_2602" s="T102">They took bread, he ate.</ta>
            <ta e="T111" id="Seg_2603" s="T106">"And now what do you need?"</ta>
            <ta e="T113" id="Seg_2604" s="T111">"To sleep."</ta>
            <ta e="T117" id="Seg_2605" s="T113">Then they brought him a feather bed.</ta>
            <ta e="T122" id="Seg_2606" s="T117">He says: "I cannot sleep on this.</ta>
            <ta e="T126" id="Seg_2607" s="T122">Bring me straw."</ta>
            <ta e="T128" id="Seg_2608" s="T126">They brought him [it].</ta>
            <ta e="T131" id="Seg_2609" s="T128">He lied down, slept.</ta>
            <ta e="T139" id="Seg_2610" s="T131">Then he says: "I did everything bad here.</ta>
            <ta e="T142" id="Seg_2611" s="T139">Bring me scourges."</ta>
            <ta e="T147" id="Seg_2612" s="T142">They brought him scourges to beat him.</ta>
            <ta e="T149" id="Seg_2613" s="T147">He says:</ta>
            <ta e="T154" id="Seg_2614" s="T149">"There stood your man at the door.</ta>
            <ta e="T159" id="Seg_2615" s="T154">He says [=said]: give me half of [what the czar will give you]."</ta>
            <ta e="T164" id="Seg_2616" s="T159">Then they beat [the nobleman], gave him a half.</ta>
            <ta e="T168" id="Seg_2617" s="T164">And gave him everything.</ta>
            <ta e="T172" id="Seg_2618" s="T168">Then he [=they?] beat him.</ta>
            <ta e="T179" id="Seg_2619" s="T172">But he took his hat and ran away.</ta>
            <ta e="T180" id="Seg_2620" s="T179">He left.</ta>
            <ta e="T181" id="Seg_2621" s="T180">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_2622" s="T0">Ein Mann grub in der Erde.</ta>
            <ta e="T8" id="Seg_2623" s="T4">Und fand dort Gold.</ta>
            <ta e="T14" id="Seg_2624" s="T8">Dann denkt er: "Wohin es machen [=tun]?</ta>
            <ta e="T18" id="Seg_2625" s="T14">Die Leute werden [es von] mir nehmen (greifend).</ta>
            <ta e="T25" id="Seg_2626" s="T18">Dann werde ich zum Garten des Zaren gehen und [es ihm] geben."</ta>
            <ta e="T30" id="Seg_2627" s="T25">Dann ging und ging er, er geht und geht.</ta>
            <ta e="T35" id="Seg_2628" s="T30">Er kam dorthin, sie ließen ihn nicht.</ta>
            <ta e="T38" id="Seg_2629" s="T35">"Wozu bist du gekommen?"</ta>
            <ta e="T41" id="Seg_2630" s="T38">"Ich musste!"</ta>
            <ta e="T46" id="Seg_2631" s="T41">Dann ließ [ihn] eine Wache und die [zweite], dritte und vierte Wache [ließen ihn].</ta>
            <ta e="T50" id="Seg_2632" s="T46">Dann steht eine an der Tür.</ta>
            <ta e="T52" id="Seg_2633" s="T50">"Wozu bist du gekommen?"</ta>
            <ta e="T57" id="Seg_2634" s="T52">"Ich bringe dem Zaren Gold."</ta>
            <ta e="T58" id="Seg_2635" s="T57">"Zeig [es]!"</ta>
            <ta e="T60" id="Seg_2636" s="T58">Er zeigte [es].</ta>
            <ta e="T64" id="Seg_2637" s="T60">"Nun, was wird er dir geben?</ta>
            <ta e="T67" id="Seg_2638" s="T64">Gib mir die Hälfte davon!"</ta>
            <ta e="T68" id="Seg_2639" s="T67">"Gebe ich!"</ta>
            <ta e="T70" id="Seg_2640" s="T68">Dann kam er.</ta>
            <ta e="T75" id="Seg_2641" s="T70">Er sagt: "Warum kamst du zu (mir?)?"</ta>
            <ta e="T79" id="Seg_2642" s="T75">"Ich brachte dir Gold."</ta>
            <ta e="T81" id="Seg_2643" s="T79">Er gab [es] ihm.</ta>
            <ta e="T85" id="Seg_2644" s="T81">"Was soll ich dir geben?"</ta>
            <ta e="T87" id="Seg_2645" s="T85">"Gib [mir] Brot!"</ta>
            <ta e="T91" id="Seg_2646" s="T87">Sie brachten ihm weißes Brot.</ta>
            <ta e="T96" id="Seg_2647" s="T91">"Ich esse nicht so ein Brot.</ta>
            <ta e="T100" id="Seg_2648" s="T96">Ich brauche Schwarzbrot."</ta>
            <ta e="T102" id="Seg_2649" s="T100">Sie gingen zu einem Bettler.</ta>
            <ta e="T106" id="Seg_2650" s="T102">Sie nahmen Brot, er aß.</ta>
            <ta e="T111" id="Seg_2651" s="T106">"Und was brauchst du jetzt?"</ta>
            <ta e="T113" id="Seg_2652" s="T111">"Schlafen."</ta>
            <ta e="T117" id="Seg_2653" s="T113">Dann brachten sie ihm ein Federbett.</ta>
            <ta e="T122" id="Seg_2654" s="T117">Er sagt: "Ich kann darauf nicht schlafen.</ta>
            <ta e="T126" id="Seg_2655" s="T122">Bringt mir Stroh."</ta>
            <ta e="T128" id="Seg_2656" s="T126">Sie brachten [es] ihm.</ta>
            <ta e="T131" id="Seg_2657" s="T128">Er legte sich hin, schlief.</ta>
            <ta e="T139" id="Seg_2658" s="T131">Dann sagt er: "Ich habe hier alles schlecht gemacht.</ta>
            <ta e="T142" id="Seg_2659" s="T139">Bringt mir Geißeln."</ta>
            <ta e="T147" id="Seg_2660" s="T142">Sie brachten ihm Geißeln, um ihn zu schlagen.</ta>
            <ta e="T149" id="Seg_2661" s="T147">Er sagt:</ta>
            <ta e="T154" id="Seg_2662" s="T149">"Dort steht dein Mann an der Tür.</ta>
            <ta e="T159" id="Seg_2663" s="T154">Er sagt [=sagte]: "Gib mir die Hälfte [von dem, was der Zar dir gibt]."</ta>
            <ta e="T164" id="Seg_2664" s="T159">Dann schlagen sie [den Edelmann], gaben eine Hälfte.</ta>
            <ta e="T168" id="Seg_2665" s="T164">Und gaben ihm alles.</ta>
            <ta e="T172" id="Seg_2666" s="T168">Dann schlug ihn er [= sie?].</ta>
            <ta e="T179" id="Seg_2667" s="T172">Aber er nahm seinen Hut und rannte davon.</ta>
            <ta e="T180" id="Seg_2668" s="T179">Er ging fort.</ta>
            <ta e="T181" id="Seg_2669" s="T180">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T4" id="Seg_2670" s="T0">[GVY:] The version of the tale, that is presented here, see e.g. http://sokrnarmira.ru/index/0-7468</ta>
            <ta e="T50" id="Seg_2671" s="T46">[GVY:] In the tale, the last sentinel that stood at the czar's door, was a general or a nobleman. || ‎‎T.: ajeːndə</ta>
            <ta e="T67" id="Seg_2672" s="T64">[GVY:] mĭleʔ - imperative 2PL?</ta>
            <ta e="T75" id="Seg_2673" s="T70">[GVY:] "me" and "you" are mixed up?</ta>
            <ta e="T102" id="Seg_2674" s="T100">[GVY:] They couldn't find black bread in the czar's palace and went to a beggar that stood at the street.</ta>
            <ta e="T126" id="Seg_2675" s="T122">[GVY:] Detəgeʔ = deʔkeʔ?</ta>
            <ta e="T131" id="Seg_2676" s="T128">[GVY:] "he was lying and sleeping for some time"?</ta>
            <ta e="T168" id="Seg_2677" s="T164">[GVY:] Evidently, the nobleman received the scourges in full.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T182" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
