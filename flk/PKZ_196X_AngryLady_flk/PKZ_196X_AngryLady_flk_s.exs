<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID26F25945-565C-B5FE-5AA1-4D75ED9A5EB0">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_AngryLady_flk.wav" />
         <referenced-file url="PKZ_196X_AngryLady_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_AngryLady_flk\PKZ_196X_AngryLady_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">309</ud-information>
            <ud-information attribute-name="# HIAT:w">201</ud-information>
            <ud-information attribute-name="# e">201</ud-information>
            <ud-information attribute-name="# HIAT:u">49</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.027" type="appl" />
         <tli id="T1" time="0.711" type="appl" />
         <tli id="T2" time="1.3950000443294583" />
         <tli id="T3" time="2.0869998973502764" />
         <tli id="T4" time="2.836" type="appl" />
         <tli id="T5" time="3.593" type="appl" />
         <tli id="T6" time="4.349" type="appl" />
         <tli id="T7" time="5.106" type="appl" />
         <tli id="T8" time="6.579728354790822" />
         <tli id="T9" time="7.381" type="appl" />
         <tli id="T10" time="8.042" type="appl" />
         <tli id="T11" time="8.703" type="appl" />
         <tli id="T12" time="9.365" type="appl" />
         <tli id="T13" time="10.026" type="appl" />
         <tli id="T14" time="10.687" type="appl" />
         <tli id="T15" time="11.355999413376828" />
         <tli id="T16" time="12.516" type="appl" />
         <tli id="T17" time="13.489" type="appl" />
         <tli id="T18" time="14.463" type="appl" />
         <tli id="T19" time="15.436" type="appl" />
         <tli id="T20" time="16.479319648473062" />
         <tli id="T21" time="17.148" type="appl" />
         <tli id="T22" time="17.795" type="appl" />
         <tli id="T23" time="18.442" type="appl" />
         <tli id="T24" time="19.159209008783" />
         <tli id="T25" time="19.767" type="appl" />
         <tli id="T26" time="20.445" type="appl" />
         <tli id="T27" time="21.122" type="appl" />
         <tli id="T28" time="22.149" type="appl" />
         <tli id="T29" time="23.12" type="appl" />
         <tli id="T30" time="24.33899516042684" />
         <tli id="T31" time="25.85893240955785" />
         <tli id="T32" time="26.95" type="appl" />
         <tli id="T33" time="27.828" type="appl" />
         <tli id="T34" time="28.706" type="appl" />
         <tli id="T35" time="29.583" type="appl" />
         <tli id="T36" time="30.46" type="appl" />
         <tli id="T37" time="31.338" type="appl" />
         <tli id="T38" time="32.336" type="appl" />
         <tli id="T39" time="33.334" type="appl" />
         <tli id="T40" time="33.996" type="appl" />
         <tli id="T41" time="34.659" type="appl" />
         <tli id="T42" time="35.321" type="appl" />
         <tli id="T43" time="35.984" type="appl" />
         <tli id="T44" time="36.646" type="appl" />
         <tli id="T45" time="37.308" type="appl" />
         <tli id="T46" time="37.971" type="appl" />
         <tli id="T47" time="39.34504229987379" />
         <tli id="T48" time="40.133" type="appl" />
         <tli id="T49" time="40.81" type="appl" />
         <tli id="T50" time="41.487" type="appl" />
         <tli id="T51" time="42.164" type="appl" />
         <tli id="T52" time="42.841" type="appl" />
         <tli id="T53" time="43.65819756385521" />
         <tli id="T54" time="44.273" type="appl" />
         <tli id="T55" time="45.028" type="appl" />
         <tli id="T56" time="45.79633194686194" />
         <tli id="T57" time="46.27" type="appl" />
         <tli id="T58" time="46.756" type="appl" />
         <tli id="T59" time="47.243" type="appl" />
         <tli id="T60" time="48.79798536683771" />
         <tli id="T61" time="49.867" type="appl" />
         <tli id="T62" time="50.731" type="appl" />
         <tli id="T63" time="51.594" type="appl" />
         <tli id="T64" time="52.458" type="appl" />
         <tli id="T65" time="53.322" type="appl" />
         <tli id="T66" time="54.6044123141759" />
         <tli id="T67" time="55.665" type="appl" />
         <tli id="T68" time="57.28430167448585" />
         <tli id="T69" time="58.332" type="appl" />
         <tli id="T70" time="59.181" type="appl" />
         <tli id="T71" time="60.03" type="appl" />
         <tli id="T72" time="61.730784767338406" />
         <tli id="T73" time="62.51" type="appl" />
         <tli id="T74" time="63.281" type="appl" />
         <tli id="T75" time="64.369" type="appl" />
         <tli id="T76" time="65.457" type="appl" />
         <tli id="T77" time="66.545" type="appl" />
         <tli id="T78" time="67.459" type="appl" />
         <tli id="T79" time="68.374" type="appl" />
         <tli id="T80" time="68.895" type="appl" />
         <tli id="T81" time="69.415" type="appl" />
         <tli id="T82" time="69.936" type="appl" />
         <tli id="T83" time="70.456" type="appl" />
         <tli id="T84" time="70.977" type="appl" />
         <tli id="T85" time="71.498" type="appl" />
         <tli id="T86" time="72.018" type="appl" />
         <tli id="T87" time="72.539" type="appl" />
         <tli id="T88" time="73.059" type="appl" />
         <tli id="T89" time="73.97027945770917" />
         <tli id="T90" time="74.793" type="appl" />
         <tli id="T91" time="75.538" type="appl" />
         <tli id="T92" time="76.39684594316395" />
         <tli id="T93" time="77.473" type="appl" />
         <tli id="T94" time="78.662" type="appl" />
         <tli id="T95" time="79.851" type="appl" />
         <tli id="T96" time="81.28997726273482" />
         <tli id="T97" time="82.199" type="appl" />
         <tli id="T98" time="83.90986910005276" />
         <tli id="T99" time="84.854" type="appl" />
         <tli id="T100" time="85.512" type="appl" />
         <tli id="T101" time="86.169" type="appl" />
         <tli id="T102" time="86.827" type="appl" />
         <tli id="T103" time="87.494" type="appl" />
         <tli id="T104" time="88.161" type="appl" />
         <tli id="T105" time="88.828" type="appl" />
         <tli id="T106" time="89.495" type="appl" />
         <tli id="T107" time="90.56292776072272" />
         <tli id="T108" time="91.337" type="appl" />
         <tli id="T109" time="92.104" type="appl" />
         <tli id="T110" time="93.07615733494373" />
         <tli id="T111" time="94.05" type="appl" />
         <tli id="T112" time="94.783" type="appl" />
         <tli id="T113" time="95.517" type="appl" />
         <tli id="T114" time="96.25" type="appl" />
         <tli id="T115" time="97.08265859252153" />
         <tli id="T116" time="97.663" type="appl" />
         <tli id="T117" time="98.343" type="appl" />
         <tli id="T118" time="99.59588816674253" />
         <tli id="T119" time="100.29" type="appl" />
         <tli id="T120" time="100.82" type="appl" />
         <tli id="T121" time="101.35" type="appl" />
         <tli id="T122" time="101.89333237886576" />
         <tli id="T123" time="102.512" type="appl" />
         <tli id="T124" time="103.58905664143322" />
         <tli id="T125" time="104.152" type="appl" />
         <tli id="T126" time="104.544" type="appl" />
         <tli id="T127" time="105.19" type="appl" />
         <tli id="T128" time="105.836" type="appl" />
         <tli id="T129" time="106.602265573921" />
         <tli id="T130" time="107.611" type="appl" />
         <tli id="T131" time="108.554" type="appl" />
         <tli id="T132" time="109.497" type="appl" />
         <tli id="T133" time="110.44" type="appl" />
         <tli id="T134" time="111.34300735158683" />
         <tli id="T135" time="112.228" type="appl" />
         <tli id="T136" time="113.072" type="appl" />
         <tli id="T137" time="113.917" type="appl" />
         <tli id="T138" time="114.761" type="appl" />
         <tli id="T139" time="115.606" type="appl" />
         <tli id="T140" time="116.2" type="appl" />
         <tli id="T141" time="116.795" type="appl" />
         <tli id="T142" time="117.389" type="appl" />
         <tli id="T143" time="117.984" type="appl" />
         <tli id="T144" time="118.74843078408196" />
         <tli id="T145" time="119.577" type="appl" />
         <tli id="T146" time="120.408" type="appl" />
         <tli id="T147" time="121.214" type="appl" />
         <tli id="T148" time="122.021" type="appl" />
         <tli id="T149" time="122.827" type="appl" />
         <tli id="T150" time="123.537" type="appl" />
         <tli id="T151" time="124.074" type="appl" />
         <tli id="T152" time="125.57481562228442" />
         <tli id="T153" time="126.456" type="appl" />
         <tli id="T154" time="127.157" type="appl" />
         <tli id="T155" time="127.857" type="appl" />
         <tli id="T156" time="128.6346892948771" />
         <tli id="T157" time="129.317" type="appl" />
         <tli id="T158" time="130.076" type="appl" />
         <tli id="T159" time="130.835" type="appl" />
         <tli id="T160" time="131.594" type="appl" />
         <tli id="T161" time="132.353" type="appl" />
         <tli id="T162" time="133.2145002166008" />
         <tli id="T163" time="133.935" type="appl" />
         <tli id="T164" time="134.757" type="appl" />
         <tli id="T165" time="135.58" type="appl" />
         <tli id="T166" time="136.282" type="appl" />
         <tli id="T167" time="136.983" type="appl" />
         <tli id="T168" time="137.685" type="appl" />
         <tli id="T169" time="138.387" type="appl" />
         <tli id="T170" time="139.089" type="appl" />
         <tli id="T171" time="139.79" type="appl" />
         <tli id="T172" time="140.492" type="appl" />
         <tli id="T173" time="141.74" type="appl" />
         <tli id="T174" time="142.988" type="appl" />
         <tli id="T175" time="144.236" type="appl" />
         <tli id="T176" time="144.765" type="appl" />
         <tli id="T177" time="145.295" type="appl" />
         <tli id="T178" time="145.824" type="appl" />
         <tli id="T179" time="146.353" type="appl" />
         <tli id="T180" time="146.882" type="appl" />
         <tli id="T181" time="147.412" type="appl" />
         <tli id="T182" time="148.4472046651287" />
         <tli id="T183" time="149.311" type="appl" />
         <tli id="T184" time="149.941" type="appl" />
         <tli id="T185" time="150.571" type="appl" />
         <tli id="T186" time="151.202" type="appl" />
         <tli id="T187" time="151.832" type="appl" />
         <tli id="T188" time="152.462" type="appl" />
         <tli id="T189" time="153.1936753729413" />
         <tli id="T190" time="153.816" type="appl" />
         <tli id="T191" time="154.392" type="appl" />
         <tli id="T192" time="154.968" type="appl" />
         <tli id="T193" time="155.544" type="appl" />
         <tli id="T194" time="156.12" type="appl" />
         <tli id="T195" time="156.696" type="appl" />
         <tli id="T196" time="157.298" type="appl" />
         <tli id="T197" time="157.9" type="appl" />
         <tli id="T198" time="158.501" type="appl" />
         <tli id="T199" time="159.103" type="appl" />
         <tli id="T200" time="159.452" type="appl" />
         <tli id="T201" time="159.8733995993855" />
         <tli id="T202" time="161.1" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T202" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Ibi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">kurojok</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">ne</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Di</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">üge</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">kurolaʔpi</ts>
                  <nts id="Seg_23" n="HIAT:ip">,</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">tibizeŋ</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">münörlaʔpi</ts>
                  <nts id="Seg_30" n="HIAT:ip">.</nts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_33" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_35" n="HIAT:w" s="T8">Tarəsta</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_38" n="HIAT:w" s="T9">šoləj</ts>
                  <nts id="Seg_39" n="HIAT:ip">,</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_42" n="HIAT:w" s="T10">dĭ</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_45" n="HIAT:w" s="T11">bar</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_48" n="HIAT:w" s="T12">kirgarlaʔbə</ts>
                  <nts id="Seg_49" n="HIAT:ip">,</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">münörləj</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">dĭm</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_59" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_61" n="HIAT:w" s="T15">Ugaːndə</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">kurojok</ts>
                  <nts id="Seg_65" n="HIAT:ip">,</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_68" n="HIAT:w" s="T17">il</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_71" n="HIAT:w" s="T18">pimnieʔi</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_74" n="HIAT:w" s="T19">dĭgəʔ</ts>
                  <nts id="Seg_75" n="HIAT:ip">.</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_78" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_80" n="HIAT:w" s="T20">Dĭgəttə</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_83" n="HIAT:w" s="T21">šobi</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_86" n="HIAT:w" s="T22">onʼiʔ</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_89" n="HIAT:w" s="T23">soldat</ts>
                  <nts id="Seg_90" n="HIAT:ip">.</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_93" n="HIAT:u" s="T24">
                  <nts id="Seg_94" n="HIAT:ip">"</nts>
                  <ts e="T25" id="Seg_96" n="HIAT:w" s="T24">Măn</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_99" n="HIAT:w" s="T25">dĭm</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_102" n="HIAT:w" s="T26">tüšüʔlim</ts>
                  <nts id="Seg_103" n="HIAT:ip">"</nts>
                  <nts id="Seg_104" n="HIAT:ip">.</nts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_107" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_109" n="HIAT:w" s="T27">Nüdʼin</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_112" n="HIAT:w" s="T28">körerbi</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_115" n="HIAT:w" s="T29">inezeŋdə</ts>
                  <nts id="Seg_116" n="HIAT:ip">.</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_119" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_121" n="HIAT:w" s="T30">Šobi</ts>
                  <nts id="Seg_122" n="HIAT:ip">.</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_125" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_127" n="HIAT:w" s="T31">Dĭ</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_130" n="HIAT:w" s="T32">dĭm</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_133" n="HIAT:w" s="T33">iluʔpi</ts>
                  <nts id="Seg_134" n="HIAT:ip">,</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_136" n="HIAT:ip">(</nts>
                  <ts e="T35" id="Seg_138" n="HIAT:w" s="T34">ine=</ts>
                  <nts id="Seg_139" n="HIAT:ip">)</nts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_142" n="HIAT:w" s="T35">ineʔinə</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_145" n="HIAT:w" s="T36">embi</ts>
                  <nts id="Seg_146" n="HIAT:ip">.</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_149" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_151" n="HIAT:w" s="T37">Deʔpi</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_154" n="HIAT:w" s="T38">sapožniktə</ts>
                  <nts id="Seg_155" n="HIAT:ip">.</nts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_158" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_160" n="HIAT:w" s="T39">Sapožnikən</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_163" n="HIAT:w" s="T40">nebə</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_166" n="HIAT:w" s="T41">deʔpi</ts>
                  <nts id="Seg_167" n="HIAT:ip">,</nts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_170" n="HIAT:w" s="T42">döbər</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_173" n="HIAT:w" s="T43">gijen</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_176" n="HIAT:w" s="T44">dĭ</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_179" n="HIAT:w" s="T45">barɨnʼa</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_182" n="HIAT:w" s="T46">amnobi</ts>
                  <nts id="Seg_183" n="HIAT:ip">.</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_186" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_188" n="HIAT:w" s="T47">Dĭgəttə</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_191" n="HIAT:w" s="T48">dĭ</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_194" n="HIAT:w" s="T49">ertən</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_196" n="HIAT:ip">(</nts>
                  <ts e="T51" id="Seg_198" n="HIAT:w" s="T50">uʔ-</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_201" n="HIAT:w" s="T51">uʔ-</ts>
                  <nts id="Seg_202" n="HIAT:ip">)</nts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_205" n="HIAT:w" s="T52">uʔluʔpi</ts>
                  <nts id="Seg_206" n="HIAT:ip">.</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_209" n="HIAT:u" s="T53">
                  <nts id="Seg_210" n="HIAT:ip">"</nts>
                  <ts e="T54" id="Seg_212" n="HIAT:w" s="T53">Gijen</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_215" n="HIAT:w" s="T54">măn</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_218" n="HIAT:w" s="T55">kunollaʔbəm</ts>
                  <nts id="Seg_219" n="HIAT:ip">?</nts>
                  <nts id="Seg_220" n="HIAT:ip">"</nts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_223" n="HIAT:u" s="T56">
                  <nts id="Seg_224" n="HIAT:ip">(</nts>
                  <ts e="T57" id="Seg_226" n="HIAT:w" s="T56">Dĭ=</ts>
                  <nts id="Seg_227" n="HIAT:ip">)</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_230" n="HIAT:w" s="T57">Dĭʔnə</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_233" n="HIAT:w" s="T58">bü</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_236" n="HIAT:w" s="T59">deʔpiʔi</ts>
                  <nts id="Seg_237" n="HIAT:ip">.</nts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_240" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_242" n="HIAT:w" s="T60">Deʔpiʔi</ts>
                  <nts id="Seg_243" n="HIAT:ip">,</nts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_246" n="HIAT:w" s="T61">kadəldə</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_249" n="HIAT:w" s="T62">kĭškəsʼtə</ts>
                  <nts id="Seg_250" n="HIAT:ip">,</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_252" n="HIAT:ip">(</nts>
                  <ts e="T64" id="Seg_254" n="HIAT:w" s="T63">rušnʼikziʔ</ts>
                  <nts id="Seg_255" n="HIAT:ip">)</nts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_258" n="HIAT:w" s="T64">dĭ</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_261" n="HIAT:w" s="T65">kĭškəluʔpi</ts>
                  <nts id="Seg_262" n="HIAT:ip">.</nts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_265" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_267" n="HIAT:w" s="T66">Amzittə</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_270" n="HIAT:w" s="T67">mĭbiʔi</ts>
                  <nts id="Seg_271" n="HIAT:ip">.</nts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_274" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_276" n="HIAT:w" s="T68">Kujdərgan</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_279" n="HIAT:w" s="T69">nuldəbiʔi</ts>
                  <nts id="Seg_280" n="HIAT:ip">,</nts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_283" n="HIAT:w" s="T70">segi</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_286" n="HIAT:w" s="T71">bü</ts>
                  <nts id="Seg_287" n="HIAT:ip">.</nts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_290" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_292" n="HIAT:w" s="T72">Dĭ</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_295" n="HIAT:w" s="T73">amorlaʔbə</ts>
                  <nts id="Seg_296" n="HIAT:ip">.</nts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_299" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_301" n="HIAT:w" s="T74">Šobi</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_304" n="HIAT:w" s="T75">tarəsta</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_307" n="HIAT:w" s="T76">dĭʔnə</ts>
                  <nts id="Seg_308" n="HIAT:ip">.</nts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_311" n="HIAT:u" s="T77">
                  <nts id="Seg_312" n="HIAT:ip">"</nts>
                  <ts e="T78" id="Seg_314" n="HIAT:w" s="T77">Ĭmbi</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_317" n="HIAT:w" s="T78">azittə</ts>
                  <nts id="Seg_318" n="HIAT:ip">?</nts>
                  <nts id="Seg_319" n="HIAT:ip">"</nts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_322" n="HIAT:u" s="T79">
                  <nts id="Seg_323" n="HIAT:ip">(</nts>
                  <ts e="T80" id="Seg_325" n="HIAT:w" s="T79">Ambi-</ts>
                  <nts id="Seg_326" n="HIAT:ip">)</nts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_329" n="HIAT:w" s="T80">Ĭmbi</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_332" n="HIAT:w" s="T81">taldʼen</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_335" n="HIAT:w" s="T82">abial</ts>
                  <nts id="Seg_336" n="HIAT:ip">,</nts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_339" n="HIAT:w" s="T83">teinen</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_341" n="HIAT:ip">(</nts>
                  <ts e="T85" id="Seg_343" n="HIAT:w" s="T84">i</ts>
                  <nts id="Seg_344" n="HIAT:ip">)</nts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_347" n="HIAT:w" s="T85">i</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_350" n="HIAT:w" s="T86">dĭm</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_352" n="HIAT:ip">(</nts>
                  <ts e="T88" id="Seg_354" n="HIAT:w" s="T87">alaʔ-</ts>
                  <nts id="Seg_355" n="HIAT:ip">)</nts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_358" n="HIAT:w" s="T88">aʔ</ts>
                  <nts id="Seg_359" n="HIAT:ip">.</nts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_362" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_364" n="HIAT:w" s="T89">Dĭ</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_367" n="HIAT:w" s="T90">kambi</ts>
                  <nts id="Seg_368" n="HIAT:ip">,</nts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_371" n="HIAT:w" s="T91">măndliaʔi</ts>
                  <nts id="Seg_372" n="HIAT:ip">.</nts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_375" n="HIAT:u" s="T92">
                  <nts id="Seg_376" n="HIAT:ip">"</nts>
                  <ts e="T93" id="Seg_378" n="HIAT:w" s="T92">Jakšə</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_381" n="HIAT:w" s="T93">molaːmbi</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_384" n="HIAT:w" s="T94">barɨnʼa</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_387" n="HIAT:w" s="T95">miʔ</ts>
                  <nts id="Seg_388" n="HIAT:ip">.</nts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_391" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_393" n="HIAT:w" s="T96">Ej</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_396" n="HIAT:w" s="T97">kudonzlia</ts>
                  <nts id="Seg_397" n="HIAT:ip">.</nts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_400" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_402" n="HIAT:w" s="T98">Dĭgəttə</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_405" n="HIAT:w" s="T99">ej</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_408" n="HIAT:w" s="T100">münöria</ts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_411" n="HIAT:w" s="T101">šindimdə</ts>
                  <nts id="Seg_412" n="HIAT:ip">"</nts>
                  <nts id="Seg_413" n="HIAT:ip">.</nts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_416" n="HIAT:u" s="T102">
                  <ts e="T103" id="Seg_418" n="HIAT:w" s="T102">Dĭgəttə</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_421" n="HIAT:w" s="T103">nünəbi</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_424" n="HIAT:w" s="T104">dĭ</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_426" n="HIAT:ip">(</nts>
                  <ts e="T106" id="Seg_428" n="HIAT:w" s="T105">sə-</ts>
                  <nts id="Seg_429" n="HIAT:ip">)</nts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_432" n="HIAT:w" s="T106">soldat</ts>
                  <nts id="Seg_433" n="HIAT:ip">.</nts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_436" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_438" n="HIAT:w" s="T107">Šide</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_441" n="HIAT:w" s="T108">mesʼats</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_444" n="HIAT:w" s="T109">kambi</ts>
                  <nts id="Seg_445" n="HIAT:ip">.</nts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_448" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_450" n="HIAT:w" s="T110">A</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_453" n="HIAT:w" s="T111">dĭ</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_456" n="HIAT:w" s="T112">kuznʼetsgən</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_459" n="HIAT:w" s="T113">barɨnʼa</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_462" n="HIAT:w" s="T114">amnobi</ts>
                  <nts id="Seg_463" n="HIAT:ip">.</nts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_466" n="HIAT:u" s="T115">
                  <nts id="Seg_467" n="HIAT:ip">"</nts>
                  <ts e="T116" id="Seg_469" n="HIAT:w" s="T115">Ĭmbi</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_472" n="HIAT:w" s="T116">ej</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_475" n="HIAT:w" s="T117">uʔblial</ts>
                  <nts id="Seg_476" n="HIAT:ip">?</nts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_479" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_481" n="HIAT:w" s="T118">Ĭmbi</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_484" n="HIAT:w" s="T119">pʼeš</ts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_487" n="HIAT:w" s="T120">ej</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_489" n="HIAT:ip">(</nts>
                  <ts e="T122" id="Seg_491" n="HIAT:w" s="T121">nendlie</ts>
                  <nts id="Seg_492" n="HIAT:ip">)</nts>
                  <nts id="Seg_493" n="HIAT:ip">?</nts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_496" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_498" n="HIAT:w" s="T122">Büjleʔ</ts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_501" n="HIAT:w" s="T123">kanaʔ</ts>
                  <nts id="Seg_502" n="HIAT:ip">!</nts>
                  <nts id="Seg_503" n="HIAT:ip">"</nts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T129" id="Seg_506" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_508" n="HIAT:w" s="T124">Dĭ</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_511" n="HIAT:w" s="T125">măndə:</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_513" n="HIAT:ip">"</nts>
                  <ts e="T127" id="Seg_515" n="HIAT:w" s="T126">Deʔ</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_518" n="HIAT:w" s="T127">măna</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_521" n="HIAT:w" s="T128">băzejdəsʼtə</ts>
                  <nts id="Seg_522" n="HIAT:ip">!</nts>
                  <nts id="Seg_523" n="HIAT:ip">"</nts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_526" n="HIAT:u" s="T129">
                  <ts e="T130" id="Seg_528" n="HIAT:w" s="T129">Dĭ</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_531" n="HIAT:w" s="T130">ibi</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_534" n="HIAT:w" s="T131">remendə</ts>
                  <nts id="Seg_535" n="HIAT:ip">,</nts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_538" n="HIAT:w" s="T132">münörbi</ts>
                  <nts id="Seg_539" n="HIAT:ip">,</nts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_542" n="HIAT:w" s="T133">münörbi</ts>
                  <nts id="Seg_543" n="HIAT:ip">.</nts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_546" n="HIAT:u" s="T134">
                  <ts e="T135" id="Seg_548" n="HIAT:w" s="T134">Dĭ</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_551" n="HIAT:w" s="T135">kabarbi</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_554" n="HIAT:w" s="T136">aspaʔ</ts>
                  <nts id="Seg_555" n="HIAT:ip">,</nts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_558" n="HIAT:w" s="T137">kambi</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_561" n="HIAT:w" s="T138">büjleʔ</ts>
                  <nts id="Seg_562" n="HIAT:ip">.</nts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_565" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_567" n="HIAT:w" s="T139">Ĭmbidə</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_570" n="HIAT:w" s="T140">ej</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_573" n="HIAT:w" s="T141">molia</ts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_575" n="HIAT:ip">(</nts>
                  <ts e="T143" id="Seg_577" n="HIAT:w" s="T142">oj-</ts>
                  <nts id="Seg_578" n="HIAT:ip">)</nts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_581" n="HIAT:w" s="T143">azittə</ts>
                  <nts id="Seg_582" n="HIAT:ip">.</nts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T146" id="Seg_585" n="HIAT:u" s="T144">
                  <ts e="T145" id="Seg_587" n="HIAT:w" s="T144">Amnobi</ts>
                  <nts id="Seg_588" n="HIAT:ip">,</nts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_591" n="HIAT:w" s="T145">amnobi</ts>
                  <nts id="Seg_592" n="HIAT:ip">.</nts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_595" n="HIAT:u" s="T146">
                  <ts e="T147" id="Seg_597" n="HIAT:w" s="T146">Dĭgəttə</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_600" n="HIAT:w" s="T147">bazoʔ</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_603" n="HIAT:w" s="T148">kunolluʔpiʔi</ts>
                  <nts id="Seg_604" n="HIAT:ip">.</nts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_607" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_609" n="HIAT:w" s="T149">Dĭ</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_612" n="HIAT:w" s="T150">soldat</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_615" n="HIAT:w" s="T151">šobi</ts>
                  <nts id="Seg_616" n="HIAT:ip">.</nts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_619" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_621" n="HIAT:w" s="T152">Dĭ</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_624" n="HIAT:w" s="T153">barɨnʼam</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_627" n="HIAT:w" s="T154">deʔpi</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_630" n="HIAT:w" s="T155">maːndə</ts>
                  <nts id="Seg_631" n="HIAT:ip">.</nts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T162" id="Seg_634" n="HIAT:u" s="T156">
                  <ts e="T157" id="Seg_636" n="HIAT:w" s="T156">A</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_639" n="HIAT:w" s="T157">kuznʼetsən</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_642" n="HIAT:w" s="T158">nedə</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_645" n="HIAT:w" s="T159">tože</ts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_648" n="HIAT:w" s="T160">maːndə</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_651" n="HIAT:w" s="T161">deʔpi</ts>
                  <nts id="Seg_652" n="HIAT:ip">.</nts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T165" id="Seg_655" n="HIAT:u" s="T162">
                  <ts e="T163" id="Seg_657" n="HIAT:w" s="T162">Dĭgəttə</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_660" n="HIAT:w" s="T163">ertən</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_663" n="HIAT:w" s="T164">uʔbdəbi</ts>
                  <nts id="Seg_664" n="HIAT:ip">.</nts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T172" id="Seg_667" n="HIAT:u" s="T165">
                  <ts e="T166" id="Seg_669" n="HIAT:w" s="T165">Kăda</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_672" n="HIAT:w" s="T166">măn</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_675" n="HIAT:w" s="T167">döbər</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_678" n="HIAT:w" s="T168">šobiam</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_681" n="HIAT:w" s="T169">bostə</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_683" n="HIAT:ip">(</nts>
                  <ts e="T171" id="Seg_685" n="HIAT:w" s="T170">maː-</ts>
                  <nts id="Seg_686" n="HIAT:ip">)</nts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_689" n="HIAT:w" s="T171">maːndə</ts>
                  <nts id="Seg_690" n="HIAT:ip">?</nts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T175" id="Seg_693" n="HIAT:u" s="T172">
                  <ts e="T173" id="Seg_695" n="HIAT:w" s="T172">Dĭgəttə</ts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_698" n="HIAT:w" s="T173">surarlaʔbə</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_701" n="HIAT:w" s="T174">bostə</ts>
                  <nts id="Seg_702" n="HIAT:ip">.</nts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T182" id="Seg_705" n="HIAT:u" s="T175">
                  <nts id="Seg_706" n="HIAT:ip">"</nts>
                  <ts e="T176" id="Seg_708" n="HIAT:w" s="T175">Da</ts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_711" n="HIAT:w" s="T176">tăn</ts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_714" n="HIAT:w" s="T177">gijendə</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_717" n="HIAT:w" s="T178">i</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_720" n="HIAT:w" s="T179">nagobial</ts>
                  <nts id="Seg_721" n="HIAT:ip">,</nts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_724" n="HIAT:w" s="T180">dön</ts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_727" n="HIAT:w" s="T181">amnobial</ts>
                  <nts id="Seg_728" n="HIAT:ip">"</nts>
                  <nts id="Seg_729" n="HIAT:ip">.</nts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T189" id="Seg_732" n="HIAT:u" s="T182">
                  <ts e="T183" id="Seg_734" n="HIAT:w" s="T182">Dĭgəttə</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_737" n="HIAT:w" s="T183">kuznʼetsən</ts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_739" n="HIAT:ip">(</nts>
                  <ts e="T185" id="Seg_741" n="HIAT:w" s="T184">ne=</ts>
                  <nts id="Seg_742" n="HIAT:ip">)</nts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_745" n="HIAT:w" s="T185">ne</ts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_747" n="HIAT:ip">(</nts>
                  <ts e="T187" id="Seg_749" n="HIAT:w" s="T186">ha-</ts>
                  <nts id="Seg_750" n="HIAT:ip">)</nts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_753" n="HIAT:w" s="T187">jakšə</ts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_756" n="HIAT:w" s="T188">amnolaʔbə</ts>
                  <nts id="Seg_757" n="HIAT:ip">.</nts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T195" id="Seg_760" n="HIAT:u" s="T189">
                  <ts e="T190" id="Seg_762" n="HIAT:w" s="T189">A</ts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_765" n="HIAT:w" s="T190">dĭ</ts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_768" n="HIAT:w" s="T191">ej</ts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_770" n="HIAT:ip">(</nts>
                  <ts e="T193" id="Seg_772" n="HIAT:w" s="T192">kuroluʔ-</ts>
                  <nts id="Seg_773" n="HIAT:ip">)</nts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_776" n="HIAT:w" s="T193">kurojok</ts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_779" n="HIAT:w" s="T194">molaːmbi</ts>
                  <nts id="Seg_780" n="HIAT:ip">.</nts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T199" id="Seg_783" n="HIAT:u" s="T195">
                  <ts e="T196" id="Seg_785" n="HIAT:w" s="T195">Jakšə</ts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_788" n="HIAT:w" s="T196">molaːmbi</ts>
                  <nts id="Seg_789" n="HIAT:ip">,</nts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_792" n="HIAT:w" s="T197">dĭ</ts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_795" n="HIAT:w" s="T198">barɨnʼa</ts>
                  <nts id="Seg_796" n="HIAT:ip">.</nts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T201" id="Seg_799" n="HIAT:u" s="T199">
                  <ts e="T201" id="Seg_801" n="HIAT:w" s="T199">Dĭgəttə</ts>
                  <nts id="Seg_802" n="HIAT:ip">…</nts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T202" id="Seg_805" n="HIAT:u" s="T201">
                  <ts e="T202" id="Seg_807" n="HIAT:w" s="T201">Kabarləj</ts>
                  <nts id="Seg_808" n="HIAT:ip">!</nts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T202" id="Seg_810" n="sc" s="T0">
               <ts e="T1" id="Seg_812" n="e" s="T0">Ibi </ts>
               <ts e="T2" id="Seg_814" n="e" s="T1">kurojok </ts>
               <ts e="T3" id="Seg_816" n="e" s="T2">ne. </ts>
               <ts e="T4" id="Seg_818" n="e" s="T3">Di </ts>
               <ts e="T5" id="Seg_820" n="e" s="T4">üge </ts>
               <ts e="T6" id="Seg_822" n="e" s="T5">kurolaʔpi, </ts>
               <ts e="T7" id="Seg_824" n="e" s="T6">tibizeŋ </ts>
               <ts e="T8" id="Seg_826" n="e" s="T7">münörlaʔpi. </ts>
               <ts e="T9" id="Seg_828" n="e" s="T8">Tarəsta </ts>
               <ts e="T10" id="Seg_830" n="e" s="T9">šoləj, </ts>
               <ts e="T11" id="Seg_832" n="e" s="T10">dĭ </ts>
               <ts e="T12" id="Seg_834" n="e" s="T11">bar </ts>
               <ts e="T13" id="Seg_836" n="e" s="T12">kirgarlaʔbə, </ts>
               <ts e="T14" id="Seg_838" n="e" s="T13">münörləj </ts>
               <ts e="T15" id="Seg_840" n="e" s="T14">dĭm. </ts>
               <ts e="T16" id="Seg_842" n="e" s="T15">Ugaːndə </ts>
               <ts e="T17" id="Seg_844" n="e" s="T16">kurojok, </ts>
               <ts e="T18" id="Seg_846" n="e" s="T17">il </ts>
               <ts e="T19" id="Seg_848" n="e" s="T18">pimnieʔi </ts>
               <ts e="T20" id="Seg_850" n="e" s="T19">dĭgəʔ. </ts>
               <ts e="T21" id="Seg_852" n="e" s="T20">Dĭgəttə </ts>
               <ts e="T22" id="Seg_854" n="e" s="T21">šobi </ts>
               <ts e="T23" id="Seg_856" n="e" s="T22">onʼiʔ </ts>
               <ts e="T24" id="Seg_858" n="e" s="T23">soldat. </ts>
               <ts e="T25" id="Seg_860" n="e" s="T24">"Măn </ts>
               <ts e="T26" id="Seg_862" n="e" s="T25">dĭm </ts>
               <ts e="T27" id="Seg_864" n="e" s="T26">tüšüʔlim". </ts>
               <ts e="T28" id="Seg_866" n="e" s="T27">Nüdʼin </ts>
               <ts e="T29" id="Seg_868" n="e" s="T28">körerbi </ts>
               <ts e="T30" id="Seg_870" n="e" s="T29">inezeŋdə. </ts>
               <ts e="T31" id="Seg_872" n="e" s="T30">Šobi. </ts>
               <ts e="T32" id="Seg_874" n="e" s="T31">Dĭ </ts>
               <ts e="T33" id="Seg_876" n="e" s="T32">dĭm </ts>
               <ts e="T34" id="Seg_878" n="e" s="T33">iluʔpi, </ts>
               <ts e="T35" id="Seg_880" n="e" s="T34">(ine=) </ts>
               <ts e="T36" id="Seg_882" n="e" s="T35">ineʔinə </ts>
               <ts e="T37" id="Seg_884" n="e" s="T36">embi. </ts>
               <ts e="T38" id="Seg_886" n="e" s="T37">Deʔpi </ts>
               <ts e="T39" id="Seg_888" n="e" s="T38">sapožniktə. </ts>
               <ts e="T40" id="Seg_890" n="e" s="T39">Sapožnikən </ts>
               <ts e="T41" id="Seg_892" n="e" s="T40">nebə </ts>
               <ts e="T42" id="Seg_894" n="e" s="T41">deʔpi, </ts>
               <ts e="T43" id="Seg_896" n="e" s="T42">döbər </ts>
               <ts e="T44" id="Seg_898" n="e" s="T43">gijen </ts>
               <ts e="T45" id="Seg_900" n="e" s="T44">dĭ </ts>
               <ts e="T46" id="Seg_902" n="e" s="T45">barɨnʼa </ts>
               <ts e="T47" id="Seg_904" n="e" s="T46">amnobi. </ts>
               <ts e="T48" id="Seg_906" n="e" s="T47">Dĭgəttə </ts>
               <ts e="T49" id="Seg_908" n="e" s="T48">dĭ </ts>
               <ts e="T50" id="Seg_910" n="e" s="T49">ertən </ts>
               <ts e="T51" id="Seg_912" n="e" s="T50">(uʔ- </ts>
               <ts e="T52" id="Seg_914" n="e" s="T51">uʔ-) </ts>
               <ts e="T53" id="Seg_916" n="e" s="T52">uʔluʔpi. </ts>
               <ts e="T54" id="Seg_918" n="e" s="T53">"Gijen </ts>
               <ts e="T55" id="Seg_920" n="e" s="T54">măn </ts>
               <ts e="T56" id="Seg_922" n="e" s="T55">kunollaʔbəm?" </ts>
               <ts e="T57" id="Seg_924" n="e" s="T56">(Dĭ=) </ts>
               <ts e="T58" id="Seg_926" n="e" s="T57">Dĭʔnə </ts>
               <ts e="T59" id="Seg_928" n="e" s="T58">bü </ts>
               <ts e="T60" id="Seg_930" n="e" s="T59">deʔpiʔi. </ts>
               <ts e="T61" id="Seg_932" n="e" s="T60">Deʔpiʔi, </ts>
               <ts e="T62" id="Seg_934" n="e" s="T61">kadəldə </ts>
               <ts e="T63" id="Seg_936" n="e" s="T62">kĭškəsʼtə, </ts>
               <ts e="T64" id="Seg_938" n="e" s="T63">(rušnʼikziʔ) </ts>
               <ts e="T65" id="Seg_940" n="e" s="T64">dĭ </ts>
               <ts e="T66" id="Seg_942" n="e" s="T65">kĭškəluʔpi. </ts>
               <ts e="T67" id="Seg_944" n="e" s="T66">Amzittə </ts>
               <ts e="T68" id="Seg_946" n="e" s="T67">mĭbiʔi. </ts>
               <ts e="T69" id="Seg_948" n="e" s="T68">Kujdərgan </ts>
               <ts e="T70" id="Seg_950" n="e" s="T69">nuldəbiʔi, </ts>
               <ts e="T71" id="Seg_952" n="e" s="T70">segi </ts>
               <ts e="T72" id="Seg_954" n="e" s="T71">bü. </ts>
               <ts e="T73" id="Seg_956" n="e" s="T72">Dĭ </ts>
               <ts e="T74" id="Seg_958" n="e" s="T73">amorlaʔbə. </ts>
               <ts e="T75" id="Seg_960" n="e" s="T74">Šobi </ts>
               <ts e="T76" id="Seg_962" n="e" s="T75">tarəsta </ts>
               <ts e="T77" id="Seg_964" n="e" s="T76">dĭʔnə. </ts>
               <ts e="T78" id="Seg_966" n="e" s="T77">"Ĭmbi </ts>
               <ts e="T79" id="Seg_968" n="e" s="T78">azittə?" </ts>
               <ts e="T80" id="Seg_970" n="e" s="T79">(Ambi-) </ts>
               <ts e="T81" id="Seg_972" n="e" s="T80">Ĭmbi </ts>
               <ts e="T82" id="Seg_974" n="e" s="T81">taldʼen </ts>
               <ts e="T83" id="Seg_976" n="e" s="T82">abial, </ts>
               <ts e="T84" id="Seg_978" n="e" s="T83">teinen </ts>
               <ts e="T85" id="Seg_980" n="e" s="T84">(i) </ts>
               <ts e="T86" id="Seg_982" n="e" s="T85">i </ts>
               <ts e="T87" id="Seg_984" n="e" s="T86">dĭm </ts>
               <ts e="T88" id="Seg_986" n="e" s="T87">(alaʔ-) </ts>
               <ts e="T89" id="Seg_988" n="e" s="T88">aʔ. </ts>
               <ts e="T90" id="Seg_990" n="e" s="T89">Dĭ </ts>
               <ts e="T91" id="Seg_992" n="e" s="T90">kambi, </ts>
               <ts e="T92" id="Seg_994" n="e" s="T91">măndliaʔi. </ts>
               <ts e="T93" id="Seg_996" n="e" s="T92">"Jakšə </ts>
               <ts e="T94" id="Seg_998" n="e" s="T93">molaːmbi </ts>
               <ts e="T95" id="Seg_1000" n="e" s="T94">barɨnʼa </ts>
               <ts e="T96" id="Seg_1002" n="e" s="T95">miʔ. </ts>
               <ts e="T97" id="Seg_1004" n="e" s="T96">Ej </ts>
               <ts e="T98" id="Seg_1006" n="e" s="T97">kudonzlia. </ts>
               <ts e="T99" id="Seg_1008" n="e" s="T98">Dĭgəttə </ts>
               <ts e="T100" id="Seg_1010" n="e" s="T99">ej </ts>
               <ts e="T101" id="Seg_1012" n="e" s="T100">münöria </ts>
               <ts e="T102" id="Seg_1014" n="e" s="T101">šindimdə". </ts>
               <ts e="T103" id="Seg_1016" n="e" s="T102">Dĭgəttə </ts>
               <ts e="T104" id="Seg_1018" n="e" s="T103">nünəbi </ts>
               <ts e="T105" id="Seg_1020" n="e" s="T104">dĭ </ts>
               <ts e="T106" id="Seg_1022" n="e" s="T105">(sə-) </ts>
               <ts e="T107" id="Seg_1024" n="e" s="T106">soldat. </ts>
               <ts e="T108" id="Seg_1026" n="e" s="T107">Šide </ts>
               <ts e="T109" id="Seg_1028" n="e" s="T108">mesʼats </ts>
               <ts e="T110" id="Seg_1030" n="e" s="T109">kambi. </ts>
               <ts e="T111" id="Seg_1032" n="e" s="T110">A </ts>
               <ts e="T112" id="Seg_1034" n="e" s="T111">dĭ </ts>
               <ts e="T113" id="Seg_1036" n="e" s="T112">kuznʼetsgən </ts>
               <ts e="T114" id="Seg_1038" n="e" s="T113">barɨnʼa </ts>
               <ts e="T115" id="Seg_1040" n="e" s="T114">amnobi. </ts>
               <ts e="T116" id="Seg_1042" n="e" s="T115">"Ĭmbi </ts>
               <ts e="T117" id="Seg_1044" n="e" s="T116">ej </ts>
               <ts e="T118" id="Seg_1046" n="e" s="T117">uʔblial? </ts>
               <ts e="T119" id="Seg_1048" n="e" s="T118">Ĭmbi </ts>
               <ts e="T120" id="Seg_1050" n="e" s="T119">pʼeš </ts>
               <ts e="T121" id="Seg_1052" n="e" s="T120">ej </ts>
               <ts e="T122" id="Seg_1054" n="e" s="T121">(nendlie)? </ts>
               <ts e="T123" id="Seg_1056" n="e" s="T122">Büjleʔ </ts>
               <ts e="T124" id="Seg_1058" n="e" s="T123">kanaʔ!" </ts>
               <ts e="T125" id="Seg_1060" n="e" s="T124">Dĭ </ts>
               <ts e="T126" id="Seg_1062" n="e" s="T125">măndə: </ts>
               <ts e="T127" id="Seg_1064" n="e" s="T126">"Deʔ </ts>
               <ts e="T128" id="Seg_1066" n="e" s="T127">măna </ts>
               <ts e="T129" id="Seg_1068" n="e" s="T128">băzejdəsʼtə!" </ts>
               <ts e="T130" id="Seg_1070" n="e" s="T129">Dĭ </ts>
               <ts e="T131" id="Seg_1072" n="e" s="T130">ibi </ts>
               <ts e="T132" id="Seg_1074" n="e" s="T131">remendə, </ts>
               <ts e="T133" id="Seg_1076" n="e" s="T132">münörbi, </ts>
               <ts e="T134" id="Seg_1078" n="e" s="T133">münörbi. </ts>
               <ts e="T135" id="Seg_1080" n="e" s="T134">Dĭ </ts>
               <ts e="T136" id="Seg_1082" n="e" s="T135">kabarbi </ts>
               <ts e="T137" id="Seg_1084" n="e" s="T136">aspaʔ, </ts>
               <ts e="T138" id="Seg_1086" n="e" s="T137">kambi </ts>
               <ts e="T139" id="Seg_1088" n="e" s="T138">büjleʔ. </ts>
               <ts e="T140" id="Seg_1090" n="e" s="T139">Ĭmbidə </ts>
               <ts e="T141" id="Seg_1092" n="e" s="T140">ej </ts>
               <ts e="T142" id="Seg_1094" n="e" s="T141">molia </ts>
               <ts e="T143" id="Seg_1096" n="e" s="T142">(oj-) </ts>
               <ts e="T144" id="Seg_1098" n="e" s="T143">azittə. </ts>
               <ts e="T145" id="Seg_1100" n="e" s="T144">Amnobi, </ts>
               <ts e="T146" id="Seg_1102" n="e" s="T145">amnobi. </ts>
               <ts e="T147" id="Seg_1104" n="e" s="T146">Dĭgəttə </ts>
               <ts e="T148" id="Seg_1106" n="e" s="T147">bazoʔ </ts>
               <ts e="T149" id="Seg_1108" n="e" s="T148">kunolluʔpiʔi. </ts>
               <ts e="T150" id="Seg_1110" n="e" s="T149">Dĭ </ts>
               <ts e="T151" id="Seg_1112" n="e" s="T150">soldat </ts>
               <ts e="T152" id="Seg_1114" n="e" s="T151">šobi. </ts>
               <ts e="T153" id="Seg_1116" n="e" s="T152">Dĭ </ts>
               <ts e="T154" id="Seg_1118" n="e" s="T153">barɨnʼam </ts>
               <ts e="T155" id="Seg_1120" n="e" s="T154">deʔpi </ts>
               <ts e="T156" id="Seg_1122" n="e" s="T155">maːndə. </ts>
               <ts e="T157" id="Seg_1124" n="e" s="T156">A </ts>
               <ts e="T158" id="Seg_1126" n="e" s="T157">kuznʼetsən </ts>
               <ts e="T159" id="Seg_1128" n="e" s="T158">nedə </ts>
               <ts e="T160" id="Seg_1130" n="e" s="T159">tože </ts>
               <ts e="T161" id="Seg_1132" n="e" s="T160">maːndə </ts>
               <ts e="T162" id="Seg_1134" n="e" s="T161">deʔpi. </ts>
               <ts e="T163" id="Seg_1136" n="e" s="T162">Dĭgəttə </ts>
               <ts e="T164" id="Seg_1138" n="e" s="T163">ertən </ts>
               <ts e="T165" id="Seg_1140" n="e" s="T164">uʔbdəbi. </ts>
               <ts e="T166" id="Seg_1142" n="e" s="T165">Kăda </ts>
               <ts e="T167" id="Seg_1144" n="e" s="T166">măn </ts>
               <ts e="T168" id="Seg_1146" n="e" s="T167">döbər </ts>
               <ts e="T169" id="Seg_1148" n="e" s="T168">šobiam </ts>
               <ts e="T170" id="Seg_1150" n="e" s="T169">bostə </ts>
               <ts e="T171" id="Seg_1152" n="e" s="T170">(maː-) </ts>
               <ts e="T172" id="Seg_1154" n="e" s="T171">maːndə? </ts>
               <ts e="T173" id="Seg_1156" n="e" s="T172">Dĭgəttə </ts>
               <ts e="T174" id="Seg_1158" n="e" s="T173">surarlaʔbə </ts>
               <ts e="T175" id="Seg_1160" n="e" s="T174">bostə. </ts>
               <ts e="T176" id="Seg_1162" n="e" s="T175">"Da </ts>
               <ts e="T177" id="Seg_1164" n="e" s="T176">tăn </ts>
               <ts e="T178" id="Seg_1166" n="e" s="T177">gijendə </ts>
               <ts e="T179" id="Seg_1168" n="e" s="T178">i </ts>
               <ts e="T180" id="Seg_1170" n="e" s="T179">nagobial, </ts>
               <ts e="T181" id="Seg_1172" n="e" s="T180">dön </ts>
               <ts e="T182" id="Seg_1174" n="e" s="T181">amnobial". </ts>
               <ts e="T183" id="Seg_1176" n="e" s="T182">Dĭgəttə </ts>
               <ts e="T184" id="Seg_1178" n="e" s="T183">kuznʼetsən </ts>
               <ts e="T185" id="Seg_1180" n="e" s="T184">(ne=) </ts>
               <ts e="T186" id="Seg_1182" n="e" s="T185">ne </ts>
               <ts e="T187" id="Seg_1184" n="e" s="T186">(ha-) </ts>
               <ts e="T188" id="Seg_1186" n="e" s="T187">jakšə </ts>
               <ts e="T189" id="Seg_1188" n="e" s="T188">amnolaʔbə. </ts>
               <ts e="T190" id="Seg_1190" n="e" s="T189">A </ts>
               <ts e="T191" id="Seg_1192" n="e" s="T190">dĭ </ts>
               <ts e="T192" id="Seg_1194" n="e" s="T191">ej </ts>
               <ts e="T193" id="Seg_1196" n="e" s="T192">(kuroluʔ-) </ts>
               <ts e="T194" id="Seg_1198" n="e" s="T193">kurojok </ts>
               <ts e="T195" id="Seg_1200" n="e" s="T194">molaːmbi. </ts>
               <ts e="T196" id="Seg_1202" n="e" s="T195">Jakšə </ts>
               <ts e="T197" id="Seg_1204" n="e" s="T196">molaːmbi, </ts>
               <ts e="T198" id="Seg_1206" n="e" s="T197">dĭ </ts>
               <ts e="T199" id="Seg_1208" n="e" s="T198">barɨnʼa. </ts>
               <ts e="T201" id="Seg_1210" n="e" s="T199">Dĭgəttə… </ts>
               <ts e="T202" id="Seg_1212" n="e" s="T201">Kabarləj! </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_1213" s="T0">PKZ_196X_AngryLady_flk.001 (001)</ta>
            <ta e="T8" id="Seg_1214" s="T3">PKZ_196X_AngryLady_flk.002 (002)</ta>
            <ta e="T15" id="Seg_1215" s="T8">PKZ_196X_AngryLady_flk.003 (003)</ta>
            <ta e="T20" id="Seg_1216" s="T15">PKZ_196X_AngryLady_flk.004 (004)</ta>
            <ta e="T24" id="Seg_1217" s="T20">PKZ_196X_AngryLady_flk.005 (005)</ta>
            <ta e="T27" id="Seg_1218" s="T24">PKZ_196X_AngryLady_flk.006 (006)</ta>
            <ta e="T30" id="Seg_1219" s="T27">PKZ_196X_AngryLady_flk.007 (007)</ta>
            <ta e="T31" id="Seg_1220" s="T30">PKZ_196X_AngryLady_flk.008 (008)</ta>
            <ta e="T37" id="Seg_1221" s="T31">PKZ_196X_AngryLady_flk.009 (009)</ta>
            <ta e="T39" id="Seg_1222" s="T37">PKZ_196X_AngryLady_flk.010 (010)</ta>
            <ta e="T47" id="Seg_1223" s="T39">PKZ_196X_AngryLady_flk.011 (011)</ta>
            <ta e="T53" id="Seg_1224" s="T47">PKZ_196X_AngryLady_flk.012 (012)</ta>
            <ta e="T56" id="Seg_1225" s="T53">PKZ_196X_AngryLady_flk.013 (013)</ta>
            <ta e="T60" id="Seg_1226" s="T56">PKZ_196X_AngryLady_flk.014 (014)</ta>
            <ta e="T66" id="Seg_1227" s="T60">PKZ_196X_AngryLady_flk.015 (015)</ta>
            <ta e="T68" id="Seg_1228" s="T66">PKZ_196X_AngryLady_flk.016 (016)</ta>
            <ta e="T72" id="Seg_1229" s="T68">PKZ_196X_AngryLady_flk.017 (017)</ta>
            <ta e="T74" id="Seg_1230" s="T72">PKZ_196X_AngryLady_flk.018 (018)</ta>
            <ta e="T77" id="Seg_1231" s="T74">PKZ_196X_AngryLady_flk.019 (019)</ta>
            <ta e="T79" id="Seg_1232" s="T77">PKZ_196X_AngryLady_flk.020 (020)</ta>
            <ta e="T89" id="Seg_1233" s="T79">PKZ_196X_AngryLady_flk.021 (021)</ta>
            <ta e="T92" id="Seg_1234" s="T89">PKZ_196X_AngryLady_flk.022 (022)</ta>
            <ta e="T96" id="Seg_1235" s="T92">PKZ_196X_AngryLady_flk.023 (023)</ta>
            <ta e="T98" id="Seg_1236" s="T96">PKZ_196X_AngryLady_flk.024 (024)</ta>
            <ta e="T102" id="Seg_1237" s="T98">PKZ_196X_AngryLady_flk.025 (025)</ta>
            <ta e="T107" id="Seg_1238" s="T102">PKZ_196X_AngryLady_flk.026 (026)</ta>
            <ta e="T110" id="Seg_1239" s="T107">PKZ_196X_AngryLady_flk.027 (027)</ta>
            <ta e="T115" id="Seg_1240" s="T110">PKZ_196X_AngryLady_flk.028 (028)</ta>
            <ta e="T118" id="Seg_1241" s="T115">PKZ_196X_AngryLady_flk.029 (029)</ta>
            <ta e="T122" id="Seg_1242" s="T118">PKZ_196X_AngryLady_flk.030 (030)</ta>
            <ta e="T124" id="Seg_1243" s="T122">PKZ_196X_AngryLady_flk.031 (031)</ta>
            <ta e="T129" id="Seg_1244" s="T124">PKZ_196X_AngryLady_flk.032 (032) </ta>
            <ta e="T134" id="Seg_1245" s="T129">PKZ_196X_AngryLady_flk.033 (034)</ta>
            <ta e="T139" id="Seg_1246" s="T134">PKZ_196X_AngryLady_flk.034 (035)</ta>
            <ta e="T144" id="Seg_1247" s="T139">PKZ_196X_AngryLady_flk.035 (036)</ta>
            <ta e="T146" id="Seg_1248" s="T144">PKZ_196X_AngryLady_flk.036 (037)</ta>
            <ta e="T149" id="Seg_1249" s="T146">PKZ_196X_AngryLady_flk.037 (038)</ta>
            <ta e="T152" id="Seg_1250" s="T149">PKZ_196X_AngryLady_flk.038 (039)</ta>
            <ta e="T156" id="Seg_1251" s="T152">PKZ_196X_AngryLady_flk.039 (040)</ta>
            <ta e="T162" id="Seg_1252" s="T156">PKZ_196X_AngryLady_flk.040 (041)</ta>
            <ta e="T165" id="Seg_1253" s="T162">PKZ_196X_AngryLady_flk.041 (042)</ta>
            <ta e="T172" id="Seg_1254" s="T165">PKZ_196X_AngryLady_flk.042 (043)</ta>
            <ta e="T175" id="Seg_1255" s="T172">PKZ_196X_AngryLady_flk.043 (044)</ta>
            <ta e="T182" id="Seg_1256" s="T175">PKZ_196X_AngryLady_flk.044 (045)</ta>
            <ta e="T189" id="Seg_1257" s="T182">PKZ_196X_AngryLady_flk.045 (046)</ta>
            <ta e="T195" id="Seg_1258" s="T189">PKZ_196X_AngryLady_flk.046 (047)</ta>
            <ta e="T199" id="Seg_1259" s="T195">PKZ_196X_AngryLady_flk.047 (048)</ta>
            <ta e="T201" id="Seg_1260" s="T199">PKZ_196X_AngryLady_flk.048 (049)</ta>
            <ta e="T202" id="Seg_1261" s="T201">PKZ_196X_AngryLady_flk.049 (050)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_1262" s="T0">Ibi kurojok ne. </ta>
            <ta e="T8" id="Seg_1263" s="T3">Di üge kurolaʔpi, tibizeŋ münörlaʔpi. </ta>
            <ta e="T15" id="Seg_1264" s="T8">Tarəsta šoləj, dĭ bar kirgarlaʔbə, münörləj dĭm. </ta>
            <ta e="T20" id="Seg_1265" s="T15">Ugaːndə kurojok, il pimnieʔi dĭgəʔ. </ta>
            <ta e="T24" id="Seg_1266" s="T20">Dĭgəttə šobi onʼiʔ soldat. </ta>
            <ta e="T27" id="Seg_1267" s="T24">"Măn dĭm tüšüʔlim". </ta>
            <ta e="T30" id="Seg_1268" s="T27">Nüdʼin körerbi inezeŋdə. </ta>
            <ta e="T31" id="Seg_1269" s="T30">Šobi. </ta>
            <ta e="T37" id="Seg_1270" s="T31">Dĭ dĭm iluʔpi, (ine=) ineʔinə embi. </ta>
            <ta e="T39" id="Seg_1271" s="T37">Deʔpi sapožniktə. </ta>
            <ta e="T47" id="Seg_1272" s="T39">Sapožnikən nebə deʔpi, döbər gijen dĭ barɨnʼa amnobi. </ta>
            <ta e="T53" id="Seg_1273" s="T47">Dĭgəttə dĭ ertən (uʔ- uʔ-) uʔluʔpi. </ta>
            <ta e="T56" id="Seg_1274" s="T53">"Gijen măn kunollaʔbəm?" </ta>
            <ta e="T60" id="Seg_1275" s="T56">(Dĭ=) Dĭʔnə bü deʔpiʔi. </ta>
            <ta e="T66" id="Seg_1276" s="T60">Deʔpiʔi, kadəldə kĭškəsʼtə, (rušnʼikziʔ) dĭ kĭškəluʔpi. </ta>
            <ta e="T68" id="Seg_1277" s="T66">Amzittə mĭbiʔi. </ta>
            <ta e="T72" id="Seg_1278" s="T68">Kujdərgan nuldəbiʔi, segi bü. </ta>
            <ta e="T74" id="Seg_1279" s="T72">Dĭ amorlaʔbə. </ta>
            <ta e="T77" id="Seg_1280" s="T74">Šobi tarəsta dĭʔnə. </ta>
            <ta e="T79" id="Seg_1281" s="T77">"Ĭmbi azittə?" </ta>
            <ta e="T89" id="Seg_1282" s="T79">"(Ambi-) Ĭmbi taldʼen abial, teinen (i) i dĭm (alaʔ-) aʔ". </ta>
            <ta e="T92" id="Seg_1283" s="T89">Dĭ kambi, măndliaʔi. </ta>
            <ta e="T96" id="Seg_1284" s="T92">"Jakšə molaːmbi barɨnʼa miʔ. </ta>
            <ta e="T98" id="Seg_1285" s="T96">Ej kudonzlia. </ta>
            <ta e="T102" id="Seg_1286" s="T98">Dĭgəttə ej münöria šindimdə". </ta>
            <ta e="T107" id="Seg_1287" s="T102">Dĭgəttə nünəbi dĭ (sə-) soldat. </ta>
            <ta e="T110" id="Seg_1288" s="T107">Šide mesʼats kambi. </ta>
            <ta e="T115" id="Seg_1289" s="T110">A dĭ kuznʼetsgən barɨnʼa amnobi. </ta>
            <ta e="T118" id="Seg_1290" s="T115">"Ĭmbi ej uʔblial? </ta>
            <ta e="T122" id="Seg_1291" s="T118">Ĭmbi pʼeš ej (nendlie)? </ta>
            <ta e="T124" id="Seg_1292" s="T122">Büjleʔ kanaʔ!" </ta>
            <ta e="T129" id="Seg_1293" s="T124">Dĭ măndə: "Deʔ măna băzejdəsʼtə!" </ta>
            <ta e="T134" id="Seg_1294" s="T129">Dĭ ibi remendə, münörbi, münörbi. </ta>
            <ta e="T139" id="Seg_1295" s="T134">Dĭ kabarbi aspaʔ, kambi büjleʔ. </ta>
            <ta e="T144" id="Seg_1296" s="T139">Ĭmbidə ej molia (oj-) azittə. </ta>
            <ta e="T146" id="Seg_1297" s="T144">Amnobi, amnobi. </ta>
            <ta e="T149" id="Seg_1298" s="T146">Dĭgəttə bazoʔ kunolluʔpiʔi. </ta>
            <ta e="T152" id="Seg_1299" s="T149">Dĭ soldat šobi. </ta>
            <ta e="T156" id="Seg_1300" s="T152">Dĭ barɨnʼam deʔpi maːndə. </ta>
            <ta e="T162" id="Seg_1301" s="T156">A kuznʼetsən nedə tože maːndə deʔpi. </ta>
            <ta e="T165" id="Seg_1302" s="T162">Dĭgəttə ertən uʔbdəbi. </ta>
            <ta e="T172" id="Seg_1303" s="T165">"Kăda măn döbər šobiam bostə (maː-) maːndə?" </ta>
            <ta e="T175" id="Seg_1304" s="T172">Dĭgəttə surarlaʔbə bostə. </ta>
            <ta e="T182" id="Seg_1305" s="T175">"Da tăn gijendə i nagobial, dön amnobial". </ta>
            <ta e="T189" id="Seg_1306" s="T182">Dĭgəttə kuznʼetsən (ne=) ne (ha-) jakšə amnolaʔbə". </ta>
            <ta e="T195" id="Seg_1307" s="T189">A dĭ ej (kuroluʔ-) kurojok molaːmbi. </ta>
            <ta e="T199" id="Seg_1308" s="T195">Jakšə molaːmbi, dĭ barɨnʼa. </ta>
            <ta e="T201" id="Seg_1309" s="T199">Dĭgəttə… </ta>
            <ta e="T202" id="Seg_1310" s="T201">Kabarləj! </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1311" s="T0">i-bi</ta>
            <ta e="T2" id="Seg_1312" s="T1">kurojok</ta>
            <ta e="T3" id="Seg_1313" s="T2">ne</ta>
            <ta e="T4" id="Seg_1314" s="T3">di</ta>
            <ta e="T5" id="Seg_1315" s="T4">üge</ta>
            <ta e="T6" id="Seg_1316" s="T5">kuro-laʔpi</ta>
            <ta e="T7" id="Seg_1317" s="T6">tibi-zeŋ</ta>
            <ta e="T8" id="Seg_1318" s="T7">münör-laʔpi</ta>
            <ta e="T9" id="Seg_1319" s="T8">tarəsta</ta>
            <ta e="T10" id="Seg_1320" s="T9">šo-lə-j</ta>
            <ta e="T11" id="Seg_1321" s="T10">dĭ</ta>
            <ta e="T12" id="Seg_1322" s="T11">bar</ta>
            <ta e="T13" id="Seg_1323" s="T12">kirgar-laʔbə</ta>
            <ta e="T14" id="Seg_1324" s="T13">münör-lə-j</ta>
            <ta e="T15" id="Seg_1325" s="T14">dĭ-m</ta>
            <ta e="T16" id="Seg_1326" s="T15">ugaːndə</ta>
            <ta e="T17" id="Seg_1327" s="T16">kurojok</ta>
            <ta e="T18" id="Seg_1328" s="T17">il</ta>
            <ta e="T19" id="Seg_1329" s="T18">pim-nie-ʔi</ta>
            <ta e="T20" id="Seg_1330" s="T19">dĭ-gəʔ</ta>
            <ta e="T21" id="Seg_1331" s="T20">dĭgəttə</ta>
            <ta e="T22" id="Seg_1332" s="T21">šo-bi</ta>
            <ta e="T23" id="Seg_1333" s="T22">onʼiʔ</ta>
            <ta e="T24" id="Seg_1334" s="T23">soldat</ta>
            <ta e="T25" id="Seg_1335" s="T24">măn</ta>
            <ta e="T26" id="Seg_1336" s="T25">dĭ-m</ta>
            <ta e="T27" id="Seg_1337" s="T26">tüšüʔ-li-m</ta>
            <ta e="T28" id="Seg_1338" s="T27">nüdʼi-n</ta>
            <ta e="T29" id="Seg_1339" s="T28">körer-bi</ta>
            <ta e="T30" id="Seg_1340" s="T29">ine-zeŋ-də</ta>
            <ta e="T31" id="Seg_1341" s="T30">šo-bi</ta>
            <ta e="T32" id="Seg_1342" s="T31">dĭ</ta>
            <ta e="T33" id="Seg_1343" s="T32">dĭ-m</ta>
            <ta e="T34" id="Seg_1344" s="T33">i-luʔ-pi</ta>
            <ta e="T35" id="Seg_1345" s="T34">ine</ta>
            <ta e="T36" id="Seg_1346" s="T35">ine-ʔi-nə</ta>
            <ta e="T37" id="Seg_1347" s="T36">em-bi</ta>
            <ta e="T38" id="Seg_1348" s="T37">deʔ-pi</ta>
            <ta e="T39" id="Seg_1349" s="T38">sapožnik-tə</ta>
            <ta e="T40" id="Seg_1350" s="T39">sapožnik-ə-n</ta>
            <ta e="T41" id="Seg_1351" s="T40">ne-bə</ta>
            <ta e="T42" id="Seg_1352" s="T41">deʔ-pi</ta>
            <ta e="T43" id="Seg_1353" s="T42">döbər</ta>
            <ta e="T44" id="Seg_1354" s="T43">gijen</ta>
            <ta e="T45" id="Seg_1355" s="T44">dĭ</ta>
            <ta e="T46" id="Seg_1356" s="T45">barɨnʼa</ta>
            <ta e="T47" id="Seg_1357" s="T46">amno-bi</ta>
            <ta e="T48" id="Seg_1358" s="T47">dĭgəttə</ta>
            <ta e="T49" id="Seg_1359" s="T48">dĭ</ta>
            <ta e="T50" id="Seg_1360" s="T49">ertə-n</ta>
            <ta e="T53" id="Seg_1361" s="T52">uʔ-luʔ-pi</ta>
            <ta e="T54" id="Seg_1362" s="T53">gijen</ta>
            <ta e="T55" id="Seg_1363" s="T54">măn</ta>
            <ta e="T56" id="Seg_1364" s="T55">kunol-laʔbə-m</ta>
            <ta e="T57" id="Seg_1365" s="T56">dĭ</ta>
            <ta e="T58" id="Seg_1366" s="T57">dĭʔ-nə</ta>
            <ta e="T59" id="Seg_1367" s="T58">bü</ta>
            <ta e="T60" id="Seg_1368" s="T59">deʔ-pi-ʔi</ta>
            <ta e="T61" id="Seg_1369" s="T60">deʔ-pi-ʔi</ta>
            <ta e="T62" id="Seg_1370" s="T61">kadəl-də</ta>
            <ta e="T63" id="Seg_1371" s="T62">kĭškə-sʼtə</ta>
            <ta e="T64" id="Seg_1372" s="T63">rušnʼik-ziʔ</ta>
            <ta e="T65" id="Seg_1373" s="T64">dĭ</ta>
            <ta e="T66" id="Seg_1374" s="T65">kĭškə-luʔ-pi</ta>
            <ta e="T67" id="Seg_1375" s="T66">am-zittə</ta>
            <ta e="T68" id="Seg_1376" s="T67">mĭ-bi-ʔi</ta>
            <ta e="T69" id="Seg_1377" s="T68">kujdərgan</ta>
            <ta e="T70" id="Seg_1378" s="T69">nuldə-bi-ʔi</ta>
            <ta e="T71" id="Seg_1379" s="T70">segi</ta>
            <ta e="T72" id="Seg_1380" s="T71">bü</ta>
            <ta e="T73" id="Seg_1381" s="T72">dĭ</ta>
            <ta e="T74" id="Seg_1382" s="T73">amor-laʔbə</ta>
            <ta e="T75" id="Seg_1383" s="T74">šo-bi</ta>
            <ta e="T76" id="Seg_1384" s="T75">tarəsta</ta>
            <ta e="T77" id="Seg_1385" s="T76">dĭʔ-nə</ta>
            <ta e="T78" id="Seg_1386" s="T77">ĭmbi</ta>
            <ta e="T79" id="Seg_1387" s="T78">a-zittə</ta>
            <ta e="T81" id="Seg_1388" s="T80">ĭmbi</ta>
            <ta e="T82" id="Seg_1389" s="T81">taldʼen</ta>
            <ta e="T83" id="Seg_1390" s="T82">a-bia-l</ta>
            <ta e="T84" id="Seg_1391" s="T83">teinen</ta>
            <ta e="T85" id="Seg_1392" s="T84">i</ta>
            <ta e="T86" id="Seg_1393" s="T85">i</ta>
            <ta e="T87" id="Seg_1394" s="T86">dĭ-m</ta>
            <ta e="T89" id="Seg_1395" s="T88">a-ʔ</ta>
            <ta e="T90" id="Seg_1396" s="T89">dĭ</ta>
            <ta e="T91" id="Seg_1397" s="T90">kam-bi</ta>
            <ta e="T92" id="Seg_1398" s="T91">mănd-lia-ʔi</ta>
            <ta e="T93" id="Seg_1399" s="T92">jakšə</ta>
            <ta e="T94" id="Seg_1400" s="T93">mo-laːm-bi</ta>
            <ta e="T95" id="Seg_1401" s="T94">barɨnʼa</ta>
            <ta e="T96" id="Seg_1402" s="T95">miʔ</ta>
            <ta e="T97" id="Seg_1403" s="T96">ej</ta>
            <ta e="T98" id="Seg_1404" s="T97">kudo-nz-lia</ta>
            <ta e="T99" id="Seg_1405" s="T98">dĭgəttə</ta>
            <ta e="T100" id="Seg_1406" s="T99">ej</ta>
            <ta e="T101" id="Seg_1407" s="T100">münör-ia</ta>
            <ta e="T102" id="Seg_1408" s="T101">šindi-m=də</ta>
            <ta e="T103" id="Seg_1409" s="T102">dĭgəttə</ta>
            <ta e="T104" id="Seg_1410" s="T103">nünə-bi</ta>
            <ta e="T105" id="Seg_1411" s="T104">dĭ</ta>
            <ta e="T107" id="Seg_1412" s="T106">soldat</ta>
            <ta e="T108" id="Seg_1413" s="T107">šide</ta>
            <ta e="T109" id="Seg_1414" s="T108">mesʼats</ta>
            <ta e="T110" id="Seg_1415" s="T109">kam-bi</ta>
            <ta e="T111" id="Seg_1416" s="T110">a</ta>
            <ta e="T112" id="Seg_1417" s="T111">dĭ</ta>
            <ta e="T113" id="Seg_1418" s="T112">kuznʼes-gən</ta>
            <ta e="T114" id="Seg_1419" s="T113">barɨnʼa</ta>
            <ta e="T115" id="Seg_1420" s="T114">amno-bi</ta>
            <ta e="T116" id="Seg_1421" s="T115">ĭmbi</ta>
            <ta e="T117" id="Seg_1422" s="T116">ej</ta>
            <ta e="T118" id="Seg_1423" s="T117">uʔb-lia-l</ta>
            <ta e="T119" id="Seg_1424" s="T118">ĭmbi</ta>
            <ta e="T120" id="Seg_1425" s="T119">pʼeš</ta>
            <ta e="T121" id="Seg_1426" s="T120">ej</ta>
            <ta e="T122" id="Seg_1427" s="T121">nend-lie</ta>
            <ta e="T123" id="Seg_1428" s="T122">bü-j-leʔ</ta>
            <ta e="T124" id="Seg_1429" s="T123">kan-a-ʔ</ta>
            <ta e="T125" id="Seg_1430" s="T124">dĭ</ta>
            <ta e="T126" id="Seg_1431" s="T125">măn-də</ta>
            <ta e="T127" id="Seg_1432" s="T126">de-ʔ</ta>
            <ta e="T128" id="Seg_1433" s="T127">măna</ta>
            <ta e="T129" id="Seg_1434" s="T128">băzej-də-sʼtə</ta>
            <ta e="T130" id="Seg_1435" s="T129">dĭ</ta>
            <ta e="T131" id="Seg_1436" s="T130">i-bi</ta>
            <ta e="T132" id="Seg_1437" s="T131">remen-də</ta>
            <ta e="T133" id="Seg_1438" s="T132">münör-bi</ta>
            <ta e="T134" id="Seg_1439" s="T133">münör-bi</ta>
            <ta e="T135" id="Seg_1440" s="T134">dĭ</ta>
            <ta e="T136" id="Seg_1441" s="T135">kabar-bi</ta>
            <ta e="T137" id="Seg_1442" s="T136">aspaʔ</ta>
            <ta e="T138" id="Seg_1443" s="T137">kam-bi</ta>
            <ta e="T139" id="Seg_1444" s="T138">bü-j-leʔ</ta>
            <ta e="T140" id="Seg_1445" s="T139">ĭmbi=də</ta>
            <ta e="T141" id="Seg_1446" s="T140">ej</ta>
            <ta e="T142" id="Seg_1447" s="T141">mo-lia</ta>
            <ta e="T144" id="Seg_1448" s="T143">a-zittə</ta>
            <ta e="T145" id="Seg_1449" s="T144">amno-bi</ta>
            <ta e="T146" id="Seg_1450" s="T145">amno-bi</ta>
            <ta e="T147" id="Seg_1451" s="T146">dĭgəttə</ta>
            <ta e="T148" id="Seg_1452" s="T147">bazoʔ</ta>
            <ta e="T149" id="Seg_1453" s="T148">kunol-luʔ-pi-ʔi</ta>
            <ta e="T150" id="Seg_1454" s="T149">dĭ</ta>
            <ta e="T151" id="Seg_1455" s="T150">soldat</ta>
            <ta e="T152" id="Seg_1456" s="T151">šo-bi</ta>
            <ta e="T153" id="Seg_1457" s="T152">dĭ</ta>
            <ta e="T154" id="Seg_1458" s="T153">barɨnʼa-m</ta>
            <ta e="T155" id="Seg_1459" s="T154">deʔ-pi</ta>
            <ta e="T156" id="Seg_1460" s="T155">ma-ndə</ta>
            <ta e="T157" id="Seg_1461" s="T156">a</ta>
            <ta e="T158" id="Seg_1462" s="T157">kuznʼes-ən</ta>
            <ta e="T159" id="Seg_1463" s="T158">ne-də</ta>
            <ta e="T160" id="Seg_1464" s="T159">tože</ta>
            <ta e="T161" id="Seg_1465" s="T160">ma-ndə</ta>
            <ta e="T162" id="Seg_1466" s="T161">deʔ-pi</ta>
            <ta e="T163" id="Seg_1467" s="T162">dĭgəttə</ta>
            <ta e="T164" id="Seg_1468" s="T163">ertə-n</ta>
            <ta e="T165" id="Seg_1469" s="T164">uʔbdə-bi</ta>
            <ta e="T166" id="Seg_1470" s="T165">kăda</ta>
            <ta e="T167" id="Seg_1471" s="T166">măn</ta>
            <ta e="T168" id="Seg_1472" s="T167">döbər</ta>
            <ta e="T169" id="Seg_1473" s="T168">šo-bia-m</ta>
            <ta e="T170" id="Seg_1474" s="T169">bos-tə</ta>
            <ta e="T172" id="Seg_1475" s="T171">ma-ndə</ta>
            <ta e="T173" id="Seg_1476" s="T172">dĭgəttə</ta>
            <ta e="T174" id="Seg_1477" s="T173">surar-laʔbə</ta>
            <ta e="T175" id="Seg_1478" s="T174">bos-tə</ta>
            <ta e="T176" id="Seg_1479" s="T175">da</ta>
            <ta e="T177" id="Seg_1480" s="T176">tăn</ta>
            <ta e="T178" id="Seg_1481" s="T177">gijen=də</ta>
            <ta e="T179" id="Seg_1482" s="T178">i</ta>
            <ta e="T180" id="Seg_1483" s="T179">nago-bia-l</ta>
            <ta e="T181" id="Seg_1484" s="T180">dön</ta>
            <ta e="T182" id="Seg_1485" s="T181">amno-bia-l</ta>
            <ta e="T183" id="Seg_1486" s="T182">dĭgəttə</ta>
            <ta e="T184" id="Seg_1487" s="T183">kuznʼes-ən</ta>
            <ta e="T185" id="Seg_1488" s="T184">ne</ta>
            <ta e="T186" id="Seg_1489" s="T185">ne</ta>
            <ta e="T188" id="Seg_1490" s="T187">jakšə</ta>
            <ta e="T189" id="Seg_1491" s="T188">amno-laʔbə</ta>
            <ta e="T190" id="Seg_1492" s="T189">a</ta>
            <ta e="T191" id="Seg_1493" s="T190">dĭ</ta>
            <ta e="T192" id="Seg_1494" s="T191">ej</ta>
            <ta e="T194" id="Seg_1495" s="T193">kurojok</ta>
            <ta e="T195" id="Seg_1496" s="T194">mo-laːm-bi</ta>
            <ta e="T196" id="Seg_1497" s="T195">jakšə</ta>
            <ta e="T197" id="Seg_1498" s="T196">mo-laːm-bi</ta>
            <ta e="T198" id="Seg_1499" s="T197">dĭ</ta>
            <ta e="T199" id="Seg_1500" s="T198">barɨnʼa</ta>
            <ta e="T201" id="Seg_1501" s="T199">dĭgəttə</ta>
            <ta e="T202" id="Seg_1502" s="T201">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1503" s="T0">i-bi</ta>
            <ta e="T2" id="Seg_1504" s="T1">kurojok</ta>
            <ta e="T3" id="Seg_1505" s="T2">ne</ta>
            <ta e="T4" id="Seg_1506" s="T3">dĭ</ta>
            <ta e="T5" id="Seg_1507" s="T4">üge</ta>
            <ta e="T6" id="Seg_1508" s="T5">kuroː-laʔpi</ta>
            <ta e="T7" id="Seg_1509" s="T6">tibi-zAŋ</ta>
            <ta e="T8" id="Seg_1510" s="T7">münör-laʔpi</ta>
            <ta e="T9" id="Seg_1511" s="T8">tarəsta</ta>
            <ta e="T10" id="Seg_1512" s="T9">šo-lV-j</ta>
            <ta e="T11" id="Seg_1513" s="T10">dĭ</ta>
            <ta e="T12" id="Seg_1514" s="T11">bar</ta>
            <ta e="T13" id="Seg_1515" s="T12">kirgaːr-laʔbə</ta>
            <ta e="T14" id="Seg_1516" s="T13">münör-lV-j</ta>
            <ta e="T15" id="Seg_1517" s="T14">dĭ-m</ta>
            <ta e="T16" id="Seg_1518" s="T15">ugaːndə</ta>
            <ta e="T17" id="Seg_1519" s="T16">kurojok</ta>
            <ta e="T18" id="Seg_1520" s="T17">il</ta>
            <ta e="T19" id="Seg_1521" s="T18">pim-liA-jəʔ</ta>
            <ta e="T20" id="Seg_1522" s="T19">dĭ-gəʔ</ta>
            <ta e="T21" id="Seg_1523" s="T20">dĭgəttə</ta>
            <ta e="T22" id="Seg_1524" s="T21">šo-bi</ta>
            <ta e="T23" id="Seg_1525" s="T22">onʼiʔ</ta>
            <ta e="T24" id="Seg_1526" s="T23">soldat</ta>
            <ta e="T25" id="Seg_1527" s="T24">măn</ta>
            <ta e="T26" id="Seg_1528" s="T25">dĭ-m</ta>
            <ta e="T27" id="Seg_1529" s="T26">tüšüʔ-lV-m</ta>
            <ta e="T28" id="Seg_1530" s="T27">nüdʼi-n</ta>
            <ta e="T29" id="Seg_1531" s="T28">körer-bi</ta>
            <ta e="T30" id="Seg_1532" s="T29">ine-zAŋ-də</ta>
            <ta e="T31" id="Seg_1533" s="T30">šo-bi</ta>
            <ta e="T32" id="Seg_1534" s="T31">dĭ</ta>
            <ta e="T33" id="Seg_1535" s="T32">dĭ-m</ta>
            <ta e="T34" id="Seg_1536" s="T33">i-luʔbdə-bi</ta>
            <ta e="T35" id="Seg_1537" s="T34">ine</ta>
            <ta e="T36" id="Seg_1538" s="T35">ine-jəʔ-Tə</ta>
            <ta e="T37" id="Seg_1539" s="T36">hen-bi</ta>
            <ta e="T38" id="Seg_1540" s="T37">det-bi</ta>
            <ta e="T39" id="Seg_1541" s="T38">sapožnik-Tə</ta>
            <ta e="T40" id="Seg_1542" s="T39">sapožnik-ə-n</ta>
            <ta e="T41" id="Seg_1543" s="T40">ne-bə</ta>
            <ta e="T42" id="Seg_1544" s="T41">det-bi</ta>
            <ta e="T43" id="Seg_1545" s="T42">döbər</ta>
            <ta e="T44" id="Seg_1546" s="T43">gijen</ta>
            <ta e="T45" id="Seg_1547" s="T44">dĭ</ta>
            <ta e="T46" id="Seg_1548" s="T45">barɨnʼa</ta>
            <ta e="T47" id="Seg_1549" s="T46">amno-bi</ta>
            <ta e="T48" id="Seg_1550" s="T47">dĭgəttə</ta>
            <ta e="T49" id="Seg_1551" s="T48">dĭ</ta>
            <ta e="T50" id="Seg_1552" s="T49">ertə-n</ta>
            <ta e="T53" id="Seg_1553" s="T52">uʔbdə-luʔbdə-bi</ta>
            <ta e="T54" id="Seg_1554" s="T53">gijen</ta>
            <ta e="T55" id="Seg_1555" s="T54">măn</ta>
            <ta e="T56" id="Seg_1556" s="T55">kunol-laʔbə-m</ta>
            <ta e="T57" id="Seg_1557" s="T56">dĭ</ta>
            <ta e="T58" id="Seg_1558" s="T57">dĭ-Tə</ta>
            <ta e="T59" id="Seg_1559" s="T58">bü</ta>
            <ta e="T60" id="Seg_1560" s="T59">det-bi-jəʔ</ta>
            <ta e="T61" id="Seg_1561" s="T60">det-bi-jəʔ</ta>
            <ta e="T62" id="Seg_1562" s="T61">kadul-də</ta>
            <ta e="T63" id="Seg_1563" s="T62">kĭškə-zittə</ta>
            <ta e="T64" id="Seg_1564" s="T63">rušnʼik-ziʔ</ta>
            <ta e="T65" id="Seg_1565" s="T64">dĭ</ta>
            <ta e="T66" id="Seg_1566" s="T65">kĭškə-luʔbdə-bi</ta>
            <ta e="T67" id="Seg_1567" s="T66">am-zittə</ta>
            <ta e="T68" id="Seg_1568" s="T67">mĭ-bi-jəʔ</ta>
            <ta e="T69" id="Seg_1569" s="T68">kujdərgan</ta>
            <ta e="T70" id="Seg_1570" s="T69">nuldə-bi-jəʔ</ta>
            <ta e="T71" id="Seg_1571" s="T70">segi</ta>
            <ta e="T72" id="Seg_1572" s="T71">bü</ta>
            <ta e="T73" id="Seg_1573" s="T72">dĭ</ta>
            <ta e="T74" id="Seg_1574" s="T73">amor-laʔbə</ta>
            <ta e="T75" id="Seg_1575" s="T74">šo-bi</ta>
            <ta e="T76" id="Seg_1576" s="T75">tarəsta</ta>
            <ta e="T77" id="Seg_1577" s="T76">dĭ-Tə</ta>
            <ta e="T78" id="Seg_1578" s="T77">ĭmbi</ta>
            <ta e="T79" id="Seg_1579" s="T78">a-zittə</ta>
            <ta e="T81" id="Seg_1580" s="T80">ĭmbi</ta>
            <ta e="T82" id="Seg_1581" s="T81">taldʼen</ta>
            <ta e="T83" id="Seg_1582" s="T82">a-bi-l</ta>
            <ta e="T84" id="Seg_1583" s="T83">teinen</ta>
            <ta e="T85" id="Seg_1584" s="T84">i</ta>
            <ta e="T86" id="Seg_1585" s="T85">i</ta>
            <ta e="T87" id="Seg_1586" s="T86">dĭ-m</ta>
            <ta e="T89" id="Seg_1587" s="T88">a-ʔ</ta>
            <ta e="T90" id="Seg_1588" s="T89">dĭ</ta>
            <ta e="T91" id="Seg_1589" s="T90">kan-bi</ta>
            <ta e="T92" id="Seg_1590" s="T91">măndo-liA-jəʔ</ta>
            <ta e="T93" id="Seg_1591" s="T92">jakšə</ta>
            <ta e="T94" id="Seg_1592" s="T93">mo-laːm-bi</ta>
            <ta e="T95" id="Seg_1593" s="T94">barɨnʼa</ta>
            <ta e="T96" id="Seg_1594" s="T95">miʔ</ta>
            <ta e="T97" id="Seg_1595" s="T96">ej</ta>
            <ta e="T98" id="Seg_1596" s="T97">kudo-nzə-liA</ta>
            <ta e="T99" id="Seg_1597" s="T98">dĭgəttə</ta>
            <ta e="T100" id="Seg_1598" s="T99">ej</ta>
            <ta e="T101" id="Seg_1599" s="T100">münör-liA</ta>
            <ta e="T102" id="Seg_1600" s="T101">šində-m=də</ta>
            <ta e="T103" id="Seg_1601" s="T102">dĭgəttə</ta>
            <ta e="T104" id="Seg_1602" s="T103">nünə-bi</ta>
            <ta e="T105" id="Seg_1603" s="T104">dĭ</ta>
            <ta e="T107" id="Seg_1604" s="T106">soldat</ta>
            <ta e="T108" id="Seg_1605" s="T107">šide</ta>
            <ta e="T109" id="Seg_1606" s="T108">mesʼats</ta>
            <ta e="T110" id="Seg_1607" s="T109">kan-bi</ta>
            <ta e="T111" id="Seg_1608" s="T110">a</ta>
            <ta e="T112" id="Seg_1609" s="T111">dĭ</ta>
            <ta e="T113" id="Seg_1610" s="T112">kuznʼes-Kən</ta>
            <ta e="T114" id="Seg_1611" s="T113">barɨnʼa</ta>
            <ta e="T115" id="Seg_1612" s="T114">amno-bi</ta>
            <ta e="T116" id="Seg_1613" s="T115">ĭmbi</ta>
            <ta e="T117" id="Seg_1614" s="T116">ej</ta>
            <ta e="T118" id="Seg_1615" s="T117">uʔbdə-liA-l</ta>
            <ta e="T119" id="Seg_1616" s="T118">ĭmbi</ta>
            <ta e="T120" id="Seg_1617" s="T119">pʼeːš</ta>
            <ta e="T121" id="Seg_1618" s="T120">ej</ta>
            <ta e="T122" id="Seg_1619" s="T121">nend-liA</ta>
            <ta e="T123" id="Seg_1620" s="T122">bü-j-lAʔ</ta>
            <ta e="T124" id="Seg_1621" s="T123">kan-ə-ʔ</ta>
            <ta e="T125" id="Seg_1622" s="T124">dĭ</ta>
            <ta e="T126" id="Seg_1623" s="T125">măn-ntə</ta>
            <ta e="T127" id="Seg_1624" s="T126">det-ʔ</ta>
            <ta e="T128" id="Seg_1625" s="T127">măna</ta>
            <ta e="T129" id="Seg_1626" s="T128">băzəj-ntə-zittə</ta>
            <ta e="T130" id="Seg_1627" s="T129">dĭ</ta>
            <ta e="T131" id="Seg_1628" s="T130">i-bi</ta>
            <ta e="T132" id="Seg_1629" s="T131">remen-də</ta>
            <ta e="T133" id="Seg_1630" s="T132">münör-bi</ta>
            <ta e="T134" id="Seg_1631" s="T133">münör-bi</ta>
            <ta e="T135" id="Seg_1632" s="T134">dĭ</ta>
            <ta e="T136" id="Seg_1633" s="T135">kabar-bi</ta>
            <ta e="T137" id="Seg_1634" s="T136">aspaʔ</ta>
            <ta e="T138" id="Seg_1635" s="T137">kan-bi</ta>
            <ta e="T139" id="Seg_1636" s="T138">bü-j-lAʔ</ta>
            <ta e="T140" id="Seg_1637" s="T139">ĭmbi=də</ta>
            <ta e="T141" id="Seg_1638" s="T140">ej</ta>
            <ta e="T142" id="Seg_1639" s="T141">mo-liA</ta>
            <ta e="T144" id="Seg_1640" s="T143">a-zittə</ta>
            <ta e="T145" id="Seg_1641" s="T144">amno-bi</ta>
            <ta e="T146" id="Seg_1642" s="T145">amno-bi</ta>
            <ta e="T147" id="Seg_1643" s="T146">dĭgəttə</ta>
            <ta e="T148" id="Seg_1644" s="T147">bazoʔ</ta>
            <ta e="T149" id="Seg_1645" s="T148">kunol-luʔbdə-bi-jəʔ</ta>
            <ta e="T150" id="Seg_1646" s="T149">dĭ</ta>
            <ta e="T151" id="Seg_1647" s="T150">soldat</ta>
            <ta e="T152" id="Seg_1648" s="T151">šo-bi</ta>
            <ta e="T153" id="Seg_1649" s="T152">dĭ</ta>
            <ta e="T154" id="Seg_1650" s="T153">barɨnʼa-m</ta>
            <ta e="T155" id="Seg_1651" s="T154">det-bi</ta>
            <ta e="T156" id="Seg_1652" s="T155">maʔ-gəndə</ta>
            <ta e="T157" id="Seg_1653" s="T156">a</ta>
            <ta e="T158" id="Seg_1654" s="T157">kuznʼes-n</ta>
            <ta e="T159" id="Seg_1655" s="T158">ne-də</ta>
            <ta e="T160" id="Seg_1656" s="T159">tože</ta>
            <ta e="T161" id="Seg_1657" s="T160">maʔ-gəndə</ta>
            <ta e="T162" id="Seg_1658" s="T161">det-bi</ta>
            <ta e="T163" id="Seg_1659" s="T162">dĭgəttə</ta>
            <ta e="T164" id="Seg_1660" s="T163">ertə-n</ta>
            <ta e="T165" id="Seg_1661" s="T164">uʔbdə-bi</ta>
            <ta e="T166" id="Seg_1662" s="T165">kădaʔ</ta>
            <ta e="T167" id="Seg_1663" s="T166">măn</ta>
            <ta e="T168" id="Seg_1664" s="T167">döbər</ta>
            <ta e="T169" id="Seg_1665" s="T168">šo-bi-m</ta>
            <ta e="T170" id="Seg_1666" s="T169">bos-də</ta>
            <ta e="T172" id="Seg_1667" s="T171">maʔ-gəndə</ta>
            <ta e="T173" id="Seg_1668" s="T172">dĭgəttə</ta>
            <ta e="T174" id="Seg_1669" s="T173">surar-laʔbə</ta>
            <ta e="T175" id="Seg_1670" s="T174">bos-də</ta>
            <ta e="T176" id="Seg_1671" s="T175">da</ta>
            <ta e="T177" id="Seg_1672" s="T176">tăn</ta>
            <ta e="T178" id="Seg_1673" s="T177">gijen=də</ta>
            <ta e="T179" id="Seg_1674" s="T178">i</ta>
            <ta e="T180" id="Seg_1675" s="T179">naga-bi-l</ta>
            <ta e="T181" id="Seg_1676" s="T180">dön</ta>
            <ta e="T182" id="Seg_1677" s="T181">amno-bi-l</ta>
            <ta e="T183" id="Seg_1678" s="T182">dĭgəttə</ta>
            <ta e="T184" id="Seg_1679" s="T183">kuznʼes-n</ta>
            <ta e="T185" id="Seg_1680" s="T184">ne</ta>
            <ta e="T186" id="Seg_1681" s="T185">ne</ta>
            <ta e="T188" id="Seg_1682" s="T187">jakšə</ta>
            <ta e="T189" id="Seg_1683" s="T188">amno-laʔbə</ta>
            <ta e="T190" id="Seg_1684" s="T189">a</ta>
            <ta e="T191" id="Seg_1685" s="T190">dĭ</ta>
            <ta e="T192" id="Seg_1686" s="T191">ej</ta>
            <ta e="T194" id="Seg_1687" s="T193">kurojok</ta>
            <ta e="T195" id="Seg_1688" s="T194">mo-laːm-bi</ta>
            <ta e="T196" id="Seg_1689" s="T195">jakšə</ta>
            <ta e="T197" id="Seg_1690" s="T196">mo-laːm-bi</ta>
            <ta e="T198" id="Seg_1691" s="T197">dĭ</ta>
            <ta e="T199" id="Seg_1692" s="T198">barɨnʼa</ta>
            <ta e="T201" id="Seg_1693" s="T199">dĭgəttə</ta>
            <ta e="T202" id="Seg_1694" s="T201">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1695" s="T0">be-PST.[3SG]</ta>
            <ta e="T2" id="Seg_1696" s="T1">angry.[NOM.SG]</ta>
            <ta e="T3" id="Seg_1697" s="T2">woman.[NOM.SG]</ta>
            <ta e="T4" id="Seg_1698" s="T3">this.[NOM.SG]</ta>
            <ta e="T5" id="Seg_1699" s="T4">always</ta>
            <ta e="T6" id="Seg_1700" s="T5">be.angry-DUR.PST.[3SG]</ta>
            <ta e="T7" id="Seg_1701" s="T6">man-PL</ta>
            <ta e="T8" id="Seg_1702" s="T7">beat-DUR.PST.[3SG]</ta>
            <ta e="T9" id="Seg_1703" s="T8">bailiff.[NOM.SG]</ta>
            <ta e="T10" id="Seg_1704" s="T9">come-FUT-3SG</ta>
            <ta e="T11" id="Seg_1705" s="T10">this.[NOM.SG]</ta>
            <ta e="T12" id="Seg_1706" s="T11">all</ta>
            <ta e="T13" id="Seg_1707" s="T12">shout-DUR.[3SG]</ta>
            <ta e="T14" id="Seg_1708" s="T13">beat-FUT-3SG</ta>
            <ta e="T15" id="Seg_1709" s="T14">this-ACC</ta>
            <ta e="T16" id="Seg_1710" s="T15">very</ta>
            <ta e="T17" id="Seg_1711" s="T16">angry.[NOM.SG]</ta>
            <ta e="T18" id="Seg_1712" s="T17">people.[NOM.SG]</ta>
            <ta e="T19" id="Seg_1713" s="T18">fear-PRS-3PL</ta>
            <ta e="T20" id="Seg_1714" s="T19">this-ABL</ta>
            <ta e="T21" id="Seg_1715" s="T20">then</ta>
            <ta e="T22" id="Seg_1716" s="T21">come-PST.[3SG]</ta>
            <ta e="T23" id="Seg_1717" s="T22">one.[NOM.SG]</ta>
            <ta e="T24" id="Seg_1718" s="T23">soldier.[NOM.SG]</ta>
            <ta e="T25" id="Seg_1719" s="T24">I.NOM</ta>
            <ta e="T26" id="Seg_1720" s="T25">this-ACC</ta>
            <ta e="T27" id="Seg_1721" s="T26">teach-FUT-1SG</ta>
            <ta e="T28" id="Seg_1722" s="T27">evening-LOC.ADV</ta>
            <ta e="T29" id="Seg_1723" s="T28">harness-PST.[3SG]</ta>
            <ta e="T30" id="Seg_1724" s="T29">horse-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T31" id="Seg_1725" s="T30">come-PST.[3SG]</ta>
            <ta e="T32" id="Seg_1726" s="T31">this.[NOM.SG]</ta>
            <ta e="T33" id="Seg_1727" s="T32">this-ACC</ta>
            <ta e="T34" id="Seg_1728" s="T33">take-MOM-PST.[3SG]</ta>
            <ta e="T35" id="Seg_1729" s="T34">horse.[NOM.SG]</ta>
            <ta e="T36" id="Seg_1730" s="T35">horse-3PL-LAT</ta>
            <ta e="T37" id="Seg_1731" s="T36">put-PST.[3SG]</ta>
            <ta e="T38" id="Seg_1732" s="T37">bring-PST.[3SG]</ta>
            <ta e="T39" id="Seg_1733" s="T38">shoemaker-LAT</ta>
            <ta e="T40" id="Seg_1734" s="T39">shoemaker-EP-GEN</ta>
            <ta e="T41" id="Seg_1735" s="T40">woman-ACC.3SG</ta>
            <ta e="T42" id="Seg_1736" s="T41">bring-PST.[3SG]</ta>
            <ta e="T43" id="Seg_1737" s="T42">here</ta>
            <ta e="T44" id="Seg_1738" s="T43">where</ta>
            <ta e="T45" id="Seg_1739" s="T44">this.[NOM.SG]</ta>
            <ta e="T46" id="Seg_1740" s="T45">lady.[NOM.SG]</ta>
            <ta e="T47" id="Seg_1741" s="T46">live-PST.[3SG]</ta>
            <ta e="T48" id="Seg_1742" s="T47">then</ta>
            <ta e="T49" id="Seg_1743" s="T48">this.[NOM.SG]</ta>
            <ta e="T50" id="Seg_1744" s="T49">morning-LOC.ADV</ta>
            <ta e="T53" id="Seg_1745" s="T52">get.up-MOM-PST.[3SG]</ta>
            <ta e="T54" id="Seg_1746" s="T53">where</ta>
            <ta e="T55" id="Seg_1747" s="T54">I.NOM</ta>
            <ta e="T56" id="Seg_1748" s="T55">sleep-DUR-1SG</ta>
            <ta e="T57" id="Seg_1749" s="T56">this.[NOM.SG]</ta>
            <ta e="T58" id="Seg_1750" s="T57">this-LAT</ta>
            <ta e="T59" id="Seg_1751" s="T58">water.[NOM.SG]</ta>
            <ta e="T60" id="Seg_1752" s="T59">bring-PST-3PL</ta>
            <ta e="T61" id="Seg_1753" s="T60">bring-PST-3PL</ta>
            <ta e="T62" id="Seg_1754" s="T61">face-NOM/GEN/ACC.3SG</ta>
            <ta e="T63" id="Seg_1755" s="T62">rub-INF.LAT</ta>
            <ta e="T64" id="Seg_1756" s="T63">towel-INS</ta>
            <ta e="T65" id="Seg_1757" s="T64">this.[NOM.SG]</ta>
            <ta e="T66" id="Seg_1758" s="T65">rub-MOM-PST.[3SG]</ta>
            <ta e="T67" id="Seg_1759" s="T66">eat-INF.LAT</ta>
            <ta e="T68" id="Seg_1760" s="T67">give-PST-3PL</ta>
            <ta e="T69" id="Seg_1761" s="T68">%kettle.[NOM.SG]</ta>
            <ta e="T70" id="Seg_1762" s="T69">set.up-PST-3PL</ta>
            <ta e="T71" id="Seg_1763" s="T70">yellow.[NOM.SG]</ta>
            <ta e="T72" id="Seg_1764" s="T71">water.[NOM.SG]</ta>
            <ta e="T73" id="Seg_1765" s="T72">this.[NOM.SG]</ta>
            <ta e="T74" id="Seg_1766" s="T73">eat-DUR.[3SG]</ta>
            <ta e="T75" id="Seg_1767" s="T74">come-PST.[3SG]</ta>
            <ta e="T76" id="Seg_1768" s="T75">bailiff.[NOM.SG]</ta>
            <ta e="T77" id="Seg_1769" s="T76">this-LAT</ta>
            <ta e="T78" id="Seg_1770" s="T77">what.[NOM.SG]</ta>
            <ta e="T79" id="Seg_1771" s="T78">make-INF.LAT</ta>
            <ta e="T81" id="Seg_1772" s="T80">what.[NOM.SG]</ta>
            <ta e="T82" id="Seg_1773" s="T81">yesterday</ta>
            <ta e="T83" id="Seg_1774" s="T82">make-PST-2SG</ta>
            <ta e="T84" id="Seg_1775" s="T83">today</ta>
            <ta e="T85" id="Seg_1776" s="T84">and</ta>
            <ta e="T86" id="Seg_1777" s="T85">and</ta>
            <ta e="T87" id="Seg_1778" s="T86">this-ACC</ta>
            <ta e="T89" id="Seg_1779" s="T88">make-IMP.2SG</ta>
            <ta e="T90" id="Seg_1780" s="T89">this.[NOM.SG]</ta>
            <ta e="T91" id="Seg_1781" s="T90">go-PST.[3SG]</ta>
            <ta e="T92" id="Seg_1782" s="T91">look-PRS-3PL</ta>
            <ta e="T93" id="Seg_1783" s="T92">good.[NOM.SG]</ta>
            <ta e="T94" id="Seg_1784" s="T93">become-RES-PST.[3SG]</ta>
            <ta e="T95" id="Seg_1785" s="T94">lady.[NOM.SG]</ta>
            <ta e="T96" id="Seg_1786" s="T95">we.NOM</ta>
            <ta e="T97" id="Seg_1787" s="T96">NEG</ta>
            <ta e="T98" id="Seg_1788" s="T97">scold-DES-PRS.[3SG]</ta>
            <ta e="T99" id="Seg_1789" s="T98">then</ta>
            <ta e="T100" id="Seg_1790" s="T99">NEG</ta>
            <ta e="T101" id="Seg_1791" s="T100">beat-PRS.[3SG]</ta>
            <ta e="T102" id="Seg_1792" s="T101">who-ACC=INDEF</ta>
            <ta e="T103" id="Seg_1793" s="T102">then</ta>
            <ta e="T104" id="Seg_1794" s="T103">hear-PST.[3SG]</ta>
            <ta e="T105" id="Seg_1795" s="T104">this.[NOM.SG]</ta>
            <ta e="T107" id="Seg_1796" s="T106">soldier.[NOM.SG]</ta>
            <ta e="T108" id="Seg_1797" s="T107">two.[NOM.SG]</ta>
            <ta e="T109" id="Seg_1798" s="T108">month.[NOM.SG]</ta>
            <ta e="T110" id="Seg_1799" s="T109">go-PST.[3SG]</ta>
            <ta e="T111" id="Seg_1800" s="T110">and</ta>
            <ta e="T112" id="Seg_1801" s="T111">this.[NOM.SG]</ta>
            <ta e="T113" id="Seg_1802" s="T112">blacksmith-LOC</ta>
            <ta e="T114" id="Seg_1803" s="T113">lady.[NOM.SG]</ta>
            <ta e="T115" id="Seg_1804" s="T114">live-PST.[3SG]</ta>
            <ta e="T116" id="Seg_1805" s="T115">what.[NOM.SG]</ta>
            <ta e="T117" id="Seg_1806" s="T116">NEG</ta>
            <ta e="T118" id="Seg_1807" s="T117">get.up-PRS-2SG</ta>
            <ta e="T119" id="Seg_1808" s="T118">what.[NOM.SG]</ta>
            <ta e="T120" id="Seg_1809" s="T119">stove.[NOM.SG]</ta>
            <ta e="T121" id="Seg_1810" s="T120">NEG</ta>
            <ta e="T122" id="Seg_1811" s="T121">burn-PRS.[3SG]</ta>
            <ta e="T123" id="Seg_1812" s="T122">water-VBLZ-CVB</ta>
            <ta e="T124" id="Seg_1813" s="T123">go-EP-IMP.2SG</ta>
            <ta e="T125" id="Seg_1814" s="T124">this.[NOM.SG]</ta>
            <ta e="T126" id="Seg_1815" s="T125">say-IPFVZ.[3SG]</ta>
            <ta e="T127" id="Seg_1816" s="T126">bring-IMP.2SG</ta>
            <ta e="T128" id="Seg_1817" s="T127">I.LAT</ta>
            <ta e="T129" id="Seg_1818" s="T128">wash.oneself-IPFVZ-INF.LAT</ta>
            <ta e="T130" id="Seg_1819" s="T129">this.[NOM.SG]</ta>
            <ta e="T131" id="Seg_1820" s="T130">take-PST.[3SG]</ta>
            <ta e="T132" id="Seg_1821" s="T131">belt-NOM/GEN/ACC.3SG</ta>
            <ta e="T133" id="Seg_1822" s="T132">beat-PST.[3SG]</ta>
            <ta e="T134" id="Seg_1823" s="T133">beat-PST.[3SG]</ta>
            <ta e="T135" id="Seg_1824" s="T134">this.[NOM.SG]</ta>
            <ta e="T136" id="Seg_1825" s="T135">grab-PST.[3SG]</ta>
            <ta e="T137" id="Seg_1826" s="T136">cauldron.[NOM.SG]</ta>
            <ta e="T138" id="Seg_1827" s="T137">go-PST.[3SG]</ta>
            <ta e="T139" id="Seg_1828" s="T138">water-VBLZ-CVB</ta>
            <ta e="T140" id="Seg_1829" s="T139">what.[NOM.SG]=INDEF</ta>
            <ta e="T141" id="Seg_1830" s="T140">NEG</ta>
            <ta e="T142" id="Seg_1831" s="T141">can-PRS.[3SG]</ta>
            <ta e="T144" id="Seg_1832" s="T143">make-INF.LAT</ta>
            <ta e="T145" id="Seg_1833" s="T144">live-PST.[3SG]</ta>
            <ta e="T146" id="Seg_1834" s="T145">live-PST.[3SG]</ta>
            <ta e="T147" id="Seg_1835" s="T146">then</ta>
            <ta e="T148" id="Seg_1836" s="T147">again</ta>
            <ta e="T149" id="Seg_1837" s="T148">sleep-MOM-PST-3PL</ta>
            <ta e="T150" id="Seg_1838" s="T149">this.[NOM.SG]</ta>
            <ta e="T151" id="Seg_1839" s="T150">soldier.[NOM.SG]</ta>
            <ta e="T152" id="Seg_1840" s="T151">come-PST.[3SG]</ta>
            <ta e="T153" id="Seg_1841" s="T152">this.[NOM.SG]</ta>
            <ta e="T154" id="Seg_1842" s="T153">lady-ACC</ta>
            <ta e="T155" id="Seg_1843" s="T154">bring-PST.[3SG]</ta>
            <ta e="T156" id="Seg_1844" s="T155">tent-LAT/LOC.3SG</ta>
            <ta e="T157" id="Seg_1845" s="T156">and</ta>
            <ta e="T158" id="Seg_1846" s="T157">blacksmith-GEN</ta>
            <ta e="T159" id="Seg_1847" s="T158">woman-NOM/GEN/ACC.3SG</ta>
            <ta e="T160" id="Seg_1848" s="T159">also</ta>
            <ta e="T161" id="Seg_1849" s="T160">tent-LAT/LOC.3SG</ta>
            <ta e="T162" id="Seg_1850" s="T161">bring-PST.[3SG]</ta>
            <ta e="T163" id="Seg_1851" s="T162">then</ta>
            <ta e="T164" id="Seg_1852" s="T163">morning-LOC.ADV</ta>
            <ta e="T165" id="Seg_1853" s="T164">get.up-PST.[3SG]</ta>
            <ta e="T166" id="Seg_1854" s="T165">how</ta>
            <ta e="T167" id="Seg_1855" s="T166">I.NOM</ta>
            <ta e="T168" id="Seg_1856" s="T167">here</ta>
            <ta e="T169" id="Seg_1857" s="T168">come-PST-1SG</ta>
            <ta e="T170" id="Seg_1858" s="T169">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T172" id="Seg_1859" s="T171">tent-LAT/LOC.3SG</ta>
            <ta e="T173" id="Seg_1860" s="T172">then</ta>
            <ta e="T174" id="Seg_1861" s="T173">ask-DUR.[3SG]</ta>
            <ta e="T175" id="Seg_1862" s="T174">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T176" id="Seg_1863" s="T175">and</ta>
            <ta e="T177" id="Seg_1864" s="T176">you.GEN</ta>
            <ta e="T178" id="Seg_1865" s="T177">where=INDEF</ta>
            <ta e="T179" id="Seg_1866" s="T178">and</ta>
            <ta e="T180" id="Seg_1867" s="T179">NEG.EX-PST-2SG</ta>
            <ta e="T181" id="Seg_1868" s="T180">here</ta>
            <ta e="T182" id="Seg_1869" s="T181">live-PST-2SG</ta>
            <ta e="T183" id="Seg_1870" s="T182">then</ta>
            <ta e="T184" id="Seg_1871" s="T183">blacksmith-GEN</ta>
            <ta e="T185" id="Seg_1872" s="T184">woman.[NOM.SG]</ta>
            <ta e="T186" id="Seg_1873" s="T185">woman.[NOM.SG]</ta>
            <ta e="T188" id="Seg_1874" s="T187">good.[NOM.SG]</ta>
            <ta e="T189" id="Seg_1875" s="T188">live-DUR.[3SG]</ta>
            <ta e="T190" id="Seg_1876" s="T189">and</ta>
            <ta e="T191" id="Seg_1877" s="T190">this.[NOM.SG]</ta>
            <ta e="T192" id="Seg_1878" s="T191">NEG</ta>
            <ta e="T194" id="Seg_1879" s="T193">angry.[NOM.SG]</ta>
            <ta e="T195" id="Seg_1880" s="T194">become-RES-PST.[3SG]</ta>
            <ta e="T196" id="Seg_1881" s="T195">good.[NOM.SG]</ta>
            <ta e="T197" id="Seg_1882" s="T196">become-RES-PST.[3SG]</ta>
            <ta e="T198" id="Seg_1883" s="T197">this.[NOM.SG]</ta>
            <ta e="T199" id="Seg_1884" s="T198">lady.[NOM.SG]</ta>
            <ta e="T201" id="Seg_1885" s="T199">then</ta>
            <ta e="T202" id="Seg_1886" s="T201">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1887" s="T0">быть-PST.[3SG]</ta>
            <ta e="T2" id="Seg_1888" s="T1">сердитый.[NOM.SG]</ta>
            <ta e="T3" id="Seg_1889" s="T2">женщина.[NOM.SG]</ta>
            <ta e="T4" id="Seg_1890" s="T3">этот.[NOM.SG]</ta>
            <ta e="T5" id="Seg_1891" s="T4">всегда</ta>
            <ta e="T6" id="Seg_1892" s="T5">сердиться-DUR.PST.[3SG]</ta>
            <ta e="T7" id="Seg_1893" s="T6">мужчина-PL</ta>
            <ta e="T8" id="Seg_1894" s="T7">бить-DUR.PST.[3SG]</ta>
            <ta e="T9" id="Seg_1895" s="T8">староста.[NOM.SG]</ta>
            <ta e="T10" id="Seg_1896" s="T9">прийти-FUT-3SG</ta>
            <ta e="T11" id="Seg_1897" s="T10">этот.[NOM.SG]</ta>
            <ta e="T12" id="Seg_1898" s="T11">весь</ta>
            <ta e="T13" id="Seg_1899" s="T12">кричать-DUR.[3SG]</ta>
            <ta e="T14" id="Seg_1900" s="T13">бить-FUT-3SG</ta>
            <ta e="T15" id="Seg_1901" s="T14">этот-ACC</ta>
            <ta e="T16" id="Seg_1902" s="T15">очень</ta>
            <ta e="T17" id="Seg_1903" s="T16">сердитый.[NOM.SG]</ta>
            <ta e="T18" id="Seg_1904" s="T17">люди.[NOM.SG]</ta>
            <ta e="T19" id="Seg_1905" s="T18">бояться-PRS-3PL</ta>
            <ta e="T20" id="Seg_1906" s="T19">этот-ABL</ta>
            <ta e="T21" id="Seg_1907" s="T20">тогда</ta>
            <ta e="T22" id="Seg_1908" s="T21">прийти-PST.[3SG]</ta>
            <ta e="T23" id="Seg_1909" s="T22">один.[NOM.SG]</ta>
            <ta e="T24" id="Seg_1910" s="T23">солдат.[NOM.SG]</ta>
            <ta e="T25" id="Seg_1911" s="T24">я.NOM</ta>
            <ta e="T26" id="Seg_1912" s="T25">этот-ACC</ta>
            <ta e="T27" id="Seg_1913" s="T26">учить-FUT-1SG</ta>
            <ta e="T28" id="Seg_1914" s="T27">вечер-LOC.ADV</ta>
            <ta e="T29" id="Seg_1915" s="T28">запрячь-PST.[3SG]</ta>
            <ta e="T30" id="Seg_1916" s="T29">лошадь-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T31" id="Seg_1917" s="T30">прийти-PST.[3SG]</ta>
            <ta e="T32" id="Seg_1918" s="T31">этот.[NOM.SG]</ta>
            <ta e="T33" id="Seg_1919" s="T32">этот-ACC</ta>
            <ta e="T34" id="Seg_1920" s="T33">взять-MOM-PST.[3SG]</ta>
            <ta e="T35" id="Seg_1921" s="T34">лошадь.[NOM.SG]</ta>
            <ta e="T36" id="Seg_1922" s="T35">лошадь-3PL-LAT</ta>
            <ta e="T37" id="Seg_1923" s="T36">класть-PST.[3SG]</ta>
            <ta e="T38" id="Seg_1924" s="T37">принести-PST.[3SG]</ta>
            <ta e="T39" id="Seg_1925" s="T38">сапожник-LAT</ta>
            <ta e="T40" id="Seg_1926" s="T39">сапожник-EP-GEN</ta>
            <ta e="T41" id="Seg_1927" s="T40">женщина-ACC.3SG</ta>
            <ta e="T42" id="Seg_1928" s="T41">принести-PST.[3SG]</ta>
            <ta e="T43" id="Seg_1929" s="T42">здесь</ta>
            <ta e="T44" id="Seg_1930" s="T43">где</ta>
            <ta e="T45" id="Seg_1931" s="T44">этот.[NOM.SG]</ta>
            <ta e="T46" id="Seg_1932" s="T45">барыня.[NOM.SG]</ta>
            <ta e="T47" id="Seg_1933" s="T46">жить-PST.[3SG]</ta>
            <ta e="T48" id="Seg_1934" s="T47">тогда</ta>
            <ta e="T49" id="Seg_1935" s="T48">этот.[NOM.SG]</ta>
            <ta e="T50" id="Seg_1936" s="T49">утро-LOC.ADV</ta>
            <ta e="T53" id="Seg_1937" s="T52">встать-MOM-PST.[3SG]</ta>
            <ta e="T54" id="Seg_1938" s="T53">где</ta>
            <ta e="T55" id="Seg_1939" s="T54">я.NOM</ta>
            <ta e="T56" id="Seg_1940" s="T55">спать-DUR-1SG</ta>
            <ta e="T57" id="Seg_1941" s="T56">этот.[NOM.SG]</ta>
            <ta e="T58" id="Seg_1942" s="T57">этот-LAT</ta>
            <ta e="T59" id="Seg_1943" s="T58">вода.[NOM.SG]</ta>
            <ta e="T60" id="Seg_1944" s="T59">принести-PST-3PL</ta>
            <ta e="T61" id="Seg_1945" s="T60">принести-PST-3PL</ta>
            <ta e="T62" id="Seg_1946" s="T61">лицо-NOM/GEN/ACC.3SG</ta>
            <ta e="T63" id="Seg_1947" s="T62">тереть-INF.LAT</ta>
            <ta e="T64" id="Seg_1948" s="T63">рушник-INS</ta>
            <ta e="T65" id="Seg_1949" s="T64">этот.[NOM.SG]</ta>
            <ta e="T66" id="Seg_1950" s="T65">тереть-MOM-PST.[3SG]</ta>
            <ta e="T67" id="Seg_1951" s="T66">съесть-INF.LAT</ta>
            <ta e="T68" id="Seg_1952" s="T67">дать-PST-3PL</ta>
            <ta e="T69" id="Seg_1953" s="T68">%самовар.[NOM.SG]</ta>
            <ta e="T70" id="Seg_1954" s="T69">установить-PST-3PL</ta>
            <ta e="T71" id="Seg_1955" s="T70">желтый.[NOM.SG]</ta>
            <ta e="T72" id="Seg_1956" s="T71">вода.[NOM.SG]</ta>
            <ta e="T73" id="Seg_1957" s="T72">этот.[NOM.SG]</ta>
            <ta e="T74" id="Seg_1958" s="T73">есть-DUR.[3SG]</ta>
            <ta e="T75" id="Seg_1959" s="T74">прийти-PST.[3SG]</ta>
            <ta e="T76" id="Seg_1960" s="T75">староста.[NOM.SG]</ta>
            <ta e="T77" id="Seg_1961" s="T76">этот-LAT</ta>
            <ta e="T78" id="Seg_1962" s="T77">что.[NOM.SG]</ta>
            <ta e="T79" id="Seg_1963" s="T78">делать-INF.LAT</ta>
            <ta e="T81" id="Seg_1964" s="T80">что.[NOM.SG]</ta>
            <ta e="T82" id="Seg_1965" s="T81">вчера</ta>
            <ta e="T83" id="Seg_1966" s="T82">делать-PST-2SG</ta>
            <ta e="T84" id="Seg_1967" s="T83">сегодня</ta>
            <ta e="T85" id="Seg_1968" s="T84">и</ta>
            <ta e="T86" id="Seg_1969" s="T85">и</ta>
            <ta e="T87" id="Seg_1970" s="T86">этот-ACC</ta>
            <ta e="T89" id="Seg_1971" s="T88">делать-IMP.2SG</ta>
            <ta e="T90" id="Seg_1972" s="T89">этот.[NOM.SG]</ta>
            <ta e="T91" id="Seg_1973" s="T90">пойти-PST.[3SG]</ta>
            <ta e="T92" id="Seg_1974" s="T91">смотреть-PRS-3PL</ta>
            <ta e="T93" id="Seg_1975" s="T92">хороший.[NOM.SG]</ta>
            <ta e="T94" id="Seg_1976" s="T93">стать-RES-PST.[3SG]</ta>
            <ta e="T95" id="Seg_1977" s="T94">барыня.[NOM.SG]</ta>
            <ta e="T96" id="Seg_1978" s="T95">мы.NOM</ta>
            <ta e="T97" id="Seg_1979" s="T96">NEG</ta>
            <ta e="T98" id="Seg_1980" s="T97">ругать-DES-PRS.[3SG]</ta>
            <ta e="T99" id="Seg_1981" s="T98">тогда</ta>
            <ta e="T100" id="Seg_1982" s="T99">NEG</ta>
            <ta e="T101" id="Seg_1983" s="T100">бить-PRS.[3SG]</ta>
            <ta e="T102" id="Seg_1984" s="T101">кто-ACC=INDEF</ta>
            <ta e="T103" id="Seg_1985" s="T102">тогда</ta>
            <ta e="T104" id="Seg_1986" s="T103">слышать-PST.[3SG]</ta>
            <ta e="T105" id="Seg_1987" s="T104">этот.[NOM.SG]</ta>
            <ta e="T107" id="Seg_1988" s="T106">солдат.[NOM.SG]</ta>
            <ta e="T108" id="Seg_1989" s="T107">два.[NOM.SG]</ta>
            <ta e="T109" id="Seg_1990" s="T108">месяц.[NOM.SG]</ta>
            <ta e="T110" id="Seg_1991" s="T109">пойти-PST.[3SG]</ta>
            <ta e="T111" id="Seg_1992" s="T110">а</ta>
            <ta e="T112" id="Seg_1993" s="T111">этот.[NOM.SG]</ta>
            <ta e="T113" id="Seg_1994" s="T112">кузнец-LOC</ta>
            <ta e="T114" id="Seg_1995" s="T113">барыня.[NOM.SG]</ta>
            <ta e="T115" id="Seg_1996" s="T114">жить-PST.[3SG]</ta>
            <ta e="T116" id="Seg_1997" s="T115">что.[NOM.SG]</ta>
            <ta e="T117" id="Seg_1998" s="T116">NEG</ta>
            <ta e="T118" id="Seg_1999" s="T117">встать-PRS-2SG</ta>
            <ta e="T119" id="Seg_2000" s="T118">что.[NOM.SG]</ta>
            <ta e="T120" id="Seg_2001" s="T119">печь.[NOM.SG]</ta>
            <ta e="T121" id="Seg_2002" s="T120">NEG</ta>
            <ta e="T122" id="Seg_2003" s="T121">гореть-PRS.[3SG]</ta>
            <ta e="T123" id="Seg_2004" s="T122">вода-VBLZ-CVB</ta>
            <ta e="T124" id="Seg_2005" s="T123">пойти-EP-IMP.2SG</ta>
            <ta e="T125" id="Seg_2006" s="T124">этот.[NOM.SG]</ta>
            <ta e="T126" id="Seg_2007" s="T125">сказать-IPFVZ.[3SG]</ta>
            <ta e="T127" id="Seg_2008" s="T126">принести-IMP.2SG</ta>
            <ta e="T128" id="Seg_2009" s="T127">я.LAT</ta>
            <ta e="T129" id="Seg_2010" s="T128">мыться-IPFVZ-INF.LAT</ta>
            <ta e="T130" id="Seg_2011" s="T129">этот.[NOM.SG]</ta>
            <ta e="T131" id="Seg_2012" s="T130">взять-PST.[3SG]</ta>
            <ta e="T132" id="Seg_2013" s="T131">ремень-NOM/GEN/ACC.3SG</ta>
            <ta e="T133" id="Seg_2014" s="T132">бить-PST.[3SG]</ta>
            <ta e="T134" id="Seg_2015" s="T133">бить-PST.[3SG]</ta>
            <ta e="T135" id="Seg_2016" s="T134">этот.[NOM.SG]</ta>
            <ta e="T136" id="Seg_2017" s="T135">хватать-PST.[3SG]</ta>
            <ta e="T137" id="Seg_2018" s="T136">котел.[NOM.SG]</ta>
            <ta e="T138" id="Seg_2019" s="T137">пойти-PST.[3SG]</ta>
            <ta e="T139" id="Seg_2020" s="T138">вода-VBLZ-CVB</ta>
            <ta e="T140" id="Seg_2021" s="T139">что.[NOM.SG]=INDEF</ta>
            <ta e="T141" id="Seg_2022" s="T140">NEG</ta>
            <ta e="T142" id="Seg_2023" s="T141">мочь-PRS.[3SG]</ta>
            <ta e="T144" id="Seg_2024" s="T143">делать-INF.LAT</ta>
            <ta e="T145" id="Seg_2025" s="T144">жить-PST.[3SG]</ta>
            <ta e="T146" id="Seg_2026" s="T145">жить-PST.[3SG]</ta>
            <ta e="T147" id="Seg_2027" s="T146">тогда</ta>
            <ta e="T148" id="Seg_2028" s="T147">опять</ta>
            <ta e="T149" id="Seg_2029" s="T148">спать-MOM-PST-3PL</ta>
            <ta e="T150" id="Seg_2030" s="T149">этот.[NOM.SG]</ta>
            <ta e="T151" id="Seg_2031" s="T150">солдат.[NOM.SG]</ta>
            <ta e="T152" id="Seg_2032" s="T151">прийти-PST.[3SG]</ta>
            <ta e="T153" id="Seg_2033" s="T152">этот.[NOM.SG]</ta>
            <ta e="T154" id="Seg_2034" s="T153">барыня-ACC</ta>
            <ta e="T155" id="Seg_2035" s="T154">принести-PST.[3SG]</ta>
            <ta e="T156" id="Seg_2036" s="T155">чум-LAT/LOC.3SG</ta>
            <ta e="T157" id="Seg_2037" s="T156">а</ta>
            <ta e="T158" id="Seg_2038" s="T157">кузнец-GEN</ta>
            <ta e="T159" id="Seg_2039" s="T158">женщина-NOM/GEN/ACC.3SG</ta>
            <ta e="T160" id="Seg_2040" s="T159">тоже</ta>
            <ta e="T161" id="Seg_2041" s="T160">чум-LAT/LOC.3SG</ta>
            <ta e="T162" id="Seg_2042" s="T161">принести-PST.[3SG]</ta>
            <ta e="T163" id="Seg_2043" s="T162">тогда</ta>
            <ta e="T164" id="Seg_2044" s="T163">утро-LOC.ADV</ta>
            <ta e="T165" id="Seg_2045" s="T164">встать-PST.[3SG]</ta>
            <ta e="T166" id="Seg_2046" s="T165">как</ta>
            <ta e="T167" id="Seg_2047" s="T166">я.NOM</ta>
            <ta e="T168" id="Seg_2048" s="T167">здесь</ta>
            <ta e="T169" id="Seg_2049" s="T168">прийти-PST-1SG</ta>
            <ta e="T170" id="Seg_2050" s="T169">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T172" id="Seg_2051" s="T171">чум-LAT/LOC.3SG</ta>
            <ta e="T173" id="Seg_2052" s="T172">тогда</ta>
            <ta e="T174" id="Seg_2053" s="T173">спросить-DUR.[3SG]</ta>
            <ta e="T175" id="Seg_2054" s="T174">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T176" id="Seg_2055" s="T175">и</ta>
            <ta e="T177" id="Seg_2056" s="T176">ты.GEN</ta>
            <ta e="T178" id="Seg_2057" s="T177">где=INDEF</ta>
            <ta e="T179" id="Seg_2058" s="T178">и</ta>
            <ta e="T180" id="Seg_2059" s="T179">NEG.EX-PST-2SG</ta>
            <ta e="T181" id="Seg_2060" s="T180">здесь</ta>
            <ta e="T182" id="Seg_2061" s="T181">жить-PST-2SG</ta>
            <ta e="T183" id="Seg_2062" s="T182">тогда</ta>
            <ta e="T184" id="Seg_2063" s="T183">кузнец-GEN</ta>
            <ta e="T185" id="Seg_2064" s="T184">женщина.[NOM.SG]</ta>
            <ta e="T186" id="Seg_2065" s="T185">женщина.[NOM.SG]</ta>
            <ta e="T188" id="Seg_2066" s="T187">хороший.[NOM.SG]</ta>
            <ta e="T189" id="Seg_2067" s="T188">жить-DUR.[3SG]</ta>
            <ta e="T190" id="Seg_2068" s="T189">а</ta>
            <ta e="T191" id="Seg_2069" s="T190">этот.[NOM.SG]</ta>
            <ta e="T192" id="Seg_2070" s="T191">NEG</ta>
            <ta e="T194" id="Seg_2071" s="T193">сердитый.[NOM.SG]</ta>
            <ta e="T195" id="Seg_2072" s="T194">стать-RES-PST.[3SG]</ta>
            <ta e="T196" id="Seg_2073" s="T195">хороший.[NOM.SG]</ta>
            <ta e="T197" id="Seg_2074" s="T196">стать-RES-PST.[3SG]</ta>
            <ta e="T198" id="Seg_2075" s="T197">этот.[NOM.SG]</ta>
            <ta e="T199" id="Seg_2076" s="T198">барыня.[NOM.SG]</ta>
            <ta e="T201" id="Seg_2077" s="T199">тогда</ta>
            <ta e="T202" id="Seg_2078" s="T201">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_2079" s="T0">v-v:tense-v:pn</ta>
            <ta e="T2" id="Seg_2080" s="T1">adj-n:case</ta>
            <ta e="T3" id="Seg_2081" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_2082" s="T3">dempro-n:case</ta>
            <ta e="T5" id="Seg_2083" s="T4">adv</ta>
            <ta e="T6" id="Seg_2084" s="T5">v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_2085" s="T6">n-n:num</ta>
            <ta e="T8" id="Seg_2086" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_2087" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_2088" s="T9">v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_2089" s="T10">dempro-n:case</ta>
            <ta e="T12" id="Seg_2090" s="T11">quant</ta>
            <ta e="T13" id="Seg_2091" s="T12">v-v&gt;v-v:pn</ta>
            <ta e="T14" id="Seg_2092" s="T13">v-v:tense-v:pn</ta>
            <ta e="T15" id="Seg_2093" s="T14">dempro-n:case</ta>
            <ta e="T16" id="Seg_2094" s="T15">adv</ta>
            <ta e="T17" id="Seg_2095" s="T16">adj-n:case</ta>
            <ta e="T18" id="Seg_2096" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_2097" s="T18">v-v:tense-v:pn</ta>
            <ta e="T20" id="Seg_2098" s="T19">dempro-n:case</ta>
            <ta e="T21" id="Seg_2099" s="T20">adv</ta>
            <ta e="T22" id="Seg_2100" s="T21">v-v:tense-v:pn</ta>
            <ta e="T23" id="Seg_2101" s="T22">num-n:case</ta>
            <ta e="T24" id="Seg_2102" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_2103" s="T24">pers</ta>
            <ta e="T26" id="Seg_2104" s="T25">dempro-n:case</ta>
            <ta e="T27" id="Seg_2105" s="T26">v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_2106" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_2107" s="T28">v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_2108" s="T29">n-n:num-n:case.poss</ta>
            <ta e="T31" id="Seg_2109" s="T30">v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_2110" s="T31">dempro-n:case</ta>
            <ta e="T33" id="Seg_2111" s="T32">dempro-n:case</ta>
            <ta e="T34" id="Seg_2112" s="T33">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_2113" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_2114" s="T35">n-n:case.poss-n:case</ta>
            <ta e="T37" id="Seg_2115" s="T36">v-v:tense-v:pn</ta>
            <ta e="T38" id="Seg_2116" s="T37">v-v:tense-v:pn</ta>
            <ta e="T39" id="Seg_2117" s="T38">n-n:case</ta>
            <ta e="T40" id="Seg_2118" s="T39">n-n:ins-n:case</ta>
            <ta e="T41" id="Seg_2119" s="T40">n-n:case.poss</ta>
            <ta e="T42" id="Seg_2120" s="T41">v-v:tense-v:pn</ta>
            <ta e="T43" id="Seg_2121" s="T42">adv</ta>
            <ta e="T44" id="Seg_2122" s="T43">que</ta>
            <ta e="T45" id="Seg_2123" s="T44">dempro-n:case</ta>
            <ta e="T46" id="Seg_2124" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_2125" s="T46">v-v:tense-v:pn</ta>
            <ta e="T48" id="Seg_2126" s="T47">adv</ta>
            <ta e="T49" id="Seg_2127" s="T48">dempro-n:case</ta>
            <ta e="T50" id="Seg_2128" s="T49">n-adv:case</ta>
            <ta e="T53" id="Seg_2129" s="T52">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T54" id="Seg_2130" s="T53">que</ta>
            <ta e="T55" id="Seg_2131" s="T54">pers</ta>
            <ta e="T56" id="Seg_2132" s="T55">v-v&gt;v-v:pn</ta>
            <ta e="T57" id="Seg_2133" s="T56">dempro-n:case</ta>
            <ta e="T58" id="Seg_2134" s="T57">dempro-n:case</ta>
            <ta e="T59" id="Seg_2135" s="T58">n-n:case</ta>
            <ta e="T60" id="Seg_2136" s="T59">v-v:tense-v:pn</ta>
            <ta e="T61" id="Seg_2137" s="T60">v-v:tense-v:pn</ta>
            <ta e="T62" id="Seg_2138" s="T61">n-n:case.poss</ta>
            <ta e="T63" id="Seg_2139" s="T62">v-v:n.fin</ta>
            <ta e="T64" id="Seg_2140" s="T63">n-n:case</ta>
            <ta e="T65" id="Seg_2141" s="T64">dempro-n:case</ta>
            <ta e="T66" id="Seg_2142" s="T65">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T67" id="Seg_2143" s="T66">v-v:n.fin</ta>
            <ta e="T68" id="Seg_2144" s="T67">v-v:tense-v:pn</ta>
            <ta e="T69" id="Seg_2145" s="T68">n-n:case</ta>
            <ta e="T70" id="Seg_2146" s="T69">v-v:tense-v:pn</ta>
            <ta e="T71" id="Seg_2147" s="T70">adj-n:case</ta>
            <ta e="T72" id="Seg_2148" s="T71">n-n:case</ta>
            <ta e="T73" id="Seg_2149" s="T72">dempro-n:case</ta>
            <ta e="T74" id="Seg_2150" s="T73">v-v&gt;v-v:pn</ta>
            <ta e="T75" id="Seg_2151" s="T74">v-v:tense-v:pn</ta>
            <ta e="T76" id="Seg_2152" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_2153" s="T76">dempro-n:case</ta>
            <ta e="T78" id="Seg_2154" s="T77">que-n:case</ta>
            <ta e="T79" id="Seg_2155" s="T78">v-v:n.fin</ta>
            <ta e="T81" id="Seg_2156" s="T80">que-n:case</ta>
            <ta e="T82" id="Seg_2157" s="T81">adv</ta>
            <ta e="T83" id="Seg_2158" s="T82">v-v:tense-v:pn</ta>
            <ta e="T84" id="Seg_2159" s="T83">adv</ta>
            <ta e="T85" id="Seg_2160" s="T84">conj</ta>
            <ta e="T86" id="Seg_2161" s="T85">conj</ta>
            <ta e="T87" id="Seg_2162" s="T86">dempro-n:case</ta>
            <ta e="T89" id="Seg_2163" s="T88">v-v:mood.pn</ta>
            <ta e="T90" id="Seg_2164" s="T89">dempro-n:case</ta>
            <ta e="T91" id="Seg_2165" s="T90">v-v:tense-v:pn</ta>
            <ta e="T92" id="Seg_2166" s="T91">v-v:tense-v:pn</ta>
            <ta e="T93" id="Seg_2167" s="T92">adj-n:case</ta>
            <ta e="T94" id="Seg_2168" s="T93">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T95" id="Seg_2169" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_2170" s="T95">pers</ta>
            <ta e="T97" id="Seg_2171" s="T96">ptcl</ta>
            <ta e="T98" id="Seg_2172" s="T97">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T99" id="Seg_2173" s="T98">adv</ta>
            <ta e="T100" id="Seg_2174" s="T99">ptcl</ta>
            <ta e="T101" id="Seg_2175" s="T100">v-v:tense-v:pn</ta>
            <ta e="T102" id="Seg_2176" s="T101">que-n:case=ptcl</ta>
            <ta e="T103" id="Seg_2177" s="T102">adv</ta>
            <ta e="T104" id="Seg_2178" s="T103">v-v:tense-v:pn</ta>
            <ta e="T105" id="Seg_2179" s="T104">dempro-n:case</ta>
            <ta e="T107" id="Seg_2180" s="T106">n-n:case</ta>
            <ta e="T108" id="Seg_2181" s="T107">num-n:case</ta>
            <ta e="T109" id="Seg_2182" s="T108">n-n:case</ta>
            <ta e="T110" id="Seg_2183" s="T109">v-v:tense-v:pn</ta>
            <ta e="T111" id="Seg_2184" s="T110">conj</ta>
            <ta e="T112" id="Seg_2185" s="T111">dempro-n:case</ta>
            <ta e="T113" id="Seg_2186" s="T112">n-n:case</ta>
            <ta e="T114" id="Seg_2187" s="T113">n-n:case</ta>
            <ta e="T115" id="Seg_2188" s="T114">v-v:tense-v:pn</ta>
            <ta e="T116" id="Seg_2189" s="T115">que-n:case</ta>
            <ta e="T117" id="Seg_2190" s="T116">ptcl</ta>
            <ta e="T118" id="Seg_2191" s="T117">v-v:tense-v:pn</ta>
            <ta e="T119" id="Seg_2192" s="T118">que-n:case</ta>
            <ta e="T120" id="Seg_2193" s="T119">n-n:case</ta>
            <ta e="T121" id="Seg_2194" s="T120">ptcl</ta>
            <ta e="T122" id="Seg_2195" s="T121">v-v:tense-v:pn</ta>
            <ta e="T123" id="Seg_2196" s="T122">n-n&gt;v-v:n.fin</ta>
            <ta e="T124" id="Seg_2197" s="T123">v-v:ins-v:mood.pn</ta>
            <ta e="T125" id="Seg_2198" s="T124">dempro-n:case</ta>
            <ta e="T126" id="Seg_2199" s="T125">v-v&gt;v-v:pn</ta>
            <ta e="T127" id="Seg_2200" s="T126">v-v:mood.pn</ta>
            <ta e="T128" id="Seg_2201" s="T127">pers</ta>
            <ta e="T129" id="Seg_2202" s="T128">v-v&gt;v-v:n.fin</ta>
            <ta e="T130" id="Seg_2203" s="T129">dempro-n:case</ta>
            <ta e="T131" id="Seg_2204" s="T130">v-v:tense-v:pn</ta>
            <ta e="T132" id="Seg_2205" s="T131">n-n:case.poss</ta>
            <ta e="T133" id="Seg_2206" s="T132">v-v:tense-v:pn</ta>
            <ta e="T134" id="Seg_2207" s="T133">v-v:tense-v:pn</ta>
            <ta e="T135" id="Seg_2208" s="T134">dempro-n:case</ta>
            <ta e="T136" id="Seg_2209" s="T135">v-v:tense-v:pn</ta>
            <ta e="T137" id="Seg_2210" s="T136">n-n:case</ta>
            <ta e="T138" id="Seg_2211" s="T137">v-v:tense-v:pn</ta>
            <ta e="T139" id="Seg_2212" s="T138">n-n&gt;v-v:n.fin</ta>
            <ta e="T140" id="Seg_2213" s="T139">que-n:case=ptcl</ta>
            <ta e="T141" id="Seg_2214" s="T140">ptcl</ta>
            <ta e="T142" id="Seg_2215" s="T141">v-v:tense-v:pn</ta>
            <ta e="T144" id="Seg_2216" s="T143">v-v:n.fin</ta>
            <ta e="T145" id="Seg_2217" s="T144">v-v:tense-v:pn</ta>
            <ta e="T146" id="Seg_2218" s="T145">v-v:tense-v:pn</ta>
            <ta e="T147" id="Seg_2219" s="T146">adv</ta>
            <ta e="T148" id="Seg_2220" s="T147">adv</ta>
            <ta e="T149" id="Seg_2221" s="T148">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T150" id="Seg_2222" s="T149">dempro-n:case</ta>
            <ta e="T151" id="Seg_2223" s="T150">n-n:case</ta>
            <ta e="T152" id="Seg_2224" s="T151">v-v:tense-v:pn</ta>
            <ta e="T153" id="Seg_2225" s="T152">dempro-n:case</ta>
            <ta e="T154" id="Seg_2226" s="T153">n-n:case</ta>
            <ta e="T155" id="Seg_2227" s="T154">v-v:tense-v:pn</ta>
            <ta e="T156" id="Seg_2228" s="T155">n-n:case.poss</ta>
            <ta e="T157" id="Seg_2229" s="T156">conj</ta>
            <ta e="T158" id="Seg_2230" s="T157">n-n:case</ta>
            <ta e="T159" id="Seg_2231" s="T158">n-n:case.poss</ta>
            <ta e="T160" id="Seg_2232" s="T159">ptcl</ta>
            <ta e="T161" id="Seg_2233" s="T160">n-n:case.poss</ta>
            <ta e="T162" id="Seg_2234" s="T161">v-v:tense-v:pn</ta>
            <ta e="T163" id="Seg_2235" s="T162">adv</ta>
            <ta e="T164" id="Seg_2236" s="T163">n-adv:case</ta>
            <ta e="T165" id="Seg_2237" s="T164">v-v:tense-v:pn</ta>
            <ta e="T166" id="Seg_2238" s="T165">que</ta>
            <ta e="T167" id="Seg_2239" s="T166">pers</ta>
            <ta e="T168" id="Seg_2240" s="T167">adv</ta>
            <ta e="T169" id="Seg_2241" s="T168">v-v:tense-v:pn</ta>
            <ta e="T170" id="Seg_2242" s="T169">refl-n:case.poss</ta>
            <ta e="T172" id="Seg_2243" s="T171">n-n:case.poss</ta>
            <ta e="T173" id="Seg_2244" s="T172">adv</ta>
            <ta e="T174" id="Seg_2245" s="T173">v-v&gt;v-v:pn</ta>
            <ta e="T175" id="Seg_2246" s="T174">refl-n:case.poss</ta>
            <ta e="T176" id="Seg_2247" s="T175">conj</ta>
            <ta e="T177" id="Seg_2248" s="T176">pers</ta>
            <ta e="T178" id="Seg_2249" s="T177">que=ptcl</ta>
            <ta e="T179" id="Seg_2250" s="T178">conj</ta>
            <ta e="T180" id="Seg_2251" s="T179">v-v:tense-v:pn</ta>
            <ta e="T181" id="Seg_2252" s="T180">adv</ta>
            <ta e="T182" id="Seg_2253" s="T181">v-v:tense-v:pn</ta>
            <ta e="T183" id="Seg_2254" s="T182">adv</ta>
            <ta e="T184" id="Seg_2255" s="T183">n-n:case</ta>
            <ta e="T185" id="Seg_2256" s="T184">n-n:case</ta>
            <ta e="T186" id="Seg_2257" s="T185">n-n:case</ta>
            <ta e="T188" id="Seg_2258" s="T187">adj-n:case</ta>
            <ta e="T189" id="Seg_2259" s="T188">v-v&gt;v-v:pn</ta>
            <ta e="T190" id="Seg_2260" s="T189">conj</ta>
            <ta e="T191" id="Seg_2261" s="T190">dempro-n:case</ta>
            <ta e="T192" id="Seg_2262" s="T191">ptcl</ta>
            <ta e="T194" id="Seg_2263" s="T193">adj-n:case</ta>
            <ta e="T195" id="Seg_2264" s="T194">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T196" id="Seg_2265" s="T195">adj-n:case</ta>
            <ta e="T197" id="Seg_2266" s="T196">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T198" id="Seg_2267" s="T197">dempro-n:case</ta>
            <ta e="T199" id="Seg_2268" s="T198">n-n:case</ta>
            <ta e="T201" id="Seg_2269" s="T199">adv</ta>
            <ta e="T202" id="Seg_2270" s="T201">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_2271" s="T0">v</ta>
            <ta e="T2" id="Seg_2272" s="T1">adj</ta>
            <ta e="T3" id="Seg_2273" s="T2">n</ta>
            <ta e="T4" id="Seg_2274" s="T3">dempro</ta>
            <ta e="T5" id="Seg_2275" s="T4">adv</ta>
            <ta e="T6" id="Seg_2276" s="T5">v</ta>
            <ta e="T7" id="Seg_2277" s="T6">n</ta>
            <ta e="T8" id="Seg_2278" s="T7">v</ta>
            <ta e="T9" id="Seg_2279" s="T8">n</ta>
            <ta e="T10" id="Seg_2280" s="T9">v</ta>
            <ta e="T11" id="Seg_2281" s="T10">dempro</ta>
            <ta e="T12" id="Seg_2282" s="T11">quant</ta>
            <ta e="T13" id="Seg_2283" s="T12">v</ta>
            <ta e="T14" id="Seg_2284" s="T13">v</ta>
            <ta e="T15" id="Seg_2285" s="T14">dempro</ta>
            <ta e="T16" id="Seg_2286" s="T15">adv</ta>
            <ta e="T17" id="Seg_2287" s="T16">adj</ta>
            <ta e="T18" id="Seg_2288" s="T17">n</ta>
            <ta e="T19" id="Seg_2289" s="T18">v</ta>
            <ta e="T20" id="Seg_2290" s="T19">dempro</ta>
            <ta e="T21" id="Seg_2291" s="T20">adv</ta>
            <ta e="T22" id="Seg_2292" s="T21">v</ta>
            <ta e="T23" id="Seg_2293" s="T22">num</ta>
            <ta e="T24" id="Seg_2294" s="T23">n</ta>
            <ta e="T25" id="Seg_2295" s="T24">pers</ta>
            <ta e="T26" id="Seg_2296" s="T25">dempro</ta>
            <ta e="T27" id="Seg_2297" s="T26">v</ta>
            <ta e="T28" id="Seg_2298" s="T27">n</ta>
            <ta e="T29" id="Seg_2299" s="T28">v</ta>
            <ta e="T30" id="Seg_2300" s="T29">n</ta>
            <ta e="T31" id="Seg_2301" s="T30">v</ta>
            <ta e="T32" id="Seg_2302" s="T31">dempro</ta>
            <ta e="T33" id="Seg_2303" s="T32">dempro</ta>
            <ta e="T34" id="Seg_2304" s="T33">v</ta>
            <ta e="T35" id="Seg_2305" s="T34">n</ta>
            <ta e="T36" id="Seg_2306" s="T35">n</ta>
            <ta e="T37" id="Seg_2307" s="T36">v</ta>
            <ta e="T38" id="Seg_2308" s="T37">v</ta>
            <ta e="T39" id="Seg_2309" s="T38">n</ta>
            <ta e="T40" id="Seg_2310" s="T39">n</ta>
            <ta e="T41" id="Seg_2311" s="T40">n</ta>
            <ta e="T42" id="Seg_2312" s="T41">v</ta>
            <ta e="T43" id="Seg_2313" s="T42">adv</ta>
            <ta e="T44" id="Seg_2314" s="T43">que</ta>
            <ta e="T45" id="Seg_2315" s="T44">dempro</ta>
            <ta e="T46" id="Seg_2316" s="T45">n</ta>
            <ta e="T47" id="Seg_2317" s="T46">v</ta>
            <ta e="T48" id="Seg_2318" s="T47">adv</ta>
            <ta e="T49" id="Seg_2319" s="T48">dempro</ta>
            <ta e="T50" id="Seg_2320" s="T49">adv</ta>
            <ta e="T53" id="Seg_2321" s="T52">v</ta>
            <ta e="T54" id="Seg_2322" s="T53">que</ta>
            <ta e="T55" id="Seg_2323" s="T54">pers</ta>
            <ta e="T56" id="Seg_2324" s="T55">v</ta>
            <ta e="T57" id="Seg_2325" s="T56">dempro</ta>
            <ta e="T58" id="Seg_2326" s="T57">dempro</ta>
            <ta e="T59" id="Seg_2327" s="T58">n</ta>
            <ta e="T60" id="Seg_2328" s="T59">v</ta>
            <ta e="T61" id="Seg_2329" s="T60">v</ta>
            <ta e="T62" id="Seg_2330" s="T61">n</ta>
            <ta e="T63" id="Seg_2331" s="T62">v</ta>
            <ta e="T64" id="Seg_2332" s="T63">n</ta>
            <ta e="T65" id="Seg_2333" s="T64">dempro</ta>
            <ta e="T66" id="Seg_2334" s="T65">v</ta>
            <ta e="T67" id="Seg_2335" s="T66">v</ta>
            <ta e="T68" id="Seg_2336" s="T67">v</ta>
            <ta e="T69" id="Seg_2337" s="T68">n</ta>
            <ta e="T70" id="Seg_2338" s="T69">v</ta>
            <ta e="T71" id="Seg_2339" s="T70">adj</ta>
            <ta e="T72" id="Seg_2340" s="T71">n</ta>
            <ta e="T73" id="Seg_2341" s="T72">dempro</ta>
            <ta e="T74" id="Seg_2342" s="T73">v</ta>
            <ta e="T75" id="Seg_2343" s="T74">v</ta>
            <ta e="T76" id="Seg_2344" s="T75">n</ta>
            <ta e="T77" id="Seg_2345" s="T76">dempro</ta>
            <ta e="T78" id="Seg_2346" s="T77">que</ta>
            <ta e="T79" id="Seg_2347" s="T78">v</ta>
            <ta e="T81" id="Seg_2348" s="T80">que</ta>
            <ta e="T82" id="Seg_2349" s="T81">adv</ta>
            <ta e="T83" id="Seg_2350" s="T82">v</ta>
            <ta e="T84" id="Seg_2351" s="T83">adv</ta>
            <ta e="T85" id="Seg_2352" s="T84">conj</ta>
            <ta e="T86" id="Seg_2353" s="T85">conj</ta>
            <ta e="T87" id="Seg_2354" s="T86">dempro</ta>
            <ta e="T89" id="Seg_2355" s="T88">v</ta>
            <ta e="T90" id="Seg_2356" s="T89">dempro</ta>
            <ta e="T91" id="Seg_2357" s="T90">v</ta>
            <ta e="T92" id="Seg_2358" s="T91">v</ta>
            <ta e="T93" id="Seg_2359" s="T92">adj</ta>
            <ta e="T94" id="Seg_2360" s="T93">v</ta>
            <ta e="T95" id="Seg_2361" s="T94">n</ta>
            <ta e="T96" id="Seg_2362" s="T95">pers</ta>
            <ta e="T97" id="Seg_2363" s="T96">ptcl</ta>
            <ta e="T98" id="Seg_2364" s="T97">v</ta>
            <ta e="T99" id="Seg_2365" s="T98">adv</ta>
            <ta e="T100" id="Seg_2366" s="T99">ptcl</ta>
            <ta e="T101" id="Seg_2367" s="T100">v</ta>
            <ta e="T102" id="Seg_2368" s="T101">que</ta>
            <ta e="T103" id="Seg_2369" s="T102">adv</ta>
            <ta e="T104" id="Seg_2370" s="T103">v</ta>
            <ta e="T105" id="Seg_2371" s="T104">dempro</ta>
            <ta e="T107" id="Seg_2372" s="T106">n</ta>
            <ta e="T108" id="Seg_2373" s="T107">num</ta>
            <ta e="T109" id="Seg_2374" s="T108">n</ta>
            <ta e="T110" id="Seg_2375" s="T109">v</ta>
            <ta e="T111" id="Seg_2376" s="T110">conj</ta>
            <ta e="T112" id="Seg_2377" s="T111">dempro</ta>
            <ta e="T113" id="Seg_2378" s="T112">n</ta>
            <ta e="T114" id="Seg_2379" s="T113">n</ta>
            <ta e="T115" id="Seg_2380" s="T114">v</ta>
            <ta e="T116" id="Seg_2381" s="T115">que</ta>
            <ta e="T117" id="Seg_2382" s="T116">ptcl</ta>
            <ta e="T118" id="Seg_2383" s="T117">v</ta>
            <ta e="T119" id="Seg_2384" s="T118">que</ta>
            <ta e="T120" id="Seg_2385" s="T119">n</ta>
            <ta e="T121" id="Seg_2386" s="T120">ptcl</ta>
            <ta e="T122" id="Seg_2387" s="T121">v</ta>
            <ta e="T123" id="Seg_2388" s="T122">v</ta>
            <ta e="T124" id="Seg_2389" s="T123">v</ta>
            <ta e="T125" id="Seg_2390" s="T124">dempro</ta>
            <ta e="T126" id="Seg_2391" s="T125">v</ta>
            <ta e="T127" id="Seg_2392" s="T126">v</ta>
            <ta e="T128" id="Seg_2393" s="T127">pers</ta>
            <ta e="T129" id="Seg_2394" s="T128">v</ta>
            <ta e="T130" id="Seg_2395" s="T129">dempro</ta>
            <ta e="T131" id="Seg_2396" s="T130">v</ta>
            <ta e="T132" id="Seg_2397" s="T131">n</ta>
            <ta e="T133" id="Seg_2398" s="T132">v</ta>
            <ta e="T134" id="Seg_2399" s="T133">v</ta>
            <ta e="T135" id="Seg_2400" s="T134">dempro</ta>
            <ta e="T136" id="Seg_2401" s="T135">v</ta>
            <ta e="T137" id="Seg_2402" s="T136">n</ta>
            <ta e="T138" id="Seg_2403" s="T137">v</ta>
            <ta e="T139" id="Seg_2404" s="T138">v</ta>
            <ta e="T140" id="Seg_2405" s="T139">que</ta>
            <ta e="T141" id="Seg_2406" s="T140">ptcl</ta>
            <ta e="T142" id="Seg_2407" s="T141">v</ta>
            <ta e="T144" id="Seg_2408" s="T143">v</ta>
            <ta e="T145" id="Seg_2409" s="T144">v</ta>
            <ta e="T146" id="Seg_2410" s="T145">v</ta>
            <ta e="T147" id="Seg_2411" s="T146">adv</ta>
            <ta e="T148" id="Seg_2412" s="T147">adv</ta>
            <ta e="T149" id="Seg_2413" s="T148">v</ta>
            <ta e="T150" id="Seg_2414" s="T149">dempro</ta>
            <ta e="T151" id="Seg_2415" s="T150">n</ta>
            <ta e="T152" id="Seg_2416" s="T151">v</ta>
            <ta e="T153" id="Seg_2417" s="T152">dempro</ta>
            <ta e="T154" id="Seg_2418" s="T153">n</ta>
            <ta e="T155" id="Seg_2419" s="T154">v</ta>
            <ta e="T156" id="Seg_2420" s="T155">n</ta>
            <ta e="T157" id="Seg_2421" s="T156">conj</ta>
            <ta e="T158" id="Seg_2422" s="T157">n</ta>
            <ta e="T159" id="Seg_2423" s="T158">n</ta>
            <ta e="T160" id="Seg_2424" s="T159">ptcl</ta>
            <ta e="T161" id="Seg_2425" s="T160">n</ta>
            <ta e="T162" id="Seg_2426" s="T161">v</ta>
            <ta e="T163" id="Seg_2427" s="T162">adv</ta>
            <ta e="T164" id="Seg_2428" s="T163">adv</ta>
            <ta e="T165" id="Seg_2429" s="T164">v</ta>
            <ta e="T166" id="Seg_2430" s="T165">que</ta>
            <ta e="T167" id="Seg_2431" s="T166">pers</ta>
            <ta e="T168" id="Seg_2432" s="T167">adv</ta>
            <ta e="T169" id="Seg_2433" s="T168">v</ta>
            <ta e="T170" id="Seg_2434" s="T169">refl</ta>
            <ta e="T172" id="Seg_2435" s="T171">n</ta>
            <ta e="T173" id="Seg_2436" s="T172">adv</ta>
            <ta e="T174" id="Seg_2437" s="T173">v</ta>
            <ta e="T175" id="Seg_2438" s="T174">refl</ta>
            <ta e="T176" id="Seg_2439" s="T175">conj</ta>
            <ta e="T177" id="Seg_2440" s="T176">pers</ta>
            <ta e="T178" id="Seg_2441" s="T177">que</ta>
            <ta e="T179" id="Seg_2442" s="T178">conj</ta>
            <ta e="T180" id="Seg_2443" s="T179">v</ta>
            <ta e="T181" id="Seg_2444" s="T180">adv</ta>
            <ta e="T182" id="Seg_2445" s="T181">v</ta>
            <ta e="T183" id="Seg_2446" s="T182">adv</ta>
            <ta e="T184" id="Seg_2447" s="T183">n</ta>
            <ta e="T185" id="Seg_2448" s="T184">n</ta>
            <ta e="T186" id="Seg_2449" s="T185">n</ta>
            <ta e="T188" id="Seg_2450" s="T187">adj</ta>
            <ta e="T189" id="Seg_2451" s="T188">v</ta>
            <ta e="T190" id="Seg_2452" s="T189">conj</ta>
            <ta e="T191" id="Seg_2453" s="T190">dempro</ta>
            <ta e="T192" id="Seg_2454" s="T191">ptcl</ta>
            <ta e="T194" id="Seg_2455" s="T193">adj</ta>
            <ta e="T195" id="Seg_2456" s="T194">v</ta>
            <ta e="T196" id="Seg_2457" s="T195">adj</ta>
            <ta e="T197" id="Seg_2458" s="T196">v</ta>
            <ta e="T198" id="Seg_2459" s="T197">dempro</ta>
            <ta e="T199" id="Seg_2460" s="T198">n</ta>
            <ta e="T201" id="Seg_2461" s="T199">adv</ta>
            <ta e="T202" id="Seg_2462" s="T201">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_2463" s="T2">np.h:Th</ta>
            <ta e="T4" id="Seg_2464" s="T3">pro.h:Th</ta>
            <ta e="T7" id="Seg_2465" s="T6">np.h:E</ta>
            <ta e="T8" id="Seg_2466" s="T7">0.3.h:A</ta>
            <ta e="T9" id="Seg_2467" s="T8">np.h:A</ta>
            <ta e="T11" id="Seg_2468" s="T10">pro.h:A</ta>
            <ta e="T14" id="Seg_2469" s="T13">0.3.h:A</ta>
            <ta e="T15" id="Seg_2470" s="T14">pro.h:E</ta>
            <ta e="T18" id="Seg_2471" s="T17">np.h:E</ta>
            <ta e="T20" id="Seg_2472" s="T19">pro.h:Th</ta>
            <ta e="T21" id="Seg_2473" s="T20">adv:Time</ta>
            <ta e="T24" id="Seg_2474" s="T23">np.h:A</ta>
            <ta e="T25" id="Seg_2475" s="T24">pro.h:A</ta>
            <ta e="T26" id="Seg_2476" s="T25">pro.h:Th</ta>
            <ta e="T28" id="Seg_2477" s="T27">n:Time</ta>
            <ta e="T29" id="Seg_2478" s="T28">0.3.h:A</ta>
            <ta e="T30" id="Seg_2479" s="T29">np:Th</ta>
            <ta e="T31" id="Seg_2480" s="T30">0.3.h:A</ta>
            <ta e="T32" id="Seg_2481" s="T31">pro.h:A</ta>
            <ta e="T33" id="Seg_2482" s="T32">pro.h:Th</ta>
            <ta e="T36" id="Seg_2483" s="T35">np:G</ta>
            <ta e="T37" id="Seg_2484" s="T36">0.3.h:A</ta>
            <ta e="T38" id="Seg_2485" s="T37">0.3.h:A</ta>
            <ta e="T39" id="Seg_2486" s="T38">np:G</ta>
            <ta e="T40" id="Seg_2487" s="T39">np.h:Poss</ta>
            <ta e="T41" id="Seg_2488" s="T40">np.h:Th</ta>
            <ta e="T42" id="Seg_2489" s="T41">0.3.h:A</ta>
            <ta e="T43" id="Seg_2490" s="T42">adv:L</ta>
            <ta e="T46" id="Seg_2491" s="T45">np.h:E</ta>
            <ta e="T48" id="Seg_2492" s="T47">adv:Time</ta>
            <ta e="T49" id="Seg_2493" s="T48">pro.h:A</ta>
            <ta e="T50" id="Seg_2494" s="T49">n:Time</ta>
            <ta e="T55" id="Seg_2495" s="T54">pro.h:A</ta>
            <ta e="T57" id="Seg_2496" s="T56">pro.h:A</ta>
            <ta e="T58" id="Seg_2497" s="T57">pro.h:R</ta>
            <ta e="T59" id="Seg_2498" s="T58">np:Th</ta>
            <ta e="T61" id="Seg_2499" s="T60">0.3.h:A</ta>
            <ta e="T62" id="Seg_2500" s="T61">np:Th</ta>
            <ta e="T64" id="Seg_2501" s="T63">np:Ins</ta>
            <ta e="T65" id="Seg_2502" s="T64">pro.h:A</ta>
            <ta e="T68" id="Seg_2503" s="T67">0.3.h:A</ta>
            <ta e="T69" id="Seg_2504" s="T68">np:Th</ta>
            <ta e="T73" id="Seg_2505" s="T72">pro.h:A</ta>
            <ta e="T76" id="Seg_2506" s="T75">np.h:A</ta>
            <ta e="T77" id="Seg_2507" s="T76">pro:G</ta>
            <ta e="T82" id="Seg_2508" s="T81">adv:Time</ta>
            <ta e="T83" id="Seg_2509" s="T82">0.2.h:A</ta>
            <ta e="T84" id="Seg_2510" s="T83">adv:Time</ta>
            <ta e="T87" id="Seg_2511" s="T86">pro:Th</ta>
            <ta e="T90" id="Seg_2512" s="T89">pro.h:A</ta>
            <ta e="T92" id="Seg_2513" s="T91">0.3.h:A</ta>
            <ta e="T94" id="Seg_2514" s="T93">0.3.h:Th</ta>
            <ta e="T96" id="Seg_2515" s="T95">pro.h:Poss</ta>
            <ta e="T98" id="Seg_2516" s="T97">0.3.h:A</ta>
            <ta e="T101" id="Seg_2517" s="T100">0.3.h:A</ta>
            <ta e="T102" id="Seg_2518" s="T101">pro.h:E</ta>
            <ta e="T103" id="Seg_2519" s="T102">adv:Time</ta>
            <ta e="T107" id="Seg_2520" s="T106">np.h:E</ta>
            <ta e="T109" id="Seg_2521" s="T108">n:Time</ta>
            <ta e="T113" id="Seg_2522" s="T112">np:L</ta>
            <ta e="T114" id="Seg_2523" s="T113">np.h:E</ta>
            <ta e="T118" id="Seg_2524" s="T117">0.2.h:A</ta>
            <ta e="T120" id="Seg_2525" s="T119">np:Th</ta>
            <ta e="T124" id="Seg_2526" s="T123">0.2.h:A</ta>
            <ta e="T125" id="Seg_2527" s="T124">pro.h:A</ta>
            <ta e="T127" id="Seg_2528" s="T126">0.2.h:A</ta>
            <ta e="T128" id="Seg_2529" s="T127">pro.h:R</ta>
            <ta e="T130" id="Seg_2530" s="T129">pro.h:A</ta>
            <ta e="T132" id="Seg_2531" s="T131">np:Th</ta>
            <ta e="T133" id="Seg_2532" s="T132">0.3.h:A</ta>
            <ta e="T134" id="Seg_2533" s="T133">0.3.h:A</ta>
            <ta e="T135" id="Seg_2534" s="T134">pro.h:A</ta>
            <ta e="T137" id="Seg_2535" s="T136">np:Th</ta>
            <ta e="T138" id="Seg_2536" s="T137">0.3.h:A</ta>
            <ta e="T142" id="Seg_2537" s="T141">0.3.h:A</ta>
            <ta e="T145" id="Seg_2538" s="T144">0.3.h:E</ta>
            <ta e="T146" id="Seg_2539" s="T145">0.3.h:E</ta>
            <ta e="T147" id="Seg_2540" s="T146">adv:Time</ta>
            <ta e="T149" id="Seg_2541" s="T148">0.3.h:E</ta>
            <ta e="T151" id="Seg_2542" s="T150">np.h:A</ta>
            <ta e="T154" id="Seg_2543" s="T153">np.h:Th</ta>
            <ta e="T155" id="Seg_2544" s="T154">0.3.h:A</ta>
            <ta e="T156" id="Seg_2545" s="T155">np:G</ta>
            <ta e="T158" id="Seg_2546" s="T157">np.h:Poss</ta>
            <ta e="T159" id="Seg_2547" s="T158">np.h:Th</ta>
            <ta e="T161" id="Seg_2548" s="T160">np:G</ta>
            <ta e="T162" id="Seg_2549" s="T161">0.3.h:A</ta>
            <ta e="T163" id="Seg_2550" s="T162">adv:Time</ta>
            <ta e="T164" id="Seg_2551" s="T163">n:Time</ta>
            <ta e="T165" id="Seg_2552" s="T164">0.3.h:A</ta>
            <ta e="T167" id="Seg_2553" s="T166">pro.h:A</ta>
            <ta e="T168" id="Seg_2554" s="T167">adv:L</ta>
            <ta e="T170" id="Seg_2555" s="T169">pro.h:Poss</ta>
            <ta e="T172" id="Seg_2556" s="T171">np:G</ta>
            <ta e="T173" id="Seg_2557" s="T172">adv:Time</ta>
            <ta e="T175" id="Seg_2558" s="T174">pro.h:A</ta>
            <ta e="T180" id="Seg_2559" s="T179">0.2.h:Th</ta>
            <ta e="T181" id="Seg_2560" s="T180">adv:L</ta>
            <ta e="T182" id="Seg_2561" s="T181">0.2.h:E</ta>
            <ta e="T183" id="Seg_2562" s="T182">adv:Time</ta>
            <ta e="T184" id="Seg_2563" s="T183">np.h:Poss</ta>
            <ta e="T186" id="Seg_2564" s="T185">np.h:E</ta>
            <ta e="T191" id="Seg_2565" s="T190">pro.h:A</ta>
            <ta e="T197" id="Seg_2566" s="T196">0.3.h:Th</ta>
            <ta e="T201" id="Seg_2567" s="T199">adv:Time</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_2568" s="T0">v:pred</ta>
            <ta e="T3" id="Seg_2569" s="T2">np.h:S</ta>
            <ta e="T4" id="Seg_2570" s="T3">pro.h:S</ta>
            <ta e="T6" id="Seg_2571" s="T5">v:pred</ta>
            <ta e="T7" id="Seg_2572" s="T6">np.h:O</ta>
            <ta e="T8" id="Seg_2573" s="T7">v:pred 0.3.h:S</ta>
            <ta e="T9" id="Seg_2574" s="T8">np.h:S</ta>
            <ta e="T10" id="Seg_2575" s="T9">v:pred</ta>
            <ta e="T11" id="Seg_2576" s="T10">pro.h:S</ta>
            <ta e="T13" id="Seg_2577" s="T12">v:pred</ta>
            <ta e="T14" id="Seg_2578" s="T13">v:pred 0.3.h:S</ta>
            <ta e="T15" id="Seg_2579" s="T14">pro.h:O</ta>
            <ta e="T18" id="Seg_2580" s="T17">np.h:S</ta>
            <ta e="T19" id="Seg_2581" s="T18">v:pred</ta>
            <ta e="T22" id="Seg_2582" s="T21">v:pred</ta>
            <ta e="T24" id="Seg_2583" s="T23">np.h:S</ta>
            <ta e="T25" id="Seg_2584" s="T24">pro.h:S</ta>
            <ta e="T26" id="Seg_2585" s="T25">pro.h:O</ta>
            <ta e="T27" id="Seg_2586" s="T26">v:pred</ta>
            <ta e="T29" id="Seg_2587" s="T28">v:pred 0.3.h:S</ta>
            <ta e="T30" id="Seg_2588" s="T29">np:O</ta>
            <ta e="T31" id="Seg_2589" s="T30">v:pred 0.3.h:S</ta>
            <ta e="T32" id="Seg_2590" s="T31">pro.h:S</ta>
            <ta e="T33" id="Seg_2591" s="T32">pro.h:O</ta>
            <ta e="T34" id="Seg_2592" s="T33">v:pred</ta>
            <ta e="T37" id="Seg_2593" s="T36">v:pred 0.3.h:S</ta>
            <ta e="T38" id="Seg_2594" s="T37">v:pred 0.3.h:S</ta>
            <ta e="T41" id="Seg_2595" s="T40">np.h:O</ta>
            <ta e="T42" id="Seg_2596" s="T41">v:pred 0.3.h:S</ta>
            <ta e="T46" id="Seg_2597" s="T45">np.h:S</ta>
            <ta e="T47" id="Seg_2598" s="T46">v:pred</ta>
            <ta e="T49" id="Seg_2599" s="T48">pro.h:S</ta>
            <ta e="T53" id="Seg_2600" s="T52">v:pred</ta>
            <ta e="T55" id="Seg_2601" s="T54">pro.h:S</ta>
            <ta e="T56" id="Seg_2602" s="T55">v:pred</ta>
            <ta e="T57" id="Seg_2603" s="T56">pro.h:S</ta>
            <ta e="T59" id="Seg_2604" s="T58">np:O</ta>
            <ta e="T60" id="Seg_2605" s="T59">v:pred</ta>
            <ta e="T61" id="Seg_2606" s="T60">v:pred 0.3.h:S</ta>
            <ta e="T65" id="Seg_2607" s="T64">pro.h:S</ta>
            <ta e="T66" id="Seg_2608" s="T65">v:pred</ta>
            <ta e="T67" id="Seg_2609" s="T66">s:purp</ta>
            <ta e="T68" id="Seg_2610" s="T67">v:pred 0.3.h:S</ta>
            <ta e="T69" id="Seg_2611" s="T68">np:O</ta>
            <ta e="T70" id="Seg_2612" s="T69">v:pred</ta>
            <ta e="T73" id="Seg_2613" s="T72">pro.h:S</ta>
            <ta e="T74" id="Seg_2614" s="T73">v:pred</ta>
            <ta e="T75" id="Seg_2615" s="T74">v:pred</ta>
            <ta e="T76" id="Seg_2616" s="T75">np.h:S</ta>
            <ta e="T79" id="Seg_2617" s="T78">v:pred</ta>
            <ta e="T83" id="Seg_2618" s="T82">v:pred 0.2.h:S</ta>
            <ta e="T87" id="Seg_2619" s="T86">pro:O</ta>
            <ta e="T89" id="Seg_2620" s="T88">v:pred</ta>
            <ta e="T90" id="Seg_2621" s="T89">pro.h:S</ta>
            <ta e="T91" id="Seg_2622" s="T90">v:pred</ta>
            <ta e="T92" id="Seg_2623" s="T91">v:pred 0.3.h:S</ta>
            <ta e="T93" id="Seg_2624" s="T92">adj:pred</ta>
            <ta e="T94" id="Seg_2625" s="T93">cop 0.3.h:S</ta>
            <ta e="T97" id="Seg_2626" s="T96">ptcl.neg</ta>
            <ta e="T98" id="Seg_2627" s="T97">v:pred 0.3.h:S</ta>
            <ta e="T100" id="Seg_2628" s="T99">ptcl:pred</ta>
            <ta e="T101" id="Seg_2629" s="T100">v:pred 0.3.h:S</ta>
            <ta e="T102" id="Seg_2630" s="T101">pro.h:O</ta>
            <ta e="T104" id="Seg_2631" s="T103">v:pred</ta>
            <ta e="T107" id="Seg_2632" s="T106">np.h:S</ta>
            <ta e="T109" id="Seg_2633" s="T108">np:S</ta>
            <ta e="T110" id="Seg_2634" s="T109">v:pred</ta>
            <ta e="T114" id="Seg_2635" s="T113">np.h:S</ta>
            <ta e="T115" id="Seg_2636" s="T114">v:pred</ta>
            <ta e="T117" id="Seg_2637" s="T116">ptcl.neg</ta>
            <ta e="T118" id="Seg_2638" s="T117">v:pred 0.2.h:S</ta>
            <ta e="T120" id="Seg_2639" s="T119">np:S</ta>
            <ta e="T121" id="Seg_2640" s="T120">ptcl.neg</ta>
            <ta e="T122" id="Seg_2641" s="T121">v:pred</ta>
            <ta e="T124" id="Seg_2642" s="T123">v:pred 0.2.h:S</ta>
            <ta e="T125" id="Seg_2643" s="T124">pro.h:S</ta>
            <ta e="T126" id="Seg_2644" s="T125">v:pred</ta>
            <ta e="T127" id="Seg_2645" s="T126">v:pred 0.2.h:S</ta>
            <ta e="T130" id="Seg_2646" s="T129">pro.h:S</ta>
            <ta e="T131" id="Seg_2647" s="T130">v:pred</ta>
            <ta e="T132" id="Seg_2648" s="T131">np:O</ta>
            <ta e="T133" id="Seg_2649" s="T132">v:pred 0.3.h:S</ta>
            <ta e="T134" id="Seg_2650" s="T133">v:pred 0.3.h:S</ta>
            <ta e="T135" id="Seg_2651" s="T134">pro.h:S</ta>
            <ta e="T136" id="Seg_2652" s="T135">v:pred</ta>
            <ta e="T137" id="Seg_2653" s="T136">np:O</ta>
            <ta e="T138" id="Seg_2654" s="T137">v:pred 0.3.h:S</ta>
            <ta e="T139" id="Seg_2655" s="T138">conv:pred</ta>
            <ta e="T141" id="Seg_2656" s="T140">ptcl.neg</ta>
            <ta e="T142" id="Seg_2657" s="T141">v:pred 0.3.h:S</ta>
            <ta e="T145" id="Seg_2658" s="T144">v:pred 0.3.h:S</ta>
            <ta e="T146" id="Seg_2659" s="T145">v:pred 0.3.h:S</ta>
            <ta e="T149" id="Seg_2660" s="T148">v:pred 0.3.h:S</ta>
            <ta e="T151" id="Seg_2661" s="T150">np.h:S</ta>
            <ta e="T152" id="Seg_2662" s="T151">v:pred</ta>
            <ta e="T154" id="Seg_2663" s="T153">np.h:O</ta>
            <ta e="T155" id="Seg_2664" s="T154">v:pred 0.3.h:S</ta>
            <ta e="T159" id="Seg_2665" s="T158">np.h:O</ta>
            <ta e="T162" id="Seg_2666" s="T161">v:pred 0.3.h:S</ta>
            <ta e="T165" id="Seg_2667" s="T164">v:pred 0.3.h:S</ta>
            <ta e="T167" id="Seg_2668" s="T166">pro.h:S</ta>
            <ta e="T169" id="Seg_2669" s="T168">v:pred</ta>
            <ta e="T174" id="Seg_2670" s="T173">v:pred </ta>
            <ta e="T175" id="Seg_2671" s="T174">pro.h:S</ta>
            <ta e="T180" id="Seg_2672" s="T179">v:pred 0.2.h:S</ta>
            <ta e="T182" id="Seg_2673" s="T181">v:pred 0.2.h:S</ta>
            <ta e="T186" id="Seg_2674" s="T185">np.h:S</ta>
            <ta e="T189" id="Seg_2675" s="T188">v:pred</ta>
            <ta e="T191" id="Seg_2676" s="T190">pro.h:S</ta>
            <ta e="T192" id="Seg_2677" s="T191">ptcl.neg</ta>
            <ta e="T194" id="Seg_2678" s="T193">adj:pred</ta>
            <ta e="T195" id="Seg_2679" s="T194">cop</ta>
            <ta e="T196" id="Seg_2680" s="T195">adj:pred</ta>
            <ta e="T197" id="Seg_2681" s="T196">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T9" id="Seg_2682" s="T8">RUS:cult</ta>
            <ta e="T12" id="Seg_2683" s="T11">TURK:disc</ta>
            <ta e="T24" id="Seg_2684" s="T23">RUS:cult</ta>
            <ta e="T39" id="Seg_2685" s="T38">RUS:cult</ta>
            <ta e="T40" id="Seg_2686" s="T39">RUS:cult</ta>
            <ta e="T46" id="Seg_2687" s="T45">RUS:cult</ta>
            <ta e="T64" id="Seg_2688" s="T63">RUS:cult</ta>
            <ta e="T69" id="Seg_2689" s="T68">%TURK:cult</ta>
            <ta e="T76" id="Seg_2690" s="T75">RUS:cult</ta>
            <ta e="T85" id="Seg_2691" s="T84">RUS:gram</ta>
            <ta e="T86" id="Seg_2692" s="T85">RUS:gram</ta>
            <ta e="T93" id="Seg_2693" s="T92">TURK:core</ta>
            <ta e="T95" id="Seg_2694" s="T94">RUS:cult</ta>
            <ta e="T102" id="Seg_2695" s="T101">TURK:gram(INDEF)</ta>
            <ta e="T107" id="Seg_2696" s="T106">RUS:cult</ta>
            <ta e="T111" id="Seg_2697" s="T110">RUS:gram</ta>
            <ta e="T113" id="Seg_2698" s="T112">RUS:cult</ta>
            <ta e="T114" id="Seg_2699" s="T113">RUS:cult</ta>
            <ta e="T120" id="Seg_2700" s="T119">RUS:cult</ta>
            <ta e="T132" id="Seg_2701" s="T131">RUS:cult</ta>
            <ta e="T140" id="Seg_2702" s="T139">TURK:gram(INDEF)</ta>
            <ta e="T151" id="Seg_2703" s="T150">RUS:cult</ta>
            <ta e="T154" id="Seg_2704" s="T153">RUS:cult</ta>
            <ta e="T157" id="Seg_2705" s="T156">RUS:gram</ta>
            <ta e="T158" id="Seg_2706" s="T157">RUS:cult</ta>
            <ta e="T160" id="Seg_2707" s="T159">RUS:mod</ta>
            <ta e="T176" id="Seg_2708" s="T175">RUS:gram</ta>
            <ta e="T178" id="Seg_2709" s="T177">TURK:gram(INDEF)</ta>
            <ta e="T179" id="Seg_2710" s="T178">RUS:gram</ta>
            <ta e="T184" id="Seg_2711" s="T183">RUS:cult</ta>
            <ta e="T188" id="Seg_2712" s="T187">TURK:core</ta>
            <ta e="T190" id="Seg_2713" s="T189">RUS:gram</ta>
            <ta e="T196" id="Seg_2714" s="T195">TURK:core</ta>
            <ta e="T199" id="Seg_2715" s="T198">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_2716" s="T0">Жила сердитая женщина.</ta>
            <ta e="T8" id="Seg_2717" s="T3">Она всегда сердитая была, людей била.</ta>
            <ta e="T15" id="Seg_2718" s="T8">Староста придёт, она кричит, побьёт его.</ta>
            <ta e="T20" id="Seg_2719" s="T15">Очень сердитая, люди её боялись.</ta>
            <ta e="T24" id="Seg_2720" s="T20">Потом пришёл один солдат.</ta>
            <ta e="T27" id="Seg_2721" s="T24">«Я её проучу!»</ta>
            <ta e="T30" id="Seg_2722" s="T27">Вечером запряг лошадей.</ta>
            <ta e="T31" id="Seg_2723" s="T30">Приехал.</ta>
            <ta e="T37" id="Seg_2724" s="T31">Взял он её [спящую] и положил на лошадей.</ta>
            <ta e="T39" id="Seg_2725" s="T37">Привёз [её] к сапожнику.</ta>
            <ta e="T47" id="Seg_2726" s="T39">А жену сапожника привёз туда, где эта барыня жила.</ta>
            <ta e="T53" id="Seg_2727" s="T47">Потом утром она [жена сапожника] проснулась.</ta>
            <ta e="T56" id="Seg_2728" s="T53">«Где я сплю?»</ta>
            <ta e="T60" id="Seg_2729" s="T56">Ей принесли воды.</ta>
            <ta e="T66" id="Seg_2730" s="T60">Принесли вытереть лицо, она полотенцем вытерлась.</ta>
            <ta e="T68" id="Seg_2731" s="T66">Дали [ей] поесть.</ta>
            <ta e="T72" id="Seg_2732" s="T68">Самовар(?) поставили, чай [подали].</ta>
            <ta e="T74" id="Seg_2733" s="T72">Она ест.</ta>
            <ta e="T77" id="Seg_2734" s="T74">Пришёл к ней староста.</ta>
            <ta e="T79" id="Seg_2735" s="T77">«Что [мне] делать?»</ta>
            <ta e="T89" id="Seg_2736" s="T79">«Что вчера делали, то и сегодня делайте».</ta>
            <ta e="T92" id="Seg_2737" s="T89">Он пошёл, [люди на него] смотрят.</ta>
            <ta e="T96" id="Seg_2738" s="T92">«Барыня наша доброй стала.</ta>
            <ta e="T98" id="Seg_2739" s="T96">Не ругается.</ta>
            <ta e="T102" id="Seg_2740" s="T98">И не бьёт никого.»</ta>
            <ta e="T107" id="Seg_2741" s="T102">Потом солдат об этом услышал.</ta>
            <ta e="T110" id="Seg_2742" s="T107">Два месяца прошло.</ta>
            <ta e="T115" id="Seg_2743" s="T110">А у того кузнеца барыня живёт.</ta>
            <ta e="T118" id="Seg_2744" s="T115">«Ты почему не встаёшь?</ta>
            <ta e="T122" id="Seg_2745" s="T118">Почему печь не топится?</ta>
            <ta e="T124" id="Seg_2746" s="T122">Иди за водой!»</ta>
            <ta e="T126" id="Seg_2747" s="T124">Она говорит:</ta>
            <ta e="T129" id="Seg_2748" s="T126">«Принеси мне умыться».</ta>
            <ta e="T134" id="Seg_2749" s="T129">Он взял ремень, бил [её], бил.</ta>
            <ta e="T139" id="Seg_2750" s="T134">Она взяла ведро, пошла за водой.</ta>
            <ta e="T144" id="Seg_2751" s="T139">Ничего делать не умела.</ta>
            <ta e="T146" id="Seg_2752" s="T144">Жила там, жила.</ta>
            <ta e="T149" id="Seg_2753" s="T146">Потом опять [однажды] они заснули.</ta>
            <ta e="T152" id="Seg_2754" s="T149">Тот солдат пришёл.</ta>
            <ta e="T156" id="Seg_2755" s="T152">Эту барыню домой привёз.</ta>
            <ta e="T162" id="Seg_2756" s="T156">И жену кузнеца тоже домой привёз.</ta>
            <ta e="T165" id="Seg_2757" s="T162">Потом утром она [барыня] встала.</ta>
            <ta e="T172" id="Seg_2758" s="T165">«Как я сюда вернулась, в свой дом?»</ta>
            <ta e="T175" id="Seg_2759" s="T172">Потом спросила.</ta>
            <ta e="T182" id="Seg_2760" s="T175">«Да ты нигде и не была, здесь жила».</ta>
            <ta e="T189" id="Seg_2761" s="T182">А жена кузнеца хорошо живёт.</ta>
            <ta e="T195" id="Seg_2762" s="T189">А она [барыня] не стала сердитой.</ta>
            <ta e="T199" id="Seg_2763" s="T195">Доброй стала, эта барыня.</ta>
            <ta e="T201" id="Seg_2764" s="T199">Потом…</ta>
            <ta e="T202" id="Seg_2765" s="T201">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_2766" s="T0">There was an angry woman.</ta>
            <ta e="T8" id="Seg_2767" s="T3">She was always angry, she beat men.</ta>
            <ta e="T15" id="Seg_2768" s="T8">If the bailiff comes, she shouts, she will beat him.</ta>
            <ta e="T20" id="Seg_2769" s="T15">Very angry, people are afraid of her.</ta>
            <ta e="T24" id="Seg_2770" s="T20">Then one soldier came.</ta>
            <ta e="T27" id="Seg_2771" s="T24">“I will teach her!”</ta>
            <ta e="T30" id="Seg_2772" s="T27">In the evening he harnessed his horses.</ta>
            <ta e="T31" id="Seg_2773" s="T30">He came.</ta>
            <ta e="T37" id="Seg_2774" s="T31">He took her [sleeping] and put on the horses.</ta>
            <ta e="T39" id="Seg_2775" s="T37">He brought [her] to the shoemaker.</ta>
            <ta e="T47" id="Seg_2776" s="T39">[And] he brought the shoemakers' wife where that lady lived.</ta>
            <ta e="T53" id="Seg_2777" s="T47">Then in the morning she [the shoemaker’s wife] got up.</ta>
            <ta e="T56" id="Seg_2778" s="T53">"Where am I sleeping?"</ta>
            <ta e="T60" id="Seg_2779" s="T56">They brought her water.</ta>
            <ta e="T66" id="Seg_2780" s="T60">They brought [a towel] to rub [=wipe] her face, she wiped [herself] with a towel.</ta>
            <ta e="T68" id="Seg_2781" s="T66">They gave [something] to eat.</ta>
            <ta e="T72" id="Seg_2782" s="T68">They set a (kettle?), [served] tea.</ta>
            <ta e="T74" id="Seg_2783" s="T72">She is eating.</ta>
            <ta e="T77" id="Seg_2784" s="T74">The village elder came to her.</ta>
            <ta e="T79" id="Seg_2785" s="T77">"What should [I] do?"</ta>
            <ta e="T89" id="Seg_2786" s="T79">"What you did yesterday, do it today, too".</ta>
            <ta e="T92" id="Seg_2787" s="T89">He went, they are looking [at him].</ta>
            <ta e="T96" id="Seg_2788" s="T92">"She became good, our mistress.</ta>
            <ta e="T98" id="Seg_2789" s="T96">She does not swear.</ta>
            <ta e="T102" id="Seg_2790" s="T98">And she does not beat anybody."</ta>
            <ta e="T107" id="Seg_2791" s="T102">Then the soldier heard [about it].</ta>
            <ta e="T110" id="Seg_2792" s="T107">Two months went by.</ta>
            <ta e="T115" id="Seg_2793" s="T110">But the mistress lived at the blacksmith.</ta>
            <ta e="T118" id="Seg_2794" s="T115">"Why don't you get up?</ta>
            <ta e="T122" id="Seg_2795" s="T118">Why is there no fire in the oven?</ta>
            <ta e="T124" id="Seg_2796" s="T122">Go fetch water!"</ta>
            <ta e="T126" id="Seg_2797" s="T124">She says:</ta>
            <ta e="T129" id="Seg_2798" s="T126">"Bring me [water] to wash."</ta>
            <ta e="T134" id="Seg_2799" s="T129">He took a belt and beat [her], beat [her].</ta>
            <ta e="T139" id="Seg_2800" s="T134">She took the bucket, went to fetch water.</ta>
            <ta e="T144" id="Seg_2801" s="T139">She could not do anything.</ta>
            <ta e="T146" id="Seg_2802" s="T144">She lived, she lived [there].</ta>
            <ta e="T149" id="Seg_2803" s="T146">Then [once] they went to sleep again.</ta>
            <ta e="T152" id="Seg_2804" s="T149">This soldier came.</ta>
            <ta e="T156" id="Seg_2805" s="T152">He brought the lady home.</ta>
            <ta e="T162" id="Seg_2806" s="T156">And he brought the blacksmith's wife home, too.</ta>
            <ta e="T165" id="Seg_2807" s="T162">Then in the morning she [the lady] got up.</ta>
            <ta e="T172" id="Seg_2808" s="T165">"How did I come here to my own home?"</ta>
            <ta e="T175" id="Seg_2809" s="T172">Then she asks.</ta>
            <ta e="T182" id="Seg_2810" s="T175">"But you haven't been anywhere, you lived here."</ta>
            <ta e="T189" id="Seg_2811" s="T182">And the blacksmith's wife is living well.</ta>
            <ta e="T195" id="Seg_2812" s="T189">And she did not become angry.</ta>
            <ta e="T199" id="Seg_2813" s="T195">She became good, the mistress.</ta>
            <ta e="T201" id="Seg_2814" s="T199">Then…</ta>
            <ta e="T202" id="Seg_2815" s="T201">Enough [Stop the tape]!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_2816" s="T0">Es gab eine wütende Frau.</ta>
            <ta e="T8" id="Seg_2817" s="T3">Sie war immer wütend, sie schlug Männer.</ta>
            <ta e="T15" id="Seg_2818" s="T8">Wenn der Gerichtsvollzieher kommt, schreit sie, sie wird ihn schlagen.</ta>
            <ta e="T20" id="Seg_2819" s="T15">Sehr wütend, Menschen haben Angst vor ihr.</ta>
            <ta e="T24" id="Seg_2820" s="T20">Dann kam ein Soldat.</ta>
            <ta e="T27" id="Seg_2821" s="T24">"Ich werde sie lehren!"</ta>
            <ta e="T30" id="Seg_2822" s="T27">Am Abend zäumte er sein Pferd.</ta>
            <ta e="T31" id="Seg_2823" s="T30">Er kam.</ta>
            <ta e="T37" id="Seg_2824" s="T31">Er nahm sie [schlafend] und setzte sie aufs PFerd.</ta>
            <ta e="T39" id="Seg_2825" s="T37">Er brachte [sie] zum Schuhmacher.</ta>
            <ta e="T47" id="Seg_2826" s="T39">[Und] er brachte die Frau des Schuhmacher dorthin, wo die Frau lebte.</ta>
            <ta e="T53" id="Seg_2827" s="T47">Dann am Morgen wachte sie [die Frau des Schuhmachers] auf.</ta>
            <ta e="T56" id="Seg_2828" s="T53">"Wo schlafe ich?"</ta>
            <ta e="T60" id="Seg_2829" s="T56">Sie brachten ihr Wasser.</ta>
            <ta e="T66" id="Seg_2830" s="T60">Sie brachten [ein Handtuch], ihr Gesicht zu trocknen, sie trocknete sich mit einem Handtuch.</ta>
            <ta e="T68" id="Seg_2831" s="T66">Sie haben ihr [etwas] zu essen.</ta>
            <ta e="T72" id="Seg_2832" s="T68">Sie setzen einen Kessel auf, [servierten] Tee.</ta>
            <ta e="T74" id="Seg_2833" s="T72">Sie isst.</ta>
            <ta e="T77" id="Seg_2834" s="T74">Der Dorfälteste kam zu ihr. </ta>
            <ta e="T79" id="Seg_2835" s="T77">"Was soll [ich] machen?"</ta>
            <ta e="T89" id="Seg_2836" s="T79">"Was du gestern gemacht hast, das mache auch heute."</ta>
            <ta e="T92" id="Seg_2837" s="T89">Er ging, sie sahen [ihn] an.</ta>
            <ta e="T96" id="Seg_2838" s="T92">"Sie wurde gut, unsere Frau.</ta>
            <ta e="T98" id="Seg_2839" s="T96">Sie flucht nicht.</ta>
            <ta e="T102" id="Seg_2840" s="T98">Und sie schlägt niemanden."</ta>
            <ta e="T107" id="Seg_2841" s="T102">Dann hörte der Soldat [davon].</ta>
            <ta e="T110" id="Seg_2842" s="T107">Zwei Monate vergingen.</ta>
            <ta e="T115" id="Seg_2843" s="T110">Aber unsere Frau lebte beim Schmied.</ta>
            <ta e="T118" id="Seg_2844" s="T115">"Warum stehst du nicht auf?</ta>
            <ta e="T122" id="Seg_2845" s="T118">Warum ist kein Feuer im Ofen an?</ta>
            <ta e="T124" id="Seg_2846" s="T122">Geh Wasser holen!"</ta>
            <ta e="T126" id="Seg_2847" s="T124">Sie sagt: </ta>
            <ta e="T129" id="Seg_2848" s="T126">"BRing mir [Wasser] um mich zu waschen."</ta>
            <ta e="T134" id="Seg_2849" s="T129">Er nahm einen Gürtel und schlug [sie], schlug [sie].</ta>
            <ta e="T139" id="Seg_2850" s="T134">Sie nahm den Eimer, ging Wasser holen.</ta>
            <ta e="T144" id="Seg_2851" s="T139">Sie konnte nichts machen.</ta>
            <ta e="T146" id="Seg_2852" s="T144">Sie lebte, sie lebte [dort].</ta>
            <ta e="T149" id="Seg_2853" s="T146">Dann [ein mal] gingen sie wieder schlafen.</ta>
            <ta e="T152" id="Seg_2854" s="T149">Dieser Soldat kam.</ta>
            <ta e="T156" id="Seg_2855" s="T152">Er brachte die Frau nach Hause.</ta>
            <ta e="T162" id="Seg_2856" s="T156">Und er brachte auch die Frau des Schmieds nach Hause.</ta>
            <ta e="T165" id="Seg_2857" s="T162">Dann am morgen stand sie [die Frau] auf.</ta>
            <ta e="T172" id="Seg_2858" s="T165">"Wie bin ich in mein eigenes Zuhause gekommen?"</ta>
            <ta e="T175" id="Seg_2859" s="T172">Dann fragte sie.</ta>
            <ta e="T182" id="Seg_2860" s="T175">"Aber du warst nirgends, du hast hier gelebt."</ta>
            <ta e="T189" id="Seg_2861" s="T182">Und die Frau des Schmieds lebt gut.</ta>
            <ta e="T195" id="Seg_2862" s="T189">Und sie wurde nicht wütend.</ta>
            <ta e="T199" id="Seg_2863" s="T195">Und sie wurde gut, die Frau.</ta>
            <ta e="T201" id="Seg_2864" s="T199">Dann…</ta>
            <ta e="T202" id="Seg_2865" s="T201">Genug [Halte die Aufnahme an]!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T3" id="Seg_2866" s="T0">[GVY:] A tale 'Angry lady' ("Сердитая барыня"); Russian text see e.g. www.hobobo.ru/catalog/skazka/serditaya-baryinya or hyaenidae.narod.ru/story3/149.html [AAV]: Tape SU0192</ta>
            <ta e="T15" id="Seg_2867" s="T8">[GVY:] Ru. староста 'bailiff'</ta>
            <ta e="T20" id="Seg_2868" s="T15">[GVY:] Note the pronounciation dĭʔi.</ta>
            <ta e="T39" id="Seg_2869" s="T37">[KlT:] Сапожник 'shoemaker’.</ta>
            <ta e="T72" id="Seg_2870" s="T68">[GVY:] Cf. Khakas xajna- 'boil'.</ta>
            <ta e="T77" id="Seg_2871" s="T74">[GVY:] Rus. староста</ta>
            <ta e="T115" id="Seg_2872" s="T110">Кузнец - Ru. 'blacksmith’ LOC.</ta>
            <ta e="T175" id="Seg_2873" s="T172">[GVY:] lit. 'herself asks"</ta>
            <ta e="T201" id="Seg_2874" s="T199">[GVY:] This may be a part of the preceding sentence.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
