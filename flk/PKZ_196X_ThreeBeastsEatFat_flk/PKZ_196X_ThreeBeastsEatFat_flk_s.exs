<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDFD4E3ED9-F595-184A-2D05-F704EE951F8E">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_ThreeBeastsEatFat_flk.wav" />
         <referenced-file url="PKZ_196X_ThreeBeastsEatFat_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_ThreeBeastsEatFat_flk\PKZ_196X_ThreeBeastsEatFat_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">133</ud-information>
            <ud-information attribute-name="# HIAT:w">92</ud-information>
            <ud-information attribute-name="# e">90</ud-information>
            <ud-information attribute-name="# HIAT:u">16</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T2704" time="0.067" type="appl" />
         <tli id="T2705" time="1.198" type="appl" />
         <tli id="T2706" time="2.328" type="appl" />
         <tli id="T2707" time="3.459" type="appl" />
         <tli id="T2708" time="4.59" type="appl" />
         <tli id="T2709" time="5.721" type="appl" />
         <tli id="T2710" time="6.851" type="appl" />
         <tli id="T2711" time="7.982" type="appl" />
         <tli id="T2712" time="9.074" type="appl" />
         <tli id="T2713" time="10.044" type="appl" />
         <tli id="T2714" time="11.014" type="appl" />
         <tli id="T2715" time="13.05889428779215" />
         <tli id="T2716" time="13.846" type="appl" />
         <tli id="T2717" time="14.616" type="appl" />
         <tli id="T2718" time="16.14529962482521" />
         <tli id="T2719" time="16.689" type="appl" />
         <tli id="T2720" time="17.318" type="appl" />
         <tli id="T2721" time="17.948" type="appl" />
         <tli id="T2722" time="18.577" type="appl" />
         <tli id="T2723" time="19.29836598425639" />
         <tli id="T2724" time="20.049" type="appl" />
         <tli id="T2725" time="20.818" type="appl" />
         <tli id="T2726" time="21.588" type="appl" />
         <tli id="T2727" time="22.357" type="appl" />
         <tli id="T2728" time="23.126" type="appl" />
         <tli id="T2729" time="24.337939277554437" />
         <tli id="T2730" time="24.884" type="appl" />
         <tli id="T2731" time="25.4" type="appl" />
         <tli id="T2732" time="25.916" type="appl" />
         <tli id="T2733" time="26.432" type="appl" />
         <tli id="T2734" time="26.948" type="appl" />
         <tli id="T2735" time="27.624327681781864" />
         <tli id="T2736" time="28.292" type="appl" />
         <tli id="T2737" time="28.787" type="appl" />
         <tli id="T2738" time="29.283" type="appl" />
         <tli id="T2739" time="29.779" type="appl" />
         <tli id="T2740" time="30.275" type="appl" />
         <tli id="T2741" time="30.77" type="appl" />
         <tli id="T2742" time="31.266" type="appl" />
         <tli id="T2743" time="31.762" type="appl" />
         <tli id="T2744" time="32.258" type="appl" />
         <tli id="T2745" time="32.753" type="appl" />
         <tli id="T2746" time="33.249" type="appl" />
         <tli id="T2747" time="34.195" type="appl" />
         <tli id="T2748" time="35.25034864412705" />
         <tli id="T2749" time="36.128" type="appl" />
         <tli id="T2750" time="36.774" type="appl" />
         <tli id="T2751" time="37.421" type="appl" />
         <tli id="T2752" time="38.068" type="appl" />
         <tli id="T2753" time="38.714" type="appl" />
         <tli id="T2754" time="39.361" type="appl" />
         <tli id="T2755" time="40.007" type="appl" />
         <tli id="T2756" time="40.976530468125745" />
         <tli id="T2757" time="41.572" type="appl" />
         <tli id="T2758" time="42.115" type="appl" />
         <tli id="T2759" time="42.658" type="appl" />
         <tli id="T2760" time="43.201" type="appl" />
         <tli id="T2761" time="43.95300045205839" />
         <tli id="T2762" time="44.7383343475226" />
         <tli id="T2763" time="45.48" type="appl" />
         <tli id="T2764" time="46.05" type="appl" />
         <tli id="T2765" time="46.62" type="appl" />
         <tli id="T2766" time="47.19" type="appl" />
         <tli id="T2767" time="47.535975072100975" />
         <tli id="T2768" time="48.613" type="appl" />
         <tli id="T2769" time="49.465" type="appl" />
         <tli id="T2770" time="50.318" type="appl" />
         <tli id="T2771" time="51.171" type="appl" />
         <tli id="T2772" time="52.024" type="appl" />
         <tli id="T2773" time="52.876" type="appl" />
         <tli id="T2774" time="54.46205529926588" />
         <tli id="T2775" time="55.111" type="appl" />
         <tli id="T2776" time="55.627" type="appl" />
         <tli id="T2777" time="56.143" type="appl" />
         <tli id="T2778" time="56.658" type="appl" />
         <tli id="T2779" time="57.174" type="appl" />
         <tli id="T2780" time="57.69" type="appl" />
         <tli id="T2781" time="58.206" type="appl" />
         <tli id="T2782" time="59.248" type="appl" />
         <tli id="T2783" time="60.228" type="appl" />
         <tli id="T2784" time="61.207" type="appl" />
         <tli id="T2785" time="62.187" type="appl" />
         <tli id="T2786" time="62.693" type="appl" />
         <tli id="T2787" time="63.199" type="appl" />
         <tli id="T2788" time="63.705" type="appl" />
         <tli id="T2789" time="64.211" type="appl" />
         <tli id="T2790" time="64.717" type="appl" />
         <tli id="T2791" time="65.223" type="appl" />
         <tli id="T2792" time="65.729" type="appl" />
         <tli id="T2793" time="66.235" type="appl" />
         <tli id="T2794" time="66.741" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T2731" start="T2730">
            <tli id="T2730.tx.1" />
         </timeline-fork>
         <timeline-fork end="T2732" start="T2731">
            <tli id="T2731.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T2794" id="Seg_0" n="sc" s="T2704">
               <ts e="T2711" id="Seg_2" n="HIAT:u" s="T2704">
                  <ts e="T2705" id="Seg_4" n="HIAT:w" s="T2704">Amnobiʔi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2706" id="Seg_7" n="HIAT:w" s="T2705">nagurgöʔ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2707" id="Seg_10" n="HIAT:w" s="T2706">urgaːba</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2708" id="Seg_13" n="HIAT:w" s="T2707">i</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2709" id="Seg_16" n="HIAT:w" s="T2708">volk</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2710" id="Seg_19" n="HIAT:w" s="T2709">i</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2711" id="Seg_22" n="HIAT:w" s="T2710">lʼisa</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2715" id="Seg_26" n="HIAT:u" s="T2711">
                  <ts e="T2712" id="Seg_28" n="HIAT:w" s="T2711">Dĭgəttə</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2713" id="Seg_31" n="HIAT:w" s="T2712">kambiʔi</ts>
                  <nts id="Seg_32" n="HIAT:ip">,</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2714" id="Seg_35" n="HIAT:w" s="T2713">ĭmbi-nʼibudʼ</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2715" id="Seg_38" n="HIAT:w" s="T2714">dʼabəsʼtə</ts>
                  <nts id="Seg_39" n="HIAT:ip">.</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2718" id="Seg_42" n="HIAT:u" s="T2715">
                  <ts e="T2716" id="Seg_44" n="HIAT:w" s="T2715">Dĭ</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2717" id="Seg_47" n="HIAT:w" s="T2716">volk</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2718" id="Seg_50" n="HIAT:w" s="T2717">kambi</ts>
                  <nts id="Seg_51" n="HIAT:ip">.</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2723" id="Seg_54" n="HIAT:u" s="T2718">
                  <ts e="T2719" id="Seg_56" n="HIAT:w" s="T2718">I</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2720" id="Seg_59" n="HIAT:w" s="T2719">ular</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2721" id="Seg_62" n="HIAT:w" s="T2720">tojirluʔpi</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2722" id="Seg_65" n="HIAT:w" s="T2721">i</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2723" id="Seg_68" n="HIAT:w" s="T2722">deʔpi</ts>
                  <nts id="Seg_69" n="HIAT:ip">.</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2729" id="Seg_72" n="HIAT:u" s="T2723">
                  <ts e="T2724" id="Seg_74" n="HIAT:w" s="T2723">Dĭgəttə</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2725" id="Seg_77" n="HIAT:w" s="T2724">urgaːba</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2726" id="Seg_80" n="HIAT:w" s="T2725">i</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2727" id="Seg_83" n="HIAT:w" s="T2726">lʼisʼitsa</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2728" id="Seg_86" n="HIAT:w" s="T2727">kambiʔi</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_88" n="HIAT:ip">(</nts>
                  <ts e="T2729" id="Seg_90" n="HIAT:w" s="T2728">bădəsʼtə</ts>
                  <nts id="Seg_91" n="HIAT:ip">)</nts>
                  <nts id="Seg_92" n="HIAT:ip">.</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2735" id="Seg_95" n="HIAT:u" s="T2729">
                  <ts e="T2730" id="Seg_97" n="HIAT:w" s="T2729">Urgaːba</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_99" n="HIAT:ip">(</nts>
                  <ts e="T2730.tx.1" id="Seg_101" n="HIAT:w" s="T2730">m-</ts>
                  <nts id="Seg_102" n="HIAT:ip">)</nts>
                  <ts e="T2731" id="Seg_104" n="HIAT:w" s="T2730.tx.1">:</ts>
                  <nts id="Seg_105" n="HIAT:ip">"</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2731.tx.1" id="Seg_108" n="HIAT:w" s="T2731">Na</ts>
                  <nts id="Seg_109" n="HIAT:ip">_</nts>
                  <ts e="T2732" id="Seg_111" n="HIAT:w" s="T2731.tx.1">što</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2733" id="Seg_114" n="HIAT:w" s="T2732">tăn</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2734" id="Seg_117" n="HIAT:w" s="T2733">amnial</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2735" id="Seg_120" n="HIAT:w" s="T2734">sil</ts>
                  <nts id="Seg_121" n="HIAT:ip">?</nts>
                  <nts id="Seg_122" n="HIAT:ip">"</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2746" id="Seg_125" n="HIAT:u" s="T2735">
                  <ts e="T2736" id="Seg_127" n="HIAT:w" s="T2735">A</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2737" id="Seg_130" n="HIAT:w" s="T2736">dĭ</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2738" id="Seg_133" n="HIAT:w" s="T2737">măndə:</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_135" n="HIAT:ip">"</nts>
                  <nts id="Seg_136" n="HIAT:ip">(</nts>
                  <ts e="T2739" id="Seg_138" n="HIAT:w" s="T2738">Dĭm=</ts>
                  <nts id="Seg_139" n="HIAT:ip">)</nts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2740" id="Seg_142" n="HIAT:w" s="T2739">Kamən</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2741" id="Seg_145" n="HIAT:w" s="T2740">dĭ</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2742" id="Seg_148" n="HIAT:w" s="T2741">surarga</ts>
                  <nts id="Seg_149" n="HIAT:ip">,</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_151" n="HIAT:ip">(</nts>
                  <ts e="T2743" id="Seg_153" n="HIAT:w" s="T2742">ja-</ts>
                  <nts id="Seg_154" n="HIAT:ip">)</nts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2744" id="Seg_157" n="HIAT:w" s="T2743">tăn</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2745" id="Seg_160" n="HIAT:w" s="T2744">măna</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2746" id="Seg_163" n="HIAT:w" s="T2745">măndəraʔ</ts>
                  <nts id="Seg_164" n="HIAT:ip">"</nts>
                  <nts id="Seg_165" n="HIAT:ip">.</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2748" id="Seg_168" n="HIAT:u" s="T2746">
                  <ts e="T2747" id="Seg_170" n="HIAT:w" s="T2746">Dĭgəttə</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2748" id="Seg_173" n="HIAT:w" s="T2747">šobiʔi</ts>
                  <nts id="Seg_174" n="HIAT:ip">.</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2756" id="Seg_177" n="HIAT:u" s="T2748">
                  <ts e="T2749" id="Seg_179" n="HIAT:w" s="T2748">A</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2750" id="Seg_182" n="HIAT:w" s="T2749">volk</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2751" id="Seg_185" n="HIAT:w" s="T2750">măndə:</ts>
                  <nts id="Seg_186" n="HIAT:ip">"</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2752" id="Seg_189" n="HIAT:w" s="T2751">Gibər</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_191" n="HIAT:ip">(</nts>
                  <ts e="T2753" id="Seg_193" n="HIAT:w" s="T2752">si-</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2754" id="Seg_196" n="HIAT:w" s="T2753">si-</ts>
                  <nts id="Seg_197" n="HIAT:ip">)</nts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2755" id="Seg_200" n="HIAT:w" s="T2754">giraːmbi</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2756" id="Seg_203" n="HIAT:w" s="T2755">sil</ts>
                  <nts id="Seg_204" n="HIAT:ip">?</nts>
                  <nts id="Seg_205" n="HIAT:ip">"</nts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2760" id="Seg_208" n="HIAT:u" s="T2756">
                  <ts e="T2757" id="Seg_210" n="HIAT:w" s="T2756">A</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2758" id="Seg_213" n="HIAT:w" s="T2757">dĭ</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2759" id="Seg_216" n="HIAT:w" s="T2758">măndəria</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2760" id="Seg_219" n="HIAT:w" s="T2759">dĭʔnə</ts>
                  <nts id="Seg_220" n="HIAT:ip">.</nts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2762" id="Seg_223" n="HIAT:u" s="T2760">
                  <nts id="Seg_224" n="HIAT:ip">"</nts>
                  <ts e="T2761" id="Seg_226" n="HIAT:w" s="T2760">Ĭmbi</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2762" id="Seg_229" n="HIAT:w" s="T2761">măndərial</ts>
                  <nts id="Seg_230" n="HIAT:ip">?</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2767" id="Seg_233" n="HIAT:u" s="T2762">
                  <ts e="T2763" id="Seg_235" n="HIAT:w" s="T2762">Kak</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2764" id="Seg_238" n="HIAT:w" s="T2763">amnaʔbəl</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2765" id="Seg_241" n="HIAT:w" s="T2764">dăk</ts>
                  <nts id="Seg_242" n="HIAT:ip">,</nts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2766" id="Seg_245" n="HIAT:w" s="T2765">ej</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2767" id="Seg_248" n="HIAT:w" s="T2766">măndərbial</ts>
                  <nts id="Seg_249" n="HIAT:ip">"</nts>
                  <nts id="Seg_250" n="HIAT:ip">.</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2774" id="Seg_253" n="HIAT:u" s="T2767">
                  <ts e="T2768" id="Seg_255" n="HIAT:w" s="T2767">Dĭgəttə</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2769" id="Seg_258" n="HIAT:w" s="T2768">davaj</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2770" id="Seg_261" n="HIAT:w" s="T2769">dĭzeŋ</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2771" id="Seg_264" n="HIAT:w" s="T2770">urgaːba</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2772" id="Seg_267" n="HIAT:w" s="T2771">s</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2773" id="Seg_270" n="HIAT:w" s="T2772">volkəm</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2774" id="Seg_273" n="HIAT:w" s="T2773">dʼabərozittə</ts>
                  <nts id="Seg_274" n="HIAT:ip">.</nts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2781" id="Seg_277" n="HIAT:u" s="T2774">
                  <ts e="T2775" id="Seg_279" n="HIAT:w" s="T2774">A</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2776" id="Seg_282" n="HIAT:w" s="T2775">lʼisʼitsa</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2777" id="Seg_285" n="HIAT:w" s="T2776">uja</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2778" id="Seg_288" n="HIAT:w" s="T2777">ibi</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2779" id="Seg_291" n="HIAT:w" s="T2778">da</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2780" id="Seg_294" n="HIAT:w" s="T2779">kalla</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2781" id="Seg_297" n="HIAT:w" s="T2780">dʼürbi</ts>
                  <nts id="Seg_298" n="HIAT:ip">.</nts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2785" id="Seg_301" n="HIAT:u" s="T2781">
                  <ts e="T2782" id="Seg_303" n="HIAT:w" s="T2781">Dĭzeŋ</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2783" id="Seg_306" n="HIAT:w" s="T2782">nuʔməluʔpiʔi</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_308" n="HIAT:ip">(</nts>
                  <ts e="T2784" id="Seg_310" n="HIAT:w" s="T2783">d-</ts>
                  <nts id="Seg_311" n="HIAT:ip">)</nts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2785" id="Seg_314" n="HIAT:w" s="T2784">dĭziʔ</ts>
                  <nts id="Seg_315" n="HIAT:ip">.</nts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2794" id="Seg_318" n="HIAT:u" s="T2785">
                  <ts e="T2786" id="Seg_320" n="HIAT:w" s="T2785">Dĭ</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2787" id="Seg_323" n="HIAT:w" s="T2786">bar</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2788" id="Seg_326" n="HIAT:w" s="T2787">nuʔməluʔpi</ts>
                  <nts id="Seg_327" n="HIAT:ip">,</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_329" n="HIAT:ip">(</nts>
                  <ts e="T2789" id="Seg_331" n="HIAT:w" s="T2788">ej</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2790" id="Seg_334" n="HIAT:w" s="T2789">mol-</ts>
                  <nts id="Seg_335" n="HIAT:ip">)</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2791" id="Seg_338" n="HIAT:w" s="T2790">ej</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2792" id="Seg_341" n="HIAT:w" s="T2791">dʼaʔpiʔi</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2793" id="Seg_344" n="HIAT:w" s="T2792">dĭzeŋ</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2794" id="Seg_347" n="HIAT:w" s="T2793">dĭm</ts>
                  <nts id="Seg_348" n="HIAT:ip">.</nts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T2794" id="Seg_350" n="sc" s="T2704">
               <ts e="T2705" id="Seg_352" n="e" s="T2704">Amnobiʔi </ts>
               <ts e="T2706" id="Seg_354" n="e" s="T2705">nagurgöʔ </ts>
               <ts e="T2707" id="Seg_356" n="e" s="T2706">urgaːba </ts>
               <ts e="T2708" id="Seg_358" n="e" s="T2707">i </ts>
               <ts e="T2709" id="Seg_360" n="e" s="T2708">volk </ts>
               <ts e="T2710" id="Seg_362" n="e" s="T2709">i </ts>
               <ts e="T2711" id="Seg_364" n="e" s="T2710">lʼisa. </ts>
               <ts e="T2712" id="Seg_366" n="e" s="T2711">Dĭgəttə </ts>
               <ts e="T2713" id="Seg_368" n="e" s="T2712">kambiʔi, </ts>
               <ts e="T2714" id="Seg_370" n="e" s="T2713">ĭmbi-nʼibudʼ </ts>
               <ts e="T2715" id="Seg_372" n="e" s="T2714">dʼabəsʼtə. </ts>
               <ts e="T2716" id="Seg_374" n="e" s="T2715">Dĭ </ts>
               <ts e="T2717" id="Seg_376" n="e" s="T2716">volk </ts>
               <ts e="T2718" id="Seg_378" n="e" s="T2717">kambi. </ts>
               <ts e="T2719" id="Seg_380" n="e" s="T2718">I </ts>
               <ts e="T2720" id="Seg_382" n="e" s="T2719">ular </ts>
               <ts e="T2721" id="Seg_384" n="e" s="T2720">tojirluʔpi </ts>
               <ts e="T2722" id="Seg_386" n="e" s="T2721">i </ts>
               <ts e="T2723" id="Seg_388" n="e" s="T2722">deʔpi. </ts>
               <ts e="T2724" id="Seg_390" n="e" s="T2723">Dĭgəttə </ts>
               <ts e="T2725" id="Seg_392" n="e" s="T2724">urgaːba </ts>
               <ts e="T2726" id="Seg_394" n="e" s="T2725">i </ts>
               <ts e="T2727" id="Seg_396" n="e" s="T2726">lʼisʼitsa </ts>
               <ts e="T2728" id="Seg_398" n="e" s="T2727">kambiʔi </ts>
               <ts e="T2729" id="Seg_400" n="e" s="T2728">(bădəsʼtə). </ts>
               <ts e="T2730" id="Seg_402" n="e" s="T2729">Urgaːba </ts>
               <ts e="T2731" id="Seg_404" n="e" s="T2730">(m-):" </ts>
               <ts e="T2732" id="Seg_406" n="e" s="T2731">Na_što </ts>
               <ts e="T2733" id="Seg_408" n="e" s="T2732">tăn </ts>
               <ts e="T2734" id="Seg_410" n="e" s="T2733">amnial </ts>
               <ts e="T2735" id="Seg_412" n="e" s="T2734">sil?" </ts>
               <ts e="T2736" id="Seg_414" n="e" s="T2735">A </ts>
               <ts e="T2737" id="Seg_416" n="e" s="T2736">dĭ </ts>
               <ts e="T2738" id="Seg_418" n="e" s="T2737">măndə: </ts>
               <ts e="T2739" id="Seg_420" n="e" s="T2738">"(Dĭm=) </ts>
               <ts e="T2740" id="Seg_422" n="e" s="T2739">Kamən </ts>
               <ts e="T2741" id="Seg_424" n="e" s="T2740">dĭ </ts>
               <ts e="T2742" id="Seg_426" n="e" s="T2741">surarga, </ts>
               <ts e="T2743" id="Seg_428" n="e" s="T2742">(ja-) </ts>
               <ts e="T2744" id="Seg_430" n="e" s="T2743">tăn </ts>
               <ts e="T2745" id="Seg_432" n="e" s="T2744">măna </ts>
               <ts e="T2746" id="Seg_434" n="e" s="T2745">măndəraʔ". </ts>
               <ts e="T2747" id="Seg_436" n="e" s="T2746">Dĭgəttə </ts>
               <ts e="T2748" id="Seg_438" n="e" s="T2747">šobiʔi. </ts>
               <ts e="T2749" id="Seg_440" n="e" s="T2748">A </ts>
               <ts e="T2750" id="Seg_442" n="e" s="T2749">volk </ts>
               <ts e="T2751" id="Seg_444" n="e" s="T2750">măndə:" </ts>
               <ts e="T2752" id="Seg_446" n="e" s="T2751">Gibər </ts>
               <ts e="T2753" id="Seg_448" n="e" s="T2752">(si- </ts>
               <ts e="T2754" id="Seg_450" n="e" s="T2753">si-) </ts>
               <ts e="T2755" id="Seg_452" n="e" s="T2754">giraːmbi </ts>
               <ts e="T2756" id="Seg_454" n="e" s="T2755">sil?" </ts>
               <ts e="T2757" id="Seg_456" n="e" s="T2756">A </ts>
               <ts e="T2758" id="Seg_458" n="e" s="T2757">dĭ </ts>
               <ts e="T2759" id="Seg_460" n="e" s="T2758">măndəria </ts>
               <ts e="T2760" id="Seg_462" n="e" s="T2759">dĭʔnə. </ts>
               <ts e="T2761" id="Seg_464" n="e" s="T2760">"Ĭmbi </ts>
               <ts e="T2762" id="Seg_466" n="e" s="T2761">măndərial? </ts>
               <ts e="T2763" id="Seg_468" n="e" s="T2762">Kak </ts>
               <ts e="T2764" id="Seg_470" n="e" s="T2763">amnaʔbəl </ts>
               <ts e="T2765" id="Seg_472" n="e" s="T2764">dăk, </ts>
               <ts e="T2766" id="Seg_474" n="e" s="T2765">ej </ts>
               <ts e="T2767" id="Seg_476" n="e" s="T2766">măndərbial". </ts>
               <ts e="T2768" id="Seg_478" n="e" s="T2767">Dĭgəttə </ts>
               <ts e="T2769" id="Seg_480" n="e" s="T2768">davaj </ts>
               <ts e="T2770" id="Seg_482" n="e" s="T2769">dĭzeŋ </ts>
               <ts e="T2771" id="Seg_484" n="e" s="T2770">urgaːba </ts>
               <ts e="T2772" id="Seg_486" n="e" s="T2771">s </ts>
               <ts e="T2773" id="Seg_488" n="e" s="T2772">volkəm </ts>
               <ts e="T2774" id="Seg_490" n="e" s="T2773">dʼabərozittə. </ts>
               <ts e="T2775" id="Seg_492" n="e" s="T2774">A </ts>
               <ts e="T2776" id="Seg_494" n="e" s="T2775">lʼisʼitsa </ts>
               <ts e="T2777" id="Seg_496" n="e" s="T2776">uja </ts>
               <ts e="T2778" id="Seg_498" n="e" s="T2777">ibi </ts>
               <ts e="T2779" id="Seg_500" n="e" s="T2778">da </ts>
               <ts e="T2780" id="Seg_502" n="e" s="T2779">kalla </ts>
               <ts e="T2781" id="Seg_504" n="e" s="T2780">dʼürbi. </ts>
               <ts e="T2782" id="Seg_506" n="e" s="T2781">Dĭzeŋ </ts>
               <ts e="T2783" id="Seg_508" n="e" s="T2782">nuʔməluʔpiʔi </ts>
               <ts e="T2784" id="Seg_510" n="e" s="T2783">(d-) </ts>
               <ts e="T2785" id="Seg_512" n="e" s="T2784">dĭziʔ. </ts>
               <ts e="T2786" id="Seg_514" n="e" s="T2785">Dĭ </ts>
               <ts e="T2787" id="Seg_516" n="e" s="T2786">bar </ts>
               <ts e="T2788" id="Seg_518" n="e" s="T2787">nuʔməluʔpi, </ts>
               <ts e="T2789" id="Seg_520" n="e" s="T2788">(ej </ts>
               <ts e="T2790" id="Seg_522" n="e" s="T2789">mol-) </ts>
               <ts e="T2791" id="Seg_524" n="e" s="T2790">ej </ts>
               <ts e="T2792" id="Seg_526" n="e" s="T2791">dʼaʔpiʔi </ts>
               <ts e="T2793" id="Seg_528" n="e" s="T2792">dĭzeŋ </ts>
               <ts e="T2794" id="Seg_530" n="e" s="T2793">dĭm. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T2711" id="Seg_531" s="T2704">PKZ_196X_ThreeBeastsEatFat_flk.001 (001)</ta>
            <ta e="T2715" id="Seg_532" s="T2711">PKZ_196X_ThreeBeastsEatFat_flk.002 (002)</ta>
            <ta e="T2718" id="Seg_533" s="T2715">PKZ_196X_ThreeBeastsEatFat_flk.003 (003)</ta>
            <ta e="T2723" id="Seg_534" s="T2718">PKZ_196X_ThreeBeastsEatFat_flk.004 (004)</ta>
            <ta e="T2729" id="Seg_535" s="T2723">PKZ_196X_ThreeBeastsEatFat_flk.005 (005)</ta>
            <ta e="T2735" id="Seg_536" s="T2729">PKZ_196X_ThreeBeastsEatFat_flk.006 (006)</ta>
            <ta e="T2746" id="Seg_537" s="T2735">PKZ_196X_ThreeBeastsEatFat_flk.007 (007)</ta>
            <ta e="T2748" id="Seg_538" s="T2746">PKZ_196X_ThreeBeastsEatFat_flk.008 (008)</ta>
            <ta e="T2756" id="Seg_539" s="T2748">PKZ_196X_ThreeBeastsEatFat_flk.009 (009)</ta>
            <ta e="T2760" id="Seg_540" s="T2756">PKZ_196X_ThreeBeastsEatFat_flk.010 (010)</ta>
            <ta e="T2762" id="Seg_541" s="T2760">PKZ_196X_ThreeBeastsEatFat_flk.011 (011)</ta>
            <ta e="T2767" id="Seg_542" s="T2762">PKZ_196X_ThreeBeastsEatFat_flk.012 (012)</ta>
            <ta e="T2774" id="Seg_543" s="T2767">PKZ_196X_ThreeBeastsEatFat_flk.013 (013)</ta>
            <ta e="T2781" id="Seg_544" s="T2774">PKZ_196X_ThreeBeastsEatFat_flk.014 (014)</ta>
            <ta e="T2785" id="Seg_545" s="T2781">PKZ_196X_ThreeBeastsEatFat_flk.015 (015)</ta>
            <ta e="T2794" id="Seg_546" s="T2785">PKZ_196X_ThreeBeastsEatFat_flk.016 (016)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T2711" id="Seg_547" s="T2704">Amnobiʔi nagurgöʔ urgaːba i volk i lʼisa. </ta>
            <ta e="T2715" id="Seg_548" s="T2711">Dĭgəttə kambiʔi, ĭmbi-nʼibudʼ dʼabəsʼtə. </ta>
            <ta e="T2718" id="Seg_549" s="T2715">Dĭ volk kambi. </ta>
            <ta e="T2723" id="Seg_550" s="T2718">I ular tojirluʔpi i deʔpi. </ta>
            <ta e="T2729" id="Seg_551" s="T2723">Dĭgəttə urgaːba i lʼisʼitsa kambiʔi (bădəsʼtə). </ta>
            <ta e="T2735" id="Seg_552" s="T2729">Urgaːba (m-):" Na što tăn amnial sil?" </ta>
            <ta e="T2746" id="Seg_553" s="T2735">A dĭ măndə: "(Dĭm=) Kamən dĭ surarga, (ja-) tăn măna măndəraʔ". </ta>
            <ta e="T2748" id="Seg_554" s="T2746">Dĭgəttə šobiʔi. </ta>
            <ta e="T2756" id="Seg_555" s="T2748">A volk măndə:" Gibər (si- si-) giraːmbi sil?" </ta>
            <ta e="T2760" id="Seg_556" s="T2756">A dĭ măndəria dĭʔnə. </ta>
            <ta e="T2762" id="Seg_557" s="T2760">"Ĭmbi măndərial? </ta>
            <ta e="T2767" id="Seg_558" s="T2762">Kak amnaʔbəl dăk, ej măndərbial". </ta>
            <ta e="T2774" id="Seg_559" s="T2767">Dĭgəttə davaj dĭzeŋ urgaːba s volkəm dʼabərozittə. </ta>
            <ta e="T2781" id="Seg_560" s="T2774">A lʼisʼitsa uja ibi da kalla dʼürbi. </ta>
            <ta e="T2785" id="Seg_561" s="T2781">Dĭzeŋ nuʔməluʔpiʔi (d-) dĭziʔ. </ta>
            <ta e="T2794" id="Seg_562" s="T2785">Dĭ bar nuʔməluʔpi, (ej mol-) ej dʼaʔpiʔi dĭzeŋ dĭm. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2705" id="Seg_563" s="T2704">amno-bi-ʔi</ta>
            <ta e="T2706" id="Seg_564" s="T2705">nagur-göʔ</ta>
            <ta e="T2707" id="Seg_565" s="T2706">urgaːba</ta>
            <ta e="T2708" id="Seg_566" s="T2707">i</ta>
            <ta e="T2709" id="Seg_567" s="T2708">volk</ta>
            <ta e="T2710" id="Seg_568" s="T2709">i</ta>
            <ta e="T2711" id="Seg_569" s="T2710">lʼisa</ta>
            <ta e="T2712" id="Seg_570" s="T2711">dĭgəttə</ta>
            <ta e="T2713" id="Seg_571" s="T2712">kam-bi-ʔi</ta>
            <ta e="T2714" id="Seg_572" s="T2713">ĭmbi=nʼibudʼ</ta>
            <ta e="T2715" id="Seg_573" s="T2714">dʼabə-sʼtə</ta>
            <ta e="T2716" id="Seg_574" s="T2715">dĭ</ta>
            <ta e="T2717" id="Seg_575" s="T2716">volk</ta>
            <ta e="T2718" id="Seg_576" s="T2717">kam-bi</ta>
            <ta e="T2719" id="Seg_577" s="T2718">i</ta>
            <ta e="T2720" id="Seg_578" s="T2719">ular</ta>
            <ta e="T2721" id="Seg_579" s="T2720">tojir-luʔ-pi</ta>
            <ta e="T2722" id="Seg_580" s="T2721">i</ta>
            <ta e="T2723" id="Seg_581" s="T2722">deʔ-pi</ta>
            <ta e="T2724" id="Seg_582" s="T2723">dĭgəttə</ta>
            <ta e="T2725" id="Seg_583" s="T2724">urgaːba</ta>
            <ta e="T2726" id="Seg_584" s="T2725">i</ta>
            <ta e="T2727" id="Seg_585" s="T2726">lʼisʼitsa</ta>
            <ta e="T2728" id="Seg_586" s="T2727">kam-bi-ʔi</ta>
            <ta e="T2729" id="Seg_587" s="T2728">bădə-sʼtə</ta>
            <ta e="T2730" id="Seg_588" s="T2729">urgaːba</ta>
            <ta e="T2732" id="Seg_589" s="T2731">našto</ta>
            <ta e="T2733" id="Seg_590" s="T2732">tăn</ta>
            <ta e="T2734" id="Seg_591" s="T2733">am-nia-l</ta>
            <ta e="T2735" id="Seg_592" s="T2734">sil</ta>
            <ta e="T2736" id="Seg_593" s="T2735">a</ta>
            <ta e="T2737" id="Seg_594" s="T2736">dĭ</ta>
            <ta e="T2738" id="Seg_595" s="T2737">măn-də</ta>
            <ta e="T2739" id="Seg_596" s="T2738">dĭ-m</ta>
            <ta e="T2740" id="Seg_597" s="T2739">kamən</ta>
            <ta e="T2741" id="Seg_598" s="T2740">dĭ</ta>
            <ta e="T2742" id="Seg_599" s="T2741">surar-ga</ta>
            <ta e="T2744" id="Seg_600" s="T2743">tăn</ta>
            <ta e="T2745" id="Seg_601" s="T2744">măna</ta>
            <ta e="T2746" id="Seg_602" s="T2745">măndə-r-a-ʔ</ta>
            <ta e="T2747" id="Seg_603" s="T2746">dĭgəttə</ta>
            <ta e="T2748" id="Seg_604" s="T2747">šo-bi-ʔi</ta>
            <ta e="T2749" id="Seg_605" s="T2748">a</ta>
            <ta e="T2750" id="Seg_606" s="T2749">volk</ta>
            <ta e="T2751" id="Seg_607" s="T2750">măn-də</ta>
            <ta e="T2752" id="Seg_608" s="T2751">gibər</ta>
            <ta e="T2755" id="Seg_609" s="T2754">giraːm-bi</ta>
            <ta e="T2756" id="Seg_610" s="T2755">sil</ta>
            <ta e="T2757" id="Seg_611" s="T2756">a</ta>
            <ta e="T2758" id="Seg_612" s="T2757">dĭ</ta>
            <ta e="T2759" id="Seg_613" s="T2758">măndə-r-ia</ta>
            <ta e="T2760" id="Seg_614" s="T2759">dĭʔ-nə</ta>
            <ta e="T2761" id="Seg_615" s="T2760">ĭmbi</ta>
            <ta e="T2762" id="Seg_616" s="T2761">măndə-r-ia-l</ta>
            <ta e="T2763" id="Seg_617" s="T2762">kak</ta>
            <ta e="T2764" id="Seg_618" s="T2763">am-naʔbə-l</ta>
            <ta e="T2765" id="Seg_619" s="T2764">dăk</ta>
            <ta e="T2766" id="Seg_620" s="T2765">ej</ta>
            <ta e="T2767" id="Seg_621" s="T2766">măndə-r-bia-l</ta>
            <ta e="T2768" id="Seg_622" s="T2767">dĭgəttə</ta>
            <ta e="T2769" id="Seg_623" s="T2768">davaj</ta>
            <ta e="T2770" id="Seg_624" s="T2769">dĭ-zeŋ</ta>
            <ta e="T2771" id="Seg_625" s="T2770">urgaːba</ta>
            <ta e="T2774" id="Seg_626" s="T2773">dʼabəro-zittə</ta>
            <ta e="T2775" id="Seg_627" s="T2774">a</ta>
            <ta e="T2776" id="Seg_628" s="T2775">lʼisʼitsa</ta>
            <ta e="T2777" id="Seg_629" s="T2776">uja</ta>
            <ta e="T2778" id="Seg_630" s="T2777">i-bi</ta>
            <ta e="T2779" id="Seg_631" s="T2778">da</ta>
            <ta e="T2780" id="Seg_632" s="T2779">kal-la</ta>
            <ta e="T2781" id="Seg_633" s="T2780">dʼür-bi</ta>
            <ta e="T2782" id="Seg_634" s="T2781">dĭ-zeŋ</ta>
            <ta e="T2783" id="Seg_635" s="T2782">nuʔmə-luʔ-pi-ʔi</ta>
            <ta e="T2785" id="Seg_636" s="T2784">dĭ-ziʔ</ta>
            <ta e="T2786" id="Seg_637" s="T2785">dĭ</ta>
            <ta e="T2787" id="Seg_638" s="T2786">bar</ta>
            <ta e="T2788" id="Seg_639" s="T2787">nuʔmə-luʔ-pi</ta>
            <ta e="T2789" id="Seg_640" s="T2788">ej</ta>
            <ta e="T2791" id="Seg_641" s="T2790">ej</ta>
            <ta e="T2792" id="Seg_642" s="T2791">dʼaʔ-pi-ʔi</ta>
            <ta e="T2793" id="Seg_643" s="T2792">dĭ-zeŋ</ta>
            <ta e="T2794" id="Seg_644" s="T2793">dĭ-m</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2705" id="Seg_645" s="T2704">amno-bi-jəʔ</ta>
            <ta e="T2706" id="Seg_646" s="T2705">nagur-göʔ</ta>
            <ta e="T2707" id="Seg_647" s="T2706">urgaːba</ta>
            <ta e="T2708" id="Seg_648" s="T2707">i</ta>
            <ta e="T2709" id="Seg_649" s="T2708">volk</ta>
            <ta e="T2710" id="Seg_650" s="T2709">i</ta>
            <ta e="T2711" id="Seg_651" s="T2710">lʼisa</ta>
            <ta e="T2712" id="Seg_652" s="T2711">dĭgəttə</ta>
            <ta e="T2713" id="Seg_653" s="T2712">kan-bi-jəʔ</ta>
            <ta e="T2714" id="Seg_654" s="T2713">ĭmbi=nʼibudʼ</ta>
            <ta e="T2715" id="Seg_655" s="T2714">dʼabə-zittə</ta>
            <ta e="T2716" id="Seg_656" s="T2715">dĭ</ta>
            <ta e="T2717" id="Seg_657" s="T2716">volk</ta>
            <ta e="T2718" id="Seg_658" s="T2717">kan-bi</ta>
            <ta e="T2719" id="Seg_659" s="T2718">i</ta>
            <ta e="T2720" id="Seg_660" s="T2719">ular</ta>
            <ta e="T2721" id="Seg_661" s="T2720">tojar-luʔbdə-bi</ta>
            <ta e="T2722" id="Seg_662" s="T2721">i</ta>
            <ta e="T2723" id="Seg_663" s="T2722">det-bi</ta>
            <ta e="T2724" id="Seg_664" s="T2723">dĭgəttə</ta>
            <ta e="T2725" id="Seg_665" s="T2724">urgaːba</ta>
            <ta e="T2726" id="Seg_666" s="T2725">i</ta>
            <ta e="T2727" id="Seg_667" s="T2726">lʼisʼitsa</ta>
            <ta e="T2728" id="Seg_668" s="T2727">kan-bi-jəʔ</ta>
            <ta e="T2729" id="Seg_669" s="T2728">bădə-zittə</ta>
            <ta e="T2730" id="Seg_670" s="T2729">urgaːba</ta>
            <ta e="T2732" id="Seg_671" s="T2731">našto</ta>
            <ta e="T2733" id="Seg_672" s="T2732">tăn</ta>
            <ta e="T2734" id="Seg_673" s="T2733">am-liA-l</ta>
            <ta e="T2735" id="Seg_674" s="T2734">sil</ta>
            <ta e="T2736" id="Seg_675" s="T2735">a</ta>
            <ta e="T2737" id="Seg_676" s="T2736">dĭ</ta>
            <ta e="T2738" id="Seg_677" s="T2737">măn-ntə</ta>
            <ta e="T2739" id="Seg_678" s="T2738">dĭ-m</ta>
            <ta e="T2740" id="Seg_679" s="T2739">kamən</ta>
            <ta e="T2741" id="Seg_680" s="T2740">dĭ</ta>
            <ta e="T2742" id="Seg_681" s="T2741">surar-gA</ta>
            <ta e="T2744" id="Seg_682" s="T2743">tăn</ta>
            <ta e="T2745" id="Seg_683" s="T2744">măna</ta>
            <ta e="T2746" id="Seg_684" s="T2745">măndo-r-ə-ʔ</ta>
            <ta e="T2747" id="Seg_685" s="T2746">dĭgəttə</ta>
            <ta e="T2748" id="Seg_686" s="T2747">šo-bi-jəʔ</ta>
            <ta e="T2749" id="Seg_687" s="T2748">a</ta>
            <ta e="T2750" id="Seg_688" s="T2749">volk</ta>
            <ta e="T2751" id="Seg_689" s="T2750">măn-ntə</ta>
            <ta e="T2752" id="Seg_690" s="T2751">gibər</ta>
            <ta e="T2755" id="Seg_691" s="T2754">giraːm-bi</ta>
            <ta e="T2756" id="Seg_692" s="T2755">sil</ta>
            <ta e="T2757" id="Seg_693" s="T2756">a</ta>
            <ta e="T2758" id="Seg_694" s="T2757">dĭ</ta>
            <ta e="T2759" id="Seg_695" s="T2758">măndo-r-liA</ta>
            <ta e="T2760" id="Seg_696" s="T2759">dĭ-Tə</ta>
            <ta e="T2761" id="Seg_697" s="T2760">ĭmbi</ta>
            <ta e="T2762" id="Seg_698" s="T2761">măndo-r-liA-l</ta>
            <ta e="T2763" id="Seg_699" s="T2762">kak</ta>
            <ta e="T2764" id="Seg_700" s="T2763">am-laʔbə-l</ta>
            <ta e="T2765" id="Seg_701" s="T2764">tak</ta>
            <ta e="T2766" id="Seg_702" s="T2765">ej</ta>
            <ta e="T2767" id="Seg_703" s="T2766">măndo-r-bi-l</ta>
            <ta e="T2768" id="Seg_704" s="T2767">dĭgəttə</ta>
            <ta e="T2769" id="Seg_705" s="T2768">davaj</ta>
            <ta e="T2770" id="Seg_706" s="T2769">dĭ-zAŋ</ta>
            <ta e="T2771" id="Seg_707" s="T2770">urgaːba</ta>
            <ta e="T2774" id="Seg_708" s="T2773">tʼabəro-zittə</ta>
            <ta e="T2775" id="Seg_709" s="T2774">a</ta>
            <ta e="T2776" id="Seg_710" s="T2775">lʼisʼitsa</ta>
            <ta e="T2777" id="Seg_711" s="T2776">uja</ta>
            <ta e="T2778" id="Seg_712" s="T2777">i-bi</ta>
            <ta e="T2779" id="Seg_713" s="T2778">da</ta>
            <ta e="T2780" id="Seg_714" s="T2779">kan-lAʔ</ta>
            <ta e="T2781" id="Seg_715" s="T2780">tʼür-bi</ta>
            <ta e="T2782" id="Seg_716" s="T2781">dĭ-zAŋ</ta>
            <ta e="T2783" id="Seg_717" s="T2782">nuʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T2785" id="Seg_718" s="T2784">dĭ-ziʔ</ta>
            <ta e="T2786" id="Seg_719" s="T2785">dĭ</ta>
            <ta e="T2787" id="Seg_720" s="T2786">bar</ta>
            <ta e="T2788" id="Seg_721" s="T2787">nuʔmə-luʔbdə-bi</ta>
            <ta e="T2789" id="Seg_722" s="T2788">ej</ta>
            <ta e="T2791" id="Seg_723" s="T2790">ej</ta>
            <ta e="T2792" id="Seg_724" s="T2791">dʼabə-bi-jəʔ</ta>
            <ta e="T2793" id="Seg_725" s="T2792">dĭ-zAŋ</ta>
            <ta e="T2794" id="Seg_726" s="T2793">dĭ-m</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2705" id="Seg_727" s="T2704">live-PST-3PL</ta>
            <ta e="T2706" id="Seg_728" s="T2705">three-COLL</ta>
            <ta e="T2707" id="Seg_729" s="T2706">bear.[NOM.SG]</ta>
            <ta e="T2708" id="Seg_730" s="T2707">and</ta>
            <ta e="T2709" id="Seg_731" s="T2708">wolf.[NOM.SG]</ta>
            <ta e="T2710" id="Seg_732" s="T2709">and</ta>
            <ta e="T2711" id="Seg_733" s="T2710">fox.[NOM.SG]</ta>
            <ta e="T2712" id="Seg_734" s="T2711">then</ta>
            <ta e="T2713" id="Seg_735" s="T2712">go-PST-3PL</ta>
            <ta e="T2714" id="Seg_736" s="T2713">what=INDEF</ta>
            <ta e="T2715" id="Seg_737" s="T2714">capture-INF.LAT</ta>
            <ta e="T2716" id="Seg_738" s="T2715">this.[NOM.SG]</ta>
            <ta e="T2717" id="Seg_739" s="T2716">wolf.[NOM.SG]</ta>
            <ta e="T2718" id="Seg_740" s="T2717">go-PST.[3SG]</ta>
            <ta e="T2719" id="Seg_741" s="T2718">and</ta>
            <ta e="T2720" id="Seg_742" s="T2719">sheep.[NOM.SG]</ta>
            <ta e="T2721" id="Seg_743" s="T2720">steal-MOM-PST.[3SG]</ta>
            <ta e="T2722" id="Seg_744" s="T2721">and</ta>
            <ta e="T2723" id="Seg_745" s="T2722">bring-PST.[3SG]</ta>
            <ta e="T2724" id="Seg_746" s="T2723">then</ta>
            <ta e="T2725" id="Seg_747" s="T2724">bear.[NOM.SG]</ta>
            <ta e="T2726" id="Seg_748" s="T2725">and</ta>
            <ta e="T2727" id="Seg_749" s="T2726">fox.[NOM.SG]</ta>
            <ta e="T2728" id="Seg_750" s="T2727">go-PST-3PL</ta>
            <ta e="T2729" id="Seg_751" s="T2728">feed-INF.LAT</ta>
            <ta e="T2730" id="Seg_752" s="T2729">bear.[NOM.SG]</ta>
            <ta e="T2732" id="Seg_753" s="T2731">what.for</ta>
            <ta e="T2733" id="Seg_754" s="T2732">you.NOM</ta>
            <ta e="T2734" id="Seg_755" s="T2733">eat-PRS-2SG</ta>
            <ta e="T2735" id="Seg_756" s="T2734">fat.[NOM.SG]</ta>
            <ta e="T2736" id="Seg_757" s="T2735">and</ta>
            <ta e="T2737" id="Seg_758" s="T2736">this.[NOM.SG]</ta>
            <ta e="T2738" id="Seg_759" s="T2737">say-IPFVZ.[3SG]</ta>
            <ta e="T2739" id="Seg_760" s="T2738">this-ACC</ta>
            <ta e="T2740" id="Seg_761" s="T2739">when</ta>
            <ta e="T2741" id="Seg_762" s="T2740">this.[NOM.SG]</ta>
            <ta e="T2742" id="Seg_763" s="T2741">ask-PRS.[3SG]</ta>
            <ta e="T2744" id="Seg_764" s="T2743">you.NOM</ta>
            <ta e="T2745" id="Seg_765" s="T2744">I.LAT</ta>
            <ta e="T2746" id="Seg_766" s="T2745">look-FRQ-EP-IMP.2SG</ta>
            <ta e="T2747" id="Seg_767" s="T2746">then</ta>
            <ta e="T2748" id="Seg_768" s="T2747">come-PST-3PL</ta>
            <ta e="T2749" id="Seg_769" s="T2748">and</ta>
            <ta e="T2750" id="Seg_770" s="T2749">wolf.[NOM.SG]</ta>
            <ta e="T2751" id="Seg_771" s="T2750">say-IPFVZ.[3SG]</ta>
            <ta e="T2752" id="Seg_772" s="T2751">where.to</ta>
            <ta e="T2755" id="Seg_773" s="T2754">go.where-PST.[3SG]</ta>
            <ta e="T2756" id="Seg_774" s="T2755">fat.[NOM.SG]</ta>
            <ta e="T2757" id="Seg_775" s="T2756">and</ta>
            <ta e="T2758" id="Seg_776" s="T2757">this.[NOM.SG]</ta>
            <ta e="T2759" id="Seg_777" s="T2758">look-FRQ-PRS.[3SG]</ta>
            <ta e="T2760" id="Seg_778" s="T2759">this-LAT</ta>
            <ta e="T2761" id="Seg_779" s="T2760">what.[NOM.SG]</ta>
            <ta e="T2762" id="Seg_780" s="T2761">look-FRQ-PRS-2SG</ta>
            <ta e="T2763" id="Seg_781" s="T2762">like</ta>
            <ta e="T2764" id="Seg_782" s="T2763">eat-DUR-2SG</ta>
            <ta e="T2765" id="Seg_783" s="T2764">so</ta>
            <ta e="T2766" id="Seg_784" s="T2765">NEG</ta>
            <ta e="T2767" id="Seg_785" s="T2766">look-FRQ-PST-2SG</ta>
            <ta e="T2768" id="Seg_786" s="T2767">then</ta>
            <ta e="T2769" id="Seg_787" s="T2768">INCH</ta>
            <ta e="T2770" id="Seg_788" s="T2769">this-PL</ta>
            <ta e="T2771" id="Seg_789" s="T2770">bear.[NOM.SG]</ta>
            <ta e="T2774" id="Seg_790" s="T2773">fight-INF.LAT</ta>
            <ta e="T2775" id="Seg_791" s="T2774">and</ta>
            <ta e="T2776" id="Seg_792" s="T2775">fox.[NOM.SG]</ta>
            <ta e="T2777" id="Seg_793" s="T2776">meat.[NOM.SG]</ta>
            <ta e="T2778" id="Seg_794" s="T2777">take-PST.[3SG]</ta>
            <ta e="T2779" id="Seg_795" s="T2778">and</ta>
            <ta e="T2780" id="Seg_796" s="T2779">go-CVB</ta>
            <ta e="T2781" id="Seg_797" s="T2780">disappear-PST.[3SG]</ta>
            <ta e="T2782" id="Seg_798" s="T2781">this-PL</ta>
            <ta e="T2783" id="Seg_799" s="T2782">run-MOM-PST-3PL</ta>
            <ta e="T2785" id="Seg_800" s="T2784">this-INS</ta>
            <ta e="T2786" id="Seg_801" s="T2785">this.[NOM.SG]</ta>
            <ta e="T2787" id="Seg_802" s="T2786">PTCL</ta>
            <ta e="T2788" id="Seg_803" s="T2787">run-MOM-PST.[3SG]</ta>
            <ta e="T2789" id="Seg_804" s="T2788">NEG</ta>
            <ta e="T2791" id="Seg_805" s="T2790">NEG</ta>
            <ta e="T2792" id="Seg_806" s="T2791">capture-PST-3PL</ta>
            <ta e="T2793" id="Seg_807" s="T2792">this-PL</ta>
            <ta e="T2794" id="Seg_808" s="T2793">this-ACC</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2705" id="Seg_809" s="T2704">жить-PST-3PL</ta>
            <ta e="T2706" id="Seg_810" s="T2705">три-COLL</ta>
            <ta e="T2707" id="Seg_811" s="T2706">медведь.[NOM.SG]</ta>
            <ta e="T2708" id="Seg_812" s="T2707">и</ta>
            <ta e="T2709" id="Seg_813" s="T2708">волк.[NOM.SG]</ta>
            <ta e="T2710" id="Seg_814" s="T2709">и</ta>
            <ta e="T2711" id="Seg_815" s="T2710">лиса.[NOM.SG]</ta>
            <ta e="T2712" id="Seg_816" s="T2711">тогда</ta>
            <ta e="T2713" id="Seg_817" s="T2712">пойти-PST-3PL</ta>
            <ta e="T2714" id="Seg_818" s="T2713">что=INDEF</ta>
            <ta e="T2715" id="Seg_819" s="T2714">ловить-INF.LAT</ta>
            <ta e="T2716" id="Seg_820" s="T2715">этот.[NOM.SG]</ta>
            <ta e="T2717" id="Seg_821" s="T2716">волк.[NOM.SG]</ta>
            <ta e="T2718" id="Seg_822" s="T2717">пойти-PST.[3SG]</ta>
            <ta e="T2719" id="Seg_823" s="T2718">и</ta>
            <ta e="T2720" id="Seg_824" s="T2719">овца.[NOM.SG]</ta>
            <ta e="T2721" id="Seg_825" s="T2720">украсть-MOM-PST.[3SG]</ta>
            <ta e="T2722" id="Seg_826" s="T2721">и</ta>
            <ta e="T2723" id="Seg_827" s="T2722">принести-PST.[3SG]</ta>
            <ta e="T2724" id="Seg_828" s="T2723">тогда</ta>
            <ta e="T2725" id="Seg_829" s="T2724">медведь.[NOM.SG]</ta>
            <ta e="T2726" id="Seg_830" s="T2725">и</ta>
            <ta e="T2727" id="Seg_831" s="T2726">лисица.[NOM.SG]</ta>
            <ta e="T2728" id="Seg_832" s="T2727">пойти-PST-3PL</ta>
            <ta e="T2729" id="Seg_833" s="T2728">кормить-INF.LAT</ta>
            <ta e="T2730" id="Seg_834" s="T2729">медведь.[NOM.SG]</ta>
            <ta e="T2732" id="Seg_835" s="T2731">зачем</ta>
            <ta e="T2733" id="Seg_836" s="T2732">ты.NOM</ta>
            <ta e="T2734" id="Seg_837" s="T2733">съесть-PRS-2SG</ta>
            <ta e="T2735" id="Seg_838" s="T2734">жир.[NOM.SG]</ta>
            <ta e="T2736" id="Seg_839" s="T2735">а</ta>
            <ta e="T2737" id="Seg_840" s="T2736">этот.[NOM.SG]</ta>
            <ta e="T2738" id="Seg_841" s="T2737">сказать-IPFVZ.[3SG]</ta>
            <ta e="T2739" id="Seg_842" s="T2738">этот-ACC</ta>
            <ta e="T2740" id="Seg_843" s="T2739">когда</ta>
            <ta e="T2741" id="Seg_844" s="T2740">этот.[NOM.SG]</ta>
            <ta e="T2742" id="Seg_845" s="T2741">спросить-PRS.[3SG]</ta>
            <ta e="T2744" id="Seg_846" s="T2743">ты.NOM</ta>
            <ta e="T2745" id="Seg_847" s="T2744">я.LAT</ta>
            <ta e="T2746" id="Seg_848" s="T2745">смотреть-FRQ-EP-IMP.2SG</ta>
            <ta e="T2747" id="Seg_849" s="T2746">тогда</ta>
            <ta e="T2748" id="Seg_850" s="T2747">прийти-PST-3PL</ta>
            <ta e="T2749" id="Seg_851" s="T2748">а</ta>
            <ta e="T2750" id="Seg_852" s="T2749">волк.[NOM.SG]</ta>
            <ta e="T2751" id="Seg_853" s="T2750">сказать-IPFVZ.[3SG]</ta>
            <ta e="T2752" id="Seg_854" s="T2751">куда</ta>
            <ta e="T2755" id="Seg_855" s="T2754">куда.идти-PST.[3SG]</ta>
            <ta e="T2756" id="Seg_856" s="T2755">жир.[NOM.SG]</ta>
            <ta e="T2757" id="Seg_857" s="T2756">а</ta>
            <ta e="T2758" id="Seg_858" s="T2757">этот.[NOM.SG]</ta>
            <ta e="T2759" id="Seg_859" s="T2758">смотреть-FRQ-PRS.[3SG]</ta>
            <ta e="T2760" id="Seg_860" s="T2759">этот-LAT</ta>
            <ta e="T2761" id="Seg_861" s="T2760">что.[NOM.SG]</ta>
            <ta e="T2762" id="Seg_862" s="T2761">смотреть-FRQ-PRS-2SG</ta>
            <ta e="T2763" id="Seg_863" s="T2762">как</ta>
            <ta e="T2764" id="Seg_864" s="T2763">съесть-DUR-2SG</ta>
            <ta e="T2765" id="Seg_865" s="T2764">так</ta>
            <ta e="T2766" id="Seg_866" s="T2765">NEG</ta>
            <ta e="T2767" id="Seg_867" s="T2766">смотреть-FRQ-PST-2SG</ta>
            <ta e="T2768" id="Seg_868" s="T2767">тогда</ta>
            <ta e="T2769" id="Seg_869" s="T2768">INCH</ta>
            <ta e="T2770" id="Seg_870" s="T2769">этот-PL</ta>
            <ta e="T2771" id="Seg_871" s="T2770">медведь.[NOM.SG]</ta>
            <ta e="T2774" id="Seg_872" s="T2773">бороться-INF.LAT</ta>
            <ta e="T2775" id="Seg_873" s="T2774">а</ta>
            <ta e="T2776" id="Seg_874" s="T2775">лисица.[NOM.SG]</ta>
            <ta e="T2777" id="Seg_875" s="T2776">мясо.[NOM.SG]</ta>
            <ta e="T2778" id="Seg_876" s="T2777">взять-PST.[3SG]</ta>
            <ta e="T2779" id="Seg_877" s="T2778">и</ta>
            <ta e="T2780" id="Seg_878" s="T2779">пойти-CVB</ta>
            <ta e="T2781" id="Seg_879" s="T2780">исчезнуть-PST.[3SG]</ta>
            <ta e="T2782" id="Seg_880" s="T2781">этот-PL</ta>
            <ta e="T2783" id="Seg_881" s="T2782">бежать-MOM-PST-3PL</ta>
            <ta e="T2785" id="Seg_882" s="T2784">этот-INS</ta>
            <ta e="T2786" id="Seg_883" s="T2785">этот.[NOM.SG]</ta>
            <ta e="T2787" id="Seg_884" s="T2786">PTCL</ta>
            <ta e="T2788" id="Seg_885" s="T2787">бежать-MOM-PST.[3SG]</ta>
            <ta e="T2789" id="Seg_886" s="T2788">NEG</ta>
            <ta e="T2791" id="Seg_887" s="T2790">NEG</ta>
            <ta e="T2792" id="Seg_888" s="T2791">ловить-PST-3PL</ta>
            <ta e="T2793" id="Seg_889" s="T2792">этот-PL</ta>
            <ta e="T2794" id="Seg_890" s="T2793">этот-ACC</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2705" id="Seg_891" s="T2704">v-v:tense-v:pn</ta>
            <ta e="T2706" id="Seg_892" s="T2705">num-num&gt;num</ta>
            <ta e="T2707" id="Seg_893" s="T2706">n-n:case</ta>
            <ta e="T2708" id="Seg_894" s="T2707">conj</ta>
            <ta e="T2709" id="Seg_895" s="T2708">n-n:case</ta>
            <ta e="T2710" id="Seg_896" s="T2709">conj</ta>
            <ta e="T2711" id="Seg_897" s="T2710">n-n:case</ta>
            <ta e="T2712" id="Seg_898" s="T2711">adv</ta>
            <ta e="T2713" id="Seg_899" s="T2712">v-v:tense-v:pn</ta>
            <ta e="T2714" id="Seg_900" s="T2713">que=ptcl</ta>
            <ta e="T2715" id="Seg_901" s="T2714">v-v:n.fin</ta>
            <ta e="T2716" id="Seg_902" s="T2715">dempro-n:case</ta>
            <ta e="T2717" id="Seg_903" s="T2716">n-n:case</ta>
            <ta e="T2718" id="Seg_904" s="T2717">v-v:tense-v:pn</ta>
            <ta e="T2719" id="Seg_905" s="T2718">conj</ta>
            <ta e="T2720" id="Seg_906" s="T2719">n-n:case</ta>
            <ta e="T2721" id="Seg_907" s="T2720">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2722" id="Seg_908" s="T2721">conj</ta>
            <ta e="T2723" id="Seg_909" s="T2722">v-v:tense-v:pn</ta>
            <ta e="T2724" id="Seg_910" s="T2723">adv</ta>
            <ta e="T2725" id="Seg_911" s="T2724">n-n:case</ta>
            <ta e="T2726" id="Seg_912" s="T2725">conj</ta>
            <ta e="T2727" id="Seg_913" s="T2726">n-n:case</ta>
            <ta e="T2728" id="Seg_914" s="T2727">v-v:tense-v:pn</ta>
            <ta e="T2729" id="Seg_915" s="T2728">v-v:n.fin</ta>
            <ta e="T2730" id="Seg_916" s="T2729">n-n:case</ta>
            <ta e="T2732" id="Seg_917" s="T2731">adv</ta>
            <ta e="T2733" id="Seg_918" s="T2732">pers</ta>
            <ta e="T2734" id="Seg_919" s="T2733">v-v:tense-v:pn</ta>
            <ta e="T2735" id="Seg_920" s="T2734">n-n:case</ta>
            <ta e="T2736" id="Seg_921" s="T2735">conj</ta>
            <ta e="T2737" id="Seg_922" s="T2736">dempro-n:case</ta>
            <ta e="T2738" id="Seg_923" s="T2737">v-v&gt;v-v:pn</ta>
            <ta e="T2739" id="Seg_924" s="T2738">dempro-n:case</ta>
            <ta e="T2740" id="Seg_925" s="T2739">que</ta>
            <ta e="T2741" id="Seg_926" s="T2740">dempro-n:case</ta>
            <ta e="T2742" id="Seg_927" s="T2741">v-v:tense-v:pn</ta>
            <ta e="T2744" id="Seg_928" s="T2743">pers</ta>
            <ta e="T2745" id="Seg_929" s="T2744">pers</ta>
            <ta e="T2746" id="Seg_930" s="T2745">v-v&gt;v-v:ins-v:mood.pn</ta>
            <ta e="T2747" id="Seg_931" s="T2746">adv</ta>
            <ta e="T2748" id="Seg_932" s="T2747">v-v:tense-v:pn</ta>
            <ta e="T2749" id="Seg_933" s="T2748">conj</ta>
            <ta e="T2750" id="Seg_934" s="T2749">n-n:case</ta>
            <ta e="T2751" id="Seg_935" s="T2750">v-v&gt;v-v:pn</ta>
            <ta e="T2752" id="Seg_936" s="T2751">que</ta>
            <ta e="T2755" id="Seg_937" s="T2754">v-v:tense-v:pn</ta>
            <ta e="T2756" id="Seg_938" s="T2755">n-n:case</ta>
            <ta e="T2757" id="Seg_939" s="T2756">conj</ta>
            <ta e="T2758" id="Seg_940" s="T2757">dempro-n:case</ta>
            <ta e="T2759" id="Seg_941" s="T2758">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2760" id="Seg_942" s="T2759">dempro-n:case</ta>
            <ta e="T2761" id="Seg_943" s="T2760">que-n:case</ta>
            <ta e="T2762" id="Seg_944" s="T2761">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2763" id="Seg_945" s="T2762">ptcl</ta>
            <ta e="T2764" id="Seg_946" s="T2763">v-v&gt;v-v:pn</ta>
            <ta e="T2765" id="Seg_947" s="T2764">ptcl</ta>
            <ta e="T2766" id="Seg_948" s="T2765">ptcl</ta>
            <ta e="T2767" id="Seg_949" s="T2766">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2768" id="Seg_950" s="T2767">adv</ta>
            <ta e="T2769" id="Seg_951" s="T2768">ptcl</ta>
            <ta e="T2770" id="Seg_952" s="T2769">dempro-n:num</ta>
            <ta e="T2771" id="Seg_953" s="T2770">n-n:case</ta>
            <ta e="T2774" id="Seg_954" s="T2773">v-v:n.fin</ta>
            <ta e="T2775" id="Seg_955" s="T2774">conj</ta>
            <ta e="T2776" id="Seg_956" s="T2775">n-n:case</ta>
            <ta e="T2777" id="Seg_957" s="T2776">n-n:case</ta>
            <ta e="T2778" id="Seg_958" s="T2777">v-v:tense-v:pn</ta>
            <ta e="T2779" id="Seg_959" s="T2778">conj</ta>
            <ta e="T2780" id="Seg_960" s="T2779">v-v:n.fin</ta>
            <ta e="T2781" id="Seg_961" s="T2780">v-v:tense-v:pn</ta>
            <ta e="T2782" id="Seg_962" s="T2781">dempro-n:num</ta>
            <ta e="T2783" id="Seg_963" s="T2782">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2785" id="Seg_964" s="T2784">dempro-n:case</ta>
            <ta e="T2786" id="Seg_965" s="T2785">dempro-n:case</ta>
            <ta e="T2787" id="Seg_966" s="T2786">ptcl</ta>
            <ta e="T2788" id="Seg_967" s="T2787">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2789" id="Seg_968" s="T2788">ptcl</ta>
            <ta e="T2791" id="Seg_969" s="T2790">ptcl</ta>
            <ta e="T2792" id="Seg_970" s="T2791">v-v:tense-v:pn</ta>
            <ta e="T2793" id="Seg_971" s="T2792">dempro-n:num</ta>
            <ta e="T2794" id="Seg_972" s="T2793">dempro-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2705" id="Seg_973" s="T2704">v</ta>
            <ta e="T2706" id="Seg_974" s="T2705">num</ta>
            <ta e="T2707" id="Seg_975" s="T2706">n</ta>
            <ta e="T2708" id="Seg_976" s="T2707">conj</ta>
            <ta e="T2709" id="Seg_977" s="T2708">n</ta>
            <ta e="T2710" id="Seg_978" s="T2709">conj</ta>
            <ta e="T2711" id="Seg_979" s="T2710">n</ta>
            <ta e="T2712" id="Seg_980" s="T2711">adv</ta>
            <ta e="T2713" id="Seg_981" s="T2712">v</ta>
            <ta e="T2714" id="Seg_982" s="T2713">que</ta>
            <ta e="T2715" id="Seg_983" s="T2714">v</ta>
            <ta e="T2716" id="Seg_984" s="T2715">dempro</ta>
            <ta e="T2717" id="Seg_985" s="T2716">n</ta>
            <ta e="T2718" id="Seg_986" s="T2717">v</ta>
            <ta e="T2719" id="Seg_987" s="T2718">conj</ta>
            <ta e="T2720" id="Seg_988" s="T2719">n</ta>
            <ta e="T2721" id="Seg_989" s="T2720">v</ta>
            <ta e="T2722" id="Seg_990" s="T2721">conj</ta>
            <ta e="T2723" id="Seg_991" s="T2722">v</ta>
            <ta e="T2724" id="Seg_992" s="T2723">adv</ta>
            <ta e="T2725" id="Seg_993" s="T2724">n</ta>
            <ta e="T2726" id="Seg_994" s="T2725">conj</ta>
            <ta e="T2727" id="Seg_995" s="T2726">n</ta>
            <ta e="T2728" id="Seg_996" s="T2727">v</ta>
            <ta e="T2729" id="Seg_997" s="T2728">v</ta>
            <ta e="T2730" id="Seg_998" s="T2729">n</ta>
            <ta e="T2732" id="Seg_999" s="T2731">adv</ta>
            <ta e="T2733" id="Seg_1000" s="T2732">pers</ta>
            <ta e="T2734" id="Seg_1001" s="T2733">v</ta>
            <ta e="T2735" id="Seg_1002" s="T2734">n</ta>
            <ta e="T2736" id="Seg_1003" s="T2735">conj</ta>
            <ta e="T2737" id="Seg_1004" s="T2736">dempro</ta>
            <ta e="T2738" id="Seg_1005" s="T2737">v</ta>
            <ta e="T2739" id="Seg_1006" s="T2738">dempro</ta>
            <ta e="T2740" id="Seg_1007" s="T2739">conj</ta>
            <ta e="T2741" id="Seg_1008" s="T2740">dempro</ta>
            <ta e="T2742" id="Seg_1009" s="T2741">v</ta>
            <ta e="T2744" id="Seg_1010" s="T2743">pers</ta>
            <ta e="T2745" id="Seg_1011" s="T2744">pers</ta>
            <ta e="T2746" id="Seg_1012" s="T2745">v</ta>
            <ta e="T2747" id="Seg_1013" s="T2746">adv</ta>
            <ta e="T2748" id="Seg_1014" s="T2747">v</ta>
            <ta e="T2749" id="Seg_1015" s="T2748">conj</ta>
            <ta e="T2750" id="Seg_1016" s="T2749">n</ta>
            <ta e="T2751" id="Seg_1017" s="T2750">v</ta>
            <ta e="T2752" id="Seg_1018" s="T2751">que</ta>
            <ta e="T2755" id="Seg_1019" s="T2754">v</ta>
            <ta e="T2756" id="Seg_1020" s="T2755">n</ta>
            <ta e="T2757" id="Seg_1021" s="T2756">conj</ta>
            <ta e="T2758" id="Seg_1022" s="T2757">dempro</ta>
            <ta e="T2759" id="Seg_1023" s="T2758">v</ta>
            <ta e="T2760" id="Seg_1024" s="T2759">dempro</ta>
            <ta e="T2761" id="Seg_1025" s="T2760">que</ta>
            <ta e="T2762" id="Seg_1026" s="T2761">v</ta>
            <ta e="T2763" id="Seg_1027" s="T2762">ptcl</ta>
            <ta e="T2764" id="Seg_1028" s="T2763">v</ta>
            <ta e="T2765" id="Seg_1029" s="T2764">ptcl</ta>
            <ta e="T2766" id="Seg_1030" s="T2765">ptcl</ta>
            <ta e="T2767" id="Seg_1031" s="T2766">v</ta>
            <ta e="T2768" id="Seg_1032" s="T2767">adv</ta>
            <ta e="T2769" id="Seg_1033" s="T2768">ptcl</ta>
            <ta e="T2770" id="Seg_1034" s="T2769">dempro</ta>
            <ta e="T2771" id="Seg_1035" s="T2770">n</ta>
            <ta e="T2774" id="Seg_1036" s="T2773">v</ta>
            <ta e="T2775" id="Seg_1037" s="T2774">conj</ta>
            <ta e="T2776" id="Seg_1038" s="T2775">n</ta>
            <ta e="T2777" id="Seg_1039" s="T2776">n</ta>
            <ta e="T2778" id="Seg_1040" s="T2777">v</ta>
            <ta e="T2779" id="Seg_1041" s="T2778">conj</ta>
            <ta e="T2780" id="Seg_1042" s="T2779">v</ta>
            <ta e="T2781" id="Seg_1043" s="T2780">v</ta>
            <ta e="T2782" id="Seg_1044" s="T2781">dempro</ta>
            <ta e="T2783" id="Seg_1045" s="T2782">v</ta>
            <ta e="T2785" id="Seg_1046" s="T2784">dempro</ta>
            <ta e="T2786" id="Seg_1047" s="T2785">dempro</ta>
            <ta e="T2787" id="Seg_1048" s="T2786">ptcl</ta>
            <ta e="T2788" id="Seg_1049" s="T2787">v</ta>
            <ta e="T2789" id="Seg_1050" s="T2788">ptcl</ta>
            <ta e="T2791" id="Seg_1051" s="T2790">ptcl</ta>
            <ta e="T2792" id="Seg_1052" s="T2791">v</ta>
            <ta e="T2793" id="Seg_1053" s="T2792">dempro</ta>
            <ta e="T2794" id="Seg_1054" s="T2793">dempro</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2707" id="Seg_1055" s="T2706">np.h:E</ta>
            <ta e="T2709" id="Seg_1056" s="T2708">np.h:E</ta>
            <ta e="T2711" id="Seg_1057" s="T2710">np.h:E</ta>
            <ta e="T2712" id="Seg_1058" s="T2711">adv:Time</ta>
            <ta e="T2713" id="Seg_1059" s="T2712">0.3.h:A</ta>
            <ta e="T2714" id="Seg_1060" s="T2713">pro:Th</ta>
            <ta e="T2717" id="Seg_1061" s="T2716">np.h:A</ta>
            <ta e="T2720" id="Seg_1062" s="T2719">np:Th</ta>
            <ta e="T2721" id="Seg_1063" s="T2720">0.3.h:A</ta>
            <ta e="T2723" id="Seg_1064" s="T2722">0.3.h:A</ta>
            <ta e="T2724" id="Seg_1065" s="T2723">adv:Time</ta>
            <ta e="T2725" id="Seg_1066" s="T2724">np.h:A</ta>
            <ta e="T2727" id="Seg_1067" s="T2726">np.h:A</ta>
            <ta e="T2730" id="Seg_1068" s="T2729">np.h:A</ta>
            <ta e="T2733" id="Seg_1069" s="T2732">pro.h:A</ta>
            <ta e="T2735" id="Seg_1070" s="T2734">np:P</ta>
            <ta e="T2737" id="Seg_1071" s="T2736">pro.h:A</ta>
            <ta e="T2741" id="Seg_1072" s="T2740">pro.h:A</ta>
            <ta e="T2744" id="Seg_1073" s="T2743">pro.h:A</ta>
            <ta e="T2745" id="Seg_1074" s="T2744">pro:G</ta>
            <ta e="T2747" id="Seg_1075" s="T2746">adv:Time</ta>
            <ta e="T2748" id="Seg_1076" s="T2747">0.3.h:A</ta>
            <ta e="T2750" id="Seg_1077" s="T2749">np.h:A</ta>
            <ta e="T2756" id="Seg_1078" s="T2755">np:Th</ta>
            <ta e="T2758" id="Seg_1079" s="T2757">pro.h:A</ta>
            <ta e="T2760" id="Seg_1080" s="T2759">pro:G</ta>
            <ta e="T2762" id="Seg_1081" s="T2761">0.2.h:A</ta>
            <ta e="T2764" id="Seg_1082" s="T2763">0.2.h:A</ta>
            <ta e="T2767" id="Seg_1083" s="T2766">0.2.h:A</ta>
            <ta e="T2768" id="Seg_1084" s="T2767">adv:Time</ta>
            <ta e="T2770" id="Seg_1085" s="T2769">pro.h:A</ta>
            <ta e="T2771" id="Seg_1086" s="T2770">np.h:A</ta>
            <ta e="T2776" id="Seg_1087" s="T2775">np.h:A</ta>
            <ta e="T2777" id="Seg_1088" s="T2776">np:Th</ta>
            <ta e="T2781" id="Seg_1089" s="T2780">0.3.h:A</ta>
            <ta e="T2782" id="Seg_1090" s="T2781">pro.h:A</ta>
            <ta e="T2785" id="Seg_1091" s="T2784">pro.h:Com</ta>
            <ta e="T2786" id="Seg_1092" s="T2785">pro.h:A</ta>
            <ta e="T2793" id="Seg_1093" s="T2792">pro.h:A</ta>
            <ta e="T2794" id="Seg_1094" s="T2793">pro.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2705" id="Seg_1095" s="T2704">v:pred</ta>
            <ta e="T2707" id="Seg_1096" s="T2706">np.h:S</ta>
            <ta e="T2709" id="Seg_1097" s="T2708">np.h:S</ta>
            <ta e="T2711" id="Seg_1098" s="T2710">np.h:S</ta>
            <ta e="T2713" id="Seg_1099" s="T2712">v:pred 0.3.h:S</ta>
            <ta e="T2714" id="Seg_1100" s="T2713">pro:O</ta>
            <ta e="T2715" id="Seg_1101" s="T2714">s:purp</ta>
            <ta e="T2717" id="Seg_1102" s="T2716">np.h:S</ta>
            <ta e="T2718" id="Seg_1103" s="T2717">v:pred</ta>
            <ta e="T2720" id="Seg_1104" s="T2719">np:O</ta>
            <ta e="T2721" id="Seg_1105" s="T2720">v:pred 0.3.h:S</ta>
            <ta e="T2723" id="Seg_1106" s="T2722">v:pred 0.3.h:S</ta>
            <ta e="T2725" id="Seg_1107" s="T2724">np.h:S</ta>
            <ta e="T2727" id="Seg_1108" s="T2726">np.h:S</ta>
            <ta e="T2728" id="Seg_1109" s="T2727">v:pred</ta>
            <ta e="T2729" id="Seg_1110" s="T2728">s:purp</ta>
            <ta e="T2730" id="Seg_1111" s="T2729">np.h:S</ta>
            <ta e="T2733" id="Seg_1112" s="T2732">pro.h:S</ta>
            <ta e="T2734" id="Seg_1113" s="T2733">v:pred</ta>
            <ta e="T2735" id="Seg_1114" s="T2734">np:O</ta>
            <ta e="T2737" id="Seg_1115" s="T2736">pro.h:S</ta>
            <ta e="T2738" id="Seg_1116" s="T2737">v:pred</ta>
            <ta e="T2741" id="Seg_1117" s="T2740">pro.h:S</ta>
            <ta e="T2742" id="Seg_1118" s="T2741">v:pred</ta>
            <ta e="T2744" id="Seg_1119" s="T2743">pro.h:S</ta>
            <ta e="T2746" id="Seg_1120" s="T2745">v:pred</ta>
            <ta e="T2748" id="Seg_1121" s="T2747">v:pred 0.3.h:S</ta>
            <ta e="T2750" id="Seg_1122" s="T2749">np.h:S</ta>
            <ta e="T2751" id="Seg_1123" s="T2750">v:pred</ta>
            <ta e="T2755" id="Seg_1124" s="T2754">v:pred</ta>
            <ta e="T2756" id="Seg_1125" s="T2755">np:S</ta>
            <ta e="T2758" id="Seg_1126" s="T2757">pro.h:S</ta>
            <ta e="T2759" id="Seg_1127" s="T2758">v:pred</ta>
            <ta e="T2762" id="Seg_1128" s="T2761">v:pred 0.2.h:S</ta>
            <ta e="T2764" id="Seg_1129" s="T2763">v:pred 0.2.h:S</ta>
            <ta e="T2766" id="Seg_1130" s="T2765">ptcl.neg</ta>
            <ta e="T2767" id="Seg_1131" s="T2766">v:pred 0.2.h:S</ta>
            <ta e="T2769" id="Seg_1132" s="T2768">ptcl:pred</ta>
            <ta e="T2776" id="Seg_1133" s="T2775">np.h:S</ta>
            <ta e="T2777" id="Seg_1134" s="T2776">np:O</ta>
            <ta e="T2778" id="Seg_1135" s="T2777">v:pred</ta>
            <ta e="T2780" id="Seg_1136" s="T2779">conv:pred</ta>
            <ta e="T2781" id="Seg_1137" s="T2780">v:pred 0.3.h:S</ta>
            <ta e="T2782" id="Seg_1138" s="T2781">pro.h:S</ta>
            <ta e="T2783" id="Seg_1139" s="T2782">v:pred</ta>
            <ta e="T2786" id="Seg_1140" s="T2785">pro.h:S</ta>
            <ta e="T2788" id="Seg_1141" s="T2787">v:pred</ta>
            <ta e="T2789" id="Seg_1142" s="T2788">ptcl.neg</ta>
            <ta e="T2791" id="Seg_1143" s="T2790">ptcl.neg</ta>
            <ta e="T2792" id="Seg_1144" s="T2791">v:pred</ta>
            <ta e="T2793" id="Seg_1145" s="T2792">pro.h:S</ta>
            <ta e="T2794" id="Seg_1146" s="T2793">pro.h:O</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2708" id="Seg_1147" s="T2707">RUS:gram</ta>
            <ta e="T2709" id="Seg_1148" s="T2708">RUS:cult</ta>
            <ta e="T2710" id="Seg_1149" s="T2709">RUS:gram</ta>
            <ta e="T2711" id="Seg_1150" s="T2710">RUS:cult</ta>
            <ta e="T2714" id="Seg_1151" s="T2713">RUS:gram(INDEF)</ta>
            <ta e="T2717" id="Seg_1152" s="T2716">RUS:cult</ta>
            <ta e="T2719" id="Seg_1153" s="T2718">RUS:gram</ta>
            <ta e="T2722" id="Seg_1154" s="T2721">RUS:gram</ta>
            <ta e="T2726" id="Seg_1155" s="T2725">RUS:gram</ta>
            <ta e="T2727" id="Seg_1156" s="T2726">RUS:cult</ta>
            <ta e="T2732" id="Seg_1157" s="T2731">RUS:mod</ta>
            <ta e="T2736" id="Seg_1158" s="T2735">RUS:gram</ta>
            <ta e="T2749" id="Seg_1159" s="T2748">RUS:gram</ta>
            <ta e="T2750" id="Seg_1160" s="T2749">RUS:cult</ta>
            <ta e="T2757" id="Seg_1161" s="T2756">RUS:gram</ta>
            <ta e="T2763" id="Seg_1162" s="T2762">RUS:gram</ta>
            <ta e="T2765" id="Seg_1163" s="T2764">RUS:gram</ta>
            <ta e="T2769" id="Seg_1164" s="T2768">RUS:gram</ta>
            <ta e="T2775" id="Seg_1165" s="T2774">RUS:gram</ta>
            <ta e="T2776" id="Seg_1166" s="T2775">RUS:cult</ta>
            <ta e="T2779" id="Seg_1167" s="T2778">RUS:gram</ta>
            <ta e="T2787" id="Seg_1168" s="T2786">TURK:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T2773" id="Seg_1169" s="T2771">RUS:int.ins</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T2711" id="Seg_1170" s="T2704">Жили втроём медведь и волк и лиса.</ta>
            <ta e="T2715" id="Seg_1171" s="T2711">Потом пошли они кого-нибудь ловить.</ta>
            <ta e="T2718" id="Seg_1172" s="T2715">Пошёл волк.</ta>
            <ta e="T2723" id="Seg_1173" s="T2718">И овцу украл и принёс.</ta>
            <ta e="T2729" id="Seg_1174" s="T2723">Потом медведь и лисица пошли кормить [=есть].</ta>
            <ta e="T2735" id="Seg_1175" s="T2729">Медведь [говорит?]: «Ты зачем жир ешь?»</ta>
            <ta e="T2746" id="Seg_1176" s="T2735">А [лиса] говорит: «Когда он спросит, ты на меня посмотри».</ta>
            <ta e="T2748" id="Seg_1177" s="T2746">Потом они пришли.</ta>
            <ta e="T2756" id="Seg_1178" s="T2748">А волк говорит: «Куда жир делся?»</ta>
            <ta e="T2760" id="Seg_1179" s="T2756">А он [медведь] на неё [на лису] смотрит.</ta>
            <ta e="T2762" id="Seg_1180" s="T2760">[Лиса говорит:] «Ты что [на меня] смотришь?</ta>
            <ta e="T2767" id="Seg_1181" s="T2762">Когда ты ел, ты [на меня] не смотрел».</ta>
            <ta e="T2774" id="Seg_1182" s="T2767">Потом начали они, медведь с волком, бороться.</ta>
            <ta e="T2781" id="Seg_1183" s="T2774">А лисица взяла мясо и убежала.</ta>
            <ta e="T2785" id="Seg_1184" s="T2781">Они с ней [=за лисой] погнались.</ta>
            <ta e="T2794" id="Seg_1185" s="T2785">Она убежала, они её не поймали.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T2711" id="Seg_1186" s="T2704">[Once] there lived three [of them]: a bear and a wolf and a fox.</ta>
            <ta e="T2715" id="Seg_1187" s="T2711">Then they went to catch something.</ta>
            <ta e="T2718" id="Seg_1188" s="T2715">The wolf went.</ta>
            <ta e="T2723" id="Seg_1189" s="T2718">And he stole a sheep and brought it.</ta>
            <ta e="T2729" id="Seg_1190" s="T2723">Then the bear and the fox went to feed [=to eat].</ta>
            <ta e="T2735" id="Seg_1191" s="T2729">The bear [says?]: “Why do you eat fat?”</ta>
            <ta e="T2746" id="Seg_1192" s="T2735">And [the fox] says: "When he asks, look at me."</ta>
            <ta e="T2748" id="Seg_1193" s="T2746">Then they came.</ta>
            <ta e="T2756" id="Seg_1194" s="T2748">And the wolf says: "Where did the fat go?"</ta>
            <ta e="T2760" id="Seg_1195" s="T2756">And he [the bear] looks at him [the fox].</ta>
            <ta e="T2762" id="Seg_1196" s="T2760">[The fox says:] "Why are you looking [at me]?</ta>
            <ta e="T2767" id="Seg_1197" s="T2762">When you were eating, you did not look [at me]."</ta>
            <ta e="T2774" id="Seg_1198" s="T2767">Then they, the bear and the wolf started to fight.</ta>
            <ta e="T2781" id="Seg_1199" s="T2774">And the fox took the meat and left.</ta>
            <ta e="T2785" id="Seg_1200" s="T2781">They ran with him [=after the fox].</ta>
            <ta e="T2794" id="Seg_1201" s="T2785">It ran away, they did not catch it.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T2711" id="Seg_1202" s="T2704">Es lebte [einmal] drei [von ihnen]: ein Bär und ein Wolf und ein Fuchs.</ta>
            <ta e="T2715" id="Seg_1203" s="T2711">Dann gingen sie um etwas zu fangen.</ta>
            <ta e="T2718" id="Seg_1204" s="T2715">Der Wolf ging.</ta>
            <ta e="T2723" id="Seg_1205" s="T2718">Und er stahl ein Schaf und brachte es.</ta>
            <ta e="T2729" id="Seg_1206" s="T2723">Dann gingen der Bär und der Fuchs um zu füttern [=zu fressen].</ta>
            <ta e="T2735" id="Seg_1207" s="T2729">Der Bär [sagt?]: „Warum frisst du Fett?“</ta>
            <ta e="T2746" id="Seg_1208" s="T2735">Und [der Fuchs] sagt: „Wenn er fragt, schau mich an.“</ta>
            <ta e="T2748" id="Seg_1209" s="T2746">Dann kamen sie.</ta>
            <ta e="T2756" id="Seg_1210" s="T2748">Und der Wolf sagt: „Wo ist das Fett hin?“</ta>
            <ta e="T2760" id="Seg_1211" s="T2756">Und er [der Bär] schaut auf ihn [den Fuchs].</ta>
            <ta e="T2762" id="Seg_1212" s="T2760">[Der Fuch sagt:] „Warum schaust du [auf mich]?</ta>
            <ta e="T2767" id="Seg_1213" s="T2762">Als du am Fressen warst, hast du [mich] nicht angeschaut.“</ta>
            <ta e="T2774" id="Seg_1214" s="T2767">Dann fingen sie an, der Bär und der Wolf, zu kämpfen.</ta>
            <ta e="T2781" id="Seg_1215" s="T2774">Und der Fuchs nahm das Fleisch und ging.</ta>
            <ta e="T2785" id="Seg_1216" s="T2781">Sie liefen mit ihn [=nach dem Fuchs].</ta>
            <ta e="T2794" id="Seg_1217" s="T2785">Er rannte weg, sie fingen ihn nicht.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T2711" id="Seg_1218" s="T2704">[AAV] See https://www.calameo.com/read/000463903b23cbc0e124c, p. 29 (tale no. 12)</ta>
            <ta e="T2729" id="Seg_1219" s="T2723">[KlT:] why the transitive?</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T2704" />
            <conversion-tli id="T2705" />
            <conversion-tli id="T2706" />
            <conversion-tli id="T2707" />
            <conversion-tli id="T2708" />
            <conversion-tli id="T2709" />
            <conversion-tli id="T2710" />
            <conversion-tli id="T2711" />
            <conversion-tli id="T2712" />
            <conversion-tli id="T2713" />
            <conversion-tli id="T2714" />
            <conversion-tli id="T2715" />
            <conversion-tli id="T2716" />
            <conversion-tli id="T2717" />
            <conversion-tli id="T2718" />
            <conversion-tli id="T2719" />
            <conversion-tli id="T2720" />
            <conversion-tli id="T2721" />
            <conversion-tli id="T2722" />
            <conversion-tli id="T2723" />
            <conversion-tli id="T2724" />
            <conversion-tli id="T2725" />
            <conversion-tli id="T2726" />
            <conversion-tli id="T2727" />
            <conversion-tli id="T2728" />
            <conversion-tli id="T2729" />
            <conversion-tli id="T2730" />
            <conversion-tli id="T2731" />
            <conversion-tli id="T2732" />
            <conversion-tli id="T2733" />
            <conversion-tli id="T2734" />
            <conversion-tli id="T2735" />
            <conversion-tli id="T2736" />
            <conversion-tli id="T2737" />
            <conversion-tli id="T2738" />
            <conversion-tli id="T2739" />
            <conversion-tli id="T2740" />
            <conversion-tli id="T2741" />
            <conversion-tli id="T2742" />
            <conversion-tli id="T2743" />
            <conversion-tli id="T2744" />
            <conversion-tli id="T2745" />
            <conversion-tli id="T2746" />
            <conversion-tli id="T2747" />
            <conversion-tli id="T2748" />
            <conversion-tli id="T2749" />
            <conversion-tli id="T2750" />
            <conversion-tli id="T2751" />
            <conversion-tli id="T2752" />
            <conversion-tli id="T2753" />
            <conversion-tli id="T2754" />
            <conversion-tli id="T2755" />
            <conversion-tli id="T2756" />
            <conversion-tli id="T2757" />
            <conversion-tli id="T2758" />
            <conversion-tli id="T2759" />
            <conversion-tli id="T2760" />
            <conversion-tli id="T2761" />
            <conversion-tli id="T2762" />
            <conversion-tli id="T2763" />
            <conversion-tli id="T2764" />
            <conversion-tli id="T2765" />
            <conversion-tli id="T2766" />
            <conversion-tli id="T2767" />
            <conversion-tli id="T2768" />
            <conversion-tli id="T2769" />
            <conversion-tli id="T2770" />
            <conversion-tli id="T2771" />
            <conversion-tli id="T2772" />
            <conversion-tli id="T2773" />
            <conversion-tli id="T2774" />
            <conversion-tli id="T2775" />
            <conversion-tli id="T2776" />
            <conversion-tli id="T2777" />
            <conversion-tli id="T2778" />
            <conversion-tli id="T2779" />
            <conversion-tli id="T2780" />
            <conversion-tli id="T2781" />
            <conversion-tli id="T2782" />
            <conversion-tli id="T2783" />
            <conversion-tli id="T2784" />
            <conversion-tli id="T2785" />
            <conversion-tli id="T2786" />
            <conversion-tli id="T2787" />
            <conversion-tli id="T2788" />
            <conversion-tli id="T2789" />
            <conversion-tli id="T2790" />
            <conversion-tli id="T2791" />
            <conversion-tli id="T2792" />
            <conversion-tli id="T2793" />
            <conversion-tli id="T2794" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
