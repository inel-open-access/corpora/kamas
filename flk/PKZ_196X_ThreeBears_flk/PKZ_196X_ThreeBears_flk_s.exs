<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID300B3384-C325-369D-DBA2-BC37CEFDAAA7">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_ThreeBears_flk.wav" />
         <referenced-file url="PKZ_196X_ThreeBears_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_ThreeBears_flk\PKZ_196X_ThreeBears_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">213</ud-information>
            <ud-information attribute-name="# HIAT:w">136</ud-information>
            <ud-information attribute-name="# e">136</ud-information>
            <ud-information attribute-name="# HIAT:u">30</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.043" type="appl" />
         <tli id="T1" time="0.745" type="appl" />
         <tli id="T2" time="1.446" type="appl" />
         <tli id="T3" time="2.148" type="appl" />
         <tli id="T4" time="2.85" type="appl" />
         <tli id="T5" time="3.946" type="appl" />
         <tli id="T6" time="5.042" type="appl" />
         <tli id="T7" time="6.137" type="appl" />
         <tli id="T8" time="7.233" type="appl" />
         <tli id="T9" time="8.329" type="appl" />
         <tli id="T10" time="8.898" type="appl" />
         <tli id="T11" time="9.468" type="appl" />
         <tli id="T12" time="10.038" type="appl" />
         <tli id="T13" time="10.607" type="appl" />
         <tli id="T14" time="11.342" type="appl" />
         <tli id="T15" time="12.076" type="appl" />
         <tli id="T16" time="12.982" type="appl" />
         <tli id="T17" time="13.7" type="appl" />
         <tli id="T18" time="14.418" type="appl" />
         <tli id="T19" time="15.136" type="appl" />
         <tli id="T20" time="16.0457686734213" />
         <tli id="T21" time="16.596" type="appl" />
         <tli id="T22" time="17.111" type="appl" />
         <tli id="T23" time="17.625" type="appl" />
         <tli id="T24" time="18.14" type="appl" />
         <tli id="T25" time="19.014" type="appl" />
         <tli id="T26" time="19.888" type="appl" />
         <tli id="T27" time="20.88" type="appl" />
         <tli id="T28" time="21.745" type="appl" />
         <tli id="T29" time="22.343" type="appl" />
         <tli id="T30" time="22.94" type="appl" />
         <tli id="T31" time="23.538" type="appl" />
         <tli id="T32" time="24.135" type="appl" />
         <tli id="T33" time="24.733" type="appl" />
         <tli id="T34" time="25.736" type="appl" />
         <tli id="T35" time="26.47" type="appl" />
         <tli id="T36" time="27.205" type="appl" />
         <tli id="T37" time="28.285083706409047" />
         <tli id="T38" time="29.237" type="appl" />
         <tli id="T39" time="30.044" type="appl" />
         <tli id="T40" time="30.85" type="appl" />
         <tli id="T41" time="31.657" type="appl" />
         <tli id="T42" time="32.723" type="appl" />
         <tli id="T43" time="33.596" type="appl" />
         <tli id="T44" time="34.47" type="appl" />
         <tli id="T45" time="34.994" type="appl" />
         <tli id="T46" time="35.518" type="appl" />
         <tli id="T47" time="36.042" type="appl" />
         <tli id="T48" time="37.146" type="appl" />
         <tli id="T49" time="38.25" type="appl" />
         <tli id="T50" time="39.354" type="appl" />
         <tli id="T51" time="40.458" type="appl" />
         <tli id="T52" time="41.562" type="appl" />
         <tli id="T53" time="42.666" type="appl" />
         <tli id="T54" time="43.077" type="appl" />
         <tli id="T55" time="43.488" type="appl" />
         <tli id="T56" time="44.276" type="appl" />
         <tli id="T57" time="45.065" type="appl" />
         <tli id="T58" time="45.853" type="appl" />
         <tli id="T59" time="46.642" type="appl" />
         <tli id="T60" time="47.43" type="appl" />
         <tli id="T61" time="48.013" type="appl" />
         <tli id="T62" time="48.596" type="appl" />
         <tli id="T63" time="49.18" type="appl" />
         <tli id="T64" time="49.763" type="appl" />
         <tli id="T65" time="50.346" type="appl" />
         <tli id="T66" time="51.62377756833176" />
         <tli id="T67" time="52.67" type="appl" />
         <tli id="T68" time="53.709" type="appl" />
         <tli id="T69" time="54.747" type="appl" />
         <tli id="T70" time="55.786" type="appl" />
         <tli id="T71" time="56.824" type="appl" />
         <tli id="T72" time="57.706" type="appl" />
         <tli id="T73" time="58.393" type="appl" />
         <tli id="T74" time="59.079" type="appl" />
         <tli id="T75" time="59.766" type="appl" />
         <tli id="T76" time="60.74993349434496" />
         <tli id="T77" time="61.532" type="appl" />
         <tli id="T78" time="62.188" type="appl" />
         <tli id="T79" time="62.844" type="appl" />
         <tli id="T80" time="63.501" type="appl" />
         <tli id="T81" time="64.157" type="appl" />
         <tli id="T82" time="64.813" type="appl" />
         <tli id="T83" time="65.76965256833176" />
         <tli id="T84" time="67.574" type="appl" />
         <tli id="T85" time="69.324" type="appl" />
         <tli id="T86" time="71.075" type="appl" />
         <tli id="T87" time="72.825" type="appl" />
         <tli id="T88" time="74.575" type="appl" />
         <tli id="T89" time="75.939" type="appl" />
         <tli id="T90" time="77.303" type="appl" />
         <tli id="T91" time="78.667" type="appl" />
         <tli id="T92" time="79.421" type="appl" />
         <tli id="T93" time="80.151" type="appl" />
         <tli id="T94" time="80.882" type="appl" />
         <tli id="T95" time="81.612" type="appl" />
         <tli id="T96" time="82.342" type="appl" />
         <tli id="T97" time="83.24200807021677" />
         <tli id="T98" time="84.214" type="appl" />
         <tli id="T99" time="85.292" type="appl" />
         <tli id="T100" time="86.37" type="appl" />
         <tli id="T101" time="87.108" type="appl" />
         <tli id="T102" time="87.846" type="appl" />
         <tli id="T103" time="88.584" type="appl" />
         <tli id="T104" time="89.323" type="appl" />
         <tli id="T105" time="90.061" type="appl" />
         <tli id="T106" time="91.2015626178134" />
         <tli id="T107" time="92.413" type="appl" />
         <tli id="T108" time="93.355" type="appl" />
         <tli id="T109" time="94.296" type="appl" />
         <tli id="T110" time="95.238" type="appl" />
         <tli id="T111" time="96.179" type="appl" />
         <tli id="T112" time="97.121" type="appl" />
         <tli id="T113" time="98.062" type="appl" />
         <tli id="T114" time="99.004" type="appl" />
         <tli id="T115" time="99.945" type="appl" />
         <tli id="T116" time="100.53" type="appl" />
         <tli id="T117" time="101.115" type="appl" />
         <tli id="T118" time="101.7" type="appl" />
         <tli id="T119" time="102.249" type="appl" />
         <tli id="T120" time="102.797" type="appl" />
         <tli id="T121" time="103.346" type="appl" />
         <tli id="T122" time="103.895" type="appl" />
         <tli id="T123" time="104.443" type="appl" />
         <tli id="T124" time="105.16078139726673" />
         <tli id="T125" time="105.979" type="appl" />
         <tli id="T126" time="106.586" type="appl" />
         <tli id="T127" time="107.192" type="appl" />
         <tli id="T128" time="107.799" type="appl" />
         <tli id="T129" time="108.80057769792649" />
         <tli id="T130" time="109.775" type="appl" />
         <tli id="T131" time="110.406" type="appl" />
         <tli id="T132" time="111.037" type="appl" />
         <tli id="T133" time="111.668" type="appl" />
         <tli id="T134" time="112.222" type="appl" />
         <tli id="T135" time="112.694" type="appl" />
         <tli id="T136" time="113.167" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T136" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Amnobiʔi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">nüke</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">da</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">büzʼe</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_17" n="HIAT:u" s="T4">
                  <nts id="Seg_18" n="HIAT:ip">(</nts>
                  <ts e="T5" id="Seg_20" n="HIAT:w" s="T4">Dĭn=</ts>
                  <nts id="Seg_21" n="HIAT:ip">)</nts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_24" n="HIAT:w" s="T5">Dĭzeŋgən</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_27" n="HIAT:w" s="T6">koʔbdo</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_30" n="HIAT:w" s="T7">ibi</ts>
                  <nts id="Seg_31" n="HIAT:ip">,</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">vnučka</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_38" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_40" n="HIAT:w" s="T9">Dĭ</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_42" n="HIAT:ip">(</nts>
                  <ts e="T11" id="Seg_44" n="HIAT:w" s="T10">ka-</ts>
                  <nts id="Seg_45" n="HIAT:ip">)</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_48" n="HIAT:w" s="T11">kambi</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_51" n="HIAT:w" s="T12">keʔbdejle</ts>
                  <nts id="Seg_52" n="HIAT:ip">.</nts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_55" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_57" n="HIAT:w" s="T13">I</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_60" n="HIAT:w" s="T14">dʼürlambi</ts>
                  <nts id="Seg_61" n="HIAT:ip">.</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_64" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_66" n="HIAT:w" s="T15">Mĭmbi</ts>
                  <nts id="Seg_67" n="HIAT:ip">,</nts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_70" n="HIAT:w" s="T16">mĭmbi</ts>
                  <nts id="Seg_71" n="HIAT:ip">,</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_74" n="HIAT:w" s="T17">šobi</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_77" n="HIAT:w" s="T18">urgaːbanə</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_80" n="HIAT:w" s="T19">maːʔndə</ts>
                  <nts id="Seg_81" n="HIAT:ip">.</nts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_84" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_86" n="HIAT:w" s="T20">Kuliat</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_89" n="HIAT:w" s="T21">bar:</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_92" n="HIAT:w" s="T22">stol</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_95" n="HIAT:w" s="T23">nuga</ts>
                  <nts id="Seg_96" n="HIAT:ip">.</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_99" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_101" n="HIAT:w" s="T24">Stuləʔi</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_104" n="HIAT:w" s="T25">nugaʔi</ts>
                  <nts id="Seg_105" n="HIAT:ip">.</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_108" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_110" n="HIAT:w" s="T26">Takšəʔi</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_113" n="HIAT:w" s="T27">nugaʔi</ts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_117" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_119" n="HIAT:w" s="T28">Onʼiʔ</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_122" n="HIAT:w" s="T29">takšə</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_125" n="HIAT:w" s="T30">urgo</ts>
                  <nts id="Seg_126" n="HIAT:ip">,</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_129" n="HIAT:w" s="T31">onʼiʔ</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_132" n="HIAT:w" s="T32">üdʼüge</ts>
                  <nts id="Seg_133" n="HIAT:ip">.</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_136" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_138" n="HIAT:w" s="T33">A</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_141" n="HIAT:w" s="T34">nagur</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_144" n="HIAT:w" s="T35">takšə</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_146" n="HIAT:ip">(</nts>
                  <ts e="T37" id="Seg_148" n="HIAT:w" s="T36">idʼiʔeʔe</ts>
                  <nts id="Seg_149" n="HIAT:ip">)</nts>
                  <nts id="Seg_150" n="HIAT:ip">.</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_153" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_155" n="HIAT:w" s="T37">Tĭ</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_158" n="HIAT:w" s="T38">amnobi</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_161" n="HIAT:w" s="T39">urgo</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_164" n="HIAT:w" s="T40">takšənə</ts>
                  <nts id="Seg_165" n="HIAT:ip">.</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_168" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_170" n="HIAT:w" s="T41">Šalaʔje</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_172" n="HIAT:ip">(</nts>
                  <ts e="T43" id="Seg_174" n="HIAT:w" s="T42">ambi=</ts>
                  <nts id="Seg_175" n="HIAT:ip">)</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_178" n="HIAT:w" s="T43">ambi</ts>
                  <nts id="Seg_179" n="HIAT:ip">.</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_182" n="HIAT:u" s="T44">
                  <nts id="Seg_183" n="HIAT:ip">(</nts>
                  <ts e="T45" id="Seg_185" n="HIAT:w" s="T44">Ĭmbidə</ts>
                  <nts id="Seg_186" n="HIAT:ip">)</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_189" n="HIAT:w" s="T45">ej</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_192" n="HIAT:w" s="T46">jakše</ts>
                  <nts id="Seg_193" n="HIAT:ip">.</nts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_196" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_198" n="HIAT:w" s="T47">Dĭgəttə</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_200" n="HIAT:ip">(</nts>
                  <ts e="T49" id="Seg_202" n="HIAT:w" s="T48">šidögöndəjʔ</ts>
                  <nts id="Seg_203" n="HIAT:ip">)</nts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_206" n="HIAT:w" s="T49">amnobi</ts>
                  <nts id="Seg_207" n="HIAT:ip">,</nts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_210" n="HIAT:w" s="T50">tože</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_212" n="HIAT:ip">(</nts>
                  <ts e="T52" id="Seg_214" n="HIAT:w" s="T51">šaʔlaʔje</ts>
                  <nts id="Seg_215" n="HIAT:ip">)</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_218" n="HIAT:w" s="T52">ambi</ts>
                  <nts id="Seg_219" n="HIAT:ip">.</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_222" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_224" n="HIAT:w" s="T53">Ej</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_227" n="HIAT:w" s="T54">jakše</ts>
                  <nts id="Seg_228" n="HIAT:ip">.</nts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_231" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_233" n="HIAT:w" s="T55">Dĭgəttə</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_236" n="HIAT:w" s="T56">nagurdə</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_239" n="HIAT:w" s="T57">ambi</ts>
                  <nts id="Seg_240" n="HIAT:ip">,</nts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_243" n="HIAT:w" s="T58">üdʼüge</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_246" n="HIAT:w" s="T59">takšənə</ts>
                  <nts id="Seg_247" n="HIAT:ip">.</nts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_250" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_252" n="HIAT:w" s="T60">Dön</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_255" n="HIAT:w" s="T61">jakše</ts>
                  <nts id="Seg_256" n="HIAT:ip">,</nts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_259" n="HIAT:w" s="T62">ambi</ts>
                  <nts id="Seg_260" n="HIAT:ip">,</nts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_263" n="HIAT:w" s="T63">ambi</ts>
                  <nts id="Seg_264" n="HIAT:ip">,</nts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_267" n="HIAT:w" s="T64">dĭgəttə</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_270" n="HIAT:w" s="T65">kambi</ts>
                  <nts id="Seg_271" n="HIAT:ip">.</nts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_274" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_276" n="HIAT:w" s="T66">Onʼiʔ</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_279" n="HIAT:w" s="T67">krăvatʼtə</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_281" n="HIAT:ip">(</nts>
                  <ts e="T69" id="Seg_283" n="HIAT:w" s="T68">ur-</ts>
                  <nts id="Seg_284" n="HIAT:ip">)</nts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_287" n="HIAT:w" s="T69">iʔbəbi</ts>
                  <nts id="Seg_288" n="HIAT:ip">—</nts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_291" n="HIAT:w" s="T70">urgo</ts>
                  <nts id="Seg_292" n="HIAT:ip">.</nts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_295" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_297" n="HIAT:w" s="T71">Dĭgəttə</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_300" n="HIAT:w" s="T72">šide</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_303" n="HIAT:w" s="T73">krăvatʼtə</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_306" n="HIAT:w" s="T74">iʔbəbi</ts>
                  <nts id="Seg_307" n="HIAT:ip">—</nts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_310" n="HIAT:w" s="T75">urgo</ts>
                  <nts id="Seg_311" n="HIAT:ip">.</nts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_314" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_316" n="HIAT:w" s="T76">Dĭgəttə</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_319" n="HIAT:w" s="T77">nagur</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_322" n="HIAT:w" s="T78">krăvatʼtə</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_324" n="HIAT:ip">(</nts>
                  <ts e="T80" id="Seg_326" n="HIAT:w" s="T79">iʔbəbi</ts>
                  <nts id="Seg_327" n="HIAT:ip">)</nts>
                  <nts id="Seg_328" n="HIAT:ip">—</nts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_331" n="HIAT:w" s="T80">dön</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_334" n="HIAT:w" s="T81">jakše</ts>
                  <nts id="Seg_335" n="HIAT:ip">,</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_338" n="HIAT:w" s="T82">üdʼüge</ts>
                  <nts id="Seg_339" n="HIAT:ip">.</nts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_342" n="HIAT:u" s="T83">
                  <nts id="Seg_343" n="HIAT:ip">(</nts>
                  <ts e="T84" id="Seg_345" n="HIAT:w" s="T83">I</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_348" n="HIAT:w" s="T84">ko-</ts>
                  <nts id="Seg_349" n="HIAT:ip">)</nts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_352" n="HIAT:w" s="T85">I</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_354" n="HIAT:ip">(</nts>
                  <ts e="T87" id="Seg_356" n="HIAT:w" s="T86">kuno-</ts>
                  <nts id="Seg_357" n="HIAT:ip">)</nts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_360" n="HIAT:w" s="T87">kunollaʔbə</ts>
                  <nts id="Seg_361" n="HIAT:ip">.</nts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_364" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_366" n="HIAT:w" s="T88">Dĭgəttə</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_369" n="HIAT:w" s="T89">urgaːbaʔi</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_372" n="HIAT:w" s="T90">šobiʔi</ts>
                  <nts id="Seg_373" n="HIAT:ip">.</nts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_376" n="HIAT:u" s="T91">
                  <nts id="Seg_377" n="HIAT:ip">"</nts>
                  <ts e="T92" id="Seg_379" n="HIAT:w" s="T91">Šindi</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_382" n="HIAT:w" s="T92">amnəbi</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_385" n="HIAT:w" s="T93">măn</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_388" n="HIAT:w" s="T94">takšənə</ts>
                  <nts id="Seg_389" n="HIAT:ip">"</nts>
                  <nts id="Seg_390" n="HIAT:ip">,</nts>
                  <nts id="Seg_391" n="HIAT:ip">—</nts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_394" n="HIAT:w" s="T95">bar</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_397" n="HIAT:w" s="T96">kirgarlaʔbəʔjə</ts>
                  <nts id="Seg_398" n="HIAT:ip">.</nts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_401" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_403" n="HIAT:w" s="T97">Dĭgəttə</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_406" n="HIAT:w" s="T98">kambiʔi</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_409" n="HIAT:w" s="T99">kunolzittə</ts>
                  <nts id="Seg_410" n="HIAT:ip">.</nts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_413" n="HIAT:u" s="T100">
                  <nts id="Seg_414" n="HIAT:ip">"</nts>
                  <ts e="T101" id="Seg_416" n="HIAT:w" s="T100">Šindi</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_419" n="HIAT:w" s="T101">măn</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_422" n="HIAT:w" s="T102">krăvatʼtən</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_425" n="HIAT:w" s="T103">bar</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_428" n="HIAT:w" s="T104">dön</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_431" n="HIAT:w" s="T105">iʔbolaʔpi</ts>
                  <nts id="Seg_432" n="HIAT:ip">?</nts>
                  <nts id="Seg_433" n="HIAT:ip">"</nts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_436" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_438" n="HIAT:w" s="T106">A</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_441" n="HIAT:w" s="T107">dĭ</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_444" n="HIAT:w" s="T108">üdʼüge</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_447" n="HIAT:w" s="T109">urgaːba</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_450" n="HIAT:w" s="T110">bar</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_452" n="HIAT:ip">(</nts>
                  <ts e="T112" id="Seg_454" n="HIAT:w" s="T111">to-</ts>
                  <nts id="Seg_455" n="HIAT:ip">)</nts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_458" n="HIAT:w" s="T112">todam</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_461" n="HIAT:w" s="T113">bar</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_464" n="HIAT:w" s="T114">kirgarlaʔbə:</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_466" n="HIAT:ip">"</nts>
                  <ts e="T116" id="Seg_468" n="HIAT:w" s="T115">Šindidə</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_471" n="HIAT:w" s="T116">dön</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_474" n="HIAT:w" s="T117">iʔbolaʔbə</ts>
                  <nts id="Seg_475" n="HIAT:ip">!</nts>
                  <nts id="Seg_476" n="HIAT:ip">"</nts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_479" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_481" n="HIAT:w" s="T118">Dĭ</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_483" n="HIAT:ip">(</nts>
                  <ts e="T120" id="Seg_485" n="HIAT:w" s="T119">š-</ts>
                  <nts id="Seg_486" n="HIAT:ip">)</nts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_489" n="HIAT:w" s="T120">suʔmiluʔpi</ts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_492" n="HIAT:w" s="T121">da</ts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_495" n="HIAT:w" s="T122">kuznektə</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_498" n="HIAT:w" s="T123">üʔmiluʔpi</ts>
                  <nts id="Seg_499" n="HIAT:ip">.</nts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T129" id="Seg_502" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_504" n="HIAT:w" s="T124">I</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_507" n="HIAT:w" s="T125">šobi</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_510" n="HIAT:w" s="T126">babuškanə</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_513" n="HIAT:w" s="T127">da</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_516" n="HIAT:w" s="T128">deduška</ts>
                  <nts id="Seg_517" n="HIAT:ip">.</nts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_520" n="HIAT:u" s="T129">
                  <ts e="T130" id="Seg_522" n="HIAT:w" s="T129">Büzʼenə</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_525" n="HIAT:w" s="T130">da</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_528" n="HIAT:w" s="T131">nükenə</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_531" n="HIAT:w" s="T132">šobi</ts>
                  <nts id="Seg_532" n="HIAT:ip">.</nts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_535" n="HIAT:u" s="T133">
                  <ts e="T134" id="Seg_537" n="HIAT:w" s="T133">I</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_540" n="HIAT:w" s="T134">bazoʔ</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_543" n="HIAT:w" s="T135">amnobiʔi</ts>
                  <nts id="Seg_544" n="HIAT:ip">.</nts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T136" id="Seg_546" n="sc" s="T0">
               <ts e="T1" id="Seg_548" n="e" s="T0">Amnobiʔi </ts>
               <ts e="T2" id="Seg_550" n="e" s="T1">nüke </ts>
               <ts e="T3" id="Seg_552" n="e" s="T2">da </ts>
               <ts e="T4" id="Seg_554" n="e" s="T3">büzʼe. </ts>
               <ts e="T5" id="Seg_556" n="e" s="T4">(Dĭn=) </ts>
               <ts e="T6" id="Seg_558" n="e" s="T5">Dĭzeŋgən </ts>
               <ts e="T7" id="Seg_560" n="e" s="T6">koʔbdo </ts>
               <ts e="T8" id="Seg_562" n="e" s="T7">ibi, </ts>
               <ts e="T9" id="Seg_564" n="e" s="T8">vnučka. </ts>
               <ts e="T10" id="Seg_566" n="e" s="T9">Dĭ </ts>
               <ts e="T11" id="Seg_568" n="e" s="T10">(ka-) </ts>
               <ts e="T12" id="Seg_570" n="e" s="T11">kambi </ts>
               <ts e="T13" id="Seg_572" n="e" s="T12">keʔbdejle. </ts>
               <ts e="T14" id="Seg_574" n="e" s="T13">I </ts>
               <ts e="T15" id="Seg_576" n="e" s="T14">dʼürlambi. </ts>
               <ts e="T16" id="Seg_578" n="e" s="T15">Mĭmbi, </ts>
               <ts e="T17" id="Seg_580" n="e" s="T16">mĭmbi, </ts>
               <ts e="T18" id="Seg_582" n="e" s="T17">šobi </ts>
               <ts e="T19" id="Seg_584" n="e" s="T18">urgaːbanə </ts>
               <ts e="T20" id="Seg_586" n="e" s="T19">maːʔndə. </ts>
               <ts e="T21" id="Seg_588" n="e" s="T20">Kuliat </ts>
               <ts e="T22" id="Seg_590" n="e" s="T21">bar: </ts>
               <ts e="T23" id="Seg_592" n="e" s="T22">stol </ts>
               <ts e="T24" id="Seg_594" n="e" s="T23">nuga. </ts>
               <ts e="T25" id="Seg_596" n="e" s="T24">Stuləʔi </ts>
               <ts e="T26" id="Seg_598" n="e" s="T25">nugaʔi. </ts>
               <ts e="T27" id="Seg_600" n="e" s="T26">Takšəʔi </ts>
               <ts e="T28" id="Seg_602" n="e" s="T27">nugaʔi. </ts>
               <ts e="T29" id="Seg_604" n="e" s="T28">Onʼiʔ </ts>
               <ts e="T30" id="Seg_606" n="e" s="T29">takšə </ts>
               <ts e="T31" id="Seg_608" n="e" s="T30">urgo, </ts>
               <ts e="T32" id="Seg_610" n="e" s="T31">onʼiʔ </ts>
               <ts e="T33" id="Seg_612" n="e" s="T32">üdʼüge. </ts>
               <ts e="T34" id="Seg_614" n="e" s="T33">A </ts>
               <ts e="T35" id="Seg_616" n="e" s="T34">nagur </ts>
               <ts e="T36" id="Seg_618" n="e" s="T35">takšə </ts>
               <ts e="T37" id="Seg_620" n="e" s="T36">(idʼiʔeʔe). </ts>
               <ts e="T38" id="Seg_622" n="e" s="T37">Tĭ </ts>
               <ts e="T39" id="Seg_624" n="e" s="T38">amnobi </ts>
               <ts e="T40" id="Seg_626" n="e" s="T39">urgo </ts>
               <ts e="T41" id="Seg_628" n="e" s="T40">takšənə. </ts>
               <ts e="T42" id="Seg_630" n="e" s="T41">Šalaʔje </ts>
               <ts e="T43" id="Seg_632" n="e" s="T42">(ambi=) </ts>
               <ts e="T44" id="Seg_634" n="e" s="T43">ambi. </ts>
               <ts e="T45" id="Seg_636" n="e" s="T44">(Ĭmbidə) </ts>
               <ts e="T46" id="Seg_638" n="e" s="T45">ej </ts>
               <ts e="T47" id="Seg_640" n="e" s="T46">jakše. </ts>
               <ts e="T48" id="Seg_642" n="e" s="T47">Dĭgəttə </ts>
               <ts e="T49" id="Seg_644" n="e" s="T48">(šidögöndəjʔ) </ts>
               <ts e="T50" id="Seg_646" n="e" s="T49">amnobi, </ts>
               <ts e="T51" id="Seg_648" n="e" s="T50">tože </ts>
               <ts e="T52" id="Seg_650" n="e" s="T51">(šaʔlaʔje) </ts>
               <ts e="T53" id="Seg_652" n="e" s="T52">ambi. </ts>
               <ts e="T54" id="Seg_654" n="e" s="T53">Ej </ts>
               <ts e="T55" id="Seg_656" n="e" s="T54">jakše. </ts>
               <ts e="T56" id="Seg_658" n="e" s="T55">Dĭgəttə </ts>
               <ts e="T57" id="Seg_660" n="e" s="T56">nagurdə </ts>
               <ts e="T58" id="Seg_662" n="e" s="T57">ambi, </ts>
               <ts e="T59" id="Seg_664" n="e" s="T58">üdʼüge </ts>
               <ts e="T60" id="Seg_666" n="e" s="T59">takšənə. </ts>
               <ts e="T61" id="Seg_668" n="e" s="T60">Dön </ts>
               <ts e="T62" id="Seg_670" n="e" s="T61">jakše, </ts>
               <ts e="T63" id="Seg_672" n="e" s="T62">ambi, </ts>
               <ts e="T64" id="Seg_674" n="e" s="T63">ambi, </ts>
               <ts e="T65" id="Seg_676" n="e" s="T64">dĭgəttə </ts>
               <ts e="T66" id="Seg_678" n="e" s="T65">kambi. </ts>
               <ts e="T67" id="Seg_680" n="e" s="T66">Onʼiʔ </ts>
               <ts e="T68" id="Seg_682" n="e" s="T67">krăvatʼtə </ts>
               <ts e="T69" id="Seg_684" n="e" s="T68">(ur-) </ts>
               <ts e="T70" id="Seg_686" n="e" s="T69">iʔbəbi— </ts>
               <ts e="T71" id="Seg_688" n="e" s="T70">urgo. </ts>
               <ts e="T72" id="Seg_690" n="e" s="T71">Dĭgəttə </ts>
               <ts e="T73" id="Seg_692" n="e" s="T72">šide </ts>
               <ts e="T74" id="Seg_694" n="e" s="T73">krăvatʼtə </ts>
               <ts e="T75" id="Seg_696" n="e" s="T74">iʔbəbi— </ts>
               <ts e="T76" id="Seg_698" n="e" s="T75">urgo. </ts>
               <ts e="T77" id="Seg_700" n="e" s="T76">Dĭgəttə </ts>
               <ts e="T78" id="Seg_702" n="e" s="T77">nagur </ts>
               <ts e="T79" id="Seg_704" n="e" s="T78">krăvatʼtə </ts>
               <ts e="T80" id="Seg_706" n="e" s="T79">(iʔbəbi)— </ts>
               <ts e="T81" id="Seg_708" n="e" s="T80">dön </ts>
               <ts e="T82" id="Seg_710" n="e" s="T81">jakše, </ts>
               <ts e="T83" id="Seg_712" n="e" s="T82">üdʼüge. </ts>
               <ts e="T84" id="Seg_714" n="e" s="T83">(I </ts>
               <ts e="T85" id="Seg_716" n="e" s="T84">ko-) </ts>
               <ts e="T86" id="Seg_718" n="e" s="T85">I </ts>
               <ts e="T87" id="Seg_720" n="e" s="T86">(kuno-) </ts>
               <ts e="T88" id="Seg_722" n="e" s="T87">kunollaʔbə. </ts>
               <ts e="T89" id="Seg_724" n="e" s="T88">Dĭgəttə </ts>
               <ts e="T90" id="Seg_726" n="e" s="T89">urgaːbaʔi </ts>
               <ts e="T91" id="Seg_728" n="e" s="T90">šobiʔi. </ts>
               <ts e="T92" id="Seg_730" n="e" s="T91">"Šindi </ts>
               <ts e="T93" id="Seg_732" n="e" s="T92">amnəbi </ts>
               <ts e="T94" id="Seg_734" n="e" s="T93">măn </ts>
               <ts e="T95" id="Seg_736" n="e" s="T94">takšənə",— </ts>
               <ts e="T96" id="Seg_738" n="e" s="T95">bar </ts>
               <ts e="T97" id="Seg_740" n="e" s="T96">kirgarlaʔbəʔjə. </ts>
               <ts e="T98" id="Seg_742" n="e" s="T97">Dĭgəttə </ts>
               <ts e="T99" id="Seg_744" n="e" s="T98">kambiʔi </ts>
               <ts e="T100" id="Seg_746" n="e" s="T99">kunolzittə. </ts>
               <ts e="T101" id="Seg_748" n="e" s="T100">"Šindi </ts>
               <ts e="T102" id="Seg_750" n="e" s="T101">măn </ts>
               <ts e="T103" id="Seg_752" n="e" s="T102">krăvatʼtən </ts>
               <ts e="T104" id="Seg_754" n="e" s="T103">bar </ts>
               <ts e="T105" id="Seg_756" n="e" s="T104">dön </ts>
               <ts e="T106" id="Seg_758" n="e" s="T105">iʔbolaʔpi?" </ts>
               <ts e="T107" id="Seg_760" n="e" s="T106">A </ts>
               <ts e="T108" id="Seg_762" n="e" s="T107">dĭ </ts>
               <ts e="T109" id="Seg_764" n="e" s="T108">üdʼüge </ts>
               <ts e="T110" id="Seg_766" n="e" s="T109">urgaːba </ts>
               <ts e="T111" id="Seg_768" n="e" s="T110">bar </ts>
               <ts e="T112" id="Seg_770" n="e" s="T111">(to-) </ts>
               <ts e="T113" id="Seg_772" n="e" s="T112">todam </ts>
               <ts e="T114" id="Seg_774" n="e" s="T113">bar </ts>
               <ts e="T115" id="Seg_776" n="e" s="T114">kirgarlaʔbə: </ts>
               <ts e="T116" id="Seg_778" n="e" s="T115">"Šindidə </ts>
               <ts e="T117" id="Seg_780" n="e" s="T116">dön </ts>
               <ts e="T118" id="Seg_782" n="e" s="T117">iʔbolaʔbə!" </ts>
               <ts e="T119" id="Seg_784" n="e" s="T118">Dĭ </ts>
               <ts e="T120" id="Seg_786" n="e" s="T119">(š-) </ts>
               <ts e="T121" id="Seg_788" n="e" s="T120">suʔmiluʔpi </ts>
               <ts e="T122" id="Seg_790" n="e" s="T121">da </ts>
               <ts e="T123" id="Seg_792" n="e" s="T122">kuznektə </ts>
               <ts e="T124" id="Seg_794" n="e" s="T123">üʔmiluʔpi. </ts>
               <ts e="T125" id="Seg_796" n="e" s="T124">I </ts>
               <ts e="T126" id="Seg_798" n="e" s="T125">šobi </ts>
               <ts e="T127" id="Seg_800" n="e" s="T126">babuškanə </ts>
               <ts e="T128" id="Seg_802" n="e" s="T127">da </ts>
               <ts e="T129" id="Seg_804" n="e" s="T128">deduška. </ts>
               <ts e="T130" id="Seg_806" n="e" s="T129">Büzʼenə </ts>
               <ts e="T131" id="Seg_808" n="e" s="T130">da </ts>
               <ts e="T132" id="Seg_810" n="e" s="T131">nükenə </ts>
               <ts e="T133" id="Seg_812" n="e" s="T132">šobi. </ts>
               <ts e="T134" id="Seg_814" n="e" s="T133">I </ts>
               <ts e="T135" id="Seg_816" n="e" s="T134">bazoʔ </ts>
               <ts e="T136" id="Seg_818" n="e" s="T135">amnobiʔi. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_819" s="T0">PKZ_196X_ThreeBears_flk.001 (001)</ta>
            <ta e="T9" id="Seg_820" s="T4">PKZ_196X_ThreeBears_flk.002 (002)</ta>
            <ta e="T13" id="Seg_821" s="T9">PKZ_196X_ThreeBears_flk.003 (003)</ta>
            <ta e="T15" id="Seg_822" s="T13">PKZ_196X_ThreeBears_flk.004 (004)</ta>
            <ta e="T20" id="Seg_823" s="T15">PKZ_196X_ThreeBears_flk.005 (005)</ta>
            <ta e="T24" id="Seg_824" s="T20">PKZ_196X_ThreeBears_flk.006 (006)</ta>
            <ta e="T26" id="Seg_825" s="T24">PKZ_196X_ThreeBears_flk.007 (007)</ta>
            <ta e="T28" id="Seg_826" s="T26">PKZ_196X_ThreeBears_flk.008 (008)</ta>
            <ta e="T33" id="Seg_827" s="T28">PKZ_196X_ThreeBears_flk.009 (009)</ta>
            <ta e="T37" id="Seg_828" s="T33">PKZ_196X_ThreeBears_flk.010 (010)</ta>
            <ta e="T41" id="Seg_829" s="T37">PKZ_196X_ThreeBears_flk.011 (011)</ta>
            <ta e="T44" id="Seg_830" s="T41">PKZ_196X_ThreeBears_flk.012 (012)</ta>
            <ta e="T47" id="Seg_831" s="T44">PKZ_196X_ThreeBears_flk.013 (013)</ta>
            <ta e="T53" id="Seg_832" s="T47">PKZ_196X_ThreeBears_flk.014 (014)</ta>
            <ta e="T55" id="Seg_833" s="T53">PKZ_196X_ThreeBears_flk.015 (015)</ta>
            <ta e="T60" id="Seg_834" s="T55">PKZ_196X_ThreeBears_flk.016 (016)</ta>
            <ta e="T66" id="Seg_835" s="T60">PKZ_196X_ThreeBears_flk.017 (017)</ta>
            <ta e="T71" id="Seg_836" s="T66">PKZ_196X_ThreeBears_flk.018 (018)</ta>
            <ta e="T76" id="Seg_837" s="T71">PKZ_196X_ThreeBears_flk.019 (019)</ta>
            <ta e="T83" id="Seg_838" s="T76">PKZ_196X_ThreeBears_flk.020 (020)</ta>
            <ta e="T88" id="Seg_839" s="T83">PKZ_196X_ThreeBears_flk.021 (021)</ta>
            <ta e="T91" id="Seg_840" s="T88">PKZ_196X_ThreeBears_flk.022 (022)</ta>
            <ta e="T97" id="Seg_841" s="T91">PKZ_196X_ThreeBears_flk.023 (023)</ta>
            <ta e="T100" id="Seg_842" s="T97">PKZ_196X_ThreeBears_flk.024 (024)</ta>
            <ta e="T106" id="Seg_843" s="T100">PKZ_196X_ThreeBears_flk.025 (025)</ta>
            <ta e="T118" id="Seg_844" s="T106">PKZ_196X_ThreeBears_flk.026 (026) </ta>
            <ta e="T124" id="Seg_845" s="T118">PKZ_196X_ThreeBears_flk.027 (028)</ta>
            <ta e="T129" id="Seg_846" s="T124">PKZ_196X_ThreeBears_flk.028 (029)</ta>
            <ta e="T133" id="Seg_847" s="T129">PKZ_196X_ThreeBears_flk.029 (030)</ta>
            <ta e="T136" id="Seg_848" s="T133">PKZ_196X_ThreeBears_flk.030 (031)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_849" s="T0">Amnobiʔi nüke da büzʼe. </ta>
            <ta e="T9" id="Seg_850" s="T4">(Dĭn=) Dĭzeŋgən koʔbdo ibi, vnučka. </ta>
            <ta e="T13" id="Seg_851" s="T9">Dĭ (ka-) kambi keʔbdejle. </ta>
            <ta e="T15" id="Seg_852" s="T13">I dʼürlambi. </ta>
            <ta e="T20" id="Seg_853" s="T15">Mĭmbi, mĭmbi, šobi urgaːbanə maːʔndə. </ta>
            <ta e="T24" id="Seg_854" s="T20">Kuliat bar: stol nuga. </ta>
            <ta e="T26" id="Seg_855" s="T24">Stuləʔi nugaʔi. </ta>
            <ta e="T28" id="Seg_856" s="T26">Takšəʔi nugaʔi. </ta>
            <ta e="T33" id="Seg_857" s="T28">Onʼiʔ takšə urgo, onʼiʔ üdʼüge. </ta>
            <ta e="T37" id="Seg_858" s="T33">A nagur takšə (idʼiʔeʔe). </ta>
            <ta e="T41" id="Seg_859" s="T37">Tĭ amnobi urgo takšənə. </ta>
            <ta e="T44" id="Seg_860" s="T41">Šalaʔje (ambi=) ambi. </ta>
            <ta e="T47" id="Seg_861" s="T44">(Ĭmbidə) ej jakše. </ta>
            <ta e="T53" id="Seg_862" s="T47">Dĭgəttə (šidögöndəjʔ) amnobi, tože (šaʔlaʔje) ambi. </ta>
            <ta e="T55" id="Seg_863" s="T53">Ej jakše. </ta>
            <ta e="T60" id="Seg_864" s="T55">Dĭgəttə nagurdə ambi, üdʼüge takšənə. </ta>
            <ta e="T66" id="Seg_865" s="T60">Dön jakše, ambi, ambi, dĭgəttə kambi. </ta>
            <ta e="T71" id="Seg_866" s="T66">Onʼiʔ krăvatʼtə (ur-) iʔbəbi— urgo. </ta>
            <ta e="T76" id="Seg_867" s="T71">Dĭgəttə šide krăvatʼtə iʔbəbi— urgo. </ta>
            <ta e="T83" id="Seg_868" s="T76">Dĭgəttə nagur krăvatʼtə (iʔbəbi)— dön jakše, üdʼüge. </ta>
            <ta e="T88" id="Seg_869" s="T83">(I ko-) I (kuno-) kunollaʔbə. </ta>
            <ta e="T91" id="Seg_870" s="T88">Dĭgəttə urgaːbaʔi šobiʔi. </ta>
            <ta e="T97" id="Seg_871" s="T91">"Šindi amnəbi măn takšənə",— bar kirgarlaʔbəʔjə. </ta>
            <ta e="T100" id="Seg_872" s="T97">Dĭgəttə kambiʔi kunolzittə. </ta>
            <ta e="T106" id="Seg_873" s="T100">"Šindi măn krăvatʼtən bar dön iʔbolaʔpi?" </ta>
            <ta e="T118" id="Seg_874" s="T106">A dĭ üdʼüge urgaːba bar (to-) todam bar kirgarlaʔbə: "Šindidə dön iʔbolaʔbə!" </ta>
            <ta e="T124" id="Seg_875" s="T118">Dĭ (š-) suʔmiluʔpi da kuznektə üʔmiluʔpi. </ta>
            <ta e="T129" id="Seg_876" s="T124">I šobi babuškanə da deduška. </ta>
            <ta e="T133" id="Seg_877" s="T129">Büzʼenə da nükenə šobi. </ta>
            <ta e="T136" id="Seg_878" s="T133">I bazoʔ amnobiʔi. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_879" s="T0">amno-bi-ʔi</ta>
            <ta e="T2" id="Seg_880" s="T1">nüke</ta>
            <ta e="T3" id="Seg_881" s="T2">da</ta>
            <ta e="T4" id="Seg_882" s="T3">büzʼe</ta>
            <ta e="T5" id="Seg_883" s="T4">dĭ-n</ta>
            <ta e="T6" id="Seg_884" s="T5">dĭ-zeŋ-gən</ta>
            <ta e="T7" id="Seg_885" s="T6">koʔbdo</ta>
            <ta e="T8" id="Seg_886" s="T7">i-bi</ta>
            <ta e="T9" id="Seg_887" s="T8">vnučka</ta>
            <ta e="T10" id="Seg_888" s="T9">dĭ</ta>
            <ta e="T12" id="Seg_889" s="T11">kam-bi</ta>
            <ta e="T13" id="Seg_890" s="T12">keʔbde-j-le</ta>
            <ta e="T14" id="Seg_891" s="T13">i</ta>
            <ta e="T15" id="Seg_892" s="T14">dʼür-lam-bi</ta>
            <ta e="T16" id="Seg_893" s="T15">mĭm-bi</ta>
            <ta e="T17" id="Seg_894" s="T16">mĭm-bi</ta>
            <ta e="T18" id="Seg_895" s="T17">šo-bi</ta>
            <ta e="T19" id="Seg_896" s="T18">urgaːba-nə</ta>
            <ta e="T20" id="Seg_897" s="T19">maːʔ-ndə</ta>
            <ta e="T21" id="Seg_898" s="T20">ku-lia-t</ta>
            <ta e="T22" id="Seg_899" s="T21">bar</ta>
            <ta e="T23" id="Seg_900" s="T22">stol</ta>
            <ta e="T24" id="Seg_901" s="T23">nu-ga</ta>
            <ta e="T25" id="Seg_902" s="T24">stul-əʔi</ta>
            <ta e="T26" id="Seg_903" s="T25">nu-ga-ʔi</ta>
            <ta e="T27" id="Seg_904" s="T26">takšə-ʔi</ta>
            <ta e="T28" id="Seg_905" s="T27">nu-ga-ʔi</ta>
            <ta e="T29" id="Seg_906" s="T28">onʼiʔ</ta>
            <ta e="T30" id="Seg_907" s="T29">takšə</ta>
            <ta e="T31" id="Seg_908" s="T30">urgo</ta>
            <ta e="T32" id="Seg_909" s="T31">onʼiʔ</ta>
            <ta e="T33" id="Seg_910" s="T32">üdʼüge</ta>
            <ta e="T34" id="Seg_911" s="T33">a</ta>
            <ta e="T35" id="Seg_912" s="T34">nagur</ta>
            <ta e="T36" id="Seg_913" s="T35">takšə</ta>
            <ta e="T37" id="Seg_914" s="T36">idʼiʔeʔe</ta>
            <ta e="T38" id="Seg_915" s="T37">tĭ</ta>
            <ta e="T39" id="Seg_916" s="T38">amno-bi</ta>
            <ta e="T40" id="Seg_917" s="T39">urgo</ta>
            <ta e="T41" id="Seg_918" s="T40">takšə-nə</ta>
            <ta e="T42" id="Seg_919" s="T41">šalaʔje</ta>
            <ta e="T43" id="Seg_920" s="T42">am-bi</ta>
            <ta e="T44" id="Seg_921" s="T43">am-bi</ta>
            <ta e="T45" id="Seg_922" s="T44">ĭmbi=də</ta>
            <ta e="T46" id="Seg_923" s="T45">ej</ta>
            <ta e="T47" id="Seg_924" s="T46">jakše</ta>
            <ta e="T48" id="Seg_925" s="T47">dĭgəttə</ta>
            <ta e="T49" id="Seg_926" s="T48">šidö-göndəjʔ</ta>
            <ta e="T50" id="Seg_927" s="T49">amno-bi</ta>
            <ta e="T51" id="Seg_928" s="T50">tože</ta>
            <ta e="T52" id="Seg_929" s="T51">šaʔlaʔje</ta>
            <ta e="T53" id="Seg_930" s="T52">am-bi</ta>
            <ta e="T54" id="Seg_931" s="T53">ej</ta>
            <ta e="T55" id="Seg_932" s="T54">jakše</ta>
            <ta e="T56" id="Seg_933" s="T55">dĭgəttə</ta>
            <ta e="T57" id="Seg_934" s="T56">nagur-də</ta>
            <ta e="T58" id="Seg_935" s="T57">am-bi</ta>
            <ta e="T59" id="Seg_936" s="T58">üdʼüge</ta>
            <ta e="T60" id="Seg_937" s="T59">takšə-nə</ta>
            <ta e="T61" id="Seg_938" s="T60">dön</ta>
            <ta e="T62" id="Seg_939" s="T61">jakše</ta>
            <ta e="T63" id="Seg_940" s="T62">am-bi</ta>
            <ta e="T64" id="Seg_941" s="T63">am-bi</ta>
            <ta e="T65" id="Seg_942" s="T64">dĭgəttə</ta>
            <ta e="T66" id="Seg_943" s="T65">kam-bi</ta>
            <ta e="T67" id="Seg_944" s="T66">onʼiʔ</ta>
            <ta e="T68" id="Seg_945" s="T67">krăvatʼ-tə</ta>
            <ta e="T70" id="Seg_946" s="T69">iʔbə-bi</ta>
            <ta e="T71" id="Seg_947" s="T70">urgo</ta>
            <ta e="T72" id="Seg_948" s="T71">dĭgəttə</ta>
            <ta e="T73" id="Seg_949" s="T72">šide</ta>
            <ta e="T74" id="Seg_950" s="T73">krăvatʼ-tə</ta>
            <ta e="T75" id="Seg_951" s="T74">iʔbə-bi</ta>
            <ta e="T76" id="Seg_952" s="T75">urgo</ta>
            <ta e="T77" id="Seg_953" s="T76">dĭgəttə</ta>
            <ta e="T78" id="Seg_954" s="T77">nagur</ta>
            <ta e="T79" id="Seg_955" s="T78">krăvatʼ-tə</ta>
            <ta e="T80" id="Seg_956" s="T79">iʔbə-bi</ta>
            <ta e="T81" id="Seg_957" s="T80">dön</ta>
            <ta e="T82" id="Seg_958" s="T81">jakše</ta>
            <ta e="T83" id="Seg_959" s="T82">üdʼüge</ta>
            <ta e="T84" id="Seg_960" s="T83">i</ta>
            <ta e="T86" id="Seg_961" s="T85">i</ta>
            <ta e="T88" id="Seg_962" s="T87">kunol-laʔbə</ta>
            <ta e="T89" id="Seg_963" s="T88">dĭgəttə</ta>
            <ta e="T90" id="Seg_964" s="T89">urgaːba-ʔi</ta>
            <ta e="T91" id="Seg_965" s="T90">šo-bi-ʔi</ta>
            <ta e="T92" id="Seg_966" s="T91">šindi</ta>
            <ta e="T93" id="Seg_967" s="T92">amnə-bi</ta>
            <ta e="T94" id="Seg_968" s="T93">măn</ta>
            <ta e="T95" id="Seg_969" s="T94">takšə-nə</ta>
            <ta e="T96" id="Seg_970" s="T95">bar</ta>
            <ta e="T97" id="Seg_971" s="T96">kirgar-laʔbə-ʔjə</ta>
            <ta e="T98" id="Seg_972" s="T97">dĭgəttə</ta>
            <ta e="T99" id="Seg_973" s="T98">kam-bi-ʔi</ta>
            <ta e="T100" id="Seg_974" s="T99">kunol-zittə</ta>
            <ta e="T101" id="Seg_975" s="T100">šindi</ta>
            <ta e="T102" id="Seg_976" s="T101">măn</ta>
            <ta e="T103" id="Seg_977" s="T102">krăvatʼ-tən</ta>
            <ta e="T104" id="Seg_978" s="T103">bar</ta>
            <ta e="T105" id="Seg_979" s="T104">dön</ta>
            <ta e="T106" id="Seg_980" s="T105">iʔbo-laʔ-pi</ta>
            <ta e="T107" id="Seg_981" s="T106">a</ta>
            <ta e="T108" id="Seg_982" s="T107">dĭ</ta>
            <ta e="T109" id="Seg_983" s="T108">üdʼüge</ta>
            <ta e="T110" id="Seg_984" s="T109">urgaːba</ta>
            <ta e="T111" id="Seg_985" s="T110">bar</ta>
            <ta e="T113" id="Seg_986" s="T112">todam</ta>
            <ta e="T114" id="Seg_987" s="T113">bar</ta>
            <ta e="T115" id="Seg_988" s="T114">kirgar-laʔbə</ta>
            <ta e="T116" id="Seg_989" s="T115">šindi=də</ta>
            <ta e="T117" id="Seg_990" s="T116">dön</ta>
            <ta e="T118" id="Seg_991" s="T117">iʔbo-laʔbə</ta>
            <ta e="T119" id="Seg_992" s="T118">dĭ</ta>
            <ta e="T121" id="Seg_993" s="T120">suʔmi-luʔ-pi</ta>
            <ta e="T122" id="Seg_994" s="T121">da</ta>
            <ta e="T123" id="Seg_995" s="T122">kuznek-tə</ta>
            <ta e="T124" id="Seg_996" s="T123">üʔmi-luʔ-pi</ta>
            <ta e="T125" id="Seg_997" s="T124">i</ta>
            <ta e="T126" id="Seg_998" s="T125">šo-bi</ta>
            <ta e="T127" id="Seg_999" s="T126">babuška-nə</ta>
            <ta e="T128" id="Seg_1000" s="T127">da</ta>
            <ta e="T129" id="Seg_1001" s="T128">deduška</ta>
            <ta e="T130" id="Seg_1002" s="T129">büzʼe-nə</ta>
            <ta e="T131" id="Seg_1003" s="T130">da</ta>
            <ta e="T132" id="Seg_1004" s="T131">nüke-nə</ta>
            <ta e="T133" id="Seg_1005" s="T132">šo-bi</ta>
            <ta e="T134" id="Seg_1006" s="T133">i</ta>
            <ta e="T135" id="Seg_1007" s="T134">bazoʔ</ta>
            <ta e="T136" id="Seg_1008" s="T135">amno-bi-ʔi</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1009" s="T0">amno-bi-jəʔ</ta>
            <ta e="T2" id="Seg_1010" s="T1">nüke</ta>
            <ta e="T3" id="Seg_1011" s="T2">da</ta>
            <ta e="T4" id="Seg_1012" s="T3">büzʼe</ta>
            <ta e="T5" id="Seg_1013" s="T4">dĭ-n</ta>
            <ta e="T6" id="Seg_1014" s="T5">dĭ-zAŋ-Kən</ta>
            <ta e="T7" id="Seg_1015" s="T6">koʔbdo</ta>
            <ta e="T8" id="Seg_1016" s="T7">i-bi</ta>
            <ta e="T9" id="Seg_1017" s="T8">vnučka</ta>
            <ta e="T10" id="Seg_1018" s="T9">dĭ</ta>
            <ta e="T12" id="Seg_1019" s="T11">kan-bi</ta>
            <ta e="T13" id="Seg_1020" s="T12">keʔbde-j-lAʔ</ta>
            <ta e="T14" id="Seg_1021" s="T13">i</ta>
            <ta e="T15" id="Seg_1022" s="T14">tʼür-laːm-bi</ta>
            <ta e="T16" id="Seg_1023" s="T15">mĭn-bi</ta>
            <ta e="T17" id="Seg_1024" s="T16">mĭn-bi</ta>
            <ta e="T18" id="Seg_1025" s="T17">šo-bi</ta>
            <ta e="T19" id="Seg_1026" s="T18">urgaːba-Tə</ta>
            <ta e="T20" id="Seg_1027" s="T19">maʔ-gəndə</ta>
            <ta e="T21" id="Seg_1028" s="T20">ku-liA-t</ta>
            <ta e="T22" id="Seg_1029" s="T21">bar</ta>
            <ta e="T23" id="Seg_1030" s="T22">stol</ta>
            <ta e="T24" id="Seg_1031" s="T23">nu-gA</ta>
            <ta e="T25" id="Seg_1032" s="T24">stul-jəʔ</ta>
            <ta e="T26" id="Seg_1033" s="T25">nu-gA-jəʔ</ta>
            <ta e="T27" id="Seg_1034" s="T26">takše-jəʔ</ta>
            <ta e="T28" id="Seg_1035" s="T27">nu-gA-jəʔ</ta>
            <ta e="T29" id="Seg_1036" s="T28">onʼiʔ</ta>
            <ta e="T30" id="Seg_1037" s="T29">takše</ta>
            <ta e="T31" id="Seg_1038" s="T30">urgo</ta>
            <ta e="T32" id="Seg_1039" s="T31">onʼiʔ</ta>
            <ta e="T33" id="Seg_1040" s="T32">üdʼüge</ta>
            <ta e="T34" id="Seg_1041" s="T33">a</ta>
            <ta e="T35" id="Seg_1042" s="T34">nagur</ta>
            <ta e="T36" id="Seg_1043" s="T35">takše</ta>
            <ta e="T37" id="Seg_1044" s="T36">idʼiʔeʔe</ta>
            <ta e="T38" id="Seg_1045" s="T37">dĭ</ta>
            <ta e="T39" id="Seg_1046" s="T38">amnə-bi</ta>
            <ta e="T40" id="Seg_1047" s="T39">urgo</ta>
            <ta e="T41" id="Seg_1048" s="T40">takše-Tə</ta>
            <ta e="T42" id="Seg_1049" s="T41">šala</ta>
            <ta e="T43" id="Seg_1050" s="T42">am-bi</ta>
            <ta e="T44" id="Seg_1051" s="T43">am-bi</ta>
            <ta e="T45" id="Seg_1052" s="T44">ĭmbi=də</ta>
            <ta e="T46" id="Seg_1053" s="T45">ej</ta>
            <ta e="T47" id="Seg_1054" s="T46">jakšə</ta>
            <ta e="T48" id="Seg_1055" s="T47">dĭgəttə</ta>
            <ta e="T49" id="Seg_1056" s="T48">šide-göndəjʔ</ta>
            <ta e="T50" id="Seg_1057" s="T49">amnə-bi</ta>
            <ta e="T51" id="Seg_1058" s="T50">tože</ta>
            <ta e="T52" id="Seg_1059" s="T51">saʔlajə</ta>
            <ta e="T53" id="Seg_1060" s="T52">am-bi</ta>
            <ta e="T54" id="Seg_1061" s="T53">ej</ta>
            <ta e="T55" id="Seg_1062" s="T54">jakšə</ta>
            <ta e="T56" id="Seg_1063" s="T55">dĭgəttə</ta>
            <ta e="T57" id="Seg_1064" s="T56">nagur-Tə</ta>
            <ta e="T58" id="Seg_1065" s="T57">am-bi</ta>
            <ta e="T59" id="Seg_1066" s="T58">üdʼüge</ta>
            <ta e="T60" id="Seg_1067" s="T59">takše-Tə</ta>
            <ta e="T61" id="Seg_1068" s="T60">dön</ta>
            <ta e="T62" id="Seg_1069" s="T61">jakšə</ta>
            <ta e="T63" id="Seg_1070" s="T62">am-bi</ta>
            <ta e="T64" id="Seg_1071" s="T63">am-bi</ta>
            <ta e="T65" id="Seg_1072" s="T64">dĭgəttə</ta>
            <ta e="T66" id="Seg_1073" s="T65">kan-bi</ta>
            <ta e="T67" id="Seg_1074" s="T66">onʼiʔ</ta>
            <ta e="T68" id="Seg_1075" s="T67">krăvatʼ-Tə</ta>
            <ta e="T70" id="Seg_1076" s="T69">iʔbə-bi</ta>
            <ta e="T71" id="Seg_1077" s="T70">urgo</ta>
            <ta e="T72" id="Seg_1078" s="T71">dĭgəttə</ta>
            <ta e="T73" id="Seg_1079" s="T72">šide</ta>
            <ta e="T74" id="Seg_1080" s="T73">krăvatʼ-Tə</ta>
            <ta e="T75" id="Seg_1081" s="T74">iʔbə-bi</ta>
            <ta e="T76" id="Seg_1082" s="T75">urgo</ta>
            <ta e="T77" id="Seg_1083" s="T76">dĭgəttə</ta>
            <ta e="T78" id="Seg_1084" s="T77">nagur</ta>
            <ta e="T79" id="Seg_1085" s="T78">krăvatʼ-Tə</ta>
            <ta e="T80" id="Seg_1086" s="T79">iʔbə-bi</ta>
            <ta e="T81" id="Seg_1087" s="T80">dön</ta>
            <ta e="T82" id="Seg_1088" s="T81">jakšə</ta>
            <ta e="T83" id="Seg_1089" s="T82">üdʼüge</ta>
            <ta e="T84" id="Seg_1090" s="T83">i</ta>
            <ta e="T86" id="Seg_1091" s="T85">i</ta>
            <ta e="T88" id="Seg_1092" s="T87">kunol-laʔbə</ta>
            <ta e="T89" id="Seg_1093" s="T88">dĭgəttə</ta>
            <ta e="T90" id="Seg_1094" s="T89">urgaːba-jəʔ</ta>
            <ta e="T91" id="Seg_1095" s="T90">šo-bi-jəʔ</ta>
            <ta e="T92" id="Seg_1096" s="T91">šində</ta>
            <ta e="T93" id="Seg_1097" s="T92">amnə-bi</ta>
            <ta e="T94" id="Seg_1098" s="T93">măn</ta>
            <ta e="T95" id="Seg_1099" s="T94">takše-Tə</ta>
            <ta e="T96" id="Seg_1100" s="T95">bar</ta>
            <ta e="T97" id="Seg_1101" s="T96">kirgaːr-laʔbə-jəʔ</ta>
            <ta e="T98" id="Seg_1102" s="T97">dĭgəttə</ta>
            <ta e="T99" id="Seg_1103" s="T98">kan-bi-jəʔ</ta>
            <ta e="T100" id="Seg_1104" s="T99">kunol-zittə</ta>
            <ta e="T101" id="Seg_1105" s="T100">šində</ta>
            <ta e="T102" id="Seg_1106" s="T101">măn</ta>
            <ta e="T103" id="Seg_1107" s="T102">krăvatʼ-dən</ta>
            <ta e="T104" id="Seg_1108" s="T103">bar</ta>
            <ta e="T105" id="Seg_1109" s="T104">dön</ta>
            <ta e="T106" id="Seg_1110" s="T105">iʔbö-laʔbə-bi</ta>
            <ta e="T107" id="Seg_1111" s="T106">a</ta>
            <ta e="T108" id="Seg_1112" s="T107">dĭ</ta>
            <ta e="T109" id="Seg_1113" s="T108">üdʼüge</ta>
            <ta e="T110" id="Seg_1114" s="T109">urgaːba</ta>
            <ta e="T111" id="Seg_1115" s="T110">bar</ta>
            <ta e="T113" id="Seg_1116" s="T112">todam</ta>
            <ta e="T114" id="Seg_1117" s="T113">bar</ta>
            <ta e="T115" id="Seg_1118" s="T114">kirgaːr-laʔbə</ta>
            <ta e="T116" id="Seg_1119" s="T115">šində=də</ta>
            <ta e="T117" id="Seg_1120" s="T116">dön</ta>
            <ta e="T118" id="Seg_1121" s="T117">iʔbö-laʔbə</ta>
            <ta e="T119" id="Seg_1122" s="T118">dĭ</ta>
            <ta e="T121" id="Seg_1123" s="T120">süʔmə-luʔbdə-bi</ta>
            <ta e="T122" id="Seg_1124" s="T121">da</ta>
            <ta e="T123" id="Seg_1125" s="T122">kuznek-Tə</ta>
            <ta e="T124" id="Seg_1126" s="T123">üʔmə-luʔbdə-bi</ta>
            <ta e="T125" id="Seg_1127" s="T124">i</ta>
            <ta e="T126" id="Seg_1128" s="T125">šo-bi</ta>
            <ta e="T127" id="Seg_1129" s="T126">babuška-Tə</ta>
            <ta e="T128" id="Seg_1130" s="T127">da</ta>
            <ta e="T129" id="Seg_1131" s="T128">deduška</ta>
            <ta e="T130" id="Seg_1132" s="T129">büzʼe-Tə</ta>
            <ta e="T131" id="Seg_1133" s="T130">da</ta>
            <ta e="T132" id="Seg_1134" s="T131">nüke-Tə</ta>
            <ta e="T133" id="Seg_1135" s="T132">šo-bi</ta>
            <ta e="T134" id="Seg_1136" s="T133">i</ta>
            <ta e="T135" id="Seg_1137" s="T134">bazoʔ</ta>
            <ta e="T136" id="Seg_1138" s="T135">amno-bi-jəʔ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1139" s="T0">live-PST-3PL</ta>
            <ta e="T2" id="Seg_1140" s="T1">woman.[NOM.SG]</ta>
            <ta e="T3" id="Seg_1141" s="T2">and</ta>
            <ta e="T4" id="Seg_1142" s="T3">man.[NOM.SG]</ta>
            <ta e="T5" id="Seg_1143" s="T4">this-GEN</ta>
            <ta e="T6" id="Seg_1144" s="T5">this-PL-LOC</ta>
            <ta e="T7" id="Seg_1145" s="T6">girl.[NOM.SG]</ta>
            <ta e="T8" id="Seg_1146" s="T7">be-PST.[3SG]</ta>
            <ta e="T9" id="Seg_1147" s="T8">granddaughter.[NOM.SG]</ta>
            <ta e="T10" id="Seg_1148" s="T9">this.[NOM.SG]</ta>
            <ta e="T12" id="Seg_1149" s="T11">go-PST.[3SG]</ta>
            <ta e="T13" id="Seg_1150" s="T12">berry-VBLZ-CVB</ta>
            <ta e="T14" id="Seg_1151" s="T13">and</ta>
            <ta e="T15" id="Seg_1152" s="T14">disappear-RES-PST.[3SG]</ta>
            <ta e="T16" id="Seg_1153" s="T15">go-PST.[3SG]</ta>
            <ta e="T17" id="Seg_1154" s="T16">go-PST.[3SG]</ta>
            <ta e="T18" id="Seg_1155" s="T17">come-PST.[3SG]</ta>
            <ta e="T19" id="Seg_1156" s="T18">bear-LAT</ta>
            <ta e="T20" id="Seg_1157" s="T19">tent-LAT/LOC.3SG</ta>
            <ta e="T21" id="Seg_1158" s="T20">see-PRS-3SG.O</ta>
            <ta e="T22" id="Seg_1159" s="T21">PTCL</ta>
            <ta e="T23" id="Seg_1160" s="T22">table.[NOM.SG]</ta>
            <ta e="T24" id="Seg_1161" s="T23">stand-PRS.[3SG]</ta>
            <ta e="T25" id="Seg_1162" s="T24">chair-PL</ta>
            <ta e="T26" id="Seg_1163" s="T25">stand-PRS-3PL</ta>
            <ta e="T27" id="Seg_1164" s="T26">cup-PL</ta>
            <ta e="T28" id="Seg_1165" s="T27">stand-PRS-3PL</ta>
            <ta e="T29" id="Seg_1166" s="T28">one.[NOM.SG]</ta>
            <ta e="T30" id="Seg_1167" s="T29">cup.[NOM.SG]</ta>
            <ta e="T31" id="Seg_1168" s="T30">big.[NOM.SG]</ta>
            <ta e="T32" id="Seg_1169" s="T31">one.[NOM.SG]</ta>
            <ta e="T33" id="Seg_1170" s="T32">small.[NOM.SG]</ta>
            <ta e="T34" id="Seg_1171" s="T33">and</ta>
            <ta e="T35" id="Seg_1172" s="T34">three.[NOM.SG]</ta>
            <ta e="T36" id="Seg_1173" s="T35">cup.[NOM.SG]</ta>
            <ta e="T37" id="Seg_1174" s="T36">a.few</ta>
            <ta e="T38" id="Seg_1175" s="T37">this</ta>
            <ta e="T39" id="Seg_1176" s="T38">sit.down-PST.[3SG]</ta>
            <ta e="T40" id="Seg_1177" s="T39">big.[NOM.SG]</ta>
            <ta e="T41" id="Seg_1178" s="T40">cup-LAT</ta>
            <ta e="T42" id="Seg_1179" s="T41">%%</ta>
            <ta e="T43" id="Seg_1180" s="T42">eat-PST.[3SG]</ta>
            <ta e="T44" id="Seg_1181" s="T43">eat-PST.[3SG]</ta>
            <ta e="T45" id="Seg_1182" s="T44">what.[NOM.SG]=INDEF</ta>
            <ta e="T46" id="Seg_1183" s="T45">NEG</ta>
            <ta e="T47" id="Seg_1184" s="T46">good</ta>
            <ta e="T48" id="Seg_1185" s="T47">then</ta>
            <ta e="T49" id="Seg_1186" s="T48">two-%%</ta>
            <ta e="T50" id="Seg_1187" s="T49">sit.down-PST.[3SG]</ta>
            <ta e="T51" id="Seg_1188" s="T50">also</ta>
            <ta e="T52" id="Seg_1189" s="T51">%%</ta>
            <ta e="T53" id="Seg_1190" s="T52">eat-PST.[3SG]</ta>
            <ta e="T54" id="Seg_1191" s="T53">NEG</ta>
            <ta e="T55" id="Seg_1192" s="T54">good</ta>
            <ta e="T56" id="Seg_1193" s="T55">then</ta>
            <ta e="T57" id="Seg_1194" s="T56">three-LAT</ta>
            <ta e="T58" id="Seg_1195" s="T57">sit-PST.[3SG]</ta>
            <ta e="T59" id="Seg_1196" s="T58">small.[NOM.SG]</ta>
            <ta e="T60" id="Seg_1197" s="T59">cup-LAT</ta>
            <ta e="T61" id="Seg_1198" s="T60">here</ta>
            <ta e="T62" id="Seg_1199" s="T61">good</ta>
            <ta e="T63" id="Seg_1200" s="T62">eat-PST.[3SG]</ta>
            <ta e="T64" id="Seg_1201" s="T63">eat-PST.[3SG]</ta>
            <ta e="T65" id="Seg_1202" s="T64">then</ta>
            <ta e="T66" id="Seg_1203" s="T65">go-PST.[3SG]</ta>
            <ta e="T67" id="Seg_1204" s="T66">one.[NOM.SG]</ta>
            <ta e="T68" id="Seg_1205" s="T67">bed-LAT</ta>
            <ta e="T70" id="Seg_1206" s="T69">lie.down-PST.[3SG]</ta>
            <ta e="T71" id="Seg_1207" s="T70">big.[NOM.SG]</ta>
            <ta e="T72" id="Seg_1208" s="T71">then</ta>
            <ta e="T73" id="Seg_1209" s="T72">two.[NOM.SG]</ta>
            <ta e="T74" id="Seg_1210" s="T73">bed-LAT</ta>
            <ta e="T75" id="Seg_1211" s="T74">lie.down-PST.[3SG]</ta>
            <ta e="T76" id="Seg_1212" s="T75">big.[NOM.SG]</ta>
            <ta e="T77" id="Seg_1213" s="T76">then</ta>
            <ta e="T78" id="Seg_1214" s="T77">three.[NOM.SG]</ta>
            <ta e="T79" id="Seg_1215" s="T78">bed-LAT</ta>
            <ta e="T80" id="Seg_1216" s="T79">lie.down-PST.[3SG]</ta>
            <ta e="T81" id="Seg_1217" s="T80">here</ta>
            <ta e="T82" id="Seg_1218" s="T81">good</ta>
            <ta e="T83" id="Seg_1219" s="T82">small.[NOM.SG]</ta>
            <ta e="T84" id="Seg_1220" s="T83">and</ta>
            <ta e="T86" id="Seg_1221" s="T85">and</ta>
            <ta e="T88" id="Seg_1222" s="T87">sleep-DUR.[3SG]</ta>
            <ta e="T89" id="Seg_1223" s="T88">then</ta>
            <ta e="T90" id="Seg_1224" s="T89">bear-NOM/GEN/ACC.3PL</ta>
            <ta e="T91" id="Seg_1225" s="T90">come-PST-3PL</ta>
            <ta e="T92" id="Seg_1226" s="T91">who.[NOM.SG]</ta>
            <ta e="T93" id="Seg_1227" s="T92">sit-PST.[3SG]</ta>
            <ta e="T94" id="Seg_1228" s="T93">I.NOM</ta>
            <ta e="T95" id="Seg_1229" s="T94">cup-LAT</ta>
            <ta e="T96" id="Seg_1230" s="T95">PTCL</ta>
            <ta e="T97" id="Seg_1231" s="T96">shout-DUR-3PL</ta>
            <ta e="T98" id="Seg_1232" s="T97">then</ta>
            <ta e="T99" id="Seg_1233" s="T98">go-PST-3PL</ta>
            <ta e="T100" id="Seg_1234" s="T99">sleep-INF.LAT</ta>
            <ta e="T101" id="Seg_1235" s="T100">who.[NOM.SG]</ta>
            <ta e="T102" id="Seg_1236" s="T101">I.NOM</ta>
            <ta e="T103" id="Seg_1237" s="T102">bed-NOM/GEN/ACC.3PL</ta>
            <ta e="T104" id="Seg_1238" s="T103">PTCL</ta>
            <ta e="T105" id="Seg_1239" s="T104">here</ta>
            <ta e="T106" id="Seg_1240" s="T105">lie-DUR-PST.[3SG]</ta>
            <ta e="T107" id="Seg_1241" s="T106">and</ta>
            <ta e="T108" id="Seg_1242" s="T107">this.[NOM.SG]</ta>
            <ta e="T109" id="Seg_1243" s="T108">small.[NOM.SG]</ta>
            <ta e="T110" id="Seg_1244" s="T109">bear.[NOM.SG]</ta>
            <ta e="T111" id="Seg_1245" s="T110">PTCL</ta>
            <ta e="T113" id="Seg_1246" s="T112">thin.[NOM.SG]</ta>
            <ta e="T114" id="Seg_1247" s="T113">PTCL</ta>
            <ta e="T115" id="Seg_1248" s="T114">shout-DUR.[3SG]</ta>
            <ta e="T116" id="Seg_1249" s="T115">who.[NOM.SG]=INDEF</ta>
            <ta e="T117" id="Seg_1250" s="T116">here</ta>
            <ta e="T118" id="Seg_1251" s="T117">lie-DUR.[3SG]</ta>
            <ta e="T119" id="Seg_1252" s="T118">this.[NOM.SG]</ta>
            <ta e="T121" id="Seg_1253" s="T120">jump-MOM-PST.[3SG]</ta>
            <ta e="T122" id="Seg_1254" s="T121">and</ta>
            <ta e="T123" id="Seg_1255" s="T122">window-LAT</ta>
            <ta e="T124" id="Seg_1256" s="T123">run-MOM-PST.[3SG]</ta>
            <ta e="T125" id="Seg_1257" s="T124">and</ta>
            <ta e="T126" id="Seg_1258" s="T125">come-PST.[3SG]</ta>
            <ta e="T127" id="Seg_1259" s="T126">grandmother-LAT</ta>
            <ta e="T128" id="Seg_1260" s="T127">and</ta>
            <ta e="T129" id="Seg_1261" s="T128">grandfather.[NOM.SG]</ta>
            <ta e="T130" id="Seg_1262" s="T129">man-LAT</ta>
            <ta e="T131" id="Seg_1263" s="T130">and</ta>
            <ta e="T132" id="Seg_1264" s="T131">woman-LAT</ta>
            <ta e="T133" id="Seg_1265" s="T132">come-PST.[3SG]</ta>
            <ta e="T134" id="Seg_1266" s="T133">and</ta>
            <ta e="T135" id="Seg_1267" s="T134">again</ta>
            <ta e="T136" id="Seg_1268" s="T135">live-PST-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1269" s="T0">жить-PST-3PL</ta>
            <ta e="T2" id="Seg_1270" s="T1">женщина.[NOM.SG]</ta>
            <ta e="T3" id="Seg_1271" s="T2">и</ta>
            <ta e="T4" id="Seg_1272" s="T3">мужчина.[NOM.SG]</ta>
            <ta e="T5" id="Seg_1273" s="T4">этот-GEN</ta>
            <ta e="T6" id="Seg_1274" s="T5">этот-PL-LOC</ta>
            <ta e="T7" id="Seg_1275" s="T6">девушка.[NOM.SG]</ta>
            <ta e="T8" id="Seg_1276" s="T7">быть-PST.[3SG]</ta>
            <ta e="T9" id="Seg_1277" s="T8">внучка.[NOM.SG]</ta>
            <ta e="T10" id="Seg_1278" s="T9">этот.[NOM.SG]</ta>
            <ta e="T12" id="Seg_1279" s="T11">пойти-PST.[3SG]</ta>
            <ta e="T13" id="Seg_1280" s="T12">ягода-VBLZ-CVB</ta>
            <ta e="T14" id="Seg_1281" s="T13">и</ta>
            <ta e="T15" id="Seg_1282" s="T14">исчезнуть-RES-PST.[3SG]</ta>
            <ta e="T16" id="Seg_1283" s="T15">идти-PST.[3SG]</ta>
            <ta e="T17" id="Seg_1284" s="T16">идти-PST.[3SG]</ta>
            <ta e="T18" id="Seg_1285" s="T17">прийти-PST.[3SG]</ta>
            <ta e="T19" id="Seg_1286" s="T18">медведь-LAT</ta>
            <ta e="T20" id="Seg_1287" s="T19">чум-LAT/LOC.3SG</ta>
            <ta e="T21" id="Seg_1288" s="T20">видеть-PRS-3SG.O</ta>
            <ta e="T22" id="Seg_1289" s="T21">PTCL</ta>
            <ta e="T23" id="Seg_1290" s="T22">стол.[NOM.SG]</ta>
            <ta e="T24" id="Seg_1291" s="T23">стоять-PRS.[3SG]</ta>
            <ta e="T25" id="Seg_1292" s="T24">стул-PL</ta>
            <ta e="T26" id="Seg_1293" s="T25">стоять-PRS-3PL</ta>
            <ta e="T27" id="Seg_1294" s="T26">чашка-PL</ta>
            <ta e="T28" id="Seg_1295" s="T27">стоять-PRS-3PL</ta>
            <ta e="T29" id="Seg_1296" s="T28">один.[NOM.SG]</ta>
            <ta e="T30" id="Seg_1297" s="T29">чашка.[NOM.SG]</ta>
            <ta e="T31" id="Seg_1298" s="T30">большой.[NOM.SG]</ta>
            <ta e="T32" id="Seg_1299" s="T31">один.[NOM.SG]</ta>
            <ta e="T33" id="Seg_1300" s="T32">маленький.[NOM.SG]</ta>
            <ta e="T34" id="Seg_1301" s="T33">а</ta>
            <ta e="T35" id="Seg_1302" s="T34">три.[NOM.SG]</ta>
            <ta e="T36" id="Seg_1303" s="T35">чашка.[NOM.SG]</ta>
            <ta e="T37" id="Seg_1304" s="T36">немного</ta>
            <ta e="T38" id="Seg_1305" s="T37">этот</ta>
            <ta e="T39" id="Seg_1306" s="T38">сесть-PST.[3SG]</ta>
            <ta e="T40" id="Seg_1307" s="T39">большой.[NOM.SG]</ta>
            <ta e="T41" id="Seg_1308" s="T40">чашка-LAT</ta>
            <ta e="T42" id="Seg_1309" s="T41">%%</ta>
            <ta e="T43" id="Seg_1310" s="T42">съесть-PST.[3SG]</ta>
            <ta e="T44" id="Seg_1311" s="T43">съесть-PST.[3SG]</ta>
            <ta e="T45" id="Seg_1312" s="T44">что.[NOM.SG]=INDEF</ta>
            <ta e="T46" id="Seg_1313" s="T45">NEG</ta>
            <ta e="T47" id="Seg_1314" s="T46">хороший</ta>
            <ta e="T48" id="Seg_1315" s="T47">тогда</ta>
            <ta e="T49" id="Seg_1316" s="T48">два-%%</ta>
            <ta e="T50" id="Seg_1317" s="T49">сесть-PST.[3SG]</ta>
            <ta e="T51" id="Seg_1318" s="T50">тоже</ta>
            <ta e="T52" id="Seg_1319" s="T51">%%</ta>
            <ta e="T53" id="Seg_1320" s="T52">съесть-PST.[3SG]</ta>
            <ta e="T54" id="Seg_1321" s="T53">NEG</ta>
            <ta e="T55" id="Seg_1322" s="T54">хороший</ta>
            <ta e="T56" id="Seg_1323" s="T55">тогда</ta>
            <ta e="T57" id="Seg_1324" s="T56">три-LAT</ta>
            <ta e="T58" id="Seg_1325" s="T57">сидеть-PST.[3SG]</ta>
            <ta e="T59" id="Seg_1326" s="T58">маленький.[NOM.SG]</ta>
            <ta e="T60" id="Seg_1327" s="T59">чашка-LAT</ta>
            <ta e="T61" id="Seg_1328" s="T60">здесь</ta>
            <ta e="T62" id="Seg_1329" s="T61">хороший</ta>
            <ta e="T63" id="Seg_1330" s="T62">съесть-PST.[3SG]</ta>
            <ta e="T64" id="Seg_1331" s="T63">съесть-PST.[3SG]</ta>
            <ta e="T65" id="Seg_1332" s="T64">тогда</ta>
            <ta e="T66" id="Seg_1333" s="T65">пойти-PST.[3SG]</ta>
            <ta e="T67" id="Seg_1334" s="T66">один.[NOM.SG]</ta>
            <ta e="T68" id="Seg_1335" s="T67">кровать-LAT</ta>
            <ta e="T70" id="Seg_1336" s="T69">ложиться-PST.[3SG]</ta>
            <ta e="T71" id="Seg_1337" s="T70">большой.[NOM.SG]</ta>
            <ta e="T72" id="Seg_1338" s="T71">тогда</ta>
            <ta e="T73" id="Seg_1339" s="T72">два.[NOM.SG]</ta>
            <ta e="T74" id="Seg_1340" s="T73">кровать-LAT</ta>
            <ta e="T75" id="Seg_1341" s="T74">ложиться-PST.[3SG]</ta>
            <ta e="T76" id="Seg_1342" s="T75">большой.[NOM.SG]</ta>
            <ta e="T77" id="Seg_1343" s="T76">тогда</ta>
            <ta e="T78" id="Seg_1344" s="T77">три.[NOM.SG]</ta>
            <ta e="T79" id="Seg_1345" s="T78">кровать-LAT</ta>
            <ta e="T80" id="Seg_1346" s="T79">ложиться-PST.[3SG]</ta>
            <ta e="T81" id="Seg_1347" s="T80">здесь</ta>
            <ta e="T82" id="Seg_1348" s="T81">хороший</ta>
            <ta e="T83" id="Seg_1349" s="T82">маленький.[NOM.SG]</ta>
            <ta e="T84" id="Seg_1350" s="T83">и</ta>
            <ta e="T86" id="Seg_1351" s="T85">и</ta>
            <ta e="T88" id="Seg_1352" s="T87">спать-DUR.[3SG]</ta>
            <ta e="T89" id="Seg_1353" s="T88">тогда</ta>
            <ta e="T90" id="Seg_1354" s="T89">медведь-NOM/GEN/ACC.3PL</ta>
            <ta e="T91" id="Seg_1355" s="T90">прийти-PST-3PL</ta>
            <ta e="T92" id="Seg_1356" s="T91">кто.[NOM.SG]</ta>
            <ta e="T93" id="Seg_1357" s="T92">сидеть-PST.[3SG]</ta>
            <ta e="T94" id="Seg_1358" s="T93">я.NOM</ta>
            <ta e="T95" id="Seg_1359" s="T94">чашка-LAT</ta>
            <ta e="T96" id="Seg_1360" s="T95">PTCL</ta>
            <ta e="T97" id="Seg_1361" s="T96">кричать-DUR-3PL</ta>
            <ta e="T98" id="Seg_1362" s="T97">тогда</ta>
            <ta e="T99" id="Seg_1363" s="T98">пойти-PST-3PL</ta>
            <ta e="T100" id="Seg_1364" s="T99">спать-INF.LAT</ta>
            <ta e="T101" id="Seg_1365" s="T100">кто.[NOM.SG]</ta>
            <ta e="T102" id="Seg_1366" s="T101">я.NOM</ta>
            <ta e="T103" id="Seg_1367" s="T102">кровать-NOM/GEN/ACC.3PL</ta>
            <ta e="T104" id="Seg_1368" s="T103">PTCL</ta>
            <ta e="T105" id="Seg_1369" s="T104">здесь</ta>
            <ta e="T106" id="Seg_1370" s="T105">лежать-DUR-PST.[3SG]</ta>
            <ta e="T107" id="Seg_1371" s="T106">а</ta>
            <ta e="T108" id="Seg_1372" s="T107">этот.[NOM.SG]</ta>
            <ta e="T109" id="Seg_1373" s="T108">маленький.[NOM.SG]</ta>
            <ta e="T110" id="Seg_1374" s="T109">медведь.[NOM.SG]</ta>
            <ta e="T111" id="Seg_1375" s="T110">PTCL</ta>
            <ta e="T113" id="Seg_1376" s="T112">худой.[NOM.SG]</ta>
            <ta e="T114" id="Seg_1377" s="T113">PTCL</ta>
            <ta e="T115" id="Seg_1378" s="T114">кричать-DUR.[3SG]</ta>
            <ta e="T116" id="Seg_1379" s="T115">кто.[NOM.SG]=INDEF</ta>
            <ta e="T117" id="Seg_1380" s="T116">здесь</ta>
            <ta e="T118" id="Seg_1381" s="T117">лежать-DUR.[3SG]</ta>
            <ta e="T119" id="Seg_1382" s="T118">этот.[NOM.SG]</ta>
            <ta e="T121" id="Seg_1383" s="T120">прыгнуть-MOM-PST.[3SG]</ta>
            <ta e="T122" id="Seg_1384" s="T121">и</ta>
            <ta e="T123" id="Seg_1385" s="T122">окно-LAT</ta>
            <ta e="T124" id="Seg_1386" s="T123">бежать-MOM-PST.[3SG]</ta>
            <ta e="T125" id="Seg_1387" s="T124">и</ta>
            <ta e="T126" id="Seg_1388" s="T125">прийти-PST.[3SG]</ta>
            <ta e="T127" id="Seg_1389" s="T126">бабушка-LAT</ta>
            <ta e="T128" id="Seg_1390" s="T127">и</ta>
            <ta e="T129" id="Seg_1391" s="T128">дедушка.[NOM.SG]</ta>
            <ta e="T130" id="Seg_1392" s="T129">мужчина-LAT</ta>
            <ta e="T131" id="Seg_1393" s="T130">и</ta>
            <ta e="T132" id="Seg_1394" s="T131">женщина-LAT</ta>
            <ta e="T133" id="Seg_1395" s="T132">прийти-PST.[3SG]</ta>
            <ta e="T134" id="Seg_1396" s="T133">и</ta>
            <ta e="T135" id="Seg_1397" s="T134">опять</ta>
            <ta e="T136" id="Seg_1398" s="T135">жить-PST-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1399" s="T0">v-v:tense-v:pn</ta>
            <ta e="T2" id="Seg_1400" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_1401" s="T2">conj</ta>
            <ta e="T4" id="Seg_1402" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_1403" s="T4">dempro-n:case</ta>
            <ta e="T6" id="Seg_1404" s="T5">dempro-n:num-n:case</ta>
            <ta e="T7" id="Seg_1405" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_1406" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_1407" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_1408" s="T9">dempro-n:case</ta>
            <ta e="T12" id="Seg_1409" s="T11">v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_1410" s="T12">n-n&gt;v-v:n.fin</ta>
            <ta e="T14" id="Seg_1411" s="T13">conj</ta>
            <ta e="T15" id="Seg_1412" s="T14">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_1413" s="T15">v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_1414" s="T16">v-v:tense-v:pn</ta>
            <ta e="T18" id="Seg_1415" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_1416" s="T18">n-n:case</ta>
            <ta e="T20" id="Seg_1417" s="T19">n-n:case.poss</ta>
            <ta e="T21" id="Seg_1418" s="T20">v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_1419" s="T21">ptcl</ta>
            <ta e="T23" id="Seg_1420" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_1421" s="T23">v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_1422" s="T24">n-n:num</ta>
            <ta e="T26" id="Seg_1423" s="T25">v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_1424" s="T26">n-n:num</ta>
            <ta e="T28" id="Seg_1425" s="T27">v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_1426" s="T28">num-n:case</ta>
            <ta e="T30" id="Seg_1427" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_1428" s="T30">adj-n:case</ta>
            <ta e="T32" id="Seg_1429" s="T31">num-n:case</ta>
            <ta e="T33" id="Seg_1430" s="T32">adj-n:case</ta>
            <ta e="T34" id="Seg_1431" s="T33">conj</ta>
            <ta e="T35" id="Seg_1432" s="T34">num-n:case</ta>
            <ta e="T36" id="Seg_1433" s="T35">n-n:case</ta>
            <ta e="T37" id="Seg_1434" s="T36">adv</ta>
            <ta e="T38" id="Seg_1435" s="T37">dempro</ta>
            <ta e="T39" id="Seg_1436" s="T38">v-v:tense-v:pn</ta>
            <ta e="T40" id="Seg_1437" s="T39">adj-n:case</ta>
            <ta e="T41" id="Seg_1438" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_1439" s="T41">%%</ta>
            <ta e="T43" id="Seg_1440" s="T42">v-v:tense-v:pn</ta>
            <ta e="T44" id="Seg_1441" s="T43">v-v:tense-v:pn</ta>
            <ta e="T45" id="Seg_1442" s="T44">que-n:case=ptcl</ta>
            <ta e="T46" id="Seg_1443" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_1444" s="T46">adj</ta>
            <ta e="T48" id="Seg_1445" s="T47">adv</ta>
            <ta e="T49" id="Seg_1446" s="T48">num-%%</ta>
            <ta e="T50" id="Seg_1447" s="T49">v-v:tense-v:pn</ta>
            <ta e="T51" id="Seg_1448" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_1449" s="T51">%%</ta>
            <ta e="T53" id="Seg_1450" s="T52">v-v:tense-v:pn</ta>
            <ta e="T54" id="Seg_1451" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_1452" s="T54">adj</ta>
            <ta e="T56" id="Seg_1453" s="T55">adv</ta>
            <ta e="T57" id="Seg_1454" s="T56">num-n:case</ta>
            <ta e="T58" id="Seg_1455" s="T57">v-v:tense-v:pn</ta>
            <ta e="T59" id="Seg_1456" s="T58">adj-n:case</ta>
            <ta e="T60" id="Seg_1457" s="T59">n-n:case</ta>
            <ta e="T61" id="Seg_1458" s="T60">adv</ta>
            <ta e="T62" id="Seg_1459" s="T61">adj</ta>
            <ta e="T63" id="Seg_1460" s="T62">v-v:tense-v:pn</ta>
            <ta e="T64" id="Seg_1461" s="T63">v-v:tense-v:pn</ta>
            <ta e="T65" id="Seg_1462" s="T64">adv</ta>
            <ta e="T66" id="Seg_1463" s="T65">v-v:tense-v:pn</ta>
            <ta e="T67" id="Seg_1464" s="T66">num-n:case</ta>
            <ta e="T68" id="Seg_1465" s="T67">n-n:case</ta>
            <ta e="T70" id="Seg_1466" s="T69">v-v:tense-v:pn</ta>
            <ta e="T71" id="Seg_1467" s="T70">adj-n:case</ta>
            <ta e="T72" id="Seg_1468" s="T71">adv</ta>
            <ta e="T73" id="Seg_1469" s="T72">num-n:case</ta>
            <ta e="T74" id="Seg_1470" s="T73">n-n:case</ta>
            <ta e="T75" id="Seg_1471" s="T74">v-v:tense-v:pn</ta>
            <ta e="T76" id="Seg_1472" s="T75">adj-n:case</ta>
            <ta e="T77" id="Seg_1473" s="T76">adv</ta>
            <ta e="T78" id="Seg_1474" s="T77">num-n:case</ta>
            <ta e="T79" id="Seg_1475" s="T78">n-n:case</ta>
            <ta e="T80" id="Seg_1476" s="T79">v-v:tense-v:pn</ta>
            <ta e="T81" id="Seg_1477" s="T80">adv</ta>
            <ta e="T82" id="Seg_1478" s="T81">adj</ta>
            <ta e="T83" id="Seg_1479" s="T82">adj-n:case</ta>
            <ta e="T84" id="Seg_1480" s="T83">conj</ta>
            <ta e="T86" id="Seg_1481" s="T85">conj</ta>
            <ta e="T88" id="Seg_1482" s="T87">v-v&gt;v-v:pn</ta>
            <ta e="T89" id="Seg_1483" s="T88">adv</ta>
            <ta e="T90" id="Seg_1484" s="T89">n-n:case.poss</ta>
            <ta e="T91" id="Seg_1485" s="T90">v-v:tense-v:pn</ta>
            <ta e="T92" id="Seg_1486" s="T91">que-n:case</ta>
            <ta e="T93" id="Seg_1487" s="T92">v-v:tense-v:pn</ta>
            <ta e="T94" id="Seg_1488" s="T93">pers</ta>
            <ta e="T95" id="Seg_1489" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_1490" s="T95">ptcl</ta>
            <ta e="T97" id="Seg_1491" s="T96">v-v&gt;v-v:pn</ta>
            <ta e="T98" id="Seg_1492" s="T97">adv</ta>
            <ta e="T99" id="Seg_1493" s="T98">v-v:tense-v:pn</ta>
            <ta e="T100" id="Seg_1494" s="T99">v-v:n.fin</ta>
            <ta e="T101" id="Seg_1495" s="T100">que-n:case</ta>
            <ta e="T102" id="Seg_1496" s="T101">pers</ta>
            <ta e="T103" id="Seg_1497" s="T102">n-n:case.poss</ta>
            <ta e="T104" id="Seg_1498" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_1499" s="T104">adv</ta>
            <ta e="T106" id="Seg_1500" s="T105">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T107" id="Seg_1501" s="T106">conj</ta>
            <ta e="T108" id="Seg_1502" s="T107">dempro-n:case</ta>
            <ta e="T109" id="Seg_1503" s="T108">adj-n:case</ta>
            <ta e="T110" id="Seg_1504" s="T109">n-n:case</ta>
            <ta e="T111" id="Seg_1505" s="T110">ptcl</ta>
            <ta e="T113" id="Seg_1506" s="T112">adj-n:case</ta>
            <ta e="T114" id="Seg_1507" s="T113">ptcl</ta>
            <ta e="T115" id="Seg_1508" s="T114">v-v&gt;v-v:pn</ta>
            <ta e="T116" id="Seg_1509" s="T115">que-n:case=ptcl</ta>
            <ta e="T117" id="Seg_1510" s="T116">adv</ta>
            <ta e="T118" id="Seg_1511" s="T117">v-v&gt;v-v:pn</ta>
            <ta e="T119" id="Seg_1512" s="T118">dempro-n:case</ta>
            <ta e="T121" id="Seg_1513" s="T120">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T122" id="Seg_1514" s="T121">conj</ta>
            <ta e="T123" id="Seg_1515" s="T122">n-n:case</ta>
            <ta e="T124" id="Seg_1516" s="T123">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T125" id="Seg_1517" s="T124">conj</ta>
            <ta e="T126" id="Seg_1518" s="T125">v-v:tense-v:pn</ta>
            <ta e="T127" id="Seg_1519" s="T126">n-n:case</ta>
            <ta e="T128" id="Seg_1520" s="T127">conj</ta>
            <ta e="T129" id="Seg_1521" s="T128">n-n:case</ta>
            <ta e="T130" id="Seg_1522" s="T129">n-n:case</ta>
            <ta e="T131" id="Seg_1523" s="T130">conj</ta>
            <ta e="T132" id="Seg_1524" s="T131">n-n:case</ta>
            <ta e="T133" id="Seg_1525" s="T132">v-v:tense-v:pn</ta>
            <ta e="T134" id="Seg_1526" s="T133">conj</ta>
            <ta e="T135" id="Seg_1527" s="T134">adv</ta>
            <ta e="T136" id="Seg_1528" s="T135">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1529" s="T0">v</ta>
            <ta e="T2" id="Seg_1530" s="T1">n</ta>
            <ta e="T3" id="Seg_1531" s="T2">conj</ta>
            <ta e="T4" id="Seg_1532" s="T3">n</ta>
            <ta e="T5" id="Seg_1533" s="T4">dempro</ta>
            <ta e="T6" id="Seg_1534" s="T5">dempro</ta>
            <ta e="T7" id="Seg_1535" s="T6">n</ta>
            <ta e="T8" id="Seg_1536" s="T7">v</ta>
            <ta e="T9" id="Seg_1537" s="T8">n</ta>
            <ta e="T10" id="Seg_1538" s="T9">dempro</ta>
            <ta e="T12" id="Seg_1539" s="T11">v</ta>
            <ta e="T13" id="Seg_1540" s="T12">n</ta>
            <ta e="T14" id="Seg_1541" s="T13">conj</ta>
            <ta e="T15" id="Seg_1542" s="T14">v</ta>
            <ta e="T16" id="Seg_1543" s="T15">v</ta>
            <ta e="T17" id="Seg_1544" s="T16">v</ta>
            <ta e="T18" id="Seg_1545" s="T17">v</ta>
            <ta e="T19" id="Seg_1546" s="T18">n</ta>
            <ta e="T20" id="Seg_1547" s="T19">n</ta>
            <ta e="T21" id="Seg_1548" s="T20">v</ta>
            <ta e="T22" id="Seg_1549" s="T21">ptcl</ta>
            <ta e="T23" id="Seg_1550" s="T22">n</ta>
            <ta e="T24" id="Seg_1551" s="T23">v</ta>
            <ta e="T25" id="Seg_1552" s="T24">n</ta>
            <ta e="T26" id="Seg_1553" s="T25">v</ta>
            <ta e="T27" id="Seg_1554" s="T26">n</ta>
            <ta e="T28" id="Seg_1555" s="T27">v</ta>
            <ta e="T29" id="Seg_1556" s="T28">num</ta>
            <ta e="T30" id="Seg_1557" s="T29">n</ta>
            <ta e="T31" id="Seg_1558" s="T30">adj</ta>
            <ta e="T32" id="Seg_1559" s="T31">num</ta>
            <ta e="T33" id="Seg_1560" s="T32">adj</ta>
            <ta e="T34" id="Seg_1561" s="T33">conj</ta>
            <ta e="T35" id="Seg_1562" s="T34">num</ta>
            <ta e="T36" id="Seg_1563" s="T35">n</ta>
            <ta e="T37" id="Seg_1564" s="T36">adj</ta>
            <ta e="T38" id="Seg_1565" s="T37">dempro</ta>
            <ta e="T39" id="Seg_1566" s="T38">v</ta>
            <ta e="T40" id="Seg_1567" s="T39">adj</ta>
            <ta e="T41" id="Seg_1568" s="T40">n</ta>
            <ta e="T43" id="Seg_1569" s="T42">v</ta>
            <ta e="T44" id="Seg_1570" s="T43">v</ta>
            <ta e="T45" id="Seg_1571" s="T44">que</ta>
            <ta e="T46" id="Seg_1572" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_1573" s="T46">adj</ta>
            <ta e="T48" id="Seg_1574" s="T47">adv</ta>
            <ta e="T49" id="Seg_1575" s="T48">num</ta>
            <ta e="T50" id="Seg_1576" s="T49">v</ta>
            <ta e="T51" id="Seg_1577" s="T50">ptcl</ta>
            <ta e="T53" id="Seg_1578" s="T52">v</ta>
            <ta e="T54" id="Seg_1579" s="T53">ptcl</ta>
            <ta e="T55" id="Seg_1580" s="T54">adj</ta>
            <ta e="T56" id="Seg_1581" s="T55">adv</ta>
            <ta e="T57" id="Seg_1582" s="T56">num</ta>
            <ta e="T58" id="Seg_1583" s="T57">v</ta>
            <ta e="T59" id="Seg_1584" s="T58">adj</ta>
            <ta e="T60" id="Seg_1585" s="T59">n</ta>
            <ta e="T61" id="Seg_1586" s="T60">adv</ta>
            <ta e="T62" id="Seg_1587" s="T61">adj</ta>
            <ta e="T63" id="Seg_1588" s="T62">v</ta>
            <ta e="T64" id="Seg_1589" s="T63">v</ta>
            <ta e="T65" id="Seg_1590" s="T64">adv</ta>
            <ta e="T66" id="Seg_1591" s="T65">v</ta>
            <ta e="T67" id="Seg_1592" s="T66">num</ta>
            <ta e="T68" id="Seg_1593" s="T67">n</ta>
            <ta e="T70" id="Seg_1594" s="T69">v</ta>
            <ta e="T71" id="Seg_1595" s="T70">adj</ta>
            <ta e="T72" id="Seg_1596" s="T71">adv</ta>
            <ta e="T73" id="Seg_1597" s="T72">num</ta>
            <ta e="T74" id="Seg_1598" s="T73">n</ta>
            <ta e="T75" id="Seg_1599" s="T74">v</ta>
            <ta e="T76" id="Seg_1600" s="T75">adj</ta>
            <ta e="T77" id="Seg_1601" s="T76">adv</ta>
            <ta e="T78" id="Seg_1602" s="T77">num</ta>
            <ta e="T79" id="Seg_1603" s="T78">n</ta>
            <ta e="T80" id="Seg_1604" s="T79">v</ta>
            <ta e="T81" id="Seg_1605" s="T80">adv</ta>
            <ta e="T82" id="Seg_1606" s="T81">adj</ta>
            <ta e="T83" id="Seg_1607" s="T82">adj</ta>
            <ta e="T84" id="Seg_1608" s="T83">conj</ta>
            <ta e="T86" id="Seg_1609" s="T85">conj</ta>
            <ta e="T88" id="Seg_1610" s="T87">v</ta>
            <ta e="T89" id="Seg_1611" s="T88">adv</ta>
            <ta e="T90" id="Seg_1612" s="T89">n</ta>
            <ta e="T91" id="Seg_1613" s="T90">v</ta>
            <ta e="T92" id="Seg_1614" s="T91">que</ta>
            <ta e="T93" id="Seg_1615" s="T92">v</ta>
            <ta e="T94" id="Seg_1616" s="T93">pers</ta>
            <ta e="T95" id="Seg_1617" s="T94">n</ta>
            <ta e="T96" id="Seg_1618" s="T95">ptcl</ta>
            <ta e="T97" id="Seg_1619" s="T96">v</ta>
            <ta e="T98" id="Seg_1620" s="T97">adv</ta>
            <ta e="T99" id="Seg_1621" s="T98">v</ta>
            <ta e="T100" id="Seg_1622" s="T99">v</ta>
            <ta e="T101" id="Seg_1623" s="T100">que</ta>
            <ta e="T102" id="Seg_1624" s="T101">pers</ta>
            <ta e="T103" id="Seg_1625" s="T102">n</ta>
            <ta e="T104" id="Seg_1626" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_1627" s="T104">adv</ta>
            <ta e="T106" id="Seg_1628" s="T105">v</ta>
            <ta e="T107" id="Seg_1629" s="T106">conj</ta>
            <ta e="T108" id="Seg_1630" s="T107">dempro</ta>
            <ta e="T109" id="Seg_1631" s="T108">adj</ta>
            <ta e="T110" id="Seg_1632" s="T109">n</ta>
            <ta e="T111" id="Seg_1633" s="T110">ptcl</ta>
            <ta e="T113" id="Seg_1634" s="T112">adj</ta>
            <ta e="T114" id="Seg_1635" s="T113">ptcl</ta>
            <ta e="T115" id="Seg_1636" s="T114">v</ta>
            <ta e="T116" id="Seg_1637" s="T115">que</ta>
            <ta e="T117" id="Seg_1638" s="T116">adv</ta>
            <ta e="T118" id="Seg_1639" s="T117">v</ta>
            <ta e="T119" id="Seg_1640" s="T118">dempro</ta>
            <ta e="T121" id="Seg_1641" s="T120">v</ta>
            <ta e="T122" id="Seg_1642" s="T121">conj</ta>
            <ta e="T123" id="Seg_1643" s="T122">n</ta>
            <ta e="T124" id="Seg_1644" s="T123">v</ta>
            <ta e="T125" id="Seg_1645" s="T124">conj</ta>
            <ta e="T126" id="Seg_1646" s="T125">v</ta>
            <ta e="T127" id="Seg_1647" s="T126">n</ta>
            <ta e="T128" id="Seg_1648" s="T127">conj</ta>
            <ta e="T129" id="Seg_1649" s="T128">n</ta>
            <ta e="T130" id="Seg_1650" s="T129">n</ta>
            <ta e="T131" id="Seg_1651" s="T130">conj</ta>
            <ta e="T132" id="Seg_1652" s="T131">n</ta>
            <ta e="T133" id="Seg_1653" s="T132">v</ta>
            <ta e="T134" id="Seg_1654" s="T133">conj</ta>
            <ta e="T135" id="Seg_1655" s="T134">adv</ta>
            <ta e="T136" id="Seg_1656" s="T135">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_1657" s="T1">np.h:E</ta>
            <ta e="T4" id="Seg_1658" s="T3">np.h:E</ta>
            <ta e="T5" id="Seg_1659" s="T4">pro.h:Poss</ta>
            <ta e="T6" id="Seg_1660" s="T5">pro:L</ta>
            <ta e="T7" id="Seg_1661" s="T6">np.h:Th</ta>
            <ta e="T9" id="Seg_1662" s="T8">np.h:Th</ta>
            <ta e="T10" id="Seg_1663" s="T9">pro.h:A</ta>
            <ta e="T15" id="Seg_1664" s="T14">0.3.h:E</ta>
            <ta e="T16" id="Seg_1665" s="T15">0.3.h:A</ta>
            <ta e="T17" id="Seg_1666" s="T16">0.3.h:A</ta>
            <ta e="T18" id="Seg_1667" s="T17">0.3.h:A</ta>
            <ta e="T19" id="Seg_1668" s="T18">np.h:Poss</ta>
            <ta e="T20" id="Seg_1669" s="T19">np:L</ta>
            <ta e="T21" id="Seg_1670" s="T20">0.3.h:E</ta>
            <ta e="T23" id="Seg_1671" s="T22">np:Th</ta>
            <ta e="T25" id="Seg_1672" s="T24">np:Th</ta>
            <ta e="T27" id="Seg_1673" s="T26">np:Th</ta>
            <ta e="T30" id="Seg_1674" s="T29">np:Th</ta>
            <ta e="T32" id="Seg_1675" s="T31">np:Th</ta>
            <ta e="T36" id="Seg_1676" s="T35">np:Th</ta>
            <ta e="T38" id="Seg_1677" s="T37">pro.h:A</ta>
            <ta e="T41" id="Seg_1678" s="T40">np:L</ta>
            <ta e="T44" id="Seg_1679" s="T43">0.3.h:A</ta>
            <ta e="T48" id="Seg_1680" s="T47">adv:Time</ta>
            <ta e="T50" id="Seg_1681" s="T49">0.3.h:A</ta>
            <ta e="T53" id="Seg_1682" s="T52">0.3.h:A</ta>
            <ta e="T56" id="Seg_1683" s="T55">adv:Time</ta>
            <ta e="T58" id="Seg_1684" s="T57">0.3.h:E</ta>
            <ta e="T60" id="Seg_1685" s="T59">np:L</ta>
            <ta e="T61" id="Seg_1686" s="T60">adv:L</ta>
            <ta e="T63" id="Seg_1687" s="T62">0.3.h:A</ta>
            <ta e="T64" id="Seg_1688" s="T63">0.3.h:A</ta>
            <ta e="T65" id="Seg_1689" s="T64">adv:Time</ta>
            <ta e="T66" id="Seg_1690" s="T65">0.3.h:A</ta>
            <ta e="T67" id="Seg_1691" s="T66">np.h:A</ta>
            <ta e="T68" id="Seg_1692" s="T67">np:L</ta>
            <ta e="T72" id="Seg_1693" s="T71">adv:Time</ta>
            <ta e="T74" id="Seg_1694" s="T73">np:L</ta>
            <ta e="T75" id="Seg_1695" s="T74">0.3.h:A</ta>
            <ta e="T77" id="Seg_1696" s="T76">adv:Time</ta>
            <ta e="T79" id="Seg_1697" s="T78">np:L</ta>
            <ta e="T80" id="Seg_1698" s="T79">0.3.h:A</ta>
            <ta e="T81" id="Seg_1699" s="T80">adv:L</ta>
            <ta e="T88" id="Seg_1700" s="T87">0.3.h:A</ta>
            <ta e="T89" id="Seg_1701" s="T88">adv:Time</ta>
            <ta e="T90" id="Seg_1702" s="T89">np.h:A</ta>
            <ta e="T92" id="Seg_1703" s="T91">pro.h:A</ta>
            <ta e="T94" id="Seg_1704" s="T93">pro.h:Poss</ta>
            <ta e="T95" id="Seg_1705" s="T94">np:L</ta>
            <ta e="T97" id="Seg_1706" s="T96">0.3.h:A</ta>
            <ta e="T98" id="Seg_1707" s="T97">adv:Time</ta>
            <ta e="T99" id="Seg_1708" s="T98">0.3.h:A</ta>
            <ta e="T101" id="Seg_1709" s="T100">pro.h:E</ta>
            <ta e="T102" id="Seg_1710" s="T101">pro.h:Poss</ta>
            <ta e="T103" id="Seg_1711" s="T102">np:L</ta>
            <ta e="T105" id="Seg_1712" s="T104">adv:L</ta>
            <ta e="T110" id="Seg_1713" s="T109">np.h:A</ta>
            <ta e="T116" id="Seg_1714" s="T115">pro.h:E</ta>
            <ta e="T117" id="Seg_1715" s="T116">adv:L</ta>
            <ta e="T119" id="Seg_1716" s="T118">pro.h:A</ta>
            <ta e="T123" id="Seg_1717" s="T122">np:G</ta>
            <ta e="T124" id="Seg_1718" s="T123">0.3.h:A</ta>
            <ta e="T126" id="Seg_1719" s="T125">0.3.h:A</ta>
            <ta e="T127" id="Seg_1720" s="T126">np:G</ta>
            <ta e="T129" id="Seg_1721" s="T128">np:G</ta>
            <ta e="T130" id="Seg_1722" s="T129">np:G</ta>
            <ta e="T132" id="Seg_1723" s="T131">np:G</ta>
            <ta e="T133" id="Seg_1724" s="T132">0.3.h:A</ta>
            <ta e="T136" id="Seg_1725" s="T135">0.3.h:E</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_1726" s="T0">v:pred</ta>
            <ta e="T2" id="Seg_1727" s="T1">np.h:S</ta>
            <ta e="T4" id="Seg_1728" s="T3">np.h:S</ta>
            <ta e="T7" id="Seg_1729" s="T6">np.h:S</ta>
            <ta e="T8" id="Seg_1730" s="T7">cop</ta>
            <ta e="T9" id="Seg_1731" s="T8">n:pred</ta>
            <ta e="T10" id="Seg_1732" s="T9">pro.h:S</ta>
            <ta e="T12" id="Seg_1733" s="T11">v:pred</ta>
            <ta e="T13" id="Seg_1734" s="T12">conv:pred</ta>
            <ta e="T15" id="Seg_1735" s="T14">v:pred 0.3.h:S</ta>
            <ta e="T16" id="Seg_1736" s="T15">v:pred 0.3.h:S</ta>
            <ta e="T17" id="Seg_1737" s="T16">v:pred 0.3.h:S</ta>
            <ta e="T18" id="Seg_1738" s="T17">v:pred 0.3.h:S</ta>
            <ta e="T21" id="Seg_1739" s="T20">v:pred 0.3.h:S</ta>
            <ta e="T23" id="Seg_1740" s="T22">np:S</ta>
            <ta e="T24" id="Seg_1741" s="T23">v:pred</ta>
            <ta e="T25" id="Seg_1742" s="T24">np:S</ta>
            <ta e="T26" id="Seg_1743" s="T25">v:pred</ta>
            <ta e="T27" id="Seg_1744" s="T26">np:S</ta>
            <ta e="T28" id="Seg_1745" s="T27">v:pred</ta>
            <ta e="T30" id="Seg_1746" s="T29">np:S</ta>
            <ta e="T31" id="Seg_1747" s="T30">adj:pred</ta>
            <ta e="T32" id="Seg_1748" s="T31">np:S</ta>
            <ta e="T33" id="Seg_1749" s="T32">adj:pred</ta>
            <ta e="T36" id="Seg_1750" s="T35">np:S</ta>
            <ta e="T37" id="Seg_1751" s="T36">adj:pred</ta>
            <ta e="T38" id="Seg_1752" s="T37">pro.h:S</ta>
            <ta e="T39" id="Seg_1753" s="T38">v:pred</ta>
            <ta e="T44" id="Seg_1754" s="T43">v:pred 0.3.h:S</ta>
            <ta e="T46" id="Seg_1755" s="T45">ptcl.neg</ta>
            <ta e="T47" id="Seg_1756" s="T46">adj:pred</ta>
            <ta e="T50" id="Seg_1757" s="T49">v:pred 0.3.h:S</ta>
            <ta e="T53" id="Seg_1758" s="T52">v:pred 0.3.h:S</ta>
            <ta e="T54" id="Seg_1759" s="T53">ptcl.neg</ta>
            <ta e="T55" id="Seg_1760" s="T54">adj:pred</ta>
            <ta e="T58" id="Seg_1761" s="T57">v:pred 0.3.h:S</ta>
            <ta e="T62" id="Seg_1762" s="T61">adj:pred</ta>
            <ta e="T63" id="Seg_1763" s="T62">v:pred 0.3.h:S</ta>
            <ta e="T64" id="Seg_1764" s="T63">v:pred 0.3.h:S</ta>
            <ta e="T66" id="Seg_1765" s="T65">v:pred 0.3.h:S</ta>
            <ta e="T67" id="Seg_1766" s="T66">np.h:S</ta>
            <ta e="T70" id="Seg_1767" s="T69">v:pred</ta>
            <ta e="T71" id="Seg_1768" s="T70">adj:pred</ta>
            <ta e="T75" id="Seg_1769" s="T74">v:pred 0.3.h:S</ta>
            <ta e="T76" id="Seg_1770" s="T75">adj:pred</ta>
            <ta e="T80" id="Seg_1771" s="T79">v:pred 0.3.h:S</ta>
            <ta e="T82" id="Seg_1772" s="T81">adj:pred</ta>
            <ta e="T83" id="Seg_1773" s="T82">adj:pred</ta>
            <ta e="T88" id="Seg_1774" s="T87">v:pred 0.3.h:S</ta>
            <ta e="T90" id="Seg_1775" s="T89">np.h:S</ta>
            <ta e="T91" id="Seg_1776" s="T90">v:pred</ta>
            <ta e="T92" id="Seg_1777" s="T91">pro.h:S</ta>
            <ta e="T93" id="Seg_1778" s="T92">v:pred</ta>
            <ta e="T97" id="Seg_1779" s="T96">v:pred 0.3.h:S</ta>
            <ta e="T99" id="Seg_1780" s="T98">v:pred 0.3.h:S</ta>
            <ta e="T100" id="Seg_1781" s="T99">s:purp</ta>
            <ta e="T101" id="Seg_1782" s="T100">pro.h:S</ta>
            <ta e="T106" id="Seg_1783" s="T105">v:pred</ta>
            <ta e="T110" id="Seg_1784" s="T109">np.h:S</ta>
            <ta e="T115" id="Seg_1785" s="T114">v:pred</ta>
            <ta e="T116" id="Seg_1786" s="T115">pro.h:S</ta>
            <ta e="T118" id="Seg_1787" s="T117">v:pred</ta>
            <ta e="T119" id="Seg_1788" s="T118">pro.h:S</ta>
            <ta e="T121" id="Seg_1789" s="T120">v:pred</ta>
            <ta e="T124" id="Seg_1790" s="T123">v:pred 0.3.h:S</ta>
            <ta e="T126" id="Seg_1791" s="T125">v:pred 0.3.h:S</ta>
            <ta e="T133" id="Seg_1792" s="T132">v:pred 0.3.h:S</ta>
            <ta e="T136" id="Seg_1793" s="T135">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T3" id="Seg_1794" s="T2">RUS:gram</ta>
            <ta e="T9" id="Seg_1795" s="T8">RUS:core</ta>
            <ta e="T14" id="Seg_1796" s="T13">RUS:gram</ta>
            <ta e="T22" id="Seg_1797" s="T21">TURK:disc</ta>
            <ta e="T23" id="Seg_1798" s="T22">RUS:cult</ta>
            <ta e="T25" id="Seg_1799" s="T24">RUS:cult</ta>
            <ta e="T34" id="Seg_1800" s="T33">RUS:gram</ta>
            <ta e="T45" id="Seg_1801" s="T44">TURK:gram(INDEF)</ta>
            <ta e="T47" id="Seg_1802" s="T46">TURK:core</ta>
            <ta e="T51" id="Seg_1803" s="T50">RUS:mod</ta>
            <ta e="T55" id="Seg_1804" s="T54">TURK:core</ta>
            <ta e="T62" id="Seg_1805" s="T61">TURK:core</ta>
            <ta e="T68" id="Seg_1806" s="T67">RUS:cult</ta>
            <ta e="T74" id="Seg_1807" s="T73">RUS:cult</ta>
            <ta e="T79" id="Seg_1808" s="T78">RUS:cult</ta>
            <ta e="T82" id="Seg_1809" s="T81">TURK:core</ta>
            <ta e="T84" id="Seg_1810" s="T83">RUS:gram</ta>
            <ta e="T86" id="Seg_1811" s="T85">RUS:gram</ta>
            <ta e="T96" id="Seg_1812" s="T95">TURK:disc</ta>
            <ta e="T103" id="Seg_1813" s="T102">RUS:cult</ta>
            <ta e="T104" id="Seg_1814" s="T103">TURK:disc</ta>
            <ta e="T107" id="Seg_1815" s="T106">RUS:gram</ta>
            <ta e="T111" id="Seg_1816" s="T110">TURK:disc</ta>
            <ta e="T114" id="Seg_1817" s="T113">TURK:disc</ta>
            <ta e="T116" id="Seg_1818" s="T115">TURK:gram(INDEF)</ta>
            <ta e="T122" id="Seg_1819" s="T121">RUS:gram</ta>
            <ta e="T125" id="Seg_1820" s="T124">RUS:gram</ta>
            <ta e="T127" id="Seg_1821" s="T126">RUS:core</ta>
            <ta e="T128" id="Seg_1822" s="T127">RUS:gram</ta>
            <ta e="T129" id="Seg_1823" s="T128">RUS:core</ta>
            <ta e="T131" id="Seg_1824" s="T130">RUS:gram</ta>
            <ta e="T134" id="Seg_1825" s="T133">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_1826" s="T0">Жили женщина и мужчина.</ta>
            <ta e="T9" id="Seg_1827" s="T4">У них была внучка.</ta>
            <ta e="T13" id="Seg_1828" s="T9">Она пошла собирать ягоды.</ta>
            <ta e="T15" id="Seg_1829" s="T13">И потерялась.</ta>
            <ta e="T20" id="Seg_1830" s="T15">Шла-шла и пришла в дом к медведю.</ta>
            <ta e="T24" id="Seg_1831" s="T20">Видит: стоит стол.</ta>
            <ta e="T26" id="Seg_1832" s="T24">Стулья стоят.</ta>
            <ta e="T28" id="Seg_1833" s="T26">Чашки стоят.</ta>
            <ta e="T33" id="Seg_1834" s="T28">Одна чашка большая, одна маленькая.</ta>
            <ta e="T37" id="Seg_1835" s="T33">А третья чашка [совсем] маленькая.</ta>
            <ta e="T41" id="Seg_1836" s="T37">Она села к большой чашке.</ta>
            <ta e="T44" id="Seg_1837" s="T41">Поела (?).</ta>
            <ta e="T47" id="Seg_1838" s="T44">Почему-то невкусно.</ta>
            <ta e="T53" id="Seg_1839" s="T47">Потом она села ко второй чашке, тоже немного поела.</ta>
            <ta e="T55" id="Seg_1840" s="T53">Невкусно.</ta>
            <ta e="T60" id="Seg_1841" s="T55">Потом села к третьей, маленькой чашке.</ta>
            <ta e="T66" id="Seg_1842" s="T60">Тут вкусно, она поела-поела, потом ушла.</ta>
            <ta e="T71" id="Seg_1843" s="T66">В одну кровать легла - большая.</ta>
            <ta e="T76" id="Seg_1844" s="T71">Потом в другую кровать легла - большая.</ta>
            <ta e="T83" id="Seg_1845" s="T76">Потом в третью кровать легла - тут хорошо, она маленькая.</ta>
            <ta e="T88" id="Seg_1846" s="T83">И спит.</ta>
            <ta e="T91" id="Seg_1847" s="T88">Потом пришли медведи.</ta>
            <ta e="T97" id="Seg_1848" s="T91">"Кто (ел?) из моей чашки? - закричали они.</ta>
            <ta e="T100" id="Seg_1849" s="T97">Потом они пошли спать.</ta>
            <ta e="T106" id="Seg_1850" s="T100">"Кто лежал на моей кровати?"</ta>
            <ta e="T115" id="Seg_1851" s="T106">А этот маленький медведь закричал тонким голосом:</ta>
            <ta e="T118" id="Seg_1852" s="T115">"Здесь кто-то лежит!"</ta>
            <ta e="T124" id="Seg_1853" s="T118">Она прыгнула и убежала через окно.</ta>
            <ta e="T129" id="Seg_1854" s="T124">И пришла к бабушке и дедушке.</ta>
            <ta e="T133" id="Seg_1855" s="T129">Пришла к мужчине и женщине.</ta>
            <ta e="T136" id="Seg_1856" s="T133">И они жили дальше.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_1857" s="T0">[There] lived a woman and a man.</ta>
            <ta e="T9" id="Seg_1858" s="T4">They had a granddaughter.</ta>
            <ta e="T13" id="Seg_1859" s="T9">She went to gather berries.</ta>
            <ta e="T15" id="Seg_1860" s="T13">And got lost.</ta>
            <ta e="T20" id="Seg_1861" s="T15">She walked and came to a bear's house.</ta>
            <ta e="T24" id="Seg_1862" s="T20">She sees: there is a table.</ta>
            <ta e="T26" id="Seg_1863" s="T24">There are chairs.</ta>
            <ta e="T28" id="Seg_1864" s="T26">There are cups.</ta>
            <ta e="T33" id="Seg_1865" s="T28">One cup is big, another is small.</ta>
            <ta e="T37" id="Seg_1866" s="T33">And the third cup is tiny.</ta>
            <ta e="T41" id="Seg_1867" s="T37">She sat down near the big cup.</ta>
            <ta e="T44" id="Seg_1868" s="T41">She ate (?).</ta>
            <ta e="T47" id="Seg_1869" s="T44">For some reason it is not good.</ta>
            <ta e="T53" id="Seg_1870" s="T47">Then she sat to the second (cup), she ate from it, too.</ta>
            <ta e="T55" id="Seg_1871" s="T53">Not good.</ta>
            <ta e="T60" id="Seg_1872" s="T55">Then she sat to the third, small cup.</ta>
            <ta e="T66" id="Seg_1873" s="T60">Here [the food] is good, she ate, then she left.</ta>
            <ta e="T71" id="Seg_1874" s="T66">She lay down in one bed - it is [too] big.</ta>
            <ta e="T76" id="Seg_1875" s="T71">Then she lay down in the second bed - it is [too] big.</ta>
            <ta e="T83" id="Seg_1876" s="T76">Then she lay (down?) in the third bed - here it is good, it is small.</ta>
            <ta e="T88" id="Seg_1877" s="T83">And she's sleeping.</ta>
            <ta e="T91" id="Seg_1878" s="T88">Then the bears came.</ta>
            <ta e="T97" id="Seg_1879" s="T91">"Who (ate?) from my bowl?" - they shouted.</ta>
            <ta e="T100" id="Seg_1880" s="T97">Then they went to the bed.</ta>
            <ta e="T106" id="Seg_1881" s="T100">"Who lay on my bed?"</ta>
            <ta e="T115" id="Seg_1882" s="T106">And this little bear shouts in a thin voice:</ta>
            <ta e="T118" id="Seg_1883" s="T115">"Someone is lying here!"</ta>
            <ta e="T124" id="Seg_1884" s="T118">She jumped [up] and escaped through the window.</ta>
            <ta e="T129" id="Seg_1885" s="T124">And she came to her grandma and grandpa.</ta>
            <ta e="T133" id="Seg_1886" s="T129">She came to the man and the woman.</ta>
            <ta e="T136" id="Seg_1887" s="T133">And they lived again.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_1888" s="T0">Es lebten eine Frau und ein Mann.</ta>
            <ta e="T9" id="Seg_1889" s="T4">Sie hatten eine Enkelin.</ta>
            <ta e="T13" id="Seg_1890" s="T9">Sie ging Beeren pflücken.</ta>
            <ta e="T15" id="Seg_1891" s="T13">Und verirrte sich.</ta>
            <ta e="T20" id="Seg_1892" s="T15">Sie ging und kam zu einer Bärenhöhle.</ta>
            <ta e="T24" id="Seg_1893" s="T20">Sie sieht: Dort ist ein Tisch.</ta>
            <ta e="T26" id="Seg_1894" s="T24">Dort sind Stühle.</ta>
            <ta e="T28" id="Seg_1895" s="T26">Dort sind Tassen.</ta>
            <ta e="T33" id="Seg_1896" s="T28">Eine Tasse ist groß, eine ist klein.</ta>
            <ta e="T37" id="Seg_1897" s="T33">Und die dritte Tasse ist winzig.</ta>
            <ta e="T41" id="Seg_1898" s="T37">Sie setzte sich zur großen Tasse.</ta>
            <ta e="T44" id="Seg_1899" s="T41">Sie aß (?).</ta>
            <ta e="T47" id="Seg_1900" s="T44"> Aus irgendeinem Grund schmeckt das nicht.</ta>
            <ta e="T53" id="Seg_1901" s="T47">Dann setzte sie sich zur zweiten (Tasse), sie aß auch daraus.</ta>
            <ta e="T55" id="Seg_1902" s="T53">Nicht gut.</ta>
            <ta e="T60" id="Seg_1903" s="T55">Dann setzte sie sich zur dritten, zur kleinen Tasse.</ta>
            <ta e="T66" id="Seg_1904" s="T60">Hier schmeckt es, sie aß, dann ging sie weg.</ta>
            <ta e="T71" id="Seg_1905" s="T66">Sie legte sich in ein Bett - es ist [zu] groß.</ta>
            <ta e="T76" id="Seg_1906" s="T71">Dann legte sie sich in das zweite Bett - es ist [zu] groß.</ta>
            <ta e="T83" id="Seg_1907" s="T76">Dann (legte?) sie sich in das dritte Bett - hier ist es gut, es ist klein.</ta>
            <ta e="T88" id="Seg_1908" s="T83">Und sie schläft.</ta>
            <ta e="T91" id="Seg_1909" s="T88">Dann kamen die Bären.</ta>
            <ta e="T97" id="Seg_1910" s="T91">"Wer hat aus meiner Schüssel (gegessen?)", schrieen sie.</ta>
            <ta e="T100" id="Seg_1911" s="T97">Dann gingen sie ins Bett.</ta>
            <ta e="T106" id="Seg_1912" s="T100">"Wer hat sich auf mein Bett gelegt?"</ta>
            <ta e="T115" id="Seg_1913" s="T106">Und der kleine Bär schreit mit einer dünnen Stimme:</ta>
            <ta e="T118" id="Seg_1914" s="T115">"Hier liegt irgendjemand!"</ta>
            <ta e="T124" id="Seg_1915" s="T118">Sie sprang [auf] und entfloh durch das Fenster.</ta>
            <ta e="T129" id="Seg_1916" s="T124">Und sie kam zu ihrer Großmutter und ihrem Großvater.</ta>
            <ta e="T133" id="Seg_1917" s="T129">Sie kam zu dem Mann und der Frau.</ta>
            <ta e="T136" id="Seg_1918" s="T133">Und sie lebten wieder.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T4" id="Seg_1919" s="T0">[GVY:] Tale "Three bears", see e.g. https://deti-online.com/skazki/russkie-narodnye-skazki/tri-medvedya/</ta>
            <ta e="T37" id="Seg_1920" s="T33">[GVY:] idʼiʔiʔe?</ta>
            <ta e="T97" id="Seg_1921" s="T91">[GVY:] Or "whe sat to my bowl"?</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
