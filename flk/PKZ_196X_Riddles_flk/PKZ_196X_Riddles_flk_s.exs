<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID30737B9D-DA7B-D2D3-F5DA-EE216F118C57">
   <head>
      <meta-information>
         <project-name>Kamas</project-name>
         <transcription-name>PKZ_196X_Riddles_flk</transcription-name>
         <referenced-file url="PKZ_196X_Riddles_flk.wav" />
         <referenced-file url="PKZ_196X_Riddles_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_Riddles_flk\PKZ_196X_Riddles_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">244</ud-information>
            <ud-information attribute-name="# HIAT:w">143</ud-information>
            <ud-information attribute-name="# e">151</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">4</ud-information>
            <ud-information attribute-name="# HIAT:u">32</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="f" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="1.045" type="appl" />
         <tli id="T2" time="2.09" type="appl" />
         <tli id="T3" time="3.134" type="appl" />
         <tli id="T4" time="4.179" type="appl" />
         <tli id="T5" time="5.224" type="appl" />
         <tli id="T6" time="6.268" type="appl" />
         <tli id="T7" time="7.313" type="appl" />
         <tli id="T8" time="8.358" type="appl" />
         <tli id="T9" time="9.453192202695785" />
         <tli id="T10" time="10.496" type="appl" />
         <tli id="T11" time="11.322" type="appl" />
         <tli id="T12" time="12.149" type="appl" />
         <tli id="T13" time="12.976" type="appl" />
         <tli id="T14" time="13.802" type="appl" />
         <tli id="T15" time="15.126440837741" />
         <tli id="T16" time="15.806" type="appl" />
         <tli id="T17" time="16.319" type="appl" />
         <tli id="T18" time="16.832" type="appl" />
         <tli id="T19" time="17.64640321617471" />
         <tli id="T20" time="18.541" type="appl" />
         <tli id="T21" time="19.397" type="appl" />
         <tli id="T22" time="21.613010663709264" />
         <tli id="T23" time="22.759" type="appl" />
         <tli id="T24" time="24.406302294830233" />
         <tli id="T25" time="25.004" type="appl" />
         <tli id="T26" time="31.232867044872886" />
         <tli id="T27" time="32.174" type="appl" />
         <tli id="T28" time="33.058" type="appl" />
         <tli id="T29" time="33.941" type="appl" />
         <tli id="T30" time="34.825" type="appl" />
         <tli id="T31" time="35.709" type="appl" />
         <tli id="T32" time="36.11" type="appl" />
         <tli id="T33" time="36.457" type="appl" />
         <tli id="T34" time="36.804" type="appl" />
         <tli id="T35" time="37.15" type="appl" />
         <tli id="T36" time="37.827" type="appl" />
         <tli id="T37" time="40.00606939941989" />
         <tli id="T38" time="41.048" type="appl" />
         <tli id="T39" time="42.126" type="appl" />
         <tli id="T40" time="43.204" type="appl" />
         <tli id="T41" time="44.281" type="appl" />
         <tli id="T42" time="45.359" type="appl" />
         <tli id="T43" time="46.437" type="appl" />
         <tli id="T44" time="47.55928996758232" />
         <tli id="T45" time="48.552608471250636" />
         <tli id="T46" time="49.109" type="appl" />
         <tli id="T47" time="49.796" type="appl" />
         <tli id="T48" time="50.484" type="appl" />
         <tli id="T49" time="51.171" type="appl" />
         <tli id="T50" time="51.858" type="appl" />
         <tli id="T51" time="52.546" type="appl" />
         <tli id="T52" time="53.233" type="appl" />
         <tli id="T53" time="54.2525233748507" />
         <tli id="T54" time="55.115" type="appl" />
         <tli id="T55" time="56.5124896348746" />
         <tli id="T56" time="57.637" type="appl" />
         <tli id="T57" time="58.474" type="appl" />
         <tli id="T58" time="59.311" type="appl" />
         <tli id="T59" time="60.149" type="appl" />
         <tli id="T60" time="60.986" type="appl" />
         <tli id="T61" time="61.823" type="appl" />
         <tli id="T62" time="62.66" type="appl" />
         <tli id="T63" time="70.19895197065348" />
         <tli id="T64" time="71.067" type="appl" />
         <tli id="T65" time="71.853" type="appl" />
         <tli id="T66" time="72.64" type="appl" />
         <tli id="T67" time="73.427" type="appl" />
         <tli id="T68" time="74.213" type="appl" />
         <tli id="T69" time="75.0" type="appl" />
         <tli id="T70" time="76.922" type="appl" />
         <tli id="T71" time="77.813" type="appl" />
         <tli id="T72" time="78.705" type="appl" />
         <tli id="T73" time="79.597" type="appl" />
         <tli id="T74" time="80.488" type="appl" />
         <tli id="T75" time="81.38" type="appl" />
         <tli id="T76" time="81.9" type="appl" />
         <tli id="T77" time="82.4" type="appl" />
         <tli id="T78" time="82.9" type="appl" />
         <tli id="T79" time="83.4" type="appl" />
         <tli id="T80" time="86.36537728203379" />
         <tli id="T81" time="87.795" type="appl" />
         <tli id="T82" time="89.12" type="appl" />
         <tli id="T83" time="90.445" type="appl" />
         <tli id="T84" time="93.318606807712" />
         <tli id="T85" time="94.403" type="appl" />
         <tli id="T86" time="95.117" type="appl" />
         <tli id="T87" time="95.83" type="appl" />
         <tli id="T88" time="96.543" type="appl" />
         <tli id="T89" time="97.257" type="appl" />
         <tli id="T90" time="97.97" type="appl" />
         <tli id="T91" time="98.368" type="appl" />
         <tli id="T92" time="98.75" type="appl" />
         <tli id="T93" time="99.132" type="appl" />
         <tli id="T94" time="99.99850708070295" />
         <tli id="T95" time="100.696" type="appl" />
         <tli id="T96" time="101.503" type="appl" />
         <tli id="T97" time="102.31" type="appl" />
         <tli id="T98" time="103.116" type="appl" />
         <tli id="T99" time="103.922" type="appl" />
         <tli id="T100" time="104.729" type="appl" />
         <tli id="T101" time="105.536" type="appl" />
         <tli id="T102" time="106.342" type="appl" />
         <tli id="T103" time="108.07171988568503" />
         <tli id="T104" time="109.281" type="appl" />
         <tli id="T105" time="110.482" type="appl" />
         <tli id="T106" time="111.684" type="appl" />
         <tli id="T107" time="112.885" type="appl" />
         <tli id="T108" time="114.086" type="appl" />
         <tli id="T109" time="115.287" type="appl" />
         <tli id="T110" time="116.489" type="appl" />
         <tli id="T111" time="117.69" type="appl" />
         <tli id="T151" time="119.19827605554316" type="intp" />
         <tli id="T112" time="121.61151774441223" />
         <tli id="T113" time="122.348" type="appl" />
         <tli id="T114" time="122.952" type="appl" />
         <tli id="T115" time="125.4514604163112" />
         <tli id="T116" time="126.136" type="appl" />
         <tli id="T117" time="126.622" type="appl" />
         <tli id="T118" time="127.109" type="appl" />
         <tli id="T119" time="127.595" type="appl" />
         <tli id="T120" time="128.081" type="appl" />
         <tli id="T121" time="128.568" type="appl" />
         <tli id="T122" time="129.054" type="appl" />
         <tli id="T123" time="131.54470278109537" />
         <tli id="T124" time="132.87" type="appl" />
         <tli id="T125" time="134.045" type="appl" />
         <tli id="T126" time="135.22" type="appl" />
         <tli id="T127" time="137.09128664050502" />
         <tli id="T128" time="137.91" type="appl" />
         <tli id="T129" time="138.68" type="appl" />
         <tli id="T130" time="139.45" type="appl" />
         <tli id="T131" time="140.22" type="appl" />
         <tli id="T132" time="140.99" type="appl" />
         <tli id="T133" time="143.48452452653132" />
         <tli id="T134" time="144.423" type="appl" />
         <tli id="T135" time="145.202" type="appl" />
         <tli id="T136" time="145.98" type="appl" />
         <tli id="T137" time="146.758" type="appl" />
         <tli id="T138" time="147.537" type="appl" />
         <tli id="T139" time="148.315" type="appl" />
         <tli id="T140" time="149.2" type="appl" />
         <tli id="T141" time="149.971" type="appl" />
         <tli id="T142" time="150.741" type="appl" />
         <tli id="T143" time="151.512" type="appl" />
         <tli id="T144" time="152.282" type="appl" />
         <tli id="T145" time="153.053" type="appl" />
         <tli id="T146" time="153.823" type="appl" />
         <tli id="T147" time="154.724" type="appl" />
         <tli id="T148" time="155.246" type="appl" />
         <tli id="T149" time="155.768" type="appl" />
         <tli id="T150" time="156.291" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T150" id="Seg_0" n="sc" s="T0">
               <ts e="T8" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Tebte</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">plʼonkaʔi</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_9" n="HIAT:ip">(</nts>
                  <ts e="T3" id="Seg_11" n="HIAT:w" s="T2">nubi-</ts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T3">nubiʔi</ts>
                  <nts id="Seg_15" n="HIAT:ip">)</nts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_18" n="HIAT:w" s="T4">nubilaʔ</ts>
                  <nts id="Seg_19" n="HIAT:ip">,</nts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">padʼi</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">bar</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">šamnaʔbəʔjə</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_32" n="HIAT:u" s="T8">
                  <nts id="Seg_33" n="HIAT:ip">(</nts>
                  <nts id="Seg_34" n="HIAT:ip">(</nts>
                  <ats e="T9" id="Seg_35" n="HIAT:non-pho" s="T8">BRK</ats>
                  <nts id="Seg_36" n="HIAT:ip">)</nts>
                  <nts id="Seg_37" n="HIAT:ip">)</nts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_41" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_43" n="HIAT:w" s="T9">Teʔtə</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_46" n="HIAT:w" s="T10">kobsaŋ</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_49" n="HIAT:w" s="T11">onʼiʔ</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_52" n="HIAT:w" s="T12">ulunkanə</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_54" n="HIAT:ip">(</nts>
                  <ts e="T14" id="Seg_56" n="HIAT:w" s="T13">kĭn-</ts>
                  <nts id="Seg_57" n="HIAT:ip">)</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_60" n="HIAT:w" s="T14">kĭnzleʔbəʔjə</ts>
                  <nts id="Seg_61" n="HIAT:ip">.</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_64" n="HIAT:u" s="T15">
                  <nts id="Seg_65" n="HIAT:ip">(</nts>
                  <ts e="T16" id="Seg_67" n="HIAT:w" s="T15">Dĭ=</ts>
                  <nts id="Seg_68" n="HIAT:ip">)</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_71" n="HIAT:w" s="T16">Dĭ=</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_74" n="HIAT:w" s="T17">ĭmbi</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_77" n="HIAT:w" s="T18">dĭrgit</ts>
                  <nts id="Seg_78" n="HIAT:ip">?</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_81" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_83" n="HIAT:w" s="T19">Dĭ</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_86" n="HIAT:w" s="T20">tüžöj</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_89" n="HIAT:w" s="T21">sürdlial</ts>
                  <nts id="Seg_90" n="HIAT:ip">.</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_93" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_95" n="HIAT:w" s="T22">Teʔtə</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_98" n="HIAT:w" s="T23">nüjüʔi</ts>
                  <nts id="Seg_99" n="HIAT:ip">.</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_102" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_104" n="HIAT:w" s="T24">Ot</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_107" n="HIAT:w" s="T25">girgit</ts>
                  <nts id="Seg_108" n="HIAT:ip">.</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_111" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_113" n="HIAT:w" s="T26">Teʔtə</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_116" n="HIAT:w" s="T27">kobsaŋ</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_119" n="HIAT:w" s="T28">onʼiʔ</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_122" n="HIAT:w" s="T29">lunken</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_125" n="HIAT:w" s="T30">kĭnzleʔbəʔjə</ts>
                  <nts id="Seg_126" n="HIAT:ip">.</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_129" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_131" n="HIAT:w" s="T31">A</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_134" n="HIAT:w" s="T32">ĭmbi</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_137" n="HIAT:w" s="T33">dĭ</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_140" n="HIAT:w" s="T34">dirgit</ts>
                  <nts id="Seg_141" n="HIAT:ip">?</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_144" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_146" n="HIAT:w" s="T35">Ĭmbi</ts>
                  <nts id="Seg_147" n="HIAT:ip">,</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_150" n="HIAT:w" s="T36">tüžöj</ts>
                  <nts id="Seg_151" n="HIAT:ip">!</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_154" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_156" n="HIAT:w" s="T37">Teʔtə</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_159" n="HIAT:w" s="T38">nüjö</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_162" n="HIAT:w" s="T39">onʼiʔ</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_165" n="HIAT:w" s="T40">lunkaʔinə</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_167" n="HIAT:ip">(</nts>
                  <ts e="T42" id="Seg_169" n="HIAT:w" s="T41">m-</ts>
                  <nts id="Seg_170" n="HIAT:ip">)</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_173" n="HIAT:w" s="T42">süt</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_176" n="HIAT:w" s="T43">mĭlleʔbə</ts>
                  <nts id="Seg_177" n="HIAT:ip">.</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_180" n="HIAT:u" s="T44">
                  <nts id="Seg_181" n="HIAT:ip">(</nts>
                  <nts id="Seg_182" n="HIAT:ip">(</nts>
                  <ats e="T45" id="Seg_183" n="HIAT:non-pho" s="T44">BRK</ats>
                  <nts id="Seg_184" n="HIAT:ip">)</nts>
                  <nts id="Seg_185" n="HIAT:ip">)</nts>
                  <nts id="Seg_186" n="HIAT:ip">.</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_189" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_191" n="HIAT:w" s="T45">A</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_194" n="HIAT:w" s="T46">dĭ</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_197" n="HIAT:w" s="T47">sürarla:</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_200" n="HIAT:w" s="T48">Ĭmbi</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_203" n="HIAT:w" s="T49">dĭ</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_206" n="HIAT:w" s="T50">dirgit</ts>
                  <nts id="Seg_207" n="HIAT:ip">,</nts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_210" n="HIAT:w" s="T51">tăn</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_213" n="HIAT:w" s="T52">zagatkal</ts>
                  <nts id="Seg_214" n="HIAT:ip">?</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_217" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_219" n="HIAT:w" s="T53">Dĭ</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_222" n="HIAT:w" s="T54">stol</ts>
                  <nts id="Seg_223" n="HIAT:ip">.</nts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_226" n="HIAT:u" s="T55">
                  <nts id="Seg_227" n="HIAT:ip">(</nts>
                  <ts e="T56" id="Seg_229" n="HIAT:w" s="T55">Nagur=</ts>
                  <nts id="Seg_230" n="HIAT:ip">)</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_233" n="HIAT:w" s="T56">Teʔtə</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_236" n="HIAT:w" s="T57">nugaʔi</ts>
                  <nts id="Seg_237" n="HIAT:ip">,</nts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_240" n="HIAT:w" s="T58">a</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_242" n="HIAT:ip">(</nts>
                  <ts e="T60" id="Seg_244" n="HIAT:w" s="T59">uzʼu-</ts>
                  <nts id="Seg_245" n="HIAT:ip">)</nts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_248" n="HIAT:w" s="T60">üžüt</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_251" n="HIAT:w" s="T61">stoldə</ts>
                  <nts id="Seg_252" n="HIAT:ip">.</nts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_255" n="HIAT:u" s="T62">
                  <nts id="Seg_256" n="HIAT:ip">(</nts>
                  <nts id="Seg_257" n="HIAT:ip">(</nts>
                  <ats e="T63" id="Seg_258" n="HIAT:non-pho" s="T62">BRK</ats>
                  <nts id="Seg_259" n="HIAT:ip">)</nts>
                  <nts id="Seg_260" n="HIAT:ip">)</nts>
                  <nts id="Seg_261" n="HIAT:ip">.</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_264" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_266" n="HIAT:w" s="T63">Teʔtə</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_269" n="HIAT:w" s="T64">kagaʔi</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_272" n="HIAT:w" s="T65">onʼiʔ</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_275" n="HIAT:w" s="T66">üžündə</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_277" n="HIAT:ip">(</nts>
                  <ts e="T68" id="Seg_279" n="HIAT:w" s="T67">am-</ts>
                  <nts id="Seg_280" n="HIAT:ip">)</nts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_283" n="HIAT:w" s="T68">nugaʔi</ts>
                  <nts id="Seg_284" n="HIAT:ip">.</nts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_287" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_289" n="HIAT:w" s="T69">Dĭgəttə</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_292" n="HIAT:w" s="T70">šide</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_295" n="HIAT:w" s="T71">kaga</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_298" n="HIAT:w" s="T72">nuʔməleʔbəʔjə</ts>
                  <nts id="Seg_299" n="HIAT:ip">,</nts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_302" n="HIAT:w" s="T73">šide</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_305" n="HIAT:w" s="T74">bĭdleʔbəʔjə</ts>
                  <nts id="Seg_306" n="HIAT:ip">.</nts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_309" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_311" n="HIAT:w" s="T75">A</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_314" n="HIAT:w" s="T76">nörbit</ts>
                  <nts id="Seg_315" n="HIAT:ip">,</nts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_318" n="HIAT:w" s="T77">dĭ</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_321" n="HIAT:w" s="T78">ĭmbi</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_324" n="HIAT:w" s="T79">dirgit</ts>
                  <nts id="Seg_325" n="HIAT:ip">?</nts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_328" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_330" n="HIAT:w" s="T80">Tʼelega</ts>
                  <nts id="Seg_331" n="HIAT:ip">,</nts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_334" n="HIAT:w" s="T81">a</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_337" n="HIAT:w" s="T82">kalʼosaʔi</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_340" n="HIAT:w" s="T83">nuʔməleʔbəʔjə</ts>
                  <nts id="Seg_341" n="HIAT:ip">.</nts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_344" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_346" n="HIAT:w" s="T84">Šide</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_348" n="HIAT:ip">(</nts>
                  <ts e="T86" id="Seg_350" n="HIAT:w" s="T85">kam-</ts>
                  <nts id="Seg_351" n="HIAT:ip">)</nts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_354" n="HIAT:w" s="T86">kaga</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_356" n="HIAT:ip">(</nts>
                  <ts e="T88" id="Seg_358" n="HIAT:w" s="T87">kamnu-</ts>
                  <nts id="Seg_359" n="HIAT:ip">)</nts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_362" n="HIAT:w" s="T88">kamrola</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_365" n="HIAT:w" s="T89">nugaʔi</ts>
                  <nts id="Seg_366" n="HIAT:ip">.</nts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_369" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_371" n="HIAT:w" s="T90">A</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_374" n="HIAT:w" s="T91">dĭ</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_377" n="HIAT:w" s="T92">ĭmbi</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_380" n="HIAT:w" s="T93">dirgit</ts>
                  <nts id="Seg_381" n="HIAT:ip">?</nts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_384" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_386" n="HIAT:w" s="T94">Da</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_389" n="HIAT:w" s="T95">šide</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_392" n="HIAT:w" s="T96">pa</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_395" n="HIAT:w" s="T97">nulaʔbəʔjə</ts>
                  <nts id="Seg_396" n="HIAT:ip">,</nts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_399" n="HIAT:w" s="T98">a</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_401" n="HIAT:ip">(</nts>
                  <ts e="T100" id="Seg_403" n="HIAT:w" s="T99">dĭzeŋ=</ts>
                  <nts id="Seg_404" n="HIAT:ip">)</nts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_407" n="HIAT:w" s="T100">dĭzem</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_410" n="HIAT:w" s="T101">sarluʔpiʔi</ts>
                  <nts id="Seg_411" n="HIAT:ip">.</nts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_414" n="HIAT:u" s="T102">
                  <nts id="Seg_415" n="HIAT:ip">(</nts>
                  <nts id="Seg_416" n="HIAT:ip">(</nts>
                  <ats e="T103" id="Seg_417" n="HIAT:non-pho" s="T102">BRK</ats>
                  <nts id="Seg_418" n="HIAT:ip">)</nts>
                  <nts id="Seg_419" n="HIAT:ip">)</nts>
                  <nts id="Seg_420" n="HIAT:ip">.</nts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_423" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_425" n="HIAT:w" s="T103">Iat</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_427" n="HIAT:ip">(</nts>
                  <ts e="T105" id="Seg_429" n="HIAT:w" s="T104">nʼišpek</ts>
                  <nts id="Seg_430" n="HIAT:ip">)</nts>
                  <nts id="Seg_431" n="HIAT:ip">,</nts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_434" n="HIAT:w" s="T105">koʔbtot</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_437" n="HIAT:w" s="T106">komu</ts>
                  <nts id="Seg_438" n="HIAT:ip">,</nts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_441" n="HIAT:w" s="T107">nʼit</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_444" n="HIAT:w" s="T108">ebte</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_447" n="HIAT:w" s="T109">kuvas</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_450" n="HIAT:w" s="T110">nʼuʔtə</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_453" n="HIAT:w" s="T111">kallaʔ</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_456" n="HIAT:w" s="T151">dʼürbi</ts>
                  <nts id="Seg_457" n="HIAT:ip">.</nts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_460" n="HIAT:u" s="T112">
                  <ts e="T113" id="Seg_462" n="HIAT:w" s="T112">Dĭ</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_465" n="HIAT:w" s="T113">urgo</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_468" n="HIAT:w" s="T114">pʼeš</ts>
                  <nts id="Seg_469" n="HIAT:ip">.</nts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_472" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_474" n="HIAT:w" s="T115">A</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_477" n="HIAT:w" s="T116">šü</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_479" n="HIAT:ip">—</nts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_482" n="HIAT:w" s="T118">koʔbtot</ts>
                  <nts id="Seg_483" n="HIAT:ip">,</nts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_486" n="HIAT:w" s="T119">a</ts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_489" n="HIAT:w" s="T120">ber</ts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_491" n="HIAT:ip">—</nts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_494" n="HIAT:w" s="T122">nʼit</ts>
                  <nts id="Seg_495" n="HIAT:ip">.</nts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_498" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_500" n="HIAT:w" s="T123">Bügən</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_503" n="HIAT:w" s="T124">kalam-kalam</ts>
                  <nts id="Seg_504" n="HIAT:ip">,</nts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_507" n="HIAT:w" s="T125">aʔtʼi</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_510" n="HIAT:w" s="T126">naga</ts>
                  <nts id="Seg_511" n="HIAT:ip">.</nts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_514" n="HIAT:u" s="T127">
                  <nts id="Seg_515" n="HIAT:ip">(</nts>
                  <ts e="T128" id="Seg_517" n="HIAT:w" s="T127">Dagajdʼi</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_520" n="HIAT:w" s="T128">bă-</ts>
                  <nts id="Seg_521" n="HIAT:ip">)</nts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_524" n="HIAT:w" s="T129">Dagajdʼi</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_527" n="HIAT:w" s="T130">bătlam-bătlam</ts>
                  <nts id="Seg_528" n="HIAT:ip">,</nts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_531" n="HIAT:w" s="T131">kem</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_534" n="HIAT:w" s="T132">naga</ts>
                  <nts id="Seg_535" n="HIAT:ip">.</nts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_538" n="HIAT:u" s="T133">
                  <nts id="Seg_539" n="HIAT:ip">(</nts>
                  <ts e="T134" id="Seg_541" n="HIAT:w" s="T133">Dĭ</ts>
                  <nts id="Seg_542" n="HIAT:ip">)</nts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_545" n="HIAT:w" s="T134">bügən</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_548" n="HIAT:w" s="T135">kandəgal</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_550" n="HIAT:ip">—</nts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_553" n="HIAT:w" s="T137">aktʼi</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_556" n="HIAT:w" s="T138">naga</ts>
                  <nts id="Seg_557" n="HIAT:ip">.</nts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T146" id="Seg_560" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_562" n="HIAT:w" s="T139">A</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_565" n="HIAT:w" s="T140">dagajzi</ts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_568" n="HIAT:w" s="T141">ipek</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_571" n="HIAT:w" s="T142">bătlal</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_573" n="HIAT:ip">—</nts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_576" n="HIAT:w" s="T144">kem</ts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_579" n="HIAT:w" s="T145">naga</ts>
                  <nts id="Seg_580" n="HIAT:ip">.</nts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T150" id="Seg_583" n="HIAT:u" s="T146">
                  <ts e="T147" id="Seg_585" n="HIAT:w" s="T146">Это</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_587" n="HIAT:ip">(</nts>
                  <ts e="T148" id="Seg_589" n="HIAT:w" s="T147">уж</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_592" n="HIAT:w" s="T148">эта</ts>
                  <nts id="Seg_593" n="HIAT:ip">)</nts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_596" n="HIAT:w" s="T149">загадка</ts>
                  <nts id="Seg_597" n="HIAT:ip">.</nts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T150" id="Seg_599" n="sc" s="T0">
               <ts e="T1" id="Seg_601" n="e" s="T0">Tebte </ts>
               <ts e="T2" id="Seg_603" n="e" s="T1">plʼonkaʔi </ts>
               <ts e="T3" id="Seg_605" n="e" s="T2">(nubi- </ts>
               <ts e="T4" id="Seg_607" n="e" s="T3">nubiʔi) </ts>
               <ts e="T5" id="Seg_609" n="e" s="T4">nubilaʔ, </ts>
               <ts e="T6" id="Seg_611" n="e" s="T5">padʼi </ts>
               <ts e="T7" id="Seg_613" n="e" s="T6">bar </ts>
               <ts e="T8" id="Seg_615" n="e" s="T7">šamnaʔbəʔjə. </ts>
               <ts e="T9" id="Seg_617" n="e" s="T8">((BRK)). </ts>
               <ts e="T10" id="Seg_619" n="e" s="T9">Teʔtə </ts>
               <ts e="T11" id="Seg_621" n="e" s="T10">kobsaŋ </ts>
               <ts e="T12" id="Seg_623" n="e" s="T11">onʼiʔ </ts>
               <ts e="T13" id="Seg_625" n="e" s="T12">ulunkanə </ts>
               <ts e="T14" id="Seg_627" n="e" s="T13">(kĭn-) </ts>
               <ts e="T15" id="Seg_629" n="e" s="T14">kĭnzleʔbəʔjə. </ts>
               <ts e="T16" id="Seg_631" n="e" s="T15">(Dĭ=) </ts>
               <ts e="T17" id="Seg_633" n="e" s="T16">Dĭ= </ts>
               <ts e="T18" id="Seg_635" n="e" s="T17">ĭmbi </ts>
               <ts e="T19" id="Seg_637" n="e" s="T18">dĭrgit? </ts>
               <ts e="T20" id="Seg_639" n="e" s="T19">Dĭ </ts>
               <ts e="T21" id="Seg_641" n="e" s="T20">tüžöj </ts>
               <ts e="T22" id="Seg_643" n="e" s="T21">sürdlial. </ts>
               <ts e="T23" id="Seg_645" n="e" s="T22">Teʔtə </ts>
               <ts e="T24" id="Seg_647" n="e" s="T23">nüjüʔi. </ts>
               <ts e="T25" id="Seg_649" n="e" s="T24">Ot </ts>
               <ts e="T26" id="Seg_651" n="e" s="T25">girgit. </ts>
               <ts e="T27" id="Seg_653" n="e" s="T26">Teʔtə </ts>
               <ts e="T28" id="Seg_655" n="e" s="T27">kobsaŋ </ts>
               <ts e="T29" id="Seg_657" n="e" s="T28">onʼiʔ </ts>
               <ts e="T30" id="Seg_659" n="e" s="T29">lunken </ts>
               <ts e="T31" id="Seg_661" n="e" s="T30">kĭnzleʔbəʔjə. </ts>
               <ts e="T32" id="Seg_663" n="e" s="T31">A </ts>
               <ts e="T33" id="Seg_665" n="e" s="T32">ĭmbi </ts>
               <ts e="T34" id="Seg_667" n="e" s="T33">dĭ </ts>
               <ts e="T35" id="Seg_669" n="e" s="T34">dirgit? </ts>
               <ts e="T36" id="Seg_671" n="e" s="T35">Ĭmbi, </ts>
               <ts e="T37" id="Seg_673" n="e" s="T36">tüžöj! </ts>
               <ts e="T38" id="Seg_675" n="e" s="T37">Teʔtə </ts>
               <ts e="T39" id="Seg_677" n="e" s="T38">nüjö </ts>
               <ts e="T40" id="Seg_679" n="e" s="T39">onʼiʔ </ts>
               <ts e="T41" id="Seg_681" n="e" s="T40">lunkaʔinə </ts>
               <ts e="T42" id="Seg_683" n="e" s="T41">(m-) </ts>
               <ts e="T43" id="Seg_685" n="e" s="T42">süt </ts>
               <ts e="T44" id="Seg_687" n="e" s="T43">mĭlleʔbə. </ts>
               <ts e="T45" id="Seg_689" n="e" s="T44">((BRK)). </ts>
               <ts e="T46" id="Seg_691" n="e" s="T45">A </ts>
               <ts e="T47" id="Seg_693" n="e" s="T46">dĭ </ts>
               <ts e="T48" id="Seg_695" n="e" s="T47">sürarla: </ts>
               <ts e="T49" id="Seg_697" n="e" s="T48">Ĭmbi </ts>
               <ts e="T50" id="Seg_699" n="e" s="T49">dĭ </ts>
               <ts e="T51" id="Seg_701" n="e" s="T50">dirgit, </ts>
               <ts e="T52" id="Seg_703" n="e" s="T51">tăn </ts>
               <ts e="T53" id="Seg_705" n="e" s="T52">zagatkal? </ts>
               <ts e="T54" id="Seg_707" n="e" s="T53">Dĭ </ts>
               <ts e="T55" id="Seg_709" n="e" s="T54">stol. </ts>
               <ts e="T56" id="Seg_711" n="e" s="T55">(Nagur=) </ts>
               <ts e="T57" id="Seg_713" n="e" s="T56">Teʔtə </ts>
               <ts e="T58" id="Seg_715" n="e" s="T57">nugaʔi, </ts>
               <ts e="T59" id="Seg_717" n="e" s="T58">a </ts>
               <ts e="T60" id="Seg_719" n="e" s="T59">(uzʼu-) </ts>
               <ts e="T61" id="Seg_721" n="e" s="T60">üžüt </ts>
               <ts e="T62" id="Seg_723" n="e" s="T61">stoldə. </ts>
               <ts e="T63" id="Seg_725" n="e" s="T62">((BRK)). </ts>
               <ts e="T64" id="Seg_727" n="e" s="T63">Teʔtə </ts>
               <ts e="T65" id="Seg_729" n="e" s="T64">kagaʔi </ts>
               <ts e="T66" id="Seg_731" n="e" s="T65">onʼiʔ </ts>
               <ts e="T67" id="Seg_733" n="e" s="T66">üžündə </ts>
               <ts e="T68" id="Seg_735" n="e" s="T67">(am-) </ts>
               <ts e="T69" id="Seg_737" n="e" s="T68">nugaʔi. </ts>
               <ts e="T70" id="Seg_739" n="e" s="T69">Dĭgəttə </ts>
               <ts e="T71" id="Seg_741" n="e" s="T70">šide </ts>
               <ts e="T72" id="Seg_743" n="e" s="T71">kaga </ts>
               <ts e="T73" id="Seg_745" n="e" s="T72">nuʔməleʔbəʔjə, </ts>
               <ts e="T74" id="Seg_747" n="e" s="T73">šide </ts>
               <ts e="T75" id="Seg_749" n="e" s="T74">bĭdleʔbəʔjə. </ts>
               <ts e="T76" id="Seg_751" n="e" s="T75">A </ts>
               <ts e="T77" id="Seg_753" n="e" s="T76">nörbit, </ts>
               <ts e="T78" id="Seg_755" n="e" s="T77">dĭ </ts>
               <ts e="T79" id="Seg_757" n="e" s="T78">ĭmbi </ts>
               <ts e="T80" id="Seg_759" n="e" s="T79">dirgit? </ts>
               <ts e="T81" id="Seg_761" n="e" s="T80">Tʼelega, </ts>
               <ts e="T82" id="Seg_763" n="e" s="T81">a </ts>
               <ts e="T83" id="Seg_765" n="e" s="T82">kalʼosaʔi </ts>
               <ts e="T84" id="Seg_767" n="e" s="T83">nuʔməleʔbəʔjə. </ts>
               <ts e="T85" id="Seg_769" n="e" s="T84">Šide </ts>
               <ts e="T86" id="Seg_771" n="e" s="T85">(kam-) </ts>
               <ts e="T87" id="Seg_773" n="e" s="T86">kaga </ts>
               <ts e="T88" id="Seg_775" n="e" s="T87">(kamnu-) </ts>
               <ts e="T89" id="Seg_777" n="e" s="T88">kamrola </ts>
               <ts e="T90" id="Seg_779" n="e" s="T89">nugaʔi. </ts>
               <ts e="T91" id="Seg_781" n="e" s="T90">A </ts>
               <ts e="T92" id="Seg_783" n="e" s="T91">dĭ </ts>
               <ts e="T93" id="Seg_785" n="e" s="T92">ĭmbi </ts>
               <ts e="T94" id="Seg_787" n="e" s="T93">dirgit? </ts>
               <ts e="T95" id="Seg_789" n="e" s="T94">Da </ts>
               <ts e="T96" id="Seg_791" n="e" s="T95">šide </ts>
               <ts e="T97" id="Seg_793" n="e" s="T96">pa </ts>
               <ts e="T98" id="Seg_795" n="e" s="T97">nulaʔbəʔjə, </ts>
               <ts e="T99" id="Seg_797" n="e" s="T98">a </ts>
               <ts e="T100" id="Seg_799" n="e" s="T99">(dĭzeŋ=) </ts>
               <ts e="T101" id="Seg_801" n="e" s="T100">dĭzem </ts>
               <ts e="T102" id="Seg_803" n="e" s="T101">sarluʔpiʔi. </ts>
               <ts e="T103" id="Seg_805" n="e" s="T102">((BRK)). </ts>
               <ts e="T104" id="Seg_807" n="e" s="T103">Iat </ts>
               <ts e="T105" id="Seg_809" n="e" s="T104">(nʼišpek), </ts>
               <ts e="T106" id="Seg_811" n="e" s="T105">koʔbtot </ts>
               <ts e="T107" id="Seg_813" n="e" s="T106">komu, </ts>
               <ts e="T108" id="Seg_815" n="e" s="T107">nʼit </ts>
               <ts e="T109" id="Seg_817" n="e" s="T108">ebte </ts>
               <ts e="T110" id="Seg_819" n="e" s="T109">kuvas </ts>
               <ts e="T111" id="Seg_821" n="e" s="T110">nʼuʔtə </ts>
               <ts e="T151" id="Seg_823" n="e" s="T111">kallaʔ </ts>
               <ts e="T112" id="Seg_825" n="e" s="T151">dʼürbi. </ts>
               <ts e="T113" id="Seg_827" n="e" s="T112">Dĭ </ts>
               <ts e="T114" id="Seg_829" n="e" s="T113">urgo </ts>
               <ts e="T115" id="Seg_831" n="e" s="T114">pʼeš. </ts>
               <ts e="T116" id="Seg_833" n="e" s="T115">A </ts>
               <ts e="T117" id="Seg_835" n="e" s="T116">šü </ts>
               <ts e="T118" id="Seg_837" n="e" s="T117">— </ts>
               <ts e="T119" id="Seg_839" n="e" s="T118">koʔbtot, </ts>
               <ts e="T120" id="Seg_841" n="e" s="T119">a </ts>
               <ts e="T121" id="Seg_843" n="e" s="T120">ber </ts>
               <ts e="T122" id="Seg_845" n="e" s="T121">— </ts>
               <ts e="T123" id="Seg_847" n="e" s="T122">nʼit. </ts>
               <ts e="T124" id="Seg_849" n="e" s="T123">Bügən </ts>
               <ts e="T125" id="Seg_851" n="e" s="T124">kalam-kalam, </ts>
               <ts e="T126" id="Seg_853" n="e" s="T125">aʔtʼi </ts>
               <ts e="T127" id="Seg_855" n="e" s="T126">naga. </ts>
               <ts e="T128" id="Seg_857" n="e" s="T127">(Dagajdʼi </ts>
               <ts e="T129" id="Seg_859" n="e" s="T128">bă-) </ts>
               <ts e="T130" id="Seg_861" n="e" s="T129">Dagajdʼi </ts>
               <ts e="T131" id="Seg_863" n="e" s="T130">bătlam-bătlam, </ts>
               <ts e="T132" id="Seg_865" n="e" s="T131">kem </ts>
               <ts e="T133" id="Seg_867" n="e" s="T132">naga. </ts>
               <ts e="T134" id="Seg_869" n="e" s="T133">(Dĭ) </ts>
               <ts e="T135" id="Seg_871" n="e" s="T134">bügən </ts>
               <ts e="T136" id="Seg_873" n="e" s="T135">kandəgal </ts>
               <ts e="T137" id="Seg_875" n="e" s="T136">— </ts>
               <ts e="T138" id="Seg_877" n="e" s="T137">aktʼi </ts>
               <ts e="T139" id="Seg_879" n="e" s="T138">naga. </ts>
               <ts e="T140" id="Seg_881" n="e" s="T139">A </ts>
               <ts e="T141" id="Seg_883" n="e" s="T140">dagajzi </ts>
               <ts e="T142" id="Seg_885" n="e" s="T141">ipek </ts>
               <ts e="T143" id="Seg_887" n="e" s="T142">bătlal </ts>
               <ts e="T144" id="Seg_889" n="e" s="T143">— </ts>
               <ts e="T145" id="Seg_891" n="e" s="T144">kem </ts>
               <ts e="T146" id="Seg_893" n="e" s="T145">naga. </ts>
               <ts e="T147" id="Seg_895" n="e" s="T146">Это </ts>
               <ts e="T148" id="Seg_897" n="e" s="T147">(уж </ts>
               <ts e="T149" id="Seg_899" n="e" s="T148">эта) </ts>
               <ts e="T150" id="Seg_901" n="e" s="T149">загадка. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T8" id="Seg_902" s="T0">PKZ_196X_Riddles_flk.001 (001)</ta>
            <ta e="T9" id="Seg_903" s="T8">PKZ_196X_Riddles_flk.002 (002)</ta>
            <ta e="T15" id="Seg_904" s="T9">PKZ_196X_Riddles_flk.003 (003)</ta>
            <ta e="T19" id="Seg_905" s="T15">PKZ_196X_Riddles_flk.004 (004)</ta>
            <ta e="T22" id="Seg_906" s="T19">PKZ_196X_Riddles_flk.005 (005)</ta>
            <ta e="T24" id="Seg_907" s="T22">PKZ_196X_Riddles_flk.006 (006)</ta>
            <ta e="T26" id="Seg_908" s="T24">PKZ_196X_Riddles_flk.007 (007)</ta>
            <ta e="T31" id="Seg_909" s="T26">PKZ_196X_Riddles_flk.008 (008)</ta>
            <ta e="T35" id="Seg_910" s="T31">PKZ_196X_Riddles_flk.009 (009)</ta>
            <ta e="T37" id="Seg_911" s="T35">PKZ_196X_Riddles_flk.010 (010)</ta>
            <ta e="T44" id="Seg_912" s="T37">PKZ_196X_Riddles_flk.011 (011)</ta>
            <ta e="T45" id="Seg_913" s="T44">PKZ_196X_Riddles_flk.012 (012)</ta>
            <ta e="T53" id="Seg_914" s="T45">PKZ_196X_Riddles_flk.013 (013)</ta>
            <ta e="T55" id="Seg_915" s="T53">PKZ_196X_Riddles_flk.014 (014)</ta>
            <ta e="T62" id="Seg_916" s="T55">PKZ_196X_Riddles_flk.015 (015)</ta>
            <ta e="T63" id="Seg_917" s="T62">PKZ_196X_Riddles_flk.016 (016)</ta>
            <ta e="T69" id="Seg_918" s="T63">PKZ_196X_Riddles_flk.017 (017)</ta>
            <ta e="T75" id="Seg_919" s="T69">PKZ_196X_Riddles_flk.018 (018)</ta>
            <ta e="T80" id="Seg_920" s="T75">PKZ_196X_Riddles_flk.019 (019)</ta>
            <ta e="T84" id="Seg_921" s="T80">PKZ_196X_Riddles_flk.020 (020)</ta>
            <ta e="T90" id="Seg_922" s="T84">PKZ_196X_Riddles_flk.021 (021)</ta>
            <ta e="T94" id="Seg_923" s="T90">PKZ_196X_Riddles_flk.022 (022)</ta>
            <ta e="T102" id="Seg_924" s="T94">PKZ_196X_Riddles_flk.023 (023)</ta>
            <ta e="T103" id="Seg_925" s="T102">PKZ_196X_Riddles_flk.024 (024)</ta>
            <ta e="T112" id="Seg_926" s="T103">PKZ_196X_Riddles_flk.025 (025)</ta>
            <ta e="T115" id="Seg_927" s="T112">PKZ_196X_Riddles_flk.026 (026)</ta>
            <ta e="T123" id="Seg_928" s="T115">PKZ_196X_Riddles_flk.027 (027)</ta>
            <ta e="T127" id="Seg_929" s="T123">PKZ_196X_Riddles_flk.028 (028)</ta>
            <ta e="T133" id="Seg_930" s="T127">PKZ_196X_Riddles_flk.029 (029)</ta>
            <ta e="T139" id="Seg_931" s="T133">PKZ_196X_Riddles_flk.030 (030)</ta>
            <ta e="T146" id="Seg_932" s="T139">PKZ_196X_Riddles_flk.031 (031)</ta>
            <ta e="T150" id="Seg_933" s="T146">PKZ_196X_Riddles_flk.032 (032)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T8" id="Seg_934" s="T0">Tebte plʼonkaʔi (nubi- nubiʔi) nubilaʔ, padʼi bar šamnaʔbəʔjə. </ta>
            <ta e="T9" id="Seg_935" s="T8">((BRK)). </ta>
            <ta e="T15" id="Seg_936" s="T9">Teʔtə kobsaŋ onʼiʔ ulunkanə (kĭn-) kĭnzleʔbəʔjə. </ta>
            <ta e="T19" id="Seg_937" s="T15">(Dĭ=) Dĭ= ĭmbi dĭrgit? </ta>
            <ta e="T22" id="Seg_938" s="T19">Dĭ tüžöj sürdlial. </ta>
            <ta e="T24" id="Seg_939" s="T22">Teʔtə nüjüʔi. </ta>
            <ta e="T26" id="Seg_940" s="T24">Ot girgit. </ta>
            <ta e="T31" id="Seg_941" s="T26">Teʔtə kobsaŋ onʼiʔ lunken kĭnzleʔbəʔjə. </ta>
            <ta e="T35" id="Seg_942" s="T31">A ĭmbi dĭ dirgit? </ta>
            <ta e="T37" id="Seg_943" s="T35">Ĭmbi, tüžöj! </ta>
            <ta e="T44" id="Seg_944" s="T37">Teʔtə nüjö onʼiʔ lunkaʔinə (m-) süt mĭlleʔbə. </ta>
            <ta e="T45" id="Seg_945" s="T44">((BRK)). </ta>
            <ta e="T53" id="Seg_946" s="T45">A dĭ sürarla: "Ĭmbi dĭ dirgit, tăn zagatkal? </ta>
            <ta e="T55" id="Seg_947" s="T53">Dĭ stol. </ta>
            <ta e="T62" id="Seg_948" s="T55">(Nagur=) Teʔtə nugaʔi, a (uzʼu-) üžüt stoldə. </ta>
            <ta e="T63" id="Seg_949" s="T62">((BRK)). </ta>
            <ta e="T69" id="Seg_950" s="T63">Teʔtə kagaʔi onʼiʔ üžündə (am-) nugaʔi. </ta>
            <ta e="T75" id="Seg_951" s="T69">Dĭgəttə šide kaga nuʔməleʔbəʔjə, šide bĭdleʔbəʔjə. </ta>
            <ta e="T80" id="Seg_952" s="T75">A nörbit, dĭ ĭmbi dirgit? </ta>
            <ta e="T84" id="Seg_953" s="T80">Tʼelega, a kalʼosaʔi nuʔməleʔbəʔjə. </ta>
            <ta e="T90" id="Seg_954" s="T84">Šide (kam-) kaga (kamnu-) kamrola nugaʔi. </ta>
            <ta e="T94" id="Seg_955" s="T90">A dĭ ĭmbi dirgit? </ta>
            <ta e="T102" id="Seg_956" s="T94">Da šide pa nulaʔbəʔjə, a (dĭzeŋ=) dĭzem sarluʔpiʔi. </ta>
            <ta e="T103" id="Seg_957" s="T102">((BRK)). </ta>
            <ta e="T112" id="Seg_958" s="T103">Iat (nʼišpek), koʔbtot komu, nʼit ebte kuvas nʼuʔtə kallaʔ dʼürbi. </ta>
            <ta e="T115" id="Seg_959" s="T112">Dĭ urgo pʼeš. </ta>
            <ta e="T123" id="Seg_960" s="T115">A šü — koʔbtot, a ber — nʼit. </ta>
            <ta e="T127" id="Seg_961" s="T123">Bügən kalam-kalam, aʔtʼi naga. </ta>
            <ta e="T133" id="Seg_962" s="T127">(Dagajdʼi bă-) Dagajdʼi bătlam-bătlam, kem naga. </ta>
            <ta e="T139" id="Seg_963" s="T133">(Dĭ) bügən kandəgal — aktʼi naga. </ta>
            <ta e="T146" id="Seg_964" s="T139">A dagajzi ipek bătlal — kem naga. </ta>
            <ta e="T150" id="Seg_965" s="T146">Это (уж эта) загадка. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_966" s="T0">tebte</ta>
            <ta e="T2" id="Seg_967" s="T1">plʼonka-ʔi</ta>
            <ta e="T4" id="Seg_968" s="T3">nu-bi-ʔi</ta>
            <ta e="T5" id="Seg_969" s="T4">nu-bilaʔ</ta>
            <ta e="T6" id="Seg_970" s="T5">padʼi</ta>
            <ta e="T7" id="Seg_971" s="T6">bar</ta>
            <ta e="T8" id="Seg_972" s="T7">šam-naʔbə-ʔjə</ta>
            <ta e="T10" id="Seg_973" s="T9">teʔtə</ta>
            <ta e="T11" id="Seg_974" s="T10">kob-saŋ</ta>
            <ta e="T12" id="Seg_975" s="T11">onʼiʔ</ta>
            <ta e="T13" id="Seg_976" s="T12">ulunka-nə</ta>
            <ta e="T15" id="Seg_977" s="T14">kĭnz-leʔbə-ʔjə</ta>
            <ta e="T16" id="Seg_978" s="T15">dĭ</ta>
            <ta e="T17" id="Seg_979" s="T16">dĭ</ta>
            <ta e="T18" id="Seg_980" s="T17">ĭmbi</ta>
            <ta e="T19" id="Seg_981" s="T18">dĭrgit</ta>
            <ta e="T20" id="Seg_982" s="T19">dĭ</ta>
            <ta e="T21" id="Seg_983" s="T20">tüžöj</ta>
            <ta e="T22" id="Seg_984" s="T21">sürd-lia-l</ta>
            <ta e="T23" id="Seg_985" s="T22">teʔtə</ta>
            <ta e="T24" id="Seg_986" s="T23">nüjü-ʔi</ta>
            <ta e="T25" id="Seg_987" s="T24">ot</ta>
            <ta e="T26" id="Seg_988" s="T25">girgit</ta>
            <ta e="T27" id="Seg_989" s="T26">teʔtə</ta>
            <ta e="T28" id="Seg_990" s="T27">kob-saŋ</ta>
            <ta e="T29" id="Seg_991" s="T28">onʼiʔ</ta>
            <ta e="T30" id="Seg_992" s="T29">lunke-n</ta>
            <ta e="T31" id="Seg_993" s="T30">kĭnz-leʔbə-ʔjə</ta>
            <ta e="T32" id="Seg_994" s="T31">a</ta>
            <ta e="T33" id="Seg_995" s="T32">ĭmbi</ta>
            <ta e="T34" id="Seg_996" s="T33">dĭ</ta>
            <ta e="T35" id="Seg_997" s="T34">dirgit</ta>
            <ta e="T36" id="Seg_998" s="T35">ĭmbi</ta>
            <ta e="T37" id="Seg_999" s="T36">tüžöj</ta>
            <ta e="T38" id="Seg_1000" s="T37">teʔtə</ta>
            <ta e="T39" id="Seg_1001" s="T38">nüjö</ta>
            <ta e="T40" id="Seg_1002" s="T39">onʼiʔ</ta>
            <ta e="T41" id="Seg_1003" s="T40">lunkaʔi-nə</ta>
            <ta e="T43" id="Seg_1004" s="T42">süt</ta>
            <ta e="T44" id="Seg_1005" s="T43">mĭl-leʔbə</ta>
            <ta e="T46" id="Seg_1006" s="T45">a</ta>
            <ta e="T47" id="Seg_1007" s="T46">dĭ</ta>
            <ta e="T48" id="Seg_1008" s="T47">sürar-la</ta>
            <ta e="T49" id="Seg_1009" s="T48">ĭmbi</ta>
            <ta e="T50" id="Seg_1010" s="T49">dĭ</ta>
            <ta e="T51" id="Seg_1011" s="T50">dirgit</ta>
            <ta e="T52" id="Seg_1012" s="T51">tăn</ta>
            <ta e="T53" id="Seg_1013" s="T52">zagatka-l</ta>
            <ta e="T54" id="Seg_1014" s="T53">dĭ</ta>
            <ta e="T55" id="Seg_1015" s="T54">stol</ta>
            <ta e="T56" id="Seg_1016" s="T55">nagur</ta>
            <ta e="T57" id="Seg_1017" s="T56">teʔtə</ta>
            <ta e="T58" id="Seg_1018" s="T57">nu-ga-ʔi</ta>
            <ta e="T59" id="Seg_1019" s="T58">a</ta>
            <ta e="T61" id="Seg_1020" s="T60">üžü-t</ta>
            <ta e="T62" id="Seg_1021" s="T61">stol-də</ta>
            <ta e="T64" id="Seg_1022" s="T63">teʔtə</ta>
            <ta e="T65" id="Seg_1023" s="T64">kaga-ʔi</ta>
            <ta e="T66" id="Seg_1024" s="T65">onʼiʔ</ta>
            <ta e="T67" id="Seg_1025" s="T66">üžü-ndə</ta>
            <ta e="T68" id="Seg_1026" s="T67">am-</ta>
            <ta e="T69" id="Seg_1027" s="T68">nu-ga-ʔi</ta>
            <ta e="T70" id="Seg_1028" s="T69">dĭgəttə</ta>
            <ta e="T71" id="Seg_1029" s="T70">šide</ta>
            <ta e="T72" id="Seg_1030" s="T71">kaga</ta>
            <ta e="T73" id="Seg_1031" s="T72">nuʔmə-leʔbə-ʔjə</ta>
            <ta e="T74" id="Seg_1032" s="T73">šide</ta>
            <ta e="T75" id="Seg_1033" s="T74">bĭd-leʔbə-ʔjə</ta>
            <ta e="T76" id="Seg_1034" s="T75">a</ta>
            <ta e="T77" id="Seg_1035" s="T76">nörbi-t</ta>
            <ta e="T78" id="Seg_1036" s="T77">dĭ</ta>
            <ta e="T79" id="Seg_1037" s="T78">ĭmbi</ta>
            <ta e="T80" id="Seg_1038" s="T79">dirgit</ta>
            <ta e="T81" id="Seg_1039" s="T80">tʼelega</ta>
            <ta e="T82" id="Seg_1040" s="T81">a</ta>
            <ta e="T83" id="Seg_1041" s="T82">kalʼosa-ʔi</ta>
            <ta e="T84" id="Seg_1042" s="T83">nuʔmə-leʔbə-ʔjə</ta>
            <ta e="T85" id="Seg_1043" s="T84">šide</ta>
            <ta e="T87" id="Seg_1044" s="T86">kaga</ta>
            <ta e="T90" id="Seg_1045" s="T89">nu-ga-ʔi</ta>
            <ta e="T91" id="Seg_1046" s="T90">a</ta>
            <ta e="T92" id="Seg_1047" s="T91">dĭ</ta>
            <ta e="T93" id="Seg_1048" s="T92">ĭmbi</ta>
            <ta e="T94" id="Seg_1049" s="T93">dirgit</ta>
            <ta e="T95" id="Seg_1050" s="T94">da</ta>
            <ta e="T96" id="Seg_1051" s="T95">šide</ta>
            <ta e="T97" id="Seg_1052" s="T96">pa</ta>
            <ta e="T98" id="Seg_1053" s="T97">nu-laʔbə-ʔjə</ta>
            <ta e="T99" id="Seg_1054" s="T98">a</ta>
            <ta e="T100" id="Seg_1055" s="T99">dĭ-zeŋ</ta>
            <ta e="T101" id="Seg_1056" s="T100">dĭ-zem</ta>
            <ta e="T102" id="Seg_1057" s="T101">sar-luʔpi-ʔi</ta>
            <ta e="T104" id="Seg_1058" s="T103">ia-t</ta>
            <ta e="T106" id="Seg_1059" s="T105">koʔbto-t</ta>
            <ta e="T107" id="Seg_1060" s="T106">komu</ta>
            <ta e="T108" id="Seg_1061" s="T107">nʼi-t</ta>
            <ta e="T110" id="Seg_1062" s="T109">kuvas</ta>
            <ta e="T111" id="Seg_1063" s="T110">nʼuʔtə</ta>
            <ta e="T151" id="Seg_1064" s="T111">kal-laʔ</ta>
            <ta e="T112" id="Seg_1065" s="T151">dʼür-bi</ta>
            <ta e="T113" id="Seg_1066" s="T112">dĭ</ta>
            <ta e="T114" id="Seg_1067" s="T113">urgo</ta>
            <ta e="T115" id="Seg_1068" s="T114">pʼeš</ta>
            <ta e="T116" id="Seg_1069" s="T115">a</ta>
            <ta e="T117" id="Seg_1070" s="T116">šü</ta>
            <ta e="T119" id="Seg_1071" s="T118">koʔbto-t</ta>
            <ta e="T120" id="Seg_1072" s="T119">a</ta>
            <ta e="T121" id="Seg_1073" s="T120">ber</ta>
            <ta e="T123" id="Seg_1074" s="T122">nʼi-t</ta>
            <ta e="T124" id="Seg_1075" s="T123">bü-gən</ta>
            <ta e="T125" id="Seg_1076" s="T124">ka-la-m-ka-la-m</ta>
            <ta e="T126" id="Seg_1077" s="T125">aʔtʼi</ta>
            <ta e="T127" id="Seg_1078" s="T126">naga</ta>
            <ta e="T128" id="Seg_1079" s="T127">dagaj-dʼi</ta>
            <ta e="T130" id="Seg_1080" s="T129">dagaj-dʼi</ta>
            <ta e="T131" id="Seg_1081" s="T130">băt-la-m-băt-la-m</ta>
            <ta e="T132" id="Seg_1082" s="T131">kem</ta>
            <ta e="T133" id="Seg_1083" s="T132">naga</ta>
            <ta e="T134" id="Seg_1084" s="T133">dĭ</ta>
            <ta e="T135" id="Seg_1085" s="T134">bü-gən</ta>
            <ta e="T136" id="Seg_1086" s="T135">kandə-ga-l</ta>
            <ta e="T138" id="Seg_1087" s="T137">aktʼi</ta>
            <ta e="T139" id="Seg_1088" s="T138">naga</ta>
            <ta e="T140" id="Seg_1089" s="T139">a</ta>
            <ta e="T141" id="Seg_1090" s="T140">dagaj-zi</ta>
            <ta e="T142" id="Seg_1091" s="T141">ipek</ta>
            <ta e="T143" id="Seg_1092" s="T142">băt-la-l</ta>
            <ta e="T145" id="Seg_1093" s="T144">kem</ta>
            <ta e="T146" id="Seg_1094" s="T145">naga</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1095" s="T0">teʔdə</ta>
            <ta e="T2" id="Seg_1096" s="T1">plʼonka-jəʔ</ta>
            <ta e="T4" id="Seg_1097" s="T3">nu-bi-jəʔ</ta>
            <ta e="T5" id="Seg_1098" s="T4">nu-%%</ta>
            <ta e="T6" id="Seg_1099" s="T5">padʼi</ta>
            <ta e="T7" id="Seg_1100" s="T6">bar</ta>
            <ta e="T8" id="Seg_1101" s="T7">šʼaːm-laʔbə-jəʔ</ta>
            <ta e="T10" id="Seg_1102" s="T9">teʔdə</ta>
            <ta e="T11" id="Seg_1103" s="T10">koʔbdo-zAŋ</ta>
            <ta e="T12" id="Seg_1104" s="T11">onʼiʔ</ta>
            <ta e="T13" id="Seg_1105" s="T12">%%-Tə</ta>
            <ta e="T15" id="Seg_1106" s="T14">kĭnzə-laʔbə-jəʔ</ta>
            <ta e="T16" id="Seg_1107" s="T15">dĭ</ta>
            <ta e="T17" id="Seg_1108" s="T16">dĭ</ta>
            <ta e="T18" id="Seg_1109" s="T17">ĭmbi</ta>
            <ta e="T19" id="Seg_1110" s="T18">dĭrgit</ta>
            <ta e="T20" id="Seg_1111" s="T19">dĭ</ta>
            <ta e="T21" id="Seg_1112" s="T20">tüžöj</ta>
            <ta e="T22" id="Seg_1113" s="T21">surdo-liA-l</ta>
            <ta e="T23" id="Seg_1114" s="T22">teʔdə</ta>
            <ta e="T24" id="Seg_1115" s="T23">nüjü-jəʔ</ta>
            <ta e="T25" id="Seg_1116" s="T24">vot</ta>
            <ta e="T26" id="Seg_1117" s="T25">girgit</ta>
            <ta e="T27" id="Seg_1118" s="T26">teʔdə</ta>
            <ta e="T28" id="Seg_1119" s="T27">koʔbdo-zAŋ</ta>
            <ta e="T29" id="Seg_1120" s="T28">onʼiʔ</ta>
            <ta e="T30" id="Seg_1121" s="T29">%%-Tə</ta>
            <ta e="T31" id="Seg_1122" s="T30">kĭnzə-laʔbə-jəʔ</ta>
            <ta e="T32" id="Seg_1123" s="T31">a</ta>
            <ta e="T33" id="Seg_1124" s="T32">ĭmbi</ta>
            <ta e="T34" id="Seg_1125" s="T33">dĭ</ta>
            <ta e="T35" id="Seg_1126" s="T34">dĭrgit</ta>
            <ta e="T36" id="Seg_1127" s="T35">ĭmbi</ta>
            <ta e="T37" id="Seg_1128" s="T36">tüžöj</ta>
            <ta e="T38" id="Seg_1129" s="T37">teʔdə</ta>
            <ta e="T39" id="Seg_1130" s="T38">nüjü</ta>
            <ta e="T40" id="Seg_1131" s="T39">onʼiʔ</ta>
            <ta e="T41" id="Seg_1132" s="T40">%%-Tə</ta>
            <ta e="T43" id="Seg_1133" s="T42">süt</ta>
            <ta e="T44" id="Seg_1134" s="T43">mĭn-laʔbə</ta>
            <ta e="T46" id="Seg_1135" s="T45">a</ta>
            <ta e="T47" id="Seg_1136" s="T46">dĭ</ta>
            <ta e="T48" id="Seg_1137" s="T47">surar-lAʔ</ta>
            <ta e="T49" id="Seg_1138" s="T48">ĭmbi</ta>
            <ta e="T50" id="Seg_1139" s="T49">dĭ</ta>
            <ta e="T51" id="Seg_1140" s="T50">dĭrgit</ta>
            <ta e="T52" id="Seg_1141" s="T51">tăn</ta>
            <ta e="T53" id="Seg_1142" s="T52">zagadka-l</ta>
            <ta e="T54" id="Seg_1143" s="T53">dĭ</ta>
            <ta e="T55" id="Seg_1144" s="T54">stol</ta>
            <ta e="T56" id="Seg_1145" s="T55">nagur</ta>
            <ta e="T57" id="Seg_1146" s="T56">teʔdə</ta>
            <ta e="T58" id="Seg_1147" s="T57">nu-gA-jəʔ</ta>
            <ta e="T59" id="Seg_1148" s="T58">a</ta>
            <ta e="T61" id="Seg_1149" s="T60">üžü-t</ta>
            <ta e="T62" id="Seg_1150" s="T61">stol-də</ta>
            <ta e="T64" id="Seg_1151" s="T63">teʔdə</ta>
            <ta e="T65" id="Seg_1152" s="T64">kaga-jəʔ</ta>
            <ta e="T66" id="Seg_1153" s="T65">onʼiʔ</ta>
            <ta e="T67" id="Seg_1154" s="T66">üžü-gəndə</ta>
            <ta e="T68" id="Seg_1155" s="T67">am-</ta>
            <ta e="T69" id="Seg_1156" s="T68">nu-gA-jəʔ</ta>
            <ta e="T70" id="Seg_1157" s="T69">dĭgəttə</ta>
            <ta e="T71" id="Seg_1158" s="T70">šide</ta>
            <ta e="T72" id="Seg_1159" s="T71">kaga</ta>
            <ta e="T73" id="Seg_1160" s="T72">nuʔmə-laʔbə-jəʔ</ta>
            <ta e="T74" id="Seg_1161" s="T73">šide</ta>
            <ta e="T75" id="Seg_1162" s="T74">bĭdə-laʔbə-jəʔ</ta>
            <ta e="T76" id="Seg_1163" s="T75">a</ta>
            <ta e="T77" id="Seg_1164" s="T76">nörbə-t</ta>
            <ta e="T78" id="Seg_1165" s="T77">dĭ</ta>
            <ta e="T79" id="Seg_1166" s="T78">ĭmbi</ta>
            <ta e="T80" id="Seg_1167" s="T79">dĭrgit</ta>
            <ta e="T81" id="Seg_1168" s="T80">tʼelega</ta>
            <ta e="T82" id="Seg_1169" s="T81">a</ta>
            <ta e="T83" id="Seg_1170" s="T82">kalʼosa-jəʔ</ta>
            <ta e="T84" id="Seg_1171" s="T83">nuʔmə-laʔbə-jəʔ</ta>
            <ta e="T85" id="Seg_1172" s="T84">šide</ta>
            <ta e="T87" id="Seg_1173" s="T86">kaga</ta>
            <ta e="T90" id="Seg_1174" s="T89">nu-gA-jəʔ</ta>
            <ta e="T91" id="Seg_1175" s="T90">a</ta>
            <ta e="T92" id="Seg_1176" s="T91">dĭ</ta>
            <ta e="T93" id="Seg_1177" s="T92">ĭmbi</ta>
            <ta e="T94" id="Seg_1178" s="T93">dĭrgit</ta>
            <ta e="T95" id="Seg_1179" s="T94">da</ta>
            <ta e="T96" id="Seg_1180" s="T95">šide</ta>
            <ta e="T97" id="Seg_1181" s="T96">pa</ta>
            <ta e="T98" id="Seg_1182" s="T97">nu-laʔbə-jəʔ</ta>
            <ta e="T99" id="Seg_1183" s="T98">a</ta>
            <ta e="T100" id="Seg_1184" s="T99">dĭ-zAŋ</ta>
            <ta e="T101" id="Seg_1185" s="T100">dĭ-zem</ta>
            <ta e="T102" id="Seg_1186" s="T101">sar-luʔbdə-jəʔ</ta>
            <ta e="T104" id="Seg_1187" s="T103">ija-t</ta>
            <ta e="T106" id="Seg_1188" s="T105">koʔbdo-t</ta>
            <ta e="T107" id="Seg_1189" s="T106">kömə</ta>
            <ta e="T108" id="Seg_1190" s="T107">nʼi-t</ta>
            <ta e="T110" id="Seg_1191" s="T109">kuvas</ta>
            <ta e="T111" id="Seg_1192" s="T110">nʼuʔdə</ta>
            <ta e="T151" id="Seg_1193" s="T111">kal-lAʔ</ta>
            <ta e="T112" id="Seg_1194" s="T151">dʼür-bi</ta>
            <ta e="T113" id="Seg_1195" s="T112">dĭ</ta>
            <ta e="T114" id="Seg_1196" s="T113">urgo</ta>
            <ta e="T115" id="Seg_1197" s="T114">pʼeːš</ta>
            <ta e="T116" id="Seg_1198" s="T115">a</ta>
            <ta e="T117" id="Seg_1199" s="T116">šü</ta>
            <ta e="T119" id="Seg_1200" s="T118">koʔbdo-t</ta>
            <ta e="T120" id="Seg_1201" s="T119">a</ta>
            <ta e="T121" id="Seg_1202" s="T120">bor</ta>
            <ta e="T123" id="Seg_1203" s="T122">nʼi-t</ta>
            <ta e="T124" id="Seg_1204" s="T123">bü-Kən</ta>
            <ta e="T125" id="Seg_1205" s="T124">kan-lV-m-kan-lV-m</ta>
            <ta e="T126" id="Seg_1206" s="T125">aʔtʼi</ta>
            <ta e="T127" id="Seg_1207" s="T126">naga</ta>
            <ta e="T128" id="Seg_1208" s="T127">tagaj-ziʔ</ta>
            <ta e="T130" id="Seg_1209" s="T129">tagaj-ziʔ</ta>
            <ta e="T131" id="Seg_1210" s="T130">băt-lV-m-băt-lV-m</ta>
            <ta e="T132" id="Seg_1211" s="T131">kem</ta>
            <ta e="T133" id="Seg_1212" s="T132">naga</ta>
            <ta e="T134" id="Seg_1213" s="T133">dĭ</ta>
            <ta e="T135" id="Seg_1214" s="T134">bü-Kən</ta>
            <ta e="T136" id="Seg_1215" s="T135">kandə-gA-l</ta>
            <ta e="T138" id="Seg_1216" s="T137">aʔtʼi</ta>
            <ta e="T139" id="Seg_1217" s="T138">naga</ta>
            <ta e="T140" id="Seg_1218" s="T139">a</ta>
            <ta e="T141" id="Seg_1219" s="T140">tagaj-ziʔ</ta>
            <ta e="T142" id="Seg_1220" s="T141">ipek</ta>
            <ta e="T143" id="Seg_1221" s="T142">băt-lV-l</ta>
            <ta e="T145" id="Seg_1222" s="T144">kem</ta>
            <ta e="T146" id="Seg_1223" s="T145">naga</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1224" s="T0">four.[NOM.SG]</ta>
            <ta e="T2" id="Seg_1225" s="T1">tape-PL</ta>
            <ta e="T4" id="Seg_1226" s="T3">stand-PST-3PL</ta>
            <ta e="T5" id="Seg_1227" s="T4">stand-%%</ta>
            <ta e="T6" id="Seg_1228" s="T5">probably</ta>
            <ta e="T7" id="Seg_1229" s="T6">PTCL</ta>
            <ta e="T8" id="Seg_1230" s="T7">lie-DUR-3PL</ta>
            <ta e="T10" id="Seg_1231" s="T9">four.[NOM.SG]</ta>
            <ta e="T11" id="Seg_1232" s="T10">daughter-PL</ta>
            <ta e="T12" id="Seg_1233" s="T11">one.[NOM.SG]</ta>
            <ta e="T13" id="Seg_1234" s="T12">%%-LAT</ta>
            <ta e="T15" id="Seg_1235" s="T14">piss-DUR-3PL</ta>
            <ta e="T16" id="Seg_1236" s="T15">this.[NOM.SG]</ta>
            <ta e="T17" id="Seg_1237" s="T16">this</ta>
            <ta e="T18" id="Seg_1238" s="T17">what.[NOM.SG]</ta>
            <ta e="T19" id="Seg_1239" s="T18">such.[NOM.SG]</ta>
            <ta e="T20" id="Seg_1240" s="T19">this.[NOM.SG]</ta>
            <ta e="T21" id="Seg_1241" s="T20">cow.[NOM.SG]</ta>
            <ta e="T22" id="Seg_1242" s="T21">milk-PRS-2SG</ta>
            <ta e="T23" id="Seg_1243" s="T22">four.[NOM.SG]</ta>
            <ta e="T24" id="Seg_1244" s="T23">breast-PL</ta>
            <ta e="T25" id="Seg_1245" s="T24">look</ta>
            <ta e="T26" id="Seg_1246" s="T25">what.kind</ta>
            <ta e="T27" id="Seg_1247" s="T26">four.[NOM.SG]</ta>
            <ta e="T28" id="Seg_1248" s="T27">daughter-PL</ta>
            <ta e="T29" id="Seg_1249" s="T28">one.[NOM.SG]</ta>
            <ta e="T30" id="Seg_1250" s="T29">%%-LAT</ta>
            <ta e="T31" id="Seg_1251" s="T30">piss-DUR-3PL</ta>
            <ta e="T32" id="Seg_1252" s="T31">and</ta>
            <ta e="T33" id="Seg_1253" s="T32">what.[NOM.SG]</ta>
            <ta e="T34" id="Seg_1254" s="T33">this.[NOM.SG]</ta>
            <ta e="T35" id="Seg_1255" s="T34">such</ta>
            <ta e="T36" id="Seg_1256" s="T35">what.[NOM.SG]</ta>
            <ta e="T37" id="Seg_1257" s="T36">cow.[NOM.SG]</ta>
            <ta e="T38" id="Seg_1258" s="T37">four.[NOM.SG]</ta>
            <ta e="T39" id="Seg_1259" s="T38">breast.[NOM.SG]</ta>
            <ta e="T40" id="Seg_1260" s="T39">one.[NOM.SG]</ta>
            <ta e="T41" id="Seg_1261" s="T40">%%-LAT</ta>
            <ta e="T43" id="Seg_1262" s="T42">milk.[NOM.SG]</ta>
            <ta e="T44" id="Seg_1263" s="T43">go-DUR.[3SG]</ta>
            <ta e="T46" id="Seg_1264" s="T45">and</ta>
            <ta e="T47" id="Seg_1265" s="T46">this.[NOM.SG]</ta>
            <ta e="T48" id="Seg_1266" s="T47">ask-CVB</ta>
            <ta e="T49" id="Seg_1267" s="T48">what.[NOM.SG]</ta>
            <ta e="T50" id="Seg_1268" s="T49">this.[NOM.SG]</ta>
            <ta e="T51" id="Seg_1269" s="T50">such</ta>
            <ta e="T52" id="Seg_1270" s="T51">you.NOM</ta>
            <ta e="T53" id="Seg_1271" s="T52">riddle-NOM/GEN/ACC.2SG</ta>
            <ta e="T54" id="Seg_1272" s="T53">this.[NOM.SG]</ta>
            <ta e="T55" id="Seg_1273" s="T54">table.[NOM.SG]</ta>
            <ta e="T56" id="Seg_1274" s="T55">three.[NOM.SG]</ta>
            <ta e="T57" id="Seg_1275" s="T56">four.[NOM.SG]</ta>
            <ta e="T58" id="Seg_1276" s="T57">stand-PRS-3PL</ta>
            <ta e="T59" id="Seg_1277" s="T58">and</ta>
            <ta e="T61" id="Seg_1278" s="T60">cap-NOM/GEN.3SG</ta>
            <ta e="T62" id="Seg_1279" s="T61">table-NOM/GEN/ACC.3SG</ta>
            <ta e="T64" id="Seg_1280" s="T63">four.[NOM.SG]</ta>
            <ta e="T65" id="Seg_1281" s="T64">brother-PL</ta>
            <ta e="T66" id="Seg_1282" s="T65">one.[NOM.SG]</ta>
            <ta e="T67" id="Seg_1283" s="T66">cap-LAT/LOC.3SG</ta>
            <ta e="T68" id="Seg_1284" s="T67">sit-</ta>
            <ta e="T69" id="Seg_1285" s="T68">stand-PRS-3PL</ta>
            <ta e="T70" id="Seg_1286" s="T69">then</ta>
            <ta e="T71" id="Seg_1287" s="T70">two.[NOM.SG]</ta>
            <ta e="T72" id="Seg_1288" s="T71">brother.[NOM.SG]</ta>
            <ta e="T73" id="Seg_1289" s="T72">run-DUR-3PL</ta>
            <ta e="T74" id="Seg_1290" s="T73">two.[NOM.SG]</ta>
            <ta e="T75" id="Seg_1291" s="T74">catch.up-DUR-3PL</ta>
            <ta e="T76" id="Seg_1292" s="T75">and</ta>
            <ta e="T77" id="Seg_1293" s="T76">tell-IMP.2SG.O</ta>
            <ta e="T78" id="Seg_1294" s="T77">this.[NOM.SG]</ta>
            <ta e="T79" id="Seg_1295" s="T78">what.[NOM.SG]</ta>
            <ta e="T80" id="Seg_1296" s="T79">such</ta>
            <ta e="T81" id="Seg_1297" s="T80">cart.[NOM.SG]</ta>
            <ta e="T82" id="Seg_1298" s="T81">and</ta>
            <ta e="T83" id="Seg_1299" s="T82">wheel-PL</ta>
            <ta e="T84" id="Seg_1300" s="T83">run-DUR-3PL</ta>
            <ta e="T85" id="Seg_1301" s="T84">two.[NOM.SG]</ta>
            <ta e="T87" id="Seg_1302" s="T86">brother.[NOM.SG]</ta>
            <ta e="T90" id="Seg_1303" s="T89">stand-PRS-3PL</ta>
            <ta e="T91" id="Seg_1304" s="T90">and</ta>
            <ta e="T92" id="Seg_1305" s="T91">this.[NOM.SG]</ta>
            <ta e="T93" id="Seg_1306" s="T92">what.[NOM.SG]</ta>
            <ta e="T94" id="Seg_1307" s="T93">such</ta>
            <ta e="T95" id="Seg_1308" s="T94">and</ta>
            <ta e="T96" id="Seg_1309" s="T95">two.[NOM.SG]</ta>
            <ta e="T97" id="Seg_1310" s="T96">tree.[NOM.SG]</ta>
            <ta e="T98" id="Seg_1311" s="T97">stand-DUR-3PL</ta>
            <ta e="T99" id="Seg_1312" s="T98">and</ta>
            <ta e="T100" id="Seg_1313" s="T99">this-PL</ta>
            <ta e="T101" id="Seg_1314" s="T100">this-ACC.PL</ta>
            <ta e="T102" id="Seg_1315" s="T101">bind-MOM-3PL</ta>
            <ta e="T104" id="Seg_1316" s="T103">mother-NOM/GEN.3SG</ta>
            <ta e="T106" id="Seg_1317" s="T105">daughter-NOM/GEN.3SG</ta>
            <ta e="T107" id="Seg_1318" s="T106">red.[NOM.SG]</ta>
            <ta e="T108" id="Seg_1319" s="T107">son-NOM/GEN.3SG</ta>
            <ta e="T110" id="Seg_1320" s="T109">beautiful.[NOM.SG]</ta>
            <ta e="T111" id="Seg_1321" s="T110">up</ta>
            <ta e="T151" id="Seg_1322" s="T111">go-CVB</ta>
            <ta e="T112" id="Seg_1323" s="T151">disappear-PST.[3SG]</ta>
            <ta e="T113" id="Seg_1324" s="T112">this.[NOM.SG]</ta>
            <ta e="T114" id="Seg_1325" s="T113">big.[NOM.SG]</ta>
            <ta e="T115" id="Seg_1326" s="T114">stove.[NOM.SG]</ta>
            <ta e="T116" id="Seg_1327" s="T115">and</ta>
            <ta e="T117" id="Seg_1328" s="T116">fire.[NOM.SG]</ta>
            <ta e="T119" id="Seg_1329" s="T118">daughter-NOM/GEN.3SG</ta>
            <ta e="T120" id="Seg_1330" s="T119">and</ta>
            <ta e="T121" id="Seg_1331" s="T120">smoke.[NOM.SG]</ta>
            <ta e="T123" id="Seg_1332" s="T122">son-NOM/GEN.3SG</ta>
            <ta e="T124" id="Seg_1333" s="T123">water-LOC</ta>
            <ta e="T125" id="Seg_1334" s="T124">go-FUT-1SG-go-FUT-1SG</ta>
            <ta e="T126" id="Seg_1335" s="T125">road.[NOM.SG]</ta>
            <ta e="T127" id="Seg_1336" s="T126">NEG.EX.[3SG]</ta>
            <ta e="T128" id="Seg_1337" s="T127">knife-INS</ta>
            <ta e="T130" id="Seg_1338" s="T129">knife-INS</ta>
            <ta e="T131" id="Seg_1339" s="T130">cut-FUT-1SG-cut-FUT-1SG</ta>
            <ta e="T132" id="Seg_1340" s="T131">blood.[NOM.SG]</ta>
            <ta e="T133" id="Seg_1341" s="T132">NEG.EX.[3SG]</ta>
            <ta e="T134" id="Seg_1342" s="T133">this</ta>
            <ta e="T135" id="Seg_1343" s="T134">water-LOC</ta>
            <ta e="T136" id="Seg_1344" s="T135">walk-PRS-2SG</ta>
            <ta e="T138" id="Seg_1345" s="T137">road.[NOM.SG]</ta>
            <ta e="T139" id="Seg_1346" s="T138">NEG.EX.[3SG]</ta>
            <ta e="T140" id="Seg_1347" s="T139">and</ta>
            <ta e="T141" id="Seg_1348" s="T140">knife-INS</ta>
            <ta e="T142" id="Seg_1349" s="T141">bread.[NOM.SG]</ta>
            <ta e="T143" id="Seg_1350" s="T142">cut-FUT-2SG</ta>
            <ta e="T145" id="Seg_1351" s="T144">blood.[NOM.SG]</ta>
            <ta e="T146" id="Seg_1352" s="T145">NEG.EX.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1353" s="T0">четыре.[NOM.SG]</ta>
            <ta e="T2" id="Seg_1354" s="T1">пленка-PL</ta>
            <ta e="T4" id="Seg_1355" s="T3">стоять-PST-3PL</ta>
            <ta e="T5" id="Seg_1356" s="T4">стоять-%%</ta>
            <ta e="T6" id="Seg_1357" s="T5">наверное</ta>
            <ta e="T7" id="Seg_1358" s="T6">PTCL</ta>
            <ta e="T8" id="Seg_1359" s="T7">лгать-DUR-3PL</ta>
            <ta e="T10" id="Seg_1360" s="T9">четыре.[NOM.SG]</ta>
            <ta e="T11" id="Seg_1361" s="T10">дочь-PL</ta>
            <ta e="T12" id="Seg_1362" s="T11">один.[NOM.SG]</ta>
            <ta e="T13" id="Seg_1363" s="T12">%%-LAT</ta>
            <ta e="T15" id="Seg_1364" s="T14">мочиться-DUR-3PL</ta>
            <ta e="T16" id="Seg_1365" s="T15">этот.[NOM.SG]</ta>
            <ta e="T17" id="Seg_1366" s="T16">этот</ta>
            <ta e="T18" id="Seg_1367" s="T17">что.[NOM.SG]</ta>
            <ta e="T19" id="Seg_1368" s="T18">такой.[NOM.SG]</ta>
            <ta e="T20" id="Seg_1369" s="T19">этот.[NOM.SG]</ta>
            <ta e="T21" id="Seg_1370" s="T20">корова.[NOM.SG]</ta>
            <ta e="T22" id="Seg_1371" s="T21">доить-PRS-2SG</ta>
            <ta e="T23" id="Seg_1372" s="T22">четыре.[NOM.SG]</ta>
            <ta e="T24" id="Seg_1373" s="T23">грудь-PL</ta>
            <ta e="T25" id="Seg_1374" s="T24">вот</ta>
            <ta e="T26" id="Seg_1375" s="T25">какой</ta>
            <ta e="T27" id="Seg_1376" s="T26">четыре.[NOM.SG]</ta>
            <ta e="T28" id="Seg_1377" s="T27">дочь-PL</ta>
            <ta e="T29" id="Seg_1378" s="T28">один.[NOM.SG]</ta>
            <ta e="T30" id="Seg_1379" s="T29">%%-LAT</ta>
            <ta e="T31" id="Seg_1380" s="T30">мочиться-DUR-3PL</ta>
            <ta e="T32" id="Seg_1381" s="T31">а</ta>
            <ta e="T33" id="Seg_1382" s="T32">что.[NOM.SG]</ta>
            <ta e="T34" id="Seg_1383" s="T33">этот.[NOM.SG]</ta>
            <ta e="T35" id="Seg_1384" s="T34">такой</ta>
            <ta e="T36" id="Seg_1385" s="T35">что.[NOM.SG]</ta>
            <ta e="T37" id="Seg_1386" s="T36">корова.[NOM.SG]</ta>
            <ta e="T38" id="Seg_1387" s="T37">четыре.[NOM.SG]</ta>
            <ta e="T39" id="Seg_1388" s="T38">грудь.[NOM.SG]</ta>
            <ta e="T40" id="Seg_1389" s="T39">один.[NOM.SG]</ta>
            <ta e="T41" id="Seg_1390" s="T40">%%-LAT</ta>
            <ta e="T43" id="Seg_1391" s="T42">молоко.[NOM.SG]</ta>
            <ta e="T44" id="Seg_1392" s="T43">идти-DUR.[3SG]</ta>
            <ta e="T46" id="Seg_1393" s="T45">а</ta>
            <ta e="T47" id="Seg_1394" s="T46">этот.[NOM.SG]</ta>
            <ta e="T48" id="Seg_1395" s="T47">спросить-CVB</ta>
            <ta e="T49" id="Seg_1396" s="T48">что.[NOM.SG]</ta>
            <ta e="T50" id="Seg_1397" s="T49">этот.[NOM.SG]</ta>
            <ta e="T51" id="Seg_1398" s="T50">такой</ta>
            <ta e="T52" id="Seg_1399" s="T51">ты.NOM</ta>
            <ta e="T53" id="Seg_1400" s="T52">загадка-NOM/GEN/ACC.2SG</ta>
            <ta e="T54" id="Seg_1401" s="T53">этот.[NOM.SG]</ta>
            <ta e="T55" id="Seg_1402" s="T54">стол.[NOM.SG]</ta>
            <ta e="T56" id="Seg_1403" s="T55">три.[NOM.SG]</ta>
            <ta e="T57" id="Seg_1404" s="T56">четыре.[NOM.SG]</ta>
            <ta e="T58" id="Seg_1405" s="T57">стоять-PRS-3PL</ta>
            <ta e="T59" id="Seg_1406" s="T58">а</ta>
            <ta e="T61" id="Seg_1407" s="T60">шапка-NOM/GEN.3SG</ta>
            <ta e="T62" id="Seg_1408" s="T61">стол-NOM/GEN/ACC.3SG</ta>
            <ta e="T64" id="Seg_1409" s="T63">четыре.[NOM.SG]</ta>
            <ta e="T65" id="Seg_1410" s="T64">брат-PL</ta>
            <ta e="T66" id="Seg_1411" s="T65">один.[NOM.SG]</ta>
            <ta e="T67" id="Seg_1412" s="T66">шапка-LAT/LOC.3SG</ta>
            <ta e="T68" id="Seg_1413" s="T67">сидеть-</ta>
            <ta e="T69" id="Seg_1414" s="T68">стоять-PRS-3PL</ta>
            <ta e="T70" id="Seg_1415" s="T69">тогда</ta>
            <ta e="T71" id="Seg_1416" s="T70">два.[NOM.SG]</ta>
            <ta e="T72" id="Seg_1417" s="T71">брат.[NOM.SG]</ta>
            <ta e="T73" id="Seg_1418" s="T72">бежать-DUR-3PL</ta>
            <ta e="T74" id="Seg_1419" s="T73">два.[NOM.SG]</ta>
            <ta e="T75" id="Seg_1420" s="T74">догонять-DUR-3PL</ta>
            <ta e="T76" id="Seg_1421" s="T75">а</ta>
            <ta e="T77" id="Seg_1422" s="T76">сказать-IMP.2SG.O</ta>
            <ta e="T78" id="Seg_1423" s="T77">этот.[NOM.SG]</ta>
            <ta e="T79" id="Seg_1424" s="T78">что.[NOM.SG]</ta>
            <ta e="T80" id="Seg_1425" s="T79">такой</ta>
            <ta e="T81" id="Seg_1426" s="T80">телега.[NOM.SG]</ta>
            <ta e="T82" id="Seg_1427" s="T81">а</ta>
            <ta e="T83" id="Seg_1428" s="T82">колесо-PL</ta>
            <ta e="T84" id="Seg_1429" s="T83">бежать-DUR-3PL</ta>
            <ta e="T85" id="Seg_1430" s="T84">два.[NOM.SG]</ta>
            <ta e="T87" id="Seg_1431" s="T86">брат.[NOM.SG]</ta>
            <ta e="T90" id="Seg_1432" s="T89">стоять-PRS-3PL</ta>
            <ta e="T91" id="Seg_1433" s="T90">а</ta>
            <ta e="T92" id="Seg_1434" s="T91">этот.[NOM.SG]</ta>
            <ta e="T93" id="Seg_1435" s="T92">что.[NOM.SG]</ta>
            <ta e="T94" id="Seg_1436" s="T93">такой</ta>
            <ta e="T95" id="Seg_1437" s="T94">и</ta>
            <ta e="T96" id="Seg_1438" s="T95">два.[NOM.SG]</ta>
            <ta e="T97" id="Seg_1439" s="T96">дерево.[NOM.SG]</ta>
            <ta e="T98" id="Seg_1440" s="T97">стоять-DUR-3PL</ta>
            <ta e="T99" id="Seg_1441" s="T98">а</ta>
            <ta e="T100" id="Seg_1442" s="T99">этот-PL</ta>
            <ta e="T101" id="Seg_1443" s="T100">этот-ACC.PL</ta>
            <ta e="T102" id="Seg_1444" s="T101">завязать-MOM-3PL</ta>
            <ta e="T104" id="Seg_1445" s="T103">мать-NOM/GEN.3SG</ta>
            <ta e="T106" id="Seg_1446" s="T105">дочь-NOM/GEN.3SG</ta>
            <ta e="T107" id="Seg_1447" s="T106">красный.[NOM.SG]</ta>
            <ta e="T108" id="Seg_1448" s="T107">сын-NOM/GEN.3SG</ta>
            <ta e="T110" id="Seg_1449" s="T109">красивый.[NOM.SG]</ta>
            <ta e="T111" id="Seg_1450" s="T110">вверх</ta>
            <ta e="T151" id="Seg_1451" s="T111">пойти-CVB</ta>
            <ta e="T112" id="Seg_1452" s="T151">исчезнуть-PST.[3SG]</ta>
            <ta e="T113" id="Seg_1453" s="T112">этот.[NOM.SG]</ta>
            <ta e="T114" id="Seg_1454" s="T113">большой.[NOM.SG]</ta>
            <ta e="T115" id="Seg_1455" s="T114">печь.[NOM.SG]</ta>
            <ta e="T116" id="Seg_1456" s="T115">а</ta>
            <ta e="T117" id="Seg_1457" s="T116">огонь.[NOM.SG]</ta>
            <ta e="T119" id="Seg_1458" s="T118">дочь-NOM/GEN.3SG</ta>
            <ta e="T120" id="Seg_1459" s="T119">а</ta>
            <ta e="T121" id="Seg_1460" s="T120">дым.[NOM.SG]</ta>
            <ta e="T123" id="Seg_1461" s="T122">сын-NOM/GEN.3SG</ta>
            <ta e="T124" id="Seg_1462" s="T123">вода-LOC</ta>
            <ta e="T125" id="Seg_1463" s="T124">пойти-FUT-1SG-пойти-FUT-1SG</ta>
            <ta e="T126" id="Seg_1464" s="T125">дорога.[NOM.SG]</ta>
            <ta e="T127" id="Seg_1465" s="T126">NEG.EX.[3SG]</ta>
            <ta e="T128" id="Seg_1466" s="T127">нож-INS</ta>
            <ta e="T130" id="Seg_1467" s="T129">нож-INS</ta>
            <ta e="T131" id="Seg_1468" s="T130">резать-FUT-1SG-резать-FUT-1SG</ta>
            <ta e="T132" id="Seg_1469" s="T131">кровь.[NOM.SG]</ta>
            <ta e="T133" id="Seg_1470" s="T132">NEG.EX.[3SG]</ta>
            <ta e="T134" id="Seg_1471" s="T133">этот</ta>
            <ta e="T135" id="Seg_1472" s="T134">вода-LOC</ta>
            <ta e="T136" id="Seg_1473" s="T135">идти-PRS-2SG</ta>
            <ta e="T138" id="Seg_1474" s="T137">дорога.[NOM.SG]</ta>
            <ta e="T139" id="Seg_1475" s="T138">NEG.EX.[3SG]</ta>
            <ta e="T140" id="Seg_1476" s="T139">а</ta>
            <ta e="T141" id="Seg_1477" s="T140">нож-INS</ta>
            <ta e="T142" id="Seg_1478" s="T141">хлеб.[NOM.SG]</ta>
            <ta e="T143" id="Seg_1479" s="T142">резать-FUT-2SG</ta>
            <ta e="T145" id="Seg_1480" s="T144">кровь.[NOM.SG]</ta>
            <ta e="T146" id="Seg_1481" s="T145">NEG.EX.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1482" s="T0">num-n:case</ta>
            <ta e="T2" id="Seg_1483" s="T1">n-n:num</ta>
            <ta e="T4" id="Seg_1484" s="T3">v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_1485" s="T4">v-%%</ta>
            <ta e="T6" id="Seg_1486" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_1487" s="T6">ptcl</ta>
            <ta e="T8" id="Seg_1488" s="T7">v-v&gt;v-v:pn</ta>
            <ta e="T10" id="Seg_1489" s="T9">num-n:case</ta>
            <ta e="T11" id="Seg_1490" s="T10">n-n:num</ta>
            <ta e="T12" id="Seg_1491" s="T11">num-n:case</ta>
            <ta e="T13" id="Seg_1492" s="T12">%%-n:case</ta>
            <ta e="T15" id="Seg_1493" s="T14">v-v&gt;v-v:pn</ta>
            <ta e="T16" id="Seg_1494" s="T15">dempro-n:case</ta>
            <ta e="T17" id="Seg_1495" s="T16">dempro</ta>
            <ta e="T18" id="Seg_1496" s="T17">que-n:case</ta>
            <ta e="T19" id="Seg_1497" s="T18">adj-n:case</ta>
            <ta e="T20" id="Seg_1498" s="T19">dempro-n:case</ta>
            <ta e="T21" id="Seg_1499" s="T20">n-n:case</ta>
            <ta e="T22" id="Seg_1500" s="T21">v-v:tense-v:pn</ta>
            <ta e="T23" id="Seg_1501" s="T22">num-n:case</ta>
            <ta e="T24" id="Seg_1502" s="T23">n-n:num</ta>
            <ta e="T25" id="Seg_1503" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_1504" s="T25">que</ta>
            <ta e="T27" id="Seg_1505" s="T26">num-n:case</ta>
            <ta e="T28" id="Seg_1506" s="T27">n-n:num</ta>
            <ta e="T29" id="Seg_1507" s="T28">num-n:case</ta>
            <ta e="T30" id="Seg_1508" s="T29">%%-n:case</ta>
            <ta e="T31" id="Seg_1509" s="T30">v-v&gt;v-v:pn</ta>
            <ta e="T32" id="Seg_1510" s="T31">conj</ta>
            <ta e="T33" id="Seg_1511" s="T32">que-n:case</ta>
            <ta e="T34" id="Seg_1512" s="T33">dempro-n:case</ta>
            <ta e="T35" id="Seg_1513" s="T34">adj</ta>
            <ta e="T36" id="Seg_1514" s="T35">que-n:case</ta>
            <ta e="T37" id="Seg_1515" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_1516" s="T37">num-n:case</ta>
            <ta e="T39" id="Seg_1517" s="T38">n-n:case</ta>
            <ta e="T40" id="Seg_1518" s="T39">num-n:case</ta>
            <ta e="T41" id="Seg_1519" s="T40">%%-n:case</ta>
            <ta e="T43" id="Seg_1520" s="T42">n-n:case</ta>
            <ta e="T44" id="Seg_1521" s="T43">v-v&gt;v-v:pn</ta>
            <ta e="T46" id="Seg_1522" s="T45">conj</ta>
            <ta e="T47" id="Seg_1523" s="T46">dempro-n:case</ta>
            <ta e="T48" id="Seg_1524" s="T47">v-v:n-fin</ta>
            <ta e="T49" id="Seg_1525" s="T48">que-n:case</ta>
            <ta e="T50" id="Seg_1526" s="T49">dempro-n:case</ta>
            <ta e="T51" id="Seg_1527" s="T50">adj</ta>
            <ta e="T52" id="Seg_1528" s="T51">pers</ta>
            <ta e="T53" id="Seg_1529" s="T52">n-n:case-poss</ta>
            <ta e="T54" id="Seg_1530" s="T53">dempro-n:case</ta>
            <ta e="T55" id="Seg_1531" s="T54">n-n:case</ta>
            <ta e="T56" id="Seg_1532" s="T55">num-n:case</ta>
            <ta e="T57" id="Seg_1533" s="T56">num-n:case</ta>
            <ta e="T58" id="Seg_1534" s="T57">v-v:tense-v:pn</ta>
            <ta e="T59" id="Seg_1535" s="T58">conj</ta>
            <ta e="T61" id="Seg_1536" s="T60">n-n:case-poss</ta>
            <ta e="T62" id="Seg_1537" s="T61">n-n:case-poss</ta>
            <ta e="T64" id="Seg_1538" s="T63">num-n:case</ta>
            <ta e="T65" id="Seg_1539" s="T64">n-n:num</ta>
            <ta e="T66" id="Seg_1540" s="T65">num-n:case</ta>
            <ta e="T67" id="Seg_1541" s="T66">n-n:case-poss</ta>
            <ta e="T68" id="Seg_1542" s="T67">v</ta>
            <ta e="T69" id="Seg_1543" s="T68">v-v:tense-v:pn</ta>
            <ta e="T70" id="Seg_1544" s="T69">adv</ta>
            <ta e="T71" id="Seg_1545" s="T70">num-n:case</ta>
            <ta e="T72" id="Seg_1546" s="T71">n-n:case</ta>
            <ta e="T73" id="Seg_1547" s="T72">v-v&gt;v-v:pn</ta>
            <ta e="T74" id="Seg_1548" s="T73">num-n:case</ta>
            <ta e="T75" id="Seg_1549" s="T74">v-v&gt;v-v:pn</ta>
            <ta e="T76" id="Seg_1550" s="T75">conj</ta>
            <ta e="T77" id="Seg_1551" s="T76">v-v:mood-pn</ta>
            <ta e="T78" id="Seg_1552" s="T77">dempro-n:case</ta>
            <ta e="T79" id="Seg_1553" s="T78">que-n:case</ta>
            <ta e="T80" id="Seg_1554" s="T79">adj</ta>
            <ta e="T81" id="Seg_1555" s="T80">n-n:case</ta>
            <ta e="T82" id="Seg_1556" s="T81">conj</ta>
            <ta e="T83" id="Seg_1557" s="T82">n-n:num</ta>
            <ta e="T84" id="Seg_1558" s="T83">v-v&gt;v-v:pn</ta>
            <ta e="T85" id="Seg_1559" s="T84">num-n:case</ta>
            <ta e="T87" id="Seg_1560" s="T86">n-n:case</ta>
            <ta e="T90" id="Seg_1561" s="T89">v-v:tense-v:pn</ta>
            <ta e="T91" id="Seg_1562" s="T90">conj</ta>
            <ta e="T92" id="Seg_1563" s="T91">dempro-n:case</ta>
            <ta e="T93" id="Seg_1564" s="T92">que-n:case</ta>
            <ta e="T94" id="Seg_1565" s="T93">adj</ta>
            <ta e="T95" id="Seg_1566" s="T94">conj</ta>
            <ta e="T96" id="Seg_1567" s="T95">num-n:case</ta>
            <ta e="T97" id="Seg_1568" s="T96">n-n:case</ta>
            <ta e="T98" id="Seg_1569" s="T97">v-v&gt;v-v:pn</ta>
            <ta e="T99" id="Seg_1570" s="T98">conj</ta>
            <ta e="T100" id="Seg_1571" s="T99">dempro-n:num</ta>
            <ta e="T101" id="Seg_1572" s="T100">dempro-n:case</ta>
            <ta e="T102" id="Seg_1573" s="T101">v-v&gt;v-v:pn</ta>
            <ta e="T104" id="Seg_1574" s="T103">n-n:case-poss</ta>
            <ta e="T106" id="Seg_1575" s="T105">n-n:case-poss</ta>
            <ta e="T107" id="Seg_1576" s="T106">adj-n:case</ta>
            <ta e="T108" id="Seg_1577" s="T107">n-n:case-poss</ta>
            <ta e="T110" id="Seg_1578" s="T109">adj-n:case</ta>
            <ta e="T111" id="Seg_1579" s="T110">adv</ta>
            <ta e="T151" id="Seg_1580" s="T111">v-v:n-fin</ta>
            <ta e="T112" id="Seg_1581" s="T151">v-v:tense-v:pn</ta>
            <ta e="T113" id="Seg_1582" s="T112">dempro-n:case</ta>
            <ta e="T114" id="Seg_1583" s="T113">adj-n:case</ta>
            <ta e="T115" id="Seg_1584" s="T114">n-n:case</ta>
            <ta e="T116" id="Seg_1585" s="T115">conj</ta>
            <ta e="T117" id="Seg_1586" s="T116">n-n:case</ta>
            <ta e="T119" id="Seg_1587" s="T118">n-n:case-poss</ta>
            <ta e="T120" id="Seg_1588" s="T119">conj</ta>
            <ta e="T121" id="Seg_1589" s="T120">n-n:case</ta>
            <ta e="T123" id="Seg_1590" s="T122">n-n:case-poss</ta>
            <ta e="T124" id="Seg_1591" s="T123">n-n:case</ta>
            <ta e="T125" id="Seg_1592" s="T124">v-v:tense-v:pn-v-v:tense-v:pn</ta>
            <ta e="T126" id="Seg_1593" s="T125">n-n:case</ta>
            <ta e="T127" id="Seg_1594" s="T126">v-v:pn</ta>
            <ta e="T128" id="Seg_1595" s="T127">n-n:case</ta>
            <ta e="T130" id="Seg_1596" s="T129">n-n:case</ta>
            <ta e="T131" id="Seg_1597" s="T130">v-v:tense-v:pn-v-v:tense-v:pn</ta>
            <ta e="T132" id="Seg_1598" s="T131">n-n:case</ta>
            <ta e="T133" id="Seg_1599" s="T132">v-v:pn</ta>
            <ta e="T134" id="Seg_1600" s="T133">dempro</ta>
            <ta e="T135" id="Seg_1601" s="T134">n-n:case</ta>
            <ta e="T136" id="Seg_1602" s="T135">v-v:tense-v:pn</ta>
            <ta e="T138" id="Seg_1603" s="T137">n-n:case</ta>
            <ta e="T139" id="Seg_1604" s="T138">v-v:pn</ta>
            <ta e="T140" id="Seg_1605" s="T139">conj</ta>
            <ta e="T141" id="Seg_1606" s="T140">n-n:case</ta>
            <ta e="T142" id="Seg_1607" s="T141">n-n:case</ta>
            <ta e="T143" id="Seg_1608" s="T142">v-v:tense-v:pn</ta>
            <ta e="T145" id="Seg_1609" s="T144">n-n:case</ta>
            <ta e="T146" id="Seg_1610" s="T145">v-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1611" s="T0">num</ta>
            <ta e="T2" id="Seg_1612" s="T1">n</ta>
            <ta e="T4" id="Seg_1613" s="T3">v</ta>
            <ta e="T5" id="Seg_1614" s="T4">v</ta>
            <ta e="T6" id="Seg_1615" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_1616" s="T6">ptcl</ta>
            <ta e="T8" id="Seg_1617" s="T7">v</ta>
            <ta e="T10" id="Seg_1618" s="T9">num</ta>
            <ta e="T11" id="Seg_1619" s="T10">n</ta>
            <ta e="T12" id="Seg_1620" s="T11">num</ta>
            <ta e="T13" id="Seg_1621" s="T12">n</ta>
            <ta e="T15" id="Seg_1622" s="T14">v</ta>
            <ta e="T16" id="Seg_1623" s="T15">dempro</ta>
            <ta e="T17" id="Seg_1624" s="T16">dempro</ta>
            <ta e="T18" id="Seg_1625" s="T17">que</ta>
            <ta e="T19" id="Seg_1626" s="T18">adj</ta>
            <ta e="T20" id="Seg_1627" s="T19">dempro</ta>
            <ta e="T21" id="Seg_1628" s="T20">n</ta>
            <ta e="T22" id="Seg_1629" s="T21">v</ta>
            <ta e="T23" id="Seg_1630" s="T22">num</ta>
            <ta e="T24" id="Seg_1631" s="T23">n</ta>
            <ta e="T25" id="Seg_1632" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_1633" s="T25">que</ta>
            <ta e="T27" id="Seg_1634" s="T26">num</ta>
            <ta e="T28" id="Seg_1635" s="T27">n</ta>
            <ta e="T29" id="Seg_1636" s="T28">num</ta>
            <ta e="T30" id="Seg_1637" s="T29">n</ta>
            <ta e="T31" id="Seg_1638" s="T30">v</ta>
            <ta e="T32" id="Seg_1639" s="T31">conj</ta>
            <ta e="T33" id="Seg_1640" s="T32">que</ta>
            <ta e="T34" id="Seg_1641" s="T33">dempro</ta>
            <ta e="T35" id="Seg_1642" s="T34">adj</ta>
            <ta e="T36" id="Seg_1643" s="T35">que</ta>
            <ta e="T37" id="Seg_1644" s="T36">n</ta>
            <ta e="T38" id="Seg_1645" s="T37">num</ta>
            <ta e="T39" id="Seg_1646" s="T38">n</ta>
            <ta e="T40" id="Seg_1647" s="T39">num</ta>
            <ta e="T41" id="Seg_1648" s="T40">n</ta>
            <ta e="T43" id="Seg_1649" s="T42">n</ta>
            <ta e="T44" id="Seg_1650" s="T43">v</ta>
            <ta e="T46" id="Seg_1651" s="T45">conj</ta>
            <ta e="T47" id="Seg_1652" s="T46">dempro</ta>
            <ta e="T48" id="Seg_1653" s="T47">v</ta>
            <ta e="T49" id="Seg_1654" s="T48">que</ta>
            <ta e="T50" id="Seg_1655" s="T49">dempro</ta>
            <ta e="T51" id="Seg_1656" s="T50">adj</ta>
            <ta e="T52" id="Seg_1657" s="T51">pers</ta>
            <ta e="T53" id="Seg_1658" s="T52">n</ta>
            <ta e="T54" id="Seg_1659" s="T53">dempro</ta>
            <ta e="T55" id="Seg_1660" s="T54">n</ta>
            <ta e="T56" id="Seg_1661" s="T55">num</ta>
            <ta e="T57" id="Seg_1662" s="T56">num</ta>
            <ta e="T58" id="Seg_1663" s="T57">v</ta>
            <ta e="T59" id="Seg_1664" s="T58">conj</ta>
            <ta e="T61" id="Seg_1665" s="T60">n</ta>
            <ta e="T62" id="Seg_1666" s="T61">n</ta>
            <ta e="T64" id="Seg_1667" s="T63">num</ta>
            <ta e="T65" id="Seg_1668" s="T64">n</ta>
            <ta e="T66" id="Seg_1669" s="T65">num</ta>
            <ta e="T67" id="Seg_1670" s="T66">n</ta>
            <ta e="T68" id="Seg_1671" s="T67">v</ta>
            <ta e="T69" id="Seg_1672" s="T68">v</ta>
            <ta e="T70" id="Seg_1673" s="T69">adv</ta>
            <ta e="T71" id="Seg_1674" s="T70">num</ta>
            <ta e="T72" id="Seg_1675" s="T71">n</ta>
            <ta e="T73" id="Seg_1676" s="T72">v</ta>
            <ta e="T74" id="Seg_1677" s="T73">num</ta>
            <ta e="T75" id="Seg_1678" s="T74">v</ta>
            <ta e="T76" id="Seg_1679" s="T75">conj</ta>
            <ta e="T77" id="Seg_1680" s="T76">v</ta>
            <ta e="T78" id="Seg_1681" s="T77">dempro</ta>
            <ta e="T79" id="Seg_1682" s="T78">que</ta>
            <ta e="T80" id="Seg_1683" s="T79">adj</ta>
            <ta e="T81" id="Seg_1684" s="T80">n</ta>
            <ta e="T82" id="Seg_1685" s="T81">conj</ta>
            <ta e="T83" id="Seg_1686" s="T82">n</ta>
            <ta e="T84" id="Seg_1687" s="T83">v</ta>
            <ta e="T85" id="Seg_1688" s="T84">num</ta>
            <ta e="T87" id="Seg_1689" s="T86">n</ta>
            <ta e="T90" id="Seg_1690" s="T89">v</ta>
            <ta e="T91" id="Seg_1691" s="T90">conj</ta>
            <ta e="T92" id="Seg_1692" s="T91">dempro</ta>
            <ta e="T93" id="Seg_1693" s="T92">que</ta>
            <ta e="T94" id="Seg_1694" s="T93">adj</ta>
            <ta e="T95" id="Seg_1695" s="T94">conj</ta>
            <ta e="T96" id="Seg_1696" s="T95">num</ta>
            <ta e="T97" id="Seg_1697" s="T96">n</ta>
            <ta e="T98" id="Seg_1698" s="T97">v</ta>
            <ta e="T99" id="Seg_1699" s="T98">conj</ta>
            <ta e="T100" id="Seg_1700" s="T99">dempro</ta>
            <ta e="T101" id="Seg_1701" s="T100">dempro</ta>
            <ta e="T102" id="Seg_1702" s="T101">v</ta>
            <ta e="T104" id="Seg_1703" s="T103">n</ta>
            <ta e="T106" id="Seg_1704" s="T105">n</ta>
            <ta e="T107" id="Seg_1705" s="T106">adj</ta>
            <ta e="T108" id="Seg_1706" s="T107">n</ta>
            <ta e="T110" id="Seg_1707" s="T109">adj</ta>
            <ta e="T111" id="Seg_1708" s="T110">adv</ta>
            <ta e="T151" id="Seg_1709" s="T111">v</ta>
            <ta e="T112" id="Seg_1710" s="T151">v</ta>
            <ta e="T113" id="Seg_1711" s="T112">dempro</ta>
            <ta e="T114" id="Seg_1712" s="T113">adj</ta>
            <ta e="T115" id="Seg_1713" s="T114">n</ta>
            <ta e="T116" id="Seg_1714" s="T115">conj</ta>
            <ta e="T117" id="Seg_1715" s="T116">n</ta>
            <ta e="T119" id="Seg_1716" s="T118">n</ta>
            <ta e="T120" id="Seg_1717" s="T119">conj</ta>
            <ta e="T121" id="Seg_1718" s="T120">n</ta>
            <ta e="T123" id="Seg_1719" s="T122">n</ta>
            <ta e="T124" id="Seg_1720" s="T123">n</ta>
            <ta e="T125" id="Seg_1721" s="T124">v</ta>
            <ta e="T126" id="Seg_1722" s="T125">n</ta>
            <ta e="T127" id="Seg_1723" s="T126">v</ta>
            <ta e="T128" id="Seg_1724" s="T127">n</ta>
            <ta e="T130" id="Seg_1725" s="T129">n</ta>
            <ta e="T131" id="Seg_1726" s="T130">v</ta>
            <ta e="T132" id="Seg_1727" s="T131">n</ta>
            <ta e="T133" id="Seg_1728" s="T132">v</ta>
            <ta e="T134" id="Seg_1729" s="T133">dempro</ta>
            <ta e="T135" id="Seg_1730" s="T134">n</ta>
            <ta e="T136" id="Seg_1731" s="T135">v</ta>
            <ta e="T138" id="Seg_1732" s="T137">n</ta>
            <ta e="T139" id="Seg_1733" s="T138">v</ta>
            <ta e="T140" id="Seg_1734" s="T139">conj</ta>
            <ta e="T141" id="Seg_1735" s="T140">n</ta>
            <ta e="T142" id="Seg_1736" s="T141">n</ta>
            <ta e="T143" id="Seg_1737" s="T142">v</ta>
            <ta e="T145" id="Seg_1738" s="T144">n</ta>
            <ta e="T146" id="Seg_1739" s="T145">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_1740" s="T1">np:Th</ta>
            <ta e="T8" id="Seg_1741" s="T7">0.3.h:A</ta>
            <ta e="T11" id="Seg_1742" s="T10">np.h:A</ta>
            <ta e="T13" id="Seg_1743" s="T12">np:G</ta>
            <ta e="T17" id="Seg_1744" s="T16">pro:Th</ta>
            <ta e="T21" id="Seg_1745" s="T20">np:Th</ta>
            <ta e="T22" id="Seg_1746" s="T21">0.2.h:A</ta>
            <ta e="T28" id="Seg_1747" s="T27">np.h:A</ta>
            <ta e="T30" id="Seg_1748" s="T29">np:G</ta>
            <ta e="T33" id="Seg_1749" s="T32">pro:Th</ta>
            <ta e="T34" id="Seg_1750" s="T33">pro:Th</ta>
            <ta e="T39" id="Seg_1751" s="T38">np:Th</ta>
            <ta e="T41" id="Seg_1752" s="T40">np:G</ta>
            <ta e="T43" id="Seg_1753" s="T42">np:Th</ta>
            <ta e="T47" id="Seg_1754" s="T46">pro.h:A</ta>
            <ta e="T49" id="Seg_1755" s="T48">pro:Th</ta>
            <ta e="T50" id="Seg_1756" s="T49">pro:Th</ta>
            <ta e="T52" id="Seg_1757" s="T51">pro.h:Poss</ta>
            <ta e="T54" id="Seg_1758" s="T53">pro:Th</ta>
            <ta e="T55" id="Seg_1759" s="T54">np:Th</ta>
            <ta e="T57" id="Seg_1760" s="T56">np.h:Th</ta>
            <ta e="T61" id="Seg_1761" s="T60">np:Th</ta>
            <ta e="T62" id="Seg_1762" s="T61">np:Th</ta>
            <ta e="T65" id="Seg_1763" s="T64">np.h:A</ta>
            <ta e="T67" id="Seg_1764" s="T66">np:L</ta>
            <ta e="T70" id="Seg_1765" s="T69">adv:Time</ta>
            <ta e="T72" id="Seg_1766" s="T71">np.h:A</ta>
            <ta e="T74" id="Seg_1767" s="T73">np.h:A</ta>
            <ta e="T77" id="Seg_1768" s="T76">0.2.h:A</ta>
            <ta e="T78" id="Seg_1769" s="T77">pro:Th</ta>
            <ta e="T79" id="Seg_1770" s="T78">pro:Th</ta>
            <ta e="T83" id="Seg_1771" s="T82">np:Th</ta>
            <ta e="T87" id="Seg_1772" s="T86">np.h:Th</ta>
            <ta e="T92" id="Seg_1773" s="T91">pro:Th</ta>
            <ta e="T93" id="Seg_1774" s="T92">pro:Th</ta>
            <ta e="T97" id="Seg_1775" s="T96">np:Th</ta>
            <ta e="T101" id="Seg_1776" s="T100">pro:P</ta>
            <ta e="T102" id="Seg_1777" s="T101">0.3.h:A</ta>
            <ta e="T104" id="Seg_1778" s="T103">np.h:Th</ta>
            <ta e="T106" id="Seg_1779" s="T105">np.h:Th</ta>
            <ta e="T108" id="Seg_1780" s="T107">np.h:Th</ta>
            <ta e="T111" id="Seg_1781" s="T110">adv:Pth</ta>
            <ta e="T113" id="Seg_1782" s="T112">pro:Th</ta>
            <ta e="T115" id="Seg_1783" s="T114">np:Th</ta>
            <ta e="T117" id="Seg_1784" s="T116">np:Th</ta>
            <ta e="T119" id="Seg_1785" s="T118">np:Th</ta>
            <ta e="T121" id="Seg_1786" s="T120">np:Th</ta>
            <ta e="T123" id="Seg_1787" s="T122">np:Th</ta>
            <ta e="T124" id="Seg_1788" s="T123">np:L</ta>
            <ta e="T125" id="Seg_1789" s="T124">0.1.h:A</ta>
            <ta e="T126" id="Seg_1790" s="T125">np:Th</ta>
            <ta e="T130" id="Seg_1791" s="T129">np:Ins</ta>
            <ta e="T131" id="Seg_1792" s="T130">0.1.h:A</ta>
            <ta e="T132" id="Seg_1793" s="T131">np:Th</ta>
            <ta e="T135" id="Seg_1794" s="T134">np:L</ta>
            <ta e="T136" id="Seg_1795" s="T135">0.2.h:A</ta>
            <ta e="T138" id="Seg_1796" s="T137">np:Th</ta>
            <ta e="T141" id="Seg_1797" s="T140">np:Ins</ta>
            <ta e="T142" id="Seg_1798" s="T141">np:P</ta>
            <ta e="T143" id="Seg_1799" s="T142">0.2.h:A</ta>
            <ta e="T145" id="Seg_1800" s="T144">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_1801" s="T1">np:O</ta>
            <ta e="T5" id="Seg_1802" s="T4">v:pred</ta>
            <ta e="T8" id="Seg_1803" s="T7">v:pred 0.3.h:S</ta>
            <ta e="T11" id="Seg_1804" s="T10">np.h:S</ta>
            <ta e="T15" id="Seg_1805" s="T14">v:pred</ta>
            <ta e="T17" id="Seg_1806" s="T16">pro:S</ta>
            <ta e="T18" id="Seg_1807" s="T17">n:pred</ta>
            <ta e="T21" id="Seg_1808" s="T20">np:O</ta>
            <ta e="T22" id="Seg_1809" s="T21">v:pred 0.2.h:S</ta>
            <ta e="T28" id="Seg_1810" s="T27">np.h:S</ta>
            <ta e="T31" id="Seg_1811" s="T30">v:pred</ta>
            <ta e="T33" id="Seg_1812" s="T32">pro:S</ta>
            <ta e="T34" id="Seg_1813" s="T33">n:pred</ta>
            <ta e="T43" id="Seg_1814" s="T42">np:S</ta>
            <ta e="T44" id="Seg_1815" s="T43">v:pred</ta>
            <ta e="T47" id="Seg_1816" s="T46">pro.h:S</ta>
            <ta e="T48" id="Seg_1817" s="T47">conv:pred</ta>
            <ta e="T49" id="Seg_1818" s="T48">pro:S</ta>
            <ta e="T50" id="Seg_1819" s="T49">n:pred</ta>
            <ta e="T54" id="Seg_1820" s="T53">pro:S</ta>
            <ta e="T55" id="Seg_1821" s="T54">n:pred</ta>
            <ta e="T57" id="Seg_1822" s="T56">np.h:S</ta>
            <ta e="T58" id="Seg_1823" s="T57">v:pred</ta>
            <ta e="T61" id="Seg_1824" s="T60">np:S</ta>
            <ta e="T62" id="Seg_1825" s="T61">n:pred</ta>
            <ta e="T65" id="Seg_1826" s="T64">np.h:S</ta>
            <ta e="T69" id="Seg_1827" s="T68">v:pred</ta>
            <ta e="T72" id="Seg_1828" s="T71">np.h:S</ta>
            <ta e="T73" id="Seg_1829" s="T72">v:pred</ta>
            <ta e="T74" id="Seg_1830" s="T73">np.h:S</ta>
            <ta e="T75" id="Seg_1831" s="T74">v:pred</ta>
            <ta e="T77" id="Seg_1832" s="T76">v:pred 0.2.h:S</ta>
            <ta e="T78" id="Seg_1833" s="T77">pro:S</ta>
            <ta e="T79" id="Seg_1834" s="T78">n:pred</ta>
            <ta e="T83" id="Seg_1835" s="T82">np:S</ta>
            <ta e="T84" id="Seg_1836" s="T83">v:pred</ta>
            <ta e="T87" id="Seg_1837" s="T86">np.h:S</ta>
            <ta e="T90" id="Seg_1838" s="T89">v:pred</ta>
            <ta e="T92" id="Seg_1839" s="T91">pro:S</ta>
            <ta e="T93" id="Seg_1840" s="T92">n:pred</ta>
            <ta e="T97" id="Seg_1841" s="T96">np:S</ta>
            <ta e="T98" id="Seg_1842" s="T97">v:pred</ta>
            <ta e="T101" id="Seg_1843" s="T100">pro:O</ta>
            <ta e="T102" id="Seg_1844" s="T101">v:pred 0.3.h:S</ta>
            <ta e="T104" id="Seg_1845" s="T103">np.h:S</ta>
            <ta e="T106" id="Seg_1846" s="T105">np.h:S</ta>
            <ta e="T107" id="Seg_1847" s="T106">adj:pred</ta>
            <ta e="T108" id="Seg_1848" s="T107">np.h:S</ta>
            <ta e="T112" id="Seg_1849" s="T111">v:pred</ta>
            <ta e="T113" id="Seg_1850" s="T112">pro:S</ta>
            <ta e="T115" id="Seg_1851" s="T114">n:pred</ta>
            <ta e="T117" id="Seg_1852" s="T116">np:S</ta>
            <ta e="T119" id="Seg_1853" s="T118">n:pred</ta>
            <ta e="T121" id="Seg_1854" s="T120">np:S</ta>
            <ta e="T123" id="Seg_1855" s="T122">n:pred</ta>
            <ta e="T125" id="Seg_1856" s="T124">v:pred 0.1.h:S</ta>
            <ta e="T126" id="Seg_1857" s="T125">np:S</ta>
            <ta e="T127" id="Seg_1858" s="T126">v:pred</ta>
            <ta e="T131" id="Seg_1859" s="T130">v:pred 0.1.h:S</ta>
            <ta e="T132" id="Seg_1860" s="T131">np:S</ta>
            <ta e="T133" id="Seg_1861" s="T132">v:pred</ta>
            <ta e="T136" id="Seg_1862" s="T135">v:pred 0.2.h:S</ta>
            <ta e="T138" id="Seg_1863" s="T137">np:S</ta>
            <ta e="T139" id="Seg_1864" s="T138">v:pred</ta>
            <ta e="T142" id="Seg_1865" s="T141">np:O</ta>
            <ta e="T143" id="Seg_1866" s="T142">v:pred 0.2.h:S</ta>
            <ta e="T145" id="Seg_1867" s="T144">np:S</ta>
            <ta e="T146" id="Seg_1868" s="T145">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_1869" s="T1">RUS:cult</ta>
            <ta e="T6" id="Seg_1870" s="T5">RUS:mod</ta>
            <ta e="T7" id="Seg_1871" s="T6">TURK:disc</ta>
            <ta e="T21" id="Seg_1872" s="T20">TURK:cult</ta>
            <ta e="T25" id="Seg_1873" s="T24">RUS:mod</ta>
            <ta e="T32" id="Seg_1874" s="T31">RUS:gram</ta>
            <ta e="T37" id="Seg_1875" s="T36">TURK:cult</ta>
            <ta e="T43" id="Seg_1876" s="T42">TURK:cult</ta>
            <ta e="T46" id="Seg_1877" s="T45">RUS:gram</ta>
            <ta e="T53" id="Seg_1878" s="T52">RUS:core</ta>
            <ta e="T55" id="Seg_1879" s="T54">RUS:cult</ta>
            <ta e="T59" id="Seg_1880" s="T58">RUS:gram</ta>
            <ta e="T62" id="Seg_1881" s="T61">RUS:cult</ta>
            <ta e="T76" id="Seg_1882" s="T75">RUS:gram</ta>
            <ta e="T81" id="Seg_1883" s="T80">RUS:cult</ta>
            <ta e="T82" id="Seg_1884" s="T81">RUS:gram</ta>
            <ta e="T83" id="Seg_1885" s="T82">RUS:cult</ta>
            <ta e="T91" id="Seg_1886" s="T90">RUS:gram</ta>
            <ta e="T95" id="Seg_1887" s="T94">RUS:gram</ta>
            <ta e="T99" id="Seg_1888" s="T98">RUS:gram</ta>
            <ta e="T115" id="Seg_1889" s="T114">RUS:cult</ta>
            <ta e="T116" id="Seg_1890" s="T115">RUS:gram</ta>
            <ta e="T120" id="Seg_1891" s="T119">RUS:gram</ta>
            <ta e="T140" id="Seg_1892" s="T139">RUS:gram</ta>
            <ta e="T142" id="Seg_1893" s="T141">TURK:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T25" id="Seg_1894" s="T24">inCdel</ta>
            <ta e="T115" id="Seg_1895" s="T114">Csub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T2" id="Seg_1896" s="T1">dir:infl</ta>
            <ta e="T55" id="Seg_1897" s="T54">dir:bare</ta>
            <ta e="T62" id="Seg_1898" s="T61">dir:infl</ta>
            <ta e="T81" id="Seg_1899" s="T80">dir:bare</ta>
            <ta e="T83" id="Seg_1900" s="T82">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS">
            <ta e="T150" id="Seg_1901" s="T146">RUS:ext</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T8" id="Seg_1902" s="T0">(Четвёртую?) плёнку поставил, наверное врут. [?]</ta>
            <ta e="T15" id="Seg_1903" s="T9">Четыре девочки в одну дырку (/лунку?) писают.</ta>
            <ta e="T19" id="Seg_1904" s="T15">Что это?</ta>
            <ta e="T22" id="Seg_1905" s="T19">Это [когда] корову доишь.</ta>
            <ta e="T24" id="Seg_1906" s="T22">Четыре соска.</ta>
            <ta e="T26" id="Seg_1907" s="T24">Вот так.</ta>
            <ta e="T31" id="Seg_1908" s="T26">Четыре девочки в одну дырку (/лунку?) писают.</ta>
            <ta e="T35" id="Seg_1909" s="T31">А что это?</ta>
            <ta e="T37" id="Seg_1910" s="T35">Что, корова!</ta>
            <ta e="T44" id="Seg_1911" s="T37">Четыре соска в одну (дырку?) молоко дают [=льют].</ta>
            <ta e="T53" id="Seg_1912" s="T45">А этот [человек] спрашивает: «Что это такое, твоя загадка?»</ta>
            <ta e="T55" id="Seg_1913" s="T53">Это стол.</ta>
            <ta e="T62" id="Seg_1914" s="T55">Четыре человека стоят, а их шляпа [это] стол.</ta>
            <ta e="T69" id="Seg_1915" s="T63">Четыре брата в одной шляпе стоят.</ta>
            <ta e="T75" id="Seg_1916" s="T69">Два брата бегут, два догоняют.</ta>
            <ta e="T80" id="Seg_1917" s="T75">А скажи, что это такое?</ta>
            <ta e="T84" id="Seg_1918" s="T80">Телега, а колёса бегут.</ta>
            <ta e="T90" id="Seg_1919" s="T84">Два брата стоят (?).</ta>
            <ta e="T94" id="Seg_1920" s="T90">А это что такое?</ta>
            <ta e="T102" id="Seg_1921" s="T94">Да два дерева стоят, а их связывают. [?]</ta>
            <ta e="T112" id="Seg_1922" s="T103">Мать толстая, дочь красная, сын с красивыми (волосами?) вверх пошёл.</ta>
            <ta e="T115" id="Seg_1923" s="T112">Это большая печь.</ta>
            <ta e="T123" id="Seg_1924" s="T115">А огонь — её дочь, а дым — её сын.</ta>
            <ta e="T127" id="Seg_1925" s="T123">По воде хожу-хожу, а дороги нет.</ta>
            <ta e="T133" id="Seg_1926" s="T127">Ножом режу-режу, а крови нет.</ta>
            <ta e="T139" id="Seg_1927" s="T133">По реке едешь — дороги нет.</ta>
            <ta e="T146" id="Seg_1928" s="T139">А ножом хлеб режешь — крови нет.</ta>
            <ta e="T150" id="Seg_1929" s="T146">Это уж (эта?) загадка. </ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T8" id="Seg_1930" s="T0">You inserted the (fourth?) tape, they must be lying. [?]</ta>
            <ta e="T15" id="Seg_1931" s="T9">Four girls urinate in one hole.</ta>
            <ta e="T19" id="Seg_1932" s="T15">What's this?</ta>
            <ta e="T22" id="Seg_1933" s="T19">This is [when] you milk a cow.</ta>
            <ta e="T24" id="Seg_1934" s="T22">Four teats.</ta>
            <ta e="T26" id="Seg_1935" s="T24">That's it.</ta>
            <ta e="T31" id="Seg_1936" s="T26">Four girls urinate into one hole.</ta>
            <ta e="T35" id="Seg_1937" s="T31">What's it?</ta>
            <ta e="T37" id="Seg_1938" s="T35">What, a cow!</ta>
            <ta e="T44" id="Seg_1939" s="T37">Four teats give [=pour] milk in one (hole?).</ta>
            <ta e="T53" id="Seg_1940" s="T45">And this [person] asks: "What's it, your riddle?"</ta>
            <ta e="T55" id="Seg_1941" s="T53">This is a table.</ta>
            <ta e="T62" id="Seg_1942" s="T55">Four person stay, and their hat is a table.</ta>
            <ta e="T69" id="Seg_1943" s="T63">Four brothers stay [under] one hat.</ta>
            <ta e="T75" id="Seg_1944" s="T69">Then two brothers run, two follow [them].</ta>
            <ta e="T80" id="Seg_1945" s="T75">Say, what's this?</ta>
            <ta e="T84" id="Seg_1946" s="T80">A cart, [its] wheels run.</ta>
            <ta e="T90" id="Seg_1947" s="T84">Two brothers stay (?).</ta>
            <ta e="T94" id="Seg_1948" s="T90">What's this?</ta>
            <ta e="T102" id="Seg_1949" s="T94">Two trees stay, and they get bound. [?]</ta>
            <ta e="T112" id="Seg_1950" s="T103">Mother is fat, daughter is red, son with the beautiful (hair?) went upwards.</ta>
            <ta e="T115" id="Seg_1951" s="T112">It's a big stove.</ta>
            <ta e="T123" id="Seg_1952" s="T115">The fire is her daughter, the smoke is her son.</ta>
            <ta e="T127" id="Seg_1953" s="T123">I'm walking on the water, there is no road.</ta>
            <ta e="T133" id="Seg_1954" s="T127">I'm cutting with a knife, there is no blood.</ta>
            <ta e="T139" id="Seg_1955" s="T133">When you go on the river, there is no road.</ta>
            <ta e="T146" id="Seg_1956" s="T139">When you slice bread with a knife, there is no blood.</ta>
            <ta e="T150" id="Seg_1957" s="T146">That’s this riddle.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T8" id="Seg_1958" s="T0">Du hast das (vierte?) Band aufgelegt, sie lügen wohl. [?]</ta>
            <ta e="T15" id="Seg_1959" s="T9">Vier Mädchen urinieren in ein Loch.</ta>
            <ta e="T19" id="Seg_1960" s="T15">Was ist das?</ta>
            <ta e="T22" id="Seg_1961" s="T19">Das ist [wenn] du eine Kuh melkst.</ta>
            <ta e="T24" id="Seg_1962" s="T22">Vier Zitzen.</ta>
            <ta e="T26" id="Seg_1963" s="T24">Das ist es.</ta>
            <ta e="T31" id="Seg_1964" s="T26">Vier Mädchen urinieren in ein Loch.</ta>
            <ta e="T35" id="Seg_1965" s="T31">Was ist das?</ta>
            <ta e="T37" id="Seg_1966" s="T35">Was, eine Kuh.</ta>
            <ta e="T44" id="Seg_1967" s="T37">Vier Zitzen geben [=gießen] Milch in ein (Loch?).</ta>
            <ta e="T53" id="Seg_1968" s="T45">And diese [Person] fragt: "Was ist es, dein Rätsel?"</ta>
            <ta e="T55" id="Seg_1969" s="T53">Das ist ein Tisch.</ta>
            <ta e="T62" id="Seg_1970" s="T55">Vier Personen stehen und ihr Hut ist ein Tisch.</ta>
            <ta e="T69" id="Seg_1971" s="T63">Vier Brüder stehen [unter] einem Hut.</ta>
            <ta e="T75" id="Seg_1972" s="T69">Dann rennen zwei Brüder, zwei folgen [ihnen].</ta>
            <ta e="T80" id="Seg_1973" s="T75">Sag, was ist das?</ta>
            <ta e="T84" id="Seg_1974" s="T80">Ein Karren, [seine] Räder laufen.</ta>
            <ta e="T90" id="Seg_1975" s="T84">Zwei Brüder bleiben (?).</ta>
            <ta e="T94" id="Seg_1976" s="T90">Was ist das?</ta>
            <ta e="T102" id="Seg_1977" s="T94">Zwei Bäume bleiben und sie werden festgebunden. [?]</ta>
            <ta e="T112" id="Seg_1978" s="T103">Die Mutter ist fett, die Tochter ist rot, der Sohn mit dem schönen (Haar?) ging hinauf.</ta>
            <ta e="T115" id="Seg_1979" s="T112">Das ist ein großer Ofen.</ta>
            <ta e="T123" id="Seg_1980" s="T115">Das Feuer ist ihre Tochter, der Rauch ist ihr Sohn.</ta>
            <ta e="T127" id="Seg_1981" s="T123">Ich gehe auf dem Wasser, dort ist kein Weg.</ta>
            <ta e="T133" id="Seg_1982" s="T127">Ich schneide mit einem Messer, dort ist kein Blut.</ta>
            <ta e="T139" id="Seg_1983" s="T133">Wenn du auf dem Fluss gehst, ist dort kein Weg.</ta>
            <ta e="T146" id="Seg_1984" s="T139">Wenn du Brot mit dem Messer schneidest, ist dort kein Blut.</ta>
            <ta e="T150" id="Seg_1985" s="T146">Das ist schon dieses Rätsel.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T8" id="Seg_1986" s="T0">[GVY:] Unclear</ta>
            <ta e="T15" id="Seg_1987" s="T9">[GVY:] ulunka - Russian лунка?</ta>
            <ta e="T84" id="Seg_1988" s="T80">[GVY:] k'elega</ta>
            <ta e="T102" id="Seg_1989" s="T94">[GVY:] Unclear</ta>
            <ta e="T112" id="Seg_1990" s="T103">[GVY:] nʼikšpek.</ta>
            <ta e="T133" id="Seg_1991" s="T127">[GVY:] bet- or bĭt-?</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T151" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
