<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID98536023-459D-C1D3-5090-D82B95B3D35F">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_FoxAndFlea_flk.wav" />
         <referenced-file url="PKZ_196X_FoxAndFlea_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_FoxAndFlea_flk\PKZ_196X_FoxAndFlea_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">49</ud-information>
            <ud-information attribute-name="# HIAT:w">34</ud-information>
            <ud-information attribute-name="# e">33</ud-information>
            <ud-information attribute-name="# HIAT:u">8</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T915" time="0.0" type="appl" />
         <tli id="T916" time="0.688" type="appl" />
         <tli id="T917" time="1.376" type="appl" />
         <tli id="T918" time="2.064" type="appl" />
         <tli id="T919" time="2.752" type="appl" />
         <tli id="T920" time="3.553" type="appl" />
         <tli id="T921" time="4.354" type="appl" />
         <tli id="T922" time="5.156" type="appl" />
         <tli id="T923" time="5.957" type="appl" />
         <tli id="T924" time="7.036" type="appl" />
         <tli id="T925" time="8.115" type="appl" />
         <tli id="T926" time="9.194" type="appl" />
         <tli id="T927" time="10.068" type="appl" />
         <tli id="T928" time="10.942" type="appl" />
         <tli id="T929" time="11.817" type="appl" />
         <tli id="T930" time="12.691" type="appl" />
         <tli id="T931" time="13.533" type="appl" />
         <tli id="T932" time="14.374" type="appl" />
         <tli id="T933" time="15.216" type="appl" />
         <tli id="T934" time="16.058" type="appl" />
         <tli id="T935" time="16.9" type="appl" />
         <tli id="T936" time="17.741" type="appl" />
         <tli id="T937" time="18.583" type="appl" />
         <tli id="T938" time="19.164" type="appl" />
         <tli id="T939" time="19.745" type="appl" />
         <tli id="T940" time="20.326" type="appl" />
         <tli id="T941" time="20.907" type="appl" />
         <tli id="T942" time="21.404" type="appl" />
         <tli id="T943" time="21.901" type="appl" />
         <tli id="T944" time="22.397" type="appl" />
         <tli id="T945" time="22.894" type="appl" />
         <tli id="T946" time="23.824896020074124" />
         <tli id="T947" time="24.712" type="appl" />
         <tli id="T948" time="25.514" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T948" start="T947">
            <tli id="T947.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T948" id="Seg_0" n="sc" s="T915">
               <ts e="T919" id="Seg_2" n="HIAT:u" s="T915">
                  <ts e="T916" id="Seg_4" n="HIAT:w" s="T915">Lisitsa</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T917" id="Seg_7" n="HIAT:w" s="T916">i</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_9" n="HIAT:ip">(</nts>
                  <ts e="T918" id="Seg_11" n="HIAT:w" s="T917">un-</ts>
                  <nts id="Seg_12" n="HIAT:ip">)</nts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T919" id="Seg_15" n="HIAT:w" s="T918">unu</ts>
                  <nts id="Seg_16" n="HIAT:ip">.</nts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T923" id="Seg_19" n="HIAT:u" s="T919">
                  <nts id="Seg_20" n="HIAT:ip">"</nts>
                  <ts e="T920" id="Seg_22" n="HIAT:w" s="T919">Davaj</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T921" id="Seg_25" n="HIAT:w" s="T920">nuʔməleʔbəbeʔ</ts>
                  <nts id="Seg_26" n="HIAT:ip">,</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T922" id="Seg_29" n="HIAT:w" s="T921">šində</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T923" id="Seg_32" n="HIAT:w" s="T922">nuʔmələj</ts>
                  <nts id="Seg_33" n="HIAT:ip">"</nts>
                  <nts id="Seg_34" n="HIAT:ip">.</nts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T926" id="Seg_37" n="HIAT:u" s="T923">
                  <ts e="T924" id="Seg_39" n="HIAT:w" s="T923">Dĭgəttə</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T925" id="Seg_42" n="HIAT:w" s="T924">lisitsa</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T926" id="Seg_45" n="HIAT:w" s="T925">nuʔməluʔpi</ts>
                  <nts id="Seg_46" n="HIAT:ip">.</nts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T930" id="Seg_49" n="HIAT:u" s="T926">
                  <ts e="T927" id="Seg_51" n="HIAT:w" s="T926">A</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T928" id="Seg_54" n="HIAT:w" s="T927">un</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T929" id="Seg_57" n="HIAT:w" s="T928">tărdə</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T930" id="Seg_60" n="HIAT:w" s="T929">dʼabəluʔpi</ts>
                  <nts id="Seg_61" n="HIAT:ip">.</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T937" id="Seg_64" n="HIAT:u" s="T930">
                  <ts e="T931" id="Seg_66" n="HIAT:w" s="T930">Dĭ</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T932" id="Seg_69" n="HIAT:w" s="T931">nuʔməleʔ</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T933" id="Seg_72" n="HIAT:w" s="T932">šobi</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T934" id="Seg_75" n="HIAT:w" s="T933">i</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T935" id="Seg_78" n="HIAT:w" s="T934">xvostəndə</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T936" id="Seg_81" n="HIAT:w" s="T935">bar</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T937" id="Seg_84" n="HIAT:w" s="T936">parluʔpi</ts>
                  <nts id="Seg_85" n="HIAT:ip">.</nts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T941" id="Seg_88" n="HIAT:u" s="T937">
                  <ts e="T938" id="Seg_90" n="HIAT:w" s="T937">A</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T939" id="Seg_93" n="HIAT:w" s="T938">unu</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T940" id="Seg_96" n="HIAT:w" s="T939">dĭn</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T941" id="Seg_99" n="HIAT:w" s="T940">amnolaʔbə</ts>
                  <nts id="Seg_100" n="HIAT:ip">.</nts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T946" id="Seg_103" n="HIAT:u" s="T941">
                  <nts id="Seg_104" n="HIAT:ip">"</nts>
                  <ts e="T942" id="Seg_106" n="HIAT:w" s="T941">Măn</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T943" id="Seg_109" n="HIAT:w" s="T942">už</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T944" id="Seg_112" n="HIAT:w" s="T943">davno</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T945" id="Seg_115" n="HIAT:w" s="T944">dĭn</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T946" id="Seg_118" n="HIAT:w" s="T945">amnolaʔbəm</ts>
                  <nts id="Seg_119" n="HIAT:ip">"</nts>
                  <nts id="Seg_120" n="HIAT:ip">.</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T948" id="Seg_123" n="HIAT:u" s="T946">
                  <ts e="T947" id="Seg_125" n="HIAT:w" s="T946">Kabarləj</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T947.tx.1" id="Seg_128" n="HIAT:w" s="T947">što</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T948" id="Seg_131" n="HIAT:w" s="T947.tx.1">li</ts>
                  <nts id="Seg_132" n="HIAT:ip">?</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T948" id="Seg_134" n="sc" s="T915">
               <ts e="T916" id="Seg_136" n="e" s="T915">Lisitsa </ts>
               <ts e="T917" id="Seg_138" n="e" s="T916">i </ts>
               <ts e="T918" id="Seg_140" n="e" s="T917">(un-) </ts>
               <ts e="T919" id="Seg_142" n="e" s="T918">unu. </ts>
               <ts e="T920" id="Seg_144" n="e" s="T919">"Davaj </ts>
               <ts e="T921" id="Seg_146" n="e" s="T920">nuʔməleʔbəbeʔ, </ts>
               <ts e="T922" id="Seg_148" n="e" s="T921">šində </ts>
               <ts e="T923" id="Seg_150" n="e" s="T922">nuʔmələj". </ts>
               <ts e="T924" id="Seg_152" n="e" s="T923">Dĭgəttə </ts>
               <ts e="T925" id="Seg_154" n="e" s="T924">lisitsa </ts>
               <ts e="T926" id="Seg_156" n="e" s="T925">nuʔməluʔpi. </ts>
               <ts e="T927" id="Seg_158" n="e" s="T926">A </ts>
               <ts e="T928" id="Seg_160" n="e" s="T927">un </ts>
               <ts e="T929" id="Seg_162" n="e" s="T928">tărdə </ts>
               <ts e="T930" id="Seg_164" n="e" s="T929">dʼabəluʔpi. </ts>
               <ts e="T931" id="Seg_166" n="e" s="T930">Dĭ </ts>
               <ts e="T932" id="Seg_168" n="e" s="T931">nuʔməleʔ </ts>
               <ts e="T933" id="Seg_170" n="e" s="T932">šobi </ts>
               <ts e="T934" id="Seg_172" n="e" s="T933">i </ts>
               <ts e="T935" id="Seg_174" n="e" s="T934">xvostəndə </ts>
               <ts e="T936" id="Seg_176" n="e" s="T935">bar </ts>
               <ts e="T937" id="Seg_178" n="e" s="T936">parluʔpi. </ts>
               <ts e="T938" id="Seg_180" n="e" s="T937">A </ts>
               <ts e="T939" id="Seg_182" n="e" s="T938">unu </ts>
               <ts e="T940" id="Seg_184" n="e" s="T939">dĭn </ts>
               <ts e="T941" id="Seg_186" n="e" s="T940">amnolaʔbə. </ts>
               <ts e="T942" id="Seg_188" n="e" s="T941">"Măn </ts>
               <ts e="T943" id="Seg_190" n="e" s="T942">už </ts>
               <ts e="T944" id="Seg_192" n="e" s="T943">davno </ts>
               <ts e="T945" id="Seg_194" n="e" s="T944">dĭn </ts>
               <ts e="T946" id="Seg_196" n="e" s="T945">amnolaʔbəm". </ts>
               <ts e="T947" id="Seg_198" n="e" s="T946">Kabarləj </ts>
               <ts e="T948" id="Seg_200" n="e" s="T947">što li? </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T919" id="Seg_201" s="T915">PKZ_196X_FoxAndFlea_flk.001 (001)</ta>
            <ta e="T923" id="Seg_202" s="T919">PKZ_196X_FoxAndFlea_flk.002 (002)</ta>
            <ta e="T926" id="Seg_203" s="T923">PKZ_196X_FoxAndFlea_flk.003 (003)</ta>
            <ta e="T930" id="Seg_204" s="T926">PKZ_196X_FoxAndFlea_flk.004 (004)</ta>
            <ta e="T937" id="Seg_205" s="T930">PKZ_196X_FoxAndFlea_flk.005 (005)</ta>
            <ta e="T941" id="Seg_206" s="T937">PKZ_196X_FoxAndFlea_flk.006 (006)</ta>
            <ta e="T946" id="Seg_207" s="T941">PKZ_196X_FoxAndFlea_flk.007 (007)</ta>
            <ta e="T948" id="Seg_208" s="T946">PKZ_196X_FoxAndFlea_flk.008 (008)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T919" id="Seg_209" s="T915">Lisitsa i (un-) unu. </ta>
            <ta e="T923" id="Seg_210" s="T919">"Davaj nuʔməleʔbəbeʔ, šində nuʔmələj". </ta>
            <ta e="T926" id="Seg_211" s="T923">Dĭgəttə lisitsa nuʔməluʔpi. </ta>
            <ta e="T930" id="Seg_212" s="T926">A un tărdə dʼabəluʔpi. </ta>
            <ta e="T937" id="Seg_213" s="T930">Dĭ nuʔməleʔ šobi i xvostəndə bar parluʔpi. </ta>
            <ta e="T941" id="Seg_214" s="T937">A unu dĭn amnolaʔbə. </ta>
            <ta e="T946" id="Seg_215" s="T941">"Măn už davno dĭn amnolaʔbəm". </ta>
            <ta e="T948" id="Seg_216" s="T946">Kabarləj što li? </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T916" id="Seg_217" s="T915">lisitsa</ta>
            <ta e="T917" id="Seg_218" s="T916">i</ta>
            <ta e="T919" id="Seg_219" s="T918">unu</ta>
            <ta e="T920" id="Seg_220" s="T919">davaj</ta>
            <ta e="T921" id="Seg_221" s="T920">nuʔmə-leʔbə-beʔ</ta>
            <ta e="T922" id="Seg_222" s="T921">šində</ta>
            <ta e="T923" id="Seg_223" s="T922">nuʔmə-lə-j</ta>
            <ta e="T924" id="Seg_224" s="T923">dĭgəttə</ta>
            <ta e="T925" id="Seg_225" s="T924">lisitsa</ta>
            <ta e="T926" id="Seg_226" s="T925">nuʔmə-luʔ-pi</ta>
            <ta e="T927" id="Seg_227" s="T926">a</ta>
            <ta e="T928" id="Seg_228" s="T927">un</ta>
            <ta e="T929" id="Seg_229" s="T928">tăr-də</ta>
            <ta e="T930" id="Seg_230" s="T929">dʼabə-luʔ-pi</ta>
            <ta e="T931" id="Seg_231" s="T930">dĭ</ta>
            <ta e="T932" id="Seg_232" s="T931">nuʔmə-leʔ</ta>
            <ta e="T933" id="Seg_233" s="T932">šo-bi</ta>
            <ta e="T934" id="Seg_234" s="T933">i</ta>
            <ta e="T935" id="Seg_235" s="T934">xvostə-ndə</ta>
            <ta e="T936" id="Seg_236" s="T935">bar</ta>
            <ta e="T937" id="Seg_237" s="T936">par-luʔ-pi</ta>
            <ta e="T938" id="Seg_238" s="T937">a</ta>
            <ta e="T939" id="Seg_239" s="T938">unu</ta>
            <ta e="T940" id="Seg_240" s="T939">dĭn</ta>
            <ta e="T941" id="Seg_241" s="T940">amno-laʔbə</ta>
            <ta e="T942" id="Seg_242" s="T941">măn</ta>
            <ta e="T943" id="Seg_243" s="T942">už</ta>
            <ta e="T944" id="Seg_244" s="T943">davno</ta>
            <ta e="T945" id="Seg_245" s="T944">dĭn</ta>
            <ta e="T946" id="Seg_246" s="T945">amno-laʔbə-m</ta>
            <ta e="T947" id="Seg_247" s="T946">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T916" id="Seg_248" s="T915">lʼisʼitsa</ta>
            <ta e="T917" id="Seg_249" s="T916">i</ta>
            <ta e="T919" id="Seg_250" s="T918">unu</ta>
            <ta e="T920" id="Seg_251" s="T919">davaj</ta>
            <ta e="T921" id="Seg_252" s="T920">nuʔmə-laʔbə-bAʔ</ta>
            <ta e="T922" id="Seg_253" s="T921">šində</ta>
            <ta e="T923" id="Seg_254" s="T922">nuʔmə-lV-j</ta>
            <ta e="T924" id="Seg_255" s="T923">dĭgəttə</ta>
            <ta e="T925" id="Seg_256" s="T924">lʼisʼitsa</ta>
            <ta e="T926" id="Seg_257" s="T925">nuʔmə-luʔbdə-bi</ta>
            <ta e="T927" id="Seg_258" s="T926">a</ta>
            <ta e="T928" id="Seg_259" s="T927">unu</ta>
            <ta e="T929" id="Seg_260" s="T928">tar-də</ta>
            <ta e="T930" id="Seg_261" s="T929">dʼabə-luʔbdə-bi</ta>
            <ta e="T931" id="Seg_262" s="T930">dĭ</ta>
            <ta e="T932" id="Seg_263" s="T931">nuʔmə-lAʔ</ta>
            <ta e="T933" id="Seg_264" s="T932">šo-bi</ta>
            <ta e="T934" id="Seg_265" s="T933">i</ta>
            <ta e="T935" id="Seg_266" s="T934">xvostə-gəndə</ta>
            <ta e="T936" id="Seg_267" s="T935">bar</ta>
            <ta e="T937" id="Seg_268" s="T936">par-luʔbdə-bi</ta>
            <ta e="T938" id="Seg_269" s="T937">a</ta>
            <ta e="T939" id="Seg_270" s="T938">unu</ta>
            <ta e="T940" id="Seg_271" s="T939">dĭn</ta>
            <ta e="T941" id="Seg_272" s="T940">amno-laʔbə</ta>
            <ta e="T942" id="Seg_273" s="T941">măn</ta>
            <ta e="T943" id="Seg_274" s="T942">už</ta>
            <ta e="T944" id="Seg_275" s="T943">davno</ta>
            <ta e="T945" id="Seg_276" s="T944">dĭn</ta>
            <ta e="T946" id="Seg_277" s="T945">amno-laʔbə-m</ta>
            <ta e="T947" id="Seg_278" s="T946">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T916" id="Seg_279" s="T915">fox.[NOM.SG]</ta>
            <ta e="T917" id="Seg_280" s="T916">and</ta>
            <ta e="T919" id="Seg_281" s="T918">louse.[NOM.SG]</ta>
            <ta e="T920" id="Seg_282" s="T919">HORT</ta>
            <ta e="T921" id="Seg_283" s="T920">run-DUR-1PL</ta>
            <ta e="T922" id="Seg_284" s="T921">who.[NOM.SG]</ta>
            <ta e="T923" id="Seg_285" s="T922">run-FUT-3SG</ta>
            <ta e="T924" id="Seg_286" s="T923">then</ta>
            <ta e="T925" id="Seg_287" s="T924">fox.[NOM.SG]</ta>
            <ta e="T926" id="Seg_288" s="T925">run-MOM-PST.[3SG]</ta>
            <ta e="T927" id="Seg_289" s="T926">and</ta>
            <ta e="T928" id="Seg_290" s="T927">louse.[NOM.SG]</ta>
            <ta e="T929" id="Seg_291" s="T928">hair-NOM/GEN/ACC.3SG</ta>
            <ta e="T930" id="Seg_292" s="T929">capture-MOM-PST.[3SG]</ta>
            <ta e="T931" id="Seg_293" s="T930">this.[NOM.SG]</ta>
            <ta e="T932" id="Seg_294" s="T931">run-CVB</ta>
            <ta e="T933" id="Seg_295" s="T932">come-PST.[3SG]</ta>
            <ta e="T934" id="Seg_296" s="T933">and</ta>
            <ta e="T935" id="Seg_297" s="T934">tail-LAT/LOC.3SG</ta>
            <ta e="T936" id="Seg_298" s="T935">PTCL</ta>
            <ta e="T937" id="Seg_299" s="T936">return-MOM-PST.[3SG]</ta>
            <ta e="T938" id="Seg_300" s="T937">and</ta>
            <ta e="T939" id="Seg_301" s="T938">louse.[NOM.SG]</ta>
            <ta e="T940" id="Seg_302" s="T939">there</ta>
            <ta e="T941" id="Seg_303" s="T940">sit-DUR.[3SG]</ta>
            <ta e="T942" id="Seg_304" s="T941">I.NOM</ta>
            <ta e="T943" id="Seg_305" s="T942">PTCL</ta>
            <ta e="T944" id="Seg_306" s="T943">long</ta>
            <ta e="T945" id="Seg_307" s="T944">there</ta>
            <ta e="T946" id="Seg_308" s="T945">sit-DUR-1SG</ta>
            <ta e="T947" id="Seg_309" s="T946">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T916" id="Seg_310" s="T915">лисица.[NOM.SG]</ta>
            <ta e="T917" id="Seg_311" s="T916">и</ta>
            <ta e="T919" id="Seg_312" s="T918">вошь.[NOM.SG]</ta>
            <ta e="T920" id="Seg_313" s="T919">HORT</ta>
            <ta e="T921" id="Seg_314" s="T920">бежать-DUR-1PL</ta>
            <ta e="T922" id="Seg_315" s="T921">кто.[NOM.SG]</ta>
            <ta e="T923" id="Seg_316" s="T922">бежать-FUT-3SG</ta>
            <ta e="T924" id="Seg_317" s="T923">тогда</ta>
            <ta e="T925" id="Seg_318" s="T924">лисица.[NOM.SG]</ta>
            <ta e="T926" id="Seg_319" s="T925">бежать-MOM-PST.[3SG]</ta>
            <ta e="T927" id="Seg_320" s="T926">а</ta>
            <ta e="T928" id="Seg_321" s="T927">вошь.[NOM.SG]</ta>
            <ta e="T929" id="Seg_322" s="T928">шерсть-NOM/GEN/ACC.3SG</ta>
            <ta e="T930" id="Seg_323" s="T929">ловить-MOM-PST.[3SG]</ta>
            <ta e="T931" id="Seg_324" s="T930">этот.[NOM.SG]</ta>
            <ta e="T932" id="Seg_325" s="T931">бежать-CVB</ta>
            <ta e="T933" id="Seg_326" s="T932">прийти-PST.[3SG]</ta>
            <ta e="T934" id="Seg_327" s="T933">и</ta>
            <ta e="T935" id="Seg_328" s="T934">хвост-LAT/LOC.3SG</ta>
            <ta e="T936" id="Seg_329" s="T935">PTCL</ta>
            <ta e="T937" id="Seg_330" s="T936">вернуться-MOM-PST.[3SG]</ta>
            <ta e="T938" id="Seg_331" s="T937">а</ta>
            <ta e="T939" id="Seg_332" s="T938">вошь.[NOM.SG]</ta>
            <ta e="T940" id="Seg_333" s="T939">там</ta>
            <ta e="T941" id="Seg_334" s="T940">сидеть-DUR.[3SG]</ta>
            <ta e="T942" id="Seg_335" s="T941">я.NOM</ta>
            <ta e="T943" id="Seg_336" s="T942">PTCL</ta>
            <ta e="T944" id="Seg_337" s="T943">давно</ta>
            <ta e="T945" id="Seg_338" s="T944">там</ta>
            <ta e="T946" id="Seg_339" s="T945">сидеть-DUR-1SG</ta>
            <ta e="T947" id="Seg_340" s="T946">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T916" id="Seg_341" s="T915">n-n:case</ta>
            <ta e="T917" id="Seg_342" s="T916">conj</ta>
            <ta e="T919" id="Seg_343" s="T918">n-n:case</ta>
            <ta e="T920" id="Seg_344" s="T919">ptcl</ta>
            <ta e="T921" id="Seg_345" s="T920">v-v&gt;v-v:pn</ta>
            <ta e="T922" id="Seg_346" s="T921">que</ta>
            <ta e="T923" id="Seg_347" s="T922">v-v:tense-v:pn</ta>
            <ta e="T924" id="Seg_348" s="T923">adv</ta>
            <ta e="T925" id="Seg_349" s="T924">n-n:case</ta>
            <ta e="T926" id="Seg_350" s="T925">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T927" id="Seg_351" s="T926">conj</ta>
            <ta e="T928" id="Seg_352" s="T927">n-n:case</ta>
            <ta e="T929" id="Seg_353" s="T928">n-n:case.poss</ta>
            <ta e="T930" id="Seg_354" s="T929">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T931" id="Seg_355" s="T930">dempro-n:case</ta>
            <ta e="T932" id="Seg_356" s="T931">v-v:n.fin</ta>
            <ta e="T933" id="Seg_357" s="T932">v-v:tense-v:pn</ta>
            <ta e="T934" id="Seg_358" s="T933">conj</ta>
            <ta e="T935" id="Seg_359" s="T934">n-n:case.poss</ta>
            <ta e="T936" id="Seg_360" s="T935">ptcl</ta>
            <ta e="T937" id="Seg_361" s="T936">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T938" id="Seg_362" s="T937">conj</ta>
            <ta e="T939" id="Seg_363" s="T938">n-n:case</ta>
            <ta e="T940" id="Seg_364" s="T939">adv</ta>
            <ta e="T941" id="Seg_365" s="T940">v-v&gt;v-v:pn</ta>
            <ta e="T942" id="Seg_366" s="T941">pers</ta>
            <ta e="T943" id="Seg_367" s="T942">ptcl</ta>
            <ta e="T944" id="Seg_368" s="T943">n</ta>
            <ta e="T945" id="Seg_369" s="T944">adv</ta>
            <ta e="T946" id="Seg_370" s="T945">v-v&gt;v-v:pn</ta>
            <ta e="T947" id="Seg_371" s="T946">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T916" id="Seg_372" s="T915">n</ta>
            <ta e="T917" id="Seg_373" s="T916">conj</ta>
            <ta e="T919" id="Seg_374" s="T918">n</ta>
            <ta e="T920" id="Seg_375" s="T919">ptcl</ta>
            <ta e="T921" id="Seg_376" s="T920">v</ta>
            <ta e="T922" id="Seg_377" s="T921">que</ta>
            <ta e="T923" id="Seg_378" s="T922">v</ta>
            <ta e="T924" id="Seg_379" s="T923">adv</ta>
            <ta e="T925" id="Seg_380" s="T924">n</ta>
            <ta e="T926" id="Seg_381" s="T925">v</ta>
            <ta e="T927" id="Seg_382" s="T926">conj</ta>
            <ta e="T928" id="Seg_383" s="T927">n</ta>
            <ta e="T929" id="Seg_384" s="T928">n</ta>
            <ta e="T930" id="Seg_385" s="T929">v</ta>
            <ta e="T931" id="Seg_386" s="T930">dempro</ta>
            <ta e="T932" id="Seg_387" s="T931">v</ta>
            <ta e="T933" id="Seg_388" s="T932">v</ta>
            <ta e="T934" id="Seg_389" s="T933">conj</ta>
            <ta e="T935" id="Seg_390" s="T934">n</ta>
            <ta e="T936" id="Seg_391" s="T935">ptcl</ta>
            <ta e="T937" id="Seg_392" s="T936">v</ta>
            <ta e="T938" id="Seg_393" s="T937">conj</ta>
            <ta e="T939" id="Seg_394" s="T938">n</ta>
            <ta e="T940" id="Seg_395" s="T939">adv</ta>
            <ta e="T941" id="Seg_396" s="T940">v</ta>
            <ta e="T942" id="Seg_397" s="T941">pers</ta>
            <ta e="T943" id="Seg_398" s="T942">ptcl</ta>
            <ta e="T944" id="Seg_399" s="T943">n</ta>
            <ta e="T945" id="Seg_400" s="T944">adv</ta>
            <ta e="T946" id="Seg_401" s="T945">v</ta>
            <ta e="T947" id="Seg_402" s="T946">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T921" id="Seg_403" s="T920">0.1.h:A</ta>
            <ta e="T922" id="Seg_404" s="T921">pro.h:A</ta>
            <ta e="T924" id="Seg_405" s="T923">adv:Time</ta>
            <ta e="T925" id="Seg_406" s="T924">np.h:A</ta>
            <ta e="T928" id="Seg_407" s="T927">np.h:A</ta>
            <ta e="T929" id="Seg_408" s="T928">np:Th</ta>
            <ta e="T930" id="Seg_409" s="T929">pro.h:A</ta>
            <ta e="T935" id="Seg_410" s="T934">np:G</ta>
            <ta e="T937" id="Seg_411" s="T936">0.3.h:A</ta>
            <ta e="T939" id="Seg_412" s="T938">np.h:E</ta>
            <ta e="T940" id="Seg_413" s="T939">adv:L</ta>
            <ta e="T942" id="Seg_414" s="T941">pro.h:E</ta>
            <ta e="T945" id="Seg_415" s="T944">adv:L</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T920" id="Seg_416" s="T919">ptcl:pred</ta>
            <ta e="T922" id="Seg_417" s="T921">pro.h:S</ta>
            <ta e="T923" id="Seg_418" s="T922">v:pred</ta>
            <ta e="T925" id="Seg_419" s="T924">np.h:S</ta>
            <ta e="T926" id="Seg_420" s="T925">v:pred</ta>
            <ta e="T928" id="Seg_421" s="T927">np.h:S</ta>
            <ta e="T929" id="Seg_422" s="T928">np:O</ta>
            <ta e="T930" id="Seg_423" s="T929">v:pred</ta>
            <ta e="T931" id="Seg_424" s="T930">pro.h:S</ta>
            <ta e="T932" id="Seg_425" s="T931">conv:pred</ta>
            <ta e="T933" id="Seg_426" s="T932">v:pred</ta>
            <ta e="T937" id="Seg_427" s="T936">v:pred 0.3.h:S</ta>
            <ta e="T939" id="Seg_428" s="T938">np.h:S</ta>
            <ta e="T941" id="Seg_429" s="T940">v:pred</ta>
            <ta e="T942" id="Seg_430" s="T941">pro.h:S</ta>
            <ta e="T946" id="Seg_431" s="T945">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T916" id="Seg_432" s="T915">RUS:cult</ta>
            <ta e="T917" id="Seg_433" s="T916">RUS:gram</ta>
            <ta e="T920" id="Seg_434" s="T919">RUS:gram</ta>
            <ta e="T925" id="Seg_435" s="T924">RUS:cult</ta>
            <ta e="T927" id="Seg_436" s="T926">RUS:gram</ta>
            <ta e="T934" id="Seg_437" s="T933">RUS:gram</ta>
            <ta e="T935" id="Seg_438" s="T934">RUS:core</ta>
            <ta e="T936" id="Seg_439" s="T935">TURK:disc</ta>
            <ta e="T938" id="Seg_440" s="T937">RUS:gram</ta>
            <ta e="T943" id="Seg_441" s="T942">RUS:disc</ta>
            <ta e="T944" id="Seg_442" s="T943">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T948" id="Seg_443" s="T947">RUS:int.alt</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T919" id="Seg_444" s="T915">Лисица и вошь.</ta>
            <ta e="T923" id="Seg_445" s="T919">«Давай побежим, кто [первым] прибежит».</ta>
            <ta e="T926" id="Seg_446" s="T923">Потом лисица побежала.</ta>
            <ta e="T930" id="Seg_447" s="T926">А вошь зацепилась за шерсть.</ta>
            <ta e="T937" id="Seg_448" s="T930">Она побежала и вернулась к своему хвосту. [?]</ta>
            <ta e="T941" id="Seg_449" s="T937">А вошь там сидит.</ta>
            <ta e="T946" id="Seg_450" s="T941">«Я уже давно тут сижу».</ta>
            <ta e="T948" id="Seg_451" s="T946">Хватит, что ли?</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T919" id="Seg_452" s="T915">A fox and a flea.</ta>
            <ta e="T923" id="Seg_453" s="T919">"Let's run, who will run [=arrive] [the first]."</ta>
            <ta e="T926" id="Seg_454" s="T923">Then the fox ran.</ta>
            <ta e="T930" id="Seg_455" s="T926">But the flea held on to its hair.</ta>
            <ta e="T937" id="Seg_456" s="T930">He came running and turned back to its tail. [?]</ta>
            <ta e="T941" id="Seg_457" s="T937">But the flea is sitting there.</ta>
            <ta e="T946" id="Seg_458" s="T941">"I am sitting here already for a long time."</ta>
            <ta e="T948" id="Seg_459" s="T946">Enough or what?</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T919" id="Seg_460" s="T915">Ein Fuchs und ein Floh.</ta>
            <ta e="T923" id="Seg_461" s="T919">„Renne wir, wer wird [als Erster] rennen [=ankommen].“ </ta>
            <ta e="T926" id="Seg_462" s="T923">Dann lief der Fuchs.</ta>
            <ta e="T930" id="Seg_463" s="T926">Aber der Floh hielt sich an seinem Haar fest.</ta>
            <ta e="T937" id="Seg_464" s="T930">Er kam rennend und wandte sich zurück zu seinem Schwanz. [?]</ta>
            <ta e="T941" id="Seg_465" s="T937">Aber der Floh sitzt da.</ta>
            <ta e="T946" id="Seg_466" s="T941">„Ich sitzt schon lange hier.“</ta>
            <ta e="T948" id="Seg_467" s="T946">Genug oder was?</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T919" id="Seg_468" s="T915">[GVY:] ünü is a louse, but here it should be a flea.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T927" />
            <conversion-tli id="T928" />
            <conversion-tli id="T929" />
            <conversion-tli id="T930" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T935" />
            <conversion-tli id="T936" />
            <conversion-tli id="T937" />
            <conversion-tli id="T938" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T941" />
            <conversion-tli id="T942" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
