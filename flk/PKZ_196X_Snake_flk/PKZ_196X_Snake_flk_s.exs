<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID1E1EBA12-FF16-CB07-37B0-F703BE78A308">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_Snake_flk.wav" />
         <referenced-file url="PKZ_196X_Snake_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_Snake_flk\PKZ_196X_Snake_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">378</ud-information>
            <ud-information attribute-name="# HIAT:w">258</ud-information>
            <ud-information attribute-name="# e">258</ud-information>
            <ud-information attribute-name="# HIAT:u">52</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.038" type="appl" />
         <tli id="T1" time="0.808" type="appl" />
         <tli id="T2" time="1.579" type="appl" />
         <tli id="T3" time="2.349" type="appl" />
         <tli id="T4" time="3.158" type="appl" />
         <tli id="T5" time="3.95" type="appl" />
         <tli id="T6" time="4.742" type="appl" />
         <tli id="T7" time="5.535" type="appl" />
         <tli id="T8" time="6.224" type="appl" />
         <tli id="T9" time="6.913" type="appl" />
         <tli id="T10" time="7.602" type="appl" />
         <tli id="T11" time="8.291" type="appl" />
         <tli id="T12" time="8.777" type="appl" />
         <tli id="T13" time="9.263" type="appl" />
         <tli id="T14" time="9.748" type="appl" />
         <tli id="T15" time="10.234" type="appl" />
         <tli id="T16" time="10.72" type="appl" />
         <tli id="T17" time="11.229" type="appl" />
         <tli id="T18" time="11.738" type="appl" />
         <tli id="T19" time="12.247" type="appl" />
         <tli id="T20" time="12.756" type="appl" />
         <tli id="T21" time="13.265" type="appl" />
         <tli id="T22" time="13.938" type="appl" />
         <tli id="T23" time="14.61" type="appl" />
         <tli id="T24" time="15.283" type="appl" />
         <tli id="T25" time="15.955" type="appl" />
         <tli id="T26" time="16.628" type="appl" />
         <tli id="T27" time="17.217" type="appl" />
         <tli id="T28" time="17.806" type="appl" />
         <tli id="T29" time="18.396" type="appl" />
         <tli id="T30" time="18.985" type="appl" />
         <tli id="T31" time="19.365" type="appl" />
         <tli id="T32" time="19.745" type="appl" />
         <tli id="T33" time="20.125" type="appl" />
         <tli id="T34" time="20.505" type="appl" />
         <tli id="T35" time="20.884" type="appl" />
         <tli id="T36" time="21.264" type="appl" />
         <tli id="T37" time="21.644" type="appl" />
         <tli id="T38" time="22.024" type="appl" />
         <tli id="T39" time="22.404" type="appl" />
         <tli id="T40" time="22.812" type="appl" />
         <tli id="T41" time="23.22" type="appl" />
         <tli id="T42" time="23.628" type="appl" />
         <tli id="T43" time="24.036" type="appl" />
         <tli id="T44" time="24.444" type="appl" />
         <tli id="T45" time="24.852" type="appl" />
         <tli id="T46" time="25.26" type="appl" />
         <tli id="T47" time="25.667" type="appl" />
         <tli id="T48" time="26.075" type="appl" />
         <tli id="T49" time="26.483" type="appl" />
         <tli id="T50" time="26.891" type="appl" />
         <tli id="T51" time="27.299" type="appl" />
         <tli id="T52" time="27.707" type="appl" />
         <tli id="T53" time="28.115" type="appl" />
         <tli id="T54" time="29.383" type="appl" />
         <tli id="T55" time="30.689" type="appl" />
         <tli id="T56" time="31.772" type="appl" />
         <tli id="T57" time="32.645" type="appl" />
         <tli id="T58" time="33.268" type="appl" />
         <tli id="T59" time="33.892" type="appl" />
         <tli id="T60" time="34.73" type="appl" />
         <tli id="T61" time="35.343" type="appl" />
         <tli id="T62" time="35.956" type="appl" />
         <tli id="T63" time="36.569" type="appl" />
         <tli id="T64" time="37.182" type="appl" />
         <tli id="T65" time="37.795" type="appl" />
         <tli id="T66" time="38.408" type="appl" />
         <tli id="T67" time="39.302" type="appl" />
         <tli id="T68" time="40.19" type="appl" />
         <tli id="T69" time="41.077" type="appl" />
         <tli id="T70" time="41.965" type="appl" />
         <tli id="T71" time="42.852" type="appl" />
         <tli id="T72" time="44.148" type="appl" />
         <tli id="T73" time="45.443" type="appl" />
         <tli id="T74" time="46.739" type="appl" />
         <tli id="T75" time="49.715" type="appl" />
         <tli id="T76" time="50.851" type="appl" />
         <tli id="T77" time="51.986" type="appl" />
         <tli id="T78" time="53.122" type="appl" />
         <tli id="T79" time="54.258" type="appl" />
         <tli id="T80" time="55.097" type="appl" />
         <tli id="T81" time="55.762" type="appl" />
         <tli id="T82" time="56.428" type="appl" />
         <tli id="T83" time="57.094" type="appl" />
         <tli id="T84" time="60.928" type="appl" />
         <tli id="T85" time="62.058" type="appl" />
         <tli id="T86" time="63.187" type="appl" />
         <tli id="T87" time="64.317" type="appl" />
         <tli id="T88" time="65.934" type="appl" />
         <tli id="T89" time="67.057" type="appl" />
         <tli id="T90" time="71.271" type="appl" />
         <tli id="T91" time="72.083" type="appl" />
         <tli id="T92" time="72.896" type="appl" />
         <tli id="T93" time="73.708" type="appl" />
         <tli id="T94" time="74.52" type="appl" />
         <tli id="T95" time="75.332" type="appl" />
         <tli id="T96" time="76.787" type="appl" />
         <tli id="T97" time="77.476" type="appl" />
         <tli id="T98" time="78.164" type="appl" />
         <tli id="T99" time="78.972" type="appl" />
         <tli id="T100" time="79.781" type="appl" />
         <tli id="T101" time="81.025" type="appl" />
         <tli id="T102" time="81.684" type="appl" />
         <tli id="T103" time="82.344" type="appl" />
         <tli id="T104" time="83.597" type="appl" />
         <tli id="T105" time="84.515" type="appl" />
         <tli id="T106" time="86.069" type="appl" />
         <tli id="T107" time="86.739" type="appl" />
         <tli id="T108" time="88.562" type="appl" />
         <tli id="T109" time="89.24" type="appl" />
         <tli id="T110" time="89.919" type="appl" />
         <tli id="T111" time="90.598" type="appl" />
         <tli id="T112" time="91.277" type="appl" />
         <tli id="T113" time="91.955" type="appl" />
         <tli id="T114" time="92.634" type="appl" />
         <tli id="T115" time="93.369" type="appl" />
         <tli id="T116" time="94.002" type="appl" />
         <tli id="T117" time="94.636" type="appl" />
         <tli id="T118" time="95.269" type="appl" />
         <tli id="T119" time="95.902" type="appl" />
         <tli id="T120" time="96.536" type="appl" />
         <tli id="T121" time="97.169" type="appl" />
         <tli id="T122" time="97.802" type="appl" />
         <tli id="T123" time="99.205" type="appl" />
         <tli id="T124" time="99.992" type="appl" />
         <tli id="T125" time="100.78" type="appl" />
         <tli id="T126" time="101.567" type="appl" />
         <tli id="T127" time="102.355" type="appl" />
         <tli id="T128" time="103.19" type="appl" />
         <tli id="T129" time="103.723" type="appl" />
         <tli id="T130" time="104.256" type="appl" />
         <tli id="T131" time="104.789" type="appl" />
         <tli id="T132" time="105.322" type="appl" />
         <tli id="T133" time="105.855" type="appl" />
         <tli id="T134" time="106.388" type="appl" />
         <tli id="T135" time="106.921" type="appl" />
         <tli id="T136" time="107.821" type="appl" />
         <tli id="T137" time="108.722" type="appl" />
         <tli id="T138" time="109.622" type="appl" />
         <tli id="T139" time="110.523" type="appl" />
         <tli id="T140" time="111.423" type="appl" />
         <tli id="T141" time="112.646" type="appl" />
         <tli id="T142" time="114.424" type="appl" />
         <tli id="T143" time="115.556" type="appl" />
         <tli id="T144" time="116.687" type="appl" />
         <tli id="T145" time="117.818" type="appl" />
         <tli id="T146" time="118.95" type="appl" />
         <tli id="T147" time="120.081" type="appl" />
         <tli id="T148" time="120.802" type="appl" />
         <tli id="T149" time="121.523" type="appl" />
         <tli id="T150" time="122.244" type="appl" />
         <tli id="T151" time="122.966" type="appl" />
         <tli id="T152" time="123.687" type="appl" />
         <tli id="T153" time="124.408" type="appl" />
         <tli id="T154" time="125.129" type="appl" />
         <tli id="T155" time="125.818" type="appl" />
         <tli id="T156" time="126.506" type="appl" />
         <tli id="T157" time="127.195" type="appl" />
         <tli id="T158" time="127.884" type="appl" />
         <tli id="T159" time="128.85" type="appl" />
         <tli id="T160" time="129.817" type="appl" />
         <tli id="T161" time="130.783" type="appl" />
         <tli id="T162" time="131.75" type="appl" />
         <tli id="T163" time="132.716" type="appl" />
         <tli id="T164" time="133.683" type="appl" />
         <tli id="T165" time="134.649" type="appl" />
         <tli id="T166" time="135.377" type="appl" />
         <tli id="T167" time="136.082" type="appl" />
         <tli id="T168" time="136.786" type="appl" />
         <tli id="T169" time="137.49" type="appl" />
         <tli id="T170" time="138.047" type="appl" />
         <tli id="T171" time="138.603" type="appl" />
         <tli id="T172" time="139.16" type="appl" />
         <tli id="T173" time="139.916" type="appl" />
         <tli id="T174" time="140.657" type="appl" />
         <tli id="T175" time="141.397" type="appl" />
         <tli id="T176" time="142.137" type="appl" />
         <tli id="T177" time="142.878" type="appl" />
         <tli id="T178" time="143.618" type="appl" />
         <tli id="T179" time="144.114" type="appl" />
         <tli id="T180" time="144.609" type="appl" />
         <tli id="T181" time="145.105" type="appl" />
         <tli id="T182" time="145.601" type="appl" />
         <tli id="T183" time="146.096" type="appl" />
         <tli id="T184" time="146.592" type="appl" />
         <tli id="T185" time="147.59" type="appl" />
         <tli id="T186" time="148.589" type="appl" />
         <tli id="T187" time="149.587" type="appl" />
         <tli id="T188" time="150.407" type="appl" />
         <tli id="T189" time="151.051" type="appl" />
         <tli id="T190" time="151.695" type="appl" />
         <tli id="T191" time="152.339" type="appl" />
         <tli id="T192" time="152.97" type="appl" />
         <tli id="T193" time="153.601" type="appl" />
         <tli id="T194" time="154.233" type="appl" />
         <tli id="T195" time="154.864" type="appl" />
         <tli id="T196" time="155.495" type="appl" />
         <tli id="T197" time="156.171" type="appl" />
         <tli id="T198" time="156.848" type="appl" />
         <tli id="T199" time="157.524" type="appl" />
         <tli id="T200" time="158.201" type="appl" />
         <tli id="T201" time="158.877" type="appl" />
         <tli id="T202" time="159.554" type="appl" />
         <tli id="T203" time="160.23" type="appl" />
         <tli id="T204" time="160.907" type="appl" />
         <tli id="T205" time="161.583" type="appl" />
         <tli id="T206" time="162.26" type="appl" />
         <tli id="T207" time="162.936" type="appl" />
         <tli id="T208" time="164.551" type="appl" />
         <tli id="T209" time="165.719" type="appl" />
         <tli id="T210" time="166.886" type="appl" />
         <tli id="T211" time="168.054" type="appl" />
         <tli id="T212" time="169.222" type="appl" />
         <tli id="T213" time="170.39" type="appl" />
         <tli id="T214" time="172.267" type="appl" />
         <tli id="T215" time="172.834" type="appl" />
         <tli id="T216" time="173.401" type="appl" />
         <tli id="T217" time="173.968" type="appl" />
         <tli id="T218" time="174.535" type="appl" />
         <tli id="T219" time="175.007" type="appl" />
         <tli id="T220" time="175.479" type="appl" />
         <tli id="T221" time="175.95" type="appl" />
         <tli id="T222" time="176.422" type="appl" />
         <tli id="T223" time="176.894" type="appl" />
         <tli id="T224" time="177.366" type="appl" />
         <tli id="T225" time="178.02" type="appl" />
         <tli id="T226" time="178.674" type="appl" />
         <tli id="T227" time="179.328" type="appl" />
         <tli id="T228" time="179.981" type="appl" />
         <tli id="T229" time="180.635" type="appl" />
         <tli id="T230" time="181.289" type="appl" />
         <tli id="T231" time="181.916" type="appl" />
         <tli id="T232" time="182.544" type="appl" />
         <tli id="T233" time="183.171" type="appl" />
         <tli id="T234" time="183.798" type="appl" />
         <tli id="T235" time="185.05" type="appl" />
         <tli id="T236" time="185.927" type="appl" />
         <tli id="T237" time="186.443" type="appl" />
         <tli id="T238" time="186.958" type="appl" />
         <tli id="T239" time="187.474" type="appl" />
         <tli id="T240" time="187.99" type="appl" />
         <tli id="T241" time="188.505" type="appl" />
         <tli id="T242" time="189.021" type="appl" />
         <tli id="T243" time="189.537" type="appl" />
         <tli id="T244" time="190.052" type="appl" />
         <tli id="T245" time="190.568" type="appl" />
         <tli id="T246" time="191.084" type="appl" />
         <tli id="T247" time="191.599" type="appl" />
         <tli id="T248" time="192.115" type="appl" />
         <tli id="T249" time="192.746" type="appl" />
         <tli id="T250" time="193.376" type="appl" />
         <tli id="T251" time="194.007" type="appl" />
         <tli id="T252" time="194.637" type="appl" />
         <tli id="T253" time="195.268" type="appl" />
         <tli id="T254" time="195.98" type="appl" />
         <tli id="T255" time="196.499" type="appl" />
         <tli id="T256" time="197.018" type="appl" />
         <tli id="T257" time="197.536" type="appl" />
         <tli id="T258" time="198.055" type="appl" />
         <tli id="T259" time="198.574" type="appl" />
         <tli id="T260" time="199.607" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T260" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Kuza</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">šonəga</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">stʼepʼju</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Kuliat</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">bar:</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">šü</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">iʔbolaʔbə</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_29" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">A</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">šügən</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">penzə</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">iʔbolaʔbə</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_44" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">Dĭ</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">pa</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">ibi</ts>
                  <nts id="Seg_53" n="HIAT:ip">,</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">dĭm</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">baʔluʔpi</ts>
                  <nts id="Seg_60" n="HIAT:ip">.</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_63" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_65" n="HIAT:w" s="T16">A</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_68" n="HIAT:w" s="T17">dĭ</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_71" n="HIAT:w" s="T18">dĭʔnə</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_74" n="HIAT:w" s="T19">bar</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_77" n="HIAT:w" s="T20">šejanə</ts>
                  <nts id="Seg_78" n="HIAT:ip">.</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_81" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_83" n="HIAT:w" s="T21">Dĭgəttə</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_86" n="HIAT:w" s="T22">bar</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_89" n="HIAT:w" s="T23">dĭm</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_92" n="HIAT:w" s="T24">pʼaŋdlaʔbə</ts>
                  <nts id="Seg_93" n="HIAT:ip">,</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_96" n="HIAT:w" s="T25">pʼaŋdlaʔbə</ts>
                  <nts id="Seg_97" n="HIAT:ip">.</nts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_100" n="HIAT:u" s="T26">
                  <nts id="Seg_101" n="HIAT:ip">"</nts>
                  <ts e="T27" id="Seg_103" n="HIAT:w" s="T26">Ĭmbi</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_106" n="HIAT:w" s="T27">tăn</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_109" n="HIAT:w" s="T28">măna</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_112" n="HIAT:w" s="T29">pʼaŋdlaʔbəl</ts>
                  <nts id="Seg_113" n="HIAT:ip">?</nts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_116" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_118" n="HIAT:w" s="T30">Măn</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_120" n="HIAT:ip">(</nts>
                  <ts e="T32" id="Seg_122" n="HIAT:w" s="T31">tăn=</ts>
                  <nts id="Seg_123" n="HIAT:ip">)</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_126" n="HIAT:w" s="T32">tănan</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_129" n="HIAT:w" s="T33">jakšə</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_132" n="HIAT:w" s="T34">abiam</ts>
                  <nts id="Seg_133" n="HIAT:ip">,</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_136" n="HIAT:w" s="T35">a</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_139" n="HIAT:w" s="T36">tăn</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_142" n="HIAT:w" s="T37">măna</ts>
                  <nts id="Seg_143" n="HIAT:ip">…</nts>
                  <nts id="Seg_144" n="HIAT:ip">"</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_147" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_149" n="HIAT:w" s="T39">A</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_152" n="HIAT:w" s="T40">dĭ</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_154" n="HIAT:ip">(</nts>
                  <ts e="T42" id="Seg_156" n="HIAT:w" s="T41">măna-</ts>
                  <nts id="Seg_157" n="HIAT:ip">)</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_160" n="HIAT:w" s="T42">măndə:</ts>
                  <nts id="Seg_161" n="HIAT:ip">"</nts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_164" n="HIAT:w" s="T43">Tăn</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_167" n="HIAT:w" s="T44">jakšə</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_169" n="HIAT:ip">(</nts>
                  <ts e="T46" id="Seg_171" n="HIAT:w" s="T45">m-</ts>
                  <nts id="Seg_172" n="HIAT:ip">)</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_175" n="HIAT:w" s="T46">abial</ts>
                  <nts id="Seg_176" n="HIAT:ip">,</nts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_179" n="HIAT:w" s="T47">a</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_182" n="HIAT:w" s="T48">măn</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_185" n="HIAT:w" s="T49">tănan</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_188" n="HIAT:w" s="T50">ej</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_191" n="HIAT:w" s="T51">jakše</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_194" n="HIAT:w" s="T52">abiam</ts>
                  <nts id="Seg_195" n="HIAT:ip">.</nts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_198" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_200" n="HIAT:w" s="T53">Kanžəbəj</ts>
                  <nts id="Seg_201" n="HIAT:ip">.</nts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_204" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_206" n="HIAT:w" s="T54">Surarlubəj</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_209" n="HIAT:w" s="T55">bugagən</ts>
                  <nts id="Seg_210" n="HIAT:ip">"</nts>
                  <nts id="Seg_211" n="HIAT:ip">.</nts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_214" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_216" n="HIAT:w" s="T56">Buga</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_219" n="HIAT:w" s="T57">dĭn</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_222" n="HIAT:w" s="T58">mĭnleʔbə</ts>
                  <nts id="Seg_223" n="HIAT:ip">.</nts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_226" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_228" n="HIAT:w" s="T59">A</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_231" n="HIAT:w" s="T60">dĭ</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_234" n="HIAT:w" s="T61">buga</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_237" n="HIAT:w" s="T62">măndə:</ts>
                  <nts id="Seg_238" n="HIAT:ip">"</nts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_241" n="HIAT:w" s="T63">Măn</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_244" n="HIAT:w" s="T64">togonorbiam</ts>
                  <nts id="Seg_245" n="HIAT:ip">,</nts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_248" n="HIAT:w" s="T65">tarerbiam</ts>
                  <nts id="Seg_249" n="HIAT:ip">.</nts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_252" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_254" n="HIAT:w" s="T66">A</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_257" n="HIAT:w" s="T67">tuj</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_260" n="HIAT:w" s="T68">ej</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_263" n="HIAT:w" s="T69">moliam</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_266" n="HIAT:w" s="T70">togonorzittə</ts>
                  <nts id="Seg_267" n="HIAT:ip">.</nts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_270" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_272" n="HIAT:w" s="T71">Măna</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_275" n="HIAT:w" s="T72">sürerlüʔpiʔi</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_278" n="HIAT:w" s="T73">amzittə</ts>
                  <nts id="Seg_279" n="HIAT:ip">.</nts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_282" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_284" n="HIAT:w" s="T74">Štobɨ</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_287" n="HIAT:w" s="T75">măna</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_290" n="HIAT:w" s="T76">ambiʔi</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_293" n="HIAT:w" s="T77">urgaːbaʔi</ts>
                  <nts id="Seg_294" n="HIAT:ip">,</nts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_297" n="HIAT:w" s="T78">menzeŋ</ts>
                  <nts id="Seg_298" n="HIAT:ip">.</nts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_301" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_303" n="HIAT:w" s="T79">Il</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_306" n="HIAT:w" s="T80">bar</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_309" n="HIAT:w" s="T81">kăda</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_312" n="HIAT:w" s="T82">aliaʔi</ts>
                  <nts id="Seg_313" n="HIAT:ip">"</nts>
                  <nts id="Seg_314" n="HIAT:ip">.</nts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_317" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_319" n="HIAT:w" s="T83">Dĭgəttə:</ts>
                  <nts id="Seg_320" n="HIAT:ip">"</nts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_323" n="HIAT:w" s="T84">Kanžəbəj</ts>
                  <nts id="Seg_324" n="HIAT:ip">,</nts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_327" n="HIAT:w" s="T85">inegən</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_329" n="HIAT:ip">(</nts>
                  <ts e="T87" id="Seg_331" n="HIAT:w" s="T86">surarluʔpi</ts>
                  <nts id="Seg_332" n="HIAT:ip">)</nts>
                  <nts id="Seg_333" n="HIAT:ip">"</nts>
                  <nts id="Seg_334" n="HIAT:ip">.</nts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_337" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_339" n="HIAT:w" s="T87">Dĭgəttə</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_342" n="HIAT:w" s="T88">šonəgaʔi</ts>
                  <nts id="Seg_343" n="HIAT:ip">.</nts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_346" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_348" n="HIAT:w" s="T89">Dĭgəttə</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_351" n="HIAT:w" s="T90">penzə</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_354" n="HIAT:w" s="T91">măndlaʔbə:</ts>
                  <nts id="Seg_355" n="HIAT:ip">"</nts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_358" n="HIAT:w" s="T92">Kanžəbəj</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_361" n="HIAT:w" s="T93">inenə</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_364" n="HIAT:w" s="T94">surarlubəj</ts>
                  <nts id="Seg_365" n="HIAT:ip">"</nts>
                  <nts id="Seg_366" n="HIAT:ip">.</nts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_369" n="HIAT:u" s="T95">
                  <ts e="T96" id="Seg_371" n="HIAT:w" s="T95">Dĭgəttə</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_374" n="HIAT:w" s="T96">dĭzeŋ</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_377" n="HIAT:w" s="T97">šolaʔbəʔjə</ts>
                  <nts id="Seg_378" n="HIAT:ip">.</nts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_381" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_383" n="HIAT:w" s="T98">Ine</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_386" n="HIAT:w" s="T99">iʔbolaʔbə</ts>
                  <nts id="Seg_387" n="HIAT:ip">.</nts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_390" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_392" n="HIAT:w" s="T100">Sʼimat</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_395" n="HIAT:w" s="T101">naga</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_398" n="HIAT:w" s="T102">bar</ts>
                  <nts id="Seg_399" n="HIAT:ip">.</nts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_402" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_404" n="HIAT:w" s="T103">Dĭn</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_407" n="HIAT:w" s="T104">ĭzemneʔbə</ts>
                  <nts id="Seg_408" n="HIAT:ip">.</nts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_411" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_413" n="HIAT:w" s="T105">Dĭ</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_416" n="HIAT:w" s="T106">măndə:</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_418" n="HIAT:ip">"</nts>
                  <ts e="T108" id="Seg_420" n="HIAT:w" s="T107">Togonorbiam</ts>
                  <nts id="Seg_421" n="HIAT:ip">,</nts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_424" n="HIAT:w" s="T108">togonorbiam</ts>
                  <nts id="Seg_425" n="HIAT:ip">,</nts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_428" n="HIAT:w" s="T109">a</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_431" n="HIAT:w" s="T110">kuiol:</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_434" n="HIAT:w" s="T111">tüj</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_437" n="HIAT:w" s="T112">măna</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_440" n="HIAT:w" s="T113">baruʔluʔpi</ts>
                  <nts id="Seg_441" n="HIAT:ip">.</nts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_444" n="HIAT:u" s="T114">
                  <ts e="T115" id="Seg_446" n="HIAT:w" s="T114">Girgit</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_449" n="HIAT:w" s="T115">il</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_452" n="HIAT:w" s="T116">ĭmbidə</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_455" n="HIAT:w" s="T117">ej</ts>
                  <nts id="Seg_456" n="HIAT:ip">,</nts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_459" n="HIAT:w" s="T118">jakšə</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_462" n="HIAT:w" s="T119">ej</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_465" n="HIAT:w" s="T120">mĭlieʔi</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_468" n="HIAT:w" s="T121">măna</ts>
                  <nts id="Seg_469" n="HIAT:ip">"</nts>
                  <nts id="Seg_470" n="HIAT:ip">.</nts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_473" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_475" n="HIAT:w" s="T122">Dĭgəttə:</ts>
                  <nts id="Seg_476" n="HIAT:ip">"</nts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_479" n="HIAT:w" s="T123">Kuiol</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_482" n="HIAT:w" s="T124">kăda</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_485" n="HIAT:w" s="T125">mănlia</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_488" n="HIAT:w" s="T126">ine</ts>
                  <nts id="Seg_489" n="HIAT:ip">"</nts>
                  <nts id="Seg_490" n="HIAT:ip">.</nts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T135" id="Seg_493" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_495" n="HIAT:w" s="T127">Dĭgəttə</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_498" n="HIAT:w" s="T128">kambiʔi:</ts>
                  <nts id="Seg_499" n="HIAT:ip">"</nts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_502" n="HIAT:w" s="T129">No</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_505" n="HIAT:w" s="T130">măn</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_508" n="HIAT:w" s="T131">tănan</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_511" n="HIAT:w" s="T132">ĭmbidə</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_514" n="HIAT:w" s="T133">ej</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_517" n="HIAT:w" s="T134">alam</ts>
                  <nts id="Seg_518" n="HIAT:ip">.</nts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_521" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_523" n="HIAT:w" s="T135">Kanžəbəj</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_525" n="HIAT:ip">(</nts>
                  <ts e="T137" id="Seg_527" n="HIAT:w" s="T136">pi-</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_530" n="HIAT:w" s="T137">pi-</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_533" n="HIAT:w" s="T138">pi-</ts>
                  <nts id="Seg_534" n="HIAT:ip">)</nts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_537" n="HIAT:w" s="T139">piʔinə</ts>
                  <nts id="Seg_538" n="HIAT:ip">"</nts>
                  <nts id="Seg_539" n="HIAT:ip">.</nts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T141" id="Seg_542" n="HIAT:u" s="T140">
                  <ts e="T141" id="Seg_544" n="HIAT:w" s="T140">Šobiʔi</ts>
                  <nts id="Seg_545" n="HIAT:ip">.</nts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T147" id="Seg_548" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_550" n="HIAT:w" s="T141">Dĭgəttə</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_553" n="HIAT:w" s="T142">dĭ</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_555" n="HIAT:ip">(</nts>
                  <ts e="T144" id="Seg_557" n="HIAT:w" s="T143">pi-</ts>
                  <nts id="Seg_558" n="HIAT:ip">)</nts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_561" n="HIAT:w" s="T144">penzə</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_564" n="HIAT:w" s="T145">kambi</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_567" n="HIAT:w" s="T146">noranə</ts>
                  <nts id="Seg_568" n="HIAT:ip">.</nts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T154" id="Seg_571" n="HIAT:u" s="T147">
                  <ts e="T148" id="Seg_573" n="HIAT:w" s="T147">Dĭ</ts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_576" n="HIAT:w" s="T148">nuga</ts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_579" n="HIAT:w" s="T149">da:</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_581" n="HIAT:ip">"</nts>
                  <ts e="T151" id="Seg_583" n="HIAT:w" s="T150">Adnakă</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_585" n="HIAT:ip">(</nts>
                  <ts e="T152" id="Seg_587" n="HIAT:w" s="T151">suplo-</ts>
                  <nts id="Seg_588" n="HIAT:ip">)</nts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_591" n="HIAT:w" s="T152">supsoluʔjə</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_594" n="HIAT:w" s="T153">iʔgö</ts>
                  <nts id="Seg_595" n="HIAT:ip">.</nts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T158" id="Seg_598" n="HIAT:u" s="T154">
                  <ts e="T155" id="Seg_600" n="HIAT:w" s="T154">Da</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_603" n="HIAT:w" s="T155">măna</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_606" n="HIAT:w" s="T156">dĭn</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_609" n="HIAT:w" s="T157">amnoluʔluʔjə</ts>
                  <nts id="Seg_610" n="HIAT:ip">"</nts>
                  <nts id="Seg_611" n="HIAT:ip">.</nts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T165" id="Seg_614" n="HIAT:u" s="T158">
                  <ts e="T159" id="Seg_616" n="HIAT:w" s="T158">Dĭgəttə</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_619" n="HIAT:w" s="T159">dĭ</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_622" n="HIAT:w" s="T160">penzə</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_625" n="HIAT:w" s="T161">deʔpi</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_628" n="HIAT:w" s="T162">diʔnə</ts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_631" n="HIAT:w" s="T163">onʼiʔ</ts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_634" n="HIAT:w" s="T164">aktʼi</ts>
                  <nts id="Seg_635" n="HIAT:ip">.</nts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T169" id="Seg_638" n="HIAT:u" s="T165">
                  <ts e="T166" id="Seg_640" n="HIAT:w" s="T165">Mĭbi</ts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_643" n="HIAT:w" s="T166">kažnej</ts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_646" n="HIAT:w" s="T167">dʼala</ts>
                  <nts id="Seg_647" n="HIAT:ip">,</nts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_650" n="HIAT:w" s="T168">mĭmbi</ts>
                  <nts id="Seg_651" n="HIAT:ip">.</nts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T172" id="Seg_654" n="HIAT:u" s="T169">
                  <ts e="T170" id="Seg_656" n="HIAT:w" s="T169">Iʔgö</ts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_659" n="HIAT:w" s="T170">deʔpi</ts>
                  <nts id="Seg_660" n="HIAT:ip">,</nts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_663" n="HIAT:w" s="T171">deʔpi</ts>
                  <nts id="Seg_664" n="HIAT:ip">.</nts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T178" id="Seg_667" n="HIAT:u" s="T172">
                  <ts e="T173" id="Seg_669" n="HIAT:w" s="T172">Dĭgəttə</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_672" n="HIAT:w" s="T173">măndə:</ts>
                  <nts id="Seg_673" n="HIAT:ip">"</nts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_676" n="HIAT:w" s="T174">Măn</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_679" n="HIAT:w" s="T175">kalam</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_682" n="HIAT:w" s="T176">ĭmbi-nʼibudʼ</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_685" n="HIAT:w" s="T177">sadarzittə</ts>
                  <nts id="Seg_686" n="HIAT:ip">.</nts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_689" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_691" n="HIAT:w" s="T178">A</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_694" n="HIAT:w" s="T179">vot</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_697" n="HIAT:w" s="T180">măn</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_700" n="HIAT:w" s="T181">nʼim</ts>
                  <nts id="Seg_701" n="HIAT:ip">,</nts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_704" n="HIAT:w" s="T182">pušaj</ts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_707" n="HIAT:w" s="T183">mĭlleʔbə</ts>
                  <nts id="Seg_708" n="HIAT:ip">.</nts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T187" id="Seg_711" n="HIAT:u" s="T184">
                  <ts e="T185" id="Seg_713" n="HIAT:w" s="T184">Dĭ</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_716" n="HIAT:w" s="T185">nʼit</ts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_719" n="HIAT:w" s="T186">šobi</ts>
                  <nts id="Seg_720" n="HIAT:ip">.</nts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T191" id="Seg_723" n="HIAT:u" s="T187">
                  <ts e="T188" id="Seg_725" n="HIAT:w" s="T187">Kumən</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_728" n="HIAT:w" s="T188">dʼala</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_731" n="HIAT:w" s="T189">mĭmbi</ts>
                  <nts id="Seg_732" n="HIAT:ip">,</nts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_735" n="HIAT:w" s="T190">mĭmbi</ts>
                  <nts id="Seg_736" n="HIAT:ip">.</nts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T196" id="Seg_739" n="HIAT:u" s="T191">
                  <ts e="T192" id="Seg_741" n="HIAT:w" s="T191">Dĭgəttə:</ts>
                  <nts id="Seg_742" n="HIAT:ip">"</nts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_745" n="HIAT:w" s="T192">Davaj</ts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_748" n="HIAT:w" s="T193">dĭ</ts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_751" n="HIAT:w" s="T194">pĭnzəm</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_754" n="HIAT:w" s="T195">kutlim</ts>
                  <nts id="Seg_755" n="HIAT:ip">.</nts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T207" id="Seg_758" n="HIAT:u" s="T196">
                  <ts e="T197" id="Seg_760" n="HIAT:w" s="T196">A</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_762" n="HIAT:ip">(</nts>
                  <ts e="T198" id="Seg_764" n="HIAT:w" s="T197">dĭ-</ts>
                  <nts id="Seg_765" n="HIAT:ip">)</nts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_768" n="HIAT:w" s="T198">dĭn</ts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_771" n="HIAT:w" s="T199">aktʼa</ts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_774" n="HIAT:w" s="T200">iʔgö</ts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_777" n="HIAT:w" s="T201">izittə</ts>
                  <nts id="Seg_778" n="HIAT:ip">,</nts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_781" n="HIAT:w" s="T202">ĭmbi</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_784" n="HIAT:w" s="T203">kažnɨj</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_787" n="HIAT:w" s="T204">dʼala</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_790" n="HIAT:w" s="T205">kanzittə</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_793" n="HIAT:w" s="T206">döbər</ts>
                  <nts id="Seg_794" n="HIAT:ip">"</nts>
                  <nts id="Seg_795" n="HIAT:ip">.</nts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T213" id="Seg_798" n="HIAT:u" s="T207">
                  <ts e="T208" id="Seg_800" n="HIAT:w" s="T207">Dĭ</ts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_803" n="HIAT:w" s="T208">dĭm</ts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_806" n="HIAT:w" s="T209">toʔnaːrbi</ts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_809" n="HIAT:w" s="T210">da</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_812" n="HIAT:w" s="T211">xvostə</ts>
                  <nts id="Seg_813" n="HIAT:ip">…</nts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T218" id="Seg_816" n="HIAT:u" s="T213">
                  <ts e="T214" id="Seg_818" n="HIAT:w" s="T213">Dĭ</ts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_821" n="HIAT:w" s="T214">dĭm</ts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_824" n="HIAT:w" s="T215">kutlaʔpi</ts>
                  <nts id="Seg_825" n="HIAT:ip">,</nts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_828" n="HIAT:w" s="T216">dĭ</ts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_831" n="HIAT:w" s="T217">penzə</ts>
                  <nts id="Seg_832" n="HIAT:ip">.</nts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T224" id="Seg_835" n="HIAT:u" s="T218">
                  <ts e="T219" id="Seg_837" n="HIAT:w" s="T218">Dĭgəttə</ts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_840" n="HIAT:w" s="T219">dĭ</ts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_843" n="HIAT:w" s="T220">šobi:</ts>
                  <nts id="Seg_844" n="HIAT:ip">"</nts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_847" n="HIAT:w" s="T221">Gijen</ts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_850" n="HIAT:w" s="T222">măn</ts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_853" n="HIAT:w" s="T223">nʼim</ts>
                  <nts id="Seg_854" n="HIAT:ip">?</nts>
                  <nts id="Seg_855" n="HIAT:ip">"</nts>
                  <nts id="Seg_856" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T230" id="Seg_858" n="HIAT:u" s="T224">
                  <ts e="T225" id="Seg_860" n="HIAT:w" s="T224">A</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_863" n="HIAT:w" s="T225">dĭn</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_866" n="HIAT:w" s="T226">dĭzeŋ</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_869" n="HIAT:w" s="T227">măndə:</ts>
                  <nts id="Seg_870" n="HIAT:ip">"</nts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_873" n="HIAT:w" s="T228">Nʼil</ts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_876" n="HIAT:w" s="T229">naga</ts>
                  <nts id="Seg_877" n="HIAT:ip">.</nts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T234" id="Seg_880" n="HIAT:u" s="T230">
                  <ts e="T231" id="Seg_882" n="HIAT:w" s="T230">Kambi</ts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_884" n="HIAT:ip">(</nts>
                  <ts e="T232" id="Seg_886" n="HIAT:w" s="T231">dü-</ts>
                  <nts id="Seg_887" n="HIAT:ip">)</nts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_890" n="HIAT:w" s="T232">dibər</ts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_893" n="HIAT:w" s="T233">piʔinə</ts>
                  <nts id="Seg_894" n="HIAT:ip">.</nts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T236" id="Seg_897" n="HIAT:u" s="T234">
                  <ts e="T235" id="Seg_899" n="HIAT:w" s="T234">Nʼit</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_902" n="HIAT:w" s="T235">iʔbolaʔbə</ts>
                  <nts id="Seg_903" n="HIAT:ip">.</nts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T248" id="Seg_906" n="HIAT:u" s="T236">
                  <ts e="T237" id="Seg_908" n="HIAT:w" s="T236">A</ts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_911" n="HIAT:w" s="T237">dĭ</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_914" n="HIAT:w" s="T238">penzə</ts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_917" n="HIAT:w" s="T239">šobi:</ts>
                  <nts id="Seg_918" n="HIAT:ip">"</nts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_921" n="HIAT:w" s="T240">Măn</ts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_924" n="HIAT:w" s="T241">xvostə</ts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_927" n="HIAT:w" s="T242">naga</ts>
                  <nts id="Seg_928" n="HIAT:ip">,</nts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_931" n="HIAT:w" s="T243">a</ts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_933" n="HIAT:ip">(</nts>
                  <ts e="T245" id="Seg_935" n="HIAT:w" s="T244">s-</ts>
                  <nts id="Seg_936" n="HIAT:ip">)</nts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_939" n="HIAT:w" s="T245">tăn</ts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_942" n="HIAT:w" s="T246">nʼil</ts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_945" n="HIAT:w" s="T247">naga</ts>
                  <nts id="Seg_946" n="HIAT:ip">.</nts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T253" id="Seg_949" n="HIAT:u" s="T248">
                  <ts e="T249" id="Seg_951" n="HIAT:w" s="T248">Nu</ts>
                  <nts id="Seg_952" n="HIAT:ip">,</nts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_955" n="HIAT:w" s="T249">mĭʔ</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_958" n="HIAT:w" s="T250">măna</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_961" n="HIAT:w" s="T251">kažnej</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_964" n="HIAT:w" s="T252">dʼala</ts>
                  <nts id="Seg_965" n="HIAT:ip">.</nts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T259" id="Seg_968" n="HIAT:u" s="T253">
                  <nts id="Seg_969" n="HIAT:ip">"</nts>
                  <ts e="T254" id="Seg_971" n="HIAT:w" s="T253">Dʼok</ts>
                  <nts id="Seg_972" n="HIAT:ip">,</nts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_974" n="HIAT:ip">(</nts>
                  <ts e="T255" id="Seg_976" n="HIAT:w" s="T254">em-</ts>
                  <nts id="Seg_977" n="HIAT:ip">)</nts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_980" n="HIAT:w" s="T255">ej</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_983" n="HIAT:w" s="T256">mĭlem</ts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_986" n="HIAT:w" s="T257">tüj</ts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_989" n="HIAT:w" s="T258">tănan</ts>
                  <nts id="Seg_990" n="HIAT:ip">"</nts>
                  <nts id="Seg_991" n="HIAT:ip">.</nts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T260" id="Seg_994" n="HIAT:u" s="T259">
                  <ts e="T260" id="Seg_996" n="HIAT:w" s="T259">Kabarləj</ts>
                  <nts id="Seg_997" n="HIAT:ip">.</nts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T260" id="Seg_999" n="sc" s="T0">
               <ts e="T1" id="Seg_1001" n="e" s="T0">Kuza </ts>
               <ts e="T2" id="Seg_1003" n="e" s="T1">šonəga </ts>
               <ts e="T3" id="Seg_1005" n="e" s="T2">stʼepʼju. </ts>
               <ts e="T4" id="Seg_1007" n="e" s="T3">Kuliat </ts>
               <ts e="T5" id="Seg_1009" n="e" s="T4">bar: </ts>
               <ts e="T6" id="Seg_1011" n="e" s="T5">šü </ts>
               <ts e="T7" id="Seg_1013" n="e" s="T6">iʔbolaʔbə. </ts>
               <ts e="T8" id="Seg_1015" n="e" s="T7">A </ts>
               <ts e="T9" id="Seg_1017" n="e" s="T8">šügən </ts>
               <ts e="T10" id="Seg_1019" n="e" s="T9">penzə </ts>
               <ts e="T11" id="Seg_1021" n="e" s="T10">iʔbolaʔbə. </ts>
               <ts e="T12" id="Seg_1023" n="e" s="T11">Dĭ </ts>
               <ts e="T13" id="Seg_1025" n="e" s="T12">pa </ts>
               <ts e="T14" id="Seg_1027" n="e" s="T13">ibi, </ts>
               <ts e="T15" id="Seg_1029" n="e" s="T14">dĭm </ts>
               <ts e="T16" id="Seg_1031" n="e" s="T15">baʔluʔpi. </ts>
               <ts e="T17" id="Seg_1033" n="e" s="T16">A </ts>
               <ts e="T18" id="Seg_1035" n="e" s="T17">dĭ </ts>
               <ts e="T19" id="Seg_1037" n="e" s="T18">dĭʔnə </ts>
               <ts e="T20" id="Seg_1039" n="e" s="T19">bar </ts>
               <ts e="T21" id="Seg_1041" n="e" s="T20">šejanə. </ts>
               <ts e="T22" id="Seg_1043" n="e" s="T21">Dĭgəttə </ts>
               <ts e="T23" id="Seg_1045" n="e" s="T22">bar </ts>
               <ts e="T24" id="Seg_1047" n="e" s="T23">dĭm </ts>
               <ts e="T25" id="Seg_1049" n="e" s="T24">pʼaŋdlaʔbə, </ts>
               <ts e="T26" id="Seg_1051" n="e" s="T25">pʼaŋdlaʔbə. </ts>
               <ts e="T27" id="Seg_1053" n="e" s="T26">"Ĭmbi </ts>
               <ts e="T28" id="Seg_1055" n="e" s="T27">tăn </ts>
               <ts e="T29" id="Seg_1057" n="e" s="T28">măna </ts>
               <ts e="T30" id="Seg_1059" n="e" s="T29">pʼaŋdlaʔbəl? </ts>
               <ts e="T31" id="Seg_1061" n="e" s="T30">Măn </ts>
               <ts e="T32" id="Seg_1063" n="e" s="T31">(tăn=) </ts>
               <ts e="T33" id="Seg_1065" n="e" s="T32">tănan </ts>
               <ts e="T34" id="Seg_1067" n="e" s="T33">jakšə </ts>
               <ts e="T35" id="Seg_1069" n="e" s="T34">abiam, </ts>
               <ts e="T36" id="Seg_1071" n="e" s="T35">a </ts>
               <ts e="T37" id="Seg_1073" n="e" s="T36">tăn </ts>
               <ts e="T39" id="Seg_1075" n="e" s="T37">măna…" </ts>
               <ts e="T40" id="Seg_1077" n="e" s="T39">A </ts>
               <ts e="T41" id="Seg_1079" n="e" s="T40">dĭ </ts>
               <ts e="T42" id="Seg_1081" n="e" s="T41">(măna-) </ts>
               <ts e="T43" id="Seg_1083" n="e" s="T42">măndə:" </ts>
               <ts e="T44" id="Seg_1085" n="e" s="T43">Tăn </ts>
               <ts e="T45" id="Seg_1087" n="e" s="T44">jakšə </ts>
               <ts e="T46" id="Seg_1089" n="e" s="T45">(m-) </ts>
               <ts e="T47" id="Seg_1091" n="e" s="T46">abial, </ts>
               <ts e="T48" id="Seg_1093" n="e" s="T47">a </ts>
               <ts e="T49" id="Seg_1095" n="e" s="T48">măn </ts>
               <ts e="T50" id="Seg_1097" n="e" s="T49">tănan </ts>
               <ts e="T51" id="Seg_1099" n="e" s="T50">ej </ts>
               <ts e="T52" id="Seg_1101" n="e" s="T51">jakše </ts>
               <ts e="T53" id="Seg_1103" n="e" s="T52">abiam. </ts>
               <ts e="T54" id="Seg_1105" n="e" s="T53">Kanžəbəj. </ts>
               <ts e="T55" id="Seg_1107" n="e" s="T54">Surarlubəj </ts>
               <ts e="T56" id="Seg_1109" n="e" s="T55">bugagən". </ts>
               <ts e="T57" id="Seg_1111" n="e" s="T56">Buga </ts>
               <ts e="T58" id="Seg_1113" n="e" s="T57">dĭn </ts>
               <ts e="T59" id="Seg_1115" n="e" s="T58">mĭnleʔbə. </ts>
               <ts e="T60" id="Seg_1117" n="e" s="T59">A </ts>
               <ts e="T61" id="Seg_1119" n="e" s="T60">dĭ </ts>
               <ts e="T62" id="Seg_1121" n="e" s="T61">buga </ts>
               <ts e="T63" id="Seg_1123" n="e" s="T62">măndə:" </ts>
               <ts e="T64" id="Seg_1125" n="e" s="T63">Măn </ts>
               <ts e="T65" id="Seg_1127" n="e" s="T64">togonorbiam, </ts>
               <ts e="T66" id="Seg_1129" n="e" s="T65">tarerbiam. </ts>
               <ts e="T67" id="Seg_1131" n="e" s="T66">A </ts>
               <ts e="T68" id="Seg_1133" n="e" s="T67">tuj </ts>
               <ts e="T69" id="Seg_1135" n="e" s="T68">ej </ts>
               <ts e="T70" id="Seg_1137" n="e" s="T69">moliam </ts>
               <ts e="T71" id="Seg_1139" n="e" s="T70">togonorzittə. </ts>
               <ts e="T72" id="Seg_1141" n="e" s="T71">Măna </ts>
               <ts e="T73" id="Seg_1143" n="e" s="T72">sürerlüʔpiʔi </ts>
               <ts e="T74" id="Seg_1145" n="e" s="T73">amzittə. </ts>
               <ts e="T75" id="Seg_1147" n="e" s="T74">Štobɨ </ts>
               <ts e="T76" id="Seg_1149" n="e" s="T75">măna </ts>
               <ts e="T77" id="Seg_1151" n="e" s="T76">ambiʔi </ts>
               <ts e="T78" id="Seg_1153" n="e" s="T77">urgaːbaʔi, </ts>
               <ts e="T79" id="Seg_1155" n="e" s="T78">menzeŋ. </ts>
               <ts e="T80" id="Seg_1157" n="e" s="T79">Il </ts>
               <ts e="T81" id="Seg_1159" n="e" s="T80">bar </ts>
               <ts e="T82" id="Seg_1161" n="e" s="T81">kăda </ts>
               <ts e="T83" id="Seg_1163" n="e" s="T82">aliaʔi". </ts>
               <ts e="T84" id="Seg_1165" n="e" s="T83">Dĭgəttə:" </ts>
               <ts e="T85" id="Seg_1167" n="e" s="T84">Kanžəbəj, </ts>
               <ts e="T86" id="Seg_1169" n="e" s="T85">inegən </ts>
               <ts e="T87" id="Seg_1171" n="e" s="T86">(surarluʔpi)". </ts>
               <ts e="T88" id="Seg_1173" n="e" s="T87">Dĭgəttə </ts>
               <ts e="T89" id="Seg_1175" n="e" s="T88">šonəgaʔi. </ts>
               <ts e="T90" id="Seg_1177" n="e" s="T89">Dĭgəttə </ts>
               <ts e="T91" id="Seg_1179" n="e" s="T90">penzə </ts>
               <ts e="T92" id="Seg_1181" n="e" s="T91">măndlaʔbə:" </ts>
               <ts e="T93" id="Seg_1183" n="e" s="T92">Kanžəbəj </ts>
               <ts e="T94" id="Seg_1185" n="e" s="T93">inenə </ts>
               <ts e="T95" id="Seg_1187" n="e" s="T94">surarlubəj". </ts>
               <ts e="T96" id="Seg_1189" n="e" s="T95">Dĭgəttə </ts>
               <ts e="T97" id="Seg_1191" n="e" s="T96">dĭzeŋ </ts>
               <ts e="T98" id="Seg_1193" n="e" s="T97">šolaʔbəʔjə. </ts>
               <ts e="T99" id="Seg_1195" n="e" s="T98">Ine </ts>
               <ts e="T100" id="Seg_1197" n="e" s="T99">iʔbolaʔbə. </ts>
               <ts e="T101" id="Seg_1199" n="e" s="T100">Sʼimat </ts>
               <ts e="T102" id="Seg_1201" n="e" s="T101">naga </ts>
               <ts e="T103" id="Seg_1203" n="e" s="T102">bar. </ts>
               <ts e="T104" id="Seg_1205" n="e" s="T103">Dĭn </ts>
               <ts e="T105" id="Seg_1207" n="e" s="T104">ĭzemneʔbə. </ts>
               <ts e="T106" id="Seg_1209" n="e" s="T105">Dĭ </ts>
               <ts e="T107" id="Seg_1211" n="e" s="T106">măndə: </ts>
               <ts e="T108" id="Seg_1213" n="e" s="T107">"Togonorbiam, </ts>
               <ts e="T109" id="Seg_1215" n="e" s="T108">togonorbiam, </ts>
               <ts e="T110" id="Seg_1217" n="e" s="T109">a </ts>
               <ts e="T111" id="Seg_1219" n="e" s="T110">kuiol: </ts>
               <ts e="T112" id="Seg_1221" n="e" s="T111">tüj </ts>
               <ts e="T113" id="Seg_1223" n="e" s="T112">măna </ts>
               <ts e="T114" id="Seg_1225" n="e" s="T113">baruʔluʔpi. </ts>
               <ts e="T115" id="Seg_1227" n="e" s="T114">Girgit </ts>
               <ts e="T116" id="Seg_1229" n="e" s="T115">il </ts>
               <ts e="T117" id="Seg_1231" n="e" s="T116">ĭmbidə </ts>
               <ts e="T118" id="Seg_1233" n="e" s="T117">ej, </ts>
               <ts e="T119" id="Seg_1235" n="e" s="T118">jakšə </ts>
               <ts e="T120" id="Seg_1237" n="e" s="T119">ej </ts>
               <ts e="T121" id="Seg_1239" n="e" s="T120">mĭlieʔi </ts>
               <ts e="T122" id="Seg_1241" n="e" s="T121">măna". </ts>
               <ts e="T123" id="Seg_1243" n="e" s="T122">Dĭgəttə:" </ts>
               <ts e="T124" id="Seg_1245" n="e" s="T123">Kuiol </ts>
               <ts e="T125" id="Seg_1247" n="e" s="T124">kăda </ts>
               <ts e="T126" id="Seg_1249" n="e" s="T125">mănlia </ts>
               <ts e="T127" id="Seg_1251" n="e" s="T126">ine". </ts>
               <ts e="T128" id="Seg_1253" n="e" s="T127">Dĭgəttə </ts>
               <ts e="T129" id="Seg_1255" n="e" s="T128">kambiʔi:" </ts>
               <ts e="T130" id="Seg_1257" n="e" s="T129">No </ts>
               <ts e="T131" id="Seg_1259" n="e" s="T130">măn </ts>
               <ts e="T132" id="Seg_1261" n="e" s="T131">tănan </ts>
               <ts e="T133" id="Seg_1263" n="e" s="T132">ĭmbidə </ts>
               <ts e="T134" id="Seg_1265" n="e" s="T133">ej </ts>
               <ts e="T135" id="Seg_1267" n="e" s="T134">alam. </ts>
               <ts e="T136" id="Seg_1269" n="e" s="T135">Kanžəbəj </ts>
               <ts e="T137" id="Seg_1271" n="e" s="T136">(pi- </ts>
               <ts e="T138" id="Seg_1273" n="e" s="T137">pi- </ts>
               <ts e="T139" id="Seg_1275" n="e" s="T138">pi-) </ts>
               <ts e="T140" id="Seg_1277" n="e" s="T139">piʔinə". </ts>
               <ts e="T141" id="Seg_1279" n="e" s="T140">Šobiʔi. </ts>
               <ts e="T142" id="Seg_1281" n="e" s="T141">Dĭgəttə </ts>
               <ts e="T143" id="Seg_1283" n="e" s="T142">dĭ </ts>
               <ts e="T144" id="Seg_1285" n="e" s="T143">(pi-) </ts>
               <ts e="T145" id="Seg_1287" n="e" s="T144">penzə </ts>
               <ts e="T146" id="Seg_1289" n="e" s="T145">kambi </ts>
               <ts e="T147" id="Seg_1291" n="e" s="T146">noranə. </ts>
               <ts e="T148" id="Seg_1293" n="e" s="T147">Dĭ </ts>
               <ts e="T149" id="Seg_1295" n="e" s="T148">nuga </ts>
               <ts e="T150" id="Seg_1297" n="e" s="T149">da: </ts>
               <ts e="T151" id="Seg_1299" n="e" s="T150">"Adnakă </ts>
               <ts e="T152" id="Seg_1301" n="e" s="T151">(suplo-) </ts>
               <ts e="T153" id="Seg_1303" n="e" s="T152">supsoluʔjə </ts>
               <ts e="T154" id="Seg_1305" n="e" s="T153">iʔgö. </ts>
               <ts e="T155" id="Seg_1307" n="e" s="T154">Da </ts>
               <ts e="T156" id="Seg_1309" n="e" s="T155">măna </ts>
               <ts e="T157" id="Seg_1311" n="e" s="T156">dĭn </ts>
               <ts e="T158" id="Seg_1313" n="e" s="T157">amnoluʔluʔjə". </ts>
               <ts e="T159" id="Seg_1315" n="e" s="T158">Dĭgəttə </ts>
               <ts e="T160" id="Seg_1317" n="e" s="T159">dĭ </ts>
               <ts e="T161" id="Seg_1319" n="e" s="T160">penzə </ts>
               <ts e="T162" id="Seg_1321" n="e" s="T161">deʔpi </ts>
               <ts e="T163" id="Seg_1323" n="e" s="T162">diʔnə </ts>
               <ts e="T164" id="Seg_1325" n="e" s="T163">onʼiʔ </ts>
               <ts e="T165" id="Seg_1327" n="e" s="T164">aktʼi. </ts>
               <ts e="T166" id="Seg_1329" n="e" s="T165">Mĭbi </ts>
               <ts e="T167" id="Seg_1331" n="e" s="T166">kažnej </ts>
               <ts e="T168" id="Seg_1333" n="e" s="T167">dʼala, </ts>
               <ts e="T169" id="Seg_1335" n="e" s="T168">mĭmbi. </ts>
               <ts e="T170" id="Seg_1337" n="e" s="T169">Iʔgö </ts>
               <ts e="T171" id="Seg_1339" n="e" s="T170">deʔpi, </ts>
               <ts e="T172" id="Seg_1341" n="e" s="T171">deʔpi. </ts>
               <ts e="T173" id="Seg_1343" n="e" s="T172">Dĭgəttə </ts>
               <ts e="T174" id="Seg_1345" n="e" s="T173">măndə:" </ts>
               <ts e="T175" id="Seg_1347" n="e" s="T174">Măn </ts>
               <ts e="T176" id="Seg_1349" n="e" s="T175">kalam </ts>
               <ts e="T177" id="Seg_1351" n="e" s="T176">ĭmbi-nʼibudʼ </ts>
               <ts e="T178" id="Seg_1353" n="e" s="T177">sadarzittə. </ts>
               <ts e="T179" id="Seg_1355" n="e" s="T178">A </ts>
               <ts e="T180" id="Seg_1357" n="e" s="T179">vot </ts>
               <ts e="T181" id="Seg_1359" n="e" s="T180">măn </ts>
               <ts e="T182" id="Seg_1361" n="e" s="T181">nʼim, </ts>
               <ts e="T183" id="Seg_1363" n="e" s="T182">pušaj </ts>
               <ts e="T184" id="Seg_1365" n="e" s="T183">mĭlleʔbə. </ts>
               <ts e="T185" id="Seg_1367" n="e" s="T184">Dĭ </ts>
               <ts e="T186" id="Seg_1369" n="e" s="T185">nʼit </ts>
               <ts e="T187" id="Seg_1371" n="e" s="T186">šobi. </ts>
               <ts e="T188" id="Seg_1373" n="e" s="T187">Kumən </ts>
               <ts e="T189" id="Seg_1375" n="e" s="T188">dʼala </ts>
               <ts e="T190" id="Seg_1377" n="e" s="T189">mĭmbi, </ts>
               <ts e="T191" id="Seg_1379" n="e" s="T190">mĭmbi. </ts>
               <ts e="T192" id="Seg_1381" n="e" s="T191">Dĭgəttə:" </ts>
               <ts e="T193" id="Seg_1383" n="e" s="T192">Davaj </ts>
               <ts e="T194" id="Seg_1385" n="e" s="T193">dĭ </ts>
               <ts e="T195" id="Seg_1387" n="e" s="T194">pĭnzəm </ts>
               <ts e="T196" id="Seg_1389" n="e" s="T195">kutlim. </ts>
               <ts e="T197" id="Seg_1391" n="e" s="T196">A </ts>
               <ts e="T198" id="Seg_1393" n="e" s="T197">(dĭ-) </ts>
               <ts e="T199" id="Seg_1395" n="e" s="T198">dĭn </ts>
               <ts e="T200" id="Seg_1397" n="e" s="T199">aktʼa </ts>
               <ts e="T201" id="Seg_1399" n="e" s="T200">iʔgö </ts>
               <ts e="T202" id="Seg_1401" n="e" s="T201">izittə, </ts>
               <ts e="T203" id="Seg_1403" n="e" s="T202">ĭmbi </ts>
               <ts e="T204" id="Seg_1405" n="e" s="T203">kažnɨj </ts>
               <ts e="T205" id="Seg_1407" n="e" s="T204">dʼala </ts>
               <ts e="T206" id="Seg_1409" n="e" s="T205">kanzittə </ts>
               <ts e="T207" id="Seg_1411" n="e" s="T206">döbər". </ts>
               <ts e="T208" id="Seg_1413" n="e" s="T207">Dĭ </ts>
               <ts e="T209" id="Seg_1415" n="e" s="T208">dĭm </ts>
               <ts e="T210" id="Seg_1417" n="e" s="T209">toʔnaːrbi </ts>
               <ts e="T211" id="Seg_1419" n="e" s="T210">da </ts>
               <ts e="T213" id="Seg_1421" n="e" s="T211">xvostə… </ts>
               <ts e="T214" id="Seg_1423" n="e" s="T213">Dĭ </ts>
               <ts e="T215" id="Seg_1425" n="e" s="T214">dĭm </ts>
               <ts e="T216" id="Seg_1427" n="e" s="T215">kutlaʔpi, </ts>
               <ts e="T217" id="Seg_1429" n="e" s="T216">dĭ </ts>
               <ts e="T218" id="Seg_1431" n="e" s="T217">penzə. </ts>
               <ts e="T219" id="Seg_1433" n="e" s="T218">Dĭgəttə </ts>
               <ts e="T220" id="Seg_1435" n="e" s="T219">dĭ </ts>
               <ts e="T221" id="Seg_1437" n="e" s="T220">šobi:" </ts>
               <ts e="T222" id="Seg_1439" n="e" s="T221">Gijen </ts>
               <ts e="T223" id="Seg_1441" n="e" s="T222">măn </ts>
               <ts e="T224" id="Seg_1443" n="e" s="T223">nʼim?" </ts>
               <ts e="T225" id="Seg_1445" n="e" s="T224">A </ts>
               <ts e="T226" id="Seg_1447" n="e" s="T225">dĭn </ts>
               <ts e="T227" id="Seg_1449" n="e" s="T226">dĭzeŋ </ts>
               <ts e="T228" id="Seg_1451" n="e" s="T227">măndə:" </ts>
               <ts e="T229" id="Seg_1453" n="e" s="T228">Nʼil </ts>
               <ts e="T230" id="Seg_1455" n="e" s="T229">naga. </ts>
               <ts e="T231" id="Seg_1457" n="e" s="T230">Kambi </ts>
               <ts e="T232" id="Seg_1459" n="e" s="T231">(dü-) </ts>
               <ts e="T233" id="Seg_1461" n="e" s="T232">dibər </ts>
               <ts e="T234" id="Seg_1463" n="e" s="T233">piʔinə. </ts>
               <ts e="T235" id="Seg_1465" n="e" s="T234">Nʼit </ts>
               <ts e="T236" id="Seg_1467" n="e" s="T235">iʔbolaʔbə. </ts>
               <ts e="T237" id="Seg_1469" n="e" s="T236">A </ts>
               <ts e="T238" id="Seg_1471" n="e" s="T237">dĭ </ts>
               <ts e="T239" id="Seg_1473" n="e" s="T238">penzə </ts>
               <ts e="T240" id="Seg_1475" n="e" s="T239">šobi:" </ts>
               <ts e="T241" id="Seg_1477" n="e" s="T240">Măn </ts>
               <ts e="T242" id="Seg_1479" n="e" s="T241">xvostə </ts>
               <ts e="T243" id="Seg_1481" n="e" s="T242">naga, </ts>
               <ts e="T244" id="Seg_1483" n="e" s="T243">a </ts>
               <ts e="T245" id="Seg_1485" n="e" s="T244">(s-) </ts>
               <ts e="T246" id="Seg_1487" n="e" s="T245">tăn </ts>
               <ts e="T247" id="Seg_1489" n="e" s="T246">nʼil </ts>
               <ts e="T248" id="Seg_1491" n="e" s="T247">naga. </ts>
               <ts e="T249" id="Seg_1493" n="e" s="T248">Nu, </ts>
               <ts e="T250" id="Seg_1495" n="e" s="T249">mĭʔ </ts>
               <ts e="T251" id="Seg_1497" n="e" s="T250">măna </ts>
               <ts e="T252" id="Seg_1499" n="e" s="T251">kažnej </ts>
               <ts e="T253" id="Seg_1501" n="e" s="T252">dʼala. </ts>
               <ts e="T254" id="Seg_1503" n="e" s="T253">"Dʼok, </ts>
               <ts e="T255" id="Seg_1505" n="e" s="T254">(em-) </ts>
               <ts e="T256" id="Seg_1507" n="e" s="T255">ej </ts>
               <ts e="T257" id="Seg_1509" n="e" s="T256">mĭlem </ts>
               <ts e="T258" id="Seg_1511" n="e" s="T257">tüj </ts>
               <ts e="T259" id="Seg_1513" n="e" s="T258">tănan". </ts>
               <ts e="T260" id="Seg_1515" n="e" s="T259">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_1516" s="T0">PKZ_196X_Snake_flk.001 (002)</ta>
            <ta e="T7" id="Seg_1517" s="T3">PKZ_196X_Snake_flk.002 (003)</ta>
            <ta e="T11" id="Seg_1518" s="T7">PKZ_196X_Snake_flk.003 (004)</ta>
            <ta e="T16" id="Seg_1519" s="T11">PKZ_196X_Snake_flk.004 (005)</ta>
            <ta e="T21" id="Seg_1520" s="T16">PKZ_196X_Snake_flk.005 (006)</ta>
            <ta e="T26" id="Seg_1521" s="T21">PKZ_196X_Snake_flk.006 (007)</ta>
            <ta e="T30" id="Seg_1522" s="T26">PKZ_196X_Snake_flk.007 (008)</ta>
            <ta e="T39" id="Seg_1523" s="T30">PKZ_196X_Snake_flk.008 (009)</ta>
            <ta e="T53" id="Seg_1524" s="T39">PKZ_196X_Snake_flk.009 (010)</ta>
            <ta e="T54" id="Seg_1525" s="T53">PKZ_196X_Snake_flk.010 (011)</ta>
            <ta e="T56" id="Seg_1526" s="T54">PKZ_196X_Snake_flk.011 (012)</ta>
            <ta e="T59" id="Seg_1527" s="T56">PKZ_196X_Snake_flk.012 (013)</ta>
            <ta e="T66" id="Seg_1528" s="T59">PKZ_196X_Snake_flk.013 (014)</ta>
            <ta e="T71" id="Seg_1529" s="T66">PKZ_196X_Snake_flk.014 (015)</ta>
            <ta e="T74" id="Seg_1530" s="T71">PKZ_196X_Snake_flk.015 (016)</ta>
            <ta e="T79" id="Seg_1531" s="T74">PKZ_196X_Snake_flk.016 (017)</ta>
            <ta e="T83" id="Seg_1532" s="T79">PKZ_196X_Snake_flk.017 (018)</ta>
            <ta e="T87" id="Seg_1533" s="T83">PKZ_196X_Snake_flk.018 (019)</ta>
            <ta e="T89" id="Seg_1534" s="T87">PKZ_196X_Snake_flk.019 (020)</ta>
            <ta e="T95" id="Seg_1535" s="T89">PKZ_196X_Snake_flk.020 (021)</ta>
            <ta e="T98" id="Seg_1536" s="T95">PKZ_196X_Snake_flk.021 (022)</ta>
            <ta e="T100" id="Seg_1537" s="T98">PKZ_196X_Snake_flk.022 (023)</ta>
            <ta e="T103" id="Seg_1538" s="T100">PKZ_196X_Snake_flk.023 (024)</ta>
            <ta e="T105" id="Seg_1539" s="T103">PKZ_196X_Snake_flk.024 (025)</ta>
            <ta e="T114" id="Seg_1540" s="T105">PKZ_196X_Snake_flk.025 (026) </ta>
            <ta e="T122" id="Seg_1541" s="T114">PKZ_196X_Snake_flk.026 (028)</ta>
            <ta e="T127" id="Seg_1542" s="T122">PKZ_196X_Snake_flk.027 (029)</ta>
            <ta e="T135" id="Seg_1543" s="T127">PKZ_196X_Snake_flk.028 (030)</ta>
            <ta e="T140" id="Seg_1544" s="T135">PKZ_196X_Snake_flk.029 (031)</ta>
            <ta e="T141" id="Seg_1545" s="T140">PKZ_196X_Snake_flk.030 (032)</ta>
            <ta e="T147" id="Seg_1546" s="T141">PKZ_196X_Snake_flk.031 (033)</ta>
            <ta e="T154" id="Seg_1547" s="T147">PKZ_196X_Snake_flk.032 (034)</ta>
            <ta e="T158" id="Seg_1548" s="T154">PKZ_196X_Snake_flk.033 (035)</ta>
            <ta e="T165" id="Seg_1549" s="T158">PKZ_196X_Snake_flk.034 (036)</ta>
            <ta e="T169" id="Seg_1550" s="T165">PKZ_196X_Snake_flk.035 (037)</ta>
            <ta e="T172" id="Seg_1551" s="T169">PKZ_196X_Snake_flk.036 (038)</ta>
            <ta e="T178" id="Seg_1552" s="T172">PKZ_196X_Snake_flk.037 (039)</ta>
            <ta e="T184" id="Seg_1553" s="T178">PKZ_196X_Snake_flk.038 (040)</ta>
            <ta e="T187" id="Seg_1554" s="T184">PKZ_196X_Snake_flk.039 (041)</ta>
            <ta e="T191" id="Seg_1555" s="T187">PKZ_196X_Snake_flk.040 (042)</ta>
            <ta e="T196" id="Seg_1556" s="T191">PKZ_196X_Snake_flk.041 (043)</ta>
            <ta e="T207" id="Seg_1557" s="T196">PKZ_196X_Snake_flk.042 (044)</ta>
            <ta e="T213" id="Seg_1558" s="T207">PKZ_196X_Snake_flk.043 (045)</ta>
            <ta e="T218" id="Seg_1559" s="T213">PKZ_196X_Snake_flk.044 (046)</ta>
            <ta e="T224" id="Seg_1560" s="T218">PKZ_196X_Snake_flk.045 (047)</ta>
            <ta e="T230" id="Seg_1561" s="T224">PKZ_196X_Snake_flk.046 (048)</ta>
            <ta e="T234" id="Seg_1562" s="T230">PKZ_196X_Snake_flk.047 (049)</ta>
            <ta e="T236" id="Seg_1563" s="T234">PKZ_196X_Snake_flk.048 (050)</ta>
            <ta e="T248" id="Seg_1564" s="T236">PKZ_196X_Snake_flk.049 (051)</ta>
            <ta e="T253" id="Seg_1565" s="T248">PKZ_196X_Snake_flk.050 (052)</ta>
            <ta e="T259" id="Seg_1566" s="T253">PKZ_196X_Snake_flk.051 (053)</ta>
            <ta e="T260" id="Seg_1567" s="T259">PKZ_196X_Snake_flk.052 (054)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_1568" s="T0">Kuza šonəga stʼepʼju. </ta>
            <ta e="T7" id="Seg_1569" s="T3">Kuliat bar: šü iʔbolaʔbə. </ta>
            <ta e="T11" id="Seg_1570" s="T7">A šügən penzə iʔbolaʔbə. </ta>
            <ta e="T16" id="Seg_1571" s="T11">Dĭ pa ibi, dĭm baʔluʔpi. </ta>
            <ta e="T21" id="Seg_1572" s="T16">A dĭ dĭʔnə bar šejanə. </ta>
            <ta e="T26" id="Seg_1573" s="T21">Dĭgəttə bar dĭm pʼaŋdlaʔbə, pʼaŋdlaʔbə. </ta>
            <ta e="T30" id="Seg_1574" s="T26">"Ĭmbi tăn măna pʼaŋdlaʔbəl? </ta>
            <ta e="T39" id="Seg_1575" s="T30">Măn (tăn=) tănan jakšə abiam, a tăn măna …" </ta>
            <ta e="T53" id="Seg_1576" s="T39">A dĭ (măna-) măndə:" Tăn jakšə (m-) abial, a măn tănan ej jakše abiam. </ta>
            <ta e="T54" id="Seg_1577" s="T53">Kanžəbəj. </ta>
            <ta e="T56" id="Seg_1578" s="T54">Surarlubəj bugagən". </ta>
            <ta e="T59" id="Seg_1579" s="T56">Buga dĭn mĭnleʔbə. </ta>
            <ta e="T66" id="Seg_1580" s="T59">A dĭ buga măndə:" Măn togonorbiam, tarerbiam. </ta>
            <ta e="T71" id="Seg_1581" s="T66">A tuj ej moliam togonorzittə. </ta>
            <ta e="T74" id="Seg_1582" s="T71">Măna sürerlüʔpiʔi amzittə. </ta>
            <ta e="T79" id="Seg_1583" s="T74">Štobɨ măna ambiʔi urgaːbaʔi, menzeŋ. </ta>
            <ta e="T83" id="Seg_1584" s="T79">Il bar kăda aliaʔi". </ta>
            <ta e="T87" id="Seg_1585" s="T83">Dĭgəttə:" Kanžəbəj, inegən (surarluʔpi)". </ta>
            <ta e="T89" id="Seg_1586" s="T87">Dĭgəttə šonəgaʔi. </ta>
            <ta e="T95" id="Seg_1587" s="T89">Dĭgəttə penzə măndlaʔbə:" Kanžəbəj inenə surarlubəj". </ta>
            <ta e="T98" id="Seg_1588" s="T95">Dĭgəttə dĭzeŋ šolaʔbəʔjə. </ta>
            <ta e="T100" id="Seg_1589" s="T98">Ine iʔbolaʔbə. </ta>
            <ta e="T103" id="Seg_1590" s="T100">Sʼimat naga bar. </ta>
            <ta e="T105" id="Seg_1591" s="T103">Dĭn ĭzemneʔbə. </ta>
            <ta e="T114" id="Seg_1592" s="T105">Dĭ măndə: "Togonorbiam, togonorbiam, a kuiol: tüj măna baruʔluʔpi. </ta>
            <ta e="T122" id="Seg_1593" s="T114">Girgit il ĭmbidə ej, jakšə ej mĭlieʔi măna". </ta>
            <ta e="T127" id="Seg_1594" s="T122">Dĭgəttə:" Kuiol kăda mănlia ine". </ta>
            <ta e="T135" id="Seg_1595" s="T127">Dĭgəttə kambiʔi:" No măn tănan ĭmbidə ej alam. </ta>
            <ta e="T140" id="Seg_1596" s="T135">Kanžəbəj (pi- pi- pi-) piʔinə". </ta>
            <ta e="T141" id="Seg_1597" s="T140">Šobiʔi. </ta>
            <ta e="T147" id="Seg_1598" s="T141">Dĭgəttə dĭ (pi-) penzə kambi noranə. </ta>
            <ta e="T154" id="Seg_1599" s="T147">Dĭ nuga da: "Adnakă (suplo-) supsoluʔjə iʔgö. </ta>
            <ta e="T158" id="Seg_1600" s="T154">Da măna dĭn amnoluʔluʔjə". </ta>
            <ta e="T165" id="Seg_1601" s="T158">Dĭgəttə dĭ penzə deʔpi diʔnə onʼiʔ aktʼi. </ta>
            <ta e="T169" id="Seg_1602" s="T165">Mĭbi kažnej dʼala, mĭmbi. </ta>
            <ta e="T172" id="Seg_1603" s="T169">Iʔgö deʔpi, deʔpi. </ta>
            <ta e="T178" id="Seg_1604" s="T172">Dĭgəttə măndə:" Măn kalam ĭmbi-nʼibudʼ sadarzittə. </ta>
            <ta e="T184" id="Seg_1605" s="T178">A vot măn nʼim, pušaj mĭlleʔbə. </ta>
            <ta e="T187" id="Seg_1606" s="T184">Dĭ nʼit šobi. </ta>
            <ta e="T191" id="Seg_1607" s="T187">Kumən dʼala mĭmbi, mĭmbi. </ta>
            <ta e="T196" id="Seg_1608" s="T191">Dĭgəttə:" Davaj dĭ pĭnzəm kutlim. </ta>
            <ta e="T207" id="Seg_1609" s="T196">A (dĭ-) dĭn aktʼa iʔgö izittə, ĭmbi kažnɨj dʼala kanzittə döbər". </ta>
            <ta e="T213" id="Seg_1610" s="T207">Dĭ dĭm toʔnaːrbi da xvostə … </ta>
            <ta e="T218" id="Seg_1611" s="T213">Dĭ dĭm kutlaʔpi, dĭ penzə. </ta>
            <ta e="T224" id="Seg_1612" s="T218">Dĭgəttə dĭ šobi:" Gijen măn nʼim?" </ta>
            <ta e="T230" id="Seg_1613" s="T224">A dĭn dĭzeŋ măndə:" Nʼil naga. </ta>
            <ta e="T234" id="Seg_1614" s="T230">Kambi (dü-) dibər piʔinə. </ta>
            <ta e="T236" id="Seg_1615" s="T234">Nʼit iʔbolaʔbə. </ta>
            <ta e="T248" id="Seg_1616" s="T236">A dĭ penzə šobi:" Măn xvostə naga, a (s-) tăn nʼil naga. </ta>
            <ta e="T253" id="Seg_1617" s="T248">Nu, mĭʔ măna kažnej dʼala. </ta>
            <ta e="T259" id="Seg_1618" s="T253">"Dʼok, (em-) ej mĭlem tüj tănan". </ta>
            <ta e="T260" id="Seg_1619" s="T259">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1620" s="T0">kuza</ta>
            <ta e="T2" id="Seg_1621" s="T1">šonə-ga</ta>
            <ta e="T4" id="Seg_1622" s="T3">ku-lia-t</ta>
            <ta e="T5" id="Seg_1623" s="T4">bar</ta>
            <ta e="T6" id="Seg_1624" s="T5">šü</ta>
            <ta e="T7" id="Seg_1625" s="T6">iʔbo-laʔbə</ta>
            <ta e="T8" id="Seg_1626" s="T7">a</ta>
            <ta e="T9" id="Seg_1627" s="T8">šü-gən</ta>
            <ta e="T10" id="Seg_1628" s="T9">penzə</ta>
            <ta e="T11" id="Seg_1629" s="T10">iʔbo-laʔbə</ta>
            <ta e="T12" id="Seg_1630" s="T11">dĭ</ta>
            <ta e="T13" id="Seg_1631" s="T12">pa</ta>
            <ta e="T14" id="Seg_1632" s="T13">i-bi</ta>
            <ta e="T15" id="Seg_1633" s="T14">dĭ-m</ta>
            <ta e="T16" id="Seg_1634" s="T15">baʔ-luʔ-pi</ta>
            <ta e="T17" id="Seg_1635" s="T16">a</ta>
            <ta e="T18" id="Seg_1636" s="T17">dĭ</ta>
            <ta e="T19" id="Seg_1637" s="T18">dĭʔ-nə</ta>
            <ta e="T20" id="Seg_1638" s="T19">bar</ta>
            <ta e="T21" id="Seg_1639" s="T20">šeja-nə</ta>
            <ta e="T22" id="Seg_1640" s="T21">dĭgəttə</ta>
            <ta e="T23" id="Seg_1641" s="T22">bar</ta>
            <ta e="T24" id="Seg_1642" s="T23">dĭ-m</ta>
            <ta e="T25" id="Seg_1643" s="T24">pʼaŋd-laʔbə</ta>
            <ta e="T26" id="Seg_1644" s="T25">pʼaŋd-laʔbə</ta>
            <ta e="T27" id="Seg_1645" s="T26">ĭmbi</ta>
            <ta e="T28" id="Seg_1646" s="T27">tăn</ta>
            <ta e="T29" id="Seg_1647" s="T28">măna</ta>
            <ta e="T30" id="Seg_1648" s="T29">pʼaŋd-laʔbə-l</ta>
            <ta e="T31" id="Seg_1649" s="T30">măn</ta>
            <ta e="T32" id="Seg_1650" s="T31">tăn</ta>
            <ta e="T33" id="Seg_1651" s="T32">tănan</ta>
            <ta e="T34" id="Seg_1652" s="T33">jakšə</ta>
            <ta e="T35" id="Seg_1653" s="T34">a-bia-m</ta>
            <ta e="T36" id="Seg_1654" s="T35">a</ta>
            <ta e="T37" id="Seg_1655" s="T36">tăn</ta>
            <ta e="T39" id="Seg_1656" s="T37">măna</ta>
            <ta e="T40" id="Seg_1657" s="T39">a</ta>
            <ta e="T41" id="Seg_1658" s="T40">dĭ</ta>
            <ta e="T43" id="Seg_1659" s="T42">măn-də</ta>
            <ta e="T44" id="Seg_1660" s="T43">tăn</ta>
            <ta e="T45" id="Seg_1661" s="T44">jakšə</ta>
            <ta e="T47" id="Seg_1662" s="T46">a-bia-l</ta>
            <ta e="T48" id="Seg_1663" s="T47">a</ta>
            <ta e="T49" id="Seg_1664" s="T48">măn</ta>
            <ta e="T50" id="Seg_1665" s="T49">tănan</ta>
            <ta e="T51" id="Seg_1666" s="T50">ej</ta>
            <ta e="T52" id="Seg_1667" s="T51">jakše</ta>
            <ta e="T53" id="Seg_1668" s="T52">a-bia-m</ta>
            <ta e="T54" id="Seg_1669" s="T53">kan-žə-bəj</ta>
            <ta e="T55" id="Seg_1670" s="T54">surar-lu-bəj</ta>
            <ta e="T56" id="Seg_1671" s="T55">buga-gən</ta>
            <ta e="T57" id="Seg_1672" s="T56">buga</ta>
            <ta e="T58" id="Seg_1673" s="T57">dĭn</ta>
            <ta e="T59" id="Seg_1674" s="T58">mĭn-leʔbə</ta>
            <ta e="T60" id="Seg_1675" s="T59">a</ta>
            <ta e="T61" id="Seg_1676" s="T60">dĭ</ta>
            <ta e="T62" id="Seg_1677" s="T61">buga</ta>
            <ta e="T63" id="Seg_1678" s="T62">măn-də</ta>
            <ta e="T64" id="Seg_1679" s="T63">măn</ta>
            <ta e="T65" id="Seg_1680" s="T64">togonor-bia-m</ta>
            <ta e="T66" id="Seg_1681" s="T65">tarer-bia-m</ta>
            <ta e="T67" id="Seg_1682" s="T66">a</ta>
            <ta e="T68" id="Seg_1683" s="T67">tuj</ta>
            <ta e="T69" id="Seg_1684" s="T68">ej</ta>
            <ta e="T70" id="Seg_1685" s="T69">mo-lia-m</ta>
            <ta e="T71" id="Seg_1686" s="T70">togonor-zittə</ta>
            <ta e="T72" id="Seg_1687" s="T71">măna</ta>
            <ta e="T73" id="Seg_1688" s="T72">sürer-lüʔ-pi-ʔi</ta>
            <ta e="T74" id="Seg_1689" s="T73">am-zittə</ta>
            <ta e="T75" id="Seg_1690" s="T74">štobɨ</ta>
            <ta e="T76" id="Seg_1691" s="T75">măna</ta>
            <ta e="T77" id="Seg_1692" s="T76">am-bi-ʔi</ta>
            <ta e="T78" id="Seg_1693" s="T77">urgaːba-ʔi</ta>
            <ta e="T79" id="Seg_1694" s="T78">men-zeŋ</ta>
            <ta e="T80" id="Seg_1695" s="T79">il</ta>
            <ta e="T81" id="Seg_1696" s="T80">bar</ta>
            <ta e="T82" id="Seg_1697" s="T81">kăda</ta>
            <ta e="T83" id="Seg_1698" s="T82">a-lia-ʔi</ta>
            <ta e="T84" id="Seg_1699" s="T83">dĭgəttə</ta>
            <ta e="T85" id="Seg_1700" s="T84">kan-žə-bəj</ta>
            <ta e="T86" id="Seg_1701" s="T85">ine-gən</ta>
            <ta e="T87" id="Seg_1702" s="T86">surar-luʔ-pi</ta>
            <ta e="T88" id="Seg_1703" s="T87">dĭgəttə</ta>
            <ta e="T89" id="Seg_1704" s="T88">šonə-ga-ʔi</ta>
            <ta e="T90" id="Seg_1705" s="T89">dĭgəttə</ta>
            <ta e="T91" id="Seg_1706" s="T90">penzə</ta>
            <ta e="T92" id="Seg_1707" s="T91">mănd-laʔbə</ta>
            <ta e="T93" id="Seg_1708" s="T92">kan-žə-bəj</ta>
            <ta e="T94" id="Seg_1709" s="T93">ine-nə</ta>
            <ta e="T95" id="Seg_1710" s="T94">surar-lu-bəj</ta>
            <ta e="T96" id="Seg_1711" s="T95">dĭgəttə</ta>
            <ta e="T97" id="Seg_1712" s="T96">dĭ-zeŋ</ta>
            <ta e="T98" id="Seg_1713" s="T97">šo-laʔbə-ʔjə</ta>
            <ta e="T99" id="Seg_1714" s="T98">ine</ta>
            <ta e="T100" id="Seg_1715" s="T99">iʔbo-laʔbə</ta>
            <ta e="T101" id="Seg_1716" s="T100">sʼima-t</ta>
            <ta e="T102" id="Seg_1717" s="T101">naga</ta>
            <ta e="T103" id="Seg_1718" s="T102">bar</ta>
            <ta e="T104" id="Seg_1719" s="T103">dĭn</ta>
            <ta e="T105" id="Seg_1720" s="T104">ĭzem-neʔbə</ta>
            <ta e="T106" id="Seg_1721" s="T105">dĭ</ta>
            <ta e="T107" id="Seg_1722" s="T106">măn-də</ta>
            <ta e="T108" id="Seg_1723" s="T107">togonor-bia-m</ta>
            <ta e="T109" id="Seg_1724" s="T108">togonor-bia-m</ta>
            <ta e="T110" id="Seg_1725" s="T109">a</ta>
            <ta e="T111" id="Seg_1726" s="T110">ku-io-l</ta>
            <ta e="T112" id="Seg_1727" s="T111">tüj</ta>
            <ta e="T113" id="Seg_1728" s="T112">măna</ta>
            <ta e="T114" id="Seg_1729" s="T113">baruʔ-luʔ-pi</ta>
            <ta e="T115" id="Seg_1730" s="T114">girgit</ta>
            <ta e="T116" id="Seg_1731" s="T115">il</ta>
            <ta e="T117" id="Seg_1732" s="T116">ĭmbi=də</ta>
            <ta e="T118" id="Seg_1733" s="T117">ej</ta>
            <ta e="T119" id="Seg_1734" s="T118">jakšə</ta>
            <ta e="T120" id="Seg_1735" s="T119">ej</ta>
            <ta e="T121" id="Seg_1736" s="T120">mĭ-lie-ʔi</ta>
            <ta e="T122" id="Seg_1737" s="T121">măna</ta>
            <ta e="T123" id="Seg_1738" s="T122">dĭgəttə</ta>
            <ta e="T124" id="Seg_1739" s="T123">ku-io-l</ta>
            <ta e="T125" id="Seg_1740" s="T124">kăda</ta>
            <ta e="T126" id="Seg_1741" s="T125">măn-lia</ta>
            <ta e="T127" id="Seg_1742" s="T126">ine</ta>
            <ta e="T128" id="Seg_1743" s="T127">dĭgəttə</ta>
            <ta e="T129" id="Seg_1744" s="T128">kam-bi-ʔi</ta>
            <ta e="T130" id="Seg_1745" s="T129">no</ta>
            <ta e="T131" id="Seg_1746" s="T130">măn</ta>
            <ta e="T132" id="Seg_1747" s="T131">tănan</ta>
            <ta e="T133" id="Seg_1748" s="T132">ĭmbi=də</ta>
            <ta e="T134" id="Seg_1749" s="T133">ej</ta>
            <ta e="T135" id="Seg_1750" s="T134">a-la-m</ta>
            <ta e="T136" id="Seg_1751" s="T135">kan-žə-bəj</ta>
            <ta e="T140" id="Seg_1752" s="T139">pi-ʔi-nə</ta>
            <ta e="T141" id="Seg_1753" s="T140">šo-bi-ʔi</ta>
            <ta e="T142" id="Seg_1754" s="T141">dĭgəttə</ta>
            <ta e="T143" id="Seg_1755" s="T142">dĭ</ta>
            <ta e="T145" id="Seg_1756" s="T144">penzə</ta>
            <ta e="T146" id="Seg_1757" s="T145">kam-bi</ta>
            <ta e="T147" id="Seg_1758" s="T146">nora-nə</ta>
            <ta e="T148" id="Seg_1759" s="T147">dĭ</ta>
            <ta e="T149" id="Seg_1760" s="T148">nu-ga</ta>
            <ta e="T150" id="Seg_1761" s="T149">da</ta>
            <ta e="T153" id="Seg_1762" s="T152">supso-lu-ʔjə</ta>
            <ta e="T154" id="Seg_1763" s="T153">iʔgö</ta>
            <ta e="T155" id="Seg_1764" s="T154">da</ta>
            <ta e="T156" id="Seg_1765" s="T155">măna</ta>
            <ta e="T157" id="Seg_1766" s="T156">dĭn</ta>
            <ta e="T158" id="Seg_1767" s="T157">amno-luʔ-lu-ʔjə</ta>
            <ta e="T159" id="Seg_1768" s="T158">dĭgəttə</ta>
            <ta e="T160" id="Seg_1769" s="T159">dĭ</ta>
            <ta e="T161" id="Seg_1770" s="T160">penzə</ta>
            <ta e="T162" id="Seg_1771" s="T161">deʔ-pi</ta>
            <ta e="T163" id="Seg_1772" s="T162">diʔ-nə</ta>
            <ta e="T164" id="Seg_1773" s="T163">onʼiʔ</ta>
            <ta e="T165" id="Seg_1774" s="T164">aktʼi</ta>
            <ta e="T166" id="Seg_1775" s="T165">mĭ-bi</ta>
            <ta e="T167" id="Seg_1776" s="T166">kažnej</ta>
            <ta e="T168" id="Seg_1777" s="T167">dʼala</ta>
            <ta e="T169" id="Seg_1778" s="T168">mĭm-bi</ta>
            <ta e="T170" id="Seg_1779" s="T169">iʔgö</ta>
            <ta e="T171" id="Seg_1780" s="T170">deʔ-pi</ta>
            <ta e="T172" id="Seg_1781" s="T171">deʔ-pi</ta>
            <ta e="T173" id="Seg_1782" s="T172">dĭgəttə</ta>
            <ta e="T174" id="Seg_1783" s="T173">măn-də</ta>
            <ta e="T175" id="Seg_1784" s="T174">măn</ta>
            <ta e="T176" id="Seg_1785" s="T175">ka-la-m</ta>
            <ta e="T177" id="Seg_1786" s="T176">ĭmbi=nʼibudʼ</ta>
            <ta e="T178" id="Seg_1787" s="T177">sadar-zittə</ta>
            <ta e="T179" id="Seg_1788" s="T178">a</ta>
            <ta e="T180" id="Seg_1789" s="T179">vot</ta>
            <ta e="T181" id="Seg_1790" s="T180">măn</ta>
            <ta e="T182" id="Seg_1791" s="T181">nʼi-m</ta>
            <ta e="T183" id="Seg_1792" s="T182">pušaj</ta>
            <ta e="T184" id="Seg_1793" s="T183">mĭl-leʔbə</ta>
            <ta e="T185" id="Seg_1794" s="T184">dĭ</ta>
            <ta e="T186" id="Seg_1795" s="T185">nʼi-t</ta>
            <ta e="T187" id="Seg_1796" s="T186">šo-bi</ta>
            <ta e="T188" id="Seg_1797" s="T187">kumən</ta>
            <ta e="T189" id="Seg_1798" s="T188">dʼala</ta>
            <ta e="T190" id="Seg_1799" s="T189">mĭm-bi</ta>
            <ta e="T191" id="Seg_1800" s="T190">mĭm-bi</ta>
            <ta e="T192" id="Seg_1801" s="T191">dĭgəttə</ta>
            <ta e="T193" id="Seg_1802" s="T192">davaj</ta>
            <ta e="T194" id="Seg_1803" s="T193">dĭ</ta>
            <ta e="T195" id="Seg_1804" s="T194">pĭnzə-m</ta>
            <ta e="T196" id="Seg_1805" s="T195">kut-li-m</ta>
            <ta e="T197" id="Seg_1806" s="T196">a</ta>
            <ta e="T199" id="Seg_1807" s="T198">dĭn</ta>
            <ta e="T200" id="Seg_1808" s="T199">aktʼa</ta>
            <ta e="T201" id="Seg_1809" s="T200">iʔgö</ta>
            <ta e="T202" id="Seg_1810" s="T201">i-zittə</ta>
            <ta e="T203" id="Seg_1811" s="T202">ĭmbi</ta>
            <ta e="T204" id="Seg_1812" s="T203">kažnɨj</ta>
            <ta e="T205" id="Seg_1813" s="T204">dʼala</ta>
            <ta e="T206" id="Seg_1814" s="T205">kan-zittə</ta>
            <ta e="T207" id="Seg_1815" s="T206">döbər</ta>
            <ta e="T208" id="Seg_1816" s="T207">dĭ</ta>
            <ta e="T209" id="Seg_1817" s="T208">dĭ-m</ta>
            <ta e="T210" id="Seg_1818" s="T209">toʔ-nar-bi</ta>
            <ta e="T211" id="Seg_1819" s="T210">da</ta>
            <ta e="T213" id="Seg_1820" s="T211">xvostə</ta>
            <ta e="T214" id="Seg_1821" s="T213">dĭ</ta>
            <ta e="T215" id="Seg_1822" s="T214">dĭ-m</ta>
            <ta e="T216" id="Seg_1823" s="T215">kut-laʔ-pi</ta>
            <ta e="T217" id="Seg_1824" s="T216">dĭ</ta>
            <ta e="T218" id="Seg_1825" s="T217">penzə</ta>
            <ta e="T219" id="Seg_1826" s="T218">dĭgəttə</ta>
            <ta e="T220" id="Seg_1827" s="T219">dĭ</ta>
            <ta e="T221" id="Seg_1828" s="T220">šo-bi</ta>
            <ta e="T222" id="Seg_1829" s="T221">gijen</ta>
            <ta e="T223" id="Seg_1830" s="T222">măn</ta>
            <ta e="T224" id="Seg_1831" s="T223">nʼi-m</ta>
            <ta e="T225" id="Seg_1832" s="T224">a</ta>
            <ta e="T226" id="Seg_1833" s="T225">dĭn</ta>
            <ta e="T227" id="Seg_1834" s="T226">dĭ-zeŋ</ta>
            <ta e="T228" id="Seg_1835" s="T227">măn-də</ta>
            <ta e="T230" id="Seg_1836" s="T229">naga</ta>
            <ta e="T231" id="Seg_1837" s="T230">kam-bi</ta>
            <ta e="T233" id="Seg_1838" s="T232">dibər</ta>
            <ta e="T234" id="Seg_1839" s="T233">pi-ʔi-nə</ta>
            <ta e="T235" id="Seg_1840" s="T234">nʼi-t</ta>
            <ta e="T236" id="Seg_1841" s="T235">iʔbo-laʔbə</ta>
            <ta e="T237" id="Seg_1842" s="T236">a</ta>
            <ta e="T238" id="Seg_1843" s="T237">dĭ</ta>
            <ta e="T239" id="Seg_1844" s="T238">penzə</ta>
            <ta e="T240" id="Seg_1845" s="T239">šo-bi</ta>
            <ta e="T241" id="Seg_1846" s="T240">măn</ta>
            <ta e="T242" id="Seg_1847" s="T241">xvostə</ta>
            <ta e="T243" id="Seg_1848" s="T242">naga</ta>
            <ta e="T244" id="Seg_1849" s="T243">a</ta>
            <ta e="T246" id="Seg_1850" s="T245">tăn</ta>
            <ta e="T247" id="Seg_1851" s="T246">nʼi-l</ta>
            <ta e="T248" id="Seg_1852" s="T247">naga</ta>
            <ta e="T249" id="Seg_1853" s="T248">nu</ta>
            <ta e="T250" id="Seg_1854" s="T249">mĭ-ʔ</ta>
            <ta e="T251" id="Seg_1855" s="T250">măna</ta>
            <ta e="T252" id="Seg_1856" s="T251">kažnej</ta>
            <ta e="T253" id="Seg_1857" s="T252">dʼala</ta>
            <ta e="T254" id="Seg_1858" s="T253">dʼok</ta>
            <ta e="T256" id="Seg_1859" s="T255">ej</ta>
            <ta e="T257" id="Seg_1860" s="T256">mĭ-le-m</ta>
            <ta e="T258" id="Seg_1861" s="T257">tüj</ta>
            <ta e="T259" id="Seg_1862" s="T258">tănan</ta>
            <ta e="T260" id="Seg_1863" s="T259">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1864" s="T0">kuza</ta>
            <ta e="T2" id="Seg_1865" s="T1">šonə-gA</ta>
            <ta e="T4" id="Seg_1866" s="T3">ku-liA-t</ta>
            <ta e="T5" id="Seg_1867" s="T4">bar</ta>
            <ta e="T6" id="Seg_1868" s="T5">šü</ta>
            <ta e="T7" id="Seg_1869" s="T6">iʔbö-laʔbə</ta>
            <ta e="T8" id="Seg_1870" s="T7">a</ta>
            <ta e="T9" id="Seg_1871" s="T8">šü-Kən</ta>
            <ta e="T10" id="Seg_1872" s="T9">penzi</ta>
            <ta e="T11" id="Seg_1873" s="T10">iʔbö-laʔbə</ta>
            <ta e="T12" id="Seg_1874" s="T11">dĭ</ta>
            <ta e="T13" id="Seg_1875" s="T12">pa</ta>
            <ta e="T14" id="Seg_1876" s="T13">i-bi</ta>
            <ta e="T15" id="Seg_1877" s="T14">dĭ-m</ta>
            <ta e="T16" id="Seg_1878" s="T15">baʔbdə-luʔbdə-bi</ta>
            <ta e="T17" id="Seg_1879" s="T16">a</ta>
            <ta e="T18" id="Seg_1880" s="T17">dĭ</ta>
            <ta e="T19" id="Seg_1881" s="T18">dĭ-Tə</ta>
            <ta e="T20" id="Seg_1882" s="T19">bar</ta>
            <ta e="T21" id="Seg_1883" s="T20">šeja-Tə</ta>
            <ta e="T22" id="Seg_1884" s="T21">dĭgəttə</ta>
            <ta e="T23" id="Seg_1885" s="T22">bar</ta>
            <ta e="T24" id="Seg_1886" s="T23">dĭ-m</ta>
            <ta e="T25" id="Seg_1887" s="T24">pʼaŋdə-laʔbə</ta>
            <ta e="T26" id="Seg_1888" s="T25">pʼaŋdə-laʔbə</ta>
            <ta e="T27" id="Seg_1889" s="T26">ĭmbi</ta>
            <ta e="T28" id="Seg_1890" s="T27">tăn</ta>
            <ta e="T29" id="Seg_1891" s="T28">măna</ta>
            <ta e="T30" id="Seg_1892" s="T29">pʼaŋdə-laʔbə-l</ta>
            <ta e="T31" id="Seg_1893" s="T30">măn</ta>
            <ta e="T32" id="Seg_1894" s="T31">tăn</ta>
            <ta e="T33" id="Seg_1895" s="T32">tănan</ta>
            <ta e="T34" id="Seg_1896" s="T33">jakšə</ta>
            <ta e="T35" id="Seg_1897" s="T34">a-bi-m</ta>
            <ta e="T36" id="Seg_1898" s="T35">a</ta>
            <ta e="T37" id="Seg_1899" s="T36">tăn</ta>
            <ta e="T39" id="Seg_1900" s="T37">măna</ta>
            <ta e="T40" id="Seg_1901" s="T39">a</ta>
            <ta e="T41" id="Seg_1902" s="T40">dĭ</ta>
            <ta e="T43" id="Seg_1903" s="T42">măn-ntə</ta>
            <ta e="T44" id="Seg_1904" s="T43">tăn</ta>
            <ta e="T45" id="Seg_1905" s="T44">jakšə</ta>
            <ta e="T47" id="Seg_1906" s="T46">a-bi-l</ta>
            <ta e="T48" id="Seg_1907" s="T47">a</ta>
            <ta e="T49" id="Seg_1908" s="T48">măn</ta>
            <ta e="T50" id="Seg_1909" s="T49">tănan</ta>
            <ta e="T51" id="Seg_1910" s="T50">ej</ta>
            <ta e="T52" id="Seg_1911" s="T51">jakšə</ta>
            <ta e="T53" id="Seg_1912" s="T52">a-bi-m</ta>
            <ta e="T54" id="Seg_1913" s="T53">kan-žə-bəj</ta>
            <ta e="T55" id="Seg_1914" s="T54">surar-lV-bəj</ta>
            <ta e="T56" id="Seg_1915" s="T55">buga-Kən</ta>
            <ta e="T57" id="Seg_1916" s="T56">buga</ta>
            <ta e="T58" id="Seg_1917" s="T57">dĭn</ta>
            <ta e="T59" id="Seg_1918" s="T58">mĭn-laʔbə</ta>
            <ta e="T60" id="Seg_1919" s="T59">a</ta>
            <ta e="T61" id="Seg_1920" s="T60">dĭ</ta>
            <ta e="T62" id="Seg_1921" s="T61">buga</ta>
            <ta e="T63" id="Seg_1922" s="T62">măn-ntə</ta>
            <ta e="T64" id="Seg_1923" s="T63">măn</ta>
            <ta e="T65" id="Seg_1924" s="T64">togonər-bi-m</ta>
            <ta e="T66" id="Seg_1925" s="T65">tajər-bi-m</ta>
            <ta e="T67" id="Seg_1926" s="T66">a</ta>
            <ta e="T68" id="Seg_1927" s="T67">tüj</ta>
            <ta e="T69" id="Seg_1928" s="T68">ej</ta>
            <ta e="T70" id="Seg_1929" s="T69">mo-liA-m</ta>
            <ta e="T71" id="Seg_1930" s="T70">togonər-zittə</ta>
            <ta e="T72" id="Seg_1931" s="T71">măna</ta>
            <ta e="T73" id="Seg_1932" s="T72">sürer-luʔbdə-bi-jəʔ</ta>
            <ta e="T74" id="Seg_1933" s="T73">am-zittə</ta>
            <ta e="T75" id="Seg_1934" s="T74">štobɨ</ta>
            <ta e="T76" id="Seg_1935" s="T75">măna</ta>
            <ta e="T77" id="Seg_1936" s="T76">am-bi-jəʔ</ta>
            <ta e="T78" id="Seg_1937" s="T77">urgaːba-jəʔ</ta>
            <ta e="T79" id="Seg_1938" s="T78">men-zAŋ</ta>
            <ta e="T80" id="Seg_1939" s="T79">il</ta>
            <ta e="T81" id="Seg_1940" s="T80">bar</ta>
            <ta e="T82" id="Seg_1941" s="T81">kădaʔ</ta>
            <ta e="T83" id="Seg_1942" s="T82">a-liA-jəʔ</ta>
            <ta e="T84" id="Seg_1943" s="T83">dĭgəttə</ta>
            <ta e="T85" id="Seg_1944" s="T84">kan-žə-bəj</ta>
            <ta e="T86" id="Seg_1945" s="T85">ine-Kən</ta>
            <ta e="T87" id="Seg_1946" s="T86">surar-luʔbdə-bi</ta>
            <ta e="T88" id="Seg_1947" s="T87">dĭgəttə</ta>
            <ta e="T89" id="Seg_1948" s="T88">šonə-gA-jəʔ</ta>
            <ta e="T90" id="Seg_1949" s="T89">dĭgəttə</ta>
            <ta e="T91" id="Seg_1950" s="T90">penzi</ta>
            <ta e="T92" id="Seg_1951" s="T91">măndo-laʔbə</ta>
            <ta e="T93" id="Seg_1952" s="T92">kan-žə-bəj</ta>
            <ta e="T94" id="Seg_1953" s="T93">ine-Tə</ta>
            <ta e="T95" id="Seg_1954" s="T94">surar-lV-bəj</ta>
            <ta e="T96" id="Seg_1955" s="T95">dĭgəttə</ta>
            <ta e="T97" id="Seg_1956" s="T96">dĭ-zAŋ</ta>
            <ta e="T98" id="Seg_1957" s="T97">šo-laʔbə-jəʔ</ta>
            <ta e="T99" id="Seg_1958" s="T98">ine</ta>
            <ta e="T100" id="Seg_1959" s="T99">iʔbö-laʔbə</ta>
            <ta e="T101" id="Seg_1960" s="T100">sima-t</ta>
            <ta e="T102" id="Seg_1961" s="T101">naga</ta>
            <ta e="T103" id="Seg_1962" s="T102">bar</ta>
            <ta e="T104" id="Seg_1963" s="T103">dĭn</ta>
            <ta e="T105" id="Seg_1964" s="T104">ĭzem-laʔbə</ta>
            <ta e="T106" id="Seg_1965" s="T105">dĭ</ta>
            <ta e="T107" id="Seg_1966" s="T106">măn-ntə</ta>
            <ta e="T108" id="Seg_1967" s="T107">togonər-bi-m</ta>
            <ta e="T109" id="Seg_1968" s="T108">togonər-bi-m</ta>
            <ta e="T110" id="Seg_1969" s="T109">a</ta>
            <ta e="T111" id="Seg_1970" s="T110">ku-liA-l</ta>
            <ta e="T112" id="Seg_1971" s="T111">tüj</ta>
            <ta e="T113" id="Seg_1972" s="T112">măna</ta>
            <ta e="T114" id="Seg_1973" s="T113">barəʔ-luʔbdə-bi</ta>
            <ta e="T115" id="Seg_1974" s="T114">girgit</ta>
            <ta e="T116" id="Seg_1975" s="T115">il</ta>
            <ta e="T117" id="Seg_1976" s="T116">ĭmbi=də</ta>
            <ta e="T118" id="Seg_1977" s="T117">ej</ta>
            <ta e="T119" id="Seg_1978" s="T118">jakšə</ta>
            <ta e="T120" id="Seg_1979" s="T119">ej</ta>
            <ta e="T121" id="Seg_1980" s="T120">mĭ-liA-jəʔ</ta>
            <ta e="T122" id="Seg_1981" s="T121">măna</ta>
            <ta e="T123" id="Seg_1982" s="T122">dĭgəttə</ta>
            <ta e="T124" id="Seg_1983" s="T123">ku-liA-l</ta>
            <ta e="T125" id="Seg_1984" s="T124">kădaʔ</ta>
            <ta e="T126" id="Seg_1985" s="T125">măn-liA</ta>
            <ta e="T127" id="Seg_1986" s="T126">ine</ta>
            <ta e="T128" id="Seg_1987" s="T127">dĭgəttə</ta>
            <ta e="T129" id="Seg_1988" s="T128">kan-bi-jəʔ</ta>
            <ta e="T130" id="Seg_1989" s="T129">no</ta>
            <ta e="T131" id="Seg_1990" s="T130">măn</ta>
            <ta e="T132" id="Seg_1991" s="T131">tănan</ta>
            <ta e="T133" id="Seg_1992" s="T132">ĭmbi=də</ta>
            <ta e="T134" id="Seg_1993" s="T133">ej</ta>
            <ta e="T135" id="Seg_1994" s="T134">a-lV-m</ta>
            <ta e="T136" id="Seg_1995" s="T135">kan-žə-bəj</ta>
            <ta e="T140" id="Seg_1996" s="T139">pi-jəʔ-Tə</ta>
            <ta e="T141" id="Seg_1997" s="T140">šo-bi-jəʔ</ta>
            <ta e="T142" id="Seg_1998" s="T141">dĭgəttə</ta>
            <ta e="T143" id="Seg_1999" s="T142">dĭ</ta>
            <ta e="T145" id="Seg_2000" s="T144">penzi</ta>
            <ta e="T146" id="Seg_2001" s="T145">kan-bi</ta>
            <ta e="T147" id="Seg_2002" s="T146">nora-Tə</ta>
            <ta e="T148" id="Seg_2003" s="T147">dĭ</ta>
            <ta e="T149" id="Seg_2004" s="T148">nu-gA</ta>
            <ta e="T150" id="Seg_2005" s="T149">da</ta>
            <ta e="T153" id="Seg_2006" s="T152">supso-lV-jəʔ</ta>
            <ta e="T154" id="Seg_2007" s="T153">iʔgö</ta>
            <ta e="T155" id="Seg_2008" s="T154">da</ta>
            <ta e="T156" id="Seg_2009" s="T155">măna</ta>
            <ta e="T157" id="Seg_2010" s="T156">dĭn</ta>
            <ta e="T158" id="Seg_2011" s="T157">amno-luʔbdə-lV-jəʔ</ta>
            <ta e="T159" id="Seg_2012" s="T158">dĭgəttə</ta>
            <ta e="T160" id="Seg_2013" s="T159">dĭ</ta>
            <ta e="T161" id="Seg_2014" s="T160">penzi</ta>
            <ta e="T162" id="Seg_2015" s="T161">det-bi</ta>
            <ta e="T163" id="Seg_2016" s="T162">dĭ-Tə</ta>
            <ta e="T164" id="Seg_2017" s="T163">onʼiʔ</ta>
            <ta e="T165" id="Seg_2018" s="T164">aʔtʼi</ta>
            <ta e="T166" id="Seg_2019" s="T165">mĭ-bi</ta>
            <ta e="T167" id="Seg_2020" s="T166">kažən</ta>
            <ta e="T168" id="Seg_2021" s="T167">tʼala</ta>
            <ta e="T169" id="Seg_2022" s="T168">mĭn-bi</ta>
            <ta e="T170" id="Seg_2023" s="T169">iʔgö</ta>
            <ta e="T171" id="Seg_2024" s="T170">det-bi</ta>
            <ta e="T172" id="Seg_2025" s="T171">det-bi</ta>
            <ta e="T173" id="Seg_2026" s="T172">dĭgəttə</ta>
            <ta e="T174" id="Seg_2027" s="T173">măn-ntə</ta>
            <ta e="T175" id="Seg_2028" s="T174">măn</ta>
            <ta e="T176" id="Seg_2029" s="T175">kan-lV-m</ta>
            <ta e="T177" id="Seg_2030" s="T176">ĭmbi=nʼibudʼ</ta>
            <ta e="T178" id="Seg_2031" s="T177">sădar-zittə</ta>
            <ta e="T179" id="Seg_2032" s="T178">a</ta>
            <ta e="T180" id="Seg_2033" s="T179">vot</ta>
            <ta e="T181" id="Seg_2034" s="T180">măn</ta>
            <ta e="T182" id="Seg_2035" s="T181">nʼi-m</ta>
            <ta e="T183" id="Seg_2036" s="T182">pušaj</ta>
            <ta e="T184" id="Seg_2037" s="T183">mĭn-laʔbə</ta>
            <ta e="T185" id="Seg_2038" s="T184">dĭ</ta>
            <ta e="T186" id="Seg_2039" s="T185">nʼi-t</ta>
            <ta e="T187" id="Seg_2040" s="T186">šo-bi</ta>
            <ta e="T188" id="Seg_2041" s="T187">kumən</ta>
            <ta e="T189" id="Seg_2042" s="T188">tʼala</ta>
            <ta e="T190" id="Seg_2043" s="T189">mĭn-bi</ta>
            <ta e="T191" id="Seg_2044" s="T190">mĭn-bi</ta>
            <ta e="T192" id="Seg_2045" s="T191">dĭgəttə</ta>
            <ta e="T193" id="Seg_2046" s="T192">davaj</ta>
            <ta e="T194" id="Seg_2047" s="T193">dĭ</ta>
            <ta e="T195" id="Seg_2048" s="T194">pĭnzə-m</ta>
            <ta e="T196" id="Seg_2049" s="T195">kut-lV-m</ta>
            <ta e="T197" id="Seg_2050" s="T196">a</ta>
            <ta e="T199" id="Seg_2051" s="T198">dĭn</ta>
            <ta e="T200" id="Seg_2052" s="T199">aktʼa</ta>
            <ta e="T201" id="Seg_2053" s="T200">iʔgö</ta>
            <ta e="T202" id="Seg_2054" s="T201">i-zittə</ta>
            <ta e="T203" id="Seg_2055" s="T202">ĭmbi</ta>
            <ta e="T204" id="Seg_2056" s="T203">kažən</ta>
            <ta e="T205" id="Seg_2057" s="T204">tʼala</ta>
            <ta e="T206" id="Seg_2058" s="T205">kan-zittə</ta>
            <ta e="T207" id="Seg_2059" s="T206">döbər</ta>
            <ta e="T208" id="Seg_2060" s="T207">dĭ</ta>
            <ta e="T209" id="Seg_2061" s="T208">dĭ-m</ta>
            <ta e="T210" id="Seg_2062" s="T209">toʔbdə-nar-bi</ta>
            <ta e="T211" id="Seg_2063" s="T210">da</ta>
            <ta e="T213" id="Seg_2064" s="T211">xvostə</ta>
            <ta e="T214" id="Seg_2065" s="T213">dĭ</ta>
            <ta e="T215" id="Seg_2066" s="T214">dĭ-m</ta>
            <ta e="T216" id="Seg_2067" s="T215">kut-lAʔ-bi</ta>
            <ta e="T217" id="Seg_2068" s="T216">dĭ</ta>
            <ta e="T218" id="Seg_2069" s="T217">penzi</ta>
            <ta e="T219" id="Seg_2070" s="T218">dĭgəttə</ta>
            <ta e="T220" id="Seg_2071" s="T219">dĭ</ta>
            <ta e="T221" id="Seg_2072" s="T220">šo-bi</ta>
            <ta e="T222" id="Seg_2073" s="T221">gijen</ta>
            <ta e="T223" id="Seg_2074" s="T222">măn</ta>
            <ta e="T224" id="Seg_2075" s="T223">nʼi-m</ta>
            <ta e="T225" id="Seg_2076" s="T224">a</ta>
            <ta e="T226" id="Seg_2077" s="T225">dĭn</ta>
            <ta e="T227" id="Seg_2078" s="T226">dĭ-zAŋ</ta>
            <ta e="T228" id="Seg_2079" s="T227">măn-ntə</ta>
            <ta e="T230" id="Seg_2080" s="T229">naga</ta>
            <ta e="T231" id="Seg_2081" s="T230">kan-bi</ta>
            <ta e="T233" id="Seg_2082" s="T232">dĭbər</ta>
            <ta e="T234" id="Seg_2083" s="T233">pi-jəʔ-Tə</ta>
            <ta e="T235" id="Seg_2084" s="T234">nʼi-t</ta>
            <ta e="T236" id="Seg_2085" s="T235">iʔbö-laʔbə</ta>
            <ta e="T237" id="Seg_2086" s="T236">a</ta>
            <ta e="T238" id="Seg_2087" s="T237">dĭ</ta>
            <ta e="T239" id="Seg_2088" s="T238">penzi</ta>
            <ta e="T240" id="Seg_2089" s="T239">šo-bi</ta>
            <ta e="T241" id="Seg_2090" s="T240">măn</ta>
            <ta e="T242" id="Seg_2091" s="T241">xvostə</ta>
            <ta e="T243" id="Seg_2092" s="T242">naga</ta>
            <ta e="T244" id="Seg_2093" s="T243">a</ta>
            <ta e="T246" id="Seg_2094" s="T245">tăn</ta>
            <ta e="T247" id="Seg_2095" s="T246">nʼi-l</ta>
            <ta e="T248" id="Seg_2096" s="T247">naga</ta>
            <ta e="T249" id="Seg_2097" s="T248">nu</ta>
            <ta e="T250" id="Seg_2098" s="T249">mĭ-ʔ</ta>
            <ta e="T251" id="Seg_2099" s="T250">măna</ta>
            <ta e="T252" id="Seg_2100" s="T251">kažən</ta>
            <ta e="T253" id="Seg_2101" s="T252">tʼala</ta>
            <ta e="T254" id="Seg_2102" s="T253">dʼok</ta>
            <ta e="T256" id="Seg_2103" s="T255">ej</ta>
            <ta e="T257" id="Seg_2104" s="T256">mĭ-lV-m</ta>
            <ta e="T258" id="Seg_2105" s="T257">tüj</ta>
            <ta e="T259" id="Seg_2106" s="T258">tănan</ta>
            <ta e="T260" id="Seg_2107" s="T259">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_2108" s="T0">man.[NOM.SG]</ta>
            <ta e="T2" id="Seg_2109" s="T1">come-PRS.[3SG]</ta>
            <ta e="T4" id="Seg_2110" s="T3">see-PRS-3SG.O</ta>
            <ta e="T5" id="Seg_2111" s="T4">PTCL</ta>
            <ta e="T6" id="Seg_2112" s="T5">fire.[NOM.SG]</ta>
            <ta e="T7" id="Seg_2113" s="T6">lie-DUR.[3SG]</ta>
            <ta e="T8" id="Seg_2114" s="T7">and</ta>
            <ta e="T9" id="Seg_2115" s="T8">fire-LOC</ta>
            <ta e="T10" id="Seg_2116" s="T9">%snake.[NOM.SG]</ta>
            <ta e="T11" id="Seg_2117" s="T10">lie-DUR.[3SG]</ta>
            <ta e="T12" id="Seg_2118" s="T11">this.[NOM.SG]</ta>
            <ta e="T13" id="Seg_2119" s="T12">tree.[NOM.SG]</ta>
            <ta e="T14" id="Seg_2120" s="T13">take-PST.[3SG]</ta>
            <ta e="T15" id="Seg_2121" s="T14">this-ACC</ta>
            <ta e="T16" id="Seg_2122" s="T15">throw-MOM-PST.[3SG]</ta>
            <ta e="T17" id="Seg_2123" s="T16">and</ta>
            <ta e="T18" id="Seg_2124" s="T17">this.[NOM.SG]</ta>
            <ta e="T19" id="Seg_2125" s="T18">this-LAT</ta>
            <ta e="T20" id="Seg_2126" s="T19">PTCL</ta>
            <ta e="T21" id="Seg_2127" s="T20">neck-LAT</ta>
            <ta e="T22" id="Seg_2128" s="T21">then</ta>
            <ta e="T23" id="Seg_2129" s="T22">PTCL</ta>
            <ta e="T24" id="Seg_2130" s="T23">this-ACC</ta>
            <ta e="T25" id="Seg_2131" s="T24">press-DUR.[3SG]</ta>
            <ta e="T26" id="Seg_2132" s="T25">press-DUR.[3SG]</ta>
            <ta e="T27" id="Seg_2133" s="T26">what.[NOM.SG]</ta>
            <ta e="T28" id="Seg_2134" s="T27">you.NOM</ta>
            <ta e="T29" id="Seg_2135" s="T28">I.ACC</ta>
            <ta e="T30" id="Seg_2136" s="T29">write-DUR-2SG</ta>
            <ta e="T31" id="Seg_2137" s="T30">I.NOM</ta>
            <ta e="T32" id="Seg_2138" s="T31">you.NOM</ta>
            <ta e="T33" id="Seg_2139" s="T32">you.DAT</ta>
            <ta e="T34" id="Seg_2140" s="T33">good.[NOM.SG]</ta>
            <ta e="T35" id="Seg_2141" s="T34">make-PST-1SG</ta>
            <ta e="T36" id="Seg_2142" s="T35">and</ta>
            <ta e="T37" id="Seg_2143" s="T36">you.NOM</ta>
            <ta e="T39" id="Seg_2144" s="T37">I.ACC</ta>
            <ta e="T40" id="Seg_2145" s="T39">and</ta>
            <ta e="T41" id="Seg_2146" s="T40">this.[NOM.SG]</ta>
            <ta e="T43" id="Seg_2147" s="T42">say-IPFVZ.[3SG]</ta>
            <ta e="T44" id="Seg_2148" s="T43">you.NOM</ta>
            <ta e="T45" id="Seg_2149" s="T44">good.[NOM.SG]</ta>
            <ta e="T47" id="Seg_2150" s="T46">make-PST-2SG</ta>
            <ta e="T48" id="Seg_2151" s="T47">and</ta>
            <ta e="T49" id="Seg_2152" s="T48">I.NOM</ta>
            <ta e="T50" id="Seg_2153" s="T49">you.DAT</ta>
            <ta e="T51" id="Seg_2154" s="T50">NEG</ta>
            <ta e="T52" id="Seg_2155" s="T51">good</ta>
            <ta e="T53" id="Seg_2156" s="T52">make-PST-1SG</ta>
            <ta e="T54" id="Seg_2157" s="T53">go-OPT.DU/PL-1DU</ta>
            <ta e="T55" id="Seg_2158" s="T54">ask-FUT-1DU</ta>
            <ta e="T56" id="Seg_2159" s="T55">bull-LOC</ta>
            <ta e="T57" id="Seg_2160" s="T56">bull.[NOM.SG]</ta>
            <ta e="T58" id="Seg_2161" s="T57">there</ta>
            <ta e="T59" id="Seg_2162" s="T58">go-DUR.[3SG]</ta>
            <ta e="T60" id="Seg_2163" s="T59">and</ta>
            <ta e="T61" id="Seg_2164" s="T60">this.[NOM.SG]</ta>
            <ta e="T62" id="Seg_2165" s="T61">bull.[NOM.SG]</ta>
            <ta e="T63" id="Seg_2166" s="T62">say-IPFVZ.[3SG]</ta>
            <ta e="T64" id="Seg_2167" s="T63">I.NOM</ta>
            <ta e="T65" id="Seg_2168" s="T64">work-PST-1SG</ta>
            <ta e="T66" id="Seg_2169" s="T65">plough-PST-1SG</ta>
            <ta e="T67" id="Seg_2170" s="T66">and</ta>
            <ta e="T68" id="Seg_2171" s="T67">now</ta>
            <ta e="T69" id="Seg_2172" s="T68">NEG</ta>
            <ta e="T70" id="Seg_2173" s="T69">can-PRS-1SG</ta>
            <ta e="T71" id="Seg_2174" s="T70">work-INF.LAT</ta>
            <ta e="T72" id="Seg_2175" s="T71">I.ACC</ta>
            <ta e="T73" id="Seg_2176" s="T72">drive-MOM-PST-3PL</ta>
            <ta e="T74" id="Seg_2177" s="T73">eat-INF.LAT</ta>
            <ta e="T75" id="Seg_2178" s="T74">so.that</ta>
            <ta e="T76" id="Seg_2179" s="T75">I.ACC</ta>
            <ta e="T77" id="Seg_2180" s="T76">eat-PST-3PL</ta>
            <ta e="T78" id="Seg_2181" s="T77">bear-NOM/GEN/ACC.3PL</ta>
            <ta e="T79" id="Seg_2182" s="T78">dog-PL</ta>
            <ta e="T80" id="Seg_2183" s="T79">people.[NOM.SG]</ta>
            <ta e="T81" id="Seg_2184" s="T80">PTCL</ta>
            <ta e="T82" id="Seg_2185" s="T81">how</ta>
            <ta e="T83" id="Seg_2186" s="T82">make-PRS-3PL</ta>
            <ta e="T84" id="Seg_2187" s="T83">then</ta>
            <ta e="T85" id="Seg_2188" s="T84">go-OPT.DU/PL-1DU</ta>
            <ta e="T86" id="Seg_2189" s="T85">horse-LOC</ta>
            <ta e="T87" id="Seg_2190" s="T86">ask-MOM-PST.[3SG]</ta>
            <ta e="T88" id="Seg_2191" s="T87">then</ta>
            <ta e="T89" id="Seg_2192" s="T88">come-PRS-3PL</ta>
            <ta e="T90" id="Seg_2193" s="T89">then</ta>
            <ta e="T91" id="Seg_2194" s="T90">%snake.[NOM.SG]</ta>
            <ta e="T92" id="Seg_2195" s="T91">look-DUR.[3SG]</ta>
            <ta e="T93" id="Seg_2196" s="T92">go-OPT.DU/PL-1DU</ta>
            <ta e="T94" id="Seg_2197" s="T93">horse-LAT</ta>
            <ta e="T95" id="Seg_2198" s="T94">ask-FUT-1DU</ta>
            <ta e="T96" id="Seg_2199" s="T95">then</ta>
            <ta e="T97" id="Seg_2200" s="T96">this-PL</ta>
            <ta e="T98" id="Seg_2201" s="T97">come-DUR-3PL</ta>
            <ta e="T99" id="Seg_2202" s="T98">horse.[NOM.SG]</ta>
            <ta e="T100" id="Seg_2203" s="T99">lie-DUR.[3SG]</ta>
            <ta e="T101" id="Seg_2204" s="T100">eye-NOM/GEN.3SG</ta>
            <ta e="T102" id="Seg_2205" s="T101">NEG.EX</ta>
            <ta e="T103" id="Seg_2206" s="T102">PTCL</ta>
            <ta e="T104" id="Seg_2207" s="T103">there</ta>
            <ta e="T105" id="Seg_2208" s="T104">hurt-DUR.[3SG]</ta>
            <ta e="T106" id="Seg_2209" s="T105">this.[NOM.SG]</ta>
            <ta e="T107" id="Seg_2210" s="T106">say-IPFVZ.[3SG]</ta>
            <ta e="T108" id="Seg_2211" s="T107">work-PST-1SG</ta>
            <ta e="T109" id="Seg_2212" s="T108">work-PST-1SG</ta>
            <ta e="T110" id="Seg_2213" s="T109">and</ta>
            <ta e="T111" id="Seg_2214" s="T110">see-PRS-2SG</ta>
            <ta e="T112" id="Seg_2215" s="T111">now</ta>
            <ta e="T113" id="Seg_2216" s="T112">I.ACC</ta>
            <ta e="T114" id="Seg_2217" s="T113">leave-MOM-PST.[3SG]</ta>
            <ta e="T115" id="Seg_2218" s="T114">what.kind</ta>
            <ta e="T116" id="Seg_2219" s="T115">people.[NOM.SG]</ta>
            <ta e="T117" id="Seg_2220" s="T116">what.[NOM.SG]=INDEF</ta>
            <ta e="T118" id="Seg_2221" s="T117">NEG</ta>
            <ta e="T119" id="Seg_2222" s="T118">good.[NOM.SG]</ta>
            <ta e="T120" id="Seg_2223" s="T119">NEG</ta>
            <ta e="T121" id="Seg_2224" s="T120">give-PRS-3PL</ta>
            <ta e="T122" id="Seg_2225" s="T121">I.LAT</ta>
            <ta e="T123" id="Seg_2226" s="T122">then</ta>
            <ta e="T124" id="Seg_2227" s="T123">see-PRS-2SG</ta>
            <ta e="T125" id="Seg_2228" s="T124">how</ta>
            <ta e="T126" id="Seg_2229" s="T125">say-PRS.[3SG]</ta>
            <ta e="T127" id="Seg_2230" s="T126">horse.[NOM.SG]</ta>
            <ta e="T128" id="Seg_2231" s="T127">then</ta>
            <ta e="T129" id="Seg_2232" s="T128">go-PST-3PL</ta>
            <ta e="T130" id="Seg_2233" s="T129">well</ta>
            <ta e="T131" id="Seg_2234" s="T130">I.NOM</ta>
            <ta e="T132" id="Seg_2235" s="T131">you.DAT</ta>
            <ta e="T133" id="Seg_2236" s="T132">what.[NOM.SG]=INDEF</ta>
            <ta e="T134" id="Seg_2237" s="T133">NEG</ta>
            <ta e="T135" id="Seg_2238" s="T134">make-FUT-1SG</ta>
            <ta e="T136" id="Seg_2239" s="T135">go-OPT.DU/PL-1DU</ta>
            <ta e="T140" id="Seg_2240" s="T139">stone-3PL-LAT</ta>
            <ta e="T141" id="Seg_2241" s="T140">come-PST-3PL</ta>
            <ta e="T142" id="Seg_2242" s="T141">then</ta>
            <ta e="T143" id="Seg_2243" s="T142">this.[NOM.SG]</ta>
            <ta e="T145" id="Seg_2244" s="T144">%snake.[NOM.SG]</ta>
            <ta e="T146" id="Seg_2245" s="T145">go-PST.[3SG]</ta>
            <ta e="T147" id="Seg_2246" s="T146">burrow-LAT</ta>
            <ta e="T148" id="Seg_2247" s="T147">this.[NOM.SG]</ta>
            <ta e="T149" id="Seg_2248" s="T148">stand-PRS.[3SG]</ta>
            <ta e="T150" id="Seg_2249" s="T149">and</ta>
            <ta e="T153" id="Seg_2250" s="T152">depart-FUT-3PL</ta>
            <ta e="T154" id="Seg_2251" s="T153">many</ta>
            <ta e="T155" id="Seg_2252" s="T154">and</ta>
            <ta e="T156" id="Seg_2253" s="T155">I.ACC</ta>
            <ta e="T157" id="Seg_2254" s="T156">there</ta>
            <ta e="T158" id="Seg_2255" s="T157">live-MOM-FUT-3PL</ta>
            <ta e="T159" id="Seg_2256" s="T158">then</ta>
            <ta e="T160" id="Seg_2257" s="T159">this.[NOM.SG]</ta>
            <ta e="T161" id="Seg_2258" s="T160">%snake.[NOM.SG]</ta>
            <ta e="T162" id="Seg_2259" s="T161">bring-PST.[3SG]</ta>
            <ta e="T163" id="Seg_2260" s="T162">this-LAT</ta>
            <ta e="T164" id="Seg_2261" s="T163">one.[NOM.SG]</ta>
            <ta e="T165" id="Seg_2262" s="T164">road.[NOM.SG]</ta>
            <ta e="T166" id="Seg_2263" s="T165">give-PST.[3SG]</ta>
            <ta e="T167" id="Seg_2264" s="T166">each</ta>
            <ta e="T168" id="Seg_2265" s="T167">day.[NOM.SG]</ta>
            <ta e="T169" id="Seg_2266" s="T168">go-PST.[3SG]</ta>
            <ta e="T170" id="Seg_2267" s="T169">many</ta>
            <ta e="T171" id="Seg_2268" s="T170">bring-PST.[3SG]</ta>
            <ta e="T172" id="Seg_2269" s="T171">bring-PST.[3SG]</ta>
            <ta e="T173" id="Seg_2270" s="T172">then</ta>
            <ta e="T174" id="Seg_2271" s="T173">say-IPFVZ.[3SG]</ta>
            <ta e="T175" id="Seg_2272" s="T174">I.NOM</ta>
            <ta e="T176" id="Seg_2273" s="T175">go-FUT-1SG</ta>
            <ta e="T177" id="Seg_2274" s="T176">what=INDEF</ta>
            <ta e="T178" id="Seg_2275" s="T177">sell-INF.LAT</ta>
            <ta e="T179" id="Seg_2276" s="T178">and</ta>
            <ta e="T180" id="Seg_2277" s="T179">look</ta>
            <ta e="T181" id="Seg_2278" s="T180">I.NOM</ta>
            <ta e="T182" id="Seg_2279" s="T181">boy-NOM/GEN/ACC.1SG</ta>
            <ta e="T183" id="Seg_2280" s="T182">JUSS</ta>
            <ta e="T184" id="Seg_2281" s="T183">go-DUR.[3SG]</ta>
            <ta e="T185" id="Seg_2282" s="T184">this.[NOM.SG]</ta>
            <ta e="T186" id="Seg_2283" s="T185">son-NOM/GEN.3SG</ta>
            <ta e="T187" id="Seg_2284" s="T186">come-PST.[3SG]</ta>
            <ta e="T188" id="Seg_2285" s="T187">how.much</ta>
            <ta e="T189" id="Seg_2286" s="T188">day.[NOM.SG]</ta>
            <ta e="T190" id="Seg_2287" s="T189">go-PST.[3SG]</ta>
            <ta e="T191" id="Seg_2288" s="T190">go-PST.[3SG]</ta>
            <ta e="T192" id="Seg_2289" s="T191">then</ta>
            <ta e="T193" id="Seg_2290" s="T192">HORT</ta>
            <ta e="T194" id="Seg_2291" s="T193">this.[NOM.SG]</ta>
            <ta e="T195" id="Seg_2292" s="T194">serpent-NOM/GEN/ACC.1SG</ta>
            <ta e="T196" id="Seg_2293" s="T195">kill-FUT-1SG</ta>
            <ta e="T197" id="Seg_2294" s="T196">and</ta>
            <ta e="T199" id="Seg_2295" s="T198">there</ta>
            <ta e="T200" id="Seg_2296" s="T199">money.[NOM.SG]</ta>
            <ta e="T201" id="Seg_2297" s="T200">many</ta>
            <ta e="T202" id="Seg_2298" s="T201">take-INF.LAT</ta>
            <ta e="T203" id="Seg_2299" s="T202">what.[NOM.SG]</ta>
            <ta e="T204" id="Seg_2300" s="T203">each.[NOM.SG]</ta>
            <ta e="T205" id="Seg_2301" s="T204">day.[NOM.SG]</ta>
            <ta e="T206" id="Seg_2302" s="T205">go-INF.LAT</ta>
            <ta e="T207" id="Seg_2303" s="T206">here</ta>
            <ta e="T208" id="Seg_2304" s="T207">this.[NOM.SG]</ta>
            <ta e="T209" id="Seg_2305" s="T208">this-ACC</ta>
            <ta e="T210" id="Seg_2306" s="T209">hit-MULT-PST.[3SG]</ta>
            <ta e="T211" id="Seg_2307" s="T210">and</ta>
            <ta e="T213" id="Seg_2308" s="T211">tail.[NOM.SG]</ta>
            <ta e="T214" id="Seg_2309" s="T213">this.[NOM.SG]</ta>
            <ta e="T215" id="Seg_2310" s="T214">this-ACC</ta>
            <ta e="T216" id="Seg_2311" s="T215">kill-CVB-PST.[3SG]</ta>
            <ta e="T217" id="Seg_2312" s="T216">this.[NOM.SG]</ta>
            <ta e="T218" id="Seg_2313" s="T217">%snake.[NOM.SG]</ta>
            <ta e="T219" id="Seg_2314" s="T218">then</ta>
            <ta e="T220" id="Seg_2315" s="T219">this.[NOM.SG]</ta>
            <ta e="T221" id="Seg_2316" s="T220">come-PST.[3SG]</ta>
            <ta e="T222" id="Seg_2317" s="T221">where</ta>
            <ta e="T223" id="Seg_2318" s="T222">I.NOM</ta>
            <ta e="T224" id="Seg_2319" s="T223">boy-NOM/GEN/ACC.1SG</ta>
            <ta e="T225" id="Seg_2320" s="T224">and</ta>
            <ta e="T226" id="Seg_2321" s="T225">there</ta>
            <ta e="T227" id="Seg_2322" s="T226">this-PL</ta>
            <ta e="T228" id="Seg_2323" s="T227">say-IPFVZ.[3SG]</ta>
            <ta e="T230" id="Seg_2324" s="T229">NEG.EX</ta>
            <ta e="T231" id="Seg_2325" s="T230">go-PST.[3SG]</ta>
            <ta e="T233" id="Seg_2326" s="T232">there</ta>
            <ta e="T234" id="Seg_2327" s="T233">stone-3PL-LAT</ta>
            <ta e="T235" id="Seg_2328" s="T234">son-NOM/GEN.3SG</ta>
            <ta e="T236" id="Seg_2329" s="T235">lie-DUR.[3SG]</ta>
            <ta e="T237" id="Seg_2330" s="T236">and</ta>
            <ta e="T238" id="Seg_2331" s="T237">this.[NOM.SG]</ta>
            <ta e="T239" id="Seg_2332" s="T238">%snake.[NOM.SG]</ta>
            <ta e="T240" id="Seg_2333" s="T239">come-PST.[3SG]</ta>
            <ta e="T241" id="Seg_2334" s="T240">I.NOM</ta>
            <ta e="T242" id="Seg_2335" s="T241">tail.[NOM.SG]</ta>
            <ta e="T243" id="Seg_2336" s="T242">NEG.EX</ta>
            <ta e="T244" id="Seg_2337" s="T243">and</ta>
            <ta e="T246" id="Seg_2338" s="T245">you.NOM</ta>
            <ta e="T247" id="Seg_2339" s="T246">son-NOM/GEN/ACC.2SG</ta>
            <ta e="T248" id="Seg_2340" s="T247">NEG.EX</ta>
            <ta e="T249" id="Seg_2341" s="T248">well</ta>
            <ta e="T250" id="Seg_2342" s="T249">give-IMP.2SG</ta>
            <ta e="T251" id="Seg_2343" s="T250">I.LAT</ta>
            <ta e="T252" id="Seg_2344" s="T251">each</ta>
            <ta e="T253" id="Seg_2345" s="T252">day.[NOM.SG]</ta>
            <ta e="T254" id="Seg_2346" s="T253">no</ta>
            <ta e="T256" id="Seg_2347" s="T255">NEG</ta>
            <ta e="T257" id="Seg_2348" s="T256">give-FUT-1SG</ta>
            <ta e="T258" id="Seg_2349" s="T257">now</ta>
            <ta e="T259" id="Seg_2350" s="T258">you.DAT</ta>
            <ta e="T260" id="Seg_2351" s="T259">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_2352" s="T0">мужчина.[NOM.SG]</ta>
            <ta e="T2" id="Seg_2353" s="T1">прийти-PRS.[3SG]</ta>
            <ta e="T4" id="Seg_2354" s="T3">видеть-PRS-3SG.O</ta>
            <ta e="T5" id="Seg_2355" s="T4">PTCL</ta>
            <ta e="T6" id="Seg_2356" s="T5">огонь.[NOM.SG]</ta>
            <ta e="T7" id="Seg_2357" s="T6">лежать-DUR.[3SG]</ta>
            <ta e="T8" id="Seg_2358" s="T7">а</ta>
            <ta e="T9" id="Seg_2359" s="T8">огонь-LOC</ta>
            <ta e="T10" id="Seg_2360" s="T9">%snake.[NOM.SG]</ta>
            <ta e="T11" id="Seg_2361" s="T10">лежать-DUR.[3SG]</ta>
            <ta e="T12" id="Seg_2362" s="T11">этот.[NOM.SG]</ta>
            <ta e="T13" id="Seg_2363" s="T12">дерево.[NOM.SG]</ta>
            <ta e="T14" id="Seg_2364" s="T13">взять-PST.[3SG]</ta>
            <ta e="T15" id="Seg_2365" s="T14">этот-ACC</ta>
            <ta e="T16" id="Seg_2366" s="T15">бросить-MOM-PST.[3SG]</ta>
            <ta e="T17" id="Seg_2367" s="T16">а</ta>
            <ta e="T18" id="Seg_2368" s="T17">этот.[NOM.SG]</ta>
            <ta e="T19" id="Seg_2369" s="T18">этот-LAT</ta>
            <ta e="T20" id="Seg_2370" s="T19">PTCL</ta>
            <ta e="T21" id="Seg_2371" s="T20">шея-LAT</ta>
            <ta e="T22" id="Seg_2372" s="T21">тогда</ta>
            <ta e="T23" id="Seg_2373" s="T22">PTCL</ta>
            <ta e="T24" id="Seg_2374" s="T23">этот-ACC</ta>
            <ta e="T25" id="Seg_2375" s="T24">нажимать-DUR.[3SG]</ta>
            <ta e="T26" id="Seg_2376" s="T25">нажимать-DUR.[3SG]</ta>
            <ta e="T27" id="Seg_2377" s="T26">что.[NOM.SG]</ta>
            <ta e="T28" id="Seg_2378" s="T27">ты.NOM</ta>
            <ta e="T29" id="Seg_2379" s="T28">я.ACC</ta>
            <ta e="T30" id="Seg_2380" s="T29">писать-DUR-2SG</ta>
            <ta e="T31" id="Seg_2381" s="T30">я.NOM</ta>
            <ta e="T32" id="Seg_2382" s="T31">ты.NOM</ta>
            <ta e="T33" id="Seg_2383" s="T32">ты.DAT</ta>
            <ta e="T34" id="Seg_2384" s="T33">хороший.[NOM.SG]</ta>
            <ta e="T35" id="Seg_2385" s="T34">делать-PST-1SG</ta>
            <ta e="T36" id="Seg_2386" s="T35">а</ta>
            <ta e="T37" id="Seg_2387" s="T36">ты.NOM</ta>
            <ta e="T39" id="Seg_2388" s="T37">я.ACC</ta>
            <ta e="T40" id="Seg_2389" s="T39">а</ta>
            <ta e="T41" id="Seg_2390" s="T40">этот.[NOM.SG]</ta>
            <ta e="T43" id="Seg_2391" s="T42">сказать-IPFVZ.[3SG]</ta>
            <ta e="T44" id="Seg_2392" s="T43">ты.NOM</ta>
            <ta e="T45" id="Seg_2393" s="T44">хороший.[NOM.SG]</ta>
            <ta e="T47" id="Seg_2394" s="T46">делать-PST-2SG</ta>
            <ta e="T48" id="Seg_2395" s="T47">а</ta>
            <ta e="T49" id="Seg_2396" s="T48">я.NOM</ta>
            <ta e="T50" id="Seg_2397" s="T49">ты.DAT</ta>
            <ta e="T51" id="Seg_2398" s="T50">NEG</ta>
            <ta e="T52" id="Seg_2399" s="T51">хороший</ta>
            <ta e="T53" id="Seg_2400" s="T52">делать-PST-1SG</ta>
            <ta e="T54" id="Seg_2401" s="T53">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T55" id="Seg_2402" s="T54">спросить-FUT-1DU</ta>
            <ta e="T56" id="Seg_2403" s="T55">бык-LOC</ta>
            <ta e="T57" id="Seg_2404" s="T56">бык.[NOM.SG]</ta>
            <ta e="T58" id="Seg_2405" s="T57">там</ta>
            <ta e="T59" id="Seg_2406" s="T58">идти-DUR.[3SG]</ta>
            <ta e="T60" id="Seg_2407" s="T59">а</ta>
            <ta e="T61" id="Seg_2408" s="T60">этот.[NOM.SG]</ta>
            <ta e="T62" id="Seg_2409" s="T61">бык.[NOM.SG]</ta>
            <ta e="T63" id="Seg_2410" s="T62">сказать-IPFVZ.[3SG]</ta>
            <ta e="T64" id="Seg_2411" s="T63">я.NOM</ta>
            <ta e="T65" id="Seg_2412" s="T64">работать-PST-1SG</ta>
            <ta e="T66" id="Seg_2413" s="T65">пахать-PST-1SG</ta>
            <ta e="T67" id="Seg_2414" s="T66">а</ta>
            <ta e="T68" id="Seg_2415" s="T67">сейчас</ta>
            <ta e="T69" id="Seg_2416" s="T68">NEG</ta>
            <ta e="T70" id="Seg_2417" s="T69">мочь-PRS-1SG</ta>
            <ta e="T71" id="Seg_2418" s="T70">работать-INF.LAT</ta>
            <ta e="T72" id="Seg_2419" s="T71">я.ACC</ta>
            <ta e="T73" id="Seg_2420" s="T72">гнать-MOM-PST-3PL</ta>
            <ta e="T74" id="Seg_2421" s="T73">съесть-INF.LAT</ta>
            <ta e="T75" id="Seg_2422" s="T74">чтобы</ta>
            <ta e="T76" id="Seg_2423" s="T75">я.ACC</ta>
            <ta e="T77" id="Seg_2424" s="T76">съесть-PST-3PL</ta>
            <ta e="T78" id="Seg_2425" s="T77">медведь-NOM/GEN/ACC.3PL</ta>
            <ta e="T79" id="Seg_2426" s="T78">собака-PL</ta>
            <ta e="T80" id="Seg_2427" s="T79">люди.[NOM.SG]</ta>
            <ta e="T81" id="Seg_2428" s="T80">PTCL</ta>
            <ta e="T82" id="Seg_2429" s="T81">как</ta>
            <ta e="T83" id="Seg_2430" s="T82">делать-PRS-3PL</ta>
            <ta e="T84" id="Seg_2431" s="T83">тогда</ta>
            <ta e="T85" id="Seg_2432" s="T84">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T86" id="Seg_2433" s="T85">лошадь-LOC</ta>
            <ta e="T87" id="Seg_2434" s="T86">спросить-MOM-PST.[3SG]</ta>
            <ta e="T88" id="Seg_2435" s="T87">тогда</ta>
            <ta e="T89" id="Seg_2436" s="T88">прийти-PRS-3PL</ta>
            <ta e="T90" id="Seg_2437" s="T89">тогда</ta>
            <ta e="T91" id="Seg_2438" s="T90">%snake.[NOM.SG]</ta>
            <ta e="T92" id="Seg_2439" s="T91">смотреть-DUR.[3SG]</ta>
            <ta e="T93" id="Seg_2440" s="T92">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T94" id="Seg_2441" s="T93">лошадь-LAT</ta>
            <ta e="T95" id="Seg_2442" s="T94">спросить-FUT-1DU</ta>
            <ta e="T96" id="Seg_2443" s="T95">тогда</ta>
            <ta e="T97" id="Seg_2444" s="T96">этот-PL</ta>
            <ta e="T98" id="Seg_2445" s="T97">прийти-DUR-3PL</ta>
            <ta e="T99" id="Seg_2446" s="T98">лошадь.[NOM.SG]</ta>
            <ta e="T100" id="Seg_2447" s="T99">лежать-DUR.[3SG]</ta>
            <ta e="T101" id="Seg_2448" s="T100">глаз-NOM/GEN.3SG</ta>
            <ta e="T102" id="Seg_2449" s="T101">NEG.EX</ta>
            <ta e="T103" id="Seg_2450" s="T102">PTCL</ta>
            <ta e="T104" id="Seg_2451" s="T103">там</ta>
            <ta e="T105" id="Seg_2452" s="T104">болеть-DUR.[3SG]</ta>
            <ta e="T106" id="Seg_2453" s="T105">этот.[NOM.SG]</ta>
            <ta e="T107" id="Seg_2454" s="T106">сказать-IPFVZ.[3SG]</ta>
            <ta e="T108" id="Seg_2455" s="T107">работать-PST-1SG</ta>
            <ta e="T109" id="Seg_2456" s="T108">работать-PST-1SG</ta>
            <ta e="T110" id="Seg_2457" s="T109">а</ta>
            <ta e="T111" id="Seg_2458" s="T110">видеть-PRS-2SG</ta>
            <ta e="T112" id="Seg_2459" s="T111">сейчас</ta>
            <ta e="T113" id="Seg_2460" s="T112">я.ACC</ta>
            <ta e="T114" id="Seg_2461" s="T113">оставить-MOM-PST.[3SG]</ta>
            <ta e="T115" id="Seg_2462" s="T114">какой</ta>
            <ta e="T116" id="Seg_2463" s="T115">люди.[NOM.SG]</ta>
            <ta e="T117" id="Seg_2464" s="T116">что.[NOM.SG]=INDEF</ta>
            <ta e="T118" id="Seg_2465" s="T117">NEG</ta>
            <ta e="T119" id="Seg_2466" s="T118">хороший.[NOM.SG]</ta>
            <ta e="T120" id="Seg_2467" s="T119">NEG</ta>
            <ta e="T121" id="Seg_2468" s="T120">дать-PRS-3PL</ta>
            <ta e="T122" id="Seg_2469" s="T121">я.LAT</ta>
            <ta e="T123" id="Seg_2470" s="T122">тогда</ta>
            <ta e="T124" id="Seg_2471" s="T123">видеть-PRS-2SG</ta>
            <ta e="T125" id="Seg_2472" s="T124">как</ta>
            <ta e="T126" id="Seg_2473" s="T125">сказать-PRS.[3SG]</ta>
            <ta e="T127" id="Seg_2474" s="T126">лошадь.[NOM.SG]</ta>
            <ta e="T128" id="Seg_2475" s="T127">тогда</ta>
            <ta e="T129" id="Seg_2476" s="T128">пойти-PST-3PL</ta>
            <ta e="T130" id="Seg_2477" s="T129">ну</ta>
            <ta e="T131" id="Seg_2478" s="T130">я.NOM</ta>
            <ta e="T132" id="Seg_2479" s="T131">ты.DAT</ta>
            <ta e="T133" id="Seg_2480" s="T132">что.[NOM.SG]=INDEF</ta>
            <ta e="T134" id="Seg_2481" s="T133">NEG</ta>
            <ta e="T135" id="Seg_2482" s="T134">делать-FUT-1SG</ta>
            <ta e="T136" id="Seg_2483" s="T135">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T140" id="Seg_2484" s="T139">камень-3PL-LAT</ta>
            <ta e="T141" id="Seg_2485" s="T140">прийти-PST-3PL</ta>
            <ta e="T142" id="Seg_2486" s="T141">тогда</ta>
            <ta e="T143" id="Seg_2487" s="T142">этот.[NOM.SG]</ta>
            <ta e="T145" id="Seg_2488" s="T144">%snake.[NOM.SG]</ta>
            <ta e="T146" id="Seg_2489" s="T145">пойти-PST.[3SG]</ta>
            <ta e="T147" id="Seg_2490" s="T146">нора-LAT</ta>
            <ta e="T148" id="Seg_2491" s="T147">этот.[NOM.SG]</ta>
            <ta e="T149" id="Seg_2492" s="T148">стоять-PRS.[3SG]</ta>
            <ta e="T150" id="Seg_2493" s="T149">и</ta>
            <ta e="T153" id="Seg_2494" s="T152">уйти-FUT-3PL</ta>
            <ta e="T154" id="Seg_2495" s="T153">много</ta>
            <ta e="T155" id="Seg_2496" s="T154">и</ta>
            <ta e="T156" id="Seg_2497" s="T155">я.ACC</ta>
            <ta e="T157" id="Seg_2498" s="T156">там</ta>
            <ta e="T158" id="Seg_2499" s="T157">жить-MOM-FUT-3PL</ta>
            <ta e="T159" id="Seg_2500" s="T158">тогда</ta>
            <ta e="T160" id="Seg_2501" s="T159">этот.[NOM.SG]</ta>
            <ta e="T161" id="Seg_2502" s="T160">%snake.[NOM.SG]</ta>
            <ta e="T162" id="Seg_2503" s="T161">принести-PST.[3SG]</ta>
            <ta e="T163" id="Seg_2504" s="T162">этот-LAT</ta>
            <ta e="T164" id="Seg_2505" s="T163">один.[NOM.SG]</ta>
            <ta e="T165" id="Seg_2506" s="T164">дорога.[NOM.SG]</ta>
            <ta e="T166" id="Seg_2507" s="T165">дать-PST.[3SG]</ta>
            <ta e="T167" id="Seg_2508" s="T166">каждый</ta>
            <ta e="T168" id="Seg_2509" s="T167">день.[NOM.SG]</ta>
            <ta e="T169" id="Seg_2510" s="T168">идти-PST.[3SG]</ta>
            <ta e="T170" id="Seg_2511" s="T169">много</ta>
            <ta e="T171" id="Seg_2512" s="T170">принести-PST.[3SG]</ta>
            <ta e="T172" id="Seg_2513" s="T171">принести-PST.[3SG]</ta>
            <ta e="T173" id="Seg_2514" s="T172">тогда</ta>
            <ta e="T174" id="Seg_2515" s="T173">сказать-IPFVZ.[3SG]</ta>
            <ta e="T175" id="Seg_2516" s="T174">я.NOM</ta>
            <ta e="T176" id="Seg_2517" s="T175">пойти-FUT-1SG</ta>
            <ta e="T177" id="Seg_2518" s="T176">что=INDEF</ta>
            <ta e="T178" id="Seg_2519" s="T177">продавать-INF.LAT</ta>
            <ta e="T179" id="Seg_2520" s="T178">а</ta>
            <ta e="T180" id="Seg_2521" s="T179">вот</ta>
            <ta e="T181" id="Seg_2522" s="T180">я.NOM</ta>
            <ta e="T182" id="Seg_2523" s="T181">мальчик-NOM/GEN/ACC.1SG</ta>
            <ta e="T183" id="Seg_2524" s="T182">JUSS</ta>
            <ta e="T184" id="Seg_2525" s="T183">идти-DUR.[3SG]</ta>
            <ta e="T185" id="Seg_2526" s="T184">этот.[NOM.SG]</ta>
            <ta e="T186" id="Seg_2527" s="T185">сын-NOM/GEN.3SG</ta>
            <ta e="T187" id="Seg_2528" s="T186">прийти-PST.[3SG]</ta>
            <ta e="T188" id="Seg_2529" s="T187">сколько</ta>
            <ta e="T189" id="Seg_2530" s="T188">день.[NOM.SG]</ta>
            <ta e="T190" id="Seg_2531" s="T189">идти-PST.[3SG]</ta>
            <ta e="T191" id="Seg_2532" s="T190">идти-PST.[3SG]</ta>
            <ta e="T192" id="Seg_2533" s="T191">тогда</ta>
            <ta e="T193" id="Seg_2534" s="T192">HORT</ta>
            <ta e="T194" id="Seg_2535" s="T193">этот.[NOM.SG]</ta>
            <ta e="T195" id="Seg_2536" s="T194">змей-NOM/GEN/ACC.1SG</ta>
            <ta e="T196" id="Seg_2537" s="T195">убить-FUT-1SG</ta>
            <ta e="T197" id="Seg_2538" s="T196">а</ta>
            <ta e="T199" id="Seg_2539" s="T198">там</ta>
            <ta e="T200" id="Seg_2540" s="T199">деньги.[NOM.SG]</ta>
            <ta e="T201" id="Seg_2541" s="T200">много</ta>
            <ta e="T202" id="Seg_2542" s="T201">взять-INF.LAT</ta>
            <ta e="T203" id="Seg_2543" s="T202">что.[NOM.SG]</ta>
            <ta e="T204" id="Seg_2544" s="T203">каждый.[NOM.SG]</ta>
            <ta e="T205" id="Seg_2545" s="T204">день.[NOM.SG]</ta>
            <ta e="T206" id="Seg_2546" s="T205">пойти-INF.LAT</ta>
            <ta e="T207" id="Seg_2547" s="T206">здесь</ta>
            <ta e="T208" id="Seg_2548" s="T207">этот.[NOM.SG]</ta>
            <ta e="T209" id="Seg_2549" s="T208">этот-ACC</ta>
            <ta e="T210" id="Seg_2550" s="T209">ударить-MULT-PST.[3SG]</ta>
            <ta e="T211" id="Seg_2551" s="T210">и</ta>
            <ta e="T213" id="Seg_2552" s="T211">хвост.[NOM.SG]</ta>
            <ta e="T214" id="Seg_2553" s="T213">этот.[NOM.SG]</ta>
            <ta e="T215" id="Seg_2554" s="T214">этот-ACC</ta>
            <ta e="T216" id="Seg_2555" s="T215">убить-CVB-PST.[3SG]</ta>
            <ta e="T217" id="Seg_2556" s="T216">этот.[NOM.SG]</ta>
            <ta e="T218" id="Seg_2557" s="T217">%snake.[NOM.SG]</ta>
            <ta e="T219" id="Seg_2558" s="T218">тогда</ta>
            <ta e="T220" id="Seg_2559" s="T219">этот.[NOM.SG]</ta>
            <ta e="T221" id="Seg_2560" s="T220">прийти-PST.[3SG]</ta>
            <ta e="T222" id="Seg_2561" s="T221">где</ta>
            <ta e="T223" id="Seg_2562" s="T222">я.NOM</ta>
            <ta e="T224" id="Seg_2563" s="T223">мальчик-NOM/GEN/ACC.1SG</ta>
            <ta e="T225" id="Seg_2564" s="T224">а</ta>
            <ta e="T226" id="Seg_2565" s="T225">там</ta>
            <ta e="T227" id="Seg_2566" s="T226">этот-PL</ta>
            <ta e="T228" id="Seg_2567" s="T227">сказать-IPFVZ.[3SG]</ta>
            <ta e="T230" id="Seg_2568" s="T229">NEG.EX</ta>
            <ta e="T231" id="Seg_2569" s="T230">пойти-PST.[3SG]</ta>
            <ta e="T233" id="Seg_2570" s="T232">там</ta>
            <ta e="T234" id="Seg_2571" s="T233">камень-3PL-LAT</ta>
            <ta e="T235" id="Seg_2572" s="T234">сын-NOM/GEN.3SG</ta>
            <ta e="T236" id="Seg_2573" s="T235">лежать-DUR.[3SG]</ta>
            <ta e="T237" id="Seg_2574" s="T236">а</ta>
            <ta e="T238" id="Seg_2575" s="T237">этот.[NOM.SG]</ta>
            <ta e="T239" id="Seg_2576" s="T238">%snake.[NOM.SG]</ta>
            <ta e="T240" id="Seg_2577" s="T239">прийти-PST.[3SG]</ta>
            <ta e="T241" id="Seg_2578" s="T240">я.NOM</ta>
            <ta e="T242" id="Seg_2579" s="T241">хвост.[NOM.SG]</ta>
            <ta e="T243" id="Seg_2580" s="T242">NEG.EX</ta>
            <ta e="T244" id="Seg_2581" s="T243">а</ta>
            <ta e="T246" id="Seg_2582" s="T245">ты.NOM</ta>
            <ta e="T247" id="Seg_2583" s="T246">сын-NOM/GEN/ACC.2SG</ta>
            <ta e="T248" id="Seg_2584" s="T247">NEG.EX</ta>
            <ta e="T249" id="Seg_2585" s="T248">ну</ta>
            <ta e="T250" id="Seg_2586" s="T249">дать-IMP.2SG</ta>
            <ta e="T251" id="Seg_2587" s="T250">я.LAT</ta>
            <ta e="T252" id="Seg_2588" s="T251">каждый</ta>
            <ta e="T253" id="Seg_2589" s="T252">день.[NOM.SG]</ta>
            <ta e="T254" id="Seg_2590" s="T253">нет</ta>
            <ta e="T256" id="Seg_2591" s="T255">NEG</ta>
            <ta e="T257" id="Seg_2592" s="T256">дать-FUT-1SG</ta>
            <ta e="T258" id="Seg_2593" s="T257">сейчас</ta>
            <ta e="T259" id="Seg_2594" s="T258">ты.DAT</ta>
            <ta e="T260" id="Seg_2595" s="T259">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_2596" s="T0">n-n:case</ta>
            <ta e="T2" id="Seg_2597" s="T1">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_2598" s="T3">v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_2599" s="T4">ptcl</ta>
            <ta e="T6" id="Seg_2600" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_2601" s="T6">v-v&gt;v-v:pn</ta>
            <ta e="T8" id="Seg_2602" s="T7">conj</ta>
            <ta e="T9" id="Seg_2603" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_2604" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_2605" s="T10">v-v&gt;v-v:pn</ta>
            <ta e="T12" id="Seg_2606" s="T11">dempro-n:case</ta>
            <ta e="T13" id="Seg_2607" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_2608" s="T13">v-v:tense-v:pn</ta>
            <ta e="T15" id="Seg_2609" s="T14">dempro-n:case</ta>
            <ta e="T16" id="Seg_2610" s="T15">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_2611" s="T16">conj</ta>
            <ta e="T18" id="Seg_2612" s="T17">dempro-n:case</ta>
            <ta e="T19" id="Seg_2613" s="T18">dempro-n:case</ta>
            <ta e="T20" id="Seg_2614" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_2615" s="T20">n-n:case</ta>
            <ta e="T22" id="Seg_2616" s="T21">adv</ta>
            <ta e="T23" id="Seg_2617" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_2618" s="T23">dempro-n:case</ta>
            <ta e="T25" id="Seg_2619" s="T24">v-v&gt;v-v:pn</ta>
            <ta e="T26" id="Seg_2620" s="T25">v-v&gt;v-v:pn</ta>
            <ta e="T27" id="Seg_2621" s="T26">que-n:case</ta>
            <ta e="T28" id="Seg_2622" s="T27">pers</ta>
            <ta e="T29" id="Seg_2623" s="T28">pers</ta>
            <ta e="T30" id="Seg_2624" s="T29">v-v&gt;v-v:pn</ta>
            <ta e="T31" id="Seg_2625" s="T30">pers</ta>
            <ta e="T32" id="Seg_2626" s="T31">pers</ta>
            <ta e="T33" id="Seg_2627" s="T32">pers</ta>
            <ta e="T34" id="Seg_2628" s="T33">adj-n:case</ta>
            <ta e="T35" id="Seg_2629" s="T34">v-v:tense-v:pn</ta>
            <ta e="T36" id="Seg_2630" s="T35">conj</ta>
            <ta e="T37" id="Seg_2631" s="T36">pers</ta>
            <ta e="T39" id="Seg_2632" s="T37">pers</ta>
            <ta e="T40" id="Seg_2633" s="T39">conj</ta>
            <ta e="T41" id="Seg_2634" s="T40">dempro-n:case</ta>
            <ta e="T43" id="Seg_2635" s="T42">v-v&gt;v-v:pn</ta>
            <ta e="T44" id="Seg_2636" s="T43">pers</ta>
            <ta e="T45" id="Seg_2637" s="T44">adj-n:case</ta>
            <ta e="T47" id="Seg_2638" s="T46">v-v:tense-v:pn</ta>
            <ta e="T48" id="Seg_2639" s="T47">conj</ta>
            <ta e="T49" id="Seg_2640" s="T48">pers</ta>
            <ta e="T50" id="Seg_2641" s="T49">pers</ta>
            <ta e="T51" id="Seg_2642" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_2643" s="T51">adj</ta>
            <ta e="T53" id="Seg_2644" s="T52">v-v:tense-v:pn</ta>
            <ta e="T54" id="Seg_2645" s="T53">v-v:mood-v:pn</ta>
            <ta e="T55" id="Seg_2646" s="T54">v-v:tense-v:pn</ta>
            <ta e="T56" id="Seg_2647" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_2648" s="T56">n-n:case</ta>
            <ta e="T58" id="Seg_2649" s="T57">adv</ta>
            <ta e="T59" id="Seg_2650" s="T58">v-v&gt;v-v:pn</ta>
            <ta e="T60" id="Seg_2651" s="T59">conj</ta>
            <ta e="T61" id="Seg_2652" s="T60">dempro-n:case</ta>
            <ta e="T62" id="Seg_2653" s="T61">n-n:case</ta>
            <ta e="T63" id="Seg_2654" s="T62">v-v&gt;v-v:pn</ta>
            <ta e="T64" id="Seg_2655" s="T63">pers</ta>
            <ta e="T65" id="Seg_2656" s="T64">v-v:tense-v:pn</ta>
            <ta e="T66" id="Seg_2657" s="T65">v-v:tense-v:pn</ta>
            <ta e="T67" id="Seg_2658" s="T66">conj</ta>
            <ta e="T68" id="Seg_2659" s="T67">adv</ta>
            <ta e="T69" id="Seg_2660" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_2661" s="T69">v-v:tense-v:pn</ta>
            <ta e="T71" id="Seg_2662" s="T70">v-v:n.fin</ta>
            <ta e="T72" id="Seg_2663" s="T71">pers</ta>
            <ta e="T73" id="Seg_2664" s="T72">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T74" id="Seg_2665" s="T73">v-v:n.fin</ta>
            <ta e="T75" id="Seg_2666" s="T74">conj</ta>
            <ta e="T76" id="Seg_2667" s="T75">pers</ta>
            <ta e="T77" id="Seg_2668" s="T76">v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_2669" s="T77">n-n:case.poss</ta>
            <ta e="T79" id="Seg_2670" s="T78">n-n:num</ta>
            <ta e="T80" id="Seg_2671" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_2672" s="T80">ptcl</ta>
            <ta e="T82" id="Seg_2673" s="T81">que</ta>
            <ta e="T83" id="Seg_2674" s="T82">v-v:tense-v:pn</ta>
            <ta e="T84" id="Seg_2675" s="T83">adv</ta>
            <ta e="T85" id="Seg_2676" s="T84">v-v:mood-v:pn</ta>
            <ta e="T86" id="Seg_2677" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_2678" s="T86">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T88" id="Seg_2679" s="T87">adv</ta>
            <ta e="T89" id="Seg_2680" s="T88">v-v:tense-v:pn</ta>
            <ta e="T90" id="Seg_2681" s="T89">adv</ta>
            <ta e="T91" id="Seg_2682" s="T90">n-n:case</ta>
            <ta e="T92" id="Seg_2683" s="T91">v-v&gt;v-v:pn</ta>
            <ta e="T93" id="Seg_2684" s="T92">v-v:mood-v:pn</ta>
            <ta e="T94" id="Seg_2685" s="T93">n-n:case</ta>
            <ta e="T95" id="Seg_2686" s="T94">v-v:tense-v:pn</ta>
            <ta e="T96" id="Seg_2687" s="T95">adv</ta>
            <ta e="T97" id="Seg_2688" s="T96">dempro-n:num</ta>
            <ta e="T98" id="Seg_2689" s="T97">v-v&gt;v-v:pn</ta>
            <ta e="T99" id="Seg_2690" s="T98">n-n:case</ta>
            <ta e="T100" id="Seg_2691" s="T99">v-v&gt;v-v:pn</ta>
            <ta e="T101" id="Seg_2692" s="T100">n-n:case.poss</ta>
            <ta e="T102" id="Seg_2693" s="T101">v</ta>
            <ta e="T103" id="Seg_2694" s="T102">ptcl</ta>
            <ta e="T104" id="Seg_2695" s="T103">adv</ta>
            <ta e="T105" id="Seg_2696" s="T104">v-v&gt;v-v:pn</ta>
            <ta e="T106" id="Seg_2697" s="T105">dempro-n:case</ta>
            <ta e="T107" id="Seg_2698" s="T106">v-v&gt;v-v:pn</ta>
            <ta e="T108" id="Seg_2699" s="T107">v-v:tense-v:pn</ta>
            <ta e="T109" id="Seg_2700" s="T108">v-v:tense-v:pn</ta>
            <ta e="T110" id="Seg_2701" s="T109">conj</ta>
            <ta e="T111" id="Seg_2702" s="T110">v-v:tense-v:pn</ta>
            <ta e="T112" id="Seg_2703" s="T111">adv</ta>
            <ta e="T113" id="Seg_2704" s="T112">pers</ta>
            <ta e="T114" id="Seg_2705" s="T113">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T115" id="Seg_2706" s="T114">que</ta>
            <ta e="T116" id="Seg_2707" s="T115">n-n:case</ta>
            <ta e="T117" id="Seg_2708" s="T116">que-n:case=ptcl</ta>
            <ta e="T118" id="Seg_2709" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_2710" s="T118">adj-n:case</ta>
            <ta e="T120" id="Seg_2711" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_2712" s="T120">v-v:tense-v:pn</ta>
            <ta e="T122" id="Seg_2713" s="T121">pers</ta>
            <ta e="T123" id="Seg_2714" s="T122">adv</ta>
            <ta e="T124" id="Seg_2715" s="T123">v-v:tense-v:pn</ta>
            <ta e="T125" id="Seg_2716" s="T124">que</ta>
            <ta e="T126" id="Seg_2717" s="T125">v-v:tense-v:pn</ta>
            <ta e="T127" id="Seg_2718" s="T126">n-n:case</ta>
            <ta e="T128" id="Seg_2719" s="T127">adv</ta>
            <ta e="T129" id="Seg_2720" s="T128">v-v:tense-v:pn</ta>
            <ta e="T130" id="Seg_2721" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_2722" s="T130">pers</ta>
            <ta e="T132" id="Seg_2723" s="T131">pers</ta>
            <ta e="T133" id="Seg_2724" s="T132">que-n:case=ptcl</ta>
            <ta e="T134" id="Seg_2725" s="T133">ptcl</ta>
            <ta e="T135" id="Seg_2726" s="T134">v-v:tense-v:pn</ta>
            <ta e="T136" id="Seg_2727" s="T135">v-v:mood-v:pn</ta>
            <ta e="T140" id="Seg_2728" s="T139">n-n:case.poss-n:case</ta>
            <ta e="T141" id="Seg_2729" s="T140">v-v:tense-v:pn</ta>
            <ta e="T142" id="Seg_2730" s="T141">adv</ta>
            <ta e="T143" id="Seg_2731" s="T142">dempro-n:case</ta>
            <ta e="T145" id="Seg_2732" s="T144">n-n:case</ta>
            <ta e="T146" id="Seg_2733" s="T145">v-v:tense-v:pn</ta>
            <ta e="T147" id="Seg_2734" s="T146">n-n:case</ta>
            <ta e="T148" id="Seg_2735" s="T147">dempro-n:case</ta>
            <ta e="T149" id="Seg_2736" s="T148">v-v:tense-v:pn</ta>
            <ta e="T150" id="Seg_2737" s="T149">conj</ta>
            <ta e="T153" id="Seg_2738" s="T152">v-v:tense-v:pn</ta>
            <ta e="T154" id="Seg_2739" s="T153">quant</ta>
            <ta e="T155" id="Seg_2740" s="T154">conj</ta>
            <ta e="T156" id="Seg_2741" s="T155">pers</ta>
            <ta e="T157" id="Seg_2742" s="T156">adv</ta>
            <ta e="T158" id="Seg_2743" s="T157">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T159" id="Seg_2744" s="T158">adv</ta>
            <ta e="T160" id="Seg_2745" s="T159">dempro-n:case</ta>
            <ta e="T161" id="Seg_2746" s="T160">n-n:case</ta>
            <ta e="T162" id="Seg_2747" s="T161">v-v:tense-v:pn</ta>
            <ta e="T163" id="Seg_2748" s="T162">dempro-n:case</ta>
            <ta e="T164" id="Seg_2749" s="T163">num-n:case</ta>
            <ta e="T165" id="Seg_2750" s="T164">n-n:case</ta>
            <ta e="T166" id="Seg_2751" s="T165">v-v:tense-v:pn</ta>
            <ta e="T167" id="Seg_2752" s="T166">adj</ta>
            <ta e="T168" id="Seg_2753" s="T167">n-n:case</ta>
            <ta e="T169" id="Seg_2754" s="T168">v-v:tense-v:pn</ta>
            <ta e="T170" id="Seg_2755" s="T169">quant</ta>
            <ta e="T171" id="Seg_2756" s="T170">v-v:tense-v:pn</ta>
            <ta e="T172" id="Seg_2757" s="T171">v-v:tense-v:pn</ta>
            <ta e="T173" id="Seg_2758" s="T172">adv</ta>
            <ta e="T174" id="Seg_2759" s="T173">v-v&gt;v-v:pn</ta>
            <ta e="T175" id="Seg_2760" s="T174">pers</ta>
            <ta e="T176" id="Seg_2761" s="T175">v-v:tense-v:pn</ta>
            <ta e="T177" id="Seg_2762" s="T176">que=ptcl</ta>
            <ta e="T178" id="Seg_2763" s="T177">v-v:n.fin</ta>
            <ta e="T179" id="Seg_2764" s="T178">conj</ta>
            <ta e="T180" id="Seg_2765" s="T179">ptcl</ta>
            <ta e="T181" id="Seg_2766" s="T180">pers</ta>
            <ta e="T182" id="Seg_2767" s="T181">n-n:case.poss</ta>
            <ta e="T183" id="Seg_2768" s="T182">ptcl</ta>
            <ta e="T184" id="Seg_2769" s="T183">v-v&gt;v-v:pn</ta>
            <ta e="T185" id="Seg_2770" s="T184">dempro-n:case</ta>
            <ta e="T186" id="Seg_2771" s="T185">n-n:case.poss</ta>
            <ta e="T187" id="Seg_2772" s="T186">v-v:tense-v:pn</ta>
            <ta e="T188" id="Seg_2773" s="T187">adv</ta>
            <ta e="T189" id="Seg_2774" s="T188">n-n:case</ta>
            <ta e="T190" id="Seg_2775" s="T189">v-v:tense-v:pn</ta>
            <ta e="T191" id="Seg_2776" s="T190">v-v:tense-v:pn</ta>
            <ta e="T192" id="Seg_2777" s="T191">adv</ta>
            <ta e="T193" id="Seg_2778" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_2779" s="T193">dempro-n:case</ta>
            <ta e="T195" id="Seg_2780" s="T194">n-n:case.poss</ta>
            <ta e="T196" id="Seg_2781" s="T195">v-v:tense-v:pn</ta>
            <ta e="T197" id="Seg_2782" s="T196">conj</ta>
            <ta e="T199" id="Seg_2783" s="T198">adv</ta>
            <ta e="T200" id="Seg_2784" s="T199">n-n:case</ta>
            <ta e="T201" id="Seg_2785" s="T200">quant</ta>
            <ta e="T202" id="Seg_2786" s="T201">v-v:n.fin</ta>
            <ta e="T203" id="Seg_2787" s="T202">que-n:case</ta>
            <ta e="T204" id="Seg_2788" s="T203">adj-n:case</ta>
            <ta e="T205" id="Seg_2789" s="T204">n-n:case</ta>
            <ta e="T206" id="Seg_2790" s="T205">v-v:n.fin</ta>
            <ta e="T207" id="Seg_2791" s="T206">adv</ta>
            <ta e="T208" id="Seg_2792" s="T207">dempro-n:case</ta>
            <ta e="T209" id="Seg_2793" s="T208">dempro-n:case</ta>
            <ta e="T210" id="Seg_2794" s="T209">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T211" id="Seg_2795" s="T210">conj</ta>
            <ta e="T213" id="Seg_2796" s="T211">n-n:case</ta>
            <ta e="T214" id="Seg_2797" s="T213">dempro-n:case</ta>
            <ta e="T215" id="Seg_2798" s="T214">dempro-n:case</ta>
            <ta e="T216" id="Seg_2799" s="T215">v-v:n.fin-v:tense-v:pn</ta>
            <ta e="T217" id="Seg_2800" s="T216">dempro-n:case</ta>
            <ta e="T218" id="Seg_2801" s="T217">n-n:case</ta>
            <ta e="T219" id="Seg_2802" s="T218">adv</ta>
            <ta e="T220" id="Seg_2803" s="T219">dempro-n:case</ta>
            <ta e="T221" id="Seg_2804" s="T220">v-v:tense-v:pn</ta>
            <ta e="T222" id="Seg_2805" s="T221">que</ta>
            <ta e="T223" id="Seg_2806" s="T222">pers</ta>
            <ta e="T224" id="Seg_2807" s="T223">n-n:case.poss</ta>
            <ta e="T225" id="Seg_2808" s="T224">conj</ta>
            <ta e="T226" id="Seg_2809" s="T225">adv</ta>
            <ta e="T227" id="Seg_2810" s="T226">dempro-n:num</ta>
            <ta e="T228" id="Seg_2811" s="T227">v-v&gt;v-v:pn</ta>
            <ta e="T230" id="Seg_2812" s="T229">v</ta>
            <ta e="T231" id="Seg_2813" s="T230">v-v:tense-v:pn</ta>
            <ta e="T233" id="Seg_2814" s="T232">adv</ta>
            <ta e="T234" id="Seg_2815" s="T233">n-n:case.poss-n:case</ta>
            <ta e="T235" id="Seg_2816" s="T234">n-n:case.poss</ta>
            <ta e="T236" id="Seg_2817" s="T235">v-v&gt;v-v:pn</ta>
            <ta e="T237" id="Seg_2818" s="T236">conj</ta>
            <ta e="T238" id="Seg_2819" s="T237">dempro-n:case</ta>
            <ta e="T239" id="Seg_2820" s="T238">n-n:case</ta>
            <ta e="T240" id="Seg_2821" s="T239">v-v:tense-v:pn</ta>
            <ta e="T241" id="Seg_2822" s="T240">pers</ta>
            <ta e="T242" id="Seg_2823" s="T241">n-n:case</ta>
            <ta e="T243" id="Seg_2824" s="T242">v</ta>
            <ta e="T244" id="Seg_2825" s="T243">conj</ta>
            <ta e="T246" id="Seg_2826" s="T245">pers</ta>
            <ta e="T247" id="Seg_2827" s="T246">n-n:case.poss</ta>
            <ta e="T248" id="Seg_2828" s="T247">v</ta>
            <ta e="T249" id="Seg_2829" s="T248">ptcl</ta>
            <ta e="T250" id="Seg_2830" s="T249">v-v:mood.pn</ta>
            <ta e="T251" id="Seg_2831" s="T250">pers</ta>
            <ta e="T252" id="Seg_2832" s="T251">adj</ta>
            <ta e="T253" id="Seg_2833" s="T252">n-n:case</ta>
            <ta e="T254" id="Seg_2834" s="T253">ptcl</ta>
            <ta e="T256" id="Seg_2835" s="T255">ptcl</ta>
            <ta e="T257" id="Seg_2836" s="T256">v-v:tense-v:pn</ta>
            <ta e="T258" id="Seg_2837" s="T257">adv</ta>
            <ta e="T259" id="Seg_2838" s="T258">pers</ta>
            <ta e="T260" id="Seg_2839" s="T259">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_2840" s="T0">n</ta>
            <ta e="T2" id="Seg_2841" s="T1">v</ta>
            <ta e="T4" id="Seg_2842" s="T3">v</ta>
            <ta e="T5" id="Seg_2843" s="T4">ptcl</ta>
            <ta e="T6" id="Seg_2844" s="T5">n</ta>
            <ta e="T7" id="Seg_2845" s="T6">v</ta>
            <ta e="T8" id="Seg_2846" s="T7">conj</ta>
            <ta e="T9" id="Seg_2847" s="T8">n</ta>
            <ta e="T10" id="Seg_2848" s="T9">n</ta>
            <ta e="T11" id="Seg_2849" s="T10">v</ta>
            <ta e="T12" id="Seg_2850" s="T11">dempro</ta>
            <ta e="T13" id="Seg_2851" s="T12">n</ta>
            <ta e="T14" id="Seg_2852" s="T13">v</ta>
            <ta e="T15" id="Seg_2853" s="T14">dempro</ta>
            <ta e="T16" id="Seg_2854" s="T15">v</ta>
            <ta e="T17" id="Seg_2855" s="T16">conj</ta>
            <ta e="T18" id="Seg_2856" s="T17">dempro</ta>
            <ta e="T19" id="Seg_2857" s="T18">dempro</ta>
            <ta e="T20" id="Seg_2858" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_2859" s="T20">n</ta>
            <ta e="T22" id="Seg_2860" s="T21">adv</ta>
            <ta e="T23" id="Seg_2861" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_2862" s="T23">dempro</ta>
            <ta e="T25" id="Seg_2863" s="T24">v</ta>
            <ta e="T26" id="Seg_2864" s="T25">v</ta>
            <ta e="T27" id="Seg_2865" s="T26">que</ta>
            <ta e="T28" id="Seg_2866" s="T27">pers</ta>
            <ta e="T29" id="Seg_2867" s="T28">pers</ta>
            <ta e="T30" id="Seg_2868" s="T29">v</ta>
            <ta e="T31" id="Seg_2869" s="T30">pers</ta>
            <ta e="T32" id="Seg_2870" s="T31">pers</ta>
            <ta e="T33" id="Seg_2871" s="T32">pers</ta>
            <ta e="T34" id="Seg_2872" s="T33">adj</ta>
            <ta e="T35" id="Seg_2873" s="T34">v</ta>
            <ta e="T36" id="Seg_2874" s="T35">conj</ta>
            <ta e="T37" id="Seg_2875" s="T36">pers</ta>
            <ta e="T39" id="Seg_2876" s="T37">pers</ta>
            <ta e="T40" id="Seg_2877" s="T39">conj</ta>
            <ta e="T41" id="Seg_2878" s="T40">dempro</ta>
            <ta e="T43" id="Seg_2879" s="T42">v</ta>
            <ta e="T44" id="Seg_2880" s="T43">pers</ta>
            <ta e="T45" id="Seg_2881" s="T44">adj</ta>
            <ta e="T47" id="Seg_2882" s="T46">v</ta>
            <ta e="T48" id="Seg_2883" s="T47">conj</ta>
            <ta e="T49" id="Seg_2884" s="T48">pers</ta>
            <ta e="T50" id="Seg_2885" s="T49">pers</ta>
            <ta e="T51" id="Seg_2886" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_2887" s="T51">adj</ta>
            <ta e="T53" id="Seg_2888" s="T52">v</ta>
            <ta e="T54" id="Seg_2889" s="T53">v</ta>
            <ta e="T55" id="Seg_2890" s="T54">v</ta>
            <ta e="T56" id="Seg_2891" s="T55">n</ta>
            <ta e="T57" id="Seg_2892" s="T56">n</ta>
            <ta e="T58" id="Seg_2893" s="T57">adv</ta>
            <ta e="T59" id="Seg_2894" s="T58">v</ta>
            <ta e="T60" id="Seg_2895" s="T59">conj</ta>
            <ta e="T61" id="Seg_2896" s="T60">dempro</ta>
            <ta e="T62" id="Seg_2897" s="T61">n</ta>
            <ta e="T63" id="Seg_2898" s="T62">v</ta>
            <ta e="T64" id="Seg_2899" s="T63">pers</ta>
            <ta e="T65" id="Seg_2900" s="T64">v</ta>
            <ta e="T66" id="Seg_2901" s="T65">v</ta>
            <ta e="T67" id="Seg_2902" s="T66">conj</ta>
            <ta e="T68" id="Seg_2903" s="T67">adv</ta>
            <ta e="T69" id="Seg_2904" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_2905" s="T69">v</ta>
            <ta e="T71" id="Seg_2906" s="T70">v</ta>
            <ta e="T72" id="Seg_2907" s="T71">pers</ta>
            <ta e="T73" id="Seg_2908" s="T72">v</ta>
            <ta e="T74" id="Seg_2909" s="T73">v</ta>
            <ta e="T75" id="Seg_2910" s="T74">conj</ta>
            <ta e="T76" id="Seg_2911" s="T75">pers</ta>
            <ta e="T77" id="Seg_2912" s="T76">v</ta>
            <ta e="T78" id="Seg_2913" s="T77">n</ta>
            <ta e="T79" id="Seg_2914" s="T78">n</ta>
            <ta e="T80" id="Seg_2915" s="T79">n</ta>
            <ta e="T81" id="Seg_2916" s="T80">ptcl</ta>
            <ta e="T82" id="Seg_2917" s="T81">que</ta>
            <ta e="T83" id="Seg_2918" s="T82">v</ta>
            <ta e="T84" id="Seg_2919" s="T83">adv</ta>
            <ta e="T85" id="Seg_2920" s="T84">v</ta>
            <ta e="T86" id="Seg_2921" s="T85">n</ta>
            <ta e="T87" id="Seg_2922" s="T86">v</ta>
            <ta e="T88" id="Seg_2923" s="T87">adv</ta>
            <ta e="T89" id="Seg_2924" s="T88">v</ta>
            <ta e="T90" id="Seg_2925" s="T89">adv</ta>
            <ta e="T91" id="Seg_2926" s="T90">n</ta>
            <ta e="T92" id="Seg_2927" s="T91">v</ta>
            <ta e="T93" id="Seg_2928" s="T92">v</ta>
            <ta e="T94" id="Seg_2929" s="T93">n</ta>
            <ta e="T95" id="Seg_2930" s="T94">v</ta>
            <ta e="T96" id="Seg_2931" s="T95">adv</ta>
            <ta e="T97" id="Seg_2932" s="T96">dempro</ta>
            <ta e="T98" id="Seg_2933" s="T97">v</ta>
            <ta e="T99" id="Seg_2934" s="T98">n</ta>
            <ta e="T100" id="Seg_2935" s="T99">v</ta>
            <ta e="T101" id="Seg_2936" s="T100">n</ta>
            <ta e="T102" id="Seg_2937" s="T101">v</ta>
            <ta e="T103" id="Seg_2938" s="T102">ptcl</ta>
            <ta e="T104" id="Seg_2939" s="T103">adv</ta>
            <ta e="T105" id="Seg_2940" s="T104">v</ta>
            <ta e="T106" id="Seg_2941" s="T105">dempro</ta>
            <ta e="T107" id="Seg_2942" s="T106">v</ta>
            <ta e="T108" id="Seg_2943" s="T107">v</ta>
            <ta e="T109" id="Seg_2944" s="T108">v</ta>
            <ta e="T110" id="Seg_2945" s="T109">conj</ta>
            <ta e="T111" id="Seg_2946" s="T110">v</ta>
            <ta e="T112" id="Seg_2947" s="T111">adv</ta>
            <ta e="T113" id="Seg_2948" s="T112">pers</ta>
            <ta e="T114" id="Seg_2949" s="T113">v</ta>
            <ta e="T115" id="Seg_2950" s="T114">que</ta>
            <ta e="T116" id="Seg_2951" s="T115">n</ta>
            <ta e="T117" id="Seg_2952" s="T116">que</ta>
            <ta e="T118" id="Seg_2953" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_2954" s="T118">adj</ta>
            <ta e="T120" id="Seg_2955" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_2956" s="T120">v</ta>
            <ta e="T122" id="Seg_2957" s="T121">pers</ta>
            <ta e="T123" id="Seg_2958" s="T122">adv</ta>
            <ta e="T124" id="Seg_2959" s="T123">v</ta>
            <ta e="T125" id="Seg_2960" s="T124">que</ta>
            <ta e="T126" id="Seg_2961" s="T125">v</ta>
            <ta e="T127" id="Seg_2962" s="T126">n</ta>
            <ta e="T128" id="Seg_2963" s="T127">adv</ta>
            <ta e="T129" id="Seg_2964" s="T128">v</ta>
            <ta e="T130" id="Seg_2965" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_2966" s="T130">pers</ta>
            <ta e="T132" id="Seg_2967" s="T131">pers</ta>
            <ta e="T133" id="Seg_2968" s="T132">que</ta>
            <ta e="T134" id="Seg_2969" s="T133">ptcl</ta>
            <ta e="T135" id="Seg_2970" s="T134">v</ta>
            <ta e="T136" id="Seg_2971" s="T135">v</ta>
            <ta e="T140" id="Seg_2972" s="T139">n</ta>
            <ta e="T141" id="Seg_2973" s="T140">v</ta>
            <ta e="T142" id="Seg_2974" s="T141">adv</ta>
            <ta e="T143" id="Seg_2975" s="T142">dempro</ta>
            <ta e="T145" id="Seg_2976" s="T144">n</ta>
            <ta e="T146" id="Seg_2977" s="T145">v</ta>
            <ta e="T147" id="Seg_2978" s="T146">n</ta>
            <ta e="T148" id="Seg_2979" s="T147">dempro</ta>
            <ta e="T149" id="Seg_2980" s="T148">v</ta>
            <ta e="T150" id="Seg_2981" s="T149">conj</ta>
            <ta e="T153" id="Seg_2982" s="T152">v</ta>
            <ta e="T154" id="Seg_2983" s="T153">quant</ta>
            <ta e="T155" id="Seg_2984" s="T154">conj</ta>
            <ta e="T156" id="Seg_2985" s="T155">pers</ta>
            <ta e="T157" id="Seg_2986" s="T156">adv</ta>
            <ta e="T158" id="Seg_2987" s="T157">v</ta>
            <ta e="T159" id="Seg_2988" s="T158">adv</ta>
            <ta e="T160" id="Seg_2989" s="T159">dempro</ta>
            <ta e="T161" id="Seg_2990" s="T160">n</ta>
            <ta e="T162" id="Seg_2991" s="T161">v</ta>
            <ta e="T163" id="Seg_2992" s="T162">dempro</ta>
            <ta e="T164" id="Seg_2993" s="T163">num</ta>
            <ta e="T165" id="Seg_2994" s="T164">n</ta>
            <ta e="T166" id="Seg_2995" s="T165">v</ta>
            <ta e="T167" id="Seg_2996" s="T166">adj</ta>
            <ta e="T168" id="Seg_2997" s="T167">n</ta>
            <ta e="T169" id="Seg_2998" s="T168">v</ta>
            <ta e="T170" id="Seg_2999" s="T169">quant</ta>
            <ta e="T171" id="Seg_3000" s="T170">v</ta>
            <ta e="T172" id="Seg_3001" s="T171">v</ta>
            <ta e="T173" id="Seg_3002" s="T172">adv</ta>
            <ta e="T174" id="Seg_3003" s="T173">v</ta>
            <ta e="T175" id="Seg_3004" s="T174">pers</ta>
            <ta e="T176" id="Seg_3005" s="T175">v</ta>
            <ta e="T177" id="Seg_3006" s="T176">que</ta>
            <ta e="T178" id="Seg_3007" s="T177">v</ta>
            <ta e="T179" id="Seg_3008" s="T178">conj</ta>
            <ta e="T180" id="Seg_3009" s="T179">ptcl</ta>
            <ta e="T181" id="Seg_3010" s="T180">pers</ta>
            <ta e="T182" id="Seg_3011" s="T181">n</ta>
            <ta e="T183" id="Seg_3012" s="T182">ptcl</ta>
            <ta e="T184" id="Seg_3013" s="T183">v</ta>
            <ta e="T185" id="Seg_3014" s="T184">dempro</ta>
            <ta e="T186" id="Seg_3015" s="T185">n</ta>
            <ta e="T187" id="Seg_3016" s="T186">v</ta>
            <ta e="T188" id="Seg_3017" s="T187">adv</ta>
            <ta e="T189" id="Seg_3018" s="T188">n</ta>
            <ta e="T190" id="Seg_3019" s="T189">v</ta>
            <ta e="T191" id="Seg_3020" s="T190">v</ta>
            <ta e="T192" id="Seg_3021" s="T191">adv</ta>
            <ta e="T193" id="Seg_3022" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_3023" s="T193">dempro</ta>
            <ta e="T195" id="Seg_3024" s="T194">n</ta>
            <ta e="T196" id="Seg_3025" s="T195">v</ta>
            <ta e="T197" id="Seg_3026" s="T196">conj</ta>
            <ta e="T199" id="Seg_3027" s="T198">adv</ta>
            <ta e="T200" id="Seg_3028" s="T199">n</ta>
            <ta e="T201" id="Seg_3029" s="T200">quant</ta>
            <ta e="T202" id="Seg_3030" s="T201">v</ta>
            <ta e="T203" id="Seg_3031" s="T202">que</ta>
            <ta e="T204" id="Seg_3032" s="T203">adj</ta>
            <ta e="T205" id="Seg_3033" s="T204">n</ta>
            <ta e="T206" id="Seg_3034" s="T205">v</ta>
            <ta e="T207" id="Seg_3035" s="T206">adv</ta>
            <ta e="T208" id="Seg_3036" s="T207">dempro</ta>
            <ta e="T209" id="Seg_3037" s="T208">dempro</ta>
            <ta e="T210" id="Seg_3038" s="T209">v</ta>
            <ta e="T211" id="Seg_3039" s="T210">conj</ta>
            <ta e="T213" id="Seg_3040" s="T211">n</ta>
            <ta e="T214" id="Seg_3041" s="T213">dempro</ta>
            <ta e="T215" id="Seg_3042" s="T214">dempro</ta>
            <ta e="T216" id="Seg_3043" s="T215">v</ta>
            <ta e="T217" id="Seg_3044" s="T216">dempro</ta>
            <ta e="T218" id="Seg_3045" s="T217">n</ta>
            <ta e="T219" id="Seg_3046" s="T218">adv</ta>
            <ta e="T220" id="Seg_3047" s="T219">dempro</ta>
            <ta e="T221" id="Seg_3048" s="T220">v</ta>
            <ta e="T222" id="Seg_3049" s="T221">que</ta>
            <ta e="T223" id="Seg_3050" s="T222">pers</ta>
            <ta e="T224" id="Seg_3051" s="T223">n</ta>
            <ta e="T225" id="Seg_3052" s="T224">conj</ta>
            <ta e="T226" id="Seg_3053" s="T225">adv</ta>
            <ta e="T227" id="Seg_3054" s="T226">dempro</ta>
            <ta e="T228" id="Seg_3055" s="T227">v</ta>
            <ta e="T230" id="Seg_3056" s="T229">v</ta>
            <ta e="T231" id="Seg_3057" s="T230">v</ta>
            <ta e="T233" id="Seg_3058" s="T232">adv</ta>
            <ta e="T234" id="Seg_3059" s="T233">n</ta>
            <ta e="T235" id="Seg_3060" s="T234">n</ta>
            <ta e="T236" id="Seg_3061" s="T235">v</ta>
            <ta e="T237" id="Seg_3062" s="T236">conj</ta>
            <ta e="T238" id="Seg_3063" s="T237">dempro</ta>
            <ta e="T239" id="Seg_3064" s="T238">n</ta>
            <ta e="T240" id="Seg_3065" s="T239">v</ta>
            <ta e="T241" id="Seg_3066" s="T240">pers</ta>
            <ta e="T242" id="Seg_3067" s="T241">n</ta>
            <ta e="T243" id="Seg_3068" s="T242">v</ta>
            <ta e="T244" id="Seg_3069" s="T243">conj</ta>
            <ta e="T246" id="Seg_3070" s="T245">pers</ta>
            <ta e="T247" id="Seg_3071" s="T246">n</ta>
            <ta e="T248" id="Seg_3072" s="T247">v</ta>
            <ta e="T249" id="Seg_3073" s="T248">ptcl</ta>
            <ta e="T250" id="Seg_3074" s="T249">pers</ta>
            <ta e="T251" id="Seg_3075" s="T250">pers</ta>
            <ta e="T252" id="Seg_3076" s="T251">adj</ta>
            <ta e="T253" id="Seg_3077" s="T252">n</ta>
            <ta e="T254" id="Seg_3078" s="T253">ptcl</ta>
            <ta e="T256" id="Seg_3079" s="T255">ptcl</ta>
            <ta e="T257" id="Seg_3080" s="T256">v</ta>
            <ta e="T258" id="Seg_3081" s="T257">adv</ta>
            <ta e="T259" id="Seg_3082" s="T258">pers</ta>
            <ta e="T260" id="Seg_3083" s="T259">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_3084" s="T0">np.h:A</ta>
            <ta e="T3" id="Seg_3085" s="T2">np:Pth</ta>
            <ta e="T4" id="Seg_3086" s="T3">0.3.h:E</ta>
            <ta e="T6" id="Seg_3087" s="T5">np:Th</ta>
            <ta e="T9" id="Seg_3088" s="T8">np:L</ta>
            <ta e="T10" id="Seg_3089" s="T9">np.h:Th</ta>
            <ta e="T12" id="Seg_3090" s="T11">pro.h:A</ta>
            <ta e="T13" id="Seg_3091" s="T12">np:Th</ta>
            <ta e="T15" id="Seg_3092" s="T14">pro.h:Th</ta>
            <ta e="T16" id="Seg_3093" s="T15">0.3.h:A</ta>
            <ta e="T18" id="Seg_3094" s="T17">pro.h:A</ta>
            <ta e="T19" id="Seg_3095" s="T18">pro:G</ta>
            <ta e="T21" id="Seg_3096" s="T20">np:G</ta>
            <ta e="T22" id="Seg_3097" s="T21">adv:Time</ta>
            <ta e="T24" id="Seg_3098" s="T23">pro.h:E</ta>
            <ta e="T25" id="Seg_3099" s="T24">0.3.h:A</ta>
            <ta e="T26" id="Seg_3100" s="T25">0.3.h:A</ta>
            <ta e="T28" id="Seg_3101" s="T27">pro.h:A</ta>
            <ta e="T29" id="Seg_3102" s="T28">pro.h:E</ta>
            <ta e="T31" id="Seg_3103" s="T30">pro.h:A</ta>
            <ta e="T33" id="Seg_3104" s="T32">pro.h:B</ta>
            <ta e="T37" id="Seg_3105" s="T36">pro.h:A</ta>
            <ta e="T39" id="Seg_3106" s="T37">pro.h:B</ta>
            <ta e="T41" id="Seg_3107" s="T40">pro.h:A</ta>
            <ta e="T44" id="Seg_3108" s="T43">pro.h:A</ta>
            <ta e="T49" id="Seg_3109" s="T48">pro.h:A</ta>
            <ta e="T50" id="Seg_3110" s="T49">pro.h:B</ta>
            <ta e="T54" id="Seg_3111" s="T53">0.1.h:A</ta>
            <ta e="T55" id="Seg_3112" s="T54">0.1.h:A</ta>
            <ta e="T56" id="Seg_3113" s="T55">np.h:R</ta>
            <ta e="T57" id="Seg_3114" s="T56">np.h:A</ta>
            <ta e="T58" id="Seg_3115" s="T57">adv:L</ta>
            <ta e="T62" id="Seg_3116" s="T61">np.h:A</ta>
            <ta e="T64" id="Seg_3117" s="T63">pro.h:A</ta>
            <ta e="T66" id="Seg_3118" s="T65">0.1.h:A</ta>
            <ta e="T68" id="Seg_3119" s="T67">adv:Time</ta>
            <ta e="T70" id="Seg_3120" s="T69">0.1.h:A</ta>
            <ta e="T72" id="Seg_3121" s="T71">pro.h:Th</ta>
            <ta e="T73" id="Seg_3122" s="T72">0.3.h:A</ta>
            <ta e="T76" id="Seg_3123" s="T75">pro.h:P</ta>
            <ta e="T78" id="Seg_3124" s="T77">np:A</ta>
            <ta e="T79" id="Seg_3125" s="T78">np:A</ta>
            <ta e="T80" id="Seg_3126" s="T79">np.h:A</ta>
            <ta e="T84" id="Seg_3127" s="T83">adv:Time</ta>
            <ta e="T85" id="Seg_3128" s="T84">0.1.h:A</ta>
            <ta e="T86" id="Seg_3129" s="T85">np.h:R</ta>
            <ta e="T88" id="Seg_3130" s="T87">adv:Time</ta>
            <ta e="T89" id="Seg_3131" s="T88">0.3.h:A</ta>
            <ta e="T90" id="Seg_3132" s="T89">adv:Time</ta>
            <ta e="T91" id="Seg_3133" s="T90">np.h:A</ta>
            <ta e="T93" id="Seg_3134" s="T92">0.1.h:A</ta>
            <ta e="T94" id="Seg_3135" s="T93">np.h:R</ta>
            <ta e="T95" id="Seg_3136" s="T94">0.1.h:A</ta>
            <ta e="T96" id="Seg_3137" s="T95">adv:Time</ta>
            <ta e="T97" id="Seg_3138" s="T96">pro.h:A</ta>
            <ta e="T99" id="Seg_3139" s="T98">np.h:E</ta>
            <ta e="T101" id="Seg_3140" s="T100">np:Th</ta>
            <ta e="T104" id="Seg_3141" s="T103">adv:L</ta>
            <ta e="T105" id="Seg_3142" s="T104">0.3.h:E</ta>
            <ta e="T106" id="Seg_3143" s="T105">pro.h:A</ta>
            <ta e="T108" id="Seg_3144" s="T107">0.1.h:A</ta>
            <ta e="T109" id="Seg_3145" s="T108">0.1.h:A</ta>
            <ta e="T111" id="Seg_3146" s="T110">0.2.h:E</ta>
            <ta e="T112" id="Seg_3147" s="T111">adv:Time</ta>
            <ta e="T113" id="Seg_3148" s="T112">pro.h:Th</ta>
            <ta e="T114" id="Seg_3149" s="T113">0.3.h:A</ta>
            <ta e="T117" id="Seg_3150" s="T116">pro:Th</ta>
            <ta e="T121" id="Seg_3151" s="T120">0.3.h:A</ta>
            <ta e="T122" id="Seg_3152" s="T121">pro.h:R</ta>
            <ta e="T123" id="Seg_3153" s="T122">adv:Time</ta>
            <ta e="T124" id="Seg_3154" s="T123">0.2.h:E</ta>
            <ta e="T127" id="Seg_3155" s="T126">np.h:A</ta>
            <ta e="T128" id="Seg_3156" s="T127">adv:Time</ta>
            <ta e="T129" id="Seg_3157" s="T128">0.3.h:A</ta>
            <ta e="T131" id="Seg_3158" s="T130">pro.h:A</ta>
            <ta e="T132" id="Seg_3159" s="T131">pro.h:E</ta>
            <ta e="T133" id="Seg_3160" s="T132">pro:Th</ta>
            <ta e="T136" id="Seg_3161" s="T135">0.1.h:A</ta>
            <ta e="T141" id="Seg_3162" s="T140">0.3.h:A</ta>
            <ta e="T142" id="Seg_3163" s="T141">adv:Time</ta>
            <ta e="T145" id="Seg_3164" s="T144">np.h:A</ta>
            <ta e="T147" id="Seg_3165" s="T146">np:G</ta>
            <ta e="T148" id="Seg_3166" s="T147">pro.h:A</ta>
            <ta e="T154" id="Seg_3167" s="T153">np:A</ta>
            <ta e="T156" id="Seg_3168" s="T155">pro.h:P</ta>
            <ta e="T157" id="Seg_3169" s="T156">adv:L</ta>
            <ta e="T158" id="Seg_3170" s="T157">0.3.h:A</ta>
            <ta e="T159" id="Seg_3171" s="T158">adv:Time</ta>
            <ta e="T161" id="Seg_3172" s="T160">np.h:A</ta>
            <ta e="T163" id="Seg_3173" s="T162">pro.h:R</ta>
            <ta e="T165" id="Seg_3174" s="T164">np:Th</ta>
            <ta e="T166" id="Seg_3175" s="T165">0.3.h:A</ta>
            <ta e="T169" id="Seg_3176" s="T168">0.3.h:A</ta>
            <ta e="T170" id="Seg_3177" s="T169">np:Th</ta>
            <ta e="T171" id="Seg_3178" s="T170">0.3.h:A</ta>
            <ta e="T172" id="Seg_3179" s="T171">0.3.h:A</ta>
            <ta e="T173" id="Seg_3180" s="T172">adv:Time</ta>
            <ta e="T174" id="Seg_3181" s="T173">0.3.h:A</ta>
            <ta e="T175" id="Seg_3182" s="T174">pro.h:A</ta>
            <ta e="T177" id="Seg_3183" s="T176">pro:Th</ta>
            <ta e="T181" id="Seg_3184" s="T180">pro.h:Poss</ta>
            <ta e="T182" id="Seg_3185" s="T181">np.h:Th</ta>
            <ta e="T186" id="Seg_3186" s="T185">np.h:A</ta>
            <ta e="T190" id="Seg_3187" s="T189">0.3.h:A</ta>
            <ta e="T191" id="Seg_3188" s="T190">0.3.h:A</ta>
            <ta e="T192" id="Seg_3189" s="T191">adv:Time</ta>
            <ta e="T195" id="Seg_3190" s="T194">np.h:P</ta>
            <ta e="T196" id="Seg_3191" s="T195">0.1.h:A</ta>
            <ta e="T199" id="Seg_3192" s="T198">adv:L</ta>
            <ta e="T200" id="Seg_3193" s="T199">np:Th</ta>
            <ta e="T207" id="Seg_3194" s="T206">adv:L</ta>
            <ta e="T208" id="Seg_3195" s="T207">pro.h:A</ta>
            <ta e="T209" id="Seg_3196" s="T208">pro.h:E</ta>
            <ta e="T213" id="Seg_3197" s="T211">np:P</ta>
            <ta e="T214" id="Seg_3198" s="T213">pro.h:A</ta>
            <ta e="T215" id="Seg_3199" s="T214">pro.h:P</ta>
            <ta e="T219" id="Seg_3200" s="T218">adv:Time</ta>
            <ta e="T220" id="Seg_3201" s="T219">pro.h:A</ta>
            <ta e="T223" id="Seg_3202" s="T222">pro.h:Poss</ta>
            <ta e="T224" id="Seg_3203" s="T223">np.h:Th</ta>
            <ta e="T226" id="Seg_3204" s="T225">adv:L</ta>
            <ta e="T227" id="Seg_3205" s="T226">pro.h:A</ta>
            <ta e="T230" id="Seg_3206" s="T229">0.3.h:Th</ta>
            <ta e="T231" id="Seg_3207" s="T230">0.3.h:A</ta>
            <ta e="T233" id="Seg_3208" s="T232">adv:L</ta>
            <ta e="T234" id="Seg_3209" s="T233">np:G</ta>
            <ta e="T235" id="Seg_3210" s="T234">np.h:Th</ta>
            <ta e="T239" id="Seg_3211" s="T238">np.h:A</ta>
            <ta e="T241" id="Seg_3212" s="T240">pro.h:Poss</ta>
            <ta e="T242" id="Seg_3213" s="T241">np:Th</ta>
            <ta e="T246" id="Seg_3214" s="T245">pro.h:Poss</ta>
            <ta e="T247" id="Seg_3215" s="T246">np.h:Th</ta>
            <ta e="T250" id="Seg_3216" s="T249">0.2.h:A</ta>
            <ta e="T251" id="Seg_3217" s="T250">pro.h:R</ta>
            <ta e="T253" id="Seg_3218" s="T252">np:Th</ta>
            <ta e="T257" id="Seg_3219" s="T256">0.1.h:A</ta>
            <ta e="T258" id="Seg_3220" s="T257">adv:Time</ta>
            <ta e="T259" id="Seg_3221" s="T258">pro.h:R</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_3222" s="T0">np.h:S</ta>
            <ta e="T2" id="Seg_3223" s="T1">v:pred</ta>
            <ta e="T4" id="Seg_3224" s="T3">v:pred 0.3.h:S</ta>
            <ta e="T6" id="Seg_3225" s="T5">np:S</ta>
            <ta e="T7" id="Seg_3226" s="T6">v:pred</ta>
            <ta e="T10" id="Seg_3227" s="T9">np.h:S</ta>
            <ta e="T11" id="Seg_3228" s="T10">v:pred</ta>
            <ta e="T12" id="Seg_3229" s="T11">pro.h:S</ta>
            <ta e="T13" id="Seg_3230" s="T12">np:O</ta>
            <ta e="T14" id="Seg_3231" s="T13">v:pred</ta>
            <ta e="T15" id="Seg_3232" s="T14">pro.h:O</ta>
            <ta e="T16" id="Seg_3233" s="T15">v:pred 0.3.h:S</ta>
            <ta e="T18" id="Seg_3234" s="T17">pro.h:S</ta>
            <ta e="T24" id="Seg_3235" s="T23">pro.h:O</ta>
            <ta e="T25" id="Seg_3236" s="T24">v:pred 0.3.h:S</ta>
            <ta e="T26" id="Seg_3237" s="T25">v:pred 0.3.h:S</ta>
            <ta e="T28" id="Seg_3238" s="T27">pro.h:S</ta>
            <ta e="T29" id="Seg_3239" s="T28">pro.h:O</ta>
            <ta e="T30" id="Seg_3240" s="T29">v:pred</ta>
            <ta e="T31" id="Seg_3241" s="T30">pro.h:S</ta>
            <ta e="T34" id="Seg_3242" s="T33">np:O</ta>
            <ta e="T35" id="Seg_3243" s="T34">v:pred</ta>
            <ta e="T37" id="Seg_3244" s="T36">pro.h:S</ta>
            <ta e="T39" id="Seg_3245" s="T37">pro.h:O</ta>
            <ta e="T41" id="Seg_3246" s="T40">pro.h:S</ta>
            <ta e="T43" id="Seg_3247" s="T42">v:pred</ta>
            <ta e="T44" id="Seg_3248" s="T43">pro.h:S</ta>
            <ta e="T45" id="Seg_3249" s="T44">np:O</ta>
            <ta e="T47" id="Seg_3250" s="T46">v:pred</ta>
            <ta e="T49" id="Seg_3251" s="T48">pro.h:S</ta>
            <ta e="T51" id="Seg_3252" s="T50">ptcl.neg</ta>
            <ta e="T52" id="Seg_3253" s="T51">np:O</ta>
            <ta e="T53" id="Seg_3254" s="T52">v:pred</ta>
            <ta e="T54" id="Seg_3255" s="T53">v:pred 0.1.h:S</ta>
            <ta e="T55" id="Seg_3256" s="T54">v:pred 0.1.h:S</ta>
            <ta e="T57" id="Seg_3257" s="T56">np.h:S</ta>
            <ta e="T59" id="Seg_3258" s="T58">v:pred</ta>
            <ta e="T62" id="Seg_3259" s="T61">np.h:S</ta>
            <ta e="T63" id="Seg_3260" s="T62">v:pred</ta>
            <ta e="T64" id="Seg_3261" s="T63">pro.h:S</ta>
            <ta e="T65" id="Seg_3262" s="T64">v:pred</ta>
            <ta e="T66" id="Seg_3263" s="T65">v:pred 0.1.h:S</ta>
            <ta e="T69" id="Seg_3264" s="T68">ptcl.neg</ta>
            <ta e="T70" id="Seg_3265" s="T69">v:pred 0.1.h:S</ta>
            <ta e="T72" id="Seg_3266" s="T71">pro.h:O</ta>
            <ta e="T73" id="Seg_3267" s="T72">v:pred 0.3.h:S</ta>
            <ta e="T74" id="Seg_3268" s="T73">s:purp</ta>
            <ta e="T76" id="Seg_3269" s="T75">pro.h:O</ta>
            <ta e="T77" id="Seg_3270" s="T76">v:pred</ta>
            <ta e="T78" id="Seg_3271" s="T77">np:S</ta>
            <ta e="T79" id="Seg_3272" s="T78">np:S</ta>
            <ta e="T80" id="Seg_3273" s="T79">np.h:S</ta>
            <ta e="T83" id="Seg_3274" s="T82">v:pred</ta>
            <ta e="T85" id="Seg_3275" s="T84">v:pred 0.1.h:S</ta>
            <ta e="T87" id="Seg_3276" s="T86">v:pred</ta>
            <ta e="T89" id="Seg_3277" s="T88">v:pred 0.3.h:S</ta>
            <ta e="T91" id="Seg_3278" s="T90">np.h:S</ta>
            <ta e="T92" id="Seg_3279" s="T91">v:pred</ta>
            <ta e="T93" id="Seg_3280" s="T92">v:pred 0.1.h:S</ta>
            <ta e="T95" id="Seg_3281" s="T94">v:pred 0.1.h:S</ta>
            <ta e="T97" id="Seg_3282" s="T96">pro.h:S</ta>
            <ta e="T98" id="Seg_3283" s="T97">v:pred</ta>
            <ta e="T99" id="Seg_3284" s="T98">np.h:S</ta>
            <ta e="T100" id="Seg_3285" s="T99">v:pred</ta>
            <ta e="T101" id="Seg_3286" s="T100">np:S</ta>
            <ta e="T102" id="Seg_3287" s="T101">v:pred</ta>
            <ta e="T105" id="Seg_3288" s="T104">v:pred 0.3.h:S</ta>
            <ta e="T106" id="Seg_3289" s="T105">pro.h:S</ta>
            <ta e="T107" id="Seg_3290" s="T106">v:pred</ta>
            <ta e="T108" id="Seg_3291" s="T107">v:pred 0.1.h:S</ta>
            <ta e="T109" id="Seg_3292" s="T108">v:pred 0.1.h:S</ta>
            <ta e="T111" id="Seg_3293" s="T110">v:pred 0.2.h:S</ta>
            <ta e="T113" id="Seg_3294" s="T112">pro.h:O</ta>
            <ta e="T114" id="Seg_3295" s="T113">v:pred 0.3.h:S</ta>
            <ta e="T117" id="Seg_3296" s="T116">pro:O</ta>
            <ta e="T118" id="Seg_3297" s="T117">ptcl.neg</ta>
            <ta e="T120" id="Seg_3298" s="T119">ptcl.neg</ta>
            <ta e="T121" id="Seg_3299" s="T120">v:pred 0.3.h:S</ta>
            <ta e="T124" id="Seg_3300" s="T123">v:pred 0.2.h:S</ta>
            <ta e="T126" id="Seg_3301" s="T125">v:pred</ta>
            <ta e="T127" id="Seg_3302" s="T126">np.h:S</ta>
            <ta e="T129" id="Seg_3303" s="T128">v:pred 0.3.h:S</ta>
            <ta e="T131" id="Seg_3304" s="T130">pro.h:S</ta>
            <ta e="T133" id="Seg_3305" s="T132">pro:O</ta>
            <ta e="T134" id="Seg_3306" s="T133">ptcl.neg</ta>
            <ta e="T135" id="Seg_3307" s="T134">v:pred</ta>
            <ta e="T136" id="Seg_3308" s="T135">v:pred 0.1.h:S</ta>
            <ta e="T141" id="Seg_3309" s="T140">v:pred 0.3.h:S</ta>
            <ta e="T145" id="Seg_3310" s="T144">np.h:S</ta>
            <ta e="T146" id="Seg_3311" s="T145">v:pred</ta>
            <ta e="T148" id="Seg_3312" s="T147">pro.h:S</ta>
            <ta e="T149" id="Seg_3313" s="T148">v:pred</ta>
            <ta e="T153" id="Seg_3314" s="T152">v:pred</ta>
            <ta e="T154" id="Seg_3315" s="T153">np:S</ta>
            <ta e="T156" id="Seg_3316" s="T155">pro.h:O</ta>
            <ta e="T158" id="Seg_3317" s="T157">v:pred 0.3.h:S</ta>
            <ta e="T161" id="Seg_3318" s="T160">np.h:S</ta>
            <ta e="T162" id="Seg_3319" s="T161">v:pred</ta>
            <ta e="T165" id="Seg_3320" s="T164">np:O</ta>
            <ta e="T166" id="Seg_3321" s="T165">v:pred 0.3.h:S</ta>
            <ta e="T169" id="Seg_3322" s="T168">v:pred 0.3.h:S</ta>
            <ta e="T170" id="Seg_3323" s="T169">np:O</ta>
            <ta e="T171" id="Seg_3324" s="T170">v:pred 0.3.h:S</ta>
            <ta e="T172" id="Seg_3325" s="T171">v:pred 0.3.h:S</ta>
            <ta e="T174" id="Seg_3326" s="T173">v:pred 0.3.h:S</ta>
            <ta e="T175" id="Seg_3327" s="T174">pro.h:S</ta>
            <ta e="T176" id="Seg_3328" s="T175">v:pred</ta>
            <ta e="T177" id="Seg_3329" s="T176">pro:O</ta>
            <ta e="T180" id="Seg_3330" s="T179">ptcl:pred</ta>
            <ta e="T183" id="Seg_3331" s="T182">ptcl:pred</ta>
            <ta e="T186" id="Seg_3332" s="T185">np.h:S</ta>
            <ta e="T187" id="Seg_3333" s="T186">v:pred</ta>
            <ta e="T190" id="Seg_3334" s="T189">v:pred 0.3.h:S</ta>
            <ta e="T191" id="Seg_3335" s="T190">v:pred 0.3.h:S</ta>
            <ta e="T193" id="Seg_3336" s="T192">ptcl:pred</ta>
            <ta e="T195" id="Seg_3337" s="T194">np.h:O</ta>
            <ta e="T196" id="Seg_3338" s="T195">v:pred 0.1.h:S</ta>
            <ta e="T200" id="Seg_3339" s="T199">np:S</ta>
            <ta e="T202" id="Seg_3340" s="T201">v:pred</ta>
            <ta e="T206" id="Seg_3341" s="T205">v:pred</ta>
            <ta e="T208" id="Seg_3342" s="T207">pro.h:S</ta>
            <ta e="T209" id="Seg_3343" s="T208">pro.h:O</ta>
            <ta e="T210" id="Seg_3344" s="T209">v:pred</ta>
            <ta e="T213" id="Seg_3345" s="T211">np:O</ta>
            <ta e="T214" id="Seg_3346" s="T213">pro.h:S</ta>
            <ta e="T215" id="Seg_3347" s="T214">pro.h:O</ta>
            <ta e="T216" id="Seg_3348" s="T215">v:pred</ta>
            <ta e="T220" id="Seg_3349" s="T219">pro.h:S</ta>
            <ta e="T221" id="Seg_3350" s="T220">v:pred</ta>
            <ta e="T224" id="Seg_3351" s="T223">np.h:S</ta>
            <ta e="T227" id="Seg_3352" s="T226">pro.h:S</ta>
            <ta e="T228" id="Seg_3353" s="T227">v:pred</ta>
            <ta e="T230" id="Seg_3354" s="T229">v:pred 0.3.h:S</ta>
            <ta e="T231" id="Seg_3355" s="T230">v:pred 0.3.h:S</ta>
            <ta e="T235" id="Seg_3356" s="T234">np.h:S</ta>
            <ta e="T236" id="Seg_3357" s="T235">v:pred</ta>
            <ta e="T239" id="Seg_3358" s="T238">np.h:S</ta>
            <ta e="T240" id="Seg_3359" s="T239">v:pred</ta>
            <ta e="T242" id="Seg_3360" s="T241">np:S</ta>
            <ta e="T243" id="Seg_3361" s="T242">v:pred</ta>
            <ta e="T247" id="Seg_3362" s="T246">np.h:S</ta>
            <ta e="T248" id="Seg_3363" s="T247">v:pred</ta>
            <ta e="T250" id="Seg_3364" s="T249">v:pred 0.2.h:S</ta>
            <ta e="T253" id="Seg_3365" s="T252">np:O</ta>
            <ta e="T256" id="Seg_3366" s="T255">ptcl.neg</ta>
            <ta e="T257" id="Seg_3367" s="T256">v:pred 0.1.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T5" id="Seg_3368" s="T4">TURK:disc</ta>
            <ta e="T8" id="Seg_3369" s="T7">RUS:gram</ta>
            <ta e="T17" id="Seg_3370" s="T16">RUS:gram</ta>
            <ta e="T20" id="Seg_3371" s="T19">TURK:disc</ta>
            <ta e="T21" id="Seg_3372" s="T20">RUS:core</ta>
            <ta e="T23" id="Seg_3373" s="T22">TURK:disc</ta>
            <ta e="T34" id="Seg_3374" s="T33">TURK:core</ta>
            <ta e="T36" id="Seg_3375" s="T35">RUS:gram</ta>
            <ta e="T40" id="Seg_3376" s="T39">RUS:gram</ta>
            <ta e="T45" id="Seg_3377" s="T44">TURK:core</ta>
            <ta e="T48" id="Seg_3378" s="T47">RUS:gram</ta>
            <ta e="T52" id="Seg_3379" s="T51">TURK:core</ta>
            <ta e="T60" id="Seg_3380" s="T59">RUS:gram</ta>
            <ta e="T67" id="Seg_3381" s="T66">RUS:gram</ta>
            <ta e="T75" id="Seg_3382" s="T74">RUS:gram</ta>
            <ta e="T81" id="Seg_3383" s="T80">TURK:disc</ta>
            <ta e="T103" id="Seg_3384" s="T102">TURK:disc</ta>
            <ta e="T110" id="Seg_3385" s="T109">RUS:gram</ta>
            <ta e="T117" id="Seg_3386" s="T116">TURK:gram(INDEF)</ta>
            <ta e="T119" id="Seg_3387" s="T118">TURK:core</ta>
            <ta e="T130" id="Seg_3388" s="T129">RUS:disc</ta>
            <ta e="T133" id="Seg_3389" s="T132">TURK:gram(INDEF)</ta>
            <ta e="T147" id="Seg_3390" s="T146">RUS:cult</ta>
            <ta e="T150" id="Seg_3391" s="T149">RUS:gram</ta>
            <ta e="T155" id="Seg_3392" s="T154">RUS:gram</ta>
            <ta e="T167" id="Seg_3393" s="T166">RUS:core</ta>
            <ta e="T177" id="Seg_3394" s="T176">RUS:gram(INDEF)</ta>
            <ta e="T178" id="Seg_3395" s="T177">TURK:cult</ta>
            <ta e="T179" id="Seg_3396" s="T178">RUS:gram</ta>
            <ta e="T180" id="Seg_3397" s="T179">RUS:mod</ta>
            <ta e="T183" id="Seg_3398" s="T182">RUS:gram</ta>
            <ta e="T193" id="Seg_3399" s="T192">RUS:gram</ta>
            <ta e="T197" id="Seg_3400" s="T196">RUS:gram</ta>
            <ta e="T204" id="Seg_3401" s="T203">RUS:core</ta>
            <ta e="T211" id="Seg_3402" s="T210">RUS:gram</ta>
            <ta e="T213" id="Seg_3403" s="T211">RUS:core</ta>
            <ta e="T225" id="Seg_3404" s="T224">RUS:gram</ta>
            <ta e="T237" id="Seg_3405" s="T236">RUS:gram</ta>
            <ta e="T242" id="Seg_3406" s="T241">RUS:core</ta>
            <ta e="T244" id="Seg_3407" s="T243">RUS:gram</ta>
            <ta e="T252" id="Seg_3408" s="T251">RUS:core</ta>
            <ta e="T254" id="Seg_3409" s="T253">TURK:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T3" id="Seg_3410" s="T2">RUS:int.ins</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_3411" s="T0">Человек шел по степи.</ta>
            <ta e="T7" id="Seg_3412" s="T3">Видит: огонь лежит [= горит].</ta>
            <ta e="T11" id="Seg_3413" s="T7">А в огне лежит змея.</ta>
            <ta e="T16" id="Seg_3414" s="T11">Он взял палку, вытащил ее.</ta>
            <ta e="T21" id="Seg_3415" s="T16">А она [прыгнулаъ ему на шею.</ta>
            <ta e="T26" id="Seg_3416" s="T21">Она его стала душить.</ta>
            <ta e="T30" id="Seg_3417" s="T26">"Зачем ты меня душишь?</ta>
            <ta e="T39" id="Seg_3418" s="T30">Я тебе сделал добро, а ты меня [хочешь убить]".</ta>
            <ta e="T53" id="Seg_3419" s="T39">А она говорит: "Ты сделал добро, а я тебе не сделала добро.</ta>
            <ta e="T54" id="Seg_3420" s="T53">Пойдем.</ta>
            <ta e="T56" id="Seg_3421" s="T54">Спросим у быка".</ta>
            <ta e="T59" id="Seg_3422" s="T56">Бык там ходит.</ta>
            <ta e="T66" id="Seg_3423" s="T59">Бык говорит: "Я работал, пахал".</ta>
            <ta e="T71" id="Seg_3424" s="T66">А теперь я не могу работать.</ta>
            <ta e="T74" id="Seg_3425" s="T71">Меня выгнали есть. [?]</ta>
            <ta e="T79" id="Seg_3426" s="T74">Чтобы меня съели медведи и собаки.</ta>
            <ta e="T83" id="Seg_3427" s="T79">Люди [всегда] так делают.</ta>
            <ta e="T87" id="Seg_3428" s="T83">Потом: "Пойдем, спрос[ил] у лошади".</ta>
            <ta e="T89" id="Seg_3429" s="T87">Они идут.</ta>
            <ta e="T95" id="Seg_3430" s="T89">Потом змея (говорит?): "Пойдем к лошади спросим".</ta>
            <ta e="T98" id="Seg_3431" s="T95">Они идут.</ta>
            <ta e="T100" id="Seg_3432" s="T98">Лошадь лежит.</ta>
            <ta e="T103" id="Seg_3433" s="T100">У нее нет глаза.</ta>
            <ta e="T105" id="Seg_3434" s="T103">Ей больно. [?]</ta>
            <ta e="T107" id="Seg_3435" s="T105">Она говорит:</ta>
            <ta e="T114" id="Seg_3436" s="T107">"Я работала-работала, и видишь, теперь меня выбросили.</ta>
            <ta e="T122" id="Seg_3437" s="T114">Что за люди, ничего хорошего мне не дают".</ta>
            <ta e="T127" id="Seg_3438" s="T122">Тогда [змея говорит]: "Видишь, что говорит лошадь!"</ta>
            <ta e="T135" id="Seg_3439" s="T127">Потом они ушли: "Ну, я ничего тебе не сделаю.</ta>
            <ta e="T140" id="Seg_3440" s="T135">Пойдем к камням".</ta>
            <ta e="T141" id="Seg_3441" s="T140">Они пришли.</ta>
            <ta e="T147" id="Seg_3442" s="T141">Змея заползла в нору.</ta>
            <ta e="T154" id="Seg_3443" s="T147">Он стоит и [смотрит]: "Однако много [змей] вылезает. [?]</ta>
            <ta e="T158" id="Seg_3444" s="T154">Они меня съедят!" [?]</ta>
            <ta e="T165" id="Seg_3445" s="T158">Тогда та змея принесла ему одну (монетку?).</ta>
            <ta e="T169" id="Seg_3446" s="T165">Давала каждый день, ходила.</ta>
            <ta e="T172" id="Seg_3447" s="T169">Много давала, давал.</ta>
            <ta e="T178" id="Seg_3448" s="T172">Потом он говорит: "Я пойду что-нибудь продам.</ta>
            <ta e="T184" id="Seg_3449" s="T178">А вот мой сын, пусть он ходит [за деньгами].</ta>
            <ta e="T187" id="Seg_3450" s="T184">Его сын пришел.</ta>
            <ta e="T191" id="Seg_3451" s="T187">Сколько-то дней ходил-ходил.</ta>
            <ta e="T196" id="Seg_3452" s="T191">Потом [думает]: "Давай я убью эту змею.</ta>
            <ta e="T207" id="Seg_3453" s="T196">И возьму там много денег, зачем ходить туда каждый день".</ta>
            <ta e="T213" id="Seg_3454" s="T207">Он ударил ее и [отрезал ей] хвост.</ta>
            <ta e="T218" id="Seg_3455" s="T213">Она убил его, эта змея.</ta>
            <ta e="T224" id="Seg_3456" s="T218">Потом [отец] пришел: "Где мой сын?"</ta>
            <ta e="T230" id="Seg_3457" s="T224">А [ему] говорят: "Твоего сына нет".</ta>
            <ta e="T234" id="Seg_3458" s="T230">Он пошел к тем камням.</ta>
            <ta e="T236" id="Seg_3459" s="T234">Его сын лежит.</ta>
            <ta e="T248" id="Seg_3460" s="T236">А змея приползла: "У меня нет хвоста, а у тебя нет сына".</ta>
            <ta e="T253" id="Seg_3461" s="T248">"Ну, давай мне [деньги] каждый день".</ta>
            <ta e="T259" id="Seg_3462" s="T253">"Нет, теперь я не буду тебе давать".</ta>
            <ta e="T260" id="Seg_3463" s="T259">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_3464" s="T0">A man comes along the steppe.</ta>
            <ta e="T7" id="Seg_3465" s="T3">He sees: a fire is lying [= burning].</ta>
            <ta e="T11" id="Seg_3466" s="T7">But in the fire a snake is lying.</ta>
            <ta e="T16" id="Seg_3467" s="T11">He took a [piece of] wood, took it out.</ta>
            <ta e="T21" id="Seg_3468" s="T16">But it [jumped] to its neck.</ta>
            <ta e="T26" id="Seg_3469" s="T21">Then it started to strangle him.</ta>
            <ta e="T30" id="Seg_3470" s="T26">"Why are you strangling me?</ta>
            <ta e="T39" id="Seg_3471" s="T30">I did good to you, and you [want to kill] me."</ta>
            <ta e="T53" id="Seg_3472" s="T39">And it says: "You did good [to me], but I did not do good to you.</ta>
            <ta e="T54" id="Seg_3473" s="T53">Let's go.</ta>
            <ta e="T56" id="Seg_3474" s="T54">Let's ask the bull."</ta>
            <ta e="T59" id="Seg_3475" s="T56">The bull is going around there.</ta>
            <ta e="T66" id="Seg_3476" s="T59">The bull says: "I worked, I ploughed.</ta>
            <ta e="T71" id="Seg_3477" s="T66">And now I cannot work.</ta>
            <ta e="T74" id="Seg_3478" s="T71">They drove me out, so that I eat. [?]</ta>
            <ta e="T79" id="Seg_3479" s="T74">So that the bears, the dogs would eat me.</ta>
            <ta e="T83" id="Seg_3480" s="T79">People [always] do so."</ta>
            <ta e="T87" id="Seg_3481" s="T83">Then: let’s go, ask[ed] the horse."</ta>
            <ta e="T89" id="Seg_3482" s="T87">Then they come.</ta>
            <ta e="T95" id="Seg_3483" s="T89">Then the snake is (saying?): "Let's go and ask the horse."</ta>
            <ta e="T98" id="Seg_3484" s="T95">Then they are coming.</ta>
            <ta e="T100" id="Seg_3485" s="T98">The horse is lying.</ta>
            <ta e="T103" id="Seg_3486" s="T100">It has only one eye.</ta>
            <ta e="T105" id="Seg_3487" s="T103">It hurts. [?]</ta>
            <ta e="T107" id="Seg_3488" s="T105">It says:</ta>
            <ta e="T114" id="Seg_3489" s="T107">"I worked, I worked, and you see, now [they] threw me out.</ta>
            <ta e="T122" id="Seg_3490" s="T114">What a people, they don't give me anything good."</ta>
            <ta e="T127" id="Seg_3491" s="T122">Then [the snake says]: "See how the horse says!"</ta>
            <ta e="T135" id="Seg_3492" s="T127">Then they went: "Well, I will not do anything to you.</ta>
            <ta e="T140" id="Seg_3493" s="T135">Let's go to the stones.</ta>
            <ta e="T141" id="Seg_3494" s="T140">They came.</ta>
            <ta e="T147" id="Seg_3495" s="T141">Then the snake went into the burrow.</ta>
            <ta e="T154" id="Seg_3496" s="T147">He stands and [sees]: "Many [snakes] are coming out. [?]</ta>
            <ta e="T158" id="Seg_3497" s="T154">They'll eat me!" [?]</ta>
            <ta e="T165" id="Seg_3498" s="T158">Then the snake brought him one (money?).</ta>
            <ta e="T169" id="Seg_3499" s="T165">It gave every day, went.</ta>
            <ta e="T172" id="Seg_3500" s="T169">It brought [and] it brought a lot.</ta>
            <ta e="T178" id="Seg_3501" s="T172">Then he says: "I will go to sell something.</ta>
            <ta e="T184" id="Seg_3502" s="T178">Here is my son, let him go [to collect money].</ta>
            <ta e="T187" id="Seg_3503" s="T184">His son came.</ta>
            <ta e="T191" id="Seg_3504" s="T187">How many days he went, he went.</ta>
            <ta e="T196" id="Seg_3505" s="T191">Then [he thinks]: "Let me kill this snake."</ta>
            <ta e="T207" id="Seg_3506" s="T196">And I'll take a lot of money from there, why to go there every day."</ta>
            <ta e="T213" id="Seg_3507" s="T207">He hit it and [cut off] its tail.</ta>
            <ta e="T218" id="Seg_3508" s="T213">It killed him, the snake.</ta>
            <ta e="T224" id="Seg_3509" s="T218">Then [the father] came: "Where is my son?"</ta>
            <ta e="T230" id="Seg_3510" s="T224">[People] tell him: "Your son is not [here].</ta>
            <ta e="T234" id="Seg_3511" s="T230">He came there to the stones.</ta>
            <ta e="T236" id="Seg_3512" s="T234">His son is lying.</ta>
            <ta e="T248" id="Seg_3513" s="T236">And the snake came: "I have no tail, and you have no son.</ta>
            <ta e="T253" id="Seg_3514" s="T248">"Well, give me [money] every day."</ta>
            <ta e="T259" id="Seg_3515" s="T253">"No, now I will not give [it] to you."</ta>
            <ta e="T260" id="Seg_3516" s="T259">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_3517" s="T0">Ein Mann kommt durch die Steppe.</ta>
            <ta e="T7" id="Seg_3518" s="T3">Er sieht: ein Feuer liegt [= brennt].</ta>
            <ta e="T11" id="Seg_3519" s="T7">Aber im Feuer liegt eine Schlange.</ta>
            <ta e="T16" id="Seg_3520" s="T11">Er nahm ein [Stück] Holz und nahm sie heraus.</ta>
            <ta e="T21" id="Seg_3521" s="T16">Aber sie [sprang] ihm an den Hals.</ta>
            <ta e="T26" id="Seg_3522" s="T21">Dann fing sie an, ihn zu würgen.</ta>
            <ta e="T30" id="Seg_3523" s="T26">"Warum würgst du mich?</ta>
            <ta e="T39" id="Seg_3524" s="T30">Ich habe dir Gutes getan und du [möchtest] mich [töten]."</ta>
            <ta e="T53" id="Seg_3525" s="T39">Und sie sagt: "Du hast [mir] Gutes getan, aber ich habe dir nichts Gutes getan.</ta>
            <ta e="T54" id="Seg_3526" s="T53">Lass uns gehen.</ta>
            <ta e="T56" id="Seg_3527" s="T54">Lass uns den Bullen fragen."</ta>
            <ta e="T59" id="Seg_3528" s="T56">Der Bulle geht dort umher.</ta>
            <ta e="T66" id="Seg_3529" s="T59">Der Bulle sagt: "Ich arbeitete, ich pflügte.</ta>
            <ta e="T71" id="Seg_3530" s="T66">Und jetzt kann ich nicht mehr arbeiten.</ta>
            <ta e="T74" id="Seg_3531" s="T71">Sie brachten mich hinaus damit ich esse. [?]</ta>
            <ta e="T79" id="Seg_3532" s="T74">Damit die Bären, die Hunde mich fressen.</ta>
            <ta e="T83" id="Seg_3533" s="T79">Menschen machen es [immer] so."</ta>
            <ta e="T87" id="Seg_3534" s="T83">Dann: "Lass uns gehen, frag[te] das Pferd."</ta>
            <ta e="T89" id="Seg_3535" s="T87">Dann kamen sie.</ta>
            <ta e="T95" id="Seg_3536" s="T89">Dann (sagt?) die Schlange: "Lass uns gehen und das Pferd fragen."</ta>
            <ta e="T98" id="Seg_3537" s="T95">Dann kommen sie.</ta>
            <ta e="T100" id="Seg_3538" s="T98">Das Pferd liegt da.</ta>
            <ta e="T103" id="Seg_3539" s="T100">Es hat nur ein Auge.</ta>
            <ta e="T105" id="Seg_3540" s="T103">Es hat Schmerzen. [?]</ta>
            <ta e="T107" id="Seg_3541" s="T105">Es sagt:</ta>
            <ta e="T114" id="Seg_3542" s="T107">"Ich arbeitete, ich arbeitete, und du siehst, jetzt haben [sie] mich rausgeworfen.</ta>
            <ta e="T122" id="Seg_3543" s="T114">Was für Leute, sie geben mir nichts Gutes. "</ta>
            <ta e="T127" id="Seg_3544" s="T122">Dann [sagt die Schlange]: "Sieh, was das Pferd sagt!"</ta>
            <ta e="T135" id="Seg_3545" s="T127">Dann gingen sie: "Nun, ich werde dir nichts tun.</ta>
            <ta e="T140" id="Seg_3546" s="T135">Lass uns zu den Steinen gehen."</ta>
            <ta e="T141" id="Seg_3547" s="T140">Sie kamen.</ta>
            <ta e="T147" id="Seg_3548" s="T141">Dann kroch die Schlange in die Höhle.</ta>
            <ta e="T154" id="Seg_3549" s="T147">Er steht und [sieht]: "Viele [Schlangen] kommen heraus. [?]</ta>
            <ta e="T158" id="Seg_3550" s="T154">Sie werden mich fressen!" [?]</ta>
            <ta e="T165" id="Seg_3551" s="T158">Dann brachte die Schlange ihm ein [Stück] (Geld?).</ta>
            <ta e="T169" id="Seg_3552" s="T165">Sie gab jeden Tag, ging.</ta>
            <ta e="T172" id="Seg_3553" s="T169">Sie brachte [und] brachte viel.</ta>
            <ta e="T178" id="Seg_3554" s="T172">Dann sagt er: "Ich werde gehen und etwas verkaufen.</ta>
            <ta e="T184" id="Seg_3555" s="T178">Hier ist mein Sohn, lass ihn gehen [um Geld zu sammeln].</ta>
            <ta e="T187" id="Seg_3556" s="T184">Sein Sohn kam.</ta>
            <ta e="T191" id="Seg_3557" s="T187">Wie viele Tage ging er, ging er.</ta>
            <ta e="T196" id="Seg_3558" s="T191">Dann [denkt er]: "Lass mich diese Schlange töten."</ta>
            <ta e="T207" id="Seg_3559" s="T196">Und ich werde dort viel Geld nehmen, warum jeden Tag dorthin gehen."</ta>
            <ta e="T213" id="Seg_3560" s="T207">Er schlug sie und [schnitt] ihren Schwanz ab.</ta>
            <ta e="T218" id="Seg_3561" s="T213">Sie tötete ihn, die Schlange.</ta>
            <ta e="T224" id="Seg_3562" s="T218">Dann kam [der Vater]: "Wo ist mein Sohn?"</ta>
            <ta e="T230" id="Seg_3563" s="T224">[Leute] sagen ihm: "Dein Sohn ist nicht [hier].</ta>
            <ta e="T234" id="Seg_3564" s="T230">Er kam dorthin zu den Steine.</ta>
            <ta e="T236" id="Seg_3565" s="T234">Sein Sohn liegt da.</ta>
            <ta e="T248" id="Seg_3566" s="T236">Und die Schlange kam: "Ich habe keinen Schwanz und du hast keinen Sohn."</ta>
            <ta e="T253" id="Seg_3567" s="T248">"Nun, gib mir [Geld] jeden Tag."</ta>
            <ta e="T259" id="Seg_3568" s="T253">"Nein, jetzt gebe ich [es] dir nicht [mehr]."</ta>
            <ta e="T260" id="Seg_3569" s="T259">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T3" id="Seg_3570" s="T0">[KlT:] Ru. INS. [GVY:] The first part of this text is a part of a Buryat tale about a lazy man: https://librolife.ru/g6192014. The second part is a story about a snake giving money to a man, see e.g. https://ir-cherry.livejournal.com/136136.html.</ta>
            <ta e="T53" id="Seg_3571" s="T39">[GVY:] It should mean "I didn't ask you to do good to me".</ta>
            <ta e="T66" id="Seg_3572" s="T59">[KlT:] Tajər- ’plough’ contaminated form.</ta>
            <ta e="T74" id="Seg_3573" s="T71">[GVY:] amzittə probably is the beginning of an unfinished sentence.</ta>
            <ta e="T87" id="Seg_3574" s="T83">[GVY:] surarluʔpi - should be surarlubəj.</ta>
            <ta e="T140" id="Seg_3575" s="T135">[GVY:] Here begins another story about a snake living in a heap of stones, see https://ir-cherry.livejournal.com/136136.html.</ta>
            <ta e="T154" id="Seg_3576" s="T147">[GVY:] The interpretation of this and the following sentence is hypothetical.</ta>
            <ta e="T158" id="Seg_3577" s="T154">[AAV] amno- "sit/live" instead of am- "eat"?</ta>
            <ta e="T165" id="Seg_3578" s="T158">[GVY:] aktʼi = aktʼa?</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
