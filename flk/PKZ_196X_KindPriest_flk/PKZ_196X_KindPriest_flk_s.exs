<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDD2B5841B-5C39-5993-2816-01C65B2D1F37">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_KindPriest_flk.wav" />
         <referenced-file url="PKZ_196X_KindPriest_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_KindPriest_flk\PKZ_196X_KindPriest_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">146</ud-information>
            <ud-information attribute-name="# HIAT:w">95</ud-information>
            <ud-information attribute-name="# e">95</ud-information>
            <ud-information attribute-name="# HIAT:u">21</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.06" type="appl" />
         <tli id="T1" time="0.813" type="appl" />
         <tli id="T2" time="1.567" type="appl" />
         <tli id="T3" time="2.32" type="appl" />
         <tli id="T4" time="2.945" type="appl" />
         <tli id="T5" time="3.571" type="appl" />
         <tli id="T6" time="4.846666666666667" />
         <tli id="T7" time="5.739" type="appl" />
         <tli id="T8" time="6.619" type="appl" />
         <tli id="T9" time="7.103" type="appl" />
         <tli id="T10" time="7.587" type="appl" />
         <tli id="T11" time="8.071" type="appl" />
         <tli id="T12" time="8.555" type="appl" />
         <tli id="T13" time="9.039" type="appl" />
         <tli id="T14" time="9.733333333333334" />
         <tli id="T15" time="10.56" type="appl" />
         <tli id="T16" time="11.252" type="appl" />
         <tli id="T17" time="11.945" type="appl" />
         <tli id="T18" time="12.637" type="appl" />
         <tli id="T19" time="13.329" type="appl" />
         <tli id="T20" time="14.291" type="appl" />
         <tli id="T21" time="15.046" type="appl" />
         <tli id="T22" time="15.802" type="appl" />
         <tli id="T23" time="17.113333333333333" />
         <tli id="T24" time="18.102" type="appl" />
         <tli id="T25" time="19.453333333333333" />
         <tli id="T26" time="20.258" type="appl" />
         <tli id="T27" time="21.117" type="appl" />
         <tli id="T28" time="21.976" type="appl" />
         <tli id="T29" time="22.835" type="appl" />
         <tli id="T30" time="23.694" type="appl" />
         <tli id="T31" time="24.554" type="appl" />
         <tli id="T32" time="25.413" type="appl" />
         <tli id="T33" time="26.272" type="appl" />
         <tli id="T34" time="27.131" type="appl" />
         <tli id="T35" time="28.60666666666667" />
         <tli id="T36" time="29.362" type="appl" />
         <tli id="T37" time="30.079" type="appl" />
         <tli id="T38" time="30.796" type="appl" />
         <tli id="T39" time="31.513" type="appl" />
         <tli id="T40" time="32.23" type="appl" />
         <tli id="T41" time="33.452" type="appl" />
         <tli id="T42" time="34.495" type="appl" />
         <tli id="T43" time="35.537" type="appl" />
         <tli id="T44" time="36.579" type="appl" />
         <tli id="T45" time="37.622" type="appl" />
         <tli id="T46" time="38.664" type="appl" />
         <tli id="T47" time="39.707" type="appl" />
         <tli id="T48" time="41.54666666666667" />
         <tli id="T49" time="42.537" type="appl" />
         <tli id="T50" time="43.72666666666667" />
         <tli id="T51" time="44.429" type="appl" />
         <tli id="T52" time="45.092" type="appl" />
         <tli id="T53" time="45.756" type="appl" />
         <tli id="T54" time="46.419" type="appl" />
         <tli id="T55" time="47.083" type="appl" />
         <tli id="T56" time="48.39" type="appl" />
         <tli id="T57" time="49.697" type="appl" />
         <tli id="T58" time="51.26" />
         <tli id="T59" time="51.995" type="appl" />
         <tli id="T60" time="52.658" type="appl" />
         <tli id="T61" time="53.32" type="appl" />
         <tli id="T62" time="53.983" type="appl" />
         <tli id="T63" time="54.645" type="appl" />
         <tli id="T64" time="55.308" type="appl" />
         <tli id="T65" time="55.97" type="appl" />
         <tli id="T66" time="56.45" type="appl" />
         <tli id="T67" time="56.931" type="appl" />
         <tli id="T68" time="57.411" type="appl" />
         <tli id="T69" time="57.892" type="appl" />
         <tli id="T70" time="58.372" type="appl" />
         <tli id="T71" time="58.962" type="appl" />
         <tli id="T72" time="59.552" type="appl" />
         <tli id="T73" time="60.143" type="appl" />
         <tli id="T74" time="60.733" type="appl" />
         <tli id="T75" time="61.323" type="appl" />
         <tli id="T76" time="61.914" type="appl" />
         <tli id="T77" time="62.504" type="appl" />
         <tli id="T78" time="63.094" type="appl" />
         <tli id="T79" time="63.95" type="appl" />
         <tli id="T80" time="64.806" type="appl" />
         <tli id="T81" time="65.662" type="appl" />
         <tli id="T82" time="66.883" type="appl" />
         <tli id="T83" time="67.926" type="appl" />
         <tli id="T84" time="68.689" type="appl" />
         <tli id="T85" time="69.451" type="appl" />
         <tli id="T86" time="70.214" type="appl" />
         <tli id="T87" time="70.977" type="appl" />
         <tli id="T88" time="71.74" type="appl" />
         <tli id="T89" time="72.502" type="appl" />
         <tli id="T90" time="73.91333333333333" />
         <tli id="T91" time="74.57" type="appl" />
         <tli id="T92" time="75.166" type="appl" />
         <tli id="T93" time="75.762" type="appl" />
         <tli id="T94" time="76.359" type="appl" />
         <tli id="T95" time="77.424" type="appl" />
         <tli id="T96" time="77.6" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T95" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <nts id="Seg_3" n="HIAT:ip">(</nts>
                  <ts e="T1" id="Seg_5" n="HIAT:w" s="T0">Onʼ-</ts>
                  <nts id="Seg_6" n="HIAT:ip">)</nts>
                  <nts id="Seg_7" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_9" n="HIAT:w" s="T1">Amnobi</ts>
                  <nts id="Seg_10" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_12" n="HIAT:w" s="T2">abəs</ts>
                  <nts id="Seg_13" n="HIAT:ip">.</nts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T6" id="Seg_16" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_18" n="HIAT:w" s="T3">Onʼiʔ</ts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_21" n="HIAT:w" s="T4">dĭ</ts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_24" n="HIAT:w" s="T5">kambi</ts>
                  <nts id="Seg_25" n="HIAT:ip">.</nts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_28" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_30" n="HIAT:w" s="T6">Kuza</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_33" n="HIAT:w" s="T7">deʔpi</ts>
                  <nts id="Seg_34" n="HIAT:ip">.</nts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_37" n="HIAT:u" s="T8">
                  <nts id="Seg_38" n="HIAT:ip">"</nts>
                  <ts e="T9" id="Seg_40" n="HIAT:w" s="T8">Amnoʔ</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_43" n="HIAT:w" s="T9">măna</ts>
                  <nts id="Seg_44" n="HIAT:ip">,</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_47" n="HIAT:w" s="T10">măn</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_50" n="HIAT:w" s="T11">tănan</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_53" n="HIAT:w" s="T12">aktʼa</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_56" n="HIAT:w" s="T13">mĭlem</ts>
                  <nts id="Seg_57" n="HIAT:ip">"</nts>
                  <nts id="Seg_58" n="HIAT:ip">.</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_61" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_63" n="HIAT:w" s="T14">Dĭ</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_66" n="HIAT:w" s="T15">amnobi</ts>
                  <nts id="Seg_67" n="HIAT:ip">,</nts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_70" n="HIAT:w" s="T16">amnobi</ts>
                  <nts id="Seg_71" n="HIAT:ip">,</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_74" n="HIAT:w" s="T17">onʼiʔ</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_77" n="HIAT:w" s="T18">nʼedʼelʼə</ts>
                  <nts id="Seg_78" n="HIAT:ip">.</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_81" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_83" n="HIAT:w" s="T19">Dĭgəttə:</ts>
                  <nts id="Seg_84" n="HIAT:ip">"</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_87" n="HIAT:w" s="T20">Kanžəbəj</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_90" n="HIAT:w" s="T21">noʔ</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_93" n="HIAT:w" s="T22">jaʔsittə</ts>
                  <nts id="Seg_94" n="HIAT:ip">!</nts>
                  <nts id="Seg_95" n="HIAT:ip">"</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_98" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_100" n="HIAT:w" s="T23">Erten</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_103" n="HIAT:w" s="T24">uʔbdəbiʔi</ts>
                  <nts id="Seg_104" n="HIAT:ip">.</nts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_107" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_109" n="HIAT:w" s="T25">Măndə</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_112" n="HIAT:w" s="T26">iandə:</ts>
                  <nts id="Seg_113" n="HIAT:ip">"</nts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_116" n="HIAT:w" s="T27">Deʔ</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_119" n="HIAT:w" s="T28">miʔnʼibeʔ</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_122" n="HIAT:w" s="T29">amzittə</ts>
                  <nts id="Seg_123" n="HIAT:ip">,</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_126" n="HIAT:w" s="T30">miʔ</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_129" n="HIAT:w" s="T31">kanžəbəj</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_132" n="HIAT:w" s="T32">noʔ</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_135" n="HIAT:w" s="T33">jaʔsittə</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_138" n="HIAT:w" s="T34">šapkuziʔ</ts>
                  <nts id="Seg_139" n="HIAT:ip">"</nts>
                  <nts id="Seg_140" n="HIAT:ip">.</nts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_143" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_145" n="HIAT:w" s="T35">Dĭ</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_148" n="HIAT:w" s="T36">mĭbi</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_151" n="HIAT:w" s="T37">dĭzeŋdə</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_153" n="HIAT:ip">(</nts>
                  <ts e="T39" id="Seg_155" n="HIAT:w" s="T38">m-</ts>
                  <nts id="Seg_156" n="HIAT:ip">)</nts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_159" n="HIAT:w" s="T39">amzittə</ts>
                  <nts id="Seg_160" n="HIAT:ip">.</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_163" n="HIAT:u" s="T40">
                  <nts id="Seg_164" n="HIAT:ip">"</nts>
                  <nts id="Seg_165" n="HIAT:ip">(</nts>
                  <ts e="T41" id="Seg_167" n="HIAT:w" s="T40">Davaj</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_170" n="HIAT:w" s="T41">i-</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_173" n="HIAT:w" s="T42">kuza-</ts>
                  <nts id="Seg_174" n="HIAT:ip">)</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_177" n="HIAT:w" s="T43">Davaj</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_180" n="HIAT:w" s="T44">amzittə</ts>
                  <nts id="Seg_181" n="HIAT:ip">,</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_184" n="HIAT:w" s="T45">davaj</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_187" n="HIAT:w" s="T46">iššo</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_190" n="HIAT:w" s="T47">amnobəj</ts>
                  <nts id="Seg_191" n="HIAT:ip">"</nts>
                  <nts id="Seg_192" n="HIAT:ip">.</nts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_195" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_197" n="HIAT:w" s="T48">Dĭgəttə</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_200" n="HIAT:w" s="T49">amnobiʔi</ts>
                  <nts id="Seg_201" n="HIAT:ip">.</nts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_204" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_206" n="HIAT:w" s="T50">Dĭgəttə:</ts>
                  <nts id="Seg_207" n="HIAT:ip">"</nts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_210" n="HIAT:w" s="T51">Davaj</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_213" n="HIAT:w" s="T52">iššo</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_216" n="HIAT:w" s="T53">nüdʼigən</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_219" n="HIAT:w" s="T54">amzittə</ts>
                  <nts id="Seg_220" n="HIAT:ip">"</nts>
                  <nts id="Seg_221" n="HIAT:ip">.</nts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_224" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_226" n="HIAT:w" s="T55">Dĭgəttə</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_229" n="HIAT:w" s="T56">ambiʔi</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_231" n="HIAT:ip">(</nts>
                  <ts e="T58" id="Seg_233" n="HIAT:w" s="T57">šalaʔjə</ts>
                  <nts id="Seg_234" n="HIAT:ip">)</nts>
                  <nts id="Seg_235" n="HIAT:ip">.</nts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_238" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_240" n="HIAT:w" s="T58">Dĭ</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_243" n="HIAT:w" s="T59">kuza</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_246" n="HIAT:w" s="T60">iliet</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_249" n="HIAT:w" s="T61">oldʼabə</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_252" n="HIAT:w" s="T62">da</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_255" n="HIAT:w" s="T63">kunolzittə</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_258" n="HIAT:w" s="T64">kandəga</ts>
                  <nts id="Seg_259" n="HIAT:ip">.</nts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_262" n="HIAT:u" s="T65">
                  <nts id="Seg_263" n="HIAT:ip">"</nts>
                  <ts e="T66" id="Seg_265" n="HIAT:w" s="T65">A</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_268" n="HIAT:w" s="T66">ĭmbi</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_271" n="HIAT:w" s="T67">tăn</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_274" n="HIAT:w" s="T68">kunolzittə</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_277" n="HIAT:w" s="T69">kandəgal</ts>
                  <nts id="Seg_278" n="HIAT:ip">?</nts>
                  <nts id="Seg_279" n="HIAT:ip">"</nts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_282" n="HIAT:u" s="T70">
                  <nts id="Seg_283" n="HIAT:ip">"</nts>
                  <ts e="T71" id="Seg_285" n="HIAT:w" s="T70">A</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_288" n="HIAT:w" s="T71">tăn</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_291" n="HIAT:w" s="T72">boslə</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_294" n="HIAT:w" s="T73">dăre</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_297" n="HIAT:w" s="T74">nörbəbiel</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_300" n="HIAT:w" s="T75">što</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_303" n="HIAT:w" s="T76">kunolzittə</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_306" n="HIAT:w" s="T77">nada</ts>
                  <nts id="Seg_307" n="HIAT:ip">"</nts>
                  <nts id="Seg_308" n="HIAT:ip">.</nts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_311" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_313" n="HIAT:w" s="T78">I</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_316" n="HIAT:w" s="T79">kambi</ts>
                  <nts id="Seg_317" n="HIAT:ip">,</nts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_320" n="HIAT:w" s="T80">kunolbi</ts>
                  <nts id="Seg_321" n="HIAT:ip">.</nts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_324" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_326" n="HIAT:w" s="T81">I</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_328" n="HIAT:ip">(</nts>
                  <ts e="T83" id="Seg_330" n="HIAT:w" s="T82">ertegəʔ</ts>
                  <nts id="Seg_331" n="HIAT:ip">)</nts>
                  <nts id="Seg_332" n="HIAT:ip">.</nts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_335" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_337" n="HIAT:w" s="T83">Dĭgəttə</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_340" n="HIAT:w" s="T84">abəs</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_343" n="HIAT:w" s="T85">tüj</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_346" n="HIAT:w" s="T86">ej</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_349" n="HIAT:w" s="T87">mĭlie</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_352" n="HIAT:w" s="T88">dĭʔnə</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_355" n="HIAT:w" s="T89">amzittə</ts>
                  <nts id="Seg_356" n="HIAT:ip">.</nts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_359" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_361" n="HIAT:w" s="T90">I</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_364" n="HIAT:w" s="T91">mʼegluʔpi</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_367" n="HIAT:w" s="T92">dĭ</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_370" n="HIAT:w" s="T93">abəsdə</ts>
                  <nts id="Seg_371" n="HIAT:ip">.</nts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_374" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_376" n="HIAT:w" s="T94">Kabarləj</ts>
                  <nts id="Seg_377" n="HIAT:ip">.</nts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T95" id="Seg_379" n="sc" s="T0">
               <ts e="T1" id="Seg_381" n="e" s="T0">(Onʼ-) </ts>
               <ts e="T2" id="Seg_383" n="e" s="T1">Amnobi </ts>
               <ts e="T3" id="Seg_385" n="e" s="T2">abəs. </ts>
               <ts e="T4" id="Seg_387" n="e" s="T3">Onʼiʔ </ts>
               <ts e="T5" id="Seg_389" n="e" s="T4">dĭ </ts>
               <ts e="T6" id="Seg_391" n="e" s="T5">kambi. </ts>
               <ts e="T7" id="Seg_393" n="e" s="T6">Kuza </ts>
               <ts e="T8" id="Seg_395" n="e" s="T7">deʔpi. </ts>
               <ts e="T9" id="Seg_397" n="e" s="T8">"Amnoʔ </ts>
               <ts e="T10" id="Seg_399" n="e" s="T9">măna, </ts>
               <ts e="T11" id="Seg_401" n="e" s="T10">măn </ts>
               <ts e="T12" id="Seg_403" n="e" s="T11">tănan </ts>
               <ts e="T13" id="Seg_405" n="e" s="T12">aktʼa </ts>
               <ts e="T14" id="Seg_407" n="e" s="T13">mĭlem". </ts>
               <ts e="T15" id="Seg_409" n="e" s="T14">Dĭ </ts>
               <ts e="T16" id="Seg_411" n="e" s="T15">amnobi, </ts>
               <ts e="T17" id="Seg_413" n="e" s="T16">amnobi, </ts>
               <ts e="T18" id="Seg_415" n="e" s="T17">onʼiʔ </ts>
               <ts e="T19" id="Seg_417" n="e" s="T18">nʼedʼelʼə. </ts>
               <ts e="T20" id="Seg_419" n="e" s="T19">Dĭgəttə:" </ts>
               <ts e="T21" id="Seg_421" n="e" s="T20">Kanžəbəj </ts>
               <ts e="T22" id="Seg_423" n="e" s="T21">noʔ </ts>
               <ts e="T23" id="Seg_425" n="e" s="T22">jaʔsittə!" </ts>
               <ts e="T24" id="Seg_427" n="e" s="T23">Erten </ts>
               <ts e="T25" id="Seg_429" n="e" s="T24">uʔbdəbiʔi. </ts>
               <ts e="T26" id="Seg_431" n="e" s="T25">Măndə </ts>
               <ts e="T27" id="Seg_433" n="e" s="T26">iandə:" </ts>
               <ts e="T28" id="Seg_435" n="e" s="T27">Deʔ </ts>
               <ts e="T29" id="Seg_437" n="e" s="T28">miʔnʼibeʔ </ts>
               <ts e="T30" id="Seg_439" n="e" s="T29">amzittə, </ts>
               <ts e="T31" id="Seg_441" n="e" s="T30">miʔ </ts>
               <ts e="T32" id="Seg_443" n="e" s="T31">kanžəbəj </ts>
               <ts e="T33" id="Seg_445" n="e" s="T32">noʔ </ts>
               <ts e="T34" id="Seg_447" n="e" s="T33">jaʔsittə </ts>
               <ts e="T35" id="Seg_449" n="e" s="T34">šapkuziʔ". </ts>
               <ts e="T36" id="Seg_451" n="e" s="T35">Dĭ </ts>
               <ts e="T37" id="Seg_453" n="e" s="T36">mĭbi </ts>
               <ts e="T38" id="Seg_455" n="e" s="T37">dĭzeŋdə </ts>
               <ts e="T39" id="Seg_457" n="e" s="T38">(m-) </ts>
               <ts e="T40" id="Seg_459" n="e" s="T39">amzittə. </ts>
               <ts e="T41" id="Seg_461" n="e" s="T40">"(Davaj </ts>
               <ts e="T42" id="Seg_463" n="e" s="T41">i- </ts>
               <ts e="T43" id="Seg_465" n="e" s="T42">kuza-) </ts>
               <ts e="T44" id="Seg_467" n="e" s="T43">Davaj </ts>
               <ts e="T45" id="Seg_469" n="e" s="T44">amzittə, </ts>
               <ts e="T46" id="Seg_471" n="e" s="T45">davaj </ts>
               <ts e="T47" id="Seg_473" n="e" s="T46">iššo </ts>
               <ts e="T48" id="Seg_475" n="e" s="T47">amnobəj". </ts>
               <ts e="T49" id="Seg_477" n="e" s="T48">Dĭgəttə </ts>
               <ts e="T50" id="Seg_479" n="e" s="T49">amnobiʔi. </ts>
               <ts e="T51" id="Seg_481" n="e" s="T50">Dĭgəttə:" </ts>
               <ts e="T52" id="Seg_483" n="e" s="T51">Davaj </ts>
               <ts e="T53" id="Seg_485" n="e" s="T52">iššo </ts>
               <ts e="T54" id="Seg_487" n="e" s="T53">nüdʼigən </ts>
               <ts e="T55" id="Seg_489" n="e" s="T54">amzittə". </ts>
               <ts e="T56" id="Seg_491" n="e" s="T55">Dĭgəttə </ts>
               <ts e="T57" id="Seg_493" n="e" s="T56">ambiʔi </ts>
               <ts e="T58" id="Seg_495" n="e" s="T57">(šalaʔjə). </ts>
               <ts e="T59" id="Seg_497" n="e" s="T58">Dĭ </ts>
               <ts e="T60" id="Seg_499" n="e" s="T59">kuza </ts>
               <ts e="T61" id="Seg_501" n="e" s="T60">iliet </ts>
               <ts e="T62" id="Seg_503" n="e" s="T61">oldʼabə </ts>
               <ts e="T63" id="Seg_505" n="e" s="T62">da </ts>
               <ts e="T64" id="Seg_507" n="e" s="T63">kunolzittə </ts>
               <ts e="T65" id="Seg_509" n="e" s="T64">kandəga. </ts>
               <ts e="T66" id="Seg_511" n="e" s="T65">"A </ts>
               <ts e="T67" id="Seg_513" n="e" s="T66">ĭmbi </ts>
               <ts e="T68" id="Seg_515" n="e" s="T67">tăn </ts>
               <ts e="T69" id="Seg_517" n="e" s="T68">kunolzittə </ts>
               <ts e="T70" id="Seg_519" n="e" s="T69">kandəgal?" </ts>
               <ts e="T71" id="Seg_521" n="e" s="T70">"A </ts>
               <ts e="T72" id="Seg_523" n="e" s="T71">tăn </ts>
               <ts e="T73" id="Seg_525" n="e" s="T72">boslə </ts>
               <ts e="T74" id="Seg_527" n="e" s="T73">dăre </ts>
               <ts e="T75" id="Seg_529" n="e" s="T74">nörbəbiel </ts>
               <ts e="T76" id="Seg_531" n="e" s="T75">što </ts>
               <ts e="T77" id="Seg_533" n="e" s="T76">kunolzittə </ts>
               <ts e="T78" id="Seg_535" n="e" s="T77">nada". </ts>
               <ts e="T79" id="Seg_537" n="e" s="T78">I </ts>
               <ts e="T80" id="Seg_539" n="e" s="T79">kambi, </ts>
               <ts e="T81" id="Seg_541" n="e" s="T80">kunolbi. </ts>
               <ts e="T82" id="Seg_543" n="e" s="T81">I </ts>
               <ts e="T83" id="Seg_545" n="e" s="T82">(ertegəʔ). </ts>
               <ts e="T84" id="Seg_547" n="e" s="T83">Dĭgəttə </ts>
               <ts e="T85" id="Seg_549" n="e" s="T84">abəs </ts>
               <ts e="T86" id="Seg_551" n="e" s="T85">tüj </ts>
               <ts e="T87" id="Seg_553" n="e" s="T86">ej </ts>
               <ts e="T88" id="Seg_555" n="e" s="T87">mĭlie </ts>
               <ts e="T89" id="Seg_557" n="e" s="T88">dĭʔnə </ts>
               <ts e="T90" id="Seg_559" n="e" s="T89">amzittə. </ts>
               <ts e="T91" id="Seg_561" n="e" s="T90">I </ts>
               <ts e="T92" id="Seg_563" n="e" s="T91">mʼegluʔpi </ts>
               <ts e="T93" id="Seg_565" n="e" s="T92">dĭ </ts>
               <ts e="T94" id="Seg_567" n="e" s="T93">abəsdə. </ts>
               <ts e="T95" id="Seg_569" n="e" s="T94">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_570" s="T0">PKZ_196X_KindPriest_flk.001 (001)</ta>
            <ta e="T6" id="Seg_571" s="T3">PKZ_196X_KindPriest_flk.002 (002)</ta>
            <ta e="T8" id="Seg_572" s="T6">PKZ_196X_KindPriest_flk.003 (003)</ta>
            <ta e="T14" id="Seg_573" s="T8">PKZ_196X_KindPriest_flk.004 (004)</ta>
            <ta e="T19" id="Seg_574" s="T14">PKZ_196X_KindPriest_flk.005 (005)</ta>
            <ta e="T23" id="Seg_575" s="T19">PKZ_196X_KindPriest_flk.006 (006)</ta>
            <ta e="T25" id="Seg_576" s="T23">PKZ_196X_KindPriest_flk.007 (007)</ta>
            <ta e="T35" id="Seg_577" s="T25">PKZ_196X_KindPriest_flk.008 (008)</ta>
            <ta e="T40" id="Seg_578" s="T35">PKZ_196X_KindPriest_flk.009 (009)</ta>
            <ta e="T48" id="Seg_579" s="T40">PKZ_196X_KindPriest_flk.010 (010)</ta>
            <ta e="T50" id="Seg_580" s="T48">PKZ_196X_KindPriest_flk.011 (011)</ta>
            <ta e="T55" id="Seg_581" s="T50">PKZ_196X_KindPriest_flk.012 (012)</ta>
            <ta e="T58" id="Seg_582" s="T55">PKZ_196X_KindPriest_flk.013 (013)</ta>
            <ta e="T65" id="Seg_583" s="T58">PKZ_196X_KindPriest_flk.014 (014)</ta>
            <ta e="T70" id="Seg_584" s="T65">PKZ_196X_KindPriest_flk.015 (015)</ta>
            <ta e="T78" id="Seg_585" s="T70">PKZ_196X_KindPriest_flk.016 (016)</ta>
            <ta e="T81" id="Seg_586" s="T78">PKZ_196X_KindPriest_flk.017 (017)</ta>
            <ta e="T83" id="Seg_587" s="T81">PKZ_196X_KindPriest_flk.018 (018)</ta>
            <ta e="T90" id="Seg_588" s="T83">PKZ_196X_KindPriest_flk.019 (019)</ta>
            <ta e="T94" id="Seg_589" s="T90">PKZ_196X_KindPriest_flk.020 (020)</ta>
            <ta e="T95" id="Seg_590" s="T94">PKZ_196X_KindPriest_flk.021 (021)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_591" s="T0">(Onʼ-) Amnobi abəs. </ta>
            <ta e="T6" id="Seg_592" s="T3">Onʼiʔ dĭ kambi. </ta>
            <ta e="T8" id="Seg_593" s="T6">Kuza deʔpi. </ta>
            <ta e="T14" id="Seg_594" s="T8">"Amnoʔ măna, măn tănan aktʼa mĭlem". </ta>
            <ta e="T19" id="Seg_595" s="T14">Dĭ amnobi, amnobi, onʼiʔ nʼedʼelʼə. </ta>
            <ta e="T23" id="Seg_596" s="T19">Dĭgəttə:" Kanžəbəj noʔ jaʔsittə!" </ta>
            <ta e="T25" id="Seg_597" s="T23">Erten uʔbdəbiʔi. </ta>
            <ta e="T35" id="Seg_598" s="T25">Măndə iandə:" Deʔ miʔnʼibeʔ amzittə, miʔ kanžəbəj noʔ jaʔsittə šapkuziʔ". </ta>
            <ta e="T40" id="Seg_599" s="T35">Dĭ mĭbi dĭzeŋdə (m-) amzittə. </ta>
            <ta e="T48" id="Seg_600" s="T40">"(Davaj i- kuza-) Davaj amzittə, davaj iššo amnobəj". </ta>
            <ta e="T50" id="Seg_601" s="T48">Dĭgəttə amnobiʔi. </ta>
            <ta e="T55" id="Seg_602" s="T50">Dĭgəttə:" Davaj iššo nüdʼigən amzittə". </ta>
            <ta e="T58" id="Seg_603" s="T55">Dĭgəttə ambiʔi (šalaʔjə). </ta>
            <ta e="T65" id="Seg_604" s="T58">Dĭ kuza iliet oldʼabə da kunolzittə kandəga. </ta>
            <ta e="T70" id="Seg_605" s="T65">"A ĭmbi tăn kunolzittə kandəgal?" </ta>
            <ta e="T78" id="Seg_606" s="T70">"A tăn boslə dăre nörbəbiel što kunolzittə nada". </ta>
            <ta e="T81" id="Seg_607" s="T78">I kambi, kunolbi. </ta>
            <ta e="T83" id="Seg_608" s="T81">I (ertegəʔ). </ta>
            <ta e="T90" id="Seg_609" s="T83">Dĭgəttə abəs tüj ej mĭlie dĭʔnə amzittə. </ta>
            <ta e="T94" id="Seg_610" s="T90">I mʼegluʔpi dĭ abəsdə. </ta>
            <ta e="T95" id="Seg_611" s="T94">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_612" s="T1">amno-bi</ta>
            <ta e="T3" id="Seg_613" s="T2">abəs</ta>
            <ta e="T4" id="Seg_614" s="T3">onʼiʔ</ta>
            <ta e="T5" id="Seg_615" s="T4">dĭ</ta>
            <ta e="T6" id="Seg_616" s="T5">kam-bi</ta>
            <ta e="T7" id="Seg_617" s="T6">kuza</ta>
            <ta e="T8" id="Seg_618" s="T7">deʔ-pi</ta>
            <ta e="T9" id="Seg_619" s="T8">amno-ʔ</ta>
            <ta e="T10" id="Seg_620" s="T9">măna</ta>
            <ta e="T11" id="Seg_621" s="T10">măn</ta>
            <ta e="T12" id="Seg_622" s="T11">tănan</ta>
            <ta e="T13" id="Seg_623" s="T12">aktʼa</ta>
            <ta e="T14" id="Seg_624" s="T13">mĭ-le-m</ta>
            <ta e="T15" id="Seg_625" s="T14">dĭ</ta>
            <ta e="T16" id="Seg_626" s="T15">amno-bi</ta>
            <ta e="T17" id="Seg_627" s="T16">amno-bi</ta>
            <ta e="T18" id="Seg_628" s="T17">onʼiʔ</ta>
            <ta e="T20" id="Seg_629" s="T19">dĭgəttə</ta>
            <ta e="T21" id="Seg_630" s="T20">kan-žə-bəj</ta>
            <ta e="T22" id="Seg_631" s="T21">noʔ</ta>
            <ta e="T23" id="Seg_632" s="T22">jaʔ-sittə</ta>
            <ta e="T24" id="Seg_633" s="T23">erte-n</ta>
            <ta e="T25" id="Seg_634" s="T24">uʔbdə-bi-ʔi</ta>
            <ta e="T26" id="Seg_635" s="T25">măn-də</ta>
            <ta e="T27" id="Seg_636" s="T26">ia-ndə</ta>
            <ta e="T28" id="Seg_637" s="T27">de-ʔ</ta>
            <ta e="T29" id="Seg_638" s="T28">miʔnʼibeʔ</ta>
            <ta e="T30" id="Seg_639" s="T29">am-zittə</ta>
            <ta e="T31" id="Seg_640" s="T30">miʔ</ta>
            <ta e="T32" id="Seg_641" s="T31">kan-žə-bəj</ta>
            <ta e="T33" id="Seg_642" s="T32">noʔ</ta>
            <ta e="T34" id="Seg_643" s="T33">jaʔ-sittə</ta>
            <ta e="T35" id="Seg_644" s="T34">šapku-ziʔ</ta>
            <ta e="T36" id="Seg_645" s="T35">dĭ</ta>
            <ta e="T37" id="Seg_646" s="T36">mĭ-bi</ta>
            <ta e="T38" id="Seg_647" s="T37">dĭ-zeŋ-də</ta>
            <ta e="T40" id="Seg_648" s="T39">am-zittə</ta>
            <ta e="T41" id="Seg_649" s="T40">davaj</ta>
            <ta e="T44" id="Seg_650" s="T43">davaj</ta>
            <ta e="T45" id="Seg_651" s="T44">am-zittə</ta>
            <ta e="T46" id="Seg_652" s="T45">davaj</ta>
            <ta e="T47" id="Seg_653" s="T46">iššo</ta>
            <ta e="T48" id="Seg_654" s="T47">am-no-bəj</ta>
            <ta e="T49" id="Seg_655" s="T48">dĭgəttə</ta>
            <ta e="T50" id="Seg_656" s="T49">amno-bi-ʔi</ta>
            <ta e="T51" id="Seg_657" s="T50">dĭgəttə</ta>
            <ta e="T52" id="Seg_658" s="T51">davaj</ta>
            <ta e="T53" id="Seg_659" s="T52">iššo</ta>
            <ta e="T54" id="Seg_660" s="T53">nüdʼi-gən</ta>
            <ta e="T55" id="Seg_661" s="T54">am-zittə</ta>
            <ta e="T56" id="Seg_662" s="T55">dĭgəttə</ta>
            <ta e="T57" id="Seg_663" s="T56">am-bi-ʔi</ta>
            <ta e="T58" id="Seg_664" s="T57">šalaʔjə</ta>
            <ta e="T59" id="Seg_665" s="T58">dĭ</ta>
            <ta e="T60" id="Seg_666" s="T59">kuza</ta>
            <ta e="T61" id="Seg_667" s="T60">i-lie-t</ta>
            <ta e="T62" id="Seg_668" s="T61">oldʼa-bə</ta>
            <ta e="T63" id="Seg_669" s="T62">da</ta>
            <ta e="T64" id="Seg_670" s="T63">kunol-zittə</ta>
            <ta e="T65" id="Seg_671" s="T64">kandə-ga</ta>
            <ta e="T66" id="Seg_672" s="T65">a</ta>
            <ta e="T67" id="Seg_673" s="T66">ĭmbi</ta>
            <ta e="T68" id="Seg_674" s="T67">tăn</ta>
            <ta e="T69" id="Seg_675" s="T68">kunol-zittə</ta>
            <ta e="T70" id="Seg_676" s="T69">kandə-ga-l</ta>
            <ta e="T71" id="Seg_677" s="T70">a</ta>
            <ta e="T72" id="Seg_678" s="T71">tăn</ta>
            <ta e="T73" id="Seg_679" s="T72">bos-lə</ta>
            <ta e="T74" id="Seg_680" s="T73">dăre</ta>
            <ta e="T75" id="Seg_681" s="T74">nörbə-bie-l</ta>
            <ta e="T76" id="Seg_682" s="T75">što</ta>
            <ta e="T77" id="Seg_683" s="T76">kunol-zittə</ta>
            <ta e="T78" id="Seg_684" s="T77">nada</ta>
            <ta e="T79" id="Seg_685" s="T78">i</ta>
            <ta e="T80" id="Seg_686" s="T79">kam-bi</ta>
            <ta e="T81" id="Seg_687" s="T80">kunol-bi</ta>
            <ta e="T82" id="Seg_688" s="T81">i</ta>
            <ta e="T83" id="Seg_689" s="T82">erte-gəʔ</ta>
            <ta e="T84" id="Seg_690" s="T83">dĭgəttə</ta>
            <ta e="T85" id="Seg_691" s="T84">abəs</ta>
            <ta e="T86" id="Seg_692" s="T85">tüj</ta>
            <ta e="T87" id="Seg_693" s="T86">ej</ta>
            <ta e="T88" id="Seg_694" s="T87">mĭ-lie</ta>
            <ta e="T89" id="Seg_695" s="T88">dĭʔ-nə</ta>
            <ta e="T90" id="Seg_696" s="T89">am-zittə</ta>
            <ta e="T91" id="Seg_697" s="T90">i</ta>
            <ta e="T92" id="Seg_698" s="T91">mʼeg-luʔ-pi</ta>
            <ta e="T93" id="Seg_699" s="T92">dĭ</ta>
            <ta e="T94" id="Seg_700" s="T93">abəs-də</ta>
            <ta e="T95" id="Seg_701" s="T94">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_702" s="T1">amno-bi</ta>
            <ta e="T3" id="Seg_703" s="T2">abəs</ta>
            <ta e="T4" id="Seg_704" s="T3">onʼiʔ</ta>
            <ta e="T5" id="Seg_705" s="T4">dĭ</ta>
            <ta e="T6" id="Seg_706" s="T5">kan-bi</ta>
            <ta e="T7" id="Seg_707" s="T6">kuza</ta>
            <ta e="T8" id="Seg_708" s="T7">det-bi</ta>
            <ta e="T9" id="Seg_709" s="T8">amno-ʔ</ta>
            <ta e="T10" id="Seg_710" s="T9">măna</ta>
            <ta e="T11" id="Seg_711" s="T10">măn</ta>
            <ta e="T12" id="Seg_712" s="T11">tănan</ta>
            <ta e="T13" id="Seg_713" s="T12">aktʼa</ta>
            <ta e="T14" id="Seg_714" s="T13">mĭ-lV-m</ta>
            <ta e="T15" id="Seg_715" s="T14">dĭ</ta>
            <ta e="T16" id="Seg_716" s="T15">amno-bi</ta>
            <ta e="T17" id="Seg_717" s="T16">amno-bi</ta>
            <ta e="T18" id="Seg_718" s="T17">onʼiʔ</ta>
            <ta e="T20" id="Seg_719" s="T19">dĭgəttə</ta>
            <ta e="T21" id="Seg_720" s="T20">kan-žə-bəj</ta>
            <ta e="T22" id="Seg_721" s="T21">noʔ</ta>
            <ta e="T23" id="Seg_722" s="T22">hʼaʔ-zittə</ta>
            <ta e="T24" id="Seg_723" s="T23">ertə-n</ta>
            <ta e="T25" id="Seg_724" s="T24">uʔbdə-bi-jəʔ</ta>
            <ta e="T26" id="Seg_725" s="T25">măn-ntə</ta>
            <ta e="T27" id="Seg_726" s="T26">ija-gəndə</ta>
            <ta e="T28" id="Seg_727" s="T27">det-ʔ</ta>
            <ta e="T29" id="Seg_728" s="T28">miʔnʼibeʔ</ta>
            <ta e="T30" id="Seg_729" s="T29">am-zittə</ta>
            <ta e="T31" id="Seg_730" s="T30">miʔ</ta>
            <ta e="T32" id="Seg_731" s="T31">kan-žə-bəj</ta>
            <ta e="T33" id="Seg_732" s="T32">noʔ</ta>
            <ta e="T34" id="Seg_733" s="T33">hʼaʔ-zittə</ta>
            <ta e="T35" id="Seg_734" s="T34">šapku-ziʔ</ta>
            <ta e="T36" id="Seg_735" s="T35">dĭ</ta>
            <ta e="T37" id="Seg_736" s="T36">mĭ-bi</ta>
            <ta e="T38" id="Seg_737" s="T37">dĭ-zAŋ-Tə</ta>
            <ta e="T40" id="Seg_738" s="T39">am-zittə</ta>
            <ta e="T41" id="Seg_739" s="T40">davaj</ta>
            <ta e="T44" id="Seg_740" s="T43">davaj</ta>
            <ta e="T45" id="Seg_741" s="T44">am-zittə</ta>
            <ta e="T46" id="Seg_742" s="T45">davaj</ta>
            <ta e="T47" id="Seg_743" s="T46">ĭššo</ta>
            <ta e="T48" id="Seg_744" s="T47">am-lV-bəj</ta>
            <ta e="T49" id="Seg_745" s="T48">dĭgəttə</ta>
            <ta e="T50" id="Seg_746" s="T49">amnə-bi-jəʔ</ta>
            <ta e="T51" id="Seg_747" s="T50">dĭgəttə</ta>
            <ta e="T52" id="Seg_748" s="T51">davaj</ta>
            <ta e="T53" id="Seg_749" s="T52">ĭššo</ta>
            <ta e="T54" id="Seg_750" s="T53">nüdʼi-Kən</ta>
            <ta e="T55" id="Seg_751" s="T54">am-zittə</ta>
            <ta e="T56" id="Seg_752" s="T55">dĭgəttə</ta>
            <ta e="T57" id="Seg_753" s="T56">am-bi-jəʔ</ta>
            <ta e="T58" id="Seg_754" s="T57">šala</ta>
            <ta e="T59" id="Seg_755" s="T58">dĭ</ta>
            <ta e="T60" id="Seg_756" s="T59">kuza</ta>
            <ta e="T61" id="Seg_757" s="T60">i-liA-t</ta>
            <ta e="T62" id="Seg_758" s="T61">oldʼa-bə</ta>
            <ta e="T63" id="Seg_759" s="T62">da</ta>
            <ta e="T64" id="Seg_760" s="T63">kunol-zittə</ta>
            <ta e="T65" id="Seg_761" s="T64">kandə-gA</ta>
            <ta e="T66" id="Seg_762" s="T65">a</ta>
            <ta e="T67" id="Seg_763" s="T66">ĭmbi</ta>
            <ta e="T68" id="Seg_764" s="T67">tăn</ta>
            <ta e="T69" id="Seg_765" s="T68">kunol-zittə</ta>
            <ta e="T70" id="Seg_766" s="T69">kandə-gA-l</ta>
            <ta e="T71" id="Seg_767" s="T70">a</ta>
            <ta e="T72" id="Seg_768" s="T71">tăn</ta>
            <ta e="T73" id="Seg_769" s="T72">bos-l</ta>
            <ta e="T74" id="Seg_770" s="T73">dărəʔ</ta>
            <ta e="T75" id="Seg_771" s="T74">nörbə-bi-l</ta>
            <ta e="T76" id="Seg_772" s="T75">što</ta>
            <ta e="T77" id="Seg_773" s="T76">kunol-zittə</ta>
            <ta e="T78" id="Seg_774" s="T77">nadə</ta>
            <ta e="T79" id="Seg_775" s="T78">i</ta>
            <ta e="T80" id="Seg_776" s="T79">kan-bi</ta>
            <ta e="T81" id="Seg_777" s="T80">kunol-bi</ta>
            <ta e="T82" id="Seg_778" s="T81">i</ta>
            <ta e="T83" id="Seg_779" s="T82">ertə-gəʔ</ta>
            <ta e="T84" id="Seg_780" s="T83">dĭgəttə</ta>
            <ta e="T85" id="Seg_781" s="T84">abəs</ta>
            <ta e="T86" id="Seg_782" s="T85">tüj</ta>
            <ta e="T87" id="Seg_783" s="T86">ej</ta>
            <ta e="T88" id="Seg_784" s="T87">mĭ-liA</ta>
            <ta e="T89" id="Seg_785" s="T88">dĭ-Tə</ta>
            <ta e="T90" id="Seg_786" s="T89">am-zittə</ta>
            <ta e="T91" id="Seg_787" s="T90">i</ta>
            <ta e="T92" id="Seg_788" s="T91">mʼeg-luʔbdə-bi</ta>
            <ta e="T93" id="Seg_789" s="T92">dĭ</ta>
            <ta e="T94" id="Seg_790" s="T93">abəs-də</ta>
            <ta e="T95" id="Seg_791" s="T94">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_792" s="T1">live-PST.[3SG]</ta>
            <ta e="T3" id="Seg_793" s="T2">priest.[NOM.SG]</ta>
            <ta e="T4" id="Seg_794" s="T3">one.[NOM.SG]</ta>
            <ta e="T5" id="Seg_795" s="T4">this.[NOM.SG]</ta>
            <ta e="T6" id="Seg_796" s="T5">go-PST.[3SG]</ta>
            <ta e="T7" id="Seg_797" s="T6">man.[NOM.SG]</ta>
            <ta e="T8" id="Seg_798" s="T7">bring-PST.[3SG]</ta>
            <ta e="T9" id="Seg_799" s="T8">live-IMP.2SG</ta>
            <ta e="T10" id="Seg_800" s="T9">I.LAT</ta>
            <ta e="T11" id="Seg_801" s="T10">I.NOM</ta>
            <ta e="T12" id="Seg_802" s="T11">you.DAT</ta>
            <ta e="T13" id="Seg_803" s="T12">money.[NOM.SG]</ta>
            <ta e="T14" id="Seg_804" s="T13">give-FUT-1SG</ta>
            <ta e="T15" id="Seg_805" s="T14">this.[NOM.SG]</ta>
            <ta e="T16" id="Seg_806" s="T15">live-PST.[3SG]</ta>
            <ta e="T17" id="Seg_807" s="T16">live-PST.[3SG]</ta>
            <ta e="T18" id="Seg_808" s="T17">one.[NOM.SG]</ta>
            <ta e="T20" id="Seg_809" s="T19">then</ta>
            <ta e="T21" id="Seg_810" s="T20">go-OPT.DU/PL-1DU</ta>
            <ta e="T22" id="Seg_811" s="T21">grass.[NOM.SG]</ta>
            <ta e="T23" id="Seg_812" s="T22">cut-INF.LAT</ta>
            <ta e="T24" id="Seg_813" s="T23">morning-LOC.ADV</ta>
            <ta e="T25" id="Seg_814" s="T24">get.up-PST-3PL</ta>
            <ta e="T26" id="Seg_815" s="T25">say-IPFVZ.[3SG]</ta>
            <ta e="T27" id="Seg_816" s="T26">mother-LAT/LOC.3SG</ta>
            <ta e="T28" id="Seg_817" s="T27">bring-IMP.2SG</ta>
            <ta e="T29" id="Seg_818" s="T28">we.LAT</ta>
            <ta e="T30" id="Seg_819" s="T29">eat-INF.LAT</ta>
            <ta e="T31" id="Seg_820" s="T30">we.NOM</ta>
            <ta e="T32" id="Seg_821" s="T31">go-OPT.DU/PL-1DU</ta>
            <ta e="T33" id="Seg_822" s="T32">grass.[NOM.SG]</ta>
            <ta e="T34" id="Seg_823" s="T33">cut-INF.LAT</ta>
            <ta e="T35" id="Seg_824" s="T34">scythe-INS</ta>
            <ta e="T36" id="Seg_825" s="T35">this.[NOM.SG]</ta>
            <ta e="T37" id="Seg_826" s="T36">give-PST.[3SG]</ta>
            <ta e="T38" id="Seg_827" s="T37">this-PL-LAT</ta>
            <ta e="T40" id="Seg_828" s="T39">eat-INF.LAT</ta>
            <ta e="T41" id="Seg_829" s="T40">HORT</ta>
            <ta e="T44" id="Seg_830" s="T43">HORT</ta>
            <ta e="T45" id="Seg_831" s="T44">eat-INF.LAT</ta>
            <ta e="T46" id="Seg_832" s="T45">HORT</ta>
            <ta e="T47" id="Seg_833" s="T46">more</ta>
            <ta e="T48" id="Seg_834" s="T47">eat-FUT-1DU</ta>
            <ta e="T49" id="Seg_835" s="T48">then</ta>
            <ta e="T50" id="Seg_836" s="T49">sit.down-PST-3PL</ta>
            <ta e="T51" id="Seg_837" s="T50">then</ta>
            <ta e="T52" id="Seg_838" s="T51">HORT</ta>
            <ta e="T53" id="Seg_839" s="T52">more</ta>
            <ta e="T54" id="Seg_840" s="T53">evening-LOC</ta>
            <ta e="T55" id="Seg_841" s="T54">eat-INF.LAT</ta>
            <ta e="T56" id="Seg_842" s="T55">then</ta>
            <ta e="T57" id="Seg_843" s="T56">eat-PST-3PL</ta>
            <ta e="T58" id="Seg_844" s="T57">%%</ta>
            <ta e="T59" id="Seg_845" s="T58">this.[NOM.SG]</ta>
            <ta e="T60" id="Seg_846" s="T59">man.[NOM.SG]</ta>
            <ta e="T61" id="Seg_847" s="T60">take-PRS-3SG.O</ta>
            <ta e="T62" id="Seg_848" s="T61">clothing-ACC.3SG</ta>
            <ta e="T63" id="Seg_849" s="T62">and</ta>
            <ta e="T64" id="Seg_850" s="T63">sleep-INF.LAT</ta>
            <ta e="T65" id="Seg_851" s="T64">walk-PRS.[3SG]</ta>
            <ta e="T66" id="Seg_852" s="T65">and</ta>
            <ta e="T67" id="Seg_853" s="T66">what.[NOM.SG]</ta>
            <ta e="T68" id="Seg_854" s="T67">you.NOM</ta>
            <ta e="T69" id="Seg_855" s="T68">sleep-INF.LAT</ta>
            <ta e="T70" id="Seg_856" s="T69">walk-PRS-2SG</ta>
            <ta e="T71" id="Seg_857" s="T70">and</ta>
            <ta e="T72" id="Seg_858" s="T71">you.NOM</ta>
            <ta e="T73" id="Seg_859" s="T72">self-NOM/GEN/ACC.2SG</ta>
            <ta e="T74" id="Seg_860" s="T73">so</ta>
            <ta e="T75" id="Seg_861" s="T74">tell-PST-2SG</ta>
            <ta e="T76" id="Seg_862" s="T75">that</ta>
            <ta e="T77" id="Seg_863" s="T76">sleep-INF.LAT</ta>
            <ta e="T78" id="Seg_864" s="T77">one.should</ta>
            <ta e="T79" id="Seg_865" s="T78">and</ta>
            <ta e="T80" id="Seg_866" s="T79">go-PST.[3SG]</ta>
            <ta e="T81" id="Seg_867" s="T80">sleep-PST.[3SG]</ta>
            <ta e="T82" id="Seg_868" s="T81">and</ta>
            <ta e="T83" id="Seg_869" s="T82">morning-ABL</ta>
            <ta e="T84" id="Seg_870" s="T83">then</ta>
            <ta e="T85" id="Seg_871" s="T84">priest.[NOM.SG]</ta>
            <ta e="T86" id="Seg_872" s="T85">now</ta>
            <ta e="T87" id="Seg_873" s="T86">NEG</ta>
            <ta e="T88" id="Seg_874" s="T87">give-PRS.[3SG]</ta>
            <ta e="T89" id="Seg_875" s="T88">this-LAT</ta>
            <ta e="T90" id="Seg_876" s="T89">eat-INF.LAT</ta>
            <ta e="T91" id="Seg_877" s="T90">and</ta>
            <ta e="T92" id="Seg_878" s="T91">lure-MOM-PST.[3SG]</ta>
            <ta e="T93" id="Seg_879" s="T92">this.[NOM.SG]</ta>
            <ta e="T94" id="Seg_880" s="T93">priest-NOM/GEN/ACC.3SG</ta>
            <ta e="T95" id="Seg_881" s="T94">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_882" s="T1">жить-PST.[3SG]</ta>
            <ta e="T3" id="Seg_883" s="T2">священник.[NOM.SG]</ta>
            <ta e="T4" id="Seg_884" s="T3">один.[NOM.SG]</ta>
            <ta e="T5" id="Seg_885" s="T4">этот.[NOM.SG]</ta>
            <ta e="T6" id="Seg_886" s="T5">пойти-PST.[3SG]</ta>
            <ta e="T7" id="Seg_887" s="T6">мужчина.[NOM.SG]</ta>
            <ta e="T8" id="Seg_888" s="T7">принести-PST.[3SG]</ta>
            <ta e="T9" id="Seg_889" s="T8">жить-IMP.2SG</ta>
            <ta e="T10" id="Seg_890" s="T9">я.LAT</ta>
            <ta e="T11" id="Seg_891" s="T10">я.NOM</ta>
            <ta e="T12" id="Seg_892" s="T11">ты.DAT</ta>
            <ta e="T13" id="Seg_893" s="T12">деньги.[NOM.SG]</ta>
            <ta e="T14" id="Seg_894" s="T13">дать-FUT-1SG</ta>
            <ta e="T15" id="Seg_895" s="T14">этот.[NOM.SG]</ta>
            <ta e="T16" id="Seg_896" s="T15">жить-PST.[3SG]</ta>
            <ta e="T17" id="Seg_897" s="T16">жить-PST.[3SG]</ta>
            <ta e="T18" id="Seg_898" s="T17">один.[NOM.SG]</ta>
            <ta e="T20" id="Seg_899" s="T19">тогда</ta>
            <ta e="T21" id="Seg_900" s="T20">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T22" id="Seg_901" s="T21">трава.[NOM.SG]</ta>
            <ta e="T23" id="Seg_902" s="T22">резать-INF.LAT</ta>
            <ta e="T24" id="Seg_903" s="T23">утро-LOC.ADV</ta>
            <ta e="T25" id="Seg_904" s="T24">встать-PST-3PL</ta>
            <ta e="T26" id="Seg_905" s="T25">сказать-IPFVZ.[3SG]</ta>
            <ta e="T27" id="Seg_906" s="T26">мать-LAT/LOC.3SG</ta>
            <ta e="T28" id="Seg_907" s="T27">принести-IMP.2SG</ta>
            <ta e="T29" id="Seg_908" s="T28">мы.LAT</ta>
            <ta e="T30" id="Seg_909" s="T29">съесть-INF.LAT</ta>
            <ta e="T31" id="Seg_910" s="T30">мы.NOM</ta>
            <ta e="T32" id="Seg_911" s="T31">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T33" id="Seg_912" s="T32">трава.[NOM.SG]</ta>
            <ta e="T34" id="Seg_913" s="T33">резать-INF.LAT</ta>
            <ta e="T35" id="Seg_914" s="T34">коса-INS</ta>
            <ta e="T36" id="Seg_915" s="T35">этот.[NOM.SG]</ta>
            <ta e="T37" id="Seg_916" s="T36">дать-PST.[3SG]</ta>
            <ta e="T38" id="Seg_917" s="T37">этот-PL-LAT</ta>
            <ta e="T40" id="Seg_918" s="T39">съесть-INF.LAT</ta>
            <ta e="T41" id="Seg_919" s="T40">HORT</ta>
            <ta e="T44" id="Seg_920" s="T43">HORT</ta>
            <ta e="T45" id="Seg_921" s="T44">съесть-INF.LAT</ta>
            <ta e="T46" id="Seg_922" s="T45">HORT</ta>
            <ta e="T47" id="Seg_923" s="T46">еще</ta>
            <ta e="T48" id="Seg_924" s="T47">съесть-FUT-1DU</ta>
            <ta e="T49" id="Seg_925" s="T48">тогда</ta>
            <ta e="T50" id="Seg_926" s="T49">сесть-PST-3PL</ta>
            <ta e="T51" id="Seg_927" s="T50">тогда</ta>
            <ta e="T52" id="Seg_928" s="T51">HORT</ta>
            <ta e="T53" id="Seg_929" s="T52">еще</ta>
            <ta e="T54" id="Seg_930" s="T53">вечер-LOC</ta>
            <ta e="T55" id="Seg_931" s="T54">съесть-INF.LAT</ta>
            <ta e="T56" id="Seg_932" s="T55">тогда</ta>
            <ta e="T57" id="Seg_933" s="T56">съесть-PST-3PL</ta>
            <ta e="T58" id="Seg_934" s="T57">%%</ta>
            <ta e="T59" id="Seg_935" s="T58">этот.[NOM.SG]</ta>
            <ta e="T60" id="Seg_936" s="T59">мужчина.[NOM.SG]</ta>
            <ta e="T61" id="Seg_937" s="T60">взять-PRS-3SG.O</ta>
            <ta e="T62" id="Seg_938" s="T61">одежда-ACC.3SG</ta>
            <ta e="T63" id="Seg_939" s="T62">и</ta>
            <ta e="T64" id="Seg_940" s="T63">спать-INF.LAT</ta>
            <ta e="T65" id="Seg_941" s="T64">идти-PRS.[3SG]</ta>
            <ta e="T66" id="Seg_942" s="T65">а</ta>
            <ta e="T67" id="Seg_943" s="T66">что.[NOM.SG]</ta>
            <ta e="T68" id="Seg_944" s="T67">ты.NOM</ta>
            <ta e="T69" id="Seg_945" s="T68">спать-INF.LAT</ta>
            <ta e="T70" id="Seg_946" s="T69">идти-PRS-2SG</ta>
            <ta e="T71" id="Seg_947" s="T70">а</ta>
            <ta e="T72" id="Seg_948" s="T71">ты.NOM</ta>
            <ta e="T73" id="Seg_949" s="T72">сам-NOM/GEN/ACC.2SG</ta>
            <ta e="T74" id="Seg_950" s="T73">так</ta>
            <ta e="T75" id="Seg_951" s="T74">сказать-PST-2SG</ta>
            <ta e="T76" id="Seg_952" s="T75">что</ta>
            <ta e="T77" id="Seg_953" s="T76">спать-INF.LAT</ta>
            <ta e="T78" id="Seg_954" s="T77">надо</ta>
            <ta e="T79" id="Seg_955" s="T78">и</ta>
            <ta e="T80" id="Seg_956" s="T79">пойти-PST.[3SG]</ta>
            <ta e="T81" id="Seg_957" s="T80">спать-PST.[3SG]</ta>
            <ta e="T82" id="Seg_958" s="T81">и</ta>
            <ta e="T83" id="Seg_959" s="T82">утро-ABL</ta>
            <ta e="T84" id="Seg_960" s="T83">тогда</ta>
            <ta e="T85" id="Seg_961" s="T84">священник.[NOM.SG]</ta>
            <ta e="T86" id="Seg_962" s="T85">сейчас</ta>
            <ta e="T87" id="Seg_963" s="T86">NEG</ta>
            <ta e="T88" id="Seg_964" s="T87">дать-PRS.[3SG]</ta>
            <ta e="T89" id="Seg_965" s="T88">этот-LAT</ta>
            <ta e="T90" id="Seg_966" s="T89">съесть-INF.LAT</ta>
            <ta e="T91" id="Seg_967" s="T90">и</ta>
            <ta e="T92" id="Seg_968" s="T91">приманивать-MOM-PST.[3SG]</ta>
            <ta e="T93" id="Seg_969" s="T92">этот.[NOM.SG]</ta>
            <ta e="T94" id="Seg_970" s="T93">священник-NOM/GEN/ACC.3SG</ta>
            <ta e="T95" id="Seg_971" s="T94">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_972" s="T1">v-v:tense-v:pn</ta>
            <ta e="T3" id="Seg_973" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_974" s="T3">num-n:case</ta>
            <ta e="T5" id="Seg_975" s="T4">dempro-n:case</ta>
            <ta e="T6" id="Seg_976" s="T5">v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_977" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_978" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_979" s="T8">v-v:mood.pn</ta>
            <ta e="T10" id="Seg_980" s="T9">pers</ta>
            <ta e="T11" id="Seg_981" s="T10">pers</ta>
            <ta e="T12" id="Seg_982" s="T11">pers</ta>
            <ta e="T13" id="Seg_983" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_984" s="T13">v-v:tense-v:pn</ta>
            <ta e="T15" id="Seg_985" s="T14">dempro-n:case</ta>
            <ta e="T16" id="Seg_986" s="T15">v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_987" s="T16">v-v:tense-v:pn</ta>
            <ta e="T18" id="Seg_988" s="T17">num-n:case</ta>
            <ta e="T20" id="Seg_989" s="T19">adv</ta>
            <ta e="T21" id="Seg_990" s="T20">v-v:mood-v:pn</ta>
            <ta e="T22" id="Seg_991" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_992" s="T22">v-v:n.fin</ta>
            <ta e="T24" id="Seg_993" s="T23">n-adv:case</ta>
            <ta e="T25" id="Seg_994" s="T24">v-v:tense-v:pn</ta>
            <ta e="T26" id="Seg_995" s="T25">v-v&gt;v-v:pn</ta>
            <ta e="T27" id="Seg_996" s="T26">n-n:case.poss</ta>
            <ta e="T28" id="Seg_997" s="T27">v-v:mood.pn</ta>
            <ta e="T29" id="Seg_998" s="T28">pers</ta>
            <ta e="T30" id="Seg_999" s="T29">v-v:n.fin</ta>
            <ta e="T31" id="Seg_1000" s="T30">pers</ta>
            <ta e="T32" id="Seg_1001" s="T31">v-v:mood-v:pn</ta>
            <ta e="T33" id="Seg_1002" s="T32">n-n:case</ta>
            <ta e="T34" id="Seg_1003" s="T33">v-v:n.fin</ta>
            <ta e="T35" id="Seg_1004" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_1005" s="T35">dempro-n:case</ta>
            <ta e="T37" id="Seg_1006" s="T36">v-v:tense-v:pn</ta>
            <ta e="T38" id="Seg_1007" s="T37">dempro-n:num-n:case</ta>
            <ta e="T40" id="Seg_1008" s="T39">v-v:n.fin</ta>
            <ta e="T41" id="Seg_1009" s="T40">ptcl</ta>
            <ta e="T44" id="Seg_1010" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_1011" s="T44">v-v:n.fin</ta>
            <ta e="T46" id="Seg_1012" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_1013" s="T46">adv</ta>
            <ta e="T48" id="Seg_1014" s="T47">v-v:tense-v:pn</ta>
            <ta e="T49" id="Seg_1015" s="T48">adv</ta>
            <ta e="T50" id="Seg_1016" s="T49">v-v:tense-v:pn</ta>
            <ta e="T51" id="Seg_1017" s="T50">adv</ta>
            <ta e="T52" id="Seg_1018" s="T51">ptcl</ta>
            <ta e="T53" id="Seg_1019" s="T52">adv</ta>
            <ta e="T54" id="Seg_1020" s="T53">n-n:case</ta>
            <ta e="T55" id="Seg_1021" s="T54">v-v:n.fin</ta>
            <ta e="T56" id="Seg_1022" s="T55">adv</ta>
            <ta e="T57" id="Seg_1023" s="T56">v-v:tense-v:pn</ta>
            <ta e="T58" id="Seg_1024" s="T57">%%</ta>
            <ta e="T59" id="Seg_1025" s="T58">dempro-n:case</ta>
            <ta e="T60" id="Seg_1026" s="T59">n-n:case</ta>
            <ta e="T61" id="Seg_1027" s="T60">v-v:tense-v:pn</ta>
            <ta e="T62" id="Seg_1028" s="T61">n-n:case.poss</ta>
            <ta e="T63" id="Seg_1029" s="T62">conj</ta>
            <ta e="T64" id="Seg_1030" s="T63">v-v:n.fin</ta>
            <ta e="T65" id="Seg_1031" s="T64">v-v:tense-v:pn</ta>
            <ta e="T66" id="Seg_1032" s="T65">conj</ta>
            <ta e="T67" id="Seg_1033" s="T66">que-n:case</ta>
            <ta e="T68" id="Seg_1034" s="T67">pers</ta>
            <ta e="T69" id="Seg_1035" s="T68">v-v:n.fin</ta>
            <ta e="T70" id="Seg_1036" s="T69">v-v:tense-v:pn</ta>
            <ta e="T71" id="Seg_1037" s="T70">conj</ta>
            <ta e="T72" id="Seg_1038" s="T71">pers</ta>
            <ta e="T73" id="Seg_1039" s="T72">refl-n:case.poss</ta>
            <ta e="T74" id="Seg_1040" s="T73">ptcl</ta>
            <ta e="T75" id="Seg_1041" s="T74">v-v:tense-v:pn</ta>
            <ta e="T76" id="Seg_1042" s="T75">conj</ta>
            <ta e="T77" id="Seg_1043" s="T76">v-v:n.fin</ta>
            <ta e="T78" id="Seg_1044" s="T77">ptcl</ta>
            <ta e="T79" id="Seg_1045" s="T78">conj</ta>
            <ta e="T80" id="Seg_1046" s="T79">v-v:tense-v:pn</ta>
            <ta e="T81" id="Seg_1047" s="T80">v-v:tense-v:pn</ta>
            <ta e="T82" id="Seg_1048" s="T81">conj</ta>
            <ta e="T83" id="Seg_1049" s="T82">n-n:case</ta>
            <ta e="T84" id="Seg_1050" s="T83">adv</ta>
            <ta e="T85" id="Seg_1051" s="T84">n-n:case</ta>
            <ta e="T86" id="Seg_1052" s="T85">adv</ta>
            <ta e="T87" id="Seg_1053" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_1054" s="T87">v-v:tense-v:pn</ta>
            <ta e="T89" id="Seg_1055" s="T88">dempro-n:case</ta>
            <ta e="T90" id="Seg_1056" s="T89">v-v:n.fin</ta>
            <ta e="T91" id="Seg_1057" s="T90">conj</ta>
            <ta e="T92" id="Seg_1058" s="T91">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T93" id="Seg_1059" s="T92">dempro-n:case</ta>
            <ta e="T94" id="Seg_1060" s="T93">n-n:case.poss</ta>
            <ta e="T95" id="Seg_1061" s="T94">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1062" s="T1">v</ta>
            <ta e="T3" id="Seg_1063" s="T2">n</ta>
            <ta e="T4" id="Seg_1064" s="T3">num</ta>
            <ta e="T5" id="Seg_1065" s="T4">dempro</ta>
            <ta e="T6" id="Seg_1066" s="T5">v</ta>
            <ta e="T7" id="Seg_1067" s="T6">n</ta>
            <ta e="T8" id="Seg_1068" s="T7">v</ta>
            <ta e="T9" id="Seg_1069" s="T8">v</ta>
            <ta e="T10" id="Seg_1070" s="T9">pers</ta>
            <ta e="T11" id="Seg_1071" s="T10">pers</ta>
            <ta e="T12" id="Seg_1072" s="T11">pers</ta>
            <ta e="T13" id="Seg_1073" s="T12">n</ta>
            <ta e="T14" id="Seg_1074" s="T13">v</ta>
            <ta e="T15" id="Seg_1075" s="T14">dempro</ta>
            <ta e="T16" id="Seg_1076" s="T15">v</ta>
            <ta e="T17" id="Seg_1077" s="T16">v</ta>
            <ta e="T18" id="Seg_1078" s="T17">num</ta>
            <ta e="T20" id="Seg_1079" s="T19">adv</ta>
            <ta e="T21" id="Seg_1080" s="T20">v</ta>
            <ta e="T22" id="Seg_1081" s="T21">n</ta>
            <ta e="T23" id="Seg_1082" s="T22">v</ta>
            <ta e="T24" id="Seg_1083" s="T23">adv</ta>
            <ta e="T25" id="Seg_1084" s="T24">v</ta>
            <ta e="T26" id="Seg_1085" s="T25">v</ta>
            <ta e="T27" id="Seg_1086" s="T26">n</ta>
            <ta e="T28" id="Seg_1087" s="T27">v</ta>
            <ta e="T29" id="Seg_1088" s="T28">pers</ta>
            <ta e="T30" id="Seg_1089" s="T29">v</ta>
            <ta e="T31" id="Seg_1090" s="T30">pers</ta>
            <ta e="T32" id="Seg_1091" s="T31">v</ta>
            <ta e="T33" id="Seg_1092" s="T32">n</ta>
            <ta e="T34" id="Seg_1093" s="T33">v</ta>
            <ta e="T35" id="Seg_1094" s="T34">n</ta>
            <ta e="T36" id="Seg_1095" s="T35">dempro</ta>
            <ta e="T37" id="Seg_1096" s="T36">v</ta>
            <ta e="T38" id="Seg_1097" s="T37">dempro</ta>
            <ta e="T40" id="Seg_1098" s="T39">v</ta>
            <ta e="T41" id="Seg_1099" s="T40">ptcl</ta>
            <ta e="T44" id="Seg_1100" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_1101" s="T44">v</ta>
            <ta e="T46" id="Seg_1102" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_1103" s="T46">adv</ta>
            <ta e="T48" id="Seg_1104" s="T47">v</ta>
            <ta e="T49" id="Seg_1105" s="T48">adv</ta>
            <ta e="T50" id="Seg_1106" s="T49">v</ta>
            <ta e="T51" id="Seg_1107" s="T50">adv</ta>
            <ta e="T52" id="Seg_1108" s="T51">ptcl</ta>
            <ta e="T53" id="Seg_1109" s="T52">adv</ta>
            <ta e="T54" id="Seg_1110" s="T53">n</ta>
            <ta e="T55" id="Seg_1111" s="T54">v</ta>
            <ta e="T56" id="Seg_1112" s="T55">adv</ta>
            <ta e="T57" id="Seg_1113" s="T56">v</ta>
            <ta e="T59" id="Seg_1114" s="T58">dempro</ta>
            <ta e="T60" id="Seg_1115" s="T59">n</ta>
            <ta e="T61" id="Seg_1116" s="T60">v</ta>
            <ta e="T62" id="Seg_1117" s="T61">n</ta>
            <ta e="T63" id="Seg_1118" s="T62">conj</ta>
            <ta e="T64" id="Seg_1119" s="T63">v</ta>
            <ta e="T65" id="Seg_1120" s="T64">v</ta>
            <ta e="T66" id="Seg_1121" s="T65">conj</ta>
            <ta e="T67" id="Seg_1122" s="T66">que</ta>
            <ta e="T68" id="Seg_1123" s="T67">pers</ta>
            <ta e="T69" id="Seg_1124" s="T68">v</ta>
            <ta e="T70" id="Seg_1125" s="T69">v</ta>
            <ta e="T71" id="Seg_1126" s="T70">conj</ta>
            <ta e="T72" id="Seg_1127" s="T71">pers</ta>
            <ta e="T73" id="Seg_1128" s="T72">refl</ta>
            <ta e="T74" id="Seg_1129" s="T73">ptcl</ta>
            <ta e="T75" id="Seg_1130" s="T74">v</ta>
            <ta e="T76" id="Seg_1131" s="T75">conj</ta>
            <ta e="T77" id="Seg_1132" s="T76">v</ta>
            <ta e="T78" id="Seg_1133" s="T77">ptcl</ta>
            <ta e="T79" id="Seg_1134" s="T78">conj</ta>
            <ta e="T80" id="Seg_1135" s="T79">v</ta>
            <ta e="T81" id="Seg_1136" s="T80">v</ta>
            <ta e="T82" id="Seg_1137" s="T81">conj</ta>
            <ta e="T83" id="Seg_1138" s="T82">n</ta>
            <ta e="T84" id="Seg_1139" s="T83">adv</ta>
            <ta e="T85" id="Seg_1140" s="T84">n</ta>
            <ta e="T86" id="Seg_1141" s="T85">adv</ta>
            <ta e="T87" id="Seg_1142" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_1143" s="T87">v</ta>
            <ta e="T89" id="Seg_1144" s="T88">dempro</ta>
            <ta e="T90" id="Seg_1145" s="T89">v</ta>
            <ta e="T91" id="Seg_1146" s="T90">conj</ta>
            <ta e="T92" id="Seg_1147" s="T91">v</ta>
            <ta e="T93" id="Seg_1148" s="T92">dempro</ta>
            <ta e="T94" id="Seg_1149" s="T93">n</ta>
            <ta e="T95" id="Seg_1150" s="T94">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_1151" s="T2">np.h:E</ta>
            <ta e="T5" id="Seg_1152" s="T4">pro.h:A</ta>
            <ta e="T7" id="Seg_1153" s="T6">np.h:Th</ta>
            <ta e="T9" id="Seg_1154" s="T8">0.2.h:E</ta>
            <ta e="T10" id="Seg_1155" s="T9">pro.h:Com</ta>
            <ta e="T11" id="Seg_1156" s="T10">pro.h:A</ta>
            <ta e="T12" id="Seg_1157" s="T11">pro.h:R</ta>
            <ta e="T13" id="Seg_1158" s="T12">np:Th</ta>
            <ta e="T15" id="Seg_1159" s="T14">pro.h:E</ta>
            <ta e="T17" id="Seg_1160" s="T16">0.3.h:E</ta>
            <ta e="T20" id="Seg_1161" s="T19">adv:Time</ta>
            <ta e="T21" id="Seg_1162" s="T20">0.1.h:A</ta>
            <ta e="T22" id="Seg_1163" s="T21">np:P</ta>
            <ta e="T24" id="Seg_1164" s="T23">n:Time</ta>
            <ta e="T25" id="Seg_1165" s="T24">0.3.h:A</ta>
            <ta e="T26" id="Seg_1166" s="T25">0.3.h:A</ta>
            <ta e="T27" id="Seg_1167" s="T26">np.h:R</ta>
            <ta e="T28" id="Seg_1168" s="T27">0.2.h:A</ta>
            <ta e="T29" id="Seg_1169" s="T28">pro.h:R</ta>
            <ta e="T31" id="Seg_1170" s="T30">pro.h:A</ta>
            <ta e="T33" id="Seg_1171" s="T32">np:P</ta>
            <ta e="T35" id="Seg_1172" s="T34">np:Ins</ta>
            <ta e="T36" id="Seg_1173" s="T35">pro.h:A</ta>
            <ta e="T38" id="Seg_1174" s="T37">pro.h:R</ta>
            <ta e="T48" id="Seg_1175" s="T47">0.1.h:A</ta>
            <ta e="T49" id="Seg_1176" s="T48">adv:Time</ta>
            <ta e="T50" id="Seg_1177" s="T49">0.3.h:A</ta>
            <ta e="T51" id="Seg_1178" s="T50">adv:Time</ta>
            <ta e="T54" id="Seg_1179" s="T53">n:Time</ta>
            <ta e="T56" id="Seg_1180" s="T55">adv:Time</ta>
            <ta e="T57" id="Seg_1181" s="T56">0.3.h:A</ta>
            <ta e="T60" id="Seg_1182" s="T59">np.h:A</ta>
            <ta e="T62" id="Seg_1183" s="T61">np:Th</ta>
            <ta e="T65" id="Seg_1184" s="T64">0.3.h:A</ta>
            <ta e="T68" id="Seg_1185" s="T67">pro.h:A</ta>
            <ta e="T72" id="Seg_1186" s="T71">pro.h:A</ta>
            <ta e="T80" id="Seg_1187" s="T79">0.3.h:A</ta>
            <ta e="T81" id="Seg_1188" s="T80">0.3.h:E</ta>
            <ta e="T83" id="Seg_1189" s="T82">n:Time</ta>
            <ta e="T84" id="Seg_1190" s="T83">adv:Time</ta>
            <ta e="T85" id="Seg_1191" s="T84">np.h:A</ta>
            <ta e="T86" id="Seg_1192" s="T85">adv:Time</ta>
            <ta e="T89" id="Seg_1193" s="T88">pro.h:R</ta>
            <ta e="T93" id="Seg_1194" s="T92">pro.h:A</ta>
            <ta e="T94" id="Seg_1195" s="T93">np.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_1196" s="T1">v:pred</ta>
            <ta e="T3" id="Seg_1197" s="T2">np.h:S</ta>
            <ta e="T5" id="Seg_1198" s="T4">pro.h:S</ta>
            <ta e="T6" id="Seg_1199" s="T5">v:pred</ta>
            <ta e="T7" id="Seg_1200" s="T6">np.h:O</ta>
            <ta e="T8" id="Seg_1201" s="T7">v:pred</ta>
            <ta e="T9" id="Seg_1202" s="T8">v:pred 0.2.h:S</ta>
            <ta e="T11" id="Seg_1203" s="T10">pro.h:S</ta>
            <ta e="T13" id="Seg_1204" s="T12">np:O</ta>
            <ta e="T14" id="Seg_1205" s="T13">v:pred</ta>
            <ta e="T15" id="Seg_1206" s="T14">pro.h:S</ta>
            <ta e="T16" id="Seg_1207" s="T15">v:pred</ta>
            <ta e="T17" id="Seg_1208" s="T16">v:pred 0.3.h:S</ta>
            <ta e="T21" id="Seg_1209" s="T20">v:pred 0.1.h:S</ta>
            <ta e="T22" id="Seg_1210" s="T21">np:O</ta>
            <ta e="T25" id="Seg_1211" s="T24">v:pred 0.3.h:S</ta>
            <ta e="T26" id="Seg_1212" s="T25">v:pred 0.3.h:S</ta>
            <ta e="T28" id="Seg_1213" s="T27">v:pred 0.2.h:S</ta>
            <ta e="T31" id="Seg_1214" s="T30">pro.h:S</ta>
            <ta e="T32" id="Seg_1215" s="T31">v:pred</ta>
            <ta e="T33" id="Seg_1216" s="T32">np:O</ta>
            <ta e="T36" id="Seg_1217" s="T35">pro.h:S</ta>
            <ta e="T37" id="Seg_1218" s="T36">v:pred</ta>
            <ta e="T44" id="Seg_1219" s="T43">ptcl:pred</ta>
            <ta e="T46" id="Seg_1220" s="T45">ptcl:pred</ta>
            <ta e="T50" id="Seg_1221" s="T49">v:pred 0.3.h:S</ta>
            <ta e="T52" id="Seg_1222" s="T51">ptcl:pred</ta>
            <ta e="T57" id="Seg_1223" s="T56">v:pred 0.3.h:S</ta>
            <ta e="T60" id="Seg_1224" s="T59">np.h:S</ta>
            <ta e="T61" id="Seg_1225" s="T60">v:pred</ta>
            <ta e="T62" id="Seg_1226" s="T61">np:O</ta>
            <ta e="T65" id="Seg_1227" s="T64">v:pred 0.3.h:S</ta>
            <ta e="T68" id="Seg_1228" s="T67">pro.h:S</ta>
            <ta e="T70" id="Seg_1229" s="T69">v:pred</ta>
            <ta e="T72" id="Seg_1230" s="T71">pro.h:S</ta>
            <ta e="T75" id="Seg_1231" s="T74">v:pred</ta>
            <ta e="T78" id="Seg_1232" s="T77">ptcl:pred</ta>
            <ta e="T80" id="Seg_1233" s="T79">v:pred 0.3.h:S</ta>
            <ta e="T81" id="Seg_1234" s="T80">v:pred 0.3.h:S</ta>
            <ta e="T85" id="Seg_1235" s="T84">np.h:S</ta>
            <ta e="T87" id="Seg_1236" s="T86">ptcl.neg</ta>
            <ta e="T88" id="Seg_1237" s="T87">v:pred</ta>
            <ta e="T92" id="Seg_1238" s="T91">v:pred</ta>
            <ta e="T93" id="Seg_1239" s="T92">pro.h:S</ta>
            <ta e="T94" id="Seg_1240" s="T93">np.h:O</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T3" id="Seg_1241" s="T2">TURK:cult</ta>
            <ta e="T41" id="Seg_1242" s="T40">RUS:gram</ta>
            <ta e="T44" id="Seg_1243" s="T43">RUS:gram</ta>
            <ta e="T46" id="Seg_1244" s="T45">RUS:gram</ta>
            <ta e="T47" id="Seg_1245" s="T46">RUS:mod</ta>
            <ta e="T52" id="Seg_1246" s="T51">RUS:gram</ta>
            <ta e="T53" id="Seg_1247" s="T52">RUS:mod</ta>
            <ta e="T63" id="Seg_1248" s="T62">RUS:gram</ta>
            <ta e="T66" id="Seg_1249" s="T65">RUS:gram</ta>
            <ta e="T71" id="Seg_1250" s="T70">RUS:gram</ta>
            <ta e="T76" id="Seg_1251" s="T75">RUS:gram</ta>
            <ta e="T78" id="Seg_1252" s="T77">RUS:mod</ta>
            <ta e="T79" id="Seg_1253" s="T78">RUS:gram</ta>
            <ta e="T82" id="Seg_1254" s="T81">RUS:gram</ta>
            <ta e="T85" id="Seg_1255" s="T84">TURK:cult</ta>
            <ta e="T91" id="Seg_1256" s="T90">RUS:gram</ta>
            <ta e="T94" id="Seg_1257" s="T93">TURK:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_1258" s="T0">Жил священник.</ta>
            <ta e="T6" id="Seg_1259" s="T3">Однажды он пошел.</ta>
            <ta e="T8" id="Seg_1260" s="T6">Привел человека [=крестьянина].</ta>
            <ta e="T14" id="Seg_1261" s="T8">"Живи [со] мной, я дам тебе деньги".</ta>
            <ta e="T19" id="Seg_1262" s="T14">Он жил, жил, одну неделю.</ta>
            <ta e="T23" id="Seg_1263" s="T19">Потом: "Пойдем косить траву!"</ta>
            <ta e="T25" id="Seg_1264" s="T23">Утром они встали.</ta>
            <ta e="T35" id="Seg_1265" s="T25">Он говорит матери: "Дай нам поесть, мы идем косить траву косой".</ta>
            <ta e="T40" id="Seg_1266" s="T35">Она дала ему еду.</ta>
            <ta e="T48" id="Seg_1267" s="T40">"Давай поедим, давай еще поедим!"</ta>
            <ta e="T50" id="Seg_1268" s="T48">Они сели.</ta>
            <ta e="T55" id="Seg_1269" s="T50">Потом: "Давай съедим ужин".</ta>
            <ta e="T58" id="Seg_1270" s="T55">Они поели (?).</ta>
            <ta e="T65" id="Seg_1271" s="T58">Тот человек взял свою одежду и пошел спать.</ta>
            <ta e="T70" id="Seg_1272" s="T65">"Почему ты идешь спать?"</ta>
            <ta e="T78" id="Seg_1273" s="T70">"А ты сам так сказал, что надо спать [потому что мы съели ужин]".</ta>
            <ta e="T81" id="Seg_1274" s="T78">И пошел, уснул.</ta>
            <ta e="T83" id="Seg_1275" s="T81">И с утра.</ta>
            <ta e="T90" id="Seg_1276" s="T83">После этого священник не дает ему больше есть.</ta>
            <ta e="T94" id="Seg_1277" s="T90">И [=так] обманул этого священника.</ta>
            <ta e="T95" id="Seg_1278" s="T94">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_1279" s="T0">A priest lived.</ta>
            <ta e="T6" id="Seg_1280" s="T3">Once he went.</ta>
            <ta e="T8" id="Seg_1281" s="T6">He brought a man [= a paysan].</ta>
            <ta e="T14" id="Seg_1282" s="T8">"Live [with] me, I will give you money."</ta>
            <ta e="T19" id="Seg_1283" s="T14">He lived, lived, one week.</ta>
            <ta e="T23" id="Seg_1284" s="T19">Then: "Let's go to cut hay!"</ta>
            <ta e="T25" id="Seg_1285" s="T23">They got up in the morning.</ta>
            <ta e="T35" id="Seg_1286" s="T25">He tells his mother: "Give us food, we will go to cut grass with a scythe".</ta>
            <ta e="T40" id="Seg_1287" s="T35">She gave them food.</ta>
            <ta e="T48" id="Seg_1288" s="T40">"Let's eat, let's eat some more!"</ta>
            <ta e="T50" id="Seg_1289" s="T48">Then they sat down.</ta>
            <ta e="T55" id="Seg_1290" s="T50">Then: "Let's eat supper [= evening meal]."</ta>
            <ta e="T58" id="Seg_1291" s="T55">Then they ate (?).</ta>
            <ta e="T65" id="Seg_1292" s="T58">The man takes his clothes and goes to sleep.</ta>
            <ta e="T70" id="Seg_1293" s="T65">"Why do you go to sleep?"</ta>
            <ta e="T78" id="Seg_1294" s="T70">"But you said yourself, that we should sleep [because we ate supper]."</ta>
            <ta e="T81" id="Seg_1295" s="T78">And he went, fell asleep.</ta>
            <ta e="T83" id="Seg_1296" s="T81">And from the morning.</ta>
            <ta e="T90" id="Seg_1297" s="T83">Then the priest now does not give him [anything] to eat.</ta>
            <ta e="T94" id="Seg_1298" s="T90">And [= so] he tricked the priest.</ta>
            <ta e="T95" id="Seg_1299" s="T94">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_1300" s="T0">Es lebte ein Priester.</ta>
            <ta e="T6" id="Seg_1301" s="T3">Einmal ging er.</ta>
            <ta e="T8" id="Seg_1302" s="T6">Er brachte einen Mann [= einen Bauer].</ta>
            <ta e="T14" id="Seg_1303" s="T8">"Lebe [mit] mir, ich werde dir Geld geben."</ta>
            <ta e="T19" id="Seg_1304" s="T14">Er lebte, lebte, eine Woche.</ta>
            <ta e="T23" id="Seg_1305" s="T19">Dann: "Lass uns Gras schneiden gehen!"</ta>
            <ta e="T25" id="Seg_1306" s="T23">Sie standen am Morgen auf.</ta>
            <ta e="T35" id="Seg_1307" s="T25">Er erzählt seiner Mutter: "Gib uns Essen, wir gehen, um Gras mit der Sense zu schneiden."</ta>
            <ta e="T40" id="Seg_1308" s="T35">Sie gab ihnen Essen.</ta>
            <ta e="T48" id="Seg_1309" s="T40">"Lass uns essen, lass uns etwas mehr essen!"</ta>
            <ta e="T50" id="Seg_1310" s="T48">Dann setzten sie sich.</ta>
            <ta e="T55" id="Seg_1311" s="T50">Dann: "Lass uns zu Abend essen."</ta>
            <ta e="T58" id="Seg_1312" s="T55">Dann aßen sie (?).</ta>
            <ta e="T65" id="Seg_1313" s="T58">Der Mann nahm seine Kleidung und geht schlafen.</ta>
            <ta e="T70" id="Seg_1314" s="T65">"Warum gehst du schlafen?"</ta>
            <ta e="T78" id="Seg_1315" s="T70">"Aber du hast selbst gesagt, dass schlafen sollen [weil wir zu Abend gegessen haben]."</ta>
            <ta e="T81" id="Seg_1316" s="T78">Und er ging, er schlief ein.</ta>
            <ta e="T83" id="Seg_1317" s="T81">Und vom Morgen an.</ta>
            <ta e="T90" id="Seg_1318" s="T83">Dann gibt ihm der Priester jetzt nichts mehr zu essen.</ta>
            <ta e="T94" id="Seg_1319" s="T90">Und [= so] betrog er den Priester.</ta>
            <ta e="T95" id="Seg_1320" s="T94">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T3" id="Seg_1321" s="T0">[GVY:] http://hyaenidae.narod.ru/story3/119.html</ta>
            <ta e="T35" id="Seg_1322" s="T25">[GVY:] Should be "to his wife".</ta>
            <ta e="T83" id="Seg_1323" s="T81">[GVY:] Should be "(he slept) till the [following] morning".</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
