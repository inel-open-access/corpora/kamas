<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID9C4CE083-2A40-1B07-E6FE-0827A62736ED">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_ManBeatsAGirl_flk.wav" />
         <referenced-file url="PKZ_196X_ManBeatsAGirl_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_ManBeatsAGirl_flk\PKZ_196X_ManBeatsAGirl_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">67</ud-information>
            <ud-information attribute-name="# HIAT:w">46</ud-information>
            <ud-information attribute-name="# e">45</ud-information>
            <ud-information attribute-name="# HIAT:u">11</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1556" time="0.068" type="appl" />
         <tli id="T1557" time="1.12" type="appl" />
         <tli id="T1558" time="2.172" type="appl" />
         <tli id="T1559" time="3.225" type="appl" />
         <tli id="T1560" time="4.277" type="appl" />
         <tli id="T1561" time="5.329" type="appl" />
         <tli id="T1562" time="9.893254122193401" />
         <tli id="T1563" time="10.704" type="appl" />
         <tli id="T1564" time="11.576" type="appl" />
         <tli id="T1565" time="12.448" type="appl" />
         <tli id="T1566" time="13.32" type="appl" />
         <tli id="T1567" time="14.192" type="appl" />
         <tli id="T1568" time="15.19321168765415" />
         <tli id="T1569" time="15.803" type="appl" />
         <tli id="T1570" time="16.387" type="appl" />
         <tli id="T1571" time="17.24652858093957" />
         <tli id="T1572" time="17.849" type="appl" />
         <tli id="T1573" time="18.592" type="appl" />
         <tli id="T1574" time="19.334" type="appl" />
         <tli id="T1575" time="20.186505041780066" />
         <tli id="T1576" time="20.884" type="appl" />
         <tli id="T1577" time="21.573" type="appl" />
         <tli id="T1578" time="22.353154360993578" />
         <tli id="T1579" time="23.215" type="appl" />
         <tli id="T1580" time="24.399804640989114" />
         <tli id="T1581" time="24.936" type="appl" />
         <tli id="T1582" time="25.678" type="appl" />
         <tli id="T1583" time="26.42" type="appl" />
         <tli id="T1584" time="27.162" type="appl" />
         <tli id="T1585" time="27.905" type="appl" />
         <tli id="T1586" time="28.647" type="appl" />
         <tli id="T1587" time="29.389" type="appl" />
         <tli id="T1588" time="30.131" type="appl" />
         <tli id="T1589" time="31.066417930876852" />
         <tli id="T1590" time="31.716" type="appl" />
         <tli id="T1591" time="32.425" type="appl" />
         <tli id="T1592" time="33.078" type="appl" />
         <tli id="T1593" time="33.73" type="appl" />
         <tli id="T1594" time="34.383" type="appl" />
         <tli id="T1595" time="35.036" type="appl" />
         <tli id="T1596" time="35.688" type="appl" />
         <tli id="T1597" time="36.341" type="appl" />
         <tli id="T1598" time="36.877" type="appl" />
         <tli id="T1599" time="37.414" type="appl" />
         <tli id="T1600" time="38.026362205519646" />
         <tli id="T1601" time="39.193" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T1590" start="T1589">
            <tli id="T1589.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T1601" id="Seg_0" n="sc" s="T1556">
               <ts e="T1562" id="Seg_2" n="HIAT:u" s="T1556">
                  <ts e="T1557" id="Seg_4" n="HIAT:w" s="T1556">Măn</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1558" id="Seg_7" n="HIAT:w" s="T1557">mʼegluʔpim</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1559" id="Seg_10" n="HIAT:w" s="T1558">bugaʔi</ts>
                  <nts id="Seg_11" n="HIAT:ip">,</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1560" id="Seg_14" n="HIAT:w" s="T1559">dĭzeŋ</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1561" id="Seg_17" n="HIAT:w" s="T1560">bar</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1562" id="Seg_20" n="HIAT:w" s="T1561">mürerleʔbəʔjə</ts>
                  <nts id="Seg_21" n="HIAT:ip">.</nts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1568" id="Seg_24" n="HIAT:u" s="T1562">
                  <ts e="T1563" id="Seg_26" n="HIAT:w" s="T1562">Onʼiʔ</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1564" id="Seg_29" n="HIAT:w" s="T1563">kuza</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1565" id="Seg_32" n="HIAT:w" s="T1564">öʔlüʔbi</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1566" id="Seg_35" n="HIAT:w" s="T1565">bostə</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1567" id="Seg_38" n="HIAT:w" s="T1566">koʔbdo</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1568" id="Seg_41" n="HIAT:w" s="T1567">büjleʔ</ts>
                  <nts id="Seg_42" n="HIAT:ip">.</nts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1571" id="Seg_45" n="HIAT:u" s="T1568">
                  <nts id="Seg_46" n="HIAT:ip">"</nts>
                  <ts e="T1569" id="Seg_48" n="HIAT:w" s="T1568">Kanaʔ</ts>
                  <nts id="Seg_49" n="HIAT:ip">,</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1570" id="Seg_52" n="HIAT:w" s="T1569">bü</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1571" id="Seg_55" n="HIAT:w" s="T1570">deʔtə</ts>
                  <nts id="Seg_56" n="HIAT:ip">!</nts>
                  <nts id="Seg_57" n="HIAT:ip">"</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1575" id="Seg_60" n="HIAT:u" s="T1571">
                  <ts e="T1572" id="Seg_62" n="HIAT:w" s="T1571">Dĭgəttə</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1573" id="Seg_65" n="HIAT:w" s="T1572">davaj</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1574" id="Seg_68" n="HIAT:w" s="T1573">dĭm</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1575" id="Seg_71" n="HIAT:w" s="T1574">münörzittə</ts>
                  <nts id="Seg_72" n="HIAT:ip">.</nts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1578" id="Seg_75" n="HIAT:u" s="T1575">
                  <ts e="T1576" id="Seg_77" n="HIAT:w" s="T1575">A</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1577" id="Seg_80" n="HIAT:w" s="T1576">kuza</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1578" id="Seg_83" n="HIAT:w" s="T1577">šonəga</ts>
                  <nts id="Seg_84" n="HIAT:ip">.</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1580" id="Seg_87" n="HIAT:u" s="T1578">
                  <nts id="Seg_88" n="HIAT:ip">"</nts>
                  <ts e="T1579" id="Seg_90" n="HIAT:w" s="T1578">Ĭmbi</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1580" id="Seg_93" n="HIAT:w" s="T1579">münörleʔbəl</ts>
                  <nts id="Seg_94" n="HIAT:ip">?</nts>
                  <nts id="Seg_95" n="HIAT:ip">"</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1589" id="Seg_98" n="HIAT:u" s="T1580">
                  <nts id="Seg_99" n="HIAT:ip">"</nts>
                  <ts e="T1581" id="Seg_101" n="HIAT:w" s="T1580">Da</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1582" id="Seg_104" n="HIAT:w" s="T1581">măn</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1583" id="Seg_107" n="HIAT:w" s="T1582">münörleʔbəm</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1584" id="Seg_110" n="HIAT:w" s="T1583">štobɨ</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1585" id="Seg_113" n="HIAT:w" s="T1584">ej</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1586" id="Seg_116" n="HIAT:w" s="T1585">băldəbi</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1587" id="Seg_119" n="HIAT:w" s="T1586">dĭ</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1588" id="Seg_122" n="HIAT:w" s="T1587">šö</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1589" id="Seg_125" n="HIAT:w" s="T1588">šojdʼo</ts>
                  <nts id="Seg_126" n="HIAT:ip">.</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1591" id="Seg_129" n="HIAT:u" s="T1589">
                  <ts e="T1589.tx.1" id="Seg_131" n="HIAT:w" s="T1589">A</ts>
                  <nts id="Seg_132" n="HIAT:ip">_</nts>
                  <ts e="T1590" id="Seg_134" n="HIAT:w" s="T1589.tx.1">to</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1591" id="Seg_137" n="HIAT:w" s="T1590">bălluʔləj</ts>
                  <nts id="Seg_138" n="HIAT:ip">.</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1597" id="Seg_141" n="HIAT:u" s="T1591">
                  <ts e="T1592" id="Seg_143" n="HIAT:w" s="T1591">Dĭgəttə</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1593" id="Seg_146" n="HIAT:w" s="T1592">ĭmbi</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1594" id="Seg_149" n="HIAT:w" s="T1593">münörzittə</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1595" id="Seg_152" n="HIAT:w" s="T1594">kamən</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_154" n="HIAT:ip">(</nts>
                  <ts e="T1596" id="Seg_156" n="HIAT:w" s="T1595">moʔ-</ts>
                  <nts id="Seg_157" n="HIAT:ip">)</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1597" id="Seg_160" n="HIAT:w" s="T1596">bălluʔləj</ts>
                  <nts id="Seg_161" n="HIAT:ip">.</nts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1600" id="Seg_164" n="HIAT:u" s="T1597">
                  <ts e="T1598" id="Seg_166" n="HIAT:w" s="T1597">Tuj</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1599" id="Seg_169" n="HIAT:w" s="T1598">nada</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1600" id="Seg_172" n="HIAT:w" s="T1599">münörzittə</ts>
                  <nts id="Seg_173" n="HIAT:ip">"</nts>
                  <nts id="Seg_174" n="HIAT:ip">.</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1601" id="Seg_177" n="HIAT:u" s="T1600">
                  <ts e="T1601" id="Seg_179" n="HIAT:w" s="T1600">Kabarləj</ts>
                  <nts id="Seg_180" n="HIAT:ip">.</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T1601" id="Seg_182" n="sc" s="T1556">
               <ts e="T1557" id="Seg_184" n="e" s="T1556">Măn </ts>
               <ts e="T1558" id="Seg_186" n="e" s="T1557">mʼegluʔpim </ts>
               <ts e="T1559" id="Seg_188" n="e" s="T1558">bugaʔi, </ts>
               <ts e="T1560" id="Seg_190" n="e" s="T1559">dĭzeŋ </ts>
               <ts e="T1561" id="Seg_192" n="e" s="T1560">bar </ts>
               <ts e="T1562" id="Seg_194" n="e" s="T1561">mürerleʔbəʔjə. </ts>
               <ts e="T1563" id="Seg_196" n="e" s="T1562">Onʼiʔ </ts>
               <ts e="T1564" id="Seg_198" n="e" s="T1563">kuza </ts>
               <ts e="T1565" id="Seg_200" n="e" s="T1564">öʔlüʔbi </ts>
               <ts e="T1566" id="Seg_202" n="e" s="T1565">bostə </ts>
               <ts e="T1567" id="Seg_204" n="e" s="T1566">koʔbdo </ts>
               <ts e="T1568" id="Seg_206" n="e" s="T1567">büjleʔ. </ts>
               <ts e="T1569" id="Seg_208" n="e" s="T1568">"Kanaʔ, </ts>
               <ts e="T1570" id="Seg_210" n="e" s="T1569">bü </ts>
               <ts e="T1571" id="Seg_212" n="e" s="T1570">deʔtə!" </ts>
               <ts e="T1572" id="Seg_214" n="e" s="T1571">Dĭgəttə </ts>
               <ts e="T1573" id="Seg_216" n="e" s="T1572">davaj </ts>
               <ts e="T1574" id="Seg_218" n="e" s="T1573">dĭm </ts>
               <ts e="T1575" id="Seg_220" n="e" s="T1574">münörzittə. </ts>
               <ts e="T1576" id="Seg_222" n="e" s="T1575">A </ts>
               <ts e="T1577" id="Seg_224" n="e" s="T1576">kuza </ts>
               <ts e="T1578" id="Seg_226" n="e" s="T1577">šonəga. </ts>
               <ts e="T1579" id="Seg_228" n="e" s="T1578">"Ĭmbi </ts>
               <ts e="T1580" id="Seg_230" n="e" s="T1579">münörleʔbəl?" </ts>
               <ts e="T1581" id="Seg_232" n="e" s="T1580">"Da </ts>
               <ts e="T1582" id="Seg_234" n="e" s="T1581">măn </ts>
               <ts e="T1583" id="Seg_236" n="e" s="T1582">münörleʔbəm </ts>
               <ts e="T1584" id="Seg_238" n="e" s="T1583">štobɨ </ts>
               <ts e="T1585" id="Seg_240" n="e" s="T1584">ej </ts>
               <ts e="T1586" id="Seg_242" n="e" s="T1585">băldəbi </ts>
               <ts e="T1587" id="Seg_244" n="e" s="T1586">dĭ </ts>
               <ts e="T1588" id="Seg_246" n="e" s="T1587">šö </ts>
               <ts e="T1589" id="Seg_248" n="e" s="T1588">šojdʼo. </ts>
               <ts e="T1590" id="Seg_250" n="e" s="T1589">A_to </ts>
               <ts e="T1591" id="Seg_252" n="e" s="T1590">bălluʔləj. </ts>
               <ts e="T1592" id="Seg_254" n="e" s="T1591">Dĭgəttə </ts>
               <ts e="T1593" id="Seg_256" n="e" s="T1592">ĭmbi </ts>
               <ts e="T1594" id="Seg_258" n="e" s="T1593">münörzittə </ts>
               <ts e="T1595" id="Seg_260" n="e" s="T1594">kamən </ts>
               <ts e="T1596" id="Seg_262" n="e" s="T1595">(moʔ-) </ts>
               <ts e="T1597" id="Seg_264" n="e" s="T1596">bălluʔləj. </ts>
               <ts e="T1598" id="Seg_266" n="e" s="T1597">Tuj </ts>
               <ts e="T1599" id="Seg_268" n="e" s="T1598">nada </ts>
               <ts e="T1600" id="Seg_270" n="e" s="T1599">münörzittə". </ts>
               <ts e="T1601" id="Seg_272" n="e" s="T1600">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T1562" id="Seg_273" s="T1556">PKZ_196X_ManBeatsAGirl_flk.001 (001)</ta>
            <ta e="T1568" id="Seg_274" s="T1562">PKZ_196X_ManBeatsAGirl_flk.002 (002)</ta>
            <ta e="T1571" id="Seg_275" s="T1568">PKZ_196X_ManBeatsAGirl_flk.003 (003)</ta>
            <ta e="T1575" id="Seg_276" s="T1571">PKZ_196X_ManBeatsAGirl_flk.004 (004)</ta>
            <ta e="T1578" id="Seg_277" s="T1575">PKZ_196X_ManBeatsAGirl_flk.005 (005)</ta>
            <ta e="T1580" id="Seg_278" s="T1578">PKZ_196X_ManBeatsAGirl_flk.006 (006)</ta>
            <ta e="T1589" id="Seg_279" s="T1580">PKZ_196X_ManBeatsAGirl_flk.007 (007)</ta>
            <ta e="T1591" id="Seg_280" s="T1589">PKZ_196X_ManBeatsAGirl_flk.008 (008)</ta>
            <ta e="T1597" id="Seg_281" s="T1591">PKZ_196X_ManBeatsAGirl_flk.009 (009)</ta>
            <ta e="T1600" id="Seg_282" s="T1597">PKZ_196X_ManBeatsAGirl_flk.010 (010)</ta>
            <ta e="T1601" id="Seg_283" s="T1600">PKZ_196X_ManBeatsAGirl_flk.011 (011)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T1562" id="Seg_284" s="T1556">Măn mʼegluʔpim bugaʔi, dĭzeŋ bar mürerleʔbəʔjə. </ta>
            <ta e="T1568" id="Seg_285" s="T1562">Onʼiʔ kuza öʔlüʔbi bostə koʔbdo büjleʔ. </ta>
            <ta e="T1571" id="Seg_286" s="T1568">"Kanaʔ, bü deʔtə!" </ta>
            <ta e="T1575" id="Seg_287" s="T1571">Dĭgəttə davaj dĭm münörzittə. </ta>
            <ta e="T1578" id="Seg_288" s="T1575">A kuza šonəga. </ta>
            <ta e="T1580" id="Seg_289" s="T1578">"Ĭmbi münörleʔbəl?" </ta>
            <ta e="T1589" id="Seg_290" s="T1580">"Da măn münörleʔbəm štobɨ ej băldəbi dĭ šö šojdʼo. </ta>
            <ta e="T1591" id="Seg_291" s="T1589">A to bălluʔləj. </ta>
            <ta e="T1597" id="Seg_292" s="T1591">Dĭgəttə ĭmbi münörzittə kamən (moʔ-) bălluʔləj. </ta>
            <ta e="T1600" id="Seg_293" s="T1597">Tuj nada münörzittə". </ta>
            <ta e="T1601" id="Seg_294" s="T1600">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1557" id="Seg_295" s="T1556">măn</ta>
            <ta e="T1558" id="Seg_296" s="T1557">mʼeg-luʔ-pi-m</ta>
            <ta e="T1559" id="Seg_297" s="T1558">buga-ʔi</ta>
            <ta e="T1560" id="Seg_298" s="T1559">dĭ-zeŋ</ta>
            <ta e="T1561" id="Seg_299" s="T1560">bar</ta>
            <ta e="T1562" id="Seg_300" s="T1561">müre-r-leʔbə-ʔjə</ta>
            <ta e="T1563" id="Seg_301" s="T1562">onʼiʔ</ta>
            <ta e="T1564" id="Seg_302" s="T1563">kuza</ta>
            <ta e="T1565" id="Seg_303" s="T1564">öʔ-lüʔ-bi</ta>
            <ta e="T1566" id="Seg_304" s="T1565">bos-tə</ta>
            <ta e="T1567" id="Seg_305" s="T1566">koʔbdo</ta>
            <ta e="T1568" id="Seg_306" s="T1567">bü-j-leʔ</ta>
            <ta e="T1569" id="Seg_307" s="T1568">kan-a-ʔ</ta>
            <ta e="T1570" id="Seg_308" s="T1569">bü</ta>
            <ta e="T1571" id="Seg_309" s="T1570">deʔ-tə</ta>
            <ta e="T1572" id="Seg_310" s="T1571">dĭgəttə</ta>
            <ta e="T1573" id="Seg_311" s="T1572">davaj</ta>
            <ta e="T1574" id="Seg_312" s="T1573">dĭ-m</ta>
            <ta e="T1575" id="Seg_313" s="T1574">münör-zittə</ta>
            <ta e="T1576" id="Seg_314" s="T1575">a</ta>
            <ta e="T1577" id="Seg_315" s="T1576">kuza</ta>
            <ta e="T1578" id="Seg_316" s="T1577">šonə-ga</ta>
            <ta e="T1579" id="Seg_317" s="T1578">ĭmbi</ta>
            <ta e="T1580" id="Seg_318" s="T1579">münör-leʔbə-l</ta>
            <ta e="T1581" id="Seg_319" s="T1580">da</ta>
            <ta e="T1582" id="Seg_320" s="T1581">măn</ta>
            <ta e="T1583" id="Seg_321" s="T1582">münör-leʔbə-m</ta>
            <ta e="T1584" id="Seg_322" s="T1583">štobɨ</ta>
            <ta e="T1585" id="Seg_323" s="T1584">ej</ta>
            <ta e="T1586" id="Seg_324" s="T1585">băldə-bi</ta>
            <ta e="T1587" id="Seg_325" s="T1586">dĭ</ta>
            <ta e="T1588" id="Seg_326" s="T1587">šö</ta>
            <ta e="T1589" id="Seg_327" s="T1588">šojdʼo</ta>
            <ta e="T1590" id="Seg_328" s="T1589">ato</ta>
            <ta e="T1591" id="Seg_329" s="T1590">băl-luʔ-lə-j</ta>
            <ta e="T1592" id="Seg_330" s="T1591">dĭgəttə</ta>
            <ta e="T1593" id="Seg_331" s="T1592">ĭmbi</ta>
            <ta e="T1594" id="Seg_332" s="T1593">münör-zittə</ta>
            <ta e="T1595" id="Seg_333" s="T1594">kamən</ta>
            <ta e="T1597" id="Seg_334" s="T1596">băl-luʔ-lə-j</ta>
            <ta e="T1598" id="Seg_335" s="T1597">tuj</ta>
            <ta e="T1599" id="Seg_336" s="T1598">nada</ta>
            <ta e="T1600" id="Seg_337" s="T1599">münör-zittə</ta>
            <ta e="T1601" id="Seg_338" s="T1600">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1557" id="Seg_339" s="T1556">măn</ta>
            <ta e="T1558" id="Seg_340" s="T1557">mʼeg-luʔbdə-bi-m</ta>
            <ta e="T1559" id="Seg_341" s="T1558">buga-jəʔ</ta>
            <ta e="T1560" id="Seg_342" s="T1559">dĭ-zAŋ</ta>
            <ta e="T1561" id="Seg_343" s="T1560">bar</ta>
            <ta e="T1562" id="Seg_344" s="T1561">müre-r-laʔbə-jəʔ</ta>
            <ta e="T1563" id="Seg_345" s="T1562">onʼiʔ</ta>
            <ta e="T1564" id="Seg_346" s="T1563">kuza</ta>
            <ta e="T1565" id="Seg_347" s="T1564">öʔ-luʔbdə-bi</ta>
            <ta e="T1566" id="Seg_348" s="T1565">bos-də</ta>
            <ta e="T1567" id="Seg_349" s="T1566">koʔbdo</ta>
            <ta e="T1568" id="Seg_350" s="T1567">bü-j-lAʔ</ta>
            <ta e="T1569" id="Seg_351" s="T1568">kan-ə-ʔ</ta>
            <ta e="T1570" id="Seg_352" s="T1569">bü</ta>
            <ta e="T1571" id="Seg_353" s="T1570">det-t</ta>
            <ta e="T1572" id="Seg_354" s="T1571">dĭgəttə</ta>
            <ta e="T1573" id="Seg_355" s="T1572">davaj</ta>
            <ta e="T1574" id="Seg_356" s="T1573">dĭ-m</ta>
            <ta e="T1575" id="Seg_357" s="T1574">münör-zittə</ta>
            <ta e="T1576" id="Seg_358" s="T1575">a</ta>
            <ta e="T1577" id="Seg_359" s="T1576">kuza</ta>
            <ta e="T1578" id="Seg_360" s="T1577">šonə-gA</ta>
            <ta e="T1579" id="Seg_361" s="T1578">ĭmbi</ta>
            <ta e="T1580" id="Seg_362" s="T1579">münör-laʔbə-l</ta>
            <ta e="T1581" id="Seg_363" s="T1580">da</ta>
            <ta e="T1582" id="Seg_364" s="T1581">măn</ta>
            <ta e="T1583" id="Seg_365" s="T1582">münör-laʔbə-m</ta>
            <ta e="T1584" id="Seg_366" s="T1583">štobɨ</ta>
            <ta e="T1585" id="Seg_367" s="T1584">ej</ta>
            <ta e="T1586" id="Seg_368" s="T1585">băldə-bi</ta>
            <ta e="T1587" id="Seg_369" s="T1586">dĭ</ta>
            <ta e="T1588" id="Seg_370" s="T1587">šö</ta>
            <ta e="T1589" id="Seg_371" s="T1588">šojdʼo</ta>
            <ta e="T1590" id="Seg_372" s="T1589">ato</ta>
            <ta e="T1591" id="Seg_373" s="T1590">băldə-luʔbdə-lV-j</ta>
            <ta e="T1592" id="Seg_374" s="T1591">dĭgəttə</ta>
            <ta e="T1593" id="Seg_375" s="T1592">ĭmbi</ta>
            <ta e="T1594" id="Seg_376" s="T1593">münör-zittə</ta>
            <ta e="T1595" id="Seg_377" s="T1594">kamən</ta>
            <ta e="T1597" id="Seg_378" s="T1596">băldə-luʔbdə-lV-j</ta>
            <ta e="T1598" id="Seg_379" s="T1597">tüj</ta>
            <ta e="T1599" id="Seg_380" s="T1598">nadə</ta>
            <ta e="T1600" id="Seg_381" s="T1599">münör-zittə</ta>
            <ta e="T1601" id="Seg_382" s="T1600">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1557" id="Seg_383" s="T1556">I.NOM</ta>
            <ta e="T1558" id="Seg_384" s="T1557">lure-MOM-PST-1SG</ta>
            <ta e="T1559" id="Seg_385" s="T1558">bull-PL</ta>
            <ta e="T1560" id="Seg_386" s="T1559">this-PL</ta>
            <ta e="T1561" id="Seg_387" s="T1560">PTCL</ta>
            <ta e="T1562" id="Seg_388" s="T1561">bellow-FRQ-DUR-3PL</ta>
            <ta e="T1563" id="Seg_389" s="T1562">one.[NOM.SG]</ta>
            <ta e="T1564" id="Seg_390" s="T1563">man.[NOM.SG]</ta>
            <ta e="T1565" id="Seg_391" s="T1564">send-MOM-PST.[3SG]</ta>
            <ta e="T1566" id="Seg_392" s="T1565">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T1567" id="Seg_393" s="T1566">girl.[NOM.SG]</ta>
            <ta e="T1568" id="Seg_394" s="T1567">water-VBLZ-CVB</ta>
            <ta e="T1569" id="Seg_395" s="T1568">go-EP-IMP.2SG</ta>
            <ta e="T1570" id="Seg_396" s="T1569">water.[NOM.SG]</ta>
            <ta e="T1571" id="Seg_397" s="T1570">bring-IMP.2SG.O</ta>
            <ta e="T1572" id="Seg_398" s="T1571">then</ta>
            <ta e="T1573" id="Seg_399" s="T1572">INCH</ta>
            <ta e="T1574" id="Seg_400" s="T1573">this-ACC</ta>
            <ta e="T1575" id="Seg_401" s="T1574">beat-INF.LAT</ta>
            <ta e="T1576" id="Seg_402" s="T1575">and</ta>
            <ta e="T1577" id="Seg_403" s="T1576">man.[NOM.SG]</ta>
            <ta e="T1578" id="Seg_404" s="T1577">come-PRS.[3SG]</ta>
            <ta e="T1579" id="Seg_405" s="T1578">what.[NOM.SG]</ta>
            <ta e="T1580" id="Seg_406" s="T1579">beat-DUR-2SG</ta>
            <ta e="T1581" id="Seg_407" s="T1580">and</ta>
            <ta e="T1582" id="Seg_408" s="T1581">I.NOM</ta>
            <ta e="T1583" id="Seg_409" s="T1582">beat-DUR-1SG</ta>
            <ta e="T1584" id="Seg_410" s="T1583">so.that</ta>
            <ta e="T1585" id="Seg_411" s="T1584">NEG</ta>
            <ta e="T1586" id="Seg_412" s="T1585">break-PST.[3SG]</ta>
            <ta e="T1587" id="Seg_413" s="T1586">this.[NOM.SG]</ta>
            <ta e="T1588" id="Seg_414" s="T1587">that.[NOM.SG]</ta>
            <ta e="T1589" id="Seg_415" s="T1588">birchbark.vessel.[NOM.SG]</ta>
            <ta e="T1590" id="Seg_416" s="T1589">otherwise</ta>
            <ta e="T1591" id="Seg_417" s="T1590">break-MOM-FUT-3SG</ta>
            <ta e="T1592" id="Seg_418" s="T1591">then</ta>
            <ta e="T1593" id="Seg_419" s="T1592">what.[NOM.SG]</ta>
            <ta e="T1594" id="Seg_420" s="T1593">beat-INF.LAT</ta>
            <ta e="T1595" id="Seg_421" s="T1594">when</ta>
            <ta e="T1597" id="Seg_422" s="T1596">break-MOM-FUT-3SG</ta>
            <ta e="T1598" id="Seg_423" s="T1597">now</ta>
            <ta e="T1599" id="Seg_424" s="T1598">one.should</ta>
            <ta e="T1600" id="Seg_425" s="T1599">beat-INF.LAT</ta>
            <ta e="T1601" id="Seg_426" s="T1600">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1557" id="Seg_427" s="T1556">я.NOM</ta>
            <ta e="T1558" id="Seg_428" s="T1557">приманивать-MOM-PST-1SG</ta>
            <ta e="T1559" id="Seg_429" s="T1558">бык-PL</ta>
            <ta e="T1560" id="Seg_430" s="T1559">этот-PL</ta>
            <ta e="T1561" id="Seg_431" s="T1560">PTCL</ta>
            <ta e="T1562" id="Seg_432" s="T1561">мычать-FRQ-DUR-3PL</ta>
            <ta e="T1563" id="Seg_433" s="T1562">один.[NOM.SG]</ta>
            <ta e="T1564" id="Seg_434" s="T1563">мужчина.[NOM.SG]</ta>
            <ta e="T1565" id="Seg_435" s="T1564">послать-MOM-PST.[3SG]</ta>
            <ta e="T1566" id="Seg_436" s="T1565">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T1567" id="Seg_437" s="T1566">девушка.[NOM.SG]</ta>
            <ta e="T1568" id="Seg_438" s="T1567">вода-VBLZ-CVB</ta>
            <ta e="T1569" id="Seg_439" s="T1568">пойти-EP-IMP.2SG</ta>
            <ta e="T1570" id="Seg_440" s="T1569">вода.[NOM.SG]</ta>
            <ta e="T1571" id="Seg_441" s="T1570">принести-IMP.2SG.O</ta>
            <ta e="T1572" id="Seg_442" s="T1571">тогда</ta>
            <ta e="T1573" id="Seg_443" s="T1572">INCH</ta>
            <ta e="T1574" id="Seg_444" s="T1573">этот-ACC</ta>
            <ta e="T1575" id="Seg_445" s="T1574">бить-INF.LAT</ta>
            <ta e="T1576" id="Seg_446" s="T1575">а</ta>
            <ta e="T1577" id="Seg_447" s="T1576">мужчина.[NOM.SG]</ta>
            <ta e="T1578" id="Seg_448" s="T1577">прийти-PRS.[3SG]</ta>
            <ta e="T1579" id="Seg_449" s="T1578">что.[NOM.SG]</ta>
            <ta e="T1580" id="Seg_450" s="T1579">бить-DUR-2SG</ta>
            <ta e="T1581" id="Seg_451" s="T1580">и</ta>
            <ta e="T1582" id="Seg_452" s="T1581">я.NOM</ta>
            <ta e="T1583" id="Seg_453" s="T1582">бить-DUR-1SG</ta>
            <ta e="T1584" id="Seg_454" s="T1583">чтобы</ta>
            <ta e="T1585" id="Seg_455" s="T1584">NEG</ta>
            <ta e="T1586" id="Seg_456" s="T1585">сломать-PST.[3SG]</ta>
            <ta e="T1587" id="Seg_457" s="T1586">этот.[NOM.SG]</ta>
            <ta e="T1588" id="Seg_458" s="T1587">тот.[NOM.SG]</ta>
            <ta e="T1589" id="Seg_459" s="T1588">туес.[NOM.SG]</ta>
            <ta e="T1590" id="Seg_460" s="T1589">а.то</ta>
            <ta e="T1591" id="Seg_461" s="T1590">сломать-MOM-FUT-3SG</ta>
            <ta e="T1592" id="Seg_462" s="T1591">тогда</ta>
            <ta e="T1593" id="Seg_463" s="T1592">что.[NOM.SG]</ta>
            <ta e="T1594" id="Seg_464" s="T1593">бить-INF.LAT</ta>
            <ta e="T1595" id="Seg_465" s="T1594">когда</ta>
            <ta e="T1597" id="Seg_466" s="T1596">сломать-MOM-FUT-3SG</ta>
            <ta e="T1598" id="Seg_467" s="T1597">сейчас</ta>
            <ta e="T1599" id="Seg_468" s="T1598">надо</ta>
            <ta e="T1600" id="Seg_469" s="T1599">бить-INF.LAT</ta>
            <ta e="T1601" id="Seg_470" s="T1600">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1557" id="Seg_471" s="T1556">pers</ta>
            <ta e="T1558" id="Seg_472" s="T1557">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1559" id="Seg_473" s="T1558">n-n:num</ta>
            <ta e="T1560" id="Seg_474" s="T1559">dempro-n:num</ta>
            <ta e="T1561" id="Seg_475" s="T1560">ptcl</ta>
            <ta e="T1562" id="Seg_476" s="T1561">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T1563" id="Seg_477" s="T1562">num-n:case</ta>
            <ta e="T1564" id="Seg_478" s="T1563">n-n:case</ta>
            <ta e="T1565" id="Seg_479" s="T1564">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1566" id="Seg_480" s="T1565">refl-n:case.poss</ta>
            <ta e="T1567" id="Seg_481" s="T1566">n-n:case</ta>
            <ta e="T1568" id="Seg_482" s="T1567">n-n&gt;v-v:n.fin</ta>
            <ta e="T1569" id="Seg_483" s="T1568">v-v:ins-v:mood.pn</ta>
            <ta e="T1570" id="Seg_484" s="T1569">n-n:case</ta>
            <ta e="T1571" id="Seg_485" s="T1570">v-v:mood.pn</ta>
            <ta e="T1572" id="Seg_486" s="T1571">adv</ta>
            <ta e="T1573" id="Seg_487" s="T1572">ptcl</ta>
            <ta e="T1574" id="Seg_488" s="T1573">dempro-n:case</ta>
            <ta e="T1575" id="Seg_489" s="T1574">v-v:n.fin</ta>
            <ta e="T1576" id="Seg_490" s="T1575">conj</ta>
            <ta e="T1577" id="Seg_491" s="T1576">n-n:case</ta>
            <ta e="T1578" id="Seg_492" s="T1577">v-v:tense-v:pn</ta>
            <ta e="T1579" id="Seg_493" s="T1578">que-n:case</ta>
            <ta e="T1580" id="Seg_494" s="T1579">v-v&gt;v-v:pn</ta>
            <ta e="T1581" id="Seg_495" s="T1580">conj</ta>
            <ta e="T1582" id="Seg_496" s="T1581">pers</ta>
            <ta e="T1583" id="Seg_497" s="T1582">v-v&gt;v-v:pn</ta>
            <ta e="T1584" id="Seg_498" s="T1583">conj</ta>
            <ta e="T1585" id="Seg_499" s="T1584">ptcl</ta>
            <ta e="T1586" id="Seg_500" s="T1585">v-v:tense-v:pn</ta>
            <ta e="T1587" id="Seg_501" s="T1586">dempro-n:case</ta>
            <ta e="T1588" id="Seg_502" s="T1587">dempro-n:case</ta>
            <ta e="T1589" id="Seg_503" s="T1588">n-n:case</ta>
            <ta e="T1590" id="Seg_504" s="T1589">ptcl</ta>
            <ta e="T1591" id="Seg_505" s="T1590">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1592" id="Seg_506" s="T1591">adv</ta>
            <ta e="T1593" id="Seg_507" s="T1592">que-n:case</ta>
            <ta e="T1594" id="Seg_508" s="T1593">v-v:n.fin</ta>
            <ta e="T1595" id="Seg_509" s="T1594">que</ta>
            <ta e="T1597" id="Seg_510" s="T1596">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1598" id="Seg_511" s="T1597">adv</ta>
            <ta e="T1599" id="Seg_512" s="T1598">ptcl</ta>
            <ta e="T1600" id="Seg_513" s="T1599">v-v:n.fin</ta>
            <ta e="T1601" id="Seg_514" s="T1600">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1557" id="Seg_515" s="T1556">pers</ta>
            <ta e="T1558" id="Seg_516" s="T1557">v</ta>
            <ta e="T1559" id="Seg_517" s="T1558">n</ta>
            <ta e="T1560" id="Seg_518" s="T1559">dempro</ta>
            <ta e="T1561" id="Seg_519" s="T1560">ptcl</ta>
            <ta e="T1562" id="Seg_520" s="T1561">v</ta>
            <ta e="T1563" id="Seg_521" s="T1562">num</ta>
            <ta e="T1564" id="Seg_522" s="T1563">n</ta>
            <ta e="T1565" id="Seg_523" s="T1564">v</ta>
            <ta e="T1566" id="Seg_524" s="T1565">refl</ta>
            <ta e="T1567" id="Seg_525" s="T1566">n</ta>
            <ta e="T1568" id="Seg_526" s="T1567">v</ta>
            <ta e="T1569" id="Seg_527" s="T1568">v</ta>
            <ta e="T1570" id="Seg_528" s="T1569">n</ta>
            <ta e="T1571" id="Seg_529" s="T1570">v</ta>
            <ta e="T1572" id="Seg_530" s="T1571">adv</ta>
            <ta e="T1573" id="Seg_531" s="T1572">ptcl</ta>
            <ta e="T1574" id="Seg_532" s="T1573">dempro</ta>
            <ta e="T1575" id="Seg_533" s="T1574">v</ta>
            <ta e="T1576" id="Seg_534" s="T1575">conj</ta>
            <ta e="T1577" id="Seg_535" s="T1576">n</ta>
            <ta e="T1578" id="Seg_536" s="T1577">v</ta>
            <ta e="T1579" id="Seg_537" s="T1578">que</ta>
            <ta e="T1580" id="Seg_538" s="T1579">v</ta>
            <ta e="T1581" id="Seg_539" s="T1580">conj</ta>
            <ta e="T1582" id="Seg_540" s="T1581">pers</ta>
            <ta e="T1583" id="Seg_541" s="T1582">v</ta>
            <ta e="T1584" id="Seg_542" s="T1583">conj</ta>
            <ta e="T1585" id="Seg_543" s="T1584">ptcl</ta>
            <ta e="T1586" id="Seg_544" s="T1585">v</ta>
            <ta e="T1587" id="Seg_545" s="T1586">dempro</ta>
            <ta e="T1588" id="Seg_546" s="T1587">dempro</ta>
            <ta e="T1589" id="Seg_547" s="T1588">n</ta>
            <ta e="T1590" id="Seg_548" s="T1589">ptcl</ta>
            <ta e="T1591" id="Seg_549" s="T1590">v</ta>
            <ta e="T1592" id="Seg_550" s="T1591">adv</ta>
            <ta e="T1593" id="Seg_551" s="T1592">que</ta>
            <ta e="T1594" id="Seg_552" s="T1593">v</ta>
            <ta e="T1595" id="Seg_553" s="T1594">conj</ta>
            <ta e="T1597" id="Seg_554" s="T1596">v</ta>
            <ta e="T1598" id="Seg_555" s="T1597">adv</ta>
            <ta e="T1599" id="Seg_556" s="T1598">ptcl</ta>
            <ta e="T1600" id="Seg_557" s="T1599">v</ta>
            <ta e="T1601" id="Seg_558" s="T1600">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1557" id="Seg_559" s="T1556">pro.h:A</ta>
            <ta e="T1559" id="Seg_560" s="T1558">np:Th</ta>
            <ta e="T1560" id="Seg_561" s="T1559">pro:A</ta>
            <ta e="T1564" id="Seg_562" s="T1563">np.h:A</ta>
            <ta e="T1567" id="Seg_563" s="T1566">np.h:Th</ta>
            <ta e="T1569" id="Seg_564" s="T1568">0.2.h:A</ta>
            <ta e="T1570" id="Seg_565" s="T1569">np:Th</ta>
            <ta e="T1571" id="Seg_566" s="T1570">0.2.h:A</ta>
            <ta e="T1572" id="Seg_567" s="T1571">adv:Time</ta>
            <ta e="T1577" id="Seg_568" s="T1576">np.h:A</ta>
            <ta e="T1580" id="Seg_569" s="T1579">0.2.h:A</ta>
            <ta e="T1582" id="Seg_570" s="T1581">pro.h:A</ta>
            <ta e="T1586" id="Seg_571" s="T1585">0.3.h:A </ta>
            <ta e="T1589" id="Seg_572" s="T1588">np:Th</ta>
            <ta e="T1591" id="Seg_573" s="T1590">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1557" id="Seg_574" s="T1556">pro.h:S</ta>
            <ta e="T1558" id="Seg_575" s="T1557">v:pred</ta>
            <ta e="T1559" id="Seg_576" s="T1558">np:O</ta>
            <ta e="T1560" id="Seg_577" s="T1559">pro:S</ta>
            <ta e="T1562" id="Seg_578" s="T1561">v:pred</ta>
            <ta e="T1564" id="Seg_579" s="T1563">np.h:S</ta>
            <ta e="T1565" id="Seg_580" s="T1564">v:pred</ta>
            <ta e="T1567" id="Seg_581" s="T1566">np.h:O</ta>
            <ta e="T1568" id="Seg_582" s="T1567">s:purp</ta>
            <ta e="T1569" id="Seg_583" s="T1568">0.2.h:S v:pred</ta>
            <ta e="T1570" id="Seg_584" s="T1569">np:O</ta>
            <ta e="T1571" id="Seg_585" s="T1570">0.2.h:S v:pred</ta>
            <ta e="T1577" id="Seg_586" s="T1576">np.h:S</ta>
            <ta e="T1578" id="Seg_587" s="T1577">v:pred</ta>
            <ta e="T1580" id="Seg_588" s="T1579">0.2.h:S v:pred</ta>
            <ta e="T1582" id="Seg_589" s="T1581">pro.h:S</ta>
            <ta e="T1583" id="Seg_590" s="T1582">v:pred</ta>
            <ta e="T1589" id="Seg_591" s="T1583">s:purp</ta>
            <ta e="T1591" id="Seg_592" s="T1590">0.3.h:S v:pred</ta>
            <ta e="T1597" id="Seg_593" s="T1594">s:temp</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T1561" id="Seg_594" s="T1560">TURK:disc</ta>
            <ta e="T1573" id="Seg_595" s="T1572">RUS:gram</ta>
            <ta e="T1576" id="Seg_596" s="T1575">RUS:gram</ta>
            <ta e="T1581" id="Seg_597" s="T1580">RUS:gram</ta>
            <ta e="T1584" id="Seg_598" s="T1583">RUS:gram</ta>
            <ta e="T1590" id="Seg_599" s="T1589">RUS:gram</ta>
            <ta e="T1599" id="Seg_600" s="T1598">RUS:mod</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T1562" id="Seg_601" s="T1556">Я быков приманила, они все мычат.</ta>
            <ta e="T1568" id="Seg_602" s="T1562">Один человек послал свою дочь за водой.</ta>
            <ta e="T1571" id="Seg_603" s="T1568">"Иди и принеси воды!"</ta>
            <ta e="T1575" id="Seg_604" s="T1571">Потом он стал её бить.</ta>
            <ta e="T1578" id="Seg_605" s="T1575">[Тут] идёт [другой] человек.</ta>
            <ta e="T1580" id="Seg_606" s="T1578">"Почему ты её бьёшь?"</ta>
            <ta e="T1589" id="Seg_607" s="T1580">"Да я её бью, чтобы она этот кувшин не разбила.</ta>
            <ta e="T1591" id="Seg_608" s="T1589">А то разобьёт.</ta>
            <ta e="T1597" id="Seg_609" s="T1591">Потом зачем бить, когда [уже] разобьёт?</ta>
            <ta e="T1600" id="Seg_610" s="T1597">Сейчас надо бить".</ta>
            <ta e="T1601" id="Seg_611" s="T1600">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T1562" id="Seg_612" s="T1556">I lured bulls, they are all bellowing.</ta>
            <ta e="T1568" id="Seg_613" s="T1562">One man sent his own daughter to bring water.</ta>
            <ta e="T1571" id="Seg_614" s="T1568">"Go and bring water!"</ta>
            <ta e="T1575" id="Seg_615" s="T1571">Then he started to beat her.</ta>
            <ta e="T1578" id="Seg_616" s="T1575">[Then another] man comes.</ta>
            <ta e="T1580" id="Seg_617" s="T1578">"Why are you beating [her]?"</ta>
            <ta e="T1589" id="Seg_618" s="T1580">"Well, I am beating so that she does not break that vessel.</ta>
            <ta e="T1591" id="Seg_619" s="T1589">Otherwise it will break.</ta>
            <ta e="T1597" id="Seg_620" s="T1591">Why to beat [her] when it [already has been] broken?</ta>
            <ta e="T1600" id="Seg_621" s="T1597">[One] should beat [her] now."</ta>
            <ta e="T1601" id="Seg_622" s="T1600">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T1562" id="Seg_623" s="T1556">Ich lockte Bullen, sie brüllen alle.</ta>
            <ta e="T1568" id="Seg_624" s="T1562">Ein Mann schickte seine eigene Tochter, um Wasser zu holen.</ta>
            <ta e="T1571" id="Seg_625" s="T1568">„Geh und hol Wasser!“</ta>
            <ta e="T1575" id="Seg_626" s="T1571">Dann fing er an, sie zu schlagen.</ta>
            <ta e="T1578" id="Seg_627" s="T1575">[Dann] kommt ein [anderer] Mann.</ta>
            <ta e="T1580" id="Seg_628" s="T1578">„Warum schlägst du [sie]?“</ta>
            <ta e="T1589" id="Seg_629" s="T1580">„Also, ich schlage, damit sie das Gefäß nicht zerbricht.</ta>
            <ta e="T1591" id="Seg_630" s="T1589">Sonst wird es kaputt gehen.</ta>
            <ta e="T1597" id="Seg_631" s="T1591">Warum [sie] schlagen, wenn es [schon] zerbrochen [worden ist]?</ta>
            <ta e="T1600" id="Seg_632" s="T1597">[Man] sollte [sie] nun schlagen.“</ta>
            <ta e="T1601" id="Seg_633" s="T1600">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T1562" id="Seg_634" s="T1556">[GVY:] mʼegluʔpim pronounced mʼegluʔpəm (-pam). This sentence does not belong to the text.</ta>
            <ta e="T1591" id="Seg_635" s="T1589">[KlT:] intransitive form, possibly meant as transitive.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1556" />
            <conversion-tli id="T1557" />
            <conversion-tli id="T1558" />
            <conversion-tli id="T1559" />
            <conversion-tli id="T1560" />
            <conversion-tli id="T1561" />
            <conversion-tli id="T1562" />
            <conversion-tli id="T1563" />
            <conversion-tli id="T1564" />
            <conversion-tli id="T1565" />
            <conversion-tli id="T1566" />
            <conversion-tli id="T1567" />
            <conversion-tli id="T1568" />
            <conversion-tli id="T1569" />
            <conversion-tli id="T1570" />
            <conversion-tli id="T1571" />
            <conversion-tli id="T1572" />
            <conversion-tli id="T1573" />
            <conversion-tli id="T1574" />
            <conversion-tli id="T1575" />
            <conversion-tli id="T1576" />
            <conversion-tli id="T1577" />
            <conversion-tli id="T1578" />
            <conversion-tli id="T1579" />
            <conversion-tli id="T1580" />
            <conversion-tli id="T1581" />
            <conversion-tli id="T1582" />
            <conversion-tli id="T1583" />
            <conversion-tli id="T1584" />
            <conversion-tli id="T1585" />
            <conversion-tli id="T1586" />
            <conversion-tli id="T1587" />
            <conversion-tli id="T1588" />
            <conversion-tli id="T1589" />
            <conversion-tli id="T1590" />
            <conversion-tli id="T1591" />
            <conversion-tli id="T1592" />
            <conversion-tli id="T1593" />
            <conversion-tli id="T1594" />
            <conversion-tli id="T1595" />
            <conversion-tli id="T1596" />
            <conversion-tli id="T1597" />
            <conversion-tli id="T1598" />
            <conversion-tli id="T1599" />
            <conversion-tli id="T1600" />
            <conversion-tli id="T1601" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
