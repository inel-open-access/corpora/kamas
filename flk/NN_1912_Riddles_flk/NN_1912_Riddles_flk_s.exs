<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDB47B8CD3-61BD-823D-EB06-E46F15E16C79">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\NN_1912_Riddles_flk\NN_1912_Riddles_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">28</ud-information>
            <ud-information attribute-name="# HIAT:w">21</ud-information>
            <ud-information attribute-name="# e">21</ud-information>
            <ud-information attribute-name="# HIAT:u">4</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="NN">
            <abbreviation>NN</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="NN"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T21" id="Seg_0" n="sc" s="T0">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Amniom</ts>
                  <nts id="Seg_5" n="HIAT:ip">,</nts>
                  <nts id="Seg_6" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_8" n="HIAT:w" s="T1">amniom</ts>
                  <nts id="Seg_9" n="HIAT:ip">,</nts>
                  <nts id="Seg_10" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_12" n="HIAT:w" s="T2">mogənʼi</ts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_15" n="HIAT:w" s="T3">păʔlaʔ</ts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_18" n="HIAT:w" s="T4">uʔləm</ts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_21" n="HIAT:w" s="T5">tagaj</ts>
                  <nts id="Seg_22" n="HIAT:ip">.</nts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_25" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_27" n="HIAT:w" s="T6">Măjagən</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_30" n="HIAT:w" s="T7">aspaʔ</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_33" n="HIAT:w" s="T8">mĭnzəlleʔbə</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_36" n="HIAT:w" s="T9">ködəmge</ts>
                  <nts id="Seg_37" n="HIAT:ip">.</nts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_40" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_42" n="HIAT:w" s="T10">Teʔdə</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_45" n="HIAT:w" s="T11">kaganzəgəjjəʔ</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_48" n="HIAT:w" s="T12">oʔb</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_51" n="HIAT:w" s="T13">üžü</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_54" n="HIAT:w" s="T14">šerlieiʔ</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_57" n="HIAT:w" s="T15">stol</ts>
                  <nts id="Seg_58" n="HIAT:ip">.</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_61" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_63" n="HIAT:w" s="T16">Mükür</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_66" n="HIAT:w" s="T17">saləmdo</ts>
                  <nts id="Seg_67" n="HIAT:ip">,</nts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">sĭre</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">ej</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">naŋnia</ts>
                  <nts id="Seg_77" n="HIAT:ip">.</nts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T21" id="Seg_79" n="sc" s="T0">
               <ts e="T1" id="Seg_81" n="e" s="T0">Amniom, </ts>
               <ts e="T2" id="Seg_83" n="e" s="T1">amniom, </ts>
               <ts e="T3" id="Seg_85" n="e" s="T2">mogənʼi </ts>
               <ts e="T4" id="Seg_87" n="e" s="T3">păʔlaʔ </ts>
               <ts e="T5" id="Seg_89" n="e" s="T4">uʔləm </ts>
               <ts e="T6" id="Seg_91" n="e" s="T5">tagaj. </ts>
               <ts e="T7" id="Seg_93" n="e" s="T6">Măjagən </ts>
               <ts e="T8" id="Seg_95" n="e" s="T7">aspaʔ </ts>
               <ts e="T9" id="Seg_97" n="e" s="T8">mĭnzəlleʔbə </ts>
               <ts e="T10" id="Seg_99" n="e" s="T9">ködəmge. </ts>
               <ts e="T11" id="Seg_101" n="e" s="T10">Teʔdə </ts>
               <ts e="T12" id="Seg_103" n="e" s="T11">kaganzəgəjjəʔ </ts>
               <ts e="T13" id="Seg_105" n="e" s="T12">oʔb </ts>
               <ts e="T14" id="Seg_107" n="e" s="T13">üžü </ts>
               <ts e="T15" id="Seg_109" n="e" s="T14">šerlieiʔ </ts>
               <ts e="T16" id="Seg_111" n="e" s="T15">stol. </ts>
               <ts e="T17" id="Seg_113" n="e" s="T16">Mükür </ts>
               <ts e="T18" id="Seg_115" n="e" s="T17">saləmdo, </ts>
               <ts e="T19" id="Seg_117" n="e" s="T18">sĭre </ts>
               <ts e="T20" id="Seg_119" n="e" s="T19">ej </ts>
               <ts e="T21" id="Seg_121" n="e" s="T20">naŋnia. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_122" s="T0">NN_1912_Riddles_flk.001 (001.001)</ta>
            <ta e="T10" id="Seg_123" s="T6">NN_1912_Riddles_flk.002 (001.002)</ta>
            <ta e="T16" id="Seg_124" s="T10">NN_1912_Riddles_flk.003 (001.003)</ta>
            <ta e="T21" id="Seg_125" s="T16">NN_1912_Riddles_flk.004 (001.004)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_126" s="T0">Amniom, amniom, mogənʼi păʔlaʔ uʔləm — tagaj. </ta>
            <ta e="T10" id="Seg_127" s="T6">Măjagən aspaʔ mĭnzəlleʔbə — ködəmge. </ta>
            <ta e="T16" id="Seg_128" s="T10">Teʔdə kaganzəgəjjəʔ oʔb üžü šerlieiʔ — stol. </ta>
            <ta e="T21" id="Seg_129" s="T16">Mükür saləmdo, sĭre ej naŋnia. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_130" s="T0">am-nio-m</ta>
            <ta e="T2" id="Seg_131" s="T1">am-nio-m</ta>
            <ta e="T3" id="Seg_132" s="T2">mo-gənʼi</ta>
            <ta e="T4" id="Seg_133" s="T3">păʔ-laʔ</ta>
            <ta e="T5" id="Seg_134" s="T4">uʔ-lə-m</ta>
            <ta e="T6" id="Seg_135" s="T5">tagaj</ta>
            <ta e="T7" id="Seg_136" s="T6">măja-gən</ta>
            <ta e="T8" id="Seg_137" s="T7">aspaʔ</ta>
            <ta e="T9" id="Seg_138" s="T8">mĭnzəl-leʔbə</ta>
            <ta e="T10" id="Seg_139" s="T9">ködəmge</ta>
            <ta e="T11" id="Seg_140" s="T10">teʔdə</ta>
            <ta e="T12" id="Seg_141" s="T11">kaga-nzə-gəj-jəʔ</ta>
            <ta e="T13" id="Seg_142" s="T12">oʔb</ta>
            <ta e="T14" id="Seg_143" s="T13">üžü</ta>
            <ta e="T15" id="Seg_144" s="T14">šer-lie-iʔ</ta>
            <ta e="T16" id="Seg_145" s="T15">stol</ta>
            <ta e="T17" id="Seg_146" s="T16">mükür</ta>
            <ta e="T18" id="Seg_147" s="T17">saləmdo</ta>
            <ta e="T19" id="Seg_148" s="T18">sĭre</ta>
            <ta e="T20" id="Seg_149" s="T19">ej</ta>
            <ta e="T21" id="Seg_150" s="T20">naŋ-nia</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_151" s="T0">am-liA-m</ta>
            <ta e="T2" id="Seg_152" s="T1">am-liA-m</ta>
            <ta e="T3" id="Seg_153" s="T2">mo-gənʼi</ta>
            <ta e="T4" id="Seg_154" s="T3">păda-lAʔ</ta>
            <ta e="T5" id="Seg_155" s="T4">uʔbdə-lV-m</ta>
            <ta e="T6" id="Seg_156" s="T5">tagaj</ta>
            <ta e="T7" id="Seg_157" s="T6">măja-Kən</ta>
            <ta e="T8" id="Seg_158" s="T7">aspaʔ</ta>
            <ta e="T9" id="Seg_159" s="T8">mĭnzəl-laʔbə</ta>
            <ta e="T10" id="Seg_160" s="T9">ködəmge</ta>
            <ta e="T11" id="Seg_161" s="T10">teʔdə</ta>
            <ta e="T12" id="Seg_162" s="T11">kaga-zə-gəj-jəʔ</ta>
            <ta e="T13" id="Seg_163" s="T12">oʔb</ta>
            <ta e="T14" id="Seg_164" s="T13">üžü</ta>
            <ta e="T15" id="Seg_165" s="T14">šer-liA-jəʔ</ta>
            <ta e="T16" id="Seg_166" s="T15">stol</ta>
            <ta e="T17" id="Seg_167" s="T16">mükür</ta>
            <ta e="T18" id="Seg_168" s="T17">saləmdo</ta>
            <ta e="T19" id="Seg_169" s="T18">sĭri</ta>
            <ta e="T20" id="Seg_170" s="T19">ej</ta>
            <ta e="T21" id="Seg_171" s="T20">naŋ-liA</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_172" s="T0">eat-PRS-1SG</ta>
            <ta e="T2" id="Seg_173" s="T1">eat-PRS-1SG</ta>
            <ta e="T3" id="Seg_174" s="T2">den-LAT/LOC.1SG</ta>
            <ta e="T4" id="Seg_175" s="T3">creep.into-CVB</ta>
            <ta e="T5" id="Seg_176" s="T4">get.up-FUT-1SG</ta>
            <ta e="T6" id="Seg_177" s="T5">knife.[NOM.SG]</ta>
            <ta e="T7" id="Seg_178" s="T6">mountain-LOC</ta>
            <ta e="T8" id="Seg_179" s="T7">cauldron.[NOM.SG]</ta>
            <ta e="T9" id="Seg_180" s="T8">boil-DUR.[3SG]</ta>
            <ta e="T10" id="Seg_181" s="T9">ant.[NOM.SG]</ta>
            <ta e="T11" id="Seg_182" s="T10">four.[NOM.SG]</ta>
            <ta e="T12" id="Seg_183" s="T11">brother-DYA-DU-PL</ta>
            <ta e="T13" id="Seg_184" s="T12">one.[NOM.SG]</ta>
            <ta e="T14" id="Seg_185" s="T13">cap.[NOM.SG]</ta>
            <ta e="T15" id="Seg_186" s="T14">wear-PRS-3PL</ta>
            <ta e="T16" id="Seg_187" s="T15">table.[NOM.SG]</ta>
            <ta e="T17" id="Seg_188" s="T16">horn.[NOM.SG]</ta>
            <ta e="T18" id="Seg_189" s="T17">crooked.[NOM.SG]</ta>
            <ta e="T19" id="Seg_190" s="T18">snow.[NOM.SG]</ta>
            <ta e="T20" id="Seg_191" s="T19">NEG</ta>
            <ta e="T21" id="Seg_192" s="T20">cling-PRS.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_193" s="T0">съесть-PRS-1SG</ta>
            <ta e="T2" id="Seg_194" s="T1">съесть-PRS-1SG</ta>
            <ta e="T3" id="Seg_195" s="T2">берлога-LAT/LOC.1SG</ta>
            <ta e="T4" id="Seg_196" s="T3">ползти-CVB</ta>
            <ta e="T5" id="Seg_197" s="T4">встать-FUT-1SG</ta>
            <ta e="T6" id="Seg_198" s="T5">нож.[NOM.SG]</ta>
            <ta e="T7" id="Seg_199" s="T6">гора-LOC</ta>
            <ta e="T8" id="Seg_200" s="T7">котел.[NOM.SG]</ta>
            <ta e="T9" id="Seg_201" s="T8">кипеть-DUR.[3SG]</ta>
            <ta e="T10" id="Seg_202" s="T9">муравей.[NOM.SG]</ta>
            <ta e="T11" id="Seg_203" s="T10">четыре.[NOM.SG]</ta>
            <ta e="T12" id="Seg_204" s="T11">брат-DYA-DU-PL</ta>
            <ta e="T13" id="Seg_205" s="T12">один.[NOM.SG]</ta>
            <ta e="T14" id="Seg_206" s="T13">шапка.[NOM.SG]</ta>
            <ta e="T15" id="Seg_207" s="T14">носить-PRS-3PL</ta>
            <ta e="T16" id="Seg_208" s="T15">стол.[NOM.SG]</ta>
            <ta e="T17" id="Seg_209" s="T16">рог.[NOM.SG]</ta>
            <ta e="T18" id="Seg_210" s="T17">изогнутый.[NOM.SG]</ta>
            <ta e="T19" id="Seg_211" s="T18">снег.[NOM.SG]</ta>
            <ta e="T20" id="Seg_212" s="T19">NEG</ta>
            <ta e="T21" id="Seg_213" s="T20">схватить-PRS.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_214" s="T0">v-v:tense-v:pn</ta>
            <ta e="T2" id="Seg_215" s="T1">v-v:tense-v:pn</ta>
            <ta e="T3" id="Seg_216" s="T2">n-n:case.poss</ta>
            <ta e="T4" id="Seg_217" s="T3">v-v:n.fin</ta>
            <ta e="T5" id="Seg_218" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_219" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_220" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_221" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_222" s="T8">v-v&gt;v-v:pn</ta>
            <ta e="T10" id="Seg_223" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_224" s="T10">num-n:case</ta>
            <ta e="T12" id="Seg_225" s="T11">n-n&gt;n-n:num-n:num</ta>
            <ta e="T13" id="Seg_226" s="T12">num-n:case</ta>
            <ta e="T14" id="Seg_227" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_228" s="T14">v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_229" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_230" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_231" s="T17">adj-n:case</ta>
            <ta e="T19" id="Seg_232" s="T18">n-n:case</ta>
            <ta e="T20" id="Seg_233" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_234" s="T20">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_235" s="T0">v</ta>
            <ta e="T2" id="Seg_236" s="T1">v</ta>
            <ta e="T3" id="Seg_237" s="T2">n</ta>
            <ta e="T4" id="Seg_238" s="T3">v</ta>
            <ta e="T5" id="Seg_239" s="T4">v</ta>
            <ta e="T6" id="Seg_240" s="T5">n</ta>
            <ta e="T7" id="Seg_241" s="T6">n</ta>
            <ta e="T8" id="Seg_242" s="T7">n</ta>
            <ta e="T9" id="Seg_243" s="T8">v</ta>
            <ta e="T10" id="Seg_244" s="T9">n</ta>
            <ta e="T11" id="Seg_245" s="T10">num</ta>
            <ta e="T12" id="Seg_246" s="T11">n</ta>
            <ta e="T13" id="Seg_247" s="T12">num</ta>
            <ta e="T14" id="Seg_248" s="T13">n</ta>
            <ta e="T15" id="Seg_249" s="T14">v</ta>
            <ta e="T16" id="Seg_250" s="T15">n</ta>
            <ta e="T17" id="Seg_251" s="T16">n</ta>
            <ta e="T18" id="Seg_252" s="T17">adj</ta>
            <ta e="T19" id="Seg_253" s="T18">n</ta>
            <ta e="T20" id="Seg_254" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_255" s="T20">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_256" s="T0">0.1.h:A</ta>
            <ta e="T2" id="Seg_257" s="T1">0.1.h:A</ta>
            <ta e="T3" id="Seg_258" s="T2">np:G 0.1.h:Poss</ta>
            <ta e="T5" id="Seg_259" s="T4">0.1.h:A</ta>
            <ta e="T7" id="Seg_260" s="T6">np:L</ta>
            <ta e="T8" id="Seg_261" s="T7">np:Th</ta>
            <ta e="T12" id="Seg_262" s="T11">np.h:A</ta>
            <ta e="T14" id="Seg_263" s="T13">np:Th</ta>
            <ta e="T17" id="Seg_264" s="T16">np:Th</ta>
            <ta e="T19" id="Seg_265" s="T18">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_266" s="T0">v:pred 0.1.h:S</ta>
            <ta e="T2" id="Seg_267" s="T1">v:pred 0.1.h:S</ta>
            <ta e="T5" id="Seg_268" s="T4">v:pred 0.1.h:S</ta>
            <ta e="T8" id="Seg_269" s="T7">np:S</ta>
            <ta e="T9" id="Seg_270" s="T8">v:pred</ta>
            <ta e="T12" id="Seg_271" s="T11">np.h:S</ta>
            <ta e="T15" id="Seg_272" s="T14">v:pred</ta>
            <ta e="T17" id="Seg_273" s="T16">np:S</ta>
            <ta e="T18" id="Seg_274" s="T17">adj:pred</ta>
            <ta e="T19" id="Seg_275" s="T18">np:S</ta>
            <ta e="T20" id="Seg_276" s="T19">ptcl.neg</ta>
            <ta e="T21" id="Seg_277" s="T20">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T1" id="Seg_278" s="T0">0.new</ta>
            <ta e="T2" id="Seg_279" s="T1">0.giv-active</ta>
            <ta e="T3" id="Seg_280" s="T2">new </ta>
            <ta e="T5" id="Seg_281" s="T4">0.giv-active</ta>
            <ta e="T6" id="Seg_282" s="T5">accs-inf </ta>
            <ta e="T7" id="Seg_283" s="T6">new </ta>
            <ta e="T8" id="Seg_284" s="T7">new </ta>
            <ta e="T10" id="Seg_285" s="T9">accs-inf </ta>
            <ta e="T12" id="Seg_286" s="T11">new </ta>
            <ta e="T14" id="Seg_287" s="T13">new </ta>
            <ta e="T16" id="Seg_288" s="T15">accs-inf </ta>
            <ta e="T17" id="Seg_289" s="T16">new </ta>
            <ta e="T19" id="Seg_290" s="T18">accs-gen </ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T16" id="Seg_291" s="T15">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_292" s="T0">Я ем, ем и в нору ныряю. — Нож.</ta>
            <ta e="T10" id="Seg_293" s="T6">На горе котёл кипит. — Муравейник.</ta>
            <ta e="T16" id="Seg_294" s="T10">Четверо братьев одну шапку носят. — Стол.</ta>
            <ta e="T21" id="Seg_295" s="T16">К завитому рогу снег не прилипает. </ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_296" s="T0">I eat and eat, and [then] will creep into my den — a knife.</ta>
            <ta e="T10" id="Seg_297" s="T6">On a mountain a cauldron is boiling — an ant[hill].</ta>
            <ta e="T16" id="Seg_298" s="T10">Four brothers wear one cap — a table.</ta>
            <ta e="T21" id="Seg_299" s="T16">There's a crooked horn, snow does not stick to it.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_300" s="T0">Ich esse und esse und werde [dann] in meinen Bau kriechen. – Das Messer.</ta>
            <ta e="T10" id="Seg_301" s="T6">Auf einem Berg kocht ein Kessel. – Der Ameisen[haufen].</ta>
            <ta e="T16" id="Seg_302" s="T10">Vier Brüder tragen dieselbe Mütze. – Der Tisch.</ta>
            <ta e="T21" id="Seg_303" s="T16">Ein gekrümmtes Kuhhorn, kein Schnee bleibt daran haften.</ta>
         </annotation>
         <annotation name="ltg" tierref="ltg">
            <ta e="T6" id="Seg_304" s="T0">Ich esse, esse in mein Nest (od. meine Höhle) tauche ich. – Das Messer</ta>
            <ta e="T10" id="Seg_305" s="T6">Auf dem Berge kocht ein Kessel. – Der Ameisenhaufen.</ta>
            <ta e="T16" id="Seg_306" s="T10">Vier Brüder eine Mütze sich aufsetzen. Der Tisch.</ta>
            <ta e="T21" id="Seg_307" s="T16">Das Kuhhorn ist schief, der Schnee hängt sich nicht [daran].</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T21" id="Seg_308" s="T16">[AAV] No solution recorded for this riddle.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltg"
                          display-name="ltg"
                          name="ltg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
