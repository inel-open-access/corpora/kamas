<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDEF049BF3-8E6F-0208-5DD7-CE3CD13BC440">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_BirdBringsFish_flk.wav" />
         <referenced-file url="PKZ_196X_BirdBringsFish_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_BirdBringsFish_flk\PKZ_196X_BirdBringsFish_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">218</ud-information>
            <ud-information attribute-name="# HIAT:w">157</ud-information>
            <ud-information attribute-name="# e">157</ud-information>
            <ud-information attribute-name="# HIAT:u">27</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T208" time="0.029" type="appl" />
         <tli id="T209" time="0.637" type="appl" />
         <tli id="T210" time="1.245" type="appl" />
         <tli id="T211" time="1.853" type="appl" />
         <tli id="T212" time="3.0198262781958958" />
         <tli id="T213" time="3.752" type="appl" />
         <tli id="T214" time="4.568" type="appl" />
         <tli id="T215" time="5.384" type="appl" />
         <tli id="T216" time="7.072926448489725" />
         <tli id="T217" time="7.921" type="appl" />
         <tli id="T218" time="8.75" type="appl" />
         <tli id="T219" time="9.552783789524765" />
         <tli id="T220" time="10.176" type="appl" />
         <tli id="T221" time="10.773" type="appl" />
         <tli id="T222" time="11.369" type="appl" />
         <tli id="T223" time="12.195" type="appl" />
         <tli id="T224" time="13.021" type="appl" />
         <tli id="T225" time="13.847" type="appl" />
         <tli id="T226" time="14.673" type="appl" />
         <tli id="T227" time="15.499" type="appl" />
         <tli id="T228" time="16.199" type="appl" />
         <tli id="T229" time="16.78" type="appl" />
         <tli id="T230" time="17.36" type="appl" />
         <tli id="T231" time="18.172287934573976" />
         <tli id="T232" time="18.662" type="appl" />
         <tli id="T233" time="19.24" type="appl" />
         <tli id="T234" time="19.817" type="appl" />
         <tli id="T235" time="20.394" type="appl" />
         <tli id="T236" time="21.065" type="appl" />
         <tli id="T237" time="21.736" type="appl" />
         <tli id="T238" time="22.407" type="appl" />
         <tli id="T239" time="23.078" type="appl" />
         <tli id="T240" time="23.749" type="appl" />
         <tli id="T241" time="25.73851933800078" />
         <tli id="T242" time="26.304" type="appl" />
         <tli id="T243" time="26.848" type="appl" />
         <tli id="T244" time="27.392" type="appl" />
         <tli id="T245" time="27.958391635217627" />
         <tli id="T246" time="29.02" type="appl" />
         <tli id="T247" time="29.808" type="appl" />
         <tli id="T248" time="30.596" type="appl" />
         <tli id="T249" time="31.264" type="appl" />
         <tli id="T250" time="31.811" type="appl" />
         <tli id="T251" time="32.359" type="appl" />
         <tli id="T252" time="32.906" type="appl" />
         <tli id="T253" time="33.454" type="appl" />
         <tli id="T254" time="34.002" type="appl" />
         <tli id="T255" time="34.549" type="appl" />
         <tli id="T256" time="35.097" type="appl" />
         <tli id="T257" time="35.644" type="appl" />
         <tli id="T258" time="36.192" type="appl" />
         <tli id="T259" time="36.739" type="appl" />
         <tli id="T260" time="37.65116737141373" />
         <tli id="T261" time="38.165" type="appl" />
         <tli id="T262" time="38.899" type="appl" />
         <tli id="T263" time="39.632" type="appl" />
         <tli id="T264" time="40.366" type="appl" />
         <tli id="T265" time="41.74426524075651" />
         <tli id="T266" time="42.099" type="appl" />
         <tli id="T267" time="42.712" type="appl" />
         <tli id="T268" time="43.325" type="appl" />
         <tli id="T269" time="43.938" type="appl" />
         <tli id="T270" time="44.551" type="appl" />
         <tli id="T271" time="45.164" type="appl" />
         <tli id="T272" time="45.777" type="appl" />
         <tli id="T273" time="46.39" type="appl" />
         <tli id="T274" time="47.448" type="appl" />
         <tli id="T275" time="48.49" type="appl" />
         <tli id="T276" time="49.532" type="appl" />
         <tli id="T277" time="50.763746376295245" />
         <tli id="T278" time="51.265" type="appl" />
         <tli id="T279" time="51.785" type="appl" />
         <tli id="T280" time="52.305" type="appl" />
         <tli id="T281" time="52.826" type="appl" />
         <tli id="T282" time="53.346" type="appl" />
         <tli id="T283" time="53.866" type="appl" />
         <tli id="T284" time="54.968" type="appl" />
         <tli id="T285" time="56.07" type="appl" />
         <tli id="T286" time="57.173" type="appl" />
         <tli id="T287" time="58.275" type="appl" />
         <tli id="T288" time="59.56324016706475" />
         <tli id="T289" time="60.411" type="appl" />
         <tli id="T290" time="61.278" type="appl" />
         <tli id="T291" time="62.146" type="appl" />
         <tli id="T292" time="63.013" type="appl" />
         <tli id="T293" time="63.881" type="appl" />
         <tli id="T294" time="64.775" type="appl" />
         <tli id="T295" time="65.504" type="appl" />
         <tli id="T296" time="66.232" type="appl" />
         <tli id="T297" time="66.961" type="appl" />
         <tli id="T298" time="67.69" type="appl" />
         <tli id="T299" time="68.419" type="appl" />
         <tli id="T300" time="69.148" type="appl" />
         <tli id="T301" time="69.876" type="appl" />
         <tli id="T302" time="70.605" type="appl" />
         <tli id="T303" time="71.334" type="appl" />
         <tli id="T304" time="72.063" type="appl" />
         <tli id="T305" time="72.792" type="appl" />
         <tli id="T306" time="73.52" type="appl" />
         <tli id="T307" time="74.249" type="appl" />
         <tli id="T308" time="75.50232323807222" />
         <tli id="T309" time="76.205" type="appl" />
         <tli id="T310" time="76.748" type="appl" />
         <tli id="T311" time="77.29" type="appl" />
         <tli id="T312" time="77.833" type="appl" />
         <tli id="T313" time="78.376" type="appl" />
         <tli id="T314" time="79.234" type="appl" />
         <tli id="T315" time="80.092" type="appl" />
         <tli id="T316" time="80.95" type="appl" />
         <tli id="T317" time="81.808" type="appl" />
         <tli id="T318" time="82.751" type="appl" />
         <tli id="T319" time="83.545" type="appl" />
         <tli id="T320" time="84.339" type="appl" />
         <tli id="T321" time="85.128" type="appl" />
         <tli id="T322" time="85.81" type="appl" />
         <tli id="T323" time="86.492" type="appl" />
         <tli id="T324" time="87.174" type="appl" />
         <tli id="T325" time="87.98827162452014" />
         <tli id="T326" time="88.697" type="appl" />
         <tli id="T327" time="89.418" type="appl" />
         <tli id="T328" time="90.48146153190483" />
         <tli id="T329" time="91.005" type="appl" />
         <tli id="T330" time="91.746" type="appl" />
         <tli id="T331" time="92.488" type="appl" />
         <tli id="T332" time="93.229" type="appl" />
         <tli id="T333" time="94.35457205647839" />
         <tli id="T334" time="95.262" type="appl" />
         <tli id="T335" time="96.033" type="appl" />
         <tli id="T336" time="96.803" type="appl" />
         <tli id="T337" time="97.573" type="appl" />
         <tli id="T338" time="98.344" type="appl" />
         <tli id="T339" time="99.114" type="appl" />
         <tli id="T340" time="99.884" type="appl" />
         <tli id="T341" time="100.655" type="appl" />
         <tli id="T342" time="101.425" type="appl" />
         <tli id="T343" time="101.94" type="appl" />
         <tli id="T344" time="102.455" type="appl" />
         <tli id="T345" time="102.971" type="appl" />
         <tli id="T346" time="103.486" type="appl" />
         <tli id="T347" time="104.0076690856915" />
         <tli id="T348" time="104.706" type="appl" />
         <tli id="T349" time="105.412" type="appl" />
         <tli id="T350" time="106.117" type="appl" />
         <tli id="T351" time="106.823" type="appl" />
         <tli id="T352" time="107.528" type="appl" />
         <tli id="T353" time="108.234" type="appl" />
         <tli id="T354" time="108.939" type="appl" />
         <tli id="T355" time="109.484" type="appl" />
         <tli id="T356" time="110.029" type="appl" />
         <tli id="T357" time="110.574" type="appl" />
         <tli id="T358" time="111.119" type="appl" />
         <tli id="T359" time="111.664" type="appl" />
         <tli id="T360" time="112.209" type="appl" />
         <tli id="T361" time="112.755" type="appl" />
         <tli id="T362" time="113.3" type="appl" />
         <tli id="T363" time="113.845" type="appl" />
         <tli id="T364" time="114.39" type="appl" />
         <tli id="T365" time="114.935" type="appl" />
         <tli id="T366" time="115.48" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T366" id="Seg_0" n="sc" s="T208">
               <ts e="T212" id="Seg_2" n="HIAT:u" s="T208">
                  <ts e="T209" id="Seg_4" n="HIAT:w" s="T208">Amnobi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_7" n="HIAT:w" s="T209">kuza</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_10" n="HIAT:w" s="T210">nükeziʔ</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_13" n="HIAT:w" s="T211">bar</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T216" id="Seg_17" n="HIAT:u" s="T212">
                  <ts e="T213" id="Seg_19" n="HIAT:w" s="T212">Kola</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_22" n="HIAT:w" s="T213">dʼaʔpi</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_25" n="HIAT:w" s="T214">i</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_28" n="HIAT:w" s="T215">sadarbi</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T219" id="Seg_32" n="HIAT:u" s="T216">
                  <ts e="T217" id="Seg_34" n="HIAT:w" s="T216">Dăreʔ</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_37" n="HIAT:w" s="T217">amnobiʔi</ts>
                  <nts id="Seg_38" n="HIAT:ip">,</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_41" n="HIAT:w" s="T218">amnobiʔi</ts>
                  <nts id="Seg_42" n="HIAT:ip">.</nts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T222" id="Seg_45" n="HIAT:u" s="T219">
                  <ts e="T220" id="Seg_47" n="HIAT:w" s="T219">Ĭmbidə</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_50" n="HIAT:w" s="T220">nagobi</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_53" n="HIAT:w" s="T221">dĭzen</ts>
                  <nts id="Seg_54" n="HIAT:ip">.</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T227" id="Seg_57" n="HIAT:u" s="T222">
                  <ts e="T223" id="Seg_59" n="HIAT:w" s="T222">Dĭgəttə</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_62" n="HIAT:w" s="T223">nʼergöleʔ</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_65" n="HIAT:w" s="T224">šobi</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_67" n="HIAT:ip">(</nts>
                  <ts e="T226" id="Seg_69" n="HIAT:w" s="T225">šuj-</ts>
                  <nts id="Seg_70" n="HIAT:ip">)</nts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_73" n="HIAT:w" s="T226">süjö</ts>
                  <nts id="Seg_74" n="HIAT:ip">.</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T231" id="Seg_77" n="HIAT:u" s="T227">
                  <ts e="T228" id="Seg_79" n="HIAT:w" s="T227">Dĭ</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_82" n="HIAT:w" s="T228">dʼaʔpi</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_85" n="HIAT:w" s="T229">üdʼüge</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_88" n="HIAT:w" s="T230">kola</ts>
                  <nts id="Seg_89" n="HIAT:ip">.</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T235" id="Seg_92" n="HIAT:u" s="T231">
                  <nts id="Seg_93" n="HIAT:ip">"</nts>
                  <ts e="T232" id="Seg_95" n="HIAT:w" s="T231">Ĭmbi</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_98" n="HIAT:w" s="T232">alil</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_101" n="HIAT:w" s="T233">dĭ</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_104" n="HIAT:w" s="T234">kolazʼiʔ</ts>
                  <nts id="Seg_105" n="HIAT:ip">?</nts>
                  <nts id="Seg_106" n="HIAT:ip">"</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T241" id="Seg_109" n="HIAT:u" s="T235">
                  <nts id="Seg_110" n="HIAT:ip">"</nts>
                  <ts e="T236" id="Seg_112" n="HIAT:w" s="T235">Da</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_115" n="HIAT:w" s="T236">sadarlim</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_118" n="HIAT:w" s="T237">da</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_121" n="HIAT:w" s="T238">ipek</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_124" n="HIAT:w" s="T239">iləm</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_127" n="HIAT:w" s="T240">amzittə</ts>
                  <nts id="Seg_128" n="HIAT:ip">"</nts>
                  <nts id="Seg_129" n="HIAT:ip">.</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T248" id="Seg_132" n="HIAT:u" s="T241">
                  <ts e="T242" id="Seg_134" n="HIAT:w" s="T241">A</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_137" n="HIAT:w" s="T242">dĭ</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_140" n="HIAT:w" s="T243">măndə</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_143" n="HIAT:w" s="T244">dĭʔnə:</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_145" n="HIAT:ip">"</nts>
                  <ts e="T246" id="Seg_147" n="HIAT:w" s="T245">Amga</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_150" n="HIAT:w" s="T246">tănan</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_153" n="HIAT:w" s="T247">ipek</ts>
                  <nts id="Seg_154" n="HIAT:ip">.</nts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T260" id="Seg_157" n="HIAT:u" s="T248">
                  <ts e="T249" id="Seg_159" n="HIAT:w" s="T248">Kanaʔ</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_162" n="HIAT:w" s="T249">maːʔnʼi</ts>
                  <nts id="Seg_163" n="HIAT:ip">,</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_166" n="HIAT:w" s="T250">a</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_169" n="HIAT:w" s="T251">măn</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_171" n="HIAT:ip">(</nts>
                  <ts e="T253" id="Seg_173" n="HIAT:w" s="T252">kal-</ts>
                  <nts id="Seg_174" n="HIAT:ip">)</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_177" n="HIAT:w" s="T253">kažən</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_180" n="HIAT:w" s="T254">dʼala</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_183" n="HIAT:w" s="T255">tănan</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_185" n="HIAT:ip">(</nts>
                  <ts e="T257" id="Seg_187" n="HIAT:w" s="T256">deʔ-</ts>
                  <nts id="Seg_188" n="HIAT:ip">)</nts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_191" n="HIAT:w" s="T257">detləm</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_194" n="HIAT:w" s="T258">urgo</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_197" n="HIAT:w" s="T259">kola</ts>
                  <nts id="Seg_198" n="HIAT:ip">!</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T265" id="Seg_201" n="HIAT:u" s="T260">
                  <ts e="T261" id="Seg_203" n="HIAT:w" s="T260">I</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_206" n="HIAT:w" s="T261">dĭgəttə</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_209" n="HIAT:w" s="T262">šindənədə</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_212" n="HIAT:w" s="T263">iʔ</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_215" n="HIAT:w" s="T264">nörbəʔ</ts>
                  <nts id="Seg_216" n="HIAT:ip">!</nts>
                  <nts id="Seg_217" n="HIAT:ip">"</nts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T273" id="Seg_220" n="HIAT:u" s="T265">
                  <ts e="T266" id="Seg_222" n="HIAT:w" s="T265">Dĭ</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_224" n="HIAT:ip">(</nts>
                  <ts e="T267" id="Seg_226" n="HIAT:w" s="T266">kal-</ts>
                  <nts id="Seg_227" n="HIAT:ip">)</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_230" n="HIAT:w" s="T267">kažnɨj</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_233" n="HIAT:w" s="T268">dʼala</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_236" n="HIAT:w" s="T269">deʔpi</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_239" n="HIAT:w" s="T270">kola</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_242" n="HIAT:w" s="T271">urgo</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_245" n="HIAT:w" s="T272">dĭ</ts>
                  <nts id="Seg_246" n="HIAT:ip">.</nts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T277" id="Seg_249" n="HIAT:u" s="T273">
                  <ts e="T274" id="Seg_251" n="HIAT:w" s="T273">Dĭn</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_254" n="HIAT:w" s="T274">net</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_257" n="HIAT:w" s="T275">dagajzʼiʔ</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_260" n="HIAT:w" s="T276">băʔpi</ts>
                  <nts id="Seg_261" n="HIAT:ip">.</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T283" id="Seg_264" n="HIAT:u" s="T277">
                  <ts e="T278" id="Seg_266" n="HIAT:w" s="T277">I</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_269" n="HIAT:w" s="T278">pürbi</ts>
                  <nts id="Seg_270" n="HIAT:ip">,</nts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_273" n="HIAT:w" s="T279">a</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_276" n="HIAT:w" s="T280">dĭ</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_279" n="HIAT:w" s="T281">sadarlaʔ</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_282" n="HIAT:w" s="T282">kambi</ts>
                  <nts id="Seg_283" n="HIAT:ip">.</nts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T288" id="Seg_286" n="HIAT:u" s="T283">
                  <ts e="T284" id="Seg_288" n="HIAT:w" s="T283">Dĭgəttə</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_291" n="HIAT:w" s="T284">dĭzeŋ</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_294" n="HIAT:w" s="T285">ibiʔi</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_297" n="HIAT:w" s="T286">aktʼanə</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_300" n="HIAT:w" s="T287">tura</ts>
                  <nts id="Seg_301" n="HIAT:ip">.</nts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T293" id="Seg_304" n="HIAT:u" s="T288">
                  <ts e="T289" id="Seg_306" n="HIAT:w" s="T288">I</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_309" n="HIAT:w" s="T289">dĭn</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_312" n="HIAT:w" s="T290">iʔgö</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_315" n="HIAT:w" s="T291">sat</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_318" n="HIAT:w" s="T292">amnobi</ts>
                  <nts id="Seg_319" n="HIAT:ip">?</nts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T308" id="Seg_322" n="HIAT:u" s="T293">
                  <ts e="T294" id="Seg_324" n="HIAT:w" s="T293">Dĭgəttə</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_327" n="HIAT:w" s="T294">onʼiʔ</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_330" n="HIAT:w" s="T295">măndə:</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_332" n="HIAT:ip">"</nts>
                  <ts e="T297" id="Seg_334" n="HIAT:w" s="T296">Šindi</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_337" n="HIAT:w" s="T297">bɨ</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_340" n="HIAT:w" s="T298">dĭ</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_342" n="HIAT:ip">(</nts>
                  <ts e="T300" id="Seg_344" n="HIAT:w" s="T299">süj-</ts>
                  <nts id="Seg_345" n="HIAT:ip">)</nts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_348" n="HIAT:w" s="T300">süjöm</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_351" n="HIAT:w" s="T301">dʼaʔpi</ts>
                  <nts id="Seg_352" n="HIAT:ip">,</nts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_355" n="HIAT:w" s="T302">măn</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_358" n="HIAT:w" s="T303">bɨ</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_361" n="HIAT:w" s="T304">iʔgö</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_364" n="HIAT:w" s="T305">aktʼa</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_367" n="HIAT:w" s="T306">mĭbiem</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_370" n="HIAT:w" s="T307">bɨ</ts>
                  <nts id="Seg_371" n="HIAT:ip">"</nts>
                  <nts id="Seg_372" n="HIAT:ip">.</nts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T313" id="Seg_375" n="HIAT:u" s="T308">
                  <ts e="T309" id="Seg_377" n="HIAT:w" s="T308">Dĭgəttə</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_380" n="HIAT:w" s="T309">dĭ</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_383" n="HIAT:w" s="T310">măndə:</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_385" n="HIAT:ip">"</nts>
                  <ts e="T312" id="Seg_387" n="HIAT:w" s="T311">Măn</ts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_390" n="HIAT:w" s="T312">dʼaʔpiam</ts>
                  <nts id="Seg_391" n="HIAT:ip">"</nts>
                  <nts id="Seg_392" n="HIAT:ip">.</nts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T317" id="Seg_395" n="HIAT:u" s="T313">
                  <ts e="T314" id="Seg_397" n="HIAT:w" s="T313">Dĭgəttə</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_400" n="HIAT:w" s="T314">iʔgö</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_403" n="HIAT:w" s="T315">il</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_406" n="HIAT:w" s="T316">šobiʔi</ts>
                  <nts id="Seg_407" n="HIAT:ip">.</nts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T320" id="Seg_410" n="HIAT:u" s="T317">
                  <ts e="T318" id="Seg_412" n="HIAT:w" s="T317">Süjö</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_415" n="HIAT:w" s="T318">nʼergöleʔ</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_418" n="HIAT:w" s="T319">šobi</ts>
                  <nts id="Seg_419" n="HIAT:ip">.</nts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T325" id="Seg_422" n="HIAT:u" s="T320">
                  <ts e="T321" id="Seg_424" n="HIAT:w" s="T320">Dĭ</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_427" n="HIAT:w" s="T321">bar</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_430" n="HIAT:w" s="T322">nuldəbi</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_433" n="HIAT:w" s="T323">dĭn</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_436" n="HIAT:w" s="T324">amzittə</ts>
                  <nts id="Seg_437" n="HIAT:ip">.</nts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T328" id="Seg_440" n="HIAT:u" s="T325">
                  <nts id="Seg_441" n="HIAT:ip">"</nts>
                  <ts e="T326" id="Seg_443" n="HIAT:w" s="T325">Süjö</ts>
                  <nts id="Seg_444" n="HIAT:ip">,</nts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_447" n="HIAT:w" s="T326">amnoʔ</ts>
                  <nts id="Seg_448" n="HIAT:ip">,</nts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_451" n="HIAT:w" s="T327">amaʔ</ts>
                  <nts id="Seg_452" n="HIAT:ip">"</nts>
                  <nts id="Seg_453" n="HIAT:ip">.</nts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T333" id="Seg_456" n="HIAT:u" s="T328">
                  <ts e="T329" id="Seg_458" n="HIAT:w" s="T328">Dĭgəttə</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_461" n="HIAT:w" s="T329">dĭ</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_464" n="HIAT:w" s="T330">süjö</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_467" n="HIAT:w" s="T331">amnobi</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_470" n="HIAT:w" s="T332">amzittə</ts>
                  <nts id="Seg_471" n="HIAT:ip">.</nts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T342" id="Seg_474" n="HIAT:u" s="T333">
                  <ts e="T334" id="Seg_476" n="HIAT:w" s="T333">Dĭgəttə</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_479" n="HIAT:w" s="T334">dĭ</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_482" n="HIAT:w" s="T335">süjö</ts>
                  <nts id="Seg_483" n="HIAT:ip">,</nts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_486" n="HIAT:w" s="T337">dĭzeŋ</ts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_489" n="HIAT:w" s="T338">bar</ts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_492" n="HIAT:w" s="T339">il</ts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_495" n="HIAT:w" s="T340">dĭm</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_498" n="HIAT:w" s="T341">dʼaʔpiʔi</ts>
                  <nts id="Seg_499" n="HIAT:ip">.</nts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T347" id="Seg_502" n="HIAT:u" s="T342">
                  <ts e="T343" id="Seg_504" n="HIAT:w" s="T342">A</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_507" n="HIAT:w" s="T343">dĭ</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_510" n="HIAT:w" s="T344">kuza</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_513" n="HIAT:w" s="T345">dʼaʔpi</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_516" n="HIAT:w" s="T346">ujutsiʔ</ts>
                  <nts id="Seg_517" n="HIAT:ip">.</nts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T354" id="Seg_520" n="HIAT:u" s="T347">
                  <ts e="T348" id="Seg_522" n="HIAT:w" s="T347">Dĭgəttə</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_525" n="HIAT:w" s="T348">dĭ</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_528" n="HIAT:w" s="T349">kuzan</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_531" n="HIAT:w" s="T350">ujutsiʔ</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_534" n="HIAT:w" s="T351">bar</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_537" n="HIAT:w" s="T352">il</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_539" n="HIAT:ip">(</nts>
                  <ts e="T354" id="Seg_541" n="HIAT:w" s="T353">nʼuʔdəbi</ts>
                  <nts id="Seg_542" n="HIAT:ip">)</nts>
                  <nts id="Seg_543" n="HIAT:ip">.</nts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T366" id="Seg_546" n="HIAT:u" s="T354">
                  <ts e="T355" id="Seg_548" n="HIAT:w" s="T354">Dĭgəttə</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_551" n="HIAT:w" s="T355">dĭ</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_554" n="HIAT:w" s="T356">kuzan</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_557" n="HIAT:w" s="T357">udaʔi</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_560" n="HIAT:w" s="T358">tararluʔpi</ts>
                  <nts id="Seg_561" n="HIAT:ip">,</nts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_564" n="HIAT:w" s="T359">dĭ</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_567" n="HIAT:w" s="T360">saʔməluʔpi</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_570" n="HIAT:w" s="T361">i</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_572" n="HIAT:ip">(</nts>
                  <ts e="T363" id="Seg_574" n="HIAT:w" s="T362">bal-</ts>
                  <nts id="Seg_575" n="HIAT:ip">)</nts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_578" n="HIAT:w" s="T363">bar</ts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_581" n="HIAT:w" s="T364">il</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_584" n="HIAT:w" s="T365">külaːmbiʔi</ts>
                  <nts id="Seg_585" n="HIAT:ip">.</nts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T366" id="Seg_587" n="sc" s="T208">
               <ts e="T209" id="Seg_589" n="e" s="T208">Amnobi </ts>
               <ts e="T210" id="Seg_591" n="e" s="T209">kuza </ts>
               <ts e="T211" id="Seg_593" n="e" s="T210">nükeziʔ </ts>
               <ts e="T212" id="Seg_595" n="e" s="T211">bar. </ts>
               <ts e="T213" id="Seg_597" n="e" s="T212">Kola </ts>
               <ts e="T214" id="Seg_599" n="e" s="T213">dʼaʔpi </ts>
               <ts e="T215" id="Seg_601" n="e" s="T214">i </ts>
               <ts e="T216" id="Seg_603" n="e" s="T215">sadarbi. </ts>
               <ts e="T217" id="Seg_605" n="e" s="T216">Dăreʔ </ts>
               <ts e="T218" id="Seg_607" n="e" s="T217">amnobiʔi, </ts>
               <ts e="T219" id="Seg_609" n="e" s="T218">amnobiʔi. </ts>
               <ts e="T220" id="Seg_611" n="e" s="T219">Ĭmbidə </ts>
               <ts e="T221" id="Seg_613" n="e" s="T220">nagobi </ts>
               <ts e="T222" id="Seg_615" n="e" s="T221">dĭzen. </ts>
               <ts e="T223" id="Seg_617" n="e" s="T222">Dĭgəttə </ts>
               <ts e="T224" id="Seg_619" n="e" s="T223">nʼergöleʔ </ts>
               <ts e="T225" id="Seg_621" n="e" s="T224">šobi </ts>
               <ts e="T226" id="Seg_623" n="e" s="T225">(šuj-) </ts>
               <ts e="T227" id="Seg_625" n="e" s="T226">süjö. </ts>
               <ts e="T228" id="Seg_627" n="e" s="T227">Dĭ </ts>
               <ts e="T229" id="Seg_629" n="e" s="T228">dʼaʔpi </ts>
               <ts e="T230" id="Seg_631" n="e" s="T229">üdʼüge </ts>
               <ts e="T231" id="Seg_633" n="e" s="T230">kola. </ts>
               <ts e="T232" id="Seg_635" n="e" s="T231">"Ĭmbi </ts>
               <ts e="T233" id="Seg_637" n="e" s="T232">alil </ts>
               <ts e="T234" id="Seg_639" n="e" s="T233">dĭ </ts>
               <ts e="T235" id="Seg_641" n="e" s="T234">kolazʼiʔ?" </ts>
               <ts e="T236" id="Seg_643" n="e" s="T235">"Da </ts>
               <ts e="T237" id="Seg_645" n="e" s="T236">sadarlim </ts>
               <ts e="T238" id="Seg_647" n="e" s="T237">da </ts>
               <ts e="T239" id="Seg_649" n="e" s="T238">ipek </ts>
               <ts e="T240" id="Seg_651" n="e" s="T239">iləm </ts>
               <ts e="T241" id="Seg_653" n="e" s="T240">amzittə". </ts>
               <ts e="T242" id="Seg_655" n="e" s="T241">A </ts>
               <ts e="T243" id="Seg_657" n="e" s="T242">dĭ </ts>
               <ts e="T244" id="Seg_659" n="e" s="T243">măndə </ts>
               <ts e="T245" id="Seg_661" n="e" s="T244">dĭʔnə: </ts>
               <ts e="T246" id="Seg_663" n="e" s="T245">"Amga </ts>
               <ts e="T247" id="Seg_665" n="e" s="T246">tănan </ts>
               <ts e="T248" id="Seg_667" n="e" s="T247">ipek. </ts>
               <ts e="T249" id="Seg_669" n="e" s="T248">Kanaʔ </ts>
               <ts e="T250" id="Seg_671" n="e" s="T249">maːʔnʼi, </ts>
               <ts e="T251" id="Seg_673" n="e" s="T250">a </ts>
               <ts e="T252" id="Seg_675" n="e" s="T251">măn </ts>
               <ts e="T253" id="Seg_677" n="e" s="T252">(kal-) </ts>
               <ts e="T254" id="Seg_679" n="e" s="T253">kažən </ts>
               <ts e="T255" id="Seg_681" n="e" s="T254">dʼala </ts>
               <ts e="T256" id="Seg_683" n="e" s="T255">tănan </ts>
               <ts e="T257" id="Seg_685" n="e" s="T256">(deʔ-) </ts>
               <ts e="T258" id="Seg_687" n="e" s="T257">detləm </ts>
               <ts e="T259" id="Seg_689" n="e" s="T258">urgo </ts>
               <ts e="T260" id="Seg_691" n="e" s="T259">kola! </ts>
               <ts e="T261" id="Seg_693" n="e" s="T260">I </ts>
               <ts e="T262" id="Seg_695" n="e" s="T261">dĭgəttə </ts>
               <ts e="T263" id="Seg_697" n="e" s="T262">šindənədə </ts>
               <ts e="T264" id="Seg_699" n="e" s="T263">iʔ </ts>
               <ts e="T265" id="Seg_701" n="e" s="T264">nörbəʔ!" </ts>
               <ts e="T266" id="Seg_703" n="e" s="T265">Dĭ </ts>
               <ts e="T267" id="Seg_705" n="e" s="T266">(kal-) </ts>
               <ts e="T268" id="Seg_707" n="e" s="T267">kažnɨj </ts>
               <ts e="T269" id="Seg_709" n="e" s="T268">dʼala </ts>
               <ts e="T270" id="Seg_711" n="e" s="T269">deʔpi </ts>
               <ts e="T271" id="Seg_713" n="e" s="T270">kola </ts>
               <ts e="T272" id="Seg_715" n="e" s="T271">urgo </ts>
               <ts e="T273" id="Seg_717" n="e" s="T272">dĭ. </ts>
               <ts e="T274" id="Seg_719" n="e" s="T273">Dĭn </ts>
               <ts e="T275" id="Seg_721" n="e" s="T274">net </ts>
               <ts e="T276" id="Seg_723" n="e" s="T275">dagajzʼiʔ </ts>
               <ts e="T277" id="Seg_725" n="e" s="T276">băʔpi. </ts>
               <ts e="T278" id="Seg_727" n="e" s="T277">I </ts>
               <ts e="T279" id="Seg_729" n="e" s="T278">pürbi, </ts>
               <ts e="T280" id="Seg_731" n="e" s="T279">a </ts>
               <ts e="T281" id="Seg_733" n="e" s="T280">dĭ </ts>
               <ts e="T282" id="Seg_735" n="e" s="T281">sadarlaʔ </ts>
               <ts e="T283" id="Seg_737" n="e" s="T282">kambi. </ts>
               <ts e="T284" id="Seg_739" n="e" s="T283">Dĭgəttə </ts>
               <ts e="T285" id="Seg_741" n="e" s="T284">dĭzeŋ </ts>
               <ts e="T286" id="Seg_743" n="e" s="T285">ibiʔi </ts>
               <ts e="T287" id="Seg_745" n="e" s="T286">aktʼanə </ts>
               <ts e="T288" id="Seg_747" n="e" s="T287">tura. </ts>
               <ts e="T289" id="Seg_749" n="e" s="T288">I </ts>
               <ts e="T290" id="Seg_751" n="e" s="T289">dĭn </ts>
               <ts e="T291" id="Seg_753" n="e" s="T290">iʔgö </ts>
               <ts e="T292" id="Seg_755" n="e" s="T291">sat </ts>
               <ts e="T293" id="Seg_757" n="e" s="T292">amnobi? </ts>
               <ts e="T294" id="Seg_759" n="e" s="T293">Dĭgəttə </ts>
               <ts e="T295" id="Seg_761" n="e" s="T294">onʼiʔ </ts>
               <ts e="T296" id="Seg_763" n="e" s="T295">măndə: </ts>
               <ts e="T297" id="Seg_765" n="e" s="T296">"Šindi </ts>
               <ts e="T298" id="Seg_767" n="e" s="T297">bɨ </ts>
               <ts e="T299" id="Seg_769" n="e" s="T298">dĭ </ts>
               <ts e="T300" id="Seg_771" n="e" s="T299">(süj-) </ts>
               <ts e="T301" id="Seg_773" n="e" s="T300">süjöm </ts>
               <ts e="T302" id="Seg_775" n="e" s="T301">dʼaʔpi, </ts>
               <ts e="T303" id="Seg_777" n="e" s="T302">măn </ts>
               <ts e="T304" id="Seg_779" n="e" s="T303">bɨ </ts>
               <ts e="T305" id="Seg_781" n="e" s="T304">iʔgö </ts>
               <ts e="T306" id="Seg_783" n="e" s="T305">aktʼa </ts>
               <ts e="T307" id="Seg_785" n="e" s="T306">mĭbiem </ts>
               <ts e="T308" id="Seg_787" n="e" s="T307">bɨ". </ts>
               <ts e="T309" id="Seg_789" n="e" s="T308">Dĭgəttə </ts>
               <ts e="T310" id="Seg_791" n="e" s="T309">dĭ </ts>
               <ts e="T311" id="Seg_793" n="e" s="T310">măndə: </ts>
               <ts e="T312" id="Seg_795" n="e" s="T311">"Măn </ts>
               <ts e="T313" id="Seg_797" n="e" s="T312">dʼaʔpiam". </ts>
               <ts e="T314" id="Seg_799" n="e" s="T313">Dĭgəttə </ts>
               <ts e="T315" id="Seg_801" n="e" s="T314">iʔgö </ts>
               <ts e="T316" id="Seg_803" n="e" s="T315">il </ts>
               <ts e="T317" id="Seg_805" n="e" s="T316">šobiʔi. </ts>
               <ts e="T318" id="Seg_807" n="e" s="T317">Süjö </ts>
               <ts e="T319" id="Seg_809" n="e" s="T318">nʼergöleʔ </ts>
               <ts e="T320" id="Seg_811" n="e" s="T319">šobi. </ts>
               <ts e="T321" id="Seg_813" n="e" s="T320">Dĭ </ts>
               <ts e="T322" id="Seg_815" n="e" s="T321">bar </ts>
               <ts e="T323" id="Seg_817" n="e" s="T322">nuldəbi </ts>
               <ts e="T324" id="Seg_819" n="e" s="T323">dĭn </ts>
               <ts e="T325" id="Seg_821" n="e" s="T324">amzittə. </ts>
               <ts e="T326" id="Seg_823" n="e" s="T325">"Süjö, </ts>
               <ts e="T327" id="Seg_825" n="e" s="T326">amnoʔ, </ts>
               <ts e="T328" id="Seg_827" n="e" s="T327">amaʔ". </ts>
               <ts e="T329" id="Seg_829" n="e" s="T328">Dĭgəttə </ts>
               <ts e="T330" id="Seg_831" n="e" s="T329">dĭ </ts>
               <ts e="T331" id="Seg_833" n="e" s="T330">süjö </ts>
               <ts e="T332" id="Seg_835" n="e" s="T331">amnobi </ts>
               <ts e="T333" id="Seg_837" n="e" s="T332">amzittə. </ts>
               <ts e="T334" id="Seg_839" n="e" s="T333">Dĭgəttə </ts>
               <ts e="T335" id="Seg_841" n="e" s="T334">dĭ </ts>
               <ts e="T337" id="Seg_843" n="e" s="T335">süjö, </ts>
               <ts e="T338" id="Seg_845" n="e" s="T337">dĭzeŋ </ts>
               <ts e="T339" id="Seg_847" n="e" s="T338">bar </ts>
               <ts e="T340" id="Seg_849" n="e" s="T339">il </ts>
               <ts e="T341" id="Seg_851" n="e" s="T340">dĭm </ts>
               <ts e="T342" id="Seg_853" n="e" s="T341">dʼaʔpiʔi. </ts>
               <ts e="T343" id="Seg_855" n="e" s="T342">A </ts>
               <ts e="T344" id="Seg_857" n="e" s="T343">dĭ </ts>
               <ts e="T345" id="Seg_859" n="e" s="T344">kuza </ts>
               <ts e="T346" id="Seg_861" n="e" s="T345">dʼaʔpi </ts>
               <ts e="T347" id="Seg_863" n="e" s="T346">ujutsiʔ. </ts>
               <ts e="T348" id="Seg_865" n="e" s="T347">Dĭgəttə </ts>
               <ts e="T349" id="Seg_867" n="e" s="T348">dĭ </ts>
               <ts e="T350" id="Seg_869" n="e" s="T349">kuzan </ts>
               <ts e="T351" id="Seg_871" n="e" s="T350">ujutsiʔ </ts>
               <ts e="T352" id="Seg_873" n="e" s="T351">bar </ts>
               <ts e="T353" id="Seg_875" n="e" s="T352">il </ts>
               <ts e="T354" id="Seg_877" n="e" s="T353">(nʼuʔdəbi). </ts>
               <ts e="T355" id="Seg_879" n="e" s="T354">Dĭgəttə </ts>
               <ts e="T356" id="Seg_881" n="e" s="T355">dĭ </ts>
               <ts e="T357" id="Seg_883" n="e" s="T356">kuzan </ts>
               <ts e="T358" id="Seg_885" n="e" s="T357">udaʔi </ts>
               <ts e="T359" id="Seg_887" n="e" s="T358">tararluʔpi, </ts>
               <ts e="T360" id="Seg_889" n="e" s="T359">dĭ </ts>
               <ts e="T361" id="Seg_891" n="e" s="T360">saʔməluʔpi </ts>
               <ts e="T362" id="Seg_893" n="e" s="T361">i </ts>
               <ts e="T363" id="Seg_895" n="e" s="T362">(bal-) </ts>
               <ts e="T364" id="Seg_897" n="e" s="T363">bar </ts>
               <ts e="T365" id="Seg_899" n="e" s="T364">il </ts>
               <ts e="T366" id="Seg_901" n="e" s="T365">külaːmbiʔi. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T212" id="Seg_902" s="T208">PKZ_196X_BirdBringsFish_flk.001 (001)</ta>
            <ta e="T216" id="Seg_903" s="T212">PKZ_196X_BirdBringsFish_flk.002 (002)</ta>
            <ta e="T219" id="Seg_904" s="T216">PKZ_196X_BirdBringsFish_flk.003 (003)</ta>
            <ta e="T222" id="Seg_905" s="T219">PKZ_196X_BirdBringsFish_flk.004 (004)</ta>
            <ta e="T227" id="Seg_906" s="T222">PKZ_196X_BirdBringsFish_flk.005 (005)</ta>
            <ta e="T231" id="Seg_907" s="T227">PKZ_196X_BirdBringsFish_flk.006 (006)</ta>
            <ta e="T235" id="Seg_908" s="T231">PKZ_196X_BirdBringsFish_flk.007 (007)</ta>
            <ta e="T241" id="Seg_909" s="T235">PKZ_196X_BirdBringsFish_flk.008 (008)</ta>
            <ta e="T248" id="Seg_910" s="T241">PKZ_196X_BirdBringsFish_flk.009 (009)</ta>
            <ta e="T260" id="Seg_911" s="T248">PKZ_196X_BirdBringsFish_flk.010 (011)</ta>
            <ta e="T265" id="Seg_912" s="T260">PKZ_196X_BirdBringsFish_flk.011 (012)</ta>
            <ta e="T273" id="Seg_913" s="T265">PKZ_196X_BirdBringsFish_flk.012 (013)</ta>
            <ta e="T277" id="Seg_914" s="T273">PKZ_196X_BirdBringsFish_flk.013 (014)</ta>
            <ta e="T283" id="Seg_915" s="T277">PKZ_196X_BirdBringsFish_flk.014 (015)</ta>
            <ta e="T288" id="Seg_916" s="T283">PKZ_196X_BirdBringsFish_flk.015 (016)</ta>
            <ta e="T293" id="Seg_917" s="T288">PKZ_196X_BirdBringsFish_flk.016 (017)</ta>
            <ta e="T308" id="Seg_918" s="T293">PKZ_196X_BirdBringsFish_flk.017 (018)</ta>
            <ta e="T313" id="Seg_919" s="T308">PKZ_196X_BirdBringsFish_flk.018 (019)</ta>
            <ta e="T317" id="Seg_920" s="T313">PKZ_196X_BirdBringsFish_flk.019 (020)</ta>
            <ta e="T320" id="Seg_921" s="T317">PKZ_196X_BirdBringsFish_flk.020 (021)</ta>
            <ta e="T325" id="Seg_922" s="T320">PKZ_196X_BirdBringsFish_flk.021 (022)</ta>
            <ta e="T328" id="Seg_923" s="T325">PKZ_196X_BirdBringsFish_flk.022 (023)</ta>
            <ta e="T333" id="Seg_924" s="T328">PKZ_196X_BirdBringsFish_flk.023 (024)</ta>
            <ta e="T342" id="Seg_925" s="T333">PKZ_196X_BirdBringsFish_flk.024 (025)</ta>
            <ta e="T347" id="Seg_926" s="T342">PKZ_196X_BirdBringsFish_flk.025 (026)</ta>
            <ta e="T354" id="Seg_927" s="T347">PKZ_196X_BirdBringsFish_flk.026 (027)</ta>
            <ta e="T366" id="Seg_928" s="T354">PKZ_196X_BirdBringsFish_flk.027 (028)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T212" id="Seg_929" s="T208">Amnobi kuza nükkeziʔ bar. </ta>
            <ta e="T216" id="Seg_930" s="T212">Kola dʼaʔpi i sadarbi. </ta>
            <ta e="T219" id="Seg_931" s="T216">Dăreʔ amnobiʔi, amnobiʔi. </ta>
            <ta e="T222" id="Seg_932" s="T219">Ĭmbidə nagobi dĭzen. </ta>
            <ta e="T227" id="Seg_933" s="T222">Dĭgəttə nʼergöleʔ šobi (šuj-) süjö. </ta>
            <ta e="T231" id="Seg_934" s="T227">Dĭ dʼaʔpi üdʼüge kola. </ta>
            <ta e="T235" id="Seg_935" s="T231">"Ĭmbi alil dĭ kolazʼiʔ?" </ta>
            <ta e="T241" id="Seg_936" s="T235">"Da sadarlim da ipek iləm amzittə". </ta>
            <ta e="T248" id="Seg_937" s="T241">A dĭ măndə dĭʔnə: "Amga tănan ipek. </ta>
            <ta e="T260" id="Seg_938" s="T248">Kanaʔ maːʔnʼi, a măn (kal-) kažən dʼala tănan (deʔ-) detləm urgo kola! </ta>
            <ta e="T265" id="Seg_939" s="T260">I dĭgəttə šindənədə iʔ nörbəʔ!" </ta>
            <ta e="T273" id="Seg_940" s="T265">Dĭ (kal-) kažnɨj dʼala deʔpi kola urgo dĭ. </ta>
            <ta e="T277" id="Seg_941" s="T273">Dĭn net dagajzʼiʔ băʔpi. </ta>
            <ta e="T283" id="Seg_942" s="T277">I pürbi, a dĭ sadarlaʔ kambi. </ta>
            <ta e="T288" id="Seg_943" s="T283">Dĭgəttə dĭzeŋ ibiʔi aktʼanə tura. </ta>
            <ta e="T293" id="Seg_944" s="T288">I dĭn iʔgö sat amnobi? </ta>
            <ta e="T308" id="Seg_945" s="T293">Dĭgəttə onʼiʔ măndə:" Šindi bɨ dĭ (süj-) süjöm dʼaʔpi, măn bɨ iʔgö aktʼa mĭbiem bɨ". </ta>
            <ta e="T313" id="Seg_946" s="T308">Dĭgəttə dĭ măndə:" Măn dʼaʔpiam". </ta>
            <ta e="T317" id="Seg_947" s="T313">Dĭgəttə iʔgö il šobiʔi. </ta>
            <ta e="T320" id="Seg_948" s="T317">Süjö nʼergöleʔ šobi. </ta>
            <ta e="T325" id="Seg_949" s="T320">Dĭ bar nuldəbi dĭn amzittə. </ta>
            <ta e="T328" id="Seg_950" s="T325">"Süjö, amnoʔ, amaʔ". </ta>
            <ta e="T333" id="Seg_951" s="T328">Dĭgəttə dĭ süjö amnobi amzittə. </ta>
            <ta e="T342" id="Seg_952" s="T333">Dĭgəttə dĭ süjö, dĭzeŋ bar il dĭm dʼaʔpiʔi. </ta>
            <ta e="T347" id="Seg_953" s="T342">A dĭ kuza dʼaʔpi ujutsiʔ. </ta>
            <ta e="T354" id="Seg_954" s="T347">Dĭgəttə dĭ kuzan ujutsiʔ bar il (nʼuʔdəbi). </ta>
            <ta e="T366" id="Seg_955" s="T354">Dĭgəttə dĭ kuzan udaʔi tararluʔpi, dĭ saʔməluʔpi i (bal-) bar il külaːmbiʔi. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T209" id="Seg_956" s="T208">amno-bi</ta>
            <ta e="T210" id="Seg_957" s="T209">kuza</ta>
            <ta e="T211" id="Seg_958" s="T210">nüke-ziʔ</ta>
            <ta e="T212" id="Seg_959" s="T211">bar</ta>
            <ta e="T213" id="Seg_960" s="T212">kola</ta>
            <ta e="T214" id="Seg_961" s="T213">dʼaʔ-pi</ta>
            <ta e="T215" id="Seg_962" s="T214">i</ta>
            <ta e="T216" id="Seg_963" s="T215">sadar-bi</ta>
            <ta e="T217" id="Seg_964" s="T216">dăreʔ</ta>
            <ta e="T218" id="Seg_965" s="T217">amno-bi-ʔi</ta>
            <ta e="T219" id="Seg_966" s="T218">amno-bi-ʔi</ta>
            <ta e="T220" id="Seg_967" s="T219">ĭmbi=də</ta>
            <ta e="T221" id="Seg_968" s="T220">nago-bi</ta>
            <ta e="T222" id="Seg_969" s="T221">dĭ-zen</ta>
            <ta e="T223" id="Seg_970" s="T222">dĭgəttə</ta>
            <ta e="T224" id="Seg_971" s="T223">nʼergö-leʔ</ta>
            <ta e="T225" id="Seg_972" s="T224">šo-bi</ta>
            <ta e="T227" id="Seg_973" s="T226">süjö</ta>
            <ta e="T228" id="Seg_974" s="T227">dĭ</ta>
            <ta e="T229" id="Seg_975" s="T228">dʼaʔ-pi</ta>
            <ta e="T230" id="Seg_976" s="T229">üdʼüge</ta>
            <ta e="T231" id="Seg_977" s="T230">kola</ta>
            <ta e="T232" id="Seg_978" s="T231">ĭmbi</ta>
            <ta e="T233" id="Seg_979" s="T232">a-li-l</ta>
            <ta e="T234" id="Seg_980" s="T233">dĭ</ta>
            <ta e="T235" id="Seg_981" s="T234">kola-zʼiʔ</ta>
            <ta e="T236" id="Seg_982" s="T235">da</ta>
            <ta e="T237" id="Seg_983" s="T236">sadar-li-m</ta>
            <ta e="T238" id="Seg_984" s="T237">da</ta>
            <ta e="T239" id="Seg_985" s="T238">ipek</ta>
            <ta e="T240" id="Seg_986" s="T239">i-lə-m</ta>
            <ta e="T241" id="Seg_987" s="T240">am-zittə</ta>
            <ta e="T242" id="Seg_988" s="T241">a</ta>
            <ta e="T243" id="Seg_989" s="T242">dĭ</ta>
            <ta e="T244" id="Seg_990" s="T243">măn-də</ta>
            <ta e="T245" id="Seg_991" s="T244">dĭʔ-nə</ta>
            <ta e="T246" id="Seg_992" s="T245">amga</ta>
            <ta e="T247" id="Seg_993" s="T246">tănan</ta>
            <ta e="T248" id="Seg_994" s="T247">ipek</ta>
            <ta e="T249" id="Seg_995" s="T248">kan-a-ʔ</ta>
            <ta e="T250" id="Seg_996" s="T249">maːʔ-nʼi</ta>
            <ta e="T251" id="Seg_997" s="T250">a</ta>
            <ta e="T252" id="Seg_998" s="T251">măn</ta>
            <ta e="T254" id="Seg_999" s="T253">kažən</ta>
            <ta e="T255" id="Seg_1000" s="T254">dʼala</ta>
            <ta e="T256" id="Seg_1001" s="T255">tănan</ta>
            <ta e="T258" id="Seg_1002" s="T257">det-lə-m</ta>
            <ta e="T259" id="Seg_1003" s="T258">urgo</ta>
            <ta e="T260" id="Seg_1004" s="T259">kola</ta>
            <ta e="T261" id="Seg_1005" s="T260">i</ta>
            <ta e="T262" id="Seg_1006" s="T261">dĭgəttə</ta>
            <ta e="T263" id="Seg_1007" s="T262">šində-nə=də</ta>
            <ta e="T264" id="Seg_1008" s="T263">i-ʔ</ta>
            <ta e="T265" id="Seg_1009" s="T264">nörbə-ʔ</ta>
            <ta e="T266" id="Seg_1010" s="T265">dĭ</ta>
            <ta e="T268" id="Seg_1011" s="T267">kažnɨj</ta>
            <ta e="T269" id="Seg_1012" s="T268">dʼala</ta>
            <ta e="T270" id="Seg_1013" s="T269">deʔ-pi</ta>
            <ta e="T271" id="Seg_1014" s="T270">kola</ta>
            <ta e="T272" id="Seg_1015" s="T271">urgo</ta>
            <ta e="T273" id="Seg_1016" s="T272">dĭ</ta>
            <ta e="T274" id="Seg_1017" s="T273">dĭ-n</ta>
            <ta e="T275" id="Seg_1018" s="T274">ne-t</ta>
            <ta e="T276" id="Seg_1019" s="T275">dagaj-zʼiʔ</ta>
            <ta e="T277" id="Seg_1020" s="T276">băʔ-pi</ta>
            <ta e="T278" id="Seg_1021" s="T277">i</ta>
            <ta e="T279" id="Seg_1022" s="T278">pür-bi</ta>
            <ta e="T280" id="Seg_1023" s="T279">a</ta>
            <ta e="T281" id="Seg_1024" s="T280">dĭ</ta>
            <ta e="T282" id="Seg_1025" s="T281">sadar-laʔ</ta>
            <ta e="T283" id="Seg_1026" s="T282">kam-bi</ta>
            <ta e="T284" id="Seg_1027" s="T283">dĭgəttə</ta>
            <ta e="T285" id="Seg_1028" s="T284">dĭ-zeŋ</ta>
            <ta e="T286" id="Seg_1029" s="T285">i-bi-ʔi</ta>
            <ta e="T287" id="Seg_1030" s="T286">aktʼa-nə</ta>
            <ta e="T288" id="Seg_1031" s="T287">tura</ta>
            <ta e="T289" id="Seg_1032" s="T288">i</ta>
            <ta e="T290" id="Seg_1033" s="T289">dĭn</ta>
            <ta e="T291" id="Seg_1034" s="T290">iʔgö</ta>
            <ta e="T292" id="Seg_1035" s="T291">sat</ta>
            <ta e="T293" id="Seg_1036" s="T292">amno-bi</ta>
            <ta e="T294" id="Seg_1037" s="T293">dĭgəttə</ta>
            <ta e="T295" id="Seg_1038" s="T294">onʼiʔ</ta>
            <ta e="T296" id="Seg_1039" s="T295">măn-də</ta>
            <ta e="T297" id="Seg_1040" s="T296">šindi</ta>
            <ta e="T298" id="Seg_1041" s="T297">bɨ</ta>
            <ta e="T299" id="Seg_1042" s="T298">dĭ</ta>
            <ta e="T301" id="Seg_1043" s="T300">süjö-m</ta>
            <ta e="T302" id="Seg_1044" s="T301">dʼaʔ-pi</ta>
            <ta e="T303" id="Seg_1045" s="T302">măn</ta>
            <ta e="T304" id="Seg_1046" s="T303">bɨ</ta>
            <ta e="T305" id="Seg_1047" s="T304">iʔgö</ta>
            <ta e="T306" id="Seg_1048" s="T305">aktʼa</ta>
            <ta e="T307" id="Seg_1049" s="T306">mĭ-bie-m</ta>
            <ta e="T308" id="Seg_1050" s="T307">bɨ</ta>
            <ta e="T309" id="Seg_1051" s="T308">dĭgəttə</ta>
            <ta e="T310" id="Seg_1052" s="T309">dĭ</ta>
            <ta e="T311" id="Seg_1053" s="T310">măn-də</ta>
            <ta e="T312" id="Seg_1054" s="T311">măn</ta>
            <ta e="T313" id="Seg_1055" s="T312">dʼaʔ-pia-m</ta>
            <ta e="T314" id="Seg_1056" s="T313">dĭgəttə</ta>
            <ta e="T315" id="Seg_1057" s="T314">iʔgö</ta>
            <ta e="T316" id="Seg_1058" s="T315">il</ta>
            <ta e="T317" id="Seg_1059" s="T316">šo-bi-ʔi</ta>
            <ta e="T318" id="Seg_1060" s="T317">süjö</ta>
            <ta e="T319" id="Seg_1061" s="T318">nʼergö-leʔ</ta>
            <ta e="T320" id="Seg_1062" s="T319">šo-bi</ta>
            <ta e="T321" id="Seg_1063" s="T320">dĭ</ta>
            <ta e="T322" id="Seg_1064" s="T321">bar</ta>
            <ta e="T323" id="Seg_1065" s="T322">nuldə-bi</ta>
            <ta e="T324" id="Seg_1066" s="T323">dĭn</ta>
            <ta e="T325" id="Seg_1067" s="T324">am-zittə</ta>
            <ta e="T326" id="Seg_1068" s="T325">süjö</ta>
            <ta e="T327" id="Seg_1069" s="T326">amno-ʔ</ta>
            <ta e="T328" id="Seg_1070" s="T327">am-a-ʔ</ta>
            <ta e="T329" id="Seg_1071" s="T328">dĭgəttə</ta>
            <ta e="T330" id="Seg_1072" s="T329">dĭ</ta>
            <ta e="T331" id="Seg_1073" s="T330">süjö</ta>
            <ta e="T332" id="Seg_1074" s="T331">amno-bi</ta>
            <ta e="T333" id="Seg_1075" s="T332">am-zittə</ta>
            <ta e="T334" id="Seg_1076" s="T333">dĭgəttə</ta>
            <ta e="T335" id="Seg_1077" s="T334">dĭ</ta>
            <ta e="T337" id="Seg_1078" s="T335">süjö</ta>
            <ta e="T338" id="Seg_1079" s="T337">dĭ-zeŋ</ta>
            <ta e="T339" id="Seg_1080" s="T338">bar</ta>
            <ta e="T340" id="Seg_1081" s="T339">il</ta>
            <ta e="T341" id="Seg_1082" s="T340">dĭ-m</ta>
            <ta e="T342" id="Seg_1083" s="T341">dʼaʔ-pi-ʔi</ta>
            <ta e="T343" id="Seg_1084" s="T342">a</ta>
            <ta e="T344" id="Seg_1085" s="T343">dĭ</ta>
            <ta e="T345" id="Seg_1086" s="T344">kuza</ta>
            <ta e="T346" id="Seg_1087" s="T345">dʼaʔ-pi</ta>
            <ta e="T347" id="Seg_1088" s="T346">uju-t-siʔ</ta>
            <ta e="T348" id="Seg_1089" s="T347">dĭgəttə</ta>
            <ta e="T349" id="Seg_1090" s="T348">dĭ</ta>
            <ta e="T350" id="Seg_1091" s="T349">kuza-n</ta>
            <ta e="T351" id="Seg_1092" s="T350">uju-t-siʔ</ta>
            <ta e="T352" id="Seg_1093" s="T351">bar</ta>
            <ta e="T353" id="Seg_1094" s="T352">il</ta>
            <ta e="T354" id="Seg_1095" s="T353">nʼuʔdə-bi</ta>
            <ta e="T355" id="Seg_1096" s="T354">dĭgəttə</ta>
            <ta e="T356" id="Seg_1097" s="T355">dĭ</ta>
            <ta e="T357" id="Seg_1098" s="T356">kuza-n</ta>
            <ta e="T358" id="Seg_1099" s="T357">uda-ʔi</ta>
            <ta e="T359" id="Seg_1100" s="T358">tarar-luʔ-pi</ta>
            <ta e="T360" id="Seg_1101" s="T359">dĭ</ta>
            <ta e="T361" id="Seg_1102" s="T360">saʔmə-luʔ-pi</ta>
            <ta e="T362" id="Seg_1103" s="T361">i</ta>
            <ta e="T364" id="Seg_1104" s="T363">bar</ta>
            <ta e="T365" id="Seg_1105" s="T364">il</ta>
            <ta e="T366" id="Seg_1106" s="T365">kü-laːm-bi-ʔi</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T209" id="Seg_1107" s="T208">amno-bi</ta>
            <ta e="T210" id="Seg_1108" s="T209">kuza</ta>
            <ta e="T211" id="Seg_1109" s="T210">nüke-ziʔ</ta>
            <ta e="T212" id="Seg_1110" s="T211">bar</ta>
            <ta e="T213" id="Seg_1111" s="T212">kola</ta>
            <ta e="T214" id="Seg_1112" s="T213">dʼabə-bi</ta>
            <ta e="T215" id="Seg_1113" s="T214">i</ta>
            <ta e="T216" id="Seg_1114" s="T215">sădar-bi</ta>
            <ta e="T217" id="Seg_1115" s="T216">dărəʔ</ta>
            <ta e="T218" id="Seg_1116" s="T217">amno-bi-jəʔ</ta>
            <ta e="T219" id="Seg_1117" s="T218">amno-bi-jəʔ</ta>
            <ta e="T220" id="Seg_1118" s="T219">ĭmbi=də</ta>
            <ta e="T221" id="Seg_1119" s="T220">naga-bi</ta>
            <ta e="T222" id="Seg_1120" s="T221">dĭ-zen</ta>
            <ta e="T223" id="Seg_1121" s="T222">dĭgəttə</ta>
            <ta e="T224" id="Seg_1122" s="T223">nʼergö-lAʔ</ta>
            <ta e="T225" id="Seg_1123" s="T224">šo-bi</ta>
            <ta e="T227" id="Seg_1124" s="T226">süjö</ta>
            <ta e="T228" id="Seg_1125" s="T227">dĭ</ta>
            <ta e="T229" id="Seg_1126" s="T228">dʼabə-bi</ta>
            <ta e="T230" id="Seg_1127" s="T229">üdʼüge</ta>
            <ta e="T231" id="Seg_1128" s="T230">kola</ta>
            <ta e="T232" id="Seg_1129" s="T231">ĭmbi</ta>
            <ta e="T233" id="Seg_1130" s="T232">a-lV-l</ta>
            <ta e="T234" id="Seg_1131" s="T233">dĭ</ta>
            <ta e="T235" id="Seg_1132" s="T234">kola-ziʔ</ta>
            <ta e="T236" id="Seg_1133" s="T235">da</ta>
            <ta e="T237" id="Seg_1134" s="T236">sădar-lV-m</ta>
            <ta e="T238" id="Seg_1135" s="T237">da</ta>
            <ta e="T239" id="Seg_1136" s="T238">ipek</ta>
            <ta e="T240" id="Seg_1137" s="T239">i-lV-m</ta>
            <ta e="T241" id="Seg_1138" s="T240">am-zittə</ta>
            <ta e="T242" id="Seg_1139" s="T241">a</ta>
            <ta e="T243" id="Seg_1140" s="T242">dĭ</ta>
            <ta e="T244" id="Seg_1141" s="T243">măn-ntə</ta>
            <ta e="T245" id="Seg_1142" s="T244">dĭ-Tə</ta>
            <ta e="T246" id="Seg_1143" s="T245">amka</ta>
            <ta e="T247" id="Seg_1144" s="T246">tănan</ta>
            <ta e="T248" id="Seg_1145" s="T247">ipek</ta>
            <ta e="T249" id="Seg_1146" s="T248">kan-ə-ʔ</ta>
            <ta e="T250" id="Seg_1147" s="T249">maʔ-gənʼi</ta>
            <ta e="T251" id="Seg_1148" s="T250">a</ta>
            <ta e="T252" id="Seg_1149" s="T251">măn</ta>
            <ta e="T254" id="Seg_1150" s="T253">kažən</ta>
            <ta e="T255" id="Seg_1151" s="T254">tʼala</ta>
            <ta e="T256" id="Seg_1152" s="T255">tănan</ta>
            <ta e="T258" id="Seg_1153" s="T257">det-lV-m</ta>
            <ta e="T259" id="Seg_1154" s="T258">urgo</ta>
            <ta e="T260" id="Seg_1155" s="T259">kola</ta>
            <ta e="T261" id="Seg_1156" s="T260">i</ta>
            <ta e="T262" id="Seg_1157" s="T261">dĭgəttə</ta>
            <ta e="T263" id="Seg_1158" s="T262">šində-Tə=də</ta>
            <ta e="T264" id="Seg_1159" s="T263">e-ʔ</ta>
            <ta e="T265" id="Seg_1160" s="T264">nörbə-ʔ</ta>
            <ta e="T266" id="Seg_1161" s="T265">dĭ</ta>
            <ta e="T268" id="Seg_1162" s="T267">kažən</ta>
            <ta e="T269" id="Seg_1163" s="T268">tʼala</ta>
            <ta e="T270" id="Seg_1164" s="T269">det-bi</ta>
            <ta e="T271" id="Seg_1165" s="T270">kola</ta>
            <ta e="T272" id="Seg_1166" s="T271">urgo</ta>
            <ta e="T273" id="Seg_1167" s="T272">dĭ</ta>
            <ta e="T274" id="Seg_1168" s="T273">dĭ-n</ta>
            <ta e="T275" id="Seg_1169" s="T274">ne-t</ta>
            <ta e="T276" id="Seg_1170" s="T275">tagaj-ziʔ</ta>
            <ta e="T277" id="Seg_1171" s="T276">băt-bi</ta>
            <ta e="T278" id="Seg_1172" s="T277">i</ta>
            <ta e="T279" id="Seg_1173" s="T278">pür-bi</ta>
            <ta e="T280" id="Seg_1174" s="T279">a</ta>
            <ta e="T281" id="Seg_1175" s="T280">dĭ</ta>
            <ta e="T282" id="Seg_1176" s="T281">sădar-lAʔ</ta>
            <ta e="T283" id="Seg_1177" s="T282">kan-bi</ta>
            <ta e="T284" id="Seg_1178" s="T283">dĭgəttə</ta>
            <ta e="T285" id="Seg_1179" s="T284">dĭ-zAŋ</ta>
            <ta e="T286" id="Seg_1180" s="T285">i-bi-jəʔ</ta>
            <ta e="T287" id="Seg_1181" s="T286">aktʼa-Tə</ta>
            <ta e="T288" id="Seg_1182" s="T287">tura</ta>
            <ta e="T289" id="Seg_1183" s="T288">i</ta>
            <ta e="T290" id="Seg_1184" s="T289">dĭn</ta>
            <ta e="T291" id="Seg_1185" s="T290">iʔgö</ta>
            <ta e="T292" id="Seg_1186" s="T291">sat</ta>
            <ta e="T293" id="Seg_1187" s="T292">amno-bi</ta>
            <ta e="T294" id="Seg_1188" s="T293">dĭgəttə</ta>
            <ta e="T295" id="Seg_1189" s="T294">onʼiʔ</ta>
            <ta e="T296" id="Seg_1190" s="T295">măn-ntə</ta>
            <ta e="T297" id="Seg_1191" s="T296">šində</ta>
            <ta e="T298" id="Seg_1192" s="T297">bɨ</ta>
            <ta e="T299" id="Seg_1193" s="T298">dĭ</ta>
            <ta e="T301" id="Seg_1194" s="T300">süjö-m</ta>
            <ta e="T302" id="Seg_1195" s="T301">dʼabə-bi</ta>
            <ta e="T303" id="Seg_1196" s="T302">măn</ta>
            <ta e="T304" id="Seg_1197" s="T303">bɨ</ta>
            <ta e="T305" id="Seg_1198" s="T304">iʔgö</ta>
            <ta e="T306" id="Seg_1199" s="T305">aktʼa</ta>
            <ta e="T307" id="Seg_1200" s="T306">mĭ-bi-m</ta>
            <ta e="T308" id="Seg_1201" s="T307">bɨ</ta>
            <ta e="T309" id="Seg_1202" s="T308">dĭgəttə</ta>
            <ta e="T310" id="Seg_1203" s="T309">dĭ</ta>
            <ta e="T311" id="Seg_1204" s="T310">măn-ntə</ta>
            <ta e="T312" id="Seg_1205" s="T311">măn</ta>
            <ta e="T313" id="Seg_1206" s="T312">dʼabə-bi-m</ta>
            <ta e="T314" id="Seg_1207" s="T313">dĭgəttə</ta>
            <ta e="T315" id="Seg_1208" s="T314">iʔgö</ta>
            <ta e="T316" id="Seg_1209" s="T315">il</ta>
            <ta e="T317" id="Seg_1210" s="T316">šo-bi-jəʔ</ta>
            <ta e="T318" id="Seg_1211" s="T317">süjö</ta>
            <ta e="T319" id="Seg_1212" s="T318">nʼergö-lAʔ</ta>
            <ta e="T320" id="Seg_1213" s="T319">šo-bi</ta>
            <ta e="T321" id="Seg_1214" s="T320">dĭ</ta>
            <ta e="T322" id="Seg_1215" s="T321">bar</ta>
            <ta e="T323" id="Seg_1216" s="T322">nuldə-bi</ta>
            <ta e="T324" id="Seg_1217" s="T323">dĭn</ta>
            <ta e="T325" id="Seg_1218" s="T324">am-zittə</ta>
            <ta e="T326" id="Seg_1219" s="T325">süjö</ta>
            <ta e="T327" id="Seg_1220" s="T326">amnə-ʔ</ta>
            <ta e="T328" id="Seg_1221" s="T327">am-ə-ʔ</ta>
            <ta e="T329" id="Seg_1222" s="T328">dĭgəttə</ta>
            <ta e="T330" id="Seg_1223" s="T329">dĭ</ta>
            <ta e="T331" id="Seg_1224" s="T330">süjö</ta>
            <ta e="T332" id="Seg_1225" s="T331">amnə-bi</ta>
            <ta e="T333" id="Seg_1226" s="T332">am-zittə</ta>
            <ta e="T334" id="Seg_1227" s="T333">dĭgəttə</ta>
            <ta e="T335" id="Seg_1228" s="T334">dĭ</ta>
            <ta e="T337" id="Seg_1229" s="T335">süjö</ta>
            <ta e="T338" id="Seg_1230" s="T337">dĭ-zAŋ</ta>
            <ta e="T339" id="Seg_1231" s="T338">bar</ta>
            <ta e="T340" id="Seg_1232" s="T339">il</ta>
            <ta e="T341" id="Seg_1233" s="T340">dĭ-m</ta>
            <ta e="T342" id="Seg_1234" s="T341">dʼabə-bi-jəʔ</ta>
            <ta e="T343" id="Seg_1235" s="T342">a</ta>
            <ta e="T344" id="Seg_1236" s="T343">dĭ</ta>
            <ta e="T345" id="Seg_1237" s="T344">kuza</ta>
            <ta e="T346" id="Seg_1238" s="T345">dʼabə-bi</ta>
            <ta e="T347" id="Seg_1239" s="T346">üjü-t-ziʔ</ta>
            <ta e="T348" id="Seg_1240" s="T347">dĭgəttə</ta>
            <ta e="T349" id="Seg_1241" s="T348">dĭ</ta>
            <ta e="T350" id="Seg_1242" s="T349">kuza-n</ta>
            <ta e="T351" id="Seg_1243" s="T350">üjü-t-ziʔ</ta>
            <ta e="T352" id="Seg_1244" s="T351">bar</ta>
            <ta e="T353" id="Seg_1245" s="T352">il</ta>
            <ta e="T354" id="Seg_1246" s="T353">nʼuʔdə-bi</ta>
            <ta e="T355" id="Seg_1247" s="T354">dĭgəttə</ta>
            <ta e="T356" id="Seg_1248" s="T355">dĭ</ta>
            <ta e="T357" id="Seg_1249" s="T356">kuza-n</ta>
            <ta e="T358" id="Seg_1250" s="T357">uda-jəʔ</ta>
            <ta e="T359" id="Seg_1251" s="T358">tarar-luʔbdə-bi</ta>
            <ta e="T360" id="Seg_1252" s="T359">dĭ</ta>
            <ta e="T361" id="Seg_1253" s="T360">saʔmə-luʔbdə-bi</ta>
            <ta e="T362" id="Seg_1254" s="T361">i</ta>
            <ta e="T364" id="Seg_1255" s="T363">bar</ta>
            <ta e="T365" id="Seg_1256" s="T364">il</ta>
            <ta e="T366" id="Seg_1257" s="T365">kü-laːm-bi-jəʔ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T209" id="Seg_1258" s="T208">live-PST.[3SG]</ta>
            <ta e="T210" id="Seg_1259" s="T209">man.[NOM.SG]</ta>
            <ta e="T211" id="Seg_1260" s="T210">woman-INS</ta>
            <ta e="T212" id="Seg_1261" s="T211">PTCL</ta>
            <ta e="T213" id="Seg_1262" s="T212">fish.[NOM.SG]</ta>
            <ta e="T214" id="Seg_1263" s="T213">capture-PST.[3SG]</ta>
            <ta e="T215" id="Seg_1264" s="T214">and</ta>
            <ta e="T216" id="Seg_1265" s="T215">sell-PST.[3SG]</ta>
            <ta e="T217" id="Seg_1266" s="T216">so</ta>
            <ta e="T218" id="Seg_1267" s="T217">live-PST-3PL</ta>
            <ta e="T219" id="Seg_1268" s="T218">live-PST-3PL</ta>
            <ta e="T220" id="Seg_1269" s="T219">what.[NOM.SG]=INDEF</ta>
            <ta e="T221" id="Seg_1270" s="T220">NEG.EX-PST.[3SG]</ta>
            <ta e="T222" id="Seg_1271" s="T221">this-GEN.PL</ta>
            <ta e="T223" id="Seg_1272" s="T222">then</ta>
            <ta e="T224" id="Seg_1273" s="T223">fly-CVB</ta>
            <ta e="T225" id="Seg_1274" s="T224">come-PST.[3SG]</ta>
            <ta e="T227" id="Seg_1275" s="T226">bird.[NOM.SG]</ta>
            <ta e="T228" id="Seg_1276" s="T227">this.[NOM.SG]</ta>
            <ta e="T229" id="Seg_1277" s="T228">capture-PST.[3SG]</ta>
            <ta e="T230" id="Seg_1278" s="T229">small.[NOM.SG]</ta>
            <ta e="T231" id="Seg_1279" s="T230">fish.[NOM.SG]</ta>
            <ta e="T232" id="Seg_1280" s="T231">what.[NOM.SG]</ta>
            <ta e="T233" id="Seg_1281" s="T232">make-FUT-2SG</ta>
            <ta e="T234" id="Seg_1282" s="T233">this.[NOM.SG]</ta>
            <ta e="T235" id="Seg_1283" s="T234">fish-INS</ta>
            <ta e="T236" id="Seg_1284" s="T235">and</ta>
            <ta e="T237" id="Seg_1285" s="T236">sell-FUT-1SG</ta>
            <ta e="T238" id="Seg_1286" s="T237">and</ta>
            <ta e="T239" id="Seg_1287" s="T238">bread.[NOM.SG]</ta>
            <ta e="T240" id="Seg_1288" s="T239">take-FUT-1SG</ta>
            <ta e="T241" id="Seg_1289" s="T240">eat-INF.LAT</ta>
            <ta e="T242" id="Seg_1290" s="T241">and</ta>
            <ta e="T243" id="Seg_1291" s="T242">this.[NOM.SG]</ta>
            <ta e="T244" id="Seg_1292" s="T243">say-IPFVZ.[3SG]</ta>
            <ta e="T245" id="Seg_1293" s="T244">this-LAT</ta>
            <ta e="T246" id="Seg_1294" s="T245">few</ta>
            <ta e="T247" id="Seg_1295" s="T246">you.DAT</ta>
            <ta e="T248" id="Seg_1296" s="T247">bread.[NOM.SG]</ta>
            <ta e="T249" id="Seg_1297" s="T248">go-EP-IMP.2SG</ta>
            <ta e="T250" id="Seg_1298" s="T249">tent-LAT/LOC.1SG</ta>
            <ta e="T251" id="Seg_1299" s="T250">and</ta>
            <ta e="T252" id="Seg_1300" s="T251">I.NOM</ta>
            <ta e="T254" id="Seg_1301" s="T253">each.[NOM.SG]</ta>
            <ta e="T255" id="Seg_1302" s="T254">day.[NOM.SG]</ta>
            <ta e="T256" id="Seg_1303" s="T255">you.DAT</ta>
            <ta e="T258" id="Seg_1304" s="T257">bring-FUT-1SG</ta>
            <ta e="T259" id="Seg_1305" s="T258">big.[NOM.SG]</ta>
            <ta e="T260" id="Seg_1306" s="T259">fish.[NOM.SG]</ta>
            <ta e="T261" id="Seg_1307" s="T260">and</ta>
            <ta e="T262" id="Seg_1308" s="T261">then</ta>
            <ta e="T263" id="Seg_1309" s="T262">who-LAT=INDEF</ta>
            <ta e="T264" id="Seg_1310" s="T263">NEG.AUX-IMP.2SG</ta>
            <ta e="T265" id="Seg_1311" s="T264">tell-CNG</ta>
            <ta e="T266" id="Seg_1312" s="T265">this.[NOM.SG]</ta>
            <ta e="T268" id="Seg_1313" s="T267">each.[NOM.SG]</ta>
            <ta e="T269" id="Seg_1314" s="T268">day.[NOM.SG]</ta>
            <ta e="T270" id="Seg_1315" s="T269">bring-PST.[3SG]</ta>
            <ta e="T271" id="Seg_1316" s="T270">fish.[NOM.SG]</ta>
            <ta e="T272" id="Seg_1317" s="T271">big.[NOM.SG]</ta>
            <ta e="T273" id="Seg_1318" s="T272">this.[NOM.SG]</ta>
            <ta e="T274" id="Seg_1319" s="T273">this-GEN</ta>
            <ta e="T275" id="Seg_1320" s="T274">woman-NOM/GEN.3SG</ta>
            <ta e="T276" id="Seg_1321" s="T275">knife-INS</ta>
            <ta e="T277" id="Seg_1322" s="T276">cut-PST.[3SG]</ta>
            <ta e="T278" id="Seg_1323" s="T277">and</ta>
            <ta e="T279" id="Seg_1324" s="T278">bake-PST.[3SG]</ta>
            <ta e="T280" id="Seg_1325" s="T279">and</ta>
            <ta e="T281" id="Seg_1326" s="T280">this.[NOM.SG]</ta>
            <ta e="T282" id="Seg_1327" s="T281">sell-CVB</ta>
            <ta e="T283" id="Seg_1328" s="T282">go-PST.[3SG]</ta>
            <ta e="T284" id="Seg_1329" s="T283">then</ta>
            <ta e="T285" id="Seg_1330" s="T284">this-PL</ta>
            <ta e="T286" id="Seg_1331" s="T285">take-PST-3PL</ta>
            <ta e="T287" id="Seg_1332" s="T286">money-LAT</ta>
            <ta e="T288" id="Seg_1333" s="T287">house.[NOM.SG]</ta>
            <ta e="T289" id="Seg_1334" s="T288">and</ta>
            <ta e="T290" id="Seg_1335" s="T289">there</ta>
            <ta e="T291" id="Seg_1336" s="T290">many</ta>
            <ta e="T292" id="Seg_1337" s="T291">garden.[NOM.SG]</ta>
            <ta e="T293" id="Seg_1338" s="T292">sit-PST.[3SG]</ta>
            <ta e="T294" id="Seg_1339" s="T293">then</ta>
            <ta e="T295" id="Seg_1340" s="T294">single.[NOM.SG]</ta>
            <ta e="T296" id="Seg_1341" s="T295">say-IPFVZ.[3SG]</ta>
            <ta e="T297" id="Seg_1342" s="T296">who.[NOM.SG]</ta>
            <ta e="T298" id="Seg_1343" s="T297">IRREAL</ta>
            <ta e="T299" id="Seg_1344" s="T298">this.[NOM.SG]</ta>
            <ta e="T301" id="Seg_1345" s="T300">bird-ACC</ta>
            <ta e="T302" id="Seg_1346" s="T301">capture-PST.[3SG]</ta>
            <ta e="T303" id="Seg_1347" s="T302">I.NOM</ta>
            <ta e="T304" id="Seg_1348" s="T303">IRREAL</ta>
            <ta e="T305" id="Seg_1349" s="T304">many</ta>
            <ta e="T306" id="Seg_1350" s="T305">money.[NOM.SG]</ta>
            <ta e="T307" id="Seg_1351" s="T306">give-PST-1SG</ta>
            <ta e="T308" id="Seg_1352" s="T307">IRREAL</ta>
            <ta e="T309" id="Seg_1353" s="T308">then</ta>
            <ta e="T310" id="Seg_1354" s="T309">this.[NOM.SG]</ta>
            <ta e="T311" id="Seg_1355" s="T310">say-IPFVZ.[3SG]</ta>
            <ta e="T312" id="Seg_1356" s="T311">I.NOM</ta>
            <ta e="T313" id="Seg_1357" s="T312">capture-PST-1SG</ta>
            <ta e="T314" id="Seg_1358" s="T313">then</ta>
            <ta e="T315" id="Seg_1359" s="T314">many</ta>
            <ta e="T316" id="Seg_1360" s="T315">people.[NOM.SG]</ta>
            <ta e="T317" id="Seg_1361" s="T316">come-PST-3PL</ta>
            <ta e="T318" id="Seg_1362" s="T317">bird.[NOM.SG]</ta>
            <ta e="T319" id="Seg_1363" s="T318">fly-CVB</ta>
            <ta e="T320" id="Seg_1364" s="T319">come-PST.[3SG]</ta>
            <ta e="T321" id="Seg_1365" s="T320">this.[NOM.SG]</ta>
            <ta e="T322" id="Seg_1366" s="T321">PTCL</ta>
            <ta e="T323" id="Seg_1367" s="T322">place-PST.[3SG]</ta>
            <ta e="T324" id="Seg_1368" s="T323">there</ta>
            <ta e="T325" id="Seg_1369" s="T324">eat-INF.LAT</ta>
            <ta e="T326" id="Seg_1370" s="T325">bird.[NOM.SG]</ta>
            <ta e="T327" id="Seg_1371" s="T326">sit.down-IMP.2SG</ta>
            <ta e="T328" id="Seg_1372" s="T327">eat-EP-IMP.2SG</ta>
            <ta e="T329" id="Seg_1373" s="T328">then</ta>
            <ta e="T330" id="Seg_1374" s="T329">this.[NOM.SG]</ta>
            <ta e="T331" id="Seg_1375" s="T330">bird.[NOM.SG]</ta>
            <ta e="T332" id="Seg_1376" s="T331">sit.down-PST.[3SG]</ta>
            <ta e="T333" id="Seg_1377" s="T332">eat-INF.LAT</ta>
            <ta e="T334" id="Seg_1378" s="T333">then</ta>
            <ta e="T335" id="Seg_1379" s="T334">this.[NOM.SG]</ta>
            <ta e="T337" id="Seg_1380" s="T335">bird.[NOM.SG]</ta>
            <ta e="T338" id="Seg_1381" s="T337">this-PL</ta>
            <ta e="T339" id="Seg_1382" s="T338">all</ta>
            <ta e="T340" id="Seg_1383" s="T339">people.[NOM.SG]</ta>
            <ta e="T341" id="Seg_1384" s="T340">this-ACC</ta>
            <ta e="T342" id="Seg_1385" s="T341">capture-PST-3PL</ta>
            <ta e="T343" id="Seg_1386" s="T342">and</ta>
            <ta e="T344" id="Seg_1387" s="T343">this.[NOM.SG]</ta>
            <ta e="T345" id="Seg_1388" s="T344">man.[NOM.SG]</ta>
            <ta e="T346" id="Seg_1389" s="T345">capture-PST.[3SG]</ta>
            <ta e="T347" id="Seg_1390" s="T346">foot-3SG-INS</ta>
            <ta e="T348" id="Seg_1391" s="T347">then</ta>
            <ta e="T349" id="Seg_1392" s="T348">this.[NOM.SG]</ta>
            <ta e="T350" id="Seg_1393" s="T349">man-GEN</ta>
            <ta e="T351" id="Seg_1394" s="T350">foot-3SG-INS</ta>
            <ta e="T352" id="Seg_1395" s="T351">all</ta>
            <ta e="T353" id="Seg_1396" s="T352">people.[NOM.SG]</ta>
            <ta e="T354" id="Seg_1397" s="T353">up-PST.[3SG]</ta>
            <ta e="T355" id="Seg_1398" s="T354">then</ta>
            <ta e="T356" id="Seg_1399" s="T355">this.[NOM.SG]</ta>
            <ta e="T357" id="Seg_1400" s="T356">man-GEN</ta>
            <ta e="T358" id="Seg_1401" s="T357">hand-PL</ta>
            <ta e="T359" id="Seg_1402" s="T358">get.tired-MOM-PST.[3SG]</ta>
            <ta e="T360" id="Seg_1403" s="T359">this.[NOM.SG]</ta>
            <ta e="T361" id="Seg_1404" s="T360">fall-MOM-PST.[3SG]</ta>
            <ta e="T362" id="Seg_1405" s="T361">and</ta>
            <ta e="T364" id="Seg_1406" s="T363">all</ta>
            <ta e="T365" id="Seg_1407" s="T364">people.[NOM.SG]</ta>
            <ta e="T366" id="Seg_1408" s="T365">die-RES-PST-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T209" id="Seg_1409" s="T208">жить-PST.[3SG]</ta>
            <ta e="T210" id="Seg_1410" s="T209">мужчина.[NOM.SG]</ta>
            <ta e="T211" id="Seg_1411" s="T210">женщина-INS</ta>
            <ta e="T212" id="Seg_1412" s="T211">PTCL</ta>
            <ta e="T213" id="Seg_1413" s="T212">рыба.[NOM.SG]</ta>
            <ta e="T214" id="Seg_1414" s="T213">ловить-PST.[3SG]</ta>
            <ta e="T215" id="Seg_1415" s="T214">и</ta>
            <ta e="T216" id="Seg_1416" s="T215">продавать-PST.[3SG]</ta>
            <ta e="T217" id="Seg_1417" s="T216">так</ta>
            <ta e="T218" id="Seg_1418" s="T217">жить-PST-3PL</ta>
            <ta e="T219" id="Seg_1419" s="T218">жить-PST-3PL</ta>
            <ta e="T220" id="Seg_1420" s="T219">что.[NOM.SG]=INDEF</ta>
            <ta e="T221" id="Seg_1421" s="T220">NEG.EX-PST.[3SG]</ta>
            <ta e="T222" id="Seg_1422" s="T221">этот-GEN.PL</ta>
            <ta e="T223" id="Seg_1423" s="T222">тогда</ta>
            <ta e="T224" id="Seg_1424" s="T223">лететь-CVB</ta>
            <ta e="T225" id="Seg_1425" s="T224">прийти-PST.[3SG]</ta>
            <ta e="T227" id="Seg_1426" s="T226">птица.[NOM.SG]</ta>
            <ta e="T228" id="Seg_1427" s="T227">этот.[NOM.SG]</ta>
            <ta e="T229" id="Seg_1428" s="T228">ловить-PST.[3SG]</ta>
            <ta e="T230" id="Seg_1429" s="T229">маленький.[NOM.SG]</ta>
            <ta e="T231" id="Seg_1430" s="T230">рыба.[NOM.SG]</ta>
            <ta e="T232" id="Seg_1431" s="T231">что.[NOM.SG]</ta>
            <ta e="T233" id="Seg_1432" s="T232">делать-FUT-2SG</ta>
            <ta e="T234" id="Seg_1433" s="T233">этот.[NOM.SG]</ta>
            <ta e="T235" id="Seg_1434" s="T234">рыба-INS</ta>
            <ta e="T236" id="Seg_1435" s="T235">и</ta>
            <ta e="T237" id="Seg_1436" s="T236">продавать-FUT-1SG</ta>
            <ta e="T238" id="Seg_1437" s="T237">и</ta>
            <ta e="T239" id="Seg_1438" s="T238">хлеб.[NOM.SG]</ta>
            <ta e="T240" id="Seg_1439" s="T239">взять-FUT-1SG</ta>
            <ta e="T241" id="Seg_1440" s="T240">съесть-INF.LAT</ta>
            <ta e="T242" id="Seg_1441" s="T241">а</ta>
            <ta e="T243" id="Seg_1442" s="T242">этот.[NOM.SG]</ta>
            <ta e="T244" id="Seg_1443" s="T243">сказать-IPFVZ.[3SG]</ta>
            <ta e="T245" id="Seg_1444" s="T244">этот-LAT</ta>
            <ta e="T246" id="Seg_1445" s="T245">мало</ta>
            <ta e="T247" id="Seg_1446" s="T246">ты.DAT</ta>
            <ta e="T248" id="Seg_1447" s="T247">хлеб.[NOM.SG]</ta>
            <ta e="T249" id="Seg_1448" s="T248">пойти-EP-IMP.2SG</ta>
            <ta e="T250" id="Seg_1449" s="T249">чум-LAT/LOC.1SG</ta>
            <ta e="T251" id="Seg_1450" s="T250">а</ta>
            <ta e="T252" id="Seg_1451" s="T251">я.NOM</ta>
            <ta e="T254" id="Seg_1452" s="T253">каждый.[NOM.SG]</ta>
            <ta e="T255" id="Seg_1453" s="T254">день.[NOM.SG]</ta>
            <ta e="T256" id="Seg_1454" s="T255">ты.DAT</ta>
            <ta e="T258" id="Seg_1455" s="T257">принести-FUT-1SG</ta>
            <ta e="T259" id="Seg_1456" s="T258">большой.[NOM.SG]</ta>
            <ta e="T260" id="Seg_1457" s="T259">рыба.[NOM.SG]</ta>
            <ta e="T261" id="Seg_1458" s="T260">и</ta>
            <ta e="T262" id="Seg_1459" s="T261">тогда</ta>
            <ta e="T263" id="Seg_1460" s="T262">кто-LAT=INDEF</ta>
            <ta e="T264" id="Seg_1461" s="T263">NEG.AUX-IMP.2SG</ta>
            <ta e="T265" id="Seg_1462" s="T264">сказать-CNG</ta>
            <ta e="T266" id="Seg_1463" s="T265">этот.[NOM.SG]</ta>
            <ta e="T268" id="Seg_1464" s="T267">каждый.[NOM.SG]</ta>
            <ta e="T269" id="Seg_1465" s="T268">день.[NOM.SG]</ta>
            <ta e="T270" id="Seg_1466" s="T269">принести-PST.[3SG]</ta>
            <ta e="T271" id="Seg_1467" s="T270">рыба.[NOM.SG]</ta>
            <ta e="T272" id="Seg_1468" s="T271">большой.[NOM.SG]</ta>
            <ta e="T273" id="Seg_1469" s="T272">этот.[NOM.SG]</ta>
            <ta e="T274" id="Seg_1470" s="T273">этот-GEN</ta>
            <ta e="T275" id="Seg_1471" s="T274">женщина-NOM/GEN.3SG</ta>
            <ta e="T276" id="Seg_1472" s="T275">нож-INS</ta>
            <ta e="T277" id="Seg_1473" s="T276">резать-PST.[3SG]</ta>
            <ta e="T278" id="Seg_1474" s="T277">и</ta>
            <ta e="T279" id="Seg_1475" s="T278">печь-PST.[3SG]</ta>
            <ta e="T280" id="Seg_1476" s="T279">а</ta>
            <ta e="T281" id="Seg_1477" s="T280">этот.[NOM.SG]</ta>
            <ta e="T282" id="Seg_1478" s="T281">продавать-CVB</ta>
            <ta e="T283" id="Seg_1479" s="T282">пойти-PST.[3SG]</ta>
            <ta e="T284" id="Seg_1480" s="T283">тогда</ta>
            <ta e="T285" id="Seg_1481" s="T284">этот-PL</ta>
            <ta e="T286" id="Seg_1482" s="T285">взять-PST-3PL</ta>
            <ta e="T287" id="Seg_1483" s="T286">деньги-LAT</ta>
            <ta e="T288" id="Seg_1484" s="T287">дом.[NOM.SG]</ta>
            <ta e="T289" id="Seg_1485" s="T288">и</ta>
            <ta e="T290" id="Seg_1486" s="T289">там</ta>
            <ta e="T291" id="Seg_1487" s="T290">много</ta>
            <ta e="T292" id="Seg_1488" s="T291">сад.[NOM.SG]</ta>
            <ta e="T293" id="Seg_1489" s="T292">сидеть-PST.[3SG]</ta>
            <ta e="T294" id="Seg_1490" s="T293">тогда</ta>
            <ta e="T295" id="Seg_1491" s="T294">один.[NOM.SG]</ta>
            <ta e="T296" id="Seg_1492" s="T295">сказать-IPFVZ.[3SG]</ta>
            <ta e="T297" id="Seg_1493" s="T296">кто.[NOM.SG]</ta>
            <ta e="T298" id="Seg_1494" s="T297">IRREAL</ta>
            <ta e="T299" id="Seg_1495" s="T298">этот.[NOM.SG]</ta>
            <ta e="T301" id="Seg_1496" s="T300">птица-ACC</ta>
            <ta e="T302" id="Seg_1497" s="T301">ловить-PST.[3SG]</ta>
            <ta e="T303" id="Seg_1498" s="T302">я.NOM</ta>
            <ta e="T304" id="Seg_1499" s="T303">IRREAL</ta>
            <ta e="T305" id="Seg_1500" s="T304">много</ta>
            <ta e="T306" id="Seg_1501" s="T305">деньги.[NOM.SG]</ta>
            <ta e="T307" id="Seg_1502" s="T306">дать-PST-1SG</ta>
            <ta e="T308" id="Seg_1503" s="T307">IRREAL</ta>
            <ta e="T309" id="Seg_1504" s="T308">тогда</ta>
            <ta e="T310" id="Seg_1505" s="T309">этот.[NOM.SG]</ta>
            <ta e="T311" id="Seg_1506" s="T310">сказать-IPFVZ.[3SG]</ta>
            <ta e="T312" id="Seg_1507" s="T311">я.NOM</ta>
            <ta e="T313" id="Seg_1508" s="T312">ловить-PST-1SG</ta>
            <ta e="T314" id="Seg_1509" s="T313">тогда</ta>
            <ta e="T315" id="Seg_1510" s="T314">много</ta>
            <ta e="T316" id="Seg_1511" s="T315">люди.[NOM.SG]</ta>
            <ta e="T317" id="Seg_1512" s="T316">прийти-PST-3PL</ta>
            <ta e="T318" id="Seg_1513" s="T317">птица.[NOM.SG]</ta>
            <ta e="T319" id="Seg_1514" s="T318">лететь-CVB</ta>
            <ta e="T320" id="Seg_1515" s="T319">прийти-PST.[3SG]</ta>
            <ta e="T321" id="Seg_1516" s="T320">этот.[NOM.SG]</ta>
            <ta e="T322" id="Seg_1517" s="T321">PTCL</ta>
            <ta e="T323" id="Seg_1518" s="T322">поставить-PST.[3SG]</ta>
            <ta e="T324" id="Seg_1519" s="T323">там</ta>
            <ta e="T325" id="Seg_1520" s="T324">съесть-INF.LAT</ta>
            <ta e="T326" id="Seg_1521" s="T325">птица.[NOM.SG]</ta>
            <ta e="T327" id="Seg_1522" s="T326">сесть-IMP.2SG</ta>
            <ta e="T328" id="Seg_1523" s="T327">съесть-EP-IMP.2SG</ta>
            <ta e="T329" id="Seg_1524" s="T328">тогда</ta>
            <ta e="T330" id="Seg_1525" s="T329">этот.[NOM.SG]</ta>
            <ta e="T331" id="Seg_1526" s="T330">птица.[NOM.SG]</ta>
            <ta e="T332" id="Seg_1527" s="T331">сесть-PST.[3SG]</ta>
            <ta e="T333" id="Seg_1528" s="T332">съесть-INF.LAT</ta>
            <ta e="T334" id="Seg_1529" s="T333">тогда</ta>
            <ta e="T335" id="Seg_1530" s="T334">этот.[NOM.SG]</ta>
            <ta e="T337" id="Seg_1531" s="T335">птица.[NOM.SG]</ta>
            <ta e="T338" id="Seg_1532" s="T337">этот-PL</ta>
            <ta e="T339" id="Seg_1533" s="T338">весь</ta>
            <ta e="T340" id="Seg_1534" s="T339">люди.[NOM.SG]</ta>
            <ta e="T341" id="Seg_1535" s="T340">этот-ACC</ta>
            <ta e="T342" id="Seg_1536" s="T341">ловить-PST-3PL</ta>
            <ta e="T343" id="Seg_1537" s="T342">а</ta>
            <ta e="T344" id="Seg_1538" s="T343">этот.[NOM.SG]</ta>
            <ta e="T345" id="Seg_1539" s="T344">мужчина.[NOM.SG]</ta>
            <ta e="T346" id="Seg_1540" s="T345">ловить-PST.[3SG]</ta>
            <ta e="T347" id="Seg_1541" s="T346">нога-3SG-INS</ta>
            <ta e="T348" id="Seg_1542" s="T347">тогда</ta>
            <ta e="T349" id="Seg_1543" s="T348">этот.[NOM.SG]</ta>
            <ta e="T350" id="Seg_1544" s="T349">мужчина-GEN</ta>
            <ta e="T351" id="Seg_1545" s="T350">нога-3SG-INS</ta>
            <ta e="T352" id="Seg_1546" s="T351">весь</ta>
            <ta e="T353" id="Seg_1547" s="T352">люди.[NOM.SG]</ta>
            <ta e="T354" id="Seg_1548" s="T353">вверх-PST.[3SG]</ta>
            <ta e="T355" id="Seg_1549" s="T354">тогда</ta>
            <ta e="T356" id="Seg_1550" s="T355">этот.[NOM.SG]</ta>
            <ta e="T357" id="Seg_1551" s="T356">мужчина-GEN</ta>
            <ta e="T358" id="Seg_1552" s="T357">рука-PL</ta>
            <ta e="T359" id="Seg_1553" s="T358">устать-MOM-PST.[3SG]</ta>
            <ta e="T360" id="Seg_1554" s="T359">этот.[NOM.SG]</ta>
            <ta e="T361" id="Seg_1555" s="T360">упасть-MOM-PST.[3SG]</ta>
            <ta e="T362" id="Seg_1556" s="T361">и</ta>
            <ta e="T364" id="Seg_1557" s="T363">весь</ta>
            <ta e="T365" id="Seg_1558" s="T364">люди.[NOM.SG]</ta>
            <ta e="T366" id="Seg_1559" s="T365">умереть-RES-PST-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T209" id="Seg_1560" s="T208">v-v:tense-v:pn</ta>
            <ta e="T210" id="Seg_1561" s="T209">n-n:case</ta>
            <ta e="T211" id="Seg_1562" s="T210">n-n:case</ta>
            <ta e="T212" id="Seg_1563" s="T211">ptcl</ta>
            <ta e="T213" id="Seg_1564" s="T212">n-n:case</ta>
            <ta e="T214" id="Seg_1565" s="T213">v-v:tense-v:pn</ta>
            <ta e="T215" id="Seg_1566" s="T214">conj</ta>
            <ta e="T216" id="Seg_1567" s="T215">v-v:tense-v:pn</ta>
            <ta e="T217" id="Seg_1568" s="T216">ptcl</ta>
            <ta e="T218" id="Seg_1569" s="T217">v-v:tense-v:pn</ta>
            <ta e="T219" id="Seg_1570" s="T218">v-v:tense-v:pn</ta>
            <ta e="T220" id="Seg_1571" s="T219">que-n:case=ptcl</ta>
            <ta e="T221" id="Seg_1572" s="T220">v-v:tense-v:pn</ta>
            <ta e="T222" id="Seg_1573" s="T221">dempro-n:case</ta>
            <ta e="T223" id="Seg_1574" s="T222">adv</ta>
            <ta e="T224" id="Seg_1575" s="T223">v-v:n.fin</ta>
            <ta e="T225" id="Seg_1576" s="T224">v-v:tense-v:pn</ta>
            <ta e="T227" id="Seg_1577" s="T226">n-n:case</ta>
            <ta e="T228" id="Seg_1578" s="T227">dempro-n:case</ta>
            <ta e="T229" id="Seg_1579" s="T228">v-v:tense-v:pn</ta>
            <ta e="T230" id="Seg_1580" s="T229">adj-n:case</ta>
            <ta e="T231" id="Seg_1581" s="T230">n-n:case</ta>
            <ta e="T232" id="Seg_1582" s="T231">que-n:case</ta>
            <ta e="T233" id="Seg_1583" s="T232">v-v:tense-v:pn</ta>
            <ta e="T234" id="Seg_1584" s="T233">dempro-n:case</ta>
            <ta e="T235" id="Seg_1585" s="T234">n-n:case</ta>
            <ta e="T236" id="Seg_1586" s="T235">conj</ta>
            <ta e="T237" id="Seg_1587" s="T236">v-v:tense-v:pn</ta>
            <ta e="T238" id="Seg_1588" s="T237">conj</ta>
            <ta e="T239" id="Seg_1589" s="T238">n-n:case</ta>
            <ta e="T240" id="Seg_1590" s="T239">v-v:tense-v:pn</ta>
            <ta e="T241" id="Seg_1591" s="T240">v-v:n.fin</ta>
            <ta e="T242" id="Seg_1592" s="T241">conj</ta>
            <ta e="T243" id="Seg_1593" s="T242">dempro-n:case</ta>
            <ta e="T244" id="Seg_1594" s="T243">v-v&gt;v-v:pn</ta>
            <ta e="T245" id="Seg_1595" s="T244">dempro-n:case</ta>
            <ta e="T246" id="Seg_1596" s="T245">adv</ta>
            <ta e="T247" id="Seg_1597" s="T246">pers</ta>
            <ta e="T248" id="Seg_1598" s="T247">n-n:case</ta>
            <ta e="T249" id="Seg_1599" s="T248">v-v:ins-v:mood.pn</ta>
            <ta e="T250" id="Seg_1600" s="T249">n-n:case.poss</ta>
            <ta e="T251" id="Seg_1601" s="T250">conj</ta>
            <ta e="T252" id="Seg_1602" s="T251">pers</ta>
            <ta e="T254" id="Seg_1603" s="T253">adj-n:case</ta>
            <ta e="T255" id="Seg_1604" s="T254">n-n:case</ta>
            <ta e="T256" id="Seg_1605" s="T255">pers</ta>
            <ta e="T258" id="Seg_1606" s="T257">v-v:tense-v:pn</ta>
            <ta e="T259" id="Seg_1607" s="T258">adj-n:case</ta>
            <ta e="T260" id="Seg_1608" s="T259">n-n:case</ta>
            <ta e="T261" id="Seg_1609" s="T260">conj</ta>
            <ta e="T262" id="Seg_1610" s="T261">adv</ta>
            <ta e="T263" id="Seg_1611" s="T262">que-n:case=ptcl</ta>
            <ta e="T264" id="Seg_1612" s="T263">aux-v:mood.pn</ta>
            <ta e="T265" id="Seg_1613" s="T264">v-v:mood.pn</ta>
            <ta e="T266" id="Seg_1614" s="T265">dempro-n:case</ta>
            <ta e="T268" id="Seg_1615" s="T267">adj-n:case</ta>
            <ta e="T269" id="Seg_1616" s="T268">n-n:case</ta>
            <ta e="T270" id="Seg_1617" s="T269">v-v:tense-v:pn</ta>
            <ta e="T271" id="Seg_1618" s="T270">n-n:case</ta>
            <ta e="T272" id="Seg_1619" s="T271">adj-n:case</ta>
            <ta e="T273" id="Seg_1620" s="T272">dempro-n:case</ta>
            <ta e="T274" id="Seg_1621" s="T273">dempro-n:case</ta>
            <ta e="T275" id="Seg_1622" s="T274">n-n:case.poss</ta>
            <ta e="T276" id="Seg_1623" s="T275">n-n:case</ta>
            <ta e="T277" id="Seg_1624" s="T276">v-v:tense-v:pn</ta>
            <ta e="T278" id="Seg_1625" s="T277">conj</ta>
            <ta e="T279" id="Seg_1626" s="T278">v-v:tense-v:pn</ta>
            <ta e="T280" id="Seg_1627" s="T279">conj</ta>
            <ta e="T281" id="Seg_1628" s="T280">dempro-n:case</ta>
            <ta e="T282" id="Seg_1629" s="T281">v-v:n.fin</ta>
            <ta e="T283" id="Seg_1630" s="T282">v-v:tense-v:pn</ta>
            <ta e="T284" id="Seg_1631" s="T283">adv</ta>
            <ta e="T285" id="Seg_1632" s="T284">dempro-n:num</ta>
            <ta e="T286" id="Seg_1633" s="T285">v-v:tense-v:pn</ta>
            <ta e="T287" id="Seg_1634" s="T286">n-n:case</ta>
            <ta e="T288" id="Seg_1635" s="T287">n-n:case</ta>
            <ta e="T289" id="Seg_1636" s="T288">conj</ta>
            <ta e="T290" id="Seg_1637" s="T289">adv</ta>
            <ta e="T291" id="Seg_1638" s="T290">quant</ta>
            <ta e="T292" id="Seg_1639" s="T291">n-n:case</ta>
            <ta e="T293" id="Seg_1640" s="T292">v-v:tense-v:pn</ta>
            <ta e="T294" id="Seg_1641" s="T293">adv</ta>
            <ta e="T295" id="Seg_1642" s="T294">adj-n:case</ta>
            <ta e="T296" id="Seg_1643" s="T295">v-v&gt;v-v:pn</ta>
            <ta e="T297" id="Seg_1644" s="T296">que-n:case</ta>
            <ta e="T298" id="Seg_1645" s="T297">ptcl</ta>
            <ta e="T299" id="Seg_1646" s="T298">dempro-n:case</ta>
            <ta e="T301" id="Seg_1647" s="T300">n-n:case</ta>
            <ta e="T302" id="Seg_1648" s="T301">v-v:tense-v:pn</ta>
            <ta e="T303" id="Seg_1649" s="T302">pers</ta>
            <ta e="T304" id="Seg_1650" s="T303">ptcl</ta>
            <ta e="T305" id="Seg_1651" s="T304">quant</ta>
            <ta e="T306" id="Seg_1652" s="T305">n-n:case</ta>
            <ta e="T307" id="Seg_1653" s="T306">v-v:tense-v:pn</ta>
            <ta e="T308" id="Seg_1654" s="T307">ptcl</ta>
            <ta e="T309" id="Seg_1655" s="T308">adv</ta>
            <ta e="T310" id="Seg_1656" s="T309">dempro-n:case</ta>
            <ta e="T311" id="Seg_1657" s="T310">v-v&gt;v-v:pn</ta>
            <ta e="T312" id="Seg_1658" s="T311">pers</ta>
            <ta e="T313" id="Seg_1659" s="T312">v-v:tense-v:pn</ta>
            <ta e="T314" id="Seg_1660" s="T313">adv</ta>
            <ta e="T315" id="Seg_1661" s="T314">quant</ta>
            <ta e="T316" id="Seg_1662" s="T315">n-n:case</ta>
            <ta e="T317" id="Seg_1663" s="T316">v-v:tense-v:pn</ta>
            <ta e="T318" id="Seg_1664" s="T317">n-n:case</ta>
            <ta e="T319" id="Seg_1665" s="T318">v-v:n.fin</ta>
            <ta e="T320" id="Seg_1666" s="T319">v-v:tense-v:pn</ta>
            <ta e="T321" id="Seg_1667" s="T320">dempro-n:case</ta>
            <ta e="T322" id="Seg_1668" s="T321">ptcl</ta>
            <ta e="T323" id="Seg_1669" s="T322">v-v:tense-v:pn</ta>
            <ta e="T324" id="Seg_1670" s="T323">adv</ta>
            <ta e="T325" id="Seg_1671" s="T324">v-v:n.fin</ta>
            <ta e="T326" id="Seg_1672" s="T325">n-n:case</ta>
            <ta e="T327" id="Seg_1673" s="T326">v-v:mood.pn</ta>
            <ta e="T328" id="Seg_1674" s="T327">v-v:ins-v:mood.pn</ta>
            <ta e="T329" id="Seg_1675" s="T328">adv</ta>
            <ta e="T330" id="Seg_1676" s="T329">dempro-n:case</ta>
            <ta e="T331" id="Seg_1677" s="T330">n-n:case</ta>
            <ta e="T332" id="Seg_1678" s="T331">v-v:tense-v:pn</ta>
            <ta e="T333" id="Seg_1679" s="T332">v-v:n.fin</ta>
            <ta e="T334" id="Seg_1680" s="T333">adv</ta>
            <ta e="T335" id="Seg_1681" s="T334">dempro-n:case</ta>
            <ta e="T337" id="Seg_1682" s="T335">n-n:case</ta>
            <ta e="T338" id="Seg_1683" s="T337">dempro-n:num</ta>
            <ta e="T339" id="Seg_1684" s="T338">quant</ta>
            <ta e="T340" id="Seg_1685" s="T339">n-n:case</ta>
            <ta e="T341" id="Seg_1686" s="T340">dempro-n:case</ta>
            <ta e="T342" id="Seg_1687" s="T341">v-v:tense-v:pn</ta>
            <ta e="T343" id="Seg_1688" s="T342">conj</ta>
            <ta e="T344" id="Seg_1689" s="T343">dempro-n:case</ta>
            <ta e="T345" id="Seg_1690" s="T344">n-n:case</ta>
            <ta e="T346" id="Seg_1691" s="T345">v-v:tense-v:pn</ta>
            <ta e="T347" id="Seg_1692" s="T346">n-n:case.poss-n:case</ta>
            <ta e="T348" id="Seg_1693" s="T347">adv</ta>
            <ta e="T349" id="Seg_1694" s="T348">dempro-n:case</ta>
            <ta e="T350" id="Seg_1695" s="T349">n-n:case</ta>
            <ta e="T351" id="Seg_1696" s="T350">n-n:case.poss-n:case</ta>
            <ta e="T352" id="Seg_1697" s="T351">quant</ta>
            <ta e="T353" id="Seg_1698" s="T352">n-n:case</ta>
            <ta e="T354" id="Seg_1699" s="T353">adv-v:tense-v:pn</ta>
            <ta e="T355" id="Seg_1700" s="T354">adv</ta>
            <ta e="T356" id="Seg_1701" s="T355">dempro-n:case</ta>
            <ta e="T357" id="Seg_1702" s="T356">n-n:case</ta>
            <ta e="T358" id="Seg_1703" s="T357">n-n:num</ta>
            <ta e="T359" id="Seg_1704" s="T358">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T360" id="Seg_1705" s="T359">dempro-n:case</ta>
            <ta e="T361" id="Seg_1706" s="T360">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T362" id="Seg_1707" s="T361">conj</ta>
            <ta e="T364" id="Seg_1708" s="T363">quant</ta>
            <ta e="T365" id="Seg_1709" s="T364">n-n:case</ta>
            <ta e="T366" id="Seg_1710" s="T365">v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T209" id="Seg_1711" s="T208">v</ta>
            <ta e="T210" id="Seg_1712" s="T209">n</ta>
            <ta e="T211" id="Seg_1713" s="T210">n</ta>
            <ta e="T212" id="Seg_1714" s="T211">ptcl</ta>
            <ta e="T213" id="Seg_1715" s="T212">n</ta>
            <ta e="T214" id="Seg_1716" s="T213">v</ta>
            <ta e="T215" id="Seg_1717" s="T214">conj</ta>
            <ta e="T216" id="Seg_1718" s="T215">v</ta>
            <ta e="T217" id="Seg_1719" s="T216">ptcl</ta>
            <ta e="T218" id="Seg_1720" s="T217">v</ta>
            <ta e="T219" id="Seg_1721" s="T218">v</ta>
            <ta e="T220" id="Seg_1722" s="T219">que</ta>
            <ta e="T221" id="Seg_1723" s="T220">v</ta>
            <ta e="T222" id="Seg_1724" s="T221">dempro</ta>
            <ta e="T223" id="Seg_1725" s="T222">adv</ta>
            <ta e="T224" id="Seg_1726" s="T223">v</ta>
            <ta e="T225" id="Seg_1727" s="T224">v</ta>
            <ta e="T227" id="Seg_1728" s="T226">n</ta>
            <ta e="T228" id="Seg_1729" s="T227">dempro</ta>
            <ta e="T229" id="Seg_1730" s="T228">v</ta>
            <ta e="T230" id="Seg_1731" s="T229">adj</ta>
            <ta e="T231" id="Seg_1732" s="T230">n</ta>
            <ta e="T232" id="Seg_1733" s="T231">que</ta>
            <ta e="T233" id="Seg_1734" s="T232">v</ta>
            <ta e="T234" id="Seg_1735" s="T233">dempro</ta>
            <ta e="T235" id="Seg_1736" s="T234">n</ta>
            <ta e="T236" id="Seg_1737" s="T235">conj</ta>
            <ta e="T237" id="Seg_1738" s="T236">v</ta>
            <ta e="T238" id="Seg_1739" s="T237">conj</ta>
            <ta e="T239" id="Seg_1740" s="T238">n</ta>
            <ta e="T240" id="Seg_1741" s="T239">v</ta>
            <ta e="T241" id="Seg_1742" s="T240">v</ta>
            <ta e="T242" id="Seg_1743" s="T241">conj</ta>
            <ta e="T243" id="Seg_1744" s="T242">dempro</ta>
            <ta e="T244" id="Seg_1745" s="T243">v</ta>
            <ta e="T245" id="Seg_1746" s="T244">dempro</ta>
            <ta e="T246" id="Seg_1747" s="T245">adv</ta>
            <ta e="T247" id="Seg_1748" s="T246">pers</ta>
            <ta e="T248" id="Seg_1749" s="T247">n</ta>
            <ta e="T249" id="Seg_1750" s="T248">v</ta>
            <ta e="T250" id="Seg_1751" s="T249">n</ta>
            <ta e="T251" id="Seg_1752" s="T250">conj</ta>
            <ta e="T252" id="Seg_1753" s="T251">pers</ta>
            <ta e="T254" id="Seg_1754" s="T253">adj</ta>
            <ta e="T255" id="Seg_1755" s="T254">n</ta>
            <ta e="T256" id="Seg_1756" s="T255">pers</ta>
            <ta e="T258" id="Seg_1757" s="T257">v</ta>
            <ta e="T259" id="Seg_1758" s="T258">adj</ta>
            <ta e="T260" id="Seg_1759" s="T259">n</ta>
            <ta e="T261" id="Seg_1760" s="T260">conj</ta>
            <ta e="T262" id="Seg_1761" s="T261">adv</ta>
            <ta e="T263" id="Seg_1762" s="T262">que</ta>
            <ta e="T264" id="Seg_1763" s="T263">aux</ta>
            <ta e="T265" id="Seg_1764" s="T264">v</ta>
            <ta e="T266" id="Seg_1765" s="T265">dempro</ta>
            <ta e="T268" id="Seg_1766" s="T267">adj</ta>
            <ta e="T269" id="Seg_1767" s="T268">n</ta>
            <ta e="T270" id="Seg_1768" s="T269">v</ta>
            <ta e="T271" id="Seg_1769" s="T270">n</ta>
            <ta e="T272" id="Seg_1770" s="T271">adj</ta>
            <ta e="T273" id="Seg_1771" s="T272">dempro</ta>
            <ta e="T274" id="Seg_1772" s="T273">dempro</ta>
            <ta e="T275" id="Seg_1773" s="T274">n</ta>
            <ta e="T276" id="Seg_1774" s="T275">n</ta>
            <ta e="T277" id="Seg_1775" s="T276">v</ta>
            <ta e="T278" id="Seg_1776" s="T277">conj</ta>
            <ta e="T279" id="Seg_1777" s="T278">v</ta>
            <ta e="T280" id="Seg_1778" s="T279">conj</ta>
            <ta e="T281" id="Seg_1779" s="T280">dempro</ta>
            <ta e="T282" id="Seg_1780" s="T281">v</ta>
            <ta e="T283" id="Seg_1781" s="T282">v</ta>
            <ta e="T284" id="Seg_1782" s="T283">adv</ta>
            <ta e="T285" id="Seg_1783" s="T284">dempro</ta>
            <ta e="T286" id="Seg_1784" s="T285">v</ta>
            <ta e="T287" id="Seg_1785" s="T286">n</ta>
            <ta e="T288" id="Seg_1786" s="T287">n</ta>
            <ta e="T289" id="Seg_1787" s="T288">conj</ta>
            <ta e="T290" id="Seg_1788" s="T289">adv</ta>
            <ta e="T291" id="Seg_1789" s="T290">quant</ta>
            <ta e="T292" id="Seg_1790" s="T291">n</ta>
            <ta e="T293" id="Seg_1791" s="T292">v</ta>
            <ta e="T294" id="Seg_1792" s="T293">adv</ta>
            <ta e="T295" id="Seg_1793" s="T294">adj</ta>
            <ta e="T296" id="Seg_1794" s="T295">v</ta>
            <ta e="T297" id="Seg_1795" s="T296">que</ta>
            <ta e="T298" id="Seg_1796" s="T297">ptcl</ta>
            <ta e="T299" id="Seg_1797" s="T298">dempro</ta>
            <ta e="T301" id="Seg_1798" s="T300">n</ta>
            <ta e="T302" id="Seg_1799" s="T301">v</ta>
            <ta e="T303" id="Seg_1800" s="T302">pers</ta>
            <ta e="T304" id="Seg_1801" s="T303">ptcl</ta>
            <ta e="T305" id="Seg_1802" s="T304">quant</ta>
            <ta e="T306" id="Seg_1803" s="T305">n</ta>
            <ta e="T307" id="Seg_1804" s="T306">v</ta>
            <ta e="T308" id="Seg_1805" s="T307">ptcl</ta>
            <ta e="T309" id="Seg_1806" s="T308">adv</ta>
            <ta e="T310" id="Seg_1807" s="T309">dempro</ta>
            <ta e="T311" id="Seg_1808" s="T310">v</ta>
            <ta e="T312" id="Seg_1809" s="T311">pers</ta>
            <ta e="T313" id="Seg_1810" s="T312">v</ta>
            <ta e="T314" id="Seg_1811" s="T313">adv</ta>
            <ta e="T315" id="Seg_1812" s="T314">quant</ta>
            <ta e="T316" id="Seg_1813" s="T315">n</ta>
            <ta e="T317" id="Seg_1814" s="T316">v</ta>
            <ta e="T318" id="Seg_1815" s="T317">n</ta>
            <ta e="T319" id="Seg_1816" s="T318">v</ta>
            <ta e="T320" id="Seg_1817" s="T319">v</ta>
            <ta e="T321" id="Seg_1818" s="T320">dempro</ta>
            <ta e="T322" id="Seg_1819" s="T321">ptcl</ta>
            <ta e="T323" id="Seg_1820" s="T322">n</ta>
            <ta e="T324" id="Seg_1821" s="T323">adv</ta>
            <ta e="T325" id="Seg_1822" s="T324">v</ta>
            <ta e="T326" id="Seg_1823" s="T325">n</ta>
            <ta e="T327" id="Seg_1824" s="T326">v</ta>
            <ta e="T328" id="Seg_1825" s="T327">v</ta>
            <ta e="T329" id="Seg_1826" s="T328">adv</ta>
            <ta e="T330" id="Seg_1827" s="T329">dempro</ta>
            <ta e="T331" id="Seg_1828" s="T330">n</ta>
            <ta e="T332" id="Seg_1829" s="T331">v</ta>
            <ta e="T333" id="Seg_1830" s="T332">v</ta>
            <ta e="T334" id="Seg_1831" s="T333">adv</ta>
            <ta e="T335" id="Seg_1832" s="T334">dempro</ta>
            <ta e="T337" id="Seg_1833" s="T335">n</ta>
            <ta e="T338" id="Seg_1834" s="T337">dempro</ta>
            <ta e="T339" id="Seg_1835" s="T338">quant</ta>
            <ta e="T340" id="Seg_1836" s="T339">n</ta>
            <ta e="T341" id="Seg_1837" s="T340">dempro</ta>
            <ta e="T342" id="Seg_1838" s="T341">v</ta>
            <ta e="T343" id="Seg_1839" s="T342">conj</ta>
            <ta e="T344" id="Seg_1840" s="T343">dempro</ta>
            <ta e="T345" id="Seg_1841" s="T344">n</ta>
            <ta e="T346" id="Seg_1842" s="T345">v</ta>
            <ta e="T347" id="Seg_1843" s="T346">n</ta>
            <ta e="T348" id="Seg_1844" s="T347">adv</ta>
            <ta e="T349" id="Seg_1845" s="T348">dempro</ta>
            <ta e="T350" id="Seg_1846" s="T349">n</ta>
            <ta e="T351" id="Seg_1847" s="T350">n</ta>
            <ta e="T352" id="Seg_1848" s="T351">quant</ta>
            <ta e="T353" id="Seg_1849" s="T352">n</ta>
            <ta e="T354" id="Seg_1850" s="T353">v</ta>
            <ta e="T355" id="Seg_1851" s="T354">adv</ta>
            <ta e="T356" id="Seg_1852" s="T355">dempro</ta>
            <ta e="T357" id="Seg_1853" s="T356">n</ta>
            <ta e="T358" id="Seg_1854" s="T357">n</ta>
            <ta e="T359" id="Seg_1855" s="T358">v</ta>
            <ta e="T360" id="Seg_1856" s="T359">dempro</ta>
            <ta e="T361" id="Seg_1857" s="T360">v</ta>
            <ta e="T362" id="Seg_1858" s="T361">conj</ta>
            <ta e="T364" id="Seg_1859" s="T363">quant</ta>
            <ta e="T365" id="Seg_1860" s="T364">n</ta>
            <ta e="T366" id="Seg_1861" s="T365">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T210" id="Seg_1862" s="T209">np.h:E</ta>
            <ta e="T211" id="Seg_1863" s="T210">np.h:Com</ta>
            <ta e="T213" id="Seg_1864" s="T212">np:Th</ta>
            <ta e="T214" id="Seg_1865" s="T213">0.3.h:A</ta>
            <ta e="T216" id="Seg_1866" s="T215">0.3.h:A</ta>
            <ta e="T218" id="Seg_1867" s="T217">0.3.h:E</ta>
            <ta e="T219" id="Seg_1868" s="T218">0.3.h:E</ta>
            <ta e="T220" id="Seg_1869" s="T219">pro:Th</ta>
            <ta e="T222" id="Seg_1870" s="T221">pro.h:Poss</ta>
            <ta e="T223" id="Seg_1871" s="T222">adv:Time</ta>
            <ta e="T227" id="Seg_1872" s="T226">np.h:A</ta>
            <ta e="T228" id="Seg_1873" s="T227">pro.h:A</ta>
            <ta e="T231" id="Seg_1874" s="T230">np:Th</ta>
            <ta e="T233" id="Seg_1875" s="T232">0.2.h:A</ta>
            <ta e="T235" id="Seg_1876" s="T234">np:Ins</ta>
            <ta e="T237" id="Seg_1877" s="T236">0.1.h:A</ta>
            <ta e="T239" id="Seg_1878" s="T238">np:Th</ta>
            <ta e="T240" id="Seg_1879" s="T239">0.1.h:A</ta>
            <ta e="T243" id="Seg_1880" s="T242">pro.h:A</ta>
            <ta e="T245" id="Seg_1881" s="T244">pro.h:R</ta>
            <ta e="T247" id="Seg_1882" s="T246">pro.h:Poss</ta>
            <ta e="T248" id="Seg_1883" s="T247">np:Th</ta>
            <ta e="T249" id="Seg_1884" s="T248">0.2.h:A</ta>
            <ta e="T250" id="Seg_1885" s="T249">np:G</ta>
            <ta e="T252" id="Seg_1886" s="T251">pro.h:A</ta>
            <ta e="T255" id="Seg_1887" s="T254">n:Time</ta>
            <ta e="T256" id="Seg_1888" s="T255">pro.h:R</ta>
            <ta e="T260" id="Seg_1889" s="T259">np:Th</ta>
            <ta e="T262" id="Seg_1890" s="T261">adv:Time</ta>
            <ta e="T263" id="Seg_1891" s="T262">pro.h:R</ta>
            <ta e="T264" id="Seg_1892" s="T263">0.2.h:A</ta>
            <ta e="T266" id="Seg_1893" s="T265">pro.h:A</ta>
            <ta e="T269" id="Seg_1894" s="T268">n:Time</ta>
            <ta e="T271" id="Seg_1895" s="T270">np:Th</ta>
            <ta e="T274" id="Seg_1896" s="T273">pro.h:Poss</ta>
            <ta e="T275" id="Seg_1897" s="T274">np.h:A</ta>
            <ta e="T276" id="Seg_1898" s="T275">np:Ins</ta>
            <ta e="T279" id="Seg_1899" s="T278">0.3.h:A</ta>
            <ta e="T281" id="Seg_1900" s="T280">pro.h:A</ta>
            <ta e="T284" id="Seg_1901" s="T283">adv:Time</ta>
            <ta e="T285" id="Seg_1902" s="T284">pro.h:A</ta>
            <ta e="T287" id="Seg_1903" s="T286">np:G</ta>
            <ta e="T288" id="Seg_1904" s="T287">np:Th</ta>
            <ta e="T290" id="Seg_1905" s="T289">adv:L</ta>
            <ta e="T292" id="Seg_1906" s="T291">np:Th</ta>
            <ta e="T293" id="Seg_1907" s="T292">0.3.h:A</ta>
            <ta e="T294" id="Seg_1908" s="T293">adv:Time</ta>
            <ta e="T295" id="Seg_1909" s="T294">np.h:A</ta>
            <ta e="T297" id="Seg_1910" s="T296">pro.h:A</ta>
            <ta e="T301" id="Seg_1911" s="T300">np.h:Th</ta>
            <ta e="T303" id="Seg_1912" s="T302">pro.h:A</ta>
            <ta e="T306" id="Seg_1913" s="T305">np:Th</ta>
            <ta e="T309" id="Seg_1914" s="T308">adv:Time</ta>
            <ta e="T310" id="Seg_1915" s="T309">pro.h:A</ta>
            <ta e="T312" id="Seg_1916" s="T311">pro.h:A</ta>
            <ta e="T314" id="Seg_1917" s="T313">adv:Time</ta>
            <ta e="T316" id="Seg_1918" s="T315">np.h:A</ta>
            <ta e="T318" id="Seg_1919" s="T317">np.h:A</ta>
            <ta e="T321" id="Seg_1920" s="T320">pro.h:A</ta>
            <ta e="T324" id="Seg_1921" s="T323">adv:L</ta>
            <ta e="T327" id="Seg_1922" s="T326">0.2.h:A</ta>
            <ta e="T328" id="Seg_1923" s="T327">0.2.h:A</ta>
            <ta e="T329" id="Seg_1924" s="T328">adv:Time</ta>
            <ta e="T331" id="Seg_1925" s="T330">np.h:A</ta>
            <ta e="T334" id="Seg_1926" s="T333">adv:Time</ta>
            <ta e="T338" id="Seg_1927" s="T337">pro.h:A</ta>
            <ta e="T340" id="Seg_1928" s="T339">np.h:A</ta>
            <ta e="T341" id="Seg_1929" s="T340">pro.h:Th</ta>
            <ta e="T345" id="Seg_1930" s="T344">np.h:A</ta>
            <ta e="T347" id="Seg_1931" s="T346">np:Ins</ta>
            <ta e="T348" id="Seg_1932" s="T347">adv:Time</ta>
            <ta e="T350" id="Seg_1933" s="T349">np.h:Poss</ta>
            <ta e="T351" id="Seg_1934" s="T350">np:Ins</ta>
            <ta e="T353" id="Seg_1935" s="T352">np.h:A</ta>
            <ta e="T355" id="Seg_1936" s="T354">adv:Time</ta>
            <ta e="T357" id="Seg_1937" s="T356">np.h:Poss</ta>
            <ta e="T358" id="Seg_1938" s="T357">np:E</ta>
            <ta e="T360" id="Seg_1939" s="T359">pro.h:E</ta>
            <ta e="T365" id="Seg_1940" s="T364">np.h:E</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T209" id="Seg_1941" s="T208">v:pred</ta>
            <ta e="T210" id="Seg_1942" s="T209">np.h:S</ta>
            <ta e="T213" id="Seg_1943" s="T212">np:O</ta>
            <ta e="T214" id="Seg_1944" s="T213">v:pred 0.3.h:S</ta>
            <ta e="T216" id="Seg_1945" s="T215">v:pred 0.3.h:S</ta>
            <ta e="T218" id="Seg_1946" s="T217">v:pred 0.3.h:S</ta>
            <ta e="T219" id="Seg_1947" s="T218">v:pred 0.3.h:S</ta>
            <ta e="T220" id="Seg_1948" s="T219">pro:S</ta>
            <ta e="T221" id="Seg_1949" s="T220">v:pred</ta>
            <ta e="T224" id="Seg_1950" s="T223">conv:pred</ta>
            <ta e="T225" id="Seg_1951" s="T224">v:pred</ta>
            <ta e="T227" id="Seg_1952" s="T226">np.h:S</ta>
            <ta e="T228" id="Seg_1953" s="T227">pro.h:S</ta>
            <ta e="T229" id="Seg_1954" s="T228">v:pred</ta>
            <ta e="T231" id="Seg_1955" s="T230">np:O</ta>
            <ta e="T233" id="Seg_1956" s="T232">v:pred 0.2.h:S</ta>
            <ta e="T237" id="Seg_1957" s="T236">v:pred 0.1.h:S</ta>
            <ta e="T239" id="Seg_1958" s="T238">np:O</ta>
            <ta e="T240" id="Seg_1959" s="T239">v:pred 0.1.h:S</ta>
            <ta e="T241" id="Seg_1960" s="T240">v:pred s:purp</ta>
            <ta e="T243" id="Seg_1961" s="T242">pro.h:S</ta>
            <ta e="T244" id="Seg_1962" s="T243">v:pred</ta>
            <ta e="T248" id="Seg_1963" s="T247">np:S</ta>
            <ta e="T249" id="Seg_1964" s="T248">v:pred 0.2.h:S</ta>
            <ta e="T252" id="Seg_1965" s="T251">pro.h:S</ta>
            <ta e="T258" id="Seg_1966" s="T257">v:pred</ta>
            <ta e="T260" id="Seg_1967" s="T259">np:O</ta>
            <ta e="T264" id="Seg_1968" s="T263">v:pred 0.2.h:S</ta>
            <ta e="T266" id="Seg_1969" s="T265">pro.h:S</ta>
            <ta e="T270" id="Seg_1970" s="T269">v:pred</ta>
            <ta e="T271" id="Seg_1971" s="T270">np:O</ta>
            <ta e="T275" id="Seg_1972" s="T274">np.h:S</ta>
            <ta e="T277" id="Seg_1973" s="T276">v:pred</ta>
            <ta e="T279" id="Seg_1974" s="T278">v:pred 0.3.h:S</ta>
            <ta e="T281" id="Seg_1975" s="T280">pro.h:S</ta>
            <ta e="T282" id="Seg_1976" s="T281">conv:pred</ta>
            <ta e="T283" id="Seg_1977" s="T282">v:pred</ta>
            <ta e="T285" id="Seg_1978" s="T284">pro.h:S</ta>
            <ta e="T286" id="Seg_1979" s="T285">v:pred</ta>
            <ta e="T288" id="Seg_1980" s="T287">np:O</ta>
            <ta e="T292" id="Seg_1981" s="T291">np:O</ta>
            <ta e="T293" id="Seg_1982" s="T292">v:pred 0.3.h:S</ta>
            <ta e="T295" id="Seg_1983" s="T294">np.h:S</ta>
            <ta e="T296" id="Seg_1984" s="T295">v:pred</ta>
            <ta e="T297" id="Seg_1985" s="T296">pro.h:S</ta>
            <ta e="T301" id="Seg_1986" s="T300">np.h:O</ta>
            <ta e="T302" id="Seg_1987" s="T301">v:pred</ta>
            <ta e="T303" id="Seg_1988" s="T302">pro.h:S</ta>
            <ta e="T306" id="Seg_1989" s="T305">np:O</ta>
            <ta e="T307" id="Seg_1990" s="T306">v:pred</ta>
            <ta e="T310" id="Seg_1991" s="T309">pro.h:S</ta>
            <ta e="T311" id="Seg_1992" s="T310">v:pred</ta>
            <ta e="T312" id="Seg_1993" s="T311">pro.h:S</ta>
            <ta e="T313" id="Seg_1994" s="T312">v:pred</ta>
            <ta e="T316" id="Seg_1995" s="T315">np.h:S</ta>
            <ta e="T317" id="Seg_1996" s="T316">v:pred</ta>
            <ta e="T318" id="Seg_1997" s="T317">np.h:S</ta>
            <ta e="T319" id="Seg_1998" s="T318">conv:pred</ta>
            <ta e="T320" id="Seg_1999" s="T319">v:pred</ta>
            <ta e="T321" id="Seg_2000" s="T320">pro.h:S</ta>
            <ta e="T323" id="Seg_2001" s="T322">v:pred</ta>
            <ta e="T325" id="Seg_2002" s="T324">v:pred s:purp</ta>
            <ta e="T327" id="Seg_2003" s="T326">v:pred 0.2.h:S</ta>
            <ta e="T328" id="Seg_2004" s="T327">v:pred 0.2.h:S</ta>
            <ta e="T331" id="Seg_2005" s="T330">np.h:S</ta>
            <ta e="T332" id="Seg_2006" s="T331">v:pred</ta>
            <ta e="T333" id="Seg_2007" s="T332">v:pred s:purp</ta>
            <ta e="T338" id="Seg_2008" s="T337">pro.h:S</ta>
            <ta e="T340" id="Seg_2009" s="T339">np.h:S</ta>
            <ta e="T341" id="Seg_2010" s="T340">pro.h:O</ta>
            <ta e="T342" id="Seg_2011" s="T341">v:pred</ta>
            <ta e="T345" id="Seg_2012" s="T344">np.h:S</ta>
            <ta e="T346" id="Seg_2013" s="T345">v:pred</ta>
            <ta e="T353" id="Seg_2014" s="T352">np.h:S</ta>
            <ta e="T354" id="Seg_2015" s="T353">v:pred</ta>
            <ta e="T358" id="Seg_2016" s="T357">np:S</ta>
            <ta e="T359" id="Seg_2017" s="T358">v:pred</ta>
            <ta e="T360" id="Seg_2018" s="T359">pro.h:S</ta>
            <ta e="T361" id="Seg_2019" s="T360">v:pred</ta>
            <ta e="T365" id="Seg_2020" s="T364">np.h:S</ta>
            <ta e="T366" id="Seg_2021" s="T365">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T212" id="Seg_2022" s="T211">TURK:disc</ta>
            <ta e="T215" id="Seg_2023" s="T214">RUS:gram</ta>
            <ta e="T216" id="Seg_2024" s="T215">TURK:cult</ta>
            <ta e="T220" id="Seg_2025" s="T219">TURK:gram(INDEF)</ta>
            <ta e="T236" id="Seg_2026" s="T235">RUS:gram</ta>
            <ta e="T237" id="Seg_2027" s="T236">TURK:cult</ta>
            <ta e="T238" id="Seg_2028" s="T237">RUS:gram</ta>
            <ta e="T242" id="Seg_2029" s="T241">RUS:gram</ta>
            <ta e="T251" id="Seg_2030" s="T250">RUS:gram</ta>
            <ta e="T254" id="Seg_2031" s="T253">RUS:core</ta>
            <ta e="T261" id="Seg_2032" s="T260">RUS:gram</ta>
            <ta e="T263" id="Seg_2033" s="T262">TURK:gram(INDEF)</ta>
            <ta e="T268" id="Seg_2034" s="T267">RUS:core</ta>
            <ta e="T278" id="Seg_2035" s="T277">RUS:gram</ta>
            <ta e="T280" id="Seg_2036" s="T279">RUS:gram</ta>
            <ta e="T282" id="Seg_2037" s="T281">TURK:cult</ta>
            <ta e="T288" id="Seg_2038" s="T287">TAT:cult</ta>
            <ta e="T289" id="Seg_2039" s="T288">RUS:gram</ta>
            <ta e="T292" id="Seg_2040" s="T291">RUS:cult</ta>
            <ta e="T293" id="Seg_2041" s="T292">RUS:calq</ta>
            <ta e="T298" id="Seg_2042" s="T297">RUS:gram</ta>
            <ta e="T304" id="Seg_2043" s="T303">RUS:gram</ta>
            <ta e="T308" id="Seg_2044" s="T307">RUS:gram</ta>
            <ta e="T322" id="Seg_2045" s="T321">TURK:disc</ta>
            <ta e="T339" id="Seg_2046" s="T338">TURK:disc</ta>
            <ta e="T343" id="Seg_2047" s="T342">RUS:gram</ta>
            <ta e="T352" id="Seg_2048" s="T351">TURK:disc</ta>
            <ta e="T362" id="Seg_2049" s="T361">RUS:gram</ta>
            <ta e="T364" id="Seg_2050" s="T363">TURK:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T308" id="Seg_2051" s="T296">RUS:calq</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T212" id="Seg_2052" s="T208">Жил муж с женой.</ta>
            <ta e="T216" id="Seg_2053" s="T212">Он ловил рыбу и продавал.</ta>
            <ta e="T219" id="Seg_2054" s="T216">Так жили они, жили.</ta>
            <ta e="T222" id="Seg_2055" s="T219">Ничего у них не было.</ta>
            <ta e="T227" id="Seg_2056" s="T222">Потом прилетела птица.</ta>
            <ta e="T231" id="Seg_2057" s="T227">Он поймал маленькую рыбёшку.</ta>
            <ta e="T235" id="Seg_2058" s="T231">«Что ты сделаешь с этой рыбой?»</ta>
            <ta e="T241" id="Seg_2059" s="T235">«Да продам и куплю хлеба, чтобы есть».</ta>
            <ta e="T245" id="Seg_2060" s="T241">А она ему говорит:</ta>
            <ta e="T248" id="Seg_2061" s="T245">«Мало у тебя [будет] хлеба.</ta>
            <ta e="T260" id="Seg_2062" s="T248">Иди домой, а я каждый день тебе буду приносить большую рыбу.</ta>
            <ta e="T265" id="Seg_2063" s="T260">И тогда не говори никому!»</ta>
            <ta e="T273" id="Seg_2064" s="T265">Она каждый день ему приносила большую рыбу.</ta>
            <ta e="T277" id="Seg_2065" s="T273">Его жена её (разделывала?) ножом.</ta>
            <ta e="T283" id="Seg_2066" s="T277">И готовила, а он шёл её продавать.</ta>
            <ta e="T288" id="Seg_2067" s="T283">Потом они купили дом [на эти] деньги.</ta>
            <ta e="T293" id="Seg_2068" s="T288">И посадили большой сад. [?]</ta>
            <ta e="T308" id="Seg_2069" s="T293">Потом один [человек] говорит: «Если бы кто поймал эту птицу, я бы дал ему много денег».</ta>
            <ta e="T313" id="Seg_2070" s="T308">Потом он говорит: «Я (поймаю?)».</ta>
            <ta e="T317" id="Seg_2071" s="T313">Потом пришло много людей.</ta>
            <ta e="T320" id="Seg_2072" s="T317">Прилетела птица.</ta>
            <ta e="T325" id="Seg_2073" s="T320">Он положил для неё [еды] поесть.</ta>
            <ta e="T328" id="Seg_2074" s="T325">«Птица, садись, ешь».</ta>
            <ta e="T333" id="Seg_2075" s="T328">Тогда птица села поесть.</ta>
            <ta e="T342" id="Seg_2076" s="T333">Потом эта птица… все эти люди её стали ловить.</ta>
            <ta e="T347" id="Seg_2077" s="T342">А этот человек ухватил её за ногу.</ta>
            <ta e="T354" id="Seg_2078" s="T347">Потом все люди схватили этого человека за ноги.</ta>
            <ta e="T366" id="Seg_2079" s="T354">Потом у этого человека руки устали, он упал, и все люди умерли.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T212" id="Seg_2080" s="T208">There lived a man with a woman.</ta>
            <ta e="T216" id="Seg_2081" s="T212">He caught fish and sold [it].</ta>
            <ta e="T219" id="Seg_2082" s="T216">So they lived, they lived.</ta>
            <ta e="T222" id="Seg_2083" s="T219">They had nothing.</ta>
            <ta e="T227" id="Seg_2084" s="T222">Then a bird came flying.</ta>
            <ta e="T231" id="Seg_2085" s="T227">He caught a small fish.</ta>
            <ta e="T235" id="Seg_2086" s="T231">"What will you do with this fish?"</ta>
            <ta e="T241" id="Seg_2087" s="T235">"Well, I will sell [it] and buy bread to eat."</ta>
            <ta e="T245" id="Seg_2088" s="T241">But it told him:</ta>
            <ta e="T248" id="Seg_2089" s="T245">"You [will have] little bread.</ta>
            <ta e="T260" id="Seg_2090" s="T248">Go home, and I will bring you a big fish every day.</ta>
            <ta e="T265" id="Seg_2091" s="T260">And then don’t tell [it to] anyone!"</ta>
            <ta e="T273" id="Seg_2092" s="T265">It brought a big fish every day.</ta>
            <ta e="T277" id="Seg_2093" s="T273">His woman cut it with a knife.</ta>
            <ta e="T283" id="Seg_2094" s="T277">And cooked it, and [her husband] went to sell [it].</ta>
            <ta e="T288" id="Seg_2095" s="T283">Then they bought a house for the money.</ta>
            <ta e="T293" id="Seg_2096" s="T288">And [they] planted a big garden. [?]</ta>
            <ta e="T308" id="Seg_2097" s="T293">Then one [person] says: "If someone caught this bird, I would give [him] a lot of money."</ta>
            <ta e="T313" id="Seg_2098" s="T308">Then he says: "I (will?) catch [it]?"</ta>
            <ta e="T317" id="Seg_2099" s="T313">Then a lot of people came.</ta>
            <ta e="T320" id="Seg_2100" s="T317">The bird came flying.</ta>
            <ta e="T325" id="Seg_2101" s="T320">He set [food] for it to eat.</ta>
            <ta e="T328" id="Seg_2102" s="T325">"Bird, sit, eat!"</ta>
            <ta e="T333" id="Seg_2103" s="T328">Then the bird sat down to eat.</ta>
            <ta e="T342" id="Seg_2104" s="T333">Then the bird, all these people grasped it.</ta>
            <ta e="T347" id="Seg_2105" s="T342">But the man caught [it] by its feet.</ta>
            <ta e="T354" id="Seg_2106" s="T347">Then all the people pulled the man by his feet.</ta>
            <ta e="T366" id="Seg_2107" s="T354">Then the man's hands got tired, he fell down and all the people died.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T212" id="Seg_2108" s="T208">Es lebte ein Mann und eine Frau.</ta>
            <ta e="T216" id="Seg_2109" s="T212">Er fing Fische und verkaufte [sie].</ta>
            <ta e="T219" id="Seg_2110" s="T216">Also lebten sie, sie lebten.</ta>
            <ta e="T222" id="Seg_2111" s="T219">Sie hatten nichts.</ta>
            <ta e="T227" id="Seg_2112" s="T222">Dann kam ein Vogel geflogen.</ta>
            <ta e="T231" id="Seg_2113" s="T227">Er fing einen kleinen Fisch.</ta>
            <ta e="T235" id="Seg_2114" s="T231">“Was willst du mit diesem Fisch tun?"</ta>
            <ta e="T241" id="Seg_2115" s="T235">“Also, ich werde [ihn] verkaufen und kaufe Brot zum Essen.“</ta>
            <ta e="T245" id="Seg_2116" s="T241">Aber er sagte ihm:</ta>
            <ta e="T248" id="Seg_2117" s="T245">“Du [wirst] wenig Brot [haben].</ta>
            <ta e="T260" id="Seg_2118" s="T248">Gehe nach Hause, und ich werde dir jeden Tag einen großen Fisch bringen.</ta>
            <ta e="T265" id="Seg_2119" s="T260">Und erzähle [es] dann niemanden!“</ta>
            <ta e="T273" id="Seg_2120" s="T265">Er brachte jeden Tag einen großen Fisch.</ta>
            <ta e="T277" id="Seg_2121" s="T273">Seine Frau zerschnitt ihn mit einem Messer.</ta>
            <ta e="T283" id="Seg_2122" s="T277">Und kochte ihn, und [ihr Mann] ging [ihn] verkaufen.</ta>
            <ta e="T288" id="Seg_2123" s="T283">Dann kauften sie für das Geld ein Haus.</ta>
            <ta e="T293" id="Seg_2124" s="T288">Und [sie] pflanzten einen großen Garten an. [?]</ta>
            <ta e="T308" id="Seg_2125" s="T293">Dann sagt eine [Person]: „Wenn jemand diesen Vogel fangen würde, würde ich [ihm] viel Geld geben.“</ta>
            <ta e="T313" id="Seg_2126" s="T308">Dann sagt er: “Ich (werde?) [ihn] fangen?“</ta>
            <ta e="T317" id="Seg_2127" s="T313">Dann kamen viele Leute.</ta>
            <ta e="T320" id="Seg_2128" s="T317">Der Vogel kam geflogen.</ta>
            <ta e="T325" id="Seg_2129" s="T320">Er setzte [Essen] für ihn zu essen.</ta>
            <ta e="T328" id="Seg_2130" s="T325">“Vogel, setze dich, iss!”</ta>
            <ta e="T333" id="Seg_2131" s="T328">Dann setzt der Vogel sich zum Essen hin.</ta>
            <ta e="T342" id="Seg_2132" s="T333">Dann den Vogel, all diese Leute ergriffen ihn.</ta>
            <ta e="T347" id="Seg_2133" s="T342">Aber der Mann fing [ihn] an die Füße.</ta>
            <ta e="T354" id="Seg_2134" s="T347">Dann zogen alle Leute den Mann an die Füße.</ta>
            <ta e="T366" id="Seg_2135" s="T354">Dann wurden die Hände des Mannes müde, er fiel hinab und die ganzen Leute starben.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T248" id="Seg_2136" s="T245">[GVY:] Maybe "there is too little bread for you" (Russian "мало тебе хлеба")?</ta>
            <ta e="T260" id="Seg_2137" s="T248">[KlT:] maːʔnʼi instead of the POSS.2SG form, consider DU forms; каждый - кажен as a short form of кажный.</ta>
            <ta e="T265" id="Seg_2138" s="T260">[VGY:] pronounced šindinidə (ə &gt; i after i)? || [KlT:] šində + nə + idə?</ta>
            <ta e="T283" id="Seg_2139" s="T277">[KlT:] pür ’to cook’.</ta>
            <ta e="T293" id="Seg_2140" s="T288">[KlT:] сад garden? Or related to Khakas sadəγ- ”trade”? [AAV]: Rus. calque "seat a garden" = "plant a garden"?</ta>
            <ta e="T313" id="Seg_2141" s="T308">[KlT:] PST instead of intended PRS/FUT? Or forgets the bɨ? More likely in the sense of "I tamed it".</ta>
            <ta e="T354" id="Seg_2142" s="T347">[KlT:] contamination: nʼeʔbdə- ’pull’ + nʼuʔdə ’up’.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
