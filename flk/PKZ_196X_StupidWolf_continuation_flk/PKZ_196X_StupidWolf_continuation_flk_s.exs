<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID985CDBA3-40F3-8A7E-A975-4201599B11AF">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_StupidWolf_continuation_flk.wav" />
         <referenced-file url="PKZ_196X_StupidWolf_continuation_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_StupidWolf_continuation_flk\PKZ_196X_StupidWolf_continuation_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">126</ud-information>
            <ud-information attribute-name="# HIAT:w">86</ud-information>
            <ud-information attribute-name="# e">86</ud-information>
            <ud-information attribute-name="# HIAT:u">20</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T307" time="0.01" type="appl" />
         <tli id="T308" time="0.752" type="appl" />
         <tli id="T309" time="1.493" type="appl" />
         <tli id="T310" time="2.234" type="appl" />
         <tli id="T311" time="2.976" type="appl" />
         <tli id="T312" time="3.609" type="appl" />
         <tli id="T313" time="4.091" type="appl" />
         <tli id="T314" time="4.573" type="appl" />
         <tli id="T315" time="5.056" type="appl" />
         <tli id="T316" time="5.538" type="appl" />
         <tli id="T317" time="6.02" type="appl" />
         <tli id="T318" time="6.594" type="appl" />
         <tli id="T319" time="7.168" type="appl" />
         <tli id="T320" time="7.742" type="appl" />
         <tli id="T321" time="8.492" type="appl" />
         <tli id="T322" time="9.243" type="appl" />
         <tli id="T323" time="9.994" type="appl" />
         <tli id="T324" time="11.305474582319974" />
         <tli id="T325" time="11.69" type="appl" />
         <tli id="T326" time="12.3" type="appl" />
         <tli id="T327" time="12.911" type="appl" />
         <tli id="T328" time="13.521" type="appl" />
         <tli id="T329" time="14.132" type="appl" />
         <tli id="T330" time="14.856" type="appl" />
         <tli id="T331" time="15.58" type="appl" />
         <tli id="T332" time="16.303" type="appl" />
         <tli id="T333" time="17.027" type="appl" />
         <tli id="T334" time="17.811455238183356" />
         <tli id="T335" time="18.284" type="appl" />
         <tli id="T336" time="18.799" type="appl" />
         <tli id="T337" time="19.313" type="appl" />
         <tli id="T338" time="19.827" type="appl" />
         <tli id="T339" time="20.342" type="appl" />
         <tli id="T340" time="20.856" type="appl" />
         <tli id="T341" time="21.37" type="appl" />
         <tli id="T342" time="21.884" type="appl" />
         <tli id="T343" time="22.399" type="appl" />
         <tli id="T344" time="22.913" type="appl" />
         <tli id="T345" time="23.673" type="appl" />
         <tli id="T346" time="24.434" type="appl" />
         <tli id="T347" time="25.194" type="appl" />
         <tli id="T348" time="26.58386358154013" />
         <tli id="T349" time="27.085" type="appl" />
         <tli id="T350" time="27.632" type="appl" />
         <tli id="T351" time="28.178" type="appl" />
         <tli id="T352" time="28.724" type="appl" />
         <tli id="T353" time="29.439" type="appl" />
         <tli id="T354" time="30.043" type="appl" />
         <tli id="T355" time="30.647" type="appl" />
         <tli id="T356" time="31.25" type="appl" />
         <tli id="T357" time="31.854" type="appl" />
         <tli id="T358" time="32.458" type="appl" />
         <tli id="T359" time="33.062" type="appl" />
         <tli id="T360" time="33.808" type="appl" />
         <tli id="T361" time="34.554" type="appl" />
         <tli id="T362" time="35.301" type="appl" />
         <tli id="T363" time="36.047" type="appl" />
         <tli id="T364" time="37.426" type="appl" />
         <tli id="T365" time="39.98245079289812" />
         <tli id="T366" time="40.849" type="appl" />
         <tli id="T367" time="41.632" type="appl" />
         <tli id="T368" time="43.20877726568283" />
         <tli id="T369" time="44.113" type="appl" />
         <tli id="T370" time="45.127" type="appl" />
         <tli id="T371" time="46.141" type="appl" />
         <tli id="T372" time="46.701" type="appl" />
         <tli id="T373" time="47.118" type="appl" />
         <tli id="T374" time="47.536" type="appl" />
         <tli id="T375" time="47.953" type="appl" />
         <tli id="T376" time="48.371" type="appl" />
         <tli id="T377" time="49.102" type="appl" />
         <tli id="T378" time="49.832" type="appl" />
         <tli id="T379" time="50.79464405499894" />
         <tli id="T380" time="51.544" type="appl" />
         <tli id="T381" time="52.461" type="appl" />
         <tli id="T382" time="53.412" type="appl" />
         <tli id="T383" time="54.363" type="appl" />
         <tli id="T384" time="55.314" type="appl" />
         <tli id="T385" time="56.264" type="appl" />
         <tli id="T386" time="57.215" type="appl" />
         <tli id="T387" time="58.166" type="appl" />
         <tli id="T388" time="59.117" type="appl" />
         <tli id="T389" time="60.32697227004468" />
         <tli id="T390" time="60.645" type="appl" />
         <tli id="T391" time="61.221" type="appl" />
         <tli id="T392" time="61.798" type="appl" />
         <tli id="T393" time="63.04" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T393" id="Seg_0" n="sc" s="T307">
               <ts e="T311" id="Seg_2" n="HIAT:u" s="T307">
                  <ts e="T308" id="Seg_4" n="HIAT:w" s="T307">Dĭgəttə</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_7" n="HIAT:w" s="T308">dĭzeŋ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_10" n="HIAT:w" s="T309">šobiʔi</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_13" n="HIAT:w" s="T310">tʼerməndə</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T317" id="Seg_17" n="HIAT:u" s="T311">
                  <ts e="T312" id="Seg_19" n="HIAT:w" s="T311">Dĭ</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_22" n="HIAT:w" s="T312">volk</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_25" n="HIAT:w" s="T313">măndə:</ts>
                  <nts id="Seg_26" n="HIAT:ip">"</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_29" n="HIAT:w" s="T314">Tăn</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_32" n="HIAT:w" s="T315">dĭn</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_35" n="HIAT:w" s="T316">amnoʔ</ts>
                  <nts id="Seg_36" n="HIAT:ip">"</nts>
                  <nts id="Seg_37" n="HIAT:ip">.</nts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T320" id="Seg_40" n="HIAT:u" s="T317">
                  <ts e="T318" id="Seg_42" n="HIAT:w" s="T317">Dĭ</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_45" n="HIAT:w" s="T318">dibər</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_48" n="HIAT:w" s="T319">amnobi</ts>
                  <nts id="Seg_49" n="HIAT:ip">.</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T324" id="Seg_52" n="HIAT:u" s="T320">
                  <ts e="T321" id="Seg_54" n="HIAT:w" s="T320">A</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_57" n="HIAT:w" s="T321">dĭ</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_60" n="HIAT:w" s="T322">bü</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_63" n="HIAT:w" s="T323">öʔlubi</ts>
                  <nts id="Seg_64" n="HIAT:ip">.</nts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T329" id="Seg_67" n="HIAT:u" s="T324">
                  <ts e="T325" id="Seg_69" n="HIAT:w" s="T324">Dĭ</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_72" n="HIAT:w" s="T325">bü</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_75" n="HIAT:w" s="T326">saʔməluʔbi</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_78" n="HIAT:w" s="T327">dĭ</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_81" n="HIAT:w" s="T328">volkdə</ts>
                  <nts id="Seg_82" n="HIAT:ip">.</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T334" id="Seg_85" n="HIAT:u" s="T329">
                  <ts e="T330" id="Seg_87" n="HIAT:w" s="T329">Dĭ</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_90" n="HIAT:w" s="T330">bar</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_93" n="HIAT:w" s="T331">kalla</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_96" n="HIAT:w" s="T332">dʼürbi</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_99" n="HIAT:w" s="T333">büziʔ</ts>
                  <nts id="Seg_100" n="HIAT:ip">.</nts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T344" id="Seg_103" n="HIAT:u" s="T334">
                  <ts e="T335" id="Seg_105" n="HIAT:w" s="T334">Dĭ</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_107" n="HIAT:ip">(</nts>
                  <ts e="T336" id="Seg_109" n="HIAT:w" s="T335">sʼu-</ts>
                  <nts id="Seg_110" n="HIAT:ip">)</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_113" n="HIAT:w" s="T336">bostə</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_116" n="HIAT:w" s="T337">üdʼüge</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_119" n="HIAT:w" s="T338">šoškaʔi</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_122" n="HIAT:w" s="T339">iluʔpi</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_125" n="HIAT:w" s="T340">i</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_128" n="HIAT:w" s="T341">maːndə</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_131" n="HIAT:w" s="T342">kalla</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_134" n="HIAT:w" s="T343">dʼürbi</ts>
                  <nts id="Seg_135" n="HIAT:ip">.</nts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T348" id="Seg_138" n="HIAT:u" s="T344">
                  <ts e="T345" id="Seg_140" n="HIAT:w" s="T344">Ambi</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_143" n="HIAT:w" s="T345">da</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_146" n="HIAT:w" s="T346">iʔbəbi</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_149" n="HIAT:w" s="T347">kunolzittə</ts>
                  <nts id="Seg_150" n="HIAT:ip">.</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T352" id="Seg_153" n="HIAT:u" s="T348">
                  <ts e="T349" id="Seg_155" n="HIAT:w" s="T348">A</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_158" n="HIAT:w" s="T349">volk</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_160" n="HIAT:ip">(</nts>
                  <ts e="T351" id="Seg_162" n="HIAT:w" s="T350">su-</ts>
                  <nts id="Seg_163" n="HIAT:ip">)</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_166" n="HIAT:w" s="T351">supsobi</ts>
                  <nts id="Seg_167" n="HIAT:ip">.</nts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T359" id="Seg_170" n="HIAT:u" s="T352">
                  <ts e="T353" id="Seg_172" n="HIAT:w" s="T352">No</ts>
                  <nts id="Seg_173" n="HIAT:ip">,</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_176" n="HIAT:w" s="T353">tuj</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_179" n="HIAT:w" s="T354">dĭ</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_181" n="HIAT:ip">(</nts>
                  <ts e="T356" id="Seg_183" n="HIAT:w" s="T355">šol-</ts>
                  <nts id="Seg_184" n="HIAT:ip">)</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_187" n="HIAT:w" s="T356">šoška</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_189" n="HIAT:ip">(</nts>
                  <ts e="T358" id="Seg_191" n="HIAT:w" s="T357">mănanə</ts>
                  <nts id="Seg_192" n="HIAT:ip">)</nts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_195" n="HIAT:w" s="T358">igəluʔpi</ts>
                  <nts id="Seg_196" n="HIAT:ip">.</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T363" id="Seg_199" n="HIAT:u" s="T359">
                  <ts e="T360" id="Seg_201" n="HIAT:w" s="T359">Dĭgəttə</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_203" n="HIAT:ip">(</nts>
                  <ts e="T361" id="Seg_205" n="HIAT:w" s="T360">kan-</ts>
                  <nts id="Seg_206" n="HIAT:ip">)</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_209" n="HIAT:w" s="T361">kambi</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_212" n="HIAT:w" s="T362">turazaŋgə</ts>
                  <nts id="Seg_213" n="HIAT:ip">.</nts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T365" id="Seg_216" n="HIAT:u" s="T363">
                  <ts e="T364" id="Seg_218" n="HIAT:w" s="T363">Dĭn</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_221" n="HIAT:w" s="T364">kubi</ts>
                  <nts id="Seg_222" n="HIAT:ip">.</nts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T368" id="Seg_225" n="HIAT:u" s="T365">
                  <nts id="Seg_226" n="HIAT:ip">(</nts>
                  <ts e="T366" id="Seg_228" n="HIAT:w" s="T365">Ku-</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_231" n="HIAT:w" s="T366">kunə-</ts>
                  <nts id="Seg_232" n="HIAT:ip">)</nts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_235" n="HIAT:w" s="T367">Kulaʔbə</ts>
                  <nts id="Seg_236" n="HIAT:ip">.</nts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T371" id="Seg_239" n="HIAT:u" s="T368">
                  <nts id="Seg_240" n="HIAT:ip">(</nts>
                  <ts e="T369" id="Seg_242" n="HIAT:w" s="T368">Kulə-</ts>
                  <nts id="Seg_243" n="HIAT:ip">)</nts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_246" n="HIAT:w" s="T369">Kubi</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_249" n="HIAT:w" s="T370">ine</ts>
                  <nts id="Seg_250" n="HIAT:ip">.</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T376" id="Seg_253" n="HIAT:u" s="T371">
                  <ts e="T372" id="Seg_255" n="HIAT:w" s="T371">Nu</ts>
                  <nts id="Seg_256" n="HIAT:ip">,</nts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_259" n="HIAT:w" s="T372">nüdʼin</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_262" n="HIAT:w" s="T373">šolam</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_265" n="HIAT:w" s="T374">da</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_268" n="HIAT:w" s="T375">amnam</ts>
                  <nts id="Seg_269" n="HIAT:ip">.</nts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T379" id="Seg_272" n="HIAT:u" s="T376">
                  <ts e="T377" id="Seg_274" n="HIAT:w" s="T376">Dĭgəttə</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_277" n="HIAT:w" s="T377">šobi</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_280" n="HIAT:w" s="T378">nüdʼin</ts>
                  <nts id="Seg_281" n="HIAT:ip">.</nts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T381" id="Seg_284" n="HIAT:u" s="T379">
                  <ts e="T380" id="Seg_286" n="HIAT:w" s="T379">Davaj</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_289" n="HIAT:w" s="T380">amzittə</ts>
                  <nts id="Seg_290" n="HIAT:ip">.</nts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T389" id="Seg_293" n="HIAT:u" s="T381">
                  <ts e="T382" id="Seg_295" n="HIAT:w" s="T381">A</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_298" n="HIAT:w" s="T382">onʼiʔ</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_301" n="HIAT:w" s="T383">kuza</ts>
                  <nts id="Seg_302" n="HIAT:ip">,</nts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_305" n="HIAT:w" s="T384">šide</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_308" n="HIAT:w" s="T385">dʼitluʔpi</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_311" n="HIAT:w" s="T386">dĭm</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_314" n="HIAT:w" s="T387">multukziʔ</ts>
                  <nts id="Seg_315" n="HIAT:ip">,</nts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_318" n="HIAT:w" s="T388">kutlaːmbi</ts>
                  <nts id="Seg_319" n="HIAT:ip">.</nts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T392" id="Seg_322" n="HIAT:u" s="T389">
                  <ts e="T390" id="Seg_324" n="HIAT:w" s="T389">I</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_327" n="HIAT:w" s="T390">dĭ</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_330" n="HIAT:w" s="T391">külaːmbi</ts>
                  <nts id="Seg_331" n="HIAT:ip">.</nts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T393" id="Seg_334" n="HIAT:u" s="T392">
                  <ts e="T393" id="Seg_336" n="HIAT:w" s="T392">Kabarləj</ts>
                  <nts id="Seg_337" n="HIAT:ip">.</nts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T393" id="Seg_339" n="sc" s="T307">
               <ts e="T308" id="Seg_341" n="e" s="T307">Dĭgəttə </ts>
               <ts e="T309" id="Seg_343" n="e" s="T308">dĭzeŋ </ts>
               <ts e="T310" id="Seg_345" n="e" s="T309">šobiʔi </ts>
               <ts e="T311" id="Seg_347" n="e" s="T310">tʼerməndə. </ts>
               <ts e="T312" id="Seg_349" n="e" s="T311">Dĭ </ts>
               <ts e="T313" id="Seg_351" n="e" s="T312">volk </ts>
               <ts e="T314" id="Seg_353" n="e" s="T313">măndə:" </ts>
               <ts e="T315" id="Seg_355" n="e" s="T314">Tăn </ts>
               <ts e="T316" id="Seg_357" n="e" s="T315">dĭn </ts>
               <ts e="T317" id="Seg_359" n="e" s="T316">amnoʔ". </ts>
               <ts e="T318" id="Seg_361" n="e" s="T317">Dĭ </ts>
               <ts e="T319" id="Seg_363" n="e" s="T318">dibər </ts>
               <ts e="T320" id="Seg_365" n="e" s="T319">amnobi. </ts>
               <ts e="T321" id="Seg_367" n="e" s="T320">A </ts>
               <ts e="T322" id="Seg_369" n="e" s="T321">dĭ </ts>
               <ts e="T323" id="Seg_371" n="e" s="T322">bü </ts>
               <ts e="T324" id="Seg_373" n="e" s="T323">öʔlubi. </ts>
               <ts e="T325" id="Seg_375" n="e" s="T324">Dĭ </ts>
               <ts e="T326" id="Seg_377" n="e" s="T325">bü </ts>
               <ts e="T327" id="Seg_379" n="e" s="T326">saʔməluʔbi </ts>
               <ts e="T328" id="Seg_381" n="e" s="T327">dĭ </ts>
               <ts e="T329" id="Seg_383" n="e" s="T328">volkdə. </ts>
               <ts e="T330" id="Seg_385" n="e" s="T329">Dĭ </ts>
               <ts e="T331" id="Seg_387" n="e" s="T330">bar </ts>
               <ts e="T332" id="Seg_389" n="e" s="T331">kalla </ts>
               <ts e="T333" id="Seg_391" n="e" s="T332">dʼürbi </ts>
               <ts e="T334" id="Seg_393" n="e" s="T333">büziʔ. </ts>
               <ts e="T335" id="Seg_395" n="e" s="T334">Dĭ </ts>
               <ts e="T336" id="Seg_397" n="e" s="T335">(sʼu-) </ts>
               <ts e="T337" id="Seg_399" n="e" s="T336">bostə </ts>
               <ts e="T338" id="Seg_401" n="e" s="T337">üdʼüge </ts>
               <ts e="T339" id="Seg_403" n="e" s="T338">šoškaʔi </ts>
               <ts e="T340" id="Seg_405" n="e" s="T339">iluʔpi </ts>
               <ts e="T341" id="Seg_407" n="e" s="T340">i </ts>
               <ts e="T342" id="Seg_409" n="e" s="T341">maːndə </ts>
               <ts e="T343" id="Seg_411" n="e" s="T342">kalla </ts>
               <ts e="T344" id="Seg_413" n="e" s="T343">dʼürbi. </ts>
               <ts e="T345" id="Seg_415" n="e" s="T344">Ambi </ts>
               <ts e="T346" id="Seg_417" n="e" s="T345">da </ts>
               <ts e="T347" id="Seg_419" n="e" s="T346">iʔbəbi </ts>
               <ts e="T348" id="Seg_421" n="e" s="T347">kunolzittə. </ts>
               <ts e="T349" id="Seg_423" n="e" s="T348">A </ts>
               <ts e="T350" id="Seg_425" n="e" s="T349">volk </ts>
               <ts e="T351" id="Seg_427" n="e" s="T350">(su-) </ts>
               <ts e="T352" id="Seg_429" n="e" s="T351">supsobi. </ts>
               <ts e="T353" id="Seg_431" n="e" s="T352">No, </ts>
               <ts e="T354" id="Seg_433" n="e" s="T353">tuj </ts>
               <ts e="T355" id="Seg_435" n="e" s="T354">dĭ </ts>
               <ts e="T356" id="Seg_437" n="e" s="T355">(šol-) </ts>
               <ts e="T357" id="Seg_439" n="e" s="T356">šoška </ts>
               <ts e="T358" id="Seg_441" n="e" s="T357">(mănanə) </ts>
               <ts e="T359" id="Seg_443" n="e" s="T358">igəluʔpi. </ts>
               <ts e="T360" id="Seg_445" n="e" s="T359">Dĭgəttə </ts>
               <ts e="T361" id="Seg_447" n="e" s="T360">(kan-) </ts>
               <ts e="T362" id="Seg_449" n="e" s="T361">kambi </ts>
               <ts e="T363" id="Seg_451" n="e" s="T362">turazaŋgə. </ts>
               <ts e="T364" id="Seg_453" n="e" s="T363">Dĭn </ts>
               <ts e="T365" id="Seg_455" n="e" s="T364">kubi. </ts>
               <ts e="T366" id="Seg_457" n="e" s="T365">(Ku- </ts>
               <ts e="T367" id="Seg_459" n="e" s="T366">kunə-) </ts>
               <ts e="T368" id="Seg_461" n="e" s="T367">Kulaʔbə. </ts>
               <ts e="T369" id="Seg_463" n="e" s="T368">(Kulə-) </ts>
               <ts e="T370" id="Seg_465" n="e" s="T369">Kubi </ts>
               <ts e="T371" id="Seg_467" n="e" s="T370">ine. </ts>
               <ts e="T372" id="Seg_469" n="e" s="T371">Nu, </ts>
               <ts e="T373" id="Seg_471" n="e" s="T372">nüdʼin </ts>
               <ts e="T374" id="Seg_473" n="e" s="T373">šolam </ts>
               <ts e="T375" id="Seg_475" n="e" s="T374">da </ts>
               <ts e="T376" id="Seg_477" n="e" s="T375">amnam. </ts>
               <ts e="T377" id="Seg_479" n="e" s="T376">Dĭgəttə </ts>
               <ts e="T378" id="Seg_481" n="e" s="T377">šobi </ts>
               <ts e="T379" id="Seg_483" n="e" s="T378">nüdʼin. </ts>
               <ts e="T380" id="Seg_485" n="e" s="T379">Davaj </ts>
               <ts e="T381" id="Seg_487" n="e" s="T380">amzittə. </ts>
               <ts e="T382" id="Seg_489" n="e" s="T381">A </ts>
               <ts e="T383" id="Seg_491" n="e" s="T382">onʼiʔ </ts>
               <ts e="T384" id="Seg_493" n="e" s="T383">kuza, </ts>
               <ts e="T385" id="Seg_495" n="e" s="T384">šide </ts>
               <ts e="T386" id="Seg_497" n="e" s="T385">dʼitluʔpi </ts>
               <ts e="T387" id="Seg_499" n="e" s="T386">dĭm </ts>
               <ts e="T388" id="Seg_501" n="e" s="T387">multukziʔ, </ts>
               <ts e="T389" id="Seg_503" n="e" s="T388">kutlaːmbi. </ts>
               <ts e="T390" id="Seg_505" n="e" s="T389">I </ts>
               <ts e="T391" id="Seg_507" n="e" s="T390">dĭ </ts>
               <ts e="T392" id="Seg_509" n="e" s="T391">külaːmbi. </ts>
               <ts e="T393" id="Seg_511" n="e" s="T392">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T311" id="Seg_512" s="T307">PKZ_196X_StupidWolf_continuation_flk.001 (001.001)</ta>
            <ta e="T317" id="Seg_513" s="T311">PKZ_196X_StupidWolf_continuation_flk.002 (001.002)</ta>
            <ta e="T320" id="Seg_514" s="T317">PKZ_196X_StupidWolf_continuation_flk.003 (001.003)</ta>
            <ta e="T324" id="Seg_515" s="T320">PKZ_196X_StupidWolf_continuation_flk.004 (001.004)</ta>
            <ta e="T329" id="Seg_516" s="T324">PKZ_196X_StupidWolf_continuation_flk.005 (001.005)</ta>
            <ta e="T334" id="Seg_517" s="T329">PKZ_196X_StupidWolf_continuation_flk.006 (001.006)</ta>
            <ta e="T344" id="Seg_518" s="T334">PKZ_196X_StupidWolf_continuation_flk.007 (001.007)</ta>
            <ta e="T348" id="Seg_519" s="T344">PKZ_196X_StupidWolf_continuation_flk.008 (001.008)</ta>
            <ta e="T352" id="Seg_520" s="T348">PKZ_196X_StupidWolf_continuation_flk.009 (001.009)</ta>
            <ta e="T359" id="Seg_521" s="T352">PKZ_196X_StupidWolf_continuation_flk.010 (001.010)</ta>
            <ta e="T363" id="Seg_522" s="T359">PKZ_196X_StupidWolf_continuation_flk.011 (001.011)</ta>
            <ta e="T365" id="Seg_523" s="T363">PKZ_196X_StupidWolf_continuation_flk.012 (001.012)</ta>
            <ta e="T368" id="Seg_524" s="T365">PKZ_196X_StupidWolf_continuation_flk.013 (001.013)</ta>
            <ta e="T371" id="Seg_525" s="T368">PKZ_196X_StupidWolf_continuation_flk.014 (001.014)</ta>
            <ta e="T376" id="Seg_526" s="T371">PKZ_196X_StupidWolf_continuation_flk.015 (001.015)</ta>
            <ta e="T379" id="Seg_527" s="T376">PKZ_196X_StupidWolf_continuation_flk.016 (001.016)</ta>
            <ta e="T381" id="Seg_528" s="T379">PKZ_196X_StupidWolf_continuation_flk.017 (001.017)</ta>
            <ta e="T389" id="Seg_529" s="T381">PKZ_196X_StupidWolf_continuation_flk.018 (001.018)</ta>
            <ta e="T392" id="Seg_530" s="T389">PKZ_196X_StupidWolf_continuation_flk.019 (001.019)</ta>
            <ta e="T393" id="Seg_531" s="T392">PKZ_196X_StupidWolf_continuation_flk.020 (001.020)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T311" id="Seg_532" s="T307">Dĭgəttə dĭzeŋ šobiʔi tʼerməndə. </ta>
            <ta e="T317" id="Seg_533" s="T311">Dĭ volk măndə:" Tăn dĭn amnoʔ". </ta>
            <ta e="T320" id="Seg_534" s="T317">Dĭ dibər amnobi. </ta>
            <ta e="T324" id="Seg_535" s="T320">A dĭ bü öʔlubi. </ta>
            <ta e="T329" id="Seg_536" s="T324">Dĭ bü saʔməluʔbi dĭ volkdə. </ta>
            <ta e="T334" id="Seg_537" s="T329">Dĭ bar kalla dʼürbi büziʔ. </ta>
            <ta e="T344" id="Seg_538" s="T334">Dĭ (sʼu-) bostə üdʼüge šoškaʔi iluʔpi i maːndə kalla dʼürbi. </ta>
            <ta e="T348" id="Seg_539" s="T344">Ambi da iʔbəbi kunolzittə. </ta>
            <ta e="T352" id="Seg_540" s="T348">A volk (su-) supsobi. </ta>
            <ta e="T359" id="Seg_541" s="T352">No, tuj dĭ (šol-) šoška (mănanə) igəluʔpi. </ta>
            <ta e="T363" id="Seg_542" s="T359">Dĭgəttə (kan-) kambi turazaŋgə. </ta>
            <ta e="T365" id="Seg_543" s="T363">Dĭn kubi. </ta>
            <ta e="T368" id="Seg_544" s="T365">(Ku- kunə-) Kulaʔbə. </ta>
            <ta e="T371" id="Seg_545" s="T368">(Kulə-) Kubi ine. </ta>
            <ta e="T376" id="Seg_546" s="T371">Nu, nüdʼin šolam da amnam. </ta>
            <ta e="T379" id="Seg_547" s="T376">Dĭgəttə šobi nüdʼin. </ta>
            <ta e="T381" id="Seg_548" s="T379">Davaj amzittə. </ta>
            <ta e="T389" id="Seg_549" s="T381">A onʼiʔ kuza, šide dʼitluʔpi dĭm multukziʔ, kutlaːmbi. </ta>
            <ta e="T392" id="Seg_550" s="T389">I dĭ külaːmbi. </ta>
            <ta e="T393" id="Seg_551" s="T392">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T308" id="Seg_552" s="T307">dĭgəttə</ta>
            <ta e="T309" id="Seg_553" s="T308">dĭ-zeŋ</ta>
            <ta e="T310" id="Seg_554" s="T309">šo-bi-ʔi</ta>
            <ta e="T311" id="Seg_555" s="T310">tʼermən-də</ta>
            <ta e="T312" id="Seg_556" s="T311">dĭ</ta>
            <ta e="T313" id="Seg_557" s="T312">volk</ta>
            <ta e="T314" id="Seg_558" s="T313">măn-də</ta>
            <ta e="T315" id="Seg_559" s="T314">tăn</ta>
            <ta e="T316" id="Seg_560" s="T315">dĭn</ta>
            <ta e="T317" id="Seg_561" s="T316">amno-ʔ</ta>
            <ta e="T318" id="Seg_562" s="T317">dĭ</ta>
            <ta e="T319" id="Seg_563" s="T318">dibər</ta>
            <ta e="T320" id="Seg_564" s="T319">amno-bi</ta>
            <ta e="T321" id="Seg_565" s="T320">a</ta>
            <ta e="T322" id="Seg_566" s="T321">dĭ</ta>
            <ta e="T323" id="Seg_567" s="T322">bü</ta>
            <ta e="T324" id="Seg_568" s="T323">öʔlu-bi</ta>
            <ta e="T325" id="Seg_569" s="T324">dĭ</ta>
            <ta e="T326" id="Seg_570" s="T325">bü</ta>
            <ta e="T327" id="Seg_571" s="T326">saʔmə-luʔ-bi</ta>
            <ta e="T328" id="Seg_572" s="T327">dĭ</ta>
            <ta e="T329" id="Seg_573" s="T328">volk-də</ta>
            <ta e="T330" id="Seg_574" s="T329">dĭ</ta>
            <ta e="T331" id="Seg_575" s="T330">bar</ta>
            <ta e="T332" id="Seg_576" s="T331">kal-la</ta>
            <ta e="T333" id="Seg_577" s="T332">dʼür-bi</ta>
            <ta e="T334" id="Seg_578" s="T333">bü-ziʔ</ta>
            <ta e="T335" id="Seg_579" s="T334">dĭ</ta>
            <ta e="T337" id="Seg_580" s="T336">bos-tə</ta>
            <ta e="T338" id="Seg_581" s="T337">üdʼüge</ta>
            <ta e="T339" id="Seg_582" s="T338">šoška-ʔi</ta>
            <ta e="T340" id="Seg_583" s="T339">i-luʔ-pi</ta>
            <ta e="T341" id="Seg_584" s="T340">i</ta>
            <ta e="T342" id="Seg_585" s="T341">ma-ndə</ta>
            <ta e="T343" id="Seg_586" s="T342">kal-la</ta>
            <ta e="T344" id="Seg_587" s="T343">dʼür-bi</ta>
            <ta e="T345" id="Seg_588" s="T344">am-bi</ta>
            <ta e="T346" id="Seg_589" s="T345">da</ta>
            <ta e="T347" id="Seg_590" s="T346">iʔbə-bi</ta>
            <ta e="T348" id="Seg_591" s="T347">kunol-zittə</ta>
            <ta e="T349" id="Seg_592" s="T348">a</ta>
            <ta e="T350" id="Seg_593" s="T349">volk</ta>
            <ta e="T352" id="Seg_594" s="T351">supso-bi</ta>
            <ta e="T353" id="Seg_595" s="T352">no</ta>
            <ta e="T354" id="Seg_596" s="T353">tuj</ta>
            <ta e="T355" id="Seg_597" s="T354">dĭ</ta>
            <ta e="T357" id="Seg_598" s="T356">šoška</ta>
            <ta e="T358" id="Seg_599" s="T357">măna-nə</ta>
            <ta e="T359" id="Seg_600" s="T358">igə-luʔ-pi</ta>
            <ta e="T360" id="Seg_601" s="T359">dĭgəttə</ta>
            <ta e="T362" id="Seg_602" s="T361">kam-bi</ta>
            <ta e="T363" id="Seg_603" s="T362">tura-zaŋ-gə</ta>
            <ta e="T364" id="Seg_604" s="T363">dĭn</ta>
            <ta e="T365" id="Seg_605" s="T364">ku-bi</ta>
            <ta e="T368" id="Seg_606" s="T367">ku-laʔbə</ta>
            <ta e="T370" id="Seg_607" s="T369">ku-bi</ta>
            <ta e="T371" id="Seg_608" s="T370">ine</ta>
            <ta e="T372" id="Seg_609" s="T371">nu</ta>
            <ta e="T373" id="Seg_610" s="T372">nüdʼi-n</ta>
            <ta e="T374" id="Seg_611" s="T373">šo-la-m</ta>
            <ta e="T375" id="Seg_612" s="T374">da</ta>
            <ta e="T376" id="Seg_613" s="T375">am-na-m</ta>
            <ta e="T377" id="Seg_614" s="T376">dĭgəttə</ta>
            <ta e="T378" id="Seg_615" s="T377">šo-bi</ta>
            <ta e="T379" id="Seg_616" s="T378">nüdʼi-n</ta>
            <ta e="T380" id="Seg_617" s="T379">davaj</ta>
            <ta e="T381" id="Seg_618" s="T380">am-zittə</ta>
            <ta e="T382" id="Seg_619" s="T381">a</ta>
            <ta e="T383" id="Seg_620" s="T382">onʼiʔ</ta>
            <ta e="T384" id="Seg_621" s="T383">kuza</ta>
            <ta e="T385" id="Seg_622" s="T384">šide</ta>
            <ta e="T386" id="Seg_623" s="T385">dʼit-luʔ-pi</ta>
            <ta e="T387" id="Seg_624" s="T386">dĭ-m</ta>
            <ta e="T388" id="Seg_625" s="T387">multuk-ziʔ</ta>
            <ta e="T389" id="Seg_626" s="T388">kut-laːm-bi</ta>
            <ta e="T390" id="Seg_627" s="T389">i</ta>
            <ta e="T391" id="Seg_628" s="T390">dĭ</ta>
            <ta e="T392" id="Seg_629" s="T391">kü-laːm-bi</ta>
            <ta e="T393" id="Seg_630" s="T392">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T308" id="Seg_631" s="T307">dĭgəttə</ta>
            <ta e="T309" id="Seg_632" s="T308">dĭ-zAŋ</ta>
            <ta e="T310" id="Seg_633" s="T309">šo-bi-jəʔ</ta>
            <ta e="T311" id="Seg_634" s="T310">tʼermən-də</ta>
            <ta e="T312" id="Seg_635" s="T311">dĭ</ta>
            <ta e="T313" id="Seg_636" s="T312">volk</ta>
            <ta e="T314" id="Seg_637" s="T313">măn-ntə</ta>
            <ta e="T315" id="Seg_638" s="T314">tăn</ta>
            <ta e="T316" id="Seg_639" s="T315">dĭn</ta>
            <ta e="T317" id="Seg_640" s="T316">amnə-ʔ</ta>
            <ta e="T318" id="Seg_641" s="T317">dĭ</ta>
            <ta e="T319" id="Seg_642" s="T318">dĭbər</ta>
            <ta e="T320" id="Seg_643" s="T319">amnə-bi</ta>
            <ta e="T321" id="Seg_644" s="T320">a</ta>
            <ta e="T322" id="Seg_645" s="T321">dĭ</ta>
            <ta e="T323" id="Seg_646" s="T322">bü</ta>
            <ta e="T324" id="Seg_647" s="T323">öʔlu-bi</ta>
            <ta e="T325" id="Seg_648" s="T324">dĭ</ta>
            <ta e="T326" id="Seg_649" s="T325">bü</ta>
            <ta e="T327" id="Seg_650" s="T326">saʔmə-luʔbdə-bi</ta>
            <ta e="T328" id="Seg_651" s="T327">dĭ</ta>
            <ta e="T329" id="Seg_652" s="T328">volk-Tə</ta>
            <ta e="T330" id="Seg_653" s="T329">dĭ</ta>
            <ta e="T331" id="Seg_654" s="T330">bar</ta>
            <ta e="T332" id="Seg_655" s="T331">kan-lAʔ</ta>
            <ta e="T333" id="Seg_656" s="T332">tʼür-bi</ta>
            <ta e="T334" id="Seg_657" s="T333">bü-ziʔ</ta>
            <ta e="T335" id="Seg_658" s="T334">dĭ</ta>
            <ta e="T337" id="Seg_659" s="T336">bos-də</ta>
            <ta e="T338" id="Seg_660" s="T337">üdʼüge</ta>
            <ta e="T339" id="Seg_661" s="T338">šoška-jəʔ</ta>
            <ta e="T340" id="Seg_662" s="T339">i-luʔbdə-bi</ta>
            <ta e="T341" id="Seg_663" s="T340">i</ta>
            <ta e="T342" id="Seg_664" s="T341">maʔ-gəndə</ta>
            <ta e="T343" id="Seg_665" s="T342">kan-lAʔ</ta>
            <ta e="T344" id="Seg_666" s="T343">tʼür-bi</ta>
            <ta e="T345" id="Seg_667" s="T344">am-bi</ta>
            <ta e="T346" id="Seg_668" s="T345">da</ta>
            <ta e="T347" id="Seg_669" s="T346">iʔbö-bi</ta>
            <ta e="T348" id="Seg_670" s="T347">kunol-zittə</ta>
            <ta e="T349" id="Seg_671" s="T348">a</ta>
            <ta e="T350" id="Seg_672" s="T349">volk</ta>
            <ta e="T352" id="Seg_673" s="T351">supso-bi</ta>
            <ta e="T353" id="Seg_674" s="T352">no</ta>
            <ta e="T354" id="Seg_675" s="T353">tüj</ta>
            <ta e="T355" id="Seg_676" s="T354">dĭ</ta>
            <ta e="T357" id="Seg_677" s="T356">šoška</ta>
            <ta e="T358" id="Seg_678" s="T357">măna-Tə</ta>
            <ta e="T359" id="Seg_679" s="T358">igə-luʔbdə-bi</ta>
            <ta e="T360" id="Seg_680" s="T359">dĭgəttə</ta>
            <ta e="T362" id="Seg_681" s="T361">kan-bi</ta>
            <ta e="T363" id="Seg_682" s="T362">tura-zAŋ-gəʔ</ta>
            <ta e="T364" id="Seg_683" s="T363">dĭn</ta>
            <ta e="T365" id="Seg_684" s="T364">ku-bi</ta>
            <ta e="T368" id="Seg_685" s="T367">ku-laʔbə</ta>
            <ta e="T370" id="Seg_686" s="T369">ku-bi</ta>
            <ta e="T371" id="Seg_687" s="T370">ine</ta>
            <ta e="T372" id="Seg_688" s="T371">nu</ta>
            <ta e="T373" id="Seg_689" s="T372">nüdʼi-n</ta>
            <ta e="T374" id="Seg_690" s="T373">šo-lV-m</ta>
            <ta e="T375" id="Seg_691" s="T374">da</ta>
            <ta e="T376" id="Seg_692" s="T375">am-lV-m</ta>
            <ta e="T377" id="Seg_693" s="T376">dĭgəttə</ta>
            <ta e="T378" id="Seg_694" s="T377">šo-bi</ta>
            <ta e="T379" id="Seg_695" s="T378">nüdʼi-n</ta>
            <ta e="T380" id="Seg_696" s="T379">davaj</ta>
            <ta e="T381" id="Seg_697" s="T380">am-zittə</ta>
            <ta e="T382" id="Seg_698" s="T381">a</ta>
            <ta e="T383" id="Seg_699" s="T382">onʼiʔ</ta>
            <ta e="T384" id="Seg_700" s="T383">kuza</ta>
            <ta e="T385" id="Seg_701" s="T384">šide</ta>
            <ta e="T386" id="Seg_702" s="T385">tʼit-luʔbdə-bi</ta>
            <ta e="T387" id="Seg_703" s="T386">dĭ-m</ta>
            <ta e="T388" id="Seg_704" s="T387">multuk-ziʔ</ta>
            <ta e="T389" id="Seg_705" s="T388">kut-laːm-bi</ta>
            <ta e="T390" id="Seg_706" s="T389">i</ta>
            <ta e="T391" id="Seg_707" s="T390">dĭ</ta>
            <ta e="T392" id="Seg_708" s="T391">kü-laːm-bi</ta>
            <ta e="T393" id="Seg_709" s="T392">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T308" id="Seg_710" s="T307">then</ta>
            <ta e="T309" id="Seg_711" s="T308">this-PL</ta>
            <ta e="T310" id="Seg_712" s="T309">come-PST-3PL</ta>
            <ta e="T311" id="Seg_713" s="T310">mill-NOM/GEN/ACC.3SG</ta>
            <ta e="T312" id="Seg_714" s="T311">this.[NOM.SG]</ta>
            <ta e="T313" id="Seg_715" s="T312">wolf.[NOM.SG]</ta>
            <ta e="T314" id="Seg_716" s="T313">say-IPFVZ.[3SG]</ta>
            <ta e="T315" id="Seg_717" s="T314">you.NOM</ta>
            <ta e="T316" id="Seg_718" s="T315">there</ta>
            <ta e="T317" id="Seg_719" s="T316">sit-IMP.2SG</ta>
            <ta e="T318" id="Seg_720" s="T317">this.[NOM.SG]</ta>
            <ta e="T319" id="Seg_721" s="T318">there</ta>
            <ta e="T320" id="Seg_722" s="T319">sit.down-PST.[3SG]</ta>
            <ta e="T321" id="Seg_723" s="T320">and</ta>
            <ta e="T322" id="Seg_724" s="T321">this.[NOM.SG]</ta>
            <ta e="T323" id="Seg_725" s="T322">water.[NOM.SG]</ta>
            <ta e="T324" id="Seg_726" s="T323">send-PST.[3SG]</ta>
            <ta e="T325" id="Seg_727" s="T324">this.[NOM.SG]</ta>
            <ta e="T326" id="Seg_728" s="T325">water.[NOM.SG]</ta>
            <ta e="T327" id="Seg_729" s="T326">fall-MOM-PST.[3SG]</ta>
            <ta e="T328" id="Seg_730" s="T327">this.[NOM.SG]</ta>
            <ta e="T329" id="Seg_731" s="T328">wolf-LAT</ta>
            <ta e="T330" id="Seg_732" s="T329">this.[NOM.SG]</ta>
            <ta e="T331" id="Seg_733" s="T330">PTCL</ta>
            <ta e="T332" id="Seg_734" s="T331">go-CVB</ta>
            <ta e="T333" id="Seg_735" s="T332">disappear-PST.[3SG]</ta>
            <ta e="T334" id="Seg_736" s="T333">water-INS</ta>
            <ta e="T335" id="Seg_737" s="T334">this.[NOM.SG]</ta>
            <ta e="T337" id="Seg_738" s="T336">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T338" id="Seg_739" s="T337">small.[NOM.SG]</ta>
            <ta e="T339" id="Seg_740" s="T338">pig-PL</ta>
            <ta e="T340" id="Seg_741" s="T339">take-MOM-PST.[3SG]</ta>
            <ta e="T341" id="Seg_742" s="T340">and</ta>
            <ta e="T342" id="Seg_743" s="T341">tent-LAT/LOC.3SG</ta>
            <ta e="T343" id="Seg_744" s="T342">go-CVB</ta>
            <ta e="T344" id="Seg_745" s="T343">disappear-PST.[3SG]</ta>
            <ta e="T345" id="Seg_746" s="T344">eat-PST.[3SG]</ta>
            <ta e="T346" id="Seg_747" s="T345">and</ta>
            <ta e="T347" id="Seg_748" s="T346">lie-PST.[3SG]</ta>
            <ta e="T348" id="Seg_749" s="T347">sleep-INF.LAT</ta>
            <ta e="T349" id="Seg_750" s="T348">and</ta>
            <ta e="T350" id="Seg_751" s="T349">wolf.[NOM.SG]</ta>
            <ta e="T352" id="Seg_752" s="T351">depart-PST.[3SG]</ta>
            <ta e="T353" id="Seg_753" s="T352">well</ta>
            <ta e="T354" id="Seg_754" s="T353">now</ta>
            <ta e="T355" id="Seg_755" s="T354">this.[NOM.SG]</ta>
            <ta e="T357" id="Seg_756" s="T356">pig.[NOM.SG]</ta>
            <ta e="T358" id="Seg_757" s="T357">%%-LAT</ta>
            <ta e="T359" id="Seg_758" s="T358">%%-MOM-PST.[3SG]</ta>
            <ta e="T360" id="Seg_759" s="T359">then</ta>
            <ta e="T362" id="Seg_760" s="T361">go-PST.[3SG]</ta>
            <ta e="T363" id="Seg_761" s="T362">house-PL-ABL</ta>
            <ta e="T364" id="Seg_762" s="T363">there</ta>
            <ta e="T365" id="Seg_763" s="T364">see-PST.[3SG]</ta>
            <ta e="T368" id="Seg_764" s="T367">see-DUR.[3SG]</ta>
            <ta e="T370" id="Seg_765" s="T369">see-PST.[3SG]</ta>
            <ta e="T371" id="Seg_766" s="T370">horse.[NOM.SG]</ta>
            <ta e="T372" id="Seg_767" s="T371">well</ta>
            <ta e="T373" id="Seg_768" s="T372">evening-LOC.ADV</ta>
            <ta e="T374" id="Seg_769" s="T373">come-FUT-1SG</ta>
            <ta e="T375" id="Seg_770" s="T374">and</ta>
            <ta e="T376" id="Seg_771" s="T375">eat-FUT-1SG</ta>
            <ta e="T377" id="Seg_772" s="T376">then</ta>
            <ta e="T378" id="Seg_773" s="T377">come-PST.[3SG]</ta>
            <ta e="T379" id="Seg_774" s="T378">evening-LOC.ADV</ta>
            <ta e="T380" id="Seg_775" s="T379">INCH</ta>
            <ta e="T381" id="Seg_776" s="T380">eat-INF.LAT</ta>
            <ta e="T382" id="Seg_777" s="T381">and</ta>
            <ta e="T383" id="Seg_778" s="T382">one.[NOM.SG]</ta>
            <ta e="T384" id="Seg_779" s="T383">man.[NOM.SG]</ta>
            <ta e="T385" id="Seg_780" s="T384">two.[NOM.SG]</ta>
            <ta e="T386" id="Seg_781" s="T385">shoot-MOM-PST.[3SG]</ta>
            <ta e="T387" id="Seg_782" s="T386">this-ACC</ta>
            <ta e="T388" id="Seg_783" s="T387">gun-INS</ta>
            <ta e="T389" id="Seg_784" s="T388">kill-RES-PST.[3SG]</ta>
            <ta e="T390" id="Seg_785" s="T389">and</ta>
            <ta e="T391" id="Seg_786" s="T390">this.[NOM.SG]</ta>
            <ta e="T392" id="Seg_787" s="T391">die-RES-PST.[3SG]</ta>
            <ta e="T393" id="Seg_788" s="T392">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T308" id="Seg_789" s="T307">тогда</ta>
            <ta e="T309" id="Seg_790" s="T308">этот-PL</ta>
            <ta e="T310" id="Seg_791" s="T309">прийти-PST-3PL</ta>
            <ta e="T311" id="Seg_792" s="T310">мельница-NOM/GEN/ACC.3SG</ta>
            <ta e="T312" id="Seg_793" s="T311">этот.[NOM.SG]</ta>
            <ta e="T313" id="Seg_794" s="T312">волк.[NOM.SG]</ta>
            <ta e="T314" id="Seg_795" s="T313">сказать-IPFVZ.[3SG]</ta>
            <ta e="T315" id="Seg_796" s="T314">ты.NOM</ta>
            <ta e="T316" id="Seg_797" s="T315">там</ta>
            <ta e="T317" id="Seg_798" s="T316">сидеть-IMP.2SG</ta>
            <ta e="T318" id="Seg_799" s="T317">этот.[NOM.SG]</ta>
            <ta e="T319" id="Seg_800" s="T318">там</ta>
            <ta e="T320" id="Seg_801" s="T319">сесть-PST.[3SG]</ta>
            <ta e="T321" id="Seg_802" s="T320">а</ta>
            <ta e="T322" id="Seg_803" s="T321">этот.[NOM.SG]</ta>
            <ta e="T323" id="Seg_804" s="T322">вода.[NOM.SG]</ta>
            <ta e="T324" id="Seg_805" s="T323">посылать-PST.[3SG]</ta>
            <ta e="T325" id="Seg_806" s="T324">этот.[NOM.SG]</ta>
            <ta e="T326" id="Seg_807" s="T325">вода.[NOM.SG]</ta>
            <ta e="T327" id="Seg_808" s="T326">упасть-MOM-PST.[3SG]</ta>
            <ta e="T328" id="Seg_809" s="T327">этот.[NOM.SG]</ta>
            <ta e="T329" id="Seg_810" s="T328">волк-LAT</ta>
            <ta e="T330" id="Seg_811" s="T329">этот.[NOM.SG]</ta>
            <ta e="T331" id="Seg_812" s="T330">PTCL</ta>
            <ta e="T332" id="Seg_813" s="T331">пойти-CVB</ta>
            <ta e="T333" id="Seg_814" s="T332">исчезнуть-PST.[3SG]</ta>
            <ta e="T334" id="Seg_815" s="T333">вода-INS</ta>
            <ta e="T335" id="Seg_816" s="T334">этот.[NOM.SG]</ta>
            <ta e="T337" id="Seg_817" s="T336">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T338" id="Seg_818" s="T337">маленький.[NOM.SG]</ta>
            <ta e="T339" id="Seg_819" s="T338">свинья-PL</ta>
            <ta e="T340" id="Seg_820" s="T339">взять-MOM-PST.[3SG]</ta>
            <ta e="T341" id="Seg_821" s="T340">и</ta>
            <ta e="T342" id="Seg_822" s="T341">чум-LAT/LOC.3SG</ta>
            <ta e="T343" id="Seg_823" s="T342">пойти-CVB</ta>
            <ta e="T344" id="Seg_824" s="T343">исчезнуть-PST.[3SG]</ta>
            <ta e="T345" id="Seg_825" s="T344">съесть-PST.[3SG]</ta>
            <ta e="T346" id="Seg_826" s="T345">и</ta>
            <ta e="T347" id="Seg_827" s="T346">лежать-PST.[3SG]</ta>
            <ta e="T348" id="Seg_828" s="T347">спать-INF.LAT</ta>
            <ta e="T349" id="Seg_829" s="T348">а</ta>
            <ta e="T350" id="Seg_830" s="T349">волк.[NOM.SG]</ta>
            <ta e="T352" id="Seg_831" s="T351">уйти-PST.[3SG]</ta>
            <ta e="T353" id="Seg_832" s="T352">ну</ta>
            <ta e="T354" id="Seg_833" s="T353">сейчас</ta>
            <ta e="T355" id="Seg_834" s="T354">этот.[NOM.SG]</ta>
            <ta e="T357" id="Seg_835" s="T356">свинья.[NOM.SG]</ta>
            <ta e="T358" id="Seg_836" s="T357">%%-LAT</ta>
            <ta e="T359" id="Seg_837" s="T358">%%-MOM-PST.[3SG]</ta>
            <ta e="T360" id="Seg_838" s="T359">тогда</ta>
            <ta e="T362" id="Seg_839" s="T361">пойти-PST.[3SG]</ta>
            <ta e="T363" id="Seg_840" s="T362">дом-PL-ABL</ta>
            <ta e="T364" id="Seg_841" s="T363">там</ta>
            <ta e="T365" id="Seg_842" s="T364">видеть-PST.[3SG]</ta>
            <ta e="T368" id="Seg_843" s="T367">видеть-DUR.[3SG]</ta>
            <ta e="T370" id="Seg_844" s="T369">видеть-PST.[3SG]</ta>
            <ta e="T371" id="Seg_845" s="T370">лошадь.[NOM.SG]</ta>
            <ta e="T372" id="Seg_846" s="T371">ну</ta>
            <ta e="T373" id="Seg_847" s="T372">вечер-LOC.ADV</ta>
            <ta e="T374" id="Seg_848" s="T373">прийти-FUT-1SG</ta>
            <ta e="T375" id="Seg_849" s="T374">и</ta>
            <ta e="T376" id="Seg_850" s="T375">съесть-FUT-1SG</ta>
            <ta e="T377" id="Seg_851" s="T376">тогда</ta>
            <ta e="T378" id="Seg_852" s="T377">прийти-PST.[3SG]</ta>
            <ta e="T379" id="Seg_853" s="T378">вечер-LOC.ADV</ta>
            <ta e="T380" id="Seg_854" s="T379">INCH</ta>
            <ta e="T381" id="Seg_855" s="T380">съесть-INF.LAT</ta>
            <ta e="T382" id="Seg_856" s="T381">а</ta>
            <ta e="T383" id="Seg_857" s="T382">один.[NOM.SG]</ta>
            <ta e="T384" id="Seg_858" s="T383">мужчина.[NOM.SG]</ta>
            <ta e="T385" id="Seg_859" s="T384">два.[NOM.SG]</ta>
            <ta e="T386" id="Seg_860" s="T385">стрелять-MOM-PST.[3SG]</ta>
            <ta e="T387" id="Seg_861" s="T386">этот-ACC</ta>
            <ta e="T388" id="Seg_862" s="T387">ружье-INS</ta>
            <ta e="T389" id="Seg_863" s="T388">убить-RES-PST.[3SG]</ta>
            <ta e="T390" id="Seg_864" s="T389">и</ta>
            <ta e="T391" id="Seg_865" s="T390">этот.[NOM.SG]</ta>
            <ta e="T392" id="Seg_866" s="T391">умереть-RES-PST.[3SG]</ta>
            <ta e="T393" id="Seg_867" s="T392">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T308" id="Seg_868" s="T307">adv</ta>
            <ta e="T309" id="Seg_869" s="T308">dempro-n:num</ta>
            <ta e="T310" id="Seg_870" s="T309">v-v:tense-v:pn</ta>
            <ta e="T311" id="Seg_871" s="T310">n-n:case.poss</ta>
            <ta e="T312" id="Seg_872" s="T311">dempro-n:case</ta>
            <ta e="T313" id="Seg_873" s="T312">n-n:case</ta>
            <ta e="T314" id="Seg_874" s="T313">v-v&gt;v-v:pn</ta>
            <ta e="T315" id="Seg_875" s="T314">pers</ta>
            <ta e="T316" id="Seg_876" s="T315">adv</ta>
            <ta e="T317" id="Seg_877" s="T316">v-v:mood.pn</ta>
            <ta e="T318" id="Seg_878" s="T317">dempro-n:case</ta>
            <ta e="T319" id="Seg_879" s="T318">adv</ta>
            <ta e="T320" id="Seg_880" s="T319">v-v:tense-v:pn</ta>
            <ta e="T321" id="Seg_881" s="T320">conj</ta>
            <ta e="T322" id="Seg_882" s="T321">dempro-n:case</ta>
            <ta e="T323" id="Seg_883" s="T322">n-n:case</ta>
            <ta e="T324" id="Seg_884" s="T323">v-v:tense-v:pn</ta>
            <ta e="T325" id="Seg_885" s="T324">dempro-n:case</ta>
            <ta e="T326" id="Seg_886" s="T325">n-n:case</ta>
            <ta e="T327" id="Seg_887" s="T326">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T328" id="Seg_888" s="T327">dempro-n:case</ta>
            <ta e="T329" id="Seg_889" s="T328">n-n:case</ta>
            <ta e="T330" id="Seg_890" s="T329">dempro-n:case</ta>
            <ta e="T331" id="Seg_891" s="T330">ptcl</ta>
            <ta e="T332" id="Seg_892" s="T331">v-v:n.fin</ta>
            <ta e="T333" id="Seg_893" s="T332">v-v:tense-v:pn</ta>
            <ta e="T334" id="Seg_894" s="T333">n-n:case</ta>
            <ta e="T335" id="Seg_895" s="T334">dempro-n:case</ta>
            <ta e="T337" id="Seg_896" s="T336">refl-n:case.poss</ta>
            <ta e="T338" id="Seg_897" s="T337">adj-n:case</ta>
            <ta e="T339" id="Seg_898" s="T338">n-n:num</ta>
            <ta e="T340" id="Seg_899" s="T339">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T341" id="Seg_900" s="T340">conj</ta>
            <ta e="T342" id="Seg_901" s="T341">n-n:case.poss</ta>
            <ta e="T343" id="Seg_902" s="T342">v-v:n.fin</ta>
            <ta e="T344" id="Seg_903" s="T343">v-v:tense-v:pn</ta>
            <ta e="T345" id="Seg_904" s="T344">v-v:tense-v:pn</ta>
            <ta e="T346" id="Seg_905" s="T345">conj</ta>
            <ta e="T347" id="Seg_906" s="T346">v-v:tense-v:pn</ta>
            <ta e="T348" id="Seg_907" s="T347">v-v:n.fin</ta>
            <ta e="T349" id="Seg_908" s="T348">conj</ta>
            <ta e="T350" id="Seg_909" s="T349">n-n:case</ta>
            <ta e="T352" id="Seg_910" s="T351">v-v:tense-v:pn</ta>
            <ta e="T353" id="Seg_911" s="T352">ptcl</ta>
            <ta e="T354" id="Seg_912" s="T353">adv</ta>
            <ta e="T355" id="Seg_913" s="T354">dempro-n:case</ta>
            <ta e="T357" id="Seg_914" s="T356">n-n:case</ta>
            <ta e="T358" id="Seg_915" s="T357">n-n:case</ta>
            <ta e="T359" id="Seg_916" s="T358">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T360" id="Seg_917" s="T359">adv</ta>
            <ta e="T362" id="Seg_918" s="T361">v-v:tense-v:pn</ta>
            <ta e="T363" id="Seg_919" s="T362">n-n:num-n:case</ta>
            <ta e="T364" id="Seg_920" s="T363">adv</ta>
            <ta e="T365" id="Seg_921" s="T364">v-v:tense-v:pn</ta>
            <ta e="T368" id="Seg_922" s="T367">v-v&gt;v-v:pn</ta>
            <ta e="T370" id="Seg_923" s="T369">v-v:tense-v:pn</ta>
            <ta e="T371" id="Seg_924" s="T370">n-n:case</ta>
            <ta e="T372" id="Seg_925" s="T371">ptcl</ta>
            <ta e="T373" id="Seg_926" s="T372">n-n:case</ta>
            <ta e="T374" id="Seg_927" s="T373">v-v:tense-v:pn</ta>
            <ta e="T375" id="Seg_928" s="T374">conj</ta>
            <ta e="T376" id="Seg_929" s="T375">v-v:tense-v:pn</ta>
            <ta e="T377" id="Seg_930" s="T376">adv</ta>
            <ta e="T378" id="Seg_931" s="T377">v-v:tense-v:pn</ta>
            <ta e="T379" id="Seg_932" s="T378">n-n:case</ta>
            <ta e="T380" id="Seg_933" s="T379">ptcl</ta>
            <ta e="T381" id="Seg_934" s="T380">v-v:n.fin</ta>
            <ta e="T382" id="Seg_935" s="T381">conj</ta>
            <ta e="T383" id="Seg_936" s="T382">num-n:case</ta>
            <ta e="T384" id="Seg_937" s="T383">n-n:case</ta>
            <ta e="T385" id="Seg_938" s="T384">num-n:case</ta>
            <ta e="T386" id="Seg_939" s="T385">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T387" id="Seg_940" s="T386">dempro-n:case</ta>
            <ta e="T388" id="Seg_941" s="T387">n-n:case</ta>
            <ta e="T389" id="Seg_942" s="T388">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T390" id="Seg_943" s="T389">conj</ta>
            <ta e="T391" id="Seg_944" s="T390">dempro-n:case</ta>
            <ta e="T392" id="Seg_945" s="T391">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T393" id="Seg_946" s="T392">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T308" id="Seg_947" s="T307">adv</ta>
            <ta e="T309" id="Seg_948" s="T308">dempro</ta>
            <ta e="T310" id="Seg_949" s="T309">v</ta>
            <ta e="T311" id="Seg_950" s="T310">n</ta>
            <ta e="T312" id="Seg_951" s="T311">dempro</ta>
            <ta e="T313" id="Seg_952" s="T312">n</ta>
            <ta e="T314" id="Seg_953" s="T313">v</ta>
            <ta e="T315" id="Seg_954" s="T314">pers</ta>
            <ta e="T316" id="Seg_955" s="T315">adv</ta>
            <ta e="T317" id="Seg_956" s="T316">v</ta>
            <ta e="T318" id="Seg_957" s="T317">dempro</ta>
            <ta e="T319" id="Seg_958" s="T318">adv</ta>
            <ta e="T320" id="Seg_959" s="T319">v</ta>
            <ta e="T321" id="Seg_960" s="T320">conj</ta>
            <ta e="T322" id="Seg_961" s="T321">dempro</ta>
            <ta e="T323" id="Seg_962" s="T322">n</ta>
            <ta e="T324" id="Seg_963" s="T323">v</ta>
            <ta e="T325" id="Seg_964" s="T324">dempro</ta>
            <ta e="T326" id="Seg_965" s="T325">n</ta>
            <ta e="T327" id="Seg_966" s="T326">v</ta>
            <ta e="T328" id="Seg_967" s="T327">dempro</ta>
            <ta e="T329" id="Seg_968" s="T328">n</ta>
            <ta e="T330" id="Seg_969" s="T329">dempro</ta>
            <ta e="T331" id="Seg_970" s="T330">ptcl</ta>
            <ta e="T332" id="Seg_971" s="T331">v</ta>
            <ta e="T333" id="Seg_972" s="T332">v</ta>
            <ta e="T334" id="Seg_973" s="T333">n</ta>
            <ta e="T335" id="Seg_974" s="T334">dempro</ta>
            <ta e="T337" id="Seg_975" s="T336">refl</ta>
            <ta e="T338" id="Seg_976" s="T337">adj</ta>
            <ta e="T339" id="Seg_977" s="T338">n</ta>
            <ta e="T340" id="Seg_978" s="T339">v</ta>
            <ta e="T341" id="Seg_979" s="T340">conj</ta>
            <ta e="T342" id="Seg_980" s="T341">n</ta>
            <ta e="T343" id="Seg_981" s="T342">v</ta>
            <ta e="T344" id="Seg_982" s="T343">v</ta>
            <ta e="T345" id="Seg_983" s="T344">v</ta>
            <ta e="T346" id="Seg_984" s="T345">conj</ta>
            <ta e="T347" id="Seg_985" s="T346">v</ta>
            <ta e="T348" id="Seg_986" s="T347">v</ta>
            <ta e="T349" id="Seg_987" s="T348">conj</ta>
            <ta e="T350" id="Seg_988" s="T349">n</ta>
            <ta e="T352" id="Seg_989" s="T351">v</ta>
            <ta e="T353" id="Seg_990" s="T352">ptcl</ta>
            <ta e="T354" id="Seg_991" s="T353">adv</ta>
            <ta e="T355" id="Seg_992" s="T354">dempro</ta>
            <ta e="T357" id="Seg_993" s="T356">n</ta>
            <ta e="T358" id="Seg_994" s="T357">n</ta>
            <ta e="T359" id="Seg_995" s="T358">v</ta>
            <ta e="T360" id="Seg_996" s="T359">adv</ta>
            <ta e="T362" id="Seg_997" s="T361">v</ta>
            <ta e="T363" id="Seg_998" s="T362">n</ta>
            <ta e="T364" id="Seg_999" s="T363">adv</ta>
            <ta e="T365" id="Seg_1000" s="T364">v</ta>
            <ta e="T368" id="Seg_1001" s="T367">v</ta>
            <ta e="T370" id="Seg_1002" s="T369">v</ta>
            <ta e="T371" id="Seg_1003" s="T370">n</ta>
            <ta e="T372" id="Seg_1004" s="T371">ptcl</ta>
            <ta e="T373" id="Seg_1005" s="T372">n</ta>
            <ta e="T374" id="Seg_1006" s="T373">v</ta>
            <ta e="T375" id="Seg_1007" s="T374">conj</ta>
            <ta e="T376" id="Seg_1008" s="T375">v</ta>
            <ta e="T377" id="Seg_1009" s="T376">adv</ta>
            <ta e="T378" id="Seg_1010" s="T377">v</ta>
            <ta e="T379" id="Seg_1011" s="T378">n</ta>
            <ta e="T380" id="Seg_1012" s="T379">ptcl</ta>
            <ta e="T381" id="Seg_1013" s="T380">v</ta>
            <ta e="T382" id="Seg_1014" s="T381">conj</ta>
            <ta e="T383" id="Seg_1015" s="T382">num</ta>
            <ta e="T384" id="Seg_1016" s="T383">n</ta>
            <ta e="T385" id="Seg_1017" s="T384">num</ta>
            <ta e="T386" id="Seg_1018" s="T385">v</ta>
            <ta e="T387" id="Seg_1019" s="T386">dempro</ta>
            <ta e="T388" id="Seg_1020" s="T387">n</ta>
            <ta e="T389" id="Seg_1021" s="T388">v</ta>
            <ta e="T390" id="Seg_1022" s="T389">conj</ta>
            <ta e="T391" id="Seg_1023" s="T390">dempro</ta>
            <ta e="T392" id="Seg_1024" s="T391">v</ta>
            <ta e="T393" id="Seg_1025" s="T392">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T308" id="Seg_1026" s="T307">adv:Time</ta>
            <ta e="T309" id="Seg_1027" s="T308">pro.h:A</ta>
            <ta e="T311" id="Seg_1028" s="T310">np:G</ta>
            <ta e="T313" id="Seg_1029" s="T312">np.h:A</ta>
            <ta e="T315" id="Seg_1030" s="T314">pro.h:A</ta>
            <ta e="T316" id="Seg_1031" s="T315">adv:L</ta>
            <ta e="T317" id="Seg_1032" s="T316">0.2.h:E</ta>
            <ta e="T318" id="Seg_1033" s="T317">pro.h:E</ta>
            <ta e="T319" id="Seg_1034" s="T318">adv:L</ta>
            <ta e="T322" id="Seg_1035" s="T321">pro.h:A</ta>
            <ta e="T323" id="Seg_1036" s="T322">np:Th</ta>
            <ta e="T326" id="Seg_1037" s="T325">np:Th</ta>
            <ta e="T329" id="Seg_1038" s="T328">np:G</ta>
            <ta e="T330" id="Seg_1039" s="T329">pro.h:E</ta>
            <ta e="T334" id="Seg_1040" s="T333">np:Ins</ta>
            <ta e="T335" id="Seg_1041" s="T334">pro.h:A</ta>
            <ta e="T339" id="Seg_1042" s="T338">np:Th</ta>
            <ta e="T342" id="Seg_1043" s="T341">np:G</ta>
            <ta e="T344" id="Seg_1044" s="T343">0.3.h:A</ta>
            <ta e="T345" id="Seg_1045" s="T344">0.3.h:A</ta>
            <ta e="T347" id="Seg_1046" s="T346">0.3.h:E</ta>
            <ta e="T350" id="Seg_1047" s="T349">np.h:A</ta>
            <ta e="T354" id="Seg_1048" s="T353">adv:Time</ta>
            <ta e="T360" id="Seg_1049" s="T359">adv:Time</ta>
            <ta e="T362" id="Seg_1050" s="T361">0.3.h:A</ta>
            <ta e="T363" id="Seg_1051" s="T362">np:So</ta>
            <ta e="T364" id="Seg_1052" s="T363">adv:L</ta>
            <ta e="T365" id="Seg_1053" s="T364">0.3.h:E</ta>
            <ta e="T368" id="Seg_1054" s="T367">0.3.h:E</ta>
            <ta e="T370" id="Seg_1055" s="T369">0.3.h:E</ta>
            <ta e="T371" id="Seg_1056" s="T370">np:Th</ta>
            <ta e="T373" id="Seg_1057" s="T372">n:Time</ta>
            <ta e="T374" id="Seg_1058" s="T373">0.1.h:A</ta>
            <ta e="T376" id="Seg_1059" s="T375">0.1.h:A</ta>
            <ta e="T377" id="Seg_1060" s="T376">adv:Time</ta>
            <ta e="T378" id="Seg_1061" s="T377">0.3.h:A</ta>
            <ta e="T379" id="Seg_1062" s="T378">n:Time</ta>
            <ta e="T384" id="Seg_1063" s="T383">np.h:A</ta>
            <ta e="T387" id="Seg_1064" s="T386">pro.h:P</ta>
            <ta e="T388" id="Seg_1065" s="T387">np:Ins</ta>
            <ta e="T389" id="Seg_1066" s="T388">0.3.h:A</ta>
            <ta e="T391" id="Seg_1067" s="T390">pro.h:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T309" id="Seg_1068" s="T308">pro.h:S</ta>
            <ta e="T310" id="Seg_1069" s="T309">v:pred</ta>
            <ta e="T313" id="Seg_1070" s="T312">np.h:S</ta>
            <ta e="T314" id="Seg_1071" s="T313">v:pred</ta>
            <ta e="T315" id="Seg_1072" s="T314">pro.h:S</ta>
            <ta e="T317" id="Seg_1073" s="T316">v:pred 0.2.h:S</ta>
            <ta e="T318" id="Seg_1074" s="T317">pro.h:S</ta>
            <ta e="T320" id="Seg_1075" s="T319">v:pred</ta>
            <ta e="T322" id="Seg_1076" s="T321">pro.h:S</ta>
            <ta e="T323" id="Seg_1077" s="T322">np:O</ta>
            <ta e="T324" id="Seg_1078" s="T323">v:pred</ta>
            <ta e="T326" id="Seg_1079" s="T325">np:S</ta>
            <ta e="T327" id="Seg_1080" s="T326">v:pred</ta>
            <ta e="T330" id="Seg_1081" s="T329">pro.h:S</ta>
            <ta e="T332" id="Seg_1082" s="T331">conv:pred</ta>
            <ta e="T333" id="Seg_1083" s="T332">v:pred</ta>
            <ta e="T335" id="Seg_1084" s="T334">pro.h:S</ta>
            <ta e="T339" id="Seg_1085" s="T338">np:O</ta>
            <ta e="T340" id="Seg_1086" s="T339">v:pred</ta>
            <ta e="T343" id="Seg_1087" s="T342">conv:pred</ta>
            <ta e="T344" id="Seg_1088" s="T343">v:pred 0.3.h:S</ta>
            <ta e="T345" id="Seg_1089" s="T344">v:pred 0.3.h:S</ta>
            <ta e="T347" id="Seg_1090" s="T346">v:pred 0.3.h:S</ta>
            <ta e="T348" id="Seg_1091" s="T347">s:purp</ta>
            <ta e="T350" id="Seg_1092" s="T349">np.h:S</ta>
            <ta e="T352" id="Seg_1093" s="T351">v:pred</ta>
            <ta e="T362" id="Seg_1094" s="T361">v:pred 0.3.h:S</ta>
            <ta e="T365" id="Seg_1095" s="T364">v:pred 0.3.h:S</ta>
            <ta e="T368" id="Seg_1096" s="T367">v:pred 0.3.h:S</ta>
            <ta e="T370" id="Seg_1097" s="T369">v:pred 0.3.h:S</ta>
            <ta e="T371" id="Seg_1098" s="T370">np:O</ta>
            <ta e="T374" id="Seg_1099" s="T373">v:pred 0.1.h:S</ta>
            <ta e="T376" id="Seg_1100" s="T375">v:pred 0.1.h:S</ta>
            <ta e="T378" id="Seg_1101" s="T377">v:pred 0.3.h:S</ta>
            <ta e="T380" id="Seg_1102" s="T379">ptcl:pred</ta>
            <ta e="T384" id="Seg_1103" s="T383">np.h:S</ta>
            <ta e="T386" id="Seg_1104" s="T385">v:pred</ta>
            <ta e="T387" id="Seg_1105" s="T386">pro.h:O</ta>
            <ta e="T389" id="Seg_1106" s="T388">v:pred 0.3.h:S</ta>
            <ta e="T391" id="Seg_1107" s="T390">pro.h:S</ta>
            <ta e="T392" id="Seg_1108" s="T391">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T311" id="Seg_1109" s="T310">TURK:cult</ta>
            <ta e="T313" id="Seg_1110" s="T312">RUS:cult</ta>
            <ta e="T321" id="Seg_1111" s="T320">RUS:gram</ta>
            <ta e="T329" id="Seg_1112" s="T328">RUS:cult</ta>
            <ta e="T331" id="Seg_1113" s="T330">TURK:disc</ta>
            <ta e="T341" id="Seg_1114" s="T340">RUS:gram</ta>
            <ta e="T346" id="Seg_1115" s="T345">RUS:gram</ta>
            <ta e="T349" id="Seg_1116" s="T348">RUS:gram</ta>
            <ta e="T350" id="Seg_1117" s="T349">RUS:cult</ta>
            <ta e="T353" id="Seg_1118" s="T352">RUS:disc</ta>
            <ta e="T363" id="Seg_1119" s="T362">TAT:cult</ta>
            <ta e="T375" id="Seg_1120" s="T374">RUS:gram</ta>
            <ta e="T380" id="Seg_1121" s="T379">RUS:gram</ta>
            <ta e="T382" id="Seg_1122" s="T381">RUS:gram</ta>
            <ta e="T388" id="Seg_1123" s="T387">TURK:cult</ta>
            <ta e="T390" id="Seg_1124" s="T389">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T311" id="Seg_1125" s="T307">Потом они пришли на мельницу.</ta>
            <ta e="T317" id="Seg_1126" s="T311">[Этот] волк говорит: «Ты там сиди!»</ta>
            <ta e="T320" id="Seg_1127" s="T317">Он сел там.</ta>
            <ta e="T324" id="Seg_1128" s="T320">А она спустила воду [с плотины].</ta>
            <ta e="T329" id="Seg_1129" s="T324">Вода упала на волка.</ta>
            <ta e="T334" id="Seg_1130" s="T329">Его унесло водой.</ta>
            <ta e="T344" id="Seg_1131" s="T334">Она сама взяла поросят и домой ушла.</ta>
            <ta e="T348" id="Seg_1132" s="T344">Поела и легла спать.</ta>
            <ta e="T352" id="Seg_1133" s="T348">А волк выбрался.</ta>
            <ta e="T359" id="Seg_1134" s="T352">Ну, теперь эта свинья (?).</ta>
            <ta e="T363" id="Seg_1135" s="T359">Потом он ушёл от домов.</ta>
            <ta e="T365" id="Seg_1136" s="T363">Там увидел.</ta>
            <ta e="T368" id="Seg_1137" s="T365">Смотрит.</ta>
            <ta e="T371" id="Seg_1138" s="T368">Увидел (лошадь?).</ta>
            <ta e="T376" id="Seg_1139" s="T371">«Ну, вечером приду и съем».</ta>
            <ta e="T379" id="Seg_1140" s="T376">Потом вечером пришёл.</ta>
            <ta e="T381" id="Seg_1141" s="T379">Хотел съесть [её].</ta>
            <ta e="T389" id="Seg_1142" s="T381">А один человек, два, выстрелил в него из ружья, убил.</ta>
            <ta e="T392" id="Seg_1143" s="T389">И он умер</ta>
            <ta e="T393" id="Seg_1144" s="T392">Хватит.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T311" id="Seg_1145" s="T307">Then they went to the mill.</ta>
            <ta e="T317" id="Seg_1146" s="T311">The wolf says: "You sit there!"</ta>
            <ta e="T320" id="Seg_1147" s="T317">He sat there.</ta>
            <ta e="T324" id="Seg_1148" s="T320">And [the other] released the water [from the dam].</ta>
            <ta e="T329" id="Seg_1149" s="T324">The water fell on the wolf.</ta>
            <ta e="T334" id="Seg_1150" s="T329">He went away with the water [was carried away by the water].</ta>
            <ta e="T344" id="Seg_1151" s="T334">She herself took the small pigs and went home.</ta>
            <ta e="T348" id="Seg_1152" s="T344">She ate and lied down to sleep.</ta>
            <ta e="T352" id="Seg_1153" s="T348">And the wolf went out.</ta>
            <ta e="T359" id="Seg_1154" s="T352">Well, now the pig (?).</ta>
            <ta e="T363" id="Seg_1155" s="T359">Then he went away from the houses.</ta>
            <ta e="T365" id="Seg_1156" s="T363">He saw there.</ta>
            <ta e="T368" id="Seg_1157" s="T365">He is looking.</ta>
            <ta e="T371" id="Seg_1158" s="T368">He saw a (horse?).</ta>
            <ta e="T376" id="Seg_1159" s="T371">“Well, in the evening I will come and eat (it).”</ta>
            <ta e="T379" id="Seg_1160" s="T376">Then he came in the evening.</ta>
            <ta e="T381" id="Seg_1161" s="T379">He wanted to eat [it].</ta>
            <ta e="T389" id="Seg_1162" s="T381">But one man, two, shot him with a gun, killed [him].</ta>
            <ta e="T392" id="Seg_1163" s="T389">And he died.</ta>
            <ta e="T393" id="Seg_1164" s="T392">Stop.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T311" id="Seg_1165" s="T307">Dann gingen sie zur Mühle.</ta>
            <ta e="T317" id="Seg_1166" s="T311">Der Wolf sagte: „Du setz dich dahin!“</ta>
            <ta e="T320" id="Seg_1167" s="T317">Er setzte sich dort.</ta>
            <ta e="T324" id="Seg_1168" s="T320">Und [der Andere] ließ das Wasser [vom Staudamm] ablaufen.</ta>
            <ta e="T329" id="Seg_1169" s="T324">Das Wasser floss auf den Wolf.</ta>
            <ta e="T334" id="Seg_1170" s="T329">Er ging mit dem Wasser [wurde vom Wasser fortgespült].</ta>
            <ta e="T344" id="Seg_1171" s="T334">Er selber nahm die kleinen Schweine und ging nach Hause.</ta>
            <ta e="T348" id="Seg_1172" s="T344">Er aß und legte sich zum Schlafen hin.</ta>
            <ta e="T352" id="Seg_1173" s="T348">Und der Wolf ging hinaus.</ta>
            <ta e="T359" id="Seg_1174" s="T352">Also, nun (?) das Schwein.</ta>
            <ta e="T363" id="Seg_1175" s="T359">Dann ging es weg von den Häusern.</ta>
            <ta e="T365" id="Seg_1176" s="T363">Er sah dort.</ta>
            <ta e="T368" id="Seg_1177" s="T365">Er schaut.</ta>
            <ta e="T371" id="Seg_1178" s="T368">Er sah ein (Pferd?).</ta>
            <ta e="T376" id="Seg_1179" s="T371">„Also, heute Abend, werde ich kommen und (es) fressen.“</ta>
            <ta e="T379" id="Seg_1180" s="T376">Dann kam er am Abend.</ta>
            <ta e="T381" id="Seg_1181" s="T379">Er wollte [es] fressen.</ta>
            <ta e="T389" id="Seg_1182" s="T381">Doch ein Mann, zwei, erschossen [ihn] mit einem Gewehr.</ta>
            <ta e="T392" id="Seg_1183" s="T389">Und er starb.</ta>
            <ta e="T393" id="Seg_1184" s="T392">Halt.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T311" id="Seg_1185" s="T307">[GVY:] Continuation from tape 191. [AAV]: Tape SU0192</ta>
            <ta e="T317" id="Seg_1186" s="T311">[AAV:] Actually, it is the pig which talks to the wolf.</ta>
            <ta e="T344" id="Seg_1187" s="T334">[AAV:] The pig.</ta>
            <ta e="T359" id="Seg_1188" s="T352">[GVY:] The last form is a verb, but the stem is unclear. The preceding form is supposedly a noun in the Lative case.</ta>
            <ta e="T368" id="Seg_1189" s="T365">Слышно Kuləʔbə</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
