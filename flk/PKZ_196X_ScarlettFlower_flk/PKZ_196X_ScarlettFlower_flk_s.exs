<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID27F79A8E-57CE-1F62-1803-9318C4B4C1D8">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_ScarlettFlower_flk.wav" />
         <referenced-file url="PKZ_196X_ScarlettFlower_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_ScarlettFlower_flk\PKZ_196X_ScarlettFlower_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">630</ud-information>
            <ud-information attribute-name="# HIAT:w">398</ud-information>
            <ud-information attribute-name="# e">396</ud-information>
            <ud-information attribute-name="# HIAT:u">82</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.002" type="appl" />
         <tli id="T1" time="0.886" type="appl" />
         <tli id="T2" time="2.0199730648709915" />
         <tli id="T3" time="2.701" type="appl" />
         <tli id="T4" time="3.323" type="appl" />
         <tli id="T5" time="3.945" type="appl" />
         <tli id="T6" time="4.568" type="appl" />
         <tli id="T7" time="5.19" type="appl" />
         <tli id="T8" time="5.812" type="appl" />
         <tli id="T9" time="6.813242482832188" />
         <tli id="T10" time="7.49" type="appl" />
         <tli id="T11" time="8.219" type="appl" />
         <tli id="T12" time="8.948" type="appl" />
         <tli id="T13" time="9.677" type="appl" />
         <tli id="T14" time="10.406" type="appl" />
         <tli id="T15" time="12.239836789119275" />
         <tli id="T16" time="12.99" type="appl" />
         <tli id="T17" time="13.539" type="appl" />
         <tli id="T18" time="14.088" type="appl" />
         <tli id="T19" time="14.637" type="appl" />
         <tli id="T20" time="15.186" type="appl" />
         <tli id="T21" time="15.735" type="appl" />
         <tli id="T22" time="16.402" type="appl" />
         <tli id="T23" time="17.069" type="appl" />
         <tli id="T24" time="17.736" type="appl" />
         <tli id="T25" time="18.403" type="appl" />
         <tli id="T26" time="19.9130678045203" />
         <tli id="T27" time="20.51" type="appl" />
         <tli id="T28" time="20.99" type="appl" />
         <tli id="T29" time="21.469" type="appl" />
         <tli id="T30" time="22.186" type="appl" />
         <tli id="T31" time="22.903" type="appl" />
         <tli id="T32" time="23.619" type="appl" />
         <tli id="T33" time="24.336" type="appl" />
         <tli id="T34" time="26.199650643376227" />
         <tli id="T35" time="27.181" type="appl" />
         <tli id="T36" time="27.725" type="appl" />
         <tli id="T37" time="28.269" type="appl" />
         <tli id="T38" time="28.813" type="appl" />
         <tli id="T39" time="29.358" type="appl" />
         <tli id="T40" time="29.902" type="appl" />
         <tli id="T41" time="30.446" type="appl" />
         <tli id="T42" time="30.99" type="appl" />
         <tli id="T43" time="31.533" type="appl" />
         <tli id="T44" time="32.076" type="appl" />
         <tli id="T45" time="32.618" type="appl" />
         <tli id="T46" time="33.161" type="appl" />
         <tli id="T47" time="33.97288032535502" />
         <tli id="T48" time="34.63" type="appl" />
         <tli id="T49" time="35.479526901793456" />
         <tli id="T50" time="36.66" type="appl" />
         <tli id="T51" time="37.672" type="appl" />
         <tli id="T52" time="38.949" type="appl" />
         <tli id="T53" time="40.033" type="appl" />
         <tli id="T54" time="41.118" type="appl" />
         <tli id="T55" time="42.202" type="appl" />
         <tli id="T56" time="43.286" type="appl" />
         <tli id="T57" time="44.37" type="appl" />
         <tli id="T58" time="45.128" type="appl" />
         <tli id="T59" time="45.886" type="appl" />
         <tli id="T60" time="46.644" type="appl" />
         <tli id="T61" time="47.62603160210681" />
         <tli id="T62" time="48.435" type="appl" />
         <tli id="T63" time="49.094" type="appl" />
         <tli id="T64" time="49.754" type="appl" />
         <tli id="T65" time="50.413" type="appl" />
         <tli id="T66" time="51.073" type="appl" />
         <tli id="T67" time="51.732" type="appl" />
         <tli id="T68" time="52.392" type="appl" />
         <tli id="T69" time="53.051" type="appl" />
         <tli id="T70" time="54.06594572971531" />
         <tli id="T71" time="55.06" type="appl" />
         <tli id="T72" time="56.063" type="appl" />
         <tli id="T73" time="57.065" type="appl" />
         <tli id="T74" time="58.068" type="appl" />
         <tli id="T75" time="59.071" type="appl" />
         <tli id="T76" time="60.131" type="appl" />
         <tli id="T77" time="60.889" type="appl" />
         <tli id="T78" time="62.46583372224815" />
         <tli id="T79" time="63.482" type="appl" />
         <tli id="T80" time="64.32" type="appl" />
         <tli id="T81" time="65.7124570971398" />
         <tli id="T82" time="66.587" type="appl" />
         <tli id="T83" time="67.27" type="appl" />
         <tli id="T84" time="67.954" type="appl" />
         <tli id="T85" time="68.638" type="appl" />
         <tli id="T86" time="69.59" type="appl" />
         <tli id="T87" time="70.531" type="appl" />
         <tli id="T88" time="71.69904393626241" />
         <tli id="T89" time="72.482" type="appl" />
         <tli id="T90" time="73.222" type="appl" />
         <tli id="T91" time="73.963" type="appl" />
         <tli id="T92" time="74.703" type="appl" />
         <tli id="T93" time="75.443" type="appl" />
         <tli id="T94" time="76.184" type="appl" />
         <tli id="T95" time="76.924" type="appl" />
         <tli id="T96" time="78.17229095273018" />
         <tli id="T97" time="79.038" type="appl" />
         <tli id="T98" time="79.609" type="appl" />
         <tli id="T99" time="80.179" type="appl" />
         <tli id="T100" time="80.877" type="appl" />
         <tli id="T101" time="81.636" type="appl" />
         <tli id="T102" time="82.395" type="appl" />
         <tli id="T103" time="83.4855534368958" />
         <tli id="T104" time="85.369" type="appl" />
         <tli id="T105" time="86.924" type="appl" />
         <tli id="T106" time="88.48" type="appl" />
         <tli id="T107" time="90.83878871924796" />
         <tli id="T108" time="91.527" type="appl" />
         <tli id="T109" time="91.979" type="appl" />
         <tli id="T110" time="92.43" type="appl" />
         <tli id="T111" time="93.455" type="appl" />
         <tli id="T112" time="94.48" type="appl" />
         <tli id="T113" time="95.73872338155877" />
         <tli id="T114" time="96.84" type="appl" />
         <tli id="T115" time="97.385" type="appl" />
         <tli id="T116" time="97.931" type="appl" />
         <tli id="T117" time="98.476" type="appl" />
         <tli id="T118" time="99.022" type="appl" />
         <tli id="T119" time="99.567" type="appl" />
         <tli id="T120" time="99.895" type="appl" />
         <tli id="T121" time="100.216" type="appl" />
         <tli id="T122" time="100.536" type="appl" />
         <tli id="T123" time="100.856" type="appl" />
         <tli id="T124" time="101.176" type="appl" />
         <tli id="T125" time="101.497" type="appl" />
         <tli id="T126" time="101.817" type="appl" />
         <tli id="T127" time="102.561" type="appl" />
         <tli id="T128" time="103.239" type="appl" />
         <tli id="T129" time="103.918" type="appl" />
         <tli id="T130" time="104.596" type="appl" />
         <tli id="T131" time="105.274" type="appl" />
         <tli id="T132" time="106.178" type="appl" />
         <tli id="T133" time="107.042" type="appl" />
         <tli id="T134" time="107.905" type="appl" />
         <tli id="T135" time="109.37187492499501" />
         <tli id="T136" time="110.74" type="appl" />
         <tli id="T137" time="111.96" type="appl" />
         <tli id="T138" time="113.118" type="appl" />
         <tli id="T139" time="113.949" type="appl" />
         <tli id="T140" time="114.779" type="appl" />
         <tli id="T141" time="115.61" type="appl" />
         <tli id="T142" time="116.44" type="appl" />
         <tli id="T143" time="117.27" type="appl" />
         <tli id="T144" time="118.101" type="appl" />
         <tli id="T145" time="119.27174291619441" />
         <tli id="T146" time="120.088" type="appl" />
         <tli id="T147" time="120.643" type="appl" />
         <tli id="T148" time="121.199" type="appl" />
         <tli id="T149" time="121.755" type="appl" />
         <tli id="T150" time="122.52" type="appl" />
         <tli id="T151" time="123.286" type="appl" />
         <tli id="T152" time="124.051" type="appl" />
         <tli id="T153" time="124.816" type="appl" />
         <tli id="T154" time="125.582" type="appl" />
         <tli id="T155" time="126.347" type="appl" />
         <tli id="T156" time="127.113" type="appl" />
         <tli id="T157" time="127.878" type="appl" />
         <tli id="T158" time="128.916" type="appl" />
         <tli id="T159" time="129.577" type="appl" />
         <tli id="T160" time="130.43159410627374" />
         <tli id="T161" time="131.092" type="appl" />
         <tli id="T162" time="131.875" type="appl" />
         <tli id="T163" time="132.658" type="appl" />
         <tli id="T164" time="133.44" type="appl" />
         <tli id="T165" time="134.223" type="appl" />
         <tli id="T166" time="135.45819374624975" />
         <tli id="T167" time="136.547" type="appl" />
         <tli id="T168" time="137.468" type="appl" />
         <tli id="T169" time="138.388" type="appl" />
         <tli id="T170" time="139.309" type="appl" />
         <tli id="T171" time="139.774" type="appl" />
         <tli id="T172" time="140.238" type="appl" />
         <tli id="T173" time="140.703" type="appl" />
         <tli id="T174" time="141.168" type="appl" />
         <tli id="T175" time="141.633" type="appl" />
         <tli id="T176" time="142.097" type="appl" />
         <tli id="T177" time="142.562" type="appl" />
         <tli id="T178" time="143.027" type="appl" />
         <tli id="T179" time="143.492" type="appl" />
         <tli id="T180" time="143.956" type="appl" />
         <tli id="T181" time="144.69140396026404" />
         <tli id="T182" time="145.718" type="appl" />
         <tli id="T183" time="146.555" type="appl" />
         <tli id="T184" time="147.35200650355858" />
         <tli id="T185" time="147.965" type="appl" />
         <tli id="T186" time="148.539" type="appl" />
         <tli id="T187" time="150.5446592439496" />
         <tli id="T188" time="151.511" type="appl" />
         <tli id="T189" time="152.254" type="appl" />
         <tli id="T190" time="152.996" type="appl" />
         <tli id="T191" time="153.739" type="appl" />
         <tli id="T192" time="154.482" type="appl" />
         <tli id="T193" time="154.909" type="appl" />
         <tli id="T194" time="155.284" type="appl" />
         <tli id="T195" time="155.658" type="appl" />
         <tli id="T196" time="156.033" type="appl" />
         <tli id="T197" time="156.408" type="appl" />
         <tli id="T198" time="156.783" type="appl" />
         <tli id="T199" time="157.157" type="appl" />
         <tli id="T200" time="157.532" type="appl" />
         <tli id="T201" time="157.907" type="appl" />
         <tli id="T202" time="158.57" type="appl" />
         <tli id="T203" time="159.233" type="appl" />
         <tli id="T204" time="160.007" type="appl" />
         <tli id="T205" time="160.565" type="appl" />
         <tli id="T206" time="161.122" type="appl" />
         <tli id="T207" time="161.68" type="appl" />
         <tli id="T208" time="162.238" type="appl" />
         <tli id="T209" time="163.27782278818592" />
         <tli id="T210" time="164.298" type="appl" />
         <tli id="T396" time="164.68928571428572" type="intp" />
         <tli id="T211" time="165.211" type="appl" />
         <tli id="T212" time="165.905" type="appl" />
         <tli id="T213" time="166.598" type="appl" />
         <tli id="T214" time="168.63108473898257" />
         <tli id="T215" time="169.535" type="appl" />
         <tli id="T216" time="170.43" type="appl" />
         <tli id="T217" time="171.324" type="appl" />
         <tli id="T218" time="173.14435789052604" />
         <tli id="T219" time="173.966" type="appl" />
         <tli id="T220" time="174.533" type="appl" />
         <tli id="T221" time="175.099" type="appl" />
         <tli id="T222" time="175.666" type="appl" />
         <tli id="T223" time="176.232" type="appl" />
         <tli id="T224" time="176.799" type="appl" />
         <tli id="T225" time="177.365" type="appl" />
         <tli id="T226" time="177.932" type="appl" />
         <tli id="T227" time="178.498" type="appl" />
         <tli id="T228" time="179.065" type="appl" />
         <tli id="T229" time="179.75093646243081" />
         <tli id="T230" time="180.729" type="appl" />
         <tli id="T231" time="181.621" type="appl" />
         <tli id="T232" time="182.513" type="appl" />
         <tli id="T233" time="183.405" type="appl" />
         <tli id="T234" time="184.56420561370757" />
         <tli id="T235" time="185.294" type="appl" />
         <tli id="T236" time="185.953" type="appl" />
         <tli id="T237" time="186.612" type="appl" />
         <tli id="T238" time="187.271" type="appl" />
         <tli id="T239" time="187.93" type="appl" />
         <tli id="T240" time="188.589" type="appl" />
         <tli id="T241" time="189.248" type="appl" />
         <tli id="T242" time="191.2241168077872" />
         <tli id="T243" time="192.088" type="appl" />
         <tli id="T244" time="192.626" type="appl" />
         <tli id="T245" time="195.61072498166544" />
         <tli id="T246" time="196.396" type="appl" />
         <tli id="T247" time="196.896" type="appl" />
         <tli id="T248" time="197.396" type="appl" />
         <tli id="T249" time="197.896" type="appl" />
         <tli id="T250" time="198.396" type="appl" />
         <tli id="T251" time="198.896" type="appl" />
         <tli id="T252" time="199.7506697779852" />
         <tli id="T253" time="200.496" type="appl" />
         <tli id="T254" time="201.368" type="appl" />
         <tli id="T255" time="202.24" type="appl" />
         <tli id="T256" time="203.112" type="appl" />
         <tli id="T257" time="203.694" type="appl" />
         <tli id="T258" time="204.272" type="appl" />
         <tli id="T259" time="204.851" type="appl" />
         <tli id="T260" time="205.429" type="appl" />
         <tli id="T261" time="206.007" type="appl" />
         <tli id="T262" time="206.585" type="appl" />
         <tli id="T263" time="207.164" type="appl" />
         <tli id="T264" time="207.742" type="appl" />
         <tli id="T265" time="208.32" type="appl" />
         <tli id="T266" time="209.087" type="appl" />
         <tli id="T267" time="209.854" type="appl" />
         <tli id="T268" time="210.622" type="appl" />
         <tli id="T269" time="212.0638389225948" />
         <tli id="T270" time="213.082" type="appl" />
         <tli id="T271" time="213.915" type="appl" />
         <tli id="T272" time="214.417" type="appl" />
         <tli id="T273" time="214.919" type="appl" />
         <tli id="T274" time="215.421" type="appl" />
         <tli id="T275" time="215.923" type="appl" />
         <tli id="T276" time="216.425" type="appl" />
         <tli id="T277" time="217.36376825121673" />
         <tli id="T278" time="218.075" type="appl" />
         <tli id="T279" time="218.707" type="appl" />
         <tli id="T280" time="219.339" type="appl" />
         <tli id="T281" time="219.9" type="appl" />
         <tli id="T282" time="220.461" type="appl" />
         <tli id="T283" time="221.022" type="appl" />
         <tli id="T284" time="221.583" type="appl" />
         <tli id="T285" time="222.28" type="appl" />
         <tli id="T286" time="222.836" type="appl" />
         <tli id="T287" time="224.47034015601042" />
         <tli id="T288" time="225.196" type="appl" />
         <tli id="T289" time="225.728" type="appl" />
         <tli id="T290" time="226.26" type="appl" />
         <tli id="T291" time="226.952" type="appl" />
         <tli id="T292" time="227.643" type="appl" />
         <tli id="T293" time="228.30833585364024" />
         <tli id="T294" time="228.711" type="appl" />
         <tli id="T295" time="229.088" type="appl" />
         <tli id="T296" time="229.464" type="appl" />
         <tli id="T297" time="229.841" type="appl" />
         <tli id="T298" time="230.217" type="appl" />
         <tli id="T299" time="230.594" type="appl" />
         <tli id="T300" time="230.97" type="appl" />
         <tli id="T301" time="231.347" type="appl" />
         <tli id="T302" time="231.723" type="appl" />
         <tli id="T303" time="232.1" type="appl" />
         <tli id="T304" time="232.63689792652843" />
         <tli id="T305" time="233.581" type="appl" />
         <tli id="T306" time="234.192" type="appl" />
         <tli id="T307" time="234.803" type="appl" />
         <tli id="T308" time="235.414" type="appl" />
         <tli id="T309" time="236.026" type="appl" />
         <tli id="T310" time="236.637" type="appl" />
         <tli id="T311" time="237.248" type="appl" />
         <tli id="T312" time="237.859" type="appl" />
         <tli id="T313" time="238.907" type="appl" />
         <tli id="T314" time="239.955" type="appl" />
         <tli id="T315" time="241.004" type="appl" />
         <tli id="T316" time="242.052" type="appl" />
         <tli id="T317" time="243.1" type="appl" />
         <tli id="T318" time="243.62" type="appl" />
         <tli id="T319" time="244.139" type="appl" />
         <tli id="T320" time="244.727" type="appl" />
         <tli id="T321" time="245.315" type="appl" />
         <tli id="T322" time="245.903" type="appl" />
         <tli id="T323" time="246.491" type="appl" />
         <tli id="T324" time="247.079" type="appl" />
         <tli id="T325" time="247.666" type="appl" />
         <tli id="T326" time="248.254" type="appl" />
         <tli id="T327" time="248.842" type="appl" />
         <tli id="T328" time="249.43" type="appl" />
         <tli id="T329" time="250.018" type="appl" />
         <tli id="T330" time="250.84998839922662" />
         <tli id="T331" time="251.966" type="appl" />
         <tli id="T332" time="253.062" type="appl" />
         <tli id="T333" time="254.2966091072738" />
         <tli id="T334" time="254.957" type="appl" />
         <tli id="T335" time="255.54" type="appl" />
         <tli id="T336" time="256.124" type="appl" />
         <tli id="T337" time="256.707" type="appl" />
         <tli id="T338" time="257.29" type="appl" />
         <tli id="T339" time="257.873" type="appl" />
         <tli id="T340" time="258.459" type="appl" />
         <tli id="T341" time="259.045" type="appl" />
         <tli id="T342" time="259.63" type="appl" />
         <tli id="T343" time="260.216" type="appl" />
         <tli id="T344" time="261.10318501233417" />
         <tli id="T345" time="261.785" type="appl" />
         <tli id="T346" time="262.38" type="appl" />
         <tli id="T347" time="262.974" type="appl" />
         <tli id="T348" time="264.1698107873858" />
         <tli id="T349" time="264.942" type="appl" />
         <tli id="T350" time="265.57" type="appl" />
         <tli id="T351" time="266.199" type="appl" />
         <tli id="T352" time="267.045" type="appl" />
         <tli id="T353" time="267.814" type="appl" />
         <tli id="T354" time="268.582" type="appl" />
         <tli id="T355" time="269.351" type="appl" />
         <tli id="T356" time="270.12" type="appl" />
         <tli id="T357" time="270.889" type="appl" />
         <tli id="T358" time="271.581" type="appl" />
         <tli id="T359" time="272.4230340689379" />
         <tli id="T360" time="273.228" type="appl" />
         <tli id="T361" time="273.956" type="appl" />
         <tli id="T362" time="274.685" type="appl" />
         <tli id="T363" time="275.88965451030066" />
         <tli id="T364" time="276.935" type="appl" />
         <tli id="T365" time="277.825" type="appl" />
         <tli id="T366" time="278.459" type="appl" />
         <tli id="T367" time="279.96293352890194" />
         <tli id="T368" time="280.608" type="appl" />
         <tli id="T369" time="281.151" type="appl" />
         <tli id="T370" time="281.693" type="appl" />
         <tli id="T371" time="282.236" type="appl" />
         <tli id="T372" time="282.778" type="appl" />
         <tli id="T373" time="283.626" type="appl" />
         <tli id="T374" time="284.474" type="appl" />
         <tli id="T375" time="285.322" type="appl" />
         <tli id="T376" time="286.17" type="appl" />
         <tli id="T377" time="286.989" type="appl" />
         <tli id="T378" time="287.9694934328955" />
         <tli id="T379" time="289.102" type="appl" />
         <tli id="T380" time="289.778" type="appl" />
         <tli id="T381" time="290.453" type="appl" />
         <tli id="T382" time="291.129" type="appl" />
         <tli id="T383" time="291.804" type="appl" />
         <tli id="T384" time="292.479" type="appl" />
         <tli id="T385" time="293.155" type="appl" />
         <tli id="T386" time="293.83" type="appl" />
         <tli id="T387" time="294.453" type="appl" />
         <tli id="T388" time="295.076" type="appl" />
         <tli id="T389" time="295.699" type="appl" />
         <tli id="T390" time="296.322" type="appl" />
         <tli id="T391" time="296.945" type="appl" />
         <tli id="T392" time="297.568" type="appl" />
         <tli id="T393" time="298.463" type="appl" />
         <tli id="T394" time="299.219" type="appl" />
         <tli id="T395" time="299.976" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T89" start="T88">
            <tli id="T88.tx.1" />
         </timeline-fork>
         <timeline-fork end="T328" start="T327">
            <tli id="T327.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T395" id="Seg_0" n="sc" s="T0">
               <ts e="T2" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Amnobi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">büzʼe</ts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_11" n="HIAT:u" s="T2">
                  <nts id="Seg_12" n="HIAT:ip">(</nts>
                  <ts e="T3" id="Seg_14" n="HIAT:w" s="T2">Dĭn</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_17" n="HIAT:w" s="T3">bɨl</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_20" n="HIAT:w" s="T4">na-</ts>
                  <nts id="Seg_21" n="HIAT:ip">)</nts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_24" n="HIAT:w" s="T5">Dĭn</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_27" n="HIAT:w" s="T6">ibiʔi</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_30" n="HIAT:w" s="T7">nagur</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_33" n="HIAT:w" s="T8">koʔbdozaŋdə</ts>
                  <nts id="Seg_34" n="HIAT:ip">.</nts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_37" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_39" n="HIAT:w" s="T9">Dĭ</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_42" n="HIAT:w" s="T10">stal</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_44" n="HIAT:ip">(</nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">ob-</ts>
                  <nts id="Seg_47" n="HIAT:ip">)</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_50" n="HIAT:w" s="T12">oʔbdəsʼtə</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_53" n="HIAT:w" s="T13">gorăttə</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">kanzittə</ts>
                  <nts id="Seg_57" n="HIAT:ip">.</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_60" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_62" n="HIAT:w" s="T15">Urgo</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_65" n="HIAT:w" s="T16">koʔbdondə</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_68" n="HIAT:w" s="T17">măndə:</ts>
                  <nts id="Seg_69" n="HIAT:ip">"</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_72" n="HIAT:w" s="T18">Ĭmbi</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_75" n="HIAT:w" s="T19">tănan</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_78" n="HIAT:w" s="T20">izittə</ts>
                  <nts id="Seg_79" n="HIAT:ip">?</nts>
                  <nts id="Seg_80" n="HIAT:ip">"</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_83" n="HIAT:u" s="T21">
                  <nts id="Seg_84" n="HIAT:ip">"</nts>
                  <ts e="T22" id="Seg_86" n="HIAT:w" s="T21">Tăn</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_89" n="HIAT:w" s="T22">măna</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_92" n="HIAT:w" s="T23">kuvas</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_95" n="HIAT:w" s="T24">platʼtʼa</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_98" n="HIAT:w" s="T25">iʔ</ts>
                  <nts id="Seg_99" n="HIAT:ip">"</nts>
                  <nts id="Seg_100" n="HIAT:ip">.</nts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_103" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_105" n="HIAT:w" s="T26">A</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_108" n="HIAT:w" s="T27">üdʼüget</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_111" n="HIAT:w" s="T28">koʔbdo:</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_113" n="HIAT:ip">"</nts>
                  <ts e="T30" id="Seg_115" n="HIAT:w" s="T29">Tăn</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_118" n="HIAT:w" s="T30">măna</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_121" n="HIAT:w" s="T31">bătinkaʔi</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_124" n="HIAT:w" s="T32">kuvas</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_127" n="HIAT:w" s="T33">iʔ</ts>
                  <nts id="Seg_128" n="HIAT:ip">"</nts>
                  <nts id="Seg_129" n="HIAT:ip">.</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_132" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_134" n="HIAT:w" s="T34">A</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_137" n="HIAT:w" s="T35">nagur</ts>
                  <nts id="Seg_138" n="HIAT:ip">,</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_141" n="HIAT:w" s="T36">išo</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_144" n="HIAT:w" s="T37">üdʼüge</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_147" n="HIAT:w" s="T38">koʔbdo:</ts>
                  <nts id="Seg_148" n="HIAT:ip">"</nts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_151" n="HIAT:w" s="T39">Tănan</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_154" n="HIAT:w" s="T40">ĭmbi</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_157" n="HIAT:w" s="T41">izittə</ts>
                  <nts id="Seg_158" n="HIAT:ip">?</nts>
                  <nts id="Seg_159" n="HIAT:ip">"</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_162" n="HIAT:u" s="T42">
                  <nts id="Seg_163" n="HIAT:ip">"</nts>
                  <ts e="T43" id="Seg_165" n="HIAT:w" s="T42">Tăn</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_168" n="HIAT:w" s="T43">măna</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_171" n="HIAT:w" s="T44">iʔ</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_174" n="HIAT:w" s="T45">kömə</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_177" n="HIAT:w" s="T46">sʼvʼetok</ts>
                  <nts id="Seg_178" n="HIAT:ip">"</nts>
                  <nts id="Seg_179" n="HIAT:ip">.</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_182" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_184" n="HIAT:w" s="T47">Dĭ</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_187" n="HIAT:w" s="T48">kambi</ts>
                  <nts id="Seg_188" n="HIAT:ip">.</nts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_191" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_193" n="HIAT:w" s="T49">Măndərbi</ts>
                  <nts id="Seg_194" n="HIAT:ip">,</nts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_197" n="HIAT:w" s="T50">măndərbi</ts>
                  <nts id="Seg_198" n="HIAT:ip">.</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_201" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_203" n="HIAT:w" s="T51">Ibi</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_206" n="HIAT:w" s="T52">döʔnə</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_209" n="HIAT:w" s="T53">platʼtʼa</ts>
                  <nts id="Seg_210" n="HIAT:ip">,</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_213" n="HIAT:w" s="T54">onʼiʔtə</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_216" n="HIAT:w" s="T55">ibi</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_219" n="HIAT:w" s="T56">bătinkaʔi</ts>
                  <nts id="Seg_220" n="HIAT:ip">.</nts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_223" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_225" n="HIAT:w" s="T57">A</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_227" n="HIAT:ip">(</nts>
                  <ts e="T59" id="Seg_229" n="HIAT:w" s="T58">sʼvet-</ts>
                  <nts id="Seg_230" n="HIAT:ip">)</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_233" n="HIAT:w" s="T59">sʼvʼetok</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_236" n="HIAT:w" s="T60">naga</ts>
                  <nts id="Seg_237" n="HIAT:ip">.</nts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_240" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_242" n="HIAT:w" s="T61">Dĭgəttə</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_245" n="HIAT:w" s="T62">šonəga</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_248" n="HIAT:w" s="T63">i</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_251" n="HIAT:w" s="T64">tenölaʔbə:</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_254" n="HIAT:w" s="T65">gijen</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_256" n="HIAT:ip">(</nts>
                  <ts e="T67" id="Seg_258" n="HIAT:w" s="T66">i-</ts>
                  <nts id="Seg_259" n="HIAT:ip">)</nts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_262" n="HIAT:w" s="T67">izittə</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_265" n="HIAT:w" s="T68">kömə</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_268" n="HIAT:w" s="T69">sʼvʼetok</ts>
                  <nts id="Seg_269" n="HIAT:ip">?</nts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_272" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_274" n="HIAT:w" s="T70">Šonəga</ts>
                  <nts id="Seg_275" n="HIAT:ip">,</nts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_278" n="HIAT:w" s="T71">kubi</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_281" n="HIAT:w" s="T72">bar</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_284" n="HIAT:w" s="T73">košideŋgən</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_287" n="HIAT:w" s="T74">sʼvʼetogəʔi</ts>
                  <nts id="Seg_288" n="HIAT:ip">.</nts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_291" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_293" n="HIAT:w" s="T75">Dĭbər</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_296" n="HIAT:w" s="T76">kambi</ts>
                  <nts id="Seg_297" n="HIAT:ip">,</nts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_300" n="HIAT:w" s="T77">nĭŋgəbi</ts>
                  <nts id="Seg_301" n="HIAT:ip">.</nts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_304" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_306" n="HIAT:w" s="T78">Vărotaʔi</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_308" n="HIAT:ip">(</nts>
                  <ts e="T80" id="Seg_310" n="HIAT:w" s="T79">kau-</ts>
                  <nts id="Seg_311" n="HIAT:ip">)</nts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_313" n="HIAT:ip">(</nts>
                  <ts e="T81" id="Seg_315" n="HIAT:w" s="T80">kajluʔpiʔi</ts>
                  <nts id="Seg_316" n="HIAT:ip">)</nts>
                  <nts id="Seg_317" n="HIAT:ip">.</nts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_320" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_322" n="HIAT:w" s="T81">Dĭgəttə</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_325" n="HIAT:w" s="T82">mĭmbi</ts>
                  <nts id="Seg_326" n="HIAT:ip">,</nts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_329" n="HIAT:w" s="T83">mĭmbi</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_331" n="HIAT:ip">(</nts>
                  <ts e="T85" id="Seg_333" n="HIAT:w" s="T84">dunʼtʼigə</ts>
                  <nts id="Seg_334" n="HIAT:ip">)</nts>
                  <nts id="Seg_335" n="HIAT:ip">.</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_338" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_340" n="HIAT:w" s="T85">Nüdʼin</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_343" n="HIAT:w" s="T86">šobi</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_346" n="HIAT:w" s="T87">kuza</ts>
                  <nts id="Seg_347" n="HIAT:ip">.</nts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_350" n="HIAT:u" s="T88">
                  <ts e="T88.tx.1" id="Seg_352" n="HIAT:w" s="T88">|</ts>
                  <nts id="Seg_353" n="HIAT:ip">(</nts>
                  <ts e="T89" id="Seg_355" n="HIAT:w" s="T88.tx.1">Bot-</ts>
                  <nts id="Seg_356" n="HIAT:ip">)</nts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_359" n="HIAT:w" s="T89">Boslə</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_362" n="HIAT:w" s="T90">koʔbdol</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_365" n="HIAT:w" s="T91">mĭləl</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_368" n="HIAT:w" s="T92">măna</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_370" n="HIAT:ip">(</nts>
                  <ts e="T94" id="Seg_372" n="HIAT:w" s="T93">tĭbi=</ts>
                  <nts id="Seg_373" n="HIAT:ip">)</nts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_376" n="HIAT:w" s="T94">detlil</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_379" n="HIAT:w" s="T95">döbər</ts>
                  <nts id="Seg_380" n="HIAT:ip">"</nts>
                  <nts id="Seg_381" n="HIAT:ip">.</nts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T99" id="Seg_384" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_386" n="HIAT:w" s="T96">Dĭ</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_389" n="HIAT:w" s="T97">tenöbi</ts>
                  <nts id="Seg_390" n="HIAT:ip">,</nts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_393" n="HIAT:w" s="T98">tenöbi</ts>
                  <nts id="Seg_394" n="HIAT:ip">.</nts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_397" n="HIAT:u" s="T99">
                  <nts id="Seg_398" n="HIAT:ip">"</nts>
                  <ts e="T100" id="Seg_400" n="HIAT:w" s="T99">Detlim</ts>
                  <nts id="Seg_401" n="HIAT:ip">!</nts>
                  <nts id="Seg_402" n="HIAT:ip">"</nts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_405" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_407" n="HIAT:w" s="T100">Dĭgəttə</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_410" n="HIAT:w" s="T101">erten</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_413" n="HIAT:w" s="T102">uʔbdəbi</ts>
                  <nts id="Seg_414" n="HIAT:ip">.</nts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_417" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_419" n="HIAT:w" s="T103">Vărotatsi</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_421" n="HIAT:ip">(</nts>
                  <ts e="T105" id="Seg_423" n="HIAT:w" s="T104">kajluʔ-</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_426" n="HIAT:w" s="T105">kajl-</ts>
                  <nts id="Seg_427" n="HIAT:ip">)</nts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_430" n="HIAT:w" s="T106">kajluʔpiʔi</ts>
                  <nts id="Seg_431" n="HIAT:ip">.</nts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_434" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_436" n="HIAT:w" s="T107">I</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_439" n="HIAT:w" s="T108">šobi</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_442" n="HIAT:w" s="T109">maːʔndə</ts>
                  <nts id="Seg_443" n="HIAT:ip">.</nts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_446" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_448" n="HIAT:w" s="T110">Deʔpi</ts>
                  <nts id="Seg_449" n="HIAT:ip">,</nts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_452" n="HIAT:w" s="T111">bar</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_455" n="HIAT:w" s="T112">mĭbi</ts>
                  <nts id="Seg_456" n="HIAT:ip">.</nts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_459" n="HIAT:u" s="T113">
                  <nts id="Seg_460" n="HIAT:ip">(</nts>
                  <ts e="T114" id="Seg_462" n="HIAT:w" s="T113">Nagurgö</ts>
                  <nts id="Seg_463" n="HIAT:ip">)</nts>
                  <nts id="Seg_464" n="HIAT:ip">.</nts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_467" n="HIAT:u" s="T114">
                  <nts id="Seg_468" n="HIAT:ip">"</nts>
                  <ts e="T115" id="Seg_470" n="HIAT:w" s="T114">A</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_473" n="HIAT:w" s="T115">tănan</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_476" n="HIAT:w" s="T116">nada</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_479" n="HIAT:w" s="T117">dĭbər</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_482" n="HIAT:w" s="T118">kanzittə</ts>
                  <nts id="Seg_483" n="HIAT:ip">"</nts>
                  <nts id="Seg_484" n="HIAT:ip">.</nts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_487" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_489" n="HIAT:w" s="T119">Dĭ</ts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_492" n="HIAT:w" s="T120">măndə:</ts>
                  <nts id="Seg_493" n="HIAT:ip">"</nts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_496" n="HIAT:w" s="T121">Nu</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_499" n="HIAT:w" s="T122">i</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_502" n="HIAT:w" s="T123">što</ts>
                  <nts id="Seg_503" n="HIAT:ip">,</nts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_506" n="HIAT:w" s="T124">măn</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_509" n="HIAT:w" s="T125">kalam</ts>
                  <nts id="Seg_510" n="HIAT:ip">"</nts>
                  <nts id="Seg_511" n="HIAT:ip">.</nts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_514" n="HIAT:u" s="T126">
                  <nts id="Seg_515" n="HIAT:ip">"</nts>
                  <ts e="T127" id="Seg_517" n="HIAT:w" s="T126">Dĭn</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_520" n="HIAT:w" s="T127">iʔgö</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_523" n="HIAT:w" s="T128">sʼvʼetogəʔi</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_526" n="HIAT:w" s="T129">bar</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_529" n="HIAT:w" s="T130">nĭŋgələl</ts>
                  <nts id="Seg_530" n="HIAT:ip">"</nts>
                  <nts id="Seg_531" n="HIAT:ip">.</nts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T135" id="Seg_534" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_536" n="HIAT:w" s="T131">Deʔpi</ts>
                  <nts id="Seg_537" n="HIAT:ip">,</nts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_540" n="HIAT:w" s="T132">öʔlubi</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_543" n="HIAT:w" s="T133">dĭm</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_546" n="HIAT:w" s="T134">dĭbər</ts>
                  <nts id="Seg_547" n="HIAT:ip">.</nts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T137" id="Seg_550" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_552" n="HIAT:w" s="T135">Vărotaʔi</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_555" n="HIAT:w" s="T136">kajluʔpiʔi</ts>
                  <nts id="Seg_556" n="HIAT:ip">.</nts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T145" id="Seg_559" n="HIAT:u" s="T137">
                  <nts id="Seg_560" n="HIAT:ip">(</nts>
                  <ts e="T138" id="Seg_562" n="HIAT:w" s="T137">Dĭm</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_565" n="HIAT:w" s="T138">dĭ</ts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_568" n="HIAT:w" s="T139">maʔtubi</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_571" n="HIAT:w" s="T140">ma-</ts>
                  <nts id="Seg_572" n="HIAT:ip">)</nts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_575" n="HIAT:w" s="T141">maːluʔpi</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_578" n="HIAT:w" s="T142">i</ts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_581" n="HIAT:w" s="T143">maʔndə</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_584" n="HIAT:w" s="T144">šobi</ts>
                  <nts id="Seg_585" n="HIAT:ip">.</nts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_588" n="HIAT:u" s="T145">
                  <nts id="Seg_589" n="HIAT:ip">(</nts>
                  <ts e="T146" id="Seg_591" n="HIAT:w" s="T145">Tej</ts>
                  <nts id="Seg_592" n="HIAT:ip">)</nts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_595" n="HIAT:w" s="T146">dʼala</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_598" n="HIAT:w" s="T147">mĭmbi</ts>
                  <nts id="Seg_599" n="HIAT:ip">,</nts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_602" n="HIAT:w" s="T148">mĭmbi</ts>
                  <nts id="Seg_603" n="HIAT:ip">.</nts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_606" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_608" n="HIAT:w" s="T149">Sʼvʼetogəʔi</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_611" n="HIAT:w" s="T150">üge</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_613" n="HIAT:ip">(</nts>
                  <ts e="T152" id="Seg_615" n="HIAT:w" s="T151">pʼaŋdona</ts>
                  <nts id="Seg_616" n="HIAT:ip">)</nts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_619" n="HIAT:w" s="T152">Malanʼnʼan</ts>
                  <nts id="Seg_620" n="HIAT:ip">,</nts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_623" n="HIAT:w" s="T153">ĭmbi</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_626" n="HIAT:w" s="T154">mĭndləj</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_629" n="HIAT:w" s="T155">üge</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_632" n="HIAT:w" s="T156">Malanʼnʼa</ts>
                  <nts id="Seg_633" n="HIAT:ip">.</nts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_636" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_638" n="HIAT:w" s="T157">Turanə</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_641" n="HIAT:w" s="T158">šobi</ts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_644" n="HIAT:w" s="T159">bar</ts>
                  <nts id="Seg_645" n="HIAT:ip">.</nts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T166" id="Seg_648" n="HIAT:u" s="T160">
                  <nts id="Seg_649" n="HIAT:ip">(</nts>
                  <ts e="T161" id="Seg_651" n="HIAT:w" s="T160">Bazaʔi</ts>
                  <nts id="Seg_652" n="HIAT:ip">)</nts>
                  <nts id="Seg_653" n="HIAT:ip">,</nts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_656" n="HIAT:w" s="T161">girgit</ts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_659" n="HIAT:w" s="T162">šamnagəʔi</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_662" n="HIAT:w" s="T163">üge</ts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_665" n="HIAT:w" s="T164">dĭn</ts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_668" n="HIAT:w" s="T165">malanʼnʼan</ts>
                  <nts id="Seg_669" n="HIAT:ip">.</nts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T170" id="Seg_672" n="HIAT:u" s="T166">
                  <ts e="T167" id="Seg_674" n="HIAT:w" s="T166">Dagaʔi</ts>
                  <nts id="Seg_675" n="HIAT:ip">,</nts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_677" n="HIAT:ip">(</nts>
                  <ts e="T168" id="Seg_679" n="HIAT:w" s="T167">am-</ts>
                  <nts id="Seg_680" n="HIAT:ip">)</nts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_683" n="HIAT:w" s="T168">dĭgəttə</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_686" n="HIAT:w" s="T169">amorbi</ts>
                  <nts id="Seg_687" n="HIAT:ip">.</nts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_690" n="HIAT:u" s="T170">
                  <nts id="Seg_691" n="HIAT:ip">(</nts>
                  <ts e="T171" id="Seg_693" n="HIAT:w" s="T170">Ambi</ts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_696" n="HIAT:w" s="T171">sʼera</ts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_699" n="HIAT:w" s="T172">măna</ts>
                  <nts id="Seg_700" n="HIAT:ip">)</nts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_703" n="HIAT:w" s="T173">sʼo</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_706" n="HIAT:w" s="T174">ravno</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_709" n="HIAT:w" s="T175">amnosʼtə</ts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_711" n="HIAT:ip">(</nts>
                  <ts e="T177" id="Seg_713" n="HIAT:w" s="T176">dĭn</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_716" n="HIAT:w" s="T177">tö</ts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_719" n="HIAT:w" s="T178">nuge</ts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_722" n="HIAT:w" s="T179">mona</ts>
                  <nts id="Seg_723" n="HIAT:ip">,</nts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_726" n="HIAT:w" s="T180">mona</ts>
                  <nts id="Seg_727" n="HIAT:ip">)</nts>
                  <nts id="Seg_728" n="HIAT:ip">.</nts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_731" n="HIAT:u" s="T181">
                  <ts e="T182" id="Seg_733" n="HIAT:w" s="T181">Dĭgəttə</ts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_736" n="HIAT:w" s="T182">iʔbəbi</ts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_739" n="HIAT:w" s="T183">kunolzittə</ts>
                  <nts id="Seg_740" n="HIAT:ip">.</nts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T187" id="Seg_743" n="HIAT:u" s="T184">
                  <ts e="T185" id="Seg_745" n="HIAT:w" s="T184">Nüdʼin</ts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_748" n="HIAT:w" s="T185">šobi</ts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_751" n="HIAT:w" s="T186">kuza</ts>
                  <nts id="Seg_752" n="HIAT:ip">.</nts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T192" id="Seg_755" n="HIAT:u" s="T187">
                  <nts id="Seg_756" n="HIAT:ip">"</nts>
                  <ts e="T188" id="Seg_758" n="HIAT:w" s="T187">Tăn</ts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_761" n="HIAT:w" s="T188">Malanʼnʼa</ts>
                  <nts id="Seg_762" n="HIAT:ip">,</nts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_765" n="HIAT:w" s="T189">tăn</ts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_768" n="HIAT:w" s="T190">măna</ts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_771" n="HIAT:w" s="T191">ajirial</ts>
                  <nts id="Seg_772" n="HIAT:ip">?</nts>
                  <nts id="Seg_773" n="HIAT:ip">"</nts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T201" id="Seg_776" n="HIAT:u" s="T192">
                  <nts id="Seg_777" n="HIAT:ip">"</nts>
                  <ts e="T193" id="Seg_779" n="HIAT:w" s="T192">A</ts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_782" n="HIAT:w" s="T193">măn</ts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_784" n="HIAT:ip">(</nts>
                  <ts e="T195" id="Seg_786" n="HIAT:w" s="T194">en</ts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_789" n="HIAT:w" s="T195">tĭm-</ts>
                  <nts id="Seg_790" n="HIAT:ip">)</nts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_793" n="HIAT:w" s="T196">ej</ts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_796" n="HIAT:w" s="T197">tĭmnem</ts>
                  <nts id="Seg_797" n="HIAT:ip">,</nts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_799" n="HIAT:ip">(</nts>
                  <ts e="T199" id="Seg_801" n="HIAT:w" s="T198">sin-</ts>
                  <nts id="Seg_802" n="HIAT:ip">)</nts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_805" n="HIAT:w" s="T199">šindi</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_808" n="HIAT:w" s="T200">tăn</ts>
                  <nts id="Seg_809" n="HIAT:ip">.</nts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T203" id="Seg_812" n="HIAT:u" s="T201">
                  <ts e="T202" id="Seg_814" n="HIAT:w" s="T201">Šoʔ</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_817" n="HIAT:w" s="T202">döbər</ts>
                  <nts id="Seg_818" n="HIAT:ip">"</nts>
                  <nts id="Seg_819" n="HIAT:ip">.</nts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T209" id="Seg_822" n="HIAT:u" s="T203">
                  <nts id="Seg_823" n="HIAT:ip">"</nts>
                  <ts e="T204" id="Seg_825" n="HIAT:w" s="T203">Dʼok</ts>
                  <nts id="Seg_826" n="HIAT:ip">,</nts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_829" n="HIAT:w" s="T204">măn</ts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_832" n="HIAT:w" s="T205">ej</ts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_835" n="HIAT:w" s="T206">šolam</ts>
                  <nts id="Seg_836" n="HIAT:ip">,</nts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_839" n="HIAT:w" s="T207">tăn</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_841" n="HIAT:ip">(</nts>
                  <ts e="T209" id="Seg_843" n="HIAT:w" s="T208">nereʔluʔlil</ts>
                  <nts id="Seg_844" n="HIAT:ip">)</nts>
                  <nts id="Seg_845" n="HIAT:ip">"</nts>
                  <nts id="Seg_846" n="HIAT:ip">.</nts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T211" id="Seg_849" n="HIAT:u" s="T209">
                  <ts e="T210" id="Seg_851" n="HIAT:w" s="T209">Dĭgəttə</ts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_854" n="HIAT:w" s="T210">kalla</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_857" n="HIAT:w" s="T396">dʼürbi</ts>
                  <nts id="Seg_858" n="HIAT:ip">.</nts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T214" id="Seg_861" n="HIAT:u" s="T211">
                  <ts e="T212" id="Seg_863" n="HIAT:w" s="T211">Dĭ</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_866" n="HIAT:w" s="T212">tenöbi</ts>
                  <nts id="Seg_867" n="HIAT:ip">,</nts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_870" n="HIAT:w" s="T213">tenöbi</ts>
                  <nts id="Seg_871" n="HIAT:ip">.</nts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T218" id="Seg_874" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_876" n="HIAT:w" s="T214">Dĭgəttə</ts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_879" n="HIAT:w" s="T215">šide</ts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_882" n="HIAT:w" s="T216">nüdʼi</ts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_885" n="HIAT:w" s="T217">šobi</ts>
                  <nts id="Seg_886" n="HIAT:ip">.</nts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T229" id="Seg_889" n="HIAT:u" s="T218">
                  <ts e="T219" id="Seg_891" n="HIAT:w" s="T218">Dĭ</ts>
                  <nts id="Seg_892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_894" n="HIAT:w" s="T219">bar</ts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_897" n="HIAT:w" s="T220">bünə</ts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_900" n="HIAT:w" s="T221">kambi</ts>
                  <nts id="Seg_901" n="HIAT:ip">,</nts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_904" n="HIAT:w" s="T222">kola</ts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_907" n="HIAT:w" s="T223">dĭn</ts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_910" n="HIAT:w" s="T224">i</ts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_913" n="HIAT:w" s="T225">to</ts>
                  <nts id="Seg_914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_916" n="HIAT:w" s="T226">pʼaŋdona</ts>
                  <nts id="Seg_917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_919" n="HIAT:w" s="T227">Malanʼnʼan</ts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_922" n="HIAT:w" s="T228">kola</ts>
                  <nts id="Seg_923" n="HIAT:ip">.</nts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T234" id="Seg_926" n="HIAT:u" s="T229">
                  <ts e="T230" id="Seg_928" n="HIAT:w" s="T229">Dĭgəttə</ts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_931" n="HIAT:w" s="T230">dĭ</ts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_934" n="HIAT:w" s="T231">bazoʔ</ts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_937" n="HIAT:w" s="T232">nüdʼit</ts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_940" n="HIAT:w" s="T233">šobi</ts>
                  <nts id="Seg_941" n="HIAT:ip">.</nts>
                  <nts id="Seg_942" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T242" id="Seg_944" n="HIAT:u" s="T234">
                  <ts e="T235" id="Seg_946" n="HIAT:w" s="T234">Tenöbi</ts>
                  <nts id="Seg_947" n="HIAT:ip">,</nts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_950" n="HIAT:w" s="T235">tenöbi</ts>
                  <nts id="Seg_951" n="HIAT:ip">,</nts>
                  <nts id="Seg_952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_954" n="HIAT:w" s="T236">dĭgəttə</ts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_957" n="HIAT:w" s="T237">măndə:</ts>
                  <nts id="Seg_958" n="HIAT:ip">"</nts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_961" n="HIAT:w" s="T238">Ej</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_964" n="HIAT:w" s="T239">tĭmnem</ts>
                  <nts id="Seg_965" n="HIAT:ip">,</nts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_968" n="HIAT:w" s="T240">šindi</ts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_971" n="HIAT:w" s="T241">tɨ</ts>
                  <nts id="Seg_972" n="HIAT:ip">.</nts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T245" id="Seg_975" n="HIAT:u" s="T242">
                  <ts e="T243" id="Seg_977" n="HIAT:w" s="T242">Šində</ts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_980" n="HIAT:w" s="T243">tăn</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_983" n="HIAT:w" s="T244">igel</ts>
                  <nts id="Seg_984" n="HIAT:ip">"</nts>
                  <nts id="Seg_985" n="HIAT:ip">.</nts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T252" id="Seg_988" n="HIAT:u" s="T245">
                  <nts id="Seg_989" n="HIAT:ip">"</nts>
                  <ts e="T246" id="Seg_991" n="HIAT:w" s="T245">Dʼok</ts>
                  <nts id="Seg_992" n="HIAT:ip">,</nts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_995" n="HIAT:w" s="T246">măn</ts>
                  <nts id="Seg_996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_998" n="HIAT:w" s="T247">em</ts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_1001" n="HIAT:w" s="T248">šoʔ</ts>
                  <nts id="Seg_1002" n="HIAT:ip">,</nts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_1005" n="HIAT:w" s="T249">tăn</ts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_1008" n="HIAT:w" s="T250">bazoʔ</ts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_1011" n="HIAT:w" s="T251">nereʔluʔpil</ts>
                  <nts id="Seg_1012" n="HIAT:ip">"</nts>
                  <nts id="Seg_1013" n="HIAT:ip">.</nts>
                  <nts id="Seg_1014" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T256" id="Seg_1016" n="HIAT:u" s="T252">
                  <ts e="T253" id="Seg_1018" n="HIAT:w" s="T252">Dĭgəttə</ts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_1021" n="HIAT:w" s="T253">nagur</ts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_1024" n="HIAT:w" s="T254">nüdʼi</ts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_1027" n="HIAT:w" s="T255">kambi</ts>
                  <nts id="Seg_1028" n="HIAT:ip">.</nts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T265" id="Seg_1031" n="HIAT:u" s="T256">
                  <nts id="Seg_1032" n="HIAT:ip">(</nts>
                  <ts e="T257" id="Seg_1034" n="HIAT:w" s="T256">Dĭm</ts>
                  <nts id="Seg_1035" n="HIAT:ip">)</nts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_1038" n="HIAT:w" s="T257">šobi</ts>
                  <nts id="Seg_1039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_1041" n="HIAT:w" s="T258">nagur</ts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1043" n="HIAT:ip">(</nts>
                  <ts e="T260" id="Seg_1045" n="HIAT:w" s="T259">lĭd</ts>
                  <nts id="Seg_1046" n="HIAT:ip">)</nts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_1049" n="HIAT:w" s="T260">nüdʼi</ts>
                  <nts id="Seg_1050" n="HIAT:ip">,</nts>
                  <nts id="Seg_1051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1053" n="HIAT:w" s="T261">dĭ</ts>
                  <nts id="Seg_1054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1056" n="HIAT:w" s="T262">bazoʔ</ts>
                  <nts id="Seg_1057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_1059" n="HIAT:w" s="T263">iʔbəbi</ts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_1062" n="HIAT:w" s="T264">kunolzittə</ts>
                  <nts id="Seg_1063" n="HIAT:ip">.</nts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T271" id="Seg_1066" n="HIAT:u" s="T265">
                  <ts e="T266" id="Seg_1068" n="HIAT:w" s="T265">Dĭ</ts>
                  <nts id="Seg_1069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1071" n="HIAT:w" s="T266">bazoʔ</ts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1074" n="HIAT:w" s="T267">šobi</ts>
                  <nts id="Seg_1075" n="HIAT:ip">,</nts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1078" n="HIAT:w" s="T268">surarlaʔbə:</ts>
                  <nts id="Seg_1079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1080" n="HIAT:ip">"</nts>
                  <ts e="T270" id="Seg_1082" n="HIAT:w" s="T269">No</ts>
                  <nts id="Seg_1083" n="HIAT:ip">,</nts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1086" n="HIAT:w" s="T270">tenöluʔpil</ts>
                  <nts id="Seg_1087" n="HIAT:ip">?</nts>
                  <nts id="Seg_1088" n="HIAT:ip">"</nts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T277" id="Seg_1091" n="HIAT:u" s="T271">
                  <nts id="Seg_1092" n="HIAT:ip">"</nts>
                  <ts e="T272" id="Seg_1094" n="HIAT:w" s="T271">Da</ts>
                  <nts id="Seg_1095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1097" n="HIAT:w" s="T272">šoʔ</ts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1100" n="HIAT:w" s="T273">döber</ts>
                  <nts id="Seg_1101" n="HIAT:ip">,</nts>
                  <nts id="Seg_1102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1104" n="HIAT:w" s="T274">măn</ts>
                  <nts id="Seg_1105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1107" n="HIAT:w" s="T275">măndərlim</ts>
                  <nts id="Seg_1108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1110" n="HIAT:w" s="T276">tănan</ts>
                  <nts id="Seg_1111" n="HIAT:ip">"</nts>
                  <nts id="Seg_1112" n="HIAT:ip">.</nts>
                  <nts id="Seg_1113" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T280" id="Seg_1115" n="HIAT:u" s="T277">
                  <nts id="Seg_1116" n="HIAT:ip">"</nts>
                  <ts e="T278" id="Seg_1118" n="HIAT:w" s="T277">Da</ts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1121" n="HIAT:w" s="T278">tăn</ts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1124" n="HIAT:w" s="T279">nereʔluʔlil</ts>
                  <nts id="Seg_1125" n="HIAT:ip">"</nts>
                  <nts id="Seg_1126" n="HIAT:ip">.</nts>
                  <nts id="Seg_1127" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T284" id="Seg_1129" n="HIAT:u" s="T280">
                  <ts e="T281" id="Seg_1131" n="HIAT:w" s="T280">Dĭ</ts>
                  <nts id="Seg_1132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1134" n="HIAT:w" s="T281">măndə:</ts>
                  <nts id="Seg_1135" n="HIAT:ip">"</nts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1138" n="HIAT:w" s="T282">Ej</ts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1141" n="HIAT:w" s="T283">nereʔluʔlim</ts>
                  <nts id="Seg_1142" n="HIAT:ip">"</nts>
                  <nts id="Seg_1143" n="HIAT:ip">.</nts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T287" id="Seg_1146" n="HIAT:u" s="T284">
                  <ts e="T285" id="Seg_1148" n="HIAT:w" s="T284">Dĭ</ts>
                  <nts id="Seg_1149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1151" n="HIAT:w" s="T285">šobi</ts>
                  <nts id="Seg_1152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1154" n="HIAT:w" s="T286">turanə</ts>
                  <nts id="Seg_1155" n="HIAT:ip">.</nts>
                  <nts id="Seg_1156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T290" id="Seg_1158" n="HIAT:u" s="T287">
                  <ts e="T288" id="Seg_1160" n="HIAT:w" s="T287">Dĭ</ts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1163" n="HIAT:w" s="T288">ej</ts>
                  <nts id="Seg_1164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1166" n="HIAT:w" s="T289">nereʔluʔpi</ts>
                  <nts id="Seg_1167" n="HIAT:ip">.</nts>
                  <nts id="Seg_1168" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T293" id="Seg_1170" n="HIAT:u" s="T290">
                  <ts e="T291" id="Seg_1172" n="HIAT:w" s="T290">Mănlia:</ts>
                  <nts id="Seg_1173" n="HIAT:ip">"</nts>
                  <nts id="Seg_1174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1176" n="HIAT:w" s="T291">Amnolam</ts>
                  <nts id="Seg_1177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1179" n="HIAT:w" s="T292">dĭzi</ts>
                  <nts id="Seg_1180" n="HIAT:ip">"</nts>
                  <nts id="Seg_1181" n="HIAT:ip">.</nts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T304" id="Seg_1184" n="HIAT:u" s="T293">
                  <ts e="T294" id="Seg_1186" n="HIAT:w" s="T293">Ĭmbi</ts>
                  <nts id="Seg_1187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1188" n="HIAT:ip">(</nts>
                  <ts e="T295" id="Seg_1190" n="HIAT:w" s="T294">sʼera</ts>
                  <nts id="Seg_1191" n="HIAT:ip">)</nts>
                  <nts id="Seg_1192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1194" n="HIAT:w" s="T295">pušaj</ts>
                  <nts id="Seg_1195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1197" n="HIAT:w" s="T296">ej</ts>
                  <nts id="Seg_1198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1200" n="HIAT:w" s="T297">kuvas</ts>
                  <nts id="Seg_1201" n="HIAT:ip">,</nts>
                  <nts id="Seg_1202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1204" n="HIAT:w" s="T298">da</ts>
                  <nts id="Seg_1205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1207" n="HIAT:w" s="T299">dön</ts>
                  <nts id="Seg_1208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1210" n="HIAT:w" s="T300">bar</ts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1213" n="HIAT:w" s="T301">măna</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1216" n="HIAT:w" s="T302">i</ts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1219" n="HIAT:w" s="T303">măna</ts>
                  <nts id="Seg_1220" n="HIAT:ip">.</nts>
                  <nts id="Seg_1221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T312" id="Seg_1223" n="HIAT:u" s="T304">
                  <ts e="T305" id="Seg_1225" n="HIAT:w" s="T304">Dĭgəttə</ts>
                  <nts id="Seg_1226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1228" n="HIAT:w" s="T305">amnobi</ts>
                  <nts id="Seg_1229" n="HIAT:ip">,</nts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1232" n="HIAT:w" s="T306">amnobi</ts>
                  <nts id="Seg_1233" n="HIAT:ip">,</nts>
                  <nts id="Seg_1234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1236" n="HIAT:w" s="T307">măndə:</ts>
                  <nts id="Seg_1237" n="HIAT:ip">"</nts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1240" n="HIAT:w" s="T308">Măn</ts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1243" n="HIAT:w" s="T309">kalam</ts>
                  <nts id="Seg_1244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1245" n="HIAT:ip">(</nts>
                  <ts e="T311" id="Seg_1247" n="HIAT:w" s="T310">abats-</ts>
                  <nts id="Seg_1248" n="HIAT:ip">)</nts>
                  <nts id="Seg_1249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1251" n="HIAT:w" s="T311">abanə</ts>
                  <nts id="Seg_1252" n="HIAT:ip">"</nts>
                  <nts id="Seg_1253" n="HIAT:ip">.</nts>
                  <nts id="Seg_1254" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T317" id="Seg_1256" n="HIAT:u" s="T312">
                  <ts e="T313" id="Seg_1258" n="HIAT:w" s="T312">Dĭ</ts>
                  <nts id="Seg_1259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1261" n="HIAT:w" s="T313">dĭʔnə</ts>
                  <nts id="Seg_1262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1263" n="HIAT:ip">(</nts>
                  <ts e="T315" id="Seg_1265" n="HIAT:w" s="T314">k-</ts>
                  <nts id="Seg_1266" n="HIAT:ip">)</nts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1269" n="HIAT:w" s="T315">kălʼso</ts>
                  <nts id="Seg_1270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1272" n="HIAT:w" s="T316">mĭbi</ts>
                  <nts id="Seg_1273" n="HIAT:ip">.</nts>
                  <nts id="Seg_1274" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T319" id="Seg_1276" n="HIAT:u" s="T317">
                  <nts id="Seg_1277" n="HIAT:ip">"</nts>
                  <ts e="T318" id="Seg_1279" n="HIAT:w" s="T317">No</ts>
                  <nts id="Seg_1280" n="HIAT:ip">,</nts>
                  <nts id="Seg_1281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1283" n="HIAT:w" s="T318">kanaʔ</ts>
                  <nts id="Seg_1284" n="HIAT:ip">.</nts>
                  <nts id="Seg_1285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T330" id="Seg_1287" n="HIAT:u" s="T319">
                  <ts e="T320" id="Seg_1289" n="HIAT:w" s="T319">Kamen</ts>
                  <nts id="Seg_1290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1292" n="HIAT:w" s="T320">dĭ</ts>
                  <nts id="Seg_1293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1295" n="HIAT:w" s="T321">kălʼso</ts>
                  <nts id="Seg_1296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1298" n="HIAT:w" s="T322">sagər</ts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1301" n="HIAT:w" s="T323">moləj</ts>
                  <nts id="Seg_1302" n="HIAT:ip">,</nts>
                  <nts id="Seg_1303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1305" n="HIAT:w" s="T324">dĭgəttə</ts>
                  <nts id="Seg_1306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1308" n="HIAT:w" s="T325">büžü</ts>
                  <nts id="Seg_1309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1311" n="HIAT:w" s="T326">šoʔ</ts>
                  <nts id="Seg_1312" n="HIAT:ip">,</nts>
                  <nts id="Seg_1313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327.tx.1" id="Seg_1315" n="HIAT:w" s="T327">a</ts>
                  <nts id="Seg_1316" n="HIAT:ip">_</nts>
                  <ts e="T328" id="Seg_1318" n="HIAT:w" s="T327.tx.1">to</ts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1321" n="HIAT:w" s="T328">măn</ts>
                  <nts id="Seg_1322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1324" n="HIAT:w" s="T329">külalləm</ts>
                  <nts id="Seg_1325" n="HIAT:ip">"</nts>
                  <nts id="Seg_1326" n="HIAT:ip">.</nts>
                  <nts id="Seg_1327" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T333" id="Seg_1329" n="HIAT:u" s="T330">
                  <nts id="Seg_1330" n="HIAT:ip">(</nts>
                  <ts e="T331" id="Seg_1332" n="HIAT:w" s="T330">Dĭ</ts>
                  <nts id="Seg_1333" n="HIAT:ip">)</nts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1336" n="HIAT:w" s="T331">kambi</ts>
                  <nts id="Seg_1337" n="HIAT:ip">,</nts>
                  <nts id="Seg_1338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1340" n="HIAT:w" s="T332">sʼestrattə</ts>
                  <nts id="Seg_1341" n="HIAT:ip">.</nts>
                  <nts id="Seg_1342" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T339" id="Seg_1344" n="HIAT:u" s="T333">
                  <ts e="T334" id="Seg_1346" n="HIAT:w" s="T333">Bar</ts>
                  <nts id="Seg_1347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1349" n="HIAT:w" s="T334">dĭʔnə</ts>
                  <nts id="Seg_1350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1351" n="HIAT:ip">(</nts>
                  <ts e="T336" id="Seg_1353" n="HIAT:w" s="T335">dĭgət-</ts>
                  <nts id="Seg_1354" n="HIAT:ip">)</nts>
                  <nts id="Seg_1355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1357" n="HIAT:w" s="T336">bü</ts>
                  <nts id="Seg_1358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1360" n="HIAT:w" s="T337">girgitdə</ts>
                  <nts id="Seg_1361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1363" n="HIAT:w" s="T338">mĭbi</ts>
                  <nts id="Seg_1364" n="HIAT:ip">.</nts>
                  <nts id="Seg_1365" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T344" id="Seg_1367" n="HIAT:u" s="T339">
                  <ts e="T340" id="Seg_1369" n="HIAT:w" s="T339">Tĭ</ts>
                  <nts id="Seg_1370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1372" n="HIAT:w" s="T340">bar</ts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1375" n="HIAT:w" s="T341">kunolbi</ts>
                  <nts id="Seg_1376" n="HIAT:ip">,</nts>
                  <nts id="Seg_1377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1379" n="HIAT:w" s="T342">kunolbi</ts>
                  <nts id="Seg_1380" n="HIAT:ip">,</nts>
                  <nts id="Seg_1381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1383" n="HIAT:w" s="T343">uʔbdəbi</ts>
                  <nts id="Seg_1384" n="HIAT:ip">.</nts>
                  <nts id="Seg_1385" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T348" id="Seg_1387" n="HIAT:u" s="T344">
                  <ts e="T345" id="Seg_1389" n="HIAT:w" s="T344">Kălʼečkăt</ts>
                  <nts id="Seg_1390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1392" n="HIAT:w" s="T345">bar</ts>
                  <nts id="Seg_1393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1395" n="HIAT:w" s="T346">sagər</ts>
                  <nts id="Seg_1396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1398" n="HIAT:w" s="T347">dĭ</ts>
                  <nts id="Seg_1399" n="HIAT:ip">.</nts>
                  <nts id="Seg_1400" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T351" id="Seg_1402" n="HIAT:u" s="T348">
                  <ts e="T349" id="Seg_1404" n="HIAT:w" s="T348">Bar</ts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1407" n="HIAT:w" s="T349">ĭmbi</ts>
                  <nts id="Seg_1408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1410" n="HIAT:w" s="T350">baruʔpi</ts>
                  <nts id="Seg_1411" n="HIAT:ip">.</nts>
                  <nts id="Seg_1412" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T357" id="Seg_1414" n="HIAT:u" s="T351">
                  <ts e="T352" id="Seg_1416" n="HIAT:w" s="T351">I</ts>
                  <nts id="Seg_1417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1419" n="HIAT:w" s="T352">kambi</ts>
                  <nts id="Seg_1420" n="HIAT:ip">;</nts>
                  <nts id="Seg_1421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1423" n="HIAT:w" s="T353">šobi</ts>
                  <nts id="Seg_1424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1426" n="HIAT:w" s="T354">dĭbər:</ts>
                  <nts id="Seg_1427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1429" n="HIAT:w" s="T355">iʔbolaʔbə</ts>
                  <nts id="Seg_1430" n="HIAT:ip">,</nts>
                  <nts id="Seg_1431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1433" n="HIAT:w" s="T356">külambi</ts>
                  <nts id="Seg_1434" n="HIAT:ip">.</nts>
                  <nts id="Seg_1435" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T359" id="Seg_1437" n="HIAT:u" s="T357">
                  <ts e="T358" id="Seg_1439" n="HIAT:w" s="T357">Bar</ts>
                  <nts id="Seg_1440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1442" n="HIAT:w" s="T358">dʼorbi</ts>
                  <nts id="Seg_1443" n="HIAT:ip">.</nts>
                  <nts id="Seg_1444" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T363" id="Seg_1446" n="HIAT:u" s="T359">
                  <nts id="Seg_1447" n="HIAT:ip">"</nts>
                  <ts e="T360" id="Seg_1449" n="HIAT:w" s="T359">Măn</ts>
                  <nts id="Seg_1450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1452" n="HIAT:w" s="T360">tănan</ts>
                  <nts id="Seg_1453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1455" n="HIAT:w" s="T361">ajirliom</ts>
                  <nts id="Seg_1456" n="HIAT:ip">,</nts>
                  <nts id="Seg_1457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1459" n="HIAT:w" s="T362">uʔbdaʔ</ts>
                  <nts id="Seg_1460" n="HIAT:ip">"</nts>
                  <nts id="Seg_1461" n="HIAT:ip">.</nts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T365" id="Seg_1464" n="HIAT:u" s="T363">
                  <nts id="Seg_1465" n="HIAT:ip">"</nts>
                  <ts e="T364" id="Seg_1467" n="HIAT:w" s="T363">Ajirliom</ts>
                  <nts id="Seg_1468" n="HIAT:ip">,</nts>
                  <nts id="Seg_1469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1471" n="HIAT:w" s="T364">uʔbdaʔ</ts>
                  <nts id="Seg_1472" n="HIAT:ip">"</nts>
                  <nts id="Seg_1473" n="HIAT:ip">.</nts>
                  <nts id="Seg_1474" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T367" id="Seg_1476" n="HIAT:u" s="T365">
                  <ts e="T366" id="Seg_1478" n="HIAT:w" s="T365">Dĭ</ts>
                  <nts id="Seg_1479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1481" n="HIAT:w" s="T366">uʔbdəbi</ts>
                  <nts id="Seg_1482" n="HIAT:ip">.</nts>
                  <nts id="Seg_1483" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T372" id="Seg_1485" n="HIAT:u" s="T367">
                  <ts e="T368" id="Seg_1487" n="HIAT:w" s="T367">I</ts>
                  <nts id="Seg_1488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1490" n="HIAT:w" s="T368">ugandə</ts>
                  <nts id="Seg_1491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1493" n="HIAT:w" s="T369">kuvas</ts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1496" n="HIAT:w" s="T370">nʼi</ts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1499" n="HIAT:w" s="T371">molambi</ts>
                  <nts id="Seg_1500" n="HIAT:ip">.</nts>
                  <nts id="Seg_1501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T376" id="Seg_1503" n="HIAT:u" s="T372">
                  <ts e="T373" id="Seg_1505" n="HIAT:w" s="T372">Dĭgəttə</ts>
                  <nts id="Seg_1506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1508" n="HIAT:w" s="T373">koŋgoroʔi</ts>
                  <nts id="Seg_1509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1511" n="HIAT:w" s="T374">bar</ts>
                  <nts id="Seg_1512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1514" n="HIAT:w" s="T375">küzürleʔbəʔjə</ts>
                  <nts id="Seg_1515" n="HIAT:ip">.</nts>
                  <nts id="Seg_1516" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T378" id="Seg_1518" n="HIAT:u" s="T376">
                  <ts e="T377" id="Seg_1520" n="HIAT:w" s="T376">Il</ts>
                  <nts id="Seg_1521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1523" n="HIAT:w" s="T377">mĭlleʔbəʔjə</ts>
                  <nts id="Seg_1524" n="HIAT:ip">.</nts>
                  <nts id="Seg_1525" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T386" id="Seg_1527" n="HIAT:u" s="T378">
                  <ts e="T379" id="Seg_1529" n="HIAT:w" s="T378">Molambi</ts>
                  <nts id="Seg_1530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1532" n="HIAT:w" s="T379">gorăt</ts>
                  <nts id="Seg_1533" n="HIAT:ip">,</nts>
                  <nts id="Seg_1534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1536" n="HIAT:w" s="T380">a</ts>
                  <nts id="Seg_1537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1538" n="HIAT:ip">(</nts>
                  <ts e="T382" id="Seg_1540" n="HIAT:w" s="T381">dĭ=</ts>
                  <nts id="Seg_1541" n="HIAT:ip">)</nts>
                  <nts id="Seg_1542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1544" n="HIAT:w" s="T382">dĭ</ts>
                  <nts id="Seg_1545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1547" n="HIAT:w" s="T383">nʼi</ts>
                  <nts id="Seg_1548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1550" n="HIAT:w" s="T384">koŋ</ts>
                  <nts id="Seg_1551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1553" n="HIAT:w" s="T385">molambi</ts>
                  <nts id="Seg_1554" n="HIAT:ip">.</nts>
                  <nts id="Seg_1555" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T392" id="Seg_1557" n="HIAT:u" s="T386">
                  <ts e="T387" id="Seg_1559" n="HIAT:w" s="T386">Dĭgəttə</ts>
                  <nts id="Seg_1560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1562" n="HIAT:w" s="T387">dĭ</ts>
                  <nts id="Seg_1563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1564" n="HIAT:ip">(</nts>
                  <ts e="T389" id="Seg_1566" n="HIAT:w" s="T388">dĭʔn-</ts>
                  <nts id="Seg_1567" n="HIAT:ip">)</nts>
                  <nts id="Seg_1568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1570" n="HIAT:w" s="T389">dĭm</ts>
                  <nts id="Seg_1571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1573" n="HIAT:w" s="T390">tibinə</ts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1576" n="HIAT:w" s="T391">ibi</ts>
                  <nts id="Seg_1577" n="HIAT:ip">.</nts>
                  <nts id="Seg_1578" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T395" id="Seg_1580" n="HIAT:u" s="T392">
                  <ts e="T393" id="Seg_1582" n="HIAT:w" s="T392">I</ts>
                  <nts id="Seg_1583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1585" n="HIAT:w" s="T393">stalʼi</ts>
                  <nts id="Seg_1586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1588" n="HIAT:w" s="T394">amnozittə</ts>
                  <nts id="Seg_1589" n="HIAT:ip">.</nts>
                  <nts id="Seg_1590" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T395" id="Seg_1591" n="sc" s="T0">
               <ts e="T1" id="Seg_1593" n="e" s="T0">Amnobi </ts>
               <ts e="T2" id="Seg_1595" n="e" s="T1">büzʼe. </ts>
               <ts e="T3" id="Seg_1597" n="e" s="T2">(Dĭn </ts>
               <ts e="T4" id="Seg_1599" n="e" s="T3">bɨl </ts>
               <ts e="T5" id="Seg_1601" n="e" s="T4">na-) </ts>
               <ts e="T6" id="Seg_1603" n="e" s="T5">Dĭn </ts>
               <ts e="T7" id="Seg_1605" n="e" s="T6">ibiʔi </ts>
               <ts e="T8" id="Seg_1607" n="e" s="T7">nagur </ts>
               <ts e="T9" id="Seg_1609" n="e" s="T8">koʔbdozaŋdə. </ts>
               <ts e="T10" id="Seg_1611" n="e" s="T9">Dĭ </ts>
               <ts e="T11" id="Seg_1613" n="e" s="T10">stal </ts>
               <ts e="T12" id="Seg_1615" n="e" s="T11">(ob-) </ts>
               <ts e="T13" id="Seg_1617" n="e" s="T12">oʔbdəsʼtə </ts>
               <ts e="T14" id="Seg_1619" n="e" s="T13">gorăttə </ts>
               <ts e="T15" id="Seg_1621" n="e" s="T14">kanzittə. </ts>
               <ts e="T16" id="Seg_1623" n="e" s="T15">Urgo </ts>
               <ts e="T17" id="Seg_1625" n="e" s="T16">koʔbdondə </ts>
               <ts e="T18" id="Seg_1627" n="e" s="T17">măndə:" </ts>
               <ts e="T19" id="Seg_1629" n="e" s="T18">Ĭmbi </ts>
               <ts e="T20" id="Seg_1631" n="e" s="T19">tănan </ts>
               <ts e="T21" id="Seg_1633" n="e" s="T20">izittə?" </ts>
               <ts e="T22" id="Seg_1635" n="e" s="T21">"Tăn </ts>
               <ts e="T23" id="Seg_1637" n="e" s="T22">măna </ts>
               <ts e="T24" id="Seg_1639" n="e" s="T23">kuvas </ts>
               <ts e="T25" id="Seg_1641" n="e" s="T24">platʼtʼa </ts>
               <ts e="T26" id="Seg_1643" n="e" s="T25">iʔ". </ts>
               <ts e="T27" id="Seg_1645" n="e" s="T26">A </ts>
               <ts e="T28" id="Seg_1647" n="e" s="T27">üdʼüget </ts>
               <ts e="T29" id="Seg_1649" n="e" s="T28">koʔbdo: </ts>
               <ts e="T30" id="Seg_1651" n="e" s="T29">"Tăn </ts>
               <ts e="T31" id="Seg_1653" n="e" s="T30">măna </ts>
               <ts e="T32" id="Seg_1655" n="e" s="T31">bătinkaʔi </ts>
               <ts e="T33" id="Seg_1657" n="e" s="T32">kuvas </ts>
               <ts e="T34" id="Seg_1659" n="e" s="T33">iʔ". </ts>
               <ts e="T35" id="Seg_1661" n="e" s="T34">A </ts>
               <ts e="T36" id="Seg_1663" n="e" s="T35">nagur, </ts>
               <ts e="T37" id="Seg_1665" n="e" s="T36">išo </ts>
               <ts e="T38" id="Seg_1667" n="e" s="T37">üdʼüge </ts>
               <ts e="T39" id="Seg_1669" n="e" s="T38">koʔbdo:" </ts>
               <ts e="T40" id="Seg_1671" n="e" s="T39">Tănan </ts>
               <ts e="T41" id="Seg_1673" n="e" s="T40">ĭmbi </ts>
               <ts e="T42" id="Seg_1675" n="e" s="T41">izittə?" </ts>
               <ts e="T43" id="Seg_1677" n="e" s="T42">"Tăn </ts>
               <ts e="T44" id="Seg_1679" n="e" s="T43">măna </ts>
               <ts e="T45" id="Seg_1681" n="e" s="T44">iʔ </ts>
               <ts e="T46" id="Seg_1683" n="e" s="T45">kömə </ts>
               <ts e="T47" id="Seg_1685" n="e" s="T46">sʼvʼetok". </ts>
               <ts e="T48" id="Seg_1687" n="e" s="T47">Dĭ </ts>
               <ts e="T49" id="Seg_1689" n="e" s="T48">kambi. </ts>
               <ts e="T50" id="Seg_1691" n="e" s="T49">Măndərbi, </ts>
               <ts e="T51" id="Seg_1693" n="e" s="T50">măndərbi. </ts>
               <ts e="T52" id="Seg_1695" n="e" s="T51">Ibi </ts>
               <ts e="T53" id="Seg_1697" n="e" s="T52">döʔnə </ts>
               <ts e="T54" id="Seg_1699" n="e" s="T53">platʼtʼa, </ts>
               <ts e="T55" id="Seg_1701" n="e" s="T54">onʼiʔtə </ts>
               <ts e="T56" id="Seg_1703" n="e" s="T55">ibi </ts>
               <ts e="T57" id="Seg_1705" n="e" s="T56">bătinkaʔi. </ts>
               <ts e="T58" id="Seg_1707" n="e" s="T57">A </ts>
               <ts e="T59" id="Seg_1709" n="e" s="T58">(sʼvet-) </ts>
               <ts e="T60" id="Seg_1711" n="e" s="T59">sʼvʼetok </ts>
               <ts e="T61" id="Seg_1713" n="e" s="T60">naga. </ts>
               <ts e="T62" id="Seg_1715" n="e" s="T61">Dĭgəttə </ts>
               <ts e="T63" id="Seg_1717" n="e" s="T62">šonəga </ts>
               <ts e="T64" id="Seg_1719" n="e" s="T63">i </ts>
               <ts e="T65" id="Seg_1721" n="e" s="T64">tenölaʔbə: </ts>
               <ts e="T66" id="Seg_1723" n="e" s="T65">gijen </ts>
               <ts e="T67" id="Seg_1725" n="e" s="T66">(i-) </ts>
               <ts e="T68" id="Seg_1727" n="e" s="T67">izittə </ts>
               <ts e="T69" id="Seg_1729" n="e" s="T68">kömə </ts>
               <ts e="T70" id="Seg_1731" n="e" s="T69">sʼvʼetok? </ts>
               <ts e="T71" id="Seg_1733" n="e" s="T70">Šonəga, </ts>
               <ts e="T72" id="Seg_1735" n="e" s="T71">kubi </ts>
               <ts e="T73" id="Seg_1737" n="e" s="T72">bar </ts>
               <ts e="T74" id="Seg_1739" n="e" s="T73">košideŋgən </ts>
               <ts e="T75" id="Seg_1741" n="e" s="T74">sʼvʼetogəʔi. </ts>
               <ts e="T76" id="Seg_1743" n="e" s="T75">Dĭbər </ts>
               <ts e="T77" id="Seg_1745" n="e" s="T76">kambi, </ts>
               <ts e="T78" id="Seg_1747" n="e" s="T77">nĭŋgəbi. </ts>
               <ts e="T79" id="Seg_1749" n="e" s="T78">Vărotaʔi </ts>
               <ts e="T80" id="Seg_1751" n="e" s="T79">(kau-) </ts>
               <ts e="T81" id="Seg_1753" n="e" s="T80">(kajluʔpiʔi). </ts>
               <ts e="T82" id="Seg_1755" n="e" s="T81">Dĭgəttə </ts>
               <ts e="T83" id="Seg_1757" n="e" s="T82">mĭmbi, </ts>
               <ts e="T84" id="Seg_1759" n="e" s="T83">mĭmbi </ts>
               <ts e="T85" id="Seg_1761" n="e" s="T84">(dunʼtʼigə). </ts>
               <ts e="T86" id="Seg_1763" n="e" s="T85">Nüdʼin </ts>
               <ts e="T87" id="Seg_1765" n="e" s="T86">šobi </ts>
               <ts e="T88" id="Seg_1767" n="e" s="T87">kuza. </ts>
               <ts e="T89" id="Seg_1769" n="e" s="T88">|(Bot-) </ts>
               <ts e="T90" id="Seg_1771" n="e" s="T89">Boslə </ts>
               <ts e="T91" id="Seg_1773" n="e" s="T90">koʔbdol </ts>
               <ts e="T92" id="Seg_1775" n="e" s="T91">mĭləl </ts>
               <ts e="T93" id="Seg_1777" n="e" s="T92">măna </ts>
               <ts e="T94" id="Seg_1779" n="e" s="T93">(tĭbi=) </ts>
               <ts e="T95" id="Seg_1781" n="e" s="T94">detlil </ts>
               <ts e="T96" id="Seg_1783" n="e" s="T95">döbər". </ts>
               <ts e="T97" id="Seg_1785" n="e" s="T96">Dĭ </ts>
               <ts e="T98" id="Seg_1787" n="e" s="T97">tenöbi, </ts>
               <ts e="T99" id="Seg_1789" n="e" s="T98">tenöbi. </ts>
               <ts e="T100" id="Seg_1791" n="e" s="T99">"Detlim!" </ts>
               <ts e="T101" id="Seg_1793" n="e" s="T100">Dĭgəttə </ts>
               <ts e="T102" id="Seg_1795" n="e" s="T101">erten </ts>
               <ts e="T103" id="Seg_1797" n="e" s="T102">uʔbdəbi. </ts>
               <ts e="T104" id="Seg_1799" n="e" s="T103">Vărotatsi </ts>
               <ts e="T105" id="Seg_1801" n="e" s="T104">(kajluʔ- </ts>
               <ts e="T106" id="Seg_1803" n="e" s="T105">kajl-) </ts>
               <ts e="T107" id="Seg_1805" n="e" s="T106">kajluʔpiʔi. </ts>
               <ts e="T108" id="Seg_1807" n="e" s="T107">I </ts>
               <ts e="T109" id="Seg_1809" n="e" s="T108">šobi </ts>
               <ts e="T110" id="Seg_1811" n="e" s="T109">maːʔndə. </ts>
               <ts e="T111" id="Seg_1813" n="e" s="T110">Deʔpi, </ts>
               <ts e="T112" id="Seg_1815" n="e" s="T111">bar </ts>
               <ts e="T113" id="Seg_1817" n="e" s="T112">mĭbi. </ts>
               <ts e="T114" id="Seg_1819" n="e" s="T113">(Nagurgö). </ts>
               <ts e="T115" id="Seg_1821" n="e" s="T114">"A </ts>
               <ts e="T116" id="Seg_1823" n="e" s="T115">tănan </ts>
               <ts e="T117" id="Seg_1825" n="e" s="T116">nada </ts>
               <ts e="T118" id="Seg_1827" n="e" s="T117">dĭbər </ts>
               <ts e="T119" id="Seg_1829" n="e" s="T118">kanzittə". </ts>
               <ts e="T120" id="Seg_1831" n="e" s="T119">Dĭ </ts>
               <ts e="T121" id="Seg_1833" n="e" s="T120">măndə:" </ts>
               <ts e="T122" id="Seg_1835" n="e" s="T121">Nu </ts>
               <ts e="T123" id="Seg_1837" n="e" s="T122">i </ts>
               <ts e="T124" id="Seg_1839" n="e" s="T123">što, </ts>
               <ts e="T125" id="Seg_1841" n="e" s="T124">măn </ts>
               <ts e="T126" id="Seg_1843" n="e" s="T125">kalam". </ts>
               <ts e="T127" id="Seg_1845" n="e" s="T126">"Dĭn </ts>
               <ts e="T128" id="Seg_1847" n="e" s="T127">iʔgö </ts>
               <ts e="T129" id="Seg_1849" n="e" s="T128">sʼvʼetogəʔi </ts>
               <ts e="T130" id="Seg_1851" n="e" s="T129">bar </ts>
               <ts e="T131" id="Seg_1853" n="e" s="T130">nĭŋgələl". </ts>
               <ts e="T132" id="Seg_1855" n="e" s="T131">Deʔpi, </ts>
               <ts e="T133" id="Seg_1857" n="e" s="T132">öʔlubi </ts>
               <ts e="T134" id="Seg_1859" n="e" s="T133">dĭm </ts>
               <ts e="T135" id="Seg_1861" n="e" s="T134">dĭbər. </ts>
               <ts e="T136" id="Seg_1863" n="e" s="T135">Vărotaʔi </ts>
               <ts e="T137" id="Seg_1865" n="e" s="T136">kajluʔpiʔi. </ts>
               <ts e="T138" id="Seg_1867" n="e" s="T137">(Dĭm </ts>
               <ts e="T139" id="Seg_1869" n="e" s="T138">dĭ </ts>
               <ts e="T140" id="Seg_1871" n="e" s="T139">maʔtubi </ts>
               <ts e="T141" id="Seg_1873" n="e" s="T140">ma-) </ts>
               <ts e="T142" id="Seg_1875" n="e" s="T141">maːluʔpi </ts>
               <ts e="T143" id="Seg_1877" n="e" s="T142">i </ts>
               <ts e="T144" id="Seg_1879" n="e" s="T143">maʔndə </ts>
               <ts e="T145" id="Seg_1881" n="e" s="T144">šobi. </ts>
               <ts e="T146" id="Seg_1883" n="e" s="T145">(Tej) </ts>
               <ts e="T147" id="Seg_1885" n="e" s="T146">dʼala </ts>
               <ts e="T148" id="Seg_1887" n="e" s="T147">mĭmbi, </ts>
               <ts e="T149" id="Seg_1889" n="e" s="T148">mĭmbi. </ts>
               <ts e="T150" id="Seg_1891" n="e" s="T149">Sʼvʼetogəʔi </ts>
               <ts e="T151" id="Seg_1893" n="e" s="T150">üge </ts>
               <ts e="T152" id="Seg_1895" n="e" s="T151">(pʼaŋdona) </ts>
               <ts e="T153" id="Seg_1897" n="e" s="T152">Malanʼnʼan, </ts>
               <ts e="T154" id="Seg_1899" n="e" s="T153">ĭmbi </ts>
               <ts e="T155" id="Seg_1901" n="e" s="T154">mĭndləj </ts>
               <ts e="T156" id="Seg_1903" n="e" s="T155">üge </ts>
               <ts e="T157" id="Seg_1905" n="e" s="T156">Malanʼnʼa. </ts>
               <ts e="T158" id="Seg_1907" n="e" s="T157">Turanə </ts>
               <ts e="T159" id="Seg_1909" n="e" s="T158">šobi </ts>
               <ts e="T160" id="Seg_1911" n="e" s="T159">bar. </ts>
               <ts e="T161" id="Seg_1913" n="e" s="T160">(Bazaʔi), </ts>
               <ts e="T162" id="Seg_1915" n="e" s="T161">girgit </ts>
               <ts e="T163" id="Seg_1917" n="e" s="T162">šamnagəʔi </ts>
               <ts e="T164" id="Seg_1919" n="e" s="T163">üge </ts>
               <ts e="T165" id="Seg_1921" n="e" s="T164">dĭn </ts>
               <ts e="T166" id="Seg_1923" n="e" s="T165">malanʼnʼan. </ts>
               <ts e="T167" id="Seg_1925" n="e" s="T166">Dagaʔi, </ts>
               <ts e="T168" id="Seg_1927" n="e" s="T167">(am-) </ts>
               <ts e="T169" id="Seg_1929" n="e" s="T168">dĭgəttə </ts>
               <ts e="T170" id="Seg_1931" n="e" s="T169">amorbi. </ts>
               <ts e="T171" id="Seg_1933" n="e" s="T170">(Ambi </ts>
               <ts e="T172" id="Seg_1935" n="e" s="T171">sʼera </ts>
               <ts e="T173" id="Seg_1937" n="e" s="T172">măna) </ts>
               <ts e="T174" id="Seg_1939" n="e" s="T173">sʼo </ts>
               <ts e="T175" id="Seg_1941" n="e" s="T174">ravno </ts>
               <ts e="T176" id="Seg_1943" n="e" s="T175">amnosʼtə </ts>
               <ts e="T177" id="Seg_1945" n="e" s="T176">(dĭn </ts>
               <ts e="T178" id="Seg_1947" n="e" s="T177">tö </ts>
               <ts e="T179" id="Seg_1949" n="e" s="T178">nuge </ts>
               <ts e="T180" id="Seg_1951" n="e" s="T179">mona, </ts>
               <ts e="T181" id="Seg_1953" n="e" s="T180">mona). </ts>
               <ts e="T182" id="Seg_1955" n="e" s="T181">Dĭgəttə </ts>
               <ts e="T183" id="Seg_1957" n="e" s="T182">iʔbəbi </ts>
               <ts e="T184" id="Seg_1959" n="e" s="T183">kunolzittə. </ts>
               <ts e="T185" id="Seg_1961" n="e" s="T184">Nüdʼin </ts>
               <ts e="T186" id="Seg_1963" n="e" s="T185">šobi </ts>
               <ts e="T187" id="Seg_1965" n="e" s="T186">kuza. </ts>
               <ts e="T188" id="Seg_1967" n="e" s="T187">"Tăn </ts>
               <ts e="T189" id="Seg_1969" n="e" s="T188">Malanʼnʼa, </ts>
               <ts e="T190" id="Seg_1971" n="e" s="T189">tăn </ts>
               <ts e="T191" id="Seg_1973" n="e" s="T190">măna </ts>
               <ts e="T192" id="Seg_1975" n="e" s="T191">ajirial?" </ts>
               <ts e="T193" id="Seg_1977" n="e" s="T192">"A </ts>
               <ts e="T194" id="Seg_1979" n="e" s="T193">măn </ts>
               <ts e="T195" id="Seg_1981" n="e" s="T194">(en </ts>
               <ts e="T196" id="Seg_1983" n="e" s="T195">tĭm-) </ts>
               <ts e="T197" id="Seg_1985" n="e" s="T196">ej </ts>
               <ts e="T198" id="Seg_1987" n="e" s="T197">tĭmnem, </ts>
               <ts e="T199" id="Seg_1989" n="e" s="T198">(sin-) </ts>
               <ts e="T200" id="Seg_1991" n="e" s="T199">šindi </ts>
               <ts e="T201" id="Seg_1993" n="e" s="T200">tăn. </ts>
               <ts e="T202" id="Seg_1995" n="e" s="T201">Šoʔ </ts>
               <ts e="T203" id="Seg_1997" n="e" s="T202">döbər". </ts>
               <ts e="T204" id="Seg_1999" n="e" s="T203">"Dʼok, </ts>
               <ts e="T205" id="Seg_2001" n="e" s="T204">măn </ts>
               <ts e="T206" id="Seg_2003" n="e" s="T205">ej </ts>
               <ts e="T207" id="Seg_2005" n="e" s="T206">šolam, </ts>
               <ts e="T208" id="Seg_2007" n="e" s="T207">tăn </ts>
               <ts e="T209" id="Seg_2009" n="e" s="T208">(nereʔluʔlil)". </ts>
               <ts e="T210" id="Seg_2011" n="e" s="T209">Dĭgəttə </ts>
               <ts e="T396" id="Seg_2013" n="e" s="T210">kalla </ts>
               <ts e="T211" id="Seg_2015" n="e" s="T396">dʼürbi. </ts>
               <ts e="T212" id="Seg_2017" n="e" s="T211">Dĭ </ts>
               <ts e="T213" id="Seg_2019" n="e" s="T212">tenöbi, </ts>
               <ts e="T214" id="Seg_2021" n="e" s="T213">tenöbi. </ts>
               <ts e="T215" id="Seg_2023" n="e" s="T214">Dĭgəttə </ts>
               <ts e="T216" id="Seg_2025" n="e" s="T215">šide </ts>
               <ts e="T217" id="Seg_2027" n="e" s="T216">nüdʼi </ts>
               <ts e="T218" id="Seg_2029" n="e" s="T217">šobi. </ts>
               <ts e="T219" id="Seg_2031" n="e" s="T218">Dĭ </ts>
               <ts e="T220" id="Seg_2033" n="e" s="T219">bar </ts>
               <ts e="T221" id="Seg_2035" n="e" s="T220">bünə </ts>
               <ts e="T222" id="Seg_2037" n="e" s="T221">kambi, </ts>
               <ts e="T223" id="Seg_2039" n="e" s="T222">kola </ts>
               <ts e="T224" id="Seg_2041" n="e" s="T223">dĭn </ts>
               <ts e="T225" id="Seg_2043" n="e" s="T224">i </ts>
               <ts e="T226" id="Seg_2045" n="e" s="T225">to </ts>
               <ts e="T227" id="Seg_2047" n="e" s="T226">pʼaŋdona </ts>
               <ts e="T228" id="Seg_2049" n="e" s="T227">Malanʼnʼan </ts>
               <ts e="T229" id="Seg_2051" n="e" s="T228">kola. </ts>
               <ts e="T230" id="Seg_2053" n="e" s="T229">Dĭgəttə </ts>
               <ts e="T231" id="Seg_2055" n="e" s="T230">dĭ </ts>
               <ts e="T232" id="Seg_2057" n="e" s="T231">bazoʔ </ts>
               <ts e="T233" id="Seg_2059" n="e" s="T232">nüdʼit </ts>
               <ts e="T234" id="Seg_2061" n="e" s="T233">šobi. </ts>
               <ts e="T235" id="Seg_2063" n="e" s="T234">Tenöbi, </ts>
               <ts e="T236" id="Seg_2065" n="e" s="T235">tenöbi, </ts>
               <ts e="T237" id="Seg_2067" n="e" s="T236">dĭgəttə </ts>
               <ts e="T238" id="Seg_2069" n="e" s="T237">măndə:" </ts>
               <ts e="T239" id="Seg_2071" n="e" s="T238">Ej </ts>
               <ts e="T240" id="Seg_2073" n="e" s="T239">tĭmnem, </ts>
               <ts e="T241" id="Seg_2075" n="e" s="T240">šindi </ts>
               <ts e="T242" id="Seg_2077" n="e" s="T241">tɨ. </ts>
               <ts e="T243" id="Seg_2079" n="e" s="T242">Šində </ts>
               <ts e="T244" id="Seg_2081" n="e" s="T243">tăn </ts>
               <ts e="T245" id="Seg_2083" n="e" s="T244">igel". </ts>
               <ts e="T246" id="Seg_2085" n="e" s="T245">"Dʼok, </ts>
               <ts e="T247" id="Seg_2087" n="e" s="T246">măn </ts>
               <ts e="T248" id="Seg_2089" n="e" s="T247">em </ts>
               <ts e="T249" id="Seg_2091" n="e" s="T248">šoʔ, </ts>
               <ts e="T250" id="Seg_2093" n="e" s="T249">tăn </ts>
               <ts e="T251" id="Seg_2095" n="e" s="T250">bazoʔ </ts>
               <ts e="T252" id="Seg_2097" n="e" s="T251">nereʔluʔpil". </ts>
               <ts e="T253" id="Seg_2099" n="e" s="T252">Dĭgəttə </ts>
               <ts e="T254" id="Seg_2101" n="e" s="T253">nagur </ts>
               <ts e="T255" id="Seg_2103" n="e" s="T254">nüdʼi </ts>
               <ts e="T256" id="Seg_2105" n="e" s="T255">kambi. </ts>
               <ts e="T257" id="Seg_2107" n="e" s="T256">(Dĭm) </ts>
               <ts e="T258" id="Seg_2109" n="e" s="T257">šobi </ts>
               <ts e="T259" id="Seg_2111" n="e" s="T258">nagur </ts>
               <ts e="T260" id="Seg_2113" n="e" s="T259">(lĭd) </ts>
               <ts e="T261" id="Seg_2115" n="e" s="T260">nüdʼi, </ts>
               <ts e="T262" id="Seg_2117" n="e" s="T261">dĭ </ts>
               <ts e="T263" id="Seg_2119" n="e" s="T262">bazoʔ </ts>
               <ts e="T264" id="Seg_2121" n="e" s="T263">iʔbəbi </ts>
               <ts e="T265" id="Seg_2123" n="e" s="T264">kunolzittə. </ts>
               <ts e="T266" id="Seg_2125" n="e" s="T265">Dĭ </ts>
               <ts e="T267" id="Seg_2127" n="e" s="T266">bazoʔ </ts>
               <ts e="T268" id="Seg_2129" n="e" s="T267">šobi, </ts>
               <ts e="T269" id="Seg_2131" n="e" s="T268">surarlaʔbə: </ts>
               <ts e="T270" id="Seg_2133" n="e" s="T269">"No, </ts>
               <ts e="T271" id="Seg_2135" n="e" s="T270">tenöluʔpil?" </ts>
               <ts e="T272" id="Seg_2137" n="e" s="T271">"Da </ts>
               <ts e="T273" id="Seg_2139" n="e" s="T272">šoʔ </ts>
               <ts e="T274" id="Seg_2141" n="e" s="T273">döber, </ts>
               <ts e="T275" id="Seg_2143" n="e" s="T274">măn </ts>
               <ts e="T276" id="Seg_2145" n="e" s="T275">măndərlim </ts>
               <ts e="T277" id="Seg_2147" n="e" s="T276">tănan". </ts>
               <ts e="T278" id="Seg_2149" n="e" s="T277">"Da </ts>
               <ts e="T279" id="Seg_2151" n="e" s="T278">tăn </ts>
               <ts e="T280" id="Seg_2153" n="e" s="T279">nereʔluʔlil". </ts>
               <ts e="T281" id="Seg_2155" n="e" s="T280">Dĭ </ts>
               <ts e="T282" id="Seg_2157" n="e" s="T281">măndə:" </ts>
               <ts e="T283" id="Seg_2159" n="e" s="T282">Ej </ts>
               <ts e="T284" id="Seg_2161" n="e" s="T283">nereʔluʔlim". </ts>
               <ts e="T285" id="Seg_2163" n="e" s="T284">Dĭ </ts>
               <ts e="T286" id="Seg_2165" n="e" s="T285">šobi </ts>
               <ts e="T287" id="Seg_2167" n="e" s="T286">turanə. </ts>
               <ts e="T288" id="Seg_2169" n="e" s="T287">Dĭ </ts>
               <ts e="T289" id="Seg_2171" n="e" s="T288">ej </ts>
               <ts e="T290" id="Seg_2173" n="e" s="T289">nereʔluʔpi. </ts>
               <ts e="T291" id="Seg_2175" n="e" s="T290">Mănlia:" </ts>
               <ts e="T292" id="Seg_2177" n="e" s="T291">Amnolam </ts>
               <ts e="T293" id="Seg_2179" n="e" s="T292">dĭzi". </ts>
               <ts e="T294" id="Seg_2181" n="e" s="T293">Ĭmbi </ts>
               <ts e="T295" id="Seg_2183" n="e" s="T294">(sʼera) </ts>
               <ts e="T296" id="Seg_2185" n="e" s="T295">pušaj </ts>
               <ts e="T297" id="Seg_2187" n="e" s="T296">ej </ts>
               <ts e="T298" id="Seg_2189" n="e" s="T297">kuvas, </ts>
               <ts e="T299" id="Seg_2191" n="e" s="T298">da </ts>
               <ts e="T300" id="Seg_2193" n="e" s="T299">dön </ts>
               <ts e="T301" id="Seg_2195" n="e" s="T300">bar </ts>
               <ts e="T302" id="Seg_2197" n="e" s="T301">măna </ts>
               <ts e="T303" id="Seg_2199" n="e" s="T302">i </ts>
               <ts e="T304" id="Seg_2201" n="e" s="T303">măna. </ts>
               <ts e="T305" id="Seg_2203" n="e" s="T304">Dĭgəttə </ts>
               <ts e="T306" id="Seg_2205" n="e" s="T305">amnobi, </ts>
               <ts e="T307" id="Seg_2207" n="e" s="T306">amnobi, </ts>
               <ts e="T308" id="Seg_2209" n="e" s="T307">măndə:" </ts>
               <ts e="T309" id="Seg_2211" n="e" s="T308">Măn </ts>
               <ts e="T310" id="Seg_2213" n="e" s="T309">kalam </ts>
               <ts e="T311" id="Seg_2215" n="e" s="T310">(abats-) </ts>
               <ts e="T312" id="Seg_2217" n="e" s="T311">abanə". </ts>
               <ts e="T313" id="Seg_2219" n="e" s="T312">Dĭ </ts>
               <ts e="T314" id="Seg_2221" n="e" s="T313">dĭʔnə </ts>
               <ts e="T315" id="Seg_2223" n="e" s="T314">(k-) </ts>
               <ts e="T316" id="Seg_2225" n="e" s="T315">kălʼso </ts>
               <ts e="T317" id="Seg_2227" n="e" s="T316">mĭbi. </ts>
               <ts e="T318" id="Seg_2229" n="e" s="T317">"No, </ts>
               <ts e="T319" id="Seg_2231" n="e" s="T318">kanaʔ. </ts>
               <ts e="T320" id="Seg_2233" n="e" s="T319">Kamen </ts>
               <ts e="T321" id="Seg_2235" n="e" s="T320">dĭ </ts>
               <ts e="T322" id="Seg_2237" n="e" s="T321">kălʼso </ts>
               <ts e="T323" id="Seg_2239" n="e" s="T322">sagər </ts>
               <ts e="T324" id="Seg_2241" n="e" s="T323">moləj, </ts>
               <ts e="T325" id="Seg_2243" n="e" s="T324">dĭgəttə </ts>
               <ts e="T326" id="Seg_2245" n="e" s="T325">büžü </ts>
               <ts e="T327" id="Seg_2247" n="e" s="T326">šoʔ, </ts>
               <ts e="T328" id="Seg_2249" n="e" s="T327">a_to </ts>
               <ts e="T329" id="Seg_2251" n="e" s="T328">măn </ts>
               <ts e="T330" id="Seg_2253" n="e" s="T329">külalləm". </ts>
               <ts e="T331" id="Seg_2255" n="e" s="T330">(Dĭ) </ts>
               <ts e="T332" id="Seg_2257" n="e" s="T331">kambi, </ts>
               <ts e="T333" id="Seg_2259" n="e" s="T332">sʼestrattə. </ts>
               <ts e="T334" id="Seg_2261" n="e" s="T333">Bar </ts>
               <ts e="T335" id="Seg_2263" n="e" s="T334">dĭʔnə </ts>
               <ts e="T336" id="Seg_2265" n="e" s="T335">(dĭgət-) </ts>
               <ts e="T337" id="Seg_2267" n="e" s="T336">bü </ts>
               <ts e="T338" id="Seg_2269" n="e" s="T337">girgitdə </ts>
               <ts e="T339" id="Seg_2271" n="e" s="T338">mĭbi. </ts>
               <ts e="T340" id="Seg_2273" n="e" s="T339">Tĭ </ts>
               <ts e="T341" id="Seg_2275" n="e" s="T340">bar </ts>
               <ts e="T342" id="Seg_2277" n="e" s="T341">kunolbi, </ts>
               <ts e="T343" id="Seg_2279" n="e" s="T342">kunolbi, </ts>
               <ts e="T344" id="Seg_2281" n="e" s="T343">uʔbdəbi. </ts>
               <ts e="T345" id="Seg_2283" n="e" s="T344">Kălʼečkăt </ts>
               <ts e="T346" id="Seg_2285" n="e" s="T345">bar </ts>
               <ts e="T347" id="Seg_2287" n="e" s="T346">sagər </ts>
               <ts e="T348" id="Seg_2289" n="e" s="T347">dĭ. </ts>
               <ts e="T349" id="Seg_2291" n="e" s="T348">Bar </ts>
               <ts e="T350" id="Seg_2293" n="e" s="T349">ĭmbi </ts>
               <ts e="T351" id="Seg_2295" n="e" s="T350">baruʔpi. </ts>
               <ts e="T352" id="Seg_2297" n="e" s="T351">I </ts>
               <ts e="T353" id="Seg_2299" n="e" s="T352">kambi; </ts>
               <ts e="T354" id="Seg_2301" n="e" s="T353">šobi </ts>
               <ts e="T355" id="Seg_2303" n="e" s="T354">dĭbər: </ts>
               <ts e="T356" id="Seg_2305" n="e" s="T355">iʔbolaʔbə, </ts>
               <ts e="T357" id="Seg_2307" n="e" s="T356">külambi. </ts>
               <ts e="T358" id="Seg_2309" n="e" s="T357">Bar </ts>
               <ts e="T359" id="Seg_2311" n="e" s="T358">dʼorbi. </ts>
               <ts e="T360" id="Seg_2313" n="e" s="T359">"Măn </ts>
               <ts e="T361" id="Seg_2315" n="e" s="T360">tănan </ts>
               <ts e="T362" id="Seg_2317" n="e" s="T361">ajirliom, </ts>
               <ts e="T363" id="Seg_2319" n="e" s="T362">uʔbdaʔ". </ts>
               <ts e="T364" id="Seg_2321" n="e" s="T363">"Ajirliom, </ts>
               <ts e="T365" id="Seg_2323" n="e" s="T364">uʔbdaʔ". </ts>
               <ts e="T366" id="Seg_2325" n="e" s="T365">Dĭ </ts>
               <ts e="T367" id="Seg_2327" n="e" s="T366">uʔbdəbi. </ts>
               <ts e="T368" id="Seg_2329" n="e" s="T367">I </ts>
               <ts e="T369" id="Seg_2331" n="e" s="T368">ugandə </ts>
               <ts e="T370" id="Seg_2333" n="e" s="T369">kuvas </ts>
               <ts e="T371" id="Seg_2335" n="e" s="T370">nʼi </ts>
               <ts e="T372" id="Seg_2337" n="e" s="T371">molambi. </ts>
               <ts e="T373" id="Seg_2339" n="e" s="T372">Dĭgəttə </ts>
               <ts e="T374" id="Seg_2341" n="e" s="T373">koŋgoroʔi </ts>
               <ts e="T375" id="Seg_2343" n="e" s="T374">bar </ts>
               <ts e="T376" id="Seg_2345" n="e" s="T375">küzürleʔbəʔjə. </ts>
               <ts e="T377" id="Seg_2347" n="e" s="T376">Il </ts>
               <ts e="T378" id="Seg_2349" n="e" s="T377">mĭlleʔbəʔjə. </ts>
               <ts e="T379" id="Seg_2351" n="e" s="T378">Molambi </ts>
               <ts e="T380" id="Seg_2353" n="e" s="T379">gorăt, </ts>
               <ts e="T381" id="Seg_2355" n="e" s="T380">a </ts>
               <ts e="T382" id="Seg_2357" n="e" s="T381">(dĭ=) </ts>
               <ts e="T383" id="Seg_2359" n="e" s="T382">dĭ </ts>
               <ts e="T384" id="Seg_2361" n="e" s="T383">nʼi </ts>
               <ts e="T385" id="Seg_2363" n="e" s="T384">koŋ </ts>
               <ts e="T386" id="Seg_2365" n="e" s="T385">molambi. </ts>
               <ts e="T387" id="Seg_2367" n="e" s="T386">Dĭgəttə </ts>
               <ts e="T388" id="Seg_2369" n="e" s="T387">dĭ </ts>
               <ts e="T389" id="Seg_2371" n="e" s="T388">(dĭʔn-) </ts>
               <ts e="T390" id="Seg_2373" n="e" s="T389">dĭm </ts>
               <ts e="T391" id="Seg_2375" n="e" s="T390">tibinə </ts>
               <ts e="T392" id="Seg_2377" n="e" s="T391">ibi. </ts>
               <ts e="T393" id="Seg_2379" n="e" s="T392">I </ts>
               <ts e="T394" id="Seg_2381" n="e" s="T393">stalʼi </ts>
               <ts e="T395" id="Seg_2383" n="e" s="T394">amnozittə. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T2" id="Seg_2384" s="T0">PKZ_196X_ScarlettFlower_flk.001 (001)</ta>
            <ta e="T9" id="Seg_2385" s="T2">PKZ_196X_ScarlettFlower_flk.002 (002)</ta>
            <ta e="T15" id="Seg_2386" s="T9">PKZ_196X_ScarlettFlower_flk.003 (003)</ta>
            <ta e="T21" id="Seg_2387" s="T15">PKZ_196X_ScarlettFlower_flk.004 (004)</ta>
            <ta e="T26" id="Seg_2388" s="T21">PKZ_196X_ScarlettFlower_flk.005 (005)</ta>
            <ta e="T34" id="Seg_2389" s="T26">PKZ_196X_ScarlettFlower_flk.006 (006) </ta>
            <ta e="T42" id="Seg_2390" s="T34">PKZ_196X_ScarlettFlower_flk.007 (008)</ta>
            <ta e="T47" id="Seg_2391" s="T42">PKZ_196X_ScarlettFlower_flk.008 (009)</ta>
            <ta e="T49" id="Seg_2392" s="T47">PKZ_196X_ScarlettFlower_flk.009 (010)</ta>
            <ta e="T51" id="Seg_2393" s="T49">PKZ_196X_ScarlettFlower_flk.010 (011)</ta>
            <ta e="T57" id="Seg_2394" s="T51">PKZ_196X_ScarlettFlower_flk.011 (012)</ta>
            <ta e="T61" id="Seg_2395" s="T57">PKZ_196X_ScarlettFlower_flk.012 (013)</ta>
            <ta e="T70" id="Seg_2396" s="T61">PKZ_196X_ScarlettFlower_flk.013 (014)</ta>
            <ta e="T75" id="Seg_2397" s="T70">PKZ_196X_ScarlettFlower_flk.014 (015)</ta>
            <ta e="T78" id="Seg_2398" s="T75">PKZ_196X_ScarlettFlower_flk.015 (016)</ta>
            <ta e="T81" id="Seg_2399" s="T78">PKZ_196X_ScarlettFlower_flk.016 (017)</ta>
            <ta e="T85" id="Seg_2400" s="T81">PKZ_196X_ScarlettFlower_flk.017 (018)</ta>
            <ta e="T88" id="Seg_2401" s="T85">PKZ_196X_ScarlettFlower_flk.018 (019)</ta>
            <ta e="T96" id="Seg_2402" s="T88">PKZ_196X_ScarlettFlower_flk.019 (020)</ta>
            <ta e="T99" id="Seg_2403" s="T96">PKZ_196X_ScarlettFlower_flk.020 (021)</ta>
            <ta e="T100" id="Seg_2404" s="T99">PKZ_196X_ScarlettFlower_flk.021 (022)</ta>
            <ta e="T103" id="Seg_2405" s="T100">PKZ_196X_ScarlettFlower_flk.022 (023)</ta>
            <ta e="T107" id="Seg_2406" s="T103">PKZ_196X_ScarlettFlower_flk.023 (024)</ta>
            <ta e="T110" id="Seg_2407" s="T107">PKZ_196X_ScarlettFlower_flk.024 (025)</ta>
            <ta e="T113" id="Seg_2408" s="T110">PKZ_196X_ScarlettFlower_flk.025 (026)</ta>
            <ta e="T114" id="Seg_2409" s="T113">PKZ_196X_ScarlettFlower_flk.026 (027)</ta>
            <ta e="T119" id="Seg_2410" s="T114">PKZ_196X_ScarlettFlower_flk.027 (028)</ta>
            <ta e="T126" id="Seg_2411" s="T119">PKZ_196X_ScarlettFlower_flk.028 (029)</ta>
            <ta e="T131" id="Seg_2412" s="T126">PKZ_196X_ScarlettFlower_flk.029 (030)</ta>
            <ta e="T135" id="Seg_2413" s="T131">PKZ_196X_ScarlettFlower_flk.030 (031)</ta>
            <ta e="T137" id="Seg_2414" s="T135">PKZ_196X_ScarlettFlower_flk.031 (032)</ta>
            <ta e="T145" id="Seg_2415" s="T137">PKZ_196X_ScarlettFlower_flk.032 (033)</ta>
            <ta e="T149" id="Seg_2416" s="T145">PKZ_196X_ScarlettFlower_flk.033 (034)</ta>
            <ta e="T157" id="Seg_2417" s="T149">PKZ_196X_ScarlettFlower_flk.034 (035)</ta>
            <ta e="T160" id="Seg_2418" s="T157">PKZ_196X_ScarlettFlower_flk.035 (036)</ta>
            <ta e="T166" id="Seg_2419" s="T160">PKZ_196X_ScarlettFlower_flk.036 (037)</ta>
            <ta e="T170" id="Seg_2420" s="T166">PKZ_196X_ScarlettFlower_flk.037 (038)</ta>
            <ta e="T181" id="Seg_2421" s="T170">PKZ_196X_ScarlettFlower_flk.038 (039)</ta>
            <ta e="T184" id="Seg_2422" s="T181">PKZ_196X_ScarlettFlower_flk.039 (040)</ta>
            <ta e="T187" id="Seg_2423" s="T184">PKZ_196X_ScarlettFlower_flk.040 (041)</ta>
            <ta e="T192" id="Seg_2424" s="T187">PKZ_196X_ScarlettFlower_flk.041 (042)</ta>
            <ta e="T201" id="Seg_2425" s="T192">PKZ_196X_ScarlettFlower_flk.042 (043)</ta>
            <ta e="T203" id="Seg_2426" s="T201">PKZ_196X_ScarlettFlower_flk.043 (044)</ta>
            <ta e="T209" id="Seg_2427" s="T203">PKZ_196X_ScarlettFlower_flk.044 (045)</ta>
            <ta e="T211" id="Seg_2428" s="T209">PKZ_196X_ScarlettFlower_flk.045 (046)</ta>
            <ta e="T214" id="Seg_2429" s="T211">PKZ_196X_ScarlettFlower_flk.046 (047)</ta>
            <ta e="T218" id="Seg_2430" s="T214">PKZ_196X_ScarlettFlower_flk.047 (048)</ta>
            <ta e="T229" id="Seg_2431" s="T218">PKZ_196X_ScarlettFlower_flk.048 (049)</ta>
            <ta e="T234" id="Seg_2432" s="T229">PKZ_196X_ScarlettFlower_flk.049 (050)</ta>
            <ta e="T242" id="Seg_2433" s="T234">PKZ_196X_ScarlettFlower_flk.050 (051)</ta>
            <ta e="T245" id="Seg_2434" s="T242">PKZ_196X_ScarlettFlower_flk.051 (052)</ta>
            <ta e="T252" id="Seg_2435" s="T245">PKZ_196X_ScarlettFlower_flk.052 (053)</ta>
            <ta e="T256" id="Seg_2436" s="T252">PKZ_196X_ScarlettFlower_flk.053 (054)</ta>
            <ta e="T265" id="Seg_2437" s="T256">PKZ_196X_ScarlettFlower_flk.054 (055)</ta>
            <ta e="T271" id="Seg_2438" s="T265">PKZ_196X_ScarlettFlower_flk.055 (056) </ta>
            <ta e="T277" id="Seg_2439" s="T271">PKZ_196X_ScarlettFlower_flk.056 (058)</ta>
            <ta e="T280" id="Seg_2440" s="T277">PKZ_196X_ScarlettFlower_flk.057 (059)</ta>
            <ta e="T284" id="Seg_2441" s="T280">PKZ_196X_ScarlettFlower_flk.058 (060)</ta>
            <ta e="T287" id="Seg_2442" s="T284">PKZ_196X_ScarlettFlower_flk.059 (061)</ta>
            <ta e="T290" id="Seg_2443" s="T287">PKZ_196X_ScarlettFlower_flk.060 (062)</ta>
            <ta e="T293" id="Seg_2444" s="T290">PKZ_196X_ScarlettFlower_flk.061 (063)</ta>
            <ta e="T304" id="Seg_2445" s="T293">PKZ_196X_ScarlettFlower_flk.062 (064)</ta>
            <ta e="T312" id="Seg_2446" s="T304">PKZ_196X_ScarlettFlower_flk.063 (065)</ta>
            <ta e="T317" id="Seg_2447" s="T312">PKZ_196X_ScarlettFlower_flk.064 (066)</ta>
            <ta e="T319" id="Seg_2448" s="T317">PKZ_196X_ScarlettFlower_flk.065 (067)</ta>
            <ta e="T330" id="Seg_2449" s="T319">PKZ_196X_ScarlettFlower_flk.066 (068)</ta>
            <ta e="T333" id="Seg_2450" s="T330">PKZ_196X_ScarlettFlower_flk.067 (069)</ta>
            <ta e="T339" id="Seg_2451" s="T333">PKZ_196X_ScarlettFlower_flk.068 (070)</ta>
            <ta e="T344" id="Seg_2452" s="T339">PKZ_196X_ScarlettFlower_flk.069 (071)</ta>
            <ta e="T348" id="Seg_2453" s="T344">PKZ_196X_ScarlettFlower_flk.070 (072)</ta>
            <ta e="T351" id="Seg_2454" s="T348">PKZ_196X_ScarlettFlower_flk.071 (073)</ta>
            <ta e="T357" id="Seg_2455" s="T351">PKZ_196X_ScarlettFlower_flk.072 (074)</ta>
            <ta e="T359" id="Seg_2456" s="T357">PKZ_196X_ScarlettFlower_flk.073 (075)</ta>
            <ta e="T363" id="Seg_2457" s="T359">PKZ_196X_ScarlettFlower_flk.074 (076)</ta>
            <ta e="T365" id="Seg_2458" s="T363">PKZ_196X_ScarlettFlower_flk.075 (077)</ta>
            <ta e="T367" id="Seg_2459" s="T365">PKZ_196X_ScarlettFlower_flk.076 (078)</ta>
            <ta e="T372" id="Seg_2460" s="T367">PKZ_196X_ScarlettFlower_flk.077 (079)</ta>
            <ta e="T376" id="Seg_2461" s="T372">PKZ_196X_ScarlettFlower_flk.078 (080)</ta>
            <ta e="T378" id="Seg_2462" s="T376">PKZ_196X_ScarlettFlower_flk.079 (081)</ta>
            <ta e="T386" id="Seg_2463" s="T378">PKZ_196X_ScarlettFlower_flk.080 (082)</ta>
            <ta e="T392" id="Seg_2464" s="T386">PKZ_196X_ScarlettFlower_flk.081 (083)</ta>
            <ta e="T395" id="Seg_2465" s="T392">PKZ_196X_ScarlettFlower_flk.082 (084)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T2" id="Seg_2466" s="T0">Amnobi büzʼe. </ta>
            <ta e="T9" id="Seg_2467" s="T2">(Dĭn bɨl na-) Dĭn ibiʔi nagur koʔbdozaŋdə. </ta>
            <ta e="T15" id="Seg_2468" s="T9">Dĭ stal (ob-) oʔbdəsʼtə gorăttə kanzittə. </ta>
            <ta e="T21" id="Seg_2469" s="T15">Urgo koʔbdondə măndə:" Ĭmbi tănan izittə?" </ta>
            <ta e="T26" id="Seg_2470" s="T21">"Tăn măna kuvas platʼtʼa iʔ". </ta>
            <ta e="T34" id="Seg_2471" s="T26">A üdʼüget koʔbdo: "Tăn măna bătinkaʔi kuvas iʔ". </ta>
            <ta e="T42" id="Seg_2472" s="T34">A nagur, išo üdʼüge koʔbdo:" Tănan ĭmbi izittə?" </ta>
            <ta e="T47" id="Seg_2473" s="T42">"Tăn măna iʔ kömə sʼvʼetok". </ta>
            <ta e="T49" id="Seg_2474" s="T47">Dĭ kambi. </ta>
            <ta e="T51" id="Seg_2475" s="T49">Măndərbi, măndərbi. </ta>
            <ta e="T57" id="Seg_2476" s="T51">Ibi döʔnə platʼtʼa, onʼiʔtə ibi bătinkaʔi. </ta>
            <ta e="T61" id="Seg_2477" s="T57">A (sʼvet-) sʼvʼetok naga. </ta>
            <ta e="T70" id="Seg_2478" s="T61">Dĭgəttə šonəga i tenölaʔbə: gijen (i-) izittə kömə sʼvʼetok? </ta>
            <ta e="T75" id="Seg_2479" s="T70">Šonəga, kubi bar košideŋgən sʼvʼetogəʔi. </ta>
            <ta e="T78" id="Seg_2480" s="T75">Dĭbər kambi, nĭŋgəbi. </ta>
            <ta e="T81" id="Seg_2481" s="T78">Vărotaʔi (kau-) (kajluʔpiʔi). </ta>
            <ta e="T85" id="Seg_2482" s="T81">Dĭgəttə mĭmbi, mĭmbi (dunʼtʼigə). </ta>
            <ta e="T88" id="Seg_2483" s="T85">Nüdʼin šobi kuza. </ta>
            <ta e="T96" id="Seg_2484" s="T88">|(Bot-) Boslə koʔbdol mĭləl măna (tĭbi=) detlil döbər". </ta>
            <ta e="T99" id="Seg_2485" s="T96">Dĭ tenöbi, tenöbi. </ta>
            <ta e="T100" id="Seg_2486" s="T99">"Detlim!" </ta>
            <ta e="T103" id="Seg_2487" s="T100">Dĭgəttə erten uʔbdəbi. </ta>
            <ta e="T107" id="Seg_2488" s="T103">Vărotatsi (kajluʔ- kajl-) kajluʔpiʔi. </ta>
            <ta e="T110" id="Seg_2489" s="T107">I šobi maːʔndə. </ta>
            <ta e="T113" id="Seg_2490" s="T110">Deʔpi, bar mĭbi. </ta>
            <ta e="T114" id="Seg_2491" s="T113">(Nagurgö). </ta>
            <ta e="T119" id="Seg_2492" s="T114">"A tănan nada dĭbər kanzittə". </ta>
            <ta e="T126" id="Seg_2493" s="T119">Dĭ măndə:" Nu i što, măn kalam". </ta>
            <ta e="T131" id="Seg_2494" s="T126">"Dĭn iʔgö sʼvʼetogəʔi bar nĭŋgələl". </ta>
            <ta e="T135" id="Seg_2495" s="T131">Deʔpi, öʔlubi dĭm dĭbər. </ta>
            <ta e="T137" id="Seg_2496" s="T135">Vărotaʔi kajluʔpiʔi. </ta>
            <ta e="T145" id="Seg_2497" s="T137">(Dĭm dĭ maʔtubi ma-) maːluʔpi i maʔndə šobi. </ta>
            <ta e="T149" id="Seg_2498" s="T145">(Tej) dʼala mĭmbi, mĭmbi. </ta>
            <ta e="T157" id="Seg_2499" s="T149">Sʼvʼetogəʔi üge (pʼaŋdona) Malanʼnʼan, ĭmbi mĭndləj üge Malanʼnʼa. </ta>
            <ta e="T160" id="Seg_2500" s="T157">Turanə šobi bar. </ta>
            <ta e="T166" id="Seg_2501" s="T160">(Bazaʔi), girgit šamnagəʔi üge dĭn malanʼnʼan. </ta>
            <ta e="T170" id="Seg_2502" s="T166">Dagaʔi, (am-) dĭgəttə amorbi. </ta>
            <ta e="T181" id="Seg_2503" s="T170">(Ambi sʼera măna) sʼo ravno amnosʼtə (dĭn tö nuge mona, mona). </ta>
            <ta e="T184" id="Seg_2504" s="T181">Dĭgəttə iʔbəbi kunolzittə. </ta>
            <ta e="T187" id="Seg_2505" s="T184">Nüdʼin šobi kuza. </ta>
            <ta e="T192" id="Seg_2506" s="T187">"Tăn Malanʼnʼa, tăn măna ajirial?" </ta>
            <ta e="T201" id="Seg_2507" s="T192">"A măn (en tĭm-) ej tĭmnem, (sin-) šindi tăn. </ta>
            <ta e="T203" id="Seg_2508" s="T201">Šoʔ döbər". </ta>
            <ta e="T209" id="Seg_2509" s="T203">"Dʼok, măn ej šolam, tăn (nereʔluʔlil)". </ta>
            <ta e="T211" id="Seg_2510" s="T209">Dĭgəttə kalla dʼürbi. </ta>
            <ta e="T214" id="Seg_2511" s="T211">Dĭ tenöbi, tenöbi. </ta>
            <ta e="T218" id="Seg_2512" s="T214">Dĭgəttə šide nüdʼi šobi. </ta>
            <ta e="T229" id="Seg_2513" s="T218">Dĭ bar bünə kambi, kola dĭn i to pʼaŋdona Malanʼnʼan kola. </ta>
            <ta e="T234" id="Seg_2514" s="T229">Dĭgəttə dĭ bazoʔ nüdʼit šobi. </ta>
            <ta e="T242" id="Seg_2515" s="T234">Tenöbi, tenöbi, dĭgəttə măndə:" Ej tĭmnem, šindi tɨ. </ta>
            <ta e="T245" id="Seg_2516" s="T242">Šində tăn igel". </ta>
            <ta e="T252" id="Seg_2517" s="T245">"Dʼok, măn em šoʔ, tăn bazoʔ nereʔluʔpil". </ta>
            <ta e="T256" id="Seg_2518" s="T252">Dĭgəttə nagur nüdʼi kambi. </ta>
            <ta e="T265" id="Seg_2519" s="T256">(Dĭm) šobi nagur (lĭd) nüdʼi, dĭ bazoʔ iʔbəbi kunolzittə. </ta>
            <ta e="T271" id="Seg_2520" s="T265">Dĭ bazoʔ šobi, surarlaʔbə: "No, tenöluʔpil?" </ta>
            <ta e="T277" id="Seg_2521" s="T271">"Da šoʔ döber, măn măndərlim tănan". </ta>
            <ta e="T280" id="Seg_2522" s="T277">"Da tăn nereʔluʔlil". </ta>
            <ta e="T284" id="Seg_2523" s="T280">Dĭ măndə:" Ej nereʔluʔlim". </ta>
            <ta e="T287" id="Seg_2524" s="T284">Dĭ šobi turanə. </ta>
            <ta e="T290" id="Seg_2525" s="T287">Dĭ ej nereʔluʔpi. </ta>
            <ta e="T293" id="Seg_2526" s="T290">Mănlia:" Amnolam dĭzi". </ta>
            <ta e="T304" id="Seg_2527" s="T293">Ĭmbi (sʼera) pušaj ej kuvas, da dön bar măna i măna. </ta>
            <ta e="T312" id="Seg_2528" s="T304">Dĭgəttə amnobi, amnobi, măndə:" Măn kalam (abats-) abanə". </ta>
            <ta e="T317" id="Seg_2529" s="T312">Dĭ dĭʔnə (k-) kălʼso mĭbi. </ta>
            <ta e="T319" id="Seg_2530" s="T317">"No, kanaʔ. </ta>
            <ta e="T330" id="Seg_2531" s="T319">Kamen dĭ kălʼso sagər moləj, dĭgəttə büžü šoʔ, a to măn külalləm". </ta>
            <ta e="T333" id="Seg_2532" s="T330">(Dĭ) kambi, sʼestrattə. </ta>
            <ta e="T339" id="Seg_2533" s="T333">Bar dĭʔnə (dĭgət-) bü girgitdə mĭbi. </ta>
            <ta e="T344" id="Seg_2534" s="T339">Tĭ bar kunolbi, kunolbi, uʔbdəbi. </ta>
            <ta e="T348" id="Seg_2535" s="T344">Kălʼečkăt bar sagər dĭ. </ta>
            <ta e="T351" id="Seg_2536" s="T348">Bar ĭmbi baruʔpi. </ta>
            <ta e="T357" id="Seg_2537" s="T351">I kambi; šobi dĭbər: iʔbolaʔbə, külambi. </ta>
            <ta e="T359" id="Seg_2538" s="T357">Bar dʼorbi. </ta>
            <ta e="T363" id="Seg_2539" s="T359">"Măn tănan ajirliom, uʔbdaʔ". </ta>
            <ta e="T365" id="Seg_2540" s="T363">"Ajirliom, uʔbdaʔ". </ta>
            <ta e="T367" id="Seg_2541" s="T365">Dĭ uʔbdəbi. </ta>
            <ta e="T372" id="Seg_2542" s="T367">I ugandə kuvas nʼi molambi. </ta>
            <ta e="T376" id="Seg_2543" s="T372">Dĭgəttə koŋgoroʔi bar küzürleʔbəʔjə. </ta>
            <ta e="T378" id="Seg_2544" s="T376">Il mĭlleʔbəʔjə. </ta>
            <ta e="T386" id="Seg_2545" s="T378">Molambi gorăt, a (dĭ=) dĭ nʼi koŋ molambi. </ta>
            <ta e="T392" id="Seg_2546" s="T386">Dĭgəttə dĭ (dĭʔn-) dĭm tibinə ibi. </ta>
            <ta e="T395" id="Seg_2547" s="T392">I stalʼi amnozittə. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_2548" s="T0">amno-bi</ta>
            <ta e="T2" id="Seg_2549" s="T1">büzʼe</ta>
            <ta e="T3" id="Seg_2550" s="T2">dĭ-n</ta>
            <ta e="T6" id="Seg_2551" s="T5">dĭ-n</ta>
            <ta e="T7" id="Seg_2552" s="T6">i-bi-ʔi</ta>
            <ta e="T8" id="Seg_2553" s="T7">nagur</ta>
            <ta e="T9" id="Seg_2554" s="T8">koʔbdo-zaŋ-də</ta>
            <ta e="T10" id="Seg_2555" s="T9">dĭ</ta>
            <ta e="T11" id="Seg_2556" s="T10">stal</ta>
            <ta e="T13" id="Seg_2557" s="T12">oʔbdə-sʼtə</ta>
            <ta e="T14" id="Seg_2558" s="T13">gorăt-tə</ta>
            <ta e="T15" id="Seg_2559" s="T14">kan-zittə</ta>
            <ta e="T16" id="Seg_2560" s="T15">urgo</ta>
            <ta e="T17" id="Seg_2561" s="T16">koʔbdo-ndə</ta>
            <ta e="T18" id="Seg_2562" s="T17">măn-də</ta>
            <ta e="T19" id="Seg_2563" s="T18">ĭmbi</ta>
            <ta e="T20" id="Seg_2564" s="T19">tănan</ta>
            <ta e="T21" id="Seg_2565" s="T20">i-zittə</ta>
            <ta e="T22" id="Seg_2566" s="T21">tăn</ta>
            <ta e="T23" id="Seg_2567" s="T22">măna</ta>
            <ta e="T24" id="Seg_2568" s="T23">kuvas</ta>
            <ta e="T25" id="Seg_2569" s="T24">platʼtʼa</ta>
            <ta e="T26" id="Seg_2570" s="T25">i-ʔ</ta>
            <ta e="T27" id="Seg_2571" s="T26">a</ta>
            <ta e="T28" id="Seg_2572" s="T27">üdʼüge-t</ta>
            <ta e="T29" id="Seg_2573" s="T28">koʔbdo</ta>
            <ta e="T30" id="Seg_2574" s="T29">tăn</ta>
            <ta e="T31" id="Seg_2575" s="T30">măna</ta>
            <ta e="T32" id="Seg_2576" s="T31">bătinka-ʔi</ta>
            <ta e="T33" id="Seg_2577" s="T32">kuvas</ta>
            <ta e="T34" id="Seg_2578" s="T33">i-ʔ</ta>
            <ta e="T35" id="Seg_2579" s="T34">a</ta>
            <ta e="T36" id="Seg_2580" s="T35">nagur</ta>
            <ta e="T37" id="Seg_2581" s="T36">išo</ta>
            <ta e="T38" id="Seg_2582" s="T37">üdʼüge</ta>
            <ta e="T39" id="Seg_2583" s="T38">koʔbdo</ta>
            <ta e="T40" id="Seg_2584" s="T39">tănan</ta>
            <ta e="T41" id="Seg_2585" s="T40">ĭmbi</ta>
            <ta e="T42" id="Seg_2586" s="T41">i-zittə</ta>
            <ta e="T43" id="Seg_2587" s="T42">tăn</ta>
            <ta e="T44" id="Seg_2588" s="T43">măna</ta>
            <ta e="T45" id="Seg_2589" s="T44">i-ʔ</ta>
            <ta e="T46" id="Seg_2590" s="T45">kömə</ta>
            <ta e="T47" id="Seg_2591" s="T46">sʼvʼetok</ta>
            <ta e="T48" id="Seg_2592" s="T47">dĭ</ta>
            <ta e="T49" id="Seg_2593" s="T48">kam-bi</ta>
            <ta e="T50" id="Seg_2594" s="T49">măndə-r-bi</ta>
            <ta e="T51" id="Seg_2595" s="T50">măndə-r-bi</ta>
            <ta e="T52" id="Seg_2596" s="T51">i-bi</ta>
            <ta e="T53" id="Seg_2597" s="T52">döʔnə</ta>
            <ta e="T54" id="Seg_2598" s="T53">platʼtʼa</ta>
            <ta e="T55" id="Seg_2599" s="T54">onʼiʔ-tə</ta>
            <ta e="T56" id="Seg_2600" s="T55">i-bi</ta>
            <ta e="T57" id="Seg_2601" s="T56">bătinka-ʔi</ta>
            <ta e="T58" id="Seg_2602" s="T57">a</ta>
            <ta e="T60" id="Seg_2603" s="T59">sʼvʼetok</ta>
            <ta e="T61" id="Seg_2604" s="T60">naga</ta>
            <ta e="T62" id="Seg_2605" s="T61">dĭgəttə</ta>
            <ta e="T63" id="Seg_2606" s="T62">šonə-ga</ta>
            <ta e="T64" id="Seg_2607" s="T63">i</ta>
            <ta e="T65" id="Seg_2608" s="T64">tenö-laʔbə</ta>
            <ta e="T66" id="Seg_2609" s="T65">gijen</ta>
            <ta e="T68" id="Seg_2610" s="T67">i-zittə</ta>
            <ta e="T69" id="Seg_2611" s="T68">kömə</ta>
            <ta e="T70" id="Seg_2612" s="T69">sʼvʼetok</ta>
            <ta e="T71" id="Seg_2613" s="T70">šonə-ga</ta>
            <ta e="T72" id="Seg_2614" s="T71">ku-bi</ta>
            <ta e="T73" id="Seg_2615" s="T72">bar</ta>
            <ta e="T74" id="Seg_2616" s="T73">košideŋ-gən</ta>
            <ta e="T75" id="Seg_2617" s="T74">sʼvʼetog-əʔi</ta>
            <ta e="T76" id="Seg_2618" s="T75">dĭbər</ta>
            <ta e="T77" id="Seg_2619" s="T76">kam-bi</ta>
            <ta e="T78" id="Seg_2620" s="T77">nĭŋgə-bi</ta>
            <ta e="T79" id="Seg_2621" s="T78">vărota-ʔi</ta>
            <ta e="T81" id="Seg_2622" s="T80">kaj-luʔ-pi-ʔi</ta>
            <ta e="T82" id="Seg_2623" s="T81">dĭgəttə</ta>
            <ta e="T83" id="Seg_2624" s="T82">mĭm-bi</ta>
            <ta e="T84" id="Seg_2625" s="T83">mĭm-bi</ta>
            <ta e="T85" id="Seg_2626" s="T84">dunʼtʼigə</ta>
            <ta e="T86" id="Seg_2627" s="T85">nüdʼi-n</ta>
            <ta e="T87" id="Seg_2628" s="T86">šo-bi</ta>
            <ta e="T88" id="Seg_2629" s="T87">kuza</ta>
            <ta e="T90" id="Seg_2630" s="T89">bos-lə</ta>
            <ta e="T91" id="Seg_2631" s="T90">koʔbdo-l</ta>
            <ta e="T92" id="Seg_2632" s="T91">mĭ-lə-l</ta>
            <ta e="T93" id="Seg_2633" s="T92">măna</ta>
            <ta e="T95" id="Seg_2634" s="T94">det-li-l</ta>
            <ta e="T96" id="Seg_2635" s="T95">döbər</ta>
            <ta e="T97" id="Seg_2636" s="T96">dĭ</ta>
            <ta e="T98" id="Seg_2637" s="T97">tenö-bi</ta>
            <ta e="T99" id="Seg_2638" s="T98">tenö-bi</ta>
            <ta e="T100" id="Seg_2639" s="T99">det-li-m</ta>
            <ta e="T101" id="Seg_2640" s="T100">dĭgəttə</ta>
            <ta e="T102" id="Seg_2641" s="T101">erte-n</ta>
            <ta e="T103" id="Seg_2642" s="T102">uʔbdə-bi</ta>
            <ta e="T104" id="Seg_2643" s="T103">vărota-t-si</ta>
            <ta e="T107" id="Seg_2644" s="T106">kaj-luʔ-pi-ʔi</ta>
            <ta e="T108" id="Seg_2645" s="T107">i</ta>
            <ta e="T109" id="Seg_2646" s="T108">šo-bi</ta>
            <ta e="T110" id="Seg_2647" s="T109">maːʔ-ndə</ta>
            <ta e="T111" id="Seg_2648" s="T110">deʔ-pi</ta>
            <ta e="T112" id="Seg_2649" s="T111">bar</ta>
            <ta e="T113" id="Seg_2650" s="T112">mĭ-bi</ta>
            <ta e="T114" id="Seg_2651" s="T113">nagur-göʔ</ta>
            <ta e="T115" id="Seg_2652" s="T114">a</ta>
            <ta e="T116" id="Seg_2653" s="T115">tănan</ta>
            <ta e="T117" id="Seg_2654" s="T116">nada</ta>
            <ta e="T118" id="Seg_2655" s="T117">dĭbər</ta>
            <ta e="T119" id="Seg_2656" s="T118">kan-zittə</ta>
            <ta e="T120" id="Seg_2657" s="T119">dĭ</ta>
            <ta e="T121" id="Seg_2658" s="T120">măn-də</ta>
            <ta e="T122" id="Seg_2659" s="T121">nu</ta>
            <ta e="T123" id="Seg_2660" s="T122">i</ta>
            <ta e="T124" id="Seg_2661" s="T123">što</ta>
            <ta e="T125" id="Seg_2662" s="T124">măn</ta>
            <ta e="T126" id="Seg_2663" s="T125">ka-la-m</ta>
            <ta e="T127" id="Seg_2664" s="T126">dĭn</ta>
            <ta e="T128" id="Seg_2665" s="T127">iʔgö</ta>
            <ta e="T129" id="Seg_2666" s="T128">sʼvʼetog-əʔi</ta>
            <ta e="T130" id="Seg_2667" s="T129">bar</ta>
            <ta e="T131" id="Seg_2668" s="T130">nĭŋgə-lə-l</ta>
            <ta e="T132" id="Seg_2669" s="T131">deʔ-pi</ta>
            <ta e="T133" id="Seg_2670" s="T132">öʔlu-bi</ta>
            <ta e="T134" id="Seg_2671" s="T133">dĭ-m</ta>
            <ta e="T135" id="Seg_2672" s="T134">dĭbər</ta>
            <ta e="T136" id="Seg_2673" s="T135">vărota-ʔi</ta>
            <ta e="T137" id="Seg_2674" s="T136">kaj-luʔ-pi-ʔi</ta>
            <ta e="T138" id="Seg_2675" s="T137">dĭ-m</ta>
            <ta e="T139" id="Seg_2676" s="T138">dĭ</ta>
            <ta e="T142" id="Seg_2677" s="T141">ma-luʔ-pi</ta>
            <ta e="T143" id="Seg_2678" s="T142">i</ta>
            <ta e="T144" id="Seg_2679" s="T143">maʔ-ndə</ta>
            <ta e="T145" id="Seg_2680" s="T144">šo-bi</ta>
            <ta e="T147" id="Seg_2681" s="T146">dʼala</ta>
            <ta e="T148" id="Seg_2682" s="T147">mĭm-bi</ta>
            <ta e="T149" id="Seg_2683" s="T148">mĭm-bi</ta>
            <ta e="T150" id="Seg_2684" s="T149">sʼvʼetog-əʔi</ta>
            <ta e="T151" id="Seg_2685" s="T150">üge</ta>
            <ta e="T152" id="Seg_2686" s="T151">pʼaŋd-o-na</ta>
            <ta e="T153" id="Seg_2687" s="T152">Malanʼnʼa-n</ta>
            <ta e="T154" id="Seg_2688" s="T153">ĭmbi</ta>
            <ta e="T155" id="Seg_2689" s="T154">mĭnd-lə-j</ta>
            <ta e="T156" id="Seg_2690" s="T155">üge</ta>
            <ta e="T157" id="Seg_2691" s="T156">Malanʼnʼa</ta>
            <ta e="T158" id="Seg_2692" s="T157">tura-nə</ta>
            <ta e="T159" id="Seg_2693" s="T158">šo-bi</ta>
            <ta e="T160" id="Seg_2694" s="T159">bar</ta>
            <ta e="T161" id="Seg_2695" s="T160">baza-ʔi</ta>
            <ta e="T162" id="Seg_2696" s="T161">girgit</ta>
            <ta e="T163" id="Seg_2697" s="T162">šamnag-əʔi</ta>
            <ta e="T164" id="Seg_2698" s="T163">üge</ta>
            <ta e="T165" id="Seg_2699" s="T164">dĭn</ta>
            <ta e="T166" id="Seg_2700" s="T165">Malanʼnʼa-n</ta>
            <ta e="T167" id="Seg_2701" s="T166">daga-ʔi</ta>
            <ta e="T169" id="Seg_2702" s="T168">dĭgəttə</ta>
            <ta e="T170" id="Seg_2703" s="T169">amor-bi</ta>
            <ta e="T171" id="Seg_2704" s="T170">am-bi</ta>
            <ta e="T173" id="Seg_2705" s="T172">măna</ta>
            <ta e="T176" id="Seg_2706" s="T175">amno-sʼtə</ta>
            <ta e="T177" id="Seg_2707" s="T176">dĭn</ta>
            <ta e="T178" id="Seg_2708" s="T177">tö</ta>
            <ta e="T182" id="Seg_2709" s="T181">dĭgəttə</ta>
            <ta e="T183" id="Seg_2710" s="T182">iʔbə-bi</ta>
            <ta e="T184" id="Seg_2711" s="T183">kunol-zittə</ta>
            <ta e="T185" id="Seg_2712" s="T184">nüdʼi-n</ta>
            <ta e="T186" id="Seg_2713" s="T185">šo-bi</ta>
            <ta e="T187" id="Seg_2714" s="T186">kuza</ta>
            <ta e="T188" id="Seg_2715" s="T187">tăn</ta>
            <ta e="T189" id="Seg_2716" s="T188">Malanʼnʼa</ta>
            <ta e="T190" id="Seg_2717" s="T189">tăn</ta>
            <ta e="T191" id="Seg_2718" s="T190">măna</ta>
            <ta e="T192" id="Seg_2719" s="T191">ajir-ia-l</ta>
            <ta e="T193" id="Seg_2720" s="T192">a</ta>
            <ta e="T194" id="Seg_2721" s="T193">măn</ta>
            <ta e="T195" id="Seg_2722" s="T194">en</ta>
            <ta e="T197" id="Seg_2723" s="T196">ej</ta>
            <ta e="T198" id="Seg_2724" s="T197">tĭmne-m</ta>
            <ta e="T200" id="Seg_2725" s="T199">šindi</ta>
            <ta e="T201" id="Seg_2726" s="T200">tăn</ta>
            <ta e="T202" id="Seg_2727" s="T201">šo-ʔ</ta>
            <ta e="T203" id="Seg_2728" s="T202">döbər</ta>
            <ta e="T204" id="Seg_2729" s="T203">dʼok</ta>
            <ta e="T205" id="Seg_2730" s="T204">măn</ta>
            <ta e="T206" id="Seg_2731" s="T205">ej</ta>
            <ta e="T207" id="Seg_2732" s="T206">šo-la-m</ta>
            <ta e="T208" id="Seg_2733" s="T207">tăn</ta>
            <ta e="T209" id="Seg_2734" s="T208">nereʔ-luʔ-li-l</ta>
            <ta e="T210" id="Seg_2735" s="T209">dĭgəttə</ta>
            <ta e="T396" id="Seg_2736" s="T210">kal-la</ta>
            <ta e="T211" id="Seg_2737" s="T396">dʼür-bi</ta>
            <ta e="T212" id="Seg_2738" s="T211">dĭ</ta>
            <ta e="T213" id="Seg_2739" s="T212">tenö-bi</ta>
            <ta e="T214" id="Seg_2740" s="T213">tenö-bi</ta>
            <ta e="T215" id="Seg_2741" s="T214">dĭgəttə</ta>
            <ta e="T216" id="Seg_2742" s="T215">šide</ta>
            <ta e="T217" id="Seg_2743" s="T216">nüdʼi</ta>
            <ta e="T218" id="Seg_2744" s="T217">šo-bi</ta>
            <ta e="T219" id="Seg_2745" s="T218">dĭ</ta>
            <ta e="T220" id="Seg_2746" s="T219">bar</ta>
            <ta e="T221" id="Seg_2747" s="T220">bü-nə</ta>
            <ta e="T222" id="Seg_2748" s="T221">kam-bi</ta>
            <ta e="T223" id="Seg_2749" s="T222">kola</ta>
            <ta e="T224" id="Seg_2750" s="T223">dĭn</ta>
            <ta e="T225" id="Seg_2751" s="T224">i</ta>
            <ta e="T226" id="Seg_2752" s="T225">to</ta>
            <ta e="T227" id="Seg_2753" s="T226">pʼaŋd-o-na</ta>
            <ta e="T228" id="Seg_2754" s="T227">Malanʼnʼa-n</ta>
            <ta e="T229" id="Seg_2755" s="T228">kola</ta>
            <ta e="T230" id="Seg_2756" s="T229">dĭgəttə</ta>
            <ta e="T231" id="Seg_2757" s="T230">dĭ</ta>
            <ta e="T232" id="Seg_2758" s="T231">bazoʔ</ta>
            <ta e="T233" id="Seg_2759" s="T232">nüdʼi-t</ta>
            <ta e="T234" id="Seg_2760" s="T233">šo-bi</ta>
            <ta e="T235" id="Seg_2761" s="T234">tenö-bi</ta>
            <ta e="T236" id="Seg_2762" s="T235">tenö-bi</ta>
            <ta e="T237" id="Seg_2763" s="T236">dĭgəttə</ta>
            <ta e="T238" id="Seg_2764" s="T237">măn-də</ta>
            <ta e="T239" id="Seg_2765" s="T238">ej</ta>
            <ta e="T240" id="Seg_2766" s="T239">tĭmne-m</ta>
            <ta e="T241" id="Seg_2767" s="T240">šindi</ta>
            <ta e="T243" id="Seg_2768" s="T242">šində</ta>
            <ta e="T244" id="Seg_2769" s="T243">tăn</ta>
            <ta e="T245" id="Seg_2770" s="T244">i-ge-l</ta>
            <ta e="T246" id="Seg_2771" s="T245">dʼok</ta>
            <ta e="T247" id="Seg_2772" s="T246">măn</ta>
            <ta e="T248" id="Seg_2773" s="T247">e-m</ta>
            <ta e="T249" id="Seg_2774" s="T248">šo-ʔ</ta>
            <ta e="T250" id="Seg_2775" s="T249">tăn</ta>
            <ta e="T251" id="Seg_2776" s="T250">bazoʔ</ta>
            <ta e="T252" id="Seg_2777" s="T251">nereʔ-luʔ-pi-l</ta>
            <ta e="T253" id="Seg_2778" s="T252">dĭgəttə</ta>
            <ta e="T254" id="Seg_2779" s="T253">nagur</ta>
            <ta e="T255" id="Seg_2780" s="T254">nüdʼi</ta>
            <ta e="T256" id="Seg_2781" s="T255">kam-bi</ta>
            <ta e="T257" id="Seg_2782" s="T256">dĭ-m</ta>
            <ta e="T258" id="Seg_2783" s="T257">šo-bi</ta>
            <ta e="T259" id="Seg_2784" s="T258">nagur</ta>
            <ta e="T260" id="Seg_2785" s="T259">lĭd</ta>
            <ta e="T261" id="Seg_2786" s="T260">nüdʼi</ta>
            <ta e="T262" id="Seg_2787" s="T261">dĭ</ta>
            <ta e="T263" id="Seg_2788" s="T262">bazoʔ</ta>
            <ta e="T264" id="Seg_2789" s="T263">iʔbə-bi</ta>
            <ta e="T265" id="Seg_2790" s="T264">kunol-zittə</ta>
            <ta e="T266" id="Seg_2791" s="T265">dĭ</ta>
            <ta e="T267" id="Seg_2792" s="T266">bazoʔ</ta>
            <ta e="T268" id="Seg_2793" s="T267">šo-bi</ta>
            <ta e="T269" id="Seg_2794" s="T268">surar-laʔbə</ta>
            <ta e="T270" id="Seg_2795" s="T269">no</ta>
            <ta e="T271" id="Seg_2796" s="T270">tenö-luʔ-pi-l</ta>
            <ta e="T272" id="Seg_2797" s="T271">da</ta>
            <ta e="T273" id="Seg_2798" s="T272">šo-ʔ</ta>
            <ta e="T274" id="Seg_2799" s="T273">döber</ta>
            <ta e="T275" id="Seg_2800" s="T274">măn</ta>
            <ta e="T276" id="Seg_2801" s="T275">măndə-r-li-m</ta>
            <ta e="T277" id="Seg_2802" s="T276">tănan</ta>
            <ta e="T278" id="Seg_2803" s="T277">da</ta>
            <ta e="T279" id="Seg_2804" s="T278">tăn</ta>
            <ta e="T280" id="Seg_2805" s="T279">nereʔ-luʔ-li-l</ta>
            <ta e="T281" id="Seg_2806" s="T280">dĭ</ta>
            <ta e="T282" id="Seg_2807" s="T281">măn-də</ta>
            <ta e="T283" id="Seg_2808" s="T282">ej</ta>
            <ta e="T284" id="Seg_2809" s="T283">nereʔ-luʔ-li-m</ta>
            <ta e="T285" id="Seg_2810" s="T284">dĭ</ta>
            <ta e="T286" id="Seg_2811" s="T285">šo-bi</ta>
            <ta e="T287" id="Seg_2812" s="T286">tura-nə</ta>
            <ta e="T288" id="Seg_2813" s="T287">dĭ</ta>
            <ta e="T289" id="Seg_2814" s="T288">ej</ta>
            <ta e="T290" id="Seg_2815" s="T289">nereʔ-luʔ-pi</ta>
            <ta e="T291" id="Seg_2816" s="T290">măn-lia</ta>
            <ta e="T292" id="Seg_2817" s="T291">amno-la-m</ta>
            <ta e="T293" id="Seg_2818" s="T292">dĭ-zi</ta>
            <ta e="T294" id="Seg_2819" s="T293">ĭmbi</ta>
            <ta e="T296" id="Seg_2820" s="T295">pušaj</ta>
            <ta e="T297" id="Seg_2821" s="T296">ej</ta>
            <ta e="T298" id="Seg_2822" s="T297">kuvas</ta>
            <ta e="T299" id="Seg_2823" s="T298">da</ta>
            <ta e="T300" id="Seg_2824" s="T299">dön</ta>
            <ta e="T301" id="Seg_2825" s="T300">bar</ta>
            <ta e="T302" id="Seg_2826" s="T301">măna</ta>
            <ta e="T303" id="Seg_2827" s="T302">i</ta>
            <ta e="T304" id="Seg_2828" s="T303">măna</ta>
            <ta e="T305" id="Seg_2829" s="T304">dĭgəttə</ta>
            <ta e="T306" id="Seg_2830" s="T305">amno-bi</ta>
            <ta e="T307" id="Seg_2831" s="T306">amno-bi</ta>
            <ta e="T308" id="Seg_2832" s="T307">măn-də</ta>
            <ta e="T309" id="Seg_2833" s="T308">măn</ta>
            <ta e="T310" id="Seg_2834" s="T309">ka-la-m</ta>
            <ta e="T312" id="Seg_2835" s="T311">aba-nə</ta>
            <ta e="T313" id="Seg_2836" s="T312">dĭ</ta>
            <ta e="T314" id="Seg_2837" s="T313">dĭʔ-nə</ta>
            <ta e="T316" id="Seg_2838" s="T315">kălʼso</ta>
            <ta e="T317" id="Seg_2839" s="T316">mĭ-bi</ta>
            <ta e="T318" id="Seg_2840" s="T317">no</ta>
            <ta e="T319" id="Seg_2841" s="T318">kan-a-ʔ</ta>
            <ta e="T320" id="Seg_2842" s="T319">kamen</ta>
            <ta e="T321" id="Seg_2843" s="T320">dĭ</ta>
            <ta e="T322" id="Seg_2844" s="T321">kălʼso</ta>
            <ta e="T323" id="Seg_2845" s="T322">sagər</ta>
            <ta e="T324" id="Seg_2846" s="T323">mo-lə-j</ta>
            <ta e="T325" id="Seg_2847" s="T324">dĭgəttə</ta>
            <ta e="T326" id="Seg_2848" s="T325">büžü</ta>
            <ta e="T327" id="Seg_2849" s="T326">šo-ʔ</ta>
            <ta e="T328" id="Seg_2850" s="T327">ato</ta>
            <ta e="T329" id="Seg_2851" s="T328">măn</ta>
            <ta e="T330" id="Seg_2852" s="T329">kü-lal-lə-m</ta>
            <ta e="T331" id="Seg_2853" s="T330">dĭ</ta>
            <ta e="T332" id="Seg_2854" s="T331">kam-bi</ta>
            <ta e="T333" id="Seg_2855" s="T332">sʼestra-ttə</ta>
            <ta e="T334" id="Seg_2856" s="T333">bar</ta>
            <ta e="T335" id="Seg_2857" s="T334">dĭʔ-nə</ta>
            <ta e="T337" id="Seg_2858" s="T336">bü</ta>
            <ta e="T338" id="Seg_2859" s="T337">girgit=də</ta>
            <ta e="T339" id="Seg_2860" s="T338">mĭ-bi</ta>
            <ta e="T340" id="Seg_2861" s="T339">tĭ</ta>
            <ta e="T341" id="Seg_2862" s="T340">bar</ta>
            <ta e="T342" id="Seg_2863" s="T341">kunol-bi</ta>
            <ta e="T343" id="Seg_2864" s="T342">kunol-bi</ta>
            <ta e="T344" id="Seg_2865" s="T343">uʔbdə-bi</ta>
            <ta e="T345" id="Seg_2866" s="T344">kălʼečkă-t</ta>
            <ta e="T346" id="Seg_2867" s="T345">bar</ta>
            <ta e="T347" id="Seg_2868" s="T346">sagər</ta>
            <ta e="T348" id="Seg_2869" s="T347">dĭ</ta>
            <ta e="T349" id="Seg_2870" s="T348">bar</ta>
            <ta e="T350" id="Seg_2871" s="T349">ĭmbi</ta>
            <ta e="T351" id="Seg_2872" s="T350">baruʔ-pi</ta>
            <ta e="T352" id="Seg_2873" s="T351">i</ta>
            <ta e="T353" id="Seg_2874" s="T352">kam-bi</ta>
            <ta e="T354" id="Seg_2875" s="T353">šo-bi</ta>
            <ta e="T355" id="Seg_2876" s="T354">dĭbər</ta>
            <ta e="T356" id="Seg_2877" s="T355">iʔbo-laʔbə</ta>
            <ta e="T357" id="Seg_2878" s="T356">kü-lam-bi</ta>
            <ta e="T358" id="Seg_2879" s="T357">bar</ta>
            <ta e="T359" id="Seg_2880" s="T358">dʼor-bi</ta>
            <ta e="T360" id="Seg_2881" s="T359">măn</ta>
            <ta e="T361" id="Seg_2882" s="T360">tănan</ta>
            <ta e="T362" id="Seg_2883" s="T361">ajir-lio-m</ta>
            <ta e="T363" id="Seg_2884" s="T362">uʔbda-ʔ</ta>
            <ta e="T364" id="Seg_2885" s="T363">ajir-lio-m</ta>
            <ta e="T365" id="Seg_2886" s="T364">uʔbda-ʔ</ta>
            <ta e="T366" id="Seg_2887" s="T365">dĭ</ta>
            <ta e="T367" id="Seg_2888" s="T366">uʔbdə-bi</ta>
            <ta e="T368" id="Seg_2889" s="T367">i</ta>
            <ta e="T369" id="Seg_2890" s="T368">ugandə</ta>
            <ta e="T370" id="Seg_2891" s="T369">kuvas</ta>
            <ta e="T371" id="Seg_2892" s="T370">nʼi</ta>
            <ta e="T372" id="Seg_2893" s="T371">mo-lam-bi</ta>
            <ta e="T373" id="Seg_2894" s="T372">dĭgəttə</ta>
            <ta e="T374" id="Seg_2895" s="T373">koŋgoro-ʔi</ta>
            <ta e="T375" id="Seg_2896" s="T374">bar</ta>
            <ta e="T376" id="Seg_2897" s="T375">küzür-leʔbə-ʔjə</ta>
            <ta e="T377" id="Seg_2898" s="T376">il</ta>
            <ta e="T378" id="Seg_2899" s="T377">mĭl-leʔbə-ʔjə</ta>
            <ta e="T379" id="Seg_2900" s="T378">mo-lam-bi</ta>
            <ta e="T380" id="Seg_2901" s="T379">gorăt</ta>
            <ta e="T381" id="Seg_2902" s="T380">a</ta>
            <ta e="T382" id="Seg_2903" s="T381">dĭ</ta>
            <ta e="T383" id="Seg_2904" s="T382">dĭ</ta>
            <ta e="T384" id="Seg_2905" s="T383">nʼi</ta>
            <ta e="T385" id="Seg_2906" s="T384">koŋ</ta>
            <ta e="T386" id="Seg_2907" s="T385">mo-lam-bi</ta>
            <ta e="T387" id="Seg_2908" s="T386">dĭgəttə</ta>
            <ta e="T388" id="Seg_2909" s="T387">dĭ</ta>
            <ta e="T390" id="Seg_2910" s="T389">dĭ-m</ta>
            <ta e="T391" id="Seg_2911" s="T390">tibi-nə</ta>
            <ta e="T392" id="Seg_2912" s="T391">i-bi</ta>
            <ta e="T393" id="Seg_2913" s="T392">i</ta>
            <ta e="T395" id="Seg_2914" s="T394">amno-zittə</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_2915" s="T0">amno-bi</ta>
            <ta e="T2" id="Seg_2916" s="T1">büzʼe</ta>
            <ta e="T3" id="Seg_2917" s="T2">dĭ-n</ta>
            <ta e="T6" id="Seg_2918" s="T5">dĭ-n</ta>
            <ta e="T7" id="Seg_2919" s="T6">i-bi-jəʔ</ta>
            <ta e="T8" id="Seg_2920" s="T7">nagur</ta>
            <ta e="T9" id="Seg_2921" s="T8">koʔbdo-zAŋ-də</ta>
            <ta e="T10" id="Seg_2922" s="T9">dĭ</ta>
            <ta e="T11" id="Seg_2923" s="T10">stal</ta>
            <ta e="T13" id="Seg_2924" s="T12">oʔbdə-zittə</ta>
            <ta e="T14" id="Seg_2925" s="T13">gorăt-Tə</ta>
            <ta e="T15" id="Seg_2926" s="T14">kan-zittə</ta>
            <ta e="T16" id="Seg_2927" s="T15">urgo</ta>
            <ta e="T17" id="Seg_2928" s="T16">koʔbdo-gəndə</ta>
            <ta e="T18" id="Seg_2929" s="T17">măn-ntə</ta>
            <ta e="T19" id="Seg_2930" s="T18">ĭmbi</ta>
            <ta e="T20" id="Seg_2931" s="T19">tănan</ta>
            <ta e="T21" id="Seg_2932" s="T20">i-zittə</ta>
            <ta e="T22" id="Seg_2933" s="T21">tăn</ta>
            <ta e="T23" id="Seg_2934" s="T22">măna</ta>
            <ta e="T24" id="Seg_2935" s="T23">kuvas</ta>
            <ta e="T25" id="Seg_2936" s="T24">platʼtʼa</ta>
            <ta e="T26" id="Seg_2937" s="T25">i-ʔ</ta>
            <ta e="T27" id="Seg_2938" s="T26">a</ta>
            <ta e="T28" id="Seg_2939" s="T27">üdʼüge-t</ta>
            <ta e="T29" id="Seg_2940" s="T28">koʔbdo</ta>
            <ta e="T30" id="Seg_2941" s="T29">tăn</ta>
            <ta e="T31" id="Seg_2942" s="T30">măna</ta>
            <ta e="T32" id="Seg_2943" s="T31">bătʼinkă-jəʔ</ta>
            <ta e="T33" id="Seg_2944" s="T32">kuvas</ta>
            <ta e="T34" id="Seg_2945" s="T33">i-ʔ</ta>
            <ta e="T35" id="Seg_2946" s="T34">a</ta>
            <ta e="T36" id="Seg_2947" s="T35">nagur</ta>
            <ta e="T37" id="Seg_2948" s="T36">ĭššo</ta>
            <ta e="T38" id="Seg_2949" s="T37">üdʼüge</ta>
            <ta e="T39" id="Seg_2950" s="T38">koʔbdo</ta>
            <ta e="T40" id="Seg_2951" s="T39">tănan</ta>
            <ta e="T41" id="Seg_2952" s="T40">ĭmbi</ta>
            <ta e="T42" id="Seg_2953" s="T41">i-zittə</ta>
            <ta e="T43" id="Seg_2954" s="T42">tăn</ta>
            <ta e="T44" id="Seg_2955" s="T43">măna</ta>
            <ta e="T45" id="Seg_2956" s="T44">i-ʔ</ta>
            <ta e="T46" id="Seg_2957" s="T45">kömə</ta>
            <ta e="T47" id="Seg_2958" s="T46">svetok</ta>
            <ta e="T48" id="Seg_2959" s="T47">dĭ</ta>
            <ta e="T49" id="Seg_2960" s="T48">kan-bi</ta>
            <ta e="T50" id="Seg_2961" s="T49">măndo-r-bi</ta>
            <ta e="T51" id="Seg_2962" s="T50">măndo-r-bi</ta>
            <ta e="T52" id="Seg_2963" s="T51">i-bi</ta>
            <ta e="T53" id="Seg_2964" s="T52">döʔnə</ta>
            <ta e="T54" id="Seg_2965" s="T53">platʼtʼa</ta>
            <ta e="T55" id="Seg_2966" s="T54">onʼiʔ-Tə</ta>
            <ta e="T56" id="Seg_2967" s="T55">i-bi</ta>
            <ta e="T57" id="Seg_2968" s="T56">bătʼinkă-jəʔ</ta>
            <ta e="T58" id="Seg_2969" s="T57">a</ta>
            <ta e="T60" id="Seg_2970" s="T59">svetok</ta>
            <ta e="T61" id="Seg_2971" s="T60">naga</ta>
            <ta e="T62" id="Seg_2972" s="T61">dĭgəttə</ta>
            <ta e="T63" id="Seg_2973" s="T62">šonə-gA</ta>
            <ta e="T64" id="Seg_2974" s="T63">i</ta>
            <ta e="T65" id="Seg_2975" s="T64">tenö-laʔbə</ta>
            <ta e="T66" id="Seg_2976" s="T65">gijen</ta>
            <ta e="T68" id="Seg_2977" s="T67">i-zittə</ta>
            <ta e="T69" id="Seg_2978" s="T68">kömə</ta>
            <ta e="T70" id="Seg_2979" s="T69">svetok</ta>
            <ta e="T71" id="Seg_2980" s="T70">šonə-gA</ta>
            <ta e="T72" id="Seg_2981" s="T71">ku-bi</ta>
            <ta e="T73" id="Seg_2982" s="T72">bar</ta>
            <ta e="T74" id="Seg_2983" s="T73">košideŋ-Kən</ta>
            <ta e="T75" id="Seg_2984" s="T74">svetok-jəʔ</ta>
            <ta e="T76" id="Seg_2985" s="T75">dĭbər</ta>
            <ta e="T77" id="Seg_2986" s="T76">kan-bi</ta>
            <ta e="T78" id="Seg_2987" s="T77">nĭŋgə-bi</ta>
            <ta e="T79" id="Seg_2988" s="T78">varota-jəʔ</ta>
            <ta e="T81" id="Seg_2989" s="T80">kaj-luʔbdə-bi-jəʔ</ta>
            <ta e="T82" id="Seg_2990" s="T81">dĭgəttə</ta>
            <ta e="T83" id="Seg_2991" s="T82">mĭn-bi</ta>
            <ta e="T84" id="Seg_2992" s="T83">mĭn-bi</ta>
            <ta e="T85" id="Seg_2993" s="T84">dunʼtʼigə</ta>
            <ta e="T86" id="Seg_2994" s="T85">nüdʼi-n</ta>
            <ta e="T87" id="Seg_2995" s="T86">šo-bi</ta>
            <ta e="T88" id="Seg_2996" s="T87">kuza</ta>
            <ta e="T90" id="Seg_2997" s="T89">bos-l</ta>
            <ta e="T91" id="Seg_2998" s="T90">koʔbdo-l</ta>
            <ta e="T92" id="Seg_2999" s="T91">mĭ-lV-l</ta>
            <ta e="T93" id="Seg_3000" s="T92">măna</ta>
            <ta e="T95" id="Seg_3001" s="T94">det-lV-l</ta>
            <ta e="T96" id="Seg_3002" s="T95">döbər</ta>
            <ta e="T97" id="Seg_3003" s="T96">dĭ</ta>
            <ta e="T98" id="Seg_3004" s="T97">tenö-bi</ta>
            <ta e="T99" id="Seg_3005" s="T98">tenö-bi</ta>
            <ta e="T100" id="Seg_3006" s="T99">det-lV-m</ta>
            <ta e="T101" id="Seg_3007" s="T100">dĭgəttə</ta>
            <ta e="T102" id="Seg_3008" s="T101">ertə-n</ta>
            <ta e="T103" id="Seg_3009" s="T102">uʔbdə-bi</ta>
            <ta e="T104" id="Seg_3010" s="T103">varota-t-ziʔ</ta>
            <ta e="T107" id="Seg_3011" s="T106">kaj-luʔbdə-bi-jəʔ</ta>
            <ta e="T108" id="Seg_3012" s="T107">i</ta>
            <ta e="T109" id="Seg_3013" s="T108">šo-bi</ta>
            <ta e="T110" id="Seg_3014" s="T109">maʔ-gəndə</ta>
            <ta e="T111" id="Seg_3015" s="T110">det-bi</ta>
            <ta e="T112" id="Seg_3016" s="T111">bar</ta>
            <ta e="T113" id="Seg_3017" s="T112">mĭ-bi</ta>
            <ta e="T114" id="Seg_3018" s="T113">nagur-göʔ</ta>
            <ta e="T115" id="Seg_3019" s="T114">a</ta>
            <ta e="T116" id="Seg_3020" s="T115">tănan</ta>
            <ta e="T117" id="Seg_3021" s="T116">nadə</ta>
            <ta e="T118" id="Seg_3022" s="T117">dĭbər</ta>
            <ta e="T119" id="Seg_3023" s="T118">kan-zittə</ta>
            <ta e="T120" id="Seg_3024" s="T119">dĭ</ta>
            <ta e="T121" id="Seg_3025" s="T120">măn-ntə</ta>
            <ta e="T122" id="Seg_3026" s="T121">nu</ta>
            <ta e="T123" id="Seg_3027" s="T122">i</ta>
            <ta e="T124" id="Seg_3028" s="T123">što</ta>
            <ta e="T125" id="Seg_3029" s="T124">măn</ta>
            <ta e="T126" id="Seg_3030" s="T125">kan-lV-m</ta>
            <ta e="T127" id="Seg_3031" s="T126">dĭn</ta>
            <ta e="T128" id="Seg_3032" s="T127">iʔgö</ta>
            <ta e="T129" id="Seg_3033" s="T128">svetok-jəʔ</ta>
            <ta e="T130" id="Seg_3034" s="T129">bar</ta>
            <ta e="T131" id="Seg_3035" s="T130">nĭŋgə-lV-l</ta>
            <ta e="T132" id="Seg_3036" s="T131">det-bi</ta>
            <ta e="T133" id="Seg_3037" s="T132">öʔlu-bi</ta>
            <ta e="T134" id="Seg_3038" s="T133">dĭ-m</ta>
            <ta e="T135" id="Seg_3039" s="T134">dĭbər</ta>
            <ta e="T136" id="Seg_3040" s="T135">varota-jəʔ</ta>
            <ta e="T137" id="Seg_3041" s="T136">kaj-luʔbdə-bi-jəʔ</ta>
            <ta e="T138" id="Seg_3042" s="T137">dĭ-m</ta>
            <ta e="T139" id="Seg_3043" s="T138">dĭ</ta>
            <ta e="T142" id="Seg_3044" s="T141">ma-luʔbdə-bi</ta>
            <ta e="T143" id="Seg_3045" s="T142">i</ta>
            <ta e="T144" id="Seg_3046" s="T143">maʔ-gəndə</ta>
            <ta e="T145" id="Seg_3047" s="T144">šo-bi</ta>
            <ta e="T147" id="Seg_3048" s="T146">tʼala</ta>
            <ta e="T148" id="Seg_3049" s="T147">mĭn-bi</ta>
            <ta e="T149" id="Seg_3050" s="T148">mĭn-bi</ta>
            <ta e="T150" id="Seg_3051" s="T149">svetok-jəʔ</ta>
            <ta e="T151" id="Seg_3052" s="T150">üge</ta>
            <ta e="T152" id="Seg_3053" s="T151">pʼaŋdə-o-NTA</ta>
            <ta e="T153" id="Seg_3054" s="T152">Malanʼnʼa-n</ta>
            <ta e="T154" id="Seg_3055" s="T153">ĭmbi</ta>
            <ta e="T155" id="Seg_3056" s="T154">mĭn-lV-j</ta>
            <ta e="T156" id="Seg_3057" s="T155">üge</ta>
            <ta e="T157" id="Seg_3058" s="T156">Malanʼnʼa</ta>
            <ta e="T158" id="Seg_3059" s="T157">tura-Tə</ta>
            <ta e="T159" id="Seg_3060" s="T158">šo-bi</ta>
            <ta e="T160" id="Seg_3061" s="T159">bar</ta>
            <ta e="T161" id="Seg_3062" s="T160">baza-jəʔ</ta>
            <ta e="T162" id="Seg_3063" s="T161">girgit</ta>
            <ta e="T163" id="Seg_3064" s="T162">šamnak-jəʔ</ta>
            <ta e="T164" id="Seg_3065" s="T163">üge</ta>
            <ta e="T165" id="Seg_3066" s="T164">dĭn</ta>
            <ta e="T166" id="Seg_3067" s="T165">Malanʼnʼa-n</ta>
            <ta e="T167" id="Seg_3068" s="T166">tagaj-jəʔ</ta>
            <ta e="T169" id="Seg_3069" s="T168">dĭgəttə</ta>
            <ta e="T170" id="Seg_3070" s="T169">amor-bi</ta>
            <ta e="T171" id="Seg_3071" s="T170">am-bi</ta>
            <ta e="T173" id="Seg_3072" s="T172">măna</ta>
            <ta e="T176" id="Seg_3073" s="T175">amno-zittə</ta>
            <ta e="T177" id="Seg_3074" s="T176">dĭn</ta>
            <ta e="T178" id="Seg_3075" s="T177">tö</ta>
            <ta e="T182" id="Seg_3076" s="T181">dĭgəttə</ta>
            <ta e="T183" id="Seg_3077" s="T182">iʔbə-bi</ta>
            <ta e="T184" id="Seg_3078" s="T183">kunol-zittə</ta>
            <ta e="T185" id="Seg_3079" s="T184">nüdʼi-n</ta>
            <ta e="T186" id="Seg_3080" s="T185">šo-bi</ta>
            <ta e="T187" id="Seg_3081" s="T186">kuza</ta>
            <ta e="T188" id="Seg_3082" s="T187">tăn</ta>
            <ta e="T189" id="Seg_3083" s="T188">Malanʼnʼa</ta>
            <ta e="T190" id="Seg_3084" s="T189">tăn</ta>
            <ta e="T191" id="Seg_3085" s="T190">măna</ta>
            <ta e="T192" id="Seg_3086" s="T191">ajir-liA-l</ta>
            <ta e="T193" id="Seg_3087" s="T192">a</ta>
            <ta e="T194" id="Seg_3088" s="T193">măn</ta>
            <ta e="T195" id="Seg_3089" s="T194">hen</ta>
            <ta e="T197" id="Seg_3090" s="T196">ej</ta>
            <ta e="T198" id="Seg_3091" s="T197">tĭmne-m</ta>
            <ta e="T200" id="Seg_3092" s="T199">šində</ta>
            <ta e="T201" id="Seg_3093" s="T200">tăn</ta>
            <ta e="T202" id="Seg_3094" s="T201">šo-ʔ</ta>
            <ta e="T203" id="Seg_3095" s="T202">döbər</ta>
            <ta e="T204" id="Seg_3096" s="T203">dʼok</ta>
            <ta e="T205" id="Seg_3097" s="T204">măn</ta>
            <ta e="T206" id="Seg_3098" s="T205">ej</ta>
            <ta e="T207" id="Seg_3099" s="T206">šo-lV-m</ta>
            <ta e="T208" id="Seg_3100" s="T207">tăn</ta>
            <ta e="T209" id="Seg_3101" s="T208">nereʔ-luʔbdə-lV-l</ta>
            <ta e="T210" id="Seg_3102" s="T209">dĭgəttə</ta>
            <ta e="T396" id="Seg_3103" s="T210">kan-lAʔ</ta>
            <ta e="T211" id="Seg_3104" s="T396">tʼür-bi</ta>
            <ta e="T212" id="Seg_3105" s="T211">dĭ</ta>
            <ta e="T213" id="Seg_3106" s="T212">tenö-bi</ta>
            <ta e="T214" id="Seg_3107" s="T213">tenö-bi</ta>
            <ta e="T215" id="Seg_3108" s="T214">dĭgəttə</ta>
            <ta e="T216" id="Seg_3109" s="T215">šide</ta>
            <ta e="T217" id="Seg_3110" s="T216">nüdʼi</ta>
            <ta e="T218" id="Seg_3111" s="T217">šo-bi</ta>
            <ta e="T219" id="Seg_3112" s="T218">dĭ</ta>
            <ta e="T220" id="Seg_3113" s="T219">bar</ta>
            <ta e="T221" id="Seg_3114" s="T220">bü-Tə</ta>
            <ta e="T222" id="Seg_3115" s="T221">kan-bi</ta>
            <ta e="T223" id="Seg_3116" s="T222">kola</ta>
            <ta e="T224" id="Seg_3117" s="T223">dĭn</ta>
            <ta e="T225" id="Seg_3118" s="T224">i</ta>
            <ta e="T226" id="Seg_3119" s="T225">to</ta>
            <ta e="T227" id="Seg_3120" s="T226">pʼaŋdə-o-NTA</ta>
            <ta e="T228" id="Seg_3121" s="T227">Malanʼnʼa-n</ta>
            <ta e="T229" id="Seg_3122" s="T228">kola</ta>
            <ta e="T230" id="Seg_3123" s="T229">dĭgəttə</ta>
            <ta e="T231" id="Seg_3124" s="T230">dĭ</ta>
            <ta e="T232" id="Seg_3125" s="T231">bazoʔ</ta>
            <ta e="T233" id="Seg_3126" s="T232">nüdʼi-t</ta>
            <ta e="T234" id="Seg_3127" s="T233">šo-bi</ta>
            <ta e="T235" id="Seg_3128" s="T234">tenö-bi</ta>
            <ta e="T236" id="Seg_3129" s="T235">tenö-bi</ta>
            <ta e="T237" id="Seg_3130" s="T236">dĭgəttə</ta>
            <ta e="T238" id="Seg_3131" s="T237">măn-ntə</ta>
            <ta e="T239" id="Seg_3132" s="T238">ej</ta>
            <ta e="T240" id="Seg_3133" s="T239">tĭmne-m</ta>
            <ta e="T241" id="Seg_3134" s="T240">šində</ta>
            <ta e="T243" id="Seg_3135" s="T242">šində</ta>
            <ta e="T244" id="Seg_3136" s="T243">tăn</ta>
            <ta e="T245" id="Seg_3137" s="T244">i-gA-l</ta>
            <ta e="T246" id="Seg_3138" s="T245">dʼok</ta>
            <ta e="T247" id="Seg_3139" s="T246">măn</ta>
            <ta e="T248" id="Seg_3140" s="T247">e-m</ta>
            <ta e="T249" id="Seg_3141" s="T248">šo-ʔ</ta>
            <ta e="T250" id="Seg_3142" s="T249">tăn</ta>
            <ta e="T251" id="Seg_3143" s="T250">bazoʔ</ta>
            <ta e="T252" id="Seg_3144" s="T251">nereʔ-luʔbdə-bi-l</ta>
            <ta e="T253" id="Seg_3145" s="T252">dĭgəttə</ta>
            <ta e="T254" id="Seg_3146" s="T253">nagur</ta>
            <ta e="T255" id="Seg_3147" s="T254">nüdʼi</ta>
            <ta e="T256" id="Seg_3148" s="T255">kan-bi</ta>
            <ta e="T257" id="Seg_3149" s="T256">dĭ-m</ta>
            <ta e="T258" id="Seg_3150" s="T257">šo-bi</ta>
            <ta e="T259" id="Seg_3151" s="T258">nagur</ta>
            <ta e="T260" id="Seg_3152" s="T259">lĭd</ta>
            <ta e="T261" id="Seg_3153" s="T260">nüdʼi</ta>
            <ta e="T262" id="Seg_3154" s="T261">dĭ</ta>
            <ta e="T263" id="Seg_3155" s="T262">bazoʔ</ta>
            <ta e="T264" id="Seg_3156" s="T263">iʔbə-bi</ta>
            <ta e="T265" id="Seg_3157" s="T264">kunol-zittə</ta>
            <ta e="T266" id="Seg_3158" s="T265">dĭ</ta>
            <ta e="T267" id="Seg_3159" s="T266">bazoʔ</ta>
            <ta e="T268" id="Seg_3160" s="T267">šo-bi</ta>
            <ta e="T269" id="Seg_3161" s="T268">surar-laʔbə</ta>
            <ta e="T270" id="Seg_3162" s="T269">no</ta>
            <ta e="T271" id="Seg_3163" s="T270">tenö-luʔbdə-bi-l</ta>
            <ta e="T272" id="Seg_3164" s="T271">da</ta>
            <ta e="T273" id="Seg_3165" s="T272">šo-ʔ</ta>
            <ta e="T274" id="Seg_3166" s="T273">döbər</ta>
            <ta e="T275" id="Seg_3167" s="T274">măn</ta>
            <ta e="T276" id="Seg_3168" s="T275">măndo-r-lV-m</ta>
            <ta e="T277" id="Seg_3169" s="T276">tănan</ta>
            <ta e="T278" id="Seg_3170" s="T277">da</ta>
            <ta e="T279" id="Seg_3171" s="T278">tăn</ta>
            <ta e="T280" id="Seg_3172" s="T279">nereʔ-luʔbdə-lV-l</ta>
            <ta e="T281" id="Seg_3173" s="T280">dĭ</ta>
            <ta e="T282" id="Seg_3174" s="T281">măn-ntə</ta>
            <ta e="T283" id="Seg_3175" s="T282">ej</ta>
            <ta e="T284" id="Seg_3176" s="T283">nereʔ-luʔbdə-lV-m</ta>
            <ta e="T285" id="Seg_3177" s="T284">dĭ</ta>
            <ta e="T286" id="Seg_3178" s="T285">šo-bi</ta>
            <ta e="T287" id="Seg_3179" s="T286">tura-Tə</ta>
            <ta e="T288" id="Seg_3180" s="T287">dĭ</ta>
            <ta e="T289" id="Seg_3181" s="T288">ej</ta>
            <ta e="T290" id="Seg_3182" s="T289">nereʔ-luʔbdə-bi</ta>
            <ta e="T291" id="Seg_3183" s="T290">măn-liA</ta>
            <ta e="T292" id="Seg_3184" s="T291">amno-lV-m</ta>
            <ta e="T293" id="Seg_3185" s="T292">dĭ-ziʔ</ta>
            <ta e="T294" id="Seg_3186" s="T293">ĭmbi</ta>
            <ta e="T296" id="Seg_3187" s="T295">pušaj</ta>
            <ta e="T297" id="Seg_3188" s="T296">ej</ta>
            <ta e="T298" id="Seg_3189" s="T297">kuvas</ta>
            <ta e="T299" id="Seg_3190" s="T298">da</ta>
            <ta e="T300" id="Seg_3191" s="T299">dön</ta>
            <ta e="T301" id="Seg_3192" s="T300">bar</ta>
            <ta e="T302" id="Seg_3193" s="T301">măna</ta>
            <ta e="T303" id="Seg_3194" s="T302">i</ta>
            <ta e="T304" id="Seg_3195" s="T303">măna</ta>
            <ta e="T305" id="Seg_3196" s="T304">dĭgəttə</ta>
            <ta e="T306" id="Seg_3197" s="T305">amno-bi</ta>
            <ta e="T307" id="Seg_3198" s="T306">amno-bi</ta>
            <ta e="T308" id="Seg_3199" s="T307">măn-ntə</ta>
            <ta e="T309" id="Seg_3200" s="T308">măn</ta>
            <ta e="T310" id="Seg_3201" s="T309">kan-lV-m</ta>
            <ta e="T312" id="Seg_3202" s="T311">aba-Tə</ta>
            <ta e="T313" id="Seg_3203" s="T312">dĭ</ta>
            <ta e="T314" id="Seg_3204" s="T313">dĭ-Tə</ta>
            <ta e="T316" id="Seg_3205" s="T315">kălʼso</ta>
            <ta e="T317" id="Seg_3206" s="T316">mĭ-bi</ta>
            <ta e="T318" id="Seg_3207" s="T317">no</ta>
            <ta e="T319" id="Seg_3208" s="T318">kan-ə-ʔ</ta>
            <ta e="T320" id="Seg_3209" s="T319">kamən</ta>
            <ta e="T321" id="Seg_3210" s="T320">dĭ</ta>
            <ta e="T322" id="Seg_3211" s="T321">kălʼso</ta>
            <ta e="T323" id="Seg_3212" s="T322">sagər</ta>
            <ta e="T324" id="Seg_3213" s="T323">mo-lV-j</ta>
            <ta e="T325" id="Seg_3214" s="T324">dĭgəttə</ta>
            <ta e="T326" id="Seg_3215" s="T325">büžü</ta>
            <ta e="T327" id="Seg_3216" s="T326">šo-ʔ</ta>
            <ta e="T328" id="Seg_3217" s="T327">ato</ta>
            <ta e="T329" id="Seg_3218" s="T328">măn</ta>
            <ta e="T330" id="Seg_3219" s="T329">kü-laːm-lV-m</ta>
            <ta e="T331" id="Seg_3220" s="T330">dĭ</ta>
            <ta e="T332" id="Seg_3221" s="T331">kan-bi</ta>
            <ta e="T333" id="Seg_3222" s="T332">sʼestra-ttə</ta>
            <ta e="T334" id="Seg_3223" s="T333">bar</ta>
            <ta e="T335" id="Seg_3224" s="T334">dĭ-Tə</ta>
            <ta e="T337" id="Seg_3225" s="T336">bü</ta>
            <ta e="T338" id="Seg_3226" s="T337">girgit=də</ta>
            <ta e="T339" id="Seg_3227" s="T338">mĭ-bi</ta>
            <ta e="T340" id="Seg_3228" s="T339">dĭ</ta>
            <ta e="T341" id="Seg_3229" s="T340">bar</ta>
            <ta e="T342" id="Seg_3230" s="T341">kunol-bi</ta>
            <ta e="T343" id="Seg_3231" s="T342">kunol-bi</ta>
            <ta e="T344" id="Seg_3232" s="T343">uʔbdə-bi</ta>
            <ta e="T345" id="Seg_3233" s="T344">kalʼečka-t</ta>
            <ta e="T346" id="Seg_3234" s="T345">bar</ta>
            <ta e="T347" id="Seg_3235" s="T346">sagər</ta>
            <ta e="T348" id="Seg_3236" s="T347">dĭ</ta>
            <ta e="T349" id="Seg_3237" s="T348">bar</ta>
            <ta e="T350" id="Seg_3238" s="T349">ĭmbi</ta>
            <ta e="T351" id="Seg_3239" s="T350">barəʔ-bi</ta>
            <ta e="T352" id="Seg_3240" s="T351">i</ta>
            <ta e="T353" id="Seg_3241" s="T352">kan-bi</ta>
            <ta e="T354" id="Seg_3242" s="T353">šo-bi</ta>
            <ta e="T355" id="Seg_3243" s="T354">dĭbər</ta>
            <ta e="T356" id="Seg_3244" s="T355">iʔbö-laʔbə</ta>
            <ta e="T357" id="Seg_3245" s="T356">kü-laːm-bi</ta>
            <ta e="T358" id="Seg_3246" s="T357">bar</ta>
            <ta e="T359" id="Seg_3247" s="T358">tʼor-bi</ta>
            <ta e="T360" id="Seg_3248" s="T359">măn</ta>
            <ta e="T361" id="Seg_3249" s="T360">tănan</ta>
            <ta e="T362" id="Seg_3250" s="T361">ajir-liA-m</ta>
            <ta e="T363" id="Seg_3251" s="T362">uʔbdə-ʔ</ta>
            <ta e="T364" id="Seg_3252" s="T363">ajir-liA-m</ta>
            <ta e="T365" id="Seg_3253" s="T364">uʔbdə-ʔ</ta>
            <ta e="T366" id="Seg_3254" s="T365">dĭ</ta>
            <ta e="T367" id="Seg_3255" s="T366">uʔbdə-bi</ta>
            <ta e="T368" id="Seg_3256" s="T367">i</ta>
            <ta e="T369" id="Seg_3257" s="T368">ugaːndə</ta>
            <ta e="T370" id="Seg_3258" s="T369">kuvas</ta>
            <ta e="T371" id="Seg_3259" s="T370">nʼi</ta>
            <ta e="T372" id="Seg_3260" s="T371">mo-laːm-bi</ta>
            <ta e="T373" id="Seg_3261" s="T372">dĭgəttə</ta>
            <ta e="T374" id="Seg_3262" s="T373">koŋgoro-jəʔ</ta>
            <ta e="T375" id="Seg_3263" s="T374">bar</ta>
            <ta e="T376" id="Seg_3264" s="T375">kuzur-laʔbə-jəʔ</ta>
            <ta e="T377" id="Seg_3265" s="T376">il</ta>
            <ta e="T378" id="Seg_3266" s="T377">mĭn-laʔbə-jəʔ</ta>
            <ta e="T379" id="Seg_3267" s="T378">mo-laːm-bi</ta>
            <ta e="T380" id="Seg_3268" s="T379">gorăt</ta>
            <ta e="T381" id="Seg_3269" s="T380">a</ta>
            <ta e="T382" id="Seg_3270" s="T381">dĭ</ta>
            <ta e="T383" id="Seg_3271" s="T382">dĭ</ta>
            <ta e="T384" id="Seg_3272" s="T383">nʼi</ta>
            <ta e="T385" id="Seg_3273" s="T384">koŋ</ta>
            <ta e="T386" id="Seg_3274" s="T385">mo-laːm-bi</ta>
            <ta e="T387" id="Seg_3275" s="T386">dĭgəttə</ta>
            <ta e="T388" id="Seg_3276" s="T387">dĭ</ta>
            <ta e="T390" id="Seg_3277" s="T389">dĭ-m</ta>
            <ta e="T391" id="Seg_3278" s="T390">tibi-Tə</ta>
            <ta e="T392" id="Seg_3279" s="T391">i-bi</ta>
            <ta e="T393" id="Seg_3280" s="T392">i</ta>
            <ta e="T395" id="Seg_3281" s="T394">amno-zittə</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_3282" s="T0">live-PST.[3SG]</ta>
            <ta e="T2" id="Seg_3283" s="T1">man.[NOM.SG]</ta>
            <ta e="T3" id="Seg_3284" s="T2">this-GEN</ta>
            <ta e="T6" id="Seg_3285" s="T5">this-GEN</ta>
            <ta e="T7" id="Seg_3286" s="T6">be-PST-3PL</ta>
            <ta e="T8" id="Seg_3287" s="T7">three.[NOM.SG]</ta>
            <ta e="T9" id="Seg_3288" s="T8">daughter-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T10" id="Seg_3289" s="T9">this.[NOM.SG]</ta>
            <ta e="T11" id="Seg_3290" s="T10">began</ta>
            <ta e="T13" id="Seg_3291" s="T12">collect-INF.LAT</ta>
            <ta e="T14" id="Seg_3292" s="T13">town-LAT</ta>
            <ta e="T15" id="Seg_3293" s="T14">go-INF.LAT</ta>
            <ta e="T16" id="Seg_3294" s="T15">big.[NOM.SG]</ta>
            <ta e="T17" id="Seg_3295" s="T16">daughter-LAT/LOC.3SG</ta>
            <ta e="T18" id="Seg_3296" s="T17">say-IPFVZ.[3SG]</ta>
            <ta e="T19" id="Seg_3297" s="T18">what.[NOM.SG]</ta>
            <ta e="T20" id="Seg_3298" s="T19">you.DAT</ta>
            <ta e="T21" id="Seg_3299" s="T20">take-INF.LAT</ta>
            <ta e="T22" id="Seg_3300" s="T21">you.NOM</ta>
            <ta e="T23" id="Seg_3301" s="T22">I.LAT</ta>
            <ta e="T24" id="Seg_3302" s="T23">beautiful.[NOM.SG]</ta>
            <ta e="T25" id="Seg_3303" s="T24">dress.[NOM.SG]</ta>
            <ta e="T26" id="Seg_3304" s="T25">take-IMP.2SG</ta>
            <ta e="T27" id="Seg_3305" s="T26">and</ta>
            <ta e="T28" id="Seg_3306" s="T27">small-NOM/GEN.3SG</ta>
            <ta e="T29" id="Seg_3307" s="T28">girl.[NOM.SG]</ta>
            <ta e="T30" id="Seg_3308" s="T29">you.NOM</ta>
            <ta e="T31" id="Seg_3309" s="T30">I.LAT</ta>
            <ta e="T32" id="Seg_3310" s="T31">shoe-PL</ta>
            <ta e="T33" id="Seg_3311" s="T32">beautiful.[NOM.SG]</ta>
            <ta e="T34" id="Seg_3312" s="T33">take-IMP.2SG</ta>
            <ta e="T35" id="Seg_3313" s="T34">and</ta>
            <ta e="T36" id="Seg_3314" s="T35">three.[NOM.SG]</ta>
            <ta e="T37" id="Seg_3315" s="T36">more</ta>
            <ta e="T38" id="Seg_3316" s="T37">small.[NOM.SG]</ta>
            <ta e="T39" id="Seg_3317" s="T38">girl.[NOM.SG]</ta>
            <ta e="T40" id="Seg_3318" s="T39">you.DAT</ta>
            <ta e="T41" id="Seg_3319" s="T40">what.[NOM.SG]</ta>
            <ta e="T42" id="Seg_3320" s="T41">take-INF.LAT</ta>
            <ta e="T43" id="Seg_3321" s="T42">you.NOM</ta>
            <ta e="T44" id="Seg_3322" s="T43">I.LAT</ta>
            <ta e="T45" id="Seg_3323" s="T44">take-IMP.2SG</ta>
            <ta e="T46" id="Seg_3324" s="T45">red.[NOM.SG]</ta>
            <ta e="T47" id="Seg_3325" s="T46">flower.[NOM.SG]</ta>
            <ta e="T48" id="Seg_3326" s="T47">this.[NOM.SG]</ta>
            <ta e="T49" id="Seg_3327" s="T48">go-PST.[3SG]</ta>
            <ta e="T50" id="Seg_3328" s="T49">look-FRQ-PST.[3SG]</ta>
            <ta e="T51" id="Seg_3329" s="T50">look-FRQ-PST.[3SG]</ta>
            <ta e="T52" id="Seg_3330" s="T51">take-PST.[3SG]</ta>
            <ta e="T53" id="Seg_3331" s="T52">here</ta>
            <ta e="T54" id="Seg_3332" s="T53">dress.[NOM.SG]</ta>
            <ta e="T55" id="Seg_3333" s="T54">one-LAT</ta>
            <ta e="T56" id="Seg_3334" s="T55">take-PST.[3SG]</ta>
            <ta e="T57" id="Seg_3335" s="T56">shoe-PL</ta>
            <ta e="T58" id="Seg_3336" s="T57">and</ta>
            <ta e="T60" id="Seg_3337" s="T59">flower.[NOM.SG]</ta>
            <ta e="T61" id="Seg_3338" s="T60">NEG.EX</ta>
            <ta e="T62" id="Seg_3339" s="T61">then</ta>
            <ta e="T63" id="Seg_3340" s="T62">come-PRS.[3SG]</ta>
            <ta e="T64" id="Seg_3341" s="T63">and</ta>
            <ta e="T65" id="Seg_3342" s="T64">think-DUR.[3SG]</ta>
            <ta e="T66" id="Seg_3343" s="T65">where</ta>
            <ta e="T68" id="Seg_3344" s="T67">take-INF.LAT</ta>
            <ta e="T69" id="Seg_3345" s="T68">red.[NOM.SG]</ta>
            <ta e="T70" id="Seg_3346" s="T69">flower.[NOM.SG]</ta>
            <ta e="T71" id="Seg_3347" s="T70">come-PRS.[3SG]</ta>
            <ta e="T72" id="Seg_3348" s="T71">see-PST.[3SG]</ta>
            <ta e="T73" id="Seg_3349" s="T72">PTCL</ta>
            <ta e="T74" id="Seg_3350" s="T73">%%-LOC</ta>
            <ta e="T75" id="Seg_3351" s="T74">flower-PL</ta>
            <ta e="T76" id="Seg_3352" s="T75">there</ta>
            <ta e="T77" id="Seg_3353" s="T76">go-PST.[3SG]</ta>
            <ta e="T78" id="Seg_3354" s="T77">tear-PST.[3SG]</ta>
            <ta e="T79" id="Seg_3355" s="T78">gate-PL</ta>
            <ta e="T81" id="Seg_3356" s="T80">close-MOM-PST-3PL</ta>
            <ta e="T82" id="Seg_3357" s="T81">then</ta>
            <ta e="T83" id="Seg_3358" s="T82">go-PST.[3SG]</ta>
            <ta e="T84" id="Seg_3359" s="T83">go-PST.[3SG]</ta>
            <ta e="T85" id="Seg_3360" s="T84">%%</ta>
            <ta e="T86" id="Seg_3361" s="T85">evening-LOC.ADV</ta>
            <ta e="T87" id="Seg_3362" s="T86">come-PST.[3SG]</ta>
            <ta e="T88" id="Seg_3363" s="T87">man.[NOM.SG]</ta>
            <ta e="T90" id="Seg_3364" s="T89">self-NOM/GEN/ACC.2SG</ta>
            <ta e="T91" id="Seg_3365" s="T90">daughter-NOM/GEN/ACC.2SG</ta>
            <ta e="T92" id="Seg_3366" s="T91">give-FUT-2SG</ta>
            <ta e="T93" id="Seg_3367" s="T92">I.LAT</ta>
            <ta e="T95" id="Seg_3368" s="T94">bring-FUT-2SG</ta>
            <ta e="T96" id="Seg_3369" s="T95">here</ta>
            <ta e="T97" id="Seg_3370" s="T96">this.[NOM.SG]</ta>
            <ta e="T98" id="Seg_3371" s="T97">think-PST.[3SG]</ta>
            <ta e="T99" id="Seg_3372" s="T98">think-PST.[3SG]</ta>
            <ta e="T100" id="Seg_3373" s="T99">bring-FUT-1SG</ta>
            <ta e="T101" id="Seg_3374" s="T100">then</ta>
            <ta e="T102" id="Seg_3375" s="T101">morning-GEN</ta>
            <ta e="T103" id="Seg_3376" s="T102">get.up-PST.[3SG]</ta>
            <ta e="T104" id="Seg_3377" s="T103">gate-3SG-INS</ta>
            <ta e="T107" id="Seg_3378" s="T106">close-MOM-PST-3PL</ta>
            <ta e="T108" id="Seg_3379" s="T107">and</ta>
            <ta e="T109" id="Seg_3380" s="T108">come-PST.[3SG]</ta>
            <ta e="T110" id="Seg_3381" s="T109">tent-LAT/LOC.3SG</ta>
            <ta e="T111" id="Seg_3382" s="T110">bring-PST.[3SG]</ta>
            <ta e="T112" id="Seg_3383" s="T111">all</ta>
            <ta e="T113" id="Seg_3384" s="T112">give-PST.[3SG]</ta>
            <ta e="T114" id="Seg_3385" s="T113">three-COLL</ta>
            <ta e="T115" id="Seg_3386" s="T114">and</ta>
            <ta e="T116" id="Seg_3387" s="T115">you.DAT</ta>
            <ta e="T117" id="Seg_3388" s="T116">one.should</ta>
            <ta e="T118" id="Seg_3389" s="T117">there</ta>
            <ta e="T119" id="Seg_3390" s="T118">go-INF.LAT</ta>
            <ta e="T120" id="Seg_3391" s="T119">this.[NOM.SG]</ta>
            <ta e="T121" id="Seg_3392" s="T120">say-IPFVZ.[3SG]</ta>
            <ta e="T122" id="Seg_3393" s="T121">well</ta>
            <ta e="T123" id="Seg_3394" s="T122">and</ta>
            <ta e="T124" id="Seg_3395" s="T123">that</ta>
            <ta e="T125" id="Seg_3396" s="T124">I.NOM</ta>
            <ta e="T126" id="Seg_3397" s="T125">go-FUT-1SG</ta>
            <ta e="T127" id="Seg_3398" s="T126">there</ta>
            <ta e="T128" id="Seg_3399" s="T127">many</ta>
            <ta e="T129" id="Seg_3400" s="T128">flower-PL</ta>
            <ta e="T130" id="Seg_3401" s="T129">PTCL</ta>
            <ta e="T131" id="Seg_3402" s="T130">tear-FUT-2SG</ta>
            <ta e="T132" id="Seg_3403" s="T131">bring-PST.[3SG]</ta>
            <ta e="T133" id="Seg_3404" s="T132">send-PST.[3SG]</ta>
            <ta e="T134" id="Seg_3405" s="T133">this-ACC</ta>
            <ta e="T135" id="Seg_3406" s="T134">there</ta>
            <ta e="T136" id="Seg_3407" s="T135">gate-PL</ta>
            <ta e="T137" id="Seg_3408" s="T136">close-MOM-PST-3PL</ta>
            <ta e="T138" id="Seg_3409" s="T137">this-ACC</ta>
            <ta e="T139" id="Seg_3410" s="T138">this.[NOM.SG]</ta>
            <ta e="T142" id="Seg_3411" s="T141">leave-MOM-PST.[3SG]</ta>
            <ta e="T143" id="Seg_3412" s="T142">and</ta>
            <ta e="T144" id="Seg_3413" s="T143">tent-LAT/LOC.3SG</ta>
            <ta e="T145" id="Seg_3414" s="T144">come-PST.[3SG]</ta>
            <ta e="T147" id="Seg_3415" s="T146">day.[NOM.SG]</ta>
            <ta e="T148" id="Seg_3416" s="T147">go-PST.[3SG]</ta>
            <ta e="T149" id="Seg_3417" s="T148">go-PST.[3SG]</ta>
            <ta e="T150" id="Seg_3418" s="T149">flower-PL</ta>
            <ta e="T151" id="Seg_3419" s="T150">always</ta>
            <ta e="T152" id="Seg_3420" s="T151">write-DETR-PTCP</ta>
            <ta e="T153" id="Seg_3421" s="T152">Malanya-GEN</ta>
            <ta e="T154" id="Seg_3422" s="T153">what.[NOM.SG]</ta>
            <ta e="T155" id="Seg_3423" s="T154">go-FUT-3SG</ta>
            <ta e="T156" id="Seg_3424" s="T155">always</ta>
            <ta e="T157" id="Seg_3425" s="T156">Malanya.[NOM.SG]</ta>
            <ta e="T158" id="Seg_3426" s="T157">house-LAT</ta>
            <ta e="T159" id="Seg_3427" s="T158">come-PST.[3SG]</ta>
            <ta e="T160" id="Seg_3428" s="T159">PTCL</ta>
            <ta e="T161" id="Seg_3429" s="T160">iron-NOM/GEN/ACC.3PL</ta>
            <ta e="T162" id="Seg_3430" s="T161">what.kind</ta>
            <ta e="T163" id="Seg_3431" s="T162">spoon-PL</ta>
            <ta e="T164" id="Seg_3432" s="T163">always</ta>
            <ta e="T165" id="Seg_3433" s="T164">there</ta>
            <ta e="T166" id="Seg_3434" s="T165">Malanya-GEN</ta>
            <ta e="T167" id="Seg_3435" s="T166">knife-PL</ta>
            <ta e="T169" id="Seg_3436" s="T168">then</ta>
            <ta e="T170" id="Seg_3437" s="T169">eat-PST.[3SG]</ta>
            <ta e="T171" id="Seg_3438" s="T170">eat-PST.[3SG]</ta>
            <ta e="T173" id="Seg_3439" s="T172">I.LAT</ta>
            <ta e="T176" id="Seg_3440" s="T175">live-INF.LAT</ta>
            <ta e="T177" id="Seg_3441" s="T176">there</ta>
            <ta e="T178" id="Seg_3442" s="T177">run.away</ta>
            <ta e="T182" id="Seg_3443" s="T181">then</ta>
            <ta e="T183" id="Seg_3444" s="T182">lie.down-PST.[3SG]</ta>
            <ta e="T184" id="Seg_3445" s="T183">sleep-INF.LAT</ta>
            <ta e="T185" id="Seg_3446" s="T184">evening-LOC.ADV</ta>
            <ta e="T186" id="Seg_3447" s="T185">come-PST.[3SG]</ta>
            <ta e="T187" id="Seg_3448" s="T186">man.[NOM.SG]</ta>
            <ta e="T188" id="Seg_3449" s="T187">you.NOM</ta>
            <ta e="T189" id="Seg_3450" s="T188">Malanya.[NOM.SG]</ta>
            <ta e="T190" id="Seg_3451" s="T189">you.NOM</ta>
            <ta e="T191" id="Seg_3452" s="T190">I.ACC</ta>
            <ta e="T192" id="Seg_3453" s="T191">pity-PRS-2SG</ta>
            <ta e="T193" id="Seg_3454" s="T192">and</ta>
            <ta e="T194" id="Seg_3455" s="T193">I.NOM</ta>
            <ta e="T195" id="Seg_3456" s="T194">put</ta>
            <ta e="T197" id="Seg_3457" s="T196">NEG</ta>
            <ta e="T198" id="Seg_3458" s="T197">know-1SG</ta>
            <ta e="T200" id="Seg_3459" s="T199">who.[NOM.SG]</ta>
            <ta e="T201" id="Seg_3460" s="T200">you.NOM</ta>
            <ta e="T202" id="Seg_3461" s="T201">come-IMP.2SG</ta>
            <ta e="T203" id="Seg_3462" s="T202">here</ta>
            <ta e="T204" id="Seg_3463" s="T203">no</ta>
            <ta e="T205" id="Seg_3464" s="T204">I.NOM</ta>
            <ta e="T206" id="Seg_3465" s="T205">NEG</ta>
            <ta e="T207" id="Seg_3466" s="T206">come-FUT-1SG</ta>
            <ta e="T208" id="Seg_3467" s="T207">you.NOM</ta>
            <ta e="T209" id="Seg_3468" s="T208">frighten-MOM-FUT-2SG</ta>
            <ta e="T210" id="Seg_3469" s="T209">then</ta>
            <ta e="T396" id="Seg_3470" s="T210">go-CVB</ta>
            <ta e="T211" id="Seg_3471" s="T396">disappear-PST.[3SG]</ta>
            <ta e="T212" id="Seg_3472" s="T211">this.[NOM.SG]</ta>
            <ta e="T213" id="Seg_3473" s="T212">think-PST.[3SG]</ta>
            <ta e="T214" id="Seg_3474" s="T213">think-PST.[3SG]</ta>
            <ta e="T215" id="Seg_3475" s="T214">then</ta>
            <ta e="T216" id="Seg_3476" s="T215">two.[NOM.SG]</ta>
            <ta e="T217" id="Seg_3477" s="T216">evening</ta>
            <ta e="T218" id="Seg_3478" s="T217">come-PST.[3SG]</ta>
            <ta e="T219" id="Seg_3479" s="T218">this.[NOM.SG]</ta>
            <ta e="T220" id="Seg_3480" s="T219">PTCL</ta>
            <ta e="T221" id="Seg_3481" s="T220">water-LAT</ta>
            <ta e="T222" id="Seg_3482" s="T221">go-PST.[3SG]</ta>
            <ta e="T223" id="Seg_3483" s="T222">fish.[NOM.SG]</ta>
            <ta e="T224" id="Seg_3484" s="T223">there</ta>
            <ta e="T225" id="Seg_3485" s="T224">and</ta>
            <ta e="T226" id="Seg_3486" s="T225">then</ta>
            <ta e="T227" id="Seg_3487" s="T226">write-DETR-PTCP</ta>
            <ta e="T228" id="Seg_3488" s="T227">Malanya-GEN</ta>
            <ta e="T229" id="Seg_3489" s="T228">fish.[NOM.SG]</ta>
            <ta e="T230" id="Seg_3490" s="T229">then</ta>
            <ta e="T231" id="Seg_3491" s="T230">this.[NOM.SG]</ta>
            <ta e="T232" id="Seg_3492" s="T231">again</ta>
            <ta e="T233" id="Seg_3493" s="T232">evening-NOM/GEN.3SG</ta>
            <ta e="T234" id="Seg_3494" s="T233">come-PST.[3SG]</ta>
            <ta e="T235" id="Seg_3495" s="T234">think-PST.[3SG]</ta>
            <ta e="T236" id="Seg_3496" s="T235">think-PST.[3SG]</ta>
            <ta e="T237" id="Seg_3497" s="T236">then</ta>
            <ta e="T238" id="Seg_3498" s="T237">say-IPFVZ.[3SG]</ta>
            <ta e="T239" id="Seg_3499" s="T238">NEG</ta>
            <ta e="T240" id="Seg_3500" s="T239">know-1SG</ta>
            <ta e="T241" id="Seg_3501" s="T240">who.[NOM.SG]</ta>
            <ta e="T243" id="Seg_3502" s="T242">who.[NOM.SG]</ta>
            <ta e="T244" id="Seg_3503" s="T243">you.NOM</ta>
            <ta e="T245" id="Seg_3504" s="T244">be-PRS-2SG</ta>
            <ta e="T246" id="Seg_3505" s="T245">no</ta>
            <ta e="T247" id="Seg_3506" s="T246">I.NOM</ta>
            <ta e="T248" id="Seg_3507" s="T247">NEG.AUX-1SG</ta>
            <ta e="T249" id="Seg_3508" s="T248">come-CNG</ta>
            <ta e="T250" id="Seg_3509" s="T249">you.NOM</ta>
            <ta e="T251" id="Seg_3510" s="T250">again</ta>
            <ta e="T252" id="Seg_3511" s="T251">frighten-MOM-PST-2SG</ta>
            <ta e="T253" id="Seg_3512" s="T252">then</ta>
            <ta e="T254" id="Seg_3513" s="T253">three.[NOM.SG]</ta>
            <ta e="T255" id="Seg_3514" s="T254">evening</ta>
            <ta e="T256" id="Seg_3515" s="T255">go-PST.[3SG]</ta>
            <ta e="T257" id="Seg_3516" s="T256">this-ACC</ta>
            <ta e="T258" id="Seg_3517" s="T257">come-PST.[3SG]</ta>
            <ta e="T259" id="Seg_3518" s="T258">three.[NOM.SG]</ta>
            <ta e="T260" id="Seg_3519" s="T259">%%</ta>
            <ta e="T261" id="Seg_3520" s="T260">evening.[NOM.SG]</ta>
            <ta e="T262" id="Seg_3521" s="T261">this.[NOM.SG]</ta>
            <ta e="T263" id="Seg_3522" s="T262">again</ta>
            <ta e="T264" id="Seg_3523" s="T263">lie.down-PST.[3SG]</ta>
            <ta e="T265" id="Seg_3524" s="T264">sleep-INF.LAT</ta>
            <ta e="T266" id="Seg_3525" s="T265">this.[NOM.SG]</ta>
            <ta e="T267" id="Seg_3526" s="T266">again</ta>
            <ta e="T268" id="Seg_3527" s="T267">come-PST.[3SG]</ta>
            <ta e="T269" id="Seg_3528" s="T268">ask-DUR.[3SG]</ta>
            <ta e="T270" id="Seg_3529" s="T269">well</ta>
            <ta e="T271" id="Seg_3530" s="T270">think-MOM-PST-2SG</ta>
            <ta e="T272" id="Seg_3531" s="T271">PTCL</ta>
            <ta e="T273" id="Seg_3532" s="T272">come-IMP.2SG</ta>
            <ta e="T274" id="Seg_3533" s="T273">here</ta>
            <ta e="T275" id="Seg_3534" s="T274">I.NOM</ta>
            <ta e="T276" id="Seg_3535" s="T275">look-FRQ-FUT-1SG</ta>
            <ta e="T277" id="Seg_3536" s="T276">you.ACC</ta>
            <ta e="T278" id="Seg_3537" s="T277">PTCL</ta>
            <ta e="T279" id="Seg_3538" s="T278">you.NOM</ta>
            <ta e="T280" id="Seg_3539" s="T279">frighten-MOM-FUT-2SG</ta>
            <ta e="T281" id="Seg_3540" s="T280">this.[NOM.SG]</ta>
            <ta e="T282" id="Seg_3541" s="T281">say-IPFVZ.[3SG]</ta>
            <ta e="T283" id="Seg_3542" s="T282">NEG</ta>
            <ta e="T284" id="Seg_3543" s="T283">frighten-MOM-FUT-1SG</ta>
            <ta e="T285" id="Seg_3544" s="T284">this.[NOM.SG]</ta>
            <ta e="T286" id="Seg_3545" s="T285">come-PST.[3SG]</ta>
            <ta e="T287" id="Seg_3546" s="T286">house-LAT</ta>
            <ta e="T288" id="Seg_3547" s="T287">this.[NOM.SG]</ta>
            <ta e="T289" id="Seg_3548" s="T288">NEG</ta>
            <ta e="T290" id="Seg_3549" s="T289">frighten-MOM-PST.[3SG]</ta>
            <ta e="T291" id="Seg_3550" s="T290">say-PRS.[3SG]</ta>
            <ta e="T292" id="Seg_3551" s="T291">live-FUT-1SG</ta>
            <ta e="T293" id="Seg_3552" s="T292">this-INS</ta>
            <ta e="T294" id="Seg_3553" s="T293">what.[NOM.SG]</ta>
            <ta e="T296" id="Seg_3554" s="T295">JUSS</ta>
            <ta e="T297" id="Seg_3555" s="T296">NEG</ta>
            <ta e="T298" id="Seg_3556" s="T297">beautiful.[NOM.SG]</ta>
            <ta e="T299" id="Seg_3557" s="T298">and</ta>
            <ta e="T300" id="Seg_3558" s="T299">here</ta>
            <ta e="T301" id="Seg_3559" s="T300">PTCL</ta>
            <ta e="T302" id="Seg_3560" s="T301">I.LAT</ta>
            <ta e="T303" id="Seg_3561" s="T302">and</ta>
            <ta e="T304" id="Seg_3562" s="T303">I.LAT</ta>
            <ta e="T305" id="Seg_3563" s="T304">then</ta>
            <ta e="T306" id="Seg_3564" s="T305">live-PST.[3SG]</ta>
            <ta e="T307" id="Seg_3565" s="T306">live-PST.[3SG]</ta>
            <ta e="T308" id="Seg_3566" s="T307">say-IPFVZ.[3SG]</ta>
            <ta e="T309" id="Seg_3567" s="T308">I.NOM</ta>
            <ta e="T310" id="Seg_3568" s="T309">go-FUT-1SG</ta>
            <ta e="T312" id="Seg_3569" s="T311">father-LAT</ta>
            <ta e="T313" id="Seg_3570" s="T312">this.[NOM.SG]</ta>
            <ta e="T314" id="Seg_3571" s="T313">this-LAT</ta>
            <ta e="T316" id="Seg_3572" s="T315">ring.[NOM.SG]</ta>
            <ta e="T317" id="Seg_3573" s="T316">give-PST.[3SG]</ta>
            <ta e="T318" id="Seg_3574" s="T317">well</ta>
            <ta e="T319" id="Seg_3575" s="T318">go-EP-IMP.2SG</ta>
            <ta e="T320" id="Seg_3576" s="T319">when</ta>
            <ta e="T321" id="Seg_3577" s="T320">this.[NOM.SG]</ta>
            <ta e="T322" id="Seg_3578" s="T321">ring.[NOM.SG]</ta>
            <ta e="T323" id="Seg_3579" s="T322">black.[NOM.SG]</ta>
            <ta e="T324" id="Seg_3580" s="T323">become-FUT-3SG</ta>
            <ta e="T325" id="Seg_3581" s="T324">then</ta>
            <ta e="T326" id="Seg_3582" s="T325">soon</ta>
            <ta e="T327" id="Seg_3583" s="T326">come-IMP.2SG</ta>
            <ta e="T328" id="Seg_3584" s="T327">otherwise</ta>
            <ta e="T329" id="Seg_3585" s="T328">I.NOM</ta>
            <ta e="T330" id="Seg_3586" s="T329">die-RES-FUT-1SG</ta>
            <ta e="T331" id="Seg_3587" s="T330">this</ta>
            <ta e="T332" id="Seg_3588" s="T331">go-PST.[3SG]</ta>
            <ta e="T333" id="Seg_3589" s="T332">sister-ABL.3SG</ta>
            <ta e="T334" id="Seg_3590" s="T333">PTCL</ta>
            <ta e="T335" id="Seg_3591" s="T334">this-LAT</ta>
            <ta e="T337" id="Seg_3592" s="T336">water.[NOM.SG]</ta>
            <ta e="T338" id="Seg_3593" s="T337">what.kind=INDEF</ta>
            <ta e="T339" id="Seg_3594" s="T338">give-PST.[3SG]</ta>
            <ta e="T340" id="Seg_3595" s="T339">this</ta>
            <ta e="T341" id="Seg_3596" s="T340">PTCL</ta>
            <ta e="T342" id="Seg_3597" s="T341">sleep-PST.[3SG]</ta>
            <ta e="T343" id="Seg_3598" s="T342">sleep-PST.[3SG]</ta>
            <ta e="T344" id="Seg_3599" s="T343">get.up-PST.[3SG]</ta>
            <ta e="T345" id="Seg_3600" s="T344">ring-NOM/GEN.3SG</ta>
            <ta e="T346" id="Seg_3601" s="T345">PTCL</ta>
            <ta e="T347" id="Seg_3602" s="T346">black.[NOM.SG]</ta>
            <ta e="T348" id="Seg_3603" s="T347">this.[NOM.SG]</ta>
            <ta e="T349" id="Seg_3604" s="T348">PTCL</ta>
            <ta e="T350" id="Seg_3605" s="T349">what.[NOM.SG]</ta>
            <ta e="T351" id="Seg_3606" s="T350">throw.away-PST.[3SG]</ta>
            <ta e="T352" id="Seg_3607" s="T351">and</ta>
            <ta e="T353" id="Seg_3608" s="T352">go-PST.[3SG]</ta>
            <ta e="T354" id="Seg_3609" s="T353">come-PST.[3SG]</ta>
            <ta e="T355" id="Seg_3610" s="T354">there</ta>
            <ta e="T356" id="Seg_3611" s="T355">lie-DUR.[3SG]</ta>
            <ta e="T357" id="Seg_3612" s="T356">die-RES-PST.[3SG]</ta>
            <ta e="T358" id="Seg_3613" s="T357">PTCL</ta>
            <ta e="T359" id="Seg_3614" s="T358">cry-PST.[3SG]</ta>
            <ta e="T360" id="Seg_3615" s="T359">I.NOM</ta>
            <ta e="T361" id="Seg_3616" s="T360">you.DAT</ta>
            <ta e="T362" id="Seg_3617" s="T361">pity-PRS-1SG</ta>
            <ta e="T363" id="Seg_3618" s="T362">get.up-IMP.2SG</ta>
            <ta e="T364" id="Seg_3619" s="T363">pity-PRS-1SG</ta>
            <ta e="T365" id="Seg_3620" s="T364">get.up-IMP.2SG</ta>
            <ta e="T366" id="Seg_3621" s="T365">this.[NOM.SG]</ta>
            <ta e="T367" id="Seg_3622" s="T366">get.up-PST.[3SG]</ta>
            <ta e="T368" id="Seg_3623" s="T367">and</ta>
            <ta e="T369" id="Seg_3624" s="T368">very</ta>
            <ta e="T370" id="Seg_3625" s="T369">beautiful.[NOM.SG]</ta>
            <ta e="T371" id="Seg_3626" s="T370">boy.[NOM.SG]</ta>
            <ta e="T372" id="Seg_3627" s="T371">become-RES-PST.[3SG]</ta>
            <ta e="T373" id="Seg_3628" s="T372">then</ta>
            <ta e="T374" id="Seg_3629" s="T373">bell-PL</ta>
            <ta e="T375" id="Seg_3630" s="T374">PTCL</ta>
            <ta e="T376" id="Seg_3631" s="T375">rumble-DUR-3PL</ta>
            <ta e="T377" id="Seg_3632" s="T376">people.[NOM.SG]</ta>
            <ta e="T378" id="Seg_3633" s="T377">go-DUR-3PL</ta>
            <ta e="T379" id="Seg_3634" s="T378">become-RES-PST.[3SG]</ta>
            <ta e="T380" id="Seg_3635" s="T379">town.[NOM.SG]</ta>
            <ta e="T381" id="Seg_3636" s="T380">and</ta>
            <ta e="T382" id="Seg_3637" s="T381">this.[NOM.SG]</ta>
            <ta e="T383" id="Seg_3638" s="T382">this.[NOM.SG]</ta>
            <ta e="T384" id="Seg_3639" s="T383">boy.[NOM.SG]</ta>
            <ta e="T385" id="Seg_3640" s="T384">chief.[NOM.SG]</ta>
            <ta e="T386" id="Seg_3641" s="T385">become-RES-PST.[3SG]</ta>
            <ta e="T387" id="Seg_3642" s="T386">then</ta>
            <ta e="T388" id="Seg_3643" s="T387">this.[NOM.SG]</ta>
            <ta e="T390" id="Seg_3644" s="T389">this-ACC</ta>
            <ta e="T391" id="Seg_3645" s="T390">man-LAT</ta>
            <ta e="T392" id="Seg_3646" s="T391">take-PST.[3SG]</ta>
            <ta e="T393" id="Seg_3647" s="T392">and</ta>
            <ta e="T395" id="Seg_3648" s="T394">live-INF.LAT</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_3649" s="T0">жить-PST.[3SG]</ta>
            <ta e="T2" id="Seg_3650" s="T1">мужчина.[NOM.SG]</ta>
            <ta e="T3" id="Seg_3651" s="T2">этот-GEN</ta>
            <ta e="T6" id="Seg_3652" s="T5">этот-GEN</ta>
            <ta e="T7" id="Seg_3653" s="T6">быть-PST-3PL</ta>
            <ta e="T8" id="Seg_3654" s="T7">три.[NOM.SG]</ta>
            <ta e="T9" id="Seg_3655" s="T8">дочь-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T10" id="Seg_3656" s="T9">этот.[NOM.SG]</ta>
            <ta e="T11" id="Seg_3657" s="T10">стал</ta>
            <ta e="T13" id="Seg_3658" s="T12">собирать-INF.LAT</ta>
            <ta e="T14" id="Seg_3659" s="T13">город-LAT</ta>
            <ta e="T15" id="Seg_3660" s="T14">пойти-INF.LAT</ta>
            <ta e="T16" id="Seg_3661" s="T15">большой.[NOM.SG]</ta>
            <ta e="T17" id="Seg_3662" s="T16">дочь-LAT/LOC.3SG</ta>
            <ta e="T18" id="Seg_3663" s="T17">сказать-IPFVZ.[3SG]</ta>
            <ta e="T19" id="Seg_3664" s="T18">что.[NOM.SG]</ta>
            <ta e="T20" id="Seg_3665" s="T19">ты.DAT</ta>
            <ta e="T21" id="Seg_3666" s="T20">взять-INF.LAT</ta>
            <ta e="T22" id="Seg_3667" s="T21">ты.NOM</ta>
            <ta e="T23" id="Seg_3668" s="T22">я.LAT</ta>
            <ta e="T24" id="Seg_3669" s="T23">красивый.[NOM.SG]</ta>
            <ta e="T25" id="Seg_3670" s="T24">платье.[NOM.SG]</ta>
            <ta e="T26" id="Seg_3671" s="T25">взять-IMP.2SG</ta>
            <ta e="T27" id="Seg_3672" s="T26">а</ta>
            <ta e="T28" id="Seg_3673" s="T27">маленький-NOM/GEN.3SG</ta>
            <ta e="T29" id="Seg_3674" s="T28">девушка.[NOM.SG]</ta>
            <ta e="T30" id="Seg_3675" s="T29">ты.NOM</ta>
            <ta e="T31" id="Seg_3676" s="T30">я.LAT</ta>
            <ta e="T32" id="Seg_3677" s="T31">ботинок-PL</ta>
            <ta e="T33" id="Seg_3678" s="T32">красивый.[NOM.SG]</ta>
            <ta e="T34" id="Seg_3679" s="T33">взять-IMP.2SG</ta>
            <ta e="T35" id="Seg_3680" s="T34">а</ta>
            <ta e="T36" id="Seg_3681" s="T35">три.[NOM.SG]</ta>
            <ta e="T37" id="Seg_3682" s="T36">еще</ta>
            <ta e="T38" id="Seg_3683" s="T37">маленький.[NOM.SG]</ta>
            <ta e="T39" id="Seg_3684" s="T38">девушка.[NOM.SG]</ta>
            <ta e="T40" id="Seg_3685" s="T39">ты.DAT</ta>
            <ta e="T41" id="Seg_3686" s="T40">что.[NOM.SG]</ta>
            <ta e="T42" id="Seg_3687" s="T41">взять-INF.LAT</ta>
            <ta e="T43" id="Seg_3688" s="T42">ты.NOM</ta>
            <ta e="T44" id="Seg_3689" s="T43">я.LAT</ta>
            <ta e="T45" id="Seg_3690" s="T44">взять-IMP.2SG</ta>
            <ta e="T46" id="Seg_3691" s="T45">красный.[NOM.SG]</ta>
            <ta e="T47" id="Seg_3692" s="T46">цветок.[NOM.SG]</ta>
            <ta e="T48" id="Seg_3693" s="T47">этот.[NOM.SG]</ta>
            <ta e="T49" id="Seg_3694" s="T48">пойти-PST.[3SG]</ta>
            <ta e="T50" id="Seg_3695" s="T49">смотреть-FRQ-PST.[3SG]</ta>
            <ta e="T51" id="Seg_3696" s="T50">смотреть-FRQ-PST.[3SG]</ta>
            <ta e="T52" id="Seg_3697" s="T51">взять-PST.[3SG]</ta>
            <ta e="T53" id="Seg_3698" s="T52">сюда</ta>
            <ta e="T54" id="Seg_3699" s="T53">платье.[NOM.SG]</ta>
            <ta e="T55" id="Seg_3700" s="T54">один-LAT</ta>
            <ta e="T56" id="Seg_3701" s="T55">взять-PST.[3SG]</ta>
            <ta e="T57" id="Seg_3702" s="T56">ботинок-PL</ta>
            <ta e="T58" id="Seg_3703" s="T57">а</ta>
            <ta e="T60" id="Seg_3704" s="T59">цветок.[NOM.SG]</ta>
            <ta e="T61" id="Seg_3705" s="T60">NEG.EX</ta>
            <ta e="T62" id="Seg_3706" s="T61">тогда</ta>
            <ta e="T63" id="Seg_3707" s="T62">прийти-PRS.[3SG]</ta>
            <ta e="T64" id="Seg_3708" s="T63">и</ta>
            <ta e="T65" id="Seg_3709" s="T64">думать-DUR.[3SG]</ta>
            <ta e="T66" id="Seg_3710" s="T65">где</ta>
            <ta e="T68" id="Seg_3711" s="T67">взять-INF.LAT</ta>
            <ta e="T69" id="Seg_3712" s="T68">красный.[NOM.SG]</ta>
            <ta e="T70" id="Seg_3713" s="T69">цветок.[NOM.SG]</ta>
            <ta e="T71" id="Seg_3714" s="T70">прийти-PRS.[3SG]</ta>
            <ta e="T72" id="Seg_3715" s="T71">видеть-PST.[3SG]</ta>
            <ta e="T73" id="Seg_3716" s="T72">PTCL</ta>
            <ta e="T74" id="Seg_3717" s="T73">%%-LOC</ta>
            <ta e="T75" id="Seg_3718" s="T74">цветок-PL</ta>
            <ta e="T76" id="Seg_3719" s="T75">там</ta>
            <ta e="T77" id="Seg_3720" s="T76">пойти-PST.[3SG]</ta>
            <ta e="T78" id="Seg_3721" s="T77">рвать-PST.[3SG]</ta>
            <ta e="T79" id="Seg_3722" s="T78">ворота-PL</ta>
            <ta e="T81" id="Seg_3723" s="T80">закрыть-MOM-PST-3PL</ta>
            <ta e="T82" id="Seg_3724" s="T81">тогда</ta>
            <ta e="T83" id="Seg_3725" s="T82">идти-PST.[3SG]</ta>
            <ta e="T84" id="Seg_3726" s="T83">идти-PST.[3SG]</ta>
            <ta e="T85" id="Seg_3727" s="T84">%%</ta>
            <ta e="T86" id="Seg_3728" s="T85">вечер-LOC.ADV</ta>
            <ta e="T87" id="Seg_3729" s="T86">прийти-PST.[3SG]</ta>
            <ta e="T88" id="Seg_3730" s="T87">мужчина.[NOM.SG]</ta>
            <ta e="T90" id="Seg_3731" s="T89">сам-NOM/GEN/ACC.2SG</ta>
            <ta e="T91" id="Seg_3732" s="T90">дочь-NOM/GEN/ACC.2SG</ta>
            <ta e="T92" id="Seg_3733" s="T91">дать-FUT-2SG</ta>
            <ta e="T93" id="Seg_3734" s="T92">я.LAT</ta>
            <ta e="T95" id="Seg_3735" s="T94">принести-FUT-2SG</ta>
            <ta e="T96" id="Seg_3736" s="T95">здесь</ta>
            <ta e="T97" id="Seg_3737" s="T96">этот.[NOM.SG]</ta>
            <ta e="T98" id="Seg_3738" s="T97">думать-PST.[3SG]</ta>
            <ta e="T99" id="Seg_3739" s="T98">думать-PST.[3SG]</ta>
            <ta e="T100" id="Seg_3740" s="T99">принести-FUT-1SG</ta>
            <ta e="T101" id="Seg_3741" s="T100">тогда</ta>
            <ta e="T102" id="Seg_3742" s="T101">утро-GEN</ta>
            <ta e="T103" id="Seg_3743" s="T102">встать-PST.[3SG]</ta>
            <ta e="T104" id="Seg_3744" s="T103">ворота-3SG-INS</ta>
            <ta e="T107" id="Seg_3745" s="T106">закрыть-MOM-PST-3PL</ta>
            <ta e="T108" id="Seg_3746" s="T107">и</ta>
            <ta e="T109" id="Seg_3747" s="T108">прийти-PST.[3SG]</ta>
            <ta e="T110" id="Seg_3748" s="T109">чум-LAT/LOC.3SG</ta>
            <ta e="T111" id="Seg_3749" s="T110">принести-PST.[3SG]</ta>
            <ta e="T112" id="Seg_3750" s="T111">весь</ta>
            <ta e="T113" id="Seg_3751" s="T112">дать-PST.[3SG]</ta>
            <ta e="T114" id="Seg_3752" s="T113">три-COLL</ta>
            <ta e="T115" id="Seg_3753" s="T114">а</ta>
            <ta e="T116" id="Seg_3754" s="T115">ты.DAT</ta>
            <ta e="T117" id="Seg_3755" s="T116">надо</ta>
            <ta e="T118" id="Seg_3756" s="T117">там</ta>
            <ta e="T119" id="Seg_3757" s="T118">пойти-INF.LAT</ta>
            <ta e="T120" id="Seg_3758" s="T119">этот.[NOM.SG]</ta>
            <ta e="T121" id="Seg_3759" s="T120">сказать-IPFVZ.[3SG]</ta>
            <ta e="T122" id="Seg_3760" s="T121">ну</ta>
            <ta e="T123" id="Seg_3761" s="T122">и</ta>
            <ta e="T124" id="Seg_3762" s="T123">что</ta>
            <ta e="T125" id="Seg_3763" s="T124">я.NOM</ta>
            <ta e="T126" id="Seg_3764" s="T125">пойти-FUT-1SG</ta>
            <ta e="T127" id="Seg_3765" s="T126">там</ta>
            <ta e="T128" id="Seg_3766" s="T127">много</ta>
            <ta e="T129" id="Seg_3767" s="T128">цветок-PL</ta>
            <ta e="T130" id="Seg_3768" s="T129">PTCL</ta>
            <ta e="T131" id="Seg_3769" s="T130">рвать-FUT-2SG</ta>
            <ta e="T132" id="Seg_3770" s="T131">принести-PST.[3SG]</ta>
            <ta e="T133" id="Seg_3771" s="T132">посылать-PST.[3SG]</ta>
            <ta e="T134" id="Seg_3772" s="T133">этот-ACC</ta>
            <ta e="T135" id="Seg_3773" s="T134">там</ta>
            <ta e="T136" id="Seg_3774" s="T135">ворота-PL</ta>
            <ta e="T137" id="Seg_3775" s="T136">закрыть-MOM-PST-3PL</ta>
            <ta e="T138" id="Seg_3776" s="T137">этот-ACC</ta>
            <ta e="T139" id="Seg_3777" s="T138">этот.[NOM.SG]</ta>
            <ta e="T142" id="Seg_3778" s="T141">оставить-MOM-PST.[3SG]</ta>
            <ta e="T143" id="Seg_3779" s="T142">и</ta>
            <ta e="T144" id="Seg_3780" s="T143">чум-LAT/LOC.3SG</ta>
            <ta e="T145" id="Seg_3781" s="T144">прийти-PST.[3SG]</ta>
            <ta e="T147" id="Seg_3782" s="T146">день.[NOM.SG]</ta>
            <ta e="T148" id="Seg_3783" s="T147">идти-PST.[3SG]</ta>
            <ta e="T149" id="Seg_3784" s="T148">идти-PST.[3SG]</ta>
            <ta e="T150" id="Seg_3785" s="T149">цветок-PL</ta>
            <ta e="T151" id="Seg_3786" s="T150">всегда</ta>
            <ta e="T152" id="Seg_3787" s="T151">писать-DETR-PTCP</ta>
            <ta e="T153" id="Seg_3788" s="T152">Маланья-GEN</ta>
            <ta e="T154" id="Seg_3789" s="T153">что.[NOM.SG]</ta>
            <ta e="T155" id="Seg_3790" s="T154">идти-FUT-3SG</ta>
            <ta e="T156" id="Seg_3791" s="T155">всегда</ta>
            <ta e="T157" id="Seg_3792" s="T156">Маланья.[NOM.SG]</ta>
            <ta e="T158" id="Seg_3793" s="T157">дом-LAT</ta>
            <ta e="T159" id="Seg_3794" s="T158">прийти-PST.[3SG]</ta>
            <ta e="T160" id="Seg_3795" s="T159">PTCL</ta>
            <ta e="T161" id="Seg_3796" s="T160">железо-NOM/GEN/ACC.3PL</ta>
            <ta e="T162" id="Seg_3797" s="T161">какой</ta>
            <ta e="T163" id="Seg_3798" s="T162">ложка-PL</ta>
            <ta e="T164" id="Seg_3799" s="T163">всегда</ta>
            <ta e="T165" id="Seg_3800" s="T164">там</ta>
            <ta e="T166" id="Seg_3801" s="T165">Маланья-GEN</ta>
            <ta e="T167" id="Seg_3802" s="T166">нож-PL</ta>
            <ta e="T169" id="Seg_3803" s="T168">тогда</ta>
            <ta e="T170" id="Seg_3804" s="T169">есть-PST.[3SG]</ta>
            <ta e="T171" id="Seg_3805" s="T170">съесть-PST.[3SG]</ta>
            <ta e="T173" id="Seg_3806" s="T172">я.LAT</ta>
            <ta e="T176" id="Seg_3807" s="T175">жить-INF.LAT</ta>
            <ta e="T177" id="Seg_3808" s="T176">там</ta>
            <ta e="T178" id="Seg_3809" s="T177">убежать</ta>
            <ta e="T182" id="Seg_3810" s="T181">тогда</ta>
            <ta e="T183" id="Seg_3811" s="T182">ложиться-PST.[3SG]</ta>
            <ta e="T184" id="Seg_3812" s="T183">спать-INF.LAT</ta>
            <ta e="T185" id="Seg_3813" s="T184">вечер-LOC.ADV</ta>
            <ta e="T186" id="Seg_3814" s="T185">прийти-PST.[3SG]</ta>
            <ta e="T187" id="Seg_3815" s="T186">мужчина.[NOM.SG]</ta>
            <ta e="T188" id="Seg_3816" s="T187">ты.NOM</ta>
            <ta e="T189" id="Seg_3817" s="T188">Маланья.[NOM.SG]</ta>
            <ta e="T190" id="Seg_3818" s="T189">ты.NOM</ta>
            <ta e="T191" id="Seg_3819" s="T190">я.ACC</ta>
            <ta e="T192" id="Seg_3820" s="T191">жалеть-PRS-2SG</ta>
            <ta e="T193" id="Seg_3821" s="T192">а</ta>
            <ta e="T194" id="Seg_3822" s="T193">я.NOM</ta>
            <ta e="T195" id="Seg_3823" s="T194">класть</ta>
            <ta e="T197" id="Seg_3824" s="T196">NEG</ta>
            <ta e="T198" id="Seg_3825" s="T197">знать-1SG</ta>
            <ta e="T200" id="Seg_3826" s="T199">кто.[NOM.SG]</ta>
            <ta e="T201" id="Seg_3827" s="T200">ты.NOM</ta>
            <ta e="T202" id="Seg_3828" s="T201">прийти-IMP.2SG</ta>
            <ta e="T203" id="Seg_3829" s="T202">здесь</ta>
            <ta e="T204" id="Seg_3830" s="T203">нет</ta>
            <ta e="T205" id="Seg_3831" s="T204">я.NOM</ta>
            <ta e="T206" id="Seg_3832" s="T205">NEG</ta>
            <ta e="T207" id="Seg_3833" s="T206">прийти-FUT-1SG</ta>
            <ta e="T208" id="Seg_3834" s="T207">ты.NOM</ta>
            <ta e="T209" id="Seg_3835" s="T208">пугать-MOM-FUT-2SG</ta>
            <ta e="T210" id="Seg_3836" s="T209">тогда</ta>
            <ta e="T396" id="Seg_3837" s="T210">пойти-CVB</ta>
            <ta e="T211" id="Seg_3838" s="T396">исчезнуть-PST.[3SG]</ta>
            <ta e="T212" id="Seg_3839" s="T211">этот.[NOM.SG]</ta>
            <ta e="T213" id="Seg_3840" s="T212">думать-PST.[3SG]</ta>
            <ta e="T214" id="Seg_3841" s="T213">думать-PST.[3SG]</ta>
            <ta e="T215" id="Seg_3842" s="T214">тогда</ta>
            <ta e="T216" id="Seg_3843" s="T215">два.[NOM.SG]</ta>
            <ta e="T217" id="Seg_3844" s="T216">вечер</ta>
            <ta e="T218" id="Seg_3845" s="T217">прийти-PST.[3SG]</ta>
            <ta e="T219" id="Seg_3846" s="T218">этот.[NOM.SG]</ta>
            <ta e="T220" id="Seg_3847" s="T219">PTCL</ta>
            <ta e="T221" id="Seg_3848" s="T220">вода-LAT</ta>
            <ta e="T222" id="Seg_3849" s="T221">пойти-PST.[3SG]</ta>
            <ta e="T223" id="Seg_3850" s="T222">рыба.[NOM.SG]</ta>
            <ta e="T224" id="Seg_3851" s="T223">там</ta>
            <ta e="T225" id="Seg_3852" s="T224">и</ta>
            <ta e="T226" id="Seg_3853" s="T225">то</ta>
            <ta e="T227" id="Seg_3854" s="T226">писать-DETR-PTCP</ta>
            <ta e="T228" id="Seg_3855" s="T227">Маланья-GEN</ta>
            <ta e="T229" id="Seg_3856" s="T228">рыба.[NOM.SG]</ta>
            <ta e="T230" id="Seg_3857" s="T229">тогда</ta>
            <ta e="T231" id="Seg_3858" s="T230">этот.[NOM.SG]</ta>
            <ta e="T232" id="Seg_3859" s="T231">опять</ta>
            <ta e="T233" id="Seg_3860" s="T232">вечер-NOM/GEN.3SG</ta>
            <ta e="T234" id="Seg_3861" s="T233">прийти-PST.[3SG]</ta>
            <ta e="T235" id="Seg_3862" s="T234">думать-PST.[3SG]</ta>
            <ta e="T236" id="Seg_3863" s="T235">думать-PST.[3SG]</ta>
            <ta e="T237" id="Seg_3864" s="T236">тогда</ta>
            <ta e="T238" id="Seg_3865" s="T237">сказать-IPFVZ.[3SG]</ta>
            <ta e="T239" id="Seg_3866" s="T238">NEG</ta>
            <ta e="T240" id="Seg_3867" s="T239">знать-1SG</ta>
            <ta e="T241" id="Seg_3868" s="T240">кто.[NOM.SG]</ta>
            <ta e="T243" id="Seg_3869" s="T242">кто.[NOM.SG]</ta>
            <ta e="T244" id="Seg_3870" s="T243">ты.NOM</ta>
            <ta e="T245" id="Seg_3871" s="T244">быть-PRS-2SG</ta>
            <ta e="T246" id="Seg_3872" s="T245">нет</ta>
            <ta e="T247" id="Seg_3873" s="T246">я.NOM</ta>
            <ta e="T248" id="Seg_3874" s="T247">NEG.AUX-1SG</ta>
            <ta e="T249" id="Seg_3875" s="T248">прийти-CNG</ta>
            <ta e="T250" id="Seg_3876" s="T249">ты.NOM</ta>
            <ta e="T251" id="Seg_3877" s="T250">опять</ta>
            <ta e="T252" id="Seg_3878" s="T251">пугать-MOM-PST-2SG</ta>
            <ta e="T253" id="Seg_3879" s="T252">тогда</ta>
            <ta e="T254" id="Seg_3880" s="T253">три.[NOM.SG]</ta>
            <ta e="T255" id="Seg_3881" s="T254">вечер</ta>
            <ta e="T256" id="Seg_3882" s="T255">пойти-PST.[3SG]</ta>
            <ta e="T257" id="Seg_3883" s="T256">этот-ACC</ta>
            <ta e="T258" id="Seg_3884" s="T257">прийти-PST.[3SG]</ta>
            <ta e="T259" id="Seg_3885" s="T258">три.[NOM.SG]</ta>
            <ta e="T260" id="Seg_3886" s="T259">%%</ta>
            <ta e="T261" id="Seg_3887" s="T260">вечер.[NOM.SG]</ta>
            <ta e="T262" id="Seg_3888" s="T261">этот.[NOM.SG]</ta>
            <ta e="T263" id="Seg_3889" s="T262">опять</ta>
            <ta e="T264" id="Seg_3890" s="T263">ложиться-PST.[3SG]</ta>
            <ta e="T265" id="Seg_3891" s="T264">спать-INF.LAT</ta>
            <ta e="T266" id="Seg_3892" s="T265">этот.[NOM.SG]</ta>
            <ta e="T267" id="Seg_3893" s="T266">опять</ta>
            <ta e="T268" id="Seg_3894" s="T267">прийти-PST.[3SG]</ta>
            <ta e="T269" id="Seg_3895" s="T268">спросить-DUR.[3SG]</ta>
            <ta e="T270" id="Seg_3896" s="T269">ну</ta>
            <ta e="T271" id="Seg_3897" s="T270">думать-MOM-PST-2SG</ta>
            <ta e="T272" id="Seg_3898" s="T271">PTCL</ta>
            <ta e="T273" id="Seg_3899" s="T272">прийти-IMP.2SG</ta>
            <ta e="T274" id="Seg_3900" s="T273">здесь</ta>
            <ta e="T275" id="Seg_3901" s="T274">я.NOM</ta>
            <ta e="T276" id="Seg_3902" s="T275">смотреть-FRQ-FUT-1SG</ta>
            <ta e="T277" id="Seg_3903" s="T276">ты.ACC</ta>
            <ta e="T278" id="Seg_3904" s="T277">PTCL</ta>
            <ta e="T279" id="Seg_3905" s="T278">ты.NOM</ta>
            <ta e="T280" id="Seg_3906" s="T279">пугать-MOM-FUT-2SG</ta>
            <ta e="T281" id="Seg_3907" s="T280">этот.[NOM.SG]</ta>
            <ta e="T282" id="Seg_3908" s="T281">сказать-IPFVZ.[3SG]</ta>
            <ta e="T283" id="Seg_3909" s="T282">NEG</ta>
            <ta e="T284" id="Seg_3910" s="T283">пугать-MOM-FUT-1SG</ta>
            <ta e="T285" id="Seg_3911" s="T284">этот.[NOM.SG]</ta>
            <ta e="T286" id="Seg_3912" s="T285">прийти-PST.[3SG]</ta>
            <ta e="T287" id="Seg_3913" s="T286">дом-LAT</ta>
            <ta e="T288" id="Seg_3914" s="T287">этот.[NOM.SG]</ta>
            <ta e="T289" id="Seg_3915" s="T288">NEG</ta>
            <ta e="T290" id="Seg_3916" s="T289">пугать-MOM-PST.[3SG]</ta>
            <ta e="T291" id="Seg_3917" s="T290">сказать-PRS.[3SG]</ta>
            <ta e="T292" id="Seg_3918" s="T291">жить-FUT-1SG</ta>
            <ta e="T293" id="Seg_3919" s="T292">этот-INS</ta>
            <ta e="T294" id="Seg_3920" s="T293">что.[NOM.SG]</ta>
            <ta e="T296" id="Seg_3921" s="T295">JUSS</ta>
            <ta e="T297" id="Seg_3922" s="T296">NEG</ta>
            <ta e="T298" id="Seg_3923" s="T297">красивый.[NOM.SG]</ta>
            <ta e="T299" id="Seg_3924" s="T298">и</ta>
            <ta e="T300" id="Seg_3925" s="T299">здесь</ta>
            <ta e="T301" id="Seg_3926" s="T300">PTCL</ta>
            <ta e="T302" id="Seg_3927" s="T301">я.LAT</ta>
            <ta e="T303" id="Seg_3928" s="T302">и</ta>
            <ta e="T304" id="Seg_3929" s="T303">я.LAT</ta>
            <ta e="T305" id="Seg_3930" s="T304">тогда</ta>
            <ta e="T306" id="Seg_3931" s="T305">жить-PST.[3SG]</ta>
            <ta e="T307" id="Seg_3932" s="T306">жить-PST.[3SG]</ta>
            <ta e="T308" id="Seg_3933" s="T307">сказать-IPFVZ.[3SG]</ta>
            <ta e="T309" id="Seg_3934" s="T308">я.NOM</ta>
            <ta e="T310" id="Seg_3935" s="T309">пойти-FUT-1SG</ta>
            <ta e="T312" id="Seg_3936" s="T311">отец-LAT</ta>
            <ta e="T313" id="Seg_3937" s="T312">этот.[NOM.SG]</ta>
            <ta e="T314" id="Seg_3938" s="T313">этот-LAT</ta>
            <ta e="T316" id="Seg_3939" s="T315">кольцо.[NOM.SG]</ta>
            <ta e="T317" id="Seg_3940" s="T316">дать-PST.[3SG]</ta>
            <ta e="T318" id="Seg_3941" s="T317">ну</ta>
            <ta e="T319" id="Seg_3942" s="T318">пойти-EP-IMP.2SG</ta>
            <ta e="T320" id="Seg_3943" s="T319">когда</ta>
            <ta e="T321" id="Seg_3944" s="T320">этот.[NOM.SG]</ta>
            <ta e="T322" id="Seg_3945" s="T321">кольцо.[NOM.SG]</ta>
            <ta e="T323" id="Seg_3946" s="T322">черный.[NOM.SG]</ta>
            <ta e="T324" id="Seg_3947" s="T323">стать-FUT-3SG</ta>
            <ta e="T325" id="Seg_3948" s="T324">тогда</ta>
            <ta e="T326" id="Seg_3949" s="T325">скоро</ta>
            <ta e="T327" id="Seg_3950" s="T326">прийти-IMP.2SG</ta>
            <ta e="T328" id="Seg_3951" s="T327">а.то</ta>
            <ta e="T329" id="Seg_3952" s="T328">я.NOM</ta>
            <ta e="T330" id="Seg_3953" s="T329">умереть-RES-FUT-1SG</ta>
            <ta e="T331" id="Seg_3954" s="T330">этот</ta>
            <ta e="T332" id="Seg_3955" s="T331">пойти-PST.[3SG]</ta>
            <ta e="T333" id="Seg_3956" s="T332">сестра-ABL.3SG</ta>
            <ta e="T334" id="Seg_3957" s="T333">PTCL</ta>
            <ta e="T335" id="Seg_3958" s="T334">этот-LAT</ta>
            <ta e="T337" id="Seg_3959" s="T336">вода.[NOM.SG]</ta>
            <ta e="T338" id="Seg_3960" s="T337">какой=INDEF</ta>
            <ta e="T339" id="Seg_3961" s="T338">дать-PST.[3SG]</ta>
            <ta e="T340" id="Seg_3962" s="T339">этот</ta>
            <ta e="T341" id="Seg_3963" s="T340">PTCL</ta>
            <ta e="T342" id="Seg_3964" s="T341">спать-PST.[3SG]</ta>
            <ta e="T343" id="Seg_3965" s="T342">спать-PST.[3SG]</ta>
            <ta e="T344" id="Seg_3966" s="T343">встать-PST.[3SG]</ta>
            <ta e="T345" id="Seg_3967" s="T344">колечко-NOM/GEN.3SG</ta>
            <ta e="T346" id="Seg_3968" s="T345">PTCL</ta>
            <ta e="T347" id="Seg_3969" s="T346">черный.[NOM.SG]</ta>
            <ta e="T348" id="Seg_3970" s="T347">этот.[NOM.SG]</ta>
            <ta e="T349" id="Seg_3971" s="T348">PTCL</ta>
            <ta e="T350" id="Seg_3972" s="T349">что.[NOM.SG]</ta>
            <ta e="T351" id="Seg_3973" s="T350">выбросить-PST.[3SG]</ta>
            <ta e="T352" id="Seg_3974" s="T351">и</ta>
            <ta e="T353" id="Seg_3975" s="T352">пойти-PST.[3SG]</ta>
            <ta e="T354" id="Seg_3976" s="T353">прийти-PST.[3SG]</ta>
            <ta e="T355" id="Seg_3977" s="T354">там</ta>
            <ta e="T356" id="Seg_3978" s="T355">лежать-DUR.[3SG]</ta>
            <ta e="T357" id="Seg_3979" s="T356">умереть-RES-PST.[3SG]</ta>
            <ta e="T358" id="Seg_3980" s="T357">PTCL</ta>
            <ta e="T359" id="Seg_3981" s="T358">плакать-PST.[3SG]</ta>
            <ta e="T360" id="Seg_3982" s="T359">я.NOM</ta>
            <ta e="T361" id="Seg_3983" s="T360">ты.DAT</ta>
            <ta e="T362" id="Seg_3984" s="T361">жалеть-PRS-1SG</ta>
            <ta e="T363" id="Seg_3985" s="T362">встать-IMP.2SG</ta>
            <ta e="T364" id="Seg_3986" s="T363">жалеть-PRS-1SG</ta>
            <ta e="T365" id="Seg_3987" s="T364">встать-IMP.2SG</ta>
            <ta e="T366" id="Seg_3988" s="T365">этот.[NOM.SG]</ta>
            <ta e="T367" id="Seg_3989" s="T366">встать-PST.[3SG]</ta>
            <ta e="T368" id="Seg_3990" s="T367">и</ta>
            <ta e="T369" id="Seg_3991" s="T368">очень</ta>
            <ta e="T370" id="Seg_3992" s="T369">красивый.[NOM.SG]</ta>
            <ta e="T371" id="Seg_3993" s="T370">мальчик.[NOM.SG]</ta>
            <ta e="T372" id="Seg_3994" s="T371">стать-RES-PST.[3SG]</ta>
            <ta e="T373" id="Seg_3995" s="T372">тогда</ta>
            <ta e="T374" id="Seg_3996" s="T373">колокол-PL</ta>
            <ta e="T375" id="Seg_3997" s="T374">PTCL</ta>
            <ta e="T376" id="Seg_3998" s="T375">греметь-DUR-3PL</ta>
            <ta e="T377" id="Seg_3999" s="T376">люди.[NOM.SG]</ta>
            <ta e="T378" id="Seg_4000" s="T377">идти-DUR-3PL</ta>
            <ta e="T379" id="Seg_4001" s="T378">стать-RES-PST.[3SG]</ta>
            <ta e="T380" id="Seg_4002" s="T379">город.[NOM.SG]</ta>
            <ta e="T381" id="Seg_4003" s="T380">а</ta>
            <ta e="T382" id="Seg_4004" s="T381">этот.[NOM.SG]</ta>
            <ta e="T383" id="Seg_4005" s="T382">этот.[NOM.SG]</ta>
            <ta e="T384" id="Seg_4006" s="T383">мальчик.[NOM.SG]</ta>
            <ta e="T385" id="Seg_4007" s="T384">вождь.[NOM.SG]</ta>
            <ta e="T386" id="Seg_4008" s="T385">стать-RES-PST.[3SG]</ta>
            <ta e="T387" id="Seg_4009" s="T386">тогда</ta>
            <ta e="T388" id="Seg_4010" s="T387">этот.[NOM.SG]</ta>
            <ta e="T390" id="Seg_4011" s="T389">этот-ACC</ta>
            <ta e="T391" id="Seg_4012" s="T390">мужчина-LAT</ta>
            <ta e="T392" id="Seg_4013" s="T391">взять-PST.[3SG]</ta>
            <ta e="T393" id="Seg_4014" s="T392">и</ta>
            <ta e="T395" id="Seg_4015" s="T394">жить-INF.LAT</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_4016" s="T0">v-v:tense-v:pn</ta>
            <ta e="T2" id="Seg_4017" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_4018" s="T2">dempro-n:case</ta>
            <ta e="T6" id="Seg_4019" s="T5">dempro-n:case</ta>
            <ta e="T7" id="Seg_4020" s="T6">v-v:tense-v:pn</ta>
            <ta e="T8" id="Seg_4021" s="T7">num-n:case</ta>
            <ta e="T9" id="Seg_4022" s="T8">n-n:num-n:case.poss</ta>
            <ta e="T10" id="Seg_4023" s="T9">dempro-n:case</ta>
            <ta e="T11" id="Seg_4024" s="T10">v</ta>
            <ta e="T13" id="Seg_4025" s="T12">v-v:n.fin</ta>
            <ta e="T14" id="Seg_4026" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_4027" s="T14">v-v:n.fin</ta>
            <ta e="T16" id="Seg_4028" s="T15">adj-n:case</ta>
            <ta e="T17" id="Seg_4029" s="T16">n-n:case.poss</ta>
            <ta e="T18" id="Seg_4030" s="T17">v-v&gt;v-v:pn</ta>
            <ta e="T19" id="Seg_4031" s="T18">que-n:case</ta>
            <ta e="T20" id="Seg_4032" s="T19">pers</ta>
            <ta e="T21" id="Seg_4033" s="T20">v-v:n.fin</ta>
            <ta e="T22" id="Seg_4034" s="T21">pers</ta>
            <ta e="T23" id="Seg_4035" s="T22">pers</ta>
            <ta e="T24" id="Seg_4036" s="T23">adj-n:case</ta>
            <ta e="T25" id="Seg_4037" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_4038" s="T25">v-v:mood.pn</ta>
            <ta e="T27" id="Seg_4039" s="T26">conj</ta>
            <ta e="T28" id="Seg_4040" s="T27">adj-n:case.poss</ta>
            <ta e="T29" id="Seg_4041" s="T28">n-n:case</ta>
            <ta e="T30" id="Seg_4042" s="T29">pers</ta>
            <ta e="T31" id="Seg_4043" s="T30">pers</ta>
            <ta e="T32" id="Seg_4044" s="T31">n-n:num</ta>
            <ta e="T33" id="Seg_4045" s="T32">adj-n:case</ta>
            <ta e="T34" id="Seg_4046" s="T33">v-v:mood.pn</ta>
            <ta e="T35" id="Seg_4047" s="T34">conj</ta>
            <ta e="T36" id="Seg_4048" s="T35">num-n:case</ta>
            <ta e="T37" id="Seg_4049" s="T36">adv</ta>
            <ta e="T38" id="Seg_4050" s="T37">adj-n:case</ta>
            <ta e="T39" id="Seg_4051" s="T38">n-n:case</ta>
            <ta e="T40" id="Seg_4052" s="T39">pers</ta>
            <ta e="T41" id="Seg_4053" s="T40">que-n:case</ta>
            <ta e="T42" id="Seg_4054" s="T41">v-v:n.fin</ta>
            <ta e="T43" id="Seg_4055" s="T42">pers</ta>
            <ta e="T44" id="Seg_4056" s="T43">pers</ta>
            <ta e="T45" id="Seg_4057" s="T44">v-v:mood.pn</ta>
            <ta e="T46" id="Seg_4058" s="T45">adj-n:case</ta>
            <ta e="T47" id="Seg_4059" s="T46">n-n:case</ta>
            <ta e="T48" id="Seg_4060" s="T47">dempro-n:case</ta>
            <ta e="T49" id="Seg_4061" s="T48">v-v:tense-v:pn</ta>
            <ta e="T50" id="Seg_4062" s="T49">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T51" id="Seg_4063" s="T50">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_4064" s="T51">v-v:tense-v:pn</ta>
            <ta e="T53" id="Seg_4065" s="T52">adv</ta>
            <ta e="T54" id="Seg_4066" s="T53">n-n:case</ta>
            <ta e="T55" id="Seg_4067" s="T54">num-n:case</ta>
            <ta e="T56" id="Seg_4068" s="T55">v-v:tense-v:pn</ta>
            <ta e="T57" id="Seg_4069" s="T56">n-n:num</ta>
            <ta e="T58" id="Seg_4070" s="T57">conj</ta>
            <ta e="T60" id="Seg_4071" s="T59">n-n:case</ta>
            <ta e="T61" id="Seg_4072" s="T60">v</ta>
            <ta e="T62" id="Seg_4073" s="T61">adv</ta>
            <ta e="T63" id="Seg_4074" s="T62">v-v:tense-v:pn</ta>
            <ta e="T64" id="Seg_4075" s="T63">conj</ta>
            <ta e="T65" id="Seg_4076" s="T64">v-v&gt;v-v:pn</ta>
            <ta e="T66" id="Seg_4077" s="T65">que</ta>
            <ta e="T68" id="Seg_4078" s="T67">v-v:n.fin</ta>
            <ta e="T69" id="Seg_4079" s="T68">adj-n:case</ta>
            <ta e="T70" id="Seg_4080" s="T69">n-n:case</ta>
            <ta e="T71" id="Seg_4081" s="T70">v-v:tense-v:pn</ta>
            <ta e="T72" id="Seg_4082" s="T71">v-v:tense-v:pn</ta>
            <ta e="T73" id="Seg_4083" s="T72">ptcl</ta>
            <ta e="T74" id="Seg_4084" s="T73">n-n:case</ta>
            <ta e="T75" id="Seg_4085" s="T74">n-n:num</ta>
            <ta e="T76" id="Seg_4086" s="T75">adv</ta>
            <ta e="T77" id="Seg_4087" s="T76">v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_4088" s="T77">v-v:tense-v:pn</ta>
            <ta e="T79" id="Seg_4089" s="T78">n-n:num</ta>
            <ta e="T81" id="Seg_4090" s="T80">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T82" id="Seg_4091" s="T81">adv</ta>
            <ta e="T83" id="Seg_4092" s="T82">v-v:tense-v:pn</ta>
            <ta e="T84" id="Seg_4093" s="T83">v-v:tense-v:pn</ta>
            <ta e="T85" id="Seg_4094" s="T84">%%</ta>
            <ta e="T86" id="Seg_4095" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_4096" s="T86">v-v:tense-v:pn</ta>
            <ta e="T88" id="Seg_4097" s="T87">n-n:case</ta>
            <ta e="T90" id="Seg_4098" s="T89">refl-n:case.poss</ta>
            <ta e="T91" id="Seg_4099" s="T90">n-n:case.poss</ta>
            <ta e="T92" id="Seg_4100" s="T91">v-v:tense-v:pn</ta>
            <ta e="T93" id="Seg_4101" s="T92">pers</ta>
            <ta e="T95" id="Seg_4102" s="T94">v-v:tense-v:pn</ta>
            <ta e="T96" id="Seg_4103" s="T95">adv</ta>
            <ta e="T97" id="Seg_4104" s="T96">dempro-n:case</ta>
            <ta e="T98" id="Seg_4105" s="T97">v-v:tense-v:pn</ta>
            <ta e="T99" id="Seg_4106" s="T98">v-v:tense-v:pn</ta>
            <ta e="T100" id="Seg_4107" s="T99">v-v:tense-v:pn</ta>
            <ta e="T101" id="Seg_4108" s="T100">adv</ta>
            <ta e="T102" id="Seg_4109" s="T101">n-n:case</ta>
            <ta e="T103" id="Seg_4110" s="T102">v-v:tense-v:pn</ta>
            <ta e="T104" id="Seg_4111" s="T103">n-n:case.poss-n:case</ta>
            <ta e="T107" id="Seg_4112" s="T106">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T108" id="Seg_4113" s="T107">conj</ta>
            <ta e="T109" id="Seg_4114" s="T108">v-v:tense-v:pn</ta>
            <ta e="T110" id="Seg_4115" s="T109">n-n:case.poss</ta>
            <ta e="T111" id="Seg_4116" s="T110">v-v:tense-v:pn</ta>
            <ta e="T112" id="Seg_4117" s="T111">quant</ta>
            <ta e="T113" id="Seg_4118" s="T112">v-v:tense-v:pn</ta>
            <ta e="T114" id="Seg_4119" s="T113">num-num&gt;num</ta>
            <ta e="T115" id="Seg_4120" s="T114">conj</ta>
            <ta e="T116" id="Seg_4121" s="T115">pers</ta>
            <ta e="T117" id="Seg_4122" s="T116">ptcl</ta>
            <ta e="T118" id="Seg_4123" s="T117">adv</ta>
            <ta e="T119" id="Seg_4124" s="T118">v-v:n.fin</ta>
            <ta e="T120" id="Seg_4125" s="T119">dempro-n:case</ta>
            <ta e="T121" id="Seg_4126" s="T120">v-v&gt;v-v:pn</ta>
            <ta e="T122" id="Seg_4127" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_4128" s="T122">conj</ta>
            <ta e="T124" id="Seg_4129" s="T123">conj</ta>
            <ta e="T125" id="Seg_4130" s="T124">pers</ta>
            <ta e="T126" id="Seg_4131" s="T125">v-v:tense-v:pn</ta>
            <ta e="T127" id="Seg_4132" s="T126">adv</ta>
            <ta e="T128" id="Seg_4133" s="T127">quant</ta>
            <ta e="T129" id="Seg_4134" s="T128">n-n:num</ta>
            <ta e="T130" id="Seg_4135" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_4136" s="T130">v-v:tense-v:pn</ta>
            <ta e="T132" id="Seg_4137" s="T131">v-v:tense-v:pn</ta>
            <ta e="T133" id="Seg_4138" s="T132">v-v:tense-v:pn</ta>
            <ta e="T134" id="Seg_4139" s="T133">dempro-n:case</ta>
            <ta e="T135" id="Seg_4140" s="T134">adv</ta>
            <ta e="T136" id="Seg_4141" s="T135">n-n:num</ta>
            <ta e="T137" id="Seg_4142" s="T136">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T138" id="Seg_4143" s="T137">dempro-n:case</ta>
            <ta e="T139" id="Seg_4144" s="T138">dempro-n:case</ta>
            <ta e="T142" id="Seg_4145" s="T141">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T143" id="Seg_4146" s="T142">conj</ta>
            <ta e="T144" id="Seg_4147" s="T143">n-n:case.poss</ta>
            <ta e="T145" id="Seg_4148" s="T144">v-v:tense-v:pn</ta>
            <ta e="T147" id="Seg_4149" s="T146">n-n:case</ta>
            <ta e="T148" id="Seg_4150" s="T147">v-v:tense-v:pn</ta>
            <ta e="T149" id="Seg_4151" s="T148">v-v:tense-v:pn</ta>
            <ta e="T150" id="Seg_4152" s="T149">n-n:num</ta>
            <ta e="T151" id="Seg_4153" s="T150">adv</ta>
            <ta e="T152" id="Seg_4154" s="T151">v-v&gt;v-v:n.fin</ta>
            <ta e="T153" id="Seg_4155" s="T152">propr-n:case</ta>
            <ta e="T154" id="Seg_4156" s="T153">que-n:case</ta>
            <ta e="T155" id="Seg_4157" s="T154">v-v:tense-v:pn</ta>
            <ta e="T156" id="Seg_4158" s="T155">adv</ta>
            <ta e="T157" id="Seg_4159" s="T156">propr-n:case</ta>
            <ta e="T158" id="Seg_4160" s="T157">n-n:case</ta>
            <ta e="T159" id="Seg_4161" s="T158">v-v:tense-v:pn</ta>
            <ta e="T160" id="Seg_4162" s="T159">ptcl</ta>
            <ta e="T161" id="Seg_4163" s="T160">n-n:case.poss</ta>
            <ta e="T162" id="Seg_4164" s="T161">que</ta>
            <ta e="T163" id="Seg_4165" s="T162">n-n:num</ta>
            <ta e="T164" id="Seg_4166" s="T163">adv</ta>
            <ta e="T165" id="Seg_4167" s="T164">adv</ta>
            <ta e="T166" id="Seg_4168" s="T165">propr-n:case</ta>
            <ta e="T167" id="Seg_4169" s="T166">n-n:num</ta>
            <ta e="T169" id="Seg_4170" s="T168">adv</ta>
            <ta e="T170" id="Seg_4171" s="T169">v-v:tense-v:pn</ta>
            <ta e="T171" id="Seg_4172" s="T170">v-v:tense-v:pn</ta>
            <ta e="T173" id="Seg_4173" s="T172">pers</ta>
            <ta e="T176" id="Seg_4174" s="T175">v-v:n.fin</ta>
            <ta e="T177" id="Seg_4175" s="T176">adv</ta>
            <ta e="T178" id="Seg_4176" s="T177">v</ta>
            <ta e="T182" id="Seg_4177" s="T181">adv</ta>
            <ta e="T183" id="Seg_4178" s="T182">v-v:tense-v:pn</ta>
            <ta e="T184" id="Seg_4179" s="T183">v-v:n.fin</ta>
            <ta e="T185" id="Seg_4180" s="T184">n-n:case</ta>
            <ta e="T186" id="Seg_4181" s="T185">v-v:tense-v:pn</ta>
            <ta e="T187" id="Seg_4182" s="T186">n-n:case</ta>
            <ta e="T188" id="Seg_4183" s="T187">pers</ta>
            <ta e="T189" id="Seg_4184" s="T188">propr-n:case</ta>
            <ta e="T190" id="Seg_4185" s="T189">pers</ta>
            <ta e="T191" id="Seg_4186" s="T190">pers</ta>
            <ta e="T192" id="Seg_4187" s="T191">v-v:tense-v:pn</ta>
            <ta e="T193" id="Seg_4188" s="T192">conj</ta>
            <ta e="T194" id="Seg_4189" s="T193">pers</ta>
            <ta e="T195" id="Seg_4190" s="T194">v</ta>
            <ta e="T197" id="Seg_4191" s="T196">ptcl</ta>
            <ta e="T198" id="Seg_4192" s="T197">v-v:pn</ta>
            <ta e="T200" id="Seg_4193" s="T199">que-n:case</ta>
            <ta e="T201" id="Seg_4194" s="T200">pers</ta>
            <ta e="T202" id="Seg_4195" s="T201">v-v:mood.pn</ta>
            <ta e="T203" id="Seg_4196" s="T202">adv</ta>
            <ta e="T204" id="Seg_4197" s="T203">ptcl</ta>
            <ta e="T205" id="Seg_4198" s="T204">pers</ta>
            <ta e="T206" id="Seg_4199" s="T205">ptcl</ta>
            <ta e="T207" id="Seg_4200" s="T206">v-v:tense-v:pn</ta>
            <ta e="T208" id="Seg_4201" s="T207">pers</ta>
            <ta e="T209" id="Seg_4202" s="T208">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T210" id="Seg_4203" s="T209">adv</ta>
            <ta e="T396" id="Seg_4204" s="T210">v-v:n-fin</ta>
            <ta e="T211" id="Seg_4205" s="T396">v-v:tense-v:pn</ta>
            <ta e="T212" id="Seg_4206" s="T211">dempro-n:case</ta>
            <ta e="T213" id="Seg_4207" s="T212">v-v:tense-v:pn</ta>
            <ta e="T214" id="Seg_4208" s="T213">v-v:tense-v:pn</ta>
            <ta e="T215" id="Seg_4209" s="T214">adv</ta>
            <ta e="T216" id="Seg_4210" s="T215">num-n:case</ta>
            <ta e="T217" id="Seg_4211" s="T216">n</ta>
            <ta e="T218" id="Seg_4212" s="T217">v-v:tense-v:pn</ta>
            <ta e="T219" id="Seg_4213" s="T218">dempro-n:case</ta>
            <ta e="T220" id="Seg_4214" s="T219">ptcl</ta>
            <ta e="T221" id="Seg_4215" s="T220">n-n:case</ta>
            <ta e="T222" id="Seg_4216" s="T221">v-v:tense-v:pn</ta>
            <ta e="T223" id="Seg_4217" s="T222">n-n:case</ta>
            <ta e="T224" id="Seg_4218" s="T223">adv</ta>
            <ta e="T225" id="Seg_4219" s="T224">conj</ta>
            <ta e="T226" id="Seg_4220" s="T225">conj</ta>
            <ta e="T227" id="Seg_4221" s="T226">v-v&gt;v-v:n.fin</ta>
            <ta e="T228" id="Seg_4222" s="T227">propr-n:case</ta>
            <ta e="T229" id="Seg_4223" s="T228">n-n:case</ta>
            <ta e="T230" id="Seg_4224" s="T229">adv</ta>
            <ta e="T231" id="Seg_4225" s="T230">dempro-n:case</ta>
            <ta e="T232" id="Seg_4226" s="T231">adv</ta>
            <ta e="T233" id="Seg_4227" s="T232">n-n:case.poss</ta>
            <ta e="T234" id="Seg_4228" s="T233">v-v:tense-v:pn</ta>
            <ta e="T235" id="Seg_4229" s="T234">v-v:tense-v:pn</ta>
            <ta e="T236" id="Seg_4230" s="T235">v-v:tense-v:pn</ta>
            <ta e="T237" id="Seg_4231" s="T236">adv</ta>
            <ta e="T238" id="Seg_4232" s="T237">v-v&gt;v-v:pn</ta>
            <ta e="T239" id="Seg_4233" s="T238">ptcl</ta>
            <ta e="T240" id="Seg_4234" s="T239">v-v:pn</ta>
            <ta e="T241" id="Seg_4235" s="T240">que-n:case</ta>
            <ta e="T243" id="Seg_4236" s="T242">que</ta>
            <ta e="T244" id="Seg_4237" s="T243">pers</ta>
            <ta e="T245" id="Seg_4238" s="T244">v-v:tense-v:pn</ta>
            <ta e="T246" id="Seg_4239" s="T245">ptcl</ta>
            <ta e="T247" id="Seg_4240" s="T246">pers</ta>
            <ta e="T248" id="Seg_4241" s="T247">aux-v:pn</ta>
            <ta e="T249" id="Seg_4242" s="T248">v-v:mood.pn</ta>
            <ta e="T250" id="Seg_4243" s="T249">pers</ta>
            <ta e="T251" id="Seg_4244" s="T250">adv</ta>
            <ta e="T252" id="Seg_4245" s="T251">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T253" id="Seg_4246" s="T252">adv</ta>
            <ta e="T254" id="Seg_4247" s="T253">num-n:case</ta>
            <ta e="T255" id="Seg_4248" s="T254">n</ta>
            <ta e="T256" id="Seg_4249" s="T255">v-v:tense-v:pn</ta>
            <ta e="T257" id="Seg_4250" s="T256">dempro-n:case</ta>
            <ta e="T258" id="Seg_4251" s="T257">v-v:tense-v:pn</ta>
            <ta e="T259" id="Seg_4252" s="T258">num-n:case</ta>
            <ta e="T260" id="Seg_4253" s="T259">%%</ta>
            <ta e="T261" id="Seg_4254" s="T260">n-n:case</ta>
            <ta e="T262" id="Seg_4255" s="T261">dempro-n:case</ta>
            <ta e="T263" id="Seg_4256" s="T262">adv</ta>
            <ta e="T264" id="Seg_4257" s="T263">v-v:tense-v:pn</ta>
            <ta e="T265" id="Seg_4258" s="T264">v-v:n.fin</ta>
            <ta e="T266" id="Seg_4259" s="T265">dempro-n:case</ta>
            <ta e="T267" id="Seg_4260" s="T266">adv</ta>
            <ta e="T268" id="Seg_4261" s="T267">v-v:tense-v:pn</ta>
            <ta e="T269" id="Seg_4262" s="T268">v-v&gt;v-v:pn</ta>
            <ta e="T270" id="Seg_4263" s="T269">ptcl</ta>
            <ta e="T271" id="Seg_4264" s="T270">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T272" id="Seg_4265" s="T271">ptcl</ta>
            <ta e="T273" id="Seg_4266" s="T272">v-v:mood.pn</ta>
            <ta e="T274" id="Seg_4267" s="T273">adv</ta>
            <ta e="T275" id="Seg_4268" s="T274">pers</ta>
            <ta e="T276" id="Seg_4269" s="T275">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T277" id="Seg_4270" s="T276">pers</ta>
            <ta e="T278" id="Seg_4271" s="T277">ptcl</ta>
            <ta e="T279" id="Seg_4272" s="T278">pers</ta>
            <ta e="T280" id="Seg_4273" s="T279">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T281" id="Seg_4274" s="T280">dempro-n:case</ta>
            <ta e="T282" id="Seg_4275" s="T281">v-v&gt;v-v:pn</ta>
            <ta e="T283" id="Seg_4276" s="T282">ptcl</ta>
            <ta e="T284" id="Seg_4277" s="T283">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T285" id="Seg_4278" s="T284">dempro-n:case</ta>
            <ta e="T286" id="Seg_4279" s="T285">v-v:tense-v:pn</ta>
            <ta e="T287" id="Seg_4280" s="T286">n-n:case</ta>
            <ta e="T288" id="Seg_4281" s="T287">dempro-n:case</ta>
            <ta e="T289" id="Seg_4282" s="T288">ptcl</ta>
            <ta e="T290" id="Seg_4283" s="T289">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T291" id="Seg_4284" s="T290">v-v:tense-v:pn</ta>
            <ta e="T292" id="Seg_4285" s="T291">v-v:tense-v:pn</ta>
            <ta e="T293" id="Seg_4286" s="T292">dempro-n:case</ta>
            <ta e="T294" id="Seg_4287" s="T293">que-n:case</ta>
            <ta e="T296" id="Seg_4288" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_4289" s="T296">ptcl</ta>
            <ta e="T298" id="Seg_4290" s="T297">adj-n:case</ta>
            <ta e="T299" id="Seg_4291" s="T298">conj</ta>
            <ta e="T300" id="Seg_4292" s="T299">adv</ta>
            <ta e="T301" id="Seg_4293" s="T300">ptcl</ta>
            <ta e="T302" id="Seg_4294" s="T301">pers</ta>
            <ta e="T303" id="Seg_4295" s="T302">conj</ta>
            <ta e="T304" id="Seg_4296" s="T303">pers</ta>
            <ta e="T305" id="Seg_4297" s="T304">adv</ta>
            <ta e="T306" id="Seg_4298" s="T305">v-v:tense-v:pn</ta>
            <ta e="T307" id="Seg_4299" s="T306">v-v:tense-v:pn</ta>
            <ta e="T308" id="Seg_4300" s="T307">v-v&gt;v-v:pn</ta>
            <ta e="T309" id="Seg_4301" s="T308">pers</ta>
            <ta e="T310" id="Seg_4302" s="T309">v-v:tense-v:pn</ta>
            <ta e="T312" id="Seg_4303" s="T311">n-n:case</ta>
            <ta e="T313" id="Seg_4304" s="T312">dempro-n:case</ta>
            <ta e="T314" id="Seg_4305" s="T313">dempro-n:case</ta>
            <ta e="T316" id="Seg_4306" s="T315">n.[n:case]</ta>
            <ta e="T317" id="Seg_4307" s="T316">v-v:tense-v:pn</ta>
            <ta e="T318" id="Seg_4308" s="T317">ptcl</ta>
            <ta e="T319" id="Seg_4309" s="T318">v-v:ins-v:mood.pn</ta>
            <ta e="T320" id="Seg_4310" s="T319">que</ta>
            <ta e="T321" id="Seg_4311" s="T320">dempro-n:case</ta>
            <ta e="T322" id="Seg_4312" s="T321">n.[n:case]</ta>
            <ta e="T323" id="Seg_4313" s="T322">adj-n:case</ta>
            <ta e="T324" id="Seg_4314" s="T323">v-v:tense-v:pn</ta>
            <ta e="T325" id="Seg_4315" s="T324">adv</ta>
            <ta e="T326" id="Seg_4316" s="T325">adv</ta>
            <ta e="T327" id="Seg_4317" s="T326">v-v:mood.pn</ta>
            <ta e="T328" id="Seg_4318" s="T327">ptcl</ta>
            <ta e="T329" id="Seg_4319" s="T328">pers</ta>
            <ta e="T330" id="Seg_4320" s="T329">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T331" id="Seg_4321" s="T330">dempro</ta>
            <ta e="T332" id="Seg_4322" s="T331">v-v:tense-v:pn</ta>
            <ta e="T333" id="Seg_4323" s="T332">n-n:case.poss</ta>
            <ta e="T334" id="Seg_4324" s="T333">ptcl</ta>
            <ta e="T335" id="Seg_4325" s="T334">dempro-n:case</ta>
            <ta e="T337" id="Seg_4326" s="T336">n-n:case</ta>
            <ta e="T338" id="Seg_4327" s="T337">que=ptcl</ta>
            <ta e="T339" id="Seg_4328" s="T338">v-v:tense-v:pn</ta>
            <ta e="T340" id="Seg_4329" s="T339">dempro</ta>
            <ta e="T341" id="Seg_4330" s="T340">ptcl</ta>
            <ta e="T342" id="Seg_4331" s="T341">v-v:tense-v:pn</ta>
            <ta e="T343" id="Seg_4332" s="T342">v-v:tense-v:pn</ta>
            <ta e="T344" id="Seg_4333" s="T343">v-v:tense-v:pn</ta>
            <ta e="T345" id="Seg_4334" s="T344">n-n:case.poss</ta>
            <ta e="T346" id="Seg_4335" s="T345">ptcl</ta>
            <ta e="T347" id="Seg_4336" s="T346">adj-n:case</ta>
            <ta e="T348" id="Seg_4337" s="T347">dempro-n:case</ta>
            <ta e="T349" id="Seg_4338" s="T348">ptcl</ta>
            <ta e="T350" id="Seg_4339" s="T349">que-n:case</ta>
            <ta e="T351" id="Seg_4340" s="T350">v-v:tense-v:pn</ta>
            <ta e="T352" id="Seg_4341" s="T351">conj</ta>
            <ta e="T353" id="Seg_4342" s="T352">v-v:tense-v:pn</ta>
            <ta e="T354" id="Seg_4343" s="T353">v-v:tense-v:pn</ta>
            <ta e="T355" id="Seg_4344" s="T354">adv</ta>
            <ta e="T356" id="Seg_4345" s="T355">v-v&gt;v-v:pn</ta>
            <ta e="T357" id="Seg_4346" s="T356">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T358" id="Seg_4347" s="T357">ptcl</ta>
            <ta e="T359" id="Seg_4348" s="T358">v-v:tense-v:pn</ta>
            <ta e="T360" id="Seg_4349" s="T359">pers</ta>
            <ta e="T361" id="Seg_4350" s="T360">pers</ta>
            <ta e="T362" id="Seg_4351" s="T361">v-v:tense-v:pn</ta>
            <ta e="T363" id="Seg_4352" s="T362">v-v:mood.pn</ta>
            <ta e="T364" id="Seg_4353" s="T363">v-v:tense-v:pn</ta>
            <ta e="T365" id="Seg_4354" s="T364">v-v:mood.pn</ta>
            <ta e="T366" id="Seg_4355" s="T365">dempro-n:case</ta>
            <ta e="T367" id="Seg_4356" s="T366">v-v:tense-v:pn</ta>
            <ta e="T368" id="Seg_4357" s="T367">conj</ta>
            <ta e="T369" id="Seg_4358" s="T368">adv</ta>
            <ta e="T370" id="Seg_4359" s="T369">adj-n:case</ta>
            <ta e="T371" id="Seg_4360" s="T370">n-n:case</ta>
            <ta e="T372" id="Seg_4361" s="T371">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T373" id="Seg_4362" s="T372">adv</ta>
            <ta e="T374" id="Seg_4363" s="T373">n-n:num</ta>
            <ta e="T375" id="Seg_4364" s="T374">ptcl</ta>
            <ta e="T376" id="Seg_4365" s="T375">v-v&gt;v-v:pn</ta>
            <ta e="T377" id="Seg_4366" s="T376">n-n:case</ta>
            <ta e="T378" id="Seg_4367" s="T377">v-v&gt;v-v:pn</ta>
            <ta e="T379" id="Seg_4368" s="T378">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T380" id="Seg_4369" s="T379">n-n:case</ta>
            <ta e="T381" id="Seg_4370" s="T380">conj</ta>
            <ta e="T382" id="Seg_4371" s="T381">dempro-n:case</ta>
            <ta e="T383" id="Seg_4372" s="T382">dempro-n:case</ta>
            <ta e="T384" id="Seg_4373" s="T383">n-n:case</ta>
            <ta e="T385" id="Seg_4374" s="T384">n-n:case</ta>
            <ta e="T386" id="Seg_4375" s="T385">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T387" id="Seg_4376" s="T386">adv</ta>
            <ta e="T388" id="Seg_4377" s="T387">dempro-n:case</ta>
            <ta e="T390" id="Seg_4378" s="T389">dempro-n:case</ta>
            <ta e="T391" id="Seg_4379" s="T390">n-n:case</ta>
            <ta e="T392" id="Seg_4380" s="T391">v-v:tense-v:pn</ta>
            <ta e="T393" id="Seg_4381" s="T392">conj</ta>
            <ta e="T395" id="Seg_4382" s="T394">v-v:n.fin</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_4383" s="T0">v</ta>
            <ta e="T2" id="Seg_4384" s="T1">n</ta>
            <ta e="T3" id="Seg_4385" s="T2">dempro</ta>
            <ta e="T6" id="Seg_4386" s="T5">dempro</ta>
            <ta e="T7" id="Seg_4387" s="T6">v</ta>
            <ta e="T8" id="Seg_4388" s="T7">num</ta>
            <ta e="T9" id="Seg_4389" s="T8">n</ta>
            <ta e="T10" id="Seg_4390" s="T9">dempro</ta>
            <ta e="T11" id="Seg_4391" s="T10">v</ta>
            <ta e="T13" id="Seg_4392" s="T12">v</ta>
            <ta e="T14" id="Seg_4393" s="T13">n</ta>
            <ta e="T15" id="Seg_4394" s="T14">v</ta>
            <ta e="T16" id="Seg_4395" s="T15">adj</ta>
            <ta e="T17" id="Seg_4396" s="T16">n</ta>
            <ta e="T18" id="Seg_4397" s="T17">v</ta>
            <ta e="T19" id="Seg_4398" s="T18">que</ta>
            <ta e="T20" id="Seg_4399" s="T19">pers</ta>
            <ta e="T21" id="Seg_4400" s="T20">v</ta>
            <ta e="T22" id="Seg_4401" s="T21">pers</ta>
            <ta e="T23" id="Seg_4402" s="T22">pers</ta>
            <ta e="T24" id="Seg_4403" s="T23">adj</ta>
            <ta e="T25" id="Seg_4404" s="T24">n</ta>
            <ta e="T26" id="Seg_4405" s="T25">v</ta>
            <ta e="T27" id="Seg_4406" s="T26">conj</ta>
            <ta e="T29" id="Seg_4407" s="T28">n</ta>
            <ta e="T30" id="Seg_4408" s="T29">pers</ta>
            <ta e="T31" id="Seg_4409" s="T30">pers</ta>
            <ta e="T32" id="Seg_4410" s="T31">n</ta>
            <ta e="T33" id="Seg_4411" s="T32">adj</ta>
            <ta e="T34" id="Seg_4412" s="T33">v</ta>
            <ta e="T35" id="Seg_4413" s="T34">conj</ta>
            <ta e="T36" id="Seg_4414" s="T35">num</ta>
            <ta e="T37" id="Seg_4415" s="T36">adv</ta>
            <ta e="T38" id="Seg_4416" s="T37">adj</ta>
            <ta e="T39" id="Seg_4417" s="T38">n</ta>
            <ta e="T40" id="Seg_4418" s="T39">pers</ta>
            <ta e="T41" id="Seg_4419" s="T40">que</ta>
            <ta e="T42" id="Seg_4420" s="T41">v</ta>
            <ta e="T43" id="Seg_4421" s="T42">pers</ta>
            <ta e="T44" id="Seg_4422" s="T43">pers</ta>
            <ta e="T45" id="Seg_4423" s="T44">v</ta>
            <ta e="T46" id="Seg_4424" s="T45">adj</ta>
            <ta e="T47" id="Seg_4425" s="T46">n</ta>
            <ta e="T48" id="Seg_4426" s="T47">dempro</ta>
            <ta e="T49" id="Seg_4427" s="T48">v</ta>
            <ta e="T50" id="Seg_4428" s="T49">v</ta>
            <ta e="T51" id="Seg_4429" s="T50">v</ta>
            <ta e="T52" id="Seg_4430" s="T51">v</ta>
            <ta e="T53" id="Seg_4431" s="T52">adv</ta>
            <ta e="T54" id="Seg_4432" s="T53">n</ta>
            <ta e="T55" id="Seg_4433" s="T54">num</ta>
            <ta e="T56" id="Seg_4434" s="T55">v</ta>
            <ta e="T57" id="Seg_4435" s="T56">n</ta>
            <ta e="T58" id="Seg_4436" s="T57">conj</ta>
            <ta e="T60" id="Seg_4437" s="T59">n</ta>
            <ta e="T61" id="Seg_4438" s="T60">v</ta>
            <ta e="T62" id="Seg_4439" s="T61">adv</ta>
            <ta e="T63" id="Seg_4440" s="T62">v</ta>
            <ta e="T64" id="Seg_4441" s="T63">conj</ta>
            <ta e="T65" id="Seg_4442" s="T64">v</ta>
            <ta e="T66" id="Seg_4443" s="T65">que</ta>
            <ta e="T68" id="Seg_4444" s="T67">v</ta>
            <ta e="T69" id="Seg_4445" s="T68">adj</ta>
            <ta e="T70" id="Seg_4446" s="T69">n</ta>
            <ta e="T71" id="Seg_4447" s="T70">v</ta>
            <ta e="T72" id="Seg_4448" s="T71">v</ta>
            <ta e="T73" id="Seg_4449" s="T72">ptcl</ta>
            <ta e="T74" id="Seg_4450" s="T73">n</ta>
            <ta e="T75" id="Seg_4451" s="T74">n</ta>
            <ta e="T76" id="Seg_4452" s="T75">adv</ta>
            <ta e="T77" id="Seg_4453" s="T76">v</ta>
            <ta e="T78" id="Seg_4454" s="T77">v</ta>
            <ta e="T79" id="Seg_4455" s="T78">n</ta>
            <ta e="T81" id="Seg_4456" s="T80">v</ta>
            <ta e="T82" id="Seg_4457" s="T81">adv</ta>
            <ta e="T83" id="Seg_4458" s="T82">v</ta>
            <ta e="T84" id="Seg_4459" s="T83">v</ta>
            <ta e="T86" id="Seg_4460" s="T85">n</ta>
            <ta e="T87" id="Seg_4461" s="T86">v</ta>
            <ta e="T88" id="Seg_4462" s="T87">n</ta>
            <ta e="T90" id="Seg_4463" s="T89">refl</ta>
            <ta e="T91" id="Seg_4464" s="T90">n</ta>
            <ta e="T92" id="Seg_4465" s="T91">v</ta>
            <ta e="T93" id="Seg_4466" s="T92">pers</ta>
            <ta e="T95" id="Seg_4467" s="T94">v</ta>
            <ta e="T96" id="Seg_4468" s="T95">adv</ta>
            <ta e="T97" id="Seg_4469" s="T96">dempro</ta>
            <ta e="T98" id="Seg_4470" s="T97">v</ta>
            <ta e="T99" id="Seg_4471" s="T98">v</ta>
            <ta e="T100" id="Seg_4472" s="T99">v</ta>
            <ta e="T101" id="Seg_4473" s="T100">adv</ta>
            <ta e="T102" id="Seg_4474" s="T101">n</ta>
            <ta e="T103" id="Seg_4475" s="T102">v</ta>
            <ta e="T104" id="Seg_4476" s="T103">n</ta>
            <ta e="T107" id="Seg_4477" s="T106">v</ta>
            <ta e="T108" id="Seg_4478" s="T107">conj</ta>
            <ta e="T109" id="Seg_4479" s="T108">v</ta>
            <ta e="T110" id="Seg_4480" s="T109">n</ta>
            <ta e="T111" id="Seg_4481" s="T110">v</ta>
            <ta e="T112" id="Seg_4482" s="T111">quant</ta>
            <ta e="T113" id="Seg_4483" s="T112">v</ta>
            <ta e="T114" id="Seg_4484" s="T113">num</ta>
            <ta e="T115" id="Seg_4485" s="T114">conj</ta>
            <ta e="T116" id="Seg_4486" s="T115">pers</ta>
            <ta e="T117" id="Seg_4487" s="T116">ptcl</ta>
            <ta e="T118" id="Seg_4488" s="T117">adv</ta>
            <ta e="T119" id="Seg_4489" s="T118">v</ta>
            <ta e="T120" id="Seg_4490" s="T119">dempro</ta>
            <ta e="T121" id="Seg_4491" s="T120">v</ta>
            <ta e="T122" id="Seg_4492" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_4493" s="T122">conj</ta>
            <ta e="T124" id="Seg_4494" s="T123">conj</ta>
            <ta e="T125" id="Seg_4495" s="T124">pers</ta>
            <ta e="T126" id="Seg_4496" s="T125">v</ta>
            <ta e="T127" id="Seg_4497" s="T126">adv</ta>
            <ta e="T128" id="Seg_4498" s="T127">quant</ta>
            <ta e="T129" id="Seg_4499" s="T128">n</ta>
            <ta e="T130" id="Seg_4500" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_4501" s="T130">v</ta>
            <ta e="T132" id="Seg_4502" s="T131">v</ta>
            <ta e="T133" id="Seg_4503" s="T132">v</ta>
            <ta e="T134" id="Seg_4504" s="T133">dempro</ta>
            <ta e="T135" id="Seg_4505" s="T134">adv</ta>
            <ta e="T136" id="Seg_4506" s="T135">n</ta>
            <ta e="T137" id="Seg_4507" s="T136">v</ta>
            <ta e="T138" id="Seg_4508" s="T137">dempro</ta>
            <ta e="T139" id="Seg_4509" s="T138">dempro</ta>
            <ta e="T142" id="Seg_4510" s="T141">v</ta>
            <ta e="T143" id="Seg_4511" s="T142">conj</ta>
            <ta e="T144" id="Seg_4512" s="T143">n</ta>
            <ta e="T145" id="Seg_4513" s="T144">v</ta>
            <ta e="T147" id="Seg_4514" s="T146">n</ta>
            <ta e="T148" id="Seg_4515" s="T147">v</ta>
            <ta e="T149" id="Seg_4516" s="T148">v</ta>
            <ta e="T150" id="Seg_4517" s="T149">n</ta>
            <ta e="T151" id="Seg_4518" s="T150">adv</ta>
            <ta e="T152" id="Seg_4519" s="T151">adj</ta>
            <ta e="T153" id="Seg_4520" s="T152">propr</ta>
            <ta e="T154" id="Seg_4521" s="T153">que</ta>
            <ta e="T155" id="Seg_4522" s="T154">v</ta>
            <ta e="T156" id="Seg_4523" s="T155">adv</ta>
            <ta e="T157" id="Seg_4524" s="T156">propr</ta>
            <ta e="T158" id="Seg_4525" s="T157">n</ta>
            <ta e="T159" id="Seg_4526" s="T158">v</ta>
            <ta e="T160" id="Seg_4527" s="T159">ptcl</ta>
            <ta e="T161" id="Seg_4528" s="T160">n</ta>
            <ta e="T162" id="Seg_4529" s="T161">que</ta>
            <ta e="T163" id="Seg_4530" s="T162">n</ta>
            <ta e="T164" id="Seg_4531" s="T163">adv</ta>
            <ta e="T165" id="Seg_4532" s="T164">adv</ta>
            <ta e="T166" id="Seg_4533" s="T165">n</ta>
            <ta e="T167" id="Seg_4534" s="T166">n</ta>
            <ta e="T169" id="Seg_4535" s="T168">adv</ta>
            <ta e="T170" id="Seg_4536" s="T169">v</ta>
            <ta e="T171" id="Seg_4537" s="T170">v</ta>
            <ta e="T173" id="Seg_4538" s="T172">pers</ta>
            <ta e="T176" id="Seg_4539" s="T175">v</ta>
            <ta e="T177" id="Seg_4540" s="T176">adv</ta>
            <ta e="T178" id="Seg_4541" s="T177">v</ta>
            <ta e="T182" id="Seg_4542" s="T181">adv</ta>
            <ta e="T183" id="Seg_4543" s="T182">v</ta>
            <ta e="T184" id="Seg_4544" s="T183">v</ta>
            <ta e="T185" id="Seg_4545" s="T184">n</ta>
            <ta e="T186" id="Seg_4546" s="T185">v</ta>
            <ta e="T187" id="Seg_4547" s="T186">n</ta>
            <ta e="T188" id="Seg_4548" s="T187">pers</ta>
            <ta e="T189" id="Seg_4549" s="T188">propr</ta>
            <ta e="T190" id="Seg_4550" s="T189">pers</ta>
            <ta e="T191" id="Seg_4551" s="T190">pers</ta>
            <ta e="T192" id="Seg_4552" s="T191">v</ta>
            <ta e="T193" id="Seg_4553" s="T192">conj</ta>
            <ta e="T194" id="Seg_4554" s="T193">pers</ta>
            <ta e="T195" id="Seg_4555" s="T194">v</ta>
            <ta e="T197" id="Seg_4556" s="T196">ptcl</ta>
            <ta e="T198" id="Seg_4557" s="T197">v</ta>
            <ta e="T200" id="Seg_4558" s="T199">que</ta>
            <ta e="T201" id="Seg_4559" s="T200">pers</ta>
            <ta e="T202" id="Seg_4560" s="T201">v</ta>
            <ta e="T203" id="Seg_4561" s="T202">adv</ta>
            <ta e="T204" id="Seg_4562" s="T203">ptcl</ta>
            <ta e="T205" id="Seg_4563" s="T204">pers</ta>
            <ta e="T206" id="Seg_4564" s="T205">ptcl</ta>
            <ta e="T207" id="Seg_4565" s="T206">v</ta>
            <ta e="T208" id="Seg_4566" s="T207">pers</ta>
            <ta e="T209" id="Seg_4567" s="T208">v</ta>
            <ta e="T210" id="Seg_4568" s="T209">adv</ta>
            <ta e="T396" id="Seg_4569" s="T210">v</ta>
            <ta e="T211" id="Seg_4570" s="T396">v</ta>
            <ta e="T212" id="Seg_4571" s="T211">dempro</ta>
            <ta e="T213" id="Seg_4572" s="T212">v</ta>
            <ta e="T214" id="Seg_4573" s="T213">v</ta>
            <ta e="T215" id="Seg_4574" s="T214">adv</ta>
            <ta e="T216" id="Seg_4575" s="T215">num</ta>
            <ta e="T217" id="Seg_4576" s="T216">n</ta>
            <ta e="T218" id="Seg_4577" s="T217">v</ta>
            <ta e="T219" id="Seg_4578" s="T218">dempro</ta>
            <ta e="T220" id="Seg_4579" s="T219">ptcl</ta>
            <ta e="T221" id="Seg_4580" s="T220">n</ta>
            <ta e="T222" id="Seg_4581" s="T221">v</ta>
            <ta e="T223" id="Seg_4582" s="T222">n</ta>
            <ta e="T224" id="Seg_4583" s="T223">adv</ta>
            <ta e="T225" id="Seg_4584" s="T224">conj</ta>
            <ta e="T226" id="Seg_4585" s="T225">conj</ta>
            <ta e="T227" id="Seg_4586" s="T226">adj</ta>
            <ta e="T228" id="Seg_4587" s="T227">propr</ta>
            <ta e="T229" id="Seg_4588" s="T228">n</ta>
            <ta e="T230" id="Seg_4589" s="T229">adv</ta>
            <ta e="T231" id="Seg_4590" s="T230">dempro</ta>
            <ta e="T232" id="Seg_4591" s="T231">adv</ta>
            <ta e="T233" id="Seg_4592" s="T232">n</ta>
            <ta e="T234" id="Seg_4593" s="T233">v</ta>
            <ta e="T235" id="Seg_4594" s="T234">v</ta>
            <ta e="T236" id="Seg_4595" s="T235">v</ta>
            <ta e="T237" id="Seg_4596" s="T236">adv</ta>
            <ta e="T238" id="Seg_4597" s="T237">v</ta>
            <ta e="T239" id="Seg_4598" s="T238">ptcl</ta>
            <ta e="T240" id="Seg_4599" s="T239">v</ta>
            <ta e="T241" id="Seg_4600" s="T240">que</ta>
            <ta e="T243" id="Seg_4601" s="T242">que</ta>
            <ta e="T244" id="Seg_4602" s="T243">pers</ta>
            <ta e="T245" id="Seg_4603" s="T244">v</ta>
            <ta e="T246" id="Seg_4604" s="T245">ptcl</ta>
            <ta e="T247" id="Seg_4605" s="T246">pers</ta>
            <ta e="T248" id="Seg_4606" s="T247">aux</ta>
            <ta e="T249" id="Seg_4607" s="T248">v</ta>
            <ta e="T250" id="Seg_4608" s="T249">pers</ta>
            <ta e="T251" id="Seg_4609" s="T250">adv</ta>
            <ta e="T252" id="Seg_4610" s="T251">v</ta>
            <ta e="T253" id="Seg_4611" s="T252">adv</ta>
            <ta e="T254" id="Seg_4612" s="T253">num</ta>
            <ta e="T255" id="Seg_4613" s="T254">n</ta>
            <ta e="T256" id="Seg_4614" s="T255">v</ta>
            <ta e="T257" id="Seg_4615" s="T256">dempro</ta>
            <ta e="T258" id="Seg_4616" s="T257">v</ta>
            <ta e="T259" id="Seg_4617" s="T258">num</ta>
            <ta e="T261" id="Seg_4618" s="T260">n</ta>
            <ta e="T262" id="Seg_4619" s="T261">dempro</ta>
            <ta e="T263" id="Seg_4620" s="T262">adv</ta>
            <ta e="T264" id="Seg_4621" s="T263">v</ta>
            <ta e="T265" id="Seg_4622" s="T264">v</ta>
            <ta e="T266" id="Seg_4623" s="T265">dempro</ta>
            <ta e="T267" id="Seg_4624" s="T266">adv</ta>
            <ta e="T268" id="Seg_4625" s="T267">v</ta>
            <ta e="T269" id="Seg_4626" s="T268">v</ta>
            <ta e="T270" id="Seg_4627" s="T269">ptcl</ta>
            <ta e="T271" id="Seg_4628" s="T270">v</ta>
            <ta e="T272" id="Seg_4629" s="T271">ptcl</ta>
            <ta e="T273" id="Seg_4630" s="T272">v</ta>
            <ta e="T274" id="Seg_4631" s="T273">adv</ta>
            <ta e="T275" id="Seg_4632" s="T274">pers</ta>
            <ta e="T276" id="Seg_4633" s="T275">v</ta>
            <ta e="T277" id="Seg_4634" s="T276">pers</ta>
            <ta e="T278" id="Seg_4635" s="T277">ptcl</ta>
            <ta e="T279" id="Seg_4636" s="T278">pers</ta>
            <ta e="T280" id="Seg_4637" s="T279">v</ta>
            <ta e="T281" id="Seg_4638" s="T280">dempro</ta>
            <ta e="T282" id="Seg_4639" s="T281">v</ta>
            <ta e="T283" id="Seg_4640" s="T282">ptcl</ta>
            <ta e="T284" id="Seg_4641" s="T283">v</ta>
            <ta e="T285" id="Seg_4642" s="T284">dempro</ta>
            <ta e="T286" id="Seg_4643" s="T285">v</ta>
            <ta e="T287" id="Seg_4644" s="T286">n</ta>
            <ta e="T288" id="Seg_4645" s="T287">dempro</ta>
            <ta e="T289" id="Seg_4646" s="T288">ptcl</ta>
            <ta e="T290" id="Seg_4647" s="T289">v</ta>
            <ta e="T291" id="Seg_4648" s="T290">v</ta>
            <ta e="T292" id="Seg_4649" s="T291">v</ta>
            <ta e="T293" id="Seg_4650" s="T292">dempro</ta>
            <ta e="T294" id="Seg_4651" s="T293">que</ta>
            <ta e="T296" id="Seg_4652" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_4653" s="T296">ptcl</ta>
            <ta e="T298" id="Seg_4654" s="T297">adj</ta>
            <ta e="T299" id="Seg_4655" s="T298">conj</ta>
            <ta e="T300" id="Seg_4656" s="T299">adv</ta>
            <ta e="T301" id="Seg_4657" s="T300">ptcl</ta>
            <ta e="T302" id="Seg_4658" s="T301">pers</ta>
            <ta e="T303" id="Seg_4659" s="T302">conj</ta>
            <ta e="T304" id="Seg_4660" s="T303">pers</ta>
            <ta e="T305" id="Seg_4661" s="T304">adv</ta>
            <ta e="T306" id="Seg_4662" s="T305">v</ta>
            <ta e="T307" id="Seg_4663" s="T306">v</ta>
            <ta e="T308" id="Seg_4664" s="T307">v</ta>
            <ta e="T309" id="Seg_4665" s="T308">pers</ta>
            <ta e="T310" id="Seg_4666" s="T309">v</ta>
            <ta e="T312" id="Seg_4667" s="T311">n</ta>
            <ta e="T313" id="Seg_4668" s="T312">dempro</ta>
            <ta e="T314" id="Seg_4669" s="T313">dempro</ta>
            <ta e="T316" id="Seg_4670" s="T315">n</ta>
            <ta e="T317" id="Seg_4671" s="T316">v</ta>
            <ta e="T318" id="Seg_4672" s="T317">ptcl</ta>
            <ta e="T319" id="Seg_4673" s="T318">v</ta>
            <ta e="T320" id="Seg_4674" s="T319">que</ta>
            <ta e="T321" id="Seg_4675" s="T320">dempro</ta>
            <ta e="T322" id="Seg_4676" s="T321">n</ta>
            <ta e="T323" id="Seg_4677" s="T322">adj</ta>
            <ta e="T324" id="Seg_4678" s="T323">v</ta>
            <ta e="T325" id="Seg_4679" s="T324">adv</ta>
            <ta e="T326" id="Seg_4680" s="T325">adv</ta>
            <ta e="T327" id="Seg_4681" s="T326">v</ta>
            <ta e="T328" id="Seg_4682" s="T327">ptcl</ta>
            <ta e="T329" id="Seg_4683" s="T328">pers</ta>
            <ta e="T330" id="Seg_4684" s="T329">v</ta>
            <ta e="T331" id="Seg_4685" s="T330">dempro</ta>
            <ta e="T332" id="Seg_4686" s="T331">v</ta>
            <ta e="T333" id="Seg_4687" s="T332">n</ta>
            <ta e="T334" id="Seg_4688" s="T333">ptcl</ta>
            <ta e="T335" id="Seg_4689" s="T334">dempro</ta>
            <ta e="T337" id="Seg_4690" s="T336">n</ta>
            <ta e="T338" id="Seg_4691" s="T337">que</ta>
            <ta e="T339" id="Seg_4692" s="T338">v</ta>
            <ta e="T340" id="Seg_4693" s="T339">dempro</ta>
            <ta e="T341" id="Seg_4694" s="T340">ptcl</ta>
            <ta e="T342" id="Seg_4695" s="T341">v</ta>
            <ta e="T343" id="Seg_4696" s="T342">v</ta>
            <ta e="T344" id="Seg_4697" s="T343">v</ta>
            <ta e="T345" id="Seg_4698" s="T344">n</ta>
            <ta e="T346" id="Seg_4699" s="T345">ptcl</ta>
            <ta e="T347" id="Seg_4700" s="T346">adj</ta>
            <ta e="T348" id="Seg_4701" s="T347">dempro</ta>
            <ta e="T349" id="Seg_4702" s="T348">ptcl</ta>
            <ta e="T350" id="Seg_4703" s="T349">que</ta>
            <ta e="T351" id="Seg_4704" s="T350">v</ta>
            <ta e="T352" id="Seg_4705" s="T351">conj</ta>
            <ta e="T353" id="Seg_4706" s="T352">v</ta>
            <ta e="T354" id="Seg_4707" s="T353">v</ta>
            <ta e="T355" id="Seg_4708" s="T354">adv</ta>
            <ta e="T356" id="Seg_4709" s="T355">v</ta>
            <ta e="T357" id="Seg_4710" s="T356">v</ta>
            <ta e="T358" id="Seg_4711" s="T357">ptcl</ta>
            <ta e="T359" id="Seg_4712" s="T358">v</ta>
            <ta e="T360" id="Seg_4713" s="T359">pers</ta>
            <ta e="T361" id="Seg_4714" s="T360">pers</ta>
            <ta e="T362" id="Seg_4715" s="T361">v</ta>
            <ta e="T363" id="Seg_4716" s="T362">v</ta>
            <ta e="T364" id="Seg_4717" s="T363">v</ta>
            <ta e="T365" id="Seg_4718" s="T364">v</ta>
            <ta e="T366" id="Seg_4719" s="T365">dempro</ta>
            <ta e="T367" id="Seg_4720" s="T366">v</ta>
            <ta e="T368" id="Seg_4721" s="T367">conj</ta>
            <ta e="T369" id="Seg_4722" s="T368">adv</ta>
            <ta e="T370" id="Seg_4723" s="T369">adj</ta>
            <ta e="T371" id="Seg_4724" s="T370">n</ta>
            <ta e="T372" id="Seg_4725" s="T371">v</ta>
            <ta e="T373" id="Seg_4726" s="T372">adv</ta>
            <ta e="T374" id="Seg_4727" s="T373">n</ta>
            <ta e="T375" id="Seg_4728" s="T374">ptcl</ta>
            <ta e="T376" id="Seg_4729" s="T375">v</ta>
            <ta e="T377" id="Seg_4730" s="T376">n</ta>
            <ta e="T378" id="Seg_4731" s="T377">v</ta>
            <ta e="T379" id="Seg_4732" s="T378">v</ta>
            <ta e="T380" id="Seg_4733" s="T379">n</ta>
            <ta e="T381" id="Seg_4734" s="T380">conj</ta>
            <ta e="T382" id="Seg_4735" s="T381">dempro</ta>
            <ta e="T383" id="Seg_4736" s="T382">dempro</ta>
            <ta e="T384" id="Seg_4737" s="T383">n</ta>
            <ta e="T385" id="Seg_4738" s="T384">n</ta>
            <ta e="T386" id="Seg_4739" s="T385">v</ta>
            <ta e="T387" id="Seg_4740" s="T386">adv</ta>
            <ta e="T388" id="Seg_4741" s="T387">dempro</ta>
            <ta e="T390" id="Seg_4742" s="T389">dempro</ta>
            <ta e="T391" id="Seg_4743" s="T390">n</ta>
            <ta e="T392" id="Seg_4744" s="T391">v</ta>
            <ta e="T393" id="Seg_4745" s="T392">conj</ta>
            <ta e="T395" id="Seg_4746" s="T394">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_4747" s="T1">np.h:E</ta>
            <ta e="T6" id="Seg_4748" s="T5">pro.h:Poss</ta>
            <ta e="T9" id="Seg_4749" s="T8">np.h:Th</ta>
            <ta e="T10" id="Seg_4750" s="T9">pro.h:A</ta>
            <ta e="T14" id="Seg_4751" s="T13">np:G</ta>
            <ta e="T17" id="Seg_4752" s="T16">np.h:R</ta>
            <ta e="T18" id="Seg_4753" s="T17">0.3.h:A</ta>
            <ta e="T19" id="Seg_4754" s="T18">pro:Th</ta>
            <ta e="T20" id="Seg_4755" s="T19">pro.h:B</ta>
            <ta e="T21" id="Seg_4756" s="T20">pro.h:A</ta>
            <ta e="T22" id="Seg_4757" s="T21">pro.h:A</ta>
            <ta e="T23" id="Seg_4758" s="T22">pro.h:B</ta>
            <ta e="T25" id="Seg_4759" s="T24">np:Th</ta>
            <ta e="T29" id="Seg_4760" s="T28">np.h:A</ta>
            <ta e="T30" id="Seg_4761" s="T29">pro.h:A</ta>
            <ta e="T31" id="Seg_4762" s="T30">pro.h:B</ta>
            <ta e="T32" id="Seg_4763" s="T31">np:Th</ta>
            <ta e="T39" id="Seg_4764" s="T38">np.h:R</ta>
            <ta e="T40" id="Seg_4765" s="T39">pro.h:B</ta>
            <ta e="T41" id="Seg_4766" s="T40">pro:Th</ta>
            <ta e="T43" id="Seg_4767" s="T42">pro.h:A</ta>
            <ta e="T44" id="Seg_4768" s="T43">pro.h:B</ta>
            <ta e="T47" id="Seg_4769" s="T46">np:Th</ta>
            <ta e="T48" id="Seg_4770" s="T47">pro.h:A</ta>
            <ta e="T50" id="Seg_4771" s="T49">0.3.h:A</ta>
            <ta e="T51" id="Seg_4772" s="T50">0.3.h:A</ta>
            <ta e="T52" id="Seg_4773" s="T51">0.3.h:A</ta>
            <ta e="T53" id="Seg_4774" s="T52">adv:L</ta>
            <ta e="T54" id="Seg_4775" s="T53">np:Th</ta>
            <ta e="T55" id="Seg_4776" s="T54">np.h:B</ta>
            <ta e="T56" id="Seg_4777" s="T55">0.3.h:A</ta>
            <ta e="T57" id="Seg_4778" s="T56">np:Th</ta>
            <ta e="T60" id="Seg_4779" s="T59">np:Th</ta>
            <ta e="T62" id="Seg_4780" s="T61">adv:Time</ta>
            <ta e="T63" id="Seg_4781" s="T62">0.3.h:A</ta>
            <ta e="T65" id="Seg_4782" s="T64">0.3.h:A</ta>
            <ta e="T70" id="Seg_4783" s="T69">np:Th</ta>
            <ta e="T71" id="Seg_4784" s="T70">0.3.h:A</ta>
            <ta e="T72" id="Seg_4785" s="T71">0.3.h:E</ta>
            <ta e="T75" id="Seg_4786" s="T74">np:Th</ta>
            <ta e="T76" id="Seg_4787" s="T75">adv:L</ta>
            <ta e="T77" id="Seg_4788" s="T76">0.3.h:A</ta>
            <ta e="T78" id="Seg_4789" s="T77">0.3.h:A</ta>
            <ta e="T79" id="Seg_4790" s="T78">np:Th</ta>
            <ta e="T82" id="Seg_4791" s="T81">adv:Time</ta>
            <ta e="T83" id="Seg_4792" s="T82">0.3.h:A</ta>
            <ta e="T84" id="Seg_4793" s="T83">0.3.h:A</ta>
            <ta e="T86" id="Seg_4794" s="T85">n:Time</ta>
            <ta e="T88" id="Seg_4795" s="T87">np.h:A</ta>
            <ta e="T91" id="Seg_4796" s="T90">np.h:Th</ta>
            <ta e="T92" id="Seg_4797" s="T91">0.2.h:A</ta>
            <ta e="T93" id="Seg_4798" s="T92">pro.h:R</ta>
            <ta e="T95" id="Seg_4799" s="T94">0.2.h:A</ta>
            <ta e="T96" id="Seg_4800" s="T95">adv:L</ta>
            <ta e="T97" id="Seg_4801" s="T96">pro.h:E</ta>
            <ta e="T99" id="Seg_4802" s="T98">0.3.h:E</ta>
            <ta e="T100" id="Seg_4803" s="T99">0.1.h:A</ta>
            <ta e="T101" id="Seg_4804" s="T100">adv:Time</ta>
            <ta e="T102" id="Seg_4805" s="T101">n:Time</ta>
            <ta e="T103" id="Seg_4806" s="T102">0.3.h:A</ta>
            <ta e="T104" id="Seg_4807" s="T103">np:Th</ta>
            <ta e="T109" id="Seg_4808" s="T108">0.3.h:A</ta>
            <ta e="T110" id="Seg_4809" s="T109">np:G</ta>
            <ta e="T111" id="Seg_4810" s="T110">0.3.h:A</ta>
            <ta e="T112" id="Seg_4811" s="T111">np:Th</ta>
            <ta e="T113" id="Seg_4812" s="T112">0.3.h:A</ta>
            <ta e="T116" id="Seg_4813" s="T115">pro.h:A</ta>
            <ta e="T118" id="Seg_4814" s="T117">adv:L</ta>
            <ta e="T119" id="Seg_4815" s="T118">pro.h:A</ta>
            <ta e="T125" id="Seg_4816" s="T124">pro.h:A</ta>
            <ta e="T127" id="Seg_4817" s="T126">adv:L</ta>
            <ta e="T129" id="Seg_4818" s="T128">np:Th</ta>
            <ta e="T131" id="Seg_4819" s="T130">0.2.h:A</ta>
            <ta e="T132" id="Seg_4820" s="T131">0.3.h:A</ta>
            <ta e="T133" id="Seg_4821" s="T132">0.3.h:A</ta>
            <ta e="T134" id="Seg_4822" s="T133">pro.h:Th</ta>
            <ta e="T135" id="Seg_4823" s="T134">adv:L</ta>
            <ta e="T136" id="Seg_4824" s="T135">np:Th</ta>
            <ta e="T142" id="Seg_4825" s="T141">0.3.h:A</ta>
            <ta e="T144" id="Seg_4826" s="T143">np:G</ta>
            <ta e="T145" id="Seg_4827" s="T144">0.3.h:A</ta>
            <ta e="T147" id="Seg_4828" s="T146">np:Th</ta>
            <ta e="T149" id="Seg_4829" s="T148">0.3:Th</ta>
            <ta e="T150" id="Seg_4830" s="T149">np:Th</ta>
            <ta e="T153" id="Seg_4831" s="T152">np.h:Poss</ta>
            <ta e="T155" id="Seg_4832" s="T154">0.3.h:A</ta>
            <ta e="T158" id="Seg_4833" s="T157">np:G</ta>
            <ta e="T159" id="Seg_4834" s="T158">0.3.h:A</ta>
            <ta e="T163" id="Seg_4835" s="T162">np:Th</ta>
            <ta e="T165" id="Seg_4836" s="T164">adv:L</ta>
            <ta e="T166" id="Seg_4837" s="T165">np.h:Poss</ta>
            <ta e="T167" id="Seg_4838" s="T166">np:P</ta>
            <ta e="T169" id="Seg_4839" s="T168">adv:Time</ta>
            <ta e="T170" id="Seg_4840" s="T169">0.3.h:A</ta>
            <ta e="T182" id="Seg_4841" s="T181">adv:Time</ta>
            <ta e="T183" id="Seg_4842" s="T182">0.3.h:A</ta>
            <ta e="T185" id="Seg_4843" s="T184">n:Time</ta>
            <ta e="T187" id="Seg_4844" s="T186">np.h:A</ta>
            <ta e="T190" id="Seg_4845" s="T189">pro.h:A</ta>
            <ta e="T191" id="Seg_4846" s="T190">pro.h:B</ta>
            <ta e="T194" id="Seg_4847" s="T193">pro.h:E</ta>
            <ta e="T200" id="Seg_4848" s="T199">pro.h:Th</ta>
            <ta e="T201" id="Seg_4849" s="T200">pro.h:Th</ta>
            <ta e="T202" id="Seg_4850" s="T201">0.2.h:A</ta>
            <ta e="T203" id="Seg_4851" s="T202">adv:L</ta>
            <ta e="T205" id="Seg_4852" s="T204">pro.h:A</ta>
            <ta e="T208" id="Seg_4853" s="T207">pro.h:E</ta>
            <ta e="T210" id="Seg_4854" s="T209">adv:Time</ta>
            <ta e="T211" id="Seg_4855" s="T396">0.3.h:A</ta>
            <ta e="T212" id="Seg_4856" s="T211">pro.h:E</ta>
            <ta e="T214" id="Seg_4857" s="T213">0.3.h:E</ta>
            <ta e="T215" id="Seg_4858" s="T214">adv:Time</ta>
            <ta e="T217" id="Seg_4859" s="T216">np:Th</ta>
            <ta e="T219" id="Seg_4860" s="T218">pro.h:A</ta>
            <ta e="T221" id="Seg_4861" s="T220">np:G</ta>
            <ta e="T223" id="Seg_4862" s="T222">np:Th</ta>
            <ta e="T224" id="Seg_4863" s="T223">adv:L</ta>
            <ta e="T226" id="Seg_4864" s="T225">adv:Time</ta>
            <ta e="T228" id="Seg_4865" s="T227">np.h:Poss</ta>
            <ta e="T229" id="Seg_4866" s="T228">np:Th</ta>
            <ta e="T230" id="Seg_4867" s="T229">adv:Time</ta>
            <ta e="T231" id="Seg_4868" s="T230">pro.h:A</ta>
            <ta e="T233" id="Seg_4869" s="T232">n:Time</ta>
            <ta e="T235" id="Seg_4870" s="T234">0.3.h:E</ta>
            <ta e="T236" id="Seg_4871" s="T235">0.3.h:E</ta>
            <ta e="T237" id="Seg_4872" s="T236">adv:Time</ta>
            <ta e="T238" id="Seg_4873" s="T237">0.3.h:A</ta>
            <ta e="T240" id="Seg_4874" s="T239">0.1.h:E</ta>
            <ta e="T241" id="Seg_4875" s="T240">pro.h:Th</ta>
            <ta e="T243" id="Seg_4876" s="T242">pro.h:Th</ta>
            <ta e="T244" id="Seg_4877" s="T243">pro.h:Th</ta>
            <ta e="T247" id="Seg_4878" s="T246">pro.h:A</ta>
            <ta e="T250" id="Seg_4879" s="T249">pro.h:E</ta>
            <ta e="T253" id="Seg_4880" s="T252">adv:Time</ta>
            <ta e="T255" id="Seg_4881" s="T254">np:Th</ta>
            <ta e="T261" id="Seg_4882" s="T260">np:Th</ta>
            <ta e="T262" id="Seg_4883" s="T261">pro.h:A</ta>
            <ta e="T266" id="Seg_4884" s="T265">pro.h:A</ta>
            <ta e="T269" id="Seg_4885" s="T268">0.3.h:A</ta>
            <ta e="T271" id="Seg_4886" s="T270">0.2.h:E</ta>
            <ta e="T273" id="Seg_4887" s="T272">0.2.h:A</ta>
            <ta e="T274" id="Seg_4888" s="T273">adv:L</ta>
            <ta e="T275" id="Seg_4889" s="T274">pro.h:A</ta>
            <ta e="T277" id="Seg_4890" s="T276">pro.h:Th</ta>
            <ta e="T279" id="Seg_4891" s="T278">pro.h:E</ta>
            <ta e="T281" id="Seg_4892" s="T280">pro.h:A</ta>
            <ta e="T284" id="Seg_4893" s="T283">0.1.h:E</ta>
            <ta e="T285" id="Seg_4894" s="T284">pro.h:A</ta>
            <ta e="T287" id="Seg_4895" s="T286">np:G</ta>
            <ta e="T288" id="Seg_4896" s="T287">pro.h:E</ta>
            <ta e="T291" id="Seg_4897" s="T290">pro.h:A</ta>
            <ta e="T292" id="Seg_4898" s="T291">0.1.h:E</ta>
            <ta e="T293" id="Seg_4899" s="T292">pro.h:Com</ta>
            <ta e="T300" id="Seg_4900" s="T299">adv:L</ta>
            <ta e="T305" id="Seg_4901" s="T304">adv:Time</ta>
            <ta e="T306" id="Seg_4902" s="T305">0.3.h:E</ta>
            <ta e="T307" id="Seg_4903" s="T306">0.3.h:E</ta>
            <ta e="T308" id="Seg_4904" s="T307">0.3.h:A</ta>
            <ta e="T309" id="Seg_4905" s="T308">pro.h:A</ta>
            <ta e="T312" id="Seg_4906" s="T311">np:G</ta>
            <ta e="T313" id="Seg_4907" s="T312">pro.h:A</ta>
            <ta e="T314" id="Seg_4908" s="T313">pro.h:R</ta>
            <ta e="T316" id="Seg_4909" s="T315">np:Th</ta>
            <ta e="T319" id="Seg_4910" s="T318">0.2.h:A</ta>
            <ta e="T322" id="Seg_4911" s="T321">np:P</ta>
            <ta e="T325" id="Seg_4912" s="T324">adv:Time</ta>
            <ta e="T326" id="Seg_4913" s="T325">adv:Time</ta>
            <ta e="T327" id="Seg_4914" s="T326">0.2.h:A</ta>
            <ta e="T329" id="Seg_4915" s="T328">pro.h:P</ta>
            <ta e="T332" id="Seg_4916" s="T331">0.3.h:A</ta>
            <ta e="T333" id="Seg_4917" s="T332">np:G</ta>
            <ta e="T335" id="Seg_4918" s="T334">pro.h:R</ta>
            <ta e="T337" id="Seg_4919" s="T336">np:Th</ta>
            <ta e="T339" id="Seg_4920" s="T338">0.3.h:A</ta>
            <ta e="T340" id="Seg_4921" s="T339">pro.h:E</ta>
            <ta e="T343" id="Seg_4922" s="T342">0.3.h:E</ta>
            <ta e="T344" id="Seg_4923" s="T343">0.3.h:A</ta>
            <ta e="T345" id="Seg_4924" s="T344">np:Th</ta>
            <ta e="T351" id="Seg_4925" s="T350">0.3.h:A</ta>
            <ta e="T353" id="Seg_4926" s="T352">0.3.h:A</ta>
            <ta e="T354" id="Seg_4927" s="T353">0.3.h:A</ta>
            <ta e="T355" id="Seg_4928" s="T354">adv:L</ta>
            <ta e="T356" id="Seg_4929" s="T355">0.3.h:E</ta>
            <ta e="T357" id="Seg_4930" s="T356">0.3.h:P</ta>
            <ta e="T359" id="Seg_4931" s="T358">0.3.h:E</ta>
            <ta e="T360" id="Seg_4932" s="T359">pro.h:E</ta>
            <ta e="T361" id="Seg_4933" s="T360">pro.h:B</ta>
            <ta e="T363" id="Seg_4934" s="T362">0.2.h:A</ta>
            <ta e="T364" id="Seg_4935" s="T363">0.1.h:E</ta>
            <ta e="T365" id="Seg_4936" s="T364">0.2.h:A</ta>
            <ta e="T366" id="Seg_4937" s="T365">pro.h:A</ta>
            <ta e="T371" id="Seg_4938" s="T370">np.h:Th</ta>
            <ta e="T372" id="Seg_4939" s="T371">0.3.h:P</ta>
            <ta e="T373" id="Seg_4940" s="T372">adv:Time</ta>
            <ta e="T374" id="Seg_4941" s="T373">np:Th</ta>
            <ta e="T377" id="Seg_4942" s="T376">np.h:A</ta>
            <ta e="T380" id="Seg_4943" s="T379">np:P</ta>
            <ta e="T384" id="Seg_4944" s="T383">np.h:P</ta>
            <ta e="T385" id="Seg_4945" s="T384">np.h:Th</ta>
            <ta e="T387" id="Seg_4946" s="T386">adv:Time</ta>
            <ta e="T388" id="Seg_4947" s="T387">pro.h:A</ta>
            <ta e="T390" id="Seg_4948" s="T389">pro.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_4949" s="T0">v:pred</ta>
            <ta e="T2" id="Seg_4950" s="T1">np.h:S</ta>
            <ta e="T7" id="Seg_4951" s="T6">v:pred</ta>
            <ta e="T9" id="Seg_4952" s="T8">np.h:S</ta>
            <ta e="T10" id="Seg_4953" s="T9">pro.h:S</ta>
            <ta e="T11" id="Seg_4954" s="T10">v:pred</ta>
            <ta e="T15" id="Seg_4955" s="T14">s:purp</ta>
            <ta e="T18" id="Seg_4956" s="T17">v:pred 0.3.h:S</ta>
            <ta e="T21" id="Seg_4957" s="T20">v:pred</ta>
            <ta e="T22" id="Seg_4958" s="T21">pro.h:S</ta>
            <ta e="T25" id="Seg_4959" s="T24">np:O</ta>
            <ta e="T26" id="Seg_4960" s="T25">v:pred</ta>
            <ta e="T29" id="Seg_4961" s="T28">np.h:S</ta>
            <ta e="T30" id="Seg_4962" s="T29">pro.h:S</ta>
            <ta e="T32" id="Seg_4963" s="T31">np:O</ta>
            <ta e="T34" id="Seg_4964" s="T33">v:pred</ta>
            <ta e="T42" id="Seg_4965" s="T41">v:pred</ta>
            <ta e="T43" id="Seg_4966" s="T42">pro.h:S</ta>
            <ta e="T45" id="Seg_4967" s="T44">v:pred</ta>
            <ta e="T47" id="Seg_4968" s="T46">np:O</ta>
            <ta e="T48" id="Seg_4969" s="T47">pro.h:S</ta>
            <ta e="T49" id="Seg_4970" s="T48">v:pred</ta>
            <ta e="T50" id="Seg_4971" s="T49">v:pred 0.3.h:S</ta>
            <ta e="T51" id="Seg_4972" s="T50">v:pred 0.3.h:S</ta>
            <ta e="T52" id="Seg_4973" s="T51">v:pred 0.3.h:S</ta>
            <ta e="T54" id="Seg_4974" s="T53">np:O</ta>
            <ta e="T56" id="Seg_4975" s="T55">v:pred 0.3.h:S</ta>
            <ta e="T57" id="Seg_4976" s="T56">np:O</ta>
            <ta e="T60" id="Seg_4977" s="T59">np:S</ta>
            <ta e="T61" id="Seg_4978" s="T60">v:pred</ta>
            <ta e="T63" id="Seg_4979" s="T62">v:pred 0.3.h:S</ta>
            <ta e="T65" id="Seg_4980" s="T64">v:pred 0.3.h:S</ta>
            <ta e="T68" id="Seg_4981" s="T67">v:pred</ta>
            <ta e="T70" id="Seg_4982" s="T69">np:O</ta>
            <ta e="T71" id="Seg_4983" s="T70">v:pred 0.3.h:S</ta>
            <ta e="T72" id="Seg_4984" s="T71">v:pred 0.3.h:S</ta>
            <ta e="T75" id="Seg_4985" s="T74">np:O</ta>
            <ta e="T77" id="Seg_4986" s="T76">v:pred 0.3.h:S</ta>
            <ta e="T78" id="Seg_4987" s="T77">v:pred 0.3.h:S</ta>
            <ta e="T79" id="Seg_4988" s="T78">np:S</ta>
            <ta e="T81" id="Seg_4989" s="T80">v:pred</ta>
            <ta e="T83" id="Seg_4990" s="T82">v:pred 0.3.h:S</ta>
            <ta e="T84" id="Seg_4991" s="T83">v:pred 0.3.h:S</ta>
            <ta e="T87" id="Seg_4992" s="T86">v:pred</ta>
            <ta e="T88" id="Seg_4993" s="T87">np.h:S</ta>
            <ta e="T91" id="Seg_4994" s="T90">np.h:O</ta>
            <ta e="T92" id="Seg_4995" s="T91">v:pred 0.2.h:S</ta>
            <ta e="T95" id="Seg_4996" s="T94">v:pred 0.2.h:S</ta>
            <ta e="T97" id="Seg_4997" s="T96">pro.h:S</ta>
            <ta e="T98" id="Seg_4998" s="T97">v:pred</ta>
            <ta e="T99" id="Seg_4999" s="T98">v:pred 0.3.h:S</ta>
            <ta e="T100" id="Seg_5000" s="T99">v:pred 0.1.h:S</ta>
            <ta e="T103" id="Seg_5001" s="T102">v:pred 0.3.h:S</ta>
            <ta e="T104" id="Seg_5002" s="T103">np:S</ta>
            <ta e="T107" id="Seg_5003" s="T106">v:pred</ta>
            <ta e="T109" id="Seg_5004" s="T108">v:pred 0.3.h:S</ta>
            <ta e="T111" id="Seg_5005" s="T110">v:pred 0.3.h:S</ta>
            <ta e="T112" id="Seg_5006" s="T111">np:O</ta>
            <ta e="T113" id="Seg_5007" s="T112">v:pred 0.3.h:S</ta>
            <ta e="T117" id="Seg_5008" s="T116">ptcl:pred</ta>
            <ta e="T120" id="Seg_5009" s="T119">pro.h:S</ta>
            <ta e="T121" id="Seg_5010" s="T120">v:pred</ta>
            <ta e="T125" id="Seg_5011" s="T124">pro.h:S</ta>
            <ta e="T126" id="Seg_5012" s="T125">v:pred</ta>
            <ta e="T129" id="Seg_5013" s="T128">np:O</ta>
            <ta e="T131" id="Seg_5014" s="T130">v:pred 0.2.h:S</ta>
            <ta e="T132" id="Seg_5015" s="T131">v:pred 0.3.h:S</ta>
            <ta e="T133" id="Seg_5016" s="T132">v:pred 0.3.h:S</ta>
            <ta e="T134" id="Seg_5017" s="T133">pro.h:O</ta>
            <ta e="T136" id="Seg_5018" s="T135">np:S</ta>
            <ta e="T137" id="Seg_5019" s="T136">v:pred</ta>
            <ta e="T142" id="Seg_5020" s="T141">v:pred 0.3.h:S</ta>
            <ta e="T145" id="Seg_5021" s="T144">v:pred 0.3.h:S</ta>
            <ta e="T147" id="Seg_5022" s="T146">np:S</ta>
            <ta e="T148" id="Seg_5023" s="T147">v:pred</ta>
            <ta e="T149" id="Seg_5024" s="T148">v:pred 0.3:S</ta>
            <ta e="T150" id="Seg_5025" s="T149">np:S</ta>
            <ta e="T152" id="Seg_5026" s="T151">v:pred</ta>
            <ta e="T155" id="Seg_5027" s="T154">v:pred 0.3.h:S</ta>
            <ta e="T159" id="Seg_5028" s="T158">v:pred 0.3.h:S</ta>
            <ta e="T163" id="Seg_5029" s="T162">np:S</ta>
            <ta e="T170" id="Seg_5030" s="T169">v:pred 0.3.h:S</ta>
            <ta e="T176" id="Seg_5031" s="T175">v:pred</ta>
            <ta e="T183" id="Seg_5032" s="T182">v:pred 0.3.h:S</ta>
            <ta e="T184" id="Seg_5033" s="T183">s:purp</ta>
            <ta e="T186" id="Seg_5034" s="T185">v:pred</ta>
            <ta e="T187" id="Seg_5035" s="T186">np.h:S</ta>
            <ta e="T190" id="Seg_5036" s="T189">pro.h:S</ta>
            <ta e="T191" id="Seg_5037" s="T190">pro.h:O</ta>
            <ta e="T192" id="Seg_5038" s="T191">v:pred</ta>
            <ta e="T194" id="Seg_5039" s="T193">pro.h:S</ta>
            <ta e="T197" id="Seg_5040" s="T196">ptcl.neg</ta>
            <ta e="T198" id="Seg_5041" s="T197">v:pred</ta>
            <ta e="T200" id="Seg_5042" s="T199">pro.h:S</ta>
            <ta e="T201" id="Seg_5043" s="T200">n:pred</ta>
            <ta e="T202" id="Seg_5044" s="T201">v:pred 0.2.h:S</ta>
            <ta e="T204" id="Seg_5045" s="T203">ptcl.neg</ta>
            <ta e="T205" id="Seg_5046" s="T204">pro.h:S</ta>
            <ta e="T206" id="Seg_5047" s="T205">ptcl.neg</ta>
            <ta e="T207" id="Seg_5048" s="T206">v:pred</ta>
            <ta e="T208" id="Seg_5049" s="T207">pro.h:S</ta>
            <ta e="T209" id="Seg_5050" s="T208">v:pred</ta>
            <ta e="T396" id="Seg_5051" s="T210">conv:pred</ta>
            <ta e="T211" id="Seg_5052" s="T396">v:pred 0.3.h:S</ta>
            <ta e="T212" id="Seg_5053" s="T211">pro.h:S</ta>
            <ta e="T213" id="Seg_5054" s="T212">v:pred</ta>
            <ta e="T214" id="Seg_5055" s="T213">v:pred 0.3.h:S</ta>
            <ta e="T217" id="Seg_5056" s="T216">np:S</ta>
            <ta e="T218" id="Seg_5057" s="T217">v:pred</ta>
            <ta e="T219" id="Seg_5058" s="T218">pro.h:S</ta>
            <ta e="T222" id="Seg_5059" s="T221">v:pred</ta>
            <ta e="T223" id="Seg_5060" s="T222">np:S</ta>
            <ta e="T227" id="Seg_5061" s="T226">v:pred</ta>
            <ta e="T231" id="Seg_5062" s="T230">pro.h:S</ta>
            <ta e="T234" id="Seg_5063" s="T233">v:pred</ta>
            <ta e="T235" id="Seg_5064" s="T234">v:pred 0.3.h:S</ta>
            <ta e="T236" id="Seg_5065" s="T235">v:pred 0.3.h:S</ta>
            <ta e="T238" id="Seg_5066" s="T237">v:pred 0.3.h:S</ta>
            <ta e="T239" id="Seg_5067" s="T238">ptcl.neg</ta>
            <ta e="T240" id="Seg_5068" s="T239">v:pred 0.1.h:S</ta>
            <ta e="T241" id="Seg_5069" s="T240">pro.h:S</ta>
            <ta e="T243" id="Seg_5070" s="T242">pro.h:S</ta>
            <ta e="T244" id="Seg_5071" s="T243">n:pred</ta>
            <ta e="T245" id="Seg_5072" s="T244">cop</ta>
            <ta e="T246" id="Seg_5073" s="T245">ptcl.neg</ta>
            <ta e="T247" id="Seg_5074" s="T246">pro.h:S</ta>
            <ta e="T248" id="Seg_5075" s="T247">v:pred</ta>
            <ta e="T250" id="Seg_5076" s="T249">pro.h:S</ta>
            <ta e="T252" id="Seg_5077" s="T251">v:pred</ta>
            <ta e="T255" id="Seg_5078" s="T254">np:S</ta>
            <ta e="T256" id="Seg_5079" s="T255">v:pred</ta>
            <ta e="T258" id="Seg_5080" s="T257">v:pred</ta>
            <ta e="T261" id="Seg_5081" s="T260">np:S</ta>
            <ta e="T262" id="Seg_5082" s="T261">pro.h:S</ta>
            <ta e="T264" id="Seg_5083" s="T263">v:pred</ta>
            <ta e="T265" id="Seg_5084" s="T264">s:purp</ta>
            <ta e="T266" id="Seg_5085" s="T265">pro.h:S</ta>
            <ta e="T268" id="Seg_5086" s="T267">v:pred</ta>
            <ta e="T269" id="Seg_5087" s="T268">v:pred 0.3.h:S</ta>
            <ta e="T271" id="Seg_5088" s="T270">v:pred 0.2.h:S</ta>
            <ta e="T273" id="Seg_5089" s="T272">v:pred 0.2.h:S</ta>
            <ta e="T275" id="Seg_5090" s="T274">pro.h:S</ta>
            <ta e="T276" id="Seg_5091" s="T275">v:pred</ta>
            <ta e="T277" id="Seg_5092" s="T276">pro.h:O</ta>
            <ta e="T279" id="Seg_5093" s="T278">pro.h:S</ta>
            <ta e="T280" id="Seg_5094" s="T279">v:pred</ta>
            <ta e="T281" id="Seg_5095" s="T280">pro.h:S</ta>
            <ta e="T282" id="Seg_5096" s="T281">v:pred</ta>
            <ta e="T283" id="Seg_5097" s="T282">ptcl.neg</ta>
            <ta e="T284" id="Seg_5098" s="T283">v:pred 0.1.h:S</ta>
            <ta e="T285" id="Seg_5099" s="T284">pro.h:S</ta>
            <ta e="T286" id="Seg_5100" s="T285">v:pred</ta>
            <ta e="T288" id="Seg_5101" s="T287">pro.h:S</ta>
            <ta e="T289" id="Seg_5102" s="T288">ptcl.neg</ta>
            <ta e="T290" id="Seg_5103" s="T289">v:pred</ta>
            <ta e="T291" id="Seg_5104" s="T290">v:pred 0.3.h:S</ta>
            <ta e="T292" id="Seg_5105" s="T291">v:pred 0.1.h:S</ta>
            <ta e="T297" id="Seg_5106" s="T296">ptcl.neg</ta>
            <ta e="T298" id="Seg_5107" s="T297">adj:pred</ta>
            <ta e="T306" id="Seg_5108" s="T305">v:pred 0.3.h:S</ta>
            <ta e="T307" id="Seg_5109" s="T306">v:pred 0.3.h:S</ta>
            <ta e="T308" id="Seg_5110" s="T307">v:pred 0.3.h:S</ta>
            <ta e="T309" id="Seg_5111" s="T308">pro.h:S</ta>
            <ta e="T310" id="Seg_5112" s="T309">v:pred</ta>
            <ta e="T313" id="Seg_5113" s="T312">pro.h:S</ta>
            <ta e="T316" id="Seg_5114" s="T315">np:O</ta>
            <ta e="T317" id="Seg_5115" s="T316">v:pred</ta>
            <ta e="T319" id="Seg_5116" s="T318">v:pred 0.2.h:S</ta>
            <ta e="T322" id="Seg_5117" s="T321">np:S</ta>
            <ta e="T323" id="Seg_5118" s="T322">adj:pred</ta>
            <ta e="T324" id="Seg_5119" s="T323">cop</ta>
            <ta e="T327" id="Seg_5120" s="T326">v:pred 0.2.h:S</ta>
            <ta e="T329" id="Seg_5121" s="T328">pro.h:S</ta>
            <ta e="T330" id="Seg_5122" s="T329">v:pred</ta>
            <ta e="T332" id="Seg_5123" s="T331">v:pred 0.3.h:S</ta>
            <ta e="T337" id="Seg_5124" s="T336">np:O</ta>
            <ta e="T339" id="Seg_5125" s="T338">v:pred 0.3.h:S</ta>
            <ta e="T340" id="Seg_5126" s="T339">pro.h:S</ta>
            <ta e="T342" id="Seg_5127" s="T341">v:pred</ta>
            <ta e="T343" id="Seg_5128" s="T342">v:pred 0.3.h:S</ta>
            <ta e="T344" id="Seg_5129" s="T343">v:pred 0.3.h:S</ta>
            <ta e="T345" id="Seg_5130" s="T344">np:S</ta>
            <ta e="T347" id="Seg_5131" s="T346">adj:pred</ta>
            <ta e="T351" id="Seg_5132" s="T350">v:pred 0.3.h:S</ta>
            <ta e="T353" id="Seg_5133" s="T352">v:pred 0.3.h:S</ta>
            <ta e="T354" id="Seg_5134" s="T353">v:pred 0.3.h:S</ta>
            <ta e="T356" id="Seg_5135" s="T355">v:pred 0.3.h:S</ta>
            <ta e="T357" id="Seg_5136" s="T356">v:pred 0.3.h:S</ta>
            <ta e="T359" id="Seg_5137" s="T358">v:pred 0.3.h:S</ta>
            <ta e="T360" id="Seg_5138" s="T359">pro.h:S</ta>
            <ta e="T362" id="Seg_5139" s="T361">v:pred</ta>
            <ta e="T363" id="Seg_5140" s="T362">v:pred 0.2.h:S</ta>
            <ta e="T364" id="Seg_5141" s="T363">v:pred 0.1.h:S</ta>
            <ta e="T365" id="Seg_5142" s="T364">v:pred 0.2.h:S</ta>
            <ta e="T366" id="Seg_5143" s="T365">pro.h:S</ta>
            <ta e="T367" id="Seg_5144" s="T366">v:pred</ta>
            <ta e="T371" id="Seg_5145" s="T370">n:pred</ta>
            <ta e="T372" id="Seg_5146" s="T371">cop 0.3.h:S</ta>
            <ta e="T374" id="Seg_5147" s="T373">np:S</ta>
            <ta e="T376" id="Seg_5148" s="T375">v:pred</ta>
            <ta e="T377" id="Seg_5149" s="T376">np.h:S</ta>
            <ta e="T378" id="Seg_5150" s="T377">v:pred</ta>
            <ta e="T379" id="Seg_5151" s="T378">v:pred</ta>
            <ta e="T380" id="Seg_5152" s="T379">np:S</ta>
            <ta e="T384" id="Seg_5153" s="T383">np.h:S</ta>
            <ta e="T385" id="Seg_5154" s="T384">n:pred</ta>
            <ta e="T386" id="Seg_5155" s="T385">cop</ta>
            <ta e="T388" id="Seg_5156" s="T387">pro.h:S</ta>
            <ta e="T390" id="Seg_5157" s="T389">pro.h:O</ta>
            <ta e="T392" id="Seg_5158" s="T391">v:pred</ta>
            <ta e="T395" id="Seg_5159" s="T394">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T11" id="Seg_5160" s="T10">RUS:gram</ta>
            <ta e="T14" id="Seg_5161" s="T13">RUS:cult</ta>
            <ta e="T25" id="Seg_5162" s="T24">RUS:cult</ta>
            <ta e="T27" id="Seg_5163" s="T26">RUS:gram</ta>
            <ta e="T32" id="Seg_5164" s="T31">RUS:cult</ta>
            <ta e="T35" id="Seg_5165" s="T34">RUS:gram</ta>
            <ta e="T37" id="Seg_5166" s="T36">RUS:mod</ta>
            <ta e="T47" id="Seg_5167" s="T46">RUS:core</ta>
            <ta e="T54" id="Seg_5168" s="T53">RUS:cult</ta>
            <ta e="T57" id="Seg_5169" s="T56">RUS:cult</ta>
            <ta e="T58" id="Seg_5170" s="T57">RUS:gram</ta>
            <ta e="T60" id="Seg_5171" s="T59">RUS:core</ta>
            <ta e="T64" id="Seg_5172" s="T63">RUS:gram</ta>
            <ta e="T70" id="Seg_5173" s="T69">RUS:core</ta>
            <ta e="T73" id="Seg_5174" s="T72">TURK:disc</ta>
            <ta e="T75" id="Seg_5175" s="T74">RUS:core</ta>
            <ta e="T79" id="Seg_5176" s="T78">RUS:cult</ta>
            <ta e="T104" id="Seg_5177" s="T103">RUS:cult</ta>
            <ta e="T108" id="Seg_5178" s="T107">RUS:gram</ta>
            <ta e="T112" id="Seg_5179" s="T111">TURK:disc</ta>
            <ta e="T115" id="Seg_5180" s="T114">RUS:gram</ta>
            <ta e="T117" id="Seg_5181" s="T116">RUS:mod</ta>
            <ta e="T123" id="Seg_5182" s="T122">RUS:gram</ta>
            <ta e="T124" id="Seg_5183" s="T123">RUS:gram</ta>
            <ta e="T129" id="Seg_5184" s="T128">RUS:core</ta>
            <ta e="T130" id="Seg_5185" s="T129">TURK:disc</ta>
            <ta e="T136" id="Seg_5186" s="T135">RUS:cult</ta>
            <ta e="T143" id="Seg_5187" s="T142">RUS:gram</ta>
            <ta e="T150" id="Seg_5188" s="T149">RUS:core</ta>
            <ta e="T158" id="Seg_5189" s="T157">TAT:cult</ta>
            <ta e="T160" id="Seg_5190" s="T159">TURK:disc</ta>
            <ta e="T193" id="Seg_5191" s="T192">RUS:gram</ta>
            <ta e="T204" id="Seg_5192" s="T203">TURK:disc</ta>
            <ta e="T220" id="Seg_5193" s="T219">TURK:disc</ta>
            <ta e="T225" id="Seg_5194" s="T224">RUS:gram</ta>
            <ta e="T226" id="Seg_5195" s="T225">RUS:gram</ta>
            <ta e="T246" id="Seg_5196" s="T245">TURK:disc</ta>
            <ta e="T270" id="Seg_5197" s="T269">RUS:disc</ta>
            <ta e="T272" id="Seg_5198" s="T271">RUS:disc</ta>
            <ta e="T278" id="Seg_5199" s="T277">RUS:disc</ta>
            <ta e="T287" id="Seg_5200" s="T286">TAT:cult</ta>
            <ta e="T296" id="Seg_5201" s="T295">RUS:gram</ta>
            <ta e="T299" id="Seg_5202" s="T298">RUS:gram</ta>
            <ta e="T301" id="Seg_5203" s="T300">TURK:disc</ta>
            <ta e="T303" id="Seg_5204" s="T302">RUS:gram</ta>
            <ta e="T316" id="Seg_5205" s="T315">RUS:cult</ta>
            <ta e="T318" id="Seg_5206" s="T317">RUS:disc</ta>
            <ta e="T322" id="Seg_5207" s="T321">RUS:cult</ta>
            <ta e="T328" id="Seg_5208" s="T327">RUS:gram</ta>
            <ta e="T333" id="Seg_5209" s="T332">RUS:core</ta>
            <ta e="T334" id="Seg_5210" s="T333">TURK:disc</ta>
            <ta e="T338" id="Seg_5211" s="T337">TURK:gram(INDEF)</ta>
            <ta e="T341" id="Seg_5212" s="T340">TURK:disc</ta>
            <ta e="T345" id="Seg_5213" s="T344">RUS:cult</ta>
            <ta e="T346" id="Seg_5214" s="T345">TURK:disc</ta>
            <ta e="T349" id="Seg_5215" s="T348">TURK:disc</ta>
            <ta e="T352" id="Seg_5216" s="T351">RUS:gram</ta>
            <ta e="T358" id="Seg_5217" s="T357">TURK:disc</ta>
            <ta e="T368" id="Seg_5218" s="T367">RUS:gram</ta>
            <ta e="T375" id="Seg_5219" s="T374">TURK:disc</ta>
            <ta e="T380" id="Seg_5220" s="T379">RUS:cult</ta>
            <ta e="T381" id="Seg_5221" s="T380">RUS:gram</ta>
            <ta e="T385" id="Seg_5222" s="T384">TURK:cult</ta>
            <ta e="T393" id="Seg_5223" s="T392">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T175" id="Seg_5224" s="T173">RUS:int.ins</ta>
            <ta e="T242" id="Seg_5225" s="T241">RUS:int.ins</ta>
            <ta e="T394" id="Seg_5226" s="T393">RUS:int.ins</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T2" id="Seg_5227" s="T0">Жил человек.</ta>
            <ta e="T9" id="Seg_5228" s="T2">У него было три дочери.</ta>
            <ta e="T15" id="Seg_5229" s="T9">Он стал собираться ехать в город.</ta>
            <ta e="T21" id="Seg_5230" s="T15">Он спрашивает старшую сестру: "Что тебе купить?"</ta>
            <ta e="T26" id="Seg_5231" s="T21">"Купи мне красивое платье".</ta>
            <ta e="T29" id="Seg_5232" s="T26">А младшая сестра [говорит]:</ta>
            <ta e="T34" id="Seg_5233" s="T29">"Ты мне купи красивые ботинки".</ta>
            <ta e="T42" id="Seg_5234" s="T34">А третью, самую младшую дочь [он спрашивает]: "Что тебе купить?"</ta>
            <ta e="T47" id="Seg_5235" s="T42">"Ты купи мне красный цветок".</ta>
            <ta e="T49" id="Seg_5236" s="T47">Он поехал.</ta>
            <ta e="T51" id="Seg_5237" s="T49">Искал-искал.</ta>
            <ta e="T57" id="Seg_5238" s="T51">Купил одной платье, другой купил ботинки.</ta>
            <ta e="T61" id="Seg_5239" s="T57">А цветка нет.</ta>
            <ta e="T70" id="Seg_5240" s="T61">Он идет и думает: где взять красный цветок?</ta>
            <ta e="T75" id="Seg_5241" s="T70">Шел и увидел в (?) цветы.</ta>
            <ta e="T78" id="Seg_5242" s="T75">Он пошел туда, сорвал их.</ta>
            <ta e="T81" id="Seg_5243" s="T78">[И] ворота закрылись.</ta>
            <ta e="T85" id="Seg_5244" s="T81">Потом он шел, шел (?).</ta>
            <ta e="T88" id="Seg_5245" s="T85">Ночью пришел человек.</ta>
            <ta e="T96" id="Seg_5246" s="T88">"Ты дашь мне свою дочь, ты приведешь ее сюда".</ta>
            <ta e="T99" id="Seg_5247" s="T96">Он думал-думал.</ta>
            <ta e="T100" id="Seg_5248" s="T99">"Приведу!"</ta>
            <ta e="T103" id="Seg_5249" s="T100">Потом утром он встал.</ta>
            <ta e="T107" id="Seg_5250" s="T103">Ворота закрылись. [?]</ta>
            <ta e="T110" id="Seg_5251" s="T107">И пришел домой.</ta>
            <ta e="T113" id="Seg_5252" s="T110">Привёз, всё подарил. [?]</ta>
            <ta e="T114" id="Seg_5253" s="T113">Трем [дочерям]. [?]</ta>
            <ta e="T119" id="Seg_5254" s="T114">"А тебе надо туда пойти".</ta>
            <ta e="T126" id="Seg_5255" s="T119">Она говорит: "Ну и что, я пойду".</ta>
            <ta e="T131" id="Seg_5256" s="T126">"Ты там много цветов соберешь".</ta>
            <ta e="T135" id="Seg_5257" s="T131">Он (принес?), отправил ее туда.</ta>
            <ta e="T137" id="Seg_5258" s="T135">Ворота закрылись.</ta>
            <ta e="T145" id="Seg_5259" s="T137">(…) осталась и вошла в дом.</ta>
            <ta e="T149" id="Seg_5260" s="T145">(…) дней шла. [?]</ta>
            <ta e="T157" id="Seg_5261" s="T149">Цветы везде подписаны: Маланьины, куда она ни пойдет, везде Маланья.</ta>
            <ta e="T160" id="Seg_5262" s="T157">Она вошла в дом.</ta>
            <ta e="T166" id="Seg_5263" s="T160">[?]</ta>
            <ta e="T170" id="Seg_5264" s="T166">(…), потом поел(а).</ta>
            <ta e="T181" id="Seg_5265" s="T170">[?]</ta>
            <ta e="T184" id="Seg_5266" s="T181">Потом она легла спать.</ta>
            <ta e="T187" id="Seg_5267" s="T184">Ночью пришел человек.</ta>
            <ta e="T192" id="Seg_5268" s="T187">"Ты, Маланья, ты меня любишь?"</ta>
            <ta e="T201" id="Seg_5269" s="T192">"А я не знаю, кто ты.</ta>
            <ta e="T203" id="Seg_5270" s="T201">Или сюда".</ta>
            <ta e="T209" id="Seg_5271" s="T203">"Нет, я не приду, ты испугаешься".</ta>
            <ta e="T211" id="Seg_5272" s="T209">Потом он ушел.</ta>
            <ta e="T214" id="Seg_5273" s="T211">Она думала-думала.</ta>
            <ta e="T218" id="Seg_5274" s="T214">Потом вторая ночь пришла.</ta>
            <ta e="T229" id="Seg_5275" s="T218">Она пошла к реке, даже на реке там написано: Маланьина рыба.</ta>
            <ta e="T234" id="Seg_5276" s="T229">Потом опять пришла ночь.</ta>
            <ta e="T242" id="Seg_5277" s="T234">Она думала-думала, потом говорит: "Я не знаю, кто ты.</ta>
            <ta e="T245" id="Seg_5278" s="T242">Кто ты есть".</ta>
            <ta e="T252" id="Seg_5279" s="T245">"Нет, я не приду, ты опять испугаешься".</ta>
            <ta e="T256" id="Seg_5280" s="T252">Потом пришла третья ночь.</ta>
            <ta e="T265" id="Seg_5281" s="T256">(…) пришла третья ночь, она опять легла спать.</ta>
            <ta e="T269" id="Seg_5282" s="T265">Он опять пришел, спрашивает?</ta>
            <ta e="T271" id="Seg_5283" s="T269">"Ну, ты подумала?"</ta>
            <ta e="T277" id="Seg_5284" s="T271">"Да иди сюда, я посмотрю на тебя".</ta>
            <ta e="T280" id="Seg_5285" s="T277">"Да ты испугаешься".</ta>
            <ta e="T284" id="Seg_5286" s="T280">Она говорит: "Я не испугаюсь".</ta>
            <ta e="T287" id="Seg_5287" s="T284">Он вошел в дом.</ta>
            <ta e="T290" id="Seg_5288" s="T287">Она не испугалась.</ta>
            <ta e="T293" id="Seg_5289" s="T290">Говорит: "Я буду с ним жить".</ta>
            <ta e="T304" id="Seg_5290" s="T293">(…), пусть некрасивый, (…)</ta>
            <ta e="T312" id="Seg_5291" s="T304">Потом она жила, жила, [и] говорит: "Я пойду к отцу".</ta>
            <ta e="T317" id="Seg_5292" s="T312">Он дал ей кольцо.</ta>
            <ta e="T319" id="Seg_5293" s="T317">"Ну, иди.</ta>
            <ta e="T330" id="Seg_5294" s="T319">Когда кольцо станет черным, приходи скорее, а то я умру".</ta>
            <ta e="T333" id="Seg_5295" s="T330">Она пошла [к] сестре.</ta>
            <ta e="T339" id="Seg_5296" s="T333">[Сестра] дала ей какую-то воду.</ta>
            <ta e="T344" id="Seg_5297" s="T339">Она спала-спала, [потом] встала.</ta>
            <ta e="T348" id="Seg_5298" s="T344">Ее колечко черное.</ta>
            <ta e="T351" id="Seg_5299" s="T348">Она все бросила.</ta>
            <ta e="T357" id="Seg_5300" s="T351">И пошла; пришла туда: он лежит, он умер.</ta>
            <ta e="T359" id="Seg_5301" s="T357">Она заплакала.</ta>
            <ta e="T363" id="Seg_5302" s="T359">"Я люблю тебя, вставай!"</ta>
            <ta e="T365" id="Seg_5303" s="T363">"Люблю, вставай!"</ta>
            <ta e="T367" id="Seg_5304" s="T365">Он встал.</ta>
            <ta e="T372" id="Seg_5305" s="T367">И стал очень красивым юношей.</ta>
            <ta e="T376" id="Seg_5306" s="T372">Потом колокола звонят.</ta>
            <ta e="T378" id="Seg_5307" s="T376">Люди идут [=сюда]</ta>
            <ta e="T386" id="Seg_5308" s="T378">Появился дом, а этот юноша стал его князем.</ta>
            <ta e="T392" id="Seg_5309" s="T386">Потом он на ней женился.</ta>
            <ta e="T395" id="Seg_5310" s="T392">И он стали жить.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T2" id="Seg_5311" s="T0">There lived a man.</ta>
            <ta e="T9" id="Seg_5312" s="T2">He had three daugters.</ta>
            <ta e="T15" id="Seg_5313" s="T9">He began to prepare to go to the town.</ta>
            <ta e="T21" id="Seg_5314" s="T15">He asks the elder sister: "What do you want me to buy for you?"</ta>
            <ta e="T26" id="Seg_5315" s="T21">"Buy me a beautiful dress."</ta>
            <ta e="T29" id="Seg_5316" s="T26">And the small sister [says]:</ta>
            <ta e="T34" id="Seg_5317" s="T29">"Bring me beautiful shoes."</ta>
            <ta e="T42" id="Seg_5318" s="T34">And [he asks] the third, the even younger girl: "What should I buy for you?"</ta>
            <ta e="T47" id="Seg_5319" s="T42">"Buy me a red flower."</ta>
            <ta e="T49" id="Seg_5320" s="T47">He left.</ta>
            <ta e="T51" id="Seg_5321" s="T49">He looked for [it].</ta>
            <ta e="T57" id="Seg_5322" s="T51">He bought a dress for one of them, he bought shoes.</ta>
            <ta e="T61" id="Seg_5323" s="T57">But there is no flower.</ta>
            <ta e="T70" id="Seg_5324" s="T61">Then he is going and thinking: where to find a red flower?</ta>
            <ta e="T75" id="Seg_5325" s="T70">While going, he saw flowers in (?).</ta>
            <ta e="T78" id="Seg_5326" s="T75">He went there, picked it up.</ta>
            <ta e="T81" id="Seg_5327" s="T78">[And] the gate closed.</ta>
            <ta e="T85" id="Seg_5328" s="T81">Then he went, he went (?).</ta>
            <ta e="T88" id="Seg_5329" s="T85">In the evening, a man came.</ta>
            <ta e="T96" id="Seg_5330" s="T88">"You will give me your daughter, you'll bring her here."</ta>
            <ta e="T99" id="Seg_5331" s="T96">He thought for some time.</ta>
            <ta e="T100" id="Seg_5332" s="T99">"I'll bring!"</ta>
            <ta e="T103" id="Seg_5333" s="T100">Then in the morning he got up.</ta>
            <ta e="T107" id="Seg_5334" s="T103">The gate closed. [?]</ta>
            <ta e="T110" id="Seg_5335" s="T107">And he came home.</ta>
            <ta e="T113" id="Seg_5336" s="T110">He brought, gave everything. [?]</ta>
            <ta e="T114" id="Seg_5337" s="T113">To(?) the three [daughters]. [?]</ta>
            <ta e="T119" id="Seg_5338" s="T114">"And you have to go there."</ta>
            <ta e="T126" id="Seg_5339" s="T119">She says: "Why not, I'll go."</ta>
            <ta e="T131" id="Seg_5340" s="T126">"You'll pick many flowers there."</ta>
            <ta e="T135" id="Seg_5341" s="T131">He (brought?), sent her there.</ta>
            <ta e="T137" id="Seg_5342" s="T135">The gate closed.</ta>
            <ta e="T145" id="Seg_5343" s="T137">(…) remained and came home.</ta>
            <ta e="T149" id="Seg_5344" s="T145">(…) day[s] went on. [?]</ta>
            <ta e="T157" id="Seg_5345" s="T149">Flowers always have an inscription: Malanya's, wherever she goes, always Malanya.</ta>
            <ta e="T160" id="Seg_5346" s="T157">She came home.</ta>
            <ta e="T166" id="Seg_5347" s="T160">[?]</ta>
            <ta e="T170" id="Seg_5348" s="T166">(…) then s/he ate.</ta>
            <ta e="T181" id="Seg_5349" s="T170">[?]</ta>
            <ta e="T184" id="Seg_5350" s="T181">Then she lay down to sleep.</ta>
            <ta e="T187" id="Seg_5351" s="T184">At night, a man came.</ta>
            <ta e="T192" id="Seg_5352" s="T187">"You, Malanya, do you love me?"</ta>
            <ta e="T201" id="Seg_5353" s="T192">"But I don't know who you are.</ta>
            <ta e="T203" id="Seg_5354" s="T201">Come here."</ta>
            <ta e="T209" id="Seg_5355" s="T203">"No, I won't come, you'll be scared."</ta>
            <ta e="T211" id="Seg_5356" s="T209">Then he left.</ta>
            <ta e="T214" id="Seg_5357" s="T211">She thought.</ta>
            <ta e="T218" id="Seg_5358" s="T214">Then the second night came.</ta>
            <ta e="T229" id="Seg_5359" s="T218">She went to the river, even the fish there has an inscription, Malanya's fish.</ta>
            <ta e="T234" id="Seg_5360" s="T229">Then again a night came.</ta>
            <ta e="T242" id="Seg_5361" s="T234">She thought for some time, then she said: "I don't know who you are.</ta>
            <ta e="T245" id="Seg_5362" s="T242">Who you are."</ta>
            <ta e="T252" id="Seg_5363" s="T245">"No, I won't come, you'll be afraid again."</ta>
            <ta e="T256" id="Seg_5364" s="T252">Then the third night came.</ta>
            <ta e="T265" id="Seg_5365" s="T256">(…) the third night came, she went again to the bed.</ta>
            <ta e="T269" id="Seg_5366" s="T265">He came again, and asks:</ta>
            <ta e="T271" id="Seg_5367" s="T269">"Well, did you thought it over?"</ta>
            <ta e="T277" id="Seg_5368" s="T271">"Come here, I'll look at you."</ta>
            <ta e="T280" id="Seg_5369" s="T277">"But you'll be scared."</ta>
            <ta e="T284" id="Seg_5370" s="T280">She says: "I won't be scared."</ta>
            <ta e="T287" id="Seg_5371" s="T284">He entered the house.</ta>
            <ta e="T290" id="Seg_5372" s="T287">She wasn't scared.</ta>
            <ta e="T293" id="Seg_5373" s="T290">She says: "I'll live with him."</ta>
            <ta e="T304" id="Seg_5374" s="T293">(…) even if he's not handsome, (…)</ta>
            <ta e="T312" id="Seg_5375" s="T304">The she lived, and lived, [and then] she says: "I'll go to my father."</ta>
            <ta e="T317" id="Seg_5376" s="T312">He gave her a ring.</ta>
            <ta e="T319" id="Seg_5377" s="T317">"Well, go.</ta>
            <ta e="T330" id="Seg_5378" s="T319">When the ring becomes black, come quickly, or I'll die."</ta>
            <ta e="T333" id="Seg_5379" s="T330">She went [to her] sister.</ta>
            <ta e="T339" id="Seg_5380" s="T333">[Her sister] gave her some kind of water.</ta>
            <ta e="T344" id="Seg_5381" s="T339">She slept for some time, [then] she woke up.</ta>
            <ta e="T348" id="Seg_5382" s="T344">Her ring is black.</ta>
            <ta e="T351" id="Seg_5383" s="T348">She abandoned everything.</ta>
            <ta e="T357" id="Seg_5384" s="T351">And went; came there: he's lying, he has died.</ta>
            <ta e="T359" id="Seg_5385" s="T357">She [began to] cry.</ta>
            <ta e="T363" id="Seg_5386" s="T359">"I love you, get up!"</ta>
            <ta e="T365" id="Seg_5387" s="T363">"I love [you], get up!"</ta>
            <ta e="T367" id="Seg_5388" s="T365">He got up.</ta>
            <ta e="T372" id="Seg_5389" s="T367">And became a very handsome boy.</ta>
            <ta e="T376" id="Seg_5390" s="T372">Then the bells ring.</ta>
            <ta e="T378" id="Seg_5391" s="T376">People are going [=coming].</ta>
            <ta e="T386" id="Seg_5392" s="T378">There was built a city, and this boy became its chief.</ta>
            <ta e="T392" id="Seg_5393" s="T386">Then he married her.</ta>
            <ta e="T395" id="Seg_5394" s="T392">And they began to live.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T2" id="Seg_5395" s="T0">Es lebte ein Mann.</ta>
            <ta e="T9" id="Seg_5396" s="T2">Er hatte drei Töchter.</ta>
            <ta e="T15" id="Seg_5397" s="T9">Er begann sich vorzubereiten in die Stadt zu gehen.</ta>
            <ta e="T21" id="Seg_5398" s="T15">Er fragt die älteste Schwester: "Was möchtest du, dass ich dir kaufe?"</ta>
            <ta e="T26" id="Seg_5399" s="T21">"Kauf mir ein schönes Kleid."</ta>
            <ta e="T29" id="Seg_5400" s="T26">Und die kleine Schwester [sagt]:</ta>
            <ta e="T34" id="Seg_5401" s="T29">"Bring mir schöne Schuhe."</ta>
            <ta e="T42" id="Seg_5402" s="T34">Und [er fragt] das dritte, das noch jüngere Mädchen: "Was soll ich für dich kaufen?"</ta>
            <ta e="T47" id="Seg_5403" s="T42">"Kauf mir eine rote Blume."</ta>
            <ta e="T49" id="Seg_5404" s="T47">Er ging weg.</ta>
            <ta e="T51" id="Seg_5405" s="T49">Er suchte [sie].</ta>
            <ta e="T57" id="Seg_5406" s="T51">Er kaufte ein Kleid für die eine von ihnen, er kaufte Schuhe.</ta>
            <ta e="T61" id="Seg_5407" s="T57">Aber es gibt keine Blume.</ta>
            <ta e="T70" id="Seg_5408" s="T61">Dann geht er und denkt: Wo finde ich eine rote Blume?</ta>
            <ta e="T75" id="Seg_5409" s="T70">Während er geht, sah er Blumen in (?).</ta>
            <ta e="T78" id="Seg_5410" s="T75">Er ging dorthin, pflückte sie.</ta>
            <ta e="T81" id="Seg_5411" s="T78">[Und] das Tor schloss sich.</ta>
            <ta e="T85" id="Seg_5412" s="T81">So ging und ging er (?).</ta>
            <ta e="T88" id="Seg_5413" s="T85">Am Abend kam ein Mann.</ta>
            <ta e="T96" id="Seg_5414" s="T88">"Du wirst mir deine Tochter geben, du wirst sie herbringen."</ta>
            <ta e="T99" id="Seg_5415" s="T96">Er dachte eine Zeit nach.</ta>
            <ta e="T100" id="Seg_5416" s="T99">"Ich bring sie!"</ta>
            <ta e="T103" id="Seg_5417" s="T100">Dann am Morgen stand er auf.</ta>
            <ta e="T107" id="Seg_5418" s="T103">Das Tor schloss sich. [?]</ta>
            <ta e="T110" id="Seg_5419" s="T107">Und er kam nach Hause.</ta>
            <ta e="T113" id="Seg_5420" s="T110">Er brachte, gab alles. [?]</ta>
            <ta e="T114" id="Seg_5421" s="T113">Den drei [Töchtern]. [?]</ta>
            <ta e="T119" id="Seg_5422" s="T114">"Und du musst dorthin gehen."</ta>
            <ta e="T126" id="Seg_5423" s="T119">Sie sagt: "Warum nicht, ich gehe."</ta>
            <ta e="T131" id="Seg_5424" s="T126">"Du wirst dort viele Blumen pflücken."</ta>
            <ta e="T135" id="Seg_5425" s="T131">Er (brachte?) und schickte sie dorthin.</ta>
            <ta e="T137" id="Seg_5426" s="T135">Das Tor schloss sich.</ta>
            <ta e="T145" id="Seg_5427" s="T137">(…) und kam nach Hause.</ta>
            <ta e="T149" id="Seg_5428" s="T145">(…) Tag[e] ging weiter. [?]</ta>
            <ta e="T157" id="Seg_5429" s="T149">Blumen haben immer eine Inschrift: Malanjas, wohin sie auch geht, immer Malanja.</ta>
            <ta e="T160" id="Seg_5430" s="T157">Sie betrat das Haus.</ta>
            <ta e="T166" id="Seg_5431" s="T160">[?]</ta>
            <ta e="T170" id="Seg_5432" s="T166">(…) dann aß er/sie.</ta>
            <ta e="T181" id="Seg_5433" s="T170">[?]</ta>
            <ta e="T184" id="Seg_5434" s="T181">Dann legte sie sich hin zum schlafen.</ta>
            <ta e="T187" id="Seg_5435" s="T184">In der Nacht kam ein Mann.</ta>
            <ta e="T192" id="Seg_5436" s="T187">"Du, Malanja, liebst du mich?"</ta>
            <ta e="T201" id="Seg_5437" s="T192">"Aber ich weiß nicht, wer du bist.</ta>
            <ta e="T203" id="Seg_5438" s="T201">Komm her."</ta>
            <ta e="T209" id="Seg_5439" s="T203">"Nein, ich komme nicht, du wirst Angst haben."</ta>
            <ta e="T211" id="Seg_5440" s="T209">Dann ging er.</ta>
            <ta e="T214" id="Seg_5441" s="T211">Sie dachte.</ta>
            <ta e="T218" id="Seg_5442" s="T214">Dann kam die zweite Nacht.</ta>
            <ta e="T229" id="Seg_5443" s="T218">Sie ging zum Fluss, sogar der Fisch dort hat eine Inschrift, Malanjas Fisch.</ta>
            <ta e="T234" id="Seg_5444" s="T229">Dort wurde es wieder Nacht.</ta>
            <ta e="T242" id="Seg_5445" s="T234">Sie dachte einige Zeit nach, dann sagte sie: "Ich weiß nicht, wer du bist.</ta>
            <ta e="T245" id="Seg_5446" s="T242">Wer du bist."</ta>
            <ta e="T252" id="Seg_5447" s="T245">"Nein, ich komme nicht, du wirst wieder Angst haben."</ta>
            <ta e="T256" id="Seg_5448" s="T252">Dann kam die dritte Nacht.</ta>
            <ta e="T265" id="Seg_5449" s="T256">(…) die dritte Nacht kam, sie ging wieder ins Bett.</ta>
            <ta e="T269" id="Seg_5450" s="T265">Er kam wieder und fragt:</ta>
            <ta e="T271" id="Seg_5451" s="T269">"Nun, hast du es dir überlegt?"</ta>
            <ta e="T277" id="Seg_5452" s="T271">"Komm her, ich werde dich ansehen."</ta>
            <ta e="T280" id="Seg_5453" s="T277">"Aber du wirst Angst haben."</ta>
            <ta e="T284" id="Seg_5454" s="T280">Sie sagt: "Ich werde keine Angst haben."</ta>
            <ta e="T287" id="Seg_5455" s="T284">Er ging ins Haus hinein.</ta>
            <ta e="T290" id="Seg_5456" s="T287">Sie hatte keine Angst.</ta>
            <ta e="T293" id="Seg_5457" s="T290">Sie sagt: "Ich werde hier mit ihm leben."</ta>
            <ta e="T304" id="Seg_5458" s="T293">(…), wenn er auch nicht schön ist (…)</ta>
            <ta e="T312" id="Seg_5459" s="T304">Dann lebte sie und lebte, [und dann] sagt sie: "Ich gehe zu meinem Vater."</ta>
            <ta e="T317" id="Seg_5460" s="T312">Er gab ihr einen Ring.</ta>
            <ta e="T319" id="Seg_5461" s="T317">"Nun, geh.</ta>
            <ta e="T330" id="Seg_5462" s="T319">Wenn der Ring schwarz wird, komm schnell oder ich werder sterben."</ta>
            <ta e="T333" id="Seg_5463" s="T330">Sie ging [zu ihrer] Schwester.</ta>
            <ta e="T339" id="Seg_5464" s="T333">[Ihre Schwester] gab ihr irgendein Wasser.</ta>
            <ta e="T344" id="Seg_5465" s="T339">Sie schlief einige Zeit, [dann] wachte sie auf.</ta>
            <ta e="T348" id="Seg_5466" s="T344">Ihr Ring ist schwarz.</ta>
            <ta e="T351" id="Seg_5467" s="T348">Sie ließ alles zurück.</ta>
            <ta e="T357" id="Seg_5468" s="T351">Und ging; kam dorthin: er liegt dort, er ist gestorben.</ta>
            <ta e="T359" id="Seg_5469" s="T357">Sie [fing an] zu weinen.</ta>
            <ta e="T363" id="Seg_5470" s="T359">"Ich liebe dich, steh auf!"</ta>
            <ta e="T365" id="Seg_5471" s="T363">"Ich liebe [dich], steh auf!"</ta>
            <ta e="T367" id="Seg_5472" s="T365">Er stand auf.</ta>
            <ta e="T372" id="Seg_5473" s="T367">Und wurde ein sehr attraktiver Junge.</ta>
            <ta e="T376" id="Seg_5474" s="T372">Dann läutete die Glocke.</ta>
            <ta e="T378" id="Seg_5475" s="T376">Leute gehen [= kommen].</ta>
            <ta e="T386" id="Seg_5476" s="T378">Dann wurde eine Stadt gebaut und dieser Junge wurde ihr Chef.</ta>
            <ta e="T392" id="Seg_5477" s="T386">Dann heiratete er sie.</ta>
            <ta e="T395" id="Seg_5478" s="T392">Und sie fingen an zu leben.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T2" id="Seg_5479" s="T0">[GVY:] The tale "The Scarlett Flower" (Аленький цветочек); but here are some details that were not in the original text.</ta>
            <ta e="T15" id="Seg_5480" s="T9">[GVY:] NB [goratto]: the ə assimilates to the preceding "o", which it its turn is reduces to "a" like in Russian (or does it assimilate to the first syllable?).</ta>
            <ta e="T34" id="Seg_5481" s="T29">[GVY:] Or "kuvasʔi"</ta>
            <ta e="T78" id="Seg_5482" s="T75">[GVY:] nĭŋggəbi?</ta>
            <ta e="T81" id="Seg_5483" s="T78">[GVY:] unclear, but the gate will re-appear below.</ta>
            <ta e="T114" id="Seg_5484" s="T113">[GVY:] The form and the meaning are unclear.</ta>
            <ta e="T137" id="Seg_5485" s="T135">[GVY:] kaʔjluʔpiʔi? </ta>
            <ta e="T181" id="Seg_5486" s="T170">[GVY:] Totally unclear.</ta>
            <ta e="T229" id="Seg_5487" s="T218">[GVY:] In the original there is no mention of fish or river.</ta>
            <ta e="T256" id="Seg_5488" s="T252">[GVY:] kanbi</ta>
            <ta e="T317" id="Seg_5489" s="T312">[GVY:] hier - kolʼtso</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T396" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
