<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID7F9D6463-73F5-BDB6-96CF-38B95D51C3E0">
   <head>
      <meta-information>
         <project-name>Kamas</project-name>
         <transcription-name>PKZ_196X_DaughterAndStepdaughter_flk</transcription-name>
         <referenced-file url="PKZ_196X_DaughterAndStepdaughter_flk.wav" />
         <referenced-file url="PKZ_196X_DaughterAndStepdaughter_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_DaughterAndStepdaughter_flk\PKZ_196X_DaughterAndStepdaughter_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">427</ud-information>
            <ud-information attribute-name="# HIAT:w">281</ud-information>
            <ud-information attribute-name="# e">279</ud-information>
            <ud-information attribute-name="# HIAT:u">44</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.025" type="appl" />
         <tli id="T1" time="0.727" type="appl" />
         <tli id="T2" time="1.428" type="appl" />
         <tli id="T3" time="2.13" type="appl" />
         <tli id="T4" time="2.832" type="appl" />
         <tli id="T5" time="3.533" type="appl" />
         <tli id="T6" time="4.268332807367498" />
         <tli id="T7" time="4.79" type="appl" />
         <tli id="T8" time="5.345" type="appl" />
         <tli id="T9" time="5.9" type="appl" />
         <tli id="T10" time="6.461666748245041" />
         <tli id="T11" time="6.944" type="appl" />
         <tli id="T12" time="7.428" type="appl" />
         <tli id="T13" time="7.911" type="appl" />
         <tli id="T14" time="8.395" type="appl" />
         <tli id="T15" time="8.879" type="appl" />
         <tli id="T16" time="9.362" type="appl" />
         <tli id="T17" time="9.846" type="appl" />
         <tli id="T18" time="10.426497402597402" />
         <tli id="T19" time="11.294" type="appl" />
         <tli id="T20" time="12.078" type="appl" />
         <tli id="T21" time="12.861" type="appl" />
         <tli id="T22" time="13.645" type="appl" />
         <tli id="T23" time="14.429" type="appl" />
         <tli id="T24" time="15.212" type="appl" />
         <tli id="T25" time="15.996" type="appl" />
         <tli id="T26" time="17.19972077922078" />
         <tli id="T27" time="17.741" type="appl" />
         <tli id="T28" time="18.512" type="appl" />
         <tli id="T29" time="19.282" type="appl" />
         <tli id="T30" time="20.053" type="appl" />
         <tli id="T31" time="20.824" type="appl" />
         <tli id="T32" time="21.595" type="appl" />
         <tli id="T33" time="22.366" type="appl" />
         <tli id="T34" time="23.137" type="appl" />
         <tli id="T35" time="23.908" type="appl" />
         <tli id="T36" time="24.678" type="appl" />
         <tli id="T37" time="25.449" type="appl" />
         <tli id="T38" time="26.519569480519483" />
         <tli id="T39" time="27.225" type="appl" />
         <tli id="T40" time="27.89" type="appl" />
         <tli id="T41" time="28.555" type="appl" />
         <tli id="T42" time="29.22" type="appl" />
         <tli id="T43" time="29.885" type="appl" />
         <tli id="T44" time="30.55" type="appl" />
         <tli id="T45" time="31.215" type="appl" />
         <tli id="T46" time="31.88" type="appl" />
         <tli id="T47" time="32.545" type="appl" />
         <tli id="T48" time="33.21" type="appl" />
         <tli id="T49" time="33.875" type="appl" />
         <tli id="T50" time="34.54" type="appl" />
         <tli id="T51" time="35.205" type="appl" />
         <tli id="T52" time="35.97941590909091" />
         <tli id="T53" time="36.62" type="appl" />
         <tli id="T54" time="37.2" type="appl" />
         <tli id="T55" time="37.78" type="appl" />
         <tli id="T56" time="38.36" type="appl" />
         <tli id="T57" time="39.327" type="appl" />
         <tli id="T58" time="40.233" type="appl" />
         <tli id="T59" time="41.14" type="appl" />
         <tli id="T60" time="42.047" type="appl" />
         <tli id="T61" time="42.953" type="appl" />
         <tli id="T62" time="43.62595844155844" />
         <tli id="T63" time="44.34594675324675" />
         <tli id="T64" time="45.1259340909091" />
         <tli id="T65" time="46.42591298701299" />
         <tli id="T66" time="47.087" type="appl" />
         <tli id="T67" time="47.593" type="appl" />
         <tli id="T68" time="48.11333351004464" />
         <tli id="T69" time="48.641" type="appl" />
         <tli id="T70" time="49.177" type="appl" />
         <tli id="T71" time="49.714" type="appl" />
         <tli id="T72" time="50.578" type="appl" />
         <tli id="T73" time="51.402" type="appl" />
         <tli id="T74" time="52.226" type="appl" />
         <tli id="T75" time="53.05" type="appl" />
         <tli id="T76" time="54.652446103896104" />
         <tli id="T77" time="55.436" type="appl" />
         <tli id="T78" time="56.164" type="appl" />
         <tli id="T79" time="56.892" type="appl" />
         <tli id="T80" time="57.619" type="appl" />
         <tli id="T81" time="58.308" type="appl" />
         <tli id="T82" time="58.897" type="appl" />
         <tli id="T83" time="59.485" type="appl" />
         <tli id="T84" time="60.074" type="appl" />
         <tli id="T85" time="60.662" type="appl" />
         <tli id="T86" time="61.251" type="appl" />
         <tli id="T87" time="61.839" type="appl" />
         <tli id="T88" time="62.471" type="appl" />
         <tli id="T89" time="63.099" type="appl" />
         <tli id="T90" time="63.726" type="appl" />
         <tli id="T91" time="64.353" type="appl" />
         <tli id="T92" time="64.981" type="appl" />
         <tli id="T93" time="65.608" type="appl" />
         <tli id="T94" time="66.235" type="appl" />
         <tli id="T95" time="66.863" type="appl" />
         <tli id="T96" time="67.49" type="appl" />
         <tli id="T97" time="68.337" type="appl" />
         <tli id="T98" time="68.901" type="appl" />
         <tli id="T99" time="69.464" type="appl" />
         <tli id="T100" time="70.027" type="appl" />
         <tli id="T101" time="70.591" type="appl" />
         <tli id="T102" time="71.154" type="appl" />
         <tli id="T103" time="71.717" type="appl" />
         <tli id="T104" time="72.281" type="appl" />
         <tli id="T105" time="72.844" type="appl" />
         <tli id="T106" time="73.589" type="appl" />
         <tli id="T107" time="74.55212305194804" />
         <tli id="T108" time="75.384" type="appl" />
         <tli id="T109" time="76.264" type="appl" />
         <tli id="T110" time="77.144" type="appl" />
         <tli id="T111" time="78.55205811688312" />
         <tli id="T112" time="79.167" type="appl" />
         <tli id="T113" time="79.935" type="appl" />
         <tli id="T114" time="80.703" type="appl" />
         <tli id="T115" time="81.471" type="appl" />
         <tli id="T116" time="82.239" type="appl" />
         <tli id="T117" time="83.316" type="appl" />
         <tli id="T118" time="84.259" type="appl" />
         <tli id="T119" time="85.202" type="appl" />
         <tli id="T120" time="86.144" type="appl" />
         <tli id="T121" time="87.204" type="appl" />
         <tli id="T122" time="88.158" type="appl" />
         <tli id="T123" time="89.112" type="appl" />
         <tli id="T124" time="90.066" type="appl" />
         <tli id="T125" time="91.02" type="appl" />
         <tli id="T126" time="91.974" type="appl" />
         <tli id="T127" time="92.928" type="appl" />
         <tli id="T128" time="93.882" type="appl" />
         <tli id="T129" time="94.524" type="appl" />
         <tli id="T130" time="95.138" type="appl" />
         <tli id="T131" time="96.31176980519481" />
         <tli id="T132" time="97.276" type="appl" />
         <tli id="T133" time="98.096" type="appl" />
         <tli id="T134" time="98.871" type="appl" />
         <tli id="T135" time="99.601" type="appl" />
         <tli id="T136" time="100.387" type="appl" />
         <tli id="T137" time="101.044" type="appl" />
         <tli id="T138" time="101.701" type="appl" />
         <tli id="T139" time="102.359" type="appl" />
         <tli id="T140" time="103.016" type="appl" />
         <tli id="T141" time="103.673" type="appl" />
         <tli id="T142" time="104.33" type="appl" />
         <tli id="T143" time="105.134" type="appl" />
         <tli id="T144" time="105.883" type="appl" />
         <tli id="T145" time="106.632" type="appl" />
         <tli id="T146" time="107.381" type="appl" />
         <tli id="T147" time="108.13" type="appl" />
         <tli id="T148" time="109.495" type="appl" />
         <tli id="T149" time="110.73" type="appl" />
         <tli id="T150" time="111.965" type="appl" />
         <tli id="T151" time="113.4914909090909" />
         <tli id="T152" time="114.529" type="appl" />
         <tli id="T153" time="115.316" type="appl" />
         <tli id="T154" time="116.103" type="appl" />
         <tli id="T155" time="116.89" type="appl" />
         <tli id="T156" time="117.677" type="appl" />
         <tli id="T157" time="118.464" type="appl" />
         <tli id="T158" time="119.251" type="appl" />
         <tli id="T159" time="120.038" type="appl" />
         <tli id="T160" time="120.825" type="appl" />
         <tli id="T161" time="122.0913512987013" />
         <tli id="T162" time="122.817" type="appl" />
         <tli id="T163" time="123.383" type="appl" />
         <tli id="T164" time="123.95" type="appl" />
         <tli id="T165" time="124.517" type="appl" />
         <tli id="T166" time="125.083" type="appl" />
         <tli id="T167" time="125.80462435064936" />
         <tli id="T168" time="126.442" type="appl" />
         <tli id="T169" time="126.983" type="appl" />
         <tli id="T170" time="127.524" type="appl" />
         <tli id="T171" time="128.066" type="appl" />
         <tli id="T172" time="128.608" type="appl" />
         <tli id="T173" time="129.149" type="appl" />
         <tli id="T174" time="129.69" type="appl" />
         <tli id="T175" time="130.232" type="appl" />
         <tli id="T176" time="130.774" type="appl" />
         <tli id="T177" time="131.53119805194805" />
         <tli id="T178" time="131.942" type="appl" />
         <tli id="T179" time="132.533" type="appl" />
         <tli id="T180" time="133.125" type="appl" />
         <tli id="T181" time="133.717" type="appl" />
         <tli id="T182" time="134.309" type="appl" />
         <tli id="T183" time="134.9" type="appl" />
         <tli id="T184" time="135.6444646103896" />
         <tli id="T185" time="136.324" type="appl" />
         <tli id="T186" time="136.991" type="appl" />
         <tli id="T187" time="137.658" type="appl" />
         <tli id="T188" time="138.51108474025975" />
         <tli id="T189" time="139.168" type="appl" />
         <tli id="T190" time="139.886" type="appl" />
         <tli id="T191" time="140.604" type="appl" />
         <tli id="T192" time="141.322" type="appl" />
         <tli id="T193" time="142.13769253246753" />
         <tli id="T194" time="142.981" type="appl" />
         <tli id="T195" time="143.751" type="appl" />
         <tli id="T196" time="144.522" type="appl" />
         <tli id="T197" time="145.292" type="appl" />
         <tli id="T198" time="146.063" type="appl" />
         <tli id="T199" time="146.833" type="appl" />
         <tli id="T200" time="147.71760194805194" />
         <tli id="T201" time="148.402" type="appl" />
         <tli id="T202" time="149.012" type="appl" />
         <tli id="T203" time="149.622" type="appl" />
         <tli id="T204" time="150.464" type="appl" />
         <tli id="T205" time="151.178" type="appl" />
         <tli id="T206" time="151.891" type="appl" />
         <tli id="T207" time="152.605" type="appl" />
         <tli id="T208" time="153.319" type="appl" />
         <tli id="T209" time="154.032" type="appl" />
         <tli id="T210" time="154.746" type="appl" />
         <tli id="T211" time="155.53747499999997" />
         <tli id="T212" time="156.197" type="appl" />
         <tli id="T213" time="156.752" type="appl" />
         <tli id="T214" time="157.307" type="appl" />
         <tli id="T215" time="157.862" type="appl" />
         <tli id="T216" time="158.57" type="appl" />
         <tli id="T217" time="159.11" type="appl" />
         <tli id="T218" time="159.65" type="appl" />
         <tli id="T219" time="160.19" type="appl" />
         <tli id="T220" time="160.73" type="appl" />
         <tli id="T221" time="161.27" type="appl" />
         <tli id="T222" time="161.81" type="appl" />
         <tli id="T223" time="162.35" type="appl" />
         <tli id="T224" time="162.89" type="appl" />
         <tli id="T225" time="163.43" type="appl" />
         <tli id="T226" time="163.97" type="appl" />
         <tli id="T227" time="164.51" type="appl" />
         <tli id="T228" time="165.313" type="appl" />
         <tli id="T229" time="165.906" type="appl" />
         <tli id="T230" time="166.499" type="appl" />
         <tli id="T231" time="167.091" type="appl" />
         <tli id="T232" time="167.684" type="appl" />
         <tli id="T233" time="168.277" type="appl" />
         <tli id="T234" time="168.83000660447948" />
         <tli id="T235" time="169.36" type="appl" />
         <tli id="T236" time="169.85" type="appl" />
         <tli id="T237" time="170.61" type="appl" />
         <tli id="T238" time="171.55054837662337" />
         <tli id="T239" time="172.81" type="appl" />
         <tli id="T240" time="173.96" type="appl" />
         <tli id="T241" time="175.11" type="appl" />
         <tli id="T242" time="176.320002199168" />
         <tli id="T243" time="176.775" type="appl" />
         <tli id="T244" time="177.285" type="appl" />
         <tli id="T245" time="177.795" type="appl" />
         <tli id="T246" time="178.752" type="appl" />
         <tli id="T247" time="179.513" type="appl" />
         <tli id="T248" time="180.275" type="appl" />
         <tli id="T249" time="181.037" type="appl" />
         <tli id="T250" time="181.798" type="appl" />
         <tli id="T251" time="184.4636720779221" />
         <tli id="T252" time="185.489" type="appl" />
         <tli id="T253" time="186.657" type="appl" />
         <tli id="T254" time="187.826" type="appl" />
         <tli id="T255" time="188.994" type="appl" />
         <tli id="T256" time="190.163" type="appl" />
         <tli id="T257" time="191.331" type="appl" />
         <tli id="T258" time="192.5" type="appl" />
         <tli id="T259" time="193.432" type="appl" />
         <tli id="T260" time="194.054" type="appl" />
         <tli id="T261" time="194.676" type="appl" />
         <tli id="T262" time="195.298" type="appl" />
         <tli id="T263" time="195.92" type="appl" />
         <tli id="T264" time="196.59" type="appl" />
         <tli id="T265" time="197.2" type="appl" />
         <tli id="T266" time="197.81" type="appl" />
         <tli id="T267" time="198.42" type="appl" />
         <tli id="T268" time="199.03" type="appl" />
         <tli id="T269" time="199.64" type="appl" />
         <tli id="T270" time="200.5634107142857" />
         <tli id="T271" time="201.143" type="appl" />
         <tli id="T272" time="201.667" type="appl" />
         <tli id="T273" time="202.19" type="appl" />
         <tli id="T274" time="202.713" type="appl" />
         <tli id="T275" time="203.237" type="appl" />
         <tli id="T276" time="203.76" type="appl" />
         <tli id="T277" time="204.283" type="appl" />
         <tli id="T278" time="204.807" type="appl" />
         <tli id="T279" time="205.33" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T230" start="T229">
            <tli id="T229.tx.1" />
         </timeline-fork>
         <timeline-fork end="T261" start="T260">
            <tli id="T260.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T279" id="Seg_0" n="sc" s="T0">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Onʼiʔ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">büzʼe</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">amnobi</ts>
                  <nts id="Seg_11" n="HIAT:ip">,</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T3">dĭn</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_17" n="HIAT:w" s="T4">nüke</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">külambi</ts>
                  <nts id="Seg_21" n="HIAT:ip">.</nts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_24" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">Dĭ</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">ibi</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">baška</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">nüke</ts>
                  <nts id="Seg_36" n="HIAT:ip">.</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_39" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">Dĭn</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">nüken</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">koʔbdo</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">ibi</ts>
                  <nts id="Seg_51" n="HIAT:ip">,</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_54" n="HIAT:w" s="T14">i</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_57" n="HIAT:w" s="T15">dĭ</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_60" n="HIAT:w" s="T16">büzʼen</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_63" n="HIAT:w" s="T17">koʔbdo</ts>
                  <nts id="Seg_64" n="HIAT:ip">.</nts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_67" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_69" n="HIAT:w" s="T18">Dĭ</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_72" n="HIAT:w" s="T19">büzʼen</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_75" n="HIAT:w" s="T20">koʔbdot</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_78" n="HIAT:w" s="T21">bar</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_81" n="HIAT:w" s="T22">ĭmbi</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_83" n="HIAT:ip">(</nts>
                  <ts e="T24" id="Seg_85" n="HIAT:w" s="T23">togonor-</ts>
                  <nts id="Seg_86" n="HIAT:ip">)</nts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_89" n="HIAT:w" s="T24">togonorleʔpi</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_92" n="HIAT:w" s="T25">ĭmbi</ts>
                  <nts id="Seg_93" n="HIAT:ip">.</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_96" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">A</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">dĭʔnə</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">üge</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_107" n="HIAT:w" s="T29">ej</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_110" n="HIAT:w" s="T30">jakše</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_113" n="HIAT:w" s="T31">ibi</ts>
                  <nts id="Seg_114" n="HIAT:ip">,</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_117" n="HIAT:w" s="T32">măndə:</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_119" n="HIAT:ip">"</nts>
                  <ts e="T34" id="Seg_121" n="HIAT:w" s="T33">Tăn</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_124" n="HIAT:w" s="T34">ugandə</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_127" n="HIAT:w" s="T35">ɨrɨ</ts>
                  <nts id="Seg_128" n="HIAT:ip">,</nts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_131" n="HIAT:w" s="T36">ej</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_134" n="HIAT:w" s="T37">togonorial</ts>
                  <nts id="Seg_135" n="HIAT:ip">"</nts>
                  <nts id="Seg_136" n="HIAT:ip">.</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_139" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_141" n="HIAT:w" s="T38">Dĭgəttə</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_144" n="HIAT:w" s="T39">dĭ</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_147" n="HIAT:w" s="T40">büzʼenə</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_150" n="HIAT:w" s="T41">măndə:</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_152" n="HIAT:ip">"</nts>
                  <ts e="T43" id="Seg_154" n="HIAT:w" s="T42">Kondə</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_157" n="HIAT:w" s="T43">boslə</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_160" n="HIAT:w" s="T44">koʔbdo</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_163" n="HIAT:w" s="T45">dʼijenə</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_166" n="HIAT:w" s="T46">i</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_169" n="HIAT:w" s="T47">dĭn</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_171" n="HIAT:ip">(</nts>
                  <ts e="T49" id="Seg_173" n="HIAT:w" s="T48">amn-</ts>
                  <nts id="Seg_174" n="HIAT:ip">)</nts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_177" n="HIAT:w" s="T49">bar</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_180" n="HIAT:w" s="T50">dĭn</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_183" n="HIAT:w" s="T51">amnoldə</ts>
                  <nts id="Seg_184" n="HIAT:ip">"</nts>
                  <nts id="Seg_185" n="HIAT:ip">.</nts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_188" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_190" n="HIAT:w" s="T52">Dĭ</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_193" n="HIAT:w" s="T53">konbi</ts>
                  <nts id="Seg_194" n="HIAT:ip">,</nts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_197" n="HIAT:w" s="T54">dĭn</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_200" n="HIAT:w" s="T55">amnolbi</ts>
                  <nts id="Seg_201" n="HIAT:ip">.</nts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_204" n="HIAT:u" s="T56">
                  <nts id="Seg_205" n="HIAT:ip">(</nts>
                  <ts e="T57" id="Seg_207" n="HIAT:w" s="T56">Dĭ=</ts>
                  <nts id="Seg_208" n="HIAT:ip">)</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_211" n="HIAT:w" s="T57">Dĭgəttə</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_214" n="HIAT:w" s="T58">dĭ</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_217" n="HIAT:w" s="T59">tura</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_220" n="HIAT:w" s="T60">dĭn</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_223" n="HIAT:w" s="T61">kubi</ts>
                  <nts id="Seg_224" n="HIAT:ip">,</nts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_227" n="HIAT:w" s="T62">šobi</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_230" n="HIAT:w" s="T63">turanə</ts>
                  <nts id="Seg_231" n="HIAT:ip">.</nts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_234" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_236" n="HIAT:w" s="T64">Tumo:</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_238" n="HIAT:ip">"</nts>
                  <ts e="T66" id="Seg_240" n="HIAT:w" s="T65">Deʔ</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_243" n="HIAT:w" s="T66">măna</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_246" n="HIAT:w" s="T67">ipek</ts>
                  <nts id="Seg_247" n="HIAT:ip">.</nts>
                  <nts id="Seg_248" n="HIAT:ip">"</nts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_251" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_253" n="HIAT:w" s="T68">Dĭ</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_256" n="HIAT:w" s="T69">dĭʔnə</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_259" n="HIAT:w" s="T70">mĭmbi</ts>
                  <nts id="Seg_260" n="HIAT:ip">.</nts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_263" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_265" n="HIAT:w" s="T71">Dĭgəttə</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_268" n="HIAT:w" s="T72">kuza</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_271" n="HIAT:w" s="T73">šobi</ts>
                  <nts id="Seg_272" n="HIAT:ip">,</nts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_275" n="HIAT:w" s="T74">nüdʼi</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_278" n="HIAT:w" s="T75">molambi</ts>
                  <nts id="Seg_279" n="HIAT:ip">.</nts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_282" n="HIAT:u" s="T76">
                  <nts id="Seg_283" n="HIAT:ip">(</nts>
                  <ts e="T77" id="Seg_285" n="HIAT:w" s="T76">Nünnə</ts>
                  <nts id="Seg_286" n="HIAT:ip">)</nts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_289" n="HIAT:w" s="T77">dĭʔnə</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_292" n="HIAT:w" s="T78">koŋgoro</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_295" n="HIAT:w" s="T79">mĭbi</ts>
                  <nts id="Seg_296" n="HIAT:ip">.</nts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_299" n="HIAT:u" s="T80">
                  <nts id="Seg_300" n="HIAT:ip">"</nts>
                  <ts e="T81" id="Seg_302" n="HIAT:w" s="T80">Tăn</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_305" n="HIAT:w" s="T81">nuʔməleʔ</ts>
                  <nts id="Seg_306" n="HIAT:ip">,</nts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_309" n="HIAT:w" s="T82">a</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_312" n="HIAT:w" s="T83">măn</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_315" n="HIAT:w" s="T84">tănan</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_317" n="HIAT:ip">(</nts>
                  <ts e="T86" id="Seg_319" n="HIAT:w" s="T85">dʼap-</ts>
                  <nts id="Seg_320" n="HIAT:ip">)</nts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_323" n="HIAT:w" s="T86">dʼabəlam</ts>
                  <nts id="Seg_324" n="HIAT:ip">"</nts>
                  <nts id="Seg_325" n="HIAT:ip">.</nts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_328" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_330" n="HIAT:w" s="T87">Dĭgəttə</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_333" n="HIAT:w" s="T88">tumo</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_336" n="HIAT:w" s="T89">šobi</ts>
                  <nts id="Seg_337" n="HIAT:ip">,</nts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_340" n="HIAT:w" s="T90">koŋgoro</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_343" n="HIAT:w" s="T91">ibi</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_346" n="HIAT:w" s="T92">i</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_349" n="HIAT:w" s="T93">turagən</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_352" n="HIAT:w" s="T94">bar</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_355" n="HIAT:w" s="T95">nuʔməleʔpi</ts>
                  <nts id="Seg_356" n="HIAT:ip">.</nts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_359" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_361" n="HIAT:w" s="T96">Dĭ</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_363" n="HIAT:ip">(</nts>
                  <ts e="T98" id="Seg_365" n="HIAT:w" s="T97">bəl</ts>
                  <nts id="Seg_366" n="HIAT:ip">)</nts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_369" n="HIAT:w" s="T98">dĭm</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_372" n="HIAT:w" s="T99">dʼabəluʔpi</ts>
                  <nts id="Seg_373" n="HIAT:ip">,</nts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_376" n="HIAT:w" s="T100">dʼabəluʔpi</ts>
                  <nts id="Seg_377" n="HIAT:ip">,</nts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_380" n="HIAT:w" s="T101">ej</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_382" n="HIAT:ip">(</nts>
                  <ts e="T103" id="Seg_384" n="HIAT:w" s="T102">mom-</ts>
                  <nts id="Seg_385" n="HIAT:ip">)</nts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_388" n="HIAT:w" s="T103">mobi</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_391" n="HIAT:w" s="T104">dʼabəsʼtə</ts>
                  <nts id="Seg_392" n="HIAT:ip">.</nts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_395" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_397" n="HIAT:w" s="T105">Koʔbdo</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_400" n="HIAT:w" s="T106">saʔlambi</ts>
                  <nts id="Seg_401" n="HIAT:ip">.</nts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T111" id="Seg_404" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_406" n="HIAT:w" s="T107">Ertə</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_409" n="HIAT:w" s="T108">molambi</ts>
                  <nts id="Seg_410" n="HIAT:ip">,</nts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_413" n="HIAT:w" s="T109">dĭ</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_416" n="HIAT:w" s="T110">uʔbdəbi</ts>
                  <nts id="Seg_417" n="HIAT:ip">.</nts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_420" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_422" n="HIAT:w" s="T111">Dĭʔnə</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_425" n="HIAT:w" s="T112">mĭmbi</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_428" n="HIAT:w" s="T113">aktʼa</ts>
                  <nts id="Seg_429" n="HIAT:ip">,</nts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_432" n="HIAT:w" s="T114">oldʼa</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_435" n="HIAT:w" s="T115">iʔgö</ts>
                  <nts id="Seg_436" n="HIAT:ip">.</nts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_439" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_441" n="HIAT:w" s="T116">Jamaʔi</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_444" n="HIAT:w" s="T117">mĭbi</ts>
                  <nts id="Seg_445" n="HIAT:ip">,</nts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_448" n="HIAT:w" s="T118">plat</ts>
                  <nts id="Seg_449" n="HIAT:ip">,</nts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_452" n="HIAT:w" s="T119">iʔgö</ts>
                  <nts id="Seg_453" n="HIAT:ip">.</nts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_456" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_458" n="HIAT:w" s="T120">Dĭgəttə</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_461" n="HIAT:w" s="T121">dĭ</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_464" n="HIAT:w" s="T122">nüke:</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_466" n="HIAT:ip">"</nts>
                  <ts e="T124" id="Seg_468" n="HIAT:w" s="T123">Kanaʔ</ts>
                  <nts id="Seg_469" n="HIAT:ip">,</nts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_472" n="HIAT:w" s="T124">koʔbdol</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_475" n="HIAT:w" s="T125">dĭn</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_477" n="HIAT:ip">(</nts>
                  <ts e="T127" id="Seg_479" n="HIAT:w" s="T126">kostăčkaʔi</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_482" n="HIAT:w" s="T127">iʔ</ts>
                  <nts id="Seg_483" n="HIAT:ip">)</nts>
                  <nts id="Seg_484" n="HIAT:ip">.</nts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_487" n="HIAT:u" s="T128">
                  <ts e="T129" id="Seg_489" n="HIAT:w" s="T128">Oʔbdəlil</ts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_492" n="HIAT:w" s="T129">də</ts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_495" n="HIAT:w" s="T130">deʔtə</ts>
                  <nts id="Seg_496" n="HIAT:ip">.</nts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_499" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_501" n="HIAT:w" s="T131">Dʼünə</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_504" n="HIAT:w" s="T132">tĭlləbeʔ</ts>
                  <nts id="Seg_505" n="HIAT:ip">"</nts>
                  <nts id="Seg_506" n="HIAT:ip">.</nts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T135" id="Seg_509" n="HIAT:u" s="T133">
                  <ts e="T134" id="Seg_511" n="HIAT:w" s="T133">Dĭ</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_514" n="HIAT:w" s="T134">kambi</ts>
                  <nts id="Seg_515" n="HIAT:ip">.</nts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T142" id="Seg_518" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_520" n="HIAT:w" s="T135">Šobi</ts>
                  <nts id="Seg_521" n="HIAT:ip">,</nts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_524" n="HIAT:w" s="T136">koʔbdon</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_527" n="HIAT:w" s="T137">bar</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_530" n="HIAT:w" s="T138">aktʼa</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_533" n="HIAT:w" s="T139">iʔgö</ts>
                  <nts id="Seg_534" n="HIAT:ip">,</nts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_537" n="HIAT:w" s="T140">oldʼa</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_540" n="HIAT:w" s="T141">iʔgö</ts>
                  <nts id="Seg_541" n="HIAT:ip">.</nts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T147" id="Seg_544" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_546" n="HIAT:w" s="T142">Dĭm</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_549" n="HIAT:w" s="T143">ibi</ts>
                  <nts id="Seg_550" n="HIAT:ip">,</nts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_553" n="HIAT:w" s="T144">amnolbi</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_556" n="HIAT:w" s="T145">inenə</ts>
                  <nts id="Seg_557" n="HIAT:ip">,</nts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_560" n="HIAT:w" s="T146">šonəga</ts>
                  <nts id="Seg_561" n="HIAT:ip">.</nts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_564" n="HIAT:u" s="T147">
                  <ts e="T148" id="Seg_566" n="HIAT:w" s="T147">A</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_569" n="HIAT:w" s="T148">men</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_572" n="HIAT:w" s="T149">bar</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_575" n="HIAT:w" s="T150">kirgarlaʔbə:</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_577" n="HIAT:ip">"</nts>
                  <nts id="Seg_578" n="HIAT:ip">(</nts>
                  <ts e="T152" id="Seg_580" n="HIAT:w" s="T151">Büzʼe=</ts>
                  <nts id="Seg_581" n="HIAT:ip">)</nts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_584" n="HIAT:w" s="T152">Büzʼen</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_587" n="HIAT:w" s="T153">koʔbdo</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_590" n="HIAT:w" s="T154">šonəga</ts>
                  <nts id="Seg_591" n="HIAT:ip">,</nts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_594" n="HIAT:w" s="T155">aktʼa</ts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_597" n="HIAT:w" s="T156">detləge</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_600" n="HIAT:w" s="T157">i</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_603" n="HIAT:w" s="T158">oldʼa</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_606" n="HIAT:w" s="T159">iʔgö</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_609" n="HIAT:w" s="T160">detləge</ts>
                  <nts id="Seg_610" n="HIAT:ip">"</nts>
                  <nts id="Seg_611" n="HIAT:ip">.</nts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_614" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_616" n="HIAT:w" s="T161">A</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_619" n="HIAT:w" s="T162">dĭ</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_622" n="HIAT:w" s="T163">nüke</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_625" n="HIAT:w" s="T164">măndə:</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_627" n="HIAT:ip">"</nts>
                  <ts e="T166" id="Seg_629" n="HIAT:w" s="T165">Iʔ</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_632" n="HIAT:w" s="T166">šamaʔ</ts>
                  <nts id="Seg_633" n="HIAT:ip">!</nts>
                  <nts id="Seg_634" n="HIAT:ip">"</nts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T177" id="Seg_637" n="HIAT:u" s="T167">
                  <ts e="T168" id="Seg_639" n="HIAT:w" s="T167">Dĭgəttə</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_642" n="HIAT:w" s="T168">büzʼe</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_645" n="HIAT:w" s="T169">šobi</ts>
                  <nts id="Seg_646" n="HIAT:ip">,</nts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_649" n="HIAT:w" s="T170">iʔgö</ts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_652" n="HIAT:w" s="T171">deʔpi</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_655" n="HIAT:w" s="T172">i</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_658" n="HIAT:w" s="T173">aktʼa</ts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_661" n="HIAT:w" s="T174">i</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_664" n="HIAT:w" s="T175">koʔbdobə</ts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_667" n="HIAT:w" s="T176">deʔpi</ts>
                  <nts id="Seg_668" n="HIAT:ip">.</nts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_671" n="HIAT:u" s="T177">
                  <ts e="T178" id="Seg_673" n="HIAT:w" s="T177">Dĭgəttə</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_676" n="HIAT:w" s="T178">măndə:</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_678" n="HIAT:ip">"</nts>
                  <ts e="T180" id="Seg_680" n="HIAT:w" s="T179">Kundə</ts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_683" n="HIAT:w" s="T180">măn</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_686" n="HIAT:w" s="T181">koʔbdom</ts>
                  <nts id="Seg_687" n="HIAT:ip">,</nts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_690" n="HIAT:w" s="T182">dĭn</ts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_693" n="HIAT:w" s="T183">amnoldə</ts>
                  <nts id="Seg_694" n="HIAT:ip">"</nts>
                  <nts id="Seg_695" n="HIAT:ip">.</nts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_698" n="HIAT:u" s="T184">
                  <ts e="T185" id="Seg_700" n="HIAT:w" s="T184">Dĭ</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_703" n="HIAT:w" s="T185">kombi</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_706" n="HIAT:w" s="T186">dĭ</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_709" n="HIAT:w" s="T187">koʔbdom</ts>
                  <nts id="Seg_710" n="HIAT:ip">.</nts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T193" id="Seg_713" n="HIAT:u" s="T188">
                  <ts e="T189" id="Seg_715" n="HIAT:w" s="T188">Tomo</ts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_717" n="HIAT:ip">(</nts>
                  <ts e="T190" id="Seg_719" n="HIAT:w" s="T189">piebi</ts>
                  <nts id="Seg_720" n="HIAT:ip">)</nts>
                  <nts id="Seg_721" n="HIAT:ip">,</nts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_723" n="HIAT:ip">(</nts>
                  <ts e="T191" id="Seg_725" n="HIAT:w" s="T190">ipekdə</ts>
                  <nts id="Seg_726" n="HIAT:ip">)</nts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_729" n="HIAT:w" s="T191">ej</ts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_732" n="HIAT:w" s="T192">mĭbi</ts>
                  <nts id="Seg_733" n="HIAT:ip">.</nts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T200" id="Seg_736" n="HIAT:u" s="T193">
                  <ts e="T194" id="Seg_738" n="HIAT:w" s="T193">Dĭgəttə</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_741" n="HIAT:w" s="T194">nüdʼin</ts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_744" n="HIAT:w" s="T195">kubi</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_747" n="HIAT:w" s="T196">tura</ts>
                  <nts id="Seg_748" n="HIAT:ip">,</nts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_751" n="HIAT:w" s="T197">turanə</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_753" n="HIAT:ip">(</nts>
                  <ts e="T199" id="Seg_755" n="HIAT:w" s="T198">šo-</ts>
                  <nts id="Seg_756" n="HIAT:ip">)</nts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_759" n="HIAT:w" s="T199">šobi</ts>
                  <nts id="Seg_760" n="HIAT:ip">.</nts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T211" id="Seg_763" n="HIAT:u" s="T200">
                  <ts e="T201" id="Seg_765" n="HIAT:w" s="T200">Dĭn</ts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_768" n="HIAT:w" s="T201">kuza</ts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_771" n="HIAT:w" s="T202">măndə:</ts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_773" n="HIAT:ip">"</nts>
                  <ts e="T204" id="Seg_775" n="HIAT:w" s="T203">Teinen</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_778" n="HIAT:w" s="T204">nüdʼin</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_781" n="HIAT:w" s="T205">koŋgoro</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_783" n="HIAT:ip">(</nts>
                  <ts e="T207" id="Seg_785" n="HIAT:w" s="T206">mubin</ts>
                  <nts id="Seg_786" n="HIAT:ip">)</nts>
                  <nts id="Seg_787" n="HIAT:ip">,</nts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_789" n="HIAT:ip">(</nts>
                  <ts e="T208" id="Seg_791" n="HIAT:w" s="T207">tonoʔ</ts>
                  <nts id="Seg_792" n="HIAT:ip">)</nts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_795" n="HIAT:w" s="T208">măn</ts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_798" n="HIAT:w" s="T209">tănan</ts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_801" n="HIAT:w" s="T210">dʼabəlim</ts>
                  <nts id="Seg_802" n="HIAT:ip">"</nts>
                  <nts id="Seg_803" n="HIAT:ip">.</nts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T215" id="Seg_806" n="HIAT:u" s="T211">
                  <ts e="T212" id="Seg_808" n="HIAT:w" s="T211">A</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_811" n="HIAT:w" s="T212">tumo</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_814" n="HIAT:w" s="T213">ej</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_817" n="HIAT:w" s="T214">šobi</ts>
                  <nts id="Seg_818" n="HIAT:ip">.</nts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T227" id="Seg_821" n="HIAT:u" s="T215">
                  <ts e="T216" id="Seg_823" n="HIAT:w" s="T215">Dĭ</ts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_826" n="HIAT:w" s="T216">koŋgoro</ts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_829" n="HIAT:w" s="T217">ibi</ts>
                  <nts id="Seg_830" n="HIAT:ip">,</nts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_833" n="HIAT:w" s="T218">nuʔməluʔpi</ts>
                  <nts id="Seg_834" n="HIAT:ip">,</nts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_837" n="HIAT:w" s="T219">nuʔməluʔpi</ts>
                  <nts id="Seg_838" n="HIAT:ip">,</nts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_841" n="HIAT:w" s="T220">dĭ</ts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_844" n="HIAT:w" s="T221">dĭm</ts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_847" n="HIAT:w" s="T222">dʼaʔpi</ts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_850" n="HIAT:w" s="T223">da</ts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_853" n="HIAT:w" s="T224">bar</ts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_856" n="HIAT:w" s="T225">amnuʔpi</ts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_859" n="HIAT:w" s="T226">bar</ts>
                  <nts id="Seg_860" n="HIAT:ip">.</nts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T234" id="Seg_863" n="HIAT:u" s="T227">
                  <ts e="T228" id="Seg_865" n="HIAT:w" s="T227">Dĭgəttə</ts>
                  <nts id="Seg_866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_867" n="HIAT:ip">(</nts>
                  <ts e="T229" id="Seg_869" n="HIAT:w" s="T228">bü-</ts>
                  <nts id="Seg_870" n="HIAT:ip">)</nts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_872" n="HIAT:ip">(</nts>
                  <ts e="T229.tx.1" id="Seg_874" n="HIAT:w" s="T229">măndə</ts>
                  <nts id="Seg_875" n="HIAT:ip">)</nts>
                  <ts e="T230" id="Seg_877" n="HIAT:w" s="T229.tx.1">:</ts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_879" n="HIAT:ip">"</nts>
                  <ts e="T231" id="Seg_881" n="HIAT:w" s="T230">Kanaʔ</ts>
                  <nts id="Seg_882" n="HIAT:ip">,</nts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_885" n="HIAT:w" s="T231">măn</ts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_888" n="HIAT:w" s="T232">koʔbdo</ts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_891" n="HIAT:w" s="T233">ile</ts>
                  <nts id="Seg_892" n="HIAT:ip">!</nts>
                  <nts id="Seg_893" n="HIAT:ip">"</nts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T236" id="Seg_896" n="HIAT:u" s="T234">
                  <ts e="T235" id="Seg_898" n="HIAT:w" s="T234">Dĭ</ts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_901" n="HIAT:w" s="T235">kambi</ts>
                  <nts id="Seg_902" n="HIAT:ip">.</nts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T238" id="Seg_905" n="HIAT:u" s="T236">
                  <nts id="Seg_906" n="HIAT:ip">(</nts>
                  <ts e="T237" id="Seg_908" n="HIAT:w" s="T236">Bura</ts>
                  <nts id="Seg_909" n="HIAT:ip">)</nts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_912" n="HIAT:w" s="T237">ibi</ts>
                  <nts id="Seg_913" n="HIAT:ip">.</nts>
                  <nts id="Seg_914" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T242" id="Seg_916" n="HIAT:u" s="T238">
                  <ts e="T239" id="Seg_918" n="HIAT:w" s="T238">Šobi</ts>
                  <nts id="Seg_919" n="HIAT:ip">,</nts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_922" n="HIAT:w" s="T239">onʼiʔ</ts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_925" n="HIAT:w" s="T240">kostăčkaʔi</ts>
                  <nts id="Seg_926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_928" n="HIAT:w" s="T241">iʔbəlaʔbəʔjə</ts>
                  <nts id="Seg_929" n="HIAT:ip">.</nts>
                  <nts id="Seg_930" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T245" id="Seg_932" n="HIAT:u" s="T242">
                  <ts e="T243" id="Seg_934" n="HIAT:w" s="T242">Oʔbdəbi</ts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_937" n="HIAT:w" s="T243">i</ts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_940" n="HIAT:w" s="T244">deʔpi</ts>
                  <nts id="Seg_941" n="HIAT:ip">.</nts>
                  <nts id="Seg_942" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T258" id="Seg_944" n="HIAT:u" s="T245">
                  <ts e="T246" id="Seg_946" n="HIAT:w" s="T245">Dĭgəttə</ts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_948" n="HIAT:ip">(</nts>
                  <ts e="T247" id="Seg_950" n="HIAT:w" s="T246">man-</ts>
                  <nts id="Seg_951" n="HIAT:ip">)</nts>
                  <nts id="Seg_952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_954" n="HIAT:w" s="T247">men</ts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_957" n="HIAT:w" s="T248">bazoʔ</ts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_959" n="HIAT:ip">(</nts>
                  <ts e="T250" id="Seg_961" n="HIAT:w" s="T249">kürüm-</ts>
                  <nts id="Seg_962" n="HIAT:ip">)</nts>
                  <nts id="Seg_963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_965" n="HIAT:w" s="T250">kürümnaʔbə:</ts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_967" n="HIAT:ip">"</nts>
                  <ts e="T252" id="Seg_969" n="HIAT:w" s="T251">Nüken</ts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_972" n="HIAT:w" s="T252">koʔbdot</ts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_975" n="HIAT:w" s="T253">bar</ts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_978" n="HIAT:w" s="T254">kostăčkaʔi</ts>
                  <nts id="Seg_979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_980" n="HIAT:ip">(</nts>
                  <ts e="T256" id="Seg_982" n="HIAT:w" s="T255">ko-</ts>
                  <nts id="Seg_983" n="HIAT:ip">)</nts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_986" n="HIAT:w" s="T256">detleʔbə</ts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_989" n="HIAT:w" s="T257">büzʼe</ts>
                  <nts id="Seg_990" n="HIAT:ip">"</nts>
                  <nts id="Seg_991" n="HIAT:ip">.</nts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T263" id="Seg_994" n="HIAT:u" s="T258">
                  <nts id="Seg_995" n="HIAT:ip">"</nts>
                  <ts e="T259" id="Seg_997" n="HIAT:w" s="T258">Iʔ</ts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_1000" n="HIAT:w" s="T259">šamaʔ</ts>
                  <nts id="Seg_1001" n="HIAT:ip">,</nts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260.tx.1" id="Seg_1004" n="HIAT:w" s="T260">a</ts>
                  <nts id="Seg_1005" n="HIAT:ip">_</nts>
                  <ts e="T261" id="Seg_1007" n="HIAT:w" s="T260.tx.1">to</ts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1010" n="HIAT:w" s="T261">münörləl</ts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1013" n="HIAT:w" s="T262">tănan</ts>
                  <nts id="Seg_1014" n="HIAT:ip">"</nts>
                  <nts id="Seg_1015" n="HIAT:ip">.</nts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T270" id="Seg_1018" n="HIAT:u" s="T263">
                  <ts e="T264" id="Seg_1020" n="HIAT:w" s="T263">Dĭgəttə</ts>
                  <nts id="Seg_1021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_1023" n="HIAT:w" s="T264">büzʼe</ts>
                  <nts id="Seg_1024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1025" n="HIAT:ip">(</nts>
                  <ts e="T266" id="Seg_1027" n="HIAT:w" s="T265">s-</ts>
                  <nts id="Seg_1028" n="HIAT:ip">)</nts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1031" n="HIAT:w" s="T266">šobi</ts>
                  <nts id="Seg_1032" n="HIAT:ip">,</nts>
                  <nts id="Seg_1033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1035" n="HIAT:w" s="T267">onʼiʔ</ts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1038" n="HIAT:w" s="T268">kostăčkaʔi</ts>
                  <nts id="Seg_1039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1041" n="HIAT:w" s="T269">deʔpi</ts>
                  <nts id="Seg_1042" n="HIAT:ip">.</nts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T279" id="Seg_1045" n="HIAT:u" s="T270">
                  <ts e="T271" id="Seg_1047" n="HIAT:w" s="T270">I</ts>
                  <nts id="Seg_1048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1049" n="HIAT:ip">(</nts>
                  <ts e="T272" id="Seg_1051" n="HIAT:w" s="T271">dĭ-</ts>
                  <nts id="Seg_1052" n="HIAT:ip">)</nts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1055" n="HIAT:w" s="T272">dĭ</ts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1058" n="HIAT:w" s="T273">koʔbdon</ts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1060" n="HIAT:ip">(</nts>
                  <ts e="T275" id="Seg_1062" n="HIAT:w" s="T274">dʼü-</ts>
                  <nts id="Seg_1063" n="HIAT:ip">)</nts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1066" n="HIAT:w" s="T275">dʼünə</ts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1069" n="HIAT:w" s="T276">embiʔi</ts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1072" n="HIAT:w" s="T277">i</ts>
                  <nts id="Seg_1073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1075" n="HIAT:w" s="T278">tĭlbiʔi</ts>
                  <nts id="Seg_1076" n="HIAT:ip">.</nts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T279" id="Seg_1078" n="sc" s="T0">
               <ts e="T1" id="Seg_1080" n="e" s="T0">Onʼiʔ </ts>
               <ts e="T2" id="Seg_1082" n="e" s="T1">büzʼe </ts>
               <ts e="T3" id="Seg_1084" n="e" s="T2">amnobi, </ts>
               <ts e="T4" id="Seg_1086" n="e" s="T3">dĭn </ts>
               <ts e="T5" id="Seg_1088" n="e" s="T4">nüke </ts>
               <ts e="T6" id="Seg_1090" n="e" s="T5">külambi. </ts>
               <ts e="T7" id="Seg_1092" n="e" s="T6">Dĭ </ts>
               <ts e="T8" id="Seg_1094" n="e" s="T7">ibi </ts>
               <ts e="T9" id="Seg_1096" n="e" s="T8">baška </ts>
               <ts e="T10" id="Seg_1098" n="e" s="T9">nüke. </ts>
               <ts e="T11" id="Seg_1100" n="e" s="T10">Dĭn </ts>
               <ts e="T12" id="Seg_1102" n="e" s="T11">nüken </ts>
               <ts e="T13" id="Seg_1104" n="e" s="T12">koʔbdo </ts>
               <ts e="T14" id="Seg_1106" n="e" s="T13">ibi, </ts>
               <ts e="T15" id="Seg_1108" n="e" s="T14">i </ts>
               <ts e="T16" id="Seg_1110" n="e" s="T15">dĭ </ts>
               <ts e="T17" id="Seg_1112" n="e" s="T16">büzʼen </ts>
               <ts e="T18" id="Seg_1114" n="e" s="T17">koʔbdo. </ts>
               <ts e="T19" id="Seg_1116" n="e" s="T18">Dĭ </ts>
               <ts e="T20" id="Seg_1118" n="e" s="T19">büzʼen </ts>
               <ts e="T21" id="Seg_1120" n="e" s="T20">koʔbdot </ts>
               <ts e="T22" id="Seg_1122" n="e" s="T21">bar </ts>
               <ts e="T23" id="Seg_1124" n="e" s="T22">ĭmbi </ts>
               <ts e="T24" id="Seg_1126" n="e" s="T23">(togonor-) </ts>
               <ts e="T25" id="Seg_1128" n="e" s="T24">togonorleʔpi </ts>
               <ts e="T26" id="Seg_1130" n="e" s="T25">ĭmbi. </ts>
               <ts e="T27" id="Seg_1132" n="e" s="T26">A </ts>
               <ts e="T28" id="Seg_1134" n="e" s="T27">dĭʔnə </ts>
               <ts e="T29" id="Seg_1136" n="e" s="T28">üge </ts>
               <ts e="T30" id="Seg_1138" n="e" s="T29">ej </ts>
               <ts e="T31" id="Seg_1140" n="e" s="T30">jakše </ts>
               <ts e="T32" id="Seg_1142" n="e" s="T31">ibi, </ts>
               <ts e="T33" id="Seg_1144" n="e" s="T32">măndə: </ts>
               <ts e="T34" id="Seg_1146" n="e" s="T33">"Tăn </ts>
               <ts e="T35" id="Seg_1148" n="e" s="T34">ugandə </ts>
               <ts e="T36" id="Seg_1150" n="e" s="T35">ɨrɨ, </ts>
               <ts e="T37" id="Seg_1152" n="e" s="T36">ej </ts>
               <ts e="T38" id="Seg_1154" n="e" s="T37">togonorial". </ts>
               <ts e="T39" id="Seg_1156" n="e" s="T38">Dĭgəttə </ts>
               <ts e="T40" id="Seg_1158" n="e" s="T39">dĭ </ts>
               <ts e="T41" id="Seg_1160" n="e" s="T40">büzʼenə </ts>
               <ts e="T42" id="Seg_1162" n="e" s="T41">măndə: </ts>
               <ts e="T43" id="Seg_1164" n="e" s="T42">"Kondə </ts>
               <ts e="T44" id="Seg_1166" n="e" s="T43">boslə </ts>
               <ts e="T45" id="Seg_1168" n="e" s="T44">koʔbdo </ts>
               <ts e="T46" id="Seg_1170" n="e" s="T45">dʼijenə </ts>
               <ts e="T47" id="Seg_1172" n="e" s="T46">i </ts>
               <ts e="T48" id="Seg_1174" n="e" s="T47">dĭn </ts>
               <ts e="T49" id="Seg_1176" n="e" s="T48">(amn-) </ts>
               <ts e="T50" id="Seg_1178" n="e" s="T49">bar </ts>
               <ts e="T51" id="Seg_1180" n="e" s="T50">dĭn </ts>
               <ts e="T52" id="Seg_1182" n="e" s="T51">amnoldə". </ts>
               <ts e="T53" id="Seg_1184" n="e" s="T52">Dĭ </ts>
               <ts e="T54" id="Seg_1186" n="e" s="T53">konbi, </ts>
               <ts e="T55" id="Seg_1188" n="e" s="T54">dĭn </ts>
               <ts e="T56" id="Seg_1190" n="e" s="T55">amnolbi. </ts>
               <ts e="T57" id="Seg_1192" n="e" s="T56">(Dĭ=) </ts>
               <ts e="T58" id="Seg_1194" n="e" s="T57">Dĭgəttə </ts>
               <ts e="T59" id="Seg_1196" n="e" s="T58">dĭ </ts>
               <ts e="T60" id="Seg_1198" n="e" s="T59">tura </ts>
               <ts e="T61" id="Seg_1200" n="e" s="T60">dĭn </ts>
               <ts e="T62" id="Seg_1202" n="e" s="T61">kubi, </ts>
               <ts e="T63" id="Seg_1204" n="e" s="T62">šobi </ts>
               <ts e="T64" id="Seg_1206" n="e" s="T63">turanə. </ts>
               <ts e="T65" id="Seg_1208" n="e" s="T64">Tumo: </ts>
               <ts e="T66" id="Seg_1210" n="e" s="T65">"Deʔ </ts>
               <ts e="T67" id="Seg_1212" n="e" s="T66">măna </ts>
               <ts e="T68" id="Seg_1214" n="e" s="T67">ipek." </ts>
               <ts e="T69" id="Seg_1216" n="e" s="T68">Dĭ </ts>
               <ts e="T70" id="Seg_1218" n="e" s="T69">dĭʔnə </ts>
               <ts e="T71" id="Seg_1220" n="e" s="T70">mĭmbi. </ts>
               <ts e="T72" id="Seg_1222" n="e" s="T71">Dĭgəttə </ts>
               <ts e="T73" id="Seg_1224" n="e" s="T72">kuza </ts>
               <ts e="T74" id="Seg_1226" n="e" s="T73">šobi, </ts>
               <ts e="T75" id="Seg_1228" n="e" s="T74">nüdʼi </ts>
               <ts e="T76" id="Seg_1230" n="e" s="T75">molambi. </ts>
               <ts e="T77" id="Seg_1232" n="e" s="T76">(Nünnə) </ts>
               <ts e="T78" id="Seg_1234" n="e" s="T77">dĭʔnə </ts>
               <ts e="T79" id="Seg_1236" n="e" s="T78">koŋgoro </ts>
               <ts e="T80" id="Seg_1238" n="e" s="T79">mĭbi. </ts>
               <ts e="T81" id="Seg_1240" n="e" s="T80">"Tăn </ts>
               <ts e="T82" id="Seg_1242" n="e" s="T81">nuʔməleʔ, </ts>
               <ts e="T83" id="Seg_1244" n="e" s="T82">a </ts>
               <ts e="T84" id="Seg_1246" n="e" s="T83">măn </ts>
               <ts e="T85" id="Seg_1248" n="e" s="T84">tănan </ts>
               <ts e="T86" id="Seg_1250" n="e" s="T85">(dʼap-) </ts>
               <ts e="T87" id="Seg_1252" n="e" s="T86">dʼabəlam". </ts>
               <ts e="T88" id="Seg_1254" n="e" s="T87">Dĭgəttə </ts>
               <ts e="T89" id="Seg_1256" n="e" s="T88">tumo </ts>
               <ts e="T90" id="Seg_1258" n="e" s="T89">šobi, </ts>
               <ts e="T91" id="Seg_1260" n="e" s="T90">koŋgoro </ts>
               <ts e="T92" id="Seg_1262" n="e" s="T91">ibi </ts>
               <ts e="T93" id="Seg_1264" n="e" s="T92">i </ts>
               <ts e="T94" id="Seg_1266" n="e" s="T93">turagən </ts>
               <ts e="T95" id="Seg_1268" n="e" s="T94">bar </ts>
               <ts e="T96" id="Seg_1270" n="e" s="T95">nuʔməleʔpi. </ts>
               <ts e="T97" id="Seg_1272" n="e" s="T96">Dĭ </ts>
               <ts e="T98" id="Seg_1274" n="e" s="T97">(bəl) </ts>
               <ts e="T99" id="Seg_1276" n="e" s="T98">dĭm </ts>
               <ts e="T100" id="Seg_1278" n="e" s="T99">dʼabəluʔpi, </ts>
               <ts e="T101" id="Seg_1280" n="e" s="T100">dʼabəluʔpi, </ts>
               <ts e="T102" id="Seg_1282" n="e" s="T101">ej </ts>
               <ts e="T103" id="Seg_1284" n="e" s="T102">(mom-) </ts>
               <ts e="T104" id="Seg_1286" n="e" s="T103">mobi </ts>
               <ts e="T105" id="Seg_1288" n="e" s="T104">dʼabəsʼtə. </ts>
               <ts e="T106" id="Seg_1290" n="e" s="T105">Koʔbdo </ts>
               <ts e="T107" id="Seg_1292" n="e" s="T106">saʔlambi. </ts>
               <ts e="T108" id="Seg_1294" n="e" s="T107">Ertə </ts>
               <ts e="T109" id="Seg_1296" n="e" s="T108">molambi, </ts>
               <ts e="T110" id="Seg_1298" n="e" s="T109">dĭ </ts>
               <ts e="T111" id="Seg_1300" n="e" s="T110">uʔbdəbi. </ts>
               <ts e="T112" id="Seg_1302" n="e" s="T111">Dĭʔnə </ts>
               <ts e="T113" id="Seg_1304" n="e" s="T112">mĭmbi </ts>
               <ts e="T114" id="Seg_1306" n="e" s="T113">aktʼa, </ts>
               <ts e="T115" id="Seg_1308" n="e" s="T114">oldʼa </ts>
               <ts e="T116" id="Seg_1310" n="e" s="T115">iʔgö. </ts>
               <ts e="T117" id="Seg_1312" n="e" s="T116">Jamaʔi </ts>
               <ts e="T118" id="Seg_1314" n="e" s="T117">mĭbi, </ts>
               <ts e="T119" id="Seg_1316" n="e" s="T118">plat, </ts>
               <ts e="T120" id="Seg_1318" n="e" s="T119">iʔgö. </ts>
               <ts e="T121" id="Seg_1320" n="e" s="T120">Dĭgəttə </ts>
               <ts e="T122" id="Seg_1322" n="e" s="T121">dĭ </ts>
               <ts e="T123" id="Seg_1324" n="e" s="T122">nüke: </ts>
               <ts e="T124" id="Seg_1326" n="e" s="T123">"Kanaʔ, </ts>
               <ts e="T125" id="Seg_1328" n="e" s="T124">koʔbdol </ts>
               <ts e="T126" id="Seg_1330" n="e" s="T125">dĭn </ts>
               <ts e="T127" id="Seg_1332" n="e" s="T126">(kostăčkaʔi </ts>
               <ts e="T128" id="Seg_1334" n="e" s="T127">iʔ). </ts>
               <ts e="T129" id="Seg_1336" n="e" s="T128">Oʔbdəlil </ts>
               <ts e="T130" id="Seg_1338" n="e" s="T129">də </ts>
               <ts e="T131" id="Seg_1340" n="e" s="T130">deʔtə. </ts>
               <ts e="T132" id="Seg_1342" n="e" s="T131">Dʼünə </ts>
               <ts e="T133" id="Seg_1344" n="e" s="T132">tĭlləbeʔ". </ts>
               <ts e="T134" id="Seg_1346" n="e" s="T133">Dĭ </ts>
               <ts e="T135" id="Seg_1348" n="e" s="T134">kambi. </ts>
               <ts e="T136" id="Seg_1350" n="e" s="T135">Šobi, </ts>
               <ts e="T137" id="Seg_1352" n="e" s="T136">koʔbdon </ts>
               <ts e="T138" id="Seg_1354" n="e" s="T137">bar </ts>
               <ts e="T139" id="Seg_1356" n="e" s="T138">aktʼa </ts>
               <ts e="T140" id="Seg_1358" n="e" s="T139">iʔgö, </ts>
               <ts e="T141" id="Seg_1360" n="e" s="T140">oldʼa </ts>
               <ts e="T142" id="Seg_1362" n="e" s="T141">iʔgö. </ts>
               <ts e="T143" id="Seg_1364" n="e" s="T142">Dĭm </ts>
               <ts e="T144" id="Seg_1366" n="e" s="T143">ibi, </ts>
               <ts e="T145" id="Seg_1368" n="e" s="T144">amnolbi </ts>
               <ts e="T146" id="Seg_1370" n="e" s="T145">inenə, </ts>
               <ts e="T147" id="Seg_1372" n="e" s="T146">šonəga. </ts>
               <ts e="T148" id="Seg_1374" n="e" s="T147">A </ts>
               <ts e="T149" id="Seg_1376" n="e" s="T148">men </ts>
               <ts e="T150" id="Seg_1378" n="e" s="T149">bar </ts>
               <ts e="T151" id="Seg_1380" n="e" s="T150">kirgarlaʔbə: </ts>
               <ts e="T152" id="Seg_1382" n="e" s="T151">"(Büzʼe=) </ts>
               <ts e="T153" id="Seg_1384" n="e" s="T152">Büzʼen </ts>
               <ts e="T154" id="Seg_1386" n="e" s="T153">koʔbdo </ts>
               <ts e="T155" id="Seg_1388" n="e" s="T154">šonəga, </ts>
               <ts e="T156" id="Seg_1390" n="e" s="T155">aktʼa </ts>
               <ts e="T157" id="Seg_1392" n="e" s="T156">detləge </ts>
               <ts e="T158" id="Seg_1394" n="e" s="T157">i </ts>
               <ts e="T159" id="Seg_1396" n="e" s="T158">oldʼa </ts>
               <ts e="T160" id="Seg_1398" n="e" s="T159">iʔgö </ts>
               <ts e="T161" id="Seg_1400" n="e" s="T160">detləge". </ts>
               <ts e="T162" id="Seg_1402" n="e" s="T161">A </ts>
               <ts e="T163" id="Seg_1404" n="e" s="T162">dĭ </ts>
               <ts e="T164" id="Seg_1406" n="e" s="T163">nüke </ts>
               <ts e="T165" id="Seg_1408" n="e" s="T164">măndə: </ts>
               <ts e="T166" id="Seg_1410" n="e" s="T165">"Iʔ </ts>
               <ts e="T167" id="Seg_1412" n="e" s="T166">šamaʔ!" </ts>
               <ts e="T168" id="Seg_1414" n="e" s="T167">Dĭgəttə </ts>
               <ts e="T169" id="Seg_1416" n="e" s="T168">büzʼe </ts>
               <ts e="T170" id="Seg_1418" n="e" s="T169">šobi, </ts>
               <ts e="T171" id="Seg_1420" n="e" s="T170">iʔgö </ts>
               <ts e="T172" id="Seg_1422" n="e" s="T171">deʔpi </ts>
               <ts e="T173" id="Seg_1424" n="e" s="T172">i </ts>
               <ts e="T174" id="Seg_1426" n="e" s="T173">aktʼa </ts>
               <ts e="T175" id="Seg_1428" n="e" s="T174">i </ts>
               <ts e="T176" id="Seg_1430" n="e" s="T175">koʔbdobə </ts>
               <ts e="T177" id="Seg_1432" n="e" s="T176">deʔpi. </ts>
               <ts e="T178" id="Seg_1434" n="e" s="T177">Dĭgəttə </ts>
               <ts e="T179" id="Seg_1436" n="e" s="T178">măndə: </ts>
               <ts e="T180" id="Seg_1438" n="e" s="T179">"Kundə </ts>
               <ts e="T181" id="Seg_1440" n="e" s="T180">măn </ts>
               <ts e="T182" id="Seg_1442" n="e" s="T181">koʔbdom, </ts>
               <ts e="T183" id="Seg_1444" n="e" s="T182">dĭn </ts>
               <ts e="T184" id="Seg_1446" n="e" s="T183">amnoldə". </ts>
               <ts e="T185" id="Seg_1448" n="e" s="T184">Dĭ </ts>
               <ts e="T186" id="Seg_1450" n="e" s="T185">kombi </ts>
               <ts e="T187" id="Seg_1452" n="e" s="T186">dĭ </ts>
               <ts e="T188" id="Seg_1454" n="e" s="T187">koʔbdom. </ts>
               <ts e="T189" id="Seg_1456" n="e" s="T188">Tomo </ts>
               <ts e="T190" id="Seg_1458" n="e" s="T189">(piebi), </ts>
               <ts e="T191" id="Seg_1460" n="e" s="T190">(ipekdə) </ts>
               <ts e="T192" id="Seg_1462" n="e" s="T191">ej </ts>
               <ts e="T193" id="Seg_1464" n="e" s="T192">mĭbi. </ts>
               <ts e="T194" id="Seg_1466" n="e" s="T193">Dĭgəttə </ts>
               <ts e="T195" id="Seg_1468" n="e" s="T194">nüdʼin </ts>
               <ts e="T196" id="Seg_1470" n="e" s="T195">kubi </ts>
               <ts e="T197" id="Seg_1472" n="e" s="T196">tura, </ts>
               <ts e="T198" id="Seg_1474" n="e" s="T197">turanə </ts>
               <ts e="T199" id="Seg_1476" n="e" s="T198">(šo-) </ts>
               <ts e="T200" id="Seg_1478" n="e" s="T199">šobi. </ts>
               <ts e="T201" id="Seg_1480" n="e" s="T200">Dĭn </ts>
               <ts e="T202" id="Seg_1482" n="e" s="T201">kuza </ts>
               <ts e="T203" id="Seg_1484" n="e" s="T202">măndə: </ts>
               <ts e="T204" id="Seg_1486" n="e" s="T203">"Teinen </ts>
               <ts e="T205" id="Seg_1488" n="e" s="T204">nüdʼin </ts>
               <ts e="T206" id="Seg_1490" n="e" s="T205">koŋgoro </ts>
               <ts e="T207" id="Seg_1492" n="e" s="T206">(mubin), </ts>
               <ts e="T208" id="Seg_1494" n="e" s="T207">(tonoʔ) </ts>
               <ts e="T209" id="Seg_1496" n="e" s="T208">măn </ts>
               <ts e="T210" id="Seg_1498" n="e" s="T209">tănan </ts>
               <ts e="T211" id="Seg_1500" n="e" s="T210">dʼabəlim". </ts>
               <ts e="T212" id="Seg_1502" n="e" s="T211">A </ts>
               <ts e="T213" id="Seg_1504" n="e" s="T212">tumo </ts>
               <ts e="T214" id="Seg_1506" n="e" s="T213">ej </ts>
               <ts e="T215" id="Seg_1508" n="e" s="T214">šobi. </ts>
               <ts e="T216" id="Seg_1510" n="e" s="T215">Dĭ </ts>
               <ts e="T217" id="Seg_1512" n="e" s="T216">koŋgoro </ts>
               <ts e="T218" id="Seg_1514" n="e" s="T217">ibi, </ts>
               <ts e="T219" id="Seg_1516" n="e" s="T218">nuʔməluʔpi, </ts>
               <ts e="T220" id="Seg_1518" n="e" s="T219">nuʔməluʔpi, </ts>
               <ts e="T221" id="Seg_1520" n="e" s="T220">dĭ </ts>
               <ts e="T222" id="Seg_1522" n="e" s="T221">dĭm </ts>
               <ts e="T223" id="Seg_1524" n="e" s="T222">dʼaʔpi </ts>
               <ts e="T224" id="Seg_1526" n="e" s="T223">da </ts>
               <ts e="T225" id="Seg_1528" n="e" s="T224">bar </ts>
               <ts e="T226" id="Seg_1530" n="e" s="T225">amnuʔpi </ts>
               <ts e="T227" id="Seg_1532" n="e" s="T226">bar. </ts>
               <ts e="T228" id="Seg_1534" n="e" s="T227">Dĭgəttə </ts>
               <ts e="T229" id="Seg_1536" n="e" s="T228">(bü-) </ts>
               <ts e="T230" id="Seg_1538" n="e" s="T229">(măndə): </ts>
               <ts e="T231" id="Seg_1540" n="e" s="T230">"Kanaʔ, </ts>
               <ts e="T232" id="Seg_1542" n="e" s="T231">măn </ts>
               <ts e="T233" id="Seg_1544" n="e" s="T232">koʔbdo </ts>
               <ts e="T234" id="Seg_1546" n="e" s="T233">ile!" </ts>
               <ts e="T235" id="Seg_1548" n="e" s="T234">Dĭ </ts>
               <ts e="T236" id="Seg_1550" n="e" s="T235">kambi. </ts>
               <ts e="T237" id="Seg_1552" n="e" s="T236">(Bura) </ts>
               <ts e="T238" id="Seg_1554" n="e" s="T237">ibi. </ts>
               <ts e="T239" id="Seg_1556" n="e" s="T238">Šobi, </ts>
               <ts e="T240" id="Seg_1558" n="e" s="T239">onʼiʔ </ts>
               <ts e="T241" id="Seg_1560" n="e" s="T240">kostăčkaʔi </ts>
               <ts e="T242" id="Seg_1562" n="e" s="T241">iʔbəlaʔbəʔjə. </ts>
               <ts e="T243" id="Seg_1564" n="e" s="T242">Oʔbdəbi </ts>
               <ts e="T244" id="Seg_1566" n="e" s="T243">i </ts>
               <ts e="T245" id="Seg_1568" n="e" s="T244">deʔpi. </ts>
               <ts e="T246" id="Seg_1570" n="e" s="T245">Dĭgəttə </ts>
               <ts e="T247" id="Seg_1572" n="e" s="T246">(man-) </ts>
               <ts e="T248" id="Seg_1574" n="e" s="T247">men </ts>
               <ts e="T249" id="Seg_1576" n="e" s="T248">bazoʔ </ts>
               <ts e="T250" id="Seg_1578" n="e" s="T249">(kürüm-) </ts>
               <ts e="T251" id="Seg_1580" n="e" s="T250">kürümnaʔbə: </ts>
               <ts e="T252" id="Seg_1582" n="e" s="T251">"Nüken </ts>
               <ts e="T253" id="Seg_1584" n="e" s="T252">koʔbdot </ts>
               <ts e="T254" id="Seg_1586" n="e" s="T253">bar </ts>
               <ts e="T255" id="Seg_1588" n="e" s="T254">kostăčkaʔi </ts>
               <ts e="T256" id="Seg_1590" n="e" s="T255">(ko-) </ts>
               <ts e="T257" id="Seg_1592" n="e" s="T256">detleʔbə </ts>
               <ts e="T258" id="Seg_1594" n="e" s="T257">büzʼe". </ts>
               <ts e="T259" id="Seg_1596" n="e" s="T258">"Iʔ </ts>
               <ts e="T260" id="Seg_1598" n="e" s="T259">šamaʔ, </ts>
               <ts e="T261" id="Seg_1600" n="e" s="T260">a_to </ts>
               <ts e="T262" id="Seg_1602" n="e" s="T261">münörləl </ts>
               <ts e="T263" id="Seg_1604" n="e" s="T262">tănan". </ts>
               <ts e="T264" id="Seg_1606" n="e" s="T263">Dĭgəttə </ts>
               <ts e="T265" id="Seg_1608" n="e" s="T264">büzʼe </ts>
               <ts e="T266" id="Seg_1610" n="e" s="T265">(s-) </ts>
               <ts e="T267" id="Seg_1612" n="e" s="T266">šobi, </ts>
               <ts e="T268" id="Seg_1614" n="e" s="T267">onʼiʔ </ts>
               <ts e="T269" id="Seg_1616" n="e" s="T268">kostăčkaʔi </ts>
               <ts e="T270" id="Seg_1618" n="e" s="T269">deʔpi. </ts>
               <ts e="T271" id="Seg_1620" n="e" s="T270">I </ts>
               <ts e="T272" id="Seg_1622" n="e" s="T271">(dĭ-) </ts>
               <ts e="T273" id="Seg_1624" n="e" s="T272">dĭ </ts>
               <ts e="T274" id="Seg_1626" n="e" s="T273">koʔbdon </ts>
               <ts e="T275" id="Seg_1628" n="e" s="T274">(dʼü-) </ts>
               <ts e="T276" id="Seg_1630" n="e" s="T275">dʼünə </ts>
               <ts e="T277" id="Seg_1632" n="e" s="T276">embiʔi </ts>
               <ts e="T278" id="Seg_1634" n="e" s="T277">i </ts>
               <ts e="T279" id="Seg_1636" n="e" s="T278">tĭlbiʔi. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_1637" s="T0">PKZ_196X_DaughterAndStepdaughter_flk.001 (001)</ta>
            <ta e="T10" id="Seg_1638" s="T6">PKZ_196X_DaughterAndStepdaughter_flk.002 (002)</ta>
            <ta e="T18" id="Seg_1639" s="T10">PKZ_196X_DaughterAndStepdaughter_flk.003 (003)</ta>
            <ta e="T26" id="Seg_1640" s="T18">PKZ_196X_DaughterAndStepdaughter_flk.004 (004)</ta>
            <ta e="T38" id="Seg_1641" s="T26">PKZ_196X_DaughterAndStepdaughter_flk.005 (005)</ta>
            <ta e="T52" id="Seg_1642" s="T38">PKZ_196X_DaughterAndStepdaughter_flk.006 (006)</ta>
            <ta e="T56" id="Seg_1643" s="T52">PKZ_196X_DaughterAndStepdaughter_flk.007 (007)</ta>
            <ta e="T64" id="Seg_1644" s="T56">PKZ_196X_DaughterAndStepdaughter_flk.008 (008)</ta>
            <ta e="T68" id="Seg_1645" s="T64">PKZ_196X_DaughterAndStepdaughter_flk.009 (009)</ta>
            <ta e="T71" id="Seg_1646" s="T68">PKZ_196X_DaughterAndStepdaughter_flk.010 (010)</ta>
            <ta e="T76" id="Seg_1647" s="T71">PKZ_196X_DaughterAndStepdaughter_flk.011 (011)</ta>
            <ta e="T80" id="Seg_1648" s="T76">PKZ_196X_DaughterAndStepdaughter_flk.012 (012)</ta>
            <ta e="T87" id="Seg_1649" s="T80">PKZ_196X_DaughterAndStepdaughter_flk.013 (013)</ta>
            <ta e="T96" id="Seg_1650" s="T87">PKZ_196X_DaughterAndStepdaughter_flk.014 (014)</ta>
            <ta e="T105" id="Seg_1651" s="T96">PKZ_196X_DaughterAndStepdaughter_flk.015 (015)</ta>
            <ta e="T107" id="Seg_1652" s="T105">PKZ_196X_DaughterAndStepdaughter_flk.016 (016)</ta>
            <ta e="T111" id="Seg_1653" s="T107">PKZ_196X_DaughterAndStepdaughter_flk.017 (017)</ta>
            <ta e="T116" id="Seg_1654" s="T111">PKZ_196X_DaughterAndStepdaughter_flk.018 (018)</ta>
            <ta e="T120" id="Seg_1655" s="T116">PKZ_196X_DaughterAndStepdaughter_flk.019 (019)</ta>
            <ta e="T128" id="Seg_1656" s="T120">PKZ_196X_DaughterAndStepdaughter_flk.020 (020)</ta>
            <ta e="T131" id="Seg_1657" s="T128">PKZ_196X_DaughterAndStepdaughter_flk.021 (021)</ta>
            <ta e="T133" id="Seg_1658" s="T131">PKZ_196X_DaughterAndStepdaughter_flk.022 (022)</ta>
            <ta e="T135" id="Seg_1659" s="T133">PKZ_196X_DaughterAndStepdaughter_flk.023 (023)</ta>
            <ta e="T142" id="Seg_1660" s="T135">PKZ_196X_DaughterAndStepdaughter_flk.024 (024)</ta>
            <ta e="T147" id="Seg_1661" s="T142">PKZ_196X_DaughterAndStepdaughter_flk.025 (025)</ta>
            <ta e="T161" id="Seg_1662" s="T147">PKZ_196X_DaughterAndStepdaughter_flk.026 (026) </ta>
            <ta e="T167" id="Seg_1663" s="T161">PKZ_196X_DaughterAndStepdaughter_flk.027 (028)</ta>
            <ta e="T177" id="Seg_1664" s="T167">PKZ_196X_DaughterAndStepdaughter_flk.028 (029)</ta>
            <ta e="T184" id="Seg_1665" s="T177">PKZ_196X_DaughterAndStepdaughter_flk.029 (030)</ta>
            <ta e="T188" id="Seg_1666" s="T184">PKZ_196X_DaughterAndStepdaughter_flk.030 (031)</ta>
            <ta e="T193" id="Seg_1667" s="T188">PKZ_196X_DaughterAndStepdaughter_flk.031 (032)</ta>
            <ta e="T200" id="Seg_1668" s="T193">PKZ_196X_DaughterAndStepdaughter_flk.032 (033)</ta>
            <ta e="T211" id="Seg_1669" s="T200">PKZ_196X_DaughterAndStepdaughter_flk.033 (034)</ta>
            <ta e="T215" id="Seg_1670" s="T211">PKZ_196X_DaughterAndStepdaughter_flk.034 (036)</ta>
            <ta e="T227" id="Seg_1671" s="T215">PKZ_196X_DaughterAndStepdaughter_flk.035 (037)</ta>
            <ta e="T234" id="Seg_1672" s="T227">PKZ_196X_DaughterAndStepdaughter_flk.036 (038)</ta>
            <ta e="T236" id="Seg_1673" s="T234">PKZ_196X_DaughterAndStepdaughter_flk.037 (039)</ta>
            <ta e="T238" id="Seg_1674" s="T236">PKZ_196X_DaughterAndStepdaughter_flk.038 (040)</ta>
            <ta e="T242" id="Seg_1675" s="T238">PKZ_196X_DaughterAndStepdaughter_flk.039 (041)</ta>
            <ta e="T245" id="Seg_1676" s="T242">PKZ_196X_DaughterAndStepdaughter_flk.040 (042)</ta>
            <ta e="T258" id="Seg_1677" s="T245">PKZ_196X_DaughterAndStepdaughter_flk.041 (043)</ta>
            <ta e="T263" id="Seg_1678" s="T258">PKZ_196X_DaughterAndStepdaughter_flk.042 (045)</ta>
            <ta e="T270" id="Seg_1679" s="T263">PKZ_196X_DaughterAndStepdaughter_flk.043 (046)</ta>
            <ta e="T279" id="Seg_1680" s="T270">PKZ_196X_DaughterAndStepdaughter_flk.044 (047)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_1681" s="T0">Onʼiʔ büzʼe amnobi, dĭn nüke külambi. </ta>
            <ta e="T10" id="Seg_1682" s="T6">Dĭ ibi baška nüke. </ta>
            <ta e="T18" id="Seg_1683" s="T10">Dĭn nüken koʔbdo ibi, i dĭ büzʼen koʔbdo. </ta>
            <ta e="T26" id="Seg_1684" s="T18">Dĭ büzʼen koʔbdot bar ĭmbi (togonor-) togonorleʔpi ĭmbi. </ta>
            <ta e="T38" id="Seg_1685" s="T26">A dĭʔnə üge ej jakše ibi, măndə:" Tăn ugandə ɨrɨ, ej togonorial". </ta>
            <ta e="T52" id="Seg_1686" s="T38">Dĭgəttə dĭ büzʼenə măndə:" Kondə boslə koʔbdo dʼijenə i dĭn (amn-) bar dĭn amnoldə". </ta>
            <ta e="T56" id="Seg_1687" s="T52">Dĭ konbi, dĭn amnolbi. </ta>
            <ta e="T64" id="Seg_1688" s="T56">(Dĭ=) Dĭgəttə dĭ tura dĭn kubi, šobi turanə. </ta>
            <ta e="T68" id="Seg_1689" s="T64">Tumo: "Deʔ măna ipek." </ta>
            <ta e="T71" id="Seg_1690" s="T68">Dĭ dĭʔnə mĭmbi. </ta>
            <ta e="T76" id="Seg_1691" s="T71">Dĭgəttə kuza šobi, nüdʼi molambi. </ta>
            <ta e="T80" id="Seg_1692" s="T76">(Nünnə) dĭʔnə koŋgoro mĭbi. </ta>
            <ta e="T87" id="Seg_1693" s="T80">"Tăn nuʔməleʔ, a măn tănan (dʼap-) dʼabəlam". </ta>
            <ta e="T96" id="Seg_1694" s="T87">Dĭgəttə tumo šobi, koŋgoro ibi i turagən bar nuʔməleʔpi. </ta>
            <ta e="T105" id="Seg_1695" s="T96">Dĭ (bəl) dĭm dʼabəluʔpi, dʼabəluʔpi, ej (mom-) mobi dʼabəsʼtə. </ta>
            <ta e="T107" id="Seg_1696" s="T105">Koʔbdo saʔlambi. </ta>
            <ta e="T111" id="Seg_1697" s="T107">Ertə molambi, dĭ uʔbdəbi. </ta>
            <ta e="T116" id="Seg_1698" s="T111">Dĭʔnə mĭmbi aktʼa, oldʼa iʔgö. </ta>
            <ta e="T120" id="Seg_1699" s="T116">Jamaʔi mĭbi, plat, iʔgö. </ta>
            <ta e="T128" id="Seg_1700" s="T120">Dĭgəttə dĭ nüke:" Kanaʔ, koʔbdol dĭn (kostăčkaʔi iʔ). </ta>
            <ta e="T131" id="Seg_1701" s="T128">Oʔbdəlil də deʔtə. </ta>
            <ta e="T133" id="Seg_1702" s="T131">Dʼünə tĭlləbeʔ". </ta>
            <ta e="T135" id="Seg_1703" s="T133">Dĭ kambi. </ta>
            <ta e="T142" id="Seg_1704" s="T135">Šobi, koʔbdon bar aktʼa iʔgö, oldʼa iʔgö. </ta>
            <ta e="T147" id="Seg_1705" s="T142">Dĭm ibi, amnolbi inenə, šonəga. </ta>
            <ta e="T161" id="Seg_1706" s="T147">A men bar kirgarlaʔbə: "(Büzʼe=) Büzʼen koʔbdo šonəga, aktʼa detləge i oldʼa iʔgö detləge". </ta>
            <ta e="T167" id="Seg_1707" s="T161">A dĭ nüke măndə:" Iʔ šamaʔ!" </ta>
            <ta e="T177" id="Seg_1708" s="T167">Dĭgəttə büzʼe šobi, iʔgö deʔpi i aktʼa i koʔbdobə deʔpi. </ta>
            <ta e="T184" id="Seg_1709" s="T177">Dĭgəttə măndə:" Kundə măn koʔbdom, dĭn amnoldə". </ta>
            <ta e="T188" id="Seg_1710" s="T184">Dĭ kombi dĭ koʔbdom. </ta>
            <ta e="T193" id="Seg_1711" s="T188">Tomo (piebi), (ipekdə) ej mĭbi. </ta>
            <ta e="T200" id="Seg_1712" s="T193">Dĭgəttə nüdʼin kubi tura, turanə (šo-) šobi. </ta>
            <ta e="T211" id="Seg_1713" s="T200">Dĭn kuza măndə: "Teinen nüdʼin koŋgoro (mubin), (tonoʔ) măn tănan dʼabəlim". </ta>
            <ta e="T215" id="Seg_1714" s="T211">A tumo ej šobi. </ta>
            <ta e="T227" id="Seg_1715" s="T215">Dĭ koŋgoro ibi, nuʔməluʔpi, nuʔməluʔpi, dĭ dĭm dʼaʔpi da bar amnuʔpi bar. </ta>
            <ta e="T234" id="Seg_1716" s="T227">Dĭgəttə (bü-) (măndə):" Kanaʔ, măn koʔbdo ile!" </ta>
            <ta e="T236" id="Seg_1717" s="T234">Dĭ kambi. </ta>
            <ta e="T238" id="Seg_1718" s="T236">(Bura) ibi. </ta>
            <ta e="T242" id="Seg_1719" s="T238">Šobi, onʼiʔ kostăčkaʔi iʔbəlaʔbəʔjə. </ta>
            <ta e="T245" id="Seg_1720" s="T242">Oʔbdəbi i deʔpi. </ta>
            <ta e="T258" id="Seg_1721" s="T245">Dĭgəttə (man-) men bazoʔ (kürüm-) kürümnaʔbə: "Nüken koʔbdot bar kostăčkaʔi (ko-) detleʔbə büzʼe". </ta>
            <ta e="T263" id="Seg_1722" s="T258">"Iʔ šamaʔ, a to münörləl tănan". </ta>
            <ta e="T270" id="Seg_1723" s="T263">Dĭgəttə büzʼe (s-) šobi, onʼiʔ kostăčkaʔi deʔpi. </ta>
            <ta e="T279" id="Seg_1724" s="T270">I (dĭ-) dĭ koʔbdon (dʼü-) dʼünə embiʔi i tĭlbiʔi. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1725" s="T0">onʼiʔ</ta>
            <ta e="T2" id="Seg_1726" s="T1">büzʼe</ta>
            <ta e="T3" id="Seg_1727" s="T2">amno-bi</ta>
            <ta e="T4" id="Seg_1728" s="T3">dĭ-n</ta>
            <ta e="T5" id="Seg_1729" s="T4">nüke</ta>
            <ta e="T6" id="Seg_1730" s="T5">kü-lam-bi</ta>
            <ta e="T7" id="Seg_1731" s="T6">dĭ</ta>
            <ta e="T8" id="Seg_1732" s="T7">i-bi</ta>
            <ta e="T9" id="Seg_1733" s="T8">baška</ta>
            <ta e="T10" id="Seg_1734" s="T9">nüke</ta>
            <ta e="T11" id="Seg_1735" s="T10">dĭ-n</ta>
            <ta e="T12" id="Seg_1736" s="T11">nüke-n</ta>
            <ta e="T13" id="Seg_1737" s="T12">koʔbdo</ta>
            <ta e="T14" id="Seg_1738" s="T13">i-bi</ta>
            <ta e="T15" id="Seg_1739" s="T14">i</ta>
            <ta e="T16" id="Seg_1740" s="T15">dĭ</ta>
            <ta e="T17" id="Seg_1741" s="T16">büzʼe-n</ta>
            <ta e="T18" id="Seg_1742" s="T17">koʔbdo</ta>
            <ta e="T19" id="Seg_1743" s="T18">dĭ</ta>
            <ta e="T20" id="Seg_1744" s="T19">büzʼe-n</ta>
            <ta e="T21" id="Seg_1745" s="T20">koʔbdo-t</ta>
            <ta e="T22" id="Seg_1746" s="T21">bar</ta>
            <ta e="T23" id="Seg_1747" s="T22">ĭmbi</ta>
            <ta e="T24" id="Seg_1748" s="T23">togonor</ta>
            <ta e="T25" id="Seg_1749" s="T24">togonor-leʔpi</ta>
            <ta e="T26" id="Seg_1750" s="T25">ĭmbi</ta>
            <ta e="T27" id="Seg_1751" s="T26">a</ta>
            <ta e="T28" id="Seg_1752" s="T27">dĭʔ-nə</ta>
            <ta e="T29" id="Seg_1753" s="T28">üge</ta>
            <ta e="T30" id="Seg_1754" s="T29">ej</ta>
            <ta e="T31" id="Seg_1755" s="T30">jakše</ta>
            <ta e="T32" id="Seg_1756" s="T31">i-bi</ta>
            <ta e="T33" id="Seg_1757" s="T32">măn-də</ta>
            <ta e="T34" id="Seg_1758" s="T33">tăn</ta>
            <ta e="T35" id="Seg_1759" s="T34">ugandə</ta>
            <ta e="T36" id="Seg_1760" s="T35">ɨrɨ</ta>
            <ta e="T37" id="Seg_1761" s="T36">ej</ta>
            <ta e="T38" id="Seg_1762" s="T37">togonor-ia-l</ta>
            <ta e="T39" id="Seg_1763" s="T38">dĭgəttə</ta>
            <ta e="T40" id="Seg_1764" s="T39">dĭ</ta>
            <ta e="T41" id="Seg_1765" s="T40">büzʼe-nə</ta>
            <ta e="T42" id="Seg_1766" s="T41">măn-də</ta>
            <ta e="T43" id="Seg_1767" s="T42">kon-də</ta>
            <ta e="T44" id="Seg_1768" s="T43">bos-lə</ta>
            <ta e="T45" id="Seg_1769" s="T44">koʔbdo</ta>
            <ta e="T46" id="Seg_1770" s="T45">dʼije-nə</ta>
            <ta e="T47" id="Seg_1771" s="T46">i</ta>
            <ta e="T48" id="Seg_1772" s="T47">dĭn</ta>
            <ta e="T50" id="Seg_1773" s="T49">bar</ta>
            <ta e="T51" id="Seg_1774" s="T50">dĭn</ta>
            <ta e="T52" id="Seg_1775" s="T51">amnol-də</ta>
            <ta e="T53" id="Seg_1776" s="T52">dĭ</ta>
            <ta e="T54" id="Seg_1777" s="T53">kon-bi</ta>
            <ta e="T55" id="Seg_1778" s="T54">dĭn</ta>
            <ta e="T56" id="Seg_1779" s="T55">amnol-bi</ta>
            <ta e="T57" id="Seg_1780" s="T56">dĭ</ta>
            <ta e="T58" id="Seg_1781" s="T57">dĭgəttə</ta>
            <ta e="T59" id="Seg_1782" s="T58">dĭ</ta>
            <ta e="T60" id="Seg_1783" s="T59">tura</ta>
            <ta e="T61" id="Seg_1784" s="T60">dĭn</ta>
            <ta e="T62" id="Seg_1785" s="T61">ku-bi</ta>
            <ta e="T63" id="Seg_1786" s="T62">šo-bi</ta>
            <ta e="T64" id="Seg_1787" s="T63">tura-nə</ta>
            <ta e="T65" id="Seg_1788" s="T64">tumo</ta>
            <ta e="T66" id="Seg_1789" s="T65">de-ʔ</ta>
            <ta e="T67" id="Seg_1790" s="T66">măna</ta>
            <ta e="T68" id="Seg_1791" s="T67">ipek</ta>
            <ta e="T69" id="Seg_1792" s="T68">dĭ</ta>
            <ta e="T70" id="Seg_1793" s="T69">dĭʔ-nə</ta>
            <ta e="T71" id="Seg_1794" s="T70">mĭm-bi</ta>
            <ta e="T72" id="Seg_1795" s="T71">dĭgəttə</ta>
            <ta e="T73" id="Seg_1796" s="T72">kuza</ta>
            <ta e="T74" id="Seg_1797" s="T73">šo-bi</ta>
            <ta e="T75" id="Seg_1798" s="T74">nüdʼi</ta>
            <ta e="T76" id="Seg_1799" s="T75">mo-lam-bi</ta>
            <ta e="T78" id="Seg_1800" s="T77">dĭʔ-nə</ta>
            <ta e="T79" id="Seg_1801" s="T78">koŋgoro</ta>
            <ta e="T80" id="Seg_1802" s="T79">mĭ-bi</ta>
            <ta e="T81" id="Seg_1803" s="T80">tăn</ta>
            <ta e="T82" id="Seg_1804" s="T81">nuʔmə-leʔ</ta>
            <ta e="T83" id="Seg_1805" s="T82">a</ta>
            <ta e="T84" id="Seg_1806" s="T83">măn</ta>
            <ta e="T85" id="Seg_1807" s="T84">tănan</ta>
            <ta e="T87" id="Seg_1808" s="T86">dʼabə-la-m</ta>
            <ta e="T88" id="Seg_1809" s="T87">dĭgəttə</ta>
            <ta e="T89" id="Seg_1810" s="T88">tumo</ta>
            <ta e="T90" id="Seg_1811" s="T89">šo-bi</ta>
            <ta e="T91" id="Seg_1812" s="T90">koŋgoro</ta>
            <ta e="T92" id="Seg_1813" s="T91">i-bi</ta>
            <ta e="T93" id="Seg_1814" s="T92">i</ta>
            <ta e="T94" id="Seg_1815" s="T93">tura-gən</ta>
            <ta e="T95" id="Seg_1816" s="T94">bar</ta>
            <ta e="T96" id="Seg_1817" s="T95">nuʔmə-leʔpi</ta>
            <ta e="T97" id="Seg_1818" s="T96">dĭ</ta>
            <ta e="T99" id="Seg_1819" s="T98">dĭ-m</ta>
            <ta e="T100" id="Seg_1820" s="T99">dʼabə-luʔ-pi</ta>
            <ta e="T101" id="Seg_1821" s="T100">dʼabə-luʔ-pi</ta>
            <ta e="T102" id="Seg_1822" s="T101">ej</ta>
            <ta e="T104" id="Seg_1823" s="T103">mo-bi</ta>
            <ta e="T105" id="Seg_1824" s="T104">dʼabə-sʼtə</ta>
            <ta e="T106" id="Seg_1825" s="T105">koʔbdo</ta>
            <ta e="T107" id="Seg_1826" s="T106">saʔ-lam-bi</ta>
            <ta e="T108" id="Seg_1827" s="T107">ertə</ta>
            <ta e="T109" id="Seg_1828" s="T108">mo-lam-bi</ta>
            <ta e="T110" id="Seg_1829" s="T109">dĭ</ta>
            <ta e="T111" id="Seg_1830" s="T110">uʔbdə-bi</ta>
            <ta e="T112" id="Seg_1831" s="T111">dĭʔ-nə</ta>
            <ta e="T113" id="Seg_1832" s="T112">mĭm-bi</ta>
            <ta e="T114" id="Seg_1833" s="T113">aktʼa</ta>
            <ta e="T115" id="Seg_1834" s="T114">oldʼa</ta>
            <ta e="T116" id="Seg_1835" s="T115">iʔgö</ta>
            <ta e="T117" id="Seg_1836" s="T116">jama-ʔi</ta>
            <ta e="T118" id="Seg_1837" s="T117">mĭ-bi</ta>
            <ta e="T119" id="Seg_1838" s="T118">plat</ta>
            <ta e="T120" id="Seg_1839" s="T119">iʔgö</ta>
            <ta e="T121" id="Seg_1840" s="T120">dĭgəttə</ta>
            <ta e="T122" id="Seg_1841" s="T121">dĭ</ta>
            <ta e="T123" id="Seg_1842" s="T122">nüke</ta>
            <ta e="T124" id="Seg_1843" s="T123">kan-a-ʔ</ta>
            <ta e="T125" id="Seg_1844" s="T124">koʔbdo-l</ta>
            <ta e="T126" id="Seg_1845" s="T125">dĭn</ta>
            <ta e="T127" id="Seg_1846" s="T126">kostăčka-ʔi</ta>
            <ta e="T128" id="Seg_1847" s="T127">i-ʔ</ta>
            <ta e="T129" id="Seg_1848" s="T128">oʔbdə-li-l</ta>
            <ta e="T130" id="Seg_1849" s="T129">də</ta>
            <ta e="T131" id="Seg_1850" s="T130">deʔ-tə</ta>
            <ta e="T132" id="Seg_1851" s="T131">dʼü-nə</ta>
            <ta e="T133" id="Seg_1852" s="T132">tĭl-lə-beʔ</ta>
            <ta e="T134" id="Seg_1853" s="T133">dĭ</ta>
            <ta e="T135" id="Seg_1854" s="T134">kam-bi</ta>
            <ta e="T136" id="Seg_1855" s="T135">šo-bi</ta>
            <ta e="T137" id="Seg_1856" s="T136">koʔbdo-n</ta>
            <ta e="T138" id="Seg_1857" s="T137">bar</ta>
            <ta e="T139" id="Seg_1858" s="T138">aktʼa</ta>
            <ta e="T140" id="Seg_1859" s="T139">iʔgö</ta>
            <ta e="T141" id="Seg_1860" s="T140">oldʼa</ta>
            <ta e="T142" id="Seg_1861" s="T141">iʔgö</ta>
            <ta e="T143" id="Seg_1862" s="T142">dĭ-m</ta>
            <ta e="T144" id="Seg_1863" s="T143">i-bi</ta>
            <ta e="T145" id="Seg_1864" s="T144">amnol-bi</ta>
            <ta e="T146" id="Seg_1865" s="T145">ine-nə</ta>
            <ta e="T147" id="Seg_1866" s="T146">šonə-ga</ta>
            <ta e="T148" id="Seg_1867" s="T147">a</ta>
            <ta e="T149" id="Seg_1868" s="T148">men</ta>
            <ta e="T150" id="Seg_1869" s="T149">bar</ta>
            <ta e="T151" id="Seg_1870" s="T150">kirgar-laʔbə</ta>
            <ta e="T153" id="Seg_1871" s="T152">büzʼe-n</ta>
            <ta e="T154" id="Seg_1872" s="T153">koʔbdo</ta>
            <ta e="T155" id="Seg_1873" s="T154">šonə-ga</ta>
            <ta e="T156" id="Seg_1874" s="T155">aktʼa</ta>
            <ta e="T157" id="Seg_1875" s="T156">det-ləge</ta>
            <ta e="T158" id="Seg_1876" s="T157">i</ta>
            <ta e="T159" id="Seg_1877" s="T158">oldʼa</ta>
            <ta e="T160" id="Seg_1878" s="T159">iʔgö</ta>
            <ta e="T161" id="Seg_1879" s="T160">det-ləge</ta>
            <ta e="T162" id="Seg_1880" s="T161">a</ta>
            <ta e="T163" id="Seg_1881" s="T162">dĭ</ta>
            <ta e="T164" id="Seg_1882" s="T163">nüke</ta>
            <ta e="T165" id="Seg_1883" s="T164">măn-də</ta>
            <ta e="T166" id="Seg_1884" s="T165">i-ʔ</ta>
            <ta e="T167" id="Seg_1885" s="T166">šama-ʔ</ta>
            <ta e="T168" id="Seg_1886" s="T167">dĭgəttə</ta>
            <ta e="T169" id="Seg_1887" s="T168">büzʼe</ta>
            <ta e="T170" id="Seg_1888" s="T169">šo-bi</ta>
            <ta e="T171" id="Seg_1889" s="T170">iʔgö</ta>
            <ta e="T172" id="Seg_1890" s="T171">deʔ-pi</ta>
            <ta e="T173" id="Seg_1891" s="T172">i</ta>
            <ta e="T174" id="Seg_1892" s="T173">aktʼa</ta>
            <ta e="T175" id="Seg_1893" s="T174">i</ta>
            <ta e="T176" id="Seg_1894" s="T175">koʔbdo-bə</ta>
            <ta e="T177" id="Seg_1895" s="T176">deʔ-pi</ta>
            <ta e="T178" id="Seg_1896" s="T177">dĭgəttə</ta>
            <ta e="T179" id="Seg_1897" s="T178">măn-də</ta>
            <ta e="T180" id="Seg_1898" s="T179">kun-də</ta>
            <ta e="T181" id="Seg_1899" s="T180">măn</ta>
            <ta e="T182" id="Seg_1900" s="T181">koʔbdo-m</ta>
            <ta e="T183" id="Seg_1901" s="T182">dĭn</ta>
            <ta e="T184" id="Seg_1902" s="T183">amnol-də</ta>
            <ta e="T185" id="Seg_1903" s="T184">dĭ</ta>
            <ta e="T186" id="Seg_1904" s="T185">kom-bi</ta>
            <ta e="T187" id="Seg_1905" s="T186">dĭ</ta>
            <ta e="T188" id="Seg_1906" s="T187">koʔbdo-m</ta>
            <ta e="T189" id="Seg_1907" s="T188">tomo</ta>
            <ta e="T190" id="Seg_1908" s="T189">pie-bi</ta>
            <ta e="T192" id="Seg_1909" s="T191">ej</ta>
            <ta e="T193" id="Seg_1910" s="T192">mĭ-bi</ta>
            <ta e="T194" id="Seg_1911" s="T193">dĭgəttə</ta>
            <ta e="T195" id="Seg_1912" s="T194">nüdʼi-n</ta>
            <ta e="T196" id="Seg_1913" s="T195">ku-bi</ta>
            <ta e="T197" id="Seg_1914" s="T196">tura</ta>
            <ta e="T198" id="Seg_1915" s="T197">tura-nə</ta>
            <ta e="T200" id="Seg_1916" s="T199">šo-bi</ta>
            <ta e="T201" id="Seg_1917" s="T200">dĭn</ta>
            <ta e="T202" id="Seg_1918" s="T201">kuza</ta>
            <ta e="T203" id="Seg_1919" s="T202">măn-də</ta>
            <ta e="T204" id="Seg_1920" s="T203">teinen</ta>
            <ta e="T205" id="Seg_1921" s="T204">nüdʼi-n</ta>
            <ta e="T206" id="Seg_1922" s="T205">koŋgoro</ta>
            <ta e="T209" id="Seg_1923" s="T208">măn</ta>
            <ta e="T210" id="Seg_1924" s="T209">tănan</ta>
            <ta e="T211" id="Seg_1925" s="T210">dʼabə-li-m</ta>
            <ta e="T212" id="Seg_1926" s="T211">a</ta>
            <ta e="T213" id="Seg_1927" s="T212">tumo</ta>
            <ta e="T214" id="Seg_1928" s="T213">ej</ta>
            <ta e="T215" id="Seg_1929" s="T214">šo-bi</ta>
            <ta e="T216" id="Seg_1930" s="T215">dĭ</ta>
            <ta e="T217" id="Seg_1931" s="T216">koŋgoro</ta>
            <ta e="T218" id="Seg_1932" s="T217">i-bi</ta>
            <ta e="T219" id="Seg_1933" s="T218">nuʔmə-luʔ-pi</ta>
            <ta e="T220" id="Seg_1934" s="T219">nuʔmə-luʔ-pi</ta>
            <ta e="T221" id="Seg_1935" s="T220">dĭ</ta>
            <ta e="T222" id="Seg_1936" s="T221">dĭ-m</ta>
            <ta e="T223" id="Seg_1937" s="T222">dʼaʔ-pi</ta>
            <ta e="T224" id="Seg_1938" s="T223">da</ta>
            <ta e="T225" id="Seg_1939" s="T224">bar</ta>
            <ta e="T226" id="Seg_1940" s="T225">am-nuʔ-pi</ta>
            <ta e="T227" id="Seg_1941" s="T226">bar</ta>
            <ta e="T228" id="Seg_1942" s="T227">dĭgəttə</ta>
            <ta e="T230" id="Seg_1943" s="T229">măn-də</ta>
            <ta e="T231" id="Seg_1944" s="T230">kan-a-ʔ</ta>
            <ta e="T232" id="Seg_1945" s="T231">măn</ta>
            <ta e="T233" id="Seg_1946" s="T232">koʔbdo</ta>
            <ta e="T234" id="Seg_1947" s="T233">i-le</ta>
            <ta e="T235" id="Seg_1948" s="T234">dĭ</ta>
            <ta e="T236" id="Seg_1949" s="T235">kam-bi</ta>
            <ta e="T237" id="Seg_1950" s="T236">bura</ta>
            <ta e="T238" id="Seg_1951" s="T237">i-bi</ta>
            <ta e="T239" id="Seg_1952" s="T238">šo-bi</ta>
            <ta e="T240" id="Seg_1953" s="T239">onʼiʔ</ta>
            <ta e="T241" id="Seg_1954" s="T240">kostăčka-ʔi</ta>
            <ta e="T242" id="Seg_1955" s="T241">iʔbə-laʔbə-ʔjə</ta>
            <ta e="T243" id="Seg_1956" s="T242">oʔbdə-bi</ta>
            <ta e="T244" id="Seg_1957" s="T243">i</ta>
            <ta e="T245" id="Seg_1958" s="T244">deʔ-pi</ta>
            <ta e="T246" id="Seg_1959" s="T245">dĭgəttə</ta>
            <ta e="T248" id="Seg_1960" s="T247">men</ta>
            <ta e="T249" id="Seg_1961" s="T248">bazoʔ</ta>
            <ta e="T251" id="Seg_1962" s="T250">kürüm-naʔbə</ta>
            <ta e="T252" id="Seg_1963" s="T251">nüke-n</ta>
            <ta e="T253" id="Seg_1964" s="T252">koʔbdo-t</ta>
            <ta e="T254" id="Seg_1965" s="T253">bar</ta>
            <ta e="T255" id="Seg_1966" s="T254">kostăčka-ʔi</ta>
            <ta e="T257" id="Seg_1967" s="T256">det-leʔbə</ta>
            <ta e="T258" id="Seg_1968" s="T257">büzʼe</ta>
            <ta e="T259" id="Seg_1969" s="T258">i-ʔ</ta>
            <ta e="T260" id="Seg_1970" s="T259">šama-ʔ</ta>
            <ta e="T261" id="Seg_1971" s="T260">ato</ta>
            <ta e="T263" id="Seg_1972" s="T262">tănan</ta>
            <ta e="T264" id="Seg_1973" s="T263">dĭgəttə</ta>
            <ta e="T265" id="Seg_1974" s="T264">büzʼe</ta>
            <ta e="T267" id="Seg_1975" s="T266">šo-bi</ta>
            <ta e="T268" id="Seg_1976" s="T267">onʼiʔ</ta>
            <ta e="T269" id="Seg_1977" s="T268">kostăčka-ʔi</ta>
            <ta e="T270" id="Seg_1978" s="T269">deʔ-pi</ta>
            <ta e="T271" id="Seg_1979" s="T270">i</ta>
            <ta e="T273" id="Seg_1980" s="T272">dĭ</ta>
            <ta e="T274" id="Seg_1981" s="T273">koʔbdon</ta>
            <ta e="T276" id="Seg_1982" s="T275">dʼü-nə</ta>
            <ta e="T277" id="Seg_1983" s="T276">em-bi-ʔi</ta>
            <ta e="T278" id="Seg_1984" s="T277">i</ta>
            <ta e="T279" id="Seg_1985" s="T278">tĭl-bi-ʔi</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1986" s="T0">onʼiʔ</ta>
            <ta e="T2" id="Seg_1987" s="T1">büzʼe</ta>
            <ta e="T3" id="Seg_1988" s="T2">amno-bi</ta>
            <ta e="T4" id="Seg_1989" s="T3">dĭ-n</ta>
            <ta e="T5" id="Seg_1990" s="T4">nüke</ta>
            <ta e="T6" id="Seg_1991" s="T5">kü-laːm-bi</ta>
            <ta e="T7" id="Seg_1992" s="T6">dĭ</ta>
            <ta e="T8" id="Seg_1993" s="T7">i-bi</ta>
            <ta e="T9" id="Seg_1994" s="T8">baška</ta>
            <ta e="T10" id="Seg_1995" s="T9">nüke</ta>
            <ta e="T11" id="Seg_1996" s="T10">dĭ-n</ta>
            <ta e="T12" id="Seg_1997" s="T11">nüke-n</ta>
            <ta e="T13" id="Seg_1998" s="T12">koʔbdo</ta>
            <ta e="T14" id="Seg_1999" s="T13">i-bi</ta>
            <ta e="T15" id="Seg_2000" s="T14">i</ta>
            <ta e="T16" id="Seg_2001" s="T15">dĭ</ta>
            <ta e="T17" id="Seg_2002" s="T16">büzʼe-n</ta>
            <ta e="T18" id="Seg_2003" s="T17">koʔbdo</ta>
            <ta e="T19" id="Seg_2004" s="T18">dĭ</ta>
            <ta e="T20" id="Seg_2005" s="T19">büzʼe-n</ta>
            <ta e="T21" id="Seg_2006" s="T20">koʔbdo-t</ta>
            <ta e="T22" id="Seg_2007" s="T21">bar</ta>
            <ta e="T23" id="Seg_2008" s="T22">ĭmbi</ta>
            <ta e="T24" id="Seg_2009" s="T23">togonər</ta>
            <ta e="T25" id="Seg_2010" s="T24">togonər-lAʔpi</ta>
            <ta e="T26" id="Seg_2011" s="T25">ĭmbi</ta>
            <ta e="T27" id="Seg_2012" s="T26">a</ta>
            <ta e="T28" id="Seg_2013" s="T27">dĭ-Tə</ta>
            <ta e="T29" id="Seg_2014" s="T28">üge</ta>
            <ta e="T30" id="Seg_2015" s="T29">ej</ta>
            <ta e="T31" id="Seg_2016" s="T30">jakšə</ta>
            <ta e="T32" id="Seg_2017" s="T31">i-bi</ta>
            <ta e="T33" id="Seg_2018" s="T32">măn-ntə</ta>
            <ta e="T34" id="Seg_2019" s="T33">tăn</ta>
            <ta e="T35" id="Seg_2020" s="T34">ugaːndə</ta>
            <ta e="T36" id="Seg_2021" s="T35">ɨrɨː</ta>
            <ta e="T37" id="Seg_2022" s="T36">ej</ta>
            <ta e="T38" id="Seg_2023" s="T37">togonər-liA-l</ta>
            <ta e="T39" id="Seg_2024" s="T38">dĭgəttə</ta>
            <ta e="T40" id="Seg_2025" s="T39">dĭ</ta>
            <ta e="T41" id="Seg_2026" s="T40">büzʼe-Tə</ta>
            <ta e="T42" id="Seg_2027" s="T41">măn-ntə</ta>
            <ta e="T43" id="Seg_2028" s="T42">kon-t</ta>
            <ta e="T44" id="Seg_2029" s="T43">bos-l</ta>
            <ta e="T45" id="Seg_2030" s="T44">koʔbdo</ta>
            <ta e="T46" id="Seg_2031" s="T45">dʼije-Tə</ta>
            <ta e="T47" id="Seg_2032" s="T46">i</ta>
            <ta e="T48" id="Seg_2033" s="T47">dĭn</ta>
            <ta e="T50" id="Seg_2034" s="T49">bar</ta>
            <ta e="T51" id="Seg_2035" s="T50">dĭn</ta>
            <ta e="T52" id="Seg_2036" s="T51">amnol-t</ta>
            <ta e="T53" id="Seg_2037" s="T52">dĭ</ta>
            <ta e="T54" id="Seg_2038" s="T53">kon-bi</ta>
            <ta e="T55" id="Seg_2039" s="T54">dĭn</ta>
            <ta e="T56" id="Seg_2040" s="T55">amnol-bi</ta>
            <ta e="T57" id="Seg_2041" s="T56">dĭ</ta>
            <ta e="T58" id="Seg_2042" s="T57">dĭgəttə</ta>
            <ta e="T59" id="Seg_2043" s="T58">dĭ</ta>
            <ta e="T60" id="Seg_2044" s="T59">tura</ta>
            <ta e="T61" id="Seg_2045" s="T60">dĭn</ta>
            <ta e="T62" id="Seg_2046" s="T61">ku-bi</ta>
            <ta e="T63" id="Seg_2047" s="T62">šo-bi</ta>
            <ta e="T64" id="Seg_2048" s="T63">tura-Tə</ta>
            <ta e="T65" id="Seg_2049" s="T64">tumo</ta>
            <ta e="T66" id="Seg_2050" s="T65">det-ʔ</ta>
            <ta e="T67" id="Seg_2051" s="T66">măna</ta>
            <ta e="T68" id="Seg_2052" s="T67">ipek</ta>
            <ta e="T69" id="Seg_2053" s="T68">dĭ</ta>
            <ta e="T70" id="Seg_2054" s="T69">dĭ-Tə</ta>
            <ta e="T71" id="Seg_2055" s="T70">mĭn-bi</ta>
            <ta e="T72" id="Seg_2056" s="T71">dĭgəttə</ta>
            <ta e="T73" id="Seg_2057" s="T72">kuza</ta>
            <ta e="T74" id="Seg_2058" s="T73">šo-bi</ta>
            <ta e="T75" id="Seg_2059" s="T74">nüdʼi</ta>
            <ta e="T76" id="Seg_2060" s="T75">mo-laːm-bi</ta>
            <ta e="T78" id="Seg_2061" s="T77">dĭ-Tə</ta>
            <ta e="T79" id="Seg_2062" s="T78">koŋgoro</ta>
            <ta e="T80" id="Seg_2063" s="T79">mĭ-bi</ta>
            <ta e="T81" id="Seg_2064" s="T80">tăn</ta>
            <ta e="T82" id="Seg_2065" s="T81">nuʔmə-lAʔ</ta>
            <ta e="T83" id="Seg_2066" s="T82">a</ta>
            <ta e="T84" id="Seg_2067" s="T83">măn</ta>
            <ta e="T85" id="Seg_2068" s="T84">tănan</ta>
            <ta e="T87" id="Seg_2069" s="T86">dʼabə-lV-m</ta>
            <ta e="T88" id="Seg_2070" s="T87">dĭgəttə</ta>
            <ta e="T89" id="Seg_2071" s="T88">tumo</ta>
            <ta e="T90" id="Seg_2072" s="T89">šo-bi</ta>
            <ta e="T91" id="Seg_2073" s="T90">koŋgoro</ta>
            <ta e="T92" id="Seg_2074" s="T91">i-bi</ta>
            <ta e="T93" id="Seg_2075" s="T92">i</ta>
            <ta e="T94" id="Seg_2076" s="T93">tura-Kən</ta>
            <ta e="T95" id="Seg_2077" s="T94">bar</ta>
            <ta e="T96" id="Seg_2078" s="T95">nuʔmə-laʔpi</ta>
            <ta e="T97" id="Seg_2079" s="T96">dĭ</ta>
            <ta e="T99" id="Seg_2080" s="T98">dĭ-m</ta>
            <ta e="T100" id="Seg_2081" s="T99">dʼabə-luʔbdə-bi</ta>
            <ta e="T101" id="Seg_2082" s="T100">dʼabə-luʔbdə-bi</ta>
            <ta e="T102" id="Seg_2083" s="T101">ej</ta>
            <ta e="T104" id="Seg_2084" s="T103">mo-bi</ta>
            <ta e="T105" id="Seg_2085" s="T104">dʼabə-zittə</ta>
            <ta e="T106" id="Seg_2086" s="T105">koʔbdo</ta>
            <ta e="T107" id="Seg_2087" s="T106">šaʔ-laːm-bi</ta>
            <ta e="T108" id="Seg_2088" s="T107">ertə</ta>
            <ta e="T109" id="Seg_2089" s="T108">mo-laːm-bi</ta>
            <ta e="T110" id="Seg_2090" s="T109">dĭ</ta>
            <ta e="T111" id="Seg_2091" s="T110">uʔbdə-bi</ta>
            <ta e="T112" id="Seg_2092" s="T111">dĭ-Tə</ta>
            <ta e="T113" id="Seg_2093" s="T112">mĭn-bi</ta>
            <ta e="T114" id="Seg_2094" s="T113">aktʼa</ta>
            <ta e="T115" id="Seg_2095" s="T114">oldʼa</ta>
            <ta e="T116" id="Seg_2096" s="T115">iʔgö</ta>
            <ta e="T117" id="Seg_2097" s="T116">jama-jəʔ</ta>
            <ta e="T118" id="Seg_2098" s="T117">mĭ-bi</ta>
            <ta e="T119" id="Seg_2099" s="T118">plat</ta>
            <ta e="T120" id="Seg_2100" s="T119">iʔgö</ta>
            <ta e="T121" id="Seg_2101" s="T120">dĭgəttə</ta>
            <ta e="T122" id="Seg_2102" s="T121">dĭ</ta>
            <ta e="T123" id="Seg_2103" s="T122">nüke</ta>
            <ta e="T124" id="Seg_2104" s="T123">kan-ə-ʔ</ta>
            <ta e="T125" id="Seg_2105" s="T124">koʔbdo-l</ta>
            <ta e="T126" id="Seg_2106" s="T125">dĭn</ta>
            <ta e="T127" id="Seg_2107" s="T126">kostăčka-jəʔ</ta>
            <ta e="T128" id="Seg_2108" s="T127">i-ʔ</ta>
            <ta e="T129" id="Seg_2109" s="T128">oʔbdə-lV-l</ta>
            <ta e="T130" id="Seg_2110" s="T129">da</ta>
            <ta e="T131" id="Seg_2111" s="T130">det-t</ta>
            <ta e="T132" id="Seg_2112" s="T131">tʼo-Tə</ta>
            <ta e="T133" id="Seg_2113" s="T132">tĭl-lV-bAʔ</ta>
            <ta e="T134" id="Seg_2114" s="T133">dĭ</ta>
            <ta e="T135" id="Seg_2115" s="T134">kan-bi</ta>
            <ta e="T136" id="Seg_2116" s="T135">šo-bi</ta>
            <ta e="T137" id="Seg_2117" s="T136">koʔbdo-n</ta>
            <ta e="T138" id="Seg_2118" s="T137">bar</ta>
            <ta e="T139" id="Seg_2119" s="T138">aktʼa</ta>
            <ta e="T140" id="Seg_2120" s="T139">iʔgö</ta>
            <ta e="T141" id="Seg_2121" s="T140">oldʼa</ta>
            <ta e="T142" id="Seg_2122" s="T141">iʔgö</ta>
            <ta e="T143" id="Seg_2123" s="T142">dĭ-m</ta>
            <ta e="T144" id="Seg_2124" s="T143">i-bi</ta>
            <ta e="T145" id="Seg_2125" s="T144">amnol-bi</ta>
            <ta e="T146" id="Seg_2126" s="T145">ine-Tə</ta>
            <ta e="T147" id="Seg_2127" s="T146">šonə-gA</ta>
            <ta e="T148" id="Seg_2128" s="T147">a</ta>
            <ta e="T149" id="Seg_2129" s="T148">men</ta>
            <ta e="T150" id="Seg_2130" s="T149">bar</ta>
            <ta e="T151" id="Seg_2131" s="T150">kirgaːr-laʔbə</ta>
            <ta e="T153" id="Seg_2132" s="T152">büzʼe-n</ta>
            <ta e="T154" id="Seg_2133" s="T153">koʔbdo</ta>
            <ta e="T155" id="Seg_2134" s="T154">šonə-gA</ta>
            <ta e="T156" id="Seg_2135" s="T155">aktʼa</ta>
            <ta e="T157" id="Seg_2136" s="T156">det-%%</ta>
            <ta e="T158" id="Seg_2137" s="T157">i</ta>
            <ta e="T159" id="Seg_2138" s="T158">oldʼa</ta>
            <ta e="T160" id="Seg_2139" s="T159">iʔgö</ta>
            <ta e="T161" id="Seg_2140" s="T160">det-%%</ta>
            <ta e="T162" id="Seg_2141" s="T161">a</ta>
            <ta e="T163" id="Seg_2142" s="T162">dĭ</ta>
            <ta e="T164" id="Seg_2143" s="T163">nüke</ta>
            <ta e="T165" id="Seg_2144" s="T164">măn-ntə</ta>
            <ta e="T166" id="Seg_2145" s="T165">e-ʔ</ta>
            <ta e="T167" id="Seg_2146" s="T166">šʼaːm-ʔ</ta>
            <ta e="T168" id="Seg_2147" s="T167">dĭgəttə</ta>
            <ta e="T169" id="Seg_2148" s="T168">büzʼe</ta>
            <ta e="T170" id="Seg_2149" s="T169">šo-bi</ta>
            <ta e="T171" id="Seg_2150" s="T170">iʔgö</ta>
            <ta e="T172" id="Seg_2151" s="T171">det-bi</ta>
            <ta e="T173" id="Seg_2152" s="T172">i</ta>
            <ta e="T174" id="Seg_2153" s="T173">aktʼa</ta>
            <ta e="T175" id="Seg_2154" s="T174">i</ta>
            <ta e="T176" id="Seg_2155" s="T175">koʔbdo-bə</ta>
            <ta e="T177" id="Seg_2156" s="T176">det-bi</ta>
            <ta e="T178" id="Seg_2157" s="T177">dĭgəttə</ta>
            <ta e="T179" id="Seg_2158" s="T178">măn-ntə</ta>
            <ta e="T180" id="Seg_2159" s="T179">kun-t</ta>
            <ta e="T181" id="Seg_2160" s="T180">măn</ta>
            <ta e="T182" id="Seg_2161" s="T181">koʔbdo-m</ta>
            <ta e="T183" id="Seg_2162" s="T182">dĭn</ta>
            <ta e="T184" id="Seg_2163" s="T183">amnol-t</ta>
            <ta e="T185" id="Seg_2164" s="T184">dĭ</ta>
            <ta e="T186" id="Seg_2165" s="T185">kon-bi</ta>
            <ta e="T187" id="Seg_2166" s="T186">dĭ</ta>
            <ta e="T188" id="Seg_2167" s="T187">koʔbdo-m</ta>
            <ta e="T189" id="Seg_2168" s="T188">tumo</ta>
            <ta e="T190" id="Seg_2169" s="T189">%%-bi</ta>
            <ta e="T192" id="Seg_2170" s="T191">ej</ta>
            <ta e="T193" id="Seg_2171" s="T192">mĭ-bi</ta>
            <ta e="T194" id="Seg_2172" s="T193">dĭgəttə</ta>
            <ta e="T195" id="Seg_2173" s="T194">nüdʼi-n</ta>
            <ta e="T196" id="Seg_2174" s="T195">ku-bi</ta>
            <ta e="T197" id="Seg_2175" s="T196">tura</ta>
            <ta e="T198" id="Seg_2176" s="T197">tura-Tə</ta>
            <ta e="T200" id="Seg_2177" s="T199">šo-bi</ta>
            <ta e="T201" id="Seg_2178" s="T200">dĭn</ta>
            <ta e="T202" id="Seg_2179" s="T201">kuza</ta>
            <ta e="T203" id="Seg_2180" s="T202">măn-ntə</ta>
            <ta e="T204" id="Seg_2181" s="T203">teinen</ta>
            <ta e="T205" id="Seg_2182" s="T204">nüdʼi-n</ta>
            <ta e="T206" id="Seg_2183" s="T205">koŋgoro</ta>
            <ta e="T209" id="Seg_2184" s="T208">măn</ta>
            <ta e="T210" id="Seg_2185" s="T209">tănan</ta>
            <ta e="T211" id="Seg_2186" s="T210">dʼabə-lV-m</ta>
            <ta e="T212" id="Seg_2187" s="T211">a</ta>
            <ta e="T213" id="Seg_2188" s="T212">tumo</ta>
            <ta e="T214" id="Seg_2189" s="T213">ej</ta>
            <ta e="T215" id="Seg_2190" s="T214">šo-bi</ta>
            <ta e="T216" id="Seg_2191" s="T215">dĭ</ta>
            <ta e="T217" id="Seg_2192" s="T216">koŋgoro</ta>
            <ta e="T218" id="Seg_2193" s="T217">i-bi</ta>
            <ta e="T219" id="Seg_2194" s="T218">nuʔmə-luʔbdə-bi</ta>
            <ta e="T220" id="Seg_2195" s="T219">nuʔmə-luʔbdə-bi</ta>
            <ta e="T221" id="Seg_2196" s="T220">dĭ</ta>
            <ta e="T222" id="Seg_2197" s="T221">dĭ-m</ta>
            <ta e="T223" id="Seg_2198" s="T222">dʼabə-bi</ta>
            <ta e="T224" id="Seg_2199" s="T223">da</ta>
            <ta e="T225" id="Seg_2200" s="T224">bar</ta>
            <ta e="T226" id="Seg_2201" s="T225">am-luʔbdə-bi</ta>
            <ta e="T227" id="Seg_2202" s="T226">bar</ta>
            <ta e="T228" id="Seg_2203" s="T227">dĭgəttə</ta>
            <ta e="T230" id="Seg_2204" s="T229">măn-ntə</ta>
            <ta e="T231" id="Seg_2205" s="T230">kan-ə-ʔ</ta>
            <ta e="T232" id="Seg_2206" s="T231">măn</ta>
            <ta e="T233" id="Seg_2207" s="T232">koʔbdo</ta>
            <ta e="T234" id="Seg_2208" s="T233">i-lAʔ</ta>
            <ta e="T235" id="Seg_2209" s="T234">dĭ</ta>
            <ta e="T236" id="Seg_2210" s="T235">kan-bi</ta>
            <ta e="T237" id="Seg_2211" s="T236">băra</ta>
            <ta e="T238" id="Seg_2212" s="T237">i-bi</ta>
            <ta e="T239" id="Seg_2213" s="T238">šo-bi</ta>
            <ta e="T240" id="Seg_2214" s="T239">onʼiʔ</ta>
            <ta e="T241" id="Seg_2215" s="T240">kostăčka-jəʔ</ta>
            <ta e="T242" id="Seg_2216" s="T241">iʔbö-laʔbə-jəʔ</ta>
            <ta e="T243" id="Seg_2217" s="T242">oʔbdə-bi</ta>
            <ta e="T244" id="Seg_2218" s="T243">i</ta>
            <ta e="T245" id="Seg_2219" s="T244">det-bi</ta>
            <ta e="T246" id="Seg_2220" s="T245">dĭgəttə</ta>
            <ta e="T248" id="Seg_2221" s="T247">men</ta>
            <ta e="T249" id="Seg_2222" s="T248">bazoʔ</ta>
            <ta e="T251" id="Seg_2223" s="T250">kürüm-laʔbə</ta>
            <ta e="T252" id="Seg_2224" s="T251">nüke-n</ta>
            <ta e="T253" id="Seg_2225" s="T252">koʔbdo-t</ta>
            <ta e="T254" id="Seg_2226" s="T253">bar</ta>
            <ta e="T255" id="Seg_2227" s="T254">kostăčka-jəʔ</ta>
            <ta e="T257" id="Seg_2228" s="T256">det-laʔbə</ta>
            <ta e="T258" id="Seg_2229" s="T257">büzʼe</ta>
            <ta e="T259" id="Seg_2230" s="T258">e-ʔ</ta>
            <ta e="T260" id="Seg_2231" s="T259">šʼaːm-ʔ</ta>
            <ta e="T261" id="Seg_2232" s="T260">ato</ta>
            <ta e="T263" id="Seg_2233" s="T262">tănan</ta>
            <ta e="T264" id="Seg_2234" s="T263">dĭgəttə</ta>
            <ta e="T265" id="Seg_2235" s="T264">büzʼe</ta>
            <ta e="T267" id="Seg_2236" s="T266">šo-bi</ta>
            <ta e="T268" id="Seg_2237" s="T267">onʼiʔ</ta>
            <ta e="T269" id="Seg_2238" s="T268">kostăčka-jəʔ</ta>
            <ta e="T270" id="Seg_2239" s="T269">det-bi</ta>
            <ta e="T271" id="Seg_2240" s="T270">i</ta>
            <ta e="T273" id="Seg_2241" s="T272">dĭ</ta>
            <ta e="T274" id="Seg_2242" s="T273">koʔbdo</ta>
            <ta e="T276" id="Seg_2243" s="T275">tʼo-Tə</ta>
            <ta e="T277" id="Seg_2244" s="T276">hen-bi-jəʔ</ta>
            <ta e="T278" id="Seg_2245" s="T277">i</ta>
            <ta e="T279" id="Seg_2246" s="T278">tĭl-bi-jəʔ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_2247" s="T0">one.[NOM.SG]</ta>
            <ta e="T2" id="Seg_2248" s="T1">man.[NOM.SG]</ta>
            <ta e="T3" id="Seg_2249" s="T2">live-PST.[3SG]</ta>
            <ta e="T4" id="Seg_2250" s="T3">this-GEN</ta>
            <ta e="T5" id="Seg_2251" s="T4">woman.[NOM.SG]</ta>
            <ta e="T6" id="Seg_2252" s="T5">die-RES-PST.[3SG]</ta>
            <ta e="T7" id="Seg_2253" s="T6">this.[NOM.SG]</ta>
            <ta e="T8" id="Seg_2254" s="T7">take-PST.[3SG]</ta>
            <ta e="T9" id="Seg_2255" s="T8">another.[NOM.SG]</ta>
            <ta e="T10" id="Seg_2256" s="T9">woman.[NOM.SG]</ta>
            <ta e="T11" id="Seg_2257" s="T10">this-GEN</ta>
            <ta e="T12" id="Seg_2258" s="T11">woman-GEN</ta>
            <ta e="T13" id="Seg_2259" s="T12">girl.[NOM.SG]</ta>
            <ta e="T14" id="Seg_2260" s="T13">be-PST.[3SG]</ta>
            <ta e="T15" id="Seg_2261" s="T14">and</ta>
            <ta e="T16" id="Seg_2262" s="T15">this.[NOM.SG]</ta>
            <ta e="T17" id="Seg_2263" s="T16">man-GEN</ta>
            <ta e="T18" id="Seg_2264" s="T17">girl.[NOM.SG]</ta>
            <ta e="T19" id="Seg_2265" s="T18">this.[NOM.SG]</ta>
            <ta e="T20" id="Seg_2266" s="T19">man-GEN</ta>
            <ta e="T21" id="Seg_2267" s="T20">daughter-NOM/GEN.3SG</ta>
            <ta e="T22" id="Seg_2268" s="T21">all</ta>
            <ta e="T23" id="Seg_2269" s="T22">what.[NOM.SG]</ta>
            <ta e="T24" id="Seg_2270" s="T23">work</ta>
            <ta e="T25" id="Seg_2271" s="T24">work-DUR.PST.[3SG]</ta>
            <ta e="T26" id="Seg_2272" s="T25">what.[NOM.SG]</ta>
            <ta e="T27" id="Seg_2273" s="T26">and</ta>
            <ta e="T28" id="Seg_2274" s="T27">this-LAT</ta>
            <ta e="T29" id="Seg_2275" s="T28">always</ta>
            <ta e="T30" id="Seg_2276" s="T29">NEG</ta>
            <ta e="T31" id="Seg_2277" s="T30">good</ta>
            <ta e="T32" id="Seg_2278" s="T31">be-PST.[3SG]</ta>
            <ta e="T33" id="Seg_2279" s="T32">say-IPFVZ.[3SG]</ta>
            <ta e="T34" id="Seg_2280" s="T33">you.NOM</ta>
            <ta e="T35" id="Seg_2281" s="T34">very</ta>
            <ta e="T36" id="Seg_2282" s="T35">lazy.[NOM.SG]</ta>
            <ta e="T37" id="Seg_2283" s="T36">NEG</ta>
            <ta e="T38" id="Seg_2284" s="T37">work-PRS-2SG</ta>
            <ta e="T39" id="Seg_2285" s="T38">then</ta>
            <ta e="T40" id="Seg_2286" s="T39">this.[NOM.SG]</ta>
            <ta e="T41" id="Seg_2287" s="T40">man-LAT</ta>
            <ta e="T42" id="Seg_2288" s="T41">say-IPFVZ.[3SG]</ta>
            <ta e="T43" id="Seg_2289" s="T42">take.away-IMP.2SG.O</ta>
            <ta e="T44" id="Seg_2290" s="T43">self-NOM/GEN/ACC.2SG</ta>
            <ta e="T45" id="Seg_2291" s="T44">girl.[NOM.SG]</ta>
            <ta e="T46" id="Seg_2292" s="T45">forest-LAT</ta>
            <ta e="T47" id="Seg_2293" s="T46">and</ta>
            <ta e="T48" id="Seg_2294" s="T47">there</ta>
            <ta e="T50" id="Seg_2295" s="T49">PTCL</ta>
            <ta e="T51" id="Seg_2296" s="T50">there</ta>
            <ta e="T52" id="Seg_2297" s="T51">seat-IMP.2SG.O</ta>
            <ta e="T53" id="Seg_2298" s="T52">this.[NOM.SG]</ta>
            <ta e="T54" id="Seg_2299" s="T53">take.away-PST.[3SG]</ta>
            <ta e="T55" id="Seg_2300" s="T54">there</ta>
            <ta e="T56" id="Seg_2301" s="T55">seat-PST.[3SG]</ta>
            <ta e="T57" id="Seg_2302" s="T56">this</ta>
            <ta e="T58" id="Seg_2303" s="T57">then</ta>
            <ta e="T59" id="Seg_2304" s="T58">this.[NOM.SG]</ta>
            <ta e="T60" id="Seg_2305" s="T59">house.[NOM.SG]</ta>
            <ta e="T61" id="Seg_2306" s="T60">there</ta>
            <ta e="T62" id="Seg_2307" s="T61">find-PST.[3SG]</ta>
            <ta e="T63" id="Seg_2308" s="T62">come-PST.[3SG]</ta>
            <ta e="T64" id="Seg_2309" s="T63">house-LAT</ta>
            <ta e="T65" id="Seg_2310" s="T64">mouse.[NOM.SG]</ta>
            <ta e="T66" id="Seg_2311" s="T65">bring-IMP.2SG</ta>
            <ta e="T67" id="Seg_2312" s="T66">I.LAT</ta>
            <ta e="T68" id="Seg_2313" s="T67">bread.[NOM.SG]</ta>
            <ta e="T69" id="Seg_2314" s="T68">this.[NOM.SG]</ta>
            <ta e="T70" id="Seg_2315" s="T69">this-LAT</ta>
            <ta e="T71" id="Seg_2316" s="T70">go-PST.[3SG]</ta>
            <ta e="T72" id="Seg_2317" s="T71">then</ta>
            <ta e="T73" id="Seg_2318" s="T72">man.[NOM.SG]</ta>
            <ta e="T74" id="Seg_2319" s="T73">come-PST.[3SG]</ta>
            <ta e="T75" id="Seg_2320" s="T74">evening</ta>
            <ta e="T76" id="Seg_2321" s="T75">become-RES-PST.[3SG]</ta>
            <ta e="T78" id="Seg_2322" s="T77">this-LAT</ta>
            <ta e="T79" id="Seg_2323" s="T78">bell.[NOM.SG]</ta>
            <ta e="T80" id="Seg_2324" s="T79">give-PST.[3SG]</ta>
            <ta e="T81" id="Seg_2325" s="T80">you.NOM</ta>
            <ta e="T82" id="Seg_2326" s="T81">run-CVB</ta>
            <ta e="T83" id="Seg_2327" s="T82">and</ta>
            <ta e="T84" id="Seg_2328" s="T83">I.NOM</ta>
            <ta e="T85" id="Seg_2329" s="T84">you.ACC</ta>
            <ta e="T87" id="Seg_2330" s="T86">capture-FUT-1SG</ta>
            <ta e="T88" id="Seg_2331" s="T87">then</ta>
            <ta e="T89" id="Seg_2332" s="T88">mouse.[NOM.SG]</ta>
            <ta e="T90" id="Seg_2333" s="T89">come-PST.[3SG]</ta>
            <ta e="T91" id="Seg_2334" s="T90">bell.[NOM.SG]</ta>
            <ta e="T92" id="Seg_2335" s="T91">take-PST.[3SG]</ta>
            <ta e="T93" id="Seg_2336" s="T92">and</ta>
            <ta e="T94" id="Seg_2337" s="T93">house-LOC</ta>
            <ta e="T95" id="Seg_2338" s="T94">PTCL</ta>
            <ta e="T96" id="Seg_2339" s="T95">run-DUR.PST.[3SG]</ta>
            <ta e="T97" id="Seg_2340" s="T96">this.[NOM.SG]</ta>
            <ta e="T99" id="Seg_2341" s="T98">this-ACC</ta>
            <ta e="T100" id="Seg_2342" s="T99">capture-MOM-PST.[3SG]</ta>
            <ta e="T101" id="Seg_2343" s="T100">capture-MOM-PST.[3SG]</ta>
            <ta e="T102" id="Seg_2344" s="T101">NEG</ta>
            <ta e="T104" id="Seg_2345" s="T103">can-PST.[3SG]</ta>
            <ta e="T105" id="Seg_2346" s="T104">capture-INF.LAT</ta>
            <ta e="T106" id="Seg_2347" s="T105">girl.[NOM.SG]</ta>
            <ta e="T107" id="Seg_2348" s="T106">hide-RES-PST.[3SG]</ta>
            <ta e="T108" id="Seg_2349" s="T107">morning</ta>
            <ta e="T109" id="Seg_2350" s="T108">become-RES-PST.[3SG]</ta>
            <ta e="T110" id="Seg_2351" s="T109">this.[NOM.SG]</ta>
            <ta e="T111" id="Seg_2352" s="T110">get.up-PST.[3SG]</ta>
            <ta e="T112" id="Seg_2353" s="T111">this-LAT</ta>
            <ta e="T113" id="Seg_2354" s="T112">go-PST.[3SG]</ta>
            <ta e="T114" id="Seg_2355" s="T113">money.[NOM.SG]</ta>
            <ta e="T115" id="Seg_2356" s="T114">clothing.[NOM.SG]</ta>
            <ta e="T116" id="Seg_2357" s="T115">many</ta>
            <ta e="T117" id="Seg_2358" s="T116">boot-PL</ta>
            <ta e="T118" id="Seg_2359" s="T117">give-PST.[3SG]</ta>
            <ta e="T119" id="Seg_2360" s="T118">scarf.[NOM.SG]</ta>
            <ta e="T120" id="Seg_2361" s="T119">many</ta>
            <ta e="T121" id="Seg_2362" s="T120">then</ta>
            <ta e="T122" id="Seg_2363" s="T121">this.[NOM.SG]</ta>
            <ta e="T123" id="Seg_2364" s="T122">woman.[NOM.SG]</ta>
            <ta e="T124" id="Seg_2365" s="T123">go-EP-IMP.2SG</ta>
            <ta e="T125" id="Seg_2366" s="T124">daughter-NOM/GEN/ACC.2SG</ta>
            <ta e="T126" id="Seg_2367" s="T125">there</ta>
            <ta e="T127" id="Seg_2368" s="T126">bone.DIM-PL</ta>
            <ta e="T128" id="Seg_2369" s="T127">take-IMP.2SG</ta>
            <ta e="T129" id="Seg_2370" s="T128">collect-FUT-2SG</ta>
            <ta e="T130" id="Seg_2371" s="T129">and</ta>
            <ta e="T131" id="Seg_2372" s="T130">bring-IMP.2SG.O</ta>
            <ta e="T132" id="Seg_2373" s="T131">place-LAT</ta>
            <ta e="T133" id="Seg_2374" s="T132">dig-FUT-1PL</ta>
            <ta e="T134" id="Seg_2375" s="T133">this.[NOM.SG]</ta>
            <ta e="T135" id="Seg_2376" s="T134">go-PST.[3SG]</ta>
            <ta e="T136" id="Seg_2377" s="T135">come-PST.[3SG]</ta>
            <ta e="T137" id="Seg_2378" s="T136">daughter-GEN</ta>
            <ta e="T138" id="Seg_2379" s="T137">PTCL</ta>
            <ta e="T139" id="Seg_2380" s="T138">money.[NOM.SG]</ta>
            <ta e="T140" id="Seg_2381" s="T139">many</ta>
            <ta e="T141" id="Seg_2382" s="T140">clothing.[NOM.SG]</ta>
            <ta e="T142" id="Seg_2383" s="T141">many</ta>
            <ta e="T143" id="Seg_2384" s="T142">this-ACC</ta>
            <ta e="T144" id="Seg_2385" s="T143">take-PST.[3SG]</ta>
            <ta e="T145" id="Seg_2386" s="T144">seat-PST.[3SG]</ta>
            <ta e="T146" id="Seg_2387" s="T145">horse-LAT</ta>
            <ta e="T147" id="Seg_2388" s="T146">come-PRS.[3SG]</ta>
            <ta e="T148" id="Seg_2389" s="T147">and</ta>
            <ta e="T149" id="Seg_2390" s="T148">dog.[NOM.SG]</ta>
            <ta e="T150" id="Seg_2391" s="T149">PTCL</ta>
            <ta e="T151" id="Seg_2392" s="T150">shout-DUR.[3SG]</ta>
            <ta e="T153" id="Seg_2393" s="T152">man-GEN</ta>
            <ta e="T154" id="Seg_2394" s="T153">girl.[NOM.SG]</ta>
            <ta e="T155" id="Seg_2395" s="T154">come-PRS.[3SG]</ta>
            <ta e="T156" id="Seg_2396" s="T155">money.[NOM.SG]</ta>
            <ta e="T157" id="Seg_2397" s="T156">bring-%%</ta>
            <ta e="T158" id="Seg_2398" s="T157">and</ta>
            <ta e="T159" id="Seg_2399" s="T158">clothing.[NOM.SG]</ta>
            <ta e="T160" id="Seg_2400" s="T159">many</ta>
            <ta e="T161" id="Seg_2401" s="T160">bring-%%</ta>
            <ta e="T162" id="Seg_2402" s="T161">and</ta>
            <ta e="T163" id="Seg_2403" s="T162">this.[NOM.SG]</ta>
            <ta e="T164" id="Seg_2404" s="T163">woman.[NOM.SG]</ta>
            <ta e="T165" id="Seg_2405" s="T164">say-IPFVZ.[3SG]</ta>
            <ta e="T166" id="Seg_2406" s="T165">NEG.AUX-IMP.2SG</ta>
            <ta e="T167" id="Seg_2407" s="T166">lie-CNG</ta>
            <ta e="T168" id="Seg_2408" s="T167">then</ta>
            <ta e="T169" id="Seg_2409" s="T168">man.[NOM.SG]</ta>
            <ta e="T170" id="Seg_2410" s="T169">come-PST.[3SG]</ta>
            <ta e="T171" id="Seg_2411" s="T170">many</ta>
            <ta e="T172" id="Seg_2412" s="T171">bring-PST.[3SG]</ta>
            <ta e="T173" id="Seg_2413" s="T172">and</ta>
            <ta e="T174" id="Seg_2414" s="T173">money.[NOM.SG]</ta>
            <ta e="T175" id="Seg_2415" s="T174">and</ta>
            <ta e="T176" id="Seg_2416" s="T175">daughter-ACC.3SG</ta>
            <ta e="T177" id="Seg_2417" s="T176">bring-PST.[3SG]</ta>
            <ta e="T178" id="Seg_2418" s="T177">then</ta>
            <ta e="T179" id="Seg_2419" s="T178">say-IPFVZ.[3SG]</ta>
            <ta e="T180" id="Seg_2420" s="T179">bring-IMP.2SG.O</ta>
            <ta e="T181" id="Seg_2421" s="T180">I.NOM</ta>
            <ta e="T182" id="Seg_2422" s="T181">daughter-NOM/GEN/ACC.1SG</ta>
            <ta e="T183" id="Seg_2423" s="T182">there</ta>
            <ta e="T184" id="Seg_2424" s="T183">seat-IMP.2SG.O</ta>
            <ta e="T185" id="Seg_2425" s="T184">this.[NOM.SG]</ta>
            <ta e="T186" id="Seg_2426" s="T185">take.away-PST.[3SG]</ta>
            <ta e="T187" id="Seg_2427" s="T186">this.[NOM.SG]</ta>
            <ta e="T188" id="Seg_2428" s="T187">daughter-NOM/GEN/ACC.1SG</ta>
            <ta e="T189" id="Seg_2429" s="T188">mouse.[NOM.SG]</ta>
            <ta e="T190" id="Seg_2430" s="T189">%%-PST.[3SG]</ta>
            <ta e="T192" id="Seg_2431" s="T191">NEG</ta>
            <ta e="T193" id="Seg_2432" s="T192">give-PST.[3SG]</ta>
            <ta e="T194" id="Seg_2433" s="T193">then</ta>
            <ta e="T195" id="Seg_2434" s="T194">evening-LOC.ADV</ta>
            <ta e="T196" id="Seg_2435" s="T195">find-PST.[3SG]</ta>
            <ta e="T197" id="Seg_2436" s="T196">house.[NOM.SG]</ta>
            <ta e="T198" id="Seg_2437" s="T197">house-LAT</ta>
            <ta e="T200" id="Seg_2438" s="T199">come-PST.[3SG]</ta>
            <ta e="T201" id="Seg_2439" s="T200">there</ta>
            <ta e="T202" id="Seg_2440" s="T201">man.[NOM.SG]</ta>
            <ta e="T203" id="Seg_2441" s="T202">say-IPFVZ.[3SG]</ta>
            <ta e="T204" id="Seg_2442" s="T203">today</ta>
            <ta e="T205" id="Seg_2443" s="T204">evening-LOC.ADV</ta>
            <ta e="T206" id="Seg_2444" s="T205">bell.[NOM.SG]</ta>
            <ta e="T209" id="Seg_2445" s="T208">I.NOM</ta>
            <ta e="T210" id="Seg_2446" s="T209">you.ACC</ta>
            <ta e="T211" id="Seg_2447" s="T210">capture-FUT-1SG</ta>
            <ta e="T212" id="Seg_2448" s="T211">and</ta>
            <ta e="T213" id="Seg_2449" s="T212">mouse.[NOM.SG]</ta>
            <ta e="T214" id="Seg_2450" s="T213">NEG</ta>
            <ta e="T215" id="Seg_2451" s="T214">come-PST.[3SG]</ta>
            <ta e="T216" id="Seg_2452" s="T215">this.[NOM.SG]</ta>
            <ta e="T217" id="Seg_2453" s="T216">bell.[NOM.SG]</ta>
            <ta e="T218" id="Seg_2454" s="T217">take-PST.[3SG]</ta>
            <ta e="T219" id="Seg_2455" s="T218">run-MOM-PST.[3SG]</ta>
            <ta e="T220" id="Seg_2456" s="T219">run-MOM-PST.[3SG]</ta>
            <ta e="T221" id="Seg_2457" s="T220">this.[NOM.SG]</ta>
            <ta e="T222" id="Seg_2458" s="T221">this-ACC</ta>
            <ta e="T223" id="Seg_2459" s="T222">capture-PST.[3SG]</ta>
            <ta e="T224" id="Seg_2460" s="T223">and</ta>
            <ta e="T225" id="Seg_2461" s="T224">PTCL</ta>
            <ta e="T226" id="Seg_2462" s="T225">eat-MOM-PST.[3SG]</ta>
            <ta e="T227" id="Seg_2463" s="T226">PTCL</ta>
            <ta e="T228" id="Seg_2464" s="T227">then</ta>
            <ta e="T230" id="Seg_2465" s="T229">say-IPFVZ.[3SG]</ta>
            <ta e="T231" id="Seg_2466" s="T230">go-EP-IMP.2SG</ta>
            <ta e="T232" id="Seg_2467" s="T231">I.NOM</ta>
            <ta e="T233" id="Seg_2468" s="T232">girl.[NOM.SG]</ta>
            <ta e="T234" id="Seg_2469" s="T233">take-CVB</ta>
            <ta e="T235" id="Seg_2470" s="T234">this.[NOM.SG]</ta>
            <ta e="T236" id="Seg_2471" s="T235">go-PST.[3SG]</ta>
            <ta e="T237" id="Seg_2472" s="T236">sack.[NOM.SG]</ta>
            <ta e="T238" id="Seg_2473" s="T237">take-PST.[3SG]</ta>
            <ta e="T239" id="Seg_2474" s="T238">come-PST.[3SG]</ta>
            <ta e="T240" id="Seg_2475" s="T239">one.[NOM.SG]</ta>
            <ta e="T241" id="Seg_2476" s="T240">bone.DIM-PL</ta>
            <ta e="T242" id="Seg_2477" s="T241">lie-DUR-3PL </ta>
            <ta e="T243" id="Seg_2478" s="T242">collect-PST.[3SG]</ta>
            <ta e="T244" id="Seg_2479" s="T243">and</ta>
            <ta e="T245" id="Seg_2480" s="T244">bring-PST.[3SG]</ta>
            <ta e="T246" id="Seg_2481" s="T245">then</ta>
            <ta e="T248" id="Seg_2482" s="T247">dog.[NOM.SG]</ta>
            <ta e="T249" id="Seg_2483" s="T248">again</ta>
            <ta e="T251" id="Seg_2484" s="T250">shout-DUR.[3SG]</ta>
            <ta e="T252" id="Seg_2485" s="T251">woman-GEN</ta>
            <ta e="T253" id="Seg_2486" s="T252">daughter-NOM/GEN.3SG</ta>
            <ta e="T254" id="Seg_2487" s="T253">PTCL</ta>
            <ta e="T255" id="Seg_2488" s="T254">bone.DIM-PL</ta>
            <ta e="T257" id="Seg_2489" s="T256">bring-DUR.[3SG]</ta>
            <ta e="T258" id="Seg_2490" s="T257">man.[NOM.SG]</ta>
            <ta e="T259" id="Seg_2491" s="T258">NEG.AUX-IMP.2SG</ta>
            <ta e="T260" id="Seg_2492" s="T259">lie-CNG</ta>
            <ta e="T261" id="Seg_2493" s="T260">otherwise</ta>
            <ta e="T263" id="Seg_2494" s="T262">you.DAT</ta>
            <ta e="T264" id="Seg_2495" s="T263">then</ta>
            <ta e="T265" id="Seg_2496" s="T264">man.[NOM.SG]</ta>
            <ta e="T267" id="Seg_2497" s="T266">come-PST.[3SG]</ta>
            <ta e="T268" id="Seg_2498" s="T267">one.[NOM.SG]</ta>
            <ta e="T269" id="Seg_2499" s="T268">bone.DIM-PL</ta>
            <ta e="T270" id="Seg_2500" s="T269">bring-PST.[3SG]</ta>
            <ta e="T271" id="Seg_2501" s="T270">and</ta>
            <ta e="T273" id="Seg_2502" s="T272">this.[NOM.SG]</ta>
            <ta e="T274" id="Seg_2503" s="T273">daughter</ta>
            <ta e="T276" id="Seg_2504" s="T275">place-LAT</ta>
            <ta e="T277" id="Seg_2505" s="T276">put-PST-3PL</ta>
            <ta e="T278" id="Seg_2506" s="T277">and</ta>
            <ta e="T279" id="Seg_2507" s="T278">dig-PST-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_2508" s="T0">один.[NOM.SG]</ta>
            <ta e="T2" id="Seg_2509" s="T1">мужчина.[NOM.SG]</ta>
            <ta e="T3" id="Seg_2510" s="T2">жить-PST.[3SG]</ta>
            <ta e="T4" id="Seg_2511" s="T3">этот-GEN</ta>
            <ta e="T5" id="Seg_2512" s="T4">женщина.[NOM.SG]</ta>
            <ta e="T6" id="Seg_2513" s="T5">умереть-RES-PST.[3SG]</ta>
            <ta e="T7" id="Seg_2514" s="T6">этот.[NOM.SG]</ta>
            <ta e="T8" id="Seg_2515" s="T7">взять-PST.[3SG]</ta>
            <ta e="T9" id="Seg_2516" s="T8">другой.[NOM.SG]</ta>
            <ta e="T10" id="Seg_2517" s="T9">женщина.[NOM.SG]</ta>
            <ta e="T11" id="Seg_2518" s="T10">этот-GEN</ta>
            <ta e="T12" id="Seg_2519" s="T11">женщина-GEN</ta>
            <ta e="T13" id="Seg_2520" s="T12">девушка.[NOM.SG]</ta>
            <ta e="T14" id="Seg_2521" s="T13">быть-PST.[3SG]</ta>
            <ta e="T15" id="Seg_2522" s="T14">и</ta>
            <ta e="T16" id="Seg_2523" s="T15">этот.[NOM.SG]</ta>
            <ta e="T17" id="Seg_2524" s="T16">мужчина-GEN</ta>
            <ta e="T18" id="Seg_2525" s="T17">девушка.[NOM.SG]</ta>
            <ta e="T19" id="Seg_2526" s="T18">этот.[NOM.SG]</ta>
            <ta e="T20" id="Seg_2527" s="T19">мужчина-GEN</ta>
            <ta e="T21" id="Seg_2528" s="T20">дочь-NOM/GEN.3SG</ta>
            <ta e="T22" id="Seg_2529" s="T21">весь</ta>
            <ta e="T23" id="Seg_2530" s="T22">что.[NOM.SG]</ta>
            <ta e="T24" id="Seg_2531" s="T23">работать</ta>
            <ta e="T25" id="Seg_2532" s="T24">работать-DUR.PST.[3SG]</ta>
            <ta e="T26" id="Seg_2533" s="T25">что.[NOM.SG]</ta>
            <ta e="T27" id="Seg_2534" s="T26">а</ta>
            <ta e="T28" id="Seg_2535" s="T27">этот-LAT</ta>
            <ta e="T29" id="Seg_2536" s="T28">всегда</ta>
            <ta e="T30" id="Seg_2537" s="T29">NEG</ta>
            <ta e="T31" id="Seg_2538" s="T30">хороший</ta>
            <ta e="T32" id="Seg_2539" s="T31">быть-PST.[3SG]</ta>
            <ta e="T33" id="Seg_2540" s="T32">сказать-IPFVZ.[3SG]</ta>
            <ta e="T34" id="Seg_2541" s="T33">ты.NOM</ta>
            <ta e="T35" id="Seg_2542" s="T34">очень</ta>
            <ta e="T36" id="Seg_2543" s="T35">ленивый.[NOM.SG]</ta>
            <ta e="T37" id="Seg_2544" s="T36">NEG</ta>
            <ta e="T38" id="Seg_2545" s="T37">работать-PRS-2SG</ta>
            <ta e="T39" id="Seg_2546" s="T38">тогда</ta>
            <ta e="T40" id="Seg_2547" s="T39">этот.[NOM.SG]</ta>
            <ta e="T41" id="Seg_2548" s="T40">мужчина-LAT</ta>
            <ta e="T42" id="Seg_2549" s="T41">сказать-IPFVZ.[3SG]</ta>
            <ta e="T43" id="Seg_2550" s="T42">унести-IMP.2SG.O</ta>
            <ta e="T44" id="Seg_2551" s="T43">сам-NOM/GEN/ACC.2SG</ta>
            <ta e="T45" id="Seg_2552" s="T44">девушка.[NOM.SG]</ta>
            <ta e="T46" id="Seg_2553" s="T45">лес-LAT</ta>
            <ta e="T47" id="Seg_2554" s="T46">и</ta>
            <ta e="T48" id="Seg_2555" s="T47">там</ta>
            <ta e="T50" id="Seg_2556" s="T49">PTCL</ta>
            <ta e="T51" id="Seg_2557" s="T50">там</ta>
            <ta e="T52" id="Seg_2558" s="T51">сажать-IMP.2SG.O</ta>
            <ta e="T53" id="Seg_2559" s="T52">этот.[NOM.SG]</ta>
            <ta e="T54" id="Seg_2560" s="T53">унести-PST.[3SG]</ta>
            <ta e="T55" id="Seg_2561" s="T54">там</ta>
            <ta e="T56" id="Seg_2562" s="T55">сажать-PST.[3SG]</ta>
            <ta e="T57" id="Seg_2563" s="T56">этот</ta>
            <ta e="T58" id="Seg_2564" s="T57">тогда</ta>
            <ta e="T59" id="Seg_2565" s="T58">этот.[NOM.SG]</ta>
            <ta e="T60" id="Seg_2566" s="T59">дом.[NOM.SG]</ta>
            <ta e="T61" id="Seg_2567" s="T60">там</ta>
            <ta e="T62" id="Seg_2568" s="T61">найти-PST.[3SG]</ta>
            <ta e="T63" id="Seg_2569" s="T62">прийти-PST.[3SG]</ta>
            <ta e="T64" id="Seg_2570" s="T63">дом-LAT</ta>
            <ta e="T65" id="Seg_2571" s="T64">мышь.[NOM.SG]</ta>
            <ta e="T66" id="Seg_2572" s="T65">принести-IMP.2SG</ta>
            <ta e="T67" id="Seg_2573" s="T66">я.LAT</ta>
            <ta e="T68" id="Seg_2574" s="T67">хлеб.[NOM.SG]</ta>
            <ta e="T69" id="Seg_2575" s="T68">этот.[NOM.SG]</ta>
            <ta e="T70" id="Seg_2576" s="T69">этот-LAT</ta>
            <ta e="T71" id="Seg_2577" s="T70">идти-PST.[3SG]</ta>
            <ta e="T72" id="Seg_2578" s="T71">тогда</ta>
            <ta e="T73" id="Seg_2579" s="T72">мужчина.[NOM.SG]</ta>
            <ta e="T74" id="Seg_2580" s="T73">прийти-PST.[3SG]</ta>
            <ta e="T75" id="Seg_2581" s="T74">вечер</ta>
            <ta e="T76" id="Seg_2582" s="T75">стать-RES-PST.[3SG]</ta>
            <ta e="T78" id="Seg_2583" s="T77">этот-LAT</ta>
            <ta e="T79" id="Seg_2584" s="T78">колокол.[NOM.SG]</ta>
            <ta e="T80" id="Seg_2585" s="T79">дать-PST.[3SG]</ta>
            <ta e="T81" id="Seg_2586" s="T80">ты.NOM</ta>
            <ta e="T82" id="Seg_2587" s="T81">бежать-CVB</ta>
            <ta e="T83" id="Seg_2588" s="T82">а</ta>
            <ta e="T84" id="Seg_2589" s="T83">я.NOM</ta>
            <ta e="T85" id="Seg_2590" s="T84">ты.ACC</ta>
            <ta e="T87" id="Seg_2591" s="T86">ловить-FUT-1SG</ta>
            <ta e="T88" id="Seg_2592" s="T87">тогда</ta>
            <ta e="T89" id="Seg_2593" s="T88">мышь.[NOM.SG]</ta>
            <ta e="T90" id="Seg_2594" s="T89">прийти-PST.[3SG]</ta>
            <ta e="T91" id="Seg_2595" s="T90">колокол.[NOM.SG]</ta>
            <ta e="T92" id="Seg_2596" s="T91">взять-PST.[3SG]</ta>
            <ta e="T93" id="Seg_2597" s="T92">и</ta>
            <ta e="T94" id="Seg_2598" s="T93">дом-LOC</ta>
            <ta e="T95" id="Seg_2599" s="T94">PTCL</ta>
            <ta e="T96" id="Seg_2600" s="T95">бежать-DUR.PST.[3SG]</ta>
            <ta e="T97" id="Seg_2601" s="T96">этот.[NOM.SG]</ta>
            <ta e="T99" id="Seg_2602" s="T98">этот-ACC</ta>
            <ta e="T100" id="Seg_2603" s="T99">ловить-MOM-PST.[3SG]</ta>
            <ta e="T101" id="Seg_2604" s="T100">ловить-MOM-PST.[3SG]</ta>
            <ta e="T102" id="Seg_2605" s="T101">NEG</ta>
            <ta e="T104" id="Seg_2606" s="T103">мочь-PST.[3SG]</ta>
            <ta e="T105" id="Seg_2607" s="T104">ловить-INF.LAT</ta>
            <ta e="T106" id="Seg_2608" s="T105">девушка.[NOM.SG]</ta>
            <ta e="T107" id="Seg_2609" s="T106">спрятаться-RES-PST.[3SG]</ta>
            <ta e="T108" id="Seg_2610" s="T107">утро</ta>
            <ta e="T109" id="Seg_2611" s="T108">стать-RES-PST.[3SG]</ta>
            <ta e="T110" id="Seg_2612" s="T109">этот.[NOM.SG]</ta>
            <ta e="T111" id="Seg_2613" s="T110">встать-PST.[3SG]</ta>
            <ta e="T112" id="Seg_2614" s="T111">этот-LAT</ta>
            <ta e="T113" id="Seg_2615" s="T112">идти-PST.[3SG]</ta>
            <ta e="T114" id="Seg_2616" s="T113">деньги.[NOM.SG]</ta>
            <ta e="T115" id="Seg_2617" s="T114">одежда.[NOM.SG]</ta>
            <ta e="T116" id="Seg_2618" s="T115">много</ta>
            <ta e="T117" id="Seg_2619" s="T116">сапог-PL</ta>
            <ta e="T118" id="Seg_2620" s="T117">дать-PST.[3SG]</ta>
            <ta e="T119" id="Seg_2621" s="T118">платок.[NOM.SG]</ta>
            <ta e="T120" id="Seg_2622" s="T119">много</ta>
            <ta e="T121" id="Seg_2623" s="T120">тогда</ta>
            <ta e="T122" id="Seg_2624" s="T121">этот.[NOM.SG]</ta>
            <ta e="T123" id="Seg_2625" s="T122">женщина.[NOM.SG]</ta>
            <ta e="T124" id="Seg_2626" s="T123">пойти-EP-IMP.2SG</ta>
            <ta e="T125" id="Seg_2627" s="T124">дочь-NOM/GEN/ACC.2SG</ta>
            <ta e="T126" id="Seg_2628" s="T125">там</ta>
            <ta e="T127" id="Seg_2629" s="T126">кость.DIM-PL</ta>
            <ta e="T128" id="Seg_2630" s="T127">взять-IMP.2SG</ta>
            <ta e="T129" id="Seg_2631" s="T128">собирать-FUT-2SG</ta>
            <ta e="T130" id="Seg_2632" s="T129">и</ta>
            <ta e="T131" id="Seg_2633" s="T130">принести-IMP.2SG.O</ta>
            <ta e="T132" id="Seg_2634" s="T131">место-LAT</ta>
            <ta e="T133" id="Seg_2635" s="T132">копать-FUT-1PL</ta>
            <ta e="T134" id="Seg_2636" s="T133">этот.[NOM.SG]</ta>
            <ta e="T135" id="Seg_2637" s="T134">пойти-PST.[3SG]</ta>
            <ta e="T136" id="Seg_2638" s="T135">прийти-PST.[3SG]</ta>
            <ta e="T137" id="Seg_2639" s="T136">дочь-GEN</ta>
            <ta e="T138" id="Seg_2640" s="T137">PTCL</ta>
            <ta e="T139" id="Seg_2641" s="T138">деньги.[NOM.SG]</ta>
            <ta e="T140" id="Seg_2642" s="T139">много</ta>
            <ta e="T141" id="Seg_2643" s="T140">одежда.[NOM.SG]</ta>
            <ta e="T142" id="Seg_2644" s="T141">много</ta>
            <ta e="T143" id="Seg_2645" s="T142">этот-ACC</ta>
            <ta e="T144" id="Seg_2646" s="T143">взять-PST.[3SG]</ta>
            <ta e="T145" id="Seg_2647" s="T144">сажать-PST.[3SG]</ta>
            <ta e="T146" id="Seg_2648" s="T145">лошадь-LAT</ta>
            <ta e="T147" id="Seg_2649" s="T146">прийти-PRS.[3SG]</ta>
            <ta e="T148" id="Seg_2650" s="T147">а</ta>
            <ta e="T149" id="Seg_2651" s="T148">собака.[NOM.SG]</ta>
            <ta e="T150" id="Seg_2652" s="T149">PTCL</ta>
            <ta e="T151" id="Seg_2653" s="T150">кричать-DUR.[3SG]</ta>
            <ta e="T153" id="Seg_2654" s="T152">мужчина-GEN</ta>
            <ta e="T154" id="Seg_2655" s="T153">девушка.[NOM.SG]</ta>
            <ta e="T155" id="Seg_2656" s="T154">прийти-PRS.[3SG]</ta>
            <ta e="T156" id="Seg_2657" s="T155">деньги.[NOM.SG]</ta>
            <ta e="T157" id="Seg_2658" s="T156">принести-%%</ta>
            <ta e="T158" id="Seg_2659" s="T157">и</ta>
            <ta e="T159" id="Seg_2660" s="T158">одежда.[NOM.SG]</ta>
            <ta e="T160" id="Seg_2661" s="T159">много</ta>
            <ta e="T161" id="Seg_2662" s="T160">принести-%%</ta>
            <ta e="T162" id="Seg_2663" s="T161">а</ta>
            <ta e="T163" id="Seg_2664" s="T162">этот.[NOM.SG]</ta>
            <ta e="T164" id="Seg_2665" s="T163">женщина.[NOM.SG]</ta>
            <ta e="T165" id="Seg_2666" s="T164">сказать-IPFVZ.[3SG]</ta>
            <ta e="T166" id="Seg_2667" s="T165">NEG.AUX-IMP.2SG</ta>
            <ta e="T167" id="Seg_2668" s="T166">лгать-CNG</ta>
            <ta e="T168" id="Seg_2669" s="T167">тогда</ta>
            <ta e="T169" id="Seg_2670" s="T168">мужчина.[NOM.SG]</ta>
            <ta e="T170" id="Seg_2671" s="T169">прийти-PST.[3SG]</ta>
            <ta e="T171" id="Seg_2672" s="T170">много</ta>
            <ta e="T172" id="Seg_2673" s="T171">принести-PST.[3SG]</ta>
            <ta e="T173" id="Seg_2674" s="T172">и</ta>
            <ta e="T174" id="Seg_2675" s="T173">деньги.[NOM.SG]</ta>
            <ta e="T175" id="Seg_2676" s="T174">и</ta>
            <ta e="T176" id="Seg_2677" s="T175">дочь-ACC.3SG</ta>
            <ta e="T177" id="Seg_2678" s="T176">принести-PST.[3SG]</ta>
            <ta e="T178" id="Seg_2679" s="T177">тогда</ta>
            <ta e="T179" id="Seg_2680" s="T178">сказать-IPFVZ.[3SG]</ta>
            <ta e="T180" id="Seg_2681" s="T179">нести-IMP.2SG.O</ta>
            <ta e="T181" id="Seg_2682" s="T180">я.NOM</ta>
            <ta e="T182" id="Seg_2683" s="T181">дочь-NOM/GEN/ACC.1SG</ta>
            <ta e="T183" id="Seg_2684" s="T182">там</ta>
            <ta e="T184" id="Seg_2685" s="T183">сажать-IMP.2SG.O</ta>
            <ta e="T185" id="Seg_2686" s="T184">этот.[NOM.SG]</ta>
            <ta e="T186" id="Seg_2687" s="T185">унести-PST.[3SG]</ta>
            <ta e="T187" id="Seg_2688" s="T186">этот.[NOM.SG]</ta>
            <ta e="T188" id="Seg_2689" s="T187">дочь-NOM/GEN/ACC.1SG</ta>
            <ta e="T189" id="Seg_2690" s="T188">мышь.[NOM.SG]</ta>
            <ta e="T190" id="Seg_2691" s="T189">%%-PST.[3SG]</ta>
            <ta e="T192" id="Seg_2692" s="T191">NEG</ta>
            <ta e="T193" id="Seg_2693" s="T192">дать-PST.[3SG]</ta>
            <ta e="T194" id="Seg_2694" s="T193">тогда</ta>
            <ta e="T195" id="Seg_2695" s="T194">вечер-LOC.ADV</ta>
            <ta e="T196" id="Seg_2696" s="T195">найти-PST.[3SG]</ta>
            <ta e="T197" id="Seg_2697" s="T196">дом.[NOM.SG]</ta>
            <ta e="T198" id="Seg_2698" s="T197">дом-LAT</ta>
            <ta e="T200" id="Seg_2699" s="T199">прийти-PST.[3SG]</ta>
            <ta e="T201" id="Seg_2700" s="T200">там</ta>
            <ta e="T202" id="Seg_2701" s="T201">мужчина.[NOM.SG]</ta>
            <ta e="T203" id="Seg_2702" s="T202">сказать-IPFVZ.[3SG]</ta>
            <ta e="T204" id="Seg_2703" s="T203">сегодня</ta>
            <ta e="T205" id="Seg_2704" s="T204">вечер-LOC.ADV</ta>
            <ta e="T206" id="Seg_2705" s="T205">колокол.[NOM.SG]</ta>
            <ta e="T209" id="Seg_2706" s="T208">я.NOM</ta>
            <ta e="T210" id="Seg_2707" s="T209">ты.ACC</ta>
            <ta e="T211" id="Seg_2708" s="T210">ловить-FUT-1SG</ta>
            <ta e="T212" id="Seg_2709" s="T211">а</ta>
            <ta e="T213" id="Seg_2710" s="T212">мышь.[NOM.SG]</ta>
            <ta e="T214" id="Seg_2711" s="T213">NEG</ta>
            <ta e="T215" id="Seg_2712" s="T214">прийти-PST.[3SG]</ta>
            <ta e="T216" id="Seg_2713" s="T215">этот.[NOM.SG]</ta>
            <ta e="T217" id="Seg_2714" s="T216">колокол.[NOM.SG]</ta>
            <ta e="T218" id="Seg_2715" s="T217">взять-PST.[3SG]</ta>
            <ta e="T219" id="Seg_2716" s="T218">бежать-MOM-PST.[3SG]</ta>
            <ta e="T220" id="Seg_2717" s="T219">бежать-MOM-PST.[3SG]</ta>
            <ta e="T221" id="Seg_2718" s="T220">этот.[NOM.SG]</ta>
            <ta e="T222" id="Seg_2719" s="T221">этот-ACC</ta>
            <ta e="T223" id="Seg_2720" s="T222">ловить-PST.[3SG]</ta>
            <ta e="T224" id="Seg_2721" s="T223">и</ta>
            <ta e="T225" id="Seg_2722" s="T224">PTCL</ta>
            <ta e="T226" id="Seg_2723" s="T225">съесть-MOM-PST.[3SG]</ta>
            <ta e="T227" id="Seg_2724" s="T226">PTCL</ta>
            <ta e="T228" id="Seg_2725" s="T227">тогда</ta>
            <ta e="T230" id="Seg_2726" s="T229">сказать-IPFVZ.[3SG]</ta>
            <ta e="T231" id="Seg_2727" s="T230">пойти-EP-IMP.2SG</ta>
            <ta e="T232" id="Seg_2728" s="T231">я.NOM</ta>
            <ta e="T233" id="Seg_2729" s="T232">девушка.[NOM.SG]</ta>
            <ta e="T234" id="Seg_2730" s="T233">взять-CVB</ta>
            <ta e="T235" id="Seg_2731" s="T234">этот.[NOM.SG]</ta>
            <ta e="T236" id="Seg_2732" s="T235">пойти-PST.[3SG]</ta>
            <ta e="T237" id="Seg_2733" s="T236">мешок.[NOM.SG]</ta>
            <ta e="T238" id="Seg_2734" s="T237">взять-PST.[3SG]</ta>
            <ta e="T239" id="Seg_2735" s="T238">прийти-PST.[3SG]</ta>
            <ta e="T240" id="Seg_2736" s="T239">один.[NOM.SG]</ta>
            <ta e="T241" id="Seg_2737" s="T240">кость.DIM-PL</ta>
            <ta e="T242" id="Seg_2738" s="T241">лежать-DUR-3PL </ta>
            <ta e="T243" id="Seg_2739" s="T242">собирать-PST.[3SG]</ta>
            <ta e="T244" id="Seg_2740" s="T243">и</ta>
            <ta e="T245" id="Seg_2741" s="T244">принести-PST.[3SG]</ta>
            <ta e="T246" id="Seg_2742" s="T245">тогда</ta>
            <ta e="T248" id="Seg_2743" s="T247">собака.[NOM.SG]</ta>
            <ta e="T249" id="Seg_2744" s="T248">опять</ta>
            <ta e="T251" id="Seg_2745" s="T250">кричать-DUR.[3SG]</ta>
            <ta e="T252" id="Seg_2746" s="T251">женщина-GEN</ta>
            <ta e="T253" id="Seg_2747" s="T252">дочь-NOM/GEN.3SG</ta>
            <ta e="T254" id="Seg_2748" s="T253">PTCL</ta>
            <ta e="T255" id="Seg_2749" s="T254">кость.DIM-PL</ta>
            <ta e="T257" id="Seg_2750" s="T256">принести-DUR.[3SG]</ta>
            <ta e="T258" id="Seg_2751" s="T257">мужчина.[NOM.SG]</ta>
            <ta e="T259" id="Seg_2752" s="T258">NEG.AUX-IMP.2SG</ta>
            <ta e="T260" id="Seg_2753" s="T259">лгать-CNG</ta>
            <ta e="T261" id="Seg_2754" s="T260">а.то</ta>
            <ta e="T263" id="Seg_2755" s="T262">ты.DAT</ta>
            <ta e="T264" id="Seg_2756" s="T263">тогда</ta>
            <ta e="T265" id="Seg_2757" s="T264">мужчина.[NOM.SG]</ta>
            <ta e="T267" id="Seg_2758" s="T266">прийти-PST.[3SG]</ta>
            <ta e="T268" id="Seg_2759" s="T267">один.[NOM.SG]</ta>
            <ta e="T269" id="Seg_2760" s="T268">кость.DIM-PL</ta>
            <ta e="T270" id="Seg_2761" s="T269">принести-PST.[3SG]</ta>
            <ta e="T271" id="Seg_2762" s="T270">и</ta>
            <ta e="T273" id="Seg_2763" s="T272">этот.[NOM.SG]</ta>
            <ta e="T274" id="Seg_2764" s="T273">дочь</ta>
            <ta e="T276" id="Seg_2765" s="T275">место-LAT</ta>
            <ta e="T277" id="Seg_2766" s="T276">класть-PST-3PL</ta>
            <ta e="T278" id="Seg_2767" s="T277">и</ta>
            <ta e="T279" id="Seg_2768" s="T278">копать-PST-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_2769" s="T0">num-n:case</ta>
            <ta e="T2" id="Seg_2770" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_2771" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_2772" s="T3">dempro-n:case</ta>
            <ta e="T5" id="Seg_2773" s="T4">n-n:case</ta>
            <ta e="T6" id="Seg_2774" s="T5">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_2775" s="T6">dempro-n:case</ta>
            <ta e="T8" id="Seg_2776" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_2777" s="T8">adj-n:case</ta>
            <ta e="T10" id="Seg_2778" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_2779" s="T10">dempro-n:case</ta>
            <ta e="T12" id="Seg_2780" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_2781" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_2782" s="T13">v-v:tense-v:pn</ta>
            <ta e="T15" id="Seg_2783" s="T14">conj</ta>
            <ta e="T16" id="Seg_2784" s="T15">dempro-n:case</ta>
            <ta e="T17" id="Seg_2785" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_2786" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_2787" s="T18">dempro-n:case</ta>
            <ta e="T20" id="Seg_2788" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_2789" s="T20">n-n:case-poss</ta>
            <ta e="T22" id="Seg_2790" s="T21">quant</ta>
            <ta e="T23" id="Seg_2791" s="T22">que-n:case</ta>
            <ta e="T24" id="Seg_2792" s="T23">v</ta>
            <ta e="T25" id="Seg_2793" s="T24">v-v:tense-v:pn</ta>
            <ta e="T26" id="Seg_2794" s="T25">que-n:case</ta>
            <ta e="T27" id="Seg_2795" s="T26">conj</ta>
            <ta e="T28" id="Seg_2796" s="T27">dempro-n:case</ta>
            <ta e="T29" id="Seg_2797" s="T28">adv</ta>
            <ta e="T30" id="Seg_2798" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_2799" s="T30">adj</ta>
            <ta e="T32" id="Seg_2800" s="T31">v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_2801" s="T32">v-v&gt;v-v:pn</ta>
            <ta e="T34" id="Seg_2802" s="T33">pers</ta>
            <ta e="T35" id="Seg_2803" s="T34">adv</ta>
            <ta e="T36" id="Seg_2804" s="T35">adj-n:case</ta>
            <ta e="T37" id="Seg_2805" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_2806" s="T37">v-v:tense-v:pn</ta>
            <ta e="T39" id="Seg_2807" s="T38">adv</ta>
            <ta e="T40" id="Seg_2808" s="T39">dempro-n:case</ta>
            <ta e="T41" id="Seg_2809" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_2810" s="T41">v-v&gt;v-v:pn</ta>
            <ta e="T43" id="Seg_2811" s="T42">v-v:mood-pn</ta>
            <ta e="T44" id="Seg_2812" s="T43">refl-n:case-poss</ta>
            <ta e="T45" id="Seg_2813" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_2814" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_2815" s="T46">conj</ta>
            <ta e="T48" id="Seg_2816" s="T47">adv</ta>
            <ta e="T50" id="Seg_2817" s="T49">ptcl</ta>
            <ta e="T51" id="Seg_2818" s="T50">adv</ta>
            <ta e="T52" id="Seg_2819" s="T51">v-v:mood-pn</ta>
            <ta e="T53" id="Seg_2820" s="T52">dempro-n:case</ta>
            <ta e="T54" id="Seg_2821" s="T53">v-v:tense-v:pn</ta>
            <ta e="T55" id="Seg_2822" s="T54">adv</ta>
            <ta e="T56" id="Seg_2823" s="T55">v-v:tense-v:pn</ta>
            <ta e="T57" id="Seg_2824" s="T56">dempro</ta>
            <ta e="T58" id="Seg_2825" s="T57">adv</ta>
            <ta e="T59" id="Seg_2826" s="T58">dempro-n:case</ta>
            <ta e="T60" id="Seg_2827" s="T59">n-n:case</ta>
            <ta e="T61" id="Seg_2828" s="T60">adv</ta>
            <ta e="T62" id="Seg_2829" s="T61">v-v:tense-v:pn</ta>
            <ta e="T63" id="Seg_2830" s="T62">v-v:tense-v:pn</ta>
            <ta e="T64" id="Seg_2831" s="T63">n-n:case</ta>
            <ta e="T65" id="Seg_2832" s="T64">n-n:case</ta>
            <ta e="T66" id="Seg_2833" s="T65">v-v:mood-pn</ta>
            <ta e="T67" id="Seg_2834" s="T66">pers</ta>
            <ta e="T68" id="Seg_2835" s="T67">n-n:case</ta>
            <ta e="T69" id="Seg_2836" s="T68">dempro-n:case</ta>
            <ta e="T70" id="Seg_2837" s="T69">dempro-n:case</ta>
            <ta e="T71" id="Seg_2838" s="T70">v-v:tense-v:pn</ta>
            <ta e="T72" id="Seg_2839" s="T71">adv</ta>
            <ta e="T73" id="Seg_2840" s="T72">n-n:case</ta>
            <ta e="T74" id="Seg_2841" s="T73">v-v:tense-v:pn</ta>
            <ta e="T75" id="Seg_2842" s="T74">n</ta>
            <ta e="T76" id="Seg_2843" s="T75">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_2844" s="T77">dempro-n:case</ta>
            <ta e="T79" id="Seg_2845" s="T78">n-n:case</ta>
            <ta e="T80" id="Seg_2846" s="T79">v-v:tense-v:pn</ta>
            <ta e="T81" id="Seg_2847" s="T80">pers</ta>
            <ta e="T82" id="Seg_2848" s="T81">v-v:n.fin</ta>
            <ta e="T83" id="Seg_2849" s="T82">conj</ta>
            <ta e="T84" id="Seg_2850" s="T83">pers</ta>
            <ta e="T85" id="Seg_2851" s="T84">pers</ta>
            <ta e="T87" id="Seg_2852" s="T86">v-v:tense-v:pn</ta>
            <ta e="T88" id="Seg_2853" s="T87">adv</ta>
            <ta e="T89" id="Seg_2854" s="T88">n-n:case</ta>
            <ta e="T90" id="Seg_2855" s="T89">v-v:tense-v:pn</ta>
            <ta e="T91" id="Seg_2856" s="T90">n-n:case</ta>
            <ta e="T92" id="Seg_2857" s="T91">v-v:tense-v:pn</ta>
            <ta e="T93" id="Seg_2858" s="T92">conj</ta>
            <ta e="T94" id="Seg_2859" s="T93">n-n:case</ta>
            <ta e="T95" id="Seg_2860" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_2861" s="T95">v-v:tense-v:pn</ta>
            <ta e="T97" id="Seg_2862" s="T96">dempro-n:case</ta>
            <ta e="T99" id="Seg_2863" s="T98">dempro-n:case</ta>
            <ta e="T100" id="Seg_2864" s="T99">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T101" id="Seg_2865" s="T100">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T102" id="Seg_2866" s="T101">ptcl</ta>
            <ta e="T104" id="Seg_2867" s="T103">v-v:tense-v:pn</ta>
            <ta e="T105" id="Seg_2868" s="T104">v-v:n-fin</ta>
            <ta e="T106" id="Seg_2869" s="T105">n-n:case</ta>
            <ta e="T107" id="Seg_2870" s="T106">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T108" id="Seg_2871" s="T107">n</ta>
            <ta e="T109" id="Seg_2872" s="T108">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T110" id="Seg_2873" s="T109">dempro-n:case</ta>
            <ta e="T111" id="Seg_2874" s="T110">v-v:tense-v:pn</ta>
            <ta e="T112" id="Seg_2875" s="T111">dempro-n:case</ta>
            <ta e="T113" id="Seg_2876" s="T112">v-v:tense-v:pn</ta>
            <ta e="T114" id="Seg_2877" s="T113">n-n:case</ta>
            <ta e="T115" id="Seg_2878" s="T114">n-n:case</ta>
            <ta e="T116" id="Seg_2879" s="T115">quant</ta>
            <ta e="T117" id="Seg_2880" s="T116">n-n:num</ta>
            <ta e="T118" id="Seg_2881" s="T117">v-v:tense-v:pn</ta>
            <ta e="T119" id="Seg_2882" s="T118">n-n:case</ta>
            <ta e="T120" id="Seg_2883" s="T119">quant</ta>
            <ta e="T121" id="Seg_2884" s="T120">adv</ta>
            <ta e="T122" id="Seg_2885" s="T121">dempro-n:case</ta>
            <ta e="T123" id="Seg_2886" s="T122">n-n:case</ta>
            <ta e="T124" id="Seg_2887" s="T123">v-v:ins-v:mood-pn</ta>
            <ta e="T125" id="Seg_2888" s="T124">n-n:case-poss</ta>
            <ta e="T126" id="Seg_2889" s="T125">adv</ta>
            <ta e="T127" id="Seg_2890" s="T126">n-n:num</ta>
            <ta e="T128" id="Seg_2891" s="T127">v-v:mood-pn</ta>
            <ta e="T129" id="Seg_2892" s="T128">v-v:tense-v:pn</ta>
            <ta e="T130" id="Seg_2893" s="T129">conj</ta>
            <ta e="T131" id="Seg_2894" s="T130">v-v:mood-pn</ta>
            <ta e="T132" id="Seg_2895" s="T131">n-n:case</ta>
            <ta e="T133" id="Seg_2896" s="T132">v-v:tense-v:pn</ta>
            <ta e="T134" id="Seg_2897" s="T133">dempro-n:case</ta>
            <ta e="T135" id="Seg_2898" s="T134">v-v:tense-v:pn</ta>
            <ta e="T136" id="Seg_2899" s="T135">v-v:tense-v:pn</ta>
            <ta e="T137" id="Seg_2900" s="T136">n-n:case</ta>
            <ta e="T138" id="Seg_2901" s="T137">ptcl</ta>
            <ta e="T139" id="Seg_2902" s="T138">n-n:case</ta>
            <ta e="T140" id="Seg_2903" s="T139">quant</ta>
            <ta e="T141" id="Seg_2904" s="T140">n-n:case</ta>
            <ta e="T142" id="Seg_2905" s="T141">quant</ta>
            <ta e="T143" id="Seg_2906" s="T142">dempro-n:case</ta>
            <ta e="T144" id="Seg_2907" s="T143">v-v:tense-v:pn</ta>
            <ta e="T145" id="Seg_2908" s="T144">v-v:tense-v:pn</ta>
            <ta e="T146" id="Seg_2909" s="T145">n-n:case</ta>
            <ta e="T147" id="Seg_2910" s="T146">v-v:tense-v:pn</ta>
            <ta e="T148" id="Seg_2911" s="T147">conj</ta>
            <ta e="T149" id="Seg_2912" s="T148">n-n:case</ta>
            <ta e="T150" id="Seg_2913" s="T149">ptcl</ta>
            <ta e="T151" id="Seg_2914" s="T150">v-v&gt;v-v:pn</ta>
            <ta e="T153" id="Seg_2915" s="T152">n-n:case</ta>
            <ta e="T154" id="Seg_2916" s="T153">n-n:case</ta>
            <ta e="T155" id="Seg_2917" s="T154">v-v:tense-v:pn</ta>
            <ta e="T156" id="Seg_2918" s="T155">n-n:case</ta>
            <ta e="T157" id="Seg_2919" s="T156">v</ta>
            <ta e="T158" id="Seg_2920" s="T157">conj</ta>
            <ta e="T159" id="Seg_2921" s="T158">n-n:case</ta>
            <ta e="T160" id="Seg_2922" s="T159">quant</ta>
            <ta e="T161" id="Seg_2923" s="T160">v</ta>
            <ta e="T162" id="Seg_2924" s="T161">conj</ta>
            <ta e="T163" id="Seg_2925" s="T162">dempro-n:case</ta>
            <ta e="T164" id="Seg_2926" s="T163">n-n:case</ta>
            <ta e="T165" id="Seg_2927" s="T164">v-v&gt;v-v:pn</ta>
            <ta e="T166" id="Seg_2928" s="T165">aux-v:mood-pn</ta>
            <ta e="T167" id="Seg_2929" s="T166">v-v:n-fin</ta>
            <ta e="T168" id="Seg_2930" s="T167">adv</ta>
            <ta e="T169" id="Seg_2931" s="T168">n-n:case</ta>
            <ta e="T170" id="Seg_2932" s="T169">v-v:tense-v:pn</ta>
            <ta e="T171" id="Seg_2933" s="T170">quant</ta>
            <ta e="T172" id="Seg_2934" s="T171">v-v:tense-v:pn</ta>
            <ta e="T173" id="Seg_2935" s="T172">conj</ta>
            <ta e="T174" id="Seg_2936" s="T173">n-n:case</ta>
            <ta e="T175" id="Seg_2937" s="T174">conj</ta>
            <ta e="T176" id="Seg_2938" s="T175">n-n:case-poss</ta>
            <ta e="T177" id="Seg_2939" s="T176">v-v:tense-v:pn</ta>
            <ta e="T178" id="Seg_2940" s="T177">adv</ta>
            <ta e="T179" id="Seg_2941" s="T178">v-v&gt;v-v:pn</ta>
            <ta e="T180" id="Seg_2942" s="T179">v-v:mood-pn</ta>
            <ta e="T181" id="Seg_2943" s="T180">pers</ta>
            <ta e="T182" id="Seg_2944" s="T181">n-n:case-poss</ta>
            <ta e="T183" id="Seg_2945" s="T182">adv</ta>
            <ta e="T184" id="Seg_2946" s="T183">v-v:mood-pn</ta>
            <ta e="T185" id="Seg_2947" s="T184">dempro-n:case</ta>
            <ta e="T186" id="Seg_2948" s="T185">v-v:tense-v:pn</ta>
            <ta e="T187" id="Seg_2949" s="T186">dempro-n:case</ta>
            <ta e="T188" id="Seg_2950" s="T187">n-n:case-poss</ta>
            <ta e="T189" id="Seg_2951" s="T188">n-n:case</ta>
            <ta e="T190" id="Seg_2952" s="T189">v-v:tense-v:pn</ta>
            <ta e="T192" id="Seg_2953" s="T191">ptcl</ta>
            <ta e="T193" id="Seg_2954" s="T192">v-v:tense-v:pn</ta>
            <ta e="T194" id="Seg_2955" s="T193">adv</ta>
            <ta e="T195" id="Seg_2956" s="T194">n-n:case</ta>
            <ta e="T196" id="Seg_2957" s="T195">v-v:tense-v:pn</ta>
            <ta e="T197" id="Seg_2958" s="T196">n-n:case</ta>
            <ta e="T198" id="Seg_2959" s="T197">n-n:case</ta>
            <ta e="T200" id="Seg_2960" s="T199">v-v:tense-v:pn</ta>
            <ta e="T201" id="Seg_2961" s="T200">adv</ta>
            <ta e="T202" id="Seg_2962" s="T201">n-n:case</ta>
            <ta e="T203" id="Seg_2963" s="T202">v-v&gt;v-v:pn</ta>
            <ta e="T204" id="Seg_2964" s="T203">adv</ta>
            <ta e="T205" id="Seg_2965" s="T204">n-n:case</ta>
            <ta e="T206" id="Seg_2966" s="T205">n-n:case</ta>
            <ta e="T209" id="Seg_2967" s="T208">pers</ta>
            <ta e="T210" id="Seg_2968" s="T209">pers</ta>
            <ta e="T211" id="Seg_2969" s="T210">v-v:tense-v:pn</ta>
            <ta e="T212" id="Seg_2970" s="T211">conj</ta>
            <ta e="T213" id="Seg_2971" s="T212">n-n:case</ta>
            <ta e="T214" id="Seg_2972" s="T213">ptcl</ta>
            <ta e="T215" id="Seg_2973" s="T214">v-v:tense-v:pn</ta>
            <ta e="T216" id="Seg_2974" s="T215">dempro-n:case</ta>
            <ta e="T217" id="Seg_2975" s="T216">n-n:case</ta>
            <ta e="T218" id="Seg_2976" s="T217">v-v:tense-v:pn</ta>
            <ta e="T219" id="Seg_2977" s="T218">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T220" id="Seg_2978" s="T219">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T221" id="Seg_2979" s="T220">dempro-n:case</ta>
            <ta e="T222" id="Seg_2980" s="T221">dempro-n:case</ta>
            <ta e="T223" id="Seg_2981" s="T222">v-v:tense-v:pn</ta>
            <ta e="T224" id="Seg_2982" s="T223">conj</ta>
            <ta e="T225" id="Seg_2983" s="T224">ptcl</ta>
            <ta e="T226" id="Seg_2984" s="T225">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T227" id="Seg_2985" s="T226">ptcl</ta>
            <ta e="T228" id="Seg_2986" s="T227">adv</ta>
            <ta e="T230" id="Seg_2987" s="T229">v-v&gt;v-v:pn</ta>
            <ta e="T231" id="Seg_2988" s="T230">v-v:ins-v:mood-pn</ta>
            <ta e="T232" id="Seg_2989" s="T231">pers</ta>
            <ta e="T233" id="Seg_2990" s="T232">n-n:case</ta>
            <ta e="T234" id="Seg_2991" s="T233">v-v:n.fin</ta>
            <ta e="T235" id="Seg_2992" s="T234">dempro-n:case</ta>
            <ta e="T236" id="Seg_2993" s="T235">v-v:tense-v:pn</ta>
            <ta e="T237" id="Seg_2994" s="T236">n-n:case</ta>
            <ta e="T238" id="Seg_2995" s="T237">v-v:tense-v:pn</ta>
            <ta e="T239" id="Seg_2996" s="T238">v-v:tense-v:pn</ta>
            <ta e="T240" id="Seg_2997" s="T239">num-n:case</ta>
            <ta e="T241" id="Seg_2998" s="T240">n-n:num</ta>
            <ta e="T242" id="Seg_2999" s="T241">v-v&gt;v-v:pn </ta>
            <ta e="T243" id="Seg_3000" s="T242">v-v:tense-v:pn</ta>
            <ta e="T244" id="Seg_3001" s="T243">conj</ta>
            <ta e="T245" id="Seg_3002" s="T244">v-v:tense-v:pn</ta>
            <ta e="T246" id="Seg_3003" s="T245">adv</ta>
            <ta e="T248" id="Seg_3004" s="T247">n-n:case</ta>
            <ta e="T249" id="Seg_3005" s="T248">adv</ta>
            <ta e="T251" id="Seg_3006" s="T250">v-v&gt;v-v:pn</ta>
            <ta e="T252" id="Seg_3007" s="T251">n-n:case</ta>
            <ta e="T253" id="Seg_3008" s="T252">n-n:case-poss</ta>
            <ta e="T254" id="Seg_3009" s="T253">ptcl</ta>
            <ta e="T255" id="Seg_3010" s="T254">n-n:num</ta>
            <ta e="T257" id="Seg_3011" s="T256">v-v&gt;v-v:pn</ta>
            <ta e="T258" id="Seg_3012" s="T257">n-n:case</ta>
            <ta e="T259" id="Seg_3013" s="T258">aux-v:mood-pn</ta>
            <ta e="T260" id="Seg_3014" s="T259">v-v:n-fin</ta>
            <ta e="T261" id="Seg_3015" s="T260">ptcl</ta>
            <ta e="T263" id="Seg_3016" s="T262">pers</ta>
            <ta e="T264" id="Seg_3017" s="T263">adv</ta>
            <ta e="T265" id="Seg_3018" s="T264">n-n:case</ta>
            <ta e="T267" id="Seg_3019" s="T266">v-v:tense-v:pn</ta>
            <ta e="T268" id="Seg_3020" s="T267">num-n:case</ta>
            <ta e="T269" id="Seg_3021" s="T268">n-n:num</ta>
            <ta e="T270" id="Seg_3022" s="T269">v-v:tense-v:pn</ta>
            <ta e="T271" id="Seg_3023" s="T270">conj</ta>
            <ta e="T273" id="Seg_3024" s="T272">dempro-n:case</ta>
            <ta e="T274" id="Seg_3025" s="T273">n</ta>
            <ta e="T276" id="Seg_3026" s="T275">n-n:case</ta>
            <ta e="T277" id="Seg_3027" s="T276">v-v:tense-v:pn</ta>
            <ta e="T278" id="Seg_3028" s="T277">conj</ta>
            <ta e="T279" id="Seg_3029" s="T278">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_3030" s="T0">num</ta>
            <ta e="T2" id="Seg_3031" s="T1">n</ta>
            <ta e="T3" id="Seg_3032" s="T2">v</ta>
            <ta e="T4" id="Seg_3033" s="T3">dempro</ta>
            <ta e="T5" id="Seg_3034" s="T4">n</ta>
            <ta e="T6" id="Seg_3035" s="T5">v</ta>
            <ta e="T7" id="Seg_3036" s="T6">dempro</ta>
            <ta e="T8" id="Seg_3037" s="T7">v</ta>
            <ta e="T9" id="Seg_3038" s="T8">adj</ta>
            <ta e="T10" id="Seg_3039" s="T9">n</ta>
            <ta e="T11" id="Seg_3040" s="T10">dempro</ta>
            <ta e="T12" id="Seg_3041" s="T11">n</ta>
            <ta e="T13" id="Seg_3042" s="T12">n</ta>
            <ta e="T14" id="Seg_3043" s="T13">v</ta>
            <ta e="T15" id="Seg_3044" s="T14">conj</ta>
            <ta e="T16" id="Seg_3045" s="T15">dempro</ta>
            <ta e="T17" id="Seg_3046" s="T16">n</ta>
            <ta e="T18" id="Seg_3047" s="T17">n</ta>
            <ta e="T19" id="Seg_3048" s="T18">dempro</ta>
            <ta e="T20" id="Seg_3049" s="T19">n</ta>
            <ta e="T21" id="Seg_3050" s="T20">n</ta>
            <ta e="T22" id="Seg_3051" s="T21">quant</ta>
            <ta e="T23" id="Seg_3052" s="T22">que</ta>
            <ta e="T24" id="Seg_3053" s="T23">v</ta>
            <ta e="T25" id="Seg_3054" s="T24">v</ta>
            <ta e="T26" id="Seg_3055" s="T25">que</ta>
            <ta e="T27" id="Seg_3056" s="T26">conj</ta>
            <ta e="T28" id="Seg_3057" s="T27">dempro</ta>
            <ta e="T29" id="Seg_3058" s="T28">adv</ta>
            <ta e="T30" id="Seg_3059" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_3060" s="T30">adj</ta>
            <ta e="T32" id="Seg_3061" s="T31">v</ta>
            <ta e="T33" id="Seg_3062" s="T32">v</ta>
            <ta e="T34" id="Seg_3063" s="T33">pers</ta>
            <ta e="T35" id="Seg_3064" s="T34">adv</ta>
            <ta e="T36" id="Seg_3065" s="T35">adj</ta>
            <ta e="T37" id="Seg_3066" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_3067" s="T37">v</ta>
            <ta e="T39" id="Seg_3068" s="T38">adv</ta>
            <ta e="T40" id="Seg_3069" s="T39">dempro</ta>
            <ta e="T41" id="Seg_3070" s="T40">n</ta>
            <ta e="T42" id="Seg_3071" s="T41">v</ta>
            <ta e="T43" id="Seg_3072" s="T42">v</ta>
            <ta e="T44" id="Seg_3073" s="T43">refl</ta>
            <ta e="T45" id="Seg_3074" s="T44">n</ta>
            <ta e="T46" id="Seg_3075" s="T45">n</ta>
            <ta e="T47" id="Seg_3076" s="T46">conj</ta>
            <ta e="T48" id="Seg_3077" s="T47">adv</ta>
            <ta e="T50" id="Seg_3078" s="T49">ptcl</ta>
            <ta e="T51" id="Seg_3079" s="T50">adv</ta>
            <ta e="T52" id="Seg_3080" s="T51">v</ta>
            <ta e="T53" id="Seg_3081" s="T52">dempro</ta>
            <ta e="T54" id="Seg_3082" s="T53">v</ta>
            <ta e="T55" id="Seg_3083" s="T54">adv</ta>
            <ta e="T56" id="Seg_3084" s="T55">v</ta>
            <ta e="T57" id="Seg_3085" s="T56">dempro</ta>
            <ta e="T58" id="Seg_3086" s="T57">adv</ta>
            <ta e="T59" id="Seg_3087" s="T58">dempro</ta>
            <ta e="T60" id="Seg_3088" s="T59">n</ta>
            <ta e="T61" id="Seg_3089" s="T60">adv</ta>
            <ta e="T62" id="Seg_3090" s="T61">v</ta>
            <ta e="T63" id="Seg_3091" s="T62">v</ta>
            <ta e="T64" id="Seg_3092" s="T63">n</ta>
            <ta e="T65" id="Seg_3093" s="T64">n</ta>
            <ta e="T66" id="Seg_3094" s="T65">v</ta>
            <ta e="T67" id="Seg_3095" s="T66">pers</ta>
            <ta e="T68" id="Seg_3096" s="T67">n</ta>
            <ta e="T69" id="Seg_3097" s="T68">dempro</ta>
            <ta e="T70" id="Seg_3098" s="T69">dempro</ta>
            <ta e="T71" id="Seg_3099" s="T70">v</ta>
            <ta e="T72" id="Seg_3100" s="T71">adv</ta>
            <ta e="T73" id="Seg_3101" s="T72">n</ta>
            <ta e="T74" id="Seg_3102" s="T73">v</ta>
            <ta e="T75" id="Seg_3103" s="T74">n</ta>
            <ta e="T76" id="Seg_3104" s="T75">v</ta>
            <ta e="T78" id="Seg_3105" s="T77">dempro</ta>
            <ta e="T79" id="Seg_3106" s="T78">n</ta>
            <ta e="T80" id="Seg_3107" s="T79">v</ta>
            <ta e="T81" id="Seg_3108" s="T80">pers</ta>
            <ta e="T82" id="Seg_3109" s="T81">v</ta>
            <ta e="T83" id="Seg_3110" s="T82">conj</ta>
            <ta e="T84" id="Seg_3111" s="T83">pers</ta>
            <ta e="T85" id="Seg_3112" s="T84">pers</ta>
            <ta e="T87" id="Seg_3113" s="T86">v</ta>
            <ta e="T88" id="Seg_3114" s="T87">adv</ta>
            <ta e="T89" id="Seg_3115" s="T88">n</ta>
            <ta e="T90" id="Seg_3116" s="T89">v</ta>
            <ta e="T91" id="Seg_3117" s="T90">n</ta>
            <ta e="T92" id="Seg_3118" s="T91">v</ta>
            <ta e="T93" id="Seg_3119" s="T92">conj</ta>
            <ta e="T94" id="Seg_3120" s="T93">n</ta>
            <ta e="T95" id="Seg_3121" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_3122" s="T95">v</ta>
            <ta e="T97" id="Seg_3123" s="T96">dempro</ta>
            <ta e="T99" id="Seg_3124" s="T98">dempro</ta>
            <ta e="T100" id="Seg_3125" s="T99">v</ta>
            <ta e="T101" id="Seg_3126" s="T100">v</ta>
            <ta e="T102" id="Seg_3127" s="T101">ptcl</ta>
            <ta e="T104" id="Seg_3128" s="T103">v</ta>
            <ta e="T105" id="Seg_3129" s="T104">v</ta>
            <ta e="T106" id="Seg_3130" s="T105">n</ta>
            <ta e="T107" id="Seg_3131" s="T106">v</ta>
            <ta e="T108" id="Seg_3132" s="T107">n</ta>
            <ta e="T109" id="Seg_3133" s="T108">v</ta>
            <ta e="T110" id="Seg_3134" s="T109">dempro</ta>
            <ta e="T111" id="Seg_3135" s="T110">v</ta>
            <ta e="T112" id="Seg_3136" s="T111">dempro</ta>
            <ta e="T113" id="Seg_3137" s="T112">v</ta>
            <ta e="T114" id="Seg_3138" s="T113">n</ta>
            <ta e="T115" id="Seg_3139" s="T114">n</ta>
            <ta e="T116" id="Seg_3140" s="T115">quant</ta>
            <ta e="T117" id="Seg_3141" s="T116">n</ta>
            <ta e="T118" id="Seg_3142" s="T117">v</ta>
            <ta e="T119" id="Seg_3143" s="T118">n</ta>
            <ta e="T120" id="Seg_3144" s="T119">quant</ta>
            <ta e="T121" id="Seg_3145" s="T120">adv</ta>
            <ta e="T122" id="Seg_3146" s="T121">dempro</ta>
            <ta e="T123" id="Seg_3147" s="T122">n</ta>
            <ta e="T124" id="Seg_3148" s="T123">v</ta>
            <ta e="T125" id="Seg_3149" s="T124">n</ta>
            <ta e="T126" id="Seg_3150" s="T125">adv</ta>
            <ta e="T127" id="Seg_3151" s="T126">n</ta>
            <ta e="T128" id="Seg_3152" s="T127">v</ta>
            <ta e="T129" id="Seg_3153" s="T128">v</ta>
            <ta e="T130" id="Seg_3154" s="T129">conj</ta>
            <ta e="T131" id="Seg_3155" s="T130">v</ta>
            <ta e="T132" id="Seg_3156" s="T131">n</ta>
            <ta e="T133" id="Seg_3157" s="T132">v</ta>
            <ta e="T134" id="Seg_3158" s="T133">dempro</ta>
            <ta e="T135" id="Seg_3159" s="T134">v</ta>
            <ta e="T136" id="Seg_3160" s="T135">v</ta>
            <ta e="T137" id="Seg_3161" s="T136">n</ta>
            <ta e="T138" id="Seg_3162" s="T137">ptcl</ta>
            <ta e="T139" id="Seg_3163" s="T138">n</ta>
            <ta e="T140" id="Seg_3164" s="T139">quant</ta>
            <ta e="T141" id="Seg_3165" s="T140">n</ta>
            <ta e="T142" id="Seg_3166" s="T141">quant</ta>
            <ta e="T143" id="Seg_3167" s="T142">dempro</ta>
            <ta e="T144" id="Seg_3168" s="T143">v</ta>
            <ta e="T145" id="Seg_3169" s="T144">v</ta>
            <ta e="T146" id="Seg_3170" s="T145">n</ta>
            <ta e="T147" id="Seg_3171" s="T146">v</ta>
            <ta e="T148" id="Seg_3172" s="T147">conj</ta>
            <ta e="T149" id="Seg_3173" s="T148">n</ta>
            <ta e="T150" id="Seg_3174" s="T149">ptcl</ta>
            <ta e="T151" id="Seg_3175" s="T150">v</ta>
            <ta e="T153" id="Seg_3176" s="T152">n</ta>
            <ta e="T154" id="Seg_3177" s="T153">n</ta>
            <ta e="T155" id="Seg_3178" s="T154">v</ta>
            <ta e="T156" id="Seg_3179" s="T155">n</ta>
            <ta e="T158" id="Seg_3180" s="T157">conj</ta>
            <ta e="T159" id="Seg_3181" s="T158">n</ta>
            <ta e="T160" id="Seg_3182" s="T159">quant</ta>
            <ta e="T162" id="Seg_3183" s="T161">conj</ta>
            <ta e="T163" id="Seg_3184" s="T162">dempro</ta>
            <ta e="T164" id="Seg_3185" s="T163">n</ta>
            <ta e="T165" id="Seg_3186" s="T164">v</ta>
            <ta e="T166" id="Seg_3187" s="T165">aux</ta>
            <ta e="T167" id="Seg_3188" s="T166">v</ta>
            <ta e="T168" id="Seg_3189" s="T167">adv</ta>
            <ta e="T169" id="Seg_3190" s="T168">n</ta>
            <ta e="T170" id="Seg_3191" s="T169">v</ta>
            <ta e="T171" id="Seg_3192" s="T170">quant</ta>
            <ta e="T172" id="Seg_3193" s="T171">v</ta>
            <ta e="T173" id="Seg_3194" s="T172">conj</ta>
            <ta e="T174" id="Seg_3195" s="T173">n</ta>
            <ta e="T175" id="Seg_3196" s="T174">conj</ta>
            <ta e="T176" id="Seg_3197" s="T175">n</ta>
            <ta e="T177" id="Seg_3198" s="T176">v</ta>
            <ta e="T178" id="Seg_3199" s="T177">adv</ta>
            <ta e="T179" id="Seg_3200" s="T178">v</ta>
            <ta e="T180" id="Seg_3201" s="T179">v</ta>
            <ta e="T181" id="Seg_3202" s="T180">pers</ta>
            <ta e="T182" id="Seg_3203" s="T181">n</ta>
            <ta e="T183" id="Seg_3204" s="T182">adv</ta>
            <ta e="T184" id="Seg_3205" s="T183">v</ta>
            <ta e="T185" id="Seg_3206" s="T184">dempro</ta>
            <ta e="T186" id="Seg_3207" s="T185">v</ta>
            <ta e="T187" id="Seg_3208" s="T186">dempro</ta>
            <ta e="T188" id="Seg_3209" s="T187">n</ta>
            <ta e="T189" id="Seg_3210" s="T188">n</ta>
            <ta e="T192" id="Seg_3211" s="T191">ptcl</ta>
            <ta e="T193" id="Seg_3212" s="T192">v</ta>
            <ta e="T194" id="Seg_3213" s="T193">adv</ta>
            <ta e="T195" id="Seg_3214" s="T194">n</ta>
            <ta e="T196" id="Seg_3215" s="T195">v</ta>
            <ta e="T197" id="Seg_3216" s="T196">n</ta>
            <ta e="T198" id="Seg_3217" s="T197">n</ta>
            <ta e="T200" id="Seg_3218" s="T199">v</ta>
            <ta e="T201" id="Seg_3219" s="T200">adv</ta>
            <ta e="T202" id="Seg_3220" s="T201">n</ta>
            <ta e="T203" id="Seg_3221" s="T202">v</ta>
            <ta e="T204" id="Seg_3222" s="T203">adv</ta>
            <ta e="T205" id="Seg_3223" s="T204">n</ta>
            <ta e="T206" id="Seg_3224" s="T205">n</ta>
            <ta e="T209" id="Seg_3225" s="T208">pers</ta>
            <ta e="T210" id="Seg_3226" s="T209">pers</ta>
            <ta e="T211" id="Seg_3227" s="T210">v</ta>
            <ta e="T212" id="Seg_3228" s="T211">conj</ta>
            <ta e="T213" id="Seg_3229" s="T212">n</ta>
            <ta e="T214" id="Seg_3230" s="T213">ptcl</ta>
            <ta e="T215" id="Seg_3231" s="T214">v</ta>
            <ta e="T216" id="Seg_3232" s="T215">dempro</ta>
            <ta e="T217" id="Seg_3233" s="T216">n</ta>
            <ta e="T218" id="Seg_3234" s="T217">v</ta>
            <ta e="T219" id="Seg_3235" s="T218">v</ta>
            <ta e="T220" id="Seg_3236" s="T219">v</ta>
            <ta e="T221" id="Seg_3237" s="T220">dempro</ta>
            <ta e="T222" id="Seg_3238" s="T221">dempro</ta>
            <ta e="T223" id="Seg_3239" s="T222">v</ta>
            <ta e="T224" id="Seg_3240" s="T223">conj</ta>
            <ta e="T225" id="Seg_3241" s="T224">ptcl</ta>
            <ta e="T226" id="Seg_3242" s="T225">v</ta>
            <ta e="T227" id="Seg_3243" s="T226">ptcl</ta>
            <ta e="T228" id="Seg_3244" s="T227">adv</ta>
            <ta e="T230" id="Seg_3245" s="T229">v</ta>
            <ta e="T231" id="Seg_3246" s="T230">v</ta>
            <ta e="T232" id="Seg_3247" s="T231">pers</ta>
            <ta e="T233" id="Seg_3248" s="T232">n</ta>
            <ta e="T234" id="Seg_3249" s="T233">v</ta>
            <ta e="T235" id="Seg_3250" s="T234">dempro</ta>
            <ta e="T236" id="Seg_3251" s="T235">v</ta>
            <ta e="T237" id="Seg_3252" s="T236">n</ta>
            <ta e="T238" id="Seg_3253" s="T237">v</ta>
            <ta e="T239" id="Seg_3254" s="T238">v</ta>
            <ta e="T240" id="Seg_3255" s="T239">num</ta>
            <ta e="T241" id="Seg_3256" s="T240">n</ta>
            <ta e="T242" id="Seg_3257" s="T241">v</ta>
            <ta e="T243" id="Seg_3258" s="T242">v</ta>
            <ta e="T244" id="Seg_3259" s="T243">conj</ta>
            <ta e="T245" id="Seg_3260" s="T244">v</ta>
            <ta e="T246" id="Seg_3261" s="T245">adv</ta>
            <ta e="T248" id="Seg_3262" s="T247">n</ta>
            <ta e="T249" id="Seg_3263" s="T248">adv</ta>
            <ta e="T251" id="Seg_3264" s="T250">v</ta>
            <ta e="T252" id="Seg_3265" s="T251">n</ta>
            <ta e="T253" id="Seg_3266" s="T252">n</ta>
            <ta e="T254" id="Seg_3267" s="T253">ptcl</ta>
            <ta e="T255" id="Seg_3268" s="T254">n</ta>
            <ta e="T257" id="Seg_3269" s="T256">v</ta>
            <ta e="T258" id="Seg_3270" s="T257">n</ta>
            <ta e="T259" id="Seg_3271" s="T258">aux</ta>
            <ta e="T260" id="Seg_3272" s="T259">v</ta>
            <ta e="T261" id="Seg_3273" s="T260">ptcl</ta>
            <ta e="T263" id="Seg_3274" s="T262">pers</ta>
            <ta e="T264" id="Seg_3275" s="T263">adv</ta>
            <ta e="T265" id="Seg_3276" s="T264">n</ta>
            <ta e="T267" id="Seg_3277" s="T266">v</ta>
            <ta e="T268" id="Seg_3278" s="T267">num</ta>
            <ta e="T269" id="Seg_3279" s="T268">n</ta>
            <ta e="T270" id="Seg_3280" s="T269">v</ta>
            <ta e="T271" id="Seg_3281" s="T270">conj</ta>
            <ta e="T273" id="Seg_3282" s="T272">dempro</ta>
            <ta e="T274" id="Seg_3283" s="T273">n</ta>
            <ta e="T276" id="Seg_3284" s="T275">n</ta>
            <ta e="T277" id="Seg_3285" s="T276">v</ta>
            <ta e="T278" id="Seg_3286" s="T277">conj</ta>
            <ta e="T279" id="Seg_3287" s="T278">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_3288" s="T1">np.h:E</ta>
            <ta e="T4" id="Seg_3289" s="T3">pro.h:Poss</ta>
            <ta e="T5" id="Seg_3290" s="T4">np.h:P</ta>
            <ta e="T7" id="Seg_3291" s="T6">pro.h:A</ta>
            <ta e="T10" id="Seg_3292" s="T9">np.h:Th</ta>
            <ta e="T11" id="Seg_3293" s="T10">pro.h:Poss</ta>
            <ta e="T12" id="Seg_3294" s="T11">np.h:Poss</ta>
            <ta e="T13" id="Seg_3295" s="T12">np.h:Th</ta>
            <ta e="T17" id="Seg_3296" s="T16">np.h:Poss</ta>
            <ta e="T18" id="Seg_3297" s="T17">np.h:Th</ta>
            <ta e="T20" id="Seg_3298" s="T19">np.h:Poss</ta>
            <ta e="T21" id="Seg_3299" s="T20">np.h:A</ta>
            <ta e="T22" id="Seg_3300" s="T21">np:Th</ta>
            <ta e="T28" id="Seg_3301" s="T27">pro.h:R</ta>
            <ta e="T32" id="Seg_3302" s="T31">0.3.h:Th</ta>
            <ta e="T33" id="Seg_3303" s="T32">0.3.h:A</ta>
            <ta e="T34" id="Seg_3304" s="T33">pro.h:Th</ta>
            <ta e="T38" id="Seg_3305" s="T37">0.2.h:A</ta>
            <ta e="T39" id="Seg_3306" s="T38">adv:Time</ta>
            <ta e="T40" id="Seg_3307" s="T39">pro.h:A</ta>
            <ta e="T41" id="Seg_3308" s="T40">np.h:R</ta>
            <ta e="T43" id="Seg_3309" s="T42">0.2.h:A</ta>
            <ta e="T44" id="Seg_3310" s="T43">pro.h:Poss</ta>
            <ta e="T45" id="Seg_3311" s="T44">np.h:Th</ta>
            <ta e="T46" id="Seg_3312" s="T45">np:G</ta>
            <ta e="T48" id="Seg_3313" s="T47">adv:L</ta>
            <ta e="T51" id="Seg_3314" s="T50">adv:L</ta>
            <ta e="T52" id="Seg_3315" s="T51">0.2.h:A</ta>
            <ta e="T53" id="Seg_3316" s="T52">pro.h:A</ta>
            <ta e="T55" id="Seg_3317" s="T54">adv:L</ta>
            <ta e="T56" id="Seg_3318" s="T55">0.3.h:A</ta>
            <ta e="T58" id="Seg_3319" s="T57">adv:Time</ta>
            <ta e="T59" id="Seg_3320" s="T58">pro.h:A</ta>
            <ta e="T60" id="Seg_3321" s="T59">np:Th</ta>
            <ta e="T61" id="Seg_3322" s="T60">adv:L</ta>
            <ta e="T63" id="Seg_3323" s="T62">0.3.h:A</ta>
            <ta e="T64" id="Seg_3324" s="T63">np:G</ta>
            <ta e="T65" id="Seg_3325" s="T64">np.h:A</ta>
            <ta e="T66" id="Seg_3326" s="T65">0.2.h:A</ta>
            <ta e="T67" id="Seg_3327" s="T66">pro.h:R</ta>
            <ta e="T68" id="Seg_3328" s="T67">np:Th</ta>
            <ta e="T69" id="Seg_3329" s="T68">pro:Th</ta>
            <ta e="T70" id="Seg_3330" s="T69">pro.h:R</ta>
            <ta e="T71" id="Seg_3331" s="T70">0.3.h:A</ta>
            <ta e="T72" id="Seg_3332" s="T71">adv:Time</ta>
            <ta e="T73" id="Seg_3333" s="T72">np.h:A</ta>
            <ta e="T75" id="Seg_3334" s="T74">np:Th</ta>
            <ta e="T78" id="Seg_3335" s="T77">pro.h:R</ta>
            <ta e="T79" id="Seg_3336" s="T78">np:Th</ta>
            <ta e="T80" id="Seg_3337" s="T79">0.3.h:A</ta>
            <ta e="T81" id="Seg_3338" s="T80">pro.h:A</ta>
            <ta e="T84" id="Seg_3339" s="T83">pro.h:A</ta>
            <ta e="T85" id="Seg_3340" s="T84">pro.h:Th</ta>
            <ta e="T88" id="Seg_3341" s="T87">adv:Time</ta>
            <ta e="T89" id="Seg_3342" s="T88">np.h:A</ta>
            <ta e="T91" id="Seg_3343" s="T90">np:Th</ta>
            <ta e="T92" id="Seg_3344" s="T91">0.3.h:A</ta>
            <ta e="T94" id="Seg_3345" s="T93">np:L</ta>
            <ta e="T96" id="Seg_3346" s="T95">0.3.h:A</ta>
            <ta e="T97" id="Seg_3347" s="T96">pro.h:A</ta>
            <ta e="T99" id="Seg_3348" s="T98">pro:Th</ta>
            <ta e="T101" id="Seg_3349" s="T100">0.3.h:A</ta>
            <ta e="T104" id="Seg_3350" s="T103">0.3.h:A</ta>
            <ta e="T106" id="Seg_3351" s="T105">np.h:A</ta>
            <ta e="T108" id="Seg_3352" s="T107">np:Th</ta>
            <ta e="T110" id="Seg_3353" s="T109">pro.h:A</ta>
            <ta e="T112" id="Seg_3354" s="T111">pro.h:R</ta>
            <ta e="T113" id="Seg_3355" s="T112">0.3.h:A</ta>
            <ta e="T114" id="Seg_3356" s="T113">np:Th</ta>
            <ta e="T115" id="Seg_3357" s="T114">np:Th</ta>
            <ta e="T117" id="Seg_3358" s="T116">np:Th</ta>
            <ta e="T118" id="Seg_3359" s="T117">0.3.h:A</ta>
            <ta e="T119" id="Seg_3360" s="T118">np:Th</ta>
            <ta e="T121" id="Seg_3361" s="T120">adv:Time</ta>
            <ta e="T123" id="Seg_3362" s="T122">np.h:A</ta>
            <ta e="T124" id="Seg_3363" s="T123">0.2.h:A</ta>
            <ta e="T125" id="Seg_3364" s="T124">np.h:Poss</ta>
            <ta e="T126" id="Seg_3365" s="T125">adv:L</ta>
            <ta e="T127" id="Seg_3366" s="T126">np:Th</ta>
            <ta e="T128" id="Seg_3367" s="T127">0.2.h:A</ta>
            <ta e="T129" id="Seg_3368" s="T128">0.2.h:A</ta>
            <ta e="T131" id="Seg_3369" s="T130">0.2.h:A</ta>
            <ta e="T132" id="Seg_3370" s="T131">np:L</ta>
            <ta e="T133" id="Seg_3371" s="T132">0.1.h:A</ta>
            <ta e="T134" id="Seg_3372" s="T133">pro.h:A</ta>
            <ta e="T136" id="Seg_3373" s="T135">0.3.h:A</ta>
            <ta e="T137" id="Seg_3374" s="T136">np.h:Poss</ta>
            <ta e="T139" id="Seg_3375" s="T138">np:Th</ta>
            <ta e="T141" id="Seg_3376" s="T140">np:Th</ta>
            <ta e="T143" id="Seg_3377" s="T142">pro.h:Th</ta>
            <ta e="T144" id="Seg_3378" s="T143">0.3.h:A</ta>
            <ta e="T145" id="Seg_3379" s="T144">0.3.h:A</ta>
            <ta e="T146" id="Seg_3380" s="T145">np:G</ta>
            <ta e="T147" id="Seg_3381" s="T146">0.3.h:A</ta>
            <ta e="T149" id="Seg_3382" s="T148">np.h:A</ta>
            <ta e="T153" id="Seg_3383" s="T152">np.h:Poss</ta>
            <ta e="T154" id="Seg_3384" s="T153">np.h:A</ta>
            <ta e="T156" id="Seg_3385" s="T155">np:Th</ta>
            <ta e="T157" id="Seg_3386" s="T156">0.3.h:A</ta>
            <ta e="T159" id="Seg_3387" s="T158">np:Th</ta>
            <ta e="T161" id="Seg_3388" s="T160">0.3.h:A</ta>
            <ta e="T164" id="Seg_3389" s="T163">np.h:A</ta>
            <ta e="T166" id="Seg_3390" s="T165">0.2.h:A</ta>
            <ta e="T168" id="Seg_3391" s="T167">adv:Time</ta>
            <ta e="T169" id="Seg_3392" s="T168">np.h:A</ta>
            <ta e="T172" id="Seg_3393" s="T171">0.3.h:A</ta>
            <ta e="T174" id="Seg_3394" s="T173">np:Th</ta>
            <ta e="T176" id="Seg_3395" s="T175">np:Th</ta>
            <ta e="T177" id="Seg_3396" s="T176">0.3.h:A</ta>
            <ta e="T178" id="Seg_3397" s="T177">adv:Time</ta>
            <ta e="T179" id="Seg_3398" s="T178">0.3.h:A</ta>
            <ta e="T180" id="Seg_3399" s="T179">0.3.h:A</ta>
            <ta e="T181" id="Seg_3400" s="T180">pro.h:Poss</ta>
            <ta e="T182" id="Seg_3401" s="T181">np:Th</ta>
            <ta e="T183" id="Seg_3402" s="T182">adv:L</ta>
            <ta e="T184" id="Seg_3403" s="T183">0.2.h:A</ta>
            <ta e="T185" id="Seg_3404" s="T184">pro.h:A</ta>
            <ta e="T188" id="Seg_3405" s="T187">np.h:Th</ta>
            <ta e="T189" id="Seg_3406" s="T188">np.h:A</ta>
            <ta e="T194" id="Seg_3407" s="T193">adv:Time</ta>
            <ta e="T195" id="Seg_3408" s="T194">n:Time</ta>
            <ta e="T196" id="Seg_3409" s="T195">0.3.h:A</ta>
            <ta e="T197" id="Seg_3410" s="T196">np:Th</ta>
            <ta e="T198" id="Seg_3411" s="T197">np:G</ta>
            <ta e="T200" id="Seg_3412" s="T199">0.3.h:A</ta>
            <ta e="T201" id="Seg_3413" s="T200">adv:L</ta>
            <ta e="T202" id="Seg_3414" s="T201">np.h:A</ta>
            <ta e="T204" id="Seg_3415" s="T203">adv:Time</ta>
            <ta e="T205" id="Seg_3416" s="T204">n:Time</ta>
            <ta e="T209" id="Seg_3417" s="T208">pro.h:A</ta>
            <ta e="T210" id="Seg_3418" s="T209">pro.h:Th</ta>
            <ta e="T213" id="Seg_3419" s="T212">np.h:A</ta>
            <ta e="T216" id="Seg_3420" s="T215">pro.h:A</ta>
            <ta e="T217" id="Seg_3421" s="T216">np:Th</ta>
            <ta e="T219" id="Seg_3422" s="T218">0.3.h:A</ta>
            <ta e="T220" id="Seg_3423" s="T219">0.3.h:A</ta>
            <ta e="T221" id="Seg_3424" s="T220">pro.h:A</ta>
            <ta e="T222" id="Seg_3425" s="T221">pro.h:Th</ta>
            <ta e="T226" id="Seg_3426" s="T225">0.3.h:A</ta>
            <ta e="T228" id="Seg_3427" s="T227">adv:Time</ta>
            <ta e="T230" id="Seg_3428" s="T229">0.3.h:A</ta>
            <ta e="T231" id="Seg_3429" s="T230">0.2.h:A</ta>
            <ta e="T232" id="Seg_3430" s="T231">pro.h:Poss</ta>
            <ta e="T233" id="Seg_3431" s="T232">np.h:Th</ta>
            <ta e="T235" id="Seg_3432" s="T234">pro.h:A</ta>
            <ta e="T237" id="Seg_3433" s="T236">np:Th</ta>
            <ta e="T238" id="Seg_3434" s="T237">0.3.h:A</ta>
            <ta e="T239" id="Seg_3435" s="T238">0.3.h:A</ta>
            <ta e="T241" id="Seg_3436" s="T240">np:Th</ta>
            <ta e="T243" id="Seg_3437" s="T242">0.3.h:A</ta>
            <ta e="T245" id="Seg_3438" s="T244">0.3.h:A</ta>
            <ta e="T246" id="Seg_3439" s="T245">adv:Time</ta>
            <ta e="T248" id="Seg_3440" s="T247">np.h:A</ta>
            <ta e="T252" id="Seg_3441" s="T251">np.h:Poss</ta>
            <ta e="T253" id="Seg_3442" s="T252">np.h:Poss</ta>
            <ta e="T255" id="Seg_3443" s="T254">np:Th</ta>
            <ta e="T258" id="Seg_3444" s="T257">np.h:A</ta>
            <ta e="T259" id="Seg_3445" s="T258">0.2.h:A</ta>
            <ta e="T263" id="Seg_3446" s="T262">pro.h:E</ta>
            <ta e="T264" id="Seg_3447" s="T263">adv:Time</ta>
            <ta e="T265" id="Seg_3448" s="T264">np.h:A</ta>
            <ta e="T269" id="Seg_3449" s="T268">np:Th</ta>
            <ta e="T270" id="Seg_3450" s="T269">0.3.h:A</ta>
            <ta e="T274" id="Seg_3451" s="T273">np.h:P</ta>
            <ta e="T276" id="Seg_3452" s="T275">np:L</ta>
            <ta e="T277" id="Seg_3453" s="T276">0.3.h:A</ta>
            <ta e="T279" id="Seg_3454" s="T278">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_3455" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_3456" s="T2">v:pred</ta>
            <ta e="T5" id="Seg_3457" s="T4">np.h:S</ta>
            <ta e="T6" id="Seg_3458" s="T5">v:pred</ta>
            <ta e="T7" id="Seg_3459" s="T6">pro.h:S</ta>
            <ta e="T8" id="Seg_3460" s="T7">v:pred</ta>
            <ta e="T10" id="Seg_3461" s="T9">np.h:O</ta>
            <ta e="T13" id="Seg_3462" s="T12">np.h:S</ta>
            <ta e="T14" id="Seg_3463" s="T13">v:pred</ta>
            <ta e="T18" id="Seg_3464" s="T17">np.h:S</ta>
            <ta e="T21" id="Seg_3465" s="T20">np.h:S</ta>
            <ta e="T22" id="Seg_3466" s="T21">np:O</ta>
            <ta e="T25" id="Seg_3467" s="T24">v:pred</ta>
            <ta e="T30" id="Seg_3468" s="T29">ptcl.neg</ta>
            <ta e="T31" id="Seg_3469" s="T30">adj:pred</ta>
            <ta e="T32" id="Seg_3470" s="T31">cop 0.3.h:S</ta>
            <ta e="T33" id="Seg_3471" s="T32">v:pred 0.3.h:S</ta>
            <ta e="T34" id="Seg_3472" s="T33">pro.h:S</ta>
            <ta e="T36" id="Seg_3473" s="T35">adj:pred</ta>
            <ta e="T37" id="Seg_3474" s="T36">ptcl.neg</ta>
            <ta e="T38" id="Seg_3475" s="T37">v:pred 0.2.h:S</ta>
            <ta e="T40" id="Seg_3476" s="T39">pro.h:S</ta>
            <ta e="T42" id="Seg_3477" s="T41">v:pred</ta>
            <ta e="T43" id="Seg_3478" s="T42">v:pred 0.2.h:S</ta>
            <ta e="T45" id="Seg_3479" s="T44">np.h:O</ta>
            <ta e="T52" id="Seg_3480" s="T51">v:pred 0.2.h:S</ta>
            <ta e="T53" id="Seg_3481" s="T52">pro.h:S</ta>
            <ta e="T54" id="Seg_3482" s="T53">v:pred</ta>
            <ta e="T56" id="Seg_3483" s="T55">v:pred 0.3.h:S</ta>
            <ta e="T59" id="Seg_3484" s="T58">pro.h:S</ta>
            <ta e="T60" id="Seg_3485" s="T59">np:O</ta>
            <ta e="T62" id="Seg_3486" s="T61">v:pred</ta>
            <ta e="T63" id="Seg_3487" s="T62">v:pred 0.3.h:S</ta>
            <ta e="T65" id="Seg_3488" s="T64">np.h:S</ta>
            <ta e="T66" id="Seg_3489" s="T65">v:pred 0.2.h:S</ta>
            <ta e="T68" id="Seg_3490" s="T67">np:O</ta>
            <ta e="T69" id="Seg_3491" s="T68">pro:O</ta>
            <ta e="T71" id="Seg_3492" s="T70">v:pred 0.3.h:S</ta>
            <ta e="T73" id="Seg_3493" s="T72">np.h:S</ta>
            <ta e="T74" id="Seg_3494" s="T73">v:pred</ta>
            <ta e="T75" id="Seg_3495" s="T74">np:S</ta>
            <ta e="T76" id="Seg_3496" s="T75">v:pred</ta>
            <ta e="T79" id="Seg_3497" s="T78">np:O</ta>
            <ta e="T80" id="Seg_3498" s="T79">v:pred 0.3.h:S</ta>
            <ta e="T81" id="Seg_3499" s="T80">pro.h:S</ta>
            <ta e="T82" id="Seg_3500" s="T81">v:pred</ta>
            <ta e="T84" id="Seg_3501" s="T83">pro.h:S</ta>
            <ta e="T85" id="Seg_3502" s="T84">pro.h:O</ta>
            <ta e="T87" id="Seg_3503" s="T86">v:pred</ta>
            <ta e="T89" id="Seg_3504" s="T88">np.h:S</ta>
            <ta e="T90" id="Seg_3505" s="T89">v:pred</ta>
            <ta e="T91" id="Seg_3506" s="T90">np:O</ta>
            <ta e="T92" id="Seg_3507" s="T91">v:pred 0.3.h:S</ta>
            <ta e="T96" id="Seg_3508" s="T95">v:pred 0.3.h:S</ta>
            <ta e="T97" id="Seg_3509" s="T96">pro.h:S</ta>
            <ta e="T99" id="Seg_3510" s="T98">pro:O</ta>
            <ta e="T100" id="Seg_3511" s="T99">v:pred</ta>
            <ta e="T101" id="Seg_3512" s="T100">v:pred 0.3.h:S</ta>
            <ta e="T102" id="Seg_3513" s="T101">ptcl.neg</ta>
            <ta e="T104" id="Seg_3514" s="T103">v:pred 0.3.h:S</ta>
            <ta e="T106" id="Seg_3515" s="T105">np.h:S</ta>
            <ta e="T107" id="Seg_3516" s="T106">v:pred</ta>
            <ta e="T108" id="Seg_3517" s="T107">np:S</ta>
            <ta e="T109" id="Seg_3518" s="T108">v:pred</ta>
            <ta e="T110" id="Seg_3519" s="T109">pro.h:S</ta>
            <ta e="T111" id="Seg_3520" s="T110">v:pred</ta>
            <ta e="T113" id="Seg_3521" s="T112">v:pred 0.3.h:S</ta>
            <ta e="T114" id="Seg_3522" s="T113">np:O</ta>
            <ta e="T115" id="Seg_3523" s="T114">np:O</ta>
            <ta e="T117" id="Seg_3524" s="T116">np:O</ta>
            <ta e="T118" id="Seg_3525" s="T117">v:pred 0.3.h:S</ta>
            <ta e="T119" id="Seg_3526" s="T118">np:O</ta>
            <ta e="T123" id="Seg_3527" s="T122">np.h:S</ta>
            <ta e="T124" id="Seg_3528" s="T123">v:pred 0.2.h:S</ta>
            <ta e="T127" id="Seg_3529" s="T126">np:O</ta>
            <ta e="T128" id="Seg_3530" s="T127">v:pred 0.2.h:S</ta>
            <ta e="T129" id="Seg_3531" s="T128">v:pred 0.2.h:S</ta>
            <ta e="T131" id="Seg_3532" s="T130">v:pred 0.2.h:S</ta>
            <ta e="T133" id="Seg_3533" s="T132">v:pred 0.1.h:S</ta>
            <ta e="T134" id="Seg_3534" s="T133">pro.h:S</ta>
            <ta e="T135" id="Seg_3535" s="T134">v:pred</ta>
            <ta e="T136" id="Seg_3536" s="T135">v:pred 0.3.h:S</ta>
            <ta e="T139" id="Seg_3537" s="T138">np:S</ta>
            <ta e="T141" id="Seg_3538" s="T140">np:S</ta>
            <ta e="T143" id="Seg_3539" s="T142">pro.h:O</ta>
            <ta e="T144" id="Seg_3540" s="T143">v:pred 0.3.h:S</ta>
            <ta e="T145" id="Seg_3541" s="T144">v:pred 0.3.h:S</ta>
            <ta e="T147" id="Seg_3542" s="T146">v:pred 0.3.h:S</ta>
            <ta e="T149" id="Seg_3543" s="T148">np.h:S</ta>
            <ta e="T151" id="Seg_3544" s="T150">v:pred</ta>
            <ta e="T154" id="Seg_3545" s="T153">np.h:S</ta>
            <ta e="T155" id="Seg_3546" s="T154">v:pred</ta>
            <ta e="T156" id="Seg_3547" s="T155">np:O</ta>
            <ta e="T157" id="Seg_3548" s="T156">v:pred 0.3.h:S</ta>
            <ta e="T159" id="Seg_3549" s="T158">np:O</ta>
            <ta e="T161" id="Seg_3550" s="T160">v:pred 0.3.h:S</ta>
            <ta e="T164" id="Seg_3551" s="T163">np.h:S</ta>
            <ta e="T165" id="Seg_3552" s="T164">v:pred</ta>
            <ta e="T166" id="Seg_3553" s="T165">v:pred 0.2.h:S</ta>
            <ta e="T169" id="Seg_3554" s="T168">np.h:S</ta>
            <ta e="T170" id="Seg_3555" s="T169">v:pred</ta>
            <ta e="T172" id="Seg_3556" s="T171">v:pred 0.3.h:S</ta>
            <ta e="T174" id="Seg_3557" s="T173">np:O</ta>
            <ta e="T176" id="Seg_3558" s="T175">np:O</ta>
            <ta e="T177" id="Seg_3559" s="T176">v:pred 0.3.h:S</ta>
            <ta e="T179" id="Seg_3560" s="T178">v:pred 0.3.h:S</ta>
            <ta e="T180" id="Seg_3561" s="T179">v:pred 0.2.h:S</ta>
            <ta e="T182" id="Seg_3562" s="T181">np:O</ta>
            <ta e="T184" id="Seg_3563" s="T183">v:pred 0.2.h:S</ta>
            <ta e="T185" id="Seg_3564" s="T184">pro.h:S</ta>
            <ta e="T186" id="Seg_3565" s="T185">v:pred</ta>
            <ta e="T188" id="Seg_3566" s="T187">np.h:O</ta>
            <ta e="T189" id="Seg_3567" s="T188">np.h:S</ta>
            <ta e="T192" id="Seg_3568" s="T191">ptcl.neg</ta>
            <ta e="T193" id="Seg_3569" s="T192">v:pred</ta>
            <ta e="T196" id="Seg_3570" s="T195">v:pred 0.3.h:S</ta>
            <ta e="T197" id="Seg_3571" s="T196">np:O</ta>
            <ta e="T200" id="Seg_3572" s="T199">v:pred 0.3.h:S</ta>
            <ta e="T202" id="Seg_3573" s="T201">np.h:S</ta>
            <ta e="T203" id="Seg_3574" s="T202">v:pred</ta>
            <ta e="T209" id="Seg_3575" s="T208">pro.h:S</ta>
            <ta e="T210" id="Seg_3576" s="T209">pro.h:O</ta>
            <ta e="T211" id="Seg_3577" s="T210">v:pred</ta>
            <ta e="T213" id="Seg_3578" s="T212">np.h:S</ta>
            <ta e="T214" id="Seg_3579" s="T213">ptcl.neg</ta>
            <ta e="T215" id="Seg_3580" s="T214">v:pred</ta>
            <ta e="T216" id="Seg_3581" s="T215">pro.h:S</ta>
            <ta e="T217" id="Seg_3582" s="T216">np:O</ta>
            <ta e="T218" id="Seg_3583" s="T217">v:pred</ta>
            <ta e="T219" id="Seg_3584" s="T218">v:pred 0.3.h:S</ta>
            <ta e="T220" id="Seg_3585" s="T219">v:pred 0.3.h:S</ta>
            <ta e="T221" id="Seg_3586" s="T220">pro.h:S</ta>
            <ta e="T222" id="Seg_3587" s="T221">pro.h:O</ta>
            <ta e="T223" id="Seg_3588" s="T222">v:pred</ta>
            <ta e="T226" id="Seg_3589" s="T225">v:pred 0.3.h:S</ta>
            <ta e="T230" id="Seg_3590" s="T229">v:pred 0.3.h:S</ta>
            <ta e="T231" id="Seg_3591" s="T230">v:pred 0.2.h:S</ta>
            <ta e="T234" id="Seg_3592" s="T233">conv:pred</ta>
            <ta e="T235" id="Seg_3593" s="T234">pro.h:S</ta>
            <ta e="T236" id="Seg_3594" s="T235">v:pred</ta>
            <ta e="T237" id="Seg_3595" s="T236">np:O</ta>
            <ta e="T238" id="Seg_3596" s="T237">v:pred 0.3.h:S</ta>
            <ta e="T239" id="Seg_3597" s="T238">v:pred 0.3.h:S</ta>
            <ta e="T241" id="Seg_3598" s="T240">np:S</ta>
            <ta e="T242" id="Seg_3599" s="T241">v:pred</ta>
            <ta e="T243" id="Seg_3600" s="T242">v:pred 0.3.h:S</ta>
            <ta e="T245" id="Seg_3601" s="T244">v:pred 0.3.h:S</ta>
            <ta e="T248" id="Seg_3602" s="T247">np.h:S</ta>
            <ta e="T251" id="Seg_3603" s="T250">v:pred</ta>
            <ta e="T255" id="Seg_3604" s="T254">np:O</ta>
            <ta e="T257" id="Seg_3605" s="T256">v:pred </ta>
            <ta e="T258" id="Seg_3606" s="T257">np.h:S</ta>
            <ta e="T259" id="Seg_3607" s="T258">v:pred 0.2.h:S</ta>
            <ta e="T265" id="Seg_3608" s="T264">np.h:S</ta>
            <ta e="T267" id="Seg_3609" s="T266">v:pred</ta>
            <ta e="T269" id="Seg_3610" s="T268">np:O</ta>
            <ta e="T270" id="Seg_3611" s="T269">v:pred 0.3.h:S</ta>
            <ta e="T274" id="Seg_3612" s="T273">np.h:O</ta>
            <ta e="T277" id="Seg_3613" s="T276">v:pred 0.3.h:S</ta>
            <ta e="T279" id="Seg_3614" s="T278">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T9" id="Seg_3615" s="T8">TURK:core</ta>
            <ta e="T15" id="Seg_3616" s="T14">RUS:gram</ta>
            <ta e="T22" id="Seg_3617" s="T21">TURK:disc</ta>
            <ta e="T27" id="Seg_3618" s="T26">RUS:gram</ta>
            <ta e="T29" id="Seg_3619" s="T28">TURK:core</ta>
            <ta e="T31" id="Seg_3620" s="T30">TURK:core</ta>
            <ta e="T47" id="Seg_3621" s="T46">RUS:gram</ta>
            <ta e="T50" id="Seg_3622" s="T49">TURK:disc</ta>
            <ta e="T60" id="Seg_3623" s="T59">TAT:cult</ta>
            <ta e="T64" id="Seg_3624" s="T63">TAT:cult</ta>
            <ta e="T83" id="Seg_3625" s="T82">RUS:gram</ta>
            <ta e="T93" id="Seg_3626" s="T92">RUS:gram</ta>
            <ta e="T94" id="Seg_3627" s="T93">TAT:cult</ta>
            <ta e="T95" id="Seg_3628" s="T94">TURK:disc</ta>
            <ta e="T119" id="Seg_3629" s="T118">RUS:cult</ta>
            <ta e="T127" id="Seg_3630" s="T126">RUS:core</ta>
            <ta e="T130" id="Seg_3631" s="T129">RUS:gram</ta>
            <ta e="T138" id="Seg_3632" s="T137">TURK:disc</ta>
            <ta e="T148" id="Seg_3633" s="T147">RUS:gram</ta>
            <ta e="T150" id="Seg_3634" s="T149">TURK:disc</ta>
            <ta e="T158" id="Seg_3635" s="T157">RUS:gram</ta>
            <ta e="T162" id="Seg_3636" s="T161">RUS:gram</ta>
            <ta e="T173" id="Seg_3637" s="T172">RUS:gram</ta>
            <ta e="T175" id="Seg_3638" s="T174">RUS:gram</ta>
            <ta e="T197" id="Seg_3639" s="T196">TAT:cult</ta>
            <ta e="T198" id="Seg_3640" s="T197">TAT:cult</ta>
            <ta e="T212" id="Seg_3641" s="T211">RUS:gram</ta>
            <ta e="T224" id="Seg_3642" s="T223">RUS:gram</ta>
            <ta e="T225" id="Seg_3643" s="T224">TURK:disc</ta>
            <ta e="T227" id="Seg_3644" s="T226">TURK:disc</ta>
            <ta e="T241" id="Seg_3645" s="T240">RUS:core</ta>
            <ta e="T244" id="Seg_3646" s="T243">RUS:gram</ta>
            <ta e="T249" id="Seg_3647" s="T248">TURK:core</ta>
            <ta e="T254" id="Seg_3648" s="T253">TURK:disc</ta>
            <ta e="T255" id="Seg_3649" s="T254">RUS:core</ta>
            <ta e="T261" id="Seg_3650" s="T260">RUS:gram</ta>
            <ta e="T269" id="Seg_3651" s="T268">RUS:core</ta>
            <ta e="T271" id="Seg_3652" s="T270">RUS:gram</ta>
            <ta e="T278" id="Seg_3653" s="T277">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_3654" s="T0">Жил один мужчина, его жена умерла.</ta>
            <ta e="T10" id="Seg_3655" s="T6">Он взял другую жену.</ta>
            <ta e="T18" id="Seg_3656" s="T10">У этой жены была дочь, и у этого мужчины дочь.</ta>
            <ta e="T26" id="Seg_3657" s="T18">Дочь этого мужчины какую угодно работу делала.</ta>
            <ta e="T38" id="Seg_3658" s="T26">А [жена] с ней нехорошо обходилась, говорила: «Ты очень ленивая, не работаешь».</ta>
            <ta e="T52" id="Seg_3659" s="T38">Потом она говорит мужу: «Увези свою дочь в лес и там оставь».</ta>
            <ta e="T56" id="Seg_3660" s="T52">Он увёз [её], там оставил.</ta>
            <ta e="T64" id="Seg_3661" s="T56">Потом она дом там нашла, вошла в дом.</ta>
            <ta e="T68" id="Seg_3662" s="T64">Мышка [говорит]: «Дай мне хлеба».</ta>
            <ta e="T71" id="Seg_3663" s="T68">Она ей дала.</ta>
            <ta e="T76" id="Seg_3664" s="T71">Потом пришёл человек, ночь настала.</ta>
            <ta e="T80" id="Seg_3665" s="T76">(…) дал ей колокольчик.</ta>
            <ta e="T87" id="Seg_3666" s="T80">«Ты беги, а я тебя ловить буду».</ta>
            <ta e="T96" id="Seg_3667" s="T87">Потом пришла мышка, взяла колокольчик и стала бегать по дому.</ta>
            <ta e="T105" id="Seg_3668" s="T96">Он её ловил, ловил, не смог поймать.</ta>
            <ta e="T107" id="Seg_3669" s="T105">Девушка спряталась.</ta>
            <ta e="T111" id="Seg_3670" s="T107">Настало утро, она встала.</ta>
            <ta e="T116" id="Seg_3671" s="T111">Он дал ей деньги, много одежды.</ta>
            <ta e="T120" id="Seg_3672" s="T116">Сапоги дал, платок, много [вещей].</ta>
            <ta e="T128" id="Seg_3673" s="T120">Потом эта женщина [говорит]: «Иди, забери там твоей дочери косточки.</ta>
            <ta e="T131" id="Seg_3674" s="T128">Соберёшь и привези.</ta>
            <ta e="T133" id="Seg_3675" s="T131">Мы [их] похороним».</ta>
            <ta e="T135" id="Seg_3676" s="T133">Он пошёл.</ta>
            <ta e="T142" id="Seg_3677" s="T135">Пришёл, у дочери много денег, много одежды.</ta>
            <ta e="T147" id="Seg_3678" s="T142">Он взял её, посадил на лошадь, едет.</ta>
            <ta e="T151" id="Seg_3679" s="T147">А собака лает:</ta>
            <ta e="T161" id="Seg_3680" s="T151">«Мужнина дочка едет, деньги везёт и много одежды везёт».</ta>
            <ta e="T167" id="Seg_3681" s="T161">А жена говорит: «Не ври!»</ta>
            <ta e="T177" id="Seg_3682" s="T167">Потом муж приехал, и денег много привёз, и дочь свою привёз.</ta>
            <ta e="T184" id="Seg_3683" s="T177">Потом [женщина] говорит: «Возьми мою дочь, оставь её там».</ta>
            <ta e="T188" id="Seg_3684" s="T184">Он увёз эту девушку [в лес].</ta>
            <ta e="T193" id="Seg_3685" s="T188">Мышка (?), хлеб [ей?] не дала.</ta>
            <ta e="T200" id="Seg_3686" s="T193">Потом вечером она нашла дом, вошла в дом.</ta>
            <ta e="T203" id="Seg_3687" s="T200">Там человек говорит:</ta>
            <ta e="T211" id="Seg_3688" s="T203">«Сегодня вечером (?) колокольчик, (?) я тебя ловить буду».</ta>
            <ta e="T215" id="Seg_3689" s="T211">А мышка не пришла.</ta>
            <ta e="T227" id="Seg_3690" s="T215">Она взяла колокольчик, бегала, бегала, он её поймал и съел.</ta>
            <ta e="T234" id="Seg_3691" s="T227">Потом [жена] говорит: «Пойди, забери мою дочь!»</ta>
            <ta e="T236" id="Seg_3692" s="T234">Он пошёл.</ta>
            <ta e="T238" id="Seg_3693" s="T236">Взял (мешок).</ta>
            <ta e="T242" id="Seg_3694" s="T238">Пришёл, одни косточки лежат.</ta>
            <ta e="T245" id="Seg_3695" s="T242">[Он их] собрал и привёз.</ta>
            <ta e="T251" id="Seg_3696" s="T245">Тогда собака опять лает:</ta>
            <ta e="T258" id="Seg_3697" s="T251">«Жениной дочки косточки муж везёт».</ta>
            <ta e="T263" id="Seg_3698" s="T258">«Не ври, а то (побью?) тебя!»</ta>
            <ta e="T270" id="Seg_3699" s="T263">Потом муж приехал, одни косточки привёз.</ta>
            <ta e="T279" id="Seg_3700" s="T270">И эту дочку в землю положили и закопали.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_3701" s="T0">There lived a man, his wife died.</ta>
            <ta e="T10" id="Seg_3702" s="T6">He took another wife.</ta>
            <ta e="T18" id="Seg_3703" s="T10">This woman had a daughter, and this man [had] a daughter.</ta>
            <ta e="T26" id="Seg_3704" s="T18">This man's daughter did everything.</ta>
            <ta e="T38" id="Seg_3705" s="T26">And [the woman] wasn't good with her, she's saying: "You're always lazy, you don't work."</ta>
            <ta e="T52" id="Seg_3706" s="T38">Then she says to the man: "Take your daughter to the forest and settle her there."</ta>
            <ta e="T56" id="Seg_3707" s="T52">He carried [her], settled there.</ta>
            <ta e="T64" id="Seg_3708" s="T56">Then she found there a house, came into the house.</ta>
            <ta e="T68" id="Seg_3709" s="T64">A mouse [says]: "Give me some bread."</ta>
            <ta e="T71" id="Seg_3710" s="T68">She gave it.</ta>
            <ta e="T76" id="Seg_3711" s="T71">Then a man came, night fell.</ta>
            <ta e="T80" id="Seg_3712" s="T76">(…) gave her a bell.</ta>
            <ta e="T87" id="Seg_3713" s="T80">"You run, and I will catch you."</ta>
            <ta e="T96" id="Seg_3714" s="T87">Then the mouse came, took the bell and started running through the house.</ta>
            <ta e="T105" id="Seg_3715" s="T96">He tried to catch it, it cannot catch.</ta>
            <ta e="T107" id="Seg_3716" s="T105">The girl hid.</ta>
            <ta e="T111" id="Seg_3717" s="T107">The morning came, she got up.</ta>
            <ta e="T116" id="Seg_3718" s="T111">He gave her much money, many clothes.</ta>
            <ta e="T120" id="Seg_3719" s="T116">He gave her shoes, a scarf, many [things].</ta>
            <ta e="T128" id="Seg_3720" s="T120">Then this woman [says]: "Go and take your daughter's bones.</ta>
            <ta e="T131" id="Seg_3721" s="T128">(You'll) gather and bring them.</ta>
            <ta e="T133" id="Seg_3722" s="T131">We'll bury [them]."</ta>
            <ta e="T135" id="Seg_3723" s="T133">He went.</ta>
            <ta e="T142" id="Seg_3724" s="T135">He came, the girl has much money, many clothes.</ta>
            <ta e="T147" id="Seg_3725" s="T142">He took her, seated on the horse, they are going.</ta>
            <ta e="T151" id="Seg_3726" s="T147">And the dog is barking:</ta>
            <ta e="T161" id="Seg_3727" s="T151">"The man's daughter is coming, she brings money and many clothes."</ta>
            <ta e="T167" id="Seg_3728" s="T161">The woman says: "Don't lie!"</ta>
            <ta e="T177" id="Seg_3729" s="T167">Then the man came, brought much money, and brought his daughter.</ta>
            <ta e="T184" id="Seg_3730" s="T177">Then [the woman] says: "Take my daughter, seat her there."</ta>
            <ta e="T188" id="Seg_3731" s="T184">He took this girl [to the forest].</ta>
            <ta e="T193" id="Seg_3732" s="T188">The mouse (?), did not give [it?] bread.</ta>
            <ta e="T200" id="Seg_3733" s="T193">Then in the evening she found a house, she entered the house.</ta>
            <ta e="T203" id="Seg_3734" s="T200">A man there says:</ta>
            <ta e="T211" id="Seg_3735" s="T203">“This evening (?) the bell, (?) I'l catch you.”</ta>
            <ta e="T215" id="Seg_3736" s="T211">But the mouse didn't come.</ta>
            <ta e="T227" id="Seg_3737" s="T215">She took the bell, she ran and ran, he caught her and ate her.</ta>
            <ta e="T234" id="Seg_3738" s="T227">Then [the woman] says: "Go take my daughter!"</ta>
            <ta e="T236" id="Seg_3739" s="T234">He went.</ta>
            <ta e="T238" id="Seg_3740" s="T236">Took (a bag).</ta>
            <ta e="T242" id="Seg_3741" s="T238">He came, there are only the bones lying.</ta>
            <ta e="T245" id="Seg_3742" s="T242">He gathered and brought them.</ta>
            <ta e="T251" id="Seg_3743" s="T245">Then the dog barks again:</ta>
            <ta e="T258" id="Seg_3744" s="T251">"The man is bringing the woman's daughter's bones."</ta>
            <ta e="T263" id="Seg_3745" s="T258">"Don't lie, or I'll (beat?) you!"</ta>
            <ta e="T270" id="Seg_3746" s="T263">Then the man came, he brought only bones.</ta>
            <ta e="T279" id="Seg_3747" s="T270">They buried this girl and covered her with earth.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_3748" s="T0">Es lebte ein Mann, seine Frau starb.</ta>
            <ta e="T10" id="Seg_3749" s="T6">Er nahm eine andere Frau.</ta>
            <ta e="T18" id="Seg_3750" s="T10">Diese Frau hatte eine Tochter, und dieser Mann [hatte] eine Tochter.</ta>
            <ta e="T26" id="Seg_3751" s="T18">Die Tochter des Mannes machte alles.</ta>
            <ta e="T38" id="Seg_3752" s="T26">Und [die Frau] war nicht gut zu ihr, sie sagt: "Du bist immer faul, du arbeitest nicht."</ta>
            <ta e="T52" id="Seg_3753" s="T38">Dann sagt sie zu dem Mann: "Nimm deine Tochter in den Wald und lass sie dort leben."</ta>
            <ta e="T56" id="Seg_3754" s="T52">Er trug [sie] und ließ sie dort leben.</ta>
            <ta e="T64" id="Seg_3755" s="T56">Dann fand sie ein Haus, sie kam ins Haus hinein.</ta>
            <ta e="T68" id="Seg_3756" s="T64">Eine Maus [sagt]: "Gib mir etwas Brot."</ta>
            <ta e="T71" id="Seg_3757" s="T68">Sie gab es.</ta>
            <ta e="T76" id="Seg_3758" s="T71">Dann kam ein Mann, es wurde Nacht.</ta>
            <ta e="T80" id="Seg_3759" s="T76">(…) gab ihr eine Glocke.</ta>
            <ta e="T87" id="Seg_3760" s="T80">"Du rennst und ich werde dich fangen."</ta>
            <ta e="T96" id="Seg_3761" s="T87">Dann kam die Maus, nahm die Glocke und fing an durch das Haus zu rennen.</ta>
            <ta e="T105" id="Seg_3762" s="T96">Er versuchte, sie zu fangen, er kann [sie] nicht fangen.</ta>
            <ta e="T107" id="Seg_3763" s="T105">Das Mädchen versteckte sich.</ta>
            <ta e="T111" id="Seg_3764" s="T107">Es wurde Morgen, sie stand auf.</ta>
            <ta e="T116" id="Seg_3765" s="T111">Er gab ihr viel Geld, viel Kleidung.</ta>
            <ta e="T120" id="Seg_3766" s="T116">Er gab ihr Schuhe, einen Schal, viel.</ta>
            <ta e="T128" id="Seg_3767" s="T120">Dann [sagt] diese Frau: "Geh und nimm die Knochen deiner Tochter.</ta>
            <ta e="T131" id="Seg_3768" s="T128">[Du wirst] sie sammeln und bringen.</ta>
            <ta e="T133" id="Seg_3769" s="T131">Wir begraben [sie]."</ta>
            <ta e="T135" id="Seg_3770" s="T133">Er ging.</ta>
            <ta e="T142" id="Seg_3771" s="T135">Er kam, das Mädchen hat viel Geld, viel Kleidung.</ta>
            <ta e="T147" id="Seg_3772" s="T142">Er nahm sie, setzte sie auf das Pferd, sie gehen.</ta>
            <ta e="T151" id="Seg_3773" s="T147">Und der Hund bellt.</ta>
            <ta e="T161" id="Seg_3774" s="T151">"Die Tochter des Mannes kommt, sie bringt Geld und viel Kleidung."</ta>
            <ta e="T167" id="Seg_3775" s="T161">Die Frau sagt: "Lüg nicht!"</ta>
            <ta e="T177" id="Seg_3776" s="T167">Dann kam der Mann, brachte viel Geld und brachte seine Tochter.</ta>
            <ta e="T184" id="Seg_3777" s="T177">Dann sagt [die Frau]: "Nimm meine Tochter, setz sie hierher."</ta>
            <ta e="T188" id="Seg_3778" s="T184">Er nahm dieses Mädchen [in den Wald].</ta>
            <ta e="T193" id="Seg_3779" s="T188">Der Maus (?), gab [ihm?] kein Brot.</ta>
            <ta e="T200" id="Seg_3780" s="T193">Dann am Abend fand sie ein Haus, sie ging ins Haus hinein.</ta>
            <ta e="T203" id="Seg_3781" s="T200">Ein Mann sagt dort:</ta>
            <ta e="T211" id="Seg_3782" s="T203">„Dieser Abend (?) die Glocke, (?) ich werde dich fangen.“</ta>
            <ta e="T215" id="Seg_3783" s="T211">Aber die Maus kam nicht.</ta>
            <ta e="T227" id="Seg_3784" s="T215">Sie nahm die Glocke, sie rannte und rannte, er fing sie und aß sie.</ta>
            <ta e="T234" id="Seg_3785" s="T227">Dann sagt [die Frau]: "Geh, nimm meine Tochter!"</ta>
            <ta e="T236" id="Seg_3786" s="T234">Er ging.</ta>
            <ta e="T238" id="Seg_3787" s="T236">Nahm (einen Sack).</ta>
            <ta e="T242" id="Seg_3788" s="T238">Er kam, dort lagen nur die Knochen.</ta>
            <ta e="T245" id="Seg_3789" s="T242">Er sammelte und brachte sie.</ta>
            <ta e="T251" id="Seg_3790" s="T245">Dann bellt der Hund wieder:</ta>
            <ta e="T258" id="Seg_3791" s="T251">"Der Mann bringt die Knochen der Tochter der Frau."</ta>
            <ta e="T263" id="Seg_3792" s="T258">"Lüg nicht oder ich (schlage?) dich!"</ta>
            <ta e="T270" id="Seg_3793" s="T263">Dann kam der Mann, er brachte nur Knochen.</ta>
            <ta e="T279" id="Seg_3794" s="T270">Sie begruben dieses Mädchen und bedeckten sie mit Erde.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T71" id="Seg_3795" s="T68">[GVY:] mĭmbi = mĭbi</ta>
            <ta e="T76" id="Seg_3796" s="T71">[GVY:] That is, a bear came and wants to play hoodman-blind with the girl. The mouse takes the bell and plays instead of the girl. The bear offers the girl horses and things for a good play.</ta>
            <ta e="T87" id="Seg_3797" s="T80">[GVY] The form nuʔməleʔ is probably a converb, then the whole sentence is unfinished</ta>
            <ta e="T105" id="Seg_3798" s="T96">[GVY:] Dĭ bar?</ta>
            <ta e="T111" id="Seg_3799" s="T107">[GVY:] oʔbdəbi</ta>
            <ta e="T116" id="Seg_3800" s="T111">[GVY:] mĭmbi = mĭbi.</ta>
            <ta e="T128" id="Seg_3801" s="T120">[GVY:] -ɣiʔ?</ta>
            <ta e="T131" id="Seg_3802" s="T128">da? dettɨ</ta>
            <ta e="T161" id="Seg_3803" s="T151">[GVY:] NB detləge - detle ige?</ta>
            <ta e="T193" id="Seg_3804" s="T188">‎[GVY:] Most probably, piebi = pibi 'asked' (the mouse asked for bread)</ta>
            <ta e="T238" id="Seg_3805" s="T236">[GVY:] Or ibi 'was'</ta>
            <ta e="T245" id="Seg_3806" s="T242">[GVY:] [abtubi]</ta>
            <ta e="T258" id="Seg_3807" s="T251">[GVY:] The ʔ in koʔbdo is really never heard in Plotnikova's speech; this is usually [kopto] and here even [kopdo].</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
