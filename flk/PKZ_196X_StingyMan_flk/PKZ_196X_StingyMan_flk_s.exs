<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID6FBA64A0-B548-9AB3-F7C0-70B6975A435B">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_StingyMan_flk.wav" />
         <referenced-file url="PKZ_196X_StingyMan_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_StingyMan_flk\PKZ_196X_StingyMan_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">70</ud-information>
            <ud-information attribute-name="# HIAT:w">48</ud-information>
            <ud-information attribute-name="# e">48</ud-information>
            <ud-information attribute-name="# HIAT:u">9</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="0.667" type="appl" />
         <tli id="T2" time="1.335" type="appl" />
         <tli id="T3" time="2.8465717278462934" />
         <tli id="T4" time="3.607" type="appl" />
         <tli id="T5" time="4.255" type="appl" />
         <tli id="T6" time="5.593146790779954" />
         <tli id="T7" time="6.294" type="appl" />
         <tli id="T8" time="6.879" type="appl" />
         <tli id="T9" time="7.41308609218988" />
         <tli id="T10" time="8.109" type="appl" />
         <tli id="T11" time="8.749" type="appl" />
         <tli id="T12" time="9.388" type="appl" />
         <tli id="T13" time="10.028" type="appl" />
         <tli id="T14" time="10.667" type="appl" />
         <tli id="T15" time="11.307" type="appl" />
         <tli id="T16" time="12.452918003786596" />
         <tli id="T17" time="13.358" type="appl" />
         <tli id="T18" time="14.036" type="appl" />
         <tli id="T19" time="14.713" type="appl" />
         <tli id="T20" time="15.391" type="appl" />
         <tli id="T21" time="18.0060661286015" />
         <tli id="T22" time="18.82" type="appl" />
         <tli id="T23" time="19.306" type="appl" />
         <tli id="T24" time="19.793" type="appl" />
         <tli id="T25" time="20.279" type="appl" />
         <tli id="T26" time="20.766" type="appl" />
         <tli id="T27" time="21.133" type="appl" />
         <tli id="T28" time="21.499" type="appl" />
         <tli id="T29" time="22.519248938325013" />
         <tli id="T30" time="23.444" type="appl" />
         <tli id="T31" time="24.332" type="appl" />
         <tli id="T32" time="25.219" type="appl" />
         <tli id="T33" time="26.107" type="appl" />
         <tli id="T34" time="26.995" type="appl" />
         <tli id="T35" time="27.883" type="appl" />
         <tli id="T36" time="28.77" type="appl" />
         <tli id="T37" time="29.658" type="appl" />
         <tli id="T38" time="31.645611222684675" />
         <tli id="T39" time="32.434" type="appl" />
         <tli id="T40" time="32.915" type="appl" />
         <tli id="T41" time="33.395" type="appl" />
         <tli id="T42" time="33.875" type="appl" />
         <tli id="T43" time="34.356" type="appl" />
         <tli id="T44" time="34.836" type="appl" />
         <tli id="T45" time="35.316" type="appl" />
         <tli id="T46" time="35.797" type="appl" />
         <tli id="T47" time="38.285389772883526" />
         <tli id="T48" time="39.392" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T48" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Onʼiʔ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">kuza</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">amnobi</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T6" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Ugaːndə</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">mori</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">ibi</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_26" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">Dĭgəttə</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">kuza</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">šobi</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_38" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_40" n="HIAT:w" s="T9">Dĭ</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">dĭʔnə</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">nuldəbi</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">segi</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">bü</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">i</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">sĭreʔpne</ts>
                  <nts id="Seg_59" n="HIAT:ip">.</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_62" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">Dĭ</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">kuza</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">mandolaʔbə</ts>
                  <nts id="Seg_71" n="HIAT:ip">,</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_74" n="HIAT:w" s="T19">sĭreʔpne</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_76" n="HIAT:ip">(</nts>
                  <ts e="T21" id="Seg_78" n="HIAT:w" s="T20">üdʼügeʔi</ts>
                  <nts id="Seg_79" n="HIAT:ip">)</nts>
                  <nts id="Seg_80" n="HIAT:ip">.</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_83" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_85" n="HIAT:w" s="T21">Dĭgəttə</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_88" n="HIAT:w" s="T22">dĭ</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_91" n="HIAT:w" s="T23">šide</ts>
                  <nts id="Seg_92" n="HIAT:ip">,</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_95" n="HIAT:w" s="T24">nagur</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_97" n="HIAT:ip">(</nts>
                  <ts e="T26" id="Seg_99" n="HIAT:w" s="T25">ilie</ts>
                  <nts id="Seg_100" n="HIAT:ip">)</nts>
                  <nts id="Seg_101" n="HIAT:ip">.</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_104" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_106" n="HIAT:w" s="T26">A</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_109" n="HIAT:w" s="T27">dĭ</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_112" n="HIAT:w" s="T28">măndə:</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_115" n="HIAT:w" s="T29">Šiʔnʼileʔ</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_118" n="HIAT:w" s="T30">il</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_120" n="HIAT:ip">(</nts>
                  <ts e="T32" id="Seg_122" n="HIAT:w" s="T31">kulaʔi-</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_125" n="HIAT:w" s="T32">ku-</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_128" n="HIAT:w" s="T33">kubi-</ts>
                  <nts id="Seg_129" n="HIAT:ip">)</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_131" n="HIAT:ip">(</nts>
                  <ts e="T35" id="Seg_133" n="HIAT:w" s="T34">kulalʔi</ts>
                  <nts id="Seg_134" n="HIAT:ip">)</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_137" n="HIAT:w" s="T35">dăk</ts>
                  <nts id="Seg_138" n="HIAT:ip">,</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_141" n="HIAT:w" s="T36">nagurgön</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_144" n="HIAT:w" s="T37">ellieʔi</ts>
                  <nts id="Seg_145" n="HIAT:ip">.</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_148" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_150" n="HIAT:w" s="T38">A</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_153" n="HIAT:w" s="T39">üdʼügeʔi</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_156" n="HIAT:w" s="T40">dăk</ts>
                  <nts id="Seg_157" n="HIAT:ip">,</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_160" n="HIAT:w" s="T41">i</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_163" n="HIAT:w" s="T42">nagurgöʔ</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_166" n="HIAT:w" s="T43">ellieʔi</ts>
                  <nts id="Seg_167" n="HIAT:ip">,</nts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_170" n="HIAT:w" s="T44">i</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_173" n="HIAT:w" s="T45">šide</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_176" n="HIAT:w" s="T46">ellieʔi</ts>
                  <nts id="Seg_177" n="HIAT:ip">.</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_180" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_182" n="HIAT:w" s="T47">Kabarləj</ts>
                  <nts id="Seg_183" n="HIAT:ip">.</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T48" id="Seg_185" n="sc" s="T0">
               <ts e="T1" id="Seg_187" n="e" s="T0">Onʼiʔ </ts>
               <ts e="T2" id="Seg_189" n="e" s="T1">kuza </ts>
               <ts e="T3" id="Seg_191" n="e" s="T2">amnobi. </ts>
               <ts e="T4" id="Seg_193" n="e" s="T3">Ugaːndə </ts>
               <ts e="T5" id="Seg_195" n="e" s="T4">mori </ts>
               <ts e="T6" id="Seg_197" n="e" s="T5">ibi. </ts>
               <ts e="T7" id="Seg_199" n="e" s="T6">Dĭgəttə </ts>
               <ts e="T8" id="Seg_201" n="e" s="T7">kuza </ts>
               <ts e="T9" id="Seg_203" n="e" s="T8">šobi. </ts>
               <ts e="T10" id="Seg_205" n="e" s="T9">Dĭ </ts>
               <ts e="T11" id="Seg_207" n="e" s="T10">dĭʔnə </ts>
               <ts e="T12" id="Seg_209" n="e" s="T11">nuldəbi </ts>
               <ts e="T13" id="Seg_211" n="e" s="T12">segi </ts>
               <ts e="T14" id="Seg_213" n="e" s="T13">bü </ts>
               <ts e="T15" id="Seg_215" n="e" s="T14">i </ts>
               <ts e="T16" id="Seg_217" n="e" s="T15">sĭreʔpne. </ts>
               <ts e="T17" id="Seg_219" n="e" s="T16">Dĭ </ts>
               <ts e="T18" id="Seg_221" n="e" s="T17">kuza </ts>
               <ts e="T19" id="Seg_223" n="e" s="T18">mandolaʔbə, </ts>
               <ts e="T20" id="Seg_225" n="e" s="T19">sĭreʔpne </ts>
               <ts e="T21" id="Seg_227" n="e" s="T20">(üdʼügeʔi). </ts>
               <ts e="T22" id="Seg_229" n="e" s="T21">Dĭgəttə </ts>
               <ts e="T23" id="Seg_231" n="e" s="T22">dĭ </ts>
               <ts e="T24" id="Seg_233" n="e" s="T23">šide, </ts>
               <ts e="T25" id="Seg_235" n="e" s="T24">nagur </ts>
               <ts e="T26" id="Seg_237" n="e" s="T25">(ilie). </ts>
               <ts e="T27" id="Seg_239" n="e" s="T26">A </ts>
               <ts e="T28" id="Seg_241" n="e" s="T27">dĭ </ts>
               <ts e="T29" id="Seg_243" n="e" s="T28">măndə: </ts>
               <ts e="T30" id="Seg_245" n="e" s="T29">Šiʔnʼileʔ </ts>
               <ts e="T31" id="Seg_247" n="e" s="T30">il </ts>
               <ts e="T32" id="Seg_249" n="e" s="T31">(kulaʔi- </ts>
               <ts e="T33" id="Seg_251" n="e" s="T32">ku- </ts>
               <ts e="T34" id="Seg_253" n="e" s="T33">kubi-) </ts>
               <ts e="T35" id="Seg_255" n="e" s="T34">(kulalʔi) </ts>
               <ts e="T36" id="Seg_257" n="e" s="T35">dăk, </ts>
               <ts e="T37" id="Seg_259" n="e" s="T36">nagurgön </ts>
               <ts e="T38" id="Seg_261" n="e" s="T37">ellieʔi. </ts>
               <ts e="T39" id="Seg_263" n="e" s="T38">A </ts>
               <ts e="T40" id="Seg_265" n="e" s="T39">üdʼügeʔi </ts>
               <ts e="T41" id="Seg_267" n="e" s="T40">dăk, </ts>
               <ts e="T42" id="Seg_269" n="e" s="T41">i </ts>
               <ts e="T43" id="Seg_271" n="e" s="T42">nagurgöʔ </ts>
               <ts e="T44" id="Seg_273" n="e" s="T43">ellieʔi, </ts>
               <ts e="T45" id="Seg_275" n="e" s="T44">i </ts>
               <ts e="T46" id="Seg_277" n="e" s="T45">šide </ts>
               <ts e="T47" id="Seg_279" n="e" s="T46">ellieʔi. </ts>
               <ts e="T48" id="Seg_281" n="e" s="T47">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_282" s="T0">PKZ_196X_StingyMan_flk.001 (001)</ta>
            <ta e="T6" id="Seg_283" s="T3">PKZ_196X_StingyMan_flk.002 (002)</ta>
            <ta e="T9" id="Seg_284" s="T6">PKZ_196X_StingyMan_flk.003 (003)</ta>
            <ta e="T16" id="Seg_285" s="T9">PKZ_196X_StingyMan_flk.004 (004)</ta>
            <ta e="T21" id="Seg_286" s="T16">PKZ_196X_StingyMan_flk.005 (005)</ta>
            <ta e="T26" id="Seg_287" s="T21">PKZ_196X_StingyMan_flk.006 (006)</ta>
            <ta e="T38" id="Seg_288" s="T26">PKZ_196X_StingyMan_flk.007 (007) </ta>
            <ta e="T47" id="Seg_289" s="T38">PKZ_196X_StingyMan_flk.008 (009)</ta>
            <ta e="T48" id="Seg_290" s="T47">PKZ_196X_StingyMan_flk.009 (010)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_291" s="T0">Onʼiʔ kuza amnobi. </ta>
            <ta e="T6" id="Seg_292" s="T3">Ugaːndə mori ibi. </ta>
            <ta e="T9" id="Seg_293" s="T6">Dĭgəttə kuza šobi. </ta>
            <ta e="T16" id="Seg_294" s="T9">Dĭ dĭʔnə nuldəbi segi bü i sĭreʔpne. </ta>
            <ta e="T21" id="Seg_295" s="T16">Dĭ kuza mandolaʔbə, sĭreʔpne (üdʼügeʔi). </ta>
            <ta e="T26" id="Seg_296" s="T21">Dĭgəttə dĭ šide, nagur (ilie). </ta>
            <ta e="T38" id="Seg_297" s="T26">A dĭ măndə: Šiʔnʼileʔ il (kulaʔi- ku- kubi-) (kulalʔi) dăk, nagurgön ellieʔi. </ta>
            <ta e="T47" id="Seg_298" s="T38">A üdʼügeʔi dăk, i nagurgöʔ ellieʔi, i šide ellieʔi. </ta>
            <ta e="T48" id="Seg_299" s="T47">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_300" s="T0">onʼiʔ</ta>
            <ta e="T2" id="Seg_301" s="T1">kuza</ta>
            <ta e="T3" id="Seg_302" s="T2">amno-bi</ta>
            <ta e="T4" id="Seg_303" s="T3">ugaːndə</ta>
            <ta e="T5" id="Seg_304" s="T4">mori</ta>
            <ta e="T6" id="Seg_305" s="T5">i-bi</ta>
            <ta e="T7" id="Seg_306" s="T6">dĭgəttə</ta>
            <ta e="T8" id="Seg_307" s="T7">kuza</ta>
            <ta e="T9" id="Seg_308" s="T8">šo-bi</ta>
            <ta e="T10" id="Seg_309" s="T9">dĭ</ta>
            <ta e="T11" id="Seg_310" s="T10">dĭʔ-nə</ta>
            <ta e="T12" id="Seg_311" s="T11">nuldə-bi</ta>
            <ta e="T13" id="Seg_312" s="T12">segi</ta>
            <ta e="T14" id="Seg_313" s="T13">bü</ta>
            <ta e="T15" id="Seg_314" s="T14">i</ta>
            <ta e="T16" id="Seg_315" s="T15">sĭreʔpne</ta>
            <ta e="T17" id="Seg_316" s="T16">dĭ</ta>
            <ta e="T18" id="Seg_317" s="T17">kuza</ta>
            <ta e="T19" id="Seg_318" s="T18">mando-laʔbə</ta>
            <ta e="T20" id="Seg_319" s="T19">sĭreʔpne</ta>
            <ta e="T21" id="Seg_320" s="T20">üdʼüge-ʔi</ta>
            <ta e="T22" id="Seg_321" s="T21">dĭgəttə</ta>
            <ta e="T23" id="Seg_322" s="T22">dĭ</ta>
            <ta e="T24" id="Seg_323" s="T23">šide</ta>
            <ta e="T25" id="Seg_324" s="T24">nagur</ta>
            <ta e="T26" id="Seg_325" s="T25">i-lie</ta>
            <ta e="T27" id="Seg_326" s="T26">a</ta>
            <ta e="T28" id="Seg_327" s="T27">dĭ</ta>
            <ta e="T29" id="Seg_328" s="T28">măn-də</ta>
            <ta e="T30" id="Seg_329" s="T29">šiʔnʼileʔ</ta>
            <ta e="T31" id="Seg_330" s="T30">il</ta>
            <ta e="T35" id="Seg_331" s="T34">ku-lal-ʔi</ta>
            <ta e="T36" id="Seg_332" s="T35">dăk</ta>
            <ta e="T37" id="Seg_333" s="T36">nagur-gön</ta>
            <ta e="T38" id="Seg_334" s="T37">el-lie-ʔi</ta>
            <ta e="T39" id="Seg_335" s="T38">a</ta>
            <ta e="T40" id="Seg_336" s="T39">üdʼüge-ʔi</ta>
            <ta e="T41" id="Seg_337" s="T40">dăk</ta>
            <ta e="T42" id="Seg_338" s="T41">i</ta>
            <ta e="T43" id="Seg_339" s="T42">nagur-göʔ</ta>
            <ta e="T44" id="Seg_340" s="T43">el-lie-ʔi</ta>
            <ta e="T45" id="Seg_341" s="T44">i</ta>
            <ta e="T46" id="Seg_342" s="T45">šide</ta>
            <ta e="T47" id="Seg_343" s="T46">el-lie-ʔi</ta>
            <ta e="T48" id="Seg_344" s="T47">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_345" s="T0">onʼiʔ</ta>
            <ta e="T2" id="Seg_346" s="T1">kuza</ta>
            <ta e="T3" id="Seg_347" s="T2">amno-bi</ta>
            <ta e="T4" id="Seg_348" s="T3">ugaːndə</ta>
            <ta e="T5" id="Seg_349" s="T4">marʼi</ta>
            <ta e="T6" id="Seg_350" s="T5">i-bi</ta>
            <ta e="T7" id="Seg_351" s="T6">dĭgəttə</ta>
            <ta e="T8" id="Seg_352" s="T7">kuza</ta>
            <ta e="T9" id="Seg_353" s="T8">šo-bi</ta>
            <ta e="T10" id="Seg_354" s="T9">dĭ</ta>
            <ta e="T11" id="Seg_355" s="T10">dĭ-Tə</ta>
            <ta e="T12" id="Seg_356" s="T11">nuldə-bi</ta>
            <ta e="T13" id="Seg_357" s="T12">segi</ta>
            <ta e="T14" id="Seg_358" s="T13">bü</ta>
            <ta e="T15" id="Seg_359" s="T14">i</ta>
            <ta e="T16" id="Seg_360" s="T15">sĭreʔp</ta>
            <ta e="T17" id="Seg_361" s="T16">dĭ</ta>
            <ta e="T18" id="Seg_362" s="T17">kuza</ta>
            <ta e="T19" id="Seg_363" s="T18">măndo-laʔbə</ta>
            <ta e="T20" id="Seg_364" s="T19">sĭreʔp</ta>
            <ta e="T21" id="Seg_365" s="T20">üdʼüge-jəʔ</ta>
            <ta e="T22" id="Seg_366" s="T21">dĭgəttə</ta>
            <ta e="T23" id="Seg_367" s="T22">dĭ</ta>
            <ta e="T24" id="Seg_368" s="T23">šide</ta>
            <ta e="T25" id="Seg_369" s="T24">nagur</ta>
            <ta e="T26" id="Seg_370" s="T25">i-liA</ta>
            <ta e="T27" id="Seg_371" s="T26">a</ta>
            <ta e="T28" id="Seg_372" s="T27">dĭ</ta>
            <ta e="T29" id="Seg_373" s="T28">măn-ntə</ta>
            <ta e="T30" id="Seg_374" s="T29">šiʔnʼileʔ</ta>
            <ta e="T31" id="Seg_375" s="T30">il</ta>
            <ta e="T35" id="Seg_376" s="T34">ku-lal-jəʔ</ta>
            <ta e="T36" id="Seg_377" s="T35">tak</ta>
            <ta e="T37" id="Seg_378" s="T36">nagur-gön</ta>
            <ta e="T38" id="Seg_379" s="T37">hen-liA-jəʔ</ta>
            <ta e="T39" id="Seg_380" s="T38">a</ta>
            <ta e="T40" id="Seg_381" s="T39">üdʼüge-jəʔ</ta>
            <ta e="T41" id="Seg_382" s="T40">tak</ta>
            <ta e="T42" id="Seg_383" s="T41">i</ta>
            <ta e="T43" id="Seg_384" s="T42">nagur-göʔ</ta>
            <ta e="T44" id="Seg_385" s="T43">hen-liA-jəʔ</ta>
            <ta e="T45" id="Seg_386" s="T44">i</ta>
            <ta e="T46" id="Seg_387" s="T45">šide</ta>
            <ta e="T47" id="Seg_388" s="T46">hen-liA-jəʔ</ta>
            <ta e="T48" id="Seg_389" s="T47">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_390" s="T0">one.[NOM.SG]</ta>
            <ta e="T2" id="Seg_391" s="T1">man.[NOM.SG]</ta>
            <ta e="T3" id="Seg_392" s="T2">live-PST.[3SG]</ta>
            <ta e="T4" id="Seg_393" s="T3">very</ta>
            <ta e="T5" id="Seg_394" s="T4">stingy.[NOM.SG]</ta>
            <ta e="T6" id="Seg_395" s="T5">be-PST.[3SG]</ta>
            <ta e="T7" id="Seg_396" s="T6">then</ta>
            <ta e="T8" id="Seg_397" s="T7">man.[NOM.SG]</ta>
            <ta e="T9" id="Seg_398" s="T8">come-PST.[3SG]</ta>
            <ta e="T10" id="Seg_399" s="T9">this.[NOM.SG]</ta>
            <ta e="T11" id="Seg_400" s="T10">this-LAT</ta>
            <ta e="T12" id="Seg_401" s="T11">place-PST.[3SG]</ta>
            <ta e="T13" id="Seg_402" s="T12">yellow.[NOM.SG]</ta>
            <ta e="T14" id="Seg_403" s="T13">water.[NOM.SG]</ta>
            <ta e="T15" id="Seg_404" s="T14">and</ta>
            <ta e="T16" id="Seg_405" s="T15">sugar.[NOM.SG]</ta>
            <ta e="T17" id="Seg_406" s="T16">this.[NOM.SG]</ta>
            <ta e="T18" id="Seg_407" s="T17">man.[NOM.SG]</ta>
            <ta e="T19" id="Seg_408" s="T18">look-DUR.[3SG]</ta>
            <ta e="T20" id="Seg_409" s="T19">sugar.[NOM.SG]</ta>
            <ta e="T21" id="Seg_410" s="T20">small-PL</ta>
            <ta e="T22" id="Seg_411" s="T21">then</ta>
            <ta e="T23" id="Seg_412" s="T22">this.[NOM.SG]</ta>
            <ta e="T24" id="Seg_413" s="T23">two.[NOM.SG]</ta>
            <ta e="T25" id="Seg_414" s="T24">three.[NOM.SG]</ta>
            <ta e="T26" id="Seg_415" s="T25">take-PRS.[3SG]</ta>
            <ta e="T27" id="Seg_416" s="T26">and</ta>
            <ta e="T28" id="Seg_417" s="T27">this.[NOM.SG]</ta>
            <ta e="T29" id="Seg_418" s="T28">say-IPFVZ.[3SG]</ta>
            <ta e="T30" id="Seg_419" s="T29">you.PL.ACC</ta>
            <ta e="T31" id="Seg_420" s="T30">people.[NOM.SG]</ta>
            <ta e="T35" id="Seg_421" s="T34">see-%%-3PL</ta>
            <ta e="T36" id="Seg_422" s="T35">so</ta>
            <ta e="T37" id="Seg_423" s="T36">three-%%</ta>
            <ta e="T38" id="Seg_424" s="T37">put-PRS-3PL</ta>
            <ta e="T39" id="Seg_425" s="T38">and</ta>
            <ta e="T40" id="Seg_426" s="T39">small-PL</ta>
            <ta e="T41" id="Seg_427" s="T40">so</ta>
            <ta e="T42" id="Seg_428" s="T41">and</ta>
            <ta e="T43" id="Seg_429" s="T42">three-COLL</ta>
            <ta e="T44" id="Seg_430" s="T43">put-PRS-3PL</ta>
            <ta e="T45" id="Seg_431" s="T44">and</ta>
            <ta e="T46" id="Seg_432" s="T45">two.[NOM.SG]</ta>
            <ta e="T47" id="Seg_433" s="T46">put-PRS-3PL</ta>
            <ta e="T48" id="Seg_434" s="T47">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_435" s="T0">один.[NOM.SG]</ta>
            <ta e="T2" id="Seg_436" s="T1">мужчина.[NOM.SG]</ta>
            <ta e="T3" id="Seg_437" s="T2">жить-PST.[3SG]</ta>
            <ta e="T4" id="Seg_438" s="T3">очень</ta>
            <ta e="T5" id="Seg_439" s="T4">жадный.[NOM.SG]</ta>
            <ta e="T6" id="Seg_440" s="T5">быть-PST.[3SG]</ta>
            <ta e="T7" id="Seg_441" s="T6">тогда</ta>
            <ta e="T8" id="Seg_442" s="T7">мужчина.[NOM.SG]</ta>
            <ta e="T9" id="Seg_443" s="T8">прийти-PST.[3SG]</ta>
            <ta e="T10" id="Seg_444" s="T9">этот.[NOM.SG]</ta>
            <ta e="T11" id="Seg_445" s="T10">этот-LAT</ta>
            <ta e="T12" id="Seg_446" s="T11">поставить-PST.[3SG]</ta>
            <ta e="T13" id="Seg_447" s="T12">желтый.[NOM.SG]</ta>
            <ta e="T14" id="Seg_448" s="T13">вода.[NOM.SG]</ta>
            <ta e="T15" id="Seg_449" s="T14">и</ta>
            <ta e="T16" id="Seg_450" s="T15">сахар.[NOM.SG]</ta>
            <ta e="T17" id="Seg_451" s="T16">этот.[NOM.SG]</ta>
            <ta e="T18" id="Seg_452" s="T17">мужчина.[NOM.SG]</ta>
            <ta e="T19" id="Seg_453" s="T18">смотреть-DUR.[3SG]</ta>
            <ta e="T20" id="Seg_454" s="T19">сахар.[NOM.SG]</ta>
            <ta e="T21" id="Seg_455" s="T20">маленький-PL</ta>
            <ta e="T22" id="Seg_456" s="T21">тогда</ta>
            <ta e="T23" id="Seg_457" s="T22">этот.[NOM.SG]</ta>
            <ta e="T24" id="Seg_458" s="T23">два.[NOM.SG]</ta>
            <ta e="T25" id="Seg_459" s="T24">три.[NOM.SG]</ta>
            <ta e="T26" id="Seg_460" s="T25">взять-PRS.[3SG]</ta>
            <ta e="T27" id="Seg_461" s="T26">а</ta>
            <ta e="T28" id="Seg_462" s="T27">этот.[NOM.SG]</ta>
            <ta e="T29" id="Seg_463" s="T28">сказать-IPFVZ.[3SG]</ta>
            <ta e="T30" id="Seg_464" s="T29">вы.ACC</ta>
            <ta e="T31" id="Seg_465" s="T30">люди.[NOM.SG]</ta>
            <ta e="T35" id="Seg_466" s="T34">видеть-%%-3PL</ta>
            <ta e="T36" id="Seg_467" s="T35">так</ta>
            <ta e="T37" id="Seg_468" s="T36">три-%%</ta>
            <ta e="T38" id="Seg_469" s="T37">класть-PRS-3PL</ta>
            <ta e="T39" id="Seg_470" s="T38">а</ta>
            <ta e="T40" id="Seg_471" s="T39">маленький-PL</ta>
            <ta e="T41" id="Seg_472" s="T40">так</ta>
            <ta e="T42" id="Seg_473" s="T41">и</ta>
            <ta e="T43" id="Seg_474" s="T42">три-COLL</ta>
            <ta e="T44" id="Seg_475" s="T43">класть-PRS-3PL</ta>
            <ta e="T45" id="Seg_476" s="T44">и</ta>
            <ta e="T46" id="Seg_477" s="T45">два.[NOM.SG]</ta>
            <ta e="T47" id="Seg_478" s="T46">класть-PRS-3PL</ta>
            <ta e="T48" id="Seg_479" s="T47">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_480" s="T0">num-n:case</ta>
            <ta e="T2" id="Seg_481" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_482" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_483" s="T3">adv</ta>
            <ta e="T5" id="Seg_484" s="T4">adj-n:case</ta>
            <ta e="T6" id="Seg_485" s="T5">v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_486" s="T6">adv</ta>
            <ta e="T8" id="Seg_487" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_488" s="T8">v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_489" s="T9">dempro-n:case</ta>
            <ta e="T11" id="Seg_490" s="T10">dempro-n:case</ta>
            <ta e="T12" id="Seg_491" s="T11">v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_492" s="T12">adj-n:case</ta>
            <ta e="T14" id="Seg_493" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_494" s="T14">conj</ta>
            <ta e="T16" id="Seg_495" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_496" s="T16">dempro-n:case</ta>
            <ta e="T18" id="Seg_497" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_498" s="T18">v-v&gt;v-v:pn</ta>
            <ta e="T20" id="Seg_499" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_500" s="T20">adj-n:num</ta>
            <ta e="T22" id="Seg_501" s="T21">adv</ta>
            <ta e="T23" id="Seg_502" s="T22">dempro-n:case</ta>
            <ta e="T24" id="Seg_503" s="T23">num-n:case</ta>
            <ta e="T25" id="Seg_504" s="T24">num-n:case</ta>
            <ta e="T26" id="Seg_505" s="T25">v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_506" s="T26">conj</ta>
            <ta e="T28" id="Seg_507" s="T27">dempro-n:case</ta>
            <ta e="T29" id="Seg_508" s="T28">v-v&gt;v-v:pn</ta>
            <ta e="T30" id="Seg_509" s="T29">pers</ta>
            <ta e="T31" id="Seg_510" s="T30">n-n:case</ta>
            <ta e="T35" id="Seg_511" s="T34">v-%%-v:pn</ta>
            <ta e="T36" id="Seg_512" s="T35">ptcl</ta>
            <ta e="T37" id="Seg_513" s="T36">num-%%</ta>
            <ta e="T38" id="Seg_514" s="T37">v-v:tense-v:pn</ta>
            <ta e="T39" id="Seg_515" s="T38">conj</ta>
            <ta e="T40" id="Seg_516" s="T39">adj-n:num</ta>
            <ta e="T41" id="Seg_517" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_518" s="T41">conj</ta>
            <ta e="T43" id="Seg_519" s="T42">num-num&gt;num</ta>
            <ta e="T44" id="Seg_520" s="T43">v-v:tense-v:pn</ta>
            <ta e="T45" id="Seg_521" s="T44">conj</ta>
            <ta e="T46" id="Seg_522" s="T45">num-n:case</ta>
            <ta e="T47" id="Seg_523" s="T46">v-v:tense-v:pn</ta>
            <ta e="T48" id="Seg_524" s="T47">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_525" s="T0">num</ta>
            <ta e="T2" id="Seg_526" s="T1">n</ta>
            <ta e="T3" id="Seg_527" s="T2">v</ta>
            <ta e="T4" id="Seg_528" s="T3">adv</ta>
            <ta e="T5" id="Seg_529" s="T4">adj</ta>
            <ta e="T6" id="Seg_530" s="T5">v</ta>
            <ta e="T7" id="Seg_531" s="T6">adv</ta>
            <ta e="T8" id="Seg_532" s="T7">n</ta>
            <ta e="T9" id="Seg_533" s="T8">v</ta>
            <ta e="T10" id="Seg_534" s="T9">dempro</ta>
            <ta e="T11" id="Seg_535" s="T10">dempro</ta>
            <ta e="T12" id="Seg_536" s="T11">v</ta>
            <ta e="T13" id="Seg_537" s="T12">adj</ta>
            <ta e="T14" id="Seg_538" s="T13">n</ta>
            <ta e="T15" id="Seg_539" s="T14">conj</ta>
            <ta e="T16" id="Seg_540" s="T15">n</ta>
            <ta e="T17" id="Seg_541" s="T16">dempro</ta>
            <ta e="T18" id="Seg_542" s="T17">n</ta>
            <ta e="T19" id="Seg_543" s="T18">v</ta>
            <ta e="T20" id="Seg_544" s="T19">n</ta>
            <ta e="T21" id="Seg_545" s="T20">adj</ta>
            <ta e="T22" id="Seg_546" s="T21">adv</ta>
            <ta e="T23" id="Seg_547" s="T22">dempro</ta>
            <ta e="T24" id="Seg_548" s="T23">num</ta>
            <ta e="T25" id="Seg_549" s="T24">num</ta>
            <ta e="T26" id="Seg_550" s="T25">v</ta>
            <ta e="T27" id="Seg_551" s="T26">conj</ta>
            <ta e="T28" id="Seg_552" s="T27">dempro</ta>
            <ta e="T29" id="Seg_553" s="T28">v</ta>
            <ta e="T30" id="Seg_554" s="T29">pers</ta>
            <ta e="T31" id="Seg_555" s="T30">n</ta>
            <ta e="T35" id="Seg_556" s="T34">v</ta>
            <ta e="T36" id="Seg_557" s="T35">ptcl</ta>
            <ta e="T37" id="Seg_558" s="T36">num</ta>
            <ta e="T38" id="Seg_559" s="T37">v</ta>
            <ta e="T39" id="Seg_560" s="T38">conj</ta>
            <ta e="T40" id="Seg_561" s="T39">adj</ta>
            <ta e="T41" id="Seg_562" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_563" s="T41">conj</ta>
            <ta e="T43" id="Seg_564" s="T42">num</ta>
            <ta e="T44" id="Seg_565" s="T43">v</ta>
            <ta e="T45" id="Seg_566" s="T44">conj</ta>
            <ta e="T46" id="Seg_567" s="T45">num</ta>
            <ta e="T47" id="Seg_568" s="T46">v</ta>
            <ta e="T48" id="Seg_569" s="T47">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_570" s="T1">np.h:E</ta>
            <ta e="T6" id="Seg_571" s="T5">0.3.h:Th</ta>
            <ta e="T7" id="Seg_572" s="T6">adv:Time</ta>
            <ta e="T8" id="Seg_573" s="T7">np.h:A</ta>
            <ta e="T10" id="Seg_574" s="T9">pro.h:A</ta>
            <ta e="T11" id="Seg_575" s="T10">pro.h:B</ta>
            <ta e="T14" id="Seg_576" s="T13">np:Th</ta>
            <ta e="T16" id="Seg_577" s="T15">np:Th</ta>
            <ta e="T18" id="Seg_578" s="T17">np.h:A</ta>
            <ta e="T20" id="Seg_579" s="T19">np:Th</ta>
            <ta e="T22" id="Seg_580" s="T21">adv:Time</ta>
            <ta e="T23" id="Seg_581" s="T22">pro.h:A</ta>
            <ta e="T24" id="Seg_582" s="T23">np:Th</ta>
            <ta e="T25" id="Seg_583" s="T24">np:Th</ta>
            <ta e="T28" id="Seg_584" s="T27">pro.h:A</ta>
            <ta e="T30" id="Seg_585" s="T29">pro.h:Th</ta>
            <ta e="T31" id="Seg_586" s="T30">np.h:E</ta>
            <ta e="T37" id="Seg_587" s="T36">np.h:Th</ta>
            <ta e="T38" id="Seg_588" s="T37">0.3.h:A</ta>
            <ta e="T43" id="Seg_589" s="T42">np:Th</ta>
            <ta e="T44" id="Seg_590" s="T43">0.3.h:A</ta>
            <ta e="T46" id="Seg_591" s="T45">np:Th</ta>
            <ta e="T47" id="Seg_592" s="T46">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_593" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_594" s="T2">v:pred</ta>
            <ta e="T5" id="Seg_595" s="T4">adj:pred</ta>
            <ta e="T6" id="Seg_596" s="T5">cop 0.3.h:S</ta>
            <ta e="T8" id="Seg_597" s="T7">np.h:S</ta>
            <ta e="T9" id="Seg_598" s="T8">v:pred</ta>
            <ta e="T10" id="Seg_599" s="T9">pro.h:S</ta>
            <ta e="T12" id="Seg_600" s="T11">v:pred</ta>
            <ta e="T14" id="Seg_601" s="T13">np:O</ta>
            <ta e="T16" id="Seg_602" s="T15">np:O</ta>
            <ta e="T18" id="Seg_603" s="T17">np.h:S</ta>
            <ta e="T19" id="Seg_604" s="T18">v:pred</ta>
            <ta e="T20" id="Seg_605" s="T19">np:S</ta>
            <ta e="T21" id="Seg_606" s="T20">adj:pred</ta>
            <ta e="T23" id="Seg_607" s="T22">pro.h:S</ta>
            <ta e="T24" id="Seg_608" s="T23">np:O</ta>
            <ta e="T25" id="Seg_609" s="T24">np:O</ta>
            <ta e="T26" id="Seg_610" s="T25">v:pred</ta>
            <ta e="T28" id="Seg_611" s="T27">pro.h:S</ta>
            <ta e="T29" id="Seg_612" s="T28">v:pred</ta>
            <ta e="T30" id="Seg_613" s="T29">pro.h:O</ta>
            <ta e="T31" id="Seg_614" s="T30">np.h:S</ta>
            <ta e="T35" id="Seg_615" s="T34">v:pred</ta>
            <ta e="T37" id="Seg_616" s="T36">np.h:O</ta>
            <ta e="T38" id="Seg_617" s="T37">v:pred 0.3.h:S</ta>
            <ta e="T40" id="Seg_618" s="T39">adj:pred</ta>
            <ta e="T43" id="Seg_619" s="T42">np:O</ta>
            <ta e="T44" id="Seg_620" s="T43">v:pred 0.3.h:S</ta>
            <ta e="T46" id="Seg_621" s="T45">np:O</ta>
            <ta e="T47" id="Seg_622" s="T46">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T15" id="Seg_623" s="T14">RUS:gram</ta>
            <ta e="T27" id="Seg_624" s="T26">RUS:gram</ta>
            <ta e="T36" id="Seg_625" s="T35">RUS:gram</ta>
            <ta e="T39" id="Seg_626" s="T38">RUS:gram</ta>
            <ta e="T41" id="Seg_627" s="T40">RUS:gram</ta>
            <ta e="T42" id="Seg_628" s="T41">RUS:gram</ta>
            <ta e="T45" id="Seg_629" s="T44">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_630" s="T0">Жил один человек.</ta>
            <ta e="T6" id="Seg_631" s="T3">Он был очень жадный.</ta>
            <ta e="T9" id="Seg_632" s="T6">Потом пришёл [другой] человек.</ta>
            <ta e="T16" id="Seg_633" s="T9">Он подал ему чай и сахар.</ta>
            <ta e="T21" id="Seg_634" s="T16">Человек глядит, [кусочки] сахара маленькие.</ta>
            <ta e="T26" id="Seg_635" s="T21">Тогда он берёт два, три.</ta>
            <ta e="T29" id="Seg_636" s="T26">А тот говорит:</ta>
            <ta e="T38" id="Seg_637" s="T29">«[Если бы] люди вас увидели, положили бы три. [?]</ta>
            <ta e="T47" id="Seg_638" s="T38">А поскольку они маленьктие, и три положат, и два положат».</ta>
            <ta e="T48" id="Seg_639" s="T47">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_640" s="T0">There lived one man.</ta>
            <ta e="T6" id="Seg_641" s="T3">He was very stingy.</ta>
            <ta e="T9" id="Seg_642" s="T6">Then [another] man came.</ta>
            <ta e="T16" id="Seg_643" s="T9">He prepared him tea and sugar.</ta>
            <ta e="T21" id="Seg_644" s="T16">The man looks, the [pieces of] sugar are small.</ta>
            <ta e="T26" id="Seg_645" s="T21">Then he takes two, three.</ta>
            <ta e="T29" id="Seg_646" s="T26">And [the other] says:</ta>
            <ta e="T38" id="Seg_647" s="T29">"[If] people saw you, they put three. [?]</ta>
            <ta e="T47" id="Seg_648" s="T38">But if they are small, they put three, they put two."</ta>
            <ta e="T48" id="Seg_649" s="T47">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_650" s="T0">Es lebt ein Mann.</ta>
            <ta e="T6" id="Seg_651" s="T3">Er ist sehr knauserig.</ta>
            <ta e="T9" id="Seg_652" s="T6">Dann kam ein [anderer] Mann.</ta>
            <ta e="T16" id="Seg_653" s="T9">Er bereitete ihm Tee und Zucker vor.</ta>
            <ta e="T21" id="Seg_654" s="T16">Der Mann schaut, die Zucker[stücke] sind klein.</ta>
            <ta e="T26" id="Seg_655" s="T21">Dann nimmt er zwei, drei.</ta>
            <ta e="T29" id="Seg_656" s="T26">Und [der Andere] sagt:</ta>
            <ta e="T38" id="Seg_657" s="T29">„[Wenn] Leute dich sahen, geben sie drei [dazu]. [?]</ta>
            <ta e="T47" id="Seg_658" s="T38">Aber wenn sie klein sind, geben sie drei, sie geben zwei [dazu].“</ta>
            <ta e="T48" id="Seg_659" s="T47">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T6" id="Seg_660" s="T3">[GVY:] Donner (38a) has mari; here pronounced as mori.</ta>
            <ta e="T38" id="Seg_661" s="T29">[KlT:] Possible reading, very hard to interpret.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
