<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDD85D52EA-9ED1-0F8C-1960-1E2D95A7AFB4">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_Bread_flk.wav" />
         <referenced-file url="PKZ_196X_Bread_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_Bread_flk\PKZ_196X_Bread_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">58</ud-information>
            <ud-information attribute-name="# HIAT:w">41</ud-information>
            <ud-information attribute-name="# e">41</ud-information>
            <ud-information attribute-name="# HIAT:u">8</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="0.968" type="appl" />
         <tli id="T2" time="1.936" type="appl" />
         <tli id="T3" time="2.905" type="appl" />
         <tli id="T4" time="3.873" type="appl" />
         <tli id="T5" time="4.578" type="appl" />
         <tli id="T6" time="5.109" type="appl" />
         <tli id="T7" time="5.641" type="appl" />
         <tli id="T8" time="6.172" type="appl" />
         <tli id="T9" time="6.704" type="appl" />
         <tli id="T10" time="7.236" type="appl" />
         <tli id="T11" time="7.767" type="appl" />
         <tli id="T12" time="8.299" type="appl" />
         <tli id="T13" time="8.838" type="appl" />
         <tli id="T14" time="9.275" type="appl" />
         <tli id="T15" time="9.712" type="appl" />
         <tli id="T16" time="10.15" type="appl" />
         <tli id="T17" time="10.587" type="appl" />
         <tli id="T18" time="11.406543977872069" />
         <tli id="T19" time="12.258" type="appl" />
         <tli id="T20" time="12.908" type="appl" />
         <tli id="T21" time="13.559" type="appl" />
         <tli id="T22" time="14.21" type="appl" />
         <tli id="T23" time="14.86" type="appl" />
         <tli id="T24" time="15.511" type="appl" />
         <tli id="T25" time="16.162" type="appl" />
         <tli id="T26" time="16.813" type="appl" />
         <tli id="T27" time="17.463" type="appl" />
         <tli id="T28" time="18.24647040761885" />
         <tli id="T29" time="19.425" type="appl" />
         <tli id="T30" time="20.048" type="appl" />
         <tli id="T31" time="20.671" type="appl" />
         <tli id="T32" time="21.295" type="appl" />
         <tli id="T33" time="21.918" type="appl" />
         <tli id="T34" time="22.91308688015564" />
         <tli id="T35" time="23.333" type="appl" />
         <tli id="T36" time="23.951" type="appl" />
         <tli id="T37" time="24.568" type="appl" />
         <tli id="T38" time="25.186" type="appl" />
         <tli id="T39" time="26.933043641498045" />
         <tli id="T40" time="27.796" type="appl" />
         <tli id="T41" time="28.395" type="appl" />
         <tli id="T42" time="28.993" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T42" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Onʼiʔ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">kuza</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">ipekziʔ</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">kudonzəbi</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">Dĭ</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_21" n="HIAT:ip">(</nts>
                  <ts e="T6" id="Seg_23" n="HIAT:w" s="T5">mănd-</ts>
                  <nts id="Seg_24" n="HIAT:ip">)</nts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_27" n="HIAT:w" s="T6">ipek</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_30" n="HIAT:w" s="T7">măndə:</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_33" n="HIAT:w" s="T8">Măn</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_36" n="HIAT:w" s="T9">tănan</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_38" n="HIAT:ip">(</nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">kunla-</ts>
                  <nts id="Seg_41" n="HIAT:ip">)</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">kundlaʔbəm</ts>
                  <nts id="Seg_45" n="HIAT:ip">.</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_48" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_50" n="HIAT:w" s="T12">A</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_53" n="HIAT:w" s="T13">dĭ</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">măndə:</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">Măn</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_62" n="HIAT:w" s="T16">tănan</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">kundlaʔbəm</ts>
                  <nts id="Seg_66" n="HIAT:ip">"</nts>
                  <nts id="Seg_67" n="HIAT:ip">.</nts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_70" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_72" n="HIAT:w" s="T18">Kudonzəbi</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_75" n="HIAT:w" s="T19">idə</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_78" n="HIAT:w" s="T20">ibi</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_81" n="HIAT:w" s="T21">da</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_84" n="HIAT:w" s="T22">ipek</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_87" n="HIAT:w" s="T23">baruʔluʔbi</ts>
                  <nts id="Seg_88" n="HIAT:ip">,</nts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">i</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_94" n="HIAT:w" s="T25">bostə</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_97" n="HIAT:w" s="T26">kambi</ts>
                  <nts id="Seg_98" n="HIAT:ip">,</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">kambi</ts>
                  <nts id="Seg_102" n="HIAT:ip">.</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_105" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_107" n="HIAT:w" s="T28">Tararluʔpi</ts>
                  <nts id="Seg_108" n="HIAT:ip">.</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_111" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_113" n="HIAT:w" s="T29">Dĭgəttə</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_116" n="HIAT:w" s="T30">parluʔpi</ts>
                  <nts id="Seg_117" n="HIAT:ip">,</nts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_120" n="HIAT:w" s="T31">ipek</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_123" n="HIAT:w" s="T32">ibi</ts>
                  <nts id="Seg_124" n="HIAT:ip">,</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_127" n="HIAT:w" s="T33">amnuʔpi</ts>
                  <nts id="Seg_128" n="HIAT:ip">.</nts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_131" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_133" n="HIAT:w" s="T34">I</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_136" n="HIAT:w" s="T35">dĭgəttə</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_139" n="HIAT:w" s="T36">šaldə</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_142" n="HIAT:w" s="T37">iʔgö</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_145" n="HIAT:w" s="T38">molaːmbi</ts>
                  <nts id="Seg_146" n="HIAT:ip">.</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_149" n="HIAT:u" s="T39">
                  <ts e="T41" id="Seg_151" n="HIAT:w" s="T39">Dĭgəttə</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_154" n="HIAT:w" s="T41">kabarləj</ts>
                  <nts id="Seg_155" n="HIAT:ip">!</nts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T42" id="Seg_157" n="sc" s="T0">
               <ts e="T1" id="Seg_159" n="e" s="T0">Onʼiʔ </ts>
               <ts e="T2" id="Seg_161" n="e" s="T1">kuza </ts>
               <ts e="T3" id="Seg_163" n="e" s="T2">ipekziʔ </ts>
               <ts e="T4" id="Seg_165" n="e" s="T3">kudonzəbi. </ts>
               <ts e="T5" id="Seg_167" n="e" s="T4">Dĭ </ts>
               <ts e="T6" id="Seg_169" n="e" s="T5">(mănd-) </ts>
               <ts e="T7" id="Seg_171" n="e" s="T6">ipek </ts>
               <ts e="T8" id="Seg_173" n="e" s="T7">măndə: </ts>
               <ts e="T9" id="Seg_175" n="e" s="T8">Măn </ts>
               <ts e="T10" id="Seg_177" n="e" s="T9">tănan </ts>
               <ts e="T11" id="Seg_179" n="e" s="T10">(kunla-) </ts>
               <ts e="T12" id="Seg_181" n="e" s="T11">kundlaʔbəm. </ts>
               <ts e="T13" id="Seg_183" n="e" s="T12">A </ts>
               <ts e="T14" id="Seg_185" n="e" s="T13">dĭ </ts>
               <ts e="T15" id="Seg_187" n="e" s="T14">măndə: </ts>
               <ts e="T16" id="Seg_189" n="e" s="T15">Măn </ts>
               <ts e="T17" id="Seg_191" n="e" s="T16">tănan </ts>
               <ts e="T18" id="Seg_193" n="e" s="T17">kundlaʔbəm". </ts>
               <ts e="T19" id="Seg_195" n="e" s="T18">Kudonzəbi </ts>
               <ts e="T20" id="Seg_197" n="e" s="T19">idə </ts>
               <ts e="T21" id="Seg_199" n="e" s="T20">ibi </ts>
               <ts e="T22" id="Seg_201" n="e" s="T21">da </ts>
               <ts e="T23" id="Seg_203" n="e" s="T22">ipek </ts>
               <ts e="T24" id="Seg_205" n="e" s="T23">baruʔluʔbi, </ts>
               <ts e="T25" id="Seg_207" n="e" s="T24">i </ts>
               <ts e="T26" id="Seg_209" n="e" s="T25">bostə </ts>
               <ts e="T27" id="Seg_211" n="e" s="T26">kambi, </ts>
               <ts e="T28" id="Seg_213" n="e" s="T27">kambi. </ts>
               <ts e="T29" id="Seg_215" n="e" s="T28">Tararluʔpi. </ts>
               <ts e="T30" id="Seg_217" n="e" s="T29">Dĭgəttə </ts>
               <ts e="T31" id="Seg_219" n="e" s="T30">parluʔpi, </ts>
               <ts e="T32" id="Seg_221" n="e" s="T31">ipek </ts>
               <ts e="T33" id="Seg_223" n="e" s="T32">ibi, </ts>
               <ts e="T34" id="Seg_225" n="e" s="T33">amnuʔpi. </ts>
               <ts e="T35" id="Seg_227" n="e" s="T34">I </ts>
               <ts e="T36" id="Seg_229" n="e" s="T35">dĭgəttə </ts>
               <ts e="T37" id="Seg_231" n="e" s="T36">šaldə </ts>
               <ts e="T38" id="Seg_233" n="e" s="T37">iʔgö </ts>
               <ts e="T39" id="Seg_235" n="e" s="T38">molaːmbi. </ts>
               <ts e="T41" id="Seg_237" n="e" s="T39">Dĭgəttə </ts>
               <ts e="T42" id="Seg_239" n="e" s="T41">kabarləj! </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_240" s="T0">PKZ_196X_Bread_flk.001 (001)</ta>
            <ta e="T12" id="Seg_241" s="T4">PKZ_196X_Bread_flk.002 (002)</ta>
            <ta e="T18" id="Seg_242" s="T12">PKZ_196X_Bread_flk.003 (003)</ta>
            <ta e="T28" id="Seg_243" s="T18">PKZ_196X_Bread_flk.004 (004)</ta>
            <ta e="T29" id="Seg_244" s="T28">PKZ_196X_Bread_flk.005 (005)</ta>
            <ta e="T34" id="Seg_245" s="T29">PKZ_196X_Bread_flk.006 (006)</ta>
            <ta e="T39" id="Seg_246" s="T34">PKZ_196X_Bread_flk.007 (007)</ta>
            <ta e="T42" id="Seg_247" s="T39">PKZ_196X_Bread_flk.008 (008)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_248" s="T0">Onʼiʔ kuza ipekziʔ kudonzəbi. </ta>
            <ta e="T12" id="Seg_249" s="T4">Dĭ (mănd-) ipek măndə:" Măn tănan (kunla-) kundlaʔbəm". </ta>
            <ta e="T18" id="Seg_250" s="T12">A dĭ măndə:" Măn tănan kundlaʔbəm". </ta>
            <ta e="T28" id="Seg_251" s="T18">Kudonzəbi idə ibi da ipek baruʔluʔbi, i bostə kambi, kambi. </ta>
            <ta e="T29" id="Seg_252" s="T28">Tararluʔpi. </ta>
            <ta e="T34" id="Seg_253" s="T29">Dĭgəttə parluʔpi, ipek ibi, amnuʔpi. </ta>
            <ta e="T39" id="Seg_254" s="T34">I dĭgəttə šaldə iʔgö molaːmbi. </ta>
            <ta e="T42" id="Seg_255" s="T39">Dĭgəttə ((PAUSE)) kabarləj! </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_256" s="T0">onʼiʔ</ta>
            <ta e="T2" id="Seg_257" s="T1">kuza</ta>
            <ta e="T3" id="Seg_258" s="T2">ipek-ziʔ</ta>
            <ta e="T4" id="Seg_259" s="T3">kudo-nzə-bi</ta>
            <ta e="T5" id="Seg_260" s="T4">dĭ</ta>
            <ta e="T7" id="Seg_261" s="T6">ipek</ta>
            <ta e="T8" id="Seg_262" s="T7">măn-də</ta>
            <ta e="T9" id="Seg_263" s="T8">măn</ta>
            <ta e="T10" id="Seg_264" s="T9">tănan</ta>
            <ta e="T12" id="Seg_265" s="T11">kun-d-laʔbə-m</ta>
            <ta e="T13" id="Seg_266" s="T12">a</ta>
            <ta e="T14" id="Seg_267" s="T13">dĭ</ta>
            <ta e="T15" id="Seg_268" s="T14">măn-də</ta>
            <ta e="T16" id="Seg_269" s="T15">măn</ta>
            <ta e="T17" id="Seg_270" s="T16">tănan</ta>
            <ta e="T18" id="Seg_271" s="T17">kun-d-laʔbə-m</ta>
            <ta e="T19" id="Seg_272" s="T18">kudo-nzə-bi</ta>
            <ta e="T20" id="Seg_273" s="T19">idə</ta>
            <ta e="T21" id="Seg_274" s="T20">i-bi</ta>
            <ta e="T22" id="Seg_275" s="T21">da</ta>
            <ta e="T23" id="Seg_276" s="T22">ipek</ta>
            <ta e="T24" id="Seg_277" s="T23">baruʔ-luʔ-bi</ta>
            <ta e="T25" id="Seg_278" s="T24">i</ta>
            <ta e="T26" id="Seg_279" s="T25">bos-tə</ta>
            <ta e="T27" id="Seg_280" s="T26">kam-bi</ta>
            <ta e="T28" id="Seg_281" s="T27">kam-bi</ta>
            <ta e="T29" id="Seg_282" s="T28">tarar-luʔ-pi</ta>
            <ta e="T30" id="Seg_283" s="T29">dĭgəttə</ta>
            <ta e="T31" id="Seg_284" s="T30">par-luʔ-pi</ta>
            <ta e="T32" id="Seg_285" s="T31">ipek</ta>
            <ta e="T33" id="Seg_286" s="T32">i-bi</ta>
            <ta e="T34" id="Seg_287" s="T33">am-nuʔ-pi</ta>
            <ta e="T35" id="Seg_288" s="T34">i</ta>
            <ta e="T36" id="Seg_289" s="T35">dĭgəttə</ta>
            <ta e="T37" id="Seg_290" s="T36">šal-də</ta>
            <ta e="T38" id="Seg_291" s="T37">iʔgö</ta>
            <ta e="T39" id="Seg_292" s="T38">mo-laːm-bi</ta>
            <ta e="T41" id="Seg_293" s="T39">dĭgəttə</ta>
            <ta e="T42" id="Seg_294" s="T41">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_295" s="T0">onʼiʔ</ta>
            <ta e="T2" id="Seg_296" s="T1">kuza</ta>
            <ta e="T3" id="Seg_297" s="T2">ipek-ziʔ</ta>
            <ta e="T4" id="Seg_298" s="T3">kudo-nzə-bi</ta>
            <ta e="T5" id="Seg_299" s="T4">dĭ</ta>
            <ta e="T7" id="Seg_300" s="T6">ipek</ta>
            <ta e="T8" id="Seg_301" s="T7">măn-ntə</ta>
            <ta e="T9" id="Seg_302" s="T8">măn</ta>
            <ta e="T10" id="Seg_303" s="T9">tănan</ta>
            <ta e="T12" id="Seg_304" s="T11">kun-ntə-laʔbə-m</ta>
            <ta e="T13" id="Seg_305" s="T12">a</ta>
            <ta e="T14" id="Seg_306" s="T13">dĭ</ta>
            <ta e="T15" id="Seg_307" s="T14">măn-ntə</ta>
            <ta e="T16" id="Seg_308" s="T15">măn</ta>
            <ta e="T17" id="Seg_309" s="T16">tănan</ta>
            <ta e="T18" id="Seg_310" s="T17">kun-ntə-laʔbə-m</ta>
            <ta e="T19" id="Seg_311" s="T18">kudo-nzə-bi</ta>
            <ta e="T20" id="Seg_312" s="T19">idə</ta>
            <ta e="T21" id="Seg_313" s="T20">i-bi</ta>
            <ta e="T22" id="Seg_314" s="T21">da</ta>
            <ta e="T23" id="Seg_315" s="T22">ipek</ta>
            <ta e="T24" id="Seg_316" s="T23">barəʔ-luʔbdə-bi</ta>
            <ta e="T25" id="Seg_317" s="T24">i</ta>
            <ta e="T26" id="Seg_318" s="T25">bos-də</ta>
            <ta e="T27" id="Seg_319" s="T26">kan-bi</ta>
            <ta e="T28" id="Seg_320" s="T27">kan-bi</ta>
            <ta e="T29" id="Seg_321" s="T28">tarar-luʔbdə-bi</ta>
            <ta e="T30" id="Seg_322" s="T29">dĭgəttə</ta>
            <ta e="T31" id="Seg_323" s="T30">par-luʔbdə-bi</ta>
            <ta e="T32" id="Seg_324" s="T31">ipek</ta>
            <ta e="T33" id="Seg_325" s="T32">i-bi</ta>
            <ta e="T34" id="Seg_326" s="T33">am-luʔbdə-bi</ta>
            <ta e="T35" id="Seg_327" s="T34">i</ta>
            <ta e="T36" id="Seg_328" s="T35">dĭgəttə</ta>
            <ta e="T37" id="Seg_329" s="T36">šal-l</ta>
            <ta e="T38" id="Seg_330" s="T37">iʔgö</ta>
            <ta e="T39" id="Seg_331" s="T38">mo-laːm-bi</ta>
            <ta e="T41" id="Seg_332" s="T39">dĭgəttə</ta>
            <ta e="T42" id="Seg_333" s="T41">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_334" s="T0">one.[NOM.SG]</ta>
            <ta e="T2" id="Seg_335" s="T1">man.[NOM.SG]</ta>
            <ta e="T3" id="Seg_336" s="T2">bread-INS</ta>
            <ta e="T4" id="Seg_337" s="T3">scold-DES-PST.[3SG]</ta>
            <ta e="T5" id="Seg_338" s="T4">this.[NOM.SG]</ta>
            <ta e="T7" id="Seg_339" s="T6">bread.[NOM.SG]</ta>
            <ta e="T8" id="Seg_340" s="T7">say-IPFVZ.[3SG]</ta>
            <ta e="T9" id="Seg_341" s="T8">I.NOM</ta>
            <ta e="T10" id="Seg_342" s="T9">you.ACC</ta>
            <ta e="T12" id="Seg_343" s="T11">bring-IPFVZ-DUR-1SG</ta>
            <ta e="T13" id="Seg_344" s="T12">and</ta>
            <ta e="T14" id="Seg_345" s="T13">this.[NOM.SG]</ta>
            <ta e="T15" id="Seg_346" s="T14">say-IPFVZ.[3SG]</ta>
            <ta e="T16" id="Seg_347" s="T15">I.NOM</ta>
            <ta e="T17" id="Seg_348" s="T16">you.ACC</ta>
            <ta e="T18" id="Seg_349" s="T17">bring-IPFVZ-DUR-1SG</ta>
            <ta e="T19" id="Seg_350" s="T18">scold-DES-PST.[3SG]</ta>
            <ta e="T20" id="Seg_351" s="T19">that.[NOM.SG]</ta>
            <ta e="T21" id="Seg_352" s="T20">take-PST.[3SG]</ta>
            <ta e="T22" id="Seg_353" s="T21">and</ta>
            <ta e="T23" id="Seg_354" s="T22">bread.[NOM.SG]</ta>
            <ta e="T24" id="Seg_355" s="T23">throw.away-MOM-PST.[3SG]</ta>
            <ta e="T25" id="Seg_356" s="T24">and</ta>
            <ta e="T26" id="Seg_357" s="T25">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T27" id="Seg_358" s="T26">go-PST.[3SG]</ta>
            <ta e="T28" id="Seg_359" s="T27">go-PST.[3SG]</ta>
            <ta e="T29" id="Seg_360" s="T28">get.tired-MOM-PST.[3SG]</ta>
            <ta e="T30" id="Seg_361" s="T29">then</ta>
            <ta e="T31" id="Seg_362" s="T30">return-MOM-PST.[3SG]</ta>
            <ta e="T32" id="Seg_363" s="T31">bread.[NOM.SG]</ta>
            <ta e="T33" id="Seg_364" s="T32">take-PST.[3SG]</ta>
            <ta e="T34" id="Seg_365" s="T33">eat-MOM-PST.[3SG]</ta>
            <ta e="T35" id="Seg_366" s="T34">and</ta>
            <ta e="T36" id="Seg_367" s="T35">then</ta>
            <ta e="T37" id="Seg_368" s="T36">strength-NOM/GEN/ACC.2SG</ta>
            <ta e="T38" id="Seg_369" s="T37">many</ta>
            <ta e="T39" id="Seg_370" s="T38">become-RES-PST.[3SG]</ta>
            <ta e="T41" id="Seg_371" s="T39">then</ta>
            <ta e="T42" id="Seg_372" s="T41">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_373" s="T0">один.[NOM.SG]</ta>
            <ta e="T2" id="Seg_374" s="T1">мужчина.[NOM.SG]</ta>
            <ta e="T3" id="Seg_375" s="T2">хлеб-INS</ta>
            <ta e="T4" id="Seg_376" s="T3">ругать-DES-PST.[3SG]</ta>
            <ta e="T5" id="Seg_377" s="T4">этот.[NOM.SG]</ta>
            <ta e="T7" id="Seg_378" s="T6">хлеб.[NOM.SG]</ta>
            <ta e="T8" id="Seg_379" s="T7">сказать-IPFVZ.[3SG]</ta>
            <ta e="T9" id="Seg_380" s="T8">я.NOM</ta>
            <ta e="T10" id="Seg_381" s="T9">ты.ACC</ta>
            <ta e="T12" id="Seg_382" s="T11">нести-IPFVZ-DUR-1SG</ta>
            <ta e="T13" id="Seg_383" s="T12">а</ta>
            <ta e="T14" id="Seg_384" s="T13">этот.[NOM.SG]</ta>
            <ta e="T15" id="Seg_385" s="T14">сказать-IPFVZ.[3SG]</ta>
            <ta e="T16" id="Seg_386" s="T15">я.NOM</ta>
            <ta e="T17" id="Seg_387" s="T16">ты.ACC</ta>
            <ta e="T18" id="Seg_388" s="T17">нести-IPFVZ-DUR-1SG</ta>
            <ta e="T19" id="Seg_389" s="T18">ругать-DES-PST.[3SG]</ta>
            <ta e="T20" id="Seg_390" s="T19">тот.[NOM.SG]</ta>
            <ta e="T21" id="Seg_391" s="T20">взять-PST.[3SG]</ta>
            <ta e="T22" id="Seg_392" s="T21">и</ta>
            <ta e="T23" id="Seg_393" s="T22">хлеб.[NOM.SG]</ta>
            <ta e="T24" id="Seg_394" s="T23">выбросить-MOM-PST.[3SG]</ta>
            <ta e="T25" id="Seg_395" s="T24">и</ta>
            <ta e="T26" id="Seg_396" s="T25">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T27" id="Seg_397" s="T26">пойти-PST.[3SG]</ta>
            <ta e="T28" id="Seg_398" s="T27">пойти-PST.[3SG]</ta>
            <ta e="T29" id="Seg_399" s="T28">устать-MOM-PST.[3SG]</ta>
            <ta e="T30" id="Seg_400" s="T29">тогда</ta>
            <ta e="T31" id="Seg_401" s="T30">вернуться-MOM-PST.[3SG]</ta>
            <ta e="T32" id="Seg_402" s="T31">хлеб.[NOM.SG]</ta>
            <ta e="T33" id="Seg_403" s="T32">взять-PST.[3SG]</ta>
            <ta e="T34" id="Seg_404" s="T33">съесть-MOM-PST.[3SG]</ta>
            <ta e="T35" id="Seg_405" s="T34">и</ta>
            <ta e="T36" id="Seg_406" s="T35">тогда</ta>
            <ta e="T37" id="Seg_407" s="T36">сила-NOM/GEN/ACC.2SG</ta>
            <ta e="T38" id="Seg_408" s="T37">много</ta>
            <ta e="T39" id="Seg_409" s="T38">стать-RES-PST.[3SG]</ta>
            <ta e="T41" id="Seg_410" s="T39">тогда</ta>
            <ta e="T42" id="Seg_411" s="T41">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_412" s="T0">num-n:case</ta>
            <ta e="T2" id="Seg_413" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_414" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_415" s="T3">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_416" s="T4">dempro-n:case</ta>
            <ta e="T7" id="Seg_417" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_418" s="T7">v-v&gt;v-v:pn</ta>
            <ta e="T9" id="Seg_419" s="T8">pers</ta>
            <ta e="T10" id="Seg_420" s="T9">pers</ta>
            <ta e="T12" id="Seg_421" s="T11">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T13" id="Seg_422" s="T12">conj</ta>
            <ta e="T14" id="Seg_423" s="T13">dempro-n:case</ta>
            <ta e="T15" id="Seg_424" s="T14">v-v&gt;v-v:pn</ta>
            <ta e="T16" id="Seg_425" s="T15">pers</ta>
            <ta e="T17" id="Seg_426" s="T16">pers</ta>
            <ta e="T18" id="Seg_427" s="T17">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T19" id="Seg_428" s="T18">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T20" id="Seg_429" s="T19">dempro-n:case</ta>
            <ta e="T21" id="Seg_430" s="T20">v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_431" s="T21">conj</ta>
            <ta e="T23" id="Seg_432" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_433" s="T23">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_434" s="T24">conj</ta>
            <ta e="T26" id="Seg_435" s="T25">refl-n:case.poss</ta>
            <ta e="T27" id="Seg_436" s="T26">v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_437" s="T27">v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_438" s="T28">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_439" s="T29">adv</ta>
            <ta e="T31" id="Seg_440" s="T30">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_441" s="T31">n-n:case</ta>
            <ta e="T33" id="Seg_442" s="T32">v-v:tense-v:pn</ta>
            <ta e="T34" id="Seg_443" s="T33">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_444" s="T34">conj</ta>
            <ta e="T36" id="Seg_445" s="T35">adv</ta>
            <ta e="T37" id="Seg_446" s="T36">n-n:case.poss</ta>
            <ta e="T38" id="Seg_447" s="T37">quant</ta>
            <ta e="T39" id="Seg_448" s="T38">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_449" s="T39">adv</ta>
            <ta e="T42" id="Seg_450" s="T41">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_451" s="T0">num</ta>
            <ta e="T2" id="Seg_452" s="T1">n</ta>
            <ta e="T3" id="Seg_453" s="T2">n</ta>
            <ta e="T4" id="Seg_454" s="T3">v</ta>
            <ta e="T5" id="Seg_455" s="T4">dempro</ta>
            <ta e="T7" id="Seg_456" s="T6">n</ta>
            <ta e="T8" id="Seg_457" s="T7">v</ta>
            <ta e="T9" id="Seg_458" s="T8">pers</ta>
            <ta e="T10" id="Seg_459" s="T9">pers</ta>
            <ta e="T12" id="Seg_460" s="T11">v</ta>
            <ta e="T13" id="Seg_461" s="T12">conj</ta>
            <ta e="T14" id="Seg_462" s="T13">dempro</ta>
            <ta e="T15" id="Seg_463" s="T14">v</ta>
            <ta e="T16" id="Seg_464" s="T15">pers</ta>
            <ta e="T17" id="Seg_465" s="T16">pers</ta>
            <ta e="T18" id="Seg_466" s="T17">v</ta>
            <ta e="T19" id="Seg_467" s="T18">v</ta>
            <ta e="T20" id="Seg_468" s="T19">dempro</ta>
            <ta e="T21" id="Seg_469" s="T20">v</ta>
            <ta e="T22" id="Seg_470" s="T21">conj</ta>
            <ta e="T23" id="Seg_471" s="T22">n</ta>
            <ta e="T24" id="Seg_472" s="T23">v</ta>
            <ta e="T25" id="Seg_473" s="T24">conj</ta>
            <ta e="T26" id="Seg_474" s="T25">refl</ta>
            <ta e="T27" id="Seg_475" s="T26">v</ta>
            <ta e="T28" id="Seg_476" s="T27">v</ta>
            <ta e="T29" id="Seg_477" s="T28">v</ta>
            <ta e="T30" id="Seg_478" s="T29">adv</ta>
            <ta e="T31" id="Seg_479" s="T30">v</ta>
            <ta e="T32" id="Seg_480" s="T31">n</ta>
            <ta e="T33" id="Seg_481" s="T32">v</ta>
            <ta e="T34" id="Seg_482" s="T33">v</ta>
            <ta e="T35" id="Seg_483" s="T34">conj</ta>
            <ta e="T36" id="Seg_484" s="T35">adv</ta>
            <ta e="T37" id="Seg_485" s="T36">n</ta>
            <ta e="T38" id="Seg_486" s="T37">quant</ta>
            <ta e="T39" id="Seg_487" s="T38">v</ta>
            <ta e="T41" id="Seg_488" s="T39">adv</ta>
            <ta e="T42" id="Seg_489" s="T41">ptcl</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_490" s="T1">np.h:S</ta>
            <ta e="T4" id="Seg_491" s="T3">v:pred</ta>
            <ta e="T7" id="Seg_492" s="T6">np:S</ta>
            <ta e="T8" id="Seg_493" s="T7">v:pred</ta>
            <ta e="T9" id="Seg_494" s="T8">pro.h:S</ta>
            <ta e="T10" id="Seg_495" s="T9">pro.h:O</ta>
            <ta e="T12" id="Seg_496" s="T11">v:pred</ta>
            <ta e="T14" id="Seg_497" s="T13">pro.h:S</ta>
            <ta e="T15" id="Seg_498" s="T14">v:pred</ta>
            <ta e="T16" id="Seg_499" s="T15">pro.h:S</ta>
            <ta e="T17" id="Seg_500" s="T16">np:O</ta>
            <ta e="T18" id="Seg_501" s="T17">v:pred</ta>
            <ta e="T19" id="Seg_502" s="T18">0.3.h:S v:pred</ta>
            <ta e="T20" id="Seg_503" s="T19">pro.h:S</ta>
            <ta e="T21" id="Seg_504" s="T20">v:pred</ta>
            <ta e="T23" id="Seg_505" s="T22">np:O</ta>
            <ta e="T24" id="Seg_506" s="T23">0.3.h:S v:pred</ta>
            <ta e="T26" id="Seg_507" s="T25">pro.h:S</ta>
            <ta e="T27" id="Seg_508" s="T26">v:pred</ta>
            <ta e="T28" id="Seg_509" s="T27">0.3.h:S v:pred</ta>
            <ta e="T29" id="Seg_510" s="T28">0.3.h:S v:pred</ta>
            <ta e="T31" id="Seg_511" s="T30">0.3.h:S v:pred</ta>
            <ta e="T32" id="Seg_512" s="T31">np:O</ta>
            <ta e="T33" id="Seg_513" s="T32">0.3.h:S v:pred</ta>
            <ta e="T34" id="Seg_514" s="T33">0.3.h:S v:pred</ta>
            <ta e="T37" id="Seg_515" s="T36">np:S</ta>
            <ta e="T39" id="Seg_516" s="T38">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_517" s="T1">np.h:A</ta>
            <ta e="T7" id="Seg_518" s="T6">np:A</ta>
            <ta e="T9" id="Seg_519" s="T8">pro.h:A</ta>
            <ta e="T10" id="Seg_520" s="T9">pro.h:Th</ta>
            <ta e="T14" id="Seg_521" s="T13">pro.h:A</ta>
            <ta e="T16" id="Seg_522" s="T15">pro.h:A</ta>
            <ta e="T17" id="Seg_523" s="T16">pro.h:Th</ta>
            <ta e="T19" id="Seg_524" s="T18">0.3.h:A</ta>
            <ta e="T20" id="Seg_525" s="T19">pro.h:A</ta>
            <ta e="T23" id="Seg_526" s="T22">np:Th</ta>
            <ta e="T24" id="Seg_527" s="T23">0.3.h:A</ta>
            <ta e="T26" id="Seg_528" s="T25">pro.h:A</ta>
            <ta e="T28" id="Seg_529" s="T27">0.3.h:A</ta>
            <ta e="T29" id="Seg_530" s="T28">0.3.h:E</ta>
            <ta e="T31" id="Seg_531" s="T30">0.3.h:A</ta>
            <ta e="T32" id="Seg_532" s="T31">np:Th</ta>
            <ta e="T34" id="Seg_533" s="T33">0.3.h:A</ta>
            <ta e="T37" id="Seg_534" s="T36">np:Th 0.1.h:Poss</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T13" id="Seg_535" s="T12">RUS:gram</ta>
            <ta e="T22" id="Seg_536" s="T21">RUS:gram</ta>
            <ta e="T25" id="Seg_537" s="T24">RUS:gram</ta>
            <ta e="T35" id="Seg_538" s="T34">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_539" s="T0">Один человек поссорился с хлебом.</ta>
            <ta e="T12" id="Seg_540" s="T4">Хлеб говорит: «Я тебя несу».</ta>
            <ta e="T18" id="Seg_541" s="T12">А он говорит: «Я тебя несу!»</ta>
            <ta e="T28" id="Seg_542" s="T18">Он выругался, взял хлеб и выбросил, а сам пошёл, пошёл.</ta>
            <ta e="T29" id="Seg_543" s="T28">Устал.</ta>
            <ta e="T34" id="Seg_544" s="T29">Потом вернулся, взял хлеб, съел его.</ta>
            <ta e="T39" id="Seg_545" s="T34">И тогда силы [у него] много стало.</ta>
            <ta e="T42" id="Seg_546" s="T39">Потом… хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_547" s="T0">One man quarrelled with bread.</ta>
            <ta e="T12" id="Seg_548" s="T4">The bread says: "I am carrying you."</ta>
            <ta e="T18" id="Seg_549" s="T12">But he says: "I am carrying you!"</ta>
            <ta e="T28" id="Seg_550" s="T18">He sweared, took the bread and threw it away, and [himself] went on and on.</ta>
            <ta e="T29" id="Seg_551" s="T28">He got tired.</ta>
            <ta e="T34" id="Seg_552" s="T29">Then he returned, took the bread, ate it.</ta>
            <ta e="T39" id="Seg_553" s="T34">And then he got very strong.</ta>
            <ta e="T42" id="Seg_554" s="T39">Then… enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_555" s="T0">Ein Mann stritt sich mit dem Brot.</ta>
            <ta e="T12" id="Seg_556" s="T4">Das Brot sagt: „Ich trage dich.“</ta>
            <ta e="T18" id="Seg_557" s="T12">Aber er sagt: „Ich trage dich!“</ta>
            <ta e="T28" id="Seg_558" s="T18">Er fluchte, er nahm das Brot und warf es weg, ging selbst weiter, ging.</ta>
            <ta e="T29" id="Seg_559" s="T28">Er wurde müde.</ta>
            <ta e="T34" id="Seg_560" s="T29">Dann kehrte er zurück, nahm das Brot, aß es.</ta>
            <ta e="T39" id="Seg_561" s="T34">Und dann wurde er sehr kräftig.</ta>
            <ta e="T42" id="Seg_562" s="T39">Dann … genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T4" id="Seg_563" s="T0">[GVY:] This texts reminds a verse, published as a part of Anton Belevich's 1974 book: http://www.molodguard.ru/village04.htm.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
