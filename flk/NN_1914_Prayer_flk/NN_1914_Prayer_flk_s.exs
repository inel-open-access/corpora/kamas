<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID3472F1F8-04B1-0EC8-C8A0-7BC72EB59253">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\NN_1914_Prayer_flk\NN_1914_Prayer_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">27</ud-information>
            <ud-information attribute-name="# HIAT:w">20</ud-information>
            <ud-information attribute-name="# e">19</ud-information>
            <ud-information attribute-name="# HIAT:u">2</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="NN">
            <abbreviation>NN</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="NN"
                      type="t">
         <timeline-fork end="T5" start="T4">
            <tli id="T4.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T19" id="Seg_0" n="sc" s="T0">
               <ts e="T9" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">ijazaŋbə</ts>
                  <nts id="Seg_5" n="HIAT:ip">,</nts>
                  <nts id="Seg_6" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_8" n="HIAT:w" s="T1">abazaŋbə</ts>
                  <nts id="Seg_9" n="HIAT:ip">,</nts>
                  <nts id="Seg_10" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_12" n="HIAT:w" s="T2">tʼün</ts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_15" n="HIAT:w" s="T3">eiet</ts>
                  <nts id="Seg_16" n="HIAT:ip">,</nts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4.tx.1" id="Seg_19" n="HIAT:w" s="T4">tʼü</ts>
                  <nts id="Seg_20" n="HIAT:ip">_</nts>
                  <ts e="T5" id="Seg_22" n="HIAT:w" s="T4.tx.1">bün</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_25" n="HIAT:w" s="T5">eiet</ts>
                  <nts id="Seg_26" n="HIAT:ip">,</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_29" n="HIAT:w" s="T6">sanəlaʔ</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_32" n="HIAT:w" s="T7">ige</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_35" n="HIAT:w" s="T8">bojargut</ts>
                  <nts id="Seg_36" n="HIAT:ip">!</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_39" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_41" n="HIAT:w" s="T9">Măna</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_44" n="HIAT:w" s="T10">tʼuksu</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_47" n="HIAT:w" s="T11">sĭrerleʔ</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_50" n="HIAT:w" s="T12">amnəgubə</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_53" n="HIAT:w" s="T13">söːn</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">kurabə</ts>
                  <nts id="Seg_57" n="HIAT:ip">,</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_60" n="HIAT:w" s="T15">bulaːn</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_63" n="HIAT:w" s="T16">kurabə</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_66" n="HIAT:w" s="T17">šĭʔbdən</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_69" n="HIAT:w" s="T18">kurabə</ts>
                  <nts id="Seg_70" n="HIAT:ip">.</nts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T19" id="Seg_72" n="sc" s="T0">
               <ts e="T1" id="Seg_74" n="e" s="T0">ijazaŋbə, </ts>
               <ts e="T2" id="Seg_76" n="e" s="T1">abazaŋbə, </ts>
               <ts e="T3" id="Seg_78" n="e" s="T2">tʼün </ts>
               <ts e="T4" id="Seg_80" n="e" s="T3">eiet, </ts>
               <ts e="T5" id="Seg_82" n="e" s="T4">tʼü_bün </ts>
               <ts e="T6" id="Seg_84" n="e" s="T5">eiet, </ts>
               <ts e="T7" id="Seg_86" n="e" s="T6">sanəlaʔ </ts>
               <ts e="T8" id="Seg_88" n="e" s="T7">ige </ts>
               <ts e="T9" id="Seg_90" n="e" s="T8">bojargut! </ts>
               <ts e="T10" id="Seg_92" n="e" s="T9">Măna </ts>
               <ts e="T11" id="Seg_94" n="e" s="T10">tʼuksu </ts>
               <ts e="T12" id="Seg_96" n="e" s="T11">sĭrerleʔ </ts>
               <ts e="T13" id="Seg_98" n="e" s="T12">amnəgubə </ts>
               <ts e="T14" id="Seg_100" n="e" s="T13">söːn </ts>
               <ts e="T15" id="Seg_102" n="e" s="T14">kurabə, </ts>
               <ts e="T16" id="Seg_104" n="e" s="T15">bulaːn </ts>
               <ts e="T17" id="Seg_106" n="e" s="T16">kurabə </ts>
               <ts e="T18" id="Seg_108" n="e" s="T17">šĭʔbdən </ts>
               <ts e="T19" id="Seg_110" n="e" s="T18">kurabə. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T9" id="Seg_111" s="T0">NN_1914_Prayer_flk.001 (001.001)</ta>
            <ta e="T19" id="Seg_112" s="T9">NN_1914_Prayer_flk.002 (001.002)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T9" id="Seg_113" s="T0">ijazaŋbə, abazaŋbə, tʼün eiet, tʼü_bün eiet, sanəlaʔ ige bojargut! </ta>
            <ta e="T19" id="Seg_114" s="T9">Măna tʼuksu sĭrerleʔ amnəgubə söːn kurabə, bulaːn kurabə šĭʔbdən kurabə. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_115" s="T0">ija-zaŋ-bə</ta>
            <ta e="T2" id="Seg_116" s="T1">aba-zaŋ-bə</ta>
            <ta e="T3" id="Seg_117" s="T2">tʼü-n</ta>
            <ta e="T4" id="Seg_118" s="T3">ei-et</ta>
            <ta e="T5" id="Seg_119" s="T4">tʼü-bü-n</ta>
            <ta e="T6" id="Seg_120" s="T5">ei-et</ta>
            <ta e="T7" id="Seg_121" s="T6">sanə-laʔ</ta>
            <ta e="T8" id="Seg_122" s="T7">i-ge</ta>
            <ta e="T9" id="Seg_123" s="T8">bojar-gut</ta>
            <ta e="T10" id="Seg_124" s="T9">măna</ta>
            <ta e="T11" id="Seg_125" s="T10">tʼuksu</ta>
            <ta e="T12" id="Seg_126" s="T11">sĭrer-leʔ</ta>
            <ta e="T13" id="Seg_127" s="T12">amnə-gu-bə</ta>
            <ta e="T14" id="Seg_128" s="T13">söːn</ta>
            <ta e="T15" id="Seg_129" s="T14">kura-bə</ta>
            <ta e="T16" id="Seg_130" s="T15">bulaːn</ta>
            <ta e="T17" id="Seg_131" s="T16">kura-bə</ta>
            <ta e="T18" id="Seg_132" s="T17">šĭʔbdə-n</ta>
            <ta e="T19" id="Seg_133" s="T18">kura-bə</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_134" s="T0">ija-zAŋ-m</ta>
            <ta e="T2" id="Seg_135" s="T1">aba-zAŋ-m</ta>
            <ta e="T3" id="Seg_136" s="T2">tʼü-n</ta>
            <ta e="T4" id="Seg_137" s="T3">eje-t</ta>
            <ta e="T5" id="Seg_138" s="T4">tʼü-bü-n</ta>
            <ta e="T6" id="Seg_139" s="T5">eje-t</ta>
            <ta e="T7" id="Seg_140" s="T6">sanə-lAʔ</ta>
            <ta e="T8" id="Seg_141" s="T7">e-KAʔ</ta>
            <ta e="T9" id="Seg_142" s="T8">bojar-Kut</ta>
            <ta e="T10" id="Seg_143" s="T9">măna</ta>
            <ta e="T11" id="Seg_144" s="T10">tʼuksu</ta>
            <ta e="T12" id="Seg_145" s="T11">sürer-lAʔ</ta>
            <ta e="T13" id="Seg_146" s="T12">amnə-KV-bə</ta>
            <ta e="T14" id="Seg_147" s="T13">sĭgən</ta>
            <ta e="T15" id="Seg_148" s="T14">kura-bə</ta>
            <ta e="T16" id="Seg_149" s="T15">bulan</ta>
            <ta e="T17" id="Seg_150" s="T16">kura-bə</ta>
            <ta e="T18" id="Seg_151" s="T17">šĭʔbdə-n</ta>
            <ta e="T19" id="Seg_152" s="T18">kura-bə</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_153" s="T0">mother-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T2" id="Seg_154" s="T1">father-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T3" id="Seg_155" s="T2">land-GEN</ta>
            <ta e="T4" id="Seg_156" s="T3">master-NOM/GEN.3SG</ta>
            <ta e="T5" id="Seg_157" s="T4">land-water-GEN</ta>
            <ta e="T6" id="Seg_158" s="T5">master-NOM/GEN.3SG</ta>
            <ta e="T7" id="Seg_159" s="T6">pine.nut-NOM/GEN/ACC.2PL</ta>
            <ta e="T8" id="Seg_160" s="T7">NEG.AUX-IMP.2PL</ta>
            <ta e="T9" id="Seg_161" s="T8">despise-IMP.2PL.O</ta>
            <ta e="T10" id="Seg_162" s="T9">I.LAT</ta>
            <ta e="T11" id="Seg_163" s="T10">towards</ta>
            <ta e="T12" id="Seg_164" s="T11">drive-CVB</ta>
            <ta e="T13" id="Seg_165" s="T12">sit-IMP-IMP.3SG.O</ta>
            <ta e="T14" id="Seg_166" s="T13">deer.[NOM.SG]</ta>
            <ta e="T15" id="Seg_167" s="T14">male.animal-ACC.3SG</ta>
            <ta e="T16" id="Seg_168" s="T15">moose.[NOM.SG]</ta>
            <ta e="T17" id="Seg_169" s="T16">male.animal-ACC.3SG</ta>
            <ta e="T18" id="Seg_170" s="T17">bear-GEN</ta>
            <ta e="T19" id="Seg_171" s="T18">male.animal-ACC.3SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_172" s="T0">мать-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T2" id="Seg_173" s="T1">отец-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T3" id="Seg_174" s="T2">земля-GEN</ta>
            <ta e="T4" id="Seg_175" s="T3">хозяин-NOM/GEN.3SG</ta>
            <ta e="T5" id="Seg_176" s="T4">земля-вода-GEN</ta>
            <ta e="T6" id="Seg_177" s="T5">хозяин-NOM/GEN.3SG</ta>
            <ta e="T7" id="Seg_178" s="T6">кедровый.орех-NOM/GEN/ACC.2PL</ta>
            <ta e="T8" id="Seg_179" s="T7">NEG.AUX-IMP.2PL</ta>
            <ta e="T9" id="Seg_180" s="T8">жалеть-IMP.2PL.O</ta>
            <ta e="T10" id="Seg_181" s="T9">я.LAT</ta>
            <ta e="T11" id="Seg_182" s="T10">вперед</ta>
            <ta e="T12" id="Seg_183" s="T11">гнать-CVB</ta>
            <ta e="T13" id="Seg_184" s="T12">сидеть-IMP-IMP.3SG.O</ta>
            <ta e="T14" id="Seg_185" s="T13">олень.[NOM.SG]</ta>
            <ta e="T15" id="Seg_186" s="T14">самец-ACC.3SG</ta>
            <ta e="T16" id="Seg_187" s="T15">лось.[NOM.SG]</ta>
            <ta e="T17" id="Seg_188" s="T16">самец-ACC.3SG</ta>
            <ta e="T18" id="Seg_189" s="T17">медведь-GEN</ta>
            <ta e="T19" id="Seg_190" s="T18">самец-ACC.3SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_191" s="T0">n-n:num-n:case.poss</ta>
            <ta e="T2" id="Seg_192" s="T1">n-n:num-n:case.poss</ta>
            <ta e="T3" id="Seg_193" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_194" s="T3">n-n:case.poss</ta>
            <ta e="T5" id="Seg_195" s="T4">n-n-n:case</ta>
            <ta e="T6" id="Seg_196" s="T5">n-n:case.poss</ta>
            <ta e="T7" id="Seg_197" s="T6">n-n:case.poss</ta>
            <ta e="T8" id="Seg_198" s="T7">aux-v:mood.pn</ta>
            <ta e="T9" id="Seg_199" s="T8">v-v:mood.pn</ta>
            <ta e="T10" id="Seg_200" s="T9">pers</ta>
            <ta e="T11" id="Seg_201" s="T10">adv</ta>
            <ta e="T12" id="Seg_202" s="T11">v-v:n.fin</ta>
            <ta e="T13" id="Seg_203" s="T12">v-v:mood-v:pn</ta>
            <ta e="T14" id="Seg_204" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_205" s="T14">n-n:case.poss</ta>
            <ta e="T16" id="Seg_206" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_207" s="T16">n-n:case.poss</ta>
            <ta e="T18" id="Seg_208" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_209" s="T18">n-n:case.poss</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_210" s="T0">n</ta>
            <ta e="T2" id="Seg_211" s="T1">n</ta>
            <ta e="T3" id="Seg_212" s="T2">n</ta>
            <ta e="T4" id="Seg_213" s="T3">n</ta>
            <ta e="T5" id="Seg_214" s="T4">n</ta>
            <ta e="T6" id="Seg_215" s="T5">n</ta>
            <ta e="T7" id="Seg_216" s="T6">n</ta>
            <ta e="T8" id="Seg_217" s="T7">aux</ta>
            <ta e="T9" id="Seg_218" s="T8">v</ta>
            <ta e="T10" id="Seg_219" s="T9">pers</ta>
            <ta e="T11" id="Seg_220" s="T10">adv</ta>
            <ta e="T12" id="Seg_221" s="T11">v</ta>
            <ta e="T13" id="Seg_222" s="T12">v</ta>
            <ta e="T14" id="Seg_223" s="T13">n</ta>
            <ta e="T15" id="Seg_224" s="T14">n</ta>
            <ta e="T16" id="Seg_225" s="T15">n</ta>
            <ta e="T17" id="Seg_226" s="T16">n</ta>
            <ta e="T18" id="Seg_227" s="T17">n</ta>
            <ta e="T19" id="Seg_228" s="T18">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_229" s="T0">0.1.h:Poss</ta>
            <ta e="T2" id="Seg_230" s="T1">0.1.h:Poss</ta>
            <ta e="T3" id="Seg_231" s="T2">np:Poss</ta>
            <ta e="T5" id="Seg_232" s="T4">np:Poss</ta>
            <ta e="T7" id="Seg_233" s="T6">np:Th 0.2.h:Poss</ta>
            <ta e="T11" id="Seg_234" s="T9">pp:G</ta>
            <ta e="T13" id="Seg_235" s="T12">0.3.h:A</ta>
            <ta e="T14" id="Seg_236" s="T13">np:Poss</ta>
            <ta e="T15" id="Seg_237" s="T14">np:Th</ta>
            <ta e="T16" id="Seg_238" s="T15">np:Poss</ta>
            <ta e="T17" id="Seg_239" s="T16">np:Th</ta>
            <ta e="T18" id="Seg_240" s="T17">np:Poss</ta>
            <ta e="T19" id="Seg_241" s="T18">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T7" id="Seg_242" s="T6">np:O</ta>
            <ta e="T8" id="Seg_243" s="T7">v:pred 0.3.h:S</ta>
            <ta e="T13" id="Seg_244" s="T12">v:pred 0.3.h:S</ta>
            <ta e="T15" id="Seg_245" s="T14">np:O</ta>
            <ta e="T17" id="Seg_246" s="T16">np:O</ta>
            <ta e="T19" id="Seg_247" s="T18">np:O</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T1" id="Seg_248" s="T0">accs-gen </ta>
            <ta e="T2" id="Seg_249" s="T1">accs-gen </ta>
            <ta e="T4" id="Seg_250" s="T3">accs-gen </ta>
            <ta e="T6" id="Seg_251" s="T5">accs-gen </ta>
            <ta e="T7" id="Seg_252" s="T6">new </ta>
            <ta e="T10" id="Seg_253" s="T9">new </ta>
            <ta e="T13" id="Seg_254" s="T12">0.accs-gen</ta>
            <ta e="T15" id="Seg_255" s="T13">new</ta>
            <ta e="T17" id="Seg_256" s="T15">new</ta>
            <ta e="T19" id="Seg_257" s="T17">new</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="fr" tierref="fr">
            <ta e="T9" id="Seg_258" s="T0">Матери мои, отцы мои, владыка земли, владыка земли и воды [=мира], не побрезгуйте [вашими] кедровыми орехами!</ta>
            <ta e="T19" id="Seg_259" s="T9">Да посылает [он] мне [всегда] оленя, лося и медведя!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T9" id="Seg_260" s="T0">My mothers, my fathers, master of the earth, master of the land and water [=of the world], don't despise your cedar nuts!</ta>
            <ta e="T19" id="Seg_261" s="T9">He shall [always] drive male deer, male moose and male bear to me!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T9" id="Seg_262" s="T0">Meine Mütter, meine Väter, Herr der Erde, Herr der Erde und des Wassers [=der Welt], verschmäht eure Pinienkerne nicht!</ta>
            <ta e="T19" id="Seg_263" s="T9">Er möge [stets] Hirschbock, Elchbullen und Bärenmännchen zu mir treiben!</ta>
         </annotation>
         <annotation name="ltg" tierref="ltg">
            <ta e="T9" id="Seg_264" s="T0">Unsere Mutter, unser Vater, Geist der Erde, Geist der Welt, der Erde, des Wassers, verachtet eure Nüsse nicht!</ta>
            <ta e="T19" id="Seg_265" s="T9">Mir entgegen jage er einen männlichen sibirischen Hirsch, einen männlichen Elch, einen männlichen Bären.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltg"
                          display-name="ltg"
                          name="ltg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
