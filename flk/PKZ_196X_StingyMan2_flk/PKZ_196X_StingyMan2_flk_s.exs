<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID161A4801-0E19-CD66-2A22-5EE87DCA9BD4">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_StingyMan2_flk.wav" />
         <referenced-file url="PKZ_196X_StingyMan2_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_StingyMan2_flk\PKZ_196X_StingyMan2_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">92</ud-information>
            <ud-information attribute-name="# HIAT:w">64</ud-information>
            <ud-information attribute-name="# e">64</ud-information>
            <ud-information attribute-name="# HIAT:u">18</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T143" time="0.028" type="appl" />
         <tli id="T144" time="0.763" type="appl" />
         <tli id="T145" time="1.498" type="appl" />
         <tli id="T146" time="2.233" type="appl" />
         <tli id="T147" time="2.968" type="appl" />
         <tli id="T148" time="3.703" type="appl" />
         <tli id="T149" time="4.676" type="appl" />
         <tli id="T150" time="5.464" type="appl" />
         <tli id="T151" time="6.252" type="appl" />
         <tli id="T152" time="7.039" type="appl" />
         <tli id="T153" time="7.722" type="appl" />
         <tli id="T154" time="8.659756463436548" />
         <tli id="T155" time="9.745" type="appl" />
         <tli id="T156" time="10.753" type="appl" />
         <tli id="T157" time="12.039661411059589" />
         <tli id="T158" time="12.953" type="appl" />
         <tli id="T159" time="13.608" type="appl" />
         <tli id="T160" time="14.263" type="appl" />
         <tli id="T161" time="14.918" type="appl" />
         <tli id="T162" time="15.758" type="appl" />
         <tli id="T163" time="16.598" type="appl" />
         <tli id="T164" time="17.438" type="appl" />
         <tli id="T165" time="18.101" type="appl" />
         <tli id="T166" time="18.633" type="appl" />
         <tli id="T167" time="19.392787953916027" />
         <tli id="T168" time="20.521" type="appl" />
         <tli id="T169" time="21.4" type="appl" />
         <tli id="T170" time="22.28" type="appl" />
         <tli id="T171" time="23.159" type="appl" />
         <tli id="T172" time="23.945" type="appl" />
         <tli id="T173" time="24.63" type="appl" />
         <tli id="T174" time="25.316" type="appl" />
         <tli id="T175" time="26.001" type="appl" />
         <tli id="T176" time="27.492560165675386" />
         <tli id="T177" time="28.156" type="appl" />
         <tli id="T178" time="31.412449927415718" />
         <tli id="T179" time="32.211" type="appl" />
         <tli id="T180" time="32.809" type="appl" />
         <tli id="T181" time="33.408" type="appl" />
         <tli id="T182" time="34.006" type="appl" />
         <tli id="T183" time="38.352254760700895" />
         <tli id="T184" time="39.276" type="appl" />
         <tli id="T185" time="40.086" type="appl" />
         <tli id="T186" time="40.827" type="appl" />
         <tli id="T187" time="41.567" type="appl" />
         <tli id="T188" time="42.308" type="appl" />
         <tli id="T189" time="43.204" type="appl" />
         <tli id="T190" time="44.3920849037906" />
         <tli id="T191" time="46.21870020092809" />
         <tli id="T192" time="46.831" type="appl" />
         <tli id="T193" time="47.435" type="appl" />
         <tli id="T194" time="48.04" type="appl" />
         <tli id="T195" time="48.644" type="appl" />
         <tli id="T196" time="49.7119352947239" />
         <tli id="T197" time="50.805" type="appl" />
         <tli id="T198" time="51.788" type="appl" />
         <tli id="T199" time="53.11850615909347" />
         <tli id="T200" time="53.78" type="appl" />
         <tli id="T201" time="54.509" type="appl" />
         <tli id="T202" time="55.238" type="appl" />
         <tli id="T203" time="55.968" type="appl" />
         <tli id="T204" time="56.697" type="appl" />
         <tli id="T205" time="57.585047214137724" />
         <tli id="T206" time="58.13" type="appl" />
         <tli id="T207" time="58.627" type="appl" />
         <tli id="T208" time="59.125" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T208" id="Seg_0" n="sc" s="T143">
               <ts e="T148" id="Seg_2" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_4" n="HIAT:w" s="T143">Onʼiʔ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_7" n="HIAT:w" s="T144">kuza</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_10" n="HIAT:w" s="T145">kuzanə</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_13" n="HIAT:w" s="T146">jaʔtarlaʔ</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_16" n="HIAT:w" s="T147">šobi</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_20" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_22" n="HIAT:w" s="T148">Dĭ</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_25" n="HIAT:w" s="T149">bar</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_28" n="HIAT:w" s="T150">dʼăbaktərlaʔbə</ts>
                  <nts id="Seg_29" n="HIAT:ip">,</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_32" n="HIAT:w" s="T151">dʼăbaktərlaʔbə</ts>
                  <nts id="Seg_33" n="HIAT:ip">.</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T154" id="Seg_36" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_38" n="HIAT:w" s="T152">Mori</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_41" n="HIAT:w" s="T153">ibi</ts>
                  <nts id="Seg_42" n="HIAT:ip">.</nts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_45" n="HIAT:u" s="T154">
                  <ts e="T155" id="Seg_47" n="HIAT:w" s="T154">Ĭmbi</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_50" n="HIAT:w" s="T155">dĭʔnə</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_53" n="HIAT:w" s="T156">mĭnzərzittə</ts>
                  <nts id="Seg_54" n="HIAT:ip">.</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_57" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_59" n="HIAT:w" s="T157">Amnobi</ts>
                  <nts id="Seg_60" n="HIAT:ip">,</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_63" n="HIAT:w" s="T158">amnobi</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_66" n="HIAT:w" s="T159">dĭ</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_69" n="HIAT:w" s="T160">kuza</ts>
                  <nts id="Seg_70" n="HIAT:ip">.</nts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T164" id="Seg_73" n="HIAT:u" s="T161">
                  <nts id="Seg_74" n="HIAT:ip">(</nts>
                  <ts e="T162" id="Seg_76" n="HIAT:w" s="T161">Am-</ts>
                  <nts id="Seg_77" n="HIAT:ip">)</nts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_80" n="HIAT:w" s="T162">Amzittə</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_83" n="HIAT:w" s="T163">axota</ts>
                  <nts id="Seg_84" n="HIAT:ip">.</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T171" id="Seg_87" n="HIAT:u" s="T164">
                  <ts e="T165" id="Seg_89" n="HIAT:w" s="T164">Dĭgəttə</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_92" n="HIAT:w" s="T165">dĭ</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_95" n="HIAT:w" s="T166">măndə:</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_97" n="HIAT:ip">"</nts>
                  <ts e="T168" id="Seg_99" n="HIAT:w" s="T167">Tăn</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_102" n="HIAT:w" s="T168">padʼi</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_105" n="HIAT:w" s="T169">amorzittə</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_108" n="HIAT:w" s="T170">kereʔ</ts>
                  <nts id="Seg_109" n="HIAT:ip">.</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T176" id="Seg_112" n="HIAT:u" s="T171">
                  <ts e="T172" id="Seg_114" n="HIAT:w" s="T171">Ej</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_117" n="HIAT:w" s="T172">tĭmnem</ts>
                  <nts id="Seg_118" n="HIAT:ip">,</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_121" n="HIAT:w" s="T173">girgit</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_124" n="HIAT:w" s="T174">uja</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_127" n="HIAT:w" s="T175">mĭnzərzittə</ts>
                  <nts id="Seg_128" n="HIAT:ip">.</nts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T178" id="Seg_131" n="HIAT:u" s="T176">
                  <ts e="T178" id="Seg_133" n="HIAT:w" s="T176">Alʼi</ts>
                  <nts id="Seg_134" n="HIAT:ip">…</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T183" id="Seg_137" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_139" n="HIAT:w" s="T178">Bar</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_142" n="HIAT:w" s="T179">to</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_145" n="HIAT:w" s="T180">bar</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_148" n="HIAT:w" s="T181">puskaj</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_150" n="HIAT:ip">(</nts>
                  <ts e="T183" id="Seg_152" n="HIAT:w" s="T182">mĭnzərgəj</ts>
                  <nts id="Seg_153" n="HIAT:ip">)</nts>
                  <nts id="Seg_154" n="HIAT:ip">.</nts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T185" id="Seg_157" n="HIAT:u" s="T183">
                  <ts e="T184" id="Seg_159" n="HIAT:w" s="T183">Barəʔtə</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_162" n="HIAT:w" s="T184">bar</ts>
                  <nts id="Seg_163" n="HIAT:ip">.</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_166" n="HIAT:u" s="T185">
                  <ts e="T186" id="Seg_168" n="HIAT:w" s="T185">Dĭgəttə</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_171" n="HIAT:w" s="T186">jakšə</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_174" n="HIAT:w" s="T187">moləj</ts>
                  <nts id="Seg_175" n="HIAT:ip">.</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T190" id="Seg_178" n="HIAT:u" s="T188">
                  <ts e="T189" id="Seg_180" n="HIAT:w" s="T188">Bar</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_183" n="HIAT:w" s="T189">amluʔpibaʔ</ts>
                  <nts id="Seg_184" n="HIAT:ip">.</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T191" id="Seg_187" n="HIAT:u" s="T190">
                  <ts e="T191" id="Seg_189" n="HIAT:w" s="T190">Kabarləj</ts>
                  <nts id="Seg_190" n="HIAT:ip">.</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T196" id="Seg_193" n="HIAT:u" s="T191">
                  <ts e="T192" id="Seg_195" n="HIAT:w" s="T191">Ugaːndə</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_198" n="HIAT:w" s="T192">dĭ</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_201" n="HIAT:w" s="T193">kuza</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_204" n="HIAT:w" s="T194">mori</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_207" n="HIAT:w" s="T195">ibi</ts>
                  <nts id="Seg_208" n="HIAT:ip">.</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T199" id="Seg_211" n="HIAT:u" s="T196">
                  <ts e="T197" id="Seg_213" n="HIAT:w" s="T196">Ujabə</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_216" n="HIAT:w" s="T197">ajirbi</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_219" n="HIAT:w" s="T198">mĭnzərzittə</ts>
                  <nts id="Seg_220" n="HIAT:ip">.</nts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T205" id="Seg_223" n="HIAT:u" s="T199">
                  <nts id="Seg_224" n="HIAT:ip">(</nts>
                  <ts e="T200" id="Seg_226" n="HIAT:w" s="T199">A</ts>
                  <nts id="Seg_227" n="HIAT:ip">)</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_230" n="HIAT:w" s="T200">A</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_233" n="HIAT:w" s="T201">dĭ</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_236" n="HIAT:w" s="T202">kuza</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_239" n="HIAT:w" s="T203">amzittə</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_242" n="HIAT:w" s="T204">axota</ts>
                  <nts id="Seg_243" n="HIAT:ip">.</nts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T208" id="Seg_246" n="HIAT:u" s="T205">
                  <ts e="T206" id="Seg_248" n="HIAT:w" s="T205">Nu</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_251" n="HIAT:w" s="T206">kabarləj</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_254" n="HIAT:w" s="T207">tuj</ts>
                  <nts id="Seg_255" n="HIAT:ip">.</nts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T208" id="Seg_257" n="sc" s="T143">
               <ts e="T144" id="Seg_259" n="e" s="T143">Onʼiʔ </ts>
               <ts e="T145" id="Seg_261" n="e" s="T144">kuza </ts>
               <ts e="T146" id="Seg_263" n="e" s="T145">kuzanə </ts>
               <ts e="T147" id="Seg_265" n="e" s="T146">jaʔtarlaʔ </ts>
               <ts e="T148" id="Seg_267" n="e" s="T147">šobi. </ts>
               <ts e="T149" id="Seg_269" n="e" s="T148">Dĭ </ts>
               <ts e="T150" id="Seg_271" n="e" s="T149">bar </ts>
               <ts e="T151" id="Seg_273" n="e" s="T150">dʼăbaktərlaʔbə, </ts>
               <ts e="T152" id="Seg_275" n="e" s="T151">dʼăbaktərlaʔbə. </ts>
               <ts e="T153" id="Seg_277" n="e" s="T152">Mori </ts>
               <ts e="T154" id="Seg_279" n="e" s="T153">ibi. </ts>
               <ts e="T155" id="Seg_281" n="e" s="T154">Ĭmbi </ts>
               <ts e="T156" id="Seg_283" n="e" s="T155">dĭʔnə </ts>
               <ts e="T157" id="Seg_285" n="e" s="T156">mĭnzərzittə. </ts>
               <ts e="T158" id="Seg_287" n="e" s="T157">Amnobi, </ts>
               <ts e="T159" id="Seg_289" n="e" s="T158">amnobi </ts>
               <ts e="T160" id="Seg_291" n="e" s="T159">dĭ </ts>
               <ts e="T161" id="Seg_293" n="e" s="T160">kuza. </ts>
               <ts e="T162" id="Seg_295" n="e" s="T161">(Am-) </ts>
               <ts e="T163" id="Seg_297" n="e" s="T162">Amzittə </ts>
               <ts e="T164" id="Seg_299" n="e" s="T163">axota. </ts>
               <ts e="T165" id="Seg_301" n="e" s="T164">Dĭgəttə </ts>
               <ts e="T166" id="Seg_303" n="e" s="T165">dĭ </ts>
               <ts e="T167" id="Seg_305" n="e" s="T166">măndə: </ts>
               <ts e="T168" id="Seg_307" n="e" s="T167">"Tăn </ts>
               <ts e="T169" id="Seg_309" n="e" s="T168">padʼi </ts>
               <ts e="T170" id="Seg_311" n="e" s="T169">amorzittə </ts>
               <ts e="T171" id="Seg_313" n="e" s="T170">kereʔ. </ts>
               <ts e="T172" id="Seg_315" n="e" s="T171">Ej </ts>
               <ts e="T173" id="Seg_317" n="e" s="T172">tĭmnem, </ts>
               <ts e="T174" id="Seg_319" n="e" s="T173">girgit </ts>
               <ts e="T175" id="Seg_321" n="e" s="T174">uja </ts>
               <ts e="T176" id="Seg_323" n="e" s="T175">mĭnzərzittə. </ts>
               <ts e="T178" id="Seg_325" n="e" s="T176">Alʼi… </ts>
               <ts e="T179" id="Seg_327" n="e" s="T178">Bar </ts>
               <ts e="T180" id="Seg_329" n="e" s="T179">to </ts>
               <ts e="T181" id="Seg_331" n="e" s="T180">bar </ts>
               <ts e="T182" id="Seg_333" n="e" s="T181">puskaj </ts>
               <ts e="T183" id="Seg_335" n="e" s="T182">(mĭnzərgəj). </ts>
               <ts e="T184" id="Seg_337" n="e" s="T183">Barəʔtə </ts>
               <ts e="T185" id="Seg_339" n="e" s="T184">bar. </ts>
               <ts e="T186" id="Seg_341" n="e" s="T185">Dĭgəttə </ts>
               <ts e="T187" id="Seg_343" n="e" s="T186">jakšə </ts>
               <ts e="T188" id="Seg_345" n="e" s="T187">moləj. </ts>
               <ts e="T189" id="Seg_347" n="e" s="T188">Bar </ts>
               <ts e="T190" id="Seg_349" n="e" s="T189">amluʔpibaʔ. </ts>
               <ts e="T191" id="Seg_351" n="e" s="T190">Kabarləj. </ts>
               <ts e="T192" id="Seg_353" n="e" s="T191">Ugaːndə </ts>
               <ts e="T193" id="Seg_355" n="e" s="T192">dĭ </ts>
               <ts e="T194" id="Seg_357" n="e" s="T193">kuza </ts>
               <ts e="T195" id="Seg_359" n="e" s="T194">mori </ts>
               <ts e="T196" id="Seg_361" n="e" s="T195">ibi. </ts>
               <ts e="T197" id="Seg_363" n="e" s="T196">Ujabə </ts>
               <ts e="T198" id="Seg_365" n="e" s="T197">ajirbi </ts>
               <ts e="T199" id="Seg_367" n="e" s="T198">mĭnzərzittə. </ts>
               <ts e="T200" id="Seg_369" n="e" s="T199">(A) </ts>
               <ts e="T201" id="Seg_371" n="e" s="T200">A </ts>
               <ts e="T202" id="Seg_373" n="e" s="T201">dĭ </ts>
               <ts e="T203" id="Seg_375" n="e" s="T202">kuza </ts>
               <ts e="T204" id="Seg_377" n="e" s="T203">amzittə </ts>
               <ts e="T205" id="Seg_379" n="e" s="T204">axota. </ts>
               <ts e="T206" id="Seg_381" n="e" s="T205">Nu </ts>
               <ts e="T207" id="Seg_383" n="e" s="T206">kabarləj </ts>
               <ts e="T208" id="Seg_385" n="e" s="T207">tuj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T148" id="Seg_386" s="T143">PKZ_196X_StingyMan2_flk.001 (001)</ta>
            <ta e="T152" id="Seg_387" s="T148">PKZ_196X_StingyMan2_flk.002 (002)</ta>
            <ta e="T154" id="Seg_388" s="T152">PKZ_196X_StingyMan2_flk.003 (003)</ta>
            <ta e="T157" id="Seg_389" s="T154">PKZ_196X_StingyMan2_flk.004 (004)</ta>
            <ta e="T161" id="Seg_390" s="T157">PKZ_196X_StingyMan2_flk.005 (005)</ta>
            <ta e="T164" id="Seg_391" s="T161">PKZ_196X_StingyMan2_flk.006 (006)</ta>
            <ta e="T171" id="Seg_392" s="T164">PKZ_196X_StingyMan2_flk.007 (007) </ta>
            <ta e="T176" id="Seg_393" s="T171">PKZ_196X_StingyMan2_flk.008 (009)</ta>
            <ta e="T178" id="Seg_394" s="T176">PKZ_196X_StingyMan2_flk.009 (010)</ta>
            <ta e="T183" id="Seg_395" s="T178">PKZ_196X_StingyMan2_flk.010 (011)</ta>
            <ta e="T185" id="Seg_396" s="T183">PKZ_196X_StingyMan2_flk.011 (012)</ta>
            <ta e="T188" id="Seg_397" s="T185">PKZ_196X_StingyMan2_flk.012 (013)</ta>
            <ta e="T190" id="Seg_398" s="T188">PKZ_196X_StingyMan2_flk.013 (014)</ta>
            <ta e="T191" id="Seg_399" s="T190">PKZ_196X_StingyMan2_flk.014 (015)</ta>
            <ta e="T196" id="Seg_400" s="T191">PKZ_196X_StingyMan2_flk.015 (016)</ta>
            <ta e="T199" id="Seg_401" s="T196">PKZ_196X_StingyMan2_flk.016 (017)</ta>
            <ta e="T205" id="Seg_402" s="T199">PKZ_196X_StingyMan2_flk.017 (018)</ta>
            <ta e="T208" id="Seg_403" s="T205">PKZ_196X_StingyMan2_flk.018 (019)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T148" id="Seg_404" s="T143">Onʼiʔ kuza kuzanə jaʔtarlaʔ šobi. </ta>
            <ta e="T152" id="Seg_405" s="T148">Dĭ bar dʼăbaktərlaʔbə, dʼăbaktərlaʔbə. </ta>
            <ta e="T154" id="Seg_406" s="T152">Mori ibi. </ta>
            <ta e="T157" id="Seg_407" s="T154">Ĭmbi dĭʔnə mĭnzərzittə. </ta>
            <ta e="T161" id="Seg_408" s="T157">Amnobi, amnobi dĭ kuza. </ta>
            <ta e="T164" id="Seg_409" s="T161">(Am-) Amzittə axota. </ta>
            <ta e="T171" id="Seg_410" s="T164">Dĭgəttə dĭ măndə: "Tăn padʼi amorzittə kereʔ. </ta>
            <ta e="T176" id="Seg_411" s="T171">Ej tĭmnem, girgit uja mĭnzərzittə. </ta>
            <ta e="T178" id="Seg_412" s="T176">Alʼi… </ta>
            <ta e="T183" id="Seg_413" s="T178">Bar to bar puskaj (mĭnzərgəj). </ta>
            <ta e="T185" id="Seg_414" s="T183">Barəʔtə bar. </ta>
            <ta e="T188" id="Seg_415" s="T185">Dĭgəttə jakšə moləj. </ta>
            <ta e="T190" id="Seg_416" s="T188">Bar amluʔpibaʔ. </ta>
            <ta e="T191" id="Seg_417" s="T190">Kabarləj. </ta>
            <ta e="T196" id="Seg_418" s="T191">Ugaːndə dĭ kuza mori ibi. </ta>
            <ta e="T199" id="Seg_419" s="T196">Ujabə ajirbi mĭnzərzittə. </ta>
            <ta e="T205" id="Seg_420" s="T199">(A) A dĭ kuza amzittə axota. </ta>
            <ta e="T208" id="Seg_421" s="T205">Nu kabarləj tuj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T144" id="Seg_422" s="T143">onʼiʔ</ta>
            <ta e="T145" id="Seg_423" s="T144">kuza</ta>
            <ta e="T146" id="Seg_424" s="T145">kuza-nə</ta>
            <ta e="T147" id="Seg_425" s="T146">jaʔtar-laʔ</ta>
            <ta e="T148" id="Seg_426" s="T147">šo-bi</ta>
            <ta e="T149" id="Seg_427" s="T148">dĭ</ta>
            <ta e="T150" id="Seg_428" s="T149">bar</ta>
            <ta e="T151" id="Seg_429" s="T150">dʼăbaktər-laʔbə</ta>
            <ta e="T152" id="Seg_430" s="T151">dʼăbaktər-laʔbə</ta>
            <ta e="T153" id="Seg_431" s="T152">mori</ta>
            <ta e="T154" id="Seg_432" s="T153">i-bi</ta>
            <ta e="T155" id="Seg_433" s="T154">ĭmbi</ta>
            <ta e="T156" id="Seg_434" s="T155">dĭʔ-nə</ta>
            <ta e="T157" id="Seg_435" s="T156">mĭnzər-zittə</ta>
            <ta e="T158" id="Seg_436" s="T157">amno-bi</ta>
            <ta e="T159" id="Seg_437" s="T158">amno-bi</ta>
            <ta e="T160" id="Seg_438" s="T159">dĭ</ta>
            <ta e="T161" id="Seg_439" s="T160">kuza</ta>
            <ta e="T163" id="Seg_440" s="T162">am-zittə</ta>
            <ta e="T164" id="Seg_441" s="T163">axota</ta>
            <ta e="T165" id="Seg_442" s="T164">dĭgəttə</ta>
            <ta e="T166" id="Seg_443" s="T165">dĭ</ta>
            <ta e="T167" id="Seg_444" s="T166">măn-də</ta>
            <ta e="T168" id="Seg_445" s="T167">tăn</ta>
            <ta e="T169" id="Seg_446" s="T168">padʼi</ta>
            <ta e="T170" id="Seg_447" s="T169">amor-zittə</ta>
            <ta e="T171" id="Seg_448" s="T170">kereʔ</ta>
            <ta e="T172" id="Seg_449" s="T171">ej</ta>
            <ta e="T173" id="Seg_450" s="T172">tĭmne-m</ta>
            <ta e="T174" id="Seg_451" s="T173">girgit</ta>
            <ta e="T175" id="Seg_452" s="T174">uja</ta>
            <ta e="T176" id="Seg_453" s="T175">mĭnzər-zittə</ta>
            <ta e="T178" id="Seg_454" s="T176">alʼi</ta>
            <ta e="T179" id="Seg_455" s="T178">bar</ta>
            <ta e="T180" id="Seg_456" s="T179">to</ta>
            <ta e="T181" id="Seg_457" s="T180">bar</ta>
            <ta e="T182" id="Seg_458" s="T181">puskaj</ta>
            <ta e="T183" id="Seg_459" s="T182">mĭnzər-gə-j</ta>
            <ta e="T184" id="Seg_460" s="T183">barəʔ-tə</ta>
            <ta e="T185" id="Seg_461" s="T184">bar</ta>
            <ta e="T186" id="Seg_462" s="T185">dĭgəttə</ta>
            <ta e="T187" id="Seg_463" s="T186">jakšə</ta>
            <ta e="T188" id="Seg_464" s="T187">mo-lə-j</ta>
            <ta e="T189" id="Seg_465" s="T188">bar</ta>
            <ta e="T190" id="Seg_466" s="T189">am-luʔ-pi-baʔ</ta>
            <ta e="T191" id="Seg_467" s="T190">kabarləj</ta>
            <ta e="T192" id="Seg_468" s="T191">ugaːndə</ta>
            <ta e="T193" id="Seg_469" s="T192">dĭ</ta>
            <ta e="T194" id="Seg_470" s="T193">kuza</ta>
            <ta e="T195" id="Seg_471" s="T194">mori</ta>
            <ta e="T196" id="Seg_472" s="T195">i-bi</ta>
            <ta e="T197" id="Seg_473" s="T196">uja-bə</ta>
            <ta e="T198" id="Seg_474" s="T197">ajir-bi</ta>
            <ta e="T199" id="Seg_475" s="T198">mĭnzər-zittə</ta>
            <ta e="T200" id="Seg_476" s="T199">a</ta>
            <ta e="T201" id="Seg_477" s="T200">a</ta>
            <ta e="T202" id="Seg_478" s="T201">dĭ</ta>
            <ta e="T203" id="Seg_479" s="T202">kuza</ta>
            <ta e="T204" id="Seg_480" s="T203">am-zittə</ta>
            <ta e="T205" id="Seg_481" s="T204">axota</ta>
            <ta e="T206" id="Seg_482" s="T205">nu</ta>
            <ta e="T207" id="Seg_483" s="T206">kabarləj</ta>
            <ta e="T208" id="Seg_484" s="T207">tuj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T144" id="Seg_485" s="T143">onʼiʔ</ta>
            <ta e="T145" id="Seg_486" s="T144">kuza</ta>
            <ta e="T146" id="Seg_487" s="T145">kuza-Tə</ta>
            <ta e="T147" id="Seg_488" s="T146">jaʔtar-lAʔ</ta>
            <ta e="T148" id="Seg_489" s="T147">šo-bi</ta>
            <ta e="T149" id="Seg_490" s="T148">dĭ</ta>
            <ta e="T150" id="Seg_491" s="T149">bar</ta>
            <ta e="T151" id="Seg_492" s="T150">tʼăbaktər-laʔbə</ta>
            <ta e="T152" id="Seg_493" s="T151">tʼăbaktər-laʔbə</ta>
            <ta e="T153" id="Seg_494" s="T152">marʼi</ta>
            <ta e="T154" id="Seg_495" s="T153">i-bi</ta>
            <ta e="T155" id="Seg_496" s="T154">ĭmbi</ta>
            <ta e="T156" id="Seg_497" s="T155">dĭ-Tə</ta>
            <ta e="T157" id="Seg_498" s="T156">mĭnzər-zittə</ta>
            <ta e="T158" id="Seg_499" s="T157">amnə-bi</ta>
            <ta e="T159" id="Seg_500" s="T158">amnə-bi</ta>
            <ta e="T160" id="Seg_501" s="T159">dĭ</ta>
            <ta e="T161" id="Seg_502" s="T160">kuza</ta>
            <ta e="T163" id="Seg_503" s="T162">am-zittə</ta>
            <ta e="T164" id="Seg_504" s="T163">axota</ta>
            <ta e="T165" id="Seg_505" s="T164">dĭgəttə</ta>
            <ta e="T166" id="Seg_506" s="T165">dĭ</ta>
            <ta e="T167" id="Seg_507" s="T166">măn-ntə</ta>
            <ta e="T168" id="Seg_508" s="T167">tăn</ta>
            <ta e="T169" id="Seg_509" s="T168">padʼi</ta>
            <ta e="T170" id="Seg_510" s="T169">amor-zittə</ta>
            <ta e="T171" id="Seg_511" s="T170">kereʔ</ta>
            <ta e="T172" id="Seg_512" s="T171">ej</ta>
            <ta e="T173" id="Seg_513" s="T172">tĭmne-m</ta>
            <ta e="T174" id="Seg_514" s="T173">girgit</ta>
            <ta e="T175" id="Seg_515" s="T174">uja</ta>
            <ta e="T176" id="Seg_516" s="T175">mĭnzər-zittə</ta>
            <ta e="T178" id="Seg_517" s="T176">aľi</ta>
            <ta e="T179" id="Seg_518" s="T178">bar</ta>
            <ta e="T180" id="Seg_519" s="T179">to</ta>
            <ta e="T181" id="Seg_520" s="T180">bar</ta>
            <ta e="T182" id="Seg_521" s="T181">puskaj</ta>
            <ta e="T183" id="Seg_522" s="T182">mĭnzər-KV-j</ta>
            <ta e="T184" id="Seg_523" s="T183">barəʔ-t</ta>
            <ta e="T185" id="Seg_524" s="T184">bar</ta>
            <ta e="T186" id="Seg_525" s="T185">dĭgəttə</ta>
            <ta e="T187" id="Seg_526" s="T186">jakšə</ta>
            <ta e="T188" id="Seg_527" s="T187">mo-lV-j</ta>
            <ta e="T189" id="Seg_528" s="T188">bar</ta>
            <ta e="T190" id="Seg_529" s="T189">am-luʔbdə-bi-bAʔ</ta>
            <ta e="T191" id="Seg_530" s="T190">kabarləj</ta>
            <ta e="T192" id="Seg_531" s="T191">ugaːndə</ta>
            <ta e="T193" id="Seg_532" s="T192">dĭ</ta>
            <ta e="T194" id="Seg_533" s="T193">kuza</ta>
            <ta e="T195" id="Seg_534" s="T194">marʼi</ta>
            <ta e="T196" id="Seg_535" s="T195">i-bi</ta>
            <ta e="T197" id="Seg_536" s="T196">uja-bə</ta>
            <ta e="T198" id="Seg_537" s="T197">ajir-bi</ta>
            <ta e="T199" id="Seg_538" s="T198">mĭnzər-zittə</ta>
            <ta e="T200" id="Seg_539" s="T199">a</ta>
            <ta e="T201" id="Seg_540" s="T200">a</ta>
            <ta e="T202" id="Seg_541" s="T201">dĭ</ta>
            <ta e="T203" id="Seg_542" s="T202">kuza</ta>
            <ta e="T204" id="Seg_543" s="T203">am-zittə</ta>
            <ta e="T205" id="Seg_544" s="T204">axota</ta>
            <ta e="T206" id="Seg_545" s="T205">nu</ta>
            <ta e="T207" id="Seg_546" s="T206">kabarləj</ta>
            <ta e="T208" id="Seg_547" s="T207">tüj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T144" id="Seg_548" s="T143">one.[NOM.SG]</ta>
            <ta e="T145" id="Seg_549" s="T144">man.[NOM.SG]</ta>
            <ta e="T146" id="Seg_550" s="T145">man-LAT</ta>
            <ta e="T147" id="Seg_551" s="T146">%visit-CVB</ta>
            <ta e="T148" id="Seg_552" s="T147">come-PST.[3SG]</ta>
            <ta e="T149" id="Seg_553" s="T148">this.[NOM.SG]</ta>
            <ta e="T150" id="Seg_554" s="T149">PTCL</ta>
            <ta e="T151" id="Seg_555" s="T150">speak-DUR.[3SG]</ta>
            <ta e="T152" id="Seg_556" s="T151">speak-DUR.[3SG]</ta>
            <ta e="T153" id="Seg_557" s="T152">stingy.[NOM.SG]</ta>
            <ta e="T154" id="Seg_558" s="T153">be-PST.[3SG]</ta>
            <ta e="T155" id="Seg_559" s="T154">what.[NOM.SG]</ta>
            <ta e="T156" id="Seg_560" s="T155">this-LAT</ta>
            <ta e="T157" id="Seg_561" s="T156">boil-INF.LAT</ta>
            <ta e="T158" id="Seg_562" s="T157">sit-PST.[3SG]</ta>
            <ta e="T159" id="Seg_563" s="T158">sit-PST.[3SG]</ta>
            <ta e="T160" id="Seg_564" s="T159">this.[NOM.SG]</ta>
            <ta e="T161" id="Seg_565" s="T160">man.[NOM.SG]</ta>
            <ta e="T163" id="Seg_566" s="T162">eat-INF.LAT</ta>
            <ta e="T164" id="Seg_567" s="T163">one.wants</ta>
            <ta e="T165" id="Seg_568" s="T164">then</ta>
            <ta e="T166" id="Seg_569" s="T165">this.[NOM.SG]</ta>
            <ta e="T167" id="Seg_570" s="T166">say-IPFVZ.[3SG]</ta>
            <ta e="T168" id="Seg_571" s="T167">you.NOM</ta>
            <ta e="T169" id="Seg_572" s="T168">probably</ta>
            <ta e="T170" id="Seg_573" s="T169">eat-INF.LAT</ta>
            <ta e="T171" id="Seg_574" s="T170">one.needs</ta>
            <ta e="T172" id="Seg_575" s="T171">NEG</ta>
            <ta e="T173" id="Seg_576" s="T172">know-1SG</ta>
            <ta e="T174" id="Seg_577" s="T173">what.kind</ta>
            <ta e="T175" id="Seg_578" s="T174">meat.[NOM.SG]</ta>
            <ta e="T176" id="Seg_579" s="T175">boil-INF.LAT</ta>
            <ta e="T178" id="Seg_580" s="T176">or</ta>
            <ta e="T179" id="Seg_581" s="T178">all</ta>
            <ta e="T180" id="Seg_582" s="T179">then</ta>
            <ta e="T181" id="Seg_583" s="T180">all</ta>
            <ta e="T182" id="Seg_584" s="T181">JUSS</ta>
            <ta e="T183" id="Seg_585" s="T182">boil-IMP-3SG</ta>
            <ta e="T184" id="Seg_586" s="T183">throw.away-IMP.2SG.O</ta>
            <ta e="T185" id="Seg_587" s="T184">all</ta>
            <ta e="T186" id="Seg_588" s="T185">then</ta>
            <ta e="T187" id="Seg_589" s="T186">good.[NOM.SG]</ta>
            <ta e="T188" id="Seg_590" s="T187">become-FUT-3SG</ta>
            <ta e="T189" id="Seg_591" s="T188">all</ta>
            <ta e="T190" id="Seg_592" s="T189">eat-MOM-PST-1PL</ta>
            <ta e="T191" id="Seg_593" s="T190">enough</ta>
            <ta e="T192" id="Seg_594" s="T191">very</ta>
            <ta e="T193" id="Seg_595" s="T192">this.[NOM.SG]</ta>
            <ta e="T194" id="Seg_596" s="T193">man.[NOM.SG]</ta>
            <ta e="T195" id="Seg_597" s="T194">stingy.[NOM.SG]</ta>
            <ta e="T196" id="Seg_598" s="T195">be-PST.[3SG]</ta>
            <ta e="T197" id="Seg_599" s="T196">meat-ACC.3SG</ta>
            <ta e="T198" id="Seg_600" s="T197">pity-PST.[3SG]</ta>
            <ta e="T199" id="Seg_601" s="T198">boil-INF.LAT</ta>
            <ta e="T200" id="Seg_602" s="T199">and</ta>
            <ta e="T201" id="Seg_603" s="T200">and</ta>
            <ta e="T202" id="Seg_604" s="T201">this.[NOM.SG]</ta>
            <ta e="T203" id="Seg_605" s="T202">man.[NOM.SG]</ta>
            <ta e="T204" id="Seg_606" s="T203">eat-INF.LAT</ta>
            <ta e="T205" id="Seg_607" s="T204">one.wants</ta>
            <ta e="T206" id="Seg_608" s="T205">well</ta>
            <ta e="T207" id="Seg_609" s="T206">enough</ta>
            <ta e="T208" id="Seg_610" s="T207">now</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T144" id="Seg_611" s="T143">один.[NOM.SG]</ta>
            <ta e="T145" id="Seg_612" s="T144">мужчина.[NOM.SG]</ta>
            <ta e="T146" id="Seg_613" s="T145">мужчина-LAT</ta>
            <ta e="T147" id="Seg_614" s="T146">%посещать-CVB</ta>
            <ta e="T148" id="Seg_615" s="T147">прийти-PST.[3SG]</ta>
            <ta e="T149" id="Seg_616" s="T148">этот.[NOM.SG]</ta>
            <ta e="T150" id="Seg_617" s="T149">PTCL</ta>
            <ta e="T151" id="Seg_618" s="T150">говорить-DUR.[3SG]</ta>
            <ta e="T152" id="Seg_619" s="T151">говорить-DUR.[3SG]</ta>
            <ta e="T153" id="Seg_620" s="T152">жадный.[NOM.SG]</ta>
            <ta e="T154" id="Seg_621" s="T153">быть-PST.[3SG]</ta>
            <ta e="T155" id="Seg_622" s="T154">что.[NOM.SG]</ta>
            <ta e="T156" id="Seg_623" s="T155">этот-LAT</ta>
            <ta e="T157" id="Seg_624" s="T156">кипятить-INF.LAT</ta>
            <ta e="T158" id="Seg_625" s="T157">сидеть-PST.[3SG]</ta>
            <ta e="T159" id="Seg_626" s="T158">сидеть-PST.[3SG]</ta>
            <ta e="T160" id="Seg_627" s="T159">этот.[NOM.SG]</ta>
            <ta e="T161" id="Seg_628" s="T160">мужчина.[NOM.SG]</ta>
            <ta e="T163" id="Seg_629" s="T162">съесть-INF.LAT</ta>
            <ta e="T164" id="Seg_630" s="T163">хочется</ta>
            <ta e="T165" id="Seg_631" s="T164">тогда</ta>
            <ta e="T166" id="Seg_632" s="T165">этот.[NOM.SG]</ta>
            <ta e="T167" id="Seg_633" s="T166">сказать-IPFVZ.[3SG]</ta>
            <ta e="T168" id="Seg_634" s="T167">ты.NOM</ta>
            <ta e="T169" id="Seg_635" s="T168">наверное</ta>
            <ta e="T170" id="Seg_636" s="T169">есть-INF.LAT</ta>
            <ta e="T171" id="Seg_637" s="T170">нужно</ta>
            <ta e="T172" id="Seg_638" s="T171">NEG</ta>
            <ta e="T173" id="Seg_639" s="T172">знать-1SG</ta>
            <ta e="T174" id="Seg_640" s="T173">какой</ta>
            <ta e="T175" id="Seg_641" s="T174">мясо.[NOM.SG]</ta>
            <ta e="T176" id="Seg_642" s="T175">кипятить-INF.LAT</ta>
            <ta e="T178" id="Seg_643" s="T176">или</ta>
            <ta e="T179" id="Seg_644" s="T178">весь</ta>
            <ta e="T180" id="Seg_645" s="T179">то</ta>
            <ta e="T181" id="Seg_646" s="T180">весь</ta>
            <ta e="T182" id="Seg_647" s="T181">JUSS</ta>
            <ta e="T183" id="Seg_648" s="T182">кипятить-IMP-3SG</ta>
            <ta e="T184" id="Seg_649" s="T183">выбросить-IMP.2SG.O</ta>
            <ta e="T185" id="Seg_650" s="T184">весь</ta>
            <ta e="T186" id="Seg_651" s="T185">тогда</ta>
            <ta e="T187" id="Seg_652" s="T186">хороший.[NOM.SG]</ta>
            <ta e="T188" id="Seg_653" s="T187">стать-FUT-3SG</ta>
            <ta e="T189" id="Seg_654" s="T188">весь</ta>
            <ta e="T190" id="Seg_655" s="T189">съесть-MOM-PST-1PL</ta>
            <ta e="T191" id="Seg_656" s="T190">хватит</ta>
            <ta e="T192" id="Seg_657" s="T191">очень</ta>
            <ta e="T193" id="Seg_658" s="T192">этот.[NOM.SG]</ta>
            <ta e="T194" id="Seg_659" s="T193">мужчина.[NOM.SG]</ta>
            <ta e="T195" id="Seg_660" s="T194">жадный.[NOM.SG]</ta>
            <ta e="T196" id="Seg_661" s="T195">быть-PST.[3SG]</ta>
            <ta e="T197" id="Seg_662" s="T196">мясо-ACC.3SG</ta>
            <ta e="T198" id="Seg_663" s="T197">жалеть-PST.[3SG]</ta>
            <ta e="T199" id="Seg_664" s="T198">кипятить-INF.LAT</ta>
            <ta e="T200" id="Seg_665" s="T199">а</ta>
            <ta e="T201" id="Seg_666" s="T200">а</ta>
            <ta e="T202" id="Seg_667" s="T201">этот.[NOM.SG]</ta>
            <ta e="T203" id="Seg_668" s="T202">мужчина.[NOM.SG]</ta>
            <ta e="T204" id="Seg_669" s="T203">съесть-INF.LAT</ta>
            <ta e="T205" id="Seg_670" s="T204">хочется</ta>
            <ta e="T206" id="Seg_671" s="T205">ну</ta>
            <ta e="T207" id="Seg_672" s="T206">хватит</ta>
            <ta e="T208" id="Seg_673" s="T207">сейчас</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T144" id="Seg_674" s="T143">num-n:case</ta>
            <ta e="T145" id="Seg_675" s="T144">n-n:case</ta>
            <ta e="T146" id="Seg_676" s="T145">n-n:case</ta>
            <ta e="T147" id="Seg_677" s="T146">v-v:n.fin</ta>
            <ta e="T148" id="Seg_678" s="T147">v-v:tense-v:pn</ta>
            <ta e="T149" id="Seg_679" s="T148">dempro-n:case</ta>
            <ta e="T150" id="Seg_680" s="T149">ptcl</ta>
            <ta e="T151" id="Seg_681" s="T150">v-v&gt;v-v:pn</ta>
            <ta e="T152" id="Seg_682" s="T151">v-v&gt;v-v:pn</ta>
            <ta e="T153" id="Seg_683" s="T152">adj-n:case</ta>
            <ta e="T154" id="Seg_684" s="T153">v-v:tense-v:pn</ta>
            <ta e="T155" id="Seg_685" s="T154">que-n:case</ta>
            <ta e="T156" id="Seg_686" s="T155">dempro-n:case</ta>
            <ta e="T157" id="Seg_687" s="T156">v-v:n.fin</ta>
            <ta e="T158" id="Seg_688" s="T157">v-v:tense-v:pn</ta>
            <ta e="T159" id="Seg_689" s="T158">v-v:tense-v:pn</ta>
            <ta e="T160" id="Seg_690" s="T159">dempro-n:case</ta>
            <ta e="T161" id="Seg_691" s="T160">n-n:case</ta>
            <ta e="T163" id="Seg_692" s="T162">v-v:n.fin</ta>
            <ta e="T164" id="Seg_693" s="T163">ptcl</ta>
            <ta e="T165" id="Seg_694" s="T164">adv</ta>
            <ta e="T166" id="Seg_695" s="T165">dempro-n:case</ta>
            <ta e="T167" id="Seg_696" s="T166">v-v&gt;v-v:pn</ta>
            <ta e="T168" id="Seg_697" s="T167">pers</ta>
            <ta e="T169" id="Seg_698" s="T168">ptcl</ta>
            <ta e="T170" id="Seg_699" s="T169">v-v:n.fin</ta>
            <ta e="T171" id="Seg_700" s="T170">adv</ta>
            <ta e="T172" id="Seg_701" s="T171">ptcl</ta>
            <ta e="T173" id="Seg_702" s="T172">v-v:pn</ta>
            <ta e="T174" id="Seg_703" s="T173">que</ta>
            <ta e="T175" id="Seg_704" s="T174">n-n:case</ta>
            <ta e="T176" id="Seg_705" s="T175">v-v:n.fin</ta>
            <ta e="T178" id="Seg_706" s="T176">conj</ta>
            <ta e="T179" id="Seg_707" s="T178">quant</ta>
            <ta e="T180" id="Seg_708" s="T179">conj</ta>
            <ta e="T181" id="Seg_709" s="T180">quant</ta>
            <ta e="T182" id="Seg_710" s="T181">ptcl</ta>
            <ta e="T183" id="Seg_711" s="T182">v-v:mood-v:pn</ta>
            <ta e="T184" id="Seg_712" s="T183">v-v:mood.pn</ta>
            <ta e="T185" id="Seg_713" s="T184">quant</ta>
            <ta e="T186" id="Seg_714" s="T185">adv</ta>
            <ta e="T187" id="Seg_715" s="T186">adj-n:case</ta>
            <ta e="T188" id="Seg_716" s="T187">v-v:tense-v:pn</ta>
            <ta e="T189" id="Seg_717" s="T188">quant</ta>
            <ta e="T190" id="Seg_718" s="T189">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T191" id="Seg_719" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_720" s="T191">adv</ta>
            <ta e="T193" id="Seg_721" s="T192">dempro-n:case</ta>
            <ta e="T194" id="Seg_722" s="T193">n-n:case</ta>
            <ta e="T195" id="Seg_723" s="T194">adj-n:case</ta>
            <ta e="T196" id="Seg_724" s="T195">v-v:tense-v:pn</ta>
            <ta e="T197" id="Seg_725" s="T196">n-n:case.poss</ta>
            <ta e="T198" id="Seg_726" s="T197">v-v:tense-v:pn</ta>
            <ta e="T199" id="Seg_727" s="T198">v-v:n.fin</ta>
            <ta e="T200" id="Seg_728" s="T199">conj</ta>
            <ta e="T201" id="Seg_729" s="T200">conj</ta>
            <ta e="T202" id="Seg_730" s="T201">dempro-n:case</ta>
            <ta e="T203" id="Seg_731" s="T202">n-n:case</ta>
            <ta e="T204" id="Seg_732" s="T203">v-v:n.fin</ta>
            <ta e="T205" id="Seg_733" s="T204">ptcl</ta>
            <ta e="T206" id="Seg_734" s="T205">ptcl</ta>
            <ta e="T207" id="Seg_735" s="T206">ptcl</ta>
            <ta e="T208" id="Seg_736" s="T207">adv</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T144" id="Seg_737" s="T143">num</ta>
            <ta e="T145" id="Seg_738" s="T144">n</ta>
            <ta e="T146" id="Seg_739" s="T145">n</ta>
            <ta e="T147" id="Seg_740" s="T146">v</ta>
            <ta e="T148" id="Seg_741" s="T147">v</ta>
            <ta e="T149" id="Seg_742" s="T148">dempro</ta>
            <ta e="T150" id="Seg_743" s="T149">ptcl</ta>
            <ta e="T151" id="Seg_744" s="T150">v</ta>
            <ta e="T152" id="Seg_745" s="T151">v</ta>
            <ta e="T153" id="Seg_746" s="T152">adj</ta>
            <ta e="T154" id="Seg_747" s="T153">v</ta>
            <ta e="T155" id="Seg_748" s="T154">que</ta>
            <ta e="T156" id="Seg_749" s="T155">dempro</ta>
            <ta e="T157" id="Seg_750" s="T156">v</ta>
            <ta e="T158" id="Seg_751" s="T157">v</ta>
            <ta e="T159" id="Seg_752" s="T158">v</ta>
            <ta e="T160" id="Seg_753" s="T159">dempro</ta>
            <ta e="T161" id="Seg_754" s="T160">n</ta>
            <ta e="T163" id="Seg_755" s="T162">v</ta>
            <ta e="T164" id="Seg_756" s="T163">ptcl</ta>
            <ta e="T165" id="Seg_757" s="T164">adv</ta>
            <ta e="T166" id="Seg_758" s="T165">dempro</ta>
            <ta e="T167" id="Seg_759" s="T166">v</ta>
            <ta e="T168" id="Seg_760" s="T167">pers</ta>
            <ta e="T169" id="Seg_761" s="T168">ptcl</ta>
            <ta e="T170" id="Seg_762" s="T169">v</ta>
            <ta e="T171" id="Seg_763" s="T170">adv</ta>
            <ta e="T172" id="Seg_764" s="T171">ptcl</ta>
            <ta e="T173" id="Seg_765" s="T172">v</ta>
            <ta e="T174" id="Seg_766" s="T173">que</ta>
            <ta e="T175" id="Seg_767" s="T174">n</ta>
            <ta e="T176" id="Seg_768" s="T175">v</ta>
            <ta e="T178" id="Seg_769" s="T176">conj</ta>
            <ta e="T179" id="Seg_770" s="T178">quant</ta>
            <ta e="T180" id="Seg_771" s="T179">conj</ta>
            <ta e="T181" id="Seg_772" s="T180">quant</ta>
            <ta e="T182" id="Seg_773" s="T181">ptcl</ta>
            <ta e="T183" id="Seg_774" s="T182">v</ta>
            <ta e="T184" id="Seg_775" s="T183">v</ta>
            <ta e="T185" id="Seg_776" s="T184">quant</ta>
            <ta e="T186" id="Seg_777" s="T185">adv</ta>
            <ta e="T187" id="Seg_778" s="T186">adj</ta>
            <ta e="T188" id="Seg_779" s="T187">v</ta>
            <ta e="T189" id="Seg_780" s="T188">quant</ta>
            <ta e="T190" id="Seg_781" s="T189">v</ta>
            <ta e="T191" id="Seg_782" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_783" s="T191">adv</ta>
            <ta e="T193" id="Seg_784" s="T192">dempro</ta>
            <ta e="T194" id="Seg_785" s="T193">n</ta>
            <ta e="T195" id="Seg_786" s="T194">adj</ta>
            <ta e="T196" id="Seg_787" s="T195">v</ta>
            <ta e="T197" id="Seg_788" s="T196">n</ta>
            <ta e="T198" id="Seg_789" s="T197">v</ta>
            <ta e="T199" id="Seg_790" s="T198">v</ta>
            <ta e="T200" id="Seg_791" s="T199">conj</ta>
            <ta e="T201" id="Seg_792" s="T200">conj</ta>
            <ta e="T202" id="Seg_793" s="T201">dempro</ta>
            <ta e="T203" id="Seg_794" s="T202">n</ta>
            <ta e="T204" id="Seg_795" s="T203">v</ta>
            <ta e="T205" id="Seg_796" s="T204">ptcl</ta>
            <ta e="T206" id="Seg_797" s="T205">ptcl</ta>
            <ta e="T207" id="Seg_798" s="T206">ptcl</ta>
            <ta e="T208" id="Seg_799" s="T207">adv</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T145" id="Seg_800" s="T144">np.h:A</ta>
            <ta e="T146" id="Seg_801" s="T145">np:G</ta>
            <ta e="T149" id="Seg_802" s="T148">pro.h:A</ta>
            <ta e="T155" id="Seg_803" s="T154">pro:P</ta>
            <ta e="T156" id="Seg_804" s="T155">pro.h:B</ta>
            <ta e="T161" id="Seg_805" s="T160">np.h:E</ta>
            <ta e="T165" id="Seg_806" s="T164">adv:Time</ta>
            <ta e="T166" id="Seg_807" s="T165">pro.h:A</ta>
            <ta e="T168" id="Seg_808" s="T167">pro.h:A</ta>
            <ta e="T173" id="Seg_809" s="T172">pro.h:A</ta>
            <ta e="T175" id="Seg_810" s="T174">np:P</ta>
            <ta e="T184" id="Seg_811" s="T183">0.2.h:A</ta>
            <ta e="T186" id="Seg_812" s="T185">adv:Time</ta>
            <ta e="T188" id="Seg_813" s="T187">0.3:Th</ta>
            <ta e="T189" id="Seg_814" s="T188">pro:P</ta>
            <ta e="T190" id="Seg_815" s="T189">0.1.h:A</ta>
            <ta e="T194" id="Seg_816" s="T193">np.h:Th</ta>
            <ta e="T197" id="Seg_817" s="T196">np:B</ta>
            <ta e="T198" id="Seg_818" s="T197">0.3.h:E</ta>
            <ta e="T203" id="Seg_819" s="T202">np.h:A</ta>
            <ta e="T208" id="Seg_820" s="T207">adv:Time</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T145" id="Seg_821" s="T144">np.h:S</ta>
            <ta e="T147" id="Seg_822" s="T146">conv:pred</ta>
            <ta e="T148" id="Seg_823" s="T147">v:pred</ta>
            <ta e="T149" id="Seg_824" s="T148">pro.h:S</ta>
            <ta e="T151" id="Seg_825" s="T150">v:pred</ta>
            <ta e="T152" id="Seg_826" s="T151">v:pred</ta>
            <ta e="T153" id="Seg_827" s="T152">adj:pred</ta>
            <ta e="T154" id="Seg_828" s="T153">cop</ta>
            <ta e="T155" id="Seg_829" s="T154">pro:O</ta>
            <ta e="T157" id="Seg_830" s="T156">v:pred</ta>
            <ta e="T158" id="Seg_831" s="T157">v:pred</ta>
            <ta e="T159" id="Seg_832" s="T158">v:pred</ta>
            <ta e="T161" id="Seg_833" s="T160">np.h:S</ta>
            <ta e="T164" id="Seg_834" s="T163">ptcl:pred</ta>
            <ta e="T166" id="Seg_835" s="T165">pro.h:S</ta>
            <ta e="T167" id="Seg_836" s="T166">v:pred</ta>
            <ta e="T168" id="Seg_837" s="T167">pro.h:S</ta>
            <ta e="T170" id="Seg_838" s="T169">v:pred</ta>
            <ta e="T172" id="Seg_839" s="T171">ptcl.neg</ta>
            <ta e="T173" id="Seg_840" s="T172">v:pred 0.1.h:S</ta>
            <ta e="T175" id="Seg_841" s="T174">np:S</ta>
            <ta e="T176" id="Seg_842" s="T175">v:pred</ta>
            <ta e="T182" id="Seg_843" s="T181">ptcl:pred</ta>
            <ta e="T184" id="Seg_844" s="T183">v:pred 0.2.h:S</ta>
            <ta e="T187" id="Seg_845" s="T186">adj:pred</ta>
            <ta e="T188" id="Seg_846" s="T187">cop 0.3:S</ta>
            <ta e="T189" id="Seg_847" s="T188">pro:O</ta>
            <ta e="T190" id="Seg_848" s="T189">v:pred 0.1.h:S</ta>
            <ta e="T194" id="Seg_849" s="T193">np.h:S</ta>
            <ta e="T195" id="Seg_850" s="T194">adj:pred</ta>
            <ta e="T196" id="Seg_851" s="T195">cop</ta>
            <ta e="T197" id="Seg_852" s="T196">np:O</ta>
            <ta e="T198" id="Seg_853" s="T197">v:pred 0.3.h:S</ta>
            <ta e="T199" id="Seg_854" s="T198">v:pred</ta>
            <ta e="T205" id="Seg_855" s="T204">ptcl:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T150" id="Seg_856" s="T149">TURK:disc</ta>
            <ta e="T151" id="Seg_857" s="T150">%TURK:core</ta>
            <ta e="T152" id="Seg_858" s="T151">%TURK:core</ta>
            <ta e="T164" id="Seg_859" s="T163">RUS:mod</ta>
            <ta e="T169" id="Seg_860" s="T168">RUS:mod</ta>
            <ta e="T178" id="Seg_861" s="T176">RUS:gram</ta>
            <ta e="T179" id="Seg_862" s="T178">TURK:disc</ta>
            <ta e="T180" id="Seg_863" s="T179">RUS:gram</ta>
            <ta e="T181" id="Seg_864" s="T180">TURK:disc</ta>
            <ta e="T182" id="Seg_865" s="T181">RUS:mod</ta>
            <ta e="T185" id="Seg_866" s="T184">TURK:disc</ta>
            <ta e="T187" id="Seg_867" s="T186">TURK:core</ta>
            <ta e="T189" id="Seg_868" s="T188">TURK:disc</ta>
            <ta e="T200" id="Seg_869" s="T199">RUS:gram</ta>
            <ta e="T201" id="Seg_870" s="T200">RUS:gram</ta>
            <ta e="T205" id="Seg_871" s="T204">RUS:mod</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T148" id="Seg_872" s="T143">Один человек пришёл (в гости?) к другому человеку.</ta>
            <ta e="T152" id="Seg_873" s="T148">Он говорит, говорит.</ta>
            <ta e="T154" id="Seg_874" s="T152">Он был жадный.</ta>
            <ta e="T157" id="Seg_875" s="T154">Что ему приготовить?</ta>
            <ta e="T161" id="Seg_876" s="T157">Тот человек сидел, сидел.</ta>
            <ta e="T164" id="Seg_877" s="T161">Есть хочется.</ta>
            <ta e="T167" id="Seg_878" s="T164">Потом он говорит:</ta>
            <ta e="T171" id="Seg_879" s="T167">«Ты, наверное, есть хочешь.</ta>
            <ta e="T176" id="Seg_880" s="T171">Не знаю, какое мясо приготовить.</ta>
            <ta e="T178" id="Seg_881" s="T176">Или…»</ta>
            <ta e="T183" id="Seg_882" s="T178">Пускай тогда (всё?) сварит.</ta>
            <ta e="T185" id="Seg_883" s="T183">«Бросай всё!</ta>
            <ta e="T188" id="Seg_884" s="T185">Тогда будет хорошо».</ta>
            <ta e="T190" id="Seg_885" s="T188">Мы съели всё.</ta>
            <ta e="T191" id="Seg_886" s="T190">Хватит!</ta>
            <ta e="T196" id="Seg_887" s="T191">Очень этот человек жадный был.</ta>
            <ta e="T199" id="Seg_888" s="T196">Ему жалко было мясо варить.</ta>
            <ta e="T205" id="Seg_889" s="T199">А другой человек есть хотел.</ta>
            <ta e="T208" id="Seg_890" s="T205">Ну, теперь хватит.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T148" id="Seg_891" s="T143">One man came (to visit?) [another] man.</ta>
            <ta e="T152" id="Seg_892" s="T148">He speaks, he speaks.</ta>
            <ta e="T154" id="Seg_893" s="T152">He was stingy.</ta>
            <ta e="T157" id="Seg_894" s="T154">What to cook for him?</ta>
            <ta e="T161" id="Seg_895" s="T157">He was sitting for a long time, the man.</ta>
            <ta e="T164" id="Seg_896" s="T161">He wants to eat.</ta>
            <ta e="T167" id="Seg_897" s="T164">Then he says:</ta>
            <ta e="T171" id="Seg_898" s="T167">"You probably need to eat.</ta>
            <ta e="T176" id="Seg_899" s="T171">I don't know which kind of meat to cook.</ta>
            <ta e="T178" id="Seg_900" s="T176">Or…”</ta>
            <ta e="T183" id="Seg_901" s="T178">Let [him] cook (everything?) then.</ta>
            <ta e="T185" id="Seg_902" s="T183">Throw it all [in!].</ta>
            <ta e="T188" id="Seg_903" s="T185">Then it will become good.</ta>
            <ta e="T190" id="Seg_904" s="T188">We ate everything.</ta>
            <ta e="T191" id="Seg_905" s="T190">Enough!</ta>
            <ta e="T196" id="Seg_906" s="T191">This man was very stingy.</ta>
            <ta e="T199" id="Seg_907" s="T196">He cherished his meat [too much] to cook ([it]?).</ta>
            <ta e="T205" id="Seg_908" s="T199">But the [other] man wanted to eat.</ta>
            <ta e="T208" id="Seg_909" s="T205">Well, enough for now.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T148" id="Seg_910" s="T143">Ein Mann kam einen [anderen] Mann (besuchen?).</ta>
            <ta e="T152" id="Seg_911" s="T148">Er spricht, er spricht.</ta>
            <ta e="T154" id="Seg_912" s="T152">Er war geizig.</ta>
            <ta e="T157" id="Seg_913" s="T154">Was ihm kochen?</ta>
            <ta e="T161" id="Seg_914" s="T157">Er saß langer Zeit, der Mann.</ta>
            <ta e="T164" id="Seg_915" s="T161">Er will essen.</ta>
            <ta e="T167" id="Seg_916" s="T164">Dann sagt er:</ta>
            <ta e="T171" id="Seg_917" s="T167">„Du musst wahrscheinlich essen.</ta>
            <ta e="T176" id="Seg_918" s="T171">Ich weiß nicht, welche Art von Fleisch zu kochen.</ta>
            <ta e="T178" id="Seg_919" s="T176">Oder…“</ta>
            <ta e="T183" id="Seg_920" s="T178">Lass [ihn] dann (alles?) kochen.</ta>
            <ta e="T185" id="Seg_921" s="T183">Wirf alles [hinein!].</ta>
            <ta e="T188" id="Seg_922" s="T185">Dann wird es gut.</ta>
            <ta e="T190" id="Seg_923" s="T188">Wir aßen alles.</ta>
            <ta e="T191" id="Seg_924" s="T190">Genug!</ta>
            <ta e="T196" id="Seg_925" s="T191">Dieser Mann war sehr geizig.</ta>
            <ta e="T199" id="Seg_926" s="T196">Er schätzte sein Fleisch [zu sehr], um [es] zu (kochen?).</ta>
            <ta e="T205" id="Seg_927" s="T199">Aber der [andere] Mann wollte essen.</ta>
            <ta e="T208" id="Seg_928" s="T205">Also, nun ist genug.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T148" id="Seg_929" s="T143">[KlT:] jaʔdar- frequentative?</ta>
            <ta e="T154" id="Seg_930" s="T152">[GVY:] Donner (38a) mari.</ta>
            <ta e="T171" id="Seg_931" s="T167">[KlT:] поди 'probably' Ru.</ta>
            <ta e="T176" id="Seg_932" s="T171">[GVY:] Maybe the form tĭmnem should be glossed as tĭm-nie-m 'know-PRS-1SG'</ta>
            <ta e="T183" id="Seg_933" s="T178">[GVY:] Unclear and very tentatively transcribed.</ta>
            <ta e="T185" id="Seg_934" s="T183">[KlT:] one possible reading.</ta>
            <ta e="T199" id="Seg_935" s="T196">[KlT:] Ru. construction жалеть делать что-то.</ta>
            <ta e="T205" id="Seg_936" s="T199">[GVY:] NB kuza in Nominative, not in Dative, as in Russian with "oxota".</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
