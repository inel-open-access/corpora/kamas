<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID697C9283-4EE0-9006-2980-B748C4449E75">
   <head>
      <meta-information>
         <project-name>Kamas</project-name>
         <transcription-name>PKZ_196X_HareAndRooster_flk</transcription-name>
         <referenced-file url="PKZ_196X_HareAndRooster_flk.wav" />
         <referenced-file url="PKZ_196X_HareAndRooster_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_HareAndRooster_flk\PKZ_196X_HareAndRooster_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">221</ud-information>
            <ud-information attribute-name="# HIAT:w">140</ud-information>
            <ud-information attribute-name="# e">140</ud-information>
            <ud-information attribute-name="# HIAT:u">23</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.0" type="appl" />
         <tli id="T2" time="0.726" type="appl" />
         <tli id="T3" time="1.452" type="appl" />
         <tli id="T4" time="2.178" type="appl" />
         <tli id="T5" time="2.904" type="appl" />
         <tli id="T7" time="4.011" type="appl" />
         <tli id="T8" time="4.747923076923077" type="appl" />
         <tli id="T9" time="5.484846153846154" type="appl" />
         <tli id="T10" time="6.2217692307692305" type="appl" />
         <tli id="T11" time="6.958692307692307" type="appl" />
         <tli id="T12" time="7.695615384615384" type="appl" />
         <tli id="T13" time="8.43253846153846" type="appl" />
         <tli id="T14" time="9.169461538461537" type="appl" />
         <tli id="T15" time="9.906384615384614" type="appl" />
         <tli id="T16" time="10.64330769230769" type="appl" />
         <tli id="T17" time="11.380230769230767" type="appl" />
         <tli id="T18" time="12.117153846153844" type="appl" />
         <tli id="T19" time="12.85407692307692" type="appl" />
         <tli id="T22" time="13.711" type="appl" />
         <tli id="T23" time="14.4302" type="appl" />
         <tli id="T24" time="15.1494" type="appl" />
         <tli id="T25" time="15.868599999999999" type="appl" />
         <tli id="T26" time="16.587799999999998" type="appl" />
         <tli id="T28" time="17.331" type="appl" />
         <tli id="T29" time="18.012249999999998" type="appl" />
         <tli id="T30" time="18.6935" type="appl" />
         <tli id="T31" time="19.37475" type="appl" />
         <tli id="T32" time="20.055999999999997" type="appl" />
         <tli id="T33" time="20.73725" type="appl" />
         <tli id="T34" time="21.418499999999998" type="appl" />
         <tli id="T35" time="22.09975" type="appl" />
         <tli id="T37" time="23.001" type="appl" />
         <tli id="T38" time="23.949" type="appl" />
         <tli id="T39" time="24.897000000000002" type="appl" />
         <tli id="T40" time="25.845" type="appl" />
         <tli id="T41" time="26.793" type="appl" />
         <tli id="T43" time="27.941" type="appl" />
         <tli id="T44" time="28.523857142857143" type="appl" />
         <tli id="T45" time="29.106714285714286" type="appl" />
         <tli id="T46" time="29.689571428571426" type="appl" />
         <tli id="T47" time="30.27242857142857" type="appl" />
         <tli id="T48" time="30.855285714285714" type="appl" />
         <tli id="T49" time="31.438142857142857" type="appl" />
         <tli id="T51" time="32.026" type="appl" />
         <tli id="T52" time="32.702000000000005" type="appl" />
         <tli id="T53" time="33.378" type="appl" />
         <tli id="T54" time="34.054" type="appl" />
         <tli id="T55" time="34.73" type="appl" />
         <tli id="T57" time="35.521" type="appl" />
         <tli id="T58" time="36.235" type="appl" />
         <tli id="T59" time="36.949" type="appl" />
         <tli id="T60" time="37.663000000000004" type="appl" />
         <tli id="T61" time="38.377" type="appl" />
         <tli id="T63" time="39.741" type="appl" />
         <tli id="T64" time="40.431" type="appl" />
         <tli id="T65" time="41.121" type="appl" />
         <tli id="T66" time="41.811" type="appl" />
         <tli id="T67" time="42.501000000000005" type="appl" />
         <tli id="T69" time="43.201" type="appl" />
         <tli id="T70" time="43.971000000000004" type="appl" />
         <tli id="T72" time="44.841" type="appl" />
         <tli id="T73" time="45.64625" type="appl" />
         <tli id="T74" time="46.4515" type="appl" />
         <tli id="T75" time="47.256750000000004" type="appl" />
         <tli id="T76" time="48.062" type="appl" />
         <tli id="T77" time="48.86725" type="appl" />
         <tli id="T78" time="49.6725" type="appl" />
         <tli id="T79" time="50.47775" type="appl" />
         <tli id="T81" time="51.731" type="appl" />
         <tli id="T82" time="52.25533333333333" type="appl" />
         <tli id="T83" time="52.77966666666667" type="appl" />
         <tli id="T84" time="53.304" type="appl" />
         <tli id="T85" time="53.82833333333333" type="appl" />
         <tli id="T86" time="54.35266666666667" type="appl" />
         <tli id="T88" time="54.899" type="appl" />
         <tli id="T89" time="55.949400000000004" type="appl" />
         <tli id="T90" time="56.9998" type="appl" />
         <tli id="T91" time="58.050200000000004" type="appl" />
         <tli id="T92" time="59.1006" type="appl" />
         <tli id="T93" time="60.151" type="appl" />
         <tli id="T94" time="60.781000000000006" type="appl" />
         <tli id="T95" time="61.411" type="appl" />
         <tli id="T96" time="62.041000000000004" type="appl" />
         <tli id="T97" time="62.67100000000001" type="appl" />
         <tli id="T98" time="63.30100000000001" type="appl" />
         <tli id="T99" time="63.931000000000004" type="appl" />
         <tli id="T100" time="64.561" type="appl" />
         <tli id="T101" time="65.12766666666667" type="appl" />
         <tli id="T102" time="65.69433333333333" type="appl" />
         <tli id="T103" time="66.261" type="appl" />
         <tli id="T104" time="66.82766666666667" type="appl" />
         <tli id="T105" time="67.39433333333334" type="appl" />
         <tli id="T107" time="68.411" type="appl" />
         <tli id="T108" time="69.161" type="appl" />
         <tli id="T109" time="69.911" type="appl" />
         <tli id="T110" time="70.661" type="appl" />
         <tli id="T111" time="71.411" type="appl" />
         <tli id="T113" time="74.176" type="appl" />
         <tli id="T114" time="74.8545" type="appl" />
         <tli id="T115" time="75.533" type="appl" />
         <tli id="T116" time="76.44633333333334" type="appl" />
         <tli id="T117" time="77.35966666666667" type="appl" />
         <tli id="T118" time="78.273" type="appl" />
         <tli id="T119" time="79.18633333333334" type="appl" />
         <tli id="T120" time="80.09966666666668" type="appl" />
         <tli id="T122" time="81.401" type="appl" />
         <tli id="T123" time="82.46849999999999" type="appl" />
         <tli id="T124" time="83.536" type="appl" />
         <tli id="T125" time="84.6035" type="appl" />
         <tli id="T126" time="85.67099999999999" type="appl" />
         <tli id="T127" time="86.7385" type="appl" />
         <tli id="T128" time="87.806" type="appl" />
         <tli id="T129" time="88.8735" type="appl" />
         <tli id="T130" time="89.941" type="appl" />
         <tli id="T131" time="90.721" type="appl" />
         <tli id="T133" time="91.611" type="appl" />
         <tli id="T134" time="92.40242857142857" type="appl" />
         <tli id="T135" time="93.19385714285714" type="appl" />
         <tli id="T136" time="93.98528571428571" type="appl" />
         <tli id="T137" time="94.77671428571429" type="appl" />
         <tli id="T138" time="95.56814285714286" type="appl" />
         <tli id="T139" time="96.35957142857143" type="appl" />
         <tli id="T141" time="97.171" type="appl" />
         <tli id="T142" time="97.84766666666667" type="appl" />
         <tli id="T143" time="98.52433333333333" type="appl" />
         <tli id="T145" time="99.211" type="appl" />
         <tli id="T146" time="99.691" type="appl" />
         <tli id="T147" time="100.171" type="appl" />
         <tli id="T148" time="100.651" type="appl" />
         <tli id="T149" time="101.131" type="appl" />
         <tli id="T150" time="101.611" type="appl" />
         <tli id="T151" time="102.09100000000001" type="appl" />
         <tli id="T152" time="102.571" type="appl" />
         <tli id="T153" time="103.051" type="appl" />
         <tli id="T155" time="104.506" type="appl" />
         <tli id="T156" time="105.10814285714285" type="appl" />
         <tli id="T157" time="105.71028571428572" type="appl" />
         <tli id="T158" time="106.31242857142857" type="appl" />
         <tli id="T159" time="106.91457142857143" type="appl" />
         <tli id="T160" time="107.51671428571429" type="appl" />
         <tli id="T161" time="108.11885714285715" type="appl" />
         <tli id="T162" time="108.721" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T162" id="Seg_0" n="sc" s="T1">
               <ts e="T7" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Amnobiʔi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">šidogö</ts>
                  <nts id="Seg_8" n="HIAT:ip">,</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_11" n="HIAT:w" s="T3">zajas</ts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_14" n="HIAT:w" s="T4">i</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_17" n="HIAT:w" s="T5">pʼetux</ts>
                  <nts id="Seg_18" n="HIAT:ip">.</nts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_21" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_23" n="HIAT:w" s="T7">Pʼetugən</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_26" n="HIAT:w" s="T8">tura</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_29" n="HIAT:w" s="T9">ibi</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_31" n="HIAT:ip">(</nts>
                  <ts e="T11" id="Seg_33" n="HIAT:w" s="T10">paʔ-</ts>
                  <nts id="Seg_34" n="HIAT:ip">)</nts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_37" n="HIAT:w" s="T11">paʔi</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_40" n="HIAT:w" s="T12">abi</ts>
                  <nts id="Seg_41" n="HIAT:ip">,</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_43" n="HIAT:ip">(</nts>
                  <ts e="T14" id="Seg_45" n="HIAT:w" s="T13">a</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_48" n="HIAT:w" s="T14">pʼetux</ts>
                  <nts id="Seg_49" n="HIAT:ip">)</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_52" n="HIAT:w" s="T15">a</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_55" n="HIAT:w" s="T16">zajas</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_58" n="HIAT:w" s="T17">abi</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_61" n="HIAT:w" s="T18">sĭregə</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_64" n="HIAT:w" s="T19">tura</ts>
                  <nts id="Seg_65" n="HIAT:ip">.</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_68" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_70" n="HIAT:w" s="T22">Dĭgəttə</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_73" n="HIAT:w" s="T23">ejü</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_76" n="HIAT:w" s="T24">molambi</ts>
                  <nts id="Seg_77" n="HIAT:ip">,</nts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_80" n="HIAT:w" s="T25">kuja</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_83" n="HIAT:w" s="T26">mĭndlaʔbə</ts>
                  <nts id="Seg_84" n="HIAT:ip">.</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_87" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_89" n="HIAT:w" s="T28">Dĭ</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_92" n="HIAT:w" s="T29">zajasən</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_95" n="HIAT:w" s="T30">turat</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_98" n="HIAT:w" s="T31">bar</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_101" n="HIAT:w" s="T32">külambi</ts>
                  <nts id="Seg_102" n="HIAT:ip">,</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_104" n="HIAT:ip">(</nts>
                  <ts e="T34" id="Seg_106" n="HIAT:w" s="T33">b-</ts>
                  <nts id="Seg_107" n="HIAT:ip">)</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_110" n="HIAT:w" s="T34">bü</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_113" n="HIAT:w" s="T35">molambi</ts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_117" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_119" n="HIAT:w" s="T37">Dibər</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_122" n="HIAT:w" s="T38">dʼăʔpi</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_125" n="HIAT:w" s="T39">molambi</ts>
                  <nts id="Seg_126" n="HIAT:ip">,</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_129" n="HIAT:w" s="T40">pʼetuxtə</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_132" n="HIAT:w" s="T41">mănde:</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_134" n="HIAT:ip">"</nts>
                  <ts e="T44" id="Seg_136" n="HIAT:w" s="T43">Öʔleʔ</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_139" n="HIAT:w" s="T44">măna</ts>
                  <nts id="Seg_140" n="HIAT:ip">,</nts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_143" n="HIAT:w" s="T45">măn</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_146" n="HIAT:w" s="T46">ej</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_149" n="HIAT:w" s="T47">umlem</ts>
                  <nts id="Seg_150" n="HIAT:ip">,</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_153" n="HIAT:w" s="T48">kolam</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_156" n="HIAT:w" s="T49">tănan</ts>
                  <nts id="Seg_157" n="HIAT:ip">"</nts>
                  <nts id="Seg_158" n="HIAT:ip">.</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_161" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_163" n="HIAT:w" s="T51">Dĭ</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_166" n="HIAT:w" s="T52">öʔlubi</ts>
                  <nts id="Seg_167" n="HIAT:ip">,</nts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_170" n="HIAT:w" s="T53">dĭ</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_173" n="HIAT:w" s="T54">dĭm</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_176" n="HIAT:w" s="T55">sürerluʔpi</ts>
                  <nts id="Seg_177" n="HIAT:ip">.</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_180" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_182" n="HIAT:w" s="T57">Dĭ</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_185" n="HIAT:w" s="T58">kandəga</ts>
                  <nts id="Seg_186" n="HIAT:ip">,</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_189" n="HIAT:w" s="T59">kandəga</ts>
                  <nts id="Seg_190" n="HIAT:ip">,</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_193" n="HIAT:w" s="T60">dʼorlaʔbə</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_196" n="HIAT:w" s="T61">bar</ts>
                  <nts id="Seg_197" n="HIAT:ip">.</nts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_200" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_202" n="HIAT:w" s="T63">Dĭgəttə</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_205" n="HIAT:w" s="T64">šonəga</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_208" n="HIAT:w" s="T65">diʔnə</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_210" n="HIAT:ip">(</nts>
                  <ts e="T67" id="Seg_212" n="HIAT:w" s="T66">li-</ts>
                  <nts id="Seg_213" n="HIAT:ip">)</nts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_216" n="HIAT:w" s="T67">lisitsa</ts>
                  <nts id="Seg_217" n="HIAT:ip">.</nts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_220" n="HIAT:u" s="T69">
                  <nts id="Seg_221" n="HIAT:ip">"</nts>
                  <ts e="T70" id="Seg_223" n="HIAT:w" s="T69">Ĭmbi</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_226" n="HIAT:w" s="T70">dʼorlaʔbəl</ts>
                  <nts id="Seg_227" n="HIAT:ip">?</nts>
                  <nts id="Seg_228" n="HIAT:ip">"</nts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_231" n="HIAT:u" s="T72">
                  <nts id="Seg_232" n="HIAT:ip">"</nts>
                  <ts e="T73" id="Seg_234" n="HIAT:w" s="T72">Da</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_237" n="HIAT:w" s="T73">măn</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_240" n="HIAT:w" s="T74">turagə</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_242" n="HIAT:ip">(</nts>
                  <ts e="T76" id="Seg_244" n="HIAT:w" s="T75">za-</ts>
                  <nts id="Seg_245" n="HIAT:ip">)</nts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_248" n="HIAT:w" s="T76">zajas</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_251" n="HIAT:w" s="T77">amnolaʔbə</ts>
                  <nts id="Seg_252" n="HIAT:ip">,</nts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_255" n="HIAT:w" s="T78">măna</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_258" n="HIAT:w" s="T79">sürerluʔbi</ts>
                  <nts id="Seg_259" n="HIAT:ip">"</nts>
                  <nts id="Seg_260" n="HIAT:ip">.</nts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_263" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_265" n="HIAT:w" s="T81">Dĭ</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_268" n="HIAT:w" s="T82">mănde:</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_270" n="HIAT:ip">"</nts>
                  <ts e="T84" id="Seg_272" n="HIAT:w" s="T83">Kanžəbəj</ts>
                  <nts id="Seg_273" n="HIAT:ip">,</nts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_276" n="HIAT:w" s="T84">măn</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_279" n="HIAT:w" s="T85">dĭm</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_282" n="HIAT:w" s="T86">sürerlʼim</ts>
                  <nts id="Seg_283" n="HIAT:ip">"</nts>
                  <nts id="Seg_284" n="HIAT:ip">.</nts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_287" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_289" n="HIAT:w" s="T88">Dĭgəttə</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_292" n="HIAT:w" s="T89">šobiʔi</ts>
                  <nts id="Seg_293" n="HIAT:ip">,</nts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_295" n="HIAT:ip">(</nts>
                  <ts e="T91" id="Seg_297" n="HIAT:w" s="T90">š-</ts>
                  <nts id="Seg_298" n="HIAT:ip">)</nts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_301" n="HIAT:w" s="T91">sürerbi</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_304" n="HIAT:w" s="T92">zajasəm</ts>
                  <nts id="Seg_305" n="HIAT:ip">.</nts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_308" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_310" n="HIAT:w" s="T93">I</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_313" n="HIAT:w" s="T94">bostə</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_316" n="HIAT:w" s="T95">amnolaʔbə</ts>
                  <nts id="Seg_317" n="HIAT:ip">,</nts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_319" n="HIAT:ip">(</nts>
                  <ts e="T97" id="Seg_321" n="HIAT:w" s="T96">ej=</ts>
                  <nts id="Seg_322" n="HIAT:ip">)</nts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_325" n="HIAT:w" s="T97">ej</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_328" n="HIAT:w" s="T98">öʔle</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_331" n="HIAT:w" s="T99">pʼetugəm</ts>
                  <nts id="Seg_332" n="HIAT:ip">.</nts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_335" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_337" n="HIAT:w" s="T100">Dĭgəttə</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_340" n="HIAT:w" s="T101">dĭ</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_343" n="HIAT:w" s="T102">bazoʔ</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_346" n="HIAT:w" s="T103">kandəga</ts>
                  <nts id="Seg_347" n="HIAT:ip">,</nts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_350" n="HIAT:w" s="T104">dʼorlaʔbə</ts>
                  <nts id="Seg_351" n="HIAT:ip">,</nts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_354" n="HIAT:w" s="T105">dʼorlaʔbə</ts>
                  <nts id="Seg_355" n="HIAT:ip">.</nts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_358" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_360" n="HIAT:w" s="T107">A</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_363" n="HIAT:w" s="T108">dĭn</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_366" n="HIAT:w" s="T109">noʔi</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_369" n="HIAT:w" s="T110">jaʔpiʔi</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_372" n="HIAT:w" s="T111">tibizeŋ</ts>
                  <nts id="Seg_373" n="HIAT:ip">.</nts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_376" n="HIAT:u" s="T113">
                  <nts id="Seg_377" n="HIAT:ip">"</nts>
                  <ts e="T114" id="Seg_379" n="HIAT:w" s="T113">Ĭmbi</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_382" n="HIAT:w" s="T114">dʼorlaʔbəl</ts>
                  <nts id="Seg_383" n="HIAT:ip">?</nts>
                  <nts id="Seg_384" n="HIAT:ip">"</nts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_387" n="HIAT:u" s="T115">
                  <nts id="Seg_388" n="HIAT:ip">"</nts>
                  <ts e="T116" id="Seg_390" n="HIAT:w" s="T115">Da</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_393" n="HIAT:w" s="T116">măn</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_396" n="HIAT:w" s="T117">turanə</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_399" n="HIAT:w" s="T118">lisitsa</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_402" n="HIAT:w" s="T119">ej</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_405" n="HIAT:w" s="T120">öʔle</ts>
                  <nts id="Seg_406" n="HIAT:ip">"</nts>
                  <nts id="Seg_407" n="HIAT:ip">.</nts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_410" n="HIAT:u" s="T122">
                  <nts id="Seg_411" n="HIAT:ip">"</nts>
                  <ts e="T123" id="Seg_413" n="HIAT:w" s="T122">Kanžəbəj</ts>
                  <nts id="Seg_414" n="HIAT:ip">,</nts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_417" n="HIAT:w" s="T123">miʔ</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_420" n="HIAT:w" s="T124">tüjö</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_422" n="HIAT:ip">(</nts>
                  <ts e="T126" id="Seg_424" n="HIAT:w" s="T125">sürerlɨ-</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_427" n="HIAT:w" s="T126">sürerlɨ-</ts>
                  <nts id="Seg_428" n="HIAT:ip">)</nts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_430" n="HIAT:ip">(</nts>
                  <ts e="T128" id="Seg_432" n="HIAT:w" s="T127">sürerluʔpi</ts>
                  <nts id="Seg_433" n="HIAT:ip">)</nts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_436" n="HIAT:w" s="T128">miʔ</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_439" n="HIAT:w" s="T129">sapkuzi</ts>
                  <nts id="Seg_440" n="HIAT:ip">"</nts>
                  <nts id="Seg_441" n="HIAT:ip">.</nts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_444" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_446" n="HIAT:w" s="T130">Dĭgəttə</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_449" n="HIAT:w" s="T131">šobiʔi</ts>
                  <nts id="Seg_450" n="HIAT:ip">.</nts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T141" id="Seg_453" n="HIAT:u" s="T133">
                  <nts id="Seg_454" n="HIAT:ip">"</nts>
                  <ts e="T134" id="Seg_456" n="HIAT:w" s="T133">Kanaʔ</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_459" n="HIAT:w" s="T134">döʔə</ts>
                  <nts id="Seg_460" n="HIAT:ip">,</nts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_463" n="HIAT:w" s="T135">tüjö</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_466" n="HIAT:w" s="T136">šapkuzi</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_469" n="HIAT:w" s="T137">uləl</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_472" n="HIAT:w" s="T138">bar</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_475" n="HIAT:w" s="T139">jaʔləbaʔ</ts>
                  <nts id="Seg_476" n="HIAT:ip">"</nts>
                  <nts id="Seg_477" n="HIAT:ip">.</nts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T145" id="Seg_480" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_482" n="HIAT:w" s="T141">Tĭ</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_485" n="HIAT:w" s="T142">bar</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_488" n="HIAT:w" s="T143">uʔməlaʔpi</ts>
                  <nts id="Seg_489" n="HIAT:ip">.</nts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T155" id="Seg_492" n="HIAT:u" s="T145">
                  <ts e="T146" id="Seg_494" n="HIAT:w" s="T145">A</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_497" n="HIAT:w" s="T146">zajas</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_500" n="HIAT:w" s="T147">dibər</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_503" n="HIAT:w" s="T148">puʔlaʔpi</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_505" n="HIAT:ip">(</nts>
                  <ts e="T150" id="Seg_507" n="HIAT:w" s="T149">ton-</ts>
                  <nts id="Seg_508" n="HIAT:ip">)</nts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_511" n="HIAT:w" s="T150">turanə</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_514" n="HIAT:w" s="T151">i</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_517" n="HIAT:w" s="T152">tüj</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_519" n="HIAT:ip">(</nts>
                  <ts e="T155" id="Seg_521" n="HIAT:w" s="T153">am-</ts>
                  <nts id="Seg_522" n="HIAT:ip">)</nts>
                  <nts id="Seg_523" n="HIAT:ip">…</nts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T162" id="Seg_526" n="HIAT:u" s="T155">
                  <ts e="T157" id="Seg_528" n="HIAT:w" s="T155">Pʼetux</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_531" n="HIAT:w" s="T157">turanə</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_533" n="HIAT:ip">(</nts>
                  <ts e="T159" id="Seg_535" n="HIAT:w" s="T158">amolaʔpi</ts>
                  <nts id="Seg_536" n="HIAT:ip">)</nts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_539" n="HIAT:w" s="T159">i</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_542" n="HIAT:w" s="T160">tüj</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_545" n="HIAT:w" s="T161">amnolaʔbə</ts>
                  <nts id="Seg_546" n="HIAT:ip">.</nts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T162" id="Seg_548" n="sc" s="T1">
               <ts e="T2" id="Seg_550" n="e" s="T1">Amnobiʔi </ts>
               <ts e="T3" id="Seg_552" n="e" s="T2">šidogö, </ts>
               <ts e="T4" id="Seg_554" n="e" s="T3">zajas </ts>
               <ts e="T5" id="Seg_556" n="e" s="T4">i </ts>
               <ts e="T7" id="Seg_558" n="e" s="T5">pʼetux. </ts>
               <ts e="T8" id="Seg_560" n="e" s="T7">Pʼetugən </ts>
               <ts e="T9" id="Seg_562" n="e" s="T8">tura </ts>
               <ts e="T10" id="Seg_564" n="e" s="T9">ibi </ts>
               <ts e="T11" id="Seg_566" n="e" s="T10">(paʔ-) </ts>
               <ts e="T12" id="Seg_568" n="e" s="T11">paʔi </ts>
               <ts e="T13" id="Seg_570" n="e" s="T12">abi, </ts>
               <ts e="T14" id="Seg_572" n="e" s="T13">(a </ts>
               <ts e="T15" id="Seg_574" n="e" s="T14">pʼetux) </ts>
               <ts e="T16" id="Seg_576" n="e" s="T15">a </ts>
               <ts e="T17" id="Seg_578" n="e" s="T16">zajas </ts>
               <ts e="T18" id="Seg_580" n="e" s="T17">abi </ts>
               <ts e="T19" id="Seg_582" n="e" s="T18">sĭregə </ts>
               <ts e="T22" id="Seg_584" n="e" s="T19">tura. </ts>
               <ts e="T23" id="Seg_586" n="e" s="T22">Dĭgəttə </ts>
               <ts e="T24" id="Seg_588" n="e" s="T23">ejü </ts>
               <ts e="T25" id="Seg_590" n="e" s="T24">molambi, </ts>
               <ts e="T26" id="Seg_592" n="e" s="T25">kuja </ts>
               <ts e="T28" id="Seg_594" n="e" s="T26">mĭndlaʔbə. </ts>
               <ts e="T29" id="Seg_596" n="e" s="T28">Dĭ </ts>
               <ts e="T30" id="Seg_598" n="e" s="T29">zajasən </ts>
               <ts e="T31" id="Seg_600" n="e" s="T30">turat </ts>
               <ts e="T32" id="Seg_602" n="e" s="T31">bar </ts>
               <ts e="T33" id="Seg_604" n="e" s="T32">külambi, </ts>
               <ts e="T34" id="Seg_606" n="e" s="T33">(b-) </ts>
               <ts e="T35" id="Seg_608" n="e" s="T34">bü </ts>
               <ts e="T37" id="Seg_610" n="e" s="T35">molambi. </ts>
               <ts e="T38" id="Seg_612" n="e" s="T37">Dibər </ts>
               <ts e="T39" id="Seg_614" n="e" s="T38">dʼăʔpi </ts>
               <ts e="T40" id="Seg_616" n="e" s="T39">molambi, </ts>
               <ts e="T41" id="Seg_618" n="e" s="T40">pʼetuxtə </ts>
               <ts e="T43" id="Seg_620" n="e" s="T41">mănde: </ts>
               <ts e="T44" id="Seg_622" n="e" s="T43">"Öʔleʔ </ts>
               <ts e="T45" id="Seg_624" n="e" s="T44">măna, </ts>
               <ts e="T46" id="Seg_626" n="e" s="T45">măn </ts>
               <ts e="T47" id="Seg_628" n="e" s="T46">ej </ts>
               <ts e="T48" id="Seg_630" n="e" s="T47">umlem, </ts>
               <ts e="T49" id="Seg_632" n="e" s="T48">kolam </ts>
               <ts e="T51" id="Seg_634" n="e" s="T49">tănan". </ts>
               <ts e="T52" id="Seg_636" n="e" s="T51">Dĭ </ts>
               <ts e="T53" id="Seg_638" n="e" s="T52">öʔlubi, </ts>
               <ts e="T54" id="Seg_640" n="e" s="T53">dĭ </ts>
               <ts e="T55" id="Seg_642" n="e" s="T54">dĭm </ts>
               <ts e="T57" id="Seg_644" n="e" s="T55">sürerluʔpi. </ts>
               <ts e="T58" id="Seg_646" n="e" s="T57">Dĭ </ts>
               <ts e="T59" id="Seg_648" n="e" s="T58">kandəga, </ts>
               <ts e="T60" id="Seg_650" n="e" s="T59">kandəga, </ts>
               <ts e="T61" id="Seg_652" n="e" s="T60">dʼorlaʔbə </ts>
               <ts e="T63" id="Seg_654" n="e" s="T61">bar. </ts>
               <ts e="T64" id="Seg_656" n="e" s="T63">Dĭgəttə </ts>
               <ts e="T65" id="Seg_658" n="e" s="T64">šonəga </ts>
               <ts e="T66" id="Seg_660" n="e" s="T65">diʔnə </ts>
               <ts e="T67" id="Seg_662" n="e" s="T66">(li-) </ts>
               <ts e="T69" id="Seg_664" n="e" s="T67">lisitsa. </ts>
               <ts e="T70" id="Seg_666" n="e" s="T69">"Ĭmbi </ts>
               <ts e="T72" id="Seg_668" n="e" s="T70">dʼorlaʔbəl?" </ts>
               <ts e="T73" id="Seg_670" n="e" s="T72">"Da </ts>
               <ts e="T74" id="Seg_672" n="e" s="T73">măn </ts>
               <ts e="T75" id="Seg_674" n="e" s="T74">turagə </ts>
               <ts e="T76" id="Seg_676" n="e" s="T75">(za-) </ts>
               <ts e="T77" id="Seg_678" n="e" s="T76">zajas </ts>
               <ts e="T78" id="Seg_680" n="e" s="T77">amnolaʔbə, </ts>
               <ts e="T79" id="Seg_682" n="e" s="T78">măna </ts>
               <ts e="T81" id="Seg_684" n="e" s="T79">sürerluʔbi". </ts>
               <ts e="T82" id="Seg_686" n="e" s="T81">Dĭ </ts>
               <ts e="T83" id="Seg_688" n="e" s="T82">mănde: </ts>
               <ts e="T84" id="Seg_690" n="e" s="T83">"Kanžəbəj, </ts>
               <ts e="T85" id="Seg_692" n="e" s="T84">măn </ts>
               <ts e="T86" id="Seg_694" n="e" s="T85">dĭm </ts>
               <ts e="T88" id="Seg_696" n="e" s="T86">sürerlʼim". </ts>
               <ts e="T89" id="Seg_698" n="e" s="T88">Dĭgəttə </ts>
               <ts e="T90" id="Seg_700" n="e" s="T89">šobiʔi, </ts>
               <ts e="T91" id="Seg_702" n="e" s="T90">(š-) </ts>
               <ts e="T92" id="Seg_704" n="e" s="T91">sürerbi </ts>
               <ts e="T93" id="Seg_706" n="e" s="T92">zajasəm. </ts>
               <ts e="T94" id="Seg_708" n="e" s="T93">I </ts>
               <ts e="T95" id="Seg_710" n="e" s="T94">bostə </ts>
               <ts e="T96" id="Seg_712" n="e" s="T95">amnolaʔbə, </ts>
               <ts e="T97" id="Seg_714" n="e" s="T96">(ej=) </ts>
               <ts e="T98" id="Seg_716" n="e" s="T97">ej </ts>
               <ts e="T99" id="Seg_718" n="e" s="T98">öʔle </ts>
               <ts e="T100" id="Seg_720" n="e" s="T99">pʼetugəm. </ts>
               <ts e="T101" id="Seg_722" n="e" s="T100">Dĭgəttə </ts>
               <ts e="T102" id="Seg_724" n="e" s="T101">dĭ </ts>
               <ts e="T103" id="Seg_726" n="e" s="T102">bazoʔ </ts>
               <ts e="T104" id="Seg_728" n="e" s="T103">kandəga, </ts>
               <ts e="T105" id="Seg_730" n="e" s="T104">dʼorlaʔbə, </ts>
               <ts e="T107" id="Seg_732" n="e" s="T105">dʼorlaʔbə. </ts>
               <ts e="T108" id="Seg_734" n="e" s="T107">A </ts>
               <ts e="T109" id="Seg_736" n="e" s="T108">dĭn </ts>
               <ts e="T110" id="Seg_738" n="e" s="T109">noʔi </ts>
               <ts e="T111" id="Seg_740" n="e" s="T110">jaʔpiʔi </ts>
               <ts e="T113" id="Seg_742" n="e" s="T111">tibizeŋ. </ts>
               <ts e="T114" id="Seg_744" n="e" s="T113">"Ĭmbi </ts>
               <ts e="T115" id="Seg_746" n="e" s="T114">dʼorlaʔbəl?" </ts>
               <ts e="T116" id="Seg_748" n="e" s="T115">"Da </ts>
               <ts e="T117" id="Seg_750" n="e" s="T116">măn </ts>
               <ts e="T118" id="Seg_752" n="e" s="T117">turanə </ts>
               <ts e="T119" id="Seg_754" n="e" s="T118">lisitsa </ts>
               <ts e="T120" id="Seg_756" n="e" s="T119">ej </ts>
               <ts e="T122" id="Seg_758" n="e" s="T120">öʔle". </ts>
               <ts e="T123" id="Seg_760" n="e" s="T122">"Kanžəbəj, </ts>
               <ts e="T124" id="Seg_762" n="e" s="T123">miʔ </ts>
               <ts e="T125" id="Seg_764" n="e" s="T124">tüjö </ts>
               <ts e="T126" id="Seg_766" n="e" s="T125">(sürerlɨ- </ts>
               <ts e="T127" id="Seg_768" n="e" s="T126">sürerlɨ-) </ts>
               <ts e="T128" id="Seg_770" n="e" s="T127">(sürerluʔpi) </ts>
               <ts e="T129" id="Seg_772" n="e" s="T128">miʔ </ts>
               <ts e="T130" id="Seg_774" n="e" s="T129">sapkuzi". </ts>
               <ts e="T131" id="Seg_776" n="e" s="T130">Dĭgəttə </ts>
               <ts e="T133" id="Seg_778" n="e" s="T131">šobiʔi. </ts>
               <ts e="T134" id="Seg_780" n="e" s="T133">"Kanaʔ </ts>
               <ts e="T135" id="Seg_782" n="e" s="T134">döʔə, </ts>
               <ts e="T136" id="Seg_784" n="e" s="T135">tüjö </ts>
               <ts e="T137" id="Seg_786" n="e" s="T136">šapkuzi </ts>
               <ts e="T138" id="Seg_788" n="e" s="T137">uləl </ts>
               <ts e="T139" id="Seg_790" n="e" s="T138">bar </ts>
               <ts e="T141" id="Seg_792" n="e" s="T139">jaʔləbaʔ". </ts>
               <ts e="T142" id="Seg_794" n="e" s="T141">Tĭ </ts>
               <ts e="T143" id="Seg_796" n="e" s="T142">bar </ts>
               <ts e="T145" id="Seg_798" n="e" s="T143">uʔməlaʔpi. </ts>
               <ts e="T146" id="Seg_800" n="e" s="T145">A </ts>
               <ts e="T147" id="Seg_802" n="e" s="T146">zajas </ts>
               <ts e="T148" id="Seg_804" n="e" s="T147">dibər </ts>
               <ts e="T149" id="Seg_806" n="e" s="T148">puʔlaʔpi </ts>
               <ts e="T150" id="Seg_808" n="e" s="T149">(ton-) </ts>
               <ts e="T151" id="Seg_810" n="e" s="T150">turanə </ts>
               <ts e="T152" id="Seg_812" n="e" s="T151">i </ts>
               <ts e="T153" id="Seg_814" n="e" s="T152">tüj </ts>
               <ts e="T155" id="Seg_816" n="e" s="T153">(am-)… </ts>
               <ts e="T157" id="Seg_818" n="e" s="T155">Pʼetux </ts>
               <ts e="T158" id="Seg_820" n="e" s="T157">turanə </ts>
               <ts e="T159" id="Seg_822" n="e" s="T158">(amolaʔpi) </ts>
               <ts e="T160" id="Seg_824" n="e" s="T159">i </ts>
               <ts e="T161" id="Seg_826" n="e" s="T160">tüj </ts>
               <ts e="T162" id="Seg_828" n="e" s="T161">amnolaʔbə. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T7" id="Seg_829" s="T1">PKZ_196X_HareAndRooster_flk.001 (001)</ta>
            <ta e="T22" id="Seg_830" s="T7">PKZ_196X_HareAndRooster_flk.002 (002)</ta>
            <ta e="T28" id="Seg_831" s="T22">PKZ_196X_HareAndRooster_flk.003 (003)</ta>
            <ta e="T37" id="Seg_832" s="T28">PKZ_196X_HareAndRooster_flk.004 (004)</ta>
            <ta e="T51" id="Seg_833" s="T37">PKZ_196X_HareAndRooster_flk.005 (005)</ta>
            <ta e="T57" id="Seg_834" s="T51">PKZ_196X_HareAndRooster_flk.006 (007)</ta>
            <ta e="T63" id="Seg_835" s="T57">PKZ_196X_HareAndRooster_flk.007 (008)</ta>
            <ta e="T69" id="Seg_836" s="T63">PKZ_196X_HareAndRooster_flk.008 (009)</ta>
            <ta e="T72" id="Seg_837" s="T69">PKZ_196X_HareAndRooster_flk.009 (010)</ta>
            <ta e="T81" id="Seg_838" s="T72">PKZ_196X_HareAndRooster_flk.010 (011)</ta>
            <ta e="T88" id="Seg_839" s="T81">PKZ_196X_HareAndRooster_flk.011 (012)</ta>
            <ta e="T93" id="Seg_840" s="T88">PKZ_196X_HareAndRooster_flk.012 (013)</ta>
            <ta e="T100" id="Seg_841" s="T93">PKZ_196X_HareAndRooster_flk.013 (014)</ta>
            <ta e="T107" id="Seg_842" s="T100">PKZ_196X_HareAndRooster_flk.014 (015)</ta>
            <ta e="T113" id="Seg_843" s="T107">PKZ_196X_HareAndRooster_flk.015 (016)</ta>
            <ta e="T115" id="Seg_844" s="T113">PKZ_196X_HareAndRooster_flk.016 (017)</ta>
            <ta e="T122" id="Seg_845" s="T115">PKZ_196X_HareAndRooster_flk.017 (018)</ta>
            <ta e="T130" id="Seg_846" s="T122">PKZ_196X_HareAndRooster_flk.018 (019)</ta>
            <ta e="T133" id="Seg_847" s="T130">PKZ_196X_HareAndRooster_flk.019 (020)</ta>
            <ta e="T141" id="Seg_848" s="T133">PKZ_196X_HareAndRooster_flk.020 (021)</ta>
            <ta e="T145" id="Seg_849" s="T141">PKZ_196X_HareAndRooster_flk.021 (022)</ta>
            <ta e="T155" id="Seg_850" s="T145">PKZ_196X_HareAndRooster_flk.022 (023)</ta>
            <ta e="T162" id="Seg_851" s="T155">PKZ_196X_HareAndRooster_flk.023 (024)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T7" id="Seg_852" s="T1">Amnobiʔi šidogö, zajas i pʼetux. </ta>
            <ta e="T22" id="Seg_853" s="T7">Pʼetugən tura ibi (paʔ-) paʔi abi, (a pʼetux) a zajas abi sĭregə tura. </ta>
            <ta e="T28" id="Seg_854" s="T22">Dĭgəttə ejü molambi, kuja mĭndlaʔbə. </ta>
            <ta e="T37" id="Seg_855" s="T28">Dĭ zajasən turat bar külambi, (b-) bü molambi. </ta>
            <ta e="T51" id="Seg_856" s="T37">Dibər dʼăʔpi molambi, pʼetuxtə mănde: "Öʔleʔ măna, măn ej umlem, kolam tănan". </ta>
            <ta e="T57" id="Seg_857" s="T51">Dĭ öʔlubi, dĭ dĭm sürerluʔpi. </ta>
            <ta e="T63" id="Seg_858" s="T57">Dĭ kandəga, kandəga, dʼorlaʔbə bar. </ta>
            <ta e="T69" id="Seg_859" s="T63">Dĭgəttə šonəga diʔnə (li-) lisitsa. </ta>
            <ta e="T72" id="Seg_860" s="T69">"Ĭmbi dʼorlaʔbəl?" </ta>
            <ta e="T81" id="Seg_861" s="T72">"Da măn turagə (za-) zajas amnolaʔbə, măna sürerluʔbi". </ta>
            <ta e="T88" id="Seg_862" s="T81">Dĭ mănde: "Kanžəbəj, măn dĭm sürerlʼim". </ta>
            <ta e="T93" id="Seg_863" s="T88">Dĭgəttə šobiʔi, (š-) sürerbi zajasəm. </ta>
            <ta e="T100" id="Seg_864" s="T93">I bostə amnolaʔbə, (ej=) ej öʔle pʼetugəm. </ta>
            <ta e="T107" id="Seg_865" s="T100">Dĭgəttə dĭ bazoʔ kandəga, dʼorlaʔbə, dʼorlaʔbə. </ta>
            <ta e="T113" id="Seg_866" s="T107">A dĭn noʔi jaʔpiʔi tibizeŋ. </ta>
            <ta e="T115" id="Seg_867" s="T113">"Ĭmbi dʼorlaʔbəl?" </ta>
            <ta e="T122" id="Seg_868" s="T115">"Da măn turanə lisitsa ej öʔle". </ta>
            <ta e="T130" id="Seg_869" s="T122">"Kanžəbəj, miʔ tüjö (sürerlɨ- sürerlɨ-) (sürerluʔpi) miʔ sapkuzi". </ta>
            <ta e="T133" id="Seg_870" s="T130">Dĭgəttə šobiʔi. </ta>
            <ta e="T141" id="Seg_871" s="T133">"Kanaʔ döʔə, tüjö šapkuzi uləl bar jaʔləbaʔ". </ta>
            <ta e="T145" id="Seg_872" s="T141">Tĭ bar uʔməlaʔpi. </ta>
            <ta e="T155" id="Seg_873" s="T145">A zajas dibər puʔlaʔpi (ton-) turanə i tüj (am-)…</ta>
            <ta e="T162" id="Seg_874" s="T155">Pʼetux turanə (amolaʔpi) i tüj amnolaʔbə. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_875" s="T1">amno-bi-ʔi</ta>
            <ta e="T3" id="Seg_876" s="T2">šido-gö</ta>
            <ta e="T4" id="Seg_877" s="T3">zajas</ta>
            <ta e="T5" id="Seg_878" s="T4">i</ta>
            <ta e="T7" id="Seg_879" s="T5">pʼetux</ta>
            <ta e="T8" id="Seg_880" s="T7">pʼetug-ə-n</ta>
            <ta e="T9" id="Seg_881" s="T8">tura</ta>
            <ta e="T10" id="Seg_882" s="T9">i-bi</ta>
            <ta e="T12" id="Seg_883" s="T11">pa-ʔi</ta>
            <ta e="T13" id="Seg_884" s="T12">a-bi</ta>
            <ta e="T14" id="Seg_885" s="T13">a</ta>
            <ta e="T15" id="Seg_886" s="T14">pʼetux</ta>
            <ta e="T16" id="Seg_887" s="T15">a</ta>
            <ta e="T17" id="Seg_888" s="T16">zajas</ta>
            <ta e="T18" id="Seg_889" s="T17">a-bi</ta>
            <ta e="T19" id="Seg_890" s="T18">sĭre-gə</ta>
            <ta e="T22" id="Seg_891" s="T19">tura</ta>
            <ta e="T23" id="Seg_892" s="T22">dĭgəttə</ta>
            <ta e="T24" id="Seg_893" s="T23">ejü</ta>
            <ta e="T25" id="Seg_894" s="T24">mo-lam-bi</ta>
            <ta e="T26" id="Seg_895" s="T25">kuja</ta>
            <ta e="T28" id="Seg_896" s="T26">mĭnd-laʔbə</ta>
            <ta e="T29" id="Seg_897" s="T28">dĭ</ta>
            <ta e="T30" id="Seg_898" s="T29">zajas-ə-n</ta>
            <ta e="T31" id="Seg_899" s="T30">tura-t</ta>
            <ta e="T32" id="Seg_900" s="T31">bar</ta>
            <ta e="T33" id="Seg_901" s="T32">kü-lam-bi</ta>
            <ta e="T35" id="Seg_902" s="T34">bü</ta>
            <ta e="T37" id="Seg_903" s="T35">mo-lam-bi</ta>
            <ta e="T38" id="Seg_904" s="T37">dibər</ta>
            <ta e="T39" id="Seg_905" s="T38">dʼăʔpi</ta>
            <ta e="T40" id="Seg_906" s="T39">mo-lam-bi</ta>
            <ta e="T41" id="Seg_907" s="T40">pʼetux-tə</ta>
            <ta e="T43" id="Seg_908" s="T41">măn-de</ta>
            <ta e="T44" id="Seg_909" s="T43">öʔ-leʔ</ta>
            <ta e="T45" id="Seg_910" s="T44">măna</ta>
            <ta e="T46" id="Seg_911" s="T45">măn</ta>
            <ta e="T47" id="Seg_912" s="T46">ej</ta>
            <ta e="T48" id="Seg_913" s="T47">um-le-m</ta>
            <ta e="T49" id="Seg_914" s="T48">ko-la-m</ta>
            <ta e="T51" id="Seg_915" s="T49">tănan</ta>
            <ta e="T52" id="Seg_916" s="T51">dĭ</ta>
            <ta e="T53" id="Seg_917" s="T52">öʔlu-bi</ta>
            <ta e="T54" id="Seg_918" s="T53">dĭ</ta>
            <ta e="T55" id="Seg_919" s="T54">dĭ-m</ta>
            <ta e="T57" id="Seg_920" s="T55">sürer-luʔ-pi</ta>
            <ta e="T58" id="Seg_921" s="T57">dĭ</ta>
            <ta e="T59" id="Seg_922" s="T58">kandə-ga</ta>
            <ta e="T60" id="Seg_923" s="T59">kandə-ga</ta>
            <ta e="T61" id="Seg_924" s="T60">dʼor-laʔbə</ta>
            <ta e="T63" id="Seg_925" s="T61">bar</ta>
            <ta e="T64" id="Seg_926" s="T63">dĭgəttə</ta>
            <ta e="T65" id="Seg_927" s="T64">šonə-ga</ta>
            <ta e="T66" id="Seg_928" s="T65">diʔ-nə</ta>
            <ta e="T69" id="Seg_929" s="T67">lisitsa</ta>
            <ta e="T70" id="Seg_930" s="T69">ĭmbi</ta>
            <ta e="T72" id="Seg_931" s="T70">dʼor-laʔbə-l</ta>
            <ta e="T73" id="Seg_932" s="T72">da</ta>
            <ta e="T74" id="Seg_933" s="T73">măn</ta>
            <ta e="T75" id="Seg_934" s="T74">tura-gə</ta>
            <ta e="T77" id="Seg_935" s="T76">zajas</ta>
            <ta e="T78" id="Seg_936" s="T77">amno-laʔbə</ta>
            <ta e="T79" id="Seg_937" s="T78">măna</ta>
            <ta e="T81" id="Seg_938" s="T79">sürer-luʔ-bi</ta>
            <ta e="T82" id="Seg_939" s="T81">dĭ</ta>
            <ta e="T83" id="Seg_940" s="T82">măn-de</ta>
            <ta e="T84" id="Seg_941" s="T83">kan-žə-bəj</ta>
            <ta e="T85" id="Seg_942" s="T84">măn</ta>
            <ta e="T86" id="Seg_943" s="T85">dĭ-m</ta>
            <ta e="T88" id="Seg_944" s="T86">sürer-lʼi-m</ta>
            <ta e="T89" id="Seg_945" s="T88">dĭgəttə</ta>
            <ta e="T90" id="Seg_946" s="T89">šo-bi-ʔi</ta>
            <ta e="T92" id="Seg_947" s="T91">sürer-bi</ta>
            <ta e="T93" id="Seg_948" s="T92">zajas-ə-m</ta>
            <ta e="T94" id="Seg_949" s="T93">i</ta>
            <ta e="T95" id="Seg_950" s="T94">bos-tə</ta>
            <ta e="T96" id="Seg_951" s="T95">amno-laʔbə</ta>
            <ta e="T97" id="Seg_952" s="T96">ej</ta>
            <ta e="T98" id="Seg_953" s="T97">ej</ta>
            <ta e="T99" id="Seg_954" s="T98">öʔ-le</ta>
            <ta e="T100" id="Seg_955" s="T99">pʼetug-ə-m</ta>
            <ta e="T101" id="Seg_956" s="T100">dĭgəttə</ta>
            <ta e="T102" id="Seg_957" s="T101">dĭ</ta>
            <ta e="T103" id="Seg_958" s="T102">bazoʔ</ta>
            <ta e="T104" id="Seg_959" s="T103">kandə-ga</ta>
            <ta e="T105" id="Seg_960" s="T104">dʼor-laʔbə</ta>
            <ta e="T107" id="Seg_961" s="T105">dʼor-laʔbə</ta>
            <ta e="T108" id="Seg_962" s="T107">a</ta>
            <ta e="T109" id="Seg_963" s="T108">dĭn</ta>
            <ta e="T110" id="Seg_964" s="T109">no-ʔi</ta>
            <ta e="T111" id="Seg_965" s="T110">jaʔ-pi-ʔi</ta>
            <ta e="T113" id="Seg_966" s="T111">tibi-zeŋ</ta>
            <ta e="T114" id="Seg_967" s="T113">ĭmbi</ta>
            <ta e="T115" id="Seg_968" s="T114">dʼor-laʔbə-l</ta>
            <ta e="T116" id="Seg_969" s="T115">da</ta>
            <ta e="T117" id="Seg_970" s="T116">măn</ta>
            <ta e="T118" id="Seg_971" s="T117">tura-nə</ta>
            <ta e="T119" id="Seg_972" s="T118">lisitsa</ta>
            <ta e="T120" id="Seg_973" s="T119">ej</ta>
            <ta e="T122" id="Seg_974" s="T120">öʔ-le</ta>
            <ta e="T123" id="Seg_975" s="T122">kan-žə-bəj</ta>
            <ta e="T124" id="Seg_976" s="T123">miʔ</ta>
            <ta e="T125" id="Seg_977" s="T124">tüjö</ta>
            <ta e="T128" id="Seg_978" s="T127">sürer-luʔ-pi</ta>
            <ta e="T129" id="Seg_979" s="T128">miʔ</ta>
            <ta e="T130" id="Seg_980" s="T129">sapku-zi</ta>
            <ta e="T131" id="Seg_981" s="T130">dĭgəttə</ta>
            <ta e="T133" id="Seg_982" s="T131">šo-bi-ʔi</ta>
            <ta e="T134" id="Seg_983" s="T133">kan-a-ʔ</ta>
            <ta e="T135" id="Seg_984" s="T134">döʔə</ta>
            <ta e="T136" id="Seg_985" s="T135">tüjö</ta>
            <ta e="T137" id="Seg_986" s="T136">šapku-zi</ta>
            <ta e="T138" id="Seg_987" s="T137">ulə-l</ta>
            <ta e="T139" id="Seg_988" s="T138">bar</ta>
            <ta e="T141" id="Seg_989" s="T139">jaʔ-lə-baʔ</ta>
            <ta e="T142" id="Seg_990" s="T141">tĭ</ta>
            <ta e="T143" id="Seg_991" s="T142">bar</ta>
            <ta e="T145" id="Seg_992" s="T143">uʔmə-laʔpi</ta>
            <ta e="T146" id="Seg_993" s="T145">a</ta>
            <ta e="T147" id="Seg_994" s="T146">zajas</ta>
            <ta e="T148" id="Seg_995" s="T147">dibər</ta>
            <ta e="T149" id="Seg_996" s="T148">puʔ-laʔpi</ta>
            <ta e="T151" id="Seg_997" s="T150">tura-nə</ta>
            <ta e="T152" id="Seg_998" s="T151">i</ta>
            <ta e="T153" id="Seg_999" s="T152">tüj</ta>
            <ta e="T155" id="Seg_1000" s="T153">am</ta>
            <ta e="T157" id="Seg_1001" s="T155">pʼetux</ta>
            <ta e="T158" id="Seg_1002" s="T157">tura-nə</ta>
            <ta e="T159" id="Seg_1003" s="T158">amo-laʔpi</ta>
            <ta e="T160" id="Seg_1004" s="T159">i</ta>
            <ta e="T161" id="Seg_1005" s="T160">tüj</ta>
            <ta e="T162" id="Seg_1006" s="T161">amno-laʔbə</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_1007" s="T1">amno-bi-jəʔ</ta>
            <ta e="T3" id="Seg_1008" s="T2">šide-göʔ</ta>
            <ta e="T4" id="Seg_1009" s="T3">zajats</ta>
            <ta e="T5" id="Seg_1010" s="T4">i</ta>
            <ta e="T7" id="Seg_1011" s="T5">pʼetux</ta>
            <ta e="T8" id="Seg_1012" s="T7">pʼetux-ə-n</ta>
            <ta e="T9" id="Seg_1013" s="T8">tura</ta>
            <ta e="T10" id="Seg_1014" s="T9">i-bi</ta>
            <ta e="T12" id="Seg_1015" s="T11">pa-jəʔ</ta>
            <ta e="T13" id="Seg_1016" s="T12">a-bi</ta>
            <ta e="T14" id="Seg_1017" s="T13">a</ta>
            <ta e="T15" id="Seg_1018" s="T14">pʼetux</ta>
            <ta e="T16" id="Seg_1019" s="T15">a</ta>
            <ta e="T17" id="Seg_1020" s="T16">zajats</ta>
            <ta e="T18" id="Seg_1021" s="T17">a-bi</ta>
            <ta e="T19" id="Seg_1022" s="T18">sĭri-gəʔ</ta>
            <ta e="T22" id="Seg_1023" s="T19">tura</ta>
            <ta e="T23" id="Seg_1024" s="T22">dĭgəttə</ta>
            <ta e="T24" id="Seg_1025" s="T23">ejü</ta>
            <ta e="T25" id="Seg_1026" s="T24">mo-laːm-bi</ta>
            <ta e="T26" id="Seg_1027" s="T25">kuja</ta>
            <ta e="T28" id="Seg_1028" s="T26">mĭn-laʔbə</ta>
            <ta e="T29" id="Seg_1029" s="T28">dĭ</ta>
            <ta e="T30" id="Seg_1030" s="T29">zajats-ə-n</ta>
            <ta e="T31" id="Seg_1031" s="T30">tura-t</ta>
            <ta e="T32" id="Seg_1032" s="T31">bar</ta>
            <ta e="T33" id="Seg_1033" s="T32">kü-laːm-bi</ta>
            <ta e="T35" id="Seg_1034" s="T34">bü</ta>
            <ta e="T37" id="Seg_1035" s="T35">mo-laːm-bi</ta>
            <ta e="T38" id="Seg_1036" s="T37">dĭbər</ta>
            <ta e="T39" id="Seg_1037" s="T38">dʼüʔpi</ta>
            <ta e="T40" id="Seg_1038" s="T39">mo-laːm-bi</ta>
            <ta e="T41" id="Seg_1039" s="T40">pʼetux-Tə</ta>
            <ta e="T43" id="Seg_1040" s="T41">măn-ntə</ta>
            <ta e="T44" id="Seg_1041" s="T43">öʔ-lAʔ</ta>
            <ta e="T45" id="Seg_1042" s="T44">măna</ta>
            <ta e="T46" id="Seg_1043" s="T45">măn</ta>
            <ta e="T47" id="Seg_1044" s="T46">ej</ta>
            <ta e="T48" id="Seg_1045" s="T47">üʔmə-lV-m</ta>
            <ta e="T49" id="Seg_1046" s="T48">koː-lV-m</ta>
            <ta e="T51" id="Seg_1047" s="T49">tănan</ta>
            <ta e="T52" id="Seg_1048" s="T51">dĭ</ta>
            <ta e="T53" id="Seg_1049" s="T52">öʔlu-bi</ta>
            <ta e="T54" id="Seg_1050" s="T53">dĭ</ta>
            <ta e="T55" id="Seg_1051" s="T54">dĭ-m</ta>
            <ta e="T57" id="Seg_1052" s="T55">sürer-luʔbdə-bi</ta>
            <ta e="T58" id="Seg_1053" s="T57">dĭ</ta>
            <ta e="T59" id="Seg_1054" s="T58">kandə-gA</ta>
            <ta e="T60" id="Seg_1055" s="T59">kandə-gA</ta>
            <ta e="T61" id="Seg_1056" s="T60">tʼor-laʔbə</ta>
            <ta e="T63" id="Seg_1057" s="T61">bar</ta>
            <ta e="T64" id="Seg_1058" s="T63">dĭgəttə</ta>
            <ta e="T65" id="Seg_1059" s="T64">šonə-gA</ta>
            <ta e="T66" id="Seg_1060" s="T65">dĭ-Tə</ta>
            <ta e="T69" id="Seg_1061" s="T67">lʼisʼitsa</ta>
            <ta e="T70" id="Seg_1062" s="T69">ĭmbi</ta>
            <ta e="T72" id="Seg_1063" s="T70">tʼor-laʔbə-l</ta>
            <ta e="T73" id="Seg_1064" s="T72">da</ta>
            <ta e="T74" id="Seg_1065" s="T73">măn</ta>
            <ta e="T75" id="Seg_1066" s="T74">tura-gəʔ</ta>
            <ta e="T77" id="Seg_1067" s="T76">zajats</ta>
            <ta e="T78" id="Seg_1068" s="T77">amno-laʔbə</ta>
            <ta e="T79" id="Seg_1069" s="T78">măna</ta>
            <ta e="T81" id="Seg_1070" s="T79">sürer-luʔbdə-bi</ta>
            <ta e="T82" id="Seg_1071" s="T81">dĭ</ta>
            <ta e="T83" id="Seg_1072" s="T82">măn-ntə</ta>
            <ta e="T84" id="Seg_1073" s="T83">kan-žə-bəj</ta>
            <ta e="T85" id="Seg_1074" s="T84">măn</ta>
            <ta e="T86" id="Seg_1075" s="T85">dĭ-m</ta>
            <ta e="T88" id="Seg_1076" s="T86">sürer-lV-m</ta>
            <ta e="T89" id="Seg_1077" s="T88">dĭgəttə</ta>
            <ta e="T90" id="Seg_1078" s="T89">šo-bi-jəʔ</ta>
            <ta e="T92" id="Seg_1079" s="T91">sürer-bi</ta>
            <ta e="T93" id="Seg_1080" s="T92">zajats-ə-m</ta>
            <ta e="T94" id="Seg_1081" s="T93">i</ta>
            <ta e="T95" id="Seg_1082" s="T94">bos-də</ta>
            <ta e="T96" id="Seg_1083" s="T95">amno-laʔbə</ta>
            <ta e="T97" id="Seg_1084" s="T96">ej</ta>
            <ta e="T98" id="Seg_1085" s="T97">ej</ta>
            <ta e="T99" id="Seg_1086" s="T98">öʔ-lAʔ</ta>
            <ta e="T100" id="Seg_1087" s="T99">pʼetux-ə-m</ta>
            <ta e="T101" id="Seg_1088" s="T100">dĭgəttə</ta>
            <ta e="T102" id="Seg_1089" s="T101">dĭ</ta>
            <ta e="T103" id="Seg_1090" s="T102">bazoʔ</ta>
            <ta e="T104" id="Seg_1091" s="T103">kandə-gA</ta>
            <ta e="T105" id="Seg_1092" s="T104">tʼor-laʔbə</ta>
            <ta e="T107" id="Seg_1093" s="T105">tʼor-laʔbə</ta>
            <ta e="T108" id="Seg_1094" s="T107">a</ta>
            <ta e="T109" id="Seg_1095" s="T108">dĭn</ta>
            <ta e="T110" id="Seg_1096" s="T109">noʔ-jəʔ</ta>
            <ta e="T111" id="Seg_1097" s="T110">hʼaʔ-bi-jəʔ</ta>
            <ta e="T113" id="Seg_1098" s="T111">tibi-zAŋ</ta>
            <ta e="T114" id="Seg_1099" s="T113">ĭmbi</ta>
            <ta e="T115" id="Seg_1100" s="T114">tʼor-laʔbə-l</ta>
            <ta e="T116" id="Seg_1101" s="T115">da</ta>
            <ta e="T117" id="Seg_1102" s="T116">măn</ta>
            <ta e="T118" id="Seg_1103" s="T117">tura-Tə</ta>
            <ta e="T119" id="Seg_1104" s="T118">lʼisʼitsa</ta>
            <ta e="T120" id="Seg_1105" s="T119">ej</ta>
            <ta e="T122" id="Seg_1106" s="T120">öʔ-lAʔ</ta>
            <ta e="T123" id="Seg_1107" s="T122">kan-žə-bəj</ta>
            <ta e="T124" id="Seg_1108" s="T123">miʔ</ta>
            <ta e="T125" id="Seg_1109" s="T124">tüjö</ta>
            <ta e="T128" id="Seg_1110" s="T127">sürer-luʔbdə-bi</ta>
            <ta e="T129" id="Seg_1111" s="T128">miʔ</ta>
            <ta e="T130" id="Seg_1112" s="T129">šapku-ziʔ</ta>
            <ta e="T131" id="Seg_1113" s="T130">dĭgəttə</ta>
            <ta e="T133" id="Seg_1114" s="T131">šo-bi-jəʔ</ta>
            <ta e="T134" id="Seg_1115" s="T133">kan-ə-ʔ</ta>
            <ta e="T135" id="Seg_1116" s="T134">döʔə</ta>
            <ta e="T136" id="Seg_1117" s="T135">tüjö</ta>
            <ta e="T137" id="Seg_1118" s="T136">šapku-ziʔ</ta>
            <ta e="T138" id="Seg_1119" s="T137">ulu-l</ta>
            <ta e="T139" id="Seg_1120" s="T138">bar</ta>
            <ta e="T141" id="Seg_1121" s="T139">hʼaʔ-lV-bAʔ</ta>
            <ta e="T142" id="Seg_1122" s="T141">dĭ</ta>
            <ta e="T143" id="Seg_1123" s="T142">bar</ta>
            <ta e="T145" id="Seg_1124" s="T143">üʔmə-laʔpi</ta>
            <ta e="T146" id="Seg_1125" s="T145">a</ta>
            <ta e="T147" id="Seg_1126" s="T146">zajats</ta>
            <ta e="T148" id="Seg_1127" s="T147">dĭbər</ta>
            <ta e="T149" id="Seg_1128" s="T148">puʔ-laʔpi</ta>
            <ta e="T151" id="Seg_1129" s="T150">tura-Tə</ta>
            <ta e="T152" id="Seg_1130" s="T151">i</ta>
            <ta e="T153" id="Seg_1131" s="T152">tüj</ta>
            <ta e="T157" id="Seg_1132" s="T155">pʼetux</ta>
            <ta e="T158" id="Seg_1133" s="T157">tura-Tə</ta>
            <ta e="T159" id="Seg_1134" s="T158">amo-laʔpi</ta>
            <ta e="T160" id="Seg_1135" s="T159">i</ta>
            <ta e="T161" id="Seg_1136" s="T160">tüj</ta>
            <ta e="T162" id="Seg_1137" s="T161">amno-laʔbə</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_1138" s="T1">live-PST-3PL</ta>
            <ta e="T3" id="Seg_1139" s="T2">two-COLL</ta>
            <ta e="T4" id="Seg_1140" s="T3">hare.[NOM.SG]</ta>
            <ta e="T5" id="Seg_1141" s="T4">and</ta>
            <ta e="T7" id="Seg_1142" s="T5">rooster.[NOM.SG]</ta>
            <ta e="T8" id="Seg_1143" s="T7">rooster-EP-GEN</ta>
            <ta e="T9" id="Seg_1144" s="T8">house.[NOM.SG]</ta>
            <ta e="T10" id="Seg_1145" s="T9">be-PST.[3SG]</ta>
            <ta e="T12" id="Seg_1146" s="T11">tree-PL</ta>
            <ta e="T13" id="Seg_1147" s="T12">make-PST.[3SG]</ta>
            <ta e="T14" id="Seg_1148" s="T13">and</ta>
            <ta e="T15" id="Seg_1149" s="T14">rooster.[NOM.SG]</ta>
            <ta e="T16" id="Seg_1150" s="T15">and</ta>
            <ta e="T17" id="Seg_1151" s="T16">hare.[NOM.SG]</ta>
            <ta e="T18" id="Seg_1152" s="T17">make-PST.[3SG]</ta>
            <ta e="T19" id="Seg_1153" s="T18">snow-ABL</ta>
            <ta e="T22" id="Seg_1154" s="T19">house.[NOM.SG]</ta>
            <ta e="T23" id="Seg_1155" s="T22">then</ta>
            <ta e="T24" id="Seg_1156" s="T23">warm.[NOM.SG]</ta>
            <ta e="T25" id="Seg_1157" s="T24">become-RES-PST.[3SG]</ta>
            <ta e="T26" id="Seg_1158" s="T25">sun.[NOM.SG]</ta>
            <ta e="T28" id="Seg_1159" s="T26">go-DUR.[3SG]</ta>
            <ta e="T29" id="Seg_1160" s="T28">this.[NOM.SG]</ta>
            <ta e="T30" id="Seg_1161" s="T29">hare-EP-GEN</ta>
            <ta e="T31" id="Seg_1162" s="T30">house-NOM/GEN.3SG</ta>
            <ta e="T32" id="Seg_1163" s="T31">PTCL</ta>
            <ta e="T33" id="Seg_1164" s="T32">die-RES-PST.[3SG]</ta>
            <ta e="T35" id="Seg_1165" s="T34">water.[NOM.SG]</ta>
            <ta e="T37" id="Seg_1166" s="T35">become-RES-PST.[3SG]</ta>
            <ta e="T38" id="Seg_1167" s="T37">there</ta>
            <ta e="T39" id="Seg_1168" s="T38">wet.[NOM.SG]</ta>
            <ta e="T40" id="Seg_1169" s="T39">become-RES-PST.[3SG]</ta>
            <ta e="T41" id="Seg_1170" s="T40">rooster-LAT</ta>
            <ta e="T43" id="Seg_1171" s="T41">say-IPFVZ.[3SG]</ta>
            <ta e="T44" id="Seg_1172" s="T43">let-2PL</ta>
            <ta e="T45" id="Seg_1173" s="T44">I.ACC</ta>
            <ta e="T46" id="Seg_1174" s="T45">I.NOM</ta>
            <ta e="T47" id="Seg_1175" s="T46">NEG</ta>
            <ta e="T48" id="Seg_1176" s="T47">run-FUT-1SG</ta>
            <ta e="T49" id="Seg_1177" s="T48">become.dry-FUT-1SG</ta>
            <ta e="T51" id="Seg_1178" s="T49">you.ACC</ta>
            <ta e="T52" id="Seg_1179" s="T51">this.[NOM.SG]</ta>
            <ta e="T53" id="Seg_1180" s="T52">send-PST.[3SG]</ta>
            <ta e="T54" id="Seg_1181" s="T53">this.[NOM.SG]</ta>
            <ta e="T55" id="Seg_1182" s="T54">this-ACC</ta>
            <ta e="T57" id="Seg_1183" s="T55">drive-MOM-PST.[3SG]</ta>
            <ta e="T58" id="Seg_1184" s="T57">this.[NOM.SG]</ta>
            <ta e="T59" id="Seg_1185" s="T58">walk-PRS.[3SG]</ta>
            <ta e="T60" id="Seg_1186" s="T59">walk-PRS.[3SG]</ta>
            <ta e="T61" id="Seg_1187" s="T60">cry-DUR.[3SG]</ta>
            <ta e="T63" id="Seg_1188" s="T61">PTCL</ta>
            <ta e="T64" id="Seg_1189" s="T63">then</ta>
            <ta e="T65" id="Seg_1190" s="T64">come-PRS.[3SG]</ta>
            <ta e="T66" id="Seg_1191" s="T65">this-LAT</ta>
            <ta e="T69" id="Seg_1192" s="T67">fox.[NOM.SG]</ta>
            <ta e="T70" id="Seg_1193" s="T69">what.[NOM.SG]</ta>
            <ta e="T72" id="Seg_1194" s="T70">cry-DUR-2SG</ta>
            <ta e="T73" id="Seg_1195" s="T72">and</ta>
            <ta e="T74" id="Seg_1196" s="T73">I.GEN</ta>
            <ta e="T75" id="Seg_1197" s="T74">house-ABL</ta>
            <ta e="T77" id="Seg_1198" s="T76">hare.[NOM.SG]</ta>
            <ta e="T78" id="Seg_1199" s="T77">live-DUR.[3SG]</ta>
            <ta e="T79" id="Seg_1200" s="T78">I.ACC</ta>
            <ta e="T81" id="Seg_1201" s="T79">drive-MOM-PST.[3SG]</ta>
            <ta e="T82" id="Seg_1202" s="T81">this.[NOM.SG]</ta>
            <ta e="T83" id="Seg_1203" s="T82">say-IPFVZ.[3SG]</ta>
            <ta e="T84" id="Seg_1204" s="T83">go-OPT.DU/PL-1DU</ta>
            <ta e="T85" id="Seg_1205" s="T84">I.NOM</ta>
            <ta e="T86" id="Seg_1206" s="T85">this-ACC</ta>
            <ta e="T88" id="Seg_1207" s="T86">drive-FUT-1SG</ta>
            <ta e="T89" id="Seg_1208" s="T88">then</ta>
            <ta e="T90" id="Seg_1209" s="T89">come-PST-3PL</ta>
            <ta e="T92" id="Seg_1210" s="T91">drive-PST.[3SG]</ta>
            <ta e="T93" id="Seg_1211" s="T92">hare-EP-ACC</ta>
            <ta e="T94" id="Seg_1212" s="T93">and</ta>
            <ta e="T95" id="Seg_1213" s="T94">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T96" id="Seg_1214" s="T95">live-DUR.[3SG]</ta>
            <ta e="T97" id="Seg_1215" s="T96">NEG</ta>
            <ta e="T98" id="Seg_1216" s="T97">NEG</ta>
            <ta e="T99" id="Seg_1217" s="T98">let-CVB</ta>
            <ta e="T100" id="Seg_1218" s="T99">rooster-EP-ACC</ta>
            <ta e="T101" id="Seg_1219" s="T100">then</ta>
            <ta e="T102" id="Seg_1220" s="T101">this.[NOM.SG]</ta>
            <ta e="T103" id="Seg_1221" s="T102">again</ta>
            <ta e="T104" id="Seg_1222" s="T103">walk-PRS.[3SG]</ta>
            <ta e="T105" id="Seg_1223" s="T104">cry-DUR.[3SG]</ta>
            <ta e="T107" id="Seg_1224" s="T105">cry-DUR.[3SG]</ta>
            <ta e="T108" id="Seg_1225" s="T107">and</ta>
            <ta e="T109" id="Seg_1226" s="T108">there</ta>
            <ta e="T110" id="Seg_1227" s="T109">grass-PL</ta>
            <ta e="T111" id="Seg_1228" s="T110">cut-PST-3PL</ta>
            <ta e="T113" id="Seg_1229" s="T111">man-PL</ta>
            <ta e="T114" id="Seg_1230" s="T113">what.[NOM.SG]</ta>
            <ta e="T115" id="Seg_1231" s="T114">cry-DUR-2SG</ta>
            <ta e="T116" id="Seg_1232" s="T115">and</ta>
            <ta e="T117" id="Seg_1233" s="T116">I.NOM</ta>
            <ta e="T118" id="Seg_1234" s="T117">house-LAT</ta>
            <ta e="T119" id="Seg_1235" s="T118">fox.[NOM.SG]</ta>
            <ta e="T120" id="Seg_1236" s="T119">NEG</ta>
            <ta e="T122" id="Seg_1237" s="T120">let-CVB</ta>
            <ta e="T123" id="Seg_1238" s="T122">go-OPT.DU/PL-1DU</ta>
            <ta e="T124" id="Seg_1239" s="T123">we.NOM</ta>
            <ta e="T125" id="Seg_1240" s="T124">soon</ta>
            <ta e="T128" id="Seg_1241" s="T127">drive-MOM-PST.[3SG]</ta>
            <ta e="T129" id="Seg_1242" s="T128">we.NOM</ta>
            <ta e="T130" id="Seg_1243" s="T129">scythe-INS</ta>
            <ta e="T131" id="Seg_1244" s="T130">then</ta>
            <ta e="T133" id="Seg_1245" s="T131">come-PST-3PL</ta>
            <ta e="T134" id="Seg_1246" s="T133">go-EP-IMP.2SG</ta>
            <ta e="T135" id="Seg_1247" s="T134">hence</ta>
            <ta e="T136" id="Seg_1248" s="T135">soon</ta>
            <ta e="T137" id="Seg_1249" s="T136">scythe-INS</ta>
            <ta e="T138" id="Seg_1250" s="T137">head-NOM/GEN/ACC.2SG</ta>
            <ta e="T139" id="Seg_1251" s="T138">PTCL</ta>
            <ta e="T141" id="Seg_1252" s="T139">cut-FUT-1PL</ta>
            <ta e="T142" id="Seg_1253" s="T141">this</ta>
            <ta e="T143" id="Seg_1254" s="T142">PTCL</ta>
            <ta e="T145" id="Seg_1255" s="T143">run-DUR.PST.[3SG]</ta>
            <ta e="T146" id="Seg_1256" s="T145">and</ta>
            <ta e="T147" id="Seg_1257" s="T146">hare.[NOM.SG]</ta>
            <ta e="T148" id="Seg_1258" s="T147">there</ta>
            <ta e="T149" id="Seg_1259" s="T148">hinder-DUR.PST.[3SG]</ta>
            <ta e="T151" id="Seg_1260" s="T150">house-LAT</ta>
            <ta e="T152" id="Seg_1261" s="T151">and</ta>
            <ta e="T153" id="Seg_1262" s="T152">now</ta>
            <ta e="T157" id="Seg_1263" s="T155">rooster.[NOM.SG]</ta>
            <ta e="T158" id="Seg_1264" s="T157">house-LAT</ta>
            <ta e="T159" id="Seg_1265" s="T158">burn-DUR.PST.[3SG]</ta>
            <ta e="T160" id="Seg_1266" s="T159">and</ta>
            <ta e="T161" id="Seg_1267" s="T160">now</ta>
            <ta e="T162" id="Seg_1268" s="T161">live-DUR.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_1269" s="T1">жить-PST-3PL</ta>
            <ta e="T3" id="Seg_1270" s="T2">два-COLL</ta>
            <ta e="T4" id="Seg_1271" s="T3">заяц.[NOM.SG]</ta>
            <ta e="T5" id="Seg_1272" s="T4">и</ta>
            <ta e="T7" id="Seg_1273" s="T5">петух.[NOM.SG]</ta>
            <ta e="T8" id="Seg_1274" s="T7">петух-EP-GEN</ta>
            <ta e="T9" id="Seg_1275" s="T8">дом.[NOM.SG]</ta>
            <ta e="T10" id="Seg_1276" s="T9">быть-PST.[3SG]</ta>
            <ta e="T12" id="Seg_1277" s="T11">дерево-PL</ta>
            <ta e="T13" id="Seg_1278" s="T12">делать-PST.[3SG]</ta>
            <ta e="T14" id="Seg_1279" s="T13">а</ta>
            <ta e="T15" id="Seg_1280" s="T14">петух.[NOM.SG]</ta>
            <ta e="T16" id="Seg_1281" s="T15">а</ta>
            <ta e="T17" id="Seg_1282" s="T16">заяц.[NOM.SG]</ta>
            <ta e="T18" id="Seg_1283" s="T17">делать-PST.[3SG]</ta>
            <ta e="T19" id="Seg_1284" s="T18">снег-ABL</ta>
            <ta e="T22" id="Seg_1285" s="T19">дом.[NOM.SG]</ta>
            <ta e="T23" id="Seg_1286" s="T22">тогда</ta>
            <ta e="T24" id="Seg_1287" s="T23">теплый.[NOM.SG]</ta>
            <ta e="T25" id="Seg_1288" s="T24">стать-RES-PST.[3SG]</ta>
            <ta e="T26" id="Seg_1289" s="T25">солнце.[NOM.SG]</ta>
            <ta e="T28" id="Seg_1290" s="T26">идти-DUR.[3SG]</ta>
            <ta e="T29" id="Seg_1291" s="T28">этот.[NOM.SG]</ta>
            <ta e="T30" id="Seg_1292" s="T29">заяц-EP-GEN</ta>
            <ta e="T31" id="Seg_1293" s="T30">дом-NOM/GEN.3SG</ta>
            <ta e="T32" id="Seg_1294" s="T31">PTCL</ta>
            <ta e="T33" id="Seg_1295" s="T32">умереть-RES-PST.[3SG]</ta>
            <ta e="T35" id="Seg_1296" s="T34">вода.[NOM.SG]</ta>
            <ta e="T37" id="Seg_1297" s="T35">стать-RES-PST.[3SG]</ta>
            <ta e="T38" id="Seg_1298" s="T37">там</ta>
            <ta e="T39" id="Seg_1299" s="T38">влажный.[NOM.SG]</ta>
            <ta e="T40" id="Seg_1300" s="T39">стать-RES-PST.[3SG]</ta>
            <ta e="T41" id="Seg_1301" s="T40">петух-LAT</ta>
            <ta e="T43" id="Seg_1302" s="T41">сказать-IPFVZ.[3SG]</ta>
            <ta e="T44" id="Seg_1303" s="T43">пускать-2PL</ta>
            <ta e="T45" id="Seg_1304" s="T44">я.ACC</ta>
            <ta e="T46" id="Seg_1305" s="T45">я.NOM</ta>
            <ta e="T47" id="Seg_1306" s="T46">NEG</ta>
            <ta e="T48" id="Seg_1307" s="T47">бежать-FUT-1SG</ta>
            <ta e="T49" id="Seg_1308" s="T48">высохнуть-FUT-1SG</ta>
            <ta e="T51" id="Seg_1309" s="T49">ты.ACC</ta>
            <ta e="T52" id="Seg_1310" s="T51">этот.[NOM.SG]</ta>
            <ta e="T53" id="Seg_1311" s="T52">посылать-PST.[3SG]</ta>
            <ta e="T54" id="Seg_1312" s="T53">этот.[NOM.SG]</ta>
            <ta e="T55" id="Seg_1313" s="T54">этот-ACC</ta>
            <ta e="T57" id="Seg_1314" s="T55">гнать-MOM-PST.[3SG]</ta>
            <ta e="T58" id="Seg_1315" s="T57">этот.[NOM.SG]</ta>
            <ta e="T59" id="Seg_1316" s="T58">идти-PRS.[3SG]</ta>
            <ta e="T60" id="Seg_1317" s="T59">идти-PRS.[3SG]</ta>
            <ta e="T61" id="Seg_1318" s="T60">плакать-DUR.[3SG]</ta>
            <ta e="T63" id="Seg_1319" s="T61">PTCL</ta>
            <ta e="T64" id="Seg_1320" s="T63">тогда</ta>
            <ta e="T65" id="Seg_1321" s="T64">прийти-PRS.[3SG]</ta>
            <ta e="T66" id="Seg_1322" s="T65">этот-LAT</ta>
            <ta e="T69" id="Seg_1323" s="T67">лисица.[NOM.SG]</ta>
            <ta e="T70" id="Seg_1324" s="T69">что.[NOM.SG]</ta>
            <ta e="T72" id="Seg_1325" s="T70">плакать-DUR-2SG</ta>
            <ta e="T73" id="Seg_1326" s="T72">и</ta>
            <ta e="T74" id="Seg_1327" s="T73">я.GEN</ta>
            <ta e="T75" id="Seg_1328" s="T74">дом-ABL</ta>
            <ta e="T77" id="Seg_1329" s="T76">заяц.[NOM.SG]</ta>
            <ta e="T78" id="Seg_1330" s="T77">жить-DUR.[3SG]</ta>
            <ta e="T79" id="Seg_1331" s="T78">я.ACC</ta>
            <ta e="T81" id="Seg_1332" s="T79">гнать-MOM-PST.[3SG]</ta>
            <ta e="T82" id="Seg_1333" s="T81">этот.[NOM.SG]</ta>
            <ta e="T83" id="Seg_1334" s="T82">сказать-IPFVZ.[3SG]</ta>
            <ta e="T84" id="Seg_1335" s="T83">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T85" id="Seg_1336" s="T84">я.NOM</ta>
            <ta e="T86" id="Seg_1337" s="T85">этот-ACC</ta>
            <ta e="T88" id="Seg_1338" s="T86">гнать-FUT-1SG</ta>
            <ta e="T89" id="Seg_1339" s="T88">тогда</ta>
            <ta e="T90" id="Seg_1340" s="T89">прийти-PST-3PL</ta>
            <ta e="T92" id="Seg_1341" s="T91">гнать-PST.[3SG]</ta>
            <ta e="T93" id="Seg_1342" s="T92">заяц-EP-ACC</ta>
            <ta e="T94" id="Seg_1343" s="T93">и</ta>
            <ta e="T95" id="Seg_1344" s="T94">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T96" id="Seg_1345" s="T95">жить-DUR.[3SG]</ta>
            <ta e="T97" id="Seg_1346" s="T96">NEG</ta>
            <ta e="T98" id="Seg_1347" s="T97">NEG</ta>
            <ta e="T99" id="Seg_1348" s="T98">пускать-CVB</ta>
            <ta e="T100" id="Seg_1349" s="T99">петух-EP-ACC</ta>
            <ta e="T101" id="Seg_1350" s="T100">тогда</ta>
            <ta e="T102" id="Seg_1351" s="T101">этот.[NOM.SG]</ta>
            <ta e="T103" id="Seg_1352" s="T102">опять</ta>
            <ta e="T104" id="Seg_1353" s="T103">идти-PRS.[3SG]</ta>
            <ta e="T105" id="Seg_1354" s="T104">плакать-DUR.[3SG]</ta>
            <ta e="T107" id="Seg_1355" s="T105">плакать-DUR.[3SG]</ta>
            <ta e="T108" id="Seg_1356" s="T107">а</ta>
            <ta e="T109" id="Seg_1357" s="T108">там</ta>
            <ta e="T110" id="Seg_1358" s="T109">трава-PL</ta>
            <ta e="T111" id="Seg_1359" s="T110">резать-PST-3PL</ta>
            <ta e="T113" id="Seg_1360" s="T111">мужчина-PL</ta>
            <ta e="T114" id="Seg_1361" s="T113">что.[NOM.SG]</ta>
            <ta e="T115" id="Seg_1362" s="T114">плакать-DUR-2SG</ta>
            <ta e="T116" id="Seg_1363" s="T115">и</ta>
            <ta e="T117" id="Seg_1364" s="T116">я.NOM</ta>
            <ta e="T118" id="Seg_1365" s="T117">дом-LAT</ta>
            <ta e="T119" id="Seg_1366" s="T118">лисица.[NOM.SG]</ta>
            <ta e="T120" id="Seg_1367" s="T119">NEG</ta>
            <ta e="T122" id="Seg_1368" s="T120">пускать-CVB</ta>
            <ta e="T123" id="Seg_1369" s="T122">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T124" id="Seg_1370" s="T123">мы.NOM</ta>
            <ta e="T125" id="Seg_1371" s="T124">скоро</ta>
            <ta e="T128" id="Seg_1372" s="T127">гнать-MOM-PST.[3SG]</ta>
            <ta e="T129" id="Seg_1373" s="T128">мы.NOM</ta>
            <ta e="T130" id="Seg_1374" s="T129">коса-INS</ta>
            <ta e="T131" id="Seg_1375" s="T130">тогда</ta>
            <ta e="T133" id="Seg_1376" s="T131">прийти-PST-3PL</ta>
            <ta e="T134" id="Seg_1377" s="T133">пойти-EP-IMP.2SG</ta>
            <ta e="T135" id="Seg_1378" s="T134">отсюда</ta>
            <ta e="T136" id="Seg_1379" s="T135">скоро</ta>
            <ta e="T137" id="Seg_1380" s="T136">коса-INS</ta>
            <ta e="T138" id="Seg_1381" s="T137">голова-NOM/GEN/ACC.2SG</ta>
            <ta e="T139" id="Seg_1382" s="T138">PTCL</ta>
            <ta e="T141" id="Seg_1383" s="T139">резать-FUT-1PL</ta>
            <ta e="T142" id="Seg_1384" s="T141">этот</ta>
            <ta e="T143" id="Seg_1385" s="T142">PTCL</ta>
            <ta e="T145" id="Seg_1386" s="T143">бежать-DUR.PST.[3SG]</ta>
            <ta e="T146" id="Seg_1387" s="T145">а</ta>
            <ta e="T147" id="Seg_1388" s="T146">заяц.[NOM.SG]</ta>
            <ta e="T148" id="Seg_1389" s="T147">там</ta>
            <ta e="T149" id="Seg_1390" s="T148">мешать-DUR.PST.[3SG]</ta>
            <ta e="T151" id="Seg_1391" s="T150">дом-LAT</ta>
            <ta e="T152" id="Seg_1392" s="T151">и</ta>
            <ta e="T153" id="Seg_1393" s="T152">сейчас</ta>
            <ta e="T157" id="Seg_1394" s="T155">петух.[NOM.SG]</ta>
            <ta e="T158" id="Seg_1395" s="T157">дом-LAT</ta>
            <ta e="T159" id="Seg_1396" s="T158">гореть-DUR.PST.[3SG]</ta>
            <ta e="T160" id="Seg_1397" s="T159">и</ta>
            <ta e="T161" id="Seg_1398" s="T160">сейчас</ta>
            <ta e="T162" id="Seg_1399" s="T161">жить-DUR.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_1400" s="T1">v-v:tense-v:pn</ta>
            <ta e="T3" id="Seg_1401" s="T2">num-num&gt;num</ta>
            <ta e="T4" id="Seg_1402" s="T3">n.[n:case]</ta>
            <ta e="T5" id="Seg_1403" s="T4">conj</ta>
            <ta e="T7" id="Seg_1404" s="T5">n.[n:case]</ta>
            <ta e="T8" id="Seg_1405" s="T7">n-n:ins-n:case</ta>
            <ta e="T9" id="Seg_1406" s="T8">n.[n:case]</ta>
            <ta e="T10" id="Seg_1407" s="T9">v-v:tense.[v:pn]</ta>
            <ta e="T12" id="Seg_1408" s="T11">n-n:num</ta>
            <ta e="T13" id="Seg_1409" s="T12">v-v:tense.[v:pn]</ta>
            <ta e="T14" id="Seg_1410" s="T13">conj</ta>
            <ta e="T15" id="Seg_1411" s="T14">n.[n:case]</ta>
            <ta e="T16" id="Seg_1412" s="T15">conj</ta>
            <ta e="T17" id="Seg_1413" s="T16">n.[n:case]</ta>
            <ta e="T18" id="Seg_1414" s="T17">v-v:tense.[v:pn]</ta>
            <ta e="T19" id="Seg_1415" s="T18">n-n:case</ta>
            <ta e="T22" id="Seg_1416" s="T19">n.[n:case]</ta>
            <ta e="T23" id="Seg_1417" s="T22">adv</ta>
            <ta e="T24" id="Seg_1418" s="T23">adj.[n:case]</ta>
            <ta e="T25" id="Seg_1419" s="T24">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T26" id="Seg_1420" s="T25">n.[n:case]</ta>
            <ta e="T28" id="Seg_1421" s="T26">v-v&gt;v.[v:pn]</ta>
            <ta e="T29" id="Seg_1422" s="T28">dempro.[n:case]</ta>
            <ta e="T30" id="Seg_1423" s="T29">n-n:ins-n:case</ta>
            <ta e="T31" id="Seg_1424" s="T30">n-n:case.poss</ta>
            <ta e="T32" id="Seg_1425" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_1426" s="T32">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T35" id="Seg_1427" s="T34">n.[n:case]</ta>
            <ta e="T37" id="Seg_1428" s="T35">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T38" id="Seg_1429" s="T37">adv</ta>
            <ta e="T39" id="Seg_1430" s="T38">adj.[n:case]</ta>
            <ta e="T40" id="Seg_1431" s="T39">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T41" id="Seg_1432" s="T40">n-n:case</ta>
            <ta e="T43" id="Seg_1433" s="T41">v-v&gt;v.[v:pn]</ta>
            <ta e="T44" id="Seg_1434" s="T43">v-v:pn</ta>
            <ta e="T45" id="Seg_1435" s="T44">pers</ta>
            <ta e="T46" id="Seg_1436" s="T45">pers</ta>
            <ta e="T47" id="Seg_1437" s="T46">ptcl</ta>
            <ta e="T48" id="Seg_1438" s="T47">v-v:tense-v:pn</ta>
            <ta e="T49" id="Seg_1439" s="T48">v-v:tense-v:pn</ta>
            <ta e="T51" id="Seg_1440" s="T49">pers</ta>
            <ta e="T52" id="Seg_1441" s="T51">dempro.[n:case]</ta>
            <ta e="T53" id="Seg_1442" s="T52">v-v:tense.[v:pn]</ta>
            <ta e="T54" id="Seg_1443" s="T53">dempro.[n:case]</ta>
            <ta e="T55" id="Seg_1444" s="T54">dempro-n:case</ta>
            <ta e="T57" id="Seg_1445" s="T55">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T58" id="Seg_1446" s="T57">dempro.[n:case]</ta>
            <ta e="T59" id="Seg_1447" s="T58">v-v:tense.[v:pn]</ta>
            <ta e="T60" id="Seg_1448" s="T59">v-v:tense.[v:pn]</ta>
            <ta e="T61" id="Seg_1449" s="T60">v-v&gt;v.[v:pn]</ta>
            <ta e="T63" id="Seg_1450" s="T61">ptcl</ta>
            <ta e="T64" id="Seg_1451" s="T63">adv</ta>
            <ta e="T65" id="Seg_1452" s="T64">v-v:tense.[v:pn]</ta>
            <ta e="T66" id="Seg_1453" s="T65">dempro-n:case</ta>
            <ta e="T69" id="Seg_1454" s="T67">n.[n:case]</ta>
            <ta e="T70" id="Seg_1455" s="T69">que.[n:case]</ta>
            <ta e="T72" id="Seg_1456" s="T70">v-v&gt;v-v:pn</ta>
            <ta e="T73" id="Seg_1457" s="T72">conj</ta>
            <ta e="T74" id="Seg_1458" s="T73">pers</ta>
            <ta e="T75" id="Seg_1459" s="T74">n-n:case</ta>
            <ta e="T77" id="Seg_1460" s="T76">n.[n:case]</ta>
            <ta e="T78" id="Seg_1461" s="T77">v-v&gt;v.[v:pn]</ta>
            <ta e="T79" id="Seg_1462" s="T78">pers</ta>
            <ta e="T81" id="Seg_1463" s="T79">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T82" id="Seg_1464" s="T81">dempro.[n:case]</ta>
            <ta e="T83" id="Seg_1465" s="T82">v-v&gt;v.[v:pn]</ta>
            <ta e="T84" id="Seg_1466" s="T83">v-v:mood-v:pn</ta>
            <ta e="T85" id="Seg_1467" s="T84">pers</ta>
            <ta e="T86" id="Seg_1468" s="T85">dempro-n:case</ta>
            <ta e="T88" id="Seg_1469" s="T86">v-v:tense-v:pn</ta>
            <ta e="T89" id="Seg_1470" s="T88">adv</ta>
            <ta e="T90" id="Seg_1471" s="T89">v-v:tense-v:pn</ta>
            <ta e="T92" id="Seg_1472" s="T91">v-v:tense.[v:pn]</ta>
            <ta e="T93" id="Seg_1473" s="T92">n-n:ins-n:case</ta>
            <ta e="T94" id="Seg_1474" s="T93">conj</ta>
            <ta e="T95" id="Seg_1475" s="T94">refl-n:case.poss</ta>
            <ta e="T96" id="Seg_1476" s="T95">v-v&gt;v.[v:pn]</ta>
            <ta e="T97" id="Seg_1477" s="T96">ptcl</ta>
            <ta e="T98" id="Seg_1478" s="T97">ptcl</ta>
            <ta e="T99" id="Seg_1479" s="T98">v-v:n.fin</ta>
            <ta e="T100" id="Seg_1480" s="T99">n-n:ins-n:case</ta>
            <ta e="T101" id="Seg_1481" s="T100">adv</ta>
            <ta e="T102" id="Seg_1482" s="T101">dempro.[n:case]</ta>
            <ta e="T103" id="Seg_1483" s="T102">adv</ta>
            <ta e="T104" id="Seg_1484" s="T103">v-v:tense.[v:pn]</ta>
            <ta e="T105" id="Seg_1485" s="T104">v-v&gt;v.[v:pn]</ta>
            <ta e="T107" id="Seg_1486" s="T105">v-v&gt;v.[v:pn]</ta>
            <ta e="T108" id="Seg_1487" s="T107">conj</ta>
            <ta e="T109" id="Seg_1488" s="T108">adv</ta>
            <ta e="T110" id="Seg_1489" s="T109">n-n:num</ta>
            <ta e="T111" id="Seg_1490" s="T110">v-v:tense-v:pn</ta>
            <ta e="T113" id="Seg_1491" s="T111">n-n:num</ta>
            <ta e="T114" id="Seg_1492" s="T113">que.[n:case]</ta>
            <ta e="T115" id="Seg_1493" s="T114">v-v&gt;v-v:pn</ta>
            <ta e="T116" id="Seg_1494" s="T115">conj</ta>
            <ta e="T117" id="Seg_1495" s="T116">pers</ta>
            <ta e="T118" id="Seg_1496" s="T117">n-n:case</ta>
            <ta e="T119" id="Seg_1497" s="T118">n.[n:case]</ta>
            <ta e="T120" id="Seg_1498" s="T119">ptcl</ta>
            <ta e="T122" id="Seg_1499" s="T120">v-v:n.fin</ta>
            <ta e="T123" id="Seg_1500" s="T122">v-v:mood-v:pn</ta>
            <ta e="T124" id="Seg_1501" s="T123">pers</ta>
            <ta e="T125" id="Seg_1502" s="T124">adv</ta>
            <ta e="T128" id="Seg_1503" s="T127">v-v&gt;v-v:tense.[v:pn]</ta>
            <ta e="T129" id="Seg_1504" s="T128">pers</ta>
            <ta e="T130" id="Seg_1505" s="T129">n-n:case</ta>
            <ta e="T131" id="Seg_1506" s="T130">adv</ta>
            <ta e="T133" id="Seg_1507" s="T131">v-v:tense-v:pn</ta>
            <ta e="T134" id="Seg_1508" s="T133">v-v:ins-v:mood.pn</ta>
            <ta e="T135" id="Seg_1509" s="T134">adv</ta>
            <ta e="T136" id="Seg_1510" s="T135">adv</ta>
            <ta e="T137" id="Seg_1511" s="T136">n-n:case</ta>
            <ta e="T138" id="Seg_1512" s="T137">n-n:case.poss</ta>
            <ta e="T139" id="Seg_1513" s="T138">ptcl</ta>
            <ta e="T141" id="Seg_1514" s="T139">v-v:tense-v:pn</ta>
            <ta e="T142" id="Seg_1515" s="T141">dempro</ta>
            <ta e="T143" id="Seg_1516" s="T142">ptcl</ta>
            <ta e="T145" id="Seg_1517" s="T143">v-v:tense.[v:pn]</ta>
            <ta e="T146" id="Seg_1518" s="T145">conj</ta>
            <ta e="T147" id="Seg_1519" s="T146">n.[n:case]</ta>
            <ta e="T148" id="Seg_1520" s="T147">adv</ta>
            <ta e="T149" id="Seg_1521" s="T148">v-v:tense.[v:pn]</ta>
            <ta e="T151" id="Seg_1522" s="T150">n-n:case</ta>
            <ta e="T152" id="Seg_1523" s="T151">conj</ta>
            <ta e="T153" id="Seg_1524" s="T152">adv</ta>
            <ta e="T157" id="Seg_1525" s="T155">n</ta>
            <ta e="T158" id="Seg_1526" s="T157">n-n:case</ta>
            <ta e="T159" id="Seg_1527" s="T158">v-v:tense.[v:pn]</ta>
            <ta e="T160" id="Seg_1528" s="T159">conj</ta>
            <ta e="T161" id="Seg_1529" s="T160">adv</ta>
            <ta e="T162" id="Seg_1530" s="T161">v-v&gt;v.[v:pn]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_1531" s="T1">v</ta>
            <ta e="T3" id="Seg_1532" s="T2">num</ta>
            <ta e="T4" id="Seg_1533" s="T3">n</ta>
            <ta e="T5" id="Seg_1534" s="T4">conj</ta>
            <ta e="T7" id="Seg_1535" s="T5">n</ta>
            <ta e="T8" id="Seg_1536" s="T7">n</ta>
            <ta e="T9" id="Seg_1537" s="T8">n</ta>
            <ta e="T10" id="Seg_1538" s="T9">v</ta>
            <ta e="T12" id="Seg_1539" s="T11">n</ta>
            <ta e="T13" id="Seg_1540" s="T12">v</ta>
            <ta e="T14" id="Seg_1541" s="T13">conj</ta>
            <ta e="T15" id="Seg_1542" s="T14">n</ta>
            <ta e="T16" id="Seg_1543" s="T15">conj</ta>
            <ta e="T17" id="Seg_1544" s="T16">n</ta>
            <ta e="T18" id="Seg_1545" s="T17">v</ta>
            <ta e="T19" id="Seg_1546" s="T18">adj</ta>
            <ta e="T22" id="Seg_1547" s="T19">n</ta>
            <ta e="T23" id="Seg_1548" s="T22">adv</ta>
            <ta e="T24" id="Seg_1549" s="T23">adj</ta>
            <ta e="T25" id="Seg_1550" s="T24">v</ta>
            <ta e="T26" id="Seg_1551" s="T25">n</ta>
            <ta e="T28" id="Seg_1552" s="T26">v</ta>
            <ta e="T29" id="Seg_1553" s="T28">dempro</ta>
            <ta e="T30" id="Seg_1554" s="T29">n</ta>
            <ta e="T31" id="Seg_1555" s="T30">n</ta>
            <ta e="T32" id="Seg_1556" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_1557" s="T32">v</ta>
            <ta e="T35" id="Seg_1558" s="T34">n</ta>
            <ta e="T37" id="Seg_1559" s="T35">v</ta>
            <ta e="T38" id="Seg_1560" s="T37">adv</ta>
            <ta e="T39" id="Seg_1561" s="T38">adj</ta>
            <ta e="T40" id="Seg_1562" s="T39">v</ta>
            <ta e="T41" id="Seg_1563" s="T40">n</ta>
            <ta e="T43" id="Seg_1564" s="T41">v</ta>
            <ta e="T44" id="Seg_1565" s="T43">v</ta>
            <ta e="T45" id="Seg_1566" s="T44">pers</ta>
            <ta e="T46" id="Seg_1567" s="T45">pers</ta>
            <ta e="T47" id="Seg_1568" s="T46">ptcl</ta>
            <ta e="T48" id="Seg_1569" s="T47">v</ta>
            <ta e="T49" id="Seg_1570" s="T48">v</ta>
            <ta e="T51" id="Seg_1571" s="T49">pers</ta>
            <ta e="T52" id="Seg_1572" s="T51">dempro</ta>
            <ta e="T53" id="Seg_1573" s="T52">v</ta>
            <ta e="T54" id="Seg_1574" s="T53">dempro</ta>
            <ta e="T55" id="Seg_1575" s="T54">dempro</ta>
            <ta e="T57" id="Seg_1576" s="T55">v</ta>
            <ta e="T58" id="Seg_1577" s="T57">dempro</ta>
            <ta e="T59" id="Seg_1578" s="T58">v</ta>
            <ta e="T60" id="Seg_1579" s="T59">v</ta>
            <ta e="T61" id="Seg_1580" s="T60">v</ta>
            <ta e="T63" id="Seg_1581" s="T61">ptcl</ta>
            <ta e="T64" id="Seg_1582" s="T63">adv</ta>
            <ta e="T65" id="Seg_1583" s="T64">v</ta>
            <ta e="T66" id="Seg_1584" s="T65">dempro</ta>
            <ta e="T69" id="Seg_1585" s="T67">n</ta>
            <ta e="T70" id="Seg_1586" s="T69">que</ta>
            <ta e="T72" id="Seg_1587" s="T70">v</ta>
            <ta e="T73" id="Seg_1588" s="T72">conj</ta>
            <ta e="T74" id="Seg_1589" s="T73">pers</ta>
            <ta e="T75" id="Seg_1590" s="T74">n</ta>
            <ta e="T77" id="Seg_1591" s="T76">n</ta>
            <ta e="T78" id="Seg_1592" s="T77">v</ta>
            <ta e="T79" id="Seg_1593" s="T78">pers</ta>
            <ta e="T81" id="Seg_1594" s="T79">v</ta>
            <ta e="T82" id="Seg_1595" s="T81">dempro</ta>
            <ta e="T83" id="Seg_1596" s="T82">v</ta>
            <ta e="T84" id="Seg_1597" s="T83">v</ta>
            <ta e="T85" id="Seg_1598" s="T84">pers</ta>
            <ta e="T86" id="Seg_1599" s="T85">dempro</ta>
            <ta e="T88" id="Seg_1600" s="T86">v</ta>
            <ta e="T89" id="Seg_1601" s="T88">adv</ta>
            <ta e="T90" id="Seg_1602" s="T89">v</ta>
            <ta e="T92" id="Seg_1603" s="T91">v</ta>
            <ta e="T93" id="Seg_1604" s="T92">n</ta>
            <ta e="T94" id="Seg_1605" s="T93">conj</ta>
            <ta e="T95" id="Seg_1606" s="T94">refl</ta>
            <ta e="T96" id="Seg_1607" s="T95">v</ta>
            <ta e="T97" id="Seg_1608" s="T96">ptcl</ta>
            <ta e="T98" id="Seg_1609" s="T97">ptcl</ta>
            <ta e="T99" id="Seg_1610" s="T98">v</ta>
            <ta e="T100" id="Seg_1611" s="T99">n</ta>
            <ta e="T101" id="Seg_1612" s="T100">adv</ta>
            <ta e="T102" id="Seg_1613" s="T101">dempro</ta>
            <ta e="T103" id="Seg_1614" s="T102">adv</ta>
            <ta e="T104" id="Seg_1615" s="T103">v</ta>
            <ta e="T105" id="Seg_1616" s="T104">v</ta>
            <ta e="T107" id="Seg_1617" s="T105">v</ta>
            <ta e="T108" id="Seg_1618" s="T107">conj</ta>
            <ta e="T109" id="Seg_1619" s="T108">adv</ta>
            <ta e="T110" id="Seg_1620" s="T109">n</ta>
            <ta e="T111" id="Seg_1621" s="T110">v</ta>
            <ta e="T113" id="Seg_1622" s="T111">n</ta>
            <ta e="T114" id="Seg_1623" s="T113">que</ta>
            <ta e="T115" id="Seg_1624" s="T114">v</ta>
            <ta e="T116" id="Seg_1625" s="T115">conj</ta>
            <ta e="T117" id="Seg_1626" s="T116">pers</ta>
            <ta e="T118" id="Seg_1627" s="T117">n</ta>
            <ta e="T119" id="Seg_1628" s="T118">n</ta>
            <ta e="T120" id="Seg_1629" s="T119">ptcl</ta>
            <ta e="T122" id="Seg_1630" s="T120">v</ta>
            <ta e="T123" id="Seg_1631" s="T122">v</ta>
            <ta e="T124" id="Seg_1632" s="T123">pers</ta>
            <ta e="T125" id="Seg_1633" s="T124">adv</ta>
            <ta e="T128" id="Seg_1634" s="T127">v</ta>
            <ta e="T129" id="Seg_1635" s="T128">pers</ta>
            <ta e="T130" id="Seg_1636" s="T129">n</ta>
            <ta e="T131" id="Seg_1637" s="T130">adv</ta>
            <ta e="T133" id="Seg_1638" s="T131">v</ta>
            <ta e="T134" id="Seg_1639" s="T133">v</ta>
            <ta e="T135" id="Seg_1640" s="T134">adv</ta>
            <ta e="T136" id="Seg_1641" s="T135">adv</ta>
            <ta e="T137" id="Seg_1642" s="T136">n</ta>
            <ta e="T138" id="Seg_1643" s="T137">n</ta>
            <ta e="T139" id="Seg_1644" s="T138">ptcl</ta>
            <ta e="T141" id="Seg_1645" s="T139">v</ta>
            <ta e="T142" id="Seg_1646" s="T141">dempro</ta>
            <ta e="T143" id="Seg_1647" s="T142">ptcl</ta>
            <ta e="T145" id="Seg_1648" s="T143">v</ta>
            <ta e="T146" id="Seg_1649" s="T145">conj</ta>
            <ta e="T147" id="Seg_1650" s="T146">n</ta>
            <ta e="T148" id="Seg_1651" s="T147">adv</ta>
            <ta e="T149" id="Seg_1652" s="T148">v</ta>
            <ta e="T151" id="Seg_1653" s="T150">n</ta>
            <ta e="T152" id="Seg_1654" s="T151">conj</ta>
            <ta e="T153" id="Seg_1655" s="T152">adv</ta>
            <ta e="T155" id="Seg_1656" s="T153">v</ta>
            <ta e="T157" id="Seg_1657" s="T155">n</ta>
            <ta e="T158" id="Seg_1658" s="T157">n</ta>
            <ta e="T159" id="Seg_1659" s="T158">v</ta>
            <ta e="T160" id="Seg_1660" s="T159">conj</ta>
            <ta e="T161" id="Seg_1661" s="T160">adv</ta>
            <ta e="T162" id="Seg_1662" s="T161">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T4" id="Seg_1663" s="T3">np.h:E</ta>
            <ta e="T7" id="Seg_1664" s="T5">np.h:E</ta>
            <ta e="T8" id="Seg_1665" s="T7">np.h:Poss</ta>
            <ta e="T9" id="Seg_1666" s="T8">np:Th</ta>
            <ta e="T12" id="Seg_1667" s="T11">np:Ins</ta>
            <ta e="T15" id="Seg_1668" s="T14">np.h:A</ta>
            <ta e="T17" id="Seg_1669" s="T16">np.h:A</ta>
            <ta e="T19" id="Seg_1670" s="T18">np:Ins</ta>
            <ta e="T22" id="Seg_1671" s="T19">np:P</ta>
            <ta e="T23" id="Seg_1672" s="T22">adv:Time</ta>
            <ta e="T26" id="Seg_1673" s="T25">np:Th</ta>
            <ta e="T30" id="Seg_1674" s="T29">np.h:Poss</ta>
            <ta e="T31" id="Seg_1675" s="T30">np:P</ta>
            <ta e="T35" id="Seg_1676" s="T34">np:Th</ta>
            <ta e="T38" id="Seg_1677" s="T37">adv:L</ta>
            <ta e="T40" id="Seg_1678" s="T39">0.3:Th</ta>
            <ta e="T41" id="Seg_1679" s="T40">np.h:R</ta>
            <ta e="T43" id="Seg_1680" s="T41">0.3.h:A</ta>
            <ta e="T44" id="Seg_1681" s="T43">0.2.h:A</ta>
            <ta e="T45" id="Seg_1682" s="T44">pro.h:Th</ta>
            <ta e="T46" id="Seg_1683" s="T45">pro.h:A</ta>
            <ta e="T49" id="Seg_1684" s="T48">0.1.h:A</ta>
            <ta e="T51" id="Seg_1685" s="T49">pro.h:Th</ta>
            <ta e="T52" id="Seg_1686" s="T51">pro.h:A</ta>
            <ta e="T54" id="Seg_1687" s="T53">pro.h:A</ta>
            <ta e="T55" id="Seg_1688" s="T54">pro.h:Th</ta>
            <ta e="T58" id="Seg_1689" s="T57">pro.h:A</ta>
            <ta e="T60" id="Seg_1690" s="T59">0.3.h:A</ta>
            <ta e="T61" id="Seg_1691" s="T60">0.3.h:E</ta>
            <ta e="T64" id="Seg_1692" s="T63">adv:Time</ta>
            <ta e="T66" id="Seg_1693" s="T65">pro:G</ta>
            <ta e="T69" id="Seg_1694" s="T67">np.h:A</ta>
            <ta e="T72" id="Seg_1695" s="T70">0.2.h:E</ta>
            <ta e="T74" id="Seg_1696" s="T73">pro.h:Poss</ta>
            <ta e="T75" id="Seg_1697" s="T74">np:L</ta>
            <ta e="T77" id="Seg_1698" s="T76">np.h:A</ta>
            <ta e="T79" id="Seg_1699" s="T78">pro.h:Th</ta>
            <ta e="T81" id="Seg_1700" s="T79">0.3.h:A</ta>
            <ta e="T82" id="Seg_1701" s="T81">pro.h:A</ta>
            <ta e="T84" id="Seg_1702" s="T83">0.1.h:A</ta>
            <ta e="T85" id="Seg_1703" s="T84">pro.h:A</ta>
            <ta e="T86" id="Seg_1704" s="T85">pro.h:Th</ta>
            <ta e="T89" id="Seg_1705" s="T88">adv:Time</ta>
            <ta e="T90" id="Seg_1706" s="T89">0.3.h:A</ta>
            <ta e="T92" id="Seg_1707" s="T91">0.3.h:A</ta>
            <ta e="T93" id="Seg_1708" s="T92">np.h:Th</ta>
            <ta e="T96" id="Seg_1709" s="T95">0.3.h:E</ta>
            <ta e="T100" id="Seg_1710" s="T99">np.h:Th</ta>
            <ta e="T101" id="Seg_1711" s="T100">adv:Time</ta>
            <ta e="T102" id="Seg_1712" s="T101">pro.h:A</ta>
            <ta e="T105" id="Seg_1713" s="T104">0.3.h:E</ta>
            <ta e="T107" id="Seg_1714" s="T105">0.3.h:E</ta>
            <ta e="T109" id="Seg_1715" s="T108">adv:L</ta>
            <ta e="T110" id="Seg_1716" s="T109">np:P</ta>
            <ta e="T113" id="Seg_1717" s="T111">np.h:A</ta>
            <ta e="T115" id="Seg_1718" s="T114">0.2.h:E</ta>
            <ta e="T117" id="Seg_1719" s="T116">pro.h:Poss</ta>
            <ta e="T118" id="Seg_1720" s="T117">np:G</ta>
            <ta e="T119" id="Seg_1721" s="T118">np.h:A</ta>
            <ta e="T123" id="Seg_1722" s="T122">0.1.h:A</ta>
            <ta e="T124" id="Seg_1723" s="T123">pro.h:A</ta>
            <ta e="T125" id="Seg_1724" s="T124">adv:Time</ta>
            <ta e="T129" id="Seg_1725" s="T128">pro.h:Poss</ta>
            <ta e="T130" id="Seg_1726" s="T129">np:Ins</ta>
            <ta e="T131" id="Seg_1727" s="T130">adv:Time</ta>
            <ta e="T133" id="Seg_1728" s="T131">0.3.h:A</ta>
            <ta e="T134" id="Seg_1729" s="T133">0.2.h:A</ta>
            <ta e="T135" id="Seg_1730" s="T134">adv:So</ta>
            <ta e="T136" id="Seg_1731" s="T135">adv:Time</ta>
            <ta e="T137" id="Seg_1732" s="T136">np:Ins</ta>
            <ta e="T138" id="Seg_1733" s="T137">np:P</ta>
            <ta e="T141" id="Seg_1734" s="T139">0.1.h:A</ta>
            <ta e="T142" id="Seg_1735" s="T141">pro.h:A</ta>
            <ta e="T147" id="Seg_1736" s="T146">np.h:A</ta>
            <ta e="T148" id="Seg_1737" s="T147">adv:L</ta>
            <ta e="T151" id="Seg_1738" s="T150">np:L</ta>
            <ta e="T153" id="Seg_1739" s="T152">adv:Time</ta>
            <ta e="T157" id="Seg_1740" s="T155">np.h:A</ta>
            <ta e="T158" id="Seg_1741" s="T157">np:L</ta>
            <ta e="T161" id="Seg_1742" s="T160">adv:Time</ta>
            <ta e="T162" id="Seg_1743" s="T161">0.3.h:E</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_1744" s="T1">v:pred</ta>
            <ta e="T4" id="Seg_1745" s="T3">np.h:S</ta>
            <ta e="T7" id="Seg_1746" s="T5">np.h:S</ta>
            <ta e="T9" id="Seg_1747" s="T8">np:S</ta>
            <ta e="T10" id="Seg_1748" s="T9">v:pred</ta>
            <ta e="T13" id="Seg_1749" s="T12">v:pred</ta>
            <ta e="T15" id="Seg_1750" s="T14">np.h:S</ta>
            <ta e="T17" id="Seg_1751" s="T16">np.h:S</ta>
            <ta e="T18" id="Seg_1752" s="T17">v:pred</ta>
            <ta e="T22" id="Seg_1753" s="T19">np:O</ta>
            <ta e="T24" id="Seg_1754" s="T23">adj:pred</ta>
            <ta e="T25" id="Seg_1755" s="T24">cop 0.3:S</ta>
            <ta e="T26" id="Seg_1756" s="T25">np:S</ta>
            <ta e="T28" id="Seg_1757" s="T26">v:pred</ta>
            <ta e="T31" id="Seg_1758" s="T30">np:S</ta>
            <ta e="T33" id="Seg_1759" s="T32">v:pred</ta>
            <ta e="T35" id="Seg_1760" s="T34">n:pred</ta>
            <ta e="T37" id="Seg_1761" s="T35">cop</ta>
            <ta e="T39" id="Seg_1762" s="T38">adj:pred</ta>
            <ta e="T40" id="Seg_1763" s="T39">cop 0.3:S</ta>
            <ta e="T43" id="Seg_1764" s="T41">v:pred 0.3.h:S</ta>
            <ta e="T44" id="Seg_1765" s="T43">v:pred 0.2.h:S</ta>
            <ta e="T45" id="Seg_1766" s="T44">pro.h:O</ta>
            <ta e="T46" id="Seg_1767" s="T45">pro.h:S</ta>
            <ta e="T47" id="Seg_1768" s="T46">ptcl.neg</ta>
            <ta e="T48" id="Seg_1769" s="T47">v:pred</ta>
            <ta e="T49" id="Seg_1770" s="T48">v:pred 0.1.h:S</ta>
            <ta e="T51" id="Seg_1771" s="T49">pro.h:O</ta>
            <ta e="T52" id="Seg_1772" s="T51">pro.h:S</ta>
            <ta e="T53" id="Seg_1773" s="T52">v:pred</ta>
            <ta e="T54" id="Seg_1774" s="T53">pro.h:S</ta>
            <ta e="T55" id="Seg_1775" s="T54">pro.h:O</ta>
            <ta e="T57" id="Seg_1776" s="T55">v:pred</ta>
            <ta e="T58" id="Seg_1777" s="T57">pro.h:S</ta>
            <ta e="T59" id="Seg_1778" s="T58">v:pred</ta>
            <ta e="T60" id="Seg_1779" s="T59">v:pred 0.3.h:S</ta>
            <ta e="T61" id="Seg_1780" s="T60">v:pred 0.3.h:S</ta>
            <ta e="T65" id="Seg_1781" s="T64">v:pred</ta>
            <ta e="T69" id="Seg_1782" s="T67">np.h:S</ta>
            <ta e="T72" id="Seg_1783" s="T70">v:pred 0.2.h:S</ta>
            <ta e="T77" id="Seg_1784" s="T76">np.h:S</ta>
            <ta e="T78" id="Seg_1785" s="T77">v:pred</ta>
            <ta e="T79" id="Seg_1786" s="T78">pro.h:O</ta>
            <ta e="T81" id="Seg_1787" s="T79">v:pred 0.3.h:S</ta>
            <ta e="T82" id="Seg_1788" s="T81">pro.h:S</ta>
            <ta e="T83" id="Seg_1789" s="T82">v:pred</ta>
            <ta e="T84" id="Seg_1790" s="T83">v:pred 0.1.h:S</ta>
            <ta e="T85" id="Seg_1791" s="T84">pro.h:S</ta>
            <ta e="T86" id="Seg_1792" s="T85">pro.h:O</ta>
            <ta e="T88" id="Seg_1793" s="T86">v:pred</ta>
            <ta e="T90" id="Seg_1794" s="T89">v:pred 0.3.h:S</ta>
            <ta e="T92" id="Seg_1795" s="T91">v:pred 0.3.h:S</ta>
            <ta e="T93" id="Seg_1796" s="T92">np.h:O</ta>
            <ta e="T96" id="Seg_1797" s="T95">v:pred 0.3.h:S</ta>
            <ta e="T98" id="Seg_1798" s="T97">ptcl.neg</ta>
            <ta e="T99" id="Seg_1799" s="T98">conv:pred</ta>
            <ta e="T100" id="Seg_1800" s="T99">np.h:O</ta>
            <ta e="T102" id="Seg_1801" s="T101">pro.h:S</ta>
            <ta e="T104" id="Seg_1802" s="T103">v:pred</ta>
            <ta e="T105" id="Seg_1803" s="T104">v:pred 0.3.h:S</ta>
            <ta e="T107" id="Seg_1804" s="T105">v:pred 0.3.h:S</ta>
            <ta e="T110" id="Seg_1805" s="T109">np:O</ta>
            <ta e="T111" id="Seg_1806" s="T110">v:pred</ta>
            <ta e="T113" id="Seg_1807" s="T111">np.h:S</ta>
            <ta e="T115" id="Seg_1808" s="T114">v:pred 0.2.h:S</ta>
            <ta e="T119" id="Seg_1809" s="T118">np.h:S</ta>
            <ta e="T120" id="Seg_1810" s="T119">ptcl.neg</ta>
            <ta e="T122" id="Seg_1811" s="T120">v:pred</ta>
            <ta e="T123" id="Seg_1812" s="T122">v:pred 0.1.h:S</ta>
            <ta e="T124" id="Seg_1813" s="T123">pro.h:S</ta>
            <ta e="T128" id="Seg_1814" s="T127">v:pred</ta>
            <ta e="T133" id="Seg_1815" s="T131">v:pred 0.3.h:S</ta>
            <ta e="T134" id="Seg_1816" s="T133">v:pred 0.2.h:S</ta>
            <ta e="T138" id="Seg_1817" s="T137">np:O</ta>
            <ta e="T141" id="Seg_1818" s="T139">v:pred 0.1.h:S</ta>
            <ta e="T142" id="Seg_1819" s="T141">pro.h:S</ta>
            <ta e="T145" id="Seg_1820" s="T143">v:pred</ta>
            <ta e="T147" id="Seg_1821" s="T146">np.h:S</ta>
            <ta e="T149" id="Seg_1822" s="T148">v:pred</ta>
            <ta e="T157" id="Seg_1823" s="T155">np.h:S</ta>
            <ta e="T159" id="Seg_1824" s="T158">v:pred</ta>
            <ta e="T162" id="Seg_1825" s="T161">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T4" id="Seg_1826" s="T3">RUS:core</ta>
            <ta e="T5" id="Seg_1827" s="T4">RUS:gram</ta>
            <ta e="T7" id="Seg_1828" s="T5">RUS:core</ta>
            <ta e="T8" id="Seg_1829" s="T7">RUS:core</ta>
            <ta e="T9" id="Seg_1830" s="T8">TAT:cult</ta>
            <ta e="T14" id="Seg_1831" s="T13">RUS:gram</ta>
            <ta e="T15" id="Seg_1832" s="T14">RUS:core</ta>
            <ta e="T16" id="Seg_1833" s="T15">RUS:gram</ta>
            <ta e="T17" id="Seg_1834" s="T16">RUS:core</ta>
            <ta e="T22" id="Seg_1835" s="T19">TAT:cult</ta>
            <ta e="T30" id="Seg_1836" s="T29">RUS:core</ta>
            <ta e="T31" id="Seg_1837" s="T30">TAT:cult</ta>
            <ta e="T32" id="Seg_1838" s="T31">TURK:disc</ta>
            <ta e="T41" id="Seg_1839" s="T40">RUS:core</ta>
            <ta e="T63" id="Seg_1840" s="T61">TURK:disc</ta>
            <ta e="T69" id="Seg_1841" s="T67">RUS:cult</ta>
            <ta e="T73" id="Seg_1842" s="T72">RUS:gram</ta>
            <ta e="T75" id="Seg_1843" s="T74">TAT:cult</ta>
            <ta e="T77" id="Seg_1844" s="T76">RUS:core</ta>
            <ta e="T93" id="Seg_1845" s="T92">RUS:core</ta>
            <ta e="T94" id="Seg_1846" s="T93">RUS:gram</ta>
            <ta e="T100" id="Seg_1847" s="T99">RUS:core</ta>
            <ta e="T103" id="Seg_1848" s="T102">TURK:core</ta>
            <ta e="T108" id="Seg_1849" s="T107">RUS:gram</ta>
            <ta e="T116" id="Seg_1850" s="T115">RUS:gram</ta>
            <ta e="T118" id="Seg_1851" s="T117">TAT:cult</ta>
            <ta e="T119" id="Seg_1852" s="T118">RUS:cult</ta>
            <ta e="T139" id="Seg_1853" s="T138">TURK:disc</ta>
            <ta e="T143" id="Seg_1854" s="T142">TURK:disc</ta>
            <ta e="T146" id="Seg_1855" s="T145">RUS:gram</ta>
            <ta e="T147" id="Seg_1856" s="T146">RUS:core</ta>
            <ta e="T151" id="Seg_1857" s="T150">TAT:cult</ta>
            <ta e="T152" id="Seg_1858" s="T151">RUS:gram</ta>
            <ta e="T157" id="Seg_1859" s="T155">RUS:cult</ta>
            <ta e="T158" id="Seg_1860" s="T157">TAT:cult</ta>
            <ta e="T160" id="Seg_1861" s="T159">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T7" id="Seg_1862" s="T1">Жили вдвоём заяц и петух.</ta>
            <ta e="T22" id="Seg_1863" s="T7">У петуха дом был из дерева сделан, а у зайца из снега.</ta>
            <ta e="T28" id="Seg_1864" s="T22">Потом стало тепло, солнце (вышло?).</ta>
            <ta e="T37" id="Seg_1865" s="T28">У зайца дом умер [=растаял], водой стал.</ta>
            <ta e="T43" id="Seg_1866" s="T37">Там мокро стало, он говорит петуху:</ta>
            <ta e="T51" id="Seg_1867" s="T43">«Пусти меня, я не убегу, за тебя (замуж выйду?)». [?]</ta>
            <ta e="T57" id="Seg_1868" s="T51">Тот его впустил, он его выгнал.</ta>
            <ta e="T63" id="Seg_1869" s="T57">Он [=петух] идёт, идёт, плачет.</ta>
            <ta e="T69" id="Seg_1870" s="T63">Тут навстречу ему лиса.</ta>
            <ta e="T72" id="Seg_1871" s="T69">«Почему плачешь?»</ta>
            <ta e="T81" id="Seg_1872" s="T72">«Да в моём доме заяц живёт, меня выгнал».</ta>
            <ta e="T88" id="Seg_1873" s="T81">Она [=лиса] говорит: «Пойдём, я его выгоню».</ta>
            <ta e="T93" id="Seg_1874" s="T88">Потом пришли, она выгнала зайца.</ta>
            <ta e="T100" id="Seg_1875" s="T93">И сама живёт, не пускает петуха.</ta>
            <ta e="T107" id="Seg_1876" s="T100">Тогда он опять идёт, плачет, плачет.</ta>
            <ta e="T113" id="Seg_1877" s="T107">А там люди траву косили.</ta>
            <ta e="T115" id="Seg_1878" s="T113">«Почему ты плачешь?»</ta>
            <ta e="T122" id="Seg_1879" s="T115">«Да [меня] лиса в дом не пускает».</ta>
            <ta e="T130" id="Seg_1880" s="T122">«Пойдём, мы её косой прогоним».</ta>
            <ta e="T133" id="Seg_1881" s="T130">Потом они пришли.</ta>
            <ta e="T141" id="Seg_1882" s="T133">«Уходи отсюда, а то голову косой отрубим».</ta>
            <ta e="T145" id="Seg_1883" s="T141">Она убежала.</ta>
            <ta e="T155" id="Seg_1884" s="T145">А заяц там (?) в дом и теперь (?).</ta>
            <ta e="T162" id="Seg_1885" s="T155">Петух (поселился?) в доме и теперь живёт. </ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T7" id="Seg_1886" s="T1">A hare and a rooster lived together.</ta>
            <ta e="T22" id="Seg_1887" s="T7">The rooster's house was made of wood, and the hare had made [his] house from snow.</ta>
            <ta e="T28" id="Seg_1888" s="T22">Then it became warm, the sun is (moving [on the sky]?).</ta>
            <ta e="T37" id="Seg_1889" s="T28">The hare's house died [= melted], became water.</ta>
            <ta e="T43" id="Seg_1890" s="T37">It got wet there, he says to the rooster:</ta>
            <ta e="T51" id="Seg_1891" s="T43">"Let me [in], I won't run, I'll (marry?) you." [?]</ta>
            <ta e="T57" id="Seg_1892" s="T51">He let [him] in, he chased him away:</ta>
            <ta e="T63" id="Seg_1893" s="T57">He [=the rooster] is going, is going, he's crying.</ta>
            <ta e="T69" id="Seg_1894" s="T63">Then a fox cames to him.</ta>
            <ta e="T72" id="Seg_1895" s="T69">"Why are you crying?"</ta>
            <ta e="T81" id="Seg_1896" s="T72">"The hare is living in my house, he chased me away."</ta>
            <ta e="T88" id="Seg_1897" s="T81">He [=the fox] says: "Let's go, I'll chase him away."</ta>
            <ta e="T93" id="Seg_1898" s="T88">Then they came, he chased the hare away.</ta>
            <ta e="T100" id="Seg_1899" s="T93">And [started] living [there] himself, doesn't let the rooster in.</ta>
            <ta e="T107" id="Seg_1900" s="T100">Then he's going again, crying and crying.</ta>
            <ta e="T113" id="Seg_1901" s="T107">And there men are mowing grass.</ta>
            <ta e="T115" id="Seg_1902" s="T113">"Why are you crying?"</ta>
            <ta e="T122" id="Seg_1903" s="T115">"The fox doesn't let [me] in my house."</ta>
            <ta e="T130" id="Seg_1904" s="T122">"Let's go, we'll chase [him] away with a scythe."</ta>
            <ta e="T133" id="Seg_1905" s="T130">Then they came.</ta>
            <ta e="T141" id="Seg_1906" s="T133">"Go away, or we'll cut your head with the scythe!"</ta>
            <ta e="T145" id="Seg_1907" s="T141">He ran away.</ta>
            <ta e="T155" id="Seg_1908" s="T145">And the hare (?) there in the house and now (?).</ta>
            <ta e="T162" id="Seg_1909" s="T155">The rooster (settled?) in the house and now lives [there].</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T7" id="Seg_1910" s="T1">Ein Hase und ein Hahn lebten zusammen.</ta>
            <ta e="T22" id="Seg_1911" s="T7">Das Haus des Hahns war aus Holz gemacht und der Hase hatte [sein] Haus aus Schnee gemacht.</ta>
            <ta e="T28" id="Seg_1912" s="T22">Dann wurde es warm, die Sonne (bewegt sich [am Himmel]?).</ta>
            <ta e="T37" id="Seg_1913" s="T28">Das Haus des Hasen starb [=schmolz], es wurde zu Wasser.</ta>
            <ta e="T43" id="Seg_1914" s="T37">Er wurde nass dort, er sagt zum Hahn:</ta>
            <ta e="T51" id="Seg_1915" s="T43">"Lass mich [rein], ich laufe nicht, ich werde (dich heiraten?)." [?]</ta>
            <ta e="T57" id="Seg_1916" s="T51">Er ließ [ihn] rein, er jagte ihn weg.</ta>
            <ta e="T63" id="Seg_1917" s="T57">Er [=der Hahn] geht und geht, er weint.</ta>
            <ta e="T69" id="Seg_1918" s="T63">Dann kam ein Fuchs zu ihm.</ta>
            <ta e="T72" id="Seg_1919" s="T69">"Warum weinst du?"</ta>
            <ta e="T81" id="Seg_1920" s="T72">"Der Hase lebt in meinem Haus, er hat mich weggejagt."</ta>
            <ta e="T88" id="Seg_1921" s="T81">Er [=der Fuchs] sagt: "Gehen wir, ich jage ich weg."</ta>
            <ta e="T93" id="Seg_1922" s="T88">Dann kamen sie, er jagte den Hasen weg.</ta>
            <ta e="T100" id="Seg_1923" s="T93">Und selber [dort] lebend lässt er den Hahn nicht rein.</ta>
            <ta e="T107" id="Seg_1924" s="T100">Dann geht er wieder, weinend und weinend.</ta>
            <ta e="T113" id="Seg_1925" s="T107">Und dort mähen Männer Gras.</ta>
            <ta e="T115" id="Seg_1926" s="T113">"Warum weinst du?"</ta>
            <ta e="T122" id="Seg_1927" s="T115">"Der Fuchs lässt [mich] nicht in mein Haus."</ta>
            <ta e="T130" id="Seg_1928" s="T122">"Gehen wir, wir jagen [ihn] mit einer Sense weg."</ta>
            <ta e="T133" id="Seg_1929" s="T130">Dann kamen sie.</ta>
            <ta e="T141" id="Seg_1930" s="T133">"Geh weg, oder wir schneiden deinen Kopf mit der Sense ab!"</ta>
            <ta e="T145" id="Seg_1931" s="T141">Er rannte weg.</ta>
            <ta e="T155" id="Seg_1932" s="T145">Und der Hase (?) dort nach Hause und jetzt (?).</ta>
            <ta e="T162" id="Seg_1933" s="T155">Der Hahn (ließ sich nieder?) in dem Haus und lebt jetzt [dort].</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T7" id="Seg_1934" s="T1">[GVY:] This is a well-known tale about the hare and a fox, but the characters seem to be mixed up at the beginning.</ta>
            <ta e="T57" id="Seg_1935" s="T51">[GVY:] The rooster let the hare in his house, the hare chased the rooster away.</ta>
            <ta e="T93" id="Seg_1936" s="T88">[GVY:] zajaʔsəm</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
