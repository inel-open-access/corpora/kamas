<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID417450DA-5CB9-E8A9-9EE8-3F198D626FF9">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_FingerBoy_flk.wav" />
         <referenced-file url="PKZ_196X_FingerBoy_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_FingerBoy_flk\PKZ_196X_FingerBoy_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">286</ud-information>
            <ud-information attribute-name="# HIAT:w">193</ud-information>
            <ud-information attribute-name="# e">193</ud-information>
            <ud-information attribute-name="# HIAT:u">42</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T568" time="0.052" type="appl" />
         <tli id="T569" time="0.917" type="appl" />
         <tli id="T570" time="1.782" type="appl" />
         <tli id="T571" time="3.2266024975437655" />
         <tli id="T572" time="4.612" type="appl" />
         <tli id="T573" time="6.13" type="appl" />
         <tli id="T574" time="7.647" type="appl" />
         <tli id="T575" time="9.326481186082082" />
         <tli id="T576" time="10.169" type="appl" />
         <tli id="T577" time="11.173" type="appl" />
         <tli id="T578" time="12.177" type="appl" />
         <tli id="T579" time="13.393" type="appl" />
         <tli id="T580" time="14.577" type="appl" />
         <tli id="T581" time="15.761" type="appl" />
         <tli id="T582" time="16.945" type="appl" />
         <tli id="T583" time="18.129" type="appl" />
         <tli id="T584" time="19.313" type="appl" />
         <tli id="T585" time="20.086" type="appl" />
         <tli id="T586" time="20.798" type="appl" />
         <tli id="T587" time="21.509" type="appl" />
         <tli id="T588" time="22.221" type="appl" />
         <tli id="T589" time="22.933" type="appl" />
         <tli id="T590" time="23.62" type="appl" />
         <tli id="T591" time="24.307" type="appl" />
         <tli id="T592" time="24.995" type="appl" />
         <tli id="T593" time="25.682" type="appl" />
         <tli id="T594" time="26.369" type="appl" />
         <tli id="T595" time="27.056" type="appl" />
         <tli id="T596" time="27.743" type="appl" />
         <tli id="T597" time="28.431" type="appl" />
         <tli id="T598" time="29.118" type="appl" />
         <tli id="T599" time="29.805" type="appl" />
         <tli id="T600" time="30.435" type="appl" />
         <tli id="T601" time="31.054" type="appl" />
         <tli id="T602" time="31.81270065760093" />
         <tli id="T603" time="32.928" type="appl" />
         <tli id="T604" time="34.022" type="appl" />
         <tli id="T605" time="35.399295995779745" />
         <tli id="T606" time="36.164" type="appl" />
         <tli id="T607" time="36.726" type="appl" />
         <tli id="T608" time="37.287" type="appl" />
         <tli id="T609" time="38.07257616419926" />
         <tli id="T610" time="38.788" type="appl" />
         <tli id="T611" time="39.556" type="appl" />
         <tli id="T612" time="40.324" type="appl" />
         <tli id="T613" time="41.093" type="appl" />
         <tli id="T614" time="41.861" type="appl" />
         <tli id="T615" time="42.629" type="appl" />
         <tli id="T616" time="43.59246638727" />
         <tli id="T617" time="44.3" type="appl" />
         <tli id="T618" time="45.047" type="appl" />
         <tli id="T619" time="45.794" type="appl" />
         <tli id="T620" time="46.542" type="appl" />
         <tli id="T621" time="47.289" type="appl" />
         <tli id="T622" time="48.036" type="appl" />
         <tli id="T623" time="49.75234388258083" />
         <tli id="T624" time="50.228" type="appl" />
         <tli id="T625" time="50.718" type="appl" />
         <tli id="T626" time="51.207" type="appl" />
         <tli id="T627" time="51.697" type="appl" />
         <tli id="T628" time="52.18033270527866" />
         <tli id="T629" time="52.68" type="appl" />
         <tli id="T630" time="53.17" type="appl" />
         <tli id="T631" time="53.66" type="appl" />
         <tli id="T632" time="54.151" type="appl" />
         <tli id="T633" time="54.641" type="appl" />
         <tli id="T634" time="55.131" type="appl" />
         <tli id="T635" time="55.679" type="appl" />
         <tli id="T636" time="56.228" type="appl" />
         <tli id="T637" time="56.776" type="appl" />
         <tli id="T638" time="57.324" type="appl" />
         <tli id="T639" time="57.744" type="appl" />
         <tli id="T640" time="58.164" type="appl" />
         <tli id="T641" time="58.91216171651706" />
         <tli id="T642" time="59.576" type="appl" />
         <tli id="T643" time="60.17" type="appl" />
         <tli id="T644" time="60.763" type="appl" />
         <tli id="T645" time="61.357" type="appl" />
         <tli id="T646" time="62.192096486747495" />
         <tli id="T647" time="62.896" type="appl" />
         <tli id="T648" time="63.581" type="appl" />
         <tli id="T649" time="64.265" type="appl" />
         <tli id="T650" time="64.949" type="appl" />
         <tli id="T651" time="65.633" type="appl" />
         <tli id="T652" time="66.318" type="appl" />
         <tli id="T653" time="67.37866000552653" />
         <tli id="T654" time="67.93" type="appl" />
         <tli id="T655" time="68.466" type="appl" />
         <tli id="T656" time="69.003" type="appl" />
         <tli id="T657" time="69.54" type="appl" />
         <tli id="T658" time="70.223" type="appl" />
         <tli id="T659" time="70.906" type="appl" />
         <tli id="T660" time="71.588" type="appl" />
         <tli id="T661" time="73.01854784214228" />
         <tli id="T662" time="73.892" type="appl" />
         <tli id="T663" time="74.718" type="appl" />
         <tli id="T664" time="75.545" type="appl" />
         <tli id="T665" time="76.012" type="appl" />
         <tli id="T666" time="76.479" type="appl" />
         <tli id="T667" time="77.171798577617" />
         <tli id="T668" time="77.898" type="appl" />
         <tli id="T669" time="78.80509942864639" />
         <tli id="T670" time="79.437" type="appl" />
         <tli id="T671" time="79.923" type="appl" />
         <tli id="T672" time="80.409" type="appl" />
         <tli id="T673" time="80.895" type="appl" />
         <tli id="T674" time="81.381" type="appl" />
         <tli id="T675" time="81.867" type="appl" />
         <tli id="T676" time="82.353" type="appl" />
         <tli id="T677" time="82.882" type="appl" />
         <tli id="T678" time="83.411" type="appl" />
         <tli id="T679" time="83.941" type="appl" />
         <tli id="T680" time="84.93831078761389" />
         <tli id="T681" time="85.477" type="appl" />
         <tli id="T682" time="86.117" type="appl" />
         <tli id="T683" time="86.756" type="appl" />
         <tli id="T684" time="88.27824436461682" />
         <tli id="T685" time="88.955" type="appl" />
         <tli id="T686" time="89.446" type="appl" />
         <tli id="T687" time="89.938" type="appl" />
         <tli id="T688" time="90.429" type="appl" />
         <tli id="T689" time="90.92" type="appl" />
         <tli id="T690" time="91.411" type="appl" />
         <tli id="T691" time="92.364" type="appl" />
         <tli id="T692" time="93.318" type="appl" />
         <tli id="T693" time="94.271" type="appl" />
         <tli id="T694" time="95.224" type="appl" />
         <tli id="T695" time="95.952" type="appl" />
         <tli id="T696" time="96.681" type="appl" />
         <tli id="T697" time="97.41" type="appl" />
         <tli id="T698" time="98.138" type="appl" />
         <tli id="T699" time="98.549" type="appl" />
         <tli id="T700" time="98.96" type="appl" />
         <tli id="T701" time="99.617" type="appl" />
         <tli id="T702" time="100.274" type="appl" />
         <tli id="T703" time="101.72464361574447" />
         <tli id="T704" time="103.108" type="appl" />
         <tli id="T705" time="105.45790270381163" />
         <tli id="T706" time="106.059" type="appl" />
         <tli id="T707" time="106.525" type="appl" />
         <tli id="T708" time="106.99" type="appl" />
         <tli id="T709" time="107.456" type="appl" />
         <tli id="T710" time="107.922" type="appl" />
         <tli id="T711" time="108.388" type="appl" />
         <tli id="T712" time="109.49" type="appl" />
         <tli id="T713" time="110.592" type="appl" />
         <tli id="T714" time="111.693" type="appl" />
         <tli id="T715" time="112.795" type="appl" />
         <tli id="T716" time="114.61105400366202" />
         <tli id="T717" time="115.553" type="appl" />
         <tli id="T718" time="116.369" type="appl" />
         <tli id="T719" time="117.185" type="appl" />
         <tli id="T720" time="118.001" type="appl" />
         <tli id="T721" time="118.817" type="appl" />
         <tli id="T722" time="120.57760201048366" />
         <tli id="T723" time="121.158" type="appl" />
         <tli id="T724" time="121.767" type="appl" />
         <tli id="T725" time="122.375" type="appl" />
         <tli id="T726" time="122.984" type="appl" />
         <tli id="T727" time="123.83087064437076" />
         <tli id="T728" time="124.883" type="appl" />
         <tli id="T729" time="125.952" type="appl" />
         <tli id="T730" time="126.958" type="appl" />
         <tli id="T731" time="127.965" type="appl" />
         <tli id="T732" time="128.971" type="appl" />
         <tli id="T733" time="129.978" type="appl" />
         <tli id="T734" time="130.984" type="appl" />
         <tli id="T735" time="131.991" type="appl" />
         <tli id="T736" time="132.997" type="appl" />
         <tli id="T737" time="133.586" type="appl" />
         <tli id="T738" time="134.25066342052966" />
         <tli id="T739" time="135.008" type="appl" />
         <tli id="T740" time="135.841" type="appl" />
         <tli id="T741" time="136.7372806345458" />
         <tli id="T742" time="137.139" type="appl" />
         <tli id="T743" time="137.604" type="appl" />
         <tli id="T744" time="138.07" type="appl" />
         <tli id="T745" time="138.536" type="appl" />
         <tli id="T746" time="139.002" type="appl" />
         <tli id="T747" time="139.467" type="appl" />
         <tli id="T748" time="140.92386404044973" />
         <tli id="T749" time="141.514" type="appl" />
         <tli id="T750" time="141.994" type="appl" />
         <tli id="T751" time="142.473" type="appl" />
         <tli id="T752" time="142.952" type="appl" />
         <tli id="T753" time="143.432" type="appl" />
         <tli id="T754" time="143.97713665176178" />
         <tli id="T755" time="144.733" type="appl" />
         <tli id="T756" time="145.556" type="appl" />
         <tli id="T757" time="146.378" type="appl" />
         <tli id="T758" time="147.01" type="appl" />
         <tli id="T759" time="147.642" type="appl" />
         <tli id="T760" time="148.274" type="appl" />
         <tli id="T761" time="149.277" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T761" id="Seg_0" n="sc" s="T568">
               <ts e="T571" id="Seg_2" n="HIAT:u" s="T568">
                  <ts e="T569" id="Seg_4" n="HIAT:w" s="T568">Amnobiʔi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_7" n="HIAT:w" s="T569">nüke</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_10" n="HIAT:w" s="T570">büzʼezʼiʔ</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T575" id="Seg_14" n="HIAT:u" s="T571">
                  <ts e="T572" id="Seg_16" n="HIAT:w" s="T571">Nüke</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_19" n="HIAT:w" s="T572">davaj</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_22" n="HIAT:w" s="T573">dʼagarzittə</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_25" n="HIAT:w" s="T574">kapustam</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T578" id="Seg_29" n="HIAT:u" s="T575">
                  <ts e="T576" id="Seg_31" n="HIAT:w" s="T575">I</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_34" n="HIAT:w" s="T576">müjət</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_37" n="HIAT:w" s="T577">sajjaʔpi</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T584" id="Seg_41" n="HIAT:u" s="T578">
                  <ts e="T579" id="Seg_43" n="HIAT:w" s="T578">Dĭgəttə</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_46" n="HIAT:w" s="T579">dĭm</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_49" n="HIAT:w" s="T580">sarobi</ts>
                  <nts id="Seg_50" n="HIAT:ip">,</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_53" n="HIAT:w" s="T581">dĭn</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_56" n="HIAT:w" s="T582">embi</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_59" n="HIAT:w" s="T583">stoldə</ts>
                  <nts id="Seg_60" n="HIAT:ip">.</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T589" id="Seg_63" n="HIAT:u" s="T584">
                  <ts e="T585" id="Seg_65" n="HIAT:w" s="T584">Dĭgəttə</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_68" n="HIAT:w" s="T585">kuliat</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_71" n="HIAT:w" s="T586">bar</ts>
                  <nts id="Seg_72" n="HIAT:ip">,</nts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_75" n="HIAT:w" s="T587">tʼorlaʔbə</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_78" n="HIAT:w" s="T588">müjət</ts>
                  <nts id="Seg_79" n="HIAT:ip">.</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T599" id="Seg_82" n="HIAT:u" s="T589">
                  <ts e="T590" id="Seg_84" n="HIAT:w" s="T589">Dĭgəttə</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_87" n="HIAT:w" s="T590">dĭ</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_90" n="HIAT:w" s="T591">kubi:</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_93" n="HIAT:w" s="T592">dĭn</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_96" n="HIAT:w" s="T593">ešši</ts>
                  <nts id="Seg_97" n="HIAT:ip">,</nts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_99" n="HIAT:ip">(</nts>
                  <ts e="T595" id="Seg_101" n="HIAT:w" s="T594">nerer-</ts>
                  <nts id="Seg_102" n="HIAT:ip">)</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_105" n="HIAT:w" s="T595">nereʔluʔpi</ts>
                  <nts id="Seg_106" n="HIAT:ip">,</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_109" n="HIAT:w" s="T596">dĭn</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_112" n="HIAT:w" s="T597">ešši</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_115" n="HIAT:w" s="T598">iʔbölaʔbə</ts>
                  <nts id="Seg_116" n="HIAT:ip">.</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T602" id="Seg_119" n="HIAT:u" s="T599">
                  <nts id="Seg_120" n="HIAT:ip">"</nts>
                  <ts e="T600" id="Seg_122" n="HIAT:w" s="T599">Măn</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_125" n="HIAT:w" s="T600">tăn</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_128" n="HIAT:w" s="T601">nʼil</ts>
                  <nts id="Seg_129" n="HIAT:ip">!</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T605" id="Seg_132" n="HIAT:u" s="T602">
                  <ts e="T603" id="Seg_134" n="HIAT:w" s="T602">Tăn</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_137" n="HIAT:w" s="T603">müjəzeŋdə</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_140" n="HIAT:w" s="T604">supsobiam</ts>
                  <nts id="Seg_141" n="HIAT:ip">!</nts>
                  <nts id="Seg_142" n="HIAT:ip">"</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T609" id="Seg_145" n="HIAT:u" s="T605">
                  <ts e="T606" id="Seg_147" n="HIAT:w" s="T605">Dĭgəttə</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_150" n="HIAT:w" s="T606">dĭ</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_153" n="HIAT:w" s="T607">ibi</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_155" n="HIAT:ip">(</nts>
                  <ts e="T609" id="Seg_157" n="HIAT:w" s="T608">dĭm</ts>
                  <nts id="Seg_158" n="HIAT:ip">)</nts>
                  <nts id="Seg_159" n="HIAT:ip">.</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T616" id="Seg_162" n="HIAT:u" s="T609">
                  <ts e="T610" id="Seg_164" n="HIAT:w" s="T609">Dĭ</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_167" n="HIAT:w" s="T610">măndə:</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_169" n="HIAT:ip">"</nts>
                  <ts e="T612" id="Seg_171" n="HIAT:w" s="T611">Măn</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_174" n="HIAT:w" s="T612">kallam</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_177" n="HIAT:w" s="T613">abandə</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_179" n="HIAT:ip">(</nts>
                  <ts e="T615" id="Seg_181" n="HIAT:w" s="T614">talə-</ts>
                  <nts id="Seg_182" n="HIAT:ip">)</nts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_185" n="HIAT:w" s="T615">tarirzittə</ts>
                  <nts id="Seg_186" n="HIAT:ip">"</nts>
                  <nts id="Seg_187" n="HIAT:ip">.</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T623" id="Seg_190" n="HIAT:u" s="T616">
                  <nts id="Seg_191" n="HIAT:ip">(</nts>
                  <ts e="T617" id="Seg_193" n="HIAT:w" s="T616">Ga-</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_196" n="HIAT:w" s="T617">šu-</ts>
                  <nts id="Seg_197" n="HIAT:ip">)</nts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_200" n="HIAT:w" s="T618">Šonəga</ts>
                  <nts id="Seg_201" n="HIAT:ip">,</nts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_204" n="HIAT:w" s="T619">šobi</ts>
                  <nts id="Seg_205" n="HIAT:ip">,</nts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_208" n="HIAT:w" s="T620">abat</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_211" n="HIAT:w" s="T621">tarirlaʔbə</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_214" n="HIAT:w" s="T622">inetsiʔ</ts>
                  <nts id="Seg_215" n="HIAT:ip">.</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T628" id="Seg_218" n="HIAT:u" s="T623">
                  <ts e="T624" id="Seg_220" n="HIAT:w" s="T623">Măndə:</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_222" n="HIAT:ip">"</nts>
                  <ts e="T625" id="Seg_224" n="HIAT:w" s="T624">Abam</ts>
                  <nts id="Seg_225" n="HIAT:ip">,</nts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_228" n="HIAT:w" s="T625">măn</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_231" n="HIAT:w" s="T626">tănan</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_234" n="HIAT:w" s="T627">šobiam</ts>
                  <nts id="Seg_235" n="HIAT:ip">"</nts>
                  <nts id="Seg_236" n="HIAT:ip">.</nts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T634" id="Seg_239" n="HIAT:u" s="T628">
                  <ts e="T629" id="Seg_241" n="HIAT:w" s="T628">A</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_244" n="HIAT:w" s="T629">dĭ</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_247" n="HIAT:w" s="T630">mămbi:</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_249" n="HIAT:ip">"</nts>
                  <ts e="T632" id="Seg_251" n="HIAT:w" s="T631">Gijen</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_254" n="HIAT:w" s="T632">šində</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_257" n="HIAT:w" s="T633">dʼăbaktərlaʔbə</ts>
                  <nts id="Seg_258" n="HIAT:ip">?</nts>
                  <nts id="Seg_259" n="HIAT:ip">"</nts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T638" id="Seg_262" n="HIAT:u" s="T634">
                  <ts e="T635" id="Seg_264" n="HIAT:w" s="T634">Dĭgəttə</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_267" n="HIAT:w" s="T635">kuluʔpi</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_270" n="HIAT:w" s="T636">dĭ</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_273" n="HIAT:w" s="T637">eššim</ts>
                  <nts id="Seg_274" n="HIAT:ip">.</nts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T641" id="Seg_277" n="HIAT:u" s="T638">
                  <nts id="Seg_278" n="HIAT:ip">"</nts>
                  <ts e="T639" id="Seg_280" n="HIAT:w" s="T638">Măn</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_283" n="HIAT:w" s="T639">tăn</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_286" n="HIAT:w" s="T640">nʼil</ts>
                  <nts id="Seg_287" n="HIAT:ip">!</nts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T646" id="Seg_290" n="HIAT:u" s="T641">
                  <ts e="T642" id="Seg_292" n="HIAT:w" s="T641">Amnaʔ</ts>
                  <nts id="Seg_293" n="HIAT:ip">,</nts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_296" n="HIAT:w" s="T642">amoraʔ</ts>
                  <nts id="Seg_297" n="HIAT:ip">,</nts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_300" n="HIAT:w" s="T643">a</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_303" n="HIAT:w" s="T644">măn</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_306" n="HIAT:w" s="T645">tarirlam</ts>
                  <nts id="Seg_307" n="HIAT:ip">"</nts>
                  <nts id="Seg_308" n="HIAT:ip">.</nts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T653" id="Seg_311" n="HIAT:u" s="T646">
                  <ts e="T647" id="Seg_313" n="HIAT:w" s="T646">Dĭgəttə</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_315" n="HIAT:ip">(</nts>
                  <ts e="T648" id="Seg_317" n="HIAT:w" s="T647">i-</ts>
                  <nts id="Seg_318" n="HIAT:ip">)</nts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_321" n="HIAT:w" s="T648">inen</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_324" n="HIAT:w" s="T649">kunə</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_327" n="HIAT:w" s="T650">păʔlaːmbi</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_330" n="HIAT:w" s="T651">i</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_333" n="HIAT:w" s="T652">tarirlaʔbə</ts>
                  <nts id="Seg_334" n="HIAT:ip">.</nts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T657" id="Seg_337" n="HIAT:u" s="T653">
                  <ts e="T654" id="Seg_339" n="HIAT:w" s="T653">A</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_342" n="HIAT:w" s="T654">dĭ</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_345" n="HIAT:w" s="T655">büzʼe</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_348" n="HIAT:w" s="T656">amorlaʔbə</ts>
                  <nts id="Seg_349" n="HIAT:ip">.</nts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T661" id="Seg_352" n="HIAT:u" s="T657">
                  <ts e="T658" id="Seg_354" n="HIAT:w" s="T657">Dĭgəttə</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_357" n="HIAT:w" s="T658">šonəga</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_360" n="HIAT:w" s="T659">koŋ</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_363" n="HIAT:w" s="T660">kuza</ts>
                  <nts id="Seg_364" n="HIAT:ip">.</nts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T664" id="Seg_367" n="HIAT:u" s="T661">
                  <ts e="T662" id="Seg_369" n="HIAT:w" s="T661">Kuliat:</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_372" n="HIAT:w" s="T662">ineʔi</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_375" n="HIAT:w" s="T663">mĭlleʔbəʔjə</ts>
                  <nts id="Seg_376" n="HIAT:ip">.</nts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T667" id="Seg_379" n="HIAT:u" s="T664">
                  <ts e="T665" id="Seg_381" n="HIAT:w" s="T664">A</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_384" n="HIAT:w" s="T665">kuza</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_387" n="HIAT:w" s="T666">naga</ts>
                  <nts id="Seg_388" n="HIAT:ip">.</nts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T676" id="Seg_391" n="HIAT:u" s="T667">
                  <ts e="T668" id="Seg_393" n="HIAT:w" s="T667">Dĭgəttə</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_396" n="HIAT:w" s="T668">măndə:</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_398" n="HIAT:ip">"</nts>
                  <ts e="T670" id="Seg_400" n="HIAT:w" s="T669">Ĭmbi</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_403" n="HIAT:w" s="T670">ineʔi</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_406" n="HIAT:w" s="T671">mĭlleʔbə</ts>
                  <nts id="Seg_407" n="HIAT:ip">,</nts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_410" n="HIAT:w" s="T672">a</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_413" n="HIAT:w" s="T673">ĭmbi</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_416" n="HIAT:w" s="T674">ej</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_418" n="HIAT:ip">(</nts>
                  <ts e="T676" id="Seg_420" n="HIAT:w" s="T675">kullial</ts>
                  <nts id="Seg_421" n="HIAT:ip">)</nts>
                  <nts id="Seg_422" n="HIAT:ip">"</nts>
                  <nts id="Seg_423" n="HIAT:ip">.</nts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T680" id="Seg_426" n="HIAT:u" s="T676">
                  <nts id="Seg_427" n="HIAT:ip">"</nts>
                  <ts e="T677" id="Seg_429" n="HIAT:w" s="T676">Dĭ</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_432" n="HIAT:w" s="T677">man</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_435" n="HIAT:w" s="T678">nʼim</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_438" n="HIAT:w" s="T679">tarirlaʔbə</ts>
                  <nts id="Seg_439" n="HIAT:ip">"</nts>
                  <nts id="Seg_440" n="HIAT:ip">.</nts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T684" id="Seg_443" n="HIAT:u" s="T680">
                  <ts e="T681" id="Seg_445" n="HIAT:w" s="T680">Dĭgəttə:</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_447" n="HIAT:ip">"</nts>
                  <ts e="T682" id="Seg_449" n="HIAT:w" s="T681">Sadardə</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_452" n="HIAT:w" s="T682">măna</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_455" n="HIAT:w" s="T683">dĭm</ts>
                  <nts id="Seg_456" n="HIAT:ip">!</nts>
                  <nts id="Seg_457" n="HIAT:ip">"</nts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T690" id="Seg_460" n="HIAT:u" s="T684">
                  <ts e="T685" id="Seg_462" n="HIAT:w" s="T684">Dĭ</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_465" n="HIAT:w" s="T685">sadarbi</ts>
                  <nts id="Seg_466" n="HIAT:ip">,</nts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_469" n="HIAT:w" s="T686">dĭ</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_472" n="HIAT:w" s="T687">iʔgö</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_475" n="HIAT:w" s="T688">aktʼa</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_478" n="HIAT:w" s="T689">mĭbi</ts>
                  <nts id="Seg_479" n="HIAT:ip">.</nts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T694" id="Seg_482" n="HIAT:u" s="T690">
                  <ts e="T691" id="Seg_484" n="HIAT:w" s="T690">Dĭgəttə</ts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_487" n="HIAT:w" s="T691">embi</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_490" n="HIAT:w" s="T692">dĭm</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_493" n="HIAT:w" s="T693">kărmandə</ts>
                  <nts id="Seg_494" n="HIAT:ip">.</nts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T698" id="Seg_497" n="HIAT:u" s="T694">
                  <ts e="T695" id="Seg_499" n="HIAT:w" s="T694">Dĭ</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_502" n="HIAT:w" s="T695">kărmandə</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_505" n="HIAT:w" s="T696">bar</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_508" n="HIAT:w" s="T697">sajnʼeʔluʔpi</ts>
                  <nts id="Seg_509" n="HIAT:ip">.</nts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T700" id="Seg_512" n="HIAT:u" s="T698">
                  <ts e="T699" id="Seg_514" n="HIAT:w" s="T698">Kalla</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_517" n="HIAT:w" s="T699">dʼürbi</ts>
                  <nts id="Seg_518" n="HIAT:ip">.</nts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T703" id="Seg_521" n="HIAT:u" s="T700">
                  <ts e="T701" id="Seg_523" n="HIAT:w" s="T700">Dĭgəttə</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_526" n="HIAT:w" s="T701">šobi</ts>
                  <nts id="Seg_527" n="HIAT:ip">,</nts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_530" n="HIAT:w" s="T702">šobi</ts>
                  <nts id="Seg_531" n="HIAT:ip">.</nts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T705" id="Seg_534" n="HIAT:u" s="T703">
                  <ts e="T704" id="Seg_536" n="HIAT:w" s="T703">Aktʼigən</ts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_539" n="HIAT:w" s="T704">kunolluʔpi</ts>
                  <nts id="Seg_540" n="HIAT:ip">.</nts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T711" id="Seg_543" n="HIAT:u" s="T705">
                  <ts e="T706" id="Seg_545" n="HIAT:w" s="T705">A</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_548" n="HIAT:w" s="T706">volk</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_550" n="HIAT:ip">(</nts>
                  <ts e="T708" id="Seg_552" n="HIAT:w" s="T707">tüj</ts>
                  <nts id="Seg_553" n="HIAT:ip">)</nts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_556" n="HIAT:w" s="T708">šobi</ts>
                  <nts id="Seg_557" n="HIAT:ip">,</nts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_560" n="HIAT:w" s="T709">dĭm</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_563" n="HIAT:w" s="T710">amnuʔpi</ts>
                  <nts id="Seg_564" n="HIAT:ip">.</nts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T716" id="Seg_567" n="HIAT:u" s="T711">
                  <ts e="T712" id="Seg_569" n="HIAT:w" s="T711">Dĭgəttə</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_572" n="HIAT:w" s="T712">ularəʔi</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_575" n="HIAT:w" s="T713">bar</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_578" n="HIAT:w" s="T714">pastux</ts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_581" n="HIAT:w" s="T715">măndəlia</ts>
                  <nts id="Seg_582" n="HIAT:ip">.</nts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T722" id="Seg_585" n="HIAT:u" s="T716">
                  <ts e="T717" id="Seg_587" n="HIAT:w" s="T716">Dĭ</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_590" n="HIAT:w" s="T717">volk</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_593" n="HIAT:w" s="T718">ularə</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_596" n="HIAT:w" s="T719">tolʼko</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_599" n="HIAT:w" s="T720">năčʼnʼot</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_602" n="HIAT:w" s="T721">izittə</ts>
                  <nts id="Seg_603" n="HIAT:ip">.</nts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T727" id="Seg_606" n="HIAT:u" s="T722">
                  <ts e="T723" id="Seg_608" n="HIAT:w" s="T722">A</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_611" n="HIAT:w" s="T723">dĭ</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_614" n="HIAT:w" s="T724">nʼi</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_617" n="HIAT:w" s="T725">kirgarlaʔbə:</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_619" n="HIAT:ip">"</nts>
                  <ts e="T727" id="Seg_621" n="HIAT:w" s="T726">Uʔbdaʔ</ts>
                  <nts id="Seg_622" n="HIAT:ip">!</nts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T729" id="Seg_625" n="HIAT:u" s="T727">
                  <ts e="T728" id="Seg_627" n="HIAT:w" s="T727">Ular</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_630" n="HIAT:w" s="T728">amnaʔbə</ts>
                  <nts id="Seg_631" n="HIAT:ip">!</nts>
                  <nts id="Seg_632" n="HIAT:ip">"</nts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T736" id="Seg_635" n="HIAT:u" s="T729">
                  <ts e="T730" id="Seg_637" n="HIAT:w" s="T729">Dĭgəttə</ts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_640" n="HIAT:w" s="T730">kondʼo</ts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_643" n="HIAT:w" s="T731">amnobi</ts>
                  <nts id="Seg_644" n="HIAT:ip">,</nts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_647" n="HIAT:w" s="T732">dĭ</ts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_650" n="HIAT:w" s="T733">ugaːndə</ts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_653" n="HIAT:w" s="T734">pujolia</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_656" n="HIAT:w" s="T735">volk</ts>
                  <nts id="Seg_657" n="HIAT:ip">.</nts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T741" id="Seg_660" n="HIAT:u" s="T736">
                  <ts e="T737" id="Seg_662" n="HIAT:w" s="T736">Dĭgəttə</ts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_665" n="HIAT:w" s="T737">măndə:</ts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_667" n="HIAT:ip">"</nts>
                  <ts e="T739" id="Seg_669" n="HIAT:w" s="T738">Kanaʔ</ts>
                  <nts id="Seg_670" n="HIAT:ip">,</nts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_673" n="HIAT:w" s="T739">supsoʔ</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_676" n="HIAT:w" s="T740">măna</ts>
                  <nts id="Seg_677" n="HIAT:ip">!</nts>
                  <nts id="Seg_678" n="HIAT:ip">"</nts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T748" id="Seg_681" n="HIAT:u" s="T741">
                  <ts e="T742" id="Seg_683" n="HIAT:w" s="T741">A</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_686" n="HIAT:w" s="T742">dĭ</ts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_689" n="HIAT:w" s="T743">măndə:</ts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_691" n="HIAT:ip">"</nts>
                  <ts e="T745" id="Seg_693" n="HIAT:w" s="T744">Kunaʔ</ts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_696" n="HIAT:w" s="T745">abanə</ts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_699" n="HIAT:w" s="T746">da</ts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_702" n="HIAT:w" s="T747">ijanə</ts>
                  <nts id="Seg_703" n="HIAT:ip">!</nts>
                  <nts id="Seg_704" n="HIAT:ip">"</nts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T754" id="Seg_707" n="HIAT:u" s="T748">
                  <ts e="T749" id="Seg_709" n="HIAT:w" s="T748">Dĭ</ts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_712" n="HIAT:w" s="T749">dĭm</ts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_715" n="HIAT:w" s="T750">deʔpi</ts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_718" n="HIAT:w" s="T751">abanə</ts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_721" n="HIAT:w" s="T752">da</ts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_724" n="HIAT:w" s="T753">ijanə</ts>
                  <nts id="Seg_725" n="HIAT:ip">.</nts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T757" id="Seg_728" n="HIAT:u" s="T754">
                  <ts e="T755" id="Seg_730" n="HIAT:w" s="T754">Turandə</ts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_733" n="HIAT:w" s="T755">dĭ</ts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_736" n="HIAT:w" s="T756">barəʔluʔpi</ts>
                  <nts id="Seg_737" n="HIAT:ip">.</nts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T760" id="Seg_740" n="HIAT:u" s="T757">
                  <ts e="T758" id="Seg_742" n="HIAT:w" s="T757">A</ts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_745" n="HIAT:w" s="T758">bostə</ts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_748" n="HIAT:w" s="T759">nuʔməluʔpi</ts>
                  <nts id="Seg_749" n="HIAT:ip">.</nts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T761" id="Seg_752" n="HIAT:u" s="T760">
                  <ts e="T761" id="Seg_754" n="HIAT:w" s="T760">Kabarləj</ts>
                  <nts id="Seg_755" n="HIAT:ip">.</nts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T761" id="Seg_757" n="sc" s="T568">
               <ts e="T569" id="Seg_759" n="e" s="T568">Amnobiʔi </ts>
               <ts e="T570" id="Seg_761" n="e" s="T569">nüke </ts>
               <ts e="T571" id="Seg_763" n="e" s="T570">büzʼezʼiʔ. </ts>
               <ts e="T572" id="Seg_765" n="e" s="T571">Nüke </ts>
               <ts e="T573" id="Seg_767" n="e" s="T572">davaj </ts>
               <ts e="T574" id="Seg_769" n="e" s="T573">dʼagarzittə </ts>
               <ts e="T575" id="Seg_771" n="e" s="T574">kapustam. </ts>
               <ts e="T576" id="Seg_773" n="e" s="T575">I </ts>
               <ts e="T577" id="Seg_775" n="e" s="T576">müjət </ts>
               <ts e="T578" id="Seg_777" n="e" s="T577">sajjaʔpi. </ts>
               <ts e="T579" id="Seg_779" n="e" s="T578">Dĭgəttə </ts>
               <ts e="T580" id="Seg_781" n="e" s="T579">dĭm </ts>
               <ts e="T581" id="Seg_783" n="e" s="T580">sarobi, </ts>
               <ts e="T582" id="Seg_785" n="e" s="T581">dĭn </ts>
               <ts e="T583" id="Seg_787" n="e" s="T582">embi </ts>
               <ts e="T584" id="Seg_789" n="e" s="T583">stoldə. </ts>
               <ts e="T585" id="Seg_791" n="e" s="T584">Dĭgəttə </ts>
               <ts e="T586" id="Seg_793" n="e" s="T585">kuliat </ts>
               <ts e="T587" id="Seg_795" n="e" s="T586">bar, </ts>
               <ts e="T588" id="Seg_797" n="e" s="T587">tʼorlaʔbə </ts>
               <ts e="T589" id="Seg_799" n="e" s="T588">müjət. </ts>
               <ts e="T590" id="Seg_801" n="e" s="T589">Dĭgəttə </ts>
               <ts e="T591" id="Seg_803" n="e" s="T590">dĭ </ts>
               <ts e="T592" id="Seg_805" n="e" s="T591">kubi: </ts>
               <ts e="T593" id="Seg_807" n="e" s="T592">dĭn </ts>
               <ts e="T594" id="Seg_809" n="e" s="T593">ešši, </ts>
               <ts e="T595" id="Seg_811" n="e" s="T594">(nerer-) </ts>
               <ts e="T596" id="Seg_813" n="e" s="T595">nereʔluʔpi, </ts>
               <ts e="T597" id="Seg_815" n="e" s="T596">dĭn </ts>
               <ts e="T598" id="Seg_817" n="e" s="T597">ešši </ts>
               <ts e="T599" id="Seg_819" n="e" s="T598">iʔbölaʔbə. </ts>
               <ts e="T600" id="Seg_821" n="e" s="T599">"Măn </ts>
               <ts e="T601" id="Seg_823" n="e" s="T600">tăn </ts>
               <ts e="T602" id="Seg_825" n="e" s="T601">nʼil! </ts>
               <ts e="T603" id="Seg_827" n="e" s="T602">Tăn </ts>
               <ts e="T604" id="Seg_829" n="e" s="T603">müjəzeŋdə </ts>
               <ts e="T605" id="Seg_831" n="e" s="T604">supsobiam!" </ts>
               <ts e="T606" id="Seg_833" n="e" s="T605">Dĭgəttə </ts>
               <ts e="T607" id="Seg_835" n="e" s="T606">dĭ </ts>
               <ts e="T608" id="Seg_837" n="e" s="T607">ibi </ts>
               <ts e="T609" id="Seg_839" n="e" s="T608">(dĭm). </ts>
               <ts e="T610" id="Seg_841" n="e" s="T609">Dĭ </ts>
               <ts e="T611" id="Seg_843" n="e" s="T610">măndə: </ts>
               <ts e="T612" id="Seg_845" n="e" s="T611">"Măn </ts>
               <ts e="T613" id="Seg_847" n="e" s="T612">kallam </ts>
               <ts e="T614" id="Seg_849" n="e" s="T613">abandə </ts>
               <ts e="T615" id="Seg_851" n="e" s="T614">(talə-) </ts>
               <ts e="T616" id="Seg_853" n="e" s="T615">tarirzittə". </ts>
               <ts e="T617" id="Seg_855" n="e" s="T616">(Ga- </ts>
               <ts e="T618" id="Seg_857" n="e" s="T617">šu-) </ts>
               <ts e="T619" id="Seg_859" n="e" s="T618">Šonəga, </ts>
               <ts e="T620" id="Seg_861" n="e" s="T619">šobi, </ts>
               <ts e="T621" id="Seg_863" n="e" s="T620">abat </ts>
               <ts e="T622" id="Seg_865" n="e" s="T621">tarirlaʔbə </ts>
               <ts e="T623" id="Seg_867" n="e" s="T622">inetsiʔ. </ts>
               <ts e="T624" id="Seg_869" n="e" s="T623">Măndə: </ts>
               <ts e="T625" id="Seg_871" n="e" s="T624">"Abam, </ts>
               <ts e="T626" id="Seg_873" n="e" s="T625">măn </ts>
               <ts e="T627" id="Seg_875" n="e" s="T626">tănan </ts>
               <ts e="T628" id="Seg_877" n="e" s="T627">šobiam". </ts>
               <ts e="T629" id="Seg_879" n="e" s="T628">A </ts>
               <ts e="T630" id="Seg_881" n="e" s="T629">dĭ </ts>
               <ts e="T631" id="Seg_883" n="e" s="T630">mămbi: </ts>
               <ts e="T632" id="Seg_885" n="e" s="T631">"Gijen </ts>
               <ts e="T633" id="Seg_887" n="e" s="T632">šində </ts>
               <ts e="T634" id="Seg_889" n="e" s="T633">dʼăbaktərlaʔbə?" </ts>
               <ts e="T635" id="Seg_891" n="e" s="T634">Dĭgəttə </ts>
               <ts e="T636" id="Seg_893" n="e" s="T635">kuluʔpi </ts>
               <ts e="T637" id="Seg_895" n="e" s="T636">dĭ </ts>
               <ts e="T638" id="Seg_897" n="e" s="T637">eššim. </ts>
               <ts e="T639" id="Seg_899" n="e" s="T638">"Măn </ts>
               <ts e="T640" id="Seg_901" n="e" s="T639">tăn </ts>
               <ts e="T641" id="Seg_903" n="e" s="T640">nʼil! </ts>
               <ts e="T642" id="Seg_905" n="e" s="T641">Amnaʔ, </ts>
               <ts e="T643" id="Seg_907" n="e" s="T642">amoraʔ, </ts>
               <ts e="T644" id="Seg_909" n="e" s="T643">a </ts>
               <ts e="T645" id="Seg_911" n="e" s="T644">măn </ts>
               <ts e="T646" id="Seg_913" n="e" s="T645">tarirlam". </ts>
               <ts e="T647" id="Seg_915" n="e" s="T646">Dĭgəttə </ts>
               <ts e="T648" id="Seg_917" n="e" s="T647">(i-) </ts>
               <ts e="T649" id="Seg_919" n="e" s="T648">inen </ts>
               <ts e="T650" id="Seg_921" n="e" s="T649">kunə </ts>
               <ts e="T651" id="Seg_923" n="e" s="T650">păʔlaːmbi </ts>
               <ts e="T652" id="Seg_925" n="e" s="T651">i </ts>
               <ts e="T653" id="Seg_927" n="e" s="T652">tarirlaʔbə. </ts>
               <ts e="T654" id="Seg_929" n="e" s="T653">A </ts>
               <ts e="T655" id="Seg_931" n="e" s="T654">dĭ </ts>
               <ts e="T656" id="Seg_933" n="e" s="T655">büzʼe </ts>
               <ts e="T657" id="Seg_935" n="e" s="T656">amorlaʔbə. </ts>
               <ts e="T658" id="Seg_937" n="e" s="T657">Dĭgəttə </ts>
               <ts e="T659" id="Seg_939" n="e" s="T658">šonəga </ts>
               <ts e="T660" id="Seg_941" n="e" s="T659">koŋ </ts>
               <ts e="T661" id="Seg_943" n="e" s="T660">kuza. </ts>
               <ts e="T662" id="Seg_945" n="e" s="T661">Kuliat: </ts>
               <ts e="T663" id="Seg_947" n="e" s="T662">ineʔi </ts>
               <ts e="T664" id="Seg_949" n="e" s="T663">mĭlleʔbəʔjə. </ts>
               <ts e="T665" id="Seg_951" n="e" s="T664">A </ts>
               <ts e="T666" id="Seg_953" n="e" s="T665">kuza </ts>
               <ts e="T667" id="Seg_955" n="e" s="T666">naga. </ts>
               <ts e="T668" id="Seg_957" n="e" s="T667">Dĭgəttə </ts>
               <ts e="T669" id="Seg_959" n="e" s="T668">măndə: </ts>
               <ts e="T670" id="Seg_961" n="e" s="T669">"Ĭmbi </ts>
               <ts e="T671" id="Seg_963" n="e" s="T670">ineʔi </ts>
               <ts e="T672" id="Seg_965" n="e" s="T671">mĭlleʔbə, </ts>
               <ts e="T673" id="Seg_967" n="e" s="T672">a </ts>
               <ts e="T674" id="Seg_969" n="e" s="T673">ĭmbi </ts>
               <ts e="T675" id="Seg_971" n="e" s="T674">ej </ts>
               <ts e="T676" id="Seg_973" n="e" s="T675">(kullial)". </ts>
               <ts e="T677" id="Seg_975" n="e" s="T676">"Dĭ </ts>
               <ts e="T678" id="Seg_977" n="e" s="T677">man </ts>
               <ts e="T679" id="Seg_979" n="e" s="T678">nʼim </ts>
               <ts e="T680" id="Seg_981" n="e" s="T679">tarirlaʔbə". </ts>
               <ts e="T681" id="Seg_983" n="e" s="T680">Dĭgəttə: </ts>
               <ts e="T682" id="Seg_985" n="e" s="T681">"Sadardə </ts>
               <ts e="T683" id="Seg_987" n="e" s="T682">măna </ts>
               <ts e="T684" id="Seg_989" n="e" s="T683">dĭm!" </ts>
               <ts e="T685" id="Seg_991" n="e" s="T684">Dĭ </ts>
               <ts e="T686" id="Seg_993" n="e" s="T685">sadarbi, </ts>
               <ts e="T687" id="Seg_995" n="e" s="T686">dĭ </ts>
               <ts e="T688" id="Seg_997" n="e" s="T687">iʔgö </ts>
               <ts e="T689" id="Seg_999" n="e" s="T688">aktʼa </ts>
               <ts e="T690" id="Seg_1001" n="e" s="T689">mĭbi. </ts>
               <ts e="T691" id="Seg_1003" n="e" s="T690">Dĭgəttə </ts>
               <ts e="T692" id="Seg_1005" n="e" s="T691">embi </ts>
               <ts e="T693" id="Seg_1007" n="e" s="T692">dĭm </ts>
               <ts e="T694" id="Seg_1009" n="e" s="T693">kărmandə. </ts>
               <ts e="T695" id="Seg_1011" n="e" s="T694">Dĭ </ts>
               <ts e="T696" id="Seg_1013" n="e" s="T695">kărmandə </ts>
               <ts e="T697" id="Seg_1015" n="e" s="T696">bar </ts>
               <ts e="T698" id="Seg_1017" n="e" s="T697">sajnʼeʔluʔpi. </ts>
               <ts e="T699" id="Seg_1019" n="e" s="T698">Kalla </ts>
               <ts e="T700" id="Seg_1021" n="e" s="T699">dʼürbi. </ts>
               <ts e="T701" id="Seg_1023" n="e" s="T700">Dĭgəttə </ts>
               <ts e="T702" id="Seg_1025" n="e" s="T701">šobi, </ts>
               <ts e="T703" id="Seg_1027" n="e" s="T702">šobi. </ts>
               <ts e="T704" id="Seg_1029" n="e" s="T703">Aktʼigən </ts>
               <ts e="T705" id="Seg_1031" n="e" s="T704">kunolluʔpi. </ts>
               <ts e="T706" id="Seg_1033" n="e" s="T705">A </ts>
               <ts e="T707" id="Seg_1035" n="e" s="T706">volk </ts>
               <ts e="T708" id="Seg_1037" n="e" s="T707">(tüj) </ts>
               <ts e="T709" id="Seg_1039" n="e" s="T708">šobi, </ts>
               <ts e="T710" id="Seg_1041" n="e" s="T709">dĭm </ts>
               <ts e="T711" id="Seg_1043" n="e" s="T710">amnuʔpi. </ts>
               <ts e="T712" id="Seg_1045" n="e" s="T711">Dĭgəttə </ts>
               <ts e="T713" id="Seg_1047" n="e" s="T712">ularəʔi </ts>
               <ts e="T714" id="Seg_1049" n="e" s="T713">bar </ts>
               <ts e="T715" id="Seg_1051" n="e" s="T714">pastux </ts>
               <ts e="T716" id="Seg_1053" n="e" s="T715">măndəlia. </ts>
               <ts e="T717" id="Seg_1055" n="e" s="T716">Dĭ </ts>
               <ts e="T718" id="Seg_1057" n="e" s="T717">volk </ts>
               <ts e="T719" id="Seg_1059" n="e" s="T718">ularə </ts>
               <ts e="T720" id="Seg_1061" n="e" s="T719">tolʼko </ts>
               <ts e="T721" id="Seg_1063" n="e" s="T720">năčʼnʼot </ts>
               <ts e="T722" id="Seg_1065" n="e" s="T721">izittə. </ts>
               <ts e="T723" id="Seg_1067" n="e" s="T722">A </ts>
               <ts e="T724" id="Seg_1069" n="e" s="T723">dĭ </ts>
               <ts e="T725" id="Seg_1071" n="e" s="T724">nʼi </ts>
               <ts e="T726" id="Seg_1073" n="e" s="T725">kirgarlaʔbə: </ts>
               <ts e="T727" id="Seg_1075" n="e" s="T726">"Uʔbdaʔ! </ts>
               <ts e="T728" id="Seg_1077" n="e" s="T727">Ular </ts>
               <ts e="T729" id="Seg_1079" n="e" s="T728">amnaʔbə!" </ts>
               <ts e="T730" id="Seg_1081" n="e" s="T729">Dĭgəttə </ts>
               <ts e="T731" id="Seg_1083" n="e" s="T730">kondʼo </ts>
               <ts e="T732" id="Seg_1085" n="e" s="T731">amnobi, </ts>
               <ts e="T733" id="Seg_1087" n="e" s="T732">dĭ </ts>
               <ts e="T734" id="Seg_1089" n="e" s="T733">ugaːndə </ts>
               <ts e="T735" id="Seg_1091" n="e" s="T734">pujolia </ts>
               <ts e="T736" id="Seg_1093" n="e" s="T735">volk. </ts>
               <ts e="T737" id="Seg_1095" n="e" s="T736">Dĭgəttə </ts>
               <ts e="T738" id="Seg_1097" n="e" s="T737">măndə: </ts>
               <ts e="T739" id="Seg_1099" n="e" s="T738">"Kanaʔ, </ts>
               <ts e="T740" id="Seg_1101" n="e" s="T739">supsoʔ </ts>
               <ts e="T741" id="Seg_1103" n="e" s="T740">măna!" </ts>
               <ts e="T742" id="Seg_1105" n="e" s="T741">A </ts>
               <ts e="T743" id="Seg_1107" n="e" s="T742">dĭ </ts>
               <ts e="T744" id="Seg_1109" n="e" s="T743">măndə: </ts>
               <ts e="T745" id="Seg_1111" n="e" s="T744">"Kunaʔ </ts>
               <ts e="T746" id="Seg_1113" n="e" s="T745">abanə </ts>
               <ts e="T747" id="Seg_1115" n="e" s="T746">da </ts>
               <ts e="T748" id="Seg_1117" n="e" s="T747">ijanə!" </ts>
               <ts e="T749" id="Seg_1119" n="e" s="T748">Dĭ </ts>
               <ts e="T750" id="Seg_1121" n="e" s="T749">dĭm </ts>
               <ts e="T751" id="Seg_1123" n="e" s="T750">deʔpi </ts>
               <ts e="T752" id="Seg_1125" n="e" s="T751">abanə </ts>
               <ts e="T753" id="Seg_1127" n="e" s="T752">da </ts>
               <ts e="T754" id="Seg_1129" n="e" s="T753">ijanə. </ts>
               <ts e="T755" id="Seg_1131" n="e" s="T754">Turandə </ts>
               <ts e="T756" id="Seg_1133" n="e" s="T755">dĭ </ts>
               <ts e="T757" id="Seg_1135" n="e" s="T756">barəʔluʔpi. </ts>
               <ts e="T758" id="Seg_1137" n="e" s="T757">A </ts>
               <ts e="T759" id="Seg_1139" n="e" s="T758">bostə </ts>
               <ts e="T760" id="Seg_1141" n="e" s="T759">nuʔməluʔpi. </ts>
               <ts e="T761" id="Seg_1143" n="e" s="T760">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T571" id="Seg_1144" s="T568">PKZ_196X_FingerBoy_flk.001 (001)</ta>
            <ta e="T575" id="Seg_1145" s="T571">PKZ_196X_FingerBoy_flk.002 (002)</ta>
            <ta e="T578" id="Seg_1146" s="T575">PKZ_196X_FingerBoy_flk.003 (003)</ta>
            <ta e="T584" id="Seg_1147" s="T578">PKZ_196X_FingerBoy_flk.004 (004)</ta>
            <ta e="T589" id="Seg_1148" s="T584">PKZ_196X_FingerBoy_flk.005 (005)</ta>
            <ta e="T599" id="Seg_1149" s="T589">PKZ_196X_FingerBoy_flk.006 (006)</ta>
            <ta e="T602" id="Seg_1150" s="T599">PKZ_196X_FingerBoy_flk.007 (007)</ta>
            <ta e="T605" id="Seg_1151" s="T602">PKZ_196X_FingerBoy_flk.008 (008)</ta>
            <ta e="T609" id="Seg_1152" s="T605">PKZ_196X_FingerBoy_flk.009 (009)</ta>
            <ta e="T616" id="Seg_1153" s="T609">PKZ_196X_FingerBoy_flk.010 (010)</ta>
            <ta e="T623" id="Seg_1154" s="T616">PKZ_196X_FingerBoy_flk.011 (011)</ta>
            <ta e="T628" id="Seg_1155" s="T623">PKZ_196X_FingerBoy_flk.012 (012)</ta>
            <ta e="T634" id="Seg_1156" s="T628">PKZ_196X_FingerBoy_flk.013 (013)</ta>
            <ta e="T638" id="Seg_1157" s="T634">PKZ_196X_FingerBoy_flk.014 (014)</ta>
            <ta e="T641" id="Seg_1158" s="T638">PKZ_196X_FingerBoy_flk.015 (015)</ta>
            <ta e="T646" id="Seg_1159" s="T641">PKZ_196X_FingerBoy_flk.016 (016)</ta>
            <ta e="T653" id="Seg_1160" s="T646">PKZ_196X_FingerBoy_flk.017 (017)</ta>
            <ta e="T657" id="Seg_1161" s="T653">PKZ_196X_FingerBoy_flk.018 (018)</ta>
            <ta e="T661" id="Seg_1162" s="T657">PKZ_196X_FingerBoy_flk.019 (019)</ta>
            <ta e="T664" id="Seg_1163" s="T661">PKZ_196X_FingerBoy_flk.020 (020)</ta>
            <ta e="T667" id="Seg_1164" s="T664">PKZ_196X_FingerBoy_flk.021 (021)</ta>
            <ta e="T676" id="Seg_1165" s="T667">PKZ_196X_FingerBoy_flk.022 (022) </ta>
            <ta e="T680" id="Seg_1166" s="T676">PKZ_196X_FingerBoy_flk.023 (024)</ta>
            <ta e="T684" id="Seg_1167" s="T680">PKZ_196X_FingerBoy_flk.024 (025)</ta>
            <ta e="T690" id="Seg_1168" s="T684">PKZ_196X_FingerBoy_flk.025 (026)</ta>
            <ta e="T694" id="Seg_1169" s="T690">PKZ_196X_FingerBoy_flk.026 (027)</ta>
            <ta e="T698" id="Seg_1170" s="T694">PKZ_196X_FingerBoy_flk.027 (028)</ta>
            <ta e="T700" id="Seg_1171" s="T698">PKZ_196X_FingerBoy_flk.028 (029)</ta>
            <ta e="T703" id="Seg_1172" s="T700">PKZ_196X_FingerBoy_flk.029 (030)</ta>
            <ta e="T705" id="Seg_1173" s="T703">PKZ_196X_FingerBoy_flk.030 (031)</ta>
            <ta e="T711" id="Seg_1174" s="T705">PKZ_196X_FingerBoy_flk.031 (032)</ta>
            <ta e="T716" id="Seg_1175" s="T711">PKZ_196X_FingerBoy_flk.032 (033)</ta>
            <ta e="T722" id="Seg_1176" s="T716">PKZ_196X_FingerBoy_flk.033 (034)</ta>
            <ta e="T727" id="Seg_1177" s="T722">PKZ_196X_FingerBoy_flk.034 (035)</ta>
            <ta e="T729" id="Seg_1178" s="T727">PKZ_196X_FingerBoy_flk.035 (036)</ta>
            <ta e="T736" id="Seg_1179" s="T729">PKZ_196X_FingerBoy_flk.036 (037)</ta>
            <ta e="T741" id="Seg_1180" s="T736">PKZ_196X_FingerBoy_flk.037 (038) </ta>
            <ta e="T748" id="Seg_1181" s="T741">PKZ_196X_FingerBoy_flk.038 (040)</ta>
            <ta e="T754" id="Seg_1182" s="T748">PKZ_196X_FingerBoy_flk.039 (041)</ta>
            <ta e="T757" id="Seg_1183" s="T754">PKZ_196X_FingerBoy_flk.040 (042)</ta>
            <ta e="T760" id="Seg_1184" s="T757">PKZ_196X_FingerBoy_flk.041 (043)</ta>
            <ta e="T761" id="Seg_1185" s="T760">PKZ_196X_FingerBoy_flk.042 (044)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T571" id="Seg_1186" s="T568">Amnobiʔi nüke büzʼezʼiʔ. </ta>
            <ta e="T575" id="Seg_1187" s="T571">Nüke davaj dʼagarzittə kapustam. </ta>
            <ta e="T578" id="Seg_1188" s="T575">I müjət sajjaʔpi. </ta>
            <ta e="T584" id="Seg_1189" s="T578">Dĭgəttə dĭm sarobi, dĭn embi stoldə. </ta>
            <ta e="T589" id="Seg_1190" s="T584">Dĭgəttə kuliat bar, tʼorlaʔbə müjət. </ta>
            <ta e="T599" id="Seg_1191" s="T589">Dĭgəttə dĭ kubi: dĭn ešši, (nerer-) nereʔluʔpi, dĭn ešši iʔbəlaʔbə. </ta>
            <ta e="T602" id="Seg_1192" s="T599">"Măn tăn nʼil! </ta>
            <ta e="T605" id="Seg_1193" s="T602">Tăn müjəzeŋdə supsobiam!" </ta>
            <ta e="T609" id="Seg_1194" s="T605">Dĭgəttə dĭ ibi (dĭm). </ta>
            <ta e="T616" id="Seg_1195" s="T609">Dĭ măndə: "Măn kallam abandə (talə-) tarirzittə". </ta>
            <ta e="T623" id="Seg_1196" s="T616">(Ga- šu-) Šonəga, šobi, abat tarirlaʔbə inetsiʔ. </ta>
            <ta e="T628" id="Seg_1197" s="T623">Măndə: "Abam, măn tănan šobiam". </ta>
            <ta e="T634" id="Seg_1198" s="T628">A dĭ mămbi: "Gijen šində dʼăbaktərlaʔbə?" </ta>
            <ta e="T638" id="Seg_1199" s="T634">Dĭgəttə kuluʔpi dĭ eššim. </ta>
            <ta e="T641" id="Seg_1200" s="T638">"Măn tăn nʼil! </ta>
            <ta e="T646" id="Seg_1201" s="T641">Amnaʔ, amoraʔ, a măn tarirlam". </ta>
            <ta e="T653" id="Seg_1202" s="T646">Dĭgəttə (i-) inən kunə păʔlaːmbi i tarirlaʔbə. </ta>
            <ta e="T657" id="Seg_1203" s="T653">A dĭ büzʼe amorlaʔbə. </ta>
            <ta e="T661" id="Seg_1204" s="T657">Dĭgəttə šonəga koŋ kuza. </ta>
            <ta e="T664" id="Seg_1205" s="T661">Kuliat: ineʔi mĭlleʔbəʔjə. </ta>
            <ta e="T667" id="Seg_1206" s="T664">A kuza naga. </ta>
            <ta e="T676" id="Seg_1207" s="T667">Dĭgəttə măndə: "Ĭmbi ineʔi mĭlleʔbə, a ĭmbi ej (kullial)". </ta>
            <ta e="T680" id="Seg_1208" s="T676">"Dĭ man nʼim tarirlaʔbə". </ta>
            <ta e="T684" id="Seg_1209" s="T680">Dĭgəttə: "Sadardə măna dĭm!" </ta>
            <ta e="T690" id="Seg_1210" s="T684">Dĭ sadarbi, dĭ iʔgö aktʼa mĭbi. </ta>
            <ta e="T694" id="Seg_1211" s="T690">Dĭgəttə embi dĭm kărmandə. </ta>
            <ta e="T698" id="Seg_1212" s="T694">Dĭ kărmandə bar sajnʼeʔluʔpi. </ta>
            <ta e="T700" id="Seg_1213" s="T698">Kalla dʼürbi. </ta>
            <ta e="T703" id="Seg_1214" s="T700">Dĭgəttə šobi, šobi. </ta>
            <ta e="T705" id="Seg_1215" s="T703">Aktʼigən kunolluʔpi. </ta>
            <ta e="T711" id="Seg_1216" s="T705">A volk (tüj) šobi, dĭm amnuʔpi. </ta>
            <ta e="T716" id="Seg_1217" s="T711">Dĭgəttə ularəʔi bar pastux măndəlia. </ta>
            <ta e="T722" id="Seg_1218" s="T716">Dĭ volk ularə tolʼko năčʼnʼot izittə. </ta>
            <ta e="T727" id="Seg_1219" s="T722">A dĭ nʼi kirgarlaʔbə: "Uʔbdaʔ! </ta>
            <ta e="T729" id="Seg_1220" s="T727">Ular amnaʔbə!" </ta>
            <ta e="T736" id="Seg_1221" s="T729">Dĭgəttə kondʼo amnobi, dĭ ugaːndə pujolia volk. </ta>
            <ta e="T741" id="Seg_1222" s="T736">Dĭgəttə măndə: "Kanaʔ, supsoʔ măna!" </ta>
            <ta e="T748" id="Seg_1223" s="T741">A dĭ măndə: "Kunaʔ abanə da ijanə!" </ta>
            <ta e="T754" id="Seg_1224" s="T748">Dĭ dĭm deʔpi abanə da ijanə. </ta>
            <ta e="T757" id="Seg_1225" s="T754">Turandə dĭ barəʔluʔpi. </ta>
            <ta e="T760" id="Seg_1226" s="T757">A bostə nuʔməluʔpi. </ta>
            <ta e="T761" id="Seg_1227" s="T760">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T569" id="Seg_1228" s="T568">amno-bi-ʔi</ta>
            <ta e="T570" id="Seg_1229" s="T569">nüke</ta>
            <ta e="T571" id="Seg_1230" s="T570">büzʼe-zʼiʔ</ta>
            <ta e="T572" id="Seg_1231" s="T571">nüke</ta>
            <ta e="T573" id="Seg_1232" s="T572">davaj</ta>
            <ta e="T574" id="Seg_1233" s="T573">dʼagar-zittə</ta>
            <ta e="T575" id="Seg_1234" s="T574">kăpusta-m</ta>
            <ta e="T576" id="Seg_1235" s="T575">i</ta>
            <ta e="T577" id="Seg_1236" s="T576">müjə-t</ta>
            <ta e="T578" id="Seg_1237" s="T577">saj-jaʔ-pi</ta>
            <ta e="T579" id="Seg_1238" s="T578">dĭgəttə</ta>
            <ta e="T580" id="Seg_1239" s="T579">dĭ-m</ta>
            <ta e="T581" id="Seg_1240" s="T580">saro-bi</ta>
            <ta e="T582" id="Seg_1241" s="T581">dĭn</ta>
            <ta e="T583" id="Seg_1242" s="T582">em-bi</ta>
            <ta e="T584" id="Seg_1243" s="T583">stol-də</ta>
            <ta e="T585" id="Seg_1244" s="T584">dĭgəttə</ta>
            <ta e="T586" id="Seg_1245" s="T585">ku-lia-t</ta>
            <ta e="T587" id="Seg_1246" s="T586">bar</ta>
            <ta e="T588" id="Seg_1247" s="T587">tʼor-laʔbə</ta>
            <ta e="T589" id="Seg_1248" s="T588">müjə-t</ta>
            <ta e="T590" id="Seg_1249" s="T589">dĭgəttə</ta>
            <ta e="T591" id="Seg_1250" s="T590">dĭ</ta>
            <ta e="T592" id="Seg_1251" s="T591">ku-bi</ta>
            <ta e="T593" id="Seg_1252" s="T592">dĭn</ta>
            <ta e="T594" id="Seg_1253" s="T593">ešši</ta>
            <ta e="T596" id="Seg_1254" s="T595">nereʔ-luʔ-pi</ta>
            <ta e="T597" id="Seg_1255" s="T596">dĭn</ta>
            <ta e="T598" id="Seg_1256" s="T597">ešši</ta>
            <ta e="T599" id="Seg_1257" s="T598">iʔbö-laʔbə</ta>
            <ta e="T600" id="Seg_1258" s="T599">măn</ta>
            <ta e="T601" id="Seg_1259" s="T600">tăn</ta>
            <ta e="T602" id="Seg_1260" s="T601">nʼi-l</ta>
            <ta e="T603" id="Seg_1261" s="T602">tăn</ta>
            <ta e="T604" id="Seg_1262" s="T603">müjə-zeŋ-də</ta>
            <ta e="T605" id="Seg_1263" s="T604">supso-bia-m</ta>
            <ta e="T606" id="Seg_1264" s="T605">dĭgəttə</ta>
            <ta e="T607" id="Seg_1265" s="T606">dĭ</ta>
            <ta e="T608" id="Seg_1266" s="T607">i-bi</ta>
            <ta e="T609" id="Seg_1267" s="T608">dĭ-m</ta>
            <ta e="T610" id="Seg_1268" s="T609">dĭ</ta>
            <ta e="T611" id="Seg_1269" s="T610">măn-də</ta>
            <ta e="T612" id="Seg_1270" s="T611">măn</ta>
            <ta e="T613" id="Seg_1271" s="T612">kal-la-m</ta>
            <ta e="T614" id="Seg_1272" s="T613">aba-ndə</ta>
            <ta e="T616" id="Seg_1273" s="T615">tarir-zittə</ta>
            <ta e="T619" id="Seg_1274" s="T618">šonə-ga</ta>
            <ta e="T620" id="Seg_1275" s="T619">šo-bi</ta>
            <ta e="T621" id="Seg_1276" s="T620">aba-t</ta>
            <ta e="T622" id="Seg_1277" s="T621">tarir-laʔbə</ta>
            <ta e="T623" id="Seg_1278" s="T622">ine-t-siʔ</ta>
            <ta e="T624" id="Seg_1279" s="T623">măn-də</ta>
            <ta e="T625" id="Seg_1280" s="T624">aba-m</ta>
            <ta e="T626" id="Seg_1281" s="T625">măn</ta>
            <ta e="T627" id="Seg_1282" s="T626">tănan</ta>
            <ta e="T628" id="Seg_1283" s="T627">šo-bia-m</ta>
            <ta e="T629" id="Seg_1284" s="T628">a</ta>
            <ta e="T630" id="Seg_1285" s="T629">dĭ</ta>
            <ta e="T631" id="Seg_1286" s="T630">măm-bi</ta>
            <ta e="T632" id="Seg_1287" s="T631">gijen</ta>
            <ta e="T633" id="Seg_1288" s="T632">šində</ta>
            <ta e="T634" id="Seg_1289" s="T633">dʼăbaktər-laʔbə</ta>
            <ta e="T635" id="Seg_1290" s="T634">dĭgəttə</ta>
            <ta e="T636" id="Seg_1291" s="T635">ku-luʔ-pi</ta>
            <ta e="T637" id="Seg_1292" s="T636">dĭ</ta>
            <ta e="T638" id="Seg_1293" s="T637">ešši-m</ta>
            <ta e="T639" id="Seg_1294" s="T638">măn</ta>
            <ta e="T640" id="Seg_1295" s="T639">tăn</ta>
            <ta e="T641" id="Seg_1296" s="T640">nʼi-l</ta>
            <ta e="T642" id="Seg_1297" s="T641">amna-ʔ</ta>
            <ta e="T643" id="Seg_1298" s="T642">amor-a-ʔ</ta>
            <ta e="T644" id="Seg_1299" s="T643">a</ta>
            <ta e="T645" id="Seg_1300" s="T644">măn</ta>
            <ta e="T646" id="Seg_1301" s="T645">tarir-la-m</ta>
            <ta e="T647" id="Seg_1302" s="T646">dĭgəttə</ta>
            <ta e="T649" id="Seg_1303" s="T648">ine-n</ta>
            <ta e="T650" id="Seg_1304" s="T649">ku-nə</ta>
            <ta e="T651" id="Seg_1305" s="T650">păʔ-laːm-bi</ta>
            <ta e="T652" id="Seg_1306" s="T651">i</ta>
            <ta e="T653" id="Seg_1307" s="T652">tarir-laʔbə</ta>
            <ta e="T654" id="Seg_1308" s="T653">a</ta>
            <ta e="T655" id="Seg_1309" s="T654">dĭ</ta>
            <ta e="T656" id="Seg_1310" s="T655">büzʼe</ta>
            <ta e="T657" id="Seg_1311" s="T656">amor-laʔbə</ta>
            <ta e="T658" id="Seg_1312" s="T657">dĭgəttə</ta>
            <ta e="T659" id="Seg_1313" s="T658">šonə-ga</ta>
            <ta e="T660" id="Seg_1314" s="T659">koŋ</ta>
            <ta e="T661" id="Seg_1315" s="T660">kuza</ta>
            <ta e="T662" id="Seg_1316" s="T661">ku-lia-t</ta>
            <ta e="T663" id="Seg_1317" s="T662">ine-ʔi</ta>
            <ta e="T664" id="Seg_1318" s="T663">mĭl-leʔbə-ʔjə</ta>
            <ta e="T665" id="Seg_1319" s="T664">a</ta>
            <ta e="T666" id="Seg_1320" s="T665">kuza</ta>
            <ta e="T667" id="Seg_1321" s="T666">naga</ta>
            <ta e="T668" id="Seg_1322" s="T667">dĭgəttə</ta>
            <ta e="T669" id="Seg_1323" s="T668">măn-də</ta>
            <ta e="T670" id="Seg_1324" s="T669">ĭmbi</ta>
            <ta e="T671" id="Seg_1325" s="T670">ine-ʔi</ta>
            <ta e="T672" id="Seg_1326" s="T671">mĭl-leʔbə</ta>
            <ta e="T673" id="Seg_1327" s="T672">a</ta>
            <ta e="T674" id="Seg_1328" s="T673">ĭmbi</ta>
            <ta e="T675" id="Seg_1329" s="T674">ej</ta>
            <ta e="T676" id="Seg_1330" s="T675">ku-l-lia-l</ta>
            <ta e="T677" id="Seg_1331" s="T676">dĭ</ta>
            <ta e="T678" id="Seg_1332" s="T677">man</ta>
            <ta e="T679" id="Seg_1333" s="T678">nʼi-m</ta>
            <ta e="T680" id="Seg_1334" s="T679">tarir-laʔbə</ta>
            <ta e="T681" id="Seg_1335" s="T680">dĭgəttə</ta>
            <ta e="T682" id="Seg_1336" s="T681">sadar-də</ta>
            <ta e="T683" id="Seg_1337" s="T682">măna</ta>
            <ta e="T684" id="Seg_1338" s="T683">dĭ-m</ta>
            <ta e="T685" id="Seg_1339" s="T684">dĭ</ta>
            <ta e="T686" id="Seg_1340" s="T685">sadar-bi</ta>
            <ta e="T687" id="Seg_1341" s="T686">dĭ</ta>
            <ta e="T688" id="Seg_1342" s="T687">iʔgö</ta>
            <ta e="T689" id="Seg_1343" s="T688">aktʼa</ta>
            <ta e="T690" id="Seg_1344" s="T689">mĭ-bi</ta>
            <ta e="T691" id="Seg_1345" s="T690">dĭgəttə</ta>
            <ta e="T692" id="Seg_1346" s="T691">em-bi</ta>
            <ta e="T693" id="Seg_1347" s="T692">dĭ-m</ta>
            <ta e="T694" id="Seg_1348" s="T693">kărman-də</ta>
            <ta e="T695" id="Seg_1349" s="T694">dĭ</ta>
            <ta e="T696" id="Seg_1350" s="T695">kărman-də</ta>
            <ta e="T697" id="Seg_1351" s="T696">bar</ta>
            <ta e="T698" id="Seg_1352" s="T697">saj-nʼeʔ-luʔ-pi</ta>
            <ta e="T699" id="Seg_1353" s="T698">kal-la</ta>
            <ta e="T700" id="Seg_1354" s="T699">dʼür-bi</ta>
            <ta e="T701" id="Seg_1355" s="T700">dĭgəttə</ta>
            <ta e="T702" id="Seg_1356" s="T701">šo-bi</ta>
            <ta e="T703" id="Seg_1357" s="T702">šo-bi</ta>
            <ta e="T704" id="Seg_1358" s="T703">aktʼi-gən</ta>
            <ta e="T705" id="Seg_1359" s="T704">kunol-luʔ-pi</ta>
            <ta e="T706" id="Seg_1360" s="T705">a</ta>
            <ta e="T707" id="Seg_1361" s="T706">volk</ta>
            <ta e="T708" id="Seg_1362" s="T707">tüj</ta>
            <ta e="T709" id="Seg_1363" s="T708">šo-bi</ta>
            <ta e="T710" id="Seg_1364" s="T709">dĭ-m</ta>
            <ta e="T711" id="Seg_1365" s="T710">am-nuʔ-pi</ta>
            <ta e="T712" id="Seg_1366" s="T711">dĭgəttə</ta>
            <ta e="T713" id="Seg_1367" s="T712">ular-əʔi</ta>
            <ta e="T714" id="Seg_1368" s="T713">bar</ta>
            <ta e="T715" id="Seg_1369" s="T714">pastux</ta>
            <ta e="T716" id="Seg_1370" s="T715">măndə-lia</ta>
            <ta e="T717" id="Seg_1371" s="T716">dĭ</ta>
            <ta e="T718" id="Seg_1372" s="T717">volk</ta>
            <ta e="T719" id="Seg_1373" s="T718">ularə</ta>
            <ta e="T720" id="Seg_1374" s="T719">tolʼko</ta>
            <ta e="T722" id="Seg_1375" s="T721">i-zittə</ta>
            <ta e="T723" id="Seg_1376" s="T722">a</ta>
            <ta e="T724" id="Seg_1377" s="T723">dĭ</ta>
            <ta e="T725" id="Seg_1378" s="T724">nʼi</ta>
            <ta e="T726" id="Seg_1379" s="T725">kirgar-laʔbə</ta>
            <ta e="T727" id="Seg_1380" s="T726">uʔbda-ʔ</ta>
            <ta e="T728" id="Seg_1381" s="T727">ular</ta>
            <ta e="T729" id="Seg_1382" s="T728">am-naʔbə</ta>
            <ta e="T730" id="Seg_1383" s="T729">dĭgəttə</ta>
            <ta e="T731" id="Seg_1384" s="T730">kondʼo</ta>
            <ta e="T732" id="Seg_1385" s="T731">amno-bi</ta>
            <ta e="T733" id="Seg_1386" s="T732">dĭ</ta>
            <ta e="T734" id="Seg_1387" s="T733">ugaːndə</ta>
            <ta e="T735" id="Seg_1388" s="T734">pujo-lia</ta>
            <ta e="T736" id="Seg_1389" s="T735">volk</ta>
            <ta e="T737" id="Seg_1390" s="T736">dĭgəttə</ta>
            <ta e="T738" id="Seg_1391" s="T737">măn-də</ta>
            <ta e="T739" id="Seg_1392" s="T738">kan-a-ʔ</ta>
            <ta e="T740" id="Seg_1393" s="T739">supso-ʔ</ta>
            <ta e="T741" id="Seg_1394" s="T740">măna</ta>
            <ta e="T742" id="Seg_1395" s="T741">a</ta>
            <ta e="T743" id="Seg_1396" s="T742">dĭ</ta>
            <ta e="T744" id="Seg_1397" s="T743">măn-də</ta>
            <ta e="T745" id="Seg_1398" s="T744">kun-a-ʔ</ta>
            <ta e="T746" id="Seg_1399" s="T745">aba-nə</ta>
            <ta e="T747" id="Seg_1400" s="T746">da</ta>
            <ta e="T748" id="Seg_1401" s="T747">ija-nə</ta>
            <ta e="T749" id="Seg_1402" s="T748">dĭ</ta>
            <ta e="T750" id="Seg_1403" s="T749">dĭ-m</ta>
            <ta e="T751" id="Seg_1404" s="T750">deʔ-pi</ta>
            <ta e="T752" id="Seg_1405" s="T751">aba-nə</ta>
            <ta e="T753" id="Seg_1406" s="T752">da</ta>
            <ta e="T754" id="Seg_1407" s="T753">ija-nə</ta>
            <ta e="T755" id="Seg_1408" s="T754">tura-ndə</ta>
            <ta e="T756" id="Seg_1409" s="T755">dĭ</ta>
            <ta e="T757" id="Seg_1410" s="T756">barəʔ-luʔ-pi</ta>
            <ta e="T758" id="Seg_1411" s="T757">a</ta>
            <ta e="T759" id="Seg_1412" s="T758">bos-tə</ta>
            <ta e="T760" id="Seg_1413" s="T759">nuʔmə-luʔ-pi</ta>
            <ta e="T761" id="Seg_1414" s="T760">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T569" id="Seg_1415" s="T568">amno-bi-jəʔ</ta>
            <ta e="T570" id="Seg_1416" s="T569">nüke</ta>
            <ta e="T571" id="Seg_1417" s="T570">büzʼe-ziʔ</ta>
            <ta e="T572" id="Seg_1418" s="T571">nüke</ta>
            <ta e="T573" id="Seg_1419" s="T572">davaj</ta>
            <ta e="T574" id="Seg_1420" s="T573">dʼagar-zittə</ta>
            <ta e="T575" id="Seg_1421" s="T574">kăpusta-m</ta>
            <ta e="T576" id="Seg_1422" s="T575">i</ta>
            <ta e="T577" id="Seg_1423" s="T576">müjə-t</ta>
            <ta e="T578" id="Seg_1424" s="T577">săj-hʼaʔ-bi</ta>
            <ta e="T579" id="Seg_1425" s="T578">dĭgəttə</ta>
            <ta e="T580" id="Seg_1426" s="T579">dĭ-m</ta>
            <ta e="T581" id="Seg_1427" s="T580">saro-bi</ta>
            <ta e="T582" id="Seg_1428" s="T581">dĭn</ta>
            <ta e="T583" id="Seg_1429" s="T582">hen-bi</ta>
            <ta e="T584" id="Seg_1430" s="T583">stol-də</ta>
            <ta e="T585" id="Seg_1431" s="T584">dĭgəttə</ta>
            <ta e="T586" id="Seg_1432" s="T585">ku-liA-t</ta>
            <ta e="T587" id="Seg_1433" s="T586">bar</ta>
            <ta e="T588" id="Seg_1434" s="T587">tʼor-laʔbə</ta>
            <ta e="T589" id="Seg_1435" s="T588">müjə-t</ta>
            <ta e="T590" id="Seg_1436" s="T589">dĭgəttə</ta>
            <ta e="T591" id="Seg_1437" s="T590">dĭ</ta>
            <ta e="T592" id="Seg_1438" s="T591">ku-bi</ta>
            <ta e="T593" id="Seg_1439" s="T592">dĭn</ta>
            <ta e="T594" id="Seg_1440" s="T593">ešši</ta>
            <ta e="T596" id="Seg_1441" s="T595">nereʔ-luʔbdə-bi</ta>
            <ta e="T597" id="Seg_1442" s="T596">dĭn</ta>
            <ta e="T598" id="Seg_1443" s="T597">ešši</ta>
            <ta e="T599" id="Seg_1444" s="T598">iʔbö-laʔbə</ta>
            <ta e="T600" id="Seg_1445" s="T599">măn</ta>
            <ta e="T601" id="Seg_1446" s="T600">tăn</ta>
            <ta e="T602" id="Seg_1447" s="T601">nʼi-l</ta>
            <ta e="T603" id="Seg_1448" s="T602">tăn</ta>
            <ta e="T604" id="Seg_1449" s="T603">müjə-zAŋ-Tə</ta>
            <ta e="T605" id="Seg_1450" s="T604">supso-bi-m</ta>
            <ta e="T606" id="Seg_1451" s="T605">dĭgəttə</ta>
            <ta e="T607" id="Seg_1452" s="T606">dĭ</ta>
            <ta e="T608" id="Seg_1453" s="T607">i-bi</ta>
            <ta e="T609" id="Seg_1454" s="T608">dĭ-m</ta>
            <ta e="T610" id="Seg_1455" s="T609">dĭ</ta>
            <ta e="T611" id="Seg_1456" s="T610">măn-ntə</ta>
            <ta e="T612" id="Seg_1457" s="T611">măn</ta>
            <ta e="T613" id="Seg_1458" s="T612">kan-lV-m</ta>
            <ta e="T614" id="Seg_1459" s="T613">aba-gəndə</ta>
            <ta e="T616" id="Seg_1460" s="T615">tajər-zittə</ta>
            <ta e="T619" id="Seg_1461" s="T618">šonə-gA</ta>
            <ta e="T620" id="Seg_1462" s="T619">šo-bi</ta>
            <ta e="T621" id="Seg_1463" s="T620">aba-t</ta>
            <ta e="T622" id="Seg_1464" s="T621">tajər-laʔbə</ta>
            <ta e="T623" id="Seg_1465" s="T622">ine-t-ziʔ</ta>
            <ta e="T624" id="Seg_1466" s="T623">măn-ntə</ta>
            <ta e="T625" id="Seg_1467" s="T624">aba-m</ta>
            <ta e="T626" id="Seg_1468" s="T625">măn</ta>
            <ta e="T627" id="Seg_1469" s="T626">tănan</ta>
            <ta e="T628" id="Seg_1470" s="T627">šo-bi-m</ta>
            <ta e="T629" id="Seg_1471" s="T628">a</ta>
            <ta e="T630" id="Seg_1472" s="T629">dĭ</ta>
            <ta e="T631" id="Seg_1473" s="T630">măn-bi</ta>
            <ta e="T632" id="Seg_1474" s="T631">gijen</ta>
            <ta e="T633" id="Seg_1475" s="T632">šində</ta>
            <ta e="T634" id="Seg_1476" s="T633">tʼăbaktər-laʔbə</ta>
            <ta e="T635" id="Seg_1477" s="T634">dĭgəttə</ta>
            <ta e="T636" id="Seg_1478" s="T635">ku-luʔbdə-bi</ta>
            <ta e="T637" id="Seg_1479" s="T636">dĭ</ta>
            <ta e="T638" id="Seg_1480" s="T637">ešši-m</ta>
            <ta e="T639" id="Seg_1481" s="T638">măn</ta>
            <ta e="T640" id="Seg_1482" s="T639">tăn</ta>
            <ta e="T641" id="Seg_1483" s="T640">nʼi-l</ta>
            <ta e="T642" id="Seg_1484" s="T641">amnə-ʔ</ta>
            <ta e="T643" id="Seg_1485" s="T642">amor-ə-ʔ</ta>
            <ta e="T644" id="Seg_1486" s="T643">a</ta>
            <ta e="T645" id="Seg_1487" s="T644">măn</ta>
            <ta e="T646" id="Seg_1488" s="T645">tajər-lV-m</ta>
            <ta e="T647" id="Seg_1489" s="T646">dĭgəttə</ta>
            <ta e="T649" id="Seg_1490" s="T648">ine-n</ta>
            <ta e="T650" id="Seg_1491" s="T649">ku-Tə</ta>
            <ta e="T651" id="Seg_1492" s="T650">păda-laːm-bi</ta>
            <ta e="T652" id="Seg_1493" s="T651">i</ta>
            <ta e="T653" id="Seg_1494" s="T652">tajər-laʔbə</ta>
            <ta e="T654" id="Seg_1495" s="T653">a</ta>
            <ta e="T655" id="Seg_1496" s="T654">dĭ</ta>
            <ta e="T656" id="Seg_1497" s="T655">büzʼe</ta>
            <ta e="T657" id="Seg_1498" s="T656">amor-laʔbə</ta>
            <ta e="T658" id="Seg_1499" s="T657">dĭgəttə</ta>
            <ta e="T659" id="Seg_1500" s="T658">šonə-gA</ta>
            <ta e="T660" id="Seg_1501" s="T659">koŋ</ta>
            <ta e="T661" id="Seg_1502" s="T660">kuza</ta>
            <ta e="T662" id="Seg_1503" s="T661">ku-liA-t</ta>
            <ta e="T663" id="Seg_1504" s="T662">ine-jəʔ</ta>
            <ta e="T664" id="Seg_1505" s="T663">mĭn-laʔbə-jəʔ</ta>
            <ta e="T665" id="Seg_1506" s="T664">a</ta>
            <ta e="T666" id="Seg_1507" s="T665">kuza</ta>
            <ta e="T667" id="Seg_1508" s="T666">naga</ta>
            <ta e="T668" id="Seg_1509" s="T667">dĭgəttə</ta>
            <ta e="T669" id="Seg_1510" s="T668">măn-ntə</ta>
            <ta e="T670" id="Seg_1511" s="T669">ĭmbi</ta>
            <ta e="T671" id="Seg_1512" s="T670">ine-jəʔ</ta>
            <ta e="T672" id="Seg_1513" s="T671">mĭn-laʔbə</ta>
            <ta e="T673" id="Seg_1514" s="T672">a</ta>
            <ta e="T674" id="Seg_1515" s="T673">ĭmbi</ta>
            <ta e="T675" id="Seg_1516" s="T674">ej</ta>
            <ta e="T676" id="Seg_1517" s="T675">ku-l-liA-l</ta>
            <ta e="T677" id="Seg_1518" s="T676">dĭ</ta>
            <ta e="T678" id="Seg_1519" s="T677">măn</ta>
            <ta e="T679" id="Seg_1520" s="T678">nʼi-m</ta>
            <ta e="T680" id="Seg_1521" s="T679">tajər-laʔbə</ta>
            <ta e="T681" id="Seg_1522" s="T680">dĭgəttə</ta>
            <ta e="T682" id="Seg_1523" s="T681">sădar-t</ta>
            <ta e="T683" id="Seg_1524" s="T682">măna</ta>
            <ta e="T684" id="Seg_1525" s="T683">dĭ-m</ta>
            <ta e="T685" id="Seg_1526" s="T684">dĭ</ta>
            <ta e="T686" id="Seg_1527" s="T685">sădar-bi</ta>
            <ta e="T687" id="Seg_1528" s="T686">dĭ</ta>
            <ta e="T688" id="Seg_1529" s="T687">iʔgö</ta>
            <ta e="T689" id="Seg_1530" s="T688">aktʼa</ta>
            <ta e="T690" id="Seg_1531" s="T689">mĭ-bi</ta>
            <ta e="T691" id="Seg_1532" s="T690">dĭgəttə</ta>
            <ta e="T692" id="Seg_1533" s="T691">hen-bi</ta>
            <ta e="T693" id="Seg_1534" s="T692">dĭ-m</ta>
            <ta e="T694" id="Seg_1535" s="T693">kărman-Tə</ta>
            <ta e="T695" id="Seg_1536" s="T694">dĭ</ta>
            <ta e="T696" id="Seg_1537" s="T695">kărman-də</ta>
            <ta e="T697" id="Seg_1538" s="T696">bar</ta>
            <ta e="T698" id="Seg_1539" s="T697">săj-nʼeʔbdə-luʔbdə-bi</ta>
            <ta e="T699" id="Seg_1540" s="T698">kan-lAʔ</ta>
            <ta e="T700" id="Seg_1541" s="T699">tʼür-bi</ta>
            <ta e="T701" id="Seg_1542" s="T700">dĭgəttə</ta>
            <ta e="T702" id="Seg_1543" s="T701">šo-bi</ta>
            <ta e="T703" id="Seg_1544" s="T702">šo-bi</ta>
            <ta e="T704" id="Seg_1545" s="T703">aʔtʼi-Kən</ta>
            <ta e="T705" id="Seg_1546" s="T704">kunol-luʔbdə-bi</ta>
            <ta e="T706" id="Seg_1547" s="T705">a</ta>
            <ta e="T707" id="Seg_1548" s="T706">volk</ta>
            <ta e="T708" id="Seg_1549" s="T707">tüj</ta>
            <ta e="T709" id="Seg_1550" s="T708">šo-bi</ta>
            <ta e="T710" id="Seg_1551" s="T709">dĭ-m</ta>
            <ta e="T711" id="Seg_1552" s="T710">am-luʔbdə-bi</ta>
            <ta e="T712" id="Seg_1553" s="T711">dĭgəttə</ta>
            <ta e="T713" id="Seg_1554" s="T712">ular-jəʔ</ta>
            <ta e="T714" id="Seg_1555" s="T713">bar</ta>
            <ta e="T715" id="Seg_1556" s="T714">pastux</ta>
            <ta e="T716" id="Seg_1557" s="T715">măndo-liA</ta>
            <ta e="T717" id="Seg_1558" s="T716">dĭ</ta>
            <ta e="T718" id="Seg_1559" s="T717">volk</ta>
            <ta e="T719" id="Seg_1560" s="T718">ular</ta>
            <ta e="T720" id="Seg_1561" s="T719">tolʼko</ta>
            <ta e="T722" id="Seg_1562" s="T721">i-zittə</ta>
            <ta e="T723" id="Seg_1563" s="T722">a</ta>
            <ta e="T724" id="Seg_1564" s="T723">dĭ</ta>
            <ta e="T725" id="Seg_1565" s="T724">nʼi</ta>
            <ta e="T726" id="Seg_1566" s="T725">kirgaːr-laʔbə</ta>
            <ta e="T727" id="Seg_1567" s="T726">uʔbdə-ʔ</ta>
            <ta e="T728" id="Seg_1568" s="T727">ular</ta>
            <ta e="T729" id="Seg_1569" s="T728">am-laʔbə</ta>
            <ta e="T730" id="Seg_1570" s="T729">dĭgəttə</ta>
            <ta e="T731" id="Seg_1571" s="T730">kondʼo</ta>
            <ta e="T732" id="Seg_1572" s="T731">amno-bi</ta>
            <ta e="T733" id="Seg_1573" s="T732">dĭ</ta>
            <ta e="T734" id="Seg_1574" s="T733">ugaːndə</ta>
            <ta e="T735" id="Seg_1575" s="T734">püjö-liA</ta>
            <ta e="T736" id="Seg_1576" s="T735">volk</ta>
            <ta e="T737" id="Seg_1577" s="T736">dĭgəttə</ta>
            <ta e="T738" id="Seg_1578" s="T737">măn-ntə</ta>
            <ta e="T739" id="Seg_1579" s="T738">kan-ə-ʔ</ta>
            <ta e="T740" id="Seg_1580" s="T739">supso-ʔ</ta>
            <ta e="T741" id="Seg_1581" s="T740">măna</ta>
            <ta e="T742" id="Seg_1582" s="T741">a</ta>
            <ta e="T743" id="Seg_1583" s="T742">dĭ</ta>
            <ta e="T744" id="Seg_1584" s="T743">măn-ntə</ta>
            <ta e="T745" id="Seg_1585" s="T744">kun-ə-ʔ</ta>
            <ta e="T746" id="Seg_1586" s="T745">aba-Tə</ta>
            <ta e="T747" id="Seg_1587" s="T746">da</ta>
            <ta e="T748" id="Seg_1588" s="T747">ija-Tə</ta>
            <ta e="T749" id="Seg_1589" s="T748">dĭ</ta>
            <ta e="T750" id="Seg_1590" s="T749">dĭ-m</ta>
            <ta e="T751" id="Seg_1591" s="T750">det-bi</ta>
            <ta e="T752" id="Seg_1592" s="T751">aba-Tə</ta>
            <ta e="T753" id="Seg_1593" s="T752">da</ta>
            <ta e="T754" id="Seg_1594" s="T753">ija-Tə</ta>
            <ta e="T755" id="Seg_1595" s="T754">tura-gəndə</ta>
            <ta e="T756" id="Seg_1596" s="T755">dĭ</ta>
            <ta e="T757" id="Seg_1597" s="T756">barəʔ-luʔbdə-bi</ta>
            <ta e="T758" id="Seg_1598" s="T757">a</ta>
            <ta e="T759" id="Seg_1599" s="T758">bos-də</ta>
            <ta e="T760" id="Seg_1600" s="T759">nuʔmə-luʔbdə-bi</ta>
            <ta e="T761" id="Seg_1601" s="T760">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T569" id="Seg_1602" s="T568">live-PST-3PL</ta>
            <ta e="T570" id="Seg_1603" s="T569">woman.[NOM.SG]</ta>
            <ta e="T571" id="Seg_1604" s="T570">man-INS</ta>
            <ta e="T572" id="Seg_1605" s="T571">woman.[NOM.SG]</ta>
            <ta e="T573" id="Seg_1606" s="T572">INCH</ta>
            <ta e="T574" id="Seg_1607" s="T573">stab-INF.LAT</ta>
            <ta e="T575" id="Seg_1608" s="T574">cabbage-ACC</ta>
            <ta e="T576" id="Seg_1609" s="T575">and</ta>
            <ta e="T577" id="Seg_1610" s="T576">finger-NOM/GEN.3SG</ta>
            <ta e="T578" id="Seg_1611" s="T577">off-cut-PST.[3SG]</ta>
            <ta e="T579" id="Seg_1612" s="T578">then</ta>
            <ta e="T580" id="Seg_1613" s="T579">this-ACC</ta>
            <ta e="T581" id="Seg_1614" s="T580">tie-PST.[3SG]</ta>
            <ta e="T582" id="Seg_1615" s="T581">there</ta>
            <ta e="T583" id="Seg_1616" s="T582">put-PST.[3SG]</ta>
            <ta e="T584" id="Seg_1617" s="T583">table-NOM/GEN/ACC.3SG</ta>
            <ta e="T585" id="Seg_1618" s="T584">then</ta>
            <ta e="T586" id="Seg_1619" s="T585">see-PRS-3SG.O</ta>
            <ta e="T587" id="Seg_1620" s="T586">PTCL</ta>
            <ta e="T588" id="Seg_1621" s="T587">cry-DUR.[3SG]</ta>
            <ta e="T589" id="Seg_1622" s="T588">finger-NOM/GEN.3SG</ta>
            <ta e="T590" id="Seg_1623" s="T589">then</ta>
            <ta e="T591" id="Seg_1624" s="T590">this.[NOM.SG]</ta>
            <ta e="T592" id="Seg_1625" s="T591">see-PST.[3SG]</ta>
            <ta e="T593" id="Seg_1626" s="T592">there</ta>
            <ta e="T594" id="Seg_1627" s="T593">child.[NOM.SG]</ta>
            <ta e="T596" id="Seg_1628" s="T595">frighten-MOM-PST.[3SG]</ta>
            <ta e="T597" id="Seg_1629" s="T596">there</ta>
            <ta e="T598" id="Seg_1630" s="T597">child.[NOM.SG]</ta>
            <ta e="T599" id="Seg_1631" s="T598">lie-DUR.[3SG]</ta>
            <ta e="T600" id="Seg_1632" s="T599">I.NOM</ta>
            <ta e="T601" id="Seg_1633" s="T600">you.GEN</ta>
            <ta e="T602" id="Seg_1634" s="T601">son-NOM/GEN/ACC.2SG</ta>
            <ta e="T603" id="Seg_1635" s="T602">you.GEN</ta>
            <ta e="T604" id="Seg_1636" s="T603">finger-PL-LAT</ta>
            <ta e="T605" id="Seg_1637" s="T604">depart-PST-1SG</ta>
            <ta e="T606" id="Seg_1638" s="T605">then</ta>
            <ta e="T607" id="Seg_1639" s="T606">this.[NOM.SG]</ta>
            <ta e="T608" id="Seg_1640" s="T607">take-PST.[3SG]</ta>
            <ta e="T609" id="Seg_1641" s="T608">this-ACC</ta>
            <ta e="T610" id="Seg_1642" s="T609">this.[NOM.SG]</ta>
            <ta e="T611" id="Seg_1643" s="T610">say-IPFVZ.[3SG]</ta>
            <ta e="T612" id="Seg_1644" s="T611">I.NOM</ta>
            <ta e="T613" id="Seg_1645" s="T612">go-FUT-1SG</ta>
            <ta e="T614" id="Seg_1646" s="T613">father-LAT/LOC.3SG</ta>
            <ta e="T616" id="Seg_1647" s="T615">plough-INF.LAT</ta>
            <ta e="T619" id="Seg_1648" s="T618">come-PRS.[3SG]</ta>
            <ta e="T620" id="Seg_1649" s="T619">come-PST.[3SG]</ta>
            <ta e="T621" id="Seg_1650" s="T620">father-NOM/GEN.3SG</ta>
            <ta e="T622" id="Seg_1651" s="T621">plough-DUR.[3SG]</ta>
            <ta e="T623" id="Seg_1652" s="T622">horse-3SG-INS</ta>
            <ta e="T624" id="Seg_1653" s="T623">say-IPFVZ.[3SG]</ta>
            <ta e="T625" id="Seg_1654" s="T624">father-NOM/GEN/ACC.1SG</ta>
            <ta e="T626" id="Seg_1655" s="T625">I.NOM</ta>
            <ta e="T627" id="Seg_1656" s="T626">you.DAT</ta>
            <ta e="T628" id="Seg_1657" s="T627">come-PST-1SG</ta>
            <ta e="T629" id="Seg_1658" s="T628">and</ta>
            <ta e="T630" id="Seg_1659" s="T629">this.[NOM.SG]</ta>
            <ta e="T631" id="Seg_1660" s="T630">say-PST.[3SG]</ta>
            <ta e="T632" id="Seg_1661" s="T631">where</ta>
            <ta e="T633" id="Seg_1662" s="T632">who.[NOM.SG]</ta>
            <ta e="T634" id="Seg_1663" s="T633">speak-DUR.[3SG]</ta>
            <ta e="T635" id="Seg_1664" s="T634">then</ta>
            <ta e="T636" id="Seg_1665" s="T635">see-MOM-PST.[3SG]</ta>
            <ta e="T637" id="Seg_1666" s="T636">this.[NOM.SG]</ta>
            <ta e="T638" id="Seg_1667" s="T637">child-ACC</ta>
            <ta e="T639" id="Seg_1668" s="T638">I.NOM</ta>
            <ta e="T640" id="Seg_1669" s="T639">you.GEN</ta>
            <ta e="T641" id="Seg_1670" s="T640">son-NOM/GEN/ACC.2SG</ta>
            <ta e="T642" id="Seg_1671" s="T641">sit.down-IMP.2SG</ta>
            <ta e="T643" id="Seg_1672" s="T642">eat-EP-IMP.2SG</ta>
            <ta e="T644" id="Seg_1673" s="T643">and</ta>
            <ta e="T645" id="Seg_1674" s="T644">I.NOM</ta>
            <ta e="T646" id="Seg_1675" s="T645">plough-FUT-1SG</ta>
            <ta e="T647" id="Seg_1676" s="T646">then</ta>
            <ta e="T649" id="Seg_1677" s="T648">horse-GEN</ta>
            <ta e="T650" id="Seg_1678" s="T649">ear-LAT</ta>
            <ta e="T651" id="Seg_1679" s="T650">creep.into-RES-PST.[3SG]</ta>
            <ta e="T652" id="Seg_1680" s="T651">and</ta>
            <ta e="T653" id="Seg_1681" s="T652">plough-DUR.[3SG]</ta>
            <ta e="T654" id="Seg_1682" s="T653">and</ta>
            <ta e="T655" id="Seg_1683" s="T654">this.[NOM.SG]</ta>
            <ta e="T656" id="Seg_1684" s="T655">man.[NOM.SG]</ta>
            <ta e="T657" id="Seg_1685" s="T656">eat-DUR.[3SG]</ta>
            <ta e="T658" id="Seg_1686" s="T657">then</ta>
            <ta e="T659" id="Seg_1687" s="T658">come-PRS.[3SG]</ta>
            <ta e="T660" id="Seg_1688" s="T659">chief.[NOM.SG]</ta>
            <ta e="T661" id="Seg_1689" s="T660">man.[NOM.SG]</ta>
            <ta e="T662" id="Seg_1690" s="T661">see-PRS-3SG.O</ta>
            <ta e="T663" id="Seg_1691" s="T662">horse-PL</ta>
            <ta e="T664" id="Seg_1692" s="T663">go-DUR-3PL</ta>
            <ta e="T665" id="Seg_1693" s="T664">and</ta>
            <ta e="T666" id="Seg_1694" s="T665">human.[NOM.SG]</ta>
            <ta e="T667" id="Seg_1695" s="T666">NEG.EX.[3SG]</ta>
            <ta e="T668" id="Seg_1696" s="T667">then</ta>
            <ta e="T669" id="Seg_1697" s="T668">say-IPFVZ.[3SG]</ta>
            <ta e="T670" id="Seg_1698" s="T669">what.[NOM.SG]</ta>
            <ta e="T671" id="Seg_1699" s="T670">horse-PL</ta>
            <ta e="T672" id="Seg_1700" s="T671">go-DUR.[3SG]</ta>
            <ta e="T673" id="Seg_1701" s="T672">and</ta>
            <ta e="T674" id="Seg_1702" s="T673">what.[NOM.SG]</ta>
            <ta e="T675" id="Seg_1703" s="T674">NEG</ta>
            <ta e="T676" id="Seg_1704" s="T675">see-FRQ-PRS-2SG</ta>
            <ta e="T677" id="Seg_1705" s="T676">this</ta>
            <ta e="T678" id="Seg_1706" s="T677">I.GEN</ta>
            <ta e="T679" id="Seg_1707" s="T678">boy-NOM/GEN/ACC.1SG</ta>
            <ta e="T680" id="Seg_1708" s="T679">plough-DUR.[3SG]</ta>
            <ta e="T681" id="Seg_1709" s="T680">then</ta>
            <ta e="T682" id="Seg_1710" s="T681">sell-IMP.2SG.O</ta>
            <ta e="T683" id="Seg_1711" s="T682">I.LAT</ta>
            <ta e="T684" id="Seg_1712" s="T683">this-ACC</ta>
            <ta e="T685" id="Seg_1713" s="T684">this.[NOM.SG]</ta>
            <ta e="T686" id="Seg_1714" s="T685">sell-PST.[3SG]</ta>
            <ta e="T687" id="Seg_1715" s="T686">this.[NOM.SG]</ta>
            <ta e="T688" id="Seg_1716" s="T687">many</ta>
            <ta e="T689" id="Seg_1717" s="T688">money.[NOM.SG]</ta>
            <ta e="T690" id="Seg_1718" s="T689">give-PST.[3SG]</ta>
            <ta e="T691" id="Seg_1719" s="T690">then</ta>
            <ta e="T692" id="Seg_1720" s="T691">put-PST.[3SG]</ta>
            <ta e="T693" id="Seg_1721" s="T692">this-ACC</ta>
            <ta e="T694" id="Seg_1722" s="T693">pocket-LAT</ta>
            <ta e="T695" id="Seg_1723" s="T694">this.[NOM.SG]</ta>
            <ta e="T696" id="Seg_1724" s="T695">pocket-NOM/GEN/ACC.3SG</ta>
            <ta e="T697" id="Seg_1725" s="T696">PTCL</ta>
            <ta e="T698" id="Seg_1726" s="T697">off-pull-MOM-PST.[3SG]</ta>
            <ta e="T699" id="Seg_1727" s="T698">go-CVB</ta>
            <ta e="T700" id="Seg_1728" s="T699">disappear-PST.[3SG]</ta>
            <ta e="T701" id="Seg_1729" s="T700">then</ta>
            <ta e="T702" id="Seg_1730" s="T701">come-PST.[3SG]</ta>
            <ta e="T703" id="Seg_1731" s="T702">come-PST.[3SG]</ta>
            <ta e="T704" id="Seg_1732" s="T703">road-LOC</ta>
            <ta e="T705" id="Seg_1733" s="T704">sleep-MOM-PST.[3SG]</ta>
            <ta e="T706" id="Seg_1734" s="T705">and</ta>
            <ta e="T707" id="Seg_1735" s="T706">wolf.[NOM.SG]</ta>
            <ta e="T708" id="Seg_1736" s="T707">now</ta>
            <ta e="T709" id="Seg_1737" s="T708">come-PST.[3SG]</ta>
            <ta e="T710" id="Seg_1738" s="T709">this-ACC</ta>
            <ta e="T711" id="Seg_1739" s="T710">eat-MOM-PST.[3SG]</ta>
            <ta e="T712" id="Seg_1740" s="T711">then</ta>
            <ta e="T713" id="Seg_1741" s="T712">sheep-PL</ta>
            <ta e="T714" id="Seg_1742" s="T713">PTCL</ta>
            <ta e="T715" id="Seg_1743" s="T714">herdsman.[NOM.SG]</ta>
            <ta e="T716" id="Seg_1744" s="T715">look-PRS.[3SG]</ta>
            <ta e="T717" id="Seg_1745" s="T716">this.[NOM.SG]</ta>
            <ta e="T718" id="Seg_1746" s="T717">wolf.[NOM.SG]</ta>
            <ta e="T719" id="Seg_1747" s="T718">sheep.[NOM.SG]</ta>
            <ta e="T720" id="Seg_1748" s="T719">only</ta>
            <ta e="T722" id="Seg_1749" s="T721">take-INF.LAT</ta>
            <ta e="T723" id="Seg_1750" s="T722">and</ta>
            <ta e="T724" id="Seg_1751" s="T723">this.[NOM.SG]</ta>
            <ta e="T725" id="Seg_1752" s="T724">boy.[NOM.SG]</ta>
            <ta e="T726" id="Seg_1753" s="T725">shout-DUR.[3SG]</ta>
            <ta e="T727" id="Seg_1754" s="T726">get.up-IMP.2SG</ta>
            <ta e="T728" id="Seg_1755" s="T727">sheep.[NOM.SG]</ta>
            <ta e="T729" id="Seg_1756" s="T728">eat-DUR.[3SG]</ta>
            <ta e="T730" id="Seg_1757" s="T729">then</ta>
            <ta e="T731" id="Seg_1758" s="T730">long.time</ta>
            <ta e="T732" id="Seg_1759" s="T731">sit-PST.[3SG]</ta>
            <ta e="T733" id="Seg_1760" s="T732">this.[NOM.SG]</ta>
            <ta e="T734" id="Seg_1761" s="T733">very</ta>
            <ta e="T735" id="Seg_1762" s="T734">be.hungry-PRS.[3SG]</ta>
            <ta e="T736" id="Seg_1763" s="T735">wolf.[NOM.SG]</ta>
            <ta e="T737" id="Seg_1764" s="T736">then</ta>
            <ta e="T738" id="Seg_1765" s="T737">say-IPFVZ.[3SG]</ta>
            <ta e="T739" id="Seg_1766" s="T738">go-EP-IMP.2SG</ta>
            <ta e="T740" id="Seg_1767" s="T739">depart-IMP.2SG</ta>
            <ta e="T741" id="Seg_1768" s="T740">I.LAT</ta>
            <ta e="T742" id="Seg_1769" s="T741">and</ta>
            <ta e="T743" id="Seg_1770" s="T742">this.[NOM.SG]</ta>
            <ta e="T744" id="Seg_1771" s="T743">say-IPFVZ.[3SG]</ta>
            <ta e="T745" id="Seg_1772" s="T744">bring-EP-IMP.2SG</ta>
            <ta e="T746" id="Seg_1773" s="T745">father-LAT</ta>
            <ta e="T747" id="Seg_1774" s="T746">and</ta>
            <ta e="T748" id="Seg_1775" s="T747">mother-LAT</ta>
            <ta e="T749" id="Seg_1776" s="T748">this.[NOM.SG]</ta>
            <ta e="T750" id="Seg_1777" s="T749">this-ACC</ta>
            <ta e="T751" id="Seg_1778" s="T750">bring-PST.[3SG]</ta>
            <ta e="T752" id="Seg_1779" s="T751">father-LAT</ta>
            <ta e="T753" id="Seg_1780" s="T752">and</ta>
            <ta e="T754" id="Seg_1781" s="T753">mother-LAT</ta>
            <ta e="T755" id="Seg_1782" s="T754">house-LAT/LOC.3SG</ta>
            <ta e="T756" id="Seg_1783" s="T755">this.[NOM.SG]</ta>
            <ta e="T757" id="Seg_1784" s="T756">throw.away-MOM-PST.[3SG]</ta>
            <ta e="T758" id="Seg_1785" s="T757">and</ta>
            <ta e="T759" id="Seg_1786" s="T758">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T760" id="Seg_1787" s="T759">run-MOM-PST.[3SG]</ta>
            <ta e="T761" id="Seg_1788" s="T760">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T569" id="Seg_1789" s="T568">жить-PST-3PL</ta>
            <ta e="T570" id="Seg_1790" s="T569">женщина.[NOM.SG]</ta>
            <ta e="T571" id="Seg_1791" s="T570">мужчина-INS</ta>
            <ta e="T572" id="Seg_1792" s="T571">женщина.[NOM.SG]</ta>
            <ta e="T573" id="Seg_1793" s="T572">INCH</ta>
            <ta e="T574" id="Seg_1794" s="T573">зарезать-INF.LAT</ta>
            <ta e="T575" id="Seg_1795" s="T574">капуста-ACC</ta>
            <ta e="T576" id="Seg_1796" s="T575">и</ta>
            <ta e="T577" id="Seg_1797" s="T576">палец-NOM/GEN.3SG</ta>
            <ta e="T578" id="Seg_1798" s="T577">от-резать-PST.[3SG]</ta>
            <ta e="T579" id="Seg_1799" s="T578">тогда</ta>
            <ta e="T580" id="Seg_1800" s="T579">этот-ACC</ta>
            <ta e="T581" id="Seg_1801" s="T580">привязывать-PST.[3SG]</ta>
            <ta e="T582" id="Seg_1802" s="T581">там</ta>
            <ta e="T583" id="Seg_1803" s="T582">класть-PST.[3SG]</ta>
            <ta e="T584" id="Seg_1804" s="T583">стол-NOM/GEN/ACC.3SG</ta>
            <ta e="T585" id="Seg_1805" s="T584">тогда</ta>
            <ta e="T586" id="Seg_1806" s="T585">видеть-PRS-3SG.O</ta>
            <ta e="T587" id="Seg_1807" s="T586">PTCL</ta>
            <ta e="T588" id="Seg_1808" s="T587">плакать-DUR.[3SG]</ta>
            <ta e="T589" id="Seg_1809" s="T588">палец-NOM/GEN.3SG</ta>
            <ta e="T590" id="Seg_1810" s="T589">тогда</ta>
            <ta e="T591" id="Seg_1811" s="T590">этот.[NOM.SG]</ta>
            <ta e="T592" id="Seg_1812" s="T591">видеть-PST.[3SG]</ta>
            <ta e="T593" id="Seg_1813" s="T592">там</ta>
            <ta e="T594" id="Seg_1814" s="T593">ребенок.[NOM.SG]</ta>
            <ta e="T596" id="Seg_1815" s="T595">пугать-MOM-PST.[3SG]</ta>
            <ta e="T597" id="Seg_1816" s="T596">там</ta>
            <ta e="T598" id="Seg_1817" s="T597">ребенок.[NOM.SG]</ta>
            <ta e="T599" id="Seg_1818" s="T598">лежать-DUR.[3SG]</ta>
            <ta e="T600" id="Seg_1819" s="T599">я.NOM</ta>
            <ta e="T601" id="Seg_1820" s="T600">ты.GEN</ta>
            <ta e="T602" id="Seg_1821" s="T601">сын-NOM/GEN/ACC.2SG</ta>
            <ta e="T603" id="Seg_1822" s="T602">ты.GEN</ta>
            <ta e="T604" id="Seg_1823" s="T603">палец-PL-LAT</ta>
            <ta e="T605" id="Seg_1824" s="T604">уйти-PST-1SG</ta>
            <ta e="T606" id="Seg_1825" s="T605">тогда</ta>
            <ta e="T607" id="Seg_1826" s="T606">этот.[NOM.SG]</ta>
            <ta e="T608" id="Seg_1827" s="T607">взять-PST.[3SG]</ta>
            <ta e="T609" id="Seg_1828" s="T608">этот-ACC</ta>
            <ta e="T610" id="Seg_1829" s="T609">этот.[NOM.SG]</ta>
            <ta e="T611" id="Seg_1830" s="T610">сказать-IPFVZ.[3SG]</ta>
            <ta e="T612" id="Seg_1831" s="T611">я.NOM</ta>
            <ta e="T613" id="Seg_1832" s="T612">пойти-FUT-1SG</ta>
            <ta e="T614" id="Seg_1833" s="T613">отец-LAT/LOC.3SG</ta>
            <ta e="T616" id="Seg_1834" s="T615">пахать-INF.LAT</ta>
            <ta e="T619" id="Seg_1835" s="T618">прийти-PRS.[3SG]</ta>
            <ta e="T620" id="Seg_1836" s="T619">прийти-PST.[3SG]</ta>
            <ta e="T621" id="Seg_1837" s="T620">отец-NOM/GEN.3SG</ta>
            <ta e="T622" id="Seg_1838" s="T621">пахать-DUR.[3SG]</ta>
            <ta e="T623" id="Seg_1839" s="T622">лошадь-3SG-INS</ta>
            <ta e="T624" id="Seg_1840" s="T623">сказать-IPFVZ.[3SG]</ta>
            <ta e="T625" id="Seg_1841" s="T624">отец-NOM/GEN/ACC.1SG</ta>
            <ta e="T626" id="Seg_1842" s="T625">я.NOM</ta>
            <ta e="T627" id="Seg_1843" s="T626">ты.DAT</ta>
            <ta e="T628" id="Seg_1844" s="T627">прийти-PST-1SG</ta>
            <ta e="T629" id="Seg_1845" s="T628">а</ta>
            <ta e="T630" id="Seg_1846" s="T629">этот.[NOM.SG]</ta>
            <ta e="T631" id="Seg_1847" s="T630">сказать-PST.[3SG]</ta>
            <ta e="T632" id="Seg_1848" s="T631">где</ta>
            <ta e="T633" id="Seg_1849" s="T632">кто.[NOM.SG]</ta>
            <ta e="T634" id="Seg_1850" s="T633">говорить-DUR.[3SG]</ta>
            <ta e="T635" id="Seg_1851" s="T634">тогда</ta>
            <ta e="T636" id="Seg_1852" s="T635">видеть-MOM-PST.[3SG]</ta>
            <ta e="T637" id="Seg_1853" s="T636">этот.[NOM.SG]</ta>
            <ta e="T638" id="Seg_1854" s="T637">ребенок-ACC</ta>
            <ta e="T639" id="Seg_1855" s="T638">я.NOM</ta>
            <ta e="T640" id="Seg_1856" s="T639">ты.GEN</ta>
            <ta e="T641" id="Seg_1857" s="T640">сын-NOM/GEN/ACC.2SG</ta>
            <ta e="T642" id="Seg_1858" s="T641">сесть-IMP.2SG</ta>
            <ta e="T643" id="Seg_1859" s="T642">есть-EP-IMP.2SG</ta>
            <ta e="T644" id="Seg_1860" s="T643">а</ta>
            <ta e="T645" id="Seg_1861" s="T644">я.NOM</ta>
            <ta e="T646" id="Seg_1862" s="T645">пахать-FUT-1SG</ta>
            <ta e="T647" id="Seg_1863" s="T646">тогда</ta>
            <ta e="T649" id="Seg_1864" s="T648">лошадь-GEN</ta>
            <ta e="T650" id="Seg_1865" s="T649">ухо-LAT</ta>
            <ta e="T651" id="Seg_1866" s="T650">ползти-RES-PST.[3SG]</ta>
            <ta e="T652" id="Seg_1867" s="T651">и</ta>
            <ta e="T653" id="Seg_1868" s="T652">пахать-DUR.[3SG]</ta>
            <ta e="T654" id="Seg_1869" s="T653">а</ta>
            <ta e="T655" id="Seg_1870" s="T654">этот.[NOM.SG]</ta>
            <ta e="T656" id="Seg_1871" s="T655">мужчина.[NOM.SG]</ta>
            <ta e="T657" id="Seg_1872" s="T656">есть-DUR.[3SG]</ta>
            <ta e="T658" id="Seg_1873" s="T657">тогда</ta>
            <ta e="T659" id="Seg_1874" s="T658">прийти-PRS.[3SG]</ta>
            <ta e="T660" id="Seg_1875" s="T659">вождь.[NOM.SG]</ta>
            <ta e="T661" id="Seg_1876" s="T660">мужчина.[NOM.SG]</ta>
            <ta e="T662" id="Seg_1877" s="T661">видеть-PRS-3SG.O</ta>
            <ta e="T663" id="Seg_1878" s="T662">лошадь-PL</ta>
            <ta e="T664" id="Seg_1879" s="T663">идти-DUR-3PL</ta>
            <ta e="T665" id="Seg_1880" s="T664">а</ta>
            <ta e="T666" id="Seg_1881" s="T665">человек.[NOM.SG]</ta>
            <ta e="T667" id="Seg_1882" s="T666">NEG.EX.[3SG]</ta>
            <ta e="T668" id="Seg_1883" s="T667">тогда</ta>
            <ta e="T669" id="Seg_1884" s="T668">сказать-IPFVZ.[3SG]</ta>
            <ta e="T670" id="Seg_1885" s="T669">что.[NOM.SG]</ta>
            <ta e="T671" id="Seg_1886" s="T670">лошадь-PL</ta>
            <ta e="T672" id="Seg_1887" s="T671">идти-DUR.[3SG]</ta>
            <ta e="T673" id="Seg_1888" s="T672">а</ta>
            <ta e="T674" id="Seg_1889" s="T673">что.[NOM.SG]</ta>
            <ta e="T675" id="Seg_1890" s="T674">NEG</ta>
            <ta e="T676" id="Seg_1891" s="T675">видеть-FRQ-PRS-2SG</ta>
            <ta e="T677" id="Seg_1892" s="T676">этот</ta>
            <ta e="T678" id="Seg_1893" s="T677">я.GEN</ta>
            <ta e="T679" id="Seg_1894" s="T678">мальчик-NOM/GEN/ACC.1SG</ta>
            <ta e="T680" id="Seg_1895" s="T679">пахать-DUR.[3SG]</ta>
            <ta e="T681" id="Seg_1896" s="T680">тогда</ta>
            <ta e="T682" id="Seg_1897" s="T681">продавать-IMP.2SG.O</ta>
            <ta e="T683" id="Seg_1898" s="T682">я.LAT</ta>
            <ta e="T684" id="Seg_1899" s="T683">этот-ACC</ta>
            <ta e="T685" id="Seg_1900" s="T684">этот.[NOM.SG]</ta>
            <ta e="T686" id="Seg_1901" s="T685">продавать-PST.[3SG]</ta>
            <ta e="T687" id="Seg_1902" s="T686">этот.[NOM.SG]</ta>
            <ta e="T688" id="Seg_1903" s="T687">много</ta>
            <ta e="T689" id="Seg_1904" s="T688">деньги.[NOM.SG]</ta>
            <ta e="T690" id="Seg_1905" s="T689">дать-PST.[3SG]</ta>
            <ta e="T691" id="Seg_1906" s="T690">тогда</ta>
            <ta e="T692" id="Seg_1907" s="T691">класть-PST.[3SG]</ta>
            <ta e="T693" id="Seg_1908" s="T692">этот-ACC</ta>
            <ta e="T694" id="Seg_1909" s="T693">карман-LAT</ta>
            <ta e="T695" id="Seg_1910" s="T694">этот.[NOM.SG]</ta>
            <ta e="T696" id="Seg_1911" s="T695">карман-NOM/GEN/ACC.3SG</ta>
            <ta e="T697" id="Seg_1912" s="T696">PTCL</ta>
            <ta e="T698" id="Seg_1913" s="T697">от-тянуть-MOM-PST.[3SG]</ta>
            <ta e="T699" id="Seg_1914" s="T698">пойти-CVB</ta>
            <ta e="T700" id="Seg_1915" s="T699">исчезнуть-PST.[3SG]</ta>
            <ta e="T701" id="Seg_1916" s="T700">тогда</ta>
            <ta e="T702" id="Seg_1917" s="T701">прийти-PST.[3SG]</ta>
            <ta e="T703" id="Seg_1918" s="T702">прийти-PST.[3SG]</ta>
            <ta e="T704" id="Seg_1919" s="T703">дорога-LOC</ta>
            <ta e="T705" id="Seg_1920" s="T704">спать-MOM-PST.[3SG]</ta>
            <ta e="T706" id="Seg_1921" s="T705">а</ta>
            <ta e="T707" id="Seg_1922" s="T706">волк.[NOM.SG]</ta>
            <ta e="T708" id="Seg_1923" s="T707">сейчас</ta>
            <ta e="T709" id="Seg_1924" s="T708">прийти-PST.[3SG]</ta>
            <ta e="T710" id="Seg_1925" s="T709">этот-ACC</ta>
            <ta e="T711" id="Seg_1926" s="T710">съесть-MOM-PST.[3SG]</ta>
            <ta e="T712" id="Seg_1927" s="T711">тогда</ta>
            <ta e="T713" id="Seg_1928" s="T712">овца-PL</ta>
            <ta e="T714" id="Seg_1929" s="T713">PTCL</ta>
            <ta e="T715" id="Seg_1930" s="T714">пастух.[NOM.SG]</ta>
            <ta e="T716" id="Seg_1931" s="T715">смотреть-PRS.[3SG]</ta>
            <ta e="T717" id="Seg_1932" s="T716">этот.[NOM.SG]</ta>
            <ta e="T718" id="Seg_1933" s="T717">волк.[NOM.SG]</ta>
            <ta e="T719" id="Seg_1934" s="T718">овца.[NOM.SG]</ta>
            <ta e="T720" id="Seg_1935" s="T719">только</ta>
            <ta e="T722" id="Seg_1936" s="T721">взять-INF.LAT</ta>
            <ta e="T723" id="Seg_1937" s="T722">а</ta>
            <ta e="T724" id="Seg_1938" s="T723">этот.[NOM.SG]</ta>
            <ta e="T725" id="Seg_1939" s="T724">мальчик.[NOM.SG]</ta>
            <ta e="T726" id="Seg_1940" s="T725">кричать-DUR.[3SG]</ta>
            <ta e="T727" id="Seg_1941" s="T726">встать-IMP.2SG</ta>
            <ta e="T728" id="Seg_1942" s="T727">овца.[NOM.SG]</ta>
            <ta e="T729" id="Seg_1943" s="T728">съесть-DUR.[3SG]</ta>
            <ta e="T730" id="Seg_1944" s="T729">тогда</ta>
            <ta e="T731" id="Seg_1945" s="T730">долго</ta>
            <ta e="T732" id="Seg_1946" s="T731">сидеть-PST.[3SG]</ta>
            <ta e="T733" id="Seg_1947" s="T732">этот.[NOM.SG]</ta>
            <ta e="T734" id="Seg_1948" s="T733">очень</ta>
            <ta e="T735" id="Seg_1949" s="T734">быть.голодным-PRS.[3SG]</ta>
            <ta e="T736" id="Seg_1950" s="T735">волк.[NOM.SG]</ta>
            <ta e="T737" id="Seg_1951" s="T736">тогда</ta>
            <ta e="T738" id="Seg_1952" s="T737">сказать-IPFVZ.[3SG]</ta>
            <ta e="T739" id="Seg_1953" s="T738">пойти-EP-IMP.2SG</ta>
            <ta e="T740" id="Seg_1954" s="T739">уйти-IMP.2SG</ta>
            <ta e="T741" id="Seg_1955" s="T740">я.LAT</ta>
            <ta e="T742" id="Seg_1956" s="T741">а</ta>
            <ta e="T743" id="Seg_1957" s="T742">этот.[NOM.SG]</ta>
            <ta e="T744" id="Seg_1958" s="T743">сказать-IPFVZ.[3SG]</ta>
            <ta e="T745" id="Seg_1959" s="T744">нести-EP-IMP.2SG</ta>
            <ta e="T746" id="Seg_1960" s="T745">отец-LAT</ta>
            <ta e="T747" id="Seg_1961" s="T746">и</ta>
            <ta e="T748" id="Seg_1962" s="T747">мать-LAT</ta>
            <ta e="T749" id="Seg_1963" s="T748">этот.[NOM.SG]</ta>
            <ta e="T750" id="Seg_1964" s="T749">этот-ACC</ta>
            <ta e="T751" id="Seg_1965" s="T750">принести-PST.[3SG]</ta>
            <ta e="T752" id="Seg_1966" s="T751">отец-LAT</ta>
            <ta e="T753" id="Seg_1967" s="T752">и</ta>
            <ta e="T754" id="Seg_1968" s="T753">мать-LAT</ta>
            <ta e="T755" id="Seg_1969" s="T754">дом-LAT/LOC.3SG</ta>
            <ta e="T756" id="Seg_1970" s="T755">этот.[NOM.SG]</ta>
            <ta e="T757" id="Seg_1971" s="T756">выбросить-MOM-PST.[3SG]</ta>
            <ta e="T758" id="Seg_1972" s="T757">а</ta>
            <ta e="T759" id="Seg_1973" s="T758">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T760" id="Seg_1974" s="T759">бежать-MOM-PST.[3SG]</ta>
            <ta e="T761" id="Seg_1975" s="T760">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T569" id="Seg_1976" s="T568">v-v:tense-v:pn</ta>
            <ta e="T570" id="Seg_1977" s="T569">n-n:case</ta>
            <ta e="T571" id="Seg_1978" s="T570">n-n:case</ta>
            <ta e="T572" id="Seg_1979" s="T571">n-n:case</ta>
            <ta e="T573" id="Seg_1980" s="T572">ptcl</ta>
            <ta e="T574" id="Seg_1981" s="T573">v-v:n.fin</ta>
            <ta e="T575" id="Seg_1982" s="T574">n-n:case</ta>
            <ta e="T576" id="Seg_1983" s="T575">conj</ta>
            <ta e="T577" id="Seg_1984" s="T576">n-n:case.poss</ta>
            <ta e="T578" id="Seg_1985" s="T577">v&gt;v-v-v:tense-v:pn</ta>
            <ta e="T579" id="Seg_1986" s="T578">adv</ta>
            <ta e="T580" id="Seg_1987" s="T579">dempro-n:case</ta>
            <ta e="T581" id="Seg_1988" s="T580">v-v:tense-v:pn</ta>
            <ta e="T582" id="Seg_1989" s="T581">adv</ta>
            <ta e="T583" id="Seg_1990" s="T582">v-v:tense-v:pn</ta>
            <ta e="T584" id="Seg_1991" s="T583">n-n:case.poss</ta>
            <ta e="T585" id="Seg_1992" s="T584">adv</ta>
            <ta e="T586" id="Seg_1993" s="T585">v-v:tense-v:pn</ta>
            <ta e="T587" id="Seg_1994" s="T586">ptcl</ta>
            <ta e="T588" id="Seg_1995" s="T587">v-v&gt;v-v:pn</ta>
            <ta e="T589" id="Seg_1996" s="T588">n-n:case.poss</ta>
            <ta e="T590" id="Seg_1997" s="T589">adv</ta>
            <ta e="T591" id="Seg_1998" s="T590">dempro-n:case</ta>
            <ta e="T592" id="Seg_1999" s="T591">v-v:tense-v:pn</ta>
            <ta e="T593" id="Seg_2000" s="T592">adv</ta>
            <ta e="T594" id="Seg_2001" s="T593">n-n:case</ta>
            <ta e="T596" id="Seg_2002" s="T595">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T597" id="Seg_2003" s="T596">adv</ta>
            <ta e="T598" id="Seg_2004" s="T597">n-n:case</ta>
            <ta e="T599" id="Seg_2005" s="T598">v-v&gt;v-v:pn</ta>
            <ta e="T600" id="Seg_2006" s="T599">pers</ta>
            <ta e="T601" id="Seg_2007" s="T600">pers</ta>
            <ta e="T602" id="Seg_2008" s="T601">n-n:case.poss</ta>
            <ta e="T603" id="Seg_2009" s="T602">pers</ta>
            <ta e="T604" id="Seg_2010" s="T603">n-n:num-n:case</ta>
            <ta e="T605" id="Seg_2011" s="T604">v-v:tense-v:pn</ta>
            <ta e="T606" id="Seg_2012" s="T605">adv</ta>
            <ta e="T607" id="Seg_2013" s="T606">dempro-n:case</ta>
            <ta e="T608" id="Seg_2014" s="T607">v-v:tense-v:pn</ta>
            <ta e="T609" id="Seg_2015" s="T608">dempro-n:case</ta>
            <ta e="T610" id="Seg_2016" s="T609">dempro-n:case</ta>
            <ta e="T611" id="Seg_2017" s="T610">v-v&gt;v-v:pn</ta>
            <ta e="T612" id="Seg_2018" s="T611">pers</ta>
            <ta e="T613" id="Seg_2019" s="T612">v-v:tense-v:pn</ta>
            <ta e="T614" id="Seg_2020" s="T613">n-n:case.poss</ta>
            <ta e="T616" id="Seg_2021" s="T615">v-v:n.fin</ta>
            <ta e="T619" id="Seg_2022" s="T618">v-v:tense-v:pn</ta>
            <ta e="T620" id="Seg_2023" s="T619">v-v:tense-v:pn</ta>
            <ta e="T621" id="Seg_2024" s="T620">n-n:case.poss</ta>
            <ta e="T622" id="Seg_2025" s="T621">v-v&gt;v-v:pn</ta>
            <ta e="T623" id="Seg_2026" s="T622">n-n:case.poss-n:case</ta>
            <ta e="T624" id="Seg_2027" s="T623">v-v&gt;v-v:pn</ta>
            <ta e="T625" id="Seg_2028" s="T624">n-n:case.poss</ta>
            <ta e="T626" id="Seg_2029" s="T625">pers</ta>
            <ta e="T627" id="Seg_2030" s="T626">pers</ta>
            <ta e="T628" id="Seg_2031" s="T627">v-v:tense-v:pn</ta>
            <ta e="T629" id="Seg_2032" s="T628">conj</ta>
            <ta e="T630" id="Seg_2033" s="T629">dempro-n:case</ta>
            <ta e="T631" id="Seg_2034" s="T630">v-v:tense-v:pn</ta>
            <ta e="T632" id="Seg_2035" s="T631">que</ta>
            <ta e="T633" id="Seg_2036" s="T632">que</ta>
            <ta e="T634" id="Seg_2037" s="T633">v-v&gt;v-v:pn</ta>
            <ta e="T635" id="Seg_2038" s="T634">adv</ta>
            <ta e="T636" id="Seg_2039" s="T635">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T637" id="Seg_2040" s="T636">dempro-n:case</ta>
            <ta e="T638" id="Seg_2041" s="T637">n-n:case</ta>
            <ta e="T639" id="Seg_2042" s="T638">pers</ta>
            <ta e="T640" id="Seg_2043" s="T639">pers</ta>
            <ta e="T641" id="Seg_2044" s="T640">n-n:case.poss</ta>
            <ta e="T642" id="Seg_2045" s="T641">v-v:mood.pn</ta>
            <ta e="T643" id="Seg_2046" s="T642">v-v:ins-v:mood.pn</ta>
            <ta e="T644" id="Seg_2047" s="T643">conj</ta>
            <ta e="T645" id="Seg_2048" s="T644">pers</ta>
            <ta e="T646" id="Seg_2049" s="T645">v-v:tense-v:pn</ta>
            <ta e="T647" id="Seg_2050" s="T646">adv</ta>
            <ta e="T649" id="Seg_2051" s="T648">n-n:case</ta>
            <ta e="T650" id="Seg_2052" s="T649">n-n:case</ta>
            <ta e="T651" id="Seg_2053" s="T650">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T652" id="Seg_2054" s="T651">conj</ta>
            <ta e="T653" id="Seg_2055" s="T652">v-v&gt;v-v:pn</ta>
            <ta e="T654" id="Seg_2056" s="T653">conj</ta>
            <ta e="T655" id="Seg_2057" s="T654">dempro-n:case</ta>
            <ta e="T656" id="Seg_2058" s="T655">n-n:case</ta>
            <ta e="T657" id="Seg_2059" s="T656">v-v&gt;v-v:pn</ta>
            <ta e="T658" id="Seg_2060" s="T657">adv</ta>
            <ta e="T659" id="Seg_2061" s="T658">v-v:tense-v:pn</ta>
            <ta e="T660" id="Seg_2062" s="T659">n-n:case</ta>
            <ta e="T661" id="Seg_2063" s="T660">n-n:case</ta>
            <ta e="T662" id="Seg_2064" s="T661">v-v:tense-v:pn</ta>
            <ta e="T663" id="Seg_2065" s="T662">n-n:num</ta>
            <ta e="T664" id="Seg_2066" s="T663">v-v&gt;v-v:pn</ta>
            <ta e="T665" id="Seg_2067" s="T664">conj</ta>
            <ta e="T666" id="Seg_2068" s="T665">n-n:case</ta>
            <ta e="T667" id="Seg_2069" s="T666">v-v:pn</ta>
            <ta e="T668" id="Seg_2070" s="T667">adv</ta>
            <ta e="T669" id="Seg_2071" s="T668">v-v&gt;v-v:pn</ta>
            <ta e="T670" id="Seg_2072" s="T669">que-n:case</ta>
            <ta e="T671" id="Seg_2073" s="T670">n-n:num</ta>
            <ta e="T672" id="Seg_2074" s="T671">v-v&gt;v-v:pn</ta>
            <ta e="T673" id="Seg_2075" s="T672">conj</ta>
            <ta e="T674" id="Seg_2076" s="T673">que-n:case</ta>
            <ta e="T675" id="Seg_2077" s="T674">ptcl</ta>
            <ta e="T676" id="Seg_2078" s="T675">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T677" id="Seg_2079" s="T676">dempro</ta>
            <ta e="T678" id="Seg_2080" s="T677">pers</ta>
            <ta e="T679" id="Seg_2081" s="T678">n-n:case.poss</ta>
            <ta e="T680" id="Seg_2082" s="T679">v-v&gt;v-v:pn</ta>
            <ta e="T681" id="Seg_2083" s="T680">adv</ta>
            <ta e="T682" id="Seg_2084" s="T681">v-v:mood.pn</ta>
            <ta e="T683" id="Seg_2085" s="T682">pers</ta>
            <ta e="T684" id="Seg_2086" s="T683">dempro-n:case</ta>
            <ta e="T685" id="Seg_2087" s="T684">dempro-n:case</ta>
            <ta e="T686" id="Seg_2088" s="T685">v-v:tense-v:pn</ta>
            <ta e="T687" id="Seg_2089" s="T686">dempro-n:case</ta>
            <ta e="T688" id="Seg_2090" s="T687">quant</ta>
            <ta e="T689" id="Seg_2091" s="T688">n-n:case</ta>
            <ta e="T690" id="Seg_2092" s="T689">v-v:tense-v:pn</ta>
            <ta e="T691" id="Seg_2093" s="T690">adv</ta>
            <ta e="T692" id="Seg_2094" s="T691">v-v:tense-v:pn</ta>
            <ta e="T693" id="Seg_2095" s="T692">dempro-n:case</ta>
            <ta e="T694" id="Seg_2096" s="T693">n-n:case</ta>
            <ta e="T695" id="Seg_2097" s="T694">dempro-n:case</ta>
            <ta e="T696" id="Seg_2098" s="T695">n-n:case.poss</ta>
            <ta e="T697" id="Seg_2099" s="T696">ptcl</ta>
            <ta e="T698" id="Seg_2100" s="T697">v&gt;v-v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T699" id="Seg_2101" s="T698">v-v:n.fin</ta>
            <ta e="T700" id="Seg_2102" s="T699">v-v:tense-v:pn</ta>
            <ta e="T701" id="Seg_2103" s="T700">adv</ta>
            <ta e="T702" id="Seg_2104" s="T701">v-v:tense-v:pn</ta>
            <ta e="T703" id="Seg_2105" s="T702">v-v:tense-v:pn</ta>
            <ta e="T704" id="Seg_2106" s="T703">n-n:case</ta>
            <ta e="T705" id="Seg_2107" s="T704">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T706" id="Seg_2108" s="T705">conj</ta>
            <ta e="T707" id="Seg_2109" s="T706">n-n:case</ta>
            <ta e="T708" id="Seg_2110" s="T707">adv</ta>
            <ta e="T709" id="Seg_2111" s="T708">v-v:tense-v:pn</ta>
            <ta e="T710" id="Seg_2112" s="T709">dempro-n:case</ta>
            <ta e="T711" id="Seg_2113" s="T710">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T712" id="Seg_2114" s="T711">adv</ta>
            <ta e="T713" id="Seg_2115" s="T712">n-n:num</ta>
            <ta e="T714" id="Seg_2116" s="T713">ptcl</ta>
            <ta e="T715" id="Seg_2117" s="T714">n-n:case</ta>
            <ta e="T716" id="Seg_2118" s="T715">v-v:tense-v:pn</ta>
            <ta e="T717" id="Seg_2119" s="T716">dempro-n:case</ta>
            <ta e="T718" id="Seg_2120" s="T717">n-n:case</ta>
            <ta e="T719" id="Seg_2121" s="T718">n-n:case</ta>
            <ta e="T720" id="Seg_2122" s="T719">adv</ta>
            <ta e="T722" id="Seg_2123" s="T721">v-v:n.fin</ta>
            <ta e="T723" id="Seg_2124" s="T722">conj</ta>
            <ta e="T724" id="Seg_2125" s="T723">dempro-n:case</ta>
            <ta e="T725" id="Seg_2126" s="T724">n-n:case</ta>
            <ta e="T726" id="Seg_2127" s="T725">v-v&gt;v-v:pn</ta>
            <ta e="T727" id="Seg_2128" s="T726">v-v:mood.pn</ta>
            <ta e="T728" id="Seg_2129" s="T727">n-n:case</ta>
            <ta e="T729" id="Seg_2130" s="T728">v-v&gt;v-v:pn</ta>
            <ta e="T730" id="Seg_2131" s="T729">adv</ta>
            <ta e="T731" id="Seg_2132" s="T730">adv</ta>
            <ta e="T732" id="Seg_2133" s="T731">v-v:tense-v:pn</ta>
            <ta e="T733" id="Seg_2134" s="T732">dempro-n:case</ta>
            <ta e="T734" id="Seg_2135" s="T733">adv</ta>
            <ta e="T735" id="Seg_2136" s="T734">v-v:tense-v:pn</ta>
            <ta e="T736" id="Seg_2137" s="T735">n-n:case</ta>
            <ta e="T737" id="Seg_2138" s="T736">adv</ta>
            <ta e="T738" id="Seg_2139" s="T737">v-v&gt;v-v:pn</ta>
            <ta e="T739" id="Seg_2140" s="T738">v-v:ins-v:mood.pn</ta>
            <ta e="T740" id="Seg_2141" s="T739">v-v:mood.pn</ta>
            <ta e="T741" id="Seg_2142" s="T740">pers</ta>
            <ta e="T742" id="Seg_2143" s="T741">conj</ta>
            <ta e="T743" id="Seg_2144" s="T742">dempro-n:case</ta>
            <ta e="T744" id="Seg_2145" s="T743">v-v&gt;v-v:pn</ta>
            <ta e="T745" id="Seg_2146" s="T744">v-v:ins-v:mood.pn</ta>
            <ta e="T746" id="Seg_2147" s="T745">n-n:case</ta>
            <ta e="T747" id="Seg_2148" s="T746">conj</ta>
            <ta e="T748" id="Seg_2149" s="T747">n-n:case</ta>
            <ta e="T749" id="Seg_2150" s="T748">dempro-n:case</ta>
            <ta e="T750" id="Seg_2151" s="T749">dempro-n:case</ta>
            <ta e="T751" id="Seg_2152" s="T750">v-v:tense-v:pn</ta>
            <ta e="T752" id="Seg_2153" s="T751">n-n:case</ta>
            <ta e="T753" id="Seg_2154" s="T752">conj</ta>
            <ta e="T754" id="Seg_2155" s="T753">n-n:case</ta>
            <ta e="T755" id="Seg_2156" s="T754">n-n:case.poss</ta>
            <ta e="T756" id="Seg_2157" s="T755">dempro-n:case</ta>
            <ta e="T757" id="Seg_2158" s="T756">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T758" id="Seg_2159" s="T757">conj</ta>
            <ta e="T759" id="Seg_2160" s="T758">refl-n:case.poss</ta>
            <ta e="T760" id="Seg_2161" s="T759">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T761" id="Seg_2162" s="T760">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T569" id="Seg_2163" s="T568">v</ta>
            <ta e="T570" id="Seg_2164" s="T569">n</ta>
            <ta e="T571" id="Seg_2165" s="T570">n</ta>
            <ta e="T572" id="Seg_2166" s="T571">n</ta>
            <ta e="T573" id="Seg_2167" s="T572">ptcl</ta>
            <ta e="T574" id="Seg_2168" s="T573">v</ta>
            <ta e="T575" id="Seg_2169" s="T574">n</ta>
            <ta e="T576" id="Seg_2170" s="T575">conj</ta>
            <ta e="T577" id="Seg_2171" s="T576">n</ta>
            <ta e="T578" id="Seg_2172" s="T577">v</ta>
            <ta e="T579" id="Seg_2173" s="T578">adv</ta>
            <ta e="T580" id="Seg_2174" s="T579">dempro</ta>
            <ta e="T581" id="Seg_2175" s="T580">v</ta>
            <ta e="T582" id="Seg_2176" s="T581">adv</ta>
            <ta e="T583" id="Seg_2177" s="T582">v</ta>
            <ta e="T584" id="Seg_2178" s="T583">n</ta>
            <ta e="T585" id="Seg_2179" s="T584">adv</ta>
            <ta e="T586" id="Seg_2180" s="T585">v</ta>
            <ta e="T587" id="Seg_2181" s="T586">ptcl</ta>
            <ta e="T588" id="Seg_2182" s="T587">v</ta>
            <ta e="T589" id="Seg_2183" s="T588">n</ta>
            <ta e="T590" id="Seg_2184" s="T589">adv</ta>
            <ta e="T591" id="Seg_2185" s="T590">dempro</ta>
            <ta e="T592" id="Seg_2186" s="T591">v</ta>
            <ta e="T593" id="Seg_2187" s="T592">adv</ta>
            <ta e="T594" id="Seg_2188" s="T593">n</ta>
            <ta e="T596" id="Seg_2189" s="T595">v</ta>
            <ta e="T597" id="Seg_2190" s="T596">adv</ta>
            <ta e="T598" id="Seg_2191" s="T597">n</ta>
            <ta e="T599" id="Seg_2192" s="T598">v</ta>
            <ta e="T600" id="Seg_2193" s="T599">pers</ta>
            <ta e="T601" id="Seg_2194" s="T600">pers</ta>
            <ta e="T602" id="Seg_2195" s="T601">n</ta>
            <ta e="T603" id="Seg_2196" s="T602">pers</ta>
            <ta e="T604" id="Seg_2197" s="T603">n</ta>
            <ta e="T605" id="Seg_2198" s="T604">v</ta>
            <ta e="T606" id="Seg_2199" s="T605">adv</ta>
            <ta e="T607" id="Seg_2200" s="T606">dempro</ta>
            <ta e="T608" id="Seg_2201" s="T607">v</ta>
            <ta e="T609" id="Seg_2202" s="T608">dempro</ta>
            <ta e="T610" id="Seg_2203" s="T609">dempro</ta>
            <ta e="T611" id="Seg_2204" s="T610">v</ta>
            <ta e="T612" id="Seg_2205" s="T611">pers</ta>
            <ta e="T613" id="Seg_2206" s="T612">v</ta>
            <ta e="T614" id="Seg_2207" s="T613">n</ta>
            <ta e="T616" id="Seg_2208" s="T615">v</ta>
            <ta e="T619" id="Seg_2209" s="T618">v</ta>
            <ta e="T620" id="Seg_2210" s="T619">v</ta>
            <ta e="T621" id="Seg_2211" s="T620">n</ta>
            <ta e="T622" id="Seg_2212" s="T621">v</ta>
            <ta e="T624" id="Seg_2213" s="T623">v</ta>
            <ta e="T625" id="Seg_2214" s="T624">n</ta>
            <ta e="T626" id="Seg_2215" s="T625">pers</ta>
            <ta e="T627" id="Seg_2216" s="T626">pers</ta>
            <ta e="T628" id="Seg_2217" s="T627">v</ta>
            <ta e="T629" id="Seg_2218" s="T628">conj</ta>
            <ta e="T630" id="Seg_2219" s="T629">dempro</ta>
            <ta e="T631" id="Seg_2220" s="T630">v</ta>
            <ta e="T632" id="Seg_2221" s="T631">que</ta>
            <ta e="T633" id="Seg_2222" s="T632">que</ta>
            <ta e="T634" id="Seg_2223" s="T633">v</ta>
            <ta e="T635" id="Seg_2224" s="T634">adv</ta>
            <ta e="T636" id="Seg_2225" s="T635">v</ta>
            <ta e="T637" id="Seg_2226" s="T636">dempro</ta>
            <ta e="T638" id="Seg_2227" s="T637">n</ta>
            <ta e="T639" id="Seg_2228" s="T638">pers</ta>
            <ta e="T640" id="Seg_2229" s="T639">pers</ta>
            <ta e="T641" id="Seg_2230" s="T640">n</ta>
            <ta e="T642" id="Seg_2231" s="T641">v</ta>
            <ta e="T643" id="Seg_2232" s="T642">v</ta>
            <ta e="T644" id="Seg_2233" s="T643">conj</ta>
            <ta e="T645" id="Seg_2234" s="T644">pers</ta>
            <ta e="T646" id="Seg_2235" s="T645">v</ta>
            <ta e="T647" id="Seg_2236" s="T646">adv</ta>
            <ta e="T649" id="Seg_2237" s="T648">n</ta>
            <ta e="T650" id="Seg_2238" s="T649">n</ta>
            <ta e="T651" id="Seg_2239" s="T650">v</ta>
            <ta e="T652" id="Seg_2240" s="T651">conj</ta>
            <ta e="T653" id="Seg_2241" s="T652">v</ta>
            <ta e="T654" id="Seg_2242" s="T653">conj</ta>
            <ta e="T655" id="Seg_2243" s="T654">dempro</ta>
            <ta e="T656" id="Seg_2244" s="T655">n</ta>
            <ta e="T657" id="Seg_2245" s="T656">v</ta>
            <ta e="T658" id="Seg_2246" s="T657">adv</ta>
            <ta e="T659" id="Seg_2247" s="T658">v</ta>
            <ta e="T660" id="Seg_2248" s="T659">n</ta>
            <ta e="T661" id="Seg_2249" s="T660">n</ta>
            <ta e="T662" id="Seg_2250" s="T661">v</ta>
            <ta e="T663" id="Seg_2251" s="T662">n</ta>
            <ta e="T664" id="Seg_2252" s="T663">v</ta>
            <ta e="T665" id="Seg_2253" s="T664">conj</ta>
            <ta e="T666" id="Seg_2254" s="T665">n</ta>
            <ta e="T667" id="Seg_2255" s="T666">v</ta>
            <ta e="T668" id="Seg_2256" s="T667">adv</ta>
            <ta e="T669" id="Seg_2257" s="T668">v</ta>
            <ta e="T670" id="Seg_2258" s="T669">que</ta>
            <ta e="T671" id="Seg_2259" s="T670">n</ta>
            <ta e="T672" id="Seg_2260" s="T671">v</ta>
            <ta e="T673" id="Seg_2261" s="T672">conj</ta>
            <ta e="T674" id="Seg_2262" s="T673">que</ta>
            <ta e="T675" id="Seg_2263" s="T674">ptcl</ta>
            <ta e="T676" id="Seg_2264" s="T675">v</ta>
            <ta e="T677" id="Seg_2265" s="T676">dempro</ta>
            <ta e="T678" id="Seg_2266" s="T677">pers</ta>
            <ta e="T679" id="Seg_2267" s="T678">n</ta>
            <ta e="T680" id="Seg_2268" s="T679">v</ta>
            <ta e="T681" id="Seg_2269" s="T680">adv</ta>
            <ta e="T682" id="Seg_2270" s="T681">v</ta>
            <ta e="T683" id="Seg_2271" s="T682">pers</ta>
            <ta e="T684" id="Seg_2272" s="T683">dempro</ta>
            <ta e="T685" id="Seg_2273" s="T684">dempro</ta>
            <ta e="T686" id="Seg_2274" s="T685">v</ta>
            <ta e="T687" id="Seg_2275" s="T686">dempro</ta>
            <ta e="T688" id="Seg_2276" s="T687">quant</ta>
            <ta e="T689" id="Seg_2277" s="T688">n</ta>
            <ta e="T690" id="Seg_2278" s="T689">v</ta>
            <ta e="T691" id="Seg_2279" s="T690">adv</ta>
            <ta e="T692" id="Seg_2280" s="T691">v</ta>
            <ta e="T693" id="Seg_2281" s="T692">dempro</ta>
            <ta e="T694" id="Seg_2282" s="T693">n</ta>
            <ta e="T695" id="Seg_2283" s="T694">dempro</ta>
            <ta e="T696" id="Seg_2284" s="T695">n</ta>
            <ta e="T697" id="Seg_2285" s="T696">ptcl</ta>
            <ta e="T698" id="Seg_2286" s="T697">v</ta>
            <ta e="T699" id="Seg_2287" s="T698">v</ta>
            <ta e="T700" id="Seg_2288" s="T699">v</ta>
            <ta e="T701" id="Seg_2289" s="T700">adv</ta>
            <ta e="T702" id="Seg_2290" s="T701">v</ta>
            <ta e="T703" id="Seg_2291" s="T702">v</ta>
            <ta e="T704" id="Seg_2292" s="T703">n</ta>
            <ta e="T705" id="Seg_2293" s="T704">v</ta>
            <ta e="T706" id="Seg_2294" s="T705">conj</ta>
            <ta e="T707" id="Seg_2295" s="T706">n</ta>
            <ta e="T708" id="Seg_2296" s="T707">adv</ta>
            <ta e="T709" id="Seg_2297" s="T708">v</ta>
            <ta e="T710" id="Seg_2298" s="T709">dempro</ta>
            <ta e="T711" id="Seg_2299" s="T710">v</ta>
            <ta e="T712" id="Seg_2300" s="T711">adv</ta>
            <ta e="T713" id="Seg_2301" s="T712">n</ta>
            <ta e="T714" id="Seg_2302" s="T713">ptcl</ta>
            <ta e="T715" id="Seg_2303" s="T714">n</ta>
            <ta e="T716" id="Seg_2304" s="T715">v</ta>
            <ta e="T717" id="Seg_2305" s="T716">dempro</ta>
            <ta e="T718" id="Seg_2306" s="T717">n</ta>
            <ta e="T719" id="Seg_2307" s="T718">n</ta>
            <ta e="T720" id="Seg_2308" s="T719">adv</ta>
            <ta e="T722" id="Seg_2309" s="T721">v</ta>
            <ta e="T723" id="Seg_2310" s="T722">conj</ta>
            <ta e="T724" id="Seg_2311" s="T723">dempro</ta>
            <ta e="T725" id="Seg_2312" s="T724">n</ta>
            <ta e="T726" id="Seg_2313" s="T725">v</ta>
            <ta e="T727" id="Seg_2314" s="T726">v</ta>
            <ta e="T728" id="Seg_2315" s="T727">n</ta>
            <ta e="T729" id="Seg_2316" s="T728">v</ta>
            <ta e="T730" id="Seg_2317" s="T729">adv</ta>
            <ta e="T731" id="Seg_2318" s="T730">adv</ta>
            <ta e="T732" id="Seg_2319" s="T731">v</ta>
            <ta e="T733" id="Seg_2320" s="T732">dempro</ta>
            <ta e="T734" id="Seg_2321" s="T733">adv</ta>
            <ta e="T735" id="Seg_2322" s="T734">v</ta>
            <ta e="T736" id="Seg_2323" s="T735">n</ta>
            <ta e="T737" id="Seg_2324" s="T736">adv</ta>
            <ta e="T738" id="Seg_2325" s="T737">v</ta>
            <ta e="T739" id="Seg_2326" s="T738">v</ta>
            <ta e="T740" id="Seg_2327" s="T739">v</ta>
            <ta e="T741" id="Seg_2328" s="T740">pers</ta>
            <ta e="T742" id="Seg_2329" s="T741">conj</ta>
            <ta e="T743" id="Seg_2330" s="T742">dempro</ta>
            <ta e="T744" id="Seg_2331" s="T743">v</ta>
            <ta e="T745" id="Seg_2332" s="T744">v</ta>
            <ta e="T746" id="Seg_2333" s="T745">n</ta>
            <ta e="T747" id="Seg_2334" s="T746">conj</ta>
            <ta e="T748" id="Seg_2335" s="T747">n</ta>
            <ta e="T749" id="Seg_2336" s="T748">dempro</ta>
            <ta e="T750" id="Seg_2337" s="T749">dempro</ta>
            <ta e="T751" id="Seg_2338" s="T750">v</ta>
            <ta e="T752" id="Seg_2339" s="T751">n</ta>
            <ta e="T753" id="Seg_2340" s="T752">conj</ta>
            <ta e="T754" id="Seg_2341" s="T753">n</ta>
            <ta e="T755" id="Seg_2342" s="T754">n</ta>
            <ta e="T756" id="Seg_2343" s="T755">dempro</ta>
            <ta e="T757" id="Seg_2344" s="T756">v</ta>
            <ta e="T758" id="Seg_2345" s="T757">conj</ta>
            <ta e="T759" id="Seg_2346" s="T758">refl</ta>
            <ta e="T760" id="Seg_2347" s="T759">v</ta>
            <ta e="T761" id="Seg_2348" s="T760">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T570" id="Seg_2349" s="T569">np.h:E</ta>
            <ta e="T571" id="Seg_2350" s="T570">np.h:Com</ta>
            <ta e="T572" id="Seg_2351" s="T571">np.h:A</ta>
            <ta e="T575" id="Seg_2352" s="T574">np:P</ta>
            <ta e="T577" id="Seg_2353" s="T576">np:P</ta>
            <ta e="T579" id="Seg_2354" s="T578">adv:Time</ta>
            <ta e="T580" id="Seg_2355" s="T579">pro:P</ta>
            <ta e="T581" id="Seg_2356" s="T580">0.3.h:A</ta>
            <ta e="T582" id="Seg_2357" s="T581">adv:L</ta>
            <ta e="T583" id="Seg_2358" s="T582">0.3.h:A</ta>
            <ta e="T584" id="Seg_2359" s="T583">np:G</ta>
            <ta e="T585" id="Seg_2360" s="T584">adv:Time</ta>
            <ta e="T586" id="Seg_2361" s="T585">0.3.h:E</ta>
            <ta e="T589" id="Seg_2362" s="T588">np:E</ta>
            <ta e="T590" id="Seg_2363" s="T589">adv:Time</ta>
            <ta e="T591" id="Seg_2364" s="T590">pro.h:E</ta>
            <ta e="T593" id="Seg_2365" s="T592">adv:L</ta>
            <ta e="T594" id="Seg_2366" s="T593">np.h:Th</ta>
            <ta e="T596" id="Seg_2367" s="T595">0.3.h:E</ta>
            <ta e="T597" id="Seg_2368" s="T596">adv:L</ta>
            <ta e="T598" id="Seg_2369" s="T597">np.h:E</ta>
            <ta e="T600" id="Seg_2370" s="T599">pro.h:Th</ta>
            <ta e="T601" id="Seg_2371" s="T600">pro.h:Poss</ta>
            <ta e="T602" id="Seg_2372" s="T601">np:Th</ta>
            <ta e="T603" id="Seg_2373" s="T602">pro.h:Poss</ta>
            <ta e="T604" id="Seg_2374" s="T603">np:So</ta>
            <ta e="T605" id="Seg_2375" s="T604">0.1.h:P</ta>
            <ta e="T606" id="Seg_2376" s="T605">adv:Time</ta>
            <ta e="T607" id="Seg_2377" s="T606">pro.h:A</ta>
            <ta e="T609" id="Seg_2378" s="T608">pro.h:Th</ta>
            <ta e="T610" id="Seg_2379" s="T609">pro.h:A</ta>
            <ta e="T612" id="Seg_2380" s="T611">pro.h:A</ta>
            <ta e="T614" id="Seg_2381" s="T613">np:G</ta>
            <ta e="T619" id="Seg_2382" s="T618">0.3.h:A</ta>
            <ta e="T620" id="Seg_2383" s="T619">0.3.h:A</ta>
            <ta e="T621" id="Seg_2384" s="T620">np.h:A 0.3.h:Poss</ta>
            <ta e="T623" id="Seg_2385" s="T622">np:Ins</ta>
            <ta e="T624" id="Seg_2386" s="T623">0.3.h:A</ta>
            <ta e="T626" id="Seg_2387" s="T625">pro.h:A</ta>
            <ta e="T627" id="Seg_2388" s="T626">pro:G</ta>
            <ta e="T630" id="Seg_2389" s="T629">pro.h:A</ta>
            <ta e="T633" id="Seg_2390" s="T632">pro.h:A</ta>
            <ta e="T635" id="Seg_2391" s="T634">adv:Time</ta>
            <ta e="T636" id="Seg_2392" s="T635">0.3.h:E</ta>
            <ta e="T638" id="Seg_2393" s="T637">np.h:Th</ta>
            <ta e="T639" id="Seg_2394" s="T638">pro.h:Th</ta>
            <ta e="T640" id="Seg_2395" s="T639">pro.h:Poss</ta>
            <ta e="T641" id="Seg_2396" s="T640">np.h:Th</ta>
            <ta e="T642" id="Seg_2397" s="T641">0.2.h:A</ta>
            <ta e="T643" id="Seg_2398" s="T642">0.2.h:A</ta>
            <ta e="T645" id="Seg_2399" s="T644">pro.h:A</ta>
            <ta e="T647" id="Seg_2400" s="T646">adv:Time</ta>
            <ta e="T649" id="Seg_2401" s="T648">np:Poss</ta>
            <ta e="T650" id="Seg_2402" s="T649">np:G</ta>
            <ta e="T651" id="Seg_2403" s="T650">0.3.h:A</ta>
            <ta e="T653" id="Seg_2404" s="T652">0.3.h:A</ta>
            <ta e="T656" id="Seg_2405" s="T655">np.h:A</ta>
            <ta e="T658" id="Seg_2406" s="T657">adv:Time</ta>
            <ta e="T661" id="Seg_2407" s="T660">np.h:A</ta>
            <ta e="T662" id="Seg_2408" s="T661">0.3.h:E</ta>
            <ta e="T663" id="Seg_2409" s="T662">np:A</ta>
            <ta e="T666" id="Seg_2410" s="T665">np.h:Th</ta>
            <ta e="T668" id="Seg_2411" s="T667">adv:Time</ta>
            <ta e="T669" id="Seg_2412" s="T668">0.3.h:A</ta>
            <ta e="T671" id="Seg_2413" s="T670">np:A</ta>
            <ta e="T674" id="Seg_2414" s="T673">pro:Th</ta>
            <ta e="T676" id="Seg_2415" s="T675">0.2.h:E</ta>
            <ta e="T678" id="Seg_2416" s="T677">pro.h:Poss</ta>
            <ta e="T679" id="Seg_2417" s="T678">np.h:A</ta>
            <ta e="T681" id="Seg_2418" s="T680">adv:Time</ta>
            <ta e="T682" id="Seg_2419" s="T681">0.2.h:A</ta>
            <ta e="T683" id="Seg_2420" s="T682">pro.h:B</ta>
            <ta e="T684" id="Seg_2421" s="T683">pro.h:Th</ta>
            <ta e="T685" id="Seg_2422" s="T684">pro.h:A</ta>
            <ta e="T687" id="Seg_2423" s="T686">pro.h:A</ta>
            <ta e="T689" id="Seg_2424" s="T688">np:Th</ta>
            <ta e="T691" id="Seg_2425" s="T690">adv:Time</ta>
            <ta e="T692" id="Seg_2426" s="T691">0.3.h:A</ta>
            <ta e="T693" id="Seg_2427" s="T692">pro.h:Th</ta>
            <ta e="T694" id="Seg_2428" s="T693">np:G</ta>
            <ta e="T695" id="Seg_2429" s="T694">pro.h:A</ta>
            <ta e="T696" id="Seg_2430" s="T695">np:P</ta>
            <ta e="T700" id="Seg_2431" s="T699">0.3.h:A</ta>
            <ta e="T701" id="Seg_2432" s="T700">adv:Time</ta>
            <ta e="T702" id="Seg_2433" s="T701">0.3.h:A</ta>
            <ta e="T703" id="Seg_2434" s="T702">0.3.h:A</ta>
            <ta e="T704" id="Seg_2435" s="T703">np:L</ta>
            <ta e="T705" id="Seg_2436" s="T704">0.3.h:E</ta>
            <ta e="T707" id="Seg_2437" s="T706">np.h:A</ta>
            <ta e="T708" id="Seg_2438" s="T707">adv:Time</ta>
            <ta e="T710" id="Seg_2439" s="T709">pro.h:P</ta>
            <ta e="T711" id="Seg_2440" s="T710">0.3.h:A</ta>
            <ta e="T712" id="Seg_2441" s="T711">adv:Time</ta>
            <ta e="T713" id="Seg_2442" s="T712">np:Th</ta>
            <ta e="T715" id="Seg_2443" s="T714">np.h:A</ta>
            <ta e="T718" id="Seg_2444" s="T717">np.h:A</ta>
            <ta e="T719" id="Seg_2445" s="T718">pro:P</ta>
            <ta e="T725" id="Seg_2446" s="T724">np.h:A</ta>
            <ta e="T727" id="Seg_2447" s="T726">0.2.h:A</ta>
            <ta e="T728" id="Seg_2448" s="T727">pro:P</ta>
            <ta e="T729" id="Seg_2449" s="T728">0.3.h:A</ta>
            <ta e="T730" id="Seg_2450" s="T729">adv:Time</ta>
            <ta e="T732" id="Seg_2451" s="T731">0.3.h:E</ta>
            <ta e="T736" id="Seg_2452" s="T735">np.h:Th</ta>
            <ta e="T737" id="Seg_2453" s="T736">adv:Time</ta>
            <ta e="T738" id="Seg_2454" s="T737">0.3.h:A</ta>
            <ta e="T739" id="Seg_2455" s="T738">0.2.h:A</ta>
            <ta e="T740" id="Seg_2456" s="T739">0.2.h:A</ta>
            <ta e="T741" id="Seg_2457" s="T740">pro:So</ta>
            <ta e="T743" id="Seg_2458" s="T742">pro.h:A</ta>
            <ta e="T745" id="Seg_2459" s="T744">0.2.h:A</ta>
            <ta e="T746" id="Seg_2460" s="T745">np:G</ta>
            <ta e="T748" id="Seg_2461" s="T747">np:G</ta>
            <ta e="T749" id="Seg_2462" s="T748">pro.h:A</ta>
            <ta e="T750" id="Seg_2463" s="T749">pro.h:Th</ta>
            <ta e="T752" id="Seg_2464" s="T751">np:G</ta>
            <ta e="T754" id="Seg_2465" s="T753">np:G</ta>
            <ta e="T755" id="Seg_2466" s="T754">np:G</ta>
            <ta e="T756" id="Seg_2467" s="T755">pro.h:A</ta>
            <ta e="T760" id="Seg_2468" s="T759">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T569" id="Seg_2469" s="T568">v:pred</ta>
            <ta e="T570" id="Seg_2470" s="T569">np.h:S</ta>
            <ta e="T573" id="Seg_2471" s="T572">ptcl:pred</ta>
            <ta e="T575" id="Seg_2472" s="T574">np:O</ta>
            <ta e="T577" id="Seg_2473" s="T576">np:O</ta>
            <ta e="T578" id="Seg_2474" s="T577">v:pred 0.3.h:S</ta>
            <ta e="T580" id="Seg_2475" s="T579">pro:O</ta>
            <ta e="T581" id="Seg_2476" s="T580">v:pred 0.3.h:S</ta>
            <ta e="T583" id="Seg_2477" s="T582">v:pred 0.3.h:S</ta>
            <ta e="T586" id="Seg_2478" s="T585">v:pred 0.3.h:S</ta>
            <ta e="T588" id="Seg_2479" s="T587">v:pred</ta>
            <ta e="T589" id="Seg_2480" s="T588">np:S</ta>
            <ta e="T591" id="Seg_2481" s="T590">pro.h:S</ta>
            <ta e="T592" id="Seg_2482" s="T591">v:pred</ta>
            <ta e="T594" id="Seg_2483" s="T593">np.h:S</ta>
            <ta e="T596" id="Seg_2484" s="T595">v:pred 0.3.h:S</ta>
            <ta e="T598" id="Seg_2485" s="T597">np.h:S</ta>
            <ta e="T599" id="Seg_2486" s="T598">v:pred</ta>
            <ta e="T600" id="Seg_2487" s="T599">pro.h:S</ta>
            <ta e="T602" id="Seg_2488" s="T601">n:pred</ta>
            <ta e="T605" id="Seg_2489" s="T604">v:pred 0.1.h:S</ta>
            <ta e="T607" id="Seg_2490" s="T606">pro.h:S</ta>
            <ta e="T608" id="Seg_2491" s="T607">v:pred</ta>
            <ta e="T609" id="Seg_2492" s="T608">pro.h:O</ta>
            <ta e="T610" id="Seg_2493" s="T609">pro.h:S</ta>
            <ta e="T611" id="Seg_2494" s="T610">v:pred</ta>
            <ta e="T612" id="Seg_2495" s="T611">pro.h:S</ta>
            <ta e="T613" id="Seg_2496" s="T612">v:pred</ta>
            <ta e="T616" id="Seg_2497" s="T615">s:purp</ta>
            <ta e="T619" id="Seg_2498" s="T618">v:pred 0.3.h:S</ta>
            <ta e="T620" id="Seg_2499" s="T619">v:pred 0.3.h:S</ta>
            <ta e="T621" id="Seg_2500" s="T620">np.h:S</ta>
            <ta e="T622" id="Seg_2501" s="T621">v:pred</ta>
            <ta e="T624" id="Seg_2502" s="T623">v:pred 0.3.h:S</ta>
            <ta e="T626" id="Seg_2503" s="T625">pro.h:S</ta>
            <ta e="T628" id="Seg_2504" s="T627">v:pred</ta>
            <ta e="T630" id="Seg_2505" s="T629">pro.h:S</ta>
            <ta e="T631" id="Seg_2506" s="T630">v:pred</ta>
            <ta e="T633" id="Seg_2507" s="T632">pro.h:S</ta>
            <ta e="T634" id="Seg_2508" s="T633">v:pred</ta>
            <ta e="T636" id="Seg_2509" s="T635">v:pred 0.3.h:S</ta>
            <ta e="T638" id="Seg_2510" s="T637">np.h:O</ta>
            <ta e="T639" id="Seg_2511" s="T638">pro.h:S</ta>
            <ta e="T641" id="Seg_2512" s="T640">n:pred</ta>
            <ta e="T642" id="Seg_2513" s="T641">v:pred 0.2.h:S</ta>
            <ta e="T643" id="Seg_2514" s="T642">v:pred 0.2.h:S</ta>
            <ta e="T645" id="Seg_2515" s="T644">pro.h:S</ta>
            <ta e="T646" id="Seg_2516" s="T645">v:pred</ta>
            <ta e="T651" id="Seg_2517" s="T650">v:pred 0.3.h:S</ta>
            <ta e="T653" id="Seg_2518" s="T652">v:pred 0.3.h:S</ta>
            <ta e="T656" id="Seg_2519" s="T655">np.h:S</ta>
            <ta e="T657" id="Seg_2520" s="T656">v:pred</ta>
            <ta e="T659" id="Seg_2521" s="T658">v:pred</ta>
            <ta e="T661" id="Seg_2522" s="T660">np.h:S</ta>
            <ta e="T662" id="Seg_2523" s="T661">v:pred 0.3.h:S</ta>
            <ta e="T663" id="Seg_2524" s="T662">np:S</ta>
            <ta e="T664" id="Seg_2525" s="T663">v:pred</ta>
            <ta e="T666" id="Seg_2526" s="T665">np.h:S</ta>
            <ta e="T667" id="Seg_2527" s="T666">v:pred</ta>
            <ta e="T669" id="Seg_2528" s="T668">v:pred 0.3.h:S</ta>
            <ta e="T671" id="Seg_2529" s="T670">np:S</ta>
            <ta e="T672" id="Seg_2530" s="T671">v:pred</ta>
            <ta e="T674" id="Seg_2531" s="T673">pro:O</ta>
            <ta e="T675" id="Seg_2532" s="T674">ptcl.neg</ta>
            <ta e="T676" id="Seg_2533" s="T675">v:pred 0.2.h:S</ta>
            <ta e="T679" id="Seg_2534" s="T678">np.h:S</ta>
            <ta e="T680" id="Seg_2535" s="T679">v:pred</ta>
            <ta e="T682" id="Seg_2536" s="T681">v:pred 0.2.h:S</ta>
            <ta e="T684" id="Seg_2537" s="T683">pro.h:O</ta>
            <ta e="T685" id="Seg_2538" s="T684">pro.h:S</ta>
            <ta e="T686" id="Seg_2539" s="T685">v:pred</ta>
            <ta e="T687" id="Seg_2540" s="T686">pro.h:S</ta>
            <ta e="T689" id="Seg_2541" s="T688">np:O</ta>
            <ta e="T690" id="Seg_2542" s="T689">v:pred</ta>
            <ta e="T692" id="Seg_2543" s="T691">v:pred 0.3.h:S</ta>
            <ta e="T693" id="Seg_2544" s="T692">pro.h:O</ta>
            <ta e="T695" id="Seg_2545" s="T694">pro.h:S</ta>
            <ta e="T696" id="Seg_2546" s="T695">np:O</ta>
            <ta e="T698" id="Seg_2547" s="T697">v:pred</ta>
            <ta e="T699" id="Seg_2548" s="T698">conv:pred</ta>
            <ta e="T700" id="Seg_2549" s="T699">v:pred 0.3.h:S</ta>
            <ta e="T702" id="Seg_2550" s="T701">v:pred 0.3.h:S</ta>
            <ta e="T703" id="Seg_2551" s="T702">v:pred 0.3.h:S</ta>
            <ta e="T705" id="Seg_2552" s="T704"> 0.3.h:S v:pred</ta>
            <ta e="T707" id="Seg_2553" s="T706">np.h:S</ta>
            <ta e="T709" id="Seg_2554" s="T708">v:pred</ta>
            <ta e="T710" id="Seg_2555" s="T709">pro.h:O</ta>
            <ta e="T711" id="Seg_2556" s="T710">v:pred 0.3.h:S</ta>
            <ta e="T713" id="Seg_2557" s="T712">np:O</ta>
            <ta e="T715" id="Seg_2558" s="T714">np.h:S</ta>
            <ta e="T716" id="Seg_2559" s="T715">v:pred</ta>
            <ta e="T718" id="Seg_2560" s="T717">np.h:S</ta>
            <ta e="T719" id="Seg_2561" s="T718">pro:O</ta>
            <ta e="T722" id="Seg_2562" s="T721">v:pred</ta>
            <ta e="T725" id="Seg_2563" s="T724">np.h:S</ta>
            <ta e="T726" id="Seg_2564" s="T725">v:pred</ta>
            <ta e="T727" id="Seg_2565" s="T726">v:pred 0.2.h:S</ta>
            <ta e="T728" id="Seg_2566" s="T727">pro:O</ta>
            <ta e="T729" id="Seg_2567" s="T728">v:pred 0.3.h:S</ta>
            <ta e="T732" id="Seg_2568" s="T731">v:pred 0.3.h:S</ta>
            <ta e="T735" id="Seg_2569" s="T734">adj:pred</ta>
            <ta e="T736" id="Seg_2570" s="T735">np.h:S</ta>
            <ta e="T738" id="Seg_2571" s="T737">v:pred 0.3.h:S</ta>
            <ta e="T739" id="Seg_2572" s="T738">v:pred 0.2.h:S</ta>
            <ta e="T740" id="Seg_2573" s="T739">v:pred 0.2.h:S</ta>
            <ta e="T743" id="Seg_2574" s="T742">pro.h:S</ta>
            <ta e="T744" id="Seg_2575" s="T743">v:pred</ta>
            <ta e="T745" id="Seg_2576" s="T744">v:pred 0.2.h:S</ta>
            <ta e="T749" id="Seg_2577" s="T748">pro.h:S</ta>
            <ta e="T750" id="Seg_2578" s="T749">pro.h:O</ta>
            <ta e="T751" id="Seg_2579" s="T750">v:pred</ta>
            <ta e="T756" id="Seg_2580" s="T755">pro.h:S</ta>
            <ta e="T757" id="Seg_2581" s="T756">v:pred</ta>
            <ta e="T760" id="Seg_2582" s="T759">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T573" id="Seg_2583" s="T572">RUS:gram</ta>
            <ta e="T575" id="Seg_2584" s="T574">RUS:cult</ta>
            <ta e="T576" id="Seg_2585" s="T575">RUS:gram</ta>
            <ta e="T584" id="Seg_2586" s="T583">RUS:cult</ta>
            <ta e="T587" id="Seg_2587" s="T586">TURK:disc</ta>
            <ta e="T629" id="Seg_2588" s="T628">RUS:gram</ta>
            <ta e="T634" id="Seg_2589" s="T633">%TURK:core</ta>
            <ta e="T644" id="Seg_2590" s="T643">RUS:gram</ta>
            <ta e="T652" id="Seg_2591" s="T651">RUS:gram</ta>
            <ta e="T654" id="Seg_2592" s="T653">RUS:gram</ta>
            <ta e="T660" id="Seg_2593" s="T659">TURK:cult</ta>
            <ta e="T665" id="Seg_2594" s="T664">RUS:gram</ta>
            <ta e="T673" id="Seg_2595" s="T672">RUS:gram</ta>
            <ta e="T682" id="Seg_2596" s="T681">TURK:cult</ta>
            <ta e="T686" id="Seg_2597" s="T685">TURK:cult</ta>
            <ta e="T694" id="Seg_2598" s="T693">RUS:cult</ta>
            <ta e="T696" id="Seg_2599" s="T695">RUS:cult</ta>
            <ta e="T697" id="Seg_2600" s="T696">TURK:disc</ta>
            <ta e="T706" id="Seg_2601" s="T705">RUS:gram</ta>
            <ta e="T707" id="Seg_2602" s="T706">RUS:cult</ta>
            <ta e="T714" id="Seg_2603" s="T713">TURK:disc</ta>
            <ta e="T715" id="Seg_2604" s="T714">RUS:cult</ta>
            <ta e="T718" id="Seg_2605" s="T717">RUS:cult</ta>
            <ta e="T720" id="Seg_2606" s="T719">RUS:mod</ta>
            <ta e="T723" id="Seg_2607" s="T722">RUS:gram</ta>
            <ta e="T736" id="Seg_2608" s="T735">RUS:cult</ta>
            <ta e="T742" id="Seg_2609" s="T741">RUS:gram</ta>
            <ta e="T747" id="Seg_2610" s="T746">RUS:gram</ta>
            <ta e="T753" id="Seg_2611" s="T752">RUS:gram</ta>
            <ta e="T755" id="Seg_2612" s="T754">TAT:cult</ta>
            <ta e="T758" id="Seg_2613" s="T757">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T721" id="Seg_2614" s="T719">RUS:int.ins</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T571" id="Seg_2615" s="T568">Жили женщина и мужчина.</ta>
            <ta e="T575" id="Seg_2616" s="T571">Женщина стала резать капусту.</ta>
            <ta e="T578" id="Seg_2617" s="T575">И отрезала палец.</ta>
            <ta e="T584" id="Seg_2618" s="T578">Она его завязала и положила на стол.</ta>
            <ta e="T589" id="Seg_2619" s="T584">Потом смотрит: палец плачет.</ta>
            <ta e="T599" id="Seg_2620" s="T589">Потом она увидел, что это ребенок, она испугалась: ребенок там лежит.</ta>
            <ta e="T602" id="Seg_2621" s="T599">"Я твой сын!</ta>
            <ta e="T605" id="Seg_2622" s="T602">Я [родился] из твоего пальца!"</ta>
            <ta e="T609" id="Seg_2623" s="T605">Она его взяла. [?]</ta>
            <ta e="T616" id="Seg_2624" s="T609">Он говорит: "Я пойду к отцу пахать".</ta>
            <ta e="T623" id="Seg_2625" s="T616">Идет, пришел, отец пашет на лошади.</ta>
            <ta e="T628" id="Seg_2626" s="T623">Говорит: "Отец, я к тебе пришел".</ta>
            <ta e="T634" id="Seg_2627" s="T628">А тот говорит: "Кто где разговаривает?"</ta>
            <ta e="T638" id="Seg_2628" s="T634">Потом заметил ребенка.</ta>
            <ta e="T641" id="Seg_2629" s="T638">"Я твой сын!</ta>
            <ta e="T646" id="Seg_2630" s="T641">Садись, поешь, а я буду пахать".</ta>
            <ta e="T653" id="Seg_2631" s="T646">Он залез к лошади в ухо и [стал] пахать.</ta>
            <ta e="T657" id="Seg_2632" s="T653">А тот мужчина ест.</ta>
            <ta e="T661" id="Seg_2633" s="T657">Потом пришел барин.</ta>
            <ta e="T664" id="Seg_2634" s="T661">Видит: лошади идут.</ta>
            <ta e="T667" id="Seg_2635" s="T664">А человека нет.</ta>
            <ta e="T669" id="Seg_2636" s="T667">Он говорит:</ta>
            <ta e="T676" id="Seg_2637" s="T669">"Почему лошади идут, а никого не видно?"</ta>
            <ta e="T680" id="Seg_2638" s="T676">"Это мой сын пашет".</ta>
            <ta e="T684" id="Seg_2639" s="T680">"Продай его мне!"</ta>
            <ta e="T690" id="Seg_2640" s="T684">Он продал, тот дал много денег.</ta>
            <ta e="T694" id="Seg_2641" s="T690">Положил его в карман.</ta>
            <ta e="T698" id="Seg_2642" s="T694">Тот карман оторвал.</ta>
            <ta e="T700" id="Seg_2643" s="T698">И вышел наружу.</ta>
            <ta e="T703" id="Seg_2644" s="T700">Шел, шел.</ta>
            <ta e="T705" id="Seg_2645" s="T703">Уснул на дороге.</ta>
            <ta e="T711" id="Seg_2646" s="T705">Волк пришел, съел его.</ta>
            <ta e="T716" id="Seg_2647" s="T711">Пастух пасет овец.</ta>
            <ta e="T722" id="Seg_2648" s="T716">Волк только начнет хватать овцу.</ta>
            <ta e="T727" id="Seg_2649" s="T722">А мальчик кричит: "Вставай!</ta>
            <ta e="T729" id="Seg_2650" s="T727">Он ест овцу!"</ta>
            <ta e="T736" id="Seg_2651" s="T729">Долго он [там] сидел, волк стал очень голодным.</ta>
            <ta e="T738" id="Seg_2652" s="T736">Тогда он говорит:</ta>
            <ta e="T741" id="Seg_2653" s="T738">"Иди, уходи от меня!"</ta>
            <ta e="T748" id="Seg_2654" s="T741">А [мальчик] говорит: "Отвези меня к отцу и матери!"</ta>
            <ta e="T754" id="Seg_2655" s="T748">[Волк] отвез его к отцу и матери.</ta>
            <ta e="T757" id="Seg_2656" s="T754">В дом его бросил.</ta>
            <ta e="T760" id="Seg_2657" s="T757">А сам убежал.</ta>
            <ta e="T761" id="Seg_2658" s="T760">Все.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T571" id="Seg_2659" s="T568">[Once] there lived a woman with a man.</ta>
            <ta e="T575" id="Seg_2660" s="T571">The woman started to cut cabbage.</ta>
            <ta e="T578" id="Seg_2661" s="T575">And cut off her finger.</ta>
            <ta e="T584" id="Seg_2662" s="T578">Then she tied it up, put it there on the table.</ta>
            <ta e="T589" id="Seg_2663" s="T584">Then she sees, her finger is crying.</ta>
            <ta e="T599" id="Seg_2664" s="T589">Then she saw there a child, she got scared, a child is lying there.</ta>
            <ta e="T602" id="Seg_2665" s="T599">"I am your son!</ta>
            <ta e="T605" id="Seg_2666" s="T602">I came out of your fingers!"</ta>
            <ta e="T609" id="Seg_2667" s="T605">Then she took him. [?]</ta>
            <ta e="T616" id="Seg_2668" s="T609">He says: "I will go to father [to help] to plough."</ta>
            <ta e="T623" id="Seg_2669" s="T616">He comes, he came, his father is ploughing with his horse.</ta>
            <ta e="T628" id="Seg_2670" s="T623">He says: "Father, I came to you."</ta>
            <ta e="T634" id="Seg_2671" s="T628">But he said: "Who and where is talking?"</ta>
            <ta e="T638" id="Seg_2672" s="T634">Then he noticed the child.</ta>
            <ta e="T641" id="Seg_2673" s="T638">"I am your son!</ta>
            <ta e="T646" id="Seg_2674" s="T641">Sit and eat, and I will plough!"</ta>
            <ta e="T653" id="Seg_2675" s="T646">Then he entered the horse’s ear and is ploughing.</ta>
            <ta e="T657" id="Seg_2676" s="T653">And the man is eating.</ta>
            <ta e="T661" id="Seg_2677" s="T657">Then a landlord comes.</ta>
            <ta e="T664" id="Seg_2678" s="T661">He sees: horses are going.</ta>
            <ta e="T667" id="Seg_2679" s="T664">And there is nobody.</ta>
            <ta e="T669" id="Seg_2680" s="T667">Then he says:</ta>
            <ta e="T676" id="Seg_2681" s="T669">"Why are the horses going, but one can't see anything."</ta>
            <ta e="T680" id="Seg_2682" s="T676">"It is my son ploughing."</ta>
            <ta e="T684" id="Seg_2683" s="T680">Then: "Sell him to me!"</ta>
            <ta e="T690" id="Seg_2684" s="T684">He sold, he gave a lot of money.</ta>
            <ta e="T694" id="Seg_2685" s="T690">Then he put him in the pocket.</ta>
            <ta e="T698" id="Seg_2686" s="T694">He tore apart his pocket.</ta>
            <ta e="T700" id="Seg_2687" s="T698">He went away.</ta>
            <ta e="T703" id="Seg_2688" s="T700">Then he came, he came.</ta>
            <ta e="T705" id="Seg_2689" s="T703">He fell asleep on the road.</ta>
            <ta e="T711" id="Seg_2690" s="T705">But the wolf came, ate him.</ta>
            <ta e="T716" id="Seg_2691" s="T711">A herdsman is watching sheep.</ta>
            <ta e="T722" id="Seg_2692" s="T716">The wolf just starts to take a sheep.</ta>
            <ta e="T727" id="Seg_2693" s="T722">And the boy yells: "Get up!</ta>
            <ta e="T729" id="Seg_2694" s="T727">It is eating the sheep!"</ta>
            <ta e="T736" id="Seg_2695" s="T729">Then for a long time he sat, the wolf got very hungry.</ta>
            <ta e="T738" id="Seg_2696" s="T736">Then it says:</ta>
            <ta e="T741" id="Seg_2697" s="T738">"Go out of me!"</ta>
            <ta e="T748" id="Seg_2698" s="T741">But he says: "Take [me] to my father and my mother!"</ta>
            <ta e="T754" id="Seg_2699" s="T748">It brought him to father and mother.</ta>
            <ta e="T757" id="Seg_2700" s="T754">It threw him into his house.</ta>
            <ta e="T760" id="Seg_2701" s="T757">But itself it ran away.</ta>
            <ta e="T761" id="Seg_2702" s="T760">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T571" id="Seg_2703" s="T568">Es lebte [einmal] eine Frau mit einem Mann.</ta>
            <ta e="T575" id="Seg_2704" s="T571">Di Frau fing an, Kohl zu schneiden.</ta>
            <ta e="T578" id="Seg_2705" s="T575">Und schnitt sich den Finger ab.</ta>
            <ta e="T584" id="Seg_2706" s="T578">Dann schnürte sie ihn ein, legte ihn dort auf den Tisch.</ta>
            <ta e="T589" id="Seg_2707" s="T584">Dann sieht sie, ihr Finger weint.</ta>
            <ta e="T599" id="Seg_2708" s="T589">Dann sah sie ein Kind da, sie bekam Angst, ein Kind liegt da.</ta>
            <ta e="T602" id="Seg_2709" s="T599">„Ich bin dein Sohn!</ta>
            <ta e="T605" id="Seg_2710" s="T602">Ich kam von deinen Fingern heraus!“</ta>
            <ta e="T609" id="Seg_2711" s="T605">Dann nahm sie ihn. [?]</ta>
            <ta e="T616" id="Seg_2712" s="T609">Er sagt: „Ich werde zu Vater gehen, [um] pflügen [zu helfen].“</ta>
            <ta e="T623" id="Seg_2713" s="T616">Er kommt, er kam, sein Vater pflügt mit seinem Pferd.</ta>
            <ta e="T628" id="Seg_2714" s="T623">Er sagt: „Vater, ich kam zu dir.“</ta>
            <ta e="T634" id="Seg_2715" s="T628">Aber er sagt: „Wer und wo redet?“</ta>
            <ta e="T638" id="Seg_2716" s="T634">Dann bemerkte er das Kind.</ta>
            <ta e="T641" id="Seg_2717" s="T638">„Ich bin dein Sohn!</ta>
            <ta e="T646" id="Seg_2718" s="T641">Sitz und iss, und ich werde pflügen!“ </ta>
            <ta e="T653" id="Seg_2719" s="T646">Dann schlüpfte er in das Ohr des Pferdes und pflügte.</ta>
            <ta e="T657" id="Seg_2720" s="T653">Und der Mann isst.</ta>
            <ta e="T661" id="Seg_2721" s="T657">Dann kommt ein Anführer.</ta>
            <ta e="T664" id="Seg_2722" s="T661">Er sieht: Pferde gehen.</ta>
            <ta e="T667" id="Seg_2723" s="T664">Und es ist niemand.</ta>
            <ta e="T669" id="Seg_2724" s="T667">Dann sagt er:</ta>
            <ta e="T676" id="Seg_2725" s="T669">„Warum gehen die Pferde, aber man kann nichts sehen.“</ta>
            <ta e="T680" id="Seg_2726" s="T676">„Es ist mein Sohn, der pflügt.“</ta>
            <ta e="T684" id="Seg_2727" s="T680">Dann: „Verkaufe ihn mir!“</ta>
            <ta e="T690" id="Seg_2728" s="T684">Er verkaufte, er gab viel Geld.</ta>
            <ta e="T694" id="Seg_2729" s="T690">Dann steckte er ihn in die Tasche.</ta>
            <ta e="T698" id="Seg_2730" s="T694">Er zerriss die Tasche.</ta>
            <ta e="T700" id="Seg_2731" s="T698">Er ging weg.</ta>
            <ta e="T703" id="Seg_2732" s="T700">Dann kam er, er kam.</ta>
            <ta e="T705" id="Seg_2733" s="T703">Er schlief auf der Straße ein.</ta>
            <ta e="T711" id="Seg_2734" s="T705">Aber der Wolf kam, fraß ihn.</ta>
            <ta e="T716" id="Seg_2735" s="T711">Dann hütet ein Hirt Schafe.</ta>
            <ta e="T722" id="Seg_2736" s="T716">Soeben fängt der Wolf an, ein Schaf zu nehmen.</ta>
            <ta e="T727" id="Seg_2737" s="T722">Und der Junge ruft: „Steh auf!“</ta>
            <ta e="T729" id="Seg_2738" s="T727">Er frisst die Schafe!“</ta>
            <ta e="T736" id="Seg_2739" s="T729">Dann langer Zeit saß er, der Wolf wurde sehr hungrig.</ta>
            <ta e="T738" id="Seg_2740" s="T736">Dann sagt er:</ta>
            <ta e="T741" id="Seg_2741" s="T738">„Raus aus mir!“</ta>
            <ta e="T748" id="Seg_2742" s="T741">Aber er sagt: „Nimm [mich] zu meinem Vater und meine Mutter!“</ta>
            <ta e="T754" id="Seg_2743" s="T748">Er nahm ihn zu Vater und Mutter.</ta>
            <ta e="T757" id="Seg_2744" s="T754">Er warf ihn in sein Haus.</ta>
            <ta e="T760" id="Seg_2745" s="T757">Aber selbst lief er weg.</ta>
            <ta e="T761" id="Seg_2746" s="T760">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T584" id="Seg_2747" s="T578">[GVY:] Or "da embi"</ta>
            <ta e="T605" id="Seg_2748" s="T602">[KlT:] Unnecessary plural. [GVY:] Lat instead of Abl?</ta>
            <ta e="T616" id="Seg_2749" s="T609">[KlT:] Tajər ’plough’, possibly contamination; POSS.3SG instead of POSS.1SG. [GVY:] or tarerzittə</ta>
            <ta e="T676" id="Seg_2750" s="T669">[GVY:] The form kullial is not fully clear.</ta>
            <ta e="T711" id="Seg_2751" s="T705">[KlT:] Or Ru. -то or POSS.3SG?</ta>
            <ta e="T741" id="Seg_2752" s="T738">[KlT:] ABL would be expected.</ta>
            <ta e="T748" id="Seg_2753" s="T741">[GVY:] Maybe Kunnaʔ.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
