<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDCE11344F-F615-FFB1-9915-334308771D38">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_Children_flk.wav" />
         <referenced-file url="PKZ_196X_Children_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_Children_flk\PKZ_196X_Children_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">41</ud-information>
            <ud-information attribute-name="# HIAT:w">28</ud-information>
            <ud-information attribute-name="# e">28</ud-information>
            <ud-information attribute-name="# HIAT:u">5</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T42" time="0.0" type="appl" />
         <tli id="T43" time="0.68" type="appl" />
         <tli id="T44" time="1.36" type="appl" />
         <tli id="T45" time="2.039" type="appl" />
         <tli id="T46" time="2.719" type="appl" />
         <tli id="T47" time="3.573" type="appl" />
         <tli id="T48" time="4.428" type="appl" />
         <tli id="T49" time="5.282" type="appl" />
         <tli id="T50" time="6.269" type="appl" />
         <tli id="T51" time="7.256" type="appl" />
         <tli id="T52" time="8.242" type="appl" />
         <tli id="T53" time="9.122370144532525" />
         <tli id="T54" time="10.114" type="appl" />
         <tli id="T55" time="11.0" type="appl" />
         <tli id="T56" time="11.358" type="appl" />
         <tli id="T57" time="11.942558985857191" />
         <tli id="T58" time="12.58" type="appl" />
         <tli id="T59" time="13.029" type="appl" />
         <tli id="T60" time="13.478" type="appl" />
         <tli id="T61" time="13.926" type="appl" />
         <tli id="T62" time="14.375" type="appl" />
         <tli id="T63" time="14.824" type="appl" />
         <tli id="T64" time="15.273" type="appl" />
         <tli id="T65" time="15.722" type="appl" />
         <tli id="T66" time="16.442" type="appl" />
         <tli id="T67" time="17.161" type="appl" />
         <tli id="T68" time="17.881" type="appl" />
         <tli id="T69" time="18.6" type="appl" />
         <tli id="T70" time="19.32" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T70" id="Seg_0" n="sc" s="T42">
               <ts e="T49" id="Seg_2" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_4" n="HIAT:w" s="T42">Dĭ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_7" n="HIAT:w" s="T43">kuza</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_10" n="HIAT:w" s="T44">esseŋdə</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_13" n="HIAT:w" s="T45">măndə:</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_15" n="HIAT:ip">"</nts>
                  <nts id="Seg_16" n="HIAT:ip">(</nts>
                  <ts e="T47" id="Seg_18" n="HIAT:w" s="T46">Kaʔ-</ts>
                  <nts id="Seg_19" n="HIAT:ip">)</nts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_22" n="HIAT:w" s="T47">Kangaʔ</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_25" n="HIAT:w" s="T48">krospaʔinə</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_29" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_31" n="HIAT:w" s="T49">Dĭn</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_33" n="HIAT:ip">(</nts>
                  <ts e="T51" id="Seg_35" n="HIAT:w" s="T50">nʼamga-</ts>
                  <nts id="Seg_36" n="HIAT:ip">)</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_39" n="HIAT:w" s="T51">nʼamgaʔi</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_42" n="HIAT:w" s="T52">mĭleʔbəʔi</ts>
                  <nts id="Seg_43" n="HIAT:ip">"</nts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_47" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_49" n="HIAT:w" s="T53">Esseŋ</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_52" n="HIAT:w" s="T54">nuʔməluʔpiʔi</ts>
                  <nts id="Seg_53" n="HIAT:ip">.</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_56" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_58" n="HIAT:w" s="T55">Dĭ</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_61" n="HIAT:w" s="T56">măndə:</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_63" n="HIAT:ip">"</nts>
                  <ts e="T58" id="Seg_65" n="HIAT:w" s="T57">Ĭmbidə</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_68" n="HIAT:w" s="T58">ej</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_71" n="HIAT:w" s="T59">mĭbiʔi</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_74" n="HIAT:w" s="T60">dak</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_77" n="HIAT:w" s="T61">dĭzeŋ</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_80" n="HIAT:w" s="T62">bɨ</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_83" n="HIAT:w" s="T63">ej</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_86" n="HIAT:w" s="T64">nuʔməbiʔi</ts>
                  <nts id="Seg_87" n="HIAT:ip">"</nts>
                  <nts id="Seg_88" n="HIAT:ip">.</nts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_91" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_93" n="HIAT:w" s="T65">I</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_96" n="HIAT:w" s="T66">bostə</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_99" n="HIAT:w" s="T67">nuʔməlaːmbi</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_102" n="HIAT:w" s="T68">dĭzeŋziʔ</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_105" n="HIAT:w" s="T69">že</ts>
                  <nts id="Seg_106" n="HIAT:ip">.</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T70" id="Seg_108" n="sc" s="T42">
               <ts e="T43" id="Seg_110" n="e" s="T42">Dĭ </ts>
               <ts e="T44" id="Seg_112" n="e" s="T43">kuza </ts>
               <ts e="T45" id="Seg_114" n="e" s="T44">esseŋdə </ts>
               <ts e="T46" id="Seg_116" n="e" s="T45">măndə: </ts>
               <ts e="T47" id="Seg_118" n="e" s="T46">"(Kaʔ-) </ts>
               <ts e="T48" id="Seg_120" n="e" s="T47">Kangaʔ </ts>
               <ts e="T49" id="Seg_122" n="e" s="T48">krospaʔinə. </ts>
               <ts e="T50" id="Seg_124" n="e" s="T49">Dĭn </ts>
               <ts e="T51" id="Seg_126" n="e" s="T50">(nʼamga-) </ts>
               <ts e="T52" id="Seg_128" n="e" s="T51">nʼamgaʔi </ts>
               <ts e="T53" id="Seg_130" n="e" s="T52">mĭleʔbəʔi". </ts>
               <ts e="T54" id="Seg_132" n="e" s="T53">Esseŋ </ts>
               <ts e="T55" id="Seg_134" n="e" s="T54">nuʔməluʔpiʔi. </ts>
               <ts e="T56" id="Seg_136" n="e" s="T55">Dĭ </ts>
               <ts e="T57" id="Seg_138" n="e" s="T56">măndə: </ts>
               <ts e="T58" id="Seg_140" n="e" s="T57">"Ĭmbidə </ts>
               <ts e="T59" id="Seg_142" n="e" s="T58">ej </ts>
               <ts e="T60" id="Seg_144" n="e" s="T59">mĭbiʔi </ts>
               <ts e="T61" id="Seg_146" n="e" s="T60">dak </ts>
               <ts e="T62" id="Seg_148" n="e" s="T61">dĭzeŋ </ts>
               <ts e="T63" id="Seg_150" n="e" s="T62">bɨ </ts>
               <ts e="T64" id="Seg_152" n="e" s="T63">ej </ts>
               <ts e="T65" id="Seg_154" n="e" s="T64">nuʔməbiʔi". </ts>
               <ts e="T66" id="Seg_156" n="e" s="T65">I </ts>
               <ts e="T67" id="Seg_158" n="e" s="T66">bostə </ts>
               <ts e="T68" id="Seg_160" n="e" s="T67">nuʔməlaːmbi </ts>
               <ts e="T69" id="Seg_162" n="e" s="T68">dĭzeŋziʔ </ts>
               <ts e="T70" id="Seg_164" n="e" s="T69">že. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T49" id="Seg_165" s="T42">PKZ_196X_Children_flk.001 (001)</ta>
            <ta e="T53" id="Seg_166" s="T49">PKZ_196X_Children_flk.002 (003)</ta>
            <ta e="T55" id="Seg_167" s="T53">PKZ_196X_Children_flk.003 (004)</ta>
            <ta e="T65" id="Seg_168" s="T55">PKZ_196X_Children_flk.004 (005) </ta>
            <ta e="T70" id="Seg_169" s="T65">PKZ_196X_Children_flk.005 (007)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T49" id="Seg_170" s="T42">Dĭ kuza esseŋdə măndə: "(Kaʔ-) Kangaʔ krospaʔinə. </ta>
            <ta e="T53" id="Seg_171" s="T49">Dĭn (nʼamga-) nʼamgaʔi mĭleʔbəʔi". </ta>
            <ta e="T55" id="Seg_172" s="T53">Esseŋ nuʔməluʔpiʔi. </ta>
            <ta e="T65" id="Seg_173" s="T55">Dĭ măndə: "Ĭmbidə ej mĭbiʔi dak dĭzeŋ bɨ ej nuʔməbiʔi". </ta>
            <ta e="T70" id="Seg_174" s="T65">I bostə nuʔməlaːmbi dĭzeŋziʔ že. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T43" id="Seg_175" s="T42">dĭ</ta>
            <ta e="T44" id="Seg_176" s="T43">kuza</ta>
            <ta e="T45" id="Seg_177" s="T44">es-seŋ-də</ta>
            <ta e="T46" id="Seg_178" s="T45">măn-də</ta>
            <ta e="T48" id="Seg_179" s="T47">kan-gaʔ</ta>
            <ta e="T49" id="Seg_180" s="T48">krospa-ʔi-nə</ta>
            <ta e="T50" id="Seg_181" s="T49">dĭn</ta>
            <ta e="T52" id="Seg_182" s="T51">nʼamga-ʔi</ta>
            <ta e="T53" id="Seg_183" s="T52">mĭ-leʔbə-ʔi</ta>
            <ta e="T54" id="Seg_184" s="T53">es-seŋ</ta>
            <ta e="T55" id="Seg_185" s="T54">nuʔmə-luʔ-pi-ʔi</ta>
            <ta e="T56" id="Seg_186" s="T55">dĭ</ta>
            <ta e="T57" id="Seg_187" s="T56">măn-də</ta>
            <ta e="T58" id="Seg_188" s="T57">ĭmbi=də</ta>
            <ta e="T59" id="Seg_189" s="T58">ej</ta>
            <ta e="T60" id="Seg_190" s="T59">mĭ-bi-ʔi</ta>
            <ta e="T61" id="Seg_191" s="T60">dak</ta>
            <ta e="T62" id="Seg_192" s="T61">dĭ-zeŋ</ta>
            <ta e="T63" id="Seg_193" s="T62">bɨ</ta>
            <ta e="T64" id="Seg_194" s="T63">ej</ta>
            <ta e="T65" id="Seg_195" s="T64">nuʔmə-bi-ʔi</ta>
            <ta e="T66" id="Seg_196" s="T65">i</ta>
            <ta e="T67" id="Seg_197" s="T66">bos-tə</ta>
            <ta e="T68" id="Seg_198" s="T67">nuʔmə-laːm-bi</ta>
            <ta e="T69" id="Seg_199" s="T68">dĭ-zeŋ-ziʔ</ta>
            <ta e="T70" id="Seg_200" s="T69">že</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T43" id="Seg_201" s="T42">dĭ</ta>
            <ta e="T44" id="Seg_202" s="T43">kuza</ta>
            <ta e="T45" id="Seg_203" s="T44">ešši-zAŋ-Tə</ta>
            <ta e="T46" id="Seg_204" s="T45">măn-ntə</ta>
            <ta e="T48" id="Seg_205" s="T47">kan-KAʔ</ta>
            <ta e="T49" id="Seg_206" s="T48">krospa-jəʔ-Tə</ta>
            <ta e="T50" id="Seg_207" s="T49">dĭn</ta>
            <ta e="T52" id="Seg_208" s="T51">nʼamga-jəʔ</ta>
            <ta e="T53" id="Seg_209" s="T52">mĭ-laʔbə-jəʔ</ta>
            <ta e="T54" id="Seg_210" s="T53">ešši-zAŋ</ta>
            <ta e="T55" id="Seg_211" s="T54">nuʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T56" id="Seg_212" s="T55">dĭ</ta>
            <ta e="T57" id="Seg_213" s="T56">măn-ntə</ta>
            <ta e="T58" id="Seg_214" s="T57">ĭmbi=də</ta>
            <ta e="T59" id="Seg_215" s="T58">ej</ta>
            <ta e="T60" id="Seg_216" s="T59">mĭ-bi-jəʔ</ta>
            <ta e="T61" id="Seg_217" s="T60">tak</ta>
            <ta e="T62" id="Seg_218" s="T61">dĭ-zAŋ</ta>
            <ta e="T63" id="Seg_219" s="T62">bɨ</ta>
            <ta e="T64" id="Seg_220" s="T63">ej</ta>
            <ta e="T65" id="Seg_221" s="T64">nuʔmə-bi-jəʔ</ta>
            <ta e="T66" id="Seg_222" s="T65">i</ta>
            <ta e="T67" id="Seg_223" s="T66">bos-də</ta>
            <ta e="T68" id="Seg_224" s="T67">nuʔmə-laːm-bi</ta>
            <ta e="T69" id="Seg_225" s="T68">dĭ-zAŋ-ziʔ</ta>
            <ta e="T70" id="Seg_226" s="T69">že</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T43" id="Seg_227" s="T42">this.[NOM.SG]</ta>
            <ta e="T44" id="Seg_228" s="T43">man.[NOM.SG]</ta>
            <ta e="T45" id="Seg_229" s="T44">child-PL-LAT</ta>
            <ta e="T46" id="Seg_230" s="T45">say-IPFVZ.[3SG]</ta>
            <ta e="T48" id="Seg_231" s="T47">go-IMP.2PL</ta>
            <ta e="T49" id="Seg_232" s="T48">cross-PL-LAT</ta>
            <ta e="T50" id="Seg_233" s="T49">there</ta>
            <ta e="T52" id="Seg_234" s="T51">sweet-PL</ta>
            <ta e="T53" id="Seg_235" s="T52">give-DUR-3PL</ta>
            <ta e="T54" id="Seg_236" s="T53">child-PL</ta>
            <ta e="T55" id="Seg_237" s="T54">run-MOM-PST-3PL</ta>
            <ta e="T56" id="Seg_238" s="T55">this.[NOM.SG]</ta>
            <ta e="T57" id="Seg_239" s="T56">say-IPFVZ.[3SG]</ta>
            <ta e="T58" id="Seg_240" s="T57">what.[NOM.SG]=INDEF</ta>
            <ta e="T59" id="Seg_241" s="T58">NEG</ta>
            <ta e="T60" id="Seg_242" s="T59">give-PST-3PL</ta>
            <ta e="T61" id="Seg_243" s="T60">so</ta>
            <ta e="T62" id="Seg_244" s="T61">this-PL</ta>
            <ta e="T63" id="Seg_245" s="T62">IRREAL</ta>
            <ta e="T64" id="Seg_246" s="T63">NEG</ta>
            <ta e="T65" id="Seg_247" s="T64">run-PST-3PL</ta>
            <ta e="T66" id="Seg_248" s="T65">and</ta>
            <ta e="T67" id="Seg_249" s="T66">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T68" id="Seg_250" s="T67">run-RES-PST.[3SG]</ta>
            <ta e="T69" id="Seg_251" s="T68">this-PL-INS</ta>
            <ta e="T70" id="Seg_252" s="T69">PTCL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T43" id="Seg_253" s="T42">этот.[NOM.SG]</ta>
            <ta e="T44" id="Seg_254" s="T43">мужчина.[NOM.SG]</ta>
            <ta e="T45" id="Seg_255" s="T44">ребенок-PL-LAT</ta>
            <ta e="T46" id="Seg_256" s="T45">сказать-IPFVZ.[3SG]</ta>
            <ta e="T48" id="Seg_257" s="T47">пойти-IMP.2PL</ta>
            <ta e="T49" id="Seg_258" s="T48">крест-PL-LAT</ta>
            <ta e="T50" id="Seg_259" s="T49">там</ta>
            <ta e="T52" id="Seg_260" s="T51">сладкий-PL</ta>
            <ta e="T53" id="Seg_261" s="T52">дать-DUR-3PL</ta>
            <ta e="T54" id="Seg_262" s="T53">ребенок-PL</ta>
            <ta e="T55" id="Seg_263" s="T54">бежать-MOM-PST-3PL</ta>
            <ta e="T56" id="Seg_264" s="T55">этот.[NOM.SG]</ta>
            <ta e="T57" id="Seg_265" s="T56">сказать-IPFVZ.[3SG]</ta>
            <ta e="T58" id="Seg_266" s="T57">что.[NOM.SG]=INDEF</ta>
            <ta e="T59" id="Seg_267" s="T58">NEG</ta>
            <ta e="T60" id="Seg_268" s="T59">дать-PST-3PL</ta>
            <ta e="T61" id="Seg_269" s="T60">так</ta>
            <ta e="T62" id="Seg_270" s="T61">этот-PL</ta>
            <ta e="T63" id="Seg_271" s="T62">IRREAL</ta>
            <ta e="T64" id="Seg_272" s="T63">NEG</ta>
            <ta e="T65" id="Seg_273" s="T64">бежать-PST-3PL</ta>
            <ta e="T66" id="Seg_274" s="T65">и</ta>
            <ta e="T67" id="Seg_275" s="T66">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T68" id="Seg_276" s="T67">бежать-RES-PST.[3SG]</ta>
            <ta e="T69" id="Seg_277" s="T68">этот-PL-INS</ta>
            <ta e="T70" id="Seg_278" s="T69">же</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T43" id="Seg_279" s="T42">dempro-n:case</ta>
            <ta e="T44" id="Seg_280" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_281" s="T44">n-n:num-n:case</ta>
            <ta e="T46" id="Seg_282" s="T45">v-v&gt;v-v:pn</ta>
            <ta e="T48" id="Seg_283" s="T47">v-v:mood.pn</ta>
            <ta e="T49" id="Seg_284" s="T48">n-n:num-n:case</ta>
            <ta e="T50" id="Seg_285" s="T49">adv</ta>
            <ta e="T52" id="Seg_286" s="T51">adj-n:num</ta>
            <ta e="T53" id="Seg_287" s="T52">v-v&gt;v-v:pn</ta>
            <ta e="T54" id="Seg_288" s="T53">n-n:num</ta>
            <ta e="T55" id="Seg_289" s="T54">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T56" id="Seg_290" s="T55">dempro-n:case</ta>
            <ta e="T57" id="Seg_291" s="T56">v-v&gt;v-v:pn</ta>
            <ta e="T58" id="Seg_292" s="T57">que-n:case=ptcl</ta>
            <ta e="T59" id="Seg_293" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_294" s="T59">v-v:tense-v:pn</ta>
            <ta e="T61" id="Seg_295" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_296" s="T61">dempro-n:num</ta>
            <ta e="T63" id="Seg_297" s="T62">ptcl</ta>
            <ta e="T64" id="Seg_298" s="T63">ptcl</ta>
            <ta e="T65" id="Seg_299" s="T64">v-v:tense-v:pn</ta>
            <ta e="T66" id="Seg_300" s="T65">conj</ta>
            <ta e="T67" id="Seg_301" s="T66">refl-n:case.poss</ta>
            <ta e="T68" id="Seg_302" s="T67">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T69" id="Seg_303" s="T68">dempro-n:num-n:case</ta>
            <ta e="T70" id="Seg_304" s="T69">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T43" id="Seg_305" s="T42">dempro</ta>
            <ta e="T44" id="Seg_306" s="T43">n</ta>
            <ta e="T45" id="Seg_307" s="T44">n</ta>
            <ta e="T46" id="Seg_308" s="T45">v</ta>
            <ta e="T48" id="Seg_309" s="T47">v</ta>
            <ta e="T49" id="Seg_310" s="T48">n</ta>
            <ta e="T50" id="Seg_311" s="T49">adv</ta>
            <ta e="T52" id="Seg_312" s="T51">adj</ta>
            <ta e="T53" id="Seg_313" s="T52">v</ta>
            <ta e="T54" id="Seg_314" s="T53">n</ta>
            <ta e="T55" id="Seg_315" s="T54">v</ta>
            <ta e="T56" id="Seg_316" s="T55">dempro</ta>
            <ta e="T57" id="Seg_317" s="T56">v</ta>
            <ta e="T58" id="Seg_318" s="T57">que</ta>
            <ta e="T59" id="Seg_319" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_320" s="T59">v</ta>
            <ta e="T61" id="Seg_321" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_322" s="T61">dempro</ta>
            <ta e="T63" id="Seg_323" s="T62">ptcl</ta>
            <ta e="T64" id="Seg_324" s="T63">ptcl</ta>
            <ta e="T65" id="Seg_325" s="T64">v</ta>
            <ta e="T66" id="Seg_326" s="T65">conj</ta>
            <ta e="T67" id="Seg_327" s="T66">refl</ta>
            <ta e="T68" id="Seg_328" s="T67">v</ta>
            <ta e="T69" id="Seg_329" s="T68">dempro</ta>
            <ta e="T70" id="Seg_330" s="T69">ptcl</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T44" id="Seg_331" s="T43">np.h:S</ta>
            <ta e="T46" id="Seg_332" s="T45">v:pred</ta>
            <ta e="T48" id="Seg_333" s="T47">0.2.h:S v:pred</ta>
            <ta e="T52" id="Seg_334" s="T51">np:O</ta>
            <ta e="T53" id="Seg_335" s="T52">0.3.h:S v:pred</ta>
            <ta e="T54" id="Seg_336" s="T53">np.h:S</ta>
            <ta e="T55" id="Seg_337" s="T54">v:pred</ta>
            <ta e="T56" id="Seg_338" s="T55">pro.h:S</ta>
            <ta e="T57" id="Seg_339" s="T56">v:pred</ta>
            <ta e="T60" id="Seg_340" s="T57">s:cond</ta>
            <ta e="T62" id="Seg_341" s="T61">pro.h:S</ta>
            <ta e="T65" id="Seg_342" s="T64">v:pred</ta>
            <ta e="T67" id="Seg_343" s="T66">pro.h:S</ta>
            <ta e="T68" id="Seg_344" s="T67">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T44" id="Seg_345" s="T43">np.h:A</ta>
            <ta e="T45" id="Seg_346" s="T44">np.h:R</ta>
            <ta e="T48" id="Seg_347" s="T47">0.2.h:A</ta>
            <ta e="T49" id="Seg_348" s="T48">np.h:R</ta>
            <ta e="T52" id="Seg_349" s="T51">np:Th</ta>
            <ta e="T53" id="Seg_350" s="T52">0.3.h:A</ta>
            <ta e="T54" id="Seg_351" s="T53">np.h:A</ta>
            <ta e="T56" id="Seg_352" s="T55">pro.h:A</ta>
            <ta e="T60" id="Seg_353" s="T59">0.3.h:A</ta>
            <ta e="T62" id="Seg_354" s="T61">pro.h:A</ta>
            <ta e="T67" id="Seg_355" s="T66">pro.h:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T58" id="Seg_356" s="T57">TURK:gram(INDEF)</ta>
            <ta e="T61" id="Seg_357" s="T60">RUS:gram</ta>
            <ta e="T63" id="Seg_358" s="T62">RUS:gram</ta>
            <ta e="T66" id="Seg_359" s="T65">RUS:gram</ta>
            <ta e="T70" id="Seg_360" s="T69">RUS:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T46" id="Seg_361" s="T42">Этот человек говорит детям:</ta>
            <ta e="T49" id="Seg_362" s="T46">«Идите на кладбище!</ta>
            <ta e="T53" id="Seg_363" s="T49">Там сладости дают».</ta>
            <ta e="T55" id="Seg_364" s="T53">Дети убежали.</ta>
            <ta e="T57" id="Seg_365" s="T55">Он говорит:</ta>
            <ta e="T65" id="Seg_366" s="T57">«[Если бы] ничего не давали, так они бы не побежали».</ta>
            <ta e="T70" id="Seg_367" s="T65">И сам с ними же и побежал.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T46" id="Seg_368" s="T42">This man says to the children:</ta>
            <ta e="T49" id="Seg_369" s="T46">"Go to the cemetery!</ta>
            <ta e="T53" id="Seg_370" s="T49">They are giving sweets there."</ta>
            <ta e="T55" id="Seg_371" s="T53">The children ran away.</ta>
            <ta e="T57" id="Seg_372" s="T55">He says:</ta>
            <ta e="T65" id="Seg_373" s="T57">"If they gave nothing, then they would not have run."</ta>
            <ta e="T70" id="Seg_374" s="T65">And he himself ran with them.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T46" id="Seg_375" s="T42">Dieser Mann sagt den Kindern:</ta>
            <ta e="T49" id="Seg_376" s="T46">„Geht auf den Friedhof!</ta>
            <ta e="T53" id="Seg_377" s="T49">Sie schenken dort Süßigkeiten.“</ta>
            <ta e="T55" id="Seg_378" s="T53">Die Kinder rannten weg.</ta>
            <ta e="T57" id="Seg_379" s="T55">Er sagt:</ta>
            <ta e="T65" id="Seg_380" s="T57">“Wenn sie nichts geschenkt hätten, hätten sie nicht gerannt.“</ta>
            <ta e="T70" id="Seg_381" s="T65">Und er selbst rannte mit ihnen.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
