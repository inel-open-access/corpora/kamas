<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID5188C61B-4297-E582-E2AD-8736E31689EA">
   <head>
      <meta-information>
         <project-name>Kamas</project-name>
         <transcription-name>AA_1914_Mouse_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\AA_1914_Mouse_flk\AA_1914_Mouse_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">175</ud-information>
            <ud-information attribute-name="# HIAT:w">110</ud-information>
            <ud-information attribute-name="# e">110</ud-information>
            <ud-information attribute-name="# HIAT:u">27</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="AA">
            <abbreviation>AA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="AA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T110" id="Seg_0" n="sc" s="T0">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Tumo</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">da</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">kös</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">suzəj</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">saməjlaʔ</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">kambijəʔ</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_23" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">Tʼăga</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">toʔbdobi</ts>
                  <nts id="Seg_29" n="HIAT:ip">,</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">bejde</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">moliaiʔ</ts>
                  <nts id="Seg_36" n="HIAT:ip">.</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_39" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">Šalguj</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">băldəbiiʔ</ts>
                  <nts id="Seg_45" n="HIAT:ip">,</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_48" n="HIAT:w" s="T12">köbrügəmbi</ts>
                  <nts id="Seg_49" n="HIAT:ip">.</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_52" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_54" n="HIAT:w" s="T13">Kös</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_57" n="HIAT:w" s="T14">bejlaːndəbi</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_60" n="HIAT:w" s="T15">šalguj</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_63" n="HIAT:w" s="T16">amoluʔbdəbi</ts>
                  <nts id="Seg_64" n="HIAT:ip">.</nts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_67" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_69" n="HIAT:w" s="T17">Kös</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_72" n="HIAT:w" s="T18">bünə</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_75" n="HIAT:w" s="T19">üzəbi</ts>
                  <nts id="Seg_76" n="HIAT:ip">,</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_79" n="HIAT:w" s="T20">tʼărlərluʔbi</ts>
                  <nts id="Seg_80" n="HIAT:ip">.</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_83" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_85" n="HIAT:w" s="T21">Suzəj</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_88" n="HIAT:w" s="T22">kakənarbi</ts>
                  <nts id="Seg_89" n="HIAT:ip">,</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_92" n="HIAT:w" s="T23">kakənarbi</ts>
                  <nts id="Seg_93" n="HIAT:ip">,</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_96" n="HIAT:w" s="T24">păktəlaʔ</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_99" n="HIAT:w" s="T25">saʔməbi</ts>
                  <nts id="Seg_100" n="HIAT:ip">.</nts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_103" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_105" n="HIAT:w" s="T26">Tumo</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_108" n="HIAT:w" s="T27">unʼə</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_111" n="HIAT:w" s="T28">maʔbi</ts>
                  <nts id="Seg_112" n="HIAT:ip">.</nts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_115" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_117" n="HIAT:w" s="T29">Tʼăgan</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_120" n="HIAT:w" s="T30">kunzə</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_123" n="HIAT:w" s="T31">mĭnǝllaːndəbi</ts>
                  <nts id="Seg_124" n="HIAT:ip">.</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_127" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_129" n="HIAT:w" s="T32">Šamnak</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_132" n="HIAT:w" s="T33">bălda</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_135" n="HIAT:w" s="T34">toʔbdobi</ts>
                  <nts id="Seg_136" n="HIAT:ip">.</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_139" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_141" n="HIAT:w" s="T35">Ibi</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_144" n="HIAT:w" s="T36">dĭ</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_147" n="HIAT:w" s="T37">šamnaːm</ts>
                  <nts id="Seg_148" n="HIAT:ip">,</nts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_151" n="HIAT:w" s="T38">amnəbi</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_154" n="HIAT:w" s="T39">šamnaktə</ts>
                  <nts id="Seg_155" n="HIAT:ip">,</nts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_158" n="HIAT:w" s="T40">penʼəjloʔbdəbi</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_161" n="HIAT:w" s="T41">tʼăgan</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_164" n="HIAT:w" s="T42">tăžə</ts>
                  <nts id="Seg_165" n="HIAT:ip">.</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_168" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_170" n="HIAT:w" s="T43">Kubində</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_173" n="HIAT:w" s="T44">maʔ</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_176" n="HIAT:w" s="T45">nuga</ts>
                  <nts id="Seg_177" n="HIAT:ip">,</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_180" n="HIAT:w" s="T46">esseŋ</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_183" n="HIAT:w" s="T47">sʼarlaʔbəjəʔ</ts>
                  <nts id="Seg_184" n="HIAT:ip">.</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_187" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_189" n="HIAT:w" s="T48">Kegərerbi</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_192" n="HIAT:w" s="T49">tumo:</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_194" n="HIAT:ip">"</nts>
                  <ts e="T51" id="Seg_196" n="HIAT:w" s="T50">Esseŋ</ts>
                  <nts id="Seg_197" n="HIAT:ip">,</nts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_200" n="HIAT:w" s="T51">urgaːbalaʔ</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_203" n="HIAT:w" s="T52">šonəga</ts>
                  <nts id="Seg_204" n="HIAT:ip">.</nts>
                  <nts id="Seg_205" n="HIAT:ip">"</nts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_208" n="HIAT:u" s="T53">
                  <nts id="Seg_209" n="HIAT:ip">"</nts>
                  <ts e="T54" id="Seg_211" n="HIAT:w" s="T53">Uja</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_214" n="HIAT:w" s="T54">paʔkaʔ</ts>
                  <nts id="Seg_215" n="HIAT:ip">!</nts>
                  <nts id="Seg_216" n="HIAT:ip">"</nts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_219" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_221" n="HIAT:w" s="T55">D</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_224" n="HIAT:w" s="T56">esseŋ</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_227" n="HIAT:w" s="T57">üʔməbiiʔ</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_230" n="HIAT:w" s="T58">ijagəndən</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_233" n="HIAT:w" s="T59">nörbəbi:</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_235" n="HIAT:ip">"</nts>
                  <ts e="T61" id="Seg_237" n="HIAT:w" s="T60">Urgaːbabaʔ</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_240" n="HIAT:w" s="T61">šonənamna</ts>
                  <nts id="Seg_241" n="HIAT:ip">.</nts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_244" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_246" n="HIAT:w" s="T62">Uja</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_249" n="HIAT:w" s="T63">padaʔ</ts>
                  <nts id="Seg_250" n="HIAT:ip">,</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_253" n="HIAT:w" s="T64">ija</ts>
                  <nts id="Seg_254" n="HIAT:ip">!</nts>
                  <nts id="Seg_255" n="HIAT:ip">"</nts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_258" n="HIAT:u" s="T65">
                  <nts id="Seg_259" n="HIAT:ip">"</nts>
                  <ts e="T66" id="Seg_261" n="HIAT:w" s="T65">Kallaʔ</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_264" n="HIAT:w" s="T66">kăštəgaʔ</ts>
                  <nts id="Seg_265" n="HIAT:ip">,</nts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_268" n="HIAT:w" s="T67">kola</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_271" n="HIAT:w" s="T68">pülaːmbi</ts>
                  <nts id="Seg_272" n="HIAT:ip">.</nts>
                  <nts id="Seg_273" n="HIAT:ip">"</nts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_276" n="HIAT:u" s="T69">
                  <nts id="Seg_277" n="HIAT:ip">"</nts>
                  <ts e="T70" id="Seg_279" n="HIAT:w" s="T69">Măn</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_282" n="HIAT:w" s="T70">kola</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_285" n="HIAT:w" s="T71">ej</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_288" n="HIAT:w" s="T72">amənzəliem</ts>
                  <nts id="Seg_289" n="HIAT:ip">.</nts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_292" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_294" n="HIAT:w" s="T73">Let</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_297" n="HIAT:w" s="T74">iʔgö</ts>
                  <nts id="Seg_298" n="HIAT:ip">,</nts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_301" n="HIAT:w" s="T75">kiaʔdolam</ts>
                  <nts id="Seg_302" n="HIAT:ip">.</nts>
                  <nts id="Seg_303" n="HIAT:ip">"</nts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_306" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_308" n="HIAT:w" s="T76">Baltubə</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_311" n="HIAT:w" s="T77">kabarbi</ts>
                  <nts id="Seg_312" n="HIAT:ip">,</nts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_315" n="HIAT:w" s="T78">tüžöjdə</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_318" n="HIAT:w" s="T79">kuppi</ts>
                  <nts id="Seg_319" n="HIAT:ip">,</nts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_322" n="HIAT:w" s="T80">uja</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_325" n="HIAT:w" s="T81">paʔbi</ts>
                  <nts id="Seg_326" n="HIAT:ip">.</nts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_329" n="HIAT:u" s="T82">
                  <nts id="Seg_330" n="HIAT:ip">"</nts>
                  <ts e="T83" id="Seg_332" n="HIAT:w" s="T82">Kăštəgut</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_335" n="HIAT:w" s="T83">urgaːbalaʔ</ts>
                  <nts id="Seg_336" n="HIAT:ip">.</nts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_339" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_341" n="HIAT:w" s="T84">Kăštəgut</ts>
                  <nts id="Seg_342" n="HIAT:ip">,</nts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_345" n="HIAT:w" s="T85">uja</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_348" n="HIAT:w" s="T86">pülaːmbi</ts>
                  <nts id="Seg_349" n="HIAT:ip">.</nts>
                  <nts id="Seg_350" n="HIAT:ip">"</nts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_353" n="HIAT:u" s="T87">
                  <nts id="Seg_354" n="HIAT:ip">"</nts>
                  <ts e="T88" id="Seg_356" n="HIAT:w" s="T87">Urgaːbaʔ</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_359" n="HIAT:w" s="T88">šoʔ</ts>
                  <nts id="Seg_360" n="HIAT:ip">,</nts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_363" n="HIAT:w" s="T89">uja</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_366" n="HIAT:w" s="T90">pülaːmbi</ts>
                  <nts id="Seg_367" n="HIAT:ip">.</nts>
                  <nts id="Seg_368" n="HIAT:ip">"</nts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_371" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_373" n="HIAT:w" s="T91">Sagər</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_376" n="HIAT:w" s="T92">konun</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_379" n="HIAT:w" s="T93">kubabə</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_382" n="HIAT:w" s="T94">šuktəlgut</ts>
                  <nts id="Seg_383" n="HIAT:ip">!</nts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_386" n="HIAT:u" s="T95">
                  <nts id="Seg_387" n="HIAT:ip">"</nts>
                  <ts e="T96" id="Seg_389" n="HIAT:w" s="T95">Šuktəlbibaʔ</ts>
                  <nts id="Seg_390" n="HIAT:ip">,</nts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_393" n="HIAT:w" s="T96">šoʔ</ts>
                  <nts id="Seg_394" n="HIAT:ip">!</nts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_397" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_399" n="HIAT:w" s="T97">Kallaʔ</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_402" n="HIAT:w" s="T98">kăštəgut</ts>
                  <nts id="Seg_403" n="HIAT:ip">,</nts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_406" n="HIAT:w" s="T99">šuktəloma</ts>
                  <nts id="Seg_407" n="HIAT:ip">.</nts>
                  <nts id="Seg_408" n="HIAT:ip">"</nts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_411" n="HIAT:u" s="T100">
                  <nts id="Seg_412" n="HIAT:ip">"</nts>
                  <ts e="T101" id="Seg_414" n="HIAT:w" s="T100">Măn</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_417" n="HIAT:w" s="T101">dĭgən</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_420" n="HIAT:w" s="T102">igem</ts>
                  <nts id="Seg_421" n="HIAT:ip">.</nts>
                  <nts id="Seg_422" n="HIAT:ip">"</nts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_425" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_427" n="HIAT:w" s="T103">Ne</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_430" n="HIAT:w" s="T104">kubi</ts>
                  <nts id="Seg_431" n="HIAT:ip">,</nts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_434" n="HIAT:w" s="T105">pa</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_437" n="HIAT:w" s="T106">kabarbi</ts>
                  <nts id="Seg_438" n="HIAT:ip">,</nts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_441" n="HIAT:w" s="T107">dĭ</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_444" n="HIAT:w" s="T108">tʼuga</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_447" n="HIAT:w" s="T109">toʔluʔbi</ts>
                  <nts id="Seg_448" n="HIAT:ip">.</nts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T110" id="Seg_450" n="sc" s="T0">
               <ts e="T1" id="Seg_452" n="e" s="T0">Tumo </ts>
               <ts e="T2" id="Seg_454" n="e" s="T1">da </ts>
               <ts e="T3" id="Seg_456" n="e" s="T2">kös </ts>
               <ts e="T4" id="Seg_458" n="e" s="T3">suzəj </ts>
               <ts e="T5" id="Seg_460" n="e" s="T4">saməjlaʔ </ts>
               <ts e="T6" id="Seg_462" n="e" s="T5">kambijəʔ. </ts>
               <ts e="T7" id="Seg_464" n="e" s="T6">Tʼăga </ts>
               <ts e="T8" id="Seg_466" n="e" s="T7">toʔbdobi, </ts>
               <ts e="T9" id="Seg_468" n="e" s="T8">bejde </ts>
               <ts e="T10" id="Seg_470" n="e" s="T9">moliaiʔ. </ts>
               <ts e="T11" id="Seg_472" n="e" s="T10">Šalguj </ts>
               <ts e="T12" id="Seg_474" n="e" s="T11">băldəbiiʔ, </ts>
               <ts e="T13" id="Seg_476" n="e" s="T12">köbrügəmbi. </ts>
               <ts e="T14" id="Seg_478" n="e" s="T13">Kös </ts>
               <ts e="T15" id="Seg_480" n="e" s="T14">bejlaːndəbi </ts>
               <ts e="T16" id="Seg_482" n="e" s="T15">šalguj </ts>
               <ts e="T17" id="Seg_484" n="e" s="T16">amoluʔbdəbi. </ts>
               <ts e="T18" id="Seg_486" n="e" s="T17">Kös </ts>
               <ts e="T19" id="Seg_488" n="e" s="T18">bünə </ts>
               <ts e="T20" id="Seg_490" n="e" s="T19">üzəbi, </ts>
               <ts e="T21" id="Seg_492" n="e" s="T20">tʼărlərluʔbi. </ts>
               <ts e="T22" id="Seg_494" n="e" s="T21">Suzəj </ts>
               <ts e="T23" id="Seg_496" n="e" s="T22">kakənarbi, </ts>
               <ts e="T24" id="Seg_498" n="e" s="T23">kakənarbi, </ts>
               <ts e="T25" id="Seg_500" n="e" s="T24">păktəlaʔ </ts>
               <ts e="T26" id="Seg_502" n="e" s="T25">saʔməbi. </ts>
               <ts e="T27" id="Seg_504" n="e" s="T26">Tumo </ts>
               <ts e="T28" id="Seg_506" n="e" s="T27">unʼə </ts>
               <ts e="T29" id="Seg_508" n="e" s="T28">maʔbi. </ts>
               <ts e="T30" id="Seg_510" n="e" s="T29">Tʼăgan </ts>
               <ts e="T31" id="Seg_512" n="e" s="T30">kunzə </ts>
               <ts e="T32" id="Seg_514" n="e" s="T31">mĭnǝllaːndəbi. </ts>
               <ts e="T33" id="Seg_516" n="e" s="T32">Šamnak </ts>
               <ts e="T34" id="Seg_518" n="e" s="T33">bălda </ts>
               <ts e="T35" id="Seg_520" n="e" s="T34">toʔbdobi. </ts>
               <ts e="T36" id="Seg_522" n="e" s="T35">Ibi </ts>
               <ts e="T37" id="Seg_524" n="e" s="T36">dĭ </ts>
               <ts e="T38" id="Seg_526" n="e" s="T37">šamnaːm, </ts>
               <ts e="T39" id="Seg_528" n="e" s="T38">amnəbi </ts>
               <ts e="T40" id="Seg_530" n="e" s="T39">šamnaktə, </ts>
               <ts e="T41" id="Seg_532" n="e" s="T40">penʼəjloʔbdəbi </ts>
               <ts e="T42" id="Seg_534" n="e" s="T41">tʼăgan </ts>
               <ts e="T43" id="Seg_536" n="e" s="T42">tăžə. </ts>
               <ts e="T44" id="Seg_538" n="e" s="T43">Kubində </ts>
               <ts e="T45" id="Seg_540" n="e" s="T44">maʔ </ts>
               <ts e="T46" id="Seg_542" n="e" s="T45">nuga, </ts>
               <ts e="T47" id="Seg_544" n="e" s="T46">esseŋ </ts>
               <ts e="T48" id="Seg_546" n="e" s="T47">sʼarlaʔbəjəʔ. </ts>
               <ts e="T49" id="Seg_548" n="e" s="T48">Kegərerbi </ts>
               <ts e="T50" id="Seg_550" n="e" s="T49">tumo: </ts>
               <ts e="T51" id="Seg_552" n="e" s="T50">"Esseŋ, </ts>
               <ts e="T52" id="Seg_554" n="e" s="T51">urgaːbalaʔ </ts>
               <ts e="T53" id="Seg_556" n="e" s="T52">šonəga." </ts>
               <ts e="T54" id="Seg_558" n="e" s="T53">"Uja </ts>
               <ts e="T55" id="Seg_560" n="e" s="T54">paʔkaʔ!" </ts>
               <ts e="T56" id="Seg_562" n="e" s="T55">D </ts>
               <ts e="T57" id="Seg_564" n="e" s="T56">esseŋ </ts>
               <ts e="T58" id="Seg_566" n="e" s="T57">üʔməbiiʔ </ts>
               <ts e="T59" id="Seg_568" n="e" s="T58">ijagəndən </ts>
               <ts e="T60" id="Seg_570" n="e" s="T59">nörbəbi: </ts>
               <ts e="T61" id="Seg_572" n="e" s="T60">"Urgaːbabaʔ </ts>
               <ts e="T62" id="Seg_574" n="e" s="T61">šonənamna. </ts>
               <ts e="T63" id="Seg_576" n="e" s="T62">Uja </ts>
               <ts e="T64" id="Seg_578" n="e" s="T63">padaʔ, </ts>
               <ts e="T65" id="Seg_580" n="e" s="T64">ija!" </ts>
               <ts e="T66" id="Seg_582" n="e" s="T65">"Kallaʔ </ts>
               <ts e="T67" id="Seg_584" n="e" s="T66">kăštəgaʔ, </ts>
               <ts e="T68" id="Seg_586" n="e" s="T67">kola </ts>
               <ts e="T69" id="Seg_588" n="e" s="T68">pülaːmbi." </ts>
               <ts e="T70" id="Seg_590" n="e" s="T69">"Măn </ts>
               <ts e="T71" id="Seg_592" n="e" s="T70">kola </ts>
               <ts e="T72" id="Seg_594" n="e" s="T71">ej </ts>
               <ts e="T73" id="Seg_596" n="e" s="T72">amənzəliem. </ts>
               <ts e="T74" id="Seg_598" n="e" s="T73">Let </ts>
               <ts e="T75" id="Seg_600" n="e" s="T74">iʔgö, </ts>
               <ts e="T76" id="Seg_602" n="e" s="T75">kiaʔdolam." </ts>
               <ts e="T77" id="Seg_604" n="e" s="T76">Baltubə </ts>
               <ts e="T78" id="Seg_606" n="e" s="T77">kabarbi, </ts>
               <ts e="T79" id="Seg_608" n="e" s="T78">tüžöjdə </ts>
               <ts e="T80" id="Seg_610" n="e" s="T79">kuppi, </ts>
               <ts e="T81" id="Seg_612" n="e" s="T80">uja </ts>
               <ts e="T82" id="Seg_614" n="e" s="T81">paʔbi. </ts>
               <ts e="T83" id="Seg_616" n="e" s="T82">"Kăštəgut </ts>
               <ts e="T84" id="Seg_618" n="e" s="T83">urgaːbalaʔ. </ts>
               <ts e="T85" id="Seg_620" n="e" s="T84">Kăštəgut, </ts>
               <ts e="T86" id="Seg_622" n="e" s="T85">uja </ts>
               <ts e="T87" id="Seg_624" n="e" s="T86">pülaːmbi." </ts>
               <ts e="T88" id="Seg_626" n="e" s="T87">"Urgaːbaʔ </ts>
               <ts e="T89" id="Seg_628" n="e" s="T88">šoʔ, </ts>
               <ts e="T90" id="Seg_630" n="e" s="T89">uja </ts>
               <ts e="T91" id="Seg_632" n="e" s="T90">pülaːmbi." </ts>
               <ts e="T92" id="Seg_634" n="e" s="T91">Sagər </ts>
               <ts e="T93" id="Seg_636" n="e" s="T92">konun </ts>
               <ts e="T94" id="Seg_638" n="e" s="T93">kubabə </ts>
               <ts e="T95" id="Seg_640" n="e" s="T94">šuktəlgut! </ts>
               <ts e="T96" id="Seg_642" n="e" s="T95">"Šuktəlbibaʔ, </ts>
               <ts e="T97" id="Seg_644" n="e" s="T96">šoʔ! </ts>
               <ts e="T98" id="Seg_646" n="e" s="T97">Kallaʔ </ts>
               <ts e="T99" id="Seg_648" n="e" s="T98">kăštəgut, </ts>
               <ts e="T100" id="Seg_650" n="e" s="T99">šuktəloma." </ts>
               <ts e="T101" id="Seg_652" n="e" s="T100">"Măn </ts>
               <ts e="T102" id="Seg_654" n="e" s="T101">dĭgən </ts>
               <ts e="T103" id="Seg_656" n="e" s="T102">igem." </ts>
               <ts e="T104" id="Seg_658" n="e" s="T103">Ne </ts>
               <ts e="T105" id="Seg_660" n="e" s="T104">kubi, </ts>
               <ts e="T106" id="Seg_662" n="e" s="T105">pa </ts>
               <ts e="T107" id="Seg_664" n="e" s="T106">kabarbi, </ts>
               <ts e="T108" id="Seg_666" n="e" s="T107">dĭ </ts>
               <ts e="T109" id="Seg_668" n="e" s="T108">tʼuga </ts>
               <ts e="T110" id="Seg_670" n="e" s="T109">toʔluʔbi. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_671" s="T0">AA_1914_Mouse_flk.001 (001.001)</ta>
            <ta e="T10" id="Seg_672" s="T6">AA_1914_Mouse_flk.002 (001.002)</ta>
            <ta e="T13" id="Seg_673" s="T10">AA_1914_Mouse_flk.003 (001.003)</ta>
            <ta e="T17" id="Seg_674" s="T13">AA_1914_Mouse_flk.004 (001.004)</ta>
            <ta e="T21" id="Seg_675" s="T17">AA_1914_Mouse_flk.005 (001.005)</ta>
            <ta e="T26" id="Seg_676" s="T21">AA_1914_Mouse_flk.006 (001.006)</ta>
            <ta e="T29" id="Seg_677" s="T26">AA_1914_Mouse_flk.007 (001.007)</ta>
            <ta e="T32" id="Seg_678" s="T29">AA_1914_Mouse_flk.008 (001.008)</ta>
            <ta e="T35" id="Seg_679" s="T32">AA_1914_Mouse_flk.009 (001.009)</ta>
            <ta e="T43" id="Seg_680" s="T35">AA_1914_Mouse_flk.010 (001.010)</ta>
            <ta e="T48" id="Seg_681" s="T43">AA_1914_Mouse_flk.011 (001.011)</ta>
            <ta e="T53" id="Seg_682" s="T48">AA_1914_Mouse_flk.012 (001.012)</ta>
            <ta e="T55" id="Seg_683" s="T53">AA_1914_Mouse_flk.013 (001.014)</ta>
            <ta e="T62" id="Seg_684" s="T55">AA_1914_Mouse_flk.014 (001.015)</ta>
            <ta e="T65" id="Seg_685" s="T62">AA_1914_Mouse_flk.015 (001.017)</ta>
            <ta e="T69" id="Seg_686" s="T65">AA_1914_Mouse_flk.016 (001.018)</ta>
            <ta e="T73" id="Seg_687" s="T69">AA_1914_Mouse_flk.017 (001.019)</ta>
            <ta e="T76" id="Seg_688" s="T73">AA_1914_Mouse_flk.018 (001.020)</ta>
            <ta e="T82" id="Seg_689" s="T76">AA_1914_Mouse_flk.019 (001.021)</ta>
            <ta e="T84" id="Seg_690" s="T82">AA_1914_Mouse_flk.020 (001.022)</ta>
            <ta e="T87" id="Seg_691" s="T84">AA_1914_Mouse_flk.021 (001.023)</ta>
            <ta e="T91" id="Seg_692" s="T87">AA_1914_Mouse_flk.022 (001.024)</ta>
            <ta e="T95" id="Seg_693" s="T91">AA_1914_Mouse_flk.023 (001.025)</ta>
            <ta e="T97" id="Seg_694" s="T95">AA_1914_Mouse_flk.024 (001.026)</ta>
            <ta e="T100" id="Seg_695" s="T97">AA_1914_Mouse_flk.025 (001.027)</ta>
            <ta e="T103" id="Seg_696" s="T100">AA_1914_Mouse_flk.026 (001.028)</ta>
            <ta e="T110" id="Seg_697" s="T103">AA_1914_Mouse_flk.027 (001.029)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_698" s="T0">Tumo da kös suzəj saməjlaʔ kambijəʔ. </ta>
            <ta e="T10" id="Seg_699" s="T6">Tʼăga toʔbdobi, bejde moliaiʔ. </ta>
            <ta e="T13" id="Seg_700" s="T10">Šalguj băldəbiiʔ, köbrügəmbi. </ta>
            <ta e="T17" id="Seg_701" s="T13">Kös bejlaːndəbi šalguj amoluʔbdəbi. </ta>
            <ta e="T21" id="Seg_702" s="T17">Kös bünə üzəbi, tʼărlərluʔbi. </ta>
            <ta e="T26" id="Seg_703" s="T21">Suzəj kakənarbi, kakənarbi, păktəlaʔ saʔməbi. </ta>
            <ta e="T29" id="Seg_704" s="T26">Tumo unʼə maʔbi. </ta>
            <ta e="T32" id="Seg_705" s="T29">Tʼăgan kunzə mĭnǝllaːndəbi. </ta>
            <ta e="T35" id="Seg_706" s="T32">Šamnak bălda toʔbdobi. </ta>
            <ta e="T43" id="Seg_707" s="T35">Ibi dĭ šamnaːm, amnəbi šamnaktə, penʼəjloʔbdəbi tʼăgan tăžə. </ta>
            <ta e="T48" id="Seg_708" s="T43">Kubində maʔ nuga, esseŋ sʼarlaʔbəjəʔ. </ta>
            <ta e="T53" id="Seg_709" s="T48">Kegərerbi tumo: "Esseŋ, urgaːbalaʔ šonəga." </ta>
            <ta e="T55" id="Seg_710" s="T53">"Uja paʔkaʔ!" </ta>
            <ta e="T62" id="Seg_711" s="T55">D esseŋ üʔməbiiʔ ijagəndən nörbəbi: "Urgaːbabaʔ šonənamna. </ta>
            <ta e="T65" id="Seg_712" s="T62">Uja padaʔ, ija!" </ta>
            <ta e="T69" id="Seg_713" s="T65">"Kallaʔ kăštəgaʔ, kola pülaːmbi." </ta>
            <ta e="T73" id="Seg_714" s="T69">"Măn kola ej amənzəliem. </ta>
            <ta e="T76" id="Seg_715" s="T73">Let iʔgö, kiaʔdolam." </ta>
            <ta e="T82" id="Seg_716" s="T76">Baltubə kabarbi, tüžöjdə kuppi, uja paʔbi. </ta>
            <ta e="T84" id="Seg_717" s="T82">"Kăštəgut urgaːbalaʔ. </ta>
            <ta e="T87" id="Seg_718" s="T84">Kăštəgut, uja pülaːmbi." </ta>
            <ta e="T91" id="Seg_719" s="T87">"Urgaːbaʔ šoʔ, uja pülaːmbi." </ta>
            <ta e="T95" id="Seg_720" s="T91">Sagər konun kubabə šuktəlgut! </ta>
            <ta e="T97" id="Seg_721" s="T95">"Šuktəlbibaʔ, šoʔ! </ta>
            <ta e="T100" id="Seg_722" s="T97">Kallaʔ kăštəgut, šuktəloma." </ta>
            <ta e="T103" id="Seg_723" s="T100">"Măn dĭgən igem." </ta>
            <ta e="T110" id="Seg_724" s="T103">Ne kubi, pa kabarbi, dĭ tʼuga toʔluʔbi. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_725" s="T0">tumo</ta>
            <ta e="T2" id="Seg_726" s="T1">da</ta>
            <ta e="T3" id="Seg_727" s="T2">kös</ta>
            <ta e="T4" id="Seg_728" s="T3">suzəj</ta>
            <ta e="T5" id="Seg_729" s="T4">saməj-laʔ</ta>
            <ta e="T6" id="Seg_730" s="T5">kam-bi-jəʔ</ta>
            <ta e="T7" id="Seg_731" s="T6">tʼăga</ta>
            <ta e="T8" id="Seg_732" s="T7">toʔbdo-bi</ta>
            <ta e="T9" id="Seg_733" s="T8">bej-de</ta>
            <ta e="T10" id="Seg_734" s="T9">mo-lia-iʔ</ta>
            <ta e="T11" id="Seg_735" s="T10">šalguj</ta>
            <ta e="T12" id="Seg_736" s="T11">băldə-bi-iʔ</ta>
            <ta e="T13" id="Seg_737" s="T12">köbrügəm-bi</ta>
            <ta e="T14" id="Seg_738" s="T13">kös</ta>
            <ta e="T15" id="Seg_739" s="T14">bej-laːndə-bi</ta>
            <ta e="T16" id="Seg_740" s="T15">šalguj</ta>
            <ta e="T17" id="Seg_741" s="T16">amo-luʔbdə-bi</ta>
            <ta e="T18" id="Seg_742" s="T17">kös</ta>
            <ta e="T19" id="Seg_743" s="T18">bü-nə</ta>
            <ta e="T20" id="Seg_744" s="T19">üzə-bi</ta>
            <ta e="T21" id="Seg_745" s="T20">tʼărlər-luʔ-bi</ta>
            <ta e="T22" id="Seg_746" s="T21">suzəj</ta>
            <ta e="T23" id="Seg_747" s="T22">kakənar-bi</ta>
            <ta e="T24" id="Seg_748" s="T23">kakənar-bi</ta>
            <ta e="T25" id="Seg_749" s="T24">păktə-laʔ</ta>
            <ta e="T26" id="Seg_750" s="T25">saʔmə-bi</ta>
            <ta e="T27" id="Seg_751" s="T26">tumo</ta>
            <ta e="T28" id="Seg_752" s="T27">unʼə</ta>
            <ta e="T29" id="Seg_753" s="T28">maʔ-bi</ta>
            <ta e="T30" id="Seg_754" s="T29">tʼăga-n</ta>
            <ta e="T31" id="Seg_755" s="T30">kunzə</ta>
            <ta e="T32" id="Seg_756" s="T31">mĭnəl-laːndə-bi</ta>
            <ta e="T33" id="Seg_757" s="T32">šamnak</ta>
            <ta e="T34" id="Seg_758" s="T33">băl-da</ta>
            <ta e="T35" id="Seg_759" s="T34">toʔbdo-bi</ta>
            <ta e="T36" id="Seg_760" s="T35">i-bi</ta>
            <ta e="T37" id="Seg_761" s="T36">dĭ</ta>
            <ta e="T38" id="Seg_762" s="T37">šamnaː-m</ta>
            <ta e="T39" id="Seg_763" s="T38">amnə-bi</ta>
            <ta e="T40" id="Seg_764" s="T39">šamnak-tǝ</ta>
            <ta e="T41" id="Seg_765" s="T40">penʼəj-loʔbdə-bi</ta>
            <ta e="T42" id="Seg_766" s="T41">tʼăga-n</ta>
            <ta e="T43" id="Seg_767" s="T42">tăžə</ta>
            <ta e="T44" id="Seg_768" s="T43">ku-bi-ndə</ta>
            <ta e="T45" id="Seg_769" s="T44">maʔ</ta>
            <ta e="T46" id="Seg_770" s="T45">nu-ga</ta>
            <ta e="T47" id="Seg_771" s="T46">es-seŋ</ta>
            <ta e="T48" id="Seg_772" s="T47">sʼar-laʔbə-jəʔ</ta>
            <ta e="T49" id="Seg_773" s="T48">kegərer-bi</ta>
            <ta e="T50" id="Seg_774" s="T49">tumo</ta>
            <ta e="T51" id="Seg_775" s="T50">es-seŋ</ta>
            <ta e="T52" id="Seg_776" s="T51">urgaːba-laʔ</ta>
            <ta e="T53" id="Seg_777" s="T52">šonə-ga</ta>
            <ta e="T54" id="Seg_778" s="T53">uja</ta>
            <ta e="T55" id="Seg_779" s="T54">paʔ-kaʔ</ta>
            <ta e="T56" id="Seg_780" s="T55">d</ta>
            <ta e="T57" id="Seg_781" s="T56">es-seŋ</ta>
            <ta e="T58" id="Seg_782" s="T57">üʔmə-bi-iʔ</ta>
            <ta e="T59" id="Seg_783" s="T58">ija-gəndən</ta>
            <ta e="T60" id="Seg_784" s="T59">nörbə-bi</ta>
            <ta e="T61" id="Seg_785" s="T60">urgaːba-baʔ</ta>
            <ta e="T62" id="Seg_786" s="T61">šonə-namna</ta>
            <ta e="T63" id="Seg_787" s="T62">uja</ta>
            <ta e="T64" id="Seg_788" s="T63">pada-ʔ</ta>
            <ta e="T65" id="Seg_789" s="T64">ija</ta>
            <ta e="T66" id="Seg_790" s="T65">kal-laʔ</ta>
            <ta e="T67" id="Seg_791" s="T66">kăštə-gaʔ</ta>
            <ta e="T68" id="Seg_792" s="T67">kola</ta>
            <ta e="T69" id="Seg_793" s="T68">pü-laːm-bi</ta>
            <ta e="T70" id="Seg_794" s="T69">măn</ta>
            <ta e="T71" id="Seg_795" s="T70">kola</ta>
            <ta e="T72" id="Seg_796" s="T71">ej</ta>
            <ta e="T73" id="Seg_797" s="T72">amə-nzə-lie-m</ta>
            <ta e="T74" id="Seg_798" s="T73">le-t</ta>
            <ta e="T75" id="Seg_799" s="T74">iʔgö</ta>
            <ta e="T76" id="Seg_800" s="T75">kiaʔdo-la-m</ta>
            <ta e="T77" id="Seg_801" s="T76">baltu-bə</ta>
            <ta e="T78" id="Seg_802" s="T77">kabar-bi</ta>
            <ta e="T79" id="Seg_803" s="T78">tüžöj-də</ta>
            <ta e="T80" id="Seg_804" s="T79">kup-pi</ta>
            <ta e="T81" id="Seg_805" s="T80">uja</ta>
            <ta e="T82" id="Seg_806" s="T81">paʔ-bi</ta>
            <ta e="T83" id="Seg_807" s="T82">kăštə-gut</ta>
            <ta e="T84" id="Seg_808" s="T83">urgaːba-laʔ</ta>
            <ta e="T85" id="Seg_809" s="T84">kăštə-gut</ta>
            <ta e="T86" id="Seg_810" s="T85">uja</ta>
            <ta e="T87" id="Seg_811" s="T86">pü-laːm-bi</ta>
            <ta e="T88" id="Seg_812" s="T87">urgaːbaʔ</ta>
            <ta e="T89" id="Seg_813" s="T88">šo-ʔ</ta>
            <ta e="T90" id="Seg_814" s="T89">uja</ta>
            <ta e="T91" id="Seg_815" s="T90">pü-laːm-bi</ta>
            <ta e="T92" id="Seg_816" s="T91">sagər</ta>
            <ta e="T93" id="Seg_817" s="T92">konu-n</ta>
            <ta e="T94" id="Seg_818" s="T93">kuba-bə</ta>
            <ta e="T95" id="Seg_819" s="T94">šuktəl-gut</ta>
            <ta e="T96" id="Seg_820" s="T95">Šuktəl-bi-baʔ</ta>
            <ta e="T97" id="Seg_821" s="T96">šo-ʔ</ta>
            <ta e="T98" id="Seg_822" s="T97">kal-laʔ</ta>
            <ta e="T99" id="Seg_823" s="T98">kăštə-gut</ta>
            <ta e="T100" id="Seg_824" s="T99">šuktəl-oma</ta>
            <ta e="T101" id="Seg_825" s="T100">măn</ta>
            <ta e="T102" id="Seg_826" s="T101">dĭgən</ta>
            <ta e="T103" id="Seg_827" s="T102">i-ge-m</ta>
            <ta e="T104" id="Seg_828" s="T103">ne</ta>
            <ta e="T105" id="Seg_829" s="T104">ku-bi</ta>
            <ta e="T106" id="Seg_830" s="T105">pa</ta>
            <ta e="T107" id="Seg_831" s="T106">kabar-bi</ta>
            <ta e="T108" id="Seg_832" s="T107">dĭ</ta>
            <ta e="T109" id="Seg_833" s="T108">tʼuga</ta>
            <ta e="T110" id="Seg_834" s="T109">toʔ-luʔ-bi</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_835" s="T0">tumo</ta>
            <ta e="T2" id="Seg_836" s="T1">da</ta>
            <ta e="T3" id="Seg_837" s="T2">kös</ta>
            <ta e="T4" id="Seg_838" s="T3">suzəj</ta>
            <ta e="T5" id="Seg_839" s="T4">saməj-lAʔ</ta>
            <ta e="T6" id="Seg_840" s="T5">kan-bi-jəʔ</ta>
            <ta e="T7" id="Seg_841" s="T6">tʼăga</ta>
            <ta e="T8" id="Seg_842" s="T7">toʔbdo-bi</ta>
            <ta e="T9" id="Seg_843" s="T8">bej-NTA</ta>
            <ta e="T10" id="Seg_844" s="T9">mo-liA-jəʔ</ta>
            <ta e="T11" id="Seg_845" s="T10">šalguj</ta>
            <ta e="T12" id="Seg_846" s="T11">băldə-bi-jəʔ</ta>
            <ta e="T13" id="Seg_847" s="T12">köbrügəm-bi</ta>
            <ta e="T14" id="Seg_848" s="T13">kös</ta>
            <ta e="T15" id="Seg_849" s="T14">bej-laːndə-bi</ta>
            <ta e="T16" id="Seg_850" s="T15">šalguj</ta>
            <ta e="T17" id="Seg_851" s="T16">amo-luʔbdə-bi</ta>
            <ta e="T18" id="Seg_852" s="T17">kös</ta>
            <ta e="T19" id="Seg_853" s="T18">bü-Tə</ta>
            <ta e="T20" id="Seg_854" s="T19">üzə-bi</ta>
            <ta e="T21" id="Seg_855" s="T20">tʼărlər-luʔbdə-bi</ta>
            <ta e="T22" id="Seg_856" s="T21">suzəj</ta>
            <ta e="T23" id="Seg_857" s="T22">kakənar-bi</ta>
            <ta e="T24" id="Seg_858" s="T23">kakənar-bi</ta>
            <ta e="T25" id="Seg_859" s="T24">păktə-lAʔ</ta>
            <ta e="T26" id="Seg_860" s="T25">saʔmə-bi</ta>
            <ta e="T27" id="Seg_861" s="T26">tumo</ta>
            <ta e="T28" id="Seg_862" s="T27">unʼə</ta>
            <ta e="T29" id="Seg_863" s="T28">ma-bi</ta>
            <ta e="T30" id="Seg_864" s="T29">tʼăga-n</ta>
            <ta e="T31" id="Seg_865" s="T30">kunzə</ta>
            <ta e="T32" id="Seg_866" s="T31">mĭnəl-laːndə-bi</ta>
            <ta e="T33" id="Seg_867" s="T32">šamnak</ta>
            <ta e="T34" id="Seg_868" s="T33">băldə-NTA</ta>
            <ta e="T35" id="Seg_869" s="T34">toʔbdo-bi</ta>
            <ta e="T36" id="Seg_870" s="T35">i-bi</ta>
            <ta e="T37" id="Seg_871" s="T36">dĭ</ta>
            <ta e="T38" id="Seg_872" s="T37">šamnak-m</ta>
            <ta e="T39" id="Seg_873" s="T38">amnə-bi</ta>
            <ta e="T40" id="Seg_874" s="T39">šamnak-Tə</ta>
            <ta e="T41" id="Seg_875" s="T40">penʼəj-loʔbdə-bi</ta>
            <ta e="T42" id="Seg_876" s="T41">tʼăga-n</ta>
            <ta e="T43" id="Seg_877" s="T42">tăžə</ta>
            <ta e="T44" id="Seg_878" s="T43">ku-bi-gəndə</ta>
            <ta e="T45" id="Seg_879" s="T44">maʔ</ta>
            <ta e="T46" id="Seg_880" s="T45">nu-gA</ta>
            <ta e="T47" id="Seg_881" s="T46">ešši-zAŋ</ta>
            <ta e="T48" id="Seg_882" s="T47">sʼar-laʔbə-jəʔ</ta>
            <ta e="T49" id="Seg_883" s="T48">kegərer-bi</ta>
            <ta e="T50" id="Seg_884" s="T49">tumo</ta>
            <ta e="T51" id="Seg_885" s="T50">ešši-zAŋ</ta>
            <ta e="T52" id="Seg_886" s="T51">urgaːba-lAʔ</ta>
            <ta e="T53" id="Seg_887" s="T52">šonə-gA</ta>
            <ta e="T54" id="Seg_888" s="T53">uja</ta>
            <ta e="T55" id="Seg_889" s="T54">pada-KAʔ</ta>
            <ta e="T56" id="Seg_890" s="T55">dĭ</ta>
            <ta e="T57" id="Seg_891" s="T56">ešši-zAŋ</ta>
            <ta e="T58" id="Seg_892" s="T57">üʔmə-bi-jəʔ</ta>
            <ta e="T59" id="Seg_893" s="T58">ija-gəndən</ta>
            <ta e="T60" id="Seg_894" s="T59">nörbə-bi</ta>
            <ta e="T61" id="Seg_895" s="T60">urgaːba-baʔ</ta>
            <ta e="T62" id="Seg_896" s="T61">šonə-lamnə</ta>
            <ta e="T63" id="Seg_897" s="T62">uja</ta>
            <ta e="T64" id="Seg_898" s="T63">pada-ʔ</ta>
            <ta e="T65" id="Seg_899" s="T64">ija</ta>
            <ta e="T66" id="Seg_900" s="T65">kan-lAʔ</ta>
            <ta e="T67" id="Seg_901" s="T66">kăštə-KAʔ</ta>
            <ta e="T68" id="Seg_902" s="T67">kola</ta>
            <ta e="T69" id="Seg_903" s="T68">pü-laːm-bi</ta>
            <ta e="T70" id="Seg_904" s="T69">măn</ta>
            <ta e="T71" id="Seg_905" s="T70">kola</ta>
            <ta e="T72" id="Seg_906" s="T71">ej</ta>
            <ta e="T73" id="Seg_907" s="T72">am-nzə-liA-m</ta>
            <ta e="T74" id="Seg_908" s="T73">le-t</ta>
            <ta e="T75" id="Seg_909" s="T74">iʔgö</ta>
            <ta e="T76" id="Seg_910" s="T75">kiaʔdo-lV-m</ta>
            <ta e="T77" id="Seg_911" s="T76">baltu-bə</ta>
            <ta e="T78" id="Seg_912" s="T77">kabar-bi</ta>
            <ta e="T79" id="Seg_913" s="T78">tüžöj-də</ta>
            <ta e="T80" id="Seg_914" s="T79">kut-bi</ta>
            <ta e="T81" id="Seg_915" s="T80">uja</ta>
            <ta e="T82" id="Seg_916" s="T81">pada-bi</ta>
            <ta e="T83" id="Seg_917" s="T82">kăštə-Kut</ta>
            <ta e="T84" id="Seg_918" s="T83">urgaːba-lAʔ</ta>
            <ta e="T85" id="Seg_919" s="T84">kăštə-Kut</ta>
            <ta e="T86" id="Seg_920" s="T85">uja</ta>
            <ta e="T87" id="Seg_921" s="T86">pü-laːm-bi</ta>
            <ta e="T88" id="Seg_922" s="T87">urgaːba</ta>
            <ta e="T89" id="Seg_923" s="T88">šo-ʔ</ta>
            <ta e="T90" id="Seg_924" s="T89">uja</ta>
            <ta e="T91" id="Seg_925" s="T90">pü-laːm-bi</ta>
            <ta e="T92" id="Seg_926" s="T91">sagər</ta>
            <ta e="T93" id="Seg_927" s="T92">konu-n</ta>
            <ta e="T94" id="Seg_928" s="T93">kuba-bə</ta>
            <ta e="T95" id="Seg_929" s="T94">šuktəl-Kut</ta>
            <ta e="T96" id="Seg_930" s="T95">šuktəl-bi-bAʔ</ta>
            <ta e="T97" id="Seg_931" s="T96">šo-ʔ</ta>
            <ta e="T98" id="Seg_932" s="T97">kan-lAʔ</ta>
            <ta e="T99" id="Seg_933" s="T98">kăštə-Kut</ta>
            <ta e="T100" id="Seg_934" s="T99">šuktəl-oma</ta>
            <ta e="T101" id="Seg_935" s="T100">măn</ta>
            <ta e="T102" id="Seg_936" s="T101">dĭgən</ta>
            <ta e="T103" id="Seg_937" s="T102">i-gA-m</ta>
            <ta e="T104" id="Seg_938" s="T103">ne</ta>
            <ta e="T105" id="Seg_939" s="T104">ku-bi</ta>
            <ta e="T106" id="Seg_940" s="T105">pa</ta>
            <ta e="T107" id="Seg_941" s="T106">kabar-bi</ta>
            <ta e="T108" id="Seg_942" s="T107">dĭ</ta>
            <ta e="T109" id="Seg_943" s="T108">tʼuga</ta>
            <ta e="T110" id="Seg_944" s="T109">toʔbdə-luʔbdə-bi</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_945" s="T0">mouse.[NOM.SG]</ta>
            <ta e="T2" id="Seg_946" s="T1">and</ta>
            <ta e="T3" id="Seg_947" s="T2">coal.[NOM.SG]</ta>
            <ta e="T4" id="Seg_948" s="T3">bladder.[NOM.SG]</ta>
            <ta e="T5" id="Seg_949" s="T4">hunt-CVB</ta>
            <ta e="T6" id="Seg_950" s="T5">go-PST-3PL</ta>
            <ta e="T7" id="Seg_951" s="T6">river.[NOM.SG]</ta>
            <ta e="T8" id="Seg_952" s="T7">come.up-PST.[3SG]</ta>
            <ta e="T9" id="Seg_953" s="T8">cross-PTCP</ta>
            <ta e="T10" id="Seg_954" s="T9">want-PRS-3PL</ta>
            <ta e="T11" id="Seg_955" s="T10">blade.of.grass.[NOM.SG]</ta>
            <ta e="T12" id="Seg_956" s="T11">break-PST-3PL</ta>
            <ta e="T13" id="Seg_957" s="T12">become.a.bridge-PST.[3SG]</ta>
            <ta e="T14" id="Seg_958" s="T13">coal.[NOM.SG]</ta>
            <ta e="T15" id="Seg_959" s="T14">cross-DUR-PST.[3SG]</ta>
            <ta e="T16" id="Seg_960" s="T15">blade.of.grass.[NOM.SG]</ta>
            <ta e="T17" id="Seg_961" s="T16">burn-MOM-PST.[3SG]</ta>
            <ta e="T18" id="Seg_962" s="T17">coal.[NOM.SG]</ta>
            <ta e="T19" id="Seg_963" s="T18">water-LAT</ta>
            <ta e="T20" id="Seg_964" s="T19">fall-PST.[3SG]</ta>
            <ta e="T21" id="Seg_965" s="T20">fizzle-MOM-PST.[3SG]</ta>
            <ta e="T22" id="Seg_966" s="T21">bladder.[NOM.SG]</ta>
            <ta e="T23" id="Seg_967" s="T22">laugh-PST.[3SG]</ta>
            <ta e="T24" id="Seg_968" s="T23">laugh-PST.[3SG]</ta>
            <ta e="T25" id="Seg_969" s="T24">burst-CVB</ta>
            <ta e="T26" id="Seg_970" s="T25">collapse-PST.[3SG]</ta>
            <ta e="T27" id="Seg_971" s="T26">mouse.[NOM.SG]</ta>
            <ta e="T28" id="Seg_972" s="T27">alone</ta>
            <ta e="T29" id="Seg_973" s="T28">stay-PST.[3SG]</ta>
            <ta e="T30" id="Seg_974" s="T29">river-GEN</ta>
            <ta e="T31" id="Seg_975" s="T30">along</ta>
            <ta e="T32" id="Seg_976" s="T31">pass-DUR-PST.[3SG]</ta>
            <ta e="T33" id="Seg_977" s="T32">spoon.[NOM.SG]</ta>
            <ta e="T34" id="Seg_978" s="T33">break-PTCP</ta>
            <ta e="T35" id="Seg_979" s="T34">come.up-PST.[3SG]</ta>
            <ta e="T36" id="Seg_980" s="T35">take-PST.[3SG]</ta>
            <ta e="T37" id="Seg_981" s="T36">this.[NOM.SG]</ta>
            <ta e="T38" id="Seg_982" s="T37">spoon-ACC</ta>
            <ta e="T39" id="Seg_983" s="T38">sit-PST.[3SG]</ta>
            <ta e="T40" id="Seg_984" s="T39">spoon-LAT</ta>
            <ta e="T41" id="Seg_985" s="T40">float-INCH-PST.[3SG]</ta>
            <ta e="T42" id="Seg_986" s="T41">river-GEN</ta>
            <ta e="T43" id="Seg_987" s="T42">downstream</ta>
            <ta e="T44" id="Seg_988" s="T43">see-PTCP.PRS-LAT/LOC.3SG</ta>
            <ta e="T45" id="Seg_989" s="T44">tent.[NOM.SG]</ta>
            <ta e="T46" id="Seg_990" s="T45">stand-PRS.[3SG]</ta>
            <ta e="T47" id="Seg_991" s="T46">child-PL</ta>
            <ta e="T48" id="Seg_992" s="T47">play-DUR-3PL</ta>
            <ta e="T49" id="Seg_993" s="T48">call.out-PST.[3SG]</ta>
            <ta e="T50" id="Seg_994" s="T49">mouse.[NOM.SG]</ta>
            <ta e="T51" id="Seg_995" s="T50">child-PL</ta>
            <ta e="T52" id="Seg_996" s="T51">grandfather-NOM/GEN/ACC.2PL</ta>
            <ta e="T53" id="Seg_997" s="T52">come-PRS.[3SG]</ta>
            <ta e="T54" id="Seg_998" s="T53">meat.[NOM.SG]</ta>
            <ta e="T55" id="Seg_999" s="T54">cook-IMP.2PL</ta>
            <ta e="T56" id="Seg_1000" s="T55">this.[NOM.SG]</ta>
            <ta e="T57" id="Seg_1001" s="T56">child-PL</ta>
            <ta e="T58" id="Seg_1002" s="T57">run-PST-3PL</ta>
            <ta e="T59" id="Seg_1003" s="T58">mother-LAT/LOC.3PL</ta>
            <ta e="T60" id="Seg_1004" s="T59">tell-PST.[3SG]</ta>
            <ta e="T61" id="Seg_1005" s="T60">grandfather-NOM/GEN/ACC.1PL</ta>
            <ta e="T62" id="Seg_1006" s="T61">come-DUR.[3SG]</ta>
            <ta e="T63" id="Seg_1007" s="T62">meat.[NOM.SG]</ta>
            <ta e="T64" id="Seg_1008" s="T63">cook-IMP.2SG</ta>
            <ta e="T65" id="Seg_1009" s="T64">mother.[NOM.SG]</ta>
            <ta e="T66" id="Seg_1010" s="T65">go-CVB</ta>
            <ta e="T67" id="Seg_1011" s="T66">call-IMP.2PL</ta>
            <ta e="T68" id="Seg_1012" s="T67">fish.[NOM.SG]</ta>
            <ta e="T69" id="Seg_1013" s="T68">get.ready-RES-PST.[3SG]</ta>
            <ta e="T70" id="Seg_1014" s="T69">I.NOM</ta>
            <ta e="T71" id="Seg_1015" s="T70">fish.[NOM.SG]</ta>
            <ta e="T72" id="Seg_1016" s="T71">NEG</ta>
            <ta e="T73" id="Seg_1017" s="T72">eat-DES-PRS-1SG</ta>
            <ta e="T74" id="Seg_1018" s="T73">bone-NOM/GEN.3SG</ta>
            <ta e="T75" id="Seg_1019" s="T74">many</ta>
            <ta e="T76" id="Seg_1020" s="T75">choke-FUT-1SG</ta>
            <ta e="T77" id="Seg_1021" s="T76">axe-ACC.3SG</ta>
            <ta e="T78" id="Seg_1022" s="T77">grab-PST.[3SG]</ta>
            <ta e="T79" id="Seg_1023" s="T78">cow-NOM/GEN/ACC.3SG</ta>
            <ta e="T80" id="Seg_1024" s="T79">kill-PST.[3SG]</ta>
            <ta e="T81" id="Seg_1025" s="T80">meat.[NOM.SG]</ta>
            <ta e="T82" id="Seg_1026" s="T81">cook-PST.[3SG]</ta>
            <ta e="T83" id="Seg_1027" s="T82">invite-IMP.2PL.O</ta>
            <ta e="T84" id="Seg_1028" s="T83">grandfather-NOM/GEN/ACC.2PL</ta>
            <ta e="T85" id="Seg_1029" s="T84">invite-IMP.2PL.O</ta>
            <ta e="T86" id="Seg_1030" s="T85">meat.[NOM.SG]</ta>
            <ta e="T87" id="Seg_1031" s="T86">get.ready-RES-PST.[3SG]</ta>
            <ta e="T88" id="Seg_1032" s="T87">grandfather</ta>
            <ta e="T89" id="Seg_1033" s="T88">come-IMP.2SG</ta>
            <ta e="T90" id="Seg_1034" s="T89">meat.[NOM.SG]</ta>
            <ta e="T91" id="Seg_1035" s="T90">get.ready-RES-PST.[3SG]</ta>
            <ta e="T92" id="Seg_1036" s="T91">black.[NOM.SG]</ta>
            <ta e="T93" id="Seg_1037" s="T92">bear-GEN</ta>
            <ta e="T94" id="Seg_1038" s="T93">skin-ACC.3SG</ta>
            <ta e="T95" id="Seg_1039" s="T94">spread-IMP.2PL.O</ta>
            <ta e="T96" id="Seg_1040" s="T95">spread-PST-1PL</ta>
            <ta e="T97" id="Seg_1041" s="T96">come-IMP.2SG</ta>
            <ta e="T98" id="Seg_1042" s="T97">go-CVB</ta>
            <ta e="T99" id="Seg_1043" s="T98">invite-IMP.2PL.O</ta>
            <ta e="T100" id="Seg_1044" s="T99">spread-PTCP.PASS</ta>
            <ta e="T101" id="Seg_1045" s="T100">I.NOM</ta>
            <ta e="T102" id="Seg_1046" s="T101">here</ta>
            <ta e="T103" id="Seg_1047" s="T102">be-PRS-1SG</ta>
            <ta e="T104" id="Seg_1048" s="T103">woman.[NOM.SG]</ta>
            <ta e="T105" id="Seg_1049" s="T104">see-PST.[3SG]</ta>
            <ta e="T106" id="Seg_1050" s="T105">tree.[NOM.SG]</ta>
            <ta e="T107" id="Seg_1051" s="T106">grab-PST.[3SG]</ta>
            <ta e="T108" id="Seg_1052" s="T107">this.[NOM.SG]</ta>
            <ta e="T109" id="Seg_1053" s="T108">dead.[NOM.SG]</ta>
            <ta e="T110" id="Seg_1054" s="T109">hit-MOM-PST.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="TIE6">
            <ta e="T1" id="Seg_1055" s="T0">мышь.[NOM.SG]</ta>
            <ta e="T2" id="Seg_1056" s="T1">и</ta>
            <ta e="T3" id="Seg_1057" s="T2">уголь.[NOM.SG]</ta>
            <ta e="T4" id="Seg_1058" s="T3">пузырь.[NOM.SG]</ta>
            <ta e="T5" id="Seg_1059" s="T4">охотиться-CVB</ta>
            <ta e="T6" id="Seg_1060" s="T5">пойти-PST-3PL</ta>
            <ta e="T7" id="Seg_1061" s="T6">река.[NOM.SG]</ta>
            <ta e="T8" id="Seg_1062" s="T7">попасть-PST.[3SG]</ta>
            <ta e="T9" id="Seg_1063" s="T8">пересечь-PTCP</ta>
            <ta e="T10" id="Seg_1064" s="T9">хотеть-PRS-3PL</ta>
            <ta e="T11" id="Seg_1065" s="T10">соломинка.[NOM.SG]</ta>
            <ta e="T12" id="Seg_1066" s="T11">сломать-PST-3PL</ta>
            <ta e="T13" id="Seg_1067" s="T12">стать.мостом-PST.[3SG]</ta>
            <ta e="T14" id="Seg_1068" s="T13">уголь.[NOM.SG]</ta>
            <ta e="T15" id="Seg_1069" s="T14">пересечь-DUR-PST.[3SG]</ta>
            <ta e="T16" id="Seg_1070" s="T15">соломинка.[NOM.SG]</ta>
            <ta e="T17" id="Seg_1071" s="T16">гореть-MOM-PST.[3SG]</ta>
            <ta e="T18" id="Seg_1072" s="T17">уголь.[NOM.SG]</ta>
            <ta e="T19" id="Seg_1073" s="T18">вода-LAT</ta>
            <ta e="T20" id="Seg_1074" s="T19">упасть-PST.[3SG]</ta>
            <ta e="T21" id="Seg_1075" s="T20">шипеть-MOM-PST.[3SG]</ta>
            <ta e="T22" id="Seg_1076" s="T21">пузырь.[NOM.SG]</ta>
            <ta e="T23" id="Seg_1077" s="T22">смеяться-PST.[3SG]</ta>
            <ta e="T24" id="Seg_1078" s="T23">смеяться-PST.[3SG]</ta>
            <ta e="T25" id="Seg_1079" s="T24">лопнуть-CVB</ta>
            <ta e="T26" id="Seg_1080" s="T25">обрушиться-PST.[3SG]</ta>
            <ta e="T27" id="Seg_1081" s="T26">мышь.[NOM.SG]</ta>
            <ta e="T28" id="Seg_1082" s="T27">один</ta>
            <ta e="T29" id="Seg_1083" s="T28">остаться-PST.[3SG]</ta>
            <ta e="T30" id="Seg_1084" s="T29">река-GEN</ta>
            <ta e="T31" id="Seg_1085" s="T30">вдоль</ta>
            <ta e="T32" id="Seg_1086" s="T31">пройти-DUR-PST.[3SG]</ta>
            <ta e="T33" id="Seg_1087" s="T32">ложка.[NOM.SG]</ta>
            <ta e="T34" id="Seg_1088" s="T33">сломать-PTCP</ta>
            <ta e="T35" id="Seg_1089" s="T34">попасть-PST.[3SG]</ta>
            <ta e="T36" id="Seg_1090" s="T35">взять-PST.[3SG]</ta>
            <ta e="T37" id="Seg_1091" s="T36">этот.[NOM.SG]</ta>
            <ta e="T38" id="Seg_1092" s="T37">ложка-ACC</ta>
            <ta e="T39" id="Seg_1093" s="T38">сидеть-PST.[3SG]</ta>
            <ta e="T40" id="Seg_1094" s="T39">ложка-LAT</ta>
            <ta e="T41" id="Seg_1095" s="T40">плыть-INCH-PST.[3SG]</ta>
            <ta e="T42" id="Seg_1096" s="T41">река-GEN</ta>
            <ta e="T43" id="Seg_1097" s="T42">вниз.по.течению</ta>
            <ta e="T44" id="Seg_1098" s="T43">видеть-CVB.TEMP-LAT/LOC.3SG</ta>
            <ta e="T45" id="Seg_1099" s="T44">чум.[NOM.SG]</ta>
            <ta e="T46" id="Seg_1100" s="T45">стоять-PRS.[3SG]</ta>
            <ta e="T47" id="Seg_1101" s="T46">ребенок-PL</ta>
            <ta e="T48" id="Seg_1102" s="T47">играть-DUR-3PL</ta>
            <ta e="T49" id="Seg_1103" s="T48">кричать-PST.[3SG]</ta>
            <ta e="T50" id="Seg_1104" s="T49">мышь.[NOM.SG]</ta>
            <ta e="T51" id="Seg_1105" s="T50">ребенок-PL</ta>
            <ta e="T52" id="Seg_1106" s="T51">дед-NOM/GEN/ACC.2PL</ta>
            <ta e="T53" id="Seg_1107" s="T52">прийти-PRS.[3SG]</ta>
            <ta e="T54" id="Seg_1108" s="T53">мясо.[NOM.SG]</ta>
            <ta e="T55" id="Seg_1109" s="T54">варить-IMP.2PL</ta>
            <ta e="T56" id="Seg_1110" s="T55">этот.[NOM.SG]</ta>
            <ta e="T57" id="Seg_1111" s="T56">ребенок-PL</ta>
            <ta e="T58" id="Seg_1112" s="T57">бежать-PST-3PL</ta>
            <ta e="T59" id="Seg_1113" s="T58">мать-LAT/LOC.3PL</ta>
            <ta e="T60" id="Seg_1114" s="T59">сказать-PST.[3SG]</ta>
            <ta e="T61" id="Seg_1115" s="T60">дед-NOM/GEN/ACC.1PL</ta>
            <ta e="T62" id="Seg_1116" s="T61">прийти-DUR.[3SG]</ta>
            <ta e="T63" id="Seg_1117" s="T62">мясо.[NOM.SG]</ta>
            <ta e="T64" id="Seg_1118" s="T63">варить-IMP.2SG</ta>
            <ta e="T65" id="Seg_1119" s="T64">мать.[NOM.SG]</ta>
            <ta e="T66" id="Seg_1120" s="T65">пойти-CVB</ta>
            <ta e="T67" id="Seg_1121" s="T66">позвать-IMP.2PL</ta>
            <ta e="T68" id="Seg_1122" s="T67">рыба.[NOM.SG]</ta>
            <ta e="T69" id="Seg_1123" s="T68">стать.готовым-RES-PST.[3SG]</ta>
            <ta e="T70" id="Seg_1124" s="T69">я.NOM</ta>
            <ta e="T71" id="Seg_1125" s="T70">рыба.[NOM.SG]</ta>
            <ta e="T72" id="Seg_1126" s="T71">NEG</ta>
            <ta e="T73" id="Seg_1127" s="T72">съесть-DES-PRS-1SG</ta>
            <ta e="T74" id="Seg_1128" s="T73">кость-NOM/GEN.3SG</ta>
            <ta e="T75" id="Seg_1129" s="T74">много</ta>
            <ta e="T76" id="Seg_1130" s="T75">давиться-FUT-1SG</ta>
            <ta e="T77" id="Seg_1131" s="T76">топор-ACC.3SG</ta>
            <ta e="T78" id="Seg_1132" s="T77">хватать-PST.[3SG]</ta>
            <ta e="T79" id="Seg_1133" s="T78">корова-NOM/GEN/ACC.3SG</ta>
            <ta e="T80" id="Seg_1134" s="T79">убить-PST.[3SG]</ta>
            <ta e="T81" id="Seg_1135" s="T80">мясо.[NOM.SG]</ta>
            <ta e="T82" id="Seg_1136" s="T81">варить-PST.[3SG]</ta>
            <ta e="T83" id="Seg_1137" s="T82">пригласить-IMP.2PL.O</ta>
            <ta e="T84" id="Seg_1138" s="T83">дед-NOM/GEN/ACC.2PL</ta>
            <ta e="T85" id="Seg_1139" s="T84">пригласить-IMP.2PL.O</ta>
            <ta e="T86" id="Seg_1140" s="T85">мясо.[NOM.SG]</ta>
            <ta e="T87" id="Seg_1141" s="T86">стать.готовым-RES-PST.[3SG]</ta>
            <ta e="T88" id="Seg_1142" s="T87">дед</ta>
            <ta e="T89" id="Seg_1143" s="T88">прийти-IMP.2SG</ta>
            <ta e="T90" id="Seg_1144" s="T89">мясо.[NOM.SG]</ta>
            <ta e="T91" id="Seg_1145" s="T90">стать.готовым-RES-PST.[3SG]</ta>
            <ta e="T92" id="Seg_1146" s="T91">черный.[NOM.SG]</ta>
            <ta e="T93" id="Seg_1147" s="T92">медведь-GEN</ta>
            <ta e="T94" id="Seg_1148" s="T93">кожа-ACC.3SG</ta>
            <ta e="T95" id="Seg_1149" s="T94">разложить-IMP.2PL.O</ta>
            <ta e="T96" id="Seg_1150" s="T95">разложить-PST-1PL</ta>
            <ta e="T97" id="Seg_1151" s="T96">прийти-IMP.2SG</ta>
            <ta e="T98" id="Seg_1152" s="T97">пойти-CVB</ta>
            <ta e="T99" id="Seg_1153" s="T98">пригласить-IMP.2PL.O</ta>
            <ta e="T100" id="Seg_1154" s="T99">разложить-PTCP.PASS</ta>
            <ta e="T101" id="Seg_1155" s="T100">я.NOM</ta>
            <ta e="T102" id="Seg_1156" s="T101">здесь</ta>
            <ta e="T103" id="Seg_1157" s="T102">быть-PRS-1SG</ta>
            <ta e="T104" id="Seg_1158" s="T103">женщина.[NOM.SG]</ta>
            <ta e="T105" id="Seg_1159" s="T104">видеть-PST.[3SG]</ta>
            <ta e="T106" id="Seg_1160" s="T105">дерево.[NOM.SG]</ta>
            <ta e="T107" id="Seg_1161" s="T106">хватать-PST.[3SG]</ta>
            <ta e="T108" id="Seg_1162" s="T107">этот.[NOM.SG]</ta>
            <ta e="T109" id="Seg_1163" s="T108">мертвый.[NOM.SG]</ta>
            <ta e="T110" id="Seg_1164" s="T109">ударить-MOM-PST.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1165" s="T0">n-n:case</ta>
            <ta e="T2" id="Seg_1166" s="T1">conj</ta>
            <ta e="T3" id="Seg_1167" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_1168" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_1169" s="T4">v-v:n.fin</ta>
            <ta e="T6" id="Seg_1170" s="T5">v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_1171" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_1172" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_1173" s="T8">v-v:n.fin</ta>
            <ta e="T10" id="Seg_1174" s="T9">v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_1175" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_1176" s="T11">v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_1177" s="T12">v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_1178" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_1179" s="T14">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_1180" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_1181" s="T16">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T18" id="Seg_1182" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_1183" s="T18">n-n:case</ta>
            <ta e="T20" id="Seg_1184" s="T19">v-v:tense-v:pn</ta>
            <ta e="T21" id="Seg_1185" s="T20">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_1186" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_1187" s="T22">v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_1188" s="T23">v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_1189" s="T24">v-v:n.fin</ta>
            <ta e="T26" id="Seg_1190" s="T25">v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_1191" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_1192" s="T27">adv</ta>
            <ta e="T29" id="Seg_1193" s="T28">v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_1194" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_1195" s="T30">post</ta>
            <ta e="T32" id="Seg_1196" s="T31">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_1197" s="T32">n-n:case</ta>
            <ta e="T34" id="Seg_1198" s="T33">v-v:n.fin</ta>
            <ta e="T35" id="Seg_1199" s="T34">v-v:tense-v:pn</ta>
            <ta e="T36" id="Seg_1200" s="T35">v-v:tense-v:pn</ta>
            <ta e="T37" id="Seg_1201" s="T36">dempro-n:case</ta>
            <ta e="T38" id="Seg_1202" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_1203" s="T38">v-v:tense-v:pn</ta>
            <ta e="T40" id="Seg_1204" s="T39">n-n:case</ta>
            <ta e="T41" id="Seg_1205" s="T40">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T42" id="Seg_1206" s="T41">n-n:case</ta>
            <ta e="T43" id="Seg_1207" s="T42">post</ta>
            <ta e="T44" id="Seg_1208" s="T43">v-v:n.fin-v:(case.poss)</ta>
            <ta e="T45" id="Seg_1209" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_1210" s="T45">v-v:tense-v:pn</ta>
            <ta e="T47" id="Seg_1211" s="T46">n-n:num</ta>
            <ta e="T48" id="Seg_1212" s="T47">v-v&gt;v-v:pn</ta>
            <ta e="T49" id="Seg_1213" s="T48">v-v:tense-v:pn</ta>
            <ta e="T50" id="Seg_1214" s="T49">n-n:case</ta>
            <ta e="T51" id="Seg_1215" s="T50">n-n:num</ta>
            <ta e="T52" id="Seg_1216" s="T51">n-n:case.poss</ta>
            <ta e="T53" id="Seg_1217" s="T52">v-v:tense-v:pn</ta>
            <ta e="T54" id="Seg_1218" s="T53">n-n:case</ta>
            <ta e="T55" id="Seg_1219" s="T54">v-v:mood.pn</ta>
            <ta e="T56" id="Seg_1220" s="T55">dempro-n:case</ta>
            <ta e="T57" id="Seg_1221" s="T56">n-n:num</ta>
            <ta e="T58" id="Seg_1222" s="T57">v-v:tense-v:pn</ta>
            <ta e="T59" id="Seg_1223" s="T58">n-n:case.poss</ta>
            <ta e="T60" id="Seg_1224" s="T59">v-v:tense-v:pn</ta>
            <ta e="T61" id="Seg_1225" s="T60">n-n:case.poss</ta>
            <ta e="T62" id="Seg_1226" s="T61">v-v&gt;v-v:pn</ta>
            <ta e="T63" id="Seg_1227" s="T62">n-n:case</ta>
            <ta e="T64" id="Seg_1228" s="T63">v-v:mood.pn</ta>
            <ta e="T65" id="Seg_1229" s="T64">n-n:case</ta>
            <ta e="T66" id="Seg_1230" s="T65">v-v:n.fin</ta>
            <ta e="T67" id="Seg_1231" s="T66">v-v:mood.pn</ta>
            <ta e="T68" id="Seg_1232" s="T67">n-n:case</ta>
            <ta e="T69" id="Seg_1233" s="T68">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T70" id="Seg_1234" s="T69">pers</ta>
            <ta e="T71" id="Seg_1235" s="T70">n-n:case</ta>
            <ta e="T72" id="Seg_1236" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_1237" s="T72">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T74" id="Seg_1238" s="T73">n-n:case.poss</ta>
            <ta e="T75" id="Seg_1239" s="T74">quant</ta>
            <ta e="T76" id="Seg_1240" s="T75">v-v:tense-v:pn</ta>
            <ta e="T77" id="Seg_1241" s="T76">n-n:case.poss</ta>
            <ta e="T78" id="Seg_1242" s="T77">v-v:tense-v:pn</ta>
            <ta e="T79" id="Seg_1243" s="T78">n-n:case.poss</ta>
            <ta e="T80" id="Seg_1244" s="T79">v-v:tense-v:pn</ta>
            <ta e="T81" id="Seg_1245" s="T80">n-n:case</ta>
            <ta e="T82" id="Seg_1246" s="T81">v-v:tense-v:pn</ta>
            <ta e="T83" id="Seg_1247" s="T82">v-v:mood.pn</ta>
            <ta e="T84" id="Seg_1248" s="T83">n-n:case.poss</ta>
            <ta e="T85" id="Seg_1249" s="T84">v-v:mood.pn</ta>
            <ta e="T86" id="Seg_1250" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_1251" s="T86">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T88" id="Seg_1252" s="T87">n</ta>
            <ta e="T89" id="Seg_1253" s="T88">v-v:mood.pn</ta>
            <ta e="T90" id="Seg_1254" s="T89">n-n:case</ta>
            <ta e="T91" id="Seg_1255" s="T90">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T92" id="Seg_1256" s="T91">adj-n:case</ta>
            <ta e="T93" id="Seg_1257" s="T92">n-n:case</ta>
            <ta e="T94" id="Seg_1258" s="T93">n-n:case.poss</ta>
            <ta e="T95" id="Seg_1259" s="T94">v-v:mood.pn</ta>
            <ta e="T96" id="Seg_1260" s="T95">v-v:tense-v:pn</ta>
            <ta e="T97" id="Seg_1261" s="T96">v-v:mood.pn</ta>
            <ta e="T98" id="Seg_1262" s="T97">v-v:n.fin</ta>
            <ta e="T99" id="Seg_1263" s="T98">v-v:mood.pn</ta>
            <ta e="T100" id="Seg_1264" s="T99">v-v:n.fin</ta>
            <ta e="T101" id="Seg_1265" s="T100">pers</ta>
            <ta e="T102" id="Seg_1266" s="T101">adv</ta>
            <ta e="T103" id="Seg_1267" s="T102">v-v:tense-v:pn</ta>
            <ta e="T104" id="Seg_1268" s="T103">n</ta>
            <ta e="T105" id="Seg_1269" s="T104">v-v:tense-v:pn</ta>
            <ta e="T106" id="Seg_1270" s="T105">n-n:case</ta>
            <ta e="T107" id="Seg_1271" s="T106">v-v:tense-v:pn</ta>
            <ta e="T108" id="Seg_1272" s="T107">dempro-n:case</ta>
            <ta e="T109" id="Seg_1273" s="T108">adj-n:case</ta>
            <ta e="T110" id="Seg_1274" s="T109">v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1275" s="T0">n</ta>
            <ta e="T2" id="Seg_1276" s="T1">conj</ta>
            <ta e="T3" id="Seg_1277" s="T2">n</ta>
            <ta e="T4" id="Seg_1278" s="T3">n</ta>
            <ta e="T5" id="Seg_1279" s="T4">v</ta>
            <ta e="T6" id="Seg_1280" s="T5">v</ta>
            <ta e="T7" id="Seg_1281" s="T6">n</ta>
            <ta e="T8" id="Seg_1282" s="T7">v</ta>
            <ta e="T9" id="Seg_1283" s="T8">adj</ta>
            <ta e="T10" id="Seg_1284" s="T9">v</ta>
            <ta e="T11" id="Seg_1285" s="T10">n</ta>
            <ta e="T12" id="Seg_1286" s="T11">v</ta>
            <ta e="T13" id="Seg_1287" s="T12">v</ta>
            <ta e="T14" id="Seg_1288" s="T13">n</ta>
            <ta e="T15" id="Seg_1289" s="T14">v</ta>
            <ta e="T16" id="Seg_1290" s="T15">n</ta>
            <ta e="T17" id="Seg_1291" s="T16">v</ta>
            <ta e="T18" id="Seg_1292" s="T17">n</ta>
            <ta e="T19" id="Seg_1293" s="T18">n</ta>
            <ta e="T20" id="Seg_1294" s="T19">v</ta>
            <ta e="T21" id="Seg_1295" s="T20">v</ta>
            <ta e="T22" id="Seg_1296" s="T21">n</ta>
            <ta e="T23" id="Seg_1297" s="T22">v</ta>
            <ta e="T24" id="Seg_1298" s="T23">v</ta>
            <ta e="T25" id="Seg_1299" s="T24">v</ta>
            <ta e="T26" id="Seg_1300" s="T25">v</ta>
            <ta e="T27" id="Seg_1301" s="T26">n</ta>
            <ta e="T28" id="Seg_1302" s="T27">adv</ta>
            <ta e="T29" id="Seg_1303" s="T28">v</ta>
            <ta e="T30" id="Seg_1304" s="T29">n</ta>
            <ta e="T31" id="Seg_1305" s="T30">post</ta>
            <ta e="T32" id="Seg_1306" s="T31">v</ta>
            <ta e="T33" id="Seg_1307" s="T32">n</ta>
            <ta e="T34" id="Seg_1308" s="T33">adj</ta>
            <ta e="T35" id="Seg_1309" s="T34">v</ta>
            <ta e="T36" id="Seg_1310" s="T35">v</ta>
            <ta e="T37" id="Seg_1311" s="T36">dempro</ta>
            <ta e="T38" id="Seg_1312" s="T37">n</ta>
            <ta e="T39" id="Seg_1313" s="T38">v</ta>
            <ta e="T40" id="Seg_1314" s="T39">n</ta>
            <ta e="T41" id="Seg_1315" s="T40">v</ta>
            <ta e="T42" id="Seg_1316" s="T41">n</ta>
            <ta e="T43" id="Seg_1317" s="T42">post</ta>
            <ta e="T44" id="Seg_1318" s="T43">adj</ta>
            <ta e="T45" id="Seg_1319" s="T44">n</ta>
            <ta e="T46" id="Seg_1320" s="T45">v</ta>
            <ta e="T47" id="Seg_1321" s="T46">n</ta>
            <ta e="T48" id="Seg_1322" s="T47">v</ta>
            <ta e="T49" id="Seg_1323" s="T48">v</ta>
            <ta e="T50" id="Seg_1324" s="T49">n</ta>
            <ta e="T51" id="Seg_1325" s="T50">n</ta>
            <ta e="T52" id="Seg_1326" s="T51">n</ta>
            <ta e="T53" id="Seg_1327" s="T52">v</ta>
            <ta e="T54" id="Seg_1328" s="T53">n</ta>
            <ta e="T55" id="Seg_1329" s="T54">v</ta>
            <ta e="T56" id="Seg_1330" s="T55">dempro</ta>
            <ta e="T57" id="Seg_1331" s="T56">n</ta>
            <ta e="T58" id="Seg_1332" s="T57">v</ta>
            <ta e="T59" id="Seg_1333" s="T58">n</ta>
            <ta e="T61" id="Seg_1334" s="T60">n</ta>
            <ta e="T62" id="Seg_1335" s="T61">v</ta>
            <ta e="T63" id="Seg_1336" s="T62">n</ta>
            <ta e="T64" id="Seg_1337" s="T63">v</ta>
            <ta e="T65" id="Seg_1338" s="T64">n</ta>
            <ta e="T66" id="Seg_1339" s="T65">v</ta>
            <ta e="T67" id="Seg_1340" s="T66">v</ta>
            <ta e="T68" id="Seg_1341" s="T67">n</ta>
            <ta e="T69" id="Seg_1342" s="T68">v</ta>
            <ta e="T70" id="Seg_1343" s="T69">pers</ta>
            <ta e="T71" id="Seg_1344" s="T70">n</ta>
            <ta e="T72" id="Seg_1345" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_1346" s="T72">v</ta>
            <ta e="T74" id="Seg_1347" s="T73">n</ta>
            <ta e="T75" id="Seg_1348" s="T74">quant</ta>
            <ta e="T76" id="Seg_1349" s="T75">v</ta>
            <ta e="T77" id="Seg_1350" s="T76">n</ta>
            <ta e="T78" id="Seg_1351" s="T77">v</ta>
            <ta e="T79" id="Seg_1352" s="T78">n</ta>
            <ta e="T80" id="Seg_1353" s="T79">v</ta>
            <ta e="T81" id="Seg_1354" s="T80">n</ta>
            <ta e="T82" id="Seg_1355" s="T81">v</ta>
            <ta e="T83" id="Seg_1356" s="T82">v</ta>
            <ta e="T84" id="Seg_1357" s="T83">n</ta>
            <ta e="T85" id="Seg_1358" s="T84">v</ta>
            <ta e="T86" id="Seg_1359" s="T85">n</ta>
            <ta e="T87" id="Seg_1360" s="T86">v</ta>
            <ta e="T88" id="Seg_1361" s="T87">n</ta>
            <ta e="T89" id="Seg_1362" s="T88">v</ta>
            <ta e="T90" id="Seg_1363" s="T89">n</ta>
            <ta e="T91" id="Seg_1364" s="T90">v</ta>
            <ta e="T92" id="Seg_1365" s="T91">adj</ta>
            <ta e="T93" id="Seg_1366" s="T92">n</ta>
            <ta e="T94" id="Seg_1367" s="T93">n</ta>
            <ta e="T95" id="Seg_1368" s="T94">v</ta>
            <ta e="T96" id="Seg_1369" s="T95">v</ta>
            <ta e="T97" id="Seg_1370" s="T96">v</ta>
            <ta e="T98" id="Seg_1371" s="T97">v</ta>
            <ta e="T99" id="Seg_1372" s="T98">v</ta>
            <ta e="T100" id="Seg_1373" s="T99">adj</ta>
            <ta e="T101" id="Seg_1374" s="T100">pers</ta>
            <ta e="T102" id="Seg_1375" s="T101">adv</ta>
            <ta e="T103" id="Seg_1376" s="T102">v</ta>
            <ta e="T104" id="Seg_1377" s="T103">n</ta>
            <ta e="T105" id="Seg_1378" s="T104">v</ta>
            <ta e="T106" id="Seg_1379" s="T105">n</ta>
            <ta e="T107" id="Seg_1380" s="T106">v</ta>
            <ta e="T108" id="Seg_1381" s="T107">dempro</ta>
            <ta e="T109" id="Seg_1382" s="T108">adj</ta>
            <ta e="T110" id="Seg_1383" s="T109">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_1384" s="T0">np:A</ta>
            <ta e="T3" id="Seg_1385" s="T2">np:A</ta>
            <ta e="T4" id="Seg_1386" s="T3">np:A</ta>
            <ta e="T7" id="Seg_1387" s="T6">np:Th</ta>
            <ta e="T10" id="Seg_1388" s="T9">0.3:E</ta>
            <ta e="T11" id="Seg_1389" s="T10">np:P</ta>
            <ta e="T12" id="Seg_1390" s="T11">0.3:A</ta>
            <ta e="T13" id="Seg_1391" s="T12">0.3:Th</ta>
            <ta e="T14" id="Seg_1392" s="T13">np:A</ta>
            <ta e="T16" id="Seg_1393" s="T15">np:Th</ta>
            <ta e="T18" id="Seg_1394" s="T17">np:P</ta>
            <ta e="T19" id="Seg_1395" s="T18">np:G</ta>
            <ta e="T21" id="Seg_1396" s="T20">np:P</ta>
            <ta e="T22" id="Seg_1397" s="T21">np:A</ta>
            <ta e="T24" id="Seg_1398" s="T23">0.3:A</ta>
            <ta e="T26" id="Seg_1399" s="T25">0.3:A</ta>
            <ta e="T27" id="Seg_1400" s="T26">np:Th</ta>
            <ta e="T31" id="Seg_1401" s="T29">pp:Pth</ta>
            <ta e="T32" id="Seg_1402" s="T31">0.3:A</ta>
            <ta e="T33" id="Seg_1403" s="T32">np:Th</ta>
            <ta e="T36" id="Seg_1404" s="T35">0.3:A</ta>
            <ta e="T38" id="Seg_1405" s="T37">np:Th</ta>
            <ta e="T39" id="Seg_1406" s="T38">0.3.h:A</ta>
            <ta e="T40" id="Seg_1407" s="T39">np:G</ta>
            <ta e="T41" id="Seg_1408" s="T40">0.3:A</ta>
            <ta e="T43" id="Seg_1409" s="T41">pp:Pth</ta>
            <ta e="T44" id="Seg_1410" s="T43">0.3:E</ta>
            <ta e="T45" id="Seg_1411" s="T44">np:Th</ta>
            <ta e="T47" id="Seg_1412" s="T46">np.h:A</ta>
            <ta e="T50" id="Seg_1413" s="T49">np:A</ta>
            <ta e="T52" id="Seg_1414" s="T51">np.h:A 0.2.h:Poss</ta>
            <ta e="T54" id="Seg_1415" s="T53">np:P</ta>
            <ta e="T55" id="Seg_1416" s="T54">0.2.h:A</ta>
            <ta e="T57" id="Seg_1417" s="T56">np.h:A</ta>
            <ta e="T59" id="Seg_1418" s="T58">np:G 0.3.h:Poss</ta>
            <ta e="T60" id="Seg_1419" s="T59">0.3.h:A</ta>
            <ta e="T61" id="Seg_1420" s="T60">np.h:A 0.1.h:Poss</ta>
            <ta e="T63" id="Seg_1421" s="T62">np:P</ta>
            <ta e="T64" id="Seg_1422" s="T63">0.2.h:A</ta>
            <ta e="T67" id="Seg_1423" s="T66">0.2.h:Poss</ta>
            <ta e="T68" id="Seg_1424" s="T67">np:Th</ta>
            <ta e="T70" id="Seg_1425" s="T69">pro.h:A</ta>
            <ta e="T71" id="Seg_1426" s="T70">np:P</ta>
            <ta e="T74" id="Seg_1427" s="T73">np:Th 0.3:Poss</ta>
            <ta e="T76" id="Seg_1428" s="T75">0.1.h:E</ta>
            <ta e="T77" id="Seg_1429" s="T76">np:Th 0.3.h:Poss</ta>
            <ta e="T78" id="Seg_1430" s="T77">0.3.h:A</ta>
            <ta e="T79" id="Seg_1431" s="T78">np:P 0.3.h:Poss</ta>
            <ta e="T80" id="Seg_1432" s="T79">0.3.h:A</ta>
            <ta e="T81" id="Seg_1433" s="T80">np:P</ta>
            <ta e="T82" id="Seg_1434" s="T81">0.3.h:A</ta>
            <ta e="T83" id="Seg_1435" s="T82">0.2.h:A</ta>
            <ta e="T84" id="Seg_1436" s="T83">np.h:Th 0.2.h:Poss</ta>
            <ta e="T85" id="Seg_1437" s="T84">0.2.h:A 0.3.h:Th</ta>
            <ta e="T86" id="Seg_1438" s="T85">np:Th</ta>
            <ta e="T89" id="Seg_1439" s="T88">0.2.h:A</ta>
            <ta e="T90" id="Seg_1440" s="T89">np:Th</ta>
            <ta e="T93" id="Seg_1441" s="T92">np:Poss</ta>
            <ta e="T94" id="Seg_1442" s="T93">np:P</ta>
            <ta e="T95" id="Seg_1443" s="T94">0.2.h:A</ta>
            <ta e="T96" id="Seg_1444" s="T95">0.1.h:A</ta>
            <ta e="T97" id="Seg_1445" s="T96">0.2.h:A</ta>
            <ta e="T99" id="Seg_1446" s="T98">0.2.h:A 0.3.h:Th</ta>
            <ta e="T100" id="Seg_1447" s="T99">0.3:Th</ta>
            <ta e="T101" id="Seg_1448" s="T100">pro.h:Th</ta>
            <ta e="T102" id="Seg_1449" s="T101">adv:L</ta>
            <ta e="T103" id="Seg_1450" s="T102">cop</ta>
            <ta e="T104" id="Seg_1451" s="T103">np.h:E</ta>
            <ta e="T106" id="Seg_1452" s="T105">np:Th</ta>
            <ta e="T107" id="Seg_1453" s="T106">0.3.h:A</ta>
            <ta e="T108" id="Seg_1454" s="T107">pro.h:P</ta>
            <ta e="T110" id="Seg_1455" s="T109">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_1456" s="T0">np:S</ta>
            <ta e="T3" id="Seg_1457" s="T2">np:S</ta>
            <ta e="T4" id="Seg_1458" s="T3">np:S</ta>
            <ta e="T5" id="Seg_1459" s="T4">s:purp</ta>
            <ta e="T6" id="Seg_1460" s="T5">v:pred</ta>
            <ta e="T7" id="Seg_1461" s="T6">np:S</ta>
            <ta e="T8" id="Seg_1462" s="T7">v:pred</ta>
            <ta e="T9" id="Seg_1463" s="T8">s:purp</ta>
            <ta e="T10" id="Seg_1464" s="T9">v:pred 0.3:S</ta>
            <ta e="T11" id="Seg_1465" s="T10">np:O</ta>
            <ta e="T12" id="Seg_1466" s="T11">v:pred 0.3:S</ta>
            <ta e="T13" id="Seg_1467" s="T12">v:pred 0.3:S</ta>
            <ta e="T14" id="Seg_1468" s="T13">np:S</ta>
            <ta e="T15" id="Seg_1469" s="T14">v:pred</ta>
            <ta e="T16" id="Seg_1470" s="T15">np:S</ta>
            <ta e="T17" id="Seg_1471" s="T16">v:pred</ta>
            <ta e="T18" id="Seg_1472" s="T17">np:S</ta>
            <ta e="T20" id="Seg_1473" s="T19">v:pred</ta>
            <ta e="T21" id="Seg_1474" s="T20">v:pred 0.3:S</ta>
            <ta e="T22" id="Seg_1475" s="T21">np:S</ta>
            <ta e="T23" id="Seg_1476" s="T22">v:pred</ta>
            <ta e="T24" id="Seg_1477" s="T23">v:pred 0.3:S</ta>
            <ta e="T25" id="Seg_1478" s="T24">conv:pred</ta>
            <ta e="T26" id="Seg_1479" s="T25">v:pred 0.3:S</ta>
            <ta e="T27" id="Seg_1480" s="T26">np:S</ta>
            <ta e="T29" id="Seg_1481" s="T28">v:pred</ta>
            <ta e="T32" id="Seg_1482" s="T31">v:pred 0.3:S</ta>
            <ta e="T33" id="Seg_1483" s="T32">np:S</ta>
            <ta e="T35" id="Seg_1484" s="T34">v:pred</ta>
            <ta e="T36" id="Seg_1485" s="T35">v:pred 0.3:S</ta>
            <ta e="T38" id="Seg_1486" s="T37">np:O</ta>
            <ta e="T39" id="Seg_1487" s="T38">v:pred 0.3:S</ta>
            <ta e="T41" id="Seg_1488" s="T40">v:pred 0.3:S</ta>
            <ta e="T44" id="Seg_1489" s="T43">s:temp</ta>
            <ta e="T45" id="Seg_1490" s="T44">np:S</ta>
            <ta e="T46" id="Seg_1491" s="T45">v:pred</ta>
            <ta e="T47" id="Seg_1492" s="T46">np.h:S</ta>
            <ta e="T48" id="Seg_1493" s="T47">v:pred</ta>
            <ta e="T49" id="Seg_1494" s="T48">v:pred</ta>
            <ta e="T50" id="Seg_1495" s="T49">np:S</ta>
            <ta e="T52" id="Seg_1496" s="T51">np.h:S</ta>
            <ta e="T53" id="Seg_1497" s="T52">v:pred</ta>
            <ta e="T54" id="Seg_1498" s="T53">np:O</ta>
            <ta e="T55" id="Seg_1499" s="T54">v:pred 0.2.h:S</ta>
            <ta e="T57" id="Seg_1500" s="T56">np.h:S</ta>
            <ta e="T58" id="Seg_1501" s="T57">v:pred</ta>
            <ta e="T60" id="Seg_1502" s="T59">v:pred 0.3.h:S</ta>
            <ta e="T61" id="Seg_1503" s="T60">np.h:S</ta>
            <ta e="T62" id="Seg_1504" s="T61">v:pred</ta>
            <ta e="T63" id="Seg_1505" s="T62">np:O</ta>
            <ta e="T64" id="Seg_1506" s="T63">v:pred 0.2.h:S</ta>
            <ta e="T66" id="Seg_1507" s="T65">conv:pred</ta>
            <ta e="T67" id="Seg_1508" s="T66">v:pred 0.2.h:S</ta>
            <ta e="T68" id="Seg_1509" s="T67">np:S</ta>
            <ta e="T69" id="Seg_1510" s="T68">v:pred</ta>
            <ta e="T70" id="Seg_1511" s="T69">pro.h:S</ta>
            <ta e="T71" id="Seg_1512" s="T70">np:O</ta>
            <ta e="T72" id="Seg_1513" s="T71">ptcl.neg</ta>
            <ta e="T73" id="Seg_1514" s="T72">v:pred</ta>
            <ta e="T74" id="Seg_1515" s="T73">np:S</ta>
            <ta e="T75" id="Seg_1516" s="T74">adj:pred</ta>
            <ta e="T76" id="Seg_1517" s="T75">v:pred 0.1.h:S</ta>
            <ta e="T77" id="Seg_1518" s="T76">np:O</ta>
            <ta e="T78" id="Seg_1519" s="T77">v:pred 0.3.h:S</ta>
            <ta e="T79" id="Seg_1520" s="T78">np:O</ta>
            <ta e="T80" id="Seg_1521" s="T79">v:pred 0.3.h:S</ta>
            <ta e="T81" id="Seg_1522" s="T80">np:O</ta>
            <ta e="T82" id="Seg_1523" s="T81">v:pred 0.3.h:S</ta>
            <ta e="T83" id="Seg_1524" s="T82">v:pred 0.2.h:S</ta>
            <ta e="T84" id="Seg_1525" s="T83">np.h:O</ta>
            <ta e="T85" id="Seg_1526" s="T84">0.2.h:S 0.3.h:O v:pred </ta>
            <ta e="T86" id="Seg_1527" s="T85">np:S</ta>
            <ta e="T87" id="Seg_1528" s="T86">v:pred</ta>
            <ta e="T89" id="Seg_1529" s="T88">v:pred 0.2.h:S</ta>
            <ta e="T90" id="Seg_1530" s="T89">np:S</ta>
            <ta e="T91" id="Seg_1531" s="T90">v:pred</ta>
            <ta e="T94" id="Seg_1532" s="T93">np:O</ta>
            <ta e="T95" id="Seg_1533" s="T94">v:pred 0.2.h:S</ta>
            <ta e="T96" id="Seg_1534" s="T95">v:pred 0.1.h:S</ta>
            <ta e="T97" id="Seg_1535" s="T96">v:pred 0.2.h:S</ta>
            <ta e="T98" id="Seg_1536" s="T97">conv:pred</ta>
            <ta e="T99" id="Seg_1537" s="T98">v:pred 0.2.h:S 0.3.h:O</ta>
            <ta e="T101" id="Seg_1538" s="T100">pro.h:S</ta>
            <ta e="T103" id="Seg_1539" s="T102">v:pred</ta>
            <ta e="T104" id="Seg_1540" s="T103">np.h:S</ta>
            <ta e="T105" id="Seg_1541" s="T104">v:pred</ta>
            <ta e="T106" id="Seg_1542" s="T105">np:O</ta>
            <ta e="T107" id="Seg_1543" s="T106">0.3.h:S v:pred</ta>
            <ta e="T108" id="Seg_1544" s="T107">pro.h:O</ta>
            <ta e="T110" id="Seg_1545" s="T109">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T1" id="Seg_1546" s="T0">new </ta>
            <ta e="T3" id="Seg_1547" s="T2">new </ta>
            <ta e="T4" id="Seg_1548" s="T3">new </ta>
            <ta e="T7" id="Seg_1549" s="T6">new </ta>
            <ta e="T10" id="Seg_1550" s="T9">0.accs-aggr</ta>
            <ta e="T11" id="Seg_1551" s="T10">new </ta>
            <ta e="T12" id="Seg_1552" s="T11">0.giv-active</ta>
            <ta e="T13" id="Seg_1553" s="T12">0.giv-active</ta>
            <ta e="T14" id="Seg_1554" s="T13">giv-inactive </ta>
            <ta e="T16" id="Seg_1555" s="T15">giv-active </ta>
            <ta e="T18" id="Seg_1556" s="T17">giv-active </ta>
            <ta e="T19" id="Seg_1557" s="T18">accs-sit </ta>
            <ta e="T21" id="Seg_1558" s="T20">0.giv-active</ta>
            <ta e="T22" id="Seg_1559" s="T21">giv-inactive </ta>
            <ta e="T26" id="Seg_1560" s="T25">0.giv-active</ta>
            <ta e="T27" id="Seg_1561" s="T26">giv-inactive </ta>
            <ta e="T30" id="Seg_1562" s="T29">giv-inactive </ta>
            <ta e="T32" id="Seg_1563" s="T31">0.giv-active</ta>
            <ta e="T33" id="Seg_1564" s="T32">new </ta>
            <ta e="T36" id="Seg_1565" s="T35">0.giv-inactive</ta>
            <ta e="T38" id="Seg_1566" s="T37">giv-active </ta>
            <ta e="T39" id="Seg_1567" s="T38">0.giv-active</ta>
            <ta e="T40" id="Seg_1568" s="T39">giv-active </ta>
            <ta e="T41" id="Seg_1569" s="T40">0.giv-active</ta>
            <ta e="T42" id="Seg_1570" s="T41">giv-inactive </ta>
            <ta e="T44" id="Seg_1571" s="T43">0.giv-active</ta>
            <ta e="T45" id="Seg_1572" s="T44">new </ta>
            <ta e="T47" id="Seg_1573" s="T46">new </ta>
            <ta e="T49" id="Seg_1574" s="T48">quot-sp</ta>
            <ta e="T50" id="Seg_1575" s="T49">giv-inactive </ta>
            <ta e="T51" id="Seg_1576" s="T50">giv-inactive-Q </ta>
            <ta e="T52" id="Seg_1577" s="T51">accs-gen-Q</ta>
            <ta e="T54" id="Seg_1578" s="T53">new-Q </ta>
            <ta e="T55" id="Seg_1579" s="T54">0.giv-active</ta>
            <ta e="T57" id="Seg_1580" s="T56">giv-inactive </ta>
            <ta e="T59" id="Seg_1581" s="T58">new</ta>
            <ta e="T60" id="Seg_1582" s="T59">quot-sp</ta>
            <ta e="T61" id="Seg_1583" s="T60">giv-inactive-Q </ta>
            <ta e="T63" id="Seg_1584" s="T62">giv-inactive-Q</ta>
            <ta e="T65" id="Seg_1585" s="T64">giv-inactive-Q </ta>
            <ta e="T67" id="Seg_1586" s="T66">0.giv-active-Q</ta>
            <ta e="T68" id="Seg_1587" s="T67">new-Q</ta>
            <ta e="T70" id="Seg_1588" s="T69">giv-inactive-Q </ta>
            <ta e="T71" id="Seg_1589" s="T70">giv-active-Q </ta>
            <ta e="T74" id="Seg_1590" s="T73">accs-inf-Q </ta>
            <ta e="T76" id="Seg_1591" s="T75">0.giv-active-Q</ta>
            <ta e="T77" id="Seg_1592" s="T76">new </ta>
            <ta e="T78" id="Seg_1593" s="T77">0.giv-inactive</ta>
            <ta e="T79" id="Seg_1594" s="T78">new </ta>
            <ta e="T80" id="Seg_1595" s="T79">0.giv-active</ta>
            <ta e="T81" id="Seg_1596" s="T80">accs-inf </ta>
            <ta e="T83" id="Seg_1597" s="T82">0.giv-active-Q</ta>
            <ta e="T84" id="Seg_1598" s="T83">giv-inactive-Q </ta>
            <ta e="T86" id="Seg_1599" s="T85">giv-inactive-Q </ta>
            <ta e="T88" id="Seg_1600" s="T87">giv-active-Q </ta>
            <ta e="T90" id="Seg_1601" s="T89">giv-active-Q </ta>
            <ta e="T94" id="Seg_1602" s="T93">new-Q </ta>
            <ta e="T95" id="Seg_1603" s="T94">0.giv-active-Q</ta>
            <ta e="T96" id="Seg_1604" s="T95">0.giv-active-Q</ta>
            <ta e="T97" id="Seg_1605" s="T96">0.giv-active-Q</ta>
            <ta e="T99" id="Seg_1606" s="T98">0.giv-active-Q</ta>
            <ta e="T100" id="Seg_1607" s="T99">0.giv-active-Q</ta>
            <ta e="T101" id="Seg_1608" s="T100">giv-active-Q </ta>
            <ta e="T104" id="Seg_1609" s="T103">giv-inactive </ta>
            <ta e="T106" id="Seg_1610" s="T105">new </ta>
            <ta e="T107" id="Seg_1611" s="T106">0.giv-active</ta>
            <ta e="T108" id="Seg_1612" s="T107">giv-active </ta>
            <ta e="T110" id="Seg_1613" s="T109">0.giv-active</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_1614" s="T1">RUS:gram</ta>
            <ta e="T79" id="Seg_1615" s="T78">TURK:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_1616" s="T0">Мышка, уголёк и пузырь пошли на охоту.</ta>
            <ta e="T10" id="Seg_1617" s="T6">Показалась река, захотели они её перейти.</ta>
            <ta e="T13" id="Seg_1618" s="T10">Соломинку сломали, она стала мостиком.</ta>
            <ta e="T17" id="Seg_1619" s="T13">Уголёк по соломинке пошёл, она загорелась. </ta>
            <ta e="T21" id="Seg_1620" s="T17">Упал уголёк в воду и потух.</ta>
            <ta e="T26" id="Seg_1621" s="T21">Пузырь смеялся, смеялся и лопнул.</ta>
            <ta e="T29" id="Seg_1622" s="T26">Осталась мышка одна.</ta>
            <ta e="T32" id="Seg_1623" s="T29">Вдоль реки пошла.</ta>
            <ta e="T35" id="Seg_1624" s="T32">Ложку сломанную нашла.</ta>
            <ta e="T43" id="Seg_1625" s="T35">Взяла эту ложку, села в неё, поплыла вниз по реке.</ta>
            <ta e="T48" id="Seg_1626" s="T43">Смотрит, стоит чум, дети играют.</ta>
            <ta e="T50" id="Seg_1627" s="T48">Закричала мышка: </ta>
            <ta e="T53" id="Seg_1628" s="T50">"Дети, дедушка идёт!</ta>
            <ta e="T55" id="Seg_1629" s="T53">Мясо варите!"</ta>
            <ta e="T60" id="Seg_1630" s="T55">Дети побежали к матери и сказали:</ta>
            <ta e="T62" id="Seg_1631" s="T60">"Наш дедушка идёт. </ta>
            <ta e="T65" id="Seg_1632" s="T62">Вари мясо, мама!"</ta>
            <ta e="T69" id="Seg_1633" s="T65">"Идите зовите его, рыба готова".</ta>
            <ta e="T73" id="Seg_1634" s="T69">"Не хочу я рыбу есть.</ta>
            <ta e="T76" id="Seg_1635" s="T73">В ней костей много, я подавлюсь".</ta>
            <ta e="T82" id="Seg_1636" s="T76">Та взяла топор, корову зарубила, сварила мясо.</ta>
            <ta e="T84" id="Seg_1637" s="T82">"Зовите дедушку. </ta>
            <ta e="T87" id="Seg_1638" s="T84">Зовите его, мясо готово."</ta>
            <ta e="T91" id="Seg_1639" s="T87">"Дедушка, заходи! Мясо готово."</ta>
            <ta e="T95" id="Seg_1640" s="T91">"Шкуру чёрную медвежью расстелите!"</ta>
            <ta e="T97" id="Seg_1641" s="T95">"Расстелили, заходи!"</ta>
            <ta e="T100" id="Seg_1642" s="T97">"Идите зовите его, постелено."</ta>
            <ta e="T103" id="Seg_1643" s="T100">"Вот и я."</ta>
            <ta e="T110" id="Seg_1644" s="T103">Увидала женщина [мышь], схватила полено и прибила её насмерть.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_1645" s="T0">A mouse, a coal and a bladder went hunting.</ta>
            <ta e="T10" id="Seg_1646" s="T6">A river came up, they wanted to cross it.</ta>
            <ta e="T13" id="Seg_1647" s="T10">They broke a straw which served as a bridge.</ta>
            <ta e="T17" id="Seg_1648" s="T13">The coal was going across when the blade burned off.</ta>
            <ta e="T21" id="Seg_1649" s="T17">The coal fell into the water and fizzled.</ta>
            <ta e="T26" id="Seg_1650" s="T21">The bladder laughed, laughed and burst.</ta>
            <ta e="T29" id="Seg_1651" s="T26">The mouse remained alone.</ta>
            <ta e="T32" id="Seg_1652" s="T29">It went along the river.</ta>
            <ta e="T35" id="Seg_1653" s="T32">A broken spoon came up.</ta>
            <ta e="T43" id="Seg_1654" s="T35">It took this spoon, sat into the spoon, started floating down the river.</ta>
            <ta e="T48" id="Seg_1655" s="T43">When it looked there was a tent standing, children were playing.</ta>
            <ta e="T50" id="Seg_1656" s="T48">The mouse shouted:</ta>
            <ta e="T53" id="Seg_1657" s="T50">"Children, grandfather is coming.</ta>
            <ta e="T55" id="Seg_1658" s="T53">Cook meat!"</ta>
            <ta e="T60" id="Seg_1659" s="T55">The children ran to their mother and said:</ta>
            <ta e="T62" id="Seg_1660" s="T60">"Our grandfather is coming.</ta>
            <ta e="T65" id="Seg_1661" s="T62">Cook meat, mother!"</ta>
            <ta e="T69" id="Seg_1662" s="T65">"Go and invite him, the fish is ready."</ta>
            <ta e="T73" id="Seg_1663" s="T69">"I don’t want to eat fish.</ta>
            <ta e="T76" id="Seg_1664" s="T73">It has many bones, I will choke.</ta>
            <ta e="T82" id="Seg_1665" s="T76">She took her axe, killed her cow and cooked the meat.</ta>
            <ta e="T84" id="Seg_1666" s="T82">"Invite your grandfather.</ta>
            <ta e="T87" id="Seg_1667" s="T84">Invite him, the meat is ready."</ta>
            <ta e="T91" id="Seg_1668" s="T87">"Grandfather, come! The meat is done.</ta>
            <ta e="T95" id="Seg_1669" s="T91">"Spread out a black bear’s skin!"</ta>
            <ta e="T97" id="Seg_1670" s="T95">"We spread it out, come!"</ta>
            <ta e="T100" id="Seg_1671" s="T97">"Go and invite him, it’s spread out."</ta>
            <ta e="T103" id="Seg_1672" s="T100">"I’m here."</ta>
            <ta e="T110" id="Seg_1673" s="T103">The woman saw [the mouse], took a piece of wood and hit it dead.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_1674" s="T0">Eine Maus, eine Kohle und eine Blase gingen jagen im Wald.</ta>
            <ta e="T10" id="Seg_1675" s="T6">Sie kamen an einen Fluss und wollten ihn überqueren.</ta>
            <ta e="T13" id="Seg_1676" s="T10">Sie brachen einen Strohhalm ab, der wurde zu einer Brücke.</ta>
            <ta e="T17" id="Seg_1677" s="T13">Die Kohle ging gerade hinüber, als der Grashalm anfing zu brennen.</ta>
            <ta e="T21" id="Seg_1678" s="T17">Die Kohle fiel ins Wasser und zischte.</ta>
            <ta e="T26" id="Seg_1679" s="T21">Die Blase lachte, lachte und platzte. </ta>
            <ta e="T29" id="Seg_1680" s="T26">Die Maus blieb allein.</ta>
            <ta e="T32" id="Seg_1681" s="T29">Sie ging den Fluss entlang.</ta>
            <ta e="T35" id="Seg_1682" s="T32">Sie kam zu einem zerbrochenen Löffel.</ta>
            <ta e="T43" id="Seg_1683" s="T35">Sie nahm den Löffel, setzte sich in den Löffel hinein und begann flussabwärts zu treiben.</ta>
            <ta e="T48" id="Seg_1684" s="T43">Als sie sich umschaute, stand da ein Zelt. Kinder spielten da.</ta>
            <ta e="T50" id="Seg_1685" s="T48">Die Maus schrie:</ta>
            <ta e="T53" id="Seg_1686" s="T50">"Kinder, euer Großvater kommt.</ta>
            <ta e="T55" id="Seg_1687" s="T53">Kocht Fleisch!"</ta>
            <ta e="T60" id="Seg_1688" s="T55">Die Kinder liefen zu ihrer Mutter und sagten:</ta>
            <ta e="T62" id="Seg_1689" s="T60">"Unser Großvater kommt.</ta>
            <ta e="T65" id="Seg_1690" s="T62">Koch Fleisch, Mutter!"</ta>
            <ta e="T69" id="Seg_1691" s="T65">"Geht ihn rufen, der Fisch ist fertig."</ta>
            <ta e="T73" id="Seg_1692" s="T69">„Ich will keinen Fisch essen!</ta>
            <ta e="T76" id="Seg_1693" s="T73">Er hat viele Gräten, ich werde daran ersticken."</ta>
            <ta e="T82" id="Seg_1694" s="T76">Sie griff nach ihrer Axt, schlachtete ihre Kuh und kochte das Fleisch.</ta>
            <ta e="T84" id="Seg_1695" s="T82">"Ruft euren Großvater.</ta>
            <ta e="T87" id="Seg_1696" s="T84">Ruft ihn, das Fleisch ist fertig."</ta>
            <ta e="T91" id="Seg_1697" s="T87">"Großvater, komm! Das Fleisch ist fertig."</ta>
            <ta e="T95" id="Seg_1698" s="T91">"Breitet ein Schwarzbärenfell aus!"</ta>
            <ta e="T97" id="Seg_1699" s="T95">"Wir haben [eins] ausgebreitet. Komm!"</ta>
            <ta e="T100" id="Seg_1700" s="T97">"Geht und ruft ihn. Es ist ausgebreitet."</ta>
            <ta e="T103" id="Seg_1701" s="T100">"Hier bin ich."</ta>
            <ta e="T110" id="Seg_1702" s="T103">Die Frau sah [die Maus], griff nach einem [Stück] Holz und erschlug die Maus.</ta>
         </annotation>
         <annotation name="ltg" tierref="ltg">
            <ta e="T6" id="Seg_1703" s="T0">Eine Maus und eine Kohle (und) eine Blase gingen jagen.</ta>
            <ta e="T10" id="Seg_1704" s="T6">Ein Fluss kam ihnen entgegen, sie wollen hinübergehen.</ta>
            <ta e="T13" id="Seg_1705" s="T10">Sie brachen einen Strohhalm ab, machten eine Brücke.</ta>
            <ta e="T17" id="Seg_1706" s="T13">Die Kohle begann hinüberzugehen, der Halm brannte ab.</ta>
            <ta e="T21" id="Seg_1707" s="T17">Die Kohle fiel ins Wasser, erlosch.</ta>
            <ta e="T26" id="Seg_1708" s="T21">Als die Blase lachte, lachte, platzte sie.</ta>
            <ta e="T29" id="Seg_1709" s="T26">Die Maus blieb allein.</ta>
            <ta e="T32" id="Seg_1710" s="T29">Machte sich auf den Weg, den Fluss entlang (abwärts).</ta>
            <ta e="T35" id="Seg_1711" s="T32">Einen zerbrochenen Löffel fand sie,</ta>
            <ta e="T43" id="Seg_1712" s="T35">nahm den Löffel, setzte sich in den Löffel, ging abwärts mit dem Strome.</ta>
            <ta e="T48" id="Seg_1713" s="T43">Als sie schaute: ein Zelt steht [da]. Die Kinder spielen. </ta>
            <ta e="T50" id="Seg_1714" s="T48">Schrie die Maus:</ta>
            <ta e="T53" id="Seg_1715" s="T50">»Kinder, euer Grossvater kommt.</ta>
            <ta e="T55" id="Seg_1716" s="T53">Kochet Fleisch!»</ta>
            <ta e="T60" id="Seg_1717" s="T55">Die Kinder schritten zu ihrer Mutter, sagten:</ta>
            <ta e="T62" id="Seg_1718" s="T60">»Unser Grossvater kommt.</ta>
            <ta e="T65" id="Seg_1719" s="T62">Tue Fleisch (in den Topf)!» </ta>
            <ta e="T69" id="Seg_1720" s="T65">»Gehet einladen, der Fisch wurde gar»</ta>
            <ta e="T73" id="Seg_1721" s="T69">»Ich will nicht Fisch essen,</ta>
            <ta e="T76" id="Seg_1722" s="T73">⌈seine⌉ Gräten sind viel, ich ersticke».</ta>
            <ta e="T82" id="Seg_1723" s="T76">Die Axt sie ergriff, erschlug ihre Kuh, kochte Fleisch.</ta>
            <ta e="T84" id="Seg_1724" s="T82">»Ladet euren Grossvater ein,</ta>
            <ta e="T87" id="Seg_1725" s="T84">ladet ihn ein! Das Fleisch wurde gar.»</ta>
            <ta e="T91" id="Seg_1726" s="T87">»Grossvater, komm! Das Fleisch wurde gar.»</ta>
            <ta e="T95" id="Seg_1727" s="T91">»Ein schwarzes Bärenfell doch breitet aus!»</ta>
            <ta e="T97" id="Seg_1728" s="T95">»Wir breiteten [es] aus. Komm,</ta>
            <ta e="T100" id="Seg_1729" s="T97">gehend ladet ein, [es ist] ausgebreitet.»</ta>
            <ta e="T103" id="Seg_1730" s="T100">»Hier bin ich.»</ta>
            <ta e="T110" id="Seg_1731" s="T103">Das Weib sah, ergriff ein Holz. Sie schlug [die Maus tot.]</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltg"
                          display-name="ltg"
                          name="ltg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
