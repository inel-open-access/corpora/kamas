<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID0D77F8D1-23C9-A680-6B12-DF69BCF9CCCA">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\AIN_1914_Trickster_flk\AIN_1914_Trickster_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">214</ud-information>
            <ud-information attribute-name="# HIAT:w">143</ud-information>
            <ud-information attribute-name="# e">143</ud-information>
            <ud-information attribute-name="# HIAT:u">45</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="AIN">
            <abbreviation>AIN</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="AIN"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T143" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Nüke</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">büzʼetsʼəʔ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">amnobi</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T5" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Najdʼžʼədən</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">ibi</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_23" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_25" n="HIAT:w" s="T5">Dĭ</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">jadajlaʔ</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">šobi</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_35" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_37" n="HIAT:w" s="T8">Nüket</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_40" n="HIAT:w" s="T9">büjleʔ</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">öʔliedən</ts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_47" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_49" n="HIAT:w" s="T11">Nüket</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_52" n="HIAT:w" s="T12">ej</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_55" n="HIAT:w" s="T13">kallia</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_59" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_61" n="HIAT:w" s="T14">Dĭgəttə</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_64" n="HIAT:w" s="T15">kamdʼžʼu</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_67" n="HIAT:w" s="T16">kabarbi</ts>
                  <nts id="Seg_68" n="HIAT:ip">,</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_71" n="HIAT:w" s="T17">iʔdəbi</ts>
                  <nts id="Seg_72" n="HIAT:ip">.</nts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_75" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_77" n="HIAT:w" s="T18">Tagaj</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_80" n="HIAT:w" s="T19">kabarbi</ts>
                  <nts id="Seg_81" n="HIAT:ip">.</nts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_84" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_86" n="HIAT:w" s="T20">Nanʼabə</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_89" n="HIAT:w" s="T21">păktəj</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_92" n="HIAT:w" s="T22">müʔlüʔbi</ts>
                  <nts id="Seg_93" n="HIAT:ip">.</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_96" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_98" n="HIAT:w" s="T23">Nanʼaandə</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_101" n="HIAT:w" s="T24">kem</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_104" n="HIAT:w" s="T25">sarbi</ts>
                  <nts id="Seg_105" n="HIAT:ip">.</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_108" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_110" n="HIAT:w" s="T26">Kem</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_113" n="HIAT:w" s="T27">mʼaŋbi</ts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_117" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_119" n="HIAT:w" s="T28">Aspaʔdə</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_122" n="HIAT:w" s="T29">kabarbi</ts>
                  <nts id="Seg_123" n="HIAT:ip">,</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_126" n="HIAT:w" s="T30">büjleʔ</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_129" n="HIAT:w" s="T31">üʔməlüʔbi</ts>
                  <nts id="Seg_130" n="HIAT:ip">.</nts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_133" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_135" n="HIAT:w" s="T32">Bü</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_138" n="HIAT:w" s="T33">deppi</ts>
                  <nts id="Seg_139" n="HIAT:ip">,</nts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_142" n="HIAT:w" s="T34">aspaʔdə</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_145" n="HIAT:w" s="T35">mĭnzərluʔbi</ts>
                  <nts id="Seg_146" n="HIAT:ip">.</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_149" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_151" n="HIAT:w" s="T36">Dĭ</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_154" n="HIAT:w" s="T37">kuza</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_157" n="HIAT:w" s="T38">parluʔbi</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_160" n="HIAT:w" s="T39">maʔgəndə</ts>
                  <nts id="Seg_161" n="HIAT:ip">.</nts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_164" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_166" n="HIAT:w" s="T40">Maʔgəndə</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_169" n="HIAT:w" s="T41">kambiːza</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_172" n="HIAT:w" s="T42">nükeendə:</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_174" n="HIAT:ip">"</nts>
                  <ts e="T44" id="Seg_176" n="HIAT:w" s="T43">Büjleʔ</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_179" n="HIAT:w" s="T44">kanaʔ</ts>
                  <nts id="Seg_180" n="HIAT:ip">!</nts>
                  <nts id="Seg_181" n="HIAT:ip">"</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_184" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_186" n="HIAT:w" s="T45">Nüket</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_189" n="HIAT:w" s="T46">ej</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_192" n="HIAT:w" s="T47">kallia</ts>
                  <nts id="Seg_193" n="HIAT:ip">.</nts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_196" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_198" n="HIAT:w" s="T48">Dĭ</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_201" n="HIAT:w" s="T49">kamdʼžʼu</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_204" n="HIAT:w" s="T50">kabarbi</ts>
                  <nts id="Seg_205" n="HIAT:ip">,</nts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_208" n="HIAT:w" s="T51">iʔdəleibi</ts>
                  <nts id="Seg_209" n="HIAT:ip">.</nts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_212" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_214" n="HIAT:w" s="T52">Ej</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_217" n="HIAT:w" s="T53">kallia</ts>
                  <nts id="Seg_218" n="HIAT:ip">.</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_221" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_223" n="HIAT:w" s="T54">Tagaj</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_226" n="HIAT:w" s="T55">kabarbi</ts>
                  <nts id="Seg_227" n="HIAT:ip">,</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_230" n="HIAT:w" s="T56">nükebə</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_233" n="HIAT:w" s="T57">tʼăgarluʔbi</ts>
                  <nts id="Seg_234" n="HIAT:ip">.</nts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_237" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_239" n="HIAT:w" s="T58">Nüket</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_242" n="HIAT:w" s="T59">mĭŋgəldəžət</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_245" n="HIAT:w" s="T60">külaːmbi</ts>
                  <nts id="Seg_246" n="HIAT:ip">.</nts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_249" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_251" n="HIAT:w" s="T61">Kambi</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_254" n="HIAT:w" s="T62">il</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_257" n="HIAT:w" s="T63">oʔbdəzʼəttə</ts>
                  <nts id="Seg_258" n="HIAT:ip">.</nts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_261" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_263" n="HIAT:w" s="T64">Il</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_266" n="HIAT:w" s="T65">oʔbdobi</ts>
                  <nts id="Seg_267" n="HIAT:ip">,</nts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_270" n="HIAT:w" s="T66">šobi</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_273" n="HIAT:w" s="T67">idə</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_276" n="HIAT:w" s="T68">kuzanə</ts>
                  <nts id="Seg_277" n="HIAT:ip">.</nts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_280" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_282" n="HIAT:w" s="T69">Dĭ</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_285" n="HIAT:w" s="T70">šăʔlaːmbi</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_288" n="HIAT:w" s="T71">batpolanə</ts>
                  <nts id="Seg_289" n="HIAT:ip">.</nts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_292" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_294" n="HIAT:w" s="T72">Surarliadən:</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_296" n="HIAT:ip">"</nts>
                  <ts e="T74" id="Seg_298" n="HIAT:w" s="T73">Gijen</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_301" n="HIAT:w" s="T74">büzʼel</ts>
                  <nts id="Seg_302" n="HIAT:ip">?</nts>
                  <nts id="Seg_303" n="HIAT:ip">"</nts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_306" n="HIAT:u" s="T75">
                  <nts id="Seg_307" n="HIAT:ip">"</nts>
                  <ts e="T76" id="Seg_309" n="HIAT:w" s="T75">Kallaʔ</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_312" n="HIAT:w" s="T76">tʼürbi</ts>
                  <nts id="Seg_313" n="HIAT:ip">.</nts>
                  <nts id="Seg_314" n="HIAT:ip">"</nts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_317" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_319" n="HIAT:w" s="T77">Dĭ</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_322" n="HIAT:w" s="T78">kuza</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_325" n="HIAT:w" s="T79">amnəbi</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_328" n="HIAT:w" s="T80">batpolan</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_331" n="HIAT:w" s="T81">aŋgəndə</ts>
                  <nts id="Seg_332" n="HIAT:ip">.</nts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_335" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_337" n="HIAT:w" s="T82">Dĭ</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_340" n="HIAT:w" s="T83">büštözʼəʔ</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_343" n="HIAT:w" s="T84">kötengəndə</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_346" n="HIAT:w" s="T85">müʔlüʔbi</ts>
                  <nts id="Seg_347" n="HIAT:ip">.</nts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_350" n="HIAT:u" s="T86">
                  <ts e="T87" id="Seg_352" n="HIAT:w" s="T86">Dĭ</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_355" n="HIAT:w" s="T87">karəj</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_358" n="HIAT:w" s="T88">nʼeʔlüʔbi</ts>
                  <nts id="Seg_359" n="HIAT:ip">.</nts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_362" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_364" n="HIAT:w" s="T89">Kubindən</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_367" n="HIAT:w" s="T90">büštʼerleʔ</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_370" n="HIAT:w" s="T91">amna</ts>
                  <nts id="Seg_371" n="HIAT:ip">.</nts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_374" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_376" n="HIAT:w" s="T92">Dĭm</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_379" n="HIAT:w" s="T93">šüʔdəbiiʔ</ts>
                  <nts id="Seg_380" n="HIAT:ip">,</nts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_383" n="HIAT:w" s="T94">bün</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_386" n="HIAT:w" s="T95">toʔgəndə</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_389" n="HIAT:w" s="T96">kumbiiʔ</ts>
                  <nts id="Seg_390" n="HIAT:ip">.</nts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T99" id="Seg_393" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_395" n="HIAT:w" s="T97">So</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_398" n="HIAT:w" s="T98">alaʔbəjəʔ</ts>
                  <nts id="Seg_399" n="HIAT:ip">.</nts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_402" n="HIAT:u" s="T99">
                  <ts e="T100" id="Seg_404" n="HIAT:w" s="T99">Săgər</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_407" n="HIAT:w" s="T100">kuza</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_410" n="HIAT:w" s="T101">šonəbi</ts>
                  <nts id="Seg_411" n="HIAT:ip">.</nts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_414" n="HIAT:u" s="T102">
                  <ts e="T103" id="Seg_416" n="HIAT:w" s="T102">Pʼel</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_419" n="HIAT:w" s="T103">simabə</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_422" n="HIAT:w" s="T104">kajlabaʔbi</ts>
                  <nts id="Seg_423" n="HIAT:ip">.</nts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_426" n="HIAT:u" s="T105">
                  <nts id="Seg_427" n="HIAT:ip">"</nts>
                  <ts e="T106" id="Seg_429" n="HIAT:w" s="T105">Tăn</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_432" n="HIAT:w" s="T106">kădəʔ</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_435" n="HIAT:w" s="T107">molaʔ</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_438" n="HIAT:w" s="T108">iʔbəl</ts>
                  <nts id="Seg_439" n="HIAT:ip">?</nts>
                  <nts id="Seg_440" n="HIAT:ip">"</nts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_443" n="HIAT:u" s="T109">
                  <nts id="Seg_444" n="HIAT:ip">"</nts>
                  <ts e="T110" id="Seg_446" n="HIAT:w" s="T109">Măn</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_449" n="HIAT:w" s="T110">simam</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_452" n="HIAT:w" s="T111">tʼezerdələm</ts>
                  <nts id="Seg_453" n="HIAT:ip">.</nts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T117" id="Seg_456" n="HIAT:u" s="T112">
                  <ts e="T113" id="Seg_458" n="HIAT:w" s="T112">Öʔleʔ</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_461" n="HIAT:w" s="T113">măna</ts>
                  <nts id="Seg_462" n="HIAT:ip">,</nts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_465" n="HIAT:w" s="T114">tăn</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_468" n="HIAT:w" s="T115">simal</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_471" n="HIAT:w" s="T116">tʼezerladən</ts>
                  <nts id="Seg_472" n="HIAT:ip">.</nts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_475" n="HIAT:u" s="T117">
                  <ts e="T118" id="Seg_477" n="HIAT:w" s="T117">Tʼikeʔ</ts>
                  <nts id="Seg_478" n="HIAT:ip">!</nts>
                  <nts id="Seg_479" n="HIAT:ip">"</nts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_482" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_484" n="HIAT:w" s="T118">Tʼikəbi</ts>
                  <nts id="Seg_485" n="HIAT:ip">.</nts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T121" id="Seg_488" n="HIAT:u" s="T119">
                  <nts id="Seg_489" n="HIAT:ip">"</nts>
                  <ts e="T120" id="Seg_491" n="HIAT:w" s="T119">Iʔbeʔ</ts>
                  <nts id="Seg_492" n="HIAT:ip">,</nts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_495" n="HIAT:w" s="T120">šüʔdlem</ts>
                  <nts id="Seg_496" n="HIAT:ip">.</nts>
                  <nts id="Seg_497" n="HIAT:ip">"</nts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_500" n="HIAT:u" s="T121">
                  <ts e="T122" id="Seg_502" n="HIAT:w" s="T121">Iʔbəbi</ts>
                  <nts id="Seg_503" n="HIAT:ip">.</nts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_506" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_508" n="HIAT:w" s="T122">Šüʔdəbi</ts>
                  <nts id="Seg_509" n="HIAT:ip">.</nts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T125" id="Seg_512" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_514" n="HIAT:w" s="T123">Dĭzeŋ</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_517" n="HIAT:w" s="T124">šobiiʔ</ts>
                  <nts id="Seg_518" n="HIAT:ip">.</nts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T129" id="Seg_521" n="HIAT:u" s="T125">
                  <nts id="Seg_522" n="HIAT:ip">"</nts>
                  <ts e="T126" id="Seg_524" n="HIAT:w" s="T125">Kădəʔ</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_527" n="HIAT:w" s="T126">molaʔ</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_530" n="HIAT:w" s="T127">šʼaːmnaʔ</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_533" n="HIAT:w" s="T128">iʔbəl</ts>
                  <nts id="Seg_534" n="HIAT:ip">?</nts>
                  <nts id="Seg_535" n="HIAT:ip">"</nts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T132" id="Seg_538" n="HIAT:u" s="T129">
                  <nts id="Seg_539" n="HIAT:ip">"</nts>
                  <ts e="T130" id="Seg_541" n="HIAT:w" s="T129">Măn</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_544" n="HIAT:w" s="T130">ej</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_547" n="HIAT:w" s="T131">šʼaːmniam</ts>
                  <nts id="Seg_548" n="HIAT:ip">.</nts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_551" n="HIAT:u" s="T132">
                  <ts e="T133" id="Seg_553" n="HIAT:w" s="T132">Măn</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_556" n="HIAT:w" s="T133">săgər</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_559" n="HIAT:w" s="T134">kuza</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_562" n="HIAT:w" s="T135">igem</ts>
                  <nts id="Seg_563" n="HIAT:ip">.</nts>
                  <nts id="Seg_564" n="HIAT:ip">"</nts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_567" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_569" n="HIAT:w" s="T136">Ibiiʔ</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_572" n="HIAT:w" s="T137">idəm</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_575" n="HIAT:w" s="T138">sogənə</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_578" n="HIAT:w" s="T139">sarbiiʔ</ts>
                  <nts id="Seg_579" n="HIAT:ip">,</nts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_582" n="HIAT:w" s="T140">bünə</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_585" n="HIAT:w" s="T141">öʔleʔ</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_588" n="HIAT:w" s="T142">mĭbi</ts>
                  <nts id="Seg_589" n="HIAT:ip">.</nts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T143" id="Seg_591" n="sc" s="T0">
               <ts e="T1" id="Seg_593" n="e" s="T0">Nüke </ts>
               <ts e="T2" id="Seg_595" n="e" s="T1">büzʼetsʼəʔ </ts>
               <ts e="T3" id="Seg_597" n="e" s="T2">amnobi. </ts>
               <ts e="T4" id="Seg_599" n="e" s="T3">Najdʼžʼədən </ts>
               <ts e="T5" id="Seg_601" n="e" s="T4">ibi. </ts>
               <ts e="T6" id="Seg_603" n="e" s="T5">Dĭ </ts>
               <ts e="T7" id="Seg_605" n="e" s="T6">jadajlaʔ </ts>
               <ts e="T8" id="Seg_607" n="e" s="T7">šobi. </ts>
               <ts e="T9" id="Seg_609" n="e" s="T8">Nüket </ts>
               <ts e="T10" id="Seg_611" n="e" s="T9">büjleʔ </ts>
               <ts e="T11" id="Seg_613" n="e" s="T10">öʔliedən. </ts>
               <ts e="T12" id="Seg_615" n="e" s="T11">Nüket </ts>
               <ts e="T13" id="Seg_617" n="e" s="T12">ej </ts>
               <ts e="T14" id="Seg_619" n="e" s="T13">kallia. </ts>
               <ts e="T15" id="Seg_621" n="e" s="T14">Dĭgəttə </ts>
               <ts e="T16" id="Seg_623" n="e" s="T15">kamdʼžʼu </ts>
               <ts e="T17" id="Seg_625" n="e" s="T16">kabarbi, </ts>
               <ts e="T18" id="Seg_627" n="e" s="T17">iʔdəbi. </ts>
               <ts e="T19" id="Seg_629" n="e" s="T18">Tagaj </ts>
               <ts e="T20" id="Seg_631" n="e" s="T19">kabarbi. </ts>
               <ts e="T21" id="Seg_633" n="e" s="T20">Nanʼabə </ts>
               <ts e="T22" id="Seg_635" n="e" s="T21">păktəj </ts>
               <ts e="T23" id="Seg_637" n="e" s="T22">müʔlüʔbi. </ts>
               <ts e="T24" id="Seg_639" n="e" s="T23">Nanʼaandə </ts>
               <ts e="T25" id="Seg_641" n="e" s="T24">kem </ts>
               <ts e="T26" id="Seg_643" n="e" s="T25">sarbi. </ts>
               <ts e="T27" id="Seg_645" n="e" s="T26">Kem </ts>
               <ts e="T28" id="Seg_647" n="e" s="T27">mʼaŋbi. </ts>
               <ts e="T29" id="Seg_649" n="e" s="T28">Aspaʔdə </ts>
               <ts e="T30" id="Seg_651" n="e" s="T29">kabarbi, </ts>
               <ts e="T31" id="Seg_653" n="e" s="T30">büjleʔ </ts>
               <ts e="T32" id="Seg_655" n="e" s="T31">üʔməlüʔbi. </ts>
               <ts e="T33" id="Seg_657" n="e" s="T32">Bü </ts>
               <ts e="T34" id="Seg_659" n="e" s="T33">deppi, </ts>
               <ts e="T35" id="Seg_661" n="e" s="T34">aspaʔdə </ts>
               <ts e="T36" id="Seg_663" n="e" s="T35">mĭnzərluʔbi. </ts>
               <ts e="T37" id="Seg_665" n="e" s="T36">Dĭ </ts>
               <ts e="T38" id="Seg_667" n="e" s="T37">kuza </ts>
               <ts e="T39" id="Seg_669" n="e" s="T38">parluʔbi </ts>
               <ts e="T40" id="Seg_671" n="e" s="T39">maʔgəndə. </ts>
               <ts e="T41" id="Seg_673" n="e" s="T40">Maʔgəndə </ts>
               <ts e="T42" id="Seg_675" n="e" s="T41">kambiːza </ts>
               <ts e="T43" id="Seg_677" n="e" s="T42">nükeendə: </ts>
               <ts e="T44" id="Seg_679" n="e" s="T43">"Büjleʔ </ts>
               <ts e="T45" id="Seg_681" n="e" s="T44">kanaʔ!" </ts>
               <ts e="T46" id="Seg_683" n="e" s="T45">Nüket </ts>
               <ts e="T47" id="Seg_685" n="e" s="T46">ej </ts>
               <ts e="T48" id="Seg_687" n="e" s="T47">kallia. </ts>
               <ts e="T49" id="Seg_689" n="e" s="T48">Dĭ </ts>
               <ts e="T50" id="Seg_691" n="e" s="T49">kamdʼžʼu </ts>
               <ts e="T51" id="Seg_693" n="e" s="T50">kabarbi, </ts>
               <ts e="T52" id="Seg_695" n="e" s="T51">iʔdəleibi. </ts>
               <ts e="T53" id="Seg_697" n="e" s="T52">Ej </ts>
               <ts e="T54" id="Seg_699" n="e" s="T53">kallia. </ts>
               <ts e="T55" id="Seg_701" n="e" s="T54">Tagaj </ts>
               <ts e="T56" id="Seg_703" n="e" s="T55">kabarbi, </ts>
               <ts e="T57" id="Seg_705" n="e" s="T56">nükebə </ts>
               <ts e="T58" id="Seg_707" n="e" s="T57">tʼăgarluʔbi. </ts>
               <ts e="T59" id="Seg_709" n="e" s="T58">Nüket </ts>
               <ts e="T60" id="Seg_711" n="e" s="T59">mĭŋgəldəžət </ts>
               <ts e="T61" id="Seg_713" n="e" s="T60">külaːmbi. </ts>
               <ts e="T62" id="Seg_715" n="e" s="T61">Kambi </ts>
               <ts e="T63" id="Seg_717" n="e" s="T62">il </ts>
               <ts e="T64" id="Seg_719" n="e" s="T63">oʔbdəzʼəttə. </ts>
               <ts e="T65" id="Seg_721" n="e" s="T64">Il </ts>
               <ts e="T66" id="Seg_723" n="e" s="T65">oʔbdobi, </ts>
               <ts e="T67" id="Seg_725" n="e" s="T66">šobi </ts>
               <ts e="T68" id="Seg_727" n="e" s="T67">idə </ts>
               <ts e="T69" id="Seg_729" n="e" s="T68">kuzanə. </ts>
               <ts e="T70" id="Seg_731" n="e" s="T69">Dĭ </ts>
               <ts e="T71" id="Seg_733" n="e" s="T70">šăʔlaːmbi </ts>
               <ts e="T72" id="Seg_735" n="e" s="T71">batpolanə. </ts>
               <ts e="T73" id="Seg_737" n="e" s="T72">Surarliadən: </ts>
               <ts e="T74" id="Seg_739" n="e" s="T73">"Gijen </ts>
               <ts e="T75" id="Seg_741" n="e" s="T74">büzʼel?" </ts>
               <ts e="T76" id="Seg_743" n="e" s="T75">"Kallaʔ </ts>
               <ts e="T77" id="Seg_745" n="e" s="T76">tʼürbi." </ts>
               <ts e="T78" id="Seg_747" n="e" s="T77">Dĭ </ts>
               <ts e="T79" id="Seg_749" n="e" s="T78">kuza </ts>
               <ts e="T80" id="Seg_751" n="e" s="T79">amnəbi </ts>
               <ts e="T81" id="Seg_753" n="e" s="T80">batpolan </ts>
               <ts e="T82" id="Seg_755" n="e" s="T81">aŋgəndə. </ts>
               <ts e="T83" id="Seg_757" n="e" s="T82">Dĭ </ts>
               <ts e="T84" id="Seg_759" n="e" s="T83">büštözʼəʔ </ts>
               <ts e="T85" id="Seg_761" n="e" s="T84">kötengəndə </ts>
               <ts e="T86" id="Seg_763" n="e" s="T85">müʔlüʔbi. </ts>
               <ts e="T87" id="Seg_765" n="e" s="T86">Dĭ </ts>
               <ts e="T88" id="Seg_767" n="e" s="T87">karəj </ts>
               <ts e="T89" id="Seg_769" n="e" s="T88">nʼeʔlüʔbi. </ts>
               <ts e="T90" id="Seg_771" n="e" s="T89">Kubindən </ts>
               <ts e="T91" id="Seg_773" n="e" s="T90">büštʼerleʔ </ts>
               <ts e="T92" id="Seg_775" n="e" s="T91">amna. </ts>
               <ts e="T93" id="Seg_777" n="e" s="T92">Dĭm </ts>
               <ts e="T94" id="Seg_779" n="e" s="T93">šüʔdəbiiʔ, </ts>
               <ts e="T95" id="Seg_781" n="e" s="T94">bün </ts>
               <ts e="T96" id="Seg_783" n="e" s="T95">toʔgəndə </ts>
               <ts e="T97" id="Seg_785" n="e" s="T96">kumbiiʔ. </ts>
               <ts e="T98" id="Seg_787" n="e" s="T97">So </ts>
               <ts e="T99" id="Seg_789" n="e" s="T98">alaʔbəjəʔ. </ts>
               <ts e="T100" id="Seg_791" n="e" s="T99">Săgər </ts>
               <ts e="T101" id="Seg_793" n="e" s="T100">kuza </ts>
               <ts e="T102" id="Seg_795" n="e" s="T101">šonəbi. </ts>
               <ts e="T103" id="Seg_797" n="e" s="T102">Pʼel </ts>
               <ts e="T104" id="Seg_799" n="e" s="T103">simabə </ts>
               <ts e="T105" id="Seg_801" n="e" s="T104">kajlabaʔbi. </ts>
               <ts e="T106" id="Seg_803" n="e" s="T105">"Tăn </ts>
               <ts e="T107" id="Seg_805" n="e" s="T106">kădəʔ </ts>
               <ts e="T108" id="Seg_807" n="e" s="T107">molaʔ </ts>
               <ts e="T109" id="Seg_809" n="e" s="T108">iʔbəl?" </ts>
               <ts e="T110" id="Seg_811" n="e" s="T109">"Măn </ts>
               <ts e="T111" id="Seg_813" n="e" s="T110">simam </ts>
               <ts e="T112" id="Seg_815" n="e" s="T111">tʼezerdələm. </ts>
               <ts e="T113" id="Seg_817" n="e" s="T112">Öʔleʔ </ts>
               <ts e="T114" id="Seg_819" n="e" s="T113">măna, </ts>
               <ts e="T115" id="Seg_821" n="e" s="T114">tăn </ts>
               <ts e="T116" id="Seg_823" n="e" s="T115">simal </ts>
               <ts e="T117" id="Seg_825" n="e" s="T116">tʼezerladən. </ts>
               <ts e="T118" id="Seg_827" n="e" s="T117">Tʼikeʔ!" </ts>
               <ts e="T119" id="Seg_829" n="e" s="T118">Tʼikəbi. </ts>
               <ts e="T120" id="Seg_831" n="e" s="T119">"Iʔbeʔ, </ts>
               <ts e="T121" id="Seg_833" n="e" s="T120">šüʔdlem." </ts>
               <ts e="T122" id="Seg_835" n="e" s="T121">Iʔbəbi. </ts>
               <ts e="T123" id="Seg_837" n="e" s="T122">Šüʔdəbi. </ts>
               <ts e="T124" id="Seg_839" n="e" s="T123">Dĭzeŋ </ts>
               <ts e="T125" id="Seg_841" n="e" s="T124">šobiiʔ. </ts>
               <ts e="T126" id="Seg_843" n="e" s="T125">"Kădəʔ </ts>
               <ts e="T127" id="Seg_845" n="e" s="T126">molaʔ </ts>
               <ts e="T128" id="Seg_847" n="e" s="T127">šʼaːmnaʔ </ts>
               <ts e="T129" id="Seg_849" n="e" s="T128">iʔbəl?" </ts>
               <ts e="T130" id="Seg_851" n="e" s="T129">"Măn </ts>
               <ts e="T131" id="Seg_853" n="e" s="T130">ej </ts>
               <ts e="T132" id="Seg_855" n="e" s="T131">šʼaːmniam. </ts>
               <ts e="T133" id="Seg_857" n="e" s="T132">Măn </ts>
               <ts e="T134" id="Seg_859" n="e" s="T133">săgər </ts>
               <ts e="T135" id="Seg_861" n="e" s="T134">kuza </ts>
               <ts e="T136" id="Seg_863" n="e" s="T135">igem." </ts>
               <ts e="T137" id="Seg_865" n="e" s="T136">Ibiiʔ </ts>
               <ts e="T138" id="Seg_867" n="e" s="T137">idəm </ts>
               <ts e="T139" id="Seg_869" n="e" s="T138">sogənə </ts>
               <ts e="T140" id="Seg_871" n="e" s="T139">sarbiiʔ, </ts>
               <ts e="T141" id="Seg_873" n="e" s="T140">bünə </ts>
               <ts e="T142" id="Seg_875" n="e" s="T141">öʔleʔ </ts>
               <ts e="T143" id="Seg_877" n="e" s="T142">mĭbi. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_878" s="T0">AIN_1914_Trickster_flk.001 (001.001)</ta>
            <ta e="T5" id="Seg_879" s="T3">AIN_1914_Trickster_flk.002 (001.002)</ta>
            <ta e="T8" id="Seg_880" s="T5">AIN_1914_Trickster_flk.003 (001.003)</ta>
            <ta e="T11" id="Seg_881" s="T8">AIN_1914_Trickster_flk.004 (001.004)</ta>
            <ta e="T14" id="Seg_882" s="T11">AIN_1914_Trickster_flk.005 (001.005)</ta>
            <ta e="T18" id="Seg_883" s="T14">AIN_1914_Trickster_flk.006 (001.006)</ta>
            <ta e="T20" id="Seg_884" s="T18">AIN_1914_Trickster_flk.007 (001.007)</ta>
            <ta e="T23" id="Seg_885" s="T20">AIN_1914_Trickster_flk.008 (001.008)</ta>
            <ta e="T26" id="Seg_886" s="T23">AIN_1914_Trickster_flk.009 (001.009)</ta>
            <ta e="T28" id="Seg_887" s="T26">AIN_1914_Trickster_flk.010 (001.010)</ta>
            <ta e="T32" id="Seg_888" s="T28">AIN_1914_Trickster_flk.011 (001.011)</ta>
            <ta e="T36" id="Seg_889" s="T32">AIN_1914_Trickster_flk.012 (001.012)</ta>
            <ta e="T40" id="Seg_890" s="T36">AIN_1914_Trickster_flk.013 (001.013)</ta>
            <ta e="T45" id="Seg_891" s="T40">AIN_1914_Trickster_flk.014 (001.014)</ta>
            <ta e="T48" id="Seg_892" s="T45">AIN_1914_Trickster_flk.015 (001.016)</ta>
            <ta e="T52" id="Seg_893" s="T48">AIN_1914_Trickster_flk.016 (001.017)</ta>
            <ta e="T54" id="Seg_894" s="T52">AIN_1914_Trickster_flk.017 (001.018)</ta>
            <ta e="T58" id="Seg_895" s="T54">AIN_1914_Trickster_flk.018 (001.019)</ta>
            <ta e="T61" id="Seg_896" s="T58">AIN_1914_Trickster_flk.019 (001.020)</ta>
            <ta e="T64" id="Seg_897" s="T61">AIN_1914_Trickster_flk.020 (002.001)</ta>
            <ta e="T69" id="Seg_898" s="T64">AIN_1914_Trickster_flk.021 (002.002)</ta>
            <ta e="T72" id="Seg_899" s="T69">AIN_1914_Trickster_flk.022 (002.003)</ta>
            <ta e="T75" id="Seg_900" s="T72">AIN_1914_Trickster_flk.023 (002.004)</ta>
            <ta e="T77" id="Seg_901" s="T75">AIN_1914_Trickster_flk.024 (002.006)</ta>
            <ta e="T82" id="Seg_902" s="T77">AIN_1914_Trickster_flk.025 (002.007)</ta>
            <ta e="T86" id="Seg_903" s="T82">AIN_1914_Trickster_flk.026 (002.008)</ta>
            <ta e="T89" id="Seg_904" s="T86">AIN_1914_Trickster_flk.027 (002.009)</ta>
            <ta e="T92" id="Seg_905" s="T89">AIN_1914_Trickster_flk.028 (002.010)</ta>
            <ta e="T97" id="Seg_906" s="T92">AIN_1914_Trickster_flk.029 (002.011)</ta>
            <ta e="T99" id="Seg_907" s="T97">AIN_1914_Trickster_flk.030 (002.012)</ta>
            <ta e="T102" id="Seg_908" s="T99">AIN_1914_Trickster_flk.031 (003.001)</ta>
            <ta e="T105" id="Seg_909" s="T102">AIN_1914_Trickster_flk.032 (003.002)</ta>
            <ta e="T109" id="Seg_910" s="T105">AIN_1914_Trickster_flk.033 (003.003)</ta>
            <ta e="T112" id="Seg_911" s="T109">AIN_1914_Trickster_flk.034 (003.004)</ta>
            <ta e="T117" id="Seg_912" s="T112">AIN_1914_Trickster_flk.035 (003.005)</ta>
            <ta e="T118" id="Seg_913" s="T117">AIN_1914_Trickster_flk.036 (003.006)</ta>
            <ta e="T119" id="Seg_914" s="T118">AIN_1914_Trickster_flk.037 (003.007)</ta>
            <ta e="T121" id="Seg_915" s="T119">AIN_1914_Trickster_flk.038 (003.008)</ta>
            <ta e="T122" id="Seg_916" s="T121">AIN_1914_Trickster_flk.039 (003.009)</ta>
            <ta e="T123" id="Seg_917" s="T122">AIN_1914_Trickster_flk.040 (003.010)</ta>
            <ta e="T125" id="Seg_918" s="T123">AIN_1914_Trickster_flk.041 (003.011)</ta>
            <ta e="T129" id="Seg_919" s="T125">AIN_1914_Trickster_flk.042 (003.012)</ta>
            <ta e="T132" id="Seg_920" s="T129">AIN_1914_Trickster_flk.043 (003.013)</ta>
            <ta e="T136" id="Seg_921" s="T132">AIN_1914_Trickster_flk.044 (003.014)</ta>
            <ta e="T143" id="Seg_922" s="T136">AIN_1914_Trickster_flk.045 (003.015)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_923" s="T0">Nüke büzʼetsʼəʔ amnobi. </ta>
            <ta e="T5" id="Seg_924" s="T3">Najdʼžʼədən ibi. </ta>
            <ta e="T8" id="Seg_925" s="T5">Dĭ jadajlaʔ šobi. </ta>
            <ta e="T11" id="Seg_926" s="T8">Nüket büjleʔ öʔliedən. </ta>
            <ta e="T14" id="Seg_927" s="T11">Nüket ej kallia. </ta>
            <ta e="T18" id="Seg_928" s="T14">Dĭgəttə kamdʼžʼu kabarbi, iʔdəbi. </ta>
            <ta e="T20" id="Seg_929" s="T18">Tagaj kabarbi. </ta>
            <ta e="T23" id="Seg_930" s="T20">Nanʼabə păktəj müʔlüʔbi. </ta>
            <ta e="T26" id="Seg_931" s="T23">Nanʼaandə kem sarbi. </ta>
            <ta e="T28" id="Seg_932" s="T26">Kem mʼaŋbi. </ta>
            <ta e="T32" id="Seg_933" s="T28">Aspaʔdə kabarbi, büjleʔ üʔməlüʔbi. </ta>
            <ta e="T36" id="Seg_934" s="T32">Bü deppi, aspaʔdə mĭnzərluʔbi. </ta>
            <ta e="T40" id="Seg_935" s="T36">Dĭ kuza parluʔbi maʔgəndə. </ta>
            <ta e="T45" id="Seg_936" s="T40">Maʔgəndə kambiːza nükeendə: "Büjleʔ kanaʔ!" </ta>
            <ta e="T48" id="Seg_937" s="T45">Nüket ej kallia. </ta>
            <ta e="T52" id="Seg_938" s="T48">Dĭ kamdʼžʼu kabarbi, iʔdəleibi. </ta>
            <ta e="T54" id="Seg_939" s="T52">Ej kallia. </ta>
            <ta e="T58" id="Seg_940" s="T54">Tagaj kabarbi, nükebə tʼăgarluʔbi. </ta>
            <ta e="T61" id="Seg_941" s="T58">Nüket mĭŋgəldəžət külaːmbi. </ta>
            <ta e="T64" id="Seg_942" s="T61">Kambi il oʔbdəzʼəttə. </ta>
            <ta e="T69" id="Seg_943" s="T64">Il oʔbdobi, šobi idə kuzanə. </ta>
            <ta e="T72" id="Seg_944" s="T69">Dĭ šăʔlaːmbi batpolanə. </ta>
            <ta e="T75" id="Seg_945" s="T72">Surarliadən: "Gijen büzʼel?" </ta>
            <ta e="T77" id="Seg_946" s="T75">"Kallaʔ tʼürbi." </ta>
            <ta e="T82" id="Seg_947" s="T77">Dĭ kuza amnəbi batpolan aŋgəndə. </ta>
            <ta e="T86" id="Seg_948" s="T82">Dĭ büštözʼəʔ kötengəndə müʔlüʔbi. </ta>
            <ta e="T89" id="Seg_949" s="T86">Dĭ karəj nʼeʔlüʔbi. </ta>
            <ta e="T92" id="Seg_950" s="T89">Kubindən büštʼerleʔ amna. </ta>
            <ta e="T97" id="Seg_951" s="T92">Dĭm šüʔdəbiiʔ, bün toʔgəndə kumbiiʔ. </ta>
            <ta e="T99" id="Seg_952" s="T97">So alaʔbəjəʔ. </ta>
            <ta e="T102" id="Seg_953" s="T99">Săgər kuza šonəbi. </ta>
            <ta e="T105" id="Seg_954" s="T102">Pʼel simabə kajlabaʔbi. </ta>
            <ta e="T109" id="Seg_955" s="T105">"Tăn kădəʔ molaʔ iʔbəl?" </ta>
            <ta e="T112" id="Seg_956" s="T109">"Măn simam tʼezerdələm. </ta>
            <ta e="T117" id="Seg_957" s="T112">Öʔleʔ măna, tăn simal tʼezerladən. </ta>
            <ta e="T118" id="Seg_958" s="T117">Tʼikeʔ!" </ta>
            <ta e="T119" id="Seg_959" s="T118">Tʼikəbi. </ta>
            <ta e="T121" id="Seg_960" s="T119">"Iʔbeʔ, šüʔdlem." </ta>
            <ta e="T122" id="Seg_961" s="T121">Iʔbəbi. </ta>
            <ta e="T123" id="Seg_962" s="T122">Šüʔdəbi. </ta>
            <ta e="T125" id="Seg_963" s="T123">Dĭzeŋ šobiiʔ. </ta>
            <ta e="T129" id="Seg_964" s="T125">"Kădəʔ molaʔ šʼaːmnaʔ iʔbəl?" </ta>
            <ta e="T132" id="Seg_965" s="T129">"Măn ej šʼaːmniam. </ta>
            <ta e="T136" id="Seg_966" s="T132">Măn săgər kuza igem." </ta>
            <ta e="T143" id="Seg_967" s="T136">Ibiiʔ idəm sogənə sarbiiʔ, bünə öʔleʔ mĭbi. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_968" s="T0">nüke</ta>
            <ta e="T2" id="Seg_969" s="T1">büzʼe-t-sʼəʔ</ta>
            <ta e="T3" id="Seg_970" s="T2">amno-bi</ta>
            <ta e="T4" id="Seg_971" s="T3">najdʼžʼə-dən</ta>
            <ta e="T5" id="Seg_972" s="T4">i-bi</ta>
            <ta e="T6" id="Seg_973" s="T5">dĭ</ta>
            <ta e="T7" id="Seg_974" s="T6">jada-j-laʔ</ta>
            <ta e="T8" id="Seg_975" s="T7">šo-bi</ta>
            <ta e="T9" id="Seg_976" s="T8">nüke-t</ta>
            <ta e="T10" id="Seg_977" s="T9">bü-j-leʔ</ta>
            <ta e="T11" id="Seg_978" s="T10">öʔ-lie-dən</ta>
            <ta e="T12" id="Seg_979" s="T11">nüke-t</ta>
            <ta e="T13" id="Seg_980" s="T12">ej</ta>
            <ta e="T14" id="Seg_981" s="T13">kal-lia</ta>
            <ta e="T15" id="Seg_982" s="T14">dĭgəttə</ta>
            <ta e="T16" id="Seg_983" s="T15">kamdʼžʼu</ta>
            <ta e="T17" id="Seg_984" s="T16">kabar-bi</ta>
            <ta e="T18" id="Seg_985" s="T17">iʔdə-bi</ta>
            <ta e="T19" id="Seg_986" s="T18">tagaj</ta>
            <ta e="T20" id="Seg_987" s="T19">kabar-bi</ta>
            <ta e="T21" id="Seg_988" s="T20">nanʼa-bə</ta>
            <ta e="T22" id="Seg_989" s="T21">păktə-j</ta>
            <ta e="T23" id="Seg_990" s="T22">müʔ-lüʔ-bi</ta>
            <ta e="T24" id="Seg_991" s="T23">nanʼa-andə</ta>
            <ta e="T25" id="Seg_992" s="T24">kem</ta>
            <ta e="T26" id="Seg_993" s="T25">sar-bi</ta>
            <ta e="T27" id="Seg_994" s="T26">kem</ta>
            <ta e="T28" id="Seg_995" s="T27">mʼaŋ-bi</ta>
            <ta e="T29" id="Seg_996" s="T28">aspaʔ-də</ta>
            <ta e="T30" id="Seg_997" s="T29">kabar-bi</ta>
            <ta e="T31" id="Seg_998" s="T30">bü-j-leʔ</ta>
            <ta e="T32" id="Seg_999" s="T31">üʔmə-lüʔ-bi</ta>
            <ta e="T33" id="Seg_1000" s="T32">bü</ta>
            <ta e="T34" id="Seg_1001" s="T33">dep-pi</ta>
            <ta e="T35" id="Seg_1002" s="T34">aspaʔ-də</ta>
            <ta e="T36" id="Seg_1003" s="T35">mĭnzər-luʔ-bi</ta>
            <ta e="T37" id="Seg_1004" s="T36">dĭ</ta>
            <ta e="T38" id="Seg_1005" s="T37">kuza</ta>
            <ta e="T39" id="Seg_1006" s="T38">par-luʔ-bi</ta>
            <ta e="T40" id="Seg_1007" s="T39">maʔ-gəndə</ta>
            <ta e="T41" id="Seg_1008" s="T40">maʔ-gəndə</ta>
            <ta e="T42" id="Seg_1009" s="T41">kam-biːza</ta>
            <ta e="T43" id="Seg_1010" s="T42">nüke-endə</ta>
            <ta e="T44" id="Seg_1011" s="T43">bü-j-leʔ</ta>
            <ta e="T45" id="Seg_1012" s="T44">kan-a-ʔ</ta>
            <ta e="T46" id="Seg_1013" s="T45">nüke-t</ta>
            <ta e="T47" id="Seg_1014" s="T46">ej</ta>
            <ta e="T48" id="Seg_1015" s="T47">kal-lia</ta>
            <ta e="T49" id="Seg_1016" s="T48">dĭ</ta>
            <ta e="T50" id="Seg_1017" s="T49">kamdʼžʼu</ta>
            <ta e="T51" id="Seg_1018" s="T50">kabar-bi</ta>
            <ta e="T52" id="Seg_1019" s="T51">iʔdə-lei-bi</ta>
            <ta e="T53" id="Seg_1020" s="T52">ej</ta>
            <ta e="T54" id="Seg_1021" s="T53">kal-lia</ta>
            <ta e="T55" id="Seg_1022" s="T54">tagaj</ta>
            <ta e="T56" id="Seg_1023" s="T55">kabar-bi</ta>
            <ta e="T57" id="Seg_1024" s="T56">nüke-bə</ta>
            <ta e="T58" id="Seg_1025" s="T57">tʼăgar-luʔ-bi</ta>
            <ta e="T59" id="Seg_1026" s="T58">nüke-t</ta>
            <ta e="T60" id="Seg_1027" s="T59">mĭŋgəl-də-žət</ta>
            <ta e="T61" id="Seg_1028" s="T60">kü-laːm-bi</ta>
            <ta e="T62" id="Seg_1029" s="T61">kam-bi</ta>
            <ta e="T63" id="Seg_1030" s="T62">il</ta>
            <ta e="T64" id="Seg_1031" s="T63">oʔbdə-zʼəttə</ta>
            <ta e="T65" id="Seg_1032" s="T64">il</ta>
            <ta e="T66" id="Seg_1033" s="T65">oʔbdo-bi</ta>
            <ta e="T67" id="Seg_1034" s="T66">šo-bi</ta>
            <ta e="T68" id="Seg_1035" s="T67">idə</ta>
            <ta e="T69" id="Seg_1036" s="T68">kuza-nə</ta>
            <ta e="T70" id="Seg_1037" s="T69">dĭ</ta>
            <ta e="T71" id="Seg_1038" s="T70">šăʔ-laːm-bi</ta>
            <ta e="T72" id="Seg_1039" s="T71">batpola-nə</ta>
            <ta e="T73" id="Seg_1040" s="T72">surar-lia-dən</ta>
            <ta e="T74" id="Seg_1041" s="T73">gijen</ta>
            <ta e="T75" id="Seg_1042" s="T74">büzʼe-l</ta>
            <ta e="T76" id="Seg_1043" s="T75">kal-laʔ</ta>
            <ta e="T77" id="Seg_1044" s="T76">tʼür-bi</ta>
            <ta e="T78" id="Seg_1045" s="T77">dĭ</ta>
            <ta e="T79" id="Seg_1046" s="T78">kuza</ta>
            <ta e="T80" id="Seg_1047" s="T79">amnə-bi</ta>
            <ta e="T81" id="Seg_1048" s="T80">batpola-n</ta>
            <ta e="T82" id="Seg_1049" s="T81">aŋ-gəndə</ta>
            <ta e="T83" id="Seg_1050" s="T82">dĭ</ta>
            <ta e="T84" id="Seg_1051" s="T83">büštö-zʼəʔ</ta>
            <ta e="T85" id="Seg_1052" s="T84">köten-gəndə</ta>
            <ta e="T86" id="Seg_1053" s="T85">müʔ-lüʔ-bi</ta>
            <ta e="T87" id="Seg_1054" s="T86">dĭ</ta>
            <ta e="T88" id="Seg_1055" s="T87">kar-əj</ta>
            <ta e="T89" id="Seg_1056" s="T88">nʼeʔ-lüʔ-bi</ta>
            <ta e="T90" id="Seg_1057" s="T89">ku-bi-ndən</ta>
            <ta e="T91" id="Seg_1058" s="T90">büštʼer-leʔ</ta>
            <ta e="T92" id="Seg_1059" s="T91">amna</ta>
            <ta e="T93" id="Seg_1060" s="T92">dĭ-m</ta>
            <ta e="T94" id="Seg_1061" s="T93">šüʔdə-bi-iʔ</ta>
            <ta e="T95" id="Seg_1062" s="T94">bü-n</ta>
            <ta e="T96" id="Seg_1063" s="T95">toʔ-gəndə</ta>
            <ta e="T97" id="Seg_1064" s="T96">kum-bi-iʔ</ta>
            <ta e="T98" id="Seg_1065" s="T97">so</ta>
            <ta e="T99" id="Seg_1066" s="T98">a-laʔbə-jəʔ</ta>
            <ta e="T100" id="Seg_1067" s="T99">săgər</ta>
            <ta e="T101" id="Seg_1068" s="T100">kuza</ta>
            <ta e="T102" id="Seg_1069" s="T101">šonə-bi</ta>
            <ta e="T103" id="Seg_1070" s="T102">pʼel</ta>
            <ta e="T104" id="Seg_1071" s="T103">sima-bə</ta>
            <ta e="T105" id="Seg_1072" s="T104">kaj-labaʔ-bi</ta>
            <ta e="T106" id="Seg_1073" s="T105">tăn</ta>
            <ta e="T107" id="Seg_1074" s="T106">kădəʔ</ta>
            <ta e="T108" id="Seg_1075" s="T107">mo-laʔ</ta>
            <ta e="T109" id="Seg_1076" s="T108">iʔbə-l</ta>
            <ta e="T110" id="Seg_1077" s="T109">măn</ta>
            <ta e="T111" id="Seg_1078" s="T110">sima-m</ta>
            <ta e="T112" id="Seg_1079" s="T111">tʼezer-də-lə-m</ta>
            <ta e="T113" id="Seg_1080" s="T112">öʔ-leʔ</ta>
            <ta e="T114" id="Seg_1081" s="T113">măna</ta>
            <ta e="T115" id="Seg_1082" s="T114">tăn</ta>
            <ta e="T116" id="Seg_1083" s="T115">sima-l</ta>
            <ta e="T117" id="Seg_1084" s="T116">tʼezer-la-dən</ta>
            <ta e="T118" id="Seg_1085" s="T117">tʼike-ʔ</ta>
            <ta e="T119" id="Seg_1086" s="T118">tʼikə-bi</ta>
            <ta e="T120" id="Seg_1087" s="T119">iʔbe-ʔ</ta>
            <ta e="T121" id="Seg_1088" s="T120">šüʔd-le-m</ta>
            <ta e="T122" id="Seg_1089" s="T121">iʔbə-bi</ta>
            <ta e="T123" id="Seg_1090" s="T122">šüʔdə-bi</ta>
            <ta e="T124" id="Seg_1091" s="T123">dĭ-zeŋ</ta>
            <ta e="T125" id="Seg_1092" s="T124">šo-bi-iʔ</ta>
            <ta e="T126" id="Seg_1093" s="T125">kădəʔ</ta>
            <ta e="T127" id="Seg_1094" s="T126">mo-laʔ</ta>
            <ta e="T128" id="Seg_1095" s="T127">šʼaːm-naʔ</ta>
            <ta e="T129" id="Seg_1096" s="T128">iʔbə-l</ta>
            <ta e="T130" id="Seg_1097" s="T129">măn</ta>
            <ta e="T131" id="Seg_1098" s="T130">ej</ta>
            <ta e="T132" id="Seg_1099" s="T131">šʼaːm-nia-m</ta>
            <ta e="T133" id="Seg_1100" s="T132">măn</ta>
            <ta e="T134" id="Seg_1101" s="T133">săgər</ta>
            <ta e="T135" id="Seg_1102" s="T134">kuza</ta>
            <ta e="T136" id="Seg_1103" s="T135">i-ge-m</ta>
            <ta e="T137" id="Seg_1104" s="T136">i-bi-iʔ</ta>
            <ta e="T138" id="Seg_1105" s="T137">idə-m</ta>
            <ta e="T139" id="Seg_1106" s="T138">so-gənə</ta>
            <ta e="T140" id="Seg_1107" s="T139">sar-bi-iʔ</ta>
            <ta e="T141" id="Seg_1108" s="T140">bü-nə</ta>
            <ta e="T142" id="Seg_1109" s="T141">öʔ-leʔ</ta>
            <ta e="T143" id="Seg_1110" s="T142">mĭ-bi</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1111" s="T0">nüke</ta>
            <ta e="T2" id="Seg_1112" s="T1">büzʼe-t-ziʔ</ta>
            <ta e="T3" id="Seg_1113" s="T2">amno-bi</ta>
            <ta e="T4" id="Seg_1114" s="T3">najdʼžʼə-dən</ta>
            <ta e="T5" id="Seg_1115" s="T4">i-bi</ta>
            <ta e="T6" id="Seg_1116" s="T5">dĭ</ta>
            <ta e="T7" id="Seg_1117" s="T6">jada-j-lAʔ</ta>
            <ta e="T8" id="Seg_1118" s="T7">šo-bi</ta>
            <ta e="T9" id="Seg_1119" s="T8">nüke-t</ta>
            <ta e="T10" id="Seg_1120" s="T9">bü-j-lAʔ</ta>
            <ta e="T11" id="Seg_1121" s="T10">öʔ-liA-dən</ta>
            <ta e="T12" id="Seg_1122" s="T11">nüke-t</ta>
            <ta e="T13" id="Seg_1123" s="T12">ej</ta>
            <ta e="T14" id="Seg_1124" s="T13">kan-liA</ta>
            <ta e="T15" id="Seg_1125" s="T14">dĭgəttə</ta>
            <ta e="T16" id="Seg_1126" s="T15">kamdʼžʼu</ta>
            <ta e="T17" id="Seg_1127" s="T16">kabar-bi</ta>
            <ta e="T18" id="Seg_1128" s="T17">iʔdə-bi</ta>
            <ta e="T19" id="Seg_1129" s="T18">tagaj</ta>
            <ta e="T20" id="Seg_1130" s="T19">kabar-bi</ta>
            <ta e="T21" id="Seg_1131" s="T20">nanə-bə</ta>
            <ta e="T22" id="Seg_1132" s="T21">păktə-j</ta>
            <ta e="T23" id="Seg_1133" s="T22">müʔbdə-luʔbdə-bi</ta>
            <ta e="T24" id="Seg_1134" s="T23">nanə-gəndə</ta>
            <ta e="T25" id="Seg_1135" s="T24">kem</ta>
            <ta e="T26" id="Seg_1136" s="T25">sar-bi</ta>
            <ta e="T27" id="Seg_1137" s="T26">kem</ta>
            <ta e="T28" id="Seg_1138" s="T27">mʼaŋ-bi</ta>
            <ta e="T29" id="Seg_1139" s="T28">aspaʔ-də</ta>
            <ta e="T30" id="Seg_1140" s="T29">kabar-bi</ta>
            <ta e="T31" id="Seg_1141" s="T30">bü-j-lAʔ</ta>
            <ta e="T32" id="Seg_1142" s="T31">üʔmə-luʔbdə-bi</ta>
            <ta e="T33" id="Seg_1143" s="T32">bü</ta>
            <ta e="T34" id="Seg_1144" s="T33">det-bi</ta>
            <ta e="T35" id="Seg_1145" s="T34">aspaʔ-də</ta>
            <ta e="T36" id="Seg_1146" s="T35">mĭnzər-luʔbdə-bi</ta>
            <ta e="T37" id="Seg_1147" s="T36">dĭ</ta>
            <ta e="T38" id="Seg_1148" s="T37">kuza</ta>
            <ta e="T39" id="Seg_1149" s="T38">par-luʔbdə-bi</ta>
            <ta e="T40" id="Seg_1150" s="T39">maʔ-gəndə</ta>
            <ta e="T41" id="Seg_1151" s="T40">maʔ-gəndə</ta>
            <ta e="T42" id="Seg_1152" s="T41">kan-bizA</ta>
            <ta e="T43" id="Seg_1153" s="T42">nüke-gəndə</ta>
            <ta e="T44" id="Seg_1154" s="T43">bü-j-lAʔ</ta>
            <ta e="T45" id="Seg_1155" s="T44">kan-ə-ʔ</ta>
            <ta e="T46" id="Seg_1156" s="T45">nüke-t</ta>
            <ta e="T47" id="Seg_1157" s="T46">ej</ta>
            <ta e="T48" id="Seg_1158" s="T47">kan-liA</ta>
            <ta e="T49" id="Seg_1159" s="T48">dĭ</ta>
            <ta e="T50" id="Seg_1160" s="T49">kamdʼžʼu</ta>
            <ta e="T51" id="Seg_1161" s="T50">kabar-bi</ta>
            <ta e="T52" id="Seg_1162" s="T51">iʔdə-lei-bi</ta>
            <ta e="T53" id="Seg_1163" s="T52">ej</ta>
            <ta e="T54" id="Seg_1164" s="T53">kan-liA</ta>
            <ta e="T55" id="Seg_1165" s="T54">tagaj</ta>
            <ta e="T56" id="Seg_1166" s="T55">kabar-bi</ta>
            <ta e="T57" id="Seg_1167" s="T56">nüke-bə</ta>
            <ta e="T58" id="Seg_1168" s="T57">tʼăgar-luʔbdə-bi</ta>
            <ta e="T59" id="Seg_1169" s="T58">nüke-t</ta>
            <ta e="T60" id="Seg_1170" s="T59">mĭŋgəl-də-žət</ta>
            <ta e="T61" id="Seg_1171" s="T60">kü-laːm-bi</ta>
            <ta e="T62" id="Seg_1172" s="T61">kan-bi</ta>
            <ta e="T63" id="Seg_1173" s="T62">il</ta>
            <ta e="T64" id="Seg_1174" s="T63">oʔbdə-zittə</ta>
            <ta e="T65" id="Seg_1175" s="T64">il</ta>
            <ta e="T66" id="Seg_1176" s="T65">oʔbdo-bi</ta>
            <ta e="T67" id="Seg_1177" s="T66">šo-bi</ta>
            <ta e="T68" id="Seg_1178" s="T67">idə</ta>
            <ta e="T69" id="Seg_1179" s="T68">kuza-Tə</ta>
            <ta e="T70" id="Seg_1180" s="T69">dĭ</ta>
            <ta e="T71" id="Seg_1181" s="T70">šaʔ-laːm-bi</ta>
            <ta e="T72" id="Seg_1182" s="T71">batpola-Tə</ta>
            <ta e="T73" id="Seg_1183" s="T72">surar-liA-dən</ta>
            <ta e="T74" id="Seg_1184" s="T73">gijen</ta>
            <ta e="T75" id="Seg_1185" s="T74">büzʼe-l</ta>
            <ta e="T76" id="Seg_1186" s="T75">kan-lAʔ</ta>
            <ta e="T77" id="Seg_1187" s="T76">tʼür-bi</ta>
            <ta e="T78" id="Seg_1188" s="T77">dĭ</ta>
            <ta e="T79" id="Seg_1189" s="T78">kuza</ta>
            <ta e="T80" id="Seg_1190" s="T79">amnə-bi</ta>
            <ta e="T81" id="Seg_1191" s="T80">batpola-n</ta>
            <ta e="T82" id="Seg_1192" s="T81">aŋ-gəndə</ta>
            <ta e="T83" id="Seg_1193" s="T82">dĭ</ta>
            <ta e="T84" id="Seg_1194" s="T83">büštö-ziʔ</ta>
            <ta e="T85" id="Seg_1195" s="T84">köten-gəndə</ta>
            <ta e="T86" id="Seg_1196" s="T85">müʔbdə-luʔbdə-bi</ta>
            <ta e="T87" id="Seg_1197" s="T86">dĭ</ta>
            <ta e="T88" id="Seg_1198" s="T87">kar-j</ta>
            <ta e="T89" id="Seg_1199" s="T88">nʼeʔbdə-luʔbdə-bi</ta>
            <ta e="T90" id="Seg_1200" s="T89">ku-bi-gəndən</ta>
            <ta e="T91" id="Seg_1201" s="T90">büštʼer-lAʔ</ta>
            <ta e="T92" id="Seg_1202" s="T91">amnə</ta>
            <ta e="T93" id="Seg_1203" s="T92">dĭ-m</ta>
            <ta e="T94" id="Seg_1204" s="T93">šüʔdə-bi-jəʔ</ta>
            <ta e="T95" id="Seg_1205" s="T94">bü-n</ta>
            <ta e="T96" id="Seg_1206" s="T95">toʔ-gəndə</ta>
            <ta e="T97" id="Seg_1207" s="T96">kun-bi-jəʔ</ta>
            <ta e="T98" id="Seg_1208" s="T97">so</ta>
            <ta e="T99" id="Seg_1209" s="T98">a-laʔbə-jəʔ</ta>
            <ta e="T100" id="Seg_1210" s="T99">săgər</ta>
            <ta e="T101" id="Seg_1211" s="T100">kuza</ta>
            <ta e="T102" id="Seg_1212" s="T101">šonə-bi</ta>
            <ta e="T103" id="Seg_1213" s="T102">pʼel</ta>
            <ta e="T104" id="Seg_1214" s="T103">sima-bə</ta>
            <ta e="T105" id="Seg_1215" s="T104">kaj-labaʔ-bi</ta>
            <ta e="T106" id="Seg_1216" s="T105">tăn</ta>
            <ta e="T107" id="Seg_1217" s="T106">kădaʔ</ta>
            <ta e="T108" id="Seg_1218" s="T107">mo-lAʔ</ta>
            <ta e="T109" id="Seg_1219" s="T108">iʔbə-l</ta>
            <ta e="T110" id="Seg_1220" s="T109">măn</ta>
            <ta e="T111" id="Seg_1221" s="T110">sima-m</ta>
            <ta e="T112" id="Seg_1222" s="T111">tʼezer-də-lV-m</ta>
            <ta e="T113" id="Seg_1223" s="T112">öʔ-lAʔ</ta>
            <ta e="T114" id="Seg_1224" s="T113">măna</ta>
            <ta e="T115" id="Seg_1225" s="T114">tăn</ta>
            <ta e="T116" id="Seg_1226" s="T115">sima-l</ta>
            <ta e="T117" id="Seg_1227" s="T116">tʼezer-lV-dən</ta>
            <ta e="T118" id="Seg_1228" s="T117">tʼikə-ʔ</ta>
            <ta e="T119" id="Seg_1229" s="T118">tʼikə-bi</ta>
            <ta e="T120" id="Seg_1230" s="T119">iʔbə-ʔ</ta>
            <ta e="T121" id="Seg_1231" s="T120">šüʔdə-lV-m</ta>
            <ta e="T122" id="Seg_1232" s="T121">iʔbə-bi</ta>
            <ta e="T123" id="Seg_1233" s="T122">šüʔdə-bi</ta>
            <ta e="T124" id="Seg_1234" s="T123">dĭ-zAŋ</ta>
            <ta e="T125" id="Seg_1235" s="T124">šo-bi-jəʔ</ta>
            <ta e="T126" id="Seg_1236" s="T125">kădaʔ</ta>
            <ta e="T127" id="Seg_1237" s="T126">mo-lAʔ</ta>
            <ta e="T128" id="Seg_1238" s="T127">šʼaːm-lAʔ</ta>
            <ta e="T129" id="Seg_1239" s="T128">iʔbə-l</ta>
            <ta e="T130" id="Seg_1240" s="T129">măn</ta>
            <ta e="T131" id="Seg_1241" s="T130">ej</ta>
            <ta e="T132" id="Seg_1242" s="T131">šʼaːm-liA-m</ta>
            <ta e="T133" id="Seg_1243" s="T132">măn</ta>
            <ta e="T134" id="Seg_1244" s="T133">săgər</ta>
            <ta e="T135" id="Seg_1245" s="T134">kuza</ta>
            <ta e="T136" id="Seg_1246" s="T135">i-gA-m</ta>
            <ta e="T137" id="Seg_1247" s="T136">i-bi-jəʔ</ta>
            <ta e="T138" id="Seg_1248" s="T137">idə-m</ta>
            <ta e="T139" id="Seg_1249" s="T138">so-Kən</ta>
            <ta e="T140" id="Seg_1250" s="T139">sar-bi-jəʔ</ta>
            <ta e="T141" id="Seg_1251" s="T140">bü-Tə</ta>
            <ta e="T142" id="Seg_1252" s="T141">öʔ-lAʔ</ta>
            <ta e="T143" id="Seg_1253" s="T142">mĭ-bi</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1254" s="T0">woman.[NOM.SG]</ta>
            <ta e="T2" id="Seg_1255" s="T1">man-3SG-INS</ta>
            <ta e="T3" id="Seg_1256" s="T2">live-PST.[3SG]</ta>
            <ta e="T4" id="Seg_1257" s="T3">friend-NOM/GEN/ACC.3PL</ta>
            <ta e="T5" id="Seg_1258" s="T4">be-PST.[3SG]</ta>
            <ta e="T6" id="Seg_1259" s="T5">this.[NOM.SG]</ta>
            <ta e="T7" id="Seg_1260" s="T6">village-VBLZ-CVB</ta>
            <ta e="T8" id="Seg_1261" s="T7">come-PST.[3SG]</ta>
            <ta e="T9" id="Seg_1262" s="T8">woman-NOM/GEN.3SG</ta>
            <ta e="T10" id="Seg_1263" s="T9">water-VBLZ-CVB</ta>
            <ta e="T11" id="Seg_1264" s="T10">send-PRS-3PL.O</ta>
            <ta e="T12" id="Seg_1265" s="T11">woman-NOM/GEN.3SG</ta>
            <ta e="T13" id="Seg_1266" s="T12">NEG</ta>
            <ta e="T14" id="Seg_1267" s="T13">go-PRS.[3SG]</ta>
            <ta e="T15" id="Seg_1268" s="T14">then</ta>
            <ta e="T16" id="Seg_1269" s="T15">whip.[NOM.SG]</ta>
            <ta e="T17" id="Seg_1270" s="T16">grab-PST.[3SG]</ta>
            <ta e="T18" id="Seg_1271" s="T17">hit-PST.[3SG]</ta>
            <ta e="T19" id="Seg_1272" s="T18">knife.[NOM.SG]</ta>
            <ta e="T20" id="Seg_1273" s="T19">grab-PST.[3SG]</ta>
            <ta e="T21" id="Seg_1274" s="T20">belly-ACC.3SG</ta>
            <ta e="T22" id="Seg_1275" s="T21">burst-CVB</ta>
            <ta e="T23" id="Seg_1276" s="T22">push-MOM-PST.[3SG]</ta>
            <ta e="T24" id="Seg_1277" s="T23">belly-LAT/LOC.3SG</ta>
            <ta e="T25" id="Seg_1278" s="T24">blood.[NOM.SG]</ta>
            <ta e="T26" id="Seg_1279" s="T25">bind-PST.[3SG]</ta>
            <ta e="T27" id="Seg_1280" s="T26">blood.[NOM.SG]</ta>
            <ta e="T28" id="Seg_1281" s="T27">flow-PST.[3SG]</ta>
            <ta e="T29" id="Seg_1282" s="T28">cauldron-NOM/GEN/ACC.3SG</ta>
            <ta e="T30" id="Seg_1283" s="T29">grab-PST.[3SG]</ta>
            <ta e="T31" id="Seg_1284" s="T30">water-VBLZ-CVB</ta>
            <ta e="T32" id="Seg_1285" s="T31">run-MOM-PST.[3SG]</ta>
            <ta e="T33" id="Seg_1286" s="T32">water.[NOM.SG]</ta>
            <ta e="T34" id="Seg_1287" s="T33">bring-PST.[3SG]</ta>
            <ta e="T35" id="Seg_1288" s="T34">cauldron-NOM/GEN/ACC.3SG</ta>
            <ta e="T36" id="Seg_1289" s="T35">boil-MOM-PST.[3SG]</ta>
            <ta e="T37" id="Seg_1290" s="T36">this.[NOM.SG]</ta>
            <ta e="T38" id="Seg_1291" s="T37">man.[NOM.SG]</ta>
            <ta e="T39" id="Seg_1292" s="T38">return-MOM-PST.[3SG]</ta>
            <ta e="T40" id="Seg_1293" s="T39">tent-LAT/LOC.3SG</ta>
            <ta e="T41" id="Seg_1294" s="T40">tent-LAT/LOC.3SG</ta>
            <ta e="T42" id="Seg_1295" s="T41">go-CVB.ANT</ta>
            <ta e="T43" id="Seg_1296" s="T42">woman-LAT/LOC.3SG</ta>
            <ta e="T44" id="Seg_1297" s="T43">water-VBLZ-CVB</ta>
            <ta e="T45" id="Seg_1298" s="T44">go-EP-IMP.2SG</ta>
            <ta e="T46" id="Seg_1299" s="T45">woman-NOM/GEN.3SG</ta>
            <ta e="T47" id="Seg_1300" s="T46">NEG</ta>
            <ta e="T48" id="Seg_1301" s="T47">go-PRS.[3SG]</ta>
            <ta e="T49" id="Seg_1302" s="T48">this.[NOM.SG]</ta>
            <ta e="T50" id="Seg_1303" s="T49">whip.[NOM.SG]</ta>
            <ta e="T51" id="Seg_1304" s="T50">grab-PST.[3SG]</ta>
            <ta e="T52" id="Seg_1305" s="T51">hit-SEM-PST.[3SG]</ta>
            <ta e="T53" id="Seg_1306" s="T52">NEG</ta>
            <ta e="T54" id="Seg_1307" s="T53">go-PRS.[3SG]</ta>
            <ta e="T55" id="Seg_1308" s="T54">knife.[NOM.SG]</ta>
            <ta e="T56" id="Seg_1309" s="T55">grab-PST.[3SG]</ta>
            <ta e="T57" id="Seg_1310" s="T56">woman-ACC.3SG</ta>
            <ta e="T58" id="Seg_1311" s="T57">stab-MOM-PST.[3SG]</ta>
            <ta e="T59" id="Seg_1312" s="T58">woman-NOM/GEN.3SG</ta>
            <ta e="T60" id="Seg_1313" s="T59">move-TR-CVB.NEG</ta>
            <ta e="T61" id="Seg_1314" s="T60">die-RES-PST.[3SG]</ta>
            <ta e="T62" id="Seg_1315" s="T61">go-PST.[3SG]</ta>
            <ta e="T63" id="Seg_1316" s="T62">people.[NOM.SG]</ta>
            <ta e="T64" id="Seg_1317" s="T63">gather-INF.LAT</ta>
            <ta e="T65" id="Seg_1318" s="T64">people.[NOM.SG]</ta>
            <ta e="T66" id="Seg_1319" s="T65">gather-PST.[3SG]</ta>
            <ta e="T67" id="Seg_1320" s="T66">come-PST.[3SG]</ta>
            <ta e="T68" id="Seg_1321" s="T67">that.[NOM.SG]</ta>
            <ta e="T69" id="Seg_1322" s="T68">man-LAT</ta>
            <ta e="T70" id="Seg_1323" s="T69">this.[NOM.SG]</ta>
            <ta e="T71" id="Seg_1324" s="T70">hide-RES-PST.[3SG]</ta>
            <ta e="T72" id="Seg_1325" s="T71">cellar-LAT</ta>
            <ta e="T73" id="Seg_1326" s="T72">ask-PRS-3PL.O</ta>
            <ta e="T74" id="Seg_1327" s="T73">where</ta>
            <ta e="T75" id="Seg_1328" s="T74">man-NOM/GEN/ACC.2SG</ta>
            <ta e="T76" id="Seg_1329" s="T75">go-CVB</ta>
            <ta e="T77" id="Seg_1330" s="T76">disappear-PST.[3SG]</ta>
            <ta e="T78" id="Seg_1331" s="T77">this.[NOM.SG]</ta>
            <ta e="T79" id="Seg_1332" s="T78">man.[NOM.SG]</ta>
            <ta e="T80" id="Seg_1333" s="T79">sit.down-PST.[3SG]</ta>
            <ta e="T81" id="Seg_1334" s="T80">cellar-GEN</ta>
            <ta e="T82" id="Seg_1335" s="T81">trap.door-LAT/LOC.3SG</ta>
            <ta e="T83" id="Seg_1336" s="T82">this.[NOM.SG]</ta>
            <ta e="T84" id="Seg_1337" s="T83">awl-INS</ta>
            <ta e="T85" id="Seg_1338" s="T84">ass-LAT/LOC.3SG</ta>
            <ta e="T86" id="Seg_1339" s="T85">push-MOM-PST.[3SG]</ta>
            <ta e="T87" id="Seg_1340" s="T86">this.[NOM.SG]</ta>
            <ta e="T88" id="Seg_1341" s="T87">open-CVB</ta>
            <ta e="T89" id="Seg_1342" s="T88">pull-MOM-PST.[3SG]</ta>
            <ta e="T90" id="Seg_1343" s="T89">see-CVB.TEMP-LAT/LOC.3PL</ta>
            <ta e="T91" id="Seg_1344" s="T90">laugh-CVB</ta>
            <ta e="T92" id="Seg_1345" s="T91">sit.[3SG]</ta>
            <ta e="T93" id="Seg_1346" s="T92">this-ACC</ta>
            <ta e="T94" id="Seg_1347" s="T93">fetter-PST-3PL</ta>
            <ta e="T95" id="Seg_1348" s="T94">river-GEN</ta>
            <ta e="T96" id="Seg_1349" s="T95">edge-LAT/LOC.3SG</ta>
            <ta e="T97" id="Seg_1350" s="T96">bring-PST-3PL</ta>
            <ta e="T98" id="Seg_1351" s="T97">raft.[NOM.SG]</ta>
            <ta e="T99" id="Seg_1352" s="T98">make-DUR-3PL</ta>
            <ta e="T100" id="Seg_1353" s="T99">one_eyed.[NOM.SG]</ta>
            <ta e="T101" id="Seg_1354" s="T100">man.[NOM.SG]</ta>
            <ta e="T102" id="Seg_1355" s="T101">come-PST.[3SG]</ta>
            <ta e="T103" id="Seg_1356" s="T102">half</ta>
            <ta e="T104" id="Seg_1357" s="T103">eye-ACC.3SG</ta>
            <ta e="T105" id="Seg_1358" s="T104">close-RES-PST.[3SG]</ta>
            <ta e="T106" id="Seg_1359" s="T105">you.NOM</ta>
            <ta e="T107" id="Seg_1360" s="T106">how</ta>
            <ta e="T108" id="Seg_1361" s="T107">become-CVB</ta>
            <ta e="T109" id="Seg_1362" s="T108">lie.down-2SG</ta>
            <ta e="T110" id="Seg_1363" s="T109">I.GEN</ta>
            <ta e="T111" id="Seg_1364" s="T110">eye-NOM/GEN/ACC.1SG</ta>
            <ta e="T112" id="Seg_1365" s="T111">heal-TR-FUT-1SG</ta>
            <ta e="T113" id="Seg_1366" s="T112">let-2PL</ta>
            <ta e="T114" id="Seg_1367" s="T113">I.ACC</ta>
            <ta e="T115" id="Seg_1368" s="T114">you.GEN</ta>
            <ta e="T116" id="Seg_1369" s="T115">eye-NOM/GEN/ACC.2SG</ta>
            <ta e="T117" id="Seg_1370" s="T116">heal-FUT-3PL.O</ta>
            <ta e="T118" id="Seg_1371" s="T117">tie.loose-IMP.2SG</ta>
            <ta e="T119" id="Seg_1372" s="T118">tie.loose-PST.[3SG]</ta>
            <ta e="T120" id="Seg_1373" s="T119">lie.down-IMP.2SG</ta>
            <ta e="T121" id="Seg_1374" s="T120">fetter-FUT-1SG</ta>
            <ta e="T122" id="Seg_1375" s="T121">lie.down-PST.[3SG]</ta>
            <ta e="T123" id="Seg_1376" s="T122">fetter-PST.[3SG]</ta>
            <ta e="T124" id="Seg_1377" s="T123">this-PL</ta>
            <ta e="T125" id="Seg_1378" s="T124">come-PST-3PL</ta>
            <ta e="T126" id="Seg_1379" s="T125">how</ta>
            <ta e="T127" id="Seg_1380" s="T126">become-CVB</ta>
            <ta e="T128" id="Seg_1381" s="T127">lie-CVB</ta>
            <ta e="T129" id="Seg_1382" s="T128">lie.down-2SG</ta>
            <ta e="T130" id="Seg_1383" s="T129">I.NOM</ta>
            <ta e="T131" id="Seg_1384" s="T130">NEG</ta>
            <ta e="T132" id="Seg_1385" s="T131">lie-PRS-1SG</ta>
            <ta e="T133" id="Seg_1386" s="T132">I.NOM</ta>
            <ta e="T134" id="Seg_1387" s="T133">one_eyed.[NOM.SG]</ta>
            <ta e="T135" id="Seg_1388" s="T134">man.[NOM.SG]</ta>
            <ta e="T136" id="Seg_1389" s="T135">be-PRS-1SG</ta>
            <ta e="T137" id="Seg_1390" s="T136">take-PST-3PL</ta>
            <ta e="T138" id="Seg_1391" s="T137">that-ACC</ta>
            <ta e="T139" id="Seg_1392" s="T138">raft-LOC</ta>
            <ta e="T140" id="Seg_1393" s="T139">bind-PST-3PL</ta>
            <ta e="T141" id="Seg_1394" s="T140">river-LAT</ta>
            <ta e="T142" id="Seg_1395" s="T141">let-CVB</ta>
            <ta e="T143" id="Seg_1396" s="T142">give-PST.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1397" s="T0">женщина.[NOM.SG]</ta>
            <ta e="T2" id="Seg_1398" s="T1">мужчина-3SG-INS</ta>
            <ta e="T3" id="Seg_1399" s="T2">жить-PST.[3SG]</ta>
            <ta e="T4" id="Seg_1400" s="T3">друг-NOM/GEN/ACC.3PL</ta>
            <ta e="T5" id="Seg_1401" s="T4">быть-PST.[3SG]</ta>
            <ta e="T6" id="Seg_1402" s="T5">этот.[NOM.SG]</ta>
            <ta e="T7" id="Seg_1403" s="T6">деревня-VBLZ-CVB</ta>
            <ta e="T8" id="Seg_1404" s="T7">прийти-PST.[3SG]</ta>
            <ta e="T9" id="Seg_1405" s="T8">женщина-NOM/GEN.3SG</ta>
            <ta e="T10" id="Seg_1406" s="T9">вода-VBLZ-CVB</ta>
            <ta e="T11" id="Seg_1407" s="T10">послать-PRS-3PL.O</ta>
            <ta e="T12" id="Seg_1408" s="T11">женщина-NOM/GEN.3SG</ta>
            <ta e="T13" id="Seg_1409" s="T12">NEG</ta>
            <ta e="T14" id="Seg_1410" s="T13">пойти-PRS.[3SG]</ta>
            <ta e="T15" id="Seg_1411" s="T14">тогда</ta>
            <ta e="T16" id="Seg_1412" s="T15">кнут.[NOM.SG]</ta>
            <ta e="T17" id="Seg_1413" s="T16">хватать-PST.[3SG]</ta>
            <ta e="T18" id="Seg_1414" s="T17">бить-PST.[3SG]</ta>
            <ta e="T19" id="Seg_1415" s="T18">нож.[NOM.SG]</ta>
            <ta e="T20" id="Seg_1416" s="T19">хватать-PST.[3SG]</ta>
            <ta e="T21" id="Seg_1417" s="T20">живот-ACC.3SG</ta>
            <ta e="T22" id="Seg_1418" s="T21">лопнуть-CVB</ta>
            <ta e="T23" id="Seg_1419" s="T22">вставить-MOM-PST.[3SG]</ta>
            <ta e="T24" id="Seg_1420" s="T23">живот-LAT/LOC.3SG</ta>
            <ta e="T25" id="Seg_1421" s="T24">кровь.[NOM.SG]</ta>
            <ta e="T26" id="Seg_1422" s="T25">завязать-PST.[3SG]</ta>
            <ta e="T27" id="Seg_1423" s="T26">кровь.[NOM.SG]</ta>
            <ta e="T28" id="Seg_1424" s="T27">течь-PST.[3SG]</ta>
            <ta e="T29" id="Seg_1425" s="T28">котел-NOM/GEN/ACC.3SG</ta>
            <ta e="T30" id="Seg_1426" s="T29">хватать-PST.[3SG]</ta>
            <ta e="T31" id="Seg_1427" s="T30">вода-VBLZ-CVB</ta>
            <ta e="T32" id="Seg_1428" s="T31">бежать-MOM-PST.[3SG]</ta>
            <ta e="T33" id="Seg_1429" s="T32">вода.[NOM.SG]</ta>
            <ta e="T34" id="Seg_1430" s="T33">принести-PST.[3SG]</ta>
            <ta e="T35" id="Seg_1431" s="T34">котел-NOM/GEN/ACC.3SG</ta>
            <ta e="T36" id="Seg_1432" s="T35">кипятить-MOM-PST.[3SG]</ta>
            <ta e="T37" id="Seg_1433" s="T36">этот.[NOM.SG]</ta>
            <ta e="T38" id="Seg_1434" s="T37">мужчина.[NOM.SG]</ta>
            <ta e="T39" id="Seg_1435" s="T38">вернуться-MOM-PST.[3SG]</ta>
            <ta e="T40" id="Seg_1436" s="T39">чум-LAT/LOC.3SG</ta>
            <ta e="T41" id="Seg_1437" s="T40">чум-LAT/LOC.3SG</ta>
            <ta e="T42" id="Seg_1438" s="T41">пойти-CVB.ANT</ta>
            <ta e="T43" id="Seg_1439" s="T42">женщина-LAT/LOC.3SG</ta>
            <ta e="T44" id="Seg_1440" s="T43">вода-VBLZ-CVB</ta>
            <ta e="T45" id="Seg_1441" s="T44">пойти-EP-IMP.2SG</ta>
            <ta e="T46" id="Seg_1442" s="T45">женщина-NOM/GEN.3SG</ta>
            <ta e="T47" id="Seg_1443" s="T46">NEG</ta>
            <ta e="T48" id="Seg_1444" s="T47">пойти-PRS.[3SG]</ta>
            <ta e="T49" id="Seg_1445" s="T48">этот.[NOM.SG]</ta>
            <ta e="T50" id="Seg_1446" s="T49">кнут.[NOM.SG]</ta>
            <ta e="T51" id="Seg_1447" s="T50">хватать-PST.[3SG]</ta>
            <ta e="T52" id="Seg_1448" s="T51">бить-SEM-PST.[3SG]</ta>
            <ta e="T53" id="Seg_1449" s="T52">NEG</ta>
            <ta e="T54" id="Seg_1450" s="T53">пойти-PRS.[3SG]</ta>
            <ta e="T55" id="Seg_1451" s="T54">нож.[NOM.SG]</ta>
            <ta e="T56" id="Seg_1452" s="T55">хватать-PST.[3SG]</ta>
            <ta e="T57" id="Seg_1453" s="T56">женщина-ACC.3SG</ta>
            <ta e="T58" id="Seg_1454" s="T57">колоть-MOM-PST.[3SG]</ta>
            <ta e="T59" id="Seg_1455" s="T58">женщина-NOM/GEN.3SG</ta>
            <ta e="T60" id="Seg_1456" s="T59">двигаться-TR-CVB.NEG</ta>
            <ta e="T61" id="Seg_1457" s="T60">умереть-RES-PST.[3SG]</ta>
            <ta e="T62" id="Seg_1458" s="T61">пойти-PST.[3SG]</ta>
            <ta e="T63" id="Seg_1459" s="T62">люди.[NOM.SG]</ta>
            <ta e="T64" id="Seg_1460" s="T63">собирать-INF.LAT</ta>
            <ta e="T65" id="Seg_1461" s="T64">люди.[NOM.SG]</ta>
            <ta e="T66" id="Seg_1462" s="T65">собрать-PST.[3SG]</ta>
            <ta e="T67" id="Seg_1463" s="T66">прийти-PST.[3SG]</ta>
            <ta e="T68" id="Seg_1464" s="T67">тот.[NOM.SG]</ta>
            <ta e="T69" id="Seg_1465" s="T68">мужчина-LAT</ta>
            <ta e="T70" id="Seg_1466" s="T69">этот.[NOM.SG]</ta>
            <ta e="T71" id="Seg_1467" s="T70">спрятаться-RES-PST.[3SG]</ta>
            <ta e="T72" id="Seg_1468" s="T71">подвал-LAT</ta>
            <ta e="T73" id="Seg_1469" s="T72">спросить-PRS-3PL.O</ta>
            <ta e="T74" id="Seg_1470" s="T73">где</ta>
            <ta e="T75" id="Seg_1471" s="T74">мужчина-NOM/GEN/ACC.2SG</ta>
            <ta e="T76" id="Seg_1472" s="T75">пойти-CVB</ta>
            <ta e="T77" id="Seg_1473" s="T76">исчезнуть-PST.[3SG]</ta>
            <ta e="T78" id="Seg_1474" s="T77">этот.[NOM.SG]</ta>
            <ta e="T79" id="Seg_1475" s="T78">мужчина.[NOM.SG]</ta>
            <ta e="T80" id="Seg_1476" s="T79">сесть-PST.[3SG]</ta>
            <ta e="T81" id="Seg_1477" s="T80">подвал-GEN</ta>
            <ta e="T82" id="Seg_1478" s="T81">крышка.капкана-LAT/LOC.3SG</ta>
            <ta e="T83" id="Seg_1479" s="T82">этот.[NOM.SG]</ta>
            <ta e="T84" id="Seg_1480" s="T83">сова-INS</ta>
            <ta e="T85" id="Seg_1481" s="T84">зад-LAT/LOC.3SG</ta>
            <ta e="T86" id="Seg_1482" s="T85">вставить-MOM-PST.[3SG]</ta>
            <ta e="T87" id="Seg_1483" s="T86">этот.[NOM.SG]</ta>
            <ta e="T88" id="Seg_1484" s="T87">открыть-CVB</ta>
            <ta e="T89" id="Seg_1485" s="T88">тянуть-MOM-PST.[3SG]</ta>
            <ta e="T90" id="Seg_1486" s="T89">видеть-CVB.TEMP-LAT/LOC.3PL</ta>
            <ta e="T91" id="Seg_1487" s="T90">смеяться-CVB</ta>
            <ta e="T92" id="Seg_1488" s="T91">сидеть.[3SG]</ta>
            <ta e="T93" id="Seg_1489" s="T92">этот-ACC</ta>
            <ta e="T94" id="Seg_1490" s="T93">перо-PST-3PL</ta>
            <ta e="T95" id="Seg_1491" s="T94">река-GEN</ta>
            <ta e="T96" id="Seg_1492" s="T95">край-LAT/LOC.3SG</ta>
            <ta e="T97" id="Seg_1493" s="T96">нести-PST-3PL</ta>
            <ta e="T98" id="Seg_1494" s="T97">плот.[NOM.SG]</ta>
            <ta e="T99" id="Seg_1495" s="T98">делать-DUR-3PL</ta>
            <ta e="T100" id="Seg_1496" s="T99">одноглазый.[NOM.SG]</ta>
            <ta e="T101" id="Seg_1497" s="T100">мужчина.[NOM.SG]</ta>
            <ta e="T102" id="Seg_1498" s="T101">прийти-PST.[3SG]</ta>
            <ta e="T103" id="Seg_1499" s="T102">половина</ta>
            <ta e="T104" id="Seg_1500" s="T103">глаз-ACC.3SG</ta>
            <ta e="T105" id="Seg_1501" s="T104">закрыть-RES-PST.[3SG]</ta>
            <ta e="T106" id="Seg_1502" s="T105">ты.NOM</ta>
            <ta e="T107" id="Seg_1503" s="T106">как</ta>
            <ta e="T108" id="Seg_1504" s="T107">стать-CVB</ta>
            <ta e="T109" id="Seg_1505" s="T108">ложиться-2SG</ta>
            <ta e="T110" id="Seg_1506" s="T109">я.GEN</ta>
            <ta e="T111" id="Seg_1507" s="T110">глаз-NOM/GEN/ACC.1SG</ta>
            <ta e="T112" id="Seg_1508" s="T111">вылечить-TR-FUT-1SG</ta>
            <ta e="T113" id="Seg_1509" s="T112">пускать-2PL</ta>
            <ta e="T114" id="Seg_1510" s="T113">я.ACC</ta>
            <ta e="T115" id="Seg_1511" s="T114">ты.GEN</ta>
            <ta e="T116" id="Seg_1512" s="T115">глаз-NOM/GEN/ACC.2SG</ta>
            <ta e="T117" id="Seg_1513" s="T116">вылечить-FUT-3PL.O</ta>
            <ta e="T118" id="Seg_1514" s="T117">развязать-IMP.2SG</ta>
            <ta e="T119" id="Seg_1515" s="T118">развязать-PST.[3SG]</ta>
            <ta e="T120" id="Seg_1516" s="T119">ложиться-IMP.2SG</ta>
            <ta e="T121" id="Seg_1517" s="T120">перо-FUT-1SG</ta>
            <ta e="T122" id="Seg_1518" s="T121">ложиться-PST.[3SG]</ta>
            <ta e="T123" id="Seg_1519" s="T122">перо-PST.[3SG]</ta>
            <ta e="T124" id="Seg_1520" s="T123">этот-PL</ta>
            <ta e="T125" id="Seg_1521" s="T124">прийти-PST-3PL</ta>
            <ta e="T126" id="Seg_1522" s="T125">как</ta>
            <ta e="T127" id="Seg_1523" s="T126">стать-CVB</ta>
            <ta e="T128" id="Seg_1524" s="T127">лгать-CVB</ta>
            <ta e="T129" id="Seg_1525" s="T128">ложиться-2SG</ta>
            <ta e="T130" id="Seg_1526" s="T129">я.NOM</ta>
            <ta e="T131" id="Seg_1527" s="T130">NEG</ta>
            <ta e="T132" id="Seg_1528" s="T131">лгать-PRS-1SG</ta>
            <ta e="T133" id="Seg_1529" s="T132">я.NOM</ta>
            <ta e="T134" id="Seg_1530" s="T133">одноглазый.[NOM.SG]</ta>
            <ta e="T135" id="Seg_1531" s="T134">мужчина.[NOM.SG]</ta>
            <ta e="T136" id="Seg_1532" s="T135">быть-PRS-1SG</ta>
            <ta e="T137" id="Seg_1533" s="T136">взять-PST-3PL</ta>
            <ta e="T138" id="Seg_1534" s="T137">тот-ACC</ta>
            <ta e="T139" id="Seg_1535" s="T138">плот-LOC</ta>
            <ta e="T140" id="Seg_1536" s="T139">завязать-PST-3PL</ta>
            <ta e="T141" id="Seg_1537" s="T140">река-LAT</ta>
            <ta e="T142" id="Seg_1538" s="T141">пускать-CVB</ta>
            <ta e="T143" id="Seg_1539" s="T142">дать-PST.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1540" s="T0">n-n:case</ta>
            <ta e="T2" id="Seg_1541" s="T1">n-n:case.poss-n:case</ta>
            <ta e="T3" id="Seg_1542" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_1543" s="T3">n-n:case.poss</ta>
            <ta e="T5" id="Seg_1544" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_1545" s="T5">dempro-n:case</ta>
            <ta e="T7" id="Seg_1546" s="T6">n-n&gt;v-v:n.fin</ta>
            <ta e="T8" id="Seg_1547" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_1548" s="T8">n-n:case.poss</ta>
            <ta e="T10" id="Seg_1549" s="T9">n-n&gt;v-v:n.fin</ta>
            <ta e="T11" id="Seg_1550" s="T10">v-v:tense-v:pn</ta>
            <ta e="T12" id="Seg_1551" s="T11">n-n:case.poss</ta>
            <ta e="T13" id="Seg_1552" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_1553" s="T13">v-v:tense-v:pn</ta>
            <ta e="T15" id="Seg_1554" s="T14">adv</ta>
            <ta e="T16" id="Seg_1555" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_1556" s="T16">v-v:tense-v:pn</ta>
            <ta e="T18" id="Seg_1557" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_1558" s="T18">n-n:case</ta>
            <ta e="T20" id="Seg_1559" s="T19">v-v:tense-v:pn</ta>
            <ta e="T21" id="Seg_1560" s="T20">n-n:case.poss</ta>
            <ta e="T22" id="Seg_1561" s="T21">v-v:n.fin</ta>
            <ta e="T23" id="Seg_1562" s="T22">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_1563" s="T23">n-n:case.poss</ta>
            <ta e="T25" id="Seg_1564" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_1565" s="T25">v-v:tense-v:pn</ta>
            <ta e="T27" id="Seg_1566" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_1567" s="T27">v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_1568" s="T28">n-n:case.poss</ta>
            <ta e="T30" id="Seg_1569" s="T29">v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_1570" s="T30">n-n&gt;v-v:n.fin</ta>
            <ta e="T32" id="Seg_1571" s="T31">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_1572" s="T32">n-n:case</ta>
            <ta e="T34" id="Seg_1573" s="T33">v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_1574" s="T34">n-n:case.poss</ta>
            <ta e="T36" id="Seg_1575" s="T35">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T37" id="Seg_1576" s="T36">dempro-n:case</ta>
            <ta e="T38" id="Seg_1577" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_1578" s="T38">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T40" id="Seg_1579" s="T39">n-n:case.poss</ta>
            <ta e="T41" id="Seg_1580" s="T40">n-n:case.poss</ta>
            <ta e="T42" id="Seg_1581" s="T41">v-v:n.fin</ta>
            <ta e="T43" id="Seg_1582" s="T42">n-n:case.poss</ta>
            <ta e="T44" id="Seg_1583" s="T43">n-n&gt;v-v:n.fin</ta>
            <ta e="T45" id="Seg_1584" s="T44">v-v:ins-v:mood.pn</ta>
            <ta e="T46" id="Seg_1585" s="T45">n-n:case.poss</ta>
            <ta e="T47" id="Seg_1586" s="T46">ptcl</ta>
            <ta e="T48" id="Seg_1587" s="T47">v-v:tense-v:pn</ta>
            <ta e="T49" id="Seg_1588" s="T48">dempro-n:case</ta>
            <ta e="T50" id="Seg_1589" s="T49">n-n:case</ta>
            <ta e="T51" id="Seg_1590" s="T50">v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_1591" s="T51">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T53" id="Seg_1592" s="T52">ptcl</ta>
            <ta e="T54" id="Seg_1593" s="T53">v-v:tense-v:pn</ta>
            <ta e="T55" id="Seg_1594" s="T54">n-n:case</ta>
            <ta e="T56" id="Seg_1595" s="T55">v-v:tense-v:pn</ta>
            <ta e="T57" id="Seg_1596" s="T56">n-n:case.poss</ta>
            <ta e="T58" id="Seg_1597" s="T57">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T59" id="Seg_1598" s="T58">n-n:case.poss</ta>
            <ta e="T60" id="Seg_1599" s="T59">v-v&gt;v-v:n.fin</ta>
            <ta e="T61" id="Seg_1600" s="T60">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T62" id="Seg_1601" s="T61">v-v:tense-v:pn</ta>
            <ta e="T63" id="Seg_1602" s="T62">n-n:case</ta>
            <ta e="T64" id="Seg_1603" s="T63">v-v:n.fin</ta>
            <ta e="T65" id="Seg_1604" s="T64">n-n:case</ta>
            <ta e="T66" id="Seg_1605" s="T65">v-v:tense-v:pn</ta>
            <ta e="T67" id="Seg_1606" s="T66">v-v:tense-v:pn</ta>
            <ta e="T68" id="Seg_1607" s="T67">dempro-n:case</ta>
            <ta e="T69" id="Seg_1608" s="T68">n-n:case</ta>
            <ta e="T70" id="Seg_1609" s="T69">dempro-n:case</ta>
            <ta e="T71" id="Seg_1610" s="T70">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T72" id="Seg_1611" s="T71">n-n:case</ta>
            <ta e="T73" id="Seg_1612" s="T72">v-v:tense-v:pn</ta>
            <ta e="T74" id="Seg_1613" s="T73">que</ta>
            <ta e="T75" id="Seg_1614" s="T74">n-n:case.poss</ta>
            <ta e="T76" id="Seg_1615" s="T75">v-v:n.fin</ta>
            <ta e="T77" id="Seg_1616" s="T76">v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_1617" s="T77">dempro-n:case</ta>
            <ta e="T79" id="Seg_1618" s="T78">n-n:case</ta>
            <ta e="T80" id="Seg_1619" s="T79">v-v:tense-v:pn</ta>
            <ta e="T81" id="Seg_1620" s="T80">n-n:case</ta>
            <ta e="T82" id="Seg_1621" s="T81">n-n:case.poss</ta>
            <ta e="T83" id="Seg_1622" s="T82">dempro-n:case</ta>
            <ta e="T84" id="Seg_1623" s="T83">n-n:case</ta>
            <ta e="T85" id="Seg_1624" s="T84">n-n:case.poss</ta>
            <ta e="T86" id="Seg_1625" s="T85">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T87" id="Seg_1626" s="T86">dempro-n:case</ta>
            <ta e="T88" id="Seg_1627" s="T87">v-v:n.fin</ta>
            <ta e="T89" id="Seg_1628" s="T88">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T90" id="Seg_1629" s="T89">v-v:n.fin-v:(case.poss)</ta>
            <ta e="T91" id="Seg_1630" s="T90">v-v:n.fin</ta>
            <ta e="T92" id="Seg_1631" s="T91">v-v:pn</ta>
            <ta e="T93" id="Seg_1632" s="T92">dempro-n:case</ta>
            <ta e="T94" id="Seg_1633" s="T93">v-v:tense-v:pn</ta>
            <ta e="T95" id="Seg_1634" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_1635" s="T95">n-n:case.poss</ta>
            <ta e="T97" id="Seg_1636" s="T96">v-v:tense-v:pn</ta>
            <ta e="T98" id="Seg_1637" s="T97">n-n:case</ta>
            <ta e="T99" id="Seg_1638" s="T98">v-v&gt;v-v:pn</ta>
            <ta e="T100" id="Seg_1639" s="T99">adj-n:case</ta>
            <ta e="T101" id="Seg_1640" s="T100">n-n:case</ta>
            <ta e="T102" id="Seg_1641" s="T101">v-v:tense-v:pn</ta>
            <ta e="T103" id="Seg_1642" s="T102">quant</ta>
            <ta e="T104" id="Seg_1643" s="T103">n-n:case.poss</ta>
            <ta e="T105" id="Seg_1644" s="T104">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T106" id="Seg_1645" s="T105">pers</ta>
            <ta e="T107" id="Seg_1646" s="T106">que</ta>
            <ta e="T108" id="Seg_1647" s="T107">v-v:n.fin</ta>
            <ta e="T109" id="Seg_1648" s="T108">v-v:pn</ta>
            <ta e="T110" id="Seg_1649" s="T109">pers</ta>
            <ta e="T111" id="Seg_1650" s="T110">n-n:case.poss</ta>
            <ta e="T112" id="Seg_1651" s="T111">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T113" id="Seg_1652" s="T112">v-v:pn</ta>
            <ta e="T114" id="Seg_1653" s="T113">pers</ta>
            <ta e="T115" id="Seg_1654" s="T114">pers</ta>
            <ta e="T116" id="Seg_1655" s="T115">n-n:case.poss</ta>
            <ta e="T117" id="Seg_1656" s="T116">v-v:tense-v:pn</ta>
            <ta e="T118" id="Seg_1657" s="T117">v-v:mood.pn</ta>
            <ta e="T119" id="Seg_1658" s="T118">v-v:tense-v:pn</ta>
            <ta e="T120" id="Seg_1659" s="T119">v-v:mood.pn</ta>
            <ta e="T121" id="Seg_1660" s="T120">v-v:tense-v:pn</ta>
            <ta e="T122" id="Seg_1661" s="T121">v-v:tense-v:pn</ta>
            <ta e="T123" id="Seg_1662" s="T122">v-v:tense-v:pn</ta>
            <ta e="T124" id="Seg_1663" s="T123">dempro-n:num</ta>
            <ta e="T125" id="Seg_1664" s="T124">v-v:tense-v:pn</ta>
            <ta e="T126" id="Seg_1665" s="T125">que</ta>
            <ta e="T127" id="Seg_1666" s="T126">v-v:n.fin</ta>
            <ta e="T128" id="Seg_1667" s="T127">v-v:n.fin</ta>
            <ta e="T129" id="Seg_1668" s="T128">v-v:pn</ta>
            <ta e="T130" id="Seg_1669" s="T129">pers</ta>
            <ta e="T131" id="Seg_1670" s="T130">ptcl</ta>
            <ta e="T132" id="Seg_1671" s="T131">v-v:tense-v:pn</ta>
            <ta e="T133" id="Seg_1672" s="T132">pers</ta>
            <ta e="T134" id="Seg_1673" s="T133">adj-n:case</ta>
            <ta e="T135" id="Seg_1674" s="T134">n-n:case</ta>
            <ta e="T136" id="Seg_1675" s="T135">v-v:tense-v:pn</ta>
            <ta e="T137" id="Seg_1676" s="T136">v-v:tense-v:pn</ta>
            <ta e="T138" id="Seg_1677" s="T137">dempro-n:case</ta>
            <ta e="T139" id="Seg_1678" s="T138">n-n:case</ta>
            <ta e="T140" id="Seg_1679" s="T139">v-v:tense-v:pn</ta>
            <ta e="T141" id="Seg_1680" s="T140">n-n:case</ta>
            <ta e="T142" id="Seg_1681" s="T141">v-v:n.fin</ta>
            <ta e="T143" id="Seg_1682" s="T142">v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1683" s="T0">n</ta>
            <ta e="T2" id="Seg_1684" s="T1">n</ta>
            <ta e="T3" id="Seg_1685" s="T2">v</ta>
            <ta e="T4" id="Seg_1686" s="T3">n</ta>
            <ta e="T5" id="Seg_1687" s="T4">v</ta>
            <ta e="T6" id="Seg_1688" s="T5">dempro</ta>
            <ta e="T7" id="Seg_1689" s="T6">v</ta>
            <ta e="T8" id="Seg_1690" s="T7">v</ta>
            <ta e="T9" id="Seg_1691" s="T8">n</ta>
            <ta e="T10" id="Seg_1692" s="T9">v</ta>
            <ta e="T11" id="Seg_1693" s="T10">v</ta>
            <ta e="T12" id="Seg_1694" s="T11">n</ta>
            <ta e="T13" id="Seg_1695" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_1696" s="T13">v</ta>
            <ta e="T15" id="Seg_1697" s="T14">adv</ta>
            <ta e="T16" id="Seg_1698" s="T15">n</ta>
            <ta e="T17" id="Seg_1699" s="T16">v</ta>
            <ta e="T18" id="Seg_1700" s="T17">v</ta>
            <ta e="T19" id="Seg_1701" s="T18">n</ta>
            <ta e="T20" id="Seg_1702" s="T19">v</ta>
            <ta e="T21" id="Seg_1703" s="T20">n</ta>
            <ta e="T22" id="Seg_1704" s="T21">v</ta>
            <ta e="T23" id="Seg_1705" s="T22">v</ta>
            <ta e="T24" id="Seg_1706" s="T23">n</ta>
            <ta e="T25" id="Seg_1707" s="T24">n</ta>
            <ta e="T26" id="Seg_1708" s="T25">v</ta>
            <ta e="T27" id="Seg_1709" s="T26">n</ta>
            <ta e="T28" id="Seg_1710" s="T27">v</ta>
            <ta e="T29" id="Seg_1711" s="T28">n</ta>
            <ta e="T30" id="Seg_1712" s="T29">v</ta>
            <ta e="T31" id="Seg_1713" s="T30">v</ta>
            <ta e="T32" id="Seg_1714" s="T31">v</ta>
            <ta e="T33" id="Seg_1715" s="T32">n</ta>
            <ta e="T34" id="Seg_1716" s="T33">v</ta>
            <ta e="T35" id="Seg_1717" s="T34">n</ta>
            <ta e="T36" id="Seg_1718" s="T35">v</ta>
            <ta e="T37" id="Seg_1719" s="T36">dempro</ta>
            <ta e="T38" id="Seg_1720" s="T37">n</ta>
            <ta e="T39" id="Seg_1721" s="T38">v</ta>
            <ta e="T40" id="Seg_1722" s="T39">n</ta>
            <ta e="T41" id="Seg_1723" s="T40">n</ta>
            <ta e="T42" id="Seg_1724" s="T41">v</ta>
            <ta e="T43" id="Seg_1725" s="T42">n</ta>
            <ta e="T44" id="Seg_1726" s="T43">v</ta>
            <ta e="T45" id="Seg_1727" s="T44">v</ta>
            <ta e="T46" id="Seg_1728" s="T45">n</ta>
            <ta e="T47" id="Seg_1729" s="T46">ptcl</ta>
            <ta e="T48" id="Seg_1730" s="T47">v</ta>
            <ta e="T49" id="Seg_1731" s="T48">dempro</ta>
            <ta e="T50" id="Seg_1732" s="T49">n</ta>
            <ta e="T51" id="Seg_1733" s="T50">v</ta>
            <ta e="T52" id="Seg_1734" s="T51">v</ta>
            <ta e="T53" id="Seg_1735" s="T52">ptcl</ta>
            <ta e="T54" id="Seg_1736" s="T53">v</ta>
            <ta e="T55" id="Seg_1737" s="T54">n</ta>
            <ta e="T56" id="Seg_1738" s="T55">v</ta>
            <ta e="T57" id="Seg_1739" s="T56">n</ta>
            <ta e="T58" id="Seg_1740" s="T57">v</ta>
            <ta e="T59" id="Seg_1741" s="T58">n</ta>
            <ta e="T60" id="Seg_1742" s="T59">v</ta>
            <ta e="T61" id="Seg_1743" s="T60">v</ta>
            <ta e="T62" id="Seg_1744" s="T61">v</ta>
            <ta e="T63" id="Seg_1745" s="T62">n</ta>
            <ta e="T64" id="Seg_1746" s="T63">v</ta>
            <ta e="T65" id="Seg_1747" s="T64">n</ta>
            <ta e="T66" id="Seg_1748" s="T65">v</ta>
            <ta e="T67" id="Seg_1749" s="T66">v</ta>
            <ta e="T68" id="Seg_1750" s="T67">dempro</ta>
            <ta e="T69" id="Seg_1751" s="T68">n</ta>
            <ta e="T70" id="Seg_1752" s="T69">dempro</ta>
            <ta e="T71" id="Seg_1753" s="T70">v</ta>
            <ta e="T72" id="Seg_1754" s="T71">n</ta>
            <ta e="T73" id="Seg_1755" s="T72">v</ta>
            <ta e="T74" id="Seg_1756" s="T73">que</ta>
            <ta e="T75" id="Seg_1757" s="T74">n</ta>
            <ta e="T76" id="Seg_1758" s="T75">v</ta>
            <ta e="T77" id="Seg_1759" s="T76">v</ta>
            <ta e="T78" id="Seg_1760" s="T77">dempro</ta>
            <ta e="T79" id="Seg_1761" s="T78">n</ta>
            <ta e="T80" id="Seg_1762" s="T79">v</ta>
            <ta e="T81" id="Seg_1763" s="T80">n</ta>
            <ta e="T82" id="Seg_1764" s="T81">n</ta>
            <ta e="T83" id="Seg_1765" s="T82">dempro</ta>
            <ta e="T84" id="Seg_1766" s="T83">n</ta>
            <ta e="T85" id="Seg_1767" s="T84">n</ta>
            <ta e="T86" id="Seg_1768" s="T85">v</ta>
            <ta e="T87" id="Seg_1769" s="T86">dempro</ta>
            <ta e="T88" id="Seg_1770" s="T87">v</ta>
            <ta e="T89" id="Seg_1771" s="T88">v</ta>
            <ta e="T90" id="Seg_1772" s="T89">v</ta>
            <ta e="T91" id="Seg_1773" s="T90">v</ta>
            <ta e="T92" id="Seg_1774" s="T91">v</ta>
            <ta e="T93" id="Seg_1775" s="T92">dempro</ta>
            <ta e="T94" id="Seg_1776" s="T93">v</ta>
            <ta e="T95" id="Seg_1777" s="T94">n</ta>
            <ta e="T96" id="Seg_1778" s="T95">n</ta>
            <ta e="T97" id="Seg_1779" s="T96">v</ta>
            <ta e="T98" id="Seg_1780" s="T97">n</ta>
            <ta e="T99" id="Seg_1781" s="T98">v</ta>
            <ta e="T100" id="Seg_1782" s="T99">adj</ta>
            <ta e="T101" id="Seg_1783" s="T100">n</ta>
            <ta e="T102" id="Seg_1784" s="T101">v</ta>
            <ta e="T103" id="Seg_1785" s="T102">quant</ta>
            <ta e="T104" id="Seg_1786" s="T103">n</ta>
            <ta e="T105" id="Seg_1787" s="T104">v</ta>
            <ta e="T106" id="Seg_1788" s="T105">pers</ta>
            <ta e="T107" id="Seg_1789" s="T106">que</ta>
            <ta e="T108" id="Seg_1790" s="T107">v</ta>
            <ta e="T109" id="Seg_1791" s="T108">v</ta>
            <ta e="T110" id="Seg_1792" s="T109">pers</ta>
            <ta e="T111" id="Seg_1793" s="T110">n</ta>
            <ta e="T112" id="Seg_1794" s="T111">v</ta>
            <ta e="T113" id="Seg_1795" s="T112">v</ta>
            <ta e="T114" id="Seg_1796" s="T113">pers</ta>
            <ta e="T115" id="Seg_1797" s="T114">pers</ta>
            <ta e="T116" id="Seg_1798" s="T115">n</ta>
            <ta e="T117" id="Seg_1799" s="T116">v</ta>
            <ta e="T118" id="Seg_1800" s="T117">v</ta>
            <ta e="T119" id="Seg_1801" s="T118">v</ta>
            <ta e="T120" id="Seg_1802" s="T119">v</ta>
            <ta e="T121" id="Seg_1803" s="T120">v</ta>
            <ta e="T122" id="Seg_1804" s="T121">v</ta>
            <ta e="T123" id="Seg_1805" s="T122">v</ta>
            <ta e="T124" id="Seg_1806" s="T123">dempro</ta>
            <ta e="T125" id="Seg_1807" s="T124">v</ta>
            <ta e="T126" id="Seg_1808" s="T125">que</ta>
            <ta e="T127" id="Seg_1809" s="T126">v</ta>
            <ta e="T128" id="Seg_1810" s="T127">v</ta>
            <ta e="T129" id="Seg_1811" s="T128">v</ta>
            <ta e="T130" id="Seg_1812" s="T129">pers</ta>
            <ta e="T131" id="Seg_1813" s="T130">ptcl</ta>
            <ta e="T132" id="Seg_1814" s="T131">v</ta>
            <ta e="T133" id="Seg_1815" s="T132">pers</ta>
            <ta e="T134" id="Seg_1816" s="T133">adj</ta>
            <ta e="T135" id="Seg_1817" s="T134">n</ta>
            <ta e="T136" id="Seg_1818" s="T135">v</ta>
            <ta e="T137" id="Seg_1819" s="T136">v</ta>
            <ta e="T138" id="Seg_1820" s="T137">dempro</ta>
            <ta e="T139" id="Seg_1821" s="T138">n</ta>
            <ta e="T140" id="Seg_1822" s="T139">v</ta>
            <ta e="T141" id="Seg_1823" s="T140">n</ta>
            <ta e="T142" id="Seg_1824" s="T141">v</ta>
            <ta e="T143" id="Seg_1825" s="T142">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_1826" s="T0">np.h:Th</ta>
            <ta e="T2" id="Seg_1827" s="T1">np.h:Com 0.3.h:Poss</ta>
            <ta e="T4" id="Seg_1828" s="T3">np.h:Th 0.3.h:Poss</ta>
            <ta e="T6" id="Seg_1829" s="T5">pro.h:A</ta>
            <ta e="T9" id="Seg_1830" s="T8">np.h:Th 0.3.h:Poss</ta>
            <ta e="T12" id="Seg_1831" s="T11">np.h:A 0.3.h:Poss</ta>
            <ta e="T15" id="Seg_1832" s="T14">adv:Time</ta>
            <ta e="T16" id="Seg_1833" s="T15">np:Th</ta>
            <ta e="T17" id="Seg_1834" s="T16">0.3.h:A</ta>
            <ta e="T18" id="Seg_1835" s="T17">0.3.h:A</ta>
            <ta e="T19" id="Seg_1836" s="T18">np:Th</ta>
            <ta e="T20" id="Seg_1837" s="T19">0.3.h:A</ta>
            <ta e="T21" id="Seg_1838" s="T20">np:P 0.3.h:Poss</ta>
            <ta e="T23" id="Seg_1839" s="T22">0.3.h:A</ta>
            <ta e="T24" id="Seg_1840" s="T23">np:G 0.3.h:Poss</ta>
            <ta e="T25" id="Seg_1841" s="T24">np:P</ta>
            <ta e="T26" id="Seg_1842" s="T25">0.3.h:A</ta>
            <ta e="T27" id="Seg_1843" s="T26">np:Th</ta>
            <ta e="T29" id="Seg_1844" s="T28">np:Th 0.3.h:Poss</ta>
            <ta e="T30" id="Seg_1845" s="T29">0.3.h:A</ta>
            <ta e="T32" id="Seg_1846" s="T31">0.3.h:A</ta>
            <ta e="T33" id="Seg_1847" s="T32">np:Th</ta>
            <ta e="T34" id="Seg_1848" s="T33">0.3.h:A</ta>
            <ta e="T35" id="Seg_1849" s="T34">np:Th 0.3.h:Poss</ta>
            <ta e="T36" id="Seg_1850" s="T35">0.3.h:A</ta>
            <ta e="T38" id="Seg_1851" s="T37">np.h:A</ta>
            <ta e="T40" id="Seg_1852" s="T39">np:G 0.3.h:Poss</ta>
            <ta e="T41" id="Seg_1853" s="T40">np:G 0.3.h:Poss</ta>
            <ta e="T42" id="Seg_1854" s="T41">0.3.h:A</ta>
            <ta e="T43" id="Seg_1855" s="T42">np.h:R 0.3.h:Poss</ta>
            <ta e="T45" id="Seg_1856" s="T44">0.2.h:A</ta>
            <ta e="T46" id="Seg_1857" s="T45">np.h:A 0.3.h:Poss</ta>
            <ta e="T49" id="Seg_1858" s="T48">pro.h:A</ta>
            <ta e="T50" id="Seg_1859" s="T49">np:Th</ta>
            <ta e="T52" id="Seg_1860" s="T51">0.3.h:A</ta>
            <ta e="T54" id="Seg_1861" s="T53">0.3.h:A</ta>
            <ta e="T55" id="Seg_1862" s="T54">np:Th</ta>
            <ta e="T56" id="Seg_1863" s="T55">0.3.h:A</ta>
            <ta e="T57" id="Seg_1864" s="T56">np.h:P 0.3.h:Poss</ta>
            <ta e="T58" id="Seg_1865" s="T57">0.3.h:A</ta>
            <ta e="T59" id="Seg_1866" s="T58">np.h:E 0.3.h:Poss</ta>
            <ta e="T62" id="Seg_1867" s="T61">0.3.h:A</ta>
            <ta e="T63" id="Seg_1868" s="T62">np.h:P</ta>
            <ta e="T65" id="Seg_1869" s="T64">np.h:A</ta>
            <ta e="T67" id="Seg_1870" s="T66">0.3.h:A</ta>
            <ta e="T69" id="Seg_1871" s="T68">np:G</ta>
            <ta e="T70" id="Seg_1872" s="T69">np.h:A</ta>
            <ta e="T72" id="Seg_1873" s="T71">np:G</ta>
            <ta e="T73" id="Seg_1874" s="T72">0.3.h:A 0.3.h:R</ta>
            <ta e="T75" id="Seg_1875" s="T74">np.h:Th 0.3.h:Poss</ta>
            <ta e="T77" id="Seg_1876" s="T76">0.3.h:A</ta>
            <ta e="T79" id="Seg_1877" s="T78">np.h:A</ta>
            <ta e="T82" id="Seg_1878" s="T81">np:G</ta>
            <ta e="T83" id="Seg_1879" s="T82">pro.h:A</ta>
            <ta e="T84" id="Seg_1880" s="T83">np:Ins</ta>
            <ta e="T85" id="Seg_1881" s="T84">np:G 0.3.h:Poss</ta>
            <ta e="T87" id="Seg_1882" s="T86">pro.h:A</ta>
            <ta e="T90" id="Seg_1883" s="T89">0.3.h:E</ta>
            <ta e="T92" id="Seg_1884" s="T91">0.3.h:A</ta>
            <ta e="T93" id="Seg_1885" s="T92">pro.h:P</ta>
            <ta e="T94" id="Seg_1886" s="T93">0.3.h:A</ta>
            <ta e="T95" id="Seg_1887" s="T94">np:Poss</ta>
            <ta e="T96" id="Seg_1888" s="T95">np:G</ta>
            <ta e="T97" id="Seg_1889" s="T96">0.3.h:A</ta>
            <ta e="T98" id="Seg_1890" s="T97">np:P</ta>
            <ta e="T99" id="Seg_1891" s="T98">0.3.h:A</ta>
            <ta e="T101" id="Seg_1892" s="T100">np.h:A</ta>
            <ta e="T104" id="Seg_1893" s="T103">np:P 0.3.h:Poss</ta>
            <ta e="T105" id="Seg_1894" s="T104">0.3.h:A</ta>
            <ta e="T106" id="Seg_1895" s="T105">pro.h:Th</ta>
            <ta e="T110" id="Seg_1896" s="T109">pro.h:Poss</ta>
            <ta e="T111" id="Seg_1897" s="T110">np:P</ta>
            <ta e="T112" id="Seg_1898" s="T111">0.1.h:A</ta>
            <ta e="T113" id="Seg_1899" s="T112">0.2.h:A</ta>
            <ta e="T114" id="Seg_1900" s="T113">pro.h:P</ta>
            <ta e="T115" id="Seg_1901" s="T114">pro.h:Poss</ta>
            <ta e="T116" id="Seg_1902" s="T115">np:P</ta>
            <ta e="T117" id="Seg_1903" s="T116">0.3.h:A</ta>
            <ta e="T118" id="Seg_1904" s="T117">0.2.h:A</ta>
            <ta e="T119" id="Seg_1905" s="T118">0.3.h:A</ta>
            <ta e="T120" id="Seg_1906" s="T119">0.2.h:A</ta>
            <ta e="T121" id="Seg_1907" s="T120">0.1.h:A</ta>
            <ta e="T122" id="Seg_1908" s="T121">0.3.h:A</ta>
            <ta e="T123" id="Seg_1909" s="T122">0.3.h:A</ta>
            <ta e="T124" id="Seg_1910" s="T123">pro.h:A</ta>
            <ta e="T129" id="Seg_1911" s="T128">0.2.h:A</ta>
            <ta e="T130" id="Seg_1912" s="T129">pro.h:A</ta>
            <ta e="T133" id="Seg_1913" s="T132">pro.h:Th</ta>
            <ta e="T137" id="Seg_1914" s="T136">0.3.h:A</ta>
            <ta e="T138" id="Seg_1915" s="T137">pro.h:Th</ta>
            <ta e="T139" id="Seg_1916" s="T138">np:G</ta>
            <ta e="T140" id="Seg_1917" s="T139">0.3.h:A</ta>
            <ta e="T141" id="Seg_1918" s="T140">np:G</ta>
            <ta e="T143" id="Seg_1919" s="T142">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_1920" s="T0">np.h:S</ta>
            <ta e="T3" id="Seg_1921" s="T2">v:pred</ta>
            <ta e="T4" id="Seg_1922" s="T3">np.h:S</ta>
            <ta e="T5" id="Seg_1923" s="T4">v:pred</ta>
            <ta e="T6" id="Seg_1924" s="T5">pro.h:S</ta>
            <ta e="T7" id="Seg_1925" s="T6">s:purp</ta>
            <ta e="T8" id="Seg_1926" s="T7">v:pred</ta>
            <ta e="T9" id="Seg_1927" s="T8">np.h:O</ta>
            <ta e="T10" id="Seg_1928" s="T9">s:purp</ta>
            <ta e="T11" id="Seg_1929" s="T10">v:pred 0.3.h:S</ta>
            <ta e="T12" id="Seg_1930" s="T11">np.h:S</ta>
            <ta e="T13" id="Seg_1931" s="T12">ptcl.neg</ta>
            <ta e="T14" id="Seg_1932" s="T13">v:pred</ta>
            <ta e="T16" id="Seg_1933" s="T15">np:O</ta>
            <ta e="T17" id="Seg_1934" s="T16">v:pred 0.3.h:S</ta>
            <ta e="T18" id="Seg_1935" s="T17">v:pred 0.3.h:S</ta>
            <ta e="T19" id="Seg_1936" s="T18">np:O</ta>
            <ta e="T20" id="Seg_1937" s="T19">v:pred 0.3.h:S</ta>
            <ta e="T21" id="Seg_1938" s="T20">np:O</ta>
            <ta e="T23" id="Seg_1939" s="T22">v:pred 0.3.h:S</ta>
            <ta e="T25" id="Seg_1940" s="T24">np:O</ta>
            <ta e="T26" id="Seg_1941" s="T25">v:pred 0.3.h:S</ta>
            <ta e="T27" id="Seg_1942" s="T26">np:S</ta>
            <ta e="T28" id="Seg_1943" s="T27">v:pred</ta>
            <ta e="T29" id="Seg_1944" s="T28">np:O</ta>
            <ta e="T30" id="Seg_1945" s="T29">v:pred 0.3.h:S</ta>
            <ta e="T31" id="Seg_1946" s="T30">s:purp</ta>
            <ta e="T32" id="Seg_1947" s="T31">v:pred 0.3.h:S</ta>
            <ta e="T33" id="Seg_1948" s="T32">np:O</ta>
            <ta e="T34" id="Seg_1949" s="T33">v:pred 0.3.h:S</ta>
            <ta e="T35" id="Seg_1950" s="T34">np:O</ta>
            <ta e="T36" id="Seg_1951" s="T35">v:pred 0.3.h:S</ta>
            <ta e="T38" id="Seg_1952" s="T37">np.h:S</ta>
            <ta e="T39" id="Seg_1953" s="T38">v:pred</ta>
            <ta e="T42" id="Seg_1954" s="T40">s:temp</ta>
            <ta e="T44" id="Seg_1955" s="T43">s:purp</ta>
            <ta e="T45" id="Seg_1956" s="T44">v:pred 0.2.h:S</ta>
            <ta e="T46" id="Seg_1957" s="T45">np.h:S</ta>
            <ta e="T47" id="Seg_1958" s="T46">ptcl.neg</ta>
            <ta e="T48" id="Seg_1959" s="T47">v:pred</ta>
            <ta e="T49" id="Seg_1960" s="T48">pro.h:S</ta>
            <ta e="T50" id="Seg_1961" s="T49">np:O</ta>
            <ta e="T51" id="Seg_1962" s="T50">v:pred</ta>
            <ta e="T52" id="Seg_1963" s="T51">v:pred 0.3.h:S</ta>
            <ta e="T54" id="Seg_1964" s="T53">v:pred 0.3.h:S</ta>
            <ta e="T55" id="Seg_1965" s="T54">np:O</ta>
            <ta e="T56" id="Seg_1966" s="T55">v:pred 0.3.h:S</ta>
            <ta e="T57" id="Seg_1967" s="T56">np.h:O</ta>
            <ta e="T58" id="Seg_1968" s="T57">v:pred 0.3.h:S</ta>
            <ta e="T59" id="Seg_1969" s="T58">np.h:S</ta>
            <ta e="T60" id="Seg_1970" s="T59">s:adv</ta>
            <ta e="T61" id="Seg_1971" s="T60">v:pred</ta>
            <ta e="T62" id="Seg_1972" s="T61">v:pred 0.3.h:S</ta>
            <ta e="T64" id="Seg_1973" s="T62">s:purp</ta>
            <ta e="T65" id="Seg_1974" s="T64">np.h:S</ta>
            <ta e="T66" id="Seg_1975" s="T65">v:pred</ta>
            <ta e="T67" id="Seg_1976" s="T66">v:pred 0.3.h:S</ta>
            <ta e="T70" id="Seg_1977" s="T69">pro.h:S</ta>
            <ta e="T71" id="Seg_1978" s="T70">v:pred</ta>
            <ta e="T73" id="Seg_1979" s="T72">v:pred 0.3.h:S 0.3.h:O</ta>
            <ta e="T74" id="Seg_1980" s="T73">adv:pred</ta>
            <ta e="T75" id="Seg_1981" s="T74">np.h:S</ta>
            <ta e="T77" id="Seg_1982" s="T76">v:pred 0.3.h:S</ta>
            <ta e="T79" id="Seg_1983" s="T78">np.h:S</ta>
            <ta e="T80" id="Seg_1984" s="T79">v:pred</ta>
            <ta e="T83" id="Seg_1985" s="T82">pro.h:S</ta>
            <ta e="T86" id="Seg_1986" s="T85">v:pred</ta>
            <ta e="T87" id="Seg_1987" s="T86">pro.h:S</ta>
            <ta e="T89" id="Seg_1988" s="T88">v:pred</ta>
            <ta e="T90" id="Seg_1989" s="T89">s:temp</ta>
            <ta e="T91" id="Seg_1990" s="T90">s:adv</ta>
            <ta e="T92" id="Seg_1991" s="T91">v:pred 0.3.h:S</ta>
            <ta e="T93" id="Seg_1992" s="T92">pro.h:O</ta>
            <ta e="T94" id="Seg_1993" s="T93">v:pred 0.3.h:S</ta>
            <ta e="T97" id="Seg_1994" s="T96">v:pred 0.3.h:S</ta>
            <ta e="T98" id="Seg_1995" s="T97">np:O</ta>
            <ta e="T99" id="Seg_1996" s="T98">v:pred 0.3.h:S</ta>
            <ta e="T101" id="Seg_1997" s="T100">np:S</ta>
            <ta e="T102" id="Seg_1998" s="T101">v:pred</ta>
            <ta e="T104" id="Seg_1999" s="T103">np:O</ta>
            <ta e="T105" id="Seg_2000" s="T104">v:pred 0.3.h:S</ta>
            <ta e="T106" id="Seg_2001" s="T105">pro.h:S</ta>
            <ta e="T108" id="Seg_2002" s="T107">conv:pred</ta>
            <ta e="T109" id="Seg_2003" s="T108">v:pred</ta>
            <ta e="T111" id="Seg_2004" s="T110">np:O</ta>
            <ta e="T112" id="Seg_2005" s="T111">v:pred 0.1.h:S</ta>
            <ta e="T113" id="Seg_2006" s="T112">v:pred 0.2.h:S</ta>
            <ta e="T114" id="Seg_2007" s="T113">pro.h:O</ta>
            <ta e="T116" id="Seg_2008" s="T115">np:O</ta>
            <ta e="T117" id="Seg_2009" s="T116">v:pred 0.3.h:S</ta>
            <ta e="T118" id="Seg_2010" s="T117">v:pred 0.2.h:S</ta>
            <ta e="T119" id="Seg_2011" s="T118">v:pred 0.3.h:S</ta>
            <ta e="T120" id="Seg_2012" s="T119">v:pred 0.2.h:S</ta>
            <ta e="T121" id="Seg_2013" s="T120">v:pred 0.1.h:S</ta>
            <ta e="T122" id="Seg_2014" s="T121">v:pred 0.3.h:S</ta>
            <ta e="T123" id="Seg_2015" s="T122">v:pred 0.3.h:S</ta>
            <ta e="T124" id="Seg_2016" s="T123">pro.h:S</ta>
            <ta e="T125" id="Seg_2017" s="T124">v:pred</ta>
            <ta e="T127" id="Seg_2018" s="T126">conv:pred</ta>
            <ta e="T128" id="Seg_2019" s="T127">s:adv</ta>
            <ta e="T129" id="Seg_2020" s="T128">v:pred 0.2.h:S</ta>
            <ta e="T130" id="Seg_2021" s="T129">pro.h:S</ta>
            <ta e="T131" id="Seg_2022" s="T130">ptcl.neg</ta>
            <ta e="T132" id="Seg_2023" s="T131">v:pred</ta>
            <ta e="T133" id="Seg_2024" s="T132">pro.h:S</ta>
            <ta e="T135" id="Seg_2025" s="T134">n:pred</ta>
            <ta e="T136" id="Seg_2026" s="T135">cop</ta>
            <ta e="T137" id="Seg_2027" s="T136">v:pred 0.3.h:S</ta>
            <ta e="T138" id="Seg_2028" s="T137">pro.h:O</ta>
            <ta e="T140" id="Seg_2029" s="T139">v:pred 0.3.h:S</ta>
            <ta e="T143" id="Seg_2030" s="T142">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T1" id="Seg_2031" s="T0">new </ta>
            <ta e="T2" id="Seg_2032" s="T1">new </ta>
            <ta e="T4" id="Seg_2033" s="T3">new </ta>
            <ta e="T6" id="Seg_2034" s="T5">giv-active </ta>
            <ta e="T9" id="Seg_2035" s="T8">giv-inactive </ta>
            <ta e="T12" id="Seg_2036" s="T11">giv-active </ta>
            <ta e="T16" id="Seg_2037" s="T15">new </ta>
            <ta e="T17" id="Seg_2038" s="T16">0.giv-inactive</ta>
            <ta e="T18" id="Seg_2039" s="T17">0.giv-active</ta>
            <ta e="T19" id="Seg_2040" s="T18">new </ta>
            <ta e="T20" id="Seg_2041" s="T19">0.giv-active</ta>
            <ta e="T21" id="Seg_2042" s="T20">accs-inf </ta>
            <ta e="T23" id="Seg_2043" s="T22">0.giv-active</ta>
            <ta e="T24" id="Seg_2044" s="T23">giv-active </ta>
            <ta e="T25" id="Seg_2045" s="T24">new </ta>
            <ta e="T26" id="Seg_2046" s="T25">0.giv-active</ta>
            <ta e="T27" id="Seg_2047" s="T26">giv-active </ta>
            <ta e="T29" id="Seg_2048" s="T28">new </ta>
            <ta e="T30" id="Seg_2049" s="T29">0.giv-inactive</ta>
            <ta e="T32" id="Seg_2050" s="T31">0.giv-active</ta>
            <ta e="T33" id="Seg_2051" s="T32">accs-sit </ta>
            <ta e="T34" id="Seg_2052" s="T33">0.giv-active</ta>
            <ta e="T35" id="Seg_2053" s="T34">giv-active </ta>
            <ta e="T36" id="Seg_2054" s="T35">0.giv-active</ta>
            <ta e="T38" id="Seg_2055" s="T37">giv-inactive </ta>
            <ta e="T40" id="Seg_2056" s="T39">accs-sit</ta>
            <ta e="T41" id="Seg_2057" s="T40">giv-active </ta>
            <ta e="T42" id="Seg_2058" s="T41">0.giv-active</ta>
            <ta e="T43" id="Seg_2059" s="T42">giv-inactive </ta>
            <ta e="T45" id="Seg_2060" s="T44">0.giv-active-Q</ta>
            <ta e="T46" id="Seg_2061" s="T45">giv-inactive</ta>
            <ta e="T49" id="Seg_2062" s="T48">giv-inactive </ta>
            <ta e="T50" id="Seg_2063" s="T49">new </ta>
            <ta e="T52" id="Seg_2064" s="T51">0.giv-active</ta>
            <ta e="T54" id="Seg_2065" s="T53">0.giv-active</ta>
            <ta e="T55" id="Seg_2066" s="T54">new </ta>
            <ta e="T56" id="Seg_2067" s="T55">0.giv-inactive</ta>
            <ta e="T57" id="Seg_2068" s="T56">giv-inactive </ta>
            <ta e="T58" id="Seg_2069" s="T57">0.giv-active</ta>
            <ta e="T59" id="Seg_2070" s="T58">giv-active </ta>
            <ta e="T62" id="Seg_2071" s="T61">0.giv-inactive</ta>
            <ta e="T63" id="Seg_2072" s="T62">new </ta>
            <ta e="T65" id="Seg_2073" s="T64">giv-active </ta>
            <ta e="T67" id="Seg_2074" s="T66">0.giv-active</ta>
            <ta e="T69" id="Seg_2075" s="T68">giv-active </ta>
            <ta e="T70" id="Seg_2076" s="T69">giv-active </ta>
            <ta e="T72" id="Seg_2077" s="T71">accs-inf </ta>
            <ta e="T73" id="Seg_2078" s="T72">0.giv-inactive quot-sp</ta>
            <ta e="T75" id="Seg_2079" s="T74">giv-inactive-Q </ta>
            <ta e="T77" id="Seg_2080" s="T76">0.giv-active-Q</ta>
            <ta e="T79" id="Seg_2081" s="T78">giv-inactive </ta>
            <ta e="T82" id="Seg_2082" s="T81">accs-inf </ta>
            <ta e="T83" id="Seg_2083" s="T82">giv-inactive</ta>
            <ta e="T84" id="Seg_2084" s="T83">new </ta>
            <ta e="T85" id="Seg_2085" s="T84">accs-inf </ta>
            <ta e="T87" id="Seg_2086" s="T86">giv-inactive </ta>
            <ta e="T90" id="Seg_2087" s="T89">0.accs-aggr</ta>
            <ta e="T92" id="Seg_2088" s="T91">0.giv-inactive</ta>
            <ta e="T93" id="Seg_2089" s="T92">giv-active </ta>
            <ta e="T94" id="Seg_2090" s="T93">0.giv-active</ta>
            <ta e="T96" id="Seg_2091" s="T95">accs-gen </ta>
            <ta e="T97" id="Seg_2092" s="T96">0.giv-active</ta>
            <ta e="T98" id="Seg_2093" s="T97">new </ta>
            <ta e="T99" id="Seg_2094" s="T98">0.giv-active</ta>
            <ta e="T101" id="Seg_2095" s="T100">new </ta>
            <ta e="T104" id="Seg_2096" s="T103">accs-inf </ta>
            <ta e="T105" id="Seg_2097" s="T104">0.giv-active</ta>
            <ta e="T106" id="Seg_2098" s="T105">giv-inactive-Q </ta>
            <ta e="T111" id="Seg_2099" s="T110">giv-inactive-Q</ta>
            <ta e="T112" id="Seg_2100" s="T111">0.giv-active-Q</ta>
            <ta e="T113" id="Seg_2101" s="T112">0.giv-active-Q</ta>
            <ta e="T114" id="Seg_2102" s="T113">giv-active-Q </ta>
            <ta e="T116" id="Seg_2103" s="T115">giv-active-Q </ta>
            <ta e="T117" id="Seg_2104" s="T116">0.giv-inactive-Q</ta>
            <ta e="T118" id="Seg_2105" s="T117">0.giv-active-Q</ta>
            <ta e="T119" id="Seg_2106" s="T118">0.giv-active</ta>
            <ta e="T120" id="Seg_2107" s="T119">0.giv-active-Q</ta>
            <ta e="T121" id="Seg_2108" s="T120">0.giv-active-Q</ta>
            <ta e="T122" id="Seg_2109" s="T121">0.giv-active</ta>
            <ta e="T123" id="Seg_2110" s="T122">0.giv-inactive</ta>
            <ta e="T124" id="Seg_2111" s="T123">giv-inactive </ta>
            <ta e="T129" id="Seg_2112" s="T128">0.giv-inactive-Q</ta>
            <ta e="T130" id="Seg_2113" s="T129">giv-active-Q </ta>
            <ta e="T133" id="Seg_2114" s="T132">giv-active-Q </ta>
            <ta e="T135" id="Seg_2115" s="T134">accs-sit-Q </ta>
            <ta e="T137" id="Seg_2116" s="T136">0.giv-inactive</ta>
            <ta e="T138" id="Seg_2117" s="T137">giv-active </ta>
            <ta e="T139" id="Seg_2118" s="T138">giv-inactive </ta>
            <ta e="T140" id="Seg_2119" s="T139">0.giv-active</ta>
            <ta e="T141" id="Seg_2120" s="T140">giv-inactive </ta>
            <ta e="T143" id="Seg_2121" s="T142">0.giv-active</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T4" id="Seg_2122" s="T3">MONG:core</ta>
            <ta e="T72" id="Seg_2123" s="T71">RUS:cult</ta>
            <ta e="T81" id="Seg_2124" s="T80">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T81" id="Seg_2125" s="T80">Csub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T81" id="Seg_2126" s="T80">dir:infl</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_2127" s="T0">Жила жена со своим мужем.</ta>
            <ta e="T5" id="Seg_2128" s="T3">Был у них друг.</ta>
            <ta e="T8" id="Seg_2129" s="T5">Пришел он к ним в гости.</ta>
            <ta e="T11" id="Seg_2130" s="T8">[Муж] жену за водой послал.</ta>
            <ta e="T14" id="Seg_2131" s="T11">Жена не идет.</ta>
            <ta e="T18" id="Seg_2132" s="T14">Схватил хлыст, ударил ее.</ta>
            <ta e="T20" id="Seg_2133" s="T18">Нож схватил.</ta>
            <ta e="T23" id="Seg_2134" s="T20">Живот ей разрезал.</ta>
            <ta e="T26" id="Seg_2135" s="T23">Он ей к животу [заранее пузырь с] кровью привязал.</ta>
            <ta e="T28" id="Seg_2136" s="T26">Кровь потекла.</ta>
            <ta e="T32" id="Seg_2137" s="T28">Она схватила свой котелок, побежала воду набирать.</ta>
            <ta e="T36" id="Seg_2138" s="T32">Воды принесла, котёл вскипятила.</ta>
            <ta e="T40" id="Seg_2139" s="T36">Тот человек домой вернулся.</ta>
            <ta e="T43" id="Seg_2140" s="T40">Домой придя, жене [говорит]: </ta>
            <ta e="T45" id="Seg_2141" s="T43">"Иди воды принеси!"</ta>
            <ta e="T48" id="Seg_2142" s="T45">Жена его не идет.</ta>
            <ta e="T52" id="Seg_2143" s="T48">Он схватил хлыст и ударил её.</ta>
            <ta e="T54" id="Seg_2144" s="T52">Не идет она.</ta>
            <ta e="T58" id="Seg_2145" s="T54">Нож схватил и жену зарезал.</ta>
            <ta e="T61" id="Seg_2146" s="T58">Жена умерла не пошевелившись.</ta>
            <ta e="T64" id="Seg_2147" s="T61">Пошёл народ собирать.</ta>
            <ta e="T69" id="Seg_2148" s="T64">Народ собрал, к тому [первому] человеку пришел.</ta>
            <ta e="T72" id="Seg_2149" s="T69">Тот в подвале спрятался.</ta>
            <ta e="T73" id="Seg_2150" s="T72">Спрашивают [жену]:</ta>
            <ta e="T75" id="Seg_2151" s="T73">"Где твой муж?"</ta>
            <ta e="T77" id="Seg_2152" s="T75">"Ушёл."</ta>
            <ta e="T82" id="Seg_2153" s="T77">Этот человек сел на крышку подвала.</ta>
            <ta e="T86" id="Seg_2154" s="T82">А тот [первый] его шилом в зад уколол.</ta>
            <ta e="T89" id="Seg_2155" s="T86">Этот [крышку] открыл.</ta>
            <ta e="T92" id="Seg_2156" s="T89">Видят, тот сидит и смеётся.</ta>
            <ta e="T97" id="Seg_2157" s="T92">Его связали, на берег реки привели.</ta>
            <ta e="T99" id="Seg_2158" s="T97">Строят плот.</ta>
            <ta e="T102" id="Seg_2159" s="T99">Пришел одноглазый человек.</ta>
            <ta e="T105" id="Seg_2160" s="T102">[Тот] один свой глаз закрыл.</ta>
            <ta e="T109" id="Seg_2161" s="T105">"Ты чего тут лежишь?"</ta>
            <ta e="T112" id="Seg_2162" s="T109">"Я [так] глаз вылечу.</ta>
            <ta e="T117" id="Seg_2163" s="T112">Отпусти меня, твой глаз [вместо моего] вылечится.</ta>
            <ta e="T118" id="Seg_2164" s="T117">Развяжи!"</ta>
            <ta e="T119" id="Seg_2165" s="T118">Тот развязал [его].</ta>
            <ta e="T121" id="Seg_2166" s="T119">"Ложись, привяжу тебя!"</ta>
            <ta e="T122" id="Seg_2167" s="T121">Тот лёг.</ta>
            <ta e="T123" id="Seg_2168" s="T122"> Он привязал [его].</ta>
            <ta e="T125" id="Seg_2169" s="T123">Пришли [люди].</ta>
            <ta e="T129" id="Seg_2170" s="T125">"Ты зачем лежишь притворяешься?"</ta>
            <ta e="T132" id="Seg_2171" s="T129">"Я не притворяюсь.</ta>
            <ta e="T136" id="Seg_2172" s="T132">Я одноглазый."</ta>
            <ta e="T143" id="Seg_2173" s="T136">Взяли они этого [одноглазого], привязали к плоту и по реке пустили.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_2174" s="T0">A woman lived with her husband.</ta>
            <ta e="T5" id="Seg_2175" s="T3">They had a friend.</ta>
            <ta e="T8" id="Seg_2176" s="T5">He came visiting.</ta>
            <ta e="T11" id="Seg_2177" s="T8">[He] sent his wife to fetch water.</ta>
            <ta e="T14" id="Seg_2178" s="T11">His wife doesn’t go.</ta>
            <ta e="T18" id="Seg_2179" s="T14">Then he seized a whip and hit her.</ta>
            <ta e="T20" id="Seg_2180" s="T18">He seized a knife.</ta>
            <ta e="T23" id="Seg_2181" s="T20">He stabbed her belly open.</ta>
            <ta e="T26" id="Seg_2182" s="T23">He [had] tied [a bladder of] blood to her belly.</ta>
            <ta e="T28" id="Seg_2183" s="T26">The blood ran.</ta>
            <ta e="T32" id="Seg_2184" s="T28">She seized her cauldron and ran fetching water.</ta>
            <ta e="T36" id="Seg_2185" s="T32">She brought water, made her cauldron boil.</ta>
            <ta e="T40" id="Seg_2186" s="T36">This man returned home.</ta>
            <ta e="T43" id="Seg_2187" s="T40">Having returned home, [he says] to his wife:</ta>
            <ta e="T45" id="Seg_2188" s="T43">"Go, get water!"</ta>
            <ta e="T48" id="Seg_2189" s="T45">His wife doesn’t go.</ta>
            <ta e="T52" id="Seg_2190" s="T48">He seized a whip and hit her.</ta>
            <ta e="T54" id="Seg_2191" s="T52">She doesn’t go.</ta>
            <ta e="T58" id="Seg_2192" s="T54">He seized a knife and stabbed his wife.</ta>
            <ta e="T61" id="Seg_2193" s="T58">His wife died without making a move.</ta>
            <ta e="T64" id="Seg_2194" s="T61">​He went to gather people.</ta>
            <ta e="T69" id="Seg_2195" s="T64">The people gathered, they came to that man.</ta>
            <ta e="T72" id="Seg_2196" s="T69">He hid in the cellar.</ta>
            <ta e="T73" id="Seg_2197" s="T72">They ask [his wife]:</ta>
            <ta e="T75" id="Seg_2198" s="T73">"Where’s your husband?"</ta>
            <ta e="T77" id="Seg_2199" s="T75">‎‎"He went away."</ta>
            <ta e="T82" id="Seg_2200" s="T77">This man sat down on the cellar trap-door.</ta>
            <ta e="T86" id="Seg_2201" s="T82">That one stabbed him with an awl in the ass.</ta>
            <ta e="T89" id="Seg_2202" s="T86">This one pulled [the door] open.</ta>
            <ta e="T92" id="Seg_2203" s="T89">At their sight, he sits laughing.</ta>
            <ta e="T97" id="Seg_2204" s="T92">They fettered him and brought him to the river side.</ta>
            <ta e="T99" id="Seg_2205" s="T97">They are building a raft.</ta>
            <ta e="T102" id="Seg_2206" s="T99">A one-eyed man came.</ta>
            <ta e="T105" id="Seg_2207" s="T102">He closed one of his eyes.</ta>
            <ta e="T109" id="Seg_2208" s="T105">"Why do you lie here?"</ta>
            <ta e="T112" id="Seg_2209" s="T109">"I will have my eye cured.</ta>
            <ta e="T117" id="Seg_2210" s="T112">If you let me free, your eye will be cured [instead of mine].</ta>
            <ta e="T118" id="Seg_2211" s="T117">Untie (me)!"</ta>
            <ta e="T119" id="Seg_2212" s="T118">He untied [him].</ta>
            <ta e="T121" id="Seg_2213" s="T119">"Lie down, I’ll fetter you."</ta>
            <ta e="T122" id="Seg_2214" s="T121">He lied down.</ta>
            <ta e="T123" id="Seg_2215" s="T122">He fettered [him].</ta>
            <ta e="T125" id="Seg_2216" s="T123">[The people] came.</ta>
            <ta e="T129" id="Seg_2217" s="T125">"Why do you lie [here], pretending?"</ta>
            <ta e="T132" id="Seg_2218" s="T129">"I’m not pretending.</ta>
            <ta e="T136" id="Seg_2219" s="T132">I am a one-eyed person."</ta>
            <ta e="T143" id="Seg_2220" s="T136">They took this [one-eyed], tied him onto the raft and committed him to the river.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_2221" s="T0">Es lebte eine Frau mit ihrem Mann.</ta>
            <ta e="T5" id="Seg_2222" s="T3">Sie hatten einen Freund.</ta>
            <ta e="T8" id="Seg_2223" s="T5">Er kam zu Besuch.</ta>
            <ta e="T11" id="Seg_2224" s="T8">[Der Mann] schickt seine Frau Wasser holen.</ta>
            <ta e="T14" id="Seg_2225" s="T11">[Doch] seine Frau geht nicht.</ta>
            <ta e="T18" id="Seg_2226" s="T14">Dann nahm er die Peitsche und schlug sie.</ta>
            <ta e="T20" id="Seg_2227" s="T18">Er nahm ein Messer.</ta>
            <ta e="T23" id="Seg_2228" s="T20">Er schlitzte ihr den Bauch auf.</ta>
            <ta e="T26" id="Seg_2229" s="T23">Er hatte [vorher] Blut [in einem Behälter] an ihrem Bauch befestigt.</ta>
            <ta e="T28" id="Seg_2230" s="T26">Das Blut floss.</ta>
            <ta e="T32" id="Seg_2231" s="T28">Sie nahm ihren Kessel und lief Wasser holen.</ta>
            <ta e="T36" id="Seg_2232" s="T32">Sie brachte das Wasser und setzte den Kessel auf.</ta>
            <ta e="T40" id="Seg_2233" s="T36">Der [andere] Mann kehrte zurück nach Hause.</ta>
            <ta e="T43" id="Seg_2234" s="T40">Nachdem er nach Hause zurückgekehrt war, [sagt er] zu seiner Frau: </ta>
            <ta e="T45" id="Seg_2235" s="T43"> "Geh Wasser holen!" </ta>
            <ta e="T48" id="Seg_2236" s="T45">[Doch] seine Frau geht nicht.</ta>
            <ta e="T52" id="Seg_2237" s="T48">Er nahm die Peitsche und schlug sie einmal.</ta>
            <ta e="T54" id="Seg_2238" s="T52">Sie geht nicht.</ta>
            <ta e="T58" id="Seg_2239" s="T54">Er nahm ein Messer und erstach seine Frau.</ta>
            <ta e="T61" id="Seg_2240" s="T58">Seine Frau starb ohne ein Zucken.</ta>
            <ta e="T64" id="Seg_2241" s="T61">Er ging und versammelte Leute.</ta>
            <ta e="T69" id="Seg_2242" s="T64">Die Leute versammelten sich und kamen zu dem [ersten] Mann.</ta>
            <ta e="T72" id="Seg_2243" s="T69">Er hatte sich im Keller versteckt.</ta>
            <ta e="T73" id="Seg_2244" s="T72">Sie fragen [seine Frau]:</ta>
            <ta e="T75" id="Seg_2245" s="T73">"Wo ist dein Mann?"</ta>
            <ta e="T77" id="Seg_2246" s="T75">"Er ging fort."</ta>
            <ta e="T82" id="Seg_2247" s="T77">Der [andere] Mann setzte sich auf die Kellerluke.</ta>
            <ta e="T86" id="Seg_2248" s="T82">[Der erste Mann] stach ihm mit einer Ahle in den Hintern.</ta>
            <ta e="T89" id="Seg_2249" s="T86">[Der andere Mann] riss [die Kellerluke] auf;</ta>
            <ta e="T92" id="Seg_2250" s="T89">Als sie hinein sehen, sitzt [ der erste Mann] [dort unten und] lacht.</ta>
            <ta e="T97" id="Seg_2251" s="T92">Sie fesselten ihn und brachten ihn ans Flussufer.</ta>
            <ta e="T99" id="Seg_2252" s="T97">Sie bauten ein Floß.</ta>
            <ta e="T102" id="Seg_2253" s="T99">Ein Einäugiger kam daher.</ta>
            <ta e="T105" id="Seg_2254" s="T102">[Der Mann] schloss eines seiner Augen.</ta>
            <ta e="T109" id="Seg_2255" s="T105">[Der Einäugige:] "Warum liegst du hier?"</ta>
            <ta e="T112" id="Seg_2256" s="T109">"Ich werde mein Auge heilen lassen.</ta>
            <ta e="T117" id="Seg_2257" s="T112">Wenn du mich losmachst, werden sie [stattdessen] dein Auge heilen.</ta>
            <ta e="T118" id="Seg_2258" s="T117">Mach mich los!"</ta>
            <ta e="T119" id="Seg_2259" s="T118">Er machte [ihn] los.</ta>
            <ta e="T121" id="Seg_2260" s="T119">"Leg dich hin, ich werde dich fesseln."</ta>
            <ta e="T122" id="Seg_2261" s="T121">Er legte sich hin.</ta>
            <ta e="T123" id="Seg_2262" s="T122">[Der Mann] fesselte ihn.</ta>
            <ta e="T125" id="Seg_2263" s="T123">[Die Leute] kamen.</ta>
            <ta e="T129" id="Seg_2264" s="T125">"Warum liegst du [hier] und tust so [als wärst du ein Einäugiger]?"</ta>
            <ta e="T132" id="Seg_2265" s="T129">"Ich tue nicht so.</ta>
            <ta e="T136" id="Seg_2266" s="T132">Ich bin ein einäugiger Mensch."</ta>
            <ta e="T143" id="Seg_2267" s="T136">Sie nahmen diesen, banden ihn auf das Floß und überließen ihn dem Fluss.</ta>
         </annotation>
         <annotation name="ltg" tierref="ltg">
            <ta e="T3" id="Seg_2268" s="T0">Ein altes Weib lebte mit einem Alten.</ta>
            <ta e="T5" id="Seg_2269" s="T3">Sie hatten einen Freund.</ta>
            <ta e="T8" id="Seg_2270" s="T5">Er kam zu Besuch,</ta>
            <ta e="T11" id="Seg_2271" s="T8">Seine Alte Wasser holen er schickt,</ta>
            <ta e="T14" id="Seg_2272" s="T11">seine Alte geht nicht.</ta>
            <ta e="T18" id="Seg_2273" s="T14">Dann die Peitsche nahm er, schlug.</ta>
            <ta e="T20" id="Seg_2274" s="T18">Das Messer nahm er,</ta>
            <ta e="T23" id="Seg_2275" s="T20">schlitzte den Bauch auf,</ta>
            <ta e="T26" id="Seg_2276" s="T23">auf ihren Bauch Blut band,</ta>
            <ta e="T28" id="Seg_2277" s="T26">das Blut tropfte.</ta>
            <ta e="T32" id="Seg_2278" s="T28">Ihren Kessel nahm sie, Wasser holen lief sie.</ta>
            <ta e="T36" id="Seg_2279" s="T32">Wasser brachte sie, ihr Kessel kochte.</ta>
            <ta e="T40" id="Seg_2280" s="T36">Jener (fremde) Mann kehrte nach Hause zurück.</ta>
            <ta e="T43" id="Seg_2281" s="T40">Nachdem er nach Hause gegangen war, zu seiner Alten: </ta>
            <ta e="T45" id="Seg_2282" s="T43">»Wasser holen gehe!»</ta>
            <ta e="T48" id="Seg_2283" s="T45">Seine Alte geht nicht.</ta>
            <ta e="T52" id="Seg_2284" s="T48">Er die Peitsche nahm, schlug,</ta>
            <ta e="T54" id="Seg_2285" s="T52">geht nicht.</ta>
            <ta e="T58" id="Seg_2286" s="T54">Das Messer nahm er, die Alte schnitt er.</ta>
            <ta e="T61" id="Seg_2287" s="T58">Seine Alte bewegte sich, starb.</ta>
            <ta e="T64" id="Seg_2288" s="T61">Ging Leute sammeln;</ta>
            <ta e="T69" id="Seg_2289" s="T64">Leute versammelten sich. Kam zu jenem Manne.</ta>
            <ta e="T72" id="Seg_2290" s="T69">Dieser versteckte sich unter dem Fussboden.</ta>
            <ta e="T73" id="Seg_2291" s="T72">Sie fragen:</ta>
            <ta e="T75" id="Seg_2292" s="T73">»Wo ist dein Mann?»</ta>
            <ta e="T77" id="Seg_2293" s="T75">»Ging fort.»</ta>
            <ta e="T82" id="Seg_2294" s="T77">Dieser Mensch sass auf dem Deckel des unterirdischen Raumes.</ta>
            <ta e="T86" id="Seg_2295" s="T82">Im Spass stach er ihn in den Hintern.</ta>
            <ta e="T89" id="Seg_2296" s="T86">Er riss [den Deckel] auf;</ta>
            <ta e="T92" id="Seg_2297" s="T89">wie sie hinein sehen, sitzt er lachend.</ta>
            <ta e="T97" id="Seg_2298" s="T92">Sie banden ihn, an das Ufer des Flusses brachten sie [ihn],</ta>
            <ta e="T99" id="Seg_2299" s="T97">ein Floss machten sie.</ta>
            <ta e="T102" id="Seg_2300" s="T99">Ein einäugiger Mensch kam.</ta>
            <ta e="T105" id="Seg_2301" s="T102">Das eine Auge schloss er aber.</ta>
            <ta e="T109" id="Seg_2302" s="T105">»Du, wie dich befindend du liegst?»</ta>
            <ta e="T112" id="Seg_2303" s="T109">»Ich werde mein Auge heilen.</ta>
            <ta e="T117" id="Seg_2304" s="T112">Lass mich los! Deine Augen werden sie heilen.</ta>
            <ta e="T118" id="Seg_2305" s="T117">Mache los!»</ta>
            <ta e="T119" id="Seg_2306" s="T118">Er machte [ihn] los.</ta>
            <ta e="T121" id="Seg_2307" s="T119">»Liege, ich binde!»</ta>
            <ta e="T122" id="Seg_2308" s="T121">Lag,</ta>
            <ta e="T123" id="Seg_2309" s="T122">band.</ta>
            <ta e="T125" id="Seg_2310" s="T123">Sie kamen.</ta>
            <ta e="T129" id="Seg_2311" s="T125">»Wie dich befindend lügend du liegst?»</ta>
            <ta e="T132" id="Seg_2312" s="T129">»Ich lüge nicht,</ta>
            <ta e="T136" id="Seg_2313" s="T132">ich bin ein einäugiger Mensch.»</ta>
            <ta e="T143" id="Seg_2314" s="T136">Nahmen, diesen dort auf das Floss banden, in das Wasser liessen.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltg"
                          display-name="ltg"
                          name="ltg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
