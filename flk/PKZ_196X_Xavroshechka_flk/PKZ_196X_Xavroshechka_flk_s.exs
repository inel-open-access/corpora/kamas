<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID011FC709-43B5-498F-9F15-023E13E7264E">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_Xavroshechka_flk.wav" />
         <referenced-file url="PKZ_196X_Xavroshechka_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_Xavroshechka_flk\PKZ_196X_Xavroshechka_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">493</ud-information>
            <ud-information attribute-name="# HIAT:w">332</ud-information>
            <ud-information attribute-name="# e">331</ud-information>
            <ud-information attribute-name="# HIAT:u">68</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T2918" time="0.002" type="appl" />
         <tli id="T2919" time="0.679" type="appl" />
         <tli id="T2920" time="1.356" type="appl" />
         <tli id="T2921" time="2.033" type="appl" />
         <tli id="T2922" time="3.103" type="appl" />
         <tli id="T2923" time="4.172" type="appl" />
         <tli id="T2924" time="5.242" type="appl" />
         <tli id="T2925" time="6.312" type="appl" />
         <tli id="T2926" time="6.999" type="appl" />
         <tli id="T2927" time="7.687" type="appl" />
         <tli id="T2928" time="8.374" type="appl" />
         <tli id="T2929" time="9.061" type="appl" />
         <tli id="T2930" time="9.745" type="appl" />
         <tli id="T2931" time="10.424" type="appl" />
         <tli id="T2932" time="11.102" type="appl" />
         <tli id="T2933" time="11.78" type="appl" />
         <tli id="T2934" time="12.826" type="appl" />
         <tli id="T2935" time="13.872" type="appl" />
         <tli id="T2936" time="14.918" type="appl" />
         <tli id="T2937" time="15.453" type="appl" />
         <tli id="T2938" time="15.988" type="appl" />
         <tli id="T2939" time="16.523" type="appl" />
         <tli id="T2940" time="17.058" type="appl" />
         <tli id="T2941" time="17.647" type="appl" />
         <tli id="T2942" time="18.235" type="appl" />
         <tli id="T2943" time="18.824" type="appl" />
         <tli id="T2944" time="19.444" type="appl" />
         <tli id="T2945" time="20.064" type="appl" />
         <tli id="T2946" time="20.684" type="appl" />
         <tli id="T2947" time="21.304" type="appl" />
         <tli id="T2948" time="21.924" type="appl" />
         <tli id="T2949" time="22.624" type="appl" />
         <tli id="T2950" time="23.325" type="appl" />
         <tli id="T2951" time="24.025" type="appl" />
         <tli id="T2952" time="24.725" type="appl" />
         <tli id="T2953" time="25.425" type="appl" />
         <tli id="T2954" time="26.126" type="appl" />
         <tli id="T2955" time="26.826" type="appl" />
         <tli id="T2956" time="27.709" type="appl" />
         <tli id="T2957" time="28.592" type="appl" />
         <tli id="T2958" time="29.475" type="appl" />
         <tli id="T2959" time="31.255" type="appl" />
         <tli id="T2960" time="33.035" type="appl" />
         <tli id="T2961" time="34.418" type="appl" />
         <tli id="T2962" time="35.802" type="appl" />
         <tli id="T2963" time="37.185" type="appl" />
         <tli id="T2964" time="38.569" type="appl" />
         <tli id="T2965" time="39.952" type="appl" />
         <tli id="T2966" time="40.628" type="appl" />
         <tli id="T2967" time="41.289" type="appl" />
         <tli id="T2968" time="41.95" type="appl" />
         <tli id="T2969" time="42.61" type="appl" />
         <tli id="T2970" time="43.271" type="appl" />
         <tli id="T2971" time="43.932" type="appl" />
         <tli id="T2972" time="44.702" type="appl" />
         <tli id="T2973" time="45.472" type="appl" />
         <tli id="T2974" time="46.243" type="appl" />
         <tli id="T2975" time="47.013" type="appl" />
         <tli id="T2976" time="47.624" type="appl" />
         <tli id="T2977" time="48.236" type="appl" />
         <tli id="T2978" time="48.848" type="appl" />
         <tli id="T2979" time="49.459" type="appl" />
         <tli id="T2980" time="50.07" type="appl" />
         <tli id="T2981" time="50.682" type="appl" />
         <tli id="T2982" time="51.345" type="appl" />
         <tli id="T2983" time="52.008" type="appl" />
         <tli id="T2984" time="52.671" type="appl" />
         <tli id="T2985" time="53.334" type="appl" />
         <tli id="T2986" time="53.996" type="appl" />
         <tli id="T2987" time="54.659" type="appl" />
         <tli id="T2988" time="55.322" type="appl" />
         <tli id="T2989" time="55.985" type="appl" />
         <tli id="T2990" time="56.648" type="appl" />
         <tli id="T2991" time="57.311" type="appl" />
         <tli id="T2992" time="57.973" type="appl" />
         <tli id="T2993" time="58.687" type="appl" />
         <tli id="T2994" time="59.293" type="appl" />
         <tli id="T2995" time="59.898" type="appl" />
         <tli id="T2996" time="60.504" type="appl" />
         <tli id="T2997" time="61.785142218202985" />
         <tli id="T2998" time="62.226" type="appl" />
         <tli id="T2999" time="63.342" type="appl" />
         <tli id="T3000" time="64.459" type="appl" />
         <tli id="T3001" time="65.61171447038775" />
         <tli id="T3002" time="66.512" type="appl" />
         <tli id="T3003" time="67.369" type="appl" />
         <tli id="T3004" time="68.49164341279861" />
         <tli id="T3005" time="68.974" type="appl" />
         <tli id="T3006" time="69.722" type="appl" />
         <tli id="T3007" time="70.471" type="appl" />
         <tli id="T3008" time="71.22" type="appl" />
         <tli id="T3009" time="71.786" type="appl" />
         <tli id="T3010" time="72.352" type="appl" />
         <tli id="T3011" time="72.917" type="appl" />
         <tli id="T3012" time="73.483" type="appl" />
         <tli id="T3013" time="74.049" type="appl" />
         <tli id="T3014" time="74.615" type="appl" />
         <tli id="T3015" time="75.319" type="appl" />
         <tli id="T3016" time="76.023" type="appl" />
         <tli id="T3017" time="76.726" type="appl" />
         <tli id="T3018" time="77.43" type="appl" />
         <tli id="T3019" time="78.321" type="appl" />
         <tli id="T3020" time="79.208" type="appl" />
         <tli id="T3021" time="79.857" type="appl" />
         <tli id="T3022" time="80.506" type="appl" />
         <tli id="T3023" time="81.154" type="appl" />
         <tli id="T3024" time="82.34463494596928" />
         <tli id="T3025" time="83.293" type="appl" />
         <tli id="T3026" time="84.409" type="appl" />
         <tli id="T3027" time="85.525" type="appl" />
         <tli id="T3028" time="86.156" type="appl" />
         <tli id="T3029" time="86.787" type="appl" />
         <tli id="T3030" time="87.418" type="appl" />
         <tli id="T3031" time="88.05" type="appl" />
         <tli id="T3032" time="88.681" type="appl" />
         <tli id="T3033" time="89.312" type="appl" />
         <tli id="T3034" time="89.943" type="appl" />
         <tli id="T3035" time="90.83775872520866" />
         <tli id="T3036" time="91.284" type="appl" />
         <tli id="T3037" time="91.994" type="appl" />
         <tli id="T3038" time="92.704" type="appl" />
         <tli id="T3039" time="93.415" type="appl" />
         <tli id="T3040" time="94.125" type="appl" />
         <tli id="T3041" time="95.25764967154754" />
         <tli id="T3042" time="95.84" type="appl" />
         <tli id="T3043" time="96.846" type="appl" />
         <tli id="T3044" time="97.99091556596524" />
         <tli id="T3045" time="98.679" type="appl" />
         <tli id="T3046" time="99.507" type="appl" />
         <tli id="T3047" time="100.335" type="appl" />
         <tli id="T3048" time="101.151" type="appl" />
         <tli id="T3049" time="101.967" type="appl" />
         <tli id="T3050" time="102.782" type="appl" />
         <tli id="T3051" time="103.598" type="appl" />
         <tli id="T3052" time="104.414" type="appl" />
         <tli id="T3053" time="105.23" type="appl" />
         <tli id="T3054" time="105.68" type="appl" />
         <tli id="T3055" time="106.13" type="appl" />
         <tli id="T3056" time="106.58" type="appl" />
         <tli id="T3057" time="107.03" type="appl" />
         <tli id="T3058" time="107.48" type="appl" />
         <tli id="T3059" time="108.157" type="appl" />
         <tli id="T3060" time="108.834" type="appl" />
         <tli id="T3061" time="109.511" type="appl" />
         <tli id="T3062" time="110.355" type="appl" />
         <tli id="T3063" time="111.199" type="appl" />
         <tli id="T3064" time="111.798" type="appl" />
         <tli id="T3065" time="112.396" type="appl" />
         <tli id="T3066" time="112.995" type="appl" />
         <tli id="T3067" time="114.03051981467006" />
         <tli id="T3068" time="114.444" type="appl" />
         <tli id="T3069" time="115.28" type="appl" />
         <tli id="T3070" time="116.116" type="appl" />
         <tli id="T3071" time="116.952" type="appl" />
         <tli id="T3072" time="117.787" type="appl" />
         <tli id="T3073" time="118.623" type="appl" />
         <tli id="T3074" time="119.459" type="appl" />
         <tli id="T3075" time="120.8636845507143" />
         <tli id="T3076" time="121.499" type="appl" />
         <tli id="T3077" time="122.57" type="appl" />
         <tli id="T3078" time="123.642" type="appl" />
         <tli id="T3079" time="124.301" type="appl" />
         <tli id="T3080" time="124.96" type="appl" />
         <tli id="T3081" time="125.619" type="appl" />
         <tli id="T3082" time="126.278" type="appl" />
         <tli id="T3083" time="126.938" type="appl" />
         <tli id="T3084" time="127.597" type="appl" />
         <tli id="T3085" time="128.256" type="appl" />
         <tli id="T3086" time="129.62346841721396" />
         <tli id="T3087" time="130.536" type="appl" />
         <tli id="T3088" time="131.555" type="appl" />
         <tli id="T3089" time="132.573" type="appl" />
         <tli id="T3090" time="133.592" type="appl" />
         <tli id="T3091" time="134.482" type="appl" />
         <tli id="T3092" time="135.371" type="appl" />
         <tli id="T3093" time="136.261" type="appl" />
         <tli id="T3094" time="137.151" type="appl" />
         <tli id="T3095" time="138.04" type="appl" />
         <tli id="T3096" time="138.93" type="appl" />
         <tli id="T3097" time="139.577" type="appl" />
         <tli id="T3098" time="140.203" type="appl" />
         <tli id="T3099" time="141.07651916504227" />
         <tli id="T3100" time="141.907" type="appl" />
         <tli id="T3101" time="142.986" type="appl" />
         <tli id="T3102" time="144.53643379724414" />
         <tli id="T3103" time="145.435" type="appl" />
         <tli id="T3104" time="146.479" type="appl" />
         <tli id="T3105" time="147.524" type="appl" />
         <tli id="T3106" time="148.568" type="appl" />
         <tli id="T3107" time="149.612" type="appl" />
         <tli id="T3108" time="151.08293893948363" />
         <tli id="T3109" time="151.635" type="appl" />
         <tli id="T3110" time="152.56" type="appl" />
         <tli id="T3111" time="153.486" type="appl" />
         <tli id="T3112" time="154.411" type="appl" />
         <tli id="T3113" time="155.336" type="appl" />
         <tli id="T3114" time="156.262" type="appl" />
         <tli id="T3115" time="157.187" type="appl" />
         <tli id="T3116" time="158.3227603085998" />
         <tli id="T3117" time="159.154" type="appl" />
         <tli id="T3118" time="159.76" type="appl" />
         <tli id="T3119" time="160.367" type="appl" />
         <tli id="T3120" time="160.974" type="appl" />
         <tli id="T3121" time="161.58" type="appl" />
         <tli id="T3122" time="162.187" type="appl" />
         <tli id="T3123" time="162.794" type="appl" />
         <tli id="T3124" time="163.4" type="appl" />
         <tli id="T3125" time="164.27594675668516" />
         <tli id="T3126" time="164.885" type="appl" />
         <tli id="T3127" time="165.64" type="appl" />
         <tli id="T3128" time="166.394" type="appl" />
         <tli id="T3129" time="167.60253134525692" />
         <tli id="T3130" time="168.214" type="appl" />
         <tli id="T3131" time="168.784" type="appl" />
         <tli id="T3132" time="169.354" type="appl" />
         <tli id="T3133" time="170.002" type="appl" />
         <tli id="T3134" time="170.65" type="appl" />
         <tli id="T3135" time="173.16906066681494" />
         <tli id="T3136" time="174.321" type="appl" />
         <tli id="T3137" time="175.092" type="appl" />
         <tli id="T3138" time="175.862" type="appl" />
         <tli id="T3139" time="176.632" type="appl" />
         <tli id="T3140" time="177.403" type="appl" />
         <tli id="T3141" time="178.173" type="appl" />
         <tli id="T3142" time="179.076" type="appl" />
         <tli id="T3143" time="179.98" type="appl" />
         <tli id="T3144" time="180.349" type="appl" />
         <tli id="T3145" time="180.719" type="appl" />
         <tli id="T3146" time="181.24219480861942" />
         <tli id="T3147" time="182.113" type="appl" />
         <tli id="T3148" time="182.844" type="appl" />
         <tli id="T3149" time="183.576" type="appl" />
         <tli id="T3150" time="184.307" type="appl" />
         <tli id="T3151" time="185.039" type="appl" />
         <tli id="T3152" time="185.508" type="appl" />
         <tli id="T3153" time="185.964" type="appl" />
         <tli id="T3154" time="186.419" type="appl" />
         <tli id="T3155" time="187.075" type="appl" />
         <tli id="T3156" time="187.709" type="appl" />
         <tli id="T3157" time="188.342" type="appl" />
         <tli id="T3158" time="188.975" type="appl" />
         <tli id="T3159" time="189.608" type="appl" />
         <tli id="T3160" time="190.242" type="appl" />
         <tli id="T3161" time="190.875" type="appl" />
         <tli id="T3162" time="191.341" type="appl" />
         <tli id="T3163" time="191.767" type="appl" />
         <tli id="T3164" time="192.192" type="appl" />
         <tli id="T3165" time="192.617" type="appl" />
         <tli id="T3166" time="193.042" type="appl" />
         <tli id="T3167" time="193.468" type="appl" />
         <tli id="T3168" time="193.893" type="appl" />
         <tli id="T3169" time="194.474" type="appl" />
         <tli id="T3170" time="195.055" type="appl" />
         <tli id="T3171" time="195.637" type="appl" />
         <tli id="T3172" time="196.218" type="appl" />
         <tli id="T3173" time="196.799" type="appl" />
         <tli id="T3174" time="197.599" type="appl" />
         <tli id="T3175" time="198.35" type="appl" />
         <tli id="T3176" time="198.838" type="appl" />
         <tli id="T3177" time="199.326" type="appl" />
         <tli id="T3178" time="199.815" type="appl" />
         <tli id="T3179" time="200.303" type="appl" />
         <tli id="T3180" time="200.91504274624535" />
         <tli id="T3181" time="202.226" type="appl" />
         <tli id="T3182" time="203.399" type="appl" />
         <tli id="T3183" time="204.443" type="appl" />
         <tli id="T3184" time="205.487" type="appl" />
         <tli id="T3185" time="206.531" type="appl" />
         <tli id="T3186" time="207.575" type="appl" />
         <tli id="T3187" time="208.508" type="appl" />
         <tli id="T3188" time="209.441" type="appl" />
         <tli id="T3189" time="210.305" type="appl" />
         <tli id="T3190" time="211.169" type="appl" />
         <tli id="T3191" time="212.032" type="appl" />
         <tli id="T3192" time="212.896" type="appl" />
         <tli id="T3193" time="213.81472446746056" />
         <tli id="T3194" time="214.591" type="appl" />
         <tli id="T3195" time="215.422" type="appl" />
         <tli id="T3196" time="216.46132583351866" />
         <tli id="T3197" time="217.095" type="appl" />
         <tli id="T3198" time="217.937" type="appl" />
         <tli id="T3199" time="218.779" type="appl" />
         <tli id="T3200" time="219.621" type="appl" />
         <tli id="T3201" time="220.463" type="appl" />
         <tli id="T3202" time="221.305" type="appl" />
         <tli id="T3203" time="222.147" type="appl" />
         <tli id="T3204" time="222.948" type="appl" />
         <tli id="T3205" time="223.743" type="appl" />
         <tli id="T3206" time="224.538" type="appl" />
         <tli id="T3207" time="225.283" type="appl" />
         <tli id="T3208" time="226.027" type="appl" />
         <tli id="T3209" time="226.772" type="appl" />
         <tli id="T3210" time="227.516" type="appl" />
         <tli id="T3211" time="228.261" type="appl" />
         <tli id="T3212" time="228.947" type="appl" />
         <tli id="T3213" time="229.576" type="appl" />
         <tli id="T3214" time="230.206" type="appl" />
         <tli id="T3215" time="230.836" type="appl" />
         <tli id="T3216" time="231.465" type="appl" />
         <tli id="T3217" time="232.095" type="appl" />
         <tli id="T3218" time="232.724" type="appl" />
         <tli id="T3219" time="233.354" type="appl" />
         <tli id="T3220" time="234.073" type="appl" />
         <tli id="T3221" time="234.793" type="appl" />
         <tli id="T3222" time="235.512" type="appl" />
         <tli id="T3223" time="237.72080129034313" />
         <tli id="T3224" time="239.143" type="appl" />
         <tli id="T3225" time="240.829" type="appl" />
         <tli id="T3226" time="241.522" type="appl" />
         <tli id="T3227" time="242.075" type="appl" />
         <tli id="T3228" time="242.627" type="appl" />
         <tli id="T3229" time="243.179" type="appl" />
         <tli id="T3230" time="243.732" type="appl" />
         <tli id="T3231" time="244.284" type="appl" />
         <tli id="T3232" time="244.836" type="appl" />
         <tli id="T3233" time="245.389" type="appl" />
         <tli id="T3234" time="245.941" type="appl" />
         <tli id="T3235" time="246.493" type="appl" />
         <tli id="T3236" time="247.046" type="appl" />
         <tli id="T3237" time="247.598" type="appl" />
         <tli id="T3238" time="248.15" type="appl" />
         <tli id="T3239" time="248.703" type="appl" />
         <tli id="T3240" time="249.255" type="appl" />
         <tli id="T3241" time="249.832" type="appl" />
         <tli id="T3242" time="250.409" type="appl" />
         <tli id="T3243" time="250.986" type="appl" />
         <tli id="T3244" time="251.562" type="appl" />
         <tli id="T3245" time="252.139" type="appl" />
         <tli id="T3246" time="252.716" type="appl" />
         <tli id="T3247" time="253.293" type="appl" />
         <tli id="T3248" time="253.87" type="appl" />
         <tli id="T3249" time="254.787" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T3147" start="T3146">
            <tli id="T3146.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T3249" id="Seg_0" n="sc" s="T2918">
               <ts e="T2921" id="Seg_2" n="HIAT:u" s="T2918">
                  <ts e="T2919" id="Seg_4" n="HIAT:w" s="T2918">Amnobi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2920" id="Seg_7" n="HIAT:w" s="T2919">onʼiʔ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2921" id="Seg_10" n="HIAT:w" s="T2920">koʔbdo</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2925" id="Seg_14" n="HIAT:u" s="T2921">
                  <nts id="Seg_15" n="HIAT:ip">(</nts>
                  <ts e="T2922" id="Seg_17" n="HIAT:w" s="T2921">Di-</ts>
                  <nts id="Seg_18" n="HIAT:ip">)</nts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2923" id="Seg_21" n="HIAT:w" s="T2922">Dĭm</ts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2924" id="Seg_24" n="HIAT:w" s="T2923">numəjleʔbəʔi</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2925" id="Seg_27" n="HIAT:w" s="T2924">Xavrošečka</ts>
                  <nts id="Seg_28" n="HIAT:ip">.</nts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2929" id="Seg_31" n="HIAT:u" s="T2925">
                  <ts e="T2926" id="Seg_33" n="HIAT:w" s="T2925">Dĭn</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2927" id="Seg_36" n="HIAT:w" s="T2926">ijat</ts>
                  <nts id="Seg_37" n="HIAT:ip">,</nts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2928" id="Seg_40" n="HIAT:w" s="T2927">abat</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2929" id="Seg_43" n="HIAT:w" s="T2928">külaːmbiʔi</ts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2933" id="Seg_47" n="HIAT:u" s="T2929">
                  <ts e="T2930" id="Seg_49" n="HIAT:w" s="T2929">Il</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2931" id="Seg_52" n="HIAT:w" s="T2930">ej</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2932" id="Seg_55" n="HIAT:w" s="T2931">jakšəʔi</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2933" id="Seg_58" n="HIAT:w" s="T2932">bar</ts>
                  <nts id="Seg_59" n="HIAT:ip">.</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2936" id="Seg_62" n="HIAT:u" s="T2933">
                  <nts id="Seg_63" n="HIAT:ip">(</nts>
                  <ts e="T2934" id="Seg_65" n="HIAT:w" s="T2933">Dĭʔnə=</ts>
                  <nts id="Seg_66" n="HIAT:ip">)</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2935" id="Seg_69" n="HIAT:w" s="T2934">Dĭm</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2936" id="Seg_72" n="HIAT:w" s="T2935">ibiʔi</ts>
                  <nts id="Seg_73" n="HIAT:ip">.</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2940" id="Seg_76" n="HIAT:u" s="T2936">
                  <ts e="T2937" id="Seg_78" n="HIAT:w" s="T2936">Dĭn</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2938" id="Seg_81" n="HIAT:w" s="T2937">nagur</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2939" id="Seg_84" n="HIAT:w" s="T2938">koʔbdot</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2940" id="Seg_87" n="HIAT:w" s="T2939">ibiʔi</ts>
                  <nts id="Seg_88" n="HIAT:ip">.</nts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2943" id="Seg_91" n="HIAT:u" s="T2940">
                  <ts e="T2941" id="Seg_93" n="HIAT:w" s="T2940">Ĭmbidə</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2942" id="Seg_96" n="HIAT:w" s="T2941">ej</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2943" id="Seg_99" n="HIAT:w" s="T2942">abiʔi</ts>
                  <nts id="Seg_100" n="HIAT:ip">.</nts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2948" id="Seg_103" n="HIAT:u" s="T2943">
                  <ts e="T2944" id="Seg_105" n="HIAT:w" s="T2943">A</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2945" id="Seg_108" n="HIAT:w" s="T2944">dĭ</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2946" id="Seg_111" n="HIAT:w" s="T2945">Xavrošečka</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2947" id="Seg_114" n="HIAT:w" s="T2946">bar</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2948" id="Seg_117" n="HIAT:w" s="T2947">togonorbi</ts>
                  <nts id="Seg_118" n="HIAT:ip">.</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2955" id="Seg_121" n="HIAT:u" s="T2948">
                  <ts e="T2949" id="Seg_123" n="HIAT:w" s="T2948">Bar</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2950" id="Seg_126" n="HIAT:w" s="T2949">ĭmbi</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2951" id="Seg_129" n="HIAT:w" s="T2950">togonoria</ts>
                  <nts id="Seg_130" n="HIAT:ip">,</nts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_132" n="HIAT:ip">(</nts>
                  <ts e="T2952" id="Seg_134" n="HIAT:w" s="T2951">ej=</ts>
                  <nts id="Seg_135" n="HIAT:ip">)</nts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2953" id="Seg_138" n="HIAT:w" s="T2952">dĭzeŋdə</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2954" id="Seg_141" n="HIAT:w" s="T2953">ej</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2955" id="Seg_144" n="HIAT:w" s="T2954">molia</ts>
                  <nts id="Seg_145" n="HIAT:ip">.</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2958" id="Seg_148" n="HIAT:u" s="T2955">
                  <ts e="T2956" id="Seg_150" n="HIAT:w" s="T2955">Uge</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2957" id="Seg_153" n="HIAT:w" s="T2956">kudollaʔbə</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2958" id="Seg_156" n="HIAT:w" s="T2957">nüke</ts>
                  <nts id="Seg_157" n="HIAT:ip">.</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2965" id="Seg_160" n="HIAT:u" s="T2958">
                  <ts e="T2959" id="Seg_162" n="HIAT:w" s="T2958">Dĭgəttə</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2960" id="Seg_165" n="HIAT:w" s="T2959">măndə:</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_167" n="HIAT:ip">"</nts>
                  <ts e="T2961" id="Seg_169" n="HIAT:w" s="T2960">Kanaʔ</ts>
                  <nts id="Seg_170" n="HIAT:ip">,</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2962" id="Seg_173" n="HIAT:w" s="T2961">măna</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2963" id="Seg_176" n="HIAT:w" s="T2962">aʔ</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2964" id="Seg_179" n="HIAT:w" s="T2963">iʔgö</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2965" id="Seg_182" n="HIAT:w" s="T2964">xolstə</ts>
                  <nts id="Seg_183" n="HIAT:ip">"</nts>
                  <nts id="Seg_184" n="HIAT:ip">.</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2975" id="Seg_187" n="HIAT:u" s="T2965">
                  <ts e="T2966" id="Seg_189" n="HIAT:w" s="T2965">Dĭ</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2967" id="Seg_192" n="HIAT:w" s="T2966">kambi</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2968" id="Seg_195" n="HIAT:w" s="T2967">tüžöjdə</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2969" id="Seg_198" n="HIAT:w" s="T2968">da</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2970" id="Seg_201" n="HIAT:w" s="T2969">bar</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2971" id="Seg_204" n="HIAT:w" s="T2970">dʼorlaʔbə:</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_206" n="HIAT:ip">"</nts>
                  <nts id="Seg_207" n="HIAT:ip">(</nts>
                  <ts e="T2972" id="Seg_209" n="HIAT:w" s="T2971">M-</ts>
                  <nts id="Seg_210" n="HIAT:ip">)</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2973" id="Seg_213" n="HIAT:w" s="T2972">Măna</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2974" id="Seg_216" n="HIAT:w" s="T2973">teŋ</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2975" id="Seg_219" n="HIAT:w" s="T2974">münörleʔbəʔjə</ts>
                  <nts id="Seg_220" n="HIAT:ip">.</nts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2981" id="Seg_223" n="HIAT:u" s="T2975">
                  <ts e="T2976" id="Seg_225" n="HIAT:w" s="T2975">Dĭzeŋdə</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2977" id="Seg_228" n="HIAT:w" s="T2976">ĭmbidə</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2978" id="Seg_231" n="HIAT:w" s="T2977">asʼtə</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2979" id="Seg_234" n="HIAT:w" s="T2978">ej</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_236" n="HIAT:ip">(</nts>
                  <ts e="T2980" id="Seg_238" n="HIAT:w" s="T2979">m-</ts>
                  <nts id="Seg_239" n="HIAT:ip">)</nts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2981" id="Seg_242" n="HIAT:w" s="T2980">moliam</ts>
                  <nts id="Seg_243" n="HIAT:ip">"</nts>
                  <nts id="Seg_244" n="HIAT:ip">.</nts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2990" id="Seg_247" n="HIAT:u" s="T2981">
                  <ts e="T2982" id="Seg_249" n="HIAT:w" s="T2981">I</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2983" id="Seg_252" n="HIAT:w" s="T2982">tüžöj</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2984" id="Seg_255" n="HIAT:w" s="T2983">măndə:</ts>
                  <nts id="Seg_256" n="HIAT:ip">"</nts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2985" id="Seg_259" n="HIAT:w" s="T2984">Onʼiʔ</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2986" id="Seg_262" n="HIAT:w" s="T2985">kuʔdə</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2987" id="Seg_265" n="HIAT:w" s="T2986">pădaʔ</ts>
                  <nts id="Seg_266" n="HIAT:ip">,</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2988" id="Seg_269" n="HIAT:w" s="T2987">a</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2989" id="Seg_272" n="HIAT:w" s="T2988">onʼiʔtə</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2990" id="Seg_275" n="HIAT:w" s="T2989">supsoʔ</ts>
                  <nts id="Seg_276" n="HIAT:ip">!</nts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2992" id="Seg_279" n="HIAT:u" s="T2990">
                  <ts e="T2991" id="Seg_281" n="HIAT:w" s="T2990">Bar</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2992" id="Seg_284" n="HIAT:w" s="T2991">moləj</ts>
                  <nts id="Seg_285" n="HIAT:ip">"</nts>
                  <nts id="Seg_286" n="HIAT:ip">.</nts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2997" id="Seg_289" n="HIAT:u" s="T2992">
                  <ts e="T2993" id="Seg_291" n="HIAT:w" s="T2992">Dĭgəttə</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_293" n="HIAT:ip">(</nts>
                  <ts e="T2994" id="Seg_295" n="HIAT:w" s="T2993">dĭn-</ts>
                  <nts id="Seg_296" n="HIAT:ip">)</nts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2995" id="Seg_299" n="HIAT:w" s="T2994">dĭʔnə</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_301" n="HIAT:ip">(</nts>
                  <ts e="T2996" id="Seg_303" n="HIAT:w" s="T2995">am-</ts>
                  <nts id="Seg_304" n="HIAT:ip">)</nts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2997" id="Seg_307" n="HIAT:w" s="T2996">abi</ts>
                  <nts id="Seg_308" n="HIAT:ip">.</nts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3001" id="Seg_311" n="HIAT:u" s="T2997">
                  <ts e="T2998" id="Seg_313" n="HIAT:w" s="T2997">I</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2999" id="Seg_316" n="HIAT:w" s="T2998">ererbi</ts>
                  <nts id="Seg_317" n="HIAT:ip">,</nts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3000" id="Seg_320" n="HIAT:w" s="T2999">xolstə</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3001" id="Seg_323" n="HIAT:w" s="T3000">abi</ts>
                  <nts id="Seg_324" n="HIAT:ip">.</nts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3004" id="Seg_327" n="HIAT:u" s="T3001">
                  <nts id="Seg_328" n="HIAT:ip">(</nts>
                  <ts e="T3002" id="Seg_330" n="HIAT:w" s="T3001">Dĭ</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3003" id="Seg_333" n="HIAT:w" s="T3002">dĭgəttə</ts>
                  <nts id="Seg_334" n="HIAT:ip">)</nts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3004" id="Seg_337" n="HIAT:w" s="T3003">deʔpi</ts>
                  <nts id="Seg_338" n="HIAT:ip">.</nts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3008" id="Seg_341" n="HIAT:u" s="T3004">
                  <ts e="T3005" id="Seg_343" n="HIAT:w" s="T3004">Dĭgəttə</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3006" id="Seg_346" n="HIAT:w" s="T3005">bazoʔ</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3007" id="Seg_349" n="HIAT:w" s="T3006">iʔgö</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3008" id="Seg_352" n="HIAT:w" s="T3007">mobi</ts>
                  <nts id="Seg_353" n="HIAT:ip">.</nts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3014" id="Seg_356" n="HIAT:u" s="T3008">
                  <ts e="T3009" id="Seg_358" n="HIAT:w" s="T3008">Dĭ</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3010" id="Seg_361" n="HIAT:w" s="T3009">bazoʔ</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3011" id="Seg_364" n="HIAT:w" s="T3010">kambi</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3012" id="Seg_367" n="HIAT:w" s="T3011">tüžöjdə</ts>
                  <nts id="Seg_368" n="HIAT:ip">,</nts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3013" id="Seg_371" n="HIAT:w" s="T3012">bazoʔ</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3014" id="Seg_374" n="HIAT:w" s="T3013">abi</ts>
                  <nts id="Seg_375" n="HIAT:ip">.</nts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3018" id="Seg_378" n="HIAT:u" s="T3014">
                  <ts e="T3015" id="Seg_380" n="HIAT:w" s="T3014">Dĭgəttə</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3016" id="Seg_383" n="HIAT:w" s="T3015">măndə</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3017" id="Seg_386" n="HIAT:w" s="T3016">onʼiʔ</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3018" id="Seg_389" n="HIAT:w" s="T3017">koʔbdondə</ts>
                  <nts id="Seg_390" n="HIAT:ip">.</nts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3020" id="Seg_393" n="HIAT:u" s="T3018">
                  <ts e="T3019" id="Seg_395" n="HIAT:w" s="T3018">Onʼiʔ</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3020" id="Seg_398" n="HIAT:w" s="T3019">sʼimandə</ts>
                  <nts id="Seg_399" n="HIAT:ip">.</nts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3024" id="Seg_402" n="HIAT:u" s="T3020">
                  <nts id="Seg_403" n="HIAT:ip">"</nts>
                  <ts e="T3021" id="Seg_405" n="HIAT:w" s="T3020">Kanaʔ</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3022" id="Seg_408" n="HIAT:w" s="T3021">dĭziʔ</ts>
                  <nts id="Seg_409" n="HIAT:ip">"</nts>
                  <nts id="Seg_410" n="HIAT:ip">;</nts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3023" id="Seg_413" n="HIAT:w" s="T3022">dĭ</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3024" id="Seg_416" n="HIAT:w" s="T3023">kambi</ts>
                  <nts id="Seg_417" n="HIAT:ip">.</nts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3027" id="Seg_420" n="HIAT:u" s="T3024">
                  <ts e="T3025" id="Seg_422" n="HIAT:w" s="T3024">Dĭn</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3026" id="Seg_425" n="HIAT:w" s="T3025">iʔbəbi</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3027" id="Seg_428" n="HIAT:w" s="T3026">kunolzittə</ts>
                  <nts id="Seg_429" n="HIAT:ip">.</nts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3035" id="Seg_432" n="HIAT:u" s="T3027">
                  <ts e="T3028" id="Seg_434" n="HIAT:w" s="T3027">Dĭ</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3029" id="Seg_437" n="HIAT:w" s="T3028">bar:</ts>
                  <nts id="Seg_438" n="HIAT:ip">"</nts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3030" id="Seg_441" n="HIAT:w" s="T3029">Kunoldə</ts>
                  <nts id="Seg_442" n="HIAT:ip">,</nts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3031" id="Seg_445" n="HIAT:w" s="T3030">sima</ts>
                  <nts id="Seg_446" n="HIAT:ip">,</nts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3032" id="Seg_449" n="HIAT:w" s="T3031">kunoldə</ts>
                  <nts id="Seg_450" n="HIAT:ip">,</nts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3033" id="Seg_453" n="HIAT:w" s="T3032">sima</ts>
                  <nts id="Seg_454" n="HIAT:ip">"</nts>
                  <nts id="Seg_455" n="HIAT:ip">;</nts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3034" id="Seg_458" n="HIAT:w" s="T3033">simat</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3035" id="Seg_461" n="HIAT:w" s="T3034">kunolluʔpi</ts>
                  <nts id="Seg_462" n="HIAT:ip">.</nts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3041" id="Seg_465" n="HIAT:u" s="T3035">
                  <ts e="T3036" id="Seg_467" n="HIAT:w" s="T3035">Dĭʔnə</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3037" id="Seg_470" n="HIAT:w" s="T3036">bazoʔ</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3038" id="Seg_473" n="HIAT:w" s="T3037">tüžöj</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3039" id="Seg_476" n="HIAT:w" s="T3038">ererluʔpi</ts>
                  <nts id="Seg_477" n="HIAT:ip">,</nts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3040" id="Seg_480" n="HIAT:w" s="T3039">dĭ</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3041" id="Seg_483" n="HIAT:w" s="T3040">ibi</ts>
                  <nts id="Seg_484" n="HIAT:ip">.</nts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3044" id="Seg_487" n="HIAT:u" s="T3041">
                  <ts e="T3042" id="Seg_489" n="HIAT:w" s="T3041">Trubkaʔi</ts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3043" id="Seg_492" n="HIAT:w" s="T3042">xolstəʔi</ts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3044" id="Seg_495" n="HIAT:w" s="T3043">deʔpi</ts>
                  <nts id="Seg_496" n="HIAT:ip">.</nts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3047" id="Seg_499" n="HIAT:u" s="T3044">
                  <ts e="T3045" id="Seg_501" n="HIAT:w" s="T3044">Dĭ</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3046" id="Seg_504" n="HIAT:w" s="T3045">nükenə</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3047" id="Seg_507" n="HIAT:w" s="T3046">mĭbi</ts>
                  <nts id="Seg_508" n="HIAT:ip">.</nts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3053" id="Seg_511" n="HIAT:u" s="T3047">
                  <ts e="T3048" id="Seg_513" n="HIAT:w" s="T3047">Dĭgəttə</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3049" id="Seg_516" n="HIAT:w" s="T3048">dĭ</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3050" id="Seg_519" n="HIAT:w" s="T3049">nüke</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3051" id="Seg_522" n="HIAT:w" s="T3050">baška</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3052" id="Seg_525" n="HIAT:w" s="T3051">koʔbdobə</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3053" id="Seg_528" n="HIAT:w" s="T3052">öʔleʔbə</ts>
                  <nts id="Seg_529" n="HIAT:ip">.</nts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3058" id="Seg_532" n="HIAT:u" s="T3053">
                  <nts id="Seg_533" n="HIAT:ip">"</nts>
                  <ts e="T3054" id="Seg_535" n="HIAT:w" s="T3053">Kanaʔ</ts>
                  <nts id="Seg_536" n="HIAT:ip">,</nts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3055" id="Seg_539" n="HIAT:w" s="T3054">ĭmbi</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3056" id="Seg_542" n="HIAT:w" s="T3055">dĭ</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3057" id="Seg_545" n="HIAT:w" s="T3056">dĭn</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3058" id="Seg_548" n="HIAT:w" s="T3057">alia</ts>
                  <nts id="Seg_549" n="HIAT:ip">?</nts>
                  <nts id="Seg_550" n="HIAT:ip">"</nts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3061" id="Seg_553" n="HIAT:u" s="T3058">
                  <ts e="T3059" id="Seg_555" n="HIAT:w" s="T3058">Dĭ</ts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3060" id="Seg_558" n="HIAT:w" s="T3059">koʔbdo</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3061" id="Seg_561" n="HIAT:w" s="T3060">kambi</ts>
                  <nts id="Seg_562" n="HIAT:ip">.</nts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3063" id="Seg_565" n="HIAT:u" s="T3061">
                  <ts e="T3062" id="Seg_567" n="HIAT:w" s="T3061">Dĭn</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3063" id="Seg_570" n="HIAT:w" s="T3062">iʔbəbi</ts>
                  <nts id="Seg_571" n="HIAT:ip">.</nts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3067" id="Seg_574" n="HIAT:u" s="T3063">
                  <ts e="T3064" id="Seg_576" n="HIAT:w" s="T3063">Dĭ</ts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3065" id="Seg_579" n="HIAT:w" s="T3064">bar</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3066" id="Seg_582" n="HIAT:w" s="T3065">dĭm</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3067" id="Seg_585" n="HIAT:w" s="T3066">kunolluʔpi</ts>
                  <nts id="Seg_586" n="HIAT:ip">.</nts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3075" id="Seg_589" n="HIAT:u" s="T3067">
                  <nts id="Seg_590" n="HIAT:ip">"</nts>
                  <nts id="Seg_591" n="HIAT:ip">(</nts>
                  <ts e="T3068" id="Seg_593" n="HIAT:w" s="T3067">Ku-</ts>
                  <nts id="Seg_594" n="HIAT:ip">)</nts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3069" id="Seg_597" n="HIAT:w" s="T3068">Kunolaʔ</ts>
                  <nts id="Seg_598" n="HIAT:ip">,</nts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3070" id="Seg_601" n="HIAT:w" s="T3069">sima</ts>
                  <nts id="Seg_602" n="HIAT:ip">,</nts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3071" id="Seg_605" n="HIAT:w" s="T3070">kunolaʔ</ts>
                  <nts id="Seg_606" n="HIAT:ip">,</nts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3072" id="Seg_609" n="HIAT:w" s="T3071">baška</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3073" id="Seg_612" n="HIAT:w" s="T3072">sima</ts>
                  <nts id="Seg_613" n="HIAT:ip">"</nts>
                  <nts id="Seg_614" n="HIAT:ip">;</nts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3074" id="Seg_617" n="HIAT:w" s="T3073">simazaŋdə</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3075" id="Seg_620" n="HIAT:w" s="T3074">kunolluʔpi</ts>
                  <nts id="Seg_621" n="HIAT:ip">.</nts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3078" id="Seg_624" n="HIAT:u" s="T3075">
                  <ts e="T3076" id="Seg_626" n="HIAT:w" s="T3075">Tüžöj</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3077" id="Seg_629" n="HIAT:w" s="T3076">dĭʔnə</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3078" id="Seg_632" n="HIAT:w" s="T3077">abi</ts>
                  <nts id="Seg_633" n="HIAT:ip">.</nts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3086" id="Seg_636" n="HIAT:u" s="T3078">
                  <ts e="T3079" id="Seg_638" n="HIAT:w" s="T3078">Xolstə</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3080" id="Seg_641" n="HIAT:w" s="T3079">ererbi</ts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3081" id="Seg_644" n="HIAT:w" s="T3080">i</ts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3082" id="Seg_647" n="HIAT:w" s="T3081">trubkaʔi</ts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3083" id="Seg_650" n="HIAT:w" s="T3082">mĭbi</ts>
                  <nts id="Seg_651" n="HIAT:ip">,</nts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3084" id="Seg_654" n="HIAT:w" s="T3083">dĭgəttə</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3085" id="Seg_657" n="HIAT:w" s="T3084">dĭ</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3086" id="Seg_660" n="HIAT:w" s="T3085">šobi</ts>
                  <nts id="Seg_661" n="HIAT:ip">.</nts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3090" id="Seg_664" n="HIAT:u" s="T3086">
                  <ts e="T3087" id="Seg_666" n="HIAT:w" s="T3086">Dĭ</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3088" id="Seg_669" n="HIAT:w" s="T3087">iššo</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3089" id="Seg_672" n="HIAT:w" s="T3088">tăŋ</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3090" id="Seg_675" n="HIAT:w" s="T3089">kurolluʔpi</ts>
                  <nts id="Seg_676" n="HIAT:ip">.</nts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3096" id="Seg_679" n="HIAT:u" s="T3090">
                  <ts e="T3091" id="Seg_681" n="HIAT:w" s="T3090">Dĭgəttə</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3092" id="Seg_684" n="HIAT:w" s="T3091">nagur</ts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3093" id="Seg_687" n="HIAT:w" s="T3092">koʔbdobə</ts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3094" id="Seg_690" n="HIAT:w" s="T3093">öʔlüʔbi</ts>
                  <nts id="Seg_691" n="HIAT:ip">,</nts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3095" id="Seg_694" n="HIAT:w" s="T3094">nagur</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3096" id="Seg_697" n="HIAT:w" s="T3095">simatsiʔ</ts>
                  <nts id="Seg_698" n="HIAT:ip">.</nts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3099" id="Seg_701" n="HIAT:u" s="T3096">
                  <ts e="T3097" id="Seg_703" n="HIAT:w" s="T3096">Dĭ</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3098" id="Seg_706" n="HIAT:w" s="T3097">koʔbdo</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3099" id="Seg_709" n="HIAT:w" s="T3098">kambi</ts>
                  <nts id="Seg_710" n="HIAT:ip">.</nts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3102" id="Seg_713" n="HIAT:u" s="T3099">
                  <ts e="T3100" id="Seg_715" n="HIAT:w" s="T3099">I</ts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3101" id="Seg_718" n="HIAT:w" s="T3100">iʔbəbi</ts>
                  <nts id="Seg_719" n="HIAT:ip">,</nts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3102" id="Seg_722" n="HIAT:w" s="T3101">kunollaʔbə</ts>
                  <nts id="Seg_723" n="HIAT:ip">.</nts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3108" id="Seg_726" n="HIAT:u" s="T3102">
                  <nts id="Seg_727" n="HIAT:ip">(</nts>
                  <ts e="T3103" id="Seg_729" n="HIAT:w" s="T3102">Al-</ts>
                  <nts id="Seg_730" n="HIAT:ip">)</nts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3104" id="Seg_733" n="HIAT:w" s="T3103">Xavrošečka</ts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3105" id="Seg_736" n="HIAT:w" s="T3104">tože</ts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3106" id="Seg_739" n="HIAT:w" s="T3105">dĭm</ts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3107" id="Seg_742" n="HIAT:w" s="T3106">nüjnə</ts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3108" id="Seg_745" n="HIAT:w" s="T3107">nüjleʔbə</ts>
                  <nts id="Seg_746" n="HIAT:ip">.</nts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3116" id="Seg_749" n="HIAT:u" s="T3108">
                  <nts id="Seg_750" n="HIAT:ip">"</nts>
                  <ts e="T3109" id="Seg_752" n="HIAT:w" s="T3108">Kunolaʔ</ts>
                  <nts id="Seg_753" n="HIAT:ip">,</nts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3110" id="Seg_756" n="HIAT:w" s="T3109">sima</ts>
                  <nts id="Seg_757" n="HIAT:ip">,</nts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3111" id="Seg_760" n="HIAT:w" s="T3110">kunolaʔ</ts>
                  <nts id="Seg_761" n="HIAT:ip">,</nts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3112" id="Seg_764" n="HIAT:w" s="T3111">šide</ts>
                  <nts id="Seg_765" n="HIAT:ip">"</nts>
                  <nts id="Seg_766" n="HIAT:ip">;</nts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3113" id="Seg_769" n="HIAT:w" s="T3112">a</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3114" id="Seg_772" n="HIAT:w" s="T3113">nagur</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3115" id="Seg_775" n="HIAT:w" s="T3114">simattə</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3116" id="Seg_778" n="HIAT:w" s="T3115">nöməllüʔpi</ts>
                  <nts id="Seg_779" n="HIAT:ip">.</nts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3125" id="Seg_782" n="HIAT:u" s="T3116">
                  <ts e="T3117" id="Seg_784" n="HIAT:w" s="T3116">Dĭgəttə</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3118" id="Seg_787" n="HIAT:w" s="T3117">šide</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3119" id="Seg_790" n="HIAT:w" s="T3118">simat</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3120" id="Seg_793" n="HIAT:w" s="T3119">kunolluʔpiʔi</ts>
                  <nts id="Seg_794" n="HIAT:ip">,</nts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3121" id="Seg_797" n="HIAT:w" s="T3120">a</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3122" id="Seg_800" n="HIAT:w" s="T3121">onʼiʔ</ts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3123" id="Seg_803" n="HIAT:w" s="T3122">simat</ts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3124" id="Seg_806" n="HIAT:w" s="T3123">bar</ts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3125" id="Seg_809" n="HIAT:w" s="T3124">kubi</ts>
                  <nts id="Seg_810" n="HIAT:ip">.</nts>
                  <nts id="Seg_811" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3128" id="Seg_813" n="HIAT:u" s="T3125">
                  <ts e="T3126" id="Seg_815" n="HIAT:w" s="T3125">Dĭgəttə</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3127" id="Seg_818" n="HIAT:w" s="T3126">šobi</ts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3128" id="Seg_821" n="HIAT:w" s="T3127">maːʔndə</ts>
                  <nts id="Seg_822" n="HIAT:ip">.</nts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3132" id="Seg_825" n="HIAT:u" s="T3128">
                  <ts e="T3129" id="Seg_827" n="HIAT:w" s="T3128">Măndə:</ts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_829" n="HIAT:ip">"</nts>
                  <ts e="T3130" id="Seg_831" n="HIAT:w" s="T3129">Tüžöj</ts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3131" id="Seg_834" n="HIAT:w" s="T3130">dĭʔnə</ts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3132" id="Seg_837" n="HIAT:w" s="T3131">alia</ts>
                  <nts id="Seg_838" n="HIAT:ip">"</nts>
                  <nts id="Seg_839" n="HIAT:ip">.</nts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3135" id="Seg_842" n="HIAT:u" s="T3132">
                  <ts e="T3133" id="Seg_844" n="HIAT:w" s="T3132">Dĭgəttə</ts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3134" id="Seg_847" n="HIAT:w" s="T3133">ertən</ts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3135" id="Seg_850" n="HIAT:w" s="T3134">uʔbdəbi</ts>
                  <nts id="Seg_851" n="HIAT:ip">.</nts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3143" id="Seg_854" n="HIAT:u" s="T3135">
                  <ts e="T3136" id="Seg_856" n="HIAT:w" s="T3135">Dĭgəttə</ts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3137" id="Seg_859" n="HIAT:w" s="T3136">ertən</ts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3138" id="Seg_862" n="HIAT:w" s="T3137">uʔbdəbi</ts>
                  <nts id="Seg_863" n="HIAT:ip">,</nts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_865" n="HIAT:ip">(</nts>
                  <ts e="T3139" id="Seg_867" n="HIAT:w" s="T3138">büzʼen-</ts>
                  <nts id="Seg_868" n="HIAT:ip">)</nts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3140" id="Seg_871" n="HIAT:w" s="T3139">büzʼenə</ts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3141" id="Seg_874" n="HIAT:w" s="T3140">măndə:</ts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_876" n="HIAT:ip">"</nts>
                  <ts e="T3142" id="Seg_878" n="HIAT:w" s="T3141">Băttə</ts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3143" id="Seg_881" n="HIAT:w" s="T3142">tüžöjdə</ts>
                  <nts id="Seg_882" n="HIAT:ip">!</nts>
                  <nts id="Seg_883" n="HIAT:ip">"</nts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3151" id="Seg_886" n="HIAT:u" s="T3143">
                  <ts e="T3144" id="Seg_888" n="HIAT:w" s="T3143">A</ts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3145" id="Seg_891" n="HIAT:w" s="T3144">dĭ</ts>
                  <nts id="Seg_892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3146" id="Seg_894" n="HIAT:w" s="T3145">măndə:</ts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3146.tx.1" id="Seg_897" n="HIAT:w" s="T3146">Na</ts>
                  <nts id="Seg_898" n="HIAT:ip">_</nts>
                  <ts e="T3147" id="Seg_900" n="HIAT:w" s="T3146.tx.1">što</ts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3148" id="Seg_903" n="HIAT:w" s="T3147">băʔsittə</ts>
                  <nts id="Seg_904" n="HIAT:ip">,</nts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3149" id="Seg_907" n="HIAT:w" s="T3148">dĭ</ts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3150" id="Seg_910" n="HIAT:w" s="T3149">jakšə</ts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3151" id="Seg_913" n="HIAT:w" s="T3150">tüžöj</ts>
                  <nts id="Seg_914" n="HIAT:ip">.</nts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3154" id="Seg_917" n="HIAT:u" s="T3151">
                  <ts e="T3152" id="Seg_919" n="HIAT:w" s="T3151">Iʔgö</ts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3153" id="Seg_922" n="HIAT:w" s="T3152">sut</ts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3154" id="Seg_925" n="HIAT:w" s="T3153">mĭlie</ts>
                  <nts id="Seg_926" n="HIAT:ip">"</nts>
                  <nts id="Seg_927" n="HIAT:ip">.</nts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3161" id="Seg_930" n="HIAT:u" s="T3154">
                  <ts e="T3155" id="Seg_932" n="HIAT:w" s="T3154">A</ts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3156" id="Seg_935" n="HIAT:w" s="T3155">dĭ</ts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3157" id="Seg_938" n="HIAT:w" s="T3156">koʔbdo</ts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3158" id="Seg_941" n="HIAT:w" s="T3157">nuʔməluʔpi:</ts>
                  <nts id="Seg_942" n="HIAT:ip">"</nts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3159" id="Seg_945" n="HIAT:w" s="T3158">Tănan</ts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3160" id="Seg_948" n="HIAT:w" s="T3159">băʔsittə</ts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3161" id="Seg_951" n="HIAT:w" s="T3160">xočet</ts>
                  <nts id="Seg_952" n="HIAT:ip">"</nts>
                  <nts id="Seg_953" n="HIAT:ip">.</nts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3168" id="Seg_956" n="HIAT:u" s="T3161">
                  <ts e="T3162" id="Seg_958" n="HIAT:w" s="T3161">Dĭ</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3163" id="Seg_961" n="HIAT:w" s="T3162">măndə:</ts>
                  <nts id="Seg_962" n="HIAT:ip">"</nts>
                  <nts id="Seg_963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3164" id="Seg_965" n="HIAT:w" s="T3163">Iʔ</ts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3165" id="Seg_968" n="HIAT:w" s="T3164">amaʔ</ts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3166" id="Seg_971" n="HIAT:w" s="T3165">măn</ts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_973" n="HIAT:ip">(</nts>
                  <ts e="T3167" id="Seg_975" n="HIAT:w" s="T3166">um-</ts>
                  <nts id="Seg_976" n="HIAT:ip">)</nts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3168" id="Seg_979" n="HIAT:w" s="T3167">ujam</ts>
                  <nts id="Seg_980" n="HIAT:ip">.</nts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3173" id="Seg_983" n="HIAT:u" s="T3168">
                  <ts e="T3169" id="Seg_985" n="HIAT:w" s="T3168">A</ts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3170" id="Seg_988" n="HIAT:w" s="T3169">lezeŋbə</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3171" id="Seg_991" n="HIAT:w" s="T3170">it</ts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3172" id="Seg_994" n="HIAT:w" s="T3171">i</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3173" id="Seg_997" n="HIAT:w" s="T3172">kanaʔ</ts>
                  <nts id="Seg_998" n="HIAT:ip">!</nts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3175" id="Seg_1001" n="HIAT:u" s="T3173">
                  <ts e="T3174" id="Seg_1003" n="HIAT:w" s="T3173">Dʼünə</ts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3175" id="Seg_1006" n="HIAT:w" s="T3174">tĭləʔ</ts>
                  <nts id="Seg_1007" n="HIAT:ip">.</nts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3180" id="Seg_1010" n="HIAT:u" s="T3175">
                  <ts e="T3176" id="Seg_1012" n="HIAT:w" s="T3175">Dĭgəttə</ts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3177" id="Seg_1015" n="HIAT:w" s="T3176">dĭ</ts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3178" id="Seg_1018" n="HIAT:w" s="T3177">ej</ts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3179" id="Seg_1021" n="HIAT:w" s="T3178">ambi</ts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3180" id="Seg_1024" n="HIAT:w" s="T3179">uja</ts>
                  <nts id="Seg_1025" n="HIAT:ip">.</nts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3182" id="Seg_1028" n="HIAT:u" s="T3180">
                  <ts e="T3181" id="Seg_1030" n="HIAT:w" s="T3180">Lezeŋdə</ts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1032" n="HIAT:ip">(</nts>
                  <ts e="T3182" id="Seg_1034" n="HIAT:w" s="T3181">oʔbdəʔ</ts>
                  <nts id="Seg_1035" n="HIAT:ip">)</nts>
                  <nts id="Seg_1036" n="HIAT:ip">"</nts>
                  <nts id="Seg_1037" n="HIAT:ip">.</nts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3186" id="Seg_1040" n="HIAT:u" s="T3182">
                  <ts e="T3183" id="Seg_1042" n="HIAT:w" s="T3182">Dĭgəttə</ts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3184" id="Seg_1045" n="HIAT:w" s="T3183">büzʼet</ts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3185" id="Seg_1048" n="HIAT:w" s="T3184">dĭm</ts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3186" id="Seg_1051" n="HIAT:w" s="T3185">bătluʔpi</ts>
                  <nts id="Seg_1052" n="HIAT:ip">.</nts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3188" id="Seg_1055" n="HIAT:u" s="T3186">
                  <ts e="T3187" id="Seg_1057" n="HIAT:w" s="T3186">Ujazaŋdə</ts>
                  <nts id="Seg_1058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3188" id="Seg_1060" n="HIAT:w" s="T3187">ambiʔi</ts>
                  <nts id="Seg_1061" n="HIAT:ip">.</nts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3193" id="Seg_1064" n="HIAT:u" s="T3188">
                  <ts e="T3189" id="Seg_1066" n="HIAT:w" s="T3188">A</ts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3190" id="Seg_1069" n="HIAT:w" s="T3189">dĭ</ts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3191" id="Seg_1072" n="HIAT:w" s="T3190">Xavrošečka</ts>
                  <nts id="Seg_1073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3192" id="Seg_1075" n="HIAT:w" s="T3191">lezeŋdə</ts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3193" id="Seg_1078" n="HIAT:w" s="T3192">oʔbdobiʔi</ts>
                  <nts id="Seg_1079" n="HIAT:ip">.</nts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3196" id="Seg_1082" n="HIAT:u" s="T3193">
                  <ts e="T3194" id="Seg_1084" n="HIAT:w" s="T3193">Kambi</ts>
                  <nts id="Seg_1085" n="HIAT:ip">,</nts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3195" id="Seg_1088" n="HIAT:w" s="T3194">tĭlbi</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3196" id="Seg_1091" n="HIAT:w" s="T3195">dʼünə</ts>
                  <nts id="Seg_1092" n="HIAT:ip">.</nts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3203" id="Seg_1095" n="HIAT:u" s="T3196">
                  <ts e="T3197" id="Seg_1097" n="HIAT:w" s="T3196">Dĭgəttə</ts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3198" id="Seg_1100" n="HIAT:w" s="T3197">özerleʔbəʔi</ts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3199" id="Seg_1103" n="HIAT:w" s="T3198">jabloki</ts>
                  <nts id="Seg_1104" n="HIAT:ip">,</nts>
                  <nts id="Seg_1105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3200" id="Seg_1107" n="HIAT:w" s="T3199">kuvasəʔi</ts>
                  <nts id="Seg_1108" n="HIAT:ip">,</nts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3201" id="Seg_1111" n="HIAT:w" s="T3200">šində</ts>
                  <nts id="Seg_1112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3202" id="Seg_1114" n="HIAT:w" s="T3201">ej</ts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3203" id="Seg_1117" n="HIAT:w" s="T3202">šolia</ts>
                  <nts id="Seg_1118" n="HIAT:ip">.</nts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3206" id="Seg_1121" n="HIAT:u" s="T3203">
                  <ts e="T3204" id="Seg_1123" n="HIAT:w" s="T3203">Nulaʔbə</ts>
                  <nts id="Seg_1124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3205" id="Seg_1126" n="HIAT:w" s="T3204">da</ts>
                  <nts id="Seg_1127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3206" id="Seg_1129" n="HIAT:w" s="T3205">kulaʔbə</ts>
                  <nts id="Seg_1130" n="HIAT:ip">.</nts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3211" id="Seg_1133" n="HIAT:u" s="T3206">
                  <ts e="T3207" id="Seg_1135" n="HIAT:w" s="T3206">Dĭgəttə</ts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1137" n="HIAT:ip">(</nts>
                  <ts e="T3208" id="Seg_1139" n="HIAT:w" s="T3207">so-</ts>
                  <nts id="Seg_1140" n="HIAT:ip">)</nts>
                  <nts id="Seg_1141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3209" id="Seg_1143" n="HIAT:w" s="T3208">šonəga</ts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3210" id="Seg_1146" n="HIAT:w" s="T3209">koŋ</ts>
                  <nts id="Seg_1147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3211" id="Seg_1149" n="HIAT:w" s="T3210">kuza</ts>
                  <nts id="Seg_1150" n="HIAT:ip">.</nts>
                  <nts id="Seg_1151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3219" id="Seg_1153" n="HIAT:u" s="T3211">
                  <nts id="Seg_1154" n="HIAT:ip">"</nts>
                  <ts e="T3212" id="Seg_1156" n="HIAT:w" s="T3211">Šində</ts>
                  <nts id="Seg_1157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3213" id="Seg_1159" n="HIAT:w" s="T3212">măna</ts>
                  <nts id="Seg_1160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3214" id="Seg_1162" n="HIAT:w" s="T3213">mĭləj</ts>
                  <nts id="Seg_1163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3215" id="Seg_1165" n="HIAT:w" s="T3214">dĭ</ts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3216" id="Seg_1168" n="HIAT:w" s="T3215">jabločka</ts>
                  <nts id="Seg_1169" n="HIAT:ip">,</nts>
                  <nts id="Seg_1170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3217" id="Seg_1172" n="HIAT:w" s="T3216">dĭm</ts>
                  <nts id="Seg_1173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3218" id="Seg_1175" n="HIAT:w" s="T3217">tibinə</ts>
                  <nts id="Seg_1176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3219" id="Seg_1178" n="HIAT:w" s="T3218">ilem</ts>
                  <nts id="Seg_1179" n="HIAT:ip">.</nts>
                  <nts id="Seg_1180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3223" id="Seg_1182" n="HIAT:u" s="T3219">
                  <ts e="T3220" id="Seg_1184" n="HIAT:w" s="T3219">Dĭzeŋ</ts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3221" id="Seg_1187" n="HIAT:w" s="T3220">ej</ts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3222" id="Seg_1190" n="HIAT:w" s="T3221">mobiʔi</ts>
                  <nts id="Seg_1191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3223" id="Seg_1193" n="HIAT:w" s="T3222">izittə</ts>
                  <nts id="Seg_1194" n="HIAT:ip">.</nts>
                  <nts id="Seg_1195" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3225" id="Seg_1197" n="HIAT:u" s="T3223">
                  <ts e="T3224" id="Seg_1199" n="HIAT:w" s="T3223">Nʼuʔdə</ts>
                  <nts id="Seg_1200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3225" id="Seg_1202" n="HIAT:w" s="T3224">kandəgaʔi</ts>
                  <nts id="Seg_1203" n="HIAT:ip">.</nts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3240" id="Seg_1206" n="HIAT:u" s="T3225">
                  <ts e="T3226" id="Seg_1208" n="HIAT:w" s="T3225">Dĭgəttə</ts>
                  <nts id="Seg_1209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3227" id="Seg_1211" n="HIAT:w" s="T3226">dĭ</ts>
                  <nts id="Seg_1212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3228" id="Seg_1214" n="HIAT:w" s="T3227">Xavroša</ts>
                  <nts id="Seg_1215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3229" id="Seg_1217" n="HIAT:w" s="T3228">šobi</ts>
                  <nts id="Seg_1218" n="HIAT:ip">,</nts>
                  <nts id="Seg_1219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1220" n="HIAT:ip">(</nts>
                  <ts e="T3230" id="Seg_1222" n="HIAT:w" s="T3229">dĭ=</ts>
                  <nts id="Seg_1223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3231" id="Seg_1225" n="HIAT:w" s="T3230">dĭ=</ts>
                  <nts id="Seg_1226" n="HIAT:ip">)</nts>
                  <nts id="Seg_1227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3232" id="Seg_1229" n="HIAT:w" s="T3231">dĭʔnə</ts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1231" n="HIAT:ip">(</nts>
                  <ts e="T3233" id="Seg_1233" n="HIAT:w" s="T3232">na-</ts>
                  <nts id="Seg_1234" n="HIAT:ip">)</nts>
                  <nts id="Seg_1235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3234" id="Seg_1237" n="HIAT:w" s="T3233">nagnulisʼ</ts>
                  <nts id="Seg_1238" n="HIAT:ip">,</nts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3235" id="Seg_1241" n="HIAT:w" s="T3234">dĭ</ts>
                  <nts id="Seg_1242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3236" id="Seg_1244" n="HIAT:w" s="T3235">jabləkə</ts>
                  <nts id="Seg_1245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3237" id="Seg_1247" n="HIAT:w" s="T3236">mĭbi</ts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1249" n="HIAT:ip">(</nts>
                  <ts e="T3238" id="Seg_1251" n="HIAT:w" s="T3237">dĭ=</ts>
                  <nts id="Seg_1252" n="HIAT:ip">)</nts>
                  <nts id="Seg_1253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3239" id="Seg_1255" n="HIAT:w" s="T3238">dĭ</ts>
                  <nts id="Seg_1256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3240" id="Seg_1258" n="HIAT:w" s="T3239">kuzanə</ts>
                  <nts id="Seg_1259" n="HIAT:ip">.</nts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3248" id="Seg_1262" n="HIAT:u" s="T3240">
                  <ts e="T3241" id="Seg_1264" n="HIAT:w" s="T3240">Dĭ</ts>
                  <nts id="Seg_1265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3242" id="Seg_1267" n="HIAT:w" s="T3241">dĭm</ts>
                  <nts id="Seg_1268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3243" id="Seg_1270" n="HIAT:w" s="T3242">tibinə</ts>
                  <nts id="Seg_1271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3244" id="Seg_1273" n="HIAT:w" s="T3243">ibi</ts>
                  <nts id="Seg_1274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3245" id="Seg_1276" n="HIAT:w" s="T3244">i</ts>
                  <nts id="Seg_1277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3246" id="Seg_1279" n="HIAT:w" s="T3245">kalla</ts>
                  <nts id="Seg_1280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3247" id="Seg_1282" n="HIAT:w" s="T3246">dʼürbi</ts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3248" id="Seg_1285" n="HIAT:w" s="T3247">dĭzeŋgəʔ</ts>
                  <nts id="Seg_1286" n="HIAT:ip">.</nts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T3249" id="Seg_1289" n="HIAT:u" s="T3248">
                  <ts e="T3249" id="Seg_1291" n="HIAT:w" s="T3248">Kabarləj</ts>
                  <nts id="Seg_1292" n="HIAT:ip">.</nts>
                  <nts id="Seg_1293" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T3249" id="Seg_1294" n="sc" s="T2918">
               <ts e="T2919" id="Seg_1296" n="e" s="T2918">Amnobi </ts>
               <ts e="T2920" id="Seg_1298" n="e" s="T2919">onʼiʔ </ts>
               <ts e="T2921" id="Seg_1300" n="e" s="T2920">koʔbdo. </ts>
               <ts e="T2922" id="Seg_1302" n="e" s="T2921">(Di-) </ts>
               <ts e="T2923" id="Seg_1304" n="e" s="T2922">Dĭm </ts>
               <ts e="T2924" id="Seg_1306" n="e" s="T2923">numəjleʔbəʔi </ts>
               <ts e="T2925" id="Seg_1308" n="e" s="T2924">Xavrošečka. </ts>
               <ts e="T2926" id="Seg_1310" n="e" s="T2925">Dĭn </ts>
               <ts e="T2927" id="Seg_1312" n="e" s="T2926">ijat, </ts>
               <ts e="T2928" id="Seg_1314" n="e" s="T2927">abat </ts>
               <ts e="T2929" id="Seg_1316" n="e" s="T2928">külaːmbiʔi. </ts>
               <ts e="T2930" id="Seg_1318" n="e" s="T2929">Il </ts>
               <ts e="T2931" id="Seg_1320" n="e" s="T2930">ej </ts>
               <ts e="T2932" id="Seg_1322" n="e" s="T2931">jakšəʔi </ts>
               <ts e="T2933" id="Seg_1324" n="e" s="T2932">bar. </ts>
               <ts e="T2934" id="Seg_1326" n="e" s="T2933">(Dĭʔnə=) </ts>
               <ts e="T2935" id="Seg_1328" n="e" s="T2934">Dĭm </ts>
               <ts e="T2936" id="Seg_1330" n="e" s="T2935">ibiʔi. </ts>
               <ts e="T2937" id="Seg_1332" n="e" s="T2936">Dĭn </ts>
               <ts e="T2938" id="Seg_1334" n="e" s="T2937">nagur </ts>
               <ts e="T2939" id="Seg_1336" n="e" s="T2938">koʔbdot </ts>
               <ts e="T2940" id="Seg_1338" n="e" s="T2939">ibiʔi. </ts>
               <ts e="T2941" id="Seg_1340" n="e" s="T2940">Ĭmbidə </ts>
               <ts e="T2942" id="Seg_1342" n="e" s="T2941">ej </ts>
               <ts e="T2943" id="Seg_1344" n="e" s="T2942">abiʔi. </ts>
               <ts e="T2944" id="Seg_1346" n="e" s="T2943">A </ts>
               <ts e="T2945" id="Seg_1348" n="e" s="T2944">dĭ </ts>
               <ts e="T2946" id="Seg_1350" n="e" s="T2945">Xavrošečka </ts>
               <ts e="T2947" id="Seg_1352" n="e" s="T2946">bar </ts>
               <ts e="T2948" id="Seg_1354" n="e" s="T2947">togonorbi. </ts>
               <ts e="T2949" id="Seg_1356" n="e" s="T2948">Bar </ts>
               <ts e="T2950" id="Seg_1358" n="e" s="T2949">ĭmbi </ts>
               <ts e="T2951" id="Seg_1360" n="e" s="T2950">togonoria, </ts>
               <ts e="T2952" id="Seg_1362" n="e" s="T2951">(ej=) </ts>
               <ts e="T2953" id="Seg_1364" n="e" s="T2952">dĭzeŋdə </ts>
               <ts e="T2954" id="Seg_1366" n="e" s="T2953">ej </ts>
               <ts e="T2955" id="Seg_1368" n="e" s="T2954">molia. </ts>
               <ts e="T2956" id="Seg_1370" n="e" s="T2955">Uge </ts>
               <ts e="T2957" id="Seg_1372" n="e" s="T2956">kudollaʔbə </ts>
               <ts e="T2958" id="Seg_1374" n="e" s="T2957">nüke. </ts>
               <ts e="T2959" id="Seg_1376" n="e" s="T2958">Dĭgəttə </ts>
               <ts e="T2960" id="Seg_1378" n="e" s="T2959">măndə: </ts>
               <ts e="T2961" id="Seg_1380" n="e" s="T2960">"Kanaʔ, </ts>
               <ts e="T2962" id="Seg_1382" n="e" s="T2961">măna </ts>
               <ts e="T2963" id="Seg_1384" n="e" s="T2962">aʔ </ts>
               <ts e="T2964" id="Seg_1386" n="e" s="T2963">iʔgö </ts>
               <ts e="T2965" id="Seg_1388" n="e" s="T2964">xolstə". </ts>
               <ts e="T2966" id="Seg_1390" n="e" s="T2965">Dĭ </ts>
               <ts e="T2967" id="Seg_1392" n="e" s="T2966">kambi </ts>
               <ts e="T2968" id="Seg_1394" n="e" s="T2967">tüžöjdə </ts>
               <ts e="T2969" id="Seg_1396" n="e" s="T2968">da </ts>
               <ts e="T2970" id="Seg_1398" n="e" s="T2969">bar </ts>
               <ts e="T2971" id="Seg_1400" n="e" s="T2970">dʼorlaʔbə: </ts>
               <ts e="T2972" id="Seg_1402" n="e" s="T2971">"(M-) </ts>
               <ts e="T2973" id="Seg_1404" n="e" s="T2972">Măna </ts>
               <ts e="T2974" id="Seg_1406" n="e" s="T2973">teŋ </ts>
               <ts e="T2975" id="Seg_1408" n="e" s="T2974">münörleʔbəʔjə. </ts>
               <ts e="T2976" id="Seg_1410" n="e" s="T2975">Dĭzeŋdə </ts>
               <ts e="T2977" id="Seg_1412" n="e" s="T2976">ĭmbidə </ts>
               <ts e="T2978" id="Seg_1414" n="e" s="T2977">asʼtə </ts>
               <ts e="T2979" id="Seg_1416" n="e" s="T2978">ej </ts>
               <ts e="T2980" id="Seg_1418" n="e" s="T2979">(m-) </ts>
               <ts e="T2981" id="Seg_1420" n="e" s="T2980">moliam". </ts>
               <ts e="T2982" id="Seg_1422" n="e" s="T2981">I </ts>
               <ts e="T2983" id="Seg_1424" n="e" s="T2982">tüžöj </ts>
               <ts e="T2984" id="Seg_1426" n="e" s="T2983">măndə:" </ts>
               <ts e="T2985" id="Seg_1428" n="e" s="T2984">Onʼiʔ </ts>
               <ts e="T2986" id="Seg_1430" n="e" s="T2985">kuʔdə </ts>
               <ts e="T2987" id="Seg_1432" n="e" s="T2986">pădaʔ, </ts>
               <ts e="T2988" id="Seg_1434" n="e" s="T2987">a </ts>
               <ts e="T2989" id="Seg_1436" n="e" s="T2988">onʼiʔtə </ts>
               <ts e="T2990" id="Seg_1438" n="e" s="T2989">supsoʔ! </ts>
               <ts e="T2991" id="Seg_1440" n="e" s="T2990">Bar </ts>
               <ts e="T2992" id="Seg_1442" n="e" s="T2991">moləj". </ts>
               <ts e="T2993" id="Seg_1444" n="e" s="T2992">Dĭgəttə </ts>
               <ts e="T2994" id="Seg_1446" n="e" s="T2993">(dĭn-) </ts>
               <ts e="T2995" id="Seg_1448" n="e" s="T2994">dĭʔnə </ts>
               <ts e="T2996" id="Seg_1450" n="e" s="T2995">(am-) </ts>
               <ts e="T2997" id="Seg_1452" n="e" s="T2996">abi. </ts>
               <ts e="T2998" id="Seg_1454" n="e" s="T2997">I </ts>
               <ts e="T2999" id="Seg_1456" n="e" s="T2998">ererbi, </ts>
               <ts e="T3000" id="Seg_1458" n="e" s="T2999">xolstə </ts>
               <ts e="T3001" id="Seg_1460" n="e" s="T3000">abi. </ts>
               <ts e="T3002" id="Seg_1462" n="e" s="T3001">(Dĭ </ts>
               <ts e="T3003" id="Seg_1464" n="e" s="T3002">dĭgəttə) </ts>
               <ts e="T3004" id="Seg_1466" n="e" s="T3003">deʔpi. </ts>
               <ts e="T3005" id="Seg_1468" n="e" s="T3004">Dĭgəttə </ts>
               <ts e="T3006" id="Seg_1470" n="e" s="T3005">bazoʔ </ts>
               <ts e="T3007" id="Seg_1472" n="e" s="T3006">iʔgö </ts>
               <ts e="T3008" id="Seg_1474" n="e" s="T3007">mobi. </ts>
               <ts e="T3009" id="Seg_1476" n="e" s="T3008">Dĭ </ts>
               <ts e="T3010" id="Seg_1478" n="e" s="T3009">bazoʔ </ts>
               <ts e="T3011" id="Seg_1480" n="e" s="T3010">kambi </ts>
               <ts e="T3012" id="Seg_1482" n="e" s="T3011">tüžöjdə, </ts>
               <ts e="T3013" id="Seg_1484" n="e" s="T3012">bazoʔ </ts>
               <ts e="T3014" id="Seg_1486" n="e" s="T3013">abi. </ts>
               <ts e="T3015" id="Seg_1488" n="e" s="T3014">Dĭgəttə </ts>
               <ts e="T3016" id="Seg_1490" n="e" s="T3015">măndə </ts>
               <ts e="T3017" id="Seg_1492" n="e" s="T3016">onʼiʔ </ts>
               <ts e="T3018" id="Seg_1494" n="e" s="T3017">koʔbdondə. </ts>
               <ts e="T3019" id="Seg_1496" n="e" s="T3018">Onʼiʔ </ts>
               <ts e="T3020" id="Seg_1498" n="e" s="T3019">sʼimandə. </ts>
               <ts e="T3021" id="Seg_1500" n="e" s="T3020">"Kanaʔ </ts>
               <ts e="T3022" id="Seg_1502" n="e" s="T3021">dĭziʔ"; </ts>
               <ts e="T3023" id="Seg_1504" n="e" s="T3022">dĭ </ts>
               <ts e="T3024" id="Seg_1506" n="e" s="T3023">kambi. </ts>
               <ts e="T3025" id="Seg_1508" n="e" s="T3024">Dĭn </ts>
               <ts e="T3026" id="Seg_1510" n="e" s="T3025">iʔbəbi </ts>
               <ts e="T3027" id="Seg_1512" n="e" s="T3026">kunolzittə. </ts>
               <ts e="T3028" id="Seg_1514" n="e" s="T3027">Dĭ </ts>
               <ts e="T3029" id="Seg_1516" n="e" s="T3028">bar:" </ts>
               <ts e="T3030" id="Seg_1518" n="e" s="T3029">Kunoldə, </ts>
               <ts e="T3031" id="Seg_1520" n="e" s="T3030">sima, </ts>
               <ts e="T3032" id="Seg_1522" n="e" s="T3031">kunoldə, </ts>
               <ts e="T3033" id="Seg_1524" n="e" s="T3032">sima"; </ts>
               <ts e="T3034" id="Seg_1526" n="e" s="T3033">simat </ts>
               <ts e="T3035" id="Seg_1528" n="e" s="T3034">kunolluʔpi. </ts>
               <ts e="T3036" id="Seg_1530" n="e" s="T3035">Dĭʔnə </ts>
               <ts e="T3037" id="Seg_1532" n="e" s="T3036">bazoʔ </ts>
               <ts e="T3038" id="Seg_1534" n="e" s="T3037">tüžöj </ts>
               <ts e="T3039" id="Seg_1536" n="e" s="T3038">ererluʔpi, </ts>
               <ts e="T3040" id="Seg_1538" n="e" s="T3039">dĭ </ts>
               <ts e="T3041" id="Seg_1540" n="e" s="T3040">ibi. </ts>
               <ts e="T3042" id="Seg_1542" n="e" s="T3041">Trubkaʔi </ts>
               <ts e="T3043" id="Seg_1544" n="e" s="T3042">xolstəʔi </ts>
               <ts e="T3044" id="Seg_1546" n="e" s="T3043">deʔpi. </ts>
               <ts e="T3045" id="Seg_1548" n="e" s="T3044">Dĭ </ts>
               <ts e="T3046" id="Seg_1550" n="e" s="T3045">nükenə </ts>
               <ts e="T3047" id="Seg_1552" n="e" s="T3046">mĭbi. </ts>
               <ts e="T3048" id="Seg_1554" n="e" s="T3047">Dĭgəttə </ts>
               <ts e="T3049" id="Seg_1556" n="e" s="T3048">dĭ </ts>
               <ts e="T3050" id="Seg_1558" n="e" s="T3049">nüke </ts>
               <ts e="T3051" id="Seg_1560" n="e" s="T3050">baška </ts>
               <ts e="T3052" id="Seg_1562" n="e" s="T3051">koʔbdobə </ts>
               <ts e="T3053" id="Seg_1564" n="e" s="T3052">öʔleʔbə. </ts>
               <ts e="T3054" id="Seg_1566" n="e" s="T3053">"Kanaʔ, </ts>
               <ts e="T3055" id="Seg_1568" n="e" s="T3054">ĭmbi </ts>
               <ts e="T3056" id="Seg_1570" n="e" s="T3055">dĭ </ts>
               <ts e="T3057" id="Seg_1572" n="e" s="T3056">dĭn </ts>
               <ts e="T3058" id="Seg_1574" n="e" s="T3057">alia?" </ts>
               <ts e="T3059" id="Seg_1576" n="e" s="T3058">Dĭ </ts>
               <ts e="T3060" id="Seg_1578" n="e" s="T3059">koʔbdo </ts>
               <ts e="T3061" id="Seg_1580" n="e" s="T3060">kambi. </ts>
               <ts e="T3062" id="Seg_1582" n="e" s="T3061">Dĭn </ts>
               <ts e="T3063" id="Seg_1584" n="e" s="T3062">iʔbəbi. </ts>
               <ts e="T3064" id="Seg_1586" n="e" s="T3063">Dĭ </ts>
               <ts e="T3065" id="Seg_1588" n="e" s="T3064">bar </ts>
               <ts e="T3066" id="Seg_1590" n="e" s="T3065">dĭm </ts>
               <ts e="T3067" id="Seg_1592" n="e" s="T3066">kunolluʔpi. </ts>
               <ts e="T3068" id="Seg_1594" n="e" s="T3067">"(Ku-) </ts>
               <ts e="T3069" id="Seg_1596" n="e" s="T3068">Kunolaʔ, </ts>
               <ts e="T3070" id="Seg_1598" n="e" s="T3069">sima, </ts>
               <ts e="T3071" id="Seg_1600" n="e" s="T3070">kunolaʔ, </ts>
               <ts e="T3072" id="Seg_1602" n="e" s="T3071">baška </ts>
               <ts e="T3073" id="Seg_1604" n="e" s="T3072">sima"; </ts>
               <ts e="T3074" id="Seg_1606" n="e" s="T3073">simazaŋdə </ts>
               <ts e="T3075" id="Seg_1608" n="e" s="T3074">kunolluʔpi. </ts>
               <ts e="T3076" id="Seg_1610" n="e" s="T3075">Tüžöj </ts>
               <ts e="T3077" id="Seg_1612" n="e" s="T3076">dĭʔnə </ts>
               <ts e="T3078" id="Seg_1614" n="e" s="T3077">abi. </ts>
               <ts e="T3079" id="Seg_1616" n="e" s="T3078">Xolstə </ts>
               <ts e="T3080" id="Seg_1618" n="e" s="T3079">ererbi </ts>
               <ts e="T3081" id="Seg_1620" n="e" s="T3080">i </ts>
               <ts e="T3082" id="Seg_1622" n="e" s="T3081">trubkaʔi </ts>
               <ts e="T3083" id="Seg_1624" n="e" s="T3082">mĭbi, </ts>
               <ts e="T3084" id="Seg_1626" n="e" s="T3083">dĭgəttə </ts>
               <ts e="T3085" id="Seg_1628" n="e" s="T3084">dĭ </ts>
               <ts e="T3086" id="Seg_1630" n="e" s="T3085">šobi. </ts>
               <ts e="T3087" id="Seg_1632" n="e" s="T3086">Dĭ </ts>
               <ts e="T3088" id="Seg_1634" n="e" s="T3087">iššo </ts>
               <ts e="T3089" id="Seg_1636" n="e" s="T3088">tăŋ </ts>
               <ts e="T3090" id="Seg_1638" n="e" s="T3089">kurolluʔpi. </ts>
               <ts e="T3091" id="Seg_1640" n="e" s="T3090">Dĭgəttə </ts>
               <ts e="T3092" id="Seg_1642" n="e" s="T3091">nagur </ts>
               <ts e="T3093" id="Seg_1644" n="e" s="T3092">koʔbdobə </ts>
               <ts e="T3094" id="Seg_1646" n="e" s="T3093">öʔlüʔbi, </ts>
               <ts e="T3095" id="Seg_1648" n="e" s="T3094">nagur </ts>
               <ts e="T3096" id="Seg_1650" n="e" s="T3095">simatsiʔ. </ts>
               <ts e="T3097" id="Seg_1652" n="e" s="T3096">Dĭ </ts>
               <ts e="T3098" id="Seg_1654" n="e" s="T3097">koʔbdo </ts>
               <ts e="T3099" id="Seg_1656" n="e" s="T3098">kambi. </ts>
               <ts e="T3100" id="Seg_1658" n="e" s="T3099">I </ts>
               <ts e="T3101" id="Seg_1660" n="e" s="T3100">iʔbəbi, </ts>
               <ts e="T3102" id="Seg_1662" n="e" s="T3101">kunollaʔbə. </ts>
               <ts e="T3103" id="Seg_1664" n="e" s="T3102">(Al-) </ts>
               <ts e="T3104" id="Seg_1666" n="e" s="T3103">Xavrošečka </ts>
               <ts e="T3105" id="Seg_1668" n="e" s="T3104">tože </ts>
               <ts e="T3106" id="Seg_1670" n="e" s="T3105">dĭm </ts>
               <ts e="T3107" id="Seg_1672" n="e" s="T3106">nüjnə </ts>
               <ts e="T3108" id="Seg_1674" n="e" s="T3107">nüjleʔbə. </ts>
               <ts e="T3109" id="Seg_1676" n="e" s="T3108">"Kunolaʔ, </ts>
               <ts e="T3110" id="Seg_1678" n="e" s="T3109">sima, </ts>
               <ts e="T3111" id="Seg_1680" n="e" s="T3110">kunolaʔ, </ts>
               <ts e="T3112" id="Seg_1682" n="e" s="T3111">šide"; </ts>
               <ts e="T3113" id="Seg_1684" n="e" s="T3112">a </ts>
               <ts e="T3114" id="Seg_1686" n="e" s="T3113">nagur </ts>
               <ts e="T3115" id="Seg_1688" n="e" s="T3114">simattə </ts>
               <ts e="T3116" id="Seg_1690" n="e" s="T3115">nöməllüʔpi. </ts>
               <ts e="T3117" id="Seg_1692" n="e" s="T3116">Dĭgəttə </ts>
               <ts e="T3118" id="Seg_1694" n="e" s="T3117">šide </ts>
               <ts e="T3119" id="Seg_1696" n="e" s="T3118">simat </ts>
               <ts e="T3120" id="Seg_1698" n="e" s="T3119">kunolluʔpiʔi, </ts>
               <ts e="T3121" id="Seg_1700" n="e" s="T3120">a </ts>
               <ts e="T3122" id="Seg_1702" n="e" s="T3121">onʼiʔ </ts>
               <ts e="T3123" id="Seg_1704" n="e" s="T3122">simat </ts>
               <ts e="T3124" id="Seg_1706" n="e" s="T3123">bar </ts>
               <ts e="T3125" id="Seg_1708" n="e" s="T3124">kubi. </ts>
               <ts e="T3126" id="Seg_1710" n="e" s="T3125">Dĭgəttə </ts>
               <ts e="T3127" id="Seg_1712" n="e" s="T3126">šobi </ts>
               <ts e="T3128" id="Seg_1714" n="e" s="T3127">maːʔndə. </ts>
               <ts e="T3129" id="Seg_1716" n="e" s="T3128">Măndə: </ts>
               <ts e="T3130" id="Seg_1718" n="e" s="T3129">"Tüžöj </ts>
               <ts e="T3131" id="Seg_1720" n="e" s="T3130">dĭʔnə </ts>
               <ts e="T3132" id="Seg_1722" n="e" s="T3131">alia". </ts>
               <ts e="T3133" id="Seg_1724" n="e" s="T3132">Dĭgəttə </ts>
               <ts e="T3134" id="Seg_1726" n="e" s="T3133">ertən </ts>
               <ts e="T3135" id="Seg_1728" n="e" s="T3134">uʔbdəbi. </ts>
               <ts e="T3136" id="Seg_1730" n="e" s="T3135">Dĭgəttə </ts>
               <ts e="T3137" id="Seg_1732" n="e" s="T3136">ertən </ts>
               <ts e="T3138" id="Seg_1734" n="e" s="T3137">uʔbdəbi, </ts>
               <ts e="T3139" id="Seg_1736" n="e" s="T3138">(büzʼen-) </ts>
               <ts e="T3140" id="Seg_1738" n="e" s="T3139">büzʼenə </ts>
               <ts e="T3141" id="Seg_1740" n="e" s="T3140">măndə: </ts>
               <ts e="T3142" id="Seg_1742" n="e" s="T3141">"Băttə </ts>
               <ts e="T3143" id="Seg_1744" n="e" s="T3142">tüžöjdə!" </ts>
               <ts e="T3144" id="Seg_1746" n="e" s="T3143">A </ts>
               <ts e="T3145" id="Seg_1748" n="e" s="T3144">dĭ </ts>
               <ts e="T3146" id="Seg_1750" n="e" s="T3145">măndə: </ts>
               <ts e="T3147" id="Seg_1752" n="e" s="T3146">Na_što </ts>
               <ts e="T3148" id="Seg_1754" n="e" s="T3147">băʔsittə, </ts>
               <ts e="T3149" id="Seg_1756" n="e" s="T3148">dĭ </ts>
               <ts e="T3150" id="Seg_1758" n="e" s="T3149">jakšə </ts>
               <ts e="T3151" id="Seg_1760" n="e" s="T3150">tüžöj. </ts>
               <ts e="T3152" id="Seg_1762" n="e" s="T3151">Iʔgö </ts>
               <ts e="T3153" id="Seg_1764" n="e" s="T3152">sut </ts>
               <ts e="T3154" id="Seg_1766" n="e" s="T3153">mĭlie". </ts>
               <ts e="T3155" id="Seg_1768" n="e" s="T3154">A </ts>
               <ts e="T3156" id="Seg_1770" n="e" s="T3155">dĭ </ts>
               <ts e="T3157" id="Seg_1772" n="e" s="T3156">koʔbdo </ts>
               <ts e="T3158" id="Seg_1774" n="e" s="T3157">nuʔməluʔpi:" </ts>
               <ts e="T3159" id="Seg_1776" n="e" s="T3158">Tănan </ts>
               <ts e="T3160" id="Seg_1778" n="e" s="T3159">băʔsittə </ts>
               <ts e="T3161" id="Seg_1780" n="e" s="T3160">xočet". </ts>
               <ts e="T3162" id="Seg_1782" n="e" s="T3161">Dĭ </ts>
               <ts e="T3163" id="Seg_1784" n="e" s="T3162">măndə:" </ts>
               <ts e="T3164" id="Seg_1786" n="e" s="T3163">Iʔ </ts>
               <ts e="T3165" id="Seg_1788" n="e" s="T3164">amaʔ </ts>
               <ts e="T3166" id="Seg_1790" n="e" s="T3165">măn </ts>
               <ts e="T3167" id="Seg_1792" n="e" s="T3166">(um-) </ts>
               <ts e="T3168" id="Seg_1794" n="e" s="T3167">ujam. </ts>
               <ts e="T3169" id="Seg_1796" n="e" s="T3168">A </ts>
               <ts e="T3170" id="Seg_1798" n="e" s="T3169">lezeŋbə </ts>
               <ts e="T3171" id="Seg_1800" n="e" s="T3170">it </ts>
               <ts e="T3172" id="Seg_1802" n="e" s="T3171">i </ts>
               <ts e="T3173" id="Seg_1804" n="e" s="T3172">kanaʔ! </ts>
               <ts e="T3174" id="Seg_1806" n="e" s="T3173">Dʼünə </ts>
               <ts e="T3175" id="Seg_1808" n="e" s="T3174">tĭləʔ. </ts>
               <ts e="T3176" id="Seg_1810" n="e" s="T3175">Dĭgəttə </ts>
               <ts e="T3177" id="Seg_1812" n="e" s="T3176">dĭ </ts>
               <ts e="T3178" id="Seg_1814" n="e" s="T3177">ej </ts>
               <ts e="T3179" id="Seg_1816" n="e" s="T3178">ambi </ts>
               <ts e="T3180" id="Seg_1818" n="e" s="T3179">uja. </ts>
               <ts e="T3181" id="Seg_1820" n="e" s="T3180">Lezeŋdə </ts>
               <ts e="T3182" id="Seg_1822" n="e" s="T3181">(oʔbdəʔ)". </ts>
               <ts e="T3183" id="Seg_1824" n="e" s="T3182">Dĭgəttə </ts>
               <ts e="T3184" id="Seg_1826" n="e" s="T3183">büzʼet </ts>
               <ts e="T3185" id="Seg_1828" n="e" s="T3184">dĭm </ts>
               <ts e="T3186" id="Seg_1830" n="e" s="T3185">bătluʔpi. </ts>
               <ts e="T3187" id="Seg_1832" n="e" s="T3186">Ujazaŋdə </ts>
               <ts e="T3188" id="Seg_1834" n="e" s="T3187">ambiʔi. </ts>
               <ts e="T3189" id="Seg_1836" n="e" s="T3188">A </ts>
               <ts e="T3190" id="Seg_1838" n="e" s="T3189">dĭ </ts>
               <ts e="T3191" id="Seg_1840" n="e" s="T3190">Xavrošečka </ts>
               <ts e="T3192" id="Seg_1842" n="e" s="T3191">lezeŋdə </ts>
               <ts e="T3193" id="Seg_1844" n="e" s="T3192">oʔbdobiʔi. </ts>
               <ts e="T3194" id="Seg_1846" n="e" s="T3193">Kambi, </ts>
               <ts e="T3195" id="Seg_1848" n="e" s="T3194">tĭlbi </ts>
               <ts e="T3196" id="Seg_1850" n="e" s="T3195">dʼünə. </ts>
               <ts e="T3197" id="Seg_1852" n="e" s="T3196">Dĭgəttə </ts>
               <ts e="T3198" id="Seg_1854" n="e" s="T3197">özerleʔbəʔi </ts>
               <ts e="T3199" id="Seg_1856" n="e" s="T3198">jabloki, </ts>
               <ts e="T3200" id="Seg_1858" n="e" s="T3199">kuvasəʔi, </ts>
               <ts e="T3201" id="Seg_1860" n="e" s="T3200">šində </ts>
               <ts e="T3202" id="Seg_1862" n="e" s="T3201">ej </ts>
               <ts e="T3203" id="Seg_1864" n="e" s="T3202">šolia. </ts>
               <ts e="T3204" id="Seg_1866" n="e" s="T3203">Nulaʔbə </ts>
               <ts e="T3205" id="Seg_1868" n="e" s="T3204">da </ts>
               <ts e="T3206" id="Seg_1870" n="e" s="T3205">kulaʔbə. </ts>
               <ts e="T3207" id="Seg_1872" n="e" s="T3206">Dĭgəttə </ts>
               <ts e="T3208" id="Seg_1874" n="e" s="T3207">(so-) </ts>
               <ts e="T3209" id="Seg_1876" n="e" s="T3208">šonəga </ts>
               <ts e="T3210" id="Seg_1878" n="e" s="T3209">koŋ </ts>
               <ts e="T3211" id="Seg_1880" n="e" s="T3210">kuza. </ts>
               <ts e="T3212" id="Seg_1882" n="e" s="T3211">"Šində </ts>
               <ts e="T3213" id="Seg_1884" n="e" s="T3212">măna </ts>
               <ts e="T3214" id="Seg_1886" n="e" s="T3213">mĭləj </ts>
               <ts e="T3215" id="Seg_1888" n="e" s="T3214">dĭ </ts>
               <ts e="T3216" id="Seg_1890" n="e" s="T3215">jabločka, </ts>
               <ts e="T3217" id="Seg_1892" n="e" s="T3216">dĭm </ts>
               <ts e="T3218" id="Seg_1894" n="e" s="T3217">tibinə </ts>
               <ts e="T3219" id="Seg_1896" n="e" s="T3218">ilem. </ts>
               <ts e="T3220" id="Seg_1898" n="e" s="T3219">Dĭzeŋ </ts>
               <ts e="T3221" id="Seg_1900" n="e" s="T3220">ej </ts>
               <ts e="T3222" id="Seg_1902" n="e" s="T3221">mobiʔi </ts>
               <ts e="T3223" id="Seg_1904" n="e" s="T3222">izittə. </ts>
               <ts e="T3224" id="Seg_1906" n="e" s="T3223">Nʼuʔdə </ts>
               <ts e="T3225" id="Seg_1908" n="e" s="T3224">kandəgaʔi. </ts>
               <ts e="T3226" id="Seg_1910" n="e" s="T3225">Dĭgəttə </ts>
               <ts e="T3227" id="Seg_1912" n="e" s="T3226">dĭ </ts>
               <ts e="T3228" id="Seg_1914" n="e" s="T3227">Xavroša </ts>
               <ts e="T3229" id="Seg_1916" n="e" s="T3228">šobi, </ts>
               <ts e="T3230" id="Seg_1918" n="e" s="T3229">(dĭ= </ts>
               <ts e="T3231" id="Seg_1920" n="e" s="T3230">dĭ=) </ts>
               <ts e="T3232" id="Seg_1922" n="e" s="T3231">dĭʔnə </ts>
               <ts e="T3233" id="Seg_1924" n="e" s="T3232">(na-) </ts>
               <ts e="T3234" id="Seg_1926" n="e" s="T3233">nagnulisʼ, </ts>
               <ts e="T3235" id="Seg_1928" n="e" s="T3234">dĭ </ts>
               <ts e="T3236" id="Seg_1930" n="e" s="T3235">jabləkə </ts>
               <ts e="T3237" id="Seg_1932" n="e" s="T3236">mĭbi </ts>
               <ts e="T3238" id="Seg_1934" n="e" s="T3237">(dĭ=) </ts>
               <ts e="T3239" id="Seg_1936" n="e" s="T3238">dĭ </ts>
               <ts e="T3240" id="Seg_1938" n="e" s="T3239">kuzanə. </ts>
               <ts e="T3241" id="Seg_1940" n="e" s="T3240">Dĭ </ts>
               <ts e="T3242" id="Seg_1942" n="e" s="T3241">dĭm </ts>
               <ts e="T3243" id="Seg_1944" n="e" s="T3242">tibinə </ts>
               <ts e="T3244" id="Seg_1946" n="e" s="T3243">ibi </ts>
               <ts e="T3245" id="Seg_1948" n="e" s="T3244">i </ts>
               <ts e="T3246" id="Seg_1950" n="e" s="T3245">kalla </ts>
               <ts e="T3247" id="Seg_1952" n="e" s="T3246">dʼürbi </ts>
               <ts e="T3248" id="Seg_1954" n="e" s="T3247">dĭzeŋgəʔ. </ts>
               <ts e="T3249" id="Seg_1956" n="e" s="T3248">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T2921" id="Seg_1957" s="T2918">PKZ_196X_Xavroshechka_flk.001 (001)</ta>
            <ta e="T2925" id="Seg_1958" s="T2921">PKZ_196X_Xavroshechka_flk.002 (002)</ta>
            <ta e="T2929" id="Seg_1959" s="T2925">PKZ_196X_Xavroshechka_flk.003 (003)</ta>
            <ta e="T2933" id="Seg_1960" s="T2929">PKZ_196X_Xavroshechka_flk.004 (004)</ta>
            <ta e="T2936" id="Seg_1961" s="T2933">PKZ_196X_Xavroshechka_flk.005 (005)</ta>
            <ta e="T2940" id="Seg_1962" s="T2936">PKZ_196X_Xavroshechka_flk.006 (006)</ta>
            <ta e="T2943" id="Seg_1963" s="T2940">PKZ_196X_Xavroshechka_flk.007 (007)</ta>
            <ta e="T2948" id="Seg_1964" s="T2943">PKZ_196X_Xavroshechka_flk.008 (008)</ta>
            <ta e="T2955" id="Seg_1965" s="T2948">PKZ_196X_Xavroshechka_flk.009 (009)</ta>
            <ta e="T2958" id="Seg_1966" s="T2955">PKZ_196X_Xavroshechka_flk.010 (010)</ta>
            <ta e="T2965" id="Seg_1967" s="T2958">PKZ_196X_Xavroshechka_flk.011 (011) </ta>
            <ta e="T2975" id="Seg_1968" s="T2965">PKZ_196X_Xavroshechka_flk.012 (013) </ta>
            <ta e="T2981" id="Seg_1969" s="T2975">PKZ_196X_Xavroshechka_flk.013 (015)</ta>
            <ta e="T2990" id="Seg_1970" s="T2981">PKZ_196X_Xavroshechka_flk.014 (016)</ta>
            <ta e="T2992" id="Seg_1971" s="T2990">PKZ_196X_Xavroshechka_flk.015 (017)</ta>
            <ta e="T2997" id="Seg_1972" s="T2992">PKZ_196X_Xavroshechka_flk.016 (018)</ta>
            <ta e="T3001" id="Seg_1973" s="T2997">PKZ_196X_Xavroshechka_flk.017 (019)</ta>
            <ta e="T3004" id="Seg_1974" s="T3001">PKZ_196X_Xavroshechka_flk.018 (020)</ta>
            <ta e="T3008" id="Seg_1975" s="T3004">PKZ_196X_Xavroshechka_flk.019 (021)</ta>
            <ta e="T3014" id="Seg_1976" s="T3008">PKZ_196X_Xavroshechka_flk.020 (022)</ta>
            <ta e="T3018" id="Seg_1977" s="T3014">PKZ_196X_Xavroshechka_flk.021 (023)</ta>
            <ta e="T3020" id="Seg_1978" s="T3018">PKZ_196X_Xavroshechka_flk.022 (024)</ta>
            <ta e="T3024" id="Seg_1979" s="T3020">PKZ_196X_Xavroshechka_flk.023 (025)</ta>
            <ta e="T3027" id="Seg_1980" s="T3024">PKZ_196X_Xavroshechka_flk.024 (026)</ta>
            <ta e="T3035" id="Seg_1981" s="T3027">PKZ_196X_Xavroshechka_flk.025 (027)</ta>
            <ta e="T3041" id="Seg_1982" s="T3035">PKZ_196X_Xavroshechka_flk.026 (028)</ta>
            <ta e="T3044" id="Seg_1983" s="T3041">PKZ_196X_Xavroshechka_flk.027 (029)</ta>
            <ta e="T3047" id="Seg_1984" s="T3044">PKZ_196X_Xavroshechka_flk.028 (030)</ta>
            <ta e="T3053" id="Seg_1985" s="T3047">PKZ_196X_Xavroshechka_flk.029 (031)</ta>
            <ta e="T3058" id="Seg_1986" s="T3053">PKZ_196X_Xavroshechka_flk.030 (032)</ta>
            <ta e="T3061" id="Seg_1987" s="T3058">PKZ_196X_Xavroshechka_flk.031 (033)</ta>
            <ta e="T3063" id="Seg_1988" s="T3061">PKZ_196X_Xavroshechka_flk.032 (034)</ta>
            <ta e="T3067" id="Seg_1989" s="T3063">PKZ_196X_Xavroshechka_flk.033 (035)</ta>
            <ta e="T3075" id="Seg_1990" s="T3067">PKZ_196X_Xavroshechka_flk.034 (036)</ta>
            <ta e="T3078" id="Seg_1991" s="T3075">PKZ_196X_Xavroshechka_flk.035 (037)</ta>
            <ta e="T3086" id="Seg_1992" s="T3078">PKZ_196X_Xavroshechka_flk.036 (038)</ta>
            <ta e="T3090" id="Seg_1993" s="T3086">PKZ_196X_Xavroshechka_flk.037 (039)</ta>
            <ta e="T3096" id="Seg_1994" s="T3090">PKZ_196X_Xavroshechka_flk.038 (040)</ta>
            <ta e="T3099" id="Seg_1995" s="T3096">PKZ_196X_Xavroshechka_flk.039 (041)</ta>
            <ta e="T3102" id="Seg_1996" s="T3099">PKZ_196X_Xavroshechka_flk.040 (042)</ta>
            <ta e="T3108" id="Seg_1997" s="T3102">PKZ_196X_Xavroshechka_flk.041 (043)</ta>
            <ta e="T3116" id="Seg_1998" s="T3108">PKZ_196X_Xavroshechka_flk.042 (044)</ta>
            <ta e="T3125" id="Seg_1999" s="T3116">PKZ_196X_Xavroshechka_flk.043 (045)</ta>
            <ta e="T3128" id="Seg_2000" s="T3125">PKZ_196X_Xavroshechka_flk.044 (046)</ta>
            <ta e="T3132" id="Seg_2001" s="T3128">PKZ_196X_Xavroshechka_flk.045 (047)</ta>
            <ta e="T3135" id="Seg_2002" s="T3132">PKZ_196X_Xavroshechka_flk.046 (049)</ta>
            <ta e="T3143" id="Seg_2003" s="T3135">PKZ_196X_Xavroshechka_flk.047 (050) </ta>
            <ta e="T3151" id="Seg_2004" s="T3143">PKZ_196X_Xavroshechka_flk.048 (052) </ta>
            <ta e="T3154" id="Seg_2005" s="T3151">PKZ_196X_Xavroshechka_flk.049 (054)</ta>
            <ta e="T3161" id="Seg_2006" s="T3154">PKZ_196X_Xavroshechka_flk.050 (055)</ta>
            <ta e="T3168" id="Seg_2007" s="T3161">PKZ_196X_Xavroshechka_flk.051 (056)</ta>
            <ta e="T3173" id="Seg_2008" s="T3168">PKZ_196X_Xavroshechka_flk.052 (057)</ta>
            <ta e="T3175" id="Seg_2009" s="T3173">PKZ_196X_Xavroshechka_flk.053 (058)</ta>
            <ta e="T3180" id="Seg_2010" s="T3175">PKZ_196X_Xavroshechka_flk.054 (059)</ta>
            <ta e="T3182" id="Seg_2011" s="T3180">PKZ_196X_Xavroshechka_flk.055 (060)</ta>
            <ta e="T3186" id="Seg_2012" s="T3182">PKZ_196X_Xavroshechka_flk.056 (061)</ta>
            <ta e="T3188" id="Seg_2013" s="T3186">PKZ_196X_Xavroshechka_flk.057 (062)</ta>
            <ta e="T3193" id="Seg_2014" s="T3188">PKZ_196X_Xavroshechka_flk.058 (063)</ta>
            <ta e="T3196" id="Seg_2015" s="T3193">PKZ_196X_Xavroshechka_flk.059 (064)</ta>
            <ta e="T3203" id="Seg_2016" s="T3196">PKZ_196X_Xavroshechka_flk.060 (065)</ta>
            <ta e="T3206" id="Seg_2017" s="T3203">PKZ_196X_Xavroshechka_flk.061 (066)</ta>
            <ta e="T3211" id="Seg_2018" s="T3206">PKZ_196X_Xavroshechka_flk.062 (067)</ta>
            <ta e="T3219" id="Seg_2019" s="T3211">PKZ_196X_Xavroshechka_flk.063 (068)</ta>
            <ta e="T3223" id="Seg_2020" s="T3219">PKZ_196X_Xavroshechka_flk.064 (069)</ta>
            <ta e="T3225" id="Seg_2021" s="T3223">PKZ_196X_Xavroshechka_flk.065 (070)</ta>
            <ta e="T3240" id="Seg_2022" s="T3225">PKZ_196X_Xavroshechka_flk.066 (071)</ta>
            <ta e="T3248" id="Seg_2023" s="T3240">PKZ_196X_Xavroshechka_flk.067 (072)</ta>
            <ta e="T3249" id="Seg_2024" s="T3248">PKZ_196X_Xavroshechka_flk.068 (073)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T2921" id="Seg_2025" s="T2918">Amnobi onʼiʔ koʔbdo. </ta>
            <ta e="T2925" id="Seg_2026" s="T2921">(Di-) Dĭm numəjleʔbəʔi Xavrošečka. </ta>
            <ta e="T2929" id="Seg_2027" s="T2925">Dĭn ijat, abat külaːmbiʔi. </ta>
            <ta e="T2933" id="Seg_2028" s="T2929">Il ej jakšəʔi bar. </ta>
            <ta e="T2936" id="Seg_2029" s="T2933">(Dĭʔnə=) Dĭm ibiʔi. </ta>
            <ta e="T2940" id="Seg_2030" s="T2936">Dĭn nagur koʔbdot ibiʔi. </ta>
            <ta e="T2943" id="Seg_2031" s="T2940">Ĭmbidə ej abiʔi. </ta>
            <ta e="T2948" id="Seg_2032" s="T2943">A dĭ Xavrošečka bar togonorbi. </ta>
            <ta e="T2955" id="Seg_2033" s="T2948">Bar ĭmbi togonoria, (ej=) dĭzeŋdə ej molia. </ta>
            <ta e="T2958" id="Seg_2034" s="T2955">Uge kudollaʔbə nüke. </ta>
            <ta e="T2965" id="Seg_2035" s="T2958">Dĭgəttə măndə: "Kanaʔ, măna aʔ iʔgö xolstə". </ta>
            <ta e="T2975" id="Seg_2036" s="T2965">Dĭ kambi tüžöjdə da bar dʼorlaʔbə: "(M-) Măna teŋ münörleʔbəʔjə. </ta>
            <ta e="T2981" id="Seg_2037" s="T2975">Dĭzeŋdə ĭmbidə asʼtə ej (m-) moliam". </ta>
            <ta e="T2990" id="Seg_2038" s="T2981">I tüžöj măndə:" Onʼiʔ kuʔdə pădaʔ, a onʼiʔtə supsoʔ! </ta>
            <ta e="T2992" id="Seg_2039" s="T2990">Bar moləj". </ta>
            <ta e="T2997" id="Seg_2040" s="T2992">Dĭgəttə (dĭn-) dĭʔnə (am-) abi. </ta>
            <ta e="T3001" id="Seg_2041" s="T2997">I ererbi, xolstə abi. </ta>
            <ta e="T3004" id="Seg_2042" s="T3001">(Dĭ dĭgəttə) deʔpi. </ta>
            <ta e="T3008" id="Seg_2043" s="T3004">Dĭgəttə bazoʔ iʔgö mobi. </ta>
            <ta e="T3014" id="Seg_2044" s="T3008">Dĭ bazoʔ kambi tüžöjdə, bazoʔ abi. </ta>
            <ta e="T3018" id="Seg_2045" s="T3014">Dĭgəttə măndə onʼiʔ koʔbdondə. </ta>
            <ta e="T3020" id="Seg_2046" s="T3018">Onʼiʔ sʼimandə. </ta>
            <ta e="T3024" id="Seg_2047" s="T3020">"Kanaʔ dĭziʔ"; dĭ kambi. </ta>
            <ta e="T3027" id="Seg_2048" s="T3024">Dĭn iʔbəbi kunolzittə. </ta>
            <ta e="T3035" id="Seg_2049" s="T3027">Dĭ bar:" Kunoldə, sima, kunoldə, sima"; simat kunolluʔpi. </ta>
            <ta e="T3041" id="Seg_2050" s="T3035">Dĭʔnə bazoʔ tüžöj ererluʔpi, dĭ ibi. </ta>
            <ta e="T3044" id="Seg_2051" s="T3041">Trubkaʔi xolstəʔi deʔpi. </ta>
            <ta e="T3047" id="Seg_2052" s="T3044">Dĭ nükenə mĭbi. </ta>
            <ta e="T3053" id="Seg_2053" s="T3047">Dĭgəttə dĭ nüke baška koʔbdobə öʔleʔbə. </ta>
            <ta e="T3058" id="Seg_2054" s="T3053">"Kanaʔ, ĭmbi dĭ dĭn alia?" </ta>
            <ta e="T3061" id="Seg_2055" s="T3058">Dĭ koʔbdo kambi. </ta>
            <ta e="T3063" id="Seg_2056" s="T3061">Dĭn iʔbəbi. </ta>
            <ta e="T3067" id="Seg_2057" s="T3063">Dĭ bar dĭm kunolluʔpi. </ta>
            <ta e="T3075" id="Seg_2058" s="T3067">"(Ku-) Kunolaʔ, sima, kunolaʔ, baška sima"; simazaŋdə kunolluʔpi. </ta>
            <ta e="T3078" id="Seg_2059" s="T3075">Tüžöj dĭʔnə abi. </ta>
            <ta e="T3086" id="Seg_2060" s="T3078">Xolstə ererbi i trubkaʔi mĭbi, dĭgəttə dĭ šobi. </ta>
            <ta e="T3090" id="Seg_2061" s="T3086">Dĭ iššo tăŋ kurolluʔpi. </ta>
            <ta e="T3096" id="Seg_2062" s="T3090">Dĭgəttə nagur koʔbdobə öʔlüʔbi, nagur simatsiʔ. </ta>
            <ta e="T3099" id="Seg_2063" s="T3096">Dĭ koʔbdo kambi. </ta>
            <ta e="T3102" id="Seg_2064" s="T3099">I iʔbəbi, kunollaʔbə. </ta>
            <ta e="T3108" id="Seg_2065" s="T3102">(Al-) Xavrošečka tože dĭm nüjnə nüjleʔbə. </ta>
            <ta e="T3116" id="Seg_2066" s="T3108">"Kunolaʔ, sima, kunolaʔ, šide"; a nagur simattə nöməllüʔpi. </ta>
            <ta e="T3125" id="Seg_2067" s="T3116">Dĭgəttə šide simat kunolluʔpiʔi, a onʼiʔ simat bar kubi. </ta>
            <ta e="T3128" id="Seg_2068" s="T3125">Dĭgəttə šobi maːʔndə. </ta>
            <ta e="T3132" id="Seg_2069" s="T3128">Măndə: "Tüžöj dĭʔnə alia". </ta>
            <ta e="T3135" id="Seg_2070" s="T3132">Dĭgəttə ertən uʔbdəbi. </ta>
            <ta e="T3143" id="Seg_2071" s="T3135">Dĭgəttə ertən uʔbdəbi, (büzʼen-) büzʼenə măndə: "Băttə tüžöjdə!" </ta>
            <ta e="T3151" id="Seg_2072" s="T3143">A dĭ măndə: Na što băʔsittə, dĭ jakšə tüžöj. </ta>
            <ta e="T3154" id="Seg_2073" s="T3151">Iʔgö sut mĭlie". </ta>
            <ta e="T3161" id="Seg_2074" s="T3154">A dĭ koʔbdo nuʔməluʔpi:" Tănan băʔsittə xočet". </ta>
            <ta e="T3168" id="Seg_2075" s="T3161">Dĭ măndə:" Iʔ amaʔ măn (um-) ujam. </ta>
            <ta e="T3173" id="Seg_2076" s="T3168">A lezeŋbə it i kanaʔ! </ta>
            <ta e="T3175" id="Seg_2077" s="T3173">Dʼünə tĭləʔ. </ta>
            <ta e="T3180" id="Seg_2078" s="T3175">Dĭgəttə dĭ ej ambi uja. </ta>
            <ta e="T3182" id="Seg_2079" s="T3180">Lezeŋdə (oʔbdəʔ)". </ta>
            <ta e="T3186" id="Seg_2080" s="T3182">Dĭgəttə büzʼet dĭm bătluʔpi. </ta>
            <ta e="T3188" id="Seg_2081" s="T3186">Ujazaŋdə ambiʔi. </ta>
            <ta e="T3193" id="Seg_2082" s="T3188">A dĭ Xavrošečka lezeŋdə oʔbdobiʔi. </ta>
            <ta e="T3196" id="Seg_2083" s="T3193">Kambi, tĭlbi dʼünə. </ta>
            <ta e="T3203" id="Seg_2084" s="T3196">Dĭgəttə özerleʔbəʔi jabloki, kuvasəʔi, šində ej šolia. </ta>
            <ta e="T3206" id="Seg_2085" s="T3203">Nulaʔbə da kulaʔbə. </ta>
            <ta e="T3211" id="Seg_2086" s="T3206">Dĭgəttə (so-) šonəga koŋ kuza. </ta>
            <ta e="T3219" id="Seg_2087" s="T3211">"Šində măna mĭləj dĭ jabločka, dĭm tibinə ilem. </ta>
            <ta e="T3223" id="Seg_2088" s="T3219">Dĭzeŋ ej mobiʔi izittə. </ta>
            <ta e="T3225" id="Seg_2089" s="T3223">Nʼuʔdə kandəgaʔi. </ta>
            <ta e="T3240" id="Seg_2090" s="T3225">Dĭgəttə dĭ Xavroša šobi, (dĭ= dĭ=) dĭʔnə (na-) nagnulisʼ, dĭ jabləkə mĭbi (dĭ=) dĭ kuzanə. </ta>
            <ta e="T3248" id="Seg_2091" s="T3240">Dĭ dĭm tibinə ibi i kalla dʼürbi dĭzeŋgəʔ. </ta>
            <ta e="T3249" id="Seg_2092" s="T3248">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2919" id="Seg_2093" s="T2918">amno-bi</ta>
            <ta e="T2920" id="Seg_2094" s="T2919">onʼiʔ</ta>
            <ta e="T2921" id="Seg_2095" s="T2920">koʔbdo</ta>
            <ta e="T2923" id="Seg_2096" s="T2922">dĭ-m</ta>
            <ta e="T2924" id="Seg_2097" s="T2923">numəj-leʔbə-ʔi</ta>
            <ta e="T2925" id="Seg_2098" s="T2924">Xavrošečka</ta>
            <ta e="T2926" id="Seg_2099" s="T2925">dĭ-n</ta>
            <ta e="T2927" id="Seg_2100" s="T2926">ija-t</ta>
            <ta e="T2928" id="Seg_2101" s="T2927">aba-t</ta>
            <ta e="T2929" id="Seg_2102" s="T2928">kü-laːm-bi-ʔi</ta>
            <ta e="T2930" id="Seg_2103" s="T2929">il</ta>
            <ta e="T2931" id="Seg_2104" s="T2930">ej</ta>
            <ta e="T2932" id="Seg_2105" s="T2931">jakšə-ʔi</ta>
            <ta e="T2933" id="Seg_2106" s="T2932">bar</ta>
            <ta e="T2934" id="Seg_2107" s="T2933">dĭʔ-nə</ta>
            <ta e="T2935" id="Seg_2108" s="T2934">dĭ-m</ta>
            <ta e="T2936" id="Seg_2109" s="T2935">i-bi-ʔi</ta>
            <ta e="T2937" id="Seg_2110" s="T2936">dĭ-n</ta>
            <ta e="T2938" id="Seg_2111" s="T2937">nagur</ta>
            <ta e="T2939" id="Seg_2112" s="T2938">koʔbdo-t</ta>
            <ta e="T2940" id="Seg_2113" s="T2939">i-bi-ʔi</ta>
            <ta e="T2941" id="Seg_2114" s="T2940">ĭmbi=də</ta>
            <ta e="T2942" id="Seg_2115" s="T2941">ej</ta>
            <ta e="T2943" id="Seg_2116" s="T2942">a-bi-ʔi</ta>
            <ta e="T2944" id="Seg_2117" s="T2943">a</ta>
            <ta e="T2945" id="Seg_2118" s="T2944">dĭ</ta>
            <ta e="T2946" id="Seg_2119" s="T2945">Xavrošečka</ta>
            <ta e="T2947" id="Seg_2120" s="T2946">bar</ta>
            <ta e="T2948" id="Seg_2121" s="T2947">togonor-bi</ta>
            <ta e="T2949" id="Seg_2122" s="T2948">bar</ta>
            <ta e="T2950" id="Seg_2123" s="T2949">ĭmbi</ta>
            <ta e="T2951" id="Seg_2124" s="T2950">togonor-ia</ta>
            <ta e="T2952" id="Seg_2125" s="T2951">ej</ta>
            <ta e="T2953" id="Seg_2126" s="T2952">dĭ-zeŋ-də</ta>
            <ta e="T2954" id="Seg_2127" s="T2953">ej</ta>
            <ta e="T2955" id="Seg_2128" s="T2954">mo-lia</ta>
            <ta e="T2956" id="Seg_2129" s="T2955">uge</ta>
            <ta e="T2957" id="Seg_2130" s="T2956">kudo-l-laʔbə</ta>
            <ta e="T2958" id="Seg_2131" s="T2957">nüke</ta>
            <ta e="T2959" id="Seg_2132" s="T2958">dĭgəttə</ta>
            <ta e="T2960" id="Seg_2133" s="T2959">măn-də</ta>
            <ta e="T2961" id="Seg_2134" s="T2960">kan-a-ʔ</ta>
            <ta e="T2962" id="Seg_2135" s="T2961">măna</ta>
            <ta e="T2963" id="Seg_2136" s="T2962">a-ʔ</ta>
            <ta e="T2964" id="Seg_2137" s="T2963">iʔgö</ta>
            <ta e="T2965" id="Seg_2138" s="T2964">xolstə</ta>
            <ta e="T2966" id="Seg_2139" s="T2965">dĭ</ta>
            <ta e="T2967" id="Seg_2140" s="T2966">kam-bi</ta>
            <ta e="T2968" id="Seg_2141" s="T2967">tüžöj-də</ta>
            <ta e="T2969" id="Seg_2142" s="T2968">da</ta>
            <ta e="T2970" id="Seg_2143" s="T2969">bar</ta>
            <ta e="T2971" id="Seg_2144" s="T2970">dʼor-laʔbə</ta>
            <ta e="T2973" id="Seg_2145" s="T2972">măna</ta>
            <ta e="T2974" id="Seg_2146" s="T2973">teŋ</ta>
            <ta e="T2975" id="Seg_2147" s="T2974">münör-leʔbə-ʔjə</ta>
            <ta e="T2976" id="Seg_2148" s="T2975">dĭ-zeŋ-də</ta>
            <ta e="T2977" id="Seg_2149" s="T2976">ĭmbi=də</ta>
            <ta e="T2978" id="Seg_2150" s="T2977">a-sʼtə</ta>
            <ta e="T2979" id="Seg_2151" s="T2978">ej</ta>
            <ta e="T2981" id="Seg_2152" s="T2980">mo-lia-m</ta>
            <ta e="T2982" id="Seg_2153" s="T2981">i</ta>
            <ta e="T2983" id="Seg_2154" s="T2982">tüžöj</ta>
            <ta e="T2984" id="Seg_2155" s="T2983">măn-də</ta>
            <ta e="T2985" id="Seg_2156" s="T2984">onʼiʔ</ta>
            <ta e="T2986" id="Seg_2157" s="T2985">kuʔ-də</ta>
            <ta e="T2987" id="Seg_2158" s="T2986">păda-ʔ</ta>
            <ta e="T2988" id="Seg_2159" s="T2987">a</ta>
            <ta e="T2989" id="Seg_2160" s="T2988">onʼiʔ-tə</ta>
            <ta e="T2990" id="Seg_2161" s="T2989">supso-ʔ</ta>
            <ta e="T2991" id="Seg_2162" s="T2990">bar</ta>
            <ta e="T2992" id="Seg_2163" s="T2991">mo-lə-j</ta>
            <ta e="T2993" id="Seg_2164" s="T2992">dĭgəttə</ta>
            <ta e="T2995" id="Seg_2165" s="T2994">dĭʔ-nə</ta>
            <ta e="T2997" id="Seg_2166" s="T2996">a-bi</ta>
            <ta e="T2998" id="Seg_2167" s="T2997">i</ta>
            <ta e="T2999" id="Seg_2168" s="T2998">erer-bi</ta>
            <ta e="T3000" id="Seg_2169" s="T2999">xolstə</ta>
            <ta e="T3001" id="Seg_2170" s="T3000">a-bi</ta>
            <ta e="T3002" id="Seg_2171" s="T3001">dĭ</ta>
            <ta e="T3003" id="Seg_2172" s="T3002">dĭgəttə</ta>
            <ta e="T3004" id="Seg_2173" s="T3003">deʔ-pi</ta>
            <ta e="T3005" id="Seg_2174" s="T3004">dĭgəttə</ta>
            <ta e="T3006" id="Seg_2175" s="T3005">bazoʔ</ta>
            <ta e="T3007" id="Seg_2176" s="T3006">iʔgö</ta>
            <ta e="T3008" id="Seg_2177" s="T3007">mo-bi</ta>
            <ta e="T3009" id="Seg_2178" s="T3008">dĭ</ta>
            <ta e="T3010" id="Seg_2179" s="T3009">bazoʔ</ta>
            <ta e="T3011" id="Seg_2180" s="T3010">kam-bi</ta>
            <ta e="T3012" id="Seg_2181" s="T3011">tüžöj-də</ta>
            <ta e="T3013" id="Seg_2182" s="T3012">bazoʔ</ta>
            <ta e="T3014" id="Seg_2183" s="T3013">a-bi</ta>
            <ta e="T3015" id="Seg_2184" s="T3014">dĭgəttə</ta>
            <ta e="T3016" id="Seg_2185" s="T3015">măn-də</ta>
            <ta e="T3017" id="Seg_2186" s="T3016">onʼiʔ</ta>
            <ta e="T3018" id="Seg_2187" s="T3017">koʔbdo-ndə</ta>
            <ta e="T3019" id="Seg_2188" s="T3018">onʼiʔ</ta>
            <ta e="T3020" id="Seg_2189" s="T3019">sʼima-ndə</ta>
            <ta e="T3021" id="Seg_2190" s="T3020">kan-a-ʔ</ta>
            <ta e="T3022" id="Seg_2191" s="T3021">dĭ-ziʔ</ta>
            <ta e="T3023" id="Seg_2192" s="T3022">dĭ</ta>
            <ta e="T3024" id="Seg_2193" s="T3023">kam-bi</ta>
            <ta e="T3025" id="Seg_2194" s="T3024">dĭn</ta>
            <ta e="T3026" id="Seg_2195" s="T3025">iʔbə-bi</ta>
            <ta e="T3027" id="Seg_2196" s="T3026">kunol-zittə</ta>
            <ta e="T3028" id="Seg_2197" s="T3027">dĭ</ta>
            <ta e="T3029" id="Seg_2198" s="T3028">bar</ta>
            <ta e="T3030" id="Seg_2199" s="T3029">kunol-də</ta>
            <ta e="T3031" id="Seg_2200" s="T3030">sima</ta>
            <ta e="T3032" id="Seg_2201" s="T3031">kunol-də</ta>
            <ta e="T3033" id="Seg_2202" s="T3032">sima</ta>
            <ta e="T3034" id="Seg_2203" s="T3033">sima-t</ta>
            <ta e="T3035" id="Seg_2204" s="T3034">kunol-luʔ-pi</ta>
            <ta e="T3036" id="Seg_2205" s="T3035">dĭʔ-nə</ta>
            <ta e="T3037" id="Seg_2206" s="T3036">bazoʔ</ta>
            <ta e="T3038" id="Seg_2207" s="T3037">tüžöj</ta>
            <ta e="T3039" id="Seg_2208" s="T3038">erer-luʔ-pi</ta>
            <ta e="T3040" id="Seg_2209" s="T3039">dĭ</ta>
            <ta e="T3041" id="Seg_2210" s="T3040">i-bi</ta>
            <ta e="T3042" id="Seg_2211" s="T3041">trubka-ʔi</ta>
            <ta e="T3043" id="Seg_2212" s="T3042">xolstə-ʔi</ta>
            <ta e="T3044" id="Seg_2213" s="T3043">deʔ-pi</ta>
            <ta e="T3045" id="Seg_2214" s="T3044">dĭ</ta>
            <ta e="T3046" id="Seg_2215" s="T3045">nüke-nə</ta>
            <ta e="T3047" id="Seg_2216" s="T3046">mĭ-bi</ta>
            <ta e="T3048" id="Seg_2217" s="T3047">dĭgəttə</ta>
            <ta e="T3049" id="Seg_2218" s="T3048">dĭ</ta>
            <ta e="T3050" id="Seg_2219" s="T3049">nüke</ta>
            <ta e="T3051" id="Seg_2220" s="T3050">baška</ta>
            <ta e="T3052" id="Seg_2221" s="T3051">koʔbdo-bə</ta>
            <ta e="T3053" id="Seg_2222" s="T3052">öʔ-leʔbə</ta>
            <ta e="T3054" id="Seg_2223" s="T3053">kan-a-ʔ</ta>
            <ta e="T3055" id="Seg_2224" s="T3054">ĭmbi</ta>
            <ta e="T3056" id="Seg_2225" s="T3055">dĭ</ta>
            <ta e="T3057" id="Seg_2226" s="T3056">dĭn</ta>
            <ta e="T3058" id="Seg_2227" s="T3057">a-lia</ta>
            <ta e="T3059" id="Seg_2228" s="T3058">dĭ</ta>
            <ta e="T3060" id="Seg_2229" s="T3059">koʔbdo</ta>
            <ta e="T3061" id="Seg_2230" s="T3060">kam-bi</ta>
            <ta e="T3062" id="Seg_2231" s="T3061">dĭn</ta>
            <ta e="T3063" id="Seg_2232" s="T3062">iʔbə-bi</ta>
            <ta e="T3064" id="Seg_2233" s="T3063">dĭ</ta>
            <ta e="T3065" id="Seg_2234" s="T3064">bar</ta>
            <ta e="T3066" id="Seg_2235" s="T3065">dĭ-m</ta>
            <ta e="T3067" id="Seg_2236" s="T3066">kunol-luʔ-pi</ta>
            <ta e="T3069" id="Seg_2237" s="T3068">kunol-a-ʔ</ta>
            <ta e="T3070" id="Seg_2238" s="T3069">sima</ta>
            <ta e="T3071" id="Seg_2239" s="T3070">kunol-a-ʔ</ta>
            <ta e="T3072" id="Seg_2240" s="T3071">baška</ta>
            <ta e="T3073" id="Seg_2241" s="T3072">sima</ta>
            <ta e="T3074" id="Seg_2242" s="T3073">sima-zaŋ-də</ta>
            <ta e="T3075" id="Seg_2243" s="T3074">kunol-luʔ-pi</ta>
            <ta e="T3076" id="Seg_2244" s="T3075">tüžöj</ta>
            <ta e="T3077" id="Seg_2245" s="T3076">dĭʔ-nə</ta>
            <ta e="T3078" id="Seg_2246" s="T3077">a-bi</ta>
            <ta e="T3079" id="Seg_2247" s="T3078">xolstə</ta>
            <ta e="T3080" id="Seg_2248" s="T3079">erer-bi</ta>
            <ta e="T3081" id="Seg_2249" s="T3080">i</ta>
            <ta e="T3082" id="Seg_2250" s="T3081">trubka-ʔi</ta>
            <ta e="T3083" id="Seg_2251" s="T3082">mĭ-bi</ta>
            <ta e="T3084" id="Seg_2252" s="T3083">dĭgəttə</ta>
            <ta e="T3085" id="Seg_2253" s="T3084">dĭ</ta>
            <ta e="T3086" id="Seg_2254" s="T3085">šo-bi</ta>
            <ta e="T3087" id="Seg_2255" s="T3086">dĭ</ta>
            <ta e="T3088" id="Seg_2256" s="T3087">iššo</ta>
            <ta e="T3089" id="Seg_2257" s="T3088">tăŋ</ta>
            <ta e="T3090" id="Seg_2258" s="T3089">kuro-l-luʔ-pi</ta>
            <ta e="T3091" id="Seg_2259" s="T3090">dĭgəttə</ta>
            <ta e="T3092" id="Seg_2260" s="T3091">nagur</ta>
            <ta e="T3093" id="Seg_2261" s="T3092">koʔbdo-bə</ta>
            <ta e="T3094" id="Seg_2262" s="T3093">öʔ-lüʔ-bi</ta>
            <ta e="T3095" id="Seg_2263" s="T3094">nagur</ta>
            <ta e="T3096" id="Seg_2264" s="T3095">sima-t-siʔ</ta>
            <ta e="T3097" id="Seg_2265" s="T3096">dĭ</ta>
            <ta e="T3098" id="Seg_2266" s="T3097">koʔbdo</ta>
            <ta e="T3099" id="Seg_2267" s="T3098">kam-bi</ta>
            <ta e="T3100" id="Seg_2268" s="T3099">i</ta>
            <ta e="T3101" id="Seg_2269" s="T3100">iʔbə-bi</ta>
            <ta e="T3102" id="Seg_2270" s="T3101">kunol-laʔbə</ta>
            <ta e="T3104" id="Seg_2271" s="T3103">Xavrošečka</ta>
            <ta e="T3105" id="Seg_2272" s="T3104">tože</ta>
            <ta e="T3106" id="Seg_2273" s="T3105">dĭ-m</ta>
            <ta e="T3107" id="Seg_2274" s="T3106">nüjnə</ta>
            <ta e="T3108" id="Seg_2275" s="T3107">nüj-leʔbə</ta>
            <ta e="T3109" id="Seg_2276" s="T3108">kunol-a-ʔ</ta>
            <ta e="T3110" id="Seg_2277" s="T3109">sima</ta>
            <ta e="T3111" id="Seg_2278" s="T3110">kunol-a-ʔ</ta>
            <ta e="T3112" id="Seg_2279" s="T3111">šide</ta>
            <ta e="T3113" id="Seg_2280" s="T3112">a</ta>
            <ta e="T3114" id="Seg_2281" s="T3113">nagur</ta>
            <ta e="T3115" id="Seg_2282" s="T3114">sima-ttə</ta>
            <ta e="T3116" id="Seg_2283" s="T3115">nöməl-lüʔ-pi</ta>
            <ta e="T3117" id="Seg_2284" s="T3116">dĭgəttə</ta>
            <ta e="T3118" id="Seg_2285" s="T3117">šide</ta>
            <ta e="T3119" id="Seg_2286" s="T3118">sima-t</ta>
            <ta e="T3120" id="Seg_2287" s="T3119">kunol-luʔ-pi-ʔi</ta>
            <ta e="T3121" id="Seg_2288" s="T3120">a</ta>
            <ta e="T3122" id="Seg_2289" s="T3121">onʼiʔ</ta>
            <ta e="T3123" id="Seg_2290" s="T3122">sima-t</ta>
            <ta e="T3124" id="Seg_2291" s="T3123">bar</ta>
            <ta e="T3125" id="Seg_2292" s="T3124">ku-bi</ta>
            <ta e="T3126" id="Seg_2293" s="T3125">dĭgəttə</ta>
            <ta e="T3127" id="Seg_2294" s="T3126">šo-bi</ta>
            <ta e="T3128" id="Seg_2295" s="T3127">maːʔ-ndə</ta>
            <ta e="T3129" id="Seg_2296" s="T3128">măn-də</ta>
            <ta e="T3130" id="Seg_2297" s="T3129">tüžöj</ta>
            <ta e="T3131" id="Seg_2298" s="T3130">dĭʔ-nə</ta>
            <ta e="T3132" id="Seg_2299" s="T3131">a-lia</ta>
            <ta e="T3133" id="Seg_2300" s="T3132">dĭgəttə</ta>
            <ta e="T3134" id="Seg_2301" s="T3133">ertə-n</ta>
            <ta e="T3135" id="Seg_2302" s="T3134">uʔbdə-bi</ta>
            <ta e="T3136" id="Seg_2303" s="T3135">dĭgəttə</ta>
            <ta e="T3137" id="Seg_2304" s="T3136">ertə-n</ta>
            <ta e="T3138" id="Seg_2305" s="T3137">uʔbdə-bi</ta>
            <ta e="T3140" id="Seg_2306" s="T3139">büzʼe-nə</ta>
            <ta e="T3141" id="Seg_2307" s="T3140">măn-də</ta>
            <ta e="T3142" id="Seg_2308" s="T3141">băt-tə</ta>
            <ta e="T3143" id="Seg_2309" s="T3142">tüžöj-də</ta>
            <ta e="T3144" id="Seg_2310" s="T3143">a</ta>
            <ta e="T3145" id="Seg_2311" s="T3144">dĭ</ta>
            <ta e="T3146" id="Seg_2312" s="T3145">măn-də</ta>
            <ta e="T3147" id="Seg_2313" s="T3146">našto</ta>
            <ta e="T3148" id="Seg_2314" s="T3147">băʔ-sittə</ta>
            <ta e="T3149" id="Seg_2315" s="T3148">dĭ</ta>
            <ta e="T3150" id="Seg_2316" s="T3149">jakšə</ta>
            <ta e="T3151" id="Seg_2317" s="T3150">tüžöj</ta>
            <ta e="T3152" id="Seg_2318" s="T3151">iʔgö</ta>
            <ta e="T3153" id="Seg_2319" s="T3152">sut</ta>
            <ta e="T3154" id="Seg_2320" s="T3153">mĭ-lie</ta>
            <ta e="T3155" id="Seg_2321" s="T3154">a</ta>
            <ta e="T3156" id="Seg_2322" s="T3155">dĭ</ta>
            <ta e="T3157" id="Seg_2323" s="T3156">koʔbdo</ta>
            <ta e="T3158" id="Seg_2324" s="T3157">nuʔmə-luʔ-pi</ta>
            <ta e="T3159" id="Seg_2325" s="T3158">tănan</ta>
            <ta e="T3160" id="Seg_2326" s="T3159">băʔ-sittə</ta>
            <ta e="T3161" id="Seg_2327" s="T3160">xočet</ta>
            <ta e="T3162" id="Seg_2328" s="T3161">dĭ</ta>
            <ta e="T3163" id="Seg_2329" s="T3162">măn-də</ta>
            <ta e="T3164" id="Seg_2330" s="T3163">i-ʔ</ta>
            <ta e="T3165" id="Seg_2331" s="T3164">am-a-ʔ</ta>
            <ta e="T3166" id="Seg_2332" s="T3165">măn</ta>
            <ta e="T3168" id="Seg_2333" s="T3167">uja-m</ta>
            <ta e="T3169" id="Seg_2334" s="T3168">a</ta>
            <ta e="T3170" id="Seg_2335" s="T3169">le-zeŋ-bə</ta>
            <ta e="T3171" id="Seg_2336" s="T3170">i-t</ta>
            <ta e="T3172" id="Seg_2337" s="T3171">i</ta>
            <ta e="T3173" id="Seg_2338" s="T3172">kan-a-ʔ</ta>
            <ta e="T3174" id="Seg_2339" s="T3173">dʼü-nə</ta>
            <ta e="T3175" id="Seg_2340" s="T3174">tĭl-ə-ʔ</ta>
            <ta e="T3176" id="Seg_2341" s="T3175">dĭgəttə</ta>
            <ta e="T3177" id="Seg_2342" s="T3176">dĭ</ta>
            <ta e="T3178" id="Seg_2343" s="T3177">ej</ta>
            <ta e="T3179" id="Seg_2344" s="T3178">am-bi</ta>
            <ta e="T3180" id="Seg_2345" s="T3179">uja</ta>
            <ta e="T3181" id="Seg_2346" s="T3180">le-zeŋ-də</ta>
            <ta e="T3182" id="Seg_2347" s="T3181">oʔbdə-ʔ</ta>
            <ta e="T3183" id="Seg_2348" s="T3182">dĭgəttə</ta>
            <ta e="T3184" id="Seg_2349" s="T3183">büzʼe-t</ta>
            <ta e="T3185" id="Seg_2350" s="T3184">dĭ-m</ta>
            <ta e="T3186" id="Seg_2351" s="T3185">băt-luʔ-pi</ta>
            <ta e="T3187" id="Seg_2352" s="T3186">uja-zaŋ-də</ta>
            <ta e="T3188" id="Seg_2353" s="T3187">am-bi-ʔi</ta>
            <ta e="T3189" id="Seg_2354" s="T3188">a</ta>
            <ta e="T3190" id="Seg_2355" s="T3189">dĭ</ta>
            <ta e="T3191" id="Seg_2356" s="T3190">Xavrošečka</ta>
            <ta e="T3192" id="Seg_2357" s="T3191">le-zeŋ-də</ta>
            <ta e="T3193" id="Seg_2358" s="T3192">oʔbdo-bi-ʔi</ta>
            <ta e="T3194" id="Seg_2359" s="T3193">kam-bi</ta>
            <ta e="T3195" id="Seg_2360" s="T3194">tĭl-bi</ta>
            <ta e="T3196" id="Seg_2361" s="T3195">dʼü-nə</ta>
            <ta e="T3197" id="Seg_2362" s="T3196">dĭgəttə</ta>
            <ta e="T3198" id="Seg_2363" s="T3197">özer-leʔbə-ʔi</ta>
            <ta e="T3200" id="Seg_2364" s="T3199">kuvas-əʔi</ta>
            <ta e="T3201" id="Seg_2365" s="T3200">šində</ta>
            <ta e="T3202" id="Seg_2366" s="T3201">ej</ta>
            <ta e="T3203" id="Seg_2367" s="T3202">šo-lia</ta>
            <ta e="T3204" id="Seg_2368" s="T3203">nu-laʔbə</ta>
            <ta e="T3205" id="Seg_2369" s="T3204">da</ta>
            <ta e="T3206" id="Seg_2370" s="T3205">ku-laʔbə</ta>
            <ta e="T3207" id="Seg_2371" s="T3206">dĭgəttə</ta>
            <ta e="T3209" id="Seg_2372" s="T3208">šonə-ga</ta>
            <ta e="T3210" id="Seg_2373" s="T3209">koŋ</ta>
            <ta e="T3211" id="Seg_2374" s="T3210">kuza</ta>
            <ta e="T3212" id="Seg_2375" s="T3211">šində</ta>
            <ta e="T3213" id="Seg_2376" s="T3212">măna</ta>
            <ta e="T3214" id="Seg_2377" s="T3213">mĭ-lə-j</ta>
            <ta e="T3215" id="Seg_2378" s="T3214">dĭ</ta>
            <ta e="T3217" id="Seg_2379" s="T3216">dĭ-m</ta>
            <ta e="T3218" id="Seg_2380" s="T3217">tibi-nə</ta>
            <ta e="T3219" id="Seg_2381" s="T3218">i-le-m</ta>
            <ta e="T3220" id="Seg_2382" s="T3219">dĭ-zeŋ</ta>
            <ta e="T3221" id="Seg_2383" s="T3220">ej</ta>
            <ta e="T3222" id="Seg_2384" s="T3221">mo-bi-ʔi</ta>
            <ta e="T3223" id="Seg_2385" s="T3222">i-zittə</ta>
            <ta e="T3224" id="Seg_2386" s="T3223">nʼuʔdə</ta>
            <ta e="T3225" id="Seg_2387" s="T3224">kan-də-ga-ʔi</ta>
            <ta e="T3226" id="Seg_2388" s="T3225">dĭgəttə</ta>
            <ta e="T3227" id="Seg_2389" s="T3226">dĭ</ta>
            <ta e="T3228" id="Seg_2390" s="T3227">Xavroša</ta>
            <ta e="T3229" id="Seg_2391" s="T3228">šo-bi</ta>
            <ta e="T3230" id="Seg_2392" s="T3229">dĭ</ta>
            <ta e="T3231" id="Seg_2393" s="T3230">dĭ</ta>
            <ta e="T3232" id="Seg_2394" s="T3231">dĭʔ-nə</ta>
            <ta e="T3235" id="Seg_2395" s="T3234">dĭ</ta>
            <ta e="T3237" id="Seg_2396" s="T3236">mĭ-bi</ta>
            <ta e="T3238" id="Seg_2397" s="T3237">dĭ</ta>
            <ta e="T3239" id="Seg_2398" s="T3238">dĭ</ta>
            <ta e="T3240" id="Seg_2399" s="T3239">kuza-nə</ta>
            <ta e="T3241" id="Seg_2400" s="T3240">dĭ</ta>
            <ta e="T3242" id="Seg_2401" s="T3241">dĭ-m</ta>
            <ta e="T3243" id="Seg_2402" s="T3242">tibi-nə</ta>
            <ta e="T3244" id="Seg_2403" s="T3243">i-bi</ta>
            <ta e="T3245" id="Seg_2404" s="T3244">i</ta>
            <ta e="T3246" id="Seg_2405" s="T3245">kal-la</ta>
            <ta e="T3247" id="Seg_2406" s="T3246">dʼür-bi</ta>
            <ta e="T3248" id="Seg_2407" s="T3247">dĭ-zeŋ-gəʔ</ta>
            <ta e="T3249" id="Seg_2408" s="T3248">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2919" id="Seg_2409" s="T2918">amno-bi</ta>
            <ta e="T2920" id="Seg_2410" s="T2919">onʼiʔ</ta>
            <ta e="T2921" id="Seg_2411" s="T2920">koʔbdo</ta>
            <ta e="T2923" id="Seg_2412" s="T2922">dĭ-m</ta>
            <ta e="T2924" id="Seg_2413" s="T2923">numəj-laʔbə-jəʔ</ta>
            <ta e="T2925" id="Seg_2414" s="T2924">Xavrošečka</ta>
            <ta e="T2926" id="Seg_2415" s="T2925">dĭ-n</ta>
            <ta e="T2927" id="Seg_2416" s="T2926">ija-t</ta>
            <ta e="T2928" id="Seg_2417" s="T2927">aba-t</ta>
            <ta e="T2929" id="Seg_2418" s="T2928">kü-laːm-bi-jəʔ</ta>
            <ta e="T2930" id="Seg_2419" s="T2929">il</ta>
            <ta e="T2931" id="Seg_2420" s="T2930">ej</ta>
            <ta e="T2932" id="Seg_2421" s="T2931">jakšə-jəʔ</ta>
            <ta e="T2933" id="Seg_2422" s="T2932">bar</ta>
            <ta e="T2934" id="Seg_2423" s="T2933">dĭ-Tə</ta>
            <ta e="T2935" id="Seg_2424" s="T2934">dĭ-m</ta>
            <ta e="T2936" id="Seg_2425" s="T2935">i-bi-jəʔ</ta>
            <ta e="T2937" id="Seg_2426" s="T2936">dĭ-n</ta>
            <ta e="T2938" id="Seg_2427" s="T2937">nagur</ta>
            <ta e="T2939" id="Seg_2428" s="T2938">koʔbdo-t</ta>
            <ta e="T2940" id="Seg_2429" s="T2939">i-bi-jəʔ</ta>
            <ta e="T2941" id="Seg_2430" s="T2940">ĭmbi=də</ta>
            <ta e="T2942" id="Seg_2431" s="T2941">ej</ta>
            <ta e="T2943" id="Seg_2432" s="T2942">a-bi-jəʔ</ta>
            <ta e="T2944" id="Seg_2433" s="T2943">a</ta>
            <ta e="T2945" id="Seg_2434" s="T2944">dĭ</ta>
            <ta e="T2946" id="Seg_2435" s="T2945">Xavrošečka</ta>
            <ta e="T2947" id="Seg_2436" s="T2946">bar</ta>
            <ta e="T2948" id="Seg_2437" s="T2947">togonər-bi</ta>
            <ta e="T2949" id="Seg_2438" s="T2948">bar</ta>
            <ta e="T2950" id="Seg_2439" s="T2949">ĭmbi</ta>
            <ta e="T2951" id="Seg_2440" s="T2950">togonər-liA</ta>
            <ta e="T2952" id="Seg_2441" s="T2951">ej</ta>
            <ta e="T2953" id="Seg_2442" s="T2952">dĭ-zAŋ-də</ta>
            <ta e="T2954" id="Seg_2443" s="T2953">ej</ta>
            <ta e="T2955" id="Seg_2444" s="T2954">mo-liA</ta>
            <ta e="T2956" id="Seg_2445" s="T2955">üge</ta>
            <ta e="T2957" id="Seg_2446" s="T2956">kudo-l-laʔbə</ta>
            <ta e="T2958" id="Seg_2447" s="T2957">nüke</ta>
            <ta e="T2959" id="Seg_2448" s="T2958">dĭgəttə</ta>
            <ta e="T2960" id="Seg_2449" s="T2959">măn-ntə</ta>
            <ta e="T2961" id="Seg_2450" s="T2960">kan-ə-ʔ</ta>
            <ta e="T2962" id="Seg_2451" s="T2961">măna</ta>
            <ta e="T2963" id="Seg_2452" s="T2962">a-ʔ</ta>
            <ta e="T2964" id="Seg_2453" s="T2963">iʔgö</ta>
            <ta e="T2965" id="Seg_2454" s="T2964">xolstə</ta>
            <ta e="T2966" id="Seg_2455" s="T2965">dĭ</ta>
            <ta e="T2967" id="Seg_2456" s="T2966">kan-bi</ta>
            <ta e="T2968" id="Seg_2457" s="T2967">tüžöj-Tə</ta>
            <ta e="T2969" id="Seg_2458" s="T2968">da</ta>
            <ta e="T2970" id="Seg_2459" s="T2969">bar</ta>
            <ta e="T2971" id="Seg_2460" s="T2970">tʼor-laʔbə</ta>
            <ta e="T2973" id="Seg_2461" s="T2972">măna</ta>
            <ta e="T2974" id="Seg_2462" s="T2973">toŋ</ta>
            <ta e="T2975" id="Seg_2463" s="T2974">münör-laʔbə-jəʔ</ta>
            <ta e="T2976" id="Seg_2464" s="T2975">dĭ-zAŋ-Tə</ta>
            <ta e="T2977" id="Seg_2465" s="T2976">ĭmbi=də</ta>
            <ta e="T2978" id="Seg_2466" s="T2977">a-zittə</ta>
            <ta e="T2979" id="Seg_2467" s="T2978">ej</ta>
            <ta e="T2981" id="Seg_2468" s="T2980">mo-liA-m</ta>
            <ta e="T2982" id="Seg_2469" s="T2981">i</ta>
            <ta e="T2983" id="Seg_2470" s="T2982">tüžöj</ta>
            <ta e="T2984" id="Seg_2471" s="T2983">măn-ntə</ta>
            <ta e="T2985" id="Seg_2472" s="T2984">onʼiʔ</ta>
            <ta e="T2986" id="Seg_2473" s="T2985">ku-də</ta>
            <ta e="T2987" id="Seg_2474" s="T2986">păda-ʔ</ta>
            <ta e="T2988" id="Seg_2475" s="T2987">a</ta>
            <ta e="T2989" id="Seg_2476" s="T2988">onʼiʔ-Tə</ta>
            <ta e="T2990" id="Seg_2477" s="T2989">supso-ʔ</ta>
            <ta e="T2991" id="Seg_2478" s="T2990">bar</ta>
            <ta e="T2992" id="Seg_2479" s="T2991">mo-lV-j</ta>
            <ta e="T2993" id="Seg_2480" s="T2992">dĭgəttə</ta>
            <ta e="T2995" id="Seg_2481" s="T2994">dĭ-Tə</ta>
            <ta e="T2997" id="Seg_2482" s="T2996">a-bi</ta>
            <ta e="T2998" id="Seg_2483" s="T2997">i</ta>
            <ta e="T2999" id="Seg_2484" s="T2998">erer-bi</ta>
            <ta e="T3000" id="Seg_2485" s="T2999">xolstə</ta>
            <ta e="T3001" id="Seg_2486" s="T3000">a-bi</ta>
            <ta e="T3002" id="Seg_2487" s="T3001">dĭ</ta>
            <ta e="T3003" id="Seg_2488" s="T3002">dĭgəttə</ta>
            <ta e="T3004" id="Seg_2489" s="T3003">det-bi</ta>
            <ta e="T3005" id="Seg_2490" s="T3004">dĭgəttə</ta>
            <ta e="T3006" id="Seg_2491" s="T3005">bazoʔ</ta>
            <ta e="T3007" id="Seg_2492" s="T3006">iʔgö</ta>
            <ta e="T3008" id="Seg_2493" s="T3007">mo-bi</ta>
            <ta e="T3009" id="Seg_2494" s="T3008">dĭ</ta>
            <ta e="T3010" id="Seg_2495" s="T3009">bazoʔ</ta>
            <ta e="T3011" id="Seg_2496" s="T3010">kan-bi</ta>
            <ta e="T3012" id="Seg_2497" s="T3011">tüžöj-Tə</ta>
            <ta e="T3013" id="Seg_2498" s="T3012">bazoʔ</ta>
            <ta e="T3014" id="Seg_2499" s="T3013">a-bi</ta>
            <ta e="T3015" id="Seg_2500" s="T3014">dĭgəttə</ta>
            <ta e="T3016" id="Seg_2501" s="T3015">măn-ntə</ta>
            <ta e="T3017" id="Seg_2502" s="T3016">onʼiʔ</ta>
            <ta e="T3018" id="Seg_2503" s="T3017">koʔbdo-gəndə</ta>
            <ta e="T3019" id="Seg_2504" s="T3018">onʼiʔ</ta>
            <ta e="T3020" id="Seg_2505" s="T3019">sima-gəndə</ta>
            <ta e="T3021" id="Seg_2506" s="T3020">kan-ə-ʔ</ta>
            <ta e="T3022" id="Seg_2507" s="T3021">dĭ-ziʔ</ta>
            <ta e="T3023" id="Seg_2508" s="T3022">dĭ</ta>
            <ta e="T3024" id="Seg_2509" s="T3023">kan-bi</ta>
            <ta e="T3025" id="Seg_2510" s="T3024">dĭn</ta>
            <ta e="T3026" id="Seg_2511" s="T3025">iʔbö-bi</ta>
            <ta e="T3027" id="Seg_2512" s="T3026">kunol-zittə</ta>
            <ta e="T3028" id="Seg_2513" s="T3027">dĭ</ta>
            <ta e="T3029" id="Seg_2514" s="T3028">bar</ta>
            <ta e="T3030" id="Seg_2515" s="T3029">kunol-t</ta>
            <ta e="T3031" id="Seg_2516" s="T3030">sima</ta>
            <ta e="T3032" id="Seg_2517" s="T3031">kunol-t</ta>
            <ta e="T3033" id="Seg_2518" s="T3032">sima</ta>
            <ta e="T3034" id="Seg_2519" s="T3033">sima-t</ta>
            <ta e="T3035" id="Seg_2520" s="T3034">kunol-luʔbdə-bi</ta>
            <ta e="T3036" id="Seg_2521" s="T3035">dĭ-Tə</ta>
            <ta e="T3037" id="Seg_2522" s="T3036">bazoʔ</ta>
            <ta e="T3038" id="Seg_2523" s="T3037">tüžöj</ta>
            <ta e="T3039" id="Seg_2524" s="T3038">erer-luʔbdə-bi</ta>
            <ta e="T3040" id="Seg_2525" s="T3039">dĭ</ta>
            <ta e="T3041" id="Seg_2526" s="T3040">i-bi</ta>
            <ta e="T3042" id="Seg_2527" s="T3041">trubka-jəʔ</ta>
            <ta e="T3043" id="Seg_2528" s="T3042">xolstə-jəʔ</ta>
            <ta e="T3044" id="Seg_2529" s="T3043">det-bi</ta>
            <ta e="T3045" id="Seg_2530" s="T3044">dĭ</ta>
            <ta e="T3046" id="Seg_2531" s="T3045">nüke-Tə</ta>
            <ta e="T3047" id="Seg_2532" s="T3046">mĭ-bi</ta>
            <ta e="T3048" id="Seg_2533" s="T3047">dĭgəttə</ta>
            <ta e="T3049" id="Seg_2534" s="T3048">dĭ</ta>
            <ta e="T3050" id="Seg_2535" s="T3049">nüke</ta>
            <ta e="T3051" id="Seg_2536" s="T3050">baška</ta>
            <ta e="T3052" id="Seg_2537" s="T3051">koʔbdo-bə</ta>
            <ta e="T3053" id="Seg_2538" s="T3052">öʔ-laʔbə</ta>
            <ta e="T3054" id="Seg_2539" s="T3053">kan-ə-ʔ</ta>
            <ta e="T3055" id="Seg_2540" s="T3054">ĭmbi</ta>
            <ta e="T3056" id="Seg_2541" s="T3055">dĭ</ta>
            <ta e="T3057" id="Seg_2542" s="T3056">dĭn</ta>
            <ta e="T3058" id="Seg_2543" s="T3057">a-liA</ta>
            <ta e="T3059" id="Seg_2544" s="T3058">dĭ</ta>
            <ta e="T3060" id="Seg_2545" s="T3059">koʔbdo</ta>
            <ta e="T3061" id="Seg_2546" s="T3060">kan-bi</ta>
            <ta e="T3062" id="Seg_2547" s="T3061">dĭn</ta>
            <ta e="T3063" id="Seg_2548" s="T3062">iʔbö-bi</ta>
            <ta e="T3064" id="Seg_2549" s="T3063">dĭ</ta>
            <ta e="T3065" id="Seg_2550" s="T3064">bar</ta>
            <ta e="T3066" id="Seg_2551" s="T3065">dĭ-m</ta>
            <ta e="T3067" id="Seg_2552" s="T3066">kunol-luʔbdə-bi</ta>
            <ta e="T3069" id="Seg_2553" s="T3068">kunol-ə-ʔ</ta>
            <ta e="T3070" id="Seg_2554" s="T3069">sima</ta>
            <ta e="T3071" id="Seg_2555" s="T3070">kunol-ə-ʔ</ta>
            <ta e="T3072" id="Seg_2556" s="T3071">baška</ta>
            <ta e="T3073" id="Seg_2557" s="T3072">sima</ta>
            <ta e="T3074" id="Seg_2558" s="T3073">sima-zAŋ-də</ta>
            <ta e="T3075" id="Seg_2559" s="T3074">kunol-luʔbdə-bi</ta>
            <ta e="T3076" id="Seg_2560" s="T3075">tüžöj</ta>
            <ta e="T3077" id="Seg_2561" s="T3076">dĭ-Tə</ta>
            <ta e="T3078" id="Seg_2562" s="T3077">a-bi</ta>
            <ta e="T3079" id="Seg_2563" s="T3078">xolstə</ta>
            <ta e="T3080" id="Seg_2564" s="T3079">erer-bi</ta>
            <ta e="T3081" id="Seg_2565" s="T3080">i</ta>
            <ta e="T3082" id="Seg_2566" s="T3081">trubka-jəʔ</ta>
            <ta e="T3083" id="Seg_2567" s="T3082">mĭ-bi</ta>
            <ta e="T3084" id="Seg_2568" s="T3083">dĭgəttə</ta>
            <ta e="T3085" id="Seg_2569" s="T3084">dĭ</ta>
            <ta e="T3086" id="Seg_2570" s="T3085">šo-bi</ta>
            <ta e="T3087" id="Seg_2571" s="T3086">dĭ</ta>
            <ta e="T3088" id="Seg_2572" s="T3087">ĭššo</ta>
            <ta e="T3089" id="Seg_2573" s="T3088">tăŋ</ta>
            <ta e="T3090" id="Seg_2574" s="T3089">kuroː-l-luʔbdə-bi</ta>
            <ta e="T3091" id="Seg_2575" s="T3090">dĭgəttə</ta>
            <ta e="T3092" id="Seg_2576" s="T3091">nagur</ta>
            <ta e="T3093" id="Seg_2577" s="T3092">koʔbdo-bə</ta>
            <ta e="T3094" id="Seg_2578" s="T3093">öʔ-luʔbdə-bi</ta>
            <ta e="T3095" id="Seg_2579" s="T3094">nagur</ta>
            <ta e="T3096" id="Seg_2580" s="T3095">sima-t-ziʔ</ta>
            <ta e="T3097" id="Seg_2581" s="T3096">dĭ</ta>
            <ta e="T3098" id="Seg_2582" s="T3097">koʔbdo</ta>
            <ta e="T3099" id="Seg_2583" s="T3098">kan-bi</ta>
            <ta e="T3100" id="Seg_2584" s="T3099">i</ta>
            <ta e="T3101" id="Seg_2585" s="T3100">iʔbö-bi</ta>
            <ta e="T3102" id="Seg_2586" s="T3101">kunol-laʔbə</ta>
            <ta e="T3104" id="Seg_2587" s="T3103">Xavrošečka</ta>
            <ta e="T3105" id="Seg_2588" s="T3104">tože</ta>
            <ta e="T3106" id="Seg_2589" s="T3105">dĭ-m</ta>
            <ta e="T3107" id="Seg_2590" s="T3106">nüjnə</ta>
            <ta e="T3108" id="Seg_2591" s="T3107">nüj-laʔbə</ta>
            <ta e="T3109" id="Seg_2592" s="T3108">kunol-ə-ʔ</ta>
            <ta e="T3110" id="Seg_2593" s="T3109">sima</ta>
            <ta e="T3111" id="Seg_2594" s="T3110">kunol-ə-ʔ</ta>
            <ta e="T3112" id="Seg_2595" s="T3111">šide</ta>
            <ta e="T3113" id="Seg_2596" s="T3112">a</ta>
            <ta e="T3114" id="Seg_2597" s="T3113">nagur</ta>
            <ta e="T3115" id="Seg_2598" s="T3114">sima-ttə</ta>
            <ta e="T3116" id="Seg_2599" s="T3115">nöməl-luʔbdə-bi</ta>
            <ta e="T3117" id="Seg_2600" s="T3116">dĭgəttə</ta>
            <ta e="T3118" id="Seg_2601" s="T3117">šide</ta>
            <ta e="T3119" id="Seg_2602" s="T3118">sima-t</ta>
            <ta e="T3120" id="Seg_2603" s="T3119">kunol-luʔbdə-bi-jəʔ</ta>
            <ta e="T3121" id="Seg_2604" s="T3120">a</ta>
            <ta e="T3122" id="Seg_2605" s="T3121">onʼiʔ</ta>
            <ta e="T3123" id="Seg_2606" s="T3122">sima-t</ta>
            <ta e="T3124" id="Seg_2607" s="T3123">bar</ta>
            <ta e="T3125" id="Seg_2608" s="T3124">ku-bi</ta>
            <ta e="T3126" id="Seg_2609" s="T3125">dĭgəttə</ta>
            <ta e="T3127" id="Seg_2610" s="T3126">šo-bi</ta>
            <ta e="T3128" id="Seg_2611" s="T3127">maʔ-gəndə</ta>
            <ta e="T3129" id="Seg_2612" s="T3128">măn-ntə</ta>
            <ta e="T3130" id="Seg_2613" s="T3129">tüžöj</ta>
            <ta e="T3131" id="Seg_2614" s="T3130">dĭ-Tə</ta>
            <ta e="T3132" id="Seg_2615" s="T3131">a-liA</ta>
            <ta e="T3133" id="Seg_2616" s="T3132">dĭgəttə</ta>
            <ta e="T3134" id="Seg_2617" s="T3133">ertə-n</ta>
            <ta e="T3135" id="Seg_2618" s="T3134">uʔbdə-bi</ta>
            <ta e="T3136" id="Seg_2619" s="T3135">dĭgəttə</ta>
            <ta e="T3137" id="Seg_2620" s="T3136">ertə-n</ta>
            <ta e="T3138" id="Seg_2621" s="T3137">uʔbdə-bi</ta>
            <ta e="T3140" id="Seg_2622" s="T3139">büzʼe-Tə</ta>
            <ta e="T3141" id="Seg_2623" s="T3140">măn-ntə</ta>
            <ta e="T3142" id="Seg_2624" s="T3141">băt-t</ta>
            <ta e="T3143" id="Seg_2625" s="T3142">tüžöj-Tə</ta>
            <ta e="T3144" id="Seg_2626" s="T3143">a</ta>
            <ta e="T3145" id="Seg_2627" s="T3144">dĭ</ta>
            <ta e="T3146" id="Seg_2628" s="T3145">măn-ntə</ta>
            <ta e="T3147" id="Seg_2629" s="T3146">našto</ta>
            <ta e="T3148" id="Seg_2630" s="T3147">băt-zittə</ta>
            <ta e="T3149" id="Seg_2631" s="T3148">dĭ</ta>
            <ta e="T3150" id="Seg_2632" s="T3149">jakšə</ta>
            <ta e="T3151" id="Seg_2633" s="T3150">tüžöj</ta>
            <ta e="T3152" id="Seg_2634" s="T3151">iʔgö</ta>
            <ta e="T3153" id="Seg_2635" s="T3152">süt</ta>
            <ta e="T3154" id="Seg_2636" s="T3153">mĭ-liA</ta>
            <ta e="T3155" id="Seg_2637" s="T3154">a</ta>
            <ta e="T3156" id="Seg_2638" s="T3155">dĭ</ta>
            <ta e="T3157" id="Seg_2639" s="T3156">koʔbdo</ta>
            <ta e="T3158" id="Seg_2640" s="T3157">nuʔmə-luʔbdə-bi</ta>
            <ta e="T3159" id="Seg_2641" s="T3158">tănan</ta>
            <ta e="T3160" id="Seg_2642" s="T3159">băt-zittə</ta>
            <ta e="T3161" id="Seg_2643" s="T3160">xočet</ta>
            <ta e="T3162" id="Seg_2644" s="T3161">dĭ</ta>
            <ta e="T3163" id="Seg_2645" s="T3162">măn-ntə</ta>
            <ta e="T3164" id="Seg_2646" s="T3163">e-ʔ</ta>
            <ta e="T3165" id="Seg_2647" s="T3164">am-ə-ʔ</ta>
            <ta e="T3166" id="Seg_2648" s="T3165">măn</ta>
            <ta e="T3168" id="Seg_2649" s="T3167">uja-m</ta>
            <ta e="T3169" id="Seg_2650" s="T3168">a</ta>
            <ta e="T3170" id="Seg_2651" s="T3169">le-zAŋ-m</ta>
            <ta e="T3171" id="Seg_2652" s="T3170">i-t</ta>
            <ta e="T3172" id="Seg_2653" s="T3171">i</ta>
            <ta e="T3173" id="Seg_2654" s="T3172">kan-ə-ʔ</ta>
            <ta e="T3174" id="Seg_2655" s="T3173">tʼo-Tə</ta>
            <ta e="T3175" id="Seg_2656" s="T3174">tĭl-ə-ʔ</ta>
            <ta e="T3176" id="Seg_2657" s="T3175">dĭgəttə</ta>
            <ta e="T3177" id="Seg_2658" s="T3176">dĭ</ta>
            <ta e="T3178" id="Seg_2659" s="T3177">ej</ta>
            <ta e="T3179" id="Seg_2660" s="T3178">am-bi</ta>
            <ta e="T3180" id="Seg_2661" s="T3179">uja</ta>
            <ta e="T3181" id="Seg_2662" s="T3180">le-zAŋ-də</ta>
            <ta e="T3182" id="Seg_2663" s="T3181">oʔbdə-ʔ</ta>
            <ta e="T3183" id="Seg_2664" s="T3182">dĭgəttə</ta>
            <ta e="T3184" id="Seg_2665" s="T3183">büzʼe-t</ta>
            <ta e="T3185" id="Seg_2666" s="T3184">dĭ-m</ta>
            <ta e="T3186" id="Seg_2667" s="T3185">băt-luʔbdə-bi</ta>
            <ta e="T3187" id="Seg_2668" s="T3186">uja-zAŋ-də</ta>
            <ta e="T3188" id="Seg_2669" s="T3187">am-bi-jəʔ</ta>
            <ta e="T3189" id="Seg_2670" s="T3188">a</ta>
            <ta e="T3190" id="Seg_2671" s="T3189">dĭ</ta>
            <ta e="T3191" id="Seg_2672" s="T3190">Xavrošečka</ta>
            <ta e="T3192" id="Seg_2673" s="T3191">le-zAŋ-də</ta>
            <ta e="T3193" id="Seg_2674" s="T3192">oʔbdo-bi-jəʔ</ta>
            <ta e="T3194" id="Seg_2675" s="T3193">kan-bi</ta>
            <ta e="T3195" id="Seg_2676" s="T3194">tĭl-bi</ta>
            <ta e="T3196" id="Seg_2677" s="T3195">tʼo-Tə</ta>
            <ta e="T3197" id="Seg_2678" s="T3196">dĭgəttə</ta>
            <ta e="T3198" id="Seg_2679" s="T3197">özer-laʔbə-jəʔ</ta>
            <ta e="T3200" id="Seg_2680" s="T3199">kuvas-jəʔ</ta>
            <ta e="T3201" id="Seg_2681" s="T3200">šində</ta>
            <ta e="T3202" id="Seg_2682" s="T3201">ej</ta>
            <ta e="T3203" id="Seg_2683" s="T3202">šo-liA</ta>
            <ta e="T3204" id="Seg_2684" s="T3203">nu-laʔbə</ta>
            <ta e="T3205" id="Seg_2685" s="T3204">da</ta>
            <ta e="T3206" id="Seg_2686" s="T3205">ku-laʔbə</ta>
            <ta e="T3207" id="Seg_2687" s="T3206">dĭgəttə</ta>
            <ta e="T3209" id="Seg_2688" s="T3208">šonə-gA</ta>
            <ta e="T3210" id="Seg_2689" s="T3209">koŋ</ta>
            <ta e="T3211" id="Seg_2690" s="T3210">kuza</ta>
            <ta e="T3212" id="Seg_2691" s="T3211">šində</ta>
            <ta e="T3213" id="Seg_2692" s="T3212">măna</ta>
            <ta e="T3214" id="Seg_2693" s="T3213">mĭ-lV-j</ta>
            <ta e="T3215" id="Seg_2694" s="T3214">dĭ</ta>
            <ta e="T3217" id="Seg_2695" s="T3216">dĭ-m</ta>
            <ta e="T3218" id="Seg_2696" s="T3217">tibi-Tə</ta>
            <ta e="T3219" id="Seg_2697" s="T3218">i-lV-m</ta>
            <ta e="T3220" id="Seg_2698" s="T3219">dĭ-zAŋ</ta>
            <ta e="T3221" id="Seg_2699" s="T3220">ej</ta>
            <ta e="T3222" id="Seg_2700" s="T3221">mo-bi-jəʔ</ta>
            <ta e="T3223" id="Seg_2701" s="T3222">i-zittə</ta>
            <ta e="T3224" id="Seg_2702" s="T3223">nʼuʔdə</ta>
            <ta e="T3225" id="Seg_2703" s="T3224">kan-ntə-gA-jəʔ</ta>
            <ta e="T3226" id="Seg_2704" s="T3225">dĭgəttə</ta>
            <ta e="T3227" id="Seg_2705" s="T3226">dĭ</ta>
            <ta e="T3228" id="Seg_2706" s="T3227">Xavrošečka</ta>
            <ta e="T3229" id="Seg_2707" s="T3228">šo-bi</ta>
            <ta e="T3230" id="Seg_2708" s="T3229">dĭ</ta>
            <ta e="T3231" id="Seg_2709" s="T3230">dĭ</ta>
            <ta e="T3232" id="Seg_2710" s="T3231">dĭ-Tə</ta>
            <ta e="T3235" id="Seg_2711" s="T3234">dĭ</ta>
            <ta e="T3237" id="Seg_2712" s="T3236">mĭ-bi</ta>
            <ta e="T3238" id="Seg_2713" s="T3237">dĭ</ta>
            <ta e="T3239" id="Seg_2714" s="T3238">dĭ</ta>
            <ta e="T3240" id="Seg_2715" s="T3239">kuza-Tə</ta>
            <ta e="T3241" id="Seg_2716" s="T3240">dĭ</ta>
            <ta e="T3242" id="Seg_2717" s="T3241">dĭ-m</ta>
            <ta e="T3243" id="Seg_2718" s="T3242">tibi-Tə</ta>
            <ta e="T3244" id="Seg_2719" s="T3243">i-bi</ta>
            <ta e="T3245" id="Seg_2720" s="T3244">i</ta>
            <ta e="T3246" id="Seg_2721" s="T3245">kan-lAʔ</ta>
            <ta e="T3247" id="Seg_2722" s="T3246">tʼür-bi</ta>
            <ta e="T3248" id="Seg_2723" s="T3247">dĭ-zAŋ-gəʔ</ta>
            <ta e="T3249" id="Seg_2724" s="T3248">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2919" id="Seg_2725" s="T2918">live-PST.[3SG]</ta>
            <ta e="T2920" id="Seg_2726" s="T2919">one.[NOM.SG]</ta>
            <ta e="T2921" id="Seg_2727" s="T2920">girl.[NOM.SG]</ta>
            <ta e="T2923" id="Seg_2728" s="T2922">this-ACC</ta>
            <ta e="T2924" id="Seg_2729" s="T2923">name-DUR-3PL</ta>
            <ta e="T2925" id="Seg_2730" s="T2924">Khavroshechka.[NOM.SG]</ta>
            <ta e="T2926" id="Seg_2731" s="T2925">this-GEN</ta>
            <ta e="T2927" id="Seg_2732" s="T2926">mother-NOM/GEN.3SG</ta>
            <ta e="T2928" id="Seg_2733" s="T2927">father-NOM/GEN.3SG</ta>
            <ta e="T2929" id="Seg_2734" s="T2928">die-RES-PST-3PL</ta>
            <ta e="T2930" id="Seg_2735" s="T2929">people.[NOM.SG]</ta>
            <ta e="T2931" id="Seg_2736" s="T2930">NEG</ta>
            <ta e="T2932" id="Seg_2737" s="T2931">good-PL</ta>
            <ta e="T2933" id="Seg_2738" s="T2932">PTCL</ta>
            <ta e="T2934" id="Seg_2739" s="T2933">this-LAT</ta>
            <ta e="T2935" id="Seg_2740" s="T2934">this-ACC</ta>
            <ta e="T2936" id="Seg_2741" s="T2935">take-PST-3PL</ta>
            <ta e="T2937" id="Seg_2742" s="T2936">this-GEN</ta>
            <ta e="T2938" id="Seg_2743" s="T2937">three.[NOM.SG]</ta>
            <ta e="T2939" id="Seg_2744" s="T2938">daughter-NOM/GEN.3SG</ta>
            <ta e="T2940" id="Seg_2745" s="T2939">be-PST-3PL</ta>
            <ta e="T2941" id="Seg_2746" s="T2940">what.[NOM.SG]=INDEF</ta>
            <ta e="T2942" id="Seg_2747" s="T2941">NEG</ta>
            <ta e="T2943" id="Seg_2748" s="T2942">make-PST-3PL</ta>
            <ta e="T2944" id="Seg_2749" s="T2943">and</ta>
            <ta e="T2945" id="Seg_2750" s="T2944">this.[NOM.SG]</ta>
            <ta e="T2946" id="Seg_2751" s="T2945">Khavroshechka.[NOM.SG]</ta>
            <ta e="T2947" id="Seg_2752" s="T2946">all</ta>
            <ta e="T2948" id="Seg_2753" s="T2947">work-PST.[3SG]</ta>
            <ta e="T2949" id="Seg_2754" s="T2948">all</ta>
            <ta e="T2950" id="Seg_2755" s="T2949">what.[NOM.SG]</ta>
            <ta e="T2951" id="Seg_2756" s="T2950">work-PRS.[3SG]</ta>
            <ta e="T2952" id="Seg_2757" s="T2951">NEG</ta>
            <ta e="T2953" id="Seg_2758" s="T2952">this-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T2954" id="Seg_2759" s="T2953">NEG</ta>
            <ta e="T2955" id="Seg_2760" s="T2954">can-PRS.[3SG]</ta>
            <ta e="T2956" id="Seg_2761" s="T2955">always</ta>
            <ta e="T2957" id="Seg_2762" s="T2956">scold-FRQ-DUR.[3SG]</ta>
            <ta e="T2958" id="Seg_2763" s="T2957">woman.[NOM.SG]</ta>
            <ta e="T2959" id="Seg_2764" s="T2958">then</ta>
            <ta e="T2960" id="Seg_2765" s="T2959">say-IPFVZ.[3SG]</ta>
            <ta e="T2961" id="Seg_2766" s="T2960">go-EP-IMP.2SG</ta>
            <ta e="T2962" id="Seg_2767" s="T2961">I.LAT</ta>
            <ta e="T2963" id="Seg_2768" s="T2962">make-IMP.2SG</ta>
            <ta e="T2964" id="Seg_2769" s="T2963">many</ta>
            <ta e="T2965" id="Seg_2770" s="T2964">linen.[NOM.SG]</ta>
            <ta e="T2966" id="Seg_2771" s="T2965">this.[NOM.SG]</ta>
            <ta e="T2967" id="Seg_2772" s="T2966">go-PST.[3SG]</ta>
            <ta e="T2968" id="Seg_2773" s="T2967">cow-LAT</ta>
            <ta e="T2969" id="Seg_2774" s="T2968">and</ta>
            <ta e="T2970" id="Seg_2775" s="T2969">PTCL</ta>
            <ta e="T2971" id="Seg_2776" s="T2970">cry-DUR.[3SG]</ta>
            <ta e="T2973" id="Seg_2777" s="T2972">I.ACC</ta>
            <ta e="T2974" id="Seg_2778" s="T2973">strongly</ta>
            <ta e="T2975" id="Seg_2779" s="T2974">beat-DUR-3PL</ta>
            <ta e="T2976" id="Seg_2780" s="T2975">this-PL-LAT</ta>
            <ta e="T2977" id="Seg_2781" s="T2976">what.[NOM.SG]=INDEF</ta>
            <ta e="T2978" id="Seg_2782" s="T2977">make-INF.LAT</ta>
            <ta e="T2979" id="Seg_2783" s="T2978">NEG</ta>
            <ta e="T2981" id="Seg_2784" s="T2980">can-PRS-1SG</ta>
            <ta e="T2982" id="Seg_2785" s="T2981">and</ta>
            <ta e="T2983" id="Seg_2786" s="T2982">cow.[NOM.SG]</ta>
            <ta e="T2984" id="Seg_2787" s="T2983">say-IPFVZ.[3SG]</ta>
            <ta e="T2985" id="Seg_2788" s="T2984">one.[NOM.SG]</ta>
            <ta e="T2986" id="Seg_2789" s="T2985">ear-NOM/GEN/ACC.3SG</ta>
            <ta e="T2987" id="Seg_2790" s="T2986">creep.into-IMP.2SG</ta>
            <ta e="T2988" id="Seg_2791" s="T2987">and</ta>
            <ta e="T2989" id="Seg_2792" s="T2988">one-LAT</ta>
            <ta e="T2990" id="Seg_2793" s="T2989">depart-IMP.2SG</ta>
            <ta e="T2991" id="Seg_2794" s="T2990">all</ta>
            <ta e="T2992" id="Seg_2795" s="T2991">become-FUT-3SG</ta>
            <ta e="T2993" id="Seg_2796" s="T2992">then</ta>
            <ta e="T2995" id="Seg_2797" s="T2994">this-LAT</ta>
            <ta e="T2997" id="Seg_2798" s="T2996">make-PST.[3SG]</ta>
            <ta e="T2998" id="Seg_2799" s="T2997">and</ta>
            <ta e="T2999" id="Seg_2800" s="T2998">spin-PST.[3SG]</ta>
            <ta e="T3000" id="Seg_2801" s="T2999">linen.[NOM.SG]</ta>
            <ta e="T3001" id="Seg_2802" s="T3000">make-PST.[3SG]</ta>
            <ta e="T3002" id="Seg_2803" s="T3001">this</ta>
            <ta e="T3003" id="Seg_2804" s="T3002">then</ta>
            <ta e="T3004" id="Seg_2805" s="T3003">bring-PST.[3SG]</ta>
            <ta e="T3005" id="Seg_2806" s="T3004">then</ta>
            <ta e="T3006" id="Seg_2807" s="T3005">again</ta>
            <ta e="T3007" id="Seg_2808" s="T3006">many</ta>
            <ta e="T3008" id="Seg_2809" s="T3007">become-PST.[3SG]</ta>
            <ta e="T3009" id="Seg_2810" s="T3008">this.[NOM.SG]</ta>
            <ta e="T3010" id="Seg_2811" s="T3009">again</ta>
            <ta e="T3011" id="Seg_2812" s="T3010">go-PST.[3SG]</ta>
            <ta e="T3012" id="Seg_2813" s="T3011">cow-LAT</ta>
            <ta e="T3013" id="Seg_2814" s="T3012">again</ta>
            <ta e="T3014" id="Seg_2815" s="T3013">make-PST.[3SG]</ta>
            <ta e="T3015" id="Seg_2816" s="T3014">then</ta>
            <ta e="T3016" id="Seg_2817" s="T3015">say-IPFVZ.[3SG]</ta>
            <ta e="T3017" id="Seg_2818" s="T3016">one.[NOM.SG]</ta>
            <ta e="T3018" id="Seg_2819" s="T3017">daughter-LAT/LOC.3SG</ta>
            <ta e="T3019" id="Seg_2820" s="T3018">one.[NOM.SG]</ta>
            <ta e="T3020" id="Seg_2821" s="T3019">eye-LAT/LOC.3SG</ta>
            <ta e="T3021" id="Seg_2822" s="T3020">go-EP-IMP.2SG</ta>
            <ta e="T3022" id="Seg_2823" s="T3021">this-INS</ta>
            <ta e="T3023" id="Seg_2824" s="T3022">this.[NOM.SG]</ta>
            <ta e="T3024" id="Seg_2825" s="T3023">go-PST.[3SG]</ta>
            <ta e="T3025" id="Seg_2826" s="T3024">there</ta>
            <ta e="T3026" id="Seg_2827" s="T3025">lie-PST.[3SG]</ta>
            <ta e="T3027" id="Seg_2828" s="T3026">sleep-INF.LAT</ta>
            <ta e="T3028" id="Seg_2829" s="T3027">this.[NOM.SG]</ta>
            <ta e="T3029" id="Seg_2830" s="T3028">PTCL</ta>
            <ta e="T3030" id="Seg_2831" s="T3029">sleep-IMP.2SG.O</ta>
            <ta e="T3031" id="Seg_2832" s="T3030">eye.[NOM.SG]</ta>
            <ta e="T3032" id="Seg_2833" s="T3031">sleep-IMP.2SG.O</ta>
            <ta e="T3033" id="Seg_2834" s="T3032">eye.[NOM.SG]</ta>
            <ta e="T3034" id="Seg_2835" s="T3033">eye-NOM/GEN.3SG</ta>
            <ta e="T3035" id="Seg_2836" s="T3034">sleep-MOM-PST.[3SG]</ta>
            <ta e="T3036" id="Seg_2837" s="T3035">this-LAT</ta>
            <ta e="T3037" id="Seg_2838" s="T3036">again</ta>
            <ta e="T3038" id="Seg_2839" s="T3037">cow.[NOM.SG]</ta>
            <ta e="T3039" id="Seg_2840" s="T3038">spin-MOM-PST.[3SG]</ta>
            <ta e="T3040" id="Seg_2841" s="T3039">this.[NOM.SG]</ta>
            <ta e="T3041" id="Seg_2842" s="T3040">take-PST.[3SG]</ta>
            <ta e="T3042" id="Seg_2843" s="T3041">roll-PL</ta>
            <ta e="T3043" id="Seg_2844" s="T3042">linen-PL</ta>
            <ta e="T3044" id="Seg_2845" s="T3043">bring-PST.[3SG]</ta>
            <ta e="T3045" id="Seg_2846" s="T3044">this.[NOM.SG]</ta>
            <ta e="T3046" id="Seg_2847" s="T3045">woman-LAT</ta>
            <ta e="T3047" id="Seg_2848" s="T3046">give-PST.[3SG]</ta>
            <ta e="T3048" id="Seg_2849" s="T3047">then</ta>
            <ta e="T3049" id="Seg_2850" s="T3048">this.[NOM.SG]</ta>
            <ta e="T3050" id="Seg_2851" s="T3049">woman.[NOM.SG]</ta>
            <ta e="T3051" id="Seg_2852" s="T3050">another.[NOM.SG]</ta>
            <ta e="T3052" id="Seg_2853" s="T3051">daughter-ACC.3SG</ta>
            <ta e="T3053" id="Seg_2854" s="T3052">let-DUR.[3SG]</ta>
            <ta e="T3054" id="Seg_2855" s="T3053">go-EP-IMP.2SG</ta>
            <ta e="T3055" id="Seg_2856" s="T3054">what.[NOM.SG]</ta>
            <ta e="T3056" id="Seg_2857" s="T3055">this.[NOM.SG]</ta>
            <ta e="T3057" id="Seg_2858" s="T3056">there</ta>
            <ta e="T3058" id="Seg_2859" s="T3057">make-PRS.[3SG]</ta>
            <ta e="T3059" id="Seg_2860" s="T3058">this.[NOM.SG]</ta>
            <ta e="T3060" id="Seg_2861" s="T3059">girl.[NOM.SG]</ta>
            <ta e="T3061" id="Seg_2862" s="T3060">go-PST.[3SG]</ta>
            <ta e="T3062" id="Seg_2863" s="T3061">there</ta>
            <ta e="T3063" id="Seg_2864" s="T3062">lie-PST.[3SG]</ta>
            <ta e="T3064" id="Seg_2865" s="T3063">this.[NOM.SG]</ta>
            <ta e="T3065" id="Seg_2866" s="T3064">PTCL</ta>
            <ta e="T3066" id="Seg_2867" s="T3065">this-ACC</ta>
            <ta e="T3067" id="Seg_2868" s="T3066">sleep-MOM-PST.[3SG]</ta>
            <ta e="T3069" id="Seg_2869" s="T3068">sleep-EP-IMP.2SG</ta>
            <ta e="T3070" id="Seg_2870" s="T3069">eye.[NOM.SG]</ta>
            <ta e="T3071" id="Seg_2871" s="T3070">sleep-EP-IMP.2SG</ta>
            <ta e="T3072" id="Seg_2872" s="T3071">another.[NOM.SG]</ta>
            <ta e="T3073" id="Seg_2873" s="T3072">eye.[NOM.SG]</ta>
            <ta e="T3074" id="Seg_2874" s="T3073">eye-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T3075" id="Seg_2875" s="T3074">sleep-MOM-PST.[3SG]</ta>
            <ta e="T3076" id="Seg_2876" s="T3075">cow.[NOM.SG]</ta>
            <ta e="T3077" id="Seg_2877" s="T3076">this-LAT</ta>
            <ta e="T3078" id="Seg_2878" s="T3077">make-PST.[3SG]</ta>
            <ta e="T3079" id="Seg_2879" s="T3078">linen.[NOM.SG]</ta>
            <ta e="T3080" id="Seg_2880" s="T3079">spin-PST.[3SG]</ta>
            <ta e="T3081" id="Seg_2881" s="T3080">and</ta>
            <ta e="T3082" id="Seg_2882" s="T3081">roll-PL</ta>
            <ta e="T3083" id="Seg_2883" s="T3082">give-PST.[3SG]</ta>
            <ta e="T3084" id="Seg_2884" s="T3083">then</ta>
            <ta e="T3085" id="Seg_2885" s="T3084">this.[NOM.SG]</ta>
            <ta e="T3086" id="Seg_2886" s="T3085">come-PST.[3SG]</ta>
            <ta e="T3087" id="Seg_2887" s="T3086">this.[NOM.SG]</ta>
            <ta e="T3088" id="Seg_2888" s="T3087">more</ta>
            <ta e="T3089" id="Seg_2889" s="T3088">strongly</ta>
            <ta e="T3090" id="Seg_2890" s="T3089">be.angry-FRQ-MOM-PST.[3SG]</ta>
            <ta e="T3091" id="Seg_2891" s="T3090">then</ta>
            <ta e="T3092" id="Seg_2892" s="T3091">three.[NOM.SG]</ta>
            <ta e="T3093" id="Seg_2893" s="T3092">daughter-ACC.3SG</ta>
            <ta e="T3094" id="Seg_2894" s="T3093">send-MOM-PST.[3SG]</ta>
            <ta e="T3095" id="Seg_2895" s="T3094">three.[NOM.SG]</ta>
            <ta e="T3096" id="Seg_2896" s="T3095">eye-3SG-INS</ta>
            <ta e="T3097" id="Seg_2897" s="T3096">this.[NOM.SG]</ta>
            <ta e="T3098" id="Seg_2898" s="T3097">girl.[NOM.SG]</ta>
            <ta e="T3099" id="Seg_2899" s="T3098">go-PST.[3SG]</ta>
            <ta e="T3100" id="Seg_2900" s="T3099">and</ta>
            <ta e="T3101" id="Seg_2901" s="T3100">lie-PST.[3SG]</ta>
            <ta e="T3102" id="Seg_2902" s="T3101">sleep-DUR.[3SG]</ta>
            <ta e="T3104" id="Seg_2903" s="T3103">Khavroshechka.[NOM.SG]</ta>
            <ta e="T3105" id="Seg_2904" s="T3104">also</ta>
            <ta e="T3106" id="Seg_2905" s="T3105">this-ACC</ta>
            <ta e="T3107" id="Seg_2906" s="T3106">song.[NOM.SG]</ta>
            <ta e="T3108" id="Seg_2907" s="T3107">sing-DUR.[3SG]</ta>
            <ta e="T3109" id="Seg_2908" s="T3108">sleep-EP-IMP.2SG</ta>
            <ta e="T3110" id="Seg_2909" s="T3109">eye.[NOM.SG]</ta>
            <ta e="T3111" id="Seg_2910" s="T3110">sleep-EP-IMP.2SG</ta>
            <ta e="T3112" id="Seg_2911" s="T3111">two.[NOM.SG]</ta>
            <ta e="T3113" id="Seg_2912" s="T3112">and</ta>
            <ta e="T3114" id="Seg_2913" s="T3113">three.[NOM.SG]</ta>
            <ta e="T3115" id="Seg_2914" s="T3114">eye-ABL.3SG</ta>
            <ta e="T3116" id="Seg_2915" s="T3115">forget-MOM-PST.[3SG]</ta>
            <ta e="T3117" id="Seg_2916" s="T3116">then</ta>
            <ta e="T3118" id="Seg_2917" s="T3117">two.[NOM.SG]</ta>
            <ta e="T3119" id="Seg_2918" s="T3118">eye-NOM/GEN.3SG</ta>
            <ta e="T3120" id="Seg_2919" s="T3119">sleep-MOM-PST-3PL</ta>
            <ta e="T3121" id="Seg_2920" s="T3120">and</ta>
            <ta e="T3122" id="Seg_2921" s="T3121">one.[NOM.SG]</ta>
            <ta e="T3123" id="Seg_2922" s="T3122">eye-NOM/GEN.3SG</ta>
            <ta e="T3124" id="Seg_2923" s="T3123">PTCL</ta>
            <ta e="T3125" id="Seg_2924" s="T3124">see-PST.[3SG]</ta>
            <ta e="T3126" id="Seg_2925" s="T3125">then</ta>
            <ta e="T3127" id="Seg_2926" s="T3126">come-PST.[3SG]</ta>
            <ta e="T3128" id="Seg_2927" s="T3127">tent-LAT/LOC.3SG</ta>
            <ta e="T3129" id="Seg_2928" s="T3128">say-IPFVZ.[3SG]</ta>
            <ta e="T3130" id="Seg_2929" s="T3129">cow.[NOM.SG]</ta>
            <ta e="T3131" id="Seg_2930" s="T3130">this-LAT</ta>
            <ta e="T3132" id="Seg_2931" s="T3131">make-PRS.[3SG]</ta>
            <ta e="T3133" id="Seg_2932" s="T3132">then</ta>
            <ta e="T3134" id="Seg_2933" s="T3133">morning-GEN</ta>
            <ta e="T3135" id="Seg_2934" s="T3134">get.up-PST.[3SG]</ta>
            <ta e="T3136" id="Seg_2935" s="T3135">then</ta>
            <ta e="T3137" id="Seg_2936" s="T3136">morning-GEN</ta>
            <ta e="T3138" id="Seg_2937" s="T3137">get.up-PST.[3SG]</ta>
            <ta e="T3140" id="Seg_2938" s="T3139">man-LAT</ta>
            <ta e="T3141" id="Seg_2939" s="T3140">say-IPFVZ.[3SG]</ta>
            <ta e="T3142" id="Seg_2940" s="T3141">cut-IMP.2SG.O</ta>
            <ta e="T3143" id="Seg_2941" s="T3142">cow-LAT</ta>
            <ta e="T3144" id="Seg_2942" s="T3143">and</ta>
            <ta e="T3145" id="Seg_2943" s="T3144">this.[NOM.SG]</ta>
            <ta e="T3146" id="Seg_2944" s="T3145">say-IPFVZ.[3SG]</ta>
            <ta e="T3147" id="Seg_2945" s="T3146">what.for</ta>
            <ta e="T3148" id="Seg_2946" s="T3147">cut-INF.LAT</ta>
            <ta e="T3149" id="Seg_2947" s="T3148">this.[NOM.SG]</ta>
            <ta e="T3150" id="Seg_2948" s="T3149">good.[NOM.SG]</ta>
            <ta e="T3151" id="Seg_2949" s="T3150">cow.[NOM.SG]</ta>
            <ta e="T3152" id="Seg_2950" s="T3151">many</ta>
            <ta e="T3153" id="Seg_2951" s="T3152">milk.[NOM.SG]</ta>
            <ta e="T3154" id="Seg_2952" s="T3153">give-PRS.[3SG]</ta>
            <ta e="T3155" id="Seg_2953" s="T3154">and</ta>
            <ta e="T3156" id="Seg_2954" s="T3155">this.[NOM.SG]</ta>
            <ta e="T3157" id="Seg_2955" s="T3156">girl.[NOM.SG]</ta>
            <ta e="T3158" id="Seg_2956" s="T3157">run-MOM-PST.[3SG]</ta>
            <ta e="T3159" id="Seg_2957" s="T3158">you.ACC</ta>
            <ta e="T3160" id="Seg_2958" s="T3159">cut-INF.LAT</ta>
            <ta e="T3161" id="Seg_2959" s="T3160">want.3SG</ta>
            <ta e="T3162" id="Seg_2960" s="T3161">this.[NOM.SG]</ta>
            <ta e="T3163" id="Seg_2961" s="T3162">say-IPFVZ.[3SG]</ta>
            <ta e="T3164" id="Seg_2962" s="T3163">NEG.AUX-IMP.2SG</ta>
            <ta e="T3165" id="Seg_2963" s="T3164">eat-EP-CNG</ta>
            <ta e="T3166" id="Seg_2964" s="T3165">I.GEN</ta>
            <ta e="T3168" id="Seg_2965" s="T3167">meat-NOM/GEN/ACC.1SG</ta>
            <ta e="T3169" id="Seg_2966" s="T3168">and</ta>
            <ta e="T3170" id="Seg_2967" s="T3169">bone-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T3171" id="Seg_2968" s="T3170">take-IMP.2SG.O</ta>
            <ta e="T3172" id="Seg_2969" s="T3171">and</ta>
            <ta e="T3173" id="Seg_2970" s="T3172">go-EP-IMP.2SG</ta>
            <ta e="T3174" id="Seg_2971" s="T3173">place-LAT</ta>
            <ta e="T3175" id="Seg_2972" s="T3174">dig-EP-IMP.2SG</ta>
            <ta e="T3176" id="Seg_2973" s="T3175">then</ta>
            <ta e="T3177" id="Seg_2974" s="T3176">this.[NOM.SG]</ta>
            <ta e="T3178" id="Seg_2975" s="T3177">NEG</ta>
            <ta e="T3179" id="Seg_2976" s="T3178">eat-PST.[3SG]</ta>
            <ta e="T3180" id="Seg_2977" s="T3179">meat.[NOM.SG]</ta>
            <ta e="T3181" id="Seg_2978" s="T3180">bone-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T3182" id="Seg_2979" s="T3181">collect-IMP.2SG</ta>
            <ta e="T3183" id="Seg_2980" s="T3182">then</ta>
            <ta e="T3184" id="Seg_2981" s="T3183">man-NOM/GEN.3SG</ta>
            <ta e="T3185" id="Seg_2982" s="T3184">this-ACC</ta>
            <ta e="T3186" id="Seg_2983" s="T3185">cut-MOM-PST.[3SG]</ta>
            <ta e="T3187" id="Seg_2984" s="T3186">meat-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T3188" id="Seg_2985" s="T3187">eat-PST-3PL</ta>
            <ta e="T3189" id="Seg_2986" s="T3188">and</ta>
            <ta e="T3190" id="Seg_2987" s="T3189">this.[NOM.SG]</ta>
            <ta e="T3191" id="Seg_2988" s="T3190">Khavroshechka.[NOM.SG]</ta>
            <ta e="T3192" id="Seg_2989" s="T3191">bone-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T3193" id="Seg_2990" s="T3192">gather-PST-3PL</ta>
            <ta e="T3194" id="Seg_2991" s="T3193">go-PST.[3SG]</ta>
            <ta e="T3195" id="Seg_2992" s="T3194">dig-PST.[3SG]</ta>
            <ta e="T3196" id="Seg_2993" s="T3195">earth-LAT</ta>
            <ta e="T3197" id="Seg_2994" s="T3196">then</ta>
            <ta e="T3198" id="Seg_2995" s="T3197">grow-DUR-3PL</ta>
            <ta e="T3200" id="Seg_2996" s="T3199">beautiful-PL</ta>
            <ta e="T3201" id="Seg_2997" s="T3200">who.[NOM.SG]</ta>
            <ta e="T3202" id="Seg_2998" s="T3201">NEG</ta>
            <ta e="T3203" id="Seg_2999" s="T3202">come-PRS.[3SG]</ta>
            <ta e="T3204" id="Seg_3000" s="T3203">stand-DUR.[3SG]</ta>
            <ta e="T3205" id="Seg_3001" s="T3204">and</ta>
            <ta e="T3206" id="Seg_3002" s="T3205">see-DUR.[3SG]</ta>
            <ta e="T3207" id="Seg_3003" s="T3206">then</ta>
            <ta e="T3209" id="Seg_3004" s="T3208">come-PRS.[3SG]</ta>
            <ta e="T3210" id="Seg_3005" s="T3209">chief.[NOM.SG]</ta>
            <ta e="T3211" id="Seg_3006" s="T3210">man.[NOM.SG]</ta>
            <ta e="T3212" id="Seg_3007" s="T3211">who.[NOM.SG]</ta>
            <ta e="T3213" id="Seg_3008" s="T3212">I.LAT</ta>
            <ta e="T3214" id="Seg_3009" s="T3213">give-FUT-3SG</ta>
            <ta e="T3215" id="Seg_3010" s="T3214">this.[NOM.SG]</ta>
            <ta e="T3217" id="Seg_3011" s="T3216">this-ACC</ta>
            <ta e="T3218" id="Seg_3012" s="T3217">man-LAT</ta>
            <ta e="T3219" id="Seg_3013" s="T3218">take-FUT-1SG</ta>
            <ta e="T3220" id="Seg_3014" s="T3219">this-PL</ta>
            <ta e="T3221" id="Seg_3015" s="T3220">NEG</ta>
            <ta e="T3222" id="Seg_3016" s="T3221">can-PST-3PL</ta>
            <ta e="T3223" id="Seg_3017" s="T3222">take-INF.LAT</ta>
            <ta e="T3224" id="Seg_3018" s="T3223">up</ta>
            <ta e="T3225" id="Seg_3019" s="T3224">go-IPFVZ-PRS-3PL</ta>
            <ta e="T3226" id="Seg_3020" s="T3225">then</ta>
            <ta e="T3227" id="Seg_3021" s="T3226">this.[NOM.SG]</ta>
            <ta e="T3228" id="Seg_3022" s="T3227">Khavroshechka.[NOM.SG]</ta>
            <ta e="T3229" id="Seg_3023" s="T3228">come-PST.[3SG]</ta>
            <ta e="T3230" id="Seg_3024" s="T3229">this.[NOM.SG]</ta>
            <ta e="T3231" id="Seg_3025" s="T3230">this.[NOM.SG]</ta>
            <ta e="T3232" id="Seg_3026" s="T3231">this-LAT</ta>
            <ta e="T3235" id="Seg_3027" s="T3234">this.[NOM.SG]</ta>
            <ta e="T3237" id="Seg_3028" s="T3236">give-PST.[3SG]</ta>
            <ta e="T3238" id="Seg_3029" s="T3237">this.[NOM.SG]</ta>
            <ta e="T3239" id="Seg_3030" s="T3238">this.[NOM.SG]</ta>
            <ta e="T3240" id="Seg_3031" s="T3239">man-LAT</ta>
            <ta e="T3241" id="Seg_3032" s="T3240">this.[NOM.SG]</ta>
            <ta e="T3242" id="Seg_3033" s="T3241">this-ACC</ta>
            <ta e="T3243" id="Seg_3034" s="T3242">man-LAT</ta>
            <ta e="T3244" id="Seg_3035" s="T3243">take-PST.[3SG]</ta>
            <ta e="T3245" id="Seg_3036" s="T3244">and</ta>
            <ta e="T3246" id="Seg_3037" s="T3245">go-CVB</ta>
            <ta e="T3247" id="Seg_3038" s="T3246">disappear-PST.[3SG]</ta>
            <ta e="T3248" id="Seg_3039" s="T3247">this-PL-ABL</ta>
            <ta e="T3249" id="Seg_3040" s="T3248">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2919" id="Seg_3041" s="T2918">жить-PST.[3SG]</ta>
            <ta e="T2920" id="Seg_3042" s="T2919">один.[NOM.SG]</ta>
            <ta e="T2921" id="Seg_3043" s="T2920">девушка.[NOM.SG]</ta>
            <ta e="T2923" id="Seg_3044" s="T2922">этот-ACC</ta>
            <ta e="T2924" id="Seg_3045" s="T2923">называть-DUR-3PL</ta>
            <ta e="T2925" id="Seg_3046" s="T2924">Хаврошечка.[NOM.SG]</ta>
            <ta e="T2926" id="Seg_3047" s="T2925">этот-GEN</ta>
            <ta e="T2927" id="Seg_3048" s="T2926">мать-NOM/GEN.3SG</ta>
            <ta e="T2928" id="Seg_3049" s="T2927">отец-NOM/GEN.3SG</ta>
            <ta e="T2929" id="Seg_3050" s="T2928">умереть-RES-PST-3PL</ta>
            <ta e="T2930" id="Seg_3051" s="T2929">люди.[NOM.SG]</ta>
            <ta e="T2931" id="Seg_3052" s="T2930">NEG</ta>
            <ta e="T2932" id="Seg_3053" s="T2931">хороший-PL</ta>
            <ta e="T2933" id="Seg_3054" s="T2932">PTCL</ta>
            <ta e="T2934" id="Seg_3055" s="T2933">этот-LAT</ta>
            <ta e="T2935" id="Seg_3056" s="T2934">этот-ACC</ta>
            <ta e="T2936" id="Seg_3057" s="T2935">взять-PST-3PL</ta>
            <ta e="T2937" id="Seg_3058" s="T2936">этот-GEN</ta>
            <ta e="T2938" id="Seg_3059" s="T2937">три.[NOM.SG]</ta>
            <ta e="T2939" id="Seg_3060" s="T2938">дочь-NOM/GEN.3SG</ta>
            <ta e="T2940" id="Seg_3061" s="T2939">быть-PST-3PL</ta>
            <ta e="T2941" id="Seg_3062" s="T2940">что.[NOM.SG]=INDEF</ta>
            <ta e="T2942" id="Seg_3063" s="T2941">NEG</ta>
            <ta e="T2943" id="Seg_3064" s="T2942">делать-PST-3PL</ta>
            <ta e="T2944" id="Seg_3065" s="T2943">а</ta>
            <ta e="T2945" id="Seg_3066" s="T2944">этот.[NOM.SG]</ta>
            <ta e="T2946" id="Seg_3067" s="T2945">Хаврошечка.[NOM.SG]</ta>
            <ta e="T2947" id="Seg_3068" s="T2946">весь</ta>
            <ta e="T2948" id="Seg_3069" s="T2947">работать-PST.[3SG]</ta>
            <ta e="T2949" id="Seg_3070" s="T2948">весь</ta>
            <ta e="T2950" id="Seg_3071" s="T2949">что.[NOM.SG]</ta>
            <ta e="T2951" id="Seg_3072" s="T2950">работать-PRS.[3SG]</ta>
            <ta e="T2952" id="Seg_3073" s="T2951">NEG</ta>
            <ta e="T2953" id="Seg_3074" s="T2952">этот-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T2954" id="Seg_3075" s="T2953">NEG</ta>
            <ta e="T2955" id="Seg_3076" s="T2954">мочь-PRS.[3SG]</ta>
            <ta e="T2956" id="Seg_3077" s="T2955">всегда</ta>
            <ta e="T2957" id="Seg_3078" s="T2956">ругать-FRQ-DUR.[3SG]</ta>
            <ta e="T2958" id="Seg_3079" s="T2957">женщина.[NOM.SG]</ta>
            <ta e="T2959" id="Seg_3080" s="T2958">тогда</ta>
            <ta e="T2960" id="Seg_3081" s="T2959">сказать-IPFVZ.[3SG]</ta>
            <ta e="T2961" id="Seg_3082" s="T2960">пойти-EP-IMP.2SG</ta>
            <ta e="T2962" id="Seg_3083" s="T2961">я.LAT</ta>
            <ta e="T2963" id="Seg_3084" s="T2962">делать-IMP.2SG</ta>
            <ta e="T2964" id="Seg_3085" s="T2963">много</ta>
            <ta e="T2965" id="Seg_3086" s="T2964">холст.[NOM.SG]</ta>
            <ta e="T2966" id="Seg_3087" s="T2965">этот.[NOM.SG]</ta>
            <ta e="T2967" id="Seg_3088" s="T2966">пойти-PST.[3SG]</ta>
            <ta e="T2968" id="Seg_3089" s="T2967">корова-LAT</ta>
            <ta e="T2969" id="Seg_3090" s="T2968">и</ta>
            <ta e="T2970" id="Seg_3091" s="T2969">PTCL</ta>
            <ta e="T2971" id="Seg_3092" s="T2970">плакать-DUR.[3SG]</ta>
            <ta e="T2973" id="Seg_3093" s="T2972">я.ACC</ta>
            <ta e="T2974" id="Seg_3094" s="T2973">сильно</ta>
            <ta e="T2975" id="Seg_3095" s="T2974">бить-DUR-3PL</ta>
            <ta e="T2976" id="Seg_3096" s="T2975">этот-PL-LAT</ta>
            <ta e="T2977" id="Seg_3097" s="T2976">что.[NOM.SG]=INDEF</ta>
            <ta e="T2978" id="Seg_3098" s="T2977">делать-INF.LAT</ta>
            <ta e="T2979" id="Seg_3099" s="T2978">NEG</ta>
            <ta e="T2981" id="Seg_3100" s="T2980">мочь-PRS-1SG</ta>
            <ta e="T2982" id="Seg_3101" s="T2981">и</ta>
            <ta e="T2983" id="Seg_3102" s="T2982">корова.[NOM.SG]</ta>
            <ta e="T2984" id="Seg_3103" s="T2983">сказать-IPFVZ.[3SG]</ta>
            <ta e="T2985" id="Seg_3104" s="T2984">один.[NOM.SG]</ta>
            <ta e="T2986" id="Seg_3105" s="T2985">ухо-NOM/GEN/ACC.3SG</ta>
            <ta e="T2987" id="Seg_3106" s="T2986">ползти-IMP.2SG</ta>
            <ta e="T2988" id="Seg_3107" s="T2987">а</ta>
            <ta e="T2989" id="Seg_3108" s="T2988">один-LAT</ta>
            <ta e="T2990" id="Seg_3109" s="T2989">уйти-IMP.2SG</ta>
            <ta e="T2991" id="Seg_3110" s="T2990">весь</ta>
            <ta e="T2992" id="Seg_3111" s="T2991">стать-FUT-3SG</ta>
            <ta e="T2993" id="Seg_3112" s="T2992">тогда</ta>
            <ta e="T2995" id="Seg_3113" s="T2994">этот-LAT</ta>
            <ta e="T2997" id="Seg_3114" s="T2996">делать-PST.[3SG]</ta>
            <ta e="T2998" id="Seg_3115" s="T2997">и</ta>
            <ta e="T2999" id="Seg_3116" s="T2998">прясть-PST.[3SG]</ta>
            <ta e="T3000" id="Seg_3117" s="T2999">холст.[NOM.SG]</ta>
            <ta e="T3001" id="Seg_3118" s="T3000">делать-PST.[3SG]</ta>
            <ta e="T3002" id="Seg_3119" s="T3001">этот</ta>
            <ta e="T3003" id="Seg_3120" s="T3002">тогда</ta>
            <ta e="T3004" id="Seg_3121" s="T3003">принести-PST.[3SG]</ta>
            <ta e="T3005" id="Seg_3122" s="T3004">тогда</ta>
            <ta e="T3006" id="Seg_3123" s="T3005">опять</ta>
            <ta e="T3007" id="Seg_3124" s="T3006">много</ta>
            <ta e="T3008" id="Seg_3125" s="T3007">стать-PST.[3SG]</ta>
            <ta e="T3009" id="Seg_3126" s="T3008">этот.[NOM.SG]</ta>
            <ta e="T3010" id="Seg_3127" s="T3009">опять</ta>
            <ta e="T3011" id="Seg_3128" s="T3010">пойти-PST.[3SG]</ta>
            <ta e="T3012" id="Seg_3129" s="T3011">корова-LAT</ta>
            <ta e="T3013" id="Seg_3130" s="T3012">опять</ta>
            <ta e="T3014" id="Seg_3131" s="T3013">делать-PST.[3SG]</ta>
            <ta e="T3015" id="Seg_3132" s="T3014">тогда</ta>
            <ta e="T3016" id="Seg_3133" s="T3015">сказать-IPFVZ.[3SG]</ta>
            <ta e="T3017" id="Seg_3134" s="T3016">один.[NOM.SG]</ta>
            <ta e="T3018" id="Seg_3135" s="T3017">дочь-LAT/LOC.3SG</ta>
            <ta e="T3019" id="Seg_3136" s="T3018">один.[NOM.SG]</ta>
            <ta e="T3020" id="Seg_3137" s="T3019">глаз-LAT/LOC.3SG</ta>
            <ta e="T3021" id="Seg_3138" s="T3020">пойти-EP-IMP.2SG</ta>
            <ta e="T3022" id="Seg_3139" s="T3021">этот-INS</ta>
            <ta e="T3023" id="Seg_3140" s="T3022">этот.[NOM.SG]</ta>
            <ta e="T3024" id="Seg_3141" s="T3023">пойти-PST.[3SG]</ta>
            <ta e="T3025" id="Seg_3142" s="T3024">там</ta>
            <ta e="T3026" id="Seg_3143" s="T3025">лежать-PST.[3SG]</ta>
            <ta e="T3027" id="Seg_3144" s="T3026">спать-INF.LAT</ta>
            <ta e="T3028" id="Seg_3145" s="T3027">этот.[NOM.SG]</ta>
            <ta e="T3029" id="Seg_3146" s="T3028">PTCL</ta>
            <ta e="T3030" id="Seg_3147" s="T3029">спать-IMP.2SG.O</ta>
            <ta e="T3031" id="Seg_3148" s="T3030">глаз.[NOM.SG]</ta>
            <ta e="T3032" id="Seg_3149" s="T3031">спать-IMP.2SG.O</ta>
            <ta e="T3033" id="Seg_3150" s="T3032">глаз.[NOM.SG]</ta>
            <ta e="T3034" id="Seg_3151" s="T3033">глаз-NOM/GEN.3SG</ta>
            <ta e="T3035" id="Seg_3152" s="T3034">спать-MOM-PST.[3SG]</ta>
            <ta e="T3036" id="Seg_3153" s="T3035">этот-LAT</ta>
            <ta e="T3037" id="Seg_3154" s="T3036">опять</ta>
            <ta e="T3038" id="Seg_3155" s="T3037">корова.[NOM.SG]</ta>
            <ta e="T3039" id="Seg_3156" s="T3038">прясть-MOM-PST.[3SG]</ta>
            <ta e="T3040" id="Seg_3157" s="T3039">этот.[NOM.SG]</ta>
            <ta e="T3041" id="Seg_3158" s="T3040">взять-PST.[3SG]</ta>
            <ta e="T3042" id="Seg_3159" s="T3041">рулон-PL</ta>
            <ta e="T3043" id="Seg_3160" s="T3042">холст-PL</ta>
            <ta e="T3044" id="Seg_3161" s="T3043">принести-PST.[3SG]</ta>
            <ta e="T3045" id="Seg_3162" s="T3044">этот.[NOM.SG]</ta>
            <ta e="T3046" id="Seg_3163" s="T3045">женщина-LAT</ta>
            <ta e="T3047" id="Seg_3164" s="T3046">дать-PST.[3SG]</ta>
            <ta e="T3048" id="Seg_3165" s="T3047">тогда</ta>
            <ta e="T3049" id="Seg_3166" s="T3048">этот.[NOM.SG]</ta>
            <ta e="T3050" id="Seg_3167" s="T3049">женщина.[NOM.SG]</ta>
            <ta e="T3051" id="Seg_3168" s="T3050">другой.[NOM.SG]</ta>
            <ta e="T3052" id="Seg_3169" s="T3051">дочь-ACC.3SG</ta>
            <ta e="T3053" id="Seg_3170" s="T3052">пускать-DUR.[3SG]</ta>
            <ta e="T3054" id="Seg_3171" s="T3053">пойти-EP-IMP.2SG</ta>
            <ta e="T3055" id="Seg_3172" s="T3054">что.[NOM.SG]</ta>
            <ta e="T3056" id="Seg_3173" s="T3055">этот.[NOM.SG]</ta>
            <ta e="T3057" id="Seg_3174" s="T3056">там</ta>
            <ta e="T3058" id="Seg_3175" s="T3057">делать-PRS.[3SG]</ta>
            <ta e="T3059" id="Seg_3176" s="T3058">этот.[NOM.SG]</ta>
            <ta e="T3060" id="Seg_3177" s="T3059">девушка.[NOM.SG]</ta>
            <ta e="T3061" id="Seg_3178" s="T3060">пойти-PST.[3SG]</ta>
            <ta e="T3062" id="Seg_3179" s="T3061">там</ta>
            <ta e="T3063" id="Seg_3180" s="T3062">лежать-PST.[3SG]</ta>
            <ta e="T3064" id="Seg_3181" s="T3063">этот.[NOM.SG]</ta>
            <ta e="T3065" id="Seg_3182" s="T3064">PTCL</ta>
            <ta e="T3066" id="Seg_3183" s="T3065">этот-ACC</ta>
            <ta e="T3067" id="Seg_3184" s="T3066">спать-MOM-PST.[3SG]</ta>
            <ta e="T3069" id="Seg_3185" s="T3068">спать-EP-IMP.2SG</ta>
            <ta e="T3070" id="Seg_3186" s="T3069">глаз.[NOM.SG]</ta>
            <ta e="T3071" id="Seg_3187" s="T3070">спать-EP-IMP.2SG</ta>
            <ta e="T3072" id="Seg_3188" s="T3071">другой.[NOM.SG]</ta>
            <ta e="T3073" id="Seg_3189" s="T3072">глаз.[NOM.SG]</ta>
            <ta e="T3074" id="Seg_3190" s="T3073">глаз-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T3075" id="Seg_3191" s="T3074">спать-MOM-PST.[3SG]</ta>
            <ta e="T3076" id="Seg_3192" s="T3075">корова.[NOM.SG]</ta>
            <ta e="T3077" id="Seg_3193" s="T3076">этот-LAT</ta>
            <ta e="T3078" id="Seg_3194" s="T3077">делать-PST.[3SG]</ta>
            <ta e="T3079" id="Seg_3195" s="T3078">холст.[NOM.SG]</ta>
            <ta e="T3080" id="Seg_3196" s="T3079">прясть-PST.[3SG]</ta>
            <ta e="T3081" id="Seg_3197" s="T3080">и</ta>
            <ta e="T3082" id="Seg_3198" s="T3081">рулон-PL</ta>
            <ta e="T3083" id="Seg_3199" s="T3082">дать-PST.[3SG]</ta>
            <ta e="T3084" id="Seg_3200" s="T3083">тогда</ta>
            <ta e="T3085" id="Seg_3201" s="T3084">этот.[NOM.SG]</ta>
            <ta e="T3086" id="Seg_3202" s="T3085">прийти-PST.[3SG]</ta>
            <ta e="T3087" id="Seg_3203" s="T3086">этот.[NOM.SG]</ta>
            <ta e="T3088" id="Seg_3204" s="T3087">еще</ta>
            <ta e="T3089" id="Seg_3205" s="T3088">сильно</ta>
            <ta e="T3090" id="Seg_3206" s="T3089">сердиться-FRQ-MOM-PST.[3SG]</ta>
            <ta e="T3091" id="Seg_3207" s="T3090">тогда</ta>
            <ta e="T3092" id="Seg_3208" s="T3091">три.[NOM.SG]</ta>
            <ta e="T3093" id="Seg_3209" s="T3092">дочь-ACC.3SG</ta>
            <ta e="T3094" id="Seg_3210" s="T3093">послать-MOM-PST.[3SG]</ta>
            <ta e="T3095" id="Seg_3211" s="T3094">три.[NOM.SG]</ta>
            <ta e="T3096" id="Seg_3212" s="T3095">глаз-3SG-INS</ta>
            <ta e="T3097" id="Seg_3213" s="T3096">этот.[NOM.SG]</ta>
            <ta e="T3098" id="Seg_3214" s="T3097">девушка.[NOM.SG]</ta>
            <ta e="T3099" id="Seg_3215" s="T3098">пойти-PST.[3SG]</ta>
            <ta e="T3100" id="Seg_3216" s="T3099">и</ta>
            <ta e="T3101" id="Seg_3217" s="T3100">лежать-PST.[3SG]</ta>
            <ta e="T3102" id="Seg_3218" s="T3101">спать-DUR.[3SG]</ta>
            <ta e="T3104" id="Seg_3219" s="T3103">Хаврошечка.[NOM.SG]</ta>
            <ta e="T3105" id="Seg_3220" s="T3104">тоже</ta>
            <ta e="T3106" id="Seg_3221" s="T3105">этот-ACC</ta>
            <ta e="T3107" id="Seg_3222" s="T3106">песня.[NOM.SG]</ta>
            <ta e="T3108" id="Seg_3223" s="T3107">петь-DUR.[3SG]</ta>
            <ta e="T3109" id="Seg_3224" s="T3108">спать-EP-IMP.2SG</ta>
            <ta e="T3110" id="Seg_3225" s="T3109">глаз.[NOM.SG]</ta>
            <ta e="T3111" id="Seg_3226" s="T3110">спать-EP-IMP.2SG</ta>
            <ta e="T3112" id="Seg_3227" s="T3111">два.[NOM.SG]</ta>
            <ta e="T3113" id="Seg_3228" s="T3112">а</ta>
            <ta e="T3114" id="Seg_3229" s="T3113">три.[NOM.SG]</ta>
            <ta e="T3115" id="Seg_3230" s="T3114">глаз-ABL.3SG</ta>
            <ta e="T3116" id="Seg_3231" s="T3115">забыть-MOM-PST.[3SG]</ta>
            <ta e="T3117" id="Seg_3232" s="T3116">тогда</ta>
            <ta e="T3118" id="Seg_3233" s="T3117">два.[NOM.SG]</ta>
            <ta e="T3119" id="Seg_3234" s="T3118">глаз-NOM/GEN.3SG</ta>
            <ta e="T3120" id="Seg_3235" s="T3119">спать-MOM-PST-3PL</ta>
            <ta e="T3121" id="Seg_3236" s="T3120">а</ta>
            <ta e="T3122" id="Seg_3237" s="T3121">один.[NOM.SG]</ta>
            <ta e="T3123" id="Seg_3238" s="T3122">глаз-NOM/GEN.3SG</ta>
            <ta e="T3124" id="Seg_3239" s="T3123">PTCL</ta>
            <ta e="T3125" id="Seg_3240" s="T3124">видеть-PST.[3SG]</ta>
            <ta e="T3126" id="Seg_3241" s="T3125">тогда</ta>
            <ta e="T3127" id="Seg_3242" s="T3126">прийти-PST.[3SG]</ta>
            <ta e="T3128" id="Seg_3243" s="T3127">чум-LAT/LOC.3SG</ta>
            <ta e="T3129" id="Seg_3244" s="T3128">сказать-IPFVZ.[3SG]</ta>
            <ta e="T3130" id="Seg_3245" s="T3129">корова.[NOM.SG]</ta>
            <ta e="T3131" id="Seg_3246" s="T3130">этот-LAT</ta>
            <ta e="T3132" id="Seg_3247" s="T3131">делать-PRS.[3SG]</ta>
            <ta e="T3133" id="Seg_3248" s="T3132">тогда</ta>
            <ta e="T3134" id="Seg_3249" s="T3133">утро-GEN</ta>
            <ta e="T3135" id="Seg_3250" s="T3134">встать-PST.[3SG]</ta>
            <ta e="T3136" id="Seg_3251" s="T3135">тогда</ta>
            <ta e="T3137" id="Seg_3252" s="T3136">утро-GEN</ta>
            <ta e="T3138" id="Seg_3253" s="T3137">встать-PST.[3SG]</ta>
            <ta e="T3140" id="Seg_3254" s="T3139">мужчина-LAT</ta>
            <ta e="T3141" id="Seg_3255" s="T3140">сказать-IPFVZ.[3SG]</ta>
            <ta e="T3142" id="Seg_3256" s="T3141">резать-IMP.2SG.O</ta>
            <ta e="T3143" id="Seg_3257" s="T3142">корова-LAT</ta>
            <ta e="T3144" id="Seg_3258" s="T3143">а</ta>
            <ta e="T3145" id="Seg_3259" s="T3144">этот.[NOM.SG]</ta>
            <ta e="T3146" id="Seg_3260" s="T3145">сказать-IPFVZ.[3SG]</ta>
            <ta e="T3147" id="Seg_3261" s="T3146">зачем</ta>
            <ta e="T3148" id="Seg_3262" s="T3147">резать-INF.LAT</ta>
            <ta e="T3149" id="Seg_3263" s="T3148">этот.[NOM.SG]</ta>
            <ta e="T3150" id="Seg_3264" s="T3149">хороший.[NOM.SG]</ta>
            <ta e="T3151" id="Seg_3265" s="T3150">корова.[NOM.SG]</ta>
            <ta e="T3152" id="Seg_3266" s="T3151">много</ta>
            <ta e="T3153" id="Seg_3267" s="T3152">молоко.[NOM.SG]</ta>
            <ta e="T3154" id="Seg_3268" s="T3153">дать-PRS.[3SG]</ta>
            <ta e="T3155" id="Seg_3269" s="T3154">а</ta>
            <ta e="T3156" id="Seg_3270" s="T3155">этот.[NOM.SG]</ta>
            <ta e="T3157" id="Seg_3271" s="T3156">девушка.[NOM.SG]</ta>
            <ta e="T3158" id="Seg_3272" s="T3157">бежать-MOM-PST.[3SG]</ta>
            <ta e="T3159" id="Seg_3273" s="T3158">ты.ACC</ta>
            <ta e="T3160" id="Seg_3274" s="T3159">резать-INF.LAT</ta>
            <ta e="T3161" id="Seg_3275" s="T3160">хотеть.3SG</ta>
            <ta e="T3162" id="Seg_3276" s="T3161">этот.[NOM.SG]</ta>
            <ta e="T3163" id="Seg_3277" s="T3162">сказать-IPFVZ.[3SG]</ta>
            <ta e="T3164" id="Seg_3278" s="T3163">NEG.AUX-IMP.2SG</ta>
            <ta e="T3165" id="Seg_3279" s="T3164">съесть-EP-CNG</ta>
            <ta e="T3166" id="Seg_3280" s="T3165">я.GEN</ta>
            <ta e="T3168" id="Seg_3281" s="T3167">мясо-NOM/GEN/ACC.1SG</ta>
            <ta e="T3169" id="Seg_3282" s="T3168">а</ta>
            <ta e="T3170" id="Seg_3283" s="T3169">кость-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T3171" id="Seg_3284" s="T3170">взять-IMP.2SG.O</ta>
            <ta e="T3172" id="Seg_3285" s="T3171">и</ta>
            <ta e="T3173" id="Seg_3286" s="T3172">пойти-EP-IMP.2SG</ta>
            <ta e="T3174" id="Seg_3287" s="T3173">место-LAT</ta>
            <ta e="T3175" id="Seg_3288" s="T3174">копать-EP-IMP.2SG</ta>
            <ta e="T3176" id="Seg_3289" s="T3175">тогда</ta>
            <ta e="T3177" id="Seg_3290" s="T3176">этот.[NOM.SG]</ta>
            <ta e="T3178" id="Seg_3291" s="T3177">NEG</ta>
            <ta e="T3179" id="Seg_3292" s="T3178">съесть-PST.[3SG]</ta>
            <ta e="T3180" id="Seg_3293" s="T3179">мясо.[NOM.SG]</ta>
            <ta e="T3181" id="Seg_3294" s="T3180">кость-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T3182" id="Seg_3295" s="T3181">собирать-IMP.2SG</ta>
            <ta e="T3183" id="Seg_3296" s="T3182">тогда</ta>
            <ta e="T3184" id="Seg_3297" s="T3183">мужчина-NOM/GEN.3SG</ta>
            <ta e="T3185" id="Seg_3298" s="T3184">этот-ACC</ta>
            <ta e="T3186" id="Seg_3299" s="T3185">резать-MOM-PST.[3SG]</ta>
            <ta e="T3187" id="Seg_3300" s="T3186">мясо-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T3188" id="Seg_3301" s="T3187">съесть-PST-3PL</ta>
            <ta e="T3189" id="Seg_3302" s="T3188">а</ta>
            <ta e="T3190" id="Seg_3303" s="T3189">этот.[NOM.SG]</ta>
            <ta e="T3191" id="Seg_3304" s="T3190">Хаврошечка.[NOM.SG]</ta>
            <ta e="T3192" id="Seg_3305" s="T3191">кость-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T3193" id="Seg_3306" s="T3192">собрать-PST-3PL</ta>
            <ta e="T3194" id="Seg_3307" s="T3193">пойти-PST.[3SG]</ta>
            <ta e="T3195" id="Seg_3308" s="T3194">копать-PST.[3SG]</ta>
            <ta e="T3196" id="Seg_3309" s="T3195">земля-LAT</ta>
            <ta e="T3197" id="Seg_3310" s="T3196">тогда</ta>
            <ta e="T3198" id="Seg_3311" s="T3197">расти-DUR-3PL</ta>
            <ta e="T3200" id="Seg_3312" s="T3199">красивый-PL</ta>
            <ta e="T3201" id="Seg_3313" s="T3200">кто.[NOM.SG]</ta>
            <ta e="T3202" id="Seg_3314" s="T3201">NEG</ta>
            <ta e="T3203" id="Seg_3315" s="T3202">прийти-PRS.[3SG]</ta>
            <ta e="T3204" id="Seg_3316" s="T3203">стоять-DUR.[3SG]</ta>
            <ta e="T3205" id="Seg_3317" s="T3204">и</ta>
            <ta e="T3206" id="Seg_3318" s="T3205">видеть-DUR.[3SG]</ta>
            <ta e="T3207" id="Seg_3319" s="T3206">тогда</ta>
            <ta e="T3209" id="Seg_3320" s="T3208">прийти-PRS.[3SG]</ta>
            <ta e="T3210" id="Seg_3321" s="T3209">вождь.[NOM.SG]</ta>
            <ta e="T3211" id="Seg_3322" s="T3210">мужчина.[NOM.SG]</ta>
            <ta e="T3212" id="Seg_3323" s="T3211">кто.[NOM.SG]</ta>
            <ta e="T3213" id="Seg_3324" s="T3212">я.LAT</ta>
            <ta e="T3214" id="Seg_3325" s="T3213">дать-FUT-3SG</ta>
            <ta e="T3215" id="Seg_3326" s="T3214">этот.[NOM.SG]</ta>
            <ta e="T3217" id="Seg_3327" s="T3216">этот-ACC</ta>
            <ta e="T3218" id="Seg_3328" s="T3217">мужчина-LAT</ta>
            <ta e="T3219" id="Seg_3329" s="T3218">взять-FUT-1SG</ta>
            <ta e="T3220" id="Seg_3330" s="T3219">этот-PL</ta>
            <ta e="T3221" id="Seg_3331" s="T3220">NEG</ta>
            <ta e="T3222" id="Seg_3332" s="T3221">мочь-PST-3PL</ta>
            <ta e="T3223" id="Seg_3333" s="T3222">взять-INF.LAT</ta>
            <ta e="T3224" id="Seg_3334" s="T3223">вверх</ta>
            <ta e="T3225" id="Seg_3335" s="T3224">пойти-IPFVZ-PRS-3PL</ta>
            <ta e="T3226" id="Seg_3336" s="T3225">тогда</ta>
            <ta e="T3227" id="Seg_3337" s="T3226">этот.[NOM.SG]</ta>
            <ta e="T3228" id="Seg_3338" s="T3227">Хаврошечка.[NOM.SG]</ta>
            <ta e="T3229" id="Seg_3339" s="T3228">прийти-PST.[3SG]</ta>
            <ta e="T3230" id="Seg_3340" s="T3229">этот.[NOM.SG]</ta>
            <ta e="T3231" id="Seg_3341" s="T3230">этот.[NOM.SG]</ta>
            <ta e="T3232" id="Seg_3342" s="T3231">этот-LAT</ta>
            <ta e="T3235" id="Seg_3343" s="T3234">этот.[NOM.SG]</ta>
            <ta e="T3237" id="Seg_3344" s="T3236">дать-PST.[3SG]</ta>
            <ta e="T3238" id="Seg_3345" s="T3237">этот.[NOM.SG]</ta>
            <ta e="T3239" id="Seg_3346" s="T3238">этот.[NOM.SG]</ta>
            <ta e="T3240" id="Seg_3347" s="T3239">мужчина-LAT</ta>
            <ta e="T3241" id="Seg_3348" s="T3240">этот.[NOM.SG]</ta>
            <ta e="T3242" id="Seg_3349" s="T3241">этот-ACC</ta>
            <ta e="T3243" id="Seg_3350" s="T3242">мужчина-LAT</ta>
            <ta e="T3244" id="Seg_3351" s="T3243">взять-PST.[3SG]</ta>
            <ta e="T3245" id="Seg_3352" s="T3244">и</ta>
            <ta e="T3246" id="Seg_3353" s="T3245">пойти-CVB</ta>
            <ta e="T3247" id="Seg_3354" s="T3246">исчезнуть-PST.[3SG]</ta>
            <ta e="T3248" id="Seg_3355" s="T3247">этот-PL-ABL</ta>
            <ta e="T3249" id="Seg_3356" s="T3248">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2919" id="Seg_3357" s="T2918">v-v:tense-v:pn</ta>
            <ta e="T2920" id="Seg_3358" s="T2919">num-n:case</ta>
            <ta e="T2921" id="Seg_3359" s="T2920">n-n:case</ta>
            <ta e="T2923" id="Seg_3360" s="T2922">dempro-n:case</ta>
            <ta e="T2924" id="Seg_3361" s="T2923">v-v&gt;v-v:pn</ta>
            <ta e="T2925" id="Seg_3362" s="T2924">propr-n:case</ta>
            <ta e="T2926" id="Seg_3363" s="T2925">dempro-n:case</ta>
            <ta e="T2927" id="Seg_3364" s="T2926">n-n:case.poss</ta>
            <ta e="T2928" id="Seg_3365" s="T2927">n-n:case.poss</ta>
            <ta e="T2929" id="Seg_3366" s="T2928">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2930" id="Seg_3367" s="T2929">n-n:case</ta>
            <ta e="T2931" id="Seg_3368" s="T2930">ptcl</ta>
            <ta e="T2932" id="Seg_3369" s="T2931">adj-n:num</ta>
            <ta e="T2933" id="Seg_3370" s="T2932">ptcl</ta>
            <ta e="T2934" id="Seg_3371" s="T2933">dempro-n:case</ta>
            <ta e="T2935" id="Seg_3372" s="T2934">dempro-n:case</ta>
            <ta e="T2936" id="Seg_3373" s="T2935">v-v:tense-v:pn</ta>
            <ta e="T2937" id="Seg_3374" s="T2936">dempro-n:case</ta>
            <ta e="T2938" id="Seg_3375" s="T2937">num-n:case</ta>
            <ta e="T2939" id="Seg_3376" s="T2938">n-n:case.poss</ta>
            <ta e="T2940" id="Seg_3377" s="T2939">v-v:tense-v:pn</ta>
            <ta e="T2941" id="Seg_3378" s="T2940">que-n:case=ptcl</ta>
            <ta e="T2942" id="Seg_3379" s="T2941">ptcl</ta>
            <ta e="T2943" id="Seg_3380" s="T2942">v-v:tense-v:pn</ta>
            <ta e="T2944" id="Seg_3381" s="T2943">conj</ta>
            <ta e="T2945" id="Seg_3382" s="T2944">dempro-n:case</ta>
            <ta e="T2946" id="Seg_3383" s="T2945">propr-n:case</ta>
            <ta e="T2947" id="Seg_3384" s="T2946">quant</ta>
            <ta e="T2948" id="Seg_3385" s="T2947">v-v:tense-v:pn</ta>
            <ta e="T2949" id="Seg_3386" s="T2948">quant</ta>
            <ta e="T2950" id="Seg_3387" s="T2949">que-n:case</ta>
            <ta e="T2951" id="Seg_3388" s="T2950">v-v:tense-v:pn</ta>
            <ta e="T2952" id="Seg_3389" s="T2951">ptcl</ta>
            <ta e="T2953" id="Seg_3390" s="T2952">dempro-n:num-n:case.poss</ta>
            <ta e="T2954" id="Seg_3391" s="T2953">ptcl</ta>
            <ta e="T2955" id="Seg_3392" s="T2954">v-v:tense-v:pn</ta>
            <ta e="T2956" id="Seg_3393" s="T2955">adv</ta>
            <ta e="T2957" id="Seg_3394" s="T2956">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T2958" id="Seg_3395" s="T2957">n-n:case</ta>
            <ta e="T2959" id="Seg_3396" s="T2958">adv</ta>
            <ta e="T2960" id="Seg_3397" s="T2959">v-v&gt;v-v:pn</ta>
            <ta e="T2961" id="Seg_3398" s="T2960">v-v:ins-v:mood.pn</ta>
            <ta e="T2962" id="Seg_3399" s="T2961">pers</ta>
            <ta e="T2963" id="Seg_3400" s="T2962">v-v:mood.pn</ta>
            <ta e="T2964" id="Seg_3401" s="T2963">quant</ta>
            <ta e="T2965" id="Seg_3402" s="T2964">n-n:case</ta>
            <ta e="T2966" id="Seg_3403" s="T2965">dempro-n:case</ta>
            <ta e="T2967" id="Seg_3404" s="T2966">v-v:tense-v:pn</ta>
            <ta e="T2968" id="Seg_3405" s="T2967">n-n:case</ta>
            <ta e="T2969" id="Seg_3406" s="T2968">conj</ta>
            <ta e="T2970" id="Seg_3407" s="T2969">ptcl</ta>
            <ta e="T2971" id="Seg_3408" s="T2970">v-v&gt;v-v:pn</ta>
            <ta e="T2973" id="Seg_3409" s="T2972">pers</ta>
            <ta e="T2974" id="Seg_3410" s="T2973">adv</ta>
            <ta e="T2975" id="Seg_3411" s="T2974">v-v&gt;v-v:pn</ta>
            <ta e="T2976" id="Seg_3412" s="T2975">dempro-n:num-n:case</ta>
            <ta e="T2977" id="Seg_3413" s="T2976">que-n:case=ptcl</ta>
            <ta e="T2978" id="Seg_3414" s="T2977">v-v:n.fin</ta>
            <ta e="T2979" id="Seg_3415" s="T2978">ptcl</ta>
            <ta e="T2981" id="Seg_3416" s="T2980">v-v:tense-v:pn</ta>
            <ta e="T2982" id="Seg_3417" s="T2981">conj</ta>
            <ta e="T2983" id="Seg_3418" s="T2982">n-n:case</ta>
            <ta e="T2984" id="Seg_3419" s="T2983">v-v&gt;v-v:pn</ta>
            <ta e="T2985" id="Seg_3420" s="T2984">num-n:case</ta>
            <ta e="T2986" id="Seg_3421" s="T2985">n-n:case.poss</ta>
            <ta e="T2987" id="Seg_3422" s="T2986">v-v:mood.pn</ta>
            <ta e="T2988" id="Seg_3423" s="T2987">conj</ta>
            <ta e="T2989" id="Seg_3424" s="T2988">num-n:case</ta>
            <ta e="T2990" id="Seg_3425" s="T2989">v-v:mood.pn</ta>
            <ta e="T2991" id="Seg_3426" s="T2990">quant</ta>
            <ta e="T2992" id="Seg_3427" s="T2991">v-v:tense-v:pn</ta>
            <ta e="T2993" id="Seg_3428" s="T2992">adv</ta>
            <ta e="T2995" id="Seg_3429" s="T2994">dempro-n:case</ta>
            <ta e="T2997" id="Seg_3430" s="T2996">v-v:tense-v:pn</ta>
            <ta e="T2998" id="Seg_3431" s="T2997">conj</ta>
            <ta e="T2999" id="Seg_3432" s="T2998">v-v:tense-v:pn</ta>
            <ta e="T3000" id="Seg_3433" s="T2999">n-n:case</ta>
            <ta e="T3001" id="Seg_3434" s="T3000">v-v:tense-v:pn</ta>
            <ta e="T3002" id="Seg_3435" s="T3001">dempro</ta>
            <ta e="T3003" id="Seg_3436" s="T3002">adv</ta>
            <ta e="T3004" id="Seg_3437" s="T3003">v-v:tense-v:pn</ta>
            <ta e="T3005" id="Seg_3438" s="T3004">adv</ta>
            <ta e="T3006" id="Seg_3439" s="T3005">adv</ta>
            <ta e="T3007" id="Seg_3440" s="T3006">quant</ta>
            <ta e="T3008" id="Seg_3441" s="T3007">v-v:tense-v:pn</ta>
            <ta e="T3009" id="Seg_3442" s="T3008">dempro-n:case</ta>
            <ta e="T3010" id="Seg_3443" s="T3009">adv</ta>
            <ta e="T3011" id="Seg_3444" s="T3010">v-v:tense-v:pn</ta>
            <ta e="T3012" id="Seg_3445" s="T3011">n-n:case</ta>
            <ta e="T3013" id="Seg_3446" s="T3012">adv</ta>
            <ta e="T3014" id="Seg_3447" s="T3013">v-v:tense-v:pn</ta>
            <ta e="T3015" id="Seg_3448" s="T3014">adv</ta>
            <ta e="T3016" id="Seg_3449" s="T3015">v-v&gt;v-v:pn</ta>
            <ta e="T3017" id="Seg_3450" s="T3016">num-n:case</ta>
            <ta e="T3018" id="Seg_3451" s="T3017">n-n:case.poss</ta>
            <ta e="T3019" id="Seg_3452" s="T3018">num-n:case</ta>
            <ta e="T3020" id="Seg_3453" s="T3019">n-n:case.poss</ta>
            <ta e="T3021" id="Seg_3454" s="T3020">v-v:ins-v:mood.pn</ta>
            <ta e="T3022" id="Seg_3455" s="T3021">dempro-n:case</ta>
            <ta e="T3023" id="Seg_3456" s="T3022">dempro-n:case</ta>
            <ta e="T3024" id="Seg_3457" s="T3023">v-v:tense-v:pn</ta>
            <ta e="T3025" id="Seg_3458" s="T3024">adv</ta>
            <ta e="T3026" id="Seg_3459" s="T3025">v-v:tense-v:pn</ta>
            <ta e="T3027" id="Seg_3460" s="T3026">v-v:n.fin</ta>
            <ta e="T3028" id="Seg_3461" s="T3027">dempro-n:case</ta>
            <ta e="T3029" id="Seg_3462" s="T3028">ptcl</ta>
            <ta e="T3030" id="Seg_3463" s="T3029">v-v:mood.pn</ta>
            <ta e="T3031" id="Seg_3464" s="T3030">n-n:case</ta>
            <ta e="T3032" id="Seg_3465" s="T3031">v-v:mood.pn</ta>
            <ta e="T3033" id="Seg_3466" s="T3032">n-n:case</ta>
            <ta e="T3034" id="Seg_3467" s="T3033">n-n:case.poss</ta>
            <ta e="T3035" id="Seg_3468" s="T3034">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T3036" id="Seg_3469" s="T3035">dempro-n:case</ta>
            <ta e="T3037" id="Seg_3470" s="T3036">adv</ta>
            <ta e="T3038" id="Seg_3471" s="T3037">n-n:case</ta>
            <ta e="T3039" id="Seg_3472" s="T3038">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T3040" id="Seg_3473" s="T3039">dempro-n:case</ta>
            <ta e="T3041" id="Seg_3474" s="T3040">v-v:tense-v:pn</ta>
            <ta e="T3042" id="Seg_3475" s="T3041">n-n:num</ta>
            <ta e="T3043" id="Seg_3476" s="T3042">n-n:num</ta>
            <ta e="T3044" id="Seg_3477" s="T3043">v-v:tense-v:pn</ta>
            <ta e="T3045" id="Seg_3478" s="T3044">dempro-n:case</ta>
            <ta e="T3046" id="Seg_3479" s="T3045">n-n:case</ta>
            <ta e="T3047" id="Seg_3480" s="T3046">v-v:tense-v:pn</ta>
            <ta e="T3048" id="Seg_3481" s="T3047">adv</ta>
            <ta e="T3049" id="Seg_3482" s="T3048">dempro-n:case</ta>
            <ta e="T3050" id="Seg_3483" s="T3049">n-n:case</ta>
            <ta e="T3051" id="Seg_3484" s="T3050">adj-n:case</ta>
            <ta e="T3052" id="Seg_3485" s="T3051">n-n:case.poss</ta>
            <ta e="T3053" id="Seg_3486" s="T3052">v-v&gt;v-v:pn</ta>
            <ta e="T3054" id="Seg_3487" s="T3053">v-v:ins-v:mood.pn</ta>
            <ta e="T3055" id="Seg_3488" s="T3054">que-n:case</ta>
            <ta e="T3056" id="Seg_3489" s="T3055">dempro-n:case</ta>
            <ta e="T3057" id="Seg_3490" s="T3056">adv</ta>
            <ta e="T3058" id="Seg_3491" s="T3057">v-v:tense-v:pn</ta>
            <ta e="T3059" id="Seg_3492" s="T3058">dempro-n:case</ta>
            <ta e="T3060" id="Seg_3493" s="T3059">n-n:case</ta>
            <ta e="T3061" id="Seg_3494" s="T3060">v-v:tense-v:pn</ta>
            <ta e="T3062" id="Seg_3495" s="T3061">adv</ta>
            <ta e="T3063" id="Seg_3496" s="T3062">v-v:tense-v:pn</ta>
            <ta e="T3064" id="Seg_3497" s="T3063">dempro-n:case</ta>
            <ta e="T3065" id="Seg_3498" s="T3064">ptcl</ta>
            <ta e="T3066" id="Seg_3499" s="T3065">dempro-n:case</ta>
            <ta e="T3067" id="Seg_3500" s="T3066">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T3069" id="Seg_3501" s="T3068">v-v:ins-v:mood.pn</ta>
            <ta e="T3070" id="Seg_3502" s="T3069">n-n:case</ta>
            <ta e="T3071" id="Seg_3503" s="T3070">v-v:ins-v:mood.pn</ta>
            <ta e="T3072" id="Seg_3504" s="T3071">adj-n:case</ta>
            <ta e="T3073" id="Seg_3505" s="T3072">n-n:case</ta>
            <ta e="T3074" id="Seg_3506" s="T3073">n-n:num-n:case.poss</ta>
            <ta e="T3075" id="Seg_3507" s="T3074">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T3076" id="Seg_3508" s="T3075">n-n:case</ta>
            <ta e="T3077" id="Seg_3509" s="T3076">dempro-n:case</ta>
            <ta e="T3078" id="Seg_3510" s="T3077">v-v:tense-v:pn</ta>
            <ta e="T3079" id="Seg_3511" s="T3078">n-n:case</ta>
            <ta e="T3080" id="Seg_3512" s="T3079">v-v:tense-v:pn</ta>
            <ta e="T3081" id="Seg_3513" s="T3080">conj</ta>
            <ta e="T3082" id="Seg_3514" s="T3081">n-n:pn</ta>
            <ta e="T3083" id="Seg_3515" s="T3082">v-v:tense-v:pn</ta>
            <ta e="T3084" id="Seg_3516" s="T3083">adv</ta>
            <ta e="T3085" id="Seg_3517" s="T3084">dempro-n:case</ta>
            <ta e="T3086" id="Seg_3518" s="T3085">v-v:tense-v:pn</ta>
            <ta e="T3087" id="Seg_3519" s="T3086">dempro-n:case</ta>
            <ta e="T3088" id="Seg_3520" s="T3087">adv</ta>
            <ta e="T3089" id="Seg_3521" s="T3088">adv</ta>
            <ta e="T3090" id="Seg_3522" s="T3089">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T3091" id="Seg_3523" s="T3090">adv</ta>
            <ta e="T3092" id="Seg_3524" s="T3091">num-n:case</ta>
            <ta e="T3093" id="Seg_3525" s="T3092">n-n:case.poss</ta>
            <ta e="T3094" id="Seg_3526" s="T3093">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T3095" id="Seg_3527" s="T3094">num-n:case</ta>
            <ta e="T3096" id="Seg_3528" s="T3095">n-n:case.poss-n:case</ta>
            <ta e="T3097" id="Seg_3529" s="T3096">dempro-n:case</ta>
            <ta e="T3098" id="Seg_3530" s="T3097">n-n:case</ta>
            <ta e="T3099" id="Seg_3531" s="T3098">v-v:tense-v:pn</ta>
            <ta e="T3100" id="Seg_3532" s="T3099">conj</ta>
            <ta e="T3101" id="Seg_3533" s="T3100">v-v:tense-v:pn</ta>
            <ta e="T3102" id="Seg_3534" s="T3101">v-v&gt;v-v:pn</ta>
            <ta e="T3104" id="Seg_3535" s="T3103">propr-n:case</ta>
            <ta e="T3105" id="Seg_3536" s="T3104">ptcl</ta>
            <ta e="T3106" id="Seg_3537" s="T3105">dempro-n:case</ta>
            <ta e="T3107" id="Seg_3538" s="T3106">n-n:case</ta>
            <ta e="T3108" id="Seg_3539" s="T3107">v-v&gt;v-v:pn</ta>
            <ta e="T3109" id="Seg_3540" s="T3108">v-v:ins-v:mood.pn</ta>
            <ta e="T3110" id="Seg_3541" s="T3109">n-n:case</ta>
            <ta e="T3111" id="Seg_3542" s="T3110">v-v:ins-v:mood.pn</ta>
            <ta e="T3112" id="Seg_3543" s="T3111">num-n:case</ta>
            <ta e="T3113" id="Seg_3544" s="T3112">conj</ta>
            <ta e="T3114" id="Seg_3545" s="T3113">num-n:case</ta>
            <ta e="T3115" id="Seg_3546" s="T3114">n-n:case.poss</ta>
            <ta e="T3116" id="Seg_3547" s="T3115">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T3117" id="Seg_3548" s="T3116">adv</ta>
            <ta e="T3118" id="Seg_3549" s="T3117">num-n:case</ta>
            <ta e="T3119" id="Seg_3550" s="T3118">n-n:case.poss</ta>
            <ta e="T3120" id="Seg_3551" s="T3119">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T3121" id="Seg_3552" s="T3120">conj</ta>
            <ta e="T3122" id="Seg_3553" s="T3121">num-n:case</ta>
            <ta e="T3123" id="Seg_3554" s="T3122">n-n:case.poss</ta>
            <ta e="T3124" id="Seg_3555" s="T3123">ptcl</ta>
            <ta e="T3125" id="Seg_3556" s="T3124">v-v:tense-v:pn</ta>
            <ta e="T3126" id="Seg_3557" s="T3125">adv</ta>
            <ta e="T3127" id="Seg_3558" s="T3126">v-v:tense-v:pn</ta>
            <ta e="T3128" id="Seg_3559" s="T3127">n-n:case.poss</ta>
            <ta e="T3129" id="Seg_3560" s="T3128">v-v&gt;v-v:pn</ta>
            <ta e="T3130" id="Seg_3561" s="T3129">n-n:case</ta>
            <ta e="T3131" id="Seg_3562" s="T3130">dempro-n:case</ta>
            <ta e="T3132" id="Seg_3563" s="T3131">v-v:tense-v:pn</ta>
            <ta e="T3133" id="Seg_3564" s="T3132">adv</ta>
            <ta e="T3134" id="Seg_3565" s="T3133">n-n:case</ta>
            <ta e="T3135" id="Seg_3566" s="T3134">v-v:tense-v:pn</ta>
            <ta e="T3136" id="Seg_3567" s="T3135">adv</ta>
            <ta e="T3137" id="Seg_3568" s="T3136">n-n:case</ta>
            <ta e="T3138" id="Seg_3569" s="T3137">v-v:tense-v:pn</ta>
            <ta e="T3140" id="Seg_3570" s="T3139">n-n:case</ta>
            <ta e="T3141" id="Seg_3571" s="T3140">v-v&gt;v-v:pn</ta>
            <ta e="T3142" id="Seg_3572" s="T3141">v-v:mood.pn</ta>
            <ta e="T3143" id="Seg_3573" s="T3142">n-n:case</ta>
            <ta e="T3144" id="Seg_3574" s="T3143">conj</ta>
            <ta e="T3145" id="Seg_3575" s="T3144">dempro-n:case</ta>
            <ta e="T3146" id="Seg_3576" s="T3145">v-v&gt;v-v:pn</ta>
            <ta e="T3147" id="Seg_3577" s="T3146">adv</ta>
            <ta e="T3148" id="Seg_3578" s="T3147">v-v:n.fin</ta>
            <ta e="T3149" id="Seg_3579" s="T3148">dempro-n:case</ta>
            <ta e="T3150" id="Seg_3580" s="T3149">adj-n:case</ta>
            <ta e="T3151" id="Seg_3581" s="T3150">n-n:case</ta>
            <ta e="T3152" id="Seg_3582" s="T3151">quant</ta>
            <ta e="T3153" id="Seg_3583" s="T3152">n.[n:case]</ta>
            <ta e="T3154" id="Seg_3584" s="T3153">v-v:tense-v:pn</ta>
            <ta e="T3155" id="Seg_3585" s="T3154">conj</ta>
            <ta e="T3156" id="Seg_3586" s="T3155">dempro-n:case</ta>
            <ta e="T3157" id="Seg_3587" s="T3156">n-n:case</ta>
            <ta e="T3158" id="Seg_3588" s="T3157">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T3159" id="Seg_3589" s="T3158">pers</ta>
            <ta e="T3160" id="Seg_3590" s="T3159">v-v:n.fin</ta>
            <ta e="T3161" id="Seg_3591" s="T3160">v</ta>
            <ta e="T3162" id="Seg_3592" s="T3161">dempro-n:case</ta>
            <ta e="T3163" id="Seg_3593" s="T3162">v-v&gt;v-v:pn</ta>
            <ta e="T3164" id="Seg_3594" s="T3163">aux-v:mood.pn</ta>
            <ta e="T3165" id="Seg_3595" s="T3164">v-v:ins-v:n.fin</ta>
            <ta e="T3166" id="Seg_3596" s="T3165">pers</ta>
            <ta e="T3168" id="Seg_3597" s="T3167">n-n:case.poss</ta>
            <ta e="T3169" id="Seg_3598" s="T3168">conj</ta>
            <ta e="T3170" id="Seg_3599" s="T3169">n-n:num-n:case.poss</ta>
            <ta e="T3171" id="Seg_3600" s="T3170">v-v:mood.pn</ta>
            <ta e="T3172" id="Seg_3601" s="T3171">conj</ta>
            <ta e="T3173" id="Seg_3602" s="T3172">v-v:ins-v:mood.pn</ta>
            <ta e="T3174" id="Seg_3603" s="T3173">n-n:case</ta>
            <ta e="T3175" id="Seg_3604" s="T3174">v-v:ins-v:mood.pn</ta>
            <ta e="T3176" id="Seg_3605" s="T3175">adv</ta>
            <ta e="T3177" id="Seg_3606" s="T3176">dempro-n:case</ta>
            <ta e="T3178" id="Seg_3607" s="T3177">ptcl</ta>
            <ta e="T3179" id="Seg_3608" s="T3178">v-v:tense-v:pn</ta>
            <ta e="T3180" id="Seg_3609" s="T3179">n-n:case</ta>
            <ta e="T3181" id="Seg_3610" s="T3180">n-n:num-n:case.poss</ta>
            <ta e="T3182" id="Seg_3611" s="T3181">v-v:mood.pn</ta>
            <ta e="T3183" id="Seg_3612" s="T3182">adv</ta>
            <ta e="T3184" id="Seg_3613" s="T3183">n-n:case.poss</ta>
            <ta e="T3185" id="Seg_3614" s="T3184">dempro-n:case</ta>
            <ta e="T3186" id="Seg_3615" s="T3185">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T3187" id="Seg_3616" s="T3186">n-n:num-n:case.poss</ta>
            <ta e="T3188" id="Seg_3617" s="T3187">v-v:tense-v:pn</ta>
            <ta e="T3189" id="Seg_3618" s="T3188">conj</ta>
            <ta e="T3190" id="Seg_3619" s="T3189">dempro-n:case</ta>
            <ta e="T3191" id="Seg_3620" s="T3190">propr-n:case</ta>
            <ta e="T3192" id="Seg_3621" s="T3191">n-n:num-n:case.poss</ta>
            <ta e="T3193" id="Seg_3622" s="T3192">v-v:tense-v:pn</ta>
            <ta e="T3194" id="Seg_3623" s="T3193">v-v:tense-v:pn</ta>
            <ta e="T3195" id="Seg_3624" s="T3194">v-v:tense-v:pn</ta>
            <ta e="T3196" id="Seg_3625" s="T3195">n-n:case</ta>
            <ta e="T3197" id="Seg_3626" s="T3196">adv</ta>
            <ta e="T3198" id="Seg_3627" s="T3197">v-v&gt;v-v:pn</ta>
            <ta e="T3200" id="Seg_3628" s="T3199">adj-n:num</ta>
            <ta e="T3201" id="Seg_3629" s="T3200">que</ta>
            <ta e="T3202" id="Seg_3630" s="T3201">ptcl</ta>
            <ta e="T3203" id="Seg_3631" s="T3202">v-v:tense-v:pn</ta>
            <ta e="T3204" id="Seg_3632" s="T3203">v-v&gt;v-v:pn</ta>
            <ta e="T3205" id="Seg_3633" s="T3204">conj</ta>
            <ta e="T3206" id="Seg_3634" s="T3205">v-v&gt;v-v:pn</ta>
            <ta e="T3207" id="Seg_3635" s="T3206">adv</ta>
            <ta e="T3209" id="Seg_3636" s="T3208">v-v:tense-v:pn</ta>
            <ta e="T3210" id="Seg_3637" s="T3209">n-n:case</ta>
            <ta e="T3211" id="Seg_3638" s="T3210">n-n:case</ta>
            <ta e="T3212" id="Seg_3639" s="T3211">que-n:case</ta>
            <ta e="T3213" id="Seg_3640" s="T3212">pers</ta>
            <ta e="T3214" id="Seg_3641" s="T3213">v-v:tense-v:pn</ta>
            <ta e="T3215" id="Seg_3642" s="T3214">dempro-n:case</ta>
            <ta e="T3217" id="Seg_3643" s="T3216">dempro-n:case</ta>
            <ta e="T3218" id="Seg_3644" s="T3217">n-n:case</ta>
            <ta e="T3219" id="Seg_3645" s="T3218">v-v:tense-v:pn</ta>
            <ta e="T3220" id="Seg_3646" s="T3219">dempro-n:num</ta>
            <ta e="T3221" id="Seg_3647" s="T3220">ptcl</ta>
            <ta e="T3222" id="Seg_3648" s="T3221">v-v:tense-v:pn</ta>
            <ta e="T3223" id="Seg_3649" s="T3222">v-v:n.fin</ta>
            <ta e="T3224" id="Seg_3650" s="T3223">adv</ta>
            <ta e="T3225" id="Seg_3651" s="T3224">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T3226" id="Seg_3652" s="T3225">adv</ta>
            <ta e="T3227" id="Seg_3653" s="T3226">dempro-n:case</ta>
            <ta e="T3228" id="Seg_3654" s="T3227">propr-n:case</ta>
            <ta e="T3229" id="Seg_3655" s="T3228">v-v:tense-v:pn</ta>
            <ta e="T3230" id="Seg_3656" s="T3229">dempro-n:case</ta>
            <ta e="T3231" id="Seg_3657" s="T3230">dempro-n:case</ta>
            <ta e="T3232" id="Seg_3658" s="T3231">dempro-n:case</ta>
            <ta e="T3235" id="Seg_3659" s="T3234">dempro-n:case</ta>
            <ta e="T3237" id="Seg_3660" s="T3236">v-v:tense-v:pn</ta>
            <ta e="T3238" id="Seg_3661" s="T3237">dempro-n:case</ta>
            <ta e="T3239" id="Seg_3662" s="T3238">dempro-n:case</ta>
            <ta e="T3240" id="Seg_3663" s="T3239">n-n:case</ta>
            <ta e="T3241" id="Seg_3664" s="T3240">dempro-n:case</ta>
            <ta e="T3242" id="Seg_3665" s="T3241">dempro-n:case</ta>
            <ta e="T3243" id="Seg_3666" s="T3242">n-n:case</ta>
            <ta e="T3244" id="Seg_3667" s="T3243">v-v:tense-v:pn</ta>
            <ta e="T3245" id="Seg_3668" s="T3244">conj</ta>
            <ta e="T3246" id="Seg_3669" s="T3245">v-v:n-fin</ta>
            <ta e="T3247" id="Seg_3670" s="T3246">v-v:tense-v:pn</ta>
            <ta e="T3248" id="Seg_3671" s="T3247">dempro-n:num-n:case</ta>
            <ta e="T3249" id="Seg_3672" s="T3248">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2919" id="Seg_3673" s="T2918">v</ta>
            <ta e="T2920" id="Seg_3674" s="T2919">num</ta>
            <ta e="T2921" id="Seg_3675" s="T2920">n</ta>
            <ta e="T2923" id="Seg_3676" s="T2922">dempro</ta>
            <ta e="T2924" id="Seg_3677" s="T2923">v</ta>
            <ta e="T2925" id="Seg_3678" s="T2924">propr</ta>
            <ta e="T2926" id="Seg_3679" s="T2925">dempro</ta>
            <ta e="T2927" id="Seg_3680" s="T2926">n</ta>
            <ta e="T2928" id="Seg_3681" s="T2927">n</ta>
            <ta e="T2929" id="Seg_3682" s="T2928">v</ta>
            <ta e="T2930" id="Seg_3683" s="T2929">n</ta>
            <ta e="T2931" id="Seg_3684" s="T2930">ptcl</ta>
            <ta e="T2932" id="Seg_3685" s="T2931">adj</ta>
            <ta e="T2933" id="Seg_3686" s="T2932">ptcl</ta>
            <ta e="T2934" id="Seg_3687" s="T2933">dempro</ta>
            <ta e="T2935" id="Seg_3688" s="T2934">dempro</ta>
            <ta e="T2936" id="Seg_3689" s="T2935">v</ta>
            <ta e="T2937" id="Seg_3690" s="T2936">dempro</ta>
            <ta e="T2938" id="Seg_3691" s="T2937">num</ta>
            <ta e="T2939" id="Seg_3692" s="T2938">n</ta>
            <ta e="T2940" id="Seg_3693" s="T2939">v</ta>
            <ta e="T2941" id="Seg_3694" s="T2940">que</ta>
            <ta e="T2942" id="Seg_3695" s="T2941">ptcl</ta>
            <ta e="T2943" id="Seg_3696" s="T2942">v</ta>
            <ta e="T2944" id="Seg_3697" s="T2943">conj</ta>
            <ta e="T2945" id="Seg_3698" s="T2944">dempro</ta>
            <ta e="T2946" id="Seg_3699" s="T2945">propr</ta>
            <ta e="T2947" id="Seg_3700" s="T2946">quant</ta>
            <ta e="T2948" id="Seg_3701" s="T2947">v</ta>
            <ta e="T2949" id="Seg_3702" s="T2948">quant</ta>
            <ta e="T2950" id="Seg_3703" s="T2949">que</ta>
            <ta e="T2951" id="Seg_3704" s="T2950">v</ta>
            <ta e="T2952" id="Seg_3705" s="T2951">ptcl</ta>
            <ta e="T2953" id="Seg_3706" s="T2952">dempro</ta>
            <ta e="T2954" id="Seg_3707" s="T2953">ptcl</ta>
            <ta e="T2955" id="Seg_3708" s="T2954">v</ta>
            <ta e="T2956" id="Seg_3709" s="T2955">adv</ta>
            <ta e="T2957" id="Seg_3710" s="T2956">v</ta>
            <ta e="T2958" id="Seg_3711" s="T2957">n</ta>
            <ta e="T2959" id="Seg_3712" s="T2958">adv</ta>
            <ta e="T2960" id="Seg_3713" s="T2959">v</ta>
            <ta e="T2961" id="Seg_3714" s="T2960">v</ta>
            <ta e="T2962" id="Seg_3715" s="T2961">pers</ta>
            <ta e="T2963" id="Seg_3716" s="T2962">v</ta>
            <ta e="T2964" id="Seg_3717" s="T2963">quant</ta>
            <ta e="T2965" id="Seg_3718" s="T2964">n</ta>
            <ta e="T2966" id="Seg_3719" s="T2965">dempro</ta>
            <ta e="T2967" id="Seg_3720" s="T2966">v</ta>
            <ta e="T2968" id="Seg_3721" s="T2967">n</ta>
            <ta e="T2969" id="Seg_3722" s="T2968">conj</ta>
            <ta e="T2970" id="Seg_3723" s="T2969">ptcl</ta>
            <ta e="T2971" id="Seg_3724" s="T2970">v</ta>
            <ta e="T2973" id="Seg_3725" s="T2972">pers</ta>
            <ta e="T2974" id="Seg_3726" s="T2973">adv</ta>
            <ta e="T2975" id="Seg_3727" s="T2974">v</ta>
            <ta e="T2976" id="Seg_3728" s="T2975">dempro</ta>
            <ta e="T2977" id="Seg_3729" s="T2976">que</ta>
            <ta e="T2979" id="Seg_3730" s="T2978">ptcl</ta>
            <ta e="T2981" id="Seg_3731" s="T2980">v</ta>
            <ta e="T2982" id="Seg_3732" s="T2981">conj</ta>
            <ta e="T2983" id="Seg_3733" s="T2982">n</ta>
            <ta e="T2984" id="Seg_3734" s="T2983">v</ta>
            <ta e="T2985" id="Seg_3735" s="T2984">num</ta>
            <ta e="T2986" id="Seg_3736" s="T2985">n</ta>
            <ta e="T2987" id="Seg_3737" s="T2986">v</ta>
            <ta e="T2988" id="Seg_3738" s="T2987">conj</ta>
            <ta e="T2989" id="Seg_3739" s="T2988">num</ta>
            <ta e="T2990" id="Seg_3740" s="T2989">v</ta>
            <ta e="T2991" id="Seg_3741" s="T2990">quant</ta>
            <ta e="T2992" id="Seg_3742" s="T2991">v</ta>
            <ta e="T2993" id="Seg_3743" s="T2992">adv</ta>
            <ta e="T2995" id="Seg_3744" s="T2994">dempro</ta>
            <ta e="T2997" id="Seg_3745" s="T2996">v</ta>
            <ta e="T2998" id="Seg_3746" s="T2997">conj</ta>
            <ta e="T2999" id="Seg_3747" s="T2998">v</ta>
            <ta e="T3000" id="Seg_3748" s="T2999">n</ta>
            <ta e="T3001" id="Seg_3749" s="T3000">v</ta>
            <ta e="T3002" id="Seg_3750" s="T3001">dempro</ta>
            <ta e="T3003" id="Seg_3751" s="T3002">adv</ta>
            <ta e="T3004" id="Seg_3752" s="T3003">v</ta>
            <ta e="T3005" id="Seg_3753" s="T3004">adv</ta>
            <ta e="T3006" id="Seg_3754" s="T3005">adv</ta>
            <ta e="T3007" id="Seg_3755" s="T3006">quant</ta>
            <ta e="T3008" id="Seg_3756" s="T3007">v</ta>
            <ta e="T3009" id="Seg_3757" s="T3008">dempro</ta>
            <ta e="T3010" id="Seg_3758" s="T3009">adv</ta>
            <ta e="T3011" id="Seg_3759" s="T3010">v</ta>
            <ta e="T3012" id="Seg_3760" s="T3011">n</ta>
            <ta e="T3013" id="Seg_3761" s="T3012">adv</ta>
            <ta e="T3014" id="Seg_3762" s="T3013">v</ta>
            <ta e="T3015" id="Seg_3763" s="T3014">adv</ta>
            <ta e="T3016" id="Seg_3764" s="T3015">v</ta>
            <ta e="T3017" id="Seg_3765" s="T3016">num</ta>
            <ta e="T3018" id="Seg_3766" s="T3017">n</ta>
            <ta e="T3019" id="Seg_3767" s="T3018">num</ta>
            <ta e="T3020" id="Seg_3768" s="T3019">n</ta>
            <ta e="T3021" id="Seg_3769" s="T3020">v</ta>
            <ta e="T3022" id="Seg_3770" s="T3021">dempro</ta>
            <ta e="T3023" id="Seg_3771" s="T3022">dempro</ta>
            <ta e="T3024" id="Seg_3772" s="T3023">v</ta>
            <ta e="T3025" id="Seg_3773" s="T3024">adv</ta>
            <ta e="T3026" id="Seg_3774" s="T3025">v</ta>
            <ta e="T3027" id="Seg_3775" s="T3026">v</ta>
            <ta e="T3028" id="Seg_3776" s="T3027">dempro</ta>
            <ta e="T3029" id="Seg_3777" s="T3028">ptcl</ta>
            <ta e="T3030" id="Seg_3778" s="T3029">v</ta>
            <ta e="T3031" id="Seg_3779" s="T3030">n</ta>
            <ta e="T3032" id="Seg_3780" s="T3031">v</ta>
            <ta e="T3033" id="Seg_3781" s="T3032">n</ta>
            <ta e="T3034" id="Seg_3782" s="T3033">n</ta>
            <ta e="T3035" id="Seg_3783" s="T3034">v</ta>
            <ta e="T3036" id="Seg_3784" s="T3035">dempro</ta>
            <ta e="T3037" id="Seg_3785" s="T3036">adv</ta>
            <ta e="T3038" id="Seg_3786" s="T3037">n</ta>
            <ta e="T3039" id="Seg_3787" s="T3038">v</ta>
            <ta e="T3040" id="Seg_3788" s="T3039">dempro</ta>
            <ta e="T3041" id="Seg_3789" s="T3040">v</ta>
            <ta e="T3042" id="Seg_3790" s="T3041">n</ta>
            <ta e="T3043" id="Seg_3791" s="T3042">n</ta>
            <ta e="T3044" id="Seg_3792" s="T3043">v</ta>
            <ta e="T3045" id="Seg_3793" s="T3044">dempro</ta>
            <ta e="T3046" id="Seg_3794" s="T3045">n</ta>
            <ta e="T3047" id="Seg_3795" s="T3046">v</ta>
            <ta e="T3048" id="Seg_3796" s="T3047">adv</ta>
            <ta e="T3049" id="Seg_3797" s="T3048">dempro</ta>
            <ta e="T3050" id="Seg_3798" s="T3049">n</ta>
            <ta e="T3051" id="Seg_3799" s="T3050">adj</ta>
            <ta e="T3052" id="Seg_3800" s="T3051">n</ta>
            <ta e="T3053" id="Seg_3801" s="T3052">v</ta>
            <ta e="T3054" id="Seg_3802" s="T3053">v</ta>
            <ta e="T3055" id="Seg_3803" s="T3054">que</ta>
            <ta e="T3056" id="Seg_3804" s="T3055">dempro</ta>
            <ta e="T3057" id="Seg_3805" s="T3056">adv</ta>
            <ta e="T3058" id="Seg_3806" s="T3057">v</ta>
            <ta e="T3059" id="Seg_3807" s="T3058">dempro</ta>
            <ta e="T3060" id="Seg_3808" s="T3059">n</ta>
            <ta e="T3061" id="Seg_3809" s="T3060">v</ta>
            <ta e="T3062" id="Seg_3810" s="T3061">adv</ta>
            <ta e="T3063" id="Seg_3811" s="T3062">v</ta>
            <ta e="T3064" id="Seg_3812" s="T3063">dempro</ta>
            <ta e="T3065" id="Seg_3813" s="T3064">ptcl</ta>
            <ta e="T3066" id="Seg_3814" s="T3065">dempro</ta>
            <ta e="T3067" id="Seg_3815" s="T3066">v</ta>
            <ta e="T3069" id="Seg_3816" s="T3068">v</ta>
            <ta e="T3070" id="Seg_3817" s="T3069">n</ta>
            <ta e="T3071" id="Seg_3818" s="T3070">v</ta>
            <ta e="T3072" id="Seg_3819" s="T3071">adj</ta>
            <ta e="T3073" id="Seg_3820" s="T3072">n</ta>
            <ta e="T3074" id="Seg_3821" s="T3073">n</ta>
            <ta e="T3075" id="Seg_3822" s="T3074">v</ta>
            <ta e="T3076" id="Seg_3823" s="T3075">n</ta>
            <ta e="T3077" id="Seg_3824" s="T3076">dempro</ta>
            <ta e="T3078" id="Seg_3825" s="T3077">v</ta>
            <ta e="T3079" id="Seg_3826" s="T3078">n</ta>
            <ta e="T3080" id="Seg_3827" s="T3079">v</ta>
            <ta e="T3081" id="Seg_3828" s="T3080">conj</ta>
            <ta e="T3082" id="Seg_3829" s="T3081">n</ta>
            <ta e="T3083" id="Seg_3830" s="T3082">v</ta>
            <ta e="T3084" id="Seg_3831" s="T3083">adv</ta>
            <ta e="T3085" id="Seg_3832" s="T3084">dempro</ta>
            <ta e="T3086" id="Seg_3833" s="T3085">v</ta>
            <ta e="T3087" id="Seg_3834" s="T3086">dempro</ta>
            <ta e="T3088" id="Seg_3835" s="T3087">adv</ta>
            <ta e="T3089" id="Seg_3836" s="T3088">adv</ta>
            <ta e="T3090" id="Seg_3837" s="T3089">v</ta>
            <ta e="T3091" id="Seg_3838" s="T3090">adv</ta>
            <ta e="T3092" id="Seg_3839" s="T3091">num</ta>
            <ta e="T3093" id="Seg_3840" s="T3092">n</ta>
            <ta e="T3094" id="Seg_3841" s="T3093">v</ta>
            <ta e="T3095" id="Seg_3842" s="T3094">num</ta>
            <ta e="T3096" id="Seg_3843" s="T3095">n</ta>
            <ta e="T3097" id="Seg_3844" s="T3096">dempro</ta>
            <ta e="T3098" id="Seg_3845" s="T3097">n</ta>
            <ta e="T3099" id="Seg_3846" s="T3098">v</ta>
            <ta e="T3100" id="Seg_3847" s="T3099">conj</ta>
            <ta e="T3101" id="Seg_3848" s="T3100">v</ta>
            <ta e="T3102" id="Seg_3849" s="T3101">v</ta>
            <ta e="T3104" id="Seg_3850" s="T3103">propr</ta>
            <ta e="T3105" id="Seg_3851" s="T3104">ptcl</ta>
            <ta e="T3106" id="Seg_3852" s="T3105">dempro</ta>
            <ta e="T3107" id="Seg_3853" s="T3106">n</ta>
            <ta e="T3108" id="Seg_3854" s="T3107">v</ta>
            <ta e="T3109" id="Seg_3855" s="T3108">v</ta>
            <ta e="T3110" id="Seg_3856" s="T3109">n</ta>
            <ta e="T3111" id="Seg_3857" s="T3110">v</ta>
            <ta e="T3112" id="Seg_3858" s="T3111">num</ta>
            <ta e="T3113" id="Seg_3859" s="T3112">conj</ta>
            <ta e="T3114" id="Seg_3860" s="T3113">num</ta>
            <ta e="T3115" id="Seg_3861" s="T3114">n</ta>
            <ta e="T3116" id="Seg_3862" s="T3115">v</ta>
            <ta e="T3117" id="Seg_3863" s="T3116">adv</ta>
            <ta e="T3118" id="Seg_3864" s="T3117">num</ta>
            <ta e="T3119" id="Seg_3865" s="T3118">n</ta>
            <ta e="T3120" id="Seg_3866" s="T3119">v</ta>
            <ta e="T3121" id="Seg_3867" s="T3120">conj</ta>
            <ta e="T3122" id="Seg_3868" s="T3121">num</ta>
            <ta e="T3123" id="Seg_3869" s="T3122">n</ta>
            <ta e="T3124" id="Seg_3870" s="T3123">ptcl</ta>
            <ta e="T3125" id="Seg_3871" s="T3124">v</ta>
            <ta e="T3126" id="Seg_3872" s="T3125">adv</ta>
            <ta e="T3127" id="Seg_3873" s="T3126">v</ta>
            <ta e="T3128" id="Seg_3874" s="T3127">n</ta>
            <ta e="T3129" id="Seg_3875" s="T3128">v</ta>
            <ta e="T3130" id="Seg_3876" s="T3129">n</ta>
            <ta e="T3131" id="Seg_3877" s="T3130">dempro</ta>
            <ta e="T3132" id="Seg_3878" s="T3131">v</ta>
            <ta e="T3133" id="Seg_3879" s="T3132">adv</ta>
            <ta e="T3134" id="Seg_3880" s="T3133">n</ta>
            <ta e="T3135" id="Seg_3881" s="T3134">v</ta>
            <ta e="T3136" id="Seg_3882" s="T3135">adv</ta>
            <ta e="T3137" id="Seg_3883" s="T3136">n</ta>
            <ta e="T3138" id="Seg_3884" s="T3137">v</ta>
            <ta e="T3140" id="Seg_3885" s="T3139">n</ta>
            <ta e="T3141" id="Seg_3886" s="T3140">v</ta>
            <ta e="T3142" id="Seg_3887" s="T3141">v</ta>
            <ta e="T3143" id="Seg_3888" s="T3142">n</ta>
            <ta e="T3144" id="Seg_3889" s="T3143">conj</ta>
            <ta e="T3145" id="Seg_3890" s="T3144">dempro</ta>
            <ta e="T3146" id="Seg_3891" s="T3145">v</ta>
            <ta e="T3147" id="Seg_3892" s="T3146">adv</ta>
            <ta e="T3148" id="Seg_3893" s="T3147">v</ta>
            <ta e="T3149" id="Seg_3894" s="T3148">dempro</ta>
            <ta e="T3150" id="Seg_3895" s="T3149">adj</ta>
            <ta e="T3151" id="Seg_3896" s="T3150">n</ta>
            <ta e="T3152" id="Seg_3897" s="T3151">quant</ta>
            <ta e="T3153" id="Seg_3898" s="T3152">n</ta>
            <ta e="T3154" id="Seg_3899" s="T3153">v</ta>
            <ta e="T3155" id="Seg_3900" s="T3154">conj</ta>
            <ta e="T3156" id="Seg_3901" s="T3155">dempro</ta>
            <ta e="T3157" id="Seg_3902" s="T3156">n</ta>
            <ta e="T3158" id="Seg_3903" s="T3157">v</ta>
            <ta e="T3159" id="Seg_3904" s="T3158">pers</ta>
            <ta e="T3160" id="Seg_3905" s="T3159">v</ta>
            <ta e="T3161" id="Seg_3906" s="T3160">v</ta>
            <ta e="T3162" id="Seg_3907" s="T3161">dempro</ta>
            <ta e="T3163" id="Seg_3908" s="T3162">v</ta>
            <ta e="T3164" id="Seg_3909" s="T3163">aux</ta>
            <ta e="T3165" id="Seg_3910" s="T3164">v</ta>
            <ta e="T3166" id="Seg_3911" s="T3165">pers</ta>
            <ta e="T3168" id="Seg_3912" s="T3167">n</ta>
            <ta e="T3169" id="Seg_3913" s="T3168">conj</ta>
            <ta e="T3170" id="Seg_3914" s="T3169">n</ta>
            <ta e="T3171" id="Seg_3915" s="T3170">v</ta>
            <ta e="T3172" id="Seg_3916" s="T3171">conj</ta>
            <ta e="T3173" id="Seg_3917" s="T3172">v</ta>
            <ta e="T3174" id="Seg_3918" s="T3173">n</ta>
            <ta e="T3175" id="Seg_3919" s="T3174">v</ta>
            <ta e="T3176" id="Seg_3920" s="T3175">adv</ta>
            <ta e="T3177" id="Seg_3921" s="T3176">dempro</ta>
            <ta e="T3178" id="Seg_3922" s="T3177">ptcl</ta>
            <ta e="T3179" id="Seg_3923" s="T3178">v</ta>
            <ta e="T3180" id="Seg_3924" s="T3179">n</ta>
            <ta e="T3181" id="Seg_3925" s="T3180">n</ta>
            <ta e="T3182" id="Seg_3926" s="T3181">v</ta>
            <ta e="T3183" id="Seg_3927" s="T3182">adv</ta>
            <ta e="T3184" id="Seg_3928" s="T3183">n</ta>
            <ta e="T3185" id="Seg_3929" s="T3184">dempro</ta>
            <ta e="T3186" id="Seg_3930" s="T3185">v</ta>
            <ta e="T3187" id="Seg_3931" s="T3186">n</ta>
            <ta e="T3188" id="Seg_3932" s="T3187">v</ta>
            <ta e="T3189" id="Seg_3933" s="T3188">conj</ta>
            <ta e="T3190" id="Seg_3934" s="T3189">dempro</ta>
            <ta e="T3191" id="Seg_3935" s="T3190">propr</ta>
            <ta e="T3192" id="Seg_3936" s="T3191">n</ta>
            <ta e="T3193" id="Seg_3937" s="T3192">v</ta>
            <ta e="T3194" id="Seg_3938" s="T3193">v</ta>
            <ta e="T3195" id="Seg_3939" s="T3194">v</ta>
            <ta e="T3196" id="Seg_3940" s="T3195">n</ta>
            <ta e="T3197" id="Seg_3941" s="T3196">adv</ta>
            <ta e="T3198" id="Seg_3942" s="T3197">v</ta>
            <ta e="T3200" id="Seg_3943" s="T3199">adj</ta>
            <ta e="T3201" id="Seg_3944" s="T3200">que</ta>
            <ta e="T3202" id="Seg_3945" s="T3201">ptcl</ta>
            <ta e="T3203" id="Seg_3946" s="T3202">v</ta>
            <ta e="T3204" id="Seg_3947" s="T3203">v</ta>
            <ta e="T3205" id="Seg_3948" s="T3204">conj</ta>
            <ta e="T3206" id="Seg_3949" s="T3205">v</ta>
            <ta e="T3207" id="Seg_3950" s="T3206">adv</ta>
            <ta e="T3209" id="Seg_3951" s="T3208">v</ta>
            <ta e="T3210" id="Seg_3952" s="T3209">n</ta>
            <ta e="T3211" id="Seg_3953" s="T3210">n</ta>
            <ta e="T3212" id="Seg_3954" s="T3211">que</ta>
            <ta e="T3213" id="Seg_3955" s="T3212">pers</ta>
            <ta e="T3214" id="Seg_3956" s="T3213">v</ta>
            <ta e="T3215" id="Seg_3957" s="T3214">dempro</ta>
            <ta e="T3217" id="Seg_3958" s="T3216">dempro</ta>
            <ta e="T3218" id="Seg_3959" s="T3217">n</ta>
            <ta e="T3219" id="Seg_3960" s="T3218">v</ta>
            <ta e="T3220" id="Seg_3961" s="T3219">dempro</ta>
            <ta e="T3221" id="Seg_3962" s="T3220">ptcl</ta>
            <ta e="T3222" id="Seg_3963" s="T3221">v</ta>
            <ta e="T3223" id="Seg_3964" s="T3222">v</ta>
            <ta e="T3224" id="Seg_3965" s="T3223">adv</ta>
            <ta e="T3225" id="Seg_3966" s="T3224">v</ta>
            <ta e="T3226" id="Seg_3967" s="T3225">adv</ta>
            <ta e="T3227" id="Seg_3968" s="T3226">dempro</ta>
            <ta e="T3228" id="Seg_3969" s="T3227">propr</ta>
            <ta e="T3229" id="Seg_3970" s="T3228">v</ta>
            <ta e="T3230" id="Seg_3971" s="T3229">dempro</ta>
            <ta e="T3231" id="Seg_3972" s="T3230">dempro</ta>
            <ta e="T3232" id="Seg_3973" s="T3231">dempro</ta>
            <ta e="T3235" id="Seg_3974" s="T3234">dempro</ta>
            <ta e="T3237" id="Seg_3975" s="T3236">v</ta>
            <ta e="T3238" id="Seg_3976" s="T3237">dempro</ta>
            <ta e="T3239" id="Seg_3977" s="T3238">dempro</ta>
            <ta e="T3240" id="Seg_3978" s="T3239">n</ta>
            <ta e="T3241" id="Seg_3979" s="T3240">dempro</ta>
            <ta e="T3242" id="Seg_3980" s="T3241">dempro</ta>
            <ta e="T3243" id="Seg_3981" s="T3242">n</ta>
            <ta e="T3244" id="Seg_3982" s="T3243">v</ta>
            <ta e="T3245" id="Seg_3983" s="T3244">conj</ta>
            <ta e="T3246" id="Seg_3984" s="T3245">v</ta>
            <ta e="T3247" id="Seg_3985" s="T3246">v</ta>
            <ta e="T3248" id="Seg_3986" s="T3247">dempro</ta>
            <ta e="T3249" id="Seg_3987" s="T3248">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2921" id="Seg_3988" s="T2920">np.h:E</ta>
            <ta e="T2923" id="Seg_3989" s="T2922">pro.h:Th</ta>
            <ta e="T2925" id="Seg_3990" s="T2924">np.h:Th</ta>
            <ta e="T2926" id="Seg_3991" s="T2925">pro.h:Poss</ta>
            <ta e="T2927" id="Seg_3992" s="T2926">np.h:P</ta>
            <ta e="T2928" id="Seg_3993" s="T2927">np.h:P</ta>
            <ta e="T2930" id="Seg_3994" s="T2929">np.h:Th</ta>
            <ta e="T2934" id="Seg_3995" s="T2933">pro:G</ta>
            <ta e="T2935" id="Seg_3996" s="T2934">pro.h:Th</ta>
            <ta e="T2936" id="Seg_3997" s="T2935">0.3.h:A</ta>
            <ta e="T2937" id="Seg_3998" s="T2936">pro.h:Poss</ta>
            <ta e="T2939" id="Seg_3999" s="T2938">np.h:Th</ta>
            <ta e="T2943" id="Seg_4000" s="T2942">0.3.h:A</ta>
            <ta e="T2946" id="Seg_4001" s="T2945">np.h:A</ta>
            <ta e="T2951" id="Seg_4002" s="T2950">0.3.h:A</ta>
            <ta e="T2953" id="Seg_4003" s="T2952">pro.h:A</ta>
            <ta e="T2956" id="Seg_4004" s="T2955">adv:Time</ta>
            <ta e="T2958" id="Seg_4005" s="T2957">np.h:A</ta>
            <ta e="T2959" id="Seg_4006" s="T2958">adv:Time</ta>
            <ta e="T2960" id="Seg_4007" s="T2959">0.3.h:A</ta>
            <ta e="T2961" id="Seg_4008" s="T2960">0.2.h:A</ta>
            <ta e="T2962" id="Seg_4009" s="T2961">pro.h:B</ta>
            <ta e="T2963" id="Seg_4010" s="T2962">0.2.h:A</ta>
            <ta e="T2965" id="Seg_4011" s="T2964">np:P</ta>
            <ta e="T2966" id="Seg_4012" s="T2965">pro.h:A</ta>
            <ta e="T2968" id="Seg_4013" s="T2967">np:G</ta>
            <ta e="T2971" id="Seg_4014" s="T2970">0.3.h:A</ta>
            <ta e="T2973" id="Seg_4015" s="T2972">pro.h:E</ta>
            <ta e="T2975" id="Seg_4016" s="T2974">0.3.h:A</ta>
            <ta e="T2976" id="Seg_4017" s="T2975">pro.h:R</ta>
            <ta e="T2983" id="Seg_4018" s="T2982">np.h:A</ta>
            <ta e="T2986" id="Seg_4019" s="T2985">np:G</ta>
            <ta e="T2987" id="Seg_4020" s="T2986">0.2.h:A</ta>
            <ta e="T2989" id="Seg_4021" s="T2988">np:So</ta>
            <ta e="T2990" id="Seg_4022" s="T2989">0.2.h:A</ta>
            <ta e="T2991" id="Seg_4023" s="T2990">np:Th</ta>
            <ta e="T2993" id="Seg_4024" s="T2992">adv:Time</ta>
            <ta e="T2995" id="Seg_4025" s="T2994">pro.h:B</ta>
            <ta e="T2997" id="Seg_4026" s="T2996">0.3.h:A</ta>
            <ta e="T2999" id="Seg_4027" s="T2998">0.3.h:A</ta>
            <ta e="T3000" id="Seg_4028" s="T2999">np:P</ta>
            <ta e="T3001" id="Seg_4029" s="T3000">0.3.h:A</ta>
            <ta e="T3002" id="Seg_4030" s="T3001">pro.h:A</ta>
            <ta e="T3003" id="Seg_4031" s="T3002">adv:Time</ta>
            <ta e="T3005" id="Seg_4032" s="T3004">adv:Time</ta>
            <ta e="T3008" id="Seg_4033" s="T3007">0.3:Th</ta>
            <ta e="T3009" id="Seg_4034" s="T3008">pro.h:A</ta>
            <ta e="T3012" id="Seg_4035" s="T3011">np:G</ta>
            <ta e="T3014" id="Seg_4036" s="T3013">0.3.h:A</ta>
            <ta e="T3015" id="Seg_4037" s="T3014">adv:Time</ta>
            <ta e="T3016" id="Seg_4038" s="T3015">0.3.h:A</ta>
            <ta e="T3018" id="Seg_4039" s="T3017">np:R</ta>
            <ta e="T3021" id="Seg_4040" s="T3020">0.2.h:A</ta>
            <ta e="T3022" id="Seg_4041" s="T3021">pro.h:Com</ta>
            <ta e="T3023" id="Seg_4042" s="T3022">pro.h:A</ta>
            <ta e="T3025" id="Seg_4043" s="T3024">adv:L</ta>
            <ta e="T3026" id="Seg_4044" s="T3025">0.3.h:E</ta>
            <ta e="T3028" id="Seg_4045" s="T3027">pro.h:A</ta>
            <ta e="T3030" id="Seg_4046" s="T3029">0.2.h:E</ta>
            <ta e="T3032" id="Seg_4047" s="T3031">0.2.h:E</ta>
            <ta e="T3034" id="Seg_4048" s="T3033">np:E</ta>
            <ta e="T3036" id="Seg_4049" s="T3035">pro.h:B</ta>
            <ta e="T3038" id="Seg_4050" s="T3037">np.h:A</ta>
            <ta e="T3040" id="Seg_4051" s="T3039">pro.h:A</ta>
            <ta e="T3042" id="Seg_4052" s="T3041">np:Th</ta>
            <ta e="T3044" id="Seg_4053" s="T3043">0.3.h:A</ta>
            <ta e="T3046" id="Seg_4054" s="T3045">np.h:R</ta>
            <ta e="T3047" id="Seg_4055" s="T3046">0.3.h:A</ta>
            <ta e="T3048" id="Seg_4056" s="T3047">adv:Time</ta>
            <ta e="T3050" id="Seg_4057" s="T3049">np.h:A</ta>
            <ta e="T3052" id="Seg_4058" s="T3051">np.h:Th</ta>
            <ta e="T3054" id="Seg_4059" s="T3053">0.2.h:A</ta>
            <ta e="T3056" id="Seg_4060" s="T3055">pro.h:A</ta>
            <ta e="T3057" id="Seg_4061" s="T3056">adv:L</ta>
            <ta e="T3060" id="Seg_4062" s="T3059">np.h:A</ta>
            <ta e="T3062" id="Seg_4063" s="T3061">adv:L</ta>
            <ta e="T3063" id="Seg_4064" s="T3062">0.3.h:E</ta>
            <ta e="T3064" id="Seg_4065" s="T3063">pro.h:A</ta>
            <ta e="T3066" id="Seg_4066" s="T3065">pro.h:P</ta>
            <ta e="T3069" id="Seg_4067" s="T3068">0.2.h:E</ta>
            <ta e="T3071" id="Seg_4068" s="T3070">0.2.h:E</ta>
            <ta e="T3074" id="Seg_4069" s="T3073">np:E</ta>
            <ta e="T3076" id="Seg_4070" s="T3075">np.h:A</ta>
            <ta e="T3077" id="Seg_4071" s="T3076">pro.h:B</ta>
            <ta e="T3079" id="Seg_4072" s="T3078">np:P</ta>
            <ta e="T3080" id="Seg_4073" s="T3079">0.3.h:A</ta>
            <ta e="T3082" id="Seg_4074" s="T3081">np:Th</ta>
            <ta e="T3083" id="Seg_4075" s="T3082">0.3.h:A</ta>
            <ta e="T3084" id="Seg_4076" s="T3083">adv:Time</ta>
            <ta e="T3085" id="Seg_4077" s="T3084">pro.h:A</ta>
            <ta e="T3087" id="Seg_4078" s="T3086">pro.h:E</ta>
            <ta e="T3091" id="Seg_4079" s="T3090">adv:Time</ta>
            <ta e="T3093" id="Seg_4080" s="T3092">np.h:Th</ta>
            <ta e="T3094" id="Seg_4081" s="T3093">0.3.h:A</ta>
            <ta e="T3096" id="Seg_4082" s="T3095">np:Ins</ta>
            <ta e="T3098" id="Seg_4083" s="T3097">np.h:A</ta>
            <ta e="T3101" id="Seg_4084" s="T3100">0.3.h:E</ta>
            <ta e="T3102" id="Seg_4085" s="T3101">0.3.h:E</ta>
            <ta e="T3104" id="Seg_4086" s="T3103">np.h:A</ta>
            <ta e="T3106" id="Seg_4087" s="T3105">pro.h:B</ta>
            <ta e="T3107" id="Seg_4088" s="T3106">np:Th</ta>
            <ta e="T3109" id="Seg_4089" s="T3108">0.2.h:A</ta>
            <ta e="T3111" id="Seg_4090" s="T3110">0.2.h:E</ta>
            <ta e="T3115" id="Seg_4091" s="T3114">np:Th</ta>
            <ta e="T3116" id="Seg_4092" s="T3115">0.3.h:E</ta>
            <ta e="T3117" id="Seg_4093" s="T3116">adv:Time</ta>
            <ta e="T3119" id="Seg_4094" s="T3118">np:E</ta>
            <ta e="T3123" id="Seg_4095" s="T3122">np:E</ta>
            <ta e="T3126" id="Seg_4096" s="T3125">adv:Time</ta>
            <ta e="T3127" id="Seg_4097" s="T3126">0.3.h:A</ta>
            <ta e="T3128" id="Seg_4098" s="T3127">np:G</ta>
            <ta e="T3129" id="Seg_4099" s="T3128">0.3.h:A</ta>
            <ta e="T3130" id="Seg_4100" s="T3129">np.h:A</ta>
            <ta e="T3131" id="Seg_4101" s="T3130">pro.h:B</ta>
            <ta e="T3133" id="Seg_4102" s="T3132">adv:Time</ta>
            <ta e="T3134" id="Seg_4103" s="T3133">n:Time</ta>
            <ta e="T3135" id="Seg_4104" s="T3134">0.3.h:A</ta>
            <ta e="T3136" id="Seg_4105" s="T3135">adv:Time</ta>
            <ta e="T3137" id="Seg_4106" s="T3136">n:Time</ta>
            <ta e="T3138" id="Seg_4107" s="T3137">0.3.h:A</ta>
            <ta e="T3140" id="Seg_4108" s="T3139">np.h:R</ta>
            <ta e="T3141" id="Seg_4109" s="T3140">0.3.h:A</ta>
            <ta e="T3142" id="Seg_4110" s="T3141">0.2.h:A</ta>
            <ta e="T3143" id="Seg_4111" s="T3142">np.h:P</ta>
            <ta e="T3145" id="Seg_4112" s="T3144">pro.h:A</ta>
            <ta e="T3149" id="Seg_4113" s="T3148">np.h:Th</ta>
            <ta e="T3151" id="Seg_4114" s="T3150">np.h:Th</ta>
            <ta e="T3153" id="Seg_4115" s="T3152">np:Th</ta>
            <ta e="T3154" id="Seg_4116" s="T3153">0.3.h:A</ta>
            <ta e="T3157" id="Seg_4117" s="T3156">np.h:A</ta>
            <ta e="T3159" id="Seg_4118" s="T3158">pro.h:P</ta>
            <ta e="T3161" id="Seg_4119" s="T3160">0.3.h:A</ta>
            <ta e="T3162" id="Seg_4120" s="T3161">pro.h:A</ta>
            <ta e="T3164" id="Seg_4121" s="T3163">0.2.h:A</ta>
            <ta e="T3166" id="Seg_4122" s="T3165">pro.h:Poss</ta>
            <ta e="T3168" id="Seg_4123" s="T3167">np:P</ta>
            <ta e="T3170" id="Seg_4124" s="T3169">np:Th</ta>
            <ta e="T3171" id="Seg_4125" s="T3170">0.2.h:A</ta>
            <ta e="T3173" id="Seg_4126" s="T3172">0.2.h:A</ta>
            <ta e="T3174" id="Seg_4127" s="T3173">np:G</ta>
            <ta e="T3175" id="Seg_4128" s="T3174">0.2.h:A</ta>
            <ta e="T3176" id="Seg_4129" s="T3175">adv:Time</ta>
            <ta e="T3177" id="Seg_4130" s="T3176">pro.h:A</ta>
            <ta e="T3180" id="Seg_4131" s="T3179">np:P</ta>
            <ta e="T3181" id="Seg_4132" s="T3180">np:Th</ta>
            <ta e="T3182" id="Seg_4133" s="T3181">0.3.h:A</ta>
            <ta e="T3183" id="Seg_4134" s="T3182">adv:Time</ta>
            <ta e="T3184" id="Seg_4135" s="T3183">np.h:A</ta>
            <ta e="T3185" id="Seg_4136" s="T3184">pro.h:P</ta>
            <ta e="T3187" id="Seg_4137" s="T3186">np:Th 0.3.h:Poss</ta>
            <ta e="T3188" id="Seg_4138" s="T3187">0.3.h:A</ta>
            <ta e="T3191" id="Seg_4139" s="T3190">np.h:A</ta>
            <ta e="T3192" id="Seg_4140" s="T3191">np:Th</ta>
            <ta e="T3194" id="Seg_4141" s="T3193">0.3.h:A</ta>
            <ta e="T3195" id="Seg_4142" s="T3194">0.3.h:A</ta>
            <ta e="T3196" id="Seg_4143" s="T3195">np:G</ta>
            <ta e="T3197" id="Seg_4144" s="T3196">adv:Time</ta>
            <ta e="T3198" id="Seg_4145" s="T3197">0.3:P</ta>
            <ta e="T3201" id="Seg_4146" s="T3200">pro:A</ta>
            <ta e="T3204" id="Seg_4147" s="T3203">0.3.h:A</ta>
            <ta e="T3206" id="Seg_4148" s="T3205">0.3.h:E</ta>
            <ta e="T3207" id="Seg_4149" s="T3206">adv:Time</ta>
            <ta e="T3211" id="Seg_4150" s="T3210">np.h:A</ta>
            <ta e="T3212" id="Seg_4151" s="T3211">pro.h:A</ta>
            <ta e="T3213" id="Seg_4152" s="T3212">pro.h:R</ta>
            <ta e="T3215" id="Seg_4153" s="T3214">pro:Th</ta>
            <ta e="T3217" id="Seg_4154" s="T3216">pro.h:Th</ta>
            <ta e="T3219" id="Seg_4155" s="T3218">0.1.h:A</ta>
            <ta e="T3220" id="Seg_4156" s="T3219">pro.h:A</ta>
            <ta e="T3225" id="Seg_4157" s="T3224">0.3:Th</ta>
            <ta e="T3226" id="Seg_4158" s="T3225">adv:Time</ta>
            <ta e="T3228" id="Seg_4159" s="T3227">np.h:A</ta>
            <ta e="T3232" id="Seg_4160" s="T3231">pro.h:R</ta>
            <ta e="T3235" id="Seg_4161" s="T3234">pro.h:A</ta>
            <ta e="T3239" id="Seg_4162" s="T3238">pro.h:A</ta>
            <ta e="T3241" id="Seg_4163" s="T3240">pro.h:A</ta>
            <ta e="T3242" id="Seg_4164" s="T3241">pro.h:Th</ta>
            <ta e="T3247" id="Seg_4165" s="T3246">0.3.h:A</ta>
            <ta e="T3248" id="Seg_4166" s="T3247">pro:So</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2919" id="Seg_4167" s="T2918">v:pred</ta>
            <ta e="T2921" id="Seg_4168" s="T2920">np.h:S</ta>
            <ta e="T2923" id="Seg_4169" s="T2922">pro.h:O</ta>
            <ta e="T2924" id="Seg_4170" s="T2923">v:pred</ta>
            <ta e="T2925" id="Seg_4171" s="T2924">np.h:S</ta>
            <ta e="T2927" id="Seg_4172" s="T2926">np.h:S</ta>
            <ta e="T2928" id="Seg_4173" s="T2927">np.h:S</ta>
            <ta e="T2929" id="Seg_4174" s="T2928">v:pred</ta>
            <ta e="T2930" id="Seg_4175" s="T2929">np.h:S</ta>
            <ta e="T2931" id="Seg_4176" s="T2930">ptcl.neg</ta>
            <ta e="T2932" id="Seg_4177" s="T2931">adj:pred</ta>
            <ta e="T2935" id="Seg_4178" s="T2934">pro.h:O</ta>
            <ta e="T2936" id="Seg_4179" s="T2935">v:pred 0.3.h:S</ta>
            <ta e="T2939" id="Seg_4180" s="T2938">np.h:S</ta>
            <ta e="T2940" id="Seg_4181" s="T2939">v:pred</ta>
            <ta e="T2942" id="Seg_4182" s="T2941">ptcl.neg</ta>
            <ta e="T2943" id="Seg_4183" s="T2942">v:pred 0.3.h:S</ta>
            <ta e="T2946" id="Seg_4184" s="T2945">np.h:S</ta>
            <ta e="T2948" id="Seg_4185" s="T2947">v:pred</ta>
            <ta e="T2951" id="Seg_4186" s="T2950">v:pred 0.3.h:S</ta>
            <ta e="T2952" id="Seg_4187" s="T2951">ptcl.neg</ta>
            <ta e="T2953" id="Seg_4188" s="T2952">pro.h:S</ta>
            <ta e="T2954" id="Seg_4189" s="T2953">ptcl.neg</ta>
            <ta e="T2955" id="Seg_4190" s="T2954">v:pred</ta>
            <ta e="T2957" id="Seg_4191" s="T2956">v:pred</ta>
            <ta e="T2958" id="Seg_4192" s="T2957">np.h:S</ta>
            <ta e="T2960" id="Seg_4193" s="T2959">v:pred 0.3.h:S</ta>
            <ta e="T2961" id="Seg_4194" s="T2960">v:pred 0.2.h:S</ta>
            <ta e="T2963" id="Seg_4195" s="T2962">v:pred 0.2.h:S</ta>
            <ta e="T2965" id="Seg_4196" s="T2964">np:O</ta>
            <ta e="T2966" id="Seg_4197" s="T2965">pro.h:S</ta>
            <ta e="T2967" id="Seg_4198" s="T2966">v:pred</ta>
            <ta e="T2971" id="Seg_4199" s="T2970">v:pred 0.3.h:S</ta>
            <ta e="T2973" id="Seg_4200" s="T2972">pro.h:O</ta>
            <ta e="T2975" id="Seg_4201" s="T2974">v:pred 0.3.h:S</ta>
            <ta e="T2978" id="Seg_4202" s="T2977">v:pred</ta>
            <ta e="T2979" id="Seg_4203" s="T2978">ptcl.neg</ta>
            <ta e="T2981" id="Seg_4204" s="T2980">v:pred </ta>
            <ta e="T2983" id="Seg_4205" s="T2982">np.h:S</ta>
            <ta e="T2984" id="Seg_4206" s="T2983">v:pred</ta>
            <ta e="T2986" id="Seg_4207" s="T2985">np:O</ta>
            <ta e="T2987" id="Seg_4208" s="T2986">v:pred 0.2.h:S</ta>
            <ta e="T2989" id="Seg_4209" s="T2988">np:O</ta>
            <ta e="T2990" id="Seg_4210" s="T2989">v:pred 0.2.h:S</ta>
            <ta e="T2991" id="Seg_4211" s="T2990">np:S</ta>
            <ta e="T2992" id="Seg_4212" s="T2991">v:pred</ta>
            <ta e="T2997" id="Seg_4213" s="T2996">v:pred 0.3.h:S</ta>
            <ta e="T2999" id="Seg_4214" s="T2998">v:pred 0.3.h:S</ta>
            <ta e="T3000" id="Seg_4215" s="T2999">np:O</ta>
            <ta e="T3001" id="Seg_4216" s="T3000">v:pred 0.3.h:S</ta>
            <ta e="T3002" id="Seg_4217" s="T3001">pro.h:S</ta>
            <ta e="T3004" id="Seg_4218" s="T3003">v:pred</ta>
            <ta e="T3007" id="Seg_4219" s="T3006">adj:pred</ta>
            <ta e="T3008" id="Seg_4220" s="T3007">cop 0.3:S</ta>
            <ta e="T3009" id="Seg_4221" s="T3008">pro.h:S</ta>
            <ta e="T3011" id="Seg_4222" s="T3010">v:pred</ta>
            <ta e="T3014" id="Seg_4223" s="T3013">v:pred 0.3.h:S</ta>
            <ta e="T3016" id="Seg_4224" s="T3015">v:pred 0.3.h:S</ta>
            <ta e="T3021" id="Seg_4225" s="T3020">v:pred 0.2.h:S</ta>
            <ta e="T3023" id="Seg_4226" s="T3022">pro.h:S</ta>
            <ta e="T3024" id="Seg_4227" s="T3023">v:pred</ta>
            <ta e="T3026" id="Seg_4228" s="T3025">v:pred 0.3.h:S</ta>
            <ta e="T3028" id="Seg_4229" s="T3027">pro.h:S</ta>
            <ta e="T3030" id="Seg_4230" s="T3029">v:pred 0.2.h:S</ta>
            <ta e="T3032" id="Seg_4231" s="T3031">v:pred 0.2.h:S</ta>
            <ta e="T3034" id="Seg_4232" s="T3033">np:S</ta>
            <ta e="T3035" id="Seg_4233" s="T3034">v:pred</ta>
            <ta e="T3038" id="Seg_4234" s="T3037">np.h:S</ta>
            <ta e="T3039" id="Seg_4235" s="T3038">v:pred</ta>
            <ta e="T3040" id="Seg_4236" s="T3039">pro.h:S</ta>
            <ta e="T3041" id="Seg_4237" s="T3040">v:pred</ta>
            <ta e="T3042" id="Seg_4238" s="T3041">np:O</ta>
            <ta e="T3044" id="Seg_4239" s="T3043">v:pred 0.3.h:S</ta>
            <ta e="T3047" id="Seg_4240" s="T3046">v:pred 0.3.h:S</ta>
            <ta e="T3050" id="Seg_4241" s="T3049">np.h:S</ta>
            <ta e="T3052" id="Seg_4242" s="T3051">np.h:O</ta>
            <ta e="T3053" id="Seg_4243" s="T3052">v:pred</ta>
            <ta e="T3054" id="Seg_4244" s="T3053">v:pred 0.2.h:S</ta>
            <ta e="T3056" id="Seg_4245" s="T3055">pro.h:S</ta>
            <ta e="T3058" id="Seg_4246" s="T3057">v:pred</ta>
            <ta e="T3060" id="Seg_4247" s="T3059">np.h:S</ta>
            <ta e="T3061" id="Seg_4248" s="T3060">v:pred</ta>
            <ta e="T3063" id="Seg_4249" s="T3062">v:pred 0.3.h:S</ta>
            <ta e="T3064" id="Seg_4250" s="T3063">pro.h:S</ta>
            <ta e="T3066" id="Seg_4251" s="T3065">pro.h:O</ta>
            <ta e="T3067" id="Seg_4252" s="T3066">v:pred</ta>
            <ta e="T3069" id="Seg_4253" s="T3068">v:pred 0.2.h:S</ta>
            <ta e="T3071" id="Seg_4254" s="T3070">v:pred 0.2.h:S</ta>
            <ta e="T3074" id="Seg_4255" s="T3073">np:S</ta>
            <ta e="T3075" id="Seg_4256" s="T3074">v:pred</ta>
            <ta e="T3076" id="Seg_4257" s="T3075">np.h:S</ta>
            <ta e="T3078" id="Seg_4258" s="T3077">v:pred</ta>
            <ta e="T3079" id="Seg_4259" s="T3078">np:O</ta>
            <ta e="T3080" id="Seg_4260" s="T3079">v:pred 0.3.h:S</ta>
            <ta e="T3082" id="Seg_4261" s="T3081">np:O</ta>
            <ta e="T3083" id="Seg_4262" s="T3082">v:pred 0.3.h:S</ta>
            <ta e="T3085" id="Seg_4263" s="T3084">pro.h:S</ta>
            <ta e="T3086" id="Seg_4264" s="T3085">v:pred</ta>
            <ta e="T3087" id="Seg_4265" s="T3086">pro.h:S</ta>
            <ta e="T3090" id="Seg_4266" s="T3089">v:pred</ta>
            <ta e="T3093" id="Seg_4267" s="T3092">np.h:O</ta>
            <ta e="T3094" id="Seg_4268" s="T3093">v:pred 0.3.h:S</ta>
            <ta e="T3098" id="Seg_4269" s="T3097">np.h:S</ta>
            <ta e="T3099" id="Seg_4270" s="T3098">v:pred</ta>
            <ta e="T3101" id="Seg_4271" s="T3100">v:pred 0.3.h:S</ta>
            <ta e="T3102" id="Seg_4272" s="T3101">v:pred 0.3.h:S</ta>
            <ta e="T3104" id="Seg_4273" s="T3103">np.h:S</ta>
            <ta e="T3107" id="Seg_4274" s="T3106">np:O</ta>
            <ta e="T3108" id="Seg_4275" s="T3107">v:pred</ta>
            <ta e="T3109" id="Seg_4276" s="T3108">v:pred 0.2.h:S</ta>
            <ta e="T3111" id="Seg_4277" s="T3110">v:pred 0.2.h:S</ta>
            <ta e="T3116" id="Seg_4278" s="T3115">v:pred 0.3.h:S</ta>
            <ta e="T3119" id="Seg_4279" s="T3118">np:S</ta>
            <ta e="T3120" id="Seg_4280" s="T3119">v:pred</ta>
            <ta e="T3123" id="Seg_4281" s="T3122">np:S</ta>
            <ta e="T3125" id="Seg_4282" s="T3124">v:pred</ta>
            <ta e="T3127" id="Seg_4283" s="T3126">v:pred 0.3.h:S</ta>
            <ta e="T3129" id="Seg_4284" s="T3128">v:pred 0.3.h:S</ta>
            <ta e="T3130" id="Seg_4285" s="T3129">np.h:S</ta>
            <ta e="T3132" id="Seg_4286" s="T3131">v:pred</ta>
            <ta e="T3135" id="Seg_4287" s="T3134">v:pred 0.3.h:S</ta>
            <ta e="T3138" id="Seg_4288" s="T3137">v:pred 0.3.h:S</ta>
            <ta e="T3141" id="Seg_4289" s="T3140">v:pred 0.3.h:S</ta>
            <ta e="T3142" id="Seg_4290" s="T3141">v:pred 0.2.h:S</ta>
            <ta e="T3145" id="Seg_4291" s="T3144">pro.h:S</ta>
            <ta e="T3146" id="Seg_4292" s="T3145">v:pred </ta>
            <ta e="T3148" id="Seg_4293" s="T3147">v:pred</ta>
            <ta e="T3149" id="Seg_4294" s="T3148">pro.h:S</ta>
            <ta e="T3151" id="Seg_4295" s="T3150">n:pred</ta>
            <ta e="T3153" id="Seg_4296" s="T3152">np:O</ta>
            <ta e="T3154" id="Seg_4297" s="T3153">v:pred 0.3.h:S</ta>
            <ta e="T3157" id="Seg_4298" s="T3156">np.h:S</ta>
            <ta e="T3158" id="Seg_4299" s="T3157">v:pred</ta>
            <ta e="T3159" id="Seg_4300" s="T3158">pro.h:O</ta>
            <ta e="T3161" id="Seg_4301" s="T3160">v:pred</ta>
            <ta e="T3162" id="Seg_4302" s="T3161">pro.h:S</ta>
            <ta e="T3163" id="Seg_4303" s="T3162">v:pred</ta>
            <ta e="T3164" id="Seg_4304" s="T3163">v:pred 0.2.h:S</ta>
            <ta e="T3168" id="Seg_4305" s="T3167">np:O</ta>
            <ta e="T3170" id="Seg_4306" s="T3169">np:O</ta>
            <ta e="T3171" id="Seg_4307" s="T3170">v:pred 0.2.h:S</ta>
            <ta e="T3173" id="Seg_4308" s="T3172">v:pred 0.2.h:S</ta>
            <ta e="T3175" id="Seg_4309" s="T3174">v:pred 0.2.h:S</ta>
            <ta e="T3177" id="Seg_4310" s="T3176">pro.h:S</ta>
            <ta e="T3178" id="Seg_4311" s="T3177">ptcl.neg</ta>
            <ta e="T3179" id="Seg_4312" s="T3178">v:pred</ta>
            <ta e="T3180" id="Seg_4313" s="T3179">np:O</ta>
            <ta e="T3181" id="Seg_4314" s="T3180">np:O</ta>
            <ta e="T3182" id="Seg_4315" s="T3181">v:pred 0.3.h:S</ta>
            <ta e="T3184" id="Seg_4316" s="T3183">np.h:S</ta>
            <ta e="T3185" id="Seg_4317" s="T3184">pro.h:O</ta>
            <ta e="T3186" id="Seg_4318" s="T3185">v:pred</ta>
            <ta e="T3187" id="Seg_4319" s="T3186">np:O</ta>
            <ta e="T3188" id="Seg_4320" s="T3187">v:pred 0.3.h:S</ta>
            <ta e="T3191" id="Seg_4321" s="T3190">np.h:S</ta>
            <ta e="T3192" id="Seg_4322" s="T3191">np:O</ta>
            <ta e="T3193" id="Seg_4323" s="T3192">v:pred</ta>
            <ta e="T3194" id="Seg_4324" s="T3193">v:pred 0.3.h:S</ta>
            <ta e="T3195" id="Seg_4325" s="T3194">v:pred 0.3.h:S</ta>
            <ta e="T3198" id="Seg_4326" s="T3197">v:pred</ta>
            <ta e="T3201" id="Seg_4327" s="T3200">pro.h:S</ta>
            <ta e="T3202" id="Seg_4328" s="T3201">ptcl.neg</ta>
            <ta e="T3203" id="Seg_4329" s="T3202">v:pred</ta>
            <ta e="T3204" id="Seg_4330" s="T3203">v:pred 0.3.h:S</ta>
            <ta e="T3206" id="Seg_4331" s="T3205">v:pred 0.3.h:S</ta>
            <ta e="T3209" id="Seg_4332" s="T3208">v:pred</ta>
            <ta e="T3211" id="Seg_4333" s="T3210">np.h:S</ta>
            <ta e="T3212" id="Seg_4334" s="T3211">pro.h:S</ta>
            <ta e="T3214" id="Seg_4335" s="T3213">v:pred</ta>
            <ta e="T3215" id="Seg_4336" s="T3214">pro:O</ta>
            <ta e="T3217" id="Seg_4337" s="T3216">pro.h:O</ta>
            <ta e="T3219" id="Seg_4338" s="T3218">v:pred 0.1.h:S</ta>
            <ta e="T3220" id="Seg_4339" s="T3219">pro.h:S</ta>
            <ta e="T3221" id="Seg_4340" s="T3220">ptcl.neg</ta>
            <ta e="T3222" id="Seg_4341" s="T3221">v:pred </ta>
            <ta e="T3225" id="Seg_4342" s="T3224">v:pred 0.3:S</ta>
            <ta e="T3228" id="Seg_4343" s="T3227">np.h:S</ta>
            <ta e="T3229" id="Seg_4344" s="T3228">v:pred</ta>
            <ta e="T3235" id="Seg_4345" s="T3234">pro.h:S</ta>
            <ta e="T3237" id="Seg_4346" s="T3236">v:pred</ta>
            <ta e="T3239" id="Seg_4347" s="T3238">pro.h:S</ta>
            <ta e="T3241" id="Seg_4348" s="T3240">pro.h:S</ta>
            <ta e="T3242" id="Seg_4349" s="T3241">pro.h:O</ta>
            <ta e="T3244" id="Seg_4350" s="T3243">v:pred</ta>
            <ta e="T3246" id="Seg_4351" s="T3245">conv:pred</ta>
            <ta e="T3247" id="Seg_4352" s="T3246">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2932" id="Seg_4353" s="T2931">TURK:core</ta>
            <ta e="T2933" id="Seg_4354" s="T2932">TURK:disc</ta>
            <ta e="T2941" id="Seg_4355" s="T2940">TURK:gram(INDEF)</ta>
            <ta e="T2944" id="Seg_4356" s="T2943">RUS:gram</ta>
            <ta e="T2947" id="Seg_4357" s="T2946">TURK:disc</ta>
            <ta e="T2949" id="Seg_4358" s="T2948">TURK:disc</ta>
            <ta e="T2968" id="Seg_4359" s="T2967">TURK:cult</ta>
            <ta e="T2969" id="Seg_4360" s="T2968">RUS:gram</ta>
            <ta e="T2970" id="Seg_4361" s="T2969">TURK:disc</ta>
            <ta e="T2977" id="Seg_4362" s="T2976">TURK:gram(INDEF)</ta>
            <ta e="T2982" id="Seg_4363" s="T2981">RUS:gram</ta>
            <ta e="T2983" id="Seg_4364" s="T2982">TURK:cult</ta>
            <ta e="T2988" id="Seg_4365" s="T2987">RUS:gram</ta>
            <ta e="T2991" id="Seg_4366" s="T2990">TURK:disc</ta>
            <ta e="T2998" id="Seg_4367" s="T2997">RUS:gram</ta>
            <ta e="T3012" id="Seg_4368" s="T3011">TURK:cult</ta>
            <ta e="T3029" id="Seg_4369" s="T3028">TURK:disc</ta>
            <ta e="T3038" id="Seg_4370" s="T3037">TURK:cult</ta>
            <ta e="T3042" id="Seg_4371" s="T3041">RUS:cult</ta>
            <ta e="T3051" id="Seg_4372" s="T3050">TURK:core</ta>
            <ta e="T3065" id="Seg_4373" s="T3064">TURK:disc</ta>
            <ta e="T3072" id="Seg_4374" s="T3071">TURK:core</ta>
            <ta e="T3076" id="Seg_4375" s="T3075">TURK:cult</ta>
            <ta e="T3081" id="Seg_4376" s="T3080">RUS:gram</ta>
            <ta e="T3082" id="Seg_4377" s="T3081">RUS:cult</ta>
            <ta e="T3088" id="Seg_4378" s="T3087">RUS:mod</ta>
            <ta e="T3100" id="Seg_4379" s="T3099">RUS:gram</ta>
            <ta e="T3105" id="Seg_4380" s="T3104">RUS:mod</ta>
            <ta e="T3113" id="Seg_4381" s="T3112">RUS:gram</ta>
            <ta e="T3121" id="Seg_4382" s="T3120">RUS:gram</ta>
            <ta e="T3124" id="Seg_4383" s="T3123">TURK:disc</ta>
            <ta e="T3130" id="Seg_4384" s="T3129">TURK:cult</ta>
            <ta e="T3143" id="Seg_4385" s="T3142">TURK:cult</ta>
            <ta e="T3144" id="Seg_4386" s="T3143">RUS:gram</ta>
            <ta e="T3147" id="Seg_4387" s="T3146">RUS:mod</ta>
            <ta e="T3150" id="Seg_4388" s="T3149">TURK:core</ta>
            <ta e="T3151" id="Seg_4389" s="T3150">TURK:cult</ta>
            <ta e="T3153" id="Seg_4390" s="T3152">TURK:cult</ta>
            <ta e="T3155" id="Seg_4391" s="T3154">RUS:gram</ta>
            <ta e="T3161" id="Seg_4392" s="T3160">RUS:mod</ta>
            <ta e="T3169" id="Seg_4393" s="T3168">RUS:gram</ta>
            <ta e="T3172" id="Seg_4394" s="T3171">RUS:gram</ta>
            <ta e="T3189" id="Seg_4395" s="T3188">RUS:gram</ta>
            <ta e="T3199" id="Seg_4396" s="T3198">RUS:cult</ta>
            <ta e="T3205" id="Seg_4397" s="T3204">RUS:gram</ta>
            <ta e="T3210" id="Seg_4398" s="T3209">TURK:cult</ta>
            <ta e="T3216" id="Seg_4399" s="T3215">RUS:cult</ta>
            <ta e="T3236" id="Seg_4400" s="T3235">RUS:cult</ta>
            <ta e="T3245" id="Seg_4401" s="T3244">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T3161" id="Seg_4402" s="T3160">parad:bare</ta>
            <ta e="T3199" id="Seg_4403" s="T3198">parad:bare</ta>
            <ta e="T3216" id="Seg_4404" s="T3215">dir:bare</ta>
            <ta e="T3236" id="Seg_4405" s="T3235">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS">
            <ta e="T2940" id="Seg_4406" s="T2936">RUS:calq</ta>
            <ta e="T3161" id="Seg_4407" s="T3160">RUS:int</ta>
            <ta e="T3234" id="Seg_4408" s="T3233">RUS:int</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T2921" id="Seg_4409" s="T2918">Жила одна девушка.</ta>
            <ta e="T2925" id="Seg_4410" s="T2921">Её звали Хаврошечка.</ta>
            <ta e="T2929" id="Seg_4411" s="T2925">Её мать и отец умерли.</ta>
            <ta e="T2933" id="Seg_4412" s="T2929">Люди [были] нехорошие.</ta>
            <ta e="T2936" id="Seg_4413" s="T2933">Они взяли её.</ta>
            <ta e="T2940" id="Seg_4414" s="T2936">У неё [Хаврошечкиной хозяйки] было три дочери.</ta>
            <ta e="T2943" id="Seg_4415" s="T2940">Они ничего не делали.</ta>
            <ta e="T2948" id="Seg_4416" s="T2943">А эта Хаврошечка делала всю работу.</ta>
            <ta e="T2955" id="Seg_4417" s="T2948">Она любую работу делала, они ничего не умели.</ta>
            <ta e="T2958" id="Seg_4418" s="T2955">Женщина [хозяйка] всё время ругается.</ta>
            <ta e="T2960" id="Seg_4419" s="T2958">Однажды она говорит:</ta>
            <ta e="T2965" id="Seg_4420" s="T2960">«Иди, сделай мне много холста».</ta>
            <ta e="T2971" id="Seg_4421" s="T2965">Она пошла к корове и плачет:</ta>
            <ta e="T2975" id="Seg_4422" s="T2971">«Меня сильно бьют.</ta>
            <ta e="T2981" id="Seg_4423" s="T2975">А я им ничего не могу сделать».</ta>
            <ta e="T2990" id="Seg_4424" s="T2981">И корова говорит: «Влезь мне в одно ухо, а в другое вылези.</ta>
            <ta e="T2992" id="Seg_4425" s="T2990">Всё будет [хорошо]».</ta>
            <ta e="T2997" id="Seg_4426" s="T2992">(Потом) так и было. [?]</ta>
            <ta e="T3001" id="Seg_4427" s="T2997">И [корова] соткала, сделала холст.</ta>
            <ta e="T3004" id="Seg_4428" s="T3001">Она [Хаврошечка] отнесла [его].</ta>
            <ta e="T3008" id="Seg_4429" s="T3004">Потом получила снова много [работы].</ta>
            <ta e="T3014" id="Seg_4430" s="T3008">Она снова пошла к корове, [та] снова [всё] сделала.</ta>
            <ta e="T3018" id="Seg_4431" s="T3014">Потом она [хозяйка] говорит одной из своих дочерей.</ta>
            <ta e="T3020" id="Seg_4432" s="T3018">Одноглазке.</ta>
            <ta e="T3024" id="Seg_4433" s="T3020">«Иди с ней!»; та пошла.</ta>
            <ta e="T3027" id="Seg_4434" s="T3024">Она [дочь] легла там поспать.</ta>
            <ta e="T3035" id="Seg_4435" s="T3027">Она [Хаврошечка говорит]: «Спи, глазок, спи, глазок»; её [дочкин] глаз заснул.</ta>
            <ta e="T3041" id="Seg_4436" s="T3035">Корова снова для неё соткала, она [это] взяла. </ta>
            <ta e="T3044" id="Seg_4437" s="T3041">Она принесла рулоны холста.</ta>
            <ta e="T3047" id="Seg_4438" s="T3044">Отдала этой женщине.</ta>
            <ta e="T3053" id="Seg_4439" s="T3047">Потом эта женщина посылает другую дочь.</ta>
            <ta e="T3058" id="Seg_4440" s="T3053">«Пойди [посмотри,] что она там делает?»</ta>
            <ta e="T3061" id="Seg_4441" s="T3058">Эта дочь пошла.</ta>
            <ta e="T3063" id="Seg_4442" s="T3061">Легла там.</ta>
            <ta e="T3067" id="Seg_4443" s="T3063">Она [Хаврошечка] её усыпила.</ta>
            <ta e="T3075" id="Seg_4444" s="T3067">«Спи, глазок, спи, другой глазок»; её глазки заснули.</ta>
            <ta e="T3078" id="Seg_4445" s="T3075">Корова [всё] для неё сделала.</ta>
            <ta e="T3086" id="Seg_4446" s="T3078">Она наткала холста и дала [Хаврошечке] рулоны, тогда она пришла.</ta>
            <ta e="T3090" id="Seg_4447" s="T3086">Она [хозяйка] очень рассердилась.</ta>
            <ta e="T3096" id="Seg_4448" s="T3090">Тогда она послала третью дочь, с тремя глазами.</ta>
            <ta e="T3099" id="Seg_4449" s="T3096">Та дочь пошла.</ta>
            <ta e="T3102" id="Seg_4450" s="T3099">И легла, спит.</ta>
            <ta e="T3108" id="Seg_4451" s="T3102">Хаврошечка тоже ей песню поёт.</ta>
            <ta e="T3116" id="Seg_4452" s="T3108">«Спи, глазок, спи, второй [=два]»; а про третий глаз забыла.</ta>
            <ta e="T3125" id="Seg_4453" s="T3116">Потом два её глаза заснули, а один глаз всё видел.</ta>
            <ta e="T3128" id="Seg_4454" s="T3125">Потом она вернулась домой.</ta>
            <ta e="T3129" id="Seg_4455" s="T3128">Говорит:</ta>
            <ta e="T3132" id="Seg_4456" s="T3129">«Корова для неё [всё] делает».</ta>
            <ta e="T3135" id="Seg_4457" s="T3132">Потом утром она [хозяйка] встала.</ta>
            <ta e="T3141" id="Seg_4458" s="T3135">Потом утром она встала, говорит мужу.</ta>
            <ta e="T3143" id="Seg_4459" s="T3141">«Зарежь корову!»</ta>
            <ta e="T3146" id="Seg_4460" s="T3143">А он говорит:</ta>
            <ta e="T3151" id="Seg_4461" s="T3146">«Зачем резать, это хорошая корова.</ta>
            <ta e="T3154" id="Seg_4462" s="T3151">Много молока даёт».</ta>
            <ta e="T3161" id="Seg_4463" s="T3154">А девушка побежала [к корове]: «Она тебя зарезать хочет».</ta>
            <ta e="T3168" id="Seg_4464" s="T3161">Она [корова] говорит: «Не ешь моё мясо.</ta>
            <ta e="T3173" id="Seg_4465" s="T3168">А кости мои возьми и уходи.</ta>
            <ta e="T3175" id="Seg_4466" s="T3173">Зарой [их] в землю.</ta>
            <ta e="T3180" id="Seg_4467" s="T3175">Потом она не съела мясо. [?]</ta>
            <ta e="T3182" id="Seg_4468" s="T3180">(Собери?) кости».</ta>
            <ta e="T3186" id="Seg_4469" s="T3182">Потом муж зарезал её.</ta>
            <ta e="T3188" id="Seg_4470" s="T3186">Они съели её мясо.</ta>
            <ta e="T3193" id="Seg_4471" s="T3188">А Хаврошечка собрала её кости.</ta>
            <ta e="T3196" id="Seg_4472" s="T3193">Пошла, закопала в землю.</ta>
            <ta e="T3203" id="Seg_4473" s="T3196">Потом выросли яблоки, красивые, никто [мимо] не проходит.</ta>
            <ta e="T3206" id="Seg_4474" s="T3203">[Все] стоят и смотрят.</ta>
            <ta e="T3211" id="Seg_4475" s="T3206">Потом пришёл царь.</ta>
            <ta e="T3219" id="Seg_4476" s="T3211">«Кто мне даст это яблочко, ту замуж возьму».</ta>
            <ta e="T3223" id="Seg_4477" s="T3219">Они [три сестры] не смогли [яблоко] сорвать.</ta>
            <ta e="T3225" id="Seg_4478" s="T3223">[Яблоки] вверх поднимаются.</ta>
            <ta e="T3240" id="Seg_4479" s="T3225">Потом подошла эта Хавроша, [ветки] к ней нагнулись, она яблоко дала этому человеку.</ta>
            <ta e="T3248" id="Seg_4480" s="T3240">Он её взял замуж и [она] от них ушла.</ta>
            <ta e="T3249" id="Seg_4481" s="T3248">Хватит.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T2921" id="Seg_4482" s="T2918">One girl lived.</ta>
            <ta e="T2925" id="Seg_4483" s="T2921">She was called Khavroshechka.</ta>
            <ta e="T2929" id="Seg_4484" s="T2925">Her mother and father died.</ta>
            <ta e="T2933" id="Seg_4485" s="T2929">People [were] not good.</ta>
            <ta e="T2936" id="Seg_4486" s="T2933">They took her.</ta>
            <ta e="T2940" id="Seg_4487" s="T2936">She [Khavroshechka's mistress] has three daughters.</ta>
            <ta e="T2943" id="Seg_4488" s="T2940">They did not do anything.</ta>
            <ta e="T2948" id="Seg_4489" s="T2943">And this Khavroshechka did all the work.</ta>
            <ta e="T2955" id="Seg_4490" s="T2948">She did all kinds of work, they cannot [do] anything.</ta>
            <ta e="T2958" id="Seg_4491" s="T2955">The woman [the mistress] is always scolding.</ta>
            <ta e="T2960" id="Seg_4492" s="T2958">Once she says:</ta>
            <ta e="T2965" id="Seg_4493" s="T2960">"Go, make a lot of linen for me."</ta>
            <ta e="T2971" id="Seg_4494" s="T2965">She went to a cow and cries:</ta>
            <ta e="T2975" id="Seg_4495" s="T2971">"They are beating me hard.</ta>
            <ta e="T2981" id="Seg_4496" s="T2975">I cannot do anything to them."</ta>
            <ta e="T2990" id="Seg_4497" s="T2981">And the cow says: "Go in from one ear and come out from one (another).</ta>
            <ta e="T2992" id="Seg_4498" s="T2990">Everything will be [OK]."</ta>
            <ta e="T2997" id="Seg_4499" s="T2992">(Then) it was so. [?]</ta>
            <ta e="T3001" id="Seg_4500" s="T2997">And it [the cow] spun, made linen.</ta>
            <ta e="T3004" id="Seg_4501" s="T3001">She [Khavroshechka] brought [it] then.</ta>
            <ta e="T3008" id="Seg_4502" s="T3004">Then [she] got a lot [to do] again.</ta>
            <ta e="T3014" id="Seg_4503" s="T3008">She went to the cow again, [it] made [all] again.</ta>
            <ta e="T3018" id="Seg_4504" s="T3014">Then she [the lady] says to one of her daughters.</ta>
            <ta e="T3020" id="Seg_4505" s="T3018">To One-eyed.</ta>
            <ta e="T3024" id="Seg_4506" s="T3020">"Go with her!"; she went.</ta>
            <ta e="T3027" id="Seg_4507" s="T3024">She [the daughter] lied down to sleep there.</ta>
            <ta e="T3035" id="Seg_4508" s="T3027">She [Khavroshechka says]: "Sleep, eye, sleep, eye"; her eye fell asleep.</ta>
            <ta e="T3041" id="Seg_4509" s="T3035">The cow spun for her again, she took [it].</ta>
            <ta e="T3044" id="Seg_4510" s="T3041">She brought rolls of linen.</ta>
            <ta e="T3047" id="Seg_4511" s="T3044">Gave to this woman.</ta>
            <ta e="T3053" id="Seg_4512" s="T3047">Then the woman sends her other daughter.</ta>
            <ta e="T3058" id="Seg_4513" s="T3053">"Go [to see] what she does there?"</ta>
            <ta e="T3061" id="Seg_4514" s="T3058">The daughter went.</ta>
            <ta e="T3063" id="Seg_4515" s="T3061">She lied there.</ta>
            <ta e="T3067" id="Seg_4516" s="T3063">She [Khavroshechka] made her fall asleep.</ta>
            <ta e="T3075" id="Seg_4517" s="T3067">"Sleep, eye, sleep, another eye"; her eyes fell asleep.</ta>
            <ta e="T3078" id="Seg_4518" s="T3075">The cow did [everything] for her.</ta>
            <ta e="T3086" id="Seg_4519" s="T3078">It spun the linen and gave the rolls [to Khavroshechka], then she came.</ta>
            <ta e="T3090" id="Seg_4520" s="T3086">She [the lady] got very angry.</ta>
            <ta e="T3096" id="Seg_4521" s="T3090">Then she sent the third daughter, [one] with three eyes.</ta>
            <ta e="T3099" id="Seg_4522" s="T3096">The daughter went.</ta>
            <ta e="T3102" id="Seg_4523" s="T3099">And lied down, is sleeping.</ta>
            <ta e="T3108" id="Seg_4524" s="T3102">Khavrosetshka also sings her a song.</ta>
            <ta e="T3116" id="Seg_4525" s="T3108">"Sleep, [one] eye, sleep, two"; but she forgot her third eye.</ta>
            <ta e="T3125" id="Seg_4526" s="T3116">Then two of her eyes slept, and one of her eyes saw everything.</ta>
            <ta e="T3128" id="Seg_4527" s="T3125">Then she came home.</ta>
            <ta e="T3129" id="Seg_4528" s="T3128">She says:</ta>
            <ta e="T3132" id="Seg_4529" s="T3129">"The cow makes [it] for her."</ta>
            <ta e="T3135" id="Seg_4530" s="T3132">Then in the morning she [the lady] got up. </ta>
            <ta e="T3141" id="Seg_4531" s="T3135">Then in the morning she got up, tells the husband.</ta>
            <ta e="T3143" id="Seg_4532" s="T3141">"Kill the cow!"</ta>
            <ta e="T3146" id="Seg_4533" s="T3143">And he says:</ta>
            <ta e="T3151" id="Seg_4534" s="T3146">“Why to kill it, it is a good cow.</ta>
            <ta e="T3154" id="Seg_4535" s="T3151">It gives a lot of milk.”</ta>
            <ta e="T3161" id="Seg_4536" s="T3154">And the girl ran [to the cow]: "[She] wants to kill you!"</ta>
            <ta e="T3168" id="Seg_4537" s="T3161">It says: “Don't eat my meat.</ta>
            <ta e="T3173" id="Seg_4538" s="T3168">But take my bones and go [away].</ta>
            <ta e="T3175" id="Seg_4539" s="T3173">Dig into the ground.</ta>
            <ta e="T3180" id="Seg_4540" s="T3175">Then she didn't eat the meat. [?]</ta>
            <ta e="T3182" id="Seg_4541" s="T3180">(Gather?) the bones.”</ta>
            <ta e="T3186" id="Seg_4542" s="T3182">Then her husband slaughtered it.</ta>
            <ta e="T3188" id="Seg_4543" s="T3186">They ate its meat.</ta>
            <ta e="T3193" id="Seg_4544" s="T3188">And Khavroshechka gathered her bones.</ta>
            <ta e="T3196" id="Seg_4545" s="T3193">She went, dug into the ground.</ta>
            <ta e="T3203" id="Seg_4546" s="T3196">Then apples are growing, beautiful, nobody goes [by].</ta>
            <ta e="T3206" id="Seg_4547" s="T3203">[Everybody] stays and looks.</ta>
            <ta e="T3211" id="Seg_4548" s="T3206">Then a king comes.</ta>
            <ta e="T3219" id="Seg_4549" s="T3211">"Who will give me this apple, I will marry.</ta>
            <ta e="T3223" id="Seg_4550" s="T3219">They [the thee sisters] could not take [apples].</ta>
            <ta e="T3225" id="Seg_4551" s="T3223">They are going up [the apples].</ta>
            <ta e="T3240" id="Seg_4552" s="T3225">Then the Khavrosha came, [a branch] bent down towards her, she gave the apple to this man.</ta>
            <ta e="T3248" id="Seg_4553" s="T3240">She was taken by that man as a wife and went away from them.</ta>
            <ta e="T3249" id="Seg_4554" s="T3248">Enough.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T2921" id="Seg_4555" s="T2918">Es lebte ein Mädchen.</ta>
            <ta e="T2925" id="Seg_4556" s="T2921">Sie hieß Khavroschechka. </ta>
            <ta e="T2929" id="Seg_4557" s="T2925">Ihre Mutter und ihr Vater starben.</ta>
            <ta e="T2933" id="Seg_4558" s="T2929">Leute [waren] nicht gut.</ta>
            <ta e="T2936" id="Seg_4559" s="T2933">Sie nahmen sie.</ta>
            <ta e="T2940" id="Seg_4560" s="T2936">Sie [Khavroschechkas Herrin] hat drei Töchter.</ta>
            <ta e="T2943" id="Seg_4561" s="T2940">Sie machten nichts.</ta>
            <ta e="T2948" id="Seg_4562" s="T2943">Sondern diese Khavroschechka machte die ganze Arbeit</ta>
            <ta e="T2955" id="Seg_4563" s="T2948">Sie machte aller Art von Arbeit, sie konnten nichts [tun].</ta>
            <ta e="T2958" id="Seg_4564" s="T2955">Die Frau [Khavroschechkas Herrin] ist als am Schimpfen.</ta>
            <ta e="T2960" id="Seg_4565" s="T2958">Einmal sagt sie:</ta>
            <ta e="T2965" id="Seg_4566" s="T2960">„ Geh, mach mir viel Leinen.“</ta>
            <ta e="T2971" id="Seg_4567" s="T2965">Sie ging zu einer Kuh und weint:</ta>
            <ta e="T2975" id="Seg_4568" s="T2971">„Sie schlagen mich schwer.</ta>
            <ta e="T2981" id="Seg_4569" s="T2975">Ich kann ihnen nichts antun.“</ta>
            <ta e="T2990" id="Seg_4570" s="T2981">Und die Kuh sagt: „Schlüpf in ein Ohr hinein und komm aus ein (anderes) hinaus.</ta>
            <ta e="T2992" id="Seg_4571" s="T2990">Alles wird [gut] sein.“</ta>
            <ta e="T2997" id="Seg_4572" s="T2992">(Dann) war es so. [?]</ta>
            <ta e="T3001" id="Seg_4573" s="T2997">Und es [die Kuh] spann, machte Leinen.</ta>
            <ta e="T3004" id="Seg_4574" s="T3001">Dann brachte sie [Khavroschechka] [es]. </ta>
            <ta e="T3008" id="Seg_4575" s="T3004">Dann bekam [sie] wieder viel [zu machen].</ta>
            <ta e="T3014" id="Seg_4576" s="T3008">Sie ging wieder zur Kuh, [es] machte wieder [alles].</ta>
            <ta e="T3018" id="Seg_4577" s="T3014">Dann sagt sie [die Herrin] zu einer ihren Töchtern.</ta>
            <ta e="T3020" id="Seg_4578" s="T3018">Zur Einäugigen.</ta>
            <ta e="T3024" id="Seg_4579" s="T3020">„Geh mit ihr mit!“; sie ging.</ta>
            <ta e="T3027" id="Seg_4580" s="T3024">Sie [die Tochter] legte sich dort zum schlafen hin.</ta>
            <ta e="T3035" id="Seg_4581" s="T3027">Sie [Khavroschechka sagt]: „Schlaf, Auge, schlaf, Auge“, ihr Auge schlief ein.</ta>
            <ta e="T3041" id="Seg_4582" s="T3035">Die Kuh span wieder für sie, sie nahm [es].</ta>
            <ta e="T3044" id="Seg_4583" s="T3041">Sie brachte Rollen von Leinen.</ta>
            <ta e="T3047" id="Seg_4584" s="T3044">Gab sie dieser Frau.</ta>
            <ta e="T3053" id="Seg_4585" s="T3047">Dann schickte die Frau ihre andere Tochter.</ta>
            <ta e="T3058" id="Seg_4586" s="T3053">„Geh [nachsehen], was sie dort machen?“</ta>
            <ta e="T3061" id="Seg_4587" s="T3058">Die Tochter ging.</ta>
            <ta e="T3063" id="Seg_4588" s="T3061">Sie lag dort.</ta>
            <ta e="T3067" id="Seg_4589" s="T3063">Sie [Khavroschechka] ließ sie einschlafen.</ta>
            <ta e="T3075" id="Seg_4590" s="T3067">„Schlaf, Auge, schlaf, anderes Auge“; ihre Augen schliefen ein.</ta>
            <ta e="T3078" id="Seg_4591" s="T3075">Die Kuh machte [alles] für sie.</ta>
            <ta e="T3086" id="Seg_4592" s="T3078">Es span den Leinen und gab [Khavroshechka] die Rollen, dann kam sie.</ta>
            <ta e="T3090" id="Seg_4593" s="T3086">Sie [die Herrin] wurde sehr böse.</ta>
            <ta e="T3096" id="Seg_4594" s="T3090">Dann schickte sie die dritte Tochter, [eine] mit drei Augen.</ta>
            <ta e="T3099" id="Seg_4595" s="T3096">Die Tochter ging.</ta>
            <ta e="T3102" id="Seg_4596" s="T3099">Und legte sich hin, schläft.</ta>
            <ta e="T3108" id="Seg_4597" s="T3102">Khavroshechka singt ihr auch ein Lied vor.</ta>
            <ta e="T3116" id="Seg_4598" s="T3108">„Schlaf, [ein] Auge, schlaf, zwei“, aber sie vergas das dritte Auge.</ta>
            <ta e="T3125" id="Seg_4599" s="T3116">Dan schliefen zwei ihrer Augen, und eines ihrer Augen sah alles.</ta>
            <ta e="T3128" id="Seg_4600" s="T3125">Dann kam sie nach Hause.</ta>
            <ta e="T3129" id="Seg_4601" s="T3128">Sie sagt:</ta>
            <ta e="T3132" id="Seg_4602" s="T3129">„Die Kuh macht [es] für sie.“</ta>
            <ta e="T3135" id="Seg_4603" s="T3132">Dann am Morgen stand sie [die Herrin] auf. </ta>
            <ta e="T3141" id="Seg_4604" s="T3135">Dann am Morgen stand sie auf, sagt ihrem Mann:</ta>
            <ta e="T3143" id="Seg_4605" s="T3141">„Töte die Kuh!“</ta>
            <ta e="T3146" id="Seg_4606" s="T3143">Und er sagt:</ta>
            <ta e="T3151" id="Seg_4607" s="T3146">„Warum sie töten, es ist ein gute Kuh,</ta>
            <ta e="T3154" id="Seg_4608" s="T3151">Sie gibt viel Milch.“</ta>
            <ta e="T3161" id="Seg_4609" s="T3154">Und das Mädchen rannte [zur Kuh]: „[Sie] will dich töten!“</ta>
            <ta e="T3168" id="Seg_4610" s="T3161">[Die Kuh] sagt: „Eß nicht mein Fleisch.</ta>
            <ta e="T3173" id="Seg_4611" s="T3168">Sondern nimm meine Knochen und geh [weg].</ta>
            <ta e="T3175" id="Seg_4612" s="T3173">Grab in die Erde.</ta>
            <ta e="T3180" id="Seg_4613" s="T3175">Dann aß si nicht das Fleisch. [?]</ta>
            <ta e="T3182" id="Seg_4614" s="T3180">(Sammelt?) die Knochen.“</ta>
            <ta e="T3186" id="Seg_4615" s="T3182">Dann schlachtete ihr Mann sie.</ta>
            <ta e="T3188" id="Seg_4616" s="T3186">Sie aßen ihr Fleisch.</ta>
            <ta e="T3193" id="Seg_4617" s="T3188">Und Khavroshechka sammelte ihre Knochen.</ta>
            <ta e="T3196" id="Seg_4618" s="T3193">Sie ging, grub in die Erde.</ta>
            <ta e="T3203" id="Seg_4619" s="T3196">Dann wachsen Äpfel, schöne, niemand läuft [vorbei].</ta>
            <ta e="T3206" id="Seg_4620" s="T3203">[Alle] bleiben und schauen.</ta>
            <ta e="T3211" id="Seg_4621" s="T3206">Dann kommt der King.</ta>
            <ta e="T3219" id="Seg_4622" s="T3211">„Wer auch immer mir diesen Apfel gibt werde ich heiraten.</ta>
            <ta e="T3223" id="Seg_4623" s="T3219">Sie [die drei Schwestern] konnten nicht [die Äpfel] nehmen. </ta>
            <ta e="T3225" id="Seg_4624" s="T3223">Sie wachsen hinauf [die Äpfel]</ta>
            <ta e="T3240" id="Seg_4625" s="T3225">Dann kam die Khavrosha, [ein Ast] senkte sich zu ihr, sie gab diesem Mann den Apfel.</ta>
            <ta e="T3248" id="Seg_4626" s="T3240">Sie wurde von diesem Mann zur Frau genommen, und ging von ihnen weg.</ta>
            <ta e="T3249" id="Seg_4627" s="T3248">Genug.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T2921" id="Seg_4628" s="T2918">[GVY:] The Russian tale "Khavroshechka", see e.g. http://hyaenidae.narod.ru/story1/047.html</ta>
            <ta e="T2940" id="Seg_4629" s="T2936">[GVY:] Khavroshechka's mistress was not yet mentioned, but we know her from the story. Note the Russian possessive construction, lit. "by her were…"</ta>
            <ta e="T2965" id="Seg_4630" s="T2960">[KlT:] Russian part-gen. ending -u used for mass nouns.</ta>
            <ta e="T2990" id="Seg_4631" s="T2981">[GVY:] An irregular form kuʔdə, should be kunə? Onʼiʔtə can be Lative, then literally 'go out through ("in") another'; or an (irregular?) Ablative. || ‎[KlT:] Onʼittə ABL.3SG.</ta>
            <ta e="T2992" id="Seg_4632" s="T2990">[KlT:] Все будет.</ta>
            <ta e="T3001" id="Seg_4633" s="T2997">[AAV] kx- in "xolstə"</ta>
            <ta e="T3020" id="Seg_4634" s="T3018">[AAV] One daughter was one-eyed, the other two-eyed and the third three-eyed.</ta>
            <ta e="T3161" id="Seg_4635" s="T3154">[GVY:] Here băʔsittə is pronounced without glottal stop.</ta>
            <ta e="T3180" id="Seg_4636" s="T3175">[GVY:] Maybe a mistake instead of 'don't eat the meat"?</ta>
            <ta e="T3193" id="Seg_4637" s="T3188">[KlT:] Why 3PL?</ta>
            <ta e="T3203" id="Seg_4638" s="T3196">[GVY:] That is, nobody can pass by, everybody stops to look at the apple tree.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T2918" />
            <conversion-tli id="T2919" />
            <conversion-tli id="T2920" />
            <conversion-tli id="T2921" />
            <conversion-tli id="T2922" />
            <conversion-tli id="T2923" />
            <conversion-tli id="T2924" />
            <conversion-tli id="T2925" />
            <conversion-tli id="T2926" />
            <conversion-tli id="T2927" />
            <conversion-tli id="T2928" />
            <conversion-tli id="T2929" />
            <conversion-tli id="T2930" />
            <conversion-tli id="T2931" />
            <conversion-tli id="T2932" />
            <conversion-tli id="T2933" />
            <conversion-tli id="T2934" />
            <conversion-tli id="T2935" />
            <conversion-tli id="T2936" />
            <conversion-tli id="T2937" />
            <conversion-tli id="T2938" />
            <conversion-tli id="T2939" />
            <conversion-tli id="T2940" />
            <conversion-tli id="T2941" />
            <conversion-tli id="T2942" />
            <conversion-tli id="T2943" />
            <conversion-tli id="T2944" />
            <conversion-tli id="T2945" />
            <conversion-tli id="T2946" />
            <conversion-tli id="T2947" />
            <conversion-tli id="T2948" />
            <conversion-tli id="T2949" />
            <conversion-tli id="T2950" />
            <conversion-tli id="T2951" />
            <conversion-tli id="T2952" />
            <conversion-tli id="T2953" />
            <conversion-tli id="T2954" />
            <conversion-tli id="T2955" />
            <conversion-tli id="T2956" />
            <conversion-tli id="T2957" />
            <conversion-tli id="T2958" />
            <conversion-tli id="T2959" />
            <conversion-tli id="T2960" />
            <conversion-tli id="T2961" />
            <conversion-tli id="T2962" />
            <conversion-tli id="T2963" />
            <conversion-tli id="T2964" />
            <conversion-tli id="T2965" />
            <conversion-tli id="T2966" />
            <conversion-tli id="T2967" />
            <conversion-tli id="T2968" />
            <conversion-tli id="T2969" />
            <conversion-tli id="T2970" />
            <conversion-tli id="T2971" />
            <conversion-tli id="T2972" />
            <conversion-tli id="T2973" />
            <conversion-tli id="T2974" />
            <conversion-tli id="T2975" />
            <conversion-tli id="T2976" />
            <conversion-tli id="T2977" />
            <conversion-tli id="T2978" />
            <conversion-tli id="T2979" />
            <conversion-tli id="T2980" />
            <conversion-tli id="T2981" />
            <conversion-tli id="T2982" />
            <conversion-tli id="T2983" />
            <conversion-tli id="T2984" />
            <conversion-tli id="T2985" />
            <conversion-tli id="T2986" />
            <conversion-tli id="T2987" />
            <conversion-tli id="T2988" />
            <conversion-tli id="T2989" />
            <conversion-tli id="T2990" />
            <conversion-tli id="T2991" />
            <conversion-tli id="T2992" />
            <conversion-tli id="T2993" />
            <conversion-tli id="T2994" />
            <conversion-tli id="T2995" />
            <conversion-tli id="T2996" />
            <conversion-tli id="T2997" />
            <conversion-tli id="T2998" />
            <conversion-tli id="T2999" />
            <conversion-tli id="T3000" />
            <conversion-tli id="T3001" />
            <conversion-tli id="T3002" />
            <conversion-tli id="T3003" />
            <conversion-tli id="T3004" />
            <conversion-tli id="T3005" />
            <conversion-tli id="T3006" />
            <conversion-tli id="T3007" />
            <conversion-tli id="T3008" />
            <conversion-tli id="T3009" />
            <conversion-tli id="T3010" />
            <conversion-tli id="T3011" />
            <conversion-tli id="T3012" />
            <conversion-tli id="T3013" />
            <conversion-tli id="T3014" />
            <conversion-tli id="T3015" />
            <conversion-tli id="T3016" />
            <conversion-tli id="T3017" />
            <conversion-tli id="T3018" />
            <conversion-tli id="T3019" />
            <conversion-tli id="T3020" />
            <conversion-tli id="T3021" />
            <conversion-tli id="T3022" />
            <conversion-tli id="T3023" />
            <conversion-tli id="T3024" />
            <conversion-tli id="T3025" />
            <conversion-tli id="T3026" />
            <conversion-tli id="T3027" />
            <conversion-tli id="T3028" />
            <conversion-tli id="T3029" />
            <conversion-tli id="T3030" />
            <conversion-tli id="T3031" />
            <conversion-tli id="T3032" />
            <conversion-tli id="T3033" />
            <conversion-tli id="T3034" />
            <conversion-tli id="T3035" />
            <conversion-tli id="T3036" />
            <conversion-tli id="T3037" />
            <conversion-tli id="T3038" />
            <conversion-tli id="T3039" />
            <conversion-tli id="T3040" />
            <conversion-tli id="T3041" />
            <conversion-tli id="T3042" />
            <conversion-tli id="T3043" />
            <conversion-tli id="T3044" />
            <conversion-tli id="T3045" />
            <conversion-tli id="T3046" />
            <conversion-tli id="T3047" />
            <conversion-tli id="T3048" />
            <conversion-tli id="T3049" />
            <conversion-tli id="T3050" />
            <conversion-tli id="T3051" />
            <conversion-tli id="T3052" />
            <conversion-tli id="T3053" />
            <conversion-tli id="T3054" />
            <conversion-tli id="T3055" />
            <conversion-tli id="T3056" />
            <conversion-tli id="T3057" />
            <conversion-tli id="T3058" />
            <conversion-tli id="T3059" />
            <conversion-tli id="T3060" />
            <conversion-tli id="T3061" />
            <conversion-tli id="T3062" />
            <conversion-tli id="T3063" />
            <conversion-tli id="T3064" />
            <conversion-tli id="T3065" />
            <conversion-tli id="T3066" />
            <conversion-tli id="T3067" />
            <conversion-tli id="T3068" />
            <conversion-tli id="T3069" />
            <conversion-tli id="T3070" />
            <conversion-tli id="T3071" />
            <conversion-tli id="T3072" />
            <conversion-tli id="T3073" />
            <conversion-tli id="T3074" />
            <conversion-tli id="T3075" />
            <conversion-tli id="T3076" />
            <conversion-tli id="T3077" />
            <conversion-tli id="T3078" />
            <conversion-tli id="T3079" />
            <conversion-tli id="T3080" />
            <conversion-tli id="T3081" />
            <conversion-tli id="T3082" />
            <conversion-tli id="T3083" />
            <conversion-tli id="T3084" />
            <conversion-tli id="T3085" />
            <conversion-tli id="T3086" />
            <conversion-tli id="T3087" />
            <conversion-tli id="T3088" />
            <conversion-tli id="T3089" />
            <conversion-tli id="T3090" />
            <conversion-tli id="T3091" />
            <conversion-tli id="T3092" />
            <conversion-tli id="T3093" />
            <conversion-tli id="T3094" />
            <conversion-tli id="T3095" />
            <conversion-tli id="T3096" />
            <conversion-tli id="T3097" />
            <conversion-tli id="T3098" />
            <conversion-tli id="T3099" />
            <conversion-tli id="T3100" />
            <conversion-tli id="T3101" />
            <conversion-tli id="T3102" />
            <conversion-tli id="T3103" />
            <conversion-tli id="T3104" />
            <conversion-tli id="T3105" />
            <conversion-tli id="T3106" />
            <conversion-tli id="T3107" />
            <conversion-tli id="T3108" />
            <conversion-tli id="T3109" />
            <conversion-tli id="T3110" />
            <conversion-tli id="T3111" />
            <conversion-tli id="T3112" />
            <conversion-tli id="T3113" />
            <conversion-tli id="T3114" />
            <conversion-tli id="T3115" />
            <conversion-tli id="T3116" />
            <conversion-tli id="T3117" />
            <conversion-tli id="T3118" />
            <conversion-tli id="T3119" />
            <conversion-tli id="T3120" />
            <conversion-tli id="T3121" />
            <conversion-tli id="T3122" />
            <conversion-tli id="T3123" />
            <conversion-tli id="T3124" />
            <conversion-tli id="T3125" />
            <conversion-tli id="T3126" />
            <conversion-tli id="T3127" />
            <conversion-tli id="T3128" />
            <conversion-tli id="T3129" />
            <conversion-tli id="T3130" />
            <conversion-tli id="T3131" />
            <conversion-tli id="T3132" />
            <conversion-tli id="T3133" />
            <conversion-tli id="T3134" />
            <conversion-tli id="T3135" />
            <conversion-tli id="T3136" />
            <conversion-tli id="T3137" />
            <conversion-tli id="T3138" />
            <conversion-tli id="T3139" />
            <conversion-tli id="T3140" />
            <conversion-tli id="T3141" />
            <conversion-tli id="T3142" />
            <conversion-tli id="T3143" />
            <conversion-tli id="T3144" />
            <conversion-tli id="T3145" />
            <conversion-tli id="T3146" />
            <conversion-tli id="T3147" />
            <conversion-tli id="T3148" />
            <conversion-tli id="T3149" />
            <conversion-tli id="T3150" />
            <conversion-tli id="T3151" />
            <conversion-tli id="T3152" />
            <conversion-tli id="T3153" />
            <conversion-tli id="T3154" />
            <conversion-tli id="T3155" />
            <conversion-tli id="T3156" />
            <conversion-tli id="T3157" />
            <conversion-tli id="T3158" />
            <conversion-tli id="T3159" />
            <conversion-tli id="T3160" />
            <conversion-tli id="T3161" />
            <conversion-tli id="T3162" />
            <conversion-tli id="T3163" />
            <conversion-tli id="T3164" />
            <conversion-tli id="T3165" />
            <conversion-tli id="T3166" />
            <conversion-tli id="T3167" />
            <conversion-tli id="T3168" />
            <conversion-tli id="T3169" />
            <conversion-tli id="T3170" />
            <conversion-tli id="T3171" />
            <conversion-tli id="T3172" />
            <conversion-tli id="T3173" />
            <conversion-tli id="T3174" />
            <conversion-tli id="T3175" />
            <conversion-tli id="T3176" />
            <conversion-tli id="T3177" />
            <conversion-tli id="T3178" />
            <conversion-tli id="T3179" />
            <conversion-tli id="T3180" />
            <conversion-tli id="T3181" />
            <conversion-tli id="T3182" />
            <conversion-tli id="T3183" />
            <conversion-tli id="T3184" />
            <conversion-tli id="T3185" />
            <conversion-tli id="T3186" />
            <conversion-tli id="T3187" />
            <conversion-tli id="T3188" />
            <conversion-tli id="T3189" />
            <conversion-tli id="T3190" />
            <conversion-tli id="T3191" />
            <conversion-tli id="T3192" />
            <conversion-tli id="T3193" />
            <conversion-tli id="T3194" />
            <conversion-tli id="T3195" />
            <conversion-tli id="T3196" />
            <conversion-tli id="T3197" />
            <conversion-tli id="T3198" />
            <conversion-tli id="T3199" />
            <conversion-tli id="T3200" />
            <conversion-tli id="T3201" />
            <conversion-tli id="T3202" />
            <conversion-tli id="T3203" />
            <conversion-tli id="T3204" />
            <conversion-tli id="T3205" />
            <conversion-tli id="T3206" />
            <conversion-tli id="T3207" />
            <conversion-tli id="T3208" />
            <conversion-tli id="T3209" />
            <conversion-tli id="T3210" />
            <conversion-tli id="T3211" />
            <conversion-tli id="T3212" />
            <conversion-tli id="T3213" />
            <conversion-tli id="T3214" />
            <conversion-tli id="T3215" />
            <conversion-tli id="T3216" />
            <conversion-tli id="T3217" />
            <conversion-tli id="T3218" />
            <conversion-tli id="T3219" />
            <conversion-tli id="T3220" />
            <conversion-tli id="T3221" />
            <conversion-tli id="T3222" />
            <conversion-tli id="T3223" />
            <conversion-tli id="T3224" />
            <conversion-tli id="T3225" />
            <conversion-tli id="T3226" />
            <conversion-tli id="T3227" />
            <conversion-tli id="T3228" />
            <conversion-tli id="T3229" />
            <conversion-tli id="T3230" />
            <conversion-tli id="T3231" />
            <conversion-tli id="T3232" />
            <conversion-tli id="T3233" />
            <conversion-tli id="T3234" />
            <conversion-tli id="T3235" />
            <conversion-tli id="T3236" />
            <conversion-tli id="T3237" />
            <conversion-tli id="T3238" />
            <conversion-tli id="T3239" />
            <conversion-tli id="T3240" />
            <conversion-tli id="T3241" />
            <conversion-tli id="T3242" />
            <conversion-tli id="T3243" />
            <conversion-tli id="T3244" />
            <conversion-tli id="T3245" />
            <conversion-tli id="T3246" />
            <conversion-tli id="T3247" />
            <conversion-tli id="T3248" />
            <conversion-tli id="T3249" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
