<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDFDF69D8C-25D0-1576-5FFE-8D9536676008">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_UnluckyDog_flk.wav" />
         <referenced-file url="PKZ_196X_UnluckyDog_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_UnluckyDog_flk\PKZ_196X_UnluckyDog_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">195</ud-information>
            <ud-information attribute-name="# HIAT:w">124</ud-information>
            <ud-information attribute-name="# e">124</ud-information>
            <ud-information attribute-name="# HIAT:u">26</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T2794" time="0.006" type="appl" />
         <tli id="T2795" time="1.245" type="appl" />
         <tli id="T2796" time="3.4665028209747772" />
         <tli id="T2797" time="4.687" type="appl" />
         <tli id="T2798" time="5.675" type="appl" />
         <tli id="T2799" time="6.664" type="appl" />
         <tli id="T2800" time="7.652" type="appl" />
         <tli id="T2801" time="8.64" type="appl" />
         <tli id="T2802" time="9.899532094514507" />
         <tli id="T2803" time="10.666" type="appl" />
         <tli id="T2804" time="13.106047203916177" />
         <tli id="T2805" time="14.138" type="appl" />
         <tli id="T2806" time="14.977" type="appl" />
         <tli id="T2807" time="15.716" type="appl" />
         <tli id="T2808" time="16.456" type="appl" />
         <tli id="T2809" time="17.195" type="appl" />
         <tli id="T2810" time="17.861" type="appl" />
         <tli id="T2811" time="18.528" type="appl" />
         <tli id="T2812" time="19.194" type="appl" />
         <tli id="T2813" time="19.86" type="appl" />
         <tli id="T2814" time="20.526" type="appl" />
         <tli id="T2815" time="21.193" type="appl" />
         <tli id="T2816" time="21.859" type="appl" />
         <tli id="T2817" time="22.525" type="appl" />
         <tli id="T2818" time="23.191" type="appl" />
         <tli id="T2819" time="23.858" type="appl" />
         <tli id="T2820" time="24.524" type="appl" />
         <tli id="T2821" time="27.80535243516499" />
         <tli id="T2822" time="29.219" type="appl" />
         <tli id="T2823" time="30.285" type="appl" />
         <tli id="T2824" time="31.351" type="appl" />
         <tli id="T2825" time="32.418" type="appl" />
         <tli id="T2826" time="33.484" type="appl" />
         <tli id="T2827" time="34.55" type="appl" />
         <tli id="T2828" time="35.236" type="appl" />
         <tli id="T2829" time="35.923" type="appl" />
         <tli id="T2830" time="36.609" type="appl" />
         <tli id="T2831" time="37.296" type="appl" />
         <tli id="T2832" time="37.982" type="appl" />
         <tli id="T2833" time="38.427" type="appl" />
         <tli id="T2834" time="38.873" type="appl" />
         <tli id="T2835" time="39.53813121384886" />
         <tli id="T2836" time="40.15" type="appl" />
         <tli id="T2837" time="40.662" type="appl" />
         <tli id="T2838" time="41.175" type="appl" />
         <tli id="T2839" time="41.688" type="appl" />
         <tli id="T2840" time="42.2" type="appl" />
         <tli id="T2841" time="43.304619855869525" />
         <tli id="T2842" time="44.108" type="appl" />
         <tli id="T2843" time="46.42447239474682" />
         <tli id="T2844" time="46.776" type="appl" />
         <tli id="T2845" time="47.242" type="appl" />
         <tli id="T2846" time="47.707" type="appl" />
         <tli id="T2847" time="48.172" type="appl" />
         <tli id="T2848" time="48.888" type="appl" />
         <tli id="T2849" time="49.605" type="appl" />
         <tli id="T2850" time="50.322" type="appl" />
         <tli id="T2851" time="51.21757917990234" />
         <tli id="T2852" time="51.768" type="appl" />
         <tli id="T2853" time="52.295" type="appl" />
         <tli id="T2854" time="53.19081924722644" />
         <tli id="T2855" time="53.97" type="appl" />
         <tli id="T2856" time="54.856" type="appl" />
         <tli id="T2857" time="55.742" type="appl" />
         <tli id="T2858" time="56.628" type="appl" />
         <tli id="T2859" time="57.514" type="appl" />
         <tli id="T2860" time="58.097" type="appl" />
         <tli id="T2861" time="58.556" type="appl" />
         <tli id="T2862" time="59.016" type="appl" />
         <tli id="T2863" time="60.21048745970037" />
         <tli id="T2864" time="61.435" type="appl" />
         <tli id="T2865" time="62.626" type="appl" />
         <tli id="T2866" time="63.818" type="appl" />
         <tli id="T2867" time="64.514" type="appl" />
         <tli id="T2868" time="65.21" type="appl" />
         <tli id="T2869" time="66.43019348271858" />
         <tli id="T2870" time="67.179" type="appl" />
         <tli id="T2871" time="67.964" type="appl" />
         <tli id="T2872" time="68.748" type="appl" />
         <tli id="T2873" time="69.265" type="appl" />
         <tli id="T2874" time="69.781" type="appl" />
         <tli id="T2875" time="70.298" type="appl" />
         <tli id="T2876" time="70.815" type="appl" />
         <tli id="T2877" time="71.331" type="appl" />
         <tli id="T2878" time="72.47657436468803" />
         <tli id="T2879" time="73.145" type="appl" />
         <tli id="T2880" time="73.721" type="appl" />
         <tli id="T2881" time="74.297" type="appl" />
         <tli id="T2882" time="74.873" type="appl" />
         <tli id="T2883" time="75.449" type="appl" />
         <tli id="T2884" time="76.026" type="appl" />
         <tli id="T2885" time="76.602" type="appl" />
         <tli id="T2886" time="77.178" type="appl" />
         <tli id="T2887" time="77.754" type="appl" />
         <tli id="T2888" time="78.33" type="appl" />
         <tli id="T2889" time="79.94955448452019" />
         <tli id="T2890" time="80.755" type="appl" />
         <tli id="T2891" time="81.663" type="appl" />
         <tli id="T2892" time="82.57" type="appl" />
         <tli id="T2893" time="83.477" type="appl" />
         <tli id="T2894" time="84.384" type="appl" />
         <tli id="T2895" time="85.292" type="appl" />
         <tli id="T2896" time="86.199" type="appl" />
         <tli id="T2897" time="87.106" type="appl" />
         <tli id="T2898" time="88.013" type="appl" />
         <tli id="T2899" time="88.921" type="appl" />
         <tli id="T2900" time="89.828" type="appl" />
         <tli id="T2901" time="90.735" type="appl" />
         <tli id="T2902" time="91.642" type="appl" />
         <tli id="T2903" time="92.55" type="appl" />
         <tli id="T2904" time="93.457" type="appl" />
         <tli id="T2905" time="94.364" type="appl" />
         <tli id="T2906" time="95.271" type="appl" />
         <tli id="T2907" time="96.179" type="appl" />
         <tli id="T2908" time="96.77542586940545" />
         <tli id="T2909" time="98.127" type="appl" />
         <tli id="T2910" time="99.169" type="appl" />
         <tli id="T2911" time="100.21" type="appl" />
         <tli id="T2912" time="100.959" type="appl" />
         <tli id="T2913" time="101.708" type="appl" />
         <tli id="T2914" time="102.458" type="appl" />
         <tli id="T2915" time="103.207" type="appl" />
         <tli id="T2916" time="103.956" type="appl" />
         <tli id="T2917" time="104.705" type="appl" />
         <tli id="T2918" time="105.455" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T2918" id="Seg_0" n="sc" s="T2794">
               <ts e="T2796" id="Seg_2" n="HIAT:u" s="T2794">
                  <ts e="T2795" id="Seg_4" n="HIAT:w" s="T2794">Šobi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2796" id="Seg_7" n="HIAT:w" s="T2795">men</ts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2802" id="Seg_11" n="HIAT:u" s="T2796">
                  <nts id="Seg_12" n="HIAT:ip">(</nts>
                  <ts e="T2797" id="Seg_14" n="HIAT:w" s="T2796">M-</ts>
                  <nts id="Seg_15" n="HIAT:ip">)</nts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2798" id="Seg_18" n="HIAT:w" s="T2797">Măndə</ts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2799" id="Seg_21" n="HIAT:w" s="T2798">inendə:</ts>
                  <nts id="Seg_22" n="HIAT:ip">"</nts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2800" id="Seg_25" n="HIAT:w" s="T2799">Măn</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2801" id="Seg_28" n="HIAT:w" s="T2800">tănan</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2802" id="Seg_31" n="HIAT:w" s="T2801">amnam</ts>
                  <nts id="Seg_32" n="HIAT:ip">!</nts>
                  <nts id="Seg_33" n="HIAT:ip">"</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2804" id="Seg_36" n="HIAT:u" s="T2802">
                  <nts id="Seg_37" n="HIAT:ip">"</nts>
                  <ts e="T2803" id="Seg_39" n="HIAT:w" s="T2802">No</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2804" id="Seg_42" n="HIAT:w" s="T2803">amaʔdə</ts>
                  <nts id="Seg_43" n="HIAT:ip">.</nts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2806" id="Seg_46" n="HIAT:u" s="T2804">
                  <ts e="T2805" id="Seg_48" n="HIAT:w" s="T2804">Kötendə</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2806" id="Seg_51" n="HIAT:w" s="T2805">amaʔ</ts>
                  <nts id="Seg_52" n="HIAT:ip">.</nts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2809" id="Seg_55" n="HIAT:u" s="T2806">
                  <ts e="T2807" id="Seg_57" n="HIAT:w" s="T2806">Dĭn</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2808" id="Seg_60" n="HIAT:w" s="T2807">uja</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2809" id="Seg_63" n="HIAT:w" s="T2808">iʔgö</ts>
                  <nts id="Seg_64" n="HIAT:ip">.</nts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2821" id="Seg_67" n="HIAT:u" s="T2809">
                  <ts e="T2810" id="Seg_69" n="HIAT:w" s="T2809">Dĭ</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_71" n="HIAT:ip">(</nts>
                  <ts e="T2811" id="Seg_73" n="HIAT:w" s="T2810">dal-</ts>
                  <nts id="Seg_74" n="HIAT:ip">)</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2812" id="Seg_77" n="HIAT:w" s="T2811">dĭm</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_79" n="HIAT:ip">(</nts>
                  <ts e="T2813" id="Seg_81" n="HIAT:w" s="T2812">a-</ts>
                  <nts id="Seg_82" n="HIAT:ip">)</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2814" id="Seg_85" n="HIAT:w" s="T2813">amzittə</ts>
                  <nts id="Seg_86" n="HIAT:ip">,</nts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_88" n="HIAT:ip">(</nts>
                  <ts e="T2815" id="Seg_90" n="HIAT:w" s="T2814">dĭ=</ts>
                  <nts id="Seg_91" n="HIAT:ip">)</nts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2816" id="Seg_94" n="HIAT:w" s="T2815">dĭ</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2817" id="Seg_97" n="HIAT:w" s="T2816">dĭm</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2818" id="Seg_100" n="HIAT:w" s="T2817">toʔnarbi</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2819" id="Seg_103" n="HIAT:w" s="T2818">bar</ts>
                  <nts id="Seg_104" n="HIAT:ip">,</nts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2820" id="Seg_107" n="HIAT:w" s="T2819">tĭmezeŋdə</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2821" id="Seg_110" n="HIAT:w" s="T2820">bar</ts>
                  <nts id="Seg_111" n="HIAT:ip">.</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2822" id="Seg_114" n="HIAT:u" s="T2821">
                  <ts e="T2822" id="Seg_116" n="HIAT:w" s="T2821">Uzleʔbəʔjə</ts>
                  <nts id="Seg_117" n="HIAT:ip">.</nts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2827" id="Seg_120" n="HIAT:u" s="T2822">
                  <ts e="T2823" id="Seg_122" n="HIAT:w" s="T2822">Dĭgəttə</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2824" id="Seg_125" n="HIAT:w" s="T2823">dĭ</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2825" id="Seg_128" n="HIAT:w" s="T2824">nuʔməluʔpi</ts>
                  <nts id="Seg_129" n="HIAT:ip">,</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2826" id="Seg_132" n="HIAT:w" s="T2825">nerelüʔpi</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2827" id="Seg_135" n="HIAT:w" s="T2826">inegəʔ</ts>
                  <nts id="Seg_136" n="HIAT:ip">.</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2832" id="Seg_139" n="HIAT:u" s="T2827">
                  <ts e="T2828" id="Seg_141" n="HIAT:w" s="T2827">Dĭgəttə</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_143" n="HIAT:ip">(</nts>
                  <ts e="T2829" id="Seg_145" n="HIAT:w" s="T2828">ka-</ts>
                  <nts id="Seg_146" n="HIAT:ip">)</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2830" id="Seg_149" n="HIAT:w" s="T2829">šonəga</ts>
                  <nts id="Seg_150" n="HIAT:ip">,</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2831" id="Seg_153" n="HIAT:w" s="T2830">poʔto</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2832" id="Seg_156" n="HIAT:w" s="T2831">šonəga</ts>
                  <nts id="Seg_157" n="HIAT:ip">.</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2835" id="Seg_160" n="HIAT:u" s="T2832">
                  <nts id="Seg_161" n="HIAT:ip">"</nts>
                  <ts e="T2833" id="Seg_163" n="HIAT:w" s="T2832">Măn</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2834" id="Seg_166" n="HIAT:w" s="T2833">tănan</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2835" id="Seg_169" n="HIAT:w" s="T2834">amnam</ts>
                  <nts id="Seg_170" n="HIAT:ip">.</nts>
                  <nts id="Seg_171" n="HIAT:ip">"</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2841" id="Seg_174" n="HIAT:u" s="T2835">
                  <nts id="Seg_175" n="HIAT:ip">"</nts>
                  <ts e="T2836" id="Seg_177" n="HIAT:w" s="T2835">Iʔ</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2837" id="Seg_180" n="HIAT:w" s="T2836">amaʔ</ts>
                  <nts id="Seg_181" n="HIAT:ip">,</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2838" id="Seg_184" n="HIAT:w" s="T2837">măn</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_186" n="HIAT:ip">(</nts>
                  <ts e="T2839" id="Seg_188" n="HIAT:w" s="T2838">kan-</ts>
                  <nts id="Seg_189" n="HIAT:ip">)</nts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2840" id="Seg_192" n="HIAT:w" s="T2839">kallam</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2841" id="Seg_195" n="HIAT:w" s="T2840">ešseŋdə</ts>
                  <nts id="Seg_196" n="HIAT:ip">.</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2843" id="Seg_199" n="HIAT:u" s="T2841">
                  <ts e="T2842" id="Seg_201" n="HIAT:w" s="T2841">Amzittə</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2843" id="Seg_204" n="HIAT:w" s="T2842">mĭlem</ts>
                  <nts id="Seg_205" n="HIAT:ip">.</nts>
                  <nts id="Seg_206" n="HIAT:ip">"</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2847" id="Seg_209" n="HIAT:u" s="T2843">
                  <ts e="T2844" id="Seg_211" n="HIAT:w" s="T2843">Kalla</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2845" id="Seg_214" n="HIAT:w" s="T2844">dʼürbi</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2846" id="Seg_217" n="HIAT:w" s="T2845">i</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2847" id="Seg_220" n="HIAT:w" s="T2846">naga</ts>
                  <nts id="Seg_221" n="HIAT:ip">.</nts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2851" id="Seg_224" n="HIAT:u" s="T2847">
                  <ts e="T2848" id="Seg_226" n="HIAT:w" s="T2847">Šonəga</ts>
                  <nts id="Seg_227" n="HIAT:ip">,</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2849" id="Seg_230" n="HIAT:w" s="T2848">šonəga</ts>
                  <nts id="Seg_231" n="HIAT:ip">,</nts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2850" id="Seg_234" n="HIAT:w" s="T2849">ular</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2851" id="Seg_237" n="HIAT:w" s="T2850">mĭŋge</ts>
                  <nts id="Seg_238" n="HIAT:ip">.</nts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2854" id="Seg_241" n="HIAT:u" s="T2851">
                  <nts id="Seg_242" n="HIAT:ip">"</nts>
                  <ts e="T2852" id="Seg_244" n="HIAT:w" s="T2851">Măn</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2853" id="Seg_247" n="HIAT:w" s="T2852">tănan</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2854" id="Seg_250" n="HIAT:w" s="T2853">amnam</ts>
                  <nts id="Seg_251" n="HIAT:ip">!</nts>
                  <nts id="Seg_252" n="HIAT:ip">"</nts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2859" id="Seg_255" n="HIAT:u" s="T2854">
                  <nts id="Seg_256" n="HIAT:ip">"</nts>
                  <ts e="T2855" id="Seg_258" n="HIAT:w" s="T2854">Măn</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2856" id="Seg_261" n="HIAT:w" s="T2855">kallam</ts>
                  <nts id="Seg_262" n="HIAT:ip">,</nts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2857" id="Seg_265" n="HIAT:w" s="T2856">üdʼüge</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2858" id="Seg_268" n="HIAT:w" s="T2857">ular</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2859" id="Seg_271" n="HIAT:w" s="T2858">bădəsʼtə</ts>
                  <nts id="Seg_272" n="HIAT:ip">"</nts>
                  <nts id="Seg_273" n="HIAT:ip">.</nts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2863" id="Seg_276" n="HIAT:u" s="T2859">
                  <ts e="T2860" id="Seg_278" n="HIAT:w" s="T2859">Kalla</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2861" id="Seg_281" n="HIAT:w" s="T2860">dʼürbi</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2862" id="Seg_284" n="HIAT:w" s="T2861">i</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2863" id="Seg_287" n="HIAT:w" s="T2862">naga</ts>
                  <nts id="Seg_288" n="HIAT:ip">.</nts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2866" id="Seg_291" n="HIAT:u" s="T2863">
                  <ts e="T2864" id="Seg_293" n="HIAT:w" s="T2863">Dĭgəttə</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2865" id="Seg_296" n="HIAT:w" s="T2864">šonəga</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2866" id="Seg_299" n="HIAT:w" s="T2865">verblʼud</ts>
                  <nts id="Seg_300" n="HIAT:ip">.</nts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2869" id="Seg_303" n="HIAT:u" s="T2866">
                  <nts id="Seg_304" n="HIAT:ip">"</nts>
                  <ts e="T2867" id="Seg_306" n="HIAT:w" s="T2866">Măn</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2868" id="Seg_309" n="HIAT:w" s="T2867">tănan</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2869" id="Seg_312" n="HIAT:w" s="T2868">amnam</ts>
                  <nts id="Seg_313" n="HIAT:ip">!</nts>
                  <nts id="Seg_314" n="HIAT:ip">"</nts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2872" id="Seg_317" n="HIAT:u" s="T2869">
                  <ts e="T2870" id="Seg_319" n="HIAT:w" s="T2869">Dĭgəttə</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2871" id="Seg_322" n="HIAT:w" s="T2870">dĭ</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2872" id="Seg_325" n="HIAT:w" s="T2871">suʔmiluʔpi</ts>
                  <nts id="Seg_326" n="HIAT:ip">.</nts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2878" id="Seg_329" n="HIAT:u" s="T2872">
                  <nts id="Seg_330" n="HIAT:ip">(</nts>
                  <ts e="T2873" id="Seg_332" n="HIAT:w" s="T2872">Tĭme</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2874" id="Seg_335" n="HIAT:w" s="T2873">g-</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2875" id="Seg_338" n="HIAT:w" s="T2874">bɨ</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2876" id="Seg_341" n="HIAT:w" s="T2875">na-</ts>
                  <nts id="Seg_342" n="HIAT:ip">)</nts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2877" id="Seg_345" n="HIAT:w" s="T2876">Tĭmezeŋdə</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2878" id="Seg_348" n="HIAT:w" s="T2877">naga</ts>
                  <nts id="Seg_349" n="HIAT:ip">.</nts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2889" id="Seg_352" n="HIAT:u" s="T2878">
                  <ts e="T2879" id="Seg_354" n="HIAT:w" s="T2878">A</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2880" id="Seg_357" n="HIAT:w" s="T2879">verblʼud</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2881" id="Seg_360" n="HIAT:w" s="T2880">nuʔməluʔpi</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2882" id="Seg_363" n="HIAT:w" s="T2881">i</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_365" n="HIAT:ip">(</nts>
                  <ts e="T2883" id="Seg_367" n="HIAT:w" s="T2882">š-</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2884" id="Seg_370" n="HIAT:w" s="T2883">i</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2885" id="Seg_373" n="HIAT:w" s="T2884">š-</ts>
                  <nts id="Seg_374" n="HIAT:ip">)</nts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2886" id="Seg_377" n="HIAT:w" s="T2885">šobi</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2887" id="Seg_380" n="HIAT:w" s="T2886">gijen</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2888" id="Seg_383" n="HIAT:w" s="T2887">il</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2889" id="Seg_386" n="HIAT:w" s="T2888">iʔgö</ts>
                  <nts id="Seg_387" n="HIAT:ip">.</nts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2908" id="Seg_390" n="HIAT:u" s="T2889">
                  <nts id="Seg_391" n="HIAT:ip">(</nts>
                  <ts e="T2890" id="Seg_393" n="HIAT:w" s="T2889">Dĭ-</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2891" id="Seg_396" n="HIAT:w" s="T2890">dĭ</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2892" id="Seg_399" n="HIAT:w" s="T2891">i-</ts>
                  <nts id="Seg_400" n="HIAT:ip">)</nts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2893" id="Seg_403" n="HIAT:w" s="T2892">Il</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2894" id="Seg_406" n="HIAT:w" s="T2893">dĭ</ts>
                  <nts id="Seg_407" n="HIAT:ip">,</nts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2895" id="Seg_410" n="HIAT:w" s="T2894">men</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2896" id="Seg_413" n="HIAT:w" s="T2895">dĭ</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2897" id="Seg_416" n="HIAT:w" s="T2896">iluʔpiʔi</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2898" id="Seg_419" n="HIAT:w" s="T2897">da</ts>
                  <nts id="Seg_420" n="HIAT:ip">,</nts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2899" id="Seg_423" n="HIAT:w" s="T2898">tăŋ</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2900" id="Seg_426" n="HIAT:w" s="T2899">münörbiʔi</ts>
                  <nts id="Seg_427" n="HIAT:ip">,</nts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2901" id="Seg_430" n="HIAT:w" s="T2900">dĭ</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2902" id="Seg_433" n="HIAT:w" s="T2901">nuʔməluʔpi</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2903" id="Seg_436" n="HIAT:w" s="T2902">da</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2904" id="Seg_439" n="HIAT:w" s="T2903">mosttə</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2905" id="Seg_442" n="HIAT:w" s="T2904">ildə</ts>
                  <nts id="Seg_443" n="HIAT:ip">,</nts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2906" id="Seg_446" n="HIAT:w" s="T2905">šaʔlaːmbi</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2907" id="Seg_449" n="HIAT:w" s="T2906">dĭʔnə</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2908" id="Seg_452" n="HIAT:w" s="T2907">bar</ts>
                  <nts id="Seg_453" n="HIAT:ip">.</nts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2911" id="Seg_456" n="HIAT:u" s="T2908">
                  <ts e="T2909" id="Seg_458" n="HIAT:w" s="T2908">Xvostə</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_460" n="HIAT:ip">(</nts>
                  <ts e="T2910" id="Seg_462" n="HIAT:w" s="T2909">i-</ts>
                  <nts id="Seg_463" n="HIAT:ip">)</nts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2911" id="Seg_466" n="HIAT:w" s="T2910">sajjaʔluʔpiʔi</ts>
                  <nts id="Seg_467" n="HIAT:ip">.</nts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2917" id="Seg_470" n="HIAT:u" s="T2911">
                  <ts e="T2912" id="Seg_472" n="HIAT:w" s="T2911">Dĭgəttə</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2913" id="Seg_475" n="HIAT:w" s="T2912">dĭ</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2914" id="Seg_478" n="HIAT:w" s="T2913">bünə</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2915" id="Seg_481" n="HIAT:w" s="T2914">saʔməluʔpi</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2916" id="Seg_484" n="HIAT:w" s="T2915">i</ts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2917" id="Seg_487" n="HIAT:w" s="T2916">külaːmbi</ts>
                  <nts id="Seg_488" n="HIAT:ip">.</nts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2918" id="Seg_491" n="HIAT:u" s="T2917">
                  <ts e="T2918" id="Seg_493" n="HIAT:w" s="T2917">Bar</ts>
                  <nts id="Seg_494" n="HIAT:ip">.</nts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T2918" id="Seg_496" n="sc" s="T2794">
               <ts e="T2795" id="Seg_498" n="e" s="T2794">Šobi </ts>
               <ts e="T2796" id="Seg_500" n="e" s="T2795">men. </ts>
               <ts e="T2797" id="Seg_502" n="e" s="T2796">(M-) </ts>
               <ts e="T2798" id="Seg_504" n="e" s="T2797">Măndə </ts>
               <ts e="T2799" id="Seg_506" n="e" s="T2798">inendə:" </ts>
               <ts e="T2800" id="Seg_508" n="e" s="T2799">Măn </ts>
               <ts e="T2801" id="Seg_510" n="e" s="T2800">tănan </ts>
               <ts e="T2802" id="Seg_512" n="e" s="T2801">amnam!" </ts>
               <ts e="T2803" id="Seg_514" n="e" s="T2802">"No </ts>
               <ts e="T2804" id="Seg_516" n="e" s="T2803">amaʔdə. </ts>
               <ts e="T2805" id="Seg_518" n="e" s="T2804">Kötendə </ts>
               <ts e="T2806" id="Seg_520" n="e" s="T2805">amaʔ. </ts>
               <ts e="T2807" id="Seg_522" n="e" s="T2806">Dĭn </ts>
               <ts e="T2808" id="Seg_524" n="e" s="T2807">uja </ts>
               <ts e="T2809" id="Seg_526" n="e" s="T2808">iʔgö. </ts>
               <ts e="T2810" id="Seg_528" n="e" s="T2809">Dĭ </ts>
               <ts e="T2811" id="Seg_530" n="e" s="T2810">(dal-) </ts>
               <ts e="T2812" id="Seg_532" n="e" s="T2811">dĭm </ts>
               <ts e="T2813" id="Seg_534" n="e" s="T2812">(a-) </ts>
               <ts e="T2814" id="Seg_536" n="e" s="T2813">amzittə, </ts>
               <ts e="T2815" id="Seg_538" n="e" s="T2814">(dĭ=) </ts>
               <ts e="T2816" id="Seg_540" n="e" s="T2815">dĭ </ts>
               <ts e="T2817" id="Seg_542" n="e" s="T2816">dĭm </ts>
               <ts e="T2818" id="Seg_544" n="e" s="T2817">toʔnarbi </ts>
               <ts e="T2819" id="Seg_546" n="e" s="T2818">bar, </ts>
               <ts e="T2820" id="Seg_548" n="e" s="T2819">tĭmezeŋdə </ts>
               <ts e="T2821" id="Seg_550" n="e" s="T2820">bar. </ts>
               <ts e="T2822" id="Seg_552" n="e" s="T2821">Uzleʔbəʔjə. </ts>
               <ts e="T2823" id="Seg_554" n="e" s="T2822">Dĭgəttə </ts>
               <ts e="T2824" id="Seg_556" n="e" s="T2823">dĭ </ts>
               <ts e="T2825" id="Seg_558" n="e" s="T2824">nuʔməluʔpi, </ts>
               <ts e="T2826" id="Seg_560" n="e" s="T2825">nerelüʔpi </ts>
               <ts e="T2827" id="Seg_562" n="e" s="T2826">inegəʔ. </ts>
               <ts e="T2828" id="Seg_564" n="e" s="T2827">Dĭgəttə </ts>
               <ts e="T2829" id="Seg_566" n="e" s="T2828">(ka-) </ts>
               <ts e="T2830" id="Seg_568" n="e" s="T2829">šonəga, </ts>
               <ts e="T2831" id="Seg_570" n="e" s="T2830">poʔto </ts>
               <ts e="T2832" id="Seg_572" n="e" s="T2831">šonəga. </ts>
               <ts e="T2833" id="Seg_574" n="e" s="T2832">"Măn </ts>
               <ts e="T2834" id="Seg_576" n="e" s="T2833">tănan </ts>
               <ts e="T2835" id="Seg_578" n="e" s="T2834">amnam." </ts>
               <ts e="T2836" id="Seg_580" n="e" s="T2835">"Iʔ </ts>
               <ts e="T2837" id="Seg_582" n="e" s="T2836">amaʔ, </ts>
               <ts e="T2838" id="Seg_584" n="e" s="T2837">măn </ts>
               <ts e="T2839" id="Seg_586" n="e" s="T2838">(kan-) </ts>
               <ts e="T2840" id="Seg_588" n="e" s="T2839">kallam </ts>
               <ts e="T2841" id="Seg_590" n="e" s="T2840">ešseŋdə. </ts>
               <ts e="T2842" id="Seg_592" n="e" s="T2841">Amzittə </ts>
               <ts e="T2843" id="Seg_594" n="e" s="T2842">mĭlem." </ts>
               <ts e="T2844" id="Seg_596" n="e" s="T2843">Kalla </ts>
               <ts e="T2845" id="Seg_598" n="e" s="T2844">dʼürbi </ts>
               <ts e="T2846" id="Seg_600" n="e" s="T2845">i </ts>
               <ts e="T2847" id="Seg_602" n="e" s="T2846">naga. </ts>
               <ts e="T2848" id="Seg_604" n="e" s="T2847">Šonəga, </ts>
               <ts e="T2849" id="Seg_606" n="e" s="T2848">šonəga, </ts>
               <ts e="T2850" id="Seg_608" n="e" s="T2849">ular </ts>
               <ts e="T2851" id="Seg_610" n="e" s="T2850">mĭŋge. </ts>
               <ts e="T2852" id="Seg_612" n="e" s="T2851">"Măn </ts>
               <ts e="T2853" id="Seg_614" n="e" s="T2852">tănan </ts>
               <ts e="T2854" id="Seg_616" n="e" s="T2853">amnam!" </ts>
               <ts e="T2855" id="Seg_618" n="e" s="T2854">"Măn </ts>
               <ts e="T2856" id="Seg_620" n="e" s="T2855">kallam, </ts>
               <ts e="T2857" id="Seg_622" n="e" s="T2856">üdʼüge </ts>
               <ts e="T2858" id="Seg_624" n="e" s="T2857">ular </ts>
               <ts e="T2859" id="Seg_626" n="e" s="T2858">bădəsʼtə". </ts>
               <ts e="T2860" id="Seg_628" n="e" s="T2859">Kalla </ts>
               <ts e="T2861" id="Seg_630" n="e" s="T2860">dʼürbi </ts>
               <ts e="T2862" id="Seg_632" n="e" s="T2861">i </ts>
               <ts e="T2863" id="Seg_634" n="e" s="T2862">naga. </ts>
               <ts e="T2864" id="Seg_636" n="e" s="T2863">Dĭgəttə </ts>
               <ts e="T2865" id="Seg_638" n="e" s="T2864">šonəga </ts>
               <ts e="T2866" id="Seg_640" n="e" s="T2865">verblʼud. </ts>
               <ts e="T2867" id="Seg_642" n="e" s="T2866">"Măn </ts>
               <ts e="T2868" id="Seg_644" n="e" s="T2867">tănan </ts>
               <ts e="T2869" id="Seg_646" n="e" s="T2868">amnam!" </ts>
               <ts e="T2870" id="Seg_648" n="e" s="T2869">Dĭgəttə </ts>
               <ts e="T2871" id="Seg_650" n="e" s="T2870">dĭ </ts>
               <ts e="T2872" id="Seg_652" n="e" s="T2871">suʔmiluʔpi. </ts>
               <ts e="T2873" id="Seg_654" n="e" s="T2872">(Tĭme </ts>
               <ts e="T2874" id="Seg_656" n="e" s="T2873">g- </ts>
               <ts e="T2875" id="Seg_658" n="e" s="T2874">bɨ </ts>
               <ts e="T2876" id="Seg_660" n="e" s="T2875">na-) </ts>
               <ts e="T2877" id="Seg_662" n="e" s="T2876">Tĭmezeŋdə </ts>
               <ts e="T2878" id="Seg_664" n="e" s="T2877">naga. </ts>
               <ts e="T2879" id="Seg_666" n="e" s="T2878">A </ts>
               <ts e="T2880" id="Seg_668" n="e" s="T2879">verblʼud </ts>
               <ts e="T2881" id="Seg_670" n="e" s="T2880">nuʔməluʔpi </ts>
               <ts e="T2882" id="Seg_672" n="e" s="T2881">i </ts>
               <ts e="T2883" id="Seg_674" n="e" s="T2882">(š- </ts>
               <ts e="T2884" id="Seg_676" n="e" s="T2883">i </ts>
               <ts e="T2885" id="Seg_678" n="e" s="T2884">š-) </ts>
               <ts e="T2886" id="Seg_680" n="e" s="T2885">šobi </ts>
               <ts e="T2887" id="Seg_682" n="e" s="T2886">gijen </ts>
               <ts e="T2888" id="Seg_684" n="e" s="T2887">il </ts>
               <ts e="T2889" id="Seg_686" n="e" s="T2888">iʔgö. </ts>
               <ts e="T2890" id="Seg_688" n="e" s="T2889">(Dĭ- </ts>
               <ts e="T2891" id="Seg_690" n="e" s="T2890">dĭ </ts>
               <ts e="T2892" id="Seg_692" n="e" s="T2891">i-) </ts>
               <ts e="T2893" id="Seg_694" n="e" s="T2892">Il </ts>
               <ts e="T2894" id="Seg_696" n="e" s="T2893">dĭ, </ts>
               <ts e="T2895" id="Seg_698" n="e" s="T2894">men </ts>
               <ts e="T2896" id="Seg_700" n="e" s="T2895">dĭ </ts>
               <ts e="T2897" id="Seg_702" n="e" s="T2896">iluʔpiʔi </ts>
               <ts e="T2898" id="Seg_704" n="e" s="T2897">da, </ts>
               <ts e="T2899" id="Seg_706" n="e" s="T2898">tăŋ </ts>
               <ts e="T2900" id="Seg_708" n="e" s="T2899">münörbiʔi, </ts>
               <ts e="T2901" id="Seg_710" n="e" s="T2900">dĭ </ts>
               <ts e="T2902" id="Seg_712" n="e" s="T2901">nuʔməluʔpi </ts>
               <ts e="T2903" id="Seg_714" n="e" s="T2902">da </ts>
               <ts e="T2904" id="Seg_716" n="e" s="T2903">mosttə </ts>
               <ts e="T2905" id="Seg_718" n="e" s="T2904">ildə, </ts>
               <ts e="T2906" id="Seg_720" n="e" s="T2905">šaʔlaːmbi </ts>
               <ts e="T2907" id="Seg_722" n="e" s="T2906">dĭʔnə </ts>
               <ts e="T2908" id="Seg_724" n="e" s="T2907">bar. </ts>
               <ts e="T2909" id="Seg_726" n="e" s="T2908">Xvostə </ts>
               <ts e="T2910" id="Seg_728" n="e" s="T2909">(i-) </ts>
               <ts e="T2911" id="Seg_730" n="e" s="T2910">sajjaʔluʔpiʔi. </ts>
               <ts e="T2912" id="Seg_732" n="e" s="T2911">Dĭgəttə </ts>
               <ts e="T2913" id="Seg_734" n="e" s="T2912">dĭ </ts>
               <ts e="T2914" id="Seg_736" n="e" s="T2913">bünə </ts>
               <ts e="T2915" id="Seg_738" n="e" s="T2914">saʔməluʔpi </ts>
               <ts e="T2916" id="Seg_740" n="e" s="T2915">i </ts>
               <ts e="T2917" id="Seg_742" n="e" s="T2916">külaːmbi. </ts>
               <ts e="T2918" id="Seg_744" n="e" s="T2917">Bar. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T2796" id="Seg_745" s="T2794">PKZ_196X_UnluckyDog_flk.001 (001)</ta>
            <ta e="T2802" id="Seg_746" s="T2796">PKZ_196X_UnluckyDog_flk.002 (002)</ta>
            <ta e="T2804" id="Seg_747" s="T2802">PKZ_196X_UnluckyDog_flk.003 (003)</ta>
            <ta e="T2806" id="Seg_748" s="T2804">PKZ_196X_UnluckyDog_flk.004 (004)</ta>
            <ta e="T2809" id="Seg_749" s="T2806">PKZ_196X_UnluckyDog_flk.005 (005)</ta>
            <ta e="T2821" id="Seg_750" s="T2809">PKZ_196X_UnluckyDog_flk.006 (006)</ta>
            <ta e="T2822" id="Seg_751" s="T2821">PKZ_196X_UnluckyDog_flk.007 (007)</ta>
            <ta e="T2827" id="Seg_752" s="T2822">PKZ_196X_UnluckyDog_flk.008 (008)</ta>
            <ta e="T2832" id="Seg_753" s="T2827">PKZ_196X_UnluckyDog_flk.009 (009)</ta>
            <ta e="T2835" id="Seg_754" s="T2832">PKZ_196X_UnluckyDog_flk.010 (010)</ta>
            <ta e="T2841" id="Seg_755" s="T2835">PKZ_196X_UnluckyDog_flk.011 (011)</ta>
            <ta e="T2843" id="Seg_756" s="T2841">PKZ_196X_UnluckyDog_flk.012 (012)</ta>
            <ta e="T2847" id="Seg_757" s="T2843">PKZ_196X_UnluckyDog_flk.013 (013)</ta>
            <ta e="T2851" id="Seg_758" s="T2847">PKZ_196X_UnluckyDog_flk.014 (014)</ta>
            <ta e="T2854" id="Seg_759" s="T2851">PKZ_196X_UnluckyDog_flk.015 (015)</ta>
            <ta e="T2859" id="Seg_760" s="T2854">PKZ_196X_UnluckyDog_flk.016 (016)</ta>
            <ta e="T2863" id="Seg_761" s="T2859">PKZ_196X_UnluckyDog_flk.017 (017)</ta>
            <ta e="T2866" id="Seg_762" s="T2863">PKZ_196X_UnluckyDog_flk.018 (018)</ta>
            <ta e="T2869" id="Seg_763" s="T2866">PKZ_196X_UnluckyDog_flk.019 (019)</ta>
            <ta e="T2872" id="Seg_764" s="T2869">PKZ_196X_UnluckyDog_flk.020 (020)</ta>
            <ta e="T2878" id="Seg_765" s="T2872">PKZ_196X_UnluckyDog_flk.021 (021)</ta>
            <ta e="T2889" id="Seg_766" s="T2878">PKZ_196X_UnluckyDog_flk.022 (022)</ta>
            <ta e="T2908" id="Seg_767" s="T2889">PKZ_196X_UnluckyDog_flk.023 (023)</ta>
            <ta e="T2911" id="Seg_768" s="T2908">PKZ_196X_UnluckyDog_flk.024 (024)</ta>
            <ta e="T2917" id="Seg_769" s="T2911">PKZ_196X_UnluckyDog_flk.025 (025)</ta>
            <ta e="T2918" id="Seg_770" s="T2917">PKZ_196X_UnluckyDog_flk.026 (026)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T2796" id="Seg_771" s="T2794">Šobi men. </ta>
            <ta e="T2802" id="Seg_772" s="T2796">(M-) Măndə inendə:" Măn tănan amnam!" </ta>
            <ta e="T2804" id="Seg_773" s="T2802">"No amaʔdə. </ta>
            <ta e="T2806" id="Seg_774" s="T2804">Kötendə amaʔ. </ta>
            <ta e="T2809" id="Seg_775" s="T2806">Dĭn uja iʔgö. </ta>
            <ta e="T2821" id="Seg_776" s="T2809">Dĭ (dal-) dĭm (a-) amzittə, (dĭ=) dĭ dĭm toʔnarbi bar, tĭmezeŋdə bar. </ta>
            <ta e="T2822" id="Seg_777" s="T2821">Uzleʔbəʔjə. </ta>
            <ta e="T2827" id="Seg_778" s="T2822">Dĭgəttə dĭ nuʔməluʔpi, nerelüʔpi inegəʔ. </ta>
            <ta e="T2832" id="Seg_779" s="T2827">Dĭgəttə (ka-) šonəga, poʔto šonəga. </ta>
            <ta e="T2835" id="Seg_780" s="T2832">"Măn tănan amnam". </ta>
            <ta e="T2841" id="Seg_781" s="T2835">"Iʔ amaʔ, măn (kan-) kallam ešseŋdə. </ta>
            <ta e="T2843" id="Seg_782" s="T2841">Amzittə mĭlem". </ta>
            <ta e="T2847" id="Seg_783" s="T2843">Kalla dʼürbi i naga. </ta>
            <ta e="T2851" id="Seg_784" s="T2847">Šonəga, šonəga, ular mĭŋge. </ta>
            <ta e="T2854" id="Seg_785" s="T2851">"Măn tănan amnam!" </ta>
            <ta e="T2859" id="Seg_786" s="T2854">"Măn kallam, üdʼüge ular bădəsʼtə". </ta>
            <ta e="T2863" id="Seg_787" s="T2859">Kalla dʼürbi i naga. </ta>
            <ta e="T2866" id="Seg_788" s="T2863">Dĭgəttə šonəga verblʼud. </ta>
            <ta e="T2869" id="Seg_789" s="T2866">"Măn tănan amnam!" </ta>
            <ta e="T2872" id="Seg_790" s="T2869">Dĭgəttə dĭ suʔmiluʔpi. </ta>
            <ta e="T2878" id="Seg_791" s="T2872">(Tĭme g- bɨ na-) Tĭmezeŋdə naga. </ta>
            <ta e="T2889" id="Seg_792" s="T2878">A verblʼud nuʔməluʔpi i (š- i š-) šobi gijen il iʔgö. </ta>
            <ta e="T2908" id="Seg_793" s="T2889">(Dĭ- dĭ i-) Il dĭ, men dĭ iluʔpiʔi da, tăŋ münörbiʔi, dĭ nuʔməluʔpi da mosttə ildə, šaʔlaːmbi dĭʔnə bar. </ta>
            <ta e="T2911" id="Seg_794" s="T2908">Xvostə (i-) sajjaʔluʔpiʔi. </ta>
            <ta e="T2917" id="Seg_795" s="T2911">Dĭgəttə dĭ bünə saʔməluʔpi i külaːmbi. </ta>
            <ta e="T2918" id="Seg_796" s="T2917">Bar. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2795" id="Seg_797" s="T2794">šo-bi</ta>
            <ta e="T2796" id="Seg_798" s="T2795">men</ta>
            <ta e="T2798" id="Seg_799" s="T2797">măn-də</ta>
            <ta e="T2799" id="Seg_800" s="T2798">ine-ndə</ta>
            <ta e="T2800" id="Seg_801" s="T2799">măn</ta>
            <ta e="T2801" id="Seg_802" s="T2800">tănan</ta>
            <ta e="T2802" id="Seg_803" s="T2801">am-na-m</ta>
            <ta e="T2803" id="Seg_804" s="T2802">no</ta>
            <ta e="T2804" id="Seg_805" s="T2803">am-aʔ-də</ta>
            <ta e="T2805" id="Seg_806" s="T2804">köten-də</ta>
            <ta e="T2806" id="Seg_807" s="T2805">am-a-ʔ</ta>
            <ta e="T2807" id="Seg_808" s="T2806">dĭn</ta>
            <ta e="T2808" id="Seg_809" s="T2807">uja</ta>
            <ta e="T2809" id="Seg_810" s="T2808">iʔgö</ta>
            <ta e="T2810" id="Seg_811" s="T2809">dĭ</ta>
            <ta e="T2812" id="Seg_812" s="T2811">dĭ-m</ta>
            <ta e="T2814" id="Seg_813" s="T2813">am-zittə</ta>
            <ta e="T2815" id="Seg_814" s="T2814">dĭ</ta>
            <ta e="T2816" id="Seg_815" s="T2815">dĭ</ta>
            <ta e="T2817" id="Seg_816" s="T2816">dĭ-m</ta>
            <ta e="T2818" id="Seg_817" s="T2817">toʔ-nar-bi</ta>
            <ta e="T2819" id="Seg_818" s="T2818">bar</ta>
            <ta e="T2820" id="Seg_819" s="T2819">tĭme-zeŋ-də</ta>
            <ta e="T2821" id="Seg_820" s="T2820">bar</ta>
            <ta e="T2822" id="Seg_821" s="T2821">uz-leʔbə-ʔjə</ta>
            <ta e="T2823" id="Seg_822" s="T2822">dĭgəttə</ta>
            <ta e="T2824" id="Seg_823" s="T2823">dĭ</ta>
            <ta e="T2825" id="Seg_824" s="T2824">nuʔmə-luʔ-pi</ta>
            <ta e="T2826" id="Seg_825" s="T2825">nere-lüʔ-pi</ta>
            <ta e="T2827" id="Seg_826" s="T2826">ine-gəʔ</ta>
            <ta e="T2828" id="Seg_827" s="T2827">dĭgəttə</ta>
            <ta e="T2830" id="Seg_828" s="T2829">šonə-ga</ta>
            <ta e="T2831" id="Seg_829" s="T2830">poʔto</ta>
            <ta e="T2832" id="Seg_830" s="T2831">šonə-ga</ta>
            <ta e="T2833" id="Seg_831" s="T2832">măn</ta>
            <ta e="T2834" id="Seg_832" s="T2833">tănan</ta>
            <ta e="T2835" id="Seg_833" s="T2834">am-na-m</ta>
            <ta e="T2836" id="Seg_834" s="T2835">i-ʔ</ta>
            <ta e="T2837" id="Seg_835" s="T2836">am-a-ʔ</ta>
            <ta e="T2838" id="Seg_836" s="T2837">măn</ta>
            <ta e="T2840" id="Seg_837" s="T2839">kal-la-m</ta>
            <ta e="T2841" id="Seg_838" s="T2840">eš-seŋ-də</ta>
            <ta e="T2842" id="Seg_839" s="T2841">am-zittə</ta>
            <ta e="T2843" id="Seg_840" s="T2842">mĭ-le-m</ta>
            <ta e="T2844" id="Seg_841" s="T2843">kal-la</ta>
            <ta e="T2845" id="Seg_842" s="T2844">dʼür-bi</ta>
            <ta e="T2846" id="Seg_843" s="T2845">i</ta>
            <ta e="T2847" id="Seg_844" s="T2846">naga</ta>
            <ta e="T2848" id="Seg_845" s="T2847">šonə-ga</ta>
            <ta e="T2849" id="Seg_846" s="T2848">šonə-ga</ta>
            <ta e="T2850" id="Seg_847" s="T2849">ular</ta>
            <ta e="T2851" id="Seg_848" s="T2850">mĭŋ-ge</ta>
            <ta e="T2852" id="Seg_849" s="T2851">măn</ta>
            <ta e="T2853" id="Seg_850" s="T2852">tănan</ta>
            <ta e="T2854" id="Seg_851" s="T2853">am-na-m</ta>
            <ta e="T2855" id="Seg_852" s="T2854">măn</ta>
            <ta e="T2856" id="Seg_853" s="T2855">kal-la-m</ta>
            <ta e="T2857" id="Seg_854" s="T2856">üdʼüge</ta>
            <ta e="T2858" id="Seg_855" s="T2857">ular</ta>
            <ta e="T2859" id="Seg_856" s="T2858">bădə-sʼtə</ta>
            <ta e="T2860" id="Seg_857" s="T2859">kal-la</ta>
            <ta e="T2861" id="Seg_858" s="T2860">dʼür-bi</ta>
            <ta e="T2862" id="Seg_859" s="T2861">i</ta>
            <ta e="T2863" id="Seg_860" s="T2862">naga</ta>
            <ta e="T2864" id="Seg_861" s="T2863">dĭgəttə</ta>
            <ta e="T2865" id="Seg_862" s="T2864">šonə-ga</ta>
            <ta e="T2866" id="Seg_863" s="T2865">verblʼud</ta>
            <ta e="T2867" id="Seg_864" s="T2866">măn</ta>
            <ta e="T2868" id="Seg_865" s="T2867">tănan</ta>
            <ta e="T2869" id="Seg_866" s="T2868">am-na-m</ta>
            <ta e="T2870" id="Seg_867" s="T2869">dĭgəttə</ta>
            <ta e="T2871" id="Seg_868" s="T2870">dĭ</ta>
            <ta e="T2872" id="Seg_869" s="T2871">suʔmi-luʔ-pi</ta>
            <ta e="T2873" id="Seg_870" s="T2872">tĭme</ta>
            <ta e="T2877" id="Seg_871" s="T2876">tĭme-zeŋ-də</ta>
            <ta e="T2878" id="Seg_872" s="T2877">naga</ta>
            <ta e="T2879" id="Seg_873" s="T2878">a</ta>
            <ta e="T2880" id="Seg_874" s="T2879">verblʼud</ta>
            <ta e="T2881" id="Seg_875" s="T2880">nuʔmə-luʔ-pi</ta>
            <ta e="T2882" id="Seg_876" s="T2881">i</ta>
            <ta e="T2884" id="Seg_877" s="T2883">i</ta>
            <ta e="T2886" id="Seg_878" s="T2885">šo-bi</ta>
            <ta e="T2887" id="Seg_879" s="T2886">gijen</ta>
            <ta e="T2888" id="Seg_880" s="T2887">il</ta>
            <ta e="T2889" id="Seg_881" s="T2888">iʔgö</ta>
            <ta e="T2891" id="Seg_882" s="T2890">dĭ</ta>
            <ta e="T2893" id="Seg_883" s="T2892">il</ta>
            <ta e="T2894" id="Seg_884" s="T2893">dĭ</ta>
            <ta e="T2895" id="Seg_885" s="T2894">men</ta>
            <ta e="T2896" id="Seg_886" s="T2895">dĭ</ta>
            <ta e="T2897" id="Seg_887" s="T2896">i-luʔ-pi-ʔi</ta>
            <ta e="T2898" id="Seg_888" s="T2897">da</ta>
            <ta e="T2899" id="Seg_889" s="T2898">tăŋ</ta>
            <ta e="T2900" id="Seg_890" s="T2899">münör-bi-ʔi</ta>
            <ta e="T2901" id="Seg_891" s="T2900">dĭ</ta>
            <ta e="T2902" id="Seg_892" s="T2901">nuʔmə-luʔ-pi</ta>
            <ta e="T2903" id="Seg_893" s="T2902">da</ta>
            <ta e="T2904" id="Seg_894" s="T2903">most-tə</ta>
            <ta e="T2905" id="Seg_895" s="T2904">il-də</ta>
            <ta e="T2906" id="Seg_896" s="T2905">šaʔ-laːm-bi</ta>
            <ta e="T2907" id="Seg_897" s="T2906">dĭʔ-nə</ta>
            <ta e="T2908" id="Seg_898" s="T2907">bar</ta>
            <ta e="T2909" id="Seg_899" s="T2908">xvostə</ta>
            <ta e="T2911" id="Seg_900" s="T2910">saj-jaʔ-luʔ-pi-ʔi</ta>
            <ta e="T2912" id="Seg_901" s="T2911">dĭgəttə</ta>
            <ta e="T2913" id="Seg_902" s="T2912">dĭ</ta>
            <ta e="T2914" id="Seg_903" s="T2913">bü-nə</ta>
            <ta e="T2915" id="Seg_904" s="T2914">saʔmə-luʔ-pi</ta>
            <ta e="T2916" id="Seg_905" s="T2915">i</ta>
            <ta e="T2917" id="Seg_906" s="T2916">kü-laːm-bi</ta>
            <ta e="T2918" id="Seg_907" s="T2917">bar</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2795" id="Seg_908" s="T2794">šo-bi</ta>
            <ta e="T2796" id="Seg_909" s="T2795">men</ta>
            <ta e="T2798" id="Seg_910" s="T2797">măn-ntə</ta>
            <ta e="T2799" id="Seg_911" s="T2798">ine-gəndə</ta>
            <ta e="T2800" id="Seg_912" s="T2799">măn</ta>
            <ta e="T2801" id="Seg_913" s="T2800">tănan</ta>
            <ta e="T2802" id="Seg_914" s="T2801">am-lV-m</ta>
            <ta e="T2803" id="Seg_915" s="T2802">no</ta>
            <ta e="T2804" id="Seg_916" s="T2803">am-ʔ-t</ta>
            <ta e="T2805" id="Seg_917" s="T2804">köten-də</ta>
            <ta e="T2806" id="Seg_918" s="T2805">am-ə-ʔ</ta>
            <ta e="T2807" id="Seg_919" s="T2806">dĭn</ta>
            <ta e="T2808" id="Seg_920" s="T2807">uja</ta>
            <ta e="T2809" id="Seg_921" s="T2808">iʔgö</ta>
            <ta e="T2810" id="Seg_922" s="T2809">dĭ</ta>
            <ta e="T2812" id="Seg_923" s="T2811">dĭ-m</ta>
            <ta e="T2814" id="Seg_924" s="T2813">am-zittə</ta>
            <ta e="T2815" id="Seg_925" s="T2814">dĭ</ta>
            <ta e="T2816" id="Seg_926" s="T2815">dĭ</ta>
            <ta e="T2817" id="Seg_927" s="T2816">dĭ-m</ta>
            <ta e="T2818" id="Seg_928" s="T2817">toʔbdə-nar-bi</ta>
            <ta e="T2819" id="Seg_929" s="T2818">bar</ta>
            <ta e="T2820" id="Seg_930" s="T2819">tĭme-zAŋ-də</ta>
            <ta e="T2821" id="Seg_931" s="T2820">bar</ta>
            <ta e="T2822" id="Seg_932" s="T2821">üzə-laʔbə-jəʔ</ta>
            <ta e="T2823" id="Seg_933" s="T2822">dĭgəttə</ta>
            <ta e="T2824" id="Seg_934" s="T2823">dĭ</ta>
            <ta e="T2825" id="Seg_935" s="T2824">nuʔmə-luʔbdə-bi</ta>
            <ta e="T2826" id="Seg_936" s="T2825">nereʔ-luʔbdə-bi</ta>
            <ta e="T2827" id="Seg_937" s="T2826">ine-gəʔ</ta>
            <ta e="T2828" id="Seg_938" s="T2827">dĭgəttə</ta>
            <ta e="T2830" id="Seg_939" s="T2829">šonə-gA</ta>
            <ta e="T2831" id="Seg_940" s="T2830">poʔto</ta>
            <ta e="T2832" id="Seg_941" s="T2831">šonə-gA</ta>
            <ta e="T2833" id="Seg_942" s="T2832">măn</ta>
            <ta e="T2834" id="Seg_943" s="T2833">tănan</ta>
            <ta e="T2835" id="Seg_944" s="T2834">am-lV-m</ta>
            <ta e="T2836" id="Seg_945" s="T2835">e-ʔ</ta>
            <ta e="T2837" id="Seg_946" s="T2836">am-ə-ʔ</ta>
            <ta e="T2838" id="Seg_947" s="T2837">măn</ta>
            <ta e="T2840" id="Seg_948" s="T2839">kan-lV-m</ta>
            <ta e="T2841" id="Seg_949" s="T2840">ešši-zAŋ-Tə</ta>
            <ta e="T2842" id="Seg_950" s="T2841">am-zittə</ta>
            <ta e="T2843" id="Seg_951" s="T2842">mĭ-lV-m</ta>
            <ta e="T2844" id="Seg_952" s="T2843">kan-lAʔ</ta>
            <ta e="T2845" id="Seg_953" s="T2844">tʼür-bi</ta>
            <ta e="T2846" id="Seg_954" s="T2845">i</ta>
            <ta e="T2847" id="Seg_955" s="T2846">naga</ta>
            <ta e="T2848" id="Seg_956" s="T2847">šonə-gA</ta>
            <ta e="T2849" id="Seg_957" s="T2848">šonə-gA</ta>
            <ta e="T2850" id="Seg_958" s="T2849">ular</ta>
            <ta e="T2851" id="Seg_959" s="T2850">mĭn-gA</ta>
            <ta e="T2852" id="Seg_960" s="T2851">măn</ta>
            <ta e="T2853" id="Seg_961" s="T2852">tănan</ta>
            <ta e="T2854" id="Seg_962" s="T2853">am-lV-m</ta>
            <ta e="T2855" id="Seg_963" s="T2854">măn</ta>
            <ta e="T2856" id="Seg_964" s="T2855">kan-lV-m</ta>
            <ta e="T2857" id="Seg_965" s="T2856">üdʼüge</ta>
            <ta e="T2858" id="Seg_966" s="T2857">ular</ta>
            <ta e="T2859" id="Seg_967" s="T2858">bădə-zittə</ta>
            <ta e="T2860" id="Seg_968" s="T2859">kan-lAʔ</ta>
            <ta e="T2861" id="Seg_969" s="T2860">tʼür-bi</ta>
            <ta e="T2862" id="Seg_970" s="T2861">i</ta>
            <ta e="T2863" id="Seg_971" s="T2862">naga</ta>
            <ta e="T2864" id="Seg_972" s="T2863">dĭgəttə</ta>
            <ta e="T2865" id="Seg_973" s="T2864">šonə-gA</ta>
            <ta e="T2866" id="Seg_974" s="T2865">verblʼud</ta>
            <ta e="T2867" id="Seg_975" s="T2866">măn</ta>
            <ta e="T2868" id="Seg_976" s="T2867">tănan</ta>
            <ta e="T2869" id="Seg_977" s="T2868">am-lV-m</ta>
            <ta e="T2870" id="Seg_978" s="T2869">dĭgəttə</ta>
            <ta e="T2871" id="Seg_979" s="T2870">dĭ</ta>
            <ta e="T2872" id="Seg_980" s="T2871">süʔmə-luʔbdə-bi</ta>
            <ta e="T2873" id="Seg_981" s="T2872">tĭme</ta>
            <ta e="T2877" id="Seg_982" s="T2876">tĭme-zAŋ-də</ta>
            <ta e="T2878" id="Seg_983" s="T2877">naga</ta>
            <ta e="T2879" id="Seg_984" s="T2878">a</ta>
            <ta e="T2880" id="Seg_985" s="T2879">verblʼud</ta>
            <ta e="T2881" id="Seg_986" s="T2880">nuʔmə-luʔbdə-bi</ta>
            <ta e="T2882" id="Seg_987" s="T2881">i</ta>
            <ta e="T2884" id="Seg_988" s="T2883">i</ta>
            <ta e="T2886" id="Seg_989" s="T2885">šo-bi</ta>
            <ta e="T2887" id="Seg_990" s="T2886">gijen</ta>
            <ta e="T2888" id="Seg_991" s="T2887">il</ta>
            <ta e="T2889" id="Seg_992" s="T2888">iʔgö</ta>
            <ta e="T2891" id="Seg_993" s="T2890">dĭ</ta>
            <ta e="T2893" id="Seg_994" s="T2892">il</ta>
            <ta e="T2894" id="Seg_995" s="T2893">dĭ</ta>
            <ta e="T2895" id="Seg_996" s="T2894">men</ta>
            <ta e="T2896" id="Seg_997" s="T2895">dĭ</ta>
            <ta e="T2897" id="Seg_998" s="T2896">i-luʔbdə-bi-jəʔ</ta>
            <ta e="T2898" id="Seg_999" s="T2897">da</ta>
            <ta e="T2899" id="Seg_1000" s="T2898">tăŋ</ta>
            <ta e="T2900" id="Seg_1001" s="T2899">münör-bi-jəʔ</ta>
            <ta e="T2901" id="Seg_1002" s="T2900">dĭ</ta>
            <ta e="T2902" id="Seg_1003" s="T2901">nuʔmə-luʔbdə-bi</ta>
            <ta e="T2903" id="Seg_1004" s="T2902">da</ta>
            <ta e="T2904" id="Seg_1005" s="T2903">most-də</ta>
            <ta e="T2905" id="Seg_1006" s="T2904">il-Tə</ta>
            <ta e="T2906" id="Seg_1007" s="T2905">šaʔ-laːm-bi</ta>
            <ta e="T2907" id="Seg_1008" s="T2906">dĭ-Tə</ta>
            <ta e="T2908" id="Seg_1009" s="T2907">bar</ta>
            <ta e="T2909" id="Seg_1010" s="T2908">xvostə</ta>
            <ta e="T2911" id="Seg_1011" s="T2910">săj-hʼaʔ-luʔbdə-bi-jəʔ</ta>
            <ta e="T2912" id="Seg_1012" s="T2911">dĭgəttə</ta>
            <ta e="T2913" id="Seg_1013" s="T2912">dĭ</ta>
            <ta e="T2914" id="Seg_1014" s="T2913">bü-Tə</ta>
            <ta e="T2915" id="Seg_1015" s="T2914">saʔmə-luʔbdə-bi</ta>
            <ta e="T2916" id="Seg_1016" s="T2915">i</ta>
            <ta e="T2917" id="Seg_1017" s="T2916">kü-laːm-bi</ta>
            <ta e="T2918" id="Seg_1018" s="T2917">bar</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2795" id="Seg_1019" s="T2794">come-PST.[3SG]</ta>
            <ta e="T2796" id="Seg_1020" s="T2795">dog.[NOM.SG]</ta>
            <ta e="T2798" id="Seg_1021" s="T2797">say-IPFVZ.[3SG]</ta>
            <ta e="T2799" id="Seg_1022" s="T2798">horse-LAT/LOC.3SG</ta>
            <ta e="T2800" id="Seg_1023" s="T2799">I.NOM</ta>
            <ta e="T2801" id="Seg_1024" s="T2800">you.ACC</ta>
            <ta e="T2802" id="Seg_1025" s="T2801">eat-FUT-1SG</ta>
            <ta e="T2803" id="Seg_1026" s="T2802">well</ta>
            <ta e="T2804" id="Seg_1027" s="T2803">eat-IMP.2SG-IMP.2SG.O</ta>
            <ta e="T2805" id="Seg_1028" s="T2804">ass-NOM/GEN/ACC.3SG</ta>
            <ta e="T2806" id="Seg_1029" s="T2805">eat-EP-IMP.2SG</ta>
            <ta e="T2807" id="Seg_1030" s="T2806">there</ta>
            <ta e="T2808" id="Seg_1031" s="T2807">meat.[NOM.SG]</ta>
            <ta e="T2809" id="Seg_1032" s="T2808">many</ta>
            <ta e="T2810" id="Seg_1033" s="T2809">this.[NOM.SG]</ta>
            <ta e="T2812" id="Seg_1034" s="T2811">this-ACC</ta>
            <ta e="T2814" id="Seg_1035" s="T2813">eat-INF.LAT</ta>
            <ta e="T2815" id="Seg_1036" s="T2814">this.[NOM.SG]</ta>
            <ta e="T2816" id="Seg_1037" s="T2815">this.[NOM.SG]</ta>
            <ta e="T2817" id="Seg_1038" s="T2816">this-ACC</ta>
            <ta e="T2818" id="Seg_1039" s="T2817">hit-MULT-PST.[3SG]</ta>
            <ta e="T2819" id="Seg_1040" s="T2818">PTCL</ta>
            <ta e="T2820" id="Seg_1041" s="T2819">tooth-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T2821" id="Seg_1042" s="T2820">PTCL</ta>
            <ta e="T2822" id="Seg_1043" s="T2821">fall-DUR-3PL</ta>
            <ta e="T2823" id="Seg_1044" s="T2822">then</ta>
            <ta e="T2824" id="Seg_1045" s="T2823">this.[NOM.SG]</ta>
            <ta e="T2825" id="Seg_1046" s="T2824">run-MOM-PST.[3SG]</ta>
            <ta e="T2826" id="Seg_1047" s="T2825">frighten-MOM-PST.[3SG]</ta>
            <ta e="T2827" id="Seg_1048" s="T2826">horse-ABL</ta>
            <ta e="T2828" id="Seg_1049" s="T2827">then</ta>
            <ta e="T2830" id="Seg_1050" s="T2829">come-PRS.[3SG]</ta>
            <ta e="T2831" id="Seg_1051" s="T2830">goat.[NOM.SG]</ta>
            <ta e="T2832" id="Seg_1052" s="T2831">come-PRS.[3SG]</ta>
            <ta e="T2833" id="Seg_1053" s="T2832">I.NOM</ta>
            <ta e="T2834" id="Seg_1054" s="T2833">you.ACC</ta>
            <ta e="T2835" id="Seg_1055" s="T2834">eat-FUT-1SG</ta>
            <ta e="T2836" id="Seg_1056" s="T2835">NEG.AUX-IMP.2SG</ta>
            <ta e="T2837" id="Seg_1057" s="T2836">eat-EP-CNG</ta>
            <ta e="T2838" id="Seg_1058" s="T2837">I.NOM</ta>
            <ta e="T2840" id="Seg_1059" s="T2839">go-FUT-1SG</ta>
            <ta e="T2841" id="Seg_1060" s="T2840">child-PL-LAT</ta>
            <ta e="T2842" id="Seg_1061" s="T2841">eat-INF.LAT</ta>
            <ta e="T2843" id="Seg_1062" s="T2842">give-FUT-1SG</ta>
            <ta e="T2844" id="Seg_1063" s="T2843">go-CVB</ta>
            <ta e="T2845" id="Seg_1064" s="T2844">disappear-PST.[3SG]</ta>
            <ta e="T2846" id="Seg_1065" s="T2845">and</ta>
            <ta e="T2847" id="Seg_1066" s="T2846">NEG.EX.[3SG]</ta>
            <ta e="T2848" id="Seg_1067" s="T2847">come-PRS.[3SG]</ta>
            <ta e="T2849" id="Seg_1068" s="T2848">come-PRS.[3SG]</ta>
            <ta e="T2850" id="Seg_1069" s="T2849">sheep.[NOM.SG]</ta>
            <ta e="T2851" id="Seg_1070" s="T2850">go-PRS.[3SG]</ta>
            <ta e="T2852" id="Seg_1071" s="T2851">I.NOM</ta>
            <ta e="T2853" id="Seg_1072" s="T2852">you.ACC</ta>
            <ta e="T2854" id="Seg_1073" s="T2853">eat-FUT-1SG</ta>
            <ta e="T2855" id="Seg_1074" s="T2854">I.NOM</ta>
            <ta e="T2856" id="Seg_1075" s="T2855">go-FUT-1SG</ta>
            <ta e="T2857" id="Seg_1076" s="T2856">small.[NOM.SG]</ta>
            <ta e="T2858" id="Seg_1077" s="T2857">sheep.[NOM.SG]</ta>
            <ta e="T2859" id="Seg_1078" s="T2858">feed-INF.LAT</ta>
            <ta e="T2860" id="Seg_1079" s="T2859">go-CVB</ta>
            <ta e="T2861" id="Seg_1080" s="T2860">disappear-PST.[3SG]</ta>
            <ta e="T2862" id="Seg_1081" s="T2861">and</ta>
            <ta e="T2863" id="Seg_1082" s="T2862">NEG.EX.[3SG]</ta>
            <ta e="T2864" id="Seg_1083" s="T2863">then</ta>
            <ta e="T2865" id="Seg_1084" s="T2864">come-PRS.[3SG]</ta>
            <ta e="T2866" id="Seg_1085" s="T2865">camel.[NOM.SG]</ta>
            <ta e="T2867" id="Seg_1086" s="T2866">I.NOM</ta>
            <ta e="T2868" id="Seg_1087" s="T2867">you.ACC</ta>
            <ta e="T2869" id="Seg_1088" s="T2868">eat-FUT-1SG</ta>
            <ta e="T2870" id="Seg_1089" s="T2869">then</ta>
            <ta e="T2871" id="Seg_1090" s="T2870">this.[NOM.SG]</ta>
            <ta e="T2872" id="Seg_1091" s="T2871">jump-MOM-PST.[3SG]</ta>
            <ta e="T2873" id="Seg_1092" s="T2872">tooth</ta>
            <ta e="T2877" id="Seg_1093" s="T2876">tooth-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T2878" id="Seg_1094" s="T2877">NEG.EX.[3SG]</ta>
            <ta e="T2879" id="Seg_1095" s="T2878">and</ta>
            <ta e="T2880" id="Seg_1096" s="T2879">camel.[NOM.SG]</ta>
            <ta e="T2881" id="Seg_1097" s="T2880">run-MOM-PST.[3SG]</ta>
            <ta e="T2882" id="Seg_1098" s="T2881">and</ta>
            <ta e="T2884" id="Seg_1099" s="T2883">and</ta>
            <ta e="T2886" id="Seg_1100" s="T2885">come-PST.[3SG]</ta>
            <ta e="T2887" id="Seg_1101" s="T2886">where</ta>
            <ta e="T2888" id="Seg_1102" s="T2887">people.[NOM.SG]</ta>
            <ta e="T2889" id="Seg_1103" s="T2888">many</ta>
            <ta e="T2891" id="Seg_1104" s="T2890">this.[NOM.SG]</ta>
            <ta e="T2893" id="Seg_1105" s="T2892">people.[NOM.SG]</ta>
            <ta e="T2894" id="Seg_1106" s="T2893">this.[NOM.SG]</ta>
            <ta e="T2895" id="Seg_1107" s="T2894">dog.[NOM.SG]</ta>
            <ta e="T2896" id="Seg_1108" s="T2895">this.[NOM.SG]</ta>
            <ta e="T2897" id="Seg_1109" s="T2896">take-MOM-PST-3PL</ta>
            <ta e="T2898" id="Seg_1110" s="T2897">and</ta>
            <ta e="T2899" id="Seg_1111" s="T2898">strongly</ta>
            <ta e="T2900" id="Seg_1112" s="T2899">beat-PST-3PL</ta>
            <ta e="T2901" id="Seg_1113" s="T2900">this.[NOM.SG]</ta>
            <ta e="T2902" id="Seg_1114" s="T2901">run-MOM-PST.[3SG]</ta>
            <ta e="T2903" id="Seg_1115" s="T2902">and</ta>
            <ta e="T2904" id="Seg_1116" s="T2903">bridge-NOM/GEN/ACC.3SG</ta>
            <ta e="T2905" id="Seg_1117" s="T2904">underpart-LAT</ta>
            <ta e="T2906" id="Seg_1118" s="T2905">hide-RES-PST.[3SG]</ta>
            <ta e="T2907" id="Seg_1119" s="T2906">this-LAT</ta>
            <ta e="T2908" id="Seg_1120" s="T2907">PTCL</ta>
            <ta e="T2909" id="Seg_1121" s="T2908">tail.[NOM.SG]</ta>
            <ta e="T2911" id="Seg_1122" s="T2910">off-cut-MOM-PST-3PL</ta>
            <ta e="T2912" id="Seg_1123" s="T2911">then</ta>
            <ta e="T2913" id="Seg_1124" s="T2912">this.[NOM.SG]</ta>
            <ta e="T2914" id="Seg_1125" s="T2913">water-LAT</ta>
            <ta e="T2915" id="Seg_1126" s="T2914">fall-MOM-PST.[3SG]</ta>
            <ta e="T2916" id="Seg_1127" s="T2915">and</ta>
            <ta e="T2917" id="Seg_1128" s="T2916">die-RES-PST.[3SG]</ta>
            <ta e="T2918" id="Seg_1129" s="T2917">all</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2795" id="Seg_1130" s="T2794">прийти-PST.[3SG]</ta>
            <ta e="T2796" id="Seg_1131" s="T2795">собака.[NOM.SG]</ta>
            <ta e="T2798" id="Seg_1132" s="T2797">сказать-IPFVZ.[3SG]</ta>
            <ta e="T2799" id="Seg_1133" s="T2798">лошадь-LAT/LOC.3SG</ta>
            <ta e="T2800" id="Seg_1134" s="T2799">я.NOM</ta>
            <ta e="T2801" id="Seg_1135" s="T2800">ты.ACC</ta>
            <ta e="T2802" id="Seg_1136" s="T2801">съесть-FUT-1SG</ta>
            <ta e="T2803" id="Seg_1137" s="T2802">ну</ta>
            <ta e="T2804" id="Seg_1138" s="T2803">съесть-IMP.2SG-IMP.2SG.O</ta>
            <ta e="T2805" id="Seg_1139" s="T2804">зад-NOM/GEN/ACC.3SG</ta>
            <ta e="T2806" id="Seg_1140" s="T2805">съесть-EP-IMP.2SG</ta>
            <ta e="T2807" id="Seg_1141" s="T2806">там</ta>
            <ta e="T2808" id="Seg_1142" s="T2807">мясо.[NOM.SG]</ta>
            <ta e="T2809" id="Seg_1143" s="T2808">много</ta>
            <ta e="T2810" id="Seg_1144" s="T2809">этот.[NOM.SG]</ta>
            <ta e="T2812" id="Seg_1145" s="T2811">этот-ACC</ta>
            <ta e="T2814" id="Seg_1146" s="T2813">съесть-INF.LAT</ta>
            <ta e="T2815" id="Seg_1147" s="T2814">этот.[NOM.SG]</ta>
            <ta e="T2816" id="Seg_1148" s="T2815">этот.[NOM.SG]</ta>
            <ta e="T2817" id="Seg_1149" s="T2816">этот-ACC</ta>
            <ta e="T2818" id="Seg_1150" s="T2817">ударить-MULT-PST.[3SG]</ta>
            <ta e="T2819" id="Seg_1151" s="T2818">PTCL</ta>
            <ta e="T2820" id="Seg_1152" s="T2819">зуб-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T2821" id="Seg_1153" s="T2820">PTCL</ta>
            <ta e="T2822" id="Seg_1154" s="T2821">упасть-DUR-3PL</ta>
            <ta e="T2823" id="Seg_1155" s="T2822">тогда</ta>
            <ta e="T2824" id="Seg_1156" s="T2823">этот.[NOM.SG]</ta>
            <ta e="T2825" id="Seg_1157" s="T2824">бежать-MOM-PST.[3SG]</ta>
            <ta e="T2826" id="Seg_1158" s="T2825">пугать-MOM-PST.[3SG]</ta>
            <ta e="T2827" id="Seg_1159" s="T2826">лошадь-ABL</ta>
            <ta e="T2828" id="Seg_1160" s="T2827">тогда</ta>
            <ta e="T2830" id="Seg_1161" s="T2829">прийти-PRS.[3SG]</ta>
            <ta e="T2831" id="Seg_1162" s="T2830">коза.[NOM.SG]</ta>
            <ta e="T2832" id="Seg_1163" s="T2831">прийти-PRS.[3SG]</ta>
            <ta e="T2833" id="Seg_1164" s="T2832">я.NOM</ta>
            <ta e="T2834" id="Seg_1165" s="T2833">ты.ACC</ta>
            <ta e="T2835" id="Seg_1166" s="T2834">съесть-FUT-1SG</ta>
            <ta e="T2836" id="Seg_1167" s="T2835">NEG.AUX-IMP.2SG</ta>
            <ta e="T2837" id="Seg_1168" s="T2836">съесть-EP-CNG</ta>
            <ta e="T2838" id="Seg_1169" s="T2837">я.NOM</ta>
            <ta e="T2840" id="Seg_1170" s="T2839">пойти-FUT-1SG</ta>
            <ta e="T2841" id="Seg_1171" s="T2840">ребенок-PL-LAT</ta>
            <ta e="T2842" id="Seg_1172" s="T2841">съесть-INF.LAT</ta>
            <ta e="T2843" id="Seg_1173" s="T2842">дать-FUT-1SG</ta>
            <ta e="T2844" id="Seg_1174" s="T2843">пойти-CVB</ta>
            <ta e="T2845" id="Seg_1175" s="T2844">исчезнуть-PST.[3SG]</ta>
            <ta e="T2846" id="Seg_1176" s="T2845">и</ta>
            <ta e="T2847" id="Seg_1177" s="T2846">NEG.EX.[3SG]</ta>
            <ta e="T2848" id="Seg_1178" s="T2847">прийти-PRS.[3SG]</ta>
            <ta e="T2849" id="Seg_1179" s="T2848">прийти-PRS.[3SG]</ta>
            <ta e="T2850" id="Seg_1180" s="T2849">овца.[NOM.SG]</ta>
            <ta e="T2851" id="Seg_1181" s="T2850">идти-PRS.[3SG]</ta>
            <ta e="T2852" id="Seg_1182" s="T2851">я.NOM</ta>
            <ta e="T2853" id="Seg_1183" s="T2852">ты.ACC</ta>
            <ta e="T2854" id="Seg_1184" s="T2853">съесть-FUT-1SG</ta>
            <ta e="T2855" id="Seg_1185" s="T2854">я.NOM</ta>
            <ta e="T2856" id="Seg_1186" s="T2855">пойти-FUT-1SG</ta>
            <ta e="T2857" id="Seg_1187" s="T2856">маленький.[NOM.SG]</ta>
            <ta e="T2858" id="Seg_1188" s="T2857">овца.[NOM.SG]</ta>
            <ta e="T2859" id="Seg_1189" s="T2858">кормить-INF.LAT</ta>
            <ta e="T2860" id="Seg_1190" s="T2859">пойти-CVB</ta>
            <ta e="T2861" id="Seg_1191" s="T2860">исчезнуть-PST.[3SG]</ta>
            <ta e="T2862" id="Seg_1192" s="T2861">и</ta>
            <ta e="T2863" id="Seg_1193" s="T2862">NEG.EX.[3SG]</ta>
            <ta e="T2864" id="Seg_1194" s="T2863">тогда</ta>
            <ta e="T2865" id="Seg_1195" s="T2864">прийти-PRS.[3SG]</ta>
            <ta e="T2866" id="Seg_1196" s="T2865">верблюд.[NOM.SG]</ta>
            <ta e="T2867" id="Seg_1197" s="T2866">я.NOM</ta>
            <ta e="T2868" id="Seg_1198" s="T2867">ты.ACC</ta>
            <ta e="T2869" id="Seg_1199" s="T2868">съесть-FUT-1SG</ta>
            <ta e="T2870" id="Seg_1200" s="T2869">тогда</ta>
            <ta e="T2871" id="Seg_1201" s="T2870">этот.[NOM.SG]</ta>
            <ta e="T2872" id="Seg_1202" s="T2871">прыгнуть-MOM-PST.[3SG]</ta>
            <ta e="T2873" id="Seg_1203" s="T2872">зуб</ta>
            <ta e="T2877" id="Seg_1204" s="T2876">зуб-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T2878" id="Seg_1205" s="T2877">NEG.EX.[3SG]</ta>
            <ta e="T2879" id="Seg_1206" s="T2878">а</ta>
            <ta e="T2880" id="Seg_1207" s="T2879">верблюд.[NOM.SG]</ta>
            <ta e="T2881" id="Seg_1208" s="T2880">бежать-MOM-PST.[3SG]</ta>
            <ta e="T2882" id="Seg_1209" s="T2881">и</ta>
            <ta e="T2884" id="Seg_1210" s="T2883">и</ta>
            <ta e="T2886" id="Seg_1211" s="T2885">прийти-PST.[3SG]</ta>
            <ta e="T2887" id="Seg_1212" s="T2886">где</ta>
            <ta e="T2888" id="Seg_1213" s="T2887">люди.[NOM.SG]</ta>
            <ta e="T2889" id="Seg_1214" s="T2888">много</ta>
            <ta e="T2891" id="Seg_1215" s="T2890">этот.[NOM.SG]</ta>
            <ta e="T2893" id="Seg_1216" s="T2892">люди.[NOM.SG]</ta>
            <ta e="T2894" id="Seg_1217" s="T2893">этот.[NOM.SG]</ta>
            <ta e="T2895" id="Seg_1218" s="T2894">собака.[NOM.SG]</ta>
            <ta e="T2896" id="Seg_1219" s="T2895">этот.[NOM.SG]</ta>
            <ta e="T2897" id="Seg_1220" s="T2896">взять-MOM-PST-3PL</ta>
            <ta e="T2898" id="Seg_1221" s="T2897">и</ta>
            <ta e="T2899" id="Seg_1222" s="T2898">сильно</ta>
            <ta e="T2900" id="Seg_1223" s="T2899">бить-PST-3PL</ta>
            <ta e="T2901" id="Seg_1224" s="T2900">этот.[NOM.SG]</ta>
            <ta e="T2902" id="Seg_1225" s="T2901">бежать-MOM-PST.[3SG]</ta>
            <ta e="T2903" id="Seg_1226" s="T2902">и</ta>
            <ta e="T2904" id="Seg_1227" s="T2903">мост-NOM/GEN/ACC.3SG</ta>
            <ta e="T2905" id="Seg_1228" s="T2904">низ-LAT</ta>
            <ta e="T2906" id="Seg_1229" s="T2905">спрятаться-RES-PST.[3SG]</ta>
            <ta e="T2907" id="Seg_1230" s="T2906">этот-LAT</ta>
            <ta e="T2908" id="Seg_1231" s="T2907">PTCL</ta>
            <ta e="T2909" id="Seg_1232" s="T2908">хвост.[NOM.SG]</ta>
            <ta e="T2911" id="Seg_1233" s="T2910">от-резать-MOM-PST-3PL</ta>
            <ta e="T2912" id="Seg_1234" s="T2911">тогда</ta>
            <ta e="T2913" id="Seg_1235" s="T2912">этот.[NOM.SG]</ta>
            <ta e="T2914" id="Seg_1236" s="T2913">вода-LAT</ta>
            <ta e="T2915" id="Seg_1237" s="T2914">упасть-MOM-PST.[3SG]</ta>
            <ta e="T2916" id="Seg_1238" s="T2915">и</ta>
            <ta e="T2917" id="Seg_1239" s="T2916">умереть-RES-PST.[3SG]</ta>
            <ta e="T2918" id="Seg_1240" s="T2917">весь</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2795" id="Seg_1241" s="T2794">v-v:tense-v:pn</ta>
            <ta e="T2796" id="Seg_1242" s="T2795">n-n:case</ta>
            <ta e="T2798" id="Seg_1243" s="T2797">v-v&gt;v-v:pn</ta>
            <ta e="T2799" id="Seg_1244" s="T2798">n-n:case.poss</ta>
            <ta e="T2800" id="Seg_1245" s="T2799">pers</ta>
            <ta e="T2801" id="Seg_1246" s="T2800">pers</ta>
            <ta e="T2802" id="Seg_1247" s="T2801">v-v:tense-v:pn</ta>
            <ta e="T2803" id="Seg_1248" s="T2802">ptcl</ta>
            <ta e="T2804" id="Seg_1249" s="T2803">v-v:mood.pn-v:mood.pn</ta>
            <ta e="T2805" id="Seg_1250" s="T2804">n-n:case.poss</ta>
            <ta e="T2806" id="Seg_1251" s="T2805">v-v:ins-v:mood.pn</ta>
            <ta e="T2807" id="Seg_1252" s="T2806">adv</ta>
            <ta e="T2808" id="Seg_1253" s="T2807">n-n:case</ta>
            <ta e="T2809" id="Seg_1254" s="T2808">quant</ta>
            <ta e="T2810" id="Seg_1255" s="T2809">dempro-n:case</ta>
            <ta e="T2812" id="Seg_1256" s="T2811">dempro-n:case</ta>
            <ta e="T2814" id="Seg_1257" s="T2813">v-v:n.fin</ta>
            <ta e="T2815" id="Seg_1258" s="T2814">dempro-n:case</ta>
            <ta e="T2816" id="Seg_1259" s="T2815">dempro-n:case</ta>
            <ta e="T2817" id="Seg_1260" s="T2816">dempro-n:case</ta>
            <ta e="T2818" id="Seg_1261" s="T2817">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2819" id="Seg_1262" s="T2818">ptcl</ta>
            <ta e="T2820" id="Seg_1263" s="T2819">n-n:num-n:case.poss</ta>
            <ta e="T2821" id="Seg_1264" s="T2820">ptcl</ta>
            <ta e="T2822" id="Seg_1265" s="T2821">v-v&gt;v-v:pn</ta>
            <ta e="T2823" id="Seg_1266" s="T2822">adv</ta>
            <ta e="T2824" id="Seg_1267" s="T2823">dempro-n:case</ta>
            <ta e="T2825" id="Seg_1268" s="T2824">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2826" id="Seg_1269" s="T2825">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2827" id="Seg_1270" s="T2826">n-n:case</ta>
            <ta e="T2828" id="Seg_1271" s="T2827">adv</ta>
            <ta e="T2830" id="Seg_1272" s="T2829">v-v:tense-v:pn</ta>
            <ta e="T2831" id="Seg_1273" s="T2830">n-n:case</ta>
            <ta e="T2832" id="Seg_1274" s="T2831">v-v:tense-v:pn</ta>
            <ta e="T2833" id="Seg_1275" s="T2832">pers</ta>
            <ta e="T2834" id="Seg_1276" s="T2833">pers</ta>
            <ta e="T2835" id="Seg_1277" s="T2834">v-v:tense-v:pn</ta>
            <ta e="T2836" id="Seg_1278" s="T2835">aux-v:mood.pn</ta>
            <ta e="T2837" id="Seg_1279" s="T2836">v-v:ins-v:n.fin</ta>
            <ta e="T2838" id="Seg_1280" s="T2837">pers</ta>
            <ta e="T2840" id="Seg_1281" s="T2839">v-v:tense-v:pn</ta>
            <ta e="T2841" id="Seg_1282" s="T2840">n-n:num-n:case</ta>
            <ta e="T2842" id="Seg_1283" s="T2841">v-v:n.fin</ta>
            <ta e="T2843" id="Seg_1284" s="T2842">v-v:tense-v:pn</ta>
            <ta e="T2844" id="Seg_1285" s="T2843">v-v:n.fin</ta>
            <ta e="T2845" id="Seg_1286" s="T2844">v-v:tense-v:pn</ta>
            <ta e="T2846" id="Seg_1287" s="T2845">conj</ta>
            <ta e="T2847" id="Seg_1288" s="T2846">v-v:pn</ta>
            <ta e="T2848" id="Seg_1289" s="T2847">v-v:tense-v:pn</ta>
            <ta e="T2849" id="Seg_1290" s="T2848">v-v:tense-v:pn</ta>
            <ta e="T2850" id="Seg_1291" s="T2849">n-n:case</ta>
            <ta e="T2851" id="Seg_1292" s="T2850">v-v:tense-v:pn</ta>
            <ta e="T2852" id="Seg_1293" s="T2851">pers</ta>
            <ta e="T2853" id="Seg_1294" s="T2852">pers</ta>
            <ta e="T2854" id="Seg_1295" s="T2853">v-v:tense-v:pn</ta>
            <ta e="T2855" id="Seg_1296" s="T2854">pers</ta>
            <ta e="T2856" id="Seg_1297" s="T2855">v-v:tense-v:pn</ta>
            <ta e="T2857" id="Seg_1298" s="T2856">adj-n:case</ta>
            <ta e="T2858" id="Seg_1299" s="T2857">n-n:case</ta>
            <ta e="T2859" id="Seg_1300" s="T2858">v-v:n.fin</ta>
            <ta e="T2860" id="Seg_1301" s="T2859">v-v:n.fin</ta>
            <ta e="T2861" id="Seg_1302" s="T2860">v-v:tense-v:pn</ta>
            <ta e="T2862" id="Seg_1303" s="T2861">conj</ta>
            <ta e="T2863" id="Seg_1304" s="T2862">v-v:pn</ta>
            <ta e="T2864" id="Seg_1305" s="T2863">adv</ta>
            <ta e="T2865" id="Seg_1306" s="T2864">v-v:tense-v:pn</ta>
            <ta e="T2866" id="Seg_1307" s="T2865">n-n:case</ta>
            <ta e="T2867" id="Seg_1308" s="T2866">pers</ta>
            <ta e="T2868" id="Seg_1309" s="T2867">pers</ta>
            <ta e="T2869" id="Seg_1310" s="T2868">v-v:tense-v:pn</ta>
            <ta e="T2870" id="Seg_1311" s="T2869">adv</ta>
            <ta e="T2871" id="Seg_1312" s="T2870">dempro-n:case</ta>
            <ta e="T2872" id="Seg_1313" s="T2871">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2873" id="Seg_1314" s="T2872">n</ta>
            <ta e="T2877" id="Seg_1315" s="T2876">n-n:num-n:case.poss</ta>
            <ta e="T2878" id="Seg_1316" s="T2877">v-v:pn</ta>
            <ta e="T2879" id="Seg_1317" s="T2878">conj</ta>
            <ta e="T2880" id="Seg_1318" s="T2879">n-n:case</ta>
            <ta e="T2881" id="Seg_1319" s="T2880">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2882" id="Seg_1320" s="T2881">conj</ta>
            <ta e="T2884" id="Seg_1321" s="T2883">conj</ta>
            <ta e="T2886" id="Seg_1322" s="T2885">v-v:tense-v:pn</ta>
            <ta e="T2887" id="Seg_1323" s="T2886">que</ta>
            <ta e="T2888" id="Seg_1324" s="T2887">n-n:case</ta>
            <ta e="T2889" id="Seg_1325" s="T2888">quant</ta>
            <ta e="T2891" id="Seg_1326" s="T2890">dempro-n:case</ta>
            <ta e="T2893" id="Seg_1327" s="T2892">n-n:case</ta>
            <ta e="T2894" id="Seg_1328" s="T2893">dempro-n:case</ta>
            <ta e="T2895" id="Seg_1329" s="T2894">n-n:case</ta>
            <ta e="T2896" id="Seg_1330" s="T2895">dempro-n:case</ta>
            <ta e="T2897" id="Seg_1331" s="T2896">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2898" id="Seg_1332" s="T2897">conj</ta>
            <ta e="T2899" id="Seg_1333" s="T2898">adv</ta>
            <ta e="T2900" id="Seg_1334" s="T2899">v-v:tense-v:pn</ta>
            <ta e="T2901" id="Seg_1335" s="T2900">dempro-n:case</ta>
            <ta e="T2902" id="Seg_1336" s="T2901">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2903" id="Seg_1337" s="T2902">conj</ta>
            <ta e="T2904" id="Seg_1338" s="T2903">n-n:case.poss</ta>
            <ta e="T2905" id="Seg_1339" s="T2904">n-n:case</ta>
            <ta e="T2906" id="Seg_1340" s="T2905">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2907" id="Seg_1341" s="T2906">dempro-n:case</ta>
            <ta e="T2908" id="Seg_1342" s="T2907">ptcl</ta>
            <ta e="T2909" id="Seg_1343" s="T2908">n-n:case</ta>
            <ta e="T2911" id="Seg_1344" s="T2910">v&gt;v-v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2912" id="Seg_1345" s="T2911">adv</ta>
            <ta e="T2913" id="Seg_1346" s="T2912">dempro-n:case</ta>
            <ta e="T2914" id="Seg_1347" s="T2913">n-n:case</ta>
            <ta e="T2915" id="Seg_1348" s="T2914">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2916" id="Seg_1349" s="T2915">conj</ta>
            <ta e="T2917" id="Seg_1350" s="T2916">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2918" id="Seg_1351" s="T2917">quant</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2795" id="Seg_1352" s="T2794">v</ta>
            <ta e="T2796" id="Seg_1353" s="T2795">n</ta>
            <ta e="T2798" id="Seg_1354" s="T2797">v</ta>
            <ta e="T2799" id="Seg_1355" s="T2798">n</ta>
            <ta e="T2800" id="Seg_1356" s="T2799">pers</ta>
            <ta e="T2801" id="Seg_1357" s="T2800">pers</ta>
            <ta e="T2802" id="Seg_1358" s="T2801">v</ta>
            <ta e="T2803" id="Seg_1359" s="T2802">ptcl</ta>
            <ta e="T2804" id="Seg_1360" s="T2803">v</ta>
            <ta e="T2805" id="Seg_1361" s="T2804">n</ta>
            <ta e="T2806" id="Seg_1362" s="T2805">v</ta>
            <ta e="T2807" id="Seg_1363" s="T2806">adv</ta>
            <ta e="T2808" id="Seg_1364" s="T2807">n</ta>
            <ta e="T2809" id="Seg_1365" s="T2808">quant</ta>
            <ta e="T2810" id="Seg_1366" s="T2809">dempro</ta>
            <ta e="T2812" id="Seg_1367" s="T2811">dempro</ta>
            <ta e="T2814" id="Seg_1368" s="T2813">v</ta>
            <ta e="T2815" id="Seg_1369" s="T2814">dempro</ta>
            <ta e="T2816" id="Seg_1370" s="T2815">dempro</ta>
            <ta e="T2817" id="Seg_1371" s="T2816">dempro</ta>
            <ta e="T2818" id="Seg_1372" s="T2817">v</ta>
            <ta e="T2819" id="Seg_1373" s="T2818">ptcl</ta>
            <ta e="T2820" id="Seg_1374" s="T2819">n</ta>
            <ta e="T2821" id="Seg_1375" s="T2820">ptcl</ta>
            <ta e="T2822" id="Seg_1376" s="T2821">v</ta>
            <ta e="T2823" id="Seg_1377" s="T2822">adv</ta>
            <ta e="T2824" id="Seg_1378" s="T2823">dempro</ta>
            <ta e="T2825" id="Seg_1379" s="T2824">v</ta>
            <ta e="T2826" id="Seg_1380" s="T2825">v</ta>
            <ta e="T2827" id="Seg_1381" s="T2826">n</ta>
            <ta e="T2828" id="Seg_1382" s="T2827">adv</ta>
            <ta e="T2830" id="Seg_1383" s="T2829">v</ta>
            <ta e="T2831" id="Seg_1384" s="T2830">n</ta>
            <ta e="T2832" id="Seg_1385" s="T2831">v</ta>
            <ta e="T2833" id="Seg_1386" s="T2832">pers</ta>
            <ta e="T2834" id="Seg_1387" s="T2833">pers</ta>
            <ta e="T2835" id="Seg_1388" s="T2834">v</ta>
            <ta e="T2836" id="Seg_1389" s="T2835">aux</ta>
            <ta e="T2837" id="Seg_1390" s="T2836">v</ta>
            <ta e="T2838" id="Seg_1391" s="T2837">pers</ta>
            <ta e="T2840" id="Seg_1392" s="T2839">v</ta>
            <ta e="T2841" id="Seg_1393" s="T2840">n</ta>
            <ta e="T2842" id="Seg_1394" s="T2841">v</ta>
            <ta e="T2843" id="Seg_1395" s="T2842">v</ta>
            <ta e="T2844" id="Seg_1396" s="T2843">v</ta>
            <ta e="T2845" id="Seg_1397" s="T2844">v</ta>
            <ta e="T2846" id="Seg_1398" s="T2845">conj</ta>
            <ta e="T2847" id="Seg_1399" s="T2846">v</ta>
            <ta e="T2848" id="Seg_1400" s="T2847">v</ta>
            <ta e="T2849" id="Seg_1401" s="T2848">v</ta>
            <ta e="T2850" id="Seg_1402" s="T2849">n</ta>
            <ta e="T2851" id="Seg_1403" s="T2850">v</ta>
            <ta e="T2852" id="Seg_1404" s="T2851">pers</ta>
            <ta e="T2853" id="Seg_1405" s="T2852">pers</ta>
            <ta e="T2854" id="Seg_1406" s="T2853">v</ta>
            <ta e="T2855" id="Seg_1407" s="T2854">pers</ta>
            <ta e="T2856" id="Seg_1408" s="T2855">v</ta>
            <ta e="T2857" id="Seg_1409" s="T2856">adj</ta>
            <ta e="T2858" id="Seg_1410" s="T2857">n</ta>
            <ta e="T2859" id="Seg_1411" s="T2858">v</ta>
            <ta e="T2860" id="Seg_1412" s="T2859">v</ta>
            <ta e="T2861" id="Seg_1413" s="T2860">v</ta>
            <ta e="T2862" id="Seg_1414" s="T2861">conj</ta>
            <ta e="T2863" id="Seg_1415" s="T2862">v</ta>
            <ta e="T2864" id="Seg_1416" s="T2863">adv</ta>
            <ta e="T2865" id="Seg_1417" s="T2864">v</ta>
            <ta e="T2866" id="Seg_1418" s="T2865">n</ta>
            <ta e="T2867" id="Seg_1419" s="T2866">pers</ta>
            <ta e="T2868" id="Seg_1420" s="T2867">pers</ta>
            <ta e="T2869" id="Seg_1421" s="T2868">v</ta>
            <ta e="T2870" id="Seg_1422" s="T2869">adv</ta>
            <ta e="T2871" id="Seg_1423" s="T2870">dempro</ta>
            <ta e="T2872" id="Seg_1424" s="T2871">v</ta>
            <ta e="T2873" id="Seg_1425" s="T2872">n</ta>
            <ta e="T2877" id="Seg_1426" s="T2876">n</ta>
            <ta e="T2878" id="Seg_1427" s="T2877">v</ta>
            <ta e="T2879" id="Seg_1428" s="T2878">conj</ta>
            <ta e="T2880" id="Seg_1429" s="T2879">n</ta>
            <ta e="T2881" id="Seg_1430" s="T2880">v</ta>
            <ta e="T2882" id="Seg_1431" s="T2881">conj</ta>
            <ta e="T2884" id="Seg_1432" s="T2883">conj</ta>
            <ta e="T2886" id="Seg_1433" s="T2885">v</ta>
            <ta e="T2887" id="Seg_1434" s="T2886">que</ta>
            <ta e="T2888" id="Seg_1435" s="T2887">n</ta>
            <ta e="T2889" id="Seg_1436" s="T2888">quant</ta>
            <ta e="T2891" id="Seg_1437" s="T2890">dempro</ta>
            <ta e="T2893" id="Seg_1438" s="T2892">n</ta>
            <ta e="T2894" id="Seg_1439" s="T2893">dempro</ta>
            <ta e="T2895" id="Seg_1440" s="T2894">n</ta>
            <ta e="T2896" id="Seg_1441" s="T2895">dempro</ta>
            <ta e="T2897" id="Seg_1442" s="T2896">v</ta>
            <ta e="T2898" id="Seg_1443" s="T2897">conj</ta>
            <ta e="T2899" id="Seg_1444" s="T2898">adv</ta>
            <ta e="T2900" id="Seg_1445" s="T2899">v</ta>
            <ta e="T2901" id="Seg_1446" s="T2900">dempro</ta>
            <ta e="T2902" id="Seg_1447" s="T2901">v</ta>
            <ta e="T2903" id="Seg_1448" s="T2902">conj</ta>
            <ta e="T2904" id="Seg_1449" s="T2903">n</ta>
            <ta e="T2905" id="Seg_1450" s="T2904">n</ta>
            <ta e="T2906" id="Seg_1451" s="T2905">v</ta>
            <ta e="T2907" id="Seg_1452" s="T2906">dempro</ta>
            <ta e="T2908" id="Seg_1453" s="T2907">ptcl</ta>
            <ta e="T2909" id="Seg_1454" s="T2908">n</ta>
            <ta e="T2911" id="Seg_1455" s="T2910">v</ta>
            <ta e="T2912" id="Seg_1456" s="T2911">adv</ta>
            <ta e="T2913" id="Seg_1457" s="T2912">dempro</ta>
            <ta e="T2914" id="Seg_1458" s="T2913">n</ta>
            <ta e="T2915" id="Seg_1459" s="T2914">v</ta>
            <ta e="T2916" id="Seg_1460" s="T2915">conj</ta>
            <ta e="T2917" id="Seg_1461" s="T2916">v</ta>
            <ta e="T2918" id="Seg_1462" s="T2917">quant</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2796" id="Seg_1463" s="T2795">np.h:A</ta>
            <ta e="T2798" id="Seg_1464" s="T2797">0.3.h:A</ta>
            <ta e="T2799" id="Seg_1465" s="T2798">np.h:R</ta>
            <ta e="T2800" id="Seg_1466" s="T2799">pro.h:A</ta>
            <ta e="T2801" id="Seg_1467" s="T2800">pro.h:P</ta>
            <ta e="T2804" id="Seg_1468" s="T2803">0.2.h:A</ta>
            <ta e="T2805" id="Seg_1469" s="T2804">np:P</ta>
            <ta e="T2806" id="Seg_1470" s="T2805">0.2.h:A</ta>
            <ta e="T2807" id="Seg_1471" s="T2806">adv:L</ta>
            <ta e="T2808" id="Seg_1472" s="T2807">np:Th</ta>
            <ta e="T2810" id="Seg_1473" s="T2809">pro.h:A</ta>
            <ta e="T2812" id="Seg_1474" s="T2811">pro.h:Th</ta>
            <ta e="T2816" id="Seg_1475" s="T2815">pro.h:A</ta>
            <ta e="T2817" id="Seg_1476" s="T2816">pro.h:E</ta>
            <ta e="T2820" id="Seg_1477" s="T2819">np:L</ta>
            <ta e="T2822" id="Seg_1478" s="T2821">0.3:Th</ta>
            <ta e="T2823" id="Seg_1479" s="T2822">adv:Time</ta>
            <ta e="T2824" id="Seg_1480" s="T2823">pro.h:A</ta>
            <ta e="T2826" id="Seg_1481" s="T2825">0.3.h:E</ta>
            <ta e="T2827" id="Seg_1482" s="T2826">np.h:Th</ta>
            <ta e="T2828" id="Seg_1483" s="T2827">adv:Time</ta>
            <ta e="T2831" id="Seg_1484" s="T2830">np.h:A</ta>
            <ta e="T2833" id="Seg_1485" s="T2832">pro.h:A</ta>
            <ta e="T2834" id="Seg_1486" s="T2833">pro.h:P</ta>
            <ta e="T2836" id="Seg_1487" s="T2835">0.2.h:A</ta>
            <ta e="T2838" id="Seg_1488" s="T2837">pro.h:A</ta>
            <ta e="T2841" id="Seg_1489" s="T2840">np:G</ta>
            <ta e="T2843" id="Seg_1490" s="T2842">0.1.h:A</ta>
            <ta e="T2845" id="Seg_1491" s="T2844">0.3.h:A</ta>
            <ta e="T2847" id="Seg_1492" s="T2846">0.3:Th</ta>
            <ta e="T2848" id="Seg_1493" s="T2847">0.3.h:A</ta>
            <ta e="T2849" id="Seg_1494" s="T2848">0.3.h:A</ta>
            <ta e="T2850" id="Seg_1495" s="T2849">np.h:A</ta>
            <ta e="T2852" id="Seg_1496" s="T2851">pro.h:A</ta>
            <ta e="T2853" id="Seg_1497" s="T2852">pro.h:P</ta>
            <ta e="T2855" id="Seg_1498" s="T2854">pro.h:A</ta>
            <ta e="T2858" id="Seg_1499" s="T2857">np:B</ta>
            <ta e="T2861" id="Seg_1500" s="T2860">0.3.h:A</ta>
            <ta e="T2863" id="Seg_1501" s="T2862">0.3:Th</ta>
            <ta e="T2864" id="Seg_1502" s="T2863">adv:Time</ta>
            <ta e="T2866" id="Seg_1503" s="T2865">np:A</ta>
            <ta e="T2867" id="Seg_1504" s="T2866">pro.h:A</ta>
            <ta e="T2868" id="Seg_1505" s="T2867">pro:P</ta>
            <ta e="T2870" id="Seg_1506" s="T2869">adv:Time</ta>
            <ta e="T2871" id="Seg_1507" s="T2870">pro:A</ta>
            <ta e="T2877" id="Seg_1508" s="T2876">np:Th</ta>
            <ta e="T2880" id="Seg_1509" s="T2879">np:A</ta>
            <ta e="T2886" id="Seg_1510" s="T2885">0.3:A</ta>
            <ta e="T2888" id="Seg_1511" s="T2887">np.h:Th</ta>
            <ta e="T2893" id="Seg_1512" s="T2892">np.h:A</ta>
            <ta e="T2895" id="Seg_1513" s="T2894">np.h:Th</ta>
            <ta e="T2900" id="Seg_1514" s="T2899">0.3.h:A</ta>
            <ta e="T2901" id="Seg_1515" s="T2900">pro.h:A</ta>
            <ta e="T2904" id="Seg_1516" s="T2903">np:Poss</ta>
            <ta e="T2905" id="Seg_1517" s="T2904">np:L</ta>
            <ta e="T2909" id="Seg_1518" s="T2908">np:P</ta>
            <ta e="T2911" id="Seg_1519" s="T2910">0.3.h:A</ta>
            <ta e="T2912" id="Seg_1520" s="T2911">adv:Time</ta>
            <ta e="T2913" id="Seg_1521" s="T2912">pro.h:E</ta>
            <ta e="T2914" id="Seg_1522" s="T2913">np:G</ta>
            <ta e="T2917" id="Seg_1523" s="T2916">0.3.h:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2795" id="Seg_1524" s="T2794">v:pred</ta>
            <ta e="T2796" id="Seg_1525" s="T2795">np.h:S</ta>
            <ta e="T2798" id="Seg_1526" s="T2797">v:pred 0.3.h:S</ta>
            <ta e="T2800" id="Seg_1527" s="T2799">pro.h:S</ta>
            <ta e="T2801" id="Seg_1528" s="T2800">pro.h:O</ta>
            <ta e="T2802" id="Seg_1529" s="T2801">v:pred</ta>
            <ta e="T2804" id="Seg_1530" s="T2803">v:pred 0.2.h:S</ta>
            <ta e="T2805" id="Seg_1531" s="T2804">np:O</ta>
            <ta e="T2806" id="Seg_1532" s="T2805">v:pred 0.2.h:S</ta>
            <ta e="T2808" id="Seg_1533" s="T2807">np:S</ta>
            <ta e="T2810" id="Seg_1534" s="T2809">pro.h:S</ta>
            <ta e="T2812" id="Seg_1535" s="T2811">pro.h:O</ta>
            <ta e="T2816" id="Seg_1536" s="T2815">pro.h:S</ta>
            <ta e="T2817" id="Seg_1537" s="T2816">pro.h:O</ta>
            <ta e="T2818" id="Seg_1538" s="T2817">v:pred</ta>
            <ta e="T2822" id="Seg_1539" s="T2821">v:pred 0.3:S</ta>
            <ta e="T2824" id="Seg_1540" s="T2823">pro.h:S</ta>
            <ta e="T2825" id="Seg_1541" s="T2824">v:pred</ta>
            <ta e="T2826" id="Seg_1542" s="T2825">v:pred 0.3.h:S</ta>
            <ta e="T2830" id="Seg_1543" s="T2829">v:pred </ta>
            <ta e="T2831" id="Seg_1544" s="T2830">np.h:S</ta>
            <ta e="T2832" id="Seg_1545" s="T2831">v:pred</ta>
            <ta e="T2833" id="Seg_1546" s="T2832">pro.h:S</ta>
            <ta e="T2834" id="Seg_1547" s="T2833">pro.h:O</ta>
            <ta e="T2835" id="Seg_1548" s="T2834">v:pred</ta>
            <ta e="T2836" id="Seg_1549" s="T2835">v:pred 0.2.h:S</ta>
            <ta e="T2838" id="Seg_1550" s="T2837">pro.h:S</ta>
            <ta e="T2840" id="Seg_1551" s="T2839">v:pred</ta>
            <ta e="T2842" id="Seg_1552" s="T2841">v:pred s:purp</ta>
            <ta e="T2843" id="Seg_1553" s="T2842">v:pred 0.1.h:S</ta>
            <ta e="T2844" id="Seg_1554" s="T2843">conv:pred</ta>
            <ta e="T2845" id="Seg_1555" s="T2844">v:pred 0.3.h:S</ta>
            <ta e="T2847" id="Seg_1556" s="T2846">v:pred 0.3:S</ta>
            <ta e="T2848" id="Seg_1557" s="T2847">v:pred 0.3.h:S</ta>
            <ta e="T2849" id="Seg_1558" s="T2848">v:pred 0.3.h:S</ta>
            <ta e="T2850" id="Seg_1559" s="T2849">np.h:S</ta>
            <ta e="T2851" id="Seg_1560" s="T2850">v:pred</ta>
            <ta e="T2852" id="Seg_1561" s="T2851">pro.h:S</ta>
            <ta e="T2853" id="Seg_1562" s="T2852">pro.h:O</ta>
            <ta e="T2854" id="Seg_1563" s="T2853">v:pred</ta>
            <ta e="T2855" id="Seg_1564" s="T2854">pro.h:S</ta>
            <ta e="T2856" id="Seg_1565" s="T2855">v:pred</ta>
            <ta e="T2858" id="Seg_1566" s="T2857">np:O</ta>
            <ta e="T2859" id="Seg_1567" s="T2858">v:pred s:purp</ta>
            <ta e="T2860" id="Seg_1568" s="T2859">conv:pred</ta>
            <ta e="T2861" id="Seg_1569" s="T2860">v:pred 0.3.h:S</ta>
            <ta e="T2863" id="Seg_1570" s="T2862">v:pred 0.3:S</ta>
            <ta e="T2865" id="Seg_1571" s="T2864">v:pred</ta>
            <ta e="T2866" id="Seg_1572" s="T2865">np:S</ta>
            <ta e="T2867" id="Seg_1573" s="T2866">pro.h:S</ta>
            <ta e="T2868" id="Seg_1574" s="T2867">pro:O</ta>
            <ta e="T2869" id="Seg_1575" s="T2868">v:pred</ta>
            <ta e="T2871" id="Seg_1576" s="T2870">pro:S</ta>
            <ta e="T2872" id="Seg_1577" s="T2871">v:pred</ta>
            <ta e="T2877" id="Seg_1578" s="T2876">np:S</ta>
            <ta e="T2878" id="Seg_1579" s="T2877">v:pred</ta>
            <ta e="T2880" id="Seg_1580" s="T2879">np:S</ta>
            <ta e="T2881" id="Seg_1581" s="T2880">v:pred</ta>
            <ta e="T2886" id="Seg_1582" s="T2885">v:pred 0.3:S</ta>
            <ta e="T2888" id="Seg_1583" s="T2887">np.h:S</ta>
            <ta e="T2893" id="Seg_1584" s="T2892">np.h:S</ta>
            <ta e="T2895" id="Seg_1585" s="T2894">np.h:O</ta>
            <ta e="T2897" id="Seg_1586" s="T2896">v:pred</ta>
            <ta e="T2900" id="Seg_1587" s="T2899">v:pred 0.3.h:S</ta>
            <ta e="T2901" id="Seg_1588" s="T2900">pro.h:S</ta>
            <ta e="T2902" id="Seg_1589" s="T2901">v:pred</ta>
            <ta e="T2906" id="Seg_1590" s="T2905">v:pred</ta>
            <ta e="T2909" id="Seg_1591" s="T2908">np:O</ta>
            <ta e="T2911" id="Seg_1592" s="T2910">v:pred 0.3.h:S</ta>
            <ta e="T2913" id="Seg_1593" s="T2912">pro.h:S</ta>
            <ta e="T2915" id="Seg_1594" s="T2914">v:pred</ta>
            <ta e="T2917" id="Seg_1595" s="T2916">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2803" id="Seg_1596" s="T2802">RUS:disc</ta>
            <ta e="T2819" id="Seg_1597" s="T2818">TURK:disc</ta>
            <ta e="T2821" id="Seg_1598" s="T2820">TURK:disc</ta>
            <ta e="T2846" id="Seg_1599" s="T2845">RUS:gram</ta>
            <ta e="T2862" id="Seg_1600" s="T2861">RUS:gram</ta>
            <ta e="T2866" id="Seg_1601" s="T2865">RUS:cult</ta>
            <ta e="T2879" id="Seg_1602" s="T2878">RUS:gram</ta>
            <ta e="T2880" id="Seg_1603" s="T2879">RUS:cult</ta>
            <ta e="T2882" id="Seg_1604" s="T2881">RUS:gram</ta>
            <ta e="T2884" id="Seg_1605" s="T2883">RUS:gram</ta>
            <ta e="T2898" id="Seg_1606" s="T2897">RUS:gram</ta>
            <ta e="T2903" id="Seg_1607" s="T2902">RUS:gram</ta>
            <ta e="T2904" id="Seg_1608" s="T2903">RUS:cult</ta>
            <ta e="T2908" id="Seg_1609" s="T2907">TURK:disc</ta>
            <ta e="T2909" id="Seg_1610" s="T2908">RUS:core</ta>
            <ta e="T2916" id="Seg_1611" s="T2915">RUS:gram</ta>
            <ta e="T2918" id="Seg_1612" s="T2917">TURK:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T2796" id="Seg_1613" s="T2794">Пришла собака.</ta>
            <ta e="T2802" id="Seg_1614" s="T2796">Говорит лошади: «Я тебя съем!»</ta>
            <ta e="T2804" id="Seg_1615" s="T2802">«Ну, ешь!</ta>
            <ta e="T2806" id="Seg_1616" s="T2804">Ешь [мой] зад.</ta>
            <ta e="T2809" id="Seg_1617" s="T2806">Там много мяса».</ta>
            <ta e="T2821" id="Seg_1618" s="T2809">Она (дала?) ей съесть [лошадь подпустила собаку поближе], она её ударила, в зубы.</ta>
            <ta e="T2822" id="Seg_1619" s="T2821">[Зубы] выпадают.</ta>
            <ta e="T2827" id="Seg_1620" s="T2822">Потом она убежала, испугалась лошади.</ta>
            <ta e="T2832" id="Seg_1621" s="T2827">Потом идёт, [навстречу] коза идёт.</ta>
            <ta e="T2835" id="Seg_1622" s="T2832">«Я тебя съем!»</ta>
            <ta e="T2841" id="Seg_1623" s="T2835">«Не ешь меня, я к детям иду.</ta>
            <ta e="T2843" id="Seg_1624" s="T2841">Я дам [тебе кое-чего] поесть».</ta>
            <ta e="T2847" id="Seg_1625" s="T2843">[Коза] ушла и ничего [не оставила].</ta>
            <ta e="T2851" id="Seg_1626" s="T2847">[Собака] идёт, идёт, [навстречу] овца идёт.</ta>
            <ta e="T2854" id="Seg_1627" s="T2851">«Я тебя съем!»</ta>
            <ta e="T2859" id="Seg_1628" s="T2854">«Я пойду ягнят кормить».</ta>
            <ta e="T2863" id="Seg_1629" s="T2859">Ушла и ничего [не оставила].</ta>
            <ta e="T2866" id="Seg_1630" s="T2863">Потом верблюд идёт.</ta>
            <ta e="T2869" id="Seg_1631" s="T2866">«Я тебя съем!»</ta>
            <ta e="T2872" id="Seg_1632" s="T2869">Потом [собака на него] прыгнула.</ta>
            <ta e="T2878" id="Seg_1633" s="T2872">(Зуб) Зубов нет.</ta>
            <ta e="T2889" id="Seg_1634" s="T2878">А верблюд убежал и пришёл туда, где было много людей.</ta>
            <ta e="T2908" id="Seg_1635" s="T2889">Люди, собака, взяли и сильно избили, она убежала под мост и там спряталась.</ta>
            <ta e="T2911" id="Seg_1636" s="T2908">Они ей хвост отрезали.</ta>
            <ta e="T2917" id="Seg_1637" s="T2911">Она упала в реку и умерла.</ta>
            <ta e="T2918" id="Seg_1638" s="T2917">Всё.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T2796" id="Seg_1639" s="T2794">A dog came.</ta>
            <ta e="T2802" id="Seg_1640" s="T2796">It tells a horse: "I will eat you!"</ta>
            <ta e="T2804" id="Seg_1641" s="T2802">"Well, eat!</ta>
            <ta e="T2806" id="Seg_1642" s="T2804">Eat [my] bottom.</ta>
            <ta e="T2809" id="Seg_1643" s="T2806">There is a lot of meat."</ta>
            <ta e="T2821" id="Seg_1644" s="T2809">It let it eat [the horse let the dog come close], it [the horse] hit it [the dog̥], into the teeth.</ta>
            <ta e="T2822" id="Seg_1645" s="T2821">[Its teeth] are falling out.</ta>
            <ta e="T2827" id="Seg_1646" s="T2822">Then it ran off, it was scared of the horse.</ta>
            <ta e="T2832" id="Seg_1647" s="T2827">Then it comes, a goat comes.</ta>
            <ta e="T2835" id="Seg_1648" s="T2832">"I will eat you!"</ta>
            <ta e="T2841" id="Seg_1649" s="T2835">"Don't eat, I am going to children.</ta>
            <ta e="T2843" id="Seg_1650" s="T2841">I will give [you something] to eat".</ta>
            <ta e="T2847" id="Seg_1651" s="T2843">She left and there is nothing.</ta>
            <ta e="T2851" id="Seg_1652" s="T2847">It goes, it goes, there comes a sheep.</ta>
            <ta e="T2854" id="Seg_1653" s="T2851">"I will eat you!"</ta>
            <ta e="T2859" id="Seg_1654" s="T2854">"I will go to feed the small sheep".</ta>
            <ta e="T2863" id="Seg_1655" s="T2859">It left and there is nothing.</ta>
            <ta e="T2866" id="Seg_1656" s="T2863">Then a camel comes.</ta>
            <ta e="T2869" id="Seg_1657" s="T2866">"I will eat you!".</ta>
            <ta e="T2872" id="Seg_1658" s="T2869">Then it jumped.</ta>
            <ta e="T2878" id="Seg_1659" s="T2872">(Tooth) It has no teeth.</ta>
            <ta e="T2889" id="Seg_1660" s="T2878">And the camel ran and came where there are many people.</ta>
            <ta e="T2908" id="Seg_1661" s="T2889">The people, the dog, they took it and beat strongly, it ran away under the bridge and hid there.</ta>
            <ta e="T2911" id="Seg_1662" s="T2908">They cut off his tail.</ta>
            <ta e="T2917" id="Seg_1663" s="T2911">Then it fell into the river and died.</ta>
            <ta e="T2918" id="Seg_1664" s="T2917">[That’s] all.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T2796" id="Seg_1665" s="T2794">Ein Hund kam.</ta>
            <ta e="T2802" id="Seg_1666" s="T2796">Er sagt einem Pferd: „Ich werde dich fressen!“</ta>
            <ta e="T2804" id="Seg_1667" s="T2802">„Also friss!</ta>
            <ta e="T2806" id="Seg_1668" s="T2804">Friss [meinen] Hintern.</ta>
            <ta e="T2809" id="Seg_1669" s="T2806">Es ist viel Fleisch.“</ta>
            <ta e="T2821" id="Seg_1670" s="T2809">Es lies ihn fressen [das Pferd lies den Hund näher kommen], es [das Pferd] schlug ihn [den Hund], in die Zähne.</ta>
            <ta e="T2822" id="Seg_1671" s="T2821">[Die Zähne] fallen heraus.</ta>
            <ta e="T2827" id="Seg_1672" s="T2822">Dann lief er weg, er hatte Angst vor das Pferd.</ta>
            <ta e="T2832" id="Seg_1673" s="T2827">Dann kommt sie, eine Ziege kommt.</ta>
            <ta e="T2835" id="Seg_1674" s="T2832">„Ich werde dich fressen!“</ta>
            <ta e="T2841" id="Seg_1675" s="T2835">„Friss nicht, ich gehe zu den Kindern.</ta>
            <ta e="T2843" id="Seg_1676" s="T2841">Ich gebe [dir etwas] zu essen.“</ta>
            <ta e="T2847" id="Seg_1677" s="T2843">Er ging und es war nichts da.</ta>
            <ta e="T2851" id="Seg_1678" s="T2847">Er geht, er geht, es kommt ein Schaf.</ta>
            <ta e="T2854" id="Seg_1679" s="T2851">„Ich werde dich fressen!“</ta>
            <ta e="T2859" id="Seg_1680" s="T2854">„Ich will gehen, die kleinen Schafe zu füttern.“</ta>
            <ta e="T2863" id="Seg_1681" s="T2859">Es ging und es war nichts da.</ta>
            <ta e="T2866" id="Seg_1682" s="T2863">Dann kam ein Kamel.</ta>
            <ta e="T2869" id="Seg_1683" s="T2866">„Ich werde dich fressen!“</ta>
            <ta e="T2872" id="Seg_1684" s="T2869">Dann sprang es.</ta>
            <ta e="T2878" id="Seg_1685" s="T2872">(Zahn) Es hat keine Zähne.</ta>
            <ta e="T2889" id="Seg_1686" s="T2878">Und das Kamel rannte und kam zu einem Ort, wo viel Leute waren.</ta>
            <ta e="T2908" id="Seg_1687" s="T2889">Die Leute, den Hund, sie nahmen ihn und verprügelten ihn schwer, er rannte weg unter die Brücke und versteckte sich dort.</ta>
            <ta e="T2911" id="Seg_1688" s="T2908">Sie schnitten ihm den Schwanz ab.</ta>
            <ta e="T2917" id="Seg_1689" s="T2911">Dann fiel er in den Fluss und starb.</ta>
            <ta e="T2918" id="Seg_1690" s="T2917">[Das] war’s.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T2796" id="Seg_1691" s="T2794">[GVY:] This is a variation of a tale about a stupid wolf, see e.g. http://hobbitaniya.ru/buryat/buryat7.php, but with different personnages. </ta>
            <ta e="T2804" id="Seg_1692" s="T2802">[KlT:] pleonastic form (instead of 'amdə’). [GVY:] Or rather a self-correctiion: 2SG.IMP.O instead of 2SG.IMP.S</ta>
            <ta e="T2821" id="Seg_1693" s="T2809">[GVY:] dal (tal?) - Russial "дал" 'let'?</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T2794" />
            <conversion-tli id="T2795" />
            <conversion-tli id="T2796" />
            <conversion-tli id="T2797" />
            <conversion-tli id="T2798" />
            <conversion-tli id="T2799" />
            <conversion-tli id="T2800" />
            <conversion-tli id="T2801" />
            <conversion-tli id="T2802" />
            <conversion-tli id="T2803" />
            <conversion-tli id="T2804" />
            <conversion-tli id="T2805" />
            <conversion-tli id="T2806" />
            <conversion-tli id="T2807" />
            <conversion-tli id="T2808" />
            <conversion-tli id="T2809" />
            <conversion-tli id="T2810" />
            <conversion-tli id="T2811" />
            <conversion-tli id="T2812" />
            <conversion-tli id="T2813" />
            <conversion-tli id="T2814" />
            <conversion-tli id="T2815" />
            <conversion-tli id="T2816" />
            <conversion-tli id="T2817" />
            <conversion-tli id="T2818" />
            <conversion-tli id="T2819" />
            <conversion-tli id="T2820" />
            <conversion-tli id="T2821" />
            <conversion-tli id="T2822" />
            <conversion-tli id="T2823" />
            <conversion-tli id="T2824" />
            <conversion-tli id="T2825" />
            <conversion-tli id="T2826" />
            <conversion-tli id="T2827" />
            <conversion-tli id="T2828" />
            <conversion-tli id="T2829" />
            <conversion-tli id="T2830" />
            <conversion-tli id="T2831" />
            <conversion-tli id="T2832" />
            <conversion-tli id="T2833" />
            <conversion-tli id="T2834" />
            <conversion-tli id="T2835" />
            <conversion-tli id="T2836" />
            <conversion-tli id="T2837" />
            <conversion-tli id="T2838" />
            <conversion-tli id="T2839" />
            <conversion-tli id="T2840" />
            <conversion-tli id="T2841" />
            <conversion-tli id="T2842" />
            <conversion-tli id="T2843" />
            <conversion-tli id="T2844" />
            <conversion-tli id="T2845" />
            <conversion-tli id="T2846" />
            <conversion-tli id="T2847" />
            <conversion-tli id="T2848" />
            <conversion-tli id="T2849" />
            <conversion-tli id="T2850" />
            <conversion-tli id="T2851" />
            <conversion-tli id="T2852" />
            <conversion-tli id="T2853" />
            <conversion-tli id="T2854" />
            <conversion-tli id="T2855" />
            <conversion-tli id="T2856" />
            <conversion-tli id="T2857" />
            <conversion-tli id="T2858" />
            <conversion-tli id="T2859" />
            <conversion-tli id="T2860" />
            <conversion-tli id="T2861" />
            <conversion-tli id="T2862" />
            <conversion-tli id="T2863" />
            <conversion-tli id="T2864" />
            <conversion-tli id="T2865" />
            <conversion-tli id="T2866" />
            <conversion-tli id="T2867" />
            <conversion-tli id="T2868" />
            <conversion-tli id="T2869" />
            <conversion-tli id="T2870" />
            <conversion-tli id="T2871" />
            <conversion-tli id="T2872" />
            <conversion-tli id="T2873" />
            <conversion-tli id="T2874" />
            <conversion-tli id="T2875" />
            <conversion-tli id="T2876" />
            <conversion-tli id="T2877" />
            <conversion-tli id="T2878" />
            <conversion-tli id="T2879" />
            <conversion-tli id="T2880" />
            <conversion-tli id="T2881" />
            <conversion-tli id="T2882" />
            <conversion-tli id="T2883" />
            <conversion-tli id="T2884" />
            <conversion-tli id="T2885" />
            <conversion-tli id="T2886" />
            <conversion-tli id="T2887" />
            <conversion-tli id="T2888" />
            <conversion-tli id="T2889" />
            <conversion-tli id="T2890" />
            <conversion-tli id="T2891" />
            <conversion-tli id="T2892" />
            <conversion-tli id="T2893" />
            <conversion-tli id="T2894" />
            <conversion-tli id="T2895" />
            <conversion-tli id="T2896" />
            <conversion-tli id="T2897" />
            <conversion-tli id="T2898" />
            <conversion-tli id="T2899" />
            <conversion-tli id="T2900" />
            <conversion-tli id="T2901" />
            <conversion-tli id="T2902" />
            <conversion-tli id="T2903" />
            <conversion-tli id="T2904" />
            <conversion-tli id="T2905" />
            <conversion-tli id="T2906" />
            <conversion-tli id="T2907" />
            <conversion-tli id="T2908" />
            <conversion-tli id="T2909" />
            <conversion-tli id="T2910" />
            <conversion-tli id="T2911" />
            <conversion-tli id="T2912" />
            <conversion-tli id="T2913" />
            <conversion-tli id="T2914" />
            <conversion-tli id="T2915" />
            <conversion-tli id="T2916" />
            <conversion-tli id="T2917" />
            <conversion-tli id="T2918" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
