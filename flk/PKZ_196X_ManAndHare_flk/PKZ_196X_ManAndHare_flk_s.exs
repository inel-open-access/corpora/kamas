<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID80509B46-6BA0-BA18-7E13-CBBCBC379BCA">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_ManAndHare_flk.wav" />
         <referenced-file url="PKZ_196X_ManAndHare_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_ManAndHare_flk\PKZ_196X_ManAndHare_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">130</ud-information>
            <ud-information attribute-name="# HIAT:w">94</ud-information>
            <ud-information attribute-name="# e">93</ud-information>
            <ud-information attribute-name="# HIAT:u">21</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.004" type="appl" />
         <tli id="T1" time="0.596" type="appl" />
         <tli id="T2" time="1.189" type="appl" />
         <tli id="T3" time="1.782" type="appl" />
         <tli id="T4" time="2.374" type="appl" />
         <tli id="T5" time="3.106" type="appl" />
         <tli id="T6" time="3.838" type="appl" />
         <tli id="T7" time="4.569" type="appl" />
         <tli id="T8" time="5.301" type="appl" />
         <tli id="T9" time="6.007" type="appl" />
         <tli id="T10" time="6.713" type="appl" />
         <tli id="T11" time="7.418" type="appl" />
         <tli id="T12" time="8.124" type="appl" />
         <tli id="T13" time="9.899958570472045" />
         <tli id="T14" time="10.747" type="appl" />
         <tli id="T15" time="11.481" type="appl" />
         <tli id="T16" time="12.215" type="appl" />
         <tli id="T17" time="13.45327703381319" />
         <tli id="T18" time="14.648" type="appl" />
         <tli id="T19" time="15.953266571811183" />
         <tli id="T20" time="16.748" type="appl" />
         <tli id="T21" time="17.74" type="appl" />
         <tli id="T22" time="18.732" type="appl" />
         <tli id="T23" time="19.723" type="appl" />
         <tli id="T24" time="20.714" type="appl" />
         <tli id="T25" time="21.706" type="appl" />
         <tli id="T26" time="22.697" type="appl" />
         <tli id="T27" time="23.689" type="appl" />
         <tli id="T28" time="24.234" type="appl" />
         <tli id="T29" time="24.779" type="appl" />
         <tli id="T30" time="25.325" type="appl" />
         <tli id="T31" time="25.87" type="appl" />
         <tli id="T32" time="26.553222212922666" />
         <tli id="T33" time="27.274" type="appl" />
         <tli id="T34" time="28.134" type="appl" />
         <tli id="T35" time="28.994" type="appl" />
         <tli id="T36" time="30.30653983930365" />
         <tli id="T37" time="31.078" type="appl" />
         <tli id="T38" time="31.751" type="appl" />
         <tli id="T39" time="32.424" type="appl" />
         <tli id="T40" time="33.577" type="appl" />
         <tli id="T41" time="34.729" type="appl" />
         <tli id="T42" time="35.882" type="appl" />
         <tli id="T43" time="36.297" type="appl" />
         <tli id="T44" time="36.713" type="appl" />
         <tli id="T45" time="37.128" type="appl" />
         <tli id="T46" time="37.543" type="appl" />
         <tli id="T47" time="37.959" type="appl" />
         <tli id="T48" time="38.374" type="appl" />
         <tli id="T49" time="39.008" type="appl" />
         <tli id="T50" time="39.632" type="appl" />
         <tli id="T51" time="40.256" type="appl" />
         <tli id="T52" time="40.879" type="appl" />
         <tli id="T53" time="42.594" type="appl" />
         <tli id="T54" time="44.131" type="appl" />
         <tli id="T55" time="45.669" type="appl" />
         <tli id="T56" time="47.206" type="appl" />
         <tli id="T57" time="48.743" type="appl" />
         <tli id="T58" time="49.975" type="appl" />
         <tli id="T59" time="51.167" type="appl" />
         <tli id="T60" time="53.35311006026112" />
         <tli id="T61" time="54.453" type="appl" />
         <tli id="T62" time="55.557" type="appl" />
         <tli id="T63" time="56.661" type="appl" />
         <tli id="T64" time="57.633" type="appl" />
         <tli id="T65" time="58.358" type="appl" />
         <tli id="T66" time="59.082" type="appl" />
         <tli id="T67" time="59.807" type="appl" />
         <tli id="T68" time="60.531" type="appl" />
         <tli id="T69" time="61.256" type="appl" />
         <tli id="T70" time="61.98" type="appl" />
         <tli id="T71" time="62.705" type="appl" />
         <tli id="T72" time="63.429" type="appl" />
         <tli id="T73" time="65.588" type="appl" />
         <tli id="T74" time="67.747" type="appl" />
         <tli id="T75" time="69.906" type="appl" />
         <tli id="T76" time="70.527" type="appl" />
         <tli id="T77" time="71.148" type="appl" />
         <tli id="T78" time="71.769" type="appl" />
         <tli id="T79" time="72.29" type="appl" />
         <tli id="T80" time="72.782" type="appl" />
         <tli id="T81" time="73.273" type="appl" />
         <tli id="T82" time="73.764" type="appl" />
         <tli id="T83" time="74.202" type="appl" />
         <tli id="T84" time="74.617" type="appl" />
         <tli id="T85" time="75.032" type="appl" />
         <tli id="T86" time="75.447" type="appl" />
         <tli id="T87" time="75.863" type="appl" />
         <tli id="T88" time="76.278" type="appl" />
         <tli id="T89" time="76.693" type="appl" />
         <tli id="T90" time="77.108" type="appl" />
         <tli id="T91" time="77.43300929025779" />
         <tli id="T92" time="78.132" type="appl" />
         <tli id="T93" time="78.742" type="appl" />
         <tli id="T94" time="79.653" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T63" start="T62">
            <tli id="T62.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T94" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Onʼiʔ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">kuza</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">kambi</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">dʼijenə</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">Kuliat:</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">dĭn</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">amnolaʔbə</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">zajats</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_32" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">Dĭ</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">măndə:</ts>
                  <nts id="Seg_38" n="HIAT:ip">"</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">Kutlim</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">dĭ</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">zajasəm</ts>
                  <nts id="Seg_48" n="HIAT:ip">.</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_51" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_53" n="HIAT:w" s="T13">Da</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">dĭ</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">sadarlaʔ</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_62" n="HIAT:w" s="T16">ilem</ts>
                  <nts id="Seg_63" n="HIAT:ip">.</nts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_66" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_68" n="HIAT:w" s="T17">Sadarlim</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_71" n="HIAT:w" s="T18">ujat</ts>
                  <nts id="Seg_72" n="HIAT:ip">.</nts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_75" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_77" n="HIAT:w" s="T19">Sadarlaʔ</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_80" n="HIAT:w" s="T20">ilem</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_83" n="HIAT:w" s="T21">šoška</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_85" n="HIAT:ip">(</nts>
                  <ts e="T23" id="Seg_87" n="HIAT:w" s="T22">šolka</ts>
                  <nts id="Seg_88" n="HIAT:ip">)</nts>
                  <nts id="Seg_89" n="HIAT:ip">,</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_92" n="HIAT:w" s="T23">detləj</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_95" n="HIAT:w" s="T24">măna</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_98" n="HIAT:w" s="T25">bʼeʔ</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_101" n="HIAT:w" s="T26">šoškaʔi</ts>
                  <nts id="Seg_102" n="HIAT:ip">.</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_105" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_107" n="HIAT:w" s="T27">Dĭgəttə</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_110" n="HIAT:w" s="T28">bʼeʔ</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_113" n="HIAT:w" s="T29">šoškaʔi</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_116" n="HIAT:w" s="T30">išo</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_119" n="HIAT:w" s="T31">detleʔi</ts>
                  <nts id="Seg_120" n="HIAT:ip">.</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_123" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_125" n="HIAT:w" s="T32">Dĭgəttə</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_128" n="HIAT:w" s="T33">dĭzem</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_131" n="HIAT:w" s="T34">bar</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_134" n="HIAT:w" s="T35">bătlim</ts>
                  <nts id="Seg_135" n="HIAT:ip">.</nts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_138" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_140" n="HIAT:w" s="T36">I</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_143" n="HIAT:w" s="T37">uja</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_146" n="HIAT:w" s="T38">sadarlim</ts>
                  <nts id="Seg_147" n="HIAT:ip">.</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_150" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_152" n="HIAT:w" s="T39">Dĭgəttə</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_155" n="HIAT:w" s="T40">ne</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_158" n="HIAT:w" s="T41">ilem</ts>
                  <nts id="Seg_159" n="HIAT:ip">.</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_162" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_164" n="HIAT:w" s="T42">Dĭ</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_167" n="HIAT:w" s="T43">ne</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_170" n="HIAT:w" s="T44">detləj</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_173" n="HIAT:w" s="T45">măna</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_176" n="HIAT:w" s="T46">šide</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_179" n="HIAT:w" s="T47">nʼi</ts>
                  <nts id="Seg_180" n="HIAT:ip">.</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_183" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_185" n="HIAT:w" s="T48">Onʼiʔ</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_188" n="HIAT:w" s="T49">Vanʼka</ts>
                  <nts id="Seg_189" n="HIAT:ip">,</nts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_192" n="HIAT:w" s="T50">onʼiʔ</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_195" n="HIAT:w" s="T51">Vasʼka</ts>
                  <nts id="Seg_196" n="HIAT:ip">.</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_199" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_201" n="HIAT:w" s="T52">Dĭgəttə</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_204" n="HIAT:w" s="T53">dĭzeŋ</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_206" n="HIAT:ip">(</nts>
                  <ts e="T55" id="Seg_208" n="HIAT:w" s="T54">maʔ=</ts>
                  <nts id="Seg_209" n="HIAT:ip">)</nts>
                  <nts id="Seg_210" n="HIAT:ip">,</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_213" n="HIAT:w" s="T55">maʔ</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_216" n="HIAT:w" s="T56">nuldlam</ts>
                  <nts id="Seg_217" n="HIAT:ip">.</nts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_220" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_222" n="HIAT:w" s="T57">Dĭgəttə</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_225" n="HIAT:w" s="T58">dĭzeŋ</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_228" n="HIAT:w" s="T59">togonorləʔjə</ts>
                  <nts id="Seg_229" n="HIAT:ip">.</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_232" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_234" n="HIAT:w" s="T60">Măn</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_237" n="HIAT:w" s="T61">amnolam</ts>
                  <nts id="Seg_238" n="HIAT:ip">,</nts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_240" n="HIAT:ip">(</nts>
                  <ts e="T62.tx.1" id="Seg_242" n="HIAT:w" s="T62">nulam</ts>
                  <nts id="Seg_243" n="HIAT:ip">)</nts>
                  <ts e="T63" id="Seg_245" n="HIAT:w" s="T62.tx.1">:</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_248" n="HIAT:w" s="T63">Tăŋ</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_251" n="HIAT:w" s="T64">iʔ</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_254" n="HIAT:w" s="T65">togonoraʔ</ts>
                  <nts id="Seg_255" n="HIAT:ip">,</nts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_258" n="HIAT:w" s="T66">iʔ</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_261" n="HIAT:w" s="T67">iləm</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_263" n="HIAT:ip">(</nts>
                  <ts e="T70" id="Seg_265" n="HIAT:w" s="T69">iʔ=</ts>
                  <nts id="Seg_266" n="HIAT:ip">)</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_269" n="HIAT:w" s="T70">iʔ</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_272" n="HIAT:w" s="T71">toʔnargaʔ</ts>
                  <nts id="Seg_273" n="HIAT:ip">!</nts>
                  <nts id="Seg_274" n="HIAT:ip">"</nts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_277" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_279" n="HIAT:w" s="T72">Dĭgəttə</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_282" n="HIAT:w" s="T73">dĭ</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_285" n="HIAT:w" s="T74">kirgarluʔpi</ts>
                  <nts id="Seg_286" n="HIAT:ip">.</nts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_289" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_291" n="HIAT:w" s="T75">Dĭgəttə</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_294" n="HIAT:w" s="T76">zajas</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_297" n="HIAT:w" s="T77">nuʔməluʔpi</ts>
                  <nts id="Seg_298" n="HIAT:ip">.</nts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_301" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_303" n="HIAT:w" s="T78">Dĭ</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_306" n="HIAT:w" s="T79">maluʔpi</ts>
                  <nts id="Seg_307" n="HIAT:ip">,</nts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_310" n="HIAT:w" s="T80">ĭmbidə</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_313" n="HIAT:w" s="T81">naga</ts>
                  <nts id="Seg_314" n="HIAT:ip">.</nts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_317" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_319" n="HIAT:w" s="T82">I</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_322" n="HIAT:w" s="T83">tura</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_325" n="HIAT:w" s="T84">naga</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_328" n="HIAT:w" s="T85">i</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_331" n="HIAT:w" s="T86">ne</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_334" n="HIAT:w" s="T87">naga</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_337" n="HIAT:w" s="T88">i</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_340" n="HIAT:w" s="T89">šindidə</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_343" n="HIAT:w" s="T90">naga</ts>
                  <nts id="Seg_344" n="HIAT:ip">.</nts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_347" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_349" n="HIAT:w" s="T91">Unnʼa</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_352" n="HIAT:w" s="T92">maluʔpi</ts>
                  <nts id="Seg_353" n="HIAT:ip">.</nts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_356" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_358" n="HIAT:w" s="T93">Kabarləj</ts>
                  <nts id="Seg_359" n="HIAT:ip">.</nts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T94" id="Seg_361" n="sc" s="T0">
               <ts e="T1" id="Seg_363" n="e" s="T0">Onʼiʔ </ts>
               <ts e="T2" id="Seg_365" n="e" s="T1">kuza </ts>
               <ts e="T3" id="Seg_367" n="e" s="T2">kambi </ts>
               <ts e="T4" id="Seg_369" n="e" s="T3">dʼijenə. </ts>
               <ts e="T5" id="Seg_371" n="e" s="T4">Kuliat: </ts>
               <ts e="T6" id="Seg_373" n="e" s="T5">dĭn </ts>
               <ts e="T7" id="Seg_375" n="e" s="T6">amnolaʔbə </ts>
               <ts e="T8" id="Seg_377" n="e" s="T7">zajats. </ts>
               <ts e="T9" id="Seg_379" n="e" s="T8">Dĭ </ts>
               <ts e="T10" id="Seg_381" n="e" s="T9">măndə:" </ts>
               <ts e="T11" id="Seg_383" n="e" s="T10">Kutlim </ts>
               <ts e="T12" id="Seg_385" n="e" s="T11">dĭ </ts>
               <ts e="T13" id="Seg_387" n="e" s="T12">zajasəm. </ts>
               <ts e="T14" id="Seg_389" n="e" s="T13">Da </ts>
               <ts e="T15" id="Seg_391" n="e" s="T14">dĭ </ts>
               <ts e="T16" id="Seg_393" n="e" s="T15">sadarlaʔ </ts>
               <ts e="T17" id="Seg_395" n="e" s="T16">ilem. </ts>
               <ts e="T18" id="Seg_397" n="e" s="T17">Sadarlim </ts>
               <ts e="T19" id="Seg_399" n="e" s="T18">ujat. </ts>
               <ts e="T20" id="Seg_401" n="e" s="T19">Sadarlaʔ </ts>
               <ts e="T21" id="Seg_403" n="e" s="T20">ilem </ts>
               <ts e="T22" id="Seg_405" n="e" s="T21">šoška </ts>
               <ts e="T23" id="Seg_407" n="e" s="T22">(šolka), </ts>
               <ts e="T24" id="Seg_409" n="e" s="T23">detləj </ts>
               <ts e="T25" id="Seg_411" n="e" s="T24">măna </ts>
               <ts e="T26" id="Seg_413" n="e" s="T25">bʼeʔ </ts>
               <ts e="T27" id="Seg_415" n="e" s="T26">šoškaʔi. </ts>
               <ts e="T28" id="Seg_417" n="e" s="T27">Dĭgəttə </ts>
               <ts e="T29" id="Seg_419" n="e" s="T28">bʼeʔ </ts>
               <ts e="T30" id="Seg_421" n="e" s="T29">šoškaʔi </ts>
               <ts e="T31" id="Seg_423" n="e" s="T30">išo </ts>
               <ts e="T32" id="Seg_425" n="e" s="T31">detleʔi. </ts>
               <ts e="T33" id="Seg_427" n="e" s="T32">Dĭgəttə </ts>
               <ts e="T34" id="Seg_429" n="e" s="T33">dĭzem </ts>
               <ts e="T35" id="Seg_431" n="e" s="T34">bar </ts>
               <ts e="T36" id="Seg_433" n="e" s="T35">bătlim. </ts>
               <ts e="T37" id="Seg_435" n="e" s="T36">I </ts>
               <ts e="T38" id="Seg_437" n="e" s="T37">uja </ts>
               <ts e="T39" id="Seg_439" n="e" s="T38">sadarlim. </ts>
               <ts e="T40" id="Seg_441" n="e" s="T39">Dĭgəttə </ts>
               <ts e="T41" id="Seg_443" n="e" s="T40">ne </ts>
               <ts e="T42" id="Seg_445" n="e" s="T41">ilem. </ts>
               <ts e="T43" id="Seg_447" n="e" s="T42">Dĭ </ts>
               <ts e="T44" id="Seg_449" n="e" s="T43">ne </ts>
               <ts e="T45" id="Seg_451" n="e" s="T44">detləj </ts>
               <ts e="T46" id="Seg_453" n="e" s="T45">măna </ts>
               <ts e="T47" id="Seg_455" n="e" s="T46">šide </ts>
               <ts e="T48" id="Seg_457" n="e" s="T47">nʼi. </ts>
               <ts e="T49" id="Seg_459" n="e" s="T48">Onʼiʔ </ts>
               <ts e="T50" id="Seg_461" n="e" s="T49">Vanʼka, </ts>
               <ts e="T51" id="Seg_463" n="e" s="T50">onʼiʔ </ts>
               <ts e="T52" id="Seg_465" n="e" s="T51">Vasʼka. </ts>
               <ts e="T53" id="Seg_467" n="e" s="T52">Dĭgəttə </ts>
               <ts e="T54" id="Seg_469" n="e" s="T53">dĭzeŋ </ts>
               <ts e="T55" id="Seg_471" n="e" s="T54">(maʔ=), </ts>
               <ts e="T56" id="Seg_473" n="e" s="T55">maʔ </ts>
               <ts e="T57" id="Seg_475" n="e" s="T56">nuldlam. </ts>
               <ts e="T58" id="Seg_477" n="e" s="T57">Dĭgəttə </ts>
               <ts e="T59" id="Seg_479" n="e" s="T58">dĭzeŋ </ts>
               <ts e="T60" id="Seg_481" n="e" s="T59">togonorləʔjə. </ts>
               <ts e="T61" id="Seg_483" n="e" s="T60">Măn </ts>
               <ts e="T62" id="Seg_485" n="e" s="T61">amnolam, </ts>
               <ts e="T63" id="Seg_487" n="e" s="T62">(nulam): </ts>
               <ts e="T64" id="Seg_489" n="e" s="T63">Tăŋ </ts>
               <ts e="T65" id="Seg_491" n="e" s="T64">iʔ </ts>
               <ts e="T66" id="Seg_493" n="e" s="T65">togonoraʔ, </ts>
               <ts e="T67" id="Seg_495" n="e" s="T66">iʔ </ts>
               <ts e="T69" id="Seg_497" n="e" s="T67">iləm </ts>
               <ts e="T70" id="Seg_499" n="e" s="T69">(iʔ=) </ts>
               <ts e="T71" id="Seg_501" n="e" s="T70">iʔ </ts>
               <ts e="T72" id="Seg_503" n="e" s="T71">toʔnargaʔ!" </ts>
               <ts e="T73" id="Seg_505" n="e" s="T72">Dĭgəttə </ts>
               <ts e="T74" id="Seg_507" n="e" s="T73">dĭ </ts>
               <ts e="T75" id="Seg_509" n="e" s="T74">kirgarluʔpi. </ts>
               <ts e="T76" id="Seg_511" n="e" s="T75">Dĭgəttə </ts>
               <ts e="T77" id="Seg_513" n="e" s="T76">zajas </ts>
               <ts e="T78" id="Seg_515" n="e" s="T77">nuʔməluʔpi. </ts>
               <ts e="T79" id="Seg_517" n="e" s="T78">Dĭ </ts>
               <ts e="T80" id="Seg_519" n="e" s="T79">maluʔpi, </ts>
               <ts e="T81" id="Seg_521" n="e" s="T80">ĭmbidə </ts>
               <ts e="T82" id="Seg_523" n="e" s="T81">naga. </ts>
               <ts e="T83" id="Seg_525" n="e" s="T82">I </ts>
               <ts e="T84" id="Seg_527" n="e" s="T83">tura </ts>
               <ts e="T85" id="Seg_529" n="e" s="T84">naga </ts>
               <ts e="T86" id="Seg_531" n="e" s="T85">i </ts>
               <ts e="T87" id="Seg_533" n="e" s="T86">ne </ts>
               <ts e="T88" id="Seg_535" n="e" s="T87">naga </ts>
               <ts e="T89" id="Seg_537" n="e" s="T88">i </ts>
               <ts e="T90" id="Seg_539" n="e" s="T89">šindidə </ts>
               <ts e="T91" id="Seg_541" n="e" s="T90">naga. </ts>
               <ts e="T92" id="Seg_543" n="e" s="T91">Unnʼa </ts>
               <ts e="T93" id="Seg_545" n="e" s="T92">maluʔpi. </ts>
               <ts e="T94" id="Seg_547" n="e" s="T93">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_548" s="T0">PKZ_196X_ManAndHare_flk.001 (001)</ta>
            <ta e="T8" id="Seg_549" s="T4">PKZ_196X_ManAndHare_flk.002 (002)</ta>
            <ta e="T13" id="Seg_550" s="T8">PKZ_196X_ManAndHare_flk.003 (003)</ta>
            <ta e="T17" id="Seg_551" s="T13">PKZ_196X_ManAndHare_flk.004 (004)</ta>
            <ta e="T19" id="Seg_552" s="T17">PKZ_196X_ManAndHare_flk.005 (005)</ta>
            <ta e="T27" id="Seg_553" s="T19">PKZ_196X_ManAndHare_flk.006 (006)</ta>
            <ta e="T32" id="Seg_554" s="T27">PKZ_196X_ManAndHare_flk.007 (007)</ta>
            <ta e="T36" id="Seg_555" s="T32">PKZ_196X_ManAndHare_flk.008 (008)</ta>
            <ta e="T39" id="Seg_556" s="T36">PKZ_196X_ManAndHare_flk.009 (009)</ta>
            <ta e="T42" id="Seg_557" s="T39">PKZ_196X_ManAndHare_flk.010 (010)</ta>
            <ta e="T48" id="Seg_558" s="T42">PKZ_196X_ManAndHare_flk.011 (011)</ta>
            <ta e="T52" id="Seg_559" s="T48">PKZ_196X_ManAndHare_flk.012 (012)</ta>
            <ta e="T57" id="Seg_560" s="T52">PKZ_196X_ManAndHare_flk.013 (013)</ta>
            <ta e="T60" id="Seg_561" s="T57">PKZ_196X_ManAndHare_flk.014 (014)</ta>
            <ta e="T72" id="Seg_562" s="T60">PKZ_196X_ManAndHare_flk.015 (015)</ta>
            <ta e="T75" id="Seg_563" s="T72">PKZ_196X_ManAndHare_flk.016 (017)</ta>
            <ta e="T78" id="Seg_564" s="T75">PKZ_196X_ManAndHare_flk.017 (018)</ta>
            <ta e="T82" id="Seg_565" s="T78">PKZ_196X_ManAndHare_flk.018 (019)</ta>
            <ta e="T91" id="Seg_566" s="T82">PKZ_196X_ManAndHare_flk.019 (020)</ta>
            <ta e="T93" id="Seg_567" s="T91">PKZ_196X_ManAndHare_flk.020 (021)</ta>
            <ta e="T94" id="Seg_568" s="T93">PKZ_196X_ManAndHare_flk.021 (022)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_569" s="T0">Onʼiʔ kuza kambi dʼijenə. </ta>
            <ta e="T8" id="Seg_570" s="T4">Kuliat: dĭn amnolaʔbə zajats. </ta>
            <ta e="T13" id="Seg_571" s="T8">Dĭ măndə:" Kutlim dĭ zajasəm. </ta>
            <ta e="T17" id="Seg_572" s="T13">Da dĭ sadarlaʔ ilem. </ta>
            <ta e="T19" id="Seg_573" s="T17">Sadarlim ujat. </ta>
            <ta e="T27" id="Seg_574" s="T19">Sadarlaʔ ilem šoška (šolka), detləj măna bʼeʔ šoškaʔi. </ta>
            <ta e="T32" id="Seg_575" s="T27">Dĭgəttə bʼeʔ šoškaʔi išo detleʔi. </ta>
            <ta e="T36" id="Seg_576" s="T32">Dĭgəttə dĭzem bar bătlim. </ta>
            <ta e="T39" id="Seg_577" s="T36">I uja sadarlim. </ta>
            <ta e="T42" id="Seg_578" s="T39">Dĭgəttə ne ilem. </ta>
            <ta e="T48" id="Seg_579" s="T42">Dĭ ne detləj măna šide nʼi. </ta>
            <ta e="T52" id="Seg_580" s="T48">Onʼiʔ Vanʼka, onʼiʔ Vasʼka. </ta>
            <ta e="T57" id="Seg_581" s="T52">Dĭgəttə dĭzeŋ (maʔ=), maʔ nuldlam. </ta>
            <ta e="T60" id="Seg_582" s="T57">Dĭgəttə dĭzeŋ togonorləʔjə. </ta>
            <ta e="T72" id="Seg_583" s="T60">Măn amnolam, (nulam): "Tăŋ iʔ togonoraʔ, iʔ iləm ((PAUSE)) (iʔ=) iʔ toʔnargaʔ!" </ta>
            <ta e="T75" id="Seg_584" s="T72">Dĭgəttə dĭ kirgarluʔpi. </ta>
            <ta e="T78" id="Seg_585" s="T75">Dĭgəttə zajas nuʔməluʔpi. </ta>
            <ta e="T82" id="Seg_586" s="T78">Dĭ maluʔpi, ĭmbidə naga. </ta>
            <ta e="T91" id="Seg_587" s="T82">I tura naga i ne naga i šindidə naga. </ta>
            <ta e="T93" id="Seg_588" s="T91">Unnʼa maluʔpi. </ta>
            <ta e="T94" id="Seg_589" s="T93">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_590" s="T0">onʼiʔ</ta>
            <ta e="T2" id="Seg_591" s="T1">kuza</ta>
            <ta e="T3" id="Seg_592" s="T2">kam-bi</ta>
            <ta e="T4" id="Seg_593" s="T3">dʼije-nə</ta>
            <ta e="T5" id="Seg_594" s="T4">ku-lia-t</ta>
            <ta e="T6" id="Seg_595" s="T5">dĭn</ta>
            <ta e="T7" id="Seg_596" s="T6">amno-laʔbə</ta>
            <ta e="T8" id="Seg_597" s="T7">zajats</ta>
            <ta e="T9" id="Seg_598" s="T8">dĭ</ta>
            <ta e="T10" id="Seg_599" s="T9">măn-də</ta>
            <ta e="T11" id="Seg_600" s="T10">kut-li-m</ta>
            <ta e="T12" id="Seg_601" s="T11">dĭ</ta>
            <ta e="T13" id="Seg_602" s="T12">zajas-əm</ta>
            <ta e="T14" id="Seg_603" s="T13">da</ta>
            <ta e="T15" id="Seg_604" s="T14">dĭ</ta>
            <ta e="T16" id="Seg_605" s="T15">sadar-laʔ</ta>
            <ta e="T17" id="Seg_606" s="T16">i-le-m</ta>
            <ta e="T18" id="Seg_607" s="T17">sadar-li-m</ta>
            <ta e="T19" id="Seg_608" s="T18">uja-t</ta>
            <ta e="T20" id="Seg_609" s="T19">sadar-laʔ</ta>
            <ta e="T21" id="Seg_610" s="T20">i-le-m</ta>
            <ta e="T22" id="Seg_611" s="T21">šoška</ta>
            <ta e="T23" id="Seg_612" s="T22">šolka</ta>
            <ta e="T24" id="Seg_613" s="T23">det-lə-j</ta>
            <ta e="T25" id="Seg_614" s="T24">măna</ta>
            <ta e="T26" id="Seg_615" s="T25">bʼeʔ</ta>
            <ta e="T27" id="Seg_616" s="T26">šoška-ʔi</ta>
            <ta e="T28" id="Seg_617" s="T27">dĭgəttə</ta>
            <ta e="T29" id="Seg_618" s="T28">bʼeʔ</ta>
            <ta e="T30" id="Seg_619" s="T29">šoška-ʔi</ta>
            <ta e="T31" id="Seg_620" s="T30">išo</ta>
            <ta e="T32" id="Seg_621" s="T31">det-le-ʔi</ta>
            <ta e="T33" id="Seg_622" s="T32">dĭgəttə</ta>
            <ta e="T34" id="Seg_623" s="T33">dĭ-zem</ta>
            <ta e="T35" id="Seg_624" s="T34">bar</ta>
            <ta e="T36" id="Seg_625" s="T35">băt-li-m</ta>
            <ta e="T37" id="Seg_626" s="T36">i</ta>
            <ta e="T38" id="Seg_627" s="T37">uja</ta>
            <ta e="T39" id="Seg_628" s="T38">sadar-li-m</ta>
            <ta e="T40" id="Seg_629" s="T39">dĭgəttə</ta>
            <ta e="T41" id="Seg_630" s="T40">ne</ta>
            <ta e="T42" id="Seg_631" s="T41">i-le-m</ta>
            <ta e="T43" id="Seg_632" s="T42">dĭ</ta>
            <ta e="T44" id="Seg_633" s="T43">ne</ta>
            <ta e="T45" id="Seg_634" s="T44">det-lə-j</ta>
            <ta e="T46" id="Seg_635" s="T45">măna</ta>
            <ta e="T47" id="Seg_636" s="T46">šide</ta>
            <ta e="T48" id="Seg_637" s="T47">nʼi</ta>
            <ta e="T49" id="Seg_638" s="T48">onʼiʔ</ta>
            <ta e="T50" id="Seg_639" s="T49">Vanʼka</ta>
            <ta e="T51" id="Seg_640" s="T50">onʼiʔ</ta>
            <ta e="T52" id="Seg_641" s="T51">Vasʼka</ta>
            <ta e="T53" id="Seg_642" s="T52">dĭgəttə</ta>
            <ta e="T54" id="Seg_643" s="T53">dĭ-zeŋ</ta>
            <ta e="T55" id="Seg_644" s="T54">maʔ</ta>
            <ta e="T56" id="Seg_645" s="T55">maʔ</ta>
            <ta e="T57" id="Seg_646" s="T56">nuld-la-m</ta>
            <ta e="T58" id="Seg_647" s="T57">dĭgəttə</ta>
            <ta e="T59" id="Seg_648" s="T58">dĭ-zeŋ</ta>
            <ta e="T60" id="Seg_649" s="T59">togonor-lə-ʔjə</ta>
            <ta e="T61" id="Seg_650" s="T60">măn</ta>
            <ta e="T62" id="Seg_651" s="T61">amno-la-m</ta>
            <ta e="T63" id="Seg_652" s="T62">nu-la-m</ta>
            <ta e="T64" id="Seg_653" s="T63">tăŋ</ta>
            <ta e="T65" id="Seg_654" s="T64">i-ʔ</ta>
            <ta e="T66" id="Seg_655" s="T65">togonor-a-ʔ</ta>
            <ta e="T67" id="Seg_656" s="T66">i-ʔ</ta>
            <ta e="T69" id="Seg_657" s="T67">il-əm</ta>
            <ta e="T70" id="Seg_658" s="T69">i-ʔ</ta>
            <ta e="T71" id="Seg_659" s="T70">i-ʔ</ta>
            <ta e="T72" id="Seg_660" s="T71">toʔ-nar-gaʔ</ta>
            <ta e="T73" id="Seg_661" s="T72">dĭgəttə</ta>
            <ta e="T74" id="Seg_662" s="T73">dĭ</ta>
            <ta e="T75" id="Seg_663" s="T74">kirgar-luʔ-pi</ta>
            <ta e="T76" id="Seg_664" s="T75">dĭgəttə</ta>
            <ta e="T77" id="Seg_665" s="T76">zajas</ta>
            <ta e="T78" id="Seg_666" s="T77">nuʔmə-luʔ-pi</ta>
            <ta e="T79" id="Seg_667" s="T78">dĭ</ta>
            <ta e="T80" id="Seg_668" s="T79">ma-luʔ-pi</ta>
            <ta e="T81" id="Seg_669" s="T80">ĭmbi=də</ta>
            <ta e="T82" id="Seg_670" s="T81">naga</ta>
            <ta e="T83" id="Seg_671" s="T82">i</ta>
            <ta e="T84" id="Seg_672" s="T83">tura</ta>
            <ta e="T85" id="Seg_673" s="T84">naga</ta>
            <ta e="T86" id="Seg_674" s="T85">i</ta>
            <ta e="T87" id="Seg_675" s="T86">ne</ta>
            <ta e="T88" id="Seg_676" s="T87">naga</ta>
            <ta e="T89" id="Seg_677" s="T88">i</ta>
            <ta e="T90" id="Seg_678" s="T89">šindi=də</ta>
            <ta e="T91" id="Seg_679" s="T90">naga</ta>
            <ta e="T92" id="Seg_680" s="T91">unnʼa</ta>
            <ta e="T93" id="Seg_681" s="T92">ma-luʔ-pi</ta>
            <ta e="T94" id="Seg_682" s="T93">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_683" s="T0">onʼiʔ</ta>
            <ta e="T2" id="Seg_684" s="T1">kuza</ta>
            <ta e="T3" id="Seg_685" s="T2">kan-bi</ta>
            <ta e="T4" id="Seg_686" s="T3">dʼije-Tə</ta>
            <ta e="T5" id="Seg_687" s="T4">ku-liA-t</ta>
            <ta e="T6" id="Seg_688" s="T5">dĭn</ta>
            <ta e="T7" id="Seg_689" s="T6">amno-laʔbə</ta>
            <ta e="T8" id="Seg_690" s="T7">zajats</ta>
            <ta e="T9" id="Seg_691" s="T8">dĭ</ta>
            <ta e="T10" id="Seg_692" s="T9">măn-ntə</ta>
            <ta e="T11" id="Seg_693" s="T10">kut-lV-m</ta>
            <ta e="T12" id="Seg_694" s="T11">dĭ</ta>
            <ta e="T13" id="Seg_695" s="T12">zajats-m</ta>
            <ta e="T14" id="Seg_696" s="T13">da</ta>
            <ta e="T15" id="Seg_697" s="T14">dĭ</ta>
            <ta e="T16" id="Seg_698" s="T15">sădar-lAʔ</ta>
            <ta e="T17" id="Seg_699" s="T16">i-lV-m</ta>
            <ta e="T18" id="Seg_700" s="T17">sădar-lV-m</ta>
            <ta e="T19" id="Seg_701" s="T18">uja-t</ta>
            <ta e="T20" id="Seg_702" s="T19">sădar-lAʔ</ta>
            <ta e="T21" id="Seg_703" s="T20">i-lV-m</ta>
            <ta e="T22" id="Seg_704" s="T21">šoška</ta>
            <ta e="T23" id="Seg_705" s="T22">šolka</ta>
            <ta e="T24" id="Seg_706" s="T23">det-lV-j</ta>
            <ta e="T25" id="Seg_707" s="T24">măna</ta>
            <ta e="T26" id="Seg_708" s="T25">biəʔ</ta>
            <ta e="T27" id="Seg_709" s="T26">šoška-jəʔ</ta>
            <ta e="T28" id="Seg_710" s="T27">dĭgəttə</ta>
            <ta e="T29" id="Seg_711" s="T28">biəʔ</ta>
            <ta e="T30" id="Seg_712" s="T29">šoška-jəʔ</ta>
            <ta e="T31" id="Seg_713" s="T30">ĭššo</ta>
            <ta e="T32" id="Seg_714" s="T31">det-lV-jəʔ</ta>
            <ta e="T33" id="Seg_715" s="T32">dĭgəttə</ta>
            <ta e="T34" id="Seg_716" s="T33">dĭ-zem</ta>
            <ta e="T35" id="Seg_717" s="T34">bar</ta>
            <ta e="T36" id="Seg_718" s="T35">băt-lV-m</ta>
            <ta e="T37" id="Seg_719" s="T36">i</ta>
            <ta e="T38" id="Seg_720" s="T37">uja</ta>
            <ta e="T39" id="Seg_721" s="T38">sădar-lV-m</ta>
            <ta e="T40" id="Seg_722" s="T39">dĭgəttə</ta>
            <ta e="T41" id="Seg_723" s="T40">ne</ta>
            <ta e="T42" id="Seg_724" s="T41">i-lV-m</ta>
            <ta e="T43" id="Seg_725" s="T42">dĭ</ta>
            <ta e="T44" id="Seg_726" s="T43">ne</ta>
            <ta e="T45" id="Seg_727" s="T44">det-lV-j</ta>
            <ta e="T46" id="Seg_728" s="T45">măna</ta>
            <ta e="T47" id="Seg_729" s="T46">šide</ta>
            <ta e="T48" id="Seg_730" s="T47">nʼi</ta>
            <ta e="T49" id="Seg_731" s="T48">onʼiʔ</ta>
            <ta e="T50" id="Seg_732" s="T49">Vanʼka</ta>
            <ta e="T51" id="Seg_733" s="T50">onʼiʔ</ta>
            <ta e="T52" id="Seg_734" s="T51">Vasʼka</ta>
            <ta e="T53" id="Seg_735" s="T52">dĭgəttə</ta>
            <ta e="T54" id="Seg_736" s="T53">dĭ-zAŋ</ta>
            <ta e="T55" id="Seg_737" s="T54">maʔ</ta>
            <ta e="T56" id="Seg_738" s="T55">maʔ</ta>
            <ta e="T57" id="Seg_739" s="T56">nuldə-lV-m</ta>
            <ta e="T58" id="Seg_740" s="T57">dĭgəttə</ta>
            <ta e="T59" id="Seg_741" s="T58">dĭ-zAŋ</ta>
            <ta e="T60" id="Seg_742" s="T59">togonər-lV-jəʔ</ta>
            <ta e="T61" id="Seg_743" s="T60">măn</ta>
            <ta e="T62" id="Seg_744" s="T61">amno-lV-m</ta>
            <ta e="T63" id="Seg_745" s="T62">nu-lV-m</ta>
            <ta e="T64" id="Seg_746" s="T63">tăŋ</ta>
            <ta e="T65" id="Seg_747" s="T64">e-ʔ</ta>
            <ta e="T66" id="Seg_748" s="T65">togonər-ə-ʔ</ta>
            <ta e="T67" id="Seg_749" s="T66">e-ʔ</ta>
            <ta e="T69" id="Seg_750" s="T67">il-m</ta>
            <ta e="T70" id="Seg_751" s="T69">e-ʔ</ta>
            <ta e="T71" id="Seg_752" s="T70">e-ʔ</ta>
            <ta e="T72" id="Seg_753" s="T71">toʔbdə-nar-KAʔ</ta>
            <ta e="T73" id="Seg_754" s="T72">dĭgəttə</ta>
            <ta e="T74" id="Seg_755" s="T73">dĭ</ta>
            <ta e="T75" id="Seg_756" s="T74">kirgaːr-luʔbdə-bi</ta>
            <ta e="T76" id="Seg_757" s="T75">dĭgəttə</ta>
            <ta e="T77" id="Seg_758" s="T76">zajats</ta>
            <ta e="T78" id="Seg_759" s="T77">nuʔmə-luʔbdə-bi</ta>
            <ta e="T79" id="Seg_760" s="T78">dĭ</ta>
            <ta e="T80" id="Seg_761" s="T79">ma-luʔbdə-bi</ta>
            <ta e="T81" id="Seg_762" s="T80">ĭmbi=də</ta>
            <ta e="T82" id="Seg_763" s="T81">naga</ta>
            <ta e="T83" id="Seg_764" s="T82">i</ta>
            <ta e="T84" id="Seg_765" s="T83">tura</ta>
            <ta e="T85" id="Seg_766" s="T84">naga</ta>
            <ta e="T86" id="Seg_767" s="T85">i</ta>
            <ta e="T87" id="Seg_768" s="T86">ne</ta>
            <ta e="T88" id="Seg_769" s="T87">naga</ta>
            <ta e="T89" id="Seg_770" s="T88">i</ta>
            <ta e="T90" id="Seg_771" s="T89">šində=də</ta>
            <ta e="T91" id="Seg_772" s="T90">naga</ta>
            <ta e="T92" id="Seg_773" s="T91">unʼə</ta>
            <ta e="T93" id="Seg_774" s="T92">ma-luʔbdə-bi</ta>
            <ta e="T94" id="Seg_775" s="T93">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_776" s="T0">one.[NOM.SG]</ta>
            <ta e="T2" id="Seg_777" s="T1">man.[NOM.SG]</ta>
            <ta e="T3" id="Seg_778" s="T2">go-PST.[3SG]</ta>
            <ta e="T4" id="Seg_779" s="T3">forest-LAT</ta>
            <ta e="T5" id="Seg_780" s="T4">see-PRS-3SG.O</ta>
            <ta e="T6" id="Seg_781" s="T5">there</ta>
            <ta e="T7" id="Seg_782" s="T6">sit-DUR.[3SG]</ta>
            <ta e="T8" id="Seg_783" s="T7">hare.[NOM.SG]</ta>
            <ta e="T9" id="Seg_784" s="T8">this.[NOM.SG]</ta>
            <ta e="T10" id="Seg_785" s="T9">say-IPFVZ.[3SG]</ta>
            <ta e="T11" id="Seg_786" s="T10">kill-FUT-1SG</ta>
            <ta e="T12" id="Seg_787" s="T11">this.[NOM.SG]</ta>
            <ta e="T13" id="Seg_788" s="T12">hare-ACC</ta>
            <ta e="T14" id="Seg_789" s="T13">and</ta>
            <ta e="T15" id="Seg_790" s="T14">this.[NOM.SG]</ta>
            <ta e="T16" id="Seg_791" s="T15">sell-CVB</ta>
            <ta e="T17" id="Seg_792" s="T16">take-FUT-1SG</ta>
            <ta e="T18" id="Seg_793" s="T17">sell-FUT-1SG</ta>
            <ta e="T19" id="Seg_794" s="T18">meat-NOM/GEN.3SG</ta>
            <ta e="T20" id="Seg_795" s="T19">sell-CVB</ta>
            <ta e="T21" id="Seg_796" s="T20">take-FUT-1SG</ta>
            <ta e="T22" id="Seg_797" s="T21">pig.[NOM.SG]</ta>
            <ta e="T23" id="Seg_798" s="T22">%%</ta>
            <ta e="T24" id="Seg_799" s="T23">bring-FUT-3SG</ta>
            <ta e="T25" id="Seg_800" s="T24">I.LAT</ta>
            <ta e="T26" id="Seg_801" s="T25">ten.[NOM.SG]</ta>
            <ta e="T27" id="Seg_802" s="T26">pig-PL</ta>
            <ta e="T28" id="Seg_803" s="T27">then</ta>
            <ta e="T29" id="Seg_804" s="T28">ten.[NOM.SG]</ta>
            <ta e="T30" id="Seg_805" s="T29">pig-PL</ta>
            <ta e="T31" id="Seg_806" s="T30">more</ta>
            <ta e="T32" id="Seg_807" s="T31">bring-FUT-3PL</ta>
            <ta e="T33" id="Seg_808" s="T32">then</ta>
            <ta e="T34" id="Seg_809" s="T33">this-ACC.PL</ta>
            <ta e="T35" id="Seg_810" s="T34">all</ta>
            <ta e="T36" id="Seg_811" s="T35">cut-FUT-1SG</ta>
            <ta e="T37" id="Seg_812" s="T36">and</ta>
            <ta e="T38" id="Seg_813" s="T37">meat.[NOM.SG]</ta>
            <ta e="T39" id="Seg_814" s="T38">sell-FUT-1SG</ta>
            <ta e="T40" id="Seg_815" s="T39">then</ta>
            <ta e="T41" id="Seg_816" s="T40">woman.[NOM.SG]</ta>
            <ta e="T42" id="Seg_817" s="T41">take-FUT-1SG</ta>
            <ta e="T43" id="Seg_818" s="T42">this.[NOM.SG]</ta>
            <ta e="T44" id="Seg_819" s="T43">woman.[NOM.SG]</ta>
            <ta e="T45" id="Seg_820" s="T44">bring-FUT-3SG</ta>
            <ta e="T46" id="Seg_821" s="T45">I.LAT</ta>
            <ta e="T47" id="Seg_822" s="T46">two.[NOM.SG]</ta>
            <ta e="T48" id="Seg_823" s="T47">boy.[NOM.SG]</ta>
            <ta e="T49" id="Seg_824" s="T48">one.[NOM.SG]</ta>
            <ta e="T50" id="Seg_825" s="T49">Vanka.[NOM.SG]</ta>
            <ta e="T51" id="Seg_826" s="T50">one.[NOM.SG]</ta>
            <ta e="T52" id="Seg_827" s="T51">Vaska.[NOM.SG]</ta>
            <ta e="T53" id="Seg_828" s="T52">then</ta>
            <ta e="T54" id="Seg_829" s="T53">this-PL</ta>
            <ta e="T55" id="Seg_830" s="T54">tent.[NOM.SG]</ta>
            <ta e="T56" id="Seg_831" s="T55">tent.[NOM.SG]</ta>
            <ta e="T57" id="Seg_832" s="T56">place-FUT-1SG</ta>
            <ta e="T58" id="Seg_833" s="T57">then</ta>
            <ta e="T59" id="Seg_834" s="T58">this-PL</ta>
            <ta e="T60" id="Seg_835" s="T59">work-FUT-3PL</ta>
            <ta e="T61" id="Seg_836" s="T60">I.NOM</ta>
            <ta e="T62" id="Seg_837" s="T61">sit-FUT-1SG</ta>
            <ta e="T63" id="Seg_838" s="T62">stand-FUT-1SG</ta>
            <ta e="T64" id="Seg_839" s="T63">strongly</ta>
            <ta e="T65" id="Seg_840" s="T64">NEG.AUX-IMP.2SG</ta>
            <ta e="T66" id="Seg_841" s="T65">work-EP-CNG</ta>
            <ta e="T67" id="Seg_842" s="T66">NEG.AUX-IMP.2SG</ta>
            <ta e="T69" id="Seg_843" s="T67">people-ACC</ta>
            <ta e="T70" id="Seg_844" s="T69">NEG.AUX-IMP.2SG</ta>
            <ta e="T71" id="Seg_845" s="T70">NEG.AUX-IMP.2SG</ta>
            <ta e="T72" id="Seg_846" s="T71">hit-MULT-CNG</ta>
            <ta e="T73" id="Seg_847" s="T72">then</ta>
            <ta e="T74" id="Seg_848" s="T73">this.[NOM.SG]</ta>
            <ta e="T75" id="Seg_849" s="T74">shout-MOM-PST.[3SG]</ta>
            <ta e="T76" id="Seg_850" s="T75">then</ta>
            <ta e="T77" id="Seg_851" s="T76">hare.[NOM.SG]</ta>
            <ta e="T78" id="Seg_852" s="T77">run-MOM-PST.[3SG]</ta>
            <ta e="T79" id="Seg_853" s="T78">this.[NOM.SG]</ta>
            <ta e="T80" id="Seg_854" s="T79">remain-MOM-PST.[3SG]</ta>
            <ta e="T81" id="Seg_855" s="T80">what.[NOM.SG]=INDEF</ta>
            <ta e="T82" id="Seg_856" s="T81">NEG.EX</ta>
            <ta e="T83" id="Seg_857" s="T82">and</ta>
            <ta e="T84" id="Seg_858" s="T83">house.[NOM.SG]</ta>
            <ta e="T85" id="Seg_859" s="T84">NEG.EX</ta>
            <ta e="T86" id="Seg_860" s="T85">and</ta>
            <ta e="T87" id="Seg_861" s="T86">woman.[NOM.SG]</ta>
            <ta e="T88" id="Seg_862" s="T87">NEG.EX</ta>
            <ta e="T89" id="Seg_863" s="T88">and</ta>
            <ta e="T90" id="Seg_864" s="T89">who.[NOM.SG]=INDEF</ta>
            <ta e="T91" id="Seg_865" s="T90">NEG.EX</ta>
            <ta e="T92" id="Seg_866" s="T91">alone</ta>
            <ta e="T93" id="Seg_867" s="T92">remain-MOM-PST.[3SG]</ta>
            <ta e="T94" id="Seg_868" s="T93">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_869" s="T0">один.[NOM.SG]</ta>
            <ta e="T2" id="Seg_870" s="T1">мужчина.[NOM.SG]</ta>
            <ta e="T3" id="Seg_871" s="T2">пойти-PST.[3SG]</ta>
            <ta e="T4" id="Seg_872" s="T3">лес-LAT</ta>
            <ta e="T5" id="Seg_873" s="T4">видеть-PRS-3SG.O</ta>
            <ta e="T6" id="Seg_874" s="T5">там</ta>
            <ta e="T7" id="Seg_875" s="T6">сидеть-DUR.[3SG]</ta>
            <ta e="T8" id="Seg_876" s="T7">заяц.[NOM.SG]</ta>
            <ta e="T9" id="Seg_877" s="T8">этот.[NOM.SG]</ta>
            <ta e="T10" id="Seg_878" s="T9">сказать-IPFVZ.[3SG]</ta>
            <ta e="T11" id="Seg_879" s="T10">убить-FUT-1SG</ta>
            <ta e="T12" id="Seg_880" s="T11">этот.[NOM.SG]</ta>
            <ta e="T13" id="Seg_881" s="T12">заяц-ACC</ta>
            <ta e="T14" id="Seg_882" s="T13">и</ta>
            <ta e="T15" id="Seg_883" s="T14">этот.[NOM.SG]</ta>
            <ta e="T16" id="Seg_884" s="T15">продавать-CVB</ta>
            <ta e="T17" id="Seg_885" s="T16">взять-FUT-1SG</ta>
            <ta e="T18" id="Seg_886" s="T17">продавать-FUT-1SG</ta>
            <ta e="T19" id="Seg_887" s="T18">мясо-NOM/GEN.3SG</ta>
            <ta e="T20" id="Seg_888" s="T19">продавать-CVB</ta>
            <ta e="T21" id="Seg_889" s="T20">взять-FUT-1SG</ta>
            <ta e="T22" id="Seg_890" s="T21">свинья.[NOM.SG]</ta>
            <ta e="T23" id="Seg_891" s="T22">%%</ta>
            <ta e="T24" id="Seg_892" s="T23">принести-FUT-3SG</ta>
            <ta e="T25" id="Seg_893" s="T24">я.LAT</ta>
            <ta e="T26" id="Seg_894" s="T25">десять.[NOM.SG]</ta>
            <ta e="T27" id="Seg_895" s="T26">свинья-PL</ta>
            <ta e="T28" id="Seg_896" s="T27">тогда</ta>
            <ta e="T29" id="Seg_897" s="T28">десять.[NOM.SG]</ta>
            <ta e="T30" id="Seg_898" s="T29">свинья-PL</ta>
            <ta e="T31" id="Seg_899" s="T30">еще</ta>
            <ta e="T32" id="Seg_900" s="T31">принести-FUT-3PL</ta>
            <ta e="T33" id="Seg_901" s="T32">тогда</ta>
            <ta e="T34" id="Seg_902" s="T33">этот-ACC.PL</ta>
            <ta e="T35" id="Seg_903" s="T34">весь</ta>
            <ta e="T36" id="Seg_904" s="T35">резать-FUT-1SG</ta>
            <ta e="T37" id="Seg_905" s="T36">и</ta>
            <ta e="T38" id="Seg_906" s="T37">мясо.[NOM.SG]</ta>
            <ta e="T39" id="Seg_907" s="T38">продавать-FUT-1SG</ta>
            <ta e="T40" id="Seg_908" s="T39">тогда</ta>
            <ta e="T41" id="Seg_909" s="T40">женщина.[NOM.SG]</ta>
            <ta e="T42" id="Seg_910" s="T41">взять-FUT-1SG</ta>
            <ta e="T43" id="Seg_911" s="T42">этот.[NOM.SG]</ta>
            <ta e="T44" id="Seg_912" s="T43">женщина.[NOM.SG]</ta>
            <ta e="T45" id="Seg_913" s="T44">принести-FUT-3SG</ta>
            <ta e="T46" id="Seg_914" s="T45">я.LAT</ta>
            <ta e="T47" id="Seg_915" s="T46">два.[NOM.SG]</ta>
            <ta e="T48" id="Seg_916" s="T47">мальчик.[NOM.SG]</ta>
            <ta e="T49" id="Seg_917" s="T48">один.[NOM.SG]</ta>
            <ta e="T50" id="Seg_918" s="T49">Ванька.[NOM.SG]</ta>
            <ta e="T51" id="Seg_919" s="T50">один.[NOM.SG]</ta>
            <ta e="T52" id="Seg_920" s="T51">Васька.[NOM.SG]</ta>
            <ta e="T53" id="Seg_921" s="T52">тогда</ta>
            <ta e="T54" id="Seg_922" s="T53">этот-PL</ta>
            <ta e="T55" id="Seg_923" s="T54">чум.[NOM.SG]</ta>
            <ta e="T56" id="Seg_924" s="T55">чум.[NOM.SG]</ta>
            <ta e="T57" id="Seg_925" s="T56">поставить-FUT-1SG</ta>
            <ta e="T58" id="Seg_926" s="T57">тогда</ta>
            <ta e="T59" id="Seg_927" s="T58">этот-PL</ta>
            <ta e="T60" id="Seg_928" s="T59">работать-FUT-3PL</ta>
            <ta e="T61" id="Seg_929" s="T60">я.NOM</ta>
            <ta e="T62" id="Seg_930" s="T61">сидеть-FUT-1SG</ta>
            <ta e="T63" id="Seg_931" s="T62">стоять-FUT-1SG</ta>
            <ta e="T64" id="Seg_932" s="T63">сильно</ta>
            <ta e="T65" id="Seg_933" s="T64">NEG.AUX-IMP.2SG</ta>
            <ta e="T66" id="Seg_934" s="T65">работать-EP-CNG</ta>
            <ta e="T67" id="Seg_935" s="T66">NEG.AUX-IMP.2SG</ta>
            <ta e="T69" id="Seg_936" s="T67">люди-ACC</ta>
            <ta e="T70" id="Seg_937" s="T69">NEG.AUX-IMP.2SG</ta>
            <ta e="T71" id="Seg_938" s="T70">NEG.AUX-IMP.2SG</ta>
            <ta e="T72" id="Seg_939" s="T71">ударить-MULT-CNG</ta>
            <ta e="T73" id="Seg_940" s="T72">тогда</ta>
            <ta e="T74" id="Seg_941" s="T73">этот.[NOM.SG]</ta>
            <ta e="T75" id="Seg_942" s="T74">кричать-MOM-PST.[3SG]</ta>
            <ta e="T76" id="Seg_943" s="T75">тогда</ta>
            <ta e="T77" id="Seg_944" s="T76">заяц.[NOM.SG]</ta>
            <ta e="T78" id="Seg_945" s="T77">бежать-MOM-PST.[3SG]</ta>
            <ta e="T79" id="Seg_946" s="T78">этот.[NOM.SG]</ta>
            <ta e="T80" id="Seg_947" s="T79">остаться-MOM-PST.[3SG]</ta>
            <ta e="T81" id="Seg_948" s="T80">что.[NOM.SG]=INDEF</ta>
            <ta e="T82" id="Seg_949" s="T81">NEG.EX</ta>
            <ta e="T83" id="Seg_950" s="T82">и</ta>
            <ta e="T84" id="Seg_951" s="T83">дом.[NOM.SG]</ta>
            <ta e="T85" id="Seg_952" s="T84">NEG.EX</ta>
            <ta e="T86" id="Seg_953" s="T85">и</ta>
            <ta e="T87" id="Seg_954" s="T86">женщина.[NOM.SG]</ta>
            <ta e="T88" id="Seg_955" s="T87">NEG.EX</ta>
            <ta e="T89" id="Seg_956" s="T88">и</ta>
            <ta e="T90" id="Seg_957" s="T89">кто.[NOM.SG]=INDEF</ta>
            <ta e="T91" id="Seg_958" s="T90">NEG.EX</ta>
            <ta e="T92" id="Seg_959" s="T91">один</ta>
            <ta e="T93" id="Seg_960" s="T92">остаться-MOM-PST.[3SG]</ta>
            <ta e="T94" id="Seg_961" s="T93">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_962" s="T0">num-n:case</ta>
            <ta e="T2" id="Seg_963" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_964" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_965" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_966" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_967" s="T5">adv</ta>
            <ta e="T7" id="Seg_968" s="T6">v-v&gt;v-v:pn</ta>
            <ta e="T8" id="Seg_969" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_970" s="T8">dempro-n:case</ta>
            <ta e="T10" id="Seg_971" s="T9">v-v&gt;v-v:pn</ta>
            <ta e="T11" id="Seg_972" s="T10">v-v:tense-v:pn</ta>
            <ta e="T12" id="Seg_973" s="T11">dempro-n:case</ta>
            <ta e="T13" id="Seg_974" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_975" s="T13">conj</ta>
            <ta e="T15" id="Seg_976" s="T14">dempro-n:case</ta>
            <ta e="T16" id="Seg_977" s="T15">v-v:n.fin</ta>
            <ta e="T17" id="Seg_978" s="T16">v-v:tense-v:pn</ta>
            <ta e="T18" id="Seg_979" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_980" s="T18">n-n:case.poss</ta>
            <ta e="T20" id="Seg_981" s="T19">v-v:n.fin</ta>
            <ta e="T21" id="Seg_982" s="T20">v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_983" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_984" s="T22">%%</ta>
            <ta e="T24" id="Seg_985" s="T23">v-v:tense-v:pn</ta>
            <ta e="T25" id="Seg_986" s="T24">pers</ta>
            <ta e="T26" id="Seg_987" s="T25">num-n:case</ta>
            <ta e="T27" id="Seg_988" s="T26">n-n:num</ta>
            <ta e="T28" id="Seg_989" s="T27">adv</ta>
            <ta e="T29" id="Seg_990" s="T28">num-n:case</ta>
            <ta e="T30" id="Seg_991" s="T29">n-n:num</ta>
            <ta e="T31" id="Seg_992" s="T30">adv</ta>
            <ta e="T32" id="Seg_993" s="T31">v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_994" s="T32">adv</ta>
            <ta e="T34" id="Seg_995" s="T33">dempro-n:case</ta>
            <ta e="T35" id="Seg_996" s="T34">quant</ta>
            <ta e="T36" id="Seg_997" s="T35">v-v:tense-v:pn</ta>
            <ta e="T37" id="Seg_998" s="T36">conj</ta>
            <ta e="T38" id="Seg_999" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_1000" s="T38">v-v:tense-v:pn</ta>
            <ta e="T40" id="Seg_1001" s="T39">adv</ta>
            <ta e="T41" id="Seg_1002" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_1003" s="T41">v-v:tense-v:pn</ta>
            <ta e="T43" id="Seg_1004" s="T42">dempro-n:case</ta>
            <ta e="T44" id="Seg_1005" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_1006" s="T44">v-v:tense-v:pn</ta>
            <ta e="T46" id="Seg_1007" s="T45">pers</ta>
            <ta e="T47" id="Seg_1008" s="T46">num-n:case</ta>
            <ta e="T48" id="Seg_1009" s="T47">n-n:case</ta>
            <ta e="T49" id="Seg_1010" s="T48">num-n:case</ta>
            <ta e="T50" id="Seg_1011" s="T49">propr-n:case</ta>
            <ta e="T51" id="Seg_1012" s="T50">num-n:case</ta>
            <ta e="T52" id="Seg_1013" s="T51">propr-n:case</ta>
            <ta e="T53" id="Seg_1014" s="T52">adv</ta>
            <ta e="T54" id="Seg_1015" s="T53">dempro-n:num</ta>
            <ta e="T55" id="Seg_1016" s="T54">n-n:case</ta>
            <ta e="T56" id="Seg_1017" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_1018" s="T56">v-v:tense-v:pn</ta>
            <ta e="T58" id="Seg_1019" s="T57">adv</ta>
            <ta e="T59" id="Seg_1020" s="T58">dempro-n:num</ta>
            <ta e="T60" id="Seg_1021" s="T59">v-v:tense-v:pn</ta>
            <ta e="T61" id="Seg_1022" s="T60">pers</ta>
            <ta e="T62" id="Seg_1023" s="T61">v-v:tense-v:pn</ta>
            <ta e="T63" id="Seg_1024" s="T62">v-v:tense-v:pn</ta>
            <ta e="T64" id="Seg_1025" s="T63">adv</ta>
            <ta e="T65" id="Seg_1026" s="T64">aux-v:mood.pn</ta>
            <ta e="T66" id="Seg_1027" s="T65">v-v:ins-v:mood.pn</ta>
            <ta e="T67" id="Seg_1028" s="T66">aux-v:mood.pn</ta>
            <ta e="T69" id="Seg_1029" s="T67">n-n:case</ta>
            <ta e="T70" id="Seg_1030" s="T69">aux-v:mood.pn</ta>
            <ta e="T71" id="Seg_1031" s="T70">aux-v:mood.pn</ta>
            <ta e="T72" id="Seg_1032" s="T71">v-v&gt;v-v:mood.pn</ta>
            <ta e="T73" id="Seg_1033" s="T72">adv</ta>
            <ta e="T74" id="Seg_1034" s="T73">dempro-n:case</ta>
            <ta e="T75" id="Seg_1035" s="T74">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T76" id="Seg_1036" s="T75">adv</ta>
            <ta e="T77" id="Seg_1037" s="T76">n.[n:case]</ta>
            <ta e="T78" id="Seg_1038" s="T77">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T79" id="Seg_1039" s="T78">dempro-n:case</ta>
            <ta e="T80" id="Seg_1040" s="T79">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T81" id="Seg_1041" s="T80">que-n:case=ptcl</ta>
            <ta e="T82" id="Seg_1042" s="T81">v</ta>
            <ta e="T83" id="Seg_1043" s="T82">conj</ta>
            <ta e="T84" id="Seg_1044" s="T83">n-n:case</ta>
            <ta e="T85" id="Seg_1045" s="T84">v</ta>
            <ta e="T86" id="Seg_1046" s="T85">conj</ta>
            <ta e="T87" id="Seg_1047" s="T86">n-n:case</ta>
            <ta e="T88" id="Seg_1048" s="T87">v</ta>
            <ta e="T89" id="Seg_1049" s="T88">conj</ta>
            <ta e="T90" id="Seg_1050" s="T89">que-n:case=ptcl</ta>
            <ta e="T91" id="Seg_1051" s="T90">v</ta>
            <ta e="T92" id="Seg_1052" s="T91">adv</ta>
            <ta e="T93" id="Seg_1053" s="T92">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T94" id="Seg_1054" s="T93">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1055" s="T0">num</ta>
            <ta e="T2" id="Seg_1056" s="T1">n</ta>
            <ta e="T3" id="Seg_1057" s="T2">v</ta>
            <ta e="T4" id="Seg_1058" s="T3">n</ta>
            <ta e="T5" id="Seg_1059" s="T4">v</ta>
            <ta e="T6" id="Seg_1060" s="T5">adv</ta>
            <ta e="T7" id="Seg_1061" s="T6">v</ta>
            <ta e="T8" id="Seg_1062" s="T7">n</ta>
            <ta e="T9" id="Seg_1063" s="T8">dempro</ta>
            <ta e="T10" id="Seg_1064" s="T9">v</ta>
            <ta e="T11" id="Seg_1065" s="T10">v</ta>
            <ta e="T12" id="Seg_1066" s="T11">dempro</ta>
            <ta e="T13" id="Seg_1067" s="T12">n</ta>
            <ta e="T14" id="Seg_1068" s="T13">conj</ta>
            <ta e="T15" id="Seg_1069" s="T14">dempro</ta>
            <ta e="T16" id="Seg_1070" s="T15">v</ta>
            <ta e="T17" id="Seg_1071" s="T16">v</ta>
            <ta e="T18" id="Seg_1072" s="T17">v</ta>
            <ta e="T19" id="Seg_1073" s="T18">n</ta>
            <ta e="T20" id="Seg_1074" s="T19">v</ta>
            <ta e="T21" id="Seg_1075" s="T20">v</ta>
            <ta e="T22" id="Seg_1076" s="T21">n</ta>
            <ta e="T24" id="Seg_1077" s="T23">v</ta>
            <ta e="T25" id="Seg_1078" s="T24">pers</ta>
            <ta e="T26" id="Seg_1079" s="T25">num</ta>
            <ta e="T27" id="Seg_1080" s="T26">n</ta>
            <ta e="T28" id="Seg_1081" s="T27">adv</ta>
            <ta e="T29" id="Seg_1082" s="T28">num</ta>
            <ta e="T30" id="Seg_1083" s="T29">n</ta>
            <ta e="T31" id="Seg_1084" s="T30">adv</ta>
            <ta e="T32" id="Seg_1085" s="T31">v</ta>
            <ta e="T33" id="Seg_1086" s="T32">adv</ta>
            <ta e="T34" id="Seg_1087" s="T33">dempro</ta>
            <ta e="T35" id="Seg_1088" s="T34">quant</ta>
            <ta e="T36" id="Seg_1089" s="T35">v</ta>
            <ta e="T37" id="Seg_1090" s="T36">conj</ta>
            <ta e="T38" id="Seg_1091" s="T37">n</ta>
            <ta e="T39" id="Seg_1092" s="T38">v</ta>
            <ta e="T40" id="Seg_1093" s="T39">adv</ta>
            <ta e="T41" id="Seg_1094" s="T40">n</ta>
            <ta e="T42" id="Seg_1095" s="T41">v</ta>
            <ta e="T43" id="Seg_1096" s="T42">dempro</ta>
            <ta e="T44" id="Seg_1097" s="T43">n</ta>
            <ta e="T45" id="Seg_1098" s="T44">v</ta>
            <ta e="T46" id="Seg_1099" s="T45">pers</ta>
            <ta e="T47" id="Seg_1100" s="T46">num</ta>
            <ta e="T48" id="Seg_1101" s="T47">n</ta>
            <ta e="T49" id="Seg_1102" s="T48">num</ta>
            <ta e="T50" id="Seg_1103" s="T49">propr</ta>
            <ta e="T51" id="Seg_1104" s="T50">num</ta>
            <ta e="T52" id="Seg_1105" s="T51">propr</ta>
            <ta e="T53" id="Seg_1106" s="T52">adv</ta>
            <ta e="T54" id="Seg_1107" s="T53">dempro</ta>
            <ta e="T55" id="Seg_1108" s="T54">n</ta>
            <ta e="T56" id="Seg_1109" s="T55">n</ta>
            <ta e="T57" id="Seg_1110" s="T56">v</ta>
            <ta e="T58" id="Seg_1111" s="T57">adv</ta>
            <ta e="T59" id="Seg_1112" s="T58">dempro</ta>
            <ta e="T60" id="Seg_1113" s="T59">v</ta>
            <ta e="T61" id="Seg_1114" s="T60">pers</ta>
            <ta e="T62" id="Seg_1115" s="T61">v</ta>
            <ta e="T63" id="Seg_1116" s="T62">v</ta>
            <ta e="T64" id="Seg_1117" s="T63">adv</ta>
            <ta e="T65" id="Seg_1118" s="T64">aux</ta>
            <ta e="T66" id="Seg_1119" s="T65">v</ta>
            <ta e="T67" id="Seg_1120" s="T66">aux</ta>
            <ta e="T69" id="Seg_1121" s="T67">n</ta>
            <ta e="T70" id="Seg_1122" s="T69">aux</ta>
            <ta e="T71" id="Seg_1123" s="T70">aux</ta>
            <ta e="T72" id="Seg_1124" s="T71">v</ta>
            <ta e="T73" id="Seg_1125" s="T72">adv</ta>
            <ta e="T74" id="Seg_1126" s="T73">dempro</ta>
            <ta e="T75" id="Seg_1127" s="T74">v</ta>
            <ta e="T76" id="Seg_1128" s="T75">adv</ta>
            <ta e="T77" id="Seg_1129" s="T76">n</ta>
            <ta e="T78" id="Seg_1130" s="T77">v</ta>
            <ta e="T79" id="Seg_1131" s="T78">dempro</ta>
            <ta e="T80" id="Seg_1132" s="T79">v</ta>
            <ta e="T81" id="Seg_1133" s="T80">que</ta>
            <ta e="T82" id="Seg_1134" s="T81">v</ta>
            <ta e="T83" id="Seg_1135" s="T82">conj</ta>
            <ta e="T84" id="Seg_1136" s="T83">n</ta>
            <ta e="T85" id="Seg_1137" s="T84">v</ta>
            <ta e="T86" id="Seg_1138" s="T85">conj</ta>
            <ta e="T87" id="Seg_1139" s="T86">n</ta>
            <ta e="T88" id="Seg_1140" s="T87">v</ta>
            <ta e="T89" id="Seg_1141" s="T88">conj</ta>
            <ta e="T90" id="Seg_1142" s="T89">que</ta>
            <ta e="T91" id="Seg_1143" s="T90">v</ta>
            <ta e="T92" id="Seg_1144" s="T91">adv</ta>
            <ta e="T93" id="Seg_1145" s="T92">v</ta>
            <ta e="T94" id="Seg_1146" s="T93">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_1147" s="T1">np.h:A</ta>
            <ta e="T4" id="Seg_1148" s="T3">np:G</ta>
            <ta e="T5" id="Seg_1149" s="T4">0.3.h:E</ta>
            <ta e="T6" id="Seg_1150" s="T5">adv:L</ta>
            <ta e="T8" id="Seg_1151" s="T7">np:E</ta>
            <ta e="T9" id="Seg_1152" s="T8">pro.h:E</ta>
            <ta e="T11" id="Seg_1153" s="T10">0.1.h:A</ta>
            <ta e="T13" id="Seg_1154" s="T12">np:P</ta>
            <ta e="T15" id="Seg_1155" s="T14">pro:Th</ta>
            <ta e="T17" id="Seg_1156" s="T16">0.1.h:A</ta>
            <ta e="T18" id="Seg_1157" s="T17">0.1.h:A</ta>
            <ta e="T19" id="Seg_1158" s="T18">np:Th</ta>
            <ta e="T21" id="Seg_1159" s="T20">0.1.h:A</ta>
            <ta e="T22" id="Seg_1160" s="T21">np:Th</ta>
            <ta e="T24" id="Seg_1161" s="T23">0.3:A</ta>
            <ta e="T25" id="Seg_1162" s="T24">pro.h:R</ta>
            <ta e="T27" id="Seg_1163" s="T26">np:P</ta>
            <ta e="T28" id="Seg_1164" s="T27">adv:Time</ta>
            <ta e="T30" id="Seg_1165" s="T29">np:P</ta>
            <ta e="T32" id="Seg_1166" s="T31">0.3:A</ta>
            <ta e="T33" id="Seg_1167" s="T32">adv:Time</ta>
            <ta e="T34" id="Seg_1168" s="T33">pro:P</ta>
            <ta e="T36" id="Seg_1169" s="T35">0.1.h:A</ta>
            <ta e="T38" id="Seg_1170" s="T37">np:Th</ta>
            <ta e="T39" id="Seg_1171" s="T38">0.1.h:A</ta>
            <ta e="T40" id="Seg_1172" s="T39">adv:Time</ta>
            <ta e="T41" id="Seg_1173" s="T40">np.h:Th</ta>
            <ta e="T42" id="Seg_1174" s="T41">0.1.h:A</ta>
            <ta e="T44" id="Seg_1175" s="T43">np.h:A</ta>
            <ta e="T46" id="Seg_1176" s="T45">pro.h:R</ta>
            <ta e="T48" id="Seg_1177" s="T47">np.h:Th</ta>
            <ta e="T49" id="Seg_1178" s="T48">np.h:Th</ta>
            <ta e="T50" id="Seg_1179" s="T49">np.h:Th</ta>
            <ta e="T51" id="Seg_1180" s="T50">np.h:Th</ta>
            <ta e="T52" id="Seg_1181" s="T51">np.h:Th</ta>
            <ta e="T53" id="Seg_1182" s="T52">adv:Time</ta>
            <ta e="T54" id="Seg_1183" s="T53">pro.h:B</ta>
            <ta e="T56" id="Seg_1184" s="T55">np:P</ta>
            <ta e="T57" id="Seg_1185" s="T56">0.1.h:A</ta>
            <ta e="T58" id="Seg_1186" s="T57">adv:Time</ta>
            <ta e="T59" id="Seg_1187" s="T58">pro.h:A</ta>
            <ta e="T61" id="Seg_1188" s="T60">pro.h:E</ta>
            <ta e="T63" id="Seg_1189" s="T62">0.1.h:A</ta>
            <ta e="T65" id="Seg_1190" s="T64">0.2.h:A</ta>
            <ta e="T67" id="Seg_1191" s="T66">0.2.h:A</ta>
            <ta e="T69" id="Seg_1192" s="T67">np.h:E</ta>
            <ta e="T71" id="Seg_1193" s="T70">0.2.h:A</ta>
            <ta e="T73" id="Seg_1194" s="T72">adv:Time</ta>
            <ta e="T74" id="Seg_1195" s="T73">pro.h:A</ta>
            <ta e="T76" id="Seg_1196" s="T75">adv:Time</ta>
            <ta e="T77" id="Seg_1197" s="T76">np:A</ta>
            <ta e="T79" id="Seg_1198" s="T78">pro.h:Th</ta>
            <ta e="T81" id="Seg_1199" s="T80">pro:Th</ta>
            <ta e="T84" id="Seg_1200" s="T83">np:Th</ta>
            <ta e="T87" id="Seg_1201" s="T86">np.h:Th</ta>
            <ta e="T90" id="Seg_1202" s="T89">pro:Th</ta>
            <ta e="T93" id="Seg_1203" s="T92">0.3.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_1204" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_1205" s="T2">v:pred</ta>
            <ta e="T5" id="Seg_1206" s="T4">v:pred 0.3.h:S</ta>
            <ta e="T7" id="Seg_1207" s="T6">v:pred</ta>
            <ta e="T8" id="Seg_1208" s="T7">np:S</ta>
            <ta e="T9" id="Seg_1209" s="T8">pro.h:S</ta>
            <ta e="T10" id="Seg_1210" s="T9">v:pred</ta>
            <ta e="T11" id="Seg_1211" s="T10">v:pred 0.1.h:S</ta>
            <ta e="T13" id="Seg_1212" s="T12">np:O</ta>
            <ta e="T15" id="Seg_1213" s="T14">pro:O</ta>
            <ta e="T16" id="Seg_1214" s="T15">conv:pred</ta>
            <ta e="T17" id="Seg_1215" s="T16">v:pred 0.1.h:S</ta>
            <ta e="T18" id="Seg_1216" s="T17">v:pred 0.1.h:S</ta>
            <ta e="T19" id="Seg_1217" s="T18">np:O</ta>
            <ta e="T20" id="Seg_1218" s="T19">conv:pred</ta>
            <ta e="T21" id="Seg_1219" s="T20">v:pred 0.1.h:S</ta>
            <ta e="T22" id="Seg_1220" s="T21">np:O</ta>
            <ta e="T24" id="Seg_1221" s="T23">v:pred 0.3:S</ta>
            <ta e="T27" id="Seg_1222" s="T26">np:O</ta>
            <ta e="T30" id="Seg_1223" s="T29">np:O</ta>
            <ta e="T32" id="Seg_1224" s="T31">v:pred 0.3:S</ta>
            <ta e="T34" id="Seg_1225" s="T33">pro:O</ta>
            <ta e="T36" id="Seg_1226" s="T35">v:pred 0.1.h:S</ta>
            <ta e="T38" id="Seg_1227" s="T37">np:O</ta>
            <ta e="T39" id="Seg_1228" s="T38">v:pred 0.1.h:S</ta>
            <ta e="T41" id="Seg_1229" s="T40">np.h:O</ta>
            <ta e="T42" id="Seg_1230" s="T41">v:pred 0.1.h:S</ta>
            <ta e="T44" id="Seg_1231" s="T43">np.h:S</ta>
            <ta e="T45" id="Seg_1232" s="T44">v:pred</ta>
            <ta e="T48" id="Seg_1233" s="T47">np.h:O</ta>
            <ta e="T49" id="Seg_1234" s="T48">np.h:S</ta>
            <ta e="T50" id="Seg_1235" s="T49">n:pred</ta>
            <ta e="T51" id="Seg_1236" s="T50">np.h:S</ta>
            <ta e="T52" id="Seg_1237" s="T51">n:pred</ta>
            <ta e="T56" id="Seg_1238" s="T55">np:O</ta>
            <ta e="T57" id="Seg_1239" s="T56">v:pred 0.1.h:S</ta>
            <ta e="T59" id="Seg_1240" s="T58">pro.h:S</ta>
            <ta e="T60" id="Seg_1241" s="T59">v:pred</ta>
            <ta e="T61" id="Seg_1242" s="T60">pro.h:S</ta>
            <ta e="T62" id="Seg_1243" s="T61">v:pred</ta>
            <ta e="T63" id="Seg_1244" s="T62">v:pred 0.1.h:S</ta>
            <ta e="T65" id="Seg_1245" s="T64">v:pred 0.2.h:S</ta>
            <ta e="T67" id="Seg_1246" s="T66">v:pred 0.2.h:S</ta>
            <ta e="T69" id="Seg_1247" s="T67">np.h:O</ta>
            <ta e="T71" id="Seg_1248" s="T70">v:pred 0.2.h:S</ta>
            <ta e="T74" id="Seg_1249" s="T73">pro.h:S</ta>
            <ta e="T75" id="Seg_1250" s="T74">v:pred</ta>
            <ta e="T77" id="Seg_1251" s="T76">np:S</ta>
            <ta e="T78" id="Seg_1252" s="T77">v:pred</ta>
            <ta e="T79" id="Seg_1253" s="T78">pro.h:S</ta>
            <ta e="T80" id="Seg_1254" s="T79">v:pred</ta>
            <ta e="T81" id="Seg_1255" s="T80">pro:S</ta>
            <ta e="T82" id="Seg_1256" s="T81">v:pred</ta>
            <ta e="T84" id="Seg_1257" s="T83">np:S</ta>
            <ta e="T85" id="Seg_1258" s="T84">v:pred</ta>
            <ta e="T87" id="Seg_1259" s="T86">np.h:S</ta>
            <ta e="T88" id="Seg_1260" s="T87">v:pred</ta>
            <ta e="T90" id="Seg_1261" s="T89">pro:S</ta>
            <ta e="T91" id="Seg_1262" s="T90">v:pred</ta>
            <ta e="T93" id="Seg_1263" s="T92">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T8" id="Seg_1264" s="T7">RUS:core</ta>
            <ta e="T13" id="Seg_1265" s="T12">RUS:core</ta>
            <ta e="T14" id="Seg_1266" s="T13">RUS:gram</ta>
            <ta e="T16" id="Seg_1267" s="T15">TURK:cult</ta>
            <ta e="T18" id="Seg_1268" s="T17">TURK:cult</ta>
            <ta e="T20" id="Seg_1269" s="T19">TURK:cult</ta>
            <ta e="T31" id="Seg_1270" s="T30">RUS:mod</ta>
            <ta e="T35" id="Seg_1271" s="T34">TURK:disc</ta>
            <ta e="T37" id="Seg_1272" s="T36">RUS:gram</ta>
            <ta e="T39" id="Seg_1273" s="T38">TURK:cult</ta>
            <ta e="T77" id="Seg_1274" s="T76">RUS:core</ta>
            <ta e="T81" id="Seg_1275" s="T80">TURK:gram(INDEF)</ta>
            <ta e="T83" id="Seg_1276" s="T82">RUS:gram</ta>
            <ta e="T84" id="Seg_1277" s="T83">TAT:cult</ta>
            <ta e="T86" id="Seg_1278" s="T85">RUS:gram</ta>
            <ta e="T89" id="Seg_1279" s="T88">RUS:gram</ta>
            <ta e="T90" id="Seg_1280" s="T89">TURK:gram(INDEF)</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_1281" s="T0">Один человек пошел в лес.</ta>
            <ta e="T8" id="Seg_1282" s="T4">Видит, там заяц сидит.</ta>
            <ta e="T13" id="Seg_1283" s="T8">Он думает: "Я убью этого зайца.</ta>
            <ta e="T17" id="Seg_1284" s="T13">И продам его.</ta>
            <ta e="T19" id="Seg_1285" s="T17">Продам мясо.</ta>
            <ta e="T27" id="Seg_1286" s="T19">Куплю свинью, она принесет мне десять свиней.</ta>
            <ta e="T32" id="Seg_1287" s="T27">Потом они принесут еще десять свиней.</ta>
            <ta e="T36" id="Seg_1288" s="T32">Потом я их всех зарежу.</ta>
            <ta e="T39" id="Seg_1289" s="T36">И мясо продам.</ta>
            <ta e="T42" id="Seg_1290" s="T39">Потом женюсь.</ta>
            <ta e="T48" id="Seg_1291" s="T42">Эта женщина родит меня двоих сыновей.</ta>
            <ta e="T52" id="Seg_1292" s="T48">Одного Ваньку, другого Ваську.</ta>
            <ta e="T57" id="Seg_1293" s="T52">Я им построю дом.</ta>
            <ta e="T60" id="Seg_1294" s="T57">Они будут работать.</ta>
            <ta e="T63" id="Seg_1295" s="T60">Я буду сидеть и (распоряжаться?):</ta>
            <ta e="T72" id="Seg_1296" s="T63">"Сильно не работай, людей не бей".</ta>
            <ta e="T75" id="Seg_1297" s="T72">Тогда он закричал.</ta>
            <ta e="T78" id="Seg_1298" s="T75">Заяц убежал.</ta>
            <ta e="T82" id="Seg_1299" s="T78">Он остался, у него ничего нет.</ta>
            <ta e="T91" id="Seg_1300" s="T82">И дома нет, и жены нет, и ничего нет.</ta>
            <ta e="T93" id="Seg_1301" s="T91">Остался один.</ta>
            <ta e="T94" id="Seg_1302" s="T93">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_1303" s="T0">One man went to the taiga.</ta>
            <ta e="T8" id="Seg_1304" s="T4">He sees that there is a hare sitting.</ta>
            <ta e="T13" id="Seg_1305" s="T8">He thinks: "I will kill this hare!</ta>
            <ta e="T17" id="Seg_1306" s="T13">And I will sell it.</ta>
            <ta e="T19" id="Seg_1307" s="T17">I will sell its meat.</ta>
            <ta e="T27" id="Seg_1308" s="T19">I will buy a pig, the pig will give me ten pigs.</ta>
            <ta e="T32" id="Seg_1309" s="T27">Then they will give birth to ten more pigs.</ta>
            <ta e="T36" id="Seg_1310" s="T32">Then I will kill them all.</ta>
            <ta e="T39" id="Seg_1311" s="T36">And I will sell the meat.</ta>
            <ta e="T42" id="Seg_1312" s="T39">Then I will take a wife.</ta>
            <ta e="T48" id="Seg_1313" s="T42">The wife will bring me two children.</ta>
            <ta e="T52" id="Seg_1314" s="T48">One is Vanka, one is Vaska.</ta>
            <ta e="T57" id="Seg_1315" s="T52">Then I will build a house for them</ta>
            <ta e="T60" id="Seg_1316" s="T57">Then they will work.</ta>
            <ta e="T63" id="Seg_1317" s="T60">I will sit and give (orders?):</ta>
            <ta e="T72" id="Seg_1318" s="T63">Do not work hard, do not beat people [= labourers]!"</ta>
            <ta e="T75" id="Seg_1319" s="T72">Then he shouted.</ta>
            <ta e="T78" id="Seg_1320" s="T75">Then the hare ran away.</ta>
            <ta e="T82" id="Seg_1321" s="T78">He stayed alone, he had nothing.</ta>
            <ta e="T91" id="Seg_1322" s="T82">And no house and no wife and no one.</ta>
            <ta e="T93" id="Seg_1323" s="T91">He stayed alone.</ta>
            <ta e="T94" id="Seg_1324" s="T93">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_1325" s="T0">Ein Mann ging in die Taiga.</ta>
            <ta e="T8" id="Seg_1326" s="T4">Er sieht, dass dort ein Hase sitzt.</ta>
            <ta e="T13" id="Seg_1327" s="T8">Er denkt: "Ich werde diesen Hasen töten!</ta>
            <ta e="T17" id="Seg_1328" s="T13">Und werde ihn verkaufen.</ta>
            <ta e="T19" id="Seg_1329" s="T17">Ich werde sein Fleisch verkaufen.</ta>
            <ta e="T27" id="Seg_1330" s="T19">Ich werde ein Schwein kaufen, das Schwein wird mir zehn Schweine geben.</ta>
            <ta e="T32" id="Seg_1331" s="T27">Dann werden sie noch zehn Schweine mehr gebären.</ta>
            <ta e="T36" id="Seg_1332" s="T32">Dann werde ich sie alle töten.</ta>
            <ta e="T39" id="Seg_1333" s="T36">Und werde das Fleisch verkaufen.</ta>
            <ta e="T42" id="Seg_1334" s="T39">Dann werde ich eine Frau nehmen [=heiraten].</ta>
            <ta e="T48" id="Seg_1335" s="T42">Die Frau wird mir zwei Kinder bringen.</ta>
            <ta e="T52" id="Seg_1336" s="T48">Einer ist Vanka, einer ist Vaska.</ta>
            <ta e="T57" id="Seg_1337" s="T52">Dann werde ich ein Haus für sie bauen.</ta>
            <ta e="T60" id="Seg_1338" s="T57">Dann werden sie arbeiten.</ta>
            <ta e="T63" id="Seg_1339" s="T60">Ich werde sitzen und Befehle (erteilen?):</ta>
            <ta e="T72" id="Seg_1340" s="T63">"Arbeitet nicht hart, schlagt keine Leute [= Arbeiter]!"</ta>
            <ta e="T75" id="Seg_1341" s="T72">Dann schrie er.</ta>
            <ta e="T78" id="Seg_1342" s="T75">Dann rannte der Hase davon.</ta>
            <ta e="T82" id="Seg_1343" s="T78">Er blieb alleine, er hatte nichts.</ta>
            <ta e="T91" id="Seg_1344" s="T82">Weder ein Haus, noch eine Frau, noch irgendetwas.</ta>
            <ta e="T93" id="Seg_1345" s="T91">Er blieb alleine.</ta>
            <ta e="T94" id="Seg_1346" s="T93">Genau!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T4" id="Seg_1347" s="T0">[GVY:] Some versions of the tale don't mention the names of the man's prospective sons, but some do, e.g. http://vseskazki.su/narodnye-skazki/russkie-narodnie-skazki/mujik-i-zayac.html</ta>
            <ta e="T17" id="Seg_1348" s="T13">[GVY:] mistake: "sell" instead of "buy", see the following sentence.</ta>
            <ta e="T27" id="Seg_1349" s="T19">[GVY:] Šolka or šolkʼa - an unknown word or a slip of the tongue? </ta>
            <ta e="T63" id="Seg_1350" s="T60">[GVY:] The last form is unclear.</ta>
            <ta e="T75" id="Seg_1351" s="T72">[GVY:] He shouted aloud what he was thinking.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
