<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDEC7936E8-B620-1E91-FD59-27256A6FD19B">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_FoxAndBear_flk.wav" />
         <referenced-file url="PKZ_196X_FoxAndBear_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_FoxAndBear_flk\PKZ_196X_FoxAndBear_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">165</ud-information>
            <ud-information attribute-name="# HIAT:w">101</ud-information>
            <ud-information attribute-name="# e">102</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">1</ud-information>
            <ud-information attribute-name="# HIAT:u">27</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T738" time="0.092" type="appl" />
         <tli id="T739" time="0.825" type="appl" />
         <tli id="T740" time="1.559" type="appl" />
         <tli id="T741" time="2.292" type="appl" />
         <tli id="T742" time="3.3997359689156017" />
         <tli id="T743" time="4.115" type="appl" />
         <tli id="T744" time="4.887" type="appl" />
         <tli id="T745" time="5.658" type="appl" />
         <tli id="T746" time="6.223" type="appl" />
         <tli id="T747" time="6.788" type="appl" />
         <tli id="T748" time="7.352" type="appl" />
         <tli id="T749" time="7.917" type="appl" />
         <tli id="T750" time="8.188" type="appl" />
         <tli id="T751" time="8.46" type="appl" />
         <tli id="T752" time="9.365939286914548" />
         <tli id="T753" time="10.238" type="appl" />
         <tli id="T754" time="11.048" type="appl" />
         <tli id="T755" time="11.859" type="appl" />
         <tli id="T756" time="12.67" type="appl" />
         <tli id="T757" time="13.577" type="appl" />
         <tli id="T758" time="15.232150370533628" />
         <tli id="T759" time="15.976" type="appl" />
         <tli id="T760" time="16.728" type="appl" />
         <tli id="T761" time="17.48" type="appl" />
         <tli id="T762" time="18.232" type="appl" />
         <tli id="T763" time="19.08" type="appl" />
         <tli id="T764" time="19.927" type="appl" />
         <tli id="T765" time="20.775" type="appl" />
         <tli id="T766" time="21.622" type="appl" />
         <tli id="T767" time="22.47" type="appl" />
         <tli id="T768" time="23.329" type="appl" />
         <tli id="T769" time="24.188" type="appl" />
         <tli id="T770" time="25.047" type="appl" />
         <tli id="T771" time="28.257805435751443" />
         <tli id="T772" time="28.58" type="appl" />
         <tli id="T773" time="29.284" type="appl" />
         <tli id="T774" time="29.988" type="appl" />
         <tli id="T775" time="31.78" type="appl" />
         <tli id="T776" time="33.306" type="appl" />
         <tli id="T777" time="34.173" type="appl" />
         <tli id="T778" time="35.04" type="appl" />
         <tli id="T779" time="35.908" type="appl" />
         <tli id="T780" time="36.775" type="appl" />
         <tli id="T781" time="37.386" type="appl" />
         <tli id="T782" time="37.996" type="appl" />
         <tli id="T783" time="38.606" type="appl" />
         <tli id="T784" time="39.217" type="appl" />
         <tli id="T785" time="39.828" type="appl" />
         <tli id="T786" time="40.438" type="appl" />
         <tli id="T787" time="41.048" type="appl" />
         <tli id="T788" time="41.810086268703245" />
         <tli id="T789" time="42.384" type="appl" />
         <tli id="T790" time="43.11" type="appl" />
         <tli id="T791" time="44.04991231881234" />
         <tli id="T792" time="44.409" type="appl" />
         <tli id="T793" time="44.956" type="appl" />
         <tli id="T794" time="45.504" type="appl" />
         <tli id="T795" time="46.051" type="appl" />
         <tli id="T796" time="46.598" type="appl" />
         <tli id="T797" time="47.162" type="appl" />
         <tli id="T798" time="47.726" type="appl" />
         <tli id="T799" time="48.567" type="appl" />
         <tli id="T800" time="49.407" type="appl" />
         <tli id="T801" time="50.248" type="appl" />
         <tli id="T802" time="50.658" type="appl" />
         <tli id="T803" time="51.068" type="appl" />
         <tli id="T804" time="51.574" type="appl" />
         <tli id="T805" time="52.079" type="appl" />
         <tli id="T806" time="52.93588888070351" />
         <tli id="T807" time="54.245" type="appl" />
         <tli id="T808" time="55.511" type="appl" />
         <tli id="T809" time="56.778" type="appl" />
         <tli id="T810" time="57.49" type="appl" />
         <tli id="T811" time="58.201" type="appl" />
         <tli id="T812" time="58.913" type="appl" />
         <tli id="T813" time="59.454" type="appl" />
         <tli id="T814" time="59.996" type="appl" />
         <tli id="T815" time="60.538" type="appl" />
         <tli id="T816" time="61.17524899360485" />
         <tli id="T817" time="62.163" type="appl" />
         <tli id="T818" time="63.247" type="appl" />
         <tli id="T819" time="64.059" type="appl" />
         <tli id="T820" time="64.776" type="appl" />
         <tli id="T821" time="65.493" type="appl" />
         <tli id="T822" time="66.21" type="appl" />
         <tli id="T823" time="66.927" type="appl" />
         <tli id="T824" time="68.40802061374883" />
         <tli id="T0" time="68.86622222222222" type="intp" />
         <tli id="T825" time="69.844" type="appl" />
         <tli id="T826" time="71.075" type="appl" />
         <tli id="T827" time="72.307" type="appl" />
         <tli id="T828" time="73.539" type="appl" />
         <tli id="T829" time="74.771" type="appl" />
         <tli id="T830" time="76.002" type="appl" />
         <tli id="T831" time="78.00727511421643" />
         <tli id="T832" time="78.902" type="appl" />
         <tli id="T833" time="79.72" type="appl" />
         <tli id="T834" time="80.537" type="appl" />
         <tli id="T835" time="81.354" type="appl" />
         <tli id="T836" time="82.172" type="appl" />
         <tli id="T837" time="83.0935467696725" />
         <tli id="T838" time="83.902" type="appl" />
         <tli id="T839" time="84.687" type="appl" />
         <tli id="T840" time="85.54" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T840" id="Seg_0" n="sc" s="T738">
               <ts e="T742" id="Seg_2" n="HIAT:u" s="T738">
                  <ts e="T739" id="Seg_4" n="HIAT:w" s="T738">Amnobiʔi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_7" n="HIAT:w" s="T739">lʼisʼitsa</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_10" n="HIAT:w" s="T740">i</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_13" n="HIAT:w" s="T741">urgaːba</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T749" id="Seg_17" n="HIAT:u" s="T742">
                  <ts e="T743" id="Seg_19" n="HIAT:w" s="T742">Dĭgəttə</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_22" n="HIAT:w" s="T743">lʼisʼitsa</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_25" n="HIAT:w" s="T744">măndə:</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_27" n="HIAT:ip">"</nts>
                  <ts e="T746" id="Seg_29" n="HIAT:w" s="T745">Măn</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_32" n="HIAT:w" s="T746">kallam</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_35" n="HIAT:w" s="T747">kola</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_38" n="HIAT:w" s="T748">dʼabəsʼtə</ts>
                  <nts id="Seg_39" n="HIAT:ip">"</nts>
                  <nts id="Seg_40" n="HIAT:ip">.</nts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T752" id="Seg_43" n="HIAT:u" s="T749">
                  <nts id="Seg_44" n="HIAT:ip">"</nts>
                  <ts e="T750" id="Seg_46" n="HIAT:w" s="T749">A</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_49" n="HIAT:w" s="T750">kăde</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_51" n="HIAT:ip">(</nts>
                  <ts e="T752" id="Seg_53" n="HIAT:w" s="T751">tăn</ts>
                  <nts id="Seg_54" n="HIAT:ip">)</nts>
                  <nts id="Seg_55" n="HIAT:ip">?</nts>
                  <nts id="Seg_56" n="HIAT:ip">"</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T756" id="Seg_59" n="HIAT:u" s="T752">
                  <nts id="Seg_60" n="HIAT:ip">"</nts>
                  <ts e="T753" id="Seg_62" n="HIAT:w" s="T752">Da</ts>
                  <nts id="Seg_63" n="HIAT:ip">,</nts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_66" n="HIAT:w" s="T753">xvosgəndə</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_69" n="HIAT:w" s="T754">iʔgö</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_72" n="HIAT:w" s="T755">dʼabəlam</ts>
                  <nts id="Seg_73" n="HIAT:ip">"</nts>
                  <nts id="Seg_74" n="HIAT:ip">.</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T758" id="Seg_77" n="HIAT:u" s="T756">
                  <ts e="T757" id="Seg_79" n="HIAT:w" s="T756">Dĭgəttə</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_82" n="HIAT:w" s="T757">nuʔməluʔpi</ts>
                  <nts id="Seg_83" n="HIAT:ip">.</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T762" id="Seg_86" n="HIAT:u" s="T758">
                  <ts e="T759" id="Seg_88" n="HIAT:w" s="T758">Dĭn</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_91" n="HIAT:w" s="T759">tibizeŋ</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_94" n="HIAT:w" s="T760">kola</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_97" n="HIAT:w" s="T761">dʼaʔpiʔi</ts>
                  <nts id="Seg_98" n="HIAT:ip">.</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T767" id="Seg_101" n="HIAT:u" s="T762">
                  <ts e="T763" id="Seg_103" n="HIAT:w" s="T762">Dĭ</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_106" n="HIAT:w" s="T763">aʔtʼinə</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_109" n="HIAT:w" s="T764">iʔbəbi</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_112" n="HIAT:w" s="T765">bar</ts>
                  <nts id="Seg_113" n="HIAT:ip">,</nts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_116" n="HIAT:w" s="T766">külaːmbi</ts>
                  <nts id="Seg_117" n="HIAT:ip">.</nts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T771" id="Seg_120" n="HIAT:u" s="T767">
                  <ts e="T768" id="Seg_122" n="HIAT:w" s="T767">Iʔbəlaʔbə</ts>
                  <nts id="Seg_123" n="HIAT:ip">,</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_126" n="HIAT:w" s="T768">tibizeŋ</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_128" n="HIAT:ip">(</nts>
                  <ts e="T770" id="Seg_130" n="HIAT:w" s="T769">i-</ts>
                  <nts id="Seg_131" n="HIAT:ip">)</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T771" id="Seg_134" n="HIAT:w" s="T770">šonəgaʔi</ts>
                  <nts id="Seg_135" n="HIAT:ip">.</nts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T774" id="Seg_138" n="HIAT:u" s="T771">
                  <ts e="T772" id="Seg_140" n="HIAT:w" s="T771">Ibiʔi</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_143" n="HIAT:w" s="T772">i</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_146" n="HIAT:w" s="T773">dĭm</ts>
                  <nts id="Seg_147" n="HIAT:ip">.</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T776" id="Seg_150" n="HIAT:u" s="T774">
                  <ts e="T775" id="Seg_152" n="HIAT:w" s="T774">Embiʔi</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_155" n="HIAT:w" s="T775">kolanə</ts>
                  <nts id="Seg_156" n="HIAT:ip">.</nts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T780" id="Seg_159" n="HIAT:u" s="T776">
                  <nts id="Seg_160" n="HIAT:ip">(</nts>
                  <ts e="T777" id="Seg_162" n="HIAT:w" s="T776">Boskə-</ts>
                  <nts id="Seg_163" n="HIAT:ip">)</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_166" n="HIAT:w" s="T777">Bostə</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_168" n="HIAT:ip">(</nts>
                  <ts e="T779" id="Seg_170" n="HIAT:w" s="T778">ka-</ts>
                  <nts id="Seg_171" n="HIAT:ip">)</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_174" n="HIAT:w" s="T779">kallaʔbəʔjə</ts>
                  <nts id="Seg_175" n="HIAT:ip">.</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T788" id="Seg_178" n="HIAT:u" s="T780">
                  <ts e="T781" id="Seg_180" n="HIAT:w" s="T780">Dĭ</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_183" n="HIAT:w" s="T781">bar</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_186" n="HIAT:w" s="T782">băra</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_189" n="HIAT:w" s="T783">ambi</ts>
                  <nts id="Seg_190" n="HIAT:ip">,</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_193" n="HIAT:w" s="T784">kolabə</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_196" n="HIAT:w" s="T785">bar</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_199" n="HIAT:w" s="T786">baruʔpi</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_202" n="HIAT:w" s="T787">aʔtʼinə</ts>
                  <nts id="Seg_203" n="HIAT:ip">.</nts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T791" id="Seg_206" n="HIAT:u" s="T788">
                  <ts e="T789" id="Seg_208" n="HIAT:w" s="T788">I</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_211" n="HIAT:w" s="T789">bostə</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_214" n="HIAT:w" s="T790">nuʔməluʔpi</ts>
                  <nts id="Seg_215" n="HIAT:ip">.</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T796" id="Seg_218" n="HIAT:u" s="T791">
                  <ts e="T792" id="Seg_220" n="HIAT:w" s="T791">Dĭgəttə</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T793" id="Seg_223" n="HIAT:w" s="T792">šobi</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_226" n="HIAT:w" s="T793">maːndə</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_229" n="HIAT:w" s="T794">da</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_231" n="HIAT:ip">(</nts>
                  <nts id="Seg_232" n="HIAT:ip">(</nts>
                  <ats e="T796" id="Seg_233" n="HIAT:non-pho" s="T795">…</ats>
                  <nts id="Seg_234" n="HIAT:ip">)</nts>
                  <nts id="Seg_235" n="HIAT:ip">)</nts>
                  <nts id="Seg_236" n="HIAT:ip">.</nts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T798" id="Seg_239" n="HIAT:u" s="T796">
                  <ts e="T797" id="Seg_241" n="HIAT:w" s="T796">Kola</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_244" n="HIAT:w" s="T797">amnia</ts>
                  <nts id="Seg_245" n="HIAT:ip">.</nts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T801" id="Seg_248" n="HIAT:u" s="T798">
                  <ts e="T799" id="Seg_250" n="HIAT:w" s="T798">A</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_253" n="HIAT:w" s="T799">urgaːba</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_256" n="HIAT:w" s="T800">kuliot</ts>
                  <nts id="Seg_257" n="HIAT:ip">.</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T803" id="Seg_260" n="HIAT:u" s="T801">
                  <ts e="T802" id="Seg_262" n="HIAT:w" s="T801">Kola</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_265" n="HIAT:w" s="T802">amnia</ts>
                  <nts id="Seg_266" n="HIAT:ip">.</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T806" id="Seg_269" n="HIAT:u" s="T803">
                  <nts id="Seg_270" n="HIAT:ip">"</nts>
                  <ts e="T804" id="Seg_272" n="HIAT:w" s="T803">Kallam</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_274" n="HIAT:ip">(</nts>
                  <ts e="T805" id="Seg_276" n="HIAT:w" s="T804">i</ts>
                  <nts id="Seg_277" n="HIAT:ip">)</nts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_280" n="HIAT:w" s="T805">măn</ts>
                  <nts id="Seg_281" n="HIAT:ip">!</nts>
                  <nts id="Seg_282" n="HIAT:ip">"</nts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T809" id="Seg_285" n="HIAT:u" s="T806">
                  <ts e="T807" id="Seg_287" n="HIAT:w" s="T806">Xvostə</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_290" n="HIAT:w" s="T807">bünə</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_293" n="HIAT:w" s="T808">saʔməluʔpi</ts>
                  <nts id="Seg_294" n="HIAT:ip">.</nts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T812" id="Seg_297" n="HIAT:u" s="T809">
                  <ts e="T810" id="Seg_299" n="HIAT:w" s="T809">Dĭgəttə</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_302" n="HIAT:w" s="T810">amnobi</ts>
                  <nts id="Seg_303" n="HIAT:ip">,</nts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_306" n="HIAT:w" s="T811">amnobi</ts>
                  <nts id="Seg_307" n="HIAT:ip">.</nts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T816" id="Seg_310" n="HIAT:u" s="T812">
                  <ts e="T813" id="Seg_312" n="HIAT:w" s="T812">A</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T814" id="Seg_315" n="HIAT:w" s="T813">ugaːndə</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_318" n="HIAT:w" s="T814">šĭšəge</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_321" n="HIAT:w" s="T815">ibi</ts>
                  <nts id="Seg_322" n="HIAT:ip">.</nts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T818" id="Seg_325" n="HIAT:u" s="T816">
                  <ts e="T817" id="Seg_327" n="HIAT:w" s="T816">Xvostə</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_330" n="HIAT:w" s="T817">kănnaːmbi</ts>
                  <nts id="Seg_331" n="HIAT:ip">.</nts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T824" id="Seg_334" n="HIAT:u" s="T818">
                  <ts e="T819" id="Seg_336" n="HIAT:w" s="T818">Dĭ</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_338" n="HIAT:ip">(</nts>
                  <ts e="T820" id="Seg_340" n="HIAT:w" s="T819">dav-</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T821" id="Seg_343" n="HIAT:w" s="T820">d-</ts>
                  <nts id="Seg_344" n="HIAT:ip">)</nts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_347" n="HIAT:w" s="T821">măndə:</ts>
                  <nts id="Seg_348" n="HIAT:ip">"</nts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_351" n="HIAT:w" s="T822">Onnaka</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T824" id="Seg_354" n="HIAT:w" s="T823">iʔgö</ts>
                  <nts id="Seg_355" n="HIAT:ip">"</nts>
                  <nts id="Seg_356" n="HIAT:ip">.</nts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T831" id="Seg_359" n="HIAT:u" s="T824">
                  <ts e="T0" id="Seg_361" n="HIAT:w" s="T824">Stal</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_364" n="HIAT:w" s="T0">dĭm</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_367" n="HIAT:w" s="T825">nʼiʔtəsʼtə</ts>
                  <nts id="Seg_368" n="HIAT:ip">,</nts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_371" n="HIAT:w" s="T826">i</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T828" id="Seg_374" n="HIAT:w" s="T827">xvostə</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_376" n="HIAT:ip">(</nts>
                  <ts e="T829" id="Seg_378" n="HIAT:w" s="T828">saj</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T831" id="Seg_381" n="HIAT:w" s="T829">nožuʔpi</ts>
                  <nts id="Seg_382" n="HIAT:ip">)</nts>
                  <nts id="Seg_383" n="HIAT:ip">.</nts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T837" id="Seg_386" n="HIAT:u" s="T831">
                  <ts e="T832" id="Seg_388" n="HIAT:w" s="T831">I</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_391" n="HIAT:w" s="T832">dĭgəttə</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_393" n="HIAT:ip">(</nts>
                  <ts e="T834" id="Seg_395" n="HIAT:w" s="T833">na-</ts>
                  <nts id="Seg_396" n="HIAT:ip">)</nts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_399" n="HIAT:w" s="T834">naga</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_402" n="HIAT:w" s="T835">dĭn</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T837" id="Seg_405" n="HIAT:w" s="T836">xvostə</ts>
                  <nts id="Seg_406" n="HIAT:ip">.</nts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T839" id="Seg_409" n="HIAT:u" s="T837">
                  <ts e="T838" id="Seg_411" n="HIAT:w" s="T837">Bar</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_414" n="HIAT:w" s="T838">sajnʼeʔpi</ts>
                  <nts id="Seg_415" n="HIAT:ip">.</nts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T840" id="Seg_418" n="HIAT:u" s="T839">
                  <ts e="T840" id="Seg_420" n="HIAT:w" s="T839">Kabarləj</ts>
                  <nts id="Seg_421" n="HIAT:ip">.</nts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T840" id="Seg_423" n="sc" s="T738">
               <ts e="T739" id="Seg_425" n="e" s="T738">Amnobiʔi </ts>
               <ts e="T740" id="Seg_427" n="e" s="T739">lʼisʼitsa </ts>
               <ts e="T741" id="Seg_429" n="e" s="T740">i </ts>
               <ts e="T742" id="Seg_431" n="e" s="T741">urgaːba. </ts>
               <ts e="T743" id="Seg_433" n="e" s="T742">Dĭgəttə </ts>
               <ts e="T744" id="Seg_435" n="e" s="T743">lʼisʼitsa </ts>
               <ts e="T745" id="Seg_437" n="e" s="T744">măndə: </ts>
               <ts e="T746" id="Seg_439" n="e" s="T745">"Măn </ts>
               <ts e="T747" id="Seg_441" n="e" s="T746">kallam </ts>
               <ts e="T748" id="Seg_443" n="e" s="T747">kola </ts>
               <ts e="T749" id="Seg_445" n="e" s="T748">dʼabəsʼtə". </ts>
               <ts e="T750" id="Seg_447" n="e" s="T749">"A </ts>
               <ts e="T751" id="Seg_449" n="e" s="T750">kăde </ts>
               <ts e="T752" id="Seg_451" n="e" s="T751">(tăn)?" </ts>
               <ts e="T753" id="Seg_453" n="e" s="T752">"Da, </ts>
               <ts e="T754" id="Seg_455" n="e" s="T753">xvosgəndə </ts>
               <ts e="T755" id="Seg_457" n="e" s="T754">iʔgö </ts>
               <ts e="T756" id="Seg_459" n="e" s="T755">dʼabəlam". </ts>
               <ts e="T757" id="Seg_461" n="e" s="T756">Dĭgəttə </ts>
               <ts e="T758" id="Seg_463" n="e" s="T757">nuʔməluʔpi. </ts>
               <ts e="T759" id="Seg_465" n="e" s="T758">Dĭn </ts>
               <ts e="T760" id="Seg_467" n="e" s="T759">tibizeŋ </ts>
               <ts e="T761" id="Seg_469" n="e" s="T760">kola </ts>
               <ts e="T762" id="Seg_471" n="e" s="T761">dʼaʔpiʔi. </ts>
               <ts e="T763" id="Seg_473" n="e" s="T762">Dĭ </ts>
               <ts e="T764" id="Seg_475" n="e" s="T763">aʔtʼinə </ts>
               <ts e="T765" id="Seg_477" n="e" s="T764">iʔbəbi </ts>
               <ts e="T766" id="Seg_479" n="e" s="T765">bar, </ts>
               <ts e="T767" id="Seg_481" n="e" s="T766">külaːmbi. </ts>
               <ts e="T768" id="Seg_483" n="e" s="T767">Iʔbəlaʔbə, </ts>
               <ts e="T769" id="Seg_485" n="e" s="T768">tibizeŋ </ts>
               <ts e="T770" id="Seg_487" n="e" s="T769">(i-) </ts>
               <ts e="T771" id="Seg_489" n="e" s="T770">šonəgaʔi. </ts>
               <ts e="T772" id="Seg_491" n="e" s="T771">Ibiʔi </ts>
               <ts e="T773" id="Seg_493" n="e" s="T772">i </ts>
               <ts e="T774" id="Seg_495" n="e" s="T773">dĭm. </ts>
               <ts e="T775" id="Seg_497" n="e" s="T774">Embiʔi </ts>
               <ts e="T776" id="Seg_499" n="e" s="T775">kolanə. </ts>
               <ts e="T777" id="Seg_501" n="e" s="T776">(Boskə-) </ts>
               <ts e="T778" id="Seg_503" n="e" s="T777">Bostə </ts>
               <ts e="T779" id="Seg_505" n="e" s="T778">(ka-) </ts>
               <ts e="T780" id="Seg_507" n="e" s="T779">kallaʔbəʔjə. </ts>
               <ts e="T781" id="Seg_509" n="e" s="T780">Dĭ </ts>
               <ts e="T782" id="Seg_511" n="e" s="T781">bar </ts>
               <ts e="T783" id="Seg_513" n="e" s="T782">băra </ts>
               <ts e="T784" id="Seg_515" n="e" s="T783">ambi, </ts>
               <ts e="T785" id="Seg_517" n="e" s="T784">kolabə </ts>
               <ts e="T786" id="Seg_519" n="e" s="T785">bar </ts>
               <ts e="T787" id="Seg_521" n="e" s="T786">baruʔpi </ts>
               <ts e="T788" id="Seg_523" n="e" s="T787">aʔtʼinə. </ts>
               <ts e="T789" id="Seg_525" n="e" s="T788">I </ts>
               <ts e="T790" id="Seg_527" n="e" s="T789">bostə </ts>
               <ts e="T791" id="Seg_529" n="e" s="T790">nuʔməluʔpi. </ts>
               <ts e="T792" id="Seg_531" n="e" s="T791">Dĭgəttə </ts>
               <ts e="T793" id="Seg_533" n="e" s="T792">šobi </ts>
               <ts e="T794" id="Seg_535" n="e" s="T793">maːndə </ts>
               <ts e="T795" id="Seg_537" n="e" s="T794">da </ts>
               <ts e="T796" id="Seg_539" n="e" s="T795">((…)). </ts>
               <ts e="T797" id="Seg_541" n="e" s="T796">Kola </ts>
               <ts e="T798" id="Seg_543" n="e" s="T797">amnia. </ts>
               <ts e="T799" id="Seg_545" n="e" s="T798">A </ts>
               <ts e="T800" id="Seg_547" n="e" s="T799">urgaːba </ts>
               <ts e="T801" id="Seg_549" n="e" s="T800">kuliot. </ts>
               <ts e="T802" id="Seg_551" n="e" s="T801">Kola </ts>
               <ts e="T803" id="Seg_553" n="e" s="T802">amnia. </ts>
               <ts e="T804" id="Seg_555" n="e" s="T803">"Kallam </ts>
               <ts e="T805" id="Seg_557" n="e" s="T804">(i) </ts>
               <ts e="T806" id="Seg_559" n="e" s="T805">măn!" </ts>
               <ts e="T807" id="Seg_561" n="e" s="T806">Xvostə </ts>
               <ts e="T808" id="Seg_563" n="e" s="T807">bünə </ts>
               <ts e="T809" id="Seg_565" n="e" s="T808">saʔməluʔpi. </ts>
               <ts e="T810" id="Seg_567" n="e" s="T809">Dĭgəttə </ts>
               <ts e="T811" id="Seg_569" n="e" s="T810">amnobi, </ts>
               <ts e="T812" id="Seg_571" n="e" s="T811">amnobi. </ts>
               <ts e="T813" id="Seg_573" n="e" s="T812">A </ts>
               <ts e="T814" id="Seg_575" n="e" s="T813">ugaːndə </ts>
               <ts e="T815" id="Seg_577" n="e" s="T814">šĭšəge </ts>
               <ts e="T816" id="Seg_579" n="e" s="T815">ibi. </ts>
               <ts e="T817" id="Seg_581" n="e" s="T816">Xvostə </ts>
               <ts e="T818" id="Seg_583" n="e" s="T817">kănnaːmbi. </ts>
               <ts e="T819" id="Seg_585" n="e" s="T818">Dĭ </ts>
               <ts e="T820" id="Seg_587" n="e" s="T819">(dav- </ts>
               <ts e="T821" id="Seg_589" n="e" s="T820">d-) </ts>
               <ts e="T822" id="Seg_591" n="e" s="T821">măndə:" </ts>
               <ts e="T823" id="Seg_593" n="e" s="T822">Onnaka </ts>
               <ts e="T824" id="Seg_595" n="e" s="T823">iʔgö". </ts>
               <ts e="T0" id="Seg_597" n="e" s="T824">Stal </ts>
               <ts e="T825" id="Seg_599" n="e" s="T0">dĭm </ts>
               <ts e="T826" id="Seg_601" n="e" s="T825">nʼiʔtəsʼtə, </ts>
               <ts e="T827" id="Seg_603" n="e" s="T826">i </ts>
               <ts e="T828" id="Seg_605" n="e" s="T827">xvostə </ts>
               <ts e="T829" id="Seg_607" n="e" s="T828">(saj </ts>
               <ts e="T831" id="Seg_609" n="e" s="T829">nožuʔpi). </ts>
               <ts e="T832" id="Seg_611" n="e" s="T831">I </ts>
               <ts e="T833" id="Seg_613" n="e" s="T832">dĭgəttə </ts>
               <ts e="T834" id="Seg_615" n="e" s="T833">(na-) </ts>
               <ts e="T835" id="Seg_617" n="e" s="T834">naga </ts>
               <ts e="T836" id="Seg_619" n="e" s="T835">dĭn </ts>
               <ts e="T837" id="Seg_621" n="e" s="T836">xvostə. </ts>
               <ts e="T838" id="Seg_623" n="e" s="T837">Bar </ts>
               <ts e="T839" id="Seg_625" n="e" s="T838">sajnʼeʔpi. </ts>
               <ts e="T840" id="Seg_627" n="e" s="T839">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T742" id="Seg_628" s="T738">PKZ_196X_FoxAndBear_flk.001 (001)</ta>
            <ta e="T749" id="Seg_629" s="T742">PKZ_196X_FoxAndBear_flk.002 (002)</ta>
            <ta e="T752" id="Seg_630" s="T749">PKZ_196X_FoxAndBear_flk.003 (004)</ta>
            <ta e="T756" id="Seg_631" s="T752">PKZ_196X_FoxAndBear_flk.004 (005)</ta>
            <ta e="T758" id="Seg_632" s="T756">PKZ_196X_FoxAndBear_flk.005 (006)</ta>
            <ta e="T762" id="Seg_633" s="T758">PKZ_196X_FoxAndBear_flk.006 (007)</ta>
            <ta e="T767" id="Seg_634" s="T762">PKZ_196X_FoxAndBear_flk.007 (008)</ta>
            <ta e="T771" id="Seg_635" s="T767">PKZ_196X_FoxAndBear_flk.008 (009)</ta>
            <ta e="T774" id="Seg_636" s="T771">PKZ_196X_FoxAndBear_flk.009 (010)</ta>
            <ta e="T776" id="Seg_637" s="T774">PKZ_196X_FoxAndBear_flk.010 (011)</ta>
            <ta e="T780" id="Seg_638" s="T776">PKZ_196X_FoxAndBear_flk.011 (012)</ta>
            <ta e="T788" id="Seg_639" s="T780">PKZ_196X_FoxAndBear_flk.012 (013)</ta>
            <ta e="T791" id="Seg_640" s="T788">PKZ_196X_FoxAndBear_flk.013 (014)</ta>
            <ta e="T796" id="Seg_641" s="T791">PKZ_196X_FoxAndBear_flk.014 (015)</ta>
            <ta e="T798" id="Seg_642" s="T796">PKZ_196X_FoxAndBear_flk.015 (016)</ta>
            <ta e="T801" id="Seg_643" s="T798">PKZ_196X_FoxAndBear_flk.016 (017)</ta>
            <ta e="T803" id="Seg_644" s="T801">PKZ_196X_FoxAndBear_flk.017 (018)</ta>
            <ta e="T806" id="Seg_645" s="T803">PKZ_196X_FoxAndBear_flk.018 (019)</ta>
            <ta e="T809" id="Seg_646" s="T806">PKZ_196X_FoxAndBear_flk.019 (020)</ta>
            <ta e="T812" id="Seg_647" s="T809">PKZ_196X_FoxAndBear_flk.020 (021)</ta>
            <ta e="T816" id="Seg_648" s="T812">PKZ_196X_FoxAndBear_flk.021 (022)</ta>
            <ta e="T818" id="Seg_649" s="T816">PKZ_196X_FoxAndBear_flk.022 (023)</ta>
            <ta e="T824" id="Seg_650" s="T818">PKZ_196X_FoxAndBear_flk.023 (024)</ta>
            <ta e="T831" id="Seg_651" s="T824">PKZ_196X_FoxAndBear_flk.024 (025)</ta>
            <ta e="T837" id="Seg_652" s="T831">PKZ_196X_FoxAndBear_flk.025 (026)</ta>
            <ta e="T839" id="Seg_653" s="T837">PKZ_196X_FoxAndBear_flk.026 (027)</ta>
            <ta e="T840" id="Seg_654" s="T839">PKZ_196X_FoxAndBear_flk.027 (028)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T742" id="Seg_655" s="T738">Amnobiʔi lʼisʼitsa i urgaːba. </ta>
            <ta e="T749" id="Seg_656" s="T742">Dĭgəttə lʼisʼitsa măndə: "Măn kallam kola dʼabəsʼtə". </ta>
            <ta e="T752" id="Seg_657" s="T749">"A kăde (tăn)?" </ta>
            <ta e="T756" id="Seg_658" s="T752">"Da, xvosgəndə iʔgö dʼabəlam". </ta>
            <ta e="T758" id="Seg_659" s="T756">Dĭgəttə nuʔməluʔpi. </ta>
            <ta e="T762" id="Seg_660" s="T758">Dĭn tibizeŋ kola dʼaʔpiʔi. </ta>
            <ta e="T767" id="Seg_661" s="T762">Dĭ aʔtʼinə iʔbəbi bar, külaːmbi. </ta>
            <ta e="T771" id="Seg_662" s="T767">Iʔbəlaʔbə, tibizeŋ (i-) šonəgaʔi. </ta>
            <ta e="T774" id="Seg_663" s="T771">Ibiʔi i dĭm. </ta>
            <ta e="T776" id="Seg_664" s="T774">Embiʔi kolanə. </ta>
            <ta e="T780" id="Seg_665" s="T776">(Boskə-) Bostə (ka-) kallaʔbəʔjə. </ta>
            <ta e="T788" id="Seg_666" s="T780">Dĭ bar băra ambi, kolabə bar baruʔpi aʔtʼinə. </ta>
            <ta e="T791" id="Seg_667" s="T788">I bostə nuʔməluʔpi. </ta>
            <ta e="T796" id="Seg_668" s="T791">Dĭgəttə šobi maːndə da ((…)). </ta>
            <ta e="T798" id="Seg_669" s="T796">Kola amnia. </ta>
            <ta e="T801" id="Seg_670" s="T798">A urgaːba kuliot. </ta>
            <ta e="T803" id="Seg_671" s="T801">Kola amnia. </ta>
            <ta e="T806" id="Seg_672" s="T803">"Kallam (i) măn!" </ta>
            <ta e="T809" id="Seg_673" s="T806">Xvostə bünə saʔməluʔpi. </ta>
            <ta e="T812" id="Seg_674" s="T809">Dĭgəttə amnobi, amnobi. </ta>
            <ta e="T816" id="Seg_675" s="T812">A ugaːndə šĭšəge ibi. </ta>
            <ta e="T818" id="Seg_676" s="T816">Xvostə kănnaːmbi. </ta>
            <ta e="T824" id="Seg_677" s="T818">Dĭ (dav- d-) măndə:" Onnaka iʔgö". </ta>
            <ta e="T831" id="Seg_678" s="T824">Stal dĭm nʼiʔtəsʼtə, i xvostə (saj nožuʔpi). </ta>
            <ta e="T837" id="Seg_679" s="T831">I dĭgəttə (na-) naga dĭn xvostə. </ta>
            <ta e="T839" id="Seg_680" s="T837">Bar sajnʼeʔpi. </ta>
            <ta e="T840" id="Seg_681" s="T839">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T739" id="Seg_682" s="T738">amno-bi-ʔi</ta>
            <ta e="T740" id="Seg_683" s="T739">lʼisʼitsa</ta>
            <ta e="T741" id="Seg_684" s="T740">i</ta>
            <ta e="T742" id="Seg_685" s="T741">urgaːba</ta>
            <ta e="T743" id="Seg_686" s="T742">dĭgəttə</ta>
            <ta e="T744" id="Seg_687" s="T743">lʼisʼitsa</ta>
            <ta e="T745" id="Seg_688" s="T744">măn-də</ta>
            <ta e="T746" id="Seg_689" s="T745">măn</ta>
            <ta e="T747" id="Seg_690" s="T746">kal-la-m</ta>
            <ta e="T748" id="Seg_691" s="T747">kola</ta>
            <ta e="T749" id="Seg_692" s="T748">dʼabə-sʼtə</ta>
            <ta e="T750" id="Seg_693" s="T749">a</ta>
            <ta e="T751" id="Seg_694" s="T750">kăde</ta>
            <ta e="T752" id="Seg_695" s="T751">tăn</ta>
            <ta e="T753" id="Seg_696" s="T752">da</ta>
            <ta e="T754" id="Seg_697" s="T753">xvos-gəndə</ta>
            <ta e="T755" id="Seg_698" s="T754">iʔgö</ta>
            <ta e="T756" id="Seg_699" s="T755">dʼabə-la-m</ta>
            <ta e="T757" id="Seg_700" s="T756">dĭgəttə</ta>
            <ta e="T758" id="Seg_701" s="T757">nuʔmə-luʔ-pi</ta>
            <ta e="T759" id="Seg_702" s="T758">dĭn</ta>
            <ta e="T760" id="Seg_703" s="T759">tibi-zeŋ</ta>
            <ta e="T761" id="Seg_704" s="T760">kola</ta>
            <ta e="T762" id="Seg_705" s="T761">dʼaʔ-pi-ʔi</ta>
            <ta e="T763" id="Seg_706" s="T762">dĭ</ta>
            <ta e="T764" id="Seg_707" s="T763">aʔtʼi-nə</ta>
            <ta e="T765" id="Seg_708" s="T764">iʔbə-bi</ta>
            <ta e="T766" id="Seg_709" s="T765">bar</ta>
            <ta e="T767" id="Seg_710" s="T766">kü-laːm-bi</ta>
            <ta e="T768" id="Seg_711" s="T767">iʔbə-laʔbə</ta>
            <ta e="T769" id="Seg_712" s="T768">tibi-zeŋ</ta>
            <ta e="T771" id="Seg_713" s="T770">šonə-ga-ʔi</ta>
            <ta e="T772" id="Seg_714" s="T771">i-bi-ʔi</ta>
            <ta e="T773" id="Seg_715" s="T772">i</ta>
            <ta e="T774" id="Seg_716" s="T773">dĭ-m</ta>
            <ta e="T775" id="Seg_717" s="T774">em-bi-ʔi</ta>
            <ta e="T776" id="Seg_718" s="T775">kola-nə</ta>
            <ta e="T778" id="Seg_719" s="T777">bos-tə</ta>
            <ta e="T780" id="Seg_720" s="T779">kal-laʔbə-ʔjə</ta>
            <ta e="T781" id="Seg_721" s="T780">dĭ</ta>
            <ta e="T782" id="Seg_722" s="T781">bar</ta>
            <ta e="T783" id="Seg_723" s="T782">băra</ta>
            <ta e="T784" id="Seg_724" s="T783">am-bi</ta>
            <ta e="T785" id="Seg_725" s="T784">kola-bə</ta>
            <ta e="T786" id="Seg_726" s="T785">bar</ta>
            <ta e="T787" id="Seg_727" s="T786">baruʔ-pi</ta>
            <ta e="T788" id="Seg_728" s="T787">aʔtʼi-nə</ta>
            <ta e="T789" id="Seg_729" s="T788">i</ta>
            <ta e="T790" id="Seg_730" s="T789">bos-tə</ta>
            <ta e="T791" id="Seg_731" s="T790">nuʔmə-luʔ-pi</ta>
            <ta e="T792" id="Seg_732" s="T791">dĭgəttə</ta>
            <ta e="T793" id="Seg_733" s="T792">šo-bi</ta>
            <ta e="T794" id="Seg_734" s="T793">ma-ndə</ta>
            <ta e="T795" id="Seg_735" s="T794">da</ta>
            <ta e="T797" id="Seg_736" s="T796">kola</ta>
            <ta e="T798" id="Seg_737" s="T797">am-nia</ta>
            <ta e="T799" id="Seg_738" s="T798">a</ta>
            <ta e="T800" id="Seg_739" s="T799">urgaːba</ta>
            <ta e="T801" id="Seg_740" s="T800">ku-lio-t</ta>
            <ta e="T802" id="Seg_741" s="T801">kola</ta>
            <ta e="T803" id="Seg_742" s="T802">am-nia</ta>
            <ta e="T804" id="Seg_743" s="T803">kal-la-m</ta>
            <ta e="T805" id="Seg_744" s="T804">i</ta>
            <ta e="T806" id="Seg_745" s="T805">măn</ta>
            <ta e="T807" id="Seg_746" s="T806">xvostə</ta>
            <ta e="T808" id="Seg_747" s="T807">bü-nə</ta>
            <ta e="T809" id="Seg_748" s="T808">saʔmə-luʔ-pi</ta>
            <ta e="T810" id="Seg_749" s="T809">dĭgəttə</ta>
            <ta e="T811" id="Seg_750" s="T810">amno-bi</ta>
            <ta e="T812" id="Seg_751" s="T811">amno-bi</ta>
            <ta e="T813" id="Seg_752" s="T812">a</ta>
            <ta e="T814" id="Seg_753" s="T813">ugaːndə</ta>
            <ta e="T815" id="Seg_754" s="T814">šĭšəge</ta>
            <ta e="T816" id="Seg_755" s="T815">i-bi</ta>
            <ta e="T817" id="Seg_756" s="T816">xvostə</ta>
            <ta e="T818" id="Seg_757" s="T817">kăn-naːm-bi</ta>
            <ta e="T819" id="Seg_758" s="T818">dĭ</ta>
            <ta e="T822" id="Seg_759" s="T821">măn-də</ta>
            <ta e="T823" id="Seg_760" s="T822">onnaka</ta>
            <ta e="T824" id="Seg_761" s="T823">iʔgö</ta>
            <ta e="T825" id="Seg_762" s="T0">dĭ-m</ta>
            <ta e="T826" id="Seg_763" s="T825">nʼiʔtə-sʼtə</ta>
            <ta e="T827" id="Seg_764" s="T826">i</ta>
            <ta e="T828" id="Seg_765" s="T827">xvostə</ta>
            <ta e="T829" id="Seg_766" s="T828">saj</ta>
            <ta e="T831" id="Seg_767" s="T829">no-žuʔ-pi</ta>
            <ta e="T832" id="Seg_768" s="T831">i</ta>
            <ta e="T833" id="Seg_769" s="T832">dĭgəttə</ta>
            <ta e="T835" id="Seg_770" s="T834">naga</ta>
            <ta e="T836" id="Seg_771" s="T835">dĭn</ta>
            <ta e="T837" id="Seg_772" s="T836">xvostə</ta>
            <ta e="T838" id="Seg_773" s="T837">bar</ta>
            <ta e="T839" id="Seg_774" s="T838">saj-nʼeʔ-pi</ta>
            <ta e="T840" id="Seg_775" s="T839">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T739" id="Seg_776" s="T738">amno-bi-jəʔ</ta>
            <ta e="T740" id="Seg_777" s="T739">lʼisʼitsa</ta>
            <ta e="T741" id="Seg_778" s="T740">i</ta>
            <ta e="T742" id="Seg_779" s="T741">urgaːba</ta>
            <ta e="T743" id="Seg_780" s="T742">dĭgəttə</ta>
            <ta e="T744" id="Seg_781" s="T743">lʼisʼitsa</ta>
            <ta e="T745" id="Seg_782" s="T744">măn-ntə</ta>
            <ta e="T746" id="Seg_783" s="T745">măn</ta>
            <ta e="T747" id="Seg_784" s="T746">kan-lV-m</ta>
            <ta e="T748" id="Seg_785" s="T747">kola</ta>
            <ta e="T749" id="Seg_786" s="T748">dʼabə-zittə</ta>
            <ta e="T750" id="Seg_787" s="T749">a</ta>
            <ta e="T751" id="Seg_788" s="T750">kădaʔ</ta>
            <ta e="T752" id="Seg_789" s="T751">tăn</ta>
            <ta e="T753" id="Seg_790" s="T752">da</ta>
            <ta e="T754" id="Seg_791" s="T753">xvostə-gəndə</ta>
            <ta e="T755" id="Seg_792" s="T754">iʔgö</ta>
            <ta e="T756" id="Seg_793" s="T755">dʼabə-lV-m</ta>
            <ta e="T757" id="Seg_794" s="T756">dĭgəttə</ta>
            <ta e="T758" id="Seg_795" s="T757">nuʔmə-luʔbdə-bi</ta>
            <ta e="T759" id="Seg_796" s="T758">dĭn</ta>
            <ta e="T760" id="Seg_797" s="T759">tibi-zAŋ</ta>
            <ta e="T761" id="Seg_798" s="T760">kola</ta>
            <ta e="T762" id="Seg_799" s="T761">dʼabə-bi-jəʔ</ta>
            <ta e="T763" id="Seg_800" s="T762">dĭ</ta>
            <ta e="T764" id="Seg_801" s="T763">aʔtʼi-Tə</ta>
            <ta e="T765" id="Seg_802" s="T764">iʔbö-bi</ta>
            <ta e="T766" id="Seg_803" s="T765">bar</ta>
            <ta e="T767" id="Seg_804" s="T766">kü-laːm-bi</ta>
            <ta e="T768" id="Seg_805" s="T767">iʔbö-laʔbə</ta>
            <ta e="T769" id="Seg_806" s="T768">tibi-zAŋ</ta>
            <ta e="T771" id="Seg_807" s="T770">šonə-gA-jəʔ</ta>
            <ta e="T772" id="Seg_808" s="T771">i-bi-jəʔ</ta>
            <ta e="T773" id="Seg_809" s="T772">i</ta>
            <ta e="T774" id="Seg_810" s="T773">dĭ-m</ta>
            <ta e="T775" id="Seg_811" s="T774">hen-bi-jəʔ</ta>
            <ta e="T776" id="Seg_812" s="T775">kola-Tə</ta>
            <ta e="T778" id="Seg_813" s="T777">bos-də</ta>
            <ta e="T780" id="Seg_814" s="T779">kan-laʔbə-jəʔ</ta>
            <ta e="T781" id="Seg_815" s="T780">dĭ</ta>
            <ta e="T782" id="Seg_816" s="T781">bar</ta>
            <ta e="T783" id="Seg_817" s="T782">băra</ta>
            <ta e="T784" id="Seg_818" s="T783">am-bi</ta>
            <ta e="T785" id="Seg_819" s="T784">kola-bə</ta>
            <ta e="T786" id="Seg_820" s="T785">bar</ta>
            <ta e="T787" id="Seg_821" s="T786">barəʔ-bi</ta>
            <ta e="T788" id="Seg_822" s="T787">aʔtʼi-Tə</ta>
            <ta e="T789" id="Seg_823" s="T788">i</ta>
            <ta e="T790" id="Seg_824" s="T789">bos-də</ta>
            <ta e="T791" id="Seg_825" s="T790">nuʔmə-luʔbdə-bi</ta>
            <ta e="T792" id="Seg_826" s="T791">dĭgəttə</ta>
            <ta e="T793" id="Seg_827" s="T792">šo-bi</ta>
            <ta e="T794" id="Seg_828" s="T793">maʔ-gəndə</ta>
            <ta e="T795" id="Seg_829" s="T794">da</ta>
            <ta e="T797" id="Seg_830" s="T796">kola</ta>
            <ta e="T798" id="Seg_831" s="T797">am-liA</ta>
            <ta e="T799" id="Seg_832" s="T798">a</ta>
            <ta e="T800" id="Seg_833" s="T799">urgaːba</ta>
            <ta e="T801" id="Seg_834" s="T800">ku-liA-t</ta>
            <ta e="T802" id="Seg_835" s="T801">kola</ta>
            <ta e="T803" id="Seg_836" s="T802">am-liA</ta>
            <ta e="T804" id="Seg_837" s="T803">kan-lV-m</ta>
            <ta e="T805" id="Seg_838" s="T804">i</ta>
            <ta e="T806" id="Seg_839" s="T805">măn</ta>
            <ta e="T807" id="Seg_840" s="T806">xvostə</ta>
            <ta e="T808" id="Seg_841" s="T807">bü-Tə</ta>
            <ta e="T809" id="Seg_842" s="T808">saʔmə-luʔbdə-bi</ta>
            <ta e="T810" id="Seg_843" s="T809">dĭgəttə</ta>
            <ta e="T811" id="Seg_844" s="T810">amno-bi</ta>
            <ta e="T812" id="Seg_845" s="T811">amno-bi</ta>
            <ta e="T813" id="Seg_846" s="T812">a</ta>
            <ta e="T814" id="Seg_847" s="T813">ugaːndə</ta>
            <ta e="T815" id="Seg_848" s="T814">šišəge</ta>
            <ta e="T816" id="Seg_849" s="T815">i-bi</ta>
            <ta e="T817" id="Seg_850" s="T816">xvostə</ta>
            <ta e="T818" id="Seg_851" s="T817">kănzə-laːm-bi</ta>
            <ta e="T819" id="Seg_852" s="T818">dĭ</ta>
            <ta e="T822" id="Seg_853" s="T821">măn-ntə</ta>
            <ta e="T823" id="Seg_854" s="T822">adnaka</ta>
            <ta e="T824" id="Seg_855" s="T823">iʔgö</ta>
            <ta e="T825" id="Seg_856" s="T0">dĭ-m</ta>
            <ta e="T826" id="Seg_857" s="T825">nʼiʔdə-zittə</ta>
            <ta e="T827" id="Seg_858" s="T826">i</ta>
            <ta e="T828" id="Seg_859" s="T827">xvostə</ta>
            <ta e="T829" id="Seg_860" s="T828">saj</ta>
            <ta e="T831" id="Seg_861" s="T829">no-luʔbdə-bi</ta>
            <ta e="T832" id="Seg_862" s="T831">i</ta>
            <ta e="T833" id="Seg_863" s="T832">dĭgəttə</ta>
            <ta e="T835" id="Seg_864" s="T834">naga</ta>
            <ta e="T836" id="Seg_865" s="T835">dĭn</ta>
            <ta e="T837" id="Seg_866" s="T836">xvostə</ta>
            <ta e="T838" id="Seg_867" s="T837">bar</ta>
            <ta e="T839" id="Seg_868" s="T838">săj-nʼeʔbdə-bi</ta>
            <ta e="T840" id="Seg_869" s="T839">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T739" id="Seg_870" s="T738">live-PST-3PL</ta>
            <ta e="T740" id="Seg_871" s="T739">fox.[NOM.SG]</ta>
            <ta e="T741" id="Seg_872" s="T740">and</ta>
            <ta e="T742" id="Seg_873" s="T741">bear.[NOM.SG]</ta>
            <ta e="T743" id="Seg_874" s="T742">then</ta>
            <ta e="T744" id="Seg_875" s="T743">fox.[NOM.SG]</ta>
            <ta e="T745" id="Seg_876" s="T744">say-IPFVZ.[3SG]</ta>
            <ta e="T746" id="Seg_877" s="T745">I.NOM</ta>
            <ta e="T747" id="Seg_878" s="T746">go-FUT-1SG</ta>
            <ta e="T748" id="Seg_879" s="T747">fish.[NOM.SG]</ta>
            <ta e="T749" id="Seg_880" s="T748">capture-INF.LAT</ta>
            <ta e="T750" id="Seg_881" s="T749">and</ta>
            <ta e="T751" id="Seg_882" s="T750">how</ta>
            <ta e="T752" id="Seg_883" s="T751">you.NOM</ta>
            <ta e="T753" id="Seg_884" s="T752">and</ta>
            <ta e="T754" id="Seg_885" s="T753">tail-LAT/LOC.3SG</ta>
            <ta e="T755" id="Seg_886" s="T754">many</ta>
            <ta e="T756" id="Seg_887" s="T755">capture-FUT-1SG</ta>
            <ta e="T757" id="Seg_888" s="T756">then</ta>
            <ta e="T758" id="Seg_889" s="T757">run-MOM-PST.[3SG]</ta>
            <ta e="T759" id="Seg_890" s="T758">there</ta>
            <ta e="T760" id="Seg_891" s="T759">man-PL</ta>
            <ta e="T761" id="Seg_892" s="T760">fish.[NOM.SG]</ta>
            <ta e="T762" id="Seg_893" s="T761">capture-PST-3PL</ta>
            <ta e="T763" id="Seg_894" s="T762">this.[NOM.SG]</ta>
            <ta e="T764" id="Seg_895" s="T763">road-LAT</ta>
            <ta e="T765" id="Seg_896" s="T764">lie-PST.[3SG]</ta>
            <ta e="T766" id="Seg_897" s="T765">PTCL</ta>
            <ta e="T767" id="Seg_898" s="T766">die-RES-PST.[3SG]</ta>
            <ta e="T768" id="Seg_899" s="T767">lie-DUR.[3SG]</ta>
            <ta e="T769" id="Seg_900" s="T768">man-PL</ta>
            <ta e="T771" id="Seg_901" s="T770">come-PRS-3PL</ta>
            <ta e="T772" id="Seg_902" s="T771">take-PST-3PL</ta>
            <ta e="T773" id="Seg_903" s="T772">and</ta>
            <ta e="T774" id="Seg_904" s="T773">this-ACC</ta>
            <ta e="T775" id="Seg_905" s="T774">put-PST-3PL</ta>
            <ta e="T776" id="Seg_906" s="T775">fish-LAT</ta>
            <ta e="T778" id="Seg_907" s="T777">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T780" id="Seg_908" s="T779">go-DUR-3PL</ta>
            <ta e="T781" id="Seg_909" s="T780">this.[NOM.SG]</ta>
            <ta e="T782" id="Seg_910" s="T781">PTCL</ta>
            <ta e="T783" id="Seg_911" s="T782">sack.[NOM.SG]</ta>
            <ta e="T784" id="Seg_912" s="T783">eat-PST.[3SG]</ta>
            <ta e="T785" id="Seg_913" s="T784">fish-ACC.3SG</ta>
            <ta e="T786" id="Seg_914" s="T785">all</ta>
            <ta e="T787" id="Seg_915" s="T786">throw.away-PST.[3SG]</ta>
            <ta e="T788" id="Seg_916" s="T787">road-LAT</ta>
            <ta e="T789" id="Seg_917" s="T788">and</ta>
            <ta e="T790" id="Seg_918" s="T789">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T791" id="Seg_919" s="T790">run-MOM-PST.[3SG]</ta>
            <ta e="T792" id="Seg_920" s="T791">then</ta>
            <ta e="T793" id="Seg_921" s="T792">come-PST.[3SG]</ta>
            <ta e="T794" id="Seg_922" s="T793">tent-LAT/LOC.3SG</ta>
            <ta e="T795" id="Seg_923" s="T794">and</ta>
            <ta e="T797" id="Seg_924" s="T796">fish.[NOM.SG]</ta>
            <ta e="T798" id="Seg_925" s="T797">eat-PRS.[3SG]</ta>
            <ta e="T799" id="Seg_926" s="T798">and</ta>
            <ta e="T800" id="Seg_927" s="T799">bear.[NOM.SG]</ta>
            <ta e="T801" id="Seg_928" s="T800">see-PRS-3SG.O</ta>
            <ta e="T802" id="Seg_929" s="T801">fish.[NOM.SG]</ta>
            <ta e="T803" id="Seg_930" s="T802">eat-PRS.[3SG]</ta>
            <ta e="T804" id="Seg_931" s="T803">go-FUT-1SG</ta>
            <ta e="T805" id="Seg_932" s="T804">and</ta>
            <ta e="T806" id="Seg_933" s="T805">I.NOM</ta>
            <ta e="T807" id="Seg_934" s="T806">tail.[NOM.SG]</ta>
            <ta e="T808" id="Seg_935" s="T807">water-LAT</ta>
            <ta e="T809" id="Seg_936" s="T808">fall-MOM-PST.[3SG]</ta>
            <ta e="T810" id="Seg_937" s="T809">then</ta>
            <ta e="T811" id="Seg_938" s="T810">sit-PST.[3SG]</ta>
            <ta e="T812" id="Seg_939" s="T811">sit-PST.[3SG]</ta>
            <ta e="T813" id="Seg_940" s="T812">and</ta>
            <ta e="T814" id="Seg_941" s="T813">very</ta>
            <ta e="T815" id="Seg_942" s="T814">cold.[NOM.SG]</ta>
            <ta e="T816" id="Seg_943" s="T815">be-PST.[3SG]</ta>
            <ta e="T817" id="Seg_944" s="T816">tail.[NOM.SG]</ta>
            <ta e="T818" id="Seg_945" s="T817">freeze-RES-PST.[3SG]</ta>
            <ta e="T819" id="Seg_946" s="T818">this.[NOM.SG]</ta>
            <ta e="T822" id="Seg_947" s="T821">say-IPFVZ.[3SG]</ta>
            <ta e="T823" id="Seg_948" s="T822">however</ta>
            <ta e="T824" id="Seg_949" s="T823">many</ta>
            <ta e="T825" id="Seg_950" s="T0">this-ACC</ta>
            <ta e="T826" id="Seg_951" s="T825">pull-INF.LAT</ta>
            <ta e="T827" id="Seg_952" s="T826">and</ta>
            <ta e="T828" id="Seg_953" s="T827">tail.[NOM.SG]</ta>
            <ta e="T829" id="Seg_954" s="T828">off</ta>
            <ta e="T831" id="Seg_955" s="T829">%tear.off-MOM-PST.[3SG]</ta>
            <ta e="T832" id="Seg_956" s="T831">and</ta>
            <ta e="T833" id="Seg_957" s="T832">then</ta>
            <ta e="T835" id="Seg_958" s="T834">NEG.EX.[3SG]</ta>
            <ta e="T836" id="Seg_959" s="T835">there</ta>
            <ta e="T837" id="Seg_960" s="T836">tail.[NOM.SG]</ta>
            <ta e="T838" id="Seg_961" s="T837">all</ta>
            <ta e="T839" id="Seg_962" s="T838">off-pull-PST.[3SG]</ta>
            <ta e="T840" id="Seg_963" s="T839">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T739" id="Seg_964" s="T738">жить-PST-3PL</ta>
            <ta e="T740" id="Seg_965" s="T739">лисица.[NOM.SG]</ta>
            <ta e="T741" id="Seg_966" s="T740">и</ta>
            <ta e="T742" id="Seg_967" s="T741">медведь.[NOM.SG]</ta>
            <ta e="T743" id="Seg_968" s="T742">тогда</ta>
            <ta e="T744" id="Seg_969" s="T743">лисица.[NOM.SG]</ta>
            <ta e="T745" id="Seg_970" s="T744">сказать-IPFVZ.[3SG]</ta>
            <ta e="T746" id="Seg_971" s="T745">я.NOM</ta>
            <ta e="T747" id="Seg_972" s="T746">пойти-FUT-1SG</ta>
            <ta e="T748" id="Seg_973" s="T747">рыба.[NOM.SG]</ta>
            <ta e="T749" id="Seg_974" s="T748">ловить-INF.LAT</ta>
            <ta e="T750" id="Seg_975" s="T749">а</ta>
            <ta e="T751" id="Seg_976" s="T750">как</ta>
            <ta e="T752" id="Seg_977" s="T751">ты.NOM</ta>
            <ta e="T753" id="Seg_978" s="T752">и</ta>
            <ta e="T754" id="Seg_979" s="T753">хвост-LAT/LOC.3SG</ta>
            <ta e="T755" id="Seg_980" s="T754">много</ta>
            <ta e="T756" id="Seg_981" s="T755">ловить-FUT-1SG</ta>
            <ta e="T757" id="Seg_982" s="T756">тогда</ta>
            <ta e="T758" id="Seg_983" s="T757">бежать-MOM-PST.[3SG]</ta>
            <ta e="T759" id="Seg_984" s="T758">там</ta>
            <ta e="T760" id="Seg_985" s="T759">мужчина-PL</ta>
            <ta e="T761" id="Seg_986" s="T760">рыба.[NOM.SG]</ta>
            <ta e="T762" id="Seg_987" s="T761">ловить-PST-3PL</ta>
            <ta e="T763" id="Seg_988" s="T762">этот.[NOM.SG]</ta>
            <ta e="T764" id="Seg_989" s="T763">дорога-LAT</ta>
            <ta e="T765" id="Seg_990" s="T764">лежать-PST.[3SG]</ta>
            <ta e="T766" id="Seg_991" s="T765">PTCL</ta>
            <ta e="T767" id="Seg_992" s="T766">умереть-RES-PST.[3SG]</ta>
            <ta e="T768" id="Seg_993" s="T767">лежать-DUR.[3SG]</ta>
            <ta e="T769" id="Seg_994" s="T768">мужчина-PL</ta>
            <ta e="T771" id="Seg_995" s="T770">прийти-PRS-3PL</ta>
            <ta e="T772" id="Seg_996" s="T771">взять-PST-3PL</ta>
            <ta e="T773" id="Seg_997" s="T772">и</ta>
            <ta e="T774" id="Seg_998" s="T773">этот-ACC</ta>
            <ta e="T775" id="Seg_999" s="T774">класть-PST-3PL</ta>
            <ta e="T776" id="Seg_1000" s="T775">рыба-LAT</ta>
            <ta e="T778" id="Seg_1001" s="T777">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T780" id="Seg_1002" s="T779">пойти-DUR-3PL</ta>
            <ta e="T781" id="Seg_1003" s="T780">этот.[NOM.SG]</ta>
            <ta e="T782" id="Seg_1004" s="T781">PTCL</ta>
            <ta e="T783" id="Seg_1005" s="T782">мешок.[NOM.SG]</ta>
            <ta e="T784" id="Seg_1006" s="T783">съесть-PST.[3SG]</ta>
            <ta e="T785" id="Seg_1007" s="T784">рыба-ACC.3SG</ta>
            <ta e="T786" id="Seg_1008" s="T785">весь</ta>
            <ta e="T787" id="Seg_1009" s="T786">выбросить-PST.[3SG]</ta>
            <ta e="T788" id="Seg_1010" s="T787">дорога-LAT</ta>
            <ta e="T789" id="Seg_1011" s="T788">и</ta>
            <ta e="T790" id="Seg_1012" s="T789">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T791" id="Seg_1013" s="T790">бежать-MOM-PST.[3SG]</ta>
            <ta e="T792" id="Seg_1014" s="T791">тогда</ta>
            <ta e="T793" id="Seg_1015" s="T792">прийти-PST.[3SG]</ta>
            <ta e="T794" id="Seg_1016" s="T793">чум-LAT/LOC.3SG</ta>
            <ta e="T795" id="Seg_1017" s="T794">и</ta>
            <ta e="T797" id="Seg_1018" s="T796">рыба.[NOM.SG]</ta>
            <ta e="T798" id="Seg_1019" s="T797">съесть-PRS.[3SG]</ta>
            <ta e="T799" id="Seg_1020" s="T798">а</ta>
            <ta e="T800" id="Seg_1021" s="T799">медведь.[NOM.SG]</ta>
            <ta e="T801" id="Seg_1022" s="T800">видеть-PRS-3SG.O</ta>
            <ta e="T802" id="Seg_1023" s="T801">рыба.[NOM.SG]</ta>
            <ta e="T803" id="Seg_1024" s="T802">съесть-PRS.[3SG]</ta>
            <ta e="T804" id="Seg_1025" s="T803">пойти-FUT-1SG</ta>
            <ta e="T805" id="Seg_1026" s="T804">и</ta>
            <ta e="T806" id="Seg_1027" s="T805">я.NOM</ta>
            <ta e="T807" id="Seg_1028" s="T806">хвост.[NOM.SG]</ta>
            <ta e="T808" id="Seg_1029" s="T807">вода-LAT</ta>
            <ta e="T809" id="Seg_1030" s="T808">упасть-MOM-PST.[3SG]</ta>
            <ta e="T810" id="Seg_1031" s="T809">тогда</ta>
            <ta e="T811" id="Seg_1032" s="T810">сидеть-PST.[3SG]</ta>
            <ta e="T812" id="Seg_1033" s="T811">сидеть-PST.[3SG]</ta>
            <ta e="T813" id="Seg_1034" s="T812">а</ta>
            <ta e="T814" id="Seg_1035" s="T813">очень</ta>
            <ta e="T815" id="Seg_1036" s="T814">холодный.[NOM.SG]</ta>
            <ta e="T816" id="Seg_1037" s="T815">быть-PST.[3SG]</ta>
            <ta e="T817" id="Seg_1038" s="T816">хвост.[NOM.SG]</ta>
            <ta e="T818" id="Seg_1039" s="T817">замерзнуть-RES-PST.[3SG]</ta>
            <ta e="T819" id="Seg_1040" s="T818">этот.[NOM.SG]</ta>
            <ta e="T822" id="Seg_1041" s="T821">сказать-IPFVZ.[3SG]</ta>
            <ta e="T823" id="Seg_1042" s="T822">однако</ta>
            <ta e="T824" id="Seg_1043" s="T823">много</ta>
            <ta e="T825" id="Seg_1044" s="T0">этот-ACC</ta>
            <ta e="T826" id="Seg_1045" s="T825">тянуть-INF.LAT</ta>
            <ta e="T827" id="Seg_1046" s="T826">и</ta>
            <ta e="T828" id="Seg_1047" s="T827">хвост.[NOM.SG]</ta>
            <ta e="T829" id="Seg_1048" s="T828">от</ta>
            <ta e="T831" id="Seg_1049" s="T829">%оторвать-MOM-PST.[3SG]</ta>
            <ta e="T832" id="Seg_1050" s="T831">и</ta>
            <ta e="T833" id="Seg_1051" s="T832">тогда</ta>
            <ta e="T835" id="Seg_1052" s="T834">NEG.EX.[3SG]</ta>
            <ta e="T836" id="Seg_1053" s="T835">там</ta>
            <ta e="T837" id="Seg_1054" s="T836">хвост.[NOM.SG]</ta>
            <ta e="T838" id="Seg_1055" s="T837">весь</ta>
            <ta e="T839" id="Seg_1056" s="T838">от-тянуть-PST.[3SG]</ta>
            <ta e="T840" id="Seg_1057" s="T839">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T739" id="Seg_1058" s="T738">v-v:tense-v:pn</ta>
            <ta e="T740" id="Seg_1059" s="T739">n-n:case</ta>
            <ta e="T741" id="Seg_1060" s="T740">conj</ta>
            <ta e="T742" id="Seg_1061" s="T741">n-n:case</ta>
            <ta e="T743" id="Seg_1062" s="T742">adv</ta>
            <ta e="T744" id="Seg_1063" s="T743">n-n:case</ta>
            <ta e="T745" id="Seg_1064" s="T744">v-v&gt;v-v:pn</ta>
            <ta e="T746" id="Seg_1065" s="T745">pers</ta>
            <ta e="T747" id="Seg_1066" s="T746">v-v:tense-v:pn</ta>
            <ta e="T748" id="Seg_1067" s="T747">n-n:case</ta>
            <ta e="T749" id="Seg_1068" s="T748">v-v:n.fin</ta>
            <ta e="T750" id="Seg_1069" s="T749">conj</ta>
            <ta e="T751" id="Seg_1070" s="T750">que</ta>
            <ta e="T752" id="Seg_1071" s="T751">pers</ta>
            <ta e="T753" id="Seg_1072" s="T752">conj</ta>
            <ta e="T754" id="Seg_1073" s="T753">n-n:case.poss</ta>
            <ta e="T755" id="Seg_1074" s="T754">quant</ta>
            <ta e="T756" id="Seg_1075" s="T755">v-v:tense-v:pn</ta>
            <ta e="T757" id="Seg_1076" s="T756">adv</ta>
            <ta e="T758" id="Seg_1077" s="T757">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T759" id="Seg_1078" s="T758">adv</ta>
            <ta e="T760" id="Seg_1079" s="T759">n-n:num</ta>
            <ta e="T761" id="Seg_1080" s="T760">n-n:case</ta>
            <ta e="T762" id="Seg_1081" s="T761">v-v:tense-v:pn</ta>
            <ta e="T763" id="Seg_1082" s="T762">dempro-n:case</ta>
            <ta e="T764" id="Seg_1083" s="T763">n-n:case</ta>
            <ta e="T765" id="Seg_1084" s="T764">v-v:tense-v:pn</ta>
            <ta e="T766" id="Seg_1085" s="T765">ptcl</ta>
            <ta e="T767" id="Seg_1086" s="T766">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T768" id="Seg_1087" s="T767">v-v&gt;v-v:pn</ta>
            <ta e="T769" id="Seg_1088" s="T768">n-n:num</ta>
            <ta e="T771" id="Seg_1089" s="T770">v-v:tense-v:pn</ta>
            <ta e="T772" id="Seg_1090" s="T771">v-v:tense-v:pn</ta>
            <ta e="T773" id="Seg_1091" s="T772">conj</ta>
            <ta e="T774" id="Seg_1092" s="T773">dempro-n:case</ta>
            <ta e="T775" id="Seg_1093" s="T774">v-v:tense-v:pn</ta>
            <ta e="T776" id="Seg_1094" s="T775">n-n:case</ta>
            <ta e="T778" id="Seg_1095" s="T777">refl-n:case.poss</ta>
            <ta e="T780" id="Seg_1096" s="T779">v-v&gt;v-v:pn</ta>
            <ta e="T781" id="Seg_1097" s="T780">dempro-n:case</ta>
            <ta e="T782" id="Seg_1098" s="T781">ptcl</ta>
            <ta e="T783" id="Seg_1099" s="T782">n-n:case</ta>
            <ta e="T784" id="Seg_1100" s="T783">v-v:tense-v:pn</ta>
            <ta e="T785" id="Seg_1101" s="T784">n-n:case.poss</ta>
            <ta e="T786" id="Seg_1102" s="T785">quant</ta>
            <ta e="T787" id="Seg_1103" s="T786">v-v:tense-v:pn</ta>
            <ta e="T788" id="Seg_1104" s="T787">n-n:case</ta>
            <ta e="T789" id="Seg_1105" s="T788">conj</ta>
            <ta e="T790" id="Seg_1106" s="T789">refl-n:case.poss</ta>
            <ta e="T791" id="Seg_1107" s="T790">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T792" id="Seg_1108" s="T791">adv</ta>
            <ta e="T793" id="Seg_1109" s="T792">v-v:tense-v:pn</ta>
            <ta e="T794" id="Seg_1110" s="T793">n-n:case.poss</ta>
            <ta e="T795" id="Seg_1111" s="T794">conj</ta>
            <ta e="T797" id="Seg_1112" s="T796">n-n:case</ta>
            <ta e="T798" id="Seg_1113" s="T797">v-v:tense-v:pn</ta>
            <ta e="T799" id="Seg_1114" s="T798">conj</ta>
            <ta e="T800" id="Seg_1115" s="T799">n-n:case</ta>
            <ta e="T801" id="Seg_1116" s="T800">v-v:tense-v:pn</ta>
            <ta e="T802" id="Seg_1117" s="T801">n-n:case</ta>
            <ta e="T803" id="Seg_1118" s="T802">v-v:tense-v:pn</ta>
            <ta e="T804" id="Seg_1119" s="T803">v-v:tense-v:pn</ta>
            <ta e="T805" id="Seg_1120" s="T804">conj</ta>
            <ta e="T806" id="Seg_1121" s="T805">pers</ta>
            <ta e="T807" id="Seg_1122" s="T806">n-n:case</ta>
            <ta e="T808" id="Seg_1123" s="T807">n-n:case</ta>
            <ta e="T809" id="Seg_1124" s="T808">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T810" id="Seg_1125" s="T809">adv</ta>
            <ta e="T811" id="Seg_1126" s="T810">v-v:tense-v:pn</ta>
            <ta e="T812" id="Seg_1127" s="T811">v-v:tense-v:pn</ta>
            <ta e="T813" id="Seg_1128" s="T812">conj</ta>
            <ta e="T814" id="Seg_1129" s="T813">adv</ta>
            <ta e="T815" id="Seg_1130" s="T814">adj-n:case</ta>
            <ta e="T816" id="Seg_1131" s="T815">v-v:tense-v:pn</ta>
            <ta e="T817" id="Seg_1132" s="T816">n-n:case</ta>
            <ta e="T818" id="Seg_1133" s="T817">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T819" id="Seg_1134" s="T818">dempro-n:case</ta>
            <ta e="T822" id="Seg_1135" s="T821">v-v&gt;v-v:pn</ta>
            <ta e="T823" id="Seg_1136" s="T822">adv</ta>
            <ta e="T824" id="Seg_1137" s="T823">quant</ta>
            <ta e="T825" id="Seg_1138" s="T0">dempro-n:case</ta>
            <ta e="T826" id="Seg_1139" s="T825">v-v:n.fin</ta>
            <ta e="T827" id="Seg_1140" s="T826">conj</ta>
            <ta e="T828" id="Seg_1141" s="T827">n-n:case</ta>
            <ta e="T829" id="Seg_1142" s="T828">adv</ta>
            <ta e="T831" id="Seg_1143" s="T829">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T832" id="Seg_1144" s="T831">conj</ta>
            <ta e="T833" id="Seg_1145" s="T832">adv</ta>
            <ta e="T835" id="Seg_1146" s="T834">v-v:pn</ta>
            <ta e="T836" id="Seg_1147" s="T835">adv</ta>
            <ta e="T837" id="Seg_1148" s="T836">n-n:case</ta>
            <ta e="T838" id="Seg_1149" s="T837">quant</ta>
            <ta e="T839" id="Seg_1150" s="T838">v&gt;v-v-v:tense-v:pn</ta>
            <ta e="T840" id="Seg_1151" s="T839">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T739" id="Seg_1152" s="T738">v</ta>
            <ta e="T740" id="Seg_1153" s="T739">n</ta>
            <ta e="T741" id="Seg_1154" s="T740">conj</ta>
            <ta e="T742" id="Seg_1155" s="T741">n</ta>
            <ta e="T743" id="Seg_1156" s="T742">adv</ta>
            <ta e="T744" id="Seg_1157" s="T743">n</ta>
            <ta e="T745" id="Seg_1158" s="T744">v</ta>
            <ta e="T746" id="Seg_1159" s="T745">pers</ta>
            <ta e="T747" id="Seg_1160" s="T746">v</ta>
            <ta e="T748" id="Seg_1161" s="T747">n</ta>
            <ta e="T749" id="Seg_1162" s="T748">v</ta>
            <ta e="T750" id="Seg_1163" s="T749">conj</ta>
            <ta e="T751" id="Seg_1164" s="T750">que</ta>
            <ta e="T752" id="Seg_1165" s="T751">pers</ta>
            <ta e="T753" id="Seg_1166" s="T752">conj</ta>
            <ta e="T754" id="Seg_1167" s="T753">n</ta>
            <ta e="T755" id="Seg_1168" s="T754">quant</ta>
            <ta e="T756" id="Seg_1169" s="T755">v</ta>
            <ta e="T757" id="Seg_1170" s="T756">adv</ta>
            <ta e="T758" id="Seg_1171" s="T757">v</ta>
            <ta e="T759" id="Seg_1172" s="T758">adv</ta>
            <ta e="T760" id="Seg_1173" s="T759">n</ta>
            <ta e="T761" id="Seg_1174" s="T760">n</ta>
            <ta e="T762" id="Seg_1175" s="T761">v</ta>
            <ta e="T763" id="Seg_1176" s="T762">dempro</ta>
            <ta e="T764" id="Seg_1177" s="T763">n</ta>
            <ta e="T765" id="Seg_1178" s="T764">v</ta>
            <ta e="T766" id="Seg_1179" s="T765">ptcl</ta>
            <ta e="T767" id="Seg_1180" s="T766">v</ta>
            <ta e="T768" id="Seg_1181" s="T767">v</ta>
            <ta e="T769" id="Seg_1182" s="T768">n</ta>
            <ta e="T771" id="Seg_1183" s="T770">v</ta>
            <ta e="T772" id="Seg_1184" s="T771">v</ta>
            <ta e="T773" id="Seg_1185" s="T772">conj</ta>
            <ta e="T774" id="Seg_1186" s="T773">dempro</ta>
            <ta e="T775" id="Seg_1187" s="T774">v</ta>
            <ta e="T776" id="Seg_1188" s="T775">n</ta>
            <ta e="T778" id="Seg_1189" s="T777">refl</ta>
            <ta e="T780" id="Seg_1190" s="T779">v</ta>
            <ta e="T781" id="Seg_1191" s="T780">dempro</ta>
            <ta e="T782" id="Seg_1192" s="T781">ptcl</ta>
            <ta e="T783" id="Seg_1193" s="T782">n</ta>
            <ta e="T784" id="Seg_1194" s="T783">v</ta>
            <ta e="T785" id="Seg_1195" s="T784">n</ta>
            <ta e="T786" id="Seg_1196" s="T785">quant</ta>
            <ta e="T787" id="Seg_1197" s="T786">v</ta>
            <ta e="T788" id="Seg_1198" s="T787">n</ta>
            <ta e="T789" id="Seg_1199" s="T788">conj</ta>
            <ta e="T790" id="Seg_1200" s="T789">refl</ta>
            <ta e="T791" id="Seg_1201" s="T790">v</ta>
            <ta e="T792" id="Seg_1202" s="T791">adv</ta>
            <ta e="T793" id="Seg_1203" s="T792">v</ta>
            <ta e="T794" id="Seg_1204" s="T793">n</ta>
            <ta e="T795" id="Seg_1205" s="T794">conj</ta>
            <ta e="T797" id="Seg_1206" s="T796">n</ta>
            <ta e="T798" id="Seg_1207" s="T797">v</ta>
            <ta e="T799" id="Seg_1208" s="T798">conj</ta>
            <ta e="T800" id="Seg_1209" s="T799">n</ta>
            <ta e="T801" id="Seg_1210" s="T800">v</ta>
            <ta e="T802" id="Seg_1211" s="T801">n</ta>
            <ta e="T803" id="Seg_1212" s="T802">v</ta>
            <ta e="T804" id="Seg_1213" s="T803">v</ta>
            <ta e="T805" id="Seg_1214" s="T804">conj</ta>
            <ta e="T806" id="Seg_1215" s="T805">pers</ta>
            <ta e="T807" id="Seg_1216" s="T806">n</ta>
            <ta e="T808" id="Seg_1217" s="T807">n</ta>
            <ta e="T809" id="Seg_1218" s="T808">v</ta>
            <ta e="T810" id="Seg_1219" s="T809">adv</ta>
            <ta e="T811" id="Seg_1220" s="T810">v</ta>
            <ta e="T812" id="Seg_1221" s="T811">v</ta>
            <ta e="T813" id="Seg_1222" s="T812">conj</ta>
            <ta e="T814" id="Seg_1223" s="T813">adv</ta>
            <ta e="T815" id="Seg_1224" s="T814">adj</ta>
            <ta e="T816" id="Seg_1225" s="T815">v</ta>
            <ta e="T817" id="Seg_1226" s="T816">n</ta>
            <ta e="T818" id="Seg_1227" s="T817">v</ta>
            <ta e="T819" id="Seg_1228" s="T818">dempro</ta>
            <ta e="T822" id="Seg_1229" s="T821">v</ta>
            <ta e="T823" id="Seg_1230" s="T822">adv</ta>
            <ta e="T824" id="Seg_1231" s="T823">quant</ta>
            <ta e="T825" id="Seg_1232" s="T0">dempro</ta>
            <ta e="T826" id="Seg_1233" s="T825">adv</ta>
            <ta e="T827" id="Seg_1234" s="T826">conj</ta>
            <ta e="T828" id="Seg_1235" s="T827">n</ta>
            <ta e="T829" id="Seg_1236" s="T828">adv</ta>
            <ta e="T831" id="Seg_1237" s="T829">v</ta>
            <ta e="T832" id="Seg_1238" s="T831">conj</ta>
            <ta e="T833" id="Seg_1239" s="T832">adv</ta>
            <ta e="T835" id="Seg_1240" s="T834">v</ta>
            <ta e="T836" id="Seg_1241" s="T835">adv</ta>
            <ta e="T837" id="Seg_1242" s="T836">n</ta>
            <ta e="T838" id="Seg_1243" s="T837">quant</ta>
            <ta e="T839" id="Seg_1244" s="T838">v</ta>
            <ta e="T840" id="Seg_1245" s="T839">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T740" id="Seg_1246" s="T739">np.h:E</ta>
            <ta e="T742" id="Seg_1247" s="T741">np.h:E</ta>
            <ta e="T743" id="Seg_1248" s="T742">adv:Time</ta>
            <ta e="T744" id="Seg_1249" s="T743">np.h:A</ta>
            <ta e="T746" id="Seg_1250" s="T745">pro.h:A</ta>
            <ta e="T748" id="Seg_1251" s="T747">np:P</ta>
            <ta e="T752" id="Seg_1252" s="T751">pro.h:A</ta>
            <ta e="T754" id="Seg_1253" s="T753">np:Ins</ta>
            <ta e="T755" id="Seg_1254" s="T754">np:P</ta>
            <ta e="T756" id="Seg_1255" s="T755">0.1.h:A</ta>
            <ta e="T757" id="Seg_1256" s="T756">adv:Time</ta>
            <ta e="T758" id="Seg_1257" s="T757">0.3.h:A</ta>
            <ta e="T759" id="Seg_1258" s="T758">adv:L</ta>
            <ta e="T760" id="Seg_1259" s="T759">np.h:A</ta>
            <ta e="T761" id="Seg_1260" s="T760">np:P</ta>
            <ta e="T762" id="Seg_1261" s="T761">pro.h:A</ta>
            <ta e="T763" id="Seg_1262" s="T762">pro.h:A</ta>
            <ta e="T764" id="Seg_1263" s="T763">np:G</ta>
            <ta e="T767" id="Seg_1264" s="T766">0.3.h:A</ta>
            <ta e="T768" id="Seg_1265" s="T767">0.3.h:E</ta>
            <ta e="T769" id="Seg_1266" s="T768">np.h:A</ta>
            <ta e="T772" id="Seg_1267" s="T771">0.3.h:A</ta>
            <ta e="T774" id="Seg_1268" s="T773">pro.h:Th</ta>
            <ta e="T775" id="Seg_1269" s="T774">0.3.h:A</ta>
            <ta e="T776" id="Seg_1270" s="T775">np:G</ta>
            <ta e="T780" id="Seg_1271" s="T779">0.3.h:A</ta>
            <ta e="T781" id="Seg_1272" s="T780">pro.h:A</ta>
            <ta e="T783" id="Seg_1273" s="T782">np:P</ta>
            <ta e="T785" id="Seg_1274" s="T784">np:Th</ta>
            <ta e="T788" id="Seg_1275" s="T787">np:G</ta>
            <ta e="T791" id="Seg_1276" s="T790">0.3.h:A</ta>
            <ta e="T792" id="Seg_1277" s="T791">adv:Time</ta>
            <ta e="T793" id="Seg_1278" s="T792">0.3.h:A</ta>
            <ta e="T794" id="Seg_1279" s="T793">np:G</ta>
            <ta e="T797" id="Seg_1280" s="T796">np:P</ta>
            <ta e="T798" id="Seg_1281" s="T797">0.3.h:A</ta>
            <ta e="T800" id="Seg_1282" s="T799">np.h:E</ta>
            <ta e="T802" id="Seg_1283" s="T801">np:P</ta>
            <ta e="T803" id="Seg_1284" s="T802">0.3.h:A</ta>
            <ta e="T806" id="Seg_1285" s="T805">pro.h:A</ta>
            <ta e="T807" id="Seg_1286" s="T806">np:Th</ta>
            <ta e="T808" id="Seg_1287" s="T807">np:G</ta>
            <ta e="T809" id="Seg_1288" s="T808">0.3.h:A</ta>
            <ta e="T810" id="Seg_1289" s="T809">adv:Time</ta>
            <ta e="T811" id="Seg_1290" s="T810">0.3.h:E</ta>
            <ta e="T812" id="Seg_1291" s="T811">0.3.h:E</ta>
            <ta e="T816" id="Seg_1292" s="T815">0.3.h:Th</ta>
            <ta e="T817" id="Seg_1293" s="T816">np:Th</ta>
            <ta e="T819" id="Seg_1294" s="T818">pro.h:A</ta>
            <ta e="T825" id="Seg_1295" s="T0">pro:Th</ta>
            <ta e="T828" id="Seg_1296" s="T827">np:P</ta>
            <ta e="T833" id="Seg_1297" s="T832">adv:Time</ta>
            <ta e="T836" id="Seg_1298" s="T835">adv:L</ta>
            <ta e="T837" id="Seg_1299" s="T836">np:Th</ta>
            <ta e="T839" id="Seg_1300" s="T838">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T739" id="Seg_1301" s="T738">v:pred</ta>
            <ta e="T740" id="Seg_1302" s="T739">np.h:S</ta>
            <ta e="T742" id="Seg_1303" s="T741">np.h:S</ta>
            <ta e="T744" id="Seg_1304" s="T743">np.h:S</ta>
            <ta e="T745" id="Seg_1305" s="T744">v:pred</ta>
            <ta e="T746" id="Seg_1306" s="T745">pro.h:S</ta>
            <ta e="T747" id="Seg_1307" s="T746">v:pred</ta>
            <ta e="T748" id="Seg_1308" s="T747">np:O</ta>
            <ta e="T749" id="Seg_1309" s="T748">s:purp</ta>
            <ta e="T752" id="Seg_1310" s="T751">pro.h:S</ta>
            <ta e="T755" id="Seg_1311" s="T754">np:O</ta>
            <ta e="T756" id="Seg_1312" s="T755">v:pred 0.1.h:S</ta>
            <ta e="T758" id="Seg_1313" s="T757">v:pred 0.3.h:S</ta>
            <ta e="T760" id="Seg_1314" s="T759">np.h:S</ta>
            <ta e="T761" id="Seg_1315" s="T760">np:O</ta>
            <ta e="T762" id="Seg_1316" s="T761">v:pred</ta>
            <ta e="T763" id="Seg_1317" s="T762">pro.h:S</ta>
            <ta e="T765" id="Seg_1318" s="T764">v:pred</ta>
            <ta e="T767" id="Seg_1319" s="T766">v:pred 0.3.h:S</ta>
            <ta e="T768" id="Seg_1320" s="T767">v:pred 0.3.h:S</ta>
            <ta e="T769" id="Seg_1321" s="T768">np.h:S</ta>
            <ta e="T771" id="Seg_1322" s="T770">v:pred</ta>
            <ta e="T772" id="Seg_1323" s="T771">v:pred 0.3.h:S</ta>
            <ta e="T774" id="Seg_1324" s="T773">pro.h:O</ta>
            <ta e="T775" id="Seg_1325" s="T774">v:pred 0.3.h:S</ta>
            <ta e="T780" id="Seg_1326" s="T779">v:pred 0.3.h:S</ta>
            <ta e="T781" id="Seg_1327" s="T780">pro.h:S</ta>
            <ta e="T783" id="Seg_1328" s="T782">np:O</ta>
            <ta e="T784" id="Seg_1329" s="T783">v:pred</ta>
            <ta e="T785" id="Seg_1330" s="T784">np:O</ta>
            <ta e="T787" id="Seg_1331" s="T786">v:pred 0.3.h:S</ta>
            <ta e="T791" id="Seg_1332" s="T790">v:pred 0.3.h:S</ta>
            <ta e="T793" id="Seg_1333" s="T792">v:pred 0.3.h:S</ta>
            <ta e="T797" id="Seg_1334" s="T796">np:O</ta>
            <ta e="T798" id="Seg_1335" s="T797">v:pred 0.3.h:S</ta>
            <ta e="T800" id="Seg_1336" s="T799">np.h:S</ta>
            <ta e="T801" id="Seg_1337" s="T800">v:pred</ta>
            <ta e="T802" id="Seg_1338" s="T801">np:O</ta>
            <ta e="T803" id="Seg_1339" s="T802">v:pred 0.3.h:S</ta>
            <ta e="T804" id="Seg_1340" s="T803">v:pred</ta>
            <ta e="T806" id="Seg_1341" s="T805">pro.h:S</ta>
            <ta e="T807" id="Seg_1342" s="T806">np:O</ta>
            <ta e="T809" id="Seg_1343" s="T808">v:pred 0.3.h:S</ta>
            <ta e="T811" id="Seg_1344" s="T810">v:pred 0.3.h:S</ta>
            <ta e="T812" id="Seg_1345" s="T811">v:pred 0.3.h:S</ta>
            <ta e="T815" id="Seg_1346" s="T814">adj:pred</ta>
            <ta e="T816" id="Seg_1347" s="T815">cop 0.3.h:S</ta>
            <ta e="T817" id="Seg_1348" s="T816">np:S</ta>
            <ta e="T818" id="Seg_1349" s="T817">v:pred</ta>
            <ta e="T819" id="Seg_1350" s="T818">pro.h:S</ta>
            <ta e="T822" id="Seg_1351" s="T821">v:pred</ta>
            <ta e="T825" id="Seg_1352" s="T0">pro:O</ta>
            <ta e="T828" id="Seg_1353" s="T827">np:S</ta>
            <ta e="T831" id="Seg_1354" s="T829">v:pred</ta>
            <ta e="T835" id="Seg_1355" s="T834">v:pred</ta>
            <ta e="T837" id="Seg_1356" s="T836">np:S</ta>
            <ta e="T839" id="Seg_1357" s="T838">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T740" id="Seg_1358" s="T739">RUS:cult</ta>
            <ta e="T741" id="Seg_1359" s="T740">RUS:gram</ta>
            <ta e="T744" id="Seg_1360" s="T743">RUS:cult</ta>
            <ta e="T750" id="Seg_1361" s="T749">RUS:gram</ta>
            <ta e="T753" id="Seg_1362" s="T752">RUS:gram</ta>
            <ta e="T754" id="Seg_1363" s="T753">RUS:core</ta>
            <ta e="T766" id="Seg_1364" s="T765">TURK:disc</ta>
            <ta e="T773" id="Seg_1365" s="T772">RUS:gram</ta>
            <ta e="T782" id="Seg_1366" s="T781">TURK:disc</ta>
            <ta e="T786" id="Seg_1367" s="T785">TURK:disc</ta>
            <ta e="T789" id="Seg_1368" s="T788">RUS:gram</ta>
            <ta e="T795" id="Seg_1369" s="T794">RUS:gram</ta>
            <ta e="T799" id="Seg_1370" s="T798">RUS:gram</ta>
            <ta e="T805" id="Seg_1371" s="T804">RUS:gram</ta>
            <ta e="T807" id="Seg_1372" s="T806">RUS:core</ta>
            <ta e="T813" id="Seg_1373" s="T812">RUS:gram</ta>
            <ta e="T817" id="Seg_1374" s="T816">RUS:core</ta>
            <ta e="T823" id="Seg_1375" s="T822">RUS:mod</ta>
            <ta e="T827" id="Seg_1376" s="T826">RUS:gram</ta>
            <ta e="T828" id="Seg_1377" s="T827">RUS:core</ta>
            <ta e="T832" id="Seg_1378" s="T831">RUS:gram</ta>
            <ta e="T837" id="Seg_1379" s="T836">RUS:core</ta>
            <ta e="T838" id="Seg_1380" s="T837">TURK:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T754" id="Seg_1381" s="T753">medCdel finVdel</ta>
            <ta e="T807" id="Seg_1382" s="T806">finVins</ta>
            <ta e="T828" id="Seg_1383" s="T827">finVins</ta>
            <ta e="T837" id="Seg_1384" s="T836">finVins</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T754" id="Seg_1385" s="T753">dir:infl</ta>
            <ta e="T807" id="Seg_1386" s="T806">dir:bare</ta>
            <ta e="T828" id="Seg_1387" s="T827">dir:bare</ta>
            <ta e="T837" id="Seg_1388" s="T836">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS">
            <ta e="T0" id="Seg_1389" s="T824">RUS:int</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T742" id="Seg_1390" s="T738">Жили лисица и медведь.</ta>
            <ta e="T745" id="Seg_1391" s="T742">Тогда лисица говорит:</ta>
            <ta e="T749" id="Seg_1392" s="T745">«Я пойду рыбу ловить».</ta>
            <ta e="T752" id="Seg_1393" s="T749">«А как [ты]?»</ta>
            <ta e="T756" id="Seg_1394" s="T752">«Да на твой хвост много поймаю».</ta>
            <ta e="T758" id="Seg_1395" s="T756">Потом побежала.</ta>
            <ta e="T762" id="Seg_1396" s="T758">Там люди рыбу ловили.</ta>
            <ta e="T767" id="Seg_1397" s="T762">[Лисица] легла на дороге и [притворилась, что] умерла.</ta>
            <ta e="T771" id="Seg_1398" s="T767">Она лежит, приходят мужики [с санями, полными рыбы].</ta>
            <ta e="T774" id="Seg_1399" s="T771">Взяли и её тоже.</ta>
            <ta e="T776" id="Seg_1400" s="T774">Положили на рыбу.</ta>
            <ta e="T780" id="Seg_1401" s="T776">Сами пошли.</ta>
            <ta e="T788" id="Seg_1402" s="T780">Она мешок прогрызла, всю рыбу выбросила на дорогу.</ta>
            <ta e="T791" id="Seg_1403" s="T788">И сама убежала.</ta>
            <ta e="T796" id="Seg_1404" s="T791">Потом пришла домой и (?).</ta>
            <ta e="T798" id="Seg_1405" s="T796">Рыбу ест.</ta>
            <ta e="T801" id="Seg_1406" s="T798">А медведь увидел [что она]…</ta>
            <ta e="T803" id="Seg_1407" s="T801">Рыбу ест.</ta>
            <ta e="T806" id="Seg_1408" s="T803">«Пойду и я!»</ta>
            <ta e="T809" id="Seg_1409" s="T806">Хвост в воду опустил.</ta>
            <ta e="T812" id="Seg_1410" s="T809">Потом сидел, сидел.</ta>
            <ta e="T816" id="Seg_1411" s="T812">А было очень холодно.</ta>
            <ta e="T818" id="Seg_1412" s="T816">Хвост замёрз.</ta>
            <ta e="T824" id="Seg_1413" s="T818">Он говорит: «Однако много [рыбы]!»</ta>
            <ta e="T831" id="Seg_1414" s="T824">Стал он его тянуть, и хвост оторвался.</ta>
            <ta e="T837" id="Seg_1415" s="T831">И с тех пор у него нет хвоста.</ta>
            <ta e="T839" id="Seg_1416" s="T837">Весь оторвался.</ta>
            <ta e="T840" id="Seg_1417" s="T839">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T742" id="Seg_1418" s="T738">[Once] there lived a fox and a bear.</ta>
            <ta e="T745" id="Seg_1419" s="T742">Then the fox says:</ta>
            <ta e="T749" id="Seg_1420" s="T745">"I will go to catch fish."</ta>
            <ta e="T752" id="Seg_1421" s="T749">"How [will] you [do it]?"</ta>
            <ta e="T756" id="Seg_1422" s="T752">"Well, I will catch many [fish] with your tail!"</ta>
            <ta e="T758" id="Seg_1423" s="T756">Then he ran.</ta>
            <ta e="T762" id="Seg_1424" s="T758">Men were catching fish there.</ta>
            <ta e="T767" id="Seg_1425" s="T762">[The fox] lied down on the road and [pretended to] die.</ta>
            <ta e="T771" id="Seg_1426" s="T767">He is lying, men come [carrying a slegde full of fish].</ta>
            <ta e="T774" id="Seg_1427" s="T771">They took him, too.</ta>
            <ta e="T776" id="Seg_1428" s="T774">Put [him] on top of the fish.</ta>
            <ta e="T780" id="Seg_1429" s="T776">They [themselves] are going.</ta>
            <ta e="T788" id="Seg_1430" s="T780">He gnawed through the sack and threw all the fish on the road.</ta>
            <ta e="T791" id="Seg_1431" s="T788">And he [himself] ran away.</ta>
            <ta e="T796" id="Seg_1432" s="T791">Then he came home and (?).</ta>
            <ta e="T798" id="Seg_1433" s="T796">He eats fish.</ta>
            <ta e="T801" id="Seg_1434" s="T798">And the bear sees [that]…</ta>
            <ta e="T803" id="Seg_1435" s="T801">He’s eating fish.</ta>
            <ta e="T806" id="Seg_1436" s="T803">"I will go, too!"</ta>
            <ta e="T809" id="Seg_1437" s="T806">He dropped [his] tail into the water.</ta>
            <ta e="T812" id="Seg_1438" s="T809">Then he sat, he sat.</ta>
            <ta e="T816" id="Seg_1439" s="T812">And it was very cold.</ta>
            <ta e="T818" id="Seg_1440" s="T816">The tail froze.</ta>
            <ta e="T824" id="Seg_1441" s="T818">He says: "[There are] many!"</ta>
            <ta e="T831" id="Seg_1442" s="T824">He started to pull it out and tore its tail off.</ta>
            <ta e="T837" id="Seg_1443" s="T831">And since then he has no tail.</ta>
            <ta e="T839" id="Seg_1444" s="T837">He tore it all off.</ta>
            <ta e="T840" id="Seg_1445" s="T839">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T742" id="Seg_1446" s="T738">Es lebte [einmal] ein Fuchs und ein Bär.</ta>
            <ta e="T745" id="Seg_1447" s="T742">Dann sagt der Fuchs:</ta>
            <ta e="T749" id="Seg_1448" s="T745">„Ich werde Fische fangen gehen.“</ta>
            <ta e="T752" id="Seg_1449" s="T749">„Wie [wills] du [es machen]?“</ta>
            <ta e="T756" id="Seg_1450" s="T752">„Also, ich werde viele [Fische] mit deinem Schwanz fangen!“</ta>
            <ta e="T758" id="Seg_1451" s="T756">Dann lief er.</ta>
            <ta e="T762" id="Seg_1452" s="T758">Männer fangen dort Fische.</ta>
            <ta e="T767" id="Seg_1453" s="T762">[Der Fuchs] legt sich auf die Straße hin und [gab vor zu] sterben.</ta>
            <ta e="T771" id="Seg_1454" s="T767">Er liegt, Männer kommen [und tragen einen Schlitten voller Fische]</ta>
            <ta e="T774" id="Seg_1455" s="T771">Sie nahmen ihn auch.</ta>
            <ta e="T776" id="Seg_1456" s="T774">Leg [ihn] oben auf die Fische drauf.</ta>
            <ta e="T780" id="Seg_1457" s="T776">Selber gehen sie.</ta>
            <ta e="T788" id="Seg_1458" s="T780">Er knabberte den Sack durch und warf alle Fische auf die Straße.</ta>
            <ta e="T791" id="Seg_1459" s="T788">Und selber lief weg.</ta>
            <ta e="T796" id="Seg_1460" s="T791">Dann kam er nach Hause und (?).</ta>
            <ta e="T798" id="Seg_1461" s="T796">Er frisst Fisch.</ta>
            <ta e="T801" id="Seg_1462" s="T798">Und der Bär sieht [dass]…</ta>
            <ta e="T803" id="Seg_1463" s="T801">Er frisst Fisch.</ta>
            <ta e="T806" id="Seg_1464" s="T803">„Ich will auch gehen!“</ta>
            <ta e="T809" id="Seg_1465" s="T806">Er ließ [seinen] Schwanz in Wasser.</ta>
            <ta e="T812" id="Seg_1466" s="T809">Dann saß er, er saß.</ta>
            <ta e="T816" id="Seg_1467" s="T812">Und es war sehr kalt.</ta>
            <ta e="T818" id="Seg_1468" s="T816">Der Schwanz fror.</ta>
            <ta e="T824" id="Seg_1469" s="T818">Er sagt: „[Es sind] viele!“</ta>
            <ta e="T831" id="Seg_1470" s="T824">Er fing an, ihn heraus zu ziehen und riss seinen Schwanz ab.</ta>
            <ta e="T837" id="Seg_1471" s="T831">Und seit dem hat er keinen Schwanz.</ta>
            <ta e="T839" id="Seg_1472" s="T837">Er riss ihn ganz ab.</ta>
            <ta e="T840" id="Seg_1473" s="T839">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T742" id="Seg_1474" s="T738">[GVY:] A story about how the bear lost its tail. The tale is presented as a Nenets one. See e.g. http://www.skazkidetskie.ru/kak-medved-hvost-poteryal/</ta>
            <ta e="T752" id="Seg_1475" s="T749">[GVY:] dăn</ta>
            <ta e="T756" id="Seg_1476" s="T752">Wrong possessive form.</ta>
            <ta e="T774" id="Seg_1477" s="T771">[GVY:] It is not sure if there really is "i" 'too'.</ta>
            <ta e="T831" id="Seg_1478" s="T824">[KlT:] Second instance of nʼiʔdə- as a verb!</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T0" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
