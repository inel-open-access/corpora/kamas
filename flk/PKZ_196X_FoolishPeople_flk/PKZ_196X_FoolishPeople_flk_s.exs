<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID9A5CE9E0-7119-470E-A31F-9E2E70AB71A6">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_FoolishPeople_flk.wav" />
         <referenced-file url="PKZ_196X_FoolishPeople_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_FoolishPeople_flk\PKZ_196X_FoolishPeople_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">233</ud-information>
            <ud-information attribute-name="# HIAT:w">161</ud-information>
            <ud-information attribute-name="# e">161</ud-information>
            <ud-information attribute-name="# HIAT:u">35</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.06" type="appl" />
         <tli id="T1" time="1.132" type="appl" />
         <tli id="T2" time="2.203" type="appl" />
         <tli id="T3" time="3.274" type="appl" />
         <tli id="T4" time="4.106463963239659" />
         <tli id="T5" time="5.185" type="appl" />
         <tli id="T6" time="6.023" type="appl" />
         <tli id="T7" time="6.862" type="appl" />
         <tli id="T8" time="7.701" type="appl" />
         <tli id="T9" time="8.54" type="appl" />
         <tli id="T10" time="9.378" type="appl" />
         <tli id="T11" time="10.217" type="appl" />
         <tli id="T12" time="10.768" type="appl" />
         <tli id="T13" time="11.319" type="appl" />
         <tli id="T14" time="11.87" type="appl" />
         <tli id="T15" time="12.421" type="appl" />
         <tli id="T16" time="12.972" type="appl" />
         <tli id="T17" time="13.523" type="appl" />
         <tli id="T18" time="14.074" type="appl" />
         <tli id="T19" time="14.892" type="appl" />
         <tli id="T20" time="15.711" type="appl" />
         <tli id="T21" time="16.529" type="appl" />
         <tli id="T22" time="17.12" type="appl" />
         <tli id="T23" time="17.712" type="appl" />
         <tli id="T24" time="18.149" type="appl" />
         <tli id="T25" time="18.587" type="appl" />
         <tli id="T26" time="19.024" type="appl" />
         <tli id="T27" time="19.462" type="appl" />
         <tli id="T28" time="19.899" type="appl" />
         <tli id="T29" time="20.758" type="appl" />
         <tli id="T30" time="22.73221122507669" />
         <tli id="T31" time="23.614" type="appl" />
         <tli id="T32" time="24.359" type="appl" />
         <tli id="T33" time="25.103" type="appl" />
         <tli id="T34" time="25.681" type="appl" />
         <tli id="T35" time="26.26" type="appl" />
         <tli id="T36" time="26.838" type="appl" />
         <tli id="T37" time="27.416" type="appl" />
         <tli id="T38" time="27.994" type="appl" />
         <tli id="T39" time="28.573" type="appl" />
         <tli id="T40" time="29.345218126917178" />
         <tli id="T41" time="30.135" type="appl" />
         <tli id="T42" time="30.889" type="appl" />
         <tli id="T43" time="31.644" type="appl" />
         <tli id="T44" time="32.731717629069365" />
         <tli id="T45" time="33.887" type="appl" />
         <tli id="T46" time="35.17159719164357" />
         <tli id="T47" time="35.711" type="appl" />
         <tli id="T48" time="36.328" type="appl" />
         <tli id="T49" time="36.944" type="appl" />
         <tli id="T50" time="38.021" type="appl" />
         <tli id="T51" time="39.098" type="appl" />
         <tli id="T52" time="40.175" type="appl" />
         <tli id="T53" time="41.252" type="appl" />
         <tli id="T54" time="41.859" type="appl" />
         <tli id="T55" time="42.465" type="appl" />
         <tli id="T56" time="43.072" type="appl" />
         <tli id="T57" time="43.679" type="appl" />
         <tli id="T58" time="44.285" type="appl" />
         <tli id="T59" time="44.892" type="appl" />
         <tli id="T60" time="45.499" type="appl" />
         <tli id="T61" time="46.105" type="appl" />
         <tli id="T62" time="46.712" type="appl" />
         <tli id="T63" time="48.884" type="appl" />
         <tli id="T64" time="51.056" type="appl" />
         <tli id="T65" time="57.77714800226968" />
         <tli id="T66" time="58.716" type="appl" />
         <tli id="T67" time="59.544" type="appl" />
         <tli id="T68" time="60.263" type="appl" />
         <tli id="T69" time="60.983" type="appl" />
         <tli id="T70" time="61.702" type="appl" />
         <tli id="T71" time="62.422" type="appl" />
         <tli id="T72" time="63.120998802240926" />
         <tli id="T73" time="64.22" type="appl" />
         <tli id="T74" time="65.299" type="appl" />
         <tli id="T75" time="66.378" type="appl" />
         <tli id="T76" time="67.457" type="appl" />
         <tli id="T77" time="68.537" type="appl" />
         <tli id="T78" time="69.616" type="appl" />
         <tli id="T79" time="70.695" type="appl" />
         <tli id="T80" time="71.774" type="appl" />
         <tli id="T81" time="72.853" type="appl" />
         <tli id="T82" time="73.56" type="appl" />
         <tli id="T83" time="74.267" type="appl" />
         <tli id="T84" time="74.973" type="appl" />
         <tli id="T85" time="75.68" type="appl" />
         <tli id="T86" time="76.387" type="appl" />
         <tli id="T87" time="77.56" type="appl" />
         <tli id="T88" time="78.409" type="appl" />
         <tli id="T89" time="79.175" type="appl" />
         <tli id="T90" time="80.86267512028745" />
         <tli id="T91" time="81.866" type="appl" />
         <tli id="T92" time="82.667" type="appl" />
         <tli id="T93" time="83.261" type="appl" />
         <tli id="T94" time="83.855" type="appl" />
         <tli id="T95" time="84.449" type="appl" />
         <tli id="T96" time="85.043" type="appl" />
         <tli id="T97" time="85.638" type="appl" />
         <tli id="T98" time="86.232" type="appl" />
         <tli id="T99" time="86.826" type="appl" />
         <tli id="T100" time="87.42" type="appl" />
         <tli id="T101" time="88.014" type="appl" />
         <tli id="T102" time="92.46876888652162" />
         <tli id="T103" time="93.038" type="appl" />
         <tli id="T104" time="93.989" type="appl" />
         <tli id="T105" time="94.94" type="appl" />
         <tli id="T106" time="96.58189918736394" />
         <tli id="T107" time="97.639" type="appl" />
         <tli id="T108" time="98.682" type="appl" />
         <tli id="T109" time="99.726" type="appl" />
         <tli id="T110" time="101.21500382121388" />
         <tli id="T111" time="101.939" type="appl" />
         <tli id="T112" time="102.708" type="appl" />
         <tli id="T113" time="103.476" type="appl" />
         <tli id="T114" time="104.027" type="appl" />
         <tli id="T115" time="104.579" type="appl" />
         <tli id="T116" time="105.13" type="appl" />
         <tli id="T117" time="105.681" type="appl" />
         <tli id="T118" time="106.232" type="appl" />
         <tli id="T119" time="106.784" type="appl" />
         <tli id="T120" time="107.335" type="appl" />
         <tli id="T121" time="108.13" type="appl" />
         <tli id="T122" time="108.924" type="appl" />
         <tli id="T123" time="109.718" type="appl" />
         <tli id="T124" time="110.513" type="appl" />
         <tli id="T125" time="111.053" type="appl" />
         <tli id="T126" time="111.593" type="appl" />
         <tli id="T127" time="112.133" type="appl" />
         <tli id="T128" time="112.672" type="appl" />
         <tli id="T129" time="113.212" type="appl" />
         <tli id="T130" time="113.752" type="appl" />
         <tli id="T131" time="114.292" type="appl" />
         <tli id="T132" time="114.832" type="appl" />
         <tli id="T133" time="115.372" type="appl" />
         <tli id="T134" time="115.911" type="appl" />
         <tli id="T135" time="116.451" type="appl" />
         <tli id="T136" time="116.991" type="appl" />
         <tli id="T137" time="117.491" type="appl" />
         <tli id="T138" time="117.992" type="appl" />
         <tli id="T139" time="118.492" type="appl" />
         <tli id="T140" time="118.992" type="appl" />
         <tli id="T141" time="119.518" type="appl" />
         <tli id="T142" time="120.045" type="appl" />
         <tli id="T143" time="120.572" type="appl" />
         <tli id="T144" time="121.098" type="appl" />
         <tli id="T145" time="121.624" type="appl" />
         <tli id="T146" time="122.151" type="appl" />
         <tli id="T147" time="122.678" type="appl" />
         <tli id="T148" time="123.204" type="appl" />
         <tli id="T149" time="124.566" type="appl" />
         <tli id="T150" time="125.45" type="appl" />
         <tli id="T151" time="126.334" type="appl" />
         <tli id="T152" time="127.217" type="appl" />
         <tli id="T153" time="128.137" type="appl" />
         <tli id="T154" time="129.032" type="appl" />
         <tli id="T155" time="129.928" type="appl" />
         <tli id="T156" time="131.15" type="appl" />
         <tli id="T157" time="132.354" type="appl" />
         <tli id="T158" time="133.118" type="appl" />
         <tli id="T161" time="133.386125" type="intp" />
         <tli id="T159" time="133.833" type="appl" />
         <tli id="T160" time="134.74" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T160" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Amnobiʔi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">sagəštət</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">il</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">iʔgö</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">Dĭgəttə</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_21" n="HIAT:ip">(</nts>
                  <ts e="T6" id="Seg_23" n="HIAT:w" s="T5">nə-</ts>
                  <nts id="Seg_24" n="HIAT:ip">)</nts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_27" n="HIAT:w" s="T6">nʼergölaʔ</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_30" n="HIAT:w" s="T7">šobi</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_33" n="HIAT:w" s="T8">süjö</ts>
                  <nts id="Seg_34" n="HIAT:ip">,</nts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">amnobi</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">paʔinə</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_44" n="HIAT:u" s="T11">
                  <nts id="Seg_45" n="HIAT:ip">(</nts>
                  <ts e="T12" id="Seg_47" n="HIAT:w" s="T11">Dĭ-</ts>
                  <nts id="Seg_48" n="HIAT:ip">)</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_51" n="HIAT:w" s="T12">Dĭzeŋ</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_53" n="HIAT:ip">(</nts>
                  <ts e="T14" id="Seg_55" n="HIAT:w" s="T13">măn-</ts>
                  <nts id="Seg_56" n="HIAT:ip">)</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_59" n="HIAT:w" s="T14">mănliaʔi:</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_62" n="HIAT:w" s="T15">šindi</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_65" n="HIAT:w" s="T16">döbər</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_68" n="HIAT:w" s="T17">šobi</ts>
                  <nts id="Seg_69" n="HIAT:ip">?</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_72" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_74" n="HIAT:w" s="T18">Onʼiʔ</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_77" n="HIAT:w" s="T19">onʼiʔtə</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_80" n="HIAT:w" s="T20">surarlaʔbəʔjə</ts>
                  <nts id="Seg_81" n="HIAT:ip">.</nts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_84" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_86" n="HIAT:w" s="T21">Ej</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_89" n="HIAT:w" s="T22">tĭmnem</ts>
                  <nts id="Seg_90" n="HIAT:ip">.</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_93" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_95" n="HIAT:w" s="T23">Onʼiʔ</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_98" n="HIAT:w" s="T24">măndə:</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_101" n="HIAT:w" s="T25">Măn</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_104" n="HIAT:w" s="T26">ej</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_107" n="HIAT:w" s="T27">tĭmnem</ts>
                  <nts id="Seg_108" n="HIAT:ip">.</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_111" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_113" n="HIAT:w" s="T28">Dĭgəttə</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_116" n="HIAT:w" s="T29">kambiʔi</ts>
                  <nts id="Seg_117" n="HIAT:ip">.</nts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_120" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_122" n="HIAT:w" s="T30">Kuza</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_125" n="HIAT:w" s="T31">onʼiʔ</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_128" n="HIAT:w" s="T32">kubiʔi</ts>
                  <nts id="Seg_129" n="HIAT:ip">.</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_132" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_134" n="HIAT:w" s="T33">Măndə:</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_137" n="HIAT:w" s="T34">Dĭ</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_140" n="HIAT:w" s="T35">kujo</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_142" n="HIAT:ip">(</nts>
                  <ts e="T37" id="Seg_144" n="HIAT:w" s="T36">kuga-</ts>
                  <nts id="Seg_145" n="HIAT:ip">)</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_148" n="HIAT:w" s="T37">dĭ</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_151" n="HIAT:w" s="T38">kudaj</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_154" n="HIAT:w" s="T39">šobi</ts>
                  <nts id="Seg_155" n="HIAT:ip">.</nts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_158" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_160" n="HIAT:w" s="T40">Dĭzeŋ:</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_163" n="HIAT:w" s="T41">nada</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_166" n="HIAT:w" s="T42">bădəsʼtə</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_169" n="HIAT:w" s="T43">dĭm</ts>
                  <nts id="Seg_170" n="HIAT:ip">.</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_173" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_175" n="HIAT:w" s="T44">Lʼepʼoškaʔi</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_178" n="HIAT:w" s="T45">deʔpiʔi</ts>
                  <nts id="Seg_179" n="HIAT:ip">.</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_182" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_184" n="HIAT:w" s="T46">Išo</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_187" n="HIAT:w" s="T47">ĭmbidə</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_190" n="HIAT:w" s="T48">deʔpiʔi</ts>
                  <nts id="Seg_191" n="HIAT:ip">.</nts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_194" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_196" n="HIAT:w" s="T49">Măndə:</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_199" n="HIAT:w" s="T50">Mĭʔ</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_202" n="HIAT:w" s="T51">dĭn</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_205" n="HIAT:w" s="T52">amorzittə</ts>
                  <nts id="Seg_206" n="HIAT:ip">.</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_209" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_211" n="HIAT:w" s="T53">Dĭ</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_214" n="HIAT:w" s="T54">măndə:</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_217" n="HIAT:w" s="T55">Măn</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_220" n="HIAT:w" s="T56">pimniem</ts>
                  <nts id="Seg_221" n="HIAT:ip">,</nts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_224" n="HIAT:w" s="T57">dĭ</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_227" n="HIAT:w" s="T58">măna</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_230" n="HIAT:w" s="T59">išo</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_233" n="HIAT:w" s="T60">kutləj</ts>
                  <nts id="Seg_234" n="HIAT:ip">,</nts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_237" n="HIAT:w" s="T61">kuroləj</ts>
                  <nts id="Seg_238" n="HIAT:ip">.</nts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_241" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_243" n="HIAT:w" s="T62">Dĭgəttə:</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_246" n="HIAT:w" s="T63">Deʔkeʔ</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_248" n="HIAT:ip">(</nts>
                  <ts e="T65" id="Seg_250" n="HIAT:w" s="T64">ek-</ts>
                  <nts id="Seg_251" n="HIAT:ip">)</nts>
                  <nts id="Seg_252" n="HIAT:ip">!</nts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_255" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_257" n="HIAT:w" s="T65">Deʔkeʔ</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_260" n="HIAT:w" s="T66">teʔme</ts>
                  <nts id="Seg_261" n="HIAT:ip">.</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_264" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_266" n="HIAT:w" s="T67">Da</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_269" n="HIAT:w" s="T68">măna</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_271" n="HIAT:ip">(</nts>
                  <ts e="T70" id="Seg_273" n="HIAT:w" s="T69">üjü-</ts>
                  <nts id="Seg_274" n="HIAT:ip">)</nts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_277" n="HIAT:w" s="T70">üjüzeŋdə</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_280" n="HIAT:w" s="T71">sargaʔ</ts>
                  <nts id="Seg_281" n="HIAT:ip">.</nts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_284" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_286" n="HIAT:w" s="T72">Kamən</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_289" n="HIAT:w" s="T73">dĭ</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_292" n="HIAT:w" s="T74">măna</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_295" n="HIAT:w" s="T75">toʔnarləj</ts>
                  <nts id="Seg_296" n="HIAT:ip">,</nts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_299" n="HIAT:w" s="T76">šiʔ</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_302" n="HIAT:w" s="T77">măna</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_305" n="HIAT:w" s="T78">üjütsiʔ</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_308" n="HIAT:w" s="T79">dʼünə</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_311" n="HIAT:w" s="T80">deʔkeʔ</ts>
                  <nts id="Seg_312" n="HIAT:ip">.</nts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_315" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_317" n="HIAT:w" s="T81">Dĭgəttə</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_320" n="HIAT:w" s="T82">dĭ</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_323" n="HIAT:w" s="T83">dĭbər</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_325" n="HIAT:ip">(</nts>
                  <ts e="T85" id="Seg_327" n="HIAT:w" s="T84">sa-</ts>
                  <nts id="Seg_328" n="HIAT:ip">)</nts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_331" n="HIAT:w" s="T85">sʼabi</ts>
                  <nts id="Seg_332" n="HIAT:ip">.</nts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_335" n="HIAT:u" s="T86">
                  <ts e="T87" id="Seg_337" n="HIAT:w" s="T86">Süjö</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_340" n="HIAT:w" s="T87">nʼergölüʔpi</ts>
                  <nts id="Seg_341" n="HIAT:ip">.</nts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_344" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_346" n="HIAT:w" s="T88">Dĭ</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_349" n="HIAT:w" s="T89">kirgarlaʔbə:</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_352" n="HIAT:w" s="T90">Igeʔ</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_355" n="HIAT:w" s="T91">măna</ts>
                  <nts id="Seg_356" n="HIAT:ip">!</nts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_359" n="HIAT:u" s="T92">
                  <nts id="Seg_360" n="HIAT:ip">(</nts>
                  <ts e="T93" id="Seg_362" n="HIAT:w" s="T92">Dĭ-</ts>
                  <nts id="Seg_363" n="HIAT:ip">)</nts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_366" n="HIAT:w" s="T93">Dĭzeŋ</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_369" n="HIAT:w" s="T94">bar</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_372" n="HIAT:w" s="T95">dĭm</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_374" n="HIAT:ip">(</nts>
                  <ts e="T97" id="Seg_376" n="HIAT:w" s="T96">s-</ts>
                  <nts id="Seg_377" n="HIAT:ip">)</nts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_380" n="HIAT:w" s="T97">saj</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_382" n="HIAT:ip">(</nts>
                  <ts e="T99" id="Seg_384" n="HIAT:w" s="T98">uju-</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_387" n="HIAT:w" s="T99">ulu-</ts>
                  <nts id="Seg_388" n="HIAT:ip">)</nts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_391" n="HIAT:w" s="T100">ulut</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_394" n="HIAT:w" s="T101">bar</ts>
                  <nts id="Seg_395" n="HIAT:ip">.</nts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_398" n="HIAT:u" s="T102">
                  <ts e="T103" id="Seg_400" n="HIAT:w" s="T102">Dĭgəttə</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_403" n="HIAT:w" s="T103">dĭzeŋ</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_406" n="HIAT:w" s="T104">nʼeʔpiʔi</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_409" n="HIAT:w" s="T105">üjüttə</ts>
                  <nts id="Seg_410" n="HIAT:ip">.</nts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_413" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_415" n="HIAT:w" s="T106">Ulut</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_418" n="HIAT:w" s="T107">maːluʔpi</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_421" n="HIAT:w" s="T108">bar</ts>
                  <nts id="Seg_422" n="HIAT:ip">,</nts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_425" n="HIAT:w" s="T109">sajnʼeʔluʔpiʔi</ts>
                  <nts id="Seg_426" n="HIAT:ip">.</nts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_429" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_431" n="HIAT:w" s="T110">Dĭ</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_434" n="HIAT:w" s="T111">dʼünə</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_437" n="HIAT:w" s="T112">saʔməluʔpiʔi</ts>
                  <nts id="Seg_438" n="HIAT:ip">.</nts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_441" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_443" n="HIAT:w" s="T113">Dĭgəttə</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_446" n="HIAT:w" s="T114">mălliaʔi:</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_449" n="HIAT:w" s="T115">Dĭn</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_452" n="HIAT:w" s="T116">ibi</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_455" n="HIAT:w" s="T117">ulut</ts>
                  <nts id="Seg_456" n="HIAT:ip">,</nts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_459" n="HIAT:w" s="T118">alʼi</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_462" n="HIAT:w" s="T119">nagobiʔi</ts>
                  <nts id="Seg_463" n="HIAT:ip">?</nts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_466" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_468" n="HIAT:w" s="T120">Dĭgəttə</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_471" n="HIAT:w" s="T121">dĭn</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_474" n="HIAT:w" s="T122">nem</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_476" n="HIAT:ip">(</nts>
                  <ts e="T124" id="Seg_478" n="HIAT:w" s="T123">surarluʔpiʔi</ts>
                  <nts id="Seg_479" n="HIAT:ip">)</nts>
                  <nts id="Seg_480" n="HIAT:ip">.</nts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_483" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_485" n="HIAT:w" s="T124">A</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_488" n="HIAT:w" s="T125">măn</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_491" n="HIAT:w" s="T126">ej</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_494" n="HIAT:w" s="T127">tĭmnem</ts>
                  <nts id="Seg_495" n="HIAT:ip">,</nts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_498" n="HIAT:w" s="T128">kamən</ts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_501" n="HIAT:w" s="T129">dĭ</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_503" n="HIAT:ip">(</nts>
                  <ts e="T131" id="Seg_505" n="HIAT:w" s="T130">amorbi</ts>
                  <nts id="Seg_506" n="HIAT:ip">)</nts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_509" n="HIAT:w" s="T131">dak</ts>
                  <nts id="Seg_510" n="HIAT:ip">,</nts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_513" n="HIAT:w" s="T132">dĭn</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_516" n="HIAT:w" s="T133">muʔseŋdə</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_519" n="HIAT:w" s="T134">bar</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_522" n="HIAT:w" s="T135">sădərbiʔi</ts>
                  <nts id="Seg_523" n="HIAT:ip">.</nts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_526" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_528" n="HIAT:w" s="T136">A</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_531" n="HIAT:w" s="T137">tüj</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_534" n="HIAT:w" s="T138">ej</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_537" n="HIAT:w" s="T139">tĭmnem</ts>
                  <nts id="Seg_538" n="HIAT:ip">.</nts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_541" n="HIAT:u" s="T140">
                  <ts e="T141" id="Seg_543" n="HIAT:w" s="T140">Dĭgəttə</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_546" n="HIAT:w" s="T141">dibər</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_548" n="HIAT:ip">(</nts>
                  <ts e="T143" id="Seg_550" n="HIAT:w" s="T142">sa-</ts>
                  <nts id="Seg_551" n="HIAT:ip">)</nts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_554" n="HIAT:w" s="T143">sʼabiʔi</ts>
                  <nts id="Seg_555" n="HIAT:ip">,</nts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_558" n="HIAT:w" s="T144">a</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_561" n="HIAT:w" s="T145">ulut</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_564" n="HIAT:w" s="T146">dĭn</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_567" n="HIAT:w" s="T147">amnolaʔbə</ts>
                  <nts id="Seg_568" n="HIAT:ip">.</nts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_571" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_573" n="HIAT:w" s="T148">Dĭzeŋ</ts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_576" n="HIAT:w" s="T149">deʔpiʔi</ts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_579" n="HIAT:w" s="T150">ulut</ts>
                  <nts id="Seg_580" n="HIAT:ip">,</nts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_583" n="HIAT:w" s="T151">embiʔi</ts>
                  <nts id="Seg_584" n="HIAT:ip">.</nts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T155" id="Seg_587" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_589" n="HIAT:w" s="T152">Dʼüm</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_592" n="HIAT:w" s="T153">tĭlbiʔi</ts>
                  <nts id="Seg_593" n="HIAT:ip">,</nts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_596" n="HIAT:w" s="T154">embiʔi</ts>
                  <nts id="Seg_597" n="HIAT:ip">.</nts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_600" n="HIAT:u" s="T155">
                  <ts e="T156" id="Seg_602" n="HIAT:w" s="T155">Tĭlluʔpiʔi</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_605" n="HIAT:w" s="T156">dĭm</ts>
                  <nts id="Seg_606" n="HIAT:ip">.</nts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T159" id="Seg_609" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_611" n="HIAT:w" s="T157">I</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_614" n="HIAT:w" s="T158">kalla</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_617" n="HIAT:w" s="T161">dʼürbiʔi</ts>
                  <nts id="Seg_618" n="HIAT:ip">.</nts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_621" n="HIAT:u" s="T159">
                  <ts e="T160" id="Seg_623" n="HIAT:w" s="T159">Kabarləj</ts>
                  <nts id="Seg_624" n="HIAT:ip">.</nts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T160" id="Seg_626" n="sc" s="T0">
               <ts e="T1" id="Seg_628" n="e" s="T0">Amnobiʔi </ts>
               <ts e="T2" id="Seg_630" n="e" s="T1">sagəštət </ts>
               <ts e="T3" id="Seg_632" n="e" s="T2">il </ts>
               <ts e="T4" id="Seg_634" n="e" s="T3">iʔgö. </ts>
               <ts e="T5" id="Seg_636" n="e" s="T4">Dĭgəttə </ts>
               <ts e="T6" id="Seg_638" n="e" s="T5">(nə-) </ts>
               <ts e="T7" id="Seg_640" n="e" s="T6">nʼergölaʔ </ts>
               <ts e="T8" id="Seg_642" n="e" s="T7">šobi </ts>
               <ts e="T9" id="Seg_644" n="e" s="T8">süjö, </ts>
               <ts e="T10" id="Seg_646" n="e" s="T9">amnobi </ts>
               <ts e="T11" id="Seg_648" n="e" s="T10">paʔinə. </ts>
               <ts e="T12" id="Seg_650" n="e" s="T11">(Dĭ-) </ts>
               <ts e="T13" id="Seg_652" n="e" s="T12">Dĭzeŋ </ts>
               <ts e="T14" id="Seg_654" n="e" s="T13">(măn-) </ts>
               <ts e="T15" id="Seg_656" n="e" s="T14">mănliaʔi: </ts>
               <ts e="T16" id="Seg_658" n="e" s="T15">šindi </ts>
               <ts e="T17" id="Seg_660" n="e" s="T16">döbər </ts>
               <ts e="T18" id="Seg_662" n="e" s="T17">šobi? </ts>
               <ts e="T19" id="Seg_664" n="e" s="T18">Onʼiʔ </ts>
               <ts e="T20" id="Seg_666" n="e" s="T19">onʼiʔtə </ts>
               <ts e="T21" id="Seg_668" n="e" s="T20">surarlaʔbəʔjə. </ts>
               <ts e="T22" id="Seg_670" n="e" s="T21">Ej </ts>
               <ts e="T23" id="Seg_672" n="e" s="T22">tĭmnem. </ts>
               <ts e="T24" id="Seg_674" n="e" s="T23">Onʼiʔ </ts>
               <ts e="T25" id="Seg_676" n="e" s="T24">măndə: </ts>
               <ts e="T26" id="Seg_678" n="e" s="T25">Măn </ts>
               <ts e="T27" id="Seg_680" n="e" s="T26">ej </ts>
               <ts e="T28" id="Seg_682" n="e" s="T27">tĭmnem. </ts>
               <ts e="T29" id="Seg_684" n="e" s="T28">Dĭgəttə </ts>
               <ts e="T30" id="Seg_686" n="e" s="T29">kambiʔi. </ts>
               <ts e="T31" id="Seg_688" n="e" s="T30">Kuza </ts>
               <ts e="T32" id="Seg_690" n="e" s="T31">onʼiʔ </ts>
               <ts e="T33" id="Seg_692" n="e" s="T32">kubiʔi. </ts>
               <ts e="T34" id="Seg_694" n="e" s="T33">Măndə: </ts>
               <ts e="T35" id="Seg_696" n="e" s="T34">Dĭ </ts>
               <ts e="T36" id="Seg_698" n="e" s="T35">kujo </ts>
               <ts e="T37" id="Seg_700" n="e" s="T36">(kuga-) </ts>
               <ts e="T38" id="Seg_702" n="e" s="T37">dĭ </ts>
               <ts e="T39" id="Seg_704" n="e" s="T38">kudaj </ts>
               <ts e="T40" id="Seg_706" n="e" s="T39">šobi. </ts>
               <ts e="T41" id="Seg_708" n="e" s="T40">Dĭzeŋ: </ts>
               <ts e="T42" id="Seg_710" n="e" s="T41">nada </ts>
               <ts e="T43" id="Seg_712" n="e" s="T42">bădəsʼtə </ts>
               <ts e="T44" id="Seg_714" n="e" s="T43">dĭm. </ts>
               <ts e="T45" id="Seg_716" n="e" s="T44">Lʼepʼoškaʔi </ts>
               <ts e="T46" id="Seg_718" n="e" s="T45">deʔpiʔi. </ts>
               <ts e="T47" id="Seg_720" n="e" s="T46">Išo </ts>
               <ts e="T48" id="Seg_722" n="e" s="T47">ĭmbidə </ts>
               <ts e="T49" id="Seg_724" n="e" s="T48">deʔpiʔi. </ts>
               <ts e="T50" id="Seg_726" n="e" s="T49">Măndə: </ts>
               <ts e="T51" id="Seg_728" n="e" s="T50">Mĭʔ </ts>
               <ts e="T52" id="Seg_730" n="e" s="T51">dĭn </ts>
               <ts e="T53" id="Seg_732" n="e" s="T52">amorzittə. </ts>
               <ts e="T54" id="Seg_734" n="e" s="T53">Dĭ </ts>
               <ts e="T55" id="Seg_736" n="e" s="T54">măndə: </ts>
               <ts e="T56" id="Seg_738" n="e" s="T55">Măn </ts>
               <ts e="T57" id="Seg_740" n="e" s="T56">pimniem, </ts>
               <ts e="T58" id="Seg_742" n="e" s="T57">dĭ </ts>
               <ts e="T59" id="Seg_744" n="e" s="T58">măna </ts>
               <ts e="T60" id="Seg_746" n="e" s="T59">išo </ts>
               <ts e="T61" id="Seg_748" n="e" s="T60">kutləj, </ts>
               <ts e="T62" id="Seg_750" n="e" s="T61">kuroləj. </ts>
               <ts e="T63" id="Seg_752" n="e" s="T62">Dĭgəttə: </ts>
               <ts e="T64" id="Seg_754" n="e" s="T63">Deʔkeʔ </ts>
               <ts e="T65" id="Seg_756" n="e" s="T64">(ek-)! </ts>
               <ts e="T66" id="Seg_758" n="e" s="T65">Deʔkeʔ </ts>
               <ts e="T67" id="Seg_760" n="e" s="T66">teʔme. </ts>
               <ts e="T68" id="Seg_762" n="e" s="T67">Da </ts>
               <ts e="T69" id="Seg_764" n="e" s="T68">măna </ts>
               <ts e="T70" id="Seg_766" n="e" s="T69">(üjü-) </ts>
               <ts e="T71" id="Seg_768" n="e" s="T70">üjüzeŋdə </ts>
               <ts e="T72" id="Seg_770" n="e" s="T71">sargaʔ. </ts>
               <ts e="T73" id="Seg_772" n="e" s="T72">Kamən </ts>
               <ts e="T74" id="Seg_774" n="e" s="T73">dĭ </ts>
               <ts e="T75" id="Seg_776" n="e" s="T74">măna </ts>
               <ts e="T76" id="Seg_778" n="e" s="T75">toʔnarləj, </ts>
               <ts e="T77" id="Seg_780" n="e" s="T76">šiʔ </ts>
               <ts e="T78" id="Seg_782" n="e" s="T77">măna </ts>
               <ts e="T79" id="Seg_784" n="e" s="T78">üjütsiʔ </ts>
               <ts e="T80" id="Seg_786" n="e" s="T79">dʼünə </ts>
               <ts e="T81" id="Seg_788" n="e" s="T80">deʔkeʔ. </ts>
               <ts e="T82" id="Seg_790" n="e" s="T81">Dĭgəttə </ts>
               <ts e="T83" id="Seg_792" n="e" s="T82">dĭ </ts>
               <ts e="T84" id="Seg_794" n="e" s="T83">dĭbər </ts>
               <ts e="T85" id="Seg_796" n="e" s="T84">(sa-) </ts>
               <ts e="T86" id="Seg_798" n="e" s="T85">sʼabi. </ts>
               <ts e="T87" id="Seg_800" n="e" s="T86">Süjö </ts>
               <ts e="T88" id="Seg_802" n="e" s="T87">nʼergölüʔpi. </ts>
               <ts e="T89" id="Seg_804" n="e" s="T88">Dĭ </ts>
               <ts e="T90" id="Seg_806" n="e" s="T89">kirgarlaʔbə: </ts>
               <ts e="T91" id="Seg_808" n="e" s="T90">Igeʔ </ts>
               <ts e="T92" id="Seg_810" n="e" s="T91">măna! </ts>
               <ts e="T93" id="Seg_812" n="e" s="T92">(Dĭ-) </ts>
               <ts e="T94" id="Seg_814" n="e" s="T93">Dĭzeŋ </ts>
               <ts e="T95" id="Seg_816" n="e" s="T94">bar </ts>
               <ts e="T96" id="Seg_818" n="e" s="T95">dĭm </ts>
               <ts e="T97" id="Seg_820" n="e" s="T96">(s-) </ts>
               <ts e="T98" id="Seg_822" n="e" s="T97">saj </ts>
               <ts e="T99" id="Seg_824" n="e" s="T98">(uju- </ts>
               <ts e="T100" id="Seg_826" n="e" s="T99">ulu-) </ts>
               <ts e="T101" id="Seg_828" n="e" s="T100">ulut </ts>
               <ts e="T102" id="Seg_830" n="e" s="T101">bar. </ts>
               <ts e="T103" id="Seg_832" n="e" s="T102">Dĭgəttə </ts>
               <ts e="T104" id="Seg_834" n="e" s="T103">dĭzeŋ </ts>
               <ts e="T105" id="Seg_836" n="e" s="T104">nʼeʔpiʔi </ts>
               <ts e="T106" id="Seg_838" n="e" s="T105">üjüttə. </ts>
               <ts e="T107" id="Seg_840" n="e" s="T106">Ulut </ts>
               <ts e="T108" id="Seg_842" n="e" s="T107">maːluʔpi </ts>
               <ts e="T109" id="Seg_844" n="e" s="T108">bar, </ts>
               <ts e="T110" id="Seg_846" n="e" s="T109">sajnʼeʔluʔpiʔi. </ts>
               <ts e="T111" id="Seg_848" n="e" s="T110">Dĭ </ts>
               <ts e="T112" id="Seg_850" n="e" s="T111">dʼünə </ts>
               <ts e="T113" id="Seg_852" n="e" s="T112">saʔməluʔpiʔi. </ts>
               <ts e="T114" id="Seg_854" n="e" s="T113">Dĭgəttə </ts>
               <ts e="T115" id="Seg_856" n="e" s="T114">mălliaʔi: </ts>
               <ts e="T116" id="Seg_858" n="e" s="T115">Dĭn </ts>
               <ts e="T117" id="Seg_860" n="e" s="T116">ibi </ts>
               <ts e="T118" id="Seg_862" n="e" s="T117">ulut, </ts>
               <ts e="T119" id="Seg_864" n="e" s="T118">alʼi </ts>
               <ts e="T120" id="Seg_866" n="e" s="T119">nagobiʔi? </ts>
               <ts e="T121" id="Seg_868" n="e" s="T120">Dĭgəttə </ts>
               <ts e="T122" id="Seg_870" n="e" s="T121">dĭn </ts>
               <ts e="T123" id="Seg_872" n="e" s="T122">nem </ts>
               <ts e="T124" id="Seg_874" n="e" s="T123">(surarluʔpiʔi). </ts>
               <ts e="T125" id="Seg_876" n="e" s="T124">A </ts>
               <ts e="T126" id="Seg_878" n="e" s="T125">măn </ts>
               <ts e="T127" id="Seg_880" n="e" s="T126">ej </ts>
               <ts e="T128" id="Seg_882" n="e" s="T127">tĭmnem, </ts>
               <ts e="T129" id="Seg_884" n="e" s="T128">kamən </ts>
               <ts e="T130" id="Seg_886" n="e" s="T129">dĭ </ts>
               <ts e="T131" id="Seg_888" n="e" s="T130">(amorbi) </ts>
               <ts e="T132" id="Seg_890" n="e" s="T131">dak, </ts>
               <ts e="T133" id="Seg_892" n="e" s="T132">dĭn </ts>
               <ts e="T134" id="Seg_894" n="e" s="T133">muʔseŋdə </ts>
               <ts e="T135" id="Seg_896" n="e" s="T134">bar </ts>
               <ts e="T136" id="Seg_898" n="e" s="T135">sădərbiʔi. </ts>
               <ts e="T137" id="Seg_900" n="e" s="T136">A </ts>
               <ts e="T138" id="Seg_902" n="e" s="T137">tüj </ts>
               <ts e="T139" id="Seg_904" n="e" s="T138">ej </ts>
               <ts e="T140" id="Seg_906" n="e" s="T139">tĭmnem. </ts>
               <ts e="T141" id="Seg_908" n="e" s="T140">Dĭgəttə </ts>
               <ts e="T142" id="Seg_910" n="e" s="T141">dibər </ts>
               <ts e="T143" id="Seg_912" n="e" s="T142">(sa-) </ts>
               <ts e="T144" id="Seg_914" n="e" s="T143">sʼabiʔi, </ts>
               <ts e="T145" id="Seg_916" n="e" s="T144">a </ts>
               <ts e="T146" id="Seg_918" n="e" s="T145">ulut </ts>
               <ts e="T147" id="Seg_920" n="e" s="T146">dĭn </ts>
               <ts e="T148" id="Seg_922" n="e" s="T147">amnolaʔbə. </ts>
               <ts e="T149" id="Seg_924" n="e" s="T148">Dĭzeŋ </ts>
               <ts e="T150" id="Seg_926" n="e" s="T149">deʔpiʔi </ts>
               <ts e="T151" id="Seg_928" n="e" s="T150">ulut, </ts>
               <ts e="T152" id="Seg_930" n="e" s="T151">embiʔi. </ts>
               <ts e="T153" id="Seg_932" n="e" s="T152">Dʼüm </ts>
               <ts e="T154" id="Seg_934" n="e" s="T153">tĭlbiʔi, </ts>
               <ts e="T155" id="Seg_936" n="e" s="T154">embiʔi. </ts>
               <ts e="T156" id="Seg_938" n="e" s="T155">Tĭlluʔpiʔi </ts>
               <ts e="T157" id="Seg_940" n="e" s="T156">dĭm. </ts>
               <ts e="T158" id="Seg_942" n="e" s="T157">I </ts>
               <ts e="T161" id="Seg_944" n="e" s="T158">kalla </ts>
               <ts e="T159" id="Seg_946" n="e" s="T161">dʼürbiʔi. </ts>
               <ts e="T160" id="Seg_948" n="e" s="T159">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_949" s="T0">PKZ_196X_FoolishPeople_flk.001 (002)</ta>
            <ta e="T11" id="Seg_950" s="T4">PKZ_196X_FoolishPeople_flk.002 (003)</ta>
            <ta e="T18" id="Seg_951" s="T11">PKZ_196X_FoolishPeople_flk.003 (004)</ta>
            <ta e="T21" id="Seg_952" s="T18">PKZ_196X_FoolishPeople_flk.004 (005)</ta>
            <ta e="T23" id="Seg_953" s="T21">PKZ_196X_FoolishPeople_flk.005 (006)</ta>
            <ta e="T28" id="Seg_954" s="T23">PKZ_196X_FoolishPeople_flk.006 (007)</ta>
            <ta e="T30" id="Seg_955" s="T28">PKZ_196X_FoolishPeople_flk.007 (008)</ta>
            <ta e="T33" id="Seg_956" s="T30">PKZ_196X_FoolishPeople_flk.008 (009)</ta>
            <ta e="T40" id="Seg_957" s="T33">PKZ_196X_FoolishPeople_flk.009 (010)</ta>
            <ta e="T44" id="Seg_958" s="T40">PKZ_196X_FoolishPeople_flk.010 (011)</ta>
            <ta e="T46" id="Seg_959" s="T44">PKZ_196X_FoolishPeople_flk.011 (012)</ta>
            <ta e="T49" id="Seg_960" s="T46">PKZ_196X_FoolishPeople_flk.012 (013)</ta>
            <ta e="T53" id="Seg_961" s="T49">PKZ_196X_FoolishPeople_flk.013 (014)</ta>
            <ta e="T62" id="Seg_962" s="T53">PKZ_196X_FoolishPeople_flk.014 (015)</ta>
            <ta e="T65" id="Seg_963" s="T62">PKZ_196X_FoolishPeople_flk.015 (016)</ta>
            <ta e="T67" id="Seg_964" s="T65">PKZ_196X_FoolishPeople_flk.016 (017)</ta>
            <ta e="T72" id="Seg_965" s="T67">PKZ_196X_FoolishPeople_flk.017 (018)</ta>
            <ta e="T81" id="Seg_966" s="T72">PKZ_196X_FoolishPeople_flk.018 (019)</ta>
            <ta e="T86" id="Seg_967" s="T81">PKZ_196X_FoolishPeople_flk.019 (020)</ta>
            <ta e="T88" id="Seg_968" s="T86">PKZ_196X_FoolishPeople_flk.020 (021)</ta>
            <ta e="T92" id="Seg_969" s="T88">PKZ_196X_FoolishPeople_flk.021 (022)</ta>
            <ta e="T102" id="Seg_970" s="T92">PKZ_196X_FoolishPeople_flk.022 (024)</ta>
            <ta e="T106" id="Seg_971" s="T102">PKZ_196X_FoolishPeople_flk.023 (025)</ta>
            <ta e="T110" id="Seg_972" s="T106">PKZ_196X_FoolishPeople_flk.024 (026)</ta>
            <ta e="T113" id="Seg_973" s="T110">PKZ_196X_FoolishPeople_flk.025 (027)</ta>
            <ta e="T120" id="Seg_974" s="T113">PKZ_196X_FoolishPeople_flk.026 (028)</ta>
            <ta e="T124" id="Seg_975" s="T120">PKZ_196X_FoolishPeople_flk.027 (029)</ta>
            <ta e="T136" id="Seg_976" s="T124">PKZ_196X_FoolishPeople_flk.028 (030)</ta>
            <ta e="T140" id="Seg_977" s="T136">PKZ_196X_FoolishPeople_flk.029 (031)</ta>
            <ta e="T148" id="Seg_978" s="T140">PKZ_196X_FoolishPeople_flk.030 (032)</ta>
            <ta e="T152" id="Seg_979" s="T148">PKZ_196X_FoolishPeople_flk.031 (033)</ta>
            <ta e="T155" id="Seg_980" s="T152">PKZ_196X_FoolishPeople_flk.032 (034)</ta>
            <ta e="T157" id="Seg_981" s="T155">PKZ_196X_FoolishPeople_flk.033 (035)</ta>
            <ta e="T159" id="Seg_982" s="T157">PKZ_196X_FoolishPeople_flk.034 (036)</ta>
            <ta e="T160" id="Seg_983" s="T159">PKZ_196X_FoolishPeople_flk.035 (037)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_984" s="T0">Amnobiʔi sagəštət il iʔgö. </ta>
            <ta e="T11" id="Seg_985" s="T4">Dĭgəttə (nə-) nʼergölaʔ šobi süjö, amnobi paʔinə. </ta>
            <ta e="T18" id="Seg_986" s="T11">(Dĭ-) Dĭzeŋ (măn-) mănliaʔi: šindi döbər šobi? </ta>
            <ta e="T21" id="Seg_987" s="T18">Onʼiʔ onʼiʔtə surarlaʔbəʔjə. </ta>
            <ta e="T23" id="Seg_988" s="T21">"Ej tĭmnem". </ta>
            <ta e="T28" id="Seg_989" s="T23">Onʼiʔ măndə: "Măn ej tĭmnem". </ta>
            <ta e="T30" id="Seg_990" s="T28">Dĭgəttə kambiʔi. </ta>
            <ta e="T33" id="Seg_991" s="T30">Kuza onʼiʔ kubiʔi. </ta>
            <ta e="T40" id="Seg_992" s="T33">Măndə:" Dĭ kujo (kuga-) dĭ kudaj šobi". </ta>
            <ta e="T44" id="Seg_993" s="T40">Dĭzeŋ: nada bădəsʼtə dĭm. </ta>
            <ta e="T46" id="Seg_994" s="T44">Lʼepʼoškaʔi deʔpiʔi. </ta>
            <ta e="T49" id="Seg_995" s="T46">Išo ĭmbidə deʔpiʔi. </ta>
            <ta e="T53" id="Seg_996" s="T49">Măndə:" Mĭʔ dĭn amorzittə". </ta>
            <ta e="T62" id="Seg_997" s="T53">Dĭ măndə:" Măn pimniem, dĭ măna išo kutləj, kuroləj". </ta>
            <ta e="T65" id="Seg_998" s="T62">Dĭgəttə:" Deʔkeʔ (ek-)! </ta>
            <ta e="T67" id="Seg_999" s="T65">Deʔkeʔ teʔme. </ta>
            <ta e="T72" id="Seg_1000" s="T67">Da măna (üjü-) üjüzeŋdə sargaʔ. </ta>
            <ta e="T81" id="Seg_1001" s="T72">Kamən dĭ măna toʔnarləj, šiʔ măna üjütsiʔ dʼünə deʔkeʔ. </ta>
            <ta e="T86" id="Seg_1002" s="T81">Dĭgəttə dĭ dĭbər (sa-) sʼabi. </ta>
            <ta e="T88" id="Seg_1003" s="T86">Süjö nʼergölüʔpi. </ta>
            <ta e="T92" id="Seg_1004" s="T88">Dĭ kirgarlaʔbə: "Igeʔ măna!" </ta>
            <ta e="T102" id="Seg_1005" s="T92">(Dĭ-) Dĭzeŋ bar dĭm (s-) saj (uju- ulu-) ulut bar. </ta>
            <ta e="T106" id="Seg_1006" s="T102">Dĭgəttə dĭzeŋ nʼeʔpiʔi üjüttə. </ta>
            <ta e="T110" id="Seg_1007" s="T106">Ulut maːluʔpi bar, sajnʼeʔluʔpiʔi. </ta>
            <ta e="T113" id="Seg_1008" s="T110">Dĭ dʼünə saʔməluʔpiʔi. </ta>
            <ta e="T120" id="Seg_1009" s="T113">Dĭgəttə mălliaʔi:" Dĭn ibi ulut, alʼi nagobiʔi?" </ta>
            <ta e="T124" id="Seg_1010" s="T120">Dĭgəttə dĭn nem (surarluʔpiʔi). </ta>
            <ta e="T136" id="Seg_1011" s="T124">"A măn ej tĭmnem, kamən dĭ (amorbi) dak, dĭn muʔseŋdə bar sădərbiʔi. </ta>
            <ta e="T140" id="Seg_1012" s="T136">A tüj ej tĭmnem". </ta>
            <ta e="T148" id="Seg_1013" s="T140">Dĭgəttə dibər (sa-) sʼabiʔi, a ulut dĭn amnolaʔbə. </ta>
            <ta e="T152" id="Seg_1014" s="T148">Dĭzeŋ deʔpiʔi ulut, embiʔi. </ta>
            <ta e="T155" id="Seg_1015" s="T152">Dʼüm tĭlbiʔi, embiʔi. </ta>
            <ta e="T157" id="Seg_1016" s="T155">Tĭlluʔpiʔi dĭm. </ta>
            <ta e="T159" id="Seg_1017" s="T157">I kalla dʼürbiʔi. </ta>
            <ta e="T160" id="Seg_1018" s="T159">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1019" s="T0">amno-bi-ʔi</ta>
            <ta e="T2" id="Seg_1020" s="T1">sagəštə-t</ta>
            <ta e="T3" id="Seg_1021" s="T2">il</ta>
            <ta e="T4" id="Seg_1022" s="T3">iʔgö</ta>
            <ta e="T5" id="Seg_1023" s="T4">dĭgəttə</ta>
            <ta e="T7" id="Seg_1024" s="T6">nʼergö-laʔ</ta>
            <ta e="T8" id="Seg_1025" s="T7">šo-bi</ta>
            <ta e="T9" id="Seg_1026" s="T8">süjö</ta>
            <ta e="T10" id="Seg_1027" s="T9">amno-bi</ta>
            <ta e="T11" id="Seg_1028" s="T10">pa-ʔi-nə</ta>
            <ta e="T13" id="Seg_1029" s="T12">dĭ-zeŋ</ta>
            <ta e="T15" id="Seg_1030" s="T14">măn-lia-ʔi</ta>
            <ta e="T16" id="Seg_1031" s="T15">šindi</ta>
            <ta e="T17" id="Seg_1032" s="T16">döbər</ta>
            <ta e="T18" id="Seg_1033" s="T17">šo-bi</ta>
            <ta e="T19" id="Seg_1034" s="T18">onʼiʔ</ta>
            <ta e="T20" id="Seg_1035" s="T19">onʼiʔ-tə</ta>
            <ta e="T21" id="Seg_1036" s="T20">surar-laʔbə-ʔjə</ta>
            <ta e="T22" id="Seg_1037" s="T21">ej</ta>
            <ta e="T23" id="Seg_1038" s="T22">tĭmne-m</ta>
            <ta e="T24" id="Seg_1039" s="T23">onʼiʔ</ta>
            <ta e="T25" id="Seg_1040" s="T24">măn-də</ta>
            <ta e="T26" id="Seg_1041" s="T25">măn</ta>
            <ta e="T27" id="Seg_1042" s="T26">ej</ta>
            <ta e="T28" id="Seg_1043" s="T27">tĭmne-m</ta>
            <ta e="T29" id="Seg_1044" s="T28">dĭgəttə</ta>
            <ta e="T30" id="Seg_1045" s="T29">kam-bi-ʔi</ta>
            <ta e="T31" id="Seg_1046" s="T30">kuza</ta>
            <ta e="T32" id="Seg_1047" s="T31">onʼiʔ</ta>
            <ta e="T33" id="Seg_1048" s="T32">ku-bi-ʔi</ta>
            <ta e="T34" id="Seg_1049" s="T33">măn-də</ta>
            <ta e="T35" id="Seg_1050" s="T34">dĭ</ta>
            <ta e="T36" id="Seg_1051" s="T35">kujo</ta>
            <ta e="T38" id="Seg_1052" s="T37">dĭ</ta>
            <ta e="T39" id="Seg_1053" s="T38">kudaj</ta>
            <ta e="T40" id="Seg_1054" s="T39">šo-bi</ta>
            <ta e="T41" id="Seg_1055" s="T40">dĭ-zeŋ</ta>
            <ta e="T42" id="Seg_1056" s="T41">nada</ta>
            <ta e="T43" id="Seg_1057" s="T42">bădə-sʼtə</ta>
            <ta e="T44" id="Seg_1058" s="T43">dĭ-m</ta>
            <ta e="T45" id="Seg_1059" s="T44">lʼepʼoška-ʔi</ta>
            <ta e="T46" id="Seg_1060" s="T45">deʔ-pi-ʔi</ta>
            <ta e="T47" id="Seg_1061" s="T46">išo</ta>
            <ta e="T48" id="Seg_1062" s="T47">ĭmbi=də</ta>
            <ta e="T49" id="Seg_1063" s="T48">deʔ-pi-ʔi</ta>
            <ta e="T50" id="Seg_1064" s="T49">măn-də</ta>
            <ta e="T51" id="Seg_1065" s="T50">mĭ-ʔ</ta>
            <ta e="T52" id="Seg_1066" s="T51">dĭ-n</ta>
            <ta e="T53" id="Seg_1067" s="T52">amor-zittə</ta>
            <ta e="T54" id="Seg_1068" s="T53">dĭ</ta>
            <ta e="T55" id="Seg_1069" s="T54">măn-də</ta>
            <ta e="T56" id="Seg_1070" s="T55">măn</ta>
            <ta e="T57" id="Seg_1071" s="T56">pim-nie-m</ta>
            <ta e="T58" id="Seg_1072" s="T57">dĭ</ta>
            <ta e="T59" id="Seg_1073" s="T58">măna</ta>
            <ta e="T60" id="Seg_1074" s="T59">išo</ta>
            <ta e="T61" id="Seg_1075" s="T60">kut-lə-j</ta>
            <ta e="T62" id="Seg_1076" s="T61">kuro-lə-j</ta>
            <ta e="T63" id="Seg_1077" s="T62">dĭgəttə</ta>
            <ta e="T64" id="Seg_1078" s="T63">deʔ-keʔ</ta>
            <ta e="T66" id="Seg_1079" s="T65">deʔ-keʔ</ta>
            <ta e="T67" id="Seg_1080" s="T66">teʔme</ta>
            <ta e="T68" id="Seg_1081" s="T67">da</ta>
            <ta e="T69" id="Seg_1082" s="T68">măna</ta>
            <ta e="T71" id="Seg_1083" s="T70">üjü-zeŋ-də</ta>
            <ta e="T72" id="Seg_1084" s="T71">sar-gaʔ</ta>
            <ta e="T73" id="Seg_1085" s="T72">kamən</ta>
            <ta e="T74" id="Seg_1086" s="T73">dĭ</ta>
            <ta e="T75" id="Seg_1087" s="T74">măna</ta>
            <ta e="T76" id="Seg_1088" s="T75">toʔ-nar-lə-j</ta>
            <ta e="T77" id="Seg_1089" s="T76">šiʔ</ta>
            <ta e="T78" id="Seg_1090" s="T77">măna</ta>
            <ta e="T79" id="Seg_1091" s="T78">üjü-t-siʔ</ta>
            <ta e="T80" id="Seg_1092" s="T79">dʼü-nə</ta>
            <ta e="T81" id="Seg_1093" s="T80">deʔ-keʔ</ta>
            <ta e="T82" id="Seg_1094" s="T81">dĭgəttə</ta>
            <ta e="T83" id="Seg_1095" s="T82">dĭ</ta>
            <ta e="T84" id="Seg_1096" s="T83">dĭbər</ta>
            <ta e="T86" id="Seg_1097" s="T85">sʼa-bi</ta>
            <ta e="T87" id="Seg_1098" s="T86">süjö</ta>
            <ta e="T88" id="Seg_1099" s="T87">nʼergö-lüʔ-pi</ta>
            <ta e="T89" id="Seg_1100" s="T88">dĭ</ta>
            <ta e="T90" id="Seg_1101" s="T89">kirgar-laʔbə</ta>
            <ta e="T91" id="Seg_1102" s="T90">i-geʔ</ta>
            <ta e="T92" id="Seg_1103" s="T91">măna</ta>
            <ta e="T94" id="Seg_1104" s="T93">dĭ-zeŋ</ta>
            <ta e="T95" id="Seg_1105" s="T94">bar</ta>
            <ta e="T96" id="Seg_1106" s="T95">dĭ-m</ta>
            <ta e="T98" id="Seg_1107" s="T97">saj</ta>
            <ta e="T101" id="Seg_1108" s="T100">ulu-t</ta>
            <ta e="T102" id="Seg_1109" s="T101">bar</ta>
            <ta e="T103" id="Seg_1110" s="T102">dĭgəttə</ta>
            <ta e="T104" id="Seg_1111" s="T103">dĭ-zeŋ</ta>
            <ta e="T105" id="Seg_1112" s="T104">nʼeʔ-pi-ʔi</ta>
            <ta e="T106" id="Seg_1113" s="T105">üjü-ttə</ta>
            <ta e="T107" id="Seg_1114" s="T106">ulu-t</ta>
            <ta e="T108" id="Seg_1115" s="T107">ma-luʔ-pi</ta>
            <ta e="T109" id="Seg_1116" s="T108">bar</ta>
            <ta e="T110" id="Seg_1117" s="T109">saj-nʼeʔ-luʔ-pi-ʔi</ta>
            <ta e="T111" id="Seg_1118" s="T110">dĭ</ta>
            <ta e="T112" id="Seg_1119" s="T111">dʼü-nə</ta>
            <ta e="T113" id="Seg_1120" s="T112">saʔmə-luʔ-pi-ʔi</ta>
            <ta e="T114" id="Seg_1121" s="T113">dĭgəttə</ta>
            <ta e="T115" id="Seg_1122" s="T114">măl-lia-ʔi</ta>
            <ta e="T116" id="Seg_1123" s="T115">dĭ-n</ta>
            <ta e="T117" id="Seg_1124" s="T116">i-bi</ta>
            <ta e="T118" id="Seg_1125" s="T117">ulu-t</ta>
            <ta e="T119" id="Seg_1126" s="T118">alʼi</ta>
            <ta e="T120" id="Seg_1127" s="T119">nago-bi-ʔi</ta>
            <ta e="T121" id="Seg_1128" s="T120">dĭgəttə</ta>
            <ta e="T122" id="Seg_1129" s="T121">dĭ-n</ta>
            <ta e="T123" id="Seg_1130" s="T122">ne-m</ta>
            <ta e="T124" id="Seg_1131" s="T123">surar-luʔ-pi-ʔi</ta>
            <ta e="T125" id="Seg_1132" s="T124">a</ta>
            <ta e="T126" id="Seg_1133" s="T125">măn</ta>
            <ta e="T127" id="Seg_1134" s="T126">ej</ta>
            <ta e="T128" id="Seg_1135" s="T127">tĭmne-m</ta>
            <ta e="T129" id="Seg_1136" s="T128">kamən</ta>
            <ta e="T130" id="Seg_1137" s="T129">dĭ</ta>
            <ta e="T131" id="Seg_1138" s="T130">amor-bi</ta>
            <ta e="T132" id="Seg_1139" s="T131">dak</ta>
            <ta e="T133" id="Seg_1140" s="T132">dĭ-n</ta>
            <ta e="T134" id="Seg_1141" s="T133">muʔseŋ-də</ta>
            <ta e="T135" id="Seg_1142" s="T134">bar</ta>
            <ta e="T136" id="Seg_1143" s="T135">sădər-bi-ʔi</ta>
            <ta e="T137" id="Seg_1144" s="T136">a</ta>
            <ta e="T138" id="Seg_1145" s="T137">tüj</ta>
            <ta e="T139" id="Seg_1146" s="T138">ej</ta>
            <ta e="T140" id="Seg_1147" s="T139">tĭmne-m</ta>
            <ta e="T141" id="Seg_1148" s="T140">dĭgəttə</ta>
            <ta e="T142" id="Seg_1149" s="T141">dibər</ta>
            <ta e="T144" id="Seg_1150" s="T143">sʼa-bi-ʔi</ta>
            <ta e="T145" id="Seg_1151" s="T144">a</ta>
            <ta e="T146" id="Seg_1152" s="T145">ulu-t</ta>
            <ta e="T147" id="Seg_1153" s="T146">dĭn</ta>
            <ta e="T148" id="Seg_1154" s="T147">amno-laʔbə</ta>
            <ta e="T149" id="Seg_1155" s="T148">dĭ-zeŋ</ta>
            <ta e="T150" id="Seg_1156" s="T149">deʔ-pi-ʔi</ta>
            <ta e="T151" id="Seg_1157" s="T150">ulu-t</ta>
            <ta e="T152" id="Seg_1158" s="T151">em-bi-ʔi</ta>
            <ta e="T153" id="Seg_1159" s="T152">dʼü-m</ta>
            <ta e="T154" id="Seg_1160" s="T153">tĭl-bi-ʔi</ta>
            <ta e="T155" id="Seg_1161" s="T154">em-bi-ʔi</ta>
            <ta e="T156" id="Seg_1162" s="T155">tĭl-luʔ-pi-ʔi</ta>
            <ta e="T157" id="Seg_1163" s="T156">dĭ-m</ta>
            <ta e="T158" id="Seg_1164" s="T157">i</ta>
            <ta e="T161" id="Seg_1165" s="T158">kal-la</ta>
            <ta e="T159" id="Seg_1166" s="T161">dʼür-bi-ʔi</ta>
            <ta e="T160" id="Seg_1167" s="T159">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1168" s="T0">amno-bi-jəʔ</ta>
            <ta e="T2" id="Seg_1169" s="T1">sagəštə-t</ta>
            <ta e="T3" id="Seg_1170" s="T2">il</ta>
            <ta e="T4" id="Seg_1171" s="T3">iʔgö</ta>
            <ta e="T5" id="Seg_1172" s="T4">dĭgəttə</ta>
            <ta e="T7" id="Seg_1173" s="T6">nʼergö-lAʔ</ta>
            <ta e="T8" id="Seg_1174" s="T7">šo-bi</ta>
            <ta e="T9" id="Seg_1175" s="T8">süjö</ta>
            <ta e="T10" id="Seg_1176" s="T9">amnə-bi</ta>
            <ta e="T11" id="Seg_1177" s="T10">pa-jəʔ-Tə</ta>
            <ta e="T13" id="Seg_1178" s="T12">dĭ-zAŋ</ta>
            <ta e="T15" id="Seg_1179" s="T14">măn-liA-jəʔ</ta>
            <ta e="T16" id="Seg_1180" s="T15">šində</ta>
            <ta e="T17" id="Seg_1181" s="T16">döbər</ta>
            <ta e="T18" id="Seg_1182" s="T17">šo-bi</ta>
            <ta e="T19" id="Seg_1183" s="T18">onʼiʔ</ta>
            <ta e="T20" id="Seg_1184" s="T19">onʼiʔ-Tə</ta>
            <ta e="T21" id="Seg_1185" s="T20">surar-laʔbə-jəʔ</ta>
            <ta e="T22" id="Seg_1186" s="T21">ej</ta>
            <ta e="T23" id="Seg_1187" s="T22">tĭmne-m</ta>
            <ta e="T24" id="Seg_1188" s="T23">onʼiʔ</ta>
            <ta e="T25" id="Seg_1189" s="T24">măn-ntə</ta>
            <ta e="T26" id="Seg_1190" s="T25">măn</ta>
            <ta e="T27" id="Seg_1191" s="T26">ej</ta>
            <ta e="T28" id="Seg_1192" s="T27">tĭmne-m</ta>
            <ta e="T29" id="Seg_1193" s="T28">dĭgəttə</ta>
            <ta e="T30" id="Seg_1194" s="T29">kan-bi-jəʔ</ta>
            <ta e="T31" id="Seg_1195" s="T30">kuza</ta>
            <ta e="T32" id="Seg_1196" s="T31">onʼiʔ</ta>
            <ta e="T33" id="Seg_1197" s="T32">ku-bi-jəʔ</ta>
            <ta e="T34" id="Seg_1198" s="T33">măn-ntə</ta>
            <ta e="T35" id="Seg_1199" s="T34">dĭ</ta>
            <ta e="T36" id="Seg_1200" s="T35">kojo</ta>
            <ta e="T38" id="Seg_1201" s="T37">dĭ</ta>
            <ta e="T39" id="Seg_1202" s="T38">kudaj</ta>
            <ta e="T40" id="Seg_1203" s="T39">šo-bi</ta>
            <ta e="T41" id="Seg_1204" s="T40">dĭ-zAŋ</ta>
            <ta e="T42" id="Seg_1205" s="T41">nadə</ta>
            <ta e="T43" id="Seg_1206" s="T42">bădə-zittə</ta>
            <ta e="T44" id="Seg_1207" s="T43">dĭ-m</ta>
            <ta e="T45" id="Seg_1208" s="T44">lʼepʼoška-jəʔ</ta>
            <ta e="T46" id="Seg_1209" s="T45">det-bi-jəʔ</ta>
            <ta e="T47" id="Seg_1210" s="T46">ĭššo</ta>
            <ta e="T48" id="Seg_1211" s="T47">ĭmbi=də</ta>
            <ta e="T49" id="Seg_1212" s="T48">det-bi-jəʔ</ta>
            <ta e="T50" id="Seg_1213" s="T49">măn-ntə</ta>
            <ta e="T51" id="Seg_1214" s="T50">mĭ-ʔ</ta>
            <ta e="T52" id="Seg_1215" s="T51">dĭ-n</ta>
            <ta e="T53" id="Seg_1216" s="T52">amor-zittə</ta>
            <ta e="T54" id="Seg_1217" s="T53">dĭ</ta>
            <ta e="T55" id="Seg_1218" s="T54">măn-ntə</ta>
            <ta e="T56" id="Seg_1219" s="T55">măn</ta>
            <ta e="T57" id="Seg_1220" s="T56">pim-liA-m</ta>
            <ta e="T58" id="Seg_1221" s="T57">dĭ</ta>
            <ta e="T59" id="Seg_1222" s="T58">măna</ta>
            <ta e="T60" id="Seg_1223" s="T59">ĭššo</ta>
            <ta e="T61" id="Seg_1224" s="T60">kut-lV-j</ta>
            <ta e="T62" id="Seg_1225" s="T61">kuroː-lV-j</ta>
            <ta e="T63" id="Seg_1226" s="T62">dĭgəttə</ta>
            <ta e="T64" id="Seg_1227" s="T63">det-KAʔ</ta>
            <ta e="T66" id="Seg_1228" s="T65">det-KAʔ</ta>
            <ta e="T67" id="Seg_1229" s="T66">teʔme</ta>
            <ta e="T68" id="Seg_1230" s="T67">da</ta>
            <ta e="T69" id="Seg_1231" s="T68">măna</ta>
            <ta e="T71" id="Seg_1232" s="T70">üjü-zAŋ-də</ta>
            <ta e="T72" id="Seg_1233" s="T71">sar-KAʔ</ta>
            <ta e="T73" id="Seg_1234" s="T72">kamən</ta>
            <ta e="T74" id="Seg_1235" s="T73">dĭ</ta>
            <ta e="T75" id="Seg_1236" s="T74">măna</ta>
            <ta e="T76" id="Seg_1237" s="T75">toʔbdə-nar-lV-j</ta>
            <ta e="T77" id="Seg_1238" s="T76">šiʔ</ta>
            <ta e="T78" id="Seg_1239" s="T77">măna</ta>
            <ta e="T79" id="Seg_1240" s="T78">üjü-t-ziʔ</ta>
            <ta e="T80" id="Seg_1241" s="T79">tʼo-Tə</ta>
            <ta e="T81" id="Seg_1242" s="T80">det-KAʔ</ta>
            <ta e="T82" id="Seg_1243" s="T81">dĭgəttə</ta>
            <ta e="T83" id="Seg_1244" s="T82">dĭ</ta>
            <ta e="T84" id="Seg_1245" s="T83">dĭbər</ta>
            <ta e="T86" id="Seg_1246" s="T85">sʼa-bi</ta>
            <ta e="T87" id="Seg_1247" s="T86">süjö</ta>
            <ta e="T88" id="Seg_1248" s="T87">nʼergö-luʔbdə-bi</ta>
            <ta e="T89" id="Seg_1249" s="T88">dĭ</ta>
            <ta e="T90" id="Seg_1250" s="T89">kirgaːr-laʔbə</ta>
            <ta e="T91" id="Seg_1251" s="T90">i-KAʔ</ta>
            <ta e="T92" id="Seg_1252" s="T91">măna</ta>
            <ta e="T94" id="Seg_1253" s="T93">dĭ-zAŋ</ta>
            <ta e="T95" id="Seg_1254" s="T94">bar</ta>
            <ta e="T96" id="Seg_1255" s="T95">dĭ-m</ta>
            <ta e="T98" id="Seg_1256" s="T97">saj</ta>
            <ta e="T101" id="Seg_1257" s="T100">ulu-t</ta>
            <ta e="T102" id="Seg_1258" s="T101">bar</ta>
            <ta e="T103" id="Seg_1259" s="T102">dĭgəttə</ta>
            <ta e="T104" id="Seg_1260" s="T103">dĭ-zAŋ</ta>
            <ta e="T105" id="Seg_1261" s="T104">nʼeʔbdə-bi-jəʔ</ta>
            <ta e="T106" id="Seg_1262" s="T105">üjü-ttə</ta>
            <ta e="T107" id="Seg_1263" s="T106">ulu-t</ta>
            <ta e="T108" id="Seg_1264" s="T107">ma-luʔbdə-bi</ta>
            <ta e="T109" id="Seg_1265" s="T108">bar</ta>
            <ta e="T110" id="Seg_1266" s="T109">săj-nʼeʔbdə-luʔbdə-bi-jəʔ</ta>
            <ta e="T111" id="Seg_1267" s="T110">dĭ</ta>
            <ta e="T112" id="Seg_1268" s="T111">tʼo-Tə</ta>
            <ta e="T113" id="Seg_1269" s="T112">saʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T114" id="Seg_1270" s="T113">dĭgəttə</ta>
            <ta e="T115" id="Seg_1271" s="T114">măn-liA-jəʔ</ta>
            <ta e="T116" id="Seg_1272" s="T115">dĭ-n</ta>
            <ta e="T117" id="Seg_1273" s="T116">i-bi</ta>
            <ta e="T118" id="Seg_1274" s="T117">ulu-t</ta>
            <ta e="T119" id="Seg_1275" s="T118">aľi</ta>
            <ta e="T120" id="Seg_1276" s="T119">naga-bi-jəʔ</ta>
            <ta e="T121" id="Seg_1277" s="T120">dĭgəttə</ta>
            <ta e="T122" id="Seg_1278" s="T121">dĭ-n</ta>
            <ta e="T123" id="Seg_1279" s="T122">ne-m</ta>
            <ta e="T124" id="Seg_1280" s="T123">surar-luʔbdə-bi-jəʔ</ta>
            <ta e="T125" id="Seg_1281" s="T124">a</ta>
            <ta e="T126" id="Seg_1282" s="T125">măn</ta>
            <ta e="T127" id="Seg_1283" s="T126">ej</ta>
            <ta e="T128" id="Seg_1284" s="T127">tĭmne-m</ta>
            <ta e="T129" id="Seg_1285" s="T128">kamən</ta>
            <ta e="T130" id="Seg_1286" s="T129">dĭ</ta>
            <ta e="T131" id="Seg_1287" s="T130">amor-bi</ta>
            <ta e="T132" id="Seg_1288" s="T131">tak</ta>
            <ta e="T133" id="Seg_1289" s="T132">dĭ-n</ta>
            <ta e="T134" id="Seg_1290" s="T133">muʔzen-də</ta>
            <ta e="T135" id="Seg_1291" s="T134">bar</ta>
            <ta e="T136" id="Seg_1292" s="T135">sădər-bi-jəʔ</ta>
            <ta e="T137" id="Seg_1293" s="T136">a</ta>
            <ta e="T138" id="Seg_1294" s="T137">tüj</ta>
            <ta e="T139" id="Seg_1295" s="T138">ej</ta>
            <ta e="T140" id="Seg_1296" s="T139">tĭmne-m</ta>
            <ta e="T141" id="Seg_1297" s="T140">dĭgəttə</ta>
            <ta e="T142" id="Seg_1298" s="T141">dĭbər</ta>
            <ta e="T144" id="Seg_1299" s="T143">sʼa-bi-jəʔ</ta>
            <ta e="T145" id="Seg_1300" s="T144">a</ta>
            <ta e="T146" id="Seg_1301" s="T145">ulu-t</ta>
            <ta e="T147" id="Seg_1302" s="T146">dĭn</ta>
            <ta e="T148" id="Seg_1303" s="T147">amno-laʔbə</ta>
            <ta e="T149" id="Seg_1304" s="T148">dĭ-zAŋ</ta>
            <ta e="T150" id="Seg_1305" s="T149">det-bi-jəʔ</ta>
            <ta e="T151" id="Seg_1306" s="T150">ulu-t</ta>
            <ta e="T152" id="Seg_1307" s="T151">hen-bi-jəʔ</ta>
            <ta e="T153" id="Seg_1308" s="T152">tʼo-m</ta>
            <ta e="T154" id="Seg_1309" s="T153">tĭl-bi-jəʔ</ta>
            <ta e="T155" id="Seg_1310" s="T154">hen-bi-jəʔ</ta>
            <ta e="T156" id="Seg_1311" s="T155">tĭl-luʔbdə-bi-jəʔ</ta>
            <ta e="T157" id="Seg_1312" s="T156">dĭ-m</ta>
            <ta e="T158" id="Seg_1313" s="T157">i</ta>
            <ta e="T161" id="Seg_1314" s="T158">kan-lAʔ</ta>
            <ta e="T159" id="Seg_1315" s="T161">tʼür-bi-jəʔ</ta>
            <ta e="T160" id="Seg_1316" s="T159">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1317" s="T0">live-PST-3PL</ta>
            <ta e="T2" id="Seg_1318" s="T1">silly-NOM/GEN.3SG</ta>
            <ta e="T3" id="Seg_1319" s="T2">people.[NOM.SG]</ta>
            <ta e="T4" id="Seg_1320" s="T3">many</ta>
            <ta e="T5" id="Seg_1321" s="T4">then</ta>
            <ta e="T7" id="Seg_1322" s="T6">fly-CVB</ta>
            <ta e="T8" id="Seg_1323" s="T7">come-PST.[3SG]</ta>
            <ta e="T9" id="Seg_1324" s="T8">bird.[NOM.SG]</ta>
            <ta e="T10" id="Seg_1325" s="T9">sit.down-PST.[3SG]</ta>
            <ta e="T11" id="Seg_1326" s="T10">tree-3PL-LAT</ta>
            <ta e="T13" id="Seg_1327" s="T12">this-PL</ta>
            <ta e="T15" id="Seg_1328" s="T14">say-PRS-3PL</ta>
            <ta e="T16" id="Seg_1329" s="T15">who.[NOM.SG]</ta>
            <ta e="T17" id="Seg_1330" s="T16">here</ta>
            <ta e="T18" id="Seg_1331" s="T17">come-PST.[3SG]</ta>
            <ta e="T19" id="Seg_1332" s="T18">one.[NOM.SG]</ta>
            <ta e="T20" id="Seg_1333" s="T19">one-LAT</ta>
            <ta e="T21" id="Seg_1334" s="T20">ask-DUR-3PL</ta>
            <ta e="T22" id="Seg_1335" s="T21">NEG</ta>
            <ta e="T23" id="Seg_1336" s="T22">know-1SG</ta>
            <ta e="T24" id="Seg_1337" s="T23">one.[NOM.SG]</ta>
            <ta e="T25" id="Seg_1338" s="T24">say-IPFVZ.[3SG]</ta>
            <ta e="T26" id="Seg_1339" s="T25">I.NOM</ta>
            <ta e="T27" id="Seg_1340" s="T26">NEG</ta>
            <ta e="T28" id="Seg_1341" s="T27">know-1SG</ta>
            <ta e="T29" id="Seg_1342" s="T28">then</ta>
            <ta e="T30" id="Seg_1343" s="T29">go-PST-3PL</ta>
            <ta e="T31" id="Seg_1344" s="T30">man.[NOM.SG]</ta>
            <ta e="T32" id="Seg_1345" s="T31">one.[NOM.SG]</ta>
            <ta e="T33" id="Seg_1346" s="T32">see-PST-3PL</ta>
            <ta e="T34" id="Seg_1347" s="T33">say-IPFVZ.[3SG]</ta>
            <ta e="T35" id="Seg_1348" s="T34">this</ta>
            <ta e="T36" id="Seg_1349" s="T35">stay.[3SG]</ta>
            <ta e="T38" id="Seg_1350" s="T37">this.[NOM.SG]</ta>
            <ta e="T39" id="Seg_1351" s="T38">God.[NOM.SG]</ta>
            <ta e="T40" id="Seg_1352" s="T39">come-PST.[3SG]</ta>
            <ta e="T41" id="Seg_1353" s="T40">this-PL</ta>
            <ta e="T42" id="Seg_1354" s="T41">one.should</ta>
            <ta e="T43" id="Seg_1355" s="T42">feed-INF.LAT</ta>
            <ta e="T44" id="Seg_1356" s="T43">this-ACC</ta>
            <ta e="T45" id="Seg_1357" s="T44">flatbread-PL</ta>
            <ta e="T46" id="Seg_1358" s="T45">bring-PST-3PL</ta>
            <ta e="T47" id="Seg_1359" s="T46">more</ta>
            <ta e="T48" id="Seg_1360" s="T47">what.[NOM.SG]=INDEF</ta>
            <ta e="T49" id="Seg_1361" s="T48">bring-PST-3PL</ta>
            <ta e="T50" id="Seg_1362" s="T49">say-IPFVZ.[3SG]</ta>
            <ta e="T51" id="Seg_1363" s="T50">give-IMP.2SG</ta>
            <ta e="T52" id="Seg_1364" s="T51">this-GEN</ta>
            <ta e="T53" id="Seg_1365" s="T52">eat-INF.LAT</ta>
            <ta e="T54" id="Seg_1366" s="T53">this</ta>
            <ta e="T55" id="Seg_1367" s="T54">say-IPFVZ.[3SG]</ta>
            <ta e="T56" id="Seg_1368" s="T55">I.NOM</ta>
            <ta e="T57" id="Seg_1369" s="T56">fear-PRS-1SG</ta>
            <ta e="T58" id="Seg_1370" s="T57">this.[NOM.SG]</ta>
            <ta e="T59" id="Seg_1371" s="T58">I.ACC</ta>
            <ta e="T60" id="Seg_1372" s="T59">more</ta>
            <ta e="T61" id="Seg_1373" s="T60">kill-FUT-3SG</ta>
            <ta e="T62" id="Seg_1374" s="T61">be.angry-FUT-3SG</ta>
            <ta e="T63" id="Seg_1375" s="T62">then</ta>
            <ta e="T64" id="Seg_1376" s="T63">bring-IMP.2PL</ta>
            <ta e="T66" id="Seg_1377" s="T65">bring-IMP.2PL</ta>
            <ta e="T67" id="Seg_1378" s="T66">rope.[NOM.SG]</ta>
            <ta e="T68" id="Seg_1379" s="T67">and</ta>
            <ta e="T69" id="Seg_1380" s="T68">I.ACC</ta>
            <ta e="T71" id="Seg_1381" s="T70">foot-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T72" id="Seg_1382" s="T71">bind-IMP.2PL</ta>
            <ta e="T73" id="Seg_1383" s="T72">when</ta>
            <ta e="T74" id="Seg_1384" s="T73">this.[NOM.SG]</ta>
            <ta e="T75" id="Seg_1385" s="T74">I.ACC</ta>
            <ta e="T76" id="Seg_1386" s="T75">hit-MULT-FUT-3SG</ta>
            <ta e="T77" id="Seg_1387" s="T76">you.PL.NOM</ta>
            <ta e="T78" id="Seg_1388" s="T77">I.ACC</ta>
            <ta e="T79" id="Seg_1389" s="T78">foot-3SG-INS</ta>
            <ta e="T80" id="Seg_1390" s="T79">place-LAT</ta>
            <ta e="T81" id="Seg_1391" s="T80">bring-IMP.2PL</ta>
            <ta e="T82" id="Seg_1392" s="T81">then</ta>
            <ta e="T83" id="Seg_1393" s="T82">this.[NOM.SG]</ta>
            <ta e="T84" id="Seg_1394" s="T83">there</ta>
            <ta e="T86" id="Seg_1395" s="T85">climb-PST.[3SG]</ta>
            <ta e="T87" id="Seg_1396" s="T86">bird.[NOM.SG]</ta>
            <ta e="T88" id="Seg_1397" s="T87">fly-MOM-PST.[3SG]</ta>
            <ta e="T89" id="Seg_1398" s="T88">this</ta>
            <ta e="T90" id="Seg_1399" s="T89">shout-DUR.[3SG]</ta>
            <ta e="T91" id="Seg_1400" s="T90">take-IMP.2PL</ta>
            <ta e="T92" id="Seg_1401" s="T91">I.ACC</ta>
            <ta e="T94" id="Seg_1402" s="T93">this-PL</ta>
            <ta e="T95" id="Seg_1403" s="T94">PTCL</ta>
            <ta e="T96" id="Seg_1404" s="T95">this-ACC</ta>
            <ta e="T98" id="Seg_1405" s="T97">off</ta>
            <ta e="T101" id="Seg_1406" s="T100">head-NOM/GEN.3SG</ta>
            <ta e="T102" id="Seg_1407" s="T101">PTCL</ta>
            <ta e="T103" id="Seg_1408" s="T102">then</ta>
            <ta e="T104" id="Seg_1409" s="T103">this-PL</ta>
            <ta e="T105" id="Seg_1410" s="T104">pull-PST-3PL</ta>
            <ta e="T106" id="Seg_1411" s="T105">foot-ABL.3SG</ta>
            <ta e="T107" id="Seg_1412" s="T106">head-NOM/GEN.3SG</ta>
            <ta e="T108" id="Seg_1413" s="T107">leave-MOM-PST.[3SG]</ta>
            <ta e="T109" id="Seg_1414" s="T108">PTCL</ta>
            <ta e="T110" id="Seg_1415" s="T109">off-pull-MOM-PST-3PL</ta>
            <ta e="T111" id="Seg_1416" s="T110">this</ta>
            <ta e="T112" id="Seg_1417" s="T111">earth-LAT</ta>
            <ta e="T113" id="Seg_1418" s="T112">fall-MOM-PST-3PL</ta>
            <ta e="T114" id="Seg_1419" s="T113">then</ta>
            <ta e="T115" id="Seg_1420" s="T114">say-PRS-3PL</ta>
            <ta e="T116" id="Seg_1421" s="T115">this-GEN</ta>
            <ta e="T117" id="Seg_1422" s="T116">be-PST.[3SG]</ta>
            <ta e="T118" id="Seg_1423" s="T117">head-NOM/GEN.3SG</ta>
            <ta e="T119" id="Seg_1424" s="T118">or</ta>
            <ta e="T120" id="Seg_1425" s="T119">NEG.EX-PST-3PL</ta>
            <ta e="T121" id="Seg_1426" s="T120">then</ta>
            <ta e="T122" id="Seg_1427" s="T121">this-GEN</ta>
            <ta e="T123" id="Seg_1428" s="T122">woman-ACC</ta>
            <ta e="T124" id="Seg_1429" s="T123">ask-MOM-PST-3PL</ta>
            <ta e="T125" id="Seg_1430" s="T124">and</ta>
            <ta e="T126" id="Seg_1431" s="T125">I.NOM</ta>
            <ta e="T127" id="Seg_1432" s="T126">NEG</ta>
            <ta e="T128" id="Seg_1433" s="T127">know-1SG</ta>
            <ta e="T129" id="Seg_1434" s="T128">when</ta>
            <ta e="T130" id="Seg_1435" s="T129">this.[NOM.SG]</ta>
            <ta e="T131" id="Seg_1436" s="T130">eat-PST.[3SG]</ta>
            <ta e="T132" id="Seg_1437" s="T131">so</ta>
            <ta e="T133" id="Seg_1438" s="T132">this-GEN</ta>
            <ta e="T134" id="Seg_1439" s="T133">beard-NOM/GEN/ACC.3SG</ta>
            <ta e="T135" id="Seg_1440" s="T134">PTCL</ta>
            <ta e="T136" id="Seg_1441" s="T135">tremble-PST-3PL</ta>
            <ta e="T137" id="Seg_1442" s="T136">and</ta>
            <ta e="T138" id="Seg_1443" s="T137">now</ta>
            <ta e="T139" id="Seg_1444" s="T138">NEG</ta>
            <ta e="T140" id="Seg_1445" s="T139">know-1SG</ta>
            <ta e="T141" id="Seg_1446" s="T140">then</ta>
            <ta e="T142" id="Seg_1447" s="T141">there</ta>
            <ta e="T144" id="Seg_1448" s="T143">climb-PST-3PL</ta>
            <ta e="T145" id="Seg_1449" s="T144">and</ta>
            <ta e="T146" id="Seg_1450" s="T145">head-NOM/GEN.3SG</ta>
            <ta e="T147" id="Seg_1451" s="T146">there</ta>
            <ta e="T148" id="Seg_1452" s="T147">sit-DUR.[3SG]</ta>
            <ta e="T149" id="Seg_1453" s="T148">this-PL</ta>
            <ta e="T150" id="Seg_1454" s="T149">bring-PST-3PL</ta>
            <ta e="T151" id="Seg_1455" s="T150">head-NOM/GEN.3SG</ta>
            <ta e="T152" id="Seg_1456" s="T151">put-PST-3PL</ta>
            <ta e="T153" id="Seg_1457" s="T152">earth-ACC</ta>
            <ta e="T154" id="Seg_1458" s="T153">dig-PST-3PL</ta>
            <ta e="T155" id="Seg_1459" s="T154">put-PST-3PL</ta>
            <ta e="T156" id="Seg_1460" s="T155">dig-MOM-PST-3PL</ta>
            <ta e="T157" id="Seg_1461" s="T156">this-ACC</ta>
            <ta e="T158" id="Seg_1462" s="T157">and</ta>
            <ta e="T161" id="Seg_1463" s="T158">go-CVB</ta>
            <ta e="T159" id="Seg_1464" s="T161">disappear-PST-3PL</ta>
            <ta e="T160" id="Seg_1465" s="T159">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1466" s="T0">жить-PST-3PL</ta>
            <ta e="T2" id="Seg_1467" s="T1">глупый-NOM/GEN.3SG</ta>
            <ta e="T3" id="Seg_1468" s="T2">люди.[NOM.SG]</ta>
            <ta e="T4" id="Seg_1469" s="T3">много</ta>
            <ta e="T5" id="Seg_1470" s="T4">тогда</ta>
            <ta e="T7" id="Seg_1471" s="T6">лететь-CVB</ta>
            <ta e="T8" id="Seg_1472" s="T7">прийти-PST.[3SG]</ta>
            <ta e="T9" id="Seg_1473" s="T8">птица.[NOM.SG]</ta>
            <ta e="T10" id="Seg_1474" s="T9">сесть-PST.[3SG]</ta>
            <ta e="T11" id="Seg_1475" s="T10">дерево-3PL-LAT</ta>
            <ta e="T13" id="Seg_1476" s="T12">этот-PL</ta>
            <ta e="T15" id="Seg_1477" s="T14">сказать-PRS-3PL</ta>
            <ta e="T16" id="Seg_1478" s="T15">кто.[NOM.SG]</ta>
            <ta e="T17" id="Seg_1479" s="T16">здесь</ta>
            <ta e="T18" id="Seg_1480" s="T17">прийти-PST.[3SG]</ta>
            <ta e="T19" id="Seg_1481" s="T18">один.[NOM.SG]</ta>
            <ta e="T20" id="Seg_1482" s="T19">один-LAT</ta>
            <ta e="T21" id="Seg_1483" s="T20">спросить-DUR-3PL</ta>
            <ta e="T22" id="Seg_1484" s="T21">NEG</ta>
            <ta e="T23" id="Seg_1485" s="T22">знать-1SG</ta>
            <ta e="T24" id="Seg_1486" s="T23">один.[NOM.SG]</ta>
            <ta e="T25" id="Seg_1487" s="T24">сказать-IPFVZ.[3SG]</ta>
            <ta e="T26" id="Seg_1488" s="T25">я.NOM</ta>
            <ta e="T27" id="Seg_1489" s="T26">NEG</ta>
            <ta e="T28" id="Seg_1490" s="T27">знать-1SG</ta>
            <ta e="T29" id="Seg_1491" s="T28">тогда</ta>
            <ta e="T30" id="Seg_1492" s="T29">пойти-PST-3PL</ta>
            <ta e="T31" id="Seg_1493" s="T30">мужчина.[NOM.SG]</ta>
            <ta e="T32" id="Seg_1494" s="T31">один.[NOM.SG]</ta>
            <ta e="T33" id="Seg_1495" s="T32">видеть-PST-3PL</ta>
            <ta e="T34" id="Seg_1496" s="T33">сказать-IPFVZ.[3SG]</ta>
            <ta e="T35" id="Seg_1497" s="T34">этот</ta>
            <ta e="T36" id="Seg_1498" s="T35">остаться.[3SG]</ta>
            <ta e="T38" id="Seg_1499" s="T37">этот.[NOM.SG]</ta>
            <ta e="T39" id="Seg_1500" s="T38">бог.[NOM.SG]</ta>
            <ta e="T40" id="Seg_1501" s="T39">прийти-PST.[3SG]</ta>
            <ta e="T41" id="Seg_1502" s="T40">этот-PL</ta>
            <ta e="T42" id="Seg_1503" s="T41">надо</ta>
            <ta e="T43" id="Seg_1504" s="T42">кормить-INF.LAT</ta>
            <ta e="T44" id="Seg_1505" s="T43">этот-ACC</ta>
            <ta e="T45" id="Seg_1506" s="T44">лепешка-PL</ta>
            <ta e="T46" id="Seg_1507" s="T45">принести-PST-3PL</ta>
            <ta e="T47" id="Seg_1508" s="T46">еще</ta>
            <ta e="T48" id="Seg_1509" s="T47">что.[NOM.SG]=INDEF</ta>
            <ta e="T49" id="Seg_1510" s="T48">принести-PST-3PL</ta>
            <ta e="T50" id="Seg_1511" s="T49">сказать-IPFVZ.[3SG]</ta>
            <ta e="T51" id="Seg_1512" s="T50">дать-IMP.2SG</ta>
            <ta e="T52" id="Seg_1513" s="T51">этот-GEN</ta>
            <ta e="T53" id="Seg_1514" s="T52">есть-INF.LAT</ta>
            <ta e="T54" id="Seg_1515" s="T53">этот</ta>
            <ta e="T55" id="Seg_1516" s="T54">сказать-IPFVZ.[3SG]</ta>
            <ta e="T56" id="Seg_1517" s="T55">я.NOM</ta>
            <ta e="T57" id="Seg_1518" s="T56">бояться-PRS-1SG</ta>
            <ta e="T58" id="Seg_1519" s="T57">этот.[NOM.SG]</ta>
            <ta e="T59" id="Seg_1520" s="T58">я.ACC</ta>
            <ta e="T60" id="Seg_1521" s="T59">еще</ta>
            <ta e="T61" id="Seg_1522" s="T60">убить-FUT-3SG</ta>
            <ta e="T62" id="Seg_1523" s="T61">сердиться-FUT-3SG</ta>
            <ta e="T63" id="Seg_1524" s="T62">тогда</ta>
            <ta e="T64" id="Seg_1525" s="T63">принести-IMP.2PL</ta>
            <ta e="T66" id="Seg_1526" s="T65">принести-IMP.2PL</ta>
            <ta e="T67" id="Seg_1527" s="T66">веревка.[NOM.SG]</ta>
            <ta e="T68" id="Seg_1528" s="T67">и</ta>
            <ta e="T69" id="Seg_1529" s="T68">я.ACC</ta>
            <ta e="T71" id="Seg_1530" s="T70">нога-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T72" id="Seg_1531" s="T71">завязать-IMP.2PL</ta>
            <ta e="T73" id="Seg_1532" s="T72">когда</ta>
            <ta e="T74" id="Seg_1533" s="T73">этот.[NOM.SG]</ta>
            <ta e="T75" id="Seg_1534" s="T74">я.ACC</ta>
            <ta e="T76" id="Seg_1535" s="T75">ударить-MULT-FUT-3SG</ta>
            <ta e="T77" id="Seg_1536" s="T76">вы.NOM</ta>
            <ta e="T78" id="Seg_1537" s="T77">я.ACC</ta>
            <ta e="T79" id="Seg_1538" s="T78">нога-3SG-INS</ta>
            <ta e="T80" id="Seg_1539" s="T79">место-LAT</ta>
            <ta e="T81" id="Seg_1540" s="T80">принести-IMP.2PL</ta>
            <ta e="T82" id="Seg_1541" s="T81">тогда</ta>
            <ta e="T83" id="Seg_1542" s="T82">этот.[NOM.SG]</ta>
            <ta e="T84" id="Seg_1543" s="T83">там</ta>
            <ta e="T86" id="Seg_1544" s="T85">влезать-PST.[3SG]</ta>
            <ta e="T87" id="Seg_1545" s="T86">птица.[NOM.SG]</ta>
            <ta e="T88" id="Seg_1546" s="T87">лететь-MOM-PST.[3SG]</ta>
            <ta e="T89" id="Seg_1547" s="T88">этот</ta>
            <ta e="T90" id="Seg_1548" s="T89">кричать-DUR.[3SG]</ta>
            <ta e="T91" id="Seg_1549" s="T90">взять-IMP.2PL</ta>
            <ta e="T92" id="Seg_1550" s="T91">я.ACC</ta>
            <ta e="T94" id="Seg_1551" s="T93">этот-PL</ta>
            <ta e="T95" id="Seg_1552" s="T94">PTCL</ta>
            <ta e="T96" id="Seg_1553" s="T95">этот-ACC</ta>
            <ta e="T98" id="Seg_1554" s="T97">от</ta>
            <ta e="T101" id="Seg_1555" s="T100">голова-NOM/GEN.3SG</ta>
            <ta e="T102" id="Seg_1556" s="T101">PTCL</ta>
            <ta e="T103" id="Seg_1557" s="T102">тогда</ta>
            <ta e="T104" id="Seg_1558" s="T103">этот-PL</ta>
            <ta e="T105" id="Seg_1559" s="T104">тянуть-PST-3PL</ta>
            <ta e="T106" id="Seg_1560" s="T105">нога-ABL.3SG</ta>
            <ta e="T107" id="Seg_1561" s="T106">голова-NOM/GEN.3SG</ta>
            <ta e="T108" id="Seg_1562" s="T107">оставить-MOM-PST.[3SG]</ta>
            <ta e="T109" id="Seg_1563" s="T108">PTCL</ta>
            <ta e="T110" id="Seg_1564" s="T109">от-тянуть-MOM-PST-3PL</ta>
            <ta e="T111" id="Seg_1565" s="T110">этот</ta>
            <ta e="T112" id="Seg_1566" s="T111">земля-LAT</ta>
            <ta e="T113" id="Seg_1567" s="T112">упасть-MOM-PST-3PL</ta>
            <ta e="T114" id="Seg_1568" s="T113">тогда</ta>
            <ta e="T115" id="Seg_1569" s="T114">сказать-PRS-3PL</ta>
            <ta e="T116" id="Seg_1570" s="T115">этот-GEN</ta>
            <ta e="T117" id="Seg_1571" s="T116">быть-PST.[3SG]</ta>
            <ta e="T118" id="Seg_1572" s="T117">голова-NOM/GEN.3SG</ta>
            <ta e="T119" id="Seg_1573" s="T118">или</ta>
            <ta e="T120" id="Seg_1574" s="T119">NEG.EX-PST-3PL</ta>
            <ta e="T121" id="Seg_1575" s="T120">тогда</ta>
            <ta e="T122" id="Seg_1576" s="T121">этот-GEN</ta>
            <ta e="T123" id="Seg_1577" s="T122">женщина-ACC</ta>
            <ta e="T124" id="Seg_1578" s="T123">спросить-MOM-PST-3PL</ta>
            <ta e="T125" id="Seg_1579" s="T124">а</ta>
            <ta e="T126" id="Seg_1580" s="T125">я.NOM</ta>
            <ta e="T127" id="Seg_1581" s="T126">NEG</ta>
            <ta e="T128" id="Seg_1582" s="T127">знать-1SG</ta>
            <ta e="T129" id="Seg_1583" s="T128">когда</ta>
            <ta e="T130" id="Seg_1584" s="T129">этот.[NOM.SG]</ta>
            <ta e="T131" id="Seg_1585" s="T130">есть-PST.[3SG]</ta>
            <ta e="T132" id="Seg_1586" s="T131">так</ta>
            <ta e="T133" id="Seg_1587" s="T132">этот-GEN</ta>
            <ta e="T134" id="Seg_1588" s="T133">борода-NOM/GEN/ACC.3SG</ta>
            <ta e="T135" id="Seg_1589" s="T134">PTCL</ta>
            <ta e="T136" id="Seg_1590" s="T135">дрожать-PST-3PL</ta>
            <ta e="T137" id="Seg_1591" s="T136">а</ta>
            <ta e="T138" id="Seg_1592" s="T137">сейчас</ta>
            <ta e="T139" id="Seg_1593" s="T138">NEG</ta>
            <ta e="T140" id="Seg_1594" s="T139">знать-1SG</ta>
            <ta e="T141" id="Seg_1595" s="T140">тогда</ta>
            <ta e="T142" id="Seg_1596" s="T141">там</ta>
            <ta e="T144" id="Seg_1597" s="T143">влезать-PST-3PL</ta>
            <ta e="T145" id="Seg_1598" s="T144">а</ta>
            <ta e="T146" id="Seg_1599" s="T145">голова-NOM/GEN.3SG</ta>
            <ta e="T147" id="Seg_1600" s="T146">там</ta>
            <ta e="T148" id="Seg_1601" s="T147">сидеть-DUR.[3SG]</ta>
            <ta e="T149" id="Seg_1602" s="T148">этот-PL</ta>
            <ta e="T150" id="Seg_1603" s="T149">принести-PST-3PL</ta>
            <ta e="T151" id="Seg_1604" s="T150">голова-NOM/GEN.3SG</ta>
            <ta e="T152" id="Seg_1605" s="T151">класть-PST-3PL</ta>
            <ta e="T153" id="Seg_1606" s="T152">земля-ACC</ta>
            <ta e="T154" id="Seg_1607" s="T153">копать-PST-3PL</ta>
            <ta e="T155" id="Seg_1608" s="T154">класть-PST-3PL</ta>
            <ta e="T156" id="Seg_1609" s="T155">копать-MOM-PST-3PL</ta>
            <ta e="T157" id="Seg_1610" s="T156">этот-ACC</ta>
            <ta e="T158" id="Seg_1611" s="T157">и</ta>
            <ta e="T161" id="Seg_1612" s="T158">пойти-CVB</ta>
            <ta e="T159" id="Seg_1613" s="T161">исчезнуть-PST-3PL</ta>
            <ta e="T160" id="Seg_1614" s="T159">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1615" s="T0">v-v:tense-v:pn</ta>
            <ta e="T2" id="Seg_1616" s="T1">adj-n:case.poss</ta>
            <ta e="T3" id="Seg_1617" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_1618" s="T3">quant</ta>
            <ta e="T5" id="Seg_1619" s="T4">adv</ta>
            <ta e="T7" id="Seg_1620" s="T6">v-v:n.fin</ta>
            <ta e="T8" id="Seg_1621" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_1622" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_1623" s="T9">v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_1624" s="T10">n-n:case.poss-n:case</ta>
            <ta e="T13" id="Seg_1625" s="T12">dempro-n:num</ta>
            <ta e="T15" id="Seg_1626" s="T14">v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_1627" s="T15">que-n:case</ta>
            <ta e="T17" id="Seg_1628" s="T16">adv</ta>
            <ta e="T18" id="Seg_1629" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_1630" s="T18">num-n:case</ta>
            <ta e="T20" id="Seg_1631" s="T19">num-n:case</ta>
            <ta e="T21" id="Seg_1632" s="T20">v-v&gt;v-v:pn</ta>
            <ta e="T22" id="Seg_1633" s="T21">ptcl</ta>
            <ta e="T23" id="Seg_1634" s="T22">v-v:pn</ta>
            <ta e="T24" id="Seg_1635" s="T23">num-n:case</ta>
            <ta e="T25" id="Seg_1636" s="T24">v-v&gt;v-v:pn</ta>
            <ta e="T26" id="Seg_1637" s="T25">pers</ta>
            <ta e="T27" id="Seg_1638" s="T26">ptcl</ta>
            <ta e="T28" id="Seg_1639" s="T27">v-v:pn</ta>
            <ta e="T29" id="Seg_1640" s="T28">adv</ta>
            <ta e="T30" id="Seg_1641" s="T29">v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_1642" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_1643" s="T31">num-n:case</ta>
            <ta e="T33" id="Seg_1644" s="T32">v-v:tense-v:pn</ta>
            <ta e="T34" id="Seg_1645" s="T33">v-v&gt;v-v:pn</ta>
            <ta e="T35" id="Seg_1646" s="T34">dempro</ta>
            <ta e="T36" id="Seg_1647" s="T35">v-v:pn</ta>
            <ta e="T38" id="Seg_1648" s="T37">dempro-n:case</ta>
            <ta e="T39" id="Seg_1649" s="T38">n-n:case</ta>
            <ta e="T40" id="Seg_1650" s="T39">v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_1651" s="T40">dempro-n:num</ta>
            <ta e="T42" id="Seg_1652" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_1653" s="T42">v-v:n.fin</ta>
            <ta e="T44" id="Seg_1654" s="T43">dempro-n:case</ta>
            <ta e="T45" id="Seg_1655" s="T44">n-n:num</ta>
            <ta e="T46" id="Seg_1656" s="T45">v-v:tense-v:pn</ta>
            <ta e="T47" id="Seg_1657" s="T46">adv</ta>
            <ta e="T48" id="Seg_1658" s="T47">que-n:case=ptcl</ta>
            <ta e="T49" id="Seg_1659" s="T48">v-v:tense-v:pn</ta>
            <ta e="T50" id="Seg_1660" s="T49">v-v&gt;v-v:pn</ta>
            <ta e="T51" id="Seg_1661" s="T50">v-v:mood.pn</ta>
            <ta e="T52" id="Seg_1662" s="T51">dempro-n:case</ta>
            <ta e="T53" id="Seg_1663" s="T52">v-v:n.fin</ta>
            <ta e="T54" id="Seg_1664" s="T53">dempro</ta>
            <ta e="T55" id="Seg_1665" s="T54">v-v&gt;v-v:pn</ta>
            <ta e="T56" id="Seg_1666" s="T55">pers</ta>
            <ta e="T57" id="Seg_1667" s="T56">v-v:tense-v:pn</ta>
            <ta e="T58" id="Seg_1668" s="T57">dempro-n:case</ta>
            <ta e="T59" id="Seg_1669" s="T58">pers</ta>
            <ta e="T60" id="Seg_1670" s="T59">adv</ta>
            <ta e="T61" id="Seg_1671" s="T60">v-v:tense-v:pn</ta>
            <ta e="T62" id="Seg_1672" s="T61">v-v:tense-v:pn</ta>
            <ta e="T63" id="Seg_1673" s="T62">adv</ta>
            <ta e="T64" id="Seg_1674" s="T63">v-v:mood.pn</ta>
            <ta e="T66" id="Seg_1675" s="T65">v-v:mood.pn</ta>
            <ta e="T67" id="Seg_1676" s="T66">n-n:case</ta>
            <ta e="T68" id="Seg_1677" s="T67">conj</ta>
            <ta e="T69" id="Seg_1678" s="T68">pers</ta>
            <ta e="T71" id="Seg_1679" s="T70">n-n:num-n:case.poss</ta>
            <ta e="T72" id="Seg_1680" s="T71">v-v:mood.pn</ta>
            <ta e="T73" id="Seg_1681" s="T72">que</ta>
            <ta e="T74" id="Seg_1682" s="T73">dempro-n:case</ta>
            <ta e="T75" id="Seg_1683" s="T74">pers</ta>
            <ta e="T76" id="Seg_1684" s="T75">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T77" id="Seg_1685" s="T76">pers</ta>
            <ta e="T78" id="Seg_1686" s="T77">pers</ta>
            <ta e="T79" id="Seg_1687" s="T78">n-n:case.poss-n:case</ta>
            <ta e="T80" id="Seg_1688" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_1689" s="T80">v-v:mood.pn</ta>
            <ta e="T82" id="Seg_1690" s="T81">adv</ta>
            <ta e="T83" id="Seg_1691" s="T82">dempro-n:case</ta>
            <ta e="T84" id="Seg_1692" s="T83">adv</ta>
            <ta e="T86" id="Seg_1693" s="T85">v-v:tense-v:pn</ta>
            <ta e="T87" id="Seg_1694" s="T86">n-n:case</ta>
            <ta e="T88" id="Seg_1695" s="T87">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T89" id="Seg_1696" s="T88">dempro</ta>
            <ta e="T90" id="Seg_1697" s="T89">v-v&gt;v-v:pn</ta>
            <ta e="T91" id="Seg_1698" s="T90">v-v:mood.pn</ta>
            <ta e="T92" id="Seg_1699" s="T91">pers</ta>
            <ta e="T94" id="Seg_1700" s="T93">dempro-n:num</ta>
            <ta e="T95" id="Seg_1701" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_1702" s="T95">dempro-n:case</ta>
            <ta e="T98" id="Seg_1703" s="T97">adv</ta>
            <ta e="T101" id="Seg_1704" s="T100">n-n:case.poss</ta>
            <ta e="T102" id="Seg_1705" s="T101">ptcl</ta>
            <ta e="T103" id="Seg_1706" s="T102">adv</ta>
            <ta e="T104" id="Seg_1707" s="T103">dempro-n:num</ta>
            <ta e="T105" id="Seg_1708" s="T104">v-v:tense-v:pn</ta>
            <ta e="T106" id="Seg_1709" s="T105">n-n:case.poss</ta>
            <ta e="T107" id="Seg_1710" s="T106">n-n:case.poss</ta>
            <ta e="T108" id="Seg_1711" s="T107">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T109" id="Seg_1712" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_1713" s="T109">v&gt;v-v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T111" id="Seg_1714" s="T110">dempro</ta>
            <ta e="T112" id="Seg_1715" s="T111">n-n:case</ta>
            <ta e="T113" id="Seg_1716" s="T112">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T114" id="Seg_1717" s="T113">adv</ta>
            <ta e="T115" id="Seg_1718" s="T114">v-v:tense-v:pn</ta>
            <ta e="T116" id="Seg_1719" s="T115">dempro-n:case</ta>
            <ta e="T117" id="Seg_1720" s="T116">v-v:tense-v:pn</ta>
            <ta e="T118" id="Seg_1721" s="T117">n-n:case.poss</ta>
            <ta e="T119" id="Seg_1722" s="T118">conj</ta>
            <ta e="T120" id="Seg_1723" s="T119">v-v:tense-v:pn</ta>
            <ta e="T121" id="Seg_1724" s="T120">adv</ta>
            <ta e="T122" id="Seg_1725" s="T121">dempro-n:case</ta>
            <ta e="T123" id="Seg_1726" s="T122">n-n:case</ta>
            <ta e="T124" id="Seg_1727" s="T123">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T125" id="Seg_1728" s="T124">conj</ta>
            <ta e="T126" id="Seg_1729" s="T125">pers</ta>
            <ta e="T127" id="Seg_1730" s="T126">ptcl</ta>
            <ta e="T128" id="Seg_1731" s="T127">v-v:pn</ta>
            <ta e="T129" id="Seg_1732" s="T128">que</ta>
            <ta e="T130" id="Seg_1733" s="T129">dempro-n:case</ta>
            <ta e="T131" id="Seg_1734" s="T130">v-v:tense-v:pn</ta>
            <ta e="T132" id="Seg_1735" s="T131">ptcl</ta>
            <ta e="T133" id="Seg_1736" s="T132">dempro-n:case</ta>
            <ta e="T134" id="Seg_1737" s="T133">n-n:case.poss</ta>
            <ta e="T135" id="Seg_1738" s="T134">ptcl</ta>
            <ta e="T136" id="Seg_1739" s="T135">v-v:tense-v:pn</ta>
            <ta e="T137" id="Seg_1740" s="T136">conj</ta>
            <ta e="T138" id="Seg_1741" s="T137">adv</ta>
            <ta e="T139" id="Seg_1742" s="T138">ptcl</ta>
            <ta e="T140" id="Seg_1743" s="T139">v-v:pn</ta>
            <ta e="T141" id="Seg_1744" s="T140">adv</ta>
            <ta e="T142" id="Seg_1745" s="T141">adv</ta>
            <ta e="T144" id="Seg_1746" s="T143">v-v:tense-v:pn</ta>
            <ta e="T145" id="Seg_1747" s="T144">conj</ta>
            <ta e="T146" id="Seg_1748" s="T145">n-n:case.poss</ta>
            <ta e="T147" id="Seg_1749" s="T146">adv</ta>
            <ta e="T148" id="Seg_1750" s="T147">v-v&gt;v-v:pn</ta>
            <ta e="T149" id="Seg_1751" s="T148">dempro-n:num</ta>
            <ta e="T150" id="Seg_1752" s="T149">v-v:tense-v:pn</ta>
            <ta e="T151" id="Seg_1753" s="T150">n-n:case.poss</ta>
            <ta e="T152" id="Seg_1754" s="T151">v-v:tense-v:pn</ta>
            <ta e="T153" id="Seg_1755" s="T152">n-n:case</ta>
            <ta e="T154" id="Seg_1756" s="T153">v-v:tense-v:pn</ta>
            <ta e="T155" id="Seg_1757" s="T154">v-v:tense-v:pn</ta>
            <ta e="T156" id="Seg_1758" s="T155">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T157" id="Seg_1759" s="T156">dempro-n:case</ta>
            <ta e="T158" id="Seg_1760" s="T157">conj</ta>
            <ta e="T161" id="Seg_1761" s="T158">v-v:n-fin</ta>
            <ta e="T159" id="Seg_1762" s="T161">v-v:tense-v:pn</ta>
            <ta e="T160" id="Seg_1763" s="T159">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1764" s="T0">v</ta>
            <ta e="T2" id="Seg_1765" s="T1">adj</ta>
            <ta e="T3" id="Seg_1766" s="T2">n</ta>
            <ta e="T4" id="Seg_1767" s="T3">quant</ta>
            <ta e="T5" id="Seg_1768" s="T4">adv</ta>
            <ta e="T7" id="Seg_1769" s="T6">v</ta>
            <ta e="T8" id="Seg_1770" s="T7">v</ta>
            <ta e="T9" id="Seg_1771" s="T8">n</ta>
            <ta e="T10" id="Seg_1772" s="T9">v</ta>
            <ta e="T11" id="Seg_1773" s="T10">n</ta>
            <ta e="T13" id="Seg_1774" s="T12">dempro</ta>
            <ta e="T15" id="Seg_1775" s="T14">v</ta>
            <ta e="T16" id="Seg_1776" s="T15">que</ta>
            <ta e="T17" id="Seg_1777" s="T16">adv</ta>
            <ta e="T18" id="Seg_1778" s="T17">v</ta>
            <ta e="T19" id="Seg_1779" s="T18">num</ta>
            <ta e="T20" id="Seg_1780" s="T19">num</ta>
            <ta e="T21" id="Seg_1781" s="T20">v</ta>
            <ta e="T22" id="Seg_1782" s="T21">ptcl</ta>
            <ta e="T23" id="Seg_1783" s="T22">v</ta>
            <ta e="T24" id="Seg_1784" s="T23">num</ta>
            <ta e="T25" id="Seg_1785" s="T24">v</ta>
            <ta e="T26" id="Seg_1786" s="T25">pers</ta>
            <ta e="T27" id="Seg_1787" s="T26">ptcl</ta>
            <ta e="T28" id="Seg_1788" s="T27">v</ta>
            <ta e="T29" id="Seg_1789" s="T28">adv</ta>
            <ta e="T30" id="Seg_1790" s="T29">v</ta>
            <ta e="T31" id="Seg_1791" s="T30">n</ta>
            <ta e="T32" id="Seg_1792" s="T31">num</ta>
            <ta e="T33" id="Seg_1793" s="T32">v</ta>
            <ta e="T34" id="Seg_1794" s="T33">v</ta>
            <ta e="T35" id="Seg_1795" s="T34">dempro</ta>
            <ta e="T36" id="Seg_1796" s="T35">v</ta>
            <ta e="T38" id="Seg_1797" s="T37">dempro</ta>
            <ta e="T39" id="Seg_1798" s="T38">n</ta>
            <ta e="T40" id="Seg_1799" s="T39">v</ta>
            <ta e="T41" id="Seg_1800" s="T40">dempro</ta>
            <ta e="T42" id="Seg_1801" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_1802" s="T42">v</ta>
            <ta e="T44" id="Seg_1803" s="T43">dempro</ta>
            <ta e="T45" id="Seg_1804" s="T44">n</ta>
            <ta e="T46" id="Seg_1805" s="T45">v</ta>
            <ta e="T47" id="Seg_1806" s="T46">adv</ta>
            <ta e="T48" id="Seg_1807" s="T47">que</ta>
            <ta e="T49" id="Seg_1808" s="T48">v</ta>
            <ta e="T50" id="Seg_1809" s="T49">v</ta>
            <ta e="T51" id="Seg_1810" s="T50">pers</ta>
            <ta e="T52" id="Seg_1811" s="T51">dempro</ta>
            <ta e="T53" id="Seg_1812" s="T52">v</ta>
            <ta e="T54" id="Seg_1813" s="T53">dempro</ta>
            <ta e="T55" id="Seg_1814" s="T54">v</ta>
            <ta e="T56" id="Seg_1815" s="T55">pers</ta>
            <ta e="T57" id="Seg_1816" s="T56">v</ta>
            <ta e="T58" id="Seg_1817" s="T57">dempro</ta>
            <ta e="T59" id="Seg_1818" s="T58">pers</ta>
            <ta e="T60" id="Seg_1819" s="T59">adv</ta>
            <ta e="T61" id="Seg_1820" s="T60">v</ta>
            <ta e="T62" id="Seg_1821" s="T61">v</ta>
            <ta e="T63" id="Seg_1822" s="T62">adv</ta>
            <ta e="T64" id="Seg_1823" s="T63">v</ta>
            <ta e="T66" id="Seg_1824" s="T65">v</ta>
            <ta e="T67" id="Seg_1825" s="T66">n</ta>
            <ta e="T68" id="Seg_1826" s="T67">conj</ta>
            <ta e="T69" id="Seg_1827" s="T68">pers</ta>
            <ta e="T71" id="Seg_1828" s="T70">n</ta>
            <ta e="T72" id="Seg_1829" s="T71">v</ta>
            <ta e="T73" id="Seg_1830" s="T72">conj</ta>
            <ta e="T74" id="Seg_1831" s="T73">dempro</ta>
            <ta e="T75" id="Seg_1832" s="T74">pers</ta>
            <ta e="T76" id="Seg_1833" s="T75">v</ta>
            <ta e="T77" id="Seg_1834" s="T76">pers</ta>
            <ta e="T78" id="Seg_1835" s="T77">pers</ta>
            <ta e="T79" id="Seg_1836" s="T78">n</ta>
            <ta e="T80" id="Seg_1837" s="T79">n</ta>
            <ta e="T81" id="Seg_1838" s="T80">v</ta>
            <ta e="T82" id="Seg_1839" s="T81">adv</ta>
            <ta e="T83" id="Seg_1840" s="T82">dempro</ta>
            <ta e="T84" id="Seg_1841" s="T83">adv</ta>
            <ta e="T86" id="Seg_1842" s="T85">v</ta>
            <ta e="T87" id="Seg_1843" s="T86">n</ta>
            <ta e="T88" id="Seg_1844" s="T87">v</ta>
            <ta e="T89" id="Seg_1845" s="T88">dempro</ta>
            <ta e="T90" id="Seg_1846" s="T89">v</ta>
            <ta e="T91" id="Seg_1847" s="T90">v</ta>
            <ta e="T92" id="Seg_1848" s="T91">pers</ta>
            <ta e="T94" id="Seg_1849" s="T93">dempro</ta>
            <ta e="T95" id="Seg_1850" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_1851" s="T95">dempro</ta>
            <ta e="T98" id="Seg_1852" s="T97">adv</ta>
            <ta e="T101" id="Seg_1853" s="T100">n</ta>
            <ta e="T102" id="Seg_1854" s="T101">ptcl</ta>
            <ta e="T103" id="Seg_1855" s="T102">adv</ta>
            <ta e="T104" id="Seg_1856" s="T103">dempro</ta>
            <ta e="T106" id="Seg_1857" s="T105">n</ta>
            <ta e="T107" id="Seg_1858" s="T106">n</ta>
            <ta e="T108" id="Seg_1859" s="T107">v</ta>
            <ta e="T109" id="Seg_1860" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_1861" s="T109">v</ta>
            <ta e="T111" id="Seg_1862" s="T110">dempro</ta>
            <ta e="T112" id="Seg_1863" s="T111">n</ta>
            <ta e="T113" id="Seg_1864" s="T112">v</ta>
            <ta e="T114" id="Seg_1865" s="T113">adv</ta>
            <ta e="T115" id="Seg_1866" s="T114">v</ta>
            <ta e="T116" id="Seg_1867" s="T115">dempro</ta>
            <ta e="T117" id="Seg_1868" s="T116">v</ta>
            <ta e="T118" id="Seg_1869" s="T117">n</ta>
            <ta e="T119" id="Seg_1870" s="T118">conj</ta>
            <ta e="T120" id="Seg_1871" s="T119">v</ta>
            <ta e="T121" id="Seg_1872" s="T120">adv</ta>
            <ta e="T122" id="Seg_1873" s="T121">dempro</ta>
            <ta e="T123" id="Seg_1874" s="T122">n</ta>
            <ta e="T124" id="Seg_1875" s="T123">v</ta>
            <ta e="T125" id="Seg_1876" s="T124">conj</ta>
            <ta e="T126" id="Seg_1877" s="T125">pers</ta>
            <ta e="T127" id="Seg_1878" s="T126">ptcl</ta>
            <ta e="T128" id="Seg_1879" s="T127">v</ta>
            <ta e="T129" id="Seg_1880" s="T128">conj</ta>
            <ta e="T130" id="Seg_1881" s="T129">dempro</ta>
            <ta e="T131" id="Seg_1882" s="T130">v</ta>
            <ta e="T132" id="Seg_1883" s="T131">ptcl</ta>
            <ta e="T133" id="Seg_1884" s="T132">dempro</ta>
            <ta e="T134" id="Seg_1885" s="T133">n</ta>
            <ta e="T135" id="Seg_1886" s="T134">ptcl</ta>
            <ta e="T136" id="Seg_1887" s="T135">v</ta>
            <ta e="T137" id="Seg_1888" s="T136">conj</ta>
            <ta e="T138" id="Seg_1889" s="T137">adv</ta>
            <ta e="T139" id="Seg_1890" s="T138">ptcl</ta>
            <ta e="T140" id="Seg_1891" s="T139">v</ta>
            <ta e="T141" id="Seg_1892" s="T140">adv</ta>
            <ta e="T142" id="Seg_1893" s="T141">adv</ta>
            <ta e="T145" id="Seg_1894" s="T144">conj</ta>
            <ta e="T146" id="Seg_1895" s="T145">n</ta>
            <ta e="T147" id="Seg_1896" s="T146">adv</ta>
            <ta e="T148" id="Seg_1897" s="T147">v</ta>
            <ta e="T149" id="Seg_1898" s="T148">dempro</ta>
            <ta e="T150" id="Seg_1899" s="T149">v</ta>
            <ta e="T151" id="Seg_1900" s="T150">n</ta>
            <ta e="T152" id="Seg_1901" s="T151">v</ta>
            <ta e="T153" id="Seg_1902" s="T152">n</ta>
            <ta e="T154" id="Seg_1903" s="T153">v</ta>
            <ta e="T155" id="Seg_1904" s="T154">v</ta>
            <ta e="T156" id="Seg_1905" s="T155">v</ta>
            <ta e="T157" id="Seg_1906" s="T156">dempro</ta>
            <ta e="T158" id="Seg_1907" s="T157">conj</ta>
            <ta e="T161" id="Seg_1908" s="T158">v</ta>
            <ta e="T159" id="Seg_1909" s="T161">v</ta>
            <ta e="T160" id="Seg_1910" s="T159">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_1911" s="T2">np.h:E</ta>
            <ta e="T5" id="Seg_1912" s="T4">adv:Time</ta>
            <ta e="T9" id="Seg_1913" s="T8">np:A</ta>
            <ta e="T11" id="Seg_1914" s="T10">np:G</ta>
            <ta e="T13" id="Seg_1915" s="T12">pro.h:A</ta>
            <ta e="T16" id="Seg_1916" s="T15">pro:A</ta>
            <ta e="T17" id="Seg_1917" s="T16">adv:L</ta>
            <ta e="T19" id="Seg_1918" s="T18">pro.h:A</ta>
            <ta e="T20" id="Seg_1919" s="T19">pro.h:R</ta>
            <ta e="T23" id="Seg_1920" s="T22">0.1.h:E</ta>
            <ta e="T24" id="Seg_1921" s="T23">pro.h:A</ta>
            <ta e="T26" id="Seg_1922" s="T25">pro.h:E</ta>
            <ta e="T29" id="Seg_1923" s="T28">adv:Time</ta>
            <ta e="T30" id="Seg_1924" s="T29">0.3.h:A</ta>
            <ta e="T31" id="Seg_1925" s="T30">np.h:Th</ta>
            <ta e="T33" id="Seg_1926" s="T32">0.3.h:A</ta>
            <ta e="T34" id="Seg_1927" s="T33">0.3.h:A</ta>
            <ta e="T35" id="Seg_1928" s="T34">pro:Th</ta>
            <ta e="T39" id="Seg_1929" s="T38">np:A</ta>
            <ta e="T41" id="Seg_1930" s="T40">pro.h:A</ta>
            <ta e="T44" id="Seg_1931" s="T43">pro:R</ta>
            <ta e="T45" id="Seg_1932" s="T44">np:Th</ta>
            <ta e="T46" id="Seg_1933" s="T45">0.3.h:A</ta>
            <ta e="T48" id="Seg_1934" s="T47">pro:Th</ta>
            <ta e="T49" id="Seg_1935" s="T48">0.3.h:A</ta>
            <ta e="T50" id="Seg_1936" s="T49">0.3.h:A</ta>
            <ta e="T51" id="Seg_1937" s="T50">0.2.h:A</ta>
            <ta e="T52" id="Seg_1938" s="T51">pro:R</ta>
            <ta e="T54" id="Seg_1939" s="T53">pro.h:A</ta>
            <ta e="T56" id="Seg_1940" s="T55">pro.h:E</ta>
            <ta e="T58" id="Seg_1941" s="T57">pro:A</ta>
            <ta e="T59" id="Seg_1942" s="T58">pro.h:P</ta>
            <ta e="T62" id="Seg_1943" s="T61">0.3:E</ta>
            <ta e="T63" id="Seg_1944" s="T62">adv:Time</ta>
            <ta e="T64" id="Seg_1945" s="T63">0.2.h:A</ta>
            <ta e="T66" id="Seg_1946" s="T65">0.2.h:A</ta>
            <ta e="T67" id="Seg_1947" s="T66">np:Th</ta>
            <ta e="T69" id="Seg_1948" s="T68">pro.h:Poss</ta>
            <ta e="T71" id="Seg_1949" s="T70">np:Th</ta>
            <ta e="T72" id="Seg_1950" s="T71">0.2.h:A</ta>
            <ta e="T74" id="Seg_1951" s="T73">pro:A</ta>
            <ta e="T75" id="Seg_1952" s="T74">pro.h:E</ta>
            <ta e="T77" id="Seg_1953" s="T76">pro.h:A</ta>
            <ta e="T78" id="Seg_1954" s="T77">pro.h:Poss</ta>
            <ta e="T79" id="Seg_1955" s="T78">np:Th</ta>
            <ta e="T80" id="Seg_1956" s="T79">np:G</ta>
            <ta e="T82" id="Seg_1957" s="T81">adv:Time</ta>
            <ta e="T83" id="Seg_1958" s="T82">pro.h:A</ta>
            <ta e="T84" id="Seg_1959" s="T83">adv:L</ta>
            <ta e="T87" id="Seg_1960" s="T86">np:A</ta>
            <ta e="T89" id="Seg_1961" s="T88">pro.h:A</ta>
            <ta e="T91" id="Seg_1962" s="T90">0.2:S</ta>
            <ta e="T92" id="Seg_1963" s="T91">pro.h:Th</ta>
            <ta e="T94" id="Seg_1964" s="T93">pro.h:A</ta>
            <ta e="T96" id="Seg_1965" s="T95">pro.h:Poss</ta>
            <ta e="T101" id="Seg_1966" s="T100">np:P</ta>
            <ta e="T103" id="Seg_1967" s="T102">adv:Time</ta>
            <ta e="T104" id="Seg_1968" s="T103">pro.h:A</ta>
            <ta e="T106" id="Seg_1969" s="T105">np:Th</ta>
            <ta e="T107" id="Seg_1970" s="T106">np:Th</ta>
            <ta e="T110" id="Seg_1971" s="T109">0.3.h:A</ta>
            <ta e="T111" id="Seg_1972" s="T110">pro.h:E</ta>
            <ta e="T112" id="Seg_1973" s="T111">np:G</ta>
            <ta e="T114" id="Seg_1974" s="T113">adv:Time</ta>
            <ta e="T115" id="Seg_1975" s="T114">0.3.h:A</ta>
            <ta e="T116" id="Seg_1976" s="T115">pro.h:Poss</ta>
            <ta e="T118" id="Seg_1977" s="T117">np:Th</ta>
            <ta e="T120" id="Seg_1978" s="T119">0.3:Th</ta>
            <ta e="T121" id="Seg_1979" s="T120">adv:Time</ta>
            <ta e="T122" id="Seg_1980" s="T121">pro.h:Poss</ta>
            <ta e="T123" id="Seg_1981" s="T122">np.h:E</ta>
            <ta e="T124" id="Seg_1982" s="T123">0.3.h:A</ta>
            <ta e="T126" id="Seg_1983" s="T125">pro.h:E</ta>
            <ta e="T130" id="Seg_1984" s="T129">pro.h:A</ta>
            <ta e="T133" id="Seg_1985" s="T132">pro.h:Poss</ta>
            <ta e="T134" id="Seg_1986" s="T133">np:Th</ta>
            <ta e="T138" id="Seg_1987" s="T137">adv:Time</ta>
            <ta e="T140" id="Seg_1988" s="T139">0.1.h:E</ta>
            <ta e="T141" id="Seg_1989" s="T140">adv:Time</ta>
            <ta e="T142" id="Seg_1990" s="T141">adv:L</ta>
            <ta e="T144" id="Seg_1991" s="T143">0.3.h:A</ta>
            <ta e="T146" id="Seg_1992" s="T145">np:Th</ta>
            <ta e="T147" id="Seg_1993" s="T146">adv:L</ta>
            <ta e="T149" id="Seg_1994" s="T148">pro.h:A</ta>
            <ta e="T151" id="Seg_1995" s="T150">np:Th</ta>
            <ta e="T152" id="Seg_1996" s="T151">0.3.h:A</ta>
            <ta e="T153" id="Seg_1997" s="T152">np:P</ta>
            <ta e="T154" id="Seg_1998" s="T153">0.3.h:A</ta>
            <ta e="T155" id="Seg_1999" s="T154">0.3.h:A</ta>
            <ta e="T156" id="Seg_2000" s="T155">0.3.h:A</ta>
            <ta e="T157" id="Seg_2001" s="T156">pro:P</ta>
            <ta e="T159" id="Seg_2002" s="T161">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_2003" s="T0">v:pred</ta>
            <ta e="T3" id="Seg_2004" s="T2">np.h:S</ta>
            <ta e="T7" id="Seg_2005" s="T6">conv:pred</ta>
            <ta e="T8" id="Seg_2006" s="T7">v:pred</ta>
            <ta e="T9" id="Seg_2007" s="T8">np:S</ta>
            <ta e="T10" id="Seg_2008" s="T9">v:pred</ta>
            <ta e="T13" id="Seg_2009" s="T12">pro.h:S</ta>
            <ta e="T16" id="Seg_2010" s="T15">pro:S</ta>
            <ta e="T18" id="Seg_2011" s="T17">v:pred</ta>
            <ta e="T19" id="Seg_2012" s="T18">pro.h:S</ta>
            <ta e="T21" id="Seg_2013" s="T20">v:pred</ta>
            <ta e="T22" id="Seg_2014" s="T21">ptcl.neg</ta>
            <ta e="T23" id="Seg_2015" s="T22">v:pred 0.1.h:S</ta>
            <ta e="T24" id="Seg_2016" s="T23">pro.h:S</ta>
            <ta e="T25" id="Seg_2017" s="T24">v:pred</ta>
            <ta e="T26" id="Seg_2018" s="T25">pro.h:S</ta>
            <ta e="T27" id="Seg_2019" s="T26">ptcl.neg</ta>
            <ta e="T28" id="Seg_2020" s="T27">v:pred</ta>
            <ta e="T30" id="Seg_2021" s="T29">v:pred 0.3.h:S</ta>
            <ta e="T31" id="Seg_2022" s="T30">np.h:O</ta>
            <ta e="T33" id="Seg_2023" s="T32">v:pred 0.3.h:S</ta>
            <ta e="T34" id="Seg_2024" s="T33">v:pred 0.3.h:S</ta>
            <ta e="T35" id="Seg_2025" s="T34">pro:S</ta>
            <ta e="T36" id="Seg_2026" s="T35">v:pred</ta>
            <ta e="T39" id="Seg_2027" s="T38">np:S</ta>
            <ta e="T40" id="Seg_2028" s="T39">v:pred</ta>
            <ta e="T41" id="Seg_2029" s="T40">pro.h:S</ta>
            <ta e="T42" id="Seg_2030" s="T41">ptcl:pred</ta>
            <ta e="T44" id="Seg_2031" s="T43">pro:O</ta>
            <ta e="T45" id="Seg_2032" s="T44">np:O</ta>
            <ta e="T46" id="Seg_2033" s="T45">v:pred 0.3.h:S</ta>
            <ta e="T48" id="Seg_2034" s="T47">pro:O</ta>
            <ta e="T49" id="Seg_2035" s="T48">v:pred 0.3.h:S</ta>
            <ta e="T50" id="Seg_2036" s="T49">v:pred 0.3.h:S</ta>
            <ta e="T51" id="Seg_2037" s="T50">v:pred 0.2.h:S</ta>
            <ta e="T53" id="Seg_2038" s="T52">s:purp</ta>
            <ta e="T54" id="Seg_2039" s="T53">pro.h:S</ta>
            <ta e="T55" id="Seg_2040" s="T54">v:pred</ta>
            <ta e="T56" id="Seg_2041" s="T55">pro.h:S</ta>
            <ta e="T57" id="Seg_2042" s="T56">v:pred</ta>
            <ta e="T58" id="Seg_2043" s="T57">pro:S</ta>
            <ta e="T59" id="Seg_2044" s="T58">pro.h:O</ta>
            <ta e="T61" id="Seg_2045" s="T60">v:pred</ta>
            <ta e="T62" id="Seg_2046" s="T61">v:pred 0.3:S</ta>
            <ta e="T64" id="Seg_2047" s="T63">v:pred 0.2.h:S</ta>
            <ta e="T66" id="Seg_2048" s="T65">v:pred 0.2.h:S</ta>
            <ta e="T67" id="Seg_2049" s="T66">np:O</ta>
            <ta e="T71" id="Seg_2050" s="T70">np:O</ta>
            <ta e="T72" id="Seg_2051" s="T71">v:pred 0.2.h:S</ta>
            <ta e="T74" id="Seg_2052" s="T73">pro:S</ta>
            <ta e="T75" id="Seg_2053" s="T74">pro.h:O</ta>
            <ta e="T76" id="Seg_2054" s="T75">v:pred</ta>
            <ta e="T77" id="Seg_2055" s="T76">pro.h:S</ta>
            <ta e="T79" id="Seg_2056" s="T78">np:O</ta>
            <ta e="T81" id="Seg_2057" s="T80">v:pred</ta>
            <ta e="T83" id="Seg_2058" s="T82">pro.h:S</ta>
            <ta e="T86" id="Seg_2059" s="T85">v:pred</ta>
            <ta e="T87" id="Seg_2060" s="T86">np:S</ta>
            <ta e="T88" id="Seg_2061" s="T87">v:pred</ta>
            <ta e="T89" id="Seg_2062" s="T88">pro.h:S</ta>
            <ta e="T90" id="Seg_2063" s="T89">v:pred</ta>
            <ta e="T91" id="Seg_2064" s="T90">v:pred 0.2:S</ta>
            <ta e="T92" id="Seg_2065" s="T91">pro.h:O</ta>
            <ta e="T94" id="Seg_2066" s="T93">pro.h:S</ta>
            <ta e="T101" id="Seg_2067" s="T100">np:O</ta>
            <ta e="T104" id="Seg_2068" s="T103">pro.h:S</ta>
            <ta e="T105" id="Seg_2069" s="T104">v:pred</ta>
            <ta e="T106" id="Seg_2070" s="T105">np:O</ta>
            <ta e="T107" id="Seg_2071" s="T106">np:S</ta>
            <ta e="T108" id="Seg_2072" s="T107">v:pred</ta>
            <ta e="T110" id="Seg_2073" s="T109">v:pred 0.3.h:S</ta>
            <ta e="T111" id="Seg_2074" s="T110">pro.h:S</ta>
            <ta e="T113" id="Seg_2075" s="T112">v:pred</ta>
            <ta e="T115" id="Seg_2076" s="T114">v:pred 0.3.h:S</ta>
            <ta e="T117" id="Seg_2077" s="T116">v:pred</ta>
            <ta e="T118" id="Seg_2078" s="T117">np:S</ta>
            <ta e="T120" id="Seg_2079" s="T119">v:pred 0.3:S</ta>
            <ta e="T123" id="Seg_2080" s="T122">np:O</ta>
            <ta e="T124" id="Seg_2081" s="T123">v:pred 0.3.h:S</ta>
            <ta e="T126" id="Seg_2082" s="T125">pro.h:S</ta>
            <ta e="T127" id="Seg_2083" s="T126">ptcl.neg</ta>
            <ta e="T128" id="Seg_2084" s="T127">v:pred</ta>
            <ta e="T129" id="Seg_2085" s="T128">s:temp</ta>
            <ta e="T130" id="Seg_2086" s="T129">pro.h:S</ta>
            <ta e="T131" id="Seg_2087" s="T130">v:pred</ta>
            <ta e="T134" id="Seg_2088" s="T133">np:S</ta>
            <ta e="T136" id="Seg_2089" s="T135">v:pred</ta>
            <ta e="T139" id="Seg_2090" s="T138">ptcl.neg</ta>
            <ta e="T140" id="Seg_2091" s="T139">v:pred 0.1.h:S</ta>
            <ta e="T144" id="Seg_2092" s="T143">v:pred 0.3.h:S</ta>
            <ta e="T146" id="Seg_2093" s="T145">np:S</ta>
            <ta e="T148" id="Seg_2094" s="T147">v:pred</ta>
            <ta e="T149" id="Seg_2095" s="T148">pro.h:S</ta>
            <ta e="T150" id="Seg_2096" s="T149">v:pred</ta>
            <ta e="T151" id="Seg_2097" s="T150">np:O</ta>
            <ta e="T152" id="Seg_2098" s="T151">v:pred 0.3.h:S</ta>
            <ta e="T153" id="Seg_2099" s="T152">np:O</ta>
            <ta e="T154" id="Seg_2100" s="T153">v:pred 0.3.h:S</ta>
            <ta e="T155" id="Seg_2101" s="T154">v:pred 0.3.h:S</ta>
            <ta e="T156" id="Seg_2102" s="T155">v:pred 0.3.h:S</ta>
            <ta e="T157" id="Seg_2103" s="T156">pro:O</ta>
            <ta e="T161" id="Seg_2104" s="T158">conv:pred</ta>
            <ta e="T159" id="Seg_2105" s="T161">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T42" id="Seg_2106" s="T41">RUS:mod</ta>
            <ta e="T45" id="Seg_2107" s="T44">RUS:cult</ta>
            <ta e="T47" id="Seg_2108" s="T46">RUS:mod</ta>
            <ta e="T48" id="Seg_2109" s="T47">TURK:gram(INDEF)</ta>
            <ta e="T60" id="Seg_2110" s="T59">RUS:mod</ta>
            <ta e="T68" id="Seg_2111" s="T67">RUS:gram</ta>
            <ta e="T95" id="Seg_2112" s="T94">TURK:disc</ta>
            <ta e="T102" id="Seg_2113" s="T101">TURK:disc</ta>
            <ta e="T109" id="Seg_2114" s="T108">TURK:disc</ta>
            <ta e="T119" id="Seg_2115" s="T118">RUS:gram</ta>
            <ta e="T125" id="Seg_2116" s="T124">RUS:gram</ta>
            <ta e="T132" id="Seg_2117" s="T131">RUS:gram</ta>
            <ta e="T135" id="Seg_2118" s="T134">TURK:disc</ta>
            <ta e="T137" id="Seg_2119" s="T136">RUS:gram</ta>
            <ta e="T145" id="Seg_2120" s="T144">RUS:gram</ta>
            <ta e="T158" id="Seg_2121" s="T157">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_2122" s="T0">Жило много глупых людей.</ta>
            <ta e="T11" id="Seg_2123" s="T4">[Однажды] прилетела птица, села на деревьях.</ta>
            <ta e="T18" id="Seg_2124" s="T11">Они говорят: кто там прилетел?</ta>
            <ta e="T21" id="Seg_2125" s="T18">Друг друга спрашивают.</ta>
            <ta e="T23" id="Seg_2126" s="T21">"Не знаю".</ta>
            <ta e="T28" id="Seg_2127" s="T23">Один говорит: "Я не знаю".</ta>
            <ta e="T30" id="Seg_2128" s="T28">Потом они пошли.</ta>
            <ta e="T33" id="Seg_2129" s="T30">Увидели одного человека.</ta>
            <ta e="T40" id="Seg_2130" s="T33">Он говорит: "Это солнце, это бог прилетел".</ta>
            <ta e="T44" id="Seg_2131" s="T40">Они [говорят]: "Надо его покормить".</ta>
            <ta e="T46" id="Seg_2132" s="T44">Принесли лепешки.</ta>
            <ta e="T49" id="Seg_2133" s="T46">Ещё что-то принесли.</ta>
            <ta e="T53" id="Seg_2134" s="T49">Говорят: "Дай ему есть".</ta>
            <ta e="T62" id="Seg_2135" s="T53">Тот говорит: "Я боюсь, он меня еще убьет, рассердится".</ta>
            <ta e="T65" id="Seg_2136" s="T62">Потом: "Принесите!</ta>
            <ta e="T67" id="Seg_2137" s="T65">Принесите веревку!</ta>
            <ta e="T72" id="Seg_2138" s="T67">И привяжите к моим ногам!</ta>
            <ta e="T81" id="Seg_2139" s="T72">Когда он меня ударит, вы стяните меня за ноги на землю.</ta>
            <ta e="T86" id="Seg_2140" s="T81">Он туда залез.</ta>
            <ta e="T88" id="Seg_2141" s="T86">Птица прилетела.</ta>
            <ta e="T90" id="Seg_2142" s="T88">Он кричит:</ta>
            <ta e="T92" id="Seg_2143" s="T90">"Возьмите меня!"</ta>
            <ta e="T102" id="Seg_2144" s="T92">Они его голову [оторвали].</ta>
            <ta e="T106" id="Seg_2145" s="T102">Потом они потянули его за ноги.</ta>
            <ta e="T110" id="Seg_2146" s="T106">Его голова осталась, они ее оторвали.</ta>
            <ta e="T113" id="Seg_2147" s="T110">Он упал на землю.</ta>
            <ta e="T120" id="Seg_2148" s="T113">Они говорят: "У него была голова или не было?"</ta>
            <ta e="T124" id="Seg_2149" s="T120">Они спросили его жену.</ta>
            <ta e="T136" id="Seg_2150" s="T124">"Я не знаю, когда он ел, его усы тряслись.</ta>
            <ta e="T140" id="Seg_2151" s="T136">А теперь я не знаю".</ta>
            <ta e="T148" id="Seg_2152" s="T140">Тогда они туда вскарабкались, а его голова там (сидит).</ta>
            <ta e="T152" id="Seg_2153" s="T148">Они принесли голову, положили [ее].</ta>
            <ta e="T155" id="Seg_2154" s="T152">Выкопали землю, положили [ее].</ta>
            <ta e="T157" id="Seg_2155" s="T155">Закопали его.</ta>
            <ta e="T159" id="Seg_2156" s="T157">И ушли.</ta>
            <ta e="T160" id="Seg_2157" s="T159">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_2158" s="T0">There lived a lot of foolish people.</ta>
            <ta e="T11" id="Seg_2159" s="T4">[Once] a bird came flying, sat in the trees.</ta>
            <ta e="T18" id="Seg_2160" s="T11">They say: who came here?</ta>
            <ta e="T21" id="Seg_2161" s="T18">They are asking each other.</ta>
            <ta e="T23" id="Seg_2162" s="T21">"I do not know."</ta>
            <ta e="T28" id="Seg_2163" s="T23">One says: "I do not know."</ta>
            <ta e="T30" id="Seg_2164" s="T28">Then they came.</ta>
            <ta e="T33" id="Seg_2165" s="T30">They saw one man.</ta>
            <ta e="T40" id="Seg_2166" s="T33">He says: "This is the sun, this is God [that] came."</ta>
            <ta e="T44" id="Seg_2167" s="T40">They [say]: "We should feed it."</ta>
            <ta e="T46" id="Seg_2168" s="T44">They brought loaves.</ta>
            <ta e="T49" id="Seg_2169" s="T46">They brought something else.</ta>
            <ta e="T53" id="Seg_2170" s="T49">[They] say: "Give it to eat!"</ta>
            <ta e="T62" id="Seg_2171" s="T53">He says: "I am afraid, perhaps it will kill me, it will get angry."</ta>
            <ta e="T65" id="Seg_2172" s="T62">Then: "Bring!</ta>
            <ta e="T67" id="Seg_2173" s="T65">Bring a rope!</ta>
            <ta e="T72" id="Seg_2174" s="T67">And tie [it] to my feet!</ta>
            <ta e="T81" id="Seg_2175" s="T72">When it will hit me, you pull my feet to the ground.</ta>
            <ta e="T86" id="Seg_2176" s="T81">Then he climbed there.</ta>
            <ta e="T88" id="Seg_2177" s="T86">The bird flew.</ta>
            <ta e="T90" id="Seg_2178" s="T88">He is shouting:</ta>
            <ta e="T92" id="Seg_2179" s="T90">"Take me!"</ta>
            <ta e="T102" id="Seg_2180" s="T92">They [tore] off his head.</ta>
            <ta e="T106" id="Seg_2181" s="T102">Then they pulled on his legs.</ta>
            <ta e="T110" id="Seg_2182" s="T106">His head stayed, they pulled it off.</ta>
            <ta e="T113" id="Seg_2183" s="T110">He fell on the ground.</ta>
            <ta e="T120" id="Seg_2184" s="T113">Then they say: "Did he have a head or did he not?"</ta>
            <ta e="T124" id="Seg_2185" s="T120">Then they ask his wife.</ta>
            <ta e="T136" id="Seg_2186" s="T124">"I don't know, when it ate, his moustache was trembling.</ta>
            <ta e="T140" id="Seg_2187" s="T136">And now I don’t know."</ta>
            <ta e="T148" id="Seg_2188" s="T140">Then they climbed there, and his head is (sitting) there.</ta>
            <ta e="T152" id="Seg_2189" s="T148">They brought his head, put [it].</ta>
            <ta e="T155" id="Seg_2190" s="T152">They dug the ground, put [it].</ta>
            <ta e="T157" id="Seg_2191" s="T155">Buried him (dug in).</ta>
            <ta e="T159" id="Seg_2192" s="T157">And left.</ta>
            <ta e="T160" id="Seg_2193" s="T159">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_2194" s="T0">Es lebten viele dumme Leute.</ta>
            <ta e="T11" id="Seg_2195" s="T4">[EInmal] kam ein Vogel geflogen, er setzte sich auf die Bäume.</ta>
            <ta e="T18" id="Seg_2196" s="T11">Sie sagen: wer ist da gekommen?</ta>
            <ta e="T21" id="Seg_2197" s="T18">Sie fragen einander.</ta>
            <ta e="T23" id="Seg_2198" s="T21">"Ich weiß nicht."</ta>
            <ta e="T28" id="Seg_2199" s="T23">Einer sagt: "Ich weiß nicht."</ta>
            <ta e="T30" id="Seg_2200" s="T28">Dann kamen sie.</ta>
            <ta e="T33" id="Seg_2201" s="T30">Sie sahen einen Mann.</ta>
            <ta e="T40" id="Seg_2202" s="T33">Er sagt: "Es ist die Sonne, es [ist] Gott, [der] gekommen ist."</ta>
            <ta e="T44" id="Seg_2203" s="T40">Sie [sagen]: "Wir sollten ihn beköstigen."</ta>
            <ta e="T46" id="Seg_2204" s="T44">Sie brachten Fladen.</ta>
            <ta e="T49" id="Seg_2205" s="T46">Sie brachten noch etwas.</ta>
            <ta e="T53" id="Seg_2206" s="T49">[Sie] sagen: "Gib ihm zu essen!"</ta>
            <ta e="T62" id="Seg_2207" s="T53">Er sagt: "Ich habe Angst, vielleicht tötet er mich, ich werde böse."</ta>
            <ta e="T65" id="Seg_2208" s="T62">Dann: "Bring!</ta>
            <ta e="T67" id="Seg_2209" s="T65">Bring ein Seil!</ta>
            <ta e="T72" id="Seg_2210" s="T67">Und binde [es] mir an die Füße!</ta>
            <ta e="T81" id="Seg_2211" s="T72">Wenn er mich schlägt, zieht mich an den Füßen auf den Boden."</ta>
            <ta e="T86" id="Seg_2212" s="T81">Dann kletterte er dorthin.</ta>
            <ta e="T88" id="Seg_2213" s="T86">Der Vogel flog.</ta>
            <ta e="T90" id="Seg_2214" s="T88">Er schreit:</ta>
            <ta e="T92" id="Seg_2215" s="T90">"Nimm mich!"</ta>
            <ta e="T102" id="Seg_2216" s="T92">Sie [zogen] seinen Kopf ab.</ta>
            <ta e="T106" id="Seg_2217" s="T102">Dann zogen sie an seine Beine.</ta>
            <ta e="T110" id="Seg_2218" s="T106">Sein Kopf blieb, sie zogen ihn ab.</ta>
            <ta e="T113" id="Seg_2219" s="T110">Er fiel auf den Boden.</ta>
            <ta e="T120" id="Seg_2220" s="T113">Dann sagen sie: "Hatte er einen Kopf oder nicht?"</ta>
            <ta e="T124" id="Seg_2221" s="T120">Dann fragen sie seine Frau.</ta>
            <ta e="T136" id="Seg_2222" s="T124">"Ich weiß nicht, wenn er aß, zitterte sein Schnurrbart.</ta>
            <ta e="T140" id="Seg_2223" s="T136">Und jetzt weiß ich nicht."</ta>
            <ta e="T148" id="Seg_2224" s="T140">Dann kletterten sie dorthin und sein Kopf (sitzt) dort.</ta>
            <ta e="T152" id="Seg_2225" s="T148">Sie brachten seinen Kopf, legten [ihn] hin.</ta>
            <ta e="T155" id="Seg_2226" s="T152">Sie gruben im Boden, legten [ihn] hin.</ta>
            <ta e="T157" id="Seg_2227" s="T155">Begruben ihn (gruben ein).</ta>
            <ta e="T159" id="Seg_2228" s="T157">Und gingen weg.</ta>
            <ta e="T160" id="Seg_2229" s="T159">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T4" id="Seg_2230" s="T0">[GVY:] šagəštət</ta>
            <ta e="T72" id="Seg_2231" s="T67">[GVY:] ujunzeŋdə (mistakenly)</ta>
            <ta e="T136" id="Seg_2232" s="T124">[GVY:] amorbiʔ / amorbiʔi? The glottal stop in muʔzeŋ is pronounced like [k] before s.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T161" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
