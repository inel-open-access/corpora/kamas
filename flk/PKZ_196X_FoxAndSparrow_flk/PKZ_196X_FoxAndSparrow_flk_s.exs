<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID7E4A01E9-66C1-EED0-A35B-C58A9C595C4F">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_FoxAndSparrow_flk.wav" />
         <referenced-file url="PKZ_196X_FoxAndSparrow_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_FoxAndSparrow_flk\PKZ_196X_FoxAndSparrow_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">192</ud-information>
            <ud-information attribute-name="# HIAT:w">125</ud-information>
            <ud-information attribute-name="# e">125</ud-information>
            <ud-information attribute-name="# HIAT:u">27</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T932" time="0.0" type="appl" />
         <tli id="T933" time="0.568" type="appl" />
         <tli id="T934" time="1.136" type="appl" />
         <tli id="T935" time="1.703" type="appl" />
         <tli id="T936" time="2.271" type="appl" />
         <tli id="T937" time="2.839" type="appl" />
         <tli id="T938" time="3.435" type="appl" />
         <tli id="T939" time="4.031" type="appl" />
         <tli id="T940" time="4.627" type="appl" />
         <tli id="T941" time="5.222" type="appl" />
         <tli id="T942" time="5.818" type="appl" />
         <tli id="T943" time="6.414" type="appl" />
         <tli id="T944" time="7.01" type="appl" />
         <tli id="T945" time="8.618" type="appl" />
         <tli id="T946" time="9.938" type="appl" />
         <tli id="T947" time="11.257" type="appl" />
         <tli id="T948" time="12.576" type="appl" />
         <tli id="T949" time="13.569" type="appl" />
         <tli id="T950" time="14.391" type="appl" />
         <tli id="T951" time="15.53287879632382" />
         <tli id="T952" time="16.289" type="appl" />
         <tli id="T953" time="17.12" type="appl" />
         <tli id="T954" time="17.95" type="appl" />
         <tli id="T955" time="18.78" type="appl" />
         <tli id="T956" time="19.61" type="appl" />
         <tli id="T957" time="20.441" type="appl" />
         <tli id="T958" time="21.271" type="appl" />
         <tli id="T959" time="21.849" type="appl" />
         <tli id="T960" time="22.323" type="appl" />
         <tli id="T961" time="22.798" type="appl" />
         <tli id="T962" time="23.555" type="appl" />
         <tli id="T963" time="24.312" type="appl" />
         <tli id="T964" time="25.068" type="appl" />
         <tli id="T965" time="25.825" type="appl" />
         <tli id="T966" time="26.582" type="appl" />
         <tli id="T967" time="27.245" type="appl" />
         <tli id="T968" time="27.901" type="appl" />
         <tli id="T969" time="28.979151986102853" />
         <tli id="T970" time="30.19" type="appl" />
         <tli id="T971" time="31.925732427293898" />
         <tli id="T972" time="32.855" type="appl" />
         <tli id="T973" time="33.736" type="appl" />
         <tli id="T974" time="34.402" type="appl" />
         <tli id="T975" time="35.069" type="appl" />
         <tli id="T976" time="35.735" type="appl" />
         <tli id="T977" time="36.402" type="appl" />
         <tli id="T978" time="37.068" type="appl" />
         <tli id="T979" time="37.734" type="appl" />
         <tli id="T980" time="38.401" type="appl" />
         <tli id="T981" time="39.258851172339476" />
         <tli id="T982" time="40.22" type="appl" />
         <tli id="T983" time="41.373" type="appl" />
         <tli id="T984" time="43.152070578800036" />
         <tli id="T985" time="44.656" type="appl" />
         <tli id="T986" time="45.87" type="appl" />
         <tli id="T987" time="46.623" type="appl" />
         <tli id="T988" time="47.376" type="appl" />
         <tli id="T989" time="48.129" type="appl" />
         <tli id="T990" time="48.882" type="appl" />
         <tli id="T991" time="49.462" type="appl" />
         <tli id="T992" time="51.791817754781015" />
         <tli id="T993" time="52.942" type="appl" />
         <tli id="T994" time="53.768" type="appl" />
         <tli id="T995" time="54.253" type="appl" />
         <tli id="T996" time="54.633" type="appl" />
         <tli id="T997" time="55.013" type="appl" />
         <tli id="T998" time="55.591" type="appl" />
         <tli id="T999" time="56.124" type="appl" />
         <tli id="T1000" time="56.657" type="appl" />
         <tli id="T1001" time="57.19" type="appl" />
         <tli id="T1002" time="57.723" type="appl" />
         <tli id="T1003" time="58.256" type="appl" />
         <tli id="T1004" time="59.05160531237614" />
         <tli id="T1005" time="59.596" type="appl" />
         <tli id="T1006" time="60.404" type="appl" />
         <tli id="T1007" time="61.211" type="appl" />
         <tli id="T1008" time="61.773" type="appl" />
         <tli id="T1009" time="62.334" type="appl" />
         <tli id="T1010" time="62.896" type="appl" />
         <tli id="T1011" time="63.457" type="appl" />
         <tli id="T1012" time="64.83143617778934" />
         <tli id="T1013" time="65.606" type="appl" />
         <tli id="T1014" time="66.383" type="appl" />
         <tli id="T1015" time="67.161" type="appl" />
         <tli id="T1016" time="67.938" type="appl" />
         <tli id="T1017" time="68.716" type="appl" />
         <tli id="T1018" time="69.493" type="appl" />
         <tli id="T1019" time="71.40457716198473" />
         <tli id="T1020" time="72.467" type="appl" />
         <tli id="T1021" time="73.348" type="appl" />
         <tli id="T1022" time="74.23" type="appl" />
         <tli id="T1023" time="75.111" type="appl" />
         <tli id="T1024" time="75.85" type="appl" />
         <tli id="T1025" time="76.589" type="appl" />
         <tli id="T1026" time="77.328" type="appl" />
         <tli id="T1027" time="78.067" type="appl" />
         <tli id="T1028" time="78.806" type="appl" />
         <tli id="T1029" time="79.545" type="appl" />
         <tli id="T1030" time="80.284" type="appl" />
         <tli id="T1031" time="81.023" type="appl" />
         <tli id="T1032" time="81.959" type="appl" />
         <tli id="T1033" time="82.666" type="appl" />
         <tli id="T1034" time="83.372" type="appl" />
         <tli id="T1035" time="84.078" type="appl" />
         <tli id="T1036" time="84.784" type="appl" />
         <tli id="T1037" time="85.491" type="appl" />
         <tli id="T1038" time="86.197" type="appl" />
         <tli id="T1039" time="87.08" type="appl" />
         <tli id="T1040" time="87.964" type="appl" />
         <tli id="T1041" time="88.847" type="appl" />
         <tli id="T1042" time="89.731" type="appl" />
         <tli id="T1043" time="90.614" type="appl" />
         <tli id="T1044" time="91.498" type="appl" />
         <tli id="T1045" time="92.381" type="appl" />
         <tli id="T1046" time="92.995" type="appl" />
         <tli id="T1047" time="93.61" type="appl" />
         <tli id="T1048" time="94.224" type="appl" />
         <tli id="T1049" time="94.839" type="appl" />
         <tli id="T1050" time="95.453" type="appl" />
         <tli id="T1051" time="96.068" type="appl" />
         <tli id="T1052" time="96.682" type="appl" />
         <tli id="T1053" time="97.47714753641498" />
         <tli id="T1054" time="98.208" type="appl" />
         <tli id="T1055" time="98.976" type="appl" />
         <tli id="T1056" time="99.743" type="appl" />
         <tli id="T1057" time="100.788" type="appl" />
         <tli id="T0" time="100.917" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T1057" id="Seg_0" n="sc" s="T932">
               <ts e="T937" id="Seg_2" n="HIAT:u" s="T932">
                  <ts e="T933" id="Seg_4" n="HIAT:w" s="T932">Lʼisa</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T934" id="Seg_7" n="HIAT:w" s="T933">i</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T935" id="Seg_10" n="HIAT:w" s="T934">varabej</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T936" id="Seg_13" n="HIAT:w" s="T935">šidegöʔ</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T937" id="Seg_16" n="HIAT:w" s="T936">amnobiʔi</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T944" id="Seg_20" n="HIAT:u" s="T937">
                  <ts e="T938" id="Seg_22" n="HIAT:w" s="T937">Gibər</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T939" id="Seg_25" n="HIAT:w" s="T938">lʼisa</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T940" id="Seg_28" n="HIAT:w" s="T939">mĭnləj</ts>
                  <nts id="Seg_29" n="HIAT:ip">,</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T941" id="Seg_32" n="HIAT:w" s="T940">dibər</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T942" id="Seg_35" n="HIAT:w" s="T941">i</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T943" id="Seg_38" n="HIAT:w" s="T942">varabej</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T944" id="Seg_41" n="HIAT:w" s="T943">mĭmbi</ts>
                  <nts id="Seg_42" n="HIAT:ip">.</nts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T948" id="Seg_45" n="HIAT:u" s="T944">
                  <ts e="T945" id="Seg_47" n="HIAT:w" s="T944">Dĭgəttə</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T946" id="Seg_50" n="HIAT:w" s="T945">davaj</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T947" id="Seg_53" n="HIAT:w" s="T946">kuʔsittə</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T948" id="Seg_56" n="HIAT:w" s="T947">aš</ts>
                  <nts id="Seg_57" n="HIAT:ip">.</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T951" id="Seg_60" n="HIAT:u" s="T948">
                  <ts e="T949" id="Seg_62" n="HIAT:w" s="T948">Kuʔpiʔi</ts>
                  <nts id="Seg_63" n="HIAT:ip">,</nts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T950" id="Seg_66" n="HIAT:w" s="T949">dĭ</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T951" id="Seg_69" n="HIAT:w" s="T950">özerbi</ts>
                  <nts id="Seg_70" n="HIAT:ip">.</nts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T958" id="Seg_73" n="HIAT:u" s="T951">
                  <ts e="T952" id="Seg_75" n="HIAT:w" s="T951">Măndə:</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T953" id="Seg_78" n="HIAT:w" s="T952">măn</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T954" id="Seg_81" n="HIAT:w" s="T953">kallam</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T955" id="Seg_84" n="HIAT:w" s="T954">nʼuʔdə</ts>
                  <nts id="Seg_85" n="HIAT:ip">,</nts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T956" id="Seg_88" n="HIAT:w" s="T955">ej</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T957" id="Seg_91" n="HIAT:w" s="T956">öʔlim</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T958" id="Seg_94" n="HIAT:w" s="T957">döbər</ts>
                  <nts id="Seg_95" n="HIAT:ip">.</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T961" id="Seg_98" n="HIAT:u" s="T958">
                  <ts e="T959" id="Seg_100" n="HIAT:w" s="T958">A</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T960" id="Seg_103" n="HIAT:w" s="T959">tăn</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T961" id="Seg_106" n="HIAT:w" s="T960">püdeʔ</ts>
                  <nts id="Seg_107" n="HIAT:ip">.</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T966" id="Seg_110" n="HIAT:u" s="T961">
                  <ts e="T962" id="Seg_112" n="HIAT:w" s="T961">Dĭbər</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T963" id="Seg_115" n="HIAT:w" s="T962">varabej</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T964" id="Seg_118" n="HIAT:w" s="T963">uge</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T965" id="Seg_121" n="HIAT:w" s="T964">pĭdleʔbə</ts>
                  <nts id="Seg_122" n="HIAT:ip">,</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T966" id="Seg_125" n="HIAT:w" s="T965">püdleʔbə</ts>
                  <nts id="Seg_126" n="HIAT:ip">.</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T969" id="Seg_129" n="HIAT:u" s="T966">
                  <ts e="T967" id="Seg_131" n="HIAT:w" s="T966">A</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T968" id="Seg_134" n="HIAT:w" s="T967">lʼisa</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T969" id="Seg_137" n="HIAT:w" s="T968">amnolaʔbə</ts>
                  <nts id="Seg_138" n="HIAT:ip">.</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T971" id="Seg_141" n="HIAT:u" s="T969">
                  <ts e="T970" id="Seg_143" n="HIAT:w" s="T969">Dĭgəttə:</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_145" n="HIAT:ip">"</nts>
                  <ts e="T971" id="Seg_147" n="HIAT:w" s="T970">Šoʔ</ts>
                  <nts id="Seg_148" n="HIAT:ip">!</nts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T973" id="Seg_151" n="HIAT:u" s="T971">
                  <ts e="T972" id="Seg_153" n="HIAT:w" s="T971">Toʔnarzittə</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T973" id="Seg_156" n="HIAT:w" s="T972">ipek</ts>
                  <nts id="Seg_157" n="HIAT:ip">"</nts>
                  <nts id="Seg_158" n="HIAT:ip">.</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T981" id="Seg_161" n="HIAT:u" s="T973">
                  <nts id="Seg_162" n="HIAT:ip">(</nts>
                  <ts e="T974" id="Seg_164" n="HIAT:w" s="T973">Dĭgət-</ts>
                  <nts id="Seg_165" n="HIAT:ip">)</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T975" id="Seg_168" n="HIAT:w" s="T974">Dĭ</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T976" id="Seg_171" n="HIAT:w" s="T975">măndə:</ts>
                  <nts id="Seg_172" n="HIAT:ip">"</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T977" id="Seg_175" n="HIAT:w" s="T976">Măn</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T978" id="Seg_178" n="HIAT:w" s="T977">amnolam</ts>
                  <nts id="Seg_179" n="HIAT:ip">,</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T979" id="Seg_182" n="HIAT:w" s="T978">măna</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T980" id="Seg_185" n="HIAT:w" s="T979">nʼelʼzʼa</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T981" id="Seg_188" n="HIAT:w" s="T980">kanzittə</ts>
                  <nts id="Seg_189" n="HIAT:ip">"</nts>
                  <nts id="Seg_190" n="HIAT:ip">.</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T984" id="Seg_193" n="HIAT:u" s="T981">
                  <ts e="T982" id="Seg_195" n="HIAT:w" s="T981">Dĭgəttə</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T983" id="Seg_198" n="HIAT:w" s="T982">dĭ</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T984" id="Seg_201" n="HIAT:w" s="T983">püdəbi</ts>
                  <nts id="Seg_202" n="HIAT:ip">.</nts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T986" id="Seg_205" n="HIAT:u" s="T984">
                  <ts e="T985" id="Seg_207" n="HIAT:w" s="T984">Arəmbi</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T986" id="Seg_210" n="HIAT:w" s="T985">dʼü</ts>
                  <nts id="Seg_211" n="HIAT:ip">.</nts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T990" id="Seg_214" n="HIAT:u" s="T986">
                  <ts e="T987" id="Seg_216" n="HIAT:w" s="T986">Dĭgəttə</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T988" id="Seg_219" n="HIAT:w" s="T987">bar</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_221" n="HIAT:ip">(</nts>
                  <ts e="T989" id="Seg_223" n="HIAT:w" s="T988">toʔnaːrzit-</ts>
                  <nts id="Seg_224" n="HIAT:ip">)</nts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T990" id="Seg_227" n="HIAT:w" s="T989">toʔnaːrbi</ts>
                  <nts id="Seg_228" n="HIAT:ip">.</nts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T992" id="Seg_231" n="HIAT:u" s="T990">
                  <ts e="T991" id="Seg_233" n="HIAT:w" s="T990">Măndə:</ts>
                  <nts id="Seg_234" n="HIAT:ip">"</nts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T992" id="Seg_237" n="HIAT:w" s="T991">Šoʔ</ts>
                  <nts id="Seg_238" n="HIAT:ip">!</nts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T994" id="Seg_241" n="HIAT:u" s="T992">
                  <ts e="T993" id="Seg_243" n="HIAT:w" s="T992">Ipek</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T994" id="Seg_246" n="HIAT:w" s="T993">iʔ</ts>
                  <nts id="Seg_247" n="HIAT:ip">.</nts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T997" id="Seg_250" n="HIAT:u" s="T994">
                  <ts e="T995" id="Seg_252" n="HIAT:w" s="T994">I</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T996" id="Seg_255" n="HIAT:w" s="T995">măn</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T997" id="Seg_258" n="HIAT:w" s="T996">ilim</ts>
                  <nts id="Seg_259" n="HIAT:ip">"</nts>
                  <nts id="Seg_260" n="HIAT:ip">.</nts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1004" id="Seg_263" n="HIAT:u" s="T997">
                  <ts e="T998" id="Seg_265" n="HIAT:w" s="T997">Dĭ</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T999" id="Seg_268" n="HIAT:w" s="T998">šide</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1000" id="Seg_271" n="HIAT:w" s="T999">ibi</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1001" id="Seg_274" n="HIAT:w" s="T1000">mʼera</ts>
                  <nts id="Seg_275" n="HIAT:ip">,</nts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1002" id="Seg_278" n="HIAT:w" s="T1001">a</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1003" id="Seg_281" n="HIAT:w" s="T1002">dĭʔnə</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1004" id="Seg_284" n="HIAT:w" s="T1003">onʼiʔ</ts>
                  <nts id="Seg_285" n="HIAT:ip">.</nts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1007" id="Seg_288" n="HIAT:u" s="T1004">
                  <ts e="T1005" id="Seg_290" n="HIAT:w" s="T1004">Dĭgəttə</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1006" id="Seg_293" n="HIAT:w" s="T1005">dĭ</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1007" id="Seg_296" n="HIAT:w" s="T1006">kambi</ts>
                  <nts id="Seg_297" n="HIAT:ip">.</nts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1019" id="Seg_300" n="HIAT:u" s="T1007">
                  <nts id="Seg_301" n="HIAT:ip">(</nts>
                  <ts e="T1008" id="Seg_303" n="HIAT:w" s="T1007">Men=</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1009" id="Seg_306" n="HIAT:w" s="T1008">m-</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1010" id="Seg_309" n="HIAT:w" s="T1009">m-</ts>
                  <nts id="Seg_310" n="HIAT:ip">)</nts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1011" id="Seg_313" n="HIAT:w" s="T1010">Men</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1012" id="Seg_316" n="HIAT:w" s="T1011">măndə:</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_318" n="HIAT:ip">"</nts>
                  <nts id="Seg_319" n="HIAT:ip">(</nts>
                  <ts e="T1013" id="Seg_321" n="HIAT:w" s="T1012">Dĭ</ts>
                  <nts id="Seg_322" n="HIAT:ip">)</nts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1014" id="Seg_325" n="HIAT:w" s="T1013">ĭmbi</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_327" n="HIAT:ip">(</nts>
                  <ts e="T1015" id="Seg_329" n="HIAT:w" s="T1014">dăn-</ts>
                  <nts id="Seg_330" n="HIAT:ip">)</nts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1016" id="Seg_333" n="HIAT:w" s="T1015">tăn</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_335" n="HIAT:ip">(</nts>
                  <ts e="T1017" id="Seg_337" n="HIAT:w" s="T1016">dĭrgit=</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1018" id="Seg_340" n="HIAT:w" s="T1017">ə-</ts>
                  <nts id="Seg_341" n="HIAT:ip">)</nts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1019" id="Seg_344" n="HIAT:w" s="T1018">dĭrgit</ts>
                  <nts id="Seg_345" n="HIAT:ip">"</nts>
                  <nts id="Seg_346" n="HIAT:ip">?</nts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1023" id="Seg_349" n="HIAT:u" s="T1019">
                  <nts id="Seg_350" n="HIAT:ip">"</nts>
                  <ts e="T1020" id="Seg_352" n="HIAT:w" s="T1019">Da</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1021" id="Seg_355" n="HIAT:w" s="T1020">măna</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1022" id="Seg_358" n="HIAT:w" s="T1021">lʼisʼitsa</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1023" id="Seg_361" n="HIAT:w" s="T1022">mʼegluʔpi</ts>
                  <nts id="Seg_362" n="HIAT:ip">"</nts>
                  <nts id="Seg_363" n="HIAT:ip">.</nts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1031" id="Seg_366" n="HIAT:u" s="T1023">
                  <nts id="Seg_367" n="HIAT:ip">"</nts>
                  <ts e="T1024" id="Seg_369" n="HIAT:w" s="T1023">Kanžəbəj</ts>
                  <nts id="Seg_370" n="HIAT:ip">,</nts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1025" id="Seg_373" n="HIAT:w" s="T1024">măn</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1026" id="Seg_376" n="HIAT:w" s="T1025">dĭm</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1027" id="Seg_379" n="HIAT:w" s="T1026">kabarlim</ts>
                  <nts id="Seg_380" n="HIAT:ip">"</nts>
                  <nts id="Seg_381" n="HIAT:ip">,</nts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1028" id="Seg_384" n="HIAT:w" s="T1027">dĭ</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1029" id="Seg_387" n="HIAT:w" s="T1028">kambi</ts>
                  <nts id="Seg_388" n="HIAT:ip">,</nts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1030" id="Seg_391" n="HIAT:w" s="T1029">ašdə</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1031" id="Seg_394" n="HIAT:w" s="T1030">šaʔlaːmbi</ts>
                  <nts id="Seg_395" n="HIAT:ip">.</nts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1038" id="Seg_398" n="HIAT:u" s="T1031">
                  <ts e="T1032" id="Seg_400" n="HIAT:w" s="T1031">Lʼisʼitsa</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1033" id="Seg_403" n="HIAT:w" s="T1032">šobi</ts>
                  <nts id="Seg_404" n="HIAT:ip">,</nts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1034" id="Seg_407" n="HIAT:w" s="T1033">tʼerməndə</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1035" id="Seg_410" n="HIAT:w" s="T1034">nada</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_412" n="HIAT:ip">(</nts>
                  <ts e="T1036" id="Seg_414" n="HIAT:w" s="T1035">ni-</ts>
                  <nts id="Seg_415" n="HIAT:ip">)</nts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1037" id="Seg_418" n="HIAT:w" s="T1036">kunzittə</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1038" id="Seg_421" n="HIAT:w" s="T1037">aš</ts>
                  <nts id="Seg_422" n="HIAT:ip">.</nts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1045" id="Seg_425" n="HIAT:u" s="T1038">
                  <ts e="T1039" id="Seg_427" n="HIAT:w" s="T1038">Dĭgəttə</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_429" n="HIAT:ip">(</nts>
                  <ts e="T1040" id="Seg_431" n="HIAT:w" s="T1039">ku-</ts>
                  <nts id="Seg_432" n="HIAT:ip">)</nts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1041" id="Seg_435" n="HIAT:w" s="T1040">kuliot</ts>
                  <nts id="Seg_436" n="HIAT:ip">,</nts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1042" id="Seg_439" n="HIAT:w" s="T1041">što</ts>
                  <nts id="Seg_440" n="HIAT:ip">,</nts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1043" id="Seg_443" n="HIAT:w" s="T1042">kut</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1044" id="Seg_446" n="HIAT:w" s="T1043">mendə</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1045" id="Seg_449" n="HIAT:w" s="T1044">iʔbəlaʔbə</ts>
                  <nts id="Seg_450" n="HIAT:ip">.</nts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1053" id="Seg_453" n="HIAT:u" s="T1045">
                  <ts e="T1046" id="Seg_455" n="HIAT:w" s="T1045">Dĭ</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1047" id="Seg_458" n="HIAT:w" s="T1046">kabarbi</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1048" id="Seg_461" n="HIAT:w" s="T1047">a</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1049" id="Seg_464" n="HIAT:w" s="T1048">men</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1050" id="Seg_467" n="HIAT:w" s="T1049">dĭm</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1051" id="Seg_470" n="HIAT:w" s="T1050">talbəluʔpi</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1052" id="Seg_473" n="HIAT:w" s="T1051">da</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1053" id="Seg_476" n="HIAT:w" s="T1052">külaːmbi</ts>
                  <nts id="Seg_477" n="HIAT:ip">.</nts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1056" id="Seg_480" n="HIAT:u" s="T1053">
                  <ts e="T1054" id="Seg_482" n="HIAT:w" s="T1053">I</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1055" id="Seg_485" n="HIAT:w" s="T1054">ašdə</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1056" id="Seg_488" n="HIAT:w" s="T1055">maːluʔpi</ts>
                  <nts id="Seg_489" n="HIAT:ip">.</nts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1057" id="Seg_492" n="HIAT:u" s="T1056">
                  <ts e="T1057" id="Seg_494" n="HIAT:w" s="T1056">Kabarləj</ts>
                  <nts id="Seg_495" n="HIAT:ip">.</nts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T1057" id="Seg_497" n="sc" s="T932">
               <ts e="T933" id="Seg_499" n="e" s="T932">Lʼisa </ts>
               <ts e="T934" id="Seg_501" n="e" s="T933">i </ts>
               <ts e="T935" id="Seg_503" n="e" s="T934">varabej </ts>
               <ts e="T936" id="Seg_505" n="e" s="T935">šidegöʔ </ts>
               <ts e="T937" id="Seg_507" n="e" s="T936">amnobiʔi. </ts>
               <ts e="T938" id="Seg_509" n="e" s="T937">Gibər </ts>
               <ts e="T939" id="Seg_511" n="e" s="T938">lʼisa </ts>
               <ts e="T940" id="Seg_513" n="e" s="T939">mĭnləj, </ts>
               <ts e="T941" id="Seg_515" n="e" s="T940">dibər </ts>
               <ts e="T942" id="Seg_517" n="e" s="T941">i </ts>
               <ts e="T943" id="Seg_519" n="e" s="T942">varabej </ts>
               <ts e="T944" id="Seg_521" n="e" s="T943">mĭmbi. </ts>
               <ts e="T945" id="Seg_523" n="e" s="T944">Dĭgəttə </ts>
               <ts e="T946" id="Seg_525" n="e" s="T945">davaj </ts>
               <ts e="T947" id="Seg_527" n="e" s="T946">kuʔsittə </ts>
               <ts e="T948" id="Seg_529" n="e" s="T947">aš. </ts>
               <ts e="T949" id="Seg_531" n="e" s="T948">Kuʔpiʔi, </ts>
               <ts e="T950" id="Seg_533" n="e" s="T949">dĭ </ts>
               <ts e="T951" id="Seg_535" n="e" s="T950">özerbi. </ts>
               <ts e="T952" id="Seg_537" n="e" s="T951">Măndə: </ts>
               <ts e="T953" id="Seg_539" n="e" s="T952">măn </ts>
               <ts e="T954" id="Seg_541" n="e" s="T953">kallam </ts>
               <ts e="T955" id="Seg_543" n="e" s="T954">nʼuʔdə, </ts>
               <ts e="T956" id="Seg_545" n="e" s="T955">ej </ts>
               <ts e="T957" id="Seg_547" n="e" s="T956">öʔlim </ts>
               <ts e="T958" id="Seg_549" n="e" s="T957">döbər. </ts>
               <ts e="T959" id="Seg_551" n="e" s="T958">A </ts>
               <ts e="T960" id="Seg_553" n="e" s="T959">tăn </ts>
               <ts e="T961" id="Seg_555" n="e" s="T960">püdeʔ. </ts>
               <ts e="T962" id="Seg_557" n="e" s="T961">Dĭbər </ts>
               <ts e="T963" id="Seg_559" n="e" s="T962">varabej </ts>
               <ts e="T964" id="Seg_561" n="e" s="T963">uge </ts>
               <ts e="T965" id="Seg_563" n="e" s="T964">pĭdleʔbə, </ts>
               <ts e="T966" id="Seg_565" n="e" s="T965">püdleʔbə. </ts>
               <ts e="T967" id="Seg_567" n="e" s="T966">A </ts>
               <ts e="T968" id="Seg_569" n="e" s="T967">lʼisa </ts>
               <ts e="T969" id="Seg_571" n="e" s="T968">amnolaʔbə. </ts>
               <ts e="T970" id="Seg_573" n="e" s="T969">Dĭgəttə: </ts>
               <ts e="T971" id="Seg_575" n="e" s="T970">"Šoʔ! </ts>
               <ts e="T972" id="Seg_577" n="e" s="T971">Toʔnarzittə </ts>
               <ts e="T973" id="Seg_579" n="e" s="T972">ipek". </ts>
               <ts e="T974" id="Seg_581" n="e" s="T973">(Dĭgət-) </ts>
               <ts e="T975" id="Seg_583" n="e" s="T974">Dĭ </ts>
               <ts e="T976" id="Seg_585" n="e" s="T975">măndə:" </ts>
               <ts e="T977" id="Seg_587" n="e" s="T976">Măn </ts>
               <ts e="T978" id="Seg_589" n="e" s="T977">amnolam, </ts>
               <ts e="T979" id="Seg_591" n="e" s="T978">măna </ts>
               <ts e="T980" id="Seg_593" n="e" s="T979">nʼelʼzʼa </ts>
               <ts e="T981" id="Seg_595" n="e" s="T980">kanzittə". </ts>
               <ts e="T982" id="Seg_597" n="e" s="T981">Dĭgəttə </ts>
               <ts e="T983" id="Seg_599" n="e" s="T982">dĭ </ts>
               <ts e="T984" id="Seg_601" n="e" s="T983">püdəbi. </ts>
               <ts e="T985" id="Seg_603" n="e" s="T984">Arəmbi </ts>
               <ts e="T986" id="Seg_605" n="e" s="T985">dʼü. </ts>
               <ts e="T987" id="Seg_607" n="e" s="T986">Dĭgəttə </ts>
               <ts e="T988" id="Seg_609" n="e" s="T987">bar </ts>
               <ts e="T989" id="Seg_611" n="e" s="T988">(toʔnaːrzit-) </ts>
               <ts e="T990" id="Seg_613" n="e" s="T989">toʔnaːrbi. </ts>
               <ts e="T991" id="Seg_615" n="e" s="T990">Măndə:" </ts>
               <ts e="T992" id="Seg_617" n="e" s="T991">Šoʔ! </ts>
               <ts e="T993" id="Seg_619" n="e" s="T992">Ipek </ts>
               <ts e="T994" id="Seg_621" n="e" s="T993">iʔ. </ts>
               <ts e="T995" id="Seg_623" n="e" s="T994">I </ts>
               <ts e="T996" id="Seg_625" n="e" s="T995">măn </ts>
               <ts e="T997" id="Seg_627" n="e" s="T996">ilim". </ts>
               <ts e="T998" id="Seg_629" n="e" s="T997">Dĭ </ts>
               <ts e="T999" id="Seg_631" n="e" s="T998">šide </ts>
               <ts e="T1000" id="Seg_633" n="e" s="T999">ibi </ts>
               <ts e="T1001" id="Seg_635" n="e" s="T1000">mʼera, </ts>
               <ts e="T1002" id="Seg_637" n="e" s="T1001">a </ts>
               <ts e="T1003" id="Seg_639" n="e" s="T1002">dĭʔnə </ts>
               <ts e="T1004" id="Seg_641" n="e" s="T1003">onʼiʔ. </ts>
               <ts e="T1005" id="Seg_643" n="e" s="T1004">Dĭgəttə </ts>
               <ts e="T1006" id="Seg_645" n="e" s="T1005">dĭ </ts>
               <ts e="T1007" id="Seg_647" n="e" s="T1006">kambi. </ts>
               <ts e="T1008" id="Seg_649" n="e" s="T1007">(Men= </ts>
               <ts e="T1009" id="Seg_651" n="e" s="T1008">m- </ts>
               <ts e="T1010" id="Seg_653" n="e" s="T1009">m-) </ts>
               <ts e="T1011" id="Seg_655" n="e" s="T1010">Men </ts>
               <ts e="T1012" id="Seg_657" n="e" s="T1011">măndə: </ts>
               <ts e="T1013" id="Seg_659" n="e" s="T1012">"(Dĭ) </ts>
               <ts e="T1014" id="Seg_661" n="e" s="T1013">ĭmbi </ts>
               <ts e="T1015" id="Seg_663" n="e" s="T1014">(dăn-) </ts>
               <ts e="T1016" id="Seg_665" n="e" s="T1015">tăn </ts>
               <ts e="T1017" id="Seg_667" n="e" s="T1016">(dĭrgit= </ts>
               <ts e="T1018" id="Seg_669" n="e" s="T1017">ə-) </ts>
               <ts e="T1019" id="Seg_671" n="e" s="T1018">dĭrgit"? </ts>
               <ts e="T1020" id="Seg_673" n="e" s="T1019">"Da </ts>
               <ts e="T1021" id="Seg_675" n="e" s="T1020">măna </ts>
               <ts e="T1022" id="Seg_677" n="e" s="T1021">lʼisʼitsa </ts>
               <ts e="T1023" id="Seg_679" n="e" s="T1022">mʼegluʔpi". </ts>
               <ts e="T1024" id="Seg_681" n="e" s="T1023">"Kanžəbəj, </ts>
               <ts e="T1025" id="Seg_683" n="e" s="T1024">măn </ts>
               <ts e="T1026" id="Seg_685" n="e" s="T1025">dĭm </ts>
               <ts e="T1027" id="Seg_687" n="e" s="T1026">kabarlim", </ts>
               <ts e="T1028" id="Seg_689" n="e" s="T1027">dĭ </ts>
               <ts e="T1029" id="Seg_691" n="e" s="T1028">kambi, </ts>
               <ts e="T1030" id="Seg_693" n="e" s="T1029">ašdə </ts>
               <ts e="T1031" id="Seg_695" n="e" s="T1030">šaʔlaːmbi. </ts>
               <ts e="T1032" id="Seg_697" n="e" s="T1031">Lʼisʼitsa </ts>
               <ts e="T1033" id="Seg_699" n="e" s="T1032">šobi, </ts>
               <ts e="T1034" id="Seg_701" n="e" s="T1033">tʼerməndə </ts>
               <ts e="T1035" id="Seg_703" n="e" s="T1034">nada </ts>
               <ts e="T1036" id="Seg_705" n="e" s="T1035">(ni-) </ts>
               <ts e="T1037" id="Seg_707" n="e" s="T1036">kunzittə </ts>
               <ts e="T1038" id="Seg_709" n="e" s="T1037">aš. </ts>
               <ts e="T1039" id="Seg_711" n="e" s="T1038">Dĭgəttə </ts>
               <ts e="T1040" id="Seg_713" n="e" s="T1039">(ku-) </ts>
               <ts e="T1041" id="Seg_715" n="e" s="T1040">kuliot, </ts>
               <ts e="T1042" id="Seg_717" n="e" s="T1041">što, </ts>
               <ts e="T1043" id="Seg_719" n="e" s="T1042">kut </ts>
               <ts e="T1044" id="Seg_721" n="e" s="T1043">mendə </ts>
               <ts e="T1045" id="Seg_723" n="e" s="T1044">iʔbəlaʔbə. </ts>
               <ts e="T1046" id="Seg_725" n="e" s="T1045">Dĭ </ts>
               <ts e="T1047" id="Seg_727" n="e" s="T1046">kabarbi </ts>
               <ts e="T1048" id="Seg_729" n="e" s="T1047">a </ts>
               <ts e="T1049" id="Seg_731" n="e" s="T1048">men </ts>
               <ts e="T1050" id="Seg_733" n="e" s="T1049">dĭm </ts>
               <ts e="T1051" id="Seg_735" n="e" s="T1050">talbəluʔpi </ts>
               <ts e="T1052" id="Seg_737" n="e" s="T1051">da </ts>
               <ts e="T1053" id="Seg_739" n="e" s="T1052">külaːmbi. </ts>
               <ts e="T1054" id="Seg_741" n="e" s="T1053">I </ts>
               <ts e="T1055" id="Seg_743" n="e" s="T1054">ašdə </ts>
               <ts e="T1056" id="Seg_745" n="e" s="T1055">maːluʔpi. </ts>
               <ts e="T1057" id="Seg_747" n="e" s="T1056">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T937" id="Seg_748" s="T932">PKZ_196X_FoxAndSparrow_flk.001 (001)</ta>
            <ta e="T944" id="Seg_749" s="T937">PKZ_196X_FoxAndSparrow_flk.002 (002)</ta>
            <ta e="T948" id="Seg_750" s="T944">PKZ_196X_FoxAndSparrow_flk.003 (003)</ta>
            <ta e="T951" id="Seg_751" s="T948">PKZ_196X_FoxAndSparrow_flk.004 (004)</ta>
            <ta e="T958" id="Seg_752" s="T951">PKZ_196X_FoxAndSparrow_flk.005 (005)</ta>
            <ta e="T961" id="Seg_753" s="T958">PKZ_196X_FoxAndSparrow_flk.006 (006)</ta>
            <ta e="T966" id="Seg_754" s="T961">PKZ_196X_FoxAndSparrow_flk.007 (007)</ta>
            <ta e="T969" id="Seg_755" s="T966">PKZ_196X_FoxAndSparrow_flk.008 (008)</ta>
            <ta e="T971" id="Seg_756" s="T969">PKZ_196X_FoxAndSparrow_flk.009 (009)</ta>
            <ta e="T973" id="Seg_757" s="T971">PKZ_196X_FoxAndSparrow_flk.010 (010)</ta>
            <ta e="T981" id="Seg_758" s="T973">PKZ_196X_FoxAndSparrow_flk.011 (011)</ta>
            <ta e="T984" id="Seg_759" s="T981">PKZ_196X_FoxAndSparrow_flk.012 (012)</ta>
            <ta e="T986" id="Seg_760" s="T984">PKZ_196X_FoxAndSparrow_flk.013 (013)</ta>
            <ta e="T990" id="Seg_761" s="T986">PKZ_196X_FoxAndSparrow_flk.014 (014)</ta>
            <ta e="T992" id="Seg_762" s="T990">PKZ_196X_FoxAndSparrow_flk.015 (015)</ta>
            <ta e="T994" id="Seg_763" s="T992">PKZ_196X_FoxAndSparrow_flk.016 (016)</ta>
            <ta e="T997" id="Seg_764" s="T994">PKZ_196X_FoxAndSparrow_flk.017 (017)</ta>
            <ta e="T1004" id="Seg_765" s="T997">PKZ_196X_FoxAndSparrow_flk.018 (018)</ta>
            <ta e="T1007" id="Seg_766" s="T1004">PKZ_196X_FoxAndSparrow_flk.019 (019)</ta>
            <ta e="T1019" id="Seg_767" s="T1007">PKZ_196X_FoxAndSparrow_flk.020 (020)</ta>
            <ta e="T1023" id="Seg_768" s="T1019">PKZ_196X_FoxAndSparrow_flk.021 (022)</ta>
            <ta e="T1031" id="Seg_769" s="T1023">PKZ_196X_FoxAndSparrow_flk.022 (023)</ta>
            <ta e="T1038" id="Seg_770" s="T1031">PKZ_196X_FoxAndSparrow_flk.023 (024)</ta>
            <ta e="T1045" id="Seg_771" s="T1038">PKZ_196X_FoxAndSparrow_flk.024 (025)</ta>
            <ta e="T1053" id="Seg_772" s="T1045">PKZ_196X_FoxAndSparrow_flk.025 (026)</ta>
            <ta e="T1056" id="Seg_773" s="T1053">PKZ_196X_FoxAndSparrow_flk.026 (027)</ta>
            <ta e="T1057" id="Seg_774" s="T1056">PKZ_196X_FoxAndSparrow_flk.027 (028)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T937" id="Seg_775" s="T932">Lʼisa i varabej šidegöʔ amnobiʔi. </ta>
            <ta e="T944" id="Seg_776" s="T937">Gibər lʼisa mĭnləj, dibər i varabej mĭmbi. </ta>
            <ta e="T948" id="Seg_777" s="T944">Dĭgəttə davaj kuʔsittə aš. </ta>
            <ta e="T951" id="Seg_778" s="T948">Kuʔpiʔi, dĭ özerbi. </ta>
            <ta e="T958" id="Seg_779" s="T951">Măndə: măn kallam nʼuʔdə, ej öʔlim döbər. </ta>
            <ta e="T961" id="Seg_780" s="T958">A tăn püdeʔ. </ta>
            <ta e="T966" id="Seg_781" s="T961">Dĭbər varabej uge pĭdleʔbə, püdleʔbə. </ta>
            <ta e="T969" id="Seg_782" s="T966">A lʼisa amnolaʔbə. </ta>
            <ta e="T971" id="Seg_783" s="T969">Dĭgəttə: "Šoʔ! </ta>
            <ta e="T973" id="Seg_784" s="T971">Toʔnarzittə ipek". </ta>
            <ta e="T981" id="Seg_785" s="T973">(Dĭgət-) Dĭ măndə:" Măn amnolam, măna nʼelʼzʼa kanzittə". </ta>
            <ta e="T984" id="Seg_786" s="T981">Dĭgəttə dĭ püdəbi. </ta>
            <ta e="T986" id="Seg_787" s="T984">Arəmbi dʼü. </ta>
            <ta e="T990" id="Seg_788" s="T986">Dĭgəttə bar (toʔnaːrzit-) toʔnaːrbi. </ta>
            <ta e="T992" id="Seg_789" s="T990">Măndə:" Šoʔ! </ta>
            <ta e="T994" id="Seg_790" s="T992">Ipek iʔ. </ta>
            <ta e="T997" id="Seg_791" s="T994">I măn ilim". </ta>
            <ta e="T1004" id="Seg_792" s="T997">Dĭ šide ibi mʼera, a dĭʔnə onʼiʔ. </ta>
            <ta e="T1007" id="Seg_793" s="T1004">Dĭgəttə dĭ kambi. </ta>
            <ta e="T1019" id="Seg_794" s="T1007">(Men= m- m-) Men măndə: "(Dĭ) ĭmbi (dăn-) tăn (dĭrgit= ə-) dĭrgit"? </ta>
            <ta e="T1023" id="Seg_795" s="T1019">"Da măna lʼisʼitsa mʼegluʔpi". </ta>
            <ta e="T1031" id="Seg_796" s="T1023">"Kanžəbəj, măn dĭm kabarlim", dĭ kambi, ašdə šaʔlaːmbi. </ta>
            <ta e="T1038" id="Seg_797" s="T1031">Lʼisʼitsa šobi, tʼerməndə nada (ni-) kunzittə aš. </ta>
            <ta e="T1045" id="Seg_798" s="T1038">Dĭgəttə (ku-) kuliot, što, kut mendə iʔbəlaʔbə. </ta>
            <ta e="T1053" id="Seg_799" s="T1045">Dĭ kabarbi a men dĭm talbəluʔpi da külaːmbi. </ta>
            <ta e="T1056" id="Seg_800" s="T1053">I ašdə maːluʔpi. </ta>
            <ta e="T1057" id="Seg_801" s="T1056">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T933" id="Seg_802" s="T932">lʼisa</ta>
            <ta e="T934" id="Seg_803" s="T933">i</ta>
            <ta e="T935" id="Seg_804" s="T934">varabej</ta>
            <ta e="T936" id="Seg_805" s="T935">šide-göʔ</ta>
            <ta e="T937" id="Seg_806" s="T936">amno-bi-ʔi</ta>
            <ta e="T938" id="Seg_807" s="T937">gibər</ta>
            <ta e="T939" id="Seg_808" s="T938">lʼisa</ta>
            <ta e="T940" id="Seg_809" s="T939">mĭn-lə-j</ta>
            <ta e="T941" id="Seg_810" s="T940">dibər</ta>
            <ta e="T942" id="Seg_811" s="T941">i</ta>
            <ta e="T943" id="Seg_812" s="T942">varabej</ta>
            <ta e="T944" id="Seg_813" s="T943">mĭm-bi</ta>
            <ta e="T945" id="Seg_814" s="T944">dĭgəttə</ta>
            <ta e="T946" id="Seg_815" s="T945">davaj</ta>
            <ta e="T947" id="Seg_816" s="T946">kuʔ-sittə</ta>
            <ta e="T948" id="Seg_817" s="T947">aš</ta>
            <ta e="T949" id="Seg_818" s="T948">kuʔ-pi-ʔi</ta>
            <ta e="T950" id="Seg_819" s="T949">dĭ</ta>
            <ta e="T951" id="Seg_820" s="T950">özer-bi</ta>
            <ta e="T952" id="Seg_821" s="T951">măn-də</ta>
            <ta e="T953" id="Seg_822" s="T952">măn</ta>
            <ta e="T954" id="Seg_823" s="T953">kal-la-m</ta>
            <ta e="T955" id="Seg_824" s="T954">nʼuʔdə</ta>
            <ta e="T956" id="Seg_825" s="T955">ej</ta>
            <ta e="T957" id="Seg_826" s="T956">öʔ-li-m</ta>
            <ta e="T958" id="Seg_827" s="T957">döbər</ta>
            <ta e="T959" id="Seg_828" s="T958">a</ta>
            <ta e="T960" id="Seg_829" s="T959">tăn</ta>
            <ta e="T961" id="Seg_830" s="T960">püde-ʔ</ta>
            <ta e="T962" id="Seg_831" s="T961">dĭbər</ta>
            <ta e="T963" id="Seg_832" s="T962">varabej</ta>
            <ta e="T964" id="Seg_833" s="T963">uge</ta>
            <ta e="T965" id="Seg_834" s="T964">pĭd-leʔbə</ta>
            <ta e="T966" id="Seg_835" s="T965">püd-leʔbə</ta>
            <ta e="T967" id="Seg_836" s="T966">a</ta>
            <ta e="T968" id="Seg_837" s="T967">lʼisa</ta>
            <ta e="T969" id="Seg_838" s="T968">amno-laʔbə</ta>
            <ta e="T970" id="Seg_839" s="T969">dĭgəttə</ta>
            <ta e="T971" id="Seg_840" s="T970">šo-ʔ</ta>
            <ta e="T972" id="Seg_841" s="T971">toʔ-nar-zittə</ta>
            <ta e="T973" id="Seg_842" s="T972">ipek</ta>
            <ta e="T975" id="Seg_843" s="T974">dĭ</ta>
            <ta e="T976" id="Seg_844" s="T975">măn-də</ta>
            <ta e="T977" id="Seg_845" s="T976">măn</ta>
            <ta e="T978" id="Seg_846" s="T977">amno-la-m</ta>
            <ta e="T979" id="Seg_847" s="T978">măna</ta>
            <ta e="T980" id="Seg_848" s="T979">nʼelʼzʼa</ta>
            <ta e="T981" id="Seg_849" s="T980">kan-zittə</ta>
            <ta e="T982" id="Seg_850" s="T981">dĭgəttə</ta>
            <ta e="T983" id="Seg_851" s="T982">dĭ</ta>
            <ta e="T984" id="Seg_852" s="T983">püdə-bi</ta>
            <ta e="T985" id="Seg_853" s="T984">arəm-bi</ta>
            <ta e="T986" id="Seg_854" s="T985">dʼü</ta>
            <ta e="T987" id="Seg_855" s="T986">dĭgəttə</ta>
            <ta e="T988" id="Seg_856" s="T987">bar</ta>
            <ta e="T990" id="Seg_857" s="T989">toʔ-nar-bi</ta>
            <ta e="T991" id="Seg_858" s="T990">măn-də</ta>
            <ta e="T992" id="Seg_859" s="T991">šo-ʔ</ta>
            <ta e="T993" id="Seg_860" s="T992">ipek</ta>
            <ta e="T994" id="Seg_861" s="T993">i-ʔ</ta>
            <ta e="T995" id="Seg_862" s="T994">i</ta>
            <ta e="T996" id="Seg_863" s="T995">măn</ta>
            <ta e="T997" id="Seg_864" s="T996">i-li-m</ta>
            <ta e="T998" id="Seg_865" s="T997">dĭ</ta>
            <ta e="T999" id="Seg_866" s="T998">šide</ta>
            <ta e="T1000" id="Seg_867" s="T999">i-bi</ta>
            <ta e="T1001" id="Seg_868" s="T1000">mʼera</ta>
            <ta e="T1002" id="Seg_869" s="T1001">a</ta>
            <ta e="T1003" id="Seg_870" s="T1002">dĭʔ-nə</ta>
            <ta e="T1004" id="Seg_871" s="T1003">onʼiʔ</ta>
            <ta e="T1005" id="Seg_872" s="T1004">dĭgəttə</ta>
            <ta e="T1006" id="Seg_873" s="T1005">dĭ</ta>
            <ta e="T1007" id="Seg_874" s="T1006">kam-bi</ta>
            <ta e="T1008" id="Seg_875" s="T1007">men</ta>
            <ta e="T1011" id="Seg_876" s="T1010">men</ta>
            <ta e="T1012" id="Seg_877" s="T1011">măn-də</ta>
            <ta e="T1013" id="Seg_878" s="T1012">dĭ</ta>
            <ta e="T1014" id="Seg_879" s="T1013">ĭmbi</ta>
            <ta e="T1016" id="Seg_880" s="T1015">tăn</ta>
            <ta e="T1017" id="Seg_881" s="T1016">dĭrgit</ta>
            <ta e="T1019" id="Seg_882" s="T1018">dĭrgit</ta>
            <ta e="T1020" id="Seg_883" s="T1019">da</ta>
            <ta e="T1021" id="Seg_884" s="T1020">măna</ta>
            <ta e="T1022" id="Seg_885" s="T1021">lʼisʼitsa</ta>
            <ta e="T1023" id="Seg_886" s="T1022">mʼeg-luʔ-pi</ta>
            <ta e="T1024" id="Seg_887" s="T1023">kan-žə-bəj</ta>
            <ta e="T1025" id="Seg_888" s="T1024">măn</ta>
            <ta e="T1026" id="Seg_889" s="T1025">dĭ-m</ta>
            <ta e="T1027" id="Seg_890" s="T1026">kabar-li-m</ta>
            <ta e="T1028" id="Seg_891" s="T1027">dĭ</ta>
            <ta e="T1029" id="Seg_892" s="T1028">kam-bi</ta>
            <ta e="T1030" id="Seg_893" s="T1029">aš-də</ta>
            <ta e="T1031" id="Seg_894" s="T1030">šaʔ-laːm-bi</ta>
            <ta e="T1032" id="Seg_895" s="T1031">lʼisʼitsa</ta>
            <ta e="T1033" id="Seg_896" s="T1032">šo-bi</ta>
            <ta e="T1034" id="Seg_897" s="T1033">tʼermən-də</ta>
            <ta e="T1035" id="Seg_898" s="T1034">nada</ta>
            <ta e="T1037" id="Seg_899" s="T1036">kun-zittə</ta>
            <ta e="T1038" id="Seg_900" s="T1037">aš</ta>
            <ta e="T1039" id="Seg_901" s="T1038">dĭgəttə</ta>
            <ta e="T1041" id="Seg_902" s="T1040">ku-lio-t</ta>
            <ta e="T1042" id="Seg_903" s="T1041">što</ta>
            <ta e="T1043" id="Seg_904" s="T1042">ku-t</ta>
            <ta e="T1044" id="Seg_905" s="T1043">men-də</ta>
            <ta e="T1045" id="Seg_906" s="T1044">iʔbə-laʔbə</ta>
            <ta e="T1046" id="Seg_907" s="T1045">dĭ</ta>
            <ta e="T1047" id="Seg_908" s="T1046">kabar-bi</ta>
            <ta e="T1048" id="Seg_909" s="T1047">a</ta>
            <ta e="T1049" id="Seg_910" s="T1048">men</ta>
            <ta e="T1050" id="Seg_911" s="T1049">dĭ-m</ta>
            <ta e="T1051" id="Seg_912" s="T1050">talbə-luʔ-pi</ta>
            <ta e="T1052" id="Seg_913" s="T1051">da</ta>
            <ta e="T1053" id="Seg_914" s="T1052">kü-laːm-bi</ta>
            <ta e="T1054" id="Seg_915" s="T1053">i</ta>
            <ta e="T1055" id="Seg_916" s="T1054">aš-də</ta>
            <ta e="T1056" id="Seg_917" s="T1055">ma-luʔ-pi</ta>
            <ta e="T1057" id="Seg_918" s="T1056">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T933" id="Seg_919" s="T932">lʼisa</ta>
            <ta e="T934" id="Seg_920" s="T933">i</ta>
            <ta e="T935" id="Seg_921" s="T934">varabej</ta>
            <ta e="T936" id="Seg_922" s="T935">šide-göʔ</ta>
            <ta e="T937" id="Seg_923" s="T936">amno-bi-jəʔ</ta>
            <ta e="T938" id="Seg_924" s="T937">gibər</ta>
            <ta e="T939" id="Seg_925" s="T938">lʼisa</ta>
            <ta e="T940" id="Seg_926" s="T939">mĭn-lV-j</ta>
            <ta e="T941" id="Seg_927" s="T940">dĭbər</ta>
            <ta e="T942" id="Seg_928" s="T941">i</ta>
            <ta e="T943" id="Seg_929" s="T942">varabej</ta>
            <ta e="T944" id="Seg_930" s="T943">mĭn-bi</ta>
            <ta e="T945" id="Seg_931" s="T944">dĭgəttə</ta>
            <ta e="T946" id="Seg_932" s="T945">davaj</ta>
            <ta e="T947" id="Seg_933" s="T946">kuʔ-zittə</ta>
            <ta e="T948" id="Seg_934" s="T947">aš</ta>
            <ta e="T949" id="Seg_935" s="T948">kuʔ-bi-jəʔ</ta>
            <ta e="T950" id="Seg_936" s="T949">dĭ</ta>
            <ta e="T951" id="Seg_937" s="T950">özer-bi</ta>
            <ta e="T952" id="Seg_938" s="T951">măn-ntə</ta>
            <ta e="T953" id="Seg_939" s="T952">măn</ta>
            <ta e="T954" id="Seg_940" s="T953">kan-lV-m</ta>
            <ta e="T955" id="Seg_941" s="T954">nʼuʔdə</ta>
            <ta e="T956" id="Seg_942" s="T955">ej</ta>
            <ta e="T957" id="Seg_943" s="T956">öʔ-lV-m</ta>
            <ta e="T958" id="Seg_944" s="T957">döbər</ta>
            <ta e="T959" id="Seg_945" s="T958">a</ta>
            <ta e="T960" id="Seg_946" s="T959">tăn</ta>
            <ta e="T961" id="Seg_947" s="T960">püdə-ʔ</ta>
            <ta e="T962" id="Seg_948" s="T961">dĭbər</ta>
            <ta e="T963" id="Seg_949" s="T962">varabej</ta>
            <ta e="T964" id="Seg_950" s="T963">üge</ta>
            <ta e="T965" id="Seg_951" s="T964">püdə-laʔbə</ta>
            <ta e="T966" id="Seg_952" s="T965">püdə-laʔbə</ta>
            <ta e="T967" id="Seg_953" s="T966">a</ta>
            <ta e="T968" id="Seg_954" s="T967">lʼisa</ta>
            <ta e="T969" id="Seg_955" s="T968">amnə-laʔbə</ta>
            <ta e="T970" id="Seg_956" s="T969">dĭgəttə</ta>
            <ta e="T971" id="Seg_957" s="T970">šo-ʔ</ta>
            <ta e="T972" id="Seg_958" s="T971">toʔ-nar-zittə</ta>
            <ta e="T973" id="Seg_959" s="T972">ipek</ta>
            <ta e="T975" id="Seg_960" s="T974">dĭ</ta>
            <ta e="T976" id="Seg_961" s="T975">măn-ntə</ta>
            <ta e="T977" id="Seg_962" s="T976">măn</ta>
            <ta e="T978" id="Seg_963" s="T977">amnə-lV-m</ta>
            <ta e="T979" id="Seg_964" s="T978">măna</ta>
            <ta e="T980" id="Seg_965" s="T979">nʼelʼzʼa</ta>
            <ta e="T981" id="Seg_966" s="T980">kan-zittə</ta>
            <ta e="T982" id="Seg_967" s="T981">dĭgəttə</ta>
            <ta e="T983" id="Seg_968" s="T982">dĭ</ta>
            <ta e="T984" id="Seg_969" s="T983">püdə-bi</ta>
            <ta e="T985" id="Seg_970" s="T984">arəm-bi</ta>
            <ta e="T986" id="Seg_971" s="T985">tʼo</ta>
            <ta e="T987" id="Seg_972" s="T986">dĭgəttə</ta>
            <ta e="T988" id="Seg_973" s="T987">bar</ta>
            <ta e="T990" id="Seg_974" s="T989">toʔbdə-nar-bi</ta>
            <ta e="T991" id="Seg_975" s="T990">măn-ntə</ta>
            <ta e="T992" id="Seg_976" s="T991">šo-ʔ</ta>
            <ta e="T993" id="Seg_977" s="T992">ipek</ta>
            <ta e="T994" id="Seg_978" s="T993">i-ʔ</ta>
            <ta e="T995" id="Seg_979" s="T994">i</ta>
            <ta e="T996" id="Seg_980" s="T995">măn</ta>
            <ta e="T997" id="Seg_981" s="T996">i-lV-m</ta>
            <ta e="T998" id="Seg_982" s="T997">dĭ</ta>
            <ta e="T999" id="Seg_983" s="T998">šide</ta>
            <ta e="T1000" id="Seg_984" s="T999">i-bi</ta>
            <ta e="T1001" id="Seg_985" s="T1000">mʼera</ta>
            <ta e="T1002" id="Seg_986" s="T1001">a</ta>
            <ta e="T1003" id="Seg_987" s="T1002">dĭ-Tə</ta>
            <ta e="T1004" id="Seg_988" s="T1003">onʼiʔ</ta>
            <ta e="T1005" id="Seg_989" s="T1004">dĭgəttə</ta>
            <ta e="T1006" id="Seg_990" s="T1005">dĭ</ta>
            <ta e="T1007" id="Seg_991" s="T1006">kan-bi</ta>
            <ta e="T1008" id="Seg_992" s="T1007">men</ta>
            <ta e="T1011" id="Seg_993" s="T1010">men</ta>
            <ta e="T1012" id="Seg_994" s="T1011">măn-ntə</ta>
            <ta e="T1013" id="Seg_995" s="T1012">dĭ</ta>
            <ta e="T1014" id="Seg_996" s="T1013">ĭmbi</ta>
            <ta e="T1016" id="Seg_997" s="T1015">tăn</ta>
            <ta e="T1017" id="Seg_998" s="T1016">dĭrgit</ta>
            <ta e="T1019" id="Seg_999" s="T1018">dĭrgit</ta>
            <ta e="T1020" id="Seg_1000" s="T1019">da</ta>
            <ta e="T1021" id="Seg_1001" s="T1020">măna</ta>
            <ta e="T1022" id="Seg_1002" s="T1021">lʼisʼitsa</ta>
            <ta e="T1023" id="Seg_1003" s="T1022">mʼeg-luʔbdə-bi</ta>
            <ta e="T1024" id="Seg_1004" s="T1023">kan-žə-bəj</ta>
            <ta e="T1025" id="Seg_1005" s="T1024">măn</ta>
            <ta e="T1026" id="Seg_1006" s="T1025">dĭ-m</ta>
            <ta e="T1027" id="Seg_1007" s="T1026">kabar-lV-m</ta>
            <ta e="T1028" id="Seg_1008" s="T1027">dĭ</ta>
            <ta e="T1029" id="Seg_1009" s="T1028">kan-bi</ta>
            <ta e="T1030" id="Seg_1010" s="T1029">aš-Tə</ta>
            <ta e="T1031" id="Seg_1011" s="T1030">šaʔ-laːm-bi</ta>
            <ta e="T1032" id="Seg_1012" s="T1031">lʼisʼitsa</ta>
            <ta e="T1033" id="Seg_1013" s="T1032">šo-bi</ta>
            <ta e="T1034" id="Seg_1014" s="T1033">tʼermən-də</ta>
            <ta e="T1035" id="Seg_1015" s="T1034">nadə</ta>
            <ta e="T1037" id="Seg_1016" s="T1036">kun-zittə</ta>
            <ta e="T1038" id="Seg_1017" s="T1037">aš</ta>
            <ta e="T1039" id="Seg_1018" s="T1038">dĭgəttə</ta>
            <ta e="T1041" id="Seg_1019" s="T1040">ku-liA-t</ta>
            <ta e="T1042" id="Seg_1020" s="T1041">što</ta>
            <ta e="T1043" id="Seg_1021" s="T1042">ku-t</ta>
            <ta e="T1044" id="Seg_1022" s="T1043">men-də</ta>
            <ta e="T1045" id="Seg_1023" s="T1044">iʔbö-laʔbə</ta>
            <ta e="T1046" id="Seg_1024" s="T1045">dĭ</ta>
            <ta e="T1047" id="Seg_1025" s="T1046">kabar-bi</ta>
            <ta e="T1048" id="Seg_1026" s="T1047">a</ta>
            <ta e="T1049" id="Seg_1027" s="T1048">men</ta>
            <ta e="T1050" id="Seg_1028" s="T1049">dĭ-m</ta>
            <ta e="T1051" id="Seg_1029" s="T1050">talbə-luʔbdə-bi</ta>
            <ta e="T1052" id="Seg_1030" s="T1051">da</ta>
            <ta e="T1053" id="Seg_1031" s="T1052">kü-laːm-bi</ta>
            <ta e="T1054" id="Seg_1032" s="T1053">i</ta>
            <ta e="T1055" id="Seg_1033" s="T1054">aš-də</ta>
            <ta e="T1056" id="Seg_1034" s="T1055">ma-luʔbdə-bi</ta>
            <ta e="T1057" id="Seg_1035" s="T1056">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T933" id="Seg_1036" s="T932">fox.[NOM.SG]</ta>
            <ta e="T934" id="Seg_1037" s="T933">and</ta>
            <ta e="T935" id="Seg_1038" s="T934">sparrow.[NOM.SG]</ta>
            <ta e="T936" id="Seg_1039" s="T935">two-COLL</ta>
            <ta e="T937" id="Seg_1040" s="T936">live-PST-3PL</ta>
            <ta e="T938" id="Seg_1041" s="T937">where.to</ta>
            <ta e="T939" id="Seg_1042" s="T938">fox.[NOM.SG]</ta>
            <ta e="T940" id="Seg_1043" s="T939">go-FUT-3SG</ta>
            <ta e="T941" id="Seg_1044" s="T940">there</ta>
            <ta e="T942" id="Seg_1045" s="T941">and</ta>
            <ta e="T943" id="Seg_1046" s="T942">sparrow.[NOM.SG]</ta>
            <ta e="T944" id="Seg_1047" s="T943">go-PST.[3SG]</ta>
            <ta e="T945" id="Seg_1048" s="T944">then</ta>
            <ta e="T946" id="Seg_1049" s="T945">INCH</ta>
            <ta e="T947" id="Seg_1050" s="T946">sow-INF.LAT</ta>
            <ta e="T948" id="Seg_1051" s="T947">rye.[NOM.SG]</ta>
            <ta e="T949" id="Seg_1052" s="T948">sow-PST-3PL</ta>
            <ta e="T950" id="Seg_1053" s="T949">this.[NOM.SG]</ta>
            <ta e="T951" id="Seg_1054" s="T950">grow-PST.[3SG]</ta>
            <ta e="T952" id="Seg_1055" s="T951">say-IPFVZ.[3SG]</ta>
            <ta e="T953" id="Seg_1056" s="T952">I.NOM</ta>
            <ta e="T954" id="Seg_1057" s="T953">go-FUT-1SG</ta>
            <ta e="T955" id="Seg_1058" s="T954">up</ta>
            <ta e="T956" id="Seg_1059" s="T955">NEG</ta>
            <ta e="T957" id="Seg_1060" s="T956">let-FUT-1SG</ta>
            <ta e="T958" id="Seg_1061" s="T957">here</ta>
            <ta e="T959" id="Seg_1062" s="T958">and</ta>
            <ta e="T960" id="Seg_1063" s="T959">you.NOM</ta>
            <ta e="T961" id="Seg_1064" s="T960">mow-IMP.2SG</ta>
            <ta e="T962" id="Seg_1065" s="T961">there</ta>
            <ta e="T963" id="Seg_1066" s="T962">sparrow.[NOM.SG]</ta>
            <ta e="T964" id="Seg_1067" s="T963">always</ta>
            <ta e="T965" id="Seg_1068" s="T964">mow-DUR.[3SG]</ta>
            <ta e="T966" id="Seg_1069" s="T965">mow-DUR.[3SG]</ta>
            <ta e="T967" id="Seg_1070" s="T966">and</ta>
            <ta e="T968" id="Seg_1071" s="T967">fox.[NOM.SG]</ta>
            <ta e="T969" id="Seg_1072" s="T968">sit-DUR.[3SG]</ta>
            <ta e="T970" id="Seg_1073" s="T969">then</ta>
            <ta e="T971" id="Seg_1074" s="T970">come-IMP.2SG</ta>
            <ta e="T972" id="Seg_1075" s="T971">jolt-MULT-INF.LAT</ta>
            <ta e="T973" id="Seg_1076" s="T972">bread.[NOM.SG]</ta>
            <ta e="T975" id="Seg_1077" s="T974">this</ta>
            <ta e="T976" id="Seg_1078" s="T975">say-IPFVZ.[3SG]</ta>
            <ta e="T977" id="Seg_1079" s="T976">I.NOM</ta>
            <ta e="T978" id="Seg_1080" s="T977">sit-FUT-1SG</ta>
            <ta e="T979" id="Seg_1081" s="T978">I.LAT</ta>
            <ta e="T980" id="Seg_1082" s="T979">one.cannot</ta>
            <ta e="T981" id="Seg_1083" s="T980">go-INF.LAT</ta>
            <ta e="T982" id="Seg_1084" s="T981">then</ta>
            <ta e="T983" id="Seg_1085" s="T982">this.[NOM.SG]</ta>
            <ta e="T984" id="Seg_1086" s="T983">mow-PST.[3SG]</ta>
            <ta e="T985" id="Seg_1087" s="T984">clean-PST.[3SG]</ta>
            <ta e="T986" id="Seg_1088" s="T985">earth.[NOM.SG]</ta>
            <ta e="T987" id="Seg_1089" s="T986">then</ta>
            <ta e="T988" id="Seg_1090" s="T987">all</ta>
            <ta e="T990" id="Seg_1091" s="T989">hit-MULT-PST.[3SG]</ta>
            <ta e="T991" id="Seg_1092" s="T990">say-IPFVZ.[3SG]</ta>
            <ta e="T992" id="Seg_1093" s="T991">come-IMP.2SG</ta>
            <ta e="T993" id="Seg_1094" s="T992">bread.[NOM.SG]</ta>
            <ta e="T994" id="Seg_1095" s="T993">take-IMP.2SG</ta>
            <ta e="T995" id="Seg_1096" s="T994">and</ta>
            <ta e="T996" id="Seg_1097" s="T995">I.NOM</ta>
            <ta e="T997" id="Seg_1098" s="T996">take-FUT-1SG</ta>
            <ta e="T998" id="Seg_1099" s="T997">this.[NOM.SG]</ta>
            <ta e="T999" id="Seg_1100" s="T998">two.[NOM.SG]</ta>
            <ta e="T1000" id="Seg_1101" s="T999">take-PST.[3SG]</ta>
            <ta e="T1001" id="Seg_1102" s="T1000">measure.[NOM.SG]</ta>
            <ta e="T1002" id="Seg_1103" s="T1001">and</ta>
            <ta e="T1003" id="Seg_1104" s="T1002">this-LAT</ta>
            <ta e="T1004" id="Seg_1105" s="T1003">one.[NOM.SG]</ta>
            <ta e="T1005" id="Seg_1106" s="T1004">then</ta>
            <ta e="T1006" id="Seg_1107" s="T1005">this.[NOM.SG]</ta>
            <ta e="T1007" id="Seg_1108" s="T1006">go-PST.[3SG]</ta>
            <ta e="T1008" id="Seg_1109" s="T1007">dog.[NOM.SG]</ta>
            <ta e="T1011" id="Seg_1110" s="T1010">dog.[NOM.SG]</ta>
            <ta e="T1012" id="Seg_1111" s="T1011">say-IPFVZ.[3SG]</ta>
            <ta e="T1013" id="Seg_1112" s="T1012">this</ta>
            <ta e="T1014" id="Seg_1113" s="T1013">what.[NOM.SG]</ta>
            <ta e="T1016" id="Seg_1114" s="T1015">you.NOM</ta>
            <ta e="T1017" id="Seg_1115" s="T1016">such.[NOM.SG]</ta>
            <ta e="T1019" id="Seg_1116" s="T1018">such.[NOM.SG]</ta>
            <ta e="T1020" id="Seg_1117" s="T1019">and</ta>
            <ta e="T1021" id="Seg_1118" s="T1020">I.ACC</ta>
            <ta e="T1022" id="Seg_1119" s="T1021">fox.[NOM.SG]</ta>
            <ta e="T1023" id="Seg_1120" s="T1022">lure-MOM-PST.[3SG]</ta>
            <ta e="T1024" id="Seg_1121" s="T1023">go-OPT.DU/PL-1DU</ta>
            <ta e="T1025" id="Seg_1122" s="T1024">I.NOM</ta>
            <ta e="T1026" id="Seg_1123" s="T1025">this-ACC</ta>
            <ta e="T1027" id="Seg_1124" s="T1026">grab-FUT-1SG</ta>
            <ta e="T1028" id="Seg_1125" s="T1027">this.[NOM.SG]</ta>
            <ta e="T1029" id="Seg_1126" s="T1028">go-PST.[3SG]</ta>
            <ta e="T1030" id="Seg_1127" s="T1029">rye-LAT</ta>
            <ta e="T1031" id="Seg_1128" s="T1030">hide-RES-PST.[3SG]</ta>
            <ta e="T1032" id="Seg_1129" s="T1031">fox.[NOM.SG]</ta>
            <ta e="T1033" id="Seg_1130" s="T1032">come-PST.[3SG]</ta>
            <ta e="T1034" id="Seg_1131" s="T1033">mill-NOM/GEN/ACC.3SG</ta>
            <ta e="T1035" id="Seg_1132" s="T1034">one.should</ta>
            <ta e="T1037" id="Seg_1133" s="T1036">bring-INF.LAT</ta>
            <ta e="T1038" id="Seg_1134" s="T1037">rye.[NOM.SG]</ta>
            <ta e="T1039" id="Seg_1135" s="T1038">then</ta>
            <ta e="T1041" id="Seg_1136" s="T1040">see-PRS-3SG.O</ta>
            <ta e="T1042" id="Seg_1137" s="T1041">that</ta>
            <ta e="T1043" id="Seg_1138" s="T1042">ear-NOM/GEN.3SG</ta>
            <ta e="T1044" id="Seg_1139" s="T1043">dog-NOM/GEN/ACC.3SG</ta>
            <ta e="T1045" id="Seg_1140" s="T1044">lie-DUR.[3SG]</ta>
            <ta e="T1046" id="Seg_1141" s="T1045">this.[NOM.SG]</ta>
            <ta e="T1047" id="Seg_1142" s="T1046">grab-PST.[3SG]</ta>
            <ta e="T1048" id="Seg_1143" s="T1047">and</ta>
            <ta e="T1049" id="Seg_1144" s="T1048">dog.[NOM.SG]</ta>
            <ta e="T1050" id="Seg_1145" s="T1049">this-ACC</ta>
            <ta e="T1051" id="Seg_1146" s="T1050">bite-MOM-PST.[3SG]</ta>
            <ta e="T1052" id="Seg_1147" s="T1051">and</ta>
            <ta e="T1053" id="Seg_1148" s="T1052">die-RES-PST.[3SG]</ta>
            <ta e="T1054" id="Seg_1149" s="T1053">and</ta>
            <ta e="T1055" id="Seg_1150" s="T1054">rye-NOM/GEN/ACC.3SG</ta>
            <ta e="T1056" id="Seg_1151" s="T1055">leave-MOM-PST.[3SG]</ta>
            <ta e="T1057" id="Seg_1152" s="T1056">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T933" id="Seg_1153" s="T932">лиса.[NOM.SG]</ta>
            <ta e="T934" id="Seg_1154" s="T933">и</ta>
            <ta e="T935" id="Seg_1155" s="T934">воробей.[NOM.SG]</ta>
            <ta e="T936" id="Seg_1156" s="T935">два-COLL</ta>
            <ta e="T937" id="Seg_1157" s="T936">жить-PST-3PL</ta>
            <ta e="T938" id="Seg_1158" s="T937">куда</ta>
            <ta e="T939" id="Seg_1159" s="T938">лиса.[NOM.SG]</ta>
            <ta e="T940" id="Seg_1160" s="T939">идти-FUT-3SG</ta>
            <ta e="T941" id="Seg_1161" s="T940">там</ta>
            <ta e="T942" id="Seg_1162" s="T941">и</ta>
            <ta e="T943" id="Seg_1163" s="T942">воробей.[NOM.SG]</ta>
            <ta e="T944" id="Seg_1164" s="T943">идти-PST.[3SG]</ta>
            <ta e="T945" id="Seg_1165" s="T944">тогда</ta>
            <ta e="T946" id="Seg_1166" s="T945">INCH</ta>
            <ta e="T947" id="Seg_1167" s="T946">сеять-INF.LAT</ta>
            <ta e="T948" id="Seg_1168" s="T947">рожь.[NOM.SG]</ta>
            <ta e="T949" id="Seg_1169" s="T948">сеять-PST-3PL</ta>
            <ta e="T950" id="Seg_1170" s="T949">этот.[NOM.SG]</ta>
            <ta e="T951" id="Seg_1171" s="T950">расти-PST.[3SG]</ta>
            <ta e="T952" id="Seg_1172" s="T951">сказать-IPFVZ.[3SG]</ta>
            <ta e="T953" id="Seg_1173" s="T952">я.NOM</ta>
            <ta e="T954" id="Seg_1174" s="T953">пойти-FUT-1SG</ta>
            <ta e="T955" id="Seg_1175" s="T954">вверх</ta>
            <ta e="T956" id="Seg_1176" s="T955">NEG</ta>
            <ta e="T957" id="Seg_1177" s="T956">пускать-FUT-1SG</ta>
            <ta e="T958" id="Seg_1178" s="T957">здесь</ta>
            <ta e="T959" id="Seg_1179" s="T958">а</ta>
            <ta e="T960" id="Seg_1180" s="T959">ты.NOM</ta>
            <ta e="T961" id="Seg_1181" s="T960">жать-IMP.2SG</ta>
            <ta e="T962" id="Seg_1182" s="T961">там</ta>
            <ta e="T963" id="Seg_1183" s="T962">воробей.[NOM.SG]</ta>
            <ta e="T964" id="Seg_1184" s="T963">всегда</ta>
            <ta e="T965" id="Seg_1185" s="T964">жать-DUR.[3SG]</ta>
            <ta e="T966" id="Seg_1186" s="T965">жать-DUR.[3SG]</ta>
            <ta e="T967" id="Seg_1187" s="T966">а</ta>
            <ta e="T968" id="Seg_1188" s="T967">лиса.[NOM.SG]</ta>
            <ta e="T969" id="Seg_1189" s="T968">сидеть-DUR.[3SG]</ta>
            <ta e="T970" id="Seg_1190" s="T969">тогда</ta>
            <ta e="T971" id="Seg_1191" s="T970">прийти-IMP.2SG</ta>
            <ta e="T972" id="Seg_1192" s="T971">трясти-MULT-INF.LAT</ta>
            <ta e="T973" id="Seg_1193" s="T972">хлеб.[NOM.SG]</ta>
            <ta e="T975" id="Seg_1194" s="T974">этот</ta>
            <ta e="T976" id="Seg_1195" s="T975">сказать-IPFVZ.[3SG]</ta>
            <ta e="T977" id="Seg_1196" s="T976">я.NOM</ta>
            <ta e="T978" id="Seg_1197" s="T977">сидеть-FUT-1SG</ta>
            <ta e="T979" id="Seg_1198" s="T978">я.LAT</ta>
            <ta e="T980" id="Seg_1199" s="T979">нельзя</ta>
            <ta e="T981" id="Seg_1200" s="T980">пойти-INF.LAT</ta>
            <ta e="T982" id="Seg_1201" s="T981">тогда</ta>
            <ta e="T983" id="Seg_1202" s="T982">этот.[NOM.SG]</ta>
            <ta e="T984" id="Seg_1203" s="T983">жать-PST.[3SG]</ta>
            <ta e="T985" id="Seg_1204" s="T984">чистить-PST.[3SG]</ta>
            <ta e="T986" id="Seg_1205" s="T985">земля.[NOM.SG]</ta>
            <ta e="T987" id="Seg_1206" s="T986">тогда</ta>
            <ta e="T988" id="Seg_1207" s="T987">весь</ta>
            <ta e="T990" id="Seg_1208" s="T989">ударить-MULT-PST.[3SG]</ta>
            <ta e="T991" id="Seg_1209" s="T990">сказать-IPFVZ.[3SG]</ta>
            <ta e="T992" id="Seg_1210" s="T991">прийти-IMP.2SG</ta>
            <ta e="T993" id="Seg_1211" s="T992">хлеб.[NOM.SG]</ta>
            <ta e="T994" id="Seg_1212" s="T993">взять-IMP.2SG</ta>
            <ta e="T995" id="Seg_1213" s="T994">и</ta>
            <ta e="T996" id="Seg_1214" s="T995">я.NOM</ta>
            <ta e="T997" id="Seg_1215" s="T996">взять-FUT-1SG</ta>
            <ta e="T998" id="Seg_1216" s="T997">этот.[NOM.SG]</ta>
            <ta e="T999" id="Seg_1217" s="T998">два.[NOM.SG]</ta>
            <ta e="T1000" id="Seg_1218" s="T999">взять-PST.[3SG]</ta>
            <ta e="T1001" id="Seg_1219" s="T1000">мера.[NOM.SG]</ta>
            <ta e="T1002" id="Seg_1220" s="T1001">а</ta>
            <ta e="T1003" id="Seg_1221" s="T1002">этот-LAT</ta>
            <ta e="T1004" id="Seg_1222" s="T1003">один.[NOM.SG]</ta>
            <ta e="T1005" id="Seg_1223" s="T1004">тогда</ta>
            <ta e="T1006" id="Seg_1224" s="T1005">этот.[NOM.SG]</ta>
            <ta e="T1007" id="Seg_1225" s="T1006">пойти-PST.[3SG]</ta>
            <ta e="T1008" id="Seg_1226" s="T1007">собака.[NOM.SG]</ta>
            <ta e="T1011" id="Seg_1227" s="T1010">собака.[NOM.SG]</ta>
            <ta e="T1012" id="Seg_1228" s="T1011">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1013" id="Seg_1229" s="T1012">этот</ta>
            <ta e="T1014" id="Seg_1230" s="T1013">что.[NOM.SG]</ta>
            <ta e="T1016" id="Seg_1231" s="T1015">ты.NOM</ta>
            <ta e="T1017" id="Seg_1232" s="T1016">такой.[NOM.SG]</ta>
            <ta e="T1019" id="Seg_1233" s="T1018">такой.[NOM.SG]</ta>
            <ta e="T1020" id="Seg_1234" s="T1019">и</ta>
            <ta e="T1021" id="Seg_1235" s="T1020">я.ACC</ta>
            <ta e="T1022" id="Seg_1236" s="T1021">лисица.[NOM.SG]</ta>
            <ta e="T1023" id="Seg_1237" s="T1022">приманивать-MOM-PST.[3SG]</ta>
            <ta e="T1024" id="Seg_1238" s="T1023">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T1025" id="Seg_1239" s="T1024">я.NOM</ta>
            <ta e="T1026" id="Seg_1240" s="T1025">этот-ACC</ta>
            <ta e="T1027" id="Seg_1241" s="T1026">хватать-FUT-1SG</ta>
            <ta e="T1028" id="Seg_1242" s="T1027">этот.[NOM.SG]</ta>
            <ta e="T1029" id="Seg_1243" s="T1028">пойти-PST.[3SG]</ta>
            <ta e="T1030" id="Seg_1244" s="T1029">рожь-LAT</ta>
            <ta e="T1031" id="Seg_1245" s="T1030">спрятаться-RES-PST.[3SG]</ta>
            <ta e="T1032" id="Seg_1246" s="T1031">лисица.[NOM.SG]</ta>
            <ta e="T1033" id="Seg_1247" s="T1032">прийти-PST.[3SG]</ta>
            <ta e="T1034" id="Seg_1248" s="T1033">мельница-NOM/GEN/ACC.3SG</ta>
            <ta e="T1035" id="Seg_1249" s="T1034">надо</ta>
            <ta e="T1037" id="Seg_1250" s="T1036">нести-INF.LAT</ta>
            <ta e="T1038" id="Seg_1251" s="T1037">рожь.[NOM.SG]</ta>
            <ta e="T1039" id="Seg_1252" s="T1038">тогда</ta>
            <ta e="T1041" id="Seg_1253" s="T1040">видеть-PRS-3SG.O</ta>
            <ta e="T1042" id="Seg_1254" s="T1041">что</ta>
            <ta e="T1043" id="Seg_1255" s="T1042">ухо-NOM/GEN.3SG</ta>
            <ta e="T1044" id="Seg_1256" s="T1043">собака-NOM/GEN/ACC.3SG</ta>
            <ta e="T1045" id="Seg_1257" s="T1044">лежать-DUR.[3SG]</ta>
            <ta e="T1046" id="Seg_1258" s="T1045">этот.[NOM.SG]</ta>
            <ta e="T1047" id="Seg_1259" s="T1046">хватать-PST.[3SG]</ta>
            <ta e="T1048" id="Seg_1260" s="T1047">а</ta>
            <ta e="T1049" id="Seg_1261" s="T1048">собака.[NOM.SG]</ta>
            <ta e="T1050" id="Seg_1262" s="T1049">этот-ACC</ta>
            <ta e="T1051" id="Seg_1263" s="T1050">укусить-MOM-PST.[3SG]</ta>
            <ta e="T1052" id="Seg_1264" s="T1051">и</ta>
            <ta e="T1053" id="Seg_1265" s="T1052">умереть-RES-PST.[3SG]</ta>
            <ta e="T1054" id="Seg_1266" s="T1053">и</ta>
            <ta e="T1055" id="Seg_1267" s="T1054">рожь-NOM/GEN/ACC.3SG</ta>
            <ta e="T1056" id="Seg_1268" s="T1055">оставить-MOM-PST.[3SG]</ta>
            <ta e="T1057" id="Seg_1269" s="T1056">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T933" id="Seg_1270" s="T932">n-n:case</ta>
            <ta e="T934" id="Seg_1271" s="T933">conj</ta>
            <ta e="T935" id="Seg_1272" s="T934">n-n:case</ta>
            <ta e="T936" id="Seg_1273" s="T935">num-num&gt;num</ta>
            <ta e="T937" id="Seg_1274" s="T936">v-v:tense-v:pn</ta>
            <ta e="T938" id="Seg_1275" s="T937">que</ta>
            <ta e="T939" id="Seg_1276" s="T938">n-n:case</ta>
            <ta e="T940" id="Seg_1277" s="T939">v-v:tense-v:pn</ta>
            <ta e="T941" id="Seg_1278" s="T940">adv</ta>
            <ta e="T942" id="Seg_1279" s="T941">conj</ta>
            <ta e="T943" id="Seg_1280" s="T942">n-n:case</ta>
            <ta e="T944" id="Seg_1281" s="T943">v-v:tense-v:pn</ta>
            <ta e="T945" id="Seg_1282" s="T944">adv</ta>
            <ta e="T946" id="Seg_1283" s="T945">ptcl</ta>
            <ta e="T947" id="Seg_1284" s="T946">v-v:n.fin</ta>
            <ta e="T948" id="Seg_1285" s="T947">n-n:case</ta>
            <ta e="T949" id="Seg_1286" s="T948">v-v:tense-v:pn</ta>
            <ta e="T950" id="Seg_1287" s="T949">dempro-n:case</ta>
            <ta e="T951" id="Seg_1288" s="T950">v-v:tense-v:pn</ta>
            <ta e="T952" id="Seg_1289" s="T951">v-v&gt;v-v:pn</ta>
            <ta e="T953" id="Seg_1290" s="T952">pers</ta>
            <ta e="T954" id="Seg_1291" s="T953">v-v:tense-v:pn</ta>
            <ta e="T955" id="Seg_1292" s="T954">adv</ta>
            <ta e="T956" id="Seg_1293" s="T955">ptcl</ta>
            <ta e="T957" id="Seg_1294" s="T956">v-v:tense-v:pn</ta>
            <ta e="T958" id="Seg_1295" s="T957">adv</ta>
            <ta e="T959" id="Seg_1296" s="T958">conj</ta>
            <ta e="T960" id="Seg_1297" s="T959">pers</ta>
            <ta e="T961" id="Seg_1298" s="T960">v-v:mood.pn</ta>
            <ta e="T962" id="Seg_1299" s="T961">adv</ta>
            <ta e="T963" id="Seg_1300" s="T962">n-n:case</ta>
            <ta e="T964" id="Seg_1301" s="T963">adv</ta>
            <ta e="T965" id="Seg_1302" s="T964">v-v&gt;v-v:pn</ta>
            <ta e="T966" id="Seg_1303" s="T965">v-v&gt;v-v:pn</ta>
            <ta e="T967" id="Seg_1304" s="T966">conj</ta>
            <ta e="T968" id="Seg_1305" s="T967">n-n:case</ta>
            <ta e="T969" id="Seg_1306" s="T968">v-v&gt;v-v:pn</ta>
            <ta e="T970" id="Seg_1307" s="T969">adv</ta>
            <ta e="T971" id="Seg_1308" s="T970">v-v:mood.pn</ta>
            <ta e="T972" id="Seg_1309" s="T971">v-v&gt;v-v:n.fin</ta>
            <ta e="T973" id="Seg_1310" s="T972">n-n:case</ta>
            <ta e="T975" id="Seg_1311" s="T974">dempro</ta>
            <ta e="T976" id="Seg_1312" s="T975">v-v&gt;v-v:pn</ta>
            <ta e="T977" id="Seg_1313" s="T976">pers</ta>
            <ta e="T978" id="Seg_1314" s="T977">v-v:tense-v:pn</ta>
            <ta e="T979" id="Seg_1315" s="T978">pers</ta>
            <ta e="T980" id="Seg_1316" s="T979">ptcl</ta>
            <ta e="T981" id="Seg_1317" s="T980">v-v:n.fin</ta>
            <ta e="T982" id="Seg_1318" s="T981">adv</ta>
            <ta e="T983" id="Seg_1319" s="T982">dempro-n:case</ta>
            <ta e="T984" id="Seg_1320" s="T983">v-v:tense-v:pn</ta>
            <ta e="T985" id="Seg_1321" s="T984">v-v:tense-v:pn</ta>
            <ta e="T986" id="Seg_1322" s="T985">n-n:case</ta>
            <ta e="T987" id="Seg_1323" s="T986">adv</ta>
            <ta e="T988" id="Seg_1324" s="T987">quant</ta>
            <ta e="T990" id="Seg_1325" s="T989">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T991" id="Seg_1326" s="T990">v-v&gt;v-v:pn</ta>
            <ta e="T992" id="Seg_1327" s="T991">v-v:mood.pn</ta>
            <ta e="T993" id="Seg_1328" s="T992">n-n:case</ta>
            <ta e="T994" id="Seg_1329" s="T993">v-v:mood.pn</ta>
            <ta e="T995" id="Seg_1330" s="T994">conj</ta>
            <ta e="T996" id="Seg_1331" s="T995">pers</ta>
            <ta e="T997" id="Seg_1332" s="T996">v-v:tense-v:pn</ta>
            <ta e="T998" id="Seg_1333" s="T997">dempro-n:case</ta>
            <ta e="T999" id="Seg_1334" s="T998">num-n:case</ta>
            <ta e="T1000" id="Seg_1335" s="T999">v-v:tense-v:pn</ta>
            <ta e="T1001" id="Seg_1336" s="T1000">n-n:case</ta>
            <ta e="T1002" id="Seg_1337" s="T1001">conj</ta>
            <ta e="T1003" id="Seg_1338" s="T1002">dempro-n:case</ta>
            <ta e="T1004" id="Seg_1339" s="T1003">num-n:case</ta>
            <ta e="T1005" id="Seg_1340" s="T1004">adv</ta>
            <ta e="T1006" id="Seg_1341" s="T1005">dempro-n:case</ta>
            <ta e="T1007" id="Seg_1342" s="T1006">v-v:tense-v:pn</ta>
            <ta e="T1008" id="Seg_1343" s="T1007">n-n:case</ta>
            <ta e="T1011" id="Seg_1344" s="T1010">n-n:case</ta>
            <ta e="T1012" id="Seg_1345" s="T1011">v-v&gt;v-v:pn</ta>
            <ta e="T1013" id="Seg_1346" s="T1012">dempro</ta>
            <ta e="T1014" id="Seg_1347" s="T1013">que-n:case</ta>
            <ta e="T1016" id="Seg_1348" s="T1015">pers</ta>
            <ta e="T1017" id="Seg_1349" s="T1016">adj-n:case</ta>
            <ta e="T1019" id="Seg_1350" s="T1018">adj-n:case</ta>
            <ta e="T1020" id="Seg_1351" s="T1019">conj</ta>
            <ta e="T1021" id="Seg_1352" s="T1020">pers</ta>
            <ta e="T1022" id="Seg_1353" s="T1021">n-n:case</ta>
            <ta e="T1023" id="Seg_1354" s="T1022">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1024" id="Seg_1355" s="T1023">v-v:mood-v:pn</ta>
            <ta e="T1025" id="Seg_1356" s="T1024">pers</ta>
            <ta e="T1026" id="Seg_1357" s="T1025">dempro-n:case</ta>
            <ta e="T1027" id="Seg_1358" s="T1026">v-v:tense-v:pn</ta>
            <ta e="T1028" id="Seg_1359" s="T1027">dempro-n:case</ta>
            <ta e="T1029" id="Seg_1360" s="T1028">v-v:tense-v:pn</ta>
            <ta e="T1030" id="Seg_1361" s="T1029">n-n:case</ta>
            <ta e="T1031" id="Seg_1362" s="T1030">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1032" id="Seg_1363" s="T1031">n-n:case</ta>
            <ta e="T1033" id="Seg_1364" s="T1032">v-v:tense-v:pn</ta>
            <ta e="T1034" id="Seg_1365" s="T1033">n-n:case.poss</ta>
            <ta e="T1035" id="Seg_1366" s="T1034">ptcl</ta>
            <ta e="T1037" id="Seg_1367" s="T1036">v-v:n.fin</ta>
            <ta e="T1038" id="Seg_1368" s="T1037">n-n:case</ta>
            <ta e="T1039" id="Seg_1369" s="T1038">adv</ta>
            <ta e="T1041" id="Seg_1370" s="T1040">v-v:tense-v:pn</ta>
            <ta e="T1042" id="Seg_1371" s="T1041">conj</ta>
            <ta e="T1043" id="Seg_1372" s="T1042">n-n:case.poss</ta>
            <ta e="T1044" id="Seg_1373" s="T1043">n-n:case.poss</ta>
            <ta e="T1045" id="Seg_1374" s="T1044">v-v&gt;v-v:pn</ta>
            <ta e="T1046" id="Seg_1375" s="T1045">dempro-n:case</ta>
            <ta e="T1047" id="Seg_1376" s="T1046">v-v:tense-v:pn</ta>
            <ta e="T1048" id="Seg_1377" s="T1047">conj</ta>
            <ta e="T1049" id="Seg_1378" s="T1048">n-n:case</ta>
            <ta e="T1050" id="Seg_1379" s="T1049">dempro-n:case</ta>
            <ta e="T1051" id="Seg_1380" s="T1050">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1052" id="Seg_1381" s="T1051">conj</ta>
            <ta e="T1053" id="Seg_1382" s="T1052">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1054" id="Seg_1383" s="T1053">conj</ta>
            <ta e="T1055" id="Seg_1384" s="T1054">n-n:case.poss</ta>
            <ta e="T1056" id="Seg_1385" s="T1055">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1057" id="Seg_1386" s="T1056">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T933" id="Seg_1387" s="T932">n</ta>
            <ta e="T934" id="Seg_1388" s="T933">conj</ta>
            <ta e="T935" id="Seg_1389" s="T934">n</ta>
            <ta e="T936" id="Seg_1390" s="T935">num</ta>
            <ta e="T937" id="Seg_1391" s="T936">v</ta>
            <ta e="T938" id="Seg_1392" s="T937">que</ta>
            <ta e="T939" id="Seg_1393" s="T938">n</ta>
            <ta e="T940" id="Seg_1394" s="T939">v</ta>
            <ta e="T941" id="Seg_1395" s="T940">adv</ta>
            <ta e="T942" id="Seg_1396" s="T941">conj</ta>
            <ta e="T943" id="Seg_1397" s="T942">n</ta>
            <ta e="T944" id="Seg_1398" s="T943">v</ta>
            <ta e="T945" id="Seg_1399" s="T944">adv</ta>
            <ta e="T946" id="Seg_1400" s="T945">ptcl</ta>
            <ta e="T947" id="Seg_1401" s="T946">v</ta>
            <ta e="T948" id="Seg_1402" s="T947">n</ta>
            <ta e="T949" id="Seg_1403" s="T948">v</ta>
            <ta e="T950" id="Seg_1404" s="T949">dempro</ta>
            <ta e="T951" id="Seg_1405" s="T950">v</ta>
            <ta e="T952" id="Seg_1406" s="T951">v</ta>
            <ta e="T953" id="Seg_1407" s="T952">pers</ta>
            <ta e="T954" id="Seg_1408" s="T953">v</ta>
            <ta e="T955" id="Seg_1409" s="T954">adv</ta>
            <ta e="T956" id="Seg_1410" s="T955">ptcl</ta>
            <ta e="T957" id="Seg_1411" s="T956">v</ta>
            <ta e="T958" id="Seg_1412" s="T957">adv</ta>
            <ta e="T959" id="Seg_1413" s="T958">conj</ta>
            <ta e="T960" id="Seg_1414" s="T959">pers</ta>
            <ta e="T961" id="Seg_1415" s="T960">v</ta>
            <ta e="T962" id="Seg_1416" s="T961">adv</ta>
            <ta e="T963" id="Seg_1417" s="T962">n</ta>
            <ta e="T964" id="Seg_1418" s="T963">adv</ta>
            <ta e="T965" id="Seg_1419" s="T964">v</ta>
            <ta e="T966" id="Seg_1420" s="T965">v</ta>
            <ta e="T967" id="Seg_1421" s="T966">conj</ta>
            <ta e="T968" id="Seg_1422" s="T967">n</ta>
            <ta e="T969" id="Seg_1423" s="T968">v</ta>
            <ta e="T970" id="Seg_1424" s="T969">adv</ta>
            <ta e="T971" id="Seg_1425" s="T970">v</ta>
            <ta e="T972" id="Seg_1426" s="T971">v</ta>
            <ta e="T973" id="Seg_1427" s="T972">n</ta>
            <ta e="T975" id="Seg_1428" s="T974">dempro</ta>
            <ta e="T976" id="Seg_1429" s="T975">v</ta>
            <ta e="T977" id="Seg_1430" s="T976">pers</ta>
            <ta e="T978" id="Seg_1431" s="T977">v</ta>
            <ta e="T979" id="Seg_1432" s="T978">pers</ta>
            <ta e="T980" id="Seg_1433" s="T979">ptcl</ta>
            <ta e="T981" id="Seg_1434" s="T980">v</ta>
            <ta e="T982" id="Seg_1435" s="T981">adv</ta>
            <ta e="T983" id="Seg_1436" s="T982">dempro</ta>
            <ta e="T984" id="Seg_1437" s="T983">v</ta>
            <ta e="T985" id="Seg_1438" s="T984">v</ta>
            <ta e="T986" id="Seg_1439" s="T985">n</ta>
            <ta e="T987" id="Seg_1440" s="T986">adv</ta>
            <ta e="T988" id="Seg_1441" s="T987">quant</ta>
            <ta e="T990" id="Seg_1442" s="T989">v</ta>
            <ta e="T991" id="Seg_1443" s="T990">v</ta>
            <ta e="T992" id="Seg_1444" s="T991">v</ta>
            <ta e="T993" id="Seg_1445" s="T992">n</ta>
            <ta e="T994" id="Seg_1446" s="T993">v</ta>
            <ta e="T995" id="Seg_1447" s="T994">conj</ta>
            <ta e="T996" id="Seg_1448" s="T995">pers</ta>
            <ta e="T997" id="Seg_1449" s="T996">v</ta>
            <ta e="T998" id="Seg_1450" s="T997">dempro</ta>
            <ta e="T999" id="Seg_1451" s="T998">num</ta>
            <ta e="T1000" id="Seg_1452" s="T999">v</ta>
            <ta e="T1001" id="Seg_1453" s="T1000">n</ta>
            <ta e="T1002" id="Seg_1454" s="T1001">conj</ta>
            <ta e="T1003" id="Seg_1455" s="T1002">dempro</ta>
            <ta e="T1004" id="Seg_1456" s="T1003">num</ta>
            <ta e="T1005" id="Seg_1457" s="T1004">adv</ta>
            <ta e="T1006" id="Seg_1458" s="T1005">dempro</ta>
            <ta e="T1007" id="Seg_1459" s="T1006">v</ta>
            <ta e="T1008" id="Seg_1460" s="T1007">n</ta>
            <ta e="T1011" id="Seg_1461" s="T1010">n</ta>
            <ta e="T1012" id="Seg_1462" s="T1011">v</ta>
            <ta e="T1013" id="Seg_1463" s="T1012">dempro</ta>
            <ta e="T1014" id="Seg_1464" s="T1013">que</ta>
            <ta e="T1016" id="Seg_1465" s="T1015">pers</ta>
            <ta e="T1017" id="Seg_1466" s="T1016">adj</ta>
            <ta e="T1019" id="Seg_1467" s="T1018">adj</ta>
            <ta e="T1020" id="Seg_1468" s="T1019">conj</ta>
            <ta e="T1021" id="Seg_1469" s="T1020">pers</ta>
            <ta e="T1022" id="Seg_1470" s="T1021">n</ta>
            <ta e="T1023" id="Seg_1471" s="T1022">v</ta>
            <ta e="T1024" id="Seg_1472" s="T1023">v</ta>
            <ta e="T1025" id="Seg_1473" s="T1024">pers</ta>
            <ta e="T1026" id="Seg_1474" s="T1025">dempro</ta>
            <ta e="T1027" id="Seg_1475" s="T1026">v</ta>
            <ta e="T1028" id="Seg_1476" s="T1027">dempro</ta>
            <ta e="T1029" id="Seg_1477" s="T1028">v</ta>
            <ta e="T1030" id="Seg_1478" s="T1029">n</ta>
            <ta e="T1031" id="Seg_1479" s="T1030">v</ta>
            <ta e="T1032" id="Seg_1480" s="T1031">n</ta>
            <ta e="T1033" id="Seg_1481" s="T1032">v</ta>
            <ta e="T1034" id="Seg_1482" s="T1033">n</ta>
            <ta e="T1035" id="Seg_1483" s="T1034">ptcl</ta>
            <ta e="T1037" id="Seg_1484" s="T1036">v</ta>
            <ta e="T1038" id="Seg_1485" s="T1037">n</ta>
            <ta e="T1039" id="Seg_1486" s="T1038">adv</ta>
            <ta e="T1041" id="Seg_1487" s="T1040">v</ta>
            <ta e="T1042" id="Seg_1488" s="T1041">conj</ta>
            <ta e="T1043" id="Seg_1489" s="T1042">n</ta>
            <ta e="T1044" id="Seg_1490" s="T1043">n</ta>
            <ta e="T1045" id="Seg_1491" s="T1044">v</ta>
            <ta e="T1046" id="Seg_1492" s="T1045">dempro</ta>
            <ta e="T1047" id="Seg_1493" s="T1046">v</ta>
            <ta e="T1048" id="Seg_1494" s="T1047">conj</ta>
            <ta e="T1049" id="Seg_1495" s="T1048">n</ta>
            <ta e="T1050" id="Seg_1496" s="T1049">dempro</ta>
            <ta e="T1051" id="Seg_1497" s="T1050">v</ta>
            <ta e="T1052" id="Seg_1498" s="T1051">conj</ta>
            <ta e="T1053" id="Seg_1499" s="T1052">v</ta>
            <ta e="T1054" id="Seg_1500" s="T1053">conj</ta>
            <ta e="T1055" id="Seg_1501" s="T1054">n</ta>
            <ta e="T1056" id="Seg_1502" s="T1055">v</ta>
            <ta e="T1057" id="Seg_1503" s="T1056">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T933" id="Seg_1504" s="T932">np.h:E</ta>
            <ta e="T935" id="Seg_1505" s="T934">np.h:E</ta>
            <ta e="T939" id="Seg_1506" s="T938">np.h:A</ta>
            <ta e="T941" id="Seg_1507" s="T940">adv:L</ta>
            <ta e="T943" id="Seg_1508" s="T942">np.h:A</ta>
            <ta e="T945" id="Seg_1509" s="T944">adv:Time</ta>
            <ta e="T948" id="Seg_1510" s="T947">np:Th</ta>
            <ta e="T949" id="Seg_1511" s="T948">0.3.h:A</ta>
            <ta e="T950" id="Seg_1512" s="T949">pro:P</ta>
            <ta e="T952" id="Seg_1513" s="T951">0.3.h:A</ta>
            <ta e="T953" id="Seg_1514" s="T952">pro.h:A</ta>
            <ta e="T955" id="Seg_1515" s="T954">adv:Pth</ta>
            <ta e="T957" id="Seg_1516" s="T956">0.1.h:A</ta>
            <ta e="T958" id="Seg_1517" s="T957">adv:L</ta>
            <ta e="T960" id="Seg_1518" s="T959">pro.h:A</ta>
            <ta e="T962" id="Seg_1519" s="T961">adv:L</ta>
            <ta e="T963" id="Seg_1520" s="T962">np.h:A</ta>
            <ta e="T966" id="Seg_1521" s="T965">0.3.h:A</ta>
            <ta e="T968" id="Seg_1522" s="T967">np.h:E</ta>
            <ta e="T970" id="Seg_1523" s="T969">adv:Time</ta>
            <ta e="T971" id="Seg_1524" s="T970">0.2.h:A</ta>
            <ta e="T973" id="Seg_1525" s="T972">np:P</ta>
            <ta e="T975" id="Seg_1526" s="T974">pro.h:A</ta>
            <ta e="T977" id="Seg_1527" s="T976">pro.h:E</ta>
            <ta e="T979" id="Seg_1528" s="T978">pro.h:A</ta>
            <ta e="T982" id="Seg_1529" s="T981">adv:Time</ta>
            <ta e="T983" id="Seg_1530" s="T982">pro.h:A</ta>
            <ta e="T985" id="Seg_1531" s="T984">0.3.h:A</ta>
            <ta e="T986" id="Seg_1532" s="T985">np:P</ta>
            <ta e="T987" id="Seg_1533" s="T986">adv:Time</ta>
            <ta e="T988" id="Seg_1534" s="T987">np:P</ta>
            <ta e="T990" id="Seg_1535" s="T989">0.3.h:A</ta>
            <ta e="T991" id="Seg_1536" s="T990">0.3.h:A</ta>
            <ta e="T992" id="Seg_1537" s="T991">0.2.h:A</ta>
            <ta e="T993" id="Seg_1538" s="T992">np:Th</ta>
            <ta e="T994" id="Seg_1539" s="T993">0.2.h:A</ta>
            <ta e="T996" id="Seg_1540" s="T995">pro.h:A</ta>
            <ta e="T998" id="Seg_1541" s="T997">pro.h:A</ta>
            <ta e="T1001" id="Seg_1542" s="T1000">np:Th</ta>
            <ta e="T1003" id="Seg_1543" s="T1002">pro.h:R</ta>
            <ta e="T1004" id="Seg_1544" s="T1003">np:Th</ta>
            <ta e="T1005" id="Seg_1545" s="T1004">adv:Time</ta>
            <ta e="T1006" id="Seg_1546" s="T1005">pro.h:A</ta>
            <ta e="T1011" id="Seg_1547" s="T1010">np.h:A</ta>
            <ta e="T1016" id="Seg_1548" s="T1015">pro.h:Th</ta>
            <ta e="T1021" id="Seg_1549" s="T1020">pro.h:Th</ta>
            <ta e="T1022" id="Seg_1550" s="T1021">np.h:A</ta>
            <ta e="T1024" id="Seg_1551" s="T1023">0.1.h:A</ta>
            <ta e="T1025" id="Seg_1552" s="T1024">pro.h:A</ta>
            <ta e="T1026" id="Seg_1553" s="T1025">pro.h:Th</ta>
            <ta e="T1028" id="Seg_1554" s="T1027">pro.h:A</ta>
            <ta e="T1030" id="Seg_1555" s="T1029">np:L</ta>
            <ta e="T1031" id="Seg_1556" s="T1030">0.3.h:A</ta>
            <ta e="T1032" id="Seg_1557" s="T1031">np.h:A</ta>
            <ta e="T1034" id="Seg_1558" s="T1033">np:G</ta>
            <ta e="T1038" id="Seg_1559" s="T1037">np:Th</ta>
            <ta e="T1039" id="Seg_1560" s="T1038">adv:Time</ta>
            <ta e="T1041" id="Seg_1561" s="T1040">0.3.h:E</ta>
            <ta e="T1043" id="Seg_1562" s="T1042">np:Th</ta>
            <ta e="T1044" id="Seg_1563" s="T1043">np.h:Poss</ta>
            <ta e="T1046" id="Seg_1564" s="T1045">pro.h:A</ta>
            <ta e="T1049" id="Seg_1565" s="T1048">np.h:A</ta>
            <ta e="T1050" id="Seg_1566" s="T1049">pro.h:E</ta>
            <ta e="T1053" id="Seg_1567" s="T1052">0.3.h:P</ta>
            <ta e="T1055" id="Seg_1568" s="T1054">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T933" id="Seg_1569" s="T932">np.h:S</ta>
            <ta e="T935" id="Seg_1570" s="T934">np.h:S</ta>
            <ta e="T937" id="Seg_1571" s="T936">v:pred</ta>
            <ta e="T939" id="Seg_1572" s="T938">np.h:S</ta>
            <ta e="T940" id="Seg_1573" s="T939">v:pred</ta>
            <ta e="T943" id="Seg_1574" s="T942">np.h:S</ta>
            <ta e="T944" id="Seg_1575" s="T943">v:pred</ta>
            <ta e="T946" id="Seg_1576" s="T945">ptcl:pred</ta>
            <ta e="T948" id="Seg_1577" s="T947">np:O</ta>
            <ta e="T949" id="Seg_1578" s="T948">v:pred 0.3.h:S</ta>
            <ta e="T950" id="Seg_1579" s="T949">pro:S</ta>
            <ta e="T951" id="Seg_1580" s="T950">v:pred</ta>
            <ta e="T952" id="Seg_1581" s="T951">v:pred 0.3.h:S</ta>
            <ta e="T953" id="Seg_1582" s="T952">pro.h:S</ta>
            <ta e="T954" id="Seg_1583" s="T953">v:pred</ta>
            <ta e="T956" id="Seg_1584" s="T955">ptcl.neg</ta>
            <ta e="T957" id="Seg_1585" s="T956">v:pred 0.1.h:S</ta>
            <ta e="T960" id="Seg_1586" s="T959">pro.h:S</ta>
            <ta e="T961" id="Seg_1587" s="T960">v:pred</ta>
            <ta e="T963" id="Seg_1588" s="T962">np.h:S</ta>
            <ta e="T965" id="Seg_1589" s="T964">v:pred</ta>
            <ta e="T966" id="Seg_1590" s="T965">v:pred 0.3.h:S</ta>
            <ta e="T968" id="Seg_1591" s="T967">np.h:S</ta>
            <ta e="T969" id="Seg_1592" s="T968">v:pred</ta>
            <ta e="T971" id="Seg_1593" s="T970">v:pred 0.2.h:S</ta>
            <ta e="T972" id="Seg_1594" s="T971">v:pred</ta>
            <ta e="T973" id="Seg_1595" s="T972">np:O</ta>
            <ta e="T975" id="Seg_1596" s="T974">pro.h:S</ta>
            <ta e="T976" id="Seg_1597" s="T975">v:pred</ta>
            <ta e="T977" id="Seg_1598" s="T976">pro.h:S</ta>
            <ta e="T978" id="Seg_1599" s="T977">v:pred</ta>
            <ta e="T980" id="Seg_1600" s="T979">ptcl:pred</ta>
            <ta e="T983" id="Seg_1601" s="T982">pro.h:S</ta>
            <ta e="T984" id="Seg_1602" s="T983">v:pred</ta>
            <ta e="T985" id="Seg_1603" s="T984">v:pred 0.3.h:S</ta>
            <ta e="T986" id="Seg_1604" s="T985">np:O</ta>
            <ta e="T988" id="Seg_1605" s="T987">np:O</ta>
            <ta e="T990" id="Seg_1606" s="T989">v:pred 0.3.h:S</ta>
            <ta e="T991" id="Seg_1607" s="T990">v:pred 0.3.h:S</ta>
            <ta e="T992" id="Seg_1608" s="T991">v:pred 0.2.h:S</ta>
            <ta e="T993" id="Seg_1609" s="T992">np:O</ta>
            <ta e="T994" id="Seg_1610" s="T993">v:pred 0.2.h:S</ta>
            <ta e="T996" id="Seg_1611" s="T995">pro.h:S</ta>
            <ta e="T997" id="Seg_1612" s="T996">v:pred</ta>
            <ta e="T998" id="Seg_1613" s="T997">pro.h:S</ta>
            <ta e="T1000" id="Seg_1614" s="T999">v:pred</ta>
            <ta e="T1001" id="Seg_1615" s="T1000">np:O</ta>
            <ta e="T1004" id="Seg_1616" s="T1003">np:O</ta>
            <ta e="T1006" id="Seg_1617" s="T1005">pro.h:S</ta>
            <ta e="T1007" id="Seg_1618" s="T1006">v:pred</ta>
            <ta e="T1011" id="Seg_1619" s="T1010">np.h:S</ta>
            <ta e="T1012" id="Seg_1620" s="T1011">v:pred</ta>
            <ta e="T1016" id="Seg_1621" s="T1015">pro.h:S</ta>
            <ta e="T1019" id="Seg_1622" s="T1018">adj:pred</ta>
            <ta e="T1021" id="Seg_1623" s="T1020">pro.h:O</ta>
            <ta e="T1022" id="Seg_1624" s="T1021">np.h:S</ta>
            <ta e="T1023" id="Seg_1625" s="T1022">v:pred</ta>
            <ta e="T1024" id="Seg_1626" s="T1023">v:pred 0.1.h:S</ta>
            <ta e="T1025" id="Seg_1627" s="T1024">pro.h:S</ta>
            <ta e="T1026" id="Seg_1628" s="T1025">pro.h:O</ta>
            <ta e="T1027" id="Seg_1629" s="T1026">v:pred</ta>
            <ta e="T1028" id="Seg_1630" s="T1027">pro.h:S</ta>
            <ta e="T1029" id="Seg_1631" s="T1028">v:pred</ta>
            <ta e="T1031" id="Seg_1632" s="T1030">v:pred 0.3.h:S</ta>
            <ta e="T1032" id="Seg_1633" s="T1031">np.h:S</ta>
            <ta e="T1033" id="Seg_1634" s="T1032">v:pred</ta>
            <ta e="T1035" id="Seg_1635" s="T1034">ptcl:pred</ta>
            <ta e="T1038" id="Seg_1636" s="T1037">np:O</ta>
            <ta e="T1041" id="Seg_1637" s="T1040">v:pred 0.3.h:S</ta>
            <ta e="T1043" id="Seg_1638" s="T1042">np:S</ta>
            <ta e="T1045" id="Seg_1639" s="T1044">v:pred</ta>
            <ta e="T1046" id="Seg_1640" s="T1045">pro.h:S</ta>
            <ta e="T1047" id="Seg_1641" s="T1046">v:pred</ta>
            <ta e="T1049" id="Seg_1642" s="T1048">np.h:S</ta>
            <ta e="T1050" id="Seg_1643" s="T1049">pro.h:O</ta>
            <ta e="T1051" id="Seg_1644" s="T1050">v:pred</ta>
            <ta e="T1053" id="Seg_1645" s="T1052">v:pred 0.3.h:S</ta>
            <ta e="T1055" id="Seg_1646" s="T1054">np:S</ta>
            <ta e="T1056" id="Seg_1647" s="T1055">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T933" id="Seg_1648" s="T932">RUS:cult</ta>
            <ta e="T934" id="Seg_1649" s="T933">RUS:gram</ta>
            <ta e="T935" id="Seg_1650" s="T934">RUS:core</ta>
            <ta e="T939" id="Seg_1651" s="T938">RUS:cult</ta>
            <ta e="T942" id="Seg_1652" s="T941">RUS:gram</ta>
            <ta e="T943" id="Seg_1653" s="T942">RUS:core</ta>
            <ta e="T946" id="Seg_1654" s="T945">RUS:gram</ta>
            <ta e="T948" id="Seg_1655" s="T947">TURK:cult</ta>
            <ta e="T959" id="Seg_1656" s="T958">RUS:gram</ta>
            <ta e="T963" id="Seg_1657" s="T962">RUS:core</ta>
            <ta e="T967" id="Seg_1658" s="T966">RUS:gram</ta>
            <ta e="T968" id="Seg_1659" s="T967">RUS:cult</ta>
            <ta e="T980" id="Seg_1660" s="T979">RUS:mod</ta>
            <ta e="T988" id="Seg_1661" s="T987">TURK:disc</ta>
            <ta e="T995" id="Seg_1662" s="T994">RUS:gram</ta>
            <ta e="T1001" id="Seg_1663" s="T1000">RUS:cult</ta>
            <ta e="T1002" id="Seg_1664" s="T1001">RUS:gram</ta>
            <ta e="T1020" id="Seg_1665" s="T1019">RUS:gram</ta>
            <ta e="T1022" id="Seg_1666" s="T1021">RUS:cult</ta>
            <ta e="T1030" id="Seg_1667" s="T1029">TURK:cult</ta>
            <ta e="T1032" id="Seg_1668" s="T1031">RUS:cult</ta>
            <ta e="T1034" id="Seg_1669" s="T1033">TURK:cult</ta>
            <ta e="T1035" id="Seg_1670" s="T1034">RUS:mod</ta>
            <ta e="T1038" id="Seg_1671" s="T1037">TURK:cult</ta>
            <ta e="T1042" id="Seg_1672" s="T1041">RUS:gram</ta>
            <ta e="T1048" id="Seg_1673" s="T1047">RUS:gram</ta>
            <ta e="T1052" id="Seg_1674" s="T1051">RUS:gram</ta>
            <ta e="T1054" id="Seg_1675" s="T1053">RUS:gram</ta>
            <ta e="T1055" id="Seg_1676" s="T1054">TURK:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T937" id="Seg_1677" s="T932">Жили вдвоём лиса и воробей.</ta>
            <ta e="T944" id="Seg_1678" s="T937">Куда лиса пойдёт, туда и воробей пойдёт.</ta>
            <ta e="T948" id="Seg_1679" s="T944">Потом стали они рожь сеять.</ta>
            <ta e="T951" id="Seg_1680" s="T948">Посеяли, она выросла.</ta>
            <ta e="T958" id="Seg_1681" s="T951">[Лиса] говорит: «Я пойду вверх [на гору], [небо] не пускать вниз [упасть].</ta>
            <ta e="T961" id="Seg_1682" s="T958">А ты жни!»</ta>
            <ta e="T966" id="Seg_1683" s="T961">Воробей там всё жнёт, жнёт.</ta>
            <ta e="T969" id="Seg_1684" s="T966">А лиса сидит.</ta>
            <ta e="T971" id="Seg_1685" s="T969">Потом [воробей говорит]: «Иди сюда!</ta>
            <ta e="T973" id="Seg_1686" s="T971">Зерно молотить».</ta>
            <ta e="T981" id="Seg_1687" s="T973">Она говорит: «Я сижу, мне нельзя уходить».</ta>
            <ta e="T984" id="Seg_1688" s="T981">Тогда он скосил.</ta>
            <ta e="T986" id="Seg_1689" s="T984">Очистил землю.</ta>
            <ta e="T990" id="Seg_1690" s="T986">Потом всё обмолотил.</ta>
            <ta e="T992" id="Seg_1691" s="T990">И говорит: «Иди сюда!</ta>
            <ta e="T994" id="Seg_1692" s="T992">Бери зерно.</ta>
            <ta e="T997" id="Seg_1693" s="T994">И я возьму».</ta>
            <ta e="T1004" id="Seg_1694" s="T997">[Лиса] две меры взяла, а ему одну [оставила].</ta>
            <ta e="T1007" id="Seg_1695" s="T1004">Потом он улетел.</ta>
            <ta e="T1012" id="Seg_1696" s="T1007">Собака [его встретила и] говорит:</ta>
            <ta e="T1019" id="Seg_1697" s="T1012">«Ты что такой [грустный]?»</ta>
            <ta e="T1023" id="Seg_1698" s="T1019">«Лиса меня обманула».</ta>
            <ta e="T1031" id="Seg_1699" s="T1023">«Пойдём, я её поймаю», она пошла, во ржи спряталась.</ta>
            <ta e="T1038" id="Seg_1700" s="T1031">Лиса пришла, на мельницу [ей] надо рожь отнести.</ta>
            <ta e="T1045" id="Seg_1701" s="T1038">Потом видит, ухо собачье лежит.</ta>
            <ta e="T1053" id="Seg_1702" s="T1045">Она его схватила, а собака её укусила, и она умерла.</ta>
            <ta e="T1056" id="Seg_1703" s="T1053">И рожь осталась.</ta>
            <ta e="T1057" id="Seg_1704" s="T1056">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T937" id="Seg_1705" s="T932">A fox and a sparrow lived together.</ta>
            <ta e="T944" id="Seg_1706" s="T937">Where the fox will go, there the sparrow also went.</ta>
            <ta e="T948" id="Seg_1707" s="T944">Then [they] started to sow rye.</ta>
            <ta e="T951" id="Seg_1708" s="T948">They sowed, it grew.</ta>
            <ta e="T958" id="Seg_1709" s="T951">It says: “I will go up [to the hill], I will not let [the sky fall] here.</ta>
            <ta e="T961" id="Seg_1710" s="T958">And you mow!”</ta>
            <ta e="T966" id="Seg_1711" s="T961">There the sparrow always is mowing and mowing.</ta>
            <ta e="T969" id="Seg_1712" s="T966">But the fox is sitting.</ta>
            <ta e="T971" id="Seg_1713" s="T969">Then [the sparrow says]: "Come!</ta>
            <ta e="T973" id="Seg_1714" s="T971">To thresh the grain."</ta>
            <ta e="T981" id="Seg_1715" s="T973">Then [the fox] says: "I am sitting, I cannot go [away]."</ta>
            <ta e="T984" id="Seg_1716" s="T981">Then he cut.</ta>
            <ta e="T986" id="Seg_1717" s="T984">It cleaned the ground.</ta>
            <ta e="T990" id="Seg_1718" s="T986">Then he beat [=threshed] it all.</ta>
            <ta e="T992" id="Seg_1719" s="T990">It says: "Come!</ta>
            <ta e="T994" id="Seg_1720" s="T992">Take the grain!</ta>
            <ta e="T997" id="Seg_1721" s="T994">I will take it, too."</ta>
            <ta e="T1004" id="Seg_1722" s="T997">[The fox] took two parts, and [gave] one to the sparrow.</ta>
            <ta e="T1007" id="Seg_1723" s="T1004">Then [the sparrow] went [away].</ta>
            <ta e="T1012" id="Seg_1724" s="T1007">The dog [saw him and] said:</ta>
            <ta e="T1019" id="Seg_1725" s="T1012">"Why are you so [sad]?"</ta>
            <ta e="T1023" id="Seg_1726" s="T1019">"The fox tricked me."</ta>
            <ta e="T1031" id="Seg_1727" s="T1023">[The dog says:] "Let's go, I will grab it"; it went, hid in the rye.</ta>
            <ta e="T1038" id="Seg_1728" s="T1031">The fox came, the rye needs to be taken to the mill.</ta>
            <ta e="T1045" id="Seg_1729" s="T1038">Then it sees, that there is an ear of the dog lying.</ta>
            <ta e="T1053" id="Seg_1730" s="T1045">It grabbed and the dog caught it, bit [it] and [it] died.</ta>
            <ta e="T1056" id="Seg_1731" s="T1053">And its rye was left over.</ta>
            <ta e="T1057" id="Seg_1732" s="T1056">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T937" id="Seg_1733" s="T932">Ein Fuchs und ein Spatz lebten zusammen.</ta>
            <ta e="T944" id="Seg_1734" s="T937">Wohin der Fuchs auch geht, dort ging der Spatz auch.</ta>
            <ta e="T948" id="Seg_1735" s="T944">Dann fingen [sie] an, Roggen zu säen.</ta>
            <ta e="T951" id="Seg_1736" s="T948">Sie säten, er wuchs.</ta>
            <ta e="T958" id="Seg_1737" s="T951">Er sagt: ich werde [zu dem Berg] hinauf gehen, ich werde [den Himmel] hier nicht [fallen] lassen</ta>
            <ta e="T961" id="Seg_1738" s="T958">Und du schneidest!</ta>
            <ta e="T966" id="Seg_1739" s="T961">Dort schneidet der Spatz die ganze Zeit, schneidet.</ta>
            <ta e="T969" id="Seg_1740" s="T966">Aber der Fuchs sitzt.</ta>
            <ta e="T971" id="Seg_1741" s="T969">Dann [sagt der Spatz]: „Komm!“</ta>
            <ta e="T973" id="Seg_1742" s="T971">Um das Korn zu dreschen.“</ta>
            <ta e="T981" id="Seg_1743" s="T973">Dann sagt [der Fuchs]: „Ich sitze, ich kann nicht [weg] gehen.“</ta>
            <ta e="T984" id="Seg_1744" s="T981">Dann schnitt er.</ta>
            <ta e="T986" id="Seg_1745" s="T984">Er säuberte den Boden.</ta>
            <ta e="T990" id="Seg_1746" s="T986">Dann schlug [=drosch] er alles.</ta>
            <ta e="T992" id="Seg_1747" s="T990">Er sagt: „Komm!</ta>
            <ta e="T994" id="Seg_1748" s="T992">Nimm das Korn!</ta>
            <ta e="T997" id="Seg_1749" s="T994">Ich werde es auch nehmen.“</ta>
            <ta e="T1004" id="Seg_1750" s="T997">[Der Fuchs] nahm zwei Teile, und [gab] ein dem Spatzen.</ta>
            <ta e="T1007" id="Seg_1751" s="T1004">Dann ging [der Spatz weg]. </ta>
            <ta e="T1012" id="Seg_1752" s="T1007">Der Hund [sah ihn und] sagte:</ta>
            <ta e="T1019" id="Seg_1753" s="T1012">„Warum bist du so [traurig]?“</ta>
            <ta e="T1023" id="Seg_1754" s="T1019">„Der Fuchs trickste mich aus.“</ta>
            <ta e="T1031" id="Seg_1755" s="T1023">[Der Hund sagt:] „Gehen wir, ich schnappe ihn“; er ging, versteckte sich im Roggen.</ta>
            <ta e="T1038" id="Seg_1756" s="T1031">Der Fuchs kam, der Roggen muss zu Mühle gebracht werden.</ta>
            <ta e="T1045" id="Seg_1757" s="T1038">Dann sieht er, das da ein Ohr von einem Hund steckt.</ta>
            <ta e="T1053" id="Seg_1758" s="T1045">Er schnappte und der Hund fasste ihn, bis [ihn] und [er] starb.</ta>
            <ta e="T1056" id="Seg_1759" s="T1053">Und sein Roggen blieb übrig.</ta>
            <ta e="T1057" id="Seg_1760" s="T1056">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T937" id="Seg_1761" s="T932">[GVY:] A tale about a fox and a sparrow, see e.g. http://narodstory.net/bulgarian-skazki.php?id=3</ta>
            <ta e="T966" id="Seg_1762" s="T961">[GVY:] first (mis?)pronounced as pĭtleʔbə</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T932" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T935" />
            <conversion-tli id="T936" />
            <conversion-tli id="T937" />
            <conversion-tli id="T938" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T941" />
            <conversion-tli id="T942" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
            <conversion-tli id="T949" />
            <conversion-tli id="T950" />
            <conversion-tli id="T951" />
            <conversion-tli id="T952" />
            <conversion-tli id="T953" />
            <conversion-tli id="T954" />
            <conversion-tli id="T955" />
            <conversion-tli id="T956" />
            <conversion-tli id="T957" />
            <conversion-tli id="T958" />
            <conversion-tli id="T959" />
            <conversion-tli id="T960" />
            <conversion-tli id="T961" />
            <conversion-tli id="T962" />
            <conversion-tli id="T963" />
            <conversion-tli id="T964" />
            <conversion-tli id="T965" />
            <conversion-tli id="T966" />
            <conversion-tli id="T967" />
            <conversion-tli id="T968" />
            <conversion-tli id="T969" />
            <conversion-tli id="T970" />
            <conversion-tli id="T971" />
            <conversion-tli id="T972" />
            <conversion-tli id="T973" />
            <conversion-tli id="T974" />
            <conversion-tli id="T975" />
            <conversion-tli id="T976" />
            <conversion-tli id="T977" />
            <conversion-tli id="T978" />
            <conversion-tli id="T979" />
            <conversion-tli id="T980" />
            <conversion-tli id="T981" />
            <conversion-tli id="T982" />
            <conversion-tli id="T983" />
            <conversion-tli id="T984" />
            <conversion-tli id="T985" />
            <conversion-tli id="T986" />
            <conversion-tli id="T987" />
            <conversion-tli id="T988" />
            <conversion-tli id="T989" />
            <conversion-tli id="T990" />
            <conversion-tli id="T991" />
            <conversion-tli id="T992" />
            <conversion-tli id="T993" />
            <conversion-tli id="T994" />
            <conversion-tli id="T995" />
            <conversion-tli id="T996" />
            <conversion-tli id="T997" />
            <conversion-tli id="T998" />
            <conversion-tli id="T999" />
            <conversion-tli id="T1000" />
            <conversion-tli id="T1001" />
            <conversion-tli id="T1002" />
            <conversion-tli id="T1003" />
            <conversion-tli id="T1004" />
            <conversion-tli id="T1005" />
            <conversion-tli id="T1006" />
            <conversion-tli id="T1007" />
            <conversion-tli id="T1008" />
            <conversion-tli id="T1009" />
            <conversion-tli id="T1010" />
            <conversion-tli id="T1011" />
            <conversion-tli id="T1012" />
            <conversion-tli id="T1013" />
            <conversion-tli id="T1014" />
            <conversion-tli id="T1015" />
            <conversion-tli id="T1016" />
            <conversion-tli id="T1017" />
            <conversion-tli id="T1018" />
            <conversion-tli id="T1019" />
            <conversion-tli id="T1020" />
            <conversion-tli id="T1021" />
            <conversion-tli id="T1022" />
            <conversion-tli id="T1023" />
            <conversion-tli id="T1024" />
            <conversion-tli id="T1025" />
            <conversion-tli id="T1026" />
            <conversion-tli id="T1027" />
            <conversion-tli id="T1028" />
            <conversion-tli id="T1029" />
            <conversion-tli id="T1030" />
            <conversion-tli id="T1031" />
            <conversion-tli id="T1032" />
            <conversion-tli id="T1033" />
            <conversion-tli id="T1034" />
            <conversion-tli id="T1035" />
            <conversion-tli id="T1036" />
            <conversion-tli id="T1037" />
            <conversion-tli id="T1038" />
            <conversion-tli id="T1039" />
            <conversion-tli id="T1040" />
            <conversion-tli id="T1041" />
            <conversion-tli id="T1042" />
            <conversion-tli id="T1043" />
            <conversion-tli id="T1044" />
            <conversion-tli id="T1045" />
            <conversion-tli id="T1046" />
            <conversion-tli id="T1047" />
            <conversion-tli id="T1048" />
            <conversion-tli id="T1049" />
            <conversion-tli id="T1050" />
            <conversion-tli id="T1051" />
            <conversion-tli id="T1052" />
            <conversion-tli id="T1053" />
            <conversion-tli id="T1054" />
            <conversion-tli id="T1055" />
            <conversion-tli id="T1056" />
            <conversion-tli id="T1057" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
