<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDA138026E-A933-1123-DBA2-B2FE075AB7B2">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_MagpieChild_flk.wav" />
         <referenced-file url="PKZ_196X_MagpieChild_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_MagpieChild_flk\PKZ_196X_MagpieChild_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">58</ud-information>
            <ud-information attribute-name="# HIAT:w">37</ud-information>
            <ud-information attribute-name="# e">37</ud-information>
            <ud-information attribute-name="# HIAT:u">5</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1375" time="0.051" type="appl" />
         <tli id="T1376" time="0.936" type="appl" />
         <tli id="T1377" time="1.82" type="appl" />
         <tli id="T1378" time="2.704" type="appl" />
         <tli id="T1379" time="3.589" type="appl" />
         <tli id="T1380" time="4.719" type="appl" />
         <tli id="T1381" time="5.85" type="appl" />
         <tli id="T1382" time="6.98" type="appl" />
         <tli id="T1383" time="8.111" type="appl" />
         <tli id="T1384" time="9.241" type="appl" />
         <tli id="T1385" time="10.372" type="appl" />
         <tli id="T1386" time="11.502" type="appl" />
         <tli id="T1387" time="12.151" type="appl" />
         <tli id="T1388" time="12.69" type="appl" />
         <tli id="T1389" time="13.228" type="appl" />
         <tli id="T1390" time="13.767" type="appl" />
         <tli id="T1391" time="14.305" type="appl" />
         <tli id="T1392" time="14.844" type="appl" />
         <tli id="T1393" time="15.382" type="appl" />
         <tli id="T1394" time="15.921" type="appl" />
         <tli id="T1395" time="16.459" type="appl" />
         <tli id="T1396" time="17.153" type="appl" />
         <tli id="T1397" time="17.847" type="appl" />
         <tli id="T1398" time="18.54" type="appl" />
         <tli id="T1399" time="19.234" type="appl" />
         <tli id="T1400" time="19.928" type="appl" />
         <tli id="T1401" time="20.622" type="appl" />
         <tli id="T1402" time="21.316" type="appl" />
         <tli id="T1403" time="22.01" type="appl" />
         <tli id="T1404" time="22.703" type="appl" />
         <tli id="T1405" time="23.397" type="appl" />
         <tli id="T1406" time="24.091" type="appl" />
         <tli id="T1407" time="24.785" type="appl" />
         <tli id="T1408" time="25.479" type="appl" />
         <tli id="T1409" time="26.172" type="appl" />
         <tli id="T1410" time="26.866" type="appl" />
         <tli id="T1411" time="27.56" type="appl" />
         <tli id="T1412" time="28.492" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T1412" id="Seg_0" n="sc" s="T1375">
               <ts e="T1379" id="Seg_2" n="HIAT:u" s="T1375">
                  <ts e="T1376" id="Seg_4" n="HIAT:w" s="T1375">Soroka</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1377" id="Seg_7" n="HIAT:w" s="T1376">esseŋdə</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_9" n="HIAT:ip">(</nts>
                  <ts e="T1378" id="Seg_11" n="HIAT:w" s="T1377">s-</ts>
                  <nts id="Seg_12" n="HIAT:ip">)</nts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1379" id="Seg_15" n="HIAT:w" s="T1378">nörbəleʔbə</ts>
                  <nts id="Seg_16" n="HIAT:ip">.</nts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1386" id="Seg_19" n="HIAT:u" s="T1379">
                  <nts id="Seg_20" n="HIAT:ip">"</nts>
                  <ts e="T1380" id="Seg_22" n="HIAT:w" s="T1379">Kamən</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1381" id="Seg_25" n="HIAT:w" s="T1380">kuza</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1382" id="Seg_28" n="HIAT:w" s="T1381">dʼünə</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1383" id="Seg_31" n="HIAT:w" s="T1382">saʔməlaʔbə</ts>
                  <nts id="Seg_32" n="HIAT:ip">,</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1384" id="Seg_35" n="HIAT:w" s="T1383">to</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1385" id="Seg_38" n="HIAT:w" s="T1384">nʼergöleʔ</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1386" id="Seg_41" n="HIAT:w" s="T1385">büžü</ts>
                  <nts id="Seg_42" n="HIAT:ip">"</nts>
                  <nts id="Seg_43" n="HIAT:ip">.</nts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1395" id="Seg_46" n="HIAT:u" s="T1386">
                  <ts e="T1387" id="Seg_48" n="HIAT:w" s="T1386">Dĭgəttə</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1388" id="Seg_51" n="HIAT:w" s="T1387">dĭ</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1389" id="Seg_54" n="HIAT:w" s="T1388">măndə:</ts>
                  <nts id="Seg_55" n="HIAT:ip">"</nts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1390" id="Seg_58" n="HIAT:w" s="T1389">Kamən</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1391" id="Seg_61" n="HIAT:w" s="T1390">kuza</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1392" id="Seg_64" n="HIAT:w" s="T1391">kulim</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1393" id="Seg_67" n="HIAT:w" s="T1392">dăk</ts>
                  <nts id="Seg_68" n="HIAT:ip">,</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1394" id="Seg_71" n="HIAT:w" s="T1393">nʼergözittə</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1395" id="Seg_74" n="HIAT:w" s="T1394">nada</ts>
                  <nts id="Seg_75" n="HIAT:ip">"</nts>
                  <nts id="Seg_76" n="HIAT:ip">.</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1411" id="Seg_79" n="HIAT:u" s="T1395">
                  <ts e="T1396" id="Seg_81" n="HIAT:w" s="T1395">Dĭ</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_83" n="HIAT:ip">(</nts>
                  <ts e="T1397" id="Seg_85" n="HIAT:w" s="T1396">dĭ=</ts>
                  <nts id="Seg_86" n="HIAT:ip">)</nts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1398" id="Seg_89" n="HIAT:w" s="T1397">dĭm</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1399" id="Seg_92" n="HIAT:w" s="T1398">bar</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1400" id="Seg_95" n="HIAT:w" s="T1399">davaj</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1401" id="Seg_98" n="HIAT:w" s="T1400">ajərzittə</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1402" id="Seg_101" n="HIAT:w" s="T1401">i</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_103" n="HIAT:ip">(</nts>
                  <ts e="T1403" id="Seg_105" n="HIAT:w" s="T1402">ulu-</ts>
                  <nts id="Seg_106" n="HIAT:ip">)</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1404" id="Seg_109" n="HIAT:w" s="T1403">ulundə</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1405" id="Seg_112" n="HIAT:w" s="T1404">toʔnarbi</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1406" id="Seg_115" n="HIAT:w" s="T1405">i</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_117" n="HIAT:ip">(</nts>
                  <ts e="T1407" id="Seg_119" n="HIAT:w" s="T1406">kutlaʔp-</ts>
                  <nts id="Seg_120" n="HIAT:ip">)</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1408" id="Seg_123" n="HIAT:w" s="T1407">kuʔpi</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_125" n="HIAT:ip">(</nts>
                  <ts e="T1409" id="Seg_127" n="HIAT:w" s="T1408">svoi-</ts>
                  <nts id="Seg_128" n="HIAT:ip">)</nts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1410" id="Seg_131" n="HIAT:w" s="T1409">bostə</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1411" id="Seg_134" n="HIAT:w" s="T1410">ešši</ts>
                  <nts id="Seg_135" n="HIAT:ip">.</nts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1412" id="Seg_138" n="HIAT:u" s="T1411">
                  <ts e="T1412" id="Seg_140" n="HIAT:w" s="T1411">Kabarləj</ts>
                  <nts id="Seg_141" n="HIAT:ip">.</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T1412" id="Seg_143" n="sc" s="T1375">
               <ts e="T1376" id="Seg_145" n="e" s="T1375">Soroka </ts>
               <ts e="T1377" id="Seg_147" n="e" s="T1376">esseŋdə </ts>
               <ts e="T1378" id="Seg_149" n="e" s="T1377">(s-) </ts>
               <ts e="T1379" id="Seg_151" n="e" s="T1378">nörbəleʔbə. </ts>
               <ts e="T1380" id="Seg_153" n="e" s="T1379">"Kamən </ts>
               <ts e="T1381" id="Seg_155" n="e" s="T1380">kuza </ts>
               <ts e="T1382" id="Seg_157" n="e" s="T1381">dʼünə </ts>
               <ts e="T1383" id="Seg_159" n="e" s="T1382">saʔməlaʔbə, </ts>
               <ts e="T1384" id="Seg_161" n="e" s="T1383">to </ts>
               <ts e="T1385" id="Seg_163" n="e" s="T1384">nʼergöleʔ </ts>
               <ts e="T1386" id="Seg_165" n="e" s="T1385">büžü". </ts>
               <ts e="T1387" id="Seg_167" n="e" s="T1386">Dĭgəttə </ts>
               <ts e="T1388" id="Seg_169" n="e" s="T1387">dĭ </ts>
               <ts e="T1389" id="Seg_171" n="e" s="T1388">măndə:" </ts>
               <ts e="T1390" id="Seg_173" n="e" s="T1389">Kamən </ts>
               <ts e="T1391" id="Seg_175" n="e" s="T1390">kuza </ts>
               <ts e="T1392" id="Seg_177" n="e" s="T1391">kulim </ts>
               <ts e="T1393" id="Seg_179" n="e" s="T1392">dăk, </ts>
               <ts e="T1394" id="Seg_181" n="e" s="T1393">nʼergözittə </ts>
               <ts e="T1395" id="Seg_183" n="e" s="T1394">nada". </ts>
               <ts e="T1396" id="Seg_185" n="e" s="T1395">Dĭ </ts>
               <ts e="T1397" id="Seg_187" n="e" s="T1396">(dĭ=) </ts>
               <ts e="T1398" id="Seg_189" n="e" s="T1397">dĭm </ts>
               <ts e="T1399" id="Seg_191" n="e" s="T1398">bar </ts>
               <ts e="T1400" id="Seg_193" n="e" s="T1399">davaj </ts>
               <ts e="T1401" id="Seg_195" n="e" s="T1400">ajərzittə </ts>
               <ts e="T1402" id="Seg_197" n="e" s="T1401">i </ts>
               <ts e="T1403" id="Seg_199" n="e" s="T1402">(ulu-) </ts>
               <ts e="T1404" id="Seg_201" n="e" s="T1403">ulundə </ts>
               <ts e="T1405" id="Seg_203" n="e" s="T1404">toʔnarbi </ts>
               <ts e="T1406" id="Seg_205" n="e" s="T1405">i </ts>
               <ts e="T1407" id="Seg_207" n="e" s="T1406">(kutlaʔp-) </ts>
               <ts e="T1408" id="Seg_209" n="e" s="T1407">kuʔpi </ts>
               <ts e="T1409" id="Seg_211" n="e" s="T1408">(svoi-) </ts>
               <ts e="T1410" id="Seg_213" n="e" s="T1409">bostə </ts>
               <ts e="T1411" id="Seg_215" n="e" s="T1410">ešši. </ts>
               <ts e="T1412" id="Seg_217" n="e" s="T1411">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T1379" id="Seg_218" s="T1375">PKZ_196X_MagpieChild_flk.001 (001)</ta>
            <ta e="T1386" id="Seg_219" s="T1379">PKZ_196X_MagpieChild_flk.002 (002)</ta>
            <ta e="T1395" id="Seg_220" s="T1386">PKZ_196X_MagpieChild_flk.003 (003)</ta>
            <ta e="T1411" id="Seg_221" s="T1395">PKZ_196X_MagpieChild_flk.004 (004)</ta>
            <ta e="T1412" id="Seg_222" s="T1411">PKZ_196X_MagpieChild_flk.005 (005)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T1379" id="Seg_223" s="T1375">Soroka esseŋdə (s-) nörbəleʔbə. </ta>
            <ta e="T1386" id="Seg_224" s="T1379">"Kamən kuza dʼünə saʔməlaʔbə, to nʼergöleʔ büžü". </ta>
            <ta e="T1395" id="Seg_225" s="T1386">Dĭgəttə dĭ măndə:" Kamən kuza kulim dăk, nʼergözittə nada". </ta>
            <ta e="T1411" id="Seg_226" s="T1395">Dĭ (dĭ=) dĭm bar davaj ajərzittə i (ulu-) ulundə toʔnarbi i (kutlaʔp-) kuʔpi (svoi-) bostə ešši. </ta>
            <ta e="T1412" id="Seg_227" s="T1411">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1376" id="Seg_228" s="T1375">soroka</ta>
            <ta e="T1377" id="Seg_229" s="T1376">es-seŋ-də</ta>
            <ta e="T1379" id="Seg_230" s="T1378">nörbə-leʔbə</ta>
            <ta e="T1380" id="Seg_231" s="T1379">kamən</ta>
            <ta e="T1381" id="Seg_232" s="T1380">kuza</ta>
            <ta e="T1382" id="Seg_233" s="T1381">dʼü-nə</ta>
            <ta e="T1383" id="Seg_234" s="T1382">saʔmə-laʔbə</ta>
            <ta e="T1384" id="Seg_235" s="T1383">to</ta>
            <ta e="T1385" id="Seg_236" s="T1384">nʼergö-leʔ</ta>
            <ta e="T1386" id="Seg_237" s="T1385">büžü</ta>
            <ta e="T1387" id="Seg_238" s="T1386">dĭgəttə</ta>
            <ta e="T1388" id="Seg_239" s="T1387">dĭ</ta>
            <ta e="T1389" id="Seg_240" s="T1388">măn-də</ta>
            <ta e="T1390" id="Seg_241" s="T1389">kamən</ta>
            <ta e="T1391" id="Seg_242" s="T1390">kuza</ta>
            <ta e="T1392" id="Seg_243" s="T1391">ku-li-m</ta>
            <ta e="T1393" id="Seg_244" s="T1392">dăk</ta>
            <ta e="T1394" id="Seg_245" s="T1393">nʼergö-zittə</ta>
            <ta e="T1395" id="Seg_246" s="T1394">nada</ta>
            <ta e="T1396" id="Seg_247" s="T1395">dĭ</ta>
            <ta e="T1397" id="Seg_248" s="T1396">dĭ</ta>
            <ta e="T1398" id="Seg_249" s="T1397">dĭ-m</ta>
            <ta e="T1399" id="Seg_250" s="T1398">bar</ta>
            <ta e="T1400" id="Seg_251" s="T1399">davaj</ta>
            <ta e="T1401" id="Seg_252" s="T1400">ajər-zittə</ta>
            <ta e="T1402" id="Seg_253" s="T1401">i</ta>
            <ta e="T1404" id="Seg_254" s="T1403">ulu-ndə</ta>
            <ta e="T1405" id="Seg_255" s="T1404">toʔ-nar-bi</ta>
            <ta e="T1406" id="Seg_256" s="T1405">i</ta>
            <ta e="T1408" id="Seg_257" s="T1407">kuʔ-pi</ta>
            <ta e="T1410" id="Seg_258" s="T1409">bos-tə</ta>
            <ta e="T1411" id="Seg_259" s="T1410">ešši</ta>
            <ta e="T1412" id="Seg_260" s="T1411">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1376" id="Seg_261" s="T1375">soroka</ta>
            <ta e="T1377" id="Seg_262" s="T1376">ešši-zAŋ-Tə</ta>
            <ta e="T1379" id="Seg_263" s="T1378">nörbə-laʔbə</ta>
            <ta e="T1380" id="Seg_264" s="T1379">kamən</ta>
            <ta e="T1381" id="Seg_265" s="T1380">kuza</ta>
            <ta e="T1382" id="Seg_266" s="T1381">tʼo-Tə</ta>
            <ta e="T1383" id="Seg_267" s="T1382">saʔmə-laʔbə</ta>
            <ta e="T1384" id="Seg_268" s="T1383">to</ta>
            <ta e="T1385" id="Seg_269" s="T1384">nʼergö-lAʔ</ta>
            <ta e="T1386" id="Seg_270" s="T1385">büžü</ta>
            <ta e="T1387" id="Seg_271" s="T1386">dĭgəttə</ta>
            <ta e="T1388" id="Seg_272" s="T1387">dĭ</ta>
            <ta e="T1389" id="Seg_273" s="T1388">măn-ntə</ta>
            <ta e="T1390" id="Seg_274" s="T1389">kamən</ta>
            <ta e="T1391" id="Seg_275" s="T1390">kuza</ta>
            <ta e="T1392" id="Seg_276" s="T1391">ku-lV-m</ta>
            <ta e="T1393" id="Seg_277" s="T1392">tak</ta>
            <ta e="T1394" id="Seg_278" s="T1393">nʼergö-zittə</ta>
            <ta e="T1395" id="Seg_279" s="T1394">nadə</ta>
            <ta e="T1396" id="Seg_280" s="T1395">dĭ</ta>
            <ta e="T1397" id="Seg_281" s="T1396">dĭ</ta>
            <ta e="T1398" id="Seg_282" s="T1397">dĭ-m</ta>
            <ta e="T1399" id="Seg_283" s="T1398">bar</ta>
            <ta e="T1400" id="Seg_284" s="T1399">davaj</ta>
            <ta e="T1401" id="Seg_285" s="T1400">ajər-zittə</ta>
            <ta e="T1402" id="Seg_286" s="T1401">i</ta>
            <ta e="T1404" id="Seg_287" s="T1403">ulu-gəndə</ta>
            <ta e="T1405" id="Seg_288" s="T1404">toʔbdə-nar-bi</ta>
            <ta e="T1406" id="Seg_289" s="T1405">i</ta>
            <ta e="T1408" id="Seg_290" s="T1407">kut-bi</ta>
            <ta e="T1410" id="Seg_291" s="T1409">bos-də</ta>
            <ta e="T1411" id="Seg_292" s="T1410">ešši</ta>
            <ta e="T1412" id="Seg_293" s="T1411">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1376" id="Seg_294" s="T1375">magpie</ta>
            <ta e="T1377" id="Seg_295" s="T1376">child-PL-LAT</ta>
            <ta e="T1379" id="Seg_296" s="T1378">tell-DUR.[3SG]</ta>
            <ta e="T1380" id="Seg_297" s="T1379">when</ta>
            <ta e="T1381" id="Seg_298" s="T1380">man.[NOM.SG]</ta>
            <ta e="T1382" id="Seg_299" s="T1381">place-LAT</ta>
            <ta e="T1383" id="Seg_300" s="T1382">fall-DUR.[3SG]</ta>
            <ta e="T1384" id="Seg_301" s="T1383">then</ta>
            <ta e="T1385" id="Seg_302" s="T1384">fly-CVB</ta>
            <ta e="T1386" id="Seg_303" s="T1385">soon</ta>
            <ta e="T1387" id="Seg_304" s="T1386">then</ta>
            <ta e="T1388" id="Seg_305" s="T1387">this.[NOM.SG]</ta>
            <ta e="T1389" id="Seg_306" s="T1388">say-IPFVZ.[3SG]</ta>
            <ta e="T1390" id="Seg_307" s="T1389">when</ta>
            <ta e="T1391" id="Seg_308" s="T1390">man.[NOM.SG]</ta>
            <ta e="T1392" id="Seg_309" s="T1391">see-FUT-1SG</ta>
            <ta e="T1393" id="Seg_310" s="T1392">so</ta>
            <ta e="T1394" id="Seg_311" s="T1393">fly-INF.LAT</ta>
            <ta e="T1395" id="Seg_312" s="T1394">one.should</ta>
            <ta e="T1396" id="Seg_313" s="T1395">this.[NOM.SG]</ta>
            <ta e="T1397" id="Seg_314" s="T1396">this.[NOM.SG]</ta>
            <ta e="T1398" id="Seg_315" s="T1397">this-ACC</ta>
            <ta e="T1399" id="Seg_316" s="T1398">PTCL</ta>
            <ta e="T1400" id="Seg_317" s="T1399">INCH</ta>
            <ta e="T1401" id="Seg_318" s="T1400">feel.sorry-INF.LAT</ta>
            <ta e="T1402" id="Seg_319" s="T1401">and</ta>
            <ta e="T1404" id="Seg_320" s="T1403">head-LAT/LOC.3SG</ta>
            <ta e="T1405" id="Seg_321" s="T1404">hit-MULT-PST.[3SG]</ta>
            <ta e="T1406" id="Seg_322" s="T1405">and</ta>
            <ta e="T1408" id="Seg_323" s="T1407">kill-PST.[3SG]</ta>
            <ta e="T1410" id="Seg_324" s="T1409">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T1411" id="Seg_325" s="T1410">child.[NOM.SG]</ta>
            <ta e="T1412" id="Seg_326" s="T1411">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1376" id="Seg_327" s="T1375">сорока</ta>
            <ta e="T1377" id="Seg_328" s="T1376">ребенок-PL-LAT</ta>
            <ta e="T1379" id="Seg_329" s="T1378">сказать-DUR.[3SG]</ta>
            <ta e="T1380" id="Seg_330" s="T1379">когда</ta>
            <ta e="T1381" id="Seg_331" s="T1380">мужчина.[NOM.SG]</ta>
            <ta e="T1382" id="Seg_332" s="T1381">место-LAT</ta>
            <ta e="T1383" id="Seg_333" s="T1382">упасть-DUR.[3SG]</ta>
            <ta e="T1384" id="Seg_334" s="T1383">то</ta>
            <ta e="T1385" id="Seg_335" s="T1384">лететь-CVB</ta>
            <ta e="T1386" id="Seg_336" s="T1385">скоро</ta>
            <ta e="T1387" id="Seg_337" s="T1386">тогда</ta>
            <ta e="T1388" id="Seg_338" s="T1387">этот.[NOM.SG]</ta>
            <ta e="T1389" id="Seg_339" s="T1388">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1390" id="Seg_340" s="T1389">когда</ta>
            <ta e="T1391" id="Seg_341" s="T1390">мужчина.[NOM.SG]</ta>
            <ta e="T1392" id="Seg_342" s="T1391">видеть-FUT-1SG</ta>
            <ta e="T1393" id="Seg_343" s="T1392">так</ta>
            <ta e="T1394" id="Seg_344" s="T1393">лететь-INF.LAT</ta>
            <ta e="T1395" id="Seg_345" s="T1394">надо</ta>
            <ta e="T1396" id="Seg_346" s="T1395">этот.[NOM.SG]</ta>
            <ta e="T1397" id="Seg_347" s="T1396">этот.[NOM.SG]</ta>
            <ta e="T1398" id="Seg_348" s="T1397">этот-ACC</ta>
            <ta e="T1399" id="Seg_349" s="T1398">PTCL</ta>
            <ta e="T1400" id="Seg_350" s="T1399">INCH</ta>
            <ta e="T1401" id="Seg_351" s="T1400">жалеть-INF.LAT</ta>
            <ta e="T1402" id="Seg_352" s="T1401">и</ta>
            <ta e="T1404" id="Seg_353" s="T1403">голова-LAT/LOC.3SG</ta>
            <ta e="T1405" id="Seg_354" s="T1404">ударить-MULT-PST.[3SG]</ta>
            <ta e="T1406" id="Seg_355" s="T1405">и</ta>
            <ta e="T1408" id="Seg_356" s="T1407">убить-PST.[3SG]</ta>
            <ta e="T1410" id="Seg_357" s="T1409">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T1411" id="Seg_358" s="T1410">ребенок.[NOM.SG]</ta>
            <ta e="T1412" id="Seg_359" s="T1411">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1376" id="Seg_360" s="T1375">n</ta>
            <ta e="T1377" id="Seg_361" s="T1376">n-n:num-n:case</ta>
            <ta e="T1379" id="Seg_362" s="T1378">v-v&gt;v-v:pn</ta>
            <ta e="T1380" id="Seg_363" s="T1379">que</ta>
            <ta e="T1381" id="Seg_364" s="T1380">n-n:case</ta>
            <ta e="T1382" id="Seg_365" s="T1381">n-n:case</ta>
            <ta e="T1383" id="Seg_366" s="T1382">v-v&gt;v-v:pn</ta>
            <ta e="T1384" id="Seg_367" s="T1383">conj</ta>
            <ta e="T1385" id="Seg_368" s="T1384">v-v:n.fin</ta>
            <ta e="T1386" id="Seg_369" s="T1385">adv</ta>
            <ta e="T1387" id="Seg_370" s="T1386">adv</ta>
            <ta e="T1388" id="Seg_371" s="T1387">dempro-n:case</ta>
            <ta e="T1389" id="Seg_372" s="T1388">v-v&gt;v-v:pn</ta>
            <ta e="T1390" id="Seg_373" s="T1389">que</ta>
            <ta e="T1391" id="Seg_374" s="T1390">n-n:case</ta>
            <ta e="T1392" id="Seg_375" s="T1391">v-v:tense-v:pn</ta>
            <ta e="T1393" id="Seg_376" s="T1392">ptcl</ta>
            <ta e="T1394" id="Seg_377" s="T1393">v-v:n.fin</ta>
            <ta e="T1395" id="Seg_378" s="T1394">ptcl</ta>
            <ta e="T1396" id="Seg_379" s="T1395">dempro-n:case</ta>
            <ta e="T1397" id="Seg_380" s="T1396">dempro-n:case</ta>
            <ta e="T1398" id="Seg_381" s="T1397">dempro-n:case</ta>
            <ta e="T1399" id="Seg_382" s="T1398">ptcl</ta>
            <ta e="T1400" id="Seg_383" s="T1399">ptcl</ta>
            <ta e="T1401" id="Seg_384" s="T1400">v-v:n.fin</ta>
            <ta e="T1402" id="Seg_385" s="T1401">conj</ta>
            <ta e="T1404" id="Seg_386" s="T1403">n-n:case.poss</ta>
            <ta e="T1405" id="Seg_387" s="T1404">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1406" id="Seg_388" s="T1405">conj</ta>
            <ta e="T1408" id="Seg_389" s="T1407">v-v:tense-v:pn</ta>
            <ta e="T1410" id="Seg_390" s="T1409">refl-n:case.poss</ta>
            <ta e="T1411" id="Seg_391" s="T1410">n-n:case</ta>
            <ta e="T1412" id="Seg_392" s="T1411">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1376" id="Seg_393" s="T1375">n</ta>
            <ta e="T1377" id="Seg_394" s="T1376">n</ta>
            <ta e="T1379" id="Seg_395" s="T1378">v</ta>
            <ta e="T1380" id="Seg_396" s="T1379">conj</ta>
            <ta e="T1381" id="Seg_397" s="T1380">n</ta>
            <ta e="T1382" id="Seg_398" s="T1381">n</ta>
            <ta e="T1383" id="Seg_399" s="T1382">v</ta>
            <ta e="T1384" id="Seg_400" s="T1383">conj</ta>
            <ta e="T1385" id="Seg_401" s="T1384">v</ta>
            <ta e="T1386" id="Seg_402" s="T1385">adv</ta>
            <ta e="T1387" id="Seg_403" s="T1386">adv</ta>
            <ta e="T1388" id="Seg_404" s="T1387">dempro</ta>
            <ta e="T1389" id="Seg_405" s="T1388">v</ta>
            <ta e="T1390" id="Seg_406" s="T1389">conj</ta>
            <ta e="T1391" id="Seg_407" s="T1390">n</ta>
            <ta e="T1392" id="Seg_408" s="T1391">v</ta>
            <ta e="T1393" id="Seg_409" s="T1392">ptcl</ta>
            <ta e="T1394" id="Seg_410" s="T1393">v</ta>
            <ta e="T1395" id="Seg_411" s="T1394">ptcl</ta>
            <ta e="T1396" id="Seg_412" s="T1395">dempro</ta>
            <ta e="T1397" id="Seg_413" s="T1396">dempro</ta>
            <ta e="T1398" id="Seg_414" s="T1397">dempro</ta>
            <ta e="T1399" id="Seg_415" s="T1398">ptcl</ta>
            <ta e="T1400" id="Seg_416" s="T1399">ptcl</ta>
            <ta e="T1401" id="Seg_417" s="T1400">v</ta>
            <ta e="T1402" id="Seg_418" s="T1401">conj</ta>
            <ta e="T1404" id="Seg_419" s="T1403">n</ta>
            <ta e="T1405" id="Seg_420" s="T1404">v</ta>
            <ta e="T1406" id="Seg_421" s="T1405">conj</ta>
            <ta e="T1408" id="Seg_422" s="T1407">v</ta>
            <ta e="T1410" id="Seg_423" s="T1409">refl</ta>
            <ta e="T1411" id="Seg_424" s="T1410">n</ta>
            <ta e="T1412" id="Seg_425" s="T1411">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1376" id="Seg_426" s="T1375">np.h:A</ta>
            <ta e="T1377" id="Seg_427" s="T1376">np.h:R</ta>
            <ta e="T1381" id="Seg_428" s="T1380">np.h:E</ta>
            <ta e="T1382" id="Seg_429" s="T1381">np:G</ta>
            <ta e="T1386" id="Seg_430" s="T1385">adv:Time</ta>
            <ta e="T1387" id="Seg_431" s="T1386">adv:Time</ta>
            <ta e="T1388" id="Seg_432" s="T1387">pro.h:A</ta>
            <ta e="T1391" id="Seg_433" s="T1390">np.h:Th</ta>
            <ta e="T1392" id="Seg_434" s="T1391">0.1.h:E</ta>
            <ta e="T1396" id="Seg_435" s="T1395">pro.h:E</ta>
            <ta e="T1398" id="Seg_436" s="T1397">pro.h:B</ta>
            <ta e="T1404" id="Seg_437" s="T1403">np:L</ta>
            <ta e="T1405" id="Seg_438" s="T1404">0.3.h:A</ta>
            <ta e="T1408" id="Seg_439" s="T1407">0.3.h:A</ta>
            <ta e="T1410" id="Seg_440" s="T1409">pro.h:Poss</ta>
            <ta e="T1411" id="Seg_441" s="T1410">np.h:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1376" id="Seg_442" s="T1375">np.h:S</ta>
            <ta e="T1379" id="Seg_443" s="T1378">v:pred</ta>
            <ta e="T1380" id="Seg_444" s="T1379">s:temp</ta>
            <ta e="T1381" id="Seg_445" s="T1380">np.h:S</ta>
            <ta e="T1383" id="Seg_446" s="T1382">v:pred </ta>
            <ta e="T1385" id="Seg_447" s="T1384">conv:pred</ta>
            <ta e="T1388" id="Seg_448" s="T1387">pro.h:S</ta>
            <ta e="T1389" id="Seg_449" s="T1388">v:pred</ta>
            <ta e="T1390" id="Seg_450" s="T1389">s:temp</ta>
            <ta e="T1391" id="Seg_451" s="T1390">np.h:O</ta>
            <ta e="T1392" id="Seg_452" s="T1391">v:pred 0.1.h:S</ta>
            <ta e="T1395" id="Seg_453" s="T1394">ptcl:pred</ta>
            <ta e="T1398" id="Seg_454" s="T1397">pro.h:O</ta>
            <ta e="T1400" id="Seg_455" s="T1399">ptcl:pred</ta>
            <ta e="T1405" id="Seg_456" s="T1404">v:pred 0.3.h:S</ta>
            <ta e="T1408" id="Seg_457" s="T1407">v:pred 0.3.h:S</ta>
            <ta e="T1411" id="Seg_458" s="T1410">np.h:O</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T1376" id="Seg_459" s="T1375">RUS:cult</ta>
            <ta e="T1384" id="Seg_460" s="T1383">RUS:gram</ta>
            <ta e="T1393" id="Seg_461" s="T1392">RUS:gram</ta>
            <ta e="T1395" id="Seg_462" s="T1394">RUS:mod</ta>
            <ta e="T1399" id="Seg_463" s="T1398">TURK:disc</ta>
            <ta e="T1400" id="Seg_464" s="T1399">RUS:gram</ta>
            <ta e="T1402" id="Seg_465" s="T1401">RUS:gram</ta>
            <ta e="T1406" id="Seg_466" s="T1405">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T1379" id="Seg_467" s="T1375">Сорока детям рассказывает.</ta>
            <ta e="T1386" id="Seg_468" s="T1379">"Когда человек на землю падает, улетайте скорее".</ta>
            <ta e="T1395" id="Seg_469" s="T1386">Потом говорит: "Когда человека увижу, надо улетать".</ta>
            <ta e="T1411" id="Seg_470" s="T1395">Она(?) (его?) стала жалеть и бить по голове, и убила собственного ребёнка. [?]</ta>
            <ta e="T1412" id="Seg_471" s="T1411">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T1379" id="Seg_472" s="T1375">A magpie is telling its children.</ta>
            <ta e="T1386" id="Seg_473" s="T1379">"When a man falls on the ground, fly [away] soon."</ta>
            <ta e="T1395" id="Seg_474" s="T1386">Then it says: "When I see a man, [I] should fly [away]."</ta>
            <ta e="T1411" id="Seg_475" s="T1395">He started to feel sorry for it and hit it on its head and killed its own child. [?]</ta>
            <ta e="T1412" id="Seg_476" s="T1411">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T1379" id="Seg_477" s="T1375">Eine Elster erzählt ihren Kindern.</ta>
            <ta e="T1386" id="Seg_478" s="T1379">„Wenn ein Mann zu Boden fällt, fliegt bald [weg].“</ta>
            <ta e="T1395" id="Seg_479" s="T1386">Dann sagte sie: „Wenn ich einen Mann sehe, sollte [ich] [weg] fliegen.“</ta>
            <ta e="T1411" id="Seg_480" s="T1395">Es fing an ihr Leid zu tun, und sie schlug es am Kopf und tötete ihr eigenes Kind. [?]</ta>
            <ta e="T1412" id="Seg_481" s="T1411">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T1379" id="Seg_482" s="T1375">[KlT:] сорока Ru. ’magpie’.</ta>
            <ta e="T1386" id="Seg_483" s="T1379">[KlT:] stem+2PL as IMP - instead of IMP marker.</ta>
            <ta e="T1411" id="Seg_484" s="T1395">[KlT:] No idea what is going on here.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1375" />
            <conversion-tli id="T1376" />
            <conversion-tli id="T1377" />
            <conversion-tli id="T1378" />
            <conversion-tli id="T1379" />
            <conversion-tli id="T1380" />
            <conversion-tli id="T1381" />
            <conversion-tli id="T1382" />
            <conversion-tli id="T1383" />
            <conversion-tli id="T1384" />
            <conversion-tli id="T1385" />
            <conversion-tli id="T1386" />
            <conversion-tli id="T1387" />
            <conversion-tli id="T1388" />
            <conversion-tli id="T1389" />
            <conversion-tli id="T1390" />
            <conversion-tli id="T1391" />
            <conversion-tli id="T1392" />
            <conversion-tli id="T1393" />
            <conversion-tli id="T1394" />
            <conversion-tli id="T1395" />
            <conversion-tli id="T1396" />
            <conversion-tli id="T1397" />
            <conversion-tli id="T1398" />
            <conversion-tli id="T1399" />
            <conversion-tli id="T1400" />
            <conversion-tli id="T1401" />
            <conversion-tli id="T1402" />
            <conversion-tli id="T1403" />
            <conversion-tli id="T1404" />
            <conversion-tli id="T1405" />
            <conversion-tli id="T1406" />
            <conversion-tli id="T1407" />
            <conversion-tli id="T1408" />
            <conversion-tli id="T1409" />
            <conversion-tli id="T1410" />
            <conversion-tli id="T1411" />
            <conversion-tli id="T1412" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
