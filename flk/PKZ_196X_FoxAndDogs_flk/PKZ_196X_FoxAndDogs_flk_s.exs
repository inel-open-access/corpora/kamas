<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDC71BAFFC-59C4-D350-C47D-8D447820456D">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_FoxAndDogs_flk.wav" />
         <referenced-file url="PKZ_196X_FoxAndDogs_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_FoxAndDogs_flk\PKZ_196X_FoxAndDogs_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">152</ud-information>
            <ud-information attribute-name="# HIAT:w">92</ud-information>
            <ud-information attribute-name="# e">92</ud-information>
            <ud-information attribute-name="# HIAT:u">20</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T840" time="0.031" type="appl" />
         <tli id="T841" time="0.675" type="appl" />
         <tli id="T842" time="1.32" type="appl" />
         <tli id="T843" time="1.964" type="appl" />
         <tli id="T844" time="2.608" type="appl" />
         <tli id="T845" time="3.662" type="appl" />
         <tli id="T846" time="4.716" type="appl" />
         <tli id="T847" time="5.77" type="appl" />
         <tli id="T848" time="6.824" type="appl" />
         <tli id="T849" time="7.363" type="appl" />
         <tli id="T850" time="7.902" type="appl" />
         <tli id="T851" time="8.441" type="appl" />
         <tli id="T852" time="8.98" type="appl" />
         <tli id="T853" time="9.518" type="appl" />
         <tli id="T854" time="10.057" type="appl" />
         <tli id="T855" time="10.596" type="appl" />
         <tli id="T856" time="11.135" type="appl" />
         <tli id="T857" time="11.674" type="appl" />
         <tli id="T858" time="12.288" type="appl" />
         <tli id="T859" time="12.902" type="appl" />
         <tli id="T860" time="13.516" type="appl" />
         <tli id="T861" time="14.48582724584399" />
         <tli id="T862" time="15.748" type="appl" />
         <tli id="T863" time="17.57231505754476" />
         <tli id="T864" time="18.934" type="appl" />
         <tli id="T865" time="20.138" type="appl" />
         <tli id="T866" time="21.321" type="appl" />
         <tli id="T867" time="22.368" type="appl" />
         <tli id="T868" time="23.414" type="appl" />
         <tli id="T869" time="24.34" type="appl" />
         <tli id="T870" time="25.161" type="appl" />
         <tli id="T871" time="25.981" type="appl" />
         <tli id="T872" time="26.802" type="appl" />
         <tli id="T873" time="27.622" type="appl" />
         <tli id="T874" time="28.442" type="appl" />
         <tli id="T875" time="29.263" type="appl" />
         <tli id="T876" time="30.083" type="appl" />
         <tli id="T877" time="30.904" type="appl" />
         <tli id="T878" time="32.59811101342711" />
         <tli id="T879" time="33.562" type="appl" />
         <tli id="T880" time="34.192" type="appl" />
         <tli id="T881" time="34.822" type="appl" />
         <tli id="T882" time="35.452" type="appl" />
         <tli id="T883" time="36.082" type="appl" />
         <tli id="T884" time="37.232" type="appl" />
         <tli id="T885" time="38.382" type="appl" />
         <tli id="T886" time="39.532" type="appl" />
         <tli id="T887" time="40.683" type="appl" />
         <tli id="T888" time="41.833" type="appl" />
         <tli id="T889" time="43.63080502717392" />
         <tli id="T890" time="44.212" type="appl" />
         <tli id="T891" time="44.79" type="appl" />
         <tli id="T892" time="45.367" type="appl" />
         <tli id="T893" time="45.944" type="appl" />
         <tli id="T894" time="47.053" type="appl" />
         <tli id="T895" time="48.162" type="appl" />
         <tli id="T896" time="49.271" type="appl" />
         <tli id="T897" time="50.38" type="appl" />
         <tli id="T898" time="51.489" type="appl" />
         <tli id="T899" time="52.598" type="appl" />
         <tli id="T900" time="53.707" type="appl" />
         <tli id="T901" time="55.31012823689258" />
         <tli id="T902" time="56.309" type="appl" />
         <tli id="T903" time="57.414" type="appl" />
         <tli id="T904" time="58.52" type="appl" />
         <tli id="T905" time="58.946" type="appl" />
         <tli id="T906" time="59.372" type="appl" />
         <tli id="T907" time="60.272" type="appl" />
         <tli id="T908" time="61.171" type="appl" />
         <tli id="T909" time="62.071" type="appl" />
         <tli id="T910" time="62.97" type="appl" />
         <tli id="T911" time="64.2096125319693" />
         <tli id="T912" time="65.03" type="appl" />
         <tli id="T913" time="66.40281877397697" />
         <tli id="T914" time="67.319" type="appl" />
         <tli id="T915" time="68.271" type="appl" />
         <tli id="T916" time="69.224" type="appl" />
         <tli id="T917" time="70.177" type="appl" />
         <tli id="T918" time="71.129" type="appl" />
         <tli id="T919" time="72.082" type="appl" />
         <tli id="T920" time="73.097" type="appl" />
         <tli id="T921" time="73.982" type="appl" />
         <tli id="T922" time="74.868" type="appl" />
         <tli id="T923" time="75.753" type="appl" />
         <tli id="T924" time="76.639" type="appl" />
         <tli id="T925" time="77.427" type="appl" />
         <tli id="T926" time="78.215" type="appl" />
         <tli id="T927" time="80.29534706681585" />
         <tli id="T928" time="81.336" type="appl" />
         <tli id="T929" time="82.378" type="appl" />
         <tli id="T930" time="83.42" type="appl" />
         <tli id="T931" time="84.92174564418158" />
         <tli id="T932" time="86.015" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T932" id="Seg_0" n="sc" s="T840">
               <ts e="T844" id="Seg_2" n="HIAT:u" s="T840">
                  <ts e="T841" id="Seg_4" n="HIAT:w" s="T840">Vot</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_7" n="HIAT:w" s="T841">lisa</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_10" n="HIAT:w" s="T842">dʼijegən</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T844" id="Seg_13" n="HIAT:w" s="T843">mĭnleʔbə</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T848" id="Seg_17" n="HIAT:u" s="T844">
                  <ts e="T845" id="Seg_19" n="HIAT:w" s="T844">Diʔnə</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_22" n="HIAT:w" s="T845">bar</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_25" n="HIAT:w" s="T846">menzeŋdə</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_27" n="HIAT:ip">(</nts>
                  <ts e="T848" id="Seg_29" n="HIAT:w" s="T847">nuʔməluʔpibəʔ</ts>
                  <nts id="Seg_30" n="HIAT:ip">)</nts>
                  <nts id="Seg_31" n="HIAT:ip">.</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T857" id="Seg_34" n="HIAT:u" s="T848">
                  <ts e="T849" id="Seg_36" n="HIAT:w" s="T848">Dĭ</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T850" id="Seg_39" n="HIAT:w" s="T849">bar</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T851" id="Seg_42" n="HIAT:w" s="T850">nuʔməleʔbə</ts>
                  <nts id="Seg_43" n="HIAT:ip">,</nts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_46" n="HIAT:w" s="T851">a</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_48" n="HIAT:ip">(</nts>
                  <ts e="T853" id="Seg_50" n="HIAT:w" s="T852">men=</ts>
                  <nts id="Seg_51" n="HIAT:ip">)</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_54" n="HIAT:w" s="T853">menzeŋ</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T855" id="Seg_57" n="HIAT:w" s="T854">dĭʔnə</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_59" n="HIAT:ip">(</nts>
                  <ts e="T856" id="Seg_61" n="HIAT:w" s="T855">m-</ts>
                  <nts id="Seg_62" n="HIAT:ip">)</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T857" id="Seg_65" n="HIAT:w" s="T856">nuʔməleʔbə</ts>
                  <nts id="Seg_66" n="HIAT:ip">.</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T861" id="Seg_69" n="HIAT:u" s="T857">
                  <ts e="T858" id="Seg_71" n="HIAT:w" s="T857">Dĭ</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_73" n="HIAT:ip">(</nts>
                  <ts e="T859" id="Seg_75" n="HIAT:w" s="T858">mu-</ts>
                  <nts id="Seg_76" n="HIAT:ip">)</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_79" n="HIAT:w" s="T859">nuʔməbi</ts>
                  <nts id="Seg_80" n="HIAT:ip">,</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T861" id="Seg_83" n="HIAT:w" s="T860">nuʔməbi</ts>
                  <nts id="Seg_84" n="HIAT:ip">.</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T863" id="Seg_87" n="HIAT:u" s="T861">
                  <ts e="T862" id="Seg_89" n="HIAT:w" s="T861">Noranə</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T863" id="Seg_92" n="HIAT:w" s="T862">paʔlaːmbi</ts>
                  <nts id="Seg_93" n="HIAT:ip">.</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T868" id="Seg_96" n="HIAT:u" s="T863">
                  <ts e="T864" id="Seg_98" n="HIAT:w" s="T863">Sʼimazaŋdə</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T865" id="Seg_101" n="HIAT:w" s="T864">măndə:</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_103" n="HIAT:ip">"</nts>
                  <ts e="T866" id="Seg_105" n="HIAT:w" s="T865">Šiʔ</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T867" id="Seg_108" n="HIAT:w" s="T866">ĭmbi</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T868" id="Seg_111" n="HIAT:w" s="T867">mandəbilaʔ</ts>
                  <nts id="Seg_112" n="HIAT:ip">"</nts>
                  <nts id="Seg_113" n="HIAT:ip">?</nts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T878" id="Seg_116" n="HIAT:u" s="T868">
                  <nts id="Seg_117" n="HIAT:ip">"</nts>
                  <ts e="T869" id="Seg_119" n="HIAT:w" s="T868">Miʔ</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_121" n="HIAT:ip">(</nts>
                  <ts e="T870" id="Seg_123" n="HIAT:w" s="T869">mandəbilaʔ</ts>
                  <nts id="Seg_124" n="HIAT:ip">)</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T871" id="Seg_127" n="HIAT:w" s="T870">mandəlaʔbibaʔ</ts>
                  <nts id="Seg_128" n="HIAT:ip">,</nts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T872" id="Seg_131" n="HIAT:w" s="T871">štobɨ</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T873" id="Seg_134" n="HIAT:w" s="T872">menzeŋ</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T874" id="Seg_137" n="HIAT:w" s="T873">tănan</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_139" n="HIAT:ip">(</nts>
                  <ts e="T875" id="Seg_141" n="HIAT:w" s="T874">ej</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_144" n="HIAT:w" s="T875">kaʔ-</ts>
                  <nts id="Seg_145" n="HIAT:ip">)</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T877" id="Seg_148" n="HIAT:w" s="T876">ej</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T878" id="Seg_151" n="HIAT:w" s="T877">ambiʔi</ts>
                  <nts id="Seg_152" n="HIAT:ip">"</nts>
                  <nts id="Seg_153" n="HIAT:ip">.</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T883" id="Seg_156" n="HIAT:u" s="T878">
                  <nts id="Seg_157" n="HIAT:ip">"</nts>
                  <ts e="T879" id="Seg_159" n="HIAT:w" s="T878">A</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T880" id="Seg_162" n="HIAT:w" s="T879">šiʔ</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T881" id="Seg_165" n="HIAT:w" s="T880">kuzaŋdə</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T882" id="Seg_168" n="HIAT:w" s="T881">ĭmbi</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T883" id="Seg_171" n="HIAT:w" s="T882">abilaʔ</ts>
                  <nts id="Seg_172" n="HIAT:ip">?</nts>
                  <nts id="Seg_173" n="HIAT:ip">"</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T889" id="Seg_176" n="HIAT:u" s="T883">
                  <nts id="Seg_177" n="HIAT:ip">"</nts>
                  <ts e="T884" id="Seg_179" n="HIAT:w" s="T883">A</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T885" id="Seg_182" n="HIAT:w" s="T884">miʔ</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T886" id="Seg_185" n="HIAT:w" s="T885">nʼilgölaʔbibaʔ</ts>
                  <nts id="Seg_186" n="HIAT:ip">,</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T887" id="Seg_189" n="HIAT:w" s="T886">gijen</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T888" id="Seg_192" n="HIAT:w" s="T887">menzeŋdə</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T889" id="Seg_195" n="HIAT:w" s="T888">kürümnaʔbəʔjə</ts>
                  <nts id="Seg_196" n="HIAT:ip">"</nts>
                  <nts id="Seg_197" n="HIAT:ip">.</nts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T893" id="Seg_200" n="HIAT:u" s="T889">
                  <nts id="Seg_201" n="HIAT:ip">"</nts>
                  <ts e="T890" id="Seg_203" n="HIAT:w" s="T889">A</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T891" id="Seg_206" n="HIAT:w" s="T890">šiʔ</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T892" id="Seg_209" n="HIAT:w" s="T891">ujuʔjə</ts>
                  <nts id="Seg_210" n="HIAT:ip">,</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T893" id="Seg_213" n="HIAT:w" s="T892">ĭmbi</ts>
                  <nts id="Seg_214" n="HIAT:ip">?</nts>
                  <nts id="Seg_215" n="HIAT:ip">"</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T901" id="Seg_218" n="HIAT:u" s="T893">
                  <nts id="Seg_219" n="HIAT:ip">"</nts>
                  <ts e="T894" id="Seg_221" n="HIAT:w" s="T893">A</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T895" id="Seg_224" n="HIAT:w" s="T894">miʔ</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_226" n="HIAT:ip">(</nts>
                  <ts e="T896" id="Seg_228" n="HIAT:w" s="T895">nuʔməleʔbə-</ts>
                  <nts id="Seg_229" n="HIAT:ip">)</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T897" id="Seg_232" n="HIAT:w" s="T896">nuʔməleʔbəbaʔ</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898" id="Seg_235" n="HIAT:w" s="T897">büžü</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T899" id="Seg_238" n="HIAT:w" s="T898">kunzittə</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T900" id="Seg_241" n="HIAT:w" s="T899">lʼisʼitsa</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T901" id="Seg_244" n="HIAT:w" s="T900">noranə</ts>
                  <nts id="Seg_245" n="HIAT:ip">"</nts>
                  <nts id="Seg_246" n="HIAT:ip">.</nts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T904" id="Seg_249" n="HIAT:u" s="T901">
                  <nts id="Seg_250" n="HIAT:ip">"</nts>
                  <ts e="T902" id="Seg_252" n="HIAT:w" s="T901">A</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T903" id="Seg_255" n="HIAT:w" s="T902">tăn</ts>
                  <nts id="Seg_256" n="HIAT:ip">,</nts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T904" id="Seg_259" n="HIAT:w" s="T903">xvostə</ts>
                  <nts id="Seg_260" n="HIAT:ip">?</nts>
                  <nts id="Seg_261" n="HIAT:ip">"</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T906" id="Seg_264" n="HIAT:u" s="T904">
                  <ts e="T905" id="Seg_266" n="HIAT:w" s="T904">-</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_268" n="HIAT:ip">(</nts>
                  <ts e="T906" id="Seg_270" n="HIAT:w" s="T905">surarlaʔbə</ts>
                  <nts id="Seg_271" n="HIAT:ip">)</nts>
                  <nts id="Seg_272" n="HIAT:ip">.</nts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T911" id="Seg_275" n="HIAT:u" s="T906">
                  <nts id="Seg_276" n="HIAT:ip">"</nts>
                  <ts e="T907" id="Seg_278" n="HIAT:w" s="T906">A</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T908" id="Seg_281" n="HIAT:w" s="T907">măn</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909" id="Seg_284" n="HIAT:w" s="T908">onʼiʔ</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T910" id="Seg_287" n="HIAT:w" s="T909">panə</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T911" id="Seg_290" n="HIAT:w" s="T910">toʔnarluʔpiam</ts>
                  <nts id="Seg_291" n="HIAT:ip">.</nts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T913" id="Seg_294" n="HIAT:u" s="T911">
                  <ts e="T912" id="Seg_296" n="HIAT:w" s="T911">Bazoʔ</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T913" id="Seg_299" n="HIAT:w" s="T912">toʔnarluʔpiam</ts>
                  <nts id="Seg_300" n="HIAT:ip">.</nts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T919" id="Seg_303" n="HIAT:u" s="T913">
                  <ts e="T914" id="Seg_305" n="HIAT:w" s="T913">Štobɨ</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T915" id="Seg_308" n="HIAT:w" s="T914">menzeŋ</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T916" id="Seg_311" n="HIAT:w" s="T915">tănan</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T917" id="Seg_314" n="HIAT:w" s="T916">kabarluʔpiʔi</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T918" id="Seg_317" n="HIAT:w" s="T917">i</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T919" id="Seg_320" n="HIAT:w" s="T918">amluʔpiʔi</ts>
                  <nts id="Seg_321" n="HIAT:ip">"</nts>
                  <nts id="Seg_322" n="HIAT:ip">.</nts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T924" id="Seg_325" n="HIAT:u" s="T919">
                  <ts e="T920" id="Seg_327" n="HIAT:w" s="T919">Dĭ</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T921" id="Seg_330" n="HIAT:w" s="T920">bar</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T922" id="Seg_333" n="HIAT:w" s="T921">dĭm</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T923" id="Seg_336" n="HIAT:w" s="T922">nʼiʔdə</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T924" id="Seg_339" n="HIAT:w" s="T923">baʔluʔpi</ts>
                  <nts id="Seg_340" n="HIAT:ip">.</nts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T927" id="Seg_343" n="HIAT:u" s="T924">
                  <ts e="T925" id="Seg_345" n="HIAT:w" s="T924">Menzeŋ</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T926" id="Seg_348" n="HIAT:w" s="T925">kabarbiʔi</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T927" id="Seg_351" n="HIAT:w" s="T926">dĭm</ts>
                  <nts id="Seg_352" n="HIAT:ip">.</nts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T931" id="Seg_355" n="HIAT:u" s="T927">
                  <ts e="T928" id="Seg_357" n="HIAT:w" s="T927">I</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T929" id="Seg_360" n="HIAT:w" s="T928">dĭm</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_362" n="HIAT:ip">(</nts>
                  <ts e="T930" id="Seg_364" n="HIAT:w" s="T929">kutla-</ts>
                  <nts id="Seg_365" n="HIAT:ip">)</nts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T931" id="Seg_368" n="HIAT:w" s="T930">ambiʔi</ts>
                  <nts id="Seg_369" n="HIAT:ip">.</nts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T932" id="Seg_372" n="HIAT:u" s="T931">
                  <ts e="T932" id="Seg_374" n="HIAT:w" s="T931">Kabarləj</ts>
                  <nts id="Seg_375" n="HIAT:ip">.</nts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T932" id="Seg_377" n="sc" s="T840">
               <ts e="T841" id="Seg_379" n="e" s="T840">Vot </ts>
               <ts e="T842" id="Seg_381" n="e" s="T841">lisa </ts>
               <ts e="T843" id="Seg_383" n="e" s="T842">dʼijegən </ts>
               <ts e="T844" id="Seg_385" n="e" s="T843">mĭnleʔbə. </ts>
               <ts e="T845" id="Seg_387" n="e" s="T844">Diʔnə </ts>
               <ts e="T846" id="Seg_389" n="e" s="T845">bar </ts>
               <ts e="T847" id="Seg_391" n="e" s="T846">menzeŋdə </ts>
               <ts e="T848" id="Seg_393" n="e" s="T847">(nuʔməluʔpibəʔ). </ts>
               <ts e="T849" id="Seg_395" n="e" s="T848">Dĭ </ts>
               <ts e="T850" id="Seg_397" n="e" s="T849">bar </ts>
               <ts e="T851" id="Seg_399" n="e" s="T850">nuʔməleʔbə, </ts>
               <ts e="T852" id="Seg_401" n="e" s="T851">a </ts>
               <ts e="T853" id="Seg_403" n="e" s="T852">(men=) </ts>
               <ts e="T854" id="Seg_405" n="e" s="T853">menzeŋ </ts>
               <ts e="T855" id="Seg_407" n="e" s="T854">dĭʔnə </ts>
               <ts e="T856" id="Seg_409" n="e" s="T855">(m-) </ts>
               <ts e="T857" id="Seg_411" n="e" s="T856">nuʔməleʔbə. </ts>
               <ts e="T858" id="Seg_413" n="e" s="T857">Dĭ </ts>
               <ts e="T859" id="Seg_415" n="e" s="T858">(mu-) </ts>
               <ts e="T860" id="Seg_417" n="e" s="T859">nuʔməbi, </ts>
               <ts e="T861" id="Seg_419" n="e" s="T860">nuʔməbi. </ts>
               <ts e="T862" id="Seg_421" n="e" s="T861">Noranə </ts>
               <ts e="T863" id="Seg_423" n="e" s="T862">paʔlaːmbi. </ts>
               <ts e="T864" id="Seg_425" n="e" s="T863">Sʼimazaŋdə </ts>
               <ts e="T865" id="Seg_427" n="e" s="T864">măndə: </ts>
               <ts e="T866" id="Seg_429" n="e" s="T865">"Šiʔ </ts>
               <ts e="T867" id="Seg_431" n="e" s="T866">ĭmbi </ts>
               <ts e="T868" id="Seg_433" n="e" s="T867">mandəbilaʔ"? </ts>
               <ts e="T869" id="Seg_435" n="e" s="T868">"Miʔ </ts>
               <ts e="T870" id="Seg_437" n="e" s="T869">(mandəbilaʔ) </ts>
               <ts e="T871" id="Seg_439" n="e" s="T870">mandəlaʔbibaʔ, </ts>
               <ts e="T872" id="Seg_441" n="e" s="T871">štobɨ </ts>
               <ts e="T873" id="Seg_443" n="e" s="T872">menzeŋ </ts>
               <ts e="T874" id="Seg_445" n="e" s="T873">tănan </ts>
               <ts e="T875" id="Seg_447" n="e" s="T874">(ej </ts>
               <ts e="T876" id="Seg_449" n="e" s="T875">kaʔ-) </ts>
               <ts e="T877" id="Seg_451" n="e" s="T876">ej </ts>
               <ts e="T878" id="Seg_453" n="e" s="T877">ambiʔi". </ts>
               <ts e="T879" id="Seg_455" n="e" s="T878">"A </ts>
               <ts e="T880" id="Seg_457" n="e" s="T879">šiʔ </ts>
               <ts e="T881" id="Seg_459" n="e" s="T880">kuzaŋdə </ts>
               <ts e="T882" id="Seg_461" n="e" s="T881">ĭmbi </ts>
               <ts e="T883" id="Seg_463" n="e" s="T882">abilaʔ?" </ts>
               <ts e="T884" id="Seg_465" n="e" s="T883">"A </ts>
               <ts e="T885" id="Seg_467" n="e" s="T884">miʔ </ts>
               <ts e="T886" id="Seg_469" n="e" s="T885">nʼilgölaʔbibaʔ, </ts>
               <ts e="T887" id="Seg_471" n="e" s="T886">gijen </ts>
               <ts e="T888" id="Seg_473" n="e" s="T887">menzeŋdə </ts>
               <ts e="T889" id="Seg_475" n="e" s="T888">kürümnaʔbəʔjə". </ts>
               <ts e="T890" id="Seg_477" n="e" s="T889">"A </ts>
               <ts e="T891" id="Seg_479" n="e" s="T890">šiʔ </ts>
               <ts e="T892" id="Seg_481" n="e" s="T891">ujuʔjə, </ts>
               <ts e="T893" id="Seg_483" n="e" s="T892">ĭmbi?" </ts>
               <ts e="T894" id="Seg_485" n="e" s="T893">"A </ts>
               <ts e="T895" id="Seg_487" n="e" s="T894">miʔ </ts>
               <ts e="T896" id="Seg_489" n="e" s="T895">(nuʔməleʔbə-) </ts>
               <ts e="T897" id="Seg_491" n="e" s="T896">nuʔməleʔbəbaʔ </ts>
               <ts e="T898" id="Seg_493" n="e" s="T897">büžü </ts>
               <ts e="T899" id="Seg_495" n="e" s="T898">kunzittə </ts>
               <ts e="T900" id="Seg_497" n="e" s="T899">lʼisʼitsa </ts>
               <ts e="T901" id="Seg_499" n="e" s="T900">noranə". </ts>
               <ts e="T902" id="Seg_501" n="e" s="T901">"A </ts>
               <ts e="T903" id="Seg_503" n="e" s="T902">tăn, </ts>
               <ts e="T904" id="Seg_505" n="e" s="T903">xvostə?" </ts>
               <ts e="T905" id="Seg_507" n="e" s="T904">- </ts>
               <ts e="T906" id="Seg_509" n="e" s="T905">(surarlaʔbə). </ts>
               <ts e="T907" id="Seg_511" n="e" s="T906">"A </ts>
               <ts e="T908" id="Seg_513" n="e" s="T907">măn </ts>
               <ts e="T909" id="Seg_515" n="e" s="T908">onʼiʔ </ts>
               <ts e="T910" id="Seg_517" n="e" s="T909">panə </ts>
               <ts e="T911" id="Seg_519" n="e" s="T910">toʔnarluʔpiam. </ts>
               <ts e="T912" id="Seg_521" n="e" s="T911">Bazoʔ </ts>
               <ts e="T913" id="Seg_523" n="e" s="T912">toʔnarluʔpiam. </ts>
               <ts e="T914" id="Seg_525" n="e" s="T913">Štobɨ </ts>
               <ts e="T915" id="Seg_527" n="e" s="T914">menzeŋ </ts>
               <ts e="T916" id="Seg_529" n="e" s="T915">tănan </ts>
               <ts e="T917" id="Seg_531" n="e" s="T916">kabarluʔpiʔi </ts>
               <ts e="T918" id="Seg_533" n="e" s="T917">i </ts>
               <ts e="T919" id="Seg_535" n="e" s="T918">amluʔpiʔi". </ts>
               <ts e="T920" id="Seg_537" n="e" s="T919">Dĭ </ts>
               <ts e="T921" id="Seg_539" n="e" s="T920">bar </ts>
               <ts e="T922" id="Seg_541" n="e" s="T921">dĭm </ts>
               <ts e="T923" id="Seg_543" n="e" s="T922">nʼiʔdə </ts>
               <ts e="T924" id="Seg_545" n="e" s="T923">baʔluʔpi. </ts>
               <ts e="T925" id="Seg_547" n="e" s="T924">Menzeŋ </ts>
               <ts e="T926" id="Seg_549" n="e" s="T925">kabarbiʔi </ts>
               <ts e="T927" id="Seg_551" n="e" s="T926">dĭm. </ts>
               <ts e="T928" id="Seg_553" n="e" s="T927">I </ts>
               <ts e="T929" id="Seg_555" n="e" s="T928">dĭm </ts>
               <ts e="T930" id="Seg_557" n="e" s="T929">(kutla-) </ts>
               <ts e="T931" id="Seg_559" n="e" s="T930">ambiʔi. </ts>
               <ts e="T932" id="Seg_561" n="e" s="T931">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T844" id="Seg_562" s="T840">PKZ_196X_FoxAndDogs_flk.001 (001)</ta>
            <ta e="T848" id="Seg_563" s="T844">PKZ_196X_FoxAndDogs_flk.002 (002)</ta>
            <ta e="T857" id="Seg_564" s="T848">PKZ_196X_FoxAndDogs_flk.003 (003)</ta>
            <ta e="T861" id="Seg_565" s="T857">PKZ_196X_FoxAndDogs_flk.004 (004)</ta>
            <ta e="T863" id="Seg_566" s="T861">PKZ_196X_FoxAndDogs_flk.005 (005)</ta>
            <ta e="T868" id="Seg_567" s="T863">PKZ_196X_FoxAndDogs_flk.006 (006)</ta>
            <ta e="T878" id="Seg_568" s="T868">PKZ_196X_FoxAndDogs_flk.007 (008)</ta>
            <ta e="T883" id="Seg_569" s="T878">PKZ_196X_FoxAndDogs_flk.008 (009)</ta>
            <ta e="T889" id="Seg_570" s="T883">PKZ_196X_FoxAndDogs_flk.009 (010)</ta>
            <ta e="T893" id="Seg_571" s="T889">PKZ_196X_FoxAndDogs_flk.010 (011)</ta>
            <ta e="T901" id="Seg_572" s="T893">PKZ_196X_FoxAndDogs_flk.011 (012)</ta>
            <ta e="T904" id="Seg_573" s="T901">PKZ_196X_FoxAndDogs_flk.012 (013.001)</ta>
            <ta e="T906" id="Seg_574" s="T904">PKZ_196X_FoxAndDogs_flk.013 (013.002)</ta>
            <ta e="T911" id="Seg_575" s="T906">PKZ_196X_FoxAndDogs_flk.014 (014)</ta>
            <ta e="T913" id="Seg_576" s="T911">PKZ_196X_FoxAndDogs_flk.015 (015)</ta>
            <ta e="T919" id="Seg_577" s="T913">PKZ_196X_FoxAndDogs_flk.016 (016)</ta>
            <ta e="T924" id="Seg_578" s="T919">PKZ_196X_FoxAndDogs_flk.017 (017)</ta>
            <ta e="T927" id="Seg_579" s="T924">PKZ_196X_FoxAndDogs_flk.018 (018)</ta>
            <ta e="T931" id="Seg_580" s="T927">PKZ_196X_FoxAndDogs_flk.019 (019)</ta>
            <ta e="T932" id="Seg_581" s="T931">PKZ_196X_FoxAndDogs_flk.020 (020)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T844" id="Seg_582" s="T840">Vot lisa dʼijegən mĭnleʔbə. </ta>
            <ta e="T848" id="Seg_583" s="T844">Diʔnə bar menzeŋdə (nuʔməluʔpibəʔ). </ta>
            <ta e="T857" id="Seg_584" s="T848">Dĭ bar nuʔməleʔbə, a (men=) menzeŋ dĭʔnə (m-) nuʔməleʔbə. </ta>
            <ta e="T861" id="Seg_585" s="T857">Dĭ (mu-) nuʔməbi, nuʔməbi. </ta>
            <ta e="T863" id="Seg_586" s="T861">Noranə paʔlaːmbi. </ta>
            <ta e="T868" id="Seg_587" s="T863">Sʼimazaŋdə măndə: "Šiʔ ĭmbi mandəbilaʔ"? </ta>
            <ta e="T878" id="Seg_588" s="T868">"Miʔ (mandəbilaʔ) mandəlaʔbibaʔ, štobɨ menzeŋ tănan (ej kaʔ-) ej ambiʔi". </ta>
            <ta e="T883" id="Seg_589" s="T878">"A šiʔ kuzaŋdə ĭmbi abilaʔ?" </ta>
            <ta e="T889" id="Seg_590" s="T883">"A miʔ nʼilgölaʔbibaʔ, gijen menzeŋdə kürümnaʔbəʔjə". </ta>
            <ta e="T893" id="Seg_591" s="T889">"A šiʔ ujuʔjə, ĭmbi?" </ta>
            <ta e="T901" id="Seg_592" s="T893">"A miʔ (nuʔməleʔbə-) nuʔməleʔbəbaʔ büžü kunzittə lʼisʼitsa noranə". </ta>
            <ta e="T904" id="Seg_593" s="T901">"A tăn, xvostə?" </ta>
            <ta e="T906" id="Seg_594" s="T904">— (surarlaʔbə). </ta>
            <ta e="T911" id="Seg_595" s="T906">"A măn onʼiʔ panə toʔnarluʔpiam. </ta>
            <ta e="T913" id="Seg_596" s="T911">Bazoʔ toʔnarluʔpiam. </ta>
            <ta e="T919" id="Seg_597" s="T913">Štobɨ menzeŋ tănan kabarluʔpiʔi i amluʔpiʔi". </ta>
            <ta e="T924" id="Seg_598" s="T919">Dĭ bar dĭm nʼiʔdə baʔluʔpi. </ta>
            <ta e="T927" id="Seg_599" s="T924">Menzeŋ kabarbiʔi dĭm. </ta>
            <ta e="T931" id="Seg_600" s="T927">I dĭm (kutla-) ambiʔi. </ta>
            <ta e="T932" id="Seg_601" s="T931">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T843" id="Seg_602" s="T842">dʼije-gən</ta>
            <ta e="T844" id="Seg_603" s="T843">mĭn-leʔbə</ta>
            <ta e="T845" id="Seg_604" s="T844">diʔ-nə</ta>
            <ta e="T846" id="Seg_605" s="T845">bar</ta>
            <ta e="T847" id="Seg_606" s="T846">men-zeŋ-də</ta>
            <ta e="T848" id="Seg_607" s="T847">nuʔmə-luʔ-pi-bəʔ</ta>
            <ta e="T849" id="Seg_608" s="T848">dĭ</ta>
            <ta e="T850" id="Seg_609" s="T849">bar</ta>
            <ta e="T851" id="Seg_610" s="T850">nuʔmə-leʔbə</ta>
            <ta e="T852" id="Seg_611" s="T851">a</ta>
            <ta e="T853" id="Seg_612" s="T852">men</ta>
            <ta e="T854" id="Seg_613" s="T853">men-zeŋ</ta>
            <ta e="T855" id="Seg_614" s="T854">dĭʔ-nə</ta>
            <ta e="T857" id="Seg_615" s="T856">nuʔmə-leʔbə</ta>
            <ta e="T858" id="Seg_616" s="T857">dĭ</ta>
            <ta e="T860" id="Seg_617" s="T859">nuʔmə-bi</ta>
            <ta e="T861" id="Seg_618" s="T860">nuʔmə-bi</ta>
            <ta e="T862" id="Seg_619" s="T861">nora-nə</ta>
            <ta e="T863" id="Seg_620" s="T862">paʔ-laːm-bi</ta>
            <ta e="T864" id="Seg_621" s="T863">sʼima-zaŋ-də</ta>
            <ta e="T865" id="Seg_622" s="T864">măn-də</ta>
            <ta e="T866" id="Seg_623" s="T865">šiʔ</ta>
            <ta e="T867" id="Seg_624" s="T866">ĭmbi</ta>
            <ta e="T868" id="Seg_625" s="T867">mandə-bi-laʔ</ta>
            <ta e="T869" id="Seg_626" s="T868">miʔ</ta>
            <ta e="T870" id="Seg_627" s="T869">mandə-bi-laʔ</ta>
            <ta e="T871" id="Seg_628" s="T870">mandə-laʔ-bi-baʔ</ta>
            <ta e="T872" id="Seg_629" s="T871">štobɨ</ta>
            <ta e="T873" id="Seg_630" s="T872">men-zeŋ</ta>
            <ta e="T874" id="Seg_631" s="T873">tănan</ta>
            <ta e="T875" id="Seg_632" s="T874">ej</ta>
            <ta e="T877" id="Seg_633" s="T876">ej</ta>
            <ta e="T878" id="Seg_634" s="T877">am-bi-ʔi</ta>
            <ta e="T879" id="Seg_635" s="T878">a</ta>
            <ta e="T880" id="Seg_636" s="T879">šiʔ</ta>
            <ta e="T881" id="Seg_637" s="T880">ku-zaŋ-də</ta>
            <ta e="T882" id="Seg_638" s="T881">ĭmbi</ta>
            <ta e="T883" id="Seg_639" s="T882">a-bi-laʔ</ta>
            <ta e="T884" id="Seg_640" s="T883">a</ta>
            <ta e="T885" id="Seg_641" s="T884">miʔ</ta>
            <ta e="T886" id="Seg_642" s="T885">nʼilgö-laʔ-bi-baʔ</ta>
            <ta e="T887" id="Seg_643" s="T886">gijen</ta>
            <ta e="T888" id="Seg_644" s="T887">men-zeŋ-də</ta>
            <ta e="T889" id="Seg_645" s="T888">kürüm-naʔbə-ʔjə</ta>
            <ta e="T890" id="Seg_646" s="T889">a</ta>
            <ta e="T891" id="Seg_647" s="T890">šiʔ</ta>
            <ta e="T892" id="Seg_648" s="T891">uju-ʔjə</ta>
            <ta e="T893" id="Seg_649" s="T892">ĭmbi</ta>
            <ta e="T894" id="Seg_650" s="T893">a</ta>
            <ta e="T895" id="Seg_651" s="T894">miʔ</ta>
            <ta e="T897" id="Seg_652" s="T896">nuʔmə-leʔbə-baʔ</ta>
            <ta e="T898" id="Seg_653" s="T897">büžü</ta>
            <ta e="T899" id="Seg_654" s="T898">kun-zittə</ta>
            <ta e="T900" id="Seg_655" s="T899">lʼisʼitsa</ta>
            <ta e="T901" id="Seg_656" s="T900">nora-nə</ta>
            <ta e="T902" id="Seg_657" s="T901">a</ta>
            <ta e="T903" id="Seg_658" s="T902">tăn</ta>
            <ta e="T904" id="Seg_659" s="T903">xvostə</ta>
            <ta e="T906" id="Seg_660" s="T905">surar-laʔbə</ta>
            <ta e="T907" id="Seg_661" s="T906">a</ta>
            <ta e="T908" id="Seg_662" s="T907">măn</ta>
            <ta e="T909" id="Seg_663" s="T908">onʼiʔ</ta>
            <ta e="T910" id="Seg_664" s="T909">pa-nə</ta>
            <ta e="T911" id="Seg_665" s="T910">toʔ-nar-luʔ-pia-m</ta>
            <ta e="T912" id="Seg_666" s="T911">bazoʔ</ta>
            <ta e="T913" id="Seg_667" s="T912">toʔ-nar-luʔ-pia-m</ta>
            <ta e="T914" id="Seg_668" s="T913">štobɨ</ta>
            <ta e="T915" id="Seg_669" s="T914">men-zeŋ</ta>
            <ta e="T916" id="Seg_670" s="T915">tănan</ta>
            <ta e="T917" id="Seg_671" s="T916">kabar-luʔ-pi-ʔi</ta>
            <ta e="T918" id="Seg_672" s="T917">i</ta>
            <ta e="T919" id="Seg_673" s="T918">am-luʔ-pi-ʔi</ta>
            <ta e="T920" id="Seg_674" s="T919">dĭ</ta>
            <ta e="T921" id="Seg_675" s="T920">bar</ta>
            <ta e="T922" id="Seg_676" s="T921">dĭ-m</ta>
            <ta e="T923" id="Seg_677" s="T922">nʼiʔdə</ta>
            <ta e="T924" id="Seg_678" s="T923">baʔ-luʔ-pi</ta>
            <ta e="T925" id="Seg_679" s="T924">men-zeŋ</ta>
            <ta e="T926" id="Seg_680" s="T925">kabar-bi-ʔi</ta>
            <ta e="T927" id="Seg_681" s="T926">dĭ-m</ta>
            <ta e="T928" id="Seg_682" s="T927">i</ta>
            <ta e="T929" id="Seg_683" s="T928">dĭ-m</ta>
            <ta e="T931" id="Seg_684" s="T930">am-bi-ʔi</ta>
            <ta e="T932" id="Seg_685" s="T931">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T843" id="Seg_686" s="T842">dʼije-Kən</ta>
            <ta e="T844" id="Seg_687" s="T843">mĭn-laʔbə</ta>
            <ta e="T845" id="Seg_688" s="T844">dĭ-Tə</ta>
            <ta e="T846" id="Seg_689" s="T845">bar</ta>
            <ta e="T847" id="Seg_690" s="T846">men-zAŋ-də</ta>
            <ta e="T848" id="Seg_691" s="T847">nuʔmə-luʔbdə-bi-%%</ta>
            <ta e="T849" id="Seg_692" s="T848">dĭ</ta>
            <ta e="T850" id="Seg_693" s="T849">bar</ta>
            <ta e="T851" id="Seg_694" s="T850">nuʔmə-laʔbə</ta>
            <ta e="T852" id="Seg_695" s="T851">a</ta>
            <ta e="T853" id="Seg_696" s="T852">men</ta>
            <ta e="T854" id="Seg_697" s="T853">men-zAŋ</ta>
            <ta e="T855" id="Seg_698" s="T854">dĭ-Tə</ta>
            <ta e="T857" id="Seg_699" s="T856">nuʔmə-laʔbə</ta>
            <ta e="T858" id="Seg_700" s="T857">dĭ</ta>
            <ta e="T860" id="Seg_701" s="T859">nuʔmə-bi</ta>
            <ta e="T861" id="Seg_702" s="T860">nuʔmə-bi</ta>
            <ta e="T862" id="Seg_703" s="T861">nora-Tə</ta>
            <ta e="T863" id="Seg_704" s="T862">paʔ-laːm-bi</ta>
            <ta e="T864" id="Seg_705" s="T863">sima-zAŋ-Tə</ta>
            <ta e="T865" id="Seg_706" s="T864">măn-ntə</ta>
            <ta e="T866" id="Seg_707" s="T865">šiʔ</ta>
            <ta e="T867" id="Seg_708" s="T866">ĭmbi</ta>
            <ta e="T868" id="Seg_709" s="T867">măndo-bi-lAʔ</ta>
            <ta e="T869" id="Seg_710" s="T868">miʔ</ta>
            <ta e="T870" id="Seg_711" s="T869">măndo-bi-lAʔ</ta>
            <ta e="T871" id="Seg_712" s="T870">măndo-laʔbə-bi-bAʔ</ta>
            <ta e="T872" id="Seg_713" s="T871">štobɨ</ta>
            <ta e="T873" id="Seg_714" s="T872">men-zAŋ</ta>
            <ta e="T874" id="Seg_715" s="T873">tănan</ta>
            <ta e="T875" id="Seg_716" s="T874">ej</ta>
            <ta e="T877" id="Seg_717" s="T876">ej</ta>
            <ta e="T878" id="Seg_718" s="T877">am-bi-jəʔ</ta>
            <ta e="T879" id="Seg_719" s="T878">a</ta>
            <ta e="T880" id="Seg_720" s="T879">šiʔ</ta>
            <ta e="T881" id="Seg_721" s="T880">ku-zAŋ-l</ta>
            <ta e="T882" id="Seg_722" s="T881">ĭmbi</ta>
            <ta e="T883" id="Seg_723" s="T882">a-bi-lAʔ</ta>
            <ta e="T884" id="Seg_724" s="T883">a</ta>
            <ta e="T885" id="Seg_725" s="T884">miʔ</ta>
            <ta e="T886" id="Seg_726" s="T885">nʼilgö-lAʔ-bi-bAʔ</ta>
            <ta e="T887" id="Seg_727" s="T886">gijen</ta>
            <ta e="T888" id="Seg_728" s="T887">men-zAŋ-də</ta>
            <ta e="T889" id="Seg_729" s="T888">kürüm-laʔbə-jəʔ</ta>
            <ta e="T890" id="Seg_730" s="T889">a</ta>
            <ta e="T891" id="Seg_731" s="T890">šiʔ</ta>
            <ta e="T892" id="Seg_732" s="T891">üjü-jəʔ</ta>
            <ta e="T893" id="Seg_733" s="T892">ĭmbi</ta>
            <ta e="T894" id="Seg_734" s="T893">a</ta>
            <ta e="T895" id="Seg_735" s="T894">miʔ</ta>
            <ta e="T897" id="Seg_736" s="T896">nuʔmə-laʔbə-bAʔ</ta>
            <ta e="T898" id="Seg_737" s="T897">büžü</ta>
            <ta e="T899" id="Seg_738" s="T898">kun-zittə</ta>
            <ta e="T900" id="Seg_739" s="T899">lʼisʼitsa</ta>
            <ta e="T901" id="Seg_740" s="T900">nora-Tə</ta>
            <ta e="T902" id="Seg_741" s="T901">a</ta>
            <ta e="T903" id="Seg_742" s="T902">tăn</ta>
            <ta e="T904" id="Seg_743" s="T903">xvostə</ta>
            <ta e="T906" id="Seg_744" s="T905">surar-laʔbə</ta>
            <ta e="T907" id="Seg_745" s="T906">a</ta>
            <ta e="T908" id="Seg_746" s="T907">măn</ta>
            <ta e="T909" id="Seg_747" s="T908">onʼiʔ</ta>
            <ta e="T910" id="Seg_748" s="T909">pa-Tə</ta>
            <ta e="T911" id="Seg_749" s="T910">toʔbdə-nar-luʔbdə-bi-m</ta>
            <ta e="T912" id="Seg_750" s="T911">bazoʔ</ta>
            <ta e="T913" id="Seg_751" s="T912">toʔbdə-nar-luʔbdə-bi-m</ta>
            <ta e="T914" id="Seg_752" s="T913">štobɨ</ta>
            <ta e="T915" id="Seg_753" s="T914">men-zAŋ</ta>
            <ta e="T916" id="Seg_754" s="T915">tănan</ta>
            <ta e="T917" id="Seg_755" s="T916">kabar-luʔbdə-bi-jəʔ</ta>
            <ta e="T918" id="Seg_756" s="T917">i</ta>
            <ta e="T919" id="Seg_757" s="T918">am-luʔbdə-bi-jəʔ</ta>
            <ta e="T920" id="Seg_758" s="T919">dĭ</ta>
            <ta e="T921" id="Seg_759" s="T920">bar</ta>
            <ta e="T922" id="Seg_760" s="T921">dĭ-m</ta>
            <ta e="T923" id="Seg_761" s="T922">nʼiʔdə</ta>
            <ta e="T924" id="Seg_762" s="T923">baʔbdə-luʔbdə-bi</ta>
            <ta e="T925" id="Seg_763" s="T924">men-zAŋ</ta>
            <ta e="T926" id="Seg_764" s="T925">kabar-bi-jəʔ</ta>
            <ta e="T927" id="Seg_765" s="T926">dĭ-m</ta>
            <ta e="T928" id="Seg_766" s="T927">i</ta>
            <ta e="T929" id="Seg_767" s="T928">dĭ-m</ta>
            <ta e="T931" id="Seg_768" s="T930">am-bi-jəʔ</ta>
            <ta e="T932" id="Seg_769" s="T931">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T843" id="Seg_770" s="T842">forest-LOC</ta>
            <ta e="T844" id="Seg_771" s="T843">go-DUR.[3SG]</ta>
            <ta e="T845" id="Seg_772" s="T844">this-LAT</ta>
            <ta e="T846" id="Seg_773" s="T845">all</ta>
            <ta e="T847" id="Seg_774" s="T846">dog-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T848" id="Seg_775" s="T847">run-MOM-PST-%%</ta>
            <ta e="T849" id="Seg_776" s="T848">this.[NOM.SG]</ta>
            <ta e="T850" id="Seg_777" s="T849">PTCL</ta>
            <ta e="T851" id="Seg_778" s="T850">run-DUR.[3SG]</ta>
            <ta e="T852" id="Seg_779" s="T851">and</ta>
            <ta e="T853" id="Seg_780" s="T852">dog.[NOM.SG]</ta>
            <ta e="T854" id="Seg_781" s="T853">dog-PL</ta>
            <ta e="T855" id="Seg_782" s="T854">this-LAT</ta>
            <ta e="T857" id="Seg_783" s="T856">run-DUR.[3SG]</ta>
            <ta e="T858" id="Seg_784" s="T857">this.[NOM.SG]</ta>
            <ta e="T860" id="Seg_785" s="T859">run-PST.[3SG]</ta>
            <ta e="T861" id="Seg_786" s="T860">run-PST.[3SG]</ta>
            <ta e="T862" id="Seg_787" s="T861">burrow-LAT</ta>
            <ta e="T863" id="Seg_788" s="T862">climb-RES-PST.[3SG]</ta>
            <ta e="T864" id="Seg_789" s="T863">eye-PL-LAT</ta>
            <ta e="T865" id="Seg_790" s="T864">say-IPFVZ.[3SG]</ta>
            <ta e="T866" id="Seg_791" s="T865">you.PL.NOM</ta>
            <ta e="T867" id="Seg_792" s="T866">what.[NOM.SG]</ta>
            <ta e="T868" id="Seg_793" s="T867">look-PST-2PL</ta>
            <ta e="T869" id="Seg_794" s="T868">we.NOM</ta>
            <ta e="T870" id="Seg_795" s="T869">look-PST-2PL</ta>
            <ta e="T871" id="Seg_796" s="T870">look-DUR-PST-1PL</ta>
            <ta e="T872" id="Seg_797" s="T871">so.that</ta>
            <ta e="T873" id="Seg_798" s="T872">dog-PL</ta>
            <ta e="T874" id="Seg_799" s="T873">you.ACC</ta>
            <ta e="T875" id="Seg_800" s="T874">NEG</ta>
            <ta e="T877" id="Seg_801" s="T876">NEG</ta>
            <ta e="T878" id="Seg_802" s="T877">eat-PST-3PL</ta>
            <ta e="T879" id="Seg_803" s="T878">and</ta>
            <ta e="T880" id="Seg_804" s="T879">you.PL.NOM</ta>
            <ta e="T881" id="Seg_805" s="T880">ear-PL-NOM/GEN/ACC.2SG</ta>
            <ta e="T882" id="Seg_806" s="T881">what.[NOM.SG]</ta>
            <ta e="T883" id="Seg_807" s="T882">make-PST-2PL</ta>
            <ta e="T884" id="Seg_808" s="T883">and</ta>
            <ta e="T885" id="Seg_809" s="T884">we.NOM</ta>
            <ta e="T886" id="Seg_810" s="T885">listen-2PL-PST-1PL</ta>
            <ta e="T887" id="Seg_811" s="T886">where</ta>
            <ta e="T888" id="Seg_812" s="T887">dog-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T889" id="Seg_813" s="T888">shout-DUR-3PL</ta>
            <ta e="T890" id="Seg_814" s="T889">and</ta>
            <ta e="T891" id="Seg_815" s="T890">you.PL.NOM</ta>
            <ta e="T892" id="Seg_816" s="T891">foot-PL</ta>
            <ta e="T893" id="Seg_817" s="T892">what.[NOM.SG]</ta>
            <ta e="T894" id="Seg_818" s="T893">and</ta>
            <ta e="T895" id="Seg_819" s="T894">we.NOM</ta>
            <ta e="T897" id="Seg_820" s="T896">run-DUR-1PL</ta>
            <ta e="T898" id="Seg_821" s="T897">soon</ta>
            <ta e="T899" id="Seg_822" s="T898">bring-INF.LAT</ta>
            <ta e="T900" id="Seg_823" s="T899">fox.[NOM.SG]</ta>
            <ta e="T901" id="Seg_824" s="T900">burrow-LAT</ta>
            <ta e="T902" id="Seg_825" s="T901">and</ta>
            <ta e="T903" id="Seg_826" s="T902">you.GEN</ta>
            <ta e="T904" id="Seg_827" s="T903">tail.[NOM.SG]</ta>
            <ta e="T906" id="Seg_828" s="T905">ask-DUR.[3SG]</ta>
            <ta e="T907" id="Seg_829" s="T906">and</ta>
            <ta e="T908" id="Seg_830" s="T907">I.NOM</ta>
            <ta e="T909" id="Seg_831" s="T908">one.[NOM.SG]</ta>
            <ta e="T910" id="Seg_832" s="T909">tree-LAT</ta>
            <ta e="T911" id="Seg_833" s="T910">hit-MULT-MOM-PST-1SG</ta>
            <ta e="T912" id="Seg_834" s="T911">again</ta>
            <ta e="T913" id="Seg_835" s="T912">hit-MULT-MOM-PST-1SG</ta>
            <ta e="T914" id="Seg_836" s="T913">so.that</ta>
            <ta e="T915" id="Seg_837" s="T914">dog-PL</ta>
            <ta e="T916" id="Seg_838" s="T915">you.ACC</ta>
            <ta e="T917" id="Seg_839" s="T916">grab-MOM-PST-3PL</ta>
            <ta e="T918" id="Seg_840" s="T917">and</ta>
            <ta e="T919" id="Seg_841" s="T918">eat-MOM-PST-3PL</ta>
            <ta e="T920" id="Seg_842" s="T919">this.[NOM.SG]</ta>
            <ta e="T921" id="Seg_843" s="T920">all</ta>
            <ta e="T922" id="Seg_844" s="T921">this-ACC</ta>
            <ta e="T923" id="Seg_845" s="T922">outwards</ta>
            <ta e="T924" id="Seg_846" s="T923">throw-MOM-PST.[3SG]</ta>
            <ta e="T925" id="Seg_847" s="T924">dog-PL</ta>
            <ta e="T926" id="Seg_848" s="T925">grab-PST-3PL</ta>
            <ta e="T927" id="Seg_849" s="T926">this-ACC</ta>
            <ta e="T928" id="Seg_850" s="T927">and</ta>
            <ta e="T929" id="Seg_851" s="T928">this-ACC</ta>
            <ta e="T931" id="Seg_852" s="T930">eat-PST-3PL</ta>
            <ta e="T932" id="Seg_853" s="T931">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T843" id="Seg_854" s="T842">лес-LOC</ta>
            <ta e="T844" id="Seg_855" s="T843">идти-DUR.[3SG]</ta>
            <ta e="T845" id="Seg_856" s="T844">этот-LAT</ta>
            <ta e="T846" id="Seg_857" s="T845">весь</ta>
            <ta e="T847" id="Seg_858" s="T846">собака-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T848" id="Seg_859" s="T847">бежать-MOM-PST-%%</ta>
            <ta e="T849" id="Seg_860" s="T848">этот.[NOM.SG]</ta>
            <ta e="T850" id="Seg_861" s="T849">PTCL</ta>
            <ta e="T851" id="Seg_862" s="T850">бежать-DUR.[3SG]</ta>
            <ta e="T852" id="Seg_863" s="T851">а</ta>
            <ta e="T853" id="Seg_864" s="T852">собака.[NOM.SG]</ta>
            <ta e="T854" id="Seg_865" s="T853">собака-PL</ta>
            <ta e="T855" id="Seg_866" s="T854">этот-LAT</ta>
            <ta e="T857" id="Seg_867" s="T856">бежать-DUR.[3SG]</ta>
            <ta e="T858" id="Seg_868" s="T857">этот.[NOM.SG]</ta>
            <ta e="T860" id="Seg_869" s="T859">бежать-PST.[3SG]</ta>
            <ta e="T861" id="Seg_870" s="T860">бежать-PST.[3SG]</ta>
            <ta e="T862" id="Seg_871" s="T861">нора-LAT</ta>
            <ta e="T863" id="Seg_872" s="T862">лезть-RES-PST.[3SG]</ta>
            <ta e="T864" id="Seg_873" s="T863">глаз-PL-LAT</ta>
            <ta e="T865" id="Seg_874" s="T864">сказать-IPFVZ.[3SG]</ta>
            <ta e="T866" id="Seg_875" s="T865">вы.NOM</ta>
            <ta e="T867" id="Seg_876" s="T866">что.[NOM.SG]</ta>
            <ta e="T868" id="Seg_877" s="T867">смотреть-PST-2PL</ta>
            <ta e="T869" id="Seg_878" s="T868">мы.NOM</ta>
            <ta e="T870" id="Seg_879" s="T869">смотреть-PST-2PL</ta>
            <ta e="T871" id="Seg_880" s="T870">смотреть-DUR-PST-1PL</ta>
            <ta e="T872" id="Seg_881" s="T871">чтобы</ta>
            <ta e="T873" id="Seg_882" s="T872">собака-PL</ta>
            <ta e="T874" id="Seg_883" s="T873">ты.ACC</ta>
            <ta e="T875" id="Seg_884" s="T874">NEG</ta>
            <ta e="T877" id="Seg_885" s="T876">NEG</ta>
            <ta e="T878" id="Seg_886" s="T877">съесть-PST-3PL</ta>
            <ta e="T879" id="Seg_887" s="T878">а</ta>
            <ta e="T880" id="Seg_888" s="T879">вы.NOM</ta>
            <ta e="T881" id="Seg_889" s="T880">ухо-PL-NOM/GEN/ACC.2SG</ta>
            <ta e="T882" id="Seg_890" s="T881">что.[NOM.SG]</ta>
            <ta e="T883" id="Seg_891" s="T882">делать-PST-2PL</ta>
            <ta e="T884" id="Seg_892" s="T883">а</ta>
            <ta e="T885" id="Seg_893" s="T884">мы.NOM</ta>
            <ta e="T886" id="Seg_894" s="T885">слушать-2PL-PST-1PL</ta>
            <ta e="T887" id="Seg_895" s="T886">где</ta>
            <ta e="T888" id="Seg_896" s="T887">собака-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T889" id="Seg_897" s="T888">кричать-DUR-3PL</ta>
            <ta e="T890" id="Seg_898" s="T889">а</ta>
            <ta e="T891" id="Seg_899" s="T890">вы.NOM</ta>
            <ta e="T892" id="Seg_900" s="T891">нога-PL</ta>
            <ta e="T893" id="Seg_901" s="T892">что.[NOM.SG]</ta>
            <ta e="T894" id="Seg_902" s="T893">а</ta>
            <ta e="T895" id="Seg_903" s="T894">мы.NOM</ta>
            <ta e="T897" id="Seg_904" s="T896">бежать-DUR-1PL</ta>
            <ta e="T898" id="Seg_905" s="T897">скоро</ta>
            <ta e="T899" id="Seg_906" s="T898">нести-INF.LAT</ta>
            <ta e="T900" id="Seg_907" s="T899">лисица.[NOM.SG]</ta>
            <ta e="T901" id="Seg_908" s="T900">нора-LAT</ta>
            <ta e="T902" id="Seg_909" s="T901">а</ta>
            <ta e="T903" id="Seg_910" s="T902">ты.GEN</ta>
            <ta e="T904" id="Seg_911" s="T903">хвост.[NOM.SG]</ta>
            <ta e="T906" id="Seg_912" s="T905">спросить-DUR.[3SG]</ta>
            <ta e="T907" id="Seg_913" s="T906">а</ta>
            <ta e="T908" id="Seg_914" s="T907">я.NOM</ta>
            <ta e="T909" id="Seg_915" s="T908">один.[NOM.SG]</ta>
            <ta e="T910" id="Seg_916" s="T909">дерево-LAT</ta>
            <ta e="T911" id="Seg_917" s="T910">ударить-MULT-MOM-PST-1SG</ta>
            <ta e="T912" id="Seg_918" s="T911">опять</ta>
            <ta e="T913" id="Seg_919" s="T912">ударить-MULT-MOM-PST-1SG</ta>
            <ta e="T914" id="Seg_920" s="T913">чтобы</ta>
            <ta e="T915" id="Seg_921" s="T914">собака-PL</ta>
            <ta e="T916" id="Seg_922" s="T915">ты.ACC</ta>
            <ta e="T917" id="Seg_923" s="T916">хватать-MOM-PST-3PL</ta>
            <ta e="T918" id="Seg_924" s="T917">и</ta>
            <ta e="T919" id="Seg_925" s="T918">съесть-MOM-PST-3PL</ta>
            <ta e="T920" id="Seg_926" s="T919">этот.[NOM.SG]</ta>
            <ta e="T921" id="Seg_927" s="T920">весь</ta>
            <ta e="T922" id="Seg_928" s="T921">этот-ACC</ta>
            <ta e="T923" id="Seg_929" s="T922">наружу</ta>
            <ta e="T924" id="Seg_930" s="T923">бросить-MOM-PST.[3SG]</ta>
            <ta e="T925" id="Seg_931" s="T924">собака-PL</ta>
            <ta e="T926" id="Seg_932" s="T925">хватать-PST-3PL</ta>
            <ta e="T927" id="Seg_933" s="T926">этот-ACC</ta>
            <ta e="T928" id="Seg_934" s="T927">и</ta>
            <ta e="T929" id="Seg_935" s="T928">этот-ACC</ta>
            <ta e="T931" id="Seg_936" s="T930">съесть-PST-3PL</ta>
            <ta e="T932" id="Seg_937" s="T931">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T843" id="Seg_938" s="T842">n-n:case</ta>
            <ta e="T844" id="Seg_939" s="T843">v-v&gt;v-v:pn</ta>
            <ta e="T845" id="Seg_940" s="T844">dempro-n:case</ta>
            <ta e="T846" id="Seg_941" s="T845">quant</ta>
            <ta e="T847" id="Seg_942" s="T846">n-n:num-n:case.poss</ta>
            <ta e="T848" id="Seg_943" s="T847">v-v&gt;v-v:tense-%%</ta>
            <ta e="T849" id="Seg_944" s="T848">dempro-n:case</ta>
            <ta e="T850" id="Seg_945" s="T849">ptcl</ta>
            <ta e="T851" id="Seg_946" s="T850">v-v&gt;v-v:pn</ta>
            <ta e="T852" id="Seg_947" s="T851">conj</ta>
            <ta e="T853" id="Seg_948" s="T852">n-n:case</ta>
            <ta e="T854" id="Seg_949" s="T853">n-n:num</ta>
            <ta e="T855" id="Seg_950" s="T854">dempro-n:case</ta>
            <ta e="T857" id="Seg_951" s="T856">v-v&gt;v-v:pn</ta>
            <ta e="T858" id="Seg_952" s="T857">dempro-n:case</ta>
            <ta e="T860" id="Seg_953" s="T859">v-v:tense-v:pn</ta>
            <ta e="T861" id="Seg_954" s="T860">v-v:tense-v:pn</ta>
            <ta e="T862" id="Seg_955" s="T861">n-n:case</ta>
            <ta e="T863" id="Seg_956" s="T862">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T864" id="Seg_957" s="T863">n-n:num-n:case</ta>
            <ta e="T865" id="Seg_958" s="T864">v-v&gt;v-v:pn</ta>
            <ta e="T866" id="Seg_959" s="T865">pers</ta>
            <ta e="T867" id="Seg_960" s="T866">que-n:case</ta>
            <ta e="T868" id="Seg_961" s="T867">v-v:tense-v:pn</ta>
            <ta e="T869" id="Seg_962" s="T868">pers</ta>
            <ta e="T870" id="Seg_963" s="T869">v-v:tense-v:pn</ta>
            <ta e="T871" id="Seg_964" s="T870">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T872" id="Seg_965" s="T871">conj</ta>
            <ta e="T873" id="Seg_966" s="T872">n-n:num</ta>
            <ta e="T874" id="Seg_967" s="T873">pers</ta>
            <ta e="T875" id="Seg_968" s="T874">ptcl</ta>
            <ta e="T877" id="Seg_969" s="T876">ptcl</ta>
            <ta e="T878" id="Seg_970" s="T877">v-v:tense-v:pn</ta>
            <ta e="T879" id="Seg_971" s="T878">conj</ta>
            <ta e="T880" id="Seg_972" s="T879">pers</ta>
            <ta e="T881" id="Seg_973" s="T880">n-n:num-n:case.poss</ta>
            <ta e="T882" id="Seg_974" s="T881">que-n:case</ta>
            <ta e="T883" id="Seg_975" s="T882">v-v:tense-v:pn</ta>
            <ta e="T884" id="Seg_976" s="T883">conj</ta>
            <ta e="T885" id="Seg_977" s="T884">pers</ta>
            <ta e="T886" id="Seg_978" s="T885">v-v:pn-v:tense-v:pn</ta>
            <ta e="T887" id="Seg_979" s="T886">que</ta>
            <ta e="T888" id="Seg_980" s="T887">n-n:num-n:case.poss</ta>
            <ta e="T889" id="Seg_981" s="T888">v-v&gt;v-v:pn</ta>
            <ta e="T890" id="Seg_982" s="T889">conj</ta>
            <ta e="T891" id="Seg_983" s="T890">pers</ta>
            <ta e="T892" id="Seg_984" s="T891">n-n:num</ta>
            <ta e="T893" id="Seg_985" s="T892">que-n:case</ta>
            <ta e="T894" id="Seg_986" s="T893">conj</ta>
            <ta e="T895" id="Seg_987" s="T894">pers</ta>
            <ta e="T897" id="Seg_988" s="T896">v-v&gt;v-v:pn</ta>
            <ta e="T898" id="Seg_989" s="T897">adv</ta>
            <ta e="T899" id="Seg_990" s="T898">v-v:n.fin</ta>
            <ta e="T900" id="Seg_991" s="T899">n-n:case</ta>
            <ta e="T901" id="Seg_992" s="T900">n-n:case</ta>
            <ta e="T902" id="Seg_993" s="T901">conj</ta>
            <ta e="T903" id="Seg_994" s="T902">pers</ta>
            <ta e="T904" id="Seg_995" s="T903">n-n:case</ta>
            <ta e="T906" id="Seg_996" s="T905">v-v&gt;v-v:pn</ta>
            <ta e="T907" id="Seg_997" s="T906">conj</ta>
            <ta e="T908" id="Seg_998" s="T907">pers</ta>
            <ta e="T909" id="Seg_999" s="T908">num-n:case</ta>
            <ta e="T910" id="Seg_1000" s="T909">n-n:case</ta>
            <ta e="T911" id="Seg_1001" s="T910">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T912" id="Seg_1002" s="T911">adv</ta>
            <ta e="T913" id="Seg_1003" s="T912">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T914" id="Seg_1004" s="T913">conj</ta>
            <ta e="T915" id="Seg_1005" s="T914">n-n:num</ta>
            <ta e="T916" id="Seg_1006" s="T915">pers</ta>
            <ta e="T917" id="Seg_1007" s="T916">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T918" id="Seg_1008" s="T917">conj</ta>
            <ta e="T919" id="Seg_1009" s="T918">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T920" id="Seg_1010" s="T919">dempro-n:case</ta>
            <ta e="T921" id="Seg_1011" s="T920">quant</ta>
            <ta e="T922" id="Seg_1012" s="T921">dempro-n:case</ta>
            <ta e="T923" id="Seg_1013" s="T922">adv</ta>
            <ta e="T924" id="Seg_1014" s="T923">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T925" id="Seg_1015" s="T924">n-n:num</ta>
            <ta e="T926" id="Seg_1016" s="T925">v-v:tense-v:pn</ta>
            <ta e="T927" id="Seg_1017" s="T926">dempro-n:case</ta>
            <ta e="T928" id="Seg_1018" s="T927">conj</ta>
            <ta e="T929" id="Seg_1019" s="T928">dempro-n:case</ta>
            <ta e="T931" id="Seg_1020" s="T930">v-v:tense-v:pn</ta>
            <ta e="T932" id="Seg_1021" s="T931">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T843" id="Seg_1022" s="T842">n</ta>
            <ta e="T844" id="Seg_1023" s="T843">v</ta>
            <ta e="T845" id="Seg_1024" s="T844">dempro</ta>
            <ta e="T846" id="Seg_1025" s="T845">quant</ta>
            <ta e="T847" id="Seg_1026" s="T846">n</ta>
            <ta e="T848" id="Seg_1027" s="T847">v</ta>
            <ta e="T849" id="Seg_1028" s="T848">dempro</ta>
            <ta e="T850" id="Seg_1029" s="T849">ptcl</ta>
            <ta e="T851" id="Seg_1030" s="T850">v</ta>
            <ta e="T852" id="Seg_1031" s="T851">conj</ta>
            <ta e="T853" id="Seg_1032" s="T852">n</ta>
            <ta e="T854" id="Seg_1033" s="T853">n</ta>
            <ta e="T855" id="Seg_1034" s="T854">dempro</ta>
            <ta e="T857" id="Seg_1035" s="T856">v</ta>
            <ta e="T858" id="Seg_1036" s="T857">dempro</ta>
            <ta e="T860" id="Seg_1037" s="T859">v</ta>
            <ta e="T861" id="Seg_1038" s="T860">v</ta>
            <ta e="T862" id="Seg_1039" s="T861">n</ta>
            <ta e="T863" id="Seg_1040" s="T862">v</ta>
            <ta e="T864" id="Seg_1041" s="T863">n</ta>
            <ta e="T865" id="Seg_1042" s="T864">v</ta>
            <ta e="T866" id="Seg_1043" s="T865">pers</ta>
            <ta e="T867" id="Seg_1044" s="T866">que</ta>
            <ta e="T868" id="Seg_1045" s="T867">v</ta>
            <ta e="T869" id="Seg_1046" s="T868">pers</ta>
            <ta e="T870" id="Seg_1047" s="T869">v</ta>
            <ta e="T871" id="Seg_1048" s="T870">v</ta>
            <ta e="T872" id="Seg_1049" s="T871">conj</ta>
            <ta e="T873" id="Seg_1050" s="T872">n</ta>
            <ta e="T874" id="Seg_1051" s="T873">pers</ta>
            <ta e="T875" id="Seg_1052" s="T874">ptcl</ta>
            <ta e="T877" id="Seg_1053" s="T876">ptcl</ta>
            <ta e="T878" id="Seg_1054" s="T877">v</ta>
            <ta e="T879" id="Seg_1055" s="T878">conj</ta>
            <ta e="T880" id="Seg_1056" s="T879">pers</ta>
            <ta e="T881" id="Seg_1057" s="T880">n</ta>
            <ta e="T882" id="Seg_1058" s="T881">que</ta>
            <ta e="T883" id="Seg_1059" s="T882">v</ta>
            <ta e="T884" id="Seg_1060" s="T883">conj</ta>
            <ta e="T885" id="Seg_1061" s="T884">pers</ta>
            <ta e="T886" id="Seg_1062" s="T885">v</ta>
            <ta e="T887" id="Seg_1063" s="T886">que</ta>
            <ta e="T888" id="Seg_1064" s="T887">n</ta>
            <ta e="T889" id="Seg_1065" s="T888">v</ta>
            <ta e="T890" id="Seg_1066" s="T889">conj</ta>
            <ta e="T891" id="Seg_1067" s="T890">pers</ta>
            <ta e="T892" id="Seg_1068" s="T891">n</ta>
            <ta e="T893" id="Seg_1069" s="T892">que</ta>
            <ta e="T894" id="Seg_1070" s="T893">conj</ta>
            <ta e="T895" id="Seg_1071" s="T894">pers</ta>
            <ta e="T897" id="Seg_1072" s="T896">v</ta>
            <ta e="T898" id="Seg_1073" s="T897">adv</ta>
            <ta e="T899" id="Seg_1074" s="T898">v</ta>
            <ta e="T900" id="Seg_1075" s="T899">n</ta>
            <ta e="T901" id="Seg_1076" s="T900">n</ta>
            <ta e="T902" id="Seg_1077" s="T901">conj</ta>
            <ta e="T903" id="Seg_1078" s="T902">pers</ta>
            <ta e="T904" id="Seg_1079" s="T903">n</ta>
            <ta e="T906" id="Seg_1080" s="T905">v</ta>
            <ta e="T907" id="Seg_1081" s="T906">conj</ta>
            <ta e="T908" id="Seg_1082" s="T907">pers</ta>
            <ta e="T909" id="Seg_1083" s="T908">num</ta>
            <ta e="T910" id="Seg_1084" s="T909">n</ta>
            <ta e="T911" id="Seg_1085" s="T910">v</ta>
            <ta e="T912" id="Seg_1086" s="T911">adv</ta>
            <ta e="T913" id="Seg_1087" s="T912">v</ta>
            <ta e="T914" id="Seg_1088" s="T913">conj</ta>
            <ta e="T915" id="Seg_1089" s="T914">n</ta>
            <ta e="T916" id="Seg_1090" s="T915">pers</ta>
            <ta e="T917" id="Seg_1091" s="T916">v</ta>
            <ta e="T918" id="Seg_1092" s="T917">conj</ta>
            <ta e="T919" id="Seg_1093" s="T918">v</ta>
            <ta e="T920" id="Seg_1094" s="T919">dempro</ta>
            <ta e="T921" id="Seg_1095" s="T920">quant</ta>
            <ta e="T922" id="Seg_1096" s="T921">dempro</ta>
            <ta e="T923" id="Seg_1097" s="T922">adv</ta>
            <ta e="T924" id="Seg_1098" s="T923">v</ta>
            <ta e="T925" id="Seg_1099" s="T924">n</ta>
            <ta e="T926" id="Seg_1100" s="T925">v</ta>
            <ta e="T927" id="Seg_1101" s="T926">dempro</ta>
            <ta e="T928" id="Seg_1102" s="T927">conj</ta>
            <ta e="T929" id="Seg_1103" s="T928">dempro</ta>
            <ta e="T931" id="Seg_1104" s="T930">v</ta>
            <ta e="T932" id="Seg_1105" s="T931">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T843" id="Seg_1106" s="T842">np:L</ta>
            <ta e="T844" id="Seg_1107" s="T843">0.3.h:A</ta>
            <ta e="T845" id="Seg_1108" s="T844">pro:G</ta>
            <ta e="T847" id="Seg_1109" s="T846">np:A</ta>
            <ta e="T849" id="Seg_1110" s="T848">pro.h:A</ta>
            <ta e="T854" id="Seg_1111" s="T853">np:A</ta>
            <ta e="T855" id="Seg_1112" s="T854">pro:G</ta>
            <ta e="T858" id="Seg_1113" s="T857">pro.h:A</ta>
            <ta e="T862" id="Seg_1114" s="T861">np:G</ta>
            <ta e="T863" id="Seg_1115" s="T862">0.3.h:A</ta>
            <ta e="T864" id="Seg_1116" s="T863">np.h:R</ta>
            <ta e="T865" id="Seg_1117" s="T864">0.3.h:A</ta>
            <ta e="T866" id="Seg_1118" s="T865">pro.h:A</ta>
            <ta e="T867" id="Seg_1119" s="T866">pro:Th</ta>
            <ta e="T869" id="Seg_1120" s="T868">pro.h:A</ta>
            <ta e="T873" id="Seg_1121" s="T872">np:A</ta>
            <ta e="T874" id="Seg_1122" s="T873">pro.h:P</ta>
            <ta e="T880" id="Seg_1123" s="T879">pro.h:A</ta>
            <ta e="T881" id="Seg_1124" s="T880">np.h:A</ta>
            <ta e="T883" id="Seg_1125" s="T882">0.2.h:A</ta>
            <ta e="T885" id="Seg_1126" s="T884">pro.h:A</ta>
            <ta e="T888" id="Seg_1127" s="T887">np:A</ta>
            <ta e="T891" id="Seg_1128" s="T890">pro.h:A</ta>
            <ta e="T892" id="Seg_1129" s="T891">np.h:A</ta>
            <ta e="T895" id="Seg_1130" s="T894">pro.h:A</ta>
            <ta e="T898" id="Seg_1131" s="T897">adv:Time</ta>
            <ta e="T900" id="Seg_1132" s="T899">np.h:Th</ta>
            <ta e="T901" id="Seg_1133" s="T900">np:G</ta>
            <ta e="T903" id="Seg_1134" s="T902">pro.h:A</ta>
            <ta e="T904" id="Seg_1135" s="T903">np.h:A</ta>
            <ta e="T906" id="Seg_1136" s="T905">0.3.h:A</ta>
            <ta e="T908" id="Seg_1137" s="T907">pro.h:A</ta>
            <ta e="T910" id="Seg_1138" s="T909">np:L</ta>
            <ta e="T913" id="Seg_1139" s="T912">0.1.h:A</ta>
            <ta e="T915" id="Seg_1140" s="T914">np:A</ta>
            <ta e="T916" id="Seg_1141" s="T915">pro.h:P</ta>
            <ta e="T920" id="Seg_1142" s="T919">pro.h:A</ta>
            <ta e="T922" id="Seg_1143" s="T921">pro.h:Th</ta>
            <ta e="T923" id="Seg_1144" s="T922">adv:G</ta>
            <ta e="T925" id="Seg_1145" s="T924">np:A</ta>
            <ta e="T927" id="Seg_1146" s="T926">pro.h:E</ta>
            <ta e="T929" id="Seg_1147" s="T928">pro.h:P</ta>
            <ta e="T931" id="Seg_1148" s="T930">0.3:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T844" id="Seg_1149" s="T843">v:pred 0.3.h:S</ta>
            <ta e="T847" id="Seg_1150" s="T846">np:S</ta>
            <ta e="T848" id="Seg_1151" s="T847">v:pred</ta>
            <ta e="T849" id="Seg_1152" s="T848">pro.h:S</ta>
            <ta e="T851" id="Seg_1153" s="T850">v:pred</ta>
            <ta e="T854" id="Seg_1154" s="T853">np:S</ta>
            <ta e="T857" id="Seg_1155" s="T856">v:pred</ta>
            <ta e="T858" id="Seg_1156" s="T857">pro.h:S</ta>
            <ta e="T860" id="Seg_1157" s="T859">v:pred</ta>
            <ta e="T861" id="Seg_1158" s="T860">v:pred</ta>
            <ta e="T863" id="Seg_1159" s="T862">v:pred 0.3.h:S</ta>
            <ta e="T865" id="Seg_1160" s="T864">v:pred 0.3.h:S</ta>
            <ta e="T866" id="Seg_1161" s="T865">pro.h:S</ta>
            <ta e="T867" id="Seg_1162" s="T866">pro:O</ta>
            <ta e="T868" id="Seg_1163" s="T867">v:pred</ta>
            <ta e="T869" id="Seg_1164" s="T868">pro.h:S</ta>
            <ta e="T871" id="Seg_1165" s="T870">v:pred</ta>
            <ta e="T873" id="Seg_1166" s="T872">np:S</ta>
            <ta e="T874" id="Seg_1167" s="T873">pro.h:O</ta>
            <ta e="T875" id="Seg_1168" s="T874">ptcl.neg</ta>
            <ta e="T877" id="Seg_1169" s="T876">ptcl.neg</ta>
            <ta e="T878" id="Seg_1170" s="T877">v:pred</ta>
            <ta e="T880" id="Seg_1171" s="T879">pro.h:S</ta>
            <ta e="T881" id="Seg_1172" s="T880">np.h:S</ta>
            <ta e="T883" id="Seg_1173" s="T882">v:pred 0.2.h:S</ta>
            <ta e="T885" id="Seg_1174" s="T884">pro.h:S</ta>
            <ta e="T886" id="Seg_1175" s="T885">v:pred</ta>
            <ta e="T888" id="Seg_1176" s="T887">np:S</ta>
            <ta e="T889" id="Seg_1177" s="T888">v:pred</ta>
            <ta e="T891" id="Seg_1178" s="T890">pro.h:S</ta>
            <ta e="T892" id="Seg_1179" s="T891">np.h:S</ta>
            <ta e="T895" id="Seg_1180" s="T894">pro.h:S</ta>
            <ta e="T897" id="Seg_1181" s="T896">v:pred</ta>
            <ta e="T899" id="Seg_1182" s="T898">s:purp</ta>
            <ta e="T900" id="Seg_1183" s="T899">np.h:O</ta>
            <ta e="T903" id="Seg_1184" s="T902">pro.h:S</ta>
            <ta e="T904" id="Seg_1185" s="T903">np.h:S</ta>
            <ta e="T906" id="Seg_1186" s="T905">v:pred 0.3.h:S</ta>
            <ta e="T908" id="Seg_1187" s="T907">pro.h:S</ta>
            <ta e="T911" id="Seg_1188" s="T910">v:pred</ta>
            <ta e="T913" id="Seg_1189" s="T912">v:pred 0.1.h:S</ta>
            <ta e="T915" id="Seg_1190" s="T914">np:S</ta>
            <ta e="T916" id="Seg_1191" s="T915">pro.h:O</ta>
            <ta e="T917" id="Seg_1192" s="T916">v:pred</ta>
            <ta e="T919" id="Seg_1193" s="T918">v:pred</ta>
            <ta e="T920" id="Seg_1194" s="T919">pro.h:S</ta>
            <ta e="T922" id="Seg_1195" s="T921">pro.h:O</ta>
            <ta e="T924" id="Seg_1196" s="T923">v:pred</ta>
            <ta e="T925" id="Seg_1197" s="T924">np:S</ta>
            <ta e="T926" id="Seg_1198" s="T925">v:pred</ta>
            <ta e="T927" id="Seg_1199" s="T926">pro.h:O</ta>
            <ta e="T929" id="Seg_1200" s="T928">pro.h:O</ta>
            <ta e="T931" id="Seg_1201" s="T930">v:pred 0.3:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T846" id="Seg_1202" s="T845">TURK:disc</ta>
            <ta e="T850" id="Seg_1203" s="T849">TURK:disc</ta>
            <ta e="T852" id="Seg_1204" s="T851">RUS:gram</ta>
            <ta e="T862" id="Seg_1205" s="T861">RUS:cult</ta>
            <ta e="T872" id="Seg_1206" s="T871">RUS:gram</ta>
            <ta e="T879" id="Seg_1207" s="T878">RUS:gram</ta>
            <ta e="T884" id="Seg_1208" s="T883">RUS:gram</ta>
            <ta e="T890" id="Seg_1209" s="T889">RUS:gram</ta>
            <ta e="T894" id="Seg_1210" s="T893">RUS:gram</ta>
            <ta e="T900" id="Seg_1211" s="T899">RUS:cult</ta>
            <ta e="T901" id="Seg_1212" s="T900">RUS:cult</ta>
            <ta e="T902" id="Seg_1213" s="T901">RUS:gram</ta>
            <ta e="T904" id="Seg_1214" s="T903">RUS:core</ta>
            <ta e="T907" id="Seg_1215" s="T906">RUS:gram</ta>
            <ta e="T914" id="Seg_1216" s="T913">RUS:gram</ta>
            <ta e="T918" id="Seg_1217" s="T917">RUS:gram</ta>
            <ta e="T921" id="Seg_1218" s="T920">TURK:disc</ta>
            <ta e="T928" id="Seg_1219" s="T927">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T842" id="Seg_1220" s="T840">RUS:int.alt</ta>
            <ta e="T878" id="Seg_1221" s="T871">RUS:calq</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T844" id="Seg_1222" s="T840">Вот лиса в тайгу идёт.</ta>
            <ta e="T848" id="Seg_1223" s="T844">Все собаки за ней (побежали?).</ta>
            <ta e="T857" id="Seg_1224" s="T848">Она бежит, а собаки за ней бегут.</ta>
            <ta e="T861" id="Seg_1225" s="T857">Она бежала, бежала.</ta>
            <ta e="T863" id="Seg_1226" s="T861">Залезла в нору.</ta>
            <ta e="T865" id="Seg_1227" s="T863">Глазам говорит:</ta>
            <ta e="T868" id="Seg_1228" s="T865">«Вы куда смотрели?»</ta>
            <ta e="T878" id="Seg_1229" s="T868">«Мы смотрели, чтобы собаки тебя не съели».</ta>
            <ta e="T883" id="Seg_1230" s="T878">«А вы, уши, что делали?»</ta>
            <ta e="T889" id="Seg_1231" s="T883">«А мы слушали, где собаки лают».</ta>
            <ta e="T893" id="Seg_1232" s="T889">«А вы, ноги, что [делали]?»</ta>
            <ta e="T901" id="Seg_1233" s="T893">«А мы бежали, чтобы лисицу поскорее в нору привести».</ta>
            <ta e="T904" id="Seg_1234" s="T901">«А ты, хвост?»</ta>
            <ta e="T906" id="Seg_1235" s="T904">— спрашивает.</ta>
            <ta e="T911" id="Seg_1236" s="T906">«А я об одно дерево ударил.</ta>
            <ta e="T913" id="Seg_1237" s="T911">Снова ударил.</ta>
            <ta e="T919" id="Seg_1238" s="T913">Чтобы собаки тебя схватили и съели».</ta>
            <ta e="T924" id="Seg_1239" s="T919">Она его наружу высунула.</ta>
            <ta e="T927" id="Seg_1240" s="T924">Собаки его схватили.</ta>
            <ta e="T931" id="Seg_1241" s="T927">И съели её.</ta>
            <ta e="T932" id="Seg_1242" s="T931">Хватит.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T844" id="Seg_1243" s="T840">See, a fox walks in the taiga.</ta>
            <ta e="T848" id="Seg_1244" s="T844">Dogs run after it (start chasing it).</ta>
            <ta e="T857" id="Seg_1245" s="T848">He runs, and the dogs run after him.</ta>
            <ta e="T861" id="Seg_1246" s="T857">He ran, he ran.</ta>
            <ta e="T863" id="Seg_1247" s="T861">Dived into a burrow.</ta>
            <ta e="T865" id="Seg_1248" s="T863">To his eyes he says:</ta>
            <ta e="T868" id="Seg_1249" s="T865">"What did you look for?"</ta>
            <ta e="T878" id="Seg_1250" s="T868">"We looked that the dogs would not eat you."</ta>
            <ta e="T883" id="Seg_1251" s="T878">"And you, ears, what did you do?"</ta>
            <ta e="T889" id="Seg_1252" s="T883">"And we were listening, where the dogs are barking."</ta>
            <ta e="T893" id="Seg_1253" s="T889">"And you, feet, what [did you do]?</ta>
            <ta e="T901" id="Seg_1254" s="T893">"And we were running to take the fox quickly into the burrow."</ta>
            <ta e="T904" id="Seg_1255" s="T901"> ‎‎"And you, the tail?"</ta>
            <ta e="T906" id="Seg_1256" s="T904">— he asks.</ta>
            <ta e="T911" id="Seg_1257" s="T906">"And I hit one tree.</ta>
            <ta e="T913" id="Seg_1258" s="T911">[And] again I hit.</ta>
            <ta e="T919" id="Seg_1259" s="T913">So that the dogs would catch you and eat you."</ta>
            <ta e="T924" id="Seg_1260" s="T919">He [the fox] threw it out.</ta>
            <ta e="T927" id="Seg_1261" s="T924">The dogs grabbed it [the tail].</ta>
            <ta e="T931" id="Seg_1262" s="T927">And ate him [the fox].</ta>
            <ta e="T932" id="Seg_1263" s="T931">Enough.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T844" id="Seg_1264" s="T840">Seh, ein Fuchs läuft auf der Taiga.</ta>
            <ta e="T848" id="Seg_1265" s="T844">Hunde rennen zu ihm (fangen an, ihn zu jagen).</ta>
            <ta e="T857" id="Seg_1266" s="T848">Er rennt, und die Hunde rennen nach ihm.</ta>
            <ta e="T861" id="Seg_1267" s="T857">Er rannte, er rannte.</ta>
            <ta e="T863" id="Seg_1268" s="T861">Sprang in einen Bau.</ta>
            <ta e="T865" id="Seg_1269" s="T863">Zu seinen Augen sagt er:</ta>
            <ta e="T868" id="Seg_1270" s="T865">„Wonach habt ihr gesucht?“</ta>
            <ta e="T878" id="Seg_1271" s="T868">Wir schauten, dass die Hunde nicht fressen würden.“</ta>
            <ta e="T883" id="Seg_1272" s="T878">„Und ihr, Ohren, was habt ihr getan?“</ta>
            <ta e="T889" id="Seg_1273" s="T883">„Und wir horchten, wo die Hunde bellten.“</ta>
            <ta e="T893" id="Seg_1274" s="T889">„Und ihr, Füße, was [habt ihr getan]?“</ta>
            <ta e="T901" id="Seg_1275" s="T893">„Und wir liefen, um den Fuchs schnell in den Bau zu bringen.“</ta>
            <ta e="T904" id="Seg_1276" s="T901">„Und du, der Schwanz?“</ta>
            <ta e="T906" id="Seg_1277" s="T904">— fragt er.</ta>
            <ta e="T911" id="Seg_1278" s="T906">„Und ich schlug gegen einen Baum.</ta>
            <ta e="T913" id="Seg_1279" s="T911">[Und] wieder schlug ich.</ta>
            <ta e="T919" id="Seg_1280" s="T913">Damit die Hunde dich fangen würden und fressen.“</ta>
            <ta e="T924" id="Seg_1281" s="T919">Er [der Fuchs] warf ihn hinaus.</ta>
            <ta e="T927" id="Seg_1282" s="T924">Die Hunde fassten ihn [den Schwanz].</ta>
            <ta e="T931" id="Seg_1283" s="T927">Und fraßen ihn [den Fuchs].</ta>
            <ta e="T932" id="Seg_1284" s="T931">Genug.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T844" id="Seg_1285" s="T840">[GVY:] This is the second half of Alexei Tolstoy's tale "A fox and a thrush" ("Лиса и дрозд").</ta>
            <ta e="T848" id="Seg_1286" s="T844">[KlT:] Wrong verb form - 1PL because of preparations (???), unjustified POSS.3SG. [GVY:] POSS.3SG may be explained by the missing part of the text: these were [hunters'] dogs. The last form remains unclear; the ending sounds rather like -biʔ than like -beʔ. </ta>
            <ta e="T878" id="Seg_1287" s="T868">[GVY:] mandəbilaʔ… -pəbaʔ</ta>
            <ta e="T889" id="Seg_1288" s="T883">[GVY:] kurimnaʔbəʔjə</ta>
            <ta e="T911" id="Seg_1289" s="T906">[GVY:] Seems to have pronounced "toʔnarluʔpliam' twice.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T927" />
            <conversion-tli id="T928" />
            <conversion-tli id="T929" />
            <conversion-tli id="T930" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
