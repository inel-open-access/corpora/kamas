<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID8AC1959B-E02A-9735-0344-E5E3A850347C">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_Princess_flk.wav" />
         <referenced-file url="PKZ_196X_Princess_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_Princess_flk\PKZ_196X_Princess_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">378</ud-information>
            <ud-information attribute-name="# HIAT:w">245</ud-information>
            <ud-information attribute-name="# e">244</ud-information>
            <ud-information attribute-name="# HIAT:u">58</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T89" time="0.008" type="appl" />
         <tli id="T90" time="0.94" type="appl" />
         <tli id="T91" time="1.871" type="appl" />
         <tli id="T92" time="2.802" type="appl" />
         <tli id="T93" time="3.8599396132071" />
         <tli id="T94" time="5.315" type="appl" />
         <tli id="T95" time="6.335" type="appl" />
         <tli id="T96" time="7.074" type="appl" />
         <tli id="T97" time="7.814" type="appl" />
         <tli id="T98" time="8.37" type="appl" />
         <tli id="T99" time="8.926" type="appl" />
         <tli id="T100" time="9.481" type="appl" />
         <tli id="T101" time="10.299838864257287" />
         <tli id="T102" time="10.659" type="appl" />
         <tli id="T103" time="11.079" type="appl" />
         <tli id="T104" time="11.5" type="appl" />
         <tli id="T105" time="11.92" type="appl" />
         <tli id="T106" time="12.341" type="appl" />
         <tli id="T107" time="13.089" type="appl" />
         <tli id="T108" time="13.74" type="appl" />
         <tli id="T109" time="14.392" type="appl" />
         <tli id="T110" time="15.043" type="appl" />
         <tli id="T111" time="15.694" type="appl" />
         <tli id="T112" time="16.345" type="appl" />
         <tli id="T113" time="17.085" type="appl" />
         <tli id="T114" time="17.8" type="appl" />
         <tli id="T115" time="18.362" type="appl" />
         <tli id="T116" time="23.419633611738416" />
         <tli id="T117" time="24.306" type="appl" />
         <tli id="T118" time="25.149" type="appl" />
         <tli id="T119" time="25.992" type="appl" />
         <tli id="T120" time="26.971" type="appl" />
         <tli id="T121" time="27.95" type="appl" />
         <tli id="T122" time="28.929" type="appl" />
         <tli id="T123" time="29.782" type="appl" />
         <tli id="T124" time="30.377" type="appl" />
         <tli id="T125" time="30.972" type="appl" />
         <tli id="T126" time="31.567" type="appl" />
         <tli id="T127" time="32.162" type="appl" />
         <tli id="T128" time="32.646" type="appl" />
         <tli id="T129" time="33.088" type="appl" />
         <tli id="T130" time="33.531" type="appl" />
         <tli id="T131" time="33.973" type="appl" />
         <tli id="T132" time="34.415" type="appl" />
         <tli id="T133" time="35.863" type="appl" />
         <tli id="T134" time="36.917" type="appl" />
         <tli id="T135" time="38.122" type="appl" />
         <tli id="T136" time="39.245" type="appl" />
         <tli id="T137" time="40.221" type="appl" />
         <tli id="T138" time="41.183" type="appl" />
         <tli id="T139" time="42.144" type="appl" />
         <tli id="T140" time="43.092" type="appl" />
         <tli id="T141" time="44.018" type="appl" />
         <tli id="T142" time="44.728" type="appl" />
         <tli id="T143" time="45.426" type="appl" />
         <tli id="T144" time="46.124" type="appl" />
         <tli id="T145" time="47.26592721526484" />
         <tli id="T146" time="48.087" type="appl" />
         <tli id="T147" time="49.032" type="appl" />
         <tli id="T148" time="49.976" type="appl" />
         <tli id="T149" time="51.03253495526831" />
         <tli id="T150" time="51.703" type="appl" />
         <tli id="T151" time="52.339" type="appl" />
         <tli id="T152" time="52.974" type="appl" />
         <tli id="T153" time="53.61" type="appl" />
         <tli id="T154" time="54.3658161411121" />
         <tli id="T155" time="55.228" type="appl" />
         <tli id="T156" time="56.104" type="appl" />
         <tli id="T157" time="56.979" type="appl" />
         <tli id="T158" time="58.335" type="appl" />
         <tli id="T159" time="59.691" type="appl" />
         <tli id="T160" time="60.492" type="appl" />
         <tli id="T161" time="61.145" type="appl" />
         <tli id="T162" time="63.00568097481917" />
         <tli id="T163" time="63.856" type="appl" />
         <tli id="T164" time="64.488" type="appl" />
         <tli id="T165" time="65.121" type="appl" />
         <tli id="T166" time="65.753" type="appl" />
         <tli id="T167" time="66.60562465553046" />
         <tli id="T168" time="67.217" type="appl" />
         <tli id="T169" time="67.606" type="appl" />
         <tli id="T170" time="67.995" type="appl" />
         <tli id="T171" time="68.384" type="appl" />
         <tli id="T172" time="69.27224960420548" />
         <tli id="T173" time="70.02" type="appl" />
         <tli id="T174" time="70.626" type="appl" />
         <tli id="T175" time="71.231" type="appl" />
         <tli id="T176" time="71.837" type="appl" />
         <tli id="T177" time="73.02552421946558" />
         <tli id="T178" time="74.064" type="appl" />
         <tli id="T179" time="75.25882261398091" />
         <tli id="T180" time="76.098" type="appl" />
         <tli id="T181" time="76.896" type="appl" />
         <tli id="T182" time="77.349" type="appl" />
         <tli id="T183" time="77.803" type="appl" />
         <tli id="T184" time="78.256" type="appl" />
         <tli id="T185" time="78.709" type="appl" />
         <tli id="T186" time="79.239" type="appl" />
         <tli id="T187" time="79.77" type="appl" />
         <tli id="T188" time="80.6320718855611" />
         <tli id="T189" time="81.24" type="appl" />
         <tli id="T190" time="81.671" type="appl" />
         <tli id="T191" time="82.102" type="appl" />
         <tli id="T192" time="82.532" type="appl" />
         <tli id="T193" time="82.962" type="appl" />
         <tli id="T194" time="83.393" type="appl" />
         <tli id="T195" time="84.887" type="appl" />
         <tli id="T196" time="86.24" type="appl" />
         <tli id="T197" time="87.592" type="appl" />
         <tli id="T198" time="88.314" type="appl" />
         <tli id="T199" time="89.037" type="appl" />
         <tli id="T200" time="89.76" type="appl" />
         <tli id="T201" time="90.72524731629606" />
         <tli id="T202" time="91.036" type="appl" />
         <tli id="T203" time="91.591" type="appl" />
         <tli id="T204" time="92.145" type="appl" />
         <tli id="T205" time="92.7" type="appl" />
         <tli id="T206" time="93.254" type="appl" />
         <tli id="T207" time="93.809" type="appl" />
         <tli id="T208" time="95.02518004603455" />
         <tli id="T209" time="95.786" type="appl" />
         <tli id="T210" time="96.544" type="appl" />
         <tli id="T211" time="98.71178903757777" />
         <tli id="T212" time="99.894" type="appl" />
         <tli id="T213" time="100.866" type="appl" />
         <tli id="T214" time="101.839" type="appl" />
         <tli id="T215" time="103.93170737460915" />
         <tli id="T216" time="105.006" type="appl" />
         <tli id="T217" time="106.084" type="appl" />
         <tli id="T218" time="107.162" type="appl" />
         <tli id="T219" time="108.241" type="appl" />
         <tli id="T220" time="109.384" type="appl" />
         <tli id="T221" time="111.35825785666908" />
         <tli id="T222" time="112.902" type="appl" />
         <tli id="T223" time="114.299" type="appl" />
         <tli id="T224" time="115.696" type="appl" />
         <tli id="T225" time="117.094" type="appl" />
         <tli id="T226" time="118.492" type="appl" />
         <tli id="T227" time="119.889" type="appl" />
         <tli id="T228" time="120.591" type="appl" />
         <tli id="T229" time="121.293" type="appl" />
         <tli id="T230" time="121.995" type="appl" />
         <tli id="T231" time="123.53806730974227" />
         <tli id="T232" time="124.272" type="appl" />
         <tli id="T233" time="125.176" type="appl" />
         <tli id="T234" time="126.08" type="appl" />
         <tli id="T235" time="126.983" type="appl" />
         <tli id="T236" time="127.887" type="appl" />
         <tli id="T237" time="128.791" type="appl" />
         <tli id="T238" time="129.757" type="appl" />
         <tli id="T239" time="130.724" type="appl" />
         <tli id="T240" time="131.69" type="appl" />
         <tli id="T241" time="132.657" type="appl" />
         <tli id="T242" time="133.623" type="appl" />
         <tli id="T243" time="134.59" type="appl" />
         <tli id="T244" time="136.65786205722338" />
         <tli id="T245" time="137.582" type="appl" />
         <tli id="T246" time="138.408" type="appl" />
         <tli id="T247" time="139.234" type="appl" />
         <tli id="T248" time="140.06" type="appl" />
         <tli id="T249" time="140.767" type="appl" />
         <tli id="T250" time="141.475" type="appl" />
         <tli id="T251" time="142.182" type="appl" />
         <tli id="T252" time="142.89" type="appl" />
         <tli id="T253" time="143.597" type="appl" />
         <tli id="T254" time="145.032" type="appl" />
         <tli id="T255" time="145.973" type="appl" />
         <tli id="T256" time="146.914" type="appl" />
         <tli id="T257" time="147.346" type="appl" />
         <tli id="T258" time="147.778" type="appl" />
         <tli id="T259" time="148.21" type="appl" />
         <tli id="T260" time="149.115" type="appl" />
         <tli id="T261" time="150.02" type="appl" />
         <tli id="T262" time="151.25763365121915" />
         <tli id="T263" time="151.937" type="appl" />
         <tli id="T264" time="152.641" type="appl" />
         <tli id="T265" time="153.345" type="appl" />
         <tli id="T266" time="154.049" type="appl" />
         <tli id="T267" time="154.753" type="appl" />
         <tli id="T268" time="155.458" type="appl" />
         <tli id="T269" time="156.162" type="appl" />
         <tli id="T270" time="156.866" type="appl" />
         <tli id="T271" time="157.57" type="appl" />
         <tli id="T272" time="158.274" type="appl" />
         <tli id="T273" time="158.978" type="appl" />
         <tli id="T274" time="160.112" type="appl" />
         <tli id="T275" time="160.939" type="appl" />
         <tli id="T276" time="161.765" type="appl" />
         <tli id="T277" time="162.591" type="appl" />
         <tli id="T278" time="163.418" type="appl" />
         <tli id="T279" time="164.244" type="appl" />
         <tli id="T280" time="165.07" type="appl" />
         <tli id="T281" time="165.897" type="appl" />
         <tli id="T282" time="166.87738928808312" />
         <tli id="T283" time="167.552" type="appl" />
         <tli id="T284" time="168.106" type="appl" />
         <tli id="T285" time="168.66" type="appl" />
         <tli id="T286" time="169.214" type="appl" />
         <tli id="T287" time="169.768" type="appl" />
         <tli id="T288" time="170.322" type="appl" />
         <tli id="T289" time="171.21732139205173" />
         <tli id="T290" time="172.203" type="appl" />
         <tli id="T291" time="173.108" type="appl" />
         <tli id="T292" time="174.012" type="appl" />
         <tli id="T293" time="174.916" type="appl" />
         <tli id="T294" time="175.821" type="appl" />
         <tli id="T295" time="176.725" type="appl" />
         <tli id="T296" time="177.789" type="appl" />
         <tli id="T297" time="178.534" type="appl" />
         <tli id="T298" time="179.4838587329443" />
         <tli id="T299" time="180.45" type="appl" />
         <tli id="T300" time="181.158" type="appl" />
         <tli id="T301" time="181.866" type="appl" />
         <tli id="T302" time="182.575" type="appl" />
         <tli id="T303" time="183.284" type="appl" />
         <tli id="T304" time="183.992" type="appl" />
         <tli id="T305" time="185.293" type="appl" />
         <tli id="T306" time="186.481" type="appl" />
         <tli id="T307" time="187.67" type="appl" />
         <tli id="T308" time="189.65036634976784" />
         <tli id="T309" time="190.704" type="appl" />
         <tli id="T310" time="191.326" type="appl" />
         <tli id="T311" time="191.949" type="appl" />
         <tli id="T312" time="193.18" type="appl" />
         <tli id="T313" time="194.349" type="appl" />
         <tli id="T314" time="195.518" type="appl" />
         <tli id="T315" time="196.687" type="appl" />
         <tli id="T316" time="198.35689680719184" />
         <tli id="T317" time="199.082" type="appl" />
         <tli id="T318" time="199.803" type="appl" />
         <tli id="T319" time="200.525" type="appl" />
         <tli id="T320" time="201.246" type="appl" />
         <tli id="T321" time="201.903" type="appl" />
         <tli id="T322" time="202.559" type="appl" />
         <tli id="T323" time="203.216" type="appl" />
         <tli id="T324" time="203.855" type="appl" />
         <tli id="T325" time="204.495" type="appl" />
         <tli id="T326" time="205.134" type="appl" />
         <tli id="T327" time="205.773" type="appl" />
         <tli id="T328" time="206.412" type="appl" />
         <tli id="T329" time="207.052" type="appl" />
         <tli id="T330" time="207.691" type="appl" />
         <tli id="T331" time="208.485" type="appl" />
         <tli id="T332" time="209.28" type="appl" />
         <tli id="T333" time="210.074" type="appl" />
         <tli id="T334" time="211.07" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T289" start="T288">
            <tli id="T288.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T334" id="Seg_0" n="sc" s="T89">
               <ts e="T93" id="Seg_2" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_4" n="HIAT:w" s="T89">Onʼiʔ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_7" n="HIAT:w" s="T90">kuza</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_10" n="HIAT:w" s="T91">kambi</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_13" n="HIAT:w" s="T92">saməjdəsʼtə</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_17" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_19" n="HIAT:w" s="T93">Šonəga</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_23" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_25" n="HIAT:w" s="T94">Nuʔməleʔbə</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_28" n="HIAT:w" s="T95">volk</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_31" n="HIAT:w" s="T96">dĭʔnə</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_35" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_37" n="HIAT:w" s="T97">Dĭ</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_40" n="HIAT:w" s="T98">xatʼel</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_43" n="HIAT:w" s="T99">dĭm</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_46" n="HIAT:w" s="T100">tʼiʔtəsʼtə</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_50" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_52" n="HIAT:w" s="T101">Dĭ</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_55" n="HIAT:w" s="T102">măndə:</ts>
                  <nts id="Seg_56" n="HIAT:ip">"</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_59" n="HIAT:w" s="T103">Iʔ</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_62" n="HIAT:w" s="T104">dʼiʔtəʔ</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_65" n="HIAT:w" s="T105">măna</ts>
                  <nts id="Seg_66" n="HIAT:ip">!</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_69" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_71" n="HIAT:w" s="T106">Măn</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_73" n="HIAT:ip">(</nts>
                  <ts e="T108" id="Seg_75" n="HIAT:w" s="T107">tăn=</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_78" n="HIAT:w" s="T108">tănziʔ=</ts>
                  <nts id="Seg_79" n="HIAT:ip">)</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_82" n="HIAT:w" s="T109">tăn</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_85" n="HIAT:w" s="T110">elembə</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_88" n="HIAT:w" s="T111">molam</ts>
                  <nts id="Seg_89" n="HIAT:ip">"</nts>
                  <nts id="Seg_90" n="HIAT:ip">.</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_93" n="HIAT:u" s="T112">
                  <ts e="T113" id="Seg_95" n="HIAT:w" s="T112">Kambiʔi</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_98" n="HIAT:w" s="T113">šidegöʔ</ts>
                  <nts id="Seg_99" n="HIAT:ip">.</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_102" n="HIAT:u" s="T114">
                  <ts e="T116" id="Seg_104" n="HIAT:w" s="T114">Dĭgəttə</ts>
                  <nts id="Seg_105" n="HIAT:ip">…</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_108" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_110" n="HIAT:w" s="T116">Dĭgəttə</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_113" n="HIAT:w" s="T117">šonəga</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_115" n="HIAT:ip">(</nts>
                  <ts e="T119" id="Seg_117" n="HIAT:w" s="T118">šidegöʔjəʔ</ts>
                  <nts id="Seg_118" n="HIAT:ip">)</nts>
                  <nts id="Seg_119" n="HIAT:ip">.</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_122" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_124" n="HIAT:w" s="T119">Dĭgəttə</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_127" n="HIAT:w" s="T120">šoška</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_130" n="HIAT:w" s="T121">šonəga</ts>
                  <nts id="Seg_131" n="HIAT:ip">.</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_134" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_136" n="HIAT:w" s="T122">Dĭ</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_138" n="HIAT:ip">(</nts>
                  <ts e="T124" id="Seg_140" n="HIAT:w" s="T123">x-</ts>
                  <nts id="Seg_141" n="HIAT:ip">)</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_144" n="HIAT:w" s="T124">xatʼel</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_147" n="HIAT:w" s="T125">dĭʔnə</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_150" n="HIAT:w" s="T126">dʼiʔtəsʼtə</ts>
                  <nts id="Seg_151" n="HIAT:ip">.</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T132" id="Seg_154" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_156" n="HIAT:w" s="T127">Dĭ</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_159" n="HIAT:w" s="T128">măndə:</ts>
                  <nts id="Seg_160" n="HIAT:ip">"</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_163" n="HIAT:w" s="T129">Iʔ</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_166" n="HIAT:w" s="T130">dʼiʔtəʔ</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_169" n="HIAT:w" s="T131">măna</ts>
                  <nts id="Seg_170" n="HIAT:ip">!</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_173" n="HIAT:u" s="T132">
                  <ts e="T133" id="Seg_175" n="HIAT:w" s="T132">Elezeŋbə</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_178" n="HIAT:w" s="T133">moləbaʔ</ts>
                  <nts id="Seg_179" n="HIAT:ip">!</nts>
                  <nts id="Seg_180" n="HIAT:ip">"</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_183" n="HIAT:u" s="T134">
                  <ts e="T135" id="Seg_185" n="HIAT:w" s="T134">Kambiʔi</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_188" n="HIAT:w" s="T135">nagurgöʔ</ts>
                  <nts id="Seg_189" n="HIAT:ip">.</nts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_192" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_194" n="HIAT:w" s="T136">Dĭgəttə</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_197" n="HIAT:w" s="T137">šonəgaʔi</ts>
                  <nts id="Seg_198" n="HIAT:ip">,</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_201" n="HIAT:w" s="T138">šonəgaʔi</ts>
                  <nts id="Seg_202" n="HIAT:ip">.</nts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T141" id="Seg_205" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_207" n="HIAT:w" s="T139">Lisička</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_210" n="HIAT:w" s="T140">nuʔməleʔbə</ts>
                  <nts id="Seg_211" n="HIAT:ip">.</nts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T145" id="Seg_214" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_216" n="HIAT:w" s="T141">Dĭ</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_219" n="HIAT:w" s="T142">dĭm</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_222" n="HIAT:w" s="T143">xatʼel</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_225" n="HIAT:w" s="T144">dʼiʔtəsʼtə</ts>
                  <nts id="Seg_226" n="HIAT:ip">.</nts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_229" n="HIAT:u" s="T145">
                  <ts e="T146" id="Seg_231" n="HIAT:w" s="T145">Dĭ:</ts>
                  <nts id="Seg_232" n="HIAT:ip">"</nts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_235" n="HIAT:w" s="T146">Iʔ</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_238" n="HIAT:w" s="T147">dʼiʔtə</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_241" n="HIAT:w" s="T148">măna</ts>
                  <nts id="Seg_242" n="HIAT:ip">!</nts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T154" id="Seg_245" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_247" n="HIAT:w" s="T149">Măn</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_250" n="HIAT:w" s="T150">elem</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_253" n="HIAT:w" s="T151">tăn</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_255" n="HIAT:ip">(</nts>
                  <ts e="T153" id="Seg_257" n="HIAT:w" s="T152">molə-</ts>
                  <nts id="Seg_258" n="HIAT:ip">)</nts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_261" n="HIAT:w" s="T153">molam</ts>
                  <nts id="Seg_262" n="HIAT:ip">"</nts>
                  <nts id="Seg_263" n="HIAT:ip">.</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_266" n="HIAT:u" s="T154">
                  <ts e="T155" id="Seg_268" n="HIAT:w" s="T154">Dĭgəttə</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_270" n="HIAT:ip">(</nts>
                  <ts e="T156" id="Seg_272" n="HIAT:w" s="T155">ka-</ts>
                  <nts id="Seg_273" n="HIAT:ip">)</nts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_276" n="HIAT:w" s="T156">kambiʔi</ts>
                  <nts id="Seg_277" n="HIAT:ip">.</nts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T159" id="Seg_280" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_282" n="HIAT:w" s="T157">Šonəgaʔi</ts>
                  <nts id="Seg_283" n="HIAT:ip">,</nts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_286" n="HIAT:w" s="T158">šonəgaʔi</ts>
                  <nts id="Seg_287" n="HIAT:ip">.</nts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T162" id="Seg_290" n="HIAT:u" s="T159">
                  <ts e="T160" id="Seg_292" n="HIAT:w" s="T159">Süjö</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_295" n="HIAT:w" s="T160">nʼergölaʔbə</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_298" n="HIAT:w" s="T161">bar</ts>
                  <nts id="Seg_299" n="HIAT:ip">.</nts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_302" n="HIAT:u" s="T162">
                  <ts e="T163" id="Seg_304" n="HIAT:w" s="T162">Dĭm</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_307" n="HIAT:w" s="T163">xatʼel</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_309" n="HIAT:ip">(</nts>
                  <ts e="T165" id="Seg_311" n="HIAT:w" s="T164">tʼiʔ-</ts>
                  <nts id="Seg_312" n="HIAT:ip">)</nts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_315" n="HIAT:w" s="T165">dʼiʔtəsʼtə</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_317" n="HIAT:ip">(</nts>
                  <ts e="T167" id="Seg_319" n="HIAT:w" s="T166">bar</ts>
                  <nts id="Seg_320" n="HIAT:ip">)</nts>
                  <nts id="Seg_321" n="HIAT:ip">.</nts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T172" id="Seg_324" n="HIAT:u" s="T167">
                  <ts e="T168" id="Seg_326" n="HIAT:w" s="T167">Dĭ</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_329" n="HIAT:w" s="T168">măndə:</ts>
                  <nts id="Seg_330" n="HIAT:ip">"</nts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_333" n="HIAT:w" s="T169">Iʔ</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_336" n="HIAT:w" s="T170">dʼiʔtəʔ</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_339" n="HIAT:w" s="T171">măna</ts>
                  <nts id="Seg_340" n="HIAT:ip">!</nts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T177" id="Seg_343" n="HIAT:u" s="T172">
                  <nts id="Seg_344" n="HIAT:ip">(</nts>
                  <ts e="T173" id="Seg_346" n="HIAT:w" s="T172">Măn=</ts>
                  <nts id="Seg_347" n="HIAT:ip">)</nts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_350" n="HIAT:w" s="T173">Măn</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_353" n="HIAT:w" s="T174">tăn</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_356" n="HIAT:w" s="T175">elem</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_359" n="HIAT:w" s="T176">molam</ts>
                  <nts id="Seg_360" n="HIAT:ip">!</nts>
                  <nts id="Seg_361" n="HIAT:ip">"</nts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T179" id="Seg_364" n="HIAT:u" s="T177">
                  <ts e="T178" id="Seg_366" n="HIAT:w" s="T177">Dĭgəttə</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_369" n="HIAT:w" s="T178">šonəgaʔi</ts>
                  <nts id="Seg_370" n="HIAT:ip">.</nts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_373" n="HIAT:u" s="T179">
                  <nts id="Seg_374" n="HIAT:ip">(</nts>
                  <ts e="T180" id="Seg_376" n="HIAT:w" s="T179">Penzü</ts>
                  <nts id="Seg_377" n="HIAT:ip">)</nts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_380" n="HIAT:w" s="T180">iʔbəlaʔbə</ts>
                  <nts id="Seg_381" n="HIAT:ip">.</nts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T185" id="Seg_384" n="HIAT:u" s="T181">
                  <ts e="T182" id="Seg_386" n="HIAT:w" s="T181">Dĭ</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_389" n="HIAT:w" s="T182">dĭm</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_392" n="HIAT:w" s="T183">xatʼel</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_395" n="HIAT:w" s="T184">dʼiʔtəsʼtə</ts>
                  <nts id="Seg_396" n="HIAT:ip">.</nts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_399" n="HIAT:u" s="T185">
                  <nts id="Seg_400" n="HIAT:ip">"</nts>
                  <ts e="T186" id="Seg_402" n="HIAT:w" s="T185">Iʔ</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_405" n="HIAT:w" s="T186">dʼiʔtəʔ</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_408" n="HIAT:w" s="T187">măna</ts>
                  <nts id="Seg_409" n="HIAT:ip">!</nts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T194" id="Seg_412" n="HIAT:u" s="T188">
                  <nts id="Seg_413" n="HIAT:ip">(</nts>
                  <ts e="T189" id="Seg_415" n="HIAT:w" s="T188">Măn</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_418" n="HIAT:w" s="T189">tn-</ts>
                  <nts id="Seg_419" n="HIAT:ip">)</nts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_422" n="HIAT:w" s="T190">Măn</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_425" n="HIAT:w" s="T191">tăn</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_428" n="HIAT:w" s="T192">elel</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_431" n="HIAT:w" s="T193">molam</ts>
                  <nts id="Seg_432" n="HIAT:ip">"</nts>
                  <nts id="Seg_433" n="HIAT:ip">.</nts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T197" id="Seg_436" n="HIAT:u" s="T194">
                  <ts e="T195" id="Seg_438" n="HIAT:w" s="T194">Dĭgəttə</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_441" n="HIAT:w" s="T195">šobiʔi</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_444" n="HIAT:w" s="T196">măjanə</ts>
                  <nts id="Seg_445" n="HIAT:ip">.</nts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T201" id="Seg_448" n="HIAT:u" s="T197">
                  <ts e="T198" id="Seg_450" n="HIAT:w" s="T197">Dĭn</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_453" n="HIAT:w" s="T198">kubiʔi</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_456" n="HIAT:w" s="T199">ši</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_459" n="HIAT:w" s="T200">dibər</ts>
                  <nts id="Seg_460" n="HIAT:ip">.</nts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T208" id="Seg_463" n="HIAT:u" s="T201">
                  <ts e="T202" id="Seg_465" n="HIAT:w" s="T201">Dĭn</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_468" n="HIAT:w" s="T202">amnolaʔbə</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_470" n="HIAT:ip">(</nts>
                  <ts e="T204" id="Seg_472" n="HIAT:w" s="T203">i</ts>
                  <nts id="Seg_473" n="HIAT:ip">)</nts>
                  <nts id="Seg_474" n="HIAT:ip">,</nts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_477" n="HIAT:w" s="T204">bar</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_480" n="HIAT:w" s="T205">ĭmbi</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_483" n="HIAT:w" s="T206">ige</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_486" n="HIAT:w" s="T207">amzittə</ts>
                  <nts id="Seg_487" n="HIAT:ip">.</nts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T215" id="Seg_490" n="HIAT:u" s="T208">
                  <ts e="T209" id="Seg_492" n="HIAT:w" s="T208">Dĭgəttə</ts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_495" n="HIAT:w" s="T209">măndəʔi</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_498" n="HIAT:w" s="T210">eleʔi:</ts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_500" n="HIAT:ip">"</nts>
                  <ts e="T212" id="Seg_502" n="HIAT:w" s="T211">Miʔ</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_505" n="HIAT:w" s="T212">kuzan</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_508" n="HIAT:w" s="T213">nada</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_511" n="HIAT:w" s="T214">nejzittə</ts>
                  <nts id="Seg_512" n="HIAT:ip">.</nts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T219" id="Seg_515" n="HIAT:u" s="T215">
                  <ts e="T216" id="Seg_517" n="HIAT:w" s="T215">Tsarskɨj</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_519" n="HIAT:ip">(</nts>
                  <ts e="T217" id="Seg_521" n="HIAT:w" s="T216">ko-</ts>
                  <nts id="Seg_522" n="HIAT:ip">)</nts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_525" n="HIAT:w" s="T217">koʔbdo</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_528" n="HIAT:w" s="T218">ilbeʔ</ts>
                  <nts id="Seg_529" n="HIAT:ip">"</nts>
                  <nts id="Seg_530" n="HIAT:ip">.</nts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T221" id="Seg_533" n="HIAT:u" s="T219">
                  <ts e="T220" id="Seg_535" n="HIAT:w" s="T219">Dĭgəttə</ts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_538" n="HIAT:w" s="T220">kambiʔi</ts>
                  <nts id="Seg_539" n="HIAT:ip">.</nts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T227" id="Seg_542" n="HIAT:u" s="T221">
                  <ts e="T222" id="Seg_544" n="HIAT:w" s="T221">Šoška</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_547" n="HIAT:w" s="T222">i</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_550" n="HIAT:w" s="T223">lʼisʼitsa</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_553" n="HIAT:w" s="T224">i</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_556" n="HIAT:w" s="T225">volk</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_559" n="HIAT:w" s="T226">kambiʔi</ts>
                  <nts id="Seg_560" n="HIAT:ip">.</nts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T231" id="Seg_563" n="HIAT:u" s="T227">
                  <nts id="Seg_564" n="HIAT:ip">(</nts>
                  <ts e="T228" id="Seg_566" n="HIAT:w" s="T227">Sʼarlaʔbə</ts>
                  <nts id="Seg_567" n="HIAT:ip">)</nts>
                  <nts id="Seg_568" n="HIAT:ip">,</nts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_571" n="HIAT:w" s="T228">bar</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_574" n="HIAT:w" s="T229">il</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_577" n="HIAT:w" s="T230">šobiʔi</ts>
                  <nts id="Seg_578" n="HIAT:ip">.</nts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T237" id="Seg_581" n="HIAT:u" s="T231">
                  <ts e="T232" id="Seg_583" n="HIAT:w" s="T231">Dĭgəttə</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_586" n="HIAT:w" s="T232">i</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_589" n="HIAT:w" s="T233">koʔbdo</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_591" n="HIAT:ip">(</nts>
                  <ts e="T235" id="Seg_593" n="HIAT:w" s="T234">š-</ts>
                  <nts id="Seg_594" n="HIAT:ip">)</nts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_597" n="HIAT:w" s="T235">nʼiʔdə</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_600" n="HIAT:w" s="T236">šobi</ts>
                  <nts id="Seg_601" n="HIAT:ip">.</nts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T244" id="Seg_604" n="HIAT:u" s="T237">
                  <ts e="T238" id="Seg_606" n="HIAT:w" s="T237">Dĭgəttə</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_609" n="HIAT:w" s="T238">süjö</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_612" n="HIAT:w" s="T239">dĭm</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_615" n="HIAT:w" s="T240">kabarlaʔ</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_618" n="HIAT:w" s="T241">kunnaːmbi</ts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_621" n="HIAT:w" s="T242">dĭ</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_624" n="HIAT:w" s="T243">kuzanə</ts>
                  <nts id="Seg_625" n="HIAT:ip">.</nts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T248" id="Seg_628" n="HIAT:u" s="T244">
                  <ts e="T245" id="Seg_630" n="HIAT:w" s="T244">Dĭgəttə</ts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_632" n="HIAT:ip">(</nts>
                  <ts e="T246" id="Seg_634" n="HIAT:w" s="T245">d-</ts>
                  <nts id="Seg_635" n="HIAT:ip">)</nts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_638" n="HIAT:w" s="T246">amnobiʔi</ts>
                  <nts id="Seg_639" n="HIAT:ip">,</nts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_642" n="HIAT:w" s="T247">amnobiʔi</ts>
                  <nts id="Seg_643" n="HIAT:ip">.</nts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T253" id="Seg_646" n="HIAT:u" s="T248">
                  <ts e="T249" id="Seg_648" n="HIAT:w" s="T248">Dĭgəttə</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_650" n="HIAT:ip">(</nts>
                  <ts e="T250" id="Seg_652" n="HIAT:w" s="T249">dĭ=</ts>
                  <nts id="Seg_653" n="HIAT:ip">)</nts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_656" n="HIAT:w" s="T250">dĭʔnə</ts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_659" n="HIAT:w" s="T251">tibinə</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_662" n="HIAT:w" s="T252">kandəga</ts>
                  <nts id="Seg_663" n="HIAT:ip">.</nts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T256" id="Seg_666" n="HIAT:u" s="T253">
                  <ts e="T254" id="Seg_668" n="HIAT:w" s="T253">Koʔbdo</ts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_671" n="HIAT:w" s="T254">kambi</ts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_674" n="HIAT:w" s="T255">bünə</ts>
                  <nts id="Seg_675" n="HIAT:ip">.</nts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T259" id="Seg_678" n="HIAT:u" s="T256">
                  <ts e="T257" id="Seg_680" n="HIAT:w" s="T256">Dĭn</ts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_683" n="HIAT:w" s="T257">nüke</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_686" n="HIAT:w" s="T258">nulaʔbə</ts>
                  <nts id="Seg_687" n="HIAT:ip">.</nts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T262" id="Seg_690" n="HIAT:u" s="T259">
                  <nts id="Seg_691" n="HIAT:ip">"</nts>
                  <ts e="T260" id="Seg_693" n="HIAT:w" s="T259">Kanžəbəj</ts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_696" n="HIAT:w" s="T260">maːʔnʼi</ts>
                  <nts id="Seg_697" n="HIAT:ip">,</nts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_700" n="HIAT:w" s="T261">abandə</ts>
                  <nts id="Seg_701" n="HIAT:ip">"</nts>
                  <nts id="Seg_702" n="HIAT:ip">.</nts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T273" id="Seg_705" n="HIAT:u" s="T262">
                  <nts id="Seg_706" n="HIAT:ip">"</nts>
                  <ts e="T263" id="Seg_708" n="HIAT:w" s="T262">Măn</ts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_711" n="HIAT:w" s="T263">kallam</ts>
                  <nts id="Seg_712" n="HIAT:ip">,</nts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_715" n="HIAT:w" s="T264">surarlam</ts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_718" n="HIAT:w" s="T265">bostə</ts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_721" n="HIAT:w" s="T266">kuzan</ts>
                  <nts id="Seg_722" n="HIAT:ip">,</nts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_725" n="HIAT:w" s="T267">dĭ</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_728" n="HIAT:w" s="T268">măna</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_731" n="HIAT:w" s="T269">măləj:</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_734" n="HIAT:w" s="T270">kanaʔ</ts>
                  <nts id="Seg_735" n="HIAT:ip">,</nts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_738" n="HIAT:w" s="T271">dak</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_741" n="HIAT:w" s="T272">kallam</ts>
                  <nts id="Seg_742" n="HIAT:ip">"</nts>
                  <nts id="Seg_743" n="HIAT:ip">.</nts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T282" id="Seg_746" n="HIAT:u" s="T273">
                  <ts e="T274" id="Seg_748" n="HIAT:w" s="T273">Dĭgəttə</ts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_751" n="HIAT:w" s="T274">šobi</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_754" n="HIAT:w" s="T275">penzü</ts>
                  <nts id="Seg_755" n="HIAT:ip">,</nts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_757" n="HIAT:ip">(</nts>
                  <ts e="T277" id="Seg_759" n="HIAT:w" s="T276">dĭ</ts>
                  <nts id="Seg_760" n="HIAT:ip">)</nts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_763" n="HIAT:w" s="T277">dĭ</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_766" n="HIAT:w" s="T278">nükem</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_768" n="HIAT:ip">(</nts>
                  <ts e="T280" id="Seg_770" n="HIAT:w" s="T279">tabə-</ts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_773" n="HIAT:w" s="T280">tarbəluʔ-</ts>
                  <nts id="Seg_774" n="HIAT:ip">)</nts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_777" n="HIAT:w" s="T281">talbəluʔpi</ts>
                  <nts id="Seg_778" n="HIAT:ip">.</nts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T295" id="Seg_781" n="HIAT:u" s="T282">
                  <ts e="T283" id="Seg_783" n="HIAT:w" s="T282">A</ts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_786" n="HIAT:w" s="T283">dĭ</ts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_789" n="HIAT:w" s="T284">nüke:</ts>
                  <nts id="Seg_790" n="HIAT:ip">"</nts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_793" n="HIAT:w" s="T285">Kanaʔ</ts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_796" n="HIAT:w" s="T286">abandə</ts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_799" n="HIAT:w" s="T287">da</ts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_801" n="HIAT:ip">(</nts>
                  <ts e="T288.tx.1" id="Seg_803" n="HIAT:w" s="T288">nörbit</ts>
                  <nts id="Seg_804" n="HIAT:ip">)</nts>
                  <ts e="T289" id="Seg_806" n="HIAT:w" s="T288.tx.1">:</ts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_809" n="HIAT:w" s="T289">Dĭn</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_812" n="HIAT:w" s="T290">koʔbdo</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_815" n="HIAT:w" s="T291">kalləj</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_818" n="HIAT:w" s="T292">dĭ</ts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_821" n="HIAT:w" s="T293">kuzanə</ts>
                  <nts id="Seg_822" n="HIAT:ip">,</nts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_825" n="HIAT:w" s="T294">saməjlaʔbə</ts>
                  <nts id="Seg_826" n="HIAT:ip">"</nts>
                  <nts id="Seg_827" n="HIAT:ip">.</nts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T298" id="Seg_830" n="HIAT:u" s="T295">
                  <ts e="T296" id="Seg_832" n="HIAT:w" s="T295">Dĭgəttə</ts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_835" n="HIAT:w" s="T296">dĭ</ts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_838" n="HIAT:w" s="T297">kambi</ts>
                  <nts id="Seg_839" n="HIAT:ip">.</nts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T304" id="Seg_842" n="HIAT:u" s="T298">
                  <ts e="T299" id="Seg_844" n="HIAT:w" s="T298">Abat</ts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_847" n="HIAT:w" s="T299">amnobia</ts>
                  <nts id="Seg_848" n="HIAT:ip">,</nts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_851" n="HIAT:w" s="T300">amnobia</ts>
                  <nts id="Seg_852" n="HIAT:ip">,</nts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_855" n="HIAT:w" s="T301">măndə:</ts>
                  <nts id="Seg_856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_858" n="HIAT:w" s="T302">puskaj</ts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_861" n="HIAT:w" s="T303">kaŋgəj</ts>
                  <nts id="Seg_862" n="HIAT:ip">!</nts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T308" id="Seg_865" n="HIAT:u" s="T304">
                  <ts e="T305" id="Seg_867" n="HIAT:w" s="T304">Dĭgəttə</ts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_869" n="HIAT:ip">(</nts>
                  <ts e="T306" id="Seg_871" n="HIAT:w" s="T305">mĭbi</ts>
                  <nts id="Seg_872" n="HIAT:ip">)</nts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_875" n="HIAT:w" s="T306">dĭʔnə</ts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_878" n="HIAT:w" s="T307">iʔgö</ts>
                  <nts id="Seg_879" n="HIAT:ip">.</nts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T311" id="Seg_882" n="HIAT:u" s="T308">
                  <ts e="T309" id="Seg_884" n="HIAT:w" s="T308">Dĭ</ts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_887" n="HIAT:w" s="T309">šobi</ts>
                  <nts id="Seg_888" n="HIAT:ip">,</nts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_891" n="HIAT:w" s="T310">nörbəbi</ts>
                  <nts id="Seg_892" n="HIAT:ip">.</nts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T316" id="Seg_895" n="HIAT:u" s="T311">
                  <ts e="T312" id="Seg_897" n="HIAT:w" s="T311">Dĭgəttə</ts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_900" n="HIAT:w" s="T312">dĭzeŋ</ts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_903" n="HIAT:w" s="T313">ara</ts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_906" n="HIAT:w" s="T314">bĭʔpiʔi</ts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_909" n="HIAT:w" s="T315">bar</ts>
                  <nts id="Seg_910" n="HIAT:ip">.</nts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T320" id="Seg_913" n="HIAT:u" s="T316">
                  <ts e="T317" id="Seg_915" n="HIAT:w" s="T316">Dĭgəttə</ts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_918" n="HIAT:w" s="T317">abat:</ts>
                  <nts id="Seg_919" n="HIAT:ip">"</nts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_922" n="HIAT:w" s="T318">Šogaʔ</ts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_925" n="HIAT:w" s="T319">miʔnʼibeʔ</ts>
                  <nts id="Seg_926" n="HIAT:ip">!</nts>
                  <nts id="Seg_927" n="HIAT:ip">"</nts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T323" id="Seg_930" n="HIAT:u" s="T320">
                  <ts e="T321" id="Seg_932" n="HIAT:w" s="T320">Dĭzeŋ</ts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_935" n="HIAT:w" s="T321">ej</ts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_938" n="HIAT:w" s="T322">kambiʔi</ts>
                  <nts id="Seg_939" n="HIAT:ip">.</nts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T330" id="Seg_942" n="HIAT:u" s="T323">
                  <ts e="T324" id="Seg_944" n="HIAT:w" s="T323">Dĭgəttə</ts>
                  <nts id="Seg_945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_946" n="HIAT:ip">(</nts>
                  <ts e="T325" id="Seg_948" n="HIAT:w" s="T324">dĭz-</ts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_951" n="HIAT:w" s="T325">dĭʔn-</ts>
                  <nts id="Seg_952" n="HIAT:ip">)</nts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_955" n="HIAT:w" s="T326">dĭzeŋdə</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_958" n="HIAT:w" s="T327">bar</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_961" n="HIAT:w" s="T328">tura</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_964" n="HIAT:w" s="T329">jaʔpi</ts>
                  <nts id="Seg_965" n="HIAT:ip">.</nts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T333" id="Seg_968" n="HIAT:u" s="T330">
                  <ts e="T331" id="Seg_970" n="HIAT:w" s="T330">Bar</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_973" n="HIAT:w" s="T331">ĭmbi</ts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_976" n="HIAT:w" s="T332">mĭbi</ts>
                  <nts id="Seg_977" n="HIAT:ip">.</nts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T334" id="Seg_980" n="HIAT:u" s="T333">
                  <ts e="T334" id="Seg_982" n="HIAT:w" s="T333">Kabarləj</ts>
                  <nts id="Seg_983" n="HIAT:ip">.</nts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T334" id="Seg_985" n="sc" s="T89">
               <ts e="T90" id="Seg_987" n="e" s="T89">Onʼiʔ </ts>
               <ts e="T91" id="Seg_989" n="e" s="T90">kuza </ts>
               <ts e="T92" id="Seg_991" n="e" s="T91">kambi </ts>
               <ts e="T93" id="Seg_993" n="e" s="T92">saməjdəsʼtə. </ts>
               <ts e="T94" id="Seg_995" n="e" s="T93">Šonəga. </ts>
               <ts e="T95" id="Seg_997" n="e" s="T94">Nuʔməleʔbə </ts>
               <ts e="T96" id="Seg_999" n="e" s="T95">volk </ts>
               <ts e="T97" id="Seg_1001" n="e" s="T96">dĭʔnə. </ts>
               <ts e="T98" id="Seg_1003" n="e" s="T97">Dĭ </ts>
               <ts e="T99" id="Seg_1005" n="e" s="T98">xatʼel </ts>
               <ts e="T100" id="Seg_1007" n="e" s="T99">dĭm </ts>
               <ts e="T101" id="Seg_1009" n="e" s="T100">tʼiʔtəsʼtə. </ts>
               <ts e="T102" id="Seg_1011" n="e" s="T101">Dĭ </ts>
               <ts e="T103" id="Seg_1013" n="e" s="T102">măndə:" </ts>
               <ts e="T104" id="Seg_1015" n="e" s="T103">Iʔ </ts>
               <ts e="T105" id="Seg_1017" n="e" s="T104">dʼiʔtəʔ </ts>
               <ts e="T106" id="Seg_1019" n="e" s="T105">măna! </ts>
               <ts e="T107" id="Seg_1021" n="e" s="T106">Măn </ts>
               <ts e="T108" id="Seg_1023" n="e" s="T107">(tăn= </ts>
               <ts e="T109" id="Seg_1025" n="e" s="T108">tănziʔ=) </ts>
               <ts e="T110" id="Seg_1027" n="e" s="T109">tăn </ts>
               <ts e="T111" id="Seg_1029" n="e" s="T110">elembə </ts>
               <ts e="T112" id="Seg_1031" n="e" s="T111">molam". </ts>
               <ts e="T113" id="Seg_1033" n="e" s="T112">Kambiʔi </ts>
               <ts e="T114" id="Seg_1035" n="e" s="T113">šidegöʔ. </ts>
               <ts e="T116" id="Seg_1037" n="e" s="T114">Dĭgəttə… </ts>
               <ts e="T117" id="Seg_1039" n="e" s="T116">Dĭgəttə </ts>
               <ts e="T118" id="Seg_1041" n="e" s="T117">šonəga </ts>
               <ts e="T119" id="Seg_1043" n="e" s="T118">(šidegöʔjəʔ). </ts>
               <ts e="T120" id="Seg_1045" n="e" s="T119">Dĭgəttə </ts>
               <ts e="T121" id="Seg_1047" n="e" s="T120">šoška </ts>
               <ts e="T122" id="Seg_1049" n="e" s="T121">šonəga. </ts>
               <ts e="T123" id="Seg_1051" n="e" s="T122">Dĭ </ts>
               <ts e="T124" id="Seg_1053" n="e" s="T123">(x-) </ts>
               <ts e="T125" id="Seg_1055" n="e" s="T124">xatʼel </ts>
               <ts e="T126" id="Seg_1057" n="e" s="T125">dĭʔnə </ts>
               <ts e="T127" id="Seg_1059" n="e" s="T126">dʼiʔtəsʼtə. </ts>
               <ts e="T128" id="Seg_1061" n="e" s="T127">Dĭ </ts>
               <ts e="T129" id="Seg_1063" n="e" s="T128">măndə:" </ts>
               <ts e="T130" id="Seg_1065" n="e" s="T129">Iʔ </ts>
               <ts e="T131" id="Seg_1067" n="e" s="T130">dʼiʔtəʔ </ts>
               <ts e="T132" id="Seg_1069" n="e" s="T131">măna! </ts>
               <ts e="T133" id="Seg_1071" n="e" s="T132">Elezeŋbə </ts>
               <ts e="T134" id="Seg_1073" n="e" s="T133">moləbaʔ!" </ts>
               <ts e="T135" id="Seg_1075" n="e" s="T134">Kambiʔi </ts>
               <ts e="T136" id="Seg_1077" n="e" s="T135">nagurgöʔ. </ts>
               <ts e="T137" id="Seg_1079" n="e" s="T136">Dĭgəttə </ts>
               <ts e="T138" id="Seg_1081" n="e" s="T137">šonəgaʔi, </ts>
               <ts e="T139" id="Seg_1083" n="e" s="T138">šonəgaʔi. </ts>
               <ts e="T140" id="Seg_1085" n="e" s="T139">Lisička </ts>
               <ts e="T141" id="Seg_1087" n="e" s="T140">nuʔməleʔbə. </ts>
               <ts e="T142" id="Seg_1089" n="e" s="T141">Dĭ </ts>
               <ts e="T143" id="Seg_1091" n="e" s="T142">dĭm </ts>
               <ts e="T144" id="Seg_1093" n="e" s="T143">xatʼel </ts>
               <ts e="T145" id="Seg_1095" n="e" s="T144">dʼiʔtəsʼtə. </ts>
               <ts e="T146" id="Seg_1097" n="e" s="T145">Dĭ:" </ts>
               <ts e="T147" id="Seg_1099" n="e" s="T146">Iʔ </ts>
               <ts e="T148" id="Seg_1101" n="e" s="T147">dʼiʔtə </ts>
               <ts e="T149" id="Seg_1103" n="e" s="T148">măna! </ts>
               <ts e="T150" id="Seg_1105" n="e" s="T149">Măn </ts>
               <ts e="T151" id="Seg_1107" n="e" s="T150">elem </ts>
               <ts e="T152" id="Seg_1109" n="e" s="T151">tăn </ts>
               <ts e="T153" id="Seg_1111" n="e" s="T152">(molə-) </ts>
               <ts e="T154" id="Seg_1113" n="e" s="T153">molam". </ts>
               <ts e="T155" id="Seg_1115" n="e" s="T154">Dĭgəttə </ts>
               <ts e="T156" id="Seg_1117" n="e" s="T155">(ka-) </ts>
               <ts e="T157" id="Seg_1119" n="e" s="T156">kambiʔi. </ts>
               <ts e="T158" id="Seg_1121" n="e" s="T157">Šonəgaʔi, </ts>
               <ts e="T159" id="Seg_1123" n="e" s="T158">šonəgaʔi. </ts>
               <ts e="T160" id="Seg_1125" n="e" s="T159">Süjö </ts>
               <ts e="T161" id="Seg_1127" n="e" s="T160">nʼergölaʔbə </ts>
               <ts e="T162" id="Seg_1129" n="e" s="T161">bar. </ts>
               <ts e="T163" id="Seg_1131" n="e" s="T162">Dĭm </ts>
               <ts e="T164" id="Seg_1133" n="e" s="T163">xatʼel </ts>
               <ts e="T165" id="Seg_1135" n="e" s="T164">(tʼiʔ-) </ts>
               <ts e="T166" id="Seg_1137" n="e" s="T165">dʼiʔtəsʼtə </ts>
               <ts e="T167" id="Seg_1139" n="e" s="T166">(bar). </ts>
               <ts e="T168" id="Seg_1141" n="e" s="T167">Dĭ </ts>
               <ts e="T169" id="Seg_1143" n="e" s="T168">măndə:" </ts>
               <ts e="T170" id="Seg_1145" n="e" s="T169">Iʔ </ts>
               <ts e="T171" id="Seg_1147" n="e" s="T170">dʼiʔtəʔ </ts>
               <ts e="T172" id="Seg_1149" n="e" s="T171">măna! </ts>
               <ts e="T173" id="Seg_1151" n="e" s="T172">(Măn=) </ts>
               <ts e="T174" id="Seg_1153" n="e" s="T173">Măn </ts>
               <ts e="T175" id="Seg_1155" n="e" s="T174">tăn </ts>
               <ts e="T176" id="Seg_1157" n="e" s="T175">elem </ts>
               <ts e="T177" id="Seg_1159" n="e" s="T176">molam!" </ts>
               <ts e="T178" id="Seg_1161" n="e" s="T177">Dĭgəttə </ts>
               <ts e="T179" id="Seg_1163" n="e" s="T178">šonəgaʔi. </ts>
               <ts e="T180" id="Seg_1165" n="e" s="T179">(Penzü) </ts>
               <ts e="T181" id="Seg_1167" n="e" s="T180">iʔbəlaʔbə. </ts>
               <ts e="T182" id="Seg_1169" n="e" s="T181">Dĭ </ts>
               <ts e="T183" id="Seg_1171" n="e" s="T182">dĭm </ts>
               <ts e="T184" id="Seg_1173" n="e" s="T183">xatʼel </ts>
               <ts e="T185" id="Seg_1175" n="e" s="T184">dʼiʔtəsʼtə. </ts>
               <ts e="T186" id="Seg_1177" n="e" s="T185">"Iʔ </ts>
               <ts e="T187" id="Seg_1179" n="e" s="T186">dʼiʔtəʔ </ts>
               <ts e="T188" id="Seg_1181" n="e" s="T187">măna! </ts>
               <ts e="T189" id="Seg_1183" n="e" s="T188">(Măn </ts>
               <ts e="T190" id="Seg_1185" n="e" s="T189">tn-) </ts>
               <ts e="T191" id="Seg_1187" n="e" s="T190">Măn </ts>
               <ts e="T192" id="Seg_1189" n="e" s="T191">tăn </ts>
               <ts e="T193" id="Seg_1191" n="e" s="T192">elel </ts>
               <ts e="T194" id="Seg_1193" n="e" s="T193">molam". </ts>
               <ts e="T195" id="Seg_1195" n="e" s="T194">Dĭgəttə </ts>
               <ts e="T196" id="Seg_1197" n="e" s="T195">šobiʔi </ts>
               <ts e="T197" id="Seg_1199" n="e" s="T196">măjanə. </ts>
               <ts e="T198" id="Seg_1201" n="e" s="T197">Dĭn </ts>
               <ts e="T199" id="Seg_1203" n="e" s="T198">kubiʔi </ts>
               <ts e="T200" id="Seg_1205" n="e" s="T199">ši </ts>
               <ts e="T201" id="Seg_1207" n="e" s="T200">dibər. </ts>
               <ts e="T202" id="Seg_1209" n="e" s="T201">Dĭn </ts>
               <ts e="T203" id="Seg_1211" n="e" s="T202">amnolaʔbə </ts>
               <ts e="T204" id="Seg_1213" n="e" s="T203">(i), </ts>
               <ts e="T205" id="Seg_1215" n="e" s="T204">bar </ts>
               <ts e="T206" id="Seg_1217" n="e" s="T205">ĭmbi </ts>
               <ts e="T207" id="Seg_1219" n="e" s="T206">ige </ts>
               <ts e="T208" id="Seg_1221" n="e" s="T207">amzittə. </ts>
               <ts e="T209" id="Seg_1223" n="e" s="T208">Dĭgəttə </ts>
               <ts e="T210" id="Seg_1225" n="e" s="T209">măndəʔi </ts>
               <ts e="T211" id="Seg_1227" n="e" s="T210">eleʔi: </ts>
               <ts e="T212" id="Seg_1229" n="e" s="T211">"Miʔ </ts>
               <ts e="T213" id="Seg_1231" n="e" s="T212">kuzan </ts>
               <ts e="T214" id="Seg_1233" n="e" s="T213">nada </ts>
               <ts e="T215" id="Seg_1235" n="e" s="T214">nejzittə. </ts>
               <ts e="T216" id="Seg_1237" n="e" s="T215">Tsarskɨj </ts>
               <ts e="T217" id="Seg_1239" n="e" s="T216">(ko-) </ts>
               <ts e="T218" id="Seg_1241" n="e" s="T217">koʔbdo </ts>
               <ts e="T219" id="Seg_1243" n="e" s="T218">ilbeʔ". </ts>
               <ts e="T220" id="Seg_1245" n="e" s="T219">Dĭgəttə </ts>
               <ts e="T221" id="Seg_1247" n="e" s="T220">kambiʔi. </ts>
               <ts e="T222" id="Seg_1249" n="e" s="T221">Šoška </ts>
               <ts e="T223" id="Seg_1251" n="e" s="T222">i </ts>
               <ts e="T224" id="Seg_1253" n="e" s="T223">lʼisʼitsa </ts>
               <ts e="T225" id="Seg_1255" n="e" s="T224">i </ts>
               <ts e="T226" id="Seg_1257" n="e" s="T225">volk </ts>
               <ts e="T227" id="Seg_1259" n="e" s="T226">kambiʔi. </ts>
               <ts e="T228" id="Seg_1261" n="e" s="T227">(Sʼarlaʔbə), </ts>
               <ts e="T229" id="Seg_1263" n="e" s="T228">bar </ts>
               <ts e="T230" id="Seg_1265" n="e" s="T229">il </ts>
               <ts e="T231" id="Seg_1267" n="e" s="T230">šobiʔi. </ts>
               <ts e="T232" id="Seg_1269" n="e" s="T231">Dĭgəttə </ts>
               <ts e="T233" id="Seg_1271" n="e" s="T232">i </ts>
               <ts e="T234" id="Seg_1273" n="e" s="T233">koʔbdo </ts>
               <ts e="T235" id="Seg_1275" n="e" s="T234">(š-) </ts>
               <ts e="T236" id="Seg_1277" n="e" s="T235">nʼiʔdə </ts>
               <ts e="T237" id="Seg_1279" n="e" s="T236">šobi. </ts>
               <ts e="T238" id="Seg_1281" n="e" s="T237">Dĭgəttə </ts>
               <ts e="T239" id="Seg_1283" n="e" s="T238">süjö </ts>
               <ts e="T240" id="Seg_1285" n="e" s="T239">dĭm </ts>
               <ts e="T241" id="Seg_1287" n="e" s="T240">kabarlaʔ </ts>
               <ts e="T242" id="Seg_1289" n="e" s="T241">kunnaːmbi </ts>
               <ts e="T243" id="Seg_1291" n="e" s="T242">dĭ </ts>
               <ts e="T244" id="Seg_1293" n="e" s="T243">kuzanə. </ts>
               <ts e="T245" id="Seg_1295" n="e" s="T244">Dĭgəttə </ts>
               <ts e="T246" id="Seg_1297" n="e" s="T245">(d-) </ts>
               <ts e="T247" id="Seg_1299" n="e" s="T246">amnobiʔi, </ts>
               <ts e="T248" id="Seg_1301" n="e" s="T247">amnobiʔi. </ts>
               <ts e="T249" id="Seg_1303" n="e" s="T248">Dĭgəttə </ts>
               <ts e="T250" id="Seg_1305" n="e" s="T249">(dĭ=) </ts>
               <ts e="T251" id="Seg_1307" n="e" s="T250">dĭʔnə </ts>
               <ts e="T252" id="Seg_1309" n="e" s="T251">tibinə </ts>
               <ts e="T253" id="Seg_1311" n="e" s="T252">kandəga. </ts>
               <ts e="T254" id="Seg_1313" n="e" s="T253">Koʔbdo </ts>
               <ts e="T255" id="Seg_1315" n="e" s="T254">kambi </ts>
               <ts e="T256" id="Seg_1317" n="e" s="T255">bünə. </ts>
               <ts e="T257" id="Seg_1319" n="e" s="T256">Dĭn </ts>
               <ts e="T258" id="Seg_1321" n="e" s="T257">nüke </ts>
               <ts e="T259" id="Seg_1323" n="e" s="T258">nulaʔbə. </ts>
               <ts e="T260" id="Seg_1325" n="e" s="T259">"Kanžəbəj </ts>
               <ts e="T261" id="Seg_1327" n="e" s="T260">maːʔnʼi, </ts>
               <ts e="T262" id="Seg_1329" n="e" s="T261">abandə". </ts>
               <ts e="T263" id="Seg_1331" n="e" s="T262">"Măn </ts>
               <ts e="T264" id="Seg_1333" n="e" s="T263">kallam, </ts>
               <ts e="T265" id="Seg_1335" n="e" s="T264">surarlam </ts>
               <ts e="T266" id="Seg_1337" n="e" s="T265">bostə </ts>
               <ts e="T267" id="Seg_1339" n="e" s="T266">kuzan, </ts>
               <ts e="T268" id="Seg_1341" n="e" s="T267">dĭ </ts>
               <ts e="T269" id="Seg_1343" n="e" s="T268">măna </ts>
               <ts e="T270" id="Seg_1345" n="e" s="T269">măləj: </ts>
               <ts e="T271" id="Seg_1347" n="e" s="T270">kanaʔ, </ts>
               <ts e="T272" id="Seg_1349" n="e" s="T271">dak </ts>
               <ts e="T273" id="Seg_1351" n="e" s="T272">kallam". </ts>
               <ts e="T274" id="Seg_1353" n="e" s="T273">Dĭgəttə </ts>
               <ts e="T275" id="Seg_1355" n="e" s="T274">šobi </ts>
               <ts e="T276" id="Seg_1357" n="e" s="T275">penzü, </ts>
               <ts e="T277" id="Seg_1359" n="e" s="T276">(dĭ) </ts>
               <ts e="T278" id="Seg_1361" n="e" s="T277">dĭ </ts>
               <ts e="T279" id="Seg_1363" n="e" s="T278">nükem </ts>
               <ts e="T280" id="Seg_1365" n="e" s="T279">(tabə- </ts>
               <ts e="T281" id="Seg_1367" n="e" s="T280">tarbəluʔ-) </ts>
               <ts e="T282" id="Seg_1369" n="e" s="T281">talbəluʔpi. </ts>
               <ts e="T283" id="Seg_1371" n="e" s="T282">A </ts>
               <ts e="T284" id="Seg_1373" n="e" s="T283">dĭ </ts>
               <ts e="T285" id="Seg_1375" n="e" s="T284">nüke:" </ts>
               <ts e="T286" id="Seg_1377" n="e" s="T285">Kanaʔ </ts>
               <ts e="T287" id="Seg_1379" n="e" s="T286">abandə </ts>
               <ts e="T288" id="Seg_1381" n="e" s="T287">da </ts>
               <ts e="T289" id="Seg_1383" n="e" s="T288">(nörbit): </ts>
               <ts e="T290" id="Seg_1385" n="e" s="T289">Dĭn </ts>
               <ts e="T291" id="Seg_1387" n="e" s="T290">koʔbdo </ts>
               <ts e="T292" id="Seg_1389" n="e" s="T291">kalləj </ts>
               <ts e="T293" id="Seg_1391" n="e" s="T292">dĭ </ts>
               <ts e="T294" id="Seg_1393" n="e" s="T293">kuzanə, </ts>
               <ts e="T295" id="Seg_1395" n="e" s="T294">saməjlaʔbə". </ts>
               <ts e="T296" id="Seg_1397" n="e" s="T295">Dĭgəttə </ts>
               <ts e="T297" id="Seg_1399" n="e" s="T296">dĭ </ts>
               <ts e="T298" id="Seg_1401" n="e" s="T297">kambi. </ts>
               <ts e="T299" id="Seg_1403" n="e" s="T298">Abat </ts>
               <ts e="T300" id="Seg_1405" n="e" s="T299">amnobia, </ts>
               <ts e="T301" id="Seg_1407" n="e" s="T300">amnobia, </ts>
               <ts e="T302" id="Seg_1409" n="e" s="T301">măndə: </ts>
               <ts e="T303" id="Seg_1411" n="e" s="T302">puskaj </ts>
               <ts e="T304" id="Seg_1413" n="e" s="T303">kaŋgəj! </ts>
               <ts e="T305" id="Seg_1415" n="e" s="T304">Dĭgəttə </ts>
               <ts e="T306" id="Seg_1417" n="e" s="T305">(mĭbi) </ts>
               <ts e="T307" id="Seg_1419" n="e" s="T306">dĭʔnə </ts>
               <ts e="T308" id="Seg_1421" n="e" s="T307">iʔgö. </ts>
               <ts e="T309" id="Seg_1423" n="e" s="T308">Dĭ </ts>
               <ts e="T310" id="Seg_1425" n="e" s="T309">šobi, </ts>
               <ts e="T311" id="Seg_1427" n="e" s="T310">nörbəbi. </ts>
               <ts e="T312" id="Seg_1429" n="e" s="T311">Dĭgəttə </ts>
               <ts e="T313" id="Seg_1431" n="e" s="T312">dĭzeŋ </ts>
               <ts e="T314" id="Seg_1433" n="e" s="T313">ara </ts>
               <ts e="T315" id="Seg_1435" n="e" s="T314">bĭʔpiʔi </ts>
               <ts e="T316" id="Seg_1437" n="e" s="T315">bar. </ts>
               <ts e="T317" id="Seg_1439" n="e" s="T316">Dĭgəttə </ts>
               <ts e="T318" id="Seg_1441" n="e" s="T317">abat:" </ts>
               <ts e="T319" id="Seg_1443" n="e" s="T318">Šogaʔ </ts>
               <ts e="T320" id="Seg_1445" n="e" s="T319">miʔnʼibeʔ!" </ts>
               <ts e="T321" id="Seg_1447" n="e" s="T320">Dĭzeŋ </ts>
               <ts e="T322" id="Seg_1449" n="e" s="T321">ej </ts>
               <ts e="T323" id="Seg_1451" n="e" s="T322">kambiʔi. </ts>
               <ts e="T324" id="Seg_1453" n="e" s="T323">Dĭgəttə </ts>
               <ts e="T325" id="Seg_1455" n="e" s="T324">(dĭz- </ts>
               <ts e="T326" id="Seg_1457" n="e" s="T325">dĭʔn-) </ts>
               <ts e="T327" id="Seg_1459" n="e" s="T326">dĭzeŋdə </ts>
               <ts e="T328" id="Seg_1461" n="e" s="T327">bar </ts>
               <ts e="T329" id="Seg_1463" n="e" s="T328">tura </ts>
               <ts e="T330" id="Seg_1465" n="e" s="T329">jaʔpi. </ts>
               <ts e="T331" id="Seg_1467" n="e" s="T330">Bar </ts>
               <ts e="T332" id="Seg_1469" n="e" s="T331">ĭmbi </ts>
               <ts e="T333" id="Seg_1471" n="e" s="T332">mĭbi. </ts>
               <ts e="T334" id="Seg_1473" n="e" s="T333">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T93" id="Seg_1474" s="T89">PKZ_196X_Princess_flk.001 (001)</ta>
            <ta e="T94" id="Seg_1475" s="T93">PKZ_196X_Princess_flk.002 (002)</ta>
            <ta e="T97" id="Seg_1476" s="T94">PKZ_196X_Princess_flk.003 (003)</ta>
            <ta e="T101" id="Seg_1477" s="T97">PKZ_196X_Princess_flk.004 (004)</ta>
            <ta e="T106" id="Seg_1478" s="T101">PKZ_196X_Princess_flk.005 (005)</ta>
            <ta e="T112" id="Seg_1479" s="T106">PKZ_196X_Princess_flk.006 (006)</ta>
            <ta e="T114" id="Seg_1480" s="T112">PKZ_196X_Princess_flk.007 (007)</ta>
            <ta e="T116" id="Seg_1481" s="T114">PKZ_196X_Princess_flk.008 (008)</ta>
            <ta e="T119" id="Seg_1482" s="T116">PKZ_196X_Princess_flk.009 (009)</ta>
            <ta e="T122" id="Seg_1483" s="T119">PKZ_196X_Princess_flk.010 (010)</ta>
            <ta e="T127" id="Seg_1484" s="T122">PKZ_196X_Princess_flk.011 (011)</ta>
            <ta e="T132" id="Seg_1485" s="T127">PKZ_196X_Princess_flk.012 (012)</ta>
            <ta e="T134" id="Seg_1486" s="T132">PKZ_196X_Princess_flk.013 (013)</ta>
            <ta e="T136" id="Seg_1487" s="T134">PKZ_196X_Princess_flk.014 (014)</ta>
            <ta e="T139" id="Seg_1488" s="T136">PKZ_196X_Princess_flk.015 (015)</ta>
            <ta e="T141" id="Seg_1489" s="T139">PKZ_196X_Princess_flk.016 (016)</ta>
            <ta e="T145" id="Seg_1490" s="T141">PKZ_196X_Princess_flk.017 (017)</ta>
            <ta e="T149" id="Seg_1491" s="T145">PKZ_196X_Princess_flk.018 (018)</ta>
            <ta e="T154" id="Seg_1492" s="T149">PKZ_196X_Princess_flk.019 (019)</ta>
            <ta e="T157" id="Seg_1493" s="T154">PKZ_196X_Princess_flk.020 (020)</ta>
            <ta e="T159" id="Seg_1494" s="T157">PKZ_196X_Princess_flk.021 (021)</ta>
            <ta e="T162" id="Seg_1495" s="T159">PKZ_196X_Princess_flk.022 (022)</ta>
            <ta e="T167" id="Seg_1496" s="T162">PKZ_196X_Princess_flk.023 (023)</ta>
            <ta e="T172" id="Seg_1497" s="T167">PKZ_196X_Princess_flk.024 (024)</ta>
            <ta e="T177" id="Seg_1498" s="T172">PKZ_196X_Princess_flk.025 (025)</ta>
            <ta e="T179" id="Seg_1499" s="T177">PKZ_196X_Princess_flk.026 (026)</ta>
            <ta e="T181" id="Seg_1500" s="T179">PKZ_196X_Princess_flk.027 (027)</ta>
            <ta e="T185" id="Seg_1501" s="T181">PKZ_196X_Princess_flk.028 (028)</ta>
            <ta e="T188" id="Seg_1502" s="T185">PKZ_196X_Princess_flk.029 (029)</ta>
            <ta e="T194" id="Seg_1503" s="T188">PKZ_196X_Princess_flk.030 (030)</ta>
            <ta e="T197" id="Seg_1504" s="T194">PKZ_196X_Princess_flk.031 (031)</ta>
            <ta e="T201" id="Seg_1505" s="T197">PKZ_196X_Princess_flk.032 (032)</ta>
            <ta e="T208" id="Seg_1506" s="T201">PKZ_196X_Princess_flk.033 (033)</ta>
            <ta e="T215" id="Seg_1507" s="T208">PKZ_196X_Princess_flk.034 (034) </ta>
            <ta e="T219" id="Seg_1508" s="T215">PKZ_196X_Princess_flk.035 (036)</ta>
            <ta e="T221" id="Seg_1509" s="T219">PKZ_196X_Princess_flk.036 (037)</ta>
            <ta e="T227" id="Seg_1510" s="T221">PKZ_196X_Princess_flk.037 (038)</ta>
            <ta e="T231" id="Seg_1511" s="T227">PKZ_196X_Princess_flk.038 (039)</ta>
            <ta e="T237" id="Seg_1512" s="T231">PKZ_196X_Princess_flk.039 (040)</ta>
            <ta e="T244" id="Seg_1513" s="T237">PKZ_196X_Princess_flk.040 (041)</ta>
            <ta e="T248" id="Seg_1514" s="T244">PKZ_196X_Princess_flk.041 (042)</ta>
            <ta e="T253" id="Seg_1515" s="T248">PKZ_196X_Princess_flk.042 (043)</ta>
            <ta e="T256" id="Seg_1516" s="T253">PKZ_196X_Princess_flk.043 (044)</ta>
            <ta e="T259" id="Seg_1517" s="T256">PKZ_196X_Princess_flk.044 (045)</ta>
            <ta e="T262" id="Seg_1518" s="T259">PKZ_196X_Princess_flk.045 (046)</ta>
            <ta e="T273" id="Seg_1519" s="T262">PKZ_196X_Princess_flk.046 (047)</ta>
            <ta e="T282" id="Seg_1520" s="T273">PKZ_196X_Princess_flk.047 (048)</ta>
            <ta e="T295" id="Seg_1521" s="T282">PKZ_196X_Princess_flk.048 (049) </ta>
            <ta e="T298" id="Seg_1522" s="T295">PKZ_196X_Princess_flk.049 (051)</ta>
            <ta e="T304" id="Seg_1523" s="T298">PKZ_196X_Princess_flk.050 (052)</ta>
            <ta e="T308" id="Seg_1524" s="T304">PKZ_196X_Princess_flk.051 (053)</ta>
            <ta e="T311" id="Seg_1525" s="T308">PKZ_196X_Princess_flk.052 (054)</ta>
            <ta e="T316" id="Seg_1526" s="T311">PKZ_196X_Princess_flk.053 (055)</ta>
            <ta e="T320" id="Seg_1527" s="T316">PKZ_196X_Princess_flk.054 (056)</ta>
            <ta e="T323" id="Seg_1528" s="T320">PKZ_196X_Princess_flk.055 (057)</ta>
            <ta e="T330" id="Seg_1529" s="T323">PKZ_196X_Princess_flk.056 (058)</ta>
            <ta e="T333" id="Seg_1530" s="T330">PKZ_196X_Princess_flk.057 (059)</ta>
            <ta e="T334" id="Seg_1531" s="T333">PKZ_196X_Princess_flk.058 (060)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T93" id="Seg_1532" s="T89">Onʼiʔ kuza kambi saməjdəsʼtə. </ta>
            <ta e="T94" id="Seg_1533" s="T93">Šonəga. </ta>
            <ta e="T97" id="Seg_1534" s="T94">Nuʔməleʔbə volk dĭʔnə. </ta>
            <ta e="T101" id="Seg_1535" s="T97">Dĭ xatʼel dĭm tʼiʔtəsʼtə. </ta>
            <ta e="T106" id="Seg_1536" s="T101">Dĭ măndə:" Iʔ dʼiʔtəʔ măna! </ta>
            <ta e="T112" id="Seg_1537" s="T106">Măn (tăn= tănziʔ=) tăn elembə molam". </ta>
            <ta e="T114" id="Seg_1538" s="T112">Kambiʔi šidegöʔ. </ta>
            <ta e="T116" id="Seg_1539" s="T114">Dĭgəttə … </ta>
            <ta e="T119" id="Seg_1540" s="T116">Dĭgəttə šonəga (šidegöʔjəʔ). </ta>
            <ta e="T122" id="Seg_1541" s="T119">Dĭgəttə šoška šonəga. </ta>
            <ta e="T127" id="Seg_1542" s="T122">Dĭ (x-) xatʼel dĭʔnə dʼiʔtəsʼtə. </ta>
            <ta e="T132" id="Seg_1543" s="T127">Dĭ măndə:" Iʔ dʼiʔtəʔ măna! </ta>
            <ta e="T134" id="Seg_1544" s="T132">Elezeŋbə moləbaʔ!" </ta>
            <ta e="T136" id="Seg_1545" s="T134">Kambiʔi nagurgöʔ. </ta>
            <ta e="T139" id="Seg_1546" s="T136">Dĭgəttə šonəgaʔi, šonəgaʔi. </ta>
            <ta e="T141" id="Seg_1547" s="T139">Lisička nuʔməleʔbə. </ta>
            <ta e="T145" id="Seg_1548" s="T141">Dĭ dĭm xatʼel dʼiʔtəsʼtə. </ta>
            <ta e="T149" id="Seg_1549" s="T145">Dĭ:" Iʔ dʼiʔtə măna! </ta>
            <ta e="T154" id="Seg_1550" s="T149">Măn elem tăn (molə-) molam". </ta>
            <ta e="T157" id="Seg_1551" s="T154">Dĭgəttə (ka-) kambiʔi. </ta>
            <ta e="T159" id="Seg_1552" s="T157">Šonəgaʔi, šonəgaʔi. </ta>
            <ta e="T162" id="Seg_1553" s="T159">Süjö nʼergölaʔbə bar. </ta>
            <ta e="T167" id="Seg_1554" s="T162">Dĭm xatʼel (tʼiʔ-) dʼiʔtəsʼtə (bar). </ta>
            <ta e="T172" id="Seg_1555" s="T167">Dĭ măndə:" Iʔ dʼiʔtəʔ măna! </ta>
            <ta e="T177" id="Seg_1556" s="T172">(Măn=) Măn tăn elem molam!" </ta>
            <ta e="T179" id="Seg_1557" s="T177">Dĭgəttə šonəgaʔi. </ta>
            <ta e="T181" id="Seg_1558" s="T179">(Penzü) iʔbəlaʔbə. </ta>
            <ta e="T185" id="Seg_1559" s="T181">Dĭ dĭm xatʼel dʼiʔtəsʼtə. </ta>
            <ta e="T188" id="Seg_1560" s="T185">"Iʔ dʼiʔtəʔ măna! </ta>
            <ta e="T194" id="Seg_1561" s="T188">(Măn tn-) Măn tăn elel molam". </ta>
            <ta e="T197" id="Seg_1562" s="T194">Dĭgəttə šobiʔi măjanə. </ta>
            <ta e="T201" id="Seg_1563" s="T197">Dĭn kubiʔi ši dibər. </ta>
            <ta e="T208" id="Seg_1564" s="T201">Dĭn amnolaʔbə (i), bar ĭmbi ige amzittə. </ta>
            <ta e="T215" id="Seg_1565" s="T208">Dĭgəttə măndəʔi eleʔi: "Miʔ kuzan nada nejzittə. </ta>
            <ta e="T219" id="Seg_1566" s="T215">Tsarskɨj (ko-) koʔbdo ilbeʔ". </ta>
            <ta e="T221" id="Seg_1567" s="T219">Dĭgəttə kambiʔi. </ta>
            <ta e="T227" id="Seg_1568" s="T221">Šoška i lʼisʼitsa i volk kambiʔi. </ta>
            <ta e="T231" id="Seg_1569" s="T227">(Sʼarlaʔbə), bar il šobiʔi. </ta>
            <ta e="T237" id="Seg_1570" s="T231">Dĭgəttə i koʔbdo (š-) nʼiʔdə šobi. </ta>
            <ta e="T244" id="Seg_1571" s="T237">Dĭgəttə süjö dĭm kabarlaʔ kunnaːmbi dĭ kuzanə. </ta>
            <ta e="T248" id="Seg_1572" s="T244">Dĭgəttə (d-) amnobiʔi, amnobiʔi. </ta>
            <ta e="T253" id="Seg_1573" s="T248">Dĭgəttə (dĭ=) dĭʔnə tibinə kandəga. </ta>
            <ta e="T256" id="Seg_1574" s="T253">Koʔbdo kambi bünə. </ta>
            <ta e="T259" id="Seg_1575" s="T256">Dĭn nüke nulaʔbə. </ta>
            <ta e="T262" id="Seg_1576" s="T259">"Kanžəbəj maːʔnʼi, abandə!" </ta>
            <ta e="T273" id="Seg_1577" s="T262">"Măn kallam, surarlam bostə kuzan, dĭ măna măləj: kanaʔ, dak kallam". </ta>
            <ta e="T282" id="Seg_1578" s="T273">Dĭgəttə šobi penzü, (dĭ) dĭ nükem (tabə- tarbəluʔ-) talbəluʔpi. </ta>
            <ta e="T295" id="Seg_1579" s="T282">A dĭ nüke:" Kanaʔ abandə da (nörbit): Dĭn koʔbdo kalləj dĭ kuzanə, saməjlaʔbə". </ta>
            <ta e="T298" id="Seg_1580" s="T295">Dĭgəttə dĭ kambi. </ta>
            <ta e="T304" id="Seg_1581" s="T298">Abat amnobia, amnobia, măndə: puskaj kaŋgəj! </ta>
            <ta e="T308" id="Seg_1582" s="T304">Dĭgəttə (mĭbi) dĭʔnə iʔgö. </ta>
            <ta e="T311" id="Seg_1583" s="T308">Dĭ šobi, nörbəbi. </ta>
            <ta e="T316" id="Seg_1584" s="T311">Dĭgəttə dĭzeŋ ara bĭʔpiʔi bar. </ta>
            <ta e="T320" id="Seg_1585" s="T316">Dĭgəttə abat:" Šogaʔ miʔnʼibeʔ!" </ta>
            <ta e="T323" id="Seg_1586" s="T320">Dĭzeŋ ej kambiʔi. </ta>
            <ta e="T330" id="Seg_1587" s="T323">Dĭgəttə (dĭz- dĭʔn-) dĭzeŋdə bar tura jaʔpi. </ta>
            <ta e="T333" id="Seg_1588" s="T330">Bar ĭmbi mĭbi. </ta>
            <ta e="T334" id="Seg_1589" s="T333">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T90" id="Seg_1590" s="T89">onʼiʔ</ta>
            <ta e="T91" id="Seg_1591" s="T90">kuza</ta>
            <ta e="T92" id="Seg_1592" s="T91">kam-bi</ta>
            <ta e="T93" id="Seg_1593" s="T92">saməj-də-sʼtə</ta>
            <ta e="T94" id="Seg_1594" s="T93">šonə-ga</ta>
            <ta e="T95" id="Seg_1595" s="T94">nuʔmə-leʔbə</ta>
            <ta e="T96" id="Seg_1596" s="T95">volk</ta>
            <ta e="T97" id="Seg_1597" s="T96">dĭʔ-nə</ta>
            <ta e="T98" id="Seg_1598" s="T97">dĭ</ta>
            <ta e="T99" id="Seg_1599" s="T98">xatʼel</ta>
            <ta e="T100" id="Seg_1600" s="T99">dĭ-m</ta>
            <ta e="T101" id="Seg_1601" s="T100">tʼiʔ-tə-sʼtə</ta>
            <ta e="T102" id="Seg_1602" s="T101">dĭ</ta>
            <ta e="T103" id="Seg_1603" s="T102">măn-də</ta>
            <ta e="T104" id="Seg_1604" s="T103">i-ʔ</ta>
            <ta e="T105" id="Seg_1605" s="T104">dʼiʔ-tə-ʔ</ta>
            <ta e="T106" id="Seg_1606" s="T105">măna</ta>
            <ta e="T107" id="Seg_1607" s="T106">măn</ta>
            <ta e="T108" id="Seg_1608" s="T107">tăn</ta>
            <ta e="T109" id="Seg_1609" s="T108">tăn-ziʔ</ta>
            <ta e="T110" id="Seg_1610" s="T109">tăn</ta>
            <ta e="T111" id="Seg_1611" s="T110">elem-bə</ta>
            <ta e="T112" id="Seg_1612" s="T111">mo-la-m</ta>
            <ta e="T113" id="Seg_1613" s="T112">kam-bi-ʔi</ta>
            <ta e="T114" id="Seg_1614" s="T113">šide-göʔ</ta>
            <ta e="T116" id="Seg_1615" s="T114">dĭgəttə</ta>
            <ta e="T117" id="Seg_1616" s="T116">dĭgəttə</ta>
            <ta e="T118" id="Seg_1617" s="T117">šonə-ga</ta>
            <ta e="T119" id="Seg_1618" s="T118">šide-göʔ-jəʔ</ta>
            <ta e="T120" id="Seg_1619" s="T119">dĭgəttə</ta>
            <ta e="T121" id="Seg_1620" s="T120">šoška</ta>
            <ta e="T122" id="Seg_1621" s="T121">šonə-ga</ta>
            <ta e="T123" id="Seg_1622" s="T122">dĭ</ta>
            <ta e="T125" id="Seg_1623" s="T124">xatʼel</ta>
            <ta e="T126" id="Seg_1624" s="T125">dĭʔ-nə</ta>
            <ta e="T127" id="Seg_1625" s="T126">dʼiʔ-tə-sʼtə</ta>
            <ta e="T128" id="Seg_1626" s="T127">dĭ</ta>
            <ta e="T129" id="Seg_1627" s="T128">măn-də</ta>
            <ta e="T130" id="Seg_1628" s="T129">i-ʔ</ta>
            <ta e="T131" id="Seg_1629" s="T130">dʼiʔ-tə-ʔ</ta>
            <ta e="T132" id="Seg_1630" s="T131">măna</ta>
            <ta e="T133" id="Seg_1631" s="T132">ele-zeŋ-bə</ta>
            <ta e="T134" id="Seg_1632" s="T133">mo-lə-baʔ</ta>
            <ta e="T135" id="Seg_1633" s="T134">kam-bi-ʔi</ta>
            <ta e="T136" id="Seg_1634" s="T135">nagur-göʔ</ta>
            <ta e="T137" id="Seg_1635" s="T136">dĭgəttə</ta>
            <ta e="T138" id="Seg_1636" s="T137">šonə-ga-ʔi</ta>
            <ta e="T139" id="Seg_1637" s="T138">šonə-ga-ʔi</ta>
            <ta e="T140" id="Seg_1638" s="T139">lisička</ta>
            <ta e="T141" id="Seg_1639" s="T140">nuʔmə-leʔbə</ta>
            <ta e="T142" id="Seg_1640" s="T141">dĭ</ta>
            <ta e="T143" id="Seg_1641" s="T142">dĭ-m</ta>
            <ta e="T144" id="Seg_1642" s="T143">xatʼel</ta>
            <ta e="T145" id="Seg_1643" s="T144">dʼiʔ-tə-sʼtə</ta>
            <ta e="T146" id="Seg_1644" s="T145">dĭ</ta>
            <ta e="T147" id="Seg_1645" s="T146">i-ʔ</ta>
            <ta e="T148" id="Seg_1646" s="T147">dʼiʔ-tə</ta>
            <ta e="T149" id="Seg_1647" s="T148">măna</ta>
            <ta e="T150" id="Seg_1648" s="T149">măn</ta>
            <ta e="T151" id="Seg_1649" s="T150">elem</ta>
            <ta e="T152" id="Seg_1650" s="T151">tăn</ta>
            <ta e="T154" id="Seg_1651" s="T153">mo-la-m</ta>
            <ta e="T155" id="Seg_1652" s="T154">dĭgəttə</ta>
            <ta e="T157" id="Seg_1653" s="T156">kam-bi-ʔi</ta>
            <ta e="T158" id="Seg_1654" s="T157">šonə-ga-ʔi</ta>
            <ta e="T159" id="Seg_1655" s="T158">šonə-ga-ʔi</ta>
            <ta e="T160" id="Seg_1656" s="T159">süjö</ta>
            <ta e="T161" id="Seg_1657" s="T160">nʼergö-laʔbə</ta>
            <ta e="T162" id="Seg_1658" s="T161">bar</ta>
            <ta e="T163" id="Seg_1659" s="T162">dĭ-m</ta>
            <ta e="T164" id="Seg_1660" s="T163">xatʼel</ta>
            <ta e="T166" id="Seg_1661" s="T165">dʼiʔ-tə-sʼtə</ta>
            <ta e="T167" id="Seg_1662" s="T166">bar</ta>
            <ta e="T168" id="Seg_1663" s="T167">dĭ</ta>
            <ta e="T169" id="Seg_1664" s="T168">măn-də</ta>
            <ta e="T170" id="Seg_1665" s="T169">i-ʔ</ta>
            <ta e="T171" id="Seg_1666" s="T170">dʼiʔ-tə-ʔ</ta>
            <ta e="T172" id="Seg_1667" s="T171">măna</ta>
            <ta e="T173" id="Seg_1668" s="T172">măn</ta>
            <ta e="T174" id="Seg_1669" s="T173">măn</ta>
            <ta e="T175" id="Seg_1670" s="T174">tăn</ta>
            <ta e="T176" id="Seg_1671" s="T175">elem</ta>
            <ta e="T177" id="Seg_1672" s="T176">mo-la-m</ta>
            <ta e="T178" id="Seg_1673" s="T177">dĭgəttə</ta>
            <ta e="T179" id="Seg_1674" s="T178">šonə-ga-ʔi</ta>
            <ta e="T180" id="Seg_1675" s="T179">penzü</ta>
            <ta e="T181" id="Seg_1676" s="T180">iʔbə-laʔbə</ta>
            <ta e="T182" id="Seg_1677" s="T181">dĭ</ta>
            <ta e="T183" id="Seg_1678" s="T182">dĭ-m</ta>
            <ta e="T184" id="Seg_1679" s="T183">xatʼel</ta>
            <ta e="T185" id="Seg_1680" s="T184">dʼiʔ-tə-sʼtə</ta>
            <ta e="T186" id="Seg_1681" s="T185">i-ʔ</ta>
            <ta e="T187" id="Seg_1682" s="T186">dʼiʔ-tə-ʔ</ta>
            <ta e="T188" id="Seg_1683" s="T187">măna</ta>
            <ta e="T189" id="Seg_1684" s="T188">măn</ta>
            <ta e="T191" id="Seg_1685" s="T190">măn</ta>
            <ta e="T192" id="Seg_1686" s="T191">tăn</ta>
            <ta e="T193" id="Seg_1687" s="T192">ele-l</ta>
            <ta e="T194" id="Seg_1688" s="T193">mo-la-m</ta>
            <ta e="T195" id="Seg_1689" s="T194">dĭgəttə</ta>
            <ta e="T196" id="Seg_1690" s="T195">šo-bi-ʔi</ta>
            <ta e="T197" id="Seg_1691" s="T196">măja-nə</ta>
            <ta e="T198" id="Seg_1692" s="T197">dĭn</ta>
            <ta e="T199" id="Seg_1693" s="T198">ku-bi-ʔi</ta>
            <ta e="T200" id="Seg_1694" s="T199">ši</ta>
            <ta e="T201" id="Seg_1695" s="T200">dibər</ta>
            <ta e="T202" id="Seg_1696" s="T201">dĭn</ta>
            <ta e="T203" id="Seg_1697" s="T202">amno-laʔbə</ta>
            <ta e="T204" id="Seg_1698" s="T203">i</ta>
            <ta e="T205" id="Seg_1699" s="T204">bar</ta>
            <ta e="T206" id="Seg_1700" s="T205">ĭmbi</ta>
            <ta e="T207" id="Seg_1701" s="T206">i-ge</ta>
            <ta e="T208" id="Seg_1702" s="T207">am-zittə</ta>
            <ta e="T209" id="Seg_1703" s="T208">dĭgəttə</ta>
            <ta e="T210" id="Seg_1704" s="T209">măn-də-ʔi</ta>
            <ta e="T211" id="Seg_1705" s="T210">ele-ʔi</ta>
            <ta e="T212" id="Seg_1706" s="T211">miʔ</ta>
            <ta e="T213" id="Seg_1707" s="T212">kuza-n</ta>
            <ta e="T214" id="Seg_1708" s="T213">nada</ta>
            <ta e="T215" id="Seg_1709" s="T214">nej-zittə</ta>
            <ta e="T216" id="Seg_1710" s="T215">tsarskɨj</ta>
            <ta e="T218" id="Seg_1711" s="T217">koʔbdo</ta>
            <ta e="T219" id="Seg_1712" s="T218">i-l-beʔ</ta>
            <ta e="T220" id="Seg_1713" s="T219">dĭgəttə</ta>
            <ta e="T221" id="Seg_1714" s="T220">kam-bi-ʔi</ta>
            <ta e="T222" id="Seg_1715" s="T221">šoška</ta>
            <ta e="T223" id="Seg_1716" s="T222">i</ta>
            <ta e="T224" id="Seg_1717" s="T223">lʼisʼitsa</ta>
            <ta e="T225" id="Seg_1718" s="T224">i</ta>
            <ta e="T226" id="Seg_1719" s="T225">volk</ta>
            <ta e="T227" id="Seg_1720" s="T226">kam-bi-ʔi</ta>
            <ta e="T228" id="Seg_1721" s="T227">sʼar-laʔbə</ta>
            <ta e="T229" id="Seg_1722" s="T228">bar</ta>
            <ta e="T230" id="Seg_1723" s="T229">il</ta>
            <ta e="T231" id="Seg_1724" s="T230">šo-bi-ʔi</ta>
            <ta e="T232" id="Seg_1725" s="T231">dĭgəttə</ta>
            <ta e="T233" id="Seg_1726" s="T232">i</ta>
            <ta e="T234" id="Seg_1727" s="T233">koʔbdo</ta>
            <ta e="T236" id="Seg_1728" s="T235">nʼiʔdə</ta>
            <ta e="T237" id="Seg_1729" s="T236">šo-bi</ta>
            <ta e="T238" id="Seg_1730" s="T237">dĭgəttə</ta>
            <ta e="T239" id="Seg_1731" s="T238">süjö</ta>
            <ta e="T240" id="Seg_1732" s="T239">dĭ-m</ta>
            <ta e="T241" id="Seg_1733" s="T240">kabar-laʔ</ta>
            <ta e="T242" id="Seg_1734" s="T241">kun-naːm-bi</ta>
            <ta e="T243" id="Seg_1735" s="T242">dĭ</ta>
            <ta e="T244" id="Seg_1736" s="T243">kuza-nə</ta>
            <ta e="T245" id="Seg_1737" s="T244">dĭgəttə</ta>
            <ta e="T247" id="Seg_1738" s="T246">amno-bi-ʔi</ta>
            <ta e="T248" id="Seg_1739" s="T247">amno-bi-ʔi</ta>
            <ta e="T249" id="Seg_1740" s="T248">dĭgəttə</ta>
            <ta e="T250" id="Seg_1741" s="T249">dĭ</ta>
            <ta e="T251" id="Seg_1742" s="T250">dĭʔ-nə</ta>
            <ta e="T252" id="Seg_1743" s="T251">tibi-nə</ta>
            <ta e="T253" id="Seg_1744" s="T252">kandə-ga</ta>
            <ta e="T254" id="Seg_1745" s="T253">koʔbdo</ta>
            <ta e="T255" id="Seg_1746" s="T254">kam-bi</ta>
            <ta e="T256" id="Seg_1747" s="T255">bü-nə</ta>
            <ta e="T257" id="Seg_1748" s="T256">dĭn</ta>
            <ta e="T258" id="Seg_1749" s="T257">nüke</ta>
            <ta e="T259" id="Seg_1750" s="T258">nu-laʔbə</ta>
            <ta e="T260" id="Seg_1751" s="T259">kan-žə-bəj</ta>
            <ta e="T261" id="Seg_1752" s="T260">maːʔ-nʼi</ta>
            <ta e="T262" id="Seg_1753" s="T261">aba-ndə</ta>
            <ta e="T263" id="Seg_1754" s="T262">măn</ta>
            <ta e="T264" id="Seg_1755" s="T263">kal-la-m</ta>
            <ta e="T265" id="Seg_1756" s="T264">surar-la-m</ta>
            <ta e="T266" id="Seg_1757" s="T265">bos-tə</ta>
            <ta e="T267" id="Seg_1758" s="T266">kuza-n</ta>
            <ta e="T268" id="Seg_1759" s="T267">dĭ</ta>
            <ta e="T269" id="Seg_1760" s="T268">măna</ta>
            <ta e="T270" id="Seg_1761" s="T269">mă-lə-j</ta>
            <ta e="T271" id="Seg_1762" s="T270">kan-a-ʔ</ta>
            <ta e="T272" id="Seg_1763" s="T271">dak</ta>
            <ta e="T273" id="Seg_1764" s="T272">kal-la-m</ta>
            <ta e="T274" id="Seg_1765" s="T273">dĭgəttə</ta>
            <ta e="T275" id="Seg_1766" s="T274">šo-bi</ta>
            <ta e="T276" id="Seg_1767" s="T275">penzü</ta>
            <ta e="T277" id="Seg_1768" s="T276">dĭ</ta>
            <ta e="T278" id="Seg_1769" s="T277">dĭ</ta>
            <ta e="T279" id="Seg_1770" s="T278">nüke-m</ta>
            <ta e="T282" id="Seg_1771" s="T281">talbə-luʔ-pi</ta>
            <ta e="T283" id="Seg_1772" s="T282">a</ta>
            <ta e="T284" id="Seg_1773" s="T283">dĭ</ta>
            <ta e="T285" id="Seg_1774" s="T284">nüke</ta>
            <ta e="T286" id="Seg_1775" s="T285">kan-a-ʔ</ta>
            <ta e="T287" id="Seg_1776" s="T286">aba-ndə</ta>
            <ta e="T288" id="Seg_1777" s="T287">da</ta>
            <ta e="T289" id="Seg_1778" s="T288">nörbi-t</ta>
            <ta e="T290" id="Seg_1779" s="T289">dĭ-n</ta>
            <ta e="T291" id="Seg_1780" s="T290">koʔbdo</ta>
            <ta e="T292" id="Seg_1781" s="T291">kal-lə-j</ta>
            <ta e="T293" id="Seg_1782" s="T292">dĭ</ta>
            <ta e="T294" id="Seg_1783" s="T293">kuza-nə</ta>
            <ta e="T295" id="Seg_1784" s="T294">saməj-laʔbə</ta>
            <ta e="T296" id="Seg_1785" s="T295">dĭgəttə</ta>
            <ta e="T297" id="Seg_1786" s="T296">dĭ</ta>
            <ta e="T298" id="Seg_1787" s="T297">kam-bi</ta>
            <ta e="T299" id="Seg_1788" s="T298">aba-t</ta>
            <ta e="T300" id="Seg_1789" s="T299">amno-bia</ta>
            <ta e="T301" id="Seg_1790" s="T300">amno-bia</ta>
            <ta e="T302" id="Seg_1791" s="T301">măn-də</ta>
            <ta e="T303" id="Seg_1792" s="T302">puskaj</ta>
            <ta e="T304" id="Seg_1793" s="T303">kaŋ-gə-j</ta>
            <ta e="T305" id="Seg_1794" s="T304">dĭgəttə</ta>
            <ta e="T306" id="Seg_1795" s="T305">mĭ-bi</ta>
            <ta e="T307" id="Seg_1796" s="T306">dĭʔ-nə</ta>
            <ta e="T308" id="Seg_1797" s="T307">iʔgö</ta>
            <ta e="T309" id="Seg_1798" s="T308">dĭ</ta>
            <ta e="T310" id="Seg_1799" s="T309">šo-bi</ta>
            <ta e="T311" id="Seg_1800" s="T310">nörbə-bi</ta>
            <ta e="T312" id="Seg_1801" s="T311">dĭgəttə</ta>
            <ta e="T313" id="Seg_1802" s="T312">dĭ-zeŋ</ta>
            <ta e="T314" id="Seg_1803" s="T313">ara</ta>
            <ta e="T315" id="Seg_1804" s="T314">bĭʔ-pi-ʔi</ta>
            <ta e="T316" id="Seg_1805" s="T315">bar</ta>
            <ta e="T317" id="Seg_1806" s="T316">dĭgəttə</ta>
            <ta e="T318" id="Seg_1807" s="T317">aba-t</ta>
            <ta e="T319" id="Seg_1808" s="T318">šo-gaʔ</ta>
            <ta e="T320" id="Seg_1809" s="T319">miʔnʼibeʔ</ta>
            <ta e="T321" id="Seg_1810" s="T320">dĭ-zeŋ</ta>
            <ta e="T322" id="Seg_1811" s="T321">ej</ta>
            <ta e="T323" id="Seg_1812" s="T322">kam-bi-ʔi</ta>
            <ta e="T324" id="Seg_1813" s="T323">dĭgəttə</ta>
            <ta e="T327" id="Seg_1814" s="T326">dĭ-zeŋ-də</ta>
            <ta e="T328" id="Seg_1815" s="T327">bar</ta>
            <ta e="T329" id="Seg_1816" s="T328">tura</ta>
            <ta e="T330" id="Seg_1817" s="T329">jaʔ-pi</ta>
            <ta e="T331" id="Seg_1818" s="T330">bar</ta>
            <ta e="T332" id="Seg_1819" s="T331">ĭmbi</ta>
            <ta e="T333" id="Seg_1820" s="T332">mĭ-bi</ta>
            <ta e="T334" id="Seg_1821" s="T333">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T90" id="Seg_1822" s="T89">onʼiʔ</ta>
            <ta e="T91" id="Seg_1823" s="T90">kuza</ta>
            <ta e="T92" id="Seg_1824" s="T91">kan-bi</ta>
            <ta e="T93" id="Seg_1825" s="T92">saməj-ntə-zittə</ta>
            <ta e="T94" id="Seg_1826" s="T93">šonə-gA</ta>
            <ta e="T95" id="Seg_1827" s="T94">nuʔmə-laʔbə</ta>
            <ta e="T96" id="Seg_1828" s="T95">volk</ta>
            <ta e="T97" id="Seg_1829" s="T96">dĭ-Tə</ta>
            <ta e="T98" id="Seg_1830" s="T97">dĭ</ta>
            <ta e="T99" id="Seg_1831" s="T98">xatʼel</ta>
            <ta e="T100" id="Seg_1832" s="T99">dĭ-m</ta>
            <ta e="T101" id="Seg_1833" s="T100">tʼit-ntə-zittə</ta>
            <ta e="T102" id="Seg_1834" s="T101">dĭ</ta>
            <ta e="T103" id="Seg_1835" s="T102">măn-ntə</ta>
            <ta e="T104" id="Seg_1836" s="T103">e-ʔ</ta>
            <ta e="T105" id="Seg_1837" s="T104">tʼit-ntə-ʔ</ta>
            <ta e="T106" id="Seg_1838" s="T105">măna</ta>
            <ta e="T107" id="Seg_1839" s="T106">măn</ta>
            <ta e="T108" id="Seg_1840" s="T107">tăn</ta>
            <ta e="T109" id="Seg_1841" s="T108">tăn-ziʔ</ta>
            <ta e="T110" id="Seg_1842" s="T109">tăn</ta>
            <ta e="T111" id="Seg_1843" s="T110">hele-bə</ta>
            <ta e="T112" id="Seg_1844" s="T111">mo-lV-m</ta>
            <ta e="T113" id="Seg_1845" s="T112">kan-bi-jəʔ</ta>
            <ta e="T114" id="Seg_1846" s="T113">šide-göʔ</ta>
            <ta e="T116" id="Seg_1847" s="T114">dĭgəttə</ta>
            <ta e="T117" id="Seg_1848" s="T116">dĭgəttə</ta>
            <ta e="T118" id="Seg_1849" s="T117">šonə-gA</ta>
            <ta e="T119" id="Seg_1850" s="T118">šide-göʔ-jəʔ</ta>
            <ta e="T120" id="Seg_1851" s="T119">dĭgəttə</ta>
            <ta e="T121" id="Seg_1852" s="T120">šoška</ta>
            <ta e="T122" id="Seg_1853" s="T121">šonə-gA</ta>
            <ta e="T123" id="Seg_1854" s="T122">dĭ</ta>
            <ta e="T125" id="Seg_1855" s="T124">xatʼel</ta>
            <ta e="T126" id="Seg_1856" s="T125">dĭ-Tə</ta>
            <ta e="T127" id="Seg_1857" s="T126">tʼit-ntə-zittə</ta>
            <ta e="T128" id="Seg_1858" s="T127">dĭ</ta>
            <ta e="T129" id="Seg_1859" s="T128">măn-ntə</ta>
            <ta e="T130" id="Seg_1860" s="T129">e-ʔ</ta>
            <ta e="T131" id="Seg_1861" s="T130">tʼit-ntə-ʔ</ta>
            <ta e="T132" id="Seg_1862" s="T131">măna</ta>
            <ta e="T133" id="Seg_1863" s="T132">hele-zAŋ-bə</ta>
            <ta e="T134" id="Seg_1864" s="T133">mo-lV-bAʔ</ta>
            <ta e="T135" id="Seg_1865" s="T134">kan-bi-jəʔ</ta>
            <ta e="T136" id="Seg_1866" s="T135">nagur-göʔ</ta>
            <ta e="T137" id="Seg_1867" s="T136">dĭgəttə</ta>
            <ta e="T138" id="Seg_1868" s="T137">šonə-gA-jəʔ</ta>
            <ta e="T139" id="Seg_1869" s="T138">šonə-gA-jəʔ</ta>
            <ta e="T140" id="Seg_1870" s="T139">lisička</ta>
            <ta e="T141" id="Seg_1871" s="T140">nuʔmə-laʔbə</ta>
            <ta e="T142" id="Seg_1872" s="T141">dĭ</ta>
            <ta e="T143" id="Seg_1873" s="T142">dĭ-m</ta>
            <ta e="T144" id="Seg_1874" s="T143">xatʼel</ta>
            <ta e="T145" id="Seg_1875" s="T144">tʼit-ntə-zittə</ta>
            <ta e="T146" id="Seg_1876" s="T145">dĭ</ta>
            <ta e="T147" id="Seg_1877" s="T146">e-ʔ</ta>
            <ta e="T148" id="Seg_1878" s="T147">tʼit-t</ta>
            <ta e="T149" id="Seg_1879" s="T148">măna</ta>
            <ta e="T150" id="Seg_1880" s="T149">măn</ta>
            <ta e="T151" id="Seg_1881" s="T150">hele</ta>
            <ta e="T152" id="Seg_1882" s="T151">tăn</ta>
            <ta e="T154" id="Seg_1883" s="T153">mo-lV-m</ta>
            <ta e="T155" id="Seg_1884" s="T154">dĭgəttə</ta>
            <ta e="T157" id="Seg_1885" s="T156">kan-bi-jəʔ</ta>
            <ta e="T158" id="Seg_1886" s="T157">šonə-gA-jəʔ</ta>
            <ta e="T159" id="Seg_1887" s="T158">šonə-gA-jəʔ</ta>
            <ta e="T160" id="Seg_1888" s="T159">süjö</ta>
            <ta e="T161" id="Seg_1889" s="T160">nʼergö-laʔbə</ta>
            <ta e="T162" id="Seg_1890" s="T161">bar</ta>
            <ta e="T163" id="Seg_1891" s="T162">dĭ-m</ta>
            <ta e="T164" id="Seg_1892" s="T163">xatʼel</ta>
            <ta e="T166" id="Seg_1893" s="T165">tʼit-ntə-zittə</ta>
            <ta e="T167" id="Seg_1894" s="T166">bar</ta>
            <ta e="T168" id="Seg_1895" s="T167">dĭ</ta>
            <ta e="T169" id="Seg_1896" s="T168">măn-ntə</ta>
            <ta e="T170" id="Seg_1897" s="T169">e-ʔ</ta>
            <ta e="T171" id="Seg_1898" s="T170">tʼit-ntə-ʔ</ta>
            <ta e="T172" id="Seg_1899" s="T171">măna</ta>
            <ta e="T173" id="Seg_1900" s="T172">măn</ta>
            <ta e="T174" id="Seg_1901" s="T173">măn</ta>
            <ta e="T175" id="Seg_1902" s="T174">tăn</ta>
            <ta e="T176" id="Seg_1903" s="T175">hele</ta>
            <ta e="T177" id="Seg_1904" s="T176">mo-lV-m</ta>
            <ta e="T178" id="Seg_1905" s="T177">dĭgəttə</ta>
            <ta e="T179" id="Seg_1906" s="T178">šonə-gA-jəʔ</ta>
            <ta e="T180" id="Seg_1907" s="T179">penzi</ta>
            <ta e="T181" id="Seg_1908" s="T180">iʔbö-laʔbə</ta>
            <ta e="T182" id="Seg_1909" s="T181">dĭ</ta>
            <ta e="T183" id="Seg_1910" s="T182">dĭ-m</ta>
            <ta e="T184" id="Seg_1911" s="T183">xatʼel</ta>
            <ta e="T185" id="Seg_1912" s="T184">tʼit-ntə-zittə</ta>
            <ta e="T186" id="Seg_1913" s="T185">e-ʔ</ta>
            <ta e="T187" id="Seg_1914" s="T186">tʼit-ntə-ʔ</ta>
            <ta e="T188" id="Seg_1915" s="T187">măna</ta>
            <ta e="T189" id="Seg_1916" s="T188">măn</ta>
            <ta e="T191" id="Seg_1917" s="T190">măn</ta>
            <ta e="T192" id="Seg_1918" s="T191">tăn</ta>
            <ta e="T193" id="Seg_1919" s="T192">hele-l</ta>
            <ta e="T194" id="Seg_1920" s="T193">mo-lV-m</ta>
            <ta e="T195" id="Seg_1921" s="T194">dĭgəttə</ta>
            <ta e="T196" id="Seg_1922" s="T195">šo-bi-jəʔ</ta>
            <ta e="T197" id="Seg_1923" s="T196">măja-Tə</ta>
            <ta e="T198" id="Seg_1924" s="T197">dĭn</ta>
            <ta e="T199" id="Seg_1925" s="T198">ku-bi-jəʔ</ta>
            <ta e="T200" id="Seg_1926" s="T199">ši</ta>
            <ta e="T201" id="Seg_1927" s="T200">dĭbər</ta>
            <ta e="T202" id="Seg_1928" s="T201">dĭn</ta>
            <ta e="T203" id="Seg_1929" s="T202">amno-laʔbə</ta>
            <ta e="T204" id="Seg_1930" s="T203">i</ta>
            <ta e="T205" id="Seg_1931" s="T204">bar</ta>
            <ta e="T206" id="Seg_1932" s="T205">ĭmbi</ta>
            <ta e="T207" id="Seg_1933" s="T206">i-gA</ta>
            <ta e="T208" id="Seg_1934" s="T207">am-zittə</ta>
            <ta e="T209" id="Seg_1935" s="T208">dĭgəttə</ta>
            <ta e="T210" id="Seg_1936" s="T209">măn-ntə-jəʔ</ta>
            <ta e="T211" id="Seg_1937" s="T210">hele-jəʔ</ta>
            <ta e="T212" id="Seg_1938" s="T211">miʔ</ta>
            <ta e="T213" id="Seg_1939" s="T212">kuza-n</ta>
            <ta e="T214" id="Seg_1940" s="T213">nadə</ta>
            <ta e="T215" id="Seg_1941" s="T214">nej-zittə</ta>
            <ta e="T216" id="Seg_1942" s="T215">tsarskɨj</ta>
            <ta e="T218" id="Seg_1943" s="T217">koʔbdo</ta>
            <ta e="T219" id="Seg_1944" s="T218">i-l-bAʔ</ta>
            <ta e="T220" id="Seg_1945" s="T219">dĭgəttə</ta>
            <ta e="T221" id="Seg_1946" s="T220">kan-bi-jəʔ</ta>
            <ta e="T222" id="Seg_1947" s="T221">šoška</ta>
            <ta e="T223" id="Seg_1948" s="T222">i</ta>
            <ta e="T224" id="Seg_1949" s="T223">lʼisʼitsa</ta>
            <ta e="T225" id="Seg_1950" s="T224">i</ta>
            <ta e="T226" id="Seg_1951" s="T225">volk</ta>
            <ta e="T227" id="Seg_1952" s="T226">kan-bi-jəʔ</ta>
            <ta e="T228" id="Seg_1953" s="T227">sʼar-laʔbə</ta>
            <ta e="T229" id="Seg_1954" s="T228">bar</ta>
            <ta e="T230" id="Seg_1955" s="T229">il</ta>
            <ta e="T231" id="Seg_1956" s="T230">šo-bi-jəʔ</ta>
            <ta e="T232" id="Seg_1957" s="T231">dĭgəttə</ta>
            <ta e="T233" id="Seg_1958" s="T232">i</ta>
            <ta e="T234" id="Seg_1959" s="T233">koʔbdo</ta>
            <ta e="T236" id="Seg_1960" s="T235">nʼiʔdə</ta>
            <ta e="T237" id="Seg_1961" s="T236">šo-bi</ta>
            <ta e="T238" id="Seg_1962" s="T237">dĭgəttə</ta>
            <ta e="T239" id="Seg_1963" s="T238">süjö</ta>
            <ta e="T240" id="Seg_1964" s="T239">dĭ-m</ta>
            <ta e="T241" id="Seg_1965" s="T240">kabar-lAʔ</ta>
            <ta e="T242" id="Seg_1966" s="T241">kun-laːm-bi</ta>
            <ta e="T243" id="Seg_1967" s="T242">dĭ</ta>
            <ta e="T244" id="Seg_1968" s="T243">kuza-Tə</ta>
            <ta e="T245" id="Seg_1969" s="T244">dĭgəttə</ta>
            <ta e="T247" id="Seg_1970" s="T246">amno-bi-jəʔ</ta>
            <ta e="T248" id="Seg_1971" s="T247">amno-bi-jəʔ</ta>
            <ta e="T249" id="Seg_1972" s="T248">dĭgəttə</ta>
            <ta e="T250" id="Seg_1973" s="T249">dĭ</ta>
            <ta e="T251" id="Seg_1974" s="T250">dĭ-Tə</ta>
            <ta e="T252" id="Seg_1975" s="T251">tibi-Tə</ta>
            <ta e="T253" id="Seg_1976" s="T252">kandə-gA</ta>
            <ta e="T254" id="Seg_1977" s="T253">koʔbdo</ta>
            <ta e="T255" id="Seg_1978" s="T254">kan-bi</ta>
            <ta e="T256" id="Seg_1979" s="T255">bü-Tə</ta>
            <ta e="T257" id="Seg_1980" s="T256">dĭn</ta>
            <ta e="T258" id="Seg_1981" s="T257">nüke</ta>
            <ta e="T259" id="Seg_1982" s="T258">nu-laʔbə</ta>
            <ta e="T260" id="Seg_1983" s="T259">kan-žə-bəj</ta>
            <ta e="T261" id="Seg_1984" s="T260">maʔ-gənʼi</ta>
            <ta e="T262" id="Seg_1985" s="T261">aba-gəndə</ta>
            <ta e="T263" id="Seg_1986" s="T262">măn</ta>
            <ta e="T264" id="Seg_1987" s="T263">kan-lV-m</ta>
            <ta e="T265" id="Seg_1988" s="T264">surar-lV-m</ta>
            <ta e="T266" id="Seg_1989" s="T265">bos-də</ta>
            <ta e="T267" id="Seg_1990" s="T266">kuza-n</ta>
            <ta e="T268" id="Seg_1991" s="T267">dĭ</ta>
            <ta e="T269" id="Seg_1992" s="T268">măna</ta>
            <ta e="T270" id="Seg_1993" s="T269">măn-lV-j</ta>
            <ta e="T271" id="Seg_1994" s="T270">kan-ə-ʔ</ta>
            <ta e="T272" id="Seg_1995" s="T271">tak</ta>
            <ta e="T273" id="Seg_1996" s="T272">kan-lV-m</ta>
            <ta e="T274" id="Seg_1997" s="T273">dĭgəttə</ta>
            <ta e="T275" id="Seg_1998" s="T274">šo-bi</ta>
            <ta e="T276" id="Seg_1999" s="T275">penzi</ta>
            <ta e="T277" id="Seg_2000" s="T276">dĭ</ta>
            <ta e="T278" id="Seg_2001" s="T277">dĭ</ta>
            <ta e="T279" id="Seg_2002" s="T278">nüke-m</ta>
            <ta e="T282" id="Seg_2003" s="T281">talbə-luʔbdə-bi</ta>
            <ta e="T283" id="Seg_2004" s="T282">a</ta>
            <ta e="T284" id="Seg_2005" s="T283">dĭ</ta>
            <ta e="T285" id="Seg_2006" s="T284">nüke</ta>
            <ta e="T286" id="Seg_2007" s="T285">kan-ə-ʔ</ta>
            <ta e="T287" id="Seg_2008" s="T286">aba-gəndə</ta>
            <ta e="T288" id="Seg_2009" s="T287">da</ta>
            <ta e="T289" id="Seg_2010" s="T288">nörbə-t</ta>
            <ta e="T290" id="Seg_2011" s="T289">dĭ-n</ta>
            <ta e="T291" id="Seg_2012" s="T290">koʔbdo</ta>
            <ta e="T292" id="Seg_2013" s="T291">kan-lV-j</ta>
            <ta e="T293" id="Seg_2014" s="T292">dĭ</ta>
            <ta e="T294" id="Seg_2015" s="T293">kuza-Tə</ta>
            <ta e="T295" id="Seg_2016" s="T294">saməj-laʔbə</ta>
            <ta e="T296" id="Seg_2017" s="T295">dĭgəttə</ta>
            <ta e="T297" id="Seg_2018" s="T296">dĭ</ta>
            <ta e="T298" id="Seg_2019" s="T297">kan-bi</ta>
            <ta e="T299" id="Seg_2020" s="T298">aba-t</ta>
            <ta e="T300" id="Seg_2021" s="T299">amno-bi</ta>
            <ta e="T301" id="Seg_2022" s="T300">amno-bi</ta>
            <ta e="T302" id="Seg_2023" s="T301">măn-ntə</ta>
            <ta e="T303" id="Seg_2024" s="T302">puskaj</ta>
            <ta e="T304" id="Seg_2025" s="T303">kan-KV-j</ta>
            <ta e="T305" id="Seg_2026" s="T304">dĭgəttə</ta>
            <ta e="T306" id="Seg_2027" s="T305">mĭ-bi</ta>
            <ta e="T307" id="Seg_2028" s="T306">dĭ-Tə</ta>
            <ta e="T308" id="Seg_2029" s="T307">iʔgö</ta>
            <ta e="T309" id="Seg_2030" s="T308">dĭ</ta>
            <ta e="T310" id="Seg_2031" s="T309">šo-bi</ta>
            <ta e="T311" id="Seg_2032" s="T310">nörbə-bi</ta>
            <ta e="T312" id="Seg_2033" s="T311">dĭgəttə</ta>
            <ta e="T313" id="Seg_2034" s="T312">dĭ-zAŋ</ta>
            <ta e="T314" id="Seg_2035" s="T313">ara</ta>
            <ta e="T315" id="Seg_2036" s="T314">bĭs-bi-jəʔ</ta>
            <ta e="T316" id="Seg_2037" s="T315">bar</ta>
            <ta e="T317" id="Seg_2038" s="T316">dĭgəttə</ta>
            <ta e="T318" id="Seg_2039" s="T317">aba-t</ta>
            <ta e="T319" id="Seg_2040" s="T318">šo-KAʔ</ta>
            <ta e="T320" id="Seg_2041" s="T319">miʔnʼibeʔ</ta>
            <ta e="T321" id="Seg_2042" s="T320">dĭ-zAŋ</ta>
            <ta e="T322" id="Seg_2043" s="T321">ej</ta>
            <ta e="T323" id="Seg_2044" s="T322">kan-bi-jəʔ</ta>
            <ta e="T324" id="Seg_2045" s="T323">dĭgəttə</ta>
            <ta e="T327" id="Seg_2046" s="T326">dĭ-zAŋ-Tə</ta>
            <ta e="T328" id="Seg_2047" s="T327">bar</ta>
            <ta e="T329" id="Seg_2048" s="T328">tura</ta>
            <ta e="T330" id="Seg_2049" s="T329">hʼaʔ-bi</ta>
            <ta e="T331" id="Seg_2050" s="T330">bar</ta>
            <ta e="T332" id="Seg_2051" s="T331">ĭmbi</ta>
            <ta e="T333" id="Seg_2052" s="T332">mĭ-bi</ta>
            <ta e="T334" id="Seg_2053" s="T333">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T90" id="Seg_2054" s="T89">one.[NOM.SG]</ta>
            <ta e="T91" id="Seg_2055" s="T90">man.[NOM.SG]</ta>
            <ta e="T92" id="Seg_2056" s="T91">go-PST.[3SG]</ta>
            <ta e="T93" id="Seg_2057" s="T92">hunt-IPFVZ-INF.LAT</ta>
            <ta e="T94" id="Seg_2058" s="T93">come-PRS.[3SG]</ta>
            <ta e="T95" id="Seg_2059" s="T94">run-DUR.[3SG]</ta>
            <ta e="T96" id="Seg_2060" s="T95">wolf.[NOM.SG]</ta>
            <ta e="T97" id="Seg_2061" s="T96">this-LAT</ta>
            <ta e="T98" id="Seg_2062" s="T97">this.[NOM.SG]</ta>
            <ta e="T99" id="Seg_2063" s="T98">want.PST.M.SG</ta>
            <ta e="T100" id="Seg_2064" s="T99">this-ACC</ta>
            <ta e="T101" id="Seg_2065" s="T100">shoot-IPFVZ-INF.LAT</ta>
            <ta e="T102" id="Seg_2066" s="T101">this.[NOM.SG]</ta>
            <ta e="T103" id="Seg_2067" s="T102">say-IPFVZ.[3SG]</ta>
            <ta e="T104" id="Seg_2068" s="T103">NEG.AUX-IMP.2SG</ta>
            <ta e="T105" id="Seg_2069" s="T104">shoot-IPFVZ-CNG</ta>
            <ta e="T106" id="Seg_2070" s="T105">I.ACC</ta>
            <ta e="T107" id="Seg_2071" s="T106">I.NOM</ta>
            <ta e="T108" id="Seg_2072" s="T107">you.GEN</ta>
            <ta e="T109" id="Seg_2073" s="T108">you.NOM-INS</ta>
            <ta e="T110" id="Seg_2074" s="T109">you.GEN</ta>
            <ta e="T111" id="Seg_2075" s="T110">companion-ACC.3SG</ta>
            <ta e="T112" id="Seg_2076" s="T111">become-FUT-1SG</ta>
            <ta e="T113" id="Seg_2077" s="T112">go-PST-3PL</ta>
            <ta e="T114" id="Seg_2078" s="T113">two-COLL</ta>
            <ta e="T116" id="Seg_2079" s="T114">then</ta>
            <ta e="T117" id="Seg_2080" s="T116">then</ta>
            <ta e="T118" id="Seg_2081" s="T117">come-PRS.[3SG]</ta>
            <ta e="T119" id="Seg_2082" s="T118">two-COLL-PL</ta>
            <ta e="T120" id="Seg_2083" s="T119">then</ta>
            <ta e="T121" id="Seg_2084" s="T120">pig.[NOM.SG]</ta>
            <ta e="T122" id="Seg_2085" s="T121">come-PRS.[3SG]</ta>
            <ta e="T123" id="Seg_2086" s="T122">this.[NOM.SG]</ta>
            <ta e="T125" id="Seg_2087" s="T124">want.PST.M.SG</ta>
            <ta e="T126" id="Seg_2088" s="T125">this-LAT</ta>
            <ta e="T127" id="Seg_2089" s="T126">shoot-IPFVZ-INF.LAT</ta>
            <ta e="T128" id="Seg_2090" s="T127">this.[NOM.SG]</ta>
            <ta e="T129" id="Seg_2091" s="T128">say-IPFVZ.[3SG]</ta>
            <ta e="T130" id="Seg_2092" s="T129">NEG.AUX-IMP.2SG</ta>
            <ta e="T131" id="Seg_2093" s="T130">shoot-IPFVZ-CNG</ta>
            <ta e="T132" id="Seg_2094" s="T131">I.ACC</ta>
            <ta e="T133" id="Seg_2095" s="T132">companion-PL-ACC.3SG</ta>
            <ta e="T134" id="Seg_2096" s="T133">become-FUT-1PL</ta>
            <ta e="T135" id="Seg_2097" s="T134">go-PST-3PL</ta>
            <ta e="T136" id="Seg_2098" s="T135">three-COLL</ta>
            <ta e="T137" id="Seg_2099" s="T136">then</ta>
            <ta e="T138" id="Seg_2100" s="T137">come-PRS-3PL</ta>
            <ta e="T139" id="Seg_2101" s="T138">come-PRS-3PL</ta>
            <ta e="T140" id="Seg_2102" s="T139">fox.[NOM.SG]</ta>
            <ta e="T141" id="Seg_2103" s="T140">run-DUR.[3SG]</ta>
            <ta e="T142" id="Seg_2104" s="T141">this.[NOM.SG]</ta>
            <ta e="T143" id="Seg_2105" s="T142">this-ACC</ta>
            <ta e="T144" id="Seg_2106" s="T143">want.PST.M.SG</ta>
            <ta e="T145" id="Seg_2107" s="T144">shoot-IPFVZ-INF.LAT</ta>
            <ta e="T146" id="Seg_2108" s="T145">this.[NOM.SG]</ta>
            <ta e="T147" id="Seg_2109" s="T146">NEG.AUX-IMP.2SG</ta>
            <ta e="T148" id="Seg_2110" s="T147">shoot-IMP.2SG.O</ta>
            <ta e="T149" id="Seg_2111" s="T148">I.ACC</ta>
            <ta e="T150" id="Seg_2112" s="T149">I.NOM</ta>
            <ta e="T151" id="Seg_2113" s="T150">companion.[NOM.SG]</ta>
            <ta e="T152" id="Seg_2114" s="T151">you.GEN</ta>
            <ta e="T154" id="Seg_2115" s="T153">become-FUT-1SG</ta>
            <ta e="T155" id="Seg_2116" s="T154">then</ta>
            <ta e="T157" id="Seg_2117" s="T156">go-PST-3PL</ta>
            <ta e="T158" id="Seg_2118" s="T157">come-PRS-3PL</ta>
            <ta e="T159" id="Seg_2119" s="T158">come-PRS-3PL</ta>
            <ta e="T160" id="Seg_2120" s="T159">bird.[NOM.SG]</ta>
            <ta e="T161" id="Seg_2121" s="T160">fly-DUR.[3SG]</ta>
            <ta e="T162" id="Seg_2122" s="T161">PTCL</ta>
            <ta e="T163" id="Seg_2123" s="T162">this-ACC</ta>
            <ta e="T164" id="Seg_2124" s="T163">want.PST.M.SG</ta>
            <ta e="T166" id="Seg_2125" s="T165">shoot-IPFVZ-INF.LAT</ta>
            <ta e="T167" id="Seg_2126" s="T166">PTCL</ta>
            <ta e="T168" id="Seg_2127" s="T167">this.[NOM.SG]</ta>
            <ta e="T169" id="Seg_2128" s="T168">say-IPFVZ.[3SG]</ta>
            <ta e="T170" id="Seg_2129" s="T169">NEG.AUX-IMP.2SG</ta>
            <ta e="T171" id="Seg_2130" s="T170">shoot-IPFVZ-CNG</ta>
            <ta e="T172" id="Seg_2131" s="T171">I.ACC</ta>
            <ta e="T173" id="Seg_2132" s="T172">I.NOM</ta>
            <ta e="T174" id="Seg_2133" s="T173">I.NOM</ta>
            <ta e="T175" id="Seg_2134" s="T174">you.GEN</ta>
            <ta e="T176" id="Seg_2135" s="T175">companion.[NOM.SG]</ta>
            <ta e="T177" id="Seg_2136" s="T176">become-FUT-1SG</ta>
            <ta e="T178" id="Seg_2137" s="T177">then</ta>
            <ta e="T179" id="Seg_2138" s="T178">come-PRS-3PL</ta>
            <ta e="T180" id="Seg_2139" s="T179">%snake.[NOM.SG]</ta>
            <ta e="T181" id="Seg_2140" s="T180">lie-DUR.[3SG]</ta>
            <ta e="T182" id="Seg_2141" s="T181">this.[NOM.SG]</ta>
            <ta e="T183" id="Seg_2142" s="T182">this-ACC</ta>
            <ta e="T184" id="Seg_2143" s="T183">want.PST.M.SG</ta>
            <ta e="T185" id="Seg_2144" s="T184">shoot-IPFVZ-INF.LAT</ta>
            <ta e="T186" id="Seg_2145" s="T185">NEG.AUX-IMP.2SG</ta>
            <ta e="T187" id="Seg_2146" s="T186">shoot-IPFVZ-CNG</ta>
            <ta e="T188" id="Seg_2147" s="T187">I.ACC</ta>
            <ta e="T189" id="Seg_2148" s="T188">I.NOM</ta>
            <ta e="T191" id="Seg_2149" s="T190">I.NOM</ta>
            <ta e="T192" id="Seg_2150" s="T191">you.GEN</ta>
            <ta e="T193" id="Seg_2151" s="T192">companion-NOM/GEN/ACC.2SG</ta>
            <ta e="T194" id="Seg_2152" s="T193">become-FUT-1SG</ta>
            <ta e="T195" id="Seg_2153" s="T194">then</ta>
            <ta e="T196" id="Seg_2154" s="T195">come-PST-3PL</ta>
            <ta e="T197" id="Seg_2155" s="T196">mountain-LAT</ta>
            <ta e="T198" id="Seg_2156" s="T197">there</ta>
            <ta e="T199" id="Seg_2157" s="T198">see-PST-3PL</ta>
            <ta e="T200" id="Seg_2158" s="T199">hole.[NOM.SG]</ta>
            <ta e="T201" id="Seg_2159" s="T200">there</ta>
            <ta e="T202" id="Seg_2160" s="T201">there</ta>
            <ta e="T203" id="Seg_2161" s="T202">live-DUR.[3SG]</ta>
            <ta e="T204" id="Seg_2162" s="T203">and</ta>
            <ta e="T205" id="Seg_2163" s="T204">all</ta>
            <ta e="T206" id="Seg_2164" s="T205">what.[NOM.SG]</ta>
            <ta e="T207" id="Seg_2165" s="T206">be-PRS.[3SG]</ta>
            <ta e="T208" id="Seg_2166" s="T207">eat-INF.LAT</ta>
            <ta e="T209" id="Seg_2167" s="T208">then</ta>
            <ta e="T210" id="Seg_2168" s="T209">say-IPFVZ-3PL</ta>
            <ta e="T211" id="Seg_2169" s="T210">companion-PL</ta>
            <ta e="T212" id="Seg_2170" s="T211">we.GEN</ta>
            <ta e="T213" id="Seg_2171" s="T212">man-GEN</ta>
            <ta e="T214" id="Seg_2172" s="T213">one.should</ta>
            <ta e="T215" id="Seg_2173" s="T214">marry-INF.LAT</ta>
            <ta e="T216" id="Seg_2174" s="T215">tsar.ADJ.[NOM.SG]</ta>
            <ta e="T218" id="Seg_2175" s="T217">girl.[NOM.SG]</ta>
            <ta e="T219" id="Seg_2176" s="T218">take-FUT-1PL</ta>
            <ta e="T220" id="Seg_2177" s="T219">then</ta>
            <ta e="T221" id="Seg_2178" s="T220">go-PST-3PL</ta>
            <ta e="T222" id="Seg_2179" s="T221">pig.[NOM.SG]</ta>
            <ta e="T223" id="Seg_2180" s="T222">and</ta>
            <ta e="T224" id="Seg_2181" s="T223">fox.[NOM.SG]</ta>
            <ta e="T225" id="Seg_2182" s="T224">and</ta>
            <ta e="T226" id="Seg_2183" s="T225">wolf.[NOM.SG]</ta>
            <ta e="T227" id="Seg_2184" s="T226">go-PST-3PL</ta>
            <ta e="T228" id="Seg_2185" s="T227">play-DUR.[3SG]</ta>
            <ta e="T229" id="Seg_2186" s="T228">all</ta>
            <ta e="T230" id="Seg_2187" s="T229">people.[NOM.SG]</ta>
            <ta e="T231" id="Seg_2188" s="T230">come-PST-3PL</ta>
            <ta e="T232" id="Seg_2189" s="T231">then</ta>
            <ta e="T233" id="Seg_2190" s="T232">and</ta>
            <ta e="T234" id="Seg_2191" s="T233">girl.[NOM.SG]</ta>
            <ta e="T236" id="Seg_2192" s="T235">outwards</ta>
            <ta e="T237" id="Seg_2193" s="T236">come-PST.[3SG]</ta>
            <ta e="T238" id="Seg_2194" s="T237">then</ta>
            <ta e="T239" id="Seg_2195" s="T238">bird.[NOM.SG]</ta>
            <ta e="T240" id="Seg_2196" s="T239">this-ACC</ta>
            <ta e="T241" id="Seg_2197" s="T240">grab-CVB</ta>
            <ta e="T242" id="Seg_2198" s="T241">bring-RES-PST.[3SG]</ta>
            <ta e="T243" id="Seg_2199" s="T242">this.[NOM.SG]</ta>
            <ta e="T244" id="Seg_2200" s="T243">man-LAT</ta>
            <ta e="T245" id="Seg_2201" s="T244">then</ta>
            <ta e="T247" id="Seg_2202" s="T246">live-PST-3PL</ta>
            <ta e="T248" id="Seg_2203" s="T247">live-PST-3PL</ta>
            <ta e="T249" id="Seg_2204" s="T248">then</ta>
            <ta e="T250" id="Seg_2205" s="T249">this.[NOM.SG]</ta>
            <ta e="T251" id="Seg_2206" s="T250">this-LAT</ta>
            <ta e="T252" id="Seg_2207" s="T251">man-LAT</ta>
            <ta e="T253" id="Seg_2208" s="T252">walk-PRS.[3SG]</ta>
            <ta e="T254" id="Seg_2209" s="T253">girl.[NOM.SG]</ta>
            <ta e="T255" id="Seg_2210" s="T254">go-PST.[3SG]</ta>
            <ta e="T256" id="Seg_2211" s="T255">water-LAT</ta>
            <ta e="T257" id="Seg_2212" s="T256">there</ta>
            <ta e="T258" id="Seg_2213" s="T257">woman.[NOM.SG]</ta>
            <ta e="T259" id="Seg_2214" s="T258">stand-DUR.[3SG]</ta>
            <ta e="T260" id="Seg_2215" s="T259">go-OPT.DU/PL-1DU</ta>
            <ta e="T261" id="Seg_2216" s="T260">tent-LAT/LOC.1SG</ta>
            <ta e="T262" id="Seg_2217" s="T261">father-LAT/LOC.3SG</ta>
            <ta e="T263" id="Seg_2218" s="T262">I.NOM</ta>
            <ta e="T264" id="Seg_2219" s="T263">go-FUT-1SG</ta>
            <ta e="T265" id="Seg_2220" s="T264">ask-FUT-1SG</ta>
            <ta e="T266" id="Seg_2221" s="T265">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T267" id="Seg_2222" s="T266">man-GEN</ta>
            <ta e="T268" id="Seg_2223" s="T267">this.[NOM.SG]</ta>
            <ta e="T269" id="Seg_2224" s="T268">I.LAT</ta>
            <ta e="T270" id="Seg_2225" s="T269">say-FUT-3SG</ta>
            <ta e="T271" id="Seg_2226" s="T270">go-EP-IMP.2SG</ta>
            <ta e="T272" id="Seg_2227" s="T271">so</ta>
            <ta e="T273" id="Seg_2228" s="T272">go-FUT-1SG</ta>
            <ta e="T274" id="Seg_2229" s="T273">then</ta>
            <ta e="T275" id="Seg_2230" s="T274">come-PST.[3SG]</ta>
            <ta e="T276" id="Seg_2231" s="T275">%snake.[NOM.SG]</ta>
            <ta e="T277" id="Seg_2232" s="T276">this.[NOM.SG]</ta>
            <ta e="T278" id="Seg_2233" s="T277">this.[NOM.SG]</ta>
            <ta e="T279" id="Seg_2234" s="T278">woman-NOM/GEN/ACC.1SG</ta>
            <ta e="T282" id="Seg_2235" s="T281">bite-MOM-PST.[3SG]</ta>
            <ta e="T283" id="Seg_2236" s="T282">and</ta>
            <ta e="T284" id="Seg_2237" s="T283">this.[NOM.SG]</ta>
            <ta e="T285" id="Seg_2238" s="T284">woman.[NOM.SG]</ta>
            <ta e="T286" id="Seg_2239" s="T285">go-EP-IMP.2SG</ta>
            <ta e="T287" id="Seg_2240" s="T286">father-LAT/LOC.3SG</ta>
            <ta e="T288" id="Seg_2241" s="T287">and</ta>
            <ta e="T289" id="Seg_2242" s="T288">tell-IMP.2SG.O</ta>
            <ta e="T290" id="Seg_2243" s="T289">this-GEN</ta>
            <ta e="T291" id="Seg_2244" s="T290">girl.[NOM.SG]</ta>
            <ta e="T292" id="Seg_2245" s="T291">go-FUT-3SG</ta>
            <ta e="T293" id="Seg_2246" s="T292">this.[NOM.SG]</ta>
            <ta e="T294" id="Seg_2247" s="T293">man-LAT</ta>
            <ta e="T295" id="Seg_2248" s="T294">hunt-DUR.[3SG]</ta>
            <ta e="T296" id="Seg_2249" s="T295">then</ta>
            <ta e="T297" id="Seg_2250" s="T296">this.[NOM.SG]</ta>
            <ta e="T298" id="Seg_2251" s="T297">go-PST.[3SG]</ta>
            <ta e="T299" id="Seg_2252" s="T298">father-NOM/GEN.3SG</ta>
            <ta e="T300" id="Seg_2253" s="T299">sit-PST.[3SG]</ta>
            <ta e="T301" id="Seg_2254" s="T300">sit-PST.[3SG]</ta>
            <ta e="T302" id="Seg_2255" s="T301">say-IPFVZ.[3SG]</ta>
            <ta e="T303" id="Seg_2256" s="T302">JUSS</ta>
            <ta e="T304" id="Seg_2257" s="T303">go-IMP-3SG</ta>
            <ta e="T305" id="Seg_2258" s="T304">then</ta>
            <ta e="T306" id="Seg_2259" s="T305">give-PST.[3SG]</ta>
            <ta e="T307" id="Seg_2260" s="T306">this-LAT</ta>
            <ta e="T308" id="Seg_2261" s="T307">many</ta>
            <ta e="T309" id="Seg_2262" s="T308">this.[NOM.SG]</ta>
            <ta e="T310" id="Seg_2263" s="T309">come-PST.[3SG]</ta>
            <ta e="T311" id="Seg_2264" s="T310">tell-PST.[3SG]</ta>
            <ta e="T312" id="Seg_2265" s="T311">then</ta>
            <ta e="T313" id="Seg_2266" s="T312">this-PL</ta>
            <ta e="T314" id="Seg_2267" s="T313">vodka.[NOM.SG]</ta>
            <ta e="T315" id="Seg_2268" s="T314">drink-PST-3PL</ta>
            <ta e="T316" id="Seg_2269" s="T315">PTCL</ta>
            <ta e="T317" id="Seg_2270" s="T316">then</ta>
            <ta e="T318" id="Seg_2271" s="T317">father-NOM/GEN.3SG</ta>
            <ta e="T319" id="Seg_2272" s="T318">come-IMP.2PL</ta>
            <ta e="T320" id="Seg_2273" s="T319">we.LAT</ta>
            <ta e="T321" id="Seg_2274" s="T320">this-PL</ta>
            <ta e="T322" id="Seg_2275" s="T321">NEG</ta>
            <ta e="T323" id="Seg_2276" s="T322">go-PST-3PL</ta>
            <ta e="T324" id="Seg_2277" s="T323">then</ta>
            <ta e="T327" id="Seg_2278" s="T326">this-PL-LAT</ta>
            <ta e="T328" id="Seg_2279" s="T327">PTCL</ta>
            <ta e="T329" id="Seg_2280" s="T328">house.[NOM.SG]</ta>
            <ta e="T330" id="Seg_2281" s="T329">cut-PST.[3SG]</ta>
            <ta e="T331" id="Seg_2282" s="T330">all</ta>
            <ta e="T332" id="Seg_2283" s="T331">what.[NOM.SG]</ta>
            <ta e="T333" id="Seg_2284" s="T332">give-PST.[3SG]</ta>
            <ta e="T334" id="Seg_2285" s="T333">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T90" id="Seg_2286" s="T89">один.[NOM.SG]</ta>
            <ta e="T91" id="Seg_2287" s="T90">мужчина.[NOM.SG]</ta>
            <ta e="T92" id="Seg_2288" s="T91">пойти-PST.[3SG]</ta>
            <ta e="T93" id="Seg_2289" s="T92">охотиться-IPFVZ-INF.LAT</ta>
            <ta e="T94" id="Seg_2290" s="T93">прийти-PRS.[3SG]</ta>
            <ta e="T95" id="Seg_2291" s="T94">бежать-DUR.[3SG]</ta>
            <ta e="T96" id="Seg_2292" s="T95">волк.[NOM.SG]</ta>
            <ta e="T97" id="Seg_2293" s="T96">этот-LAT</ta>
            <ta e="T98" id="Seg_2294" s="T97">этот.[NOM.SG]</ta>
            <ta e="T99" id="Seg_2295" s="T98">хотеть.PST.M.SG</ta>
            <ta e="T100" id="Seg_2296" s="T99">этот-ACC</ta>
            <ta e="T101" id="Seg_2297" s="T100">стрелять-IPFVZ-INF.LAT</ta>
            <ta e="T102" id="Seg_2298" s="T101">этот.[NOM.SG]</ta>
            <ta e="T103" id="Seg_2299" s="T102">сказать-IPFVZ.[3SG]</ta>
            <ta e="T104" id="Seg_2300" s="T103">NEG.AUX-IMP.2SG</ta>
            <ta e="T105" id="Seg_2301" s="T104">стрелять-IPFVZ-CNG</ta>
            <ta e="T106" id="Seg_2302" s="T105">я.ACC</ta>
            <ta e="T107" id="Seg_2303" s="T106">я.NOM</ta>
            <ta e="T108" id="Seg_2304" s="T107">ты.GEN</ta>
            <ta e="T109" id="Seg_2305" s="T108">ты.NOM-INS</ta>
            <ta e="T110" id="Seg_2306" s="T109">ты.GEN</ta>
            <ta e="T111" id="Seg_2307" s="T110">товарищ-ACC.3SG</ta>
            <ta e="T112" id="Seg_2308" s="T111">стать-FUT-1SG</ta>
            <ta e="T113" id="Seg_2309" s="T112">пойти-PST-3PL</ta>
            <ta e="T114" id="Seg_2310" s="T113">два-COLL</ta>
            <ta e="T116" id="Seg_2311" s="T114">тогда</ta>
            <ta e="T117" id="Seg_2312" s="T116">тогда</ta>
            <ta e="T118" id="Seg_2313" s="T117">прийти-PRS.[3SG]</ta>
            <ta e="T119" id="Seg_2314" s="T118">два-COLL-PL</ta>
            <ta e="T120" id="Seg_2315" s="T119">тогда</ta>
            <ta e="T121" id="Seg_2316" s="T120">свинья.[NOM.SG]</ta>
            <ta e="T122" id="Seg_2317" s="T121">прийти-PRS.[3SG]</ta>
            <ta e="T123" id="Seg_2318" s="T122">этот.[NOM.SG]</ta>
            <ta e="T125" id="Seg_2319" s="T124">хотеть.PST.M.SG</ta>
            <ta e="T126" id="Seg_2320" s="T125">этот-LAT</ta>
            <ta e="T127" id="Seg_2321" s="T126">стрелять-IPFVZ-INF.LAT</ta>
            <ta e="T128" id="Seg_2322" s="T127">этот.[NOM.SG]</ta>
            <ta e="T129" id="Seg_2323" s="T128">сказать-IPFVZ.[3SG]</ta>
            <ta e="T130" id="Seg_2324" s="T129">NEG.AUX-IMP.2SG</ta>
            <ta e="T131" id="Seg_2325" s="T130">стрелять-IPFVZ-CNG</ta>
            <ta e="T132" id="Seg_2326" s="T131">я.ACC</ta>
            <ta e="T133" id="Seg_2327" s="T132">товарищ-PL-ACC.3SG</ta>
            <ta e="T134" id="Seg_2328" s="T133">стать-FUT-1PL</ta>
            <ta e="T135" id="Seg_2329" s="T134">пойти-PST-3PL</ta>
            <ta e="T136" id="Seg_2330" s="T135">три-COLL</ta>
            <ta e="T137" id="Seg_2331" s="T136">тогда</ta>
            <ta e="T138" id="Seg_2332" s="T137">прийти-PRS-3PL</ta>
            <ta e="T139" id="Seg_2333" s="T138">прийти-PRS-3PL</ta>
            <ta e="T140" id="Seg_2334" s="T139">лисичка.[NOM.SG]</ta>
            <ta e="T141" id="Seg_2335" s="T140">бежать-DUR.[3SG]</ta>
            <ta e="T142" id="Seg_2336" s="T141">этот.[NOM.SG]</ta>
            <ta e="T143" id="Seg_2337" s="T142">этот-ACC</ta>
            <ta e="T144" id="Seg_2338" s="T143">хотеть.PST.M.SG</ta>
            <ta e="T145" id="Seg_2339" s="T144">стрелять-IPFVZ-INF.LAT</ta>
            <ta e="T146" id="Seg_2340" s="T145">этот.[NOM.SG]</ta>
            <ta e="T147" id="Seg_2341" s="T146">NEG.AUX-IMP.2SG</ta>
            <ta e="T148" id="Seg_2342" s="T147">стрелять-IMP.2SG.O</ta>
            <ta e="T149" id="Seg_2343" s="T148">я.ACC</ta>
            <ta e="T150" id="Seg_2344" s="T149">я.NOM</ta>
            <ta e="T151" id="Seg_2345" s="T150">товарищ.[NOM.SG]</ta>
            <ta e="T152" id="Seg_2346" s="T151">ты.GEN</ta>
            <ta e="T154" id="Seg_2347" s="T153">стать-FUT-1SG</ta>
            <ta e="T155" id="Seg_2348" s="T154">тогда</ta>
            <ta e="T157" id="Seg_2349" s="T156">пойти-PST-3PL</ta>
            <ta e="T158" id="Seg_2350" s="T157">прийти-PRS-3PL</ta>
            <ta e="T159" id="Seg_2351" s="T158">прийти-PRS-3PL</ta>
            <ta e="T160" id="Seg_2352" s="T159">птица.[NOM.SG]</ta>
            <ta e="T161" id="Seg_2353" s="T160">лететь-DUR.[3SG]</ta>
            <ta e="T162" id="Seg_2354" s="T161">PTCL</ta>
            <ta e="T163" id="Seg_2355" s="T162">этот-ACC</ta>
            <ta e="T164" id="Seg_2356" s="T163">хотеть.PST.M.SG</ta>
            <ta e="T166" id="Seg_2357" s="T165">стрелять-IPFVZ-INF.LAT</ta>
            <ta e="T167" id="Seg_2358" s="T166">PTCL</ta>
            <ta e="T168" id="Seg_2359" s="T167">этот.[NOM.SG]</ta>
            <ta e="T169" id="Seg_2360" s="T168">сказать-IPFVZ.[3SG]</ta>
            <ta e="T170" id="Seg_2361" s="T169">NEG.AUX-IMP.2SG</ta>
            <ta e="T171" id="Seg_2362" s="T170">стрелять-IPFVZ-CNG</ta>
            <ta e="T172" id="Seg_2363" s="T171">я.ACC</ta>
            <ta e="T173" id="Seg_2364" s="T172">я.NOM</ta>
            <ta e="T174" id="Seg_2365" s="T173">я.NOM</ta>
            <ta e="T175" id="Seg_2366" s="T174">ты.GEN</ta>
            <ta e="T176" id="Seg_2367" s="T175">товарищ.[NOM.SG]</ta>
            <ta e="T177" id="Seg_2368" s="T176">стать-FUT-1SG</ta>
            <ta e="T178" id="Seg_2369" s="T177">тогда</ta>
            <ta e="T179" id="Seg_2370" s="T178">прийти-PRS-3PL</ta>
            <ta e="T180" id="Seg_2371" s="T179">%змея.[NOM.SG]</ta>
            <ta e="T181" id="Seg_2372" s="T180">лежать-DUR.[3SG]</ta>
            <ta e="T182" id="Seg_2373" s="T181">этот.[NOM.SG]</ta>
            <ta e="T183" id="Seg_2374" s="T182">этот-ACC</ta>
            <ta e="T184" id="Seg_2375" s="T183">хотеть.PST.M.SG</ta>
            <ta e="T185" id="Seg_2376" s="T184">стрелять-IPFVZ-INF.LAT</ta>
            <ta e="T186" id="Seg_2377" s="T185">NEG.AUX-IMP.2SG</ta>
            <ta e="T187" id="Seg_2378" s="T186">стрелять-IPFVZ-CNG</ta>
            <ta e="T188" id="Seg_2379" s="T187">я.ACC</ta>
            <ta e="T189" id="Seg_2380" s="T188">я.NOM</ta>
            <ta e="T191" id="Seg_2381" s="T190">я.NOM</ta>
            <ta e="T192" id="Seg_2382" s="T191">ты.GEN</ta>
            <ta e="T193" id="Seg_2383" s="T192">товарищ-NOM/GEN/ACC.2SG</ta>
            <ta e="T194" id="Seg_2384" s="T193">стать-FUT-1SG</ta>
            <ta e="T195" id="Seg_2385" s="T194">тогда</ta>
            <ta e="T196" id="Seg_2386" s="T195">прийти-PST-3PL</ta>
            <ta e="T197" id="Seg_2387" s="T196">гора-LAT</ta>
            <ta e="T198" id="Seg_2388" s="T197">там</ta>
            <ta e="T199" id="Seg_2389" s="T198">видеть-PST-3PL</ta>
            <ta e="T200" id="Seg_2390" s="T199">отверстие.[NOM.SG]</ta>
            <ta e="T201" id="Seg_2391" s="T200">там</ta>
            <ta e="T202" id="Seg_2392" s="T201">там</ta>
            <ta e="T203" id="Seg_2393" s="T202">жить-DUR.[3SG]</ta>
            <ta e="T204" id="Seg_2394" s="T203">и</ta>
            <ta e="T205" id="Seg_2395" s="T204">весь</ta>
            <ta e="T206" id="Seg_2396" s="T205">что.[NOM.SG]</ta>
            <ta e="T207" id="Seg_2397" s="T206">быть-PRS.[3SG]</ta>
            <ta e="T208" id="Seg_2398" s="T207">съесть-INF.LAT</ta>
            <ta e="T209" id="Seg_2399" s="T208">тогда</ta>
            <ta e="T210" id="Seg_2400" s="T209">сказать-IPFVZ-3PL</ta>
            <ta e="T211" id="Seg_2401" s="T210">товарищ-PL</ta>
            <ta e="T212" id="Seg_2402" s="T211">мы.GEN</ta>
            <ta e="T213" id="Seg_2403" s="T212">мужчина-GEN</ta>
            <ta e="T214" id="Seg_2404" s="T213">надо</ta>
            <ta e="T215" id="Seg_2405" s="T214">жениться-INF.LAT</ta>
            <ta e="T216" id="Seg_2406" s="T215">царский.[NOM.SG]</ta>
            <ta e="T218" id="Seg_2407" s="T217">девушка.[NOM.SG]</ta>
            <ta e="T219" id="Seg_2408" s="T218">взять-FUT-1PL</ta>
            <ta e="T220" id="Seg_2409" s="T219">тогда</ta>
            <ta e="T221" id="Seg_2410" s="T220">пойти-PST-3PL</ta>
            <ta e="T222" id="Seg_2411" s="T221">свинья.[NOM.SG]</ta>
            <ta e="T223" id="Seg_2412" s="T222">и</ta>
            <ta e="T224" id="Seg_2413" s="T223">лисица.[NOM.SG]</ta>
            <ta e="T225" id="Seg_2414" s="T224">и</ta>
            <ta e="T226" id="Seg_2415" s="T225">волк.[NOM.SG]</ta>
            <ta e="T227" id="Seg_2416" s="T226">пойти-PST-3PL</ta>
            <ta e="T228" id="Seg_2417" s="T227">играть-DUR.[3SG]</ta>
            <ta e="T229" id="Seg_2418" s="T228">весь</ta>
            <ta e="T230" id="Seg_2419" s="T229">люди.[NOM.SG]</ta>
            <ta e="T231" id="Seg_2420" s="T230">прийти-PST-3PL</ta>
            <ta e="T232" id="Seg_2421" s="T231">тогда</ta>
            <ta e="T233" id="Seg_2422" s="T232">и</ta>
            <ta e="T234" id="Seg_2423" s="T233">девушка.[NOM.SG]</ta>
            <ta e="T236" id="Seg_2424" s="T235">наружу</ta>
            <ta e="T237" id="Seg_2425" s="T236">прийти-PST.[3SG]</ta>
            <ta e="T238" id="Seg_2426" s="T237">тогда</ta>
            <ta e="T239" id="Seg_2427" s="T238">птица.[NOM.SG]</ta>
            <ta e="T240" id="Seg_2428" s="T239">этот-ACC</ta>
            <ta e="T241" id="Seg_2429" s="T240">хватать-CVB</ta>
            <ta e="T242" id="Seg_2430" s="T241">нести-RES-PST.[3SG]</ta>
            <ta e="T243" id="Seg_2431" s="T242">этот.[NOM.SG]</ta>
            <ta e="T244" id="Seg_2432" s="T243">мужчина-LAT</ta>
            <ta e="T245" id="Seg_2433" s="T244">тогда</ta>
            <ta e="T247" id="Seg_2434" s="T246">жить-PST-3PL</ta>
            <ta e="T248" id="Seg_2435" s="T247">жить-PST-3PL</ta>
            <ta e="T249" id="Seg_2436" s="T248">тогда</ta>
            <ta e="T250" id="Seg_2437" s="T249">этот.[NOM.SG]</ta>
            <ta e="T251" id="Seg_2438" s="T250">этот-LAT</ta>
            <ta e="T252" id="Seg_2439" s="T251">мужчина-LAT</ta>
            <ta e="T253" id="Seg_2440" s="T252">идти-PRS.[3SG]</ta>
            <ta e="T254" id="Seg_2441" s="T253">девушка.[NOM.SG]</ta>
            <ta e="T255" id="Seg_2442" s="T254">пойти-PST.[3SG]</ta>
            <ta e="T256" id="Seg_2443" s="T255">вода-LAT</ta>
            <ta e="T257" id="Seg_2444" s="T256">там</ta>
            <ta e="T258" id="Seg_2445" s="T257">женщина.[NOM.SG]</ta>
            <ta e="T259" id="Seg_2446" s="T258">стоять-DUR.[3SG]</ta>
            <ta e="T260" id="Seg_2447" s="T259">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T261" id="Seg_2448" s="T260">чум-LAT/LOC.1SG</ta>
            <ta e="T262" id="Seg_2449" s="T261">отец-LAT/LOC.3SG</ta>
            <ta e="T263" id="Seg_2450" s="T262">я.NOM</ta>
            <ta e="T264" id="Seg_2451" s="T263">пойти-FUT-1SG</ta>
            <ta e="T265" id="Seg_2452" s="T264">спросить-FUT-1SG</ta>
            <ta e="T266" id="Seg_2453" s="T265">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T267" id="Seg_2454" s="T266">мужчина-GEN</ta>
            <ta e="T268" id="Seg_2455" s="T267">этот.[NOM.SG]</ta>
            <ta e="T269" id="Seg_2456" s="T268">я.LAT</ta>
            <ta e="T270" id="Seg_2457" s="T269">сказать-FUT-3SG</ta>
            <ta e="T271" id="Seg_2458" s="T270">пойти-EP-IMP.2SG</ta>
            <ta e="T272" id="Seg_2459" s="T271">так</ta>
            <ta e="T273" id="Seg_2460" s="T272">пойти-FUT-1SG</ta>
            <ta e="T274" id="Seg_2461" s="T273">тогда</ta>
            <ta e="T275" id="Seg_2462" s="T274">прийти-PST.[3SG]</ta>
            <ta e="T276" id="Seg_2463" s="T275">%snake.[NOM.SG]</ta>
            <ta e="T277" id="Seg_2464" s="T276">этот.[NOM.SG]</ta>
            <ta e="T278" id="Seg_2465" s="T277">этот.[NOM.SG]</ta>
            <ta e="T279" id="Seg_2466" s="T278">женщина-NOM/GEN/ACC.1SG</ta>
            <ta e="T282" id="Seg_2467" s="T281">укусить-MOM-PST.[3SG]</ta>
            <ta e="T283" id="Seg_2468" s="T282">а</ta>
            <ta e="T284" id="Seg_2469" s="T283">этот.[NOM.SG]</ta>
            <ta e="T285" id="Seg_2470" s="T284">женщина.[NOM.SG]</ta>
            <ta e="T286" id="Seg_2471" s="T285">пойти-EP-IMP.2SG</ta>
            <ta e="T287" id="Seg_2472" s="T286">отец-LAT/LOC.3SG</ta>
            <ta e="T288" id="Seg_2473" s="T287">и</ta>
            <ta e="T289" id="Seg_2474" s="T288">сказать-IMP.2SG.O</ta>
            <ta e="T290" id="Seg_2475" s="T289">этот-GEN</ta>
            <ta e="T291" id="Seg_2476" s="T290">девушка.[NOM.SG]</ta>
            <ta e="T292" id="Seg_2477" s="T291">пойти-FUT-3SG</ta>
            <ta e="T293" id="Seg_2478" s="T292">этот.[NOM.SG]</ta>
            <ta e="T294" id="Seg_2479" s="T293">мужчина-LAT</ta>
            <ta e="T295" id="Seg_2480" s="T294">охотиться-DUR.[3SG]</ta>
            <ta e="T296" id="Seg_2481" s="T295">тогда</ta>
            <ta e="T297" id="Seg_2482" s="T296">этот.[NOM.SG]</ta>
            <ta e="T298" id="Seg_2483" s="T297">пойти-PST.[3SG]</ta>
            <ta e="T299" id="Seg_2484" s="T298">отец-NOM/GEN.3SG</ta>
            <ta e="T300" id="Seg_2485" s="T299">сидеть-PST.[3SG]</ta>
            <ta e="T301" id="Seg_2486" s="T300">сидеть-PST.[3SG]</ta>
            <ta e="T302" id="Seg_2487" s="T301">сказать-IPFVZ.[3SG]</ta>
            <ta e="T303" id="Seg_2488" s="T302">JUSS</ta>
            <ta e="T304" id="Seg_2489" s="T303">пойти-IMP-3SG</ta>
            <ta e="T305" id="Seg_2490" s="T304">тогда</ta>
            <ta e="T306" id="Seg_2491" s="T305">дать-PST.[3SG]</ta>
            <ta e="T307" id="Seg_2492" s="T306">этот-LAT</ta>
            <ta e="T308" id="Seg_2493" s="T307">много</ta>
            <ta e="T309" id="Seg_2494" s="T308">этот.[NOM.SG]</ta>
            <ta e="T310" id="Seg_2495" s="T309">прийти-PST.[3SG]</ta>
            <ta e="T311" id="Seg_2496" s="T310">сказать-PST.[3SG]</ta>
            <ta e="T312" id="Seg_2497" s="T311">тогда</ta>
            <ta e="T313" id="Seg_2498" s="T312">этот-PL</ta>
            <ta e="T314" id="Seg_2499" s="T313">водка.[NOM.SG]</ta>
            <ta e="T315" id="Seg_2500" s="T314">пить-PST-3PL</ta>
            <ta e="T316" id="Seg_2501" s="T315">PTCL</ta>
            <ta e="T317" id="Seg_2502" s="T316">тогда</ta>
            <ta e="T318" id="Seg_2503" s="T317">отец-NOM/GEN.3SG</ta>
            <ta e="T319" id="Seg_2504" s="T318">прийти-IMP.2PL</ta>
            <ta e="T320" id="Seg_2505" s="T319">мы.LAT</ta>
            <ta e="T321" id="Seg_2506" s="T320">этот-PL</ta>
            <ta e="T322" id="Seg_2507" s="T321">NEG</ta>
            <ta e="T323" id="Seg_2508" s="T322">пойти-PST-3PL</ta>
            <ta e="T324" id="Seg_2509" s="T323">тогда</ta>
            <ta e="T327" id="Seg_2510" s="T326">этот-PL-LAT</ta>
            <ta e="T328" id="Seg_2511" s="T327">PTCL</ta>
            <ta e="T329" id="Seg_2512" s="T328">дом.[NOM.SG]</ta>
            <ta e="T330" id="Seg_2513" s="T329">резать-PST.[3SG]</ta>
            <ta e="T331" id="Seg_2514" s="T330">весь</ta>
            <ta e="T332" id="Seg_2515" s="T331">что.[NOM.SG]</ta>
            <ta e="T333" id="Seg_2516" s="T332">дать-PST.[3SG]</ta>
            <ta e="T334" id="Seg_2517" s="T333">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T90" id="Seg_2518" s="T89">num-n:case</ta>
            <ta e="T91" id="Seg_2519" s="T90">n-n:case</ta>
            <ta e="T92" id="Seg_2520" s="T91">v-v:tense-v:pn</ta>
            <ta e="T93" id="Seg_2521" s="T92">v-v&gt;v-v:n.fin</ta>
            <ta e="T94" id="Seg_2522" s="T93">v-v:tense-v:pn</ta>
            <ta e="T95" id="Seg_2523" s="T94">v-v&gt;v-v:pn</ta>
            <ta e="T96" id="Seg_2524" s="T95">n-n:case</ta>
            <ta e="T97" id="Seg_2525" s="T96">dempro-n:case</ta>
            <ta e="T98" id="Seg_2526" s="T97">dempro-n:case</ta>
            <ta e="T99" id="Seg_2527" s="T98">v</ta>
            <ta e="T100" id="Seg_2528" s="T99">dempro-n:case</ta>
            <ta e="T101" id="Seg_2529" s="T100">v-v&gt;v-v:n.fin</ta>
            <ta e="T102" id="Seg_2530" s="T101">dempro-n:case</ta>
            <ta e="T103" id="Seg_2531" s="T102">v-v&gt;v-v:pn</ta>
            <ta e="T104" id="Seg_2532" s="T103">aux-v:mood.pn</ta>
            <ta e="T105" id="Seg_2533" s="T104">v-v&gt;v-v:n.fin</ta>
            <ta e="T106" id="Seg_2534" s="T105">pers</ta>
            <ta e="T107" id="Seg_2535" s="T106">pers</ta>
            <ta e="T108" id="Seg_2536" s="T107">pers</ta>
            <ta e="T109" id="Seg_2537" s="T108">pers-n:case</ta>
            <ta e="T110" id="Seg_2538" s="T109">pers</ta>
            <ta e="T111" id="Seg_2539" s="T110">n-n:case.poss</ta>
            <ta e="T112" id="Seg_2540" s="T111">v-v:tense-v:pn</ta>
            <ta e="T113" id="Seg_2541" s="T112">v-v:tense-v:pn</ta>
            <ta e="T114" id="Seg_2542" s="T113">num-num&gt;num</ta>
            <ta e="T116" id="Seg_2543" s="T114">adv</ta>
            <ta e="T117" id="Seg_2544" s="T116">adv</ta>
            <ta e="T118" id="Seg_2545" s="T117">v-v:tense-v:pn</ta>
            <ta e="T119" id="Seg_2546" s="T118">num-num&gt;num-n:num</ta>
            <ta e="T120" id="Seg_2547" s="T119">adv</ta>
            <ta e="T121" id="Seg_2548" s="T120">n-n:case</ta>
            <ta e="T122" id="Seg_2549" s="T121">v-v:tense-v:pn</ta>
            <ta e="T123" id="Seg_2550" s="T122">dempro-n:case</ta>
            <ta e="T125" id="Seg_2551" s="T124">v</ta>
            <ta e="T126" id="Seg_2552" s="T125">dempro-n:case</ta>
            <ta e="T127" id="Seg_2553" s="T126">v-v&gt;v-v:n.fin</ta>
            <ta e="T128" id="Seg_2554" s="T127">dempro-n:case</ta>
            <ta e="T129" id="Seg_2555" s="T128">v-v&gt;v-v:pn</ta>
            <ta e="T130" id="Seg_2556" s="T129">aux-v:mood.pn</ta>
            <ta e="T131" id="Seg_2557" s="T130">v-v&gt;v-v:n.fin</ta>
            <ta e="T132" id="Seg_2558" s="T131">pers</ta>
            <ta e="T133" id="Seg_2559" s="T132">n-n:num-n:case.poss</ta>
            <ta e="T134" id="Seg_2560" s="T133">v-v:tense-v:pn</ta>
            <ta e="T135" id="Seg_2561" s="T134">v-v:tense-v:pn</ta>
            <ta e="T136" id="Seg_2562" s="T135">num-num&gt;num</ta>
            <ta e="T137" id="Seg_2563" s="T136">adv</ta>
            <ta e="T138" id="Seg_2564" s="T137">v-v:tense-v:pn</ta>
            <ta e="T139" id="Seg_2565" s="T138">v-v:tense-v:pn</ta>
            <ta e="T140" id="Seg_2566" s="T139">n-n:case</ta>
            <ta e="T141" id="Seg_2567" s="T140">v-v&gt;v-v:pn</ta>
            <ta e="T142" id="Seg_2568" s="T141">dempro-n:case</ta>
            <ta e="T143" id="Seg_2569" s="T142">dempro-n:case</ta>
            <ta e="T144" id="Seg_2570" s="T143">v</ta>
            <ta e="T145" id="Seg_2571" s="T144">v-v&gt;v-v:n.fin</ta>
            <ta e="T146" id="Seg_2572" s="T145">dempro-n:case</ta>
            <ta e="T147" id="Seg_2573" s="T146">aux-v:mood.pn</ta>
            <ta e="T148" id="Seg_2574" s="T147">v-v:mood.pn</ta>
            <ta e="T149" id="Seg_2575" s="T148">pers</ta>
            <ta e="T150" id="Seg_2576" s="T149">pers</ta>
            <ta e="T151" id="Seg_2577" s="T150">n-n:case</ta>
            <ta e="T152" id="Seg_2578" s="T151">pers</ta>
            <ta e="T154" id="Seg_2579" s="T153">v-v:tense-v:pn</ta>
            <ta e="T155" id="Seg_2580" s="T154">adv</ta>
            <ta e="T157" id="Seg_2581" s="T156">v-v:tense-v:pn</ta>
            <ta e="T158" id="Seg_2582" s="T157">v-v:tense-v:pn</ta>
            <ta e="T159" id="Seg_2583" s="T158">v-v:tense-v:pn</ta>
            <ta e="T160" id="Seg_2584" s="T159">n-n:case</ta>
            <ta e="T161" id="Seg_2585" s="T160">v-v&gt;v-v:pn</ta>
            <ta e="T162" id="Seg_2586" s="T161">ptcl</ta>
            <ta e="T163" id="Seg_2587" s="T162">dempro-n:case</ta>
            <ta e="T164" id="Seg_2588" s="T163">v</ta>
            <ta e="T166" id="Seg_2589" s="T165">v-v&gt;v-v:n.fin</ta>
            <ta e="T167" id="Seg_2590" s="T166">ptcl</ta>
            <ta e="T168" id="Seg_2591" s="T167">dempro-n:case</ta>
            <ta e="T169" id="Seg_2592" s="T168">v-v&gt;v-v:pn</ta>
            <ta e="T170" id="Seg_2593" s="T169">aux-v:mood.pn</ta>
            <ta e="T171" id="Seg_2594" s="T170">v-v&gt;v-v:n.fin</ta>
            <ta e="T172" id="Seg_2595" s="T171">pers</ta>
            <ta e="T173" id="Seg_2596" s="T172">pers</ta>
            <ta e="T174" id="Seg_2597" s="T173">pers</ta>
            <ta e="T175" id="Seg_2598" s="T174">pers</ta>
            <ta e="T176" id="Seg_2599" s="T175">n-n:case</ta>
            <ta e="T177" id="Seg_2600" s="T176">v-v:tense-v:pn</ta>
            <ta e="T178" id="Seg_2601" s="T177">adv</ta>
            <ta e="T179" id="Seg_2602" s="T178">v-v:tense-v:pn</ta>
            <ta e="T180" id="Seg_2603" s="T179">n-n:case</ta>
            <ta e="T181" id="Seg_2604" s="T180">v-v&gt;v-v:pn</ta>
            <ta e="T182" id="Seg_2605" s="T181">dempro-n:case</ta>
            <ta e="T183" id="Seg_2606" s="T182">dempro-n:case</ta>
            <ta e="T184" id="Seg_2607" s="T183">v</ta>
            <ta e="T185" id="Seg_2608" s="T184">v-v&gt;v-v:n.fin</ta>
            <ta e="T186" id="Seg_2609" s="T185">aux-v:mood.pn</ta>
            <ta e="T187" id="Seg_2610" s="T186">v-v&gt;v-v:n.fin</ta>
            <ta e="T188" id="Seg_2611" s="T187">pers</ta>
            <ta e="T189" id="Seg_2612" s="T188">pers</ta>
            <ta e="T191" id="Seg_2613" s="T190">pers</ta>
            <ta e="T192" id="Seg_2614" s="T191">pers</ta>
            <ta e="T193" id="Seg_2615" s="T192">n-n:case.poss</ta>
            <ta e="T194" id="Seg_2616" s="T193">v-v:tense-v:pn</ta>
            <ta e="T195" id="Seg_2617" s="T194">adv</ta>
            <ta e="T196" id="Seg_2618" s="T195">v-v:tense-v:pn</ta>
            <ta e="T197" id="Seg_2619" s="T196">n-n:case</ta>
            <ta e="T198" id="Seg_2620" s="T197">adv</ta>
            <ta e="T199" id="Seg_2621" s="T198">v-v:tense-v:pn</ta>
            <ta e="T200" id="Seg_2622" s="T199">n-n:case</ta>
            <ta e="T201" id="Seg_2623" s="T200">adv</ta>
            <ta e="T202" id="Seg_2624" s="T201">adv</ta>
            <ta e="T203" id="Seg_2625" s="T202">v-v&gt;v-v:pn</ta>
            <ta e="T204" id="Seg_2626" s="T203">conj</ta>
            <ta e="T205" id="Seg_2627" s="T204">quant</ta>
            <ta e="T206" id="Seg_2628" s="T205">que-n:case</ta>
            <ta e="T207" id="Seg_2629" s="T206">v-v:tense-v:pn</ta>
            <ta e="T208" id="Seg_2630" s="T207">v-v:n.fin</ta>
            <ta e="T209" id="Seg_2631" s="T208">adv</ta>
            <ta e="T210" id="Seg_2632" s="T209">v-v&gt;v-v:pn</ta>
            <ta e="T211" id="Seg_2633" s="T210">n-n:num</ta>
            <ta e="T212" id="Seg_2634" s="T211">pers</ta>
            <ta e="T213" id="Seg_2635" s="T212">n-n:case</ta>
            <ta e="T214" id="Seg_2636" s="T213">ptcl</ta>
            <ta e="T215" id="Seg_2637" s="T214">v-v:n.fin</ta>
            <ta e="T216" id="Seg_2638" s="T215">adj-n:case</ta>
            <ta e="T218" id="Seg_2639" s="T217">n-n:case</ta>
            <ta e="T219" id="Seg_2640" s="T218">v-v:tense-v:pn</ta>
            <ta e="T220" id="Seg_2641" s="T219">adv</ta>
            <ta e="T221" id="Seg_2642" s="T220">v-v:tense-v:pn</ta>
            <ta e="T222" id="Seg_2643" s="T221">n-n:case</ta>
            <ta e="T223" id="Seg_2644" s="T222">conj</ta>
            <ta e="T224" id="Seg_2645" s="T223">n-n:case</ta>
            <ta e="T225" id="Seg_2646" s="T224">conj</ta>
            <ta e="T226" id="Seg_2647" s="T225">n-n:case</ta>
            <ta e="T227" id="Seg_2648" s="T226">v-v:tense-v:pn</ta>
            <ta e="T228" id="Seg_2649" s="T227">v-v&gt;v-v:pn</ta>
            <ta e="T229" id="Seg_2650" s="T228">quant</ta>
            <ta e="T230" id="Seg_2651" s="T229">n-n:case</ta>
            <ta e="T231" id="Seg_2652" s="T230">v-v:tense-v:pn</ta>
            <ta e="T232" id="Seg_2653" s="T231">adv</ta>
            <ta e="T233" id="Seg_2654" s="T232">conj</ta>
            <ta e="T234" id="Seg_2655" s="T233">n-n:case</ta>
            <ta e="T236" id="Seg_2656" s="T235">adv</ta>
            <ta e="T237" id="Seg_2657" s="T236">v-v:tense-v:pn</ta>
            <ta e="T238" id="Seg_2658" s="T237">adv</ta>
            <ta e="T239" id="Seg_2659" s="T238">n-n:case</ta>
            <ta e="T240" id="Seg_2660" s="T239">dempro-n:case</ta>
            <ta e="T241" id="Seg_2661" s="T240">v-v:n.fin</ta>
            <ta e="T242" id="Seg_2662" s="T241">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T243" id="Seg_2663" s="T242">dempro-n:case</ta>
            <ta e="T244" id="Seg_2664" s="T243">n-n:case</ta>
            <ta e="T245" id="Seg_2665" s="T244">adv</ta>
            <ta e="T247" id="Seg_2666" s="T246">v-v:tense-v:pn</ta>
            <ta e="T248" id="Seg_2667" s="T247">v-v:tense-v:pn</ta>
            <ta e="T249" id="Seg_2668" s="T248">adv</ta>
            <ta e="T250" id="Seg_2669" s="T249">dempro-n:case</ta>
            <ta e="T251" id="Seg_2670" s="T250">dempro-n:case</ta>
            <ta e="T252" id="Seg_2671" s="T251">n-n:case</ta>
            <ta e="T253" id="Seg_2672" s="T252">v-v:tense-v:pn</ta>
            <ta e="T254" id="Seg_2673" s="T253">n-n:case</ta>
            <ta e="T255" id="Seg_2674" s="T254">v-v:tense-v:pn</ta>
            <ta e="T256" id="Seg_2675" s="T255">n-n:case</ta>
            <ta e="T257" id="Seg_2676" s="T256">adv</ta>
            <ta e="T258" id="Seg_2677" s="T257">n-n:case</ta>
            <ta e="T259" id="Seg_2678" s="T258">v-v&gt;v-v:pn</ta>
            <ta e="T260" id="Seg_2679" s="T259">v-v:mood-v:pn</ta>
            <ta e="T261" id="Seg_2680" s="T260">n-n:case.poss</ta>
            <ta e="T262" id="Seg_2681" s="T261">n-n:case.poss</ta>
            <ta e="T263" id="Seg_2682" s="T262">pers</ta>
            <ta e="T264" id="Seg_2683" s="T263">v-v:tense-v:pn</ta>
            <ta e="T265" id="Seg_2684" s="T264">v-v:tense-v:pn</ta>
            <ta e="T266" id="Seg_2685" s="T265">refl-n:case.poss</ta>
            <ta e="T267" id="Seg_2686" s="T266">n-n:case</ta>
            <ta e="T268" id="Seg_2687" s="T267">dempro-n:case</ta>
            <ta e="T269" id="Seg_2688" s="T268">pers</ta>
            <ta e="T270" id="Seg_2689" s="T269">v-v:tense-v:pn</ta>
            <ta e="T271" id="Seg_2690" s="T270">v-v:ins-v:mood.pn</ta>
            <ta e="T272" id="Seg_2691" s="T271">ptcl</ta>
            <ta e="T273" id="Seg_2692" s="T272">v-v:tense-v:pn</ta>
            <ta e="T274" id="Seg_2693" s="T273">adv</ta>
            <ta e="T275" id="Seg_2694" s="T274">v-v:tense-v:pn</ta>
            <ta e="T276" id="Seg_2695" s="T275">n-n:case</ta>
            <ta e="T277" id="Seg_2696" s="T276">dempro-n:case</ta>
            <ta e="T278" id="Seg_2697" s="T277">dempro-n:case</ta>
            <ta e="T279" id="Seg_2698" s="T278">n-n:case.poss</ta>
            <ta e="T282" id="Seg_2699" s="T281">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T283" id="Seg_2700" s="T282">conj</ta>
            <ta e="T284" id="Seg_2701" s="T283">dempro-n:case</ta>
            <ta e="T285" id="Seg_2702" s="T284">n-n:case</ta>
            <ta e="T286" id="Seg_2703" s="T285">v-v:ins-v:mood.pn</ta>
            <ta e="T287" id="Seg_2704" s="T286">n-n:case.poss</ta>
            <ta e="T288" id="Seg_2705" s="T287">conj</ta>
            <ta e="T289" id="Seg_2706" s="T288">v-v:mood.pn</ta>
            <ta e="T290" id="Seg_2707" s="T289">dempro-n:case</ta>
            <ta e="T291" id="Seg_2708" s="T290">n-n:case</ta>
            <ta e="T292" id="Seg_2709" s="T291">v-v:tense-v:pn</ta>
            <ta e="T293" id="Seg_2710" s="T292">dempro-n:case</ta>
            <ta e="T294" id="Seg_2711" s="T293">n-n:case</ta>
            <ta e="T295" id="Seg_2712" s="T294">v-v&gt;v-v:pn</ta>
            <ta e="T296" id="Seg_2713" s="T295">adv</ta>
            <ta e="T297" id="Seg_2714" s="T296">dempro-n:case</ta>
            <ta e="T298" id="Seg_2715" s="T297">v-v:tense-v:pn</ta>
            <ta e="T299" id="Seg_2716" s="T298">n-n:case.poss</ta>
            <ta e="T300" id="Seg_2717" s="T299">v-v:tense-v:pn</ta>
            <ta e="T301" id="Seg_2718" s="T300">v-v:tense-v:pn</ta>
            <ta e="T302" id="Seg_2719" s="T301">v-v&gt;v-v:pn</ta>
            <ta e="T303" id="Seg_2720" s="T302">ptcl</ta>
            <ta e="T304" id="Seg_2721" s="T303">v-v:mood-v:pn</ta>
            <ta e="T305" id="Seg_2722" s="T304">adv</ta>
            <ta e="T306" id="Seg_2723" s="T305">v-v:tense-v:pn</ta>
            <ta e="T307" id="Seg_2724" s="T306">dempro-n:case</ta>
            <ta e="T308" id="Seg_2725" s="T307">quant</ta>
            <ta e="T309" id="Seg_2726" s="T308">dempro-n:case</ta>
            <ta e="T310" id="Seg_2727" s="T309">v-v:tense-v:pn</ta>
            <ta e="T311" id="Seg_2728" s="T310">v-v:tense-v:pn</ta>
            <ta e="T312" id="Seg_2729" s="T311">adv</ta>
            <ta e="T313" id="Seg_2730" s="T312">dempro-n:num</ta>
            <ta e="T314" id="Seg_2731" s="T313">n-n:case</ta>
            <ta e="T315" id="Seg_2732" s="T314">v-v:tense-v:pn</ta>
            <ta e="T316" id="Seg_2733" s="T315">ptcl</ta>
            <ta e="T317" id="Seg_2734" s="T316">adv</ta>
            <ta e="T318" id="Seg_2735" s="T317">n-n:case.poss</ta>
            <ta e="T319" id="Seg_2736" s="T318">v-v:mood.pn</ta>
            <ta e="T320" id="Seg_2737" s="T319">pers</ta>
            <ta e="T321" id="Seg_2738" s="T320">dempro-n:num</ta>
            <ta e="T322" id="Seg_2739" s="T321">ptcl</ta>
            <ta e="T323" id="Seg_2740" s="T322">v-v:tense-v:pn</ta>
            <ta e="T324" id="Seg_2741" s="T323">adv</ta>
            <ta e="T327" id="Seg_2742" s="T326">dempro-n:num-n:case</ta>
            <ta e="T328" id="Seg_2743" s="T327">ptcl</ta>
            <ta e="T329" id="Seg_2744" s="T328">n-n:case</ta>
            <ta e="T330" id="Seg_2745" s="T329">v-v:tense-v:pn</ta>
            <ta e="T331" id="Seg_2746" s="T330">quant</ta>
            <ta e="T332" id="Seg_2747" s="T331">que-n:case</ta>
            <ta e="T333" id="Seg_2748" s="T332">v-v:tense-v:pn</ta>
            <ta e="T334" id="Seg_2749" s="T333">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T90" id="Seg_2750" s="T89">num</ta>
            <ta e="T91" id="Seg_2751" s="T90">n</ta>
            <ta e="T92" id="Seg_2752" s="T91">v</ta>
            <ta e="T93" id="Seg_2753" s="T92">v</ta>
            <ta e="T94" id="Seg_2754" s="T93">v</ta>
            <ta e="T95" id="Seg_2755" s="T94">v</ta>
            <ta e="T96" id="Seg_2756" s="T95">n</ta>
            <ta e="T97" id="Seg_2757" s="T96">dempro</ta>
            <ta e="T98" id="Seg_2758" s="T97">dempro</ta>
            <ta e="T99" id="Seg_2759" s="T98">v</ta>
            <ta e="T100" id="Seg_2760" s="T99">dempro</ta>
            <ta e="T101" id="Seg_2761" s="T100">v</ta>
            <ta e="T102" id="Seg_2762" s="T101">dempro</ta>
            <ta e="T103" id="Seg_2763" s="T102">v</ta>
            <ta e="T104" id="Seg_2764" s="T103">aux</ta>
            <ta e="T105" id="Seg_2765" s="T104">v</ta>
            <ta e="T106" id="Seg_2766" s="T105">pers</ta>
            <ta e="T107" id="Seg_2767" s="T106">pers</ta>
            <ta e="T108" id="Seg_2768" s="T107">pers</ta>
            <ta e="T109" id="Seg_2769" s="T108">pers</ta>
            <ta e="T110" id="Seg_2770" s="T109">pers</ta>
            <ta e="T111" id="Seg_2771" s="T110">n</ta>
            <ta e="T112" id="Seg_2772" s="T111">v</ta>
            <ta e="T113" id="Seg_2773" s="T112">v</ta>
            <ta e="T114" id="Seg_2774" s="T113">num</ta>
            <ta e="T116" id="Seg_2775" s="T114">adv</ta>
            <ta e="T117" id="Seg_2776" s="T116">adv</ta>
            <ta e="T118" id="Seg_2777" s="T117">v</ta>
            <ta e="T119" id="Seg_2778" s="T118">num</ta>
            <ta e="T120" id="Seg_2779" s="T119">adv</ta>
            <ta e="T121" id="Seg_2780" s="T120">n</ta>
            <ta e="T122" id="Seg_2781" s="T121">v</ta>
            <ta e="T123" id="Seg_2782" s="T122">dempro</ta>
            <ta e="T125" id="Seg_2783" s="T124">v</ta>
            <ta e="T126" id="Seg_2784" s="T125">dempro</ta>
            <ta e="T127" id="Seg_2785" s="T126">v</ta>
            <ta e="T128" id="Seg_2786" s="T127">dempro</ta>
            <ta e="T129" id="Seg_2787" s="T128">v</ta>
            <ta e="T130" id="Seg_2788" s="T129">aux</ta>
            <ta e="T131" id="Seg_2789" s="T130">v</ta>
            <ta e="T132" id="Seg_2790" s="T131">pers</ta>
            <ta e="T133" id="Seg_2791" s="T132">n</ta>
            <ta e="T134" id="Seg_2792" s="T133">v</ta>
            <ta e="T135" id="Seg_2793" s="T134">v</ta>
            <ta e="T136" id="Seg_2794" s="T135">num</ta>
            <ta e="T137" id="Seg_2795" s="T136">adv</ta>
            <ta e="T138" id="Seg_2796" s="T137">v</ta>
            <ta e="T139" id="Seg_2797" s="T138">v</ta>
            <ta e="T140" id="Seg_2798" s="T139">n</ta>
            <ta e="T141" id="Seg_2799" s="T140">v</ta>
            <ta e="T142" id="Seg_2800" s="T141">dempro</ta>
            <ta e="T143" id="Seg_2801" s="T142">dempro</ta>
            <ta e="T144" id="Seg_2802" s="T143">v</ta>
            <ta e="T145" id="Seg_2803" s="T144">v</ta>
            <ta e="T146" id="Seg_2804" s="T145">dempro</ta>
            <ta e="T147" id="Seg_2805" s="T146">aux</ta>
            <ta e="T148" id="Seg_2806" s="T147">v</ta>
            <ta e="T149" id="Seg_2807" s="T148">pers</ta>
            <ta e="T150" id="Seg_2808" s="T149">pers</ta>
            <ta e="T151" id="Seg_2809" s="T150">n</ta>
            <ta e="T152" id="Seg_2810" s="T151">pers</ta>
            <ta e="T154" id="Seg_2811" s="T153">v</ta>
            <ta e="T155" id="Seg_2812" s="T154">adv</ta>
            <ta e="T157" id="Seg_2813" s="T156">v</ta>
            <ta e="T158" id="Seg_2814" s="T157">v</ta>
            <ta e="T159" id="Seg_2815" s="T158">v</ta>
            <ta e="T160" id="Seg_2816" s="T159">n</ta>
            <ta e="T161" id="Seg_2817" s="T160">v</ta>
            <ta e="T162" id="Seg_2818" s="T161">ptcl</ta>
            <ta e="T163" id="Seg_2819" s="T162">dempro</ta>
            <ta e="T164" id="Seg_2820" s="T163">v</ta>
            <ta e="T166" id="Seg_2821" s="T165">v</ta>
            <ta e="T167" id="Seg_2822" s="T166">ptcl</ta>
            <ta e="T168" id="Seg_2823" s="T167">dempro</ta>
            <ta e="T169" id="Seg_2824" s="T168">v</ta>
            <ta e="T170" id="Seg_2825" s="T169">aux</ta>
            <ta e="T171" id="Seg_2826" s="T170">v</ta>
            <ta e="T172" id="Seg_2827" s="T171">pers</ta>
            <ta e="T173" id="Seg_2828" s="T172">pers</ta>
            <ta e="T174" id="Seg_2829" s="T173">pers</ta>
            <ta e="T175" id="Seg_2830" s="T174">pers</ta>
            <ta e="T176" id="Seg_2831" s="T175">n</ta>
            <ta e="T177" id="Seg_2832" s="T176">v</ta>
            <ta e="T178" id="Seg_2833" s="T177">adv</ta>
            <ta e="T179" id="Seg_2834" s="T178">v</ta>
            <ta e="T180" id="Seg_2835" s="T179">n</ta>
            <ta e="T181" id="Seg_2836" s="T180">v</ta>
            <ta e="T182" id="Seg_2837" s="T181">dempro</ta>
            <ta e="T183" id="Seg_2838" s="T182">dempro</ta>
            <ta e="T184" id="Seg_2839" s="T183">v</ta>
            <ta e="T185" id="Seg_2840" s="T184">v</ta>
            <ta e="T186" id="Seg_2841" s="T185">aux</ta>
            <ta e="T187" id="Seg_2842" s="T186">v</ta>
            <ta e="T188" id="Seg_2843" s="T187">pers</ta>
            <ta e="T189" id="Seg_2844" s="T188">pers</ta>
            <ta e="T191" id="Seg_2845" s="T190">pers</ta>
            <ta e="T192" id="Seg_2846" s="T191">pers</ta>
            <ta e="T193" id="Seg_2847" s="T192">n</ta>
            <ta e="T194" id="Seg_2848" s="T193">v</ta>
            <ta e="T195" id="Seg_2849" s="T194">adv</ta>
            <ta e="T196" id="Seg_2850" s="T195">v</ta>
            <ta e="T197" id="Seg_2851" s="T196">n</ta>
            <ta e="T198" id="Seg_2852" s="T197">adv</ta>
            <ta e="T199" id="Seg_2853" s="T198">v</ta>
            <ta e="T200" id="Seg_2854" s="T199">n</ta>
            <ta e="T201" id="Seg_2855" s="T200">adv</ta>
            <ta e="T202" id="Seg_2856" s="T201">adv</ta>
            <ta e="T203" id="Seg_2857" s="T202">v</ta>
            <ta e="T204" id="Seg_2858" s="T203">conj</ta>
            <ta e="T205" id="Seg_2859" s="T204">quant</ta>
            <ta e="T206" id="Seg_2860" s="T205">que</ta>
            <ta e="T207" id="Seg_2861" s="T206">v</ta>
            <ta e="T208" id="Seg_2862" s="T207">v</ta>
            <ta e="T209" id="Seg_2863" s="T208">adv</ta>
            <ta e="T210" id="Seg_2864" s="T209">v</ta>
            <ta e="T211" id="Seg_2865" s="T210">n</ta>
            <ta e="T212" id="Seg_2866" s="T211">pers</ta>
            <ta e="T213" id="Seg_2867" s="T212">n</ta>
            <ta e="T214" id="Seg_2868" s="T213">ptcl</ta>
            <ta e="T215" id="Seg_2869" s="T214">v</ta>
            <ta e="T216" id="Seg_2870" s="T215">adj</ta>
            <ta e="T218" id="Seg_2871" s="T217">n</ta>
            <ta e="T219" id="Seg_2872" s="T218">v</ta>
            <ta e="T220" id="Seg_2873" s="T219">adv</ta>
            <ta e="T221" id="Seg_2874" s="T220">v</ta>
            <ta e="T222" id="Seg_2875" s="T221">n</ta>
            <ta e="T223" id="Seg_2876" s="T222">conj</ta>
            <ta e="T224" id="Seg_2877" s="T223">n</ta>
            <ta e="T225" id="Seg_2878" s="T224">conj</ta>
            <ta e="T226" id="Seg_2879" s="T225">n</ta>
            <ta e="T227" id="Seg_2880" s="T226">v</ta>
            <ta e="T228" id="Seg_2881" s="T227">v</ta>
            <ta e="T229" id="Seg_2882" s="T228">quant</ta>
            <ta e="T230" id="Seg_2883" s="T229">n</ta>
            <ta e="T231" id="Seg_2884" s="T230">v</ta>
            <ta e="T232" id="Seg_2885" s="T231">adv</ta>
            <ta e="T233" id="Seg_2886" s="T232">conj</ta>
            <ta e="T234" id="Seg_2887" s="T233">n</ta>
            <ta e="T236" id="Seg_2888" s="T235">adv</ta>
            <ta e="T237" id="Seg_2889" s="T236">v</ta>
            <ta e="T238" id="Seg_2890" s="T237">adv</ta>
            <ta e="T239" id="Seg_2891" s="T238">n</ta>
            <ta e="T240" id="Seg_2892" s="T239">dempro</ta>
            <ta e="T241" id="Seg_2893" s="T240">v</ta>
            <ta e="T242" id="Seg_2894" s="T241">v</ta>
            <ta e="T243" id="Seg_2895" s="T242">dempro</ta>
            <ta e="T244" id="Seg_2896" s="T243">n</ta>
            <ta e="T245" id="Seg_2897" s="T244">adv</ta>
            <ta e="T247" id="Seg_2898" s="T246">v</ta>
            <ta e="T248" id="Seg_2899" s="T247">v</ta>
            <ta e="T249" id="Seg_2900" s="T248">adv</ta>
            <ta e="T250" id="Seg_2901" s="T249">dempro</ta>
            <ta e="T251" id="Seg_2902" s="T250">dempro</ta>
            <ta e="T252" id="Seg_2903" s="T251">n</ta>
            <ta e="T253" id="Seg_2904" s="T252">v</ta>
            <ta e="T254" id="Seg_2905" s="T253">n</ta>
            <ta e="T255" id="Seg_2906" s="T254">v</ta>
            <ta e="T256" id="Seg_2907" s="T255">n</ta>
            <ta e="T257" id="Seg_2908" s="T256">adv</ta>
            <ta e="T258" id="Seg_2909" s="T257">n</ta>
            <ta e="T259" id="Seg_2910" s="T258">v</ta>
            <ta e="T260" id="Seg_2911" s="T259">v</ta>
            <ta e="T261" id="Seg_2912" s="T260">n</ta>
            <ta e="T262" id="Seg_2913" s="T261">n</ta>
            <ta e="T263" id="Seg_2914" s="T262">pers</ta>
            <ta e="T264" id="Seg_2915" s="T263">v</ta>
            <ta e="T265" id="Seg_2916" s="T264">v</ta>
            <ta e="T266" id="Seg_2917" s="T265">refl</ta>
            <ta e="T267" id="Seg_2918" s="T266">n</ta>
            <ta e="T268" id="Seg_2919" s="T267">dempro</ta>
            <ta e="T269" id="Seg_2920" s="T268">pers</ta>
            <ta e="T270" id="Seg_2921" s="T269">v</ta>
            <ta e="T271" id="Seg_2922" s="T270">v</ta>
            <ta e="T272" id="Seg_2923" s="T271">ptcl</ta>
            <ta e="T273" id="Seg_2924" s="T272">v</ta>
            <ta e="T274" id="Seg_2925" s="T273">adv</ta>
            <ta e="T275" id="Seg_2926" s="T274">v</ta>
            <ta e="T276" id="Seg_2927" s="T275">n</ta>
            <ta e="T277" id="Seg_2928" s="T276">dempro</ta>
            <ta e="T278" id="Seg_2929" s="T277">dempro</ta>
            <ta e="T279" id="Seg_2930" s="T278">n</ta>
            <ta e="T282" id="Seg_2931" s="T281">v</ta>
            <ta e="T283" id="Seg_2932" s="T282">conj</ta>
            <ta e="T284" id="Seg_2933" s="T283">dempro</ta>
            <ta e="T285" id="Seg_2934" s="T284">n</ta>
            <ta e="T286" id="Seg_2935" s="T285">v</ta>
            <ta e="T287" id="Seg_2936" s="T286">n</ta>
            <ta e="T288" id="Seg_2937" s="T287">conj</ta>
            <ta e="T289" id="Seg_2938" s="T288">v</ta>
            <ta e="T290" id="Seg_2939" s="T289">dempro</ta>
            <ta e="T291" id="Seg_2940" s="T290">n</ta>
            <ta e="T292" id="Seg_2941" s="T291">v</ta>
            <ta e="T293" id="Seg_2942" s="T292">dempro</ta>
            <ta e="T294" id="Seg_2943" s="T293">n</ta>
            <ta e="T295" id="Seg_2944" s="T294">v</ta>
            <ta e="T296" id="Seg_2945" s="T295">adv</ta>
            <ta e="T297" id="Seg_2946" s="T296">dempro</ta>
            <ta e="T298" id="Seg_2947" s="T297">v</ta>
            <ta e="T299" id="Seg_2948" s="T298">n</ta>
            <ta e="T300" id="Seg_2949" s="T299">v</ta>
            <ta e="T301" id="Seg_2950" s="T300">v</ta>
            <ta e="T302" id="Seg_2951" s="T301">v</ta>
            <ta e="T303" id="Seg_2952" s="T302">ptcl</ta>
            <ta e="T304" id="Seg_2953" s="T303">v</ta>
            <ta e="T305" id="Seg_2954" s="T304">adv</ta>
            <ta e="T306" id="Seg_2955" s="T305">v</ta>
            <ta e="T307" id="Seg_2956" s="T306">dempro</ta>
            <ta e="T308" id="Seg_2957" s="T307">quant</ta>
            <ta e="T309" id="Seg_2958" s="T308">dempro</ta>
            <ta e="T310" id="Seg_2959" s="T309">v</ta>
            <ta e="T311" id="Seg_2960" s="T310">v</ta>
            <ta e="T312" id="Seg_2961" s="T311">adv</ta>
            <ta e="T313" id="Seg_2962" s="T312">dempro</ta>
            <ta e="T314" id="Seg_2963" s="T313">n</ta>
            <ta e="T315" id="Seg_2964" s="T314">v</ta>
            <ta e="T316" id="Seg_2965" s="T315">ptcl</ta>
            <ta e="T317" id="Seg_2966" s="T316">adv</ta>
            <ta e="T318" id="Seg_2967" s="T317">n</ta>
            <ta e="T319" id="Seg_2968" s="T318">v</ta>
            <ta e="T320" id="Seg_2969" s="T319">pers</ta>
            <ta e="T321" id="Seg_2970" s="T320">dempro</ta>
            <ta e="T322" id="Seg_2971" s="T321">ptcl</ta>
            <ta e="T323" id="Seg_2972" s="T322">v</ta>
            <ta e="T324" id="Seg_2973" s="T323">adv</ta>
            <ta e="T327" id="Seg_2974" s="T326">dempro</ta>
            <ta e="T328" id="Seg_2975" s="T327">ptcl</ta>
            <ta e="T329" id="Seg_2976" s="T328">n</ta>
            <ta e="T330" id="Seg_2977" s="T329">v</ta>
            <ta e="T331" id="Seg_2978" s="T330">quant</ta>
            <ta e="T332" id="Seg_2979" s="T331">que</ta>
            <ta e="T333" id="Seg_2980" s="T332">v</ta>
            <ta e="T334" id="Seg_2981" s="T333">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T91" id="Seg_2982" s="T90">np.h:A</ta>
            <ta e="T94" id="Seg_2983" s="T93">0.3.h:A</ta>
            <ta e="T96" id="Seg_2984" s="T95">np.h:A</ta>
            <ta e="T97" id="Seg_2985" s="T96">pro:G</ta>
            <ta e="T98" id="Seg_2986" s="T97">pro.h:A</ta>
            <ta e="T100" id="Seg_2987" s="T99">pro.h:P</ta>
            <ta e="T102" id="Seg_2988" s="T101">pro.h:A</ta>
            <ta e="T104" id="Seg_2989" s="T103">0.2.h:A</ta>
            <ta e="T106" id="Seg_2990" s="T105">pro.h:P</ta>
            <ta e="T107" id="Seg_2991" s="T106">pro.h:Th</ta>
            <ta e="T110" id="Seg_2992" s="T109">pro.h:Poss</ta>
            <ta e="T111" id="Seg_2993" s="T110">np:Th</ta>
            <ta e="T113" id="Seg_2994" s="T112">0.3.h:A</ta>
            <ta e="T116" id="Seg_2995" s="T114">adv:Time</ta>
            <ta e="T117" id="Seg_2996" s="T116">adv:Time</ta>
            <ta e="T118" id="Seg_2997" s="T117">0.3.h:A</ta>
            <ta e="T120" id="Seg_2998" s="T119">adv:Time</ta>
            <ta e="T121" id="Seg_2999" s="T120">np.h:A</ta>
            <ta e="T123" id="Seg_3000" s="T122">pro.h:A</ta>
            <ta e="T126" id="Seg_3001" s="T125">pro.h:P</ta>
            <ta e="T128" id="Seg_3002" s="T127">pro.h:A</ta>
            <ta e="T130" id="Seg_3003" s="T129">0.2.h:A</ta>
            <ta e="T132" id="Seg_3004" s="T131">pro.h:P</ta>
            <ta e="T133" id="Seg_3005" s="T132">np:Th</ta>
            <ta e="T134" id="Seg_3006" s="T133">0.1.h:Th</ta>
            <ta e="T135" id="Seg_3007" s="T134">0.3.h:A</ta>
            <ta e="T137" id="Seg_3008" s="T136">adv:Time</ta>
            <ta e="T138" id="Seg_3009" s="T137">0.3.h:A</ta>
            <ta e="T139" id="Seg_3010" s="T138">0.3.h:A</ta>
            <ta e="T140" id="Seg_3011" s="T139">np.h:A</ta>
            <ta e="T142" id="Seg_3012" s="T141">pro.h:A</ta>
            <ta e="T143" id="Seg_3013" s="T142">pro.h:P</ta>
            <ta e="T146" id="Seg_3014" s="T145">pro.h:A</ta>
            <ta e="T147" id="Seg_3015" s="T146">0.2.h:A</ta>
            <ta e="T149" id="Seg_3016" s="T148">pro.h:P</ta>
            <ta e="T150" id="Seg_3017" s="T149">pro.h:Th</ta>
            <ta e="T151" id="Seg_3018" s="T150">np:Th</ta>
            <ta e="T152" id="Seg_3019" s="T151">pro.h:Poss</ta>
            <ta e="T155" id="Seg_3020" s="T154">adv:Time</ta>
            <ta e="T157" id="Seg_3021" s="T156">0.3.h:A</ta>
            <ta e="T158" id="Seg_3022" s="T157">0.3.h:A</ta>
            <ta e="T159" id="Seg_3023" s="T158">0.3.h:A</ta>
            <ta e="T160" id="Seg_3024" s="T159">np.h:A</ta>
            <ta e="T163" id="Seg_3025" s="T162">pro.h:P</ta>
            <ta e="T168" id="Seg_3026" s="T167">pro.h:A</ta>
            <ta e="T170" id="Seg_3027" s="T169">0.2.h:A</ta>
            <ta e="T172" id="Seg_3028" s="T171">pro.h:P</ta>
            <ta e="T174" id="Seg_3029" s="T173">pro.h:Th</ta>
            <ta e="T175" id="Seg_3030" s="T174">pro.h:Poss</ta>
            <ta e="T176" id="Seg_3031" s="T175">np:Th</ta>
            <ta e="T178" id="Seg_3032" s="T177">adv:Time</ta>
            <ta e="T179" id="Seg_3033" s="T178">0.3.h:A</ta>
            <ta e="T180" id="Seg_3034" s="T179">np.h:E</ta>
            <ta e="T182" id="Seg_3035" s="T181">pro.h:A</ta>
            <ta e="T183" id="Seg_3036" s="T182">pro.h:P</ta>
            <ta e="T186" id="Seg_3037" s="T185">0.2.h:A</ta>
            <ta e="T188" id="Seg_3038" s="T187">pro.h:P</ta>
            <ta e="T191" id="Seg_3039" s="T190">pro.h:Th</ta>
            <ta e="T192" id="Seg_3040" s="T191">pro.h:Poss</ta>
            <ta e="T193" id="Seg_3041" s="T192">np:Th</ta>
            <ta e="T195" id="Seg_3042" s="T194">adv:Time</ta>
            <ta e="T196" id="Seg_3043" s="T195">0.3.h:A</ta>
            <ta e="T197" id="Seg_3044" s="T196">np:G</ta>
            <ta e="T198" id="Seg_3045" s="T197">adv:L</ta>
            <ta e="T199" id="Seg_3046" s="T198">0.3.h:E</ta>
            <ta e="T200" id="Seg_3047" s="T199">np:Th</ta>
            <ta e="T201" id="Seg_3048" s="T200">adv:L</ta>
            <ta e="T202" id="Seg_3049" s="T201">adv:L</ta>
            <ta e="T203" id="Seg_3050" s="T202">0.3.h:E</ta>
            <ta e="T206" id="Seg_3051" s="T205">pro:Th</ta>
            <ta e="T209" id="Seg_3052" s="T208">adv:Time</ta>
            <ta e="T211" id="Seg_3053" s="T210">np.h:A</ta>
            <ta e="T212" id="Seg_3054" s="T211">pro.h:Poss</ta>
            <ta e="T213" id="Seg_3055" s="T212">np.h:A</ta>
            <ta e="T216" id="Seg_3056" s="T215">np.h:Poss</ta>
            <ta e="T218" id="Seg_3057" s="T217">np.h:Th</ta>
            <ta e="T219" id="Seg_3058" s="T218">0.1.h:A</ta>
            <ta e="T220" id="Seg_3059" s="T219">adv:Time</ta>
            <ta e="T221" id="Seg_3060" s="T220">0.3.h:A</ta>
            <ta e="T222" id="Seg_3061" s="T221">np.h:A</ta>
            <ta e="T224" id="Seg_3062" s="T223">np.h:A</ta>
            <ta e="T226" id="Seg_3063" s="T225">np.h:A</ta>
            <ta e="T228" id="Seg_3064" s="T227">0.3.h:A</ta>
            <ta e="T230" id="Seg_3065" s="T229">np.h:A</ta>
            <ta e="T232" id="Seg_3066" s="T231">adv:Time</ta>
            <ta e="T234" id="Seg_3067" s="T233">np.h:A</ta>
            <ta e="T236" id="Seg_3068" s="T235">adv:G</ta>
            <ta e="T238" id="Seg_3069" s="T237">adv:Time</ta>
            <ta e="T239" id="Seg_3070" s="T238">np.h:A</ta>
            <ta e="T240" id="Seg_3071" s="T239">np.h:Th</ta>
            <ta e="T243" id="Seg_3072" s="T242">pro.h:Th</ta>
            <ta e="T244" id="Seg_3073" s="T243">np:G</ta>
            <ta e="T245" id="Seg_3074" s="T244">adv:Time</ta>
            <ta e="T247" id="Seg_3075" s="T246">0.3.h:E</ta>
            <ta e="T248" id="Seg_3076" s="T247">0.3.h:E</ta>
            <ta e="T249" id="Seg_3077" s="T248">adv:Time</ta>
            <ta e="T250" id="Seg_3078" s="T249">pro.h:A</ta>
            <ta e="T251" id="Seg_3079" s="T250">pro:G</ta>
            <ta e="T254" id="Seg_3080" s="T253">np.h:A</ta>
            <ta e="T256" id="Seg_3081" s="T255">np:G</ta>
            <ta e="T257" id="Seg_3082" s="T256">adv:L</ta>
            <ta e="T258" id="Seg_3083" s="T257">np.h:E</ta>
            <ta e="T260" id="Seg_3084" s="T259">0.1.h:A</ta>
            <ta e="T261" id="Seg_3085" s="T260">np:G</ta>
            <ta e="T262" id="Seg_3086" s="T261">np:G</ta>
            <ta e="T263" id="Seg_3087" s="T262">pro.h:A</ta>
            <ta e="T265" id="Seg_3088" s="T264">0.1.h:A</ta>
            <ta e="T266" id="Seg_3089" s="T265">pro.h:Poss</ta>
            <ta e="T267" id="Seg_3090" s="T266">np.h:R</ta>
            <ta e="T268" id="Seg_3091" s="T267">pro.h:A</ta>
            <ta e="T269" id="Seg_3092" s="T268">pro.h:R</ta>
            <ta e="T271" id="Seg_3093" s="T270">0.2.h:A</ta>
            <ta e="T273" id="Seg_3094" s="T272">0.1.h:A</ta>
            <ta e="T274" id="Seg_3095" s="T273">adv:Time</ta>
            <ta e="T276" id="Seg_3096" s="T275">np.h:A</ta>
            <ta e="T278" id="Seg_3097" s="T277">pro.h:A</ta>
            <ta e="T279" id="Seg_3098" s="T278">np.h:E</ta>
            <ta e="T285" id="Seg_3099" s="T284">np.h:A</ta>
            <ta e="T286" id="Seg_3100" s="T285">0.2.h:A</ta>
            <ta e="T287" id="Seg_3101" s="T286">np:G</ta>
            <ta e="T289" id="Seg_3102" s="T288">0.2.h:A</ta>
            <ta e="T290" id="Seg_3103" s="T289">pro.h:Poss</ta>
            <ta e="T291" id="Seg_3104" s="T290">np.h:A</ta>
            <ta e="T294" id="Seg_3105" s="T293">np:G</ta>
            <ta e="T295" id="Seg_3106" s="T294">0.3.h:A</ta>
            <ta e="T296" id="Seg_3107" s="T295">adv:Time</ta>
            <ta e="T297" id="Seg_3108" s="T296">pro.h:A</ta>
            <ta e="T299" id="Seg_3109" s="T298">np.h:E 0.3.h:Poss</ta>
            <ta e="T301" id="Seg_3110" s="T300">0.3.h:E</ta>
            <ta e="T302" id="Seg_3111" s="T301">0.3.h:A</ta>
            <ta e="T304" id="Seg_3112" s="T303">0.3.h:A</ta>
            <ta e="T305" id="Seg_3113" s="T304">adv:Time</ta>
            <ta e="T306" id="Seg_3114" s="T305">0.3.h:A</ta>
            <ta e="T307" id="Seg_3115" s="T306">pro.h:R</ta>
            <ta e="T308" id="Seg_3116" s="T307">np:Th</ta>
            <ta e="T309" id="Seg_3117" s="T308">pro.h:A</ta>
            <ta e="T311" id="Seg_3118" s="T310">0.3.h:A</ta>
            <ta e="T312" id="Seg_3119" s="T311">adv:Time</ta>
            <ta e="T313" id="Seg_3120" s="T312">pro.h:A</ta>
            <ta e="T314" id="Seg_3121" s="T313">np:P</ta>
            <ta e="T317" id="Seg_3122" s="T316">adv:Time</ta>
            <ta e="T318" id="Seg_3123" s="T317">np.h:A 0.3.h:Poss</ta>
            <ta e="T319" id="Seg_3124" s="T318">0.2.h:A</ta>
            <ta e="T320" id="Seg_3125" s="T319">pro:G</ta>
            <ta e="T321" id="Seg_3126" s="T320">pro.h:A</ta>
            <ta e="T324" id="Seg_3127" s="T323">adv:Time</ta>
            <ta e="T327" id="Seg_3128" s="T326">pro.h:B</ta>
            <ta e="T329" id="Seg_3129" s="T328">np:P</ta>
            <ta e="T330" id="Seg_3130" s="T329">0.3.h:A</ta>
            <ta e="T332" id="Seg_3131" s="T331">pro:Th</ta>
            <ta e="T333" id="Seg_3132" s="T332">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T91" id="Seg_3133" s="T90">np.h:S</ta>
            <ta e="T92" id="Seg_3134" s="T91">v:pred</ta>
            <ta e="T93" id="Seg_3135" s="T92">s:purp</ta>
            <ta e="T94" id="Seg_3136" s="T93">v:pred 0.3.h:S</ta>
            <ta e="T95" id="Seg_3137" s="T94">v:pred</ta>
            <ta e="T96" id="Seg_3138" s="T95">np.h:S</ta>
            <ta e="T99" id="Seg_3139" s="T98">ptcl:pred</ta>
            <ta e="T100" id="Seg_3140" s="T99">pro.h:O</ta>
            <ta e="T102" id="Seg_3141" s="T101">pro.h:S</ta>
            <ta e="T103" id="Seg_3142" s="T102">v:pred</ta>
            <ta e="T104" id="Seg_3143" s="T103">v:pred 0.2.h:S</ta>
            <ta e="T106" id="Seg_3144" s="T105">pro.h:O</ta>
            <ta e="T107" id="Seg_3145" s="T106">pro.h:S</ta>
            <ta e="T111" id="Seg_3146" s="T110">n:pred</ta>
            <ta e="T112" id="Seg_3147" s="T111">cop</ta>
            <ta e="T113" id="Seg_3148" s="T112">v:pred 0.3.h:S</ta>
            <ta e="T118" id="Seg_3149" s="T117">v:pred 0.3.h:S</ta>
            <ta e="T121" id="Seg_3150" s="T120">np.h:S</ta>
            <ta e="T122" id="Seg_3151" s="T121">v:pred</ta>
            <ta e="T125" id="Seg_3152" s="T124">ptcl:pred</ta>
            <ta e="T126" id="Seg_3153" s="T125">pro.h:O</ta>
            <ta e="T127" id="Seg_3154" s="T126">v:pred</ta>
            <ta e="T128" id="Seg_3155" s="T127">pro.h:S</ta>
            <ta e="T129" id="Seg_3156" s="T128">v:pred</ta>
            <ta e="T130" id="Seg_3157" s="T129">v:pred 0.2.h:S</ta>
            <ta e="T132" id="Seg_3158" s="T131">pro.h:O</ta>
            <ta e="T133" id="Seg_3159" s="T132">n:pred</ta>
            <ta e="T134" id="Seg_3160" s="T133">cop 0.1.h:S</ta>
            <ta e="T135" id="Seg_3161" s="T134">v:pred 0.3.h:S</ta>
            <ta e="T138" id="Seg_3162" s="T137">v:pred 0.3.h:S</ta>
            <ta e="T139" id="Seg_3163" s="T138">v:pred 0.3.h:S</ta>
            <ta e="T140" id="Seg_3164" s="T139">np.h:S</ta>
            <ta e="T141" id="Seg_3165" s="T140">v:pred</ta>
            <ta e="T143" id="Seg_3166" s="T142">pro.h:O</ta>
            <ta e="T144" id="Seg_3167" s="T143">ptcl:pred</ta>
            <ta e="T146" id="Seg_3168" s="T145">pro.h:S</ta>
            <ta e="T147" id="Seg_3169" s="T146">v:pred 0.2.h:S</ta>
            <ta e="T149" id="Seg_3170" s="T148">pro.h:O</ta>
            <ta e="T150" id="Seg_3171" s="T149">pro.h:S</ta>
            <ta e="T151" id="Seg_3172" s="T150">n:pred</ta>
            <ta e="T154" id="Seg_3173" s="T153">cop</ta>
            <ta e="T157" id="Seg_3174" s="T156">v:pred 0.3.h:S</ta>
            <ta e="T158" id="Seg_3175" s="T157">v:pred 0.3.h:S</ta>
            <ta e="T159" id="Seg_3176" s="T158">v:pred 0.3.h:S</ta>
            <ta e="T160" id="Seg_3177" s="T159">np.h:S</ta>
            <ta e="T161" id="Seg_3178" s="T160">v:pred</ta>
            <ta e="T163" id="Seg_3179" s="T162">pro.h:O</ta>
            <ta e="T164" id="Seg_3180" s="T163">ptcl:pred</ta>
            <ta e="T168" id="Seg_3181" s="T167">pro.h:S</ta>
            <ta e="T169" id="Seg_3182" s="T168">v:pred</ta>
            <ta e="T170" id="Seg_3183" s="T169">v:pred 0.2.h:S</ta>
            <ta e="T172" id="Seg_3184" s="T171">pro.h:O</ta>
            <ta e="T174" id="Seg_3185" s="T173">pro.h:S</ta>
            <ta e="T176" id="Seg_3186" s="T175">n:pred</ta>
            <ta e="T177" id="Seg_3187" s="T176">cop</ta>
            <ta e="T179" id="Seg_3188" s="T178">v:pred 0.3.h:S</ta>
            <ta e="T180" id="Seg_3189" s="T179">np.h:S</ta>
            <ta e="T181" id="Seg_3190" s="T180">v:pred</ta>
            <ta e="T183" id="Seg_3191" s="T182">pro.h:O</ta>
            <ta e="T184" id="Seg_3192" s="T183">ptcl:pred</ta>
            <ta e="T186" id="Seg_3193" s="T185">v:pred 0.2.h:S</ta>
            <ta e="T188" id="Seg_3194" s="T187">pro.h:O</ta>
            <ta e="T191" id="Seg_3195" s="T190">pro.h:S</ta>
            <ta e="T193" id="Seg_3196" s="T192">n:pred</ta>
            <ta e="T194" id="Seg_3197" s="T193">cop</ta>
            <ta e="T196" id="Seg_3198" s="T195">v:pred 0.3.h:S</ta>
            <ta e="T199" id="Seg_3199" s="T198">v:pred 0.3.h:S</ta>
            <ta e="T200" id="Seg_3200" s="T199">np:O</ta>
            <ta e="T203" id="Seg_3201" s="T202">v:pred 0.3.h:S</ta>
            <ta e="T206" id="Seg_3202" s="T205">pro:S</ta>
            <ta e="T207" id="Seg_3203" s="T206">v:pred</ta>
            <ta e="T208" id="Seg_3204" s="T207">s:purp</ta>
            <ta e="T210" id="Seg_3205" s="T209">v:pred </ta>
            <ta e="T211" id="Seg_3206" s="T210">np.h:S</ta>
            <ta e="T214" id="Seg_3207" s="T213">ptcl:pred</ta>
            <ta e="T218" id="Seg_3208" s="T217">np.h:O</ta>
            <ta e="T219" id="Seg_3209" s="T218">v:pred 0.1.h:S</ta>
            <ta e="T221" id="Seg_3210" s="T220">v:pred 0.3.h:S</ta>
            <ta e="T222" id="Seg_3211" s="T221">np.h:S</ta>
            <ta e="T224" id="Seg_3212" s="T223">np.h:S</ta>
            <ta e="T226" id="Seg_3213" s="T225">np.h:S</ta>
            <ta e="T227" id="Seg_3214" s="T226">v:pred</ta>
            <ta e="T228" id="Seg_3215" s="T227">v:pred 0.3.h:S</ta>
            <ta e="T230" id="Seg_3216" s="T229">np.h:S</ta>
            <ta e="T231" id="Seg_3217" s="T230">v:pred</ta>
            <ta e="T234" id="Seg_3218" s="T233">np.h:S</ta>
            <ta e="T237" id="Seg_3219" s="T236">v:pred</ta>
            <ta e="T239" id="Seg_3220" s="T238">np.h:S</ta>
            <ta e="T240" id="Seg_3221" s="T239">pro.h:O</ta>
            <ta e="T241" id="Seg_3222" s="T240">conv:pred</ta>
            <ta e="T242" id="Seg_3223" s="T241">v:pred</ta>
            <ta e="T243" id="Seg_3224" s="T242">pro.h:O</ta>
            <ta e="T247" id="Seg_3225" s="T246">v:pred 0.3.h:S</ta>
            <ta e="T248" id="Seg_3226" s="T247">v:pred 0.3.h:S</ta>
            <ta e="T250" id="Seg_3227" s="T249">pro.h:S</ta>
            <ta e="T253" id="Seg_3228" s="T252">v:pred</ta>
            <ta e="T254" id="Seg_3229" s="T253">np.h:S</ta>
            <ta e="T255" id="Seg_3230" s="T254">v:pred</ta>
            <ta e="T258" id="Seg_3231" s="T257">np.h:S</ta>
            <ta e="T259" id="Seg_3232" s="T258">v:pred</ta>
            <ta e="T260" id="Seg_3233" s="T259">v:pred 0.1.h:S</ta>
            <ta e="T263" id="Seg_3234" s="T262">pro.h:S</ta>
            <ta e="T264" id="Seg_3235" s="T263">v:pred</ta>
            <ta e="T265" id="Seg_3236" s="T264">v:pred 0.1.h:S</ta>
            <ta e="T268" id="Seg_3237" s="T267">pro.h:S</ta>
            <ta e="T270" id="Seg_3238" s="T269">v:pred</ta>
            <ta e="T271" id="Seg_3239" s="T270">v:pred 0.2.h:S</ta>
            <ta e="T273" id="Seg_3240" s="T272">v:pred 0.1.h:S</ta>
            <ta e="T275" id="Seg_3241" s="T274">v:pred</ta>
            <ta e="T276" id="Seg_3242" s="T275">np.h:S</ta>
            <ta e="T278" id="Seg_3243" s="T277">pro.h:S</ta>
            <ta e="T279" id="Seg_3244" s="T278">np.h:O</ta>
            <ta e="T282" id="Seg_3245" s="T281">v:pred</ta>
            <ta e="T285" id="Seg_3246" s="T284">np.h:S</ta>
            <ta e="T286" id="Seg_3247" s="T285">v:pred 0.2.h:S</ta>
            <ta e="T289" id="Seg_3248" s="T288">v:pred 0.2.h:S</ta>
            <ta e="T291" id="Seg_3249" s="T290">np.h:S</ta>
            <ta e="T292" id="Seg_3250" s="T291">v:pred</ta>
            <ta e="T295" id="Seg_3251" s="T294">v:pred 0.3.h:S</ta>
            <ta e="T297" id="Seg_3252" s="T296">pro.h:S</ta>
            <ta e="T298" id="Seg_3253" s="T297">v:pred</ta>
            <ta e="T299" id="Seg_3254" s="T298">np.h:S</ta>
            <ta e="T300" id="Seg_3255" s="T299">v:pred</ta>
            <ta e="T301" id="Seg_3256" s="T300">v:pred 0.3.h:S</ta>
            <ta e="T302" id="Seg_3257" s="T301">v:pred 0.3.h:S</ta>
            <ta e="T303" id="Seg_3258" s="T302">ptcl:pred</ta>
            <ta e="T306" id="Seg_3259" s="T305">v:pred 0.3.h:S</ta>
            <ta e="T308" id="Seg_3260" s="T307">np:O</ta>
            <ta e="T309" id="Seg_3261" s="T308">pro.h:S</ta>
            <ta e="T310" id="Seg_3262" s="T309">v:pred</ta>
            <ta e="T311" id="Seg_3263" s="T310">v:pred 0.3.h:S</ta>
            <ta e="T313" id="Seg_3264" s="T312">pro.h:S</ta>
            <ta e="T314" id="Seg_3265" s="T313">np:O</ta>
            <ta e="T315" id="Seg_3266" s="T314">v:pred</ta>
            <ta e="T318" id="Seg_3267" s="T317">np.h:S</ta>
            <ta e="T319" id="Seg_3268" s="T318">v:pred 0.2.h:S</ta>
            <ta e="T321" id="Seg_3269" s="T320">pro.h:S</ta>
            <ta e="T322" id="Seg_3270" s="T321">ptcl.neg</ta>
            <ta e="T323" id="Seg_3271" s="T322">v:pred</ta>
            <ta e="T329" id="Seg_3272" s="T328">np:O</ta>
            <ta e="T330" id="Seg_3273" s="T329">v:pred 0.3.h:S</ta>
            <ta e="T332" id="Seg_3274" s="T331">pro:O</ta>
            <ta e="T333" id="Seg_3275" s="T332">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T96" id="Seg_3276" s="T95">RUS:cult</ta>
            <ta e="T99" id="Seg_3277" s="T98">RUS:mod</ta>
            <ta e="T125" id="Seg_3278" s="T124">RUS:mod</ta>
            <ta e="T140" id="Seg_3279" s="T139">RUS:cult</ta>
            <ta e="T144" id="Seg_3280" s="T143">RUS:mod</ta>
            <ta e="T162" id="Seg_3281" s="T161">TURK:disc</ta>
            <ta e="T164" id="Seg_3282" s="T163">RUS:mod</ta>
            <ta e="T167" id="Seg_3283" s="T166">TURK:disc</ta>
            <ta e="T184" id="Seg_3284" s="T183">RUS:mod</ta>
            <ta e="T204" id="Seg_3285" s="T203">RUS:gram</ta>
            <ta e="T205" id="Seg_3286" s="T204">TURK:disc</ta>
            <ta e="T214" id="Seg_3287" s="T213">RUS:mod</ta>
            <ta e="T216" id="Seg_3288" s="T215">RUS:cult</ta>
            <ta e="T223" id="Seg_3289" s="T222">RUS:gram</ta>
            <ta e="T224" id="Seg_3290" s="T223">RUS:cult</ta>
            <ta e="T225" id="Seg_3291" s="T224">RUS:gram</ta>
            <ta e="T226" id="Seg_3292" s="T225">RUS:cult</ta>
            <ta e="T229" id="Seg_3293" s="T228">TURK:disc</ta>
            <ta e="T233" id="Seg_3294" s="T232">RUS:gram</ta>
            <ta e="T272" id="Seg_3295" s="T271">RUS:gram</ta>
            <ta e="T283" id="Seg_3296" s="T282">RUS:gram</ta>
            <ta e="T288" id="Seg_3297" s="T287">RUS:gram</ta>
            <ta e="T303" id="Seg_3298" s="T302">RUS:mod</ta>
            <ta e="T314" id="Seg_3299" s="T313">TURK:cult</ta>
            <ta e="T316" id="Seg_3300" s="T315">TURK:disc</ta>
            <ta e="T328" id="Seg_3301" s="T327">TURK:disc</ta>
            <ta e="T329" id="Seg_3302" s="T328">TAT:cult</ta>
            <ta e="T331" id="Seg_3303" s="T330">TURK:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T330" id="Seg_3304" s="T329">RUS:calq</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T93" id="Seg_3305" s="T89">Один человек пошёл на охоту.</ta>
            <ta e="T94" id="Seg_3306" s="T93">Идёт он.</ta>
            <ta e="T97" id="Seg_3307" s="T94">На него волк бежит.</ta>
            <ta e="T101" id="Seg_3308" s="T97">Он хотел в него выстрелить.</ta>
            <ta e="T106" id="Seg_3309" s="T101">[А волк] говорит: «Не стреляй в меня!</ta>
            <ta e="T112" id="Seg_3310" s="T106">Я твоим другом стану».</ta>
            <ta e="T114" id="Seg_3311" s="T112">Пошли они вдвоём.</ta>
            <ta e="T116" id="Seg_3312" s="T114">Потом…</ta>
            <ta e="T119" id="Seg_3313" s="T116">Потом идут вдвоём.</ta>
            <ta e="T122" id="Seg_3314" s="T119">Потом идёт свинья.</ta>
            <ta e="T127" id="Seg_3315" s="T122">Он хотел в неё выстрелить.</ta>
            <ta e="T132" id="Seg_3316" s="T127">Она говорит: «Не стреляй в меня!</ta>
            <ta e="T134" id="Seg_3317" s="T132">Друзьями станем!»</ta>
            <ta e="T136" id="Seg_3318" s="T134">Пошли они втроём.</ta>
            <ta e="T139" id="Seg_3319" s="T136">Потом идут, идут.</ta>
            <ta e="T141" id="Seg_3320" s="T139">Лисичка бежит.</ta>
            <ta e="T145" id="Seg_3321" s="T141">Он хотел в неё выстрелить.</ta>
            <ta e="T149" id="Seg_3322" s="T145">Она [говорит]: «Не стреляй в меня!</ta>
            <ta e="T154" id="Seg_3323" s="T149">Я твоим другом стану».</ta>
            <ta e="T157" id="Seg_3324" s="T154">Потом они пошли.</ta>
            <ta e="T159" id="Seg_3325" s="T157">Идут они, идут.</ta>
            <ta e="T162" id="Seg_3326" s="T159">Летит птица.</ta>
            <ta e="T167" id="Seg_3327" s="T162">Он хотел в неё выстрелить.</ta>
            <ta e="T172" id="Seg_3328" s="T167">Она говорит: «Не стреляй в меня!</ta>
            <ta e="T177" id="Seg_3329" s="T172">Я твоим другом стану!»</ta>
            <ta e="T179" id="Seg_3330" s="T177">Потом идут.</ta>
            <ta e="T181" id="Seg_3331" s="T179">Лежит (змея?).</ta>
            <ta e="T185" id="Seg_3332" s="T181">Он хотел в неё выстрелить.</ta>
            <ta e="T188" id="Seg_3333" s="T185">«Не стреляй!</ta>
            <ta e="T194" id="Seg_3334" s="T188">Я твоим другом стану».</ta>
            <ta e="T197" id="Seg_3335" s="T194">Потом пришли к горе.</ta>
            <ta e="T201" id="Seg_3336" s="T197">Там увидели пещеру.</ta>
            <ta e="T208" id="Seg_3337" s="T201">Там живут, всё есть для еды.</ta>
            <ta e="T211" id="Seg_3338" s="T208">Потом друзья говорят:</ta>
            <ta e="T215" id="Seg_3339" s="T211">«Нашего парня надо женить.</ta>
            <ta e="T219" id="Seg_3340" s="T215">Мы царскую дочь возьмём».</ta>
            <ta e="T221" id="Seg_3341" s="T219">Потом они ушли.</ta>
            <ta e="T227" id="Seg_3342" s="T221">Свинья, лисица и волк пошли.</ta>
            <ta e="T231" id="Seg_3343" s="T227">Они (играют?), пришли люди.</ta>
            <ta e="T237" id="Seg_3344" s="T231">Потом и [царская] дочь вышла.</ta>
            <ta e="T244" id="Seg_3345" s="T237">Потом птица её схватила и принесла к этому человеку.</ta>
            <ta e="T248" id="Seg_3346" s="T244">Потом они живут, живут.</ta>
            <ta e="T253" id="Seg_3347" s="T248">Потом она за него (замуж?) выходит.</ta>
            <ta e="T256" id="Seg_3348" s="T253">Пошла девушка на реку.</ta>
            <ta e="T259" id="Seg_3349" s="T256">Там женщина стоит.</ta>
            <ta e="T262" id="Seg_3350" s="T259">«Пойдём домой, к твоему отцу!»</ta>
            <ta e="T273" id="Seg_3351" s="T262">«Я пойду, спрошу у мужа, если он мне скажет: иди, так я пойду».</ta>
            <ta e="T282" id="Seg_3352" s="T273">Потом приползла (змея?), эту женщину укусила.</ta>
            <ta e="T289" id="Seg_3353" s="T282">А эта женщина [говорит]: «Иди к [своему?] отцу и скажи:</ta>
            <ta e="T295" id="Seg_3354" s="T289">Его дочь пойдёт к этому человеку, он охотится».</ta>
            <ta e="T298" id="Seg_3355" s="T295">Потом (она?) пошла [к отцу].</ta>
            <ta e="T304" id="Seg_3356" s="T298">Отец сидел, сидел, [потом] говорит: «Пускай идёт!»</ta>
            <ta e="T308" id="Seg_3357" s="T304">Потом он ей дал много [вещей].</ta>
            <ta e="T311" id="Seg_3358" s="T308">Она пришла, рассказала [мужу].</ta>
            <ta e="T316" id="Seg_3359" s="T311">Потом они выпили водки.</ta>
            <ta e="T320" id="Seg_3360" s="T316">Потом её отец [говорит]: «Идите ко мне!»</ta>
            <ta e="T323" id="Seg_3361" s="T320">Они не пошли.</ta>
            <ta e="T330" id="Seg_3362" s="T323">Потом он срубил для них дом.</ta>
            <ta e="T333" id="Seg_3363" s="T330">Всё [им] дал.</ta>
            <ta e="T334" id="Seg_3364" s="T333">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T93" id="Seg_3365" s="T89">One man went to hunt.</ta>
            <ta e="T94" id="Seg_3366" s="T93">He is coming.</ta>
            <ta e="T97" id="Seg_3367" s="T94">A wolf is running to him.</ta>
            <ta e="T101" id="Seg_3368" s="T97">He wanted to shoot it.</ta>
            <ta e="T106" id="Seg_3369" s="T101">[And the wolf] says: "Don't shoot at me!</ta>
            <ta e="T112" id="Seg_3370" s="T106">I will become your friend."</ta>
            <ta e="T114" id="Seg_3371" s="T112">They went, the two of them.</ta>
            <ta e="T116" id="Seg_3372" s="T114">Then…</ta>
            <ta e="T119" id="Seg_3373" s="T116">Then they are coming, the two of them.</ta>
            <ta e="T122" id="Seg_3374" s="T119">Then a pig comes.</ta>
            <ta e="T127" id="Seg_3375" s="T122">He wanted to shoot it.</ta>
            <ta e="T132" id="Seg_3376" s="T127">It says: "Don't shoot at me!</ta>
            <ta e="T134" id="Seg_3377" s="T132">We will become friends!"</ta>
            <ta e="T136" id="Seg_3378" s="T134">They went, the three of them.</ta>
            <ta e="T139" id="Seg_3379" s="T136">Then they come, they come.</ta>
            <ta e="T141" id="Seg_3380" s="T139">A fox is running.</ta>
            <ta e="T145" id="Seg_3381" s="T141">He wanted to shoot it.</ta>
            <ta e="T149" id="Seg_3382" s="T145">It [says]: "Don't shoot at me!</ta>
            <ta e="T154" id="Seg_3383" s="T149">I will become your friend"</ta>
            <ta e="T157" id="Seg_3384" s="T154">Then they went.</ta>
            <ta e="T159" id="Seg_3385" s="T157">They come, they come.</ta>
            <ta e="T162" id="Seg_3386" s="T159">A bird is flying.</ta>
            <ta e="T167" id="Seg_3387" s="T162">He wanted to shoot it.</ta>
            <ta e="T172" id="Seg_3388" s="T167">It says: "Don't shoot at me!</ta>
            <ta e="T177" id="Seg_3389" s="T172">I will become your friend!"</ta>
            <ta e="T179" id="Seg_3390" s="T177">Then they come.</ta>
            <ta e="T181" id="Seg_3391" s="T179">A (snake?) is lying.</ta>
            <ta e="T185" id="Seg_3392" s="T181">He wanted to shoot it.</ta>
            <ta e="T188" id="Seg_3393" s="T185">"Don't shoot at me!</ta>
            <ta e="T194" id="Seg_3394" s="T188">I will become your friend."</ta>
            <ta e="T197" id="Seg_3395" s="T194">Then they arrived at a mountain.</ta>
            <ta e="T201" id="Seg_3396" s="T197">They saw a cave there.</ta>
            <ta e="T208" id="Seg_3397" s="T201">There they are living, they have everything to eat.</ta>
            <ta e="T211" id="Seg_3398" s="T208">Then the friends say:</ta>
            <ta e="T215" id="Seg_3399" s="T211">"Our man needs to marry.</ta>
            <ta e="T219" id="Seg_3400" s="T215">We will take the tsar's daughter.</ta>
            <ta e="T221" id="Seg_3401" s="T219">Then they left.</ta>
            <ta e="T227" id="Seg_3402" s="T221">The pig and the fox and the wolf left.</ta>
            <ta e="T231" id="Seg_3403" s="T227">They are (playing?), people came.</ta>
            <ta e="T237" id="Seg_3404" s="T231">Then the [tsar's] daughter came out.</ta>
            <ta e="T244" id="Seg_3405" s="T237">Then the bird grabbing her took her to the man.</ta>
            <ta e="T248" id="Seg_3406" s="T244">Then they lived, they lived.</ta>
            <ta e="T253" id="Seg_3407" s="T248">Then she (marries?) him.</ta>
            <ta e="T256" id="Seg_3408" s="T253">The girl went to the river.</ta>
            <ta e="T259" id="Seg_3409" s="T256">A woman is standing there.</ta>
            <ta e="T262" id="Seg_3410" s="T259">"Let's go home to your father!"</ta>
            <ta e="T273" id="Seg_3411" s="T262">"I will go, I will ask my husband, if he will tell me: go!, then I will go."</ta>
            <ta e="T282" id="Seg_3412" s="T273">Then the (snake?) came, it bit the woman.</ta>
            <ta e="T289" id="Seg_3413" s="T282">And the woman [says]: "Go to [your?] father and tell [him]:</ta>
            <ta e="T295" id="Seg_3414" s="T289">His daughter will go there to the man, he is [away] hunting."</ta>
            <ta e="T298" id="Seg_3415" s="T295">Then (she?) went [to her father].</ta>
            <ta e="T304" id="Seg_3416" s="T298">Her father was sitting and sitting, [then he] says: "Let her go!"</ta>
            <ta e="T308" id="Seg_3417" s="T304">Then he gave her a lot [of things].</ta>
            <ta e="T311" id="Seg_3418" s="T308">She came, told [her husband].</ta>
            <ta e="T316" id="Seg_3419" s="T311">Then they drank vodka.</ta>
            <ta e="T320" id="Seg_3420" s="T316">Then her father [says]: "Come to me!"</ta>
            <ta e="T323" id="Seg_3421" s="T320">They did not go.</ta>
            <ta e="T330" id="Seg_3422" s="T323">Then he built a house for them.</ta>
            <ta e="T333" id="Seg_3423" s="T330">He gave everything.</ta>
            <ta e="T334" id="Seg_3424" s="T333">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T93" id="Seg_3425" s="T89">Ein Mann ging jagen.</ta>
            <ta e="T94" id="Seg_3426" s="T93">Er kommt.</ta>
            <ta e="T97" id="Seg_3427" s="T94">Ein Wolf rennt zu ihm.</ta>
            <ta e="T101" id="Seg_3428" s="T97">Er wollte ihn erschießen.</ta>
            <ta e="T106" id="Seg_3429" s="T101">[Und der Wolf] sagt: „Schieß nicht auf mich!</ta>
            <ta e="T112" id="Seg_3430" s="T106">Ich werde dein Freund werden.“</ta>
            <ta e="T114" id="Seg_3431" s="T112">Sie gingen, alle beide.</ta>
            <ta e="T116" id="Seg_3432" s="T114">Dann…</ta>
            <ta e="T119" id="Seg_3433" s="T116">Dann kommen sie, alle beide.</ta>
            <ta e="T122" id="Seg_3434" s="T119">Dann kommt ein Schwein.</ta>
            <ta e="T127" id="Seg_3435" s="T122">Er wollte es erschießen.</ta>
            <ta e="T132" id="Seg_3436" s="T127">Es sagt: „Schieß nicht auf mich!“</ta>
            <ta e="T134" id="Seg_3437" s="T132">Wir werden Freunde werden!“</ta>
            <ta e="T136" id="Seg_3438" s="T134">Sie gingen, alle drei.</ta>
            <ta e="T139" id="Seg_3439" s="T136">Dann kommen sie, sie kommen.</ta>
            <ta e="T141" id="Seg_3440" s="T139">Ein Fuchs läuft.</ta>
            <ta e="T145" id="Seg_3441" s="T141">Er wollte ihn erschießen.</ta>
            <ta e="T149" id="Seg_3442" s="T145">Er [sagt]: „Schieß nicht auf mich!“</ta>
            <ta e="T154" id="Seg_3443" s="T149">Ich werde dein Freund werden“</ta>
            <ta e="T157" id="Seg_3444" s="T154">Dann gingen sie.</ta>
            <ta e="T159" id="Seg_3445" s="T157">Sie kommen, sie kommen.</ta>
            <ta e="T162" id="Seg_3446" s="T159">Ein Vogel fliegt.</ta>
            <ta e="T167" id="Seg_3447" s="T162">Er wollte ihn erschießen.</ta>
            <ta e="T172" id="Seg_3448" s="T167">Er sagte: „Schieß nicht auf mich!</ta>
            <ta e="T177" id="Seg_3449" s="T172">Ich werde dein Freund werden!“</ta>
            <ta e="T179" id="Seg_3450" s="T177">Dann kommen sie.</ta>
            <ta e="T181" id="Seg_3451" s="T179">Eine (Schlange?) liegt.</ta>
            <ta e="T185" id="Seg_3452" s="T181">Er wollte ihn erschießen.</ta>
            <ta e="T188" id="Seg_3453" s="T185">„Schieß nicht auf mich!</ta>
            <ta e="T194" id="Seg_3454" s="T188">Ich werde dein Freund werden.“</ta>
            <ta e="T197" id="Seg_3455" s="T194">Dann kamen sie an einem Berg an.</ta>
            <ta e="T201" id="Seg_3456" s="T197">Sie sahen eine Höhle dort.</ta>
            <ta e="T208" id="Seg_3457" s="T201">Dann leben sie, sie haben alles zu essen.</ta>
            <ta e="T211" id="Seg_3458" s="T208">Dann sagen die Freunde:</ta>
            <ta e="T215" id="Seg_3459" s="T211">„Unser Mann muss heiraten.</ta>
            <ta e="T219" id="Seg_3460" s="T215">Wir werden die Tochter des Zaren nehmen.</ta>
            <ta e="T221" id="Seg_3461" s="T219">Dann gingen sie.</ta>
            <ta e="T227" id="Seg_3462" s="T221">Das Schwein und der Fuchs und der Wolf gingen.</ta>
            <ta e="T231" id="Seg_3463" s="T227">Sie (spielen?), Leute kommen.</ta>
            <ta e="T237" id="Seg_3464" s="T231">Dann kamm die Tochter [des Zaren] heraus.</ta>
            <ta e="T244" id="Seg_3465" s="T237">Dann schnappte sie der Vogel und nahm sie zum Mann.</ta>
            <ta e="T248" id="Seg_3466" s="T244">Dann lebten sie, sie lebten!</ta>
            <ta e="T253" id="Seg_3467" s="T248">Dann (heiratet?) sie ihn.</ta>
            <ta e="T256" id="Seg_3468" s="T253">Das Mädchen ging zum Fluss.</ta>
            <ta e="T259" id="Seg_3469" s="T256">Eine Frau steht dort.</ta>
            <ta e="T262" id="Seg_3470" s="T259">„Lass uns nach Hause gehen zu deinem Vater!“</ta>
            <ta e="T273" id="Seg_3471" s="T262">„Ich werde gehen, Ich werde meinen Mann fragen, wenn er mir sagt: geh!, dann werde ich gehen.“</ta>
            <ta e="T282" id="Seg_3472" s="T273">Dann kam die (Schlange?), sie biss die Frau.</ta>
            <ta e="T289" id="Seg_3473" s="T282">Und die Frau [sagt]: „Geh zu [deinem?] Vater und erzähl [ihm]:</ta>
            <ta e="T295" id="Seg_3474" s="T289">Seine Tochter will dort zu dem Mann gehen, er ist [fort] auf Jagd.“</ta>
            <ta e="T298" id="Seg_3475" s="T295">Dann ging (sie?) [zu ihrem Vater].</ta>
            <ta e="T304" id="Seg_3476" s="T298">Ihr Vater (saß?), [dann] sagte [er]: „Lass sie gehen!“</ta>
            <ta e="T308" id="Seg_3477" s="T304">Dann gab er ihr eine Menge [Sachen].</ta>
            <ta e="T311" id="Seg_3478" s="T308">Sie kam, erzählte [ihrem Mann].</ta>
            <ta e="T316" id="Seg_3479" s="T311">Dann tranken sie Vodka.</ta>
            <ta e="T320" id="Seg_3480" s="T316">Dann [sagte] ihr Vater: „Komm zu mir!“</ta>
            <ta e="T323" id="Seg_3481" s="T320">Sie gingen nicht.</ta>
            <ta e="T330" id="Seg_3482" s="T323">Dann baute er ihnen ein Haus.</ta>
            <ta e="T333" id="Seg_3483" s="T330">Er gab alles.</ta>
            <ta e="T334" id="Seg_3484" s="T333">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T93" id="Seg_3485" s="T89">[KlT:] Unnecessary additional transitivizer suffix.</ta>
            <ta e="T101" id="Seg_3486" s="T97">[GVY:] Both t' are pronounced as a palatal, almost as [k'], in this sentence. "Xatel" is pronounced witn an "i" in the second syllable, but in the next sentences it becomes "e".</ta>
            <ta e="T181" id="Seg_3487" s="T179">[KlT:] Pɨnzə ”snake”? No proof, but probable. [GVY:] penzit (Donner 51:b) 'dienstbare Geister des Schamanen, Teufel'. Here without -t 3SG.</ta>
            <ta e="T201" id="Seg_3488" s="T197">[KlT:] Ši ’hole’ - cave?</ta>
            <ta e="T215" id="Seg_3489" s="T211">[GVY:] The genitive of kuzan is strange. Cf. also below, sentence 47.</ta>
            <ta e="T231" id="Seg_3490" s="T227">[GVY:] Sʼarlaʔtə?</ta>
            <ta e="T262" id="Seg_3491" s="T259">[GVY:] abandə pronounced with a very long "a": abaːndə (&lt; abagəndə?)</ta>
            <ta e="T273" id="Seg_3492" s="T262">[KlT:] GEN instead of ACC.</ta>
            <ta e="T289" id="Seg_3493" s="T282">[GVY:] Here, too, abaːndə with a very long a. [AAV:] Unclear to whom the woman speaks, to the girl or to the snake?</ta>
            <ta e="T298" id="Seg_3494" s="T295">[AAV:] Unclear who went to the father</ta>
            <ta e="T304" id="Seg_3495" s="T298">[AAV:] Perhaps "abat" as GEN in the sense "by her father (Rus. "у отца") [she stayed]"?</ta>
            <ta e="T311" id="Seg_3496" s="T308">[AAV:] Who?</ta>
            <ta e="T320" id="Seg_3497" s="T316">[GVY:] miʔnʼibəʔ</ta>
            <ta e="T330" id="Seg_3498" s="T323">[KlT:] Jaʔ- lit. ’chop’, here ’build’.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
