<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID9834BA1A-3420-7D79-3CC2-ED3B52E7AF4B">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_RedCap_flk.wav" />
         <referenced-file url="PKZ_196X_RedCap_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_RedCap_flk\PKZ_196X_RedCap_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">277</ud-information>
            <ud-information attribute-name="# HIAT:w">171</ud-information>
            <ud-information attribute-name="# e">171</ud-information>
            <ud-information attribute-name="# HIAT:u">44</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.005" type="appl" />
         <tli id="T1" time="0.889" type="appl" />
         <tli id="T2" time="1.772" type="appl" />
         <tli id="T3" time="2.656" type="appl" />
         <tli id="T4" time="3.54" type="appl" />
         <tli id="T5" time="4.423" type="appl" />
         <tli id="T6" time="5.307" type="appl" />
         <tli id="T7" time="6.216" type="appl" />
         <tli id="T8" time="7.091" type="appl" />
         <tli id="T9" time="7.965" type="appl" />
         <tli id="T10" time="8.839" type="appl" />
         <tli id="T11" time="9.69" type="appl" />
         <tli id="T12" time="10.54" type="appl" />
         <tli id="T13" time="11.391" type="appl" />
         <tli id="T14" time="12.242" type="appl" />
         <tli id="T15" time="13.093" type="appl" />
         <tli id="T16" time="13.943" type="appl" />
         <tli id="T17" time="14.794" type="appl" />
         <tli id="T18" time="15.794" type="appl" />
         <tli id="T19" time="16.794" type="appl" />
         <tli id="T20" time="17.314" type="appl" />
         <tli id="T21" time="17.834" type="appl" />
         <tli id="T22" time="18.355" type="appl" />
         <tli id="T23" time="18.875" type="appl" />
         <tli id="T24" time="19.754" type="appl" />
         <tli id="T25" time="20.633" type="appl" />
         <tli id="T26" time="21.513" type="appl" />
         <tli id="T27" time="22.392" type="appl" />
         <tli id="T28" time="24.192230930465687" />
         <tli id="T29" time="25.724" type="appl" />
         <tli id="T30" time="27.195" type="appl" />
         <tli id="T31" time="28.666" type="appl" />
         <tli id="T32" time="29.592" type="appl" />
         <tli id="T33" time="31.33190558643944" />
         <tli id="T34" time="32.249" type="appl" />
         <tli id="T35" time="32.945" type="appl" />
         <tli id="T36" time="33.641" type="appl" />
         <tli id="T37" time="34.044" type="appl" />
         <tli id="T38" time="34.446" type="appl" />
         <tli id="T39" time="34.849" type="appl" />
         <tli id="T40" time="35.252" type="appl" />
         <tli id="T41" time="35.813" type="appl" />
         <tli id="T42" time="36.375" type="appl" />
         <tli id="T43" time="36.936" type="appl" />
         <tli id="T44" time="37.737" type="appl" />
         <tli id="T45" time="38.537" type="appl" />
         <tli id="T46" time="40.171502779549805" />
         <tli id="T47" time="41.344" type="appl" />
         <tli id="T48" time="41.826" type="appl" />
         <tli id="T49" time="42.307" type="appl" />
         <tli id="T50" time="42.788" type="appl" />
         <tli id="T51" time="43.27" type="appl" />
         <tli id="T52" time="44.55" type="appl" />
         <tli id="T53" time="45.829" type="appl" />
         <tli id="T54" time="47.108" type="appl" />
         <tli id="T55" time="48.388" type="appl" />
         <tli id="T56" time="49.668" type="appl" />
         <tli id="T57" time="50.947" type="appl" />
         <tli id="T58" time="52.226" type="appl" />
         <tli id="T59" time="54.70417388134511" />
         <tli id="T60" time="55.712" type="appl" />
         <tli id="T61" time="56.319" type="appl" />
         <tli id="T62" time="56.926" type="appl" />
         <tli id="T63" time="57.532" type="appl" />
         <tli id="T64" time="58.051" type="appl" />
         <tli id="T65" time="58.57" type="appl" />
         <tli id="T66" time="59.089" type="appl" />
         <tli id="T67" time="59.728" type="appl" />
         <tli id="T68" time="60.368" type="appl" />
         <tli id="T69" time="61.007" type="appl" />
         <tli id="T70" time="61.646" type="appl" />
         <tli id="T71" time="62.286" type="appl" />
         <tli id="T72" time="64.25707190376379" />
         <tli id="T73" time="64.999" type="appl" />
         <tli id="T74" time="65.77" type="appl" />
         <tli id="T75" time="66.54" type="appl" />
         <tli id="T76" time="67.255" type="appl" />
         <tli id="T77" time="67.97" type="appl" />
         <tli id="T78" time="68.685" type="appl" />
         <tli id="T79" time="69.4" type="appl" />
         <tli id="T80" time="71.09009386676388" />
         <tli id="T81" time="72.142" type="appl" />
         <tli id="T82" time="73.217" type="appl" />
         <tli id="T83" time="76.88982958170054" />
         <tli id="T84" time="77.551" type="appl" />
         <tli id="T85" time="78.21" type="appl" />
         <tli id="T86" time="78.87" type="appl" />
         <tli id="T87" time="79.53" type="appl" />
         <tli id="T88" time="80.334" type="appl" />
         <tli id="T89" time="81.138" type="appl" />
         <tli id="T90" time="81.943" type="appl" />
         <tli id="T91" time="82.747" type="appl" />
         <tli id="T92" time="83.422" type="appl" />
         <tli id="T93" time="84.098" type="appl" />
         <tli id="T94" time="84.789" type="appl" />
         <tli id="T95" time="85.48" type="appl" />
         <tli id="T96" time="86.17" type="appl" />
         <tli id="T97" time="86.861" type="appl" />
         <tli id="T98" time="87.552" type="appl" />
         <tli id="T99" time="88.243" type="appl" />
         <tli id="T100" time="88.892" type="appl" />
         <tli id="T101" time="89.542" type="appl" />
         <tli id="T102" time="90.217" type="appl" />
         <tli id="T103" time="90.892" type="appl" />
         <tli id="T104" time="91.567" type="appl" />
         <tli id="T105" time="92.242" type="appl" />
         <tli id="T106" time="92.917" type="appl" />
         <tli id="T107" time="93.575" type="appl" />
         <tli id="T108" time="94.232" type="appl" />
         <tli id="T109" time="94.89" type="appl" />
         <tli id="T110" time="95.471" type="appl" />
         <tli id="T111" time="96.052" type="appl" />
         <tli id="T112" time="96.632" type="appl" />
         <tli id="T113" time="97.213" type="appl" />
         <tli id="T114" time="97.967" type="appl" />
         <tli id="T115" time="98.722" type="appl" />
         <tli id="T116" time="99.476" type="appl" />
         <tli id="T117" time="100.489" type="appl" />
         <tli id="T118" time="101.199" type="appl" />
         <tli id="T119" time="101.908" type="appl" />
         <tli id="T120" time="102.618" type="appl" />
         <tli id="T121" time="103.543" type="appl" />
         <tli id="T122" time="104.468" type="appl" />
         <tli id="T123" time="105.95517178529117" />
         <tli id="T124" time="106.949" type="appl" />
         <tli id="T125" time="107.849" type="appl" />
         <tli id="T126" time="108.749" type="appl" />
         <tli id="T127" time="109.649" type="appl" />
         <tli id="T128" time="110.549" type="appl" />
         <tli id="T129" time="111.312" type="appl" />
         <tli id="T130" time="112.074" type="appl" />
         <tli id="T131" time="112.837" type="appl" />
         <tli id="T132" time="113.531" type="appl" />
         <tli id="T133" time="114.226" type="appl" />
         <tli id="T134" time="114.92" type="appl" />
         <tli id="T135" time="115.614" type="appl" />
         <tli id="T136" time="116.258" type="appl" />
         <tli id="T137" time="116.902" type="appl" />
         <tli id="T138" time="117.547" type="appl" />
         <tli id="T139" time="118.191" type="appl" />
         <tli id="T140" time="118.835" type="appl" />
         <tli id="T141" time="119.479" type="appl" />
         <tli id="T142" time="120.124" type="appl" />
         <tli id="T143" time="120.768" type="appl" />
         <tli id="T144" time="121.412" type="appl" />
         <tli id="T145" time="122.008" type="appl" />
         <tli id="T146" time="122.603" type="appl" />
         <tli id="T147" time="123.198" type="appl" />
         <tli id="T148" time="123.794" type="appl" />
         <tli id="T149" time="124.463" type="appl" />
         <tli id="T150" time="125.132" type="appl" />
         <tli id="T151" time="125.801" type="appl" />
         <tli id="T152" time="126.47" type="appl" />
         <tli id="T153" time="127.378" type="appl" />
         <tli id="T154" time="128.287" type="appl" />
         <tli id="T155" time="129.653" type="appl" />
         <tli id="T156" time="131.018" type="appl" />
         <tli id="T157" time="132.384" type="appl" />
         <tli id="T158" time="133.749" type="appl" />
         <tli id="T159" time="135.115" type="appl" />
         <tli id="T160" time="135.998" type="appl" />
         <tli id="T161" time="136.881" type="appl" />
         <tli id="T162" time="137.764" type="appl" />
         <tli id="T163" time="138.646" type="appl" />
         <tli id="T164" time="139.529" type="appl" />
         <tli id="T165" time="140.87358060694433" />
         <tli id="T166" time="141.873" type="appl" />
         <tli id="T167" time="142.607" type="appl" />
         <tli id="T168" time="143.34" type="appl" />
         <tli id="T169" time="144.074" type="appl" />
         <tli id="T170" time="144.807" type="appl" />
         <tli id="T171" time="146.3" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T171" id="Seg_0" n="sc" s="T0">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Amnobiʔi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">ijat</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">i</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">koʔbdot</ts>
                  <nts id="Seg_14" n="HIAT:ip">,</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_17" n="HIAT:w" s="T4">kömə</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">üžü</ts>
                  <nts id="Seg_21" n="HIAT:ip">.</nts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_24" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">Dĭgəttə</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_28" n="HIAT:ip">(</nts>
                  <ts e="T8" id="Seg_30" n="HIAT:w" s="T7">urg-</ts>
                  <nts id="Seg_31" n="HIAT:ip">)</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">urgaja</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">ĭzemnuʔpi</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_41" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">Iat</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">măndə:</ts>
                  <nts id="Seg_47" n="HIAT:ip">"</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_50" n="HIAT:w" s="T12">Kanaʔ</ts>
                  <nts id="Seg_51" n="HIAT:ip">,</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_54" n="HIAT:w" s="T13">urgaja</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_57" n="HIAT:w" s="T14">kut</ts>
                  <nts id="Seg_58" n="HIAT:ip">,</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_61" n="HIAT:w" s="T15">dĭ</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">ĭzemneʔbə</ts>
                  <nts id="Seg_65" n="HIAT:ip">"</nts>
                  <nts id="Seg_66" n="HIAT:ip">.</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_69" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_71" n="HIAT:w" s="T17">Pirogəʔi</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_74" n="HIAT:w" s="T18">pürbi</ts>
                  <nts id="Seg_75" n="HIAT:ip">.</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_78" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_80" n="HIAT:w" s="T19">Kajaʔ</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_83" n="HIAT:w" s="T20">embi</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_86" n="HIAT:w" s="T21">da</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_89" n="HIAT:w" s="T22">kambi</ts>
                  <nts id="Seg_90" n="HIAT:ip">.</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_93" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_95" n="HIAT:w" s="T23">Keʔbde</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_98" n="HIAT:w" s="T24">oʔbdəbi</ts>
                  <nts id="Seg_99" n="HIAT:ip">,</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_102" n="HIAT:w" s="T25">svʼetogəʔi</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_104" n="HIAT:ip">(</nts>
                  <ts e="T27" id="Seg_106" n="HIAT:w" s="T26">mĭmbi-</ts>
                  <nts id="Seg_107" n="HIAT:ip">)</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_110" n="HIAT:w" s="T27">nĭŋgəbi</ts>
                  <nts id="Seg_111" n="HIAT:ip">.</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_114" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_116" n="HIAT:w" s="T28">Šonəga</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_119" n="HIAT:w" s="T29">dĭʔnə</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_122" n="HIAT:w" s="T30">volk</ts>
                  <nts id="Seg_123" n="HIAT:ip">.</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_126" n="HIAT:u" s="T31">
                  <nts id="Seg_127" n="HIAT:ip">"</nts>
                  <ts e="T32" id="Seg_129" n="HIAT:w" s="T31">Gibər</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_132" n="HIAT:w" s="T32">kandəgal</ts>
                  <nts id="Seg_133" n="HIAT:ip">?</nts>
                  <nts id="Seg_134" n="HIAT:ip">"</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_137" n="HIAT:u" s="T33">
                  <nts id="Seg_138" n="HIAT:ip">"</nts>
                  <ts e="T34" id="Seg_140" n="HIAT:w" s="T33">Urgajanə</ts>
                  <nts id="Seg_141" n="HIAT:ip">,</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_144" n="HIAT:w" s="T34">dĭ</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_147" n="HIAT:w" s="T35">ĭzemneʔbə</ts>
                  <nts id="Seg_148" n="HIAT:ip">"</nts>
                  <nts id="Seg_149" n="HIAT:ip">.</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_152" n="HIAT:u" s="T36">
                  <nts id="Seg_153" n="HIAT:ip">"</nts>
                  <ts e="T37" id="Seg_155" n="HIAT:w" s="T36">A</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_158" n="HIAT:w" s="T37">dĭ</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_161" n="HIAT:w" s="T38">gijen</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_164" n="HIAT:w" s="T39">amnolaʔbə</ts>
                  <nts id="Seg_165" n="HIAT:ip">?</nts>
                  <nts id="Seg_166" n="HIAT:ip">"</nts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_169" n="HIAT:u" s="T40">
                  <nts id="Seg_170" n="HIAT:ip">"</nts>
                  <ts e="T41" id="Seg_172" n="HIAT:w" s="T40">Dĭn</ts>
                  <nts id="Seg_173" n="HIAT:ip">,</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_176" n="HIAT:w" s="T41">ej</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_179" n="HIAT:w" s="T42">kuŋgəʔ</ts>
                  <nts id="Seg_180" n="HIAT:ip">"</nts>
                  <nts id="Seg_181" n="HIAT:ip">.</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_184" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_186" n="HIAT:w" s="T43">Dĭgəttə</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_189" n="HIAT:w" s="T44">volk</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_192" n="HIAT:w" s="T45">nuʔməluʔpi</ts>
                  <nts id="Seg_193" n="HIAT:ip">.</nts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_196" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_198" n="HIAT:w" s="T46">Küzürbi</ts>
                  <nts id="Seg_199" n="HIAT:ip">.</nts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_202" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_204" n="HIAT:w" s="T47">Nüke</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_207" n="HIAT:w" s="T48">măndə:</ts>
                  <nts id="Seg_208" n="HIAT:ip">"</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_211" n="HIAT:w" s="T49">Šində</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_214" n="HIAT:w" s="T50">dĭn</ts>
                  <nts id="Seg_215" n="HIAT:ip">?</nts>
                  <nts id="Seg_216" n="HIAT:ip">"</nts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_219" n="HIAT:u" s="T51">
                  <nts id="Seg_220" n="HIAT:ip">"</nts>
                  <ts e="T52" id="Seg_222" n="HIAT:w" s="T51">Măn</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_225" n="HIAT:w" s="T52">tăn</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_228" n="HIAT:w" s="T53">vnučkal</ts>
                  <nts id="Seg_229" n="HIAT:ip">,</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_232" n="HIAT:w" s="T54">kömə</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_235" n="HIAT:w" s="T55">üžü</ts>
                  <nts id="Seg_236" n="HIAT:ip">,</nts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_239" n="HIAT:w" s="T56">tănan</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_242" n="HIAT:w" s="T57">šobial</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_245" n="HIAT:w" s="T58">măndərzittə</ts>
                  <nts id="Seg_246" n="HIAT:ip">"</nts>
                  <nts id="Seg_247" n="HIAT:ip">.</nts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_250" n="HIAT:u" s="T59">
                  <nts id="Seg_251" n="HIAT:ip">"</nts>
                  <ts e="T60" id="Seg_253" n="HIAT:w" s="T59">Ködərdə</ts>
                  <nts id="Seg_254" n="HIAT:ip">,</nts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_257" n="HIAT:w" s="T60">ajə</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_260" n="HIAT:w" s="T61">i</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_263" n="HIAT:w" s="T62">šoləl</ts>
                  <nts id="Seg_264" n="HIAT:ip">"</nts>
                  <nts id="Seg_265" n="HIAT:ip">.</nts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_268" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_270" n="HIAT:w" s="T63">Dĭgəttə</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_273" n="HIAT:w" s="T64">dĭ</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_276" n="HIAT:w" s="T65">šobi</ts>
                  <nts id="Seg_277" n="HIAT:ip">.</nts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_280" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_282" n="HIAT:w" s="T66">Dĭ</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_285" n="HIAT:w" s="T67">nükem</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_288" n="HIAT:w" s="T68">amluʔpi</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_291" n="HIAT:w" s="T69">i</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_294" n="HIAT:w" s="T70">bostə</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_297" n="HIAT:w" s="T71">iʔbəbi</ts>
                  <nts id="Seg_298" n="HIAT:ip">.</nts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_301" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_303" n="HIAT:w" s="T72">Nüken</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_306" n="HIAT:w" s="T73">üžübə</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_309" n="HIAT:w" s="T74">šerbi</ts>
                  <nts id="Seg_310" n="HIAT:ip">.</nts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_313" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_315" n="HIAT:w" s="T75">Dĭgəttə</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_318" n="HIAT:w" s="T76">šobi</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_321" n="HIAT:w" s="T77">koʔbdo</ts>
                  <nts id="Seg_322" n="HIAT:ip">,</nts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_325" n="HIAT:w" s="T78">kömə</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_328" n="HIAT:w" s="T79">üžü</ts>
                  <nts id="Seg_329" n="HIAT:ip">.</nts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_332" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_334" n="HIAT:w" s="T80">Küzürleʔbə</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_337" n="HIAT:w" s="T81">ajəndə</ts>
                  <nts id="Seg_338" n="HIAT:ip">.</nts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_341" n="HIAT:u" s="T82">
                  <nts id="Seg_342" n="HIAT:ip">"</nts>
                  <ts e="T83" id="Seg_344" n="HIAT:w" s="T82">Kardə</ts>
                  <nts id="Seg_345" n="HIAT:ip">!</nts>
                  <nts id="Seg_346" n="HIAT:ip">"</nts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_349" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_351" n="HIAT:w" s="T83">Dĭ</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_354" n="HIAT:w" s="T84">karluʔpi</ts>
                  <nts id="Seg_355" n="HIAT:ip">,</nts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_358" n="HIAT:w" s="T85">dĭ</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_361" n="HIAT:w" s="T86">šobi</ts>
                  <nts id="Seg_362" n="HIAT:ip">.</nts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_365" n="HIAT:u" s="T87">
                  <nts id="Seg_366" n="HIAT:ip">"</nts>
                  <ts e="T88" id="Seg_368" n="HIAT:w" s="T87">Endə</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_371" n="HIAT:w" s="T88">stoldə</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_374" n="HIAT:w" s="T89">pirog</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_377" n="HIAT:w" s="T90">kajaʔ</ts>
                  <nts id="Seg_378" n="HIAT:ip">!</nts>
                  <nts id="Seg_379" n="HIAT:ip">"</nts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_382" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_384" n="HIAT:w" s="T91">Dĭ</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_387" n="HIAT:w" s="T92">embi</ts>
                  <nts id="Seg_388" n="HIAT:ip">.</nts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T99" id="Seg_391" n="HIAT:u" s="T93">
                  <nts id="Seg_392" n="HIAT:ip">"</nts>
                  <ts e="T94" id="Seg_394" n="HIAT:w" s="T93">No</ts>
                  <nts id="Seg_395" n="HIAT:ip">,</nts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_398" n="HIAT:w" s="T94">šoʔ</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_401" n="HIAT:w" s="T95">măna</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_404" n="HIAT:w" s="T96">toːndə</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_406" n="HIAT:ip">(</nts>
                  <ts e="T98" id="Seg_408" n="HIAT:w" s="T97">amnolaʔ-</ts>
                  <nts id="Seg_409" n="HIAT:ip">)</nts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_412" n="HIAT:w" s="T98">amnaʔ</ts>
                  <nts id="Seg_413" n="HIAT:ip">!</nts>
                  <nts id="Seg_414" n="HIAT:ip">"</nts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_417" n="HIAT:u" s="T99">
                  <ts e="T100" id="Seg_419" n="HIAT:w" s="T99">Dĭ</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_422" n="HIAT:w" s="T100">amnolaʔbə</ts>
                  <nts id="Seg_423" n="HIAT:ip">.</nts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_426" n="HIAT:u" s="T101">
                  <nts id="Seg_427" n="HIAT:ip">"</nts>
                  <ts e="T102" id="Seg_429" n="HIAT:w" s="T101">Ĭmbi</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_432" n="HIAT:w" s="T102">tăn</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_435" n="HIAT:w" s="T103">kuzaŋdə</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_438" n="HIAT:w" s="T104">dĭrgit</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_441" n="HIAT:w" s="T105">urgoʔi</ts>
                  <nts id="Seg_442" n="HIAT:ip">?</nts>
                  <nts id="Seg_443" n="HIAT:ip">"</nts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_446" n="HIAT:u" s="T106">
                  <nts id="Seg_447" n="HIAT:ip">"</nts>
                  <ts e="T107" id="Seg_449" n="HIAT:w" s="T106">Štobɨ</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_452" n="HIAT:w" s="T107">tănan</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_455" n="HIAT:w" s="T108">nʼilgösʼtə</ts>
                  <nts id="Seg_456" n="HIAT:ip">"</nts>
                  <nts id="Seg_457" n="HIAT:ip">.</nts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_460" n="HIAT:u" s="T109">
                  <nts id="Seg_461" n="HIAT:ip">"</nts>
                  <ts e="T110" id="Seg_463" n="HIAT:w" s="T109">A</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_466" n="HIAT:w" s="T110">ĭmbi</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_469" n="HIAT:w" s="T111">sʼimazaŋdə</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_472" n="HIAT:w" s="T112">urgoʔi</ts>
                  <nts id="Seg_473" n="HIAT:ip">?</nts>
                  <nts id="Seg_474" n="HIAT:ip">"</nts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_477" n="HIAT:u" s="T113">
                  <nts id="Seg_478" n="HIAT:ip">"</nts>
                  <ts e="T114" id="Seg_480" n="HIAT:w" s="T113">Štobɨ</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_483" n="HIAT:w" s="T114">tănan</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_486" n="HIAT:w" s="T115">măndərzittə</ts>
                  <nts id="Seg_487" n="HIAT:ip">"</nts>
                  <nts id="Seg_488" n="HIAT:ip">.</nts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_491" n="HIAT:u" s="T116">
                  <nts id="Seg_492" n="HIAT:ip">"</nts>
                  <ts e="T117" id="Seg_494" n="HIAT:w" s="T116">A</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_497" n="HIAT:w" s="T117">ĭmbi</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_500" n="HIAT:w" s="T118">udaʔi</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_503" n="HIAT:w" s="T119">numoʔi</ts>
                  <nts id="Seg_504" n="HIAT:ip">?</nts>
                  <nts id="Seg_505" n="HIAT:ip">"</nts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_508" n="HIAT:u" s="T120">
                  <nts id="Seg_509" n="HIAT:ip">"</nts>
                  <ts e="T121" id="Seg_511" n="HIAT:w" s="T120">Štobɨ</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_514" n="HIAT:w" s="T121">tănan</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_516" n="HIAT:ip">(</nts>
                  <ts e="T123" id="Seg_518" n="HIAT:w" s="T122">kamnərosʼtə</ts>
                  <nts id="Seg_519" n="HIAT:ip">)</nts>
                  <nts id="Seg_520" n="HIAT:ip">"</nts>
                  <nts id="Seg_521" n="HIAT:ip">.</nts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_524" n="HIAT:u" s="T123">
                  <nts id="Seg_525" n="HIAT:ip">"</nts>
                  <ts e="T124" id="Seg_527" n="HIAT:w" s="T123">A</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_530" n="HIAT:w" s="T124">ĭmbi</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_533" n="HIAT:w" s="T125">tăn</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_536" n="HIAT:w" s="T126">aŋdə</ts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_539" n="HIAT:w" s="T127">urgo</ts>
                  <nts id="Seg_540" n="HIAT:ip">?</nts>
                  <nts id="Seg_541" n="HIAT:ip">"</nts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_544" n="HIAT:u" s="T128">
                  <nts id="Seg_545" n="HIAT:ip">"</nts>
                  <ts e="T129" id="Seg_547" n="HIAT:w" s="T128">Štobɨ</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_550" n="HIAT:w" s="T129">tănan</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_553" n="HIAT:w" s="T130">amzittə</ts>
                  <nts id="Seg_554" n="HIAT:ip">!</nts>
                  <nts id="Seg_555" n="HIAT:ip">"</nts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T135" id="Seg_558" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_560" n="HIAT:w" s="T131">Dĭgəttə</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_563" n="HIAT:w" s="T132">kabarlaʔ</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_566" n="HIAT:w" s="T133">ibi</ts>
                  <nts id="Seg_567" n="HIAT:ip">,</nts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_570" n="HIAT:w" s="T134">amnuʔpi</ts>
                  <nts id="Seg_571" n="HIAT:ip">.</nts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_574" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_576" n="HIAT:w" s="T135">Kajak</ts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_579" n="HIAT:w" s="T136">amnuʔpi</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_582" n="HIAT:w" s="T137">i</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_585" n="HIAT:w" s="T138">pirog</ts>
                  <nts id="Seg_586" n="HIAT:ip">,</nts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_589" n="HIAT:w" s="T139">i</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_592" n="HIAT:w" s="T140">bostə</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_595" n="HIAT:w" s="T141">iʔbolaʔpi</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_598" n="HIAT:w" s="T142">i</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_601" n="HIAT:w" s="T143">kunollaʔpi</ts>
                  <nts id="Seg_602" n="HIAT:ip">.</nts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_605" n="HIAT:u" s="T144">
                  <ts e="T145" id="Seg_607" n="HIAT:w" s="T144">Dön</ts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_610" n="HIAT:w" s="T145">il</ts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_613" n="HIAT:w" s="T146">šobiʔi</ts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_616" n="HIAT:w" s="T147">multuksiʔ</ts>
                  <nts id="Seg_617" n="HIAT:ip">.</nts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_620" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_622" n="HIAT:w" s="T148">Dĭ</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_625" n="HIAT:w" s="T149">bar</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_628" n="HIAT:w" s="T150">kunollaʔbə</ts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_631" n="HIAT:w" s="T151">tăŋ</ts>
                  <nts id="Seg_632" n="HIAT:ip">.</nts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T154" id="Seg_635" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_637" n="HIAT:w" s="T152">Dĭgəttə</ts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_640" n="HIAT:w" s="T153">šobiʔi</ts>
                  <nts id="Seg_641" n="HIAT:ip">.</nts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T159" id="Seg_644" n="HIAT:u" s="T154">
                  <ts e="T155" id="Seg_646" n="HIAT:w" s="T154">I</ts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_649" n="HIAT:w" s="T155">dĭm</ts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_652" n="HIAT:w" s="T156">nanabə</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_655" n="HIAT:w" s="T157">dagajziʔ</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_658" n="HIAT:w" s="T158">müʔluʔpiʔi</ts>
                  <nts id="Seg_659" n="HIAT:ip">.</nts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T165" id="Seg_662" n="HIAT:u" s="T159">
                  <ts e="T160" id="Seg_664" n="HIAT:w" s="T159">Dĭzeŋ</ts>
                  <nts id="Seg_665" n="HIAT:ip">,</nts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_668" n="HIAT:w" s="T160">nüke</ts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_671" n="HIAT:w" s="T161">i</ts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_674" n="HIAT:w" s="T162">koʔbdo</ts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_677" n="HIAT:w" s="T163">supsoluʔpiʔi</ts>
                  <nts id="Seg_678" n="HIAT:ip">,</nts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_681" n="HIAT:w" s="T164">dʼiliʔi</ts>
                  <nts id="Seg_682" n="HIAT:ip">.</nts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T170" id="Seg_685" n="HIAT:u" s="T165">
                  <ts e="T166" id="Seg_687" n="HIAT:w" s="T165">A</ts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_690" n="HIAT:w" s="T166">volktə</ts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_693" n="HIAT:w" s="T167">bar</ts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_696" n="HIAT:w" s="T168">dʼagarlaʔ</ts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_699" n="HIAT:w" s="T169">kuʔpiʔi</ts>
                  <nts id="Seg_700" n="HIAT:ip">.</nts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T171" id="Seg_703" n="HIAT:u" s="T170">
                  <ts e="T171" id="Seg_705" n="HIAT:w" s="T170">Kabarləj</ts>
                  <nts id="Seg_706" n="HIAT:ip">.</nts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T171" id="Seg_708" n="sc" s="T0">
               <ts e="T1" id="Seg_710" n="e" s="T0">Amnobiʔi </ts>
               <ts e="T2" id="Seg_712" n="e" s="T1">ijat </ts>
               <ts e="T3" id="Seg_714" n="e" s="T2">i </ts>
               <ts e="T4" id="Seg_716" n="e" s="T3">koʔbdot, </ts>
               <ts e="T5" id="Seg_718" n="e" s="T4">kömə </ts>
               <ts e="T6" id="Seg_720" n="e" s="T5">üžü. </ts>
               <ts e="T7" id="Seg_722" n="e" s="T6">Dĭgəttə </ts>
               <ts e="T8" id="Seg_724" n="e" s="T7">(urg-) </ts>
               <ts e="T9" id="Seg_726" n="e" s="T8">urgaja </ts>
               <ts e="T10" id="Seg_728" n="e" s="T9">ĭzemnuʔpi. </ts>
               <ts e="T11" id="Seg_730" n="e" s="T10">Iat </ts>
               <ts e="T12" id="Seg_732" n="e" s="T11">măndə:" </ts>
               <ts e="T13" id="Seg_734" n="e" s="T12">Kanaʔ, </ts>
               <ts e="T14" id="Seg_736" n="e" s="T13">urgaja </ts>
               <ts e="T15" id="Seg_738" n="e" s="T14">kut, </ts>
               <ts e="T16" id="Seg_740" n="e" s="T15">dĭ </ts>
               <ts e="T17" id="Seg_742" n="e" s="T16">ĭzemneʔbə". </ts>
               <ts e="T18" id="Seg_744" n="e" s="T17">Pirogəʔi </ts>
               <ts e="T19" id="Seg_746" n="e" s="T18">pürbi. </ts>
               <ts e="T20" id="Seg_748" n="e" s="T19">Kajaʔ </ts>
               <ts e="T21" id="Seg_750" n="e" s="T20">embi </ts>
               <ts e="T22" id="Seg_752" n="e" s="T21">da </ts>
               <ts e="T23" id="Seg_754" n="e" s="T22">kambi. </ts>
               <ts e="T24" id="Seg_756" n="e" s="T23">Keʔbde </ts>
               <ts e="T25" id="Seg_758" n="e" s="T24">oʔbdəbi, </ts>
               <ts e="T26" id="Seg_760" n="e" s="T25">svʼetogəʔi </ts>
               <ts e="T27" id="Seg_762" n="e" s="T26">(mĭmbi-) </ts>
               <ts e="T28" id="Seg_764" n="e" s="T27">nĭŋgəbi. </ts>
               <ts e="T29" id="Seg_766" n="e" s="T28">Šonəga </ts>
               <ts e="T30" id="Seg_768" n="e" s="T29">dĭʔnə </ts>
               <ts e="T31" id="Seg_770" n="e" s="T30">volk. </ts>
               <ts e="T32" id="Seg_772" n="e" s="T31">"Gibər </ts>
               <ts e="T33" id="Seg_774" n="e" s="T32">kandəgal?" </ts>
               <ts e="T34" id="Seg_776" n="e" s="T33">"Urgajanə, </ts>
               <ts e="T35" id="Seg_778" n="e" s="T34">dĭ </ts>
               <ts e="T36" id="Seg_780" n="e" s="T35">ĭzemneʔbə". </ts>
               <ts e="T37" id="Seg_782" n="e" s="T36">"A </ts>
               <ts e="T38" id="Seg_784" n="e" s="T37">dĭ </ts>
               <ts e="T39" id="Seg_786" n="e" s="T38">gijen </ts>
               <ts e="T40" id="Seg_788" n="e" s="T39">amnolaʔbə?" </ts>
               <ts e="T41" id="Seg_790" n="e" s="T40">"Dĭn, </ts>
               <ts e="T42" id="Seg_792" n="e" s="T41">ej </ts>
               <ts e="T43" id="Seg_794" n="e" s="T42">kuŋgəʔ". </ts>
               <ts e="T44" id="Seg_796" n="e" s="T43">Dĭgəttə </ts>
               <ts e="T45" id="Seg_798" n="e" s="T44">volk </ts>
               <ts e="T46" id="Seg_800" n="e" s="T45">nuʔməluʔpi. </ts>
               <ts e="T47" id="Seg_802" n="e" s="T46">Küzürbi. </ts>
               <ts e="T48" id="Seg_804" n="e" s="T47">Nüke </ts>
               <ts e="T49" id="Seg_806" n="e" s="T48">măndə:" </ts>
               <ts e="T50" id="Seg_808" n="e" s="T49">Šində </ts>
               <ts e="T51" id="Seg_810" n="e" s="T50">dĭn?" </ts>
               <ts e="T52" id="Seg_812" n="e" s="T51">"Măn </ts>
               <ts e="T53" id="Seg_814" n="e" s="T52">tăn </ts>
               <ts e="T54" id="Seg_816" n="e" s="T53">vnučkal, </ts>
               <ts e="T55" id="Seg_818" n="e" s="T54">kömə </ts>
               <ts e="T56" id="Seg_820" n="e" s="T55">üžü, </ts>
               <ts e="T57" id="Seg_822" n="e" s="T56">tănan </ts>
               <ts e="T58" id="Seg_824" n="e" s="T57">šobial </ts>
               <ts e="T59" id="Seg_826" n="e" s="T58">măndərzittə". </ts>
               <ts e="T60" id="Seg_828" n="e" s="T59">"Ködərdə, </ts>
               <ts e="T61" id="Seg_830" n="e" s="T60">ajə </ts>
               <ts e="T62" id="Seg_832" n="e" s="T61">i </ts>
               <ts e="T63" id="Seg_834" n="e" s="T62">šoləl". </ts>
               <ts e="T64" id="Seg_836" n="e" s="T63">Dĭgəttə </ts>
               <ts e="T65" id="Seg_838" n="e" s="T64">dĭ </ts>
               <ts e="T66" id="Seg_840" n="e" s="T65">šobi. </ts>
               <ts e="T67" id="Seg_842" n="e" s="T66">Dĭ </ts>
               <ts e="T68" id="Seg_844" n="e" s="T67">nükem </ts>
               <ts e="T69" id="Seg_846" n="e" s="T68">amluʔpi </ts>
               <ts e="T70" id="Seg_848" n="e" s="T69">i </ts>
               <ts e="T71" id="Seg_850" n="e" s="T70">bostə </ts>
               <ts e="T72" id="Seg_852" n="e" s="T71">iʔbəbi. </ts>
               <ts e="T73" id="Seg_854" n="e" s="T72">Nüken </ts>
               <ts e="T74" id="Seg_856" n="e" s="T73">üžübə </ts>
               <ts e="T75" id="Seg_858" n="e" s="T74">šerbi. </ts>
               <ts e="T76" id="Seg_860" n="e" s="T75">Dĭgəttə </ts>
               <ts e="T77" id="Seg_862" n="e" s="T76">šobi </ts>
               <ts e="T78" id="Seg_864" n="e" s="T77">koʔbdo, </ts>
               <ts e="T79" id="Seg_866" n="e" s="T78">kömə </ts>
               <ts e="T80" id="Seg_868" n="e" s="T79">üžü. </ts>
               <ts e="T81" id="Seg_870" n="e" s="T80">Küzürleʔbə </ts>
               <ts e="T82" id="Seg_872" n="e" s="T81">ajəndə. </ts>
               <ts e="T83" id="Seg_874" n="e" s="T82">"Kardə!" </ts>
               <ts e="T84" id="Seg_876" n="e" s="T83">Dĭ </ts>
               <ts e="T85" id="Seg_878" n="e" s="T84">karluʔpi, </ts>
               <ts e="T86" id="Seg_880" n="e" s="T85">dĭ </ts>
               <ts e="T87" id="Seg_882" n="e" s="T86">šobi. </ts>
               <ts e="T88" id="Seg_884" n="e" s="T87">"Endə </ts>
               <ts e="T89" id="Seg_886" n="e" s="T88">stoldə </ts>
               <ts e="T90" id="Seg_888" n="e" s="T89">pirog </ts>
               <ts e="T91" id="Seg_890" n="e" s="T90">kajaʔ!" </ts>
               <ts e="T92" id="Seg_892" n="e" s="T91">Dĭ </ts>
               <ts e="T93" id="Seg_894" n="e" s="T92">embi. </ts>
               <ts e="T94" id="Seg_896" n="e" s="T93">"No, </ts>
               <ts e="T95" id="Seg_898" n="e" s="T94">šoʔ </ts>
               <ts e="T96" id="Seg_900" n="e" s="T95">măna </ts>
               <ts e="T97" id="Seg_902" n="e" s="T96">toːndə </ts>
               <ts e="T98" id="Seg_904" n="e" s="T97">(amnolaʔ-) </ts>
               <ts e="T99" id="Seg_906" n="e" s="T98">amnaʔ!" </ts>
               <ts e="T100" id="Seg_908" n="e" s="T99">Dĭ </ts>
               <ts e="T101" id="Seg_910" n="e" s="T100">amnolaʔbə. </ts>
               <ts e="T102" id="Seg_912" n="e" s="T101">"Ĭmbi </ts>
               <ts e="T103" id="Seg_914" n="e" s="T102">tăn </ts>
               <ts e="T104" id="Seg_916" n="e" s="T103">kuzaŋdə </ts>
               <ts e="T105" id="Seg_918" n="e" s="T104">dĭrgit </ts>
               <ts e="T106" id="Seg_920" n="e" s="T105">urgoʔi?" </ts>
               <ts e="T107" id="Seg_922" n="e" s="T106">"Štobɨ </ts>
               <ts e="T108" id="Seg_924" n="e" s="T107">tănan </ts>
               <ts e="T109" id="Seg_926" n="e" s="T108">nʼilgösʼtə". </ts>
               <ts e="T110" id="Seg_928" n="e" s="T109">"A </ts>
               <ts e="T111" id="Seg_930" n="e" s="T110">ĭmbi </ts>
               <ts e="T112" id="Seg_932" n="e" s="T111">sʼimazaŋdə </ts>
               <ts e="T113" id="Seg_934" n="e" s="T112">urgoʔi?" </ts>
               <ts e="T114" id="Seg_936" n="e" s="T113">"Štobɨ </ts>
               <ts e="T115" id="Seg_938" n="e" s="T114">tănan </ts>
               <ts e="T116" id="Seg_940" n="e" s="T115">măndərzittə". </ts>
               <ts e="T117" id="Seg_942" n="e" s="T116">"A </ts>
               <ts e="T118" id="Seg_944" n="e" s="T117">ĭmbi </ts>
               <ts e="T119" id="Seg_946" n="e" s="T118">udaʔi </ts>
               <ts e="T120" id="Seg_948" n="e" s="T119">numoʔi?" </ts>
               <ts e="T121" id="Seg_950" n="e" s="T120">"Štobɨ </ts>
               <ts e="T122" id="Seg_952" n="e" s="T121">tănan </ts>
               <ts e="T123" id="Seg_954" n="e" s="T122">(kamnərosʼtə)". </ts>
               <ts e="T124" id="Seg_956" n="e" s="T123">"A </ts>
               <ts e="T125" id="Seg_958" n="e" s="T124">ĭmbi </ts>
               <ts e="T126" id="Seg_960" n="e" s="T125">tăn </ts>
               <ts e="T127" id="Seg_962" n="e" s="T126">aŋdə </ts>
               <ts e="T128" id="Seg_964" n="e" s="T127">urgo?" </ts>
               <ts e="T129" id="Seg_966" n="e" s="T128">"Štobɨ </ts>
               <ts e="T130" id="Seg_968" n="e" s="T129">tănan </ts>
               <ts e="T131" id="Seg_970" n="e" s="T130">amzittə!" </ts>
               <ts e="T132" id="Seg_972" n="e" s="T131">Dĭgəttə </ts>
               <ts e="T133" id="Seg_974" n="e" s="T132">kabarlaʔ </ts>
               <ts e="T134" id="Seg_976" n="e" s="T133">ibi, </ts>
               <ts e="T135" id="Seg_978" n="e" s="T134">amnuʔpi. </ts>
               <ts e="T136" id="Seg_980" n="e" s="T135">Kajak </ts>
               <ts e="T137" id="Seg_982" n="e" s="T136">amnuʔpi </ts>
               <ts e="T138" id="Seg_984" n="e" s="T137">i </ts>
               <ts e="T139" id="Seg_986" n="e" s="T138">pirog, </ts>
               <ts e="T140" id="Seg_988" n="e" s="T139">i </ts>
               <ts e="T141" id="Seg_990" n="e" s="T140">bostə </ts>
               <ts e="T142" id="Seg_992" n="e" s="T141">iʔbolaʔpi </ts>
               <ts e="T143" id="Seg_994" n="e" s="T142">i </ts>
               <ts e="T144" id="Seg_996" n="e" s="T143">kunollaʔpi. </ts>
               <ts e="T145" id="Seg_998" n="e" s="T144">Dön </ts>
               <ts e="T146" id="Seg_1000" n="e" s="T145">il </ts>
               <ts e="T147" id="Seg_1002" n="e" s="T146">šobiʔi </ts>
               <ts e="T148" id="Seg_1004" n="e" s="T147">multuksiʔ. </ts>
               <ts e="T149" id="Seg_1006" n="e" s="T148">Dĭ </ts>
               <ts e="T150" id="Seg_1008" n="e" s="T149">bar </ts>
               <ts e="T151" id="Seg_1010" n="e" s="T150">kunollaʔbə </ts>
               <ts e="T152" id="Seg_1012" n="e" s="T151">tăŋ. </ts>
               <ts e="T153" id="Seg_1014" n="e" s="T152">Dĭgəttə </ts>
               <ts e="T154" id="Seg_1016" n="e" s="T153">šobiʔi. </ts>
               <ts e="T155" id="Seg_1018" n="e" s="T154">I </ts>
               <ts e="T156" id="Seg_1020" n="e" s="T155">dĭm </ts>
               <ts e="T157" id="Seg_1022" n="e" s="T156">nanabə </ts>
               <ts e="T158" id="Seg_1024" n="e" s="T157">dagajziʔ </ts>
               <ts e="T159" id="Seg_1026" n="e" s="T158">müʔluʔpiʔi. </ts>
               <ts e="T160" id="Seg_1028" n="e" s="T159">Dĭzeŋ, </ts>
               <ts e="T161" id="Seg_1030" n="e" s="T160">nüke </ts>
               <ts e="T162" id="Seg_1032" n="e" s="T161">i </ts>
               <ts e="T163" id="Seg_1034" n="e" s="T162">koʔbdo </ts>
               <ts e="T164" id="Seg_1036" n="e" s="T163">supsoluʔpiʔi, </ts>
               <ts e="T165" id="Seg_1038" n="e" s="T164">dʼiliʔi. </ts>
               <ts e="T166" id="Seg_1040" n="e" s="T165">A </ts>
               <ts e="T167" id="Seg_1042" n="e" s="T166">volktə </ts>
               <ts e="T168" id="Seg_1044" n="e" s="T167">bar </ts>
               <ts e="T169" id="Seg_1046" n="e" s="T168">dʼagarlaʔ </ts>
               <ts e="T170" id="Seg_1048" n="e" s="T169">kuʔpiʔi. </ts>
               <ts e="T171" id="Seg_1050" n="e" s="T170">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_1051" s="T0">PKZ_196X_RedCap_flk.001 (002)</ta>
            <ta e="T10" id="Seg_1052" s="T6">PKZ_196X_RedCap_flk.002 (003)</ta>
            <ta e="T17" id="Seg_1053" s="T10">PKZ_196X_RedCap_flk.003 (004)</ta>
            <ta e="T19" id="Seg_1054" s="T17">PKZ_196X_RedCap_flk.004 (005)</ta>
            <ta e="T23" id="Seg_1055" s="T19">PKZ_196X_RedCap_flk.005 (006)</ta>
            <ta e="T28" id="Seg_1056" s="T23">PKZ_196X_RedCap_flk.006 (007)</ta>
            <ta e="T31" id="Seg_1057" s="T28">PKZ_196X_RedCap_flk.007 (008)</ta>
            <ta e="T33" id="Seg_1058" s="T31">PKZ_196X_RedCap_flk.008 (009)</ta>
            <ta e="T36" id="Seg_1059" s="T33">PKZ_196X_RedCap_flk.009 (010)</ta>
            <ta e="T40" id="Seg_1060" s="T36">PKZ_196X_RedCap_flk.010 (011)</ta>
            <ta e="T43" id="Seg_1061" s="T40">PKZ_196X_RedCap_flk.011 (012)</ta>
            <ta e="T46" id="Seg_1062" s="T43">PKZ_196X_RedCap_flk.012 (013)</ta>
            <ta e="T47" id="Seg_1063" s="T46">PKZ_196X_RedCap_flk.013 (014)</ta>
            <ta e="T51" id="Seg_1064" s="T47">PKZ_196X_RedCap_flk.014 (015)</ta>
            <ta e="T59" id="Seg_1065" s="T51">PKZ_196X_RedCap_flk.015 (016)</ta>
            <ta e="T63" id="Seg_1066" s="T59">PKZ_196X_RedCap_flk.016 (017)</ta>
            <ta e="T66" id="Seg_1067" s="T63">PKZ_196X_RedCap_flk.017 (018)</ta>
            <ta e="T72" id="Seg_1068" s="T66">PKZ_196X_RedCap_flk.018 (019)</ta>
            <ta e="T75" id="Seg_1069" s="T72">PKZ_196X_RedCap_flk.019 (020)</ta>
            <ta e="T80" id="Seg_1070" s="T75">PKZ_196X_RedCap_flk.020 (021)</ta>
            <ta e="T82" id="Seg_1071" s="T80">PKZ_196X_RedCap_flk.021 (022)</ta>
            <ta e="T83" id="Seg_1072" s="T82">PKZ_196X_RedCap_flk.022 (023)</ta>
            <ta e="T87" id="Seg_1073" s="T83">PKZ_196X_RedCap_flk.023 (024)</ta>
            <ta e="T91" id="Seg_1074" s="T87">PKZ_196X_RedCap_flk.024 (025)</ta>
            <ta e="T93" id="Seg_1075" s="T91">PKZ_196X_RedCap_flk.025 (026)</ta>
            <ta e="T99" id="Seg_1076" s="T93">PKZ_196X_RedCap_flk.026 (027)</ta>
            <ta e="T101" id="Seg_1077" s="T99">PKZ_196X_RedCap_flk.027 (028)</ta>
            <ta e="T106" id="Seg_1078" s="T101">PKZ_196X_RedCap_flk.028 (029)</ta>
            <ta e="T109" id="Seg_1079" s="T106">PKZ_196X_RedCap_flk.029 (030)</ta>
            <ta e="T113" id="Seg_1080" s="T109">PKZ_196X_RedCap_flk.030 (031)</ta>
            <ta e="T116" id="Seg_1081" s="T113">PKZ_196X_RedCap_flk.031 (032)</ta>
            <ta e="T120" id="Seg_1082" s="T116">PKZ_196X_RedCap_flk.032 (033)</ta>
            <ta e="T123" id="Seg_1083" s="T120">PKZ_196X_RedCap_flk.033 (034)</ta>
            <ta e="T128" id="Seg_1084" s="T123">PKZ_196X_RedCap_flk.034 (035)</ta>
            <ta e="T131" id="Seg_1085" s="T128">PKZ_196X_RedCap_flk.035 (036)</ta>
            <ta e="T135" id="Seg_1086" s="T131">PKZ_196X_RedCap_flk.036 (037)</ta>
            <ta e="T144" id="Seg_1087" s="T135">PKZ_196X_RedCap_flk.037 (038)</ta>
            <ta e="T148" id="Seg_1088" s="T144">PKZ_196X_RedCap_flk.038 (039)</ta>
            <ta e="T152" id="Seg_1089" s="T148">PKZ_196X_RedCap_flk.039 (040)</ta>
            <ta e="T154" id="Seg_1090" s="T152">PKZ_196X_RedCap_flk.040 (041)</ta>
            <ta e="T159" id="Seg_1091" s="T154">PKZ_196X_RedCap_flk.041 (042)</ta>
            <ta e="T165" id="Seg_1092" s="T159">PKZ_196X_RedCap_flk.042 (043)</ta>
            <ta e="T170" id="Seg_1093" s="T165">PKZ_196X_RedCap_flk.043 (044)</ta>
            <ta e="T171" id="Seg_1094" s="T170">PKZ_196X_RedCap_flk.044 (045)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_1095" s="T0">Amnobiʔi ijat i koʔbdot, kömə üžü. </ta>
            <ta e="T10" id="Seg_1096" s="T6">Dĭgəttə (urg-) urgaja ĭzemnuʔpi. </ta>
            <ta e="T17" id="Seg_1097" s="T10">Iat măndə:" Kanaʔ, urgaja kut, dĭ ĭzemneʔbə". </ta>
            <ta e="T19" id="Seg_1098" s="T17">Pirogəʔi pürbi. </ta>
            <ta e="T23" id="Seg_1099" s="T19">Kajaʔ embi da kambi. </ta>
            <ta e="T28" id="Seg_1100" s="T23">Keʔbde oʔbdəbi, svʼetogəʔi (mĭmbi-) nĭŋgəbi. </ta>
            <ta e="T31" id="Seg_1101" s="T28">Šonəga dĭʔnə volk. </ta>
            <ta e="T33" id="Seg_1102" s="T31">"Gibər kandəgal?" </ta>
            <ta e="T36" id="Seg_1103" s="T33">"Urgajanə, dĭ ĭzemneʔbə". </ta>
            <ta e="T40" id="Seg_1104" s="T36">"A dĭ gijen amnolaʔbə?" </ta>
            <ta e="T43" id="Seg_1105" s="T40">"Dĭn, ej kuŋgəʔ". </ta>
            <ta e="T46" id="Seg_1106" s="T43">Dĭgəttə volk nuʔməluʔpi. </ta>
            <ta e="T47" id="Seg_1107" s="T46">Küzürbi. </ta>
            <ta e="T51" id="Seg_1108" s="T47">Nüke măndə:" Šində dĭn?" </ta>
            <ta e="T59" id="Seg_1109" s="T51">"Măn tăn vnučkal, kömə üžü, tănan šobial măndərzittə". </ta>
            <ta e="T63" id="Seg_1110" s="T59">"Ködərdə, ajə i šoləl". </ta>
            <ta e="T66" id="Seg_1111" s="T63">Dĭgəttə dĭ šobi. </ta>
            <ta e="T72" id="Seg_1112" s="T66">Dĭ nükem amluʔpi i bostə iʔbəbi. </ta>
            <ta e="T75" id="Seg_1113" s="T72">Nüken üžübə šerbi. </ta>
            <ta e="T80" id="Seg_1114" s="T75">Dĭgəttə šobi koʔbdo, kömə üžü. </ta>
            <ta e="T82" id="Seg_1115" s="T80">Küzürleʔbə ajəndə. </ta>
            <ta e="T83" id="Seg_1116" s="T82">"Kardə!" </ta>
            <ta e="T87" id="Seg_1117" s="T83">Dĭ karluʔpi, dĭ šobi. </ta>
            <ta e="T91" id="Seg_1118" s="T87">"Endə stoldə pirog kajaʔ!" </ta>
            <ta e="T93" id="Seg_1119" s="T91">Dĭ embi. </ta>
            <ta e="T99" id="Seg_1120" s="T93">"No, šoʔ măna toːndə (amnolaʔ-) amnaʔ!" </ta>
            <ta e="T101" id="Seg_1121" s="T99">Dĭ amnolaʔbə. </ta>
            <ta e="T106" id="Seg_1122" s="T101">"Ĭmbi tăn kuzaŋdə dĭrgit urgoʔi?" </ta>
            <ta e="T109" id="Seg_1123" s="T106">"Štobɨ tănan nʼilgösʼtə". </ta>
            <ta e="T113" id="Seg_1124" s="T109">"A ĭmbi sʼimazaŋdə urgoʔi?" </ta>
            <ta e="T116" id="Seg_1125" s="T113">"Štobɨ tănan măndərzittə". </ta>
            <ta e="T120" id="Seg_1126" s="T116">"A ĭmbi udaʔi numoʔi?" </ta>
            <ta e="T123" id="Seg_1127" s="T120">"Štobɨ tănan (kamnərosʼtə)". </ta>
            <ta e="T128" id="Seg_1128" s="T123">"A ĭmbi tăn aŋdə urgo?" </ta>
            <ta e="T131" id="Seg_1129" s="T128">"Štobɨ tănan amzittə!" </ta>
            <ta e="T135" id="Seg_1130" s="T131">Dĭgəttə kabarlaʔ ibi, amnuʔpi. </ta>
            <ta e="T144" id="Seg_1131" s="T135">Kajak amnuʔpi i pirog, i bostə iʔbolaʔpi i kunollaʔpi. </ta>
            <ta e="T148" id="Seg_1132" s="T144">Dön il šobiʔi multuksiʔ. </ta>
            <ta e="T152" id="Seg_1133" s="T148">Dĭ bar kunollaʔbə tăŋ. </ta>
            <ta e="T154" id="Seg_1134" s="T152">Dĭgəttə šobiʔi. </ta>
            <ta e="T159" id="Seg_1135" s="T154">I dĭm nanabə dagajziʔ müʔluʔpiʔi. </ta>
            <ta e="T165" id="Seg_1136" s="T159">Dĭzeŋ, nüke i koʔbdo supsoluʔpiʔi, dʼiliʔi. </ta>
            <ta e="T170" id="Seg_1137" s="T165">A volktə bar dʼagarlaʔ kuʔpiʔi. </ta>
            <ta e="T171" id="Seg_1138" s="T170">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1139" s="T0">amno-bi-ʔi</ta>
            <ta e="T2" id="Seg_1140" s="T1">ija-t</ta>
            <ta e="T3" id="Seg_1141" s="T2">i</ta>
            <ta e="T4" id="Seg_1142" s="T3">koʔbdo-t</ta>
            <ta e="T5" id="Seg_1143" s="T4">kömə</ta>
            <ta e="T6" id="Seg_1144" s="T5">üžü</ta>
            <ta e="T7" id="Seg_1145" s="T6">dĭgəttə</ta>
            <ta e="T9" id="Seg_1146" s="T8">urgaja</ta>
            <ta e="T10" id="Seg_1147" s="T9">ĭzem-nuʔ-pi</ta>
            <ta e="T11" id="Seg_1148" s="T10">ia-t</ta>
            <ta e="T12" id="Seg_1149" s="T11">măn-də</ta>
            <ta e="T13" id="Seg_1150" s="T12">kan-a-ʔ</ta>
            <ta e="T14" id="Seg_1151" s="T13">urgaja</ta>
            <ta e="T15" id="Seg_1152" s="T14">ku-t</ta>
            <ta e="T16" id="Seg_1153" s="T15">dĭ</ta>
            <ta e="T17" id="Seg_1154" s="T16">ĭzem-neʔbə</ta>
            <ta e="T18" id="Seg_1155" s="T17">pirog-əʔi</ta>
            <ta e="T19" id="Seg_1156" s="T18">pür-bi</ta>
            <ta e="T20" id="Seg_1157" s="T19">kajaʔ</ta>
            <ta e="T21" id="Seg_1158" s="T20">em-bi</ta>
            <ta e="T22" id="Seg_1159" s="T21">da</ta>
            <ta e="T23" id="Seg_1160" s="T22">kam-bi</ta>
            <ta e="T24" id="Seg_1161" s="T23">keʔbde</ta>
            <ta e="T25" id="Seg_1162" s="T24">oʔbdə-bi</ta>
            <ta e="T26" id="Seg_1163" s="T25">svʼetog-əʔi</ta>
            <ta e="T28" id="Seg_1164" s="T27">nĭŋgə-bi</ta>
            <ta e="T29" id="Seg_1165" s="T28">šonə-ga</ta>
            <ta e="T30" id="Seg_1166" s="T29">dĭʔ-nə</ta>
            <ta e="T31" id="Seg_1167" s="T30">volk</ta>
            <ta e="T32" id="Seg_1168" s="T31">gibər</ta>
            <ta e="T33" id="Seg_1169" s="T32">kandə-ga-l</ta>
            <ta e="T34" id="Seg_1170" s="T33">urgaja-nə</ta>
            <ta e="T35" id="Seg_1171" s="T34">dĭ</ta>
            <ta e="T36" id="Seg_1172" s="T35">ĭzem-neʔbə</ta>
            <ta e="T37" id="Seg_1173" s="T36">a</ta>
            <ta e="T38" id="Seg_1174" s="T37">dĭ</ta>
            <ta e="T39" id="Seg_1175" s="T38">gijen</ta>
            <ta e="T40" id="Seg_1176" s="T39">amno-laʔbə</ta>
            <ta e="T41" id="Seg_1177" s="T40">dĭn</ta>
            <ta e="T42" id="Seg_1178" s="T41">ej</ta>
            <ta e="T43" id="Seg_1179" s="T42">kuŋgəʔ</ta>
            <ta e="T44" id="Seg_1180" s="T43">dĭgəttə</ta>
            <ta e="T45" id="Seg_1181" s="T44">volk</ta>
            <ta e="T46" id="Seg_1182" s="T45">nuʔmə-luʔ-pi</ta>
            <ta e="T47" id="Seg_1183" s="T46">küzür-bi</ta>
            <ta e="T48" id="Seg_1184" s="T47">nüke</ta>
            <ta e="T49" id="Seg_1185" s="T48">măn-də</ta>
            <ta e="T50" id="Seg_1186" s="T49">šində</ta>
            <ta e="T51" id="Seg_1187" s="T50">dĭn</ta>
            <ta e="T52" id="Seg_1188" s="T51">măn</ta>
            <ta e="T53" id="Seg_1189" s="T52">tăn</ta>
            <ta e="T54" id="Seg_1190" s="T53">vnučka-l</ta>
            <ta e="T55" id="Seg_1191" s="T54">kömə</ta>
            <ta e="T56" id="Seg_1192" s="T55">üžü</ta>
            <ta e="T57" id="Seg_1193" s="T56">tănan</ta>
            <ta e="T58" id="Seg_1194" s="T57">šo-bia-l</ta>
            <ta e="T59" id="Seg_1195" s="T58">măndə-r-zittə</ta>
            <ta e="T60" id="Seg_1196" s="T59">ködər-də</ta>
            <ta e="T61" id="Seg_1197" s="T60">ajə</ta>
            <ta e="T62" id="Seg_1198" s="T61">i</ta>
            <ta e="T63" id="Seg_1199" s="T62">šo-lə-l</ta>
            <ta e="T64" id="Seg_1200" s="T63">dĭgəttə</ta>
            <ta e="T65" id="Seg_1201" s="T64">dĭ</ta>
            <ta e="T66" id="Seg_1202" s="T65">šo-bi</ta>
            <ta e="T67" id="Seg_1203" s="T66">dĭ</ta>
            <ta e="T68" id="Seg_1204" s="T67">nüke-m</ta>
            <ta e="T69" id="Seg_1205" s="T68">am-luʔ-pi</ta>
            <ta e="T70" id="Seg_1206" s="T69">i</ta>
            <ta e="T71" id="Seg_1207" s="T70">bos-tə</ta>
            <ta e="T72" id="Seg_1208" s="T71">iʔbə-bi</ta>
            <ta e="T73" id="Seg_1209" s="T72">nüke-n</ta>
            <ta e="T74" id="Seg_1210" s="T73">üžü-bə</ta>
            <ta e="T75" id="Seg_1211" s="T74">šer-bi</ta>
            <ta e="T76" id="Seg_1212" s="T75">dĭgəttə</ta>
            <ta e="T77" id="Seg_1213" s="T76">šo-bi</ta>
            <ta e="T78" id="Seg_1214" s="T77">koʔbdo</ta>
            <ta e="T79" id="Seg_1215" s="T78">kömə</ta>
            <ta e="T80" id="Seg_1216" s="T79">üžü</ta>
            <ta e="T81" id="Seg_1217" s="T80">küzür-leʔbə</ta>
            <ta e="T82" id="Seg_1218" s="T81">ajə-ndə</ta>
            <ta e="T83" id="Seg_1219" s="T82">kar-də</ta>
            <ta e="T84" id="Seg_1220" s="T83">dĭ</ta>
            <ta e="T85" id="Seg_1221" s="T84">kar-luʔ-pi</ta>
            <ta e="T86" id="Seg_1222" s="T85">dĭ</ta>
            <ta e="T87" id="Seg_1223" s="T86">šo-bi</ta>
            <ta e="T88" id="Seg_1224" s="T87">en-də</ta>
            <ta e="T89" id="Seg_1225" s="T88">stol-də</ta>
            <ta e="T90" id="Seg_1226" s="T89">pirog</ta>
            <ta e="T91" id="Seg_1227" s="T90">kajaʔ</ta>
            <ta e="T92" id="Seg_1228" s="T91">dĭ</ta>
            <ta e="T93" id="Seg_1229" s="T92">em-bi</ta>
            <ta e="T94" id="Seg_1230" s="T93">no</ta>
            <ta e="T95" id="Seg_1231" s="T94">šo-ʔ</ta>
            <ta e="T96" id="Seg_1232" s="T95">măna</ta>
            <ta e="T97" id="Seg_1233" s="T96">toː-ndə</ta>
            <ta e="T99" id="Seg_1234" s="T98">amna-ʔ</ta>
            <ta e="T100" id="Seg_1235" s="T99">dĭ</ta>
            <ta e="T101" id="Seg_1236" s="T100">amno-laʔbə</ta>
            <ta e="T102" id="Seg_1237" s="T101">ĭmbi</ta>
            <ta e="T103" id="Seg_1238" s="T102">tăn</ta>
            <ta e="T104" id="Seg_1239" s="T103">ku-zaŋ-də</ta>
            <ta e="T105" id="Seg_1240" s="T104">dĭrgit</ta>
            <ta e="T106" id="Seg_1241" s="T105">urgo-ʔi</ta>
            <ta e="T107" id="Seg_1242" s="T106">štobɨ</ta>
            <ta e="T108" id="Seg_1243" s="T107">tănan</ta>
            <ta e="T109" id="Seg_1244" s="T108">nʼilgö-sʼtə</ta>
            <ta e="T110" id="Seg_1245" s="T109">a</ta>
            <ta e="T111" id="Seg_1246" s="T110">ĭmbi</ta>
            <ta e="T112" id="Seg_1247" s="T111">sʼima-zaŋ-də</ta>
            <ta e="T113" id="Seg_1248" s="T112">urgo-ʔi</ta>
            <ta e="T114" id="Seg_1249" s="T113">štobɨ</ta>
            <ta e="T115" id="Seg_1250" s="T114">tănan</ta>
            <ta e="T116" id="Seg_1251" s="T115">măndə-r-zittə</ta>
            <ta e="T117" id="Seg_1252" s="T116">a</ta>
            <ta e="T118" id="Seg_1253" s="T117">ĭmbi</ta>
            <ta e="T119" id="Seg_1254" s="T118">uda-ʔi</ta>
            <ta e="T120" id="Seg_1255" s="T119">numo-ʔi</ta>
            <ta e="T121" id="Seg_1256" s="T120">štobɨ</ta>
            <ta e="T122" id="Seg_1257" s="T121">tănan</ta>
            <ta e="T123" id="Seg_1258" s="T122">kamnəro-sʼtə</ta>
            <ta e="T124" id="Seg_1259" s="T123">a</ta>
            <ta e="T125" id="Seg_1260" s="T124">ĭmbi</ta>
            <ta e="T126" id="Seg_1261" s="T125">tăn</ta>
            <ta e="T127" id="Seg_1262" s="T126">aŋ-də</ta>
            <ta e="T128" id="Seg_1263" s="T127">urgo</ta>
            <ta e="T129" id="Seg_1264" s="T128">štobɨ</ta>
            <ta e="T130" id="Seg_1265" s="T129">tănan</ta>
            <ta e="T131" id="Seg_1266" s="T130">am-zittə</ta>
            <ta e="T132" id="Seg_1267" s="T131">dĭgəttə</ta>
            <ta e="T133" id="Seg_1268" s="T132">kabar-laʔ</ta>
            <ta e="T134" id="Seg_1269" s="T133">i-bi</ta>
            <ta e="T135" id="Seg_1270" s="T134">am-nuʔ-pi</ta>
            <ta e="T136" id="Seg_1271" s="T135">kajak</ta>
            <ta e="T137" id="Seg_1272" s="T136">am-nuʔ-pi</ta>
            <ta e="T138" id="Seg_1273" s="T137">i</ta>
            <ta e="T139" id="Seg_1274" s="T138">pirog</ta>
            <ta e="T140" id="Seg_1275" s="T139">i</ta>
            <ta e="T141" id="Seg_1276" s="T140">bos-tə</ta>
            <ta e="T142" id="Seg_1277" s="T141">iʔbo-laʔ-pi</ta>
            <ta e="T143" id="Seg_1278" s="T142">i</ta>
            <ta e="T144" id="Seg_1279" s="T143">kunol-laʔ-pi</ta>
            <ta e="T145" id="Seg_1280" s="T144">dön</ta>
            <ta e="T146" id="Seg_1281" s="T145">il</ta>
            <ta e="T147" id="Seg_1282" s="T146">šo-bi-ʔi</ta>
            <ta e="T148" id="Seg_1283" s="T147">multuk-siʔ</ta>
            <ta e="T149" id="Seg_1284" s="T148">dĭ</ta>
            <ta e="T150" id="Seg_1285" s="T149">bar</ta>
            <ta e="T151" id="Seg_1286" s="T150">kunol-laʔbə</ta>
            <ta e="T152" id="Seg_1287" s="T151">tăŋ</ta>
            <ta e="T153" id="Seg_1288" s="T152">dĭgəttə</ta>
            <ta e="T154" id="Seg_1289" s="T153">šo-bi-ʔi</ta>
            <ta e="T155" id="Seg_1290" s="T154">i</ta>
            <ta e="T156" id="Seg_1291" s="T155">dĭ-m</ta>
            <ta e="T157" id="Seg_1292" s="T156">nana-bə</ta>
            <ta e="T158" id="Seg_1293" s="T157">dagaj-ziʔ</ta>
            <ta e="T159" id="Seg_1294" s="T158">müʔ-luʔ-pi-ʔi</ta>
            <ta e="T160" id="Seg_1295" s="T159">dĭ-zeŋ</ta>
            <ta e="T161" id="Seg_1296" s="T160">nüke</ta>
            <ta e="T162" id="Seg_1297" s="T161">i</ta>
            <ta e="T163" id="Seg_1298" s="T162">koʔbdo</ta>
            <ta e="T164" id="Seg_1299" s="T163">supso-luʔ-pi-ʔi</ta>
            <ta e="T165" id="Seg_1300" s="T164">dʼili-ʔi</ta>
            <ta e="T166" id="Seg_1301" s="T165">a</ta>
            <ta e="T167" id="Seg_1302" s="T166">volk-tə</ta>
            <ta e="T168" id="Seg_1303" s="T167">bar</ta>
            <ta e="T169" id="Seg_1304" s="T168">dʼagar-laʔ</ta>
            <ta e="T170" id="Seg_1305" s="T169">kuʔ-pi-ʔi</ta>
            <ta e="T171" id="Seg_1306" s="T170">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1307" s="T0">amno-bi-jəʔ</ta>
            <ta e="T2" id="Seg_1308" s="T1">ija-t</ta>
            <ta e="T3" id="Seg_1309" s="T2">i</ta>
            <ta e="T4" id="Seg_1310" s="T3">koʔbdo-t</ta>
            <ta e="T5" id="Seg_1311" s="T4">kömə</ta>
            <ta e="T6" id="Seg_1312" s="T5">üžü</ta>
            <ta e="T7" id="Seg_1313" s="T6">dĭgəttə</ta>
            <ta e="T9" id="Seg_1314" s="T8">urgaja</ta>
            <ta e="T10" id="Seg_1315" s="T9">ĭzem-luʔbdə-bi</ta>
            <ta e="T11" id="Seg_1316" s="T10">ija-t</ta>
            <ta e="T12" id="Seg_1317" s="T11">măn-ntə</ta>
            <ta e="T13" id="Seg_1318" s="T12">kan-ə-ʔ</ta>
            <ta e="T14" id="Seg_1319" s="T13">urgaja</ta>
            <ta e="T15" id="Seg_1320" s="T14">ku-t</ta>
            <ta e="T16" id="Seg_1321" s="T15">dĭ</ta>
            <ta e="T17" id="Seg_1322" s="T16">ĭzem-laʔbə</ta>
            <ta e="T18" id="Seg_1323" s="T17">pirog-jəʔ</ta>
            <ta e="T19" id="Seg_1324" s="T18">pür-bi</ta>
            <ta e="T20" id="Seg_1325" s="T19">kajaʔ</ta>
            <ta e="T21" id="Seg_1326" s="T20">hen-bi</ta>
            <ta e="T22" id="Seg_1327" s="T21">da</ta>
            <ta e="T23" id="Seg_1328" s="T22">kan-bi</ta>
            <ta e="T24" id="Seg_1329" s="T23">keʔbde</ta>
            <ta e="T25" id="Seg_1330" s="T24">oʔbdə-bi</ta>
            <ta e="T26" id="Seg_1331" s="T25">svetok-jəʔ</ta>
            <ta e="T28" id="Seg_1332" s="T27">nĭŋgə-bi</ta>
            <ta e="T29" id="Seg_1333" s="T28">šonə-gA</ta>
            <ta e="T30" id="Seg_1334" s="T29">dĭ-Tə</ta>
            <ta e="T31" id="Seg_1335" s="T30">volk</ta>
            <ta e="T32" id="Seg_1336" s="T31">gibər</ta>
            <ta e="T33" id="Seg_1337" s="T32">kandə-gA-l</ta>
            <ta e="T34" id="Seg_1338" s="T33">urgaja-Tə</ta>
            <ta e="T35" id="Seg_1339" s="T34">dĭ</ta>
            <ta e="T36" id="Seg_1340" s="T35">ĭzem-laʔbə</ta>
            <ta e="T37" id="Seg_1341" s="T36">a</ta>
            <ta e="T38" id="Seg_1342" s="T37">dĭ</ta>
            <ta e="T39" id="Seg_1343" s="T38">gijen</ta>
            <ta e="T40" id="Seg_1344" s="T39">amno-laʔbə</ta>
            <ta e="T41" id="Seg_1345" s="T40">dĭn</ta>
            <ta e="T42" id="Seg_1346" s="T41">ej</ta>
            <ta e="T43" id="Seg_1347" s="T42">kuŋgə</ta>
            <ta e="T44" id="Seg_1348" s="T43">dĭgəttə</ta>
            <ta e="T45" id="Seg_1349" s="T44">volk</ta>
            <ta e="T46" id="Seg_1350" s="T45">nuʔmə-luʔbdə-bi</ta>
            <ta e="T47" id="Seg_1351" s="T46">kuzur-bi</ta>
            <ta e="T48" id="Seg_1352" s="T47">nüke</ta>
            <ta e="T49" id="Seg_1353" s="T48">măn-ntə</ta>
            <ta e="T50" id="Seg_1354" s="T49">šində</ta>
            <ta e="T51" id="Seg_1355" s="T50">dĭn</ta>
            <ta e="T52" id="Seg_1356" s="T51">măn</ta>
            <ta e="T53" id="Seg_1357" s="T52">tăn</ta>
            <ta e="T54" id="Seg_1358" s="T53">vnučka-l</ta>
            <ta e="T55" id="Seg_1359" s="T54">kömə</ta>
            <ta e="T56" id="Seg_1360" s="T55">üžü</ta>
            <ta e="T57" id="Seg_1361" s="T56">tănan</ta>
            <ta e="T58" id="Seg_1362" s="T57">šo-bi-l</ta>
            <ta e="T59" id="Seg_1363" s="T58">măndo-r-zittə</ta>
            <ta e="T60" id="Seg_1364" s="T59">ködər-t</ta>
            <ta e="T61" id="Seg_1365" s="T60">ajə</ta>
            <ta e="T62" id="Seg_1366" s="T61">i</ta>
            <ta e="T63" id="Seg_1367" s="T62">šo-lV-l</ta>
            <ta e="T64" id="Seg_1368" s="T63">dĭgəttə</ta>
            <ta e="T65" id="Seg_1369" s="T64">dĭ</ta>
            <ta e="T66" id="Seg_1370" s="T65">šo-bi</ta>
            <ta e="T67" id="Seg_1371" s="T66">dĭ</ta>
            <ta e="T68" id="Seg_1372" s="T67">nüke-m</ta>
            <ta e="T69" id="Seg_1373" s="T68">am-luʔbdə-bi</ta>
            <ta e="T70" id="Seg_1374" s="T69">i</ta>
            <ta e="T71" id="Seg_1375" s="T70">bos-də</ta>
            <ta e="T72" id="Seg_1376" s="T71">iʔbə-bi</ta>
            <ta e="T73" id="Seg_1377" s="T72">nüke-n</ta>
            <ta e="T74" id="Seg_1378" s="T73">üžü-bə</ta>
            <ta e="T75" id="Seg_1379" s="T74">šer-bi</ta>
            <ta e="T76" id="Seg_1380" s="T75">dĭgəttə</ta>
            <ta e="T77" id="Seg_1381" s="T76">šo-bi</ta>
            <ta e="T78" id="Seg_1382" s="T77">koʔbdo</ta>
            <ta e="T79" id="Seg_1383" s="T78">kömə</ta>
            <ta e="T80" id="Seg_1384" s="T79">üžü</ta>
            <ta e="T81" id="Seg_1385" s="T80">kuzur-laʔbə</ta>
            <ta e="T82" id="Seg_1386" s="T81">ajə-ndə</ta>
            <ta e="T83" id="Seg_1387" s="T82">kar-t</ta>
            <ta e="T84" id="Seg_1388" s="T83">dĭ</ta>
            <ta e="T85" id="Seg_1389" s="T84">kar-luʔbdə-bi</ta>
            <ta e="T86" id="Seg_1390" s="T85">dĭ</ta>
            <ta e="T87" id="Seg_1391" s="T86">šo-bi</ta>
            <ta e="T88" id="Seg_1392" s="T87">hen-t</ta>
            <ta e="T89" id="Seg_1393" s="T88">stol-Tə</ta>
            <ta e="T90" id="Seg_1394" s="T89">pirog</ta>
            <ta e="T91" id="Seg_1395" s="T90">kajaʔ</ta>
            <ta e="T92" id="Seg_1396" s="T91">dĭ</ta>
            <ta e="T93" id="Seg_1397" s="T92">hen-bi</ta>
            <ta e="T94" id="Seg_1398" s="T93">no</ta>
            <ta e="T95" id="Seg_1399" s="T94">šo-ʔ</ta>
            <ta e="T96" id="Seg_1400" s="T95">măna</ta>
            <ta e="T97" id="Seg_1401" s="T96">toʔ-gəndə</ta>
            <ta e="T99" id="Seg_1402" s="T98">amnə-ʔ</ta>
            <ta e="T100" id="Seg_1403" s="T99">dĭ</ta>
            <ta e="T101" id="Seg_1404" s="T100">amno-laʔbə</ta>
            <ta e="T102" id="Seg_1405" s="T101">ĭmbi</ta>
            <ta e="T103" id="Seg_1406" s="T102">tăn</ta>
            <ta e="T104" id="Seg_1407" s="T103">ku-zAŋ-l</ta>
            <ta e="T105" id="Seg_1408" s="T104">dĭrgit</ta>
            <ta e="T106" id="Seg_1409" s="T105">urgo-jəʔ</ta>
            <ta e="T107" id="Seg_1410" s="T106">štobɨ</ta>
            <ta e="T108" id="Seg_1411" s="T107">tănan</ta>
            <ta e="T109" id="Seg_1412" s="T108">nʼilgö-zittə</ta>
            <ta e="T110" id="Seg_1413" s="T109">a</ta>
            <ta e="T111" id="Seg_1414" s="T110">ĭmbi</ta>
            <ta e="T112" id="Seg_1415" s="T111">sima-zAŋ-də</ta>
            <ta e="T113" id="Seg_1416" s="T112">urgo-jəʔ</ta>
            <ta e="T114" id="Seg_1417" s="T113">štobɨ</ta>
            <ta e="T115" id="Seg_1418" s="T114">tănan</ta>
            <ta e="T116" id="Seg_1419" s="T115">măndo-r-zittə</ta>
            <ta e="T117" id="Seg_1420" s="T116">a</ta>
            <ta e="T118" id="Seg_1421" s="T117">ĭmbi</ta>
            <ta e="T119" id="Seg_1422" s="T118">uda-jəʔ</ta>
            <ta e="T120" id="Seg_1423" s="T119">numo-jəʔ</ta>
            <ta e="T121" id="Seg_1424" s="T120">štobɨ</ta>
            <ta e="T122" id="Seg_1425" s="T121">tănan</ta>
            <ta e="T123" id="Seg_1426" s="T122">kamro-zittə</ta>
            <ta e="T124" id="Seg_1427" s="T123">a</ta>
            <ta e="T125" id="Seg_1428" s="T124">ĭmbi</ta>
            <ta e="T126" id="Seg_1429" s="T125">tăn</ta>
            <ta e="T127" id="Seg_1430" s="T126">aŋ-də</ta>
            <ta e="T128" id="Seg_1431" s="T127">urgo</ta>
            <ta e="T129" id="Seg_1432" s="T128">štobɨ</ta>
            <ta e="T130" id="Seg_1433" s="T129">tănan</ta>
            <ta e="T131" id="Seg_1434" s="T130">am-zittə</ta>
            <ta e="T132" id="Seg_1435" s="T131">dĭgəttə</ta>
            <ta e="T133" id="Seg_1436" s="T132">kabar-lAʔ</ta>
            <ta e="T134" id="Seg_1437" s="T133">i-bi</ta>
            <ta e="T135" id="Seg_1438" s="T134">am-luʔbdə-bi</ta>
            <ta e="T136" id="Seg_1439" s="T135">kajaʔ</ta>
            <ta e="T137" id="Seg_1440" s="T136">am-luʔbdə-bi</ta>
            <ta e="T138" id="Seg_1441" s="T137">i</ta>
            <ta e="T139" id="Seg_1442" s="T138">pirog</ta>
            <ta e="T140" id="Seg_1443" s="T139">i</ta>
            <ta e="T141" id="Seg_1444" s="T140">bos-də</ta>
            <ta e="T142" id="Seg_1445" s="T141">iʔbö-laʔbə-bi</ta>
            <ta e="T143" id="Seg_1446" s="T142">i</ta>
            <ta e="T144" id="Seg_1447" s="T143">kunol-laʔbə-bi</ta>
            <ta e="T145" id="Seg_1448" s="T144">dön</ta>
            <ta e="T146" id="Seg_1449" s="T145">il</ta>
            <ta e="T147" id="Seg_1450" s="T146">šo-bi-jəʔ</ta>
            <ta e="T148" id="Seg_1451" s="T147">multuk-ziʔ</ta>
            <ta e="T149" id="Seg_1452" s="T148">dĭ</ta>
            <ta e="T150" id="Seg_1453" s="T149">bar</ta>
            <ta e="T151" id="Seg_1454" s="T150">kunol-laʔbə</ta>
            <ta e="T152" id="Seg_1455" s="T151">tăŋ</ta>
            <ta e="T153" id="Seg_1456" s="T152">dĭgəttə</ta>
            <ta e="T154" id="Seg_1457" s="T153">šo-bi-jəʔ</ta>
            <ta e="T155" id="Seg_1458" s="T154">i</ta>
            <ta e="T156" id="Seg_1459" s="T155">dĭ-m</ta>
            <ta e="T157" id="Seg_1460" s="T156">nanə-bə</ta>
            <ta e="T158" id="Seg_1461" s="T157">tagaj-ziʔ</ta>
            <ta e="T159" id="Seg_1462" s="T158">müʔbdə-luʔbdə-bi-jəʔ</ta>
            <ta e="T160" id="Seg_1463" s="T159">dĭ-zAŋ</ta>
            <ta e="T161" id="Seg_1464" s="T160">nüke</ta>
            <ta e="T162" id="Seg_1465" s="T161">i</ta>
            <ta e="T163" id="Seg_1466" s="T162">koʔbdo</ta>
            <ta e="T164" id="Seg_1467" s="T163">supso-luʔbdə-bi-jəʔ</ta>
            <ta e="T165" id="Seg_1468" s="T164">tʼili-jəʔ</ta>
            <ta e="T166" id="Seg_1469" s="T165">a</ta>
            <ta e="T167" id="Seg_1470" s="T166">volk-də</ta>
            <ta e="T168" id="Seg_1471" s="T167">bar</ta>
            <ta e="T169" id="Seg_1472" s="T168">dʼagar-lAʔ</ta>
            <ta e="T170" id="Seg_1473" s="T169">kut-bi-jəʔ</ta>
            <ta e="T171" id="Seg_1474" s="T170">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1475" s="T0">live-PST-3PL</ta>
            <ta e="T2" id="Seg_1476" s="T1">mother-NOM/GEN.3SG</ta>
            <ta e="T3" id="Seg_1477" s="T2">and</ta>
            <ta e="T4" id="Seg_1478" s="T3">daughter-NOM/GEN.3SG</ta>
            <ta e="T5" id="Seg_1479" s="T4">red.[NOM.SG]</ta>
            <ta e="T6" id="Seg_1480" s="T5">cap.[NOM.SG]</ta>
            <ta e="T7" id="Seg_1481" s="T6">then</ta>
            <ta e="T9" id="Seg_1482" s="T8">grandmother.[NOM.SG]</ta>
            <ta e="T10" id="Seg_1483" s="T9">hurt-MOM-PST.[3SG]</ta>
            <ta e="T11" id="Seg_1484" s="T10">mother-NOM/GEN.3SG</ta>
            <ta e="T12" id="Seg_1485" s="T11">say-IPFVZ.[3SG]</ta>
            <ta e="T13" id="Seg_1486" s="T12">go-EP-IMP.2SG</ta>
            <ta e="T14" id="Seg_1487" s="T13">grandmother.[NOM.SG]</ta>
            <ta e="T15" id="Seg_1488" s="T14">see-IMP.2SG.O</ta>
            <ta e="T16" id="Seg_1489" s="T15">this.[NOM.SG]</ta>
            <ta e="T17" id="Seg_1490" s="T16">hurt-DUR.[3SG]</ta>
            <ta e="T18" id="Seg_1491" s="T17">pie-PL</ta>
            <ta e="T19" id="Seg_1492" s="T18">bake-PST.[3SG]</ta>
            <ta e="T20" id="Seg_1493" s="T19">butter.[NOM.SG]</ta>
            <ta e="T21" id="Seg_1494" s="T20">put-PST.[3SG]</ta>
            <ta e="T22" id="Seg_1495" s="T21">and</ta>
            <ta e="T23" id="Seg_1496" s="T22">go-PST.[3SG]</ta>
            <ta e="T24" id="Seg_1497" s="T23">berry.[NOM.SG]</ta>
            <ta e="T25" id="Seg_1498" s="T24">collect-PST.[3SG]</ta>
            <ta e="T26" id="Seg_1499" s="T25">flower-PL</ta>
            <ta e="T28" id="Seg_1500" s="T27">tear-PST.[3SG]</ta>
            <ta e="T29" id="Seg_1501" s="T28">come-PRS.[3SG]</ta>
            <ta e="T30" id="Seg_1502" s="T29">this-LAT</ta>
            <ta e="T31" id="Seg_1503" s="T30">wolf.[NOM.SG]</ta>
            <ta e="T32" id="Seg_1504" s="T31">where.to</ta>
            <ta e="T33" id="Seg_1505" s="T32">walk-PRS-2SG</ta>
            <ta e="T34" id="Seg_1506" s="T33">grandmother-LAT</ta>
            <ta e="T35" id="Seg_1507" s="T34">this.[NOM.SG]</ta>
            <ta e="T36" id="Seg_1508" s="T35">hurt-DUR.[3SG]</ta>
            <ta e="T37" id="Seg_1509" s="T36">and</ta>
            <ta e="T38" id="Seg_1510" s="T37">this.[NOM.SG]</ta>
            <ta e="T39" id="Seg_1511" s="T38">where</ta>
            <ta e="T40" id="Seg_1512" s="T39">live-DUR.[3SG]</ta>
            <ta e="T41" id="Seg_1513" s="T40">there</ta>
            <ta e="T42" id="Seg_1514" s="T41">NEG</ta>
            <ta e="T43" id="Seg_1515" s="T42">far</ta>
            <ta e="T44" id="Seg_1516" s="T43">then</ta>
            <ta e="T45" id="Seg_1517" s="T44">wolf.[NOM.SG]</ta>
            <ta e="T46" id="Seg_1518" s="T45">run-MOM-PST.[3SG]</ta>
            <ta e="T47" id="Seg_1519" s="T46">rumble-PST.[3SG]</ta>
            <ta e="T48" id="Seg_1520" s="T47">woman.[NOM.SG]</ta>
            <ta e="T49" id="Seg_1521" s="T48">say-IPFVZ.[3SG]</ta>
            <ta e="T50" id="Seg_1522" s="T49">who.[NOM.SG]</ta>
            <ta e="T51" id="Seg_1523" s="T50">there</ta>
            <ta e="T52" id="Seg_1524" s="T51">I.NOM</ta>
            <ta e="T53" id="Seg_1525" s="T52">you.NOM</ta>
            <ta e="T54" id="Seg_1526" s="T53">granddaughter-NOM/GEN/ACC.2SG</ta>
            <ta e="T55" id="Seg_1527" s="T54">red.[NOM.SG]</ta>
            <ta e="T56" id="Seg_1528" s="T55">cap.[NOM.SG]</ta>
            <ta e="T57" id="Seg_1529" s="T56">you.ACC</ta>
            <ta e="T58" id="Seg_1530" s="T57">come-PST-2SG</ta>
            <ta e="T59" id="Seg_1531" s="T58">look-FRQ-INF.LAT</ta>
            <ta e="T60" id="Seg_1532" s="T59">untie-IMP.2SG.O</ta>
            <ta e="T61" id="Seg_1533" s="T60">door.[NOM.SG]</ta>
            <ta e="T62" id="Seg_1534" s="T61">and</ta>
            <ta e="T63" id="Seg_1535" s="T62">come-FUT-2SG</ta>
            <ta e="T64" id="Seg_1536" s="T63">then</ta>
            <ta e="T65" id="Seg_1537" s="T64">this.[NOM.SG]</ta>
            <ta e="T66" id="Seg_1538" s="T65">come-PST.[3SG]</ta>
            <ta e="T67" id="Seg_1539" s="T66">this.[NOM.SG]</ta>
            <ta e="T68" id="Seg_1540" s="T67">woman-NOM/GEN/ACC.1SG</ta>
            <ta e="T69" id="Seg_1541" s="T68">eat-MOM-PST.[3SG]</ta>
            <ta e="T70" id="Seg_1542" s="T69">and</ta>
            <ta e="T71" id="Seg_1543" s="T70">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T72" id="Seg_1544" s="T71">lie.down-PST.[3SG]</ta>
            <ta e="T73" id="Seg_1545" s="T72">woman-GEN</ta>
            <ta e="T74" id="Seg_1546" s="T73">cap-ACC.3SG</ta>
            <ta e="T75" id="Seg_1547" s="T74">dress-PST.[3SG]</ta>
            <ta e="T76" id="Seg_1548" s="T75">then</ta>
            <ta e="T77" id="Seg_1549" s="T76">come-PST.[3SG]</ta>
            <ta e="T78" id="Seg_1550" s="T77">girl.[NOM.SG]</ta>
            <ta e="T79" id="Seg_1551" s="T78">red.[NOM.SG]</ta>
            <ta e="T80" id="Seg_1552" s="T79">cap.[NOM.SG]</ta>
            <ta e="T81" id="Seg_1553" s="T80">rumble-DUR.[3SG]</ta>
            <ta e="T82" id="Seg_1554" s="T81">door-GEN.3SG</ta>
            <ta e="T83" id="Seg_1555" s="T82">open-IMP.2SG.O</ta>
            <ta e="T84" id="Seg_1556" s="T83">this.[NOM.SG]</ta>
            <ta e="T85" id="Seg_1557" s="T84">open-MOM-PST.[3SG]</ta>
            <ta e="T86" id="Seg_1558" s="T85">this.[NOM.SG]</ta>
            <ta e="T87" id="Seg_1559" s="T86">come-PST.[3SG]</ta>
            <ta e="T88" id="Seg_1560" s="T87">put-IMP.2SG.O</ta>
            <ta e="T89" id="Seg_1561" s="T88">table-LAT</ta>
            <ta e="T90" id="Seg_1562" s="T89">pie.[NOM.SG]</ta>
            <ta e="T91" id="Seg_1563" s="T90">butter.[NOM.SG]</ta>
            <ta e="T92" id="Seg_1564" s="T91">this.[NOM.SG]</ta>
            <ta e="T93" id="Seg_1565" s="T92">put-PST.[3SG]</ta>
            <ta e="T94" id="Seg_1566" s="T93">well</ta>
            <ta e="T95" id="Seg_1567" s="T94">come-IMP.2SG</ta>
            <ta e="T96" id="Seg_1568" s="T95">I.LAT</ta>
            <ta e="T97" id="Seg_1569" s="T96">edge-LAT/LOC.3SG</ta>
            <ta e="T99" id="Seg_1570" s="T98">sit-IMP.2SG</ta>
            <ta e="T100" id="Seg_1571" s="T99">this.[NOM.SG]</ta>
            <ta e="T101" id="Seg_1572" s="T100">sit-DUR.[3SG]</ta>
            <ta e="T102" id="Seg_1573" s="T101">what.[NOM.SG]</ta>
            <ta e="T103" id="Seg_1574" s="T102">you.NOM</ta>
            <ta e="T104" id="Seg_1575" s="T103">ear-PL-NOM/GEN/ACC.2SG</ta>
            <ta e="T105" id="Seg_1576" s="T104">such.[NOM.SG]</ta>
            <ta e="T106" id="Seg_1577" s="T105">big-PL</ta>
            <ta e="T107" id="Seg_1578" s="T106">so.that</ta>
            <ta e="T108" id="Seg_1579" s="T107">you.ACC</ta>
            <ta e="T109" id="Seg_1580" s="T108">listen-INF.LAT</ta>
            <ta e="T110" id="Seg_1581" s="T109">and</ta>
            <ta e="T111" id="Seg_1582" s="T110">what.[NOM.SG]</ta>
            <ta e="T112" id="Seg_1583" s="T111">eye-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T113" id="Seg_1584" s="T112">big-PL</ta>
            <ta e="T114" id="Seg_1585" s="T113">so.that</ta>
            <ta e="T115" id="Seg_1586" s="T114">you.ACC</ta>
            <ta e="T116" id="Seg_1587" s="T115">look-FRQ-INF.LAT</ta>
            <ta e="T117" id="Seg_1588" s="T116">and</ta>
            <ta e="T118" id="Seg_1589" s="T117">what.[NOM.SG]</ta>
            <ta e="T119" id="Seg_1590" s="T118">hand-PL</ta>
            <ta e="T120" id="Seg_1591" s="T119">long-PL</ta>
            <ta e="T121" id="Seg_1592" s="T120">so.that</ta>
            <ta e="T122" id="Seg_1593" s="T121">you.ACC</ta>
            <ta e="T123" id="Seg_1594" s="T122">hug-INF.LAT</ta>
            <ta e="T124" id="Seg_1595" s="T123">and</ta>
            <ta e="T125" id="Seg_1596" s="T124">what.[NOM.SG]</ta>
            <ta e="T126" id="Seg_1597" s="T125">you.NOM</ta>
            <ta e="T127" id="Seg_1598" s="T126">mouth-NOM/GEN/ACC.3SG</ta>
            <ta e="T128" id="Seg_1599" s="T127">big.[NOM.SG]</ta>
            <ta e="T129" id="Seg_1600" s="T128">so.that</ta>
            <ta e="T130" id="Seg_1601" s="T129">you.ACC</ta>
            <ta e="T131" id="Seg_1602" s="T130">eat-INF.LAT</ta>
            <ta e="T132" id="Seg_1603" s="T131">then</ta>
            <ta e="T133" id="Seg_1604" s="T132">grab-CVB</ta>
            <ta e="T134" id="Seg_1605" s="T133">take-PST.[3SG]</ta>
            <ta e="T135" id="Seg_1606" s="T134">eat-MOM-PST.[3SG]</ta>
            <ta e="T136" id="Seg_1607" s="T135">butter.[NOM.SG]</ta>
            <ta e="T137" id="Seg_1608" s="T136">eat-MOM-PST.[3SG]</ta>
            <ta e="T138" id="Seg_1609" s="T137">and</ta>
            <ta e="T139" id="Seg_1610" s="T138">pie.[NOM.SG]</ta>
            <ta e="T140" id="Seg_1611" s="T139">and</ta>
            <ta e="T141" id="Seg_1612" s="T140">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T142" id="Seg_1613" s="T141">lie-DUR-PST.[3SG]</ta>
            <ta e="T143" id="Seg_1614" s="T142">and</ta>
            <ta e="T144" id="Seg_1615" s="T143">sleep-DUR-PST.[3SG]</ta>
            <ta e="T145" id="Seg_1616" s="T144">here</ta>
            <ta e="T146" id="Seg_1617" s="T145">people.[NOM.SG]</ta>
            <ta e="T147" id="Seg_1618" s="T146">come-PST-3PL</ta>
            <ta e="T148" id="Seg_1619" s="T147">gun-INS</ta>
            <ta e="T149" id="Seg_1620" s="T148">this.[NOM.SG]</ta>
            <ta e="T150" id="Seg_1621" s="T149">PTCL</ta>
            <ta e="T151" id="Seg_1622" s="T150">sleep-DUR.[3SG]</ta>
            <ta e="T152" id="Seg_1623" s="T151">strongly</ta>
            <ta e="T153" id="Seg_1624" s="T152">then</ta>
            <ta e="T154" id="Seg_1625" s="T153">come-PST-3PL</ta>
            <ta e="T155" id="Seg_1626" s="T154">and</ta>
            <ta e="T156" id="Seg_1627" s="T155">this-ACC</ta>
            <ta e="T157" id="Seg_1628" s="T156">belly-ACC.3SG</ta>
            <ta e="T158" id="Seg_1629" s="T157">knife-INS</ta>
            <ta e="T159" id="Seg_1630" s="T158">push-MOM-PST-3PL</ta>
            <ta e="T160" id="Seg_1631" s="T159">this-PL</ta>
            <ta e="T161" id="Seg_1632" s="T160">woman.[NOM.SG]</ta>
            <ta e="T162" id="Seg_1633" s="T161">and</ta>
            <ta e="T163" id="Seg_1634" s="T162">girl.[NOM.SG]</ta>
            <ta e="T164" id="Seg_1635" s="T163">depart-MOM-PST-3PL</ta>
            <ta e="T165" id="Seg_1636" s="T164">alive-PL</ta>
            <ta e="T166" id="Seg_1637" s="T165">and</ta>
            <ta e="T167" id="Seg_1638" s="T166">wolf-NOM/GEN/ACC.3SG</ta>
            <ta e="T168" id="Seg_1639" s="T167">PTCL</ta>
            <ta e="T169" id="Seg_1640" s="T168">stab-CVB</ta>
            <ta e="T170" id="Seg_1641" s="T169">kill-PST-3PL</ta>
            <ta e="T171" id="Seg_1642" s="T170">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1643" s="T0">жить-PST-3PL</ta>
            <ta e="T2" id="Seg_1644" s="T1">мать-NOM/GEN.3SG</ta>
            <ta e="T3" id="Seg_1645" s="T2">и</ta>
            <ta e="T4" id="Seg_1646" s="T3">дочь-NOM/GEN.3SG</ta>
            <ta e="T5" id="Seg_1647" s="T4">красный.[NOM.SG]</ta>
            <ta e="T6" id="Seg_1648" s="T5">шапка.[NOM.SG]</ta>
            <ta e="T7" id="Seg_1649" s="T6">тогда</ta>
            <ta e="T9" id="Seg_1650" s="T8">бабушка.[NOM.SG]</ta>
            <ta e="T10" id="Seg_1651" s="T9">болеть-MOM-PST.[3SG]</ta>
            <ta e="T11" id="Seg_1652" s="T10">мать-NOM/GEN.3SG</ta>
            <ta e="T12" id="Seg_1653" s="T11">сказать-IPFVZ.[3SG]</ta>
            <ta e="T13" id="Seg_1654" s="T12">пойти-EP-IMP.2SG</ta>
            <ta e="T14" id="Seg_1655" s="T13">бабушка.[NOM.SG]</ta>
            <ta e="T15" id="Seg_1656" s="T14">видеть-IMP.2SG.O</ta>
            <ta e="T16" id="Seg_1657" s="T15">этот.[NOM.SG]</ta>
            <ta e="T17" id="Seg_1658" s="T16">болеть-DUR.[3SG]</ta>
            <ta e="T18" id="Seg_1659" s="T17">пирог-PL</ta>
            <ta e="T19" id="Seg_1660" s="T18">печь-PST.[3SG]</ta>
            <ta e="T20" id="Seg_1661" s="T19">масло.[NOM.SG]</ta>
            <ta e="T21" id="Seg_1662" s="T20">класть-PST.[3SG]</ta>
            <ta e="T22" id="Seg_1663" s="T21">и</ta>
            <ta e="T23" id="Seg_1664" s="T22">пойти-PST.[3SG]</ta>
            <ta e="T24" id="Seg_1665" s="T23">ягода.[NOM.SG]</ta>
            <ta e="T25" id="Seg_1666" s="T24">собирать-PST.[3SG]</ta>
            <ta e="T26" id="Seg_1667" s="T25">цветок-PL</ta>
            <ta e="T28" id="Seg_1668" s="T27">рвать-PST.[3SG]</ta>
            <ta e="T29" id="Seg_1669" s="T28">прийти-PRS.[3SG]</ta>
            <ta e="T30" id="Seg_1670" s="T29">этот-LAT</ta>
            <ta e="T31" id="Seg_1671" s="T30">волк.[NOM.SG]</ta>
            <ta e="T32" id="Seg_1672" s="T31">куда</ta>
            <ta e="T33" id="Seg_1673" s="T32">идти-PRS-2SG</ta>
            <ta e="T34" id="Seg_1674" s="T33">бабушка-LAT</ta>
            <ta e="T35" id="Seg_1675" s="T34">этот.[NOM.SG]</ta>
            <ta e="T36" id="Seg_1676" s="T35">болеть-DUR.[3SG]</ta>
            <ta e="T37" id="Seg_1677" s="T36">а</ta>
            <ta e="T38" id="Seg_1678" s="T37">этот.[NOM.SG]</ta>
            <ta e="T39" id="Seg_1679" s="T38">где</ta>
            <ta e="T40" id="Seg_1680" s="T39">жить-DUR.[3SG]</ta>
            <ta e="T41" id="Seg_1681" s="T40">там</ta>
            <ta e="T42" id="Seg_1682" s="T41">NEG</ta>
            <ta e="T43" id="Seg_1683" s="T42">далеко</ta>
            <ta e="T44" id="Seg_1684" s="T43">тогда</ta>
            <ta e="T45" id="Seg_1685" s="T44">волк.[NOM.SG]</ta>
            <ta e="T46" id="Seg_1686" s="T45">бежать-MOM-PST.[3SG]</ta>
            <ta e="T47" id="Seg_1687" s="T46">греметь-PST.[3SG]</ta>
            <ta e="T48" id="Seg_1688" s="T47">женщина.[NOM.SG]</ta>
            <ta e="T49" id="Seg_1689" s="T48">сказать-IPFVZ.[3SG]</ta>
            <ta e="T50" id="Seg_1690" s="T49">кто.[NOM.SG]</ta>
            <ta e="T51" id="Seg_1691" s="T50">там</ta>
            <ta e="T52" id="Seg_1692" s="T51">я.NOM</ta>
            <ta e="T53" id="Seg_1693" s="T52">ты.NOM</ta>
            <ta e="T54" id="Seg_1694" s="T53">внучка-NOM/GEN/ACC.2SG</ta>
            <ta e="T55" id="Seg_1695" s="T54">красный.[NOM.SG]</ta>
            <ta e="T56" id="Seg_1696" s="T55">шапка.[NOM.SG]</ta>
            <ta e="T57" id="Seg_1697" s="T56">ты.ACC</ta>
            <ta e="T58" id="Seg_1698" s="T57">прийти-PST-2SG</ta>
            <ta e="T59" id="Seg_1699" s="T58">смотреть-FRQ-INF.LAT</ta>
            <ta e="T60" id="Seg_1700" s="T59">развязать-IMP.2SG.O</ta>
            <ta e="T61" id="Seg_1701" s="T60">дверь.[NOM.SG]</ta>
            <ta e="T62" id="Seg_1702" s="T61">и</ta>
            <ta e="T63" id="Seg_1703" s="T62">прийти-FUT-2SG</ta>
            <ta e="T64" id="Seg_1704" s="T63">тогда</ta>
            <ta e="T65" id="Seg_1705" s="T64">этот.[NOM.SG]</ta>
            <ta e="T66" id="Seg_1706" s="T65">прийти-PST.[3SG]</ta>
            <ta e="T67" id="Seg_1707" s="T66">этот.[NOM.SG]</ta>
            <ta e="T68" id="Seg_1708" s="T67">женщина-NOM/GEN/ACC.1SG</ta>
            <ta e="T69" id="Seg_1709" s="T68">съесть-MOM-PST.[3SG]</ta>
            <ta e="T70" id="Seg_1710" s="T69">и</ta>
            <ta e="T71" id="Seg_1711" s="T70">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T72" id="Seg_1712" s="T71">ложиться-PST.[3SG]</ta>
            <ta e="T73" id="Seg_1713" s="T72">женщина-GEN</ta>
            <ta e="T74" id="Seg_1714" s="T73">шапка-ACC.3SG</ta>
            <ta e="T75" id="Seg_1715" s="T74">надеть-PST.[3SG]</ta>
            <ta e="T76" id="Seg_1716" s="T75">тогда</ta>
            <ta e="T77" id="Seg_1717" s="T76">прийти-PST.[3SG]</ta>
            <ta e="T78" id="Seg_1718" s="T77">девушка.[NOM.SG]</ta>
            <ta e="T79" id="Seg_1719" s="T78">красный.[NOM.SG]</ta>
            <ta e="T80" id="Seg_1720" s="T79">шапка.[NOM.SG]</ta>
            <ta e="T81" id="Seg_1721" s="T80">греметь-DUR.[3SG]</ta>
            <ta e="T82" id="Seg_1722" s="T81">дверь-GEN.3SG</ta>
            <ta e="T83" id="Seg_1723" s="T82">открыть-IMP.2SG.O</ta>
            <ta e="T84" id="Seg_1724" s="T83">этот.[NOM.SG]</ta>
            <ta e="T85" id="Seg_1725" s="T84">открыть-MOM-PST.[3SG]</ta>
            <ta e="T86" id="Seg_1726" s="T85">этот.[NOM.SG]</ta>
            <ta e="T87" id="Seg_1727" s="T86">прийти-PST.[3SG]</ta>
            <ta e="T88" id="Seg_1728" s="T87">класть-IMP.2SG.O</ta>
            <ta e="T89" id="Seg_1729" s="T88">стол-LAT</ta>
            <ta e="T90" id="Seg_1730" s="T89">пирог.[NOM.SG]</ta>
            <ta e="T91" id="Seg_1731" s="T90">масло.[NOM.SG]</ta>
            <ta e="T92" id="Seg_1732" s="T91">этот.[NOM.SG]</ta>
            <ta e="T93" id="Seg_1733" s="T92">класть-PST.[3SG]</ta>
            <ta e="T94" id="Seg_1734" s="T93">ну</ta>
            <ta e="T95" id="Seg_1735" s="T94">прийти-IMP.2SG</ta>
            <ta e="T96" id="Seg_1736" s="T95">я.LAT</ta>
            <ta e="T97" id="Seg_1737" s="T96">край-LAT/LOC.3SG</ta>
            <ta e="T99" id="Seg_1738" s="T98">сидеть-IMP.2SG</ta>
            <ta e="T100" id="Seg_1739" s="T99">этот.[NOM.SG]</ta>
            <ta e="T101" id="Seg_1740" s="T100">сидеть-DUR.[3SG]</ta>
            <ta e="T102" id="Seg_1741" s="T101">что.[NOM.SG]</ta>
            <ta e="T103" id="Seg_1742" s="T102">ты.NOM</ta>
            <ta e="T104" id="Seg_1743" s="T103">ухо-PL-NOM/GEN/ACC.2SG</ta>
            <ta e="T105" id="Seg_1744" s="T104">такой.[NOM.SG]</ta>
            <ta e="T106" id="Seg_1745" s="T105">большой-PL</ta>
            <ta e="T107" id="Seg_1746" s="T106">чтобы</ta>
            <ta e="T108" id="Seg_1747" s="T107">ты.ACC</ta>
            <ta e="T109" id="Seg_1748" s="T108">слушать-INF.LAT</ta>
            <ta e="T110" id="Seg_1749" s="T109">а</ta>
            <ta e="T111" id="Seg_1750" s="T110">что.[NOM.SG]</ta>
            <ta e="T112" id="Seg_1751" s="T111">глаз-PL-NOM/GEN/ACC.3SG</ta>
            <ta e="T113" id="Seg_1752" s="T112">большой-PL</ta>
            <ta e="T114" id="Seg_1753" s="T113">чтобы</ta>
            <ta e="T115" id="Seg_1754" s="T114">ты.ACC</ta>
            <ta e="T116" id="Seg_1755" s="T115">смотреть-FRQ-INF.LAT</ta>
            <ta e="T117" id="Seg_1756" s="T116">а</ta>
            <ta e="T118" id="Seg_1757" s="T117">что.[NOM.SG]</ta>
            <ta e="T119" id="Seg_1758" s="T118">рука-PL</ta>
            <ta e="T120" id="Seg_1759" s="T119">длинный-PL</ta>
            <ta e="T121" id="Seg_1760" s="T120">чтобы</ta>
            <ta e="T122" id="Seg_1761" s="T121">ты.ACC</ta>
            <ta e="T123" id="Seg_1762" s="T122">обнимать-INF.LAT</ta>
            <ta e="T124" id="Seg_1763" s="T123">а</ta>
            <ta e="T125" id="Seg_1764" s="T124">что.[NOM.SG]</ta>
            <ta e="T126" id="Seg_1765" s="T125">ты.NOM</ta>
            <ta e="T127" id="Seg_1766" s="T126">рот-NOM/GEN/ACC.3SG</ta>
            <ta e="T128" id="Seg_1767" s="T127">большой.[NOM.SG]</ta>
            <ta e="T129" id="Seg_1768" s="T128">чтобы</ta>
            <ta e="T130" id="Seg_1769" s="T129">ты.ACC</ta>
            <ta e="T131" id="Seg_1770" s="T130">съесть-INF.LAT</ta>
            <ta e="T132" id="Seg_1771" s="T131">тогда</ta>
            <ta e="T133" id="Seg_1772" s="T132">хватать-CVB</ta>
            <ta e="T134" id="Seg_1773" s="T133">взять-PST.[3SG]</ta>
            <ta e="T135" id="Seg_1774" s="T134">съесть-MOM-PST.[3SG]</ta>
            <ta e="T136" id="Seg_1775" s="T135">масло.[NOM.SG]</ta>
            <ta e="T137" id="Seg_1776" s="T136">съесть-MOM-PST.[3SG]</ta>
            <ta e="T138" id="Seg_1777" s="T137">и</ta>
            <ta e="T139" id="Seg_1778" s="T138">пирог.[NOM.SG]</ta>
            <ta e="T140" id="Seg_1779" s="T139">и</ta>
            <ta e="T141" id="Seg_1780" s="T140">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T142" id="Seg_1781" s="T141">лежать-DUR-PST.[3SG]</ta>
            <ta e="T143" id="Seg_1782" s="T142">и</ta>
            <ta e="T144" id="Seg_1783" s="T143">спать-DUR-PST.[3SG]</ta>
            <ta e="T145" id="Seg_1784" s="T144">здесь</ta>
            <ta e="T146" id="Seg_1785" s="T145">люди.[NOM.SG]</ta>
            <ta e="T147" id="Seg_1786" s="T146">прийти-PST-3PL</ta>
            <ta e="T148" id="Seg_1787" s="T147">ружье-INS</ta>
            <ta e="T149" id="Seg_1788" s="T148">этот.[NOM.SG]</ta>
            <ta e="T150" id="Seg_1789" s="T149">PTCL</ta>
            <ta e="T151" id="Seg_1790" s="T150">спать-DUR.[3SG]</ta>
            <ta e="T152" id="Seg_1791" s="T151">сильно</ta>
            <ta e="T153" id="Seg_1792" s="T152">тогда</ta>
            <ta e="T154" id="Seg_1793" s="T153">прийти-PST-3PL</ta>
            <ta e="T155" id="Seg_1794" s="T154">и</ta>
            <ta e="T156" id="Seg_1795" s="T155">этот-ACC</ta>
            <ta e="T157" id="Seg_1796" s="T156">живот-ACC.3SG</ta>
            <ta e="T158" id="Seg_1797" s="T157">нож-INS</ta>
            <ta e="T159" id="Seg_1798" s="T158">вставить-MOM-PST-3PL</ta>
            <ta e="T160" id="Seg_1799" s="T159">этот-PL</ta>
            <ta e="T161" id="Seg_1800" s="T160">женщина.[NOM.SG]</ta>
            <ta e="T162" id="Seg_1801" s="T161">и</ta>
            <ta e="T163" id="Seg_1802" s="T162">девушка.[NOM.SG]</ta>
            <ta e="T164" id="Seg_1803" s="T163">уйти-MOM-PST-3PL</ta>
            <ta e="T165" id="Seg_1804" s="T164">живой-PL</ta>
            <ta e="T166" id="Seg_1805" s="T165">а</ta>
            <ta e="T167" id="Seg_1806" s="T166">волк-NOM/GEN/ACC.3SG</ta>
            <ta e="T168" id="Seg_1807" s="T167">PTCL</ta>
            <ta e="T169" id="Seg_1808" s="T168">зарезать-CVB</ta>
            <ta e="T170" id="Seg_1809" s="T169">убить-PST-3PL</ta>
            <ta e="T171" id="Seg_1810" s="T170">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1811" s="T0">v-v:tense-v:pn</ta>
            <ta e="T2" id="Seg_1812" s="T1">n-n:case.poss</ta>
            <ta e="T3" id="Seg_1813" s="T2">conj</ta>
            <ta e="T4" id="Seg_1814" s="T3">n-n:case.poss</ta>
            <ta e="T5" id="Seg_1815" s="T4">adj-n:case</ta>
            <ta e="T6" id="Seg_1816" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_1817" s="T6">adv</ta>
            <ta e="T9" id="Seg_1818" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_1819" s="T9">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T11" id="Seg_1820" s="T10">n-n:case.poss</ta>
            <ta e="T12" id="Seg_1821" s="T11">v-v&gt;v-v:pn</ta>
            <ta e="T13" id="Seg_1822" s="T12">v-v:ins-v:mood.pn</ta>
            <ta e="T14" id="Seg_1823" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_1824" s="T14">v-v:mood.pn</ta>
            <ta e="T16" id="Seg_1825" s="T15">dempro-n:case</ta>
            <ta e="T17" id="Seg_1826" s="T16">v-v&gt;v-v:pn</ta>
            <ta e="T18" id="Seg_1827" s="T17">n-n:num</ta>
            <ta e="T19" id="Seg_1828" s="T18">v-v:tense-v:pn</ta>
            <ta e="T20" id="Seg_1829" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_1830" s="T20">v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_1831" s="T21">conj</ta>
            <ta e="T23" id="Seg_1832" s="T22">v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_1833" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_1834" s="T24">v-v:tense-v:pn</ta>
            <ta e="T26" id="Seg_1835" s="T25">n-n:num</ta>
            <ta e="T28" id="Seg_1836" s="T27">v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_1837" s="T28">v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_1838" s="T29">dempro-n:case</ta>
            <ta e="T31" id="Seg_1839" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_1840" s="T31">que</ta>
            <ta e="T33" id="Seg_1841" s="T32">v-v:tense-v:pn</ta>
            <ta e="T34" id="Seg_1842" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_1843" s="T34">dempro-n:case</ta>
            <ta e="T36" id="Seg_1844" s="T35">v-v&gt;v-v:pn</ta>
            <ta e="T37" id="Seg_1845" s="T36">conj</ta>
            <ta e="T38" id="Seg_1846" s="T37">dempro-n:case</ta>
            <ta e="T39" id="Seg_1847" s="T38">que</ta>
            <ta e="T40" id="Seg_1848" s="T39">v-v&gt;v-v:pn</ta>
            <ta e="T41" id="Seg_1849" s="T40">adv</ta>
            <ta e="T42" id="Seg_1850" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_1851" s="T42">adv</ta>
            <ta e="T44" id="Seg_1852" s="T43">adv</ta>
            <ta e="T45" id="Seg_1853" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_1854" s="T45">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T47" id="Seg_1855" s="T46">v-v:tense-v:pn</ta>
            <ta e="T48" id="Seg_1856" s="T47">n-n:case</ta>
            <ta e="T49" id="Seg_1857" s="T48">v-v&gt;v-v:pn</ta>
            <ta e="T50" id="Seg_1858" s="T49">que-n:case</ta>
            <ta e="T51" id="Seg_1859" s="T50">adv</ta>
            <ta e="T52" id="Seg_1860" s="T51">pers</ta>
            <ta e="T53" id="Seg_1861" s="T52">pers</ta>
            <ta e="T54" id="Seg_1862" s="T53">n-n:case.poss</ta>
            <ta e="T55" id="Seg_1863" s="T54">adj-n:case</ta>
            <ta e="T56" id="Seg_1864" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_1865" s="T56">pers</ta>
            <ta e="T58" id="Seg_1866" s="T57">v-v:tense-v:pn</ta>
            <ta e="T59" id="Seg_1867" s="T58">v-v&gt;v-v:n.fin</ta>
            <ta e="T60" id="Seg_1868" s="T59">v-v:mood.pn</ta>
            <ta e="T61" id="Seg_1869" s="T60">n-n:case</ta>
            <ta e="T62" id="Seg_1870" s="T61">conj</ta>
            <ta e="T63" id="Seg_1871" s="T62">v-v:tense-v:pn</ta>
            <ta e="T64" id="Seg_1872" s="T63">adv</ta>
            <ta e="T65" id="Seg_1873" s="T64">dempro-n:case</ta>
            <ta e="T66" id="Seg_1874" s="T65">v-v:tense-v:pn</ta>
            <ta e="T67" id="Seg_1875" s="T66">dempro-n:case</ta>
            <ta e="T68" id="Seg_1876" s="T67">n-n:case.poss</ta>
            <ta e="T69" id="Seg_1877" s="T68">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T70" id="Seg_1878" s="T69">conj</ta>
            <ta e="T71" id="Seg_1879" s="T70">refl-n:case.poss</ta>
            <ta e="T72" id="Seg_1880" s="T71">v-v:tense-v:pn</ta>
            <ta e="T73" id="Seg_1881" s="T72">n-n:case</ta>
            <ta e="T74" id="Seg_1882" s="T73">n-n:case.poss</ta>
            <ta e="T75" id="Seg_1883" s="T74">v-v:tense-v:pn</ta>
            <ta e="T76" id="Seg_1884" s="T75">adv</ta>
            <ta e="T77" id="Seg_1885" s="T76">v-v:tense-v:pn</ta>
            <ta e="T78" id="Seg_1886" s="T77">n-n:case</ta>
            <ta e="T79" id="Seg_1887" s="T78">adj-n:case</ta>
            <ta e="T80" id="Seg_1888" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_1889" s="T80">v-v&gt;v-v:pn</ta>
            <ta e="T82" id="Seg_1890" s="T81">n-n:case.poss</ta>
            <ta e="T83" id="Seg_1891" s="T82">v-v:mood.pn</ta>
            <ta e="T84" id="Seg_1892" s="T83">dempro-n:case</ta>
            <ta e="T85" id="Seg_1893" s="T84">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T86" id="Seg_1894" s="T85">dempro-n:case</ta>
            <ta e="T87" id="Seg_1895" s="T86">v-v:tense-v:pn</ta>
            <ta e="T88" id="Seg_1896" s="T87">v-v:mood.pn</ta>
            <ta e="T89" id="Seg_1897" s="T88">n-n:case</ta>
            <ta e="T90" id="Seg_1898" s="T89">n-n:case</ta>
            <ta e="T91" id="Seg_1899" s="T90">n-n:case</ta>
            <ta e="T92" id="Seg_1900" s="T91">dempro-n:case</ta>
            <ta e="T93" id="Seg_1901" s="T92">v-v:tense-v:pn</ta>
            <ta e="T94" id="Seg_1902" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_1903" s="T94">v-v:mood.pn</ta>
            <ta e="T96" id="Seg_1904" s="T95">pers</ta>
            <ta e="T97" id="Seg_1905" s="T96">n-n:case.poss</ta>
            <ta e="T99" id="Seg_1906" s="T98">v-v:mood.pn</ta>
            <ta e="T100" id="Seg_1907" s="T99">dempro-n:case</ta>
            <ta e="T101" id="Seg_1908" s="T100">v-v&gt;v-v:pn</ta>
            <ta e="T102" id="Seg_1909" s="T101">que-n:case</ta>
            <ta e="T103" id="Seg_1910" s="T102">pers</ta>
            <ta e="T104" id="Seg_1911" s="T103">n-n:num-n:case.poss</ta>
            <ta e="T105" id="Seg_1912" s="T104">adj-n:case</ta>
            <ta e="T106" id="Seg_1913" s="T105">adj-n:num</ta>
            <ta e="T107" id="Seg_1914" s="T106">conj</ta>
            <ta e="T108" id="Seg_1915" s="T107">pers</ta>
            <ta e="T109" id="Seg_1916" s="T108">v-v:n.fin</ta>
            <ta e="T110" id="Seg_1917" s="T109">conj</ta>
            <ta e="T111" id="Seg_1918" s="T110">que-n:case</ta>
            <ta e="T112" id="Seg_1919" s="T111">n-n:num-n:case.poss</ta>
            <ta e="T113" id="Seg_1920" s="T112">adj-n:num</ta>
            <ta e="T114" id="Seg_1921" s="T113">conj</ta>
            <ta e="T115" id="Seg_1922" s="T114">pers</ta>
            <ta e="T116" id="Seg_1923" s="T115">v-v&gt;v-v:n.fin</ta>
            <ta e="T117" id="Seg_1924" s="T116">conj</ta>
            <ta e="T118" id="Seg_1925" s="T117">que-n:case</ta>
            <ta e="T119" id="Seg_1926" s="T118">n-n:num</ta>
            <ta e="T120" id="Seg_1927" s="T119">adj-n:num</ta>
            <ta e="T121" id="Seg_1928" s="T120">conj</ta>
            <ta e="T122" id="Seg_1929" s="T121">pers</ta>
            <ta e="T123" id="Seg_1930" s="T122">v-v:n.fin</ta>
            <ta e="T124" id="Seg_1931" s="T123">conj</ta>
            <ta e="T125" id="Seg_1932" s="T124">que-n:case</ta>
            <ta e="T126" id="Seg_1933" s="T125">pers</ta>
            <ta e="T127" id="Seg_1934" s="T126">n-n:case.poss</ta>
            <ta e="T128" id="Seg_1935" s="T127">adj-n:case</ta>
            <ta e="T129" id="Seg_1936" s="T128">conj</ta>
            <ta e="T130" id="Seg_1937" s="T129">pers</ta>
            <ta e="T131" id="Seg_1938" s="T130">v-v:n.fin</ta>
            <ta e="T132" id="Seg_1939" s="T131">adv</ta>
            <ta e="T133" id="Seg_1940" s="T132">v-v:n.fin</ta>
            <ta e="T134" id="Seg_1941" s="T133">v-v:tense-v:pn</ta>
            <ta e="T135" id="Seg_1942" s="T134">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T136" id="Seg_1943" s="T135">n-n:case</ta>
            <ta e="T137" id="Seg_1944" s="T136">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T138" id="Seg_1945" s="T137">conj</ta>
            <ta e="T139" id="Seg_1946" s="T138">n-n:case</ta>
            <ta e="T140" id="Seg_1947" s="T139">conj</ta>
            <ta e="T141" id="Seg_1948" s="T140">refl-n:case.poss</ta>
            <ta e="T142" id="Seg_1949" s="T141">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T143" id="Seg_1950" s="T142">conj</ta>
            <ta e="T144" id="Seg_1951" s="T143">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T145" id="Seg_1952" s="T144">adv</ta>
            <ta e="T146" id="Seg_1953" s="T145">n-n:case</ta>
            <ta e="T147" id="Seg_1954" s="T146">v-v:tense-v:pn</ta>
            <ta e="T148" id="Seg_1955" s="T147">n-n:case</ta>
            <ta e="T149" id="Seg_1956" s="T148">dempro-n:case</ta>
            <ta e="T150" id="Seg_1957" s="T149">ptcl</ta>
            <ta e="T151" id="Seg_1958" s="T150">v-v&gt;v-v:pn</ta>
            <ta e="T152" id="Seg_1959" s="T151">adv</ta>
            <ta e="T153" id="Seg_1960" s="T152">adv</ta>
            <ta e="T154" id="Seg_1961" s="T153">v-v:tense-v:pn</ta>
            <ta e="T155" id="Seg_1962" s="T154">conj</ta>
            <ta e="T156" id="Seg_1963" s="T155">dempro-n:case</ta>
            <ta e="T157" id="Seg_1964" s="T156">n-n:case.poss</ta>
            <ta e="T158" id="Seg_1965" s="T157">n-n:case</ta>
            <ta e="T159" id="Seg_1966" s="T158">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T160" id="Seg_1967" s="T159">dempro-n:num</ta>
            <ta e="T161" id="Seg_1968" s="T160">n-n:case</ta>
            <ta e="T162" id="Seg_1969" s="T161">conj</ta>
            <ta e="T163" id="Seg_1970" s="T162">n-n:case</ta>
            <ta e="T164" id="Seg_1971" s="T163">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T165" id="Seg_1972" s="T164">adj-n:num</ta>
            <ta e="T166" id="Seg_1973" s="T165">conj</ta>
            <ta e="T167" id="Seg_1974" s="T166">n-n:case.poss</ta>
            <ta e="T168" id="Seg_1975" s="T167">ptcl</ta>
            <ta e="T169" id="Seg_1976" s="T168">v-v:n.fin</ta>
            <ta e="T170" id="Seg_1977" s="T169">v-v:tense-v:pn</ta>
            <ta e="T171" id="Seg_1978" s="T170">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1979" s="T0">v</ta>
            <ta e="T2" id="Seg_1980" s="T1">n</ta>
            <ta e="T3" id="Seg_1981" s="T2">conj</ta>
            <ta e="T4" id="Seg_1982" s="T3">n</ta>
            <ta e="T5" id="Seg_1983" s="T4">adj</ta>
            <ta e="T6" id="Seg_1984" s="T5">n</ta>
            <ta e="T7" id="Seg_1985" s="T6">adv</ta>
            <ta e="T9" id="Seg_1986" s="T8">n</ta>
            <ta e="T10" id="Seg_1987" s="T9">v</ta>
            <ta e="T11" id="Seg_1988" s="T10">n</ta>
            <ta e="T12" id="Seg_1989" s="T11">v</ta>
            <ta e="T13" id="Seg_1990" s="T12">v</ta>
            <ta e="T14" id="Seg_1991" s="T13">n</ta>
            <ta e="T15" id="Seg_1992" s="T14">v</ta>
            <ta e="T16" id="Seg_1993" s="T15">dempro</ta>
            <ta e="T17" id="Seg_1994" s="T16">v</ta>
            <ta e="T18" id="Seg_1995" s="T17">n</ta>
            <ta e="T19" id="Seg_1996" s="T18">v</ta>
            <ta e="T20" id="Seg_1997" s="T19">n</ta>
            <ta e="T21" id="Seg_1998" s="T20">v</ta>
            <ta e="T22" id="Seg_1999" s="T21">conj</ta>
            <ta e="T23" id="Seg_2000" s="T22">v</ta>
            <ta e="T24" id="Seg_2001" s="T23">n</ta>
            <ta e="T25" id="Seg_2002" s="T24">v</ta>
            <ta e="T26" id="Seg_2003" s="T25">n</ta>
            <ta e="T28" id="Seg_2004" s="T27">v</ta>
            <ta e="T29" id="Seg_2005" s="T28">v</ta>
            <ta e="T30" id="Seg_2006" s="T29">dempro</ta>
            <ta e="T31" id="Seg_2007" s="T30">n</ta>
            <ta e="T32" id="Seg_2008" s="T31">que</ta>
            <ta e="T33" id="Seg_2009" s="T32">v</ta>
            <ta e="T34" id="Seg_2010" s="T33">n</ta>
            <ta e="T35" id="Seg_2011" s="T34">dempro</ta>
            <ta e="T36" id="Seg_2012" s="T35">v</ta>
            <ta e="T37" id="Seg_2013" s="T36">conj</ta>
            <ta e="T38" id="Seg_2014" s="T37">dempro</ta>
            <ta e="T39" id="Seg_2015" s="T38">que</ta>
            <ta e="T40" id="Seg_2016" s="T39">v</ta>
            <ta e="T41" id="Seg_2017" s="T40">adv</ta>
            <ta e="T42" id="Seg_2018" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_2019" s="T42">adv</ta>
            <ta e="T44" id="Seg_2020" s="T43">adv</ta>
            <ta e="T45" id="Seg_2021" s="T44">n</ta>
            <ta e="T46" id="Seg_2022" s="T45">v</ta>
            <ta e="T47" id="Seg_2023" s="T46">v</ta>
            <ta e="T48" id="Seg_2024" s="T47">n</ta>
            <ta e="T49" id="Seg_2025" s="T48">v</ta>
            <ta e="T50" id="Seg_2026" s="T49">que</ta>
            <ta e="T51" id="Seg_2027" s="T50">adv</ta>
            <ta e="T52" id="Seg_2028" s="T51">pers</ta>
            <ta e="T53" id="Seg_2029" s="T52">pers</ta>
            <ta e="T54" id="Seg_2030" s="T53">n</ta>
            <ta e="T55" id="Seg_2031" s="T54">adj</ta>
            <ta e="T56" id="Seg_2032" s="T55">n</ta>
            <ta e="T57" id="Seg_2033" s="T56">pers</ta>
            <ta e="T58" id="Seg_2034" s="T57">v</ta>
            <ta e="T59" id="Seg_2035" s="T58">v</ta>
            <ta e="T60" id="Seg_2036" s="T59">v</ta>
            <ta e="T61" id="Seg_2037" s="T60">n</ta>
            <ta e="T62" id="Seg_2038" s="T61">conj</ta>
            <ta e="T63" id="Seg_2039" s="T62">v</ta>
            <ta e="T64" id="Seg_2040" s="T63">adv</ta>
            <ta e="T65" id="Seg_2041" s="T64">dempro</ta>
            <ta e="T66" id="Seg_2042" s="T65">v</ta>
            <ta e="T67" id="Seg_2043" s="T66">dempro</ta>
            <ta e="T68" id="Seg_2044" s="T67">n</ta>
            <ta e="T69" id="Seg_2045" s="T68">v</ta>
            <ta e="T70" id="Seg_2046" s="T69">conj</ta>
            <ta e="T71" id="Seg_2047" s="T70">refl</ta>
            <ta e="T72" id="Seg_2048" s="T71">v</ta>
            <ta e="T73" id="Seg_2049" s="T72">n</ta>
            <ta e="T74" id="Seg_2050" s="T73">n</ta>
            <ta e="T75" id="Seg_2051" s="T74">v</ta>
            <ta e="T76" id="Seg_2052" s="T75">adv</ta>
            <ta e="T77" id="Seg_2053" s="T76">v</ta>
            <ta e="T78" id="Seg_2054" s="T77">n</ta>
            <ta e="T79" id="Seg_2055" s="T78">adj</ta>
            <ta e="T80" id="Seg_2056" s="T79">n</ta>
            <ta e="T81" id="Seg_2057" s="T80">v</ta>
            <ta e="T82" id="Seg_2058" s="T81">n</ta>
            <ta e="T83" id="Seg_2059" s="T82">v</ta>
            <ta e="T84" id="Seg_2060" s="T83">dempro</ta>
            <ta e="T85" id="Seg_2061" s="T84">v</ta>
            <ta e="T86" id="Seg_2062" s="T85">dempro</ta>
            <ta e="T87" id="Seg_2063" s="T86">v</ta>
            <ta e="T88" id="Seg_2064" s="T87">v</ta>
            <ta e="T89" id="Seg_2065" s="T88">n</ta>
            <ta e="T90" id="Seg_2066" s="T89">n</ta>
            <ta e="T91" id="Seg_2067" s="T90">n</ta>
            <ta e="T92" id="Seg_2068" s="T91">dempro</ta>
            <ta e="T93" id="Seg_2069" s="T92">v</ta>
            <ta e="T94" id="Seg_2070" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_2071" s="T94">v</ta>
            <ta e="T96" id="Seg_2072" s="T95">pers</ta>
            <ta e="T97" id="Seg_2073" s="T96">n</ta>
            <ta e="T99" id="Seg_2074" s="T98">v</ta>
            <ta e="T100" id="Seg_2075" s="T99">dempro</ta>
            <ta e="T101" id="Seg_2076" s="T100">v</ta>
            <ta e="T102" id="Seg_2077" s="T101">que</ta>
            <ta e="T103" id="Seg_2078" s="T102">pers</ta>
            <ta e="T104" id="Seg_2079" s="T103">n</ta>
            <ta e="T105" id="Seg_2080" s="T104">adj</ta>
            <ta e="T106" id="Seg_2081" s="T105">adj</ta>
            <ta e="T107" id="Seg_2082" s="T106">conj</ta>
            <ta e="T108" id="Seg_2083" s="T107">pers</ta>
            <ta e="T109" id="Seg_2084" s="T108">v</ta>
            <ta e="T110" id="Seg_2085" s="T109">conj</ta>
            <ta e="T111" id="Seg_2086" s="T110">que</ta>
            <ta e="T112" id="Seg_2087" s="T111">n</ta>
            <ta e="T113" id="Seg_2088" s="T112">adj</ta>
            <ta e="T114" id="Seg_2089" s="T113">conj</ta>
            <ta e="T115" id="Seg_2090" s="T114">pers</ta>
            <ta e="T116" id="Seg_2091" s="T115">v</ta>
            <ta e="T117" id="Seg_2092" s="T116">conj</ta>
            <ta e="T118" id="Seg_2093" s="T117">que</ta>
            <ta e="T119" id="Seg_2094" s="T118">n</ta>
            <ta e="T120" id="Seg_2095" s="T119">adj</ta>
            <ta e="T121" id="Seg_2096" s="T120">conj</ta>
            <ta e="T122" id="Seg_2097" s="T121">pers</ta>
            <ta e="T123" id="Seg_2098" s="T122">v</ta>
            <ta e="T124" id="Seg_2099" s="T123">conj</ta>
            <ta e="T125" id="Seg_2100" s="T124">que</ta>
            <ta e="T126" id="Seg_2101" s="T125">pers</ta>
            <ta e="T127" id="Seg_2102" s="T126">n</ta>
            <ta e="T128" id="Seg_2103" s="T127">adj</ta>
            <ta e="T129" id="Seg_2104" s="T128">conj</ta>
            <ta e="T130" id="Seg_2105" s="T129">pers</ta>
            <ta e="T131" id="Seg_2106" s="T130">v</ta>
            <ta e="T132" id="Seg_2107" s="T131">adv</ta>
            <ta e="T133" id="Seg_2108" s="T132">v</ta>
            <ta e="T134" id="Seg_2109" s="T133">v</ta>
            <ta e="T135" id="Seg_2110" s="T134">v</ta>
            <ta e="T136" id="Seg_2111" s="T135">n</ta>
            <ta e="T137" id="Seg_2112" s="T136">v</ta>
            <ta e="T138" id="Seg_2113" s="T137">conj</ta>
            <ta e="T139" id="Seg_2114" s="T138">n</ta>
            <ta e="T140" id="Seg_2115" s="T139">conj</ta>
            <ta e="T141" id="Seg_2116" s="T140">refl</ta>
            <ta e="T142" id="Seg_2117" s="T141">v</ta>
            <ta e="T143" id="Seg_2118" s="T142">conj</ta>
            <ta e="T144" id="Seg_2119" s="T143">v</ta>
            <ta e="T145" id="Seg_2120" s="T144">adv</ta>
            <ta e="T146" id="Seg_2121" s="T145">n</ta>
            <ta e="T147" id="Seg_2122" s="T146">v</ta>
            <ta e="T148" id="Seg_2123" s="T147">n</ta>
            <ta e="T149" id="Seg_2124" s="T148">dempro</ta>
            <ta e="T150" id="Seg_2125" s="T149">ptcl</ta>
            <ta e="T151" id="Seg_2126" s="T150">v</ta>
            <ta e="T152" id="Seg_2127" s="T151">adv</ta>
            <ta e="T153" id="Seg_2128" s="T152">adv</ta>
            <ta e="T154" id="Seg_2129" s="T153">v</ta>
            <ta e="T155" id="Seg_2130" s="T154">conj</ta>
            <ta e="T156" id="Seg_2131" s="T155">dempro</ta>
            <ta e="T157" id="Seg_2132" s="T156">n</ta>
            <ta e="T158" id="Seg_2133" s="T157">n</ta>
            <ta e="T159" id="Seg_2134" s="T158">v</ta>
            <ta e="T160" id="Seg_2135" s="T159">dempro</ta>
            <ta e="T161" id="Seg_2136" s="T160">n</ta>
            <ta e="T162" id="Seg_2137" s="T161">conj</ta>
            <ta e="T163" id="Seg_2138" s="T162">n</ta>
            <ta e="T164" id="Seg_2139" s="T163">v</ta>
            <ta e="T165" id="Seg_2140" s="T164">adj</ta>
            <ta e="T166" id="Seg_2141" s="T165">conj</ta>
            <ta e="T167" id="Seg_2142" s="T166">n</ta>
            <ta e="T168" id="Seg_2143" s="T167">ptcl</ta>
            <ta e="T169" id="Seg_2144" s="T168">v</ta>
            <ta e="T170" id="Seg_2145" s="T169">v</ta>
            <ta e="T171" id="Seg_2146" s="T170">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_2147" s="T1">np.h:E</ta>
            <ta e="T4" id="Seg_2148" s="T3">np.h:E</ta>
            <ta e="T7" id="Seg_2149" s="T6">adv:Time</ta>
            <ta e="T9" id="Seg_2150" s="T8">np.h:E</ta>
            <ta e="T11" id="Seg_2151" s="T10">np.h:A</ta>
            <ta e="T13" id="Seg_2152" s="T12">0.2.h:A</ta>
            <ta e="T14" id="Seg_2153" s="T13">np.h:Th</ta>
            <ta e="T15" id="Seg_2154" s="T14">0.2.h:E</ta>
            <ta e="T16" id="Seg_2155" s="T15">pro.h:E</ta>
            <ta e="T18" id="Seg_2156" s="T17">np:Th</ta>
            <ta e="T19" id="Seg_2157" s="T18">0.3.h:A</ta>
            <ta e="T20" id="Seg_2158" s="T19">np:Th</ta>
            <ta e="T21" id="Seg_2159" s="T20">0.3.h:A</ta>
            <ta e="T23" id="Seg_2160" s="T22">0.3.h:A</ta>
            <ta e="T24" id="Seg_2161" s="T23">np:Th</ta>
            <ta e="T25" id="Seg_2162" s="T24">0.3.h:A</ta>
            <ta e="T26" id="Seg_2163" s="T25">np:P</ta>
            <ta e="T28" id="Seg_2164" s="T27">0.3.h:A</ta>
            <ta e="T30" id="Seg_2165" s="T29">pro:G</ta>
            <ta e="T31" id="Seg_2166" s="T30">np.h:A</ta>
            <ta e="T33" id="Seg_2167" s="T32">0.2.h:A</ta>
            <ta e="T34" id="Seg_2168" s="T33">np:G</ta>
            <ta e="T35" id="Seg_2169" s="T34">pro.h:E</ta>
            <ta e="T38" id="Seg_2170" s="T37">pro.h:E</ta>
            <ta e="T41" id="Seg_2171" s="T40">adv:L</ta>
            <ta e="T44" id="Seg_2172" s="T43">adv:Time</ta>
            <ta e="T45" id="Seg_2173" s="T44">np.h:A</ta>
            <ta e="T47" id="Seg_2174" s="T46">0.3.h:A</ta>
            <ta e="T48" id="Seg_2175" s="T47">np.h:A</ta>
            <ta e="T50" id="Seg_2176" s="T49">pro.h:Th</ta>
            <ta e="T52" id="Seg_2177" s="T51">pro.h:A</ta>
            <ta e="T53" id="Seg_2178" s="T52">pro.h:Poss</ta>
            <ta e="T54" id="Seg_2179" s="T53">np.h:Th</ta>
            <ta e="T57" id="Seg_2180" s="T56">pro.h:Th</ta>
            <ta e="T58" id="Seg_2181" s="T57">0.2.h:A</ta>
            <ta e="T60" id="Seg_2182" s="T59">0.2.h:A</ta>
            <ta e="T61" id="Seg_2183" s="T60">np:Th</ta>
            <ta e="T63" id="Seg_2184" s="T62">0.2.h:A</ta>
            <ta e="T64" id="Seg_2185" s="T63">adv:Time</ta>
            <ta e="T65" id="Seg_2186" s="T64">pro.h:A</ta>
            <ta e="T68" id="Seg_2187" s="T67">np.h:P</ta>
            <ta e="T69" id="Seg_2188" s="T68">0.3.h:A</ta>
            <ta e="T72" id="Seg_2189" s="T71">0.3.h:A</ta>
            <ta e="T73" id="Seg_2190" s="T72">np.h:Poss</ta>
            <ta e="T74" id="Seg_2191" s="T73">np:Th</ta>
            <ta e="T75" id="Seg_2192" s="T74">0.3.h:A</ta>
            <ta e="T76" id="Seg_2193" s="T75">adv:Time</ta>
            <ta e="T78" id="Seg_2194" s="T77">np.h:A</ta>
            <ta e="T81" id="Seg_2195" s="T80">0.3.h:A</ta>
            <ta e="T82" id="Seg_2196" s="T81">np:Th</ta>
            <ta e="T83" id="Seg_2197" s="T82">0.2.h:A</ta>
            <ta e="T84" id="Seg_2198" s="T83">pro.h:A</ta>
            <ta e="T86" id="Seg_2199" s="T85">pro.h:A</ta>
            <ta e="T88" id="Seg_2200" s="T87">0.2.h:A</ta>
            <ta e="T89" id="Seg_2201" s="T88">np:G</ta>
            <ta e="T90" id="Seg_2202" s="T89">np:Th</ta>
            <ta e="T91" id="Seg_2203" s="T90">np:Th</ta>
            <ta e="T92" id="Seg_2204" s="T91">pro.h:A</ta>
            <ta e="T95" id="Seg_2205" s="T94">0.2.h:A</ta>
            <ta e="T96" id="Seg_2206" s="T95">pro.h:Poss</ta>
            <ta e="T97" id="Seg_2207" s="T96">np:L</ta>
            <ta e="T99" id="Seg_2208" s="T98">0.2.h:E</ta>
            <ta e="T100" id="Seg_2209" s="T99">pro.h:E</ta>
            <ta e="T103" id="Seg_2210" s="T102">pro.h:Poss</ta>
            <ta e="T104" id="Seg_2211" s="T103">np:Th</ta>
            <ta e="T108" id="Seg_2212" s="T107">pro.h:Th</ta>
            <ta e="T112" id="Seg_2213" s="T111">np:Th</ta>
            <ta e="T115" id="Seg_2214" s="T114">pro.h:Th</ta>
            <ta e="T119" id="Seg_2215" s="T118">np:Th</ta>
            <ta e="T122" id="Seg_2216" s="T121">pro.h:Th</ta>
            <ta e="T126" id="Seg_2217" s="T125">pro.h:Poss</ta>
            <ta e="T127" id="Seg_2218" s="T126">np:Th</ta>
            <ta e="T130" id="Seg_2219" s="T129">pro.h:P</ta>
            <ta e="T132" id="Seg_2220" s="T131">adv:Time</ta>
            <ta e="T134" id="Seg_2221" s="T133">0.3.h:A</ta>
            <ta e="T135" id="Seg_2222" s="T134">0.3.h:A</ta>
            <ta e="T136" id="Seg_2223" s="T135">np:P</ta>
            <ta e="T137" id="Seg_2224" s="T136">0.3.h:A</ta>
            <ta e="T139" id="Seg_2225" s="T138">np:P</ta>
            <ta e="T142" id="Seg_2226" s="T141">0.3.h:E</ta>
            <ta e="T144" id="Seg_2227" s="T143">0.3.h:E</ta>
            <ta e="T145" id="Seg_2228" s="T144">adv:L</ta>
            <ta e="T146" id="Seg_2229" s="T145">np.h:A</ta>
            <ta e="T149" id="Seg_2230" s="T148">pro.h:A</ta>
            <ta e="T153" id="Seg_2231" s="T152">adv:Time</ta>
            <ta e="T154" id="Seg_2232" s="T153">0.3.h:A</ta>
            <ta e="T156" id="Seg_2233" s="T155">pro.h:Poss</ta>
            <ta e="T157" id="Seg_2234" s="T156">np:P</ta>
            <ta e="T158" id="Seg_2235" s="T157">np:Ins</ta>
            <ta e="T159" id="Seg_2236" s="T158">0.3.h:A</ta>
            <ta e="T160" id="Seg_2237" s="T159">pro.h:A</ta>
            <ta e="T161" id="Seg_2238" s="T160">np.h:A</ta>
            <ta e="T163" id="Seg_2239" s="T162">np.h:A</ta>
            <ta e="T167" id="Seg_2240" s="T166">np.h:P</ta>
            <ta e="T170" id="Seg_2241" s="T169">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_2242" s="T0">v:pred </ta>
            <ta e="T2" id="Seg_2243" s="T1">np.h:S</ta>
            <ta e="T4" id="Seg_2244" s="T3">np.h:S</ta>
            <ta e="T9" id="Seg_2245" s="T8">np.h:S</ta>
            <ta e="T10" id="Seg_2246" s="T9">v:pred</ta>
            <ta e="T11" id="Seg_2247" s="T10">np.h:S</ta>
            <ta e="T12" id="Seg_2248" s="T11">v:pred</ta>
            <ta e="T13" id="Seg_2249" s="T12">v:pred 0.2.h:S</ta>
            <ta e="T14" id="Seg_2250" s="T13">np.h:O</ta>
            <ta e="T15" id="Seg_2251" s="T14">v:pred 0.2.h:S</ta>
            <ta e="T16" id="Seg_2252" s="T15">pro.h:S</ta>
            <ta e="T17" id="Seg_2253" s="T16">v:pred</ta>
            <ta e="T18" id="Seg_2254" s="T17">np:O</ta>
            <ta e="T19" id="Seg_2255" s="T18">v:pred 0.3.h:S</ta>
            <ta e="T20" id="Seg_2256" s="T19">np:O</ta>
            <ta e="T21" id="Seg_2257" s="T20">v:pred 0.3.h:S</ta>
            <ta e="T23" id="Seg_2258" s="T22">v:pred 0.3.h:S</ta>
            <ta e="T24" id="Seg_2259" s="T23">np:O</ta>
            <ta e="T25" id="Seg_2260" s="T24">v:pred 0.3.h:S</ta>
            <ta e="T26" id="Seg_2261" s="T25">np:O</ta>
            <ta e="T28" id="Seg_2262" s="T27"> 0.3.h:S v:pred</ta>
            <ta e="T29" id="Seg_2263" s="T28">v:pred</ta>
            <ta e="T31" id="Seg_2264" s="T30">np.h:S</ta>
            <ta e="T33" id="Seg_2265" s="T32">v:pred 0.2.h:S</ta>
            <ta e="T35" id="Seg_2266" s="T34">pro.h:S</ta>
            <ta e="T36" id="Seg_2267" s="T35">v:pred</ta>
            <ta e="T38" id="Seg_2268" s="T37">pro.h:S</ta>
            <ta e="T40" id="Seg_2269" s="T39">v:pred</ta>
            <ta e="T42" id="Seg_2270" s="T41">ptcl.neg</ta>
            <ta e="T45" id="Seg_2271" s="T44">np.h:S</ta>
            <ta e="T46" id="Seg_2272" s="T45">v:pred</ta>
            <ta e="T47" id="Seg_2273" s="T46">v:pred 0.3.h:S</ta>
            <ta e="T48" id="Seg_2274" s="T47">np.h:S</ta>
            <ta e="T49" id="Seg_2275" s="T48">v:pred</ta>
            <ta e="T50" id="Seg_2276" s="T49">pro.h:S</ta>
            <ta e="T51" id="Seg_2277" s="T50">adj:pred</ta>
            <ta e="T52" id="Seg_2278" s="T51">pro.h:S</ta>
            <ta e="T54" id="Seg_2279" s="T53">n:pred</ta>
            <ta e="T57" id="Seg_2280" s="T56">pro.h:O</ta>
            <ta e="T58" id="Seg_2281" s="T57">v:pred 0.2.h:S</ta>
            <ta e="T59" id="Seg_2282" s="T58">s:purp</ta>
            <ta e="T60" id="Seg_2283" s="T59">v:pred 0.2.h:S</ta>
            <ta e="T61" id="Seg_2284" s="T60">np:O</ta>
            <ta e="T63" id="Seg_2285" s="T62">v:pred 0.2.h:S</ta>
            <ta e="T65" id="Seg_2286" s="T64">pro.h:S</ta>
            <ta e="T66" id="Seg_2287" s="T65">v:pred</ta>
            <ta e="T68" id="Seg_2288" s="T67">np.h:O</ta>
            <ta e="T69" id="Seg_2289" s="T68">v:pred 0.3.h:S</ta>
            <ta e="T72" id="Seg_2290" s="T71">v:pred 0.3.h:S</ta>
            <ta e="T74" id="Seg_2291" s="T73">np:O</ta>
            <ta e="T75" id="Seg_2292" s="T74">v:pred 0.3.h:S</ta>
            <ta e="T77" id="Seg_2293" s="T76">v:pred</ta>
            <ta e="T78" id="Seg_2294" s="T77">np.h:S</ta>
            <ta e="T81" id="Seg_2295" s="T80">v:pred 0.3.h:S</ta>
            <ta e="T83" id="Seg_2296" s="T82">v:pred 0.2.h:S</ta>
            <ta e="T84" id="Seg_2297" s="T83">pro.h:S</ta>
            <ta e="T85" id="Seg_2298" s="T84">v:pred</ta>
            <ta e="T86" id="Seg_2299" s="T85">pro.h:S</ta>
            <ta e="T87" id="Seg_2300" s="T86">v:pred</ta>
            <ta e="T88" id="Seg_2301" s="T87">v:pred 0.2.h:S</ta>
            <ta e="T90" id="Seg_2302" s="T89">np:O</ta>
            <ta e="T91" id="Seg_2303" s="T90">np:O</ta>
            <ta e="T92" id="Seg_2304" s="T91">pro.h:S</ta>
            <ta e="T93" id="Seg_2305" s="T92">v:pred</ta>
            <ta e="T95" id="Seg_2306" s="T94">v:pred 0.2.h:S</ta>
            <ta e="T99" id="Seg_2307" s="T98">v:pred 0.2.h:S</ta>
            <ta e="T100" id="Seg_2308" s="T99">pro.h:S</ta>
            <ta e="T101" id="Seg_2309" s="T100">v:pred</ta>
            <ta e="T104" id="Seg_2310" s="T103">np:S</ta>
            <ta e="T106" id="Seg_2311" s="T105">adj:pred</ta>
            <ta e="T108" id="Seg_2312" s="T107">pro.h:O</ta>
            <ta e="T109" id="Seg_2313" s="T108">v:pred s:purp</ta>
            <ta e="T112" id="Seg_2314" s="T111">np:S</ta>
            <ta e="T113" id="Seg_2315" s="T112">adj:pred</ta>
            <ta e="T115" id="Seg_2316" s="T114">pro.h:O</ta>
            <ta e="T116" id="Seg_2317" s="T115">v:pred s:purp</ta>
            <ta e="T119" id="Seg_2318" s="T118">np:S</ta>
            <ta e="T120" id="Seg_2319" s="T119">adj:pred</ta>
            <ta e="T122" id="Seg_2320" s="T121">pro.h:O</ta>
            <ta e="T123" id="Seg_2321" s="T122">v:pred s:purp</ta>
            <ta e="T127" id="Seg_2322" s="T126">np:S</ta>
            <ta e="T128" id="Seg_2323" s="T127">adj:pred</ta>
            <ta e="T130" id="Seg_2324" s="T129">pro.h:O</ta>
            <ta e="T131" id="Seg_2325" s="T130">v:pred s:purp</ta>
            <ta e="T133" id="Seg_2326" s="T132">conv:pred</ta>
            <ta e="T134" id="Seg_2327" s="T133">v:pred 0.3.h:S</ta>
            <ta e="T135" id="Seg_2328" s="T134">v:pred 0.3.h:S</ta>
            <ta e="T136" id="Seg_2329" s="T135">np:O</ta>
            <ta e="T137" id="Seg_2330" s="T136">v:pred 0.3.h:S</ta>
            <ta e="T139" id="Seg_2331" s="T138">np:O</ta>
            <ta e="T142" id="Seg_2332" s="T141">v:pred 0.3.h:S</ta>
            <ta e="T144" id="Seg_2333" s="T143">v:pred 0.3.h:S</ta>
            <ta e="T146" id="Seg_2334" s="T145">np.h:S</ta>
            <ta e="T147" id="Seg_2335" s="T146">v:pred</ta>
            <ta e="T149" id="Seg_2336" s="T148">pro.h:S</ta>
            <ta e="T151" id="Seg_2337" s="T150">v:pred</ta>
            <ta e="T154" id="Seg_2338" s="T153">v:pred 0.3.h:S</ta>
            <ta e="T157" id="Seg_2339" s="T156">np:O</ta>
            <ta e="T159" id="Seg_2340" s="T158">v:pred 0.3.h:S</ta>
            <ta e="T160" id="Seg_2341" s="T159">pro.h:S</ta>
            <ta e="T161" id="Seg_2342" s="T160">np.h:S</ta>
            <ta e="T163" id="Seg_2343" s="T162">np.h:S</ta>
            <ta e="T164" id="Seg_2344" s="T163">v:pred</ta>
            <ta e="T167" id="Seg_2345" s="T166">np.h:O</ta>
            <ta e="T169" id="Seg_2346" s="T168">conv:pred</ta>
            <ta e="T170" id="Seg_2347" s="T169">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T3" id="Seg_2348" s="T2">RUS:gram</ta>
            <ta e="T18" id="Seg_2349" s="T17">RUS:cult</ta>
            <ta e="T22" id="Seg_2350" s="T21">RUS:gram</ta>
            <ta e="T26" id="Seg_2351" s="T25">RUS:core</ta>
            <ta e="T31" id="Seg_2352" s="T30">RUS:core</ta>
            <ta e="T37" id="Seg_2353" s="T36">RUS:gram</ta>
            <ta e="T45" id="Seg_2354" s="T44">RUS:core</ta>
            <ta e="T54" id="Seg_2355" s="T53">RUS:core</ta>
            <ta e="T62" id="Seg_2356" s="T61">RUS:gram</ta>
            <ta e="T70" id="Seg_2357" s="T69">RUS:gram</ta>
            <ta e="T89" id="Seg_2358" s="T88">RUS:cult</ta>
            <ta e="T90" id="Seg_2359" s="T89">RUS:cult</ta>
            <ta e="T94" id="Seg_2360" s="T93">RUS:disc</ta>
            <ta e="T107" id="Seg_2361" s="T106">RUS:gram</ta>
            <ta e="T110" id="Seg_2362" s="T109">RUS:gram</ta>
            <ta e="T114" id="Seg_2363" s="T113">RUS:gram</ta>
            <ta e="T117" id="Seg_2364" s="T116">RUS:gram</ta>
            <ta e="T121" id="Seg_2365" s="T120">RUS:gram</ta>
            <ta e="T124" id="Seg_2366" s="T123">RUS:gram</ta>
            <ta e="T129" id="Seg_2367" s="T128">RUS:gram</ta>
            <ta e="T138" id="Seg_2368" s="T137">RUS:gram</ta>
            <ta e="T139" id="Seg_2369" s="T138">RUS:cult</ta>
            <ta e="T140" id="Seg_2370" s="T139">RUS:gram</ta>
            <ta e="T143" id="Seg_2371" s="T142">RUS:gram</ta>
            <ta e="T148" id="Seg_2372" s="T147">TURK:cult</ta>
            <ta e="T150" id="Seg_2373" s="T149">TURK:disc</ta>
            <ta e="T155" id="Seg_2374" s="T154">RUS:gram</ta>
            <ta e="T162" id="Seg_2375" s="T161">RUS:gram</ta>
            <ta e="T166" id="Seg_2376" s="T165">RUS:gram</ta>
            <ta e="T167" id="Seg_2377" s="T166">RUS:core</ta>
            <ta e="T168" id="Seg_2378" s="T167">TURK:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_2379" s="T0">Жили мама и дочка, Красная Шапочка.</ta>
            <ta e="T10" id="Seg_2380" s="T6">Потом бабушка заболела.</ta>
            <ta e="T17" id="Seg_2381" s="T10">Мать говорит: "Иди повидай бабушку, она болеет".</ta>
            <ta e="T19" id="Seg_2382" s="T17">Она напекла пирогов.</ta>
            <ta e="T23" id="Seg_2383" s="T19">Положила масло [в корзину?] и пошла.</ta>
            <ta e="T28" id="Seg_2384" s="T23">Собирала ягоды, срывала цветы.</ta>
            <ta e="T31" id="Seg_2385" s="T28">Приходит к ней волк.</ta>
            <ta e="T33" id="Seg_2386" s="T31">"Куда ты идешь?"</ta>
            <ta e="T36" id="Seg_2387" s="T33">"К бабушке, она больна".</ta>
            <ta e="T40" id="Seg_2388" s="T36">"А где она живет?"</ta>
            <ta e="T43" id="Seg_2389" s="T40">"Там, недалеко".</ta>
            <ta e="T46" id="Seg_2390" s="T43">Тогда волк побежал.</ta>
            <ta e="T47" id="Seg_2391" s="T46">Постучал [в дверь].</ta>
            <ta e="T51" id="Seg_2392" s="T47">Женщина говорит: "Кто там?"</ta>
            <ta e="T59" id="Seg_2393" s="T51">"Я твоя внучка, Красная Шапочка, я пришла тебя проведать".</ta>
            <ta e="T63" id="Seg_2394" s="T59">"Развяжи дверь [=потяни за веревочку], и зайдешь".</ta>
            <ta e="T66" id="Seg_2395" s="T63">Тогда он вошел.</ta>
            <ta e="T72" id="Seg_2396" s="T66">Съел эту женщину и лег сам.</ta>
            <ta e="T75" id="Seg_2397" s="T72">Надел шапку женщины.</ta>
            <ta e="T80" id="Seg_2398" s="T75">Потом пришла девочка, Красная Шапочка.</ta>
            <ta e="T82" id="Seg_2399" s="T80">Стучится в дверь.</ta>
            <ta e="T83" id="Seg_2400" s="T82">"Открой!"</ta>
            <ta e="T87" id="Seg_2401" s="T83">Она открыла, вошла.</ta>
            <ta e="T91" id="Seg_2402" s="T87">"Положи пирог и масло на стол!"</ta>
            <ta e="T93" id="Seg_2403" s="T91">Она положила.</ta>
            <ta e="T99" id="Seg_2404" s="T93">"Ну, иди сюда, садись со мной рядом!"</ta>
            <ta e="T101" id="Seg_2405" s="T99">Она сидит.</ta>
            <ta e="T106" id="Seg_2406" s="T101">"Почему у тебя такие большие уши!"</ta>
            <ta e="T109" id="Seg_2407" s="T106">"Чтобы тебя слушать".</ta>
            <ta e="T113" id="Seg_2408" s="T109">"А почему у тебя большие глаза?"</ta>
            <ta e="T116" id="Seg_2409" s="T113">"Чтобы на тебя смотреть!"</ta>
            <ta e="T120" id="Seg_2410" s="T116">"А почему у тебя длинные руки?"</ta>
            <ta e="T123" id="Seg_2411" s="T120">"Чтобы обнять тебя".</ta>
            <ta e="T128" id="Seg_2412" s="T123">"А почему у тебя большой рот?"</ta>
            <ta e="T131" id="Seg_2413" s="T128">"Чтобы съесть тебя!"</ta>
            <ta e="T135" id="Seg_2414" s="T131">Тогда он схватил ее и съел.</ta>
            <ta e="T144" id="Seg_2415" s="T135">Он съел масло и пирог, сам лег и заснул.</ta>
            <ta e="T148" id="Seg_2416" s="T144">Туда пришли люди с ружьем.</ta>
            <ta e="T152" id="Seg_2417" s="T148">Он крепко спит.</ta>
            <ta e="T154" id="Seg_2418" s="T152">Потом он вошли.</ta>
            <ta e="T159" id="Seg_2419" s="T154">И ударили его ножом в живот.</ta>
            <ta e="T165" id="Seg_2420" s="T159">Женщина и девочка вышли живые.</ta>
            <ta e="T170" id="Seg_2421" s="T165">А волка они зарезали.</ta>
            <ta e="T171" id="Seg_2422" s="T170">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_2423" s="T0">[Once] there lived a mother and a daughter, Little Red Cap.</ta>
            <ta e="T10" id="Seg_2424" s="T6">Then grandmother got ill.</ta>
            <ta e="T17" id="Seg_2425" s="T10">Her mother says: "Go, see [= visit] the grandmother, she is ill."</ta>
            <ta e="T19" id="Seg_2426" s="T17">She baked pies.</ta>
            <ta e="T23" id="Seg_2427" s="T19">She put butter [in the basket?] and went.</ta>
            <ta e="T28" id="Seg_2428" s="T23">She gathered berries, picked flowers.</ta>
            <ta e="T31" id="Seg_2429" s="T28">A wolf comes to her.</ta>
            <ta e="T33" id="Seg_2430" s="T31">"Where are you going?"</ta>
            <ta e="T36" id="Seg_2431" s="T33">"To my grandmother, she is ill."</ta>
            <ta e="T40" id="Seg_2432" s="T36">"And where does she live?"</ta>
            <ta e="T43" id="Seg_2433" s="T40">"There, not far."</ta>
            <ta e="T46" id="Seg_2434" s="T43">Then the wolf ran.</ta>
            <ta e="T47" id="Seg_2435" s="T46">He knocked [on the door].</ta>
            <ta e="T51" id="Seg_2436" s="T47">The woman says: "Who is there?"</ta>
            <ta e="T59" id="Seg_2437" s="T51">"I am your granddaughter, Red Cap, I came to see you."</ta>
            <ta e="T63" id="Seg_2438" s="T59">"Untie the door [=pull the rope] and you will come."</ta>
            <ta e="T66" id="Seg_2439" s="T63">Then he came in.</ta>
            <ta e="T72" id="Seg_2440" s="T66">He ate the woman and himself lay down.</ta>
            <ta e="T75" id="Seg_2441" s="T72">He put on the woman's hat.</ta>
            <ta e="T80" id="Seg_2442" s="T75">Then the girl comes, Red Cap.</ta>
            <ta e="T82" id="Seg_2443" s="T80">She knocks on the door.</ta>
            <ta e="T83" id="Seg_2444" s="T82">"Open it!"</ta>
            <ta e="T87" id="Seg_2445" s="T83">She opened, she came in.</ta>
            <ta e="T91" id="Seg_2446" s="T87">"Put the pie, butter on the table!"</ta>
            <ta e="T93" id="Seg_2447" s="T91">She put.</ta>
            <ta e="T99" id="Seg_2448" s="T93">"Well, come sit by my side!"</ta>
            <ta e="T101" id="Seg_2449" s="T99">She is sitting.</ta>
            <ta e="T106" id="Seg_2450" s="T101">"Why are your ears so big?"</ta>
            <ta e="T109" id="Seg_2451" s="T106">"To listen to you."</ta>
            <ta e="T113" id="Seg_2452" s="T109">"And why are your eyes big?"</ta>
            <ta e="T116" id="Seg_2453" s="T113">"To look at you!"</ta>
            <ta e="T120" id="Seg_2454" s="T116">"And why are your hands long?"</ta>
            <ta e="T123" id="Seg_2455" s="T120">"To embrace you."</ta>
            <ta e="T128" id="Seg_2456" s="T123">"And why is your mouth big?"</ta>
            <ta e="T131" id="Seg_2457" s="T128">"To eat you!"</ta>
            <ta e="T135" id="Seg_2458" s="T131">Then he grabbed her and ate.</ta>
            <ta e="T144" id="Seg_2459" s="T135">He ate the butter and the pie, and himself lay down and fell asleep.</ta>
            <ta e="T148" id="Seg_2460" s="T144">People came there with a gun.</ta>
            <ta e="T152" id="Seg_2461" s="T148">He is fast asleep.</ta>
            <ta e="T154" id="Seg_2462" s="T152">Then they came in.</ta>
            <ta e="T159" id="Seg_2463" s="T154">They stabbed its belly with a knife.</ta>
            <ta e="T165" id="Seg_2464" s="T159">They, the woman and the girl came out, alive.</ta>
            <ta e="T170" id="Seg_2465" s="T165">And the wolf, they stabbed it to death.</ta>
            <ta e="T171" id="Seg_2466" s="T170">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_2467" s="T0">Es lebten [einmal] eine Mutter und eine Tochter, Rotkäppchen.</ta>
            <ta e="T10" id="Seg_2468" s="T6">Dann wurde die Großmutter krank.</ta>
            <ta e="T17" id="Seg_2469" s="T10">Ihre Mutter sagt: "Geh, sieh [= besuch] die Großmutter, sie ist krank."</ta>
            <ta e="T19" id="Seg_2470" s="T17">Sie buk Piroggen.</ta>
            <ta e="T23" id="Seg_2471" s="T19">Sie tat Butter [in den Korb?] und ging.</ta>
            <ta e="T28" id="Seg_2472" s="T23">Sie sammelte Beeren, pflückte Blumen.</ta>
            <ta e="T31" id="Seg_2473" s="T28">Ein Wolf kommt zu ihr.</ta>
            <ta e="T33" id="Seg_2474" s="T31">"Wohin gehst du?"</ta>
            <ta e="T36" id="Seg_2475" s="T33">"Zu meiner Großmutter, sie ist krank."</ta>
            <ta e="T40" id="Seg_2476" s="T36">"Und wo wohnt sie?"</ta>
            <ta e="T43" id="Seg_2477" s="T40">"Dort, nicht weit."</ta>
            <ta e="T46" id="Seg_2478" s="T43">Dann rannte der Wolf.</ta>
            <ta e="T47" id="Seg_2479" s="T46">Er klopfte [an der Tür].</ta>
            <ta e="T51" id="Seg_2480" s="T47">Die Frau sagt: "Wer ist da?"</ta>
            <ta e="T59" id="Seg_2481" s="T51">"Ich bin deine Enkelin, Rotkäppchen, ich bin gekommen, um dich zu sehen."</ta>
            <ta e="T63" id="Seg_2482" s="T59">"Binde die Tür los [=ziehe den Strick] und du wirst kommen."</ta>
            <ta e="T66" id="Seg_2483" s="T63">Dann kam er ein.</ta>
            <ta e="T72" id="Seg_2484" s="T66">Er fraß die Frau und legte sich selbst hin.</ta>
            <ta e="T75" id="Seg_2485" s="T72">Er hatte den Hut der Frau auf.</ta>
            <ta e="T80" id="Seg_2486" s="T75">Dann kommt das Mädchen, Rotkäppchen.</ta>
            <ta e="T82" id="Seg_2487" s="T80">Sie klopft an der Tür.</ta>
            <ta e="T83" id="Seg_2488" s="T82">"Öffne sie!"</ta>
            <ta e="T87" id="Seg_2489" s="T83">Sie öffnete, sie kam hinein.</ta>
            <ta e="T91" id="Seg_2490" s="T87">"Leg die Pirogge, die Butter auf den Tisch!"</ta>
            <ta e="T93" id="Seg_2491" s="T91">Sie legte [es hin].</ta>
            <ta e="T99" id="Seg_2492" s="T93">"Nun, komm und setz dich zu mir!"</ta>
            <ta e="T101" id="Seg_2493" s="T99">Sie sitzt.</ta>
            <ta e="T106" id="Seg_2494" s="T101">"Warum sind deine Ohren so groß?"</ta>
            <ta e="T109" id="Seg_2495" s="T106">"Um dir zuzuhören."</ta>
            <ta e="T113" id="Seg_2496" s="T109">"Und warum sind deine Augen so groß?"</ta>
            <ta e="T116" id="Seg_2497" s="T113">"Um dich anzusehen!"</ta>
            <ta e="T120" id="Seg_2498" s="T116">"Und warum sind deine Hände so lang?"</ta>
            <ta e="T123" id="Seg_2499" s="T120">"Um dich zu umarmen."</ta>
            <ta e="T128" id="Seg_2500" s="T123">"Und warum ist dein Mund so groß?"</ta>
            <ta e="T131" id="Seg_2501" s="T128">"Um dich zu fressen!"</ta>
            <ta e="T135" id="Seg_2502" s="T131">Und er griff sie und fraß sie.</ta>
            <ta e="T144" id="Seg_2503" s="T135">Er fraß die Butter und die Pirogge, er selbst legte sich hin und schlief ein.</ta>
            <ta e="T148" id="Seg_2504" s="T144">Leute kamen dorthin mit einem Gewehr.</ta>
            <ta e="T152" id="Seg_2505" s="T148">Er schläft fest.</ta>
            <ta e="T154" id="Seg_2506" s="T152">Dann kamen sie hinein.</ta>
            <ta e="T159" id="Seg_2507" s="T154">Sie stachen mit einem Messer in seinen Bauch.</ta>
            <ta e="T165" id="Seg_2508" s="T159">Sie, die Frau und das Mädchen, kamen heraus, lebendig.</ta>
            <ta e="T170" id="Seg_2509" s="T165">Und der Wolf, sie erstachen ihn.</ta>
            <ta e="T171" id="Seg_2510" s="T170">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T63" id="Seg_2511" s="T59">[GVY:] Cf. kedər- 'untie, unbind' (D29b).</ta>
            <ta e="T123" id="Seg_2512" s="T120">[GVY:] kamnərosʼtə = kamrosʼtə?</ta>
            <ta e="T144" id="Seg_2513" s="T135">[GVY:] literally "was lying and sleeping"?</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
