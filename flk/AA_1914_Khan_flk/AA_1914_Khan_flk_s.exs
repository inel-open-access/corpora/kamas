<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDE0944454-7438-5821-C5BE-87AEF2AD19A6">
   <head>
      <meta-information>
         <project-name>Kamas</project-name>
         <transcription-name>AA_1914_Khan_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\AA_1914_Khan_flk\AA_1914_Khan_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">367</ud-information>
            <ud-information attribute-name="# HIAT:w">250</ud-information>
            <ud-information attribute-name="# e">247</ud-information>
            <ud-information attribute-name="# HIAT:u">56</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="AA">
            <abbreviation>AA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
         <tli id="T135" time="71.5" type="appl" />
         <tli id="T136" time="72.0" type="appl" />
         <tli id="T137" time="72.5" type="appl" />
         <tli id="T138" time="73.0" type="appl" />
         <tli id="T139" time="73.5" type="appl" />
         <tli id="T140" time="74.0" type="appl" />
         <tli id="T141" time="74.5" type="appl" />
         <tli id="T142" time="75.0" type="appl" />
         <tli id="T143" time="75.5" type="appl" />
         <tli id="T144" time="76.0" type="appl" />
         <tli id="T145" time="76.5" type="appl" />
         <tli id="T146" time="77.0" type="appl" />
         <tli id="T147" time="77.5" type="appl" />
         <tli id="T148" time="78.0" type="appl" />
         <tli id="T149" time="78.5" type="appl" />
         <tli id="T150" time="79.0" type="appl" />
         <tli id="T151" time="79.5" type="appl" />
         <tli id="T152" time="80.0" type="appl" />
         <tli id="T153" time="80.5" type="appl" />
         <tli id="T154" time="81.0" type="appl" />
         <tli id="T155" time="81.5" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.5" type="appl" />
         <tli id="T158" time="83.0" type="appl" />
         <tli id="T159" time="83.5" type="appl" />
         <tli id="T160" time="84.0" type="appl" />
         <tli id="T161" time="84.5" type="appl" />
         <tli id="T162" time="85.0" type="appl" />
         <tli id="T163" time="85.5" type="appl" />
         <tli id="T164" time="86.0" type="appl" />
         <tli id="T165" time="86.5" type="appl" />
         <tli id="T166" time="87.0" type="appl" />
         <tli id="T167" time="87.5" type="appl" />
         <tli id="T168" time="88.0" type="appl" />
         <tli id="T169" time="88.5" type="appl" />
         <tli id="T170" time="89.0" type="appl" />
         <tli id="T171" time="89.5" type="appl" />
         <tli id="T172" time="90.0" type="appl" />
         <tli id="T173" time="90.5" type="appl" />
         <tli id="T174" time="91.0" type="appl" />
         <tli id="T175" time="91.5" type="appl" />
         <tli id="T176" time="92.0" type="appl" />
         <tli id="T177" time="92.5" type="appl" />
         <tli id="T178" time="93.0" type="appl" />
         <tli id="T179" time="93.5" type="appl" />
         <tli id="T180" time="94.0" type="appl" />
         <tli id="T181" time="94.5" type="appl" />
         <tli id="T182" time="95.0" type="appl" />
         <tli id="T183" time="95.5" type="appl" />
         <tli id="T184" time="96.0" type="appl" />
         <tli id="T185" time="96.5" type="appl" />
         <tli id="T186" time="97.0" type="appl" />
         <tli id="T187" time="97.5" type="appl" />
         <tli id="T188" time="98.0" type="appl" />
         <tli id="T189" time="98.5" type="appl" />
         <tli id="T190" time="99.0" type="appl" />
         <tli id="T191" time="99.5" type="appl" />
         <tli id="T192" time="100.0" type="appl" />
         <tli id="T193" time="100.5" type="appl" />
         <tli id="T194" time="101.0" type="appl" />
         <tli id="T195" time="101.5" type="appl" />
         <tli id="T196" time="102.0" type="appl" />
         <tli id="T197" time="102.5" type="appl" />
         <tli id="T198" time="103.0" type="appl" />
         <tli id="T199" time="103.5" type="appl" />
         <tli id="T200" time="104.0" type="appl" />
         <tli id="T201" time="104.5" type="appl" />
         <tli id="T202" time="105.0" type="appl" />
         <tli id="T203" time="105.5" type="appl" />
         <tli id="T204" time="106.0" type="appl" />
         <tli id="T205" time="106.5" type="appl" />
         <tli id="T206" time="107.0" type="appl" />
         <tli id="T207" time="107.5" type="appl" />
         <tli id="T208" time="108.0" type="appl" />
         <tli id="T209" time="108.5" type="appl" />
         <tli id="T210" time="109.0" type="appl" />
         <tli id="T211" time="109.5" type="appl" />
         <tli id="T212" time="110.0" type="appl" />
         <tli id="T213" time="110.5" type="appl" />
         <tli id="T214" time="111.0" type="appl" />
         <tli id="T215" time="111.5" type="appl" />
         <tli id="T216" time="112.0" type="appl" />
         <tli id="T217" time="112.5" type="appl" />
         <tli id="T218" time="113.0" type="appl" />
         <tli id="T219" time="113.5" type="appl" />
         <tli id="T220" time="114.0" type="appl" />
         <tli id="T221" time="114.5" type="appl" />
         <tli id="T222" time="115.0" type="appl" />
         <tli id="T223" time="115.5" type="appl" />
         <tli id="T224" time="116.0" type="appl" />
         <tli id="T225" time="116.5" type="appl" />
         <tli id="T226" time="117.0" type="appl" />
         <tli id="T227" time="117.5" type="appl" />
         <tli id="T228" time="118.0" type="appl" />
         <tli id="T229" time="118.5" type="appl" />
         <tli id="T230" time="119.0" type="appl" />
         <tli id="T231" time="119.5" type="appl" />
         <tli id="T232" time="120.0" type="appl" />
         <tli id="T233" time="120.5" type="appl" />
         <tli id="T234" time="121.0" type="appl" />
         <tli id="T235" time="121.5" type="appl" />
         <tli id="T236" time="122.0" type="appl" />
         <tli id="T237" time="122.5" type="appl" />
         <tli id="T238" time="123.0" type="appl" />
         <tli id="T239" time="123.5" type="appl" />
         <tli id="T240" time="124.0" type="appl" />
         <tli id="T241" time="124.5" type="appl" />
         <tli id="T242" time="125.0" type="appl" />
         <tli id="T243" time="125.5" type="appl" />
         <tli id="T244" time="126.0" type="appl" />
         <tli id="T245" time="126.5" type="appl" />
         <tli id="T246" time="127.0" type="appl" />
         <tli id="T247" time="127.5" type="appl" />
         <tli id="T248" time="128.0" type="appl" />
         <tli id="T249" time="128.5" type="appl" />
         <tli id="T250" time="129.0" type="appl" />
         <tli id="T251" time="129.5" type="appl" />
         <tli id="T252" time="130.0" type="appl" />
         <tli id="T253" time="130.5" type="appl" />
         <tli id="T254" time="131.0" type="appl" />
         <tli id="T255" time="131.5" type="appl" />
         <tli id="T256" time="132.0" type="appl" />
         <tli id="T257" time="132.5" type="appl" />
         <tli id="T258" time="133.0" type="appl" />
         <tli id="T259" time="133.5" type="appl" />
         <tli id="T260" time="134.0" type="appl" />
         <tli id="T261" time="134.5" type="appl" />
         <tli id="T262" time="135.0" type="appl" />
         <tli id="T263" time="135.5" type="appl" />
         <tli id="T264" time="136.0" type="appl" />
         <tli id="T265" time="136.5" type="appl" />
         <tli id="T266" time="137.0" type="appl" />
         <tli id="T267" time="137.5" type="appl" />
         <tli id="T268" time="138.0" type="appl" />
         <tli id="T269" time="138.5" type="appl" />
         <tli id="T270" time="139.0" type="appl" />
         <tli id="T271" time="139.5" type="appl" />
         <tli id="T272" time="140.0" type="appl" />
         <tli id="T273" time="140.5" type="appl" />
         <tli id="T274" time="141.0" type="appl" />
         <tli id="T275" time="141.5" type="appl" />
         <tli id="T276" time="142.0" type="appl" />
         <tli id="T277" time="142.5" type="appl" />
         <tli id="T278" time="143.0" type="appl" />
         <tli id="T279" time="143.5" type="appl" />
         <tli id="T280" time="144.0" type="appl" />
         <tli id="T281" time="144.5" type="appl" />
         <tli id="T282" time="145.0" type="appl" />
         <tli id="T283" time="145.5" type="appl" />
         <tli id="T284" time="146.0" type="appl" />
         <tli id="T285" time="146.5" type="appl" />
         <tli id="T286" time="147.0" type="appl" />
         <tli id="T287" time="147.5" type="appl" />
         <tli id="T288" time="148.0" type="appl" />
         <tli id="T289" time="148.5" type="appl" />
         <tli id="T290" time="149.0" type="appl" />
         <tli id="T291" time="149.5" type="appl" />
         <tli id="T292" time="150.0" type="appl" />
         <tli id="T293" time="150.5" type="appl" />
         <tli id="T294" time="151.0" type="appl" />
         <tli id="T295" time="151.5" type="appl" />
         <tli id="T296" time="152.0" type="appl" />
         <tli id="T297" time="152.5" type="appl" />
         <tli id="T298" time="153.0" type="appl" />
         <tli id="T299" time="153.5" type="appl" />
         <tli id="T300" time="154.0" type="appl" />
         <tli id="T301" time="154.5" type="appl" />
         <tli id="T302" time="155.0" type="appl" />
         <tli id="T303" time="155.5" type="appl" />
         <tli id="T304" time="156.0" type="appl" />
         <tli id="T305" time="156.5" type="appl" />
         <tli id="T306" time="157.0" type="appl" />
         <tli id="T307" time="157.5" type="appl" />
         <tli id="T308" time="158.0" type="appl" />
         <tli id="T309" time="158.5" type="appl" />
         <tli id="T310" time="159.0" type="appl" />
         <tli id="T311" time="159.5" type="appl" />
         <tli id="T312" time="160.0" type="appl" />
         <tli id="T313" time="160.5" type="appl" />
         <tli id="T314" time="161.0" type="appl" />
         <tli id="T315" time="161.5" type="appl" />
         <tli id="T316" time="162.0" type="appl" />
         <tli id="T317" time="162.5" type="appl" />
         <tli id="T318" time="163.0" type="appl" />
         <tli id="T319" time="163.5" type="appl" />
         <tli id="T320" time="164.0" type="appl" />
         <tli id="T321" time="164.5" type="appl" />
         <tli id="T322" time="165.0" type="appl" />
         <tli id="T323" time="165.5" type="appl" />
         <tli id="T324" time="166.0" type="appl" />
         <tli id="T325" time="166.5" type="appl" />
         <tli id="T326" time="167.0" type="appl" />
         <tli id="T327" time="167.5" type="appl" />
         <tli id="T328" time="168.0" type="appl" />
         <tli id="T329" time="168.5" type="appl" />
         <tli id="T330" time="169.0" type="appl" />
         <tli id="T331" time="169.5" type="appl" />
         <tli id="T332" time="170.0" type="appl" />
         <tli id="T333" time="170.5" type="appl" />
         <tli id="T334" time="171.0" type="appl" />
         <tli id="T335" time="171.5" type="appl" />
         <tli id="T336" time="172.0" type="appl" />
         <tli id="T337" time="172.5" type="appl" />
         <tli id="T338" time="173.0" type="appl" />
         <tli id="T339" time="173.5" type="appl" />
         <tli id="T340" time="174.0" type="appl" />
         <tli id="T341" time="174.5" type="appl" />
         <tli id="T342" time="175.0" type="appl" />
         <tli id="T343" time="175.5" type="appl" />
         <tli id="T344" time="176.0" type="appl" />
         <tli id="T345" time="176.5" type="appl" />
         <tli id="T346" time="177.0" type="appl" />
         <tli id="T347" time="177.5" type="appl" />
         <tli id="T348" time="178.0" type="appl" />
         <tli id="T349" time="178.5" type="appl" />
         <tli id="T350" time="179.0" type="appl" />
         <tli id="T351" time="179.5" type="appl" />
         <tli id="T352" time="180.0" type="appl" />
         <tli id="T353" time="180.5" type="appl" />
         <tli id="T354" time="181.0" type="appl" />
         <tli id="T355" time="181.5" type="appl" />
         <tli id="T356" time="182.0" type="appl" />
         <tli id="T357" time="182.5" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="AA"
                      type="t">
         <timeline-fork end="T115" start="T114">
            <tli id="T114.tx.1" />
         </timeline-fork>
         <timeline-fork end="T318" start="T317">
            <tli id="T317.tx.1" />
         </timeline-fork>
         <timeline-fork end="T319" start="T318">
            <tli id="T318.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T357" id="Seg_0" n="sc" s="T110">
               <ts e="T113" id="Seg_2" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_4" n="HIAT:w" s="T110">Bile</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_7" n="HIAT:w" s="T111">kuza</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_10" n="HIAT:w" s="T112">amnobi</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_14" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_16" n="HIAT:w" s="T113">Nĭmdə</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114.tx.1" id="Seg_19" n="HIAT:w" s="T114">tardʼža</ts>
                  <nts id="Seg_20" n="HIAT:ip">_</nts>
                  <ts e="T115" id="Seg_22" n="HIAT:w" s="T114.tx.1">bərdʼža</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_26" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_28" n="HIAT:w" s="T115">Bile</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_31" n="HIAT:w" s="T116">sejmübə</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_34" n="HIAT:w" s="T117">dʼappi</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T121" id="Seg_38" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_40" n="HIAT:w" s="T118">Aksaʔ</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_43" n="HIAT:w" s="T119">ibi</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_46" n="HIAT:w" s="T120">sejmüt</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_50" n="HIAT:u" s="T121">
                  <ts e="T122" id="Seg_52" n="HIAT:w" s="T121">Konzandəbi</ts>
                  <nts id="Seg_53" n="HIAT:ip">,</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_56" n="HIAT:w" s="T122">sagər</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_59" n="HIAT:w" s="T123">bărabə</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_62" n="HIAT:w" s="T124">sarbi</ts>
                  <nts id="Seg_63" n="HIAT:ip">,</nts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_66" n="HIAT:w" s="T125">šoj</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_69" n="HIAT:w" s="T126">tʼobə</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_72" n="HIAT:w" s="T127">sarbi</ts>
                  <nts id="Seg_73" n="HIAT:ip">.</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_76" n="HIAT:u" s="T128">
                  <ts e="T129" id="Seg_78" n="HIAT:w" s="T128">Mĭlloʔbdəbi</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_80" n="HIAT:ip">(</nts>
                  <ts e="T130" id="Seg_82" n="HIAT:w" s="T129">mĭllüʔbi</ts>
                  <nts id="Seg_83" n="HIAT:ip">)</nts>
                  <nts id="Seg_84" n="HIAT:ip">.</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T132" id="Seg_87" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_89" n="HIAT:w" s="T130">Urgaːba</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_92" n="HIAT:w" s="T131">toʔbdobi</ts>
                  <nts id="Seg_93" n="HIAT:ip">.</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T135" id="Seg_96" n="HIAT:u" s="T132">
                  <nts id="Seg_97" n="HIAT:ip">"</nts>
                  <ts e="T133" id="Seg_99" n="HIAT:w" s="T132">Măna</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_102" n="HIAT:w" s="T133">it</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_105" n="HIAT:w" s="T134">helaːzʼəttə</ts>
                  <nts id="Seg_106" n="HIAT:ip">!</nts>
                  <nts id="Seg_107" n="HIAT:ip">"</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T138" id="Seg_110" n="HIAT:u" s="T135">
                  <nts id="Seg_111" n="HIAT:ip">"</nts>
                  <ts e="T136" id="Seg_113" n="HIAT:w" s="T135">Pădaʔ</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_116" n="HIAT:w" s="T136">sagər</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_119" n="HIAT:w" s="T137">băranə</ts>
                  <nts id="Seg_120" n="HIAT:ip">!</nts>
                  <nts id="Seg_121" n="HIAT:ip">"</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_124" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_126" n="HIAT:w" s="T138">Mĭlloʔbdəbi</ts>
                  <nts id="Seg_127" n="HIAT:ip">.</nts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T141" id="Seg_130" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_132" n="HIAT:w" s="T139">Mʼeŋgej</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_135" n="HIAT:w" s="T140">toʔbdobi</ts>
                  <nts id="Seg_136" n="HIAT:ip">.</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_139" n="HIAT:u" s="T141">
                  <nts id="Seg_140" n="HIAT:ip">"</nts>
                  <ts e="T142" id="Seg_142" n="HIAT:w" s="T141">Măna</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_145" n="HIAT:w" s="T142">it</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_148" n="HIAT:w" s="T143">helaːzʼəttə</ts>
                  <nts id="Seg_149" n="HIAT:ip">!</nts>
                  <nts id="Seg_150" n="HIAT:ip">"</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T147" id="Seg_153" n="HIAT:u" s="T144">
                  <nts id="Seg_154" n="HIAT:ip">"</nts>
                  <ts e="T145" id="Seg_156" n="HIAT:w" s="T144">Pădaʔ</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_159" n="HIAT:w" s="T145">sagər</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_162" n="HIAT:w" s="T146">băranə</ts>
                  <nts id="Seg_163" n="HIAT:ip">!</nts>
                  <nts id="Seg_164" n="HIAT:ip">"</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T150" id="Seg_167" n="HIAT:u" s="T147">
                  <ts e="T148" id="Seg_169" n="HIAT:w" s="T147">Păʔdəbi</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_172" n="HIAT:w" s="T148">sagər</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_175" n="HIAT:w" s="T149">băranə</ts>
                  <nts id="Seg_176" n="HIAT:ip">.</nts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_179" n="HIAT:u" s="T150">
                  <ts e="T151" id="Seg_181" n="HIAT:w" s="T150">Dĭgəttə</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_184" n="HIAT:w" s="T151">kuš</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_187" n="HIAT:w" s="T152">toʔbdobi</ts>
                  <nts id="Seg_188" n="HIAT:ip">.</nts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T155" id="Seg_191" n="HIAT:u" s="T153">
                  <nts id="Seg_192" n="HIAT:ip">"</nts>
                  <ts e="T154" id="Seg_194" n="HIAT:w" s="T153">Iʔ</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_197" n="HIAT:w" s="T154">helaːzʼət</ts>
                  <nts id="Seg_198" n="HIAT:ip">!</nts>
                  <nts id="Seg_199" n="HIAT:ip">"</nts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T158" id="Seg_202" n="HIAT:u" s="T155">
                  <nts id="Seg_203" n="HIAT:ip">"</nts>
                  <ts e="T156" id="Seg_205" n="HIAT:w" s="T155">Pădaʔ</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_208" n="HIAT:w" s="T156">sagər</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_211" n="HIAT:w" s="T157">băranə</ts>
                  <nts id="Seg_212" n="HIAT:ip">!</nts>
                  <nts id="Seg_213" n="HIAT:ip">"</nts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_216" n="HIAT:u" s="T158">
                  <ts e="T159" id="Seg_218" n="HIAT:w" s="T158">Dĭgəttə</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_221" n="HIAT:w" s="T159">bü</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_224" n="HIAT:w" s="T160">toʔbdobi</ts>
                  <nts id="Seg_225" n="HIAT:ip">.</nts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T173" id="Seg_228" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_230" n="HIAT:w" s="T161">Dĭ</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_233" n="HIAT:w" s="T162">bün</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_236" n="HIAT:w" s="T163">suʔlaʔ</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_239" n="HIAT:w" s="T164">ibi</ts>
                  <nts id="Seg_240" n="HIAT:ip">,</nts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_243" n="HIAT:w" s="T165">sagər</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_246" n="HIAT:w" s="T166">bărandə</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_249" n="HIAT:w" s="T167">šoj</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_252" n="HIAT:w" s="T168">tʼoziʔ</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_255" n="HIAT:w" s="T169">suʔbdəbi</ts>
                  <nts id="Seg_256" n="HIAT:ip">,</nts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_259" n="HIAT:w" s="T170">šüjməndə</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_262" n="HIAT:w" s="T171">sarlaʔ</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_265" n="HIAT:w" s="T172">baʔbi</ts>
                  <nts id="Seg_266" n="HIAT:ip">.</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T178" id="Seg_269" n="HIAT:u" s="T173">
                  <ts e="T174" id="Seg_271" n="HIAT:w" s="T173">Kubində</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_274" n="HIAT:w" s="T174">talaində</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_277" n="HIAT:w" s="T175">toʔəndə</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_280" n="HIAT:w" s="T176">jada</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_283" n="HIAT:w" s="T177">nuga</ts>
                  <nts id="Seg_284" n="HIAT:ip">.</nts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T187" id="Seg_287" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_289" n="HIAT:w" s="T178">Dĭ</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_292" n="HIAT:w" s="T179">jadanə</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_295" n="HIAT:w" s="T180">ej</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_298" n="HIAT:w" s="T181">tuga</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_301" n="HIAT:w" s="T182">üzəbi</ts>
                  <nts id="Seg_302" n="HIAT:ip">,</nts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_305" n="HIAT:w" s="T183">šü</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_308" n="HIAT:w" s="T184">embi</ts>
                  <nts id="Seg_309" n="HIAT:ip">,</nts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_312" n="HIAT:w" s="T185">šüjməbə</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_315" n="HIAT:w" s="T186">ötʼtʼerbi</ts>
                  <nts id="Seg_316" n="HIAT:ip">.</nts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_319" n="HIAT:u" s="T187">
                  <ts e="T188" id="Seg_321" n="HIAT:w" s="T187">Amna</ts>
                  <nts id="Seg_322" n="HIAT:ip">.</nts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T195" id="Seg_325" n="HIAT:u" s="T188">
                  <ts e="T189" id="Seg_327" n="HIAT:w" s="T188">Kaːn</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_330" n="HIAT:w" s="T189">nʼiʔnen</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_333" n="HIAT:w" s="T190">mĭlleʔbi</ts>
                  <nts id="Seg_334" n="HIAT:ip">,</nts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_337" n="HIAT:w" s="T191">kulabaʔbi:</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_339" n="HIAT:ip">"</nts>
                  <ts e="T193" id="Seg_341" n="HIAT:w" s="T192">Šində</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_344" n="HIAT:w" s="T193">dĭgən</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_347" n="HIAT:w" s="T194">amna</ts>
                  <nts id="Seg_348" n="HIAT:ip">?</nts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T203" id="Seg_351" n="HIAT:u" s="T195">
                  <ts e="T196" id="Seg_353" n="HIAT:w" s="T195">Inebə</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_356" n="HIAT:w" s="T196">ötʼtʼerlabaʔbi</ts>
                  <nts id="Seg_357" n="HIAT:ip">,</nts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_360" n="HIAT:w" s="T197">măn</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_363" n="HIAT:w" s="T198">kük</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_366" n="HIAT:w" s="T199">noʔbə</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_369" n="HIAT:w" s="T200">tonəllaʔbə</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_372" n="HIAT:w" s="T201">ej</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_375" n="HIAT:w" s="T202">surarga</ts>
                  <nts id="Seg_376" n="HIAT:ip">.</nts>
                  <nts id="Seg_377" n="HIAT:ip">"</nts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T207" id="Seg_380" n="HIAT:u" s="T203">
                  <ts e="T204" id="Seg_382" n="HIAT:w" s="T203">Toʔgəndə</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_385" n="HIAT:w" s="T204">mĭndə</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_388" n="HIAT:w" s="T205">nʼekəbə</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_391" n="HIAT:w" s="T206">öʔləʔbə</ts>
                  <nts id="Seg_392" n="HIAT:ip">.</nts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T209" id="Seg_395" n="HIAT:u" s="T207">
                  <nts id="Seg_396" n="HIAT:ip">"</nts>
                  <ts e="T208" id="Seg_398" n="HIAT:w" s="T207">Kallaʔ</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_401" n="HIAT:w" s="T208">surardə</ts>
                  <nts id="Seg_402" n="HIAT:ip">!</nts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T212" id="Seg_405" n="HIAT:u" s="T209">
                  <ts e="T210" id="Seg_407" n="HIAT:w" s="T209">Šində</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_410" n="HIAT:w" s="T210">dĭrgit</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_413" n="HIAT:w" s="T211">igel</ts>
                  <nts id="Seg_414" n="HIAT:ip">?</nts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T218" id="Seg_417" n="HIAT:u" s="T212">
                  <ts e="T213" id="Seg_419" n="HIAT:w" s="T212">Ej</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_422" n="HIAT:w" s="T213">surarga</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_425" n="HIAT:w" s="T214">kaːnəm</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_428" n="HIAT:w" s="T215">kaːnən</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_431" n="HIAT:w" s="T216">noʔbdə</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_434" n="HIAT:w" s="T217">tonnaʔbə</ts>
                  <nts id="Seg_435" n="HIAT:ip">.</nts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T221" id="Seg_438" n="HIAT:u" s="T218">
                  <ts e="T219" id="Seg_440" n="HIAT:w" s="T218">Ĭmbi</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_443" n="HIAT:w" s="T219">tănan</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_446" n="HIAT:w" s="T220">kereʔ</ts>
                  <nts id="Seg_447" n="HIAT:ip">?</nts>
                  <nts id="Seg_448" n="HIAT:ip">"</nts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T223" id="Seg_451" n="HIAT:u" s="T221">
                  <ts e="T222" id="Seg_453" n="HIAT:w" s="T221">Kambi</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_456" n="HIAT:w" s="T222">surarlaʔbə</ts>
                  <nts id="Seg_457" n="HIAT:ip">.</nts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T226" id="Seg_460" n="HIAT:u" s="T223">
                  <nts id="Seg_461" n="HIAT:ip">"</nts>
                  <ts e="T224" id="Seg_463" n="HIAT:w" s="T223">Kajət</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_466" n="HIAT:w" s="T224">kuza</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_469" n="HIAT:w" s="T225">igel</ts>
                  <nts id="Seg_470" n="HIAT:ip">?</nts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T229" id="Seg_473" n="HIAT:u" s="T226">
                  <ts e="T227" id="Seg_475" n="HIAT:w" s="T226">Ĭmbim</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_478" n="HIAT:w" s="T227">pileʔ</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_481" n="HIAT:w" s="T228">mĭŋgel</ts>
                  <nts id="Seg_482" n="HIAT:ip">?</nts>
                  <nts id="Seg_483" n="HIAT:ip">"</nts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T233" id="Seg_486" n="HIAT:u" s="T229">
                  <nts id="Seg_487" n="HIAT:ip">"</nts>
                  <ts e="T230" id="Seg_489" n="HIAT:w" s="T229">Kaːnən</ts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_492" n="HIAT:w" s="T230">koʔbdobə</ts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_495" n="HIAT:w" s="T231">izʼəttə</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_498" n="HIAT:w" s="T232">šobiam</ts>
                  <nts id="Seg_499" n="HIAT:ip">.</nts>
                  <nts id="Seg_500" n="HIAT:ip">"</nts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T242" id="Seg_503" n="HIAT:u" s="T233">
                  <ts e="T234" id="Seg_505" n="HIAT:w" s="T233">Dĭ</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_508" n="HIAT:w" s="T234">kuza</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_511" n="HIAT:w" s="T235">parbi</ts>
                  <nts id="Seg_512" n="HIAT:ip">,</nts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_515" n="HIAT:w" s="T236">kaːndə</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_518" n="HIAT:w" s="T237">nörbəbi:</ts>
                  <nts id="Seg_519" n="HIAT:ip">"</nts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_522" n="HIAT:w" s="T238">Kaːnən</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_525" n="HIAT:w" s="T239">koʔbdobə</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_528" n="HIAT:w" s="T240">izʼəttə</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_531" n="HIAT:w" s="T241">šobiam</ts>
                  <nts id="Seg_532" n="HIAT:ip">.</nts>
                  <nts id="Seg_533" n="HIAT:ip">"</nts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T246" id="Seg_536" n="HIAT:u" s="T242">
                  <nts id="Seg_537" n="HIAT:ip">"</nts>
                  <ts e="T243" id="Seg_539" n="HIAT:w" s="T242">Kallaʔ</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_542" n="HIAT:w" s="T243">nörbeʔ:</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_545" n="HIAT:w" s="T244">Jakšəlzʼəʔ</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_548" n="HIAT:w" s="T245">paraʔ</ts>
                  <nts id="Seg_549" n="HIAT:ip">!</nts>
                  <nts id="Seg_550" n="HIAT:ip">"</nts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T248" id="Seg_553" n="HIAT:u" s="T246">
                  <nts id="Seg_554" n="HIAT:ip">"</nts>
                  <ts e="T247" id="Seg_556" n="HIAT:w" s="T246">Em</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_559" n="HIAT:w" s="T247">paraʔ</ts>
                  <nts id="Seg_560" n="HIAT:ip">.</nts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T253" id="Seg_563" n="HIAT:u" s="T248">
                  <ts e="T249" id="Seg_565" n="HIAT:w" s="T248">Jakšəzʼəʔ</ts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_568" n="HIAT:w" s="T249">ej</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_571" n="HIAT:w" s="T250">mĭbində</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_574" n="HIAT:w" s="T251">bilezʼəʔ</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_577" n="HIAT:w" s="T252">ilim</ts>
                  <nts id="Seg_578" n="HIAT:ip">.</nts>
                  <nts id="Seg_579" n="HIAT:ip">"</nts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T257" id="Seg_582" n="HIAT:u" s="T253">
                  <nts id="Seg_583" n="HIAT:ip">"</nts>
                  <ts e="T254" id="Seg_585" n="HIAT:w" s="T253">Öʔle</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_588" n="HIAT:w" s="T254">it</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_591" n="HIAT:w" s="T255">kazər</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_594" n="HIAT:w" s="T256">askərim</ts>
                  <nts id="Seg_595" n="HIAT:ip">!</nts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T260" id="Seg_598" n="HIAT:u" s="T257">
                  <ts e="T258" id="Seg_600" n="HIAT:w" s="T257">Sejmütsʼəʔ</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_603" n="HIAT:w" s="T258">taləj</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_606" n="HIAT:w" s="T259">nʼeʔledən</ts>
                  <nts id="Seg_607" n="HIAT:ip">.</nts>
                  <nts id="Seg_608" n="HIAT:ip">"</nts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T263" id="Seg_611" n="HIAT:u" s="T260">
                  <ts e="T261" id="Seg_613" n="HIAT:w" s="T260">Uʔbdələbi</ts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_616" n="HIAT:w" s="T261">kazər</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_619" n="HIAT:w" s="T262">askərzaŋdən</ts>
                  <nts id="Seg_620" n="HIAT:ip">.</nts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T265" id="Seg_623" n="HIAT:u" s="T263">
                  <ts e="T264" id="Seg_625" n="HIAT:w" s="T263">Askəriʔ</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_628" n="HIAT:w" s="T264">tunolaːndəgaiʔ</ts>
                  <nts id="Seg_629" n="HIAT:ip">.</nts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T270" id="Seg_632" n="HIAT:u" s="T265">
                  <ts e="T266" id="Seg_634" n="HIAT:w" s="T265">Sagər</ts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_637" n="HIAT:w" s="T266">bărabə</ts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_640" n="HIAT:w" s="T267">tʼikəbi</ts>
                  <nts id="Seg_641" n="HIAT:ip">,</nts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_644" n="HIAT:w" s="T268">konum</ts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_647" n="HIAT:w" s="T269">uʔbdəlluʔbi</ts>
                  <nts id="Seg_648" n="HIAT:ip">.</nts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T274" id="Seg_651" n="HIAT:u" s="T270">
                  <ts e="T271" id="Seg_653" n="HIAT:w" s="T270">Urgaːba</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_656" n="HIAT:w" s="T271">askərim</ts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_659" n="HIAT:w" s="T272">pʼaŋdəllaʔ</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_662" n="HIAT:w" s="T273">kunnaːmbi</ts>
                  <nts id="Seg_663" n="HIAT:ip">.</nts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T281" id="Seg_666" n="HIAT:u" s="T274">
                  <nts id="Seg_667" n="HIAT:ip">"</nts>
                  <ts e="T275" id="Seg_669" n="HIAT:w" s="T274">Uʔbdəlgut</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_672" n="HIAT:w" s="T275">bugaim</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_675" n="HIAT:w" s="T276">kazərim</ts>
                  <nts id="Seg_676" n="HIAT:ip">,</nts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_679" n="HIAT:w" s="T277">sejmütsʼəʔ</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_682" n="HIAT:w" s="T278">tʼuga</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_685" n="HIAT:w" s="T279">müʔleʔ</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_688" n="HIAT:w" s="T280">kunnədən</ts>
                  <nts id="Seg_689" n="HIAT:ip">.</nts>
                  <nts id="Seg_690" n="HIAT:ip">"</nts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T287" id="Seg_693" n="HIAT:u" s="T281">
                  <ts e="T282" id="Seg_695" n="HIAT:w" s="T281">Sagər</ts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_698" n="HIAT:w" s="T282">bărabə</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_701" n="HIAT:w" s="T283">tʼikəbi</ts>
                  <nts id="Seg_702" n="HIAT:ip">,</nts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_705" n="HIAT:w" s="T284">kuštə</ts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_708" n="HIAT:w" s="T285">öʔleʔ</ts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_711" n="HIAT:w" s="T286">mĭbi</ts>
                  <nts id="Seg_712" n="HIAT:ip">.</nts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T293" id="Seg_715" n="HIAT:u" s="T287">
                  <ts e="T288" id="Seg_717" n="HIAT:w" s="T287">Kuš</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_720" n="HIAT:w" s="T288">bugaim</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_723" n="HIAT:w" s="T289">pʼaŋdəlaʔ</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_726" n="HIAT:w" s="T290">kumbi</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_729" n="HIAT:w" s="T291">pan</ts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_732" n="HIAT:w" s="T292">nĭgəndən</ts>
                  <nts id="Seg_733" n="HIAT:ip">.</nts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T296" id="Seg_736" n="HIAT:u" s="T293">
                  <ts e="T294" id="Seg_738" n="HIAT:w" s="T293">Kaːn</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_741" n="HIAT:w" s="T294">maʔlaʔ</ts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_744" n="HIAT:w" s="T295">kojobi</ts>
                  <nts id="Seg_745" n="HIAT:ip">.</nts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T299" id="Seg_748" n="HIAT:u" s="T296">
                  <ts e="T297" id="Seg_750" n="HIAT:w" s="T296">Il</ts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_753" n="HIAT:w" s="T297">oʔbdobi</ts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_756" n="HIAT:w" s="T298">pardəj</ts>
                  <nts id="Seg_757" n="HIAT:ip">.</nts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T301" id="Seg_760" n="HIAT:u" s="T299">
                  <nts id="Seg_761" n="HIAT:ip">"</nts>
                  <ts e="T300" id="Seg_763" n="HIAT:w" s="T299">Išpeʔ</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_766" n="HIAT:w" s="T300">dĭm</ts>
                  <nts id="Seg_767" n="HIAT:ip">!</nts>
                  <nts id="Seg_768" n="HIAT:ip">"</nts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T304" id="Seg_771" n="HIAT:u" s="T301">
                  <ts e="T302" id="Seg_773" n="HIAT:w" s="T301">Parəldluʔbiiʔ</ts>
                  <nts id="Seg_774" n="HIAT:ip">,</nts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_777" n="HIAT:w" s="T302">tʼabəna</ts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_780" n="HIAT:w" s="T303">moliaiʔ</ts>
                  <nts id="Seg_781" n="HIAT:ip">.</nts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T311" id="Seg_784" n="HIAT:u" s="T304">
                  <ts e="T305" id="Seg_786" n="HIAT:w" s="T304">Sagər</ts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_789" n="HIAT:w" s="T305">bărabə</ts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_792" n="HIAT:w" s="T306">tʼikəj</ts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_795" n="HIAT:w" s="T307">nʼeʔbdəbi</ts>
                  <nts id="Seg_796" n="HIAT:ip">,</nts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_799" n="HIAT:w" s="T308">bübə</ts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_802" n="HIAT:w" s="T309">kămnəj</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_805" n="HIAT:w" s="T310">baʔbdəbi</ts>
                  <nts id="Seg_806" n="HIAT:ip">.</nts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T315" id="Seg_809" n="HIAT:u" s="T311">
                  <ts e="T312" id="Seg_811" n="HIAT:w" s="T311">Iləm</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_814" n="HIAT:w" s="T312">bü</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_817" n="HIAT:w" s="T313">kunnaʔ</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_820" n="HIAT:w" s="T314">kandəgat</ts>
                  <nts id="Seg_821" n="HIAT:ip">.</nts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T327" id="Seg_824" n="HIAT:u" s="T315">
                  <ts e="T316" id="Seg_826" n="HIAT:w" s="T315">Kaːn</ts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_829" n="HIAT:w" s="T316">kegərerie:</ts>
                  <nts id="Seg_830" n="HIAT:ip">"</nts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317.tx.1" id="Seg_833" n="HIAT:w" s="T317">Tʼartʼa</ts>
                  <nts id="Seg_834" n="HIAT:ip">_</nts>
                  <ts e="T318" id="Seg_836" n="HIAT:w" s="T317.tx.1">bartʼa</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318.tx.1" id="Seg_839" n="HIAT:w" s="T318">Pol</ts>
                  <nts id="Seg_840" n="HIAT:ip">_</nts>
                  <ts e="T319" id="Seg_842" n="HIAT:w" s="T318.tx.1">bartʼa</ts>
                  <nts id="Seg_843" n="HIAT:ip">,</nts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_846" n="HIAT:w" s="T319">pʼel</ts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_849" n="HIAT:w" s="T320">malbə</ts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_852" n="HIAT:w" s="T321">mĭlim</ts>
                  <nts id="Seg_853" n="HIAT:ip">,</nts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_856" n="HIAT:w" s="T322">pʼel</ts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_859" n="HIAT:w" s="T323">bajbə</ts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_862" n="HIAT:w" s="T324">mĭlim</ts>
                  <nts id="Seg_863" n="HIAT:ip">,</nts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_866" n="HIAT:w" s="T325">koʔbdom</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_869" n="HIAT:w" s="T326">mĭlim</ts>
                  <nts id="Seg_870" n="HIAT:ip">.</nts>
                  <nts id="Seg_871" n="HIAT:ip">"</nts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T342" id="Seg_874" n="HIAT:u" s="T327">
                  <nts id="Seg_875" n="HIAT:ip">"</nts>
                  <ts e="T328" id="Seg_877" n="HIAT:w" s="T327">Ej</ts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_880" n="HIAT:w" s="T328">kereʔ</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_883" n="HIAT:w" s="T329">măna</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_886" n="HIAT:w" s="T330">tăn</ts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_889" n="HIAT:w" s="T331">mallə</ts>
                  <nts id="Seg_890" n="HIAT:ip">,</nts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_893" n="HIAT:w" s="T332">ej</ts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_896" n="HIAT:w" s="T333">kereʔ</ts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_899" n="HIAT:w" s="T334">măna</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_902" n="HIAT:w" s="T335">tăn</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_905" n="HIAT:w" s="T336">bajlə</ts>
                  <nts id="Seg_906" n="HIAT:ip">,</nts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_909" n="HIAT:w" s="T337">toľko</ts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_912" n="HIAT:w" s="T338">abakaj</ts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_915" n="HIAT:w" s="T339">koʔbdol</ts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_918" n="HIAT:w" s="T340">kereʔ</ts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_921" n="HIAT:w" s="T341">măna</ts>
                  <nts id="Seg_922" n="HIAT:ip">.</nts>
                  <nts id="Seg_923" n="HIAT:ip">"</nts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T347" id="Seg_926" n="HIAT:u" s="T342">
                  <ts e="T343" id="Seg_928" n="HIAT:w" s="T342">Ibi</ts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_931" n="HIAT:w" s="T343">šoj</ts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_934" n="HIAT:w" s="T344">tʼobə</ts>
                  <nts id="Seg_935" n="HIAT:ip">,</nts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_938" n="HIAT:w" s="T345">bübə</ts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_941" n="HIAT:w" s="T346">suʔbdəbi</ts>
                  <nts id="Seg_942" n="HIAT:ip">.</nts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T357" id="Seg_945" n="HIAT:u" s="T347">
                  <ts e="T348" id="Seg_947" n="HIAT:w" s="T347">Kaːn</ts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_950" n="HIAT:w" s="T348">koʔbdobə</ts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_953" n="HIAT:w" s="T349">mĭbi</ts>
                  <nts id="Seg_954" n="HIAT:ip">,</nts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_957" n="HIAT:w" s="T350">aksaʔ</ts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_960" n="HIAT:w" s="T351">sejmündə</ts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_963" n="HIAT:w" s="T352">šĭldəbi</ts>
                  <nts id="Seg_964" n="HIAT:ip">,</nts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_967" n="HIAT:w" s="T353">bokullaʔ</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_970" n="HIAT:w" s="T354">uʔbdəbi</ts>
                  <nts id="Seg_971" n="HIAT:ip">,</nts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_974" n="HIAT:w" s="T355">maʔgəndə</ts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_977" n="HIAT:w" s="T356">kunnaːmbi</ts>
                  <nts id="Seg_978" n="HIAT:ip">.</nts>
                  <nts id="Seg_979" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T357" id="Seg_980" n="sc" s="T110">
               <ts e="T111" id="Seg_982" n="e" s="T110">Bile </ts>
               <ts e="T112" id="Seg_984" n="e" s="T111">kuza </ts>
               <ts e="T113" id="Seg_986" n="e" s="T112">amnobi. </ts>
               <ts e="T114" id="Seg_988" n="e" s="T113">Nĭmdə </ts>
               <ts e="T115" id="Seg_990" n="e" s="T114">tardʼža_bərdʼža. </ts>
               <ts e="T116" id="Seg_992" n="e" s="T115">Bile </ts>
               <ts e="T117" id="Seg_994" n="e" s="T116">sejmübə </ts>
               <ts e="T118" id="Seg_996" n="e" s="T117">dʼappi. </ts>
               <ts e="T119" id="Seg_998" n="e" s="T118">Aksaʔ </ts>
               <ts e="T120" id="Seg_1000" n="e" s="T119">ibi </ts>
               <ts e="T121" id="Seg_1002" n="e" s="T120">sejmüt. </ts>
               <ts e="T122" id="Seg_1004" n="e" s="T121">Konzandəbi, </ts>
               <ts e="T123" id="Seg_1006" n="e" s="T122">sagər </ts>
               <ts e="T124" id="Seg_1008" n="e" s="T123">bărabə </ts>
               <ts e="T125" id="Seg_1010" n="e" s="T124">sarbi, </ts>
               <ts e="T126" id="Seg_1012" n="e" s="T125">šoj </ts>
               <ts e="T127" id="Seg_1014" n="e" s="T126">tʼobə </ts>
               <ts e="T128" id="Seg_1016" n="e" s="T127">sarbi. </ts>
               <ts e="T129" id="Seg_1018" n="e" s="T128">Mĭlloʔbdəbi </ts>
               <ts e="T130" id="Seg_1020" n="e" s="T129">(mĭllüʔbi). </ts>
               <ts e="T131" id="Seg_1022" n="e" s="T130">Urgaːba </ts>
               <ts e="T132" id="Seg_1024" n="e" s="T131">toʔbdobi. </ts>
               <ts e="T133" id="Seg_1026" n="e" s="T132">"Măna </ts>
               <ts e="T134" id="Seg_1028" n="e" s="T133">it </ts>
               <ts e="T135" id="Seg_1030" n="e" s="T134">helaːzʼəttə!" </ts>
               <ts e="T136" id="Seg_1032" n="e" s="T135">"Pădaʔ </ts>
               <ts e="T137" id="Seg_1034" n="e" s="T136">sagər </ts>
               <ts e="T138" id="Seg_1036" n="e" s="T137">băranə!" </ts>
               <ts e="T139" id="Seg_1038" n="e" s="T138">Mĭlloʔbdəbi. </ts>
               <ts e="T140" id="Seg_1040" n="e" s="T139">Mʼeŋgej </ts>
               <ts e="T141" id="Seg_1042" n="e" s="T140">toʔbdobi. </ts>
               <ts e="T142" id="Seg_1044" n="e" s="T141">"Măna </ts>
               <ts e="T143" id="Seg_1046" n="e" s="T142">it </ts>
               <ts e="T144" id="Seg_1048" n="e" s="T143">helaːzʼəttə!" </ts>
               <ts e="T145" id="Seg_1050" n="e" s="T144">"Pădaʔ </ts>
               <ts e="T146" id="Seg_1052" n="e" s="T145">sagər </ts>
               <ts e="T147" id="Seg_1054" n="e" s="T146">băranə!" </ts>
               <ts e="T148" id="Seg_1056" n="e" s="T147">Păʔdəbi </ts>
               <ts e="T149" id="Seg_1058" n="e" s="T148">sagər </ts>
               <ts e="T150" id="Seg_1060" n="e" s="T149">băranə. </ts>
               <ts e="T151" id="Seg_1062" n="e" s="T150">Dĭgəttə </ts>
               <ts e="T152" id="Seg_1064" n="e" s="T151">kuš </ts>
               <ts e="T153" id="Seg_1066" n="e" s="T152">toʔbdobi. </ts>
               <ts e="T154" id="Seg_1068" n="e" s="T153">"Iʔ </ts>
               <ts e="T155" id="Seg_1070" n="e" s="T154">helaːzʼət!" </ts>
               <ts e="T156" id="Seg_1072" n="e" s="T155">"Pădaʔ </ts>
               <ts e="T157" id="Seg_1074" n="e" s="T156">sagər </ts>
               <ts e="T158" id="Seg_1076" n="e" s="T157">băranə!" </ts>
               <ts e="T159" id="Seg_1078" n="e" s="T158">Dĭgəttə </ts>
               <ts e="T160" id="Seg_1080" n="e" s="T159">bü </ts>
               <ts e="T161" id="Seg_1082" n="e" s="T160">toʔbdobi. </ts>
               <ts e="T162" id="Seg_1084" n="e" s="T161">Dĭ </ts>
               <ts e="T163" id="Seg_1086" n="e" s="T162">bün </ts>
               <ts e="T164" id="Seg_1088" n="e" s="T163">suʔlaʔ </ts>
               <ts e="T165" id="Seg_1090" n="e" s="T164">ibi, </ts>
               <ts e="T166" id="Seg_1092" n="e" s="T165">sagər </ts>
               <ts e="T167" id="Seg_1094" n="e" s="T166">bărandə </ts>
               <ts e="T168" id="Seg_1096" n="e" s="T167">šoj </ts>
               <ts e="T169" id="Seg_1098" n="e" s="T168">tʼoziʔ </ts>
               <ts e="T170" id="Seg_1100" n="e" s="T169">suʔbdəbi, </ts>
               <ts e="T171" id="Seg_1102" n="e" s="T170">šüjməndə </ts>
               <ts e="T172" id="Seg_1104" n="e" s="T171">sarlaʔ </ts>
               <ts e="T173" id="Seg_1106" n="e" s="T172">baʔbi. </ts>
               <ts e="T174" id="Seg_1108" n="e" s="T173">Kubində </ts>
               <ts e="T175" id="Seg_1110" n="e" s="T174">talaində </ts>
               <ts e="T176" id="Seg_1112" n="e" s="T175">toʔəndə </ts>
               <ts e="T177" id="Seg_1114" n="e" s="T176">jada </ts>
               <ts e="T178" id="Seg_1116" n="e" s="T177">nuga. </ts>
               <ts e="T179" id="Seg_1118" n="e" s="T178">Dĭ </ts>
               <ts e="T180" id="Seg_1120" n="e" s="T179">jadanə </ts>
               <ts e="T181" id="Seg_1122" n="e" s="T180">ej </ts>
               <ts e="T182" id="Seg_1124" n="e" s="T181">tuga </ts>
               <ts e="T183" id="Seg_1126" n="e" s="T182">üzəbi, </ts>
               <ts e="T184" id="Seg_1128" n="e" s="T183">šü </ts>
               <ts e="T185" id="Seg_1130" n="e" s="T184">embi, </ts>
               <ts e="T186" id="Seg_1132" n="e" s="T185">šüjməbə </ts>
               <ts e="T187" id="Seg_1134" n="e" s="T186">ötʼtʼerbi. </ts>
               <ts e="T188" id="Seg_1136" n="e" s="T187">Amna. </ts>
               <ts e="T189" id="Seg_1138" n="e" s="T188">Kaːn </ts>
               <ts e="T190" id="Seg_1140" n="e" s="T189">nʼiʔnen </ts>
               <ts e="T191" id="Seg_1142" n="e" s="T190">mĭlleʔbi, </ts>
               <ts e="T192" id="Seg_1144" n="e" s="T191">kulabaʔbi: </ts>
               <ts e="T193" id="Seg_1146" n="e" s="T192">"Šində </ts>
               <ts e="T194" id="Seg_1148" n="e" s="T193">dĭgən </ts>
               <ts e="T195" id="Seg_1150" n="e" s="T194">amna? </ts>
               <ts e="T196" id="Seg_1152" n="e" s="T195">Inebə </ts>
               <ts e="T197" id="Seg_1154" n="e" s="T196">ötʼtʼerlabaʔbi, </ts>
               <ts e="T198" id="Seg_1156" n="e" s="T197">măn </ts>
               <ts e="T199" id="Seg_1158" n="e" s="T198">kük </ts>
               <ts e="T200" id="Seg_1160" n="e" s="T199">noʔbə </ts>
               <ts e="T201" id="Seg_1162" n="e" s="T200">tonəllaʔbə </ts>
               <ts e="T202" id="Seg_1164" n="e" s="T201">ej </ts>
               <ts e="T203" id="Seg_1166" n="e" s="T202">surarga." </ts>
               <ts e="T204" id="Seg_1168" n="e" s="T203">Toʔgəndə </ts>
               <ts e="T205" id="Seg_1170" n="e" s="T204">mĭndə </ts>
               <ts e="T206" id="Seg_1172" n="e" s="T205">nʼekəbə </ts>
               <ts e="T207" id="Seg_1174" n="e" s="T206">öʔləʔbə. </ts>
               <ts e="T208" id="Seg_1176" n="e" s="T207">"Kallaʔ </ts>
               <ts e="T209" id="Seg_1178" n="e" s="T208">surardə! </ts>
               <ts e="T210" id="Seg_1180" n="e" s="T209">Šində </ts>
               <ts e="T211" id="Seg_1182" n="e" s="T210">dĭrgit </ts>
               <ts e="T212" id="Seg_1184" n="e" s="T211">igel? </ts>
               <ts e="T213" id="Seg_1186" n="e" s="T212">Ej </ts>
               <ts e="T214" id="Seg_1188" n="e" s="T213">surarga </ts>
               <ts e="T215" id="Seg_1190" n="e" s="T214">kaːnəm </ts>
               <ts e="T216" id="Seg_1192" n="e" s="T215">kaːnən </ts>
               <ts e="T217" id="Seg_1194" n="e" s="T216">noʔbdə </ts>
               <ts e="T218" id="Seg_1196" n="e" s="T217">tonnaʔbə. </ts>
               <ts e="T219" id="Seg_1198" n="e" s="T218">Ĭmbi </ts>
               <ts e="T220" id="Seg_1200" n="e" s="T219">tănan </ts>
               <ts e="T221" id="Seg_1202" n="e" s="T220">kereʔ?" </ts>
               <ts e="T222" id="Seg_1204" n="e" s="T221">Kambi </ts>
               <ts e="T223" id="Seg_1206" n="e" s="T222">surarlaʔbə. </ts>
               <ts e="T224" id="Seg_1208" n="e" s="T223">"Kajət </ts>
               <ts e="T225" id="Seg_1210" n="e" s="T224">kuza </ts>
               <ts e="T226" id="Seg_1212" n="e" s="T225">igel? </ts>
               <ts e="T227" id="Seg_1214" n="e" s="T226">Ĭmbim </ts>
               <ts e="T228" id="Seg_1216" n="e" s="T227">pileʔ </ts>
               <ts e="T229" id="Seg_1218" n="e" s="T228">mĭŋgel?" </ts>
               <ts e="T230" id="Seg_1220" n="e" s="T229">"Kaːnən </ts>
               <ts e="T231" id="Seg_1222" n="e" s="T230">koʔbdobə </ts>
               <ts e="T232" id="Seg_1224" n="e" s="T231">izʼəttə </ts>
               <ts e="T233" id="Seg_1226" n="e" s="T232">šobiam." </ts>
               <ts e="T234" id="Seg_1228" n="e" s="T233">Dĭ </ts>
               <ts e="T235" id="Seg_1230" n="e" s="T234">kuza </ts>
               <ts e="T236" id="Seg_1232" n="e" s="T235">parbi, </ts>
               <ts e="T237" id="Seg_1234" n="e" s="T236">kaːndə </ts>
               <ts e="T238" id="Seg_1236" n="e" s="T237">nörbəbi:" </ts>
               <ts e="T239" id="Seg_1238" n="e" s="T238">Kaːnən </ts>
               <ts e="T240" id="Seg_1240" n="e" s="T239">koʔbdobə </ts>
               <ts e="T241" id="Seg_1242" n="e" s="T240">izʼəttə </ts>
               <ts e="T242" id="Seg_1244" n="e" s="T241">šobiam." </ts>
               <ts e="T243" id="Seg_1246" n="e" s="T242">"Kallaʔ </ts>
               <ts e="T244" id="Seg_1248" n="e" s="T243">nörbeʔ: </ts>
               <ts e="T245" id="Seg_1250" n="e" s="T244">Jakšəlzʼəʔ </ts>
               <ts e="T246" id="Seg_1252" n="e" s="T245">paraʔ!" </ts>
               <ts e="T247" id="Seg_1254" n="e" s="T246">"Em </ts>
               <ts e="T248" id="Seg_1256" n="e" s="T247">paraʔ. </ts>
               <ts e="T249" id="Seg_1258" n="e" s="T248">Jakšəzʼəʔ </ts>
               <ts e="T250" id="Seg_1260" n="e" s="T249">ej </ts>
               <ts e="T251" id="Seg_1262" n="e" s="T250">mĭbində </ts>
               <ts e="T252" id="Seg_1264" n="e" s="T251">bilezʼəʔ </ts>
               <ts e="T253" id="Seg_1266" n="e" s="T252">ilim." </ts>
               <ts e="T254" id="Seg_1268" n="e" s="T253">"Öʔle </ts>
               <ts e="T255" id="Seg_1270" n="e" s="T254">it </ts>
               <ts e="T256" id="Seg_1272" n="e" s="T255">kazər </ts>
               <ts e="T257" id="Seg_1274" n="e" s="T256">askərim! </ts>
               <ts e="T258" id="Seg_1276" n="e" s="T257">Sejmütsʼəʔ </ts>
               <ts e="T259" id="Seg_1278" n="e" s="T258">taləj </ts>
               <ts e="T260" id="Seg_1280" n="e" s="T259">nʼeʔledən." </ts>
               <ts e="T261" id="Seg_1282" n="e" s="T260">Uʔbdələbi </ts>
               <ts e="T262" id="Seg_1284" n="e" s="T261">kazər </ts>
               <ts e="T263" id="Seg_1286" n="e" s="T262">askərzaŋdən. </ts>
               <ts e="T264" id="Seg_1288" n="e" s="T263">Askəriʔ </ts>
               <ts e="T265" id="Seg_1290" n="e" s="T264">tunolaːndəgaiʔ. </ts>
               <ts e="T266" id="Seg_1292" n="e" s="T265">Sagər </ts>
               <ts e="T267" id="Seg_1294" n="e" s="T266">bărabə </ts>
               <ts e="T268" id="Seg_1296" n="e" s="T267">tʼikəbi, </ts>
               <ts e="T269" id="Seg_1298" n="e" s="T268">konum </ts>
               <ts e="T270" id="Seg_1300" n="e" s="T269">uʔbdəlluʔbi. </ts>
               <ts e="T271" id="Seg_1302" n="e" s="T270">Urgaːba </ts>
               <ts e="T272" id="Seg_1304" n="e" s="T271">askərim </ts>
               <ts e="T273" id="Seg_1306" n="e" s="T272">pʼaŋdəllaʔ </ts>
               <ts e="T274" id="Seg_1308" n="e" s="T273">kunnaːmbi. </ts>
               <ts e="T275" id="Seg_1310" n="e" s="T274">"Uʔbdəlgut </ts>
               <ts e="T276" id="Seg_1312" n="e" s="T275">bugaim </ts>
               <ts e="T277" id="Seg_1314" n="e" s="T276">kazərim, </ts>
               <ts e="T278" id="Seg_1316" n="e" s="T277">sejmütsʼəʔ </ts>
               <ts e="T279" id="Seg_1318" n="e" s="T278">tʼuga </ts>
               <ts e="T280" id="Seg_1320" n="e" s="T279">müʔleʔ </ts>
               <ts e="T281" id="Seg_1322" n="e" s="T280">kunnədən." </ts>
               <ts e="T282" id="Seg_1324" n="e" s="T281">Sagər </ts>
               <ts e="T283" id="Seg_1326" n="e" s="T282">bărabə </ts>
               <ts e="T284" id="Seg_1328" n="e" s="T283">tʼikəbi, </ts>
               <ts e="T285" id="Seg_1330" n="e" s="T284">kuštə </ts>
               <ts e="T286" id="Seg_1332" n="e" s="T285">öʔleʔ </ts>
               <ts e="T287" id="Seg_1334" n="e" s="T286">mĭbi. </ts>
               <ts e="T288" id="Seg_1336" n="e" s="T287">Kuš </ts>
               <ts e="T289" id="Seg_1338" n="e" s="T288">bugaim </ts>
               <ts e="T290" id="Seg_1340" n="e" s="T289">pʼaŋdəlaʔ </ts>
               <ts e="T291" id="Seg_1342" n="e" s="T290">kumbi </ts>
               <ts e="T292" id="Seg_1344" n="e" s="T291">pan </ts>
               <ts e="T293" id="Seg_1346" n="e" s="T292">nĭgəndən. </ts>
               <ts e="T294" id="Seg_1348" n="e" s="T293">Kaːn </ts>
               <ts e="T295" id="Seg_1350" n="e" s="T294">maʔlaʔ </ts>
               <ts e="T296" id="Seg_1352" n="e" s="T295">kojobi. </ts>
               <ts e="T297" id="Seg_1354" n="e" s="T296">Il </ts>
               <ts e="T298" id="Seg_1356" n="e" s="T297">oʔbdobi </ts>
               <ts e="T299" id="Seg_1358" n="e" s="T298">pardəj. </ts>
               <ts e="T300" id="Seg_1360" n="e" s="T299">"Išpeʔ </ts>
               <ts e="T301" id="Seg_1362" n="e" s="T300">dĭm!" </ts>
               <ts e="T302" id="Seg_1364" n="e" s="T301">Parəldluʔbiiʔ, </ts>
               <ts e="T303" id="Seg_1366" n="e" s="T302">tʼabəna </ts>
               <ts e="T304" id="Seg_1368" n="e" s="T303">moliaiʔ. </ts>
               <ts e="T305" id="Seg_1370" n="e" s="T304">Sagər </ts>
               <ts e="T306" id="Seg_1372" n="e" s="T305">bărabə </ts>
               <ts e="T307" id="Seg_1374" n="e" s="T306">tʼikəj </ts>
               <ts e="T308" id="Seg_1376" n="e" s="T307">nʼeʔbdəbi, </ts>
               <ts e="T309" id="Seg_1378" n="e" s="T308">bübə </ts>
               <ts e="T310" id="Seg_1380" n="e" s="T309">kămnəj </ts>
               <ts e="T311" id="Seg_1382" n="e" s="T310">baʔbdəbi. </ts>
               <ts e="T312" id="Seg_1384" n="e" s="T311">Iləm </ts>
               <ts e="T313" id="Seg_1386" n="e" s="T312">bü </ts>
               <ts e="T314" id="Seg_1388" n="e" s="T313">kunnaʔ </ts>
               <ts e="T315" id="Seg_1390" n="e" s="T314">kandəgat. </ts>
               <ts e="T316" id="Seg_1392" n="e" s="T315">Kaːn </ts>
               <ts e="T317" id="Seg_1394" n="e" s="T316">kegərerie:" </ts>
               <ts e="T318" id="Seg_1396" n="e" s="T317">Tʼartʼa_bartʼa </ts>
               <ts e="T319" id="Seg_1398" n="e" s="T318">Pol_bartʼa, </ts>
               <ts e="T320" id="Seg_1400" n="e" s="T319">pʼel </ts>
               <ts e="T321" id="Seg_1402" n="e" s="T320">malbə </ts>
               <ts e="T322" id="Seg_1404" n="e" s="T321">mĭlim, </ts>
               <ts e="T323" id="Seg_1406" n="e" s="T322">pʼel </ts>
               <ts e="T324" id="Seg_1408" n="e" s="T323">bajbə </ts>
               <ts e="T325" id="Seg_1410" n="e" s="T324">mĭlim, </ts>
               <ts e="T326" id="Seg_1412" n="e" s="T325">koʔbdom </ts>
               <ts e="T327" id="Seg_1414" n="e" s="T326">mĭlim." </ts>
               <ts e="T328" id="Seg_1416" n="e" s="T327">"Ej </ts>
               <ts e="T329" id="Seg_1418" n="e" s="T328">kereʔ </ts>
               <ts e="T330" id="Seg_1420" n="e" s="T329">măna </ts>
               <ts e="T331" id="Seg_1422" n="e" s="T330">tăn </ts>
               <ts e="T332" id="Seg_1424" n="e" s="T331">mallə, </ts>
               <ts e="T333" id="Seg_1426" n="e" s="T332">ej </ts>
               <ts e="T334" id="Seg_1428" n="e" s="T333">kereʔ </ts>
               <ts e="T335" id="Seg_1430" n="e" s="T334">măna </ts>
               <ts e="T336" id="Seg_1432" n="e" s="T335">tăn </ts>
               <ts e="T337" id="Seg_1434" n="e" s="T336">bajlə, </ts>
               <ts e="T338" id="Seg_1436" n="e" s="T337">toľko </ts>
               <ts e="T339" id="Seg_1438" n="e" s="T338">abakaj </ts>
               <ts e="T340" id="Seg_1440" n="e" s="T339">koʔbdol </ts>
               <ts e="T341" id="Seg_1442" n="e" s="T340">kereʔ </ts>
               <ts e="T342" id="Seg_1444" n="e" s="T341">măna." </ts>
               <ts e="T343" id="Seg_1446" n="e" s="T342">Ibi </ts>
               <ts e="T344" id="Seg_1448" n="e" s="T343">šoj </ts>
               <ts e="T345" id="Seg_1450" n="e" s="T344">tʼobə, </ts>
               <ts e="T346" id="Seg_1452" n="e" s="T345">bübə </ts>
               <ts e="T347" id="Seg_1454" n="e" s="T346">suʔbdəbi. </ts>
               <ts e="T348" id="Seg_1456" n="e" s="T347">Kaːn </ts>
               <ts e="T349" id="Seg_1458" n="e" s="T348">koʔbdobə </ts>
               <ts e="T350" id="Seg_1460" n="e" s="T349">mĭbi, </ts>
               <ts e="T351" id="Seg_1462" n="e" s="T350">aksaʔ </ts>
               <ts e="T352" id="Seg_1464" n="e" s="T351">sejmündə </ts>
               <ts e="T353" id="Seg_1466" n="e" s="T352">šĭldəbi, </ts>
               <ts e="T354" id="Seg_1468" n="e" s="T353">bokullaʔ </ts>
               <ts e="T355" id="Seg_1470" n="e" s="T354">uʔbdəbi, </ts>
               <ts e="T356" id="Seg_1472" n="e" s="T355">maʔgəndə </ts>
               <ts e="T357" id="Seg_1474" n="e" s="T356">kunnaːmbi. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T113" id="Seg_1475" s="T110">AA_1914_Khan_flk.001 (001.001)</ta>
            <ta e="T115" id="Seg_1476" s="T113">AA_1914_Khan_flk.002 (001.002)</ta>
            <ta e="T118" id="Seg_1477" s="T115">AA_1914_Khan_flk.003 (001.003)</ta>
            <ta e="T121" id="Seg_1478" s="T118">AA_1914_Khan_flk.004 (001.004)</ta>
            <ta e="T128" id="Seg_1479" s="T121">AA_1914_Khan_flk.005 (001.005)</ta>
            <ta e="T130" id="Seg_1480" s="T128">AA_1914_Khan_flk.006 (001.006)</ta>
            <ta e="T132" id="Seg_1481" s="T130">AA_1914_Khan_flk.007 (001.007)</ta>
            <ta e="T135" id="Seg_1482" s="T132">AA_1914_Khan_flk.008 (001.008)</ta>
            <ta e="T138" id="Seg_1483" s="T135">AA_1914_Khan_flk.009 (001.009)</ta>
            <ta e="T139" id="Seg_1484" s="T138">AA_1914_Khan_flk.010 (001.010)</ta>
            <ta e="T141" id="Seg_1485" s="T139">AA_1914_Khan_flk.011 (001.011)</ta>
            <ta e="T144" id="Seg_1486" s="T141">AA_1914_Khan_flk.012 (001.012)</ta>
            <ta e="T147" id="Seg_1487" s="T144">AA_1914_Khan_flk.013 (001.013)</ta>
            <ta e="T150" id="Seg_1488" s="T147">AA_1914_Khan_flk.014 (001.014)</ta>
            <ta e="T153" id="Seg_1489" s="T150">AA_1914_Khan_flk.015 (001.015)</ta>
            <ta e="T155" id="Seg_1490" s="T153">AA_1914_Khan_flk.016 (001.016)</ta>
            <ta e="T158" id="Seg_1491" s="T155">AA_1914_Khan_flk.017 (001.017)</ta>
            <ta e="T161" id="Seg_1492" s="T158">AA_1914_Khan_flk.018 (001.018)</ta>
            <ta e="T173" id="Seg_1493" s="T161">AA_1914_Khan_flk.019 (001.019)</ta>
            <ta e="T178" id="Seg_1494" s="T173">AA_1914_Khan_flk.020 (001.020)</ta>
            <ta e="T187" id="Seg_1495" s="T178">AA_1914_Khan_flk.021 (001.021)</ta>
            <ta e="T188" id="Seg_1496" s="T187">AA_1914_Khan_flk.022 (001.022)</ta>
            <ta e="T195" id="Seg_1497" s="T188">AA_1914_Khan_flk.023 (002.001)</ta>
            <ta e="T203" id="Seg_1498" s="T195">AA_1914_Khan_flk.024 (002.003)</ta>
            <ta e="T207" id="Seg_1499" s="T203">AA_1914_Khan_flk.025 (002.004)</ta>
            <ta e="T209" id="Seg_1500" s="T207">AA_1914_Khan_flk.026 (002.005)</ta>
            <ta e="T212" id="Seg_1501" s="T209">AA_1914_Khan_flk.027 (002.006)</ta>
            <ta e="T218" id="Seg_1502" s="T212">AA_1914_Khan_flk.028 (002.007)</ta>
            <ta e="T221" id="Seg_1503" s="T218">AA_1914_Khan_flk.029 (002.008)</ta>
            <ta e="T223" id="Seg_1504" s="T221">AA_1914_Khan_flk.030 (002.009)</ta>
            <ta e="T226" id="Seg_1505" s="T223">AA_1914_Khan_flk.031 (002.010)</ta>
            <ta e="T229" id="Seg_1506" s="T226">AA_1914_Khan_flk.032 (002.011)</ta>
            <ta e="T233" id="Seg_1507" s="T229">AA_1914_Khan_flk.033 (002.012)</ta>
            <ta e="T242" id="Seg_1508" s="T233">AA_1914_Khan_flk.034 (002.013)</ta>
            <ta e="T246" id="Seg_1509" s="T242">AA_1914_Khan_flk.035 (002.014)</ta>
            <ta e="T248" id="Seg_1510" s="T246">AA_1914_Khan_flk.036 (002.015)</ta>
            <ta e="T253" id="Seg_1511" s="T248">AA_1914_Khan_flk.037 (002.016)</ta>
            <ta e="T257" id="Seg_1512" s="T253">AA_1914_Khan_flk.038 (002.017)</ta>
            <ta e="T260" id="Seg_1513" s="T257">AA_1914_Khan_flk.039 (002.018)</ta>
            <ta e="T263" id="Seg_1514" s="T260">AA_1914_Khan_flk.040 (002.019)</ta>
            <ta e="T265" id="Seg_1515" s="T263">AA_1914_Khan_flk.041 (002.020)</ta>
            <ta e="T270" id="Seg_1516" s="T265">AA_1914_Khan_flk.042 (002.021)</ta>
            <ta e="T274" id="Seg_1517" s="T270">AA_1914_Khan_flk.043 (002.022)</ta>
            <ta e="T281" id="Seg_1518" s="T274">AA_1914_Khan_flk.044 (002.023)</ta>
            <ta e="T287" id="Seg_1519" s="T281">AA_1914_Khan_flk.045 (002.024)</ta>
            <ta e="T293" id="Seg_1520" s="T287">AA_1914_Khan_flk.046 (002.025)</ta>
            <ta e="T296" id="Seg_1521" s="T293">AA_1914_Khan_flk.047 (002.026)</ta>
            <ta e="T299" id="Seg_1522" s="T296">AA_1914_Khan_flk.048 (002.027)</ta>
            <ta e="T301" id="Seg_1523" s="T299">AA_1914_Khan_flk.049 (002.028)</ta>
            <ta e="T304" id="Seg_1524" s="T301">AA_1914_Khan_flk.050 (002.029)</ta>
            <ta e="T311" id="Seg_1525" s="T304">AA_1914_Khan_flk.051 (002.030)</ta>
            <ta e="T315" id="Seg_1526" s="T311">AA_1914_Khan_flk.052 (002.031)</ta>
            <ta e="T327" id="Seg_1527" s="T315">AA_1914_Khan_flk.053 (002.032)</ta>
            <ta e="T342" id="Seg_1528" s="T327">AA_1914_Khan_flk.054 (002.033)</ta>
            <ta e="T347" id="Seg_1529" s="T342">AA_1914_Khan_flk.055 (002.034)</ta>
            <ta e="T357" id="Seg_1530" s="T347">AA_1914_Khan_flk.056 (002.035)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T113" id="Seg_1531" s="T110">Bile kuza amnobi. </ta>
            <ta e="T115" id="Seg_1532" s="T113">Nĭmdə tardʼža_bərdʼža. </ta>
            <ta e="T118" id="Seg_1533" s="T115">Bile sejmübə dʼappi. </ta>
            <ta e="T121" id="Seg_1534" s="T118">Aksaʔ ibi sejmüt. </ta>
            <ta e="T128" id="Seg_1535" s="T121">Konzandəbi, sagər bărabə sarbi, šoj tʼobə sarbi. </ta>
            <ta e="T130" id="Seg_1536" s="T128">Mĭlloʔbdəbi (mĭllüʔbi). </ta>
            <ta e="T132" id="Seg_1537" s="T130">Urgaːba toʔbdobi. </ta>
            <ta e="T135" id="Seg_1538" s="T132">"Măna it helaːzʼəttə!" </ta>
            <ta e="T138" id="Seg_1539" s="T135">"Pădaʔ sagər băranə!" </ta>
            <ta e="T139" id="Seg_1540" s="T138">Mĭlloʔbdəbi. </ta>
            <ta e="T141" id="Seg_1541" s="T139">Mʼeŋgej toʔbdobi. </ta>
            <ta e="T144" id="Seg_1542" s="T141">"Măna it helaːzʼəttə!" </ta>
            <ta e="T147" id="Seg_1543" s="T144">"Pădaʔ sagər băranə!" </ta>
            <ta e="T150" id="Seg_1544" s="T147">Păʔdəbi sagər băranə. </ta>
            <ta e="T153" id="Seg_1545" s="T150">Dĭgəttə kuš toʔbdobi. </ta>
            <ta e="T155" id="Seg_1546" s="T153">"Iʔ helaːzʼət!" </ta>
            <ta e="T158" id="Seg_1547" s="T155">"Pădaʔ sagər băranə!" </ta>
            <ta e="T161" id="Seg_1548" s="T158">Dĭgəttə bü toʔbdobi. </ta>
            <ta e="T173" id="Seg_1549" s="T161">Dĭ bün suʔlaʔ ibi, sagər bărandə šoj tʼoziʔ suʔbdəbi, šüjməndə sarlaʔ baʔbi. </ta>
            <ta e="T178" id="Seg_1550" s="T173">Kubində talaində toʔəndə jada nuga. </ta>
            <ta e="T187" id="Seg_1551" s="T178">Dĭ jadanə ej tuga üzəbi, šü embi, šüjməbə ötʼtʼerbi. </ta>
            <ta e="T188" id="Seg_1552" s="T187">Amna. </ta>
            <ta e="T195" id="Seg_1553" s="T188">Kaːn nʼiʔnen mĭlleʔbi, kulabaʔbi: "Šində dĭgən amna? </ta>
            <ta e="T203" id="Seg_1554" s="T195">Inebə ötʼtʼerlabaʔbi, măn kük noʔbə tonəllaʔbə ej surarga." </ta>
            <ta e="T207" id="Seg_1555" s="T203">Toʔgəndə mĭndə nʼekəbə öʔləʔbə. </ta>
            <ta e="T209" id="Seg_1556" s="T207">"Kallaʔ surardə! </ta>
            <ta e="T212" id="Seg_1557" s="T209">Šində dĭrgit igel? </ta>
            <ta e="T218" id="Seg_1558" s="T212">Ej surarga kaːnəm kaːnən noʔbdə tonnaʔbə. </ta>
            <ta e="T221" id="Seg_1559" s="T218">Ĭmbi tănan kereʔ?" </ta>
            <ta e="T223" id="Seg_1560" s="T221">Kambi surarlaʔbə. </ta>
            <ta e="T226" id="Seg_1561" s="T223">"Kajət kuza igel? </ta>
            <ta e="T229" id="Seg_1562" s="T226">Ĭmbim pileʔ mĭŋgel?" </ta>
            <ta e="T233" id="Seg_1563" s="T229">"Kaːnən koʔbdobə izʼəttə šobiam." </ta>
            <ta e="T242" id="Seg_1564" s="T233">Dĭ kuza parbi, kaːndə nörbəbi:" Kaːnən koʔbdobə izʼəttə šobiam." </ta>
            <ta e="T246" id="Seg_1565" s="T242">"Kallaʔ nörbeʔ: Jakšəlzʼəʔ paraʔ!" </ta>
            <ta e="T248" id="Seg_1566" s="T246">"Em paraʔ. </ta>
            <ta e="T253" id="Seg_1567" s="T248">Jakšəzʼəʔ ej mĭbində bilezʼəʔ ilim." </ta>
            <ta e="T257" id="Seg_1568" s="T253">"Öʔle it kazər askərim! </ta>
            <ta e="T260" id="Seg_1569" s="T257">Sejmütsʼəʔ taləj nʼeʔledən." </ta>
            <ta e="T263" id="Seg_1570" s="T260">Uʔbdələbi kazər askərzaŋdən. </ta>
            <ta e="T265" id="Seg_1571" s="T263">Askəriʔ tunolaːndəgaiʔ. </ta>
            <ta e="T270" id="Seg_1572" s="T265">Sagər bărabə tʼikəbi, konum uʔbdəlluʔbi. </ta>
            <ta e="T274" id="Seg_1573" s="T270">Urgaːba askərim pʼaŋdəllaʔ kunnaːmbi. </ta>
            <ta e="T281" id="Seg_1574" s="T274">"Uʔbdəlgut bugaim kazərim, sejmütsʼəʔ tʼuga müʔleʔ kunnədən." </ta>
            <ta e="T287" id="Seg_1575" s="T281">Sagər bărabə tʼikəbi, kuštə öʔleʔ mĭbi. </ta>
            <ta e="T293" id="Seg_1576" s="T287">Kuš bugaim pʼaŋdəlaʔ kumbi pan nĭgəndən. </ta>
            <ta e="T296" id="Seg_1577" s="T293">Kaːn maʔlaʔ kojobi. </ta>
            <ta e="T299" id="Seg_1578" s="T296">Il oʔbdobi pardəj. </ta>
            <ta e="T301" id="Seg_1579" s="T299">"Išpeʔ dĭm!" </ta>
            <ta e="T304" id="Seg_1580" s="T301">Parəldluʔbiiʔ, tʼabəna moliaiʔ. </ta>
            <ta e="T311" id="Seg_1581" s="T304">Sagər bărabə tʼikəj nʼeʔbdəbi, bübə kămnəj baʔbdəbi. </ta>
            <ta e="T315" id="Seg_1582" s="T311">Iləm bü kunnaʔ kandəgat. </ta>
            <ta e="T327" id="Seg_1583" s="T315">Kaːn kegərerie:" Tʼartʼa_bartʼa pol_bartʼa, pʼel malbə mĭlim, pʼel bajbə mĭlim, koʔbdom mĭlim." </ta>
            <ta e="T342" id="Seg_1584" s="T327">"Ej kereʔ măna tăn mallə, ej kereʔ măna tăn bajlə, toľko abakaj koʔbdol kereʔ măna." </ta>
            <ta e="T347" id="Seg_1585" s="T342">Ibi šoj tʼobə, bübə suʔbdəbi. </ta>
            <ta e="T357" id="Seg_1586" s="T347">Kaːn koʔbdobə mĭbi, aksaʔ sejmündə šĭldəbi, bokullaʔ uʔbdəbi, maʔgəndə kunnaːmbi. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T111" id="Seg_1587" s="T110">bile</ta>
            <ta e="T112" id="Seg_1588" s="T111">kuza</ta>
            <ta e="T113" id="Seg_1589" s="T112">amno-bi</ta>
            <ta e="T114" id="Seg_1590" s="T113">nĭm-də</ta>
            <ta e="T115" id="Seg_1591" s="T114">tardʼža_bərdʼža</ta>
            <ta e="T116" id="Seg_1592" s="T115">bile</ta>
            <ta e="T117" id="Seg_1593" s="T116">sejmü-bə</ta>
            <ta e="T118" id="Seg_1594" s="T117">dʼap-pi</ta>
            <ta e="T119" id="Seg_1595" s="T118">aksaʔ</ta>
            <ta e="T120" id="Seg_1596" s="T119">i-bi</ta>
            <ta e="T121" id="Seg_1597" s="T120">sejmü-t</ta>
            <ta e="T122" id="Seg_1598" s="T121">konzan-də-bi</ta>
            <ta e="T123" id="Seg_1599" s="T122">sagər</ta>
            <ta e="T124" id="Seg_1600" s="T123">băra-bə</ta>
            <ta e="T125" id="Seg_1601" s="T124">sar-bi</ta>
            <ta e="T126" id="Seg_1602" s="T125">šo-j</ta>
            <ta e="T127" id="Seg_1603" s="T126">tʼo-bə</ta>
            <ta e="T128" id="Seg_1604" s="T127">sar-bi</ta>
            <ta e="T129" id="Seg_1605" s="T128">mĭl-loʔbdə-bi</ta>
            <ta e="T130" id="Seg_1606" s="T129">mĭl-lüʔ-bi</ta>
            <ta e="T131" id="Seg_1607" s="T130">urgaːba</ta>
            <ta e="T132" id="Seg_1608" s="T131">toʔbdo-bi</ta>
            <ta e="T133" id="Seg_1609" s="T132">măna</ta>
            <ta e="T134" id="Seg_1610" s="T133">i-t</ta>
            <ta e="T135" id="Seg_1611" s="T134">helaː-zʼəttə</ta>
            <ta e="T136" id="Seg_1612" s="T135">păda-ʔ</ta>
            <ta e="T137" id="Seg_1613" s="T136">sagər</ta>
            <ta e="T138" id="Seg_1614" s="T137">băra-nə</ta>
            <ta e="T139" id="Seg_1615" s="T138">mĭl-loʔbdə-bi</ta>
            <ta e="T140" id="Seg_1616" s="T139">mʼeŋgej</ta>
            <ta e="T141" id="Seg_1617" s="T140">toʔbdo-bi</ta>
            <ta e="T142" id="Seg_1618" s="T141">măna</ta>
            <ta e="T143" id="Seg_1619" s="T142">i-t</ta>
            <ta e="T144" id="Seg_1620" s="T143">helaː-zʼəttə</ta>
            <ta e="T145" id="Seg_1621" s="T144">păda-ʔ</ta>
            <ta e="T146" id="Seg_1622" s="T145">sagər</ta>
            <ta e="T147" id="Seg_1623" s="T146">băra-nə</ta>
            <ta e="T148" id="Seg_1624" s="T147">păʔ-də-bi</ta>
            <ta e="T149" id="Seg_1625" s="T148">sagər</ta>
            <ta e="T150" id="Seg_1626" s="T149">băra-nə</ta>
            <ta e="T151" id="Seg_1627" s="T150">dĭgəttə</ta>
            <ta e="T152" id="Seg_1628" s="T151">kuš</ta>
            <ta e="T153" id="Seg_1629" s="T152">toʔbdo-bi</ta>
            <ta e="T154" id="Seg_1630" s="T153">i-ʔ</ta>
            <ta e="T155" id="Seg_1631" s="T154">helaː-zʼət</ta>
            <ta e="T156" id="Seg_1632" s="T155">păda-ʔ</ta>
            <ta e="T157" id="Seg_1633" s="T156">sagər</ta>
            <ta e="T158" id="Seg_1634" s="T157">băra-nə</ta>
            <ta e="T159" id="Seg_1635" s="T158">dĭgəttə</ta>
            <ta e="T160" id="Seg_1636" s="T159">bü</ta>
            <ta e="T161" id="Seg_1637" s="T160">toʔbdo-bi</ta>
            <ta e="T162" id="Seg_1638" s="T161">dĭ</ta>
            <ta e="T163" id="Seg_1639" s="T162">bü-n</ta>
            <ta e="T164" id="Seg_1640" s="T163">suʔ-laʔ</ta>
            <ta e="T165" id="Seg_1641" s="T164">i-bi</ta>
            <ta e="T166" id="Seg_1642" s="T165">sagər</ta>
            <ta e="T167" id="Seg_1643" s="T166">băra-ndə</ta>
            <ta e="T168" id="Seg_1644" s="T167">šo-j</ta>
            <ta e="T169" id="Seg_1645" s="T168">tʼo-ziʔ</ta>
            <ta e="T170" id="Seg_1646" s="T169">suʔbdə-bi</ta>
            <ta e="T171" id="Seg_1647" s="T170">šüjmə-ndə</ta>
            <ta e="T172" id="Seg_1648" s="T171">sar-laʔ</ta>
            <ta e="T173" id="Seg_1649" s="T172">baʔ-bi</ta>
            <ta e="T174" id="Seg_1650" s="T173">ku-bi-ndə</ta>
            <ta e="T175" id="Seg_1651" s="T174">talai-ndə</ta>
            <ta e="T176" id="Seg_1652" s="T175">toʔ-əndə</ta>
            <ta e="T177" id="Seg_1653" s="T176">jada</ta>
            <ta e="T178" id="Seg_1654" s="T177">nu-ga</ta>
            <ta e="T179" id="Seg_1655" s="T178">dĭ</ta>
            <ta e="T180" id="Seg_1656" s="T179">jada-nə</ta>
            <ta e="T181" id="Seg_1657" s="T180">ej</ta>
            <ta e="T182" id="Seg_1658" s="T181">tu-ga</ta>
            <ta e="T183" id="Seg_1659" s="T182">üzə-bi</ta>
            <ta e="T184" id="Seg_1660" s="T183">šü</ta>
            <ta e="T185" id="Seg_1661" s="T184">em-bi</ta>
            <ta e="T186" id="Seg_1662" s="T185">šüjmə-bə</ta>
            <ta e="T187" id="Seg_1663" s="T186">ötʼtʼer-bi</ta>
            <ta e="T188" id="Seg_1664" s="T187">amna</ta>
            <ta e="T189" id="Seg_1665" s="T188">kaːn</ta>
            <ta e="T190" id="Seg_1666" s="T189">nʼiʔnen</ta>
            <ta e="T191" id="Seg_1667" s="T190">mĭl-leʔ-bi</ta>
            <ta e="T192" id="Seg_1668" s="T191">ku-labaʔ-bi</ta>
            <ta e="T193" id="Seg_1669" s="T192">šində</ta>
            <ta e="T194" id="Seg_1670" s="T193">dĭgən</ta>
            <ta e="T195" id="Seg_1671" s="T194">amna</ta>
            <ta e="T196" id="Seg_1672" s="T195">ine-bə</ta>
            <ta e="T197" id="Seg_1673" s="T196">ötʼtʼer-labaʔ-bi</ta>
            <ta e="T198" id="Seg_1674" s="T197">măn</ta>
            <ta e="T199" id="Seg_1675" s="T198">kük</ta>
            <ta e="T200" id="Seg_1676" s="T199">noʔ-bə</ta>
            <ta e="T201" id="Seg_1677" s="T200">tonə-l-laʔbə</ta>
            <ta e="T202" id="Seg_1678" s="T201">ej</ta>
            <ta e="T203" id="Seg_1679" s="T202">surar-ga</ta>
            <ta e="T204" id="Seg_1680" s="T203">toʔ-gəndə</ta>
            <ta e="T205" id="Seg_1681" s="T204">mĭn-də</ta>
            <ta e="T206" id="Seg_1682" s="T205">nʼekə-bə</ta>
            <ta e="T207" id="Seg_1683" s="T206">öʔ-ləʔbə</ta>
            <ta e="T208" id="Seg_1684" s="T207">kal-laʔ</ta>
            <ta e="T209" id="Seg_1685" s="T208">surar-də</ta>
            <ta e="T210" id="Seg_1686" s="T209">šində</ta>
            <ta e="T211" id="Seg_1687" s="T210">dĭrgit</ta>
            <ta e="T212" id="Seg_1688" s="T211">i-ge-l</ta>
            <ta e="T213" id="Seg_1689" s="T212">ej</ta>
            <ta e="T214" id="Seg_1690" s="T213">surar-ga</ta>
            <ta e="T215" id="Seg_1691" s="T214">kaːn-ə-m</ta>
            <ta e="T216" id="Seg_1692" s="T215">kaːn-ə-n</ta>
            <ta e="T217" id="Seg_1693" s="T216">noʔb-də</ta>
            <ta e="T218" id="Seg_1694" s="T217">ton-naʔbə</ta>
            <ta e="T219" id="Seg_1695" s="T218">ĭmbi</ta>
            <ta e="T220" id="Seg_1696" s="T219">tănan</ta>
            <ta e="T221" id="Seg_1697" s="T220">kereʔ</ta>
            <ta e="T222" id="Seg_1698" s="T221">kam-bi</ta>
            <ta e="T223" id="Seg_1699" s="T222">surar-laʔbə</ta>
            <ta e="T224" id="Seg_1700" s="T223">Kajət</ta>
            <ta e="T225" id="Seg_1701" s="T224">kuza</ta>
            <ta e="T226" id="Seg_1702" s="T225">i-ge-l</ta>
            <ta e="T227" id="Seg_1703" s="T226">ĭmbi-m</ta>
            <ta e="T228" id="Seg_1704" s="T227">pi-leʔ</ta>
            <ta e="T229" id="Seg_1705" s="T228">mĭŋ-ge-l</ta>
            <ta e="T230" id="Seg_1706" s="T229">kaːn-ə-n</ta>
            <ta e="T231" id="Seg_1707" s="T230">koʔbdo-bə</ta>
            <ta e="T232" id="Seg_1708" s="T231">i-zʼəttə</ta>
            <ta e="T233" id="Seg_1709" s="T232">šo-bia-m</ta>
            <ta e="T234" id="Seg_1710" s="T233">dĭ</ta>
            <ta e="T235" id="Seg_1711" s="T234">kuza</ta>
            <ta e="T236" id="Seg_1712" s="T235">par-bi</ta>
            <ta e="T237" id="Seg_1713" s="T236">kaːn-də</ta>
            <ta e="T238" id="Seg_1714" s="T237">nörbə-bi</ta>
            <ta e="T239" id="Seg_1715" s="T238">kaːn-ə-n</ta>
            <ta e="T240" id="Seg_1716" s="T239">koʔbdo-bə</ta>
            <ta e="T241" id="Seg_1717" s="T240">i-zʼəttə</ta>
            <ta e="T242" id="Seg_1718" s="T241">šo-bia-m</ta>
            <ta e="T243" id="Seg_1719" s="T242">kal-laʔ</ta>
            <ta e="T244" id="Seg_1720" s="T243">nörbe-ʔ</ta>
            <ta e="T245" id="Seg_1721" s="T244">jakšə-l-zʼəʔ</ta>
            <ta e="T246" id="Seg_1722" s="T245">par-a-ʔ</ta>
            <ta e="T247" id="Seg_1723" s="T246">e-m</ta>
            <ta e="T248" id="Seg_1724" s="T247">par-a-ʔ</ta>
            <ta e="T249" id="Seg_1725" s="T248">jakšə-zʼəʔ</ta>
            <ta e="T250" id="Seg_1726" s="T249">ej</ta>
            <ta e="T251" id="Seg_1727" s="T250">mĭ-bi-ndə</ta>
            <ta e="T252" id="Seg_1728" s="T251">bile-zʼəʔ</ta>
            <ta e="T253" id="Seg_1729" s="T252">i-li-m</ta>
            <ta e="T254" id="Seg_1730" s="T253">öʔ-le</ta>
            <ta e="T255" id="Seg_1731" s="T254">i-t</ta>
            <ta e="T256" id="Seg_1732" s="T255">kazər</ta>
            <ta e="T257" id="Seg_1733" s="T256">askər-i-m</ta>
            <ta e="T258" id="Seg_1734" s="T257">sejmü-t-sʼəʔ</ta>
            <ta e="T259" id="Seg_1735" s="T258">talə-j</ta>
            <ta e="T260" id="Seg_1736" s="T259">nʼeʔ-le-dən</ta>
            <ta e="T261" id="Seg_1737" s="T260">uʔbdə-lə-bi</ta>
            <ta e="T262" id="Seg_1738" s="T261">kazər</ta>
            <ta e="T263" id="Seg_1739" s="T262">askər-zaŋ-dən</ta>
            <ta e="T264" id="Seg_1740" s="T263">askər-iʔ</ta>
            <ta e="T265" id="Seg_1741" s="T264">tuno-laːndə-ga-iʔ</ta>
            <ta e="T266" id="Seg_1742" s="T265">sagər</ta>
            <ta e="T267" id="Seg_1743" s="T266">băra-bə</ta>
            <ta e="T268" id="Seg_1744" s="T267">tʼikə-bi</ta>
            <ta e="T269" id="Seg_1745" s="T268">konu-m</ta>
            <ta e="T270" id="Seg_1746" s="T269">uʔbdə-l-luʔ-bi</ta>
            <ta e="T271" id="Seg_1747" s="T270">urgaːba</ta>
            <ta e="T272" id="Seg_1748" s="T271">askər-i-m</ta>
            <ta e="T273" id="Seg_1749" s="T272">pʼaŋdəl-laʔ</ta>
            <ta e="T274" id="Seg_1750" s="T273">kun-naːm-bi</ta>
            <ta e="T275" id="Seg_1751" s="T274">uʔbdə-l-gut</ta>
            <ta e="T276" id="Seg_1752" s="T275">buga-i-m</ta>
            <ta e="T277" id="Seg_1753" s="T276">kazər-i-m</ta>
            <ta e="T278" id="Seg_1754" s="T277">sejmü-t-sʼəʔ</ta>
            <ta e="T279" id="Seg_1755" s="T278">tʼuga</ta>
            <ta e="T280" id="Seg_1756" s="T279">müʔ-leʔ</ta>
            <ta e="T281" id="Seg_1757" s="T280">kun-nə-dən</ta>
            <ta e="T282" id="Seg_1758" s="T281">sagər</ta>
            <ta e="T283" id="Seg_1759" s="T282">băra-bə</ta>
            <ta e="T284" id="Seg_1760" s="T283">tʼikə-bi</ta>
            <ta e="T285" id="Seg_1761" s="T284">kuš-tə</ta>
            <ta e="T286" id="Seg_1762" s="T285">öʔ-leʔ</ta>
            <ta e="T287" id="Seg_1763" s="T286">mĭ-bi</ta>
            <ta e="T288" id="Seg_1764" s="T287">kuš</ta>
            <ta e="T289" id="Seg_1765" s="T288">buga-i-m</ta>
            <ta e="T290" id="Seg_1766" s="T289">pʼaŋdə-laʔ</ta>
            <ta e="T291" id="Seg_1767" s="T290">kum-bi</ta>
            <ta e="T292" id="Seg_1768" s="T291">pa-n</ta>
            <ta e="T293" id="Seg_1769" s="T292">nĭ-gəndən</ta>
            <ta e="T294" id="Seg_1770" s="T293">kaːn</ta>
            <ta e="T295" id="Seg_1771" s="T294">maʔ-laʔ</ta>
            <ta e="T296" id="Seg_1772" s="T295">kojo-bi</ta>
            <ta e="T297" id="Seg_1773" s="T296">il</ta>
            <ta e="T298" id="Seg_1774" s="T297">oʔbdo-bi</ta>
            <ta e="T299" id="Seg_1775" s="T298">pardə-j</ta>
            <ta e="T300" id="Seg_1776" s="T299">i-š-peʔ</ta>
            <ta e="T301" id="Seg_1777" s="T300">dĭ-m</ta>
            <ta e="T302" id="Seg_1778" s="T301">par-əl-d-luʔ-bi-iʔ</ta>
            <ta e="T303" id="Seg_1779" s="T302">tʼabə-na</ta>
            <ta e="T304" id="Seg_1780" s="T303">mo-lia-iʔ</ta>
            <ta e="T305" id="Seg_1781" s="T304">sagər</ta>
            <ta e="T306" id="Seg_1782" s="T305">băra-bə</ta>
            <ta e="T307" id="Seg_1783" s="T306">tʼikə-j</ta>
            <ta e="T308" id="Seg_1784" s="T307">nʼeʔbdə-bi</ta>
            <ta e="T309" id="Seg_1785" s="T308">bü-bə</ta>
            <ta e="T310" id="Seg_1786" s="T309">kămnə-j</ta>
            <ta e="T311" id="Seg_1787" s="T310">baʔbdə-bi</ta>
            <ta e="T312" id="Seg_1788" s="T311">il-ə-m</ta>
            <ta e="T313" id="Seg_1789" s="T312">bü</ta>
            <ta e="T314" id="Seg_1790" s="T313">kun-naʔ</ta>
            <ta e="T315" id="Seg_1791" s="T314">kandə-ga-t</ta>
            <ta e="T316" id="Seg_1792" s="T315">kaːn</ta>
            <ta e="T317" id="Seg_1793" s="T316">kegərer-ie</ta>
            <ta e="T318" id="Seg_1794" s="T317">Tʼartʼa_bartʼa</ta>
            <ta e="T319" id="Seg_1795" s="T318">Pol_bartʼa</ta>
            <ta e="T320" id="Seg_1796" s="T319">pʼel</ta>
            <ta e="T321" id="Seg_1797" s="T320">mal-bə</ta>
            <ta e="T322" id="Seg_1798" s="T321">mĭ-li-m</ta>
            <ta e="T323" id="Seg_1799" s="T322">pʼel</ta>
            <ta e="T324" id="Seg_1800" s="T323">baj-bə</ta>
            <ta e="T325" id="Seg_1801" s="T324">mĭ-li-m</ta>
            <ta e="T326" id="Seg_1802" s="T325">koʔbdo-m</ta>
            <ta e="T327" id="Seg_1803" s="T326">mĭ-li-m</ta>
            <ta e="T328" id="Seg_1804" s="T327">ej</ta>
            <ta e="T329" id="Seg_1805" s="T328">kereʔ</ta>
            <ta e="T330" id="Seg_1806" s="T329">măna</ta>
            <ta e="T331" id="Seg_1807" s="T330">tăn</ta>
            <ta e="T332" id="Seg_1808" s="T331">mal-lə</ta>
            <ta e="T333" id="Seg_1809" s="T332">ej</ta>
            <ta e="T334" id="Seg_1810" s="T333">kereʔ</ta>
            <ta e="T335" id="Seg_1811" s="T334">măna</ta>
            <ta e="T336" id="Seg_1812" s="T335">tăn</ta>
            <ta e="T337" id="Seg_1813" s="T336">baj-lə</ta>
            <ta e="T338" id="Seg_1814" s="T337">toľko</ta>
            <ta e="T339" id="Seg_1815" s="T338">abakaj</ta>
            <ta e="T340" id="Seg_1816" s="T339">koʔbdo-l</ta>
            <ta e="T341" id="Seg_1817" s="T340">kereʔ</ta>
            <ta e="T342" id="Seg_1818" s="T341">măna</ta>
            <ta e="T343" id="Seg_1819" s="T342">i-bi</ta>
            <ta e="T344" id="Seg_1820" s="T343">šo-j</ta>
            <ta e="T345" id="Seg_1821" s="T344">tʼo-bə</ta>
            <ta e="T346" id="Seg_1822" s="T345">bü-bə</ta>
            <ta e="T347" id="Seg_1823" s="T346">suʔbdə-bi</ta>
            <ta e="T348" id="Seg_1824" s="T347">kaːn</ta>
            <ta e="T349" id="Seg_1825" s="T348">koʔbdo-bə</ta>
            <ta e="T350" id="Seg_1826" s="T349">mĭ-bi</ta>
            <ta e="T351" id="Seg_1827" s="T350">aksaʔ</ta>
            <ta e="T352" id="Seg_1828" s="T351">sejmü-ndə</ta>
            <ta e="T353" id="Seg_1829" s="T352">šĭ-l-də-bi</ta>
            <ta e="T354" id="Seg_1830" s="T353">bokul-laʔ</ta>
            <ta e="T355" id="Seg_1831" s="T354">uʔbdə-bi</ta>
            <ta e="T356" id="Seg_1832" s="T355">maʔ-gəndə</ta>
            <ta e="T357" id="Seg_1833" s="T356">kun-naːm-bi</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T111" id="Seg_1834" s="T110">bile</ta>
            <ta e="T112" id="Seg_1835" s="T111">kuza</ta>
            <ta e="T113" id="Seg_1836" s="T112">amno-bi</ta>
            <ta e="T114" id="Seg_1837" s="T113">nĭm-də</ta>
            <ta e="T115" id="Seg_1838" s="T114">tardʼža_bərdʼža</ta>
            <ta e="T116" id="Seg_1839" s="T115">bile</ta>
            <ta e="T117" id="Seg_1840" s="T116">sejmü-bə</ta>
            <ta e="T118" id="Seg_1841" s="T117">dʼabə-bi</ta>
            <ta e="T119" id="Seg_1842" s="T118">aksaʔ</ta>
            <ta e="T120" id="Seg_1843" s="T119">i-bi</ta>
            <ta e="T121" id="Seg_1844" s="T120">sejmü-t</ta>
            <ta e="T122" id="Seg_1845" s="T121">konzan-də-bi</ta>
            <ta e="T123" id="Seg_1846" s="T122">sagər</ta>
            <ta e="T124" id="Seg_1847" s="T123">băra-bə</ta>
            <ta e="T125" id="Seg_1848" s="T124">sar-bi</ta>
            <ta e="T126" id="Seg_1849" s="T125">šo-j</ta>
            <ta e="T127" id="Seg_1850" s="T126">tʼo-bə</ta>
            <ta e="T128" id="Seg_1851" s="T127">sar-bi</ta>
            <ta e="T129" id="Seg_1852" s="T128">mĭn-loʔbdə-bi</ta>
            <ta e="T130" id="Seg_1853" s="T129">mĭn-luʔbdə-bi</ta>
            <ta e="T131" id="Seg_1854" s="T130">urgaːba</ta>
            <ta e="T132" id="Seg_1855" s="T131">toʔbdo-bi</ta>
            <ta e="T133" id="Seg_1856" s="T132">măna</ta>
            <ta e="T134" id="Seg_1857" s="T133">i-t</ta>
            <ta e="T135" id="Seg_1858" s="T134">helaː-zittə</ta>
            <ta e="T136" id="Seg_1859" s="T135">păda-ʔ</ta>
            <ta e="T137" id="Seg_1860" s="T136">sagər</ta>
            <ta e="T138" id="Seg_1861" s="T137">băra-Tə</ta>
            <ta e="T139" id="Seg_1862" s="T138">mĭn-loʔbdə-bi</ta>
            <ta e="T140" id="Seg_1863" s="T139">mʼeŋgej</ta>
            <ta e="T141" id="Seg_1864" s="T140">toʔbdo-bi</ta>
            <ta e="T142" id="Seg_1865" s="T141">măna</ta>
            <ta e="T143" id="Seg_1866" s="T142">i-t</ta>
            <ta e="T144" id="Seg_1867" s="T143">helaː-zittə</ta>
            <ta e="T145" id="Seg_1868" s="T144">păda-ʔ</ta>
            <ta e="T146" id="Seg_1869" s="T145">sagər</ta>
            <ta e="T147" id="Seg_1870" s="T146">băra-Tə</ta>
            <ta e="T148" id="Seg_1871" s="T147">păda-də-bi</ta>
            <ta e="T149" id="Seg_1872" s="T148">sagər</ta>
            <ta e="T150" id="Seg_1873" s="T149">băra-Tə</ta>
            <ta e="T151" id="Seg_1874" s="T150">dĭgəttə</ta>
            <ta e="T152" id="Seg_1875" s="T151">kuš</ta>
            <ta e="T153" id="Seg_1876" s="T152">toʔbdo-bi</ta>
            <ta e="T154" id="Seg_1877" s="T153">i-ʔ</ta>
            <ta e="T155" id="Seg_1878" s="T154">helaː-zit</ta>
            <ta e="T156" id="Seg_1879" s="T155">păda-ʔ</ta>
            <ta e="T157" id="Seg_1880" s="T156">sagər</ta>
            <ta e="T158" id="Seg_1881" s="T157">băra-Tə</ta>
            <ta e="T159" id="Seg_1882" s="T158">dĭgəttə</ta>
            <ta e="T160" id="Seg_1883" s="T159">bü</ta>
            <ta e="T161" id="Seg_1884" s="T160">toʔbdo-bi</ta>
            <ta e="T162" id="Seg_1885" s="T161">dĭ</ta>
            <ta e="T163" id="Seg_1886" s="T162">bü-n</ta>
            <ta e="T164" id="Seg_1887" s="T163">suʔbdə-lAʔ</ta>
            <ta e="T165" id="Seg_1888" s="T164">i-bi</ta>
            <ta e="T166" id="Seg_1889" s="T165">sagər</ta>
            <ta e="T167" id="Seg_1890" s="T166">băra-gəndə</ta>
            <ta e="T168" id="Seg_1891" s="T167">šo-j</ta>
            <ta e="T169" id="Seg_1892" s="T168">tʼo-ziʔ</ta>
            <ta e="T170" id="Seg_1893" s="T169">suʔbdə-bi</ta>
            <ta e="T171" id="Seg_1894" s="T170">sejmü-gəndə</ta>
            <ta e="T172" id="Seg_1895" s="T171">sar-lAʔ</ta>
            <ta e="T173" id="Seg_1896" s="T172">baʔbdə-bi</ta>
            <ta e="T174" id="Seg_1897" s="T173">ku-bi-gəndə</ta>
            <ta e="T175" id="Seg_1898" s="T174">talai-gəndə</ta>
            <ta e="T176" id="Seg_1899" s="T175">toʔ-gəndə</ta>
            <ta e="T177" id="Seg_1900" s="T176">jada</ta>
            <ta e="T178" id="Seg_1901" s="T177">nu-gA</ta>
            <ta e="T179" id="Seg_1902" s="T178">dĭ</ta>
            <ta e="T180" id="Seg_1903" s="T179">jada-Tə</ta>
            <ta e="T181" id="Seg_1904" s="T180">ej</ta>
            <ta e="T182" id="Seg_1905" s="T181">tu-ga</ta>
            <ta e="T183" id="Seg_1906" s="T182">üzə-bi</ta>
            <ta e="T184" id="Seg_1907" s="T183">šü</ta>
            <ta e="T185" id="Seg_1908" s="T184">hen-bi</ta>
            <ta e="T186" id="Seg_1909" s="T185">sejmü-bə</ta>
            <ta e="T187" id="Seg_1910" s="T186">ötʼtʼer-bi</ta>
            <ta e="T188" id="Seg_1911" s="T187">amnə</ta>
            <ta e="T189" id="Seg_1912" s="T188">kaːn</ta>
            <ta e="T190" id="Seg_1913" s="T189">nʼiʔnen</ta>
            <ta e="T191" id="Seg_1914" s="T190">mĭn-laʔbə-bi</ta>
            <ta e="T192" id="Seg_1915" s="T191">ku-labaʔ-bi</ta>
            <ta e="T193" id="Seg_1916" s="T192">šində</ta>
            <ta e="T194" id="Seg_1917" s="T193">dĭgən</ta>
            <ta e="T195" id="Seg_1918" s="T194">amnə</ta>
            <ta e="T196" id="Seg_1919" s="T195">ine-bə</ta>
            <ta e="T197" id="Seg_1920" s="T196">ötʼtʼer-labaʔ-bi</ta>
            <ta e="T198" id="Seg_1921" s="T197">măn</ta>
            <ta e="T199" id="Seg_1922" s="T198">kük</ta>
            <ta e="T200" id="Seg_1923" s="T199">noʔ-bə</ta>
            <ta e="T201" id="Seg_1924" s="T200">tonə-l-laʔbə</ta>
            <ta e="T202" id="Seg_1925" s="T201">ej</ta>
            <ta e="T203" id="Seg_1926" s="T202">surar-ga</ta>
            <ta e="T204" id="Seg_1927" s="T203">toʔ-gəndə</ta>
            <ta e="T205" id="Seg_1928" s="T204">mĭn-də</ta>
            <ta e="T206" id="Seg_1929" s="T205">nʼekə-bə</ta>
            <ta e="T207" id="Seg_1930" s="T206">öʔ-laʔbə</ta>
            <ta e="T208" id="Seg_1931" s="T207">kan-lAʔ</ta>
            <ta e="T209" id="Seg_1932" s="T208">surar-t</ta>
            <ta e="T210" id="Seg_1933" s="T209">šində</ta>
            <ta e="T211" id="Seg_1934" s="T210">dĭrgit</ta>
            <ta e="T212" id="Seg_1935" s="T211">i-gA-l</ta>
            <ta e="T213" id="Seg_1936" s="T212">ej</ta>
            <ta e="T214" id="Seg_1937" s="T213">surar-ga</ta>
            <ta e="T215" id="Seg_1938" s="T214">kaːn-ə-m</ta>
            <ta e="T216" id="Seg_1939" s="T215">kaːn-ə-n</ta>
            <ta e="T217" id="Seg_1940" s="T216">noʔ-də</ta>
            <ta e="T218" id="Seg_1941" s="T217">tonə-laʔbə</ta>
            <ta e="T219" id="Seg_1942" s="T218">ĭmbi</ta>
            <ta e="T220" id="Seg_1943" s="T219">tănan</ta>
            <ta e="T221" id="Seg_1944" s="T220">kereʔ</ta>
            <ta e="T222" id="Seg_1945" s="T221">kan-bi</ta>
            <ta e="T223" id="Seg_1946" s="T222">surar-laʔbə</ta>
            <ta e="T224" id="Seg_1947" s="T223">kajət</ta>
            <ta e="T225" id="Seg_1948" s="T224">kuza</ta>
            <ta e="T226" id="Seg_1949" s="T225">i-gA-l</ta>
            <ta e="T227" id="Seg_1950" s="T226">ĭmbi-m</ta>
            <ta e="T228" id="Seg_1951" s="T227">pi-lAʔ</ta>
            <ta e="T229" id="Seg_1952" s="T228">mĭn-gA-l</ta>
            <ta e="T230" id="Seg_1953" s="T229">kaːn-ə-n</ta>
            <ta e="T231" id="Seg_1954" s="T230">koʔbdo-bə</ta>
            <ta e="T232" id="Seg_1955" s="T231">i-zittə</ta>
            <ta e="T233" id="Seg_1956" s="T232">šo-bi-m</ta>
            <ta e="T234" id="Seg_1957" s="T233">dĭ</ta>
            <ta e="T235" id="Seg_1958" s="T234">kuza</ta>
            <ta e="T236" id="Seg_1959" s="T235">par-bi</ta>
            <ta e="T237" id="Seg_1960" s="T236">kaːn-Tə</ta>
            <ta e="T238" id="Seg_1961" s="T237">nörbə-bi</ta>
            <ta e="T239" id="Seg_1962" s="T238">kaːn-ə-n</ta>
            <ta e="T240" id="Seg_1963" s="T239">koʔbdo-bə</ta>
            <ta e="T241" id="Seg_1964" s="T240">i-zittə</ta>
            <ta e="T242" id="Seg_1965" s="T241">šo-bi-m</ta>
            <ta e="T243" id="Seg_1966" s="T242">kan-lAʔ</ta>
            <ta e="T244" id="Seg_1967" s="T243">nörbə-ʔ</ta>
            <ta e="T245" id="Seg_1968" s="T244">jakšə-l-ziʔ</ta>
            <ta e="T246" id="Seg_1969" s="T245">par-ə-ʔ</ta>
            <ta e="T247" id="Seg_1970" s="T246">e-m</ta>
            <ta e="T248" id="Seg_1971" s="T247">par-ə-ʔ</ta>
            <ta e="T249" id="Seg_1972" s="T248">jakšə-ziʔ</ta>
            <ta e="T250" id="Seg_1973" s="T249">ej</ta>
            <ta e="T251" id="Seg_1974" s="T250">mĭ-bi-gəndə</ta>
            <ta e="T252" id="Seg_1975" s="T251">bile-ziʔ</ta>
            <ta e="T253" id="Seg_1976" s="T252">i-lV-m</ta>
            <ta e="T254" id="Seg_1977" s="T253">öʔ-lAʔ</ta>
            <ta e="T255" id="Seg_1978" s="T254">i-t</ta>
            <ta e="T256" id="Seg_1979" s="T255">kazər</ta>
            <ta e="T257" id="Seg_1980" s="T256">askər-jəʔ-m</ta>
            <ta e="T258" id="Seg_1981" s="T257">sejmü-t-ziʔ</ta>
            <ta e="T259" id="Seg_1982" s="T258">talə-j</ta>
            <ta e="T260" id="Seg_1983" s="T259">nʼeʔbdə-lV-dən</ta>
            <ta e="T261" id="Seg_1984" s="T260">uʔbdə-lə-bi</ta>
            <ta e="T262" id="Seg_1985" s="T261">kazər</ta>
            <ta e="T263" id="Seg_1986" s="T262">askər-zAŋ-dən</ta>
            <ta e="T264" id="Seg_1987" s="T263">askər-jəʔ</ta>
            <ta e="T265" id="Seg_1988" s="T264">tuno-laːndə-gA-jəʔ</ta>
            <ta e="T266" id="Seg_1989" s="T265">sagər</ta>
            <ta e="T267" id="Seg_1990" s="T266">băra-bə</ta>
            <ta e="T268" id="Seg_1991" s="T267">tʼikə-bi</ta>
            <ta e="T269" id="Seg_1992" s="T268">konu-m</ta>
            <ta e="T270" id="Seg_1993" s="T269">uʔbdə-lə-luʔbdə-bi</ta>
            <ta e="T271" id="Seg_1994" s="T270">urgaːba</ta>
            <ta e="T272" id="Seg_1995" s="T271">askər-jəʔ-m</ta>
            <ta e="T273" id="Seg_1996" s="T272">pʼaŋdəl-lAʔ</ta>
            <ta e="T274" id="Seg_1997" s="T273">kun-laːm-bi</ta>
            <ta e="T275" id="Seg_1998" s="T274">uʔbdə-lə-Kut</ta>
            <ta e="T276" id="Seg_1999" s="T275">buga-jəʔ-m</ta>
            <ta e="T277" id="Seg_2000" s="T276">kazər-jəʔ-m</ta>
            <ta e="T278" id="Seg_2001" s="T277">sejmü-t-ziʔ</ta>
            <ta e="T279" id="Seg_2002" s="T278">tʼuga</ta>
            <ta e="T280" id="Seg_2003" s="T279">müʔbdə-lAʔ</ta>
            <ta e="T281" id="Seg_2004" s="T280">kun-lV-dən</ta>
            <ta e="T282" id="Seg_2005" s="T281">sagər</ta>
            <ta e="T283" id="Seg_2006" s="T282">băra-bə</ta>
            <ta e="T284" id="Seg_2007" s="T283">tʼikə-bi</ta>
            <ta e="T285" id="Seg_2008" s="T284">kuš-də</ta>
            <ta e="T286" id="Seg_2009" s="T285">öʔ-lAʔ</ta>
            <ta e="T287" id="Seg_2010" s="T286">mĭ-bi</ta>
            <ta e="T288" id="Seg_2011" s="T287">kuš</ta>
            <ta e="T289" id="Seg_2012" s="T288">buga-jəʔ-m</ta>
            <ta e="T290" id="Seg_2013" s="T289">pʼaŋdə-lAʔ</ta>
            <ta e="T291" id="Seg_2014" s="T290">kun-bi</ta>
            <ta e="T292" id="Seg_2015" s="T291">pa-n</ta>
            <ta e="T293" id="Seg_2016" s="T292">nĭ-gəndən</ta>
            <ta e="T294" id="Seg_2017" s="T293">kaːn</ta>
            <ta e="T295" id="Seg_2018" s="T294">ma-lAʔ</ta>
            <ta e="T296" id="Seg_2019" s="T295">kojo-bi</ta>
            <ta e="T297" id="Seg_2020" s="T296">il</ta>
            <ta e="T298" id="Seg_2021" s="T297">oʔbdo-bi</ta>
            <ta e="T299" id="Seg_2022" s="T298">pardə-j</ta>
            <ta e="T300" id="Seg_2023" s="T299">i-šə-bAʔ</ta>
            <ta e="T301" id="Seg_2024" s="T300">dĭ-m</ta>
            <ta e="T302" id="Seg_2025" s="T301">par-lə-də-luʔbdə-bi-jəʔ</ta>
            <ta e="T303" id="Seg_2026" s="T302">tʼabə-NTA</ta>
            <ta e="T304" id="Seg_2027" s="T303">mo-liA-jəʔ</ta>
            <ta e="T305" id="Seg_2028" s="T304">sagər</ta>
            <ta e="T306" id="Seg_2029" s="T305">băra-bə</ta>
            <ta e="T307" id="Seg_2030" s="T306">tʼikə-j</ta>
            <ta e="T308" id="Seg_2031" s="T307">nʼeʔbdə-bi</ta>
            <ta e="T309" id="Seg_2032" s="T308">bü-bə</ta>
            <ta e="T310" id="Seg_2033" s="T309">kămnə-j</ta>
            <ta e="T311" id="Seg_2034" s="T310">baʔbdə-bi</ta>
            <ta e="T312" id="Seg_2035" s="T311">il-ə-m</ta>
            <ta e="T313" id="Seg_2036" s="T312">bü</ta>
            <ta e="T314" id="Seg_2037" s="T313">kun-lAʔ</ta>
            <ta e="T315" id="Seg_2038" s="T314">kandə-gA-t</ta>
            <ta e="T316" id="Seg_2039" s="T315">kaːn</ta>
            <ta e="T317" id="Seg_2040" s="T316">kegərer-liA</ta>
            <ta e="T318" id="Seg_2041" s="T317">tardʼža_bərdʼža</ta>
            <ta e="T319" id="Seg_2042" s="T318">pol_bartʼa</ta>
            <ta e="T320" id="Seg_2043" s="T319">pʼel</ta>
            <ta e="T321" id="Seg_2044" s="T320">mal-m</ta>
            <ta e="T322" id="Seg_2045" s="T321">mĭ-lV-m</ta>
            <ta e="T323" id="Seg_2046" s="T322">pʼel</ta>
            <ta e="T324" id="Seg_2047" s="T323">baj-m</ta>
            <ta e="T325" id="Seg_2048" s="T324">mĭ-lV-m</ta>
            <ta e="T326" id="Seg_2049" s="T325">koʔbdo-m</ta>
            <ta e="T327" id="Seg_2050" s="T326">mĭ-lV-m</ta>
            <ta e="T328" id="Seg_2051" s="T327">ej</ta>
            <ta e="T329" id="Seg_2052" s="T328">kereʔ</ta>
            <ta e="T330" id="Seg_2053" s="T329">măna</ta>
            <ta e="T331" id="Seg_2054" s="T330">tăn</ta>
            <ta e="T332" id="Seg_2055" s="T331">mal-l</ta>
            <ta e="T333" id="Seg_2056" s="T332">ej</ta>
            <ta e="T334" id="Seg_2057" s="T333">kereʔ</ta>
            <ta e="T335" id="Seg_2058" s="T334">măna</ta>
            <ta e="T336" id="Seg_2059" s="T335">tăn</ta>
            <ta e="T337" id="Seg_2060" s="T336">baj-l</ta>
            <ta e="T338" id="Seg_2061" s="T337">toľko</ta>
            <ta e="T339" id="Seg_2062" s="T338">abakaj</ta>
            <ta e="T340" id="Seg_2063" s="T339">koʔbdo-l</ta>
            <ta e="T341" id="Seg_2064" s="T340">kereʔ</ta>
            <ta e="T342" id="Seg_2065" s="T341">măna</ta>
            <ta e="T343" id="Seg_2066" s="T342">i-bi</ta>
            <ta e="T344" id="Seg_2067" s="T343">šo-j</ta>
            <ta e="T345" id="Seg_2068" s="T344">tʼo-bə</ta>
            <ta e="T346" id="Seg_2069" s="T345">bü-bə</ta>
            <ta e="T347" id="Seg_2070" s="T346">suʔbdə-bi</ta>
            <ta e="T348" id="Seg_2071" s="T347">kaːn</ta>
            <ta e="T349" id="Seg_2072" s="T348">koʔbdo-bə</ta>
            <ta e="T350" id="Seg_2073" s="T349">mĭ-bi</ta>
            <ta e="T351" id="Seg_2074" s="T350">aksaʔ</ta>
            <ta e="T352" id="Seg_2075" s="T351">sejmü-gəndə</ta>
            <ta e="T353" id="Seg_2076" s="T352">šĭ-lə-də-bi</ta>
            <ta e="T354" id="Seg_2077" s="T353">bokuʔ-lAʔ</ta>
            <ta e="T355" id="Seg_2078" s="T354">uʔbdə-bi</ta>
            <ta e="T356" id="Seg_2079" s="T355">maʔ-gəndə</ta>
            <ta e="T357" id="Seg_2080" s="T356">kun-laːm-bi</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T111" id="Seg_2081" s="T110">poor.[NOM.SG]</ta>
            <ta e="T112" id="Seg_2082" s="T111">man.[NOM.SG]</ta>
            <ta e="T113" id="Seg_2083" s="T112">live-PST.[3SG]</ta>
            <ta e="T114" id="Seg_2084" s="T113">name-NOM/GEN/ACC.3SG</ta>
            <ta e="T115" id="Seg_2085" s="T114">Tardzha_Bardzha.[NOM.SG]</ta>
            <ta e="T116" id="Seg_2086" s="T115">bad.[NOM.SG]</ta>
            <ta e="T117" id="Seg_2087" s="T116">mare-ACC.3SG</ta>
            <ta e="T118" id="Seg_2088" s="T117">capture-PST.[3SG]</ta>
            <ta e="T119" id="Seg_2089" s="T118">lame.[NOM.SG]</ta>
            <ta e="T120" id="Seg_2090" s="T119">be-PST.[3SG]</ta>
            <ta e="T121" id="Seg_2091" s="T120">mare-NOM/GEN.3SG</ta>
            <ta e="T122" id="Seg_2092" s="T121">saddle-TR-PST.[3SG]</ta>
            <ta e="T123" id="Seg_2093" s="T122">black.[NOM.SG]</ta>
            <ta e="T124" id="Seg_2094" s="T123">sack-ACC.3SG</ta>
            <ta e="T125" id="Seg_2095" s="T124">bind-PST.[3SG]</ta>
            <ta e="T126" id="Seg_2096" s="T125">birchbark-ADJZ.[NOM.SG]</ta>
            <ta e="T127" id="Seg_2097" s="T126">container-ACC.3SG</ta>
            <ta e="T128" id="Seg_2098" s="T127">bind-PST.[3SG]</ta>
            <ta e="T129" id="Seg_2099" s="T128">go-INCH-PST.[3SG]</ta>
            <ta e="T130" id="Seg_2100" s="T129">go-MOM-PST.[3SG]</ta>
            <ta e="T131" id="Seg_2101" s="T130">bear.[NOM.SG]</ta>
            <ta e="T132" id="Seg_2102" s="T131">come.up-PST.[3SG]</ta>
            <ta e="T133" id="Seg_2103" s="T132">I.ACC</ta>
            <ta e="T134" id="Seg_2104" s="T133">take-IMP.2SG.O</ta>
            <ta e="T135" id="Seg_2105" s="T134">make.friends-INF.LAT</ta>
            <ta e="T136" id="Seg_2106" s="T135">creep.into-IMP.2SG</ta>
            <ta e="T137" id="Seg_2107" s="T136">black.[NOM.SG]</ta>
            <ta e="T138" id="Seg_2108" s="T137">sack-LAT</ta>
            <ta e="T139" id="Seg_2109" s="T138">go-INCH-PST.[3SG]</ta>
            <ta e="T140" id="Seg_2110" s="T139">fox.[NOM.SG]</ta>
            <ta e="T141" id="Seg_2111" s="T140">come.up-PST.[3SG]</ta>
            <ta e="T142" id="Seg_2112" s="T141">I.ACC</ta>
            <ta e="T143" id="Seg_2113" s="T142">take-IMP.2SG.O</ta>
            <ta e="T144" id="Seg_2114" s="T143">make.friends-INF.LAT</ta>
            <ta e="T145" id="Seg_2115" s="T144">creep.into-IMP.2SG</ta>
            <ta e="T146" id="Seg_2116" s="T145">black.[NOM.SG]</ta>
            <ta e="T147" id="Seg_2117" s="T146">sack-LAT</ta>
            <ta e="T148" id="Seg_2118" s="T147">creep.into-TR-PST.[3SG]</ta>
            <ta e="T149" id="Seg_2119" s="T148">black.[NOM.SG]</ta>
            <ta e="T150" id="Seg_2120" s="T149">sack-LAT</ta>
            <ta e="T151" id="Seg_2121" s="T150">then</ta>
            <ta e="T152" id="Seg_2122" s="T151">wolf.[NOM.SG]</ta>
            <ta e="T153" id="Seg_2123" s="T152">come.up-PST.[3SG]</ta>
            <ta e="T154" id="Seg_2124" s="T153">take-IMP.2SG</ta>
            <ta e="T155" id="Seg_2125" s="T154">make.friends-INF</ta>
            <ta e="T156" id="Seg_2126" s="T155">creep.into-IMP.2SG</ta>
            <ta e="T157" id="Seg_2127" s="T156">black.[NOM.SG]</ta>
            <ta e="T158" id="Seg_2128" s="T157">sack-LAT</ta>
            <ta e="T159" id="Seg_2129" s="T158">then</ta>
            <ta e="T160" id="Seg_2130" s="T159">water.[NOM.SG]</ta>
            <ta e="T161" id="Seg_2131" s="T160">come.up-PST.[3SG]</ta>
            <ta e="T162" id="Seg_2132" s="T161">this.[NOM.SG]</ta>
            <ta e="T163" id="Seg_2133" s="T162">water-GEN</ta>
            <ta e="T164" id="Seg_2134" s="T163">scoop-CVB</ta>
            <ta e="T165" id="Seg_2135" s="T164">take-PST.[3SG]</ta>
            <ta e="T166" id="Seg_2136" s="T165">black.[NOM.SG]</ta>
            <ta e="T167" id="Seg_2137" s="T166">sack-LAT/LOC.3SG</ta>
            <ta e="T168" id="Seg_2138" s="T167">birchbark-ADJZ.[NOM.SG]</ta>
            <ta e="T169" id="Seg_2139" s="T168">container-INS</ta>
            <ta e="T170" id="Seg_2140" s="T169">scoop-PST.[3SG]</ta>
            <ta e="T171" id="Seg_2141" s="T170">mare-LAT/LOC.3SG</ta>
            <ta e="T172" id="Seg_2142" s="T171">bind-CVB</ta>
            <ta e="T173" id="Seg_2143" s="T172">throw-PST.[3SG]</ta>
            <ta e="T174" id="Seg_2144" s="T173">see-CVB.TEMP-LAT/LOC.3SG</ta>
            <ta e="T175" id="Seg_2145" s="T174">sea-LAT/LOC.3SG</ta>
            <ta e="T176" id="Seg_2146" s="T175">shore-LAT/LOC.3SG</ta>
            <ta e="T177" id="Seg_2147" s="T176">village.[NOM.SG]</ta>
            <ta e="T178" id="Seg_2148" s="T177">stand-PRS.[3SG]</ta>
            <ta e="T179" id="Seg_2149" s="T178">this.[NOM.SG]</ta>
            <ta e="T180" id="Seg_2150" s="T179">village-LAT</ta>
            <ta e="T181" id="Seg_2151" s="T180">NEG</ta>
            <ta e="T182" id="Seg_2152" s="T181">come-PTCP.PRS</ta>
            <ta e="T183" id="Seg_2153" s="T182">descend-PST.[3SG]</ta>
            <ta e="T184" id="Seg_2154" s="T183">fire.[NOM.SG]</ta>
            <ta e="T185" id="Seg_2155" s="T184">put-PST.[3SG]</ta>
            <ta e="T186" id="Seg_2156" s="T185">mare-ACC.3SG</ta>
            <ta e="T187" id="Seg_2157" s="T186">hobble-PST.[3SG]</ta>
            <ta e="T188" id="Seg_2158" s="T187">sit.[3SG]</ta>
            <ta e="T189" id="Seg_2159" s="T188">khan.[NOM.SG]</ta>
            <ta e="T190" id="Seg_2160" s="T189">outside</ta>
            <ta e="T191" id="Seg_2161" s="T190">go-DUR-PST.[3SG]</ta>
            <ta e="T192" id="Seg_2162" s="T191">see-RES-PST.[3SG]</ta>
            <ta e="T193" id="Seg_2163" s="T192">who.[NOM.SG]</ta>
            <ta e="T194" id="Seg_2164" s="T193">there</ta>
            <ta e="T195" id="Seg_2165" s="T194">sit.[3SG]</ta>
            <ta e="T196" id="Seg_2166" s="T195">horse-ACC.3SG</ta>
            <ta e="T197" id="Seg_2167" s="T196">hobble-RES-PST.[3SG]</ta>
            <ta e="T198" id="Seg_2168" s="T197">I.GEN</ta>
            <ta e="T199" id="Seg_2169" s="T198">green.[NOM.SG]</ta>
            <ta e="T200" id="Seg_2170" s="T199">grass-ACC.3SG</ta>
            <ta e="T201" id="Seg_2171" s="T200">tread-FRQ-DUR.[3SG]</ta>
            <ta e="T202" id="Seg_2172" s="T201">NEG</ta>
            <ta e="T203" id="Seg_2173" s="T202">ask-PTCP.PRS</ta>
            <ta e="T204" id="Seg_2174" s="T203">shore-LAT/LOC.3SG</ta>
            <ta e="T205" id="Seg_2175" s="T204">go-PTCP.PRS</ta>
            <ta e="T206" id="Seg_2176" s="T205">boy-ACC.3SG</ta>
            <ta e="T207" id="Seg_2177" s="T206">let-DUR.[3SG]</ta>
            <ta e="T208" id="Seg_2178" s="T207">go-CVB</ta>
            <ta e="T209" id="Seg_2179" s="T208">ask-IMP.2SG.O</ta>
            <ta e="T210" id="Seg_2180" s="T209">who.[NOM.SG]</ta>
            <ta e="T211" id="Seg_2181" s="T210">such.[NOM.SG]</ta>
            <ta e="T212" id="Seg_2182" s="T211">be-PRS-2SG</ta>
            <ta e="T213" id="Seg_2183" s="T212">NEG</ta>
            <ta e="T214" id="Seg_2184" s="T213">ask-PTCP.PRS</ta>
            <ta e="T215" id="Seg_2185" s="T214">khan-EP-ACC</ta>
            <ta e="T216" id="Seg_2186" s="T215">khan-EP-GEN</ta>
            <ta e="T217" id="Seg_2187" s="T216">grass-NOM/GEN/ACC.3SG</ta>
            <ta e="T218" id="Seg_2188" s="T217">tread-DUR.[3SG]</ta>
            <ta e="T219" id="Seg_2189" s="T218">what</ta>
            <ta e="T220" id="Seg_2190" s="T219">you.DAT</ta>
            <ta e="T221" id="Seg_2191" s="T220">one.needs</ta>
            <ta e="T222" id="Seg_2192" s="T221">go-PST.[3SG]</ta>
            <ta e="T223" id="Seg_2193" s="T222">ask-DUR.[3SG]</ta>
            <ta e="T224" id="Seg_2194" s="T223">what.kind</ta>
            <ta e="T225" id="Seg_2195" s="T224">man.[NOM.SG]</ta>
            <ta e="T226" id="Seg_2196" s="T225">be-PRS-2SG</ta>
            <ta e="T227" id="Seg_2197" s="T226">what-ACC</ta>
            <ta e="T228" id="Seg_2198" s="T227">look.for-CVB</ta>
            <ta e="T229" id="Seg_2199" s="T228">go-PRS-2SG</ta>
            <ta e="T230" id="Seg_2200" s="T229">khan-EP-GEN</ta>
            <ta e="T231" id="Seg_2201" s="T230">daughter-ACC.3SG</ta>
            <ta e="T232" id="Seg_2202" s="T231">take-INF.LAT</ta>
            <ta e="T233" id="Seg_2203" s="T232">come-PST-1SG</ta>
            <ta e="T234" id="Seg_2204" s="T233">this.[NOM.SG]</ta>
            <ta e="T235" id="Seg_2205" s="T234">man.[NOM.SG]</ta>
            <ta e="T236" id="Seg_2206" s="T235">return-PST.[3SG]</ta>
            <ta e="T237" id="Seg_2207" s="T236">khan-LAT</ta>
            <ta e="T238" id="Seg_2208" s="T237">tell-PST.[3SG]</ta>
            <ta e="T239" id="Seg_2209" s="T238">khan-EP-GEN</ta>
            <ta e="T240" id="Seg_2210" s="T239">daughter-ACC.3SG</ta>
            <ta e="T241" id="Seg_2211" s="T240">take-INF.LAT</ta>
            <ta e="T242" id="Seg_2212" s="T241">come-PST-1SG</ta>
            <ta e="T243" id="Seg_2213" s="T242">go-CVB</ta>
            <ta e="T244" id="Seg_2214" s="T243">tell-IMP.2SG</ta>
            <ta e="T245" id="Seg_2215" s="T244">good-NOM/GEN/ACC.2SG-INS</ta>
            <ta e="T246" id="Seg_2216" s="T245">return-EP-IMP.2SG</ta>
            <ta e="T247" id="Seg_2217" s="T246">NEG.AUX-1SG</ta>
            <ta e="T248" id="Seg_2218" s="T247">return-EP-CNG</ta>
            <ta e="T249" id="Seg_2219" s="T248">good-INS</ta>
            <ta e="T250" id="Seg_2220" s="T249">NEG</ta>
            <ta e="T251" id="Seg_2221" s="T250">give-CVB.TEMP-LAT/LOC.3SG</ta>
            <ta e="T252" id="Seg_2222" s="T251">bad-INS</ta>
            <ta e="T253" id="Seg_2223" s="T252">take-FUT-1SG</ta>
            <ta e="T254" id="Seg_2224" s="T253">let-CVB</ta>
            <ta e="T255" id="Seg_2225" s="T254">take-IMP.2SG.O</ta>
            <ta e="T256" id="Seg_2226" s="T255">wild.[NOM.SG]</ta>
            <ta e="T257" id="Seg_2227" s="T256">stallion-PL-ACC</ta>
            <ta e="T258" id="Seg_2228" s="T257">mare-3SG-INS</ta>
            <ta e="T259" id="Seg_2229" s="T258">tear.apart-CVB</ta>
            <ta e="T260" id="Seg_2230" s="T259">pull-FUT-3PL.O</ta>
            <ta e="T261" id="Seg_2231" s="T260">get.up-TR-PST.[3SG]</ta>
            <ta e="T262" id="Seg_2232" s="T261">wild.[NOM.SG]</ta>
            <ta e="T263" id="Seg_2233" s="T262">stallion-PL-NOM/GEN/ACC.3PL</ta>
            <ta e="T264" id="Seg_2234" s="T263">stallion-PL</ta>
            <ta e="T265" id="Seg_2235" s="T264">gallop-DUR-PRS-3PL</ta>
            <ta e="T266" id="Seg_2236" s="T265">black.[NOM.SG]</ta>
            <ta e="T267" id="Seg_2237" s="T266">sack-ACC.3SG</ta>
            <ta e="T268" id="Seg_2238" s="T267">tie.loose-PST.[3SG]</ta>
            <ta e="T269" id="Seg_2239" s="T268">bear-ACC</ta>
            <ta e="T270" id="Seg_2240" s="T269">get.up-TR-MOM-PST.[3SG]</ta>
            <ta e="T271" id="Seg_2241" s="T270">bear.[NOM.SG]</ta>
            <ta e="T272" id="Seg_2242" s="T271">stallion-PL-ACC</ta>
            <ta e="T273" id="Seg_2243" s="T272">chase-CVB</ta>
            <ta e="T274" id="Seg_2244" s="T273">bring-RES-PST.[3SG]</ta>
            <ta e="T275" id="Seg_2245" s="T274">get.up-TR-IMP.2PL.O</ta>
            <ta e="T276" id="Seg_2246" s="T275">bull-PL-ACC</ta>
            <ta e="T277" id="Seg_2247" s="T276">wild-PL-ACC</ta>
            <ta e="T278" id="Seg_2248" s="T277">mare-3SG-INS</ta>
            <ta e="T279" id="Seg_2249" s="T278">dead.[NOM.SG]</ta>
            <ta e="T280" id="Seg_2250" s="T279">punch-CVB</ta>
            <ta e="T281" id="Seg_2251" s="T280">bring-FUT-3PL.O</ta>
            <ta e="T282" id="Seg_2252" s="T281">black.[NOM.SG]</ta>
            <ta e="T283" id="Seg_2253" s="T282">sack-ACC.3SG</ta>
            <ta e="T284" id="Seg_2254" s="T283">tie.loose-PST.[3SG]</ta>
            <ta e="T285" id="Seg_2255" s="T284">wolf-NOM/GEN/ACC.3SG</ta>
            <ta e="T286" id="Seg_2256" s="T285">let-CVB</ta>
            <ta e="T287" id="Seg_2257" s="T286">give-PST.[3SG]</ta>
            <ta e="T288" id="Seg_2258" s="T287">wolf.[NOM.SG]</ta>
            <ta e="T289" id="Seg_2259" s="T288">bull-PL-ACC</ta>
            <ta e="T290" id="Seg_2260" s="T289">press-CVB</ta>
            <ta e="T291" id="Seg_2261" s="T290">bring-PST.[3SG]</ta>
            <ta e="T292" id="Seg_2262" s="T291">tree-GEN</ta>
            <ta e="T293" id="Seg_2263" s="T292">top-LAT/LOC.3PL</ta>
            <ta e="T294" id="Seg_2264" s="T293">khan.[NOM.SG]</ta>
            <ta e="T295" id="Seg_2265" s="T294">stay-CVB</ta>
            <ta e="T296" id="Seg_2266" s="T295">stay-PST.[3SG]</ta>
            <ta e="T297" id="Seg_2267" s="T296">people.[NOM.SG]</ta>
            <ta e="T298" id="Seg_2268" s="T297">gather-PST.[3SG]</ta>
            <ta e="T299" id="Seg_2269" s="T298">surround-CVB</ta>
            <ta e="T300" id="Seg_2270" s="T299">take-OPT.DU/PL-1PL</ta>
            <ta e="T301" id="Seg_2271" s="T300">this-ACC</ta>
            <ta e="T302" id="Seg_2272" s="T301">return-TR-TR-MOM-PST-3PL</ta>
            <ta e="T303" id="Seg_2273" s="T302">seize-PTCP</ta>
            <ta e="T304" id="Seg_2274" s="T303">want-PRS-3PL</ta>
            <ta e="T305" id="Seg_2275" s="T304">black.[NOM.SG]</ta>
            <ta e="T306" id="Seg_2276" s="T305">sack-ACC.3SG</ta>
            <ta e="T307" id="Seg_2277" s="T306">tie.loose-CVB</ta>
            <ta e="T308" id="Seg_2278" s="T307">pull-PST.[3SG]</ta>
            <ta e="T309" id="Seg_2279" s="T308">water-ACC.3SG</ta>
            <ta e="T310" id="Seg_2280" s="T309">pour-CVB</ta>
            <ta e="T311" id="Seg_2281" s="T310">throw-PST.[3SG]</ta>
            <ta e="T312" id="Seg_2282" s="T311">people-EP-ACC</ta>
            <ta e="T313" id="Seg_2283" s="T312">water.[NOM.SG]</ta>
            <ta e="T314" id="Seg_2284" s="T313">bring-CVB</ta>
            <ta e="T315" id="Seg_2285" s="T314">walk-PRS-3SG.O</ta>
            <ta e="T316" id="Seg_2286" s="T315">khan.[NOM.SG]</ta>
            <ta e="T317" id="Seg_2287" s="T316">call.out-PRS.[3SG]</ta>
            <ta e="T318" id="Seg_2288" s="T317">Tardzha_Bardzha</ta>
            <ta e="T319" id="Seg_2289" s="T318">Pol_Bardzha</ta>
            <ta e="T320" id="Seg_2290" s="T319">half</ta>
            <ta e="T321" id="Seg_2291" s="T320">cattle-NOM/GEN/ACC.1SG</ta>
            <ta e="T322" id="Seg_2292" s="T321">give-FUT-1SG</ta>
            <ta e="T323" id="Seg_2293" s="T322">half</ta>
            <ta e="T324" id="Seg_2294" s="T323">wealth-NOM/GEN/ACC.1SG</ta>
            <ta e="T325" id="Seg_2295" s="T324">give-FUT-1SG</ta>
            <ta e="T326" id="Seg_2296" s="T325">daughter-NOM/GEN/ACC.1SG</ta>
            <ta e="T327" id="Seg_2297" s="T326">give-FUT-1SG</ta>
            <ta e="T328" id="Seg_2298" s="T327">NEG</ta>
            <ta e="T329" id="Seg_2299" s="T328">one.needs</ta>
            <ta e="T330" id="Seg_2300" s="T329">I.LAT</ta>
            <ta e="T331" id="Seg_2301" s="T330">you.GEN</ta>
            <ta e="T332" id="Seg_2302" s="T331">cattle-NOM/GEN/ACC.2SG</ta>
            <ta e="T333" id="Seg_2303" s="T332">NEG</ta>
            <ta e="T334" id="Seg_2304" s="T333">one.needs</ta>
            <ta e="T335" id="Seg_2305" s="T334">I.LAT</ta>
            <ta e="T336" id="Seg_2306" s="T335">you.GEN</ta>
            <ta e="T337" id="Seg_2307" s="T336">wealth-NOM/GEN/ACC.2SG</ta>
            <ta e="T338" id="Seg_2308" s="T337">only</ta>
            <ta e="T339" id="Seg_2309" s="T338">princess.[NOM.SG]</ta>
            <ta e="T340" id="Seg_2310" s="T339">daughter-NOM/GEN/ACC.2SG</ta>
            <ta e="T341" id="Seg_2311" s="T340">one.needs</ta>
            <ta e="T342" id="Seg_2312" s="T341">I.LAT</ta>
            <ta e="T343" id="Seg_2313" s="T342">take-PST.[3SG]</ta>
            <ta e="T344" id="Seg_2314" s="T343">birchbark-ADJZ.[NOM.SG]</ta>
            <ta e="T345" id="Seg_2315" s="T344">container-ACC.3SG</ta>
            <ta e="T346" id="Seg_2316" s="T345">water-ACC.3SG</ta>
            <ta e="T347" id="Seg_2317" s="T346">scoop-PST.[3SG]</ta>
            <ta e="T348" id="Seg_2318" s="T347">khan.[NOM.SG]</ta>
            <ta e="T349" id="Seg_2319" s="T348">daughter-ACC.3SG</ta>
            <ta e="T350" id="Seg_2320" s="T349">give-PST.[3SG]</ta>
            <ta e="T351" id="Seg_2321" s="T350">lame.[NOM.SG]</ta>
            <ta e="T352" id="Seg_2322" s="T351">mare-LAT/LOC.3SG</ta>
            <ta e="T353" id="Seg_2323" s="T352">mount-TR-TR-PST.[3SG]</ta>
            <ta e="T354" id="Seg_2324" s="T353">tie.up-CVB</ta>
            <ta e="T355" id="Seg_2325" s="T354">get.up-PST.[3SG]</ta>
            <ta e="T356" id="Seg_2326" s="T355">tent-LAT/LOC.3SG</ta>
            <ta e="T357" id="Seg_2327" s="T356">bring-RES-PST.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T111" id="Seg_2328" s="T110">бедный.[NOM.SG]</ta>
            <ta e="T112" id="Seg_2329" s="T111">мужчина.[NOM.SG]</ta>
            <ta e="T113" id="Seg_2330" s="T112">жить-PST.[3SG]</ta>
            <ta e="T114" id="Seg_2331" s="T113">имя-NOM/GEN/ACC.3SG</ta>
            <ta e="T115" id="Seg_2332" s="T114">Тарджа_Барджа.[NOM.SG]</ta>
            <ta e="T116" id="Seg_2333" s="T115">плохой.[NOM.SG]</ta>
            <ta e="T117" id="Seg_2334" s="T116">кобыла-ACC.3SG</ta>
            <ta e="T118" id="Seg_2335" s="T117">ловить-PST.[3SG]</ta>
            <ta e="T119" id="Seg_2336" s="T118">хромой.[NOM.SG]</ta>
            <ta e="T120" id="Seg_2337" s="T119">быть-PST.[3SG]</ta>
            <ta e="T121" id="Seg_2338" s="T120">кобыла-NOM/GEN.3SG</ta>
            <ta e="T122" id="Seg_2339" s="T121">седло-TR-PST.[3SG]</ta>
            <ta e="T123" id="Seg_2340" s="T122">черный.[NOM.SG]</ta>
            <ta e="T124" id="Seg_2341" s="T123">мешок-ACC.3SG</ta>
            <ta e="T125" id="Seg_2342" s="T124">завязать-PST.[3SG]</ta>
            <ta e="T126" id="Seg_2343" s="T125">береста-ADJZ.[NOM.SG]</ta>
            <ta e="T127" id="Seg_2344" s="T126">корзина-ACC.3SG</ta>
            <ta e="T128" id="Seg_2345" s="T127">завязать-PST.[3SG]</ta>
            <ta e="T129" id="Seg_2346" s="T128">идти-INCH-PST.[3SG]</ta>
            <ta e="T130" id="Seg_2347" s="T129">идти-MOM-PST.[3SG]</ta>
            <ta e="T131" id="Seg_2348" s="T130">медведь.[NOM.SG]</ta>
            <ta e="T132" id="Seg_2349" s="T131">попасть-PST.[3SG]</ta>
            <ta e="T133" id="Seg_2350" s="T132">я.ACC</ta>
            <ta e="T134" id="Seg_2351" s="T133">взять-IMP.2SG.O</ta>
            <ta e="T135" id="Seg_2352" s="T134">брать.в.друзья-INF.LAT</ta>
            <ta e="T136" id="Seg_2353" s="T135">ползти-IMP.2SG</ta>
            <ta e="T137" id="Seg_2354" s="T136">черный.[NOM.SG]</ta>
            <ta e="T138" id="Seg_2355" s="T137">мешок-LAT</ta>
            <ta e="T139" id="Seg_2356" s="T138">идти-INCH-PST.[3SG]</ta>
            <ta e="T140" id="Seg_2357" s="T139">лиса.[NOM.SG]</ta>
            <ta e="T141" id="Seg_2358" s="T140">попасть-PST.[3SG]</ta>
            <ta e="T142" id="Seg_2359" s="T141">я.ACC</ta>
            <ta e="T143" id="Seg_2360" s="T142">взять-IMP.2SG.O</ta>
            <ta e="T144" id="Seg_2361" s="T143">брать.в.друзья-INF.LAT</ta>
            <ta e="T145" id="Seg_2362" s="T144">ползти-IMP.2SG</ta>
            <ta e="T146" id="Seg_2363" s="T145">черный.[NOM.SG]</ta>
            <ta e="T147" id="Seg_2364" s="T146">мешок-LAT</ta>
            <ta e="T148" id="Seg_2365" s="T147">ползти-TR-PST.[3SG]</ta>
            <ta e="T149" id="Seg_2366" s="T148">черный.[NOM.SG]</ta>
            <ta e="T150" id="Seg_2367" s="T149">мешок-LAT</ta>
            <ta e="T151" id="Seg_2368" s="T150">тогда</ta>
            <ta e="T152" id="Seg_2369" s="T151">волк.[NOM.SG]</ta>
            <ta e="T153" id="Seg_2370" s="T152">попасть-PST.[3SG]</ta>
            <ta e="T154" id="Seg_2371" s="T153">взять-IMP.2SG</ta>
            <ta e="T155" id="Seg_2372" s="T154">брать.в.друзья-INF</ta>
            <ta e="T156" id="Seg_2373" s="T155">ползти-IMP.2SG</ta>
            <ta e="T157" id="Seg_2374" s="T156">черный.[NOM.SG]</ta>
            <ta e="T158" id="Seg_2375" s="T157">мешок-LAT</ta>
            <ta e="T159" id="Seg_2376" s="T158">тогда</ta>
            <ta e="T160" id="Seg_2377" s="T159">вода.[NOM.SG]</ta>
            <ta e="T161" id="Seg_2378" s="T160">попасть-PST.[3SG]</ta>
            <ta e="T162" id="Seg_2379" s="T161">этот.[NOM.SG]</ta>
            <ta e="T163" id="Seg_2380" s="T162">вода-GEN</ta>
            <ta e="T164" id="Seg_2381" s="T163">черпать-CVB</ta>
            <ta e="T165" id="Seg_2382" s="T164">взять-PST.[3SG]</ta>
            <ta e="T166" id="Seg_2383" s="T165">черный.[NOM.SG]</ta>
            <ta e="T167" id="Seg_2384" s="T166">мешок-LAT/LOC.3SG</ta>
            <ta e="T168" id="Seg_2385" s="T167">береста-ADJZ.[NOM.SG]</ta>
            <ta e="T169" id="Seg_2386" s="T168">корзина-INS</ta>
            <ta e="T170" id="Seg_2387" s="T169">черпать-PST.[3SG]</ta>
            <ta e="T171" id="Seg_2388" s="T170">кобыла-LAT/LOC.3SG</ta>
            <ta e="T172" id="Seg_2389" s="T171">завязать-CVB</ta>
            <ta e="T173" id="Seg_2390" s="T172">бросить-PST.[3SG]</ta>
            <ta e="T174" id="Seg_2391" s="T173">видеть-CVB.TEMP-LAT/LOC.3SG</ta>
            <ta e="T175" id="Seg_2392" s="T174">море-LAT/LOC.3SG</ta>
            <ta e="T176" id="Seg_2393" s="T175">берег-LAT/LOC.3SG</ta>
            <ta e="T177" id="Seg_2394" s="T176">деревня.[NOM.SG]</ta>
            <ta e="T178" id="Seg_2395" s="T177">стоять-PRS.[3SG]</ta>
            <ta e="T179" id="Seg_2396" s="T178">этот.[NOM.SG]</ta>
            <ta e="T180" id="Seg_2397" s="T179">деревня-LAT</ta>
            <ta e="T181" id="Seg_2398" s="T180">NEG</ta>
            <ta e="T182" id="Seg_2399" s="T181">прийти-PTCP.PRS</ta>
            <ta e="T183" id="Seg_2400" s="T182">спуститься-PST.[3SG]</ta>
            <ta e="T184" id="Seg_2401" s="T183">огонь.[NOM.SG]</ta>
            <ta e="T185" id="Seg_2402" s="T184">класть-PST.[3SG]</ta>
            <ta e="T186" id="Seg_2403" s="T185">кобыла-ACC.3SG</ta>
            <ta e="T187" id="Seg_2404" s="T186">стреножить-PST.[3SG]</ta>
            <ta e="T188" id="Seg_2405" s="T187">сидеть.[3SG]</ta>
            <ta e="T189" id="Seg_2406" s="T188">хан.[NOM.SG]</ta>
            <ta e="T190" id="Seg_2407" s="T189">снаружи</ta>
            <ta e="T191" id="Seg_2408" s="T190">идти-DUR-PST.[3SG]</ta>
            <ta e="T192" id="Seg_2409" s="T191">видеть-RES-PST.[3SG]</ta>
            <ta e="T193" id="Seg_2410" s="T192">кто.[NOM.SG]</ta>
            <ta e="T194" id="Seg_2411" s="T193">там</ta>
            <ta e="T195" id="Seg_2412" s="T194">сидеть.[3SG]</ta>
            <ta e="T196" id="Seg_2413" s="T195">лошадь-ACC.3SG</ta>
            <ta e="T197" id="Seg_2414" s="T196">стреножить-RES-PST.[3SG]</ta>
            <ta e="T198" id="Seg_2415" s="T197">я.GEN</ta>
            <ta e="T199" id="Seg_2416" s="T198">зеленый.[NOM.SG]</ta>
            <ta e="T200" id="Seg_2417" s="T199">трава-ACC.3SG</ta>
            <ta e="T201" id="Seg_2418" s="T200">ступать-FRQ-DUR.[3SG]</ta>
            <ta e="T202" id="Seg_2419" s="T201">NEG</ta>
            <ta e="T203" id="Seg_2420" s="T202">спросить-PTCP.PRS</ta>
            <ta e="T204" id="Seg_2421" s="T203">берег-LAT/LOC.3SG</ta>
            <ta e="T205" id="Seg_2422" s="T204">идти-PTCP.PRS</ta>
            <ta e="T206" id="Seg_2423" s="T205">мальчик-ACC.3SG</ta>
            <ta e="T207" id="Seg_2424" s="T206">пускать-DUR.[3SG]</ta>
            <ta e="T208" id="Seg_2425" s="T207">пойти-CVB</ta>
            <ta e="T209" id="Seg_2426" s="T208">спросить-IMP.2SG.O</ta>
            <ta e="T210" id="Seg_2427" s="T209">кто.[NOM.SG]</ta>
            <ta e="T211" id="Seg_2428" s="T210">такой.[NOM.SG]</ta>
            <ta e="T212" id="Seg_2429" s="T211">быть-PRS-2SG</ta>
            <ta e="T213" id="Seg_2430" s="T212">NEG</ta>
            <ta e="T214" id="Seg_2431" s="T213">спросить-PTCP.PRS</ta>
            <ta e="T215" id="Seg_2432" s="T214">хан-EP-ACC</ta>
            <ta e="T216" id="Seg_2433" s="T215">хан-EP-GEN</ta>
            <ta e="T217" id="Seg_2434" s="T216">трава-NOM/GEN/ACC.3SG</ta>
            <ta e="T218" id="Seg_2435" s="T217">ступать-DUR.[3SG]</ta>
            <ta e="T219" id="Seg_2436" s="T218">что</ta>
            <ta e="T220" id="Seg_2437" s="T219">ты.DAT</ta>
            <ta e="T221" id="Seg_2438" s="T220">нужно</ta>
            <ta e="T222" id="Seg_2439" s="T221">пойти-PST.[3SG]</ta>
            <ta e="T223" id="Seg_2440" s="T222">спросить-DUR.[3SG]</ta>
            <ta e="T224" id="Seg_2441" s="T223">какой</ta>
            <ta e="T225" id="Seg_2442" s="T224">мужчина.[NOM.SG]</ta>
            <ta e="T226" id="Seg_2443" s="T225">быть-PRS-2SG</ta>
            <ta e="T227" id="Seg_2444" s="T226">что-ACC</ta>
            <ta e="T228" id="Seg_2445" s="T227">искать-CVB</ta>
            <ta e="T229" id="Seg_2446" s="T228">идти-PRS-2SG</ta>
            <ta e="T230" id="Seg_2447" s="T229">хан-EP-GEN</ta>
            <ta e="T231" id="Seg_2448" s="T230">дочь-ACC.3SG</ta>
            <ta e="T232" id="Seg_2449" s="T231">взять-INF.LAT</ta>
            <ta e="T233" id="Seg_2450" s="T232">прийти-PST-1SG</ta>
            <ta e="T234" id="Seg_2451" s="T233">этот.[NOM.SG]</ta>
            <ta e="T235" id="Seg_2452" s="T234">мужчина.[NOM.SG]</ta>
            <ta e="T236" id="Seg_2453" s="T235">вернуться-PST.[3SG]</ta>
            <ta e="T237" id="Seg_2454" s="T236">хан-LAT</ta>
            <ta e="T238" id="Seg_2455" s="T237">сказать-PST.[3SG]</ta>
            <ta e="T239" id="Seg_2456" s="T238">хан-EP-GEN</ta>
            <ta e="T240" id="Seg_2457" s="T239">дочь-ACC.3SG</ta>
            <ta e="T241" id="Seg_2458" s="T240">взять-INF.LAT</ta>
            <ta e="T242" id="Seg_2459" s="T241">прийти-PST-1SG</ta>
            <ta e="T243" id="Seg_2460" s="T242">пойти-CVB</ta>
            <ta e="T244" id="Seg_2461" s="T243">сказать-IMP.2SG</ta>
            <ta e="T245" id="Seg_2462" s="T244">хороший-NOM/GEN/ACC.2SG-INS</ta>
            <ta e="T246" id="Seg_2463" s="T245">вернуться-EP-IMP.2SG</ta>
            <ta e="T247" id="Seg_2464" s="T246">NEG.AUX-1SG</ta>
            <ta e="T248" id="Seg_2465" s="T247">вернуться-EP-CNG</ta>
            <ta e="T249" id="Seg_2466" s="T248">хороший-INS</ta>
            <ta e="T250" id="Seg_2467" s="T249">NEG</ta>
            <ta e="T251" id="Seg_2468" s="T250">дать-CVB.TEMP-LAT/LOC.3SG</ta>
            <ta e="T252" id="Seg_2469" s="T251">плохой-INS</ta>
            <ta e="T253" id="Seg_2470" s="T252">взять-FUT-1SG</ta>
            <ta e="T254" id="Seg_2471" s="T253">пускать-CVB</ta>
            <ta e="T255" id="Seg_2472" s="T254">взять-IMP.2SG.O</ta>
            <ta e="T256" id="Seg_2473" s="T255">дикий.[NOM.SG]</ta>
            <ta e="T257" id="Seg_2474" s="T256">жеребец-PL-ACC</ta>
            <ta e="T258" id="Seg_2475" s="T257">кобыла-3SG-INS</ta>
            <ta e="T259" id="Seg_2476" s="T258">оторвать-CVB</ta>
            <ta e="T260" id="Seg_2477" s="T259">тянуть-FUT-3PL.O</ta>
            <ta e="T261" id="Seg_2478" s="T260">встать-TR-PST.[3SG]</ta>
            <ta e="T262" id="Seg_2479" s="T261">дикий.[NOM.SG]</ta>
            <ta e="T263" id="Seg_2480" s="T262">жеребец-PL-NOM/GEN/ACC.3PL</ta>
            <ta e="T264" id="Seg_2481" s="T263">жеребец-PL</ta>
            <ta e="T265" id="Seg_2482" s="T264">скакать-DUR-PRS-3PL</ta>
            <ta e="T266" id="Seg_2483" s="T265">черный.[NOM.SG]</ta>
            <ta e="T267" id="Seg_2484" s="T266">мешок-ACC.3SG</ta>
            <ta e="T268" id="Seg_2485" s="T267">развязать-PST.[3SG]</ta>
            <ta e="T269" id="Seg_2486" s="T268">медведь-ACC</ta>
            <ta e="T270" id="Seg_2487" s="T269">встать-TR-MOM-PST.[3SG]</ta>
            <ta e="T271" id="Seg_2488" s="T270">медведь.[NOM.SG]</ta>
            <ta e="T272" id="Seg_2489" s="T271">жеребец-PL-ACC</ta>
            <ta e="T273" id="Seg_2490" s="T272">прогнать-CVB</ta>
            <ta e="T274" id="Seg_2491" s="T273">нести-RES-PST.[3SG]</ta>
            <ta e="T275" id="Seg_2492" s="T274">встать-TR-IMP.2PL.O</ta>
            <ta e="T276" id="Seg_2493" s="T275">бык-PL-ACC</ta>
            <ta e="T277" id="Seg_2494" s="T276">дикий-PL-ACC</ta>
            <ta e="T278" id="Seg_2495" s="T277">кобыла-3SG-INS</ta>
            <ta e="T279" id="Seg_2496" s="T278">мертвый.[NOM.SG]</ta>
            <ta e="T280" id="Seg_2497" s="T279">уколоть-CVB</ta>
            <ta e="T281" id="Seg_2498" s="T280">нести-FUT-3PL.O</ta>
            <ta e="T282" id="Seg_2499" s="T281">черный.[NOM.SG]</ta>
            <ta e="T283" id="Seg_2500" s="T282">мешок-ACC.3SG</ta>
            <ta e="T284" id="Seg_2501" s="T283">развязать-PST.[3SG]</ta>
            <ta e="T285" id="Seg_2502" s="T284">волк-NOM/GEN/ACC.3SG</ta>
            <ta e="T286" id="Seg_2503" s="T285">пускать-CVB</ta>
            <ta e="T287" id="Seg_2504" s="T286">дать-PST.[3SG]</ta>
            <ta e="T288" id="Seg_2505" s="T287">волк.[NOM.SG]</ta>
            <ta e="T289" id="Seg_2506" s="T288">бык-PL-ACC</ta>
            <ta e="T290" id="Seg_2507" s="T289">нажимать-CVB</ta>
            <ta e="T291" id="Seg_2508" s="T290">нести-PST.[3SG]</ta>
            <ta e="T292" id="Seg_2509" s="T291">дерево-GEN</ta>
            <ta e="T293" id="Seg_2510" s="T292">верх-LAT/LOC.3PL</ta>
            <ta e="T294" id="Seg_2511" s="T293">хан.[NOM.SG]</ta>
            <ta e="T295" id="Seg_2512" s="T294">остаться-CVB</ta>
            <ta e="T296" id="Seg_2513" s="T295">остаться-PST.[3SG]</ta>
            <ta e="T297" id="Seg_2514" s="T296">люди.[NOM.SG]</ta>
            <ta e="T298" id="Seg_2515" s="T297">собрать-PST.[3SG]</ta>
            <ta e="T299" id="Seg_2516" s="T298">окружать-CVB</ta>
            <ta e="T300" id="Seg_2517" s="T299">взять-OPT.DU/PL-1PL</ta>
            <ta e="T301" id="Seg_2518" s="T300">этот-ACC</ta>
            <ta e="T302" id="Seg_2519" s="T301">вернуться-TR-TR-MOM-PST-3PL</ta>
            <ta e="T303" id="Seg_2520" s="T302">схватить-PTCP</ta>
            <ta e="T304" id="Seg_2521" s="T303">хотеть-PRS-3PL</ta>
            <ta e="T305" id="Seg_2522" s="T304">черный.[NOM.SG]</ta>
            <ta e="T306" id="Seg_2523" s="T305">мешок-ACC.3SG</ta>
            <ta e="T307" id="Seg_2524" s="T306">развязать-CVB</ta>
            <ta e="T308" id="Seg_2525" s="T307">тянуть-PST.[3SG]</ta>
            <ta e="T309" id="Seg_2526" s="T308">вода-ACC.3SG</ta>
            <ta e="T310" id="Seg_2527" s="T309">лить-CVB</ta>
            <ta e="T311" id="Seg_2528" s="T310">бросить-PST.[3SG]</ta>
            <ta e="T312" id="Seg_2529" s="T311">люди-EP-ACC</ta>
            <ta e="T313" id="Seg_2530" s="T312">вода.[NOM.SG]</ta>
            <ta e="T314" id="Seg_2531" s="T313">нести-CVB</ta>
            <ta e="T315" id="Seg_2532" s="T314">идти-PRS-3SG.O</ta>
            <ta e="T316" id="Seg_2533" s="T315">хан.[NOM.SG]</ta>
            <ta e="T317" id="Seg_2534" s="T316">кричать-PRS.[3SG]</ta>
            <ta e="T318" id="Seg_2535" s="T317">Тарджа_Барджа</ta>
            <ta e="T319" id="Seg_2536" s="T318">Пол_Барджа</ta>
            <ta e="T320" id="Seg_2537" s="T319">половина</ta>
            <ta e="T321" id="Seg_2538" s="T320">скот-NOM/GEN/ACC.1SG</ta>
            <ta e="T322" id="Seg_2539" s="T321">дать-FUT-1SG</ta>
            <ta e="T323" id="Seg_2540" s="T322">половина</ta>
            <ta e="T324" id="Seg_2541" s="T323">богатство-NOM/GEN/ACC.1SG</ta>
            <ta e="T325" id="Seg_2542" s="T324">дать-FUT-1SG</ta>
            <ta e="T326" id="Seg_2543" s="T325">дочь-NOM/GEN/ACC.1SG</ta>
            <ta e="T327" id="Seg_2544" s="T326">дать-FUT-1SG</ta>
            <ta e="T328" id="Seg_2545" s="T327">NEG</ta>
            <ta e="T329" id="Seg_2546" s="T328">нужно</ta>
            <ta e="T330" id="Seg_2547" s="T329">я.LAT</ta>
            <ta e="T331" id="Seg_2548" s="T330">ты.GEN</ta>
            <ta e="T332" id="Seg_2549" s="T331">скот-NOM/GEN/ACC.2SG</ta>
            <ta e="T333" id="Seg_2550" s="T332">NEG</ta>
            <ta e="T334" id="Seg_2551" s="T333">нужно</ta>
            <ta e="T335" id="Seg_2552" s="T334">я.LAT</ta>
            <ta e="T336" id="Seg_2553" s="T335">ты.GEN</ta>
            <ta e="T337" id="Seg_2554" s="T336">богатство-NOM/GEN/ACC.2SG</ta>
            <ta e="T338" id="Seg_2555" s="T337">только</ta>
            <ta e="T339" id="Seg_2556" s="T338">принцесса.[NOM.SG]</ta>
            <ta e="T340" id="Seg_2557" s="T339">дочь-NOM/GEN/ACC.2SG</ta>
            <ta e="T341" id="Seg_2558" s="T340">нужно</ta>
            <ta e="T342" id="Seg_2559" s="T341">я.LAT</ta>
            <ta e="T343" id="Seg_2560" s="T342">взять-PST.[3SG]</ta>
            <ta e="T344" id="Seg_2561" s="T343">береста-ADJZ.[NOM.SG]</ta>
            <ta e="T345" id="Seg_2562" s="T344">корзина-ACC.3SG</ta>
            <ta e="T346" id="Seg_2563" s="T345">вода-ACC.3SG</ta>
            <ta e="T347" id="Seg_2564" s="T346">черпать-PST.[3SG]</ta>
            <ta e="T348" id="Seg_2565" s="T347">хан.[NOM.SG]</ta>
            <ta e="T349" id="Seg_2566" s="T348">дочь-ACC.3SG</ta>
            <ta e="T350" id="Seg_2567" s="T349">дать-PST.[3SG]</ta>
            <ta e="T351" id="Seg_2568" s="T350">хромой.[NOM.SG]</ta>
            <ta e="T352" id="Seg_2569" s="T351">кобыла-LAT/LOC.3SG</ta>
            <ta e="T353" id="Seg_2570" s="T352">садиться.на.лошадь-TR-TR-PST.[3SG]</ta>
            <ta e="T354" id="Seg_2571" s="T353">привязывать-CVB</ta>
            <ta e="T355" id="Seg_2572" s="T354">встать-PST.[3SG]</ta>
            <ta e="T356" id="Seg_2573" s="T355">чум-LAT/LOC.3SG</ta>
            <ta e="T357" id="Seg_2574" s="T356">нести-RES-PST.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T111" id="Seg_2575" s="T110">adj-n:case</ta>
            <ta e="T112" id="Seg_2576" s="T111">n-n:case</ta>
            <ta e="T113" id="Seg_2577" s="T112">v-v:tense-v:pn</ta>
            <ta e="T114" id="Seg_2578" s="T113">n-n:case.poss</ta>
            <ta e="T115" id="Seg_2579" s="T114">propr-n:case</ta>
            <ta e="T116" id="Seg_2580" s="T115">adj-n:case</ta>
            <ta e="T117" id="Seg_2581" s="T116">n-n:case.poss</ta>
            <ta e="T118" id="Seg_2582" s="T117">v-v:tense-v:pn</ta>
            <ta e="T119" id="Seg_2583" s="T118">adj-n:case</ta>
            <ta e="T120" id="Seg_2584" s="T119">v-v:tense-v:pn</ta>
            <ta e="T121" id="Seg_2585" s="T120">n-n:case.poss</ta>
            <ta e="T122" id="Seg_2586" s="T121">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T123" id="Seg_2587" s="T122">adj-n:case</ta>
            <ta e="T124" id="Seg_2588" s="T123">n-n:case.poss</ta>
            <ta e="T125" id="Seg_2589" s="T124">v-v:tense-v:pn</ta>
            <ta e="T126" id="Seg_2590" s="T125">n-n&gt;adj-n:case</ta>
            <ta e="T127" id="Seg_2591" s="T126">n-n:case.poss</ta>
            <ta e="T128" id="Seg_2592" s="T127">v-v:tense-v:pn</ta>
            <ta e="T129" id="Seg_2593" s="T128">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T130" id="Seg_2594" s="T129">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T131" id="Seg_2595" s="T130">n-n:case</ta>
            <ta e="T132" id="Seg_2596" s="T131">v-v:tense-v:pn</ta>
            <ta e="T133" id="Seg_2597" s="T132">pers</ta>
            <ta e="T134" id="Seg_2598" s="T133">v-v:mood.pn</ta>
            <ta e="T135" id="Seg_2599" s="T134">v-v:n.fin</ta>
            <ta e="T136" id="Seg_2600" s="T135">v-v:mood.pn</ta>
            <ta e="T137" id="Seg_2601" s="T136">adj-n:case</ta>
            <ta e="T138" id="Seg_2602" s="T137">n-n:case</ta>
            <ta e="T139" id="Seg_2603" s="T138">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T140" id="Seg_2604" s="T139">n-n:case</ta>
            <ta e="T141" id="Seg_2605" s="T140">v-v:tense-v:pn</ta>
            <ta e="T142" id="Seg_2606" s="T141">pers</ta>
            <ta e="T143" id="Seg_2607" s="T142">v-v:mood.pn</ta>
            <ta e="T144" id="Seg_2608" s="T143">v-v:n.fin</ta>
            <ta e="T145" id="Seg_2609" s="T144">v-v:mood.pn</ta>
            <ta e="T146" id="Seg_2610" s="T145">adj-n:case</ta>
            <ta e="T147" id="Seg_2611" s="T146">n-n:case</ta>
            <ta e="T148" id="Seg_2612" s="T147">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T149" id="Seg_2613" s="T148">adj-n:case</ta>
            <ta e="T150" id="Seg_2614" s="T149">n-n:case</ta>
            <ta e="T151" id="Seg_2615" s="T150">adv</ta>
            <ta e="T152" id="Seg_2616" s="T151">n-n:case</ta>
            <ta e="T153" id="Seg_2617" s="T152">v-v:tense-v:pn</ta>
            <ta e="T154" id="Seg_2618" s="T153">v-v:mood.pn</ta>
            <ta e="T155" id="Seg_2619" s="T154">v-v:n.fin</ta>
            <ta e="T156" id="Seg_2620" s="T155">v-v:mood.pn</ta>
            <ta e="T157" id="Seg_2621" s="T156">adj-n:case</ta>
            <ta e="T158" id="Seg_2622" s="T157">n-n:case</ta>
            <ta e="T159" id="Seg_2623" s="T158">adv</ta>
            <ta e="T160" id="Seg_2624" s="T159">n-n:case</ta>
            <ta e="T161" id="Seg_2625" s="T160">v-v:tense-v:pn</ta>
            <ta e="T162" id="Seg_2626" s="T161">dempro-n:case</ta>
            <ta e="T163" id="Seg_2627" s="T162">n-n:case</ta>
            <ta e="T164" id="Seg_2628" s="T163">v-v:n.fin</ta>
            <ta e="T165" id="Seg_2629" s="T164">v-v:tense-v:pn</ta>
            <ta e="T166" id="Seg_2630" s="T165">adj-n:case</ta>
            <ta e="T167" id="Seg_2631" s="T166">n-n:case.poss</ta>
            <ta e="T168" id="Seg_2632" s="T167">n-n&gt;adj-n:case</ta>
            <ta e="T169" id="Seg_2633" s="T168">n-n:case</ta>
            <ta e="T170" id="Seg_2634" s="T169">v-v:tense-v:pn</ta>
            <ta e="T171" id="Seg_2635" s="T170">n-n:case.poss</ta>
            <ta e="T172" id="Seg_2636" s="T171">v-v:n.fin</ta>
            <ta e="T173" id="Seg_2637" s="T172">v-v:tense-v:pn</ta>
            <ta e="T174" id="Seg_2638" s="T173">v-v:n.fin-v:(case.poss)</ta>
            <ta e="T175" id="Seg_2639" s="T174">n-n:case.poss</ta>
            <ta e="T176" id="Seg_2640" s="T175">n-n:case.poss</ta>
            <ta e="T177" id="Seg_2641" s="T176">n-n:case</ta>
            <ta e="T178" id="Seg_2642" s="T177">v-v:tense-v:pn</ta>
            <ta e="T179" id="Seg_2643" s="T178">dempro-n:case</ta>
            <ta e="T180" id="Seg_2644" s="T179">n-n:case</ta>
            <ta e="T181" id="Seg_2645" s="T180">ptcl</ta>
            <ta e="T182" id="Seg_2646" s="T181">v-v&gt;v</ta>
            <ta e="T183" id="Seg_2647" s="T182">v-v:tense-v:pn</ta>
            <ta e="T184" id="Seg_2648" s="T183">n-n:case</ta>
            <ta e="T185" id="Seg_2649" s="T184">v-v:tense-v:pn</ta>
            <ta e="T186" id="Seg_2650" s="T185">n-n:case.poss</ta>
            <ta e="T187" id="Seg_2651" s="T186">v-v:tense-v:pn</ta>
            <ta e="T188" id="Seg_2652" s="T187">v-v:pn</ta>
            <ta e="T189" id="Seg_2653" s="T188">n-n:case</ta>
            <ta e="T190" id="Seg_2654" s="T189">adv</ta>
            <ta e="T191" id="Seg_2655" s="T190">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T192" id="Seg_2656" s="T191">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T193" id="Seg_2657" s="T192">que</ta>
            <ta e="T194" id="Seg_2658" s="T193">adv</ta>
            <ta e="T195" id="Seg_2659" s="T194">v-v:pn</ta>
            <ta e="T196" id="Seg_2660" s="T195">n-n:case.poss</ta>
            <ta e="T197" id="Seg_2661" s="T196">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T198" id="Seg_2662" s="T197">pers</ta>
            <ta e="T199" id="Seg_2663" s="T198">adj-n:case</ta>
            <ta e="T200" id="Seg_2664" s="T199">n-n:case.poss</ta>
            <ta e="T201" id="Seg_2665" s="T200">v-v&gt;v-v&gt;v-v:pn</ta>
            <ta e="T202" id="Seg_2666" s="T201">ptcl</ta>
            <ta e="T203" id="Seg_2667" s="T202">v-v&gt;v</ta>
            <ta e="T204" id="Seg_2668" s="T203">n-n:case.poss</ta>
            <ta e="T205" id="Seg_2669" s="T204">v-v:n.fin</ta>
            <ta e="T206" id="Seg_2670" s="T205">n-n:case.poss</ta>
            <ta e="T207" id="Seg_2671" s="T206">v-v&gt;v-v:pn</ta>
            <ta e="T208" id="Seg_2672" s="T207">v-v:n.fin</ta>
            <ta e="T209" id="Seg_2673" s="T208">v-v:mood.pn</ta>
            <ta e="T210" id="Seg_2674" s="T209">que</ta>
            <ta e="T211" id="Seg_2675" s="T210">adj-n:case</ta>
            <ta e="T212" id="Seg_2676" s="T211">v-v:tense-v:pn</ta>
            <ta e="T213" id="Seg_2677" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_2678" s="T213">v-v&gt;v</ta>
            <ta e="T215" id="Seg_2679" s="T214">n-n:ins-n:case</ta>
            <ta e="T216" id="Seg_2680" s="T215">n-n:ins-n:case</ta>
            <ta e="T217" id="Seg_2681" s="T216">n-n:case.poss</ta>
            <ta e="T218" id="Seg_2682" s="T217">v-v&gt;v-v:pn</ta>
            <ta e="T219" id="Seg_2683" s="T218">que</ta>
            <ta e="T220" id="Seg_2684" s="T219">pers</ta>
            <ta e="T221" id="Seg_2685" s="T220">adv</ta>
            <ta e="T222" id="Seg_2686" s="T221">v-v:tense-v:pn</ta>
            <ta e="T223" id="Seg_2687" s="T222">v-v&gt;v-v:pn</ta>
            <ta e="T224" id="Seg_2688" s="T223">que</ta>
            <ta e="T225" id="Seg_2689" s="T224">n-n:case</ta>
            <ta e="T226" id="Seg_2690" s="T225">v-v:tense-v:pn</ta>
            <ta e="T227" id="Seg_2691" s="T226">que-n:case</ta>
            <ta e="T228" id="Seg_2692" s="T227">v-v:n.fin</ta>
            <ta e="T229" id="Seg_2693" s="T228">v-v:tense-v:pn</ta>
            <ta e="T230" id="Seg_2694" s="T229">n-n:ins-n:case</ta>
            <ta e="T231" id="Seg_2695" s="T230">n-n:case.poss</ta>
            <ta e="T232" id="Seg_2696" s="T231">v-v:n.fin</ta>
            <ta e="T233" id="Seg_2697" s="T232">v-v:tense-v:pn</ta>
            <ta e="T234" id="Seg_2698" s="T233">dempro-n:case</ta>
            <ta e="T235" id="Seg_2699" s="T234">n-n:case</ta>
            <ta e="T236" id="Seg_2700" s="T235">v-v:tense-v:pn</ta>
            <ta e="T237" id="Seg_2701" s="T236">n-n:case</ta>
            <ta e="T238" id="Seg_2702" s="T237">v-v:tense-v:pn</ta>
            <ta e="T239" id="Seg_2703" s="T238">n-n:ins-n:case</ta>
            <ta e="T240" id="Seg_2704" s="T239">n-n:case.poss</ta>
            <ta e="T241" id="Seg_2705" s="T240">v-v:n.fin</ta>
            <ta e="T242" id="Seg_2706" s="T241">v-v:tense-v:pn</ta>
            <ta e="T243" id="Seg_2707" s="T242">v-v:n.fin</ta>
            <ta e="T244" id="Seg_2708" s="T243">v-v:mood.pn</ta>
            <ta e="T245" id="Seg_2709" s="T244">adj-n:case.poss-n:case</ta>
            <ta e="T246" id="Seg_2710" s="T245">v-v:ins-v:mood.pn</ta>
            <ta e="T247" id="Seg_2711" s="T246">aux-v:pn</ta>
            <ta e="T248" id="Seg_2712" s="T247">v-v:ins-v:n.fin</ta>
            <ta e="T249" id="Seg_2713" s="T248">adj-n:case</ta>
            <ta e="T250" id="Seg_2714" s="T249">ptcl</ta>
            <ta e="T251" id="Seg_2715" s="T250">v-v:n.fin-v:(case.poss)</ta>
            <ta e="T252" id="Seg_2716" s="T251">adj-n:case</ta>
            <ta e="T253" id="Seg_2717" s="T252">v-v:tense-v:pn</ta>
            <ta e="T254" id="Seg_2718" s="T253">v-v:n.fin</ta>
            <ta e="T255" id="Seg_2719" s="T254">v-v:mood.pn</ta>
            <ta e="T256" id="Seg_2720" s="T255">adj-n:case</ta>
            <ta e="T257" id="Seg_2721" s="T256">n-n:num-n:case</ta>
            <ta e="T258" id="Seg_2722" s="T257">n-n:case.poss-n:case</ta>
            <ta e="T259" id="Seg_2723" s="T258">v-v:n.fin</ta>
            <ta e="T260" id="Seg_2724" s="T259">v-v:tense-v:pn</ta>
            <ta e="T261" id="Seg_2725" s="T260">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T262" id="Seg_2726" s="T261">adj-n:case</ta>
            <ta e="T263" id="Seg_2727" s="T262">n-n:num-n:case.poss</ta>
            <ta e="T264" id="Seg_2728" s="T263">n-n:num</ta>
            <ta e="T265" id="Seg_2729" s="T264">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T266" id="Seg_2730" s="T265">adj-n:case</ta>
            <ta e="T267" id="Seg_2731" s="T266">n-n:case.poss</ta>
            <ta e="T268" id="Seg_2732" s="T267">v-v:tense-v:pn</ta>
            <ta e="T269" id="Seg_2733" s="T268">n-n:case</ta>
            <ta e="T270" id="Seg_2734" s="T269">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T271" id="Seg_2735" s="T270">n-n:case</ta>
            <ta e="T272" id="Seg_2736" s="T271">n-n:num-n:case</ta>
            <ta e="T273" id="Seg_2737" s="T272">v-v:n.fin</ta>
            <ta e="T274" id="Seg_2738" s="T273">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T275" id="Seg_2739" s="T274">v-v&gt;v-v:mood.pn</ta>
            <ta e="T276" id="Seg_2740" s="T275">n-n:num-n:case</ta>
            <ta e="T277" id="Seg_2741" s="T276">adj-n:num-n:case</ta>
            <ta e="T278" id="Seg_2742" s="T277">n-n:case.poss-n:case</ta>
            <ta e="T279" id="Seg_2743" s="T278">adj-n:case</ta>
            <ta e="T280" id="Seg_2744" s="T279">v-v:n.fin</ta>
            <ta e="T281" id="Seg_2745" s="T280">v-v:tense-v:pn</ta>
            <ta e="T282" id="Seg_2746" s="T281">adj-n:case</ta>
            <ta e="T283" id="Seg_2747" s="T282">n-n:case.poss</ta>
            <ta e="T284" id="Seg_2748" s="T283">v-v:tense-v:pn</ta>
            <ta e="T285" id="Seg_2749" s="T284">n-n:case.poss</ta>
            <ta e="T286" id="Seg_2750" s="T285">v-v:n.fin</ta>
            <ta e="T287" id="Seg_2751" s="T286">v-v:tense-v:pn</ta>
            <ta e="T288" id="Seg_2752" s="T287">n-n:case</ta>
            <ta e="T289" id="Seg_2753" s="T288">n-n:num-n:case</ta>
            <ta e="T290" id="Seg_2754" s="T289">v-v:n.fin</ta>
            <ta e="T291" id="Seg_2755" s="T290">v-v:tense-v:pn</ta>
            <ta e="T292" id="Seg_2756" s="T291">n-n:case</ta>
            <ta e="T293" id="Seg_2757" s="T292">n-n:case.poss</ta>
            <ta e="T294" id="Seg_2758" s="T293">n-n:case</ta>
            <ta e="T295" id="Seg_2759" s="T294">v-v:n.fin</ta>
            <ta e="T296" id="Seg_2760" s="T295">v-v:tense-v:pn</ta>
            <ta e="T297" id="Seg_2761" s="T296">n-n:case</ta>
            <ta e="T298" id="Seg_2762" s="T297">v-v:tense-v:pn</ta>
            <ta e="T299" id="Seg_2763" s="T298">v-v:n.fin</ta>
            <ta e="T300" id="Seg_2764" s="T299">v-v:mood-v:pn</ta>
            <ta e="T301" id="Seg_2765" s="T300">dempro-n:case</ta>
            <ta e="T302" id="Seg_2766" s="T301">v-v&gt;v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T303" id="Seg_2767" s="T302">v-v:n.fin</ta>
            <ta e="T304" id="Seg_2768" s="T303">v-v:tense-v:pn</ta>
            <ta e="T305" id="Seg_2769" s="T304">adj-n:case</ta>
            <ta e="T306" id="Seg_2770" s="T305">n-n:case.poss</ta>
            <ta e="T307" id="Seg_2771" s="T306">v-v:n.fin</ta>
            <ta e="T308" id="Seg_2772" s="T307">v-v:tense-v:pn</ta>
            <ta e="T309" id="Seg_2773" s="T308">n-n:case.poss</ta>
            <ta e="T310" id="Seg_2774" s="T309">v-v:n.fin</ta>
            <ta e="T311" id="Seg_2775" s="T310">v-v:tense-v:pn</ta>
            <ta e="T312" id="Seg_2776" s="T311">n-n:ins-n:case</ta>
            <ta e="T313" id="Seg_2777" s="T312">n-n:case</ta>
            <ta e="T314" id="Seg_2778" s="T313">v-v:n.fin</ta>
            <ta e="T315" id="Seg_2779" s="T314">v-v:tense-v:pn</ta>
            <ta e="T316" id="Seg_2780" s="T315">n-n:case</ta>
            <ta e="T317" id="Seg_2781" s="T316">v-v:tense-v:pn</ta>
            <ta e="T318" id="Seg_2782" s="T317">propr</ta>
            <ta e="T319" id="Seg_2783" s="T318">propr</ta>
            <ta e="T320" id="Seg_2784" s="T319">quant</ta>
            <ta e="T321" id="Seg_2785" s="T320">n-n:case.poss</ta>
            <ta e="T322" id="Seg_2786" s="T321">v-v:tense-v:pn</ta>
            <ta e="T323" id="Seg_2787" s="T322">quant</ta>
            <ta e="T324" id="Seg_2788" s="T323">n-n:case.poss</ta>
            <ta e="T325" id="Seg_2789" s="T324">v-v:tense-v:pn</ta>
            <ta e="T326" id="Seg_2790" s="T325">n-n:case.poss</ta>
            <ta e="T327" id="Seg_2791" s="T326">v-v:tense-v:pn</ta>
            <ta e="T328" id="Seg_2792" s="T327">ptcl</ta>
            <ta e="T329" id="Seg_2793" s="T328">adv</ta>
            <ta e="T330" id="Seg_2794" s="T329">pers</ta>
            <ta e="T331" id="Seg_2795" s="T330">pers</ta>
            <ta e="T332" id="Seg_2796" s="T331">n-n:case.poss</ta>
            <ta e="T333" id="Seg_2797" s="T332">ptcl</ta>
            <ta e="T334" id="Seg_2798" s="T333">adv</ta>
            <ta e="T335" id="Seg_2799" s="T334">pers</ta>
            <ta e="T336" id="Seg_2800" s="T335">pers</ta>
            <ta e="T337" id="Seg_2801" s="T336">n-n:case.poss</ta>
            <ta e="T338" id="Seg_2802" s="T337">adv</ta>
            <ta e="T339" id="Seg_2803" s="T338">n-n:case</ta>
            <ta e="T340" id="Seg_2804" s="T339">n-n:case.poss</ta>
            <ta e="T341" id="Seg_2805" s="T340">adv</ta>
            <ta e="T342" id="Seg_2806" s="T341">pers</ta>
            <ta e="T343" id="Seg_2807" s="T342">v-v:tense-v:pn</ta>
            <ta e="T344" id="Seg_2808" s="T343">n-n&gt;adj-n:case</ta>
            <ta e="T345" id="Seg_2809" s="T344">n-n:case.poss</ta>
            <ta e="T346" id="Seg_2810" s="T345">n-n:case.poss</ta>
            <ta e="T347" id="Seg_2811" s="T346">v-v:tense-v:pn</ta>
            <ta e="T348" id="Seg_2812" s="T347">n-n:case</ta>
            <ta e="T349" id="Seg_2813" s="T348">n-n:case.poss</ta>
            <ta e="T350" id="Seg_2814" s="T349">v-v:tense-v:pn</ta>
            <ta e="T351" id="Seg_2815" s="T350">adj-n:case</ta>
            <ta e="T352" id="Seg_2816" s="T351">n-n:case.poss</ta>
            <ta e="T353" id="Seg_2817" s="T352">v-v&gt;v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T354" id="Seg_2818" s="T353">v-v:n.fin</ta>
            <ta e="T355" id="Seg_2819" s="T354">v-v:tense-v:pn</ta>
            <ta e="T356" id="Seg_2820" s="T355">n-n:case.poss</ta>
            <ta e="T357" id="Seg_2821" s="T356">v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T111" id="Seg_2822" s="T110">adj</ta>
            <ta e="T112" id="Seg_2823" s="T111">n</ta>
            <ta e="T113" id="Seg_2824" s="T112">v</ta>
            <ta e="T114" id="Seg_2825" s="T113">n</ta>
            <ta e="T115" id="Seg_2826" s="T114">propr</ta>
            <ta e="T116" id="Seg_2827" s="T115">adj</ta>
            <ta e="T117" id="Seg_2828" s="T116">n</ta>
            <ta e="T118" id="Seg_2829" s="T117">v</ta>
            <ta e="T119" id="Seg_2830" s="T118">adj</ta>
            <ta e="T120" id="Seg_2831" s="T119">v</ta>
            <ta e="T121" id="Seg_2832" s="T120">n</ta>
            <ta e="T122" id="Seg_2833" s="T121">n</ta>
            <ta e="T123" id="Seg_2834" s="T122">adj</ta>
            <ta e="T124" id="Seg_2835" s="T123">n</ta>
            <ta e="T125" id="Seg_2836" s="T124">v</ta>
            <ta e="T126" id="Seg_2837" s="T125">adj</ta>
            <ta e="T127" id="Seg_2838" s="T126">n</ta>
            <ta e="T128" id="Seg_2839" s="T127">v</ta>
            <ta e="T129" id="Seg_2840" s="T128">v</ta>
            <ta e="T130" id="Seg_2841" s="T129">v</ta>
            <ta e="T131" id="Seg_2842" s="T130">n</ta>
            <ta e="T132" id="Seg_2843" s="T131">v</ta>
            <ta e="T133" id="Seg_2844" s="T132">pers</ta>
            <ta e="T134" id="Seg_2845" s="T133">v</ta>
            <ta e="T135" id="Seg_2846" s="T134">v</ta>
            <ta e="T136" id="Seg_2847" s="T135">v</ta>
            <ta e="T137" id="Seg_2848" s="T136">adj</ta>
            <ta e="T138" id="Seg_2849" s="T137">n</ta>
            <ta e="T139" id="Seg_2850" s="T138">v</ta>
            <ta e="T140" id="Seg_2851" s="T139">n</ta>
            <ta e="T141" id="Seg_2852" s="T140">v</ta>
            <ta e="T142" id="Seg_2853" s="T141">pers</ta>
            <ta e="T143" id="Seg_2854" s="T142">v</ta>
            <ta e="T144" id="Seg_2855" s="T143">v</ta>
            <ta e="T145" id="Seg_2856" s="T144">v</ta>
            <ta e="T146" id="Seg_2857" s="T145">adj</ta>
            <ta e="T147" id="Seg_2858" s="T146">n</ta>
            <ta e="T148" id="Seg_2859" s="T147">v</ta>
            <ta e="T149" id="Seg_2860" s="T148">adj</ta>
            <ta e="T150" id="Seg_2861" s="T149">n</ta>
            <ta e="T151" id="Seg_2862" s="T150">adv</ta>
            <ta e="T152" id="Seg_2863" s="T151">n</ta>
            <ta e="T153" id="Seg_2864" s="T152">v</ta>
            <ta e="T154" id="Seg_2865" s="T153">v</ta>
            <ta e="T155" id="Seg_2866" s="T154">v</ta>
            <ta e="T156" id="Seg_2867" s="T155">v</ta>
            <ta e="T157" id="Seg_2868" s="T156">adj</ta>
            <ta e="T158" id="Seg_2869" s="T157">n</ta>
            <ta e="T159" id="Seg_2870" s="T158">adv</ta>
            <ta e="T160" id="Seg_2871" s="T159">n</ta>
            <ta e="T161" id="Seg_2872" s="T160">v</ta>
            <ta e="T162" id="Seg_2873" s="T161">dempro</ta>
            <ta e="T163" id="Seg_2874" s="T162">n</ta>
            <ta e="T164" id="Seg_2875" s="T163">v</ta>
            <ta e="T165" id="Seg_2876" s="T164">v</ta>
            <ta e="T166" id="Seg_2877" s="T165">adj</ta>
            <ta e="T167" id="Seg_2878" s="T166">n</ta>
            <ta e="T168" id="Seg_2879" s="T167">adj</ta>
            <ta e="T169" id="Seg_2880" s="T168">n</ta>
            <ta e="T170" id="Seg_2881" s="T169">v</ta>
            <ta e="T171" id="Seg_2882" s="T170">n</ta>
            <ta e="T172" id="Seg_2883" s="T171">v</ta>
            <ta e="T173" id="Seg_2884" s="T172">v</ta>
            <ta e="T174" id="Seg_2885" s="T173">v</ta>
            <ta e="T175" id="Seg_2886" s="T174">n</ta>
            <ta e="T176" id="Seg_2887" s="T175">n</ta>
            <ta e="T177" id="Seg_2888" s="T176">n</ta>
            <ta e="T178" id="Seg_2889" s="T177">v</ta>
            <ta e="T179" id="Seg_2890" s="T178">dempro</ta>
            <ta e="T180" id="Seg_2891" s="T179">n</ta>
            <ta e="T181" id="Seg_2892" s="T180">ptcl</ta>
            <ta e="T182" id="Seg_2893" s="T181">adj</ta>
            <ta e="T183" id="Seg_2894" s="T182">v</ta>
            <ta e="T184" id="Seg_2895" s="T183">n</ta>
            <ta e="T185" id="Seg_2896" s="T184">v</ta>
            <ta e="T186" id="Seg_2897" s="T185">n</ta>
            <ta e="T187" id="Seg_2898" s="T186">v</ta>
            <ta e="T188" id="Seg_2899" s="T187">v</ta>
            <ta e="T189" id="Seg_2900" s="T188">n</ta>
            <ta e="T190" id="Seg_2901" s="T189">adv</ta>
            <ta e="T191" id="Seg_2902" s="T190">v</ta>
            <ta e="T192" id="Seg_2903" s="T191">v</ta>
            <ta e="T193" id="Seg_2904" s="T192">que</ta>
            <ta e="T194" id="Seg_2905" s="T193">adv</ta>
            <ta e="T195" id="Seg_2906" s="T194">v</ta>
            <ta e="T196" id="Seg_2907" s="T195">n</ta>
            <ta e="T197" id="Seg_2908" s="T196">v</ta>
            <ta e="T198" id="Seg_2909" s="T197">pers</ta>
            <ta e="T199" id="Seg_2910" s="T198">adj</ta>
            <ta e="T200" id="Seg_2911" s="T199">n</ta>
            <ta e="T201" id="Seg_2912" s="T200">v</ta>
            <ta e="T202" id="Seg_2913" s="T201">ptcl</ta>
            <ta e="T203" id="Seg_2914" s="T202">adj</ta>
            <ta e="T204" id="Seg_2915" s="T203">n</ta>
            <ta e="T205" id="Seg_2916" s="T204">adj</ta>
            <ta e="T206" id="Seg_2917" s="T205">n</ta>
            <ta e="T207" id="Seg_2918" s="T206">v</ta>
            <ta e="T208" id="Seg_2919" s="T207">v</ta>
            <ta e="T209" id="Seg_2920" s="T208">v</ta>
            <ta e="T210" id="Seg_2921" s="T209">que</ta>
            <ta e="T211" id="Seg_2922" s="T210">adj</ta>
            <ta e="T212" id="Seg_2923" s="T211">v</ta>
            <ta e="T213" id="Seg_2924" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_2925" s="T213">adj</ta>
            <ta e="T215" id="Seg_2926" s="T214">n</ta>
            <ta e="T216" id="Seg_2927" s="T215">n</ta>
            <ta e="T217" id="Seg_2928" s="T216">n</ta>
            <ta e="T218" id="Seg_2929" s="T217">v</ta>
            <ta e="T219" id="Seg_2930" s="T218">que</ta>
            <ta e="T220" id="Seg_2931" s="T219">pers</ta>
            <ta e="T221" id="Seg_2932" s="T220">adv</ta>
            <ta e="T222" id="Seg_2933" s="T221">v</ta>
            <ta e="T223" id="Seg_2934" s="T222">v</ta>
            <ta e="T224" id="Seg_2935" s="T223">que</ta>
            <ta e="T225" id="Seg_2936" s="T224">n</ta>
            <ta e="T226" id="Seg_2937" s="T225">v</ta>
            <ta e="T227" id="Seg_2938" s="T226">que</ta>
            <ta e="T228" id="Seg_2939" s="T227">v</ta>
            <ta e="T229" id="Seg_2940" s="T228">v</ta>
            <ta e="T230" id="Seg_2941" s="T229">n</ta>
            <ta e="T231" id="Seg_2942" s="T230">n</ta>
            <ta e="T232" id="Seg_2943" s="T231">v</ta>
            <ta e="T233" id="Seg_2944" s="T232">v</ta>
            <ta e="T234" id="Seg_2945" s="T233">dempro</ta>
            <ta e="T235" id="Seg_2946" s="T234">n</ta>
            <ta e="T236" id="Seg_2947" s="T235">v</ta>
            <ta e="T237" id="Seg_2948" s="T236">n</ta>
            <ta e="T238" id="Seg_2949" s="T237">v</ta>
            <ta e="T239" id="Seg_2950" s="T238">n</ta>
            <ta e="T240" id="Seg_2951" s="T239">n</ta>
            <ta e="T241" id="Seg_2952" s="T240">v</ta>
            <ta e="T242" id="Seg_2953" s="T241">v</ta>
            <ta e="T243" id="Seg_2954" s="T242">v</ta>
            <ta e="T244" id="Seg_2955" s="T243">v</ta>
            <ta e="T245" id="Seg_2956" s="T244">adj</ta>
            <ta e="T246" id="Seg_2957" s="T245">v</ta>
            <ta e="T247" id="Seg_2958" s="T246">aux</ta>
            <ta e="T248" id="Seg_2959" s="T247">v</ta>
            <ta e="T249" id="Seg_2960" s="T248">adj</ta>
            <ta e="T250" id="Seg_2961" s="T249">ptcl</ta>
            <ta e="T251" id="Seg_2962" s="T250">v</ta>
            <ta e="T252" id="Seg_2963" s="T251">adj</ta>
            <ta e="T253" id="Seg_2964" s="T252">v</ta>
            <ta e="T254" id="Seg_2965" s="T253">v</ta>
            <ta e="T255" id="Seg_2966" s="T254">v</ta>
            <ta e="T256" id="Seg_2967" s="T255">adj</ta>
            <ta e="T257" id="Seg_2968" s="T256">n</ta>
            <ta e="T258" id="Seg_2969" s="T257">n</ta>
            <ta e="T259" id="Seg_2970" s="T258">v</ta>
            <ta e="T260" id="Seg_2971" s="T259">v</ta>
            <ta e="T261" id="Seg_2972" s="T260">v</ta>
            <ta e="T262" id="Seg_2973" s="T261">adj</ta>
            <ta e="T263" id="Seg_2974" s="T262">n</ta>
            <ta e="T264" id="Seg_2975" s="T263">n</ta>
            <ta e="T265" id="Seg_2976" s="T264">v</ta>
            <ta e="T266" id="Seg_2977" s="T265">adj</ta>
            <ta e="T267" id="Seg_2978" s="T266">n</ta>
            <ta e="T268" id="Seg_2979" s="T267">v</ta>
            <ta e="T269" id="Seg_2980" s="T268">n</ta>
            <ta e="T270" id="Seg_2981" s="T269">v</ta>
            <ta e="T271" id="Seg_2982" s="T270">n</ta>
            <ta e="T272" id="Seg_2983" s="T271">n</ta>
            <ta e="T273" id="Seg_2984" s="T272">v</ta>
            <ta e="T274" id="Seg_2985" s="T273">v</ta>
            <ta e="T275" id="Seg_2986" s="T274">v</ta>
            <ta e="T276" id="Seg_2987" s="T275">adj</ta>
            <ta e="T277" id="Seg_2988" s="T276">adj</ta>
            <ta e="T278" id="Seg_2989" s="T277">n</ta>
            <ta e="T279" id="Seg_2990" s="T278">adj</ta>
            <ta e="T280" id="Seg_2991" s="T279">v</ta>
            <ta e="T281" id="Seg_2992" s="T280">v</ta>
            <ta e="T282" id="Seg_2993" s="T281">adj</ta>
            <ta e="T283" id="Seg_2994" s="T282">n</ta>
            <ta e="T284" id="Seg_2995" s="T283">v</ta>
            <ta e="T285" id="Seg_2996" s="T284">n</ta>
            <ta e="T286" id="Seg_2997" s="T285">v</ta>
            <ta e="T287" id="Seg_2998" s="T286">v</ta>
            <ta e="T288" id="Seg_2999" s="T287">n</ta>
            <ta e="T289" id="Seg_3000" s="T288">adj</ta>
            <ta e="T290" id="Seg_3001" s="T289">v</ta>
            <ta e="T291" id="Seg_3002" s="T290">v</ta>
            <ta e="T292" id="Seg_3003" s="T291">n</ta>
            <ta e="T293" id="Seg_3004" s="T292">n</ta>
            <ta e="T294" id="Seg_3005" s="T293">n</ta>
            <ta e="T295" id="Seg_3006" s="T294">v</ta>
            <ta e="T296" id="Seg_3007" s="T295">v</ta>
            <ta e="T297" id="Seg_3008" s="T296">n</ta>
            <ta e="T298" id="Seg_3009" s="T297">v</ta>
            <ta e="T299" id="Seg_3010" s="T298">adj</ta>
            <ta e="T300" id="Seg_3011" s="T299">v</ta>
            <ta e="T301" id="Seg_3012" s="T300">dempro</ta>
            <ta e="T302" id="Seg_3013" s="T301">v</ta>
            <ta e="T303" id="Seg_3014" s="T302">adj</ta>
            <ta e="T304" id="Seg_3015" s="T303">v</ta>
            <ta e="T305" id="Seg_3016" s="T304">adj</ta>
            <ta e="T306" id="Seg_3017" s="T305">n</ta>
            <ta e="T307" id="Seg_3018" s="T306">v</ta>
            <ta e="T308" id="Seg_3019" s="T307">v</ta>
            <ta e="T309" id="Seg_3020" s="T308">n</ta>
            <ta e="T310" id="Seg_3021" s="T309">v</ta>
            <ta e="T311" id="Seg_3022" s="T310">v</ta>
            <ta e="T312" id="Seg_3023" s="T311">n</ta>
            <ta e="T313" id="Seg_3024" s="T312">n</ta>
            <ta e="T314" id="Seg_3025" s="T313">v</ta>
            <ta e="T315" id="Seg_3026" s="T314">v</ta>
            <ta e="T316" id="Seg_3027" s="T315">n</ta>
            <ta e="T317" id="Seg_3028" s="T316">v</ta>
            <ta e="T318" id="Seg_3029" s="T317">propr</ta>
            <ta e="T319" id="Seg_3030" s="T318">propr</ta>
            <ta e="T320" id="Seg_3031" s="T319">quant</ta>
            <ta e="T321" id="Seg_3032" s="T320">n</ta>
            <ta e="T322" id="Seg_3033" s="T321">v</ta>
            <ta e="T323" id="Seg_3034" s="T322">quant</ta>
            <ta e="T324" id="Seg_3035" s="T323">n</ta>
            <ta e="T325" id="Seg_3036" s="T324">v</ta>
            <ta e="T326" id="Seg_3037" s="T325">n</ta>
            <ta e="T327" id="Seg_3038" s="T326">v</ta>
            <ta e="T328" id="Seg_3039" s="T327">ptcl</ta>
            <ta e="T329" id="Seg_3040" s="T328">adv</ta>
            <ta e="T330" id="Seg_3041" s="T329">pers</ta>
            <ta e="T331" id="Seg_3042" s="T330">pers</ta>
            <ta e="T332" id="Seg_3043" s="T331">n</ta>
            <ta e="T333" id="Seg_3044" s="T332">ptcl</ta>
            <ta e="T334" id="Seg_3045" s="T333">adv</ta>
            <ta e="T335" id="Seg_3046" s="T334">pers</ta>
            <ta e="T336" id="Seg_3047" s="T335">pers</ta>
            <ta e="T337" id="Seg_3048" s="T336">n</ta>
            <ta e="T338" id="Seg_3049" s="T337">adv</ta>
            <ta e="T339" id="Seg_3050" s="T338">n</ta>
            <ta e="T340" id="Seg_3051" s="T339">n</ta>
            <ta e="T341" id="Seg_3052" s="T340">adv</ta>
            <ta e="T342" id="Seg_3053" s="T341">pers</ta>
            <ta e="T343" id="Seg_3054" s="T342">v</ta>
            <ta e="T344" id="Seg_3055" s="T343">adj</ta>
            <ta e="T345" id="Seg_3056" s="T344">n</ta>
            <ta e="T346" id="Seg_3057" s="T345">n</ta>
            <ta e="T347" id="Seg_3058" s="T346">v</ta>
            <ta e="T348" id="Seg_3059" s="T347">n</ta>
            <ta e="T349" id="Seg_3060" s="T348">n</ta>
            <ta e="T350" id="Seg_3061" s="T349">v</ta>
            <ta e="T351" id="Seg_3062" s="T350">adj</ta>
            <ta e="T352" id="Seg_3063" s="T351">n</ta>
            <ta e="T353" id="Seg_3064" s="T352">v</ta>
            <ta e="T354" id="Seg_3065" s="T353">v</ta>
            <ta e="T355" id="Seg_3066" s="T354">v</ta>
            <ta e="T356" id="Seg_3067" s="T355">n</ta>
            <ta e="T357" id="Seg_3068" s="T356">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T112" id="Seg_3069" s="T111">np.h:Th</ta>
            <ta e="T114" id="Seg_3070" s="T113">np.h:Th 0.3.h:Poss</ta>
            <ta e="T117" id="Seg_3071" s="T116">np:P 0.3.h:Poss</ta>
            <ta e="T118" id="Seg_3072" s="T117">0.3.h:A</ta>
            <ta e="T121" id="Seg_3073" s="T120">np:Th 0.3.h:Poss</ta>
            <ta e="T122" id="Seg_3074" s="T121">0.3.h:A</ta>
            <ta e="T124" id="Seg_3075" s="T123">np:Th 0.3.h:Poss</ta>
            <ta e="T125" id="Seg_3076" s="T124">0.3.h:A</ta>
            <ta e="T127" id="Seg_3077" s="T126">np:Th 0.3.h:Poss</ta>
            <ta e="T128" id="Seg_3078" s="T127">0.3.h:A</ta>
            <ta e="T129" id="Seg_3079" s="T128">0.3.h:A</ta>
            <ta e="T130" id="Seg_3080" s="T129">0.3.h:A</ta>
            <ta e="T131" id="Seg_3081" s="T130">np:Th</ta>
            <ta e="T133" id="Seg_3082" s="T132">pro.h:Th</ta>
            <ta e="T134" id="Seg_3083" s="T133">0.2.h:A</ta>
            <ta e="T136" id="Seg_3084" s="T135">0.2.h:A</ta>
            <ta e="T138" id="Seg_3085" s="T137">np:G</ta>
            <ta e="T139" id="Seg_3086" s="T138">0.3.h:A</ta>
            <ta e="T140" id="Seg_3087" s="T139">np:Th</ta>
            <ta e="T142" id="Seg_3088" s="T141">pro.h:Th</ta>
            <ta e="T143" id="Seg_3089" s="T142">0.2.h:A</ta>
            <ta e="T145" id="Seg_3090" s="T144">0.2.h:A</ta>
            <ta e="T147" id="Seg_3091" s="T146">np:G</ta>
            <ta e="T148" id="Seg_3092" s="T147">0.3.h:A</ta>
            <ta e="T150" id="Seg_3093" s="T149">np:G</ta>
            <ta e="T151" id="Seg_3094" s="T150">adv:Time</ta>
            <ta e="T152" id="Seg_3095" s="T151">np:Th</ta>
            <ta e="T154" id="Seg_3096" s="T153">0.2.h:A</ta>
            <ta e="T156" id="Seg_3097" s="T155">0.2.h:A</ta>
            <ta e="T158" id="Seg_3098" s="T157">np:G</ta>
            <ta e="T159" id="Seg_3099" s="T158">adv:Time</ta>
            <ta e="T160" id="Seg_3100" s="T159">np:Th</ta>
            <ta e="T163" id="Seg_3101" s="T162">np:P</ta>
            <ta e="T165" id="Seg_3102" s="T164">0.3.h:A</ta>
            <ta e="T167" id="Seg_3103" s="T166">np:G 0.3.h:Poss</ta>
            <ta e="T169" id="Seg_3104" s="T168">np:Ins</ta>
            <ta e="T170" id="Seg_3105" s="T169">0.3.h:A</ta>
            <ta e="T171" id="Seg_3106" s="T170">np:Ins 0.3.h:Poss</ta>
            <ta e="T173" id="Seg_3107" s="T172">0.3.h:A</ta>
            <ta e="T174" id="Seg_3108" s="T173">0.3.h:E</ta>
            <ta e="T175" id="Seg_3109" s="T174">np:Poss</ta>
            <ta e="T176" id="Seg_3110" s="T175">np:L</ta>
            <ta e="T177" id="Seg_3111" s="T176">np:Th</ta>
            <ta e="T180" id="Seg_3112" s="T179">np:G</ta>
            <ta e="T182" id="Seg_3113" s="T181">0.3.h:A</ta>
            <ta e="T183" id="Seg_3114" s="T182">0.3.h:A</ta>
            <ta e="T184" id="Seg_3115" s="T183">np:P</ta>
            <ta e="T185" id="Seg_3116" s="T184">0.3.h:A</ta>
            <ta e="T186" id="Seg_3117" s="T185">np:P 0.3.h:Poss</ta>
            <ta e="T187" id="Seg_3118" s="T186">0.3.h:A</ta>
            <ta e="T188" id="Seg_3119" s="T187">0.3.h:Th</ta>
            <ta e="T189" id="Seg_3120" s="T188">np.h:A</ta>
            <ta e="T190" id="Seg_3121" s="T189">adv:L</ta>
            <ta e="T191" id="Seg_3122" s="T190">0.3.h:A</ta>
            <ta e="T192" id="Seg_3123" s="T191">0.3.h:E</ta>
            <ta e="T193" id="Seg_3124" s="T192">pro.h:Th</ta>
            <ta e="T194" id="Seg_3125" s="T193">adv:L</ta>
            <ta e="T196" id="Seg_3126" s="T195">np:P 0.3.h:Poss</ta>
            <ta e="T197" id="Seg_3127" s="T196">0.3.h:A</ta>
            <ta e="T198" id="Seg_3128" s="T197">pro.h:Poss</ta>
            <ta e="T200" id="Seg_3129" s="T199">np:P</ta>
            <ta e="T201" id="Seg_3130" s="T200">0.3.h:A</ta>
            <ta e="T203" id="Seg_3131" s="T202">0.3.h:A</ta>
            <ta e="T204" id="Seg_3132" s="T203">np:G 0.3:Poss</ta>
            <ta e="T206" id="Seg_3133" s="T205">np.h:Th 0.3.h:Poss</ta>
            <ta e="T207" id="Seg_3134" s="T206">0.3.h:A</ta>
            <ta e="T209" id="Seg_3135" s="T208">0.3.h:P 0.3.h:E</ta>
            <ta e="T212" id="Seg_3136" s="T211">0.2.h:Th</ta>
            <ta e="T215" id="Seg_3137" s="T214">np.h:Th</ta>
            <ta e="T216" id="Seg_3138" s="T215">np.h:Poss</ta>
            <ta e="T217" id="Seg_3139" s="T216">np:P</ta>
            <ta e="T218" id="Seg_3140" s="T217">0.3.h:A</ta>
            <ta e="T222" id="Seg_3141" s="T221">0.3.h:A</ta>
            <ta e="T223" id="Seg_3142" s="T222">0.3.h:A</ta>
            <ta e="T226" id="Seg_3143" s="T225">0.2.h:Th</ta>
            <ta e="T227" id="Seg_3144" s="T226">pro:Th</ta>
            <ta e="T229" id="Seg_3145" s="T228">0.2.h:A</ta>
            <ta e="T230" id="Seg_3146" s="T229">np.h:Poss</ta>
            <ta e="T231" id="Seg_3147" s="T230">np.h:Th</ta>
            <ta e="T233" id="Seg_3148" s="T232">0.1.h:A</ta>
            <ta e="T235" id="Seg_3149" s="T234">np.h:A</ta>
            <ta e="T237" id="Seg_3150" s="T236">np.h:R</ta>
            <ta e="T239" id="Seg_3151" s="T238">np.h:Poss</ta>
            <ta e="T240" id="Seg_3152" s="T239">np.h:Th</ta>
            <ta e="T242" id="Seg_3153" s="T241">0.1.h:A</ta>
            <ta e="T244" id="Seg_3154" s="T243">0.2.h:A</ta>
            <ta e="T246" id="Seg_3155" s="T245">0.2.h:A</ta>
            <ta e="T247" id="Seg_3156" s="T246">0.1.h:A</ta>
            <ta e="T251" id="Seg_3157" s="T250">0.3.h:A</ta>
            <ta e="T253" id="Seg_3158" s="T252">0.1.h:A</ta>
            <ta e="T255" id="Seg_3159" s="T254">0.2.h:A</ta>
            <ta e="T257" id="Seg_3160" s="T256">np:Th</ta>
            <ta e="T258" id="Seg_3161" s="T257">np:Com 0.3.h:Poss</ta>
            <ta e="T260" id="Seg_3162" s="T259">0.3:A 0.3.h:P</ta>
            <ta e="T261" id="Seg_3163" s="T260">0.3.h:A</ta>
            <ta e="T263" id="Seg_3164" s="T262">np:Th 0.3.h:Poss</ta>
            <ta e="T264" id="Seg_3165" s="T263">np:A</ta>
            <ta e="T267" id="Seg_3166" s="T266">np:P 0.3.h:Poss</ta>
            <ta e="T268" id="Seg_3167" s="T267">0.3.h:A</ta>
            <ta e="T269" id="Seg_3168" s="T268">np:Th</ta>
            <ta e="T270" id="Seg_3169" s="T269">0.3.h:A</ta>
            <ta e="T271" id="Seg_3170" s="T270">np:A</ta>
            <ta e="T272" id="Seg_3171" s="T271">np:P</ta>
            <ta e="T275" id="Seg_3172" s="T274">0.2.h:A</ta>
            <ta e="T276" id="Seg_3173" s="T275">np:Th</ta>
            <ta e="T278" id="Seg_3174" s="T277">np:Com 0.3.h:Poss</ta>
            <ta e="T281" id="Seg_3175" s="T280">0.3.h:Poss</ta>
            <ta e="T283" id="Seg_3176" s="T282">np:P 0.3.h:Poss</ta>
            <ta e="T284" id="Seg_3177" s="T283">0.3.h:A</ta>
            <ta e="T285" id="Seg_3178" s="T284">np:Th 0.3.h:Poss</ta>
            <ta e="T287" id="Seg_3179" s="T286">0.3.h:A</ta>
            <ta e="T288" id="Seg_3180" s="T287">np:A</ta>
            <ta e="T289" id="Seg_3181" s="T288">np:P</ta>
            <ta e="T293" id="Seg_3182" s="T291">np:G</ta>
            <ta e="T294" id="Seg_3183" s="T293">np.h:Th</ta>
            <ta e="T297" id="Seg_3184" s="T296">np.h:Th</ta>
            <ta e="T298" id="Seg_3185" s="T297">0.3.h:A</ta>
            <ta e="T300" id="Seg_3186" s="T299">0.1.h:A</ta>
            <ta e="T301" id="Seg_3187" s="T300">pro.h:P</ta>
            <ta e="T302" id="Seg_3188" s="T301">0.3.h:A</ta>
            <ta e="T304" id="Seg_3189" s="T303">0.3.h:E</ta>
            <ta e="T306" id="Seg_3190" s="T305">np:P 0.3.h:Poss</ta>
            <ta e="T309" id="Seg_3191" s="T308">np:Th 0.3.h:Poss</ta>
            <ta e="T311" id="Seg_3192" s="T310">0.3.h:A</ta>
            <ta e="T312" id="Seg_3193" s="T311">np.h:P</ta>
            <ta e="T313" id="Seg_3194" s="T312">np:Th</ta>
            <ta e="T316" id="Seg_3195" s="T315">np.h:A</ta>
            <ta e="T318" id="Seg_3196" s="T317">np:Th</ta>
            <ta e="T319" id="Seg_3197" s="T318">np:Th</ta>
            <ta e="T321" id="Seg_3198" s="T320">np:Th 0.1.h:Poss</ta>
            <ta e="T322" id="Seg_3199" s="T321">0.1.h:A</ta>
            <ta e="T324" id="Seg_3200" s="T323">np:Th 0.1.h:Poss</ta>
            <ta e="T325" id="Seg_3201" s="T324">0.1.h:A</ta>
            <ta e="T326" id="Seg_3202" s="T325">np.h:Th 0.1.h:Poss</ta>
            <ta e="T327" id="Seg_3203" s="T326">0.1.h:A</ta>
            <ta e="T330" id="Seg_3204" s="T329">pro.h:Th</ta>
            <ta e="T331" id="Seg_3205" s="T330">pro.h:Poss</ta>
            <ta e="T332" id="Seg_3206" s="T331">np:Th</ta>
            <ta e="T335" id="Seg_3207" s="T334">pro.h:Th</ta>
            <ta e="T336" id="Seg_3208" s="T335">pro.h:Poss</ta>
            <ta e="T337" id="Seg_3209" s="T336">pro:Th</ta>
            <ta e="T340" id="Seg_3210" s="T339">np.h:Th 0.2.h:Poss</ta>
            <ta e="T342" id="Seg_3211" s="T341">pro.h:Th</ta>
            <ta e="T343" id="Seg_3212" s="T342">0.3.h:A</ta>
            <ta e="T345" id="Seg_3213" s="T344">np:Th 0.3.h:Poss</ta>
            <ta e="T346" id="Seg_3214" s="T345">np:P 0.3.h:Poss</ta>
            <ta e="T347" id="Seg_3215" s="T346">0.3.h:A</ta>
            <ta e="T348" id="Seg_3216" s="T347">np.h:A</ta>
            <ta e="T349" id="Seg_3217" s="T348">np.h:Th 0.3.h:Poss</ta>
            <ta e="T352" id="Seg_3218" s="T351">np:G 0.3.h:Poss</ta>
            <ta e="T353" id="Seg_3219" s="T352">0.3.h:A</ta>
            <ta e="T355" id="Seg_3220" s="T354">0.3.h:A</ta>
            <ta e="T356" id="Seg_3221" s="T355">np:G 0.3.h:Poss</ta>
            <ta e="T357" id="Seg_3222" s="T356">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T112" id="Seg_3223" s="T111">np.h:S</ta>
            <ta e="T113" id="Seg_3224" s="T112">v:pred</ta>
            <ta e="T114" id="Seg_3225" s="T113">np:S</ta>
            <ta e="T115" id="Seg_3226" s="T114">n:pred</ta>
            <ta e="T117" id="Seg_3227" s="T116">np:O</ta>
            <ta e="T118" id="Seg_3228" s="T117">v:pred 0.3.h:S</ta>
            <ta e="T119" id="Seg_3229" s="T118">adj:pred</ta>
            <ta e="T120" id="Seg_3230" s="T119">cop</ta>
            <ta e="T121" id="Seg_3231" s="T120">np:S</ta>
            <ta e="T122" id="Seg_3232" s="T121">v:pred 0.3.h:S</ta>
            <ta e="T124" id="Seg_3233" s="T123">np:O</ta>
            <ta e="T125" id="Seg_3234" s="T124">v:pred 0.3.h:S</ta>
            <ta e="T127" id="Seg_3235" s="T126">np:O</ta>
            <ta e="T128" id="Seg_3236" s="T127">v:pred 0.3.h:S</ta>
            <ta e="T129" id="Seg_3237" s="T128">v:pred 0.3.h:S</ta>
            <ta e="T130" id="Seg_3238" s="T129">v:pred 0.3.h:S</ta>
            <ta e="T131" id="Seg_3239" s="T130">np:S</ta>
            <ta e="T132" id="Seg_3240" s="T131">v:pred</ta>
            <ta e="T133" id="Seg_3241" s="T132">pro:O</ta>
            <ta e="T134" id="Seg_3242" s="T133">v:pred 0.2.h:S</ta>
            <ta e="T135" id="Seg_3243" s="T134">s:purp</ta>
            <ta e="T136" id="Seg_3244" s="T135">v:pred 0.2.h:S</ta>
            <ta e="T139" id="Seg_3245" s="T138">v:pred 0.3.h:S</ta>
            <ta e="T140" id="Seg_3246" s="T139">np:S</ta>
            <ta e="T141" id="Seg_3247" s="T140">v:pred</ta>
            <ta e="T142" id="Seg_3248" s="T141">pro:S</ta>
            <ta e="T143" id="Seg_3249" s="T142">v:pred 0.2.h:S</ta>
            <ta e="T144" id="Seg_3250" s="T143">s:purp</ta>
            <ta e="T145" id="Seg_3251" s="T144">v:pred 0.2.h:S</ta>
            <ta e="T148" id="Seg_3252" s="T147">v:pred 0.3:S</ta>
            <ta e="T152" id="Seg_3253" s="T151">np:S</ta>
            <ta e="T153" id="Seg_3254" s="T152">v:pred</ta>
            <ta e="T154" id="Seg_3255" s="T153">v:pred 0.2.h:S</ta>
            <ta e="T155" id="Seg_3256" s="T154">s:purp</ta>
            <ta e="T156" id="Seg_3257" s="T155">v:pred 0.2.h:S</ta>
            <ta e="T160" id="Seg_3258" s="T159">np:S</ta>
            <ta e="T161" id="Seg_3259" s="T160">v:pred</ta>
            <ta e="T163" id="Seg_3260" s="T162">np:O</ta>
            <ta e="T164" id="Seg_3261" s="T163">conv:pred</ta>
            <ta e="T165" id="Seg_3262" s="T164">v:pred 0.3.h:S</ta>
            <ta e="T170" id="Seg_3263" s="T169">v:pred 0.3.h:S</ta>
            <ta e="T172" id="Seg_3264" s="T171">conv:pred</ta>
            <ta e="T173" id="Seg_3265" s="T172">v:pred 0.3.h:S</ta>
            <ta e="T174" id="Seg_3266" s="T173">s:temp</ta>
            <ta e="T177" id="Seg_3267" s="T176">np:S</ta>
            <ta e="T178" id="Seg_3268" s="T177">v:pred</ta>
            <ta e="T182" id="Seg_3269" s="T180">s:adv</ta>
            <ta e="T183" id="Seg_3270" s="T182">v:pred 0.3.h:S</ta>
            <ta e="T184" id="Seg_3271" s="T183">np:O</ta>
            <ta e="T185" id="Seg_3272" s="T184">v:pred 0.3.h:S</ta>
            <ta e="T186" id="Seg_3273" s="T185">np:O</ta>
            <ta e="T187" id="Seg_3274" s="T186">v:pred 0.3.h:S</ta>
            <ta e="T188" id="Seg_3275" s="T187">v:pred 0.3.h:S</ta>
            <ta e="T189" id="Seg_3276" s="T188">np.h:S</ta>
            <ta e="T191" id="Seg_3277" s="T190">v:pred</ta>
            <ta e="T192" id="Seg_3278" s="T191">v:pred 0.3.h:S</ta>
            <ta e="T193" id="Seg_3279" s="T192">pro.h:S</ta>
            <ta e="T195" id="Seg_3280" s="T194">v:pred</ta>
            <ta e="T196" id="Seg_3281" s="T195">np:O</ta>
            <ta e="T197" id="Seg_3282" s="T196">v:pred 0.3.h:S</ta>
            <ta e="T200" id="Seg_3283" s="T199">np:O</ta>
            <ta e="T201" id="Seg_3284" s="T200">v:pred 0.3.h:S</ta>
            <ta e="T203" id="Seg_3285" s="T201">s:adv</ta>
            <ta e="T206" id="Seg_3286" s="T205">np:O</ta>
            <ta e="T207" id="Seg_3287" s="T206">v:pred 0.3.h:S</ta>
            <ta e="T208" id="Seg_3288" s="T207">conv:pred</ta>
            <ta e="T209" id="Seg_3289" s="T208">v:pred 0.2.h:S</ta>
            <ta e="T210" id="Seg_3290" s="T209">n:pred</ta>
            <ta e="T212" id="Seg_3291" s="T211">0.2.h:S</ta>
            <ta e="T215" id="Seg_3292" s="T212">s:adv</ta>
            <ta e="T217" id="Seg_3293" s="T216">np:O</ta>
            <ta e="T218" id="Seg_3294" s="T217">v:pred 0.3.h:S</ta>
            <ta e="T219" id="Seg_3295" s="T218">pro:S</ta>
            <ta e="T221" id="Seg_3296" s="T220">ptcl:pred</ta>
            <ta e="T222" id="Seg_3297" s="T221">v:pred 0.3.h:S</ta>
            <ta e="T223" id="Seg_3298" s="T222">v:pred 0.3.h:S</ta>
            <ta e="T224" id="Seg_3299" s="T223">adj:pred</ta>
            <ta e="T225" id="Seg_3300" s="T224">np.h:S</ta>
            <ta e="T226" id="Seg_3301" s="T225">cop</ta>
            <ta e="T227" id="Seg_3302" s="T226">pro:O</ta>
            <ta e="T228" id="Seg_3303" s="T227">conv:pred</ta>
            <ta e="T229" id="Seg_3304" s="T228">v:pred 0.2.h:S</ta>
            <ta e="T232" id="Seg_3305" s="T229">s:purp</ta>
            <ta e="T233" id="Seg_3306" s="T232">0.1.h:S v:pred </ta>
            <ta e="T235" id="Seg_3307" s="T234">np.h:S</ta>
            <ta e="T236" id="Seg_3308" s="T235">v:pred</ta>
            <ta e="T238" id="Seg_3309" s="T237">v:pred 0.3.h:S</ta>
            <ta e="T241" id="Seg_3310" s="T238">s:purp</ta>
            <ta e="T242" id="Seg_3311" s="T241">v:pred 0.1.h:S</ta>
            <ta e="T243" id="Seg_3312" s="T242">conv:pred</ta>
            <ta e="T244" id="Seg_3313" s="T243">v:pred 0.2.h:S</ta>
            <ta e="T246" id="Seg_3314" s="T245">v:pred 0.2.h:S</ta>
            <ta e="T247" id="Seg_3315" s="T246">v:pred 0.1.h:S</ta>
            <ta e="T251" id="Seg_3316" s="T248">s:cond</ta>
            <ta e="T253" id="Seg_3317" s="T252">v:pred 0.1.h:S</ta>
            <ta e="T254" id="Seg_3318" s="T253">conv:pred</ta>
            <ta e="T255" id="Seg_3319" s="T254">v:pred 0.2.h:S</ta>
            <ta e="T257" id="Seg_3320" s="T256">np:O</ta>
            <ta e="T259" id="Seg_3321" s="T258">conv:pred</ta>
            <ta e="T260" id="Seg_3322" s="T259">v:pred 0.3:S 0.3.h:O</ta>
            <ta e="T261" id="Seg_3323" s="T260">v:pred 0.3.h:S</ta>
            <ta e="T263" id="Seg_3324" s="T262">np:O</ta>
            <ta e="T264" id="Seg_3325" s="T263">np:S</ta>
            <ta e="T265" id="Seg_3326" s="T264">v:pred</ta>
            <ta e="T267" id="Seg_3327" s="T266">np:O</ta>
            <ta e="T268" id="Seg_3328" s="T267">v:pred 0.3.h:S</ta>
            <ta e="T269" id="Seg_3329" s="T268">np:O</ta>
            <ta e="T270" id="Seg_3330" s="T269">v:pred 0.3.h:S</ta>
            <ta e="T271" id="Seg_3331" s="T270">np:S</ta>
            <ta e="T272" id="Seg_3332" s="T271">np:O</ta>
            <ta e="T273" id="Seg_3333" s="T272">conv:pred</ta>
            <ta e="T274" id="Seg_3334" s="T273">v:pred</ta>
            <ta e="T275" id="Seg_3335" s="T274">v:pred 0.2.h:S</ta>
            <ta e="T277" id="Seg_3336" s="T276">np:O</ta>
            <ta e="T280" id="Seg_3337" s="T279">conv:pred</ta>
            <ta e="T281" id="Seg_3338" s="T280">v:pred 0.3:S 0.3.h:O</ta>
            <ta e="T283" id="Seg_3339" s="T282">np:O</ta>
            <ta e="T284" id="Seg_3340" s="T283">v:pred 0.3.h:S</ta>
            <ta e="T285" id="Seg_3341" s="T284">np:O</ta>
            <ta e="T286" id="Seg_3342" s="T285">conv:pred</ta>
            <ta e="T287" id="Seg_3343" s="T286">v:pred 0.3.h:S</ta>
            <ta e="T288" id="Seg_3344" s="T287">np:S</ta>
            <ta e="T289" id="Seg_3345" s="T288">np:O</ta>
            <ta e="T290" id="Seg_3346" s="T289">conv:pred</ta>
            <ta e="T291" id="Seg_3347" s="T290">v:pred</ta>
            <ta e="T294" id="Seg_3348" s="T293">np.h:S</ta>
            <ta e="T295" id="Seg_3349" s="T294">conv:pred</ta>
            <ta e="T296" id="Seg_3350" s="T295">v:pred</ta>
            <ta e="T297" id="Seg_3351" s="T296">np:O</ta>
            <ta e="T298" id="Seg_3352" s="T297">v:pred 0.3.h:S</ta>
            <ta e="T299" id="Seg_3353" s="T298">conv:pred</ta>
            <ta e="T300" id="Seg_3354" s="T299">v:pred 0.1.h:S</ta>
            <ta e="T301" id="Seg_3355" s="T300">pro.h:O</ta>
            <ta e="T302" id="Seg_3356" s="T301">v:pred 0.3.h:S</ta>
            <ta e="T304" id="Seg_3357" s="T303">v:pred 0.3.h:S</ta>
            <ta e="T306" id="Seg_3358" s="T305">np:O</ta>
            <ta e="T307" id="Seg_3359" s="T306">conv:pred</ta>
            <ta e="T308" id="Seg_3360" s="T307">v:pred 0.3.h:S</ta>
            <ta e="T309" id="Seg_3361" s="T308">np:O</ta>
            <ta e="T310" id="Seg_3362" s="T309">conv:pred</ta>
            <ta e="T311" id="Seg_3363" s="T310">v:pred 0.3.h:S</ta>
            <ta e="T312" id="Seg_3364" s="T311">np.h:O</ta>
            <ta e="T313" id="Seg_3365" s="T312">np:S</ta>
            <ta e="T314" id="Seg_3366" s="T313">conv:pred</ta>
            <ta e="T315" id="Seg_3367" s="T314">v:pred</ta>
            <ta e="T316" id="Seg_3368" s="T315">np.h:S</ta>
            <ta e="T317" id="Seg_3369" s="T316">v:pred</ta>
            <ta e="T321" id="Seg_3370" s="T320">np:O</ta>
            <ta e="T322" id="Seg_3371" s="T321">v:pred 0.1.h:S</ta>
            <ta e="T324" id="Seg_3372" s="T323">np:O</ta>
            <ta e="T325" id="Seg_3373" s="T324">v:pred 0.1.h:S</ta>
            <ta e="T326" id="Seg_3374" s="T325">np.h:O</ta>
            <ta e="T327" id="Seg_3375" s="T326">v:pred 0.1.h:S</ta>
            <ta e="T328" id="Seg_3376" s="T327">ptcl.neg</ta>
            <ta e="T329" id="Seg_3377" s="T328">ptcl:pred</ta>
            <ta e="T332" id="Seg_3378" s="T331">np:S</ta>
            <ta e="T334" id="Seg_3379" s="T333">ptcl:pred</ta>
            <ta e="T337" id="Seg_3380" s="T336">np:S</ta>
            <ta e="T340" id="Seg_3381" s="T339">np.h:S</ta>
            <ta e="T341" id="Seg_3382" s="T340">ptcl:pred</ta>
            <ta e="T343" id="Seg_3383" s="T342">v:pred 0.3.h:S</ta>
            <ta e="T345" id="Seg_3384" s="T344">np:O</ta>
            <ta e="T346" id="Seg_3385" s="T345">np:O</ta>
            <ta e="T347" id="Seg_3386" s="T346">v:pred 0.3.h:S</ta>
            <ta e="T348" id="Seg_3387" s="T347">np.h:S</ta>
            <ta e="T349" id="Seg_3388" s="T348">np.h:O</ta>
            <ta e="T350" id="Seg_3389" s="T349">v:pred</ta>
            <ta e="T352" id="Seg_3390" s="T351">np:O</ta>
            <ta e="T353" id="Seg_3391" s="T352">v:pred 0.3.h:S</ta>
            <ta e="T354" id="Seg_3392" s="T353">conv:pred</ta>
            <ta e="T355" id="Seg_3393" s="T354">v:pred 0.3.h:S</ta>
            <ta e="T357" id="Seg_3394" s="T356">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T112" id="Seg_3395" s="T111">new </ta>
            <ta e="T114" id="Seg_3396" s="T113">giv-active </ta>
            <ta e="T115" id="Seg_3397" s="T114">accs-sit </ta>
            <ta e="T117" id="Seg_3398" s="T116">new </ta>
            <ta e="T118" id="Seg_3399" s="T117">0.giv-active</ta>
            <ta e="T121" id="Seg_3400" s="T120">giv-active </ta>
            <ta e="T122" id="Seg_3401" s="T121">0.giv-inactive</ta>
            <ta e="T124" id="Seg_3402" s="T123">new</ta>
            <ta e="T125" id="Seg_3403" s="T124">0.giv-active</ta>
            <ta e="T127" id="Seg_3404" s="T126">new </ta>
            <ta e="T128" id="Seg_3405" s="T127">0.giv-active</ta>
            <ta e="T129" id="Seg_3406" s="T128">0.giv-active</ta>
            <ta e="T130" id="Seg_3407" s="T129">0.giv-active</ta>
            <ta e="T131" id="Seg_3408" s="T130">new </ta>
            <ta e="T133" id="Seg_3409" s="T132">giv-active-Q</ta>
            <ta e="T134" id="Seg_3410" s="T133">0.giv-inactive-Q</ta>
            <ta e="T136" id="Seg_3411" s="T135">0.giv-active-Q</ta>
            <ta e="T138" id="Seg_3412" s="T137">giv-inactive-Q</ta>
            <ta e="T139" id="Seg_3413" s="T138">0.giv-active</ta>
            <ta e="T140" id="Seg_3414" s="T139">new</ta>
            <ta e="T142" id="Seg_3415" s="T141">giv-active-Q</ta>
            <ta e="T143" id="Seg_3416" s="T142">0.giv-inactive-Q</ta>
            <ta e="T145" id="Seg_3417" s="T144">0.giv-active-Q</ta>
            <ta e="T147" id="Seg_3418" s="T146">giv-inactive-Q</ta>
            <ta e="T148" id="Seg_3419" s="T147">0.giv-active</ta>
            <ta e="T150" id="Seg_3420" s="T149">giv-active</ta>
            <ta e="T152" id="Seg_3421" s="T151">new </ta>
            <ta e="T154" id="Seg_3422" s="T153">0.giv-inactive-Q</ta>
            <ta e="T156" id="Seg_3423" s="T155">0.giv-active-Q</ta>
            <ta e="T158" id="Seg_3424" s="T157">giv-inactive-Q </ta>
            <ta e="T160" id="Seg_3425" s="T159">new </ta>
            <ta e="T163" id="Seg_3426" s="T162">giv-active</ta>
            <ta e="T165" id="Seg_3427" s="T164">0.giv-inactive</ta>
            <ta e="T167" id="Seg_3428" s="T166">giv-inactive</ta>
            <ta e="T169" id="Seg_3429" s="T168">giv-inactive </ta>
            <ta e="T170" id="Seg_3430" s="T169">0.giv-active</ta>
            <ta e="T171" id="Seg_3431" s="T170">giv-inactive</ta>
            <ta e="T173" id="Seg_3432" s="T172">0.giv-active</ta>
            <ta e="T174" id="Seg_3433" s="T173">0.giv-active</ta>
            <ta e="T176" id="Seg_3434" s="T175">accs-gen </ta>
            <ta e="T177" id="Seg_3435" s="T176">new </ta>
            <ta e="T180" id="Seg_3436" s="T179">giv-active </ta>
            <ta e="T182" id="Seg_3437" s="T181">0.giv-active</ta>
            <ta e="T183" id="Seg_3438" s="T182">0.giv-active</ta>
            <ta e="T184" id="Seg_3439" s="T183">new </ta>
            <ta e="T185" id="Seg_3440" s="T184">0.giv-active</ta>
            <ta e="T186" id="Seg_3441" s="T185">giv-inactive </ta>
            <ta e="T187" id="Seg_3442" s="T186">0.giv-active</ta>
            <ta e="T188" id="Seg_3443" s="T187">0.giv-active</ta>
            <ta e="T189" id="Seg_3444" s="T188">accs-gen </ta>
            <ta e="T192" id="Seg_3445" s="T191">0.giv-active</ta>
            <ta e="T196" id="Seg_3446" s="T195">giv-inactive-Q </ta>
            <ta e="T197" id="Seg_3447" s="T196">0.giv-active-Q</ta>
            <ta e="T200" id="Seg_3448" s="T199">new-Q </ta>
            <ta e="T201" id="Seg_3449" s="T200">0.giv-active-Q</ta>
            <ta e="T204" id="Seg_3450" s="T203">giv-inactive</ta>
            <ta e="T206" id="Seg_3451" s="T205">new </ta>
            <ta e="T207" id="Seg_3452" s="T206">0.giv-active</ta>
            <ta e="T209" id="Seg_3453" s="T208">0.giv-active-Q</ta>
            <ta e="T212" id="Seg_3454" s="T211">0.giv-inactive-Q</ta>
            <ta e="T215" id="Seg_3455" s="T214">giv-inactive-Q</ta>
            <ta e="T217" id="Seg_3456" s="T216">giv-inactive-Q </ta>
            <ta e="T218" id="Seg_3457" s="T217">0.giv-active-Q</ta>
            <ta e="T220" id="Seg_3458" s="T219">0.giv-active-Q</ta>
            <ta e="T222" id="Seg_3459" s="T221">0.giv-active</ta>
            <ta e="T223" id="Seg_3460" s="T222">quot-sp</ta>
            <ta e="T225" id="Seg_3461" s="T224">accs-gen-Q</ta>
            <ta e="T226" id="Seg_3462" s="T225">0.giv-inactive-Q</ta>
            <ta e="T229" id="Seg_3463" s="T228">0.giv-active-Q</ta>
            <ta e="T231" id="Seg_3464" s="T230">new-Q </ta>
            <ta e="T233" id="Seg_3465" s="T232">0.giv-active-Q</ta>
            <ta e="T235" id="Seg_3466" s="T234">giv-inactive</ta>
            <ta e="T237" id="Seg_3467" s="T236">giv-inactive </ta>
            <ta e="T238" id="Seg_3468" s="T237">quot-sp</ta>
            <ta e="T240" id="Seg_3469" s="T239">giv-active-Q </ta>
            <ta e="T242" id="Seg_3470" s="T241">0.giv-active-Q</ta>
            <ta e="T244" id="Seg_3471" s="T243">0.giv-inactive-Q quot-sp</ta>
            <ta e="T246" id="Seg_3472" s="T245">0.giv-inactive-Q</ta>
            <ta e="T247" id="Seg_3473" s="T246">0.giv-active-Q</ta>
            <ta e="T251" id="Seg_3474" s="T250">0.giv-inactive-Q</ta>
            <ta e="T253" id="Seg_3475" s="T252">0.giv-active-Q</ta>
            <ta e="T255" id="Seg_3476" s="T254">0.giv-inactive-Q</ta>
            <ta e="T257" id="Seg_3477" s="T256">new-Q </ta>
            <ta e="T258" id="Seg_3478" s="T257">giv-inactive-Q</ta>
            <ta e="T260" id="Seg_3479" s="T259">0.giv-active-Q</ta>
            <ta e="T261" id="Seg_3480" s="T260">0.giv-active</ta>
            <ta e="T263" id="Seg_3481" s="T262">giv-inactive </ta>
            <ta e="T264" id="Seg_3482" s="T263">giv-active </ta>
            <ta e="T267" id="Seg_3483" s="T266">giv-inactive </ta>
            <ta e="T268" id="Seg_3484" s="T267">0.giv-inactive</ta>
            <ta e="T269" id="Seg_3485" s="T268">giv-inactive </ta>
            <ta e="T271" id="Seg_3486" s="T270">giv-active </ta>
            <ta e="T272" id="Seg_3487" s="T271">giv-inactive </ta>
            <ta e="T275" id="Seg_3488" s="T274">0.giv-inactive-Q</ta>
            <ta e="T276" id="Seg_3489" s="T275">new-Q </ta>
            <ta e="T278" id="Seg_3490" s="T277">giv-inactive-Q </ta>
            <ta e="T281" id="Seg_3491" s="T280">0.giv-active-Q</ta>
            <ta e="T283" id="Seg_3492" s="T282">giv-inactive </ta>
            <ta e="T284" id="Seg_3493" s="T283">0.giv-inactive</ta>
            <ta e="T285" id="Seg_3494" s="T284">giv-inactive </ta>
            <ta e="T287" id="Seg_3495" s="T286">0.giv-active</ta>
            <ta e="T288" id="Seg_3496" s="T287">giv-active </ta>
            <ta e="T289" id="Seg_3497" s="T288">giv-inactive </ta>
            <ta e="T293" id="Seg_3498" s="T291">new</ta>
            <ta e="T294" id="Seg_3499" s="T293">giv-inactive </ta>
            <ta e="T297" id="Seg_3500" s="T296">new </ta>
            <ta e="T298" id="Seg_3501" s="T297">0.giv-active</ta>
            <ta e="T300" id="Seg_3502" s="T299">accs-aggr-Q</ta>
            <ta e="T301" id="Seg_3503" s="T300">giv-inactive-Q </ta>
            <ta e="T302" id="Seg_3504" s="T301">0.giv-active</ta>
            <ta e="T304" id="Seg_3505" s="T303">0.giv-active</ta>
            <ta e="T306" id="Seg_3506" s="T305">giv-inactive </ta>
            <ta e="T308" id="Seg_3507" s="T307">0.giv-active</ta>
            <ta e="T309" id="Seg_3508" s="T308">giv-inactive </ta>
            <ta e="T311" id="Seg_3509" s="T310">0.giv-active</ta>
            <ta e="T312" id="Seg_3510" s="T311">giv-inactive </ta>
            <ta e="T313" id="Seg_3511" s="T312">giv-active </ta>
            <ta e="T316" id="Seg_3512" s="T315">giv-inactive </ta>
            <ta e="T317" id="Seg_3513" s="T316">quot-sp</ta>
            <ta e="T318" id="Seg_3514" s="T317">giv-inactive-Q </ta>
            <ta e="T319" id="Seg_3515" s="T318">giv-inactive-Q </ta>
            <ta e="T321" id="Seg_3516" s="T320">new-Q </ta>
            <ta e="T322" id="Seg_3517" s="T321">0.giv-active-Q</ta>
            <ta e="T324" id="Seg_3518" s="T323">new-Q</ta>
            <ta e="T325" id="Seg_3519" s="T324">0.giv-active-Q</ta>
            <ta e="T326" id="Seg_3520" s="T325">giv-inactive-Q </ta>
            <ta e="T327" id="Seg_3521" s="T326">0.giv-active-Q</ta>
            <ta e="T330" id="Seg_3522" s="T329">giv-inactive-Q </ta>
            <ta e="T332" id="Seg_3523" s="T331">giv-active-Q </ta>
            <ta e="T335" id="Seg_3524" s="T334">giv-active-Q </ta>
            <ta e="T337" id="Seg_3525" s="T336">giv-active-Q</ta>
            <ta e="T339" id="Seg_3526" s="T338">giv-active-Q</ta>
            <ta e="T340" id="Seg_3527" s="T339">giv-active-Q </ta>
            <ta e="T342" id="Seg_3528" s="T341">giv-active-Q</ta>
            <ta e="T343" id="Seg_3529" s="T342">0.giv-active</ta>
            <ta e="T345" id="Seg_3530" s="T344">giv-inactive </ta>
            <ta e="T346" id="Seg_3531" s="T345">giv-inactive </ta>
            <ta e="T347" id="Seg_3532" s="T346">0.giv-active</ta>
            <ta e="T348" id="Seg_3533" s="T347">giv-inactive </ta>
            <ta e="T349" id="Seg_3534" s="T348">giv-inactive</ta>
            <ta e="T352" id="Seg_3535" s="T351">giv-inactive </ta>
            <ta e="T353" id="Seg_3536" s="T352">0.giv-active</ta>
            <ta e="T355" id="Seg_3537" s="T354">0.giv-active</ta>
            <ta e="T356" id="Seg_3538" s="T355">new </ta>
            <ta e="T357" id="Seg_3539" s="T356">0.giv-active</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T175" id="Seg_3540" s="T174">TURK:cult</ta>
            <ta e="T189" id="Seg_3541" s="T188">TURK:cult</ta>
            <ta e="T215" id="Seg_3542" s="T214">TURK:cult</ta>
            <ta e="T216" id="Seg_3543" s="T215">TURK:cult</ta>
            <ta e="T230" id="Seg_3544" s="T229">TURK:cult</ta>
            <ta e="T237" id="Seg_3545" s="T236">TURK:cult</ta>
            <ta e="T239" id="Seg_3546" s="T238">TURK:cult</ta>
            <ta e="T245" id="Seg_3547" s="T244">TURK:core</ta>
            <ta e="T249" id="Seg_3548" s="T248">TURK:core</ta>
            <ta e="T257" id="Seg_3549" s="T256">TURK:cult</ta>
            <ta e="T263" id="Seg_3550" s="T262">TURK:cult</ta>
            <ta e="T264" id="Seg_3551" s="T263">TURK:cult</ta>
            <ta e="T272" id="Seg_3552" s="T271">TURK:cult</ta>
            <ta e="T294" id="Seg_3553" s="T293">TURK:cult</ta>
            <ta e="T316" id="Seg_3554" s="T315">TURK:cult</ta>
            <ta e="T321" id="Seg_3555" s="T320">TURK:cult</ta>
            <ta e="T324" id="Seg_3556" s="T323">TURK:core</ta>
            <ta e="T332" id="Seg_3557" s="T331">TURK:cult</ta>
            <ta e="T337" id="Seg_3558" s="T336">TURK:core</ta>
            <ta e="T338" id="Seg_3559" s="T337">RUS:mod</ta>
            <ta e="T348" id="Seg_3560" s="T347">TURK:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="fr" tierref="fr">
            <ta e="T113" id="Seg_3561" s="T110">Жил бедняк.</ta>
            <ta e="T115" id="Seg_3562" s="T113">Имя его было Тарджа-Барджа.</ta>
            <ta e="T118" id="Seg_3563" s="T115">Поймал он плохую кобылу.</ta>
            <ta e="T121" id="Seg_3564" s="T118">Хромой была его кобыла.</ta>
            <ta e="T128" id="Seg_3565" s="T121">Оседлал он её, привязал к ней свой черный мешок и сосуд из бересты.</ta>
            <ta e="T130" id="Seg_3566" s="T128">Отправился в путь.</ta>
            <ta e="T132" id="Seg_3567" s="T130">Повстречался ему медведь.</ta>
            <ta e="T135" id="Seg_3568" s="T132">"Возьми меня в попутчики!"</ta>
            <ta e="T138" id="Seg_3569" s="T135">"Полезай в черный мешок!"</ta>
            <ta e="T139" id="Seg_3570" s="T138">Дальше пошёл.</ta>
            <ta e="T141" id="Seg_3571" s="T139">Лиса ему повстречалась.</ta>
            <ta e="T144" id="Seg_3572" s="T141">"Возьми меня в попутчики!"</ta>
            <ta e="T147" id="Seg_3573" s="T144">"Полезай в черный мешок!"</ta>
            <ta e="T150" id="Seg_3574" s="T147">Запихнул её в чёрный мешок.</ta>
            <ta e="T153" id="Seg_3575" s="T150">Затем волк ему повстречался.</ta>
            <ta e="T155" id="Seg_3576" s="T153">"Возьми меня в попутчики!"</ta>
            <ta e="T158" id="Seg_3577" s="T155">"Полезай в чёрный мешок!</ta>
            <ta e="T161" id="Seg_3578" s="T158">Затем к воде вышли. </ta>
            <ta e="T173" id="Seg_3579" s="T161">Сосудом из бересты начерпал он из того водоёма в свой черный мешок и к кобыле его привязал.</ta>
            <ta e="T178" id="Seg_3580" s="T173">Видит, на берегу озера деревня стоит.</ta>
            <ta e="T187" id="Seg_3581" s="T178">Не заходя в ту деревню, спустился, огонь развёл, кобылу привязал.</ta>
            <ta e="T188" id="Seg_3582" s="T187">Сидит.</ta>
            <ta e="T192" id="Seg_3583" s="T188">Хан, мимо проходя, его увидал:</ta>
            <ta e="T195" id="Seg_3584" s="T192">"Кто там сидит?</ta>
            <ta e="T203" id="Seg_3585" s="T195">Кобылу свою привязал, по моей зелёной траве без спросу ходит."</ta>
            <ta e="T207" id="Seg_3586" s="T203">Гонца своего на тот берег шлет:</ta>
            <ta e="T209" id="Seg_3587" s="T207">"Поди и спроси его!</ta>
            <ta e="T212" id="Seg_3588" s="T209">Что ты за человек?</ta>
            <ta e="T218" id="Seg_3589" s="T212">Хана не спросив, траву ханскую топчешь. </ta>
            <ta e="T221" id="Seg_3590" s="T218">Чего надобно?"</ta>
            <ta e="T223" id="Seg_3591" s="T221">Гонец пошел и спрашивает:</ta>
            <ta e="T226" id="Seg_3592" s="T223">"Что ты за человек?</ta>
            <ta e="T229" id="Seg_3593" s="T226">Чего ищешь?"</ta>
            <ta e="T233" id="Seg_3594" s="T229">"Пришел забрать ханскую дочь."</ta>
            <ta e="T242" id="Seg_3595" s="T233">Этот человек вернулся, хану рассказал: "Пришел, мол, дочь хана забрать".</ta>
            <ta e="T246" id="Seg_3596" s="T242">"Пойди и скажи ему: Уходи по хорошему!"</ta>
            <ta e="T248" id="Seg_3597" s="T246">"Не уйду.</ta>
            <ta e="T253" id="Seg_3598" s="T248">Если не даст мне её по-хорошему, возьму её по-плохому."</ta>
            <ta e="T257" id="Seg_3599" s="T253">"Выпускай диких жеребцов!</ta>
            <ta e="T260" id="Seg_3600" s="T257">Затопчут они его вместе с его кобылой."</ta>
            <ta e="T263" id="Seg_3601" s="T260">Тот выпустил диких жеребцов.</ta>
            <ta e="T265" id="Seg_3602" s="T263">Скачут жеребцы.</ta>
            <ta e="T270" id="Seg_3603" s="T265">Раскрыл он свой черный мешок, медведя выпустил.</ta>
            <ta e="T274" id="Seg_3604" s="T270">Медведь разогнал жеребцов.</ta>
            <ta e="T281" id="Seg_3605" s="T274">"Выпусти диких быков, затопчут они его вместе с кобылой!"</ta>
            <ta e="T287" id="Seg_3606" s="T281">Развязал он свой чёрный мешок, волка выпустил.</ta>
            <ta e="T293" id="Seg_3607" s="T287">Волк быков прогнал (в лес?).</ta>
            <ta e="T296" id="Seg_3608" s="T293">У хана ничего больше не осталось.</ta>
            <ta e="T299" id="Seg_3609" s="T296">Собрал он народ вокруг себя:</ta>
            <ta e="T301" id="Seg_3610" s="T299">"Схватим его!"</ta>
            <ta e="T304" id="Seg_3611" s="T301">Окружили его [Тарджу-Барджу], схватить хотят.</ta>
            <ta e="T311" id="Seg_3612" s="T304">Он свой чёрный мешок раскрыл, оттуда воду вылил.</ta>
            <ta e="T315" id="Seg_3613" s="T311">Вода людей смыла.</ta>
            <ta e="T327" id="Seg_3614" s="T315">Хан закричал: "Тарджа-Барджа, Пол-Барджа, я тебе половину своего скота отдам, половину богатство своего отдам, дочь свою тебе отдам!"</ta>
            <ta e="T342" id="Seg_3615" s="T327">"Не надо мне твоего скота, и богатства не надо, только дочь твою красавицу".</ta>
            <ta e="T347" id="Seg_3616" s="T342">Взял он берестяной сосуд, воду вычерпал.</ta>
            <ta e="T357" id="Seg_3617" s="T347">Хан отдал ему свою дочь, [Тарджа-Барджа] посадил её на свою хромую кобылу, привязал её, сам сел [верхом] и домой её повез.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T113" id="Seg_3618" s="T110">A poor man was living.</ta>
            <ta e="T115" id="Seg_3619" s="T113">His name was Tardzha-Bardzha.</ta>
            <ta e="T118" id="Seg_3620" s="T115">He caught a bad mare [for himself].</ta>
            <ta e="T121" id="Seg_3621" s="T118">Lame was his mare.</ta>
            <ta e="T128" id="Seg_3622" s="T121">He saddled her, tied his black sack and his birchbark vessel to it.</ta>
            <ta e="T130" id="Seg_3623" s="T128">He got going.</ta>
            <ta e="T132" id="Seg_3624" s="T130">A bear came up.</ta>
            <ta e="T135" id="Seg_3625" s="T132">"Take me as a companion!"</ta>
            <ta e="T138" id="Seg_3626" s="T135">"Crawl into the black sack!"</ta>
            <ta e="T139" id="Seg_3627" s="T138">He got going.</ta>
            <ta e="T141" id="Seg_3628" s="T139">A fox came up.</ta>
            <ta e="T144" id="Seg_3629" s="T141">"Take me as a companion!"</ta>
            <ta e="T147" id="Seg_3630" s="T144">"Crawl into the black sack!"</ta>
            <ta e="T150" id="Seg_3631" s="T147">He put him into the black sack.</ta>
            <ta e="T153" id="Seg_3632" s="T150">Then a wolf came up.</ta>
            <ta e="T155" id="Seg_3633" s="T153">"Take (me) as a companion!"</ta>
            <ta e="T158" id="Seg_3634" s="T155">"Creep into the black sack!"</ta>
            <ta e="T161" id="Seg_3635" s="T158">Then a big water appeared.</ta>
            <ta e="T173" id="Seg_3636" s="T161">He scooped from this water, scooped it with the birchbark vessel into his black bag and tied it to his mare.</ta>
            <ta e="T178" id="Seg_3637" s="T173">As he sees, there is a village standing on the shore of the sea.</ta>
            <ta e="T187" id="Seg_3638" s="T178">Not getting to this village, he descended, made a fire and hobbled his mare.</ta>
            <ta e="T188" id="Seg_3639" s="T187">He sits.</ta>
            <ta e="T192" id="Seg_3640" s="T188">The khan was walking outside, caught sight of him: </ta>
            <ta e="T195" id="Seg_3641" s="T192">"Who sits there?</ta>
            <ta e="T203" id="Seg_3642" s="T195">He hobbled his horse, steps on my green grass without asking.</ta>
            <ta e="T207" id="Seg_3643" s="T203">He is sending his running boy to the shore:</ta>
            <ta e="T209" id="Seg_3644" s="T207">"Go and ask him!</ta>
            <ta e="T212" id="Seg_3645" s="T209">What kind of person are you?</ta>
            <ta e="T218" id="Seg_3646" s="T212">Without asking the khan, stepping on the khan’s grass.</ta>
            <ta e="T221" id="Seg_3647" s="T218">What do you need?"</ta>
            <ta e="T223" id="Seg_3648" s="T221">He went and is asking:</ta>
            <ta e="T226" id="Seg_3649" s="T223">"What kind of person are you?</ta>
            <ta e="T229" id="Seg_3650" s="T226">What are you looking for?"</ta>
            <ta e="T233" id="Seg_3651" s="T229">"I’ve come to take the khan’s daughter."</ta>
            <ta e="T242" id="Seg_3652" s="T233">This man returned, told the khan: "I’ve come to take the khan’s daughter."</ta>
            <ta e="T246" id="Seg_3653" s="T242">"Go and tell (him): Return in peace!"</ta>
            <ta e="T248" id="Seg_3654" s="T246">"I will not return.</ta>
            <ta e="T253" id="Seg_3655" s="T248">If he doesn’t give [her] with good I will take [her] with bad."</ta>
            <ta e="T257" id="Seg_3656" s="T253">"Let the wild stallions loose!</ta>
            <ta e="T260" id="Seg_3657" s="T257">They will tear him apart along with his mare."</ta>
            <ta e="T263" id="Seg_3658" s="T260">He let their wild stallions go.</ta>
            <ta e="T265" id="Seg_3659" s="T263">The stallions were galloping.</ta>
            <ta e="T270" id="Seg_3660" s="T265">He tied his black sack loose and let the bear go.</ta>
            <ta e="T274" id="Seg_3661" s="T270">The bear chased the stallions away.</ta>
            <ta e="T281" id="Seg_3662" s="T274">"Let the bulls go, the wild ones, they will punch him dead along with his mare!"</ta>
            <ta e="T287" id="Seg_3663" s="T281">He tied his black sack loose and let his wolf [onto them].</ta>
            <ta e="T293" id="Seg_3664" s="T287">The wolf chased the bulls away (onto the treetops?).</ta>
            <ta e="T296" id="Seg_3665" s="T293">The Khan was left alone.</ta>
            <ta e="T299" id="Seg_3666" s="T296">He gathered people around him.</ta>
            <ta e="T301" id="Seg_3667" s="T299">"Let’s seize him!"</ta>
            <ta e="T304" id="Seg_3668" s="T301">They surrounded [Tardzha-Bardzha], they want to seize him.</ta>
            <ta e="T311" id="Seg_3669" s="T304">He tore his black sack open and poured his water out.</ta>
            <ta e="T315" id="Seg_3670" s="T311">The people were taken away by the water.</ta>
            <ta e="T327" id="Seg_3671" s="T315">The khan shouts: "Tardzha-Bardzha, Pol-Bardzha, I will give half of my cattle, I will give half of my wealth, I will give my daughter."</ta>
            <ta e="T342" id="Seg_3672" s="T327">"I don’t need your cattle, I don’t need your wealth, I only need your noble daughter."</ta>
            <ta e="T347" id="Seg_3673" s="T342">He took his birch bark vessel, scooped up his water.</ta>
            <ta e="T357" id="Seg_3674" s="T347">The khan gave his daughter, [Tardzha-Bardzha] seated her on his lame mare, tied up, mounted and brought her home.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T113" id="Seg_3675" s="T110">Ein armer Mann lebte.</ta>
            <ta e="T115" id="Seg_3676" s="T113">Sein Name war Tardzha-Bardzha.</ta>
            <ta e="T118" id="Seg_3677" s="T115">Er fing eine schlechte Stute.</ta>
            <ta e="T121" id="Seg_3678" s="T118">Seine Stute lahmte.</ta>
            <ta e="T128" id="Seg_3679" s="T121">Er sattelte sie und band seinen schwarzen Sack und sein Gefäß aus Birkenrinde an sie.</ta>
            <ta e="T130" id="Seg_3680" s="T128">Er ging los.</ta>
            <ta e="T132" id="Seg_3681" s="T130">Ein Bär kam daher. </ta>
            <ta e="T135" id="Seg_3682" s="T132">"Nimm mich zum Gefährten!" </ta>
            <ta e="T138" id="Seg_3683" s="T135">"Kriech in den schwarzen Sack!"</ta>
            <ta e="T139" id="Seg_3684" s="T138">Er ging [wieder] los.</ta>
            <ta e="T141" id="Seg_3685" s="T139">Ein Fuchs kam daher.</ta>
            <ta e="T144" id="Seg_3686" s="T141">"Nimm mich zum Gefährten!"</ta>
            <ta e="T147" id="Seg_3687" s="T144">"Kriech in den schwarzen Sack!"</ta>
            <ta e="T150" id="Seg_3688" s="T147">Er stopfte ihn in den schwarzen Sack.</ta>
            <ta e="T153" id="Seg_3689" s="T150">Dann kam ein Wolf daher.</ta>
            <ta e="T155" id="Seg_3690" s="T153">"Nimm mich zum Gefährten!"</ta>
            <ta e="T158" id="Seg_3691" s="T155">"Kriech in den schwarzen Sack!"</ta>
            <ta e="T161" id="Seg_3692" s="T158">Dann kam er an ein Gewässer.</ta>
            <ta e="T173" id="Seg_3693" s="T161">Er schöpfte von diesem Wasser, schöpfte es mit dem Gefäß aus Birkenrinde in seinen schwarzen Sack und band ihn an seine Stute.</ta>
            <ta e="T178" id="Seg_3694" s="T173">Er sieht, dass an dem Meeresufer ein Dorf steht.</ta>
            <ta e="T187" id="Seg_3695" s="T178">Als er nicht in das Dorf gelangte, stieg er hinab, machte ein Feuer und band seine Stute an.</ta>
            <ta e="T188" id="Seg_3696" s="T187">Er sitzt.</ta>
            <ta e="T192" id="Seg_3697" s="T188">Der Khan ging draußen umher und erblickte ihn:</ta>
            <ta e="T195" id="Seg_3698" s="T192">"Wer sitzt dort?</ta>
            <ta e="T203" id="Seg_3699" s="T195">Er band sein Pferd an und zertrampelt mein grünes Gras, ohne zu fragen."</ta>
            <ta e="T207" id="Seg_3700" s="T203">Er schickt seinen Botenjungen zum Ufer:</ta>
            <ta e="T209" id="Seg_3701" s="T207">"Geh und frag ihn!</ta>
            <ta e="T212" id="Seg_3702" s="T209">Was bist du für einer?</ta>
            <ta e="T218" id="Seg_3703" s="T212">Zertrittst das Gras des Khans, ohne zu fragen.</ta>
            <ta e="T221" id="Seg_3704" s="T218">Was willst du?"</ta>
            <ta e="T223" id="Seg_3705" s="T221">Er ging und fragte:</ta>
            <ta e="T226" id="Seg_3706" s="T223">"Was für ein Mensch bist du?</ta>
            <ta e="T229" id="Seg_3707" s="T226">Wonach suchst du?"</ta>
            <ta e="T233" id="Seg_3708" s="T229">"Ich kam, um mir die Tochter des Khans zu holen."</ta>
            <ta e="T242" id="Seg_3709" s="T233">Der Mann kehrte zurück und berichtete dem Khan: "Ich kam, um mir die Tochter des Khans zu holen."</ta>
            <ta e="T246" id="Seg_3710" s="T242">"Geh sag ihm: Kehre in Frieden um!"</ta>
            <ta e="T248" id="Seg_3711" s="T246">"Ich werde nicht umkehren.</ta>
            <ta e="T253" id="Seg_3712" s="T248">Gibt er sie mir nicht im Guten, nehme ich sie mir im Bösen."</ta>
            <ta e="T257" id="Seg_3713" s="T253">"Lass die wilden Hengste los!</ta>
            <ta e="T260" id="Seg_3714" s="T257">Samt seiner Stute werden sie ihn in Stücke reißen."</ta>
            <ta e="T263" id="Seg_3715" s="T260">Er ließ die wilden Hengste los.</ta>
            <ta e="T265" id="Seg_3716" s="T263">Die Hengste galoppieren.</ta>
            <ta e="T270" id="Seg_3717" s="T265">[Tardzha-Bardzha] öffnete den schwarzen Sack und ließ den Bären los.</ta>
            <ta e="T274" id="Seg_3718" s="T270">Der Bär jagte die Hengste davon.</ta>
            <ta e="T281" id="Seg_3719" s="T274">"Lasst die wilden Stiere los, samt seiner Stute werden sie ihn totschlagen!"</ta>
            <ta e="T287" id="Seg_3720" s="T281">Er öffnete seinen schwarzen Sack und ließ seinen Wolf los.</ta>
            <ta e="T293" id="Seg_3721" s="T287">Der Wolf trieb die Stiere (auf die Bäume?).</ta>
            <ta e="T296" id="Seg_3722" s="T293">Der Khan blieb allein zurück.</ta>
            <ta e="T299" id="Seg_3723" s="T296">Er versammelte Leute um sich:</ta>
            <ta e="T301" id="Seg_3724" s="T299">"Schnappen wir ihn uns!"</ta>
            <ta e="T304" id="Seg_3725" s="T301">Sie haben ihn [Tardzha-Bardzha] umzingelt, wollen ihn ergreifen.</ta>
            <ta e="T311" id="Seg_3726" s="T304">Er riss den schwarzen Sack auf und schüttete das Wasser aus.</ta>
            <ta e="T315" id="Seg_3727" s="T311">Das Wasser schwemmt die Leute weg.</ta>
            <ta e="T327" id="Seg_3728" s="T315">Der Khan schreit: "Tardzha-Bardzha, Pol-Bardzha! Ich werde dir die Hälfte meines Viehs geben, ich werdr die Hälfte meines Vermögens gebn und ich werde dir meine Tochter geben!"</ta>
            <ta e="T342" id="Seg_3729" s="T327">"Dein Vieh brauche ich nicht, dein Vermögen brauche ich nicht, ich brauche nur deine adlige Tochter."</ta>
            <ta e="T347" id="Seg_3730" s="T342">Er nahm das Gefäß aus Birkenrinde und schöpfte sein Wasser auf.</ta>
            <ta e="T357" id="Seg_3731" s="T347">Der Khan gab ihm seine Tochter. [Tardzha-Bardzha] setzte sie auf seine lahme Stute, band sie fest, stieg auf und brachte sie nach Hause.</ta>
         </annotation>
         <annotation name="ltg" tierref="ltg">
            <ta e="T113" id="Seg_3732" s="T110">Ein schlechter Mensch lebte,</ta>
            <ta e="T115" id="Seg_3733" s="T113">sein Name war tʿart́š́ābᵊrd́ž́e.</ta>
            <ta e="T118" id="Seg_3734" s="T115">Eine schlechte Stute aber fing er,</ta>
            <ta e="T121" id="Seg_3735" s="T118">hinkend war seine Stute.</ta>
            <ta e="T128" id="Seg_3736" s="T121">Er schirrte [sie] an, band auch einen schwarzen Sack an, band auch ein Gefäss aus Birkenrinde an,</ta>
            <ta e="T130" id="Seg_3737" s="T128">ging.</ta>
            <ta e="T132" id="Seg_3738" s="T130">Ein Bär kam [ihm] entgegen.</ta>
            <ta e="T135" id="Seg_3739" s="T132">»Nimm mich zum Gefährten!</ta>
            <ta e="T138" id="Seg_3740" s="T135">Stecke6 [mich] in den schwarzen Sack!»</ta>
            <ta e="T139" id="Seg_3741" s="T138">Er ging,</ta>
            <ta e="T141" id="Seg_3742" s="T139">ein Fuchs kam [ihm] entgegen. </ta>
            <ta e="T144" id="Seg_3743" s="T141">»Nimm mich zum Gefährten!</ta>
            <ta e="T147" id="Seg_3744" s="T144">Stecke [mich] in den schwarzen Sack!»</ta>
            <ta e="T150" id="Seg_3745" s="T147">Er steckte [ihn] in den schwarzen Sack.</ta>
            <ta e="T153" id="Seg_3746" s="T150">Dann kam [ihm] ein Wolf entgegen.</ta>
            <ta e="T155" id="Seg_3747" s="T153">»Nimm [mich] zum Gefährten!</ta>
            <ta e="T158" id="Seg_3748" s="T155">Stecke [mich] in den schwarzen Sack!»</ta>
            <ta e="T161" id="Seg_3749" s="T158">Dann kam [ihm] Wasser (ein See) entgegen;</ta>
            <ta e="T173" id="Seg_3750" s="T161">er schöpfte das Wasser in seinen schwarzen Sack, mit dem Gefäss aus Birkenrinde schöpfte er. Seine Stute band er an.</ta>
            <ta e="T178" id="Seg_3751" s="T173">Als er schaute: an dem Meeresufer steht ein Dorf.</ta>
            <ta e="T187" id="Seg_3752" s="T178">In das Dorf gelangt er nicht. [Er] stieg herunter, machte Feuer, die Stute aber band er,</ta>
            <ta e="T188" id="Seg_3753" s="T187">sitzt.</ta>
            <ta e="T192" id="Seg_3754" s="T188">Der Kaiser draussen ging, sah.</ta>
            <ta e="T195" id="Seg_3755" s="T192">»Wer sitzt dort?</ta>
            <ta e="T203" id="Seg_3756" s="T195">Das Pferd aber band er an. Zertritt mein grünes Gras, fragt nicht.</ta>
            <ta e="T207" id="Seg_3757" s="T203">Verbringt am Rande den Tag, liess frei.</ta>
            <ta e="T209" id="Seg_3758" s="T207">Gehend frage:</ta>
            <ta e="T212" id="Seg_3759" s="T209">Was für einer bist du?</ta>
            <ta e="T218" id="Seg_3760" s="T212">Er fragt nicht den Kaiser. Zertritt des Kaisers Gras.</ta>
            <ta e="T221" id="Seg_3761" s="T218">Was hast du nötig?»</ta>
            <ta e="T223" id="Seg_3762" s="T221">Er ging, fragte:</ta>
            <ta e="T226" id="Seg_3763" s="T223">»Was für ein Mensch bist du?</ta>
            <ta e="T229" id="Seg_3764" s="T226">Was suchend gehst du?»</ta>
            <ta e="T233" id="Seg_3765" s="T229">»Des Kaisers Tochter doch zu nehmen kam ich.»</ta>
            <ta e="T242" id="Seg_3766" s="T233">Jener Mann kehrte zurück, erzählte dem Kaiser: »Des Kaisers Tochter doch kam ich zu nehmen.»</ta>
            <ta e="T246" id="Seg_3767" s="T242">»Gehe, sage: Im Guten kehre zurück!»</ta>
            <ta e="T248" id="Seg_3768" s="T246">»Ich kehre nicht zurück.</ta>
            <ta e="T253" id="Seg_3769" s="T248">Im Guten wenn er nicht gibt, nehme ich im Bösen.»</ta>
            <ta e="T257" id="Seg_3770" s="T253">»Lass die wilden Hengste los!</ta>
            <ta e="T260" id="Seg_3771" s="T257">Seine Stute greifen sie an, zerreissen ⌈sie⌉.»</ta>
            <ta e="T263" id="Seg_3772" s="T260">Er liess ihre wilden Hengste los.</ta>
            <ta e="T265" id="Seg_3773" s="T263">Die Hengste laufen.</ta>
            <ta e="T270" id="Seg_3774" s="T265">Den schwarzen Sack aber öffnete er, den schwarzen Bären liess er frei.</ta>
            <ta e="T274" id="Seg_3775" s="T270">Der Bär die Hengste verfolgend führte weg.</ta>
            <ta e="T281" id="Seg_3776" s="T274">»Lasst die wilden Stiere mit seiner Stute [zusammen], um sie tot zu stossen!»</ta>
            <ta e="T287" id="Seg_3777" s="T281">Seinen schwarzen Sack aber öffnete er, liess seinen Wolf hinaus.</ta>
            <ta e="T293" id="Seg_3778" s="T287">Der Wolf die Stiere verfolgend führte weg in den Wald.</ta>
            <ta e="T296" id="Seg_3779" s="T293">Der Kaiser blieb zurück.</ta>
            <ta e="T299" id="Seg_3780" s="T296">Leute sammelte er um sich:</ta>
            <ta e="T301" id="Seg_3781" s="T299">»Lasst uns ihn nehmen!»</ta>
            <ta e="T304" id="Seg_3782" s="T301">Sie umgaben [ihn], wollen [ihn] festnehmen.</ta>
            <ta e="T311" id="Seg_3783" s="T304">Den schwarzen Sack öffnete er, zog, das Wasser aber schüttete er aus.</ta>
            <ta e="T315" id="Seg_3784" s="T311">Das Wasser führt die Leute fort, geht fort.</ta>
            <ta e="T327" id="Seg_3785" s="T315">​Der Kaiser schreit: »tart́ā bart́a, pʿoл bart́ȧ. Die Hälfte meines Viehs gebe ich, die Hälfte meines Reichtums gebe ich, meine Tochter gebe ich!»</ta>
            <ta e="T342" id="Seg_3786" s="T327">»Nicht nötig für mich dein Vieh, nicht nötig dein Reichtum, nur dein Fräulein Tochter ist nötig für mich.»</ta>
            <ta e="T347" id="Seg_3787" s="T342">Er nahm das Gefäss aus Birkenrinde, schöpfte das Wasser.</ta>
            <ta e="T357" id="Seg_3788" s="T347">Der Kaiser gab die Tochter. Auf die hinkende Stute setzte er [sie], befestigend stieg er auf (näml. auf den Rücken des Pferdes), brachte [sie] nach Hause.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltg"
                          display-name="ltg"
                          name="ltg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
