<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDA7BE64F7-7474-5072-E052-623D5357C3CA">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_Rooster_flk.wav" />
         <referenced-file url="PKZ_196X_Rooster_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_Rooster_flk\PKZ_196X_Rooster_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">127</ud-information>
            <ud-information attribute-name="# HIAT:w">80</ud-information>
            <ud-information attribute-name="# e">80</ud-information>
            <ud-information attribute-name="# HIAT:u">18</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T2065" time="0.019" type="appl" />
         <tli id="T2066" time="1.062" type="appl" />
         <tli id="T2067" time="2.105" type="appl" />
         <tli id="T2068" time="3.148" type="appl" />
         <tli id="T2069" time="4.19" type="appl" />
         <tli id="T2070" time="5.233" type="appl" />
         <tli id="T2071" time="6.276" type="appl" />
         <tli id="T2072" time="7.319" type="appl" />
         <tli id="T2073" time="7.74" type="appl" />
         <tli id="T2074" time="8.154" type="appl" />
         <tli id="T2075" time="8.569" type="appl" />
         <tli id="T2076" time="8.983" type="appl" />
         <tli id="T2077" time="9.711" type="appl" />
         <tli id="T2078" time="10.438" type="appl" />
         <tli id="T2079" time="11.166" type="appl" />
         <tli id="T2080" time="11.893" type="appl" />
         <tli id="T2081" time="12.621" type="appl" />
         <tli id="T2082" time="13.475" type="appl" />
         <tli id="T2083" time="14.31306759144973" />
         <tli id="T2084" time="14.845" type="appl" />
         <tli id="T2085" time="15.555" type="appl" />
         <tli id="T2086" time="16.266" type="appl" />
         <tli id="T2087" time="16.977" type="appl" />
         <tli id="T2088" time="17.687" type="appl" />
         <tli id="T2089" time="18.699652815098506" />
         <tli id="T2090" time="19.628" type="appl" />
         <tli id="T2091" time="20.729" type="appl" />
         <tli id="T2092" time="21.83" type="appl" />
         <tli id="T2093" time="22.931" type="appl" />
         <tli id="T2094" time="24.032" type="appl" />
         <tli id="T2095" time="25.133" type="appl" />
         <tli id="T2096" time="26.234" type="appl" />
         <tli id="T2097" time="27.335" type="appl" />
         <tli id="T2098" time="28.436" type="appl" />
         <tli id="T2099" time="29.537" type="appl" />
         <tli id="T2100" time="30.638" type="appl" />
         <tli id="T2101" time="31.739" type="appl" />
         <tli id="T2102" time="32.707" type="appl" />
         <tli id="T2103" time="33.674" type="appl" />
         <tli id="T2104" time="34.642" type="appl" />
         <tli id="T2105" time="35.229" type="appl" />
         <tli id="T2106" time="35.815" type="appl" />
         <tli id="T2107" time="36.402" type="appl" />
         <tli id="T2108" time="36.988" type="appl" />
         <tli id="T2109" time="38.27262274919092" />
         <tli id="T2110" time="39.128" type="appl" />
         <tli id="T2111" time="39.639" type="appl" />
         <tli id="T2112" time="40.149" type="appl" />
         <tli id="T2113" time="40.66" type="appl" />
         <tli id="T2114" time="41.452" type="appl" />
         <tli id="T2115" time="42.244" type="appl" />
         <tli id="T2116" time="43.036" type="appl" />
         <tli id="T2117" time="43.829" type="appl" />
         <tli id="T2118" time="44.621" type="appl" />
         <tli id="T2119" time="45.413" type="appl" />
         <tli id="T2120" time="46.216" type="appl" />
         <tli id="T2121" time="47.018" type="appl" />
         <tli id="T2122" time="47.821" type="appl" />
         <tli id="T2123" time="48.624" type="appl" />
         <tli id="T2124" time="49.361" type="appl" />
         <tli id="T2125" time="50.097" type="appl" />
         <tli id="T2126" time="50.834" type="appl" />
         <tli id="T2127" time="51.642" type="appl" />
         <tli id="T2128" time="52.89235131372248" />
         <tli id="T2129" time="53.505" type="appl" />
         <tli id="T2130" time="54.25" type="appl" />
         <tli id="T2131" time="54.995" type="appl" />
         <tli id="T2132" time="55.741" type="appl" />
         <tli id="T2133" time="56.486" type="appl" />
         <tli id="T2134" time="57.231" type="appl" />
         <tli id="T2135" time="59.44556297762331" />
         <tli id="T2136" time="61.042" type="appl" />
         <tli id="T2137" time="62.555" type="appl" />
         <tli id="T2138" time="64.067" type="appl" />
         <tli id="T2139" time="64.894" type="appl" />
         <tli id="T2140" time="65.72" type="appl" />
         <tli id="T2141" time="66.65876238793938" />
         <tli id="T2142" time="67.507" type="appl" />
         <tli id="T2143" time="68.466" type="appl" />
         <tli id="T2144" time="69.426" type="appl" />
         <tli id="T2145" time="70.552" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T2145" id="Seg_0" n="sc" s="T2065">
               <ts e="T2072" id="Seg_2" n="HIAT:u" s="T2065">
                  <ts e="T2066" id="Seg_4" n="HIAT:w" s="T2065">Kuriza</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2067" id="Seg_7" n="HIAT:w" s="T2066">tibi</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2068" id="Seg_10" n="HIAT:w" s="T2067">nʼergölüʔpi</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2069" id="Seg_13" n="HIAT:w" s="T2068">zaplottə</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2070" id="Seg_16" n="HIAT:w" s="T2069">i</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2071" id="Seg_19" n="HIAT:w" s="T2070">davaj</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2072" id="Seg_22" n="HIAT:w" s="T2071">kirgarzittə</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2076" id="Seg_26" n="HIAT:u" s="T2072">
                  <nts id="Seg_27" n="HIAT:ip">"</nts>
                  <ts e="T2073" id="Seg_29" n="HIAT:w" s="T2072">Măn</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2074" id="Seg_32" n="HIAT:w" s="T2073">koŋ</ts>
                  <nts id="Seg_33" n="HIAT:ip">,</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2075" id="Seg_36" n="HIAT:w" s="T2074">măn</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2076" id="Seg_39" n="HIAT:w" s="T2075">koŋ</ts>
                  <nts id="Seg_40" n="HIAT:ip">"</nts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2081" id="Seg_44" n="HIAT:u" s="T2076">
                  <ts e="T2077" id="Seg_46" n="HIAT:w" s="T2076">Kurizaʔi</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2078" id="Seg_49" n="HIAT:w" s="T2077">šobiʔi</ts>
                  <nts id="Seg_50" n="HIAT:ip">,</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2079" id="Seg_53" n="HIAT:w" s="T2078">tože</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2080" id="Seg_56" n="HIAT:w" s="T2079">davaj</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2081" id="Seg_59" n="HIAT:w" s="T2080">kirgarzittə</ts>
                  <nts id="Seg_60" n="HIAT:ip">.</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2083" id="Seg_63" n="HIAT:u" s="T2081">
                  <nts id="Seg_64" n="HIAT:ip">"</nts>
                  <ts e="T2082" id="Seg_66" n="HIAT:w" s="T2081">Tăn</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2083" id="Seg_69" n="HIAT:w" s="T2082">koŋ</ts>
                  <nts id="Seg_70" n="HIAT:ip">!</nts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2089" id="Seg_73" n="HIAT:u" s="T2083">
                  <ts e="T2084" id="Seg_75" n="HIAT:w" s="T2083">Ugaːndə</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2085" id="Seg_78" n="HIAT:w" s="T2084">idlial</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_80" n="HIAT:ip">(</nts>
                  <ts e="T2086" id="Seg_82" n="HIAT:w" s="T2085">măn=</ts>
                  <nts id="Seg_83" n="HIAT:ip">)</nts>
                  <nts id="Seg_84" n="HIAT:ip">,</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2087" id="Seg_87" n="HIAT:w" s="T2086">tăn</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2088" id="Seg_90" n="HIAT:w" s="T2087">ulul</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2089" id="Seg_93" n="HIAT:w" s="T2088">kuvas</ts>
                  <nts id="Seg_94" n="HIAT:ip">"</nts>
                  <nts id="Seg_95" n="HIAT:ip">.</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2101" id="Seg_98" n="HIAT:u" s="T2089">
                  <ts e="T2090" id="Seg_100" n="HIAT:w" s="T2089">Dĭgəttə</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_102" n="HIAT:ip">(</nts>
                  <ts e="T2091" id="Seg_104" n="HIAT:w" s="T2090">dĭ=</ts>
                  <nts id="Seg_105" n="HIAT:ip">)</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2092" id="Seg_108" n="HIAT:w" s="T2091">baška</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2093" id="Seg_111" n="HIAT:w" s="T2092">koŋ</ts>
                  <nts id="Seg_112" n="HIAT:ip">,</nts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2094" id="Seg_115" n="HIAT:w" s="T2093">kuza</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2095" id="Seg_118" n="HIAT:w" s="T2094">koŋ</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2096" id="Seg_121" n="HIAT:w" s="T2095">dĭm</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2097" id="Seg_124" n="HIAT:w" s="T2096">dʼappi</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2098" id="Seg_127" n="HIAT:w" s="T2097">da</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_129" n="HIAT:ip">(</nts>
                  <ts e="T2099" id="Seg_131" n="HIAT:w" s="T2098">davaj=</ts>
                  <nts id="Seg_132" n="HIAT:ip">)</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2100" id="Seg_135" n="HIAT:w" s="T2099">băʔpi</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2101" id="Seg_138" n="HIAT:w" s="T2100">uluttə</ts>
                  <nts id="Seg_139" n="HIAT:ip">.</nts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2104" id="Seg_142" n="HIAT:u" s="T2101">
                  <ts e="T2102" id="Seg_144" n="HIAT:w" s="T2101">Dĭgəttə</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2103" id="Seg_147" n="HIAT:w" s="T2102">mĭnzərbi</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2104" id="Seg_150" n="HIAT:w" s="T2103">pʼešgən</ts>
                  <nts id="Seg_151" n="HIAT:ip">.</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2109" id="Seg_154" n="HIAT:u" s="T2104">
                  <ts e="T2105" id="Seg_156" n="HIAT:w" s="T2104">Ambi</ts>
                  <nts id="Seg_157" n="HIAT:ip">,</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2106" id="Seg_160" n="HIAT:w" s="T2105">a</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2107" id="Seg_163" n="HIAT:w" s="T2106">dĭ</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2108" id="Seg_166" n="HIAT:w" s="T2107">uge</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2109" id="Seg_169" n="HIAT:w" s="T2108">kirgarlaʔbə</ts>
                  <nts id="Seg_170" n="HIAT:ip">.</nts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2113" id="Seg_173" n="HIAT:u" s="T2109">
                  <nts id="Seg_174" n="HIAT:ip">"</nts>
                  <ts e="T2110" id="Seg_176" n="HIAT:w" s="T2109">Măn</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2111" id="Seg_179" n="HIAT:w" s="T2110">koŋ</ts>
                  <nts id="Seg_180" n="HIAT:ip">,</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2112" id="Seg_183" n="HIAT:w" s="T2111">măn</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2113" id="Seg_186" n="HIAT:w" s="T2112">koŋ</ts>
                  <nts id="Seg_187" n="HIAT:ip">"</nts>
                  <nts id="Seg_188" n="HIAT:ip">.</nts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2119" id="Seg_191" n="HIAT:u" s="T2113">
                  <ts e="T2114" id="Seg_193" n="HIAT:w" s="T2113">Dĭgəttə</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2115" id="Seg_196" n="HIAT:w" s="T2114">ertən</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2116" id="Seg_199" n="HIAT:w" s="T2115">uʔbdəbi</ts>
                  <nts id="Seg_200" n="HIAT:ip">,</nts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2117" id="Seg_203" n="HIAT:w" s="T2116">dĭ</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2118" id="Seg_206" n="HIAT:w" s="T2117">uge</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2119" id="Seg_209" n="HIAT:w" s="T2118">kirgarluʔpi</ts>
                  <nts id="Seg_210" n="HIAT:ip">.</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2123" id="Seg_213" n="HIAT:u" s="T2119">
                  <ts e="T2120" id="Seg_215" n="HIAT:w" s="T2119">Onʼiʔ</ts>
                  <nts id="Seg_216" n="HIAT:ip">,</nts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2121" id="Seg_219" n="HIAT:w" s="T2120">šide</ts>
                  <nts id="Seg_220" n="HIAT:ip">,</nts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2122" id="Seg_223" n="HIAT:w" s="T2121">nagur</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_225" n="HIAT:ip">(</nts>
                  <ts e="T2123" id="Seg_227" n="HIAT:w" s="T2122">kirgaria</ts>
                  <nts id="Seg_228" n="HIAT:ip">)</nts>
                  <nts id="Seg_229" n="HIAT:ip">.</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2126" id="Seg_232" n="HIAT:u" s="T2123">
                  <nts id="Seg_233" n="HIAT:ip">"</nts>
                  <ts e="T2124" id="Seg_235" n="HIAT:w" s="T2123">Băʔgaʔ</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2125" id="Seg_238" n="HIAT:w" s="T2124">măna</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2126" id="Seg_241" n="HIAT:w" s="T2125">nanə</ts>
                  <nts id="Seg_242" n="HIAT:ip">"</nts>
                  <nts id="Seg_243" n="HIAT:ip">.</nts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2128" id="Seg_246" n="HIAT:u" s="T2126">
                  <ts e="T2127" id="Seg_248" n="HIAT:w" s="T2126">Dĭʔnə</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2128" id="Seg_251" n="HIAT:w" s="T2127">băʔpiʔi</ts>
                  <nts id="Seg_252" n="HIAT:ip">.</nts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2135" id="Seg_255" n="HIAT:u" s="T2128">
                  <ts e="T2129" id="Seg_257" n="HIAT:w" s="T2128">Dĭ</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2130" id="Seg_260" n="HIAT:w" s="T2129">tibi</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2131" id="Seg_263" n="HIAT:w" s="T2130">kuriza</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2132" id="Seg_266" n="HIAT:w" s="T2131">nʼergöluʔpi</ts>
                  <nts id="Seg_267" n="HIAT:ip">,</nts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2133" id="Seg_270" n="HIAT:w" s="T2132">a</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2134" id="Seg_273" n="HIAT:w" s="T2133">bazoʔ</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2135" id="Seg_276" n="HIAT:w" s="T2134">kirgarluʔpi</ts>
                  <nts id="Seg_277" n="HIAT:ip">.</nts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2138" id="Seg_280" n="HIAT:u" s="T2135">
                  <nts id="Seg_281" n="HIAT:ip">"</nts>
                  <ts e="T2136" id="Seg_283" n="HIAT:w" s="T2135">Kallam</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2137" id="Seg_286" n="HIAT:w" s="T2136">dĭʔnə</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2138" id="Seg_289" n="HIAT:w" s="T2137">münörzittə</ts>
                  <nts id="Seg_290" n="HIAT:ip">"</nts>
                  <nts id="Seg_291" n="HIAT:ip">.</nts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2141" id="Seg_294" n="HIAT:u" s="T2138">
                  <ts e="T2139" id="Seg_296" n="HIAT:w" s="T2138">Dĭgəttə</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2140" id="Seg_299" n="HIAT:w" s="T2139">šobi</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2141" id="Seg_302" n="HIAT:w" s="T2140">šedendə</ts>
                  <nts id="Seg_303" n="HIAT:ip">.</nts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2144" id="Seg_306" n="HIAT:u" s="T2141">
                  <ts e="T2142" id="Seg_308" n="HIAT:w" s="T2141">Červəʔi</ts>
                  <nts id="Seg_309" n="HIAT:ip">,</nts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2143" id="Seg_312" n="HIAT:w" s="T2142">muxaʔi</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2144" id="Seg_315" n="HIAT:w" s="T2143">dʼaʔpi</ts>
                  <nts id="Seg_316" n="HIAT:ip">.</nts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2145" id="Seg_319" n="HIAT:u" s="T2144">
                  <ts e="T2145" id="Seg_321" n="HIAT:w" s="T2144">Kabarləj</ts>
                  <nts id="Seg_322" n="HIAT:ip">.</nts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T2145" id="Seg_324" n="sc" s="T2065">
               <ts e="T2066" id="Seg_326" n="e" s="T2065">Kuriza </ts>
               <ts e="T2067" id="Seg_328" n="e" s="T2066">tibi </ts>
               <ts e="T2068" id="Seg_330" n="e" s="T2067">nʼergölüʔpi </ts>
               <ts e="T2069" id="Seg_332" n="e" s="T2068">zaplottə </ts>
               <ts e="T2070" id="Seg_334" n="e" s="T2069">i </ts>
               <ts e="T2071" id="Seg_336" n="e" s="T2070">davaj </ts>
               <ts e="T2072" id="Seg_338" n="e" s="T2071">kirgarzittə. </ts>
               <ts e="T2073" id="Seg_340" n="e" s="T2072">"Măn </ts>
               <ts e="T2074" id="Seg_342" n="e" s="T2073">koŋ, </ts>
               <ts e="T2075" id="Seg_344" n="e" s="T2074">măn </ts>
               <ts e="T2076" id="Seg_346" n="e" s="T2075">koŋ". </ts>
               <ts e="T2077" id="Seg_348" n="e" s="T2076">Kurizaʔi </ts>
               <ts e="T2078" id="Seg_350" n="e" s="T2077">šobiʔi, </ts>
               <ts e="T2079" id="Seg_352" n="e" s="T2078">tože </ts>
               <ts e="T2080" id="Seg_354" n="e" s="T2079">davaj </ts>
               <ts e="T2081" id="Seg_356" n="e" s="T2080">kirgarzittə. </ts>
               <ts e="T2082" id="Seg_358" n="e" s="T2081">"Tăn </ts>
               <ts e="T2083" id="Seg_360" n="e" s="T2082">koŋ! </ts>
               <ts e="T2084" id="Seg_362" n="e" s="T2083">Ugaːndə </ts>
               <ts e="T2085" id="Seg_364" n="e" s="T2084">idlial </ts>
               <ts e="T2086" id="Seg_366" n="e" s="T2085">(măn=), </ts>
               <ts e="T2087" id="Seg_368" n="e" s="T2086">tăn </ts>
               <ts e="T2088" id="Seg_370" n="e" s="T2087">ulul </ts>
               <ts e="T2089" id="Seg_372" n="e" s="T2088">kuvas". </ts>
               <ts e="T2090" id="Seg_374" n="e" s="T2089">Dĭgəttə </ts>
               <ts e="T2091" id="Seg_376" n="e" s="T2090">(dĭ=) </ts>
               <ts e="T2092" id="Seg_378" n="e" s="T2091">baška </ts>
               <ts e="T2093" id="Seg_380" n="e" s="T2092">koŋ, </ts>
               <ts e="T2094" id="Seg_382" n="e" s="T2093">kuza </ts>
               <ts e="T2095" id="Seg_384" n="e" s="T2094">koŋ </ts>
               <ts e="T2096" id="Seg_386" n="e" s="T2095">dĭm </ts>
               <ts e="T2097" id="Seg_388" n="e" s="T2096">dʼappi </ts>
               <ts e="T2098" id="Seg_390" n="e" s="T2097">da </ts>
               <ts e="T2099" id="Seg_392" n="e" s="T2098">(davaj=) </ts>
               <ts e="T2100" id="Seg_394" n="e" s="T2099">băʔpi </ts>
               <ts e="T2101" id="Seg_396" n="e" s="T2100">uluttə. </ts>
               <ts e="T2102" id="Seg_398" n="e" s="T2101">Dĭgəttə </ts>
               <ts e="T2103" id="Seg_400" n="e" s="T2102">mĭnzərbi </ts>
               <ts e="T2104" id="Seg_402" n="e" s="T2103">pʼešgən. </ts>
               <ts e="T2105" id="Seg_404" n="e" s="T2104">Ambi, </ts>
               <ts e="T2106" id="Seg_406" n="e" s="T2105">a </ts>
               <ts e="T2107" id="Seg_408" n="e" s="T2106">dĭ </ts>
               <ts e="T2108" id="Seg_410" n="e" s="T2107">uge </ts>
               <ts e="T2109" id="Seg_412" n="e" s="T2108">kirgarlaʔbə. </ts>
               <ts e="T2110" id="Seg_414" n="e" s="T2109">"Măn </ts>
               <ts e="T2111" id="Seg_416" n="e" s="T2110">koŋ, </ts>
               <ts e="T2112" id="Seg_418" n="e" s="T2111">măn </ts>
               <ts e="T2113" id="Seg_420" n="e" s="T2112">koŋ". </ts>
               <ts e="T2114" id="Seg_422" n="e" s="T2113">Dĭgəttə </ts>
               <ts e="T2115" id="Seg_424" n="e" s="T2114">ertən </ts>
               <ts e="T2116" id="Seg_426" n="e" s="T2115">uʔbdəbi, </ts>
               <ts e="T2117" id="Seg_428" n="e" s="T2116">dĭ </ts>
               <ts e="T2118" id="Seg_430" n="e" s="T2117">uge </ts>
               <ts e="T2119" id="Seg_432" n="e" s="T2118">kirgarluʔpi. </ts>
               <ts e="T2120" id="Seg_434" n="e" s="T2119">Onʼiʔ, </ts>
               <ts e="T2121" id="Seg_436" n="e" s="T2120">šide, </ts>
               <ts e="T2122" id="Seg_438" n="e" s="T2121">nagur </ts>
               <ts e="T2123" id="Seg_440" n="e" s="T2122">(kirgaria). </ts>
               <ts e="T2124" id="Seg_442" n="e" s="T2123">"Băʔgaʔ </ts>
               <ts e="T2125" id="Seg_444" n="e" s="T2124">măna </ts>
               <ts e="T2126" id="Seg_446" n="e" s="T2125">nanə". </ts>
               <ts e="T2127" id="Seg_448" n="e" s="T2126">Dĭʔnə </ts>
               <ts e="T2128" id="Seg_450" n="e" s="T2127">băʔpiʔi. </ts>
               <ts e="T2129" id="Seg_452" n="e" s="T2128">Dĭ </ts>
               <ts e="T2130" id="Seg_454" n="e" s="T2129">tibi </ts>
               <ts e="T2131" id="Seg_456" n="e" s="T2130">kuriza </ts>
               <ts e="T2132" id="Seg_458" n="e" s="T2131">nʼergöluʔpi, </ts>
               <ts e="T2133" id="Seg_460" n="e" s="T2132">a </ts>
               <ts e="T2134" id="Seg_462" n="e" s="T2133">bazoʔ </ts>
               <ts e="T2135" id="Seg_464" n="e" s="T2134">kirgarluʔpi. </ts>
               <ts e="T2136" id="Seg_466" n="e" s="T2135">"Kallam </ts>
               <ts e="T2137" id="Seg_468" n="e" s="T2136">dĭʔnə </ts>
               <ts e="T2138" id="Seg_470" n="e" s="T2137">münörzittə". </ts>
               <ts e="T2139" id="Seg_472" n="e" s="T2138">Dĭgəttə </ts>
               <ts e="T2140" id="Seg_474" n="e" s="T2139">šobi </ts>
               <ts e="T2141" id="Seg_476" n="e" s="T2140">šedendə. </ts>
               <ts e="T2142" id="Seg_478" n="e" s="T2141">Červəʔi, </ts>
               <ts e="T2143" id="Seg_480" n="e" s="T2142">muxaʔi </ts>
               <ts e="T2144" id="Seg_482" n="e" s="T2143">dʼaʔpi. </ts>
               <ts e="T2145" id="Seg_484" n="e" s="T2144">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T2072" id="Seg_485" s="T2065">PKZ_196X_Rooster_flk.001 (001)</ta>
            <ta e="T2076" id="Seg_486" s="T2072">PKZ_196X_Rooster_flk.002 (002)</ta>
            <ta e="T2081" id="Seg_487" s="T2076">PKZ_196X_Rooster_flk.003 (003)</ta>
            <ta e="T2083" id="Seg_488" s="T2081">PKZ_196X_Rooster_flk.004 (004)</ta>
            <ta e="T2089" id="Seg_489" s="T2083">PKZ_196X_Rooster_flk.005 (005)</ta>
            <ta e="T2101" id="Seg_490" s="T2089">PKZ_196X_Rooster_flk.006 (006)</ta>
            <ta e="T2104" id="Seg_491" s="T2101">PKZ_196X_Rooster_flk.007 (007)</ta>
            <ta e="T2109" id="Seg_492" s="T2104">PKZ_196X_Rooster_flk.008 (008)</ta>
            <ta e="T2113" id="Seg_493" s="T2109">PKZ_196X_Rooster_flk.009 (009)</ta>
            <ta e="T2119" id="Seg_494" s="T2113">PKZ_196X_Rooster_flk.010 (010)</ta>
            <ta e="T2123" id="Seg_495" s="T2119">PKZ_196X_Rooster_flk.011 (011)</ta>
            <ta e="T2126" id="Seg_496" s="T2123">PKZ_196X_Rooster_flk.012 (012)</ta>
            <ta e="T2128" id="Seg_497" s="T2126">PKZ_196X_Rooster_flk.013 (013)</ta>
            <ta e="T2135" id="Seg_498" s="T2128">PKZ_196X_Rooster_flk.014 (014)</ta>
            <ta e="T2138" id="Seg_499" s="T2135">PKZ_196X_Rooster_flk.015 (015)</ta>
            <ta e="T2141" id="Seg_500" s="T2138">PKZ_196X_Rooster_flk.016 (016)</ta>
            <ta e="T2144" id="Seg_501" s="T2141">PKZ_196X_Rooster_flk.017 (017)</ta>
            <ta e="T2145" id="Seg_502" s="T2144">PKZ_196X_Rooster_flk.018 (018)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T2072" id="Seg_503" s="T2065">Kuriza tibi nʼergölüʔpi zaplottə i davaj kirgarzittə. </ta>
            <ta e="T2076" id="Seg_504" s="T2072">"Măn koŋ, măn koŋ". </ta>
            <ta e="T2081" id="Seg_505" s="T2076">Kurizaʔi šobiʔi, tože davaj kirgarzittə. </ta>
            <ta e="T2083" id="Seg_506" s="T2081">"Tăn koŋ! </ta>
            <ta e="T2089" id="Seg_507" s="T2083">Ugaːndə idlial (măn=), tăn ulul kuvas". </ta>
            <ta e="T2101" id="Seg_508" s="T2089">Dĭgəttə (dĭ=) baška koŋ, kuza koŋ dĭm dʼappi da (davaj=) băʔpi uluttə. </ta>
            <ta e="T2104" id="Seg_509" s="T2101">Dĭgəttə mĭnzərbi pʼešgən. </ta>
            <ta e="T2109" id="Seg_510" s="T2104">Ambi, a dĭ uge kirgarlaʔbə. </ta>
            <ta e="T2113" id="Seg_511" s="T2109">"Măn koŋ, măn koŋ". </ta>
            <ta e="T2119" id="Seg_512" s="T2113">Dĭgəttə ertən uʔbdəbi, dĭ uge kirgarluʔpi. </ta>
            <ta e="T2123" id="Seg_513" s="T2119">Onʼiʔ, šide, nagur (kirgaria). </ta>
            <ta e="T2126" id="Seg_514" s="T2123">"Băʔgaʔ măna nanə". </ta>
            <ta e="T2128" id="Seg_515" s="T2126">Dĭʔnə băʔpiʔi. </ta>
            <ta e="T2135" id="Seg_516" s="T2128">Dĭ tibi kuriza nʼergöluʔpi, a bazoʔ kirgarluʔpi. </ta>
            <ta e="T2138" id="Seg_517" s="T2135">"Kallam dĭʔnə münörzittə". </ta>
            <ta e="T2141" id="Seg_518" s="T2138">Dĭgəttə šobi šedendə. </ta>
            <ta e="T2144" id="Seg_519" s="T2141">Červəʔi, muxaʔi dʼaʔpi. </ta>
            <ta e="T2145" id="Seg_520" s="T2144">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2066" id="Seg_521" s="T2065">kuriza</ta>
            <ta e="T2067" id="Seg_522" s="T2066">tibi</ta>
            <ta e="T2068" id="Seg_523" s="T2067">nʼergö-lüʔ-pi</ta>
            <ta e="T2069" id="Seg_524" s="T2068">zaplot-tə</ta>
            <ta e="T2070" id="Seg_525" s="T2069">i</ta>
            <ta e="T2071" id="Seg_526" s="T2070">davaj</ta>
            <ta e="T2072" id="Seg_527" s="T2071">kirgar-zittə</ta>
            <ta e="T2073" id="Seg_528" s="T2072">măn</ta>
            <ta e="T2074" id="Seg_529" s="T2073">koŋ</ta>
            <ta e="T2075" id="Seg_530" s="T2074">măn</ta>
            <ta e="T2076" id="Seg_531" s="T2075">koŋ</ta>
            <ta e="T2077" id="Seg_532" s="T2076">kuriza-ʔi</ta>
            <ta e="T2078" id="Seg_533" s="T2077">šo-bi-ʔi</ta>
            <ta e="T2079" id="Seg_534" s="T2078">tože</ta>
            <ta e="T2080" id="Seg_535" s="T2079">davaj</ta>
            <ta e="T2081" id="Seg_536" s="T2080">kirgar-zittə</ta>
            <ta e="T2082" id="Seg_537" s="T2081">tăn</ta>
            <ta e="T2083" id="Seg_538" s="T2082">koŋ</ta>
            <ta e="T2084" id="Seg_539" s="T2083">ugaːndə</ta>
            <ta e="T2085" id="Seg_540" s="T2084">id-lia-l</ta>
            <ta e="T2086" id="Seg_541" s="T2085">măn</ta>
            <ta e="T2087" id="Seg_542" s="T2086">tăn</ta>
            <ta e="T2088" id="Seg_543" s="T2087">ulu-l</ta>
            <ta e="T2089" id="Seg_544" s="T2088">kuvas</ta>
            <ta e="T2090" id="Seg_545" s="T2089">dĭgəttə</ta>
            <ta e="T2091" id="Seg_546" s="T2090">dĭ</ta>
            <ta e="T2092" id="Seg_547" s="T2091">baška</ta>
            <ta e="T2093" id="Seg_548" s="T2092">koŋ</ta>
            <ta e="T2094" id="Seg_549" s="T2093">kuza</ta>
            <ta e="T2095" id="Seg_550" s="T2094">koŋ</ta>
            <ta e="T2096" id="Seg_551" s="T2095">dĭ-m</ta>
            <ta e="T2097" id="Seg_552" s="T2096">dʼap-pi</ta>
            <ta e="T2098" id="Seg_553" s="T2097">da</ta>
            <ta e="T2099" id="Seg_554" s="T2098">davaj</ta>
            <ta e="T2100" id="Seg_555" s="T2099">băʔ-pi</ta>
            <ta e="T2101" id="Seg_556" s="T2100">ulu-ttə</ta>
            <ta e="T2102" id="Seg_557" s="T2101">dĭgəttə</ta>
            <ta e="T2103" id="Seg_558" s="T2102">mĭnzər-bi</ta>
            <ta e="T2104" id="Seg_559" s="T2103">pʼeš-gən</ta>
            <ta e="T2105" id="Seg_560" s="T2104">am-bi</ta>
            <ta e="T2106" id="Seg_561" s="T2105">a</ta>
            <ta e="T2107" id="Seg_562" s="T2106">dĭ</ta>
            <ta e="T2108" id="Seg_563" s="T2107">uge</ta>
            <ta e="T2109" id="Seg_564" s="T2108">kirgar-laʔbə</ta>
            <ta e="T2110" id="Seg_565" s="T2109">măn</ta>
            <ta e="T2111" id="Seg_566" s="T2110">koŋ</ta>
            <ta e="T2112" id="Seg_567" s="T2111">măn</ta>
            <ta e="T2113" id="Seg_568" s="T2112">koŋ</ta>
            <ta e="T2114" id="Seg_569" s="T2113">dĭgəttə</ta>
            <ta e="T2115" id="Seg_570" s="T2114">ertə-n</ta>
            <ta e="T2116" id="Seg_571" s="T2115">uʔbdə-bi</ta>
            <ta e="T2117" id="Seg_572" s="T2116">dĭ</ta>
            <ta e="T2118" id="Seg_573" s="T2117">uge</ta>
            <ta e="T2119" id="Seg_574" s="T2118">kirgar-luʔ-pi</ta>
            <ta e="T2120" id="Seg_575" s="T2119">onʼiʔ</ta>
            <ta e="T2121" id="Seg_576" s="T2120">šide</ta>
            <ta e="T2122" id="Seg_577" s="T2121">nagur</ta>
            <ta e="T2123" id="Seg_578" s="T2122">kirgar-ia</ta>
            <ta e="T2124" id="Seg_579" s="T2123">băʔ-gaʔ</ta>
            <ta e="T2125" id="Seg_580" s="T2124">măna</ta>
            <ta e="T2126" id="Seg_581" s="T2125">nanə</ta>
            <ta e="T2127" id="Seg_582" s="T2126">dĭʔ-nə</ta>
            <ta e="T2128" id="Seg_583" s="T2127">băʔ-pi-ʔi</ta>
            <ta e="T2129" id="Seg_584" s="T2128">dĭ</ta>
            <ta e="T2130" id="Seg_585" s="T2129">tibi</ta>
            <ta e="T2131" id="Seg_586" s="T2130">kuriza</ta>
            <ta e="T2132" id="Seg_587" s="T2131">nʼergö-luʔ-pi</ta>
            <ta e="T2133" id="Seg_588" s="T2132">a</ta>
            <ta e="T2134" id="Seg_589" s="T2133">bazoʔ</ta>
            <ta e="T2135" id="Seg_590" s="T2134">kirgar-luʔ-pi</ta>
            <ta e="T2136" id="Seg_591" s="T2135">kal-la-m</ta>
            <ta e="T2137" id="Seg_592" s="T2136">dĭʔ-nə</ta>
            <ta e="T2138" id="Seg_593" s="T2137">münör-zittə</ta>
            <ta e="T2139" id="Seg_594" s="T2138">dĭgəttə</ta>
            <ta e="T2140" id="Seg_595" s="T2139">šo-bi</ta>
            <ta e="T2141" id="Seg_596" s="T2140">šeden-də</ta>
            <ta e="T2142" id="Seg_597" s="T2141">červə-ʔi</ta>
            <ta e="T2143" id="Seg_598" s="T2142">muxa-ʔi</ta>
            <ta e="T2144" id="Seg_599" s="T2143">dʼaʔ-pi</ta>
            <ta e="T2145" id="Seg_600" s="T2144">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2066" id="Seg_601" s="T2065">kuriza</ta>
            <ta e="T2067" id="Seg_602" s="T2066">tibi</ta>
            <ta e="T2068" id="Seg_603" s="T2067">nʼergö-luʔbdə-bi</ta>
            <ta e="T2069" id="Seg_604" s="T2068">zaplot-Tə</ta>
            <ta e="T2070" id="Seg_605" s="T2069">i</ta>
            <ta e="T2071" id="Seg_606" s="T2070">davaj</ta>
            <ta e="T2072" id="Seg_607" s="T2071">kirgaːr-zittə</ta>
            <ta e="T2073" id="Seg_608" s="T2072">măn</ta>
            <ta e="T2074" id="Seg_609" s="T2073">koŋ</ta>
            <ta e="T2075" id="Seg_610" s="T2074">măn</ta>
            <ta e="T2076" id="Seg_611" s="T2075">koŋ</ta>
            <ta e="T2077" id="Seg_612" s="T2076">kuriza-jəʔ</ta>
            <ta e="T2078" id="Seg_613" s="T2077">šo-bi-jəʔ</ta>
            <ta e="T2079" id="Seg_614" s="T2078">tože</ta>
            <ta e="T2080" id="Seg_615" s="T2079">davaj</ta>
            <ta e="T2081" id="Seg_616" s="T2080">kirgaːr-zittə</ta>
            <ta e="T2082" id="Seg_617" s="T2081">tăn</ta>
            <ta e="T2083" id="Seg_618" s="T2082">koŋ</ta>
            <ta e="T2084" id="Seg_619" s="T2083">ugaːndə</ta>
            <ta e="T2085" id="Seg_620" s="T2084">ed-liA-l</ta>
            <ta e="T2086" id="Seg_621" s="T2085">măn</ta>
            <ta e="T2087" id="Seg_622" s="T2086">tăn</ta>
            <ta e="T2088" id="Seg_623" s="T2087">ulu-l</ta>
            <ta e="T2089" id="Seg_624" s="T2088">kuvas</ta>
            <ta e="T2090" id="Seg_625" s="T2089">dĭgəttə</ta>
            <ta e="T2091" id="Seg_626" s="T2090">dĭ</ta>
            <ta e="T2092" id="Seg_627" s="T2091">baška</ta>
            <ta e="T2093" id="Seg_628" s="T2092">koŋ</ta>
            <ta e="T2094" id="Seg_629" s="T2093">kuza</ta>
            <ta e="T2095" id="Seg_630" s="T2094">koŋ</ta>
            <ta e="T2096" id="Seg_631" s="T2095">dĭ-m</ta>
            <ta e="T2097" id="Seg_632" s="T2096">dʼabə-bi</ta>
            <ta e="T2098" id="Seg_633" s="T2097">da</ta>
            <ta e="T2099" id="Seg_634" s="T2098">davaj</ta>
            <ta e="T2100" id="Seg_635" s="T2099">băd-bi</ta>
            <ta e="T2101" id="Seg_636" s="T2100">ulu-ttə</ta>
            <ta e="T2102" id="Seg_637" s="T2101">dĭgəttə</ta>
            <ta e="T2103" id="Seg_638" s="T2102">mĭnzər-bi</ta>
            <ta e="T2104" id="Seg_639" s="T2103">pʼeːš-Kən</ta>
            <ta e="T2105" id="Seg_640" s="T2104">am-bi</ta>
            <ta e="T2106" id="Seg_641" s="T2105">a</ta>
            <ta e="T2107" id="Seg_642" s="T2106">dĭ</ta>
            <ta e="T2108" id="Seg_643" s="T2107">üge</ta>
            <ta e="T2109" id="Seg_644" s="T2108">kirgaːr-laʔbə</ta>
            <ta e="T2110" id="Seg_645" s="T2109">măn</ta>
            <ta e="T2111" id="Seg_646" s="T2110">koŋ</ta>
            <ta e="T2112" id="Seg_647" s="T2111">măn</ta>
            <ta e="T2113" id="Seg_648" s="T2112">koŋ</ta>
            <ta e="T2114" id="Seg_649" s="T2113">dĭgəttə</ta>
            <ta e="T2115" id="Seg_650" s="T2114">ertə-n</ta>
            <ta e="T2116" id="Seg_651" s="T2115">uʔbdə-bi</ta>
            <ta e="T2117" id="Seg_652" s="T2116">dĭ</ta>
            <ta e="T2118" id="Seg_653" s="T2117">üge</ta>
            <ta e="T2119" id="Seg_654" s="T2118">kirgaːr-luʔbdə-bi</ta>
            <ta e="T2120" id="Seg_655" s="T2119">onʼiʔ</ta>
            <ta e="T2121" id="Seg_656" s="T2120">šide</ta>
            <ta e="T2122" id="Seg_657" s="T2121">nagur</ta>
            <ta e="T2123" id="Seg_658" s="T2122">kirgaːr-liA</ta>
            <ta e="T2124" id="Seg_659" s="T2123">băt-KAʔ</ta>
            <ta e="T2125" id="Seg_660" s="T2124">măna</ta>
            <ta e="T2126" id="Seg_661" s="T2125">nanə</ta>
            <ta e="T2127" id="Seg_662" s="T2126">dĭ-Tə</ta>
            <ta e="T2128" id="Seg_663" s="T2127">băt-bi-jəʔ</ta>
            <ta e="T2129" id="Seg_664" s="T2128">dĭ</ta>
            <ta e="T2130" id="Seg_665" s="T2129">tibi</ta>
            <ta e="T2131" id="Seg_666" s="T2130">kuriza</ta>
            <ta e="T2132" id="Seg_667" s="T2131">nʼergö-luʔbdə-bi</ta>
            <ta e="T2133" id="Seg_668" s="T2132">a</ta>
            <ta e="T2134" id="Seg_669" s="T2133">bazoʔ</ta>
            <ta e="T2135" id="Seg_670" s="T2134">kirgaːr-luʔbdə-bi</ta>
            <ta e="T2136" id="Seg_671" s="T2135">kan-lV-m</ta>
            <ta e="T2137" id="Seg_672" s="T2136">dĭ-Tə</ta>
            <ta e="T2138" id="Seg_673" s="T2137">münör-zittə</ta>
            <ta e="T2139" id="Seg_674" s="T2138">dĭgəttə</ta>
            <ta e="T2140" id="Seg_675" s="T2139">šo-bi</ta>
            <ta e="T2141" id="Seg_676" s="T2140">šeden-də</ta>
            <ta e="T2142" id="Seg_677" s="T2141">červə-jəʔ</ta>
            <ta e="T2143" id="Seg_678" s="T2142">muxa-jəʔ</ta>
            <ta e="T2144" id="Seg_679" s="T2143">dʼabə-bi</ta>
            <ta e="T2145" id="Seg_680" s="T2144">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2066" id="Seg_681" s="T2065">hen.[NOM.SG]</ta>
            <ta e="T2067" id="Seg_682" s="T2066">man.[NOM.SG]</ta>
            <ta e="T2068" id="Seg_683" s="T2067">fly-MOM-PST.[3SG]</ta>
            <ta e="T2069" id="Seg_684" s="T2068">fence-LAT</ta>
            <ta e="T2070" id="Seg_685" s="T2069">and</ta>
            <ta e="T2071" id="Seg_686" s="T2070">INCH</ta>
            <ta e="T2072" id="Seg_687" s="T2071">shout-INF.LAT</ta>
            <ta e="T2073" id="Seg_688" s="T2072">I.NOM</ta>
            <ta e="T2074" id="Seg_689" s="T2073">chief.[NOM.SG]</ta>
            <ta e="T2075" id="Seg_690" s="T2074">I.NOM</ta>
            <ta e="T2076" id="Seg_691" s="T2075">chief.[NOM.SG]</ta>
            <ta e="T2077" id="Seg_692" s="T2076">hen-PL</ta>
            <ta e="T2078" id="Seg_693" s="T2077">come-PST-3PL</ta>
            <ta e="T2079" id="Seg_694" s="T2078">also</ta>
            <ta e="T2080" id="Seg_695" s="T2079">INCH</ta>
            <ta e="T2081" id="Seg_696" s="T2080">shout-INF.LAT</ta>
            <ta e="T2082" id="Seg_697" s="T2081">you.NOM</ta>
            <ta e="T2083" id="Seg_698" s="T2082">chief.[NOM.SG]</ta>
            <ta e="T2084" id="Seg_699" s="T2083">very</ta>
            <ta e="T2085" id="Seg_700" s="T2084">be.visible-PRS-2SG</ta>
            <ta e="T2086" id="Seg_701" s="T2085">I.NOM</ta>
            <ta e="T2087" id="Seg_702" s="T2086">you.GEN</ta>
            <ta e="T2088" id="Seg_703" s="T2087">head-NOM/GEN/ACC.2SG</ta>
            <ta e="T2089" id="Seg_704" s="T2088">beautiful.[NOM.SG]</ta>
            <ta e="T2090" id="Seg_705" s="T2089">then</ta>
            <ta e="T2091" id="Seg_706" s="T2090">this.[NOM.SG]</ta>
            <ta e="T2092" id="Seg_707" s="T2091">another.[NOM.SG]</ta>
            <ta e="T2093" id="Seg_708" s="T2092">chief.[NOM.SG]</ta>
            <ta e="T2094" id="Seg_709" s="T2093">man.[NOM.SG]</ta>
            <ta e="T2095" id="Seg_710" s="T2094">chief.[NOM.SG]</ta>
            <ta e="T2096" id="Seg_711" s="T2095">this-ACC</ta>
            <ta e="T2097" id="Seg_712" s="T2096">capture-PST.[3SG]</ta>
            <ta e="T2098" id="Seg_713" s="T2097">and</ta>
            <ta e="T2099" id="Seg_714" s="T2098">INCH</ta>
            <ta e="T2100" id="Seg_715" s="T2099">%%-PST.[3SG]</ta>
            <ta e="T2101" id="Seg_716" s="T2100">head-ABL.3SG</ta>
            <ta e="T2102" id="Seg_717" s="T2101">then</ta>
            <ta e="T2103" id="Seg_718" s="T2102">boil-PST.[3SG]</ta>
            <ta e="T2104" id="Seg_719" s="T2103">stove-LOC</ta>
            <ta e="T2105" id="Seg_720" s="T2104">eat-PST.[3SG]</ta>
            <ta e="T2106" id="Seg_721" s="T2105">and</ta>
            <ta e="T2107" id="Seg_722" s="T2106">this.[NOM.SG]</ta>
            <ta e="T2108" id="Seg_723" s="T2107">always</ta>
            <ta e="T2109" id="Seg_724" s="T2108">shout-DUR.[3SG]</ta>
            <ta e="T2110" id="Seg_725" s="T2109">I.NOM</ta>
            <ta e="T2111" id="Seg_726" s="T2110">chief.[NOM.SG]</ta>
            <ta e="T2112" id="Seg_727" s="T2111">I.NOM</ta>
            <ta e="T2113" id="Seg_728" s="T2112">chief.[NOM.SG]</ta>
            <ta e="T2114" id="Seg_729" s="T2113">then</ta>
            <ta e="T2115" id="Seg_730" s="T2114">morning-GEN</ta>
            <ta e="T2116" id="Seg_731" s="T2115">get.up-PST.[3SG]</ta>
            <ta e="T2117" id="Seg_732" s="T2116">this.[NOM.SG]</ta>
            <ta e="T2118" id="Seg_733" s="T2117">always</ta>
            <ta e="T2119" id="Seg_734" s="T2118">shout-MOM-PST.[3SG]</ta>
            <ta e="T2120" id="Seg_735" s="T2119">one.[NOM.SG]</ta>
            <ta e="T2121" id="Seg_736" s="T2120">two.[NOM.SG]</ta>
            <ta e="T2122" id="Seg_737" s="T2121">three.[NOM.SG]</ta>
            <ta e="T2123" id="Seg_738" s="T2122">shout-PRS.[3SG]</ta>
            <ta e="T2124" id="Seg_739" s="T2123">cut-IMP.2PL</ta>
            <ta e="T2125" id="Seg_740" s="T2124">I.LAT</ta>
            <ta e="T2126" id="Seg_741" s="T2125">belly.[NOM.SG]</ta>
            <ta e="T2127" id="Seg_742" s="T2126">this-LAT</ta>
            <ta e="T2128" id="Seg_743" s="T2127">cut-PST-3PL</ta>
            <ta e="T2129" id="Seg_744" s="T2128">this.[NOM.SG]</ta>
            <ta e="T2130" id="Seg_745" s="T2129">man.[NOM.SG]</ta>
            <ta e="T2131" id="Seg_746" s="T2130">hen.[NOM.SG]</ta>
            <ta e="T2132" id="Seg_747" s="T2131">fly-MOM-PST.[3SG]</ta>
            <ta e="T2133" id="Seg_748" s="T2132">and</ta>
            <ta e="T2134" id="Seg_749" s="T2133">again</ta>
            <ta e="T2135" id="Seg_750" s="T2134">shout-MOM-PST.[3SG]</ta>
            <ta e="T2136" id="Seg_751" s="T2135">go-FUT-1SG</ta>
            <ta e="T2137" id="Seg_752" s="T2136">this-LAT</ta>
            <ta e="T2138" id="Seg_753" s="T2137">beat-INF.LAT</ta>
            <ta e="T2139" id="Seg_754" s="T2138">then</ta>
            <ta e="T2140" id="Seg_755" s="T2139">come-PST.[3SG]</ta>
            <ta e="T2141" id="Seg_756" s="T2140">corral-NOM/GEN/ACC.3SG</ta>
            <ta e="T2142" id="Seg_757" s="T2141">worm-NOM/GEN/ACC.3PL</ta>
            <ta e="T2143" id="Seg_758" s="T2142">fly-PL</ta>
            <ta e="T2144" id="Seg_759" s="T2143">capture-PST.[3SG]</ta>
            <ta e="T2145" id="Seg_760" s="T2144">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2066" id="Seg_761" s="T2065">курица.[NOM.SG]</ta>
            <ta e="T2067" id="Seg_762" s="T2066">мужчина.[NOM.SG]</ta>
            <ta e="T2068" id="Seg_763" s="T2067">лететь-MOM-PST.[3SG]</ta>
            <ta e="T2069" id="Seg_764" s="T2068">забор-LAT</ta>
            <ta e="T2070" id="Seg_765" s="T2069">и</ta>
            <ta e="T2071" id="Seg_766" s="T2070">INCH</ta>
            <ta e="T2072" id="Seg_767" s="T2071">кричать-INF.LAT</ta>
            <ta e="T2073" id="Seg_768" s="T2072">я.NOM</ta>
            <ta e="T2074" id="Seg_769" s="T2073">вождь.[NOM.SG]</ta>
            <ta e="T2075" id="Seg_770" s="T2074">я.NOM</ta>
            <ta e="T2076" id="Seg_771" s="T2075">вождь.[NOM.SG]</ta>
            <ta e="T2077" id="Seg_772" s="T2076">курица-PL</ta>
            <ta e="T2078" id="Seg_773" s="T2077">прийти-PST-3PL</ta>
            <ta e="T2079" id="Seg_774" s="T2078">тоже</ta>
            <ta e="T2080" id="Seg_775" s="T2079">INCH</ta>
            <ta e="T2081" id="Seg_776" s="T2080">кричать-INF.LAT</ta>
            <ta e="T2082" id="Seg_777" s="T2081">ты.NOM</ta>
            <ta e="T2083" id="Seg_778" s="T2082">вождь.[NOM.SG]</ta>
            <ta e="T2084" id="Seg_779" s="T2083">очень</ta>
            <ta e="T2085" id="Seg_780" s="T2084">быть.видным-PRS-2SG</ta>
            <ta e="T2086" id="Seg_781" s="T2085">я.NOM</ta>
            <ta e="T2087" id="Seg_782" s="T2086">ты.GEN</ta>
            <ta e="T2088" id="Seg_783" s="T2087">голова-NOM/GEN/ACC.2SG</ta>
            <ta e="T2089" id="Seg_784" s="T2088">красивый.[NOM.SG]</ta>
            <ta e="T2090" id="Seg_785" s="T2089">тогда</ta>
            <ta e="T2091" id="Seg_786" s="T2090">этот.[NOM.SG]</ta>
            <ta e="T2092" id="Seg_787" s="T2091">другой.[NOM.SG]</ta>
            <ta e="T2093" id="Seg_788" s="T2092">вождь.[NOM.SG]</ta>
            <ta e="T2094" id="Seg_789" s="T2093">мужчина.[NOM.SG]</ta>
            <ta e="T2095" id="Seg_790" s="T2094">вождь.[NOM.SG]</ta>
            <ta e="T2096" id="Seg_791" s="T2095">этот-ACC</ta>
            <ta e="T2097" id="Seg_792" s="T2096">ловить-PST.[3SG]</ta>
            <ta e="T2098" id="Seg_793" s="T2097">и</ta>
            <ta e="T2099" id="Seg_794" s="T2098">INCH</ta>
            <ta e="T2100" id="Seg_795" s="T2099">%%-PST.[3SG]</ta>
            <ta e="T2101" id="Seg_796" s="T2100">голова-ABL.3SG</ta>
            <ta e="T2102" id="Seg_797" s="T2101">тогда</ta>
            <ta e="T2103" id="Seg_798" s="T2102">кипятить-PST.[3SG]</ta>
            <ta e="T2104" id="Seg_799" s="T2103">печь-LOC</ta>
            <ta e="T2105" id="Seg_800" s="T2104">съесть-PST.[3SG]</ta>
            <ta e="T2106" id="Seg_801" s="T2105">а</ta>
            <ta e="T2107" id="Seg_802" s="T2106">этот.[NOM.SG]</ta>
            <ta e="T2108" id="Seg_803" s="T2107">всегда</ta>
            <ta e="T2109" id="Seg_804" s="T2108">кричать-DUR.[3SG]</ta>
            <ta e="T2110" id="Seg_805" s="T2109">я.NOM</ta>
            <ta e="T2111" id="Seg_806" s="T2110">вождь.[NOM.SG]</ta>
            <ta e="T2112" id="Seg_807" s="T2111">я.NOM</ta>
            <ta e="T2113" id="Seg_808" s="T2112">вождь.[NOM.SG]</ta>
            <ta e="T2114" id="Seg_809" s="T2113">тогда</ta>
            <ta e="T2115" id="Seg_810" s="T2114">утро-GEN</ta>
            <ta e="T2116" id="Seg_811" s="T2115">встать-PST.[3SG]</ta>
            <ta e="T2117" id="Seg_812" s="T2116">этот.[NOM.SG]</ta>
            <ta e="T2118" id="Seg_813" s="T2117">всегда</ta>
            <ta e="T2119" id="Seg_814" s="T2118">кричать-MOM-PST.[3SG]</ta>
            <ta e="T2120" id="Seg_815" s="T2119">один.[NOM.SG]</ta>
            <ta e="T2121" id="Seg_816" s="T2120">два.[NOM.SG]</ta>
            <ta e="T2122" id="Seg_817" s="T2121">три.[NOM.SG]</ta>
            <ta e="T2123" id="Seg_818" s="T2122">кричать-PRS.[3SG]</ta>
            <ta e="T2124" id="Seg_819" s="T2123">резать-IMP.2PL</ta>
            <ta e="T2125" id="Seg_820" s="T2124">я.LAT</ta>
            <ta e="T2126" id="Seg_821" s="T2125">живот.[NOM.SG]</ta>
            <ta e="T2127" id="Seg_822" s="T2126">этот-LAT</ta>
            <ta e="T2128" id="Seg_823" s="T2127">резать-PST-3PL</ta>
            <ta e="T2129" id="Seg_824" s="T2128">этот.[NOM.SG]</ta>
            <ta e="T2130" id="Seg_825" s="T2129">мужчина.[NOM.SG]</ta>
            <ta e="T2131" id="Seg_826" s="T2130">курица.[NOM.SG]</ta>
            <ta e="T2132" id="Seg_827" s="T2131">лететь-MOM-PST.[3SG]</ta>
            <ta e="T2133" id="Seg_828" s="T2132">а</ta>
            <ta e="T2134" id="Seg_829" s="T2133">опять</ta>
            <ta e="T2135" id="Seg_830" s="T2134">кричать-MOM-PST.[3SG]</ta>
            <ta e="T2136" id="Seg_831" s="T2135">пойти-FUT-1SG</ta>
            <ta e="T2137" id="Seg_832" s="T2136">этот-LAT</ta>
            <ta e="T2138" id="Seg_833" s="T2137">бить-INF.LAT</ta>
            <ta e="T2139" id="Seg_834" s="T2138">тогда</ta>
            <ta e="T2140" id="Seg_835" s="T2139">прийти-PST.[3SG]</ta>
            <ta e="T2141" id="Seg_836" s="T2140">кораль-NOM/GEN/ACC.3SG</ta>
            <ta e="T2142" id="Seg_837" s="T2141">червь-NOM/GEN/ACC.3PL</ta>
            <ta e="T2143" id="Seg_838" s="T2142">муха-PL</ta>
            <ta e="T2144" id="Seg_839" s="T2143">ловить-PST.[3SG]</ta>
            <ta e="T2145" id="Seg_840" s="T2144">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2066" id="Seg_841" s="T2065">n-n:case</ta>
            <ta e="T2067" id="Seg_842" s="T2066">n-n:case</ta>
            <ta e="T2068" id="Seg_843" s="T2067">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2069" id="Seg_844" s="T2068">n-n:case</ta>
            <ta e="T2070" id="Seg_845" s="T2069">conj</ta>
            <ta e="T2071" id="Seg_846" s="T2070">ptcl</ta>
            <ta e="T2072" id="Seg_847" s="T2071">v-v:n.fin</ta>
            <ta e="T2073" id="Seg_848" s="T2072">pers</ta>
            <ta e="T2074" id="Seg_849" s="T2073">n-n:case</ta>
            <ta e="T2075" id="Seg_850" s="T2074">pers</ta>
            <ta e="T2076" id="Seg_851" s="T2075">n-n:case</ta>
            <ta e="T2077" id="Seg_852" s="T2076">n-n:num</ta>
            <ta e="T2078" id="Seg_853" s="T2077">v-v:tense-v:pn</ta>
            <ta e="T2079" id="Seg_854" s="T2078">ptcl</ta>
            <ta e="T2080" id="Seg_855" s="T2079">ptcl</ta>
            <ta e="T2081" id="Seg_856" s="T2080">v-v:n.fin</ta>
            <ta e="T2082" id="Seg_857" s="T2081">pers</ta>
            <ta e="T2083" id="Seg_858" s="T2082">n-n:case</ta>
            <ta e="T2084" id="Seg_859" s="T2083">adv</ta>
            <ta e="T2085" id="Seg_860" s="T2084">v-v:tense-v:pn</ta>
            <ta e="T2086" id="Seg_861" s="T2085">pers</ta>
            <ta e="T2087" id="Seg_862" s="T2086">pers</ta>
            <ta e="T2088" id="Seg_863" s="T2087">n-n:case.poss</ta>
            <ta e="T2089" id="Seg_864" s="T2088">adj-n:case</ta>
            <ta e="T2090" id="Seg_865" s="T2089">adv</ta>
            <ta e="T2091" id="Seg_866" s="T2090">dempro-n:case</ta>
            <ta e="T2092" id="Seg_867" s="T2091">adj-n:case</ta>
            <ta e="T2093" id="Seg_868" s="T2092">n-n:case</ta>
            <ta e="T2094" id="Seg_869" s="T2093">n-n:case</ta>
            <ta e="T2095" id="Seg_870" s="T2094">n-n:case</ta>
            <ta e="T2096" id="Seg_871" s="T2095">dempro-n:case</ta>
            <ta e="T2097" id="Seg_872" s="T2096">v-v:tense-v:pn</ta>
            <ta e="T2098" id="Seg_873" s="T2097">conj</ta>
            <ta e="T2099" id="Seg_874" s="T2098">ptcl</ta>
            <ta e="T2100" id="Seg_875" s="T2099">v-v:tense-v:pn</ta>
            <ta e="T2101" id="Seg_876" s="T2100">n-n:case.poss</ta>
            <ta e="T2102" id="Seg_877" s="T2101">adv</ta>
            <ta e="T2103" id="Seg_878" s="T2102">v-v:tense-v:pn</ta>
            <ta e="T2104" id="Seg_879" s="T2103">n-n:case</ta>
            <ta e="T2105" id="Seg_880" s="T2104">v-v:tense-v:pn</ta>
            <ta e="T2106" id="Seg_881" s="T2105">conj</ta>
            <ta e="T2107" id="Seg_882" s="T2106">dempro-n:case</ta>
            <ta e="T2108" id="Seg_883" s="T2107">adv</ta>
            <ta e="T2109" id="Seg_884" s="T2108">v-v&gt;v-v:pn</ta>
            <ta e="T2110" id="Seg_885" s="T2109">pers</ta>
            <ta e="T2111" id="Seg_886" s="T2110">n-n:case</ta>
            <ta e="T2112" id="Seg_887" s="T2111">pers</ta>
            <ta e="T2113" id="Seg_888" s="T2112">n-n:case</ta>
            <ta e="T2114" id="Seg_889" s="T2113">adv</ta>
            <ta e="T2115" id="Seg_890" s="T2114">n-n:case</ta>
            <ta e="T2116" id="Seg_891" s="T2115">v-v:tense-v:pn</ta>
            <ta e="T2117" id="Seg_892" s="T2116">dempro-n:case</ta>
            <ta e="T2118" id="Seg_893" s="T2117">adv</ta>
            <ta e="T2119" id="Seg_894" s="T2118">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2120" id="Seg_895" s="T2119">num-n:case</ta>
            <ta e="T2121" id="Seg_896" s="T2120">num-n:case</ta>
            <ta e="T2122" id="Seg_897" s="T2121">num-n:case</ta>
            <ta e="T2123" id="Seg_898" s="T2122">v-v:tense-v:pn</ta>
            <ta e="T2124" id="Seg_899" s="T2123">v-v:mood.pn</ta>
            <ta e="T2125" id="Seg_900" s="T2124">pers</ta>
            <ta e="T2126" id="Seg_901" s="T2125">n-n:case</ta>
            <ta e="T2127" id="Seg_902" s="T2126">dempro-n:case</ta>
            <ta e="T2128" id="Seg_903" s="T2127">v-v:tense-v:pn</ta>
            <ta e="T2129" id="Seg_904" s="T2128">dempro-n:case</ta>
            <ta e="T2130" id="Seg_905" s="T2129">n-n:case</ta>
            <ta e="T2131" id="Seg_906" s="T2130">n-n:case</ta>
            <ta e="T2132" id="Seg_907" s="T2131">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2133" id="Seg_908" s="T2132">conj</ta>
            <ta e="T2134" id="Seg_909" s="T2133">adv</ta>
            <ta e="T2135" id="Seg_910" s="T2134">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2136" id="Seg_911" s="T2135">v-v:tense-v:pn</ta>
            <ta e="T2137" id="Seg_912" s="T2136">dempro-n:case</ta>
            <ta e="T2138" id="Seg_913" s="T2137">v-v:n.fin</ta>
            <ta e="T2139" id="Seg_914" s="T2138">adv</ta>
            <ta e="T2140" id="Seg_915" s="T2139">v-v:tense-v:pn</ta>
            <ta e="T2141" id="Seg_916" s="T2140">n-n:case.poss</ta>
            <ta e="T2142" id="Seg_917" s="T2141">n-n:case.poss</ta>
            <ta e="T2143" id="Seg_918" s="T2142">n-n:num</ta>
            <ta e="T2144" id="Seg_919" s="T2143">v-v:tense-v:pn</ta>
            <ta e="T2145" id="Seg_920" s="T2144">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2066" id="Seg_921" s="T2065">n</ta>
            <ta e="T2067" id="Seg_922" s="T2066">n</ta>
            <ta e="T2068" id="Seg_923" s="T2067">v</ta>
            <ta e="T2069" id="Seg_924" s="T2068">n</ta>
            <ta e="T2070" id="Seg_925" s="T2069">conj</ta>
            <ta e="T2071" id="Seg_926" s="T2070">ptcl</ta>
            <ta e="T2072" id="Seg_927" s="T2071">v</ta>
            <ta e="T2073" id="Seg_928" s="T2072">pers</ta>
            <ta e="T2074" id="Seg_929" s="T2073">n</ta>
            <ta e="T2075" id="Seg_930" s="T2074">pers</ta>
            <ta e="T2076" id="Seg_931" s="T2075">n</ta>
            <ta e="T2077" id="Seg_932" s="T2076">n</ta>
            <ta e="T2078" id="Seg_933" s="T2077">v</ta>
            <ta e="T2079" id="Seg_934" s="T2078">ptcl</ta>
            <ta e="T2080" id="Seg_935" s="T2079">ptcl</ta>
            <ta e="T2081" id="Seg_936" s="T2080">v</ta>
            <ta e="T2082" id="Seg_937" s="T2081">pers</ta>
            <ta e="T2083" id="Seg_938" s="T2082">n</ta>
            <ta e="T2084" id="Seg_939" s="T2083">adv</ta>
            <ta e="T2085" id="Seg_940" s="T2084">v</ta>
            <ta e="T2086" id="Seg_941" s="T2085">pers</ta>
            <ta e="T2087" id="Seg_942" s="T2086">pers</ta>
            <ta e="T2088" id="Seg_943" s="T2087">n</ta>
            <ta e="T2089" id="Seg_944" s="T2088">adj</ta>
            <ta e="T2090" id="Seg_945" s="T2089">adv</ta>
            <ta e="T2091" id="Seg_946" s="T2090">dempro</ta>
            <ta e="T2092" id="Seg_947" s="T2091">adj</ta>
            <ta e="T2093" id="Seg_948" s="T2092">n</ta>
            <ta e="T2094" id="Seg_949" s="T2093">n</ta>
            <ta e="T2095" id="Seg_950" s="T2094">n</ta>
            <ta e="T2096" id="Seg_951" s="T2095">dempro</ta>
            <ta e="T2097" id="Seg_952" s="T2096">v</ta>
            <ta e="T2098" id="Seg_953" s="T2097">conj</ta>
            <ta e="T2099" id="Seg_954" s="T2098">ptcl</ta>
            <ta e="T2100" id="Seg_955" s="T2099">v</ta>
            <ta e="T2101" id="Seg_956" s="T2100">n</ta>
            <ta e="T2102" id="Seg_957" s="T2101">adv</ta>
            <ta e="T2103" id="Seg_958" s="T2102">v</ta>
            <ta e="T2104" id="Seg_959" s="T2103">n</ta>
            <ta e="T2105" id="Seg_960" s="T2104">v</ta>
            <ta e="T2106" id="Seg_961" s="T2105">conj</ta>
            <ta e="T2107" id="Seg_962" s="T2106">dempro</ta>
            <ta e="T2108" id="Seg_963" s="T2107">adv</ta>
            <ta e="T2109" id="Seg_964" s="T2108">v</ta>
            <ta e="T2110" id="Seg_965" s="T2109">pers</ta>
            <ta e="T2111" id="Seg_966" s="T2110">n</ta>
            <ta e="T2112" id="Seg_967" s="T2111">pers</ta>
            <ta e="T2113" id="Seg_968" s="T2112">n</ta>
            <ta e="T2114" id="Seg_969" s="T2113">adv</ta>
            <ta e="T2115" id="Seg_970" s="T2114">n</ta>
            <ta e="T2116" id="Seg_971" s="T2115">v</ta>
            <ta e="T2117" id="Seg_972" s="T2116">dempro</ta>
            <ta e="T2118" id="Seg_973" s="T2117">adv</ta>
            <ta e="T2119" id="Seg_974" s="T2118">v</ta>
            <ta e="T2120" id="Seg_975" s="T2119">num</ta>
            <ta e="T2121" id="Seg_976" s="T2120">num</ta>
            <ta e="T2122" id="Seg_977" s="T2121">num</ta>
            <ta e="T2123" id="Seg_978" s="T2122">v</ta>
            <ta e="T2124" id="Seg_979" s="T2123">v</ta>
            <ta e="T2125" id="Seg_980" s="T2124">pers</ta>
            <ta e="T2126" id="Seg_981" s="T2125">n</ta>
            <ta e="T2127" id="Seg_982" s="T2126">dempro</ta>
            <ta e="T2128" id="Seg_983" s="T2127">v</ta>
            <ta e="T2129" id="Seg_984" s="T2128">dempro</ta>
            <ta e="T2130" id="Seg_985" s="T2129">n</ta>
            <ta e="T2131" id="Seg_986" s="T2130">n</ta>
            <ta e="T2132" id="Seg_987" s="T2131">v</ta>
            <ta e="T2133" id="Seg_988" s="T2132">conj</ta>
            <ta e="T2134" id="Seg_989" s="T2133">adv</ta>
            <ta e="T2135" id="Seg_990" s="T2134">v</ta>
            <ta e="T2136" id="Seg_991" s="T2135">v</ta>
            <ta e="T2137" id="Seg_992" s="T2136">dempro</ta>
            <ta e="T2138" id="Seg_993" s="T2137">v</ta>
            <ta e="T2139" id="Seg_994" s="T2138">adv</ta>
            <ta e="T2140" id="Seg_995" s="T2139">v</ta>
            <ta e="T2141" id="Seg_996" s="T2140">n</ta>
            <ta e="T2142" id="Seg_997" s="T2141">n</ta>
            <ta e="T2143" id="Seg_998" s="T2142">n</ta>
            <ta e="T2144" id="Seg_999" s="T2143">v</ta>
            <ta e="T2145" id="Seg_1000" s="T2144">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2066" id="Seg_1001" s="T2065">np.h:Poss</ta>
            <ta e="T2067" id="Seg_1002" s="T2066">np.h:A</ta>
            <ta e="T2069" id="Seg_1003" s="T2068">np:G</ta>
            <ta e="T2073" id="Seg_1004" s="T2072">pro.h:Th</ta>
            <ta e="T2074" id="Seg_1005" s="T2073">np.h:Th</ta>
            <ta e="T2075" id="Seg_1006" s="T2074">pro.h:Th</ta>
            <ta e="T2076" id="Seg_1007" s="T2075">np.h:Th</ta>
            <ta e="T2077" id="Seg_1008" s="T2076">np.h:A</ta>
            <ta e="T2082" id="Seg_1009" s="T2081">pro.h:Th</ta>
            <ta e="T2083" id="Seg_1010" s="T2082">np.h:Th</ta>
            <ta e="T2085" id="Seg_1011" s="T2084">0.2.h:Th</ta>
            <ta e="T2087" id="Seg_1012" s="T2086">pro.h:Poss</ta>
            <ta e="T2088" id="Seg_1013" s="T2087">np:Th</ta>
            <ta e="T2090" id="Seg_1014" s="T2089">adv:Time</ta>
            <ta e="T2093" id="Seg_1015" s="T2092">np.h:A</ta>
            <ta e="T2094" id="Seg_1016" s="T2093">np.h:Poss</ta>
            <ta e="T2095" id="Seg_1017" s="T2094">np.h:A</ta>
            <ta e="T2096" id="Seg_1018" s="T2095">pro.h:Th</ta>
            <ta e="T2101" id="Seg_1019" s="T2100">np:P</ta>
            <ta e="T2102" id="Seg_1020" s="T2101">adv:Time</ta>
            <ta e="T2103" id="Seg_1021" s="T2102">0.3.h:A</ta>
            <ta e="T2104" id="Seg_1022" s="T2103">np:L</ta>
            <ta e="T2105" id="Seg_1023" s="T2104">0.3.h:A</ta>
            <ta e="T2107" id="Seg_1024" s="T2106">pro.h:A</ta>
            <ta e="T2110" id="Seg_1025" s="T2109">pro.h:Th</ta>
            <ta e="T2111" id="Seg_1026" s="T2110">np.h:Th</ta>
            <ta e="T2112" id="Seg_1027" s="T2111">pro.h:Th</ta>
            <ta e="T2113" id="Seg_1028" s="T2112">np.h:Th</ta>
            <ta e="T2114" id="Seg_1029" s="T2113">adv:Time</ta>
            <ta e="T2115" id="Seg_1030" s="T2114">n:Time</ta>
            <ta e="T2116" id="Seg_1031" s="T2115">0.3.h:A</ta>
            <ta e="T2117" id="Seg_1032" s="T2116">pro.h:A</ta>
            <ta e="T2123" id="Seg_1033" s="T2122">0.3.h:A</ta>
            <ta e="T2124" id="Seg_1034" s="T2123">0.2.h:A</ta>
            <ta e="T2125" id="Seg_1035" s="T2124">pro.h:Poss</ta>
            <ta e="T2126" id="Seg_1036" s="T2125">np:P</ta>
            <ta e="T2127" id="Seg_1037" s="T2126">pro:P</ta>
            <ta e="T2128" id="Seg_1038" s="T2127">0.3.h:A</ta>
            <ta e="T2130" id="Seg_1039" s="T2129">np.h:A</ta>
            <ta e="T2131" id="Seg_1040" s="T2130">np.h:Poss</ta>
            <ta e="T2135" id="Seg_1041" s="T2134">0.3.h:A</ta>
            <ta e="T2136" id="Seg_1042" s="T2135">0.1.h:A</ta>
            <ta e="T2137" id="Seg_1043" s="T2136">pro:G</ta>
            <ta e="T2139" id="Seg_1044" s="T2138">adv:Time</ta>
            <ta e="T2140" id="Seg_1045" s="T2139">0.3.h:A</ta>
            <ta e="T2141" id="Seg_1046" s="T2140">np:G</ta>
            <ta e="T2142" id="Seg_1047" s="T2141">np:P</ta>
            <ta e="T2143" id="Seg_1048" s="T2142">np:P</ta>
            <ta e="T2144" id="Seg_1049" s="T2143">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2067" id="Seg_1050" s="T2066">np.h:S</ta>
            <ta e="T2068" id="Seg_1051" s="T2067">v:pred</ta>
            <ta e="T2071" id="Seg_1052" s="T2070">ptcl:pred</ta>
            <ta e="T2073" id="Seg_1053" s="T2072">pro.h:S</ta>
            <ta e="T2074" id="Seg_1054" s="T2073">n:pred</ta>
            <ta e="T2075" id="Seg_1055" s="T2074">pro.h:S</ta>
            <ta e="T2076" id="Seg_1056" s="T2075">n:pred</ta>
            <ta e="T2077" id="Seg_1057" s="T2076">np.h:S</ta>
            <ta e="T2078" id="Seg_1058" s="T2077">v:pred</ta>
            <ta e="T2080" id="Seg_1059" s="T2079">ptcl:pred</ta>
            <ta e="T2082" id="Seg_1060" s="T2081">pro.h:S</ta>
            <ta e="T2083" id="Seg_1061" s="T2082">n:pred</ta>
            <ta e="T2085" id="Seg_1062" s="T2084">v:pred 0.2.h:S</ta>
            <ta e="T2088" id="Seg_1063" s="T2087">np:S</ta>
            <ta e="T2089" id="Seg_1064" s="T2088">adj:pred</ta>
            <ta e="T2093" id="Seg_1065" s="T2092">np.h:S</ta>
            <ta e="T2095" id="Seg_1066" s="T2094">np.h:S</ta>
            <ta e="T2096" id="Seg_1067" s="T2095">pro.h:O</ta>
            <ta e="T2097" id="Seg_1068" s="T2096">v:pred</ta>
            <ta e="T2099" id="Seg_1069" s="T2098">ptcl:pred</ta>
            <ta e="T2101" id="Seg_1070" s="T2100">np:O</ta>
            <ta e="T2103" id="Seg_1071" s="T2102">v:pred 0.3.h:S</ta>
            <ta e="T2105" id="Seg_1072" s="T2104">v:pred 0.3.h:S</ta>
            <ta e="T2107" id="Seg_1073" s="T2106">pro.h:S</ta>
            <ta e="T2109" id="Seg_1074" s="T2108">v:pred</ta>
            <ta e="T2110" id="Seg_1075" s="T2109">pro.h:S</ta>
            <ta e="T2111" id="Seg_1076" s="T2110">n:pred</ta>
            <ta e="T2112" id="Seg_1077" s="T2111">pro.h:S</ta>
            <ta e="T2113" id="Seg_1078" s="T2112">n:pred</ta>
            <ta e="T2116" id="Seg_1079" s="T2115">v:pred 0.3.h:S</ta>
            <ta e="T2117" id="Seg_1080" s="T2116">pro.h:S</ta>
            <ta e="T2119" id="Seg_1081" s="T2118">v:pred</ta>
            <ta e="T2123" id="Seg_1082" s="T2122">v:pred 0.3.h:S</ta>
            <ta e="T2124" id="Seg_1083" s="T2123">v:pred 0.2.h:S</ta>
            <ta e="T2126" id="Seg_1084" s="T2125">np:O</ta>
            <ta e="T2128" id="Seg_1085" s="T2127">v:pred 0.3.h:S</ta>
            <ta e="T2130" id="Seg_1086" s="T2129">np.h:S</ta>
            <ta e="T2132" id="Seg_1087" s="T2131">v:pred</ta>
            <ta e="T2135" id="Seg_1088" s="T2134">v:pred 0.3.h:S</ta>
            <ta e="T2136" id="Seg_1089" s="T2135">v:pred 0.1.h:S</ta>
            <ta e="T2138" id="Seg_1090" s="T2137">s:purp</ta>
            <ta e="T2140" id="Seg_1091" s="T2139">v:pred 0.3.h:S</ta>
            <ta e="T2142" id="Seg_1092" s="T2141">np:O</ta>
            <ta e="T2143" id="Seg_1093" s="T2142">np:O</ta>
            <ta e="T2144" id="Seg_1094" s="T2143">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2066" id="Seg_1095" s="T2065">RUS:cult</ta>
            <ta e="T2069" id="Seg_1096" s="T2068">RUS:cult</ta>
            <ta e="T2070" id="Seg_1097" s="T2069">RUS:gram</ta>
            <ta e="T2071" id="Seg_1098" s="T2070">RUS:gram</ta>
            <ta e="T2074" id="Seg_1099" s="T2073">TURK:cult</ta>
            <ta e="T2076" id="Seg_1100" s="T2075">TURK:cult</ta>
            <ta e="T2077" id="Seg_1101" s="T2076">RUS:cult</ta>
            <ta e="T2079" id="Seg_1102" s="T2078">RUS:mod</ta>
            <ta e="T2080" id="Seg_1103" s="T2079">RUS:gram</ta>
            <ta e="T2083" id="Seg_1104" s="T2082">TURK:cult</ta>
            <ta e="T2092" id="Seg_1105" s="T2091">TURK:core</ta>
            <ta e="T2093" id="Seg_1106" s="T2092">TURK:cult</ta>
            <ta e="T2095" id="Seg_1107" s="T2094">TURK:cult</ta>
            <ta e="T2098" id="Seg_1108" s="T2097">RUS:gram</ta>
            <ta e="T2099" id="Seg_1109" s="T2098">RUS:gram</ta>
            <ta e="T2104" id="Seg_1110" s="T2103">RUS:cult</ta>
            <ta e="T2106" id="Seg_1111" s="T2105">RUS:gram</ta>
            <ta e="T2111" id="Seg_1112" s="T2110">TURK:cult</ta>
            <ta e="T2113" id="Seg_1113" s="T2112">TURK:cult</ta>
            <ta e="T2131" id="Seg_1114" s="T2130">RUS:cult</ta>
            <ta e="T2133" id="Seg_1115" s="T2132">RUS:gram</ta>
            <ta e="T2142" id="Seg_1116" s="T2141">RUS:cult</ta>
            <ta e="T2143" id="Seg_1117" s="T2142">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T2072" id="Seg_1118" s="T2065">Петух взлетел на забор и стал кричать.</ta>
            <ta e="T2076" id="Seg_1119" s="T2072">«Я царь, я царь».</ta>
            <ta e="T2081" id="Seg_1120" s="T2076">Куры пришли и тоже закричали.</ta>
            <ta e="T2083" id="Seg_1121" s="T2081">«Ты царь!</ta>
            <ta e="T2089" id="Seg_1122" s="T2083">Ты такой яркий, у тебя голова такая красивая».</ta>
            <ta e="T2101" id="Seg_1123" s="T2089">Потом другой царь, царь людей поймал его и отрезал голову.</ta>
            <ta e="T2104" id="Seg_1124" s="T2101">И приготовил в печке.</ta>
            <ta e="T2109" id="Seg_1125" s="T2104">Съел его, а тот всё равно кричит.</ta>
            <ta e="T2113" id="Seg_1126" s="T2109">«Я царь, я царь».</ta>
            <ta e="T2119" id="Seg_1127" s="T2113">Потом утром он встал, а тот всё кричит.</ta>
            <ta e="T2123" id="Seg_1128" s="T2119">Один [раз], два, три кричит.</ta>
            <ta e="T2126" id="Seg_1129" s="T2123">«Разрежьте мне живот».</ta>
            <ta e="T2128" id="Seg_1130" s="T2126">Разрезали ему [живот].</ta>
            <ta e="T2135" id="Seg_1131" s="T2128">Петух взлетел и снова закричал.</ta>
            <ta e="T2138" id="Seg_1132" s="T2135">«Пойду и побью его».</ta>
            <ta e="T2141" id="Seg_1133" s="T2138">Потом прилетел в загон.</ta>
            <ta e="T2144" id="Seg_1134" s="T2141">Стал червей и мух ловить.</ta>
            <ta e="T2145" id="Seg_1135" s="T2144">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T2072" id="Seg_1136" s="T2065">The rooster flew to the top of the fence and started to shout.</ta>
            <ta e="T2076" id="Seg_1137" s="T2072">"I'm the chief, I'm the chief."</ta>
            <ta e="T2081" id="Seg_1138" s="T2076">The hens came, also started to shout.</ta>
            <ta e="T2083" id="Seg_1139" s="T2081">"You are the chief!</ta>
            <ta e="T2089" id="Seg_1140" s="T2083">You are very stately, your head is beautiful."</ta>
            <ta e="T2101" id="Seg_1141" s="T2089">Then another chief, the men's chief caught it and cut off its head.</ta>
            <ta e="T2104" id="Seg_1142" s="T2101">Then boiled it on the stove.</ta>
            <ta e="T2109" id="Seg_1143" s="T2104">Ate, but it is always (still) shouting.</ta>
            <ta e="T2113" id="Seg_1144" s="T2109">"I'm the chief, I'm the chief."</ta>
            <ta e="T2119" id="Seg_1145" s="T2113">Then in the morning he got up, it still shouted.</ta>
            <ta e="T2123" id="Seg_1146" s="T2119">One, two, three [times] it shouts.</ta>
            <ta e="T2126" id="Seg_1147" s="T2123">"Cut my stomach [open]."</ta>
            <ta e="T2128" id="Seg_1148" s="T2126">They cut him [the stomach].</ta>
            <ta e="T2135" id="Seg_1149" s="T2128">The rooster flew out and shouted again.</ta>
            <ta e="T2138" id="Seg_1150" s="T2135">"I will go and beat it."</ta>
            <ta e="T2141" id="Seg_1151" s="T2138">Then it came to the corral.</ta>
            <ta e="T2144" id="Seg_1152" s="T2141">It caught worms, flies.</ta>
            <ta e="T2145" id="Seg_1153" s="T2144">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T2072" id="Seg_1154" s="T2065">Der Hahn flog auf den Zaun und fing an, zu rufen.</ta>
            <ta e="T2076" id="Seg_1155" s="T2072">„Ich bin der Chef, ich bin der Chef.“</ta>
            <ta e="T2081" id="Seg_1156" s="T2076">Die Hühner kamen, fingen auch an zu rufen.</ta>
            <ta e="T2083" id="Seg_1157" s="T2081">„Du bist der Chef!</ta>
            <ta e="T2089" id="Seg_1158" s="T2083">Du bist sehr gediegen, dein Kopf ist wunderschön.“</ta>
            <ta e="T2101" id="Seg_1159" s="T2089">Dann ein anderer Chef, der Chef der Männer, fing ihn und schlug ihm den Kopf ab.</ta>
            <ta e="T2104" id="Seg_1160" s="T2101">Dann kochte ihn auf dem Herd.</ta>
            <ta e="T2109" id="Seg_1161" s="T2104">Aß, aber er ruft immer (noch) weiter.</ta>
            <ta e="T2113" id="Seg_1162" s="T2109">„Ich bin der Chef, ich bin der Chef.“</ta>
            <ta e="T2119" id="Seg_1163" s="T2113">Dann am Morgen stand er auf. Er rief immer noch.</ta>
            <ta e="T2123" id="Seg_1164" s="T2119">Ein, zwei, drei [-mal] rief er.</ta>
            <ta e="T2126" id="Seg_1165" s="T2123">„Schneid mir den Magen [auf].“</ta>
            <ta e="T2128" id="Seg_1166" s="T2126">Sie schnitten.</ta>
            <ta e="T2135" id="Seg_1167" s="T2128">Der Hahn flog hinaus und rief wieder.</ta>
            <ta e="T2138" id="Seg_1168" s="T2135">„Ich werde gehen und abhauen.“</ta>
            <ta e="T2141" id="Seg_1169" s="T2138">Dann kam er zum Zwinger.</ta>
            <ta e="T2144" id="Seg_1170" s="T2141">Er fing Würmer, Fliegen.</ta>
            <ta e="T2145" id="Seg_1171" s="T2144">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T2072" id="Seg_1172" s="T2065">[GVY:] See comment to PKZ_196X_BearWoodenFoot_flk, 14</ta>
            <ta e="T2101" id="Seg_1173" s="T2089">[KlT:] ABL? expectable would be ACC ’ulubə’.</ta>
            <ta e="T2119" id="Seg_1174" s="T2113">[GVY:] kirgarluʔpi should be kirgarlaʔpi?</ta>
            <ta e="T2138" id="Seg_1175" s="T2135">[GVY:] kalam?</ta>
            <ta e="T2144" id="Seg_1176" s="T2141">Червь ’larvae’.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T2065" />
            <conversion-tli id="T2066" />
            <conversion-tli id="T2067" />
            <conversion-tli id="T2068" />
            <conversion-tli id="T2069" />
            <conversion-tli id="T2070" />
            <conversion-tli id="T2071" />
            <conversion-tli id="T2072" />
            <conversion-tli id="T2073" />
            <conversion-tli id="T2074" />
            <conversion-tli id="T2075" />
            <conversion-tli id="T2076" />
            <conversion-tli id="T2077" />
            <conversion-tli id="T2078" />
            <conversion-tli id="T2079" />
            <conversion-tli id="T2080" />
            <conversion-tli id="T2081" />
            <conversion-tli id="T2082" />
            <conversion-tli id="T2083" />
            <conversion-tli id="T2084" />
            <conversion-tli id="T2085" />
            <conversion-tli id="T2086" />
            <conversion-tli id="T2087" />
            <conversion-tli id="T2088" />
            <conversion-tli id="T2089" />
            <conversion-tli id="T2090" />
            <conversion-tli id="T2091" />
            <conversion-tli id="T2092" />
            <conversion-tli id="T2093" />
            <conversion-tli id="T2094" />
            <conversion-tli id="T2095" />
            <conversion-tli id="T2096" />
            <conversion-tli id="T2097" />
            <conversion-tli id="T2098" />
            <conversion-tli id="T2099" />
            <conversion-tli id="T2100" />
            <conversion-tli id="T2101" />
            <conversion-tli id="T2102" />
            <conversion-tli id="T2103" />
            <conversion-tli id="T2104" />
            <conversion-tli id="T2105" />
            <conversion-tli id="T2106" />
            <conversion-tli id="T2107" />
            <conversion-tli id="T2108" />
            <conversion-tli id="T2109" />
            <conversion-tli id="T2110" />
            <conversion-tli id="T2111" />
            <conversion-tli id="T2112" />
            <conversion-tli id="T2113" />
            <conversion-tli id="T2114" />
            <conversion-tli id="T2115" />
            <conversion-tli id="T2116" />
            <conversion-tli id="T2117" />
            <conversion-tli id="T2118" />
            <conversion-tli id="T2119" />
            <conversion-tli id="T2120" />
            <conversion-tli id="T2121" />
            <conversion-tli id="T2122" />
            <conversion-tli id="T2123" />
            <conversion-tli id="T2124" />
            <conversion-tli id="T2125" />
            <conversion-tli id="T2126" />
            <conversion-tli id="T2127" />
            <conversion-tli id="T2128" />
            <conversion-tli id="T2129" />
            <conversion-tli id="T2130" />
            <conversion-tli id="T2131" />
            <conversion-tli id="T2132" />
            <conversion-tli id="T2133" />
            <conversion-tli id="T2134" />
            <conversion-tli id="T2135" />
            <conversion-tli id="T2136" />
            <conversion-tli id="T2137" />
            <conversion-tli id="T2138" />
            <conversion-tli id="T2139" />
            <conversion-tli id="T2140" />
            <conversion-tli id="T2141" />
            <conversion-tli id="T2142" />
            <conversion-tli id="T2143" />
            <conversion-tli id="T2144" />
            <conversion-tli id="T2145" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
