<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID1EA06FF6-72B7-98B2-5F53-7D93B507F047">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_ThreeBeasts2_flk.wav" />
         <referenced-file url="PKZ_196X_ThreeBeasts2_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_ThreeBeasts2_flk\PKZ_196X_ThreeBeasts2_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">84</ud-information>
            <ud-information attribute-name="# HIAT:w">55</ud-information>
            <ud-information attribute-name="# e">56</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">1</ud-information>
            <ud-information attribute-name="# HIAT:u">9</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T2648" time="0.078" type="appl" />
         <tli id="T2649" time="0.946" type="appl" />
         <tli id="T2650" time="1.815" type="appl" />
         <tli id="T2651" time="2.683" type="appl" />
         <tli id="T2652" time="3.551" type="appl" />
         <tli id="T2653" time="4.419" type="appl" />
         <tli id="T2654" time="5.288" type="appl" />
         <tli id="T2655" time="6.559311949857058" />
         <tli id="T2656" time="7.216" type="appl" />
         <tli id="T2657" time="7.928" type="appl" />
         <tli id="T2658" time="8.641" type="appl" />
         <tli id="T2659" time="11.952079599688723" />
         <tli id="T2660" time="12.84" type="appl" />
         <tli id="T2661" time="13.754" type="appl" />
         <tli id="T2662" time="14.858441398609123" />
         <tli id="T2663" time="15.635" type="appl" />
         <tli id="T2664" time="16.409" type="appl" />
         <tli id="T2665" time="17.184" type="appl" />
         <tli id="T2666" time="17.958" type="appl" />
         <tli id="T2667" time="18.918015562697484" />
         <tli id="T2668" time="19.789" type="appl" />
         <tli id="T2669" time="20.577" type="appl" />
         <tli id="T2670" time="21.364" type="appl" />
         <tli id="T2671" time="22.152" type="appl" />
         <tli id="T2672" time="22.94" type="appl" />
         <tli id="T2673" time="23.728" type="appl" />
         <tli id="T2674" time="24.515" type="appl" />
         <tli id="T2675" time="25.303" type="appl" />
         <tli id="T2676" time="26.470556659433303" />
         <tli id="T2677" time="27.034" type="appl" />
         <tli id="T2678" time="27.669" type="appl" />
         <tli id="T2679" time="28.303" type="appl" />
         <tli id="T2680" time="28.937" type="appl" />
         <tli id="T2681" time="29.571" type="appl" />
         <tli id="T2682" time="30.206" type="appl" />
         <tli id="T2683" time="30.84" type="appl" />
         <tli id="T2684" time="31.474" type="appl" />
         <tli id="T2685" time="32.108" type="appl" />
         <tli id="T2686" time="32.743" type="appl" />
         <tli id="T2687" time="33.636471645303565" />
         <tli id="T2688" time="34.36" type="appl" />
         <tli id="T2689" time="34.812" type="appl" />
         <tli id="T2690" time="35.265" type="appl" />
         <tli id="T2691" time="35.718" type="appl" />
         <tli id="T2692" time="36.171" type="appl" />
         <tli id="T2693" time="36.624" type="appl" />
         <tli id="T2694" time="37.076" type="appl" />
         <tli id="T2695" time="37.83603112539497" />
         <tli id="T2696" time="38.635" type="appl" />
         <tli id="T2697" time="39.422" type="appl" />
         <tli id="T2698" time="40.45575632488057" />
         <tli id="T2699" time="41.101" type="appl" />
         <tli id="T2700" time="41.741" type="appl" />
         <tli id="T2701" time="42.382" type="appl" />
         <tli id="T2702" time="43.022" type="appl" />
         <tli id="T2703" time="43.662" type="appl" />
         <tli id="T2704" time="44.302" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T2704" id="Seg_0" n="sc" s="T2648">
               <ts e="T2655" id="Seg_2" n="HIAT:u" s="T2648">
                  <ts e="T2649" id="Seg_4" n="HIAT:w" s="T2648">Amnobiʔi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2650" id="Seg_7" n="HIAT:w" s="T2649">nagurgöʔ</ts>
                  <nts id="Seg_8" n="HIAT:ip">,</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2651" id="Seg_11" n="HIAT:w" s="T2650">lʼisʼitsa</ts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2652" id="Seg_14" n="HIAT:w" s="T2651">i</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2653" id="Seg_17" n="HIAT:w" s="T2652">volk</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2654" id="Seg_20" n="HIAT:w" s="T2653">i</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2655" id="Seg_23" n="HIAT:w" s="T2654">urgaːba</ts>
                  <nts id="Seg_24" n="HIAT:ip">.</nts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2659" id="Seg_27" n="HIAT:u" s="T2655">
                  <ts e="T2656" id="Seg_29" n="HIAT:w" s="T2655">Kambi</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2657" id="Seg_32" n="HIAT:w" s="T2656">urgaːba</ts>
                  <nts id="Seg_33" n="HIAT:ip">,</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2658" id="Seg_36" n="HIAT:w" s="T2657">paʔi</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2659" id="Seg_39" n="HIAT:w" s="T2658">deʔpi</ts>
                  <nts id="Seg_40" n="HIAT:ip">.</nts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2662" id="Seg_43" n="HIAT:u" s="T2659">
                  <ts e="T2660" id="Seg_45" n="HIAT:w" s="T2659">Šĭsəge</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2661" id="Seg_48" n="HIAT:w" s="T2660">molaːmbi</ts>
                  <nts id="Seg_49" n="HIAT:ip">,</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2662" id="Seg_52" n="HIAT:w" s="T2661">kănnaːmbiʔi</ts>
                  <nts id="Seg_53" n="HIAT:ip">.</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2667" id="Seg_56" n="HIAT:u" s="T2662">
                  <ts e="T2663" id="Seg_58" n="HIAT:w" s="T2662">Urgaːba</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2664" id="Seg_61" n="HIAT:w" s="T2663">kambi</ts>
                  <nts id="Seg_62" n="HIAT:ip">,</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2665" id="Seg_65" n="HIAT:w" s="T2664">pa</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_67" n="HIAT:ip">(</nts>
                  <ts e="T2666" id="Seg_69" n="HIAT:w" s="T2665">dep-</ts>
                  <nts id="Seg_70" n="HIAT:ip">)</nts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2667" id="Seg_73" n="HIAT:w" s="T2666">deʔsʼittə</ts>
                  <nts id="Seg_74" n="HIAT:ip">.</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2676" id="Seg_77" n="HIAT:u" s="T2667">
                  <ts e="T2668" id="Seg_79" n="HIAT:w" s="T2667">Dĭgəttə</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2669" id="Seg_82" n="HIAT:w" s="T2668">lʼisʼitsa</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2670" id="Seg_85" n="HIAT:w" s="T2669">măndə</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2671" id="Seg_88" n="HIAT:w" s="T2670">volkdə:</ts>
                  <nts id="Seg_89" n="HIAT:ip">"</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2672" id="Seg_92" n="HIAT:w" s="T2671">Davaj</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_94" n="HIAT:ip">(</nts>
                  <ts e="T2673" id="Seg_96" n="HIAT:w" s="T2672">am-</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2674" id="Seg_99" n="HIAT:w" s="T2673">am-</ts>
                  <nts id="Seg_100" n="HIAT:ip">)</nts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2675" id="Seg_103" n="HIAT:w" s="T2674">ambibaʔ</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2676" id="Seg_106" n="HIAT:w" s="T2675">urgaːba</ts>
                  <nts id="Seg_107" n="HIAT:ip">.</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2687" id="Seg_110" n="HIAT:u" s="T2676">
                  <ts e="T2677" id="Seg_112" n="HIAT:w" s="T2676">Da</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2678" id="Seg_115" n="HIAT:w" s="T2677">tăn</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2679" id="Seg_118" n="HIAT:w" s="T2678">iʔ</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2680" id="Seg_121" n="HIAT:w" s="T2679">nörbəʔ</ts>
                  <nts id="Seg_122" n="HIAT:ip">,</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2681" id="Seg_125" n="HIAT:w" s="T2680">dĭ</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2682" id="Seg_128" n="HIAT:w" s="T2681">nünələj</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2683" id="Seg_131" n="HIAT:w" s="T2682">dăk</ts>
                  <nts id="Seg_132" n="HIAT:ip">,</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2684" id="Seg_135" n="HIAT:w" s="T2683">tănan</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2685" id="Seg_138" n="HIAT:w" s="T2684">i</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2686" id="Seg_141" n="HIAT:w" s="T2685">măna</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2687" id="Seg_144" n="HIAT:w" s="T2686">amluʔləj</ts>
                  <nts id="Seg_145" n="HIAT:ip">.</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2695" id="Seg_148" n="HIAT:u" s="T2687">
                  <ts e="T2688" id="Seg_150" n="HIAT:w" s="T2687">A</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2689" id="Seg_153" n="HIAT:w" s="T2688">tăn</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2690" id="Seg_156" n="HIAT:w" s="T2689">külalləl</ts>
                  <nts id="Seg_157" n="HIAT:ip">,</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2691" id="Seg_160" n="HIAT:w" s="T2690">a</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2692" id="Seg_163" n="HIAT:w" s="T2691">măn</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2693" id="Seg_166" n="HIAT:w" s="T2692">bünə</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2694" id="Seg_169" n="HIAT:w" s="T2693">kunnam</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2695" id="Seg_172" n="HIAT:w" s="T2694">tănan</ts>
                  <nts id="Seg_173" n="HIAT:ip">.</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2698" id="Seg_176" n="HIAT:u" s="T2695">
                  <ts e="T2696" id="Seg_178" n="HIAT:w" s="T2695">Dĭgəttə</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2697" id="Seg_181" n="HIAT:w" s="T2696">dĭ</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2698" id="Seg_184" n="HIAT:w" s="T2697">külaːmbi</ts>
                  <nts id="Seg_185" n="HIAT:ip">.</nts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T2704" id="Seg_188" n="HIAT:u" s="T2698">
                  <ts e="T2699" id="Seg_190" n="HIAT:w" s="T2698">Urgaːba</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2700" id="Seg_193" n="HIAT:w" s="T2699">šobi</ts>
                  <nts id="Seg_194" n="HIAT:ip">,</nts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2701" id="Seg_197" n="HIAT:w" s="T2700">dʼorbi</ts>
                  <nts id="Seg_198" n="HIAT:ip">,</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2702" id="Seg_201" n="HIAT:w" s="T2701">dʼorbi</ts>
                  <nts id="Seg_202" n="HIAT:ip">,</nts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2703" id="Seg_205" n="HIAT:w" s="T2702">dĭgəttə</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_207" n="HIAT:ip">(</nts>
                  <nts id="Seg_208" n="HIAT:ip">(</nts>
                  <ats e="T2704" id="Seg_209" n="HIAT:non-pho" s="T2703">BRK</ats>
                  <nts id="Seg_210" n="HIAT:ip">)</nts>
                  <nts id="Seg_211" n="HIAT:ip">)</nts>
                  <nts id="Seg_212" n="HIAT:ip">.</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T2704" id="Seg_214" n="sc" s="T2648">
               <ts e="T2649" id="Seg_216" n="e" s="T2648">Amnobiʔi </ts>
               <ts e="T2650" id="Seg_218" n="e" s="T2649">nagurgöʔ, </ts>
               <ts e="T2651" id="Seg_220" n="e" s="T2650">lʼisʼitsa </ts>
               <ts e="T2652" id="Seg_222" n="e" s="T2651">i </ts>
               <ts e="T2653" id="Seg_224" n="e" s="T2652">volk </ts>
               <ts e="T2654" id="Seg_226" n="e" s="T2653">i </ts>
               <ts e="T2655" id="Seg_228" n="e" s="T2654">urgaːba. </ts>
               <ts e="T2656" id="Seg_230" n="e" s="T2655">Kambi </ts>
               <ts e="T2657" id="Seg_232" n="e" s="T2656">urgaːba, </ts>
               <ts e="T2658" id="Seg_234" n="e" s="T2657">paʔi </ts>
               <ts e="T2659" id="Seg_236" n="e" s="T2658">deʔpi. </ts>
               <ts e="T2660" id="Seg_238" n="e" s="T2659">Šĭsəge </ts>
               <ts e="T2661" id="Seg_240" n="e" s="T2660">molaːmbi, </ts>
               <ts e="T2662" id="Seg_242" n="e" s="T2661">kănnaːmbiʔi. </ts>
               <ts e="T2663" id="Seg_244" n="e" s="T2662">Urgaːba </ts>
               <ts e="T2664" id="Seg_246" n="e" s="T2663">kambi, </ts>
               <ts e="T2665" id="Seg_248" n="e" s="T2664">pa </ts>
               <ts e="T2666" id="Seg_250" n="e" s="T2665">(dep-) </ts>
               <ts e="T2667" id="Seg_252" n="e" s="T2666">deʔsʼittə. </ts>
               <ts e="T2668" id="Seg_254" n="e" s="T2667">Dĭgəttə </ts>
               <ts e="T2669" id="Seg_256" n="e" s="T2668">lʼisʼitsa </ts>
               <ts e="T2670" id="Seg_258" n="e" s="T2669">măndə </ts>
               <ts e="T2671" id="Seg_260" n="e" s="T2670">volkdə:" </ts>
               <ts e="T2672" id="Seg_262" n="e" s="T2671">Davaj </ts>
               <ts e="T2673" id="Seg_264" n="e" s="T2672">(am- </ts>
               <ts e="T2674" id="Seg_266" n="e" s="T2673">am-) </ts>
               <ts e="T2675" id="Seg_268" n="e" s="T2674">ambibaʔ </ts>
               <ts e="T2676" id="Seg_270" n="e" s="T2675">urgaːba. </ts>
               <ts e="T2677" id="Seg_272" n="e" s="T2676">Da </ts>
               <ts e="T2678" id="Seg_274" n="e" s="T2677">tăn </ts>
               <ts e="T2679" id="Seg_276" n="e" s="T2678">iʔ </ts>
               <ts e="T2680" id="Seg_278" n="e" s="T2679">nörbəʔ, </ts>
               <ts e="T2681" id="Seg_280" n="e" s="T2680">dĭ </ts>
               <ts e="T2682" id="Seg_282" n="e" s="T2681">nünələj </ts>
               <ts e="T2683" id="Seg_284" n="e" s="T2682">dăk, </ts>
               <ts e="T2684" id="Seg_286" n="e" s="T2683">tănan </ts>
               <ts e="T2685" id="Seg_288" n="e" s="T2684">i </ts>
               <ts e="T2686" id="Seg_290" n="e" s="T2685">măna </ts>
               <ts e="T2687" id="Seg_292" n="e" s="T2686">amluʔləj. </ts>
               <ts e="T2688" id="Seg_294" n="e" s="T2687">A </ts>
               <ts e="T2689" id="Seg_296" n="e" s="T2688">tăn </ts>
               <ts e="T2690" id="Seg_298" n="e" s="T2689">külalləl, </ts>
               <ts e="T2691" id="Seg_300" n="e" s="T2690">a </ts>
               <ts e="T2692" id="Seg_302" n="e" s="T2691">măn </ts>
               <ts e="T2693" id="Seg_304" n="e" s="T2692">bünə </ts>
               <ts e="T2694" id="Seg_306" n="e" s="T2693">kunnam </ts>
               <ts e="T2695" id="Seg_308" n="e" s="T2694">tănan. </ts>
               <ts e="T2696" id="Seg_310" n="e" s="T2695">Dĭgəttə </ts>
               <ts e="T2697" id="Seg_312" n="e" s="T2696">dĭ </ts>
               <ts e="T2698" id="Seg_314" n="e" s="T2697">külaːmbi. </ts>
               <ts e="T2699" id="Seg_316" n="e" s="T2698">Urgaːba </ts>
               <ts e="T2700" id="Seg_318" n="e" s="T2699">šobi, </ts>
               <ts e="T2701" id="Seg_320" n="e" s="T2700">dʼorbi, </ts>
               <ts e="T2702" id="Seg_322" n="e" s="T2701">dʼorbi, </ts>
               <ts e="T2703" id="Seg_324" n="e" s="T2702">dĭgəttə </ts>
               <ts e="T2704" id="Seg_326" n="e" s="T2703">((BRK)). </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T2655" id="Seg_327" s="T2648">PKZ_196X_ThreeBeasts2_flk.001 (001)</ta>
            <ta e="T2659" id="Seg_328" s="T2655">PKZ_196X_ThreeBeasts2_flk.002 (002)</ta>
            <ta e="T2662" id="Seg_329" s="T2659">PKZ_196X_ThreeBeasts2_flk.003 (003)</ta>
            <ta e="T2667" id="Seg_330" s="T2662">PKZ_196X_ThreeBeasts2_flk.004 (004)</ta>
            <ta e="T2676" id="Seg_331" s="T2667">PKZ_196X_ThreeBeasts2_flk.005 (005)</ta>
            <ta e="T2687" id="Seg_332" s="T2676">PKZ_196X_ThreeBeasts2_flk.006 (006)</ta>
            <ta e="T2695" id="Seg_333" s="T2687">PKZ_196X_ThreeBeasts2_flk.007 (007)</ta>
            <ta e="T2698" id="Seg_334" s="T2695">PKZ_196X_ThreeBeasts2_flk.008 (008)</ta>
            <ta e="T2704" id="Seg_335" s="T2698">PKZ_196X_ThreeBeasts2_flk.009 (009)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T2655" id="Seg_336" s="T2648">Amnobiʔi nagurgöʔ, lʼisʼitsa i volk i urgaːba. </ta>
            <ta e="T2659" id="Seg_337" s="T2655">Kambi urgaːba, paʔi deʔpi. </ta>
            <ta e="T2662" id="Seg_338" s="T2659">Šĭsəge molaːmbi, kănnaːmbiʔi. </ta>
            <ta e="T2667" id="Seg_339" s="T2662">Urgaːba kambi, pa (dep-) deʔsʼittə. </ta>
            <ta e="T2676" id="Seg_340" s="T2667">Dĭgəttə lʼisʼitsa măndə volkdə:" Davaj (am- am-) ambibaʔ urgaːba. </ta>
            <ta e="T2687" id="Seg_341" s="T2676">Da tăn iʔ nörbəʔ, dĭ nünələj dăk, tănan i măna amluʔləj. </ta>
            <ta e="T2695" id="Seg_342" s="T2687">A tăn külalləl, a măn bünə kunnam tănan. </ta>
            <ta e="T2698" id="Seg_343" s="T2695">Dĭgəttə dĭ külaːmbi. </ta>
            <ta e="T2704" id="Seg_344" s="T2698">Urgaːba šobi, dʼorbi, dʼorbi, dĭgəttə… ((BRK)). </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2649" id="Seg_345" s="T2648">amno-bi-ʔi</ta>
            <ta e="T2650" id="Seg_346" s="T2649">nagur-göʔ</ta>
            <ta e="T2651" id="Seg_347" s="T2650">lʼisʼitsa</ta>
            <ta e="T2652" id="Seg_348" s="T2651">i</ta>
            <ta e="T2653" id="Seg_349" s="T2652">volk</ta>
            <ta e="T2654" id="Seg_350" s="T2653">i</ta>
            <ta e="T2655" id="Seg_351" s="T2654">urgaːba</ta>
            <ta e="T2656" id="Seg_352" s="T2655">kam-bi</ta>
            <ta e="T2657" id="Seg_353" s="T2656">urgaːba</ta>
            <ta e="T2658" id="Seg_354" s="T2657">pa-ʔi</ta>
            <ta e="T2659" id="Seg_355" s="T2658">deʔ-pi</ta>
            <ta e="T2660" id="Seg_356" s="T2659">šĭsəge</ta>
            <ta e="T2661" id="Seg_357" s="T2660">mo-laːm-bi</ta>
            <ta e="T2662" id="Seg_358" s="T2661">kăn-naːm-bi-ʔi</ta>
            <ta e="T2663" id="Seg_359" s="T2662">urgaːba</ta>
            <ta e="T2664" id="Seg_360" s="T2663">kam-bi</ta>
            <ta e="T2665" id="Seg_361" s="T2664">pa</ta>
            <ta e="T2667" id="Seg_362" s="T2666">deʔ-sʼittə</ta>
            <ta e="T2668" id="Seg_363" s="T2667">dĭgəttə</ta>
            <ta e="T2669" id="Seg_364" s="T2668">lʼisʼitsa</ta>
            <ta e="T2670" id="Seg_365" s="T2669">măn-də</ta>
            <ta e="T2671" id="Seg_366" s="T2670">volk-də</ta>
            <ta e="T2672" id="Seg_367" s="T2671">davaj</ta>
            <ta e="T2675" id="Seg_368" s="T2674">am-bi-baʔ</ta>
            <ta e="T2676" id="Seg_369" s="T2675">urgaːba</ta>
            <ta e="T2677" id="Seg_370" s="T2676">da</ta>
            <ta e="T2678" id="Seg_371" s="T2677">tăn</ta>
            <ta e="T2679" id="Seg_372" s="T2678">i-ʔ</ta>
            <ta e="T2680" id="Seg_373" s="T2679">nörbə-ʔ</ta>
            <ta e="T2681" id="Seg_374" s="T2680">dĭ</ta>
            <ta e="T2682" id="Seg_375" s="T2681">nünə-lə-j</ta>
            <ta e="T2683" id="Seg_376" s="T2682">dăk</ta>
            <ta e="T2684" id="Seg_377" s="T2683">tănan</ta>
            <ta e="T2685" id="Seg_378" s="T2684">i</ta>
            <ta e="T2686" id="Seg_379" s="T2685">măna</ta>
            <ta e="T2687" id="Seg_380" s="T2686">am-luʔ-lə-j</ta>
            <ta e="T2688" id="Seg_381" s="T2687">a</ta>
            <ta e="T2689" id="Seg_382" s="T2688">tăn</ta>
            <ta e="T2690" id="Seg_383" s="T2689">kü-lal-lə-l</ta>
            <ta e="T2691" id="Seg_384" s="T2690">a</ta>
            <ta e="T2692" id="Seg_385" s="T2691">măn</ta>
            <ta e="T2693" id="Seg_386" s="T2692">bü-nə</ta>
            <ta e="T2694" id="Seg_387" s="T2693">kun-na-m</ta>
            <ta e="T2695" id="Seg_388" s="T2694">tănan</ta>
            <ta e="T2696" id="Seg_389" s="T2695">dĭgəttə</ta>
            <ta e="T2697" id="Seg_390" s="T2696">dĭ</ta>
            <ta e="T2698" id="Seg_391" s="T2697">kü-laːm-bi</ta>
            <ta e="T2699" id="Seg_392" s="T2698">urgaːba</ta>
            <ta e="T2700" id="Seg_393" s="T2699">šo-bi</ta>
            <ta e="T2701" id="Seg_394" s="T2700">dʼor-bi</ta>
            <ta e="T2702" id="Seg_395" s="T2701">dʼor-bi</ta>
            <ta e="T2703" id="Seg_396" s="T2702">dĭgəttə</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2649" id="Seg_397" s="T2648">amno-bi-jəʔ</ta>
            <ta e="T2650" id="Seg_398" s="T2649">nagur-göʔ</ta>
            <ta e="T2651" id="Seg_399" s="T2650">lʼisʼitsa</ta>
            <ta e="T2652" id="Seg_400" s="T2651">i</ta>
            <ta e="T2653" id="Seg_401" s="T2652">volk</ta>
            <ta e="T2654" id="Seg_402" s="T2653">i</ta>
            <ta e="T2655" id="Seg_403" s="T2654">urgaːba</ta>
            <ta e="T2656" id="Seg_404" s="T2655">kan-bi</ta>
            <ta e="T2657" id="Seg_405" s="T2656">urgaːba</ta>
            <ta e="T2658" id="Seg_406" s="T2657">pa-jəʔ</ta>
            <ta e="T2659" id="Seg_407" s="T2658">det-bi</ta>
            <ta e="T2660" id="Seg_408" s="T2659">šišəge</ta>
            <ta e="T2661" id="Seg_409" s="T2660">mo-laːm-bi</ta>
            <ta e="T2662" id="Seg_410" s="T2661">kănzə-laːm-bi-jəʔ</ta>
            <ta e="T2663" id="Seg_411" s="T2662">urgaːba</ta>
            <ta e="T2664" id="Seg_412" s="T2663">kan-bi</ta>
            <ta e="T2665" id="Seg_413" s="T2664">pa</ta>
            <ta e="T2667" id="Seg_414" s="T2666">det-zittə</ta>
            <ta e="T2668" id="Seg_415" s="T2667">dĭgəttə</ta>
            <ta e="T2669" id="Seg_416" s="T2668">lʼisʼitsa</ta>
            <ta e="T2670" id="Seg_417" s="T2669">măn-ntə</ta>
            <ta e="T2671" id="Seg_418" s="T2670">volk-Tə</ta>
            <ta e="T2672" id="Seg_419" s="T2671">davaj</ta>
            <ta e="T2675" id="Seg_420" s="T2674">am-bi-bAʔ</ta>
            <ta e="T2676" id="Seg_421" s="T2675">urgaːba</ta>
            <ta e="T2677" id="Seg_422" s="T2676">da</ta>
            <ta e="T2678" id="Seg_423" s="T2677">tăn</ta>
            <ta e="T2679" id="Seg_424" s="T2678">e-ʔ</ta>
            <ta e="T2680" id="Seg_425" s="T2679">nörbə-ʔ</ta>
            <ta e="T2681" id="Seg_426" s="T2680">dĭ</ta>
            <ta e="T2682" id="Seg_427" s="T2681">nünə-lV-j</ta>
            <ta e="T2683" id="Seg_428" s="T2682">tak</ta>
            <ta e="T2684" id="Seg_429" s="T2683">tănan</ta>
            <ta e="T2685" id="Seg_430" s="T2684">i</ta>
            <ta e="T2686" id="Seg_431" s="T2685">măna</ta>
            <ta e="T2687" id="Seg_432" s="T2686">am-luʔbdə-lV-j</ta>
            <ta e="T2688" id="Seg_433" s="T2687">a</ta>
            <ta e="T2689" id="Seg_434" s="T2688">tăn</ta>
            <ta e="T2690" id="Seg_435" s="T2689">kü-laːm-lV-l</ta>
            <ta e="T2691" id="Seg_436" s="T2690">a</ta>
            <ta e="T2692" id="Seg_437" s="T2691">măn</ta>
            <ta e="T2693" id="Seg_438" s="T2692">bü-Tə</ta>
            <ta e="T2694" id="Seg_439" s="T2693">kun-lV-m</ta>
            <ta e="T2695" id="Seg_440" s="T2694">tănan</ta>
            <ta e="T2696" id="Seg_441" s="T2695">dĭgəttə</ta>
            <ta e="T2697" id="Seg_442" s="T2696">dĭ</ta>
            <ta e="T2698" id="Seg_443" s="T2697">kü-laːm-bi</ta>
            <ta e="T2699" id="Seg_444" s="T2698">urgaːba</ta>
            <ta e="T2700" id="Seg_445" s="T2699">šo-bi</ta>
            <ta e="T2701" id="Seg_446" s="T2700">tʼor-bi</ta>
            <ta e="T2702" id="Seg_447" s="T2701">tʼor-bi</ta>
            <ta e="T2703" id="Seg_448" s="T2702">dĭgəttə</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2649" id="Seg_449" s="T2648">live-PST-3PL</ta>
            <ta e="T2650" id="Seg_450" s="T2649">three-COLL</ta>
            <ta e="T2651" id="Seg_451" s="T2650">fox.[NOM.SG]</ta>
            <ta e="T2652" id="Seg_452" s="T2651">and</ta>
            <ta e="T2653" id="Seg_453" s="T2652">wolf.[NOM.SG]</ta>
            <ta e="T2654" id="Seg_454" s="T2653">and</ta>
            <ta e="T2655" id="Seg_455" s="T2654">bear.[NOM.SG]</ta>
            <ta e="T2656" id="Seg_456" s="T2655">go-PST.[3SG]</ta>
            <ta e="T2657" id="Seg_457" s="T2656">bear.[NOM.SG]</ta>
            <ta e="T2658" id="Seg_458" s="T2657">tree-PL</ta>
            <ta e="T2659" id="Seg_459" s="T2658">bring-PST.[3SG]</ta>
            <ta e="T2660" id="Seg_460" s="T2659">cold.[NOM.SG]</ta>
            <ta e="T2661" id="Seg_461" s="T2660">become-RES-PST.[3SG]</ta>
            <ta e="T2662" id="Seg_462" s="T2661">freeze-RES-PST-3PL</ta>
            <ta e="T2663" id="Seg_463" s="T2662">bear.[NOM.SG]</ta>
            <ta e="T2664" id="Seg_464" s="T2663">go-PST.[3SG]</ta>
            <ta e="T2665" id="Seg_465" s="T2664">tree.[NOM.SG]</ta>
            <ta e="T2667" id="Seg_466" s="T2666">bring-INF.LAT</ta>
            <ta e="T2668" id="Seg_467" s="T2667">then</ta>
            <ta e="T2669" id="Seg_468" s="T2668">fox.[NOM.SG]</ta>
            <ta e="T2670" id="Seg_469" s="T2669">say-IPFVZ.[3SG]</ta>
            <ta e="T2671" id="Seg_470" s="T2670">wolf-LAT</ta>
            <ta e="T2672" id="Seg_471" s="T2671">HORT</ta>
            <ta e="T2675" id="Seg_472" s="T2674">eat-PST-1PL</ta>
            <ta e="T2676" id="Seg_473" s="T2675">bear.[NOM.SG]</ta>
            <ta e="T2677" id="Seg_474" s="T2676">and</ta>
            <ta e="T2678" id="Seg_475" s="T2677">you.NOM</ta>
            <ta e="T2679" id="Seg_476" s="T2678">NEG.AUX-IMP.2SG</ta>
            <ta e="T2680" id="Seg_477" s="T2679">tell-CNG</ta>
            <ta e="T2681" id="Seg_478" s="T2680">this.[NOM.SG]</ta>
            <ta e="T2682" id="Seg_479" s="T2681">hear-FUT-3SG</ta>
            <ta e="T2683" id="Seg_480" s="T2682">so</ta>
            <ta e="T2684" id="Seg_481" s="T2683">you.ACC</ta>
            <ta e="T2685" id="Seg_482" s="T2684">and</ta>
            <ta e="T2686" id="Seg_483" s="T2685">I.ACC</ta>
            <ta e="T2687" id="Seg_484" s="T2686">eat-MOM-FUT-3SG</ta>
            <ta e="T2688" id="Seg_485" s="T2687">and</ta>
            <ta e="T2689" id="Seg_486" s="T2688">you.NOM</ta>
            <ta e="T2690" id="Seg_487" s="T2689">die-RES-FUT-2SG</ta>
            <ta e="T2691" id="Seg_488" s="T2690">and</ta>
            <ta e="T2692" id="Seg_489" s="T2691">I.NOM</ta>
            <ta e="T2693" id="Seg_490" s="T2692">water-LAT</ta>
            <ta e="T2694" id="Seg_491" s="T2693">bring-FUT-1SG</ta>
            <ta e="T2695" id="Seg_492" s="T2694">you.ACC</ta>
            <ta e="T2696" id="Seg_493" s="T2695">then</ta>
            <ta e="T2697" id="Seg_494" s="T2696">this.[NOM.SG]</ta>
            <ta e="T2698" id="Seg_495" s="T2697">die-RES-PST.[3SG]</ta>
            <ta e="T2699" id="Seg_496" s="T2698">bear.[NOM.SG]</ta>
            <ta e="T2700" id="Seg_497" s="T2699">come-PST.[3SG]</ta>
            <ta e="T2701" id="Seg_498" s="T2700">cry-PST.[3SG]</ta>
            <ta e="T2702" id="Seg_499" s="T2701">cry-PST.[3SG]</ta>
            <ta e="T2703" id="Seg_500" s="T2702">then</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2649" id="Seg_501" s="T2648">жить-PST-3PL</ta>
            <ta e="T2650" id="Seg_502" s="T2649">три-COLL</ta>
            <ta e="T2651" id="Seg_503" s="T2650">лисица.[NOM.SG]</ta>
            <ta e="T2652" id="Seg_504" s="T2651">и</ta>
            <ta e="T2653" id="Seg_505" s="T2652">волк.[NOM.SG]</ta>
            <ta e="T2654" id="Seg_506" s="T2653">и</ta>
            <ta e="T2655" id="Seg_507" s="T2654">медведь.[NOM.SG]</ta>
            <ta e="T2656" id="Seg_508" s="T2655">пойти-PST.[3SG]</ta>
            <ta e="T2657" id="Seg_509" s="T2656">медведь.[NOM.SG]</ta>
            <ta e="T2658" id="Seg_510" s="T2657">дерево-PL</ta>
            <ta e="T2659" id="Seg_511" s="T2658">принести-PST.[3SG]</ta>
            <ta e="T2660" id="Seg_512" s="T2659">холодный.[NOM.SG]</ta>
            <ta e="T2661" id="Seg_513" s="T2660">стать-RES-PST.[3SG]</ta>
            <ta e="T2662" id="Seg_514" s="T2661">замерзнуть-RES-PST-3PL</ta>
            <ta e="T2663" id="Seg_515" s="T2662">медведь.[NOM.SG]</ta>
            <ta e="T2664" id="Seg_516" s="T2663">пойти-PST.[3SG]</ta>
            <ta e="T2665" id="Seg_517" s="T2664">дерево.[NOM.SG]</ta>
            <ta e="T2667" id="Seg_518" s="T2666">принести-INF.LAT</ta>
            <ta e="T2668" id="Seg_519" s="T2667">тогда</ta>
            <ta e="T2669" id="Seg_520" s="T2668">лисица.[NOM.SG]</ta>
            <ta e="T2670" id="Seg_521" s="T2669">сказать-IPFVZ.[3SG]</ta>
            <ta e="T2671" id="Seg_522" s="T2670">волк-LAT</ta>
            <ta e="T2672" id="Seg_523" s="T2671">HORT</ta>
            <ta e="T2675" id="Seg_524" s="T2674">съесть-PST-1PL</ta>
            <ta e="T2676" id="Seg_525" s="T2675">медведь.[NOM.SG]</ta>
            <ta e="T2677" id="Seg_526" s="T2676">и</ta>
            <ta e="T2678" id="Seg_527" s="T2677">ты.NOM</ta>
            <ta e="T2679" id="Seg_528" s="T2678">NEG.AUX-IMP.2SG</ta>
            <ta e="T2680" id="Seg_529" s="T2679">сказать-CNG</ta>
            <ta e="T2681" id="Seg_530" s="T2680">этот.[NOM.SG]</ta>
            <ta e="T2682" id="Seg_531" s="T2681">слышать-FUT-3SG</ta>
            <ta e="T2683" id="Seg_532" s="T2682">так</ta>
            <ta e="T2684" id="Seg_533" s="T2683">ты.ACC</ta>
            <ta e="T2685" id="Seg_534" s="T2684">и</ta>
            <ta e="T2686" id="Seg_535" s="T2685">я.ACC</ta>
            <ta e="T2687" id="Seg_536" s="T2686">съесть-MOM-FUT-3SG</ta>
            <ta e="T2688" id="Seg_537" s="T2687">а</ta>
            <ta e="T2689" id="Seg_538" s="T2688">ты.NOM</ta>
            <ta e="T2690" id="Seg_539" s="T2689">умереть-RES-FUT-2SG</ta>
            <ta e="T2691" id="Seg_540" s="T2690">а</ta>
            <ta e="T2692" id="Seg_541" s="T2691">я.NOM</ta>
            <ta e="T2693" id="Seg_542" s="T2692">вода-LAT</ta>
            <ta e="T2694" id="Seg_543" s="T2693">нести-FUT-1SG</ta>
            <ta e="T2695" id="Seg_544" s="T2694">ты.ACC</ta>
            <ta e="T2696" id="Seg_545" s="T2695">тогда</ta>
            <ta e="T2697" id="Seg_546" s="T2696">этот.[NOM.SG]</ta>
            <ta e="T2698" id="Seg_547" s="T2697">умереть-RES-PST.[3SG]</ta>
            <ta e="T2699" id="Seg_548" s="T2698">медведь.[NOM.SG]</ta>
            <ta e="T2700" id="Seg_549" s="T2699">прийти-PST.[3SG]</ta>
            <ta e="T2701" id="Seg_550" s="T2700">плакать-PST.[3SG]</ta>
            <ta e="T2702" id="Seg_551" s="T2701">плакать-PST.[3SG]</ta>
            <ta e="T2703" id="Seg_552" s="T2702">тогда</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2649" id="Seg_553" s="T2648">v-v:tense-v:pn</ta>
            <ta e="T2650" id="Seg_554" s="T2649">num-num&gt;num</ta>
            <ta e="T2651" id="Seg_555" s="T2650">n-n:case</ta>
            <ta e="T2652" id="Seg_556" s="T2651">conj</ta>
            <ta e="T2653" id="Seg_557" s="T2652">n-n:case</ta>
            <ta e="T2654" id="Seg_558" s="T2653">conj</ta>
            <ta e="T2655" id="Seg_559" s="T2654">n-n:case</ta>
            <ta e="T2656" id="Seg_560" s="T2655">v-v:tense-v:pn</ta>
            <ta e="T2657" id="Seg_561" s="T2656">n-n:case</ta>
            <ta e="T2658" id="Seg_562" s="T2657">n-n:num</ta>
            <ta e="T2659" id="Seg_563" s="T2658">v-v:tense-v:pn</ta>
            <ta e="T2660" id="Seg_564" s="T2659">adj-n:case</ta>
            <ta e="T2661" id="Seg_565" s="T2660">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2662" id="Seg_566" s="T2661">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2663" id="Seg_567" s="T2662">n-n:case</ta>
            <ta e="T2664" id="Seg_568" s="T2663">v-v:tense-v:pn</ta>
            <ta e="T2665" id="Seg_569" s="T2664">n-n:case</ta>
            <ta e="T2667" id="Seg_570" s="T2666">v-v:n.fin</ta>
            <ta e="T2668" id="Seg_571" s="T2667">adv</ta>
            <ta e="T2669" id="Seg_572" s="T2668">n-n:case</ta>
            <ta e="T2670" id="Seg_573" s="T2669">v-v&gt;v-v:pn</ta>
            <ta e="T2671" id="Seg_574" s="T2670">n-n:case</ta>
            <ta e="T2672" id="Seg_575" s="T2671">ptcl</ta>
            <ta e="T2675" id="Seg_576" s="T2674">v-v:tense-v:pn</ta>
            <ta e="T2676" id="Seg_577" s="T2675">n-n:case</ta>
            <ta e="T2677" id="Seg_578" s="T2676">conj</ta>
            <ta e="T2678" id="Seg_579" s="T2677">pers</ta>
            <ta e="T2679" id="Seg_580" s="T2678">aux-v:mood.pn</ta>
            <ta e="T2680" id="Seg_581" s="T2679">v-v:mood.pn</ta>
            <ta e="T2681" id="Seg_582" s="T2680">dempro-n:case</ta>
            <ta e="T2682" id="Seg_583" s="T2681">v-v:tense-v:pn</ta>
            <ta e="T2683" id="Seg_584" s="T2682">ptcl</ta>
            <ta e="T2684" id="Seg_585" s="T2683">pers</ta>
            <ta e="T2685" id="Seg_586" s="T2684">conj</ta>
            <ta e="T2686" id="Seg_587" s="T2685">pers</ta>
            <ta e="T2687" id="Seg_588" s="T2686">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2688" id="Seg_589" s="T2687">conj</ta>
            <ta e="T2689" id="Seg_590" s="T2688">pers</ta>
            <ta e="T2690" id="Seg_591" s="T2689">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2691" id="Seg_592" s="T2690">conj</ta>
            <ta e="T2692" id="Seg_593" s="T2691">pers</ta>
            <ta e="T2693" id="Seg_594" s="T2692">n-n:case</ta>
            <ta e="T2694" id="Seg_595" s="T2693">v-v:tense-v:pn</ta>
            <ta e="T2695" id="Seg_596" s="T2694">pers</ta>
            <ta e="T2696" id="Seg_597" s="T2695">adv</ta>
            <ta e="T2697" id="Seg_598" s="T2696">dempro-n:case</ta>
            <ta e="T2698" id="Seg_599" s="T2697">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2699" id="Seg_600" s="T2698">n-n:case</ta>
            <ta e="T2700" id="Seg_601" s="T2699">v-v:tense-v:pn</ta>
            <ta e="T2701" id="Seg_602" s="T2700">v-v:tense-v:pn</ta>
            <ta e="T2702" id="Seg_603" s="T2701">v-v:tense-v:pn</ta>
            <ta e="T2703" id="Seg_604" s="T2702">adv</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2649" id="Seg_605" s="T2648">v</ta>
            <ta e="T2650" id="Seg_606" s="T2649">num</ta>
            <ta e="T2651" id="Seg_607" s="T2650">n</ta>
            <ta e="T2652" id="Seg_608" s="T2651">conj</ta>
            <ta e="T2653" id="Seg_609" s="T2652">n</ta>
            <ta e="T2654" id="Seg_610" s="T2653">conj</ta>
            <ta e="T2655" id="Seg_611" s="T2654">n</ta>
            <ta e="T2656" id="Seg_612" s="T2655">v</ta>
            <ta e="T2657" id="Seg_613" s="T2656">n</ta>
            <ta e="T2658" id="Seg_614" s="T2657">n</ta>
            <ta e="T2659" id="Seg_615" s="T2658">v</ta>
            <ta e="T2660" id="Seg_616" s="T2659">adj</ta>
            <ta e="T2661" id="Seg_617" s="T2660">v</ta>
            <ta e="T2662" id="Seg_618" s="T2661">v</ta>
            <ta e="T2663" id="Seg_619" s="T2662">n</ta>
            <ta e="T2664" id="Seg_620" s="T2663">v</ta>
            <ta e="T2665" id="Seg_621" s="T2664">n</ta>
            <ta e="T2667" id="Seg_622" s="T2666">v</ta>
            <ta e="T2668" id="Seg_623" s="T2667">adv</ta>
            <ta e="T2669" id="Seg_624" s="T2668">n</ta>
            <ta e="T2670" id="Seg_625" s="T2669">v</ta>
            <ta e="T2671" id="Seg_626" s="T2670">n</ta>
            <ta e="T2672" id="Seg_627" s="T2671">ptcl</ta>
            <ta e="T2675" id="Seg_628" s="T2674">v</ta>
            <ta e="T2676" id="Seg_629" s="T2675">n</ta>
            <ta e="T2677" id="Seg_630" s="T2676">conj</ta>
            <ta e="T2678" id="Seg_631" s="T2677">pers</ta>
            <ta e="T2679" id="Seg_632" s="T2678">aux</ta>
            <ta e="T2680" id="Seg_633" s="T2679">v</ta>
            <ta e="T2681" id="Seg_634" s="T2680">dempro</ta>
            <ta e="T2682" id="Seg_635" s="T2681">v</ta>
            <ta e="T2683" id="Seg_636" s="T2682">ptcl</ta>
            <ta e="T2684" id="Seg_637" s="T2683">pers</ta>
            <ta e="T2685" id="Seg_638" s="T2684">conj</ta>
            <ta e="T2686" id="Seg_639" s="T2685">pers</ta>
            <ta e="T2687" id="Seg_640" s="T2686">v</ta>
            <ta e="T2688" id="Seg_641" s="T2687">conj</ta>
            <ta e="T2689" id="Seg_642" s="T2688">pers</ta>
            <ta e="T2690" id="Seg_643" s="T2689">v</ta>
            <ta e="T2691" id="Seg_644" s="T2690">conj</ta>
            <ta e="T2692" id="Seg_645" s="T2691">pers</ta>
            <ta e="T2693" id="Seg_646" s="T2692">n</ta>
            <ta e="T2694" id="Seg_647" s="T2693">v</ta>
            <ta e="T2695" id="Seg_648" s="T2694">pers</ta>
            <ta e="T2696" id="Seg_649" s="T2695">adv</ta>
            <ta e="T2697" id="Seg_650" s="T2696">dempro</ta>
            <ta e="T2698" id="Seg_651" s="T2697">v</ta>
            <ta e="T2699" id="Seg_652" s="T2698">n</ta>
            <ta e="T2700" id="Seg_653" s="T2699">v</ta>
            <ta e="T2701" id="Seg_654" s="T2700">v</ta>
            <ta e="T2702" id="Seg_655" s="T2701">v</ta>
            <ta e="T2703" id="Seg_656" s="T2702">adv</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2651" id="Seg_657" s="T2650">np.h:E</ta>
            <ta e="T2653" id="Seg_658" s="T2652">np.h:E</ta>
            <ta e="T2655" id="Seg_659" s="T2654">np.h:E</ta>
            <ta e="T2657" id="Seg_660" s="T2656">np.h:A</ta>
            <ta e="T2658" id="Seg_661" s="T2657">np:Th</ta>
            <ta e="T2662" id="Seg_662" s="T2661">0.3.h:E</ta>
            <ta e="T2663" id="Seg_663" s="T2662">np.h:A</ta>
            <ta e="T2665" id="Seg_664" s="T2664">np:Th</ta>
            <ta e="T2669" id="Seg_665" s="T2668">np.h:A</ta>
            <ta e="T2671" id="Seg_666" s="T2670">np.h:R</ta>
            <ta e="T2675" id="Seg_667" s="T2674">0.1.h:A</ta>
            <ta e="T2676" id="Seg_668" s="T2675">np.h:P</ta>
            <ta e="T2678" id="Seg_669" s="T2677">pro.h:A</ta>
            <ta e="T2679" id="Seg_670" s="T2678">0.2.h:A</ta>
            <ta e="T2681" id="Seg_671" s="T2680">pro.h:E</ta>
            <ta e="T2684" id="Seg_672" s="T2683">pro.h:P</ta>
            <ta e="T2686" id="Seg_673" s="T2685">pro.h:P</ta>
            <ta e="T2687" id="Seg_674" s="T2686">0.3.h:A</ta>
            <ta e="T2689" id="Seg_675" s="T2688">pro.h:P</ta>
            <ta e="T2692" id="Seg_676" s="T2691">pro.h:A</ta>
            <ta e="T2693" id="Seg_677" s="T2692">np:G</ta>
            <ta e="T2695" id="Seg_678" s="T2694">pro.h:Th</ta>
            <ta e="T2696" id="Seg_679" s="T2695">adv:Time</ta>
            <ta e="T2697" id="Seg_680" s="T2696">pro.h:P</ta>
            <ta e="T2699" id="Seg_681" s="T2698">np.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2649" id="Seg_682" s="T2648">v:pred</ta>
            <ta e="T2651" id="Seg_683" s="T2650">np.h:S</ta>
            <ta e="T2653" id="Seg_684" s="T2652">np.h:S</ta>
            <ta e="T2655" id="Seg_685" s="T2654">np.h:S</ta>
            <ta e="T2656" id="Seg_686" s="T2655">v:pred</ta>
            <ta e="T2657" id="Seg_687" s="T2656">np.h:S</ta>
            <ta e="T2658" id="Seg_688" s="T2657">np:O</ta>
            <ta e="T2659" id="Seg_689" s="T2658">v:pred</ta>
            <ta e="T2660" id="Seg_690" s="T2659">adj:pred</ta>
            <ta e="T2661" id="Seg_691" s="T2660">cop</ta>
            <ta e="T2662" id="Seg_692" s="T2661">v:pred 0.3.h:S</ta>
            <ta e="T2663" id="Seg_693" s="T2662">np.h:S</ta>
            <ta e="T2664" id="Seg_694" s="T2663">v:pred</ta>
            <ta e="T2665" id="Seg_695" s="T2664">np:O</ta>
            <ta e="T2667" id="Seg_696" s="T2666">s:purp</ta>
            <ta e="T2669" id="Seg_697" s="T2668">np.h:S</ta>
            <ta e="T2670" id="Seg_698" s="T2669">v:pred</ta>
            <ta e="T2672" id="Seg_699" s="T2671">ptcl:pred</ta>
            <ta e="T2675" id="Seg_700" s="T2674">v:pred 0.1.h:S</ta>
            <ta e="T2676" id="Seg_701" s="T2675">np.h:O</ta>
            <ta e="T2678" id="Seg_702" s="T2677">pro.h:S</ta>
            <ta e="T2679" id="Seg_703" s="T2678">v:pred 0.2.h:S</ta>
            <ta e="T2681" id="Seg_704" s="T2680">pro.h:S</ta>
            <ta e="T2682" id="Seg_705" s="T2681">v:pred</ta>
            <ta e="T2684" id="Seg_706" s="T2683">pro.h:O</ta>
            <ta e="T2686" id="Seg_707" s="T2685">pro.h:O</ta>
            <ta e="T2687" id="Seg_708" s="T2686">v:pred 0.3.h:S</ta>
            <ta e="T2689" id="Seg_709" s="T2688">pro.h:S</ta>
            <ta e="T2690" id="Seg_710" s="T2689">v:pred</ta>
            <ta e="T2692" id="Seg_711" s="T2691">pro.h:S</ta>
            <ta e="T2694" id="Seg_712" s="T2693">v:pred</ta>
            <ta e="T2695" id="Seg_713" s="T2694">pro.h:O</ta>
            <ta e="T2697" id="Seg_714" s="T2696">pro.h:S</ta>
            <ta e="T2698" id="Seg_715" s="T2697">v:pred</ta>
            <ta e="T2699" id="Seg_716" s="T2698">np.h:S</ta>
            <ta e="T2700" id="Seg_717" s="T2699">v:pred</ta>
            <ta e="T2701" id="Seg_718" s="T2700">v:pred</ta>
            <ta e="T2702" id="Seg_719" s="T2701">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2651" id="Seg_720" s="T2650">RUS:cult</ta>
            <ta e="T2652" id="Seg_721" s="T2651">RUS:gram</ta>
            <ta e="T2653" id="Seg_722" s="T2652">RUS:cult</ta>
            <ta e="T2654" id="Seg_723" s="T2653">RUS:gram</ta>
            <ta e="T2669" id="Seg_724" s="T2668">RUS:cult</ta>
            <ta e="T2671" id="Seg_725" s="T2670">RUS:cult</ta>
            <ta e="T2672" id="Seg_726" s="T2671">RUS:gram</ta>
            <ta e="T2677" id="Seg_727" s="T2676">RUS:gram</ta>
            <ta e="T2683" id="Seg_728" s="T2682">RUS:gram</ta>
            <ta e="T2685" id="Seg_729" s="T2684">RUS:gram</ta>
            <ta e="T2688" id="Seg_730" s="T2687">RUS:gram</ta>
            <ta e="T2691" id="Seg_731" s="T2690">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T2655" id="Seg_732" s="T2648">Жили трое, лисица и волк и медведь.</ta>
            <ta e="T2659" id="Seg_733" s="T2655">Пошёл медведь, принёс дрова.</ta>
            <ta e="T2662" id="Seg_734" s="T2659">Стало холодно, они замёрзли.</ta>
            <ta e="T2667" id="Seg_735" s="T2662">Медведь пошёл за дровами.</ta>
            <ta e="T2676" id="Seg_736" s="T2667">Тогда лисица говорит волку: «Давай съедим медведя».</ta>
            <ta e="T2687" id="Seg_737" s="T2676">«Да ты не говори, а то он услышит и съест и тебя, и меня».</ta>
            <ta e="T2695" id="Seg_738" s="T2687">«А ты притворись, что умер, а я тебя на реку отнесу».</ta>
            <ta e="T2698" id="Seg_739" s="T2695">Потом он [притворился, что] умер.</ta>
            <ta e="T2704" id="Seg_740" s="T2698">Пришёл медведь, плакал, плакал, потом…</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T2655" id="Seg_741" s="T2648">Together [there] lived three [of them]: a fox and a wolf and a bear.</ta>
            <ta e="T2659" id="Seg_742" s="T2655">The bear went, brought wood.</ta>
            <ta e="T2662" id="Seg_743" s="T2659">It became cold, they were freezing.</ta>
            <ta e="T2667" id="Seg_744" s="T2662">The bear went to bring wood.</ta>
            <ta e="T2676" id="Seg_745" s="T2667">Then the fox told the wolf: “Let’s eat the bear.”</ta>
            <ta e="T2687" id="Seg_746" s="T2676">“Don’t you say [so], [otherwise] he will hear, and will eat you and me.”</ta>
            <ta e="T2695" id="Seg_747" s="T2687">“But you will [pretend to] die, and I will take you to the river.”</ta>
            <ta e="T2698" id="Seg_748" s="T2695">Then it [pretended to] die.</ta>
            <ta e="T2704" id="Seg_749" s="T2698">The bear came, cried, cried, then…</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T2655" id="Seg_750" s="T2648">[Es] lebten zusammen drei [von ihnen]: ein Fuchs und ein Wolf und ein Bär.</ta>
            <ta e="T2659" id="Seg_751" s="T2655">Der Bär ging, holte Holz.</ta>
            <ta e="T2662" id="Seg_752" s="T2659">Es wurde kalt, sie froren.</ta>
            <ta e="T2667" id="Seg_753" s="T2662">Der Bär ging Holz holen.</ta>
            <ta e="T2676" id="Seg_754" s="T2667">Dann erzählte de Fuchs dem Wolf: „Fressen wir den Bär.“</ta>
            <ta e="T2687" id="Seg_755" s="T2676">„Und du erzählst nicht, er wird es hören, und wird dich und mich fressen.“</ta>
            <ta e="T2695" id="Seg_756" s="T2687">„Doch du [machst so, als ob du] stirbst, und ich werde dich zum Fluss bringen.“</ta>
            <ta e="T2698" id="Seg_757" s="T2695">Dann [machte er so, als ob] er stirbt </ta>
            <ta e="T2704" id="Seg_758" s="T2698">Der Bär kam, weinte, weinte, dann…</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T2655" id="Seg_759" s="T2648">[AAV] See https://www.calameo.com/read/000463903b23cbc0e124c, p. 39-40 (tale no. 21)</ta>
            <ta e="T2676" id="Seg_760" s="T2667">[KlT:] why PST form?</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T2648" />
            <conversion-tli id="T2649" />
            <conversion-tli id="T2650" />
            <conversion-tli id="T2651" />
            <conversion-tli id="T2652" />
            <conversion-tli id="T2653" />
            <conversion-tli id="T2654" />
            <conversion-tli id="T2655" />
            <conversion-tli id="T2656" />
            <conversion-tli id="T2657" />
            <conversion-tli id="T2658" />
            <conversion-tli id="T2659" />
            <conversion-tli id="T2660" />
            <conversion-tli id="T2661" />
            <conversion-tli id="T2662" />
            <conversion-tli id="T2663" />
            <conversion-tli id="T2664" />
            <conversion-tli id="T2665" />
            <conversion-tli id="T2666" />
            <conversion-tli id="T2667" />
            <conversion-tli id="T2668" />
            <conversion-tli id="T2669" />
            <conversion-tli id="T2670" />
            <conversion-tli id="T2671" />
            <conversion-tli id="T2672" />
            <conversion-tli id="T2673" />
            <conversion-tli id="T2674" />
            <conversion-tli id="T2675" />
            <conversion-tli id="T2676" />
            <conversion-tli id="T2677" />
            <conversion-tli id="T2678" />
            <conversion-tli id="T2679" />
            <conversion-tli id="T2680" />
            <conversion-tli id="T2681" />
            <conversion-tli id="T2682" />
            <conversion-tli id="T2683" />
            <conversion-tli id="T2684" />
            <conversion-tli id="T2685" />
            <conversion-tli id="T2686" />
            <conversion-tli id="T2687" />
            <conversion-tli id="T2688" />
            <conversion-tli id="T2689" />
            <conversion-tli id="T2690" />
            <conversion-tli id="T2691" />
            <conversion-tli id="T2692" />
            <conversion-tli id="T2693" />
            <conversion-tli id="T2694" />
            <conversion-tli id="T2695" />
            <conversion-tli id="T2696" />
            <conversion-tli id="T2697" />
            <conversion-tli id="T2698" />
            <conversion-tli id="T2699" />
            <conversion-tli id="T2700" />
            <conversion-tli id="T2701" />
            <conversion-tli id="T2702" />
            <conversion-tli id="T2703" />
            <conversion-tli id="T2704" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
