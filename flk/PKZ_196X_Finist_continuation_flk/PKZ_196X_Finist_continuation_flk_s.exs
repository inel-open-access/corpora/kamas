<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID62C40AF2-4EA9-36F3-4985-8475E1C0C02E">
   <head>
      <meta-information>
         <project-name>Kamas</project-name>
         <transcription-name>PKZ_196X_Finist_continuation_flk</transcription-name>
         <referenced-file url="PKZ_196X_Finist_continuation_flk.wav" />
         <referenced-file url="PKZ_196X_Finist_continuation_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_Finist_continuation_flk\PKZ_196X_Finist_continuation_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">511</ud-information>
            <ud-information attribute-name="# HIAT:w">327</ud-information>
            <ud-information attribute-name="# e">325</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">2</ud-information>
            <ud-information attribute-name="# HIAT:u">66</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="f" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="0.792" type="appl" />
         <tli id="T2" time="1.584" type="appl" />
         <tli id="T3" time="2.376" type="appl" />
         <tli id="T4" time="3.168" type="appl" />
         <tli id="T5" time="3.96" type="appl" />
         <tli id="T6" time="4.616" type="appl" />
         <tli id="T7" time="5.153" type="appl" />
         <tli id="T8" time="5.689" type="appl" />
         <tli id="T9" time="6.225" type="appl" />
         <tli id="T10" time="6.762" type="appl" />
         <tli id="T11" time="7.298" type="appl" />
         <tli id="T12" time="7.835" type="appl" />
         <tli id="T13" time="8.371" type="appl" />
         <tli id="T14" time="8.907" type="appl" />
         <tli id="T15" time="9.444" type="appl" />
         <tli id="T16" time="10.846530391738069" />
         <tli id="T17" time="11.415" type="appl" />
         <tli id="T18" time="12.14" type="appl" />
         <tli id="T19" time="12.865" type="appl" />
         <tli id="T20" time="13.59" type="appl" />
         <tli id="T21" time="14.315" type="appl" />
         <tli id="T22" time="15.04" type="appl" />
         <tli id="T23" time="15.765" type="appl" />
         <tli id="T24" time="16.49" type="appl" />
         <tli id="T25" time="17.215" type="appl" />
         <tli id="T26" time="18.27310375153906" />
         <tli id="T27" time="18.662" type="appl" />
         <tli id="T28" time="19.214" type="appl" />
         <tli id="T29" time="19.766" type="appl" />
         <tli id="T30" time="20.318" type="appl" />
         <tli id="T31" time="21.35973163806317" />
         <tli id="T32" time="22.234" type="appl" />
         <tli id="T33" time="23.119" type="appl" />
         <tli id="T34" time="24.004" type="appl" />
         <tli id="T35" time="24.888" type="appl" />
         <tli id="T36" time="25.772" type="appl" />
         <tli id="T37" time="27.719651732542655" />
         <tli id="T38" time="28.807" type="appl" />
         <tli id="T39" time="29.655" type="appl" />
         <tli id="T40" time="30.502" type="appl" />
         <tli id="T41" time="31.35" type="appl" />
         <tli id="T42" time="32.197" type="appl" />
         <tli id="T43" time="33.044" type="appl" />
         <tli id="T44" time="33.892" type="appl" />
         <tli id="T45" time="34.739" type="appl" />
         <tli id="T46" time="35.587" type="appl" />
         <tli id="T47" time="36.76620473905068" />
         <tli id="T48" time="37.586" type="appl" />
         <tli id="T49" time="38.362" type="appl" />
         <tli id="T50" time="39.138" type="appl" />
         <tli id="T51" time="39.913" type="appl" />
         <tli id="T52" time="40.689" type="appl" />
         <tli id="T53" time="41.465" type="appl" />
         <tli id="T54" time="42.02" type="appl" />
         <tli id="T55" time="42.575" type="appl" />
         <tli id="T56" time="43.13" type="appl" />
         <tli id="T57" time="44.052779857778226" />
         <tli id="T58" time="44.788" type="appl" />
         <tli id="T59" time="45.686" type="appl" />
         <tli id="T60" time="46.584" type="appl" />
         <tli id="T61" time="47.482" type="appl" />
         <tli id="T62" time="51.44602030303792" />
         <tli id="T63" time="52.03267959896474" />
         <tli id="T64" time="52.852" type="appl" />
         <tli id="T65" time="53.478" type="appl" />
         <tli id="T66" time="54.105" type="appl" />
         <tli id="T67" time="54.708" type="appl" />
         <tli id="T68" time="55.286" type="appl" />
         <tli id="T69" time="55.864" type="appl" />
         <tli id="T70" time="56.442" type="appl" />
         <tli id="T71" time="57.272613764856644" />
         <tli id="T72" time="57.94" type="appl" />
         <tli id="T73" time="58.56" type="appl" />
         <tli id="T74" time="59.18" type="appl" />
         <tli id="T75" time="60.48" type="appl" />
         <tli id="T76" time="62.35254994095032" />
         <tli id="T77" time="63.02" type="appl" />
         <tli id="T78" time="63.755" type="appl" />
         <tli id="T79" time="64.49" type="appl" />
         <tli id="T80" time="65.225" type="appl" />
         <tli id="T81" time="65.96" type="appl" />
         <tli id="T82" time="67.155" type="appl" />
         <tli id="T83" time="68.33" type="appl" />
         <tli id="T84" time="68.902" type="appl" />
         <tli id="T85" time="69.472" type="appl" />
         <tli id="T86" time="70.042" type="appl" />
         <tli id="T87" time="70.612" type="appl" />
         <tli id="T88" time="71.182" type="appl" />
         <tli id="T89" time="72.02576174083474" />
         <tli id="T90" time="72.85" type="appl" />
         <tli id="T91" time="73.645" type="appl" />
         <tli id="T92" time="74.44" type="appl" />
         <tli id="T93" time="75.33905344623967" />
         <tli id="T94" time="76.057" type="appl" />
         <tli id="T95" time="76.783" type="appl" />
         <tli id="T96" time="77.51" type="appl" />
         <tli id="T97" time="78.237" type="appl" />
         <tli id="T98" time="78.963" type="appl" />
         <tli id="T99" time="79.69" type="appl" />
         <tli id="T100" time="80.417" type="appl" />
         <tli id="T101" time="81.143" type="appl" />
         <tli id="T102" time="81.87" type="appl" />
         <tli id="T103" time="82.364" type="appl" />
         <tli id="T104" time="82.859" type="appl" />
         <tli id="T105" time="83.353" type="appl" />
         <tli id="T106" time="83.847" type="appl" />
         <tli id="T107" time="84.341" type="appl" />
         <tli id="T108" time="84.836" type="appl" />
         <tli id="T109" time="85.57225821041787" />
         <tli id="T110" time="86.238" type="appl" />
         <tli id="T111" time="86.992" type="appl" />
         <tli id="T112" time="88.33889011734553" />
         <tli id="T113" time="89.285" type="appl" />
         <tli id="T114" time="90.22" type="appl" />
         <tli id="T115" time="90.79" type="appl" />
         <tli id="T116" time="91.35" type="appl" />
         <tli id="T117" time="92.292" type="appl" />
         <tli id="T118" time="93.208" type="appl" />
         <tli id="T329" time="94.04977325641919" type="intp" />
         <tli id="T119" time="95.17213759831142" />
         <tli id="T120" time="96.395" type="appl" />
         <tli id="T121" time="97.455" type="appl" />
         <tli id="T122" time="99.83874563409302" />
         <tli id="T123" time="100.612" type="appl" />
         <tli id="T124" time="101.614" type="appl" />
         <tli id="T125" time="102.616" type="appl" />
         <tli id="T126" time="103.618" type="appl" />
         <tli id="T127" time="105.56534035228786" />
         <tli id="T128" time="106.318" type="appl" />
         <tli id="T129" time="107.265" type="appl" />
         <tli id="T130" time="108.212" type="appl" />
         <tli id="T131" time="109.3852923587205" />
         <tli id="T132" time="110.266" type="appl" />
         <tli id="T133" time="111.132" type="appl" />
         <tli id="T134" time="111.998" type="appl" />
         <tli id="T135" time="112.864" type="appl" />
         <tli id="T136" time="114.14523255521773" />
         <tli id="T137" time="114.836" type="appl" />
         <tli id="T138" time="115.522" type="appl" />
         <tli id="T139" time="116.208" type="appl" />
         <tli id="T140" time="116.894" type="appl" />
         <tli id="T141" time="117.581" type="appl" />
         <tli id="T142" time="118.267" type="appl" />
         <tli id="T143" time="118.953" type="appl" />
         <tli id="T144" time="119.639" type="appl" />
         <tli id="T145" time="120.325" type="appl" />
         <tli id="T146" time="121.145" type="appl" />
         <tli id="T147" time="121.959" type="appl" />
         <tli id="T148" time="122.93845541121189" />
         <tli id="T149" time="123.564" type="appl" />
         <tli id="T150" time="124.062" type="appl" />
         <tli id="T151" time="124.56" type="appl" />
         <tli id="T152" time="125.13176118802926" />
         <tli id="T153" time="125.75" type="appl" />
         <tli id="T154" time="126.324" type="appl" />
         <tli id="T155" time="126.897" type="appl" />
         <tli id="T156" time="127.47" type="appl" />
         <tli id="T157" time="128.044" type="appl" />
         <tli id="T158" time="128.617" type="appl" />
         <tli id="T159" time="129.158" type="appl" />
         <tli id="T160" time="129.7" type="appl" />
         <tli id="T161" time="130.33" type="appl" />
         <tli id="T162" time="130.96" type="appl" />
         <tli id="T163" time="132.7449988692615" />
         <tli id="T164" time="133.61832123024348" />
         <tli id="T165" time="134.517" type="appl" />
         <tli id="T166" time="135.264" type="appl" />
         <tli id="T167" time="136.011" type="appl" />
         <tli id="T168" time="136.759" type="appl" />
         <tli id="T169" time="137.506" type="appl" />
         <tli id="T170" time="138.253" type="appl" />
         <tli id="T171" time="139.0" type="appl" />
         <tli id="T172" time="139.496" type="appl" />
         <tli id="T173" time="139.982" type="appl" />
         <tli id="T174" time="140.468" type="appl" />
         <tli id="T175" time="140.954" type="appl" />
         <tli id="T176" time="141.44" type="appl" />
         <tli id="T177" time="141.995" type="appl" />
         <tli id="T178" time="142.55" type="appl" />
         <tli id="T179" time="143.105" type="appl" />
         <tli id="T180" time="143.66" type="appl" />
         <tli id="T181" time="144.215" type="appl" />
         <tli id="T182" time="144.77" type="appl" />
         <tli id="T183" time="145.325" type="appl" />
         <tli id="T184" time="145.88" type="appl" />
         <tli id="T185" time="146.5" type="appl" />
         <tli id="T186" time="147.12" type="appl" />
         <tli id="T187" time="147.742" type="appl" />
         <tli id="T188" time="148.358" type="appl" />
         <tli id="T189" time="149.29812423046963" />
         <tli id="T190" time="150.674" type="appl" />
         <tli id="T191" time="151.998" type="appl" />
         <tli id="T192" time="153.322" type="appl" />
         <tli id="T193" time="154.646" type="appl" />
         <tli id="T194" time="155.99804005327036" />
         <tli id="T195" time="156.643" type="appl" />
         <tli id="T196" time="157.277" type="appl" />
         <tli id="T197" time="157.91" type="appl" />
         <tli id="T198" time="158.543" type="appl" />
         <tli id="T199" time="159.177" type="appl" />
         <tli id="T200" time="160.01798954695076" />
         <tli id="T201" time="160.895" type="appl" />
         <tli id="T202" time="161.705" type="appl" />
         <tli id="T203" time="162.515" type="appl" />
         <tli id="T204" time="163.068" type="appl" />
         <tli id="T205" time="163.582" type="appl" />
         <tli id="T206" time="164.095" type="appl" />
         <tli id="T207" time="164.845" type="appl" />
         <tli id="T208" time="165.54" type="appl" />
         <tli id="T209" time="166.235" type="appl" />
         <tli id="T210" time="166.93" type="appl" />
         <tli id="T211" time="167.637" type="appl" />
         <tli id="T212" time="168.313" type="appl" />
         <tli id="T213" time="169.44453777922956" />
         <tli id="T214" time="170.135" type="appl" />
         <tli id="T215" time="171.20451566701007" />
         <tli id="T216" time="172.134" type="appl" />
         <tli id="T217" time="173.058" type="appl" />
         <tli id="T218" time="173.982" type="appl" />
         <tli id="T219" time="174.906" type="appl" />
         <tli id="T220" time="176.04445485840643" />
         <tli id="T221" time="177.122" type="appl" />
         <tli id="T222" time="178.028" type="appl" />
         <tli id="T223" time="180.82439480362842" />
         <tli id="T224" time="181.469" type="appl" />
         <tli id="T225" time="182.047" type="appl" />
         <tli id="T226" time="182.626" type="appl" />
         <tli id="T227" time="183.205" type="appl" />
         <tli id="T228" time="183.784" type="appl" />
         <tli id="T229" time="184.362" type="appl" />
         <tli id="T230" time="184.941" type="appl" />
         <tli id="T231" time="185.52" type="appl" />
         <tli id="T232" time="186.099" type="appl" />
         <tli id="T233" time="186.678" type="appl" />
         <tli id="T234" time="187.256" type="appl" />
         <tli id="T235" time="187.835" type="appl" />
         <tli id="T236" time="188.552" type="appl" />
         <tli id="T237" time="189.235" type="appl" />
         <tli id="T238" time="189.918" type="appl" />
         <tli id="T239" time="190.7776030856597" />
         <tli id="T240" time="191.395" type="appl" />
         <tli id="T241" time="192.13" type="appl" />
         <tli id="T242" time="192.625" type="appl" />
         <tli id="T243" time="193.12" type="appl" />
         <tli id="T244" time="193.615" type="appl" />
         <tli id="T245" time="194.11" type="appl" />
         <tli id="T246" time="195.058" type="appl" />
         <tli id="T247" time="195.97" type="appl" />
         <tli id="T248" time="196.882" type="appl" />
         <tli id="T249" time="197.795" type="appl" />
         <tli id="T250" time="198.708" type="appl" />
         <tli id="T251" time="200.31748322737894" />
         <tli id="T252" time="201.022" type="appl" />
         <tli id="T253" time="201.557" type="appl" />
         <tli id="T254" time="202.302" type="appl" />
         <tli id="T255" time="203.047" type="appl" />
         <tli id="T256" time="203.792" type="appl" />
         <tli id="T257" time="204.537" type="appl" />
         <tli id="T258" time="205.282" type="appl" />
         <tli id="T259" time="206.2240756840968" />
         <tli id="T260" time="207.171" type="appl" />
         <tli id="T261" time="208.073" type="appl" />
         <tli id="T262" time="208.974" type="appl" />
         <tli id="T263" time="209.876" type="appl" />
         <tli id="T264" time="210.99068246350228" />
         <tli id="T265" time="211.616" type="appl" />
         <tli id="T266" time="212.171" type="appl" />
         <tli id="T267" time="212.727" type="appl" />
         <tli id="T268" time="213.283" type="appl" />
         <tli id="T269" time="213.839" type="appl" />
         <tli id="T270" time="214.394" type="appl" />
         <tli id="T271" time="214.95" type="appl" />
         <tli id="T272" time="215.835" type="appl" />
         <tli id="T273" time="216.72" type="appl" />
         <tli id="T274" time="217.605" type="appl" />
         <tli id="T275" time="218.49" type="appl" />
         <tli id="T276" time="219.375" type="appl" />
         <tli id="T277" time="220.26" type="appl" />
         <tli id="T278" time="221.145" type="appl" />
         <tli id="T279" time="222.03" type="appl" />
         <tli id="T280" time="222.768" type="appl" />
         <tli id="T281" time="223.506" type="appl" />
         <tli id="T282" time="224.244" type="appl" />
         <tli id="T283" time="224.982" type="appl" />
         <tli id="T284" time="225.72" type="appl" />
         <tli id="T285" time="226.889" type="appl" />
         <tli id="T286" time="228.009" type="appl" />
         <tli id="T287" time="229.128" type="appl" />
         <tli id="T288" time="230.248" type="appl" />
         <tli id="T289" time="231.64375631328994" />
         <tli id="T290" time="232.763" type="appl" />
         <tli id="T291" time="233.906" type="appl" />
         <tli id="T292" time="235.049" type="appl" />
         <tli id="T293" time="236.192" type="appl" />
         <tli id="T294" time="237.335" type="appl" />
         <tli id="T295" time="238.205" type="appl" />
         <tli id="T296" time="239.069" type="appl" />
         <tli id="T297" time="240.53031132999976" />
         <tli id="T298" time="241.672" type="appl" />
         <tli id="T299" time="242.712" type="appl" />
         <tli id="T300" time="243.752" type="appl" />
         <tli id="T301" time="244.274" type="appl" />
         <tli id="T302" time="244.796" type="appl" />
         <tli id="T303" time="245.318" type="appl" />
         <tli id="T304" time="245.84" type="appl" />
         <tli id="T305" time="246.72356685177274" />
         <tli id="T306" time="247.337" type="appl" />
         <tli id="T307" time="247.932" type="appl" />
         <tli id="T308" time="248.527" type="appl" />
         <tli id="T309" time="249.19020252782872" />
         <tli id="T310" time="249.675" type="appl" />
         <tli id="T311" time="250.14" type="appl" />
         <tli id="T312" time="250.605" type="appl" />
         <tli id="T313" time="251.07" type="appl" />
         <tli id="T314" time="251.535" type="appl" />
         <tli id="T315" time="252.0" type="appl" />
         <tli id="T316" time="252.465" type="appl" />
         <tli id="T317" time="252.93" type="appl" />
         <tli id="T318" time="253.395" type="appl" />
         <tli id="T319" time="254.49680252280325" />
         <tli id="T320" time="255.628" type="appl" />
         <tli id="T321" time="256.875" type="appl" />
         <tli id="T322" time="258.122" type="appl" />
         <tli id="T323" time="259.37" type="appl" />
         <tli id="T324" time="260.618" type="appl" />
         <tli id="T325" time="261.865" type="appl" />
         <tli id="T326" time="263.112" type="appl" />
         <tli id="T327" time="264.36" type="appl" />
         <tli id="T328" time="265.31" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T64" start="T62">
            <tli id="T62.tx.1" />
         </timeline-fork>
         <timeline-fork end="T71" start="T70">
            <tli id="T70.tx.1" />
         </timeline-fork>
         <timeline-fork end="T165" start="T163">
            <tli id="T163.tx.1" />
         </timeline-fork>
         <timeline-fork end="T224" start="T223">
            <tli id="T223.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T328" id="Seg_0" n="sc" s="T0">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Dĭgəttə</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">dĭ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">kambi</ts>
                  <nts id="Seg_11" n="HIAT:ip">,</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T3">kluboktə</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_17" n="HIAT:w" s="T4">baruʔpi</ts>
                  <nts id="Seg_18" n="HIAT:ip">.</nts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_21" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_23" n="HIAT:w" s="T5">Gibər</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">dĭ</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">kaləj</ts>
                  <nts id="Seg_30" n="HIAT:ip">,</nts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_33" n="HIAT:w" s="T8">i</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_36" n="HIAT:w" s="T9">dĭ</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_39" n="HIAT:w" s="T10">dĭʔnə</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_41" n="HIAT:ip">(</nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">ka-</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">dĭʔnə</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">kalə-</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">kan-</ts>
                  <nts id="Seg_53" n="HIAT:ip">)</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_56" n="HIAT:w" s="T15">kandəga</ts>
                  <nts id="Seg_57" n="HIAT:ip">.</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_60" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_62" n="HIAT:w" s="T16">Kluboktə</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">nuʔməbi</ts>
                  <nts id="Seg_66" n="HIAT:ip">,</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_69" n="HIAT:w" s="T18">nuʔməbi</ts>
                  <nts id="Seg_70" n="HIAT:ip">,</nts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">i</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">deʔpi</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">dĭbər</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">gijen</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_85" n="HIAT:w" s="T23">dĭn</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_88" n="HIAT:w" s="T24">tibi</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_91" n="HIAT:w" s="T25">amnolaʔbə</ts>
                  <nts id="Seg_92" n="HIAT:ip">.</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_95" n="HIAT:u" s="T26">
                  <nts id="Seg_96" n="HIAT:ip">(</nts>
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">Dĭ=</ts>
                  <nts id="Seg_99" n="HIAT:ip">)</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_102" n="HIAT:w" s="T27">Dĭn</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_105" n="HIAT:w" s="T28">už</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_108" n="HIAT:w" s="T29">ne</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_111" n="HIAT:w" s="T30">ige</ts>
                  <nts id="Seg_112" n="HIAT:ip">.</nts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_115" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_117" n="HIAT:w" s="T31">Pimnie</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_120" n="HIAT:w" s="T32">kanzittə</ts>
                  <nts id="Seg_121" n="HIAT:ip">,</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_124" n="HIAT:w" s="T33">dĭgəttə</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_127" n="HIAT:w" s="T34">nʼiʔnen</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_129" n="HIAT:ip">(</nts>
                  <ts e="T36" id="Seg_131" n="HIAT:w" s="T35">am-</ts>
                  <nts id="Seg_132" n="HIAT:ip">)</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_135" n="HIAT:w" s="T36">amnobi</ts>
                  <nts id="Seg_136" n="HIAT:ip">.</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_139" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_141" n="HIAT:w" s="T37">I</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_144" n="HIAT:w" s="T38">takšebə</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_147" n="HIAT:w" s="T39">nuldəbi</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_150" n="HIAT:w" s="T40">i</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_153" n="HIAT:w" s="T41">munəjʔtə</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_155" n="HIAT:ip">(</nts>
                  <ts e="T43" id="Seg_157" n="HIAT:w" s="T42">emdəbi</ts>
                  <nts id="Seg_158" n="HIAT:ip">)</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_161" n="HIAT:w" s="T43">i</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_164" n="HIAT:w" s="T44">bar</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_167" n="HIAT:w" s="T45">sʼarla</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_170" n="HIAT:w" s="T46">amnolaʔbə</ts>
                  <nts id="Seg_171" n="HIAT:ip">.</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_174" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_176" n="HIAT:w" s="T47">A</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_179" n="HIAT:w" s="T48">dĭ</ts>
                  <nts id="Seg_180" n="HIAT:ip">,</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_183" n="HIAT:w" s="T49">tibin</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_186" n="HIAT:w" s="T50">net:</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_188" n="HIAT:ip">"</nts>
                  <ts e="T52" id="Seg_190" n="HIAT:w" s="T51">Sadardə</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_193" n="HIAT:w" s="T52">măna</ts>
                  <nts id="Seg_194" n="HIAT:ip">"</nts>
                  <nts id="Seg_195" n="HIAT:ip">.</nts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_198" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_200" n="HIAT:w" s="T53">A</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_203" n="HIAT:w" s="T54">dĭ</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_205" n="HIAT:ip">(</nts>
                  <ts e="T56" id="Seg_207" n="HIAT:w" s="T55">măna=</ts>
                  <nts id="Seg_208" n="HIAT:ip">)</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_211" n="HIAT:w" s="T56">măndə:</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_213" n="HIAT:ip">"</nts>
                  <ts e="T58" id="Seg_215" n="HIAT:w" s="T57">Mĭləl</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_218" n="HIAT:w" s="T58">tăn</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_221" n="HIAT:w" s="T59">tibi</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_224" n="HIAT:w" s="T60">măna</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_227" n="HIAT:w" s="T61">kunolzittə</ts>
                  <nts id="Seg_228" n="HIAT:ip">?</nts>
                  <nts id="Seg_229" n="HIAT:ip">"</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_232" n="HIAT:u" s="T62">
                  <nts id="Seg_233" n="HIAT:ip">(</nts>
                  <nts id="Seg_234" n="HIAT:ip">(</nts>
                  <ats e="T62.tx.1" id="Seg_235" n="HIAT:non-pho" s="T62">BRK</ats>
                  <nts id="Seg_236" n="HIAT:ip">)</nts>
                  <nts id="Seg_237" n="HIAT:ip">)</nts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_240" n="HIAT:w" s="T62.tx.1">Dĭ</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_243" n="HIAT:w" s="T64">dĭʔnə</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_246" n="HIAT:w" s="T65">mĭbi</ts>
                  <nts id="Seg_247" n="HIAT:ip">.</nts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_250" n="HIAT:u" s="T66">
                  <nts id="Seg_251" n="HIAT:ip">"</nts>
                  <ts e="T67" id="Seg_253" n="HIAT:w" s="T66">Kunolaʔ</ts>
                  <nts id="Seg_254" n="HIAT:ip">,</nts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_257" n="HIAT:w" s="T67">ĭmbi</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_260" n="HIAT:w" s="T68">măna</ts>
                  <nts id="Seg_261" n="HIAT:ip">,</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_264" n="HIAT:w" s="T69">ajiriam</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70.tx.1" id="Seg_267" n="HIAT:w" s="T70">što</ts>
                  <nts id="Seg_268" n="HIAT:ip">_</nts>
                  <ts e="T71" id="Seg_270" n="HIAT:w" s="T70.tx.1">li</ts>
                  <nts id="Seg_271" n="HIAT:ip">?</nts>
                  <nts id="Seg_272" n="HIAT:ip">"</nts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_275" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_277" n="HIAT:w" s="T71">Dĭ</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_280" n="HIAT:w" s="T72">šobi</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_283" n="HIAT:w" s="T73">maːʔndə</ts>
                  <nts id="Seg_284" n="HIAT:ip">.</nts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_287" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_289" n="HIAT:w" s="T74">Amnobi</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_292" n="HIAT:w" s="T75">amorzittə</ts>
                  <nts id="Seg_293" n="HIAT:ip">.</nts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_296" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_298" n="HIAT:w" s="T76">Dĭ</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_301" n="HIAT:w" s="T77">dĭʔnə</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_303" n="HIAT:ip">(</nts>
                  <ts e="T79" id="Seg_305" n="HIAT:w" s="T78">mĭ-</ts>
                  <nts id="Seg_306" n="HIAT:ip">)</nts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_309" n="HIAT:w" s="T79">mĭbi</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_312" n="HIAT:w" s="T80">kaplʼaʔi</ts>
                  <nts id="Seg_313" n="HIAT:ip">.</nts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_316" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_318" n="HIAT:w" s="T81">Dĭ</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_321" n="HIAT:w" s="T82">kunolluʔpi</ts>
                  <nts id="Seg_322" n="HIAT:ip">.</nts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_325" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_327" n="HIAT:w" s="T83">Dĭ</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_330" n="HIAT:w" s="T84">dĭm</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_333" n="HIAT:w" s="T85">kumbi</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_336" n="HIAT:w" s="T86">dĭbər</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_339" n="HIAT:w" s="T87">gijen</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_342" n="HIAT:w" s="T88">kunollia</ts>
                  <nts id="Seg_343" n="HIAT:ip">.</nts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_346" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_348" n="HIAT:w" s="T89">Dĭgəttə</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_351" n="HIAT:w" s="T90">dĭm</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_354" n="HIAT:w" s="T91">dĭbər</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_357" n="HIAT:w" s="T92">öʔlubi</ts>
                  <nts id="Seg_358" n="HIAT:ip">.</nts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_361" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_363" n="HIAT:w" s="T93">Dĭ</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_366" n="HIAT:w" s="T94">bar</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_369" n="HIAT:w" s="T95">amnobi</ts>
                  <nts id="Seg_370" n="HIAT:ip">,</nts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_373" n="HIAT:w" s="T96">amnobi</ts>
                  <nts id="Seg_374" n="HIAT:ip">,</nts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_377" n="HIAT:w" s="T97">kürümbi</ts>
                  <nts id="Seg_378" n="HIAT:ip">,</nts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_381" n="HIAT:w" s="T98">kürümbi</ts>
                  <nts id="Seg_382" n="HIAT:ip">,</nts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_385" n="HIAT:w" s="T99">dʼăbaktərbi</ts>
                  <nts id="Seg_386" n="HIAT:ip">,</nts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_389" n="HIAT:w" s="T100">măndə:</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_391" n="HIAT:ip">"</nts>
                  <ts e="T102" id="Seg_393" n="HIAT:w" s="T101">Uʔbdaʔ</ts>
                  <nts id="Seg_394" n="HIAT:ip">!</nts>
                  <nts id="Seg_395" n="HIAT:ip">"</nts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_398" n="HIAT:u" s="T102">
                  <ts e="T103" id="Seg_400" n="HIAT:w" s="T102">A</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_403" n="HIAT:w" s="T103">dĭ</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_406" n="HIAT:w" s="T104">uge</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_409" n="HIAT:w" s="T105">ej</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_412" n="HIAT:w" s="T106">uʔblia</ts>
                  <nts id="Seg_413" n="HIAT:ip">,</nts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_416" n="HIAT:w" s="T107">kunollaʔbə</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_419" n="HIAT:w" s="T108">tăŋ</ts>
                  <nts id="Seg_420" n="HIAT:ip">.</nts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_423" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_425" n="HIAT:w" s="T109">Dĭgəttə</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_428" n="HIAT:w" s="T110">ertə</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_431" n="HIAT:w" s="T111">molambi</ts>
                  <nts id="Seg_432" n="HIAT:ip">.</nts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_435" n="HIAT:u" s="T112">
                  <ts e="T113" id="Seg_437" n="HIAT:w" s="T112">Dĭ</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_439" n="HIAT:ip">(</nts>
                  <ts e="T114" id="Seg_441" n="HIAT:w" s="T113">kajbear</ts>
                  <nts id="Seg_442" n="HIAT:ip">)</nts>
                  <nts id="Seg_443" n="HIAT:ip">.</nts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_446" n="HIAT:u" s="T114">
                  <nts id="Seg_447" n="HIAT:ip">"</nts>
                  <ts e="T115" id="Seg_449" n="HIAT:w" s="T114">Soʔ</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_452" n="HIAT:w" s="T115">döbər</ts>
                  <nts id="Seg_453" n="HIAT:ip">!</nts>
                  <nts id="Seg_454" n="HIAT:ip">"</nts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_457" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_459" n="HIAT:w" s="T116">Dĭ</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_462" n="HIAT:w" s="T117">šobi</ts>
                  <nts id="Seg_463" n="HIAT:ip">,</nts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_466" n="HIAT:w" s="T118">kalla</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_469" n="HIAT:w" s="T329">dʼürbi</ts>
                  <nts id="Seg_470" n="HIAT:ip">.</nts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_473" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_475" n="HIAT:w" s="T119">Dĭgəttə</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_478" n="HIAT:w" s="T120">dĭ</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_481" n="HIAT:w" s="T121">uʔbdəbi</ts>
                  <nts id="Seg_482" n="HIAT:ip">.</nts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_485" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_487" n="HIAT:w" s="T122">Dĭgəttə</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_490" n="HIAT:w" s="T123">bazoʔ</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_493" n="HIAT:w" s="T124">amnolaʔbə</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_496" n="HIAT:w" s="T125">i</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_499" n="HIAT:w" s="T126">sʼarlaʔbə</ts>
                  <nts id="Seg_500" n="HIAT:ip">.</nts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_503" n="HIAT:u" s="T127">
                  <nts id="Seg_504" n="HIAT:ip">(</nts>
                  <ts e="T128" id="Seg_506" n="HIAT:w" s="T127">Am-</ts>
                  <nts id="Seg_507" n="HIAT:ip">)</nts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_510" n="HIAT:w" s="T128">Amnobi</ts>
                  <nts id="Seg_511" n="HIAT:ip">,</nts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_514" n="HIAT:w" s="T129">sedendən</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_517" n="HIAT:w" s="T130">tondə</ts>
                  <nts id="Seg_518" n="HIAT:ip">.</nts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_521" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_523" n="HIAT:w" s="T131">Dĭgəttə</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_526" n="HIAT:w" s="T132">davaj</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_529" n="HIAT:w" s="T133">ererzittə</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_532" n="HIAT:w" s="T134">dĭ</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_535" n="HIAT:w" s="T135">kudʼelʼam</ts>
                  <nts id="Seg_536" n="HIAT:ip">.</nts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T145" id="Seg_539" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_541" n="HIAT:w" s="T136">Ererleʔbə</ts>
                  <nts id="Seg_542" n="HIAT:ip">,</nts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_545" n="HIAT:w" s="T137">ererleʔbə</ts>
                  <nts id="Seg_546" n="HIAT:ip">,</nts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_548" n="HIAT:ip">(</nts>
                  <ts e="T139" id="Seg_550" n="HIAT:w" s="T138">dĭn=</ts>
                  <nts id="Seg_551" n="HIAT:ip">)</nts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_554" n="HIAT:w" s="T139">dĭ</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_557" n="HIAT:w" s="T140">ne</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_560" n="HIAT:w" s="T141">bazoʔ</ts>
                  <nts id="Seg_561" n="HIAT:ip">,</nts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_564" n="HIAT:w" s="T142">tibin</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_567" n="HIAT:w" s="T143">ne</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_570" n="HIAT:w" s="T144">šobi</ts>
                  <nts id="Seg_571" n="HIAT:ip">.</nts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_574" n="HIAT:u" s="T145">
                  <nts id="Seg_575" n="HIAT:ip">"</nts>
                  <ts e="T146" id="Seg_577" n="HIAT:w" s="T145">Sadardə</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_580" n="HIAT:w" s="T146">măna</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_583" n="HIAT:w" s="T147">dĭ</ts>
                  <nts id="Seg_584" n="HIAT:ip">"</nts>
                  <nts id="Seg_585" n="HIAT:ip">.</nts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_588" n="HIAT:u" s="T148">
                  <nts id="Seg_589" n="HIAT:ip">"</nts>
                  <ts e="T149" id="Seg_591" n="HIAT:w" s="T148">Dʼok</ts>
                  <nts id="Seg_592" n="HIAT:ip">,</nts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_595" n="HIAT:w" s="T149">măn</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_598" n="HIAT:w" s="T150">ej</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_601" n="HIAT:w" s="T151">sadarlam</ts>
                  <nts id="Seg_602" n="HIAT:ip">.</nts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T158" id="Seg_605" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_607" n="HIAT:w" s="T152">Tibim</ts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_609" n="HIAT:ip">(</nts>
                  <ts e="T154" id="Seg_611" n="HIAT:w" s="T153">mĭne-</ts>
                  <nts id="Seg_612" n="HIAT:ip">)</nts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_615" n="HIAT:w" s="T154">mĭləl</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_618" n="HIAT:w" s="T155">măna</ts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_621" n="HIAT:w" s="T156">nüdʼi</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_624" n="HIAT:w" s="T157">kunolzittə</ts>
                  <nts id="Seg_625" n="HIAT:ip">.</nts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_628" n="HIAT:u" s="T158">
                  <ts e="T159" id="Seg_630" n="HIAT:w" s="T158">Dĭgəttə</ts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_633" n="HIAT:w" s="T159">mĭlim</ts>
                  <nts id="Seg_634" n="HIAT:ip">"</nts>
                  <nts id="Seg_635" n="HIAT:ip">.</nts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T163" id="Seg_638" n="HIAT:u" s="T160">
                  <nts id="Seg_639" n="HIAT:ip">"</nts>
                  <ts e="T161" id="Seg_641" n="HIAT:w" s="T160">Nu</ts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_644" n="HIAT:w" s="T161">it</ts>
                  <nts id="Seg_645" n="HIAT:ip">,</nts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_648" n="HIAT:w" s="T162">mĭləm</ts>
                  <nts id="Seg_649" n="HIAT:ip">"</nts>
                  <nts id="Seg_650" n="HIAT:ip">.</nts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T171" id="Seg_653" n="HIAT:u" s="T163">
                  <nts id="Seg_654" n="HIAT:ip">(</nts>
                  <nts id="Seg_655" n="HIAT:ip">(</nts>
                  <ats e="T163.tx.1" id="Seg_656" n="HIAT:non-pho" s="T163">BRK</ats>
                  <nts id="Seg_657" n="HIAT:ip">)</nts>
                  <nts id="Seg_658" n="HIAT:ip">)</nts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_661" n="HIAT:w" s="T163.tx.1">Dĭgəttə</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_664" n="HIAT:w" s="T165">dĭ</ts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_667" n="HIAT:w" s="T166">šobi</ts>
                  <nts id="Seg_668" n="HIAT:ip">,</nts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_671" n="HIAT:w" s="T167">dĭ</ts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_674" n="HIAT:w" s="T168">bazoʔ</ts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_677" n="HIAT:w" s="T169">amorzittə</ts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_680" n="HIAT:w" s="T170">amnobi</ts>
                  <nts id="Seg_681" n="HIAT:ip">.</nts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T176" id="Seg_684" n="HIAT:u" s="T171">
                  <ts e="T172" id="Seg_686" n="HIAT:w" s="T171">Dĭ</ts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_689" n="HIAT:w" s="T172">dĭʔnə</ts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_692" n="HIAT:w" s="T173">bazoʔ</ts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_695" n="HIAT:w" s="T174">kaplʼaʔi</ts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_698" n="HIAT:w" s="T175">mĭbi</ts>
                  <nts id="Seg_699" n="HIAT:ip">.</nts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_702" n="HIAT:u" s="T176">
                  <ts e="T177" id="Seg_704" n="HIAT:w" s="T176">Dĭ</ts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_707" n="HIAT:w" s="T177">kunolluʔpi</ts>
                  <nts id="Seg_708" n="HIAT:ip">,</nts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_711" n="HIAT:w" s="T178">dĭ</ts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_714" n="HIAT:w" s="T179">dĭm</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_717" n="HIAT:w" s="T180">turanə</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_720" n="HIAT:w" s="T181">kambi</ts>
                  <nts id="Seg_721" n="HIAT:ip">,</nts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_724" n="HIAT:w" s="T182">gijen</ts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_727" n="HIAT:w" s="T183">kunollaʔbə</ts>
                  <nts id="Seg_728" n="HIAT:ip">.</nts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_731" n="HIAT:u" s="T184">
                  <nts id="Seg_732" n="HIAT:ip">"</nts>
                  <ts e="T185" id="Seg_734" n="HIAT:w" s="T184">Kanaʔ</ts>
                  <nts id="Seg_735" n="HIAT:ip">,</nts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_738" n="HIAT:w" s="T185">kunollaʔ</ts>
                  <nts id="Seg_739" n="HIAT:ip">!</nts>
                  <nts id="Seg_740" n="HIAT:ip">"</nts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T189" id="Seg_743" n="HIAT:u" s="T186">
                  <ts e="T187" id="Seg_745" n="HIAT:w" s="T186">Dĭ</ts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_748" n="HIAT:w" s="T187">dĭbər</ts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_751" n="HIAT:w" s="T188">kambi</ts>
                  <nts id="Seg_752" n="HIAT:ip">.</nts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T194" id="Seg_755" n="HIAT:u" s="T189">
                  <nts id="Seg_756" n="HIAT:ip">(</nts>
                  <ts e="T190" id="Seg_758" n="HIAT:w" s="T189">Dĭm=</ts>
                  <nts id="Seg_759" n="HIAT:ip">)</nts>
                  <nts id="Seg_760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_762" n="HIAT:w" s="T190">Kürimbi</ts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_765" n="HIAT:w" s="T191">dĭʔnə:</ts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_767" n="HIAT:ip">"</nts>
                  <ts e="T193" id="Seg_769" n="HIAT:w" s="T192">Uʔbdaʔ</ts>
                  <nts id="Seg_770" n="HIAT:ip">,</nts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_773" n="HIAT:w" s="T193">uʔbdaʔ</ts>
                  <nts id="Seg_774" n="HIAT:ip">"</nts>
                  <nts id="Seg_775" n="HIAT:ip">.</nts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T200" id="Seg_778" n="HIAT:u" s="T194">
                  <ts e="T196" id="Seg_780" n="HIAT:w" s="T194">Dĭgəttə</ts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_783" n="HIAT:w" s="T196">kădedə</ts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_786" n="HIAT:w" s="T197">ej</ts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_789" n="HIAT:w" s="T198">molia</ts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_792" n="HIAT:w" s="T199">uʔbsittə</ts>
                  <nts id="Seg_793" n="HIAT:ip">.</nts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T203" id="Seg_796" n="HIAT:u" s="T200">
                  <ts e="T201" id="Seg_798" n="HIAT:w" s="T200">Dĭgəttə</ts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_801" n="HIAT:w" s="T201">ertə</ts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_804" n="HIAT:w" s="T202">molambi</ts>
                  <nts id="Seg_805" n="HIAT:ip">.</nts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T206" id="Seg_808" n="HIAT:u" s="T203">
                  <ts e="T204" id="Seg_810" n="HIAT:w" s="T203">Dĭ</ts>
                  <nts id="Seg_811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_813" n="HIAT:w" s="T204">măndə:</ts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_815" n="HIAT:ip">"</nts>
                  <ts e="T206" id="Seg_817" n="HIAT:w" s="T205">Kanaʔ</ts>
                  <nts id="Seg_818" n="HIAT:ip">!</nts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T210" id="Seg_821" n="HIAT:u" s="T206">
                  <ts e="T207" id="Seg_823" n="HIAT:w" s="T206">Kabarləj</ts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_826" n="HIAT:w" s="T207">tănan</ts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_829" n="HIAT:w" s="T208">dön</ts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_832" n="HIAT:w" s="T209">amnozittə</ts>
                  <nts id="Seg_833" n="HIAT:ip">"</nts>
                  <nts id="Seg_834" n="HIAT:ip">.</nts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T213" id="Seg_837" n="HIAT:u" s="T210">
                  <ts e="T211" id="Seg_839" n="HIAT:w" s="T210">Dĭgəttə</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_842" n="HIAT:w" s="T211">dĭ</ts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_845" n="HIAT:w" s="T212">kambi</ts>
                  <nts id="Seg_846" n="HIAT:ip">.</nts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T215" id="Seg_849" n="HIAT:u" s="T213">
                  <ts e="T214" id="Seg_851" n="HIAT:w" s="T213">Dĭ</ts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_854" n="HIAT:w" s="T214">uʔbdəbi</ts>
                  <nts id="Seg_855" n="HIAT:ip">.</nts>
                  <nts id="Seg_856" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T220" id="Seg_858" n="HIAT:u" s="T215">
                  <ts e="T216" id="Seg_860" n="HIAT:w" s="T215">Dĭgəttə</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_863" n="HIAT:w" s="T216">bazoʔ</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_865" n="HIAT:ip">(</nts>
                  <ts e="T218" id="Seg_867" n="HIAT:w" s="T217">šeže-</ts>
                  <nts id="Seg_868" n="HIAT:ip">)</nts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_870" n="HIAT:ip">(</nts>
                  <ts e="T219" id="Seg_872" n="HIAT:w" s="T218">sezeŋgən</ts>
                  <nts id="Seg_873" n="HIAT:ip">)</nts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_876" n="HIAT:w" s="T219">amnolaʔbə</ts>
                  <nts id="Seg_877" n="HIAT:ip">.</nts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T223" id="Seg_880" n="HIAT:u" s="T220">
                  <ts e="T221" id="Seg_882" n="HIAT:w" s="T220">Kornɨš</ts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_885" n="HIAT:w" s="T221">nuldəbi</ts>
                  <nts id="Seg_886" n="HIAT:ip">,</nts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_889" n="HIAT:w" s="T222">kuvas</ts>
                  <nts id="Seg_890" n="HIAT:ip">.</nts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T235" id="Seg_893" n="HIAT:u" s="T223">
                  <ts e="T223.tx.1" id="Seg_895" n="HIAT:w" s="T223">Kolʼečkə</ts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_898" n="HIAT:w" s="T223.tx.1">zălatoj</ts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_900" n="HIAT:ip">(</nts>
                  <ts e="T225" id="Seg_902" n="HIAT:w" s="T224">uda-</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_905" n="HIAT:w" s="T225">udandə=</ts>
                  <nts id="Seg_906" n="HIAT:ip">)</nts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_909" n="HIAT:w" s="T226">udandə</ts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_912" n="HIAT:w" s="T227">šerie</ts>
                  <nts id="Seg_913" n="HIAT:ip">,</nts>
                  <nts id="Seg_914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_916" n="HIAT:w" s="T228">to</ts>
                  <nts id="Seg_917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_919" n="HIAT:w" s="T229">dö</ts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_922" n="HIAT:w" s="T230">udandə</ts>
                  <nts id="Seg_923" n="HIAT:ip">,</nts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_926" n="HIAT:w" s="T231">to</ts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_929" n="HIAT:w" s="T232">dö</ts>
                  <nts id="Seg_930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_932" n="HIAT:w" s="T233">udandə</ts>
                  <nts id="Seg_933" n="HIAT:ip">.</nts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T239" id="Seg_936" n="HIAT:u" s="T235">
                  <ts e="T236" id="Seg_938" n="HIAT:w" s="T235">Dĭ</ts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_941" n="HIAT:w" s="T236">kulaʔpi</ts>
                  <nts id="Seg_942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_944" n="HIAT:w" s="T237">da</ts>
                  <nts id="Seg_945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_947" n="HIAT:w" s="T238">kambi</ts>
                  <nts id="Seg_948" n="HIAT:ip">.</nts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T241" id="Seg_951" n="HIAT:u" s="T239">
                  <nts id="Seg_952" n="HIAT:ip">"</nts>
                  <ts e="T240" id="Seg_954" n="HIAT:w" s="T239">Sadardə</ts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_957" n="HIAT:w" s="T240">măna</ts>
                  <nts id="Seg_958" n="HIAT:ip">!</nts>
                  <nts id="Seg_959" n="HIAT:ip">"</nts>
                  <nts id="Seg_960" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T245" id="Seg_962" n="HIAT:u" s="T241">
                  <nts id="Seg_963" n="HIAT:ip">"</nts>
                  <ts e="T242" id="Seg_965" n="HIAT:w" s="T241">Dʼok</ts>
                  <nts id="Seg_966" n="HIAT:ip">,</nts>
                  <nts id="Seg_967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_969" n="HIAT:w" s="T242">măn</ts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_972" n="HIAT:w" s="T243">ej</ts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_975" n="HIAT:w" s="T244">sadariam</ts>
                  <nts id="Seg_976" n="HIAT:ip">.</nts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_979" n="HIAT:u" s="T245">
                  <ts e="T246" id="Seg_981" n="HIAT:w" s="T245">Tibi</ts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_984" n="HIAT:w" s="T246">mĭʔ</ts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_987" n="HIAT:w" s="T247">măna</ts>
                  <nts id="Seg_988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_990" n="HIAT:w" s="T248">teinen</ts>
                  <nts id="Seg_991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_993" n="HIAT:w" s="T249">šazittə</ts>
                  <nts id="Seg_994" n="HIAT:ip">,</nts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_997" n="HIAT:w" s="T250">kunolzittə</ts>
                  <nts id="Seg_998" n="HIAT:ip">.</nts>
                  <nts id="Seg_999" n="HIAT:ip">"</nts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T253" id="Seg_1002" n="HIAT:u" s="T251">
                  <nts id="Seg_1003" n="HIAT:ip">"</nts>
                  <ts e="T252" id="Seg_1005" n="HIAT:w" s="T251">No</ts>
                  <nts id="Seg_1006" n="HIAT:ip">,</nts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_1009" n="HIAT:w" s="T252">iʔ</ts>
                  <nts id="Seg_1010" n="HIAT:ip">"</nts>
                  <nts id="Seg_1011" n="HIAT:ip">.</nts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T259" id="Seg_1014" n="HIAT:u" s="T253">
                  <ts e="T254" id="Seg_1016" n="HIAT:w" s="T253">Dĭgəttə</ts>
                  <nts id="Seg_1017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_1019" n="HIAT:w" s="T254">dĭʔnə</ts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_1022" n="HIAT:w" s="T255">mĭbi</ts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_1025" n="HIAT:w" s="T256">kalečka</ts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_1028" n="HIAT:w" s="T257">i</ts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_1031" n="HIAT:w" s="T258">kornɨs</ts>
                  <nts id="Seg_1032" n="HIAT:ip">.</nts>
                  <nts id="Seg_1033" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T264" id="Seg_1035" n="HIAT:u" s="T259">
                  <ts e="T260" id="Seg_1037" n="HIAT:w" s="T259">Dĭgəttə</ts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_1040" n="HIAT:w" s="T260">tibi</ts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1043" n="HIAT:w" s="T261">šobi</ts>
                  <nts id="Seg_1044" n="HIAT:ip">,</nts>
                  <nts id="Seg_1045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1047" n="HIAT:w" s="T262">amorzittə</ts>
                  <nts id="Seg_1048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_1050" n="HIAT:w" s="T263">amnobi</ts>
                  <nts id="Seg_1051" n="HIAT:ip">.</nts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T271" id="Seg_1054" n="HIAT:u" s="T264">
                  <ts e="T265" id="Seg_1056" n="HIAT:w" s="T264">Dĭ</ts>
                  <nts id="Seg_1057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1059" n="HIAT:w" s="T265">dĭʔnə</ts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1062" n="HIAT:w" s="T266">kaplʼaʔi</ts>
                  <nts id="Seg_1063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1065" n="HIAT:w" s="T267">bazoʔ</ts>
                  <nts id="Seg_1066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1068" n="HIAT:w" s="T268">mĭbi</ts>
                  <nts id="Seg_1069" n="HIAT:ip">,</nts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1072" n="HIAT:w" s="T269">dĭ</ts>
                  <nts id="Seg_1073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1075" n="HIAT:w" s="T270">kunolluʔpi</ts>
                  <nts id="Seg_1076" n="HIAT:ip">.</nts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T279" id="Seg_1079" n="HIAT:u" s="T271">
                  <ts e="T272" id="Seg_1081" n="HIAT:w" s="T271">Dĭ</ts>
                  <nts id="Seg_1082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1084" n="HIAT:w" s="T272">bazoʔ</ts>
                  <nts id="Seg_1085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1087" n="HIAT:w" s="T273">turanə</ts>
                  <nts id="Seg_1088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1090" n="HIAT:w" s="T274">embi</ts>
                  <nts id="Seg_1091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1093" n="HIAT:w" s="T275">dĭm</ts>
                  <nts id="Seg_1094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1096" n="HIAT:w" s="T276">i</ts>
                  <nts id="Seg_1097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1099" n="HIAT:w" s="T277">dĭm</ts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1102" n="HIAT:w" s="T278">oʔlubi</ts>
                  <nts id="Seg_1103" n="HIAT:ip">.</nts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T284" id="Seg_1106" n="HIAT:u" s="T279">
                  <ts e="T280" id="Seg_1108" n="HIAT:w" s="T279">Dĭ</ts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1111" n="HIAT:w" s="T280">dĭn</ts>
                  <nts id="Seg_1112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1114" n="HIAT:w" s="T281">kambi</ts>
                  <nts id="Seg_1115" n="HIAT:ip">,</nts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1118" n="HIAT:w" s="T282">bazoʔ</ts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1121" n="HIAT:w" s="T283">dʼorbi</ts>
                  <nts id="Seg_1122" n="HIAT:ip">.</nts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T289" id="Seg_1125" n="HIAT:u" s="T284">
                  <ts e="T285" id="Seg_1127" n="HIAT:w" s="T284">Dĭm</ts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1130" n="HIAT:w" s="T285">bar</ts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1133" n="HIAT:w" s="T286">uʔbdlia</ts>
                  <nts id="Seg_1134" n="HIAT:ip">,</nts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1137" n="HIAT:w" s="T287">dʼildlie</ts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1140" n="HIAT:w" s="T288">dĭm</ts>
                  <nts id="Seg_1141" n="HIAT:ip">.</nts>
                  <nts id="Seg_1142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T294" id="Seg_1144" n="HIAT:u" s="T289">
                  <ts e="T290" id="Seg_1146" n="HIAT:w" s="T289">Dʼorbi</ts>
                  <nts id="Seg_1147" n="HIAT:ip">,</nts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1150" n="HIAT:w" s="T290">dʼorbi</ts>
                  <nts id="Seg_1151" n="HIAT:ip">,</nts>
                  <nts id="Seg_1152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1154" n="HIAT:w" s="T291">kajeldə</ts>
                  <nts id="Seg_1155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1157" n="HIAT:w" s="T292">saʔməluʔpi</ts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1160" n="HIAT:w" s="T293">kadəlgəndə</ts>
                  <nts id="Seg_1161" n="HIAT:ip">.</nts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T297" id="Seg_1164" n="HIAT:u" s="T294">
                  <ts e="T295" id="Seg_1166" n="HIAT:w" s="T294">Dĭ</ts>
                  <nts id="Seg_1167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1169" n="HIAT:w" s="T295">bar</ts>
                  <nts id="Seg_1170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1172" n="HIAT:w" s="T296">šüʔdərluʔpi</ts>
                  <nts id="Seg_1173" n="HIAT:ip">.</nts>
                  <nts id="Seg_1174" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T300" id="Seg_1176" n="HIAT:u" s="T297">
                  <ts e="T298" id="Seg_1178" n="HIAT:w" s="T297">Dĭgəttə</ts>
                  <nts id="Seg_1179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1180" n="HIAT:ip">(</nts>
                  <ts e="T299" id="Seg_1182" n="HIAT:w" s="T298">kamno-</ts>
                  <nts id="Seg_1183" n="HIAT:ip">)</nts>
                  <nts id="Seg_1184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1186" n="HIAT:w" s="T299">kamroluʔpiʔi</ts>
                  <nts id="Seg_1187" n="HIAT:ip">.</nts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T305" id="Seg_1190" n="HIAT:u" s="T300">
                  <ts e="T301" id="Seg_1192" n="HIAT:w" s="T300">Dĭgəttə</ts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1195" n="HIAT:w" s="T301">ertə</ts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1198" n="HIAT:w" s="T302">molambi</ts>
                  <nts id="Seg_1199" n="HIAT:ip">,</nts>
                  <nts id="Seg_1200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1202" n="HIAT:w" s="T303">dĭ</ts>
                  <nts id="Seg_1203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1205" n="HIAT:w" s="T304">šobi</ts>
                  <nts id="Seg_1206" n="HIAT:ip">.</nts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T309" id="Seg_1209" n="HIAT:u" s="T305">
                  <nts id="Seg_1210" n="HIAT:ip">"</nts>
                  <ts e="T306" id="Seg_1212" n="HIAT:w" s="T305">A</ts>
                  <nts id="Seg_1213" n="HIAT:ip">,</nts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1216" n="HIAT:w" s="T306">tăn</ts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1219" n="HIAT:w" s="T307">măna</ts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1222" n="HIAT:w" s="T308">sadarbial</ts>
                  <nts id="Seg_1223" n="HIAT:ip">?</nts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T319" id="Seg_1226" n="HIAT:u" s="T309">
                  <ts e="T310" id="Seg_1228" n="HIAT:w" s="T309">Dak</ts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1231" n="HIAT:w" s="T310">tăn</ts>
                  <nts id="Seg_1232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1233" n="HIAT:ip">(</nts>
                  <ts e="T312" id="Seg_1235" n="HIAT:w" s="T311">măn=</ts>
                  <nts id="Seg_1236" n="HIAT:ip">)</nts>
                  <nts id="Seg_1237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1239" n="HIAT:w" s="T312">ej</ts>
                  <nts id="Seg_1240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1242" n="HIAT:w" s="T313">măn</ts>
                  <nts id="Seg_1243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1245" n="HIAT:w" s="T314">nem</ts>
                  <nts id="Seg_1246" n="HIAT:ip">,</nts>
                  <nts id="Seg_1247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1249" n="HIAT:w" s="T315">a</ts>
                  <nts id="Seg_1250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1252" n="HIAT:w" s="T316">dö</ts>
                  <nts id="Seg_1253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1255" n="HIAT:w" s="T317">măn</ts>
                  <nts id="Seg_1256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1258" n="HIAT:w" s="T318">nem</ts>
                  <nts id="Seg_1259" n="HIAT:ip">"</nts>
                  <nts id="Seg_1260" n="HIAT:ip">.</nts>
                  <nts id="Seg_1261" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T327" id="Seg_1263" n="HIAT:u" s="T319">
                  <ts e="T320" id="Seg_1265" n="HIAT:w" s="T319">Dĭgəttə</ts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1268" n="HIAT:w" s="T320">poldə</ts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1270" n="HIAT:ip">(</nts>
                  <ts e="T322" id="Seg_1272" n="HIAT:w" s="T321">saʔməluʔpibeʔ=</ts>
                  <nts id="Seg_1273" n="HIAT:ip">)</nts>
                  <nts id="Seg_1274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1276" n="HIAT:w" s="T322">saʔməluʔpiʔi</ts>
                  <nts id="Seg_1277" n="HIAT:ip">,</nts>
                  <nts id="Seg_1278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1280" n="HIAT:w" s="T323">süjözi</ts>
                  <nts id="Seg_1281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1283" n="HIAT:w" s="T324">molaʔpiʔi</ts>
                  <nts id="Seg_1284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1286" n="HIAT:w" s="T325">i</ts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1289" n="HIAT:w" s="T326">nʼergöluʔpiʔi</ts>
                  <nts id="Seg_1290" n="HIAT:ip">.</nts>
                  <nts id="Seg_1291" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T328" id="Seg_1293" n="HIAT:u" s="T327">
                  <nts id="Seg_1294" n="HIAT:ip">(</nts>
                  <ts e="T328" id="Seg_1296" n="HIAT:w" s="T327">Bart</ts>
                  <nts id="Seg_1297" n="HIAT:ip">)</nts>
                  <nts id="Seg_1298" n="HIAT:ip">.</nts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T328" id="Seg_1300" n="sc" s="T0">
               <ts e="T1" id="Seg_1302" n="e" s="T0">Dĭgəttə </ts>
               <ts e="T2" id="Seg_1304" n="e" s="T1">dĭ </ts>
               <ts e="T3" id="Seg_1306" n="e" s="T2">kambi, </ts>
               <ts e="T4" id="Seg_1308" n="e" s="T3">kluboktə </ts>
               <ts e="T5" id="Seg_1310" n="e" s="T4">baruʔpi. </ts>
               <ts e="T6" id="Seg_1312" n="e" s="T5">Gibər </ts>
               <ts e="T7" id="Seg_1314" n="e" s="T6">dĭ </ts>
               <ts e="T8" id="Seg_1316" n="e" s="T7">kaləj, </ts>
               <ts e="T9" id="Seg_1318" n="e" s="T8">i </ts>
               <ts e="T10" id="Seg_1320" n="e" s="T9">dĭ </ts>
               <ts e="T11" id="Seg_1322" n="e" s="T10">dĭʔnə </ts>
               <ts e="T12" id="Seg_1324" n="e" s="T11">(ka- </ts>
               <ts e="T13" id="Seg_1326" n="e" s="T12">dĭʔnə </ts>
               <ts e="T14" id="Seg_1328" n="e" s="T13">kalə- </ts>
               <ts e="T15" id="Seg_1330" n="e" s="T14">kan-) </ts>
               <ts e="T16" id="Seg_1332" n="e" s="T15">kandəga. </ts>
               <ts e="T17" id="Seg_1334" n="e" s="T16">Kluboktə </ts>
               <ts e="T18" id="Seg_1336" n="e" s="T17">nuʔməbi, </ts>
               <ts e="T19" id="Seg_1338" n="e" s="T18">nuʔməbi, </ts>
               <ts e="T20" id="Seg_1340" n="e" s="T19">i </ts>
               <ts e="T21" id="Seg_1342" n="e" s="T20">deʔpi </ts>
               <ts e="T22" id="Seg_1344" n="e" s="T21">dĭbər </ts>
               <ts e="T23" id="Seg_1346" n="e" s="T22">gijen </ts>
               <ts e="T24" id="Seg_1348" n="e" s="T23">dĭn </ts>
               <ts e="T25" id="Seg_1350" n="e" s="T24">tibi </ts>
               <ts e="T26" id="Seg_1352" n="e" s="T25">amnolaʔbə. </ts>
               <ts e="T27" id="Seg_1354" n="e" s="T26">(Dĭ=) </ts>
               <ts e="T28" id="Seg_1356" n="e" s="T27">Dĭn </ts>
               <ts e="T29" id="Seg_1358" n="e" s="T28">už </ts>
               <ts e="T30" id="Seg_1360" n="e" s="T29">ne </ts>
               <ts e="T31" id="Seg_1362" n="e" s="T30">ige. </ts>
               <ts e="T32" id="Seg_1364" n="e" s="T31">Pimnie </ts>
               <ts e="T33" id="Seg_1366" n="e" s="T32">kanzittə, </ts>
               <ts e="T34" id="Seg_1368" n="e" s="T33">dĭgəttə </ts>
               <ts e="T35" id="Seg_1370" n="e" s="T34">nʼiʔnen </ts>
               <ts e="T36" id="Seg_1372" n="e" s="T35">(am-) </ts>
               <ts e="T37" id="Seg_1374" n="e" s="T36">amnobi. </ts>
               <ts e="T38" id="Seg_1376" n="e" s="T37">I </ts>
               <ts e="T39" id="Seg_1378" n="e" s="T38">takšebə </ts>
               <ts e="T40" id="Seg_1380" n="e" s="T39">nuldəbi </ts>
               <ts e="T41" id="Seg_1382" n="e" s="T40">i </ts>
               <ts e="T42" id="Seg_1384" n="e" s="T41">munəjʔtə </ts>
               <ts e="T43" id="Seg_1386" n="e" s="T42">(emdəbi) </ts>
               <ts e="T44" id="Seg_1388" n="e" s="T43">i </ts>
               <ts e="T45" id="Seg_1390" n="e" s="T44">bar </ts>
               <ts e="T46" id="Seg_1392" n="e" s="T45">sʼarla </ts>
               <ts e="T47" id="Seg_1394" n="e" s="T46">amnolaʔbə. </ts>
               <ts e="T48" id="Seg_1396" n="e" s="T47">A </ts>
               <ts e="T49" id="Seg_1398" n="e" s="T48">dĭ, </ts>
               <ts e="T50" id="Seg_1400" n="e" s="T49">tibin </ts>
               <ts e="T51" id="Seg_1402" n="e" s="T50">net: </ts>
               <ts e="T52" id="Seg_1404" n="e" s="T51">"Sadardə </ts>
               <ts e="T53" id="Seg_1406" n="e" s="T52">măna". </ts>
               <ts e="T54" id="Seg_1408" n="e" s="T53">A </ts>
               <ts e="T55" id="Seg_1410" n="e" s="T54">dĭ </ts>
               <ts e="T56" id="Seg_1412" n="e" s="T55">(măna=) </ts>
               <ts e="T57" id="Seg_1414" n="e" s="T56">măndə: </ts>
               <ts e="T58" id="Seg_1416" n="e" s="T57">"Mĭləl </ts>
               <ts e="T59" id="Seg_1418" n="e" s="T58">tăn </ts>
               <ts e="T60" id="Seg_1420" n="e" s="T59">tibi </ts>
               <ts e="T61" id="Seg_1422" n="e" s="T60">măna </ts>
               <ts e="T62" id="Seg_1424" n="e" s="T61">kunolzittə?" </ts>
               <ts e="T64" id="Seg_1426" n="e" s="T62">((BRK)) Dĭ </ts>
               <ts e="T65" id="Seg_1428" n="e" s="T64">dĭʔnə </ts>
               <ts e="T66" id="Seg_1430" n="e" s="T65">mĭbi. </ts>
               <ts e="T67" id="Seg_1432" n="e" s="T66">"Kunolaʔ, </ts>
               <ts e="T68" id="Seg_1434" n="e" s="T67">ĭmbi </ts>
               <ts e="T69" id="Seg_1436" n="e" s="T68">măna, </ts>
               <ts e="T70" id="Seg_1438" n="e" s="T69">ajiriam </ts>
               <ts e="T71" id="Seg_1440" n="e" s="T70">što_li?" </ts>
               <ts e="T72" id="Seg_1442" n="e" s="T71">Dĭ </ts>
               <ts e="T73" id="Seg_1444" n="e" s="T72">šobi </ts>
               <ts e="T74" id="Seg_1446" n="e" s="T73">maːʔndə. </ts>
               <ts e="T75" id="Seg_1448" n="e" s="T74">Amnobi </ts>
               <ts e="T76" id="Seg_1450" n="e" s="T75">amorzittə. </ts>
               <ts e="T77" id="Seg_1452" n="e" s="T76">Dĭ </ts>
               <ts e="T78" id="Seg_1454" n="e" s="T77">dĭʔnə </ts>
               <ts e="T79" id="Seg_1456" n="e" s="T78">(mĭ-) </ts>
               <ts e="T80" id="Seg_1458" n="e" s="T79">mĭbi </ts>
               <ts e="T81" id="Seg_1460" n="e" s="T80">kaplʼaʔi. </ts>
               <ts e="T82" id="Seg_1462" n="e" s="T81">Dĭ </ts>
               <ts e="T83" id="Seg_1464" n="e" s="T82">kunolluʔpi. </ts>
               <ts e="T84" id="Seg_1466" n="e" s="T83">Dĭ </ts>
               <ts e="T85" id="Seg_1468" n="e" s="T84">dĭm </ts>
               <ts e="T86" id="Seg_1470" n="e" s="T85">kumbi </ts>
               <ts e="T87" id="Seg_1472" n="e" s="T86">dĭbər </ts>
               <ts e="T88" id="Seg_1474" n="e" s="T87">gijen </ts>
               <ts e="T89" id="Seg_1476" n="e" s="T88">kunollia. </ts>
               <ts e="T90" id="Seg_1478" n="e" s="T89">Dĭgəttə </ts>
               <ts e="T91" id="Seg_1480" n="e" s="T90">dĭm </ts>
               <ts e="T92" id="Seg_1482" n="e" s="T91">dĭbər </ts>
               <ts e="T93" id="Seg_1484" n="e" s="T92">öʔlubi. </ts>
               <ts e="T94" id="Seg_1486" n="e" s="T93">Dĭ </ts>
               <ts e="T95" id="Seg_1488" n="e" s="T94">bar </ts>
               <ts e="T96" id="Seg_1490" n="e" s="T95">amnobi, </ts>
               <ts e="T97" id="Seg_1492" n="e" s="T96">amnobi, </ts>
               <ts e="T98" id="Seg_1494" n="e" s="T97">kürümbi, </ts>
               <ts e="T99" id="Seg_1496" n="e" s="T98">kürümbi, </ts>
               <ts e="T100" id="Seg_1498" n="e" s="T99">dʼăbaktərbi, </ts>
               <ts e="T101" id="Seg_1500" n="e" s="T100">măndə: </ts>
               <ts e="T102" id="Seg_1502" n="e" s="T101">"Uʔbdaʔ!" </ts>
               <ts e="T103" id="Seg_1504" n="e" s="T102">A </ts>
               <ts e="T104" id="Seg_1506" n="e" s="T103">dĭ </ts>
               <ts e="T105" id="Seg_1508" n="e" s="T104">uge </ts>
               <ts e="T106" id="Seg_1510" n="e" s="T105">ej </ts>
               <ts e="T107" id="Seg_1512" n="e" s="T106">uʔblia, </ts>
               <ts e="T108" id="Seg_1514" n="e" s="T107">kunollaʔbə </ts>
               <ts e="T109" id="Seg_1516" n="e" s="T108">tăŋ. </ts>
               <ts e="T110" id="Seg_1518" n="e" s="T109">Dĭgəttə </ts>
               <ts e="T111" id="Seg_1520" n="e" s="T110">ertə </ts>
               <ts e="T112" id="Seg_1522" n="e" s="T111">molambi. </ts>
               <ts e="T113" id="Seg_1524" n="e" s="T112">Dĭ </ts>
               <ts e="T114" id="Seg_1526" n="e" s="T113">(kajbear). </ts>
               <ts e="T115" id="Seg_1528" n="e" s="T114">"Soʔ </ts>
               <ts e="T116" id="Seg_1530" n="e" s="T115">döbər!" </ts>
               <ts e="T117" id="Seg_1532" n="e" s="T116">Dĭ </ts>
               <ts e="T118" id="Seg_1534" n="e" s="T117">šobi, </ts>
               <ts e="T329" id="Seg_1536" n="e" s="T118">kalla </ts>
               <ts e="T119" id="Seg_1538" n="e" s="T329">dʼürbi. </ts>
               <ts e="T120" id="Seg_1540" n="e" s="T119">Dĭgəttə </ts>
               <ts e="T121" id="Seg_1542" n="e" s="T120">dĭ </ts>
               <ts e="T122" id="Seg_1544" n="e" s="T121">uʔbdəbi. </ts>
               <ts e="T123" id="Seg_1546" n="e" s="T122">Dĭgəttə </ts>
               <ts e="T124" id="Seg_1548" n="e" s="T123">bazoʔ </ts>
               <ts e="T125" id="Seg_1550" n="e" s="T124">amnolaʔbə </ts>
               <ts e="T126" id="Seg_1552" n="e" s="T125">i </ts>
               <ts e="T127" id="Seg_1554" n="e" s="T126">sʼarlaʔbə. </ts>
               <ts e="T128" id="Seg_1556" n="e" s="T127">(Am-) </ts>
               <ts e="T129" id="Seg_1558" n="e" s="T128">Amnobi, </ts>
               <ts e="T130" id="Seg_1560" n="e" s="T129">sedendən </ts>
               <ts e="T131" id="Seg_1562" n="e" s="T130">tondə. </ts>
               <ts e="T132" id="Seg_1564" n="e" s="T131">Dĭgəttə </ts>
               <ts e="T133" id="Seg_1566" n="e" s="T132">davaj </ts>
               <ts e="T134" id="Seg_1568" n="e" s="T133">ererzittə </ts>
               <ts e="T135" id="Seg_1570" n="e" s="T134">dĭ </ts>
               <ts e="T136" id="Seg_1572" n="e" s="T135">kudʼelʼam. </ts>
               <ts e="T137" id="Seg_1574" n="e" s="T136">Ererleʔbə, </ts>
               <ts e="T138" id="Seg_1576" n="e" s="T137">ererleʔbə, </ts>
               <ts e="T139" id="Seg_1578" n="e" s="T138">(dĭn=) </ts>
               <ts e="T140" id="Seg_1580" n="e" s="T139">dĭ </ts>
               <ts e="T141" id="Seg_1582" n="e" s="T140">ne </ts>
               <ts e="T142" id="Seg_1584" n="e" s="T141">bazoʔ, </ts>
               <ts e="T143" id="Seg_1586" n="e" s="T142">tibin </ts>
               <ts e="T144" id="Seg_1588" n="e" s="T143">ne </ts>
               <ts e="T145" id="Seg_1590" n="e" s="T144">šobi. </ts>
               <ts e="T146" id="Seg_1592" n="e" s="T145">"Sadardə </ts>
               <ts e="T147" id="Seg_1594" n="e" s="T146">măna </ts>
               <ts e="T148" id="Seg_1596" n="e" s="T147">dĭ". </ts>
               <ts e="T149" id="Seg_1598" n="e" s="T148">"Dʼok, </ts>
               <ts e="T150" id="Seg_1600" n="e" s="T149">măn </ts>
               <ts e="T151" id="Seg_1602" n="e" s="T150">ej </ts>
               <ts e="T152" id="Seg_1604" n="e" s="T151">sadarlam. </ts>
               <ts e="T153" id="Seg_1606" n="e" s="T152">Tibim </ts>
               <ts e="T154" id="Seg_1608" n="e" s="T153">(mĭne-) </ts>
               <ts e="T155" id="Seg_1610" n="e" s="T154">mĭləl </ts>
               <ts e="T156" id="Seg_1612" n="e" s="T155">măna </ts>
               <ts e="T157" id="Seg_1614" n="e" s="T156">nüdʼi </ts>
               <ts e="T158" id="Seg_1616" n="e" s="T157">kunolzittə. </ts>
               <ts e="T159" id="Seg_1618" n="e" s="T158">Dĭgəttə </ts>
               <ts e="T160" id="Seg_1620" n="e" s="T159">mĭlim". </ts>
               <ts e="T161" id="Seg_1622" n="e" s="T160">"Nu </ts>
               <ts e="T162" id="Seg_1624" n="e" s="T161">it, </ts>
               <ts e="T163" id="Seg_1626" n="e" s="T162">mĭləm". </ts>
               <ts e="T165" id="Seg_1628" n="e" s="T163">((BRK)) Dĭgəttə </ts>
               <ts e="T166" id="Seg_1630" n="e" s="T165">dĭ </ts>
               <ts e="T167" id="Seg_1632" n="e" s="T166">šobi, </ts>
               <ts e="T168" id="Seg_1634" n="e" s="T167">dĭ </ts>
               <ts e="T169" id="Seg_1636" n="e" s="T168">bazoʔ </ts>
               <ts e="T170" id="Seg_1638" n="e" s="T169">amorzittə </ts>
               <ts e="T171" id="Seg_1640" n="e" s="T170">amnobi. </ts>
               <ts e="T172" id="Seg_1642" n="e" s="T171">Dĭ </ts>
               <ts e="T173" id="Seg_1644" n="e" s="T172">dĭʔnə </ts>
               <ts e="T174" id="Seg_1646" n="e" s="T173">bazoʔ </ts>
               <ts e="T175" id="Seg_1648" n="e" s="T174">kaplʼaʔi </ts>
               <ts e="T176" id="Seg_1650" n="e" s="T175">mĭbi. </ts>
               <ts e="T177" id="Seg_1652" n="e" s="T176">Dĭ </ts>
               <ts e="T178" id="Seg_1654" n="e" s="T177">kunolluʔpi, </ts>
               <ts e="T179" id="Seg_1656" n="e" s="T178">dĭ </ts>
               <ts e="T180" id="Seg_1658" n="e" s="T179">dĭm </ts>
               <ts e="T181" id="Seg_1660" n="e" s="T180">turanə </ts>
               <ts e="T182" id="Seg_1662" n="e" s="T181">kambi, </ts>
               <ts e="T183" id="Seg_1664" n="e" s="T182">gijen </ts>
               <ts e="T184" id="Seg_1666" n="e" s="T183">kunollaʔbə. </ts>
               <ts e="T185" id="Seg_1668" n="e" s="T184">"Kanaʔ, </ts>
               <ts e="T186" id="Seg_1670" n="e" s="T185">kunollaʔ!" </ts>
               <ts e="T187" id="Seg_1672" n="e" s="T186">Dĭ </ts>
               <ts e="T188" id="Seg_1674" n="e" s="T187">dĭbər </ts>
               <ts e="T189" id="Seg_1676" n="e" s="T188">kambi. </ts>
               <ts e="T190" id="Seg_1678" n="e" s="T189">(Dĭm=) </ts>
               <ts e="T191" id="Seg_1680" n="e" s="T190">Kürimbi </ts>
               <ts e="T192" id="Seg_1682" n="e" s="T191">dĭʔnə: </ts>
               <ts e="T193" id="Seg_1684" n="e" s="T192">"Uʔbdaʔ, </ts>
               <ts e="T194" id="Seg_1686" n="e" s="T193">uʔbdaʔ". </ts>
               <ts e="T196" id="Seg_1688" n="e" s="T194">Dĭgəttə </ts>
               <ts e="T197" id="Seg_1690" n="e" s="T196">kădedə </ts>
               <ts e="T198" id="Seg_1692" n="e" s="T197">ej </ts>
               <ts e="T199" id="Seg_1694" n="e" s="T198">molia </ts>
               <ts e="T200" id="Seg_1696" n="e" s="T199">uʔbsittə. </ts>
               <ts e="T201" id="Seg_1698" n="e" s="T200">Dĭgəttə </ts>
               <ts e="T202" id="Seg_1700" n="e" s="T201">ertə </ts>
               <ts e="T203" id="Seg_1702" n="e" s="T202">molambi. </ts>
               <ts e="T204" id="Seg_1704" n="e" s="T203">Dĭ </ts>
               <ts e="T205" id="Seg_1706" n="e" s="T204">măndə: </ts>
               <ts e="T206" id="Seg_1708" n="e" s="T205">"Kanaʔ! </ts>
               <ts e="T207" id="Seg_1710" n="e" s="T206">Kabarləj </ts>
               <ts e="T208" id="Seg_1712" n="e" s="T207">tănan </ts>
               <ts e="T209" id="Seg_1714" n="e" s="T208">dön </ts>
               <ts e="T210" id="Seg_1716" n="e" s="T209">amnozittə". </ts>
               <ts e="T211" id="Seg_1718" n="e" s="T210">Dĭgəttə </ts>
               <ts e="T212" id="Seg_1720" n="e" s="T211">dĭ </ts>
               <ts e="T213" id="Seg_1722" n="e" s="T212">kambi. </ts>
               <ts e="T214" id="Seg_1724" n="e" s="T213">Dĭ </ts>
               <ts e="T215" id="Seg_1726" n="e" s="T214">uʔbdəbi. </ts>
               <ts e="T216" id="Seg_1728" n="e" s="T215">Dĭgəttə </ts>
               <ts e="T217" id="Seg_1730" n="e" s="T216">bazoʔ </ts>
               <ts e="T218" id="Seg_1732" n="e" s="T217">(šeže-) </ts>
               <ts e="T219" id="Seg_1734" n="e" s="T218">(sezeŋgən) </ts>
               <ts e="T220" id="Seg_1736" n="e" s="T219">amnolaʔbə. </ts>
               <ts e="T221" id="Seg_1738" n="e" s="T220">Kornɨš </ts>
               <ts e="T222" id="Seg_1740" n="e" s="T221">nuldəbi, </ts>
               <ts e="T223" id="Seg_1742" n="e" s="T222">kuvas. </ts>
               <ts e="T224" id="Seg_1744" n="e" s="T223">Kolʼečkə zălatoj </ts>
               <ts e="T225" id="Seg_1746" n="e" s="T224">(uda- </ts>
               <ts e="T226" id="Seg_1748" n="e" s="T225">udandə=) </ts>
               <ts e="T227" id="Seg_1750" n="e" s="T226">udandə </ts>
               <ts e="T228" id="Seg_1752" n="e" s="T227">šerie, </ts>
               <ts e="T229" id="Seg_1754" n="e" s="T228">to </ts>
               <ts e="T230" id="Seg_1756" n="e" s="T229">dö </ts>
               <ts e="T231" id="Seg_1758" n="e" s="T230">udandə, </ts>
               <ts e="T232" id="Seg_1760" n="e" s="T231">to </ts>
               <ts e="T233" id="Seg_1762" n="e" s="T232">dö </ts>
               <ts e="T235" id="Seg_1764" n="e" s="T233">udandə. </ts>
               <ts e="T236" id="Seg_1766" n="e" s="T235">Dĭ </ts>
               <ts e="T237" id="Seg_1768" n="e" s="T236">kulaʔpi </ts>
               <ts e="T238" id="Seg_1770" n="e" s="T237">da </ts>
               <ts e="T239" id="Seg_1772" n="e" s="T238">kambi. </ts>
               <ts e="T240" id="Seg_1774" n="e" s="T239">"Sadardə </ts>
               <ts e="T241" id="Seg_1776" n="e" s="T240">măna!" </ts>
               <ts e="T242" id="Seg_1778" n="e" s="T241">"Dʼok, </ts>
               <ts e="T243" id="Seg_1780" n="e" s="T242">măn </ts>
               <ts e="T244" id="Seg_1782" n="e" s="T243">ej </ts>
               <ts e="T245" id="Seg_1784" n="e" s="T244">sadariam. </ts>
               <ts e="T246" id="Seg_1786" n="e" s="T245">Tibi </ts>
               <ts e="T247" id="Seg_1788" n="e" s="T246">mĭʔ </ts>
               <ts e="T248" id="Seg_1790" n="e" s="T247">măna </ts>
               <ts e="T249" id="Seg_1792" n="e" s="T248">teinen </ts>
               <ts e="T250" id="Seg_1794" n="e" s="T249">šazittə, </ts>
               <ts e="T251" id="Seg_1796" n="e" s="T250">kunolzittə." </ts>
               <ts e="T252" id="Seg_1798" n="e" s="T251">"No, </ts>
               <ts e="T253" id="Seg_1800" n="e" s="T252">iʔ". </ts>
               <ts e="T254" id="Seg_1802" n="e" s="T253">Dĭgəttə </ts>
               <ts e="T255" id="Seg_1804" n="e" s="T254">dĭʔnə </ts>
               <ts e="T256" id="Seg_1806" n="e" s="T255">mĭbi </ts>
               <ts e="T257" id="Seg_1808" n="e" s="T256">kalečka </ts>
               <ts e="T258" id="Seg_1810" n="e" s="T257">i </ts>
               <ts e="T259" id="Seg_1812" n="e" s="T258">kornɨs. </ts>
               <ts e="T260" id="Seg_1814" n="e" s="T259">Dĭgəttə </ts>
               <ts e="T261" id="Seg_1816" n="e" s="T260">tibi </ts>
               <ts e="T262" id="Seg_1818" n="e" s="T261">šobi, </ts>
               <ts e="T263" id="Seg_1820" n="e" s="T262">amorzittə </ts>
               <ts e="T264" id="Seg_1822" n="e" s="T263">amnobi. </ts>
               <ts e="T265" id="Seg_1824" n="e" s="T264">Dĭ </ts>
               <ts e="T266" id="Seg_1826" n="e" s="T265">dĭʔnə </ts>
               <ts e="T267" id="Seg_1828" n="e" s="T266">kaplʼaʔi </ts>
               <ts e="T268" id="Seg_1830" n="e" s="T267">bazoʔ </ts>
               <ts e="T269" id="Seg_1832" n="e" s="T268">mĭbi, </ts>
               <ts e="T270" id="Seg_1834" n="e" s="T269">dĭ </ts>
               <ts e="T271" id="Seg_1836" n="e" s="T270">kunolluʔpi. </ts>
               <ts e="T272" id="Seg_1838" n="e" s="T271">Dĭ </ts>
               <ts e="T273" id="Seg_1840" n="e" s="T272">bazoʔ </ts>
               <ts e="T274" id="Seg_1842" n="e" s="T273">turanə </ts>
               <ts e="T275" id="Seg_1844" n="e" s="T274">embi </ts>
               <ts e="T276" id="Seg_1846" n="e" s="T275">dĭm </ts>
               <ts e="T277" id="Seg_1848" n="e" s="T276">i </ts>
               <ts e="T278" id="Seg_1850" n="e" s="T277">dĭm </ts>
               <ts e="T279" id="Seg_1852" n="e" s="T278">oʔlubi. </ts>
               <ts e="T280" id="Seg_1854" n="e" s="T279">Dĭ </ts>
               <ts e="T281" id="Seg_1856" n="e" s="T280">dĭn </ts>
               <ts e="T282" id="Seg_1858" n="e" s="T281">kambi, </ts>
               <ts e="T283" id="Seg_1860" n="e" s="T282">bazoʔ </ts>
               <ts e="T284" id="Seg_1862" n="e" s="T283">dʼorbi. </ts>
               <ts e="T285" id="Seg_1864" n="e" s="T284">Dĭm </ts>
               <ts e="T286" id="Seg_1866" n="e" s="T285">bar </ts>
               <ts e="T287" id="Seg_1868" n="e" s="T286">uʔbdlia, </ts>
               <ts e="T288" id="Seg_1870" n="e" s="T287">dʼildlie </ts>
               <ts e="T289" id="Seg_1872" n="e" s="T288">dĭm. </ts>
               <ts e="T290" id="Seg_1874" n="e" s="T289">Dʼorbi, </ts>
               <ts e="T291" id="Seg_1876" n="e" s="T290">dʼorbi, </ts>
               <ts e="T292" id="Seg_1878" n="e" s="T291">kajeldə </ts>
               <ts e="T293" id="Seg_1880" n="e" s="T292">saʔməluʔpi </ts>
               <ts e="T294" id="Seg_1882" n="e" s="T293">kadəlgəndə. </ts>
               <ts e="T295" id="Seg_1884" n="e" s="T294">Dĭ </ts>
               <ts e="T296" id="Seg_1886" n="e" s="T295">bar </ts>
               <ts e="T297" id="Seg_1888" n="e" s="T296">šüʔdərluʔpi. </ts>
               <ts e="T298" id="Seg_1890" n="e" s="T297">Dĭgəttə </ts>
               <ts e="T299" id="Seg_1892" n="e" s="T298">(kamno-) </ts>
               <ts e="T300" id="Seg_1894" n="e" s="T299">kamroluʔpiʔi. </ts>
               <ts e="T301" id="Seg_1896" n="e" s="T300">Dĭgəttə </ts>
               <ts e="T302" id="Seg_1898" n="e" s="T301">ertə </ts>
               <ts e="T303" id="Seg_1900" n="e" s="T302">molambi, </ts>
               <ts e="T304" id="Seg_1902" n="e" s="T303">dĭ </ts>
               <ts e="T305" id="Seg_1904" n="e" s="T304">šobi. </ts>
               <ts e="T306" id="Seg_1906" n="e" s="T305">"A, </ts>
               <ts e="T307" id="Seg_1908" n="e" s="T306">tăn </ts>
               <ts e="T308" id="Seg_1910" n="e" s="T307">măna </ts>
               <ts e="T309" id="Seg_1912" n="e" s="T308">sadarbial? </ts>
               <ts e="T310" id="Seg_1914" n="e" s="T309">Dak </ts>
               <ts e="T311" id="Seg_1916" n="e" s="T310">tăn </ts>
               <ts e="T312" id="Seg_1918" n="e" s="T311">(măn=) </ts>
               <ts e="T313" id="Seg_1920" n="e" s="T312">ej </ts>
               <ts e="T314" id="Seg_1922" n="e" s="T313">măn </ts>
               <ts e="T315" id="Seg_1924" n="e" s="T314">nem, </ts>
               <ts e="T316" id="Seg_1926" n="e" s="T315">a </ts>
               <ts e="T317" id="Seg_1928" n="e" s="T316">dö </ts>
               <ts e="T318" id="Seg_1930" n="e" s="T317">măn </ts>
               <ts e="T319" id="Seg_1932" n="e" s="T318">nem". </ts>
               <ts e="T320" id="Seg_1934" n="e" s="T319">Dĭgəttə </ts>
               <ts e="T321" id="Seg_1936" n="e" s="T320">poldə </ts>
               <ts e="T322" id="Seg_1938" n="e" s="T321">(saʔməluʔpibeʔ=) </ts>
               <ts e="T323" id="Seg_1940" n="e" s="T322">saʔməluʔpiʔi, </ts>
               <ts e="T324" id="Seg_1942" n="e" s="T323">süjözi </ts>
               <ts e="T325" id="Seg_1944" n="e" s="T324">molaʔpiʔi </ts>
               <ts e="T326" id="Seg_1946" n="e" s="T325">i </ts>
               <ts e="T327" id="Seg_1948" n="e" s="T326">nʼergöluʔpiʔi. </ts>
               <ts e="T328" id="Seg_1950" n="e" s="T327">(Bart). </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_1951" s="T0">PKZ_196X_Finist_continuation_flk.001 (001)</ta>
            <ta e="T16" id="Seg_1952" s="T5">PKZ_196X_Finist_continuation_flk.002 (002)</ta>
            <ta e="T26" id="Seg_1953" s="T16">PKZ_196X_Finist_continuation_flk.003 (003)</ta>
            <ta e="T31" id="Seg_1954" s="T26">PKZ_196X_Finist_continuation_flk.004 (004)</ta>
            <ta e="T37" id="Seg_1955" s="T31">PKZ_196X_Finist_continuation_flk.005 (005)</ta>
            <ta e="T47" id="Seg_1956" s="T37">PKZ_196X_Finist_continuation_flk.006 (006)</ta>
            <ta e="T53" id="Seg_1957" s="T47">PKZ_196X_Finist_continuation_flk.007 (007)</ta>
            <ta e="T62" id="Seg_1958" s="T53">PKZ_196X_Finist_continuation_flk.008 (008)</ta>
            <ta e="T66" id="Seg_1959" s="T62">PKZ_196X_Finist_continuation_flk.009 (010) </ta>
            <ta e="T71" id="Seg_1960" s="T66">PKZ_196X_Finist_continuation_flk.010 (012)</ta>
            <ta e="T74" id="Seg_1961" s="T71">PKZ_196X_Finist_continuation_flk.011 (013)</ta>
            <ta e="T76" id="Seg_1962" s="T74">PKZ_196X_Finist_continuation_flk.012 (014)</ta>
            <ta e="T81" id="Seg_1963" s="T76">PKZ_196X_Finist_continuation_flk.013 (015)</ta>
            <ta e="T83" id="Seg_1964" s="T81">PKZ_196X_Finist_continuation_flk.014 (016)</ta>
            <ta e="T89" id="Seg_1965" s="T83">PKZ_196X_Finist_continuation_flk.015 (017)</ta>
            <ta e="T93" id="Seg_1966" s="T89">PKZ_196X_Finist_continuation_flk.016 (018)</ta>
            <ta e="T102" id="Seg_1967" s="T93">PKZ_196X_Finist_continuation_flk.017 (019)</ta>
            <ta e="T109" id="Seg_1968" s="T102">PKZ_196X_Finist_continuation_flk.018 (020)</ta>
            <ta e="T112" id="Seg_1969" s="T109">PKZ_196X_Finist_continuation_flk.019 (021)</ta>
            <ta e="T114" id="Seg_1970" s="T112">PKZ_196X_Finist_continuation_flk.020 (022)</ta>
            <ta e="T116" id="Seg_1971" s="T114">PKZ_196X_Finist_continuation_flk.021 (023)</ta>
            <ta e="T119" id="Seg_1972" s="T116">PKZ_196X_Finist_continuation_flk.022 (024)</ta>
            <ta e="T122" id="Seg_1973" s="T119">PKZ_196X_Finist_continuation_flk.023 (025)</ta>
            <ta e="T127" id="Seg_1974" s="T122">PKZ_196X_Finist_continuation_flk.024 (026)</ta>
            <ta e="T131" id="Seg_1975" s="T127">PKZ_196X_Finist_continuation_flk.025 (027)</ta>
            <ta e="T136" id="Seg_1976" s="T131">PKZ_196X_Finist_continuation_flk.026 (028)</ta>
            <ta e="T145" id="Seg_1977" s="T136">PKZ_196X_Finist_continuation_flk.027 (029)</ta>
            <ta e="T148" id="Seg_1978" s="T145">PKZ_196X_Finist_continuation_flk.028 (030)</ta>
            <ta e="T152" id="Seg_1979" s="T148">PKZ_196X_Finist_continuation_flk.029 (031)</ta>
            <ta e="T158" id="Seg_1980" s="T152">PKZ_196X_Finist_continuation_flk.030 (032)</ta>
            <ta e="T160" id="Seg_1981" s="T158">PKZ_196X_Finist_continuation_flk.031 (033)</ta>
            <ta e="T163" id="Seg_1982" s="T160">PKZ_196X_Finist_continuation_flk.032 (034)</ta>
            <ta e="T171" id="Seg_1983" s="T163">PKZ_196X_Finist_continuation_flk.033 (035) </ta>
            <ta e="T176" id="Seg_1984" s="T171">PKZ_196X_Finist_continuation_flk.034 (037)</ta>
            <ta e="T184" id="Seg_1985" s="T176">PKZ_196X_Finist_continuation_flk.035 (038)</ta>
            <ta e="T186" id="Seg_1986" s="T184">PKZ_196X_Finist_continuation_flk.036 (039)</ta>
            <ta e="T189" id="Seg_1987" s="T186">PKZ_196X_Finist_continuation_flk.037 (040)</ta>
            <ta e="T194" id="Seg_1988" s="T189">PKZ_196X_Finist_continuation_flk.038 (041)</ta>
            <ta e="T200" id="Seg_1989" s="T194">PKZ_196X_Finist_continuation_flk.039 (042)</ta>
            <ta e="T203" id="Seg_1990" s="T200">PKZ_196X_Finist_continuation_flk.040 (043)</ta>
            <ta e="T206" id="Seg_1991" s="T203">PKZ_196X_Finist_continuation_flk.041 (044)</ta>
            <ta e="T210" id="Seg_1992" s="T206">PKZ_196X_Finist_continuation_flk.042 (045)</ta>
            <ta e="T213" id="Seg_1993" s="T210">PKZ_196X_Finist_continuation_flk.043 (046)</ta>
            <ta e="T215" id="Seg_1994" s="T213">PKZ_196X_Finist_continuation_flk.044 (047)</ta>
            <ta e="T220" id="Seg_1995" s="T215">PKZ_196X_Finist_continuation_flk.045 (048)</ta>
            <ta e="T223" id="Seg_1996" s="T220">PKZ_196X_Finist_continuation_flk.046 (049)</ta>
            <ta e="T235" id="Seg_1997" s="T223">PKZ_196X_Finist_continuation_flk.047 (050)</ta>
            <ta e="T239" id="Seg_1998" s="T235">PKZ_196X_Finist_continuation_flk.048 (051)</ta>
            <ta e="T241" id="Seg_1999" s="T239">PKZ_196X_Finist_continuation_flk.049 (052)</ta>
            <ta e="T245" id="Seg_2000" s="T241">PKZ_196X_Finist_continuation_flk.050 (053)</ta>
            <ta e="T251" id="Seg_2001" s="T245">PKZ_196X_Finist_continuation_flk.051 (054)</ta>
            <ta e="T253" id="Seg_2002" s="T251">PKZ_196X_Finist_continuation_flk.052 (055)</ta>
            <ta e="T259" id="Seg_2003" s="T253">PKZ_196X_Finist_continuation_flk.053 (056)</ta>
            <ta e="T264" id="Seg_2004" s="T259">PKZ_196X_Finist_continuation_flk.054 (057)</ta>
            <ta e="T271" id="Seg_2005" s="T264">PKZ_196X_Finist_continuation_flk.055 (058)</ta>
            <ta e="T279" id="Seg_2006" s="T271">PKZ_196X_Finist_continuation_flk.056 (059)</ta>
            <ta e="T284" id="Seg_2007" s="T279">PKZ_196X_Finist_continuation_flk.057 (060)</ta>
            <ta e="T289" id="Seg_2008" s="T284">PKZ_196X_Finist_continuation_flk.058 (061)</ta>
            <ta e="T294" id="Seg_2009" s="T289">PKZ_196X_Finist_continuation_flk.059 (062)</ta>
            <ta e="T297" id="Seg_2010" s="T294">PKZ_196X_Finist_continuation_flk.060 (063)</ta>
            <ta e="T300" id="Seg_2011" s="T297">PKZ_196X_Finist_continuation_flk.061 (064)</ta>
            <ta e="T305" id="Seg_2012" s="T300">PKZ_196X_Finist_continuation_flk.062 (065)</ta>
            <ta e="T309" id="Seg_2013" s="T305">PKZ_196X_Finist_continuation_flk.063 (066)</ta>
            <ta e="T319" id="Seg_2014" s="T309">PKZ_196X_Finist_continuation_flk.064 (067)</ta>
            <ta e="T327" id="Seg_2015" s="T319">PKZ_196X_Finist_continuation_flk.065 (068)</ta>
            <ta e="T328" id="Seg_2016" s="T327">PKZ_196X_Finist_continuation_flk.066 (069)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_2017" s="T0">Dĭgəttə dĭ kambi, kluboktə baruʔpi. </ta>
            <ta e="T16" id="Seg_2018" s="T5">Gibər dĭ kaləj, i dĭ dĭʔnə (ka- dĭʔnə kalə- kan-) kandəga. </ta>
            <ta e="T26" id="Seg_2019" s="T16">Kluboktə nuʔməbi, nuʔməbi, i deʔpi dĭbər gijen tĭn tibi amnolaʔbə. </ta>
            <ta e="T31" id="Seg_2020" s="T26">(Dĭ=) Dĭn už ne ige. </ta>
            <ta e="T37" id="Seg_2021" s="T31">Pimnie kanzittə, dĭgəttə miʔnen (am-) amnobi. </ta>
            <ta e="T47" id="Seg_2022" s="T37">I takšebə nuldəbi i munəjʔtə (emdəbi) i bar sʼarla amnolaʔbə. </ta>
            <ta e="T53" id="Seg_2023" s="T47">A dĭ, tibin net: "Sadardə măna". </ta>
            <ta e="T62" id="Seg_2024" s="T53">A dĭ (măna=) măndə: "Mĭləl tăn tibi măna kunolzittə?" </ta>
            <ta e="T66" id="Seg_2025" s="T62">((BRK)) Dĭ dĭʔnə mĭbi. </ta>
            <ta e="T71" id="Seg_2026" s="T66">"Kunolaʔ, ĭmbi măna, ajiriam što li?" </ta>
            <ta e="T74" id="Seg_2027" s="T71">Dĭ šobi maːʔndə. </ta>
            <ta e="T76" id="Seg_2028" s="T74">Amnobi amorzittə. </ta>
            <ta e="T81" id="Seg_2029" s="T76">Dĭ dĭʔnə (mĭ-) mĭbi kaplʼaʔi. </ta>
            <ta e="T83" id="Seg_2030" s="T81">Dĭ kunolluʔpi. </ta>
            <ta e="T89" id="Seg_2031" s="T83">Dĭ dĭm kumbi dĭbər gijen kunollia. </ta>
            <ta e="T93" id="Seg_2032" s="T89">Dĭgəttə dĭm dĭbər öʔlubi. </ta>
            <ta e="T102" id="Seg_2033" s="T93">Dĭ bar amnobi, amnobi, kürümbi, kürümbi, dʼăbaktərbi, măndə: "Uʔbdaʔ!". </ta>
            <ta e="T109" id="Seg_2034" s="T102">A dĭ uge ej uʔblia, kunollaʔbə tăŋ. </ta>
            <ta e="T112" id="Seg_2035" s="T109">Dĭgəttə ertə molambi. </ta>
            <ta e="T114" id="Seg_2036" s="T112">Dĭ (kajbear). </ta>
            <ta e="T116" id="Seg_2037" s="T114">"Soʔ döbər!" </ta>
            <ta e="T119" id="Seg_2038" s="T116">Dĭ šobi, kalla dʼürbi. </ta>
            <ta e="T122" id="Seg_2039" s="T119">Dĭgəttə dĭ uʔbdəbi. </ta>
            <ta e="T127" id="Seg_2040" s="T122">Dĭgəttə bazoʔ amnolaʔbə i sʼarlaʔbə. </ta>
            <ta e="T131" id="Seg_2041" s="T127">(Am-) Amnobi, (sedendən) tondə. </ta>
            <ta e="T136" id="Seg_2042" s="T131">Dĭgəttə davaj ererzittə dĭ kugʼelʼam. </ta>
            <ta e="T145" id="Seg_2043" s="T136">Ererleʔbə, ererleʔbə, (dĭn=) dĭ ne bazoʔ, tibin ne šobi. </ta>
            <ta e="T148" id="Seg_2044" s="T145">"Sadardə măna dĭ". </ta>
            <ta e="T152" id="Seg_2045" s="T148">"Dʼok, măn ej sadarlam. </ta>
            <ta e="T158" id="Seg_2046" s="T152">Tibim (mĭne-) mĭləl măna nüdʼi kunolzittə. </ta>
            <ta e="T160" id="Seg_2047" s="T158">Dĭgəttə mĭlim". </ta>
            <ta e="T163" id="Seg_2048" s="T160">"Nu it, mĭləm". </ta>
            <ta e="T171" id="Seg_2049" s="T163">((BRK)) Dĭgəttə dĭ šobi, dĭ bazoʔ amorzittə amnobi. </ta>
            <ta e="T176" id="Seg_2050" s="T171">Dĭ dĭʔnə bazoʔ kaplʼaʔi mĭbi. </ta>
            <ta e="T184" id="Seg_2051" s="T176">Dĭ kunolluʔpi, dĭ dĭm turanə kambi, gijen kunollaʔbə. </ta>
            <ta e="T186" id="Seg_2052" s="T184">"Kanaʔ, kunollaʔ!" </ta>
            <ta e="T189" id="Seg_2053" s="T186">Dĭ dĭbər kambi. </ta>
            <ta e="T194" id="Seg_2054" s="T189">(Dĭm=) Kürimbi dĭʔnə:" Uʔbdaʔ, uʔbdaʔ". </ta>
            <ta e="T200" id="Seg_2055" s="T194">Dĭgəttə kădedə ej molia uʔbsittə. </ta>
            <ta e="T203" id="Seg_2056" s="T200">Dĭgəttə ertə molambi. </ta>
            <ta e="T206" id="Seg_2057" s="T203">Dĭ măndə:" Kanaʔ! </ta>
            <ta e="T210" id="Seg_2058" s="T206">Kabarləj tănan dön amnozittə". </ta>
            <ta e="T213" id="Seg_2059" s="T210">Dĭgəttə dĭ kambi. </ta>
            <ta e="T215" id="Seg_2060" s="T213">Dĭ uʔbdəbi. </ta>
            <ta e="T220" id="Seg_2061" s="T215">Dĭgəttə bazoʔ (šeže-) (sezeŋgən) amnolaʔbə. </ta>
            <ta e="T223" id="Seg_2062" s="T220">Kornɨš nuldəbi, kuvas. </ta>
            <ta e="T235" id="Seg_2063" s="T223">Kolʼečkə zălatoj (uda- udandə=) udandə šerie, to dö udandə, to dö udandə. </ta>
            <ta e="T239" id="Seg_2064" s="T235">Dĭ kulaʔpi da kambi. </ta>
            <ta e="T241" id="Seg_2065" s="T239">"Sadardə măna!" </ta>
            <ta e="T245" id="Seg_2066" s="T241">"Dʼok, măn ej sadariam. </ta>
            <ta e="T251" id="Seg_2067" s="T245">Tibi mĭʔ măna teinem šazittə, kunolzittə." </ta>
            <ta e="T253" id="Seg_2068" s="T251">"No, iʔ". </ta>
            <ta e="T259" id="Seg_2069" s="T253">Dĭgəttə dĭʔnə mĭbi kalečka i kornɨs. </ta>
            <ta e="T264" id="Seg_2070" s="T259">Dĭgəttə tibi šobi, amorzittə amnobi. </ta>
            <ta e="T271" id="Seg_2071" s="T264">Dĭ dĭʔnə kaplʼaʔi bazoʔ mĭbi, dĭ kunolluʔpi. </ta>
            <ta e="T279" id="Seg_2072" s="T271">Dĭ bazoʔ turanə embi dĭm i dĭm oʔlubi. </ta>
            <ta e="T284" id="Seg_2073" s="T279">Dĭ dĭn kambi, bazoʔ dʼorbi. </ta>
            <ta e="T289" id="Seg_2074" s="T284">Dĭm bar uʔbdlia, dʼildlie dĭm. </ta>
            <ta e="T294" id="Seg_2075" s="T289">Dʼorbi, dʼorbi, kajeldə saʔməluʔpi kadəlgəndə. </ta>
            <ta e="T297" id="Seg_2076" s="T294">Dĭ bar šüʔdərluʔpi. </ta>
            <ta e="T300" id="Seg_2077" s="T297">Dĭgəttə (kamno-) kamroluʔpiʔi. </ta>
            <ta e="T305" id="Seg_2078" s="T300">Dĭgəttə ertə molambi, dĭ šobi. </ta>
            <ta e="T309" id="Seg_2079" s="T305">"A, tăn măna sadarbial? </ta>
            <ta e="T319" id="Seg_2080" s="T309">Dak tăn (măn=) ej măn nem, a dö măn nem". </ta>
            <ta e="T327" id="Seg_2081" s="T319">Dĭgəttə poldə (saʔməluʔpibeʔ=) saməluʔpiʔi, süjozi molaʔpiʔi i nʼergöluʔpiʔi. </ta>
            <ta e="T328" id="Seg_2082" s="T327">(Bart). </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_2083" s="T0">dĭgəttə</ta>
            <ta e="T2" id="Seg_2084" s="T1">dĭ</ta>
            <ta e="T3" id="Seg_2085" s="T2">kam-bi</ta>
            <ta e="T4" id="Seg_2086" s="T3">klubok-tə</ta>
            <ta e="T5" id="Seg_2087" s="T4">baruʔ-pi</ta>
            <ta e="T6" id="Seg_2088" s="T5">gibər</ta>
            <ta e="T7" id="Seg_2089" s="T6">dĭ</ta>
            <ta e="T8" id="Seg_2090" s="T7">ka-lə-j</ta>
            <ta e="T9" id="Seg_2091" s="T8">i</ta>
            <ta e="T10" id="Seg_2092" s="T9">dĭ</ta>
            <ta e="T11" id="Seg_2093" s="T10">dĭʔ-nə</ta>
            <ta e="T13" id="Seg_2094" s="T12">dĭʔ-nə</ta>
            <ta e="T15" id="Seg_2095" s="T14">kan-</ta>
            <ta e="T16" id="Seg_2096" s="T15">kandə-ga</ta>
            <ta e="T17" id="Seg_2097" s="T16">klubok-tə</ta>
            <ta e="T18" id="Seg_2098" s="T17">nuʔmə-bi</ta>
            <ta e="T19" id="Seg_2099" s="T18">nuʔmə-bi</ta>
            <ta e="T20" id="Seg_2100" s="T19">i</ta>
            <ta e="T21" id="Seg_2101" s="T20">deʔ-pi</ta>
            <ta e="T22" id="Seg_2102" s="T21">dĭbər</ta>
            <ta e="T23" id="Seg_2103" s="T22">gijen</ta>
            <ta e="T24" id="Seg_2104" s="T23">dĭ-n</ta>
            <ta e="T25" id="Seg_2105" s="T24">tibi</ta>
            <ta e="T26" id="Seg_2106" s="T25">amno-laʔbə</ta>
            <ta e="T27" id="Seg_2107" s="T26">dĭ</ta>
            <ta e="T28" id="Seg_2108" s="T27">dĭ-n</ta>
            <ta e="T29" id="Seg_2109" s="T28">už</ta>
            <ta e="T30" id="Seg_2110" s="T29">ne</ta>
            <ta e="T31" id="Seg_2111" s="T30">i-ge</ta>
            <ta e="T32" id="Seg_2112" s="T31">pim-nie</ta>
            <ta e="T33" id="Seg_2113" s="T32">kan-zittə</ta>
            <ta e="T34" id="Seg_2114" s="T33">dĭgəttə</ta>
            <ta e="T35" id="Seg_2115" s="T34">nʼiʔnen</ta>
            <ta e="T36" id="Seg_2116" s="T35">am-</ta>
            <ta e="T37" id="Seg_2117" s="T36">amno-bi</ta>
            <ta e="T38" id="Seg_2118" s="T37">i</ta>
            <ta e="T39" id="Seg_2119" s="T38">takše-bə</ta>
            <ta e="T40" id="Seg_2120" s="T39">nuldə-bi</ta>
            <ta e="T41" id="Seg_2121" s="T40">i</ta>
            <ta e="T42" id="Seg_2122" s="T41">munəj-ʔ-tə</ta>
            <ta e="T44" id="Seg_2123" s="T43">i</ta>
            <ta e="T45" id="Seg_2124" s="T44">bar</ta>
            <ta e="T46" id="Seg_2125" s="T45">sʼar-la</ta>
            <ta e="T47" id="Seg_2126" s="T46">amno-laʔbə</ta>
            <ta e="T48" id="Seg_2127" s="T47">a</ta>
            <ta e="T49" id="Seg_2128" s="T48">dĭ</ta>
            <ta e="T50" id="Seg_2129" s="T49">tibi-n</ta>
            <ta e="T51" id="Seg_2130" s="T50">ne-t</ta>
            <ta e="T52" id="Seg_2131" s="T51">sadar-də</ta>
            <ta e="T53" id="Seg_2132" s="T52">măna</ta>
            <ta e="T54" id="Seg_2133" s="T53">a</ta>
            <ta e="T55" id="Seg_2134" s="T54">dĭ</ta>
            <ta e="T56" id="Seg_2135" s="T55">măna</ta>
            <ta e="T57" id="Seg_2136" s="T56">măn-də</ta>
            <ta e="T58" id="Seg_2137" s="T57">mĭ-lə-l</ta>
            <ta e="T59" id="Seg_2138" s="T58">tăn</ta>
            <ta e="T60" id="Seg_2139" s="T59">tibi</ta>
            <ta e="T61" id="Seg_2140" s="T60">măna</ta>
            <ta e="T62" id="Seg_2141" s="T61">kunol-zittə</ta>
            <ta e="T64" id="Seg_2142" s="T62">dĭ</ta>
            <ta e="T65" id="Seg_2143" s="T64">dĭʔ-nə</ta>
            <ta e="T66" id="Seg_2144" s="T65">mĭ-bi</ta>
            <ta e="T67" id="Seg_2145" s="T66">kunol-a-ʔ</ta>
            <ta e="T68" id="Seg_2146" s="T67">ĭmbi</ta>
            <ta e="T69" id="Seg_2147" s="T68">măna</ta>
            <ta e="T70" id="Seg_2148" s="T69">ajir-ia-m</ta>
            <ta e="T71" id="Seg_2149" s="T70">štoli</ta>
            <ta e="T72" id="Seg_2150" s="T71">dĭ</ta>
            <ta e="T73" id="Seg_2151" s="T72">šo-bi</ta>
            <ta e="T74" id="Seg_2152" s="T73">maːʔ-ndə</ta>
            <ta e="T75" id="Seg_2153" s="T74">amno-bi</ta>
            <ta e="T76" id="Seg_2154" s="T75">amor-zittə</ta>
            <ta e="T77" id="Seg_2155" s="T76">dĭ</ta>
            <ta e="T78" id="Seg_2156" s="T77">dĭʔ-nə</ta>
            <ta e="T80" id="Seg_2157" s="T79">mĭ-bi</ta>
            <ta e="T81" id="Seg_2158" s="T80">kaplʼa-ʔi</ta>
            <ta e="T82" id="Seg_2159" s="T81">dĭ</ta>
            <ta e="T83" id="Seg_2160" s="T82">kunol-luʔ-pi</ta>
            <ta e="T84" id="Seg_2161" s="T83">dĭ</ta>
            <ta e="T85" id="Seg_2162" s="T84">dĭ-m</ta>
            <ta e="T86" id="Seg_2163" s="T85">kum-bi</ta>
            <ta e="T87" id="Seg_2164" s="T86">dĭbər</ta>
            <ta e="T88" id="Seg_2165" s="T87">gijen</ta>
            <ta e="T89" id="Seg_2166" s="T88">kunol-lia</ta>
            <ta e="T90" id="Seg_2167" s="T89">dĭgəttə</ta>
            <ta e="T91" id="Seg_2168" s="T90">dĭ-m</ta>
            <ta e="T92" id="Seg_2169" s="T91">dĭbər</ta>
            <ta e="T93" id="Seg_2170" s="T92">öʔlu-bi</ta>
            <ta e="T94" id="Seg_2171" s="T93">dĭ</ta>
            <ta e="T95" id="Seg_2172" s="T94">bar</ta>
            <ta e="T96" id="Seg_2173" s="T95">amno-bi</ta>
            <ta e="T97" id="Seg_2174" s="T96">amno-bi</ta>
            <ta e="T98" id="Seg_2175" s="T97">kürüm-bi</ta>
            <ta e="T99" id="Seg_2176" s="T98">kürüm-bi</ta>
            <ta e="T100" id="Seg_2177" s="T99">dʼăbaktər-bi</ta>
            <ta e="T101" id="Seg_2178" s="T100">măn-də</ta>
            <ta e="T102" id="Seg_2179" s="T101">uʔbda-ʔ</ta>
            <ta e="T103" id="Seg_2180" s="T102">a</ta>
            <ta e="T104" id="Seg_2181" s="T103">dĭ</ta>
            <ta e="T105" id="Seg_2182" s="T104">uge</ta>
            <ta e="T106" id="Seg_2183" s="T105">ej</ta>
            <ta e="T107" id="Seg_2184" s="T106">uʔb-lia</ta>
            <ta e="T108" id="Seg_2185" s="T107">kunol-laʔbə</ta>
            <ta e="T109" id="Seg_2186" s="T108">tăŋ</ta>
            <ta e="T110" id="Seg_2187" s="T109">dĭgəttə</ta>
            <ta e="T111" id="Seg_2188" s="T110">ertə</ta>
            <ta e="T112" id="Seg_2189" s="T111">mo-lam-bi</ta>
            <ta e="T113" id="Seg_2190" s="T112">dĭ</ta>
            <ta e="T115" id="Seg_2191" s="T114">so-ʔ</ta>
            <ta e="T116" id="Seg_2192" s="T115">döbər</ta>
            <ta e="T117" id="Seg_2193" s="T116">dĭ</ta>
            <ta e="T118" id="Seg_2194" s="T117">šo-bi</ta>
            <ta e="T329" id="Seg_2195" s="T118">kal-la</ta>
            <ta e="T119" id="Seg_2196" s="T329">dʼür-bi</ta>
            <ta e="T120" id="Seg_2197" s="T119">dĭgəttə</ta>
            <ta e="T121" id="Seg_2198" s="T120">dĭ</ta>
            <ta e="T122" id="Seg_2199" s="T121">uʔbdə-bi</ta>
            <ta e="T123" id="Seg_2200" s="T122">dĭgəttə</ta>
            <ta e="T124" id="Seg_2201" s="T123">bazoʔ</ta>
            <ta e="T125" id="Seg_2202" s="T124">amno-laʔbə</ta>
            <ta e="T126" id="Seg_2203" s="T125">i</ta>
            <ta e="T127" id="Seg_2204" s="T126">sʼar-laʔbə</ta>
            <ta e="T128" id="Seg_2205" s="T127">am-</ta>
            <ta e="T129" id="Seg_2206" s="T128">amno-bi</ta>
            <ta e="T130" id="Seg_2207" s="T129">seden-dən</ta>
            <ta e="T131" id="Seg_2208" s="T130">to-ndə</ta>
            <ta e="T132" id="Seg_2209" s="T131">dĭgəttə</ta>
            <ta e="T133" id="Seg_2210" s="T132">davaj</ta>
            <ta e="T134" id="Seg_2211" s="T133">erer-zittə</ta>
            <ta e="T135" id="Seg_2212" s="T134">dĭ</ta>
            <ta e="T136" id="Seg_2213" s="T135">kudʼelʼa-m</ta>
            <ta e="T137" id="Seg_2214" s="T136">erer-leʔbə</ta>
            <ta e="T138" id="Seg_2215" s="T137">erer-leʔbə</ta>
            <ta e="T139" id="Seg_2216" s="T138">dĭn</ta>
            <ta e="T140" id="Seg_2217" s="T139">dĭ</ta>
            <ta e="T141" id="Seg_2218" s="T140">ne</ta>
            <ta e="T142" id="Seg_2219" s="T141">bazoʔ</ta>
            <ta e="T143" id="Seg_2220" s="T142">tibi-n</ta>
            <ta e="T144" id="Seg_2221" s="T143">ne</ta>
            <ta e="T145" id="Seg_2222" s="T144">šo-bi</ta>
            <ta e="T146" id="Seg_2223" s="T145">sadar-də</ta>
            <ta e="T147" id="Seg_2224" s="T146">măna</ta>
            <ta e="T148" id="Seg_2225" s="T147">dĭ</ta>
            <ta e="T149" id="Seg_2226" s="T148">dʼok</ta>
            <ta e="T150" id="Seg_2227" s="T149">măn</ta>
            <ta e="T151" id="Seg_2228" s="T150">ej</ta>
            <ta e="T152" id="Seg_2229" s="T151">sadar-la-m</ta>
            <ta e="T153" id="Seg_2230" s="T152">tibi-m</ta>
            <ta e="T155" id="Seg_2231" s="T154">mĭ-lə-l</ta>
            <ta e="T156" id="Seg_2232" s="T155">măna</ta>
            <ta e="T157" id="Seg_2233" s="T156">nüdʼi</ta>
            <ta e="T158" id="Seg_2234" s="T157">kunol-zittə</ta>
            <ta e="T159" id="Seg_2235" s="T158">dĭgəttə</ta>
            <ta e="T160" id="Seg_2236" s="T159">mĭ-li-m</ta>
            <ta e="T161" id="Seg_2237" s="T160">nu</ta>
            <ta e="T162" id="Seg_2238" s="T161">i-t</ta>
            <ta e="T163" id="Seg_2239" s="T162">mĭ-lə-m</ta>
            <ta e="T165" id="Seg_2240" s="T163">dĭgəttə</ta>
            <ta e="T166" id="Seg_2241" s="T165">dĭ</ta>
            <ta e="T167" id="Seg_2242" s="T166">šo-bi</ta>
            <ta e="T168" id="Seg_2243" s="T167">dĭ</ta>
            <ta e="T169" id="Seg_2244" s="T168">bazoʔ</ta>
            <ta e="T170" id="Seg_2245" s="T169">amor-zittə</ta>
            <ta e="T171" id="Seg_2246" s="T170">amno-bi</ta>
            <ta e="T172" id="Seg_2247" s="T171">dĭ</ta>
            <ta e="T173" id="Seg_2248" s="T172">dĭʔ-nə</ta>
            <ta e="T174" id="Seg_2249" s="T173">bazoʔ</ta>
            <ta e="T175" id="Seg_2250" s="T174">kaplʼa-ʔi</ta>
            <ta e="T176" id="Seg_2251" s="T175">mĭ-bi</ta>
            <ta e="T177" id="Seg_2252" s="T176">dĭ</ta>
            <ta e="T178" id="Seg_2253" s="T177">kunol-luʔ-pi</ta>
            <ta e="T179" id="Seg_2254" s="T178">dĭ</ta>
            <ta e="T180" id="Seg_2255" s="T179">dĭ-m</ta>
            <ta e="T181" id="Seg_2256" s="T180">tura-nə</ta>
            <ta e="T182" id="Seg_2257" s="T181">kam-bi</ta>
            <ta e="T183" id="Seg_2258" s="T182">gijen</ta>
            <ta e="T184" id="Seg_2259" s="T183">kunol-laʔbə</ta>
            <ta e="T185" id="Seg_2260" s="T184">kan-a-ʔ</ta>
            <ta e="T186" id="Seg_2261" s="T185">kunol-laʔ</ta>
            <ta e="T187" id="Seg_2262" s="T186">dĭ</ta>
            <ta e="T188" id="Seg_2263" s="T187">dĭbər</ta>
            <ta e="T189" id="Seg_2264" s="T188">kam-bi</ta>
            <ta e="T190" id="Seg_2265" s="T189">dĭ-m</ta>
            <ta e="T192" id="Seg_2266" s="T191">dĭʔ-nə</ta>
            <ta e="T193" id="Seg_2267" s="T192">uʔbda-ʔ</ta>
            <ta e="T194" id="Seg_2268" s="T193">uʔbda-ʔ</ta>
            <ta e="T196" id="Seg_2269" s="T194">dĭgəttə</ta>
            <ta e="T197" id="Seg_2270" s="T196">kăde=də</ta>
            <ta e="T198" id="Seg_2271" s="T197">ej</ta>
            <ta e="T199" id="Seg_2272" s="T198">mo-lia</ta>
            <ta e="T200" id="Seg_2273" s="T199">uʔb-sittə</ta>
            <ta e="T201" id="Seg_2274" s="T200">dĭgəttə</ta>
            <ta e="T202" id="Seg_2275" s="T201">ertə</ta>
            <ta e="T203" id="Seg_2276" s="T202">mo-lam-bi</ta>
            <ta e="T204" id="Seg_2277" s="T203">dĭ</ta>
            <ta e="T205" id="Seg_2278" s="T204">măn-də</ta>
            <ta e="T206" id="Seg_2279" s="T205">kan-a-ʔ</ta>
            <ta e="T207" id="Seg_2280" s="T206">kabarləj</ta>
            <ta e="T208" id="Seg_2281" s="T207">tănan</ta>
            <ta e="T209" id="Seg_2282" s="T208">dön</ta>
            <ta e="T210" id="Seg_2283" s="T209">amno-zittə</ta>
            <ta e="T211" id="Seg_2284" s="T210">dĭgəttə</ta>
            <ta e="T212" id="Seg_2285" s="T211">dĭ</ta>
            <ta e="T213" id="Seg_2286" s="T212">kam-bi</ta>
            <ta e="T214" id="Seg_2287" s="T213">dĭ</ta>
            <ta e="T215" id="Seg_2288" s="T214">uʔbdə-bi</ta>
            <ta e="T216" id="Seg_2289" s="T215">dĭgəttə</ta>
            <ta e="T217" id="Seg_2290" s="T216">bazoʔ</ta>
            <ta e="T219" id="Seg_2291" s="T218">sezeŋ-gən</ta>
            <ta e="T220" id="Seg_2292" s="T219">amno-laʔbə</ta>
            <ta e="T221" id="Seg_2293" s="T220">kornɨš</ta>
            <ta e="T222" id="Seg_2294" s="T221">nuldə-bi</ta>
            <ta e="T223" id="Seg_2295" s="T222">kuvas</ta>
            <ta e="T226" id="Seg_2296" s="T225">uda-ndə</ta>
            <ta e="T227" id="Seg_2297" s="T226">uda-ndə</ta>
            <ta e="T228" id="Seg_2298" s="T227">šer-ie</ta>
            <ta e="T229" id="Seg_2299" s="T228">to</ta>
            <ta e="T230" id="Seg_2300" s="T229">dö</ta>
            <ta e="T231" id="Seg_2301" s="T230">uda-ndə</ta>
            <ta e="T232" id="Seg_2302" s="T231">to</ta>
            <ta e="T233" id="Seg_2303" s="T232">dö</ta>
            <ta e="T235" id="Seg_2304" s="T233">uda-ndə</ta>
            <ta e="T236" id="Seg_2305" s="T235">dĭ</ta>
            <ta e="T237" id="Seg_2306" s="T236">ku-laʔpi</ta>
            <ta e="T238" id="Seg_2307" s="T237">da</ta>
            <ta e="T239" id="Seg_2308" s="T238">kam-bi</ta>
            <ta e="T240" id="Seg_2309" s="T239">sadar-də</ta>
            <ta e="T241" id="Seg_2310" s="T240">măna</ta>
            <ta e="T242" id="Seg_2311" s="T241">dʼok</ta>
            <ta e="T243" id="Seg_2312" s="T242">măn</ta>
            <ta e="T244" id="Seg_2313" s="T243">ej</ta>
            <ta e="T245" id="Seg_2314" s="T244">sadar-ia-m</ta>
            <ta e="T246" id="Seg_2315" s="T245">tibi</ta>
            <ta e="T247" id="Seg_2316" s="T246">mĭ-ʔ</ta>
            <ta e="T248" id="Seg_2317" s="T247">măna</ta>
            <ta e="T249" id="Seg_2318" s="T248">teinen</ta>
            <ta e="T250" id="Seg_2319" s="T249">ša-zittə</ta>
            <ta e="T251" id="Seg_2320" s="T250">kunol-zittə</ta>
            <ta e="T252" id="Seg_2321" s="T251">no</ta>
            <ta e="T253" id="Seg_2322" s="T252">i-ʔ</ta>
            <ta e="T254" id="Seg_2323" s="T253">dĭgəttə</ta>
            <ta e="T255" id="Seg_2324" s="T254">dĭʔ-nə</ta>
            <ta e="T256" id="Seg_2325" s="T255">mĭ-bi</ta>
            <ta e="T257" id="Seg_2326" s="T256">kalečka</ta>
            <ta e="T258" id="Seg_2327" s="T257">i</ta>
            <ta e="T259" id="Seg_2328" s="T258">kornɨs</ta>
            <ta e="T260" id="Seg_2329" s="T259">dĭgəttə</ta>
            <ta e="T261" id="Seg_2330" s="T260">tibi</ta>
            <ta e="T262" id="Seg_2331" s="T261">šo-bi</ta>
            <ta e="T263" id="Seg_2332" s="T262">amor-zittə</ta>
            <ta e="T264" id="Seg_2333" s="T263">amno-bi</ta>
            <ta e="T265" id="Seg_2334" s="T264">dĭ</ta>
            <ta e="T266" id="Seg_2335" s="T265">dĭʔ-nə</ta>
            <ta e="T267" id="Seg_2336" s="T266">kaplʼa-ʔi</ta>
            <ta e="T268" id="Seg_2337" s="T267">bazoʔ</ta>
            <ta e="T269" id="Seg_2338" s="T268">mĭ-bi</ta>
            <ta e="T270" id="Seg_2339" s="T269">dĭ</ta>
            <ta e="T271" id="Seg_2340" s="T270">kunol-luʔ-pi</ta>
            <ta e="T272" id="Seg_2341" s="T271">dĭ</ta>
            <ta e="T273" id="Seg_2342" s="T272">bazoʔ</ta>
            <ta e="T274" id="Seg_2343" s="T273">tura-nə</ta>
            <ta e="T275" id="Seg_2344" s="T274">em-bi</ta>
            <ta e="T276" id="Seg_2345" s="T275">dĭ-m</ta>
            <ta e="T277" id="Seg_2346" s="T276">i</ta>
            <ta e="T278" id="Seg_2347" s="T277">dĭ-m</ta>
            <ta e="T279" id="Seg_2348" s="T278">oʔlu-bi</ta>
            <ta e="T280" id="Seg_2349" s="T279">dĭ</ta>
            <ta e="T281" id="Seg_2350" s="T280">dĭn</ta>
            <ta e="T282" id="Seg_2351" s="T281">kam-bi</ta>
            <ta e="T283" id="Seg_2352" s="T282">bazoʔ</ta>
            <ta e="T284" id="Seg_2353" s="T283">dʼor-bi</ta>
            <ta e="T285" id="Seg_2354" s="T284">dĭ-m</ta>
            <ta e="T286" id="Seg_2355" s="T285">bar</ta>
            <ta e="T287" id="Seg_2356" s="T286">uʔbd-lia</ta>
            <ta e="T288" id="Seg_2357" s="T287">dʼild-lie</ta>
            <ta e="T289" id="Seg_2358" s="T288">dĭ-m</ta>
            <ta e="T290" id="Seg_2359" s="T289">dʼor-bi</ta>
            <ta e="T291" id="Seg_2360" s="T290">dʼor-bi</ta>
            <ta e="T292" id="Seg_2361" s="T291">kajel-də</ta>
            <ta e="T293" id="Seg_2362" s="T292">saʔmə-luʔ-pi</ta>
            <ta e="T294" id="Seg_2363" s="T293">kadəl-gəndə</ta>
            <ta e="T295" id="Seg_2364" s="T294">dĭ</ta>
            <ta e="T296" id="Seg_2365" s="T295">bar</ta>
            <ta e="T297" id="Seg_2366" s="T296">šüʔdər-luʔ-pi</ta>
            <ta e="T298" id="Seg_2367" s="T297">dĭgəttə</ta>
            <ta e="T300" id="Seg_2368" s="T299">kamro-luʔ-pi-ʔi</ta>
            <ta e="T301" id="Seg_2369" s="T300">dĭgəttə</ta>
            <ta e="T302" id="Seg_2370" s="T301">ertə</ta>
            <ta e="T303" id="Seg_2371" s="T302">mo-lam-bi</ta>
            <ta e="T304" id="Seg_2372" s="T303">dĭ</ta>
            <ta e="T305" id="Seg_2373" s="T304">šo-bi</ta>
            <ta e="T306" id="Seg_2374" s="T305">a</ta>
            <ta e="T307" id="Seg_2375" s="T306">tăn</ta>
            <ta e="T308" id="Seg_2376" s="T307">măna</ta>
            <ta e="T309" id="Seg_2377" s="T308">sadar-bia-l</ta>
            <ta e="T310" id="Seg_2378" s="T309">dak</ta>
            <ta e="T311" id="Seg_2379" s="T310">tăn</ta>
            <ta e="T312" id="Seg_2380" s="T311">măn</ta>
            <ta e="T313" id="Seg_2381" s="T312">ej</ta>
            <ta e="T314" id="Seg_2382" s="T313">măn</ta>
            <ta e="T315" id="Seg_2383" s="T314">ne-m</ta>
            <ta e="T316" id="Seg_2384" s="T315">a</ta>
            <ta e="T317" id="Seg_2385" s="T316">dö</ta>
            <ta e="T318" id="Seg_2386" s="T317">măn</ta>
            <ta e="T319" id="Seg_2387" s="T318">ne-m</ta>
            <ta e="T320" id="Seg_2388" s="T319">dĭgəttə</ta>
            <ta e="T321" id="Seg_2389" s="T320">pol-də</ta>
            <ta e="T322" id="Seg_2390" s="T321">saʔmə-luʔ-pi-beʔ</ta>
            <ta e="T323" id="Seg_2391" s="T322">saʔmə-luʔ-pi-ʔi</ta>
            <ta e="T324" id="Seg_2392" s="T323">süjö-zi</ta>
            <ta e="T325" id="Seg_2393" s="T324">mo-laʔ-pi-ʔi</ta>
            <ta e="T326" id="Seg_2394" s="T325">i</ta>
            <ta e="T327" id="Seg_2395" s="T326">nʼergö-luʔ-pi-ʔi</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_2396" s="T0">dĭgəttə</ta>
            <ta e="T2" id="Seg_2397" s="T1">dĭ</ta>
            <ta e="T3" id="Seg_2398" s="T2">kan-bi</ta>
            <ta e="T4" id="Seg_2399" s="T3">klubok-də</ta>
            <ta e="T5" id="Seg_2400" s="T4">barəʔ-bi</ta>
            <ta e="T6" id="Seg_2401" s="T5">gibər</ta>
            <ta e="T7" id="Seg_2402" s="T6">dĭ</ta>
            <ta e="T8" id="Seg_2403" s="T7">kan-lV-j</ta>
            <ta e="T9" id="Seg_2404" s="T8">i</ta>
            <ta e="T10" id="Seg_2405" s="T9">dĭ</ta>
            <ta e="T11" id="Seg_2406" s="T10">dĭ-Tə</ta>
            <ta e="T13" id="Seg_2407" s="T12">dĭ-Tə</ta>
            <ta e="T15" id="Seg_2408" s="T14">kan-</ta>
            <ta e="T16" id="Seg_2409" s="T15">kandə-gA</ta>
            <ta e="T17" id="Seg_2410" s="T16">klubok-də</ta>
            <ta e="T18" id="Seg_2411" s="T17">nuʔmə-bi</ta>
            <ta e="T19" id="Seg_2412" s="T18">nuʔmə-bi</ta>
            <ta e="T20" id="Seg_2413" s="T19">i</ta>
            <ta e="T21" id="Seg_2414" s="T20">det-bi</ta>
            <ta e="T22" id="Seg_2415" s="T21">dĭbər</ta>
            <ta e="T23" id="Seg_2416" s="T22">gijen</ta>
            <ta e="T24" id="Seg_2417" s="T23">dĭ-n</ta>
            <ta e="T25" id="Seg_2418" s="T24">tibi</ta>
            <ta e="T26" id="Seg_2419" s="T25">amno-laʔbə</ta>
            <ta e="T27" id="Seg_2420" s="T26">dĭ</ta>
            <ta e="T28" id="Seg_2421" s="T27">dĭ-n</ta>
            <ta e="T29" id="Seg_2422" s="T28">už</ta>
            <ta e="T30" id="Seg_2423" s="T29">ne</ta>
            <ta e="T31" id="Seg_2424" s="T30">i-gA</ta>
            <ta e="T32" id="Seg_2425" s="T31">pim-liA</ta>
            <ta e="T33" id="Seg_2426" s="T32">kan-zittə</ta>
            <ta e="T34" id="Seg_2427" s="T33">dĭgəttə</ta>
            <ta e="T35" id="Seg_2428" s="T34">nʼiʔnen</ta>
            <ta e="T36" id="Seg_2429" s="T35">am-</ta>
            <ta e="T37" id="Seg_2430" s="T36">am-bi</ta>
            <ta e="T38" id="Seg_2431" s="T37">i</ta>
            <ta e="T39" id="Seg_2432" s="T38">takše-bə</ta>
            <ta e="T40" id="Seg_2433" s="T39">nuldə-bi</ta>
            <ta e="T41" id="Seg_2434" s="T40">i</ta>
            <ta e="T42" id="Seg_2435" s="T41">munəj-jəʔ-Tə</ta>
            <ta e="T44" id="Seg_2436" s="T43">i</ta>
            <ta e="T45" id="Seg_2437" s="T44">bar</ta>
            <ta e="T46" id="Seg_2438" s="T45">sʼar-lAʔ</ta>
            <ta e="T47" id="Seg_2439" s="T46">amno-laʔbə</ta>
            <ta e="T48" id="Seg_2440" s="T47">a</ta>
            <ta e="T49" id="Seg_2441" s="T48">dĭ</ta>
            <ta e="T50" id="Seg_2442" s="T49">tibi-n</ta>
            <ta e="T51" id="Seg_2443" s="T50">ne-t</ta>
            <ta e="T52" id="Seg_2444" s="T51">sădar-t</ta>
            <ta e="T53" id="Seg_2445" s="T52">măna</ta>
            <ta e="T54" id="Seg_2446" s="T53">a</ta>
            <ta e="T55" id="Seg_2447" s="T54">dĭ</ta>
            <ta e="T56" id="Seg_2448" s="T55">măna</ta>
            <ta e="T57" id="Seg_2449" s="T56">măn-ntə</ta>
            <ta e="T58" id="Seg_2450" s="T57">mĭ-lV-l</ta>
            <ta e="T59" id="Seg_2451" s="T58">tăn</ta>
            <ta e="T60" id="Seg_2452" s="T59">tibi</ta>
            <ta e="T61" id="Seg_2453" s="T60">măna</ta>
            <ta e="T62" id="Seg_2454" s="T61">kunol-zittə</ta>
            <ta e="T64" id="Seg_2455" s="T62">dĭ</ta>
            <ta e="T65" id="Seg_2456" s="T64">dĭ-Tə</ta>
            <ta e="T66" id="Seg_2457" s="T65">mĭ-bi</ta>
            <ta e="T67" id="Seg_2458" s="T66">kunol-ə-ʔ</ta>
            <ta e="T68" id="Seg_2459" s="T67">ĭmbi</ta>
            <ta e="T69" id="Seg_2460" s="T68">măna</ta>
            <ta e="T70" id="Seg_2461" s="T69">ajir-liA-m</ta>
            <ta e="T71" id="Seg_2462" s="T70">štoli</ta>
            <ta e="T72" id="Seg_2463" s="T71">dĭ</ta>
            <ta e="T73" id="Seg_2464" s="T72">šo-bi</ta>
            <ta e="T74" id="Seg_2465" s="T73">maʔ-gəndə</ta>
            <ta e="T75" id="Seg_2466" s="T74">amnə-bi</ta>
            <ta e="T76" id="Seg_2467" s="T75">amor-zittə</ta>
            <ta e="T77" id="Seg_2468" s="T76">dĭ</ta>
            <ta e="T78" id="Seg_2469" s="T77">dĭ-Tə</ta>
            <ta e="T80" id="Seg_2470" s="T79">mĭ-bi</ta>
            <ta e="T81" id="Seg_2471" s="T80">kaplʼa-jəʔ</ta>
            <ta e="T82" id="Seg_2472" s="T81">dĭ</ta>
            <ta e="T83" id="Seg_2473" s="T82">kunol-luʔbdə-bi</ta>
            <ta e="T84" id="Seg_2474" s="T83">dĭ</ta>
            <ta e="T85" id="Seg_2475" s="T84">dĭ-m</ta>
            <ta e="T86" id="Seg_2476" s="T85">kun-bi</ta>
            <ta e="T87" id="Seg_2477" s="T86">dĭbər</ta>
            <ta e="T88" id="Seg_2478" s="T87">gijen</ta>
            <ta e="T89" id="Seg_2479" s="T88">kunol-liA</ta>
            <ta e="T90" id="Seg_2480" s="T89">dĭgəttə</ta>
            <ta e="T91" id="Seg_2481" s="T90">dĭ-m</ta>
            <ta e="T92" id="Seg_2482" s="T91">dĭbər</ta>
            <ta e="T93" id="Seg_2483" s="T92">öʔlu-bi</ta>
            <ta e="T94" id="Seg_2484" s="T93">dĭ</ta>
            <ta e="T95" id="Seg_2485" s="T94">bar</ta>
            <ta e="T96" id="Seg_2486" s="T95">amno-bi</ta>
            <ta e="T97" id="Seg_2487" s="T96">amno-bi</ta>
            <ta e="T98" id="Seg_2488" s="T97">kürüm-bi</ta>
            <ta e="T99" id="Seg_2489" s="T98">kürüm-bi</ta>
            <ta e="T100" id="Seg_2490" s="T99">tʼăbaktər-bi</ta>
            <ta e="T101" id="Seg_2491" s="T100">măn-ntə</ta>
            <ta e="T102" id="Seg_2492" s="T101">uʔbdə-ʔ</ta>
            <ta e="T103" id="Seg_2493" s="T102">a</ta>
            <ta e="T104" id="Seg_2494" s="T103">dĭ</ta>
            <ta e="T105" id="Seg_2495" s="T104">üge</ta>
            <ta e="T106" id="Seg_2496" s="T105">ej</ta>
            <ta e="T107" id="Seg_2497" s="T106">uʔbdə-liA</ta>
            <ta e="T108" id="Seg_2498" s="T107">kunol-laʔbə</ta>
            <ta e="T109" id="Seg_2499" s="T108">tăŋ</ta>
            <ta e="T110" id="Seg_2500" s="T109">dĭgəttə</ta>
            <ta e="T111" id="Seg_2501" s="T110">ertə</ta>
            <ta e="T112" id="Seg_2502" s="T111">mo-laːm-bi</ta>
            <ta e="T113" id="Seg_2503" s="T112">dĭ</ta>
            <ta e="T115" id="Seg_2504" s="T114">šo-ʔ</ta>
            <ta e="T116" id="Seg_2505" s="T115">döbər</ta>
            <ta e="T117" id="Seg_2506" s="T116">dĭ</ta>
            <ta e="T118" id="Seg_2507" s="T117">šo-bi</ta>
            <ta e="T329" id="Seg_2508" s="T118">kan-lAʔ</ta>
            <ta e="T119" id="Seg_2509" s="T329">tʼür-bi</ta>
            <ta e="T120" id="Seg_2510" s="T119">dĭgəttə</ta>
            <ta e="T121" id="Seg_2511" s="T120">dĭ</ta>
            <ta e="T122" id="Seg_2512" s="T121">uʔbdə-bi</ta>
            <ta e="T123" id="Seg_2513" s="T122">dĭgəttə</ta>
            <ta e="T124" id="Seg_2514" s="T123">bazoʔ</ta>
            <ta e="T125" id="Seg_2515" s="T124">amno-laʔbə</ta>
            <ta e="T126" id="Seg_2516" s="T125">i</ta>
            <ta e="T127" id="Seg_2517" s="T126">sʼar-laʔbə</ta>
            <ta e="T128" id="Seg_2518" s="T127">am-</ta>
            <ta e="T129" id="Seg_2519" s="T128">amno-bi</ta>
            <ta e="T130" id="Seg_2520" s="T129">šeden-dən</ta>
            <ta e="T131" id="Seg_2521" s="T130">toʔ-gəndə</ta>
            <ta e="T132" id="Seg_2522" s="T131">dĭgəttə</ta>
            <ta e="T133" id="Seg_2523" s="T132">davaj</ta>
            <ta e="T134" id="Seg_2524" s="T133">erer-zittə</ta>
            <ta e="T135" id="Seg_2525" s="T134">dĭ</ta>
            <ta e="T136" id="Seg_2526" s="T135">kudʼelʼa-m</ta>
            <ta e="T137" id="Seg_2527" s="T136">erer-laʔbə</ta>
            <ta e="T138" id="Seg_2528" s="T137">erer-laʔbə</ta>
            <ta e="T139" id="Seg_2529" s="T138">dĭn</ta>
            <ta e="T140" id="Seg_2530" s="T139">dĭ</ta>
            <ta e="T141" id="Seg_2531" s="T140">ne</ta>
            <ta e="T142" id="Seg_2532" s="T141">bazoʔ</ta>
            <ta e="T143" id="Seg_2533" s="T142">tibi-n</ta>
            <ta e="T144" id="Seg_2534" s="T143">ne</ta>
            <ta e="T145" id="Seg_2535" s="T144">šo-bi</ta>
            <ta e="T146" id="Seg_2536" s="T145">sădar-t</ta>
            <ta e="T147" id="Seg_2537" s="T146">măna</ta>
            <ta e="T148" id="Seg_2538" s="T147">dĭ</ta>
            <ta e="T149" id="Seg_2539" s="T148">dʼok</ta>
            <ta e="T150" id="Seg_2540" s="T149">măn</ta>
            <ta e="T151" id="Seg_2541" s="T150">ej</ta>
            <ta e="T152" id="Seg_2542" s="T151">sădar-lV-m</ta>
            <ta e="T153" id="Seg_2543" s="T152">tibi-m</ta>
            <ta e="T155" id="Seg_2544" s="T154">mĭ-lV-l</ta>
            <ta e="T156" id="Seg_2545" s="T155">măna</ta>
            <ta e="T157" id="Seg_2546" s="T156">nüdʼi</ta>
            <ta e="T158" id="Seg_2547" s="T157">kunol-zittə</ta>
            <ta e="T159" id="Seg_2548" s="T158">dĭgəttə</ta>
            <ta e="T160" id="Seg_2549" s="T159">mĭ-lV-m</ta>
            <ta e="T161" id="Seg_2550" s="T160">nu</ta>
            <ta e="T162" id="Seg_2551" s="T161">i-t</ta>
            <ta e="T163" id="Seg_2552" s="T162">mĭ-lV-m</ta>
            <ta e="T165" id="Seg_2553" s="T163">dĭgəttə</ta>
            <ta e="T166" id="Seg_2554" s="T165">dĭ</ta>
            <ta e="T167" id="Seg_2555" s="T166">šo-bi</ta>
            <ta e="T168" id="Seg_2556" s="T167">dĭ</ta>
            <ta e="T169" id="Seg_2557" s="T168">bazoʔ</ta>
            <ta e="T170" id="Seg_2558" s="T169">amor-zittə</ta>
            <ta e="T171" id="Seg_2559" s="T170">amnə-bi</ta>
            <ta e="T172" id="Seg_2560" s="T171">dĭ</ta>
            <ta e="T173" id="Seg_2561" s="T172">dĭ-Tə</ta>
            <ta e="T174" id="Seg_2562" s="T173">bazoʔ</ta>
            <ta e="T175" id="Seg_2563" s="T174">kaplʼa-jəʔ</ta>
            <ta e="T176" id="Seg_2564" s="T175">mĭ-bi</ta>
            <ta e="T177" id="Seg_2565" s="T176">dĭ</ta>
            <ta e="T178" id="Seg_2566" s="T177">kunol-luʔbdə-bi</ta>
            <ta e="T179" id="Seg_2567" s="T178">dĭ</ta>
            <ta e="T180" id="Seg_2568" s="T179">dĭ-m</ta>
            <ta e="T181" id="Seg_2569" s="T180">tura-Tə</ta>
            <ta e="T182" id="Seg_2570" s="T181">kan-bi</ta>
            <ta e="T183" id="Seg_2571" s="T182">gijen</ta>
            <ta e="T184" id="Seg_2572" s="T183">kunol-laʔbə</ta>
            <ta e="T185" id="Seg_2573" s="T184">kan-ə-ʔ</ta>
            <ta e="T186" id="Seg_2574" s="T185">kunol-lAʔ</ta>
            <ta e="T187" id="Seg_2575" s="T186">dĭ</ta>
            <ta e="T188" id="Seg_2576" s="T187">dĭbər</ta>
            <ta e="T189" id="Seg_2577" s="T188">kan-bi</ta>
            <ta e="T190" id="Seg_2578" s="T189">dĭ-m</ta>
            <ta e="T192" id="Seg_2579" s="T191">dĭ-Tə</ta>
            <ta e="T193" id="Seg_2580" s="T192">uʔbdə-ʔ</ta>
            <ta e="T194" id="Seg_2581" s="T193">uʔbdə-ʔ</ta>
            <ta e="T196" id="Seg_2582" s="T194">dĭgəttə</ta>
            <ta e="T197" id="Seg_2583" s="T196">kădaʔ=də</ta>
            <ta e="T198" id="Seg_2584" s="T197">ej</ta>
            <ta e="T199" id="Seg_2585" s="T198">mo-liA</ta>
            <ta e="T200" id="Seg_2586" s="T199">uʔbdə-zittə</ta>
            <ta e="T201" id="Seg_2587" s="T200">dĭgəttə</ta>
            <ta e="T202" id="Seg_2588" s="T201">ertə</ta>
            <ta e="T203" id="Seg_2589" s="T202">mo-laːm-bi</ta>
            <ta e="T204" id="Seg_2590" s="T203">dĭ</ta>
            <ta e="T205" id="Seg_2591" s="T204">măn-ntə</ta>
            <ta e="T206" id="Seg_2592" s="T205">kan-ə-ʔ</ta>
            <ta e="T207" id="Seg_2593" s="T206">kabarləj</ta>
            <ta e="T208" id="Seg_2594" s="T207">tănan</ta>
            <ta e="T209" id="Seg_2595" s="T208">dön</ta>
            <ta e="T210" id="Seg_2596" s="T209">amno-zittə</ta>
            <ta e="T211" id="Seg_2597" s="T210">dĭgəttə</ta>
            <ta e="T212" id="Seg_2598" s="T211">dĭ</ta>
            <ta e="T213" id="Seg_2599" s="T212">kan-bi</ta>
            <ta e="T214" id="Seg_2600" s="T213">dĭ</ta>
            <ta e="T215" id="Seg_2601" s="T214">uʔbdə-bi</ta>
            <ta e="T216" id="Seg_2602" s="T215">dĭgəttə</ta>
            <ta e="T217" id="Seg_2603" s="T216">bazoʔ</ta>
            <ta e="T219" id="Seg_2604" s="T218">šeden-Kən</ta>
            <ta e="T220" id="Seg_2605" s="T219">amnə-laʔbə</ta>
            <ta e="T221" id="Seg_2606" s="T220">korniš</ta>
            <ta e="T222" id="Seg_2607" s="T221">nuldə-bi</ta>
            <ta e="T223" id="Seg_2608" s="T222">kuvas</ta>
            <ta e="T226" id="Seg_2609" s="T225">uda-gəndə</ta>
            <ta e="T227" id="Seg_2610" s="T226">uda-gəndə</ta>
            <ta e="T228" id="Seg_2611" s="T227">šer-liA</ta>
            <ta e="T229" id="Seg_2612" s="T228">to</ta>
            <ta e="T230" id="Seg_2613" s="T229">dö</ta>
            <ta e="T231" id="Seg_2614" s="T230">uda-gəndə</ta>
            <ta e="T232" id="Seg_2615" s="T231">to</ta>
            <ta e="T233" id="Seg_2616" s="T232">dö</ta>
            <ta e="T235" id="Seg_2617" s="T233">uda-gəndə</ta>
            <ta e="T236" id="Seg_2618" s="T235">dĭ</ta>
            <ta e="T237" id="Seg_2619" s="T236">ku-laʔpi</ta>
            <ta e="T238" id="Seg_2620" s="T237">da</ta>
            <ta e="T239" id="Seg_2621" s="T238">kan-bi</ta>
            <ta e="T240" id="Seg_2622" s="T239">sădar-t</ta>
            <ta e="T241" id="Seg_2623" s="T240">măna</ta>
            <ta e="T242" id="Seg_2624" s="T241">dʼok</ta>
            <ta e="T243" id="Seg_2625" s="T242">măn</ta>
            <ta e="T244" id="Seg_2626" s="T243">ej</ta>
            <ta e="T245" id="Seg_2627" s="T244">sădar-liA-m</ta>
            <ta e="T246" id="Seg_2628" s="T245">tibi</ta>
            <ta e="T247" id="Seg_2629" s="T246">mĭ-ʔ</ta>
            <ta e="T248" id="Seg_2630" s="T247">măna</ta>
            <ta e="T249" id="Seg_2631" s="T248">teinen</ta>
            <ta e="T250" id="Seg_2632" s="T249">šaʔ-zittə</ta>
            <ta e="T251" id="Seg_2633" s="T250">kunol-zittə</ta>
            <ta e="T252" id="Seg_2634" s="T251">no</ta>
            <ta e="T253" id="Seg_2635" s="T252">i-ʔ</ta>
            <ta e="T254" id="Seg_2636" s="T253">dĭgəttə</ta>
            <ta e="T255" id="Seg_2637" s="T254">dĭ-Tə</ta>
            <ta e="T256" id="Seg_2638" s="T255">mĭ-bi</ta>
            <ta e="T257" id="Seg_2639" s="T256">kalʼečka</ta>
            <ta e="T258" id="Seg_2640" s="T257">i</ta>
            <ta e="T259" id="Seg_2641" s="T258">kormiš</ta>
            <ta e="T260" id="Seg_2642" s="T259">dĭgəttə</ta>
            <ta e="T261" id="Seg_2643" s="T260">tibi</ta>
            <ta e="T262" id="Seg_2644" s="T261">šo-bi</ta>
            <ta e="T263" id="Seg_2645" s="T262">amor-zittə</ta>
            <ta e="T264" id="Seg_2646" s="T263">amno-bi</ta>
            <ta e="T265" id="Seg_2647" s="T264">dĭ</ta>
            <ta e="T266" id="Seg_2648" s="T265">dĭ-Tə</ta>
            <ta e="T267" id="Seg_2649" s="T266">kaplʼa-jəʔ</ta>
            <ta e="T268" id="Seg_2650" s="T267">bazoʔ</ta>
            <ta e="T269" id="Seg_2651" s="T268">mĭ-bi</ta>
            <ta e="T270" id="Seg_2652" s="T269">dĭ</ta>
            <ta e="T271" id="Seg_2653" s="T270">kunol-luʔbdə-bi</ta>
            <ta e="T272" id="Seg_2654" s="T271">dĭ</ta>
            <ta e="T273" id="Seg_2655" s="T272">bazoʔ</ta>
            <ta e="T274" id="Seg_2656" s="T273">tura-Tə</ta>
            <ta e="T275" id="Seg_2657" s="T274">hen-bi</ta>
            <ta e="T276" id="Seg_2658" s="T275">dĭ-m</ta>
            <ta e="T277" id="Seg_2659" s="T276">i</ta>
            <ta e="T278" id="Seg_2660" s="T277">dĭ-m</ta>
            <ta e="T279" id="Seg_2661" s="T278">öʔlu-bi</ta>
            <ta e="T280" id="Seg_2662" s="T279">dĭ</ta>
            <ta e="T281" id="Seg_2663" s="T280">dĭn</ta>
            <ta e="T282" id="Seg_2664" s="T281">kan-bi</ta>
            <ta e="T283" id="Seg_2665" s="T282">bazoʔ</ta>
            <ta e="T284" id="Seg_2666" s="T283">tʼor-bi</ta>
            <ta e="T285" id="Seg_2667" s="T284">dĭ-m</ta>
            <ta e="T286" id="Seg_2668" s="T285">bar</ta>
            <ta e="T287" id="Seg_2669" s="T286">uʔbdə-liA</ta>
            <ta e="T288" id="Seg_2670" s="T287">dʼildə-liA</ta>
            <ta e="T289" id="Seg_2671" s="T288">dĭ-m</ta>
            <ta e="T290" id="Seg_2672" s="T289">tʼor-bi</ta>
            <ta e="T291" id="Seg_2673" s="T290">tʼor-bi</ta>
            <ta e="T292" id="Seg_2674" s="T291">kajel-da</ta>
            <ta e="T293" id="Seg_2675" s="T292">saʔmə-luʔbdə-bi</ta>
            <ta e="T294" id="Seg_2676" s="T293">kadul-gəndə</ta>
            <ta e="T295" id="Seg_2677" s="T294">dĭ</ta>
            <ta e="T296" id="Seg_2678" s="T295">bar</ta>
            <ta e="T297" id="Seg_2679" s="T296">šüʔdər-luʔbdə-pi</ta>
            <ta e="T298" id="Seg_2680" s="T297">dĭgəttə</ta>
            <ta e="T300" id="Seg_2681" s="T299">kamro-luʔbdə-bi-jəʔ</ta>
            <ta e="T301" id="Seg_2682" s="T300">dĭgəttə</ta>
            <ta e="T302" id="Seg_2683" s="T301">ertə</ta>
            <ta e="T303" id="Seg_2684" s="T302">mo-laːm-bi</ta>
            <ta e="T304" id="Seg_2685" s="T303">dĭ</ta>
            <ta e="T305" id="Seg_2686" s="T304">šo-bi</ta>
            <ta e="T306" id="Seg_2687" s="T305">a</ta>
            <ta e="T307" id="Seg_2688" s="T306">tăn</ta>
            <ta e="T308" id="Seg_2689" s="T307">măna</ta>
            <ta e="T309" id="Seg_2690" s="T308">sădar-bi-l</ta>
            <ta e="T310" id="Seg_2691" s="T309">tak</ta>
            <ta e="T311" id="Seg_2692" s="T310">tăn</ta>
            <ta e="T312" id="Seg_2693" s="T311">măn</ta>
            <ta e="T313" id="Seg_2694" s="T312">ej</ta>
            <ta e="T314" id="Seg_2695" s="T313">măn</ta>
            <ta e="T315" id="Seg_2696" s="T314">ne-m</ta>
            <ta e="T316" id="Seg_2697" s="T315">a</ta>
            <ta e="T317" id="Seg_2698" s="T316">dö</ta>
            <ta e="T318" id="Seg_2699" s="T317">măn</ta>
            <ta e="T319" id="Seg_2700" s="T318">ne-m</ta>
            <ta e="T320" id="Seg_2701" s="T319">dĭgəttə</ta>
            <ta e="T321" id="Seg_2702" s="T320">pol-də</ta>
            <ta e="T322" id="Seg_2703" s="T321">saʔmə-luʔbdə-bi-bAʔ</ta>
            <ta e="T323" id="Seg_2704" s="T322">saʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T324" id="Seg_2705" s="T323">süjö-ziʔ</ta>
            <ta e="T325" id="Seg_2706" s="T324">mo-laʔbə-bi-jəʔ</ta>
            <ta e="T326" id="Seg_2707" s="T325">i</ta>
            <ta e="T327" id="Seg_2708" s="T326">nʼergö-luʔbdə-bi-jəʔ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_2709" s="T0">then</ta>
            <ta e="T2" id="Seg_2710" s="T1">this.[NOM.SG]</ta>
            <ta e="T3" id="Seg_2711" s="T2">go-PST.[3SG]</ta>
            <ta e="T4" id="Seg_2712" s="T3">clew-NOM/GEN/ACC.3SG</ta>
            <ta e="T5" id="Seg_2713" s="T4">throw.away-PST.[3SG]</ta>
            <ta e="T6" id="Seg_2714" s="T5">where.to</ta>
            <ta e="T7" id="Seg_2715" s="T6">this.[NOM.SG]</ta>
            <ta e="T8" id="Seg_2716" s="T7">go-FUT-3SG</ta>
            <ta e="T9" id="Seg_2717" s="T8">and</ta>
            <ta e="T10" id="Seg_2718" s="T9">this.[NOM.SG]</ta>
            <ta e="T11" id="Seg_2719" s="T10">this-LAT</ta>
            <ta e="T13" id="Seg_2720" s="T12">this-LAT</ta>
            <ta e="T15" id="Seg_2721" s="T14">go-</ta>
            <ta e="T16" id="Seg_2722" s="T15">walk-PRS.[3SG]</ta>
            <ta e="T17" id="Seg_2723" s="T16">clew-NOM/GEN/ACC.3SG</ta>
            <ta e="T18" id="Seg_2724" s="T17">run-PST.[3SG]</ta>
            <ta e="T19" id="Seg_2725" s="T18">run-PST.[3SG]</ta>
            <ta e="T20" id="Seg_2726" s="T19">and</ta>
            <ta e="T21" id="Seg_2727" s="T20">bring-PST.[3SG]</ta>
            <ta e="T22" id="Seg_2728" s="T21">there</ta>
            <ta e="T23" id="Seg_2729" s="T22">where</ta>
            <ta e="T24" id="Seg_2730" s="T23">this-GEN</ta>
            <ta e="T25" id="Seg_2731" s="T24">man.[NOM.SG]</ta>
            <ta e="T26" id="Seg_2732" s="T25">live-DUR.[3SG]</ta>
            <ta e="T27" id="Seg_2733" s="T26">this</ta>
            <ta e="T28" id="Seg_2734" s="T27">this-GEN</ta>
            <ta e="T29" id="Seg_2735" s="T28">PTCL</ta>
            <ta e="T30" id="Seg_2736" s="T29">woman.[NOM.SG]</ta>
            <ta e="T31" id="Seg_2737" s="T30">be-PRS.[3SG]</ta>
            <ta e="T32" id="Seg_2738" s="T31">fear-PRS.[3SG]</ta>
            <ta e="T33" id="Seg_2739" s="T32">go-INF.LAT</ta>
            <ta e="T34" id="Seg_2740" s="T33">then</ta>
            <ta e="T35" id="Seg_2741" s="T34">outside</ta>
            <ta e="T36" id="Seg_2742" s="T35">sit-</ta>
            <ta e="T37" id="Seg_2743" s="T36">sit-PST.[3SG]</ta>
            <ta e="T38" id="Seg_2744" s="T37">and</ta>
            <ta e="T39" id="Seg_2745" s="T38">cup-ACC.3SG</ta>
            <ta e="T40" id="Seg_2746" s="T39">place-PST.[3SG]</ta>
            <ta e="T41" id="Seg_2747" s="T40">and</ta>
            <ta e="T42" id="Seg_2748" s="T41">egg-PL-LAT</ta>
            <ta e="T44" id="Seg_2749" s="T43">and</ta>
            <ta e="T45" id="Seg_2750" s="T44">PTCL</ta>
            <ta e="T46" id="Seg_2751" s="T45">play-CVB</ta>
            <ta e="T47" id="Seg_2752" s="T46">sit-DUR.[3SG]</ta>
            <ta e="T48" id="Seg_2753" s="T47">and</ta>
            <ta e="T49" id="Seg_2754" s="T48">this.[NOM.SG]</ta>
            <ta e="T50" id="Seg_2755" s="T49">man-GEN</ta>
            <ta e="T51" id="Seg_2756" s="T50">woman-NOM/GEN.3SG</ta>
            <ta e="T52" id="Seg_2757" s="T51">sell-IMP.2SG.O</ta>
            <ta e="T53" id="Seg_2758" s="T52">I.LAT</ta>
            <ta e="T54" id="Seg_2759" s="T53">and</ta>
            <ta e="T55" id="Seg_2760" s="T54">this.[NOM.SG]</ta>
            <ta e="T56" id="Seg_2761" s="T55">I.LAT</ta>
            <ta e="T57" id="Seg_2762" s="T56">say-IPFVZ.[3SG]</ta>
            <ta e="T58" id="Seg_2763" s="T57">give-FUT-2SG</ta>
            <ta e="T59" id="Seg_2764" s="T58">you.NOM</ta>
            <ta e="T60" id="Seg_2765" s="T59">man.[NOM.SG]</ta>
            <ta e="T61" id="Seg_2766" s="T60">I.LAT</ta>
            <ta e="T62" id="Seg_2767" s="T61">sleep-INF.LAT</ta>
            <ta e="T64" id="Seg_2768" s="T62">this.[NOM.SG]</ta>
            <ta e="T65" id="Seg_2769" s="T64">this-LAT</ta>
            <ta e="T66" id="Seg_2770" s="T65">give-PST.[3SG]</ta>
            <ta e="T67" id="Seg_2771" s="T66">sleep-EP-IMP.2SG</ta>
            <ta e="T68" id="Seg_2772" s="T67">what.[NOM.SG]</ta>
            <ta e="T69" id="Seg_2773" s="T68">I.LAT</ta>
            <ta e="T70" id="Seg_2774" s="T69">pity-PRS-1SG</ta>
            <ta e="T71" id="Seg_2775" s="T70">isn_t.it</ta>
            <ta e="T72" id="Seg_2776" s="T71">this.[NOM.SG]</ta>
            <ta e="T73" id="Seg_2777" s="T72">come-PST.[3SG]</ta>
            <ta e="T74" id="Seg_2778" s="T73">tent-LAT/LOC.3SG</ta>
            <ta e="T75" id="Seg_2779" s="T74">sit.down-PST.[3SG]</ta>
            <ta e="T76" id="Seg_2780" s="T75">eat-INF.LAT</ta>
            <ta e="T77" id="Seg_2781" s="T76">this.[NOM.SG]</ta>
            <ta e="T78" id="Seg_2782" s="T77">this-LAT</ta>
            <ta e="T80" id="Seg_2783" s="T79">give-PST.[3SG]</ta>
            <ta e="T81" id="Seg_2784" s="T80">drop-PL</ta>
            <ta e="T82" id="Seg_2785" s="T81">this.[NOM.SG]</ta>
            <ta e="T83" id="Seg_2786" s="T82">sleep-MOM-PST.[3SG]</ta>
            <ta e="T84" id="Seg_2787" s="T83">this.[NOM.SG]</ta>
            <ta e="T85" id="Seg_2788" s="T84">this-ACC</ta>
            <ta e="T86" id="Seg_2789" s="T85">bring-PST.[3SG]</ta>
            <ta e="T87" id="Seg_2790" s="T86">there</ta>
            <ta e="T88" id="Seg_2791" s="T87">where</ta>
            <ta e="T89" id="Seg_2792" s="T88">sleep-PRS.[3SG]</ta>
            <ta e="T90" id="Seg_2793" s="T89">then</ta>
            <ta e="T91" id="Seg_2794" s="T90">this-ACC</ta>
            <ta e="T92" id="Seg_2795" s="T91">there</ta>
            <ta e="T93" id="Seg_2796" s="T92">send-PST.[3SG]</ta>
            <ta e="T94" id="Seg_2797" s="T93">this.[NOM.SG]</ta>
            <ta e="T95" id="Seg_2798" s="T94">PTCL</ta>
            <ta e="T96" id="Seg_2799" s="T95">sit-PST.[3SG]</ta>
            <ta e="T97" id="Seg_2800" s="T96">sit-PST.[3SG]</ta>
            <ta e="T98" id="Seg_2801" s="T97">shout-PST.[3SG]</ta>
            <ta e="T99" id="Seg_2802" s="T98">shout-PST.[3SG]</ta>
            <ta e="T100" id="Seg_2803" s="T99">speak-PST.[3SG]</ta>
            <ta e="T101" id="Seg_2804" s="T100">say-IPFVZ.[3SG]</ta>
            <ta e="T102" id="Seg_2805" s="T101">get.up-IMP.2SG</ta>
            <ta e="T103" id="Seg_2806" s="T102">and</ta>
            <ta e="T104" id="Seg_2807" s="T103">this.[NOM.SG]</ta>
            <ta e="T105" id="Seg_2808" s="T104">always</ta>
            <ta e="T106" id="Seg_2809" s="T105">NEG</ta>
            <ta e="T107" id="Seg_2810" s="T106">get.up-PRS.[3SG]</ta>
            <ta e="T108" id="Seg_2811" s="T107">sleep-DUR.[3SG]</ta>
            <ta e="T109" id="Seg_2812" s="T108">strongly</ta>
            <ta e="T110" id="Seg_2813" s="T109">then</ta>
            <ta e="T111" id="Seg_2814" s="T110">morning.[NOM.SG]</ta>
            <ta e="T112" id="Seg_2815" s="T111">become-RES-PST.[3SG]</ta>
            <ta e="T113" id="Seg_2816" s="T112">this.[NOM.SG]</ta>
            <ta e="T115" id="Seg_2817" s="T114">come-IMP.2SG</ta>
            <ta e="T116" id="Seg_2818" s="T115">here</ta>
            <ta e="T117" id="Seg_2819" s="T116">this.[NOM.SG]</ta>
            <ta e="T118" id="Seg_2820" s="T117">come-PST.[3SG]</ta>
            <ta e="T329" id="Seg_2821" s="T118">go-CVB</ta>
            <ta e="T119" id="Seg_2822" s="T329">disappear-PST.[3SG]</ta>
            <ta e="T120" id="Seg_2823" s="T119">then</ta>
            <ta e="T121" id="Seg_2824" s="T120">this.[NOM.SG]</ta>
            <ta e="T122" id="Seg_2825" s="T121">get.up-PST.[3SG]</ta>
            <ta e="T123" id="Seg_2826" s="T122">then</ta>
            <ta e="T124" id="Seg_2827" s="T123">again</ta>
            <ta e="T125" id="Seg_2828" s="T124">sit-DUR.[3SG]</ta>
            <ta e="T126" id="Seg_2829" s="T125">and</ta>
            <ta e="T127" id="Seg_2830" s="T126">play-DUR.[3SG]</ta>
            <ta e="T128" id="Seg_2831" s="T127">sit-</ta>
            <ta e="T129" id="Seg_2832" s="T128">sit-PST.[3SG]</ta>
            <ta e="T130" id="Seg_2833" s="T129">corral-NOM/GEN/ACC.3PL</ta>
            <ta e="T131" id="Seg_2834" s="T130">edge-LAT/LOC.3SG</ta>
            <ta e="T132" id="Seg_2835" s="T131">then</ta>
            <ta e="T133" id="Seg_2836" s="T132">INCH</ta>
            <ta e="T134" id="Seg_2837" s="T133">spin-INF.LAT</ta>
            <ta e="T135" id="Seg_2838" s="T134">this.[NOM.SG]</ta>
            <ta e="T136" id="Seg_2839" s="T135">tow-ACC</ta>
            <ta e="T137" id="Seg_2840" s="T136">spin-DUR.[3SG]</ta>
            <ta e="T138" id="Seg_2841" s="T137">spin-DUR.[3SG]</ta>
            <ta e="T139" id="Seg_2842" s="T138">there</ta>
            <ta e="T140" id="Seg_2843" s="T139">this.[NOM.SG]</ta>
            <ta e="T141" id="Seg_2844" s="T140">woman.[NOM.SG]</ta>
            <ta e="T142" id="Seg_2845" s="T141">again</ta>
            <ta e="T143" id="Seg_2846" s="T142">man-GEN</ta>
            <ta e="T144" id="Seg_2847" s="T143">woman.[NOM.SG]</ta>
            <ta e="T145" id="Seg_2848" s="T144">come-PST.[3SG]</ta>
            <ta e="T146" id="Seg_2849" s="T145">sell-IMP.2SG.O</ta>
            <ta e="T147" id="Seg_2850" s="T146">I.LAT</ta>
            <ta e="T148" id="Seg_2851" s="T147">this.[NOM.SG]</ta>
            <ta e="T149" id="Seg_2852" s="T148">no</ta>
            <ta e="T150" id="Seg_2853" s="T149">I.NOM</ta>
            <ta e="T151" id="Seg_2854" s="T150">NEG</ta>
            <ta e="T152" id="Seg_2855" s="T151">sell-FUT-1SG</ta>
            <ta e="T153" id="Seg_2856" s="T152">man-ACC</ta>
            <ta e="T155" id="Seg_2857" s="T154">give-FUT-2SG</ta>
            <ta e="T156" id="Seg_2858" s="T155">I.LAT</ta>
            <ta e="T157" id="Seg_2859" s="T156">evening.[NOM.SG]</ta>
            <ta e="T158" id="Seg_2860" s="T157">sleep-INF.LAT</ta>
            <ta e="T159" id="Seg_2861" s="T158">then</ta>
            <ta e="T160" id="Seg_2862" s="T159">give-FUT-1SG</ta>
            <ta e="T161" id="Seg_2863" s="T160">well</ta>
            <ta e="T162" id="Seg_2864" s="T161">take-IMP.2SG.O</ta>
            <ta e="T163" id="Seg_2865" s="T162">give-FUT-1SG</ta>
            <ta e="T165" id="Seg_2866" s="T163">then</ta>
            <ta e="T166" id="Seg_2867" s="T165">this.[NOM.SG]</ta>
            <ta e="T167" id="Seg_2868" s="T166">come-PST.[3SG]</ta>
            <ta e="T168" id="Seg_2869" s="T167">this.[NOM.SG]</ta>
            <ta e="T169" id="Seg_2870" s="T168">again</ta>
            <ta e="T170" id="Seg_2871" s="T169">eat-INF.LAT</ta>
            <ta e="T171" id="Seg_2872" s="T170">sit.down-PST.[3SG]</ta>
            <ta e="T172" id="Seg_2873" s="T171">this.[NOM.SG]</ta>
            <ta e="T173" id="Seg_2874" s="T172">this-LAT</ta>
            <ta e="T174" id="Seg_2875" s="T173">again</ta>
            <ta e="T175" id="Seg_2876" s="T174">drop-PL</ta>
            <ta e="T176" id="Seg_2877" s="T175">give-PST.[3SG]</ta>
            <ta e="T177" id="Seg_2878" s="T176">this.[NOM.SG]</ta>
            <ta e="T178" id="Seg_2879" s="T177">sleep-MOM-PST.[3SG]</ta>
            <ta e="T179" id="Seg_2880" s="T178">this.[NOM.SG]</ta>
            <ta e="T180" id="Seg_2881" s="T179">this-ACC</ta>
            <ta e="T181" id="Seg_2882" s="T180">house-LAT</ta>
            <ta e="T182" id="Seg_2883" s="T181">go-PST.[3SG]</ta>
            <ta e="T183" id="Seg_2884" s="T182">where</ta>
            <ta e="T184" id="Seg_2885" s="T183">sleep-DUR.[3SG]</ta>
            <ta e="T185" id="Seg_2886" s="T184">go-EP-IMP.2SG</ta>
            <ta e="T186" id="Seg_2887" s="T185">sleep-CVB</ta>
            <ta e="T187" id="Seg_2888" s="T186">this.[NOM.SG]</ta>
            <ta e="T188" id="Seg_2889" s="T187">there</ta>
            <ta e="T189" id="Seg_2890" s="T188">go-PST.[3SG]</ta>
            <ta e="T190" id="Seg_2891" s="T189">this-ACC</ta>
            <ta e="T192" id="Seg_2892" s="T191">this-LAT</ta>
            <ta e="T193" id="Seg_2893" s="T192">get.up-IMP.2SG</ta>
            <ta e="T194" id="Seg_2894" s="T193">get.up-IMP.2SG</ta>
            <ta e="T196" id="Seg_2895" s="T194">then</ta>
            <ta e="T197" id="Seg_2896" s="T196">how=INDEF</ta>
            <ta e="T198" id="Seg_2897" s="T197">NEG</ta>
            <ta e="T199" id="Seg_2898" s="T198">can-PRS.[3SG]</ta>
            <ta e="T200" id="Seg_2899" s="T199">get.up-INF.LAT</ta>
            <ta e="T201" id="Seg_2900" s="T200">then</ta>
            <ta e="T202" id="Seg_2901" s="T201">morning</ta>
            <ta e="T203" id="Seg_2902" s="T202">become-RES-PST.[3SG]</ta>
            <ta e="T204" id="Seg_2903" s="T203">this.[NOM.SG]</ta>
            <ta e="T205" id="Seg_2904" s="T204">say-IPFVZ.[3SG]</ta>
            <ta e="T206" id="Seg_2905" s="T205">go-EP-IMP.2SG</ta>
            <ta e="T207" id="Seg_2906" s="T206">enough</ta>
            <ta e="T208" id="Seg_2907" s="T207">you.DAT</ta>
            <ta e="T209" id="Seg_2908" s="T208">here</ta>
            <ta e="T210" id="Seg_2909" s="T209">sit-INF.LAT</ta>
            <ta e="T211" id="Seg_2910" s="T210">then</ta>
            <ta e="T212" id="Seg_2911" s="T211">this.[NOM.SG]</ta>
            <ta e="T213" id="Seg_2912" s="T212">go-PST.[3SG]</ta>
            <ta e="T214" id="Seg_2913" s="T213">this.[NOM.SG]</ta>
            <ta e="T215" id="Seg_2914" s="T214">get.up-PST.[3SG]</ta>
            <ta e="T216" id="Seg_2915" s="T215">then</ta>
            <ta e="T217" id="Seg_2916" s="T216">again</ta>
            <ta e="T219" id="Seg_2917" s="T218">corral-LOC</ta>
            <ta e="T220" id="Seg_2918" s="T219">sit-DUR.[3SG]</ta>
            <ta e="T221" id="Seg_2919" s="T220">%spindle.[NOM.SG]</ta>
            <ta e="T222" id="Seg_2920" s="T221">place-PST.[3SG]</ta>
            <ta e="T223" id="Seg_2921" s="T222">beautiful.[NOM.SG]</ta>
            <ta e="T226" id="Seg_2922" s="T225">hand-LAT/LOC.3SG</ta>
            <ta e="T227" id="Seg_2923" s="T226">hand-LAT/LOC.3SG</ta>
            <ta e="T228" id="Seg_2924" s="T227">dress-PRS.[3SG]</ta>
            <ta e="T229" id="Seg_2925" s="T228">then</ta>
            <ta e="T230" id="Seg_2926" s="T229">that.[NOM.SG]</ta>
            <ta e="T231" id="Seg_2927" s="T230">hand-LAT/LOC.3SG</ta>
            <ta e="T232" id="Seg_2928" s="T231">then</ta>
            <ta e="T233" id="Seg_2929" s="T232">that.[NOM.SG]</ta>
            <ta e="T235" id="Seg_2930" s="T233">hand-LAT/LOC.3SG</ta>
            <ta e="T236" id="Seg_2931" s="T235">this.[NOM.SG]</ta>
            <ta e="T237" id="Seg_2932" s="T236">see-DUR.PST.[3SG]</ta>
            <ta e="T238" id="Seg_2933" s="T237">and</ta>
            <ta e="T239" id="Seg_2934" s="T238">go-PST.[3SG]</ta>
            <ta e="T240" id="Seg_2935" s="T239">sell-IMP.2SG.O</ta>
            <ta e="T241" id="Seg_2936" s="T240">I.LAT</ta>
            <ta e="T242" id="Seg_2937" s="T241">no</ta>
            <ta e="T243" id="Seg_2938" s="T242">I.NOM</ta>
            <ta e="T244" id="Seg_2939" s="T243">NEG</ta>
            <ta e="T245" id="Seg_2940" s="T244">sell-PRS-1SG</ta>
            <ta e="T246" id="Seg_2941" s="T245">man.[NOM.SG]</ta>
            <ta e="T247" id="Seg_2942" s="T246">give-IMP.2SG</ta>
            <ta e="T248" id="Seg_2943" s="T247">I.LAT</ta>
            <ta e="T249" id="Seg_2944" s="T248">today</ta>
            <ta e="T250" id="Seg_2945" s="T249">spend.night-INF.LAT</ta>
            <ta e="T251" id="Seg_2946" s="T250">sleep-INF.LAT</ta>
            <ta e="T252" id="Seg_2947" s="T251">well</ta>
            <ta e="T253" id="Seg_2948" s="T252">take-IMP.2SG</ta>
            <ta e="T254" id="Seg_2949" s="T253">then</ta>
            <ta e="T255" id="Seg_2950" s="T254">this-LAT</ta>
            <ta e="T256" id="Seg_2951" s="T255">give-PST.[3SG]</ta>
            <ta e="T257" id="Seg_2952" s="T256">ring.[NOM.SG]</ta>
            <ta e="T258" id="Seg_2953" s="T257">and</ta>
            <ta e="T259" id="Seg_2954" s="T258">%spindle.[NOM.SG]</ta>
            <ta e="T260" id="Seg_2955" s="T259">then</ta>
            <ta e="T261" id="Seg_2956" s="T260">man.[NOM.SG]</ta>
            <ta e="T262" id="Seg_2957" s="T261">come-PST.[3SG]</ta>
            <ta e="T263" id="Seg_2958" s="T262">eat-INF.LAT</ta>
            <ta e="T264" id="Seg_2959" s="T263">sit-PST.[3SG]</ta>
            <ta e="T265" id="Seg_2960" s="T264">this.[NOM.SG]</ta>
            <ta e="T266" id="Seg_2961" s="T265">this-LAT</ta>
            <ta e="T267" id="Seg_2962" s="T266">drop-PL</ta>
            <ta e="T268" id="Seg_2963" s="T267">again</ta>
            <ta e="T269" id="Seg_2964" s="T268">give-PST.[3SG]</ta>
            <ta e="T270" id="Seg_2965" s="T269">this.[NOM.SG]</ta>
            <ta e="T271" id="Seg_2966" s="T270">sleep-MOM-PST.[3SG]</ta>
            <ta e="T272" id="Seg_2967" s="T271">this.[NOM.SG]</ta>
            <ta e="T273" id="Seg_2968" s="T272">again</ta>
            <ta e="T274" id="Seg_2969" s="T273">house-LAT</ta>
            <ta e="T275" id="Seg_2970" s="T274">put-PST.[3SG]</ta>
            <ta e="T276" id="Seg_2971" s="T275">this-ACC</ta>
            <ta e="T277" id="Seg_2972" s="T276">and</ta>
            <ta e="T278" id="Seg_2973" s="T277">this-ACC</ta>
            <ta e="T279" id="Seg_2974" s="T278">send-PST.[3SG]</ta>
            <ta e="T280" id="Seg_2975" s="T279">this.[NOM.SG]</ta>
            <ta e="T281" id="Seg_2976" s="T280">there</ta>
            <ta e="T282" id="Seg_2977" s="T281">go-PST.[3SG]</ta>
            <ta e="T283" id="Seg_2978" s="T282">again</ta>
            <ta e="T284" id="Seg_2979" s="T283">cry-PST.[3SG]</ta>
            <ta e="T285" id="Seg_2980" s="T284">this-ACC</ta>
            <ta e="T286" id="Seg_2981" s="T285">PTCL</ta>
            <ta e="T287" id="Seg_2982" s="T286">get.up-PRS.[3SG]</ta>
            <ta e="T288" id="Seg_2983" s="T287">lift-PRS.[3SG]</ta>
            <ta e="T289" id="Seg_2984" s="T288">this-ACC</ta>
            <ta e="T290" id="Seg_2985" s="T289">cry-PST.[3SG]</ta>
            <ta e="T291" id="Seg_2986" s="T290">cry-PST.[3SG]</ta>
            <ta e="T292" id="Seg_2987" s="T291">tear-NOM/GEN/ACC.3SG</ta>
            <ta e="T293" id="Seg_2988" s="T292">fall-MOM-PST.[3SG]</ta>
            <ta e="T294" id="Seg_2989" s="T293">face-LAT/LOC.3SG</ta>
            <ta e="T295" id="Seg_2990" s="T294">this.[NOM.SG]</ta>
            <ta e="T296" id="Seg_2991" s="T295">PTCL</ta>
            <ta e="T297" id="Seg_2992" s="T296">wake.up-MOM-PST.[3SG]</ta>
            <ta e="T298" id="Seg_2993" s="T297">then</ta>
            <ta e="T300" id="Seg_2994" s="T299">hug-MOM-PST-3PL</ta>
            <ta e="T301" id="Seg_2995" s="T300">then</ta>
            <ta e="T302" id="Seg_2996" s="T301">morning</ta>
            <ta e="T303" id="Seg_2997" s="T302">become-RES-PST.[3SG]</ta>
            <ta e="T304" id="Seg_2998" s="T303">this.[NOM.SG]</ta>
            <ta e="T305" id="Seg_2999" s="T304">come-PST.[3SG]</ta>
            <ta e="T306" id="Seg_3000" s="T305">and</ta>
            <ta e="T307" id="Seg_3001" s="T306">you.NOM</ta>
            <ta e="T308" id="Seg_3002" s="T307">I.LAT</ta>
            <ta e="T309" id="Seg_3003" s="T308">sell-PST-2SG</ta>
            <ta e="T310" id="Seg_3004" s="T309">so</ta>
            <ta e="T311" id="Seg_3005" s="T310">you.NOM</ta>
            <ta e="T312" id="Seg_3006" s="T311">I.NOM</ta>
            <ta e="T313" id="Seg_3007" s="T312">NEG</ta>
            <ta e="T314" id="Seg_3008" s="T313">I.NOM</ta>
            <ta e="T315" id="Seg_3009" s="T314">woman-ACC</ta>
            <ta e="T316" id="Seg_3010" s="T315">and</ta>
            <ta e="T317" id="Seg_3011" s="T316">that.[NOM.SG]</ta>
            <ta e="T318" id="Seg_3012" s="T317">I.NOM</ta>
            <ta e="T319" id="Seg_3013" s="T318">woman-ACC</ta>
            <ta e="T320" id="Seg_3014" s="T319">then</ta>
            <ta e="T321" id="Seg_3015" s="T320">floor-NOM/GEN/ACC.3SG</ta>
            <ta e="T322" id="Seg_3016" s="T321">fall-MOM-PST-1PL</ta>
            <ta e="T323" id="Seg_3017" s="T322">fall-MOM-PST-3PL</ta>
            <ta e="T324" id="Seg_3018" s="T323">bird-INS</ta>
            <ta e="T325" id="Seg_3019" s="T324">become-DUR-PST-3PL</ta>
            <ta e="T326" id="Seg_3020" s="T325">and</ta>
            <ta e="T327" id="Seg_3021" s="T326">fly-MOM-PST-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_3022" s="T0">тогда</ta>
            <ta e="T2" id="Seg_3023" s="T1">этот.[NOM.SG]</ta>
            <ta e="T3" id="Seg_3024" s="T2">пойти-PST.[3SG]</ta>
            <ta e="T4" id="Seg_3025" s="T3">клубок-NOM/GEN/ACC.3SG</ta>
            <ta e="T5" id="Seg_3026" s="T4">выбросить-PST.[3SG]</ta>
            <ta e="T6" id="Seg_3027" s="T5">куда</ta>
            <ta e="T7" id="Seg_3028" s="T6">этот.[NOM.SG]</ta>
            <ta e="T8" id="Seg_3029" s="T7">пойти-FUT-3SG</ta>
            <ta e="T9" id="Seg_3030" s="T8">и</ta>
            <ta e="T10" id="Seg_3031" s="T9">этот.[NOM.SG]</ta>
            <ta e="T11" id="Seg_3032" s="T10">этот-LAT</ta>
            <ta e="T13" id="Seg_3033" s="T12">этот-LAT</ta>
            <ta e="T15" id="Seg_3034" s="T14">пойти-</ta>
            <ta e="T16" id="Seg_3035" s="T15">идти-PRS.[3SG]</ta>
            <ta e="T17" id="Seg_3036" s="T16">клубок-NOM/GEN/ACC.3SG</ta>
            <ta e="T18" id="Seg_3037" s="T17">бежать-PST.[3SG]</ta>
            <ta e="T19" id="Seg_3038" s="T18">бежать-PST.[3SG]</ta>
            <ta e="T20" id="Seg_3039" s="T19">и</ta>
            <ta e="T21" id="Seg_3040" s="T20">принести-PST.[3SG]</ta>
            <ta e="T22" id="Seg_3041" s="T21">там</ta>
            <ta e="T23" id="Seg_3042" s="T22">где</ta>
            <ta e="T24" id="Seg_3043" s="T23">этот-GEN</ta>
            <ta e="T25" id="Seg_3044" s="T24">мужчина.[NOM.SG]</ta>
            <ta e="T26" id="Seg_3045" s="T25">жить-DUR.[3SG]</ta>
            <ta e="T27" id="Seg_3046" s="T26">этот</ta>
            <ta e="T28" id="Seg_3047" s="T27">этот-GEN</ta>
            <ta e="T29" id="Seg_3048" s="T28">PTCL</ta>
            <ta e="T30" id="Seg_3049" s="T29">женщина.[NOM.SG]</ta>
            <ta e="T31" id="Seg_3050" s="T30">быть-PRS.[3SG]</ta>
            <ta e="T32" id="Seg_3051" s="T31">бояться-PRS.[3SG]</ta>
            <ta e="T33" id="Seg_3052" s="T32">пойти-INF.LAT</ta>
            <ta e="T34" id="Seg_3053" s="T33">тогда</ta>
            <ta e="T35" id="Seg_3054" s="T34">снаружи</ta>
            <ta e="T36" id="Seg_3055" s="T35">сидеть-</ta>
            <ta e="T37" id="Seg_3056" s="T36">сидеть-PST.[3SG]</ta>
            <ta e="T38" id="Seg_3057" s="T37">и</ta>
            <ta e="T39" id="Seg_3058" s="T38">чашка-ACC.3SG</ta>
            <ta e="T40" id="Seg_3059" s="T39">поставить-PST.[3SG]</ta>
            <ta e="T41" id="Seg_3060" s="T40">и</ta>
            <ta e="T42" id="Seg_3061" s="T41">яйцо-PL-LAT</ta>
            <ta e="T44" id="Seg_3062" s="T43">и</ta>
            <ta e="T45" id="Seg_3063" s="T44">PTCL</ta>
            <ta e="T46" id="Seg_3064" s="T45">играть-CVB</ta>
            <ta e="T47" id="Seg_3065" s="T46">сидеть-DUR.[3SG]</ta>
            <ta e="T48" id="Seg_3066" s="T47">а</ta>
            <ta e="T49" id="Seg_3067" s="T48">этот.[NOM.SG]</ta>
            <ta e="T50" id="Seg_3068" s="T49">мужчина-GEN</ta>
            <ta e="T51" id="Seg_3069" s="T50">женщина-NOM/GEN.3SG</ta>
            <ta e="T52" id="Seg_3070" s="T51">продавать-IMP.2SG.O</ta>
            <ta e="T53" id="Seg_3071" s="T52">я.LAT</ta>
            <ta e="T54" id="Seg_3072" s="T53">а</ta>
            <ta e="T55" id="Seg_3073" s="T54">этот.[NOM.SG]</ta>
            <ta e="T56" id="Seg_3074" s="T55">я.LAT</ta>
            <ta e="T57" id="Seg_3075" s="T56">сказать-IPFVZ.[3SG]</ta>
            <ta e="T58" id="Seg_3076" s="T57">дать-FUT-2SG</ta>
            <ta e="T59" id="Seg_3077" s="T58">ты.NOM</ta>
            <ta e="T60" id="Seg_3078" s="T59">мужчина.[NOM.SG]</ta>
            <ta e="T61" id="Seg_3079" s="T60">я.LAT</ta>
            <ta e="T62" id="Seg_3080" s="T61">спать-INF.LAT</ta>
            <ta e="T64" id="Seg_3081" s="T62">этот.[NOM.SG]</ta>
            <ta e="T65" id="Seg_3082" s="T64">этот-LAT</ta>
            <ta e="T66" id="Seg_3083" s="T65">дать-PST.[3SG]</ta>
            <ta e="T67" id="Seg_3084" s="T66">спать-EP-IMP.2SG</ta>
            <ta e="T68" id="Seg_3085" s="T67">что.[NOM.SG]</ta>
            <ta e="T69" id="Seg_3086" s="T68">я.LAT</ta>
            <ta e="T70" id="Seg_3087" s="T69">жалеть-PRS-1SG</ta>
            <ta e="T71" id="Seg_3088" s="T70">что.ли</ta>
            <ta e="T72" id="Seg_3089" s="T71">этот.[NOM.SG]</ta>
            <ta e="T73" id="Seg_3090" s="T72">прийти-PST.[3SG]</ta>
            <ta e="T74" id="Seg_3091" s="T73">чум-LAT/LOC.3SG</ta>
            <ta e="T75" id="Seg_3092" s="T74">сесть-PST.[3SG]</ta>
            <ta e="T76" id="Seg_3093" s="T75">есть-INF.LAT</ta>
            <ta e="T77" id="Seg_3094" s="T76">этот.[NOM.SG]</ta>
            <ta e="T78" id="Seg_3095" s="T77">этот-LAT</ta>
            <ta e="T80" id="Seg_3096" s="T79">дать-PST.[3SG]</ta>
            <ta e="T81" id="Seg_3097" s="T80">капля-PL</ta>
            <ta e="T82" id="Seg_3098" s="T81">этот.[NOM.SG]</ta>
            <ta e="T83" id="Seg_3099" s="T82">спать-MOM-PST.[3SG]</ta>
            <ta e="T84" id="Seg_3100" s="T83">этот.[NOM.SG]</ta>
            <ta e="T85" id="Seg_3101" s="T84">этот-ACC</ta>
            <ta e="T86" id="Seg_3102" s="T85">нести-PST.[3SG]</ta>
            <ta e="T87" id="Seg_3103" s="T86">там</ta>
            <ta e="T88" id="Seg_3104" s="T87">где</ta>
            <ta e="T89" id="Seg_3105" s="T88">спать-PRS.[3SG]</ta>
            <ta e="T90" id="Seg_3106" s="T89">тогда</ta>
            <ta e="T91" id="Seg_3107" s="T90">этот-ACC</ta>
            <ta e="T92" id="Seg_3108" s="T91">там</ta>
            <ta e="T93" id="Seg_3109" s="T92">посылать-PST.[3SG]</ta>
            <ta e="T94" id="Seg_3110" s="T93">этот.[NOM.SG]</ta>
            <ta e="T95" id="Seg_3111" s="T94">PTCL</ta>
            <ta e="T96" id="Seg_3112" s="T95">сидеть-PST.[3SG]</ta>
            <ta e="T97" id="Seg_3113" s="T96">сидеть-PST.[3SG]</ta>
            <ta e="T98" id="Seg_3114" s="T97">кричать-PST.[3SG]</ta>
            <ta e="T99" id="Seg_3115" s="T98">кричать-PST.[3SG]</ta>
            <ta e="T100" id="Seg_3116" s="T99">говорить-PST.[3SG]</ta>
            <ta e="T101" id="Seg_3117" s="T100">сказать-IPFVZ.[3SG]</ta>
            <ta e="T102" id="Seg_3118" s="T101">встать-IMP.2SG</ta>
            <ta e="T103" id="Seg_3119" s="T102">а</ta>
            <ta e="T104" id="Seg_3120" s="T103">этот.[NOM.SG]</ta>
            <ta e="T105" id="Seg_3121" s="T104">всегда</ta>
            <ta e="T106" id="Seg_3122" s="T105">NEG</ta>
            <ta e="T107" id="Seg_3123" s="T106">встать-PRS.[3SG]</ta>
            <ta e="T108" id="Seg_3124" s="T107">спать-DUR.[3SG]</ta>
            <ta e="T109" id="Seg_3125" s="T108">сильно</ta>
            <ta e="T110" id="Seg_3126" s="T109">тогда</ta>
            <ta e="T111" id="Seg_3127" s="T110">утро.[NOM.SG]</ta>
            <ta e="T112" id="Seg_3128" s="T111">стать-RES-PST.[3SG]</ta>
            <ta e="T113" id="Seg_3129" s="T112">этот.[NOM.SG]</ta>
            <ta e="T115" id="Seg_3130" s="T114">прийти-IMP.2SG</ta>
            <ta e="T116" id="Seg_3131" s="T115">здесь</ta>
            <ta e="T117" id="Seg_3132" s="T116">этот.[NOM.SG]</ta>
            <ta e="T118" id="Seg_3133" s="T117">прийти-PST.[3SG]</ta>
            <ta e="T329" id="Seg_3134" s="T118">пойти-CVB</ta>
            <ta e="T119" id="Seg_3135" s="T329">исчезнуть-PST.[3SG]</ta>
            <ta e="T120" id="Seg_3136" s="T119">тогда</ta>
            <ta e="T121" id="Seg_3137" s="T120">этот.[NOM.SG]</ta>
            <ta e="T122" id="Seg_3138" s="T121">встать-PST.[3SG]</ta>
            <ta e="T123" id="Seg_3139" s="T122">тогда</ta>
            <ta e="T124" id="Seg_3140" s="T123">опять</ta>
            <ta e="T125" id="Seg_3141" s="T124">сидеть-DUR.[3SG]</ta>
            <ta e="T126" id="Seg_3142" s="T125">и</ta>
            <ta e="T127" id="Seg_3143" s="T126">играть-DUR.[3SG]</ta>
            <ta e="T128" id="Seg_3144" s="T127">сидеть-</ta>
            <ta e="T129" id="Seg_3145" s="T128">сидеть-PST.[3SG]</ta>
            <ta e="T130" id="Seg_3146" s="T129">кораль-NOM/GEN/ACC.3PL</ta>
            <ta e="T131" id="Seg_3147" s="T130">край-LAT/LOC.3SG</ta>
            <ta e="T132" id="Seg_3148" s="T131">тогда</ta>
            <ta e="T133" id="Seg_3149" s="T132">INCH</ta>
            <ta e="T134" id="Seg_3150" s="T133">прясть-INF.LAT</ta>
            <ta e="T135" id="Seg_3151" s="T134">этот.[NOM.SG]</ta>
            <ta e="T136" id="Seg_3152" s="T135">кудель-ACC</ta>
            <ta e="T137" id="Seg_3153" s="T136">прясть-DUR.[3SG]</ta>
            <ta e="T138" id="Seg_3154" s="T137">прясть-DUR.[3SG]</ta>
            <ta e="T139" id="Seg_3155" s="T138">там</ta>
            <ta e="T140" id="Seg_3156" s="T139">этот.[NOM.SG]</ta>
            <ta e="T141" id="Seg_3157" s="T140">женщина.[NOM.SG]</ta>
            <ta e="T142" id="Seg_3158" s="T141">опять</ta>
            <ta e="T143" id="Seg_3159" s="T142">мужчина-GEN</ta>
            <ta e="T144" id="Seg_3160" s="T143">женщина.[NOM.SG]</ta>
            <ta e="T145" id="Seg_3161" s="T144">прийти-PST.[3SG]</ta>
            <ta e="T146" id="Seg_3162" s="T145">продавать-IMP.2SG.O</ta>
            <ta e="T147" id="Seg_3163" s="T146">я.LAT</ta>
            <ta e="T148" id="Seg_3164" s="T147">этот.[NOM.SG]</ta>
            <ta e="T149" id="Seg_3165" s="T148">нет</ta>
            <ta e="T150" id="Seg_3166" s="T149">я.NOM</ta>
            <ta e="T151" id="Seg_3167" s="T150">NEG</ta>
            <ta e="T152" id="Seg_3168" s="T151">продавать-FUT-1SG</ta>
            <ta e="T153" id="Seg_3169" s="T152">мужчина-ACC</ta>
            <ta e="T155" id="Seg_3170" s="T154">дать-FUT-2SG</ta>
            <ta e="T156" id="Seg_3171" s="T155">я.LAT</ta>
            <ta e="T157" id="Seg_3172" s="T156">вечер.[NOM.SG]</ta>
            <ta e="T158" id="Seg_3173" s="T157">спать-INF.LAT</ta>
            <ta e="T159" id="Seg_3174" s="T158">тогда</ta>
            <ta e="T160" id="Seg_3175" s="T159">дать-FUT-1SG</ta>
            <ta e="T161" id="Seg_3176" s="T160">ну</ta>
            <ta e="T162" id="Seg_3177" s="T161">взять-IMP.2SG.O</ta>
            <ta e="T163" id="Seg_3178" s="T162">дать-FUT-1SG</ta>
            <ta e="T165" id="Seg_3179" s="T163">тогда</ta>
            <ta e="T166" id="Seg_3180" s="T165">этот.[NOM.SG]</ta>
            <ta e="T167" id="Seg_3181" s="T166">прийти-PST.[3SG]</ta>
            <ta e="T168" id="Seg_3182" s="T167">этот.[NOM.SG]</ta>
            <ta e="T169" id="Seg_3183" s="T168">опять</ta>
            <ta e="T170" id="Seg_3184" s="T169">есть-INF.LAT</ta>
            <ta e="T171" id="Seg_3185" s="T170">сесть-PST.[3SG]</ta>
            <ta e="T172" id="Seg_3186" s="T171">этот.[NOM.SG]</ta>
            <ta e="T173" id="Seg_3187" s="T172">этот-LAT</ta>
            <ta e="T174" id="Seg_3188" s="T173">опять</ta>
            <ta e="T175" id="Seg_3189" s="T174">капля-PL</ta>
            <ta e="T176" id="Seg_3190" s="T175">дать-PST.[3SG]</ta>
            <ta e="T177" id="Seg_3191" s="T176">этот.[NOM.SG]</ta>
            <ta e="T178" id="Seg_3192" s="T177">спать-MOM-PST.[3SG]</ta>
            <ta e="T179" id="Seg_3193" s="T178">этот.[NOM.SG]</ta>
            <ta e="T180" id="Seg_3194" s="T179">этот-ACC</ta>
            <ta e="T181" id="Seg_3195" s="T180">дом-LAT</ta>
            <ta e="T182" id="Seg_3196" s="T181">пойти-PST.[3SG]</ta>
            <ta e="T183" id="Seg_3197" s="T182">где</ta>
            <ta e="T184" id="Seg_3198" s="T183">спать-DUR.[3SG]</ta>
            <ta e="T185" id="Seg_3199" s="T184">пойти-EP-IMP.2SG</ta>
            <ta e="T186" id="Seg_3200" s="T185">спать-CVB</ta>
            <ta e="T187" id="Seg_3201" s="T186">этот.[NOM.SG]</ta>
            <ta e="T188" id="Seg_3202" s="T187">там</ta>
            <ta e="T189" id="Seg_3203" s="T188">пойти-PST.[3SG]</ta>
            <ta e="T190" id="Seg_3204" s="T189">этот-ACC</ta>
            <ta e="T192" id="Seg_3205" s="T191">этот-LAT</ta>
            <ta e="T193" id="Seg_3206" s="T192">встать-IMP.2SG</ta>
            <ta e="T194" id="Seg_3207" s="T193">встать-IMP.2SG</ta>
            <ta e="T196" id="Seg_3208" s="T194">тогда</ta>
            <ta e="T197" id="Seg_3209" s="T196">как=INDEF</ta>
            <ta e="T198" id="Seg_3210" s="T197">NEG</ta>
            <ta e="T199" id="Seg_3211" s="T198">мочь-PRS.[3SG]</ta>
            <ta e="T200" id="Seg_3212" s="T199">встать-INF.LAT</ta>
            <ta e="T201" id="Seg_3213" s="T200">тогда</ta>
            <ta e="T202" id="Seg_3214" s="T201">утро</ta>
            <ta e="T203" id="Seg_3215" s="T202">стать-RES-PST.[3SG]</ta>
            <ta e="T204" id="Seg_3216" s="T203">этот.[NOM.SG]</ta>
            <ta e="T205" id="Seg_3217" s="T204">сказать-IPFVZ.[3SG]</ta>
            <ta e="T206" id="Seg_3218" s="T205">пойти-EP-IMP.2SG</ta>
            <ta e="T207" id="Seg_3219" s="T206">хватит</ta>
            <ta e="T208" id="Seg_3220" s="T207">ты.DAT</ta>
            <ta e="T209" id="Seg_3221" s="T208">здесь</ta>
            <ta e="T210" id="Seg_3222" s="T209">сидеть-INF.LAT</ta>
            <ta e="T211" id="Seg_3223" s="T210">тогда</ta>
            <ta e="T212" id="Seg_3224" s="T211">этот.[NOM.SG]</ta>
            <ta e="T213" id="Seg_3225" s="T212">пойти-PST.[3SG]</ta>
            <ta e="T214" id="Seg_3226" s="T213">этот.[NOM.SG]</ta>
            <ta e="T215" id="Seg_3227" s="T214">встать-PST.[3SG]</ta>
            <ta e="T216" id="Seg_3228" s="T215">тогда</ta>
            <ta e="T217" id="Seg_3229" s="T216">опять</ta>
            <ta e="T219" id="Seg_3230" s="T218">кораль-LOC</ta>
            <ta e="T220" id="Seg_3231" s="T219">сидеть-DUR.[3SG]</ta>
            <ta e="T221" id="Seg_3232" s="T220">%веретено.[NOM.SG]</ta>
            <ta e="T222" id="Seg_3233" s="T221">поставить-PST.[3SG]</ta>
            <ta e="T223" id="Seg_3234" s="T222">красивый.[NOM.SG]</ta>
            <ta e="T226" id="Seg_3235" s="T225">рука-LAT/LOC.3SG</ta>
            <ta e="T227" id="Seg_3236" s="T226">рука-LAT/LOC.3SG</ta>
            <ta e="T228" id="Seg_3237" s="T227">надеть-PRS.[3SG]</ta>
            <ta e="T229" id="Seg_3238" s="T228">то</ta>
            <ta e="T230" id="Seg_3239" s="T229">тот.[NOM.SG]</ta>
            <ta e="T231" id="Seg_3240" s="T230">рука-LAT/LOC.3SG</ta>
            <ta e="T232" id="Seg_3241" s="T231">то</ta>
            <ta e="T233" id="Seg_3242" s="T232">тот.[NOM.SG]</ta>
            <ta e="T235" id="Seg_3243" s="T233">рука-LAT/LOC.3SG</ta>
            <ta e="T236" id="Seg_3244" s="T235">этот.[NOM.SG]</ta>
            <ta e="T237" id="Seg_3245" s="T236">видеть-DUR.PST.[3SG]</ta>
            <ta e="T238" id="Seg_3246" s="T237">и</ta>
            <ta e="T239" id="Seg_3247" s="T238">пойти-PST.[3SG]</ta>
            <ta e="T240" id="Seg_3248" s="T239">продавать-IMP.2SG.O</ta>
            <ta e="T241" id="Seg_3249" s="T240">я.LAT</ta>
            <ta e="T242" id="Seg_3250" s="T241">нет</ta>
            <ta e="T243" id="Seg_3251" s="T242">я.NOM</ta>
            <ta e="T244" id="Seg_3252" s="T243">NEG</ta>
            <ta e="T245" id="Seg_3253" s="T244">продавать-PRS-1SG</ta>
            <ta e="T246" id="Seg_3254" s="T245">мужчина.[NOM.SG]</ta>
            <ta e="T247" id="Seg_3255" s="T246">дать-IMP.2SG</ta>
            <ta e="T248" id="Seg_3256" s="T247">я.LAT</ta>
            <ta e="T249" id="Seg_3257" s="T248">сегодня</ta>
            <ta e="T250" id="Seg_3258" s="T249">ночевать-INF.LAT</ta>
            <ta e="T251" id="Seg_3259" s="T250">спать-INF.LAT</ta>
            <ta e="T252" id="Seg_3260" s="T251">ну</ta>
            <ta e="T253" id="Seg_3261" s="T252">взять-IMP.2SG</ta>
            <ta e="T254" id="Seg_3262" s="T253">тогда</ta>
            <ta e="T255" id="Seg_3263" s="T254">этот-LAT</ta>
            <ta e="T256" id="Seg_3264" s="T255">дать-PST.[3SG]</ta>
            <ta e="T257" id="Seg_3265" s="T256">колечко.[NOM.SG]</ta>
            <ta e="T258" id="Seg_3266" s="T257">и</ta>
            <ta e="T259" id="Seg_3267" s="T258">%веретено.[NOM.SG]</ta>
            <ta e="T260" id="Seg_3268" s="T259">тогда</ta>
            <ta e="T261" id="Seg_3269" s="T260">мужчина.[NOM.SG]</ta>
            <ta e="T262" id="Seg_3270" s="T261">прийти-PST.[3SG]</ta>
            <ta e="T263" id="Seg_3271" s="T262">есть-INF.LAT</ta>
            <ta e="T264" id="Seg_3272" s="T263">сидеть-PST.[3SG]</ta>
            <ta e="T265" id="Seg_3273" s="T264">этот.[NOM.SG]</ta>
            <ta e="T266" id="Seg_3274" s="T265">этот-LAT</ta>
            <ta e="T267" id="Seg_3275" s="T266">капля-PL</ta>
            <ta e="T268" id="Seg_3276" s="T267">опять</ta>
            <ta e="T269" id="Seg_3277" s="T268">дать-PST.[3SG]</ta>
            <ta e="T270" id="Seg_3278" s="T269">этот.[NOM.SG]</ta>
            <ta e="T271" id="Seg_3279" s="T270">спать-MOM-PST.[3SG]</ta>
            <ta e="T272" id="Seg_3280" s="T271">этот.[NOM.SG]</ta>
            <ta e="T273" id="Seg_3281" s="T272">опять</ta>
            <ta e="T274" id="Seg_3282" s="T273">дом-LAT</ta>
            <ta e="T275" id="Seg_3283" s="T274">класть-PST.[3SG]</ta>
            <ta e="T276" id="Seg_3284" s="T275">этот-ACC</ta>
            <ta e="T277" id="Seg_3285" s="T276">и</ta>
            <ta e="T278" id="Seg_3286" s="T277">этот-ACC</ta>
            <ta e="T279" id="Seg_3287" s="T278">посылать-PST.[3SG]</ta>
            <ta e="T280" id="Seg_3288" s="T279">этот.[NOM.SG]</ta>
            <ta e="T281" id="Seg_3289" s="T280">там</ta>
            <ta e="T282" id="Seg_3290" s="T281">пойти-PST.[3SG]</ta>
            <ta e="T283" id="Seg_3291" s="T282">опять</ta>
            <ta e="T284" id="Seg_3292" s="T283">плакать-PST.[3SG]</ta>
            <ta e="T285" id="Seg_3293" s="T284">этот-ACC</ta>
            <ta e="T286" id="Seg_3294" s="T285">PTCL</ta>
            <ta e="T287" id="Seg_3295" s="T286">встать-PRS.[3SG]</ta>
            <ta e="T288" id="Seg_3296" s="T287">поднимать-PRS.[3SG]</ta>
            <ta e="T289" id="Seg_3297" s="T288">этот-ACC</ta>
            <ta e="T290" id="Seg_3298" s="T289">плакать-PST.[3SG]</ta>
            <ta e="T291" id="Seg_3299" s="T290">плакать-PST.[3SG]</ta>
            <ta e="T292" id="Seg_3300" s="T291">слеза-NOM/GEN/ACC.3SG</ta>
            <ta e="T293" id="Seg_3301" s="T292">упасть-MOM-PST.[3SG]</ta>
            <ta e="T294" id="Seg_3302" s="T293">лицо-LAT/LOC.3SG</ta>
            <ta e="T295" id="Seg_3303" s="T294">этот.[NOM.SG]</ta>
            <ta e="T296" id="Seg_3304" s="T295">PTCL</ta>
            <ta e="T297" id="Seg_3305" s="T296">просыпаться-MOM-PST.[3SG]</ta>
            <ta e="T298" id="Seg_3306" s="T297">тогда</ta>
            <ta e="T300" id="Seg_3307" s="T299">обнимать-MOM-PST-3PL</ta>
            <ta e="T301" id="Seg_3308" s="T300">тогда</ta>
            <ta e="T302" id="Seg_3309" s="T301">утро</ta>
            <ta e="T303" id="Seg_3310" s="T302">стать-RES-PST.[3SG]</ta>
            <ta e="T304" id="Seg_3311" s="T303">этот.[NOM.SG]</ta>
            <ta e="T305" id="Seg_3312" s="T304">прийти-PST.[3SG]</ta>
            <ta e="T306" id="Seg_3313" s="T305">а</ta>
            <ta e="T307" id="Seg_3314" s="T306">ты.NOM</ta>
            <ta e="T308" id="Seg_3315" s="T307">я.LAT</ta>
            <ta e="T309" id="Seg_3316" s="T308">продавать-PST-2SG</ta>
            <ta e="T310" id="Seg_3317" s="T309">так</ta>
            <ta e="T311" id="Seg_3318" s="T310">ты.NOM</ta>
            <ta e="T312" id="Seg_3319" s="T311">я.NOM</ta>
            <ta e="T313" id="Seg_3320" s="T312">NEG</ta>
            <ta e="T314" id="Seg_3321" s="T313">я.NOM</ta>
            <ta e="T315" id="Seg_3322" s="T314">женщина-ACC</ta>
            <ta e="T316" id="Seg_3323" s="T315">а</ta>
            <ta e="T317" id="Seg_3324" s="T316">тот.[NOM.SG]</ta>
            <ta e="T318" id="Seg_3325" s="T317">я.NOM</ta>
            <ta e="T319" id="Seg_3326" s="T318">женщина-ACC</ta>
            <ta e="T320" id="Seg_3327" s="T319">тогда</ta>
            <ta e="T321" id="Seg_3328" s="T320">пол-NOM/GEN/ACC.3SG</ta>
            <ta e="T322" id="Seg_3329" s="T321">упасть-MOM-PST-1PL</ta>
            <ta e="T323" id="Seg_3330" s="T322">упасть-MOM-PST-3PL</ta>
            <ta e="T324" id="Seg_3331" s="T323">птица-INS</ta>
            <ta e="T325" id="Seg_3332" s="T324">стать-DUR-PST-3PL</ta>
            <ta e="T326" id="Seg_3333" s="T325">и</ta>
            <ta e="T327" id="Seg_3334" s="T326">лететь-MOM-PST-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_3335" s="T0">adv</ta>
            <ta e="T2" id="Seg_3336" s="T1">dempro-n:case</ta>
            <ta e="T3" id="Seg_3337" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_3338" s="T3">n-n:case-poss</ta>
            <ta e="T5" id="Seg_3339" s="T4">v-v:tense-v:pn</ta>
            <ta e="T6" id="Seg_3340" s="T5">que</ta>
            <ta e="T7" id="Seg_3341" s="T6">dempro-n:case</ta>
            <ta e="T8" id="Seg_3342" s="T7">v-v:tense-v:pn</ta>
            <ta e="T9" id="Seg_3343" s="T8">conj</ta>
            <ta e="T10" id="Seg_3344" s="T9">dempro-n:case</ta>
            <ta e="T11" id="Seg_3345" s="T10">dempro-n:case</ta>
            <ta e="T13" id="Seg_3346" s="T12">dempro-n:case</ta>
            <ta e="T15" id="Seg_3347" s="T14">v</ta>
            <ta e="T16" id="Seg_3348" s="T15">v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_3349" s="T16">n-n:case-poss</ta>
            <ta e="T18" id="Seg_3350" s="T17">v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_3351" s="T18">v-v:tense-v:pn</ta>
            <ta e="T20" id="Seg_3352" s="T19">conj</ta>
            <ta e="T21" id="Seg_3353" s="T20">v-v:tense-v:pn</ta>
            <ta e="T22" id="Seg_3354" s="T21">adv</ta>
            <ta e="T23" id="Seg_3355" s="T22">que</ta>
            <ta e="T24" id="Seg_3356" s="T23">dempro-n:case</ta>
            <ta e="T25" id="Seg_3357" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_3358" s="T25">v-v&gt;v-v:pn</ta>
            <ta e="T27" id="Seg_3359" s="T26">dempro</ta>
            <ta e="T28" id="Seg_3360" s="T27">dempro-n:case</ta>
            <ta e="T29" id="Seg_3361" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_3362" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_3363" s="T30">v-v:tense-v:pn</ta>
            <ta e="T32" id="Seg_3364" s="T31">v-v:tense-v:pn</ta>
            <ta e="T33" id="Seg_3365" s="T32">v-v:n-fin</ta>
            <ta e="T34" id="Seg_3366" s="T33">adv</ta>
            <ta e="T35" id="Seg_3367" s="T34">adv</ta>
            <ta e="T36" id="Seg_3368" s="T35">v</ta>
            <ta e="T37" id="Seg_3369" s="T36">v-v:tense-v:pn</ta>
            <ta e="T38" id="Seg_3370" s="T37">conj</ta>
            <ta e="T39" id="Seg_3371" s="T38">n-n:case-poss</ta>
            <ta e="T40" id="Seg_3372" s="T39">v-v:tense-v:pn</ta>
            <ta e="T41" id="Seg_3373" s="T40">conj</ta>
            <ta e="T42" id="Seg_3374" s="T41">n-n:num-n:case</ta>
            <ta e="T44" id="Seg_3375" s="T43">conj</ta>
            <ta e="T45" id="Seg_3376" s="T44">ptcl</ta>
            <ta e="T46" id="Seg_3377" s="T45">v-v:n-fin</ta>
            <ta e="T47" id="Seg_3378" s="T46">v-v&gt;v-v:pn</ta>
            <ta e="T48" id="Seg_3379" s="T47">conj</ta>
            <ta e="T49" id="Seg_3380" s="T48">dempro-n:case</ta>
            <ta e="T50" id="Seg_3381" s="T49">n-n:case</ta>
            <ta e="T51" id="Seg_3382" s="T50">n-n:case-poss</ta>
            <ta e="T52" id="Seg_3383" s="T51">v-v:mood-pn</ta>
            <ta e="T53" id="Seg_3384" s="T52">pers</ta>
            <ta e="T54" id="Seg_3385" s="T53">conj</ta>
            <ta e="T55" id="Seg_3386" s="T54">dempro-n:case</ta>
            <ta e="T56" id="Seg_3387" s="T55">pers</ta>
            <ta e="T57" id="Seg_3388" s="T56">v-v&gt;v-v:pn</ta>
            <ta e="T58" id="Seg_3389" s="T57">v-v:tense-v:pn</ta>
            <ta e="T59" id="Seg_3390" s="T58">pers</ta>
            <ta e="T60" id="Seg_3391" s="T59">n-n:case</ta>
            <ta e="T61" id="Seg_3392" s="T60">pers</ta>
            <ta e="T62" id="Seg_3393" s="T61">v-v:n-fin</ta>
            <ta e="T64" id="Seg_3394" s="T62">dempro-n:case</ta>
            <ta e="T65" id="Seg_3395" s="T64">dempro-n:case</ta>
            <ta e="T66" id="Seg_3396" s="T65">v-v:tense-v:pn</ta>
            <ta e="T67" id="Seg_3397" s="T66">v-v:ins-v:mood-pn</ta>
            <ta e="T68" id="Seg_3398" s="T67">que-n:case</ta>
            <ta e="T69" id="Seg_3399" s="T68">pers</ta>
            <ta e="T70" id="Seg_3400" s="T69">v-v:tense-v:pn</ta>
            <ta e="T71" id="Seg_3401" s="T70">ptcl</ta>
            <ta e="T72" id="Seg_3402" s="T71">dempro-n:case</ta>
            <ta e="T73" id="Seg_3403" s="T72">v-v:tense-v:pn</ta>
            <ta e="T74" id="Seg_3404" s="T73">n-n:case-poss</ta>
            <ta e="T75" id="Seg_3405" s="T74">v-v:tense-v:pn</ta>
            <ta e="T76" id="Seg_3406" s="T75">v-v:n-fin</ta>
            <ta e="T77" id="Seg_3407" s="T76">dempro-n:case</ta>
            <ta e="T78" id="Seg_3408" s="T77">dempro-n:case</ta>
            <ta e="T80" id="Seg_3409" s="T79">v-v:tense-v:pn</ta>
            <ta e="T81" id="Seg_3410" s="T80">n-n:num</ta>
            <ta e="T82" id="Seg_3411" s="T81">dempro-n:case</ta>
            <ta e="T83" id="Seg_3412" s="T82">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T84" id="Seg_3413" s="T83">dempro-n:case</ta>
            <ta e="T85" id="Seg_3414" s="T84">dempro-n:case</ta>
            <ta e="T86" id="Seg_3415" s="T85">v-v:tense-v:pn</ta>
            <ta e="T87" id="Seg_3416" s="T86">adv</ta>
            <ta e="T88" id="Seg_3417" s="T87">que</ta>
            <ta e="T89" id="Seg_3418" s="T88">v-v:tense-v:pn</ta>
            <ta e="T90" id="Seg_3419" s="T89">adv</ta>
            <ta e="T91" id="Seg_3420" s="T90">dempro-n:case</ta>
            <ta e="T92" id="Seg_3421" s="T91">adv</ta>
            <ta e="T93" id="Seg_3422" s="T92">v-v:tense-v:pn</ta>
            <ta e="T94" id="Seg_3423" s="T93">dempro-n:case</ta>
            <ta e="T95" id="Seg_3424" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_3425" s="T95">v-v:tense-v:pn</ta>
            <ta e="T97" id="Seg_3426" s="T96">v-v:tense-v:pn</ta>
            <ta e="T98" id="Seg_3427" s="T97">v-v:tense-v:pn</ta>
            <ta e="T99" id="Seg_3428" s="T98">v-v:tense-v:pn</ta>
            <ta e="T100" id="Seg_3429" s="T99">v-v:tense-v:pn</ta>
            <ta e="T101" id="Seg_3430" s="T100">v-v&gt;v-v:pn</ta>
            <ta e="T102" id="Seg_3431" s="T101">v-v:mood-pn</ta>
            <ta e="T103" id="Seg_3432" s="T102">conj</ta>
            <ta e="T104" id="Seg_3433" s="T103">dempro-n:case</ta>
            <ta e="T105" id="Seg_3434" s="T104">adv</ta>
            <ta e="T106" id="Seg_3435" s="T105">ptcl</ta>
            <ta e="T107" id="Seg_3436" s="T106">v-v:tense-v:pn</ta>
            <ta e="T108" id="Seg_3437" s="T107">v-v&gt;v-v:pn</ta>
            <ta e="T109" id="Seg_3438" s="T108">adv</ta>
            <ta e="T110" id="Seg_3439" s="T109">adv</ta>
            <ta e="T111" id="Seg_3440" s="T110">n-n:case</ta>
            <ta e="T112" id="Seg_3441" s="T111">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T113" id="Seg_3442" s="T112">dempro-n:case</ta>
            <ta e="T115" id="Seg_3443" s="T114">v-v:mood-pn</ta>
            <ta e="T116" id="Seg_3444" s="T115">adv</ta>
            <ta e="T117" id="Seg_3445" s="T116">dempro-n:case</ta>
            <ta e="T118" id="Seg_3446" s="T117">v-v:tense-v:pn</ta>
            <ta e="T329" id="Seg_3447" s="T118">v-v:n-fin</ta>
            <ta e="T119" id="Seg_3448" s="T329">v-v:tense-v:pn</ta>
            <ta e="T120" id="Seg_3449" s="T119">adv</ta>
            <ta e="T121" id="Seg_3450" s="T120">dempro-n:case</ta>
            <ta e="T122" id="Seg_3451" s="T121">v-v:tense-v:pn</ta>
            <ta e="T123" id="Seg_3452" s="T122">adv</ta>
            <ta e="T124" id="Seg_3453" s="T123">adv</ta>
            <ta e="T125" id="Seg_3454" s="T124">v-v&gt;v-v:pn</ta>
            <ta e="T126" id="Seg_3455" s="T125">conj</ta>
            <ta e="T127" id="Seg_3456" s="T126">v-v&gt;v-v:pn</ta>
            <ta e="T128" id="Seg_3457" s="T127">v</ta>
            <ta e="T129" id="Seg_3458" s="T128">v-v:tense-v:pn</ta>
            <ta e="T130" id="Seg_3459" s="T129">n-n:case-poss</ta>
            <ta e="T131" id="Seg_3460" s="T130">n-n:case-poss</ta>
            <ta e="T132" id="Seg_3461" s="T131">adv</ta>
            <ta e="T133" id="Seg_3462" s="T132">ptcl</ta>
            <ta e="T134" id="Seg_3463" s="T133">v-v:n-fin</ta>
            <ta e="T135" id="Seg_3464" s="T134">dempro-n:case</ta>
            <ta e="T136" id="Seg_3465" s="T135">n-n:case</ta>
            <ta e="T137" id="Seg_3466" s="T136">v-v&gt;v-v:pn</ta>
            <ta e="T138" id="Seg_3467" s="T137">v-v&gt;v-v:pn</ta>
            <ta e="T139" id="Seg_3468" s="T138">adv</ta>
            <ta e="T140" id="Seg_3469" s="T139">dempro-n:case</ta>
            <ta e="T141" id="Seg_3470" s="T140">n-n:case</ta>
            <ta e="T142" id="Seg_3471" s="T141">adv</ta>
            <ta e="T143" id="Seg_3472" s="T142">n-n:case</ta>
            <ta e="T144" id="Seg_3473" s="T143">n-n:case</ta>
            <ta e="T145" id="Seg_3474" s="T144">v-v:tense-v:pn</ta>
            <ta e="T146" id="Seg_3475" s="T145">v-v:mood-pn</ta>
            <ta e="T147" id="Seg_3476" s="T146">pers</ta>
            <ta e="T148" id="Seg_3477" s="T147">dempro-n:case</ta>
            <ta e="T149" id="Seg_3478" s="T148">ptcl</ta>
            <ta e="T150" id="Seg_3479" s="T149">pers</ta>
            <ta e="T151" id="Seg_3480" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_3481" s="T151">v-v:tense-v:pn</ta>
            <ta e="T153" id="Seg_3482" s="T152">n-n:case</ta>
            <ta e="T155" id="Seg_3483" s="T154">v-v:tense-v:pn</ta>
            <ta e="T156" id="Seg_3484" s="T155">pers</ta>
            <ta e="T157" id="Seg_3485" s="T156">n-n:case</ta>
            <ta e="T158" id="Seg_3486" s="T157">v-v:n-fin</ta>
            <ta e="T159" id="Seg_3487" s="T158">adv</ta>
            <ta e="T160" id="Seg_3488" s="T159">v-v:tense-v:pn</ta>
            <ta e="T161" id="Seg_3489" s="T160">ptcl</ta>
            <ta e="T162" id="Seg_3490" s="T161">v-v:mood-pn</ta>
            <ta e="T163" id="Seg_3491" s="T162">v-v:tense-v:pn</ta>
            <ta e="T165" id="Seg_3492" s="T163">adv</ta>
            <ta e="T166" id="Seg_3493" s="T165">dempro-n:case</ta>
            <ta e="T167" id="Seg_3494" s="T166">v-v:tense-v:pn</ta>
            <ta e="T168" id="Seg_3495" s="T167">dempro-n:case</ta>
            <ta e="T169" id="Seg_3496" s="T168">adv</ta>
            <ta e="T170" id="Seg_3497" s="T169">v-v:n-fin</ta>
            <ta e="T171" id="Seg_3498" s="T170">v-v:tense-v:pn</ta>
            <ta e="T172" id="Seg_3499" s="T171">dempro-n:case</ta>
            <ta e="T173" id="Seg_3500" s="T172">dempro-n:case</ta>
            <ta e="T174" id="Seg_3501" s="T173">adv</ta>
            <ta e="T175" id="Seg_3502" s="T174">n-n:num</ta>
            <ta e="T176" id="Seg_3503" s="T175">v-v:tense-v:pn</ta>
            <ta e="T177" id="Seg_3504" s="T176">dempro-n:case</ta>
            <ta e="T178" id="Seg_3505" s="T177">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T179" id="Seg_3506" s="T178">dempro-n:case</ta>
            <ta e="T180" id="Seg_3507" s="T179">dempro-n:case</ta>
            <ta e="T181" id="Seg_3508" s="T180">n-n:case</ta>
            <ta e="T182" id="Seg_3509" s="T181">v-v:tense-v:pn</ta>
            <ta e="T183" id="Seg_3510" s="T182">que</ta>
            <ta e="T184" id="Seg_3511" s="T183">v-v&gt;v-v:pn</ta>
            <ta e="T185" id="Seg_3512" s="T184">v-v:ins-v:mood-pn</ta>
            <ta e="T186" id="Seg_3513" s="T185">v-v:n-fin</ta>
            <ta e="T187" id="Seg_3514" s="T186">dempro-n:case</ta>
            <ta e="T188" id="Seg_3515" s="T187">adv</ta>
            <ta e="T189" id="Seg_3516" s="T188">v-v:tense-v:pn</ta>
            <ta e="T190" id="Seg_3517" s="T189">dempro-n:case</ta>
            <ta e="T192" id="Seg_3518" s="T191">dempro-n:case</ta>
            <ta e="T193" id="Seg_3519" s="T192">v-v:mood-pn</ta>
            <ta e="T194" id="Seg_3520" s="T193">v-v:mood-pn</ta>
            <ta e="T196" id="Seg_3521" s="T194">adv</ta>
            <ta e="T197" id="Seg_3522" s="T196">que=ptcl</ta>
            <ta e="T198" id="Seg_3523" s="T197">ptcl</ta>
            <ta e="T199" id="Seg_3524" s="T198">v-v:tense-v:pn</ta>
            <ta e="T200" id="Seg_3525" s="T199">v-v:n-fin</ta>
            <ta e="T201" id="Seg_3526" s="T200">adv</ta>
            <ta e="T202" id="Seg_3527" s="T201">n</ta>
            <ta e="T203" id="Seg_3528" s="T202">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T204" id="Seg_3529" s="T203">dempro-n:case</ta>
            <ta e="T205" id="Seg_3530" s="T204">v-v&gt;v-v:pn</ta>
            <ta e="T206" id="Seg_3531" s="T205">v-v:ins-v:mood-pn</ta>
            <ta e="T207" id="Seg_3532" s="T206">ptcl</ta>
            <ta e="T208" id="Seg_3533" s="T207">pers</ta>
            <ta e="T209" id="Seg_3534" s="T208">adv</ta>
            <ta e="T210" id="Seg_3535" s="T209">v-v:n-fin</ta>
            <ta e="T211" id="Seg_3536" s="T210">adv</ta>
            <ta e="T212" id="Seg_3537" s="T211">dempro-n:case</ta>
            <ta e="T213" id="Seg_3538" s="T212">v-v:tense-v:pn</ta>
            <ta e="T214" id="Seg_3539" s="T213">dempro-n:case</ta>
            <ta e="T215" id="Seg_3540" s="T214">v-v:tense-v:pn</ta>
            <ta e="T216" id="Seg_3541" s="T215">adv</ta>
            <ta e="T217" id="Seg_3542" s="T216">adv</ta>
            <ta e="T219" id="Seg_3543" s="T218">n-n:case</ta>
            <ta e="T220" id="Seg_3544" s="T219">v-v&gt;v-v:pn</ta>
            <ta e="T221" id="Seg_3545" s="T220">n-n:case</ta>
            <ta e="T222" id="Seg_3546" s="T221">v-v:tense-v:pn</ta>
            <ta e="T223" id="Seg_3547" s="T222">adj-n:case</ta>
            <ta e="T226" id="Seg_3548" s="T225">n-n:case-poss</ta>
            <ta e="T227" id="Seg_3549" s="T226">n-n:case-poss</ta>
            <ta e="T228" id="Seg_3550" s="T227">v-v:tense-v:pn</ta>
            <ta e="T229" id="Seg_3551" s="T228">conj</ta>
            <ta e="T230" id="Seg_3552" s="T229">dempro-n:case</ta>
            <ta e="T231" id="Seg_3553" s="T230">n-n:case-poss</ta>
            <ta e="T232" id="Seg_3554" s="T231">conj</ta>
            <ta e="T233" id="Seg_3555" s="T232">dempro-n:case</ta>
            <ta e="T235" id="Seg_3556" s="T233">n-n:case-poss</ta>
            <ta e="T236" id="Seg_3557" s="T235">dempro-n:case</ta>
            <ta e="T237" id="Seg_3558" s="T236">v-v:tense-v:pn</ta>
            <ta e="T238" id="Seg_3559" s="T237">conj</ta>
            <ta e="T239" id="Seg_3560" s="T238">v-v:tense-v:pn</ta>
            <ta e="T240" id="Seg_3561" s="T239">v-v:mood-pn</ta>
            <ta e="T241" id="Seg_3562" s="T240">pers</ta>
            <ta e="T242" id="Seg_3563" s="T241">ptcl</ta>
            <ta e="T243" id="Seg_3564" s="T242">pers</ta>
            <ta e="T244" id="Seg_3565" s="T243">ptcl</ta>
            <ta e="T245" id="Seg_3566" s="T244">v-v:tense-v:pn</ta>
            <ta e="T246" id="Seg_3567" s="T245">n-n:case</ta>
            <ta e="T247" id="Seg_3568" s="T246">v-v:mood-pn</ta>
            <ta e="T248" id="Seg_3569" s="T247">pers</ta>
            <ta e="T249" id="Seg_3570" s="T248">adv</ta>
            <ta e="T250" id="Seg_3571" s="T249">v-v:n-fin</ta>
            <ta e="T251" id="Seg_3572" s="T250">v-v:n-fin</ta>
            <ta e="T252" id="Seg_3573" s="T251">ptcl</ta>
            <ta e="T253" id="Seg_3574" s="T252">v-v:mood-pn</ta>
            <ta e="T254" id="Seg_3575" s="T253">adv</ta>
            <ta e="T255" id="Seg_3576" s="T254">dempro-n:case</ta>
            <ta e="T256" id="Seg_3577" s="T255">v-v:tense-v:pn</ta>
            <ta e="T257" id="Seg_3578" s="T256">n-n:case</ta>
            <ta e="T258" id="Seg_3579" s="T257">conj</ta>
            <ta e="T259" id="Seg_3580" s="T258">n-n:case</ta>
            <ta e="T260" id="Seg_3581" s="T259">adv</ta>
            <ta e="T261" id="Seg_3582" s="T260">n-n:case</ta>
            <ta e="T262" id="Seg_3583" s="T261">v-v:tense-v:pn</ta>
            <ta e="T263" id="Seg_3584" s="T262">v-v:n-fin</ta>
            <ta e="T264" id="Seg_3585" s="T263">v-v:tense-v:pn</ta>
            <ta e="T265" id="Seg_3586" s="T264">dempro-n:case</ta>
            <ta e="T266" id="Seg_3587" s="T265">dempro-n:case</ta>
            <ta e="T267" id="Seg_3588" s="T266">n-n:num</ta>
            <ta e="T268" id="Seg_3589" s="T267">adv</ta>
            <ta e="T269" id="Seg_3590" s="T268">v-v:tense-v:pn</ta>
            <ta e="T270" id="Seg_3591" s="T269">dempro-n:case</ta>
            <ta e="T271" id="Seg_3592" s="T270">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T272" id="Seg_3593" s="T271">dempro-n:case</ta>
            <ta e="T273" id="Seg_3594" s="T272">adv</ta>
            <ta e="T274" id="Seg_3595" s="T273">n-n:case</ta>
            <ta e="T275" id="Seg_3596" s="T274">v-v:tense-v:pn</ta>
            <ta e="T276" id="Seg_3597" s="T275">dempro-n:case</ta>
            <ta e="T277" id="Seg_3598" s="T276">conj</ta>
            <ta e="T278" id="Seg_3599" s="T277">dempro-n:case</ta>
            <ta e="T279" id="Seg_3600" s="T278">v-v:tense-v:pn</ta>
            <ta e="T280" id="Seg_3601" s="T279">dempro-n:case</ta>
            <ta e="T281" id="Seg_3602" s="T280">adv</ta>
            <ta e="T282" id="Seg_3603" s="T281">v-v:tense-v:pn</ta>
            <ta e="T283" id="Seg_3604" s="T282">adv</ta>
            <ta e="T284" id="Seg_3605" s="T283">v-v:tense-v:pn</ta>
            <ta e="T285" id="Seg_3606" s="T284">dempro-n:case</ta>
            <ta e="T286" id="Seg_3607" s="T285">ptcl</ta>
            <ta e="T287" id="Seg_3608" s="T286">v-v:tense-v:pn</ta>
            <ta e="T288" id="Seg_3609" s="T287">v-v:tense-v:pn</ta>
            <ta e="T289" id="Seg_3610" s="T288">dempro-n:case</ta>
            <ta e="T290" id="Seg_3611" s="T289">v-v:tense-v:pn</ta>
            <ta e="T291" id="Seg_3612" s="T290">v-v:tense-v:pn</ta>
            <ta e="T292" id="Seg_3613" s="T291">n-n:case-poss</ta>
            <ta e="T293" id="Seg_3614" s="T292">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T294" id="Seg_3615" s="T293">n-n:case-poss</ta>
            <ta e="T295" id="Seg_3616" s="T294">dempro-n:case</ta>
            <ta e="T296" id="Seg_3617" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_3618" s="T296">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T298" id="Seg_3619" s="T297">adv</ta>
            <ta e="T300" id="Seg_3620" s="T299">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T301" id="Seg_3621" s="T300">adv</ta>
            <ta e="T302" id="Seg_3622" s="T301">n</ta>
            <ta e="T303" id="Seg_3623" s="T302">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T304" id="Seg_3624" s="T303">dempro-n:case</ta>
            <ta e="T305" id="Seg_3625" s="T304">v-v:tense-v:pn</ta>
            <ta e="T306" id="Seg_3626" s="T305">conj</ta>
            <ta e="T307" id="Seg_3627" s="T306">pers</ta>
            <ta e="T308" id="Seg_3628" s="T307">pers</ta>
            <ta e="T309" id="Seg_3629" s="T308">v-v:tense-v:pn</ta>
            <ta e="T310" id="Seg_3630" s="T309">ptcl</ta>
            <ta e="T311" id="Seg_3631" s="T310">pers</ta>
            <ta e="T312" id="Seg_3632" s="T311">pers</ta>
            <ta e="T313" id="Seg_3633" s="T312">ptcl</ta>
            <ta e="T314" id="Seg_3634" s="T313">pers</ta>
            <ta e="T315" id="Seg_3635" s="T314">n-n:case</ta>
            <ta e="T316" id="Seg_3636" s="T315">conj</ta>
            <ta e="T317" id="Seg_3637" s="T316">dempro-n:case</ta>
            <ta e="T318" id="Seg_3638" s="T317">pers</ta>
            <ta e="T319" id="Seg_3639" s="T318">n-n:case</ta>
            <ta e="T320" id="Seg_3640" s="T319">adv</ta>
            <ta e="T321" id="Seg_3641" s="T320">n-n:case-poss</ta>
            <ta e="T322" id="Seg_3642" s="T321">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T323" id="Seg_3643" s="T322">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T324" id="Seg_3644" s="T323">n-n:case</ta>
            <ta e="T325" id="Seg_3645" s="T324">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T326" id="Seg_3646" s="T325">conj</ta>
            <ta e="T327" id="Seg_3647" s="T326">v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_3648" s="T0">adv</ta>
            <ta e="T2" id="Seg_3649" s="T1">dempro</ta>
            <ta e="T3" id="Seg_3650" s="T2">v</ta>
            <ta e="T4" id="Seg_3651" s="T3">n</ta>
            <ta e="T5" id="Seg_3652" s="T4">v</ta>
            <ta e="T6" id="Seg_3653" s="T5">que</ta>
            <ta e="T7" id="Seg_3654" s="T6">dempro</ta>
            <ta e="T8" id="Seg_3655" s="T7">v</ta>
            <ta e="T9" id="Seg_3656" s="T8">conj</ta>
            <ta e="T10" id="Seg_3657" s="T9">dempro</ta>
            <ta e="T11" id="Seg_3658" s="T10">dempro</ta>
            <ta e="T13" id="Seg_3659" s="T12">dempro</ta>
            <ta e="T15" id="Seg_3660" s="T14">v</ta>
            <ta e="T16" id="Seg_3661" s="T15">v</ta>
            <ta e="T17" id="Seg_3662" s="T16">n</ta>
            <ta e="T18" id="Seg_3663" s="T17">v</ta>
            <ta e="T19" id="Seg_3664" s="T18">v</ta>
            <ta e="T20" id="Seg_3665" s="T19">conj</ta>
            <ta e="T21" id="Seg_3666" s="T20">v</ta>
            <ta e="T22" id="Seg_3667" s="T21">adv</ta>
            <ta e="T23" id="Seg_3668" s="T22">que</ta>
            <ta e="T24" id="Seg_3669" s="T23">dempro</ta>
            <ta e="T25" id="Seg_3670" s="T24">n</ta>
            <ta e="T26" id="Seg_3671" s="T25">v</ta>
            <ta e="T27" id="Seg_3672" s="T26">dempro</ta>
            <ta e="T28" id="Seg_3673" s="T27">dempro</ta>
            <ta e="T29" id="Seg_3674" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_3675" s="T29">n</ta>
            <ta e="T31" id="Seg_3676" s="T30">v</ta>
            <ta e="T32" id="Seg_3677" s="T31">v</ta>
            <ta e="T33" id="Seg_3678" s="T32">v</ta>
            <ta e="T34" id="Seg_3679" s="T33">adv</ta>
            <ta e="T35" id="Seg_3680" s="T34">adv</ta>
            <ta e="T36" id="Seg_3681" s="T35">v</ta>
            <ta e="T37" id="Seg_3682" s="T36">v</ta>
            <ta e="T38" id="Seg_3683" s="T37">conj</ta>
            <ta e="T39" id="Seg_3684" s="T38">n</ta>
            <ta e="T40" id="Seg_3685" s="T39">v</ta>
            <ta e="T41" id="Seg_3686" s="T40">conj</ta>
            <ta e="T42" id="Seg_3687" s="T41">n</ta>
            <ta e="T44" id="Seg_3688" s="T43">conj</ta>
            <ta e="T45" id="Seg_3689" s="T44">ptcl</ta>
            <ta e="T46" id="Seg_3690" s="T45">v</ta>
            <ta e="T47" id="Seg_3691" s="T46">v</ta>
            <ta e="T48" id="Seg_3692" s="T47">conj</ta>
            <ta e="T49" id="Seg_3693" s="T48">dempro</ta>
            <ta e="T50" id="Seg_3694" s="T49">n</ta>
            <ta e="T51" id="Seg_3695" s="T50">n</ta>
            <ta e="T52" id="Seg_3696" s="T51">v</ta>
            <ta e="T53" id="Seg_3697" s="T52">pers</ta>
            <ta e="T54" id="Seg_3698" s="T53">conj</ta>
            <ta e="T55" id="Seg_3699" s="T54">dempro</ta>
            <ta e="T56" id="Seg_3700" s="T55">pers</ta>
            <ta e="T57" id="Seg_3701" s="T56">v</ta>
            <ta e="T58" id="Seg_3702" s="T57">v</ta>
            <ta e="T59" id="Seg_3703" s="T58">pers</ta>
            <ta e="T60" id="Seg_3704" s="T59">n</ta>
            <ta e="T61" id="Seg_3705" s="T60">pers</ta>
            <ta e="T62" id="Seg_3706" s="T61">v</ta>
            <ta e="T64" id="Seg_3707" s="T62">dempro</ta>
            <ta e="T65" id="Seg_3708" s="T64">dempro</ta>
            <ta e="T66" id="Seg_3709" s="T65">v</ta>
            <ta e="T67" id="Seg_3710" s="T66">v</ta>
            <ta e="T68" id="Seg_3711" s="T67">que</ta>
            <ta e="T69" id="Seg_3712" s="T68">pers</ta>
            <ta e="T70" id="Seg_3713" s="T69">v</ta>
            <ta e="T72" id="Seg_3714" s="T71">dempro</ta>
            <ta e="T73" id="Seg_3715" s="T72">v</ta>
            <ta e="T74" id="Seg_3716" s="T73">n</ta>
            <ta e="T75" id="Seg_3717" s="T74">v</ta>
            <ta e="T76" id="Seg_3718" s="T75">v</ta>
            <ta e="T77" id="Seg_3719" s="T76">dempro</ta>
            <ta e="T78" id="Seg_3720" s="T77">dempro</ta>
            <ta e="T80" id="Seg_3721" s="T79">v</ta>
            <ta e="T81" id="Seg_3722" s="T80">n</ta>
            <ta e="T82" id="Seg_3723" s="T81">dempro</ta>
            <ta e="T83" id="Seg_3724" s="T82">v</ta>
            <ta e="T84" id="Seg_3725" s="T83">dempro</ta>
            <ta e="T85" id="Seg_3726" s="T84">dempro</ta>
            <ta e="T86" id="Seg_3727" s="T85">v</ta>
            <ta e="T87" id="Seg_3728" s="T86">adv</ta>
            <ta e="T88" id="Seg_3729" s="T87">que</ta>
            <ta e="T89" id="Seg_3730" s="T88">v</ta>
            <ta e="T90" id="Seg_3731" s="T89">adv</ta>
            <ta e="T91" id="Seg_3732" s="T90">dempro</ta>
            <ta e="T92" id="Seg_3733" s="T91">adv</ta>
            <ta e="T93" id="Seg_3734" s="T92">v</ta>
            <ta e="T94" id="Seg_3735" s="T93">dempro</ta>
            <ta e="T95" id="Seg_3736" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_3737" s="T95">v</ta>
            <ta e="T97" id="Seg_3738" s="T96">v</ta>
            <ta e="T98" id="Seg_3739" s="T97">v</ta>
            <ta e="T99" id="Seg_3740" s="T98">v</ta>
            <ta e="T100" id="Seg_3741" s="T99">v</ta>
            <ta e="T101" id="Seg_3742" s="T100">v</ta>
            <ta e="T102" id="Seg_3743" s="T101">v</ta>
            <ta e="T103" id="Seg_3744" s="T102">conj</ta>
            <ta e="T104" id="Seg_3745" s="T103">dempro</ta>
            <ta e="T105" id="Seg_3746" s="T104">adv</ta>
            <ta e="T106" id="Seg_3747" s="T105">ptcl</ta>
            <ta e="T107" id="Seg_3748" s="T106">v</ta>
            <ta e="T108" id="Seg_3749" s="T107">v</ta>
            <ta e="T109" id="Seg_3750" s="T108">adv</ta>
            <ta e="T110" id="Seg_3751" s="T109">adv</ta>
            <ta e="T111" id="Seg_3752" s="T110">n</ta>
            <ta e="T112" id="Seg_3753" s="T111">v</ta>
            <ta e="T113" id="Seg_3754" s="T112">dempro</ta>
            <ta e="T115" id="Seg_3755" s="T114">v</ta>
            <ta e="T116" id="Seg_3756" s="T115">adv</ta>
            <ta e="T117" id="Seg_3757" s="T116">dempro</ta>
            <ta e="T118" id="Seg_3758" s="T117">v</ta>
            <ta e="T329" id="Seg_3759" s="T118">v</ta>
            <ta e="T119" id="Seg_3760" s="T329">v</ta>
            <ta e="T120" id="Seg_3761" s="T119">adv</ta>
            <ta e="T121" id="Seg_3762" s="T120">dempro</ta>
            <ta e="T122" id="Seg_3763" s="T121">v</ta>
            <ta e="T123" id="Seg_3764" s="T122">adv</ta>
            <ta e="T124" id="Seg_3765" s="T123">adv</ta>
            <ta e="T125" id="Seg_3766" s="T124">v</ta>
            <ta e="T126" id="Seg_3767" s="T125">conj</ta>
            <ta e="T127" id="Seg_3768" s="T126">v</ta>
            <ta e="T128" id="Seg_3769" s="T127">v</ta>
            <ta e="T129" id="Seg_3770" s="T128">v</ta>
            <ta e="T130" id="Seg_3771" s="T129">n</ta>
            <ta e="T131" id="Seg_3772" s="T130">n</ta>
            <ta e="T132" id="Seg_3773" s="T131">adv</ta>
            <ta e="T133" id="Seg_3774" s="T132">ptcl</ta>
            <ta e="T134" id="Seg_3775" s="T133">v</ta>
            <ta e="T135" id="Seg_3776" s="T134">dempro</ta>
            <ta e="T136" id="Seg_3777" s="T135">n</ta>
            <ta e="T137" id="Seg_3778" s="T136">v</ta>
            <ta e="T138" id="Seg_3779" s="T137">v</ta>
            <ta e="T139" id="Seg_3780" s="T138">adv</ta>
            <ta e="T140" id="Seg_3781" s="T139">dempro</ta>
            <ta e="T141" id="Seg_3782" s="T140">n</ta>
            <ta e="T142" id="Seg_3783" s="T141">adv</ta>
            <ta e="T143" id="Seg_3784" s="T142">n</ta>
            <ta e="T144" id="Seg_3785" s="T143">n</ta>
            <ta e="T145" id="Seg_3786" s="T144">v</ta>
            <ta e="T146" id="Seg_3787" s="T145">v</ta>
            <ta e="T147" id="Seg_3788" s="T146">pers</ta>
            <ta e="T148" id="Seg_3789" s="T147">dempro</ta>
            <ta e="T149" id="Seg_3790" s="T148">ptcl</ta>
            <ta e="T150" id="Seg_3791" s="T149">pers</ta>
            <ta e="T151" id="Seg_3792" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_3793" s="T151">v</ta>
            <ta e="T153" id="Seg_3794" s="T152">n</ta>
            <ta e="T155" id="Seg_3795" s="T154">v</ta>
            <ta e="T156" id="Seg_3796" s="T155">pers</ta>
            <ta e="T157" id="Seg_3797" s="T156">n</ta>
            <ta e="T158" id="Seg_3798" s="T157">v</ta>
            <ta e="T159" id="Seg_3799" s="T158">adv</ta>
            <ta e="T160" id="Seg_3800" s="T159">v</ta>
            <ta e="T161" id="Seg_3801" s="T160">ptcl</ta>
            <ta e="T162" id="Seg_3802" s="T161">v</ta>
            <ta e="T163" id="Seg_3803" s="T162">v</ta>
            <ta e="T165" id="Seg_3804" s="T163">adv</ta>
            <ta e="T166" id="Seg_3805" s="T165">dempro</ta>
            <ta e="T167" id="Seg_3806" s="T166">v</ta>
            <ta e="T168" id="Seg_3807" s="T167">dempro</ta>
            <ta e="T169" id="Seg_3808" s="T168">adv</ta>
            <ta e="T170" id="Seg_3809" s="T169">v</ta>
            <ta e="T171" id="Seg_3810" s="T170">v</ta>
            <ta e="T172" id="Seg_3811" s="T171">dempro</ta>
            <ta e="T173" id="Seg_3812" s="T172">dempro</ta>
            <ta e="T174" id="Seg_3813" s="T173">adv</ta>
            <ta e="T175" id="Seg_3814" s="T174">n</ta>
            <ta e="T176" id="Seg_3815" s="T175">v</ta>
            <ta e="T177" id="Seg_3816" s="T176">dempro</ta>
            <ta e="T178" id="Seg_3817" s="T177">v</ta>
            <ta e="T179" id="Seg_3818" s="T178">dempro</ta>
            <ta e="T180" id="Seg_3819" s="T179">dempro</ta>
            <ta e="T181" id="Seg_3820" s="T180">n</ta>
            <ta e="T182" id="Seg_3821" s="T181">v</ta>
            <ta e="T183" id="Seg_3822" s="T182">que</ta>
            <ta e="T184" id="Seg_3823" s="T183">v</ta>
            <ta e="T185" id="Seg_3824" s="T184">v</ta>
            <ta e="T186" id="Seg_3825" s="T185">adv</ta>
            <ta e="T187" id="Seg_3826" s="T186">dempro</ta>
            <ta e="T188" id="Seg_3827" s="T187">adv</ta>
            <ta e="T189" id="Seg_3828" s="T188">v</ta>
            <ta e="T190" id="Seg_3829" s="T189">dempro</ta>
            <ta e="T192" id="Seg_3830" s="T191">dempro</ta>
            <ta e="T193" id="Seg_3831" s="T192">v</ta>
            <ta e="T194" id="Seg_3832" s="T193">v</ta>
            <ta e="T196" id="Seg_3833" s="T194">adv</ta>
            <ta e="T197" id="Seg_3834" s="T196">que</ta>
            <ta e="T198" id="Seg_3835" s="T197">ptcl</ta>
            <ta e="T199" id="Seg_3836" s="T198">v</ta>
            <ta e="T200" id="Seg_3837" s="T199">v</ta>
            <ta e="T201" id="Seg_3838" s="T200">adv</ta>
            <ta e="T202" id="Seg_3839" s="T201">n</ta>
            <ta e="T203" id="Seg_3840" s="T202">v</ta>
            <ta e="T204" id="Seg_3841" s="T203">dempro</ta>
            <ta e="T205" id="Seg_3842" s="T204">v</ta>
            <ta e="T206" id="Seg_3843" s="T205">v</ta>
            <ta e="T207" id="Seg_3844" s="T206">ptcl</ta>
            <ta e="T208" id="Seg_3845" s="T207">pers</ta>
            <ta e="T209" id="Seg_3846" s="T208">adv</ta>
            <ta e="T210" id="Seg_3847" s="T209">v</ta>
            <ta e="T211" id="Seg_3848" s="T210">adv</ta>
            <ta e="T212" id="Seg_3849" s="T211">dempro</ta>
            <ta e="T213" id="Seg_3850" s="T212">v</ta>
            <ta e="T214" id="Seg_3851" s="T213">dempro</ta>
            <ta e="T215" id="Seg_3852" s="T214">v</ta>
            <ta e="T216" id="Seg_3853" s="T215">adv</ta>
            <ta e="T217" id="Seg_3854" s="T216">adv</ta>
            <ta e="T219" id="Seg_3855" s="T218">n</ta>
            <ta e="T220" id="Seg_3856" s="T219">v</ta>
            <ta e="T221" id="Seg_3857" s="T220">n</ta>
            <ta e="T222" id="Seg_3858" s="T221">n</ta>
            <ta e="T223" id="Seg_3859" s="T222">adj</ta>
            <ta e="T226" id="Seg_3860" s="T225">n</ta>
            <ta e="T227" id="Seg_3861" s="T226">n</ta>
            <ta e="T228" id="Seg_3862" s="T227">v</ta>
            <ta e="T229" id="Seg_3863" s="T228">conj</ta>
            <ta e="T230" id="Seg_3864" s="T229">dempro</ta>
            <ta e="T231" id="Seg_3865" s="T230">n</ta>
            <ta e="T232" id="Seg_3866" s="T231">conj</ta>
            <ta e="T233" id="Seg_3867" s="T232">dempro</ta>
            <ta e="T235" id="Seg_3868" s="T233">n</ta>
            <ta e="T236" id="Seg_3869" s="T235">dempro</ta>
            <ta e="T237" id="Seg_3870" s="T236">v</ta>
            <ta e="T238" id="Seg_3871" s="T237">conj</ta>
            <ta e="T239" id="Seg_3872" s="T238">v</ta>
            <ta e="T240" id="Seg_3873" s="T239">v</ta>
            <ta e="T241" id="Seg_3874" s="T240">pers</ta>
            <ta e="T242" id="Seg_3875" s="T241">ptcl</ta>
            <ta e="T243" id="Seg_3876" s="T242">pers</ta>
            <ta e="T244" id="Seg_3877" s="T243">ptcl</ta>
            <ta e="T245" id="Seg_3878" s="T244">v</ta>
            <ta e="T246" id="Seg_3879" s="T245">n</ta>
            <ta e="T247" id="Seg_3880" s="T246">pers</ta>
            <ta e="T248" id="Seg_3881" s="T247">pers</ta>
            <ta e="T249" id="Seg_3882" s="T248">adv</ta>
            <ta e="T250" id="Seg_3883" s="T249">v</ta>
            <ta e="T251" id="Seg_3884" s="T250">v</ta>
            <ta e="T252" id="Seg_3885" s="T251">ptcl</ta>
            <ta e="T253" id="Seg_3886" s="T252">aux</ta>
            <ta e="T254" id="Seg_3887" s="T253">adv</ta>
            <ta e="T255" id="Seg_3888" s="T254">dempro</ta>
            <ta e="T256" id="Seg_3889" s="T255">v</ta>
            <ta e="T257" id="Seg_3890" s="T256">n</ta>
            <ta e="T258" id="Seg_3891" s="T257">conj</ta>
            <ta e="T259" id="Seg_3892" s="T258">n</ta>
            <ta e="T260" id="Seg_3893" s="T259">adv</ta>
            <ta e="T261" id="Seg_3894" s="T260">n</ta>
            <ta e="T262" id="Seg_3895" s="T261">v</ta>
            <ta e="T263" id="Seg_3896" s="T262">v</ta>
            <ta e="T264" id="Seg_3897" s="T263">v</ta>
            <ta e="T265" id="Seg_3898" s="T264">dempro</ta>
            <ta e="T266" id="Seg_3899" s="T265">dempro</ta>
            <ta e="T267" id="Seg_3900" s="T266">n</ta>
            <ta e="T268" id="Seg_3901" s="T267">adv</ta>
            <ta e="T269" id="Seg_3902" s="T268">v</ta>
            <ta e="T270" id="Seg_3903" s="T269">dempro</ta>
            <ta e="T271" id="Seg_3904" s="T270">v</ta>
            <ta e="T272" id="Seg_3905" s="T271">dempro</ta>
            <ta e="T273" id="Seg_3906" s="T272">adv</ta>
            <ta e="T274" id="Seg_3907" s="T273">n</ta>
            <ta e="T275" id="Seg_3908" s="T274">v</ta>
            <ta e="T276" id="Seg_3909" s="T275">dempro</ta>
            <ta e="T277" id="Seg_3910" s="T276">conj</ta>
            <ta e="T278" id="Seg_3911" s="T277">dempro</ta>
            <ta e="T279" id="Seg_3912" s="T278">v</ta>
            <ta e="T280" id="Seg_3913" s="T279">dempro</ta>
            <ta e="T281" id="Seg_3914" s="T280">adv</ta>
            <ta e="T282" id="Seg_3915" s="T281">v</ta>
            <ta e="T283" id="Seg_3916" s="T282">adv</ta>
            <ta e="T284" id="Seg_3917" s="T283">v</ta>
            <ta e="T285" id="Seg_3918" s="T284">dempro</ta>
            <ta e="T286" id="Seg_3919" s="T285">ptcl</ta>
            <ta e="T287" id="Seg_3920" s="T286">v</ta>
            <ta e="T288" id="Seg_3921" s="T287">v</ta>
            <ta e="T289" id="Seg_3922" s="T288">dempro</ta>
            <ta e="T290" id="Seg_3923" s="T289">v</ta>
            <ta e="T291" id="Seg_3924" s="T290">v</ta>
            <ta e="T292" id="Seg_3925" s="T291">n</ta>
            <ta e="T293" id="Seg_3926" s="T292">v</ta>
            <ta e="T294" id="Seg_3927" s="T293">n</ta>
            <ta e="T295" id="Seg_3928" s="T294">dempro</ta>
            <ta e="T296" id="Seg_3929" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_3930" s="T296">v</ta>
            <ta e="T298" id="Seg_3931" s="T297">adv</ta>
            <ta e="T300" id="Seg_3932" s="T299">v</ta>
            <ta e="T301" id="Seg_3933" s="T300">adv</ta>
            <ta e="T302" id="Seg_3934" s="T301">n</ta>
            <ta e="T303" id="Seg_3935" s="T302">v</ta>
            <ta e="T304" id="Seg_3936" s="T303">dempro</ta>
            <ta e="T305" id="Seg_3937" s="T304">v</ta>
            <ta e="T306" id="Seg_3938" s="T305">conj</ta>
            <ta e="T307" id="Seg_3939" s="T306">pers</ta>
            <ta e="T308" id="Seg_3940" s="T307">pers</ta>
            <ta e="T309" id="Seg_3941" s="T308">v</ta>
            <ta e="T310" id="Seg_3942" s="T309">ptcl</ta>
            <ta e="T311" id="Seg_3943" s="T310">pers</ta>
            <ta e="T312" id="Seg_3944" s="T311">pers</ta>
            <ta e="T313" id="Seg_3945" s="T312">ptcl</ta>
            <ta e="T314" id="Seg_3946" s="T313">pers</ta>
            <ta e="T315" id="Seg_3947" s="T314">n</ta>
            <ta e="T316" id="Seg_3948" s="T315">conj</ta>
            <ta e="T317" id="Seg_3949" s="T316">dempro</ta>
            <ta e="T318" id="Seg_3950" s="T317">pers</ta>
            <ta e="T319" id="Seg_3951" s="T318">n</ta>
            <ta e="T320" id="Seg_3952" s="T319">adv</ta>
            <ta e="T321" id="Seg_3953" s="T320">n</ta>
            <ta e="T322" id="Seg_3954" s="T321">v</ta>
            <ta e="T323" id="Seg_3955" s="T322">v</ta>
            <ta e="T324" id="Seg_3956" s="T323">n</ta>
            <ta e="T325" id="Seg_3957" s="T324">v</ta>
            <ta e="T326" id="Seg_3958" s="T325">conj</ta>
            <ta e="T327" id="Seg_3959" s="T326">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_3960" s="T0">adv:Time</ta>
            <ta e="T2" id="Seg_3961" s="T1">pro.h:A</ta>
            <ta e="T4" id="Seg_3962" s="T3">np:Th</ta>
            <ta e="T5" id="Seg_3963" s="T4">0.3.h:A</ta>
            <ta e="T7" id="Seg_3964" s="T6">pro:Th</ta>
            <ta e="T10" id="Seg_3965" s="T9">pro.h:A</ta>
            <ta e="T11" id="Seg_3966" s="T10">pro:G</ta>
            <ta e="T17" id="Seg_3967" s="T16">np:Th</ta>
            <ta e="T19" id="Seg_3968" s="T18">0.3:Th</ta>
            <ta e="T21" id="Seg_3969" s="T20">0.3:A</ta>
            <ta e="T22" id="Seg_3970" s="T21">adv:L</ta>
            <ta e="T24" id="Seg_3971" s="T23">pro.h:Poss</ta>
            <ta e="T25" id="Seg_3972" s="T24">np.h:E</ta>
            <ta e="T28" id="Seg_3973" s="T27">pro.h:Poss</ta>
            <ta e="T30" id="Seg_3974" s="T29">np.h:Th</ta>
            <ta e="T32" id="Seg_3975" s="T31">0.3.h:E</ta>
            <ta e="T34" id="Seg_3976" s="T33">adv:Time</ta>
            <ta e="T35" id="Seg_3977" s="T34">adv:L</ta>
            <ta e="T37" id="Seg_3978" s="T36">0.3.h:A</ta>
            <ta e="T39" id="Seg_3979" s="T38">np:Th</ta>
            <ta e="T40" id="Seg_3980" s="T39">0.3.h:A</ta>
            <ta e="T42" id="Seg_3981" s="T41">np:Th</ta>
            <ta e="T47" id="Seg_3982" s="T46">0.3.h:E</ta>
            <ta e="T49" id="Seg_3983" s="T48">pro.h:A</ta>
            <ta e="T50" id="Seg_3984" s="T49">np.h:Poss</ta>
            <ta e="T52" id="Seg_3985" s="T51">0.2.h:A</ta>
            <ta e="T53" id="Seg_3986" s="T52">pro.h:R</ta>
            <ta e="T55" id="Seg_3987" s="T54">pro.h:A</ta>
            <ta e="T58" id="Seg_3988" s="T57">0.2.h:A</ta>
            <ta e="T59" id="Seg_3989" s="T58">pro.h:Poss</ta>
            <ta e="T60" id="Seg_3990" s="T59">np.h:Th</ta>
            <ta e="T61" id="Seg_3991" s="T60">pro.h:R</ta>
            <ta e="T64" id="Seg_3992" s="T62">pro.h:A</ta>
            <ta e="T65" id="Seg_3993" s="T64">pro.h:R</ta>
            <ta e="T67" id="Seg_3994" s="T66">0.2.h:E</ta>
            <ta e="T69" id="Seg_3995" s="T68">pro.h:B</ta>
            <ta e="T70" id="Seg_3996" s="T69">0.1.h:E</ta>
            <ta e="T72" id="Seg_3997" s="T71">pro.h:A</ta>
            <ta e="T74" id="Seg_3998" s="T73">np:G</ta>
            <ta e="T75" id="Seg_3999" s="T74">0.3.h:A</ta>
            <ta e="T77" id="Seg_4000" s="T76">pro.h:A</ta>
            <ta e="T78" id="Seg_4001" s="T77">pro.h:R</ta>
            <ta e="T81" id="Seg_4002" s="T80">np:P</ta>
            <ta e="T82" id="Seg_4003" s="T81">pro.h:E</ta>
            <ta e="T84" id="Seg_4004" s="T83">pro.h:A</ta>
            <ta e="T85" id="Seg_4005" s="T84">pro.h:Th</ta>
            <ta e="T87" id="Seg_4006" s="T86">adv:L</ta>
            <ta e="T89" id="Seg_4007" s="T88">0.3.h:E</ta>
            <ta e="T90" id="Seg_4008" s="T89">adv:Time</ta>
            <ta e="T91" id="Seg_4009" s="T90">pro.h:Th</ta>
            <ta e="T92" id="Seg_4010" s="T91">adv:L</ta>
            <ta e="T93" id="Seg_4011" s="T92">0.3.h:A</ta>
            <ta e="T94" id="Seg_4012" s="T93">pro.h:E</ta>
            <ta e="T97" id="Seg_4013" s="T96">0.3.h:E</ta>
            <ta e="T98" id="Seg_4014" s="T97">0.3.h:A</ta>
            <ta e="T99" id="Seg_4015" s="T98">0.3.h:A</ta>
            <ta e="T100" id="Seg_4016" s="T99">0.3.h:A</ta>
            <ta e="T101" id="Seg_4017" s="T100">0.3.h:A</ta>
            <ta e="T102" id="Seg_4018" s="T101">0.2.h:A</ta>
            <ta e="T104" id="Seg_4019" s="T103">pro.h:A</ta>
            <ta e="T108" id="Seg_4020" s="T107">0.3.h:E</ta>
            <ta e="T110" id="Seg_4021" s="T109">adv:Time</ta>
            <ta e="T111" id="Seg_4022" s="T110">np:Th</ta>
            <ta e="T115" id="Seg_4023" s="T114">0.2.h:A</ta>
            <ta e="T116" id="Seg_4024" s="T115">adv:L</ta>
            <ta e="T117" id="Seg_4025" s="T116">pro.h:A</ta>
            <ta e="T119" id="Seg_4026" s="T329">0.3.h:A</ta>
            <ta e="T120" id="Seg_4027" s="T119">adv:Time</ta>
            <ta e="T121" id="Seg_4028" s="T120">pro.h:A</ta>
            <ta e="T123" id="Seg_4029" s="T122">adv:Time</ta>
            <ta e="T125" id="Seg_4030" s="T124">0.3.h:E</ta>
            <ta e="T127" id="Seg_4031" s="T126">0.3.h:A</ta>
            <ta e="T129" id="Seg_4032" s="T128">0.3.h:E</ta>
            <ta e="T131" id="Seg_4033" s="T130">np:L</ta>
            <ta e="T132" id="Seg_4034" s="T131">adv:Time</ta>
            <ta e="T135" id="Seg_4035" s="T134">pro.h:A</ta>
            <ta e="T136" id="Seg_4036" s="T135">np:Th</ta>
            <ta e="T137" id="Seg_4037" s="T136">0.3.h:A</ta>
            <ta e="T138" id="Seg_4038" s="T137">0.3.h:A</ta>
            <ta e="T141" id="Seg_4039" s="T140">np.h:A</ta>
            <ta e="T143" id="Seg_4040" s="T142">np.h:Poss</ta>
            <ta e="T146" id="Seg_4041" s="T145">0.2.h:A</ta>
            <ta e="T147" id="Seg_4042" s="T146">pro.h:R</ta>
            <ta e="T148" id="Seg_4043" s="T147">pro:Th</ta>
            <ta e="T150" id="Seg_4044" s="T149">pro.h:A</ta>
            <ta e="T153" id="Seg_4045" s="T152">np:Th</ta>
            <ta e="T155" id="Seg_4046" s="T154">0.2.h:A</ta>
            <ta e="T156" id="Seg_4047" s="T155">pro.h:R</ta>
            <ta e="T157" id="Seg_4048" s="T156">n:Time</ta>
            <ta e="T159" id="Seg_4049" s="T158">adv:Time</ta>
            <ta e="T160" id="Seg_4050" s="T159">0.1.h:A</ta>
            <ta e="T162" id="Seg_4051" s="T161">0.2.h:A</ta>
            <ta e="T163" id="Seg_4052" s="T162">0.1.h:A</ta>
            <ta e="T165" id="Seg_4053" s="T163">adv:Time</ta>
            <ta e="T166" id="Seg_4054" s="T165">pro.h:A</ta>
            <ta e="T168" id="Seg_4055" s="T167">pro.h:A</ta>
            <ta e="T172" id="Seg_4056" s="T171">pro.h:A</ta>
            <ta e="T173" id="Seg_4057" s="T172">pro.h:R</ta>
            <ta e="T175" id="Seg_4058" s="T174">np:Th</ta>
            <ta e="T177" id="Seg_4059" s="T176">pro.h:E</ta>
            <ta e="T179" id="Seg_4060" s="T178">pro.h:A</ta>
            <ta e="T180" id="Seg_4061" s="T179">pro.h:Th</ta>
            <ta e="T181" id="Seg_4062" s="T180">np:G</ta>
            <ta e="T184" id="Seg_4063" s="T183">0.3.h:E</ta>
            <ta e="T185" id="Seg_4064" s="T184">0.2.h:A</ta>
            <ta e="T186" id="Seg_4065" s="T185">0.2.h:E</ta>
            <ta e="T187" id="Seg_4066" s="T186">pro.h:A</ta>
            <ta e="T188" id="Seg_4067" s="T187">adv:L</ta>
            <ta e="T192" id="Seg_4068" s="T191">pro.h:R</ta>
            <ta e="T193" id="Seg_4069" s="T192">0.2.h:A</ta>
            <ta e="T194" id="Seg_4070" s="T193">0.2.h:A</ta>
            <ta e="T196" id="Seg_4071" s="T194">adv:Time</ta>
            <ta e="T199" id="Seg_4072" s="T198">0.3.h:A</ta>
            <ta e="T201" id="Seg_4073" s="T200">adv:Time</ta>
            <ta e="T202" id="Seg_4074" s="T201">np:Th</ta>
            <ta e="T203" id="Seg_4075" s="T202">pro.h:A</ta>
            <ta e="T206" id="Seg_4076" s="T205">0.2.h:A</ta>
            <ta e="T208" id="Seg_4077" s="T207">pro.h:A</ta>
            <ta e="T211" id="Seg_4078" s="T210">adv:Time</ta>
            <ta e="T212" id="Seg_4079" s="T211">pro.h:A</ta>
            <ta e="T214" id="Seg_4080" s="T213">pro.h:A</ta>
            <ta e="T216" id="Seg_4081" s="T215">adv:Time</ta>
            <ta e="T220" id="Seg_4082" s="T219">0.3.h:E</ta>
            <ta e="T221" id="Seg_4083" s="T220">np:Th</ta>
            <ta e="T222" id="Seg_4084" s="T221">0.3.h:A</ta>
            <ta e="T227" id="Seg_4085" s="T226">np:G</ta>
            <ta e="T228" id="Seg_4086" s="T227">0.3.h:A</ta>
            <ta e="T229" id="Seg_4087" s="T228">adv:Time</ta>
            <ta e="T231" id="Seg_4088" s="T230">np:G</ta>
            <ta e="T232" id="Seg_4089" s="T231">adv:Time</ta>
            <ta e="T235" id="Seg_4090" s="T233">np:G</ta>
            <ta e="T236" id="Seg_4091" s="T235">pro.h:E</ta>
            <ta e="T239" id="Seg_4092" s="T238">0.3.h:A</ta>
            <ta e="T240" id="Seg_4093" s="T239">0.2.h:A</ta>
            <ta e="T241" id="Seg_4094" s="T240">pro.h:R</ta>
            <ta e="T243" id="Seg_4095" s="T242">pro.h:A</ta>
            <ta e="T246" id="Seg_4096" s="T245">np.h:Th</ta>
            <ta e="T247" id="Seg_4097" s="T246">0.2.h:A</ta>
            <ta e="T248" id="Seg_4098" s="T247">pro.h:R</ta>
            <ta e="T249" id="Seg_4099" s="T248">adv:Time</ta>
            <ta e="T253" id="Seg_4100" s="T252">0.2.h:A</ta>
            <ta e="T254" id="Seg_4101" s="T253">adv:Time</ta>
            <ta e="T255" id="Seg_4102" s="T254">pro.h:R</ta>
            <ta e="T256" id="Seg_4103" s="T255">0.3.h:A</ta>
            <ta e="T257" id="Seg_4104" s="T256">np:Th</ta>
            <ta e="T259" id="Seg_4105" s="T258">np:Th</ta>
            <ta e="T260" id="Seg_4106" s="T259">adv:Time</ta>
            <ta e="T261" id="Seg_4107" s="T260">np.h:A</ta>
            <ta e="T264" id="Seg_4108" s="T263">0.3.h:Th</ta>
            <ta e="T265" id="Seg_4109" s="T264">pro.h:A</ta>
            <ta e="T266" id="Seg_4110" s="T265">pro.h:R</ta>
            <ta e="T267" id="Seg_4111" s="T266">np:Th</ta>
            <ta e="T270" id="Seg_4112" s="T269">pro.h:E</ta>
            <ta e="T272" id="Seg_4113" s="T271">pro.h:A</ta>
            <ta e="T274" id="Seg_4114" s="T273">np:G</ta>
            <ta e="T276" id="Seg_4115" s="T275">pro.h:Th</ta>
            <ta e="T278" id="Seg_4116" s="T277">pro.h:Th</ta>
            <ta e="T279" id="Seg_4117" s="T278">0.3.h:A</ta>
            <ta e="T280" id="Seg_4118" s="T279">pro.h:A</ta>
            <ta e="T281" id="Seg_4119" s="T280">adv:L</ta>
            <ta e="T284" id="Seg_4120" s="T283">0.3.h:E</ta>
            <ta e="T285" id="Seg_4121" s="T284">pro.h:Th</ta>
            <ta e="T287" id="Seg_4122" s="T286">0.3.h:A</ta>
            <ta e="T288" id="Seg_4123" s="T287">0.3.h:A</ta>
            <ta e="T289" id="Seg_4124" s="T288">pro.h:Th</ta>
            <ta e="T290" id="Seg_4125" s="T289">0.3.h:E</ta>
            <ta e="T291" id="Seg_4126" s="T290">0.3.h:E</ta>
            <ta e="T293" id="Seg_4127" s="T292">0.3:Th</ta>
            <ta e="T294" id="Seg_4128" s="T293">np:G</ta>
            <ta e="T295" id="Seg_4129" s="T294">pro.h:A</ta>
            <ta e="T298" id="Seg_4130" s="T297">adv:Time</ta>
            <ta e="T300" id="Seg_4131" s="T299">0.3.h:A</ta>
            <ta e="T301" id="Seg_4132" s="T300">adv:Time</ta>
            <ta e="T302" id="Seg_4133" s="T301">np:Th</ta>
            <ta e="T304" id="Seg_4134" s="T303">pro.h:A</ta>
            <ta e="T307" id="Seg_4135" s="T306">pro.h:A</ta>
            <ta e="T308" id="Seg_4136" s="T307">pro.h:R</ta>
            <ta e="T311" id="Seg_4137" s="T310">pro.h:Th</ta>
            <ta e="T314" id="Seg_4138" s="T313">pro.h:Poss</ta>
            <ta e="T315" id="Seg_4139" s="T314">np.h:Th</ta>
            <ta e="T317" id="Seg_4140" s="T316">pro.h:Th</ta>
            <ta e="T318" id="Seg_4141" s="T317">pro.h:Poss</ta>
            <ta e="T319" id="Seg_4142" s="T318">np.h:Th</ta>
            <ta e="T320" id="Seg_4143" s="T319">adv:Time</ta>
            <ta e="T321" id="Seg_4144" s="T320">np:L</ta>
            <ta e="T322" id="Seg_4145" s="T321">0.3.h:E</ta>
            <ta e="T325" id="Seg_4146" s="T324">0.3.h:P</ta>
            <ta e="T327" id="Seg_4147" s="T326">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_4148" s="T1">pro.h:S</ta>
            <ta e="T3" id="Seg_4149" s="T2">v:pred</ta>
            <ta e="T4" id="Seg_4150" s="T3">np:O</ta>
            <ta e="T5" id="Seg_4151" s="T4">v:pred 0.3.h:S</ta>
            <ta e="T7" id="Seg_4152" s="T6">pro:S</ta>
            <ta e="T8" id="Seg_4153" s="T7">v:pred</ta>
            <ta e="T10" id="Seg_4154" s="T9">pro.h:S</ta>
            <ta e="T16" id="Seg_4155" s="T15">v:pred</ta>
            <ta e="T17" id="Seg_4156" s="T16">np:S</ta>
            <ta e="T18" id="Seg_4157" s="T17">v:pred</ta>
            <ta e="T19" id="Seg_4158" s="T18">v:pred 0.3:S</ta>
            <ta e="T21" id="Seg_4159" s="T20">v:pred 0.3:S</ta>
            <ta e="T25" id="Seg_4160" s="T24">np.h:S</ta>
            <ta e="T26" id="Seg_4161" s="T25">v:pred</ta>
            <ta e="T30" id="Seg_4162" s="T29">np.h:S</ta>
            <ta e="T31" id="Seg_4163" s="T30">v:pred</ta>
            <ta e="T32" id="Seg_4164" s="T31">v:pred 0.3.h:S</ta>
            <ta e="T37" id="Seg_4165" s="T36">v:pred 0.3.h:S</ta>
            <ta e="T39" id="Seg_4166" s="T38">np:O</ta>
            <ta e="T40" id="Seg_4167" s="T39">v:pred 0.3.h:S</ta>
            <ta e="T42" id="Seg_4168" s="T41">np:O</ta>
            <ta e="T46" id="Seg_4169" s="T45">conv:pred</ta>
            <ta e="T47" id="Seg_4170" s="T46"> 0.3.h:S v:pred</ta>
            <ta e="T49" id="Seg_4171" s="T48">pro.h:S</ta>
            <ta e="T52" id="Seg_4172" s="T51">v:pred 0.2.h:S</ta>
            <ta e="T55" id="Seg_4173" s="T54">pro.h:S</ta>
            <ta e="T57" id="Seg_4174" s="T56">v:pred</ta>
            <ta e="T58" id="Seg_4175" s="T57">v:pred 0.2.h:S</ta>
            <ta e="T60" id="Seg_4176" s="T59">np.h:O</ta>
            <ta e="T62" id="Seg_4177" s="T61">s:purp</ta>
            <ta e="T64" id="Seg_4178" s="T62">pro.h:S</ta>
            <ta e="T66" id="Seg_4179" s="T65">v:pred</ta>
            <ta e="T67" id="Seg_4180" s="T66">v:pred 0.2.h:S</ta>
            <ta e="T70" id="Seg_4181" s="T69">v:pred 0.1.h:S</ta>
            <ta e="T71" id="Seg_4182" s="T70">ptcl:pred</ta>
            <ta e="T72" id="Seg_4183" s="T71">pro.h:S</ta>
            <ta e="T73" id="Seg_4184" s="T72">v:pred</ta>
            <ta e="T75" id="Seg_4185" s="T74">v:pred 0.3.h:S</ta>
            <ta e="T76" id="Seg_4186" s="T75">s:purp</ta>
            <ta e="T77" id="Seg_4187" s="T76">pro.h:S</ta>
            <ta e="T80" id="Seg_4188" s="T79">v:pred</ta>
            <ta e="T81" id="Seg_4189" s="T80">np:O</ta>
            <ta e="T82" id="Seg_4190" s="T81">pro.h:S</ta>
            <ta e="T83" id="Seg_4191" s="T82">v:pred</ta>
            <ta e="T84" id="Seg_4192" s="T83">pro.h:S</ta>
            <ta e="T85" id="Seg_4193" s="T84">pro.h:O</ta>
            <ta e="T86" id="Seg_4194" s="T85">v:pred</ta>
            <ta e="T89" id="Seg_4195" s="T88">v:pred 0.3.h:S</ta>
            <ta e="T91" id="Seg_4196" s="T90">pro.h:O</ta>
            <ta e="T93" id="Seg_4197" s="T92">v:pred 0.3.h:S</ta>
            <ta e="T94" id="Seg_4198" s="T93">pro.h:S</ta>
            <ta e="T96" id="Seg_4199" s="T95">v:pred</ta>
            <ta e="T97" id="Seg_4200" s="T96">v:pred 0.3.h:S</ta>
            <ta e="T98" id="Seg_4201" s="T97">v:pred 0.3.h:S</ta>
            <ta e="T99" id="Seg_4202" s="T98">v:pred 0.3.h:S</ta>
            <ta e="T100" id="Seg_4203" s="T99">v:pred 0.3.h:S</ta>
            <ta e="T101" id="Seg_4204" s="T100">v:pred 0.3.h:S</ta>
            <ta e="T102" id="Seg_4205" s="T101">v:pred 0.2.h:S</ta>
            <ta e="T104" id="Seg_4206" s="T103">pro.h:S</ta>
            <ta e="T106" id="Seg_4207" s="T105">ptcl.neg</ta>
            <ta e="T107" id="Seg_4208" s="T106">v:pred</ta>
            <ta e="T108" id="Seg_4209" s="T107">v:pred 0.3.h:S</ta>
            <ta e="T111" id="Seg_4210" s="T110">np:S</ta>
            <ta e="T112" id="Seg_4211" s="T111">v:pred</ta>
            <ta e="T115" id="Seg_4212" s="T114">v:pred 0.2.h:S</ta>
            <ta e="T117" id="Seg_4213" s="T116">pro.h:S</ta>
            <ta e="T118" id="Seg_4214" s="T117">v:pred</ta>
            <ta e="T329" id="Seg_4215" s="T118">conv:pred</ta>
            <ta e="T119" id="Seg_4216" s="T329">v:pred 0.3.h:S</ta>
            <ta e="T121" id="Seg_4217" s="T120">pro.h:S</ta>
            <ta e="T122" id="Seg_4218" s="T121">v:pred</ta>
            <ta e="T125" id="Seg_4219" s="T124">v:pred 0.3.h:S</ta>
            <ta e="T127" id="Seg_4220" s="T126">v:pred 0.3.h:S</ta>
            <ta e="T129" id="Seg_4221" s="T128">v:pred 0.3.h:S</ta>
            <ta e="T133" id="Seg_4222" s="T132">ptcl:pred</ta>
            <ta e="T137" id="Seg_4223" s="T136">v:pred 0.3.h:S</ta>
            <ta e="T138" id="Seg_4224" s="T137">v:pred 0.3.h:S</ta>
            <ta e="T141" id="Seg_4225" s="T140">np.h:S</ta>
            <ta e="T145" id="Seg_4226" s="T144">v:pred</ta>
            <ta e="T146" id="Seg_4227" s="T145">v:pred 0.2.h:S</ta>
            <ta e="T148" id="Seg_4228" s="T147">pro:O</ta>
            <ta e="T149" id="Seg_4229" s="T148">ptcl.neg</ta>
            <ta e="T150" id="Seg_4230" s="T149">pro.h:S</ta>
            <ta e="T151" id="Seg_4231" s="T150">ptcl.neg</ta>
            <ta e="T152" id="Seg_4232" s="T151">v:pred</ta>
            <ta e="T153" id="Seg_4233" s="T152">np:O</ta>
            <ta e="T155" id="Seg_4234" s="T154">v:pred 0.2.h:S</ta>
            <ta e="T158" id="Seg_4235" s="T157">s:purp</ta>
            <ta e="T160" id="Seg_4236" s="T159">v:pred 0.1.h:S</ta>
            <ta e="T162" id="Seg_4237" s="T161">v:pred 0.2.h:S</ta>
            <ta e="T163" id="Seg_4238" s="T162">v:pred 0.1.h:S</ta>
            <ta e="T166" id="Seg_4239" s="T165">pro.h:S</ta>
            <ta e="T167" id="Seg_4240" s="T166">v:pred</ta>
            <ta e="T168" id="Seg_4241" s="T167">pro.h:S</ta>
            <ta e="T170" id="Seg_4242" s="T169">s:purp</ta>
            <ta e="T171" id="Seg_4243" s="T170">v:pred</ta>
            <ta e="T172" id="Seg_4244" s="T171">pro.h:S</ta>
            <ta e="T175" id="Seg_4245" s="T174">np:O</ta>
            <ta e="T176" id="Seg_4246" s="T175">v:pred</ta>
            <ta e="T177" id="Seg_4247" s="T176">pro.h:S</ta>
            <ta e="T178" id="Seg_4248" s="T177">v:pred</ta>
            <ta e="T179" id="Seg_4249" s="T178">pro.h:S</ta>
            <ta e="T180" id="Seg_4250" s="T179">pro.h:O</ta>
            <ta e="T182" id="Seg_4251" s="T181">v:pred</ta>
            <ta e="T184" id="Seg_4252" s="T183">v:pred 0.3.h:S</ta>
            <ta e="T185" id="Seg_4253" s="T184">v:pred 0.2.h:S</ta>
            <ta e="T186" id="Seg_4254" s="T185">conv:pred</ta>
            <ta e="T187" id="Seg_4255" s="T186">pro.h:S</ta>
            <ta e="T189" id="Seg_4256" s="T188">v:pred</ta>
            <ta e="T193" id="Seg_4257" s="T192">v:pred 0.2.h:S</ta>
            <ta e="T194" id="Seg_4258" s="T193">v:pred 0.2.h:S</ta>
            <ta e="T198" id="Seg_4259" s="T197">ptcl.neg</ta>
            <ta e="T199" id="Seg_4260" s="T198">v:pred 0.3.h:S</ta>
            <ta e="T202" id="Seg_4261" s="T201">np:S</ta>
            <ta e="T203" id="Seg_4262" s="T202">v:pred</ta>
            <ta e="T204" id="Seg_4263" s="T203">pro.h:S</ta>
            <ta e="T205" id="Seg_4264" s="T204">v:pred</ta>
            <ta e="T206" id="Seg_4265" s="T205">v:pred 0.2.h:S</ta>
            <ta e="T208" id="Seg_4266" s="T207">pro.h:S</ta>
            <ta e="T210" id="Seg_4267" s="T209">v:pred</ta>
            <ta e="T212" id="Seg_4268" s="T211">pro.h:S</ta>
            <ta e="T213" id="Seg_4269" s="T212">v:pred</ta>
            <ta e="T214" id="Seg_4270" s="T213">pro.h:S</ta>
            <ta e="T215" id="Seg_4271" s="T214">v:pred</ta>
            <ta e="T220" id="Seg_4272" s="T219">v:pred 0.3.h:S</ta>
            <ta e="T221" id="Seg_4273" s="T220">np:O</ta>
            <ta e="T222" id="Seg_4274" s="T221">v:pred 0.3.h:S</ta>
            <ta e="T228" id="Seg_4275" s="T227">v:pred 0.3.h:S</ta>
            <ta e="T236" id="Seg_4276" s="T235">pro.h:S</ta>
            <ta e="T237" id="Seg_4277" s="T236">v:pred</ta>
            <ta e="T239" id="Seg_4278" s="T238">v:pred 0.3.h:S</ta>
            <ta e="T240" id="Seg_4279" s="T239">v:pred 0.2.h:S</ta>
            <ta e="T242" id="Seg_4280" s="T241">ptcl:pred</ta>
            <ta e="T243" id="Seg_4281" s="T242">pro.h:S</ta>
            <ta e="T244" id="Seg_4282" s="T243">ptcl.neg</ta>
            <ta e="T245" id="Seg_4283" s="T244">v:pred</ta>
            <ta e="T246" id="Seg_4284" s="T245">np.h:O</ta>
            <ta e="T247" id="Seg_4285" s="T246">v:pred 0.2.h:S</ta>
            <ta e="T250" id="Seg_4286" s="T249">s:purp</ta>
            <ta e="T251" id="Seg_4287" s="T250">s:purp</ta>
            <ta e="T253" id="Seg_4288" s="T252">v:pred 0.2.h:S</ta>
            <ta e="T256" id="Seg_4289" s="T255">v:pred 0.3.h:S</ta>
            <ta e="T257" id="Seg_4290" s="T256">np:O</ta>
            <ta e="T259" id="Seg_4291" s="T258">np:O</ta>
            <ta e="T261" id="Seg_4292" s="T260">np.h:S</ta>
            <ta e="T262" id="Seg_4293" s="T261">v:pred</ta>
            <ta e="T263" id="Seg_4294" s="T262">s:purp</ta>
            <ta e="T264" id="Seg_4295" s="T263">v:pred 0.3.h:S</ta>
            <ta e="T265" id="Seg_4296" s="T264">pro.h:S</ta>
            <ta e="T267" id="Seg_4297" s="T266">np:O</ta>
            <ta e="T269" id="Seg_4298" s="T268">v:pred</ta>
            <ta e="T270" id="Seg_4299" s="T269">pro.h:S</ta>
            <ta e="T271" id="Seg_4300" s="T270">v:pred</ta>
            <ta e="T272" id="Seg_4301" s="T271">pro.h:S</ta>
            <ta e="T275" id="Seg_4302" s="T274">v:pred</ta>
            <ta e="T276" id="Seg_4303" s="T275">pro.h:O</ta>
            <ta e="T278" id="Seg_4304" s="T277">pro.h:O</ta>
            <ta e="T279" id="Seg_4305" s="T278">v:pred 0.3.h:S</ta>
            <ta e="T280" id="Seg_4306" s="T279">pro.h:S</ta>
            <ta e="T282" id="Seg_4307" s="T281">v:pred</ta>
            <ta e="T284" id="Seg_4308" s="T283">v:pred 0.3.h:S</ta>
            <ta e="T285" id="Seg_4309" s="T284">pro.h:O</ta>
            <ta e="T287" id="Seg_4310" s="T286">v:pred 0.3.h:S</ta>
            <ta e="T288" id="Seg_4311" s="T287">v:pred 0.3.h:S</ta>
            <ta e="T289" id="Seg_4312" s="T288">pro.h:O</ta>
            <ta e="T290" id="Seg_4313" s="T289">v:pred 0.3.h:S</ta>
            <ta e="T291" id="Seg_4314" s="T290">v:pred 0.3.h:S</ta>
            <ta e="T293" id="Seg_4315" s="T292">v:pred 0.3:S</ta>
            <ta e="T295" id="Seg_4316" s="T294">pro.h:S</ta>
            <ta e="T300" id="Seg_4317" s="T299">v:pred 0.3.h:S</ta>
            <ta e="T302" id="Seg_4318" s="T301">np:S</ta>
            <ta e="T303" id="Seg_4319" s="T302">v:pred</ta>
            <ta e="T304" id="Seg_4320" s="T303">pro.h:S</ta>
            <ta e="T305" id="Seg_4321" s="T304">v:pred</ta>
            <ta e="T307" id="Seg_4322" s="T306">pro.h:S</ta>
            <ta e="T309" id="Seg_4323" s="T308">v:pred</ta>
            <ta e="T311" id="Seg_4324" s="T310">pro.h:S</ta>
            <ta e="T313" id="Seg_4325" s="T312">ptcl.neg</ta>
            <ta e="T315" id="Seg_4326" s="T314">n:pred</ta>
            <ta e="T317" id="Seg_4327" s="T316">pro.h:S</ta>
            <ta e="T319" id="Seg_4328" s="T318">n:pred</ta>
            <ta e="T322" id="Seg_4329" s="T321">v:pred 0.3.h:S</ta>
            <ta e="T325" id="Seg_4330" s="T324">cop 0.3.h:S</ta>
            <ta e="T327" id="Seg_4331" s="T326">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T4" id="Seg_4332" s="T3">RUS:cult</ta>
            <ta e="T9" id="Seg_4333" s="T8">RUS:gram</ta>
            <ta e="T17" id="Seg_4334" s="T16">RUS:cult</ta>
            <ta e="T20" id="Seg_4335" s="T19">RUS:gram</ta>
            <ta e="T29" id="Seg_4336" s="T28">RUS:disc</ta>
            <ta e="T38" id="Seg_4337" s="T37">RUS:gram</ta>
            <ta e="T41" id="Seg_4338" s="T40">RUS:gram</ta>
            <ta e="T44" id="Seg_4339" s="T43">RUS:gram</ta>
            <ta e="T45" id="Seg_4340" s="T44">TURK:disc</ta>
            <ta e="T48" id="Seg_4341" s="T47">RUS:gram</ta>
            <ta e="T52" id="Seg_4342" s="T51">TURK:cult</ta>
            <ta e="T54" id="Seg_4343" s="T53">RUS:gram</ta>
            <ta e="T71" id="Seg_4344" s="T70">RUS:mod</ta>
            <ta e="T81" id="Seg_4345" s="T80">RUS:cult</ta>
            <ta e="T95" id="Seg_4346" s="T94">TURK:disc</ta>
            <ta e="T100" id="Seg_4347" s="T99">%TURK:core</ta>
            <ta e="T103" id="Seg_4348" s="T102">RUS:gram</ta>
            <ta e="T105" id="Seg_4349" s="T104">TURK:core</ta>
            <ta e="T124" id="Seg_4350" s="T123">TURK:core</ta>
            <ta e="T126" id="Seg_4351" s="T125">RUS:gram</ta>
            <ta e="T133" id="Seg_4352" s="T132">RUS:gram</ta>
            <ta e="T142" id="Seg_4353" s="T141">TURK:core</ta>
            <ta e="T146" id="Seg_4354" s="T145">TURK:cult</ta>
            <ta e="T149" id="Seg_4355" s="T148">TURK:disc</ta>
            <ta e="T152" id="Seg_4356" s="T151">TURK:cult</ta>
            <ta e="T161" id="Seg_4357" s="T160">RUS:disc</ta>
            <ta e="T169" id="Seg_4358" s="T168">TURK:core</ta>
            <ta e="T174" id="Seg_4359" s="T173">TURK:core</ta>
            <ta e="T175" id="Seg_4360" s="T174">RUS:cult</ta>
            <ta e="T181" id="Seg_4361" s="T180">TAT:cult</ta>
            <ta e="T197" id="Seg_4362" s="T196">TURK:gram(INDEF)</ta>
            <ta e="T217" id="Seg_4363" s="T216">TURK:core</ta>
            <ta e="T229" id="Seg_4364" s="T228">RUS:gram</ta>
            <ta e="T232" id="Seg_4365" s="T231">RUS:gram</ta>
            <ta e="T238" id="Seg_4366" s="T237">RUS:gram</ta>
            <ta e="T240" id="Seg_4367" s="T239">TURK:cult</ta>
            <ta e="T242" id="Seg_4368" s="T241">TURK:disc</ta>
            <ta e="T245" id="Seg_4369" s="T244">TURK:cult</ta>
            <ta e="T252" id="Seg_4370" s="T251">RUS:disc</ta>
            <ta e="T257" id="Seg_4371" s="T256">RUS:cult</ta>
            <ta e="T258" id="Seg_4372" s="T257">RUS:gram</ta>
            <ta e="T267" id="Seg_4373" s="T266">RUS:cult</ta>
            <ta e="T268" id="Seg_4374" s="T267">TURK:core</ta>
            <ta e="T273" id="Seg_4375" s="T272">TURK:core</ta>
            <ta e="T274" id="Seg_4376" s="T273">TAT:cult</ta>
            <ta e="T277" id="Seg_4377" s="T276">RUS:gram</ta>
            <ta e="T283" id="Seg_4378" s="T282">TURK:core</ta>
            <ta e="T286" id="Seg_4379" s="T285">TURK:disc</ta>
            <ta e="T292" id="Seg_4380" s="T291">TURK:mod(PTCL)</ta>
            <ta e="T296" id="Seg_4381" s="T295">TURK:disc</ta>
            <ta e="T306" id="Seg_4382" s="T305">RUS:gram</ta>
            <ta e="T309" id="Seg_4383" s="T308">TURK:cult</ta>
            <ta e="T310" id="Seg_4384" s="T309">RUS:gram</ta>
            <ta e="T316" id="Seg_4385" s="T315">RUS:gram</ta>
            <ta e="T321" id="Seg_4386" s="T320">RUS:cult</ta>
            <ta e="T326" id="Seg_4387" s="T325">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T71" id="Seg_4388" s="T67">RUS:calq</ta>
            <ta e="T224" id="Seg_4389" s="T223">RUS:int.ins</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_4390" s="T0">Потом она пошла, бросила клубок.</ta>
            <ta e="T16" id="Seg_4391" s="T5">Куда он катится, и она туда идёт.</ta>
            <ta e="T26" id="Seg_4392" s="T16">Клубок катился, катился и привёл её туда, где [её] муж живёт.</ta>
            <ta e="T31" id="Seg_4393" s="T26">У него уже жена есть.</ta>
            <ta e="T37" id="Seg_4394" s="T31">Она боится зайти, сидит снаружи.</ta>
            <ta e="T47" id="Seg_4395" s="T37">И блюдо поставила и яйцо (положила?) и сидит, играет.</ta>
            <ta e="T53" id="Seg_4396" s="T47">А та, [её] мужа жена, [говорит]: «Продай его мне».</ta>
            <ta e="T57" id="Seg_4397" s="T53">А она говорит: </ta>
            <ta e="T62" id="Seg_4398" s="T57">«А ты дашь мне мужа [чтобы с ним] поспать?»</ta>
            <ta e="T66" id="Seg_4399" s="T62">Она ей дала.</ta>
            <ta e="T71" id="Seg_4400" s="T66">«Спи, что мне, жалко, что ли?»</ta>
            <ta e="T74" id="Seg_4401" s="T71">Он пришёл домой.</ta>
            <ta e="T76" id="Seg_4402" s="T74">Сел поесть.</ta>
            <ta e="T81" id="Seg_4403" s="T76">Она ему дала капли.</ta>
            <ta e="T83" id="Seg_4404" s="T81">Он заснул.</ta>
            <ta e="T89" id="Seg_4405" s="T83">Она отвела его туда, где он спит.</ta>
            <ta e="T93" id="Seg_4406" s="T89">Потом её туда впустила.</ta>
            <ta e="T102" id="Seg_4407" s="T93">Она сидела-сидела, кричала-кричала, разговаривала, говорит: "Вставай!"</ta>
            <ta e="T109" id="Seg_4408" s="T102">А он не встаёт, крепко спит.</ta>
            <ta e="T112" id="Seg_4409" s="T109">Потом настало утро.</ta>
            <ta e="T114" id="Seg_4410" s="T112">Она (?).</ta>
            <ta e="T116" id="Seg_4411" s="T114">«Иди сюда!»</ta>
            <ta e="T119" id="Seg_4412" s="T116">Она пришла, [та?] ушла.</ta>
            <ta e="T122" id="Seg_4413" s="T119">Потом он встал.</ta>
            <ta e="T127" id="Seg_4414" s="T122">Потом она снова сидит и играет.</ta>
            <ta e="T131" id="Seg_4415" s="T127">Она на краю двора сидела.</ta>
            <ta e="T136" id="Seg_4416" s="T131">Потом стала кудель прясть.</ta>
            <ta e="T145" id="Seg_4417" s="T136">Она прядёт и прядёт, снова та женщина пришла, жена [её] мужа.</ta>
            <ta e="T148" id="Seg_4418" s="T145">«Продай мне это».</ta>
            <ta e="T152" id="Seg_4419" s="T148">«Нет, не продам.</ta>
            <ta e="T158" id="Seg_4420" s="T152">Дай мне [своего] мужа на ночь поспать.</ta>
            <ta e="T160" id="Seg_4421" s="T158">Тогда отдам».</ta>
            <ta e="T163" id="Seg_4422" s="T160">«Ну возьми, я дам».</ta>
            <ta e="T171" id="Seg_4423" s="T163">Потом она пришла, он снова сел есть.</ta>
            <ta e="T176" id="Seg_4424" s="T171">Она ему опять капли дала.</ta>
            <ta e="T184" id="Seg_4425" s="T176">Он заснул, она привела его в дом, где он спит.</ta>
            <ta e="T186" id="Seg_4426" s="T184">«Иди, спи!»</ta>
            <ta e="T189" id="Seg_4427" s="T186">Она туда пошла.</ta>
            <ta e="T194" id="Seg_4428" s="T189">Она ему кричала: «Вставай, вставай!»</ta>
            <ta e="T200" id="Seg_4429" s="T194">Потом он не может встать.</ta>
            <ta e="T203" id="Seg_4430" s="T200">Потом настало утро.</ta>
            <ta e="T206" id="Seg_4431" s="T203">Она говорит: «Уходи!</ta>
            <ta e="T210" id="Seg_4432" s="T206">Хватит здесь сидеть».</ta>
            <ta e="T213" id="Seg_4433" s="T210">Тогда она ушла.</ta>
            <ta e="T215" id="Seg_4434" s="T213">Он встал.</ta>
            <ta e="T220" id="Seg_4435" s="T215">Потом она снова сидит во дворе.</ta>
            <ta e="T223" id="Seg_4436" s="T220">(Веретено?) поставила, красивое.</ta>
            <ta e="T235" id="Seg_4437" s="T223">Колечко золотое на руку надевает, то на одну руку, то на другую.</ta>
            <ta e="T239" id="Seg_4438" s="T235">Та [женщина] увидела и пришла.</ta>
            <ta e="T241" id="Seg_4439" s="T239">«Продай мне!»</ta>
            <ta e="T245" id="Seg_4440" s="T241">«Нет, не продам.</ta>
            <ta e="T251" id="Seg_4441" s="T245">Дай мне сегодня твоего мужа переночевать, поспать».</ta>
            <ta e="T253" id="Seg_4442" s="T251">«Ну, бери».</ta>
            <ta e="T259" id="Seg_4443" s="T253">Тогда она ей дала колечко и веретено.</ta>
            <ta e="T264" id="Seg_4444" s="T259">Потом пришёл муж, сел есть.</ta>
            <ta e="T271" id="Seg_4445" s="T264">Она снова дала ему капли, он заснул.</ta>
            <ta e="T279" id="Seg_4446" s="T271">Она снова в дом его положила и её впустила.</ta>
            <ta e="T284" id="Seg_4447" s="T279">Она туда пришла, снова плачет.</ta>
            <ta e="T289" id="Seg_4448" s="T284">Его поднимает, поднимает.</ta>
            <ta e="T294" id="Seg_4449" s="T289">Плакала-плакала, и её слеза упала на его лицо.</ta>
            <ta e="T297" id="Seg_4450" s="T294">Он проснулся.</ta>
            <ta e="T300" id="Seg_4451" s="T297">Тогда они обнялись.</ta>
            <ta e="T305" id="Seg_4452" s="T300">Потом наступило утро, она пришла.</ta>
            <ta e="T309" id="Seg_4453" s="T305">«Ах, ты меня продала?</ta>
            <ta e="T319" id="Seg_4454" s="T309">Так не жена ты мне, а вот моя жена».</ta>
            <ta e="T327" id="Seg_4455" s="T319">Потом они бросились на пол, (стали птицами?) и улетели.</ta>
            <ta e="T328" id="Seg_4456" s="T327">[?]</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_4457" s="T0">Then she went, she threw the clew.</ta>
            <ta e="T16" id="Seg_4458" s="T5">Where it rolls, there she goes, too.</ta>
            <ta e="T26" id="Seg_4459" s="T16">The clew rolled, and rolled, and brought [her] where her husband lives.</ta>
            <ta e="T31" id="Seg_4460" s="T26">He has already a wife.</ta>
            <ta e="T37" id="Seg_4461" s="T31">She's afraid to come in, she's sitting outside.</ta>
            <ta e="T47" id="Seg_4462" s="T37">She set the dish and (put?) the egg [on it] and is sitting and playing.</ta>
            <ta e="T53" id="Seg_4463" s="T47">And she, [her] husband's wife, [says]: "Sell it to me."</ta>
            <ta e="T57" id="Seg_4464" s="T53">And she says:</ta>
            <ta e="T62" id="Seg_4465" s="T57">"Will you give me your husband to sleep [with]?"</ta>
            <ta e="T66" id="Seg_4466" s="T62">She gave her.</ta>
            <ta e="T71" id="Seg_4467" s="T66">"Sleep, why not, will I feel sorry [for him]?"</ta>
            <ta e="T74" id="Seg_4468" s="T71">He entered the house.</ta>
            <ta e="T76" id="Seg_4469" s="T74">Sat down to eat.</ta>
            <ta e="T81" id="Seg_4470" s="T76">She gave him drops.</ta>
            <ta e="T83" id="Seg_4471" s="T81">He fell asleep.</ta>
            <ta e="T89" id="Seg_4472" s="T83">She brought him to where he sleeps.</ta>
            <ta e="T93" id="Seg_4473" s="T89">Then let her in.</ta>
            <ta e="T102" id="Seg_4474" s="T93">She sat [there] for a long time, she shouted, spoke, she said: "Get up!"</ta>
            <ta e="T109" id="Seg_4475" s="T102">And he doesn't get up, he’s fast asleep.</ta>
            <ta e="T112" id="Seg_4476" s="T109">Then the morning came.</ta>
            <ta e="T114" id="Seg_4477" s="T112">S/he (?).</ta>
            <ta e="T116" id="Seg_4478" s="T114">"Come here!"</ta>
            <ta e="T119" id="Seg_4479" s="T116">She came and [the other?] went away.</ta>
            <ta e="T122" id="Seg_4480" s="T119">Then he got up.</ta>
            <ta e="T127" id="Seg_4481" s="T122">Then she is sitting and playing again.</ta>
            <ta e="T131" id="Seg_4482" s="T127">She was sitting at the edge of their yard.</ta>
            <ta e="T136" id="Seg_4483" s="T131">Then she began to spin that tow.</ta>
            <ta e="T145" id="Seg_4484" s="T136">She spinned and spinned, and then this woman came again, the man's woman.</ta>
            <ta e="T148" id="Seg_4485" s="T145">"Sell it to me."</ta>
            <ta e="T152" id="Seg_4486" s="T148">"No, I won't sell [it].</ta>
            <ta e="T158" id="Seg_4487" s="T152">Will you give me [your] husband to sleep with at night.</ta>
            <ta e="T160" id="Seg_4488" s="T158">Then I'll give [it]."</ta>
            <ta e="T163" id="Seg_4489" s="T160">"Well, take it, I'll give."</ta>
            <ta e="T171" id="Seg_4490" s="T163">Then she came, he sat down to eat again.</ta>
            <ta e="T176" id="Seg_4491" s="T171">She gave him drops again.</ta>
            <ta e="T184" id="Seg_4492" s="T176">He fell asleep, she brought him in the house, where he sleeps.</ta>
            <ta e="T186" id="Seg_4493" s="T184">"Go, sleep!"</ta>
            <ta e="T189" id="Seg_4494" s="T186">She went there.</ta>
            <ta e="T194" id="Seg_4495" s="T189">She shouted to him: "Get up, get up!"</ta>
            <ta e="T200" id="Seg_4496" s="T194">Then he can't get up.</ta>
            <ta e="T203" id="Seg_4497" s="T200">Then the morning came.</ta>
            <ta e="T206" id="Seg_4498" s="T203">She says: "Go away!</ta>
            <ta e="T210" id="Seg_4499" s="T206">Enough of sitting here."</ta>
            <ta e="T213" id="Seg_4500" s="T210">Then she went away.</ta>
            <ta e="T215" id="Seg_4501" s="T213">He got up.</ta>
            <ta e="T220" id="Seg_4502" s="T215">Then, again she sat in the yard.</ta>
            <ta e="T223" id="Seg_4503" s="T220">She put the (spindle?), [it is] beautiful.</ta>
            <ta e="T235" id="Seg_4504" s="T223">She puts the golden ring to her hand, first on one hand, then to another.</ta>
            <ta e="T239" id="Seg_4505" s="T235">That [woman] saw [it] and went [there].</ta>
            <ta e="T241" id="Seg_4506" s="T239">"Sell it to me!"</ta>
            <ta e="T245" id="Seg_4507" s="T241">"No, I don't sell it.</ta>
            <ta e="T251" id="Seg_4508" s="T245">Give me today your husband to pass the night, to sleep with."</ta>
            <ta e="T253" id="Seg_4509" s="T251">"Well, take [him]."</ta>
            <ta e="T259" id="Seg_4510" s="T253">Then she gave her the ring and the spindle. </ta>
            <ta e="T264" id="Seg_4511" s="T259">Then the man came, sat down to eat.</ta>
            <ta e="T271" id="Seg_4512" s="T264">She gave him again the drops, he fell asleep.</ta>
            <ta e="T279" id="Seg_4513" s="T271">She brought him again in the house and let her in.</ta>
            <ta e="T284" id="Seg_4514" s="T279">She went there, cried again.</ta>
            <ta e="T289" id="Seg_4515" s="T284">She raises him, lifts him.</ta>
            <ta e="T294" id="Seg_4516" s="T289">She cried and cried, and her tear fell on his face.</ta>
            <ta e="T297" id="Seg_4517" s="T294">He woke up.</ta>
            <ta e="T300" id="Seg_4518" s="T297">Then they hugged each other.</ta>
            <ta e="T305" id="Seg_4519" s="T300">Then the morning came, she came.</ta>
            <ta e="T309" id="Seg_4520" s="T305">"Have you sold me?</ta>
            <ta e="T319" id="Seg_4521" s="T309">So you are not my wife, this is my wife."</ta>
            <ta e="T327" id="Seg_4522" s="T319">Then they fell on the floor, (became birds?) and flew away.</ta>
            <ta e="T328" id="Seg_4523" s="T327">[?]</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_4524" s="T0">Dann ging sie, sie warf das Knäuel.</ta>
            <ta e="T16" id="Seg_4525" s="T5">Wohin es rollt, dorthin geht sie auch.</ta>
            <ta e="T26" id="Seg_4526" s="T16">Das Knäuel rollte und rollte und brachte [sie] dorthin, wo ihr Mann lebt.</ta>
            <ta e="T31" id="Seg_4527" s="T26">Er hat schon eine Frau.</ta>
            <ta e="T37" id="Seg_4528" s="T31">Sie hat Angst hineinzukommen, sie sitzt draußen.</ta>
            <ta e="T47" id="Seg_4529" s="T37">Sie stellte das Essen hin und (tat?) das Ei darauf und sie sitzt und spielt.</ta>
            <ta e="T53" id="Seg_4530" s="T47">Und sie, die Frau [ihres] Mannes, [sagt]: "Verkauf es mir."</ta>
            <ta e="T57" id="Seg_4531" s="T53">Und sie sagt:</ta>
            <ta e="T62" id="Seg_4532" s="T57">"Gibst du mir deinen Mann, um [mit ihm] zu schlafen?"</ta>
            <ta e="T66" id="Seg_4533" s="T62">Sie gab [ihn] ihr.</ta>
            <ta e="T71" id="Seg_4534" s="T66">"Schlafe, warum nicht, ist [er] mir nichts zu schade."</ta>
            <ta e="T74" id="Seg_4535" s="T71">Er ging ins Haus herein.</ta>
            <ta e="T76" id="Seg_4536" s="T74">Setzte sich, um zu essen.</ta>
            <ta e="T81" id="Seg_4537" s="T76">Sie gab ihm Tropfen.</ta>
            <ta e="T83" id="Seg_4538" s="T81">Er schlief ein.</ta>
            <ta e="T89" id="Seg_4539" s="T83">Sie brachte ihn dorthin, wo er schläft.</ta>
            <ta e="T93" id="Seg_4540" s="T89">Dann ließ sie sie herein.</ta>
            <ta e="T102" id="Seg_4541" s="T93">Sie saß lange [dort], sie schrie, sprach, sie sagte: "Steh auf!"</ta>
            <ta e="T109" id="Seg_4542" s="T102">Und er steht nicht auf, er schlief fest.</ta>
            <ta e="T112" id="Seg_4543" s="T109">Dann wurde es Morgen.</ta>
            <ta e="T114" id="Seg_4544" s="T112">Er/sie (?).</ta>
            <ta e="T116" id="Seg_4545" s="T114">"Komm her!"</ta>
            <ta e="T119" id="Seg_4546" s="T116">Sie kam und [die andere?] ging weg.</ta>
            <ta e="T122" id="Seg_4547" s="T119">Dann stand er auf.</ta>
            <ta e="T127" id="Seg_4548" s="T122">Dann sitzt und spielt sie wieder.</ta>
            <ta e="T131" id="Seg_4549" s="T127">Sie saß am Rande ihres Hofes.</ta>
            <ta e="T136" id="Seg_4550" s="T131">Dann begann sie dieses Tau zu spinnen.</ta>
            <ta e="T145" id="Seg_4551" s="T136">Sie spann und spann, und dann kam diese Frau wieder, die Frau des Mannes.</ta>
            <ta e="T148" id="Seg_4552" s="T145">"Verkaufe es mir."</ta>
            <ta e="T152" id="Seg_4553" s="T148">"Nein, ich verkaufe [es] nicht.</ta>
            <ta e="T158" id="Seg_4554" s="T152">Gibst du mir [deinen] Mann, um [mit ihm] in der Nacht zu schlafen.</ta>
            <ta e="T160" id="Seg_4555" s="T158">Dann gebe ich [es] dir."</ta>
            <ta e="T163" id="Seg_4556" s="T160">"Gut, nimm es, ich gebe [ihn]."</ta>
            <ta e="T171" id="Seg_4557" s="T163">Dann kam sie, er setzte sich wieder, um zu essen.</ta>
            <ta e="T176" id="Seg_4558" s="T171">Sie gab ihm wieder Tropfen.</ta>
            <ta e="T184" id="Seg_4559" s="T176">Er schlief ein, sie brachte ihn ins Haus, wo er schläft.</ta>
            <ta e="T186" id="Seg_4560" s="T184">"Geh, schlaf!"</ta>
            <ta e="T189" id="Seg_4561" s="T186">Sie ging dorthin.</ta>
            <ta e="T194" id="Seg_4562" s="T189">Sie schrie ihn an: "Steh auf, steh auf!"</ta>
            <ta e="T200" id="Seg_4563" s="T194">Dann er kann nicht aufstehen.</ta>
            <ta e="T203" id="Seg_4564" s="T200">Dann wurde es Morgen.</ta>
            <ta e="T206" id="Seg_4565" s="T203">Sie sagt: "Geh weg!</ta>
            <ta e="T210" id="Seg_4566" s="T206">Genug hier gesessen."</ta>
            <ta e="T213" id="Seg_4567" s="T210">Dann ging sie weg.</ta>
            <ta e="T215" id="Seg_4568" s="T213">Er stand auf.</ta>
            <ta e="T220" id="Seg_4569" s="T215">Dann saß sie wieder im Hof.</ta>
            <ta e="T223" id="Seg_4570" s="T220">Sie stellte (die Spindel?) hin, [sie ist] schön.</ta>
            <ta e="T235" id="Seg_4571" s="T223">Sie steckte den goldenen Ring auf ihre Hand, erst auf eine Hand, dann auf die andere.</ta>
            <ta e="T239" id="Seg_4572" s="T235">Jene [Frau] sah [es] und ging [dorthin].</ta>
            <ta e="T241" id="Seg_4573" s="T239">"Verkauf ihn mir!"</ta>
            <ta e="T245" id="Seg_4574" s="T241">"Nein, ich verkaufe ihn nicht.</ta>
            <ta e="T251" id="Seg_4575" s="T245">Gib mir heute deinen Mann, um die Nacht zu verbringen, um [mit ihm] zu schlafen."</ta>
            <ta e="T253" id="Seg_4576" s="T251">"Gut, nimm [ihn]."</ta>
            <ta e="T259" id="Seg_4577" s="T253">Dann gab sie ihr den Ring und die Spindel.</ta>
            <ta e="T264" id="Seg_4578" s="T259">Dann kam der Mann, er setzte sich, um zu essen.</ta>
            <ta e="T271" id="Seg_4579" s="T264">Sie gab ihm wieder die Tropfen, er schlief ein.</ta>
            <ta e="T279" id="Seg_4580" s="T271">Sie brachte ihn wieder ins Haus und ließ sie herein.</ta>
            <ta e="T284" id="Seg_4581" s="T279">Sie ging dorthin, weinte wieder.</ta>
            <ta e="T289" id="Seg_4582" s="T284">Sie hebt ihn an, hebt ihn hoch.</ta>
            <ta e="T294" id="Seg_4583" s="T289">Sie weinte und weinte und eine ihrer Tränen fiel auf sein Gesicht.</ta>
            <ta e="T297" id="Seg_4584" s="T294">Er wachte auf.</ta>
            <ta e="T300" id="Seg_4585" s="T297">Dann umarmten Sie sich.</ta>
            <ta e="T305" id="Seg_4586" s="T300">Dann wurde es Morgen, sie kam.</ta>
            <ta e="T309" id="Seg_4587" s="T305">"Hast du mich verkauft?</ta>
            <ta e="T319" id="Seg_4588" s="T309">Du bist also nicht meine Frau, das ist meine Frau."</ta>
            <ta e="T327" id="Seg_4589" s="T319">Dann fielen sie auf den Boden, (wurden zu Vögeln?) und flogen davon.</ta>
            <ta e="T328" id="Seg_4590" s="T327">[?]</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T47" id="Seg_4591" s="T37">[GVY:] emdəbi seems to be a contamination of nuldəbi and embi.</ta>
            <ta e="T131" id="Seg_4592" s="T127">[GVY:] seden = šeden (D 63a). Or seden dĭn?</ta>
            <ta e="T136" id="Seg_4593" s="T131">[GVY] kugʼelʼam</ta>
            <ta e="T152" id="Seg_4594" s="T148">[GVY:] [jok]</ta>
            <ta e="T184" id="Seg_4595" s="T176">[GVY:] kambi = kumbi [lead-PST]</ta>
            <ta e="T220" id="Seg_4596" s="T215">[GVY:] sezen = šeden (cf. above)?</ta>
            <ta e="T223" id="Seg_4597" s="T220">[GVY:] kornɨš = kormiš?</ta>
            <ta e="T328" id="Seg_4598" s="T327">[GVY:] bardə [all-3SG]?</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T329" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
