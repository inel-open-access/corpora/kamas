<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID47B7C90D-CDC8-8B2B-C359-BD5CE6AA07AA">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_BlacksmithAndMerchant_flk.wav" />
         <referenced-file url="PKZ_196X_BlacksmithAndMerchant_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_BlacksmithAndMerchant_flk\PKZ_196X_BlacksmithAndMerchant_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">137</ud-information>
            <ud-information attribute-name="# HIAT:w">92</ud-information>
            <ud-information attribute-name="# e">92</ud-information>
            <ud-information attribute-name="# HIAT:u">13</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T366" time="0.009" type="appl" />
         <tli id="T367" time="0.746" type="appl" />
         <tli id="T368" time="1.482" type="appl" />
         <tli id="T369" time="2.219" type="appl" />
         <tli id="T370" time="2.956" type="appl" />
         <tli id="T371" time="3.692" type="appl" />
         <tli id="T372" time="5.099682024880907" />
         <tli id="T373" time="5.888" type="appl" />
         <tli id="T374" time="6.628" type="appl" />
         <tli id="T375" time="8.452806284377763" />
         <tli id="T376" time="8.992" type="appl" />
         <tli id="T377" time="9.447" type="appl" />
         <tli id="T378" time="9.901" type="appl" />
         <tli id="T379" time="10.356" type="appl" />
         <tli id="T380" time="10.81" type="appl" />
         <tli id="T381" time="11.414" type="appl" />
         <tli id="T382" time="12.019" type="appl" />
         <tli id="T383" time="12.623" type="appl" />
         <tli id="T384" time="13.227" type="appl" />
         <tli id="T385" time="13.832" type="appl" />
         <tli id="T386" time="14.436" type="appl" />
         <tli id="T387" time="15.041" type="appl" />
         <tli id="T388" time="15.645" type="appl" />
         <tli id="T389" time="16.249" type="appl" />
         <tli id="T390" time="16.854" type="appl" />
         <tli id="T391" time="17.458" type="appl" />
         <tli id="T392" time="18.103" type="appl" />
         <tli id="T393" time="18.748" type="appl" />
         <tli id="T394" time="19.394" type="appl" />
         <tli id="T395" time="20.039" type="appl" />
         <tli id="T396" time="20.684" type="appl" />
         <tli id="T397" time="21.329" type="appl" />
         <tli id="T398" time="21.974" type="appl" />
         <tli id="T399" time="22.62" type="appl" />
         <tli id="T400" time="23.265" type="appl" />
         <tli id="T401" time="23.91" type="appl" />
         <tli id="T402" time="24.882" type="appl" />
         <tli id="T403" time="25.854" type="appl" />
         <tli id="T404" time="26.825" type="appl" />
         <tli id="T405" time="27.797" type="appl" />
         <tli id="T406" time="28.769" type="appl" />
         <tli id="T407" time="29.894" type="appl" />
         <tli id="T408" time="30.806" type="appl" />
         <tli id="T409" time="31.717" type="appl" />
         <tli id="T410" time="32.279" type="appl" />
         <tli id="T411" time="32.842" type="appl" />
         <tli id="T412" time="33.404" type="appl" />
         <tli id="T413" time="33.967" type="appl" />
         <tli id="T414" time="34.751166530332235" />
         <tli id="T415" time="35.37" type="appl" />
         <tli id="T416" time="36.134" type="appl" />
         <tli id="T417" time="36.899" type="appl" />
         <tli id="T418" time="37.664" type="appl" />
         <tli id="T419" time="38.429" type="appl" />
         <tli id="T420" time="39.194" type="appl" />
         <tli id="T421" time="39.958" type="appl" />
         <tli id="T422" time="43.030650288374176" />
         <tli id="T423" time="43.699" type="appl" />
         <tli id="T424" time="44.419" type="appl" />
         <tli id="T425" time="45.139" type="appl" />
         <tli id="T426" time="45.859" type="appl" />
         <tli id="T427" time="46.579" type="appl" />
         <tli id="T428" time="47.299" type="appl" />
         <tli id="T429" time="48.019" type="appl" />
         <tli id="T430" time="48.739" type="appl" />
         <tli id="T431" time="49.459" type="appl" />
         <tli id="T432" time="50.179" type="appl" />
         <tli id="T433" time="50.908" type="appl" />
         <tli id="T434" time="51.638" type="appl" />
         <tli id="T435" time="52.367" type="appl" />
         <tli id="T436" time="53.097" type="appl" />
         <tli id="T437" time="53.826" type="appl" />
         <tli id="T438" time="54.575" type="appl" />
         <tli id="T439" time="55.323" type="appl" />
         <tli id="T440" time="56.072" type="appl" />
         <tli id="T441" time="56.821" type="appl" />
         <tli id="T442" time="57.57" type="appl" />
         <tli id="T443" time="58.318" type="appl" />
         <tli id="T444" time="59.067" type="appl" />
         <tli id="T445" time="59.656" type="appl" />
         <tli id="T446" time="60.245" type="appl" />
         <tli id="T447" time="60.834" type="appl" />
         <tli id="T448" time="61.423" type="appl" />
         <tli id="T449" time="62.012" type="appl" />
         <tli id="T450" time="62.601" type="appl" />
         <tli id="T451" time="63.19" type="appl" />
         <tli id="T452" time="63.779" type="appl" />
         <tli id="T453" time="64.368" type="appl" />
         <tli id="T454" time="65.1959349063206" />
         <tli id="T455" time="65.932" type="appl" />
         <tli id="T456" time="66.578" type="appl" />
         <tli id="T457" time="67.223" type="appl" />
         <tli id="T458" time="67.869" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T458" id="Seg_0" n="sc" s="T366">
               <ts e="T372" id="Seg_2" n="HIAT:u" s="T366">
                  <ts e="T367" id="Seg_4" n="HIAT:w" s="T366">Amnobiʔi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_7" n="HIAT:w" s="T367">šide</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_10" n="HIAT:w" s="T368">kuza</ts>
                  <nts id="Seg_11" n="HIAT:ip">,</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_14" n="HIAT:w" s="T369">kupʼes</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_17" n="HIAT:w" s="T370">i</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_20" n="HIAT:w" s="T371">kuznʼes</ts>
                  <nts id="Seg_21" n="HIAT:ip">.</nts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T375" id="Seg_24" n="HIAT:u" s="T372">
                  <nts id="Seg_25" n="HIAT:ip">(</nts>
                  <ts e="T373" id="Seg_27" n="HIAT:w" s="T372">Kuznʼesən</ts>
                  <nts id="Seg_28" n="HIAT:ip">)</nts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_31" n="HIAT:w" s="T373">ĭmbidə</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_34" n="HIAT:w" s="T374">nagobi</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T380" id="Seg_38" n="HIAT:u" s="T375">
                  <ts e="T376" id="Seg_40" n="HIAT:w" s="T375">A</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_43" n="HIAT:w" s="T376">dĭn</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_46" n="HIAT:w" s="T377">ugaːndə</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_49" n="HIAT:w" s="T378">aktʼa</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_52" n="HIAT:w" s="T379">iʔgö</ts>
                  <nts id="Seg_53" n="HIAT:ip">.</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T391" id="Seg_56" n="HIAT:u" s="T380">
                  <ts e="T381" id="Seg_58" n="HIAT:w" s="T380">Dĭ</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_61" n="HIAT:w" s="T381">uge</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_64" n="HIAT:w" s="T382">mandolaʔbə</ts>
                  <nts id="Seg_65" n="HIAT:ip">,</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_68" n="HIAT:w" s="T383">šində</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_71" n="HIAT:w" s="T384">bɨ</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_74" n="HIAT:w" s="T385">ej</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_77" n="HIAT:w" s="T386">tojirbi</ts>
                  <nts id="Seg_78" n="HIAT:ip">,</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_81" n="HIAT:w" s="T387">ej</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_84" n="HIAT:w" s="T388">kunollaʔbə</ts>
                  <nts id="Seg_85" n="HIAT:ip">,</nts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_88" n="HIAT:w" s="T389">ej</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_91" n="HIAT:w" s="T390">amnia</ts>
                  <nts id="Seg_92" n="HIAT:ip">.</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T401" id="Seg_95" n="HIAT:u" s="T391">
                  <ts e="T392" id="Seg_97" n="HIAT:w" s="T391">A</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_100" n="HIAT:w" s="T392">dĭ</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_102" n="HIAT:ip">(</nts>
                  <ts e="T394" id="Seg_104" n="HIAT:w" s="T393">ka-</ts>
                  <nts id="Seg_105" n="HIAT:ip">)</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_108" n="HIAT:w" s="T394">kalləj</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_110" n="HIAT:ip">(</nts>
                  <ts e="T396" id="Seg_112" n="HIAT:w" s="T395">dĭ</ts>
                  <nts id="Seg_113" n="HIAT:ip">)</nts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_116" n="HIAT:w" s="T396">kuznʼitsanə</ts>
                  <nts id="Seg_117" n="HIAT:ip">,</nts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_120" n="HIAT:w" s="T397">dĭ</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_123" n="HIAT:w" s="T398">togonorlaʔbə</ts>
                  <nts id="Seg_124" n="HIAT:ip">,</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_127" n="HIAT:w" s="T399">nüjnə</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_130" n="HIAT:w" s="T400">nüjleʔbə</ts>
                  <nts id="Seg_131" n="HIAT:ip">.</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T406" id="Seg_134" n="HIAT:u" s="T401">
                  <ts e="T402" id="Seg_136" n="HIAT:w" s="T401">Maːndə</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_139" n="HIAT:w" s="T402">šonəga</ts>
                  <nts id="Seg_140" n="HIAT:ip">,</nts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_143" n="HIAT:w" s="T403">ĭmbi-nʼibudʼ</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_146" n="HIAT:w" s="T404">dʼăbaktərlaʔbə</ts>
                  <nts id="Seg_147" n="HIAT:ip">,</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_150" n="HIAT:w" s="T405">šaːmnaʔbə</ts>
                  <nts id="Seg_151" n="HIAT:ip">.</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T414" id="Seg_154" n="HIAT:u" s="T406">
                  <ts e="T407" id="Seg_156" n="HIAT:w" s="T406">Dĭgəttə</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_159" n="HIAT:w" s="T407">dĭ</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_162" n="HIAT:w" s="T408">mandolaʔbə:</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_164" n="HIAT:ip">"</nts>
                  <ts e="T410" id="Seg_166" n="HIAT:w" s="T409">Ĭmbi</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_169" n="HIAT:w" s="T410">tăn</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_172" n="HIAT:w" s="T411">üge</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_175" n="HIAT:w" s="T412">nüjnə</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_178" n="HIAT:w" s="T413">nüjliel</ts>
                  <nts id="Seg_179" n="HIAT:ip">?</nts>
                  <nts id="Seg_180" n="HIAT:ip">"</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T422" id="Seg_183" n="HIAT:u" s="T414">
                  <nts id="Seg_184" n="HIAT:ip">"</nts>
                  <ts e="T415" id="Seg_186" n="HIAT:w" s="T414">Da</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_189" n="HIAT:w" s="T415">măn</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_192" n="HIAT:w" s="T416">togonoriam</ts>
                  <nts id="Seg_193" n="HIAT:ip">,</nts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_196" n="HIAT:w" s="T417">sĭjdə</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_199" n="HIAT:w" s="T418">bar</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_201" n="HIAT:ip">(</nts>
                  <ts e="T420" id="Seg_203" n="HIAT:w" s="T419">vesʼo-</ts>
                  <nts id="Seg_204" n="HIAT:ip">)</nts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_207" n="HIAT:w" s="T420">vesʼolɨj</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_210" n="HIAT:w" s="T421">molaʔbə</ts>
                  <nts id="Seg_211" n="HIAT:ip">"</nts>
                  <nts id="Seg_212" n="HIAT:ip">.</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T432" id="Seg_215" n="HIAT:u" s="T422">
                  <ts e="T423" id="Seg_217" n="HIAT:w" s="T422">Dĭgəttə</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_220" n="HIAT:w" s="T423">dĭ</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_223" n="HIAT:w" s="T424">ibi</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_226" n="HIAT:w" s="T425">bostə</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_229" n="HIAT:w" s="T426">aktʼa</ts>
                  <nts id="Seg_230" n="HIAT:ip">,</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_233" n="HIAT:w" s="T427">kambi</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_236" n="HIAT:w" s="T428">da</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_239" n="HIAT:w" s="T429">baruʔluʔpi</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_242" n="HIAT:w" s="T430">dĭn</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_244" n="HIAT:ip">(</nts>
                  <ts e="T432" id="Seg_246" n="HIAT:w" s="T431">kuznʼitsanə</ts>
                  <nts id="Seg_247" n="HIAT:ip">)</nts>
                  <nts id="Seg_248" n="HIAT:ip">.</nts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T437" id="Seg_251" n="HIAT:u" s="T432">
                  <ts e="T433" id="Seg_253" n="HIAT:w" s="T432">Dĭ</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_256" n="HIAT:w" s="T433">ibi</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_259" n="HIAT:w" s="T434">bar</ts>
                  <nts id="Seg_260" n="HIAT:ip">,</nts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_263" n="HIAT:w" s="T435">davaj</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_266" n="HIAT:w" s="T436">măndərzittə</ts>
                  <nts id="Seg_267" n="HIAT:ip">.</nts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T444" id="Seg_270" n="HIAT:u" s="T437">
                  <ts e="T438" id="Seg_272" n="HIAT:w" s="T437">Dĭgəttə</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_275" n="HIAT:w" s="T438">ej</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_278" n="HIAT:w" s="T439">molia</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_281" n="HIAT:w" s="T440">togonorzittə</ts>
                  <nts id="Seg_282" n="HIAT:ip">,</nts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_285" n="HIAT:w" s="T441">ej</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_288" n="HIAT:w" s="T442">molia</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_291" n="HIAT:w" s="T443">togonorzittə</ts>
                  <nts id="Seg_292" n="HIAT:ip">.</nts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T454" id="Seg_295" n="HIAT:u" s="T444">
                  <ts e="T445" id="Seg_297" n="HIAT:w" s="T444">Da</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_300" n="HIAT:w" s="T445">kambi</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_303" n="HIAT:w" s="T446">dĭ</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_306" n="HIAT:w" s="T447">kupʼes:</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_308" n="HIAT:ip">"</nts>
                  <ts e="T449" id="Seg_310" n="HIAT:w" s="T448">Ĭmbi</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_313" n="HIAT:w" s="T449">ej</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_316" n="HIAT:w" s="T450">togonorial</ts>
                  <nts id="Seg_317" n="HIAT:ip">,</nts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_320" n="HIAT:w" s="T451">nüjnə</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_323" n="HIAT:w" s="T452">ej</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_326" n="HIAT:w" s="T453">nüjliel</ts>
                  <nts id="Seg_327" n="HIAT:ip">?</nts>
                  <nts id="Seg_328" n="HIAT:ip">"</nts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T458" id="Seg_331" n="HIAT:u" s="T454">
                  <nts id="Seg_332" n="HIAT:ip">"</nts>
                  <ts e="T455" id="Seg_334" n="HIAT:w" s="T454">Da</ts>
                  <nts id="Seg_335" n="HIAT:ip">,</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_338" n="HIAT:w" s="T455">na</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_341" n="HIAT:w" s="T456">tăn</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_344" n="HIAT:w" s="T457">aktʼal</ts>
                  <nts id="Seg_345" n="HIAT:ip">!</nts>
                  <nts id="Seg_346" n="HIAT:ip">"</nts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T458" id="Seg_348" n="sc" s="T366">
               <ts e="T367" id="Seg_350" n="e" s="T366">Amnobiʔi </ts>
               <ts e="T368" id="Seg_352" n="e" s="T367">šide </ts>
               <ts e="T369" id="Seg_354" n="e" s="T368">kuza, </ts>
               <ts e="T370" id="Seg_356" n="e" s="T369">kupʼes </ts>
               <ts e="T371" id="Seg_358" n="e" s="T370">i </ts>
               <ts e="T372" id="Seg_360" n="e" s="T371">kuznʼes. </ts>
               <ts e="T373" id="Seg_362" n="e" s="T372">(Kuznʼesən) </ts>
               <ts e="T374" id="Seg_364" n="e" s="T373">ĭmbidə </ts>
               <ts e="T375" id="Seg_366" n="e" s="T374">nagobi. </ts>
               <ts e="T376" id="Seg_368" n="e" s="T375">A </ts>
               <ts e="T377" id="Seg_370" n="e" s="T376">dĭn </ts>
               <ts e="T378" id="Seg_372" n="e" s="T377">ugaːndə </ts>
               <ts e="T379" id="Seg_374" n="e" s="T378">aktʼa </ts>
               <ts e="T380" id="Seg_376" n="e" s="T379">iʔgö. </ts>
               <ts e="T381" id="Seg_378" n="e" s="T380">Dĭ </ts>
               <ts e="T382" id="Seg_380" n="e" s="T381">uge </ts>
               <ts e="T383" id="Seg_382" n="e" s="T382">mandolaʔbə, </ts>
               <ts e="T384" id="Seg_384" n="e" s="T383">šində </ts>
               <ts e="T385" id="Seg_386" n="e" s="T384">bɨ </ts>
               <ts e="T386" id="Seg_388" n="e" s="T385">ej </ts>
               <ts e="T387" id="Seg_390" n="e" s="T386">tojirbi, </ts>
               <ts e="T388" id="Seg_392" n="e" s="T387">ej </ts>
               <ts e="T389" id="Seg_394" n="e" s="T388">kunollaʔbə, </ts>
               <ts e="T390" id="Seg_396" n="e" s="T389">ej </ts>
               <ts e="T391" id="Seg_398" n="e" s="T390">amnia. </ts>
               <ts e="T392" id="Seg_400" n="e" s="T391">A </ts>
               <ts e="T393" id="Seg_402" n="e" s="T392">dĭ </ts>
               <ts e="T394" id="Seg_404" n="e" s="T393">(ka-) </ts>
               <ts e="T395" id="Seg_406" n="e" s="T394">kalləj </ts>
               <ts e="T396" id="Seg_408" n="e" s="T395">(dĭ) </ts>
               <ts e="T397" id="Seg_410" n="e" s="T396">kuznʼitsanə, </ts>
               <ts e="T398" id="Seg_412" n="e" s="T397">dĭ </ts>
               <ts e="T399" id="Seg_414" n="e" s="T398">togonorlaʔbə, </ts>
               <ts e="T400" id="Seg_416" n="e" s="T399">nüjnə </ts>
               <ts e="T401" id="Seg_418" n="e" s="T400">nüjleʔbə. </ts>
               <ts e="T402" id="Seg_420" n="e" s="T401">Maːndə </ts>
               <ts e="T403" id="Seg_422" n="e" s="T402">šonəga, </ts>
               <ts e="T404" id="Seg_424" n="e" s="T403">ĭmbi-nʼibudʼ </ts>
               <ts e="T405" id="Seg_426" n="e" s="T404">dʼăbaktərlaʔbə, </ts>
               <ts e="T406" id="Seg_428" n="e" s="T405">šaːmnaʔbə. </ts>
               <ts e="T407" id="Seg_430" n="e" s="T406">Dĭgəttə </ts>
               <ts e="T408" id="Seg_432" n="e" s="T407">dĭ </ts>
               <ts e="T409" id="Seg_434" n="e" s="T408">mandolaʔbə: </ts>
               <ts e="T410" id="Seg_436" n="e" s="T409">"Ĭmbi </ts>
               <ts e="T411" id="Seg_438" n="e" s="T410">tăn </ts>
               <ts e="T412" id="Seg_440" n="e" s="T411">üge </ts>
               <ts e="T413" id="Seg_442" n="e" s="T412">nüjnə </ts>
               <ts e="T414" id="Seg_444" n="e" s="T413">nüjliel?" </ts>
               <ts e="T415" id="Seg_446" n="e" s="T414">"Da </ts>
               <ts e="T416" id="Seg_448" n="e" s="T415">măn </ts>
               <ts e="T417" id="Seg_450" n="e" s="T416">togonoriam, </ts>
               <ts e="T418" id="Seg_452" n="e" s="T417">sĭjdə </ts>
               <ts e="T419" id="Seg_454" n="e" s="T418">bar </ts>
               <ts e="T420" id="Seg_456" n="e" s="T419">(vesʼo-) </ts>
               <ts e="T421" id="Seg_458" n="e" s="T420">vesʼolɨj </ts>
               <ts e="T422" id="Seg_460" n="e" s="T421">molaʔbə". </ts>
               <ts e="T423" id="Seg_462" n="e" s="T422">Dĭgəttə </ts>
               <ts e="T424" id="Seg_464" n="e" s="T423">dĭ </ts>
               <ts e="T425" id="Seg_466" n="e" s="T424">ibi </ts>
               <ts e="T426" id="Seg_468" n="e" s="T425">bostə </ts>
               <ts e="T427" id="Seg_470" n="e" s="T426">aktʼa, </ts>
               <ts e="T428" id="Seg_472" n="e" s="T427">kambi </ts>
               <ts e="T429" id="Seg_474" n="e" s="T428">da </ts>
               <ts e="T430" id="Seg_476" n="e" s="T429">baruʔluʔpi </ts>
               <ts e="T431" id="Seg_478" n="e" s="T430">dĭn </ts>
               <ts e="T432" id="Seg_480" n="e" s="T431">(kuznʼitsanə). </ts>
               <ts e="T433" id="Seg_482" n="e" s="T432">Dĭ </ts>
               <ts e="T434" id="Seg_484" n="e" s="T433">ibi </ts>
               <ts e="T435" id="Seg_486" n="e" s="T434">bar, </ts>
               <ts e="T436" id="Seg_488" n="e" s="T435">davaj </ts>
               <ts e="T437" id="Seg_490" n="e" s="T436">măndərzittə. </ts>
               <ts e="T438" id="Seg_492" n="e" s="T437">Dĭgəttə </ts>
               <ts e="T439" id="Seg_494" n="e" s="T438">ej </ts>
               <ts e="T440" id="Seg_496" n="e" s="T439">molia </ts>
               <ts e="T441" id="Seg_498" n="e" s="T440">togonorzittə, </ts>
               <ts e="T442" id="Seg_500" n="e" s="T441">ej </ts>
               <ts e="T443" id="Seg_502" n="e" s="T442">molia </ts>
               <ts e="T444" id="Seg_504" n="e" s="T443">togonorzittə. </ts>
               <ts e="T445" id="Seg_506" n="e" s="T444">Da </ts>
               <ts e="T446" id="Seg_508" n="e" s="T445">kambi </ts>
               <ts e="T447" id="Seg_510" n="e" s="T446">dĭ </ts>
               <ts e="T448" id="Seg_512" n="e" s="T447">kupʼes: </ts>
               <ts e="T449" id="Seg_514" n="e" s="T448">"Ĭmbi </ts>
               <ts e="T450" id="Seg_516" n="e" s="T449">ej </ts>
               <ts e="T451" id="Seg_518" n="e" s="T450">togonorial, </ts>
               <ts e="T452" id="Seg_520" n="e" s="T451">nüjnə </ts>
               <ts e="T453" id="Seg_522" n="e" s="T452">ej </ts>
               <ts e="T454" id="Seg_524" n="e" s="T453">nüjliel?" </ts>
               <ts e="T455" id="Seg_526" n="e" s="T454">"Da, </ts>
               <ts e="T456" id="Seg_528" n="e" s="T455">na </ts>
               <ts e="T457" id="Seg_530" n="e" s="T456">tăn </ts>
               <ts e="T458" id="Seg_532" n="e" s="T457">aktʼal!" </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T372" id="Seg_533" s="T366">PKZ_196X_BlacksmithAndMerchant_flk.001 (001)</ta>
            <ta e="T375" id="Seg_534" s="T372">PKZ_196X_BlacksmithAndMerchant_flk.002 (002)</ta>
            <ta e="T380" id="Seg_535" s="T375">PKZ_196X_BlacksmithAndMerchant_flk.003 (003)</ta>
            <ta e="T391" id="Seg_536" s="T380">PKZ_196X_BlacksmithAndMerchant_flk.004 (004)</ta>
            <ta e="T401" id="Seg_537" s="T391">PKZ_196X_BlacksmithAndMerchant_flk.005 (005)</ta>
            <ta e="T406" id="Seg_538" s="T401">PKZ_196X_BlacksmithAndMerchant_flk.006 (006)</ta>
            <ta e="T414" id="Seg_539" s="T406">PKZ_196X_BlacksmithAndMerchant_flk.007 (007)</ta>
            <ta e="T422" id="Seg_540" s="T414">PKZ_196X_BlacksmithAndMerchant_flk.008 (009)</ta>
            <ta e="T432" id="Seg_541" s="T422">PKZ_196X_BlacksmithAndMerchant_flk.009 (010)</ta>
            <ta e="T437" id="Seg_542" s="T432">PKZ_196X_BlacksmithAndMerchant_flk.010 (011)</ta>
            <ta e="T444" id="Seg_543" s="T437">PKZ_196X_BlacksmithAndMerchant_flk.011 (012)</ta>
            <ta e="T454" id="Seg_544" s="T444">PKZ_196X_BlacksmithAndMerchant_flk.012 (013)</ta>
            <ta e="T458" id="Seg_545" s="T454">PKZ_196X_BlacksmithAndMerchant_flk.013 (014)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T372" id="Seg_546" s="T366">Amnobiʔi šide kuza, kupʼes i kuznʼes. </ta>
            <ta e="T375" id="Seg_547" s="T372">(Kuznʼesən) ĭmbidə nagobi. </ta>
            <ta e="T380" id="Seg_548" s="T375">A dĭn ugaːndə aktʼa iʔgö. </ta>
            <ta e="T391" id="Seg_549" s="T380">Dĭ uge mandolaʔbə, šində bɨ ej tojirbi, ej kunollaʔbə, ej amnia. </ta>
            <ta e="T401" id="Seg_550" s="T391">A dĭ (ka-) kalləj (dĭ) kuznʼitsanə, dĭ togonorlaʔbə, nüjnə nüjleʔbə. </ta>
            <ta e="T406" id="Seg_551" s="T401">Maːndə šonəga, ĭmbi-nʼibudʼ dʼăbaktərlaʔbə, šaːmnaʔbə. </ta>
            <ta e="T414" id="Seg_552" s="T406">Dĭgəttə dĭ mandolaʔbə: "Ĭmbi tăn üge nüjnə nüjliel?" </ta>
            <ta e="T422" id="Seg_553" s="T414">"Da măn togonoriam, sĭjdə bar (vesʼo-) vesʼolɨj molaʔbə". </ta>
            <ta e="T432" id="Seg_554" s="T422">Dĭgəttə dĭ ibi bostə aktʼa, kambi da baruʔluʔpi dĭn (kuznʼitsanə). </ta>
            <ta e="T437" id="Seg_555" s="T432">Dĭ ibi bar, davaj măndərzittə. </ta>
            <ta e="T444" id="Seg_556" s="T437">Dĭgəttə ej molia togonorzittə, ej molia togonorzittə. </ta>
            <ta e="T454" id="Seg_557" s="T444">Da kambi dĭ kupʼes:" Ĭmbi ej togonorial, nüjnə ej nüjliel?" </ta>
            <ta e="T458" id="Seg_558" s="T454">"Da, na tăn aktʼal!" </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T367" id="Seg_559" s="T366">amno-bi-ʔi</ta>
            <ta e="T368" id="Seg_560" s="T367">šide</ta>
            <ta e="T369" id="Seg_561" s="T368">kuza</ta>
            <ta e="T370" id="Seg_562" s="T369">kupʼes</ta>
            <ta e="T371" id="Seg_563" s="T370">i</ta>
            <ta e="T372" id="Seg_564" s="T371">kuznʼes</ta>
            <ta e="T373" id="Seg_565" s="T372">kuznʼes-ə-n</ta>
            <ta e="T374" id="Seg_566" s="T373">ĭmbi=də</ta>
            <ta e="T375" id="Seg_567" s="T374">nago-bi</ta>
            <ta e="T376" id="Seg_568" s="T375">a</ta>
            <ta e="T377" id="Seg_569" s="T376">dĭ-n</ta>
            <ta e="T378" id="Seg_570" s="T377">ugaːndə</ta>
            <ta e="T379" id="Seg_571" s="T378">aktʼa</ta>
            <ta e="T380" id="Seg_572" s="T379">iʔgö</ta>
            <ta e="T381" id="Seg_573" s="T380">dĭ</ta>
            <ta e="T382" id="Seg_574" s="T381">uge</ta>
            <ta e="T383" id="Seg_575" s="T382">mando-laʔbə</ta>
            <ta e="T384" id="Seg_576" s="T383">šində</ta>
            <ta e="T385" id="Seg_577" s="T384">bɨ</ta>
            <ta e="T386" id="Seg_578" s="T385">ej</ta>
            <ta e="T387" id="Seg_579" s="T386">tojir-bi</ta>
            <ta e="T388" id="Seg_580" s="T387">ej</ta>
            <ta e="T389" id="Seg_581" s="T388">kunol-laʔbə</ta>
            <ta e="T390" id="Seg_582" s="T389">ej</ta>
            <ta e="T391" id="Seg_583" s="T390">am-nia</ta>
            <ta e="T392" id="Seg_584" s="T391">a</ta>
            <ta e="T393" id="Seg_585" s="T392">dĭ</ta>
            <ta e="T395" id="Seg_586" s="T394">kal-lə-j</ta>
            <ta e="T396" id="Seg_587" s="T395">dĭ</ta>
            <ta e="T397" id="Seg_588" s="T396">kuznʼisa-nə</ta>
            <ta e="T398" id="Seg_589" s="T397">dĭ</ta>
            <ta e="T399" id="Seg_590" s="T398">togonor-laʔbə</ta>
            <ta e="T400" id="Seg_591" s="T399">nüjnə</ta>
            <ta e="T401" id="Seg_592" s="T400">nüj-leʔbə</ta>
            <ta e="T402" id="Seg_593" s="T401">ma-ndə</ta>
            <ta e="T403" id="Seg_594" s="T402">šonə-ga</ta>
            <ta e="T404" id="Seg_595" s="T403">ĭmbi=nʼibudʼ</ta>
            <ta e="T405" id="Seg_596" s="T404">dʼăbaktər-laʔbə</ta>
            <ta e="T406" id="Seg_597" s="T405">šaːm-naʔbə</ta>
            <ta e="T407" id="Seg_598" s="T406">dĭgəttə</ta>
            <ta e="T408" id="Seg_599" s="T407">dĭ</ta>
            <ta e="T409" id="Seg_600" s="T408">mando-laʔbə</ta>
            <ta e="T410" id="Seg_601" s="T409">ĭmbi</ta>
            <ta e="T411" id="Seg_602" s="T410">tăn</ta>
            <ta e="T412" id="Seg_603" s="T411">üge</ta>
            <ta e="T413" id="Seg_604" s="T412">nüjnə</ta>
            <ta e="T414" id="Seg_605" s="T413">nüj-lie-l</ta>
            <ta e="T415" id="Seg_606" s="T414">da</ta>
            <ta e="T416" id="Seg_607" s="T415">măn</ta>
            <ta e="T417" id="Seg_608" s="T416">togonor-ia-m</ta>
            <ta e="T418" id="Seg_609" s="T417">sĭj-də</ta>
            <ta e="T419" id="Seg_610" s="T418">bar</ta>
            <ta e="T421" id="Seg_611" s="T420">vesʼolɨj</ta>
            <ta e="T422" id="Seg_612" s="T421">mo-laʔbə</ta>
            <ta e="T423" id="Seg_613" s="T422">dĭgəttə</ta>
            <ta e="T424" id="Seg_614" s="T423">dĭ</ta>
            <ta e="T425" id="Seg_615" s="T424">i-bi</ta>
            <ta e="T426" id="Seg_616" s="T425">bos-tə</ta>
            <ta e="T427" id="Seg_617" s="T426">aktʼa</ta>
            <ta e="T428" id="Seg_618" s="T427">kam-bi</ta>
            <ta e="T429" id="Seg_619" s="T428">da</ta>
            <ta e="T430" id="Seg_620" s="T429">baruʔ-luʔ-pi</ta>
            <ta e="T431" id="Seg_621" s="T430">dĭn</ta>
            <ta e="T432" id="Seg_622" s="T431">kuznʼitsa-nə</ta>
            <ta e="T433" id="Seg_623" s="T432">dĭ</ta>
            <ta e="T434" id="Seg_624" s="T433">i-bi</ta>
            <ta e="T435" id="Seg_625" s="T434">bar</ta>
            <ta e="T436" id="Seg_626" s="T435">davaj</ta>
            <ta e="T437" id="Seg_627" s="T436">măndə-r-zittə</ta>
            <ta e="T438" id="Seg_628" s="T437">dĭgəttə</ta>
            <ta e="T439" id="Seg_629" s="T438">ej</ta>
            <ta e="T440" id="Seg_630" s="T439">mo-lia</ta>
            <ta e="T441" id="Seg_631" s="T440">togonor-zittə</ta>
            <ta e="T442" id="Seg_632" s="T441">ej</ta>
            <ta e="T443" id="Seg_633" s="T442">mo-lia</ta>
            <ta e="T444" id="Seg_634" s="T443">togonor-zittə</ta>
            <ta e="T445" id="Seg_635" s="T444">da</ta>
            <ta e="T446" id="Seg_636" s="T445">kam-bi</ta>
            <ta e="T447" id="Seg_637" s="T446">dĭ</ta>
            <ta e="T448" id="Seg_638" s="T447">kupʼes</ta>
            <ta e="T449" id="Seg_639" s="T448">ĭmbi</ta>
            <ta e="T450" id="Seg_640" s="T449">ej</ta>
            <ta e="T451" id="Seg_641" s="T450">togonor-ia-l</ta>
            <ta e="T452" id="Seg_642" s="T451">nüjnə</ta>
            <ta e="T453" id="Seg_643" s="T452">ej</ta>
            <ta e="T454" id="Seg_644" s="T453">nüj-lie-l</ta>
            <ta e="T455" id="Seg_645" s="T454">da</ta>
            <ta e="T456" id="Seg_646" s="T455">na</ta>
            <ta e="T457" id="Seg_647" s="T456">tăn</ta>
            <ta e="T458" id="Seg_648" s="T457">aktʼa-l</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T367" id="Seg_649" s="T366">amno-bi-jəʔ</ta>
            <ta e="T368" id="Seg_650" s="T367">šide</ta>
            <ta e="T369" id="Seg_651" s="T368">kuza</ta>
            <ta e="T370" id="Seg_652" s="T369">kupʼes</ta>
            <ta e="T371" id="Seg_653" s="T370">i</ta>
            <ta e="T372" id="Seg_654" s="T371">kuznʼes</ta>
            <ta e="T373" id="Seg_655" s="T372">kuznʼes-ə-n</ta>
            <ta e="T374" id="Seg_656" s="T373">ĭmbi=də</ta>
            <ta e="T375" id="Seg_657" s="T374">naga-bi</ta>
            <ta e="T376" id="Seg_658" s="T375">a</ta>
            <ta e="T377" id="Seg_659" s="T376">dĭ-n</ta>
            <ta e="T378" id="Seg_660" s="T377">ugaːndə</ta>
            <ta e="T379" id="Seg_661" s="T378">aktʼa</ta>
            <ta e="T380" id="Seg_662" s="T379">iʔgö</ta>
            <ta e="T381" id="Seg_663" s="T380">dĭ</ta>
            <ta e="T382" id="Seg_664" s="T381">üge</ta>
            <ta e="T383" id="Seg_665" s="T382">măndo-laʔbə</ta>
            <ta e="T384" id="Seg_666" s="T383">šində</ta>
            <ta e="T385" id="Seg_667" s="T384">bɨ</ta>
            <ta e="T386" id="Seg_668" s="T385">ej</ta>
            <ta e="T387" id="Seg_669" s="T386">tojar-bi</ta>
            <ta e="T388" id="Seg_670" s="T387">ej</ta>
            <ta e="T389" id="Seg_671" s="T388">kunol-laʔbə</ta>
            <ta e="T390" id="Seg_672" s="T389">ej</ta>
            <ta e="T391" id="Seg_673" s="T390">am-liA</ta>
            <ta e="T392" id="Seg_674" s="T391">a</ta>
            <ta e="T393" id="Seg_675" s="T392">dĭ</ta>
            <ta e="T395" id="Seg_676" s="T394">kan-lV-j</ta>
            <ta e="T396" id="Seg_677" s="T395">dĭ</ta>
            <ta e="T397" id="Seg_678" s="T396">kuznʼitsa-Tə</ta>
            <ta e="T398" id="Seg_679" s="T397">dĭ</ta>
            <ta e="T399" id="Seg_680" s="T398">togonər-laʔbə</ta>
            <ta e="T400" id="Seg_681" s="T399">nüjnə</ta>
            <ta e="T401" id="Seg_682" s="T400">nüj-laʔbə</ta>
            <ta e="T402" id="Seg_683" s="T401">maʔ-gəndə</ta>
            <ta e="T403" id="Seg_684" s="T402">šonə-gA</ta>
            <ta e="T404" id="Seg_685" s="T403">ĭmbi=nʼibudʼ</ta>
            <ta e="T405" id="Seg_686" s="T404">tʼăbaktər-laʔbə</ta>
            <ta e="T406" id="Seg_687" s="T405">šʼaːm-laʔbə</ta>
            <ta e="T407" id="Seg_688" s="T406">dĭgəttə</ta>
            <ta e="T408" id="Seg_689" s="T407">dĭ</ta>
            <ta e="T409" id="Seg_690" s="T408">măndo-laʔbə</ta>
            <ta e="T410" id="Seg_691" s="T409">ĭmbi</ta>
            <ta e="T411" id="Seg_692" s="T410">tăn</ta>
            <ta e="T412" id="Seg_693" s="T411">üge</ta>
            <ta e="T413" id="Seg_694" s="T412">nüjnə</ta>
            <ta e="T414" id="Seg_695" s="T413">nüj-liA-l</ta>
            <ta e="T415" id="Seg_696" s="T414">da</ta>
            <ta e="T416" id="Seg_697" s="T415">măn</ta>
            <ta e="T417" id="Seg_698" s="T416">togonər-liA-m</ta>
            <ta e="T418" id="Seg_699" s="T417">sĭj-də</ta>
            <ta e="T419" id="Seg_700" s="T418">bar</ta>
            <ta e="T421" id="Seg_701" s="T420">vesʼolɨj</ta>
            <ta e="T422" id="Seg_702" s="T421">mo-laʔbə</ta>
            <ta e="T423" id="Seg_703" s="T422">dĭgəttə</ta>
            <ta e="T424" id="Seg_704" s="T423">dĭ</ta>
            <ta e="T425" id="Seg_705" s="T424">i-bi</ta>
            <ta e="T426" id="Seg_706" s="T425">bos-də</ta>
            <ta e="T427" id="Seg_707" s="T426">aktʼa</ta>
            <ta e="T428" id="Seg_708" s="T427">kan-bi</ta>
            <ta e="T429" id="Seg_709" s="T428">da</ta>
            <ta e="T430" id="Seg_710" s="T429">barəʔ-luʔbdə-bi</ta>
            <ta e="T431" id="Seg_711" s="T430">dĭn</ta>
            <ta e="T432" id="Seg_712" s="T431">kuznʼitsa-Tə</ta>
            <ta e="T433" id="Seg_713" s="T432">dĭ</ta>
            <ta e="T434" id="Seg_714" s="T433">i-bi</ta>
            <ta e="T435" id="Seg_715" s="T434">bar</ta>
            <ta e="T436" id="Seg_716" s="T435">davaj</ta>
            <ta e="T437" id="Seg_717" s="T436">măndo-r-zittə</ta>
            <ta e="T438" id="Seg_718" s="T437">dĭgəttə</ta>
            <ta e="T439" id="Seg_719" s="T438">ej</ta>
            <ta e="T440" id="Seg_720" s="T439">mo-liA</ta>
            <ta e="T441" id="Seg_721" s="T440">togonər-zittə</ta>
            <ta e="T442" id="Seg_722" s="T441">ej</ta>
            <ta e="T443" id="Seg_723" s="T442">mo-liA</ta>
            <ta e="T444" id="Seg_724" s="T443">togonər-zittə</ta>
            <ta e="T445" id="Seg_725" s="T444">da</ta>
            <ta e="T446" id="Seg_726" s="T445">kan-bi</ta>
            <ta e="T447" id="Seg_727" s="T446">dĭ</ta>
            <ta e="T448" id="Seg_728" s="T447">kupʼes</ta>
            <ta e="T449" id="Seg_729" s="T448">ĭmbi</ta>
            <ta e="T450" id="Seg_730" s="T449">ej</ta>
            <ta e="T451" id="Seg_731" s="T450">togonər-liA-l</ta>
            <ta e="T452" id="Seg_732" s="T451">nüjnə</ta>
            <ta e="T453" id="Seg_733" s="T452">ej</ta>
            <ta e="T454" id="Seg_734" s="T453">nüj-liA-l</ta>
            <ta e="T455" id="Seg_735" s="T454">da</ta>
            <ta e="T456" id="Seg_736" s="T455">na</ta>
            <ta e="T457" id="Seg_737" s="T456">tăn</ta>
            <ta e="T458" id="Seg_738" s="T457">aktʼa-l</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T367" id="Seg_739" s="T366">live-PST-3PL</ta>
            <ta e="T368" id="Seg_740" s="T367">two.[NOM.SG]</ta>
            <ta e="T369" id="Seg_741" s="T368">man.[NOM.SG]</ta>
            <ta e="T370" id="Seg_742" s="T369">merchant.[NOM.SG]</ta>
            <ta e="T371" id="Seg_743" s="T370">and</ta>
            <ta e="T372" id="Seg_744" s="T371">blacksmith.[NOM.SG]</ta>
            <ta e="T373" id="Seg_745" s="T372">blacksmith-EP-GEN</ta>
            <ta e="T374" id="Seg_746" s="T373">what.[NOM.SG]=INDEF</ta>
            <ta e="T375" id="Seg_747" s="T374">NEG.EX-PST.[3SG]</ta>
            <ta e="T376" id="Seg_748" s="T375">and</ta>
            <ta e="T377" id="Seg_749" s="T376">this-GEN</ta>
            <ta e="T378" id="Seg_750" s="T377">very</ta>
            <ta e="T379" id="Seg_751" s="T378">money.[NOM.SG]</ta>
            <ta e="T380" id="Seg_752" s="T379">many</ta>
            <ta e="T381" id="Seg_753" s="T380">this.[NOM.SG]</ta>
            <ta e="T382" id="Seg_754" s="T381">always</ta>
            <ta e="T383" id="Seg_755" s="T382">look-DUR.[3SG]</ta>
            <ta e="T384" id="Seg_756" s="T383">who.[NOM.SG]</ta>
            <ta e="T385" id="Seg_757" s="T384">IRREAL</ta>
            <ta e="T386" id="Seg_758" s="T385">NEG</ta>
            <ta e="T387" id="Seg_759" s="T386">steal-PST.[3SG]</ta>
            <ta e="T388" id="Seg_760" s="T387">NEG</ta>
            <ta e="T389" id="Seg_761" s="T388">sleep-DUR.[3SG]</ta>
            <ta e="T390" id="Seg_762" s="T389">NEG</ta>
            <ta e="T391" id="Seg_763" s="T390">eat-PRS.[3SG]</ta>
            <ta e="T392" id="Seg_764" s="T391">and</ta>
            <ta e="T393" id="Seg_765" s="T392">this.[NOM.SG]</ta>
            <ta e="T395" id="Seg_766" s="T394">go-FUT-3SG</ta>
            <ta e="T396" id="Seg_767" s="T395">this.[NOM.SG]</ta>
            <ta e="T397" id="Seg_768" s="T396">smithy-LAT</ta>
            <ta e="T398" id="Seg_769" s="T397">this.[NOM.SG]</ta>
            <ta e="T399" id="Seg_770" s="T398">work-DUR.[3SG]</ta>
            <ta e="T400" id="Seg_771" s="T399">song.[NOM.SG]</ta>
            <ta e="T401" id="Seg_772" s="T400">sing-DUR.[3SG]</ta>
            <ta e="T402" id="Seg_773" s="T401">tent-LAT/LOC.3SG</ta>
            <ta e="T403" id="Seg_774" s="T402">come-PRS.[3SG]</ta>
            <ta e="T404" id="Seg_775" s="T403">what=INDEF</ta>
            <ta e="T405" id="Seg_776" s="T404">speak-DUR.[3SG]</ta>
            <ta e="T406" id="Seg_777" s="T405">lie-DUR.[3SG]</ta>
            <ta e="T407" id="Seg_778" s="T406">then</ta>
            <ta e="T408" id="Seg_779" s="T407">this.[NOM.SG]</ta>
            <ta e="T409" id="Seg_780" s="T408">look-DUR.[3SG]</ta>
            <ta e="T410" id="Seg_781" s="T409">what.[NOM.SG]</ta>
            <ta e="T411" id="Seg_782" s="T410">you.NOM</ta>
            <ta e="T412" id="Seg_783" s="T411">always</ta>
            <ta e="T413" id="Seg_784" s="T412">song.[NOM.SG]</ta>
            <ta e="T414" id="Seg_785" s="T413">sing-PRS-2SG</ta>
            <ta e="T415" id="Seg_786" s="T414">and</ta>
            <ta e="T416" id="Seg_787" s="T415">I.NOM</ta>
            <ta e="T417" id="Seg_788" s="T416">work-PRS-1SG</ta>
            <ta e="T418" id="Seg_789" s="T417">heart-NOM/GEN/ACC.3SG</ta>
            <ta e="T419" id="Seg_790" s="T418">PTCL</ta>
            <ta e="T421" id="Seg_791" s="T420">cheerful</ta>
            <ta e="T422" id="Seg_792" s="T421">become-DUR.[3SG]</ta>
            <ta e="T423" id="Seg_793" s="T422">then</ta>
            <ta e="T424" id="Seg_794" s="T423">this.[NOM.SG]</ta>
            <ta e="T425" id="Seg_795" s="T424">take-PST.[3SG]</ta>
            <ta e="T426" id="Seg_796" s="T425">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T427" id="Seg_797" s="T426">money.[NOM.SG]</ta>
            <ta e="T428" id="Seg_798" s="T427">go-PST.[3SG]</ta>
            <ta e="T429" id="Seg_799" s="T428">and</ta>
            <ta e="T430" id="Seg_800" s="T429">leave-MOM-PST.[3SG]</ta>
            <ta e="T431" id="Seg_801" s="T430">there</ta>
            <ta e="T432" id="Seg_802" s="T431">smithy-LAT</ta>
            <ta e="T433" id="Seg_803" s="T432">this.[NOM.SG]</ta>
            <ta e="T434" id="Seg_804" s="T433">take-PST.[3SG]</ta>
            <ta e="T435" id="Seg_805" s="T434">PTCL</ta>
            <ta e="T436" id="Seg_806" s="T435">INCH</ta>
            <ta e="T437" id="Seg_807" s="T436">look-FRQ-INF.LAT</ta>
            <ta e="T438" id="Seg_808" s="T437">then</ta>
            <ta e="T439" id="Seg_809" s="T438">NEG</ta>
            <ta e="T440" id="Seg_810" s="T439">can-PRS.[3SG]</ta>
            <ta e="T441" id="Seg_811" s="T440">work-INF.LAT</ta>
            <ta e="T442" id="Seg_812" s="T441">NEG</ta>
            <ta e="T443" id="Seg_813" s="T442">can-PRS.[3SG]</ta>
            <ta e="T444" id="Seg_814" s="T443">work-INF.LAT</ta>
            <ta e="T445" id="Seg_815" s="T444">and</ta>
            <ta e="T446" id="Seg_816" s="T445">go-PST.[3SG]</ta>
            <ta e="T447" id="Seg_817" s="T446">this.[NOM.SG]</ta>
            <ta e="T448" id="Seg_818" s="T447">merchant.[NOM.SG]</ta>
            <ta e="T449" id="Seg_819" s="T448">what.[NOM.SG]</ta>
            <ta e="T450" id="Seg_820" s="T449">NEG</ta>
            <ta e="T451" id="Seg_821" s="T450">work-PRS-2SG</ta>
            <ta e="T452" id="Seg_822" s="T451">song.[NOM.SG]</ta>
            <ta e="T453" id="Seg_823" s="T452">NEG</ta>
            <ta e="T454" id="Seg_824" s="T453">sing-PRS-2SG</ta>
            <ta e="T455" id="Seg_825" s="T454">and</ta>
            <ta e="T456" id="Seg_826" s="T455">take.it</ta>
            <ta e="T457" id="Seg_827" s="T456">you.GEN</ta>
            <ta e="T458" id="Seg_828" s="T457">money-NOM/GEN/ACC.2SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T367" id="Seg_829" s="T366">жить-PST-3PL</ta>
            <ta e="T368" id="Seg_830" s="T367">два.[NOM.SG]</ta>
            <ta e="T369" id="Seg_831" s="T368">мужчина.[NOM.SG]</ta>
            <ta e="T370" id="Seg_832" s="T369">купец.[NOM.SG]</ta>
            <ta e="T371" id="Seg_833" s="T370">и</ta>
            <ta e="T372" id="Seg_834" s="T371">кузнец.[NOM.SG]</ta>
            <ta e="T373" id="Seg_835" s="T372">кузнец-EP-GEN</ta>
            <ta e="T374" id="Seg_836" s="T373">что.[NOM.SG]=INDEF</ta>
            <ta e="T375" id="Seg_837" s="T374">NEG.EX-PST.[3SG]</ta>
            <ta e="T376" id="Seg_838" s="T375">а</ta>
            <ta e="T377" id="Seg_839" s="T376">этот-GEN</ta>
            <ta e="T378" id="Seg_840" s="T377">очень</ta>
            <ta e="T379" id="Seg_841" s="T378">деньги.[NOM.SG]</ta>
            <ta e="T380" id="Seg_842" s="T379">много</ta>
            <ta e="T381" id="Seg_843" s="T380">этот.[NOM.SG]</ta>
            <ta e="T382" id="Seg_844" s="T381">всегда</ta>
            <ta e="T383" id="Seg_845" s="T382">смотреть-DUR.[3SG]</ta>
            <ta e="T384" id="Seg_846" s="T383">кто.[NOM.SG]</ta>
            <ta e="T385" id="Seg_847" s="T384">IRREAL</ta>
            <ta e="T386" id="Seg_848" s="T385">NEG</ta>
            <ta e="T387" id="Seg_849" s="T386">украсть-PST.[3SG]</ta>
            <ta e="T388" id="Seg_850" s="T387">NEG</ta>
            <ta e="T389" id="Seg_851" s="T388">спать-DUR.[3SG]</ta>
            <ta e="T390" id="Seg_852" s="T389">NEG</ta>
            <ta e="T391" id="Seg_853" s="T390">съесть-PRS.[3SG]</ta>
            <ta e="T392" id="Seg_854" s="T391">а</ta>
            <ta e="T393" id="Seg_855" s="T392">этот.[NOM.SG]</ta>
            <ta e="T395" id="Seg_856" s="T394">пойти-FUT-3SG</ta>
            <ta e="T396" id="Seg_857" s="T395">этот.[NOM.SG]</ta>
            <ta e="T397" id="Seg_858" s="T396">кузница-LAT</ta>
            <ta e="T398" id="Seg_859" s="T397">этот.[NOM.SG]</ta>
            <ta e="T399" id="Seg_860" s="T398">работать-DUR.[3SG]</ta>
            <ta e="T400" id="Seg_861" s="T399">песня.[NOM.SG]</ta>
            <ta e="T401" id="Seg_862" s="T400">петь-DUR.[3SG]</ta>
            <ta e="T402" id="Seg_863" s="T401">чум-LAT/LOC.3SG</ta>
            <ta e="T403" id="Seg_864" s="T402">прийти-PRS.[3SG]</ta>
            <ta e="T404" id="Seg_865" s="T403">что=INDEF</ta>
            <ta e="T405" id="Seg_866" s="T404">говорить-DUR.[3SG]</ta>
            <ta e="T406" id="Seg_867" s="T405">лгать-DUR.[3SG]</ta>
            <ta e="T407" id="Seg_868" s="T406">тогда</ta>
            <ta e="T408" id="Seg_869" s="T407">этот.[NOM.SG]</ta>
            <ta e="T409" id="Seg_870" s="T408">смотреть-DUR.[3SG]</ta>
            <ta e="T410" id="Seg_871" s="T409">что.[NOM.SG]</ta>
            <ta e="T411" id="Seg_872" s="T410">ты.NOM</ta>
            <ta e="T412" id="Seg_873" s="T411">всегда</ta>
            <ta e="T413" id="Seg_874" s="T412">песня.[NOM.SG]</ta>
            <ta e="T414" id="Seg_875" s="T413">петь-PRS-2SG</ta>
            <ta e="T415" id="Seg_876" s="T414">и</ta>
            <ta e="T416" id="Seg_877" s="T415">я.NOM</ta>
            <ta e="T417" id="Seg_878" s="T416">работать-PRS-1SG</ta>
            <ta e="T418" id="Seg_879" s="T417">сердце-NOM/GEN/ACC.3SG</ta>
            <ta e="T419" id="Seg_880" s="T418">PTCL</ta>
            <ta e="T421" id="Seg_881" s="T420">веселый</ta>
            <ta e="T422" id="Seg_882" s="T421">стать-DUR.[3SG]</ta>
            <ta e="T423" id="Seg_883" s="T422">тогда</ta>
            <ta e="T424" id="Seg_884" s="T423">этот.[NOM.SG]</ta>
            <ta e="T425" id="Seg_885" s="T424">взять-PST.[3SG]</ta>
            <ta e="T426" id="Seg_886" s="T425">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T427" id="Seg_887" s="T426">деньги.[NOM.SG]</ta>
            <ta e="T428" id="Seg_888" s="T427">пойти-PST.[3SG]</ta>
            <ta e="T429" id="Seg_889" s="T428">и</ta>
            <ta e="T430" id="Seg_890" s="T429">оставить-MOM-PST.[3SG]</ta>
            <ta e="T431" id="Seg_891" s="T430">там</ta>
            <ta e="T432" id="Seg_892" s="T431">кузница-LAT</ta>
            <ta e="T433" id="Seg_893" s="T432">этот.[NOM.SG]</ta>
            <ta e="T434" id="Seg_894" s="T433">взять-PST.[3SG]</ta>
            <ta e="T435" id="Seg_895" s="T434">PTCL</ta>
            <ta e="T436" id="Seg_896" s="T435">INCH</ta>
            <ta e="T437" id="Seg_897" s="T436">смотреть-FRQ-INF.LAT</ta>
            <ta e="T438" id="Seg_898" s="T437">тогда</ta>
            <ta e="T439" id="Seg_899" s="T438">NEG</ta>
            <ta e="T440" id="Seg_900" s="T439">мочь-PRS.[3SG]</ta>
            <ta e="T441" id="Seg_901" s="T440">работать-INF.LAT</ta>
            <ta e="T442" id="Seg_902" s="T441">NEG</ta>
            <ta e="T443" id="Seg_903" s="T442">мочь-PRS.[3SG]</ta>
            <ta e="T444" id="Seg_904" s="T443">работать-INF.LAT</ta>
            <ta e="T445" id="Seg_905" s="T444">и</ta>
            <ta e="T446" id="Seg_906" s="T445">пойти-PST.[3SG]</ta>
            <ta e="T447" id="Seg_907" s="T446">этот.[NOM.SG]</ta>
            <ta e="T448" id="Seg_908" s="T447">купец.[NOM.SG]</ta>
            <ta e="T449" id="Seg_909" s="T448">что.[NOM.SG]</ta>
            <ta e="T450" id="Seg_910" s="T449">NEG</ta>
            <ta e="T451" id="Seg_911" s="T450">работать-PRS-2SG</ta>
            <ta e="T452" id="Seg_912" s="T451">песня.[NOM.SG]</ta>
            <ta e="T453" id="Seg_913" s="T452">NEG</ta>
            <ta e="T454" id="Seg_914" s="T453">петь-PRS-2SG</ta>
            <ta e="T455" id="Seg_915" s="T454">и</ta>
            <ta e="T456" id="Seg_916" s="T455">возьми</ta>
            <ta e="T457" id="Seg_917" s="T456">ты.GEN</ta>
            <ta e="T458" id="Seg_918" s="T457">деньги-NOM/GEN/ACC.2SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T367" id="Seg_919" s="T366">v-v:tense-v:pn</ta>
            <ta e="T368" id="Seg_920" s="T367">num-n:case</ta>
            <ta e="T369" id="Seg_921" s="T368">n-n:case</ta>
            <ta e="T370" id="Seg_922" s="T369">n-n:case</ta>
            <ta e="T371" id="Seg_923" s="T370">conj</ta>
            <ta e="T372" id="Seg_924" s="T371">n-n:case</ta>
            <ta e="T373" id="Seg_925" s="T372">n-n:ins-n:case</ta>
            <ta e="T374" id="Seg_926" s="T373">que-n:case=ptcl</ta>
            <ta e="T375" id="Seg_927" s="T374">v-v:tense-v:pn</ta>
            <ta e="T376" id="Seg_928" s="T375">conj</ta>
            <ta e="T377" id="Seg_929" s="T376">dempro-n:case</ta>
            <ta e="T378" id="Seg_930" s="T377">adv</ta>
            <ta e="T379" id="Seg_931" s="T378">n-n:case</ta>
            <ta e="T380" id="Seg_932" s="T379">quant</ta>
            <ta e="T381" id="Seg_933" s="T380">dempro-n:case</ta>
            <ta e="T382" id="Seg_934" s="T381">adv</ta>
            <ta e="T383" id="Seg_935" s="T382">v-v&gt;v-v:pn</ta>
            <ta e="T384" id="Seg_936" s="T383">que</ta>
            <ta e="T385" id="Seg_937" s="T384">ptcl</ta>
            <ta e="T386" id="Seg_938" s="T385">ptcl</ta>
            <ta e="T387" id="Seg_939" s="T386">v-v:tense-v:pn</ta>
            <ta e="T388" id="Seg_940" s="T387">ptcl</ta>
            <ta e="T389" id="Seg_941" s="T388">v-v&gt;v-v:pn</ta>
            <ta e="T390" id="Seg_942" s="T389">ptcl</ta>
            <ta e="T391" id="Seg_943" s="T390">v-v:tense-v:pn</ta>
            <ta e="T392" id="Seg_944" s="T391">conj</ta>
            <ta e="T393" id="Seg_945" s="T392">dempro-n:case</ta>
            <ta e="T395" id="Seg_946" s="T394">v-v:tense-v:pn</ta>
            <ta e="T396" id="Seg_947" s="T395">dempro-n:case</ta>
            <ta e="T397" id="Seg_948" s="T396">n-n:case</ta>
            <ta e="T398" id="Seg_949" s="T397">dempro-n:case</ta>
            <ta e="T399" id="Seg_950" s="T398">v-v&gt;v-v:pn</ta>
            <ta e="T400" id="Seg_951" s="T399">n-n:case</ta>
            <ta e="T401" id="Seg_952" s="T400">v-v&gt;v-v:pn</ta>
            <ta e="T402" id="Seg_953" s="T401">n-n:case.poss</ta>
            <ta e="T403" id="Seg_954" s="T402">v-v:tense-v:pn</ta>
            <ta e="T404" id="Seg_955" s="T403">que=ptcl</ta>
            <ta e="T405" id="Seg_956" s="T404">v-v&gt;v-v:pn</ta>
            <ta e="T406" id="Seg_957" s="T405">v-v&gt;v-v:pn</ta>
            <ta e="T407" id="Seg_958" s="T406">adv</ta>
            <ta e="T408" id="Seg_959" s="T407">dempro-n:case</ta>
            <ta e="T409" id="Seg_960" s="T408">v-v&gt;v-v:pn</ta>
            <ta e="T410" id="Seg_961" s="T409">que-n:case</ta>
            <ta e="T411" id="Seg_962" s="T410">pers</ta>
            <ta e="T412" id="Seg_963" s="T411">adv</ta>
            <ta e="T413" id="Seg_964" s="T412">n-n:case</ta>
            <ta e="T414" id="Seg_965" s="T413">v-v:tense-v:pn</ta>
            <ta e="T415" id="Seg_966" s="T414">conj</ta>
            <ta e="T416" id="Seg_967" s="T415">pers</ta>
            <ta e="T417" id="Seg_968" s="T416">v-v:tense-v:pn</ta>
            <ta e="T418" id="Seg_969" s="T417">n-n:case.poss</ta>
            <ta e="T419" id="Seg_970" s="T418">ptcl</ta>
            <ta e="T421" id="Seg_971" s="T420">adj</ta>
            <ta e="T422" id="Seg_972" s="T421">v-v&gt;v-v:pn</ta>
            <ta e="T423" id="Seg_973" s="T422">adv</ta>
            <ta e="T424" id="Seg_974" s="T423">dempro-n:case</ta>
            <ta e="T425" id="Seg_975" s="T424">v-v:tense-v:pn</ta>
            <ta e="T426" id="Seg_976" s="T425">refl-n:case.poss</ta>
            <ta e="T427" id="Seg_977" s="T426">n-n:case</ta>
            <ta e="T428" id="Seg_978" s="T427">v-v:tense-v:pn</ta>
            <ta e="T429" id="Seg_979" s="T428">conj</ta>
            <ta e="T430" id="Seg_980" s="T429">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T431" id="Seg_981" s="T430">adv</ta>
            <ta e="T432" id="Seg_982" s="T431">n-n:case</ta>
            <ta e="T433" id="Seg_983" s="T432">dempro-n:case</ta>
            <ta e="T434" id="Seg_984" s="T433">v-v:tense-v:pn</ta>
            <ta e="T435" id="Seg_985" s="T434">ptcl</ta>
            <ta e="T436" id="Seg_986" s="T435">ptcl</ta>
            <ta e="T437" id="Seg_987" s="T436">v-v&gt;v-v:n.fin</ta>
            <ta e="T438" id="Seg_988" s="T437">adv</ta>
            <ta e="T439" id="Seg_989" s="T438">ptcl</ta>
            <ta e="T440" id="Seg_990" s="T439">v-v:tense-v:pn</ta>
            <ta e="T441" id="Seg_991" s="T440">v-v:n.fin</ta>
            <ta e="T442" id="Seg_992" s="T441">ptcl</ta>
            <ta e="T443" id="Seg_993" s="T442">v-v:tense-v:pn</ta>
            <ta e="T444" id="Seg_994" s="T443">v-v:n.fin</ta>
            <ta e="T445" id="Seg_995" s="T444">conj</ta>
            <ta e="T446" id="Seg_996" s="T445">v-v:tense-v:pn</ta>
            <ta e="T447" id="Seg_997" s="T446">dempro-n:case</ta>
            <ta e="T448" id="Seg_998" s="T447">n-n:case</ta>
            <ta e="T449" id="Seg_999" s="T448">que-n:case</ta>
            <ta e="T450" id="Seg_1000" s="T449">ptcl</ta>
            <ta e="T451" id="Seg_1001" s="T450">v-v:tense-v:pn</ta>
            <ta e="T452" id="Seg_1002" s="T451">n-n:case</ta>
            <ta e="T453" id="Seg_1003" s="T452">ptcl</ta>
            <ta e="T454" id="Seg_1004" s="T453">v-v:tense-v:pn</ta>
            <ta e="T455" id="Seg_1005" s="T454">conj</ta>
            <ta e="T456" id="Seg_1006" s="T455">ptcl</ta>
            <ta e="T457" id="Seg_1007" s="T456">pers</ta>
            <ta e="T458" id="Seg_1008" s="T457">n-n:case.poss</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T367" id="Seg_1009" s="T366">v</ta>
            <ta e="T368" id="Seg_1010" s="T367">num</ta>
            <ta e="T369" id="Seg_1011" s="T368">n</ta>
            <ta e="T370" id="Seg_1012" s="T369">n</ta>
            <ta e="T371" id="Seg_1013" s="T370">conj</ta>
            <ta e="T372" id="Seg_1014" s="T371">n</ta>
            <ta e="T373" id="Seg_1015" s="T372">n</ta>
            <ta e="T374" id="Seg_1016" s="T373">que</ta>
            <ta e="T375" id="Seg_1017" s="T374">v</ta>
            <ta e="T376" id="Seg_1018" s="T375">conj</ta>
            <ta e="T377" id="Seg_1019" s="T376">dempro</ta>
            <ta e="T378" id="Seg_1020" s="T377">adv</ta>
            <ta e="T379" id="Seg_1021" s="T378">n</ta>
            <ta e="T380" id="Seg_1022" s="T379">quant</ta>
            <ta e="T381" id="Seg_1023" s="T380">dempro</ta>
            <ta e="T382" id="Seg_1024" s="T381">adv</ta>
            <ta e="T383" id="Seg_1025" s="T382">v</ta>
            <ta e="T384" id="Seg_1026" s="T383">que</ta>
            <ta e="T385" id="Seg_1027" s="T384">ptcl</ta>
            <ta e="T386" id="Seg_1028" s="T385">ptcl</ta>
            <ta e="T387" id="Seg_1029" s="T386">v</ta>
            <ta e="T388" id="Seg_1030" s="T387">ptcl</ta>
            <ta e="T389" id="Seg_1031" s="T388">v</ta>
            <ta e="T390" id="Seg_1032" s="T389">ptcl</ta>
            <ta e="T391" id="Seg_1033" s="T390">v</ta>
            <ta e="T392" id="Seg_1034" s="T391">conj</ta>
            <ta e="T393" id="Seg_1035" s="T392">dempro</ta>
            <ta e="T395" id="Seg_1036" s="T394">v</ta>
            <ta e="T396" id="Seg_1037" s="T395">dempro</ta>
            <ta e="T397" id="Seg_1038" s="T396">n</ta>
            <ta e="T398" id="Seg_1039" s="T397">dempro</ta>
            <ta e="T399" id="Seg_1040" s="T398">v</ta>
            <ta e="T400" id="Seg_1041" s="T399">n</ta>
            <ta e="T401" id="Seg_1042" s="T400">v</ta>
            <ta e="T402" id="Seg_1043" s="T401">n</ta>
            <ta e="T403" id="Seg_1044" s="T402">v</ta>
            <ta e="T404" id="Seg_1045" s="T403">que</ta>
            <ta e="T405" id="Seg_1046" s="T404">v</ta>
            <ta e="T406" id="Seg_1047" s="T405">v</ta>
            <ta e="T407" id="Seg_1048" s="T406">adv</ta>
            <ta e="T408" id="Seg_1049" s="T407">dempro</ta>
            <ta e="T409" id="Seg_1050" s="T408">v</ta>
            <ta e="T410" id="Seg_1051" s="T409">que</ta>
            <ta e="T411" id="Seg_1052" s="T410">pers</ta>
            <ta e="T412" id="Seg_1053" s="T411">adv</ta>
            <ta e="T413" id="Seg_1054" s="T412">n</ta>
            <ta e="T414" id="Seg_1055" s="T413">v</ta>
            <ta e="T415" id="Seg_1056" s="T414">conj</ta>
            <ta e="T416" id="Seg_1057" s="T415">pers</ta>
            <ta e="T417" id="Seg_1058" s="T416">v</ta>
            <ta e="T418" id="Seg_1059" s="T417">n</ta>
            <ta e="T419" id="Seg_1060" s="T418">ptcl</ta>
            <ta e="T421" id="Seg_1061" s="T420">adj</ta>
            <ta e="T422" id="Seg_1062" s="T421">v</ta>
            <ta e="T423" id="Seg_1063" s="T422">adv</ta>
            <ta e="T424" id="Seg_1064" s="T423">dempro</ta>
            <ta e="T425" id="Seg_1065" s="T424">v</ta>
            <ta e="T426" id="Seg_1066" s="T425">refl</ta>
            <ta e="T427" id="Seg_1067" s="T426">n</ta>
            <ta e="T428" id="Seg_1068" s="T427">v</ta>
            <ta e="T429" id="Seg_1069" s="T428">conj</ta>
            <ta e="T430" id="Seg_1070" s="T429">v</ta>
            <ta e="T431" id="Seg_1071" s="T430">adv</ta>
            <ta e="T432" id="Seg_1072" s="T431">n</ta>
            <ta e="T433" id="Seg_1073" s="T432">dempro</ta>
            <ta e="T434" id="Seg_1074" s="T433">v</ta>
            <ta e="T435" id="Seg_1075" s="T434">ptcl</ta>
            <ta e="T436" id="Seg_1076" s="T435">ptcl</ta>
            <ta e="T437" id="Seg_1077" s="T436">v</ta>
            <ta e="T438" id="Seg_1078" s="T437">adv</ta>
            <ta e="T439" id="Seg_1079" s="T438">ptcl</ta>
            <ta e="T440" id="Seg_1080" s="T439">v</ta>
            <ta e="T441" id="Seg_1081" s="T440">v</ta>
            <ta e="T442" id="Seg_1082" s="T441">ptcl</ta>
            <ta e="T443" id="Seg_1083" s="T442">v</ta>
            <ta e="T444" id="Seg_1084" s="T443">v</ta>
            <ta e="T445" id="Seg_1085" s="T444">conj</ta>
            <ta e="T446" id="Seg_1086" s="T445">v</ta>
            <ta e="T447" id="Seg_1087" s="T446">dempro</ta>
            <ta e="T448" id="Seg_1088" s="T447">n</ta>
            <ta e="T449" id="Seg_1089" s="T448">que</ta>
            <ta e="T450" id="Seg_1090" s="T449">ptcl</ta>
            <ta e="T451" id="Seg_1091" s="T450">v</ta>
            <ta e="T452" id="Seg_1092" s="T451">n</ta>
            <ta e="T453" id="Seg_1093" s="T452">ptcl</ta>
            <ta e="T454" id="Seg_1094" s="T453">v</ta>
            <ta e="T455" id="Seg_1095" s="T454">conj</ta>
            <ta e="T456" id="Seg_1096" s="T455">ptcl</ta>
            <ta e="T457" id="Seg_1097" s="T456">pers</ta>
            <ta e="T458" id="Seg_1098" s="T457">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T369" id="Seg_1099" s="T368">np.h:E</ta>
            <ta e="T373" id="Seg_1100" s="T372">np.h:Poss</ta>
            <ta e="T374" id="Seg_1101" s="T373">pro:Th</ta>
            <ta e="T377" id="Seg_1102" s="T376">pro.h:Poss</ta>
            <ta e="T379" id="Seg_1103" s="T378">np:Th</ta>
            <ta e="T381" id="Seg_1104" s="T380">pro.h:A</ta>
            <ta e="T384" id="Seg_1105" s="T383">pro.h:A</ta>
            <ta e="T393" id="Seg_1106" s="T392">pro.h:A</ta>
            <ta e="T397" id="Seg_1107" s="T396">np:G</ta>
            <ta e="T398" id="Seg_1108" s="T397">pro.h:A</ta>
            <ta e="T400" id="Seg_1109" s="T399">np:Th</ta>
            <ta e="T401" id="Seg_1110" s="T400">0.3.h:A</ta>
            <ta e="T402" id="Seg_1111" s="T401">np:G</ta>
            <ta e="T403" id="Seg_1112" s="T402">0.3.h:A</ta>
            <ta e="T404" id="Seg_1113" s="T403">pro:Th</ta>
            <ta e="T405" id="Seg_1114" s="T404">0.3.h:A</ta>
            <ta e="T406" id="Seg_1115" s="T405">0.3.h:A</ta>
            <ta e="T407" id="Seg_1116" s="T406">adv:Time</ta>
            <ta e="T408" id="Seg_1117" s="T407">pro.h:A</ta>
            <ta e="T411" id="Seg_1118" s="T410">pro.h:A</ta>
            <ta e="T413" id="Seg_1119" s="T412">np:Th</ta>
            <ta e="T416" id="Seg_1120" s="T415">pro.h:A</ta>
            <ta e="T418" id="Seg_1121" s="T417">np:Th 0.3.h:Poss</ta>
            <ta e="T423" id="Seg_1122" s="T422">adv:Time</ta>
            <ta e="T424" id="Seg_1123" s="T423">pro.h:A</ta>
            <ta e="T426" id="Seg_1124" s="T425">pro.h:Poss</ta>
            <ta e="T427" id="Seg_1125" s="T426">np:Th</ta>
            <ta e="T428" id="Seg_1126" s="T427">0.3.h:A</ta>
            <ta e="T430" id="Seg_1127" s="T429">0.3.h:A</ta>
            <ta e="T431" id="Seg_1128" s="T430">adv:L</ta>
            <ta e="T432" id="Seg_1129" s="T431">pro.h:B</ta>
            <ta e="T433" id="Seg_1130" s="T432">pro.h:A</ta>
            <ta e="T438" id="Seg_1131" s="T437">adv:Time</ta>
            <ta e="T440" id="Seg_1132" s="T439">0.3.h:A</ta>
            <ta e="T443" id="Seg_1133" s="T442">0.3.h:A</ta>
            <ta e="T448" id="Seg_1134" s="T447">np.h:A</ta>
            <ta e="T451" id="Seg_1135" s="T450">0.2.h:A</ta>
            <ta e="T452" id="Seg_1136" s="T451">np:Th</ta>
            <ta e="T454" id="Seg_1137" s="T453">0.2.h:A</ta>
            <ta e="T457" id="Seg_1138" s="T456">pro.h:Poss</ta>
            <ta e="T458" id="Seg_1139" s="T457">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T367" id="Seg_1140" s="T366">v:pred</ta>
            <ta e="T369" id="Seg_1141" s="T368">np.h:S</ta>
            <ta e="T374" id="Seg_1142" s="T373">pro:S</ta>
            <ta e="T375" id="Seg_1143" s="T374">v:pred</ta>
            <ta e="T379" id="Seg_1144" s="T378">np:S</ta>
            <ta e="T380" id="Seg_1145" s="T379">adj:pred</ta>
            <ta e="T381" id="Seg_1146" s="T380">pro.h:S</ta>
            <ta e="T383" id="Seg_1147" s="T382">v:pred</ta>
            <ta e="T384" id="Seg_1148" s="T383">pro.h:S</ta>
            <ta e="T386" id="Seg_1149" s="T385">ptcl.neg</ta>
            <ta e="T387" id="Seg_1150" s="T386">v:pred</ta>
            <ta e="T388" id="Seg_1151" s="T387">ptcl.neg</ta>
            <ta e="T389" id="Seg_1152" s="T388">v:pred 0.3.h:S</ta>
            <ta e="T390" id="Seg_1153" s="T389">ptcl.neg</ta>
            <ta e="T391" id="Seg_1154" s="T390">v:pred 0.3.h:S</ta>
            <ta e="T393" id="Seg_1155" s="T392">pro.h:S</ta>
            <ta e="T395" id="Seg_1156" s="T394">v:pred</ta>
            <ta e="T398" id="Seg_1157" s="T397">pro.h:S</ta>
            <ta e="T399" id="Seg_1158" s="T398">v:pred</ta>
            <ta e="T400" id="Seg_1159" s="T399">np:O</ta>
            <ta e="T401" id="Seg_1160" s="T400">v:pred 0.3.h:S</ta>
            <ta e="T403" id="Seg_1161" s="T402">v:pred 0.3.h:S</ta>
            <ta e="T404" id="Seg_1162" s="T403">pro:O</ta>
            <ta e="T405" id="Seg_1163" s="T404">v:pred 0.3.h:S</ta>
            <ta e="T406" id="Seg_1164" s="T405">v:pred 0.3.h:S</ta>
            <ta e="T408" id="Seg_1165" s="T407">pro.h:S</ta>
            <ta e="T409" id="Seg_1166" s="T408">v:pred</ta>
            <ta e="T411" id="Seg_1167" s="T410">pro.h:S</ta>
            <ta e="T413" id="Seg_1168" s="T412">np:O</ta>
            <ta e="T414" id="Seg_1169" s="T413">v:pred</ta>
            <ta e="T416" id="Seg_1170" s="T415">pro.h:S</ta>
            <ta e="T417" id="Seg_1171" s="T416">v:pred</ta>
            <ta e="T418" id="Seg_1172" s="T417">np:S</ta>
            <ta e="T421" id="Seg_1173" s="T420">adj:pred</ta>
            <ta e="T422" id="Seg_1174" s="T421">cop</ta>
            <ta e="T424" id="Seg_1175" s="T423">pro.h:S</ta>
            <ta e="T425" id="Seg_1176" s="T424">v:pred</ta>
            <ta e="T427" id="Seg_1177" s="T426">np:O</ta>
            <ta e="T428" id="Seg_1178" s="T427">v:pred 0.3.h:S</ta>
            <ta e="T430" id="Seg_1179" s="T429">v:pred 0.3.h:S</ta>
            <ta e="T433" id="Seg_1180" s="T432">pro.h:S</ta>
            <ta e="T434" id="Seg_1181" s="T433">v:pred</ta>
            <ta e="T436" id="Seg_1182" s="T435">ptcl:pred</ta>
            <ta e="T439" id="Seg_1183" s="T438">ptcl.neg</ta>
            <ta e="T440" id="Seg_1184" s="T439">v:pred 0.3.h:S</ta>
            <ta e="T442" id="Seg_1185" s="T441">ptcl.neg</ta>
            <ta e="T443" id="Seg_1186" s="T442">v:pred 0.3.h:S</ta>
            <ta e="T446" id="Seg_1187" s="T445">v:pred</ta>
            <ta e="T448" id="Seg_1188" s="T447">np.h:S</ta>
            <ta e="T450" id="Seg_1189" s="T449">ptcl.neg</ta>
            <ta e="T451" id="Seg_1190" s="T450">v:pred 0.2.h:S</ta>
            <ta e="T452" id="Seg_1191" s="T451">np:O</ta>
            <ta e="T453" id="Seg_1192" s="T452">ptcl.neg</ta>
            <ta e="T454" id="Seg_1193" s="T453">v:pred 0.2.h:S</ta>
            <ta e="T456" id="Seg_1194" s="T455">ptcl:pred</ta>
            <ta e="T458" id="Seg_1195" s="T457">np:O</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T370" id="Seg_1196" s="T369">RUS:cult</ta>
            <ta e="T371" id="Seg_1197" s="T370">RUS:gram</ta>
            <ta e="T372" id="Seg_1198" s="T371">RUS:cult</ta>
            <ta e="T373" id="Seg_1199" s="T372">RUS:cult</ta>
            <ta e="T374" id="Seg_1200" s="T373">TURK:gram(INDEF)</ta>
            <ta e="T376" id="Seg_1201" s="T375">RUS:gram</ta>
            <ta e="T385" id="Seg_1202" s="T384">RUS:gram</ta>
            <ta e="T392" id="Seg_1203" s="T391">RUS:gram</ta>
            <ta e="T397" id="Seg_1204" s="T396">RUS:cult</ta>
            <ta e="T404" id="Seg_1205" s="T403">RUS:gram(INDEF)</ta>
            <ta e="T405" id="Seg_1206" s="T404">%TURK:core</ta>
            <ta e="T415" id="Seg_1207" s="T414">RUS:gram</ta>
            <ta e="T419" id="Seg_1208" s="T418">TURK:disc</ta>
            <ta e="T421" id="Seg_1209" s="T420">RUS:core</ta>
            <ta e="T429" id="Seg_1210" s="T428">RUS:gram</ta>
            <ta e="T432" id="Seg_1211" s="T431">RUS:cult</ta>
            <ta e="T435" id="Seg_1212" s="T434">TURK:disc</ta>
            <ta e="T436" id="Seg_1213" s="T435">RUS:gram</ta>
            <ta e="T445" id="Seg_1214" s="T444">RUS:gram</ta>
            <ta e="T448" id="Seg_1215" s="T447">RUS:cult</ta>
            <ta e="T455" id="Seg_1216" s="T454">RUS:gram</ta>
            <ta e="T456" id="Seg_1217" s="T455">RUS:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T373" id="Seg_1218" s="T372">Csub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T387" id="Seg_1219" s="T382">RUS:calq</ta>
            <ta e="T410" id="Seg_1220" s="T409">RUS:calq</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T372" id="Seg_1221" s="T366">Жили два человека, купец и кузнец.</ta>
            <ta e="T375" id="Seg_1222" s="T372">У кузнеца ничего не было.</ta>
            <ta e="T380" id="Seg_1223" s="T375">А у этого [купца] денег очень много.</ta>
            <ta e="T391" id="Seg_1224" s="T380">Он всё время смотрит, как бы кто [их] не украл, не спит, не ест.</ta>
            <ta e="T401" id="Seg_1225" s="T391">А [кузнец] пойдёт в кузницу, работает, песни поёт.</ta>
            <ta e="T406" id="Seg_1226" s="T401">Домой приходит, говорит немного, [и спать] ложится.</ta>
            <ta e="T409" id="Seg_1227" s="T406">Потом он смотрит [на кузнеца]:</ta>
            <ta e="T414" id="Seg_1228" s="T409">«Что ты всегда песни поёшь?»</ta>
            <ta e="T422" id="Seg_1229" s="T414">«Да я работаю, на сердце весело становится».</ta>
            <ta e="T432" id="Seg_1230" s="T422">Потом он взял свои деньги, пошёл и оставил их там в кузнице.</ta>
            <ta e="T437" id="Seg_1231" s="T432">Он взял их и давай смотреть.</ta>
            <ta e="T444" id="Seg_1232" s="T437">И не может работать, не может работать.</ta>
            <ta e="T454" id="Seg_1233" s="T444">И пришёл этот купец: «Что не работаешь, песни не поёшь?»</ta>
            <ta e="T458" id="Seg_1234" s="T454">«Вот, держи свои деньги!»</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T372" id="Seg_1235" s="T366">Once there lived two men, a merchant and a blacksmith.</ta>
            <ta e="T375" id="Seg_1236" s="T372">The blacksmith did not have anything.</ta>
            <ta e="T380" id="Seg_1237" s="T375">But he [the merchant] had very much money.</ta>
            <ta e="T391" id="Seg_1238" s="T380">He is always looking that no one steals, does not sleep, does not eat.</ta>
            <ta e="T401" id="Seg_1239" s="T391">And [the blacksmith] goes to the smithy, works, sings a song.</ta>
            <ta e="T406" id="Seg_1240" s="T401">[Then] he comes home, he speaks a bit, [and] goes to bed.</ta>
            <ta e="T409" id="Seg_1241" s="T406">Then he looks [at the blacksmith]:</ta>
            <ta e="T414" id="Seg_1242" s="T409">"Why do you always sing a song?"</ta>
            <ta e="T422" id="Seg_1243" s="T414">"Because I work, [and] my heart becomes cheerful."</ta>
            <ta e="T432" id="Seg_1244" s="T422">Then he took his money, went and left it there at the forge.</ta>
            <ta e="T437" id="Seg_1245" s="T432">He took it, started to watch.</ta>
            <ta e="T444" id="Seg_1246" s="T437">Then he cannot work, he cannot work.</ta>
            <ta e="T454" id="Seg_1247" s="T444">And the merchant came: "Why don't you work, don't you sing the song?"</ta>
            <ta e="T458" id="Seg_1248" s="T454">"There, have your money!"</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T372" id="Seg_1249" s="T366">Es lebte einmal zwei Männer, ein Kaufmann und ein Schmid.</ta>
            <ta e="T375" id="Seg_1250" s="T372">Der Schmid hatte gar nichts.</ta>
            <ta e="T380" id="Seg_1251" s="T375">Aber er [der Kaufmann] hatte sehr viel Geld.</ta>
            <ta e="T391" id="Seg_1252" s="T380">Er passt immer auf, dass keiner stehlt, schläft nicht, ist nicht.</ta>
            <ta e="T401" id="Seg_1253" s="T391">Und [der Schmid] geht zur Schmiede, arbeitet, singt ein Lied.</ta>
            <ta e="T406" id="Seg_1254" s="T401">[Dann] kommt er nach Hause, er spricht ein bisschen, [und] geht ins Bett.</ta>
            <ta e="T409" id="Seg_1255" s="T406">Dann schaut er [auf den Schmid]:</ta>
            <ta e="T414" id="Seg_1256" s="T409">“Warum singst du immer ein Lied?“</ta>
            <ta e="T422" id="Seg_1257" s="T414">“Weil ich arbeite, [und] mein Herz sich erfreut.“</ta>
            <ta e="T432" id="Seg_1258" s="T422">Dann nahm er sein Geld, ging und gab es dort dem Schmid.</ta>
            <ta e="T437" id="Seg_1259" s="T432">Er nahm, fing an aufzupassen.</ta>
            <ta e="T444" id="Seg_1260" s="T437">Dann kann er nicht arbeiten, er kann nicht arbeiten.</ta>
            <ta e="T454" id="Seg_1261" s="T444">Und der Kaufmann kam: „Warum arbeitest du nicht, singst du nicht das Lied?“</ta>
            <ta e="T458" id="Seg_1262" s="T454">“Da, nimm dein Geld!“</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T406" id="Seg_1263" s="T401">[GVY:] Probably she mixed up šaːmnaʔbə 'lie-DUR[-3SG]' and šaː-laʔbə 'spend.night-DUR[-3SG]'</ta>
            <ta e="T432" id="Seg_1264" s="T422">[KlT:] Barə ’hand over’, see also previous tale.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
