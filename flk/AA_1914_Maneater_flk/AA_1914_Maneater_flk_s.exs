<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDF4E34453-4618-8A9A-03C7-531970462BBC">
   <head>
      <meta-information>
         <project-name>Kamas</project-name>
         <transcription-name>AA_1914_Maneater_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\AA_1914_Maneater_flk\AA_1914_Maneater_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">135</ud-information>
            <ud-information attribute-name="# HIAT:w">98</ud-information>
            <ud-information attribute-name="# e">98</ud-information>
            <ud-information attribute-name="# HIAT:u">23</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="AA">
            <abbreviation>AA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T357" time="182.5" type="appl" />
         <tli id="T358" time="183.0" type="appl" />
         <tli id="T359" time="183.5" type="appl" />
         <tli id="T360" time="184.0" type="appl" />
         <tli id="T361" time="184.5" type="appl" />
         <tli id="T362" time="185.0" type="appl" />
         <tli id="T363" time="185.5" type="appl" />
         <tli id="T364" time="186.0" type="appl" />
         <tli id="T365" time="186.5" type="appl" />
         <tli id="T366" time="187.0" type="appl" />
         <tli id="T367" time="187.5" type="appl" />
         <tli id="T368" time="188.0" type="appl" />
         <tli id="T369" time="188.5" type="appl" />
         <tli id="T370" time="189.0" type="appl" />
         <tli id="T371" time="189.5" type="appl" />
         <tli id="T372" time="190.0" type="appl" />
         <tli id="T373" time="190.5" type="appl" />
         <tli id="T374" time="191.0" type="appl" />
         <tli id="T375" time="191.5" type="appl" />
         <tli id="T376" time="192.0" type="appl" />
         <tli id="T377" time="192.5" type="appl" />
         <tli id="T378" time="193.0" type="appl" />
         <tli id="T379" time="193.5" type="appl" />
         <tli id="T380" time="194.0" type="appl" />
         <tli id="T381" time="194.5" type="appl" />
         <tli id="T382" time="195.0" type="appl" />
         <tli id="T383" time="195.5" type="appl" />
         <tli id="T384" time="196.0" type="appl" />
         <tli id="T385" time="196.5" type="appl" />
         <tli id="T386" time="197.0" type="appl" />
         <tli id="T387" time="197.5" type="appl" />
         <tli id="T388" time="198.0" type="appl" />
         <tli id="T389" time="198.5" type="appl" />
         <tli id="T390" time="199.0" type="appl" />
         <tli id="T391" time="199.5" type="appl" />
         <tli id="T392" time="200.0" type="appl" />
         <tli id="T393" time="200.5" type="appl" />
         <tli id="T394" time="201.0" type="appl" />
         <tli id="T395" time="201.5" type="appl" />
         <tli id="T396" time="202.0" type="appl" />
         <tli id="T397" time="202.5" type="appl" />
         <tli id="T398" time="203.0" type="appl" />
         <tli id="T399" time="203.5" type="appl" />
         <tli id="T400" time="204.0" type="appl" />
         <tli id="T401" time="204.5" type="appl" />
         <tli id="T402" time="205.0" type="appl" />
         <tli id="T403" time="205.5" type="appl" />
         <tli id="T404" time="206.0" type="appl" />
         <tli id="T405" time="206.5" type="appl" />
         <tli id="T406" time="207.0" type="appl" />
         <tli id="T407" time="207.5" type="appl" />
         <tli id="T408" time="208.0" type="appl" />
         <tli id="T409" time="208.5" type="appl" />
         <tli id="T410" time="209.0" type="appl" />
         <tli id="T411" time="209.5" type="appl" />
         <tli id="T412" time="210.0" type="appl" />
         <tli id="T413" time="210.5" type="appl" />
         <tli id="T414" time="211.0" type="appl" />
         <tli id="T415" time="211.5" type="appl" />
         <tli id="T416" time="212.0" type="appl" />
         <tli id="T417" time="212.5" type="appl" />
         <tli id="T418" time="213.0" type="appl" />
         <tli id="T419" time="213.5" type="appl" />
         <tli id="T420" time="214.0" type="appl" />
         <tli id="T421" time="214.5" type="appl" />
         <tli id="T422" time="215.0" type="appl" />
         <tli id="T423" time="215.5" type="appl" />
         <tli id="T424" time="216.0" type="appl" />
         <tli id="T425" time="216.5" type="appl" />
         <tli id="T426" time="217.0" type="appl" />
         <tli id="T427" time="217.5" type="appl" />
         <tli id="T428" time="218.0" type="appl" />
         <tli id="T429" time="218.5" type="appl" />
         <tli id="T430" time="219.0" type="appl" />
         <tli id="T431" time="219.5" type="appl" />
         <tli id="T432" time="220.0" type="appl" />
         <tli id="T433" time="220.5" type="appl" />
         <tli id="T434" time="221.0" type="appl" />
         <tli id="T435" time="221.5" type="appl" />
         <tli id="T436" time="222.0" type="appl" />
         <tli id="T437" time="222.5" type="appl" />
         <tli id="T438" time="223.0" type="appl" />
         <tli id="T439" time="223.5" type="appl" />
         <tli id="T440" time="224.0" type="appl" />
         <tli id="T441" time="224.5" type="appl" />
         <tli id="T442" time="225.0" type="appl" />
         <tli id="T443" time="225.5" type="appl" />
         <tli id="T444" time="226.0" type="appl" />
         <tli id="T445" time="226.5" type="appl" />
         <tli id="T446" time="227.0" type="appl" />
         <tli id="T447" time="227.5" type="appl" />
         <tli id="T448" time="228.0" type="appl" />
         <tli id="T449" time="228.5" type="appl" />
         <tli id="T450" time="229.0" type="appl" />
         <tli id="T451" time="229.5" type="appl" />
         <tli id="T452" time="230.0" type="appl" />
         <tli id="T453" time="230.5" type="appl" />
         <tli id="T454" time="231.0" type="appl" />
         <tli id="T455" time="231.5" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="AA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T455" id="Seg_0" n="sc" s="T357">
               <ts e="T360" id="Seg_2" n="HIAT:u" s="T357">
                  <ts e="T358" id="Seg_4" n="HIAT:w" s="T357">Šide</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_7" n="HIAT:w" s="T358">kagazəgəj</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_10" n="HIAT:w" s="T359">amnobiiʔ</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T366" id="Seg_14" n="HIAT:u" s="T360">
                  <ts e="T361" id="Seg_16" n="HIAT:w" s="T360">Nüke</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_19" n="HIAT:w" s="T361">šidegöʔ</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_22" n="HIAT:w" s="T362">ešši</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_25" n="HIAT:w" s="T363">deppi</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_27" n="HIAT:ip">(</nts>
                  <ts e="T365" id="Seg_29" n="HIAT:w" s="T364">~deppiːze</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_32" n="HIAT:w" s="T365">ibiiʔ</ts>
                  <nts id="Seg_33" n="HIAT:ip">)</nts>
                  <nts id="Seg_34" n="HIAT:ip">.</nts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T370" id="Seg_37" n="HIAT:u" s="T366">
                  <ts e="T367" id="Seg_39" n="HIAT:w" s="T366">Amit</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_42" n="HIAT:w" s="T367">kagan</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_45" n="HIAT:w" s="T368">iʔgö</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_48" n="HIAT:w" s="T369">ibi</ts>
                  <nts id="Seg_49" n="HIAT:ip">.</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T373" id="Seg_52" n="HIAT:u" s="T370">
                  <ts e="T371" id="Seg_54" n="HIAT:w" s="T370">Dĭzeŋ</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_57" n="HIAT:w" s="T371">saməjlaʔ</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_60" n="HIAT:w" s="T372">kambiiʔ</ts>
                  <nts id="Seg_61" n="HIAT:ip">.</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T376" id="Seg_64" n="HIAT:u" s="T373">
                  <ts e="T374" id="Seg_66" n="HIAT:w" s="T373">Nükezeŋdən</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_69" n="HIAT:w" s="T374">maʔlaʔ</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_72" n="HIAT:w" s="T375">kojobiiʔ</ts>
                  <nts id="Seg_73" n="HIAT:ip">.</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T380" id="Seg_76" n="HIAT:u" s="T376">
                  <ts e="T377" id="Seg_78" n="HIAT:w" s="T376">Nüdʼin</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_81" n="HIAT:w" s="T377">omnə</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_84" n="HIAT:w" s="T378">amorzʼəttə</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_87" n="HIAT:w" s="T379">amnəbi</ts>
                  <nts id="Seg_88" n="HIAT:ip">.</nts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T384" id="Seg_91" n="HIAT:u" s="T380">
                  <ts e="T381" id="Seg_93" n="HIAT:w" s="T380">Mĭjeendə</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_96" n="HIAT:w" s="T381">kulaʔ</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_99" n="HIAT:w" s="T382">baʔbi</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_102" n="HIAT:w" s="T383">gülge</ts>
                  <nts id="Seg_103" n="HIAT:ip">.</nts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T388" id="Seg_106" n="HIAT:u" s="T384">
                  <ts e="T385" id="Seg_108" n="HIAT:w" s="T384">Torikadul</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_111" n="HIAT:w" s="T385">maandə</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_114" n="HIAT:w" s="T386">nĭgəndə</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_117" n="HIAT:w" s="T387">sʼalaːmbi</ts>
                  <nts id="Seg_118" n="HIAT:ip">.</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T394" id="Seg_121" n="HIAT:u" s="T388">
                  <ts e="T389" id="Seg_123" n="HIAT:w" s="T388">Mazərogən</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_126" n="HIAT:w" s="T389">măndolaʔ</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_129" n="HIAT:w" s="T390">dĭ</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_132" n="HIAT:w" s="T391">ne</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_135" n="HIAT:w" s="T392">baʔluʔbi</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_138" n="HIAT:w" s="T393">bĭssʼəttə</ts>
                  <nts id="Seg_139" n="HIAT:ip">.</nts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T397" id="Seg_142" n="HIAT:u" s="T394">
                  <ts e="T395" id="Seg_144" n="HIAT:w" s="T394">Mĭjebə</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_147" n="HIAT:w" s="T395">edəlabaʔbi</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_150" n="HIAT:w" s="T396">kedʼi</ts>
                  <nts id="Seg_151" n="HIAT:ip">.</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T401" id="Seg_154" n="HIAT:u" s="T397">
                  <ts e="T398" id="Seg_156" n="HIAT:w" s="T397">Uʔbdə</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_159" n="HIAT:w" s="T398">bostə</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_162" n="HIAT:w" s="T399">pa</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_165" n="HIAT:w" s="T400">pajsʼtə</ts>
                  <nts id="Seg_166" n="HIAT:ip">.</nts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T406" id="Seg_169" n="HIAT:u" s="T401">
                  <ts e="T402" id="Seg_171" n="HIAT:w" s="T401">Tʼüpi</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_174" n="HIAT:w" s="T402">paiʔ</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_177" n="HIAT:w" s="T403">pajbi</ts>
                  <nts id="Seg_178" n="HIAT:ip">,</nts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_181" n="HIAT:w" s="T404">šügəndə</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_184" n="HIAT:w" s="T405">embi</ts>
                  <nts id="Seg_185" n="HIAT:ip">.</nts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T412" id="Seg_188" n="HIAT:u" s="T406">
                  <ts e="T407" id="Seg_190" n="HIAT:w" s="T406">Dĭgəttə</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_193" n="HIAT:w" s="T407">baltu</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_196" n="HIAT:w" s="T408">ibi</ts>
                  <nts id="Seg_197" n="HIAT:ip">,</nts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_200" n="HIAT:w" s="T409">jadən</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_203" n="HIAT:w" s="T410">tabəndə</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_206" n="HIAT:w" s="T411">amnəbi</ts>
                  <nts id="Seg_207" n="HIAT:ip">.</nts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T419" id="Seg_210" n="HIAT:u" s="T412">
                  <ts e="T413" id="Seg_212" n="HIAT:w" s="T412">Torigadul</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_215" n="HIAT:w" s="T413">pin</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_218" n="HIAT:w" s="T414">üzəleʔ</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_221" n="HIAT:w" s="T415">šobi</ts>
                  <nts id="Seg_222" n="HIAT:ip">,</nts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_225" n="HIAT:w" s="T416">šübi</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_228" n="HIAT:w" s="T417">maan</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_231" n="HIAT:w" s="T418">šüjööndə</ts>
                  <nts id="Seg_232" n="HIAT:ip">.</nts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T424" id="Seg_235" n="HIAT:u" s="T419">
                  <ts e="T420" id="Seg_237" n="HIAT:w" s="T419">Šüm</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_240" n="HIAT:w" s="T420">püʔliet</ts>
                  <nts id="Seg_241" n="HIAT:ip">,</nts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_244" n="HIAT:w" s="T421">püʔliet</ts>
                  <nts id="Seg_245" n="HIAT:ip">,</nts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_248" n="HIAT:w" s="T422">ej</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_251" n="HIAT:w" s="T423">amolia</ts>
                  <nts id="Seg_252" n="HIAT:ip">.</nts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T431" id="Seg_255" n="HIAT:u" s="T424">
                  <ts e="T425" id="Seg_257" n="HIAT:w" s="T424">Dö</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_260" n="HIAT:w" s="T425">ne</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_263" n="HIAT:w" s="T426">kutlaʔ</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_266" n="HIAT:w" s="T427">baʔbdəbi</ts>
                  <nts id="Seg_267" n="HIAT:ip">,</nts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_270" n="HIAT:w" s="T428">amim</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_273" n="HIAT:w" s="T429">kutlaʔ</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_276" n="HIAT:w" s="T430">baʔbdəbi</ts>
                  <nts id="Seg_277" n="HIAT:ip">.</nts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T436" id="Seg_280" n="HIAT:u" s="T431">
                  <ts e="T432" id="Seg_282" n="HIAT:w" s="T431">Dĭgəttə</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_285" n="HIAT:w" s="T432">šobi</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_288" n="HIAT:w" s="T433">dĭ</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_291" n="HIAT:w" s="T434">nenə</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_294" n="HIAT:w" s="T435">bazoʔ</ts>
                  <nts id="Seg_295" n="HIAT:ip">.</nts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T439" id="Seg_298" n="HIAT:u" s="T436">
                  <nts id="Seg_299" n="HIAT:ip">"</nts>
                  <ts e="T437" id="Seg_301" n="HIAT:w" s="T436">Tăn</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_304" n="HIAT:w" s="T437">ĭmbi</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_307" n="HIAT:w" s="T438">alial</ts>
                  <nts id="Seg_308" n="HIAT:ip">?</nts>
                  <nts id="Seg_309" n="HIAT:ip">"</nts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T441" id="Seg_312" n="HIAT:u" s="T439">
                  <nts id="Seg_313" n="HIAT:ip">"</nts>
                  <ts e="T440" id="Seg_315" n="HIAT:w" s="T439">Măn</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_318" n="HIAT:w" s="T440">kutlabaʔbiom</ts>
                  <nts id="Seg_319" n="HIAT:ip">.</nts>
                  <nts id="Seg_320" n="HIAT:ip">"</nts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T445" id="Seg_323" n="HIAT:u" s="T441">
                  <ts e="T442" id="Seg_325" n="HIAT:w" s="T441">Dĭ</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_328" n="HIAT:w" s="T442">ne</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_331" n="HIAT:w" s="T443">nörbəlie:</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_333" n="HIAT:ip">"</nts>
                  <ts e="T445" id="Seg_335" n="HIAT:w" s="T444">Püʔbdit</ts>
                  <nts id="Seg_336" n="HIAT:ip">!</nts>
                  <nts id="Seg_337" n="HIAT:ip">"</nts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T446" id="Seg_340" n="HIAT:u" s="T445">
                  <ts e="T446" id="Seg_342" n="HIAT:w" s="T445">Püʔleʔbi</ts>
                  <nts id="Seg_343" n="HIAT:ip">.</nts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T452" id="Seg_346" n="HIAT:u" s="T446">
                  <ts e="T447" id="Seg_348" n="HIAT:w" s="T446">Dĭ</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_351" n="HIAT:w" s="T447">ne</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_354" n="HIAT:w" s="T448">bajʔkəbə</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_357" n="HIAT:w" s="T449">bazoʔ</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_360" n="HIAT:w" s="T450">saj</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_363" n="HIAT:w" s="T451">hʼaʔluʔbi</ts>
                  <nts id="Seg_364" n="HIAT:ip">.</nts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T455" id="Seg_367" n="HIAT:u" s="T452">
                  <ts e="T453" id="Seg_369" n="HIAT:w" s="T452">Nem</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_372" n="HIAT:w" s="T453">esseŋdətsʼəʔ</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_375" n="HIAT:w" s="T454">kutlabaʔbi</ts>
                  <nts id="Seg_376" n="HIAT:ip">.</nts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T455" id="Seg_378" n="sc" s="T357">
               <ts e="T358" id="Seg_380" n="e" s="T357">Šide </ts>
               <ts e="T359" id="Seg_382" n="e" s="T358">kagazəgəj </ts>
               <ts e="T360" id="Seg_384" n="e" s="T359">amnobiiʔ. </ts>
               <ts e="T361" id="Seg_386" n="e" s="T360">Nüke </ts>
               <ts e="T362" id="Seg_388" n="e" s="T361">šidegöʔ </ts>
               <ts e="T363" id="Seg_390" n="e" s="T362">ešši </ts>
               <ts e="T364" id="Seg_392" n="e" s="T363">deppi </ts>
               <ts e="T365" id="Seg_394" n="e" s="T364">(~deppiːze </ts>
               <ts e="T366" id="Seg_396" n="e" s="T365">ibiiʔ). </ts>
               <ts e="T367" id="Seg_398" n="e" s="T366">Amit </ts>
               <ts e="T368" id="Seg_400" n="e" s="T367">kagan </ts>
               <ts e="T369" id="Seg_402" n="e" s="T368">iʔgö </ts>
               <ts e="T370" id="Seg_404" n="e" s="T369">ibi. </ts>
               <ts e="T371" id="Seg_406" n="e" s="T370">Dĭzeŋ </ts>
               <ts e="T372" id="Seg_408" n="e" s="T371">saməjlaʔ </ts>
               <ts e="T373" id="Seg_410" n="e" s="T372">kambiiʔ. </ts>
               <ts e="T374" id="Seg_412" n="e" s="T373">Nükezeŋdən </ts>
               <ts e="T375" id="Seg_414" n="e" s="T374">maʔlaʔ </ts>
               <ts e="T376" id="Seg_416" n="e" s="T375">kojobiiʔ. </ts>
               <ts e="T377" id="Seg_418" n="e" s="T376">Nüdʼin </ts>
               <ts e="T378" id="Seg_420" n="e" s="T377">omnə </ts>
               <ts e="T379" id="Seg_422" n="e" s="T378">amorzʼəttə </ts>
               <ts e="T380" id="Seg_424" n="e" s="T379">amnəbi. </ts>
               <ts e="T381" id="Seg_426" n="e" s="T380">Mĭjeendə </ts>
               <ts e="T382" id="Seg_428" n="e" s="T381">kulaʔ </ts>
               <ts e="T383" id="Seg_430" n="e" s="T382">baʔbi </ts>
               <ts e="T384" id="Seg_432" n="e" s="T383">gülge. </ts>
               <ts e="T385" id="Seg_434" n="e" s="T384">Torikadul </ts>
               <ts e="T386" id="Seg_436" n="e" s="T385">maandə </ts>
               <ts e="T387" id="Seg_438" n="e" s="T386">nĭgəndə </ts>
               <ts e="T388" id="Seg_440" n="e" s="T387">sʼalaːmbi. </ts>
               <ts e="T389" id="Seg_442" n="e" s="T388">Mazərogən </ts>
               <ts e="T390" id="Seg_444" n="e" s="T389">măndolaʔ </ts>
               <ts e="T391" id="Seg_446" n="e" s="T390">dĭ </ts>
               <ts e="T392" id="Seg_448" n="e" s="T391">ne </ts>
               <ts e="T393" id="Seg_450" n="e" s="T392">baʔluʔbi </ts>
               <ts e="T394" id="Seg_452" n="e" s="T393">bĭssʼəttə. </ts>
               <ts e="T395" id="Seg_454" n="e" s="T394">Mĭjebə </ts>
               <ts e="T396" id="Seg_456" n="e" s="T395">edəlabaʔbi </ts>
               <ts e="T397" id="Seg_458" n="e" s="T396">kedʼi. </ts>
               <ts e="T398" id="Seg_460" n="e" s="T397">Uʔbdə </ts>
               <ts e="T399" id="Seg_462" n="e" s="T398">bostə </ts>
               <ts e="T400" id="Seg_464" n="e" s="T399">pa </ts>
               <ts e="T401" id="Seg_466" n="e" s="T400">pajsʼtə. </ts>
               <ts e="T402" id="Seg_468" n="e" s="T401">Tʼüpi </ts>
               <ts e="T403" id="Seg_470" n="e" s="T402">paiʔ </ts>
               <ts e="T404" id="Seg_472" n="e" s="T403">pajbi, </ts>
               <ts e="T405" id="Seg_474" n="e" s="T404">šügəndə </ts>
               <ts e="T406" id="Seg_476" n="e" s="T405">embi. </ts>
               <ts e="T407" id="Seg_478" n="e" s="T406">Dĭgəttə </ts>
               <ts e="T408" id="Seg_480" n="e" s="T407">baltu </ts>
               <ts e="T409" id="Seg_482" n="e" s="T408">ibi, </ts>
               <ts e="T410" id="Seg_484" n="e" s="T409">jadən </ts>
               <ts e="T411" id="Seg_486" n="e" s="T410">tabəndə </ts>
               <ts e="T412" id="Seg_488" n="e" s="T411">amnəbi. </ts>
               <ts e="T413" id="Seg_490" n="e" s="T412">Torigadul </ts>
               <ts e="T414" id="Seg_492" n="e" s="T413">pin </ts>
               <ts e="T415" id="Seg_494" n="e" s="T414">üzəleʔ </ts>
               <ts e="T416" id="Seg_496" n="e" s="T415">šobi, </ts>
               <ts e="T417" id="Seg_498" n="e" s="T416">šübi </ts>
               <ts e="T418" id="Seg_500" n="e" s="T417">maan </ts>
               <ts e="T419" id="Seg_502" n="e" s="T418">šüjööndə. </ts>
               <ts e="T420" id="Seg_504" n="e" s="T419">Šüm </ts>
               <ts e="T421" id="Seg_506" n="e" s="T420">püʔliet, </ts>
               <ts e="T422" id="Seg_508" n="e" s="T421">püʔliet, </ts>
               <ts e="T423" id="Seg_510" n="e" s="T422">ej </ts>
               <ts e="T424" id="Seg_512" n="e" s="T423">amolia. </ts>
               <ts e="T425" id="Seg_514" n="e" s="T424">Dö </ts>
               <ts e="T426" id="Seg_516" n="e" s="T425">ne </ts>
               <ts e="T427" id="Seg_518" n="e" s="T426">kutlaʔ </ts>
               <ts e="T428" id="Seg_520" n="e" s="T427">baʔbdəbi, </ts>
               <ts e="T429" id="Seg_522" n="e" s="T428">amim </ts>
               <ts e="T430" id="Seg_524" n="e" s="T429">kutlaʔ </ts>
               <ts e="T431" id="Seg_526" n="e" s="T430">baʔbdəbi. </ts>
               <ts e="T432" id="Seg_528" n="e" s="T431">Dĭgəttə </ts>
               <ts e="T433" id="Seg_530" n="e" s="T432">šobi </ts>
               <ts e="T434" id="Seg_532" n="e" s="T433">dĭ </ts>
               <ts e="T435" id="Seg_534" n="e" s="T434">nenə </ts>
               <ts e="T436" id="Seg_536" n="e" s="T435">bazoʔ. </ts>
               <ts e="T437" id="Seg_538" n="e" s="T436">"Tăn </ts>
               <ts e="T438" id="Seg_540" n="e" s="T437">ĭmbi </ts>
               <ts e="T439" id="Seg_542" n="e" s="T438">alial?" </ts>
               <ts e="T440" id="Seg_544" n="e" s="T439">"Măn </ts>
               <ts e="T441" id="Seg_546" n="e" s="T440">kutlabaʔbiom." </ts>
               <ts e="T442" id="Seg_548" n="e" s="T441">Dĭ </ts>
               <ts e="T443" id="Seg_550" n="e" s="T442">ne </ts>
               <ts e="T444" id="Seg_552" n="e" s="T443">nörbəlie: </ts>
               <ts e="T445" id="Seg_554" n="e" s="T444">"Püʔbdit!" </ts>
               <ts e="T446" id="Seg_556" n="e" s="T445">Püʔleʔbi. </ts>
               <ts e="T447" id="Seg_558" n="e" s="T446">Dĭ </ts>
               <ts e="T448" id="Seg_560" n="e" s="T447">ne </ts>
               <ts e="T449" id="Seg_562" n="e" s="T448">bajʔkəbə </ts>
               <ts e="T450" id="Seg_564" n="e" s="T449">bazoʔ </ts>
               <ts e="T451" id="Seg_566" n="e" s="T450">saj </ts>
               <ts e="T452" id="Seg_568" n="e" s="T451">hʼaʔluʔbi. </ts>
               <ts e="T453" id="Seg_570" n="e" s="T452">Nem </ts>
               <ts e="T454" id="Seg_572" n="e" s="T453">esseŋdətsʼəʔ </ts>
               <ts e="T455" id="Seg_574" n="e" s="T454">kutlabaʔbi. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T360" id="Seg_575" s="T357">AA_1914_Maneater_flk.001 (001.001)</ta>
            <ta e="T366" id="Seg_576" s="T360">AA_1914_Maneater_flk.002 (001.002)</ta>
            <ta e="T370" id="Seg_577" s="T366">AA_1914_Maneater_flk.003 (001.003)</ta>
            <ta e="T373" id="Seg_578" s="T370">AA_1914_Maneater_flk.004 (001.004)</ta>
            <ta e="T376" id="Seg_579" s="T373">AA_1914_Maneater_flk.005 (001.005)</ta>
            <ta e="T380" id="Seg_580" s="T376">AA_1914_Maneater_flk.006 (001.006)</ta>
            <ta e="T384" id="Seg_581" s="T380">AA_1914_Maneater_flk.007 (001.007)</ta>
            <ta e="T388" id="Seg_582" s="T384">AA_1914_Maneater_flk.008 (001.008)</ta>
            <ta e="T394" id="Seg_583" s="T388">AA_1914_Maneater_flk.009 (001.009)</ta>
            <ta e="T397" id="Seg_584" s="T394">AA_1914_Maneater_flk.010 (001.010)</ta>
            <ta e="T401" id="Seg_585" s="T397">AA_1914_Maneater_flk.011 (001.011)</ta>
            <ta e="T406" id="Seg_586" s="T401">AA_1914_Maneater_flk.012 (001.012)</ta>
            <ta e="T412" id="Seg_587" s="T406">AA_1914_Maneater_flk.013 (001.013)</ta>
            <ta e="T419" id="Seg_588" s="T412">AA_1914_Maneater_flk.014 (001.014)</ta>
            <ta e="T424" id="Seg_589" s="T419">AA_1914_Maneater_flk.015 (001.015)</ta>
            <ta e="T431" id="Seg_590" s="T424">AA_1914_Maneater_flk.016 (001.016)</ta>
            <ta e="T436" id="Seg_591" s="T431">AA_1914_Maneater_flk.017 (001.017)</ta>
            <ta e="T439" id="Seg_592" s="T436">AA_1914_Maneater_flk.018 (001.018)</ta>
            <ta e="T441" id="Seg_593" s="T439">AA_1914_Maneater_flk.019 (001.019)</ta>
            <ta e="T445" id="Seg_594" s="T441">AA_1914_Maneater_flk.020 (001.020)</ta>
            <ta e="T446" id="Seg_595" s="T445">AA_1914_Maneater_flk.021 (001.022)</ta>
            <ta e="T452" id="Seg_596" s="T446">AA_1914_Maneater_flk.022 (001.023)</ta>
            <ta e="T455" id="Seg_597" s="T452">AA_1914_Maneater_flk.023 (001.024)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T360" id="Seg_598" s="T357">Šide kagazəgəj amnobiiʔ. </ta>
            <ta e="T366" id="Seg_599" s="T360">Nüke šidegöʔ ešši deppi (~deppiːze ibiiʔ). </ta>
            <ta e="T370" id="Seg_600" s="T366">Amit kagan iʔgö ibi. </ta>
            <ta e="T373" id="Seg_601" s="T370">Dĭzeŋ saməjlaʔ kambiiʔ. </ta>
            <ta e="T376" id="Seg_602" s="T373">Nükezeŋdən maʔlaʔ kojobiiʔ. </ta>
            <ta e="T380" id="Seg_603" s="T376">Nüdʼin omnə amorzʼəttə amnəbi. </ta>
            <ta e="T384" id="Seg_604" s="T380">Mĭjeendə kulaʔ baʔbi gülge. </ta>
            <ta e="T388" id="Seg_605" s="T384">Torikadul maandə nĭgəndə sʼalaːmbi. </ta>
            <ta e="T394" id="Seg_606" s="T388">Mazərogən măndolaʔ dĭ ne baʔluʔbi bĭssʼəttə. </ta>
            <ta e="T397" id="Seg_607" s="T394">Mĭjebə edəlabaʔbi kedʼi. </ta>
            <ta e="T401" id="Seg_608" s="T397">Uʔbdə bostə pa pajsʼtə. </ta>
            <ta e="T406" id="Seg_609" s="T401">Tʼüpi paiʔ pajbi, šügəndə embi. </ta>
            <ta e="T412" id="Seg_610" s="T406">Dĭgəttə baltu ibi, jadən tabəndə amnəbi. </ta>
            <ta e="T419" id="Seg_611" s="T412">Torigadul pin üzəleʔ šobi, šübi maan šüjööndə. </ta>
            <ta e="T424" id="Seg_612" s="T419">Šüm püʔliet, püʔliet, ej amolia. </ta>
            <ta e="T431" id="Seg_613" s="T424">Dö ne kutlaʔ baʔbdəbi, amim kutlaʔ baʔbdəbi. </ta>
            <ta e="T436" id="Seg_614" s="T431">Dĭgəttə šobi dĭ nenə bazoʔ. </ta>
            <ta e="T439" id="Seg_615" s="T436">"Tăn ĭmbi alial?" </ta>
            <ta e="T441" id="Seg_616" s="T439">"Măn kutlabaʔbiom." </ta>
            <ta e="T445" id="Seg_617" s="T441">Dĭ ne nörbəlie: "Püʔbdit!" </ta>
            <ta e="T446" id="Seg_618" s="T445">Püʔleʔbi. </ta>
            <ta e="T452" id="Seg_619" s="T446">Dĭ ne bajʔkəbə bazoʔ saj hʼaʔluʔbi. </ta>
            <ta e="T455" id="Seg_620" s="T452">Nem esseŋdətsʼəʔ kutlabaʔbi. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T358" id="Seg_621" s="T357">šide</ta>
            <ta e="T359" id="Seg_622" s="T358">kaga-zə-gəj</ta>
            <ta e="T360" id="Seg_623" s="T359">amno-bi-iʔ</ta>
            <ta e="T361" id="Seg_624" s="T360">nüke</ta>
            <ta e="T362" id="Seg_625" s="T361">šide-göʔ</ta>
            <ta e="T363" id="Seg_626" s="T362">ešši</ta>
            <ta e="T364" id="Seg_627" s="T363">dep-pi</ta>
            <ta e="T365" id="Seg_628" s="T364">dep-piːze</ta>
            <ta e="T366" id="Seg_629" s="T365">i-bi-iʔ</ta>
            <ta e="T367" id="Seg_630" s="T366">ami-t</ta>
            <ta e="T368" id="Seg_631" s="T367">kaga-n</ta>
            <ta e="T369" id="Seg_632" s="T368">iʔgö</ta>
            <ta e="T370" id="Seg_633" s="T369">i-bi</ta>
            <ta e="T371" id="Seg_634" s="T370">dĭ-zeŋ</ta>
            <ta e="T372" id="Seg_635" s="T371">saməj-laʔ</ta>
            <ta e="T373" id="Seg_636" s="T372">kam-bi-iʔ</ta>
            <ta e="T374" id="Seg_637" s="T373">nüke-zeŋ-dən</ta>
            <ta e="T375" id="Seg_638" s="T374">maʔ-laʔ</ta>
            <ta e="T376" id="Seg_639" s="T375">kojo-bi-iʔ</ta>
            <ta e="T377" id="Seg_640" s="T376">nüdʼi-n</ta>
            <ta e="T378" id="Seg_641" s="T377">omnə</ta>
            <ta e="T379" id="Seg_642" s="T378">amor-zʼəttə</ta>
            <ta e="T380" id="Seg_643" s="T379">amnə-bi</ta>
            <ta e="T381" id="Seg_644" s="T380">mĭje-endə</ta>
            <ta e="T382" id="Seg_645" s="T381">ku-laʔ</ta>
            <ta e="T383" id="Seg_646" s="T382">baʔ-bi</ta>
            <ta e="T384" id="Seg_647" s="T383">gülge</ta>
            <ta e="T385" id="Seg_648" s="T384">torikadul</ta>
            <ta e="T386" id="Seg_649" s="T385">ma-andə</ta>
            <ta e="T387" id="Seg_650" s="T386">nĭ-gəndə</ta>
            <ta e="T388" id="Seg_651" s="T387">sʼa-laːm-bi</ta>
            <ta e="T389" id="Seg_652" s="T388">mazəro-gən</ta>
            <ta e="T390" id="Seg_653" s="T389">măndo-laʔ</ta>
            <ta e="T391" id="Seg_654" s="T390">dĭ</ta>
            <ta e="T392" id="Seg_655" s="T391">ne</ta>
            <ta e="T393" id="Seg_656" s="T392">baʔ-luʔ-bi</ta>
            <ta e="T394" id="Seg_657" s="T393">bĭs-sʼəttə</ta>
            <ta e="T395" id="Seg_658" s="T394">mĭje-bə</ta>
            <ta e="T396" id="Seg_659" s="T395">edə-labaʔ-bi</ta>
            <ta e="T397" id="Seg_660" s="T396">kedʼi</ta>
            <ta e="T398" id="Seg_661" s="T397">uʔbdə</ta>
            <ta e="T399" id="Seg_662" s="T398">bostə</ta>
            <ta e="T400" id="Seg_663" s="T399">pa</ta>
            <ta e="T401" id="Seg_664" s="T400">pa-j-sʼtə</ta>
            <ta e="T402" id="Seg_665" s="T401">tʼüpi</ta>
            <ta e="T403" id="Seg_666" s="T402">pa-iʔ</ta>
            <ta e="T404" id="Seg_667" s="T403">pa-j-bi</ta>
            <ta e="T405" id="Seg_668" s="T404">šü-gəndə</ta>
            <ta e="T406" id="Seg_669" s="T405">em-bi</ta>
            <ta e="T407" id="Seg_670" s="T406">dĭgəttə</ta>
            <ta e="T408" id="Seg_671" s="T407">baltu</ta>
            <ta e="T409" id="Seg_672" s="T408">i-bi</ta>
            <ta e="T410" id="Seg_673" s="T409">jadə-n</ta>
            <ta e="T411" id="Seg_674" s="T410">tabə-ndə</ta>
            <ta e="T412" id="Seg_675" s="T411">amnə-bi</ta>
            <ta e="T413" id="Seg_676" s="T412">torigadul</ta>
            <ta e="T414" id="Seg_677" s="T413">pi-n</ta>
            <ta e="T415" id="Seg_678" s="T414">üzə-leʔ</ta>
            <ta e="T416" id="Seg_679" s="T415">šo-bi</ta>
            <ta e="T417" id="Seg_680" s="T416">šü-bi</ta>
            <ta e="T418" id="Seg_681" s="T417">ma-a-n</ta>
            <ta e="T419" id="Seg_682" s="T418">šüjö-öndə</ta>
            <ta e="T420" id="Seg_683" s="T419">šü-m</ta>
            <ta e="T421" id="Seg_684" s="T420">püʔ-lie-t</ta>
            <ta e="T422" id="Seg_685" s="T421">püʔ-lie-t</ta>
            <ta e="T423" id="Seg_686" s="T422">ej</ta>
            <ta e="T424" id="Seg_687" s="T423">amo-lia</ta>
            <ta e="T425" id="Seg_688" s="T424">dö</ta>
            <ta e="T426" id="Seg_689" s="T425">ne</ta>
            <ta e="T427" id="Seg_690" s="T426">kut-laʔ</ta>
            <ta e="T428" id="Seg_691" s="T427">baʔbdə-bi</ta>
            <ta e="T429" id="Seg_692" s="T428">ami-m</ta>
            <ta e="T430" id="Seg_693" s="T429">kut-laʔ</ta>
            <ta e="T431" id="Seg_694" s="T430">baʔbdə-bi</ta>
            <ta e="T432" id="Seg_695" s="T431">dĭgəttə</ta>
            <ta e="T433" id="Seg_696" s="T432">šo-bi</ta>
            <ta e="T434" id="Seg_697" s="T433">dĭ</ta>
            <ta e="T435" id="Seg_698" s="T434">ne-nə</ta>
            <ta e="T436" id="Seg_699" s="T435">bazoʔ</ta>
            <ta e="T437" id="Seg_700" s="T436">tăn</ta>
            <ta e="T438" id="Seg_701" s="T437">ĭmbi</ta>
            <ta e="T439" id="Seg_702" s="T438">a-lia-l</ta>
            <ta e="T440" id="Seg_703" s="T439">măn</ta>
            <ta e="T441" id="Seg_704" s="T440">kut-labaʔ-bio-m</ta>
            <ta e="T442" id="Seg_705" s="T441">dĭ</ta>
            <ta e="T443" id="Seg_706" s="T442">ne</ta>
            <ta e="T444" id="Seg_707" s="T443">nörbə-lie</ta>
            <ta e="T445" id="Seg_708" s="T444">püʔbdi-t</ta>
            <ta e="T446" id="Seg_709" s="T445">püʔ-leʔ-bi</ta>
            <ta e="T447" id="Seg_710" s="T446">dĭ</ta>
            <ta e="T448" id="Seg_711" s="T447">ne</ta>
            <ta e="T449" id="Seg_712" s="T448">bajʔkə-bə</ta>
            <ta e="T450" id="Seg_713" s="T449">bazoʔ</ta>
            <ta e="T451" id="Seg_714" s="T450">saj</ta>
            <ta e="T452" id="Seg_715" s="T451">hʼaʔ-luʔ-bi</ta>
            <ta e="T453" id="Seg_716" s="T452">ne-m</ta>
            <ta e="T454" id="Seg_717" s="T453">es-seŋ-də-t-sʼəʔ</ta>
            <ta e="T455" id="Seg_718" s="T454">kut-labaʔ-bi</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T358" id="Seg_719" s="T357">šide</ta>
            <ta e="T359" id="Seg_720" s="T358">kaga-zə-gəj</ta>
            <ta e="T360" id="Seg_721" s="T359">amno-bi-jəʔ</ta>
            <ta e="T361" id="Seg_722" s="T360">nüke</ta>
            <ta e="T362" id="Seg_723" s="T361">šide-göʔ</ta>
            <ta e="T363" id="Seg_724" s="T362">ešši</ta>
            <ta e="T364" id="Seg_725" s="T363">det-bi</ta>
            <ta e="T365" id="Seg_726" s="T364">det-bizA</ta>
            <ta e="T366" id="Seg_727" s="T365">i-bi-jəʔ</ta>
            <ta e="T367" id="Seg_728" s="T366">ami-t</ta>
            <ta e="T368" id="Seg_729" s="T367">kaga-n</ta>
            <ta e="T369" id="Seg_730" s="T368">iʔgö</ta>
            <ta e="T370" id="Seg_731" s="T369">i-bi</ta>
            <ta e="T371" id="Seg_732" s="T370">dĭ-zAŋ</ta>
            <ta e="T372" id="Seg_733" s="T371">saməj-lAʔ</ta>
            <ta e="T373" id="Seg_734" s="T372">kan-bi-jəʔ</ta>
            <ta e="T374" id="Seg_735" s="T373">nüke-zAŋ-dən</ta>
            <ta e="T375" id="Seg_736" s="T374">ma-lAʔ</ta>
            <ta e="T376" id="Seg_737" s="T375">kojo-bi-jəʔ</ta>
            <ta e="T377" id="Seg_738" s="T376">nüdʼi-n</ta>
            <ta e="T378" id="Seg_739" s="T377">omnə</ta>
            <ta e="T379" id="Seg_740" s="T378">amor-zittə</ta>
            <ta e="T380" id="Seg_741" s="T379">amnə-bi</ta>
            <ta e="T381" id="Seg_742" s="T380">mĭje-gəndə</ta>
            <ta e="T382" id="Seg_743" s="T381">ku-lAʔ</ta>
            <ta e="T383" id="Seg_744" s="T382">baʔbdə-bi</ta>
            <ta e="T384" id="Seg_745" s="T383">gülge</ta>
            <ta e="T385" id="Seg_746" s="T384">torikadul</ta>
            <ta e="T386" id="Seg_747" s="T385">maʔ-gəndə</ta>
            <ta e="T387" id="Seg_748" s="T386">nĭ-gəndə</ta>
            <ta e="T388" id="Seg_749" s="T387">sʼa-laːm-bi</ta>
            <ta e="T389" id="Seg_750" s="T388">mazəro-Kən</ta>
            <ta e="T390" id="Seg_751" s="T389">măndo-lAʔ</ta>
            <ta e="T391" id="Seg_752" s="T390">dĭ</ta>
            <ta e="T392" id="Seg_753" s="T391">ne</ta>
            <ta e="T393" id="Seg_754" s="T392">baʔbdə-luʔbdə-bi</ta>
            <ta e="T394" id="Seg_755" s="T393">bĭs-zittə</ta>
            <ta e="T395" id="Seg_756" s="T394">mĭje-bə</ta>
            <ta e="T396" id="Seg_757" s="T395">edə-labaʔ-bi</ta>
            <ta e="T397" id="Seg_758" s="T396">kedʼi</ta>
            <ta e="T398" id="Seg_759" s="T397">uʔbdə</ta>
            <ta e="T399" id="Seg_760" s="T398">bostə</ta>
            <ta e="T400" id="Seg_761" s="T399">pa</ta>
            <ta e="T401" id="Seg_762" s="T400">pa-j-zittə</ta>
            <ta e="T402" id="Seg_763" s="T401">tʼüpi</ta>
            <ta e="T403" id="Seg_764" s="T402">pa-jəʔ</ta>
            <ta e="T404" id="Seg_765" s="T403">pa-j-bi</ta>
            <ta e="T405" id="Seg_766" s="T404">šü-gəndə</ta>
            <ta e="T406" id="Seg_767" s="T405">hen-bi</ta>
            <ta e="T407" id="Seg_768" s="T406">dĭgəttə</ta>
            <ta e="T408" id="Seg_769" s="T407">baltu</ta>
            <ta e="T409" id="Seg_770" s="T408">i-bi</ta>
            <ta e="T410" id="Seg_771" s="T409">jadə-n</ta>
            <ta e="T411" id="Seg_772" s="T410">tabə-gəndə</ta>
            <ta e="T412" id="Seg_773" s="T411">amnə-bi</ta>
            <ta e="T413" id="Seg_774" s="T412">torikadul</ta>
            <ta e="T414" id="Seg_775" s="T413">pi-n</ta>
            <ta e="T415" id="Seg_776" s="T414">üzə-lAʔ</ta>
            <ta e="T416" id="Seg_777" s="T415">šo-bi</ta>
            <ta e="T417" id="Seg_778" s="T416">šü-bi</ta>
            <ta e="T418" id="Seg_779" s="T417">maʔ-ə-n</ta>
            <ta e="T419" id="Seg_780" s="T418">šüjə-gəndə</ta>
            <ta e="T420" id="Seg_781" s="T419">šü-m</ta>
            <ta e="T421" id="Seg_782" s="T420">püʔbdə-liA-t</ta>
            <ta e="T422" id="Seg_783" s="T421">püʔbdə-liA-t</ta>
            <ta e="T423" id="Seg_784" s="T422">ej</ta>
            <ta e="T424" id="Seg_785" s="T423">amo-liA</ta>
            <ta e="T425" id="Seg_786" s="T424">dö</ta>
            <ta e="T426" id="Seg_787" s="T425">ne</ta>
            <ta e="T427" id="Seg_788" s="T426">kut-lAʔ</ta>
            <ta e="T428" id="Seg_789" s="T427">baʔbdə-bi</ta>
            <ta e="T429" id="Seg_790" s="T428">ami-m</ta>
            <ta e="T430" id="Seg_791" s="T429">kut-lAʔ</ta>
            <ta e="T431" id="Seg_792" s="T430">baʔbdə-bi</ta>
            <ta e="T432" id="Seg_793" s="T431">dĭgəttə</ta>
            <ta e="T433" id="Seg_794" s="T432">šo-bi</ta>
            <ta e="T434" id="Seg_795" s="T433">dĭ</ta>
            <ta e="T435" id="Seg_796" s="T434">ne-Tə</ta>
            <ta e="T436" id="Seg_797" s="T435">bazoʔ</ta>
            <ta e="T437" id="Seg_798" s="T436">tăn</ta>
            <ta e="T438" id="Seg_799" s="T437">ĭmbi</ta>
            <ta e="T439" id="Seg_800" s="T438">a-liA-l</ta>
            <ta e="T440" id="Seg_801" s="T439">măn</ta>
            <ta e="T441" id="Seg_802" s="T440">kut-labaʔ-bi-m</ta>
            <ta e="T442" id="Seg_803" s="T441">dĭ</ta>
            <ta e="T443" id="Seg_804" s="T442">ne</ta>
            <ta e="T444" id="Seg_805" s="T443">nörbə-liA</ta>
            <ta e="T445" id="Seg_806" s="T444">püʔbdə-t</ta>
            <ta e="T446" id="Seg_807" s="T445">püʔbdə-laʔbə-bi</ta>
            <ta e="T447" id="Seg_808" s="T446">dĭ</ta>
            <ta e="T448" id="Seg_809" s="T447">ne</ta>
            <ta e="T449" id="Seg_810" s="T448">bajʔkə-bə</ta>
            <ta e="T450" id="Seg_811" s="T449">bazoʔ</ta>
            <ta e="T451" id="Seg_812" s="T450">saj</ta>
            <ta e="T452" id="Seg_813" s="T451">hʼaʔ-luʔbdə-bi</ta>
            <ta e="T453" id="Seg_814" s="T452">ne-m</ta>
            <ta e="T454" id="Seg_815" s="T453">ešši-zAŋ-də-t-ziʔ</ta>
            <ta e="T455" id="Seg_816" s="T454">kut-labaʔ-bi</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T358" id="Seg_817" s="T357">two.[NOM.SG]</ta>
            <ta e="T359" id="Seg_818" s="T358">brother-DYA-DU</ta>
            <ta e="T360" id="Seg_819" s="T359">live-PST-3PL</ta>
            <ta e="T361" id="Seg_820" s="T360">woman.[NOM.SG]</ta>
            <ta e="T362" id="Seg_821" s="T361">two-COLL</ta>
            <ta e="T363" id="Seg_822" s="T362">child.[NOM.SG]</ta>
            <ta e="T364" id="Seg_823" s="T363">bring-PST.[3SG]</ta>
            <ta e="T365" id="Seg_824" s="T364">bring-CVB.ANT</ta>
            <ta e="T366" id="Seg_825" s="T365">be-PST-3PL</ta>
            <ta e="T367" id="Seg_826" s="T366">other-NOM/GEN.3SG</ta>
            <ta e="T368" id="Seg_827" s="T367">brother-GEN</ta>
            <ta e="T369" id="Seg_828" s="T368">many</ta>
            <ta e="T370" id="Seg_829" s="T369">be-PST.[3SG]</ta>
            <ta e="T371" id="Seg_830" s="T370">this-PL</ta>
            <ta e="T372" id="Seg_831" s="T371">hunt-CVB</ta>
            <ta e="T373" id="Seg_832" s="T372">go-PST-3PL</ta>
            <ta e="T374" id="Seg_833" s="T373">woman-PL-NOM/GEN/ACC.3PL</ta>
            <ta e="T375" id="Seg_834" s="T374">stay-CVB</ta>
            <ta e="T376" id="Seg_835" s="T375">stay-PST-3PL</ta>
            <ta e="T377" id="Seg_836" s="T376">evening-LOC.ADV</ta>
            <ta e="T378" id="Seg_837" s="T377">both.[NOM.SG]</ta>
            <ta e="T379" id="Seg_838" s="T378">eat-INF.LAT</ta>
            <ta e="T380" id="Seg_839" s="T379">sit.down-PST.[3SG]</ta>
            <ta e="T381" id="Seg_840" s="T380">soup-LAT/LOC.3SG</ta>
            <ta e="T382" id="Seg_841" s="T381">see-CVB</ta>
            <ta e="T383" id="Seg_842" s="T382">throw-PST.[3SG]</ta>
            <ta e="T384" id="Seg_843" s="T383">shadow.[NOM.SG]</ta>
            <ta e="T385" id="Seg_844" s="T384">maneater.[NOM.SG]</ta>
            <ta e="T386" id="Seg_845" s="T385">tent-LAT/LOC.3SG</ta>
            <ta e="T387" id="Seg_846" s="T386">top-LAT/LOC.3SG</ta>
            <ta e="T388" id="Seg_847" s="T387">climb-RES-PST.[3SG]</ta>
            <ta e="T389" id="Seg_848" s="T388">smoke.hole-LOC</ta>
            <ta e="T390" id="Seg_849" s="T389">look-CVB</ta>
            <ta e="T391" id="Seg_850" s="T390">this.[NOM.SG]</ta>
            <ta e="T392" id="Seg_851" s="T391">woman.[NOM.SG]</ta>
            <ta e="T393" id="Seg_852" s="T392">throw-MOM-PST.[3SG]</ta>
            <ta e="T394" id="Seg_853" s="T393">drink-INF.LAT</ta>
            <ta e="T395" id="Seg_854" s="T394">soup-ACC.3SG</ta>
            <ta e="T396" id="Seg_855" s="T395">hang.up-RES-PST.[3SG]</ta>
            <ta e="T397" id="Seg_856" s="T396">away</ta>
            <ta e="T398" id="Seg_857" s="T397">get.up.[3SG]</ta>
            <ta e="T399" id="Seg_858" s="T398">self</ta>
            <ta e="T400" id="Seg_859" s="T399">tree.[NOM.SG]</ta>
            <ta e="T401" id="Seg_860" s="T400">tree-VBLZ-INF.LAT</ta>
            <ta e="T402" id="Seg_861" s="T401">moist.[NOM.SG]</ta>
            <ta e="T403" id="Seg_862" s="T402">tree-PL</ta>
            <ta e="T404" id="Seg_863" s="T403">tree-VBLZ-PST.[3SG]</ta>
            <ta e="T405" id="Seg_864" s="T404">fire-LAT/LOC.3SG</ta>
            <ta e="T406" id="Seg_865" s="T405">put-PST.[3SG]</ta>
            <ta e="T407" id="Seg_866" s="T406">then</ta>
            <ta e="T408" id="Seg_867" s="T407">axe.[NOM.SG]</ta>
            <ta e="T409" id="Seg_868" s="T408">take-PST.[3SG]</ta>
            <ta e="T410" id="Seg_869" s="T409">tent.pole-GEN</ta>
            <ta e="T411" id="Seg_870" s="T410">base-LAT/LOC.3SG</ta>
            <ta e="T412" id="Seg_871" s="T411">sit.down-PST.[3SG]</ta>
            <ta e="T413" id="Seg_872" s="T412">maneater.[NOM.SG]</ta>
            <ta e="T414" id="Seg_873" s="T413">night-LOC.ADV</ta>
            <ta e="T415" id="Seg_874" s="T414">fall-CVB</ta>
            <ta e="T416" id="Seg_875" s="T415">come-PST.[3SG]</ta>
            <ta e="T417" id="Seg_876" s="T416">enter-PST.[3SG]</ta>
            <ta e="T418" id="Seg_877" s="T417">tent-EP-GEN</ta>
            <ta e="T419" id="Seg_878" s="T418">inside-LAT/LOC.3SG</ta>
            <ta e="T420" id="Seg_879" s="T419">fire-ACC</ta>
            <ta e="T421" id="Seg_880" s="T420">blow-PRS-3SG.O</ta>
            <ta e="T422" id="Seg_881" s="T421">blow-PRS-3SG.O</ta>
            <ta e="T423" id="Seg_882" s="T422">NEG</ta>
            <ta e="T424" id="Seg_883" s="T423">burn-PRS.[3SG]</ta>
            <ta e="T425" id="Seg_884" s="T424">that.[NOM.SG]</ta>
            <ta e="T426" id="Seg_885" s="T425">woman.[NOM.SG]</ta>
            <ta e="T427" id="Seg_886" s="T426">kill-CVB</ta>
            <ta e="T428" id="Seg_887" s="T427">throw-PST.[3SG]</ta>
            <ta e="T429" id="Seg_888" s="T428">other-ACC</ta>
            <ta e="T430" id="Seg_889" s="T429">kill-CVB</ta>
            <ta e="T431" id="Seg_890" s="T430">throw-PST.[3SG]</ta>
            <ta e="T432" id="Seg_891" s="T431">then</ta>
            <ta e="T433" id="Seg_892" s="T432">come-PST.[3SG]</ta>
            <ta e="T434" id="Seg_893" s="T433">this.[NOM.SG]</ta>
            <ta e="T435" id="Seg_894" s="T434">woman-LAT</ta>
            <ta e="T436" id="Seg_895" s="T435">again</ta>
            <ta e="T437" id="Seg_896" s="T436">you.NOM</ta>
            <ta e="T438" id="Seg_897" s="T437">what</ta>
            <ta e="T439" id="Seg_898" s="T438">make-PRS-2SG</ta>
            <ta e="T440" id="Seg_899" s="T439">I.NOM</ta>
            <ta e="T441" id="Seg_900" s="T440">kill-RES-PST-1SG</ta>
            <ta e="T442" id="Seg_901" s="T441">this.[NOM.SG]</ta>
            <ta e="T443" id="Seg_902" s="T442">woman.[NOM.SG]</ta>
            <ta e="T444" id="Seg_903" s="T443">tell-PRS.[3SG]</ta>
            <ta e="T445" id="Seg_904" s="T444">blow-IMP.2SG.O</ta>
            <ta e="T446" id="Seg_905" s="T445">blow-DUR-PST.[3SG]</ta>
            <ta e="T447" id="Seg_906" s="T446">this.[NOM.SG]</ta>
            <ta e="T448" id="Seg_907" s="T447">woman.[NOM.SG]</ta>
            <ta e="T449" id="Seg_908" s="T448">throat-ACC.3SG</ta>
            <ta e="T450" id="Seg_909" s="T449">again</ta>
            <ta e="T451" id="Seg_910" s="T450">off</ta>
            <ta e="T452" id="Seg_911" s="T451">cut-MOM-PST.[3SG]</ta>
            <ta e="T453" id="Seg_912" s="T452">woman-ACC</ta>
            <ta e="T454" id="Seg_913" s="T453">child-PL-NOM/GEN/ACC.3SG-3SG-INS</ta>
            <ta e="T455" id="Seg_914" s="T454">kill-RES-PST.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T358" id="Seg_915" s="T357">два.[NOM.SG]</ta>
            <ta e="T359" id="Seg_916" s="T358">брат-DYA-DU</ta>
            <ta e="T360" id="Seg_917" s="T359">жить-PST-3PL</ta>
            <ta e="T361" id="Seg_918" s="T360">женщина.[NOM.SG]</ta>
            <ta e="T362" id="Seg_919" s="T361">два-COLL</ta>
            <ta e="T363" id="Seg_920" s="T362">ребенок.[NOM.SG]</ta>
            <ta e="T364" id="Seg_921" s="T363">принести-PST.[3SG]</ta>
            <ta e="T365" id="Seg_922" s="T364">принести-CVB.ANT</ta>
            <ta e="T366" id="Seg_923" s="T365">быть-PST-3PL</ta>
            <ta e="T367" id="Seg_924" s="T366">другой-NOM/GEN.3SG</ta>
            <ta e="T368" id="Seg_925" s="T367">брат-GEN</ta>
            <ta e="T369" id="Seg_926" s="T368">много</ta>
            <ta e="T370" id="Seg_927" s="T369">быть-PST.[3SG]</ta>
            <ta e="T371" id="Seg_928" s="T370">этот-PL</ta>
            <ta e="T372" id="Seg_929" s="T371">охотиться-CVB</ta>
            <ta e="T373" id="Seg_930" s="T372">пойти-PST-3PL</ta>
            <ta e="T374" id="Seg_931" s="T373">женщина-PL-NOM/GEN/ACC.3PL</ta>
            <ta e="T375" id="Seg_932" s="T374">остаться-CVB</ta>
            <ta e="T376" id="Seg_933" s="T375">остаться-PST-3PL</ta>
            <ta e="T377" id="Seg_934" s="T376">вечер-LOC.ADV</ta>
            <ta e="T378" id="Seg_935" s="T377">оба.[NOM.SG]</ta>
            <ta e="T379" id="Seg_936" s="T378">есть-INF.LAT</ta>
            <ta e="T380" id="Seg_937" s="T379">сесть-PST.[3SG]</ta>
            <ta e="T381" id="Seg_938" s="T380">суп-LAT/LOC.3SG</ta>
            <ta e="T382" id="Seg_939" s="T381">видеть-CVB</ta>
            <ta e="T383" id="Seg_940" s="T382">бросить-PST.[3SG]</ta>
            <ta e="T384" id="Seg_941" s="T383">тень.[NOM.SG]</ta>
            <ta e="T385" id="Seg_942" s="T384">людоед.[NOM.SG]</ta>
            <ta e="T386" id="Seg_943" s="T385">чум-LAT/LOC.3SG</ta>
            <ta e="T387" id="Seg_944" s="T386">верх-LAT/LOC.3SG</ta>
            <ta e="T388" id="Seg_945" s="T387">влезать-RES-PST.[3SG]</ta>
            <ta e="T389" id="Seg_946" s="T388">дымовое.отверстие-LOC</ta>
            <ta e="T390" id="Seg_947" s="T389">смотреть-CVB</ta>
            <ta e="T391" id="Seg_948" s="T390">этот.[NOM.SG]</ta>
            <ta e="T392" id="Seg_949" s="T391">женщина.[NOM.SG]</ta>
            <ta e="T393" id="Seg_950" s="T392">бросить-MOM-PST.[3SG]</ta>
            <ta e="T394" id="Seg_951" s="T393">пить-INF.LAT</ta>
            <ta e="T395" id="Seg_952" s="T394">суп-ACC.3SG</ta>
            <ta e="T396" id="Seg_953" s="T395">вешать-RES-PST.[3SG]</ta>
            <ta e="T397" id="Seg_954" s="T396">прочь</ta>
            <ta e="T398" id="Seg_955" s="T397">встать.[3SG]</ta>
            <ta e="T399" id="Seg_956" s="T398">сам</ta>
            <ta e="T400" id="Seg_957" s="T399">дерево.[NOM.SG]</ta>
            <ta e="T401" id="Seg_958" s="T400">дерево-VBLZ-INF.LAT</ta>
            <ta e="T402" id="Seg_959" s="T401">мокрый.[NOM.SG]</ta>
            <ta e="T403" id="Seg_960" s="T402">дерево-PL</ta>
            <ta e="T404" id="Seg_961" s="T403">дерево-VBLZ-PST.[3SG]</ta>
            <ta e="T405" id="Seg_962" s="T404">огонь-LAT/LOC.3SG</ta>
            <ta e="T406" id="Seg_963" s="T405">класть-PST.[3SG]</ta>
            <ta e="T407" id="Seg_964" s="T406">тогда</ta>
            <ta e="T408" id="Seg_965" s="T407">топор.[NOM.SG]</ta>
            <ta e="T409" id="Seg_966" s="T408">взять-PST.[3SG]</ta>
            <ta e="T410" id="Seg_967" s="T409">шест-GEN</ta>
            <ta e="T411" id="Seg_968" s="T410">основа-LAT/LOC.3SG</ta>
            <ta e="T412" id="Seg_969" s="T411">сесть-PST.[3SG]</ta>
            <ta e="T413" id="Seg_970" s="T412">людоед.[NOM.SG]</ta>
            <ta e="T414" id="Seg_971" s="T413">ночь-LOC.ADV</ta>
            <ta e="T415" id="Seg_972" s="T414">упасть-CVB</ta>
            <ta e="T416" id="Seg_973" s="T415">прийти-PST.[3SG]</ta>
            <ta e="T417" id="Seg_974" s="T416">войти-PST.[3SG]</ta>
            <ta e="T418" id="Seg_975" s="T417">чум-EP-GEN</ta>
            <ta e="T419" id="Seg_976" s="T418">внутри-LAT/LOC.3SG</ta>
            <ta e="T420" id="Seg_977" s="T419">огонь-ACC</ta>
            <ta e="T421" id="Seg_978" s="T420">дуть-PRS-3SG.O</ta>
            <ta e="T422" id="Seg_979" s="T421">дуть-PRS-3SG.O</ta>
            <ta e="T423" id="Seg_980" s="T422">NEG</ta>
            <ta e="T424" id="Seg_981" s="T423">гореть-PRS.[3SG]</ta>
            <ta e="T425" id="Seg_982" s="T424">тот.[NOM.SG]</ta>
            <ta e="T426" id="Seg_983" s="T425">женщина.[NOM.SG]</ta>
            <ta e="T427" id="Seg_984" s="T426">убить-CVB</ta>
            <ta e="T428" id="Seg_985" s="T427">бросить-PST.[3SG]</ta>
            <ta e="T429" id="Seg_986" s="T428">другой-ACC</ta>
            <ta e="T430" id="Seg_987" s="T429">убить-CVB</ta>
            <ta e="T431" id="Seg_988" s="T430">бросить-PST.[3SG]</ta>
            <ta e="T432" id="Seg_989" s="T431">тогда</ta>
            <ta e="T433" id="Seg_990" s="T432">прийти-PST.[3SG]</ta>
            <ta e="T434" id="Seg_991" s="T433">этот.[NOM.SG]</ta>
            <ta e="T435" id="Seg_992" s="T434">женщина-LAT</ta>
            <ta e="T436" id="Seg_993" s="T435">опять</ta>
            <ta e="T437" id="Seg_994" s="T436">ты.NOM</ta>
            <ta e="T438" id="Seg_995" s="T437">что</ta>
            <ta e="T439" id="Seg_996" s="T438">делать-PRS-2SG</ta>
            <ta e="T440" id="Seg_997" s="T439">я.NOM</ta>
            <ta e="T441" id="Seg_998" s="T440">убить-RES-PST-1SG</ta>
            <ta e="T442" id="Seg_999" s="T441">этот.[NOM.SG]</ta>
            <ta e="T443" id="Seg_1000" s="T442">женщина.[NOM.SG]</ta>
            <ta e="T444" id="Seg_1001" s="T443">сказать-PRS.[3SG]</ta>
            <ta e="T445" id="Seg_1002" s="T444">дуть-IMP.2SG.O</ta>
            <ta e="T446" id="Seg_1003" s="T445">дуть-DUR-PST.[3SG]</ta>
            <ta e="T447" id="Seg_1004" s="T446">этот.[NOM.SG]</ta>
            <ta e="T448" id="Seg_1005" s="T447">женщина.[NOM.SG]</ta>
            <ta e="T449" id="Seg_1006" s="T448">горло-ACC.3SG</ta>
            <ta e="T450" id="Seg_1007" s="T449">опять</ta>
            <ta e="T451" id="Seg_1008" s="T450">от</ta>
            <ta e="T452" id="Seg_1009" s="T451">резать-MOM-PST.[3SG]</ta>
            <ta e="T453" id="Seg_1010" s="T452">женщина-ACC</ta>
            <ta e="T454" id="Seg_1011" s="T453">ребенок-PL-NOM/GEN/ACC.3SG-3SG-INS</ta>
            <ta e="T455" id="Seg_1012" s="T454">убить-RES-PST.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T358" id="Seg_1013" s="T357">num-n:case</ta>
            <ta e="T359" id="Seg_1014" s="T358">n-n&gt;n-n:num</ta>
            <ta e="T360" id="Seg_1015" s="T359">v-v:tense-v:pn</ta>
            <ta e="T361" id="Seg_1016" s="T360">n-n:case</ta>
            <ta e="T362" id="Seg_1017" s="T361">num-num&gt;num</ta>
            <ta e="T363" id="Seg_1018" s="T362">n-n:case</ta>
            <ta e="T364" id="Seg_1019" s="T363">v-v:tense-v:pn</ta>
            <ta e="T365" id="Seg_1020" s="T364">v-v:n.fin</ta>
            <ta e="T366" id="Seg_1021" s="T365">v-v:tense-v:pn</ta>
            <ta e="T367" id="Seg_1022" s="T366">adj-n:case.poss</ta>
            <ta e="T368" id="Seg_1023" s="T367">n-n:case</ta>
            <ta e="T369" id="Seg_1024" s="T368">quant</ta>
            <ta e="T370" id="Seg_1025" s="T369">v-v:tense-v:pn</ta>
            <ta e="T371" id="Seg_1026" s="T370">dempro-n:num</ta>
            <ta e="T372" id="Seg_1027" s="T371">v-v:n.fin</ta>
            <ta e="T373" id="Seg_1028" s="T372">v-v:tense-v:pn</ta>
            <ta e="T374" id="Seg_1029" s="T373">n-n:num-n:case.poss</ta>
            <ta e="T375" id="Seg_1030" s="T374">v-v:n.fin</ta>
            <ta e="T376" id="Seg_1031" s="T375">v-v:tense-v:pn</ta>
            <ta e="T377" id="Seg_1032" s="T376">n-adv:case</ta>
            <ta e="T378" id="Seg_1033" s="T377">num-n:case</ta>
            <ta e="T379" id="Seg_1034" s="T378">v-v:n.fin</ta>
            <ta e="T380" id="Seg_1035" s="T379">v-v:tense-v:pn</ta>
            <ta e="T381" id="Seg_1036" s="T380">n-n:case.poss</ta>
            <ta e="T382" id="Seg_1037" s="T381">v-v:n.fin</ta>
            <ta e="T383" id="Seg_1038" s="T382">v-v:tense-v:pn</ta>
            <ta e="T384" id="Seg_1039" s="T383">n-n:case</ta>
            <ta e="T385" id="Seg_1040" s="T384">n-n:case</ta>
            <ta e="T386" id="Seg_1041" s="T385">n-n:case.poss</ta>
            <ta e="T387" id="Seg_1042" s="T386">n-n:case.poss</ta>
            <ta e="T388" id="Seg_1043" s="T387">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T389" id="Seg_1044" s="T388">n-n:case</ta>
            <ta e="T390" id="Seg_1045" s="T389">v-v:n.fin</ta>
            <ta e="T391" id="Seg_1046" s="T390">dempro-n:case</ta>
            <ta e="T392" id="Seg_1047" s="T391">n-n:case</ta>
            <ta e="T393" id="Seg_1048" s="T392">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T394" id="Seg_1049" s="T393">v-v:n.fin</ta>
            <ta e="T395" id="Seg_1050" s="T394">n-n:case.poss</ta>
            <ta e="T396" id="Seg_1051" s="T395">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T397" id="Seg_1052" s="T396">adv</ta>
            <ta e="T398" id="Seg_1053" s="T397">v-v:pn</ta>
            <ta e="T399" id="Seg_1054" s="T398">refl</ta>
            <ta e="T400" id="Seg_1055" s="T399">n-n:case</ta>
            <ta e="T401" id="Seg_1056" s="T400">n-n&gt;v-v:n.fin</ta>
            <ta e="T402" id="Seg_1057" s="T401">adj-n:case</ta>
            <ta e="T403" id="Seg_1058" s="T402">n-n:num</ta>
            <ta e="T404" id="Seg_1059" s="T403">n-n&gt;v-v:tense-v:pn</ta>
            <ta e="T405" id="Seg_1060" s="T404">n-n:case.poss</ta>
            <ta e="T406" id="Seg_1061" s="T405">v-v:tense-v:pn</ta>
            <ta e="T407" id="Seg_1062" s="T406">adv</ta>
            <ta e="T408" id="Seg_1063" s="T407">n-n:case</ta>
            <ta e="T409" id="Seg_1064" s="T408">v-v:tense-v:pn</ta>
            <ta e="T410" id="Seg_1065" s="T409">n-n:case</ta>
            <ta e="T411" id="Seg_1066" s="T410">n-n:case.poss</ta>
            <ta e="T412" id="Seg_1067" s="T411">v-v:tense-v:pn</ta>
            <ta e="T413" id="Seg_1068" s="T412">n-n:case</ta>
            <ta e="T414" id="Seg_1069" s="T413">n-adv:case</ta>
            <ta e="T415" id="Seg_1070" s="T414">v-v:n.fin</ta>
            <ta e="T416" id="Seg_1071" s="T415">v-v:tense-v:pn</ta>
            <ta e="T417" id="Seg_1072" s="T416">v-v:tense-v:pn</ta>
            <ta e="T418" id="Seg_1073" s="T417">n-n:ins-n:case</ta>
            <ta e="T419" id="Seg_1074" s="T418">n-n:case.poss</ta>
            <ta e="T420" id="Seg_1075" s="T419">n-n:case</ta>
            <ta e="T421" id="Seg_1076" s="T420">v-v:tense-v:pn</ta>
            <ta e="T422" id="Seg_1077" s="T421">v-v:tense-v:pn</ta>
            <ta e="T423" id="Seg_1078" s="T422">ptcl</ta>
            <ta e="T424" id="Seg_1079" s="T423">v-v:tense-v:pn</ta>
            <ta e="T425" id="Seg_1080" s="T424">dempro-n:case</ta>
            <ta e="T426" id="Seg_1081" s="T425">n-n:case</ta>
            <ta e="T427" id="Seg_1082" s="T426">v-v:n.fin</ta>
            <ta e="T428" id="Seg_1083" s="T427">v-v:tense-v:pn</ta>
            <ta e="T429" id="Seg_1084" s="T428">adj-n:case</ta>
            <ta e="T430" id="Seg_1085" s="T429">v-v:n.fin</ta>
            <ta e="T431" id="Seg_1086" s="T430">v-v:tense-v:pn</ta>
            <ta e="T432" id="Seg_1087" s="T431">adv</ta>
            <ta e="T433" id="Seg_1088" s="T432">v-v:tense-v:pn</ta>
            <ta e="T434" id="Seg_1089" s="T433">dempro-n:case</ta>
            <ta e="T435" id="Seg_1090" s="T434">n-n:case</ta>
            <ta e="T436" id="Seg_1091" s="T435">adv</ta>
            <ta e="T437" id="Seg_1092" s="T436">pers</ta>
            <ta e="T438" id="Seg_1093" s="T437">que</ta>
            <ta e="T439" id="Seg_1094" s="T438">v-v:tense-v:pn</ta>
            <ta e="T440" id="Seg_1095" s="T439">pers</ta>
            <ta e="T441" id="Seg_1096" s="T440">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T442" id="Seg_1097" s="T441">dempro-n:case</ta>
            <ta e="T443" id="Seg_1098" s="T442">n-n:case</ta>
            <ta e="T444" id="Seg_1099" s="T443">v-v:tense-v:pn</ta>
            <ta e="T445" id="Seg_1100" s="T444">v-v:mood.pn</ta>
            <ta e="T446" id="Seg_1101" s="T445">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T447" id="Seg_1102" s="T446">dempro-n:case</ta>
            <ta e="T448" id="Seg_1103" s="T447">n-n:case</ta>
            <ta e="T449" id="Seg_1104" s="T448">n-n:case.poss</ta>
            <ta e="T450" id="Seg_1105" s="T449">adv</ta>
            <ta e="T451" id="Seg_1106" s="T450">adv</ta>
            <ta e="T452" id="Seg_1107" s="T451">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T453" id="Seg_1108" s="T452">n-n:case</ta>
            <ta e="T454" id="Seg_1109" s="T453">n-n:num-n:case.poss-n:case.poss-n:case</ta>
            <ta e="T455" id="Seg_1110" s="T454">v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T358" id="Seg_1111" s="T357">num</ta>
            <ta e="T359" id="Seg_1112" s="T358">n</ta>
            <ta e="T360" id="Seg_1113" s="T359">v</ta>
            <ta e="T361" id="Seg_1114" s="T360">n</ta>
            <ta e="T362" id="Seg_1115" s="T361">num</ta>
            <ta e="T363" id="Seg_1116" s="T362">n</ta>
            <ta e="T364" id="Seg_1117" s="T363">v</ta>
            <ta e="T365" id="Seg_1118" s="T364">v</ta>
            <ta e="T366" id="Seg_1119" s="T365">v</ta>
            <ta e="T367" id="Seg_1120" s="T366">adj</ta>
            <ta e="T368" id="Seg_1121" s="T367">n</ta>
            <ta e="T369" id="Seg_1122" s="T368">quant</ta>
            <ta e="T370" id="Seg_1123" s="T369">v</ta>
            <ta e="T371" id="Seg_1124" s="T370">dempro</ta>
            <ta e="T372" id="Seg_1125" s="T371">v</ta>
            <ta e="T373" id="Seg_1126" s="T372">v</ta>
            <ta e="T374" id="Seg_1127" s="T373">n</ta>
            <ta e="T375" id="Seg_1128" s="T374">v</ta>
            <ta e="T376" id="Seg_1129" s="T375">v</ta>
            <ta e="T377" id="Seg_1130" s="T376">adv</ta>
            <ta e="T378" id="Seg_1131" s="T377">num</ta>
            <ta e="T379" id="Seg_1132" s="T378">v</ta>
            <ta e="T380" id="Seg_1133" s="T379">v</ta>
            <ta e="T381" id="Seg_1134" s="T380">n</ta>
            <ta e="T382" id="Seg_1135" s="T381">v</ta>
            <ta e="T383" id="Seg_1136" s="T382">v</ta>
            <ta e="T384" id="Seg_1137" s="T383">n</ta>
            <ta e="T385" id="Seg_1138" s="T384">n</ta>
            <ta e="T386" id="Seg_1139" s="T385">n</ta>
            <ta e="T387" id="Seg_1140" s="T386">n</ta>
            <ta e="T388" id="Seg_1141" s="T387">v</ta>
            <ta e="T389" id="Seg_1142" s="T388">n</ta>
            <ta e="T390" id="Seg_1143" s="T389">v</ta>
            <ta e="T391" id="Seg_1144" s="T390">dempro</ta>
            <ta e="T392" id="Seg_1145" s="T391">n</ta>
            <ta e="T393" id="Seg_1146" s="T392">v</ta>
            <ta e="T394" id="Seg_1147" s="T393">v</ta>
            <ta e="T395" id="Seg_1148" s="T394">n</ta>
            <ta e="T396" id="Seg_1149" s="T395">v</ta>
            <ta e="T397" id="Seg_1150" s="T396">adv</ta>
            <ta e="T398" id="Seg_1151" s="T397">v</ta>
            <ta e="T399" id="Seg_1152" s="T398">refl</ta>
            <ta e="T400" id="Seg_1153" s="T399">n</ta>
            <ta e="T401" id="Seg_1154" s="T400">v</ta>
            <ta e="T402" id="Seg_1155" s="T401">adj</ta>
            <ta e="T403" id="Seg_1156" s="T402">n</ta>
            <ta e="T404" id="Seg_1157" s="T403">v</ta>
            <ta e="T405" id="Seg_1158" s="T404">n</ta>
            <ta e="T406" id="Seg_1159" s="T405">v</ta>
            <ta e="T407" id="Seg_1160" s="T406">adv</ta>
            <ta e="T408" id="Seg_1161" s="T407">n</ta>
            <ta e="T409" id="Seg_1162" s="T408">v</ta>
            <ta e="T410" id="Seg_1163" s="T409">n</ta>
            <ta e="T411" id="Seg_1164" s="T410">n</ta>
            <ta e="T412" id="Seg_1165" s="T411">v</ta>
            <ta e="T413" id="Seg_1166" s="T412">n</ta>
            <ta e="T414" id="Seg_1167" s="T413">adv</ta>
            <ta e="T415" id="Seg_1168" s="T414">v</ta>
            <ta e="T416" id="Seg_1169" s="T415">v</ta>
            <ta e="T417" id="Seg_1170" s="T416">v</ta>
            <ta e="T418" id="Seg_1171" s="T417">n</ta>
            <ta e="T419" id="Seg_1172" s="T418">n</ta>
            <ta e="T420" id="Seg_1173" s="T419">n</ta>
            <ta e="T421" id="Seg_1174" s="T420">v</ta>
            <ta e="T422" id="Seg_1175" s="T421">v</ta>
            <ta e="T423" id="Seg_1176" s="T422">ptcl</ta>
            <ta e="T424" id="Seg_1177" s="T423">v</ta>
            <ta e="T425" id="Seg_1178" s="T424">dempro</ta>
            <ta e="T426" id="Seg_1179" s="T425">n</ta>
            <ta e="T427" id="Seg_1180" s="T426">v</ta>
            <ta e="T428" id="Seg_1181" s="T427">v</ta>
            <ta e="T429" id="Seg_1182" s="T428">adj</ta>
            <ta e="T430" id="Seg_1183" s="T429">v</ta>
            <ta e="T431" id="Seg_1184" s="T430">v</ta>
            <ta e="T432" id="Seg_1185" s="T431">adv</ta>
            <ta e="T433" id="Seg_1186" s="T432">v</ta>
            <ta e="T434" id="Seg_1187" s="T433">dempro</ta>
            <ta e="T435" id="Seg_1188" s="T434">n</ta>
            <ta e="T436" id="Seg_1189" s="T435">adv</ta>
            <ta e="T437" id="Seg_1190" s="T436">pers</ta>
            <ta e="T438" id="Seg_1191" s="T437">que</ta>
            <ta e="T439" id="Seg_1192" s="T438">v</ta>
            <ta e="T440" id="Seg_1193" s="T439">pers</ta>
            <ta e="T441" id="Seg_1194" s="T440">v</ta>
            <ta e="T442" id="Seg_1195" s="T441">dempro</ta>
            <ta e="T443" id="Seg_1196" s="T442">n</ta>
            <ta e="T444" id="Seg_1197" s="T443">v</ta>
            <ta e="T445" id="Seg_1198" s="T444">v</ta>
            <ta e="T446" id="Seg_1199" s="T445">v</ta>
            <ta e="T447" id="Seg_1200" s="T446">dempro</ta>
            <ta e="T448" id="Seg_1201" s="T447">n</ta>
            <ta e="T449" id="Seg_1202" s="T448">n</ta>
            <ta e="T450" id="Seg_1203" s="T449">adv</ta>
            <ta e="T451" id="Seg_1204" s="T450">adv</ta>
            <ta e="T452" id="Seg_1205" s="T451">v</ta>
            <ta e="T453" id="Seg_1206" s="T452">n</ta>
            <ta e="T454" id="Seg_1207" s="T453">n</ta>
            <ta e="T455" id="Seg_1208" s="T454">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T359" id="Seg_1209" s="T358">np.h:Th</ta>
            <ta e="T361" id="Seg_1210" s="T360">np.h:A</ta>
            <ta e="T363" id="Seg_1211" s="T362">np.h:P</ta>
            <ta e="T368" id="Seg_1212" s="T367">np.h:Poss</ta>
            <ta e="T371" id="Seg_1213" s="T370">pro.h:A</ta>
            <ta e="T374" id="Seg_1214" s="T373">np.h:Th 0.3.h:Poss</ta>
            <ta e="T377" id="Seg_1215" s="T376">adv:Time</ta>
            <ta e="T378" id="Seg_1216" s="T377">pro.h:A</ta>
            <ta e="T381" id="Seg_1217" s="T380">np:L 0.3.h:Poss</ta>
            <ta e="T382" id="Seg_1218" s="T381">0.3.h:E</ta>
            <ta e="T384" id="Seg_1219" s="T383">np:Th</ta>
            <ta e="T385" id="Seg_1220" s="T384">np.h:A</ta>
            <ta e="T387" id="Seg_1221" s="T385">np:G </ta>
            <ta e="T389" id="Seg_1222" s="T388">np:L</ta>
            <ta e="T390" id="Seg_1223" s="T389">0.3.h:A</ta>
            <ta e="T392" id="Seg_1224" s="T391">np.h:A</ta>
            <ta e="T395" id="Seg_1225" s="T394">np:Th 0.3.h:Poss</ta>
            <ta e="T396" id="Seg_1226" s="T395">0.3.h:A</ta>
            <ta e="T398" id="Seg_1227" s="T397">0.3.h:A</ta>
            <ta e="T399" id="Seg_1228" s="T398">0.3.h:Poss</ta>
            <ta e="T400" id="Seg_1229" s="T399">np:P</ta>
            <ta e="T403" id="Seg_1230" s="T402">np:P</ta>
            <ta e="T404" id="Seg_1231" s="T403">0.3.h:A</ta>
            <ta e="T405" id="Seg_1232" s="T404">np:G 0.3.h:Poss</ta>
            <ta e="T406" id="Seg_1233" s="T405">0.3.h:A</ta>
            <ta e="T407" id="Seg_1234" s="T406">adv:Time</ta>
            <ta e="T408" id="Seg_1235" s="T407">np:Th</ta>
            <ta e="T409" id="Seg_1236" s="T408">0.3.h:A</ta>
            <ta e="T410" id="Seg_1237" s="T409">np:Poss</ta>
            <ta e="T411" id="Seg_1238" s="T410">np:G</ta>
            <ta e="T412" id="Seg_1239" s="T411">0.3.h:A</ta>
            <ta e="T413" id="Seg_1240" s="T412">np.h:A</ta>
            <ta e="T414" id="Seg_1241" s="T413">adv:Time</ta>
            <ta e="T417" id="Seg_1242" s="T416">0.3.h:A</ta>
            <ta e="T419" id="Seg_1243" s="T417">pp:G</ta>
            <ta e="T420" id="Seg_1244" s="T419">np:Th</ta>
            <ta e="T421" id="Seg_1245" s="T420">0.3.h:A</ta>
            <ta e="T422" id="Seg_1246" s="T421">0.3.h:A</ta>
            <ta e="T424" id="Seg_1247" s="T423">0.3:Th</ta>
            <ta e="T426" id="Seg_1248" s="T425">np.h:P</ta>
            <ta e="T428" id="Seg_1249" s="T427">0.3.h:A</ta>
            <ta e="T429" id="Seg_1250" s="T428">pro.h:P</ta>
            <ta e="T431" id="Seg_1251" s="T430">0.3.h:A</ta>
            <ta e="T432" id="Seg_1252" s="T431">adv:Time</ta>
            <ta e="T433" id="Seg_1253" s="T432">0.3.h:A</ta>
            <ta e="T435" id="Seg_1254" s="T434">np:G</ta>
            <ta e="T436" id="Seg_1255" s="T435">adv:Time</ta>
            <ta e="T437" id="Seg_1256" s="T436">pro.h:A</ta>
            <ta e="T440" id="Seg_1257" s="T439">pro.h:A</ta>
            <ta e="T443" id="Seg_1258" s="T442">np.h:A</ta>
            <ta e="T445" id="Seg_1259" s="T444">0.2.h:A 0.3:Th</ta>
            <ta e="T446" id="Seg_1260" s="T445">0.3.h:A</ta>
            <ta e="T448" id="Seg_1261" s="T447">np.h:A</ta>
            <ta e="T449" id="Seg_1262" s="T448">np:P 0.3.h:Poss</ta>
            <ta e="T450" id="Seg_1263" s="T449">adv:Time</ta>
            <ta e="T453" id="Seg_1264" s="T452">np.h:P</ta>
            <ta e="T454" id="Seg_1265" s="T453">np.h:Com 0.3.h:Poss</ta>
            <ta e="T455" id="Seg_1266" s="T454">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T359" id="Seg_1267" s="T358">np.h:S</ta>
            <ta e="T360" id="Seg_1268" s="T359">v:pred</ta>
            <ta e="T361" id="Seg_1269" s="T360">np.h:S</ta>
            <ta e="T363" id="Seg_1270" s="T362">np.h:O</ta>
            <ta e="T364" id="Seg_1271" s="T363">v:pred</ta>
            <ta e="T365" id="Seg_1272" s="T364">conv:pred</ta>
            <ta e="T370" id="Seg_1273" s="T369">v:pred 0.3.h:S</ta>
            <ta e="T371" id="Seg_1274" s="T370">pro.h:S</ta>
            <ta e="T372" id="Seg_1275" s="T371">conv:pred</ta>
            <ta e="T373" id="Seg_1276" s="T372">v:pred</ta>
            <ta e="T374" id="Seg_1277" s="T373">np.h:S</ta>
            <ta e="T375" id="Seg_1278" s="T374">conv:pred</ta>
            <ta e="T376" id="Seg_1279" s="T375">v:pred</ta>
            <ta e="T378" id="Seg_1280" s="T377">pro.h:S</ta>
            <ta e="T379" id="Seg_1281" s="T378">s:purp</ta>
            <ta e="T380" id="Seg_1282" s="T379">v:pred</ta>
            <ta e="T382" id="Seg_1283" s="T381">conv:pred</ta>
            <ta e="T383" id="Seg_1284" s="T382">v:pred 0.3.h:S</ta>
            <ta e="T384" id="Seg_1285" s="T383">np:O</ta>
            <ta e="T385" id="Seg_1286" s="T384">np.h:S</ta>
            <ta e="T388" id="Seg_1287" s="T387">v:pred</ta>
            <ta e="T390" id="Seg_1288" s="T388">s:temp</ta>
            <ta e="T392" id="Seg_1289" s="T391">np.h:S</ta>
            <ta e="T393" id="Seg_1290" s="T392">v:pred</ta>
            <ta e="T394" id="Seg_1291" s="T393">s:purp</ta>
            <ta e="T395" id="Seg_1292" s="T394">np:O</ta>
            <ta e="T396" id="Seg_1293" s="T395">v:pred 0.3.h:S</ta>
            <ta e="T398" id="Seg_1294" s="T397">v:pred 0.3.h:S</ta>
            <ta e="T401" id="Seg_1295" s="T398">s:purp</ta>
            <ta e="T403" id="Seg_1296" s="T402">np:O</ta>
            <ta e="T404" id="Seg_1297" s="T403">v:pred 0.3.h:S</ta>
            <ta e="T406" id="Seg_1298" s="T405">v:pred 0.3.h:S</ta>
            <ta e="T408" id="Seg_1299" s="T407">np:O</ta>
            <ta e="T409" id="Seg_1300" s="T408">v:pred 0.3.h:S</ta>
            <ta e="T412" id="Seg_1301" s="T411">v:pred 0.3.h:S</ta>
            <ta e="T413" id="Seg_1302" s="T412">np.h:S</ta>
            <ta e="T415" id="Seg_1303" s="T414">conv:pred</ta>
            <ta e="T416" id="Seg_1304" s="T415">v:pred</ta>
            <ta e="T417" id="Seg_1305" s="T416">v:pred 0.3:S</ta>
            <ta e="T420" id="Seg_1306" s="T419">np:O</ta>
            <ta e="T421" id="Seg_1307" s="T420">v:pred 0.3:S</ta>
            <ta e="T422" id="Seg_1308" s="T421">v:pred 0.3:S</ta>
            <ta e="T423" id="Seg_1309" s="T422">ptcl.neg</ta>
            <ta e="T424" id="Seg_1310" s="T423">v:pred 0.3:S</ta>
            <ta e="T426" id="Seg_1311" s="T425">np.h:O</ta>
            <ta e="T427" id="Seg_1312" s="T426">conv:pred</ta>
            <ta e="T428" id="Seg_1313" s="T427">v:pred 0.3:S</ta>
            <ta e="T429" id="Seg_1314" s="T428">pro.h:O</ta>
            <ta e="T430" id="Seg_1315" s="T429">conv:pred</ta>
            <ta e="T431" id="Seg_1316" s="T430">v:pred 0.3:S</ta>
            <ta e="T433" id="Seg_1317" s="T432">v:pred 0.3:S</ta>
            <ta e="T437" id="Seg_1318" s="T436">pro:S</ta>
            <ta e="T438" id="Seg_1319" s="T437">pro:O</ta>
            <ta e="T439" id="Seg_1320" s="T438">v:pred</ta>
            <ta e="T440" id="Seg_1321" s="T439">pro.h:S</ta>
            <ta e="T441" id="Seg_1322" s="T440">v:pred</ta>
            <ta e="T443" id="Seg_1323" s="T442">np.h:S</ta>
            <ta e="T444" id="Seg_1324" s="T443">v:pred</ta>
            <ta e="T445" id="Seg_1325" s="T444">v:pred 0.2:S 0.3:O</ta>
            <ta e="T446" id="Seg_1326" s="T445">v:pred 0.3:S</ta>
            <ta e="T448" id="Seg_1327" s="T447">np.h:S</ta>
            <ta e="T449" id="Seg_1328" s="T448">np:O</ta>
            <ta e="T452" id="Seg_1329" s="T451">v:pred</ta>
            <ta e="T453" id="Seg_1330" s="T452">np.h:O</ta>
            <ta e="T455" id="Seg_1331" s="T454">v:pred 0.3:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T359" id="Seg_1332" s="T358">new </ta>
            <ta e="T361" id="Seg_1333" s="T360">new</ta>
            <ta e="T363" id="Seg_1334" s="T362">new </ta>
            <ta e="T368" id="Seg_1335" s="T367">accs-inf </ta>
            <ta e="T371" id="Seg_1336" s="T370">giv-inactive </ta>
            <ta e="T374" id="Seg_1337" s="T373">accs-aggr </ta>
            <ta e="T377" id="Seg_1338" s="T376">accs-gen</ta>
            <ta e="T378" id="Seg_1339" s="T377">giv-active </ta>
            <ta e="T381" id="Seg_1340" s="T380">accs-inf </ta>
            <ta e="T383" id="Seg_1341" s="T382">0.giv-active</ta>
            <ta e="T384" id="Seg_1342" s="T383">new </ta>
            <ta e="T385" id="Seg_1343" s="T384">new </ta>
            <ta e="T387" id="Seg_1344" s="T386">accs-inf </ta>
            <ta e="T389" id="Seg_1345" s="T388">accs-inf </ta>
            <ta e="T392" id="Seg_1346" s="T391">giv-inactive </ta>
            <ta e="T395" id="Seg_1347" s="T394">giv-inactive</ta>
            <ta e="T396" id="Seg_1348" s="T395">0.giv-active</ta>
            <ta e="T398" id="Seg_1349" s="T397">0.giv-active</ta>
            <ta e="T400" id="Seg_1350" s="T399">new </ta>
            <ta e="T403" id="Seg_1351" s="T402">giv-active </ta>
            <ta e="T404" id="Seg_1352" s="T403">0.giv-active</ta>
            <ta e="T405" id="Seg_1353" s="T404">accs-inf</ta>
            <ta e="T406" id="Seg_1354" s="T405">0.giv-active</ta>
            <ta e="T408" id="Seg_1355" s="T407">accs-sit </ta>
            <ta e="T409" id="Seg_1356" s="T408">0.giv-active</ta>
            <ta e="T411" id="Seg_1357" s="T409">accs-inf</ta>
            <ta e="T412" id="Seg_1358" s="T411">0.giv-active</ta>
            <ta e="T413" id="Seg_1359" s="T412">giv-inactive </ta>
            <ta e="T414" id="Seg_1360" s="T413">accs-gen </ta>
            <ta e="T417" id="Seg_1361" s="T416">0.giv-active</ta>
            <ta e="T419" id="Seg_1362" s="T418">accs-inf</ta>
            <ta e="T420" id="Seg_1363" s="T419">giv-inactive </ta>
            <ta e="T421" id="Seg_1364" s="T420">0.giv-active</ta>
            <ta e="T422" id="Seg_1365" s="T421">0.giv-active</ta>
            <ta e="T424" id="Seg_1366" s="T423">0.giv-active</ta>
            <ta e="T426" id="Seg_1367" s="T425">giv-inactive </ta>
            <ta e="T428" id="Seg_1368" s="T427">0.giv-active</ta>
            <ta e="T429" id="Seg_1369" s="T428">giv-inactive </ta>
            <ta e="T431" id="Seg_1370" s="T430">0.giv-active</ta>
            <ta e="T433" id="Seg_1371" s="T432">0.giv-active</ta>
            <ta e="T435" id="Seg_1372" s="T434">giv-active </ta>
            <ta e="T437" id="Seg_1373" s="T436">giv-active-Q</ta>
            <ta e="T440" id="Seg_1374" s="T439">giv-active-Q </ta>
            <ta e="T443" id="Seg_1375" s="T442">giv-inactive </ta>
            <ta e="T444" id="Seg_1376" s="T443">quot-sp</ta>
            <ta e="T445" id="Seg_1377" s="T444">0.giv-inactive-Q</ta>
            <ta e="T446" id="Seg_1378" s="T445">0.giv-active</ta>
            <ta e="T448" id="Seg_1379" s="T447">giv-inactive </ta>
            <ta e="T449" id="Seg_1380" s="T448">accs-inf </ta>
            <ta e="T453" id="Seg_1381" s="T452">giv-active </ta>
            <ta e="T454" id="Seg_1382" s="T453">giv-inactive </ta>
            <ta e="T455" id="Seg_1383" s="T454">0.giv-active</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="fr" tierref="fr">
            <ta e="T360" id="Seg_1384" s="T357">Жили два брата.</ta>
            <ta e="T366" id="Seg_1385" s="T360">Обе жены по ребенку родили.</ta>
            <ta e="T370" id="Seg_1386" s="T366">У другого брата много [детей?] было. </ta>
            <ta e="T373" id="Seg_1387" s="T370">Пошли они охотиться в лес.</ta>
            <ta e="T376" id="Seg_1388" s="T373">Жены дома остались.</ta>
            <ta e="T380" id="Seg_1389" s="T376">Вечером сели они обе поесть.</ta>
            <ta e="T384" id="Seg_1390" s="T380">[Одна] увидела тень в своем супе.</ta>
            <ta e="T388" id="Seg_1391" s="T384">Людоед взобрался на чум.</ta>
            <ta e="T394" id="Seg_1392" s="T388">Посмотрев в дымоход, эта женщина перестала [суп] хлебать.</ta>
            <ta e="T397" id="Seg_1393" s="T394">Суп в сторону убрала.</ta>
            <ta e="T401" id="Seg_1394" s="T397">Встаёт и идёт себе дрова рубить.</ta>
            <ta e="T406" id="Seg_1395" s="T401">Нарубила сырых дров и в огонь положила.</ta>
            <ta e="T412" id="Seg_1396" s="T406">Затем взяла топор, села на землю в центре чума.</ta>
            <ta e="T419" id="Seg_1397" s="T412">Людоед ночью сверху в чум забрался.</ta>
            <ta e="T424" id="Seg_1398" s="T419">Он огонь разжигает, разжигает, а тот не горит.</ta>
            <ta e="T431" id="Seg_1399" s="T424">Убил он ту женщину, ту другую убил.</ta>
            <ta e="T436" id="Seg_1400" s="T431">Пришел к другой.</ta>
            <ta e="T439" id="Seg_1401" s="T436">"Ты что делаешь?"</ta>
            <ta e="T441" id="Seg_1402" s="T439">"Я убил [её]."</ta>
            <ta e="T444" id="Seg_1403" s="T441">Эта женщина говорит: </ta>
            <ta e="T445" id="Seg_1404" s="T444">"Раздуй [огонь]!"</ta>
            <ta e="T446" id="Seg_1405" s="T445">Он стал дуть.</ta>
            <ta e="T452" id="Seg_1406" s="T446">А женщина ему горло и перерезала.</ta>
            <ta e="T455" id="Seg_1407" s="T452">Убил он женщину и ее детей. </ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T360" id="Seg_1408" s="T357">There lived two brothers.</ta>
            <ta e="T366" id="Seg_1409" s="T360">Both wives had given birth to a child.</ta>
            <ta e="T370" id="Seg_1410" s="T366">The other brother had many [children?].</ta>
            <ta e="T373" id="Seg_1411" s="T370">They went hunting in the forest.</ta>
            <ta e="T376" id="Seg_1412" s="T373">Their wives stayed home.</ta>
            <ta e="T380" id="Seg_1413" s="T376">In the evening they both sat down to eat.</ta>
            <ta e="T384" id="Seg_1414" s="T380">[One] discovered a shadow on her soup.</ta>
            <ta e="T388" id="Seg_1415" s="T384">A maneater had climbed to the top of her tent.</ta>
            <ta e="T394" id="Seg_1416" s="T388">Looking at the smoke hole, this woman stopped drinking [her soup].</ta>
            <ta e="T397" id="Seg_1417" s="T394">She hanged her soup away.</ta>
            <ta e="T401" id="Seg_1418" s="T397">She gets up to chop her own firewood.</ta>
            <ta e="T406" id="Seg_1419" s="T401">She chopped raw wood and put it into the fire.</ta>
            <ta e="T412" id="Seg_1420" s="T406">Then she took an axe and sat down at the root of the tent pole.</ta>
            <ta e="T419" id="Seg_1421" s="T412">At night, the maneater came down, entered the tent.</ta>
            <ta e="T424" id="Seg_1422" s="T419">He blows and blows the fire, but it doesn’t burn.</ta>
            <ta e="T431" id="Seg_1423" s="T424">He killed that woman, the other one he killed.</ta>
            <ta e="T436" id="Seg_1424" s="T431">Then he came to this woman again.</ta>
            <ta e="T439" id="Seg_1425" s="T436">"What are you doing?"</ta>
            <ta e="T441" id="Seg_1426" s="T439">"I have killed [her]."</ta>
            <ta e="T444" id="Seg_1427" s="T441">The woman says:</ta>
            <ta e="T445" id="Seg_1428" s="T444">"Blow [the fire]!"</ta>
            <ta e="T446" id="Seg_1429" s="T445">He did blow.</ta>
            <ta e="T452" id="Seg_1430" s="T446">The woman, in turn, chopped off his throat.</ta>
            <ta e="T455" id="Seg_1431" s="T452">He had killed the woman together with her children.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T360" id="Seg_1432" s="T357">Es lebten zwei Brüder.</ta>
            <ta e="T366" id="Seg_1433" s="T360">Beide ihre Frauen hatten ein Kind geboren.</ta>
            <ta e="T370" id="Seg_1434" s="T366">Der andere Bruder hatte viele [Kinder?].</ta>
            <ta e="T373" id="Seg_1435" s="T370">Sie gingen im Wald jagen.</ta>
            <ta e="T376" id="Seg_1436" s="T373">Ihre Frauen blieben zu Hause.</ta>
            <ta e="T380" id="Seg_1437" s="T376">Am Abend setzten sich die beiden [Frauen], um zu essen.</ta>
            <ta e="T384" id="Seg_1438" s="T380">[Die eine] entdeckte einen Schatten auf ihrer Suppe.</ta>
            <ta e="T388" id="Seg_1439" s="T384">Ein Menschenfresser war auf das Dach ihres Zeltes geklettert.</ta>
            <ta e="T394" id="Seg_1440" s="T388">Als [die eine Frau] in die Rauchöffnung sah, hörte sie auf, [ihre Suppe] zu essen.</ta>
            <ta e="T397" id="Seg_1441" s="T394">Sie hängte die Suppe weg.</ta>
            <ta e="T401" id="Seg_1442" s="T397">Sie steht auf, um ihr eigenes Feuerholz zu hacken.</ta>
            <ta e="T406" id="Seg_1443" s="T401">Sie hackte frisches Holz und packte es auf das Feuer auf.</ta>
            <ta e="T412" id="Seg_1444" s="T406">Dann nahm sie eine Axt und setzte sich am Fuße der Zeltstange nieder.</ta>
            <ta e="T419" id="Seg_1445" s="T412">In der Nacht kam der Menschenfresser herunter in das Innere des Zeltes.</ta>
            <ta e="T424" id="Seg_1446" s="T419">Er bläst und bläst in das Feuer, [doch] es brennt nicht. </ta>
            <ta e="T431" id="Seg_1447" s="T424">Er tötete die Frau, die andere tötete er.</ta>
            <ta e="T436" id="Seg_1448" s="T431">Dann kam er wieder zu der [einen] Frau.</ta>
            <ta e="T439" id="Seg_1449" s="T436">[Die Frau:] „Du, was tust du?“</ta>
            <ta e="T441" id="Seg_1450" s="T439">„Ich habe [die andere Frau] getötet.“</ta>
            <ta e="T444" id="Seg_1451" s="T441">Die Frau sagt:</ta>
            <ta e="T445" id="Seg_1452" s="T444">„Blas [in das Feuer]!“</ta>
            <ta e="T446" id="Seg_1453" s="T445">Er blies.</ta>
            <ta e="T452" id="Seg_1454" s="T446">Die Frau wiederum schnitt ihm die Kehle durch. </ta>
            <ta e="T455" id="Seg_1455" s="T452">Er erschlug die Frau samt ihren Kindern.</ta>
         </annotation>
         <annotation name="ltg" tierref="ltg">
            <ta e="T360" id="Seg_1456" s="T357">Zwei Brüder lebten.</ta>
            <ta e="T366" id="Seg_1457" s="T360">Zu zweien baten sie um eine Frau.</ta>
            <ta e="T370" id="Seg_1458" s="T366">mein anderer Bruder hatte viele.</ta>
            <ta e="T373" id="Seg_1459" s="T370">Sie gingen jagen, </ta>
            <ta e="T376" id="Seg_1460" s="T373">ihre Weiber blieben zurück.</ta>
            <ta e="T380" id="Seg_1461" s="T376">Am Abend setzten sie sich beide, um zu essen.</ta>
            <ta e="T384" id="Seg_1462" s="T380">In ihrer Suppe erschien ein Schatten.</ta>
            <ta e="T388" id="Seg_1463" s="T384">Ein Menschenfresser kletterte auf das Zelt.</ta>
            <ta e="T394" id="Seg_1464" s="T388">Als sie [ihn] in der Rauchöffnung sah, dieses Weib liess ihr Essen, die Suppe aber warf sie weg,</ta>
            <ta e="T397" id="Seg_1465" s="T394">die Suppe aber warf sie weg,</ta>
            <ta e="T401" id="Seg_1466" s="T397">sie steht auf. Selbst [ging sie] Holz zu hacken,</ta>
            <ta e="T406" id="Seg_1467" s="T401">frisches Holz hackte sie, tat in das Feuer.</ta>
            <ta e="T412" id="Seg_1468" s="T406">Dann nahm sie die Axt, setzte sich am Fusse der in der Mitte des Zeltes befindlichen Stange nieder.</ta>
            <ta e="T419" id="Seg_1469" s="T412">Der Menschenfresser in der Nacht sich herablassend kam, trat in das Zelt hinein.</ta>
            <ta e="T424" id="Seg_1470" s="T419">Bläst das Feuer an, bläst; [es] brennt nicht.</ta>
            <ta e="T431" id="Seg_1471" s="T424">Er erschlug die eine,</ta>
            <ta e="T436" id="Seg_1472" s="T431">kam dann noch zu diesem Weibe.</ta>
            <ta e="T439" id="Seg_1473" s="T436">»Du, was tust du?</ta>
            <ta e="T441" id="Seg_1474" s="T439">Ich erschlug dieses Weib.»</ta>
            <ta e="T444" id="Seg_1475" s="T441">Er sagt:</ta>
            <ta e="T445" id="Seg_1476" s="T444"> »Blase!»</ta>
            <ta e="T446" id="Seg_1477" s="T445">Sie blies.</ta>
            <ta e="T452" id="Seg_1478" s="T446">Dieses Weibes Hals wieder durchschnitt er, hackte ab,</ta>
            <ta e="T455" id="Seg_1479" s="T452">erschlug das Weib mit den Kindern.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T366" id="Seg_1480" s="T360">[KlT] The syntactic analysis is preliminary.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltg"
                          display-name="ltg"
                          name="ltg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
