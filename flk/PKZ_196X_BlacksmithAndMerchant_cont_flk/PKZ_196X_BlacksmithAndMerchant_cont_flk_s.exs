<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID77DF45C8-F4F9-D8B3-F2BC-EFE14B61CE69">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_BlacksmithAndMerchant_cont_flk.wav" />
         <referenced-file url="PKZ_196X_BlacksmithAndMerchant_cont_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_BlacksmithAndMerchant_cont_flk\PKZ_196X_BlacksmithAndMerchant_cont_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">58</ud-information>
            <ud-information attribute-name="# HIAT:w">40</ud-information>
            <ud-information attribute-name="# e">40</ud-information>
            <ud-information attribute-name="# HIAT:u">7</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="0.786" type="appl" />
         <tli id="T2" time="1.572" type="appl" />
         <tli id="T3" time="2.357" type="appl" />
         <tli id="T4" time="3.143" type="appl" />
         <tli id="T5" time="3.929" type="appl" />
         <tli id="T6" time="4.823" type="appl" />
         <tli id="T7" time="5.716" type="appl" />
         <tli id="T8" time="6.61" type="appl" />
         <tli id="T9" time="7.503" type="appl" />
         <tli id="T10" time="8.397" type="appl" />
         <tli id="T11" time="8.955" type="appl" />
         <tli id="T12" time="9.512" type="appl" />
         <tli id="T13" time="10.07" type="appl" />
         <tli id="T14" time="10.627" type="appl" />
         <tli id="T15" time="11.185" type="appl" />
         <tli id="T16" time="11.743" type="appl" />
         <tli id="T17" time="12.3" type="appl" />
         <tli id="T18" time="12.858" type="appl" />
         <tli id="T19" time="13.415" type="appl" />
         <tli id="T20" time="13.973" type="appl" />
         <tli id="T21" time="14.609" type="appl" />
         <tli id="T22" time="15.246" type="appl" />
         <tli id="T23" time="15.882" type="appl" />
         <tli id="T24" time="16.462" type="appl" />
         <tli id="T25" time="16.974" type="appl" />
         <tli id="T26" time="17.486" type="appl" />
         <tli id="T27" time="17.998" type="appl" />
         <tli id="T28" time="18.51" type="appl" />
         <tli id="T29" time="19.022" type="appl" />
         <tli id="T30" time="19.534" type="appl" />
         <tli id="T31" time="20.046" type="appl" />
         <tli id="T32" time="21.26540751803173" />
         <tli id="T42" time="21.978698616598937" />
         <tli id="T33" time="23.11863111991663" />
         <tli id="T34" time="23.55193879661633" />
         <tli id="T35" time="23.90525120992532" />
         <tli id="T36" time="24.198567175691277" />
         <tli id="T37" time="25.242" type="appl" />
         <tli id="T38" time="26.019" type="appl" />
         <tli id="T39" time="26.797" type="appl" />
         <tli id="T40" time="27.698" type="appl" />
         <tli id="T41" time="27.785" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T40" id="Seg_0" n="sc" s="T0">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Dĭgəttə</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">dĭ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">šobi</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">dĭ</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">kuznʼetstə</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_20" n="HIAT:u" s="T5">
                  <nts id="Seg_21" n="HIAT:ip">"</nts>
                  <ts e="T6" id="Seg_23" n="HIAT:w" s="T5">Ĭmbi</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">tăn</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">ej</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">nüjliel</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">tüj</ts>
                  <nts id="Seg_36" n="HIAT:ip">?</nts>
                  <nts id="Seg_37" n="HIAT:ip">"</nts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_40" n="HIAT:u" s="T10">
                  <nts id="Seg_41" n="HIAT:ip">"</nts>
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">Da</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">tăn</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_48" n="HIAT:ip">(</nts>
                  <ts e="T13" id="Seg_50" n="HIAT:w" s="T12">mă-</ts>
                  <nts id="Seg_51" n="HIAT:ip">)</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_54" n="HIAT:w" s="T13">a</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_57" n="HIAT:w" s="T14">na</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_60" n="HIAT:w" s="T15">tăn</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_63" n="HIAT:w" s="T16">aktʼal</ts>
                  <nts id="Seg_64" n="HIAT:ip">"</nts>
                  <nts id="Seg_65" n="HIAT:ip">—</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_68" n="HIAT:w" s="T17">barəʔluʔpi</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_71" n="HIAT:w" s="T18">dĭʔnə</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_74" n="HIAT:w" s="T19">ujutsiʔ</ts>
                  <nts id="Seg_75" n="HIAT:ip">.</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_78" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_80" n="HIAT:w" s="T20">Dĭ</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_83" n="HIAT:w" s="T21">kabarlaʔ</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_86" n="HIAT:w" s="T22">kambi</ts>
                  <nts id="Seg_87" n="HIAT:ip">.</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_90" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_92" n="HIAT:w" s="T23">A</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_95" n="HIAT:w" s="T24">dĭ</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_98" n="HIAT:w" s="T25">bazoʔ</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_101" n="HIAT:w" s="T26">kambi</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_104" n="HIAT:w" s="T27">togonorzittə</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_107" n="HIAT:w" s="T28">i</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_110" n="HIAT:w" s="T29">nüjnə</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_112" n="HIAT:ip">(</nts>
                  <ts e="T31" id="Seg_114" n="HIAT:w" s="T30">nu-</ts>
                  <nts id="Seg_115" n="HIAT:ip">)</nts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_118" n="HIAT:w" s="T31">nüjleʔbə</ts>
                  <nts id="Seg_119" n="HIAT:ip">.</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_122" n="HIAT:u" s="T32">
                  <ts e="T42" id="Seg_124" n="HIAT:w" s="T32">Lučšə</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_127" n="HIAT:w" s="T42">togonorzittə</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_129" n="HIAT:ip">(</nts>
                  <ts e="T34" id="Seg_131" n="HIAT:w" s="T33">č-</ts>
                  <nts id="Seg_132" n="HIAT:ip">)</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_135" n="HIAT:w" s="T34">čem</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_138" n="HIAT:w" s="T35">tăn</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_141" n="HIAT:w" s="T36">aktʼam</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_144" n="HIAT:w" s="T37">măndərzittə</ts>
                  <nts id="Seg_145" n="HIAT:ip">.</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_148" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_150" n="HIAT:w" s="T39">Kabarləj</ts>
                  <nts id="Seg_151" n="HIAT:ip">!</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T40" id="Seg_153" n="sc" s="T0">
               <ts e="T1" id="Seg_155" n="e" s="T0">Dĭgəttə </ts>
               <ts e="T2" id="Seg_157" n="e" s="T1">dĭ </ts>
               <ts e="T3" id="Seg_159" n="e" s="T2">šobi </ts>
               <ts e="T4" id="Seg_161" n="e" s="T3">dĭ </ts>
               <ts e="T5" id="Seg_163" n="e" s="T4">kuznʼetstə. </ts>
               <ts e="T6" id="Seg_165" n="e" s="T5">"Ĭmbi </ts>
               <ts e="T7" id="Seg_167" n="e" s="T6">tăn </ts>
               <ts e="T8" id="Seg_169" n="e" s="T7">ej </ts>
               <ts e="T9" id="Seg_171" n="e" s="T8">nüjliel </ts>
               <ts e="T10" id="Seg_173" n="e" s="T9">tüj?" </ts>
               <ts e="T11" id="Seg_175" n="e" s="T10">"Da </ts>
               <ts e="T12" id="Seg_177" n="e" s="T11">tăn </ts>
               <ts e="T13" id="Seg_179" n="e" s="T12">(mă-) </ts>
               <ts e="T14" id="Seg_181" n="e" s="T13">a </ts>
               <ts e="T15" id="Seg_183" n="e" s="T14">na </ts>
               <ts e="T16" id="Seg_185" n="e" s="T15">tăn </ts>
               <ts e="T17" id="Seg_187" n="e" s="T16">aktʼal"— </ts>
               <ts e="T18" id="Seg_189" n="e" s="T17">barəʔluʔpi </ts>
               <ts e="T19" id="Seg_191" n="e" s="T18">dĭʔnə </ts>
               <ts e="T20" id="Seg_193" n="e" s="T19">ujutsiʔ. </ts>
               <ts e="T21" id="Seg_195" n="e" s="T20">Dĭ </ts>
               <ts e="T22" id="Seg_197" n="e" s="T21">kabarlaʔ </ts>
               <ts e="T23" id="Seg_199" n="e" s="T22">kambi. </ts>
               <ts e="T24" id="Seg_201" n="e" s="T23">A </ts>
               <ts e="T25" id="Seg_203" n="e" s="T24">dĭ </ts>
               <ts e="T26" id="Seg_205" n="e" s="T25">bazoʔ </ts>
               <ts e="T27" id="Seg_207" n="e" s="T26">kambi </ts>
               <ts e="T28" id="Seg_209" n="e" s="T27">togonorzittə </ts>
               <ts e="T29" id="Seg_211" n="e" s="T28">i </ts>
               <ts e="T30" id="Seg_213" n="e" s="T29">nüjnə </ts>
               <ts e="T31" id="Seg_215" n="e" s="T30">(nu-) </ts>
               <ts e="T32" id="Seg_217" n="e" s="T31">nüjleʔbə. </ts>
               <ts e="T42" id="Seg_219" n="e" s="T32">Lučšə </ts>
               <ts e="T33" id="Seg_221" n="e" s="T42">togonorzittə </ts>
               <ts e="T34" id="Seg_223" n="e" s="T33">(č-) </ts>
               <ts e="T35" id="Seg_225" n="e" s="T34">čem </ts>
               <ts e="T36" id="Seg_227" n="e" s="T35">tăn </ts>
               <ts e="T37" id="Seg_229" n="e" s="T36">aktʼam </ts>
               <ts e="T39" id="Seg_231" n="e" s="T37">măndərzittə. </ts>
               <ts e="T40" id="Seg_233" n="e" s="T39">Kabarləj! </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_234" s="T0">PKZ_196X_BlacksmithAndMerchant_cont_flk.001 (001)</ta>
            <ta e="T10" id="Seg_235" s="T5">PKZ_196X_BlacksmithAndMerchant_cont_flk.002 (003)</ta>
            <ta e="T20" id="Seg_236" s="T10">PKZ_196X_BlacksmithAndMerchant_cont_flk.003 (004)</ta>
            <ta e="T23" id="Seg_237" s="T20">PKZ_196X_BlacksmithAndMerchant_cont_flk.004 (005)</ta>
            <ta e="T32" id="Seg_238" s="T23">PKZ_196X_BlacksmithAndMerchant_cont_flk.005 (006)</ta>
            <ta e="T39" id="Seg_239" s="T32">PKZ_196X_BlacksmithAndMerchant_cont_flk.006 (007)</ta>
            <ta e="T40" id="Seg_240" s="T39">PKZ_196X_BlacksmithAndMerchant_cont_flk.007 (008)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_241" s="T0">Dĭgəttə dĭ šobi dĭ kuznʼetstə. </ta>
            <ta e="T10" id="Seg_242" s="T5">"Ĭmbi tăn ej nüjliel tüj?" </ta>
            <ta e="T20" id="Seg_243" s="T10">"Da tăn (mă-) a na tăn aktʼal"— barəʔluʔpi dĭʔnə ujutsiʔ. </ta>
            <ta e="T23" id="Seg_244" s="T20">Dĭ kabarlaʔ kambi. </ta>
            <ta e="T32" id="Seg_245" s="T23">A dĭ bazoʔ kambi togonorzittə i nüjnə (nu-) nüjleʔbə. </ta>
            <ta e="T39" id="Seg_246" s="T32">Lučšə togonorzittə (č-) čem tăn aktʼam măndərzittə. </ta>
            <ta e="T40" id="Seg_247" s="T39">Kabarləj! </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_248" s="T0">dĭgəttə</ta>
            <ta e="T2" id="Seg_249" s="T1">dĭ</ta>
            <ta e="T3" id="Seg_250" s="T2">šo-bi</ta>
            <ta e="T4" id="Seg_251" s="T3">dĭ</ta>
            <ta e="T5" id="Seg_252" s="T4">kuznʼets-tə</ta>
            <ta e="T6" id="Seg_253" s="T5">ĭmbi</ta>
            <ta e="T7" id="Seg_254" s="T6">tăn</ta>
            <ta e="T8" id="Seg_255" s="T7">ej</ta>
            <ta e="T9" id="Seg_256" s="T8">nüj-lie-l</ta>
            <ta e="T10" id="Seg_257" s="T9">tüj</ta>
            <ta e="T11" id="Seg_258" s="T10">da</ta>
            <ta e="T12" id="Seg_259" s="T11">tăn</ta>
            <ta e="T14" id="Seg_260" s="T13">a</ta>
            <ta e="T15" id="Seg_261" s="T14">na</ta>
            <ta e="T16" id="Seg_262" s="T15">tăn</ta>
            <ta e="T17" id="Seg_263" s="T16">aktʼa-l</ta>
            <ta e="T18" id="Seg_264" s="T17">barəʔ-luʔ-pi</ta>
            <ta e="T19" id="Seg_265" s="T18">dĭʔ-nə</ta>
            <ta e="T20" id="Seg_266" s="T19">uju-t-siʔ</ta>
            <ta e="T21" id="Seg_267" s="T20">dĭ</ta>
            <ta e="T22" id="Seg_268" s="T21">kabar-laʔ</ta>
            <ta e="T23" id="Seg_269" s="T22">kam-bi</ta>
            <ta e="T24" id="Seg_270" s="T23">a</ta>
            <ta e="T25" id="Seg_271" s="T24">dĭ</ta>
            <ta e="T26" id="Seg_272" s="T25">bazoʔ</ta>
            <ta e="T27" id="Seg_273" s="T26">kam-bi</ta>
            <ta e="T28" id="Seg_274" s="T27">togonor-zittə</ta>
            <ta e="T29" id="Seg_275" s="T28">i</ta>
            <ta e="T30" id="Seg_276" s="T29">nüjnə</ta>
            <ta e="T32" id="Seg_277" s="T31">nüj-leʔbə</ta>
            <ta e="T33" id="Seg_278" s="T42">togonor-zittə</ta>
            <ta e="T36" id="Seg_279" s="T35">tăn</ta>
            <ta e="T37" id="Seg_280" s="T36">aktʼa-m</ta>
            <ta e="T39" id="Seg_281" s="T37">măndə-r-zittə</ta>
            <ta e="T40" id="Seg_282" s="T39">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_283" s="T0">dĭgəttə</ta>
            <ta e="T2" id="Seg_284" s="T1">dĭ</ta>
            <ta e="T3" id="Seg_285" s="T2">šo-bi</ta>
            <ta e="T4" id="Seg_286" s="T3">dĭ</ta>
            <ta e="T5" id="Seg_287" s="T4">kuznʼes-Tə</ta>
            <ta e="T6" id="Seg_288" s="T5">ĭmbi</ta>
            <ta e="T7" id="Seg_289" s="T6">tăn</ta>
            <ta e="T8" id="Seg_290" s="T7">ej</ta>
            <ta e="T9" id="Seg_291" s="T8">nüj-liA-l</ta>
            <ta e="T10" id="Seg_292" s="T9">tüj</ta>
            <ta e="T11" id="Seg_293" s="T10">da</ta>
            <ta e="T12" id="Seg_294" s="T11">tăn</ta>
            <ta e="T14" id="Seg_295" s="T13">a</ta>
            <ta e="T15" id="Seg_296" s="T14">na</ta>
            <ta e="T16" id="Seg_297" s="T15">tăn</ta>
            <ta e="T17" id="Seg_298" s="T16">aktʼa-l</ta>
            <ta e="T18" id="Seg_299" s="T17">barəʔ-luʔbdə-bi</ta>
            <ta e="T19" id="Seg_300" s="T18">dĭ-Tə</ta>
            <ta e="T20" id="Seg_301" s="T19">üjü-t-ziʔ</ta>
            <ta e="T21" id="Seg_302" s="T20">dĭ</ta>
            <ta e="T22" id="Seg_303" s="T21">kabar-lAʔ</ta>
            <ta e="T23" id="Seg_304" s="T22">kan-bi</ta>
            <ta e="T24" id="Seg_305" s="T23">a</ta>
            <ta e="T25" id="Seg_306" s="T24">dĭ</ta>
            <ta e="T26" id="Seg_307" s="T25">bazoʔ</ta>
            <ta e="T27" id="Seg_308" s="T26">kan-bi</ta>
            <ta e="T28" id="Seg_309" s="T27">togonər-zittə</ta>
            <ta e="T29" id="Seg_310" s="T28">i</ta>
            <ta e="T30" id="Seg_311" s="T29">nüjnə</ta>
            <ta e="T32" id="Seg_312" s="T31">nüj-laʔbə</ta>
            <ta e="T33" id="Seg_313" s="T42">togonər-zittə</ta>
            <ta e="T36" id="Seg_314" s="T35">tăn</ta>
            <ta e="T37" id="Seg_315" s="T36">aktʼa-m</ta>
            <ta e="T39" id="Seg_316" s="T37">măndo-r-zittə</ta>
            <ta e="T40" id="Seg_317" s="T39">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_318" s="T0">then</ta>
            <ta e="T2" id="Seg_319" s="T1">this.[NOM.SG]</ta>
            <ta e="T3" id="Seg_320" s="T2">come-PST.[3SG]</ta>
            <ta e="T4" id="Seg_321" s="T3">this.[NOM.SG]</ta>
            <ta e="T5" id="Seg_322" s="T4">blacksmith-LAT</ta>
            <ta e="T6" id="Seg_323" s="T5">what.[NOM.SG]</ta>
            <ta e="T7" id="Seg_324" s="T6">you.NOM</ta>
            <ta e="T8" id="Seg_325" s="T7">NEG</ta>
            <ta e="T9" id="Seg_326" s="T8">sing-PRS-2SG</ta>
            <ta e="T10" id="Seg_327" s="T9">now</ta>
            <ta e="T11" id="Seg_328" s="T10">and</ta>
            <ta e="T12" id="Seg_329" s="T11">you.NOM</ta>
            <ta e="T14" id="Seg_330" s="T13">and</ta>
            <ta e="T15" id="Seg_331" s="T14">take.it</ta>
            <ta e="T16" id="Seg_332" s="T15">you.NOM</ta>
            <ta e="T17" id="Seg_333" s="T16">money-NOM/GEN/ACC.2SG</ta>
            <ta e="T18" id="Seg_334" s="T17">throw.away-MOM-PST.[3SG]</ta>
            <ta e="T19" id="Seg_335" s="T18">this-LAT</ta>
            <ta e="T20" id="Seg_336" s="T19">foot-3SG-INS</ta>
            <ta e="T21" id="Seg_337" s="T20">this.[NOM.SG]</ta>
            <ta e="T22" id="Seg_338" s="T21">grab-CVB</ta>
            <ta e="T23" id="Seg_339" s="T22">go-PST.[3SG]</ta>
            <ta e="T24" id="Seg_340" s="T23">and</ta>
            <ta e="T25" id="Seg_341" s="T24">this.[NOM.SG]</ta>
            <ta e="T26" id="Seg_342" s="T25">again</ta>
            <ta e="T27" id="Seg_343" s="T26">go-PST.[3SG]</ta>
            <ta e="T28" id="Seg_344" s="T27">work-INF.LAT</ta>
            <ta e="T29" id="Seg_345" s="T28">and</ta>
            <ta e="T30" id="Seg_346" s="T29">song.[NOM.SG]</ta>
            <ta e="T32" id="Seg_347" s="T31">sing-DUR.[3SG]</ta>
            <ta e="T33" id="Seg_348" s="T42">work-INF.LAT</ta>
            <ta e="T36" id="Seg_349" s="T35">you.NOM</ta>
            <ta e="T37" id="Seg_350" s="T36">money-ACC</ta>
            <ta e="T39" id="Seg_351" s="T37">look-FRQ-INF.LAT</ta>
            <ta e="T40" id="Seg_352" s="T39">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_353" s="T0">тогда</ta>
            <ta e="T2" id="Seg_354" s="T1">этот.[NOM.SG]</ta>
            <ta e="T3" id="Seg_355" s="T2">прийти-PST.[3SG]</ta>
            <ta e="T4" id="Seg_356" s="T3">этот.[NOM.SG]</ta>
            <ta e="T5" id="Seg_357" s="T4">кузнец-LAT</ta>
            <ta e="T6" id="Seg_358" s="T5">что.[NOM.SG]</ta>
            <ta e="T7" id="Seg_359" s="T6">ты.NOM</ta>
            <ta e="T8" id="Seg_360" s="T7">NEG</ta>
            <ta e="T9" id="Seg_361" s="T8">петь-PRS-2SG</ta>
            <ta e="T10" id="Seg_362" s="T9">сейчас</ta>
            <ta e="T11" id="Seg_363" s="T10">и</ta>
            <ta e="T12" id="Seg_364" s="T11">ты.NOM</ta>
            <ta e="T14" id="Seg_365" s="T13">а</ta>
            <ta e="T15" id="Seg_366" s="T14">возьми</ta>
            <ta e="T16" id="Seg_367" s="T15">ты.NOM</ta>
            <ta e="T17" id="Seg_368" s="T16">деньги-NOM/GEN/ACC.2SG</ta>
            <ta e="T18" id="Seg_369" s="T17">выбросить-MOM-PST.[3SG]</ta>
            <ta e="T19" id="Seg_370" s="T18">этот-LAT</ta>
            <ta e="T20" id="Seg_371" s="T19">нога-3SG-INS</ta>
            <ta e="T21" id="Seg_372" s="T20">этот.[NOM.SG]</ta>
            <ta e="T22" id="Seg_373" s="T21">хватать-CVB</ta>
            <ta e="T23" id="Seg_374" s="T22">пойти-PST.[3SG]</ta>
            <ta e="T24" id="Seg_375" s="T23">а</ta>
            <ta e="T25" id="Seg_376" s="T24">этот.[NOM.SG]</ta>
            <ta e="T26" id="Seg_377" s="T25">опять</ta>
            <ta e="T27" id="Seg_378" s="T26">пойти-PST.[3SG]</ta>
            <ta e="T28" id="Seg_379" s="T27">работать-INF.LAT</ta>
            <ta e="T29" id="Seg_380" s="T28">и</ta>
            <ta e="T30" id="Seg_381" s="T29">песня.[NOM.SG]</ta>
            <ta e="T32" id="Seg_382" s="T31">петь-DUR.[3SG]</ta>
            <ta e="T33" id="Seg_383" s="T42">работать-INF.LAT</ta>
            <ta e="T36" id="Seg_384" s="T35">ты.NOM</ta>
            <ta e="T37" id="Seg_385" s="T36">деньги-ACC</ta>
            <ta e="T39" id="Seg_386" s="T37">смотреть-FRQ-INF.LAT</ta>
            <ta e="T40" id="Seg_387" s="T39">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_388" s="T0">adv</ta>
            <ta e="T2" id="Seg_389" s="T1">dempro-n:case</ta>
            <ta e="T3" id="Seg_390" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_391" s="T3">dempro-n:case</ta>
            <ta e="T5" id="Seg_392" s="T4">n-n:case</ta>
            <ta e="T6" id="Seg_393" s="T5">que-n:case</ta>
            <ta e="T7" id="Seg_394" s="T6">pers</ta>
            <ta e="T8" id="Seg_395" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_396" s="T8">v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_397" s="T9">adv</ta>
            <ta e="T11" id="Seg_398" s="T10">conj</ta>
            <ta e="T12" id="Seg_399" s="T11">pers</ta>
            <ta e="T14" id="Seg_400" s="T13">conj</ta>
            <ta e="T15" id="Seg_401" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_402" s="T15">pers</ta>
            <ta e="T17" id="Seg_403" s="T16">n-n:case.poss</ta>
            <ta e="T18" id="Seg_404" s="T17">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T19" id="Seg_405" s="T18">dempro-n:case</ta>
            <ta e="T20" id="Seg_406" s="T19">n-n:case.poss-n:case</ta>
            <ta e="T21" id="Seg_407" s="T20">dempro-n:case</ta>
            <ta e="T22" id="Seg_408" s="T21">v-v:n.fin</ta>
            <ta e="T23" id="Seg_409" s="T22">v-v:tense-v:pn</ta>
            <ta e="T24" id="Seg_410" s="T23">conj</ta>
            <ta e="T25" id="Seg_411" s="T24">dempro-n:case</ta>
            <ta e="T26" id="Seg_412" s="T25">adv</ta>
            <ta e="T27" id="Seg_413" s="T26">v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_414" s="T27">v-v:n.fin</ta>
            <ta e="T29" id="Seg_415" s="T28">conj</ta>
            <ta e="T30" id="Seg_416" s="T29">n-n:case</ta>
            <ta e="T32" id="Seg_417" s="T31">v-v&gt;v-v:pn</ta>
            <ta e="T33" id="Seg_418" s="T42">v-v:n.fin</ta>
            <ta e="T36" id="Seg_419" s="T35">pers</ta>
            <ta e="T37" id="Seg_420" s="T36">n-n:case</ta>
            <ta e="T39" id="Seg_421" s="T37">v-v&gt;v-v:n.fin</ta>
            <ta e="T40" id="Seg_422" s="T39">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_423" s="T0">adv</ta>
            <ta e="T2" id="Seg_424" s="T1">dempro</ta>
            <ta e="T3" id="Seg_425" s="T2">v</ta>
            <ta e="T4" id="Seg_426" s="T3">dempro</ta>
            <ta e="T5" id="Seg_427" s="T4">n</ta>
            <ta e="T6" id="Seg_428" s="T5">que</ta>
            <ta e="T7" id="Seg_429" s="T6">pers</ta>
            <ta e="T8" id="Seg_430" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_431" s="T8">v</ta>
            <ta e="T10" id="Seg_432" s="T9">adv</ta>
            <ta e="T11" id="Seg_433" s="T10">conj</ta>
            <ta e="T12" id="Seg_434" s="T11">pers</ta>
            <ta e="T14" id="Seg_435" s="T13">conj</ta>
            <ta e="T15" id="Seg_436" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_437" s="T15">pers</ta>
            <ta e="T17" id="Seg_438" s="T16">n</ta>
            <ta e="T18" id="Seg_439" s="T17">v</ta>
            <ta e="T19" id="Seg_440" s="T18">dempro</ta>
            <ta e="T20" id="Seg_441" s="T19">n</ta>
            <ta e="T21" id="Seg_442" s="T20">dempro</ta>
            <ta e="T22" id="Seg_443" s="T21">v</ta>
            <ta e="T23" id="Seg_444" s="T22">v</ta>
            <ta e="T24" id="Seg_445" s="T23">conj</ta>
            <ta e="T25" id="Seg_446" s="T24">dempro</ta>
            <ta e="T26" id="Seg_447" s="T25">adv</ta>
            <ta e="T27" id="Seg_448" s="T26">v</ta>
            <ta e="T28" id="Seg_449" s="T27">v</ta>
            <ta e="T29" id="Seg_450" s="T28">conj</ta>
            <ta e="T30" id="Seg_451" s="T29">n</ta>
            <ta e="T32" id="Seg_452" s="T31">v</ta>
            <ta e="T33" id="Seg_453" s="T42">v</ta>
            <ta e="T36" id="Seg_454" s="T35">pers</ta>
            <ta e="T37" id="Seg_455" s="T36">n</ta>
            <ta e="T39" id="Seg_456" s="T37">v</ta>
            <ta e="T40" id="Seg_457" s="T39">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_458" s="T0">adv:Time</ta>
            <ta e="T2" id="Seg_459" s="T1">pro.h:A</ta>
            <ta e="T5" id="Seg_460" s="T4">np:G</ta>
            <ta e="T7" id="Seg_461" s="T6">pro.h:A</ta>
            <ta e="T10" id="Seg_462" s="T9">adv:Time</ta>
            <ta e="T12" id="Seg_463" s="T11">pro.h:A</ta>
            <ta e="T16" id="Seg_464" s="T15">pro.h:Poss</ta>
            <ta e="T17" id="Seg_465" s="T16">np:Th</ta>
            <ta e="T18" id="Seg_466" s="T17">0.3.h:A</ta>
            <ta e="T19" id="Seg_467" s="T18">pro.h:Poss</ta>
            <ta e="T20" id="Seg_468" s="T19">np:G</ta>
            <ta e="T21" id="Seg_469" s="T20">pro.h:A</ta>
            <ta e="T25" id="Seg_470" s="T24">pro.h:A</ta>
            <ta e="T30" id="Seg_471" s="T29">np:Th</ta>
            <ta e="T32" id="Seg_472" s="T31">0.3.h:A</ta>
            <ta e="T36" id="Seg_473" s="T35">pro.h:Poss</ta>
            <ta e="T37" id="Seg_474" s="T36">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_475" s="T1">pro.h:S</ta>
            <ta e="T3" id="Seg_476" s="T2">v:pred</ta>
            <ta e="T7" id="Seg_477" s="T6">pro.h:S</ta>
            <ta e="T8" id="Seg_478" s="T7">ptcl.neg</ta>
            <ta e="T9" id="Seg_479" s="T8">v:pred</ta>
            <ta e="T15" id="Seg_480" s="T14">ptcl:pred</ta>
            <ta e="T17" id="Seg_481" s="T16">np:O</ta>
            <ta e="T18" id="Seg_482" s="T17">v:pred 0.3.h:S</ta>
            <ta e="T21" id="Seg_483" s="T20">pro.h:S</ta>
            <ta e="T22" id="Seg_484" s="T21">conv:pred</ta>
            <ta e="T23" id="Seg_485" s="T22">v:pred</ta>
            <ta e="T25" id="Seg_486" s="T24">pro.h:S</ta>
            <ta e="T27" id="Seg_487" s="T26">v:pred</ta>
            <ta e="T28" id="Seg_488" s="T27">s:purp</ta>
            <ta e="T30" id="Seg_489" s="T29">np:O</ta>
            <ta e="T32" id="Seg_490" s="T31">v:pred 0.3.h:S</ta>
            <ta e="T33" id="Seg_491" s="T42">v:pred</ta>
            <ta e="T37" id="Seg_492" s="T36">np:O</ta>
            <ta e="T39" id="Seg_493" s="T37">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T5" id="Seg_494" s="T4">RUS:cult</ta>
            <ta e="T11" id="Seg_495" s="T10">RUS:gram</ta>
            <ta e="T14" id="Seg_496" s="T13">RUS:gram</ta>
            <ta e="T15" id="Seg_497" s="T14">RUS:disc</ta>
            <ta e="T24" id="Seg_498" s="T23">RUS:gram</ta>
            <ta e="T29" id="Seg_499" s="T28">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T42" id="Seg_500" s="T32">RUS:int</ta>
            <ta e="T35" id="Seg_501" s="T34">RUS:int</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_502" s="T0">Тогда он пошел к кузнецу.</ta>
            <ta e="T10" id="Seg_503" s="T5">"Почему ты теперь не поешь?"</ta>
            <ta e="T20" id="Seg_504" s="T10">"Да забери свои деньги", - он кинул [их] ему под ноги.</ta>
            <ta e="T23" id="Seg_505" s="T20">Тот схватил их и ушел.</ta>
            <ta e="T32" id="Seg_506" s="T23">А он опять пошел работать и песни поет.</ta>
            <ta e="T39" id="Seg_507" s="T32">Лучше работать, чем следить за твоими деньгами.</ta>
            <ta e="T40" id="Seg_508" s="T39">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_509" s="T0">Then he came to the smith.</ta>
            <ta e="T10" id="Seg_510" s="T5">"Why don't you sing now?"</ta>
            <ta e="T20" id="Seg_511" s="T10">"And you, take your money," -- he threw [it] at his feet.</ta>
            <ta e="T23" id="Seg_512" s="T20">He grabbed [them] and went away.</ta>
            <ta e="T32" id="Seg_513" s="T23">But he went to work again and sings a song.</ta>
            <ta e="T39" id="Seg_514" s="T32">It is better to work than to watch over your money.</ta>
            <ta e="T40" id="Seg_515" s="T39">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_516" s="T0">Dann kam er zum Schmied.</ta>
            <ta e="T10" id="Seg_517" s="T5">"Warum singst du jetzt nicht?"</ta>
            <ta e="T20" id="Seg_518" s="T10">"Und du, nimm dein Geld", er warf [es] ihm auf die Füße.</ta>
            <ta e="T23" id="Seg_519" s="T20">Er griff [sie] und ging weg.</ta>
            <ta e="T32" id="Seg_520" s="T23">Aber er ging wieder arbeiten und singt ein Lied.</ta>
            <ta e="T39" id="Seg_521" s="T32">Es ist besser zu arbeiten, als über dein Geld zu schauen.</ta>
            <ta e="T40" id="Seg_522" s="T39">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T5" id="Seg_523" s="T0">[KlT:] Continuation from the original tape 0196</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T42" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
