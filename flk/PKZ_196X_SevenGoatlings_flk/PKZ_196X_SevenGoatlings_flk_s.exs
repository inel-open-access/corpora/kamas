<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDBBCFDD1C-9383-6396-BDFD-1F9BEC895B93">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_SevenGoatlings_flk.wav" />
         <referenced-file url="PKZ_196X_SevenGoatlings_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_SevenGoatlings_flk\PKZ_196X_SevenGoatlings_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">352</ud-information>
            <ud-information attribute-name="# HIAT:w">218</ud-information>
            <ud-information attribute-name="# e">218</ud-information>
            <ud-information attribute-name="# HIAT:u">60</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1670" time="0.096" type="appl" />
         <tli id="T1671" time="1.004" type="appl" />
         <tli id="T1672" time="1.911" type="appl" />
         <tli id="T1673" time="2.497" type="appl" />
         <tli id="T1674" time="3.084" type="appl" />
         <tli id="T1675" time="4.006606262676764" />
         <tli id="T1676" time="4.708" type="appl" />
         <tli id="T1677" time="5.725" type="appl" />
         <tli id="T1678" time="6.656" type="appl" />
         <tli id="T1679" time="7.587" type="appl" />
         <tli id="T1680" time="8.518" type="appl" />
         <tli id="T1681" time="9.311" type="appl" />
         <tli id="T1682" time="10.038" type="appl" />
         <tli id="T1683" time="10.766" type="appl" />
         <tli id="T1684" time="11.755" type="appl" />
         <tli id="T1685" time="12.72" type="appl" />
         <tli id="T1686" time="13.684" type="appl" />
         <tli id="T1687" time="14.448" type="appl" />
         <tli id="T1688" time="15.213" type="appl" />
         <tli id="T1689" time="15.763" type="appl" />
         <tli id="T1690" time="16.312" type="appl" />
         <tli id="T1691" time="17.206407261179248" />
         <tli id="T1692" time="17.994" type="appl" />
         <tli id="T1693" time="18.832" type="appl" />
         <tli id="T1694" time="20.206362033566172" />
         <tli id="T1695" time="21.058" type="appl" />
         <tli id="T1696" time="21.752" type="appl" />
         <tli id="T1697" time="24.3196333548167" />
         <tli id="T1698" time="27.02625921612579" />
         <tli id="T1699" time="28.639" type="appl" />
         <tli id="T1700" time="29.281" type="appl" />
         <tli id="T1701" time="29.922" type="appl" />
         <tli id="T1702" time="31.759521190336276" />
         <tli id="T1703" time="32.52" type="appl" />
         <tli id="T1704" time="33.114" type="appl" />
         <tli id="T1705" time="33.707" type="appl" />
         <tli id="T1706" time="34.3" type="appl" />
         <tli id="T1707" time="34.893" type="appl" />
         <tli id="T1708" time="35.487" type="appl" />
         <tli id="T1709" time="36.08" type="appl" />
         <tli id="T1710" time="36.739" type="appl" />
         <tli id="T1711" time="37.398" type="appl" />
         <tli id="T1712" time="37.868" type="appl" />
         <tli id="T1713" time="38.338" type="appl" />
         <tli id="T1714" time="38.809" type="appl" />
         <tli id="T1715" time="39.252334660235455" />
         <tli id="T1716" time="39.973" type="appl" />
         <tli id="T1717" time="40.667" type="appl" />
         <tli id="T1718" time="41.362" type="appl" />
         <tli id="T1719" time="42.056" type="appl" />
         <tli id="T1720" time="42.75" type="appl" />
         <tli id="T1721" time="43.882" type="appl" />
         <tli id="T1722" time="45.015" type="appl" />
         <tli id="T1723" time="46.147" type="appl" />
         <tli id="T1724" time="46.798" type="appl" />
         <tli id="T1725" time="47.448" type="appl" />
         <tli id="T1726" time="48.252" type="appl" />
         <tli id="T1727" time="48.946" type="appl" />
         <tli id="T1728" time="49.641" type="appl" />
         <tli id="T1729" time="50.335" type="appl" />
         <tli id="T1730" time="51.03" type="appl" />
         <tli id="T1731" time="52.058" type="appl" />
         <tli id="T1732" time="52.555" type="appl" />
         <tli id="T1733" time="53.052" type="appl" />
         <tli id="T1734" time="53.549" type="appl" />
         <tli id="T1735" time="54.047" type="appl" />
         <tli id="T1736" time="54.544" type="appl" />
         <tli id="T1737" time="55.041" type="appl" />
         <tli id="T1738" time="55.836" type="appl" />
         <tli id="T1739" time="56.632" type="appl" />
         <tli id="T1740" time="57.427" type="appl" />
         <tli id="T1741" time="58.222" type="appl" />
         <tli id="T1742" time="59.017" type="appl" />
         <tli id="T1743" time="59.813" type="appl" />
         <tli id="T1744" time="60.608" type="appl" />
         <tli id="T1745" time="61.403" type="appl" />
         <tli id="T1746" time="62.198" type="appl" />
         <tli id="T1747" time="62.994" type="appl" />
         <tli id="T1748" time="63.789" type="appl" />
         <tli id="T1749" time="64.269" type="appl" />
         <tli id="T1750" time="64.748" type="appl" />
         <tli id="T1751" time="65.228" type="appl" />
         <tli id="T1752" time="65.708" type="appl" />
         <tli id="T1753" time="66.378" type="appl" />
         <tli id="T1754" time="66.987" type="appl" />
         <tli id="T1755" time="67.9589754438052" />
         <tli id="T1756" time="69.188" type="appl" />
         <tli id="T1757" time="70.39893865867991" />
         <tli id="T1758" time="71.109" type="appl" />
         <tli id="T1759" time="71.785" type="appl" />
         <tli id="T1760" time="72.46866656984179" />
         <tli id="T1761" time="73.107" type="appl" />
         <tli id="T1762" time="73.752" type="appl" />
         <tli id="T1763" time="74.26" type="appl" />
         <tli id="T1764" time="74.768" type="appl" />
         <tli id="T1765" time="75.277" type="appl" />
         <tli id="T1766" time="75.785" type="appl" />
         <tli id="T1767" time="76.488" type="appl" />
         <tli id="T1768" time="77.97215781741667" />
         <tli id="T1769" time="79.466" type="appl" />
         <tli id="T1770" time="80.693" type="appl" />
         <tli id="T1771" time="83.20541225369165" />
         <tli id="T1772" time="84.411" type="appl" />
         <tli id="T1773" time="85.366" type="appl" />
         <tli id="T1774" time="86.57202816492588" />
         <tli id="T1775" time="87.279" type="appl" />
         <tli id="T1776" time="88.011" type="appl" />
         <tli id="T1777" time="88.642" type="appl" />
         <tli id="T1778" time="89.274" type="appl" />
         <tli id="T1779" time="89.905" type="appl" />
         <tli id="T1780" time="90.451" type="appl" />
         <tli id="T1781" time="90.998" type="appl" />
         <tli id="T1782" time="92.60527054050404" />
         <tli id="T1783" time="93.723" type="appl" />
         <tli id="T1784" time="94.664" type="appl" />
         <tli id="T1785" time="95.605" type="appl" />
         <tli id="T1786" time="96.393" type="appl" />
         <tli id="T1787" time="97.37853191167967" />
         <tli id="T1788" time="98.207" type="appl" />
         <tli id="T1789" time="98.945" type="appl" />
         <tli id="T1790" time="99.683" type="appl" />
         <tli id="T1791" time="100.421" type="appl" />
         <tli id="T1792" time="101.618" type="appl" />
         <tli id="T1793" time="102.815" type="appl" />
         <tli id="T1794" time="104.012" type="appl" />
         <tli id="T1795" time="104.797" type="appl" />
         <tli id="T1796" time="105.548" type="appl" />
         <tli id="T1797" time="106.298" type="appl" />
         <tli id="T1798" time="107.049" type="appl" />
         <tli id="T1799" time="107.8" type="appl" />
         <tli id="T1800" time="108.551" type="appl" />
         <tli id="T1801" time="109.302" type="appl" />
         <tli id="T1802" time="110.052" type="appl" />
         <tli id="T1803" time="110.803" type="appl" />
         <tli id="T1804" time="111.554" type="appl" />
         <tli id="T1805" time="112.25" type="appl" />
         <tli id="T1806" time="112.946" type="appl" />
         <tli id="T1807" time="113.643" type="appl" />
         <tli id="T1808" time="114.339" type="appl" />
         <tli id="T1809" time="115.035" type="appl" />
         <tli id="T1810" time="115.831" type="appl" />
         <tli id="T1811" time="116.627" type="appl" />
         <tli id="T1812" time="117.423" type="appl" />
         <tli id="T1813" time="118.219" type="appl" />
         <tli id="T1814" time="118.794" type="appl" />
         <tli id="T1815" time="119.368" type="appl" />
         <tli id="T1816" time="119.942" type="appl" />
         <tli id="T1817" time="120.517" type="appl" />
         <tli id="T1818" time="121.112" type="appl" />
         <tli id="T1819" time="121.683" type="appl" />
         <tli id="T1820" time="122.253" type="appl" />
         <tli id="T1821" time="122.824" type="appl" />
         <tli id="T1822" time="123.395" type="appl" />
         <tli id="T1823" time="123.935" type="appl" />
         <tli id="T1824" time="124.475" type="appl" />
         <tli id="T1825" time="125.015" type="appl" />
         <tli id="T1826" time="125.95810104328582" />
         <tli id="T1827" time="126.927" type="appl" />
         <tli id="T1828" time="127.642" type="appl" />
         <tli id="T1829" time="128.358" type="appl" />
         <tli id="T1830" time="129.074" type="appl" />
         <tli id="T1831" time="129.79" type="appl" />
         <tli id="T1832" time="130.505" type="appl" />
         <tli id="T1833" time="131.221" type="appl" />
         <tli id="T1834" time="131.886" type="appl" />
         <tli id="T1835" time="132.511" type="appl" />
         <tli id="T1836" time="133.135" type="appl" />
         <tli id="T1837" time="133.76" type="appl" />
         <tli id="T1838" time="134.39" type="appl" />
         <tli id="T1839" time="135.006" type="appl" />
         <tli id="T1840" time="136.0846150416319" />
         <tli id="T1841" time="136.785" type="appl" />
         <tli id="T1842" time="137.285" type="appl" />
         <tli id="T1843" time="137.786" type="appl" />
         <tli id="T1844" time="138.287" type="appl" />
         <tli id="T1845" time="138.787" type="appl" />
         <tli id="T1846" time="139.288" type="appl" />
         <tli id="T1847" time="139.949" type="appl" />
         <tli id="T1848" time="140.609" type="appl" />
         <tli id="T1849" time="141.014" type="appl" />
         <tli id="T1850" time="141.42" type="appl" />
         <tli id="T1851" time="141.825" type="appl" />
         <tli id="T1852" time="143.111" type="appl" />
         <tli id="T1853" time="144.311" type="appl" />
         <tli id="T1854" time="145.013" type="appl" />
         <tli id="T1855" time="145.686" type="appl" />
         <tli id="T1856" time="146.36" type="appl" />
         <tli id="T1857" time="147.033" type="appl" />
         <tli id="T1858" time="147.706" type="appl" />
         <tli id="T1859" time="148.278" type="appl" />
         <tli id="T1860" time="148.85" type="appl" />
         <tli id="T1861" time="149.423" type="appl" />
         <tli id="T1862" time="150.77772686016698" />
         <tli id="T1863" time="151.458" type="appl" />
         <tli id="T1864" time="152.184" type="appl" />
         <tli id="T1865" time="152.911" type="appl" />
         <tli id="T1866" time="153.575" type="appl" />
         <tli id="T1867" time="154.239" type="appl" />
         <tli id="T1868" time="154.739" type="appl" />
         <tli id="T1869" time="155.239" type="appl" />
         <tli id="T1870" time="155.738" type="appl" />
         <tli id="T1871" time="156.238" type="appl" />
         <tli id="T1872" time="156.738" type="appl" />
         <tli id="T1873" time="157.238" type="appl" />
         <tli id="T1874" time="158.042" type="appl" />
         <tli id="T1875" time="158.835" type="appl" />
         <tli id="T1876" time="164.19085797581698" />
         <tli id="T1877" time="165.37" type="appl" />
         <tli id="T1878" time="166.395" type="appl" />
         <tli id="T1879" time="167.42" type="appl" />
         <tli id="T1880" time="168.444" type="appl" />
         <tli id="T1881" time="169.386" type="appl" />
         <tli id="T1882" time="170.328" type="appl" />
         <tli id="T1883" time="171.27" type="appl" />
         <tli id="T1884" time="172.124" type="appl" />
         <tli id="T1885" time="172.978" type="appl" />
         <tli id="T1886" time="173.833" type="appl" />
         <tli id="T1887" time="174.687" type="appl" />
         <tli id="T1888" time="175.844" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T1888" id="Seg_0" n="sc" s="T1670">
               <ts e="T1672" id="Seg_2" n="HIAT:u" s="T1670">
                  <ts e="T1671" id="Seg_4" n="HIAT:w" s="T1670">Amnobi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1672" id="Seg_7" n="HIAT:w" s="T1671">poʔto</ts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1675" id="Seg_11" n="HIAT:u" s="T1672">
                  <ts e="T1673" id="Seg_13" n="HIAT:w" s="T1672">Dĭn</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1674" id="Seg_16" n="HIAT:w" s="T1673">turat</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1675" id="Seg_19" n="HIAT:w" s="T1674">ibi</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1677" id="Seg_23" n="HIAT:u" s="T1675">
                  <ts e="T1676" id="Seg_25" n="HIAT:w" s="T1675">Esseŋ</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1677" id="Seg_28" n="HIAT:w" s="T1676">deʔpi</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1680" id="Seg_32" n="HIAT:u" s="T1677">
                  <ts e="T1678" id="Seg_34" n="HIAT:w" s="T1677">Dĭgəttə</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1679" id="Seg_37" n="HIAT:w" s="T1678">kambi:</ts>
                  <nts id="Seg_38" n="HIAT:ip">"</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1680" id="Seg_41" n="HIAT:w" s="T1679">Amnogaʔ</ts>
                  <nts id="Seg_42" n="HIAT:ip">!</nts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1683" id="Seg_45" n="HIAT:u" s="T1680">
                  <ts e="T1681" id="Seg_47" n="HIAT:w" s="T1680">Šindində</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1682" id="Seg_50" n="HIAT:w" s="T1681">iʔ</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1683" id="Seg_53" n="HIAT:w" s="T1682">öʔlugeʔ</ts>
                  <nts id="Seg_54" n="HIAT:ip">!</nts>
                  <nts id="Seg_55" n="HIAT:ip">"</nts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1684" id="Seg_58" n="HIAT:u" s="T1683">
                  <ts e="T1684" id="Seg_60" n="HIAT:w" s="T1683">Kambi</ts>
                  <nts id="Seg_61" n="HIAT:ip">.</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1686" id="Seg_64" n="HIAT:u" s="T1684">
                  <ts e="T1685" id="Seg_66" n="HIAT:w" s="T1684">Dĭgəttə</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1686" id="Seg_69" n="HIAT:w" s="T1685">šobi</ts>
                  <nts id="Seg_70" n="HIAT:ip">.</nts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1688" id="Seg_73" n="HIAT:u" s="T1686">
                  <nts id="Seg_74" n="HIAT:ip">"</nts>
                  <ts e="T1687" id="Seg_76" n="HIAT:w" s="T1686">Kargaʔ</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1688" id="Seg_79" n="HIAT:w" s="T1687">ajə</ts>
                  <nts id="Seg_80" n="HIAT:ip">!</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1691" id="Seg_83" n="HIAT:u" s="T1688">
                  <ts e="T1689" id="Seg_85" n="HIAT:w" s="T1688">Šiʔ</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1690" id="Seg_88" n="HIAT:w" s="T1689">ial</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1691" id="Seg_91" n="HIAT:w" s="T1690">šobi</ts>
                  <nts id="Seg_92" n="HIAT:ip">.</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1694" id="Seg_95" n="HIAT:u" s="T1691">
                  <ts e="T1692" id="Seg_97" n="HIAT:w" s="T1691">Süt</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1693" id="Seg_100" n="HIAT:w" s="T1692">deʔpi</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1694" id="Seg_103" n="HIAT:w" s="T1693">šiʔnʼileʔ</ts>
                  <nts id="Seg_104" n="HIAT:ip">!</nts>
                  <nts id="Seg_105" n="HIAT:ip">"</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1697" id="Seg_108" n="HIAT:u" s="T1694">
                  <ts e="T1695" id="Seg_110" n="HIAT:w" s="T1694">Dĭgəttə</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1696" id="Seg_113" n="HIAT:w" s="T1695">dĭ</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_115" n="HIAT:ip">(</nts>
                  <ts e="T1697" id="Seg_117" n="HIAT:w" s="T1696">kajbiʔi</ts>
                  <nts id="Seg_118" n="HIAT:ip">)</nts>
                  <nts id="Seg_119" n="HIAT:ip">.</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1698" id="Seg_122" n="HIAT:u" s="T1697">
                  <nts id="Seg_123" n="HIAT:ip">(</nts>
                  <ts e="T1698" id="Seg_125" n="HIAT:w" s="T1697">Kajbiʔi</ts>
                  <nts id="Seg_126" n="HIAT:ip">)</nts>
                  <nts id="Seg_127" n="HIAT:ip">.</nts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1699" id="Seg_130" n="HIAT:u" s="T1698">
                  <ts e="T1699" id="Seg_132" n="HIAT:w" s="T1698">Karbiʔi</ts>
                  <nts id="Seg_133" n="HIAT:ip">.</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1702" id="Seg_136" n="HIAT:u" s="T1699">
                  <ts e="T1700" id="Seg_138" n="HIAT:w" s="T1699">Dĭgəttə</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1701" id="Seg_141" n="HIAT:w" s="T1700">dĭ</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1702" id="Seg_144" n="HIAT:w" s="T1701">šobi</ts>
                  <nts id="Seg_145" n="HIAT:ip">.</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1709" id="Seg_148" n="HIAT:u" s="T1702">
                  <ts e="T1703" id="Seg_150" n="HIAT:w" s="T1702">A</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_152" n="HIAT:ip">(</nts>
                  <ts e="T1704" id="Seg_154" n="HIAT:w" s="T1703">m-</ts>
                  <nts id="Seg_155" n="HIAT:ip">)</nts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1705" id="Seg_158" n="HIAT:w" s="T1704">mĭbi</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_160" n="HIAT:ip">(</nts>
                  <ts e="T1706" id="Seg_162" n="HIAT:w" s="T1705">dĭʔzit-</ts>
                  <nts id="Seg_163" n="HIAT:ip">)</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1707" id="Seg_166" n="HIAT:w" s="T1706">dĭzeŋdə</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_168" n="HIAT:ip">(</nts>
                  <ts e="T1708" id="Seg_170" n="HIAT:w" s="T1707">m-</ts>
                  <nts id="Seg_171" n="HIAT:ip">)</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1709" id="Seg_174" n="HIAT:w" s="T1708">amzittə</ts>
                  <nts id="Seg_175" n="HIAT:ip">.</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1711" id="Seg_178" n="HIAT:u" s="T1709">
                  <ts e="T1710" id="Seg_180" n="HIAT:w" s="T1709">Dĭzeŋ</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1711" id="Seg_183" n="HIAT:w" s="T1710">ambiʔi</ts>
                  <nts id="Seg_184" n="HIAT:ip">.</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1715" id="Seg_187" n="HIAT:u" s="T1711">
                  <ts e="T1712" id="Seg_189" n="HIAT:w" s="T1711">Dĭgəttə</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1713" id="Seg_192" n="HIAT:w" s="T1712">bazoʔ</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1714" id="Seg_195" n="HIAT:w" s="T1713">kalla</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1715" id="Seg_198" n="HIAT:w" s="T1714">dʼürbi</ts>
                  <nts id="Seg_199" n="HIAT:ip">.</nts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1720" id="Seg_202" n="HIAT:u" s="T1715">
                  <nts id="Seg_203" n="HIAT:ip">"</nts>
                  <ts e="T1716" id="Seg_205" n="HIAT:w" s="T1715">Amnogaʔ</ts>
                  <nts id="Seg_206" n="HIAT:ip">,</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1717" id="Seg_209" n="HIAT:w" s="T1716">šindində</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1718" id="Seg_212" n="HIAT:w" s="T1717">iʔ</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_214" n="HIAT:ip">(</nts>
                  <ts e="T1719" id="Seg_216" n="HIAT:w" s="T1718">öʔlu-</ts>
                  <nts id="Seg_217" n="HIAT:ip">)</nts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1720" id="Seg_220" n="HIAT:w" s="T1719">öʔlugeʔ</ts>
                  <nts id="Seg_221" n="HIAT:ip">!</nts>
                  <nts id="Seg_222" n="HIAT:ip">"</nts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1723" id="Seg_225" n="HIAT:u" s="T1720">
                  <ts e="T1721" id="Seg_227" n="HIAT:w" s="T1720">Dĭgəttə</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1722" id="Seg_230" n="HIAT:w" s="T1721">šobi</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1723" id="Seg_233" n="HIAT:w" s="T1722">volk</ts>
                  <nts id="Seg_234" n="HIAT:ip">.</nts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1730" id="Seg_237" n="HIAT:u" s="T1723">
                  <ts e="T1724" id="Seg_239" n="HIAT:w" s="T1723">Dĭ</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1725" id="Seg_242" n="HIAT:w" s="T1724">măndə:</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_244" n="HIAT:ip">"</nts>
                  <ts e="T1726" id="Seg_246" n="HIAT:w" s="T1725">Kargaʔ</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1727" id="Seg_249" n="HIAT:w" s="T1726">ajə</ts>
                  <nts id="Seg_250" n="HIAT:ip">,</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1728" id="Seg_253" n="HIAT:w" s="T1727">šiʔ</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1729" id="Seg_256" n="HIAT:w" s="T1728">ial</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1730" id="Seg_259" n="HIAT:w" s="T1729">šobi</ts>
                  <nts id="Seg_260" n="HIAT:ip">!</nts>
                  <nts id="Seg_261" n="HIAT:ip">"</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1737" id="Seg_264" n="HIAT:u" s="T1730">
                  <ts e="T1731" id="Seg_266" n="HIAT:w" s="T1730">Dĭzeŋ:</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_268" n="HIAT:ip">"</nts>
                  <ts e="T1732" id="Seg_270" n="HIAT:w" s="T1731">Dʼok</ts>
                  <nts id="Seg_271" n="HIAT:ip">,</nts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1733" id="Seg_274" n="HIAT:w" s="T1732">dĭ</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_276" n="HIAT:ip">(</nts>
                  <ts e="T1734" id="Seg_278" n="HIAT:w" s="T1733">im-</ts>
                  <nts id="Seg_279" n="HIAT:ip">)</nts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1735" id="Seg_282" n="HIAT:w" s="T1734">ej</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1736" id="Seg_285" n="HIAT:w" s="T1735">miʔ</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1737" id="Seg_288" n="HIAT:w" s="T1736">iam</ts>
                  <nts id="Seg_289" n="HIAT:ip">.</nts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1748" id="Seg_292" n="HIAT:u" s="T1737">
                  <nts id="Seg_293" n="HIAT:ip">"</nts>
                  <nts id="Seg_294" n="HIAT:ip">(</nts>
                  <ts e="T1738" id="Seg_296" n="HIAT:w" s="T1737">Măn=</ts>
                  <nts id="Seg_297" n="HIAT:ip">)</nts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1739" id="Seg_300" n="HIAT:w" s="T1738">Miʔ</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1740" id="Seg_303" n="HIAT:w" s="T1739">iam</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1741" id="Seg_306" n="HIAT:w" s="T1740">golosdə</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1742" id="Seg_309" n="HIAT:w" s="T1741">ej</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1743" id="Seg_312" n="HIAT:w" s="T1742">dĭrgit</ts>
                  <nts id="Seg_313" n="HIAT:ip">,</nts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1744" id="Seg_316" n="HIAT:w" s="T1743">ej</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1745" id="Seg_319" n="HIAT:w" s="T1744">nʼešpək</ts>
                  <nts id="Seg_320" n="HIAT:ip">,</nts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1746" id="Seg_323" n="HIAT:w" s="T1745">a</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1747" id="Seg_326" n="HIAT:w" s="T1746">tăn</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1748" id="Seg_329" n="HIAT:w" s="T1747">nʼešpək</ts>
                  <nts id="Seg_330" n="HIAT:ip">"</nts>
                  <nts id="Seg_331" n="HIAT:ip">.</nts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1752" id="Seg_334" n="HIAT:u" s="T1748">
                  <ts e="T1749" id="Seg_336" n="HIAT:w" s="T1748">Dĭgəttə</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1750" id="Seg_339" n="HIAT:w" s="T1749">dĭ</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1751" id="Seg_342" n="HIAT:w" s="T1750">kalla</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1752" id="Seg_345" n="HIAT:w" s="T1751">dʼürbi</ts>
                  <nts id="Seg_346" n="HIAT:ip">.</nts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1755" id="Seg_349" n="HIAT:u" s="T1752">
                  <ts e="T1753" id="Seg_351" n="HIAT:w" s="T1752">Iat</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1754" id="Seg_354" n="HIAT:w" s="T1753">šobi</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1755" id="Seg_357" n="HIAT:w" s="T1754">dĭzeŋ</ts>
                  <nts id="Seg_358" n="HIAT:ip">.</nts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1757" id="Seg_361" n="HIAT:u" s="T1755">
                  <ts e="T1756" id="Seg_363" n="HIAT:w" s="T1755">Bazoʔ</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1757" id="Seg_366" n="HIAT:w" s="T1756">kirgarlaʔbə</ts>
                  <nts id="Seg_367" n="HIAT:ip">.</nts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1760" id="Seg_370" n="HIAT:u" s="T1757">
                  <nts id="Seg_371" n="HIAT:ip">"</nts>
                  <nts id="Seg_372" n="HIAT:ip">(</nts>
                  <ts e="T1758" id="Seg_374" n="HIAT:w" s="T1757">Karə-</ts>
                  <nts id="Seg_375" n="HIAT:ip">)</nts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1759" id="Seg_378" n="HIAT:w" s="T1758">Kargaʔ</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1760" id="Seg_381" n="HIAT:w" s="T1759">ajə</ts>
                  <nts id="Seg_382" n="HIAT:ip">!</nts>
                  <nts id="Seg_383" n="HIAT:ip">"</nts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1762" id="Seg_386" n="HIAT:u" s="T1760">
                  <ts e="T1761" id="Seg_388" n="HIAT:w" s="T1760">Dĭ</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1762" id="Seg_391" n="HIAT:w" s="T1761">karluʔpiʔi</ts>
                  <nts id="Seg_392" n="HIAT:ip">.</nts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1766" id="Seg_395" n="HIAT:u" s="T1762">
                  <ts e="T1763" id="Seg_397" n="HIAT:w" s="T1762">Dĭ</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1764" id="Seg_400" n="HIAT:w" s="T1763">dĭzeŋdə</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1765" id="Seg_403" n="HIAT:w" s="T1764">amzittə</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1766" id="Seg_406" n="HIAT:w" s="T1765">mĭbi</ts>
                  <nts id="Seg_407" n="HIAT:ip">.</nts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1771" id="Seg_410" n="HIAT:u" s="T1766">
                  <ts e="T1767" id="Seg_412" n="HIAT:w" s="T1766">Dĭzeŋ</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1768" id="Seg_415" n="HIAT:w" s="T1767">măndəʔi:</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_417" n="HIAT:ip">"</nts>
                  <ts e="T1769" id="Seg_419" n="HIAT:w" s="T1768">Šobi</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1770" id="Seg_422" n="HIAT:w" s="T1769">döbər</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1771" id="Seg_425" n="HIAT:w" s="T1770">volk</ts>
                  <nts id="Seg_426" n="HIAT:ip">.</nts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1774" id="Seg_429" n="HIAT:u" s="T1771">
                  <ts e="T1772" id="Seg_431" n="HIAT:w" s="T1771">Ugaːndə</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1773" id="Seg_434" n="HIAT:w" s="T1772">nʼešpək</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1774" id="Seg_437" n="HIAT:w" s="T1773">golosdə</ts>
                  <nts id="Seg_438" n="HIAT:ip">"</nts>
                  <nts id="Seg_439" n="HIAT:ip">.</nts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1776" id="Seg_442" n="HIAT:u" s="T1774">
                  <nts id="Seg_443" n="HIAT:ip">"</nts>
                  <ts e="T1775" id="Seg_445" n="HIAT:w" s="T1774">Iʔ</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1776" id="Seg_448" n="HIAT:w" s="T1775">öʔlugeʔ</ts>
                  <nts id="Seg_449" n="HIAT:ip">!</nts>
                  <nts id="Seg_450" n="HIAT:ip">"</nts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1779" id="Seg_453" n="HIAT:u" s="T1776">
                  <ts e="T1777" id="Seg_455" n="HIAT:w" s="T1776">Dĭgəttə</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1778" id="Seg_458" n="HIAT:w" s="T1777">kalla</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1779" id="Seg_461" n="HIAT:w" s="T1778">dʼürbi</ts>
                  <nts id="Seg_462" n="HIAT:ip">.</nts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1782" id="Seg_465" n="HIAT:u" s="T1779">
                  <ts e="T1780" id="Seg_467" n="HIAT:w" s="T1779">Dĭ</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1781" id="Seg_470" n="HIAT:w" s="T1780">volk</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1782" id="Seg_473" n="HIAT:w" s="T1781">šobi</ts>
                  <nts id="Seg_474" n="HIAT:ip">.</nts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1785" id="Seg_477" n="HIAT:u" s="T1782">
                  <ts e="T1783" id="Seg_479" n="HIAT:w" s="T1782">Tădam</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1784" id="Seg_482" n="HIAT:w" s="T1783">golossiʔ</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1785" id="Seg_485" n="HIAT:w" s="T1784">dʼăbaktərlaʔbə</ts>
                  <nts id="Seg_486" n="HIAT:ip">.</nts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1787" id="Seg_489" n="HIAT:u" s="T1785">
                  <nts id="Seg_490" n="HIAT:ip">"</nts>
                  <ts e="T1786" id="Seg_492" n="HIAT:w" s="T1785">Öʔlugeʔ</ts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1787" id="Seg_495" n="HIAT:w" s="T1786">măna</ts>
                  <nts id="Seg_496" n="HIAT:ip">.</nts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1791" id="Seg_499" n="HIAT:u" s="T1787">
                  <ts e="T1788" id="Seg_501" n="HIAT:w" s="T1787">Miʔ</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1789" id="Seg_504" n="HIAT:w" s="T1788">šiʔnʼileʔ</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1790" id="Seg_507" n="HIAT:w" s="T1789">süt</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1791" id="Seg_510" n="HIAT:w" s="T1790">deʔpiem</ts>
                  <nts id="Seg_511" n="HIAT:ip">"</nts>
                  <nts id="Seg_512" n="HIAT:ip">.</nts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1794" id="Seg_515" n="HIAT:u" s="T1791">
                  <ts e="T1792" id="Seg_517" n="HIAT:w" s="T1791">Dĭgəttə</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1793" id="Seg_520" n="HIAT:w" s="T1792">dĭzeŋ</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1794" id="Seg_523" n="HIAT:w" s="T1793">öʔlubiʔi</ts>
                  <nts id="Seg_524" n="HIAT:ip">.</nts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1804" id="Seg_527" n="HIAT:u" s="T1794">
                  <ts e="T1795" id="Seg_529" n="HIAT:w" s="T1794">Dĭzeŋ</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1796" id="Seg_532" n="HIAT:w" s="T1795">bar</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1797" id="Seg_535" n="HIAT:w" s="T1796">amnuʔpi</ts>
                  <nts id="Seg_536" n="HIAT:ip">,</nts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1798" id="Seg_539" n="HIAT:w" s="T1797">onʼiʔ</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1799" id="Seg_542" n="HIAT:w" s="T1798">pʼešdə</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_544" n="HIAT:ip">(</nts>
                  <ts e="T1800" id="Seg_546" n="HIAT:w" s="T1799">š-</ts>
                  <nts id="Seg_547" n="HIAT:ip">)</nts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1801" id="Seg_550" n="HIAT:w" s="T1800">sʼaluʔpi</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1802" id="Seg_553" n="HIAT:w" s="T1801">i</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1803" id="Seg_556" n="HIAT:w" s="T1802">dĭn</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1804" id="Seg_559" n="HIAT:w" s="T1803">amnolaʔpi</ts>
                  <nts id="Seg_560" n="HIAT:ip">.</nts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1809" id="Seg_563" n="HIAT:u" s="T1804">
                  <ts e="T1805" id="Seg_565" n="HIAT:w" s="T1804">Iat</ts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1806" id="Seg_568" n="HIAT:w" s="T1805">šobi</ts>
                  <nts id="Seg_569" n="HIAT:ip">,</nts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1807" id="Seg_572" n="HIAT:w" s="T1806">kirgarbi</ts>
                  <nts id="Seg_573" n="HIAT:ip">,</nts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1808" id="Seg_576" n="HIAT:w" s="T1807">kirgarbi</ts>
                  <nts id="Seg_577" n="HIAT:ip">,</nts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1809" id="Seg_580" n="HIAT:w" s="T1808">naga</ts>
                  <nts id="Seg_581" n="HIAT:ip">.</nts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1813" id="Seg_584" n="HIAT:u" s="T1809">
                  <ts e="T1810" id="Seg_586" n="HIAT:w" s="T1809">Dĭgəttə</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1811" id="Seg_589" n="HIAT:w" s="T1810">šobi</ts>
                  <nts id="Seg_590" n="HIAT:ip">,</nts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1812" id="Seg_593" n="HIAT:w" s="T1811">esseŋdə</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1813" id="Seg_596" n="HIAT:w" s="T1812">naga</ts>
                  <nts id="Seg_597" n="HIAT:ip">.</nts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1822" id="Seg_600" n="HIAT:u" s="T1813">
                  <ts e="T1814" id="Seg_602" n="HIAT:w" s="T1813">Onʼiʔ</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1815" id="Seg_605" n="HIAT:w" s="T1814">kubi</ts>
                  <nts id="Seg_606" n="HIAT:ip">,</nts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1816" id="Seg_609" n="HIAT:w" s="T1815">dĭ</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1817" id="Seg_612" n="HIAT:w" s="T1816">măndə:</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_614" n="HIAT:ip">"</nts>
                  <ts e="T1818" id="Seg_616" n="HIAT:w" s="T1817">Šobi</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1819" id="Seg_619" n="HIAT:w" s="T1818">volk</ts>
                  <nts id="Seg_620" n="HIAT:ip">,</nts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1820" id="Seg_623" n="HIAT:w" s="T1819">dĭ</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1821" id="Seg_626" n="HIAT:w" s="T1820">bar</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1822" id="Seg_629" n="HIAT:w" s="T1821">amnuʔpi</ts>
                  <nts id="Seg_630" n="HIAT:ip">.</nts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1826" id="Seg_633" n="HIAT:u" s="T1822">
                  <ts e="T1823" id="Seg_635" n="HIAT:w" s="T1822">Tolʼko</ts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1824" id="Seg_638" n="HIAT:w" s="T1823">măn</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1825" id="Seg_641" n="HIAT:w" s="T1824">unnʼa</ts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1826" id="Seg_644" n="HIAT:w" s="T1825">maluʔpiam</ts>
                  <nts id="Seg_645" n="HIAT:ip">"</nts>
                  <nts id="Seg_646" n="HIAT:ip">.</nts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1833" id="Seg_649" n="HIAT:u" s="T1826">
                  <ts e="T1827" id="Seg_651" n="HIAT:w" s="T1826">Dĭgəttə</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1828" id="Seg_654" n="HIAT:w" s="T1827">kambi</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1829" id="Seg_657" n="HIAT:w" s="T1828">nʼiʔdə</ts>
                  <nts id="Seg_658" n="HIAT:ip">,</nts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1830" id="Seg_661" n="HIAT:w" s="T1829">dĭ</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1831" id="Seg_664" n="HIAT:w" s="T1830">bar</ts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1832" id="Seg_667" n="HIAT:w" s="T1831">amnolaʔ</ts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1833" id="Seg_670" n="HIAT:w" s="T1832">dʼorlaʔbə</ts>
                  <nts id="Seg_671" n="HIAT:ip">.</nts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1837" id="Seg_674" n="HIAT:u" s="T1833">
                  <nts id="Seg_675" n="HIAT:ip">"</nts>
                  <ts e="T1834" id="Seg_677" n="HIAT:w" s="T1833">Gijen</ts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1835" id="Seg_680" n="HIAT:w" s="T1834">măn</ts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_682" n="HIAT:ip">(</nts>
                  <ts e="T1836" id="Seg_684" n="HIAT:w" s="T1835">əj-</ts>
                  <nts id="Seg_685" n="HIAT:ip">)</nts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1837" id="Seg_688" n="HIAT:w" s="T1836">esseŋbə</ts>
                  <nts id="Seg_689" n="HIAT:ip">!</nts>
                  <nts id="Seg_690" n="HIAT:ip">"</nts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1840" id="Seg_693" n="HIAT:u" s="T1837">
                  <ts e="T1838" id="Seg_695" n="HIAT:w" s="T1837">Dĭ</ts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1839" id="Seg_698" n="HIAT:w" s="T1838">volk</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1840" id="Seg_701" n="HIAT:w" s="T1839">šobi</ts>
                  <nts id="Seg_702" n="HIAT:ip">.</nts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1846" id="Seg_705" n="HIAT:u" s="T1840">
                  <nts id="Seg_706" n="HIAT:ip">"</nts>
                  <ts e="T1841" id="Seg_708" n="HIAT:w" s="T1840">Ĭmbi</ts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1842" id="Seg_711" n="HIAT:w" s="T1841">dʼorlaʔbəl</ts>
                  <nts id="Seg_712" n="HIAT:ip">,</nts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1843" id="Seg_715" n="HIAT:w" s="T1842">dĭ</ts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1844" id="Seg_718" n="HIAT:w" s="T1843">ej</ts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1845" id="Seg_721" n="HIAT:w" s="T1844">măn</ts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1846" id="Seg_724" n="HIAT:w" s="T1845">ambiam</ts>
                  <nts id="Seg_725" n="HIAT:ip">"</nts>
                  <nts id="Seg_726" n="HIAT:ip">.</nts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1848" id="Seg_729" n="HIAT:u" s="T1846">
                  <ts e="T1847" id="Seg_731" n="HIAT:w" s="T1846">Kanžəbəj</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1848" id="Seg_734" n="HIAT:w" s="T1847">mănziʔ</ts>
                  <nts id="Seg_735" n="HIAT:ip">!</nts>
                  <nts id="Seg_736" n="HIAT:ip">"</nts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1851" id="Seg_739" n="HIAT:u" s="T1848">
                  <nts id="Seg_740" n="HIAT:ip">"</nts>
                  <ts e="T1849" id="Seg_742" n="HIAT:w" s="T1848">Dʼok</ts>
                  <nts id="Seg_743" n="HIAT:ip">,</nts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1850" id="Seg_746" n="HIAT:w" s="T1849">em</ts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1851" id="Seg_749" n="HIAT:w" s="T1850">kanaʔ</ts>
                  <nts id="Seg_750" n="HIAT:ip">"</nts>
                  <nts id="Seg_751" n="HIAT:ip">.</nts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1852" id="Seg_754" n="HIAT:u" s="T1851">
                  <nts id="Seg_755" n="HIAT:ip">"</nts>
                  <ts e="T1852" id="Seg_757" n="HIAT:w" s="T1851">Kanžəbəj</ts>
                  <nts id="Seg_758" n="HIAT:ip">!</nts>
                  <nts id="Seg_759" n="HIAT:ip">"</nts>
                  <nts id="Seg_760" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1853" id="Seg_762" n="HIAT:u" s="T1852">
                  <ts e="T1853" id="Seg_764" n="HIAT:w" s="T1852">Kambiʔi</ts>
                  <nts id="Seg_765" n="HIAT:ip">.</nts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1858" id="Seg_768" n="HIAT:u" s="T1853">
                  <ts e="T1854" id="Seg_770" n="HIAT:w" s="T1853">A</ts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1855" id="Seg_773" n="HIAT:w" s="T1854">dĭn</ts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1856" id="Seg_776" n="HIAT:w" s="T1855">kubiʔi</ts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1857" id="Seg_779" n="HIAT:w" s="T1856">urgo</ts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1858" id="Seg_782" n="HIAT:w" s="T1857">jama</ts>
                  <nts id="Seg_783" n="HIAT:ip">.</nts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1862" id="Seg_786" n="HIAT:u" s="T1858">
                  <ts e="T1859" id="Seg_788" n="HIAT:w" s="T1858">Dĭn</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1860" id="Seg_791" n="HIAT:w" s="T1859">bar</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1861" id="Seg_794" n="HIAT:w" s="T1860">šü</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1862" id="Seg_797" n="HIAT:w" s="T1861">iʔbəlaʔbə</ts>
                  <nts id="Seg_798" n="HIAT:ip">.</nts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1865" id="Seg_801" n="HIAT:u" s="T1862">
                  <ts e="T1863" id="Seg_803" n="HIAT:w" s="T1862">I</ts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1864" id="Seg_806" n="HIAT:w" s="T1863">kaša</ts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1865" id="Seg_809" n="HIAT:w" s="T1864">iʔbəlaʔbə</ts>
                  <nts id="Seg_810" n="HIAT:ip">.</nts>
                  <nts id="Seg_811" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1867" id="Seg_813" n="HIAT:u" s="T1865">
                  <nts id="Seg_814" n="HIAT:ip">"</nts>
                  <ts e="T1866" id="Seg_816" n="HIAT:w" s="T1865">No</ts>
                  <nts id="Seg_817" n="HIAT:ip">,</nts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1867" id="Seg_820" n="HIAT:w" s="T1866">suʔmiʔ</ts>
                  <nts id="Seg_821" n="HIAT:ip">!</nts>
                  <nts id="Seg_822" n="HIAT:ip">"</nts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1873" id="Seg_825" n="HIAT:u" s="T1867">
                  <ts e="T1868" id="Seg_827" n="HIAT:w" s="T1867">Dĭ</ts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1869" id="Seg_830" n="HIAT:w" s="T1868">suʔmiluʔpi</ts>
                  <nts id="Seg_831" n="HIAT:ip">,</nts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1870" id="Seg_834" n="HIAT:w" s="T1869">a</ts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1871" id="Seg_837" n="HIAT:w" s="T1870">dĭn</ts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1872" id="Seg_840" n="HIAT:w" s="T1871">šü</ts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1873" id="Seg_843" n="HIAT:w" s="T1872">ibi</ts>
                  <nts id="Seg_844" n="HIAT:ip">.</nts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1876" id="Seg_847" n="HIAT:u" s="T1873">
                  <ts e="T1874" id="Seg_849" n="HIAT:w" s="T1873">Dĭn</ts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1875" id="Seg_852" n="HIAT:w" s="T1874">nanət</ts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_854" n="HIAT:ip">(</nts>
                  <ts e="T1876" id="Seg_856" n="HIAT:w" s="T1875">münʼüʔluʔpi</ts>
                  <nts id="Seg_857" n="HIAT:ip">)</nts>
                  <nts id="Seg_858" n="HIAT:ip">.</nts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1880" id="Seg_861" n="HIAT:u" s="T1876">
                  <ts e="T1877" id="Seg_863" n="HIAT:w" s="T1876">A</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1878" id="Seg_866" n="HIAT:w" s="T1877">poʔtoʔi</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1879" id="Seg_869" n="HIAT:w" s="T1878">nanəndə</ts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1880" id="Seg_872" n="HIAT:w" s="T1879">supsoluʔpiʔi</ts>
                  <nts id="Seg_873" n="HIAT:ip">.</nts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1883" id="Seg_876" n="HIAT:u" s="T1880">
                  <ts e="T1881" id="Seg_878" n="HIAT:w" s="T1880">I</ts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1882" id="Seg_881" n="HIAT:w" s="T1881">nuʔmiluʔpiʔi</ts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1883" id="Seg_884" n="HIAT:w" s="T1882">maːʔndə</ts>
                  <nts id="Seg_885" n="HIAT:ip">.</nts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1887" id="Seg_888" n="HIAT:u" s="T1883">
                  <ts e="T1884" id="Seg_890" n="HIAT:w" s="T1883">Dĭgəttə</ts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1885" id="Seg_893" n="HIAT:w" s="T1884">dĭzeŋ</ts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1886" id="Seg_896" n="HIAT:w" s="T1885">kambiʔi</ts>
                  <nts id="Seg_897" n="HIAT:ip">,</nts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T1887" id="Seg_900" n="HIAT:w" s="T1886">amnobiʔi</ts>
                  <nts id="Seg_901" n="HIAT:ip">.</nts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T1888" id="Seg_904" n="HIAT:u" s="T1887">
                  <ts e="T1888" id="Seg_906" n="HIAT:w" s="T1887">Kabarləj</ts>
                  <nts id="Seg_907" n="HIAT:ip">.</nts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T1888" id="Seg_909" n="sc" s="T1670">
               <ts e="T1671" id="Seg_911" n="e" s="T1670">Amnobi </ts>
               <ts e="T1672" id="Seg_913" n="e" s="T1671">poʔto. </ts>
               <ts e="T1673" id="Seg_915" n="e" s="T1672">Dĭn </ts>
               <ts e="T1674" id="Seg_917" n="e" s="T1673">turat </ts>
               <ts e="T1675" id="Seg_919" n="e" s="T1674">ibi. </ts>
               <ts e="T1676" id="Seg_921" n="e" s="T1675">Esseŋ </ts>
               <ts e="T1677" id="Seg_923" n="e" s="T1676">deʔpi. </ts>
               <ts e="T1678" id="Seg_925" n="e" s="T1677">Dĭgəttə </ts>
               <ts e="T1679" id="Seg_927" n="e" s="T1678">kambi:" </ts>
               <ts e="T1680" id="Seg_929" n="e" s="T1679">Amnogaʔ! </ts>
               <ts e="T1681" id="Seg_931" n="e" s="T1680">Šindində </ts>
               <ts e="T1682" id="Seg_933" n="e" s="T1681">iʔ </ts>
               <ts e="T1683" id="Seg_935" n="e" s="T1682">öʔlugeʔ!" </ts>
               <ts e="T1684" id="Seg_937" n="e" s="T1683">Kambi. </ts>
               <ts e="T1685" id="Seg_939" n="e" s="T1684">Dĭgəttə </ts>
               <ts e="T1686" id="Seg_941" n="e" s="T1685">šobi. </ts>
               <ts e="T1687" id="Seg_943" n="e" s="T1686">"Kargaʔ </ts>
               <ts e="T1688" id="Seg_945" n="e" s="T1687">ajə! </ts>
               <ts e="T1689" id="Seg_947" n="e" s="T1688">Šiʔ </ts>
               <ts e="T1690" id="Seg_949" n="e" s="T1689">ial </ts>
               <ts e="T1691" id="Seg_951" n="e" s="T1690">šobi. </ts>
               <ts e="T1692" id="Seg_953" n="e" s="T1691">Süt </ts>
               <ts e="T1693" id="Seg_955" n="e" s="T1692">deʔpi </ts>
               <ts e="T1694" id="Seg_957" n="e" s="T1693">šiʔnʼileʔ!" </ts>
               <ts e="T1695" id="Seg_959" n="e" s="T1694">Dĭgəttə </ts>
               <ts e="T1696" id="Seg_961" n="e" s="T1695">dĭ </ts>
               <ts e="T1697" id="Seg_963" n="e" s="T1696">(kajbiʔi). </ts>
               <ts e="T1698" id="Seg_965" n="e" s="T1697">(Kajbiʔi). </ts>
               <ts e="T1699" id="Seg_967" n="e" s="T1698">Karbiʔi. </ts>
               <ts e="T1700" id="Seg_969" n="e" s="T1699">Dĭgəttə </ts>
               <ts e="T1701" id="Seg_971" n="e" s="T1700">dĭ </ts>
               <ts e="T1702" id="Seg_973" n="e" s="T1701">šobi. </ts>
               <ts e="T1703" id="Seg_975" n="e" s="T1702">A </ts>
               <ts e="T1704" id="Seg_977" n="e" s="T1703">(m-) </ts>
               <ts e="T1705" id="Seg_979" n="e" s="T1704">mĭbi </ts>
               <ts e="T1706" id="Seg_981" n="e" s="T1705">(dĭʔzit-) </ts>
               <ts e="T1707" id="Seg_983" n="e" s="T1706">dĭzeŋdə </ts>
               <ts e="T1708" id="Seg_985" n="e" s="T1707">(m-) </ts>
               <ts e="T1709" id="Seg_987" n="e" s="T1708">amzittə. </ts>
               <ts e="T1710" id="Seg_989" n="e" s="T1709">Dĭzeŋ </ts>
               <ts e="T1711" id="Seg_991" n="e" s="T1710">ambiʔi. </ts>
               <ts e="T1712" id="Seg_993" n="e" s="T1711">Dĭgəttə </ts>
               <ts e="T1713" id="Seg_995" n="e" s="T1712">bazoʔ </ts>
               <ts e="T1714" id="Seg_997" n="e" s="T1713">kalla </ts>
               <ts e="T1715" id="Seg_999" n="e" s="T1714">dʼürbi. </ts>
               <ts e="T1716" id="Seg_1001" n="e" s="T1715">"Amnogaʔ, </ts>
               <ts e="T1717" id="Seg_1003" n="e" s="T1716">šindində </ts>
               <ts e="T1718" id="Seg_1005" n="e" s="T1717">iʔ </ts>
               <ts e="T1719" id="Seg_1007" n="e" s="T1718">(öʔlu-) </ts>
               <ts e="T1720" id="Seg_1009" n="e" s="T1719">öʔlugeʔ!" </ts>
               <ts e="T1721" id="Seg_1011" n="e" s="T1720">Dĭgəttə </ts>
               <ts e="T1722" id="Seg_1013" n="e" s="T1721">šobi </ts>
               <ts e="T1723" id="Seg_1015" n="e" s="T1722">volk. </ts>
               <ts e="T1724" id="Seg_1017" n="e" s="T1723">Dĭ </ts>
               <ts e="T1725" id="Seg_1019" n="e" s="T1724">măndə: </ts>
               <ts e="T1726" id="Seg_1021" n="e" s="T1725">"Kargaʔ </ts>
               <ts e="T1727" id="Seg_1023" n="e" s="T1726">ajə, </ts>
               <ts e="T1728" id="Seg_1025" n="e" s="T1727">šiʔ </ts>
               <ts e="T1729" id="Seg_1027" n="e" s="T1728">ial </ts>
               <ts e="T1730" id="Seg_1029" n="e" s="T1729">šobi!" </ts>
               <ts e="T1731" id="Seg_1031" n="e" s="T1730">Dĭzeŋ: </ts>
               <ts e="T1732" id="Seg_1033" n="e" s="T1731">"Dʼok, </ts>
               <ts e="T1733" id="Seg_1035" n="e" s="T1732">dĭ </ts>
               <ts e="T1734" id="Seg_1037" n="e" s="T1733">(im-) </ts>
               <ts e="T1735" id="Seg_1039" n="e" s="T1734">ej </ts>
               <ts e="T1736" id="Seg_1041" n="e" s="T1735">miʔ </ts>
               <ts e="T1737" id="Seg_1043" n="e" s="T1736">iam. </ts>
               <ts e="T1738" id="Seg_1045" n="e" s="T1737">"(Măn=) </ts>
               <ts e="T1739" id="Seg_1047" n="e" s="T1738">Miʔ </ts>
               <ts e="T1740" id="Seg_1049" n="e" s="T1739">iam </ts>
               <ts e="T1741" id="Seg_1051" n="e" s="T1740">golosdə </ts>
               <ts e="T1742" id="Seg_1053" n="e" s="T1741">ej </ts>
               <ts e="T1743" id="Seg_1055" n="e" s="T1742">dĭrgit, </ts>
               <ts e="T1744" id="Seg_1057" n="e" s="T1743">ej </ts>
               <ts e="T1745" id="Seg_1059" n="e" s="T1744">nʼešpək, </ts>
               <ts e="T1746" id="Seg_1061" n="e" s="T1745">a </ts>
               <ts e="T1747" id="Seg_1063" n="e" s="T1746">tăn </ts>
               <ts e="T1748" id="Seg_1065" n="e" s="T1747">nʼešpək". </ts>
               <ts e="T1749" id="Seg_1067" n="e" s="T1748">Dĭgəttə </ts>
               <ts e="T1750" id="Seg_1069" n="e" s="T1749">dĭ </ts>
               <ts e="T1751" id="Seg_1071" n="e" s="T1750">kalla </ts>
               <ts e="T1752" id="Seg_1073" n="e" s="T1751">dʼürbi. </ts>
               <ts e="T1753" id="Seg_1075" n="e" s="T1752">Iat </ts>
               <ts e="T1754" id="Seg_1077" n="e" s="T1753">šobi </ts>
               <ts e="T1755" id="Seg_1079" n="e" s="T1754">dĭzeŋ. </ts>
               <ts e="T1756" id="Seg_1081" n="e" s="T1755">Bazoʔ </ts>
               <ts e="T1757" id="Seg_1083" n="e" s="T1756">kirgarlaʔbə. </ts>
               <ts e="T1758" id="Seg_1085" n="e" s="T1757">"(Karə-) </ts>
               <ts e="T1759" id="Seg_1087" n="e" s="T1758">Kargaʔ </ts>
               <ts e="T1760" id="Seg_1089" n="e" s="T1759">ajə!" </ts>
               <ts e="T1761" id="Seg_1091" n="e" s="T1760">Dĭ </ts>
               <ts e="T1762" id="Seg_1093" n="e" s="T1761">karluʔpiʔi. </ts>
               <ts e="T1763" id="Seg_1095" n="e" s="T1762">Dĭ </ts>
               <ts e="T1764" id="Seg_1097" n="e" s="T1763">dĭzeŋdə </ts>
               <ts e="T1765" id="Seg_1099" n="e" s="T1764">amzittə </ts>
               <ts e="T1766" id="Seg_1101" n="e" s="T1765">mĭbi. </ts>
               <ts e="T1767" id="Seg_1103" n="e" s="T1766">Dĭzeŋ </ts>
               <ts e="T1768" id="Seg_1105" n="e" s="T1767">măndəʔi: </ts>
               <ts e="T1769" id="Seg_1107" n="e" s="T1768">"Šobi </ts>
               <ts e="T1770" id="Seg_1109" n="e" s="T1769">döbər </ts>
               <ts e="T1771" id="Seg_1111" n="e" s="T1770">volk. </ts>
               <ts e="T1772" id="Seg_1113" n="e" s="T1771">Ugaːndə </ts>
               <ts e="T1773" id="Seg_1115" n="e" s="T1772">nʼešpək </ts>
               <ts e="T1774" id="Seg_1117" n="e" s="T1773">golosdə". </ts>
               <ts e="T1775" id="Seg_1119" n="e" s="T1774">"Iʔ </ts>
               <ts e="T1776" id="Seg_1121" n="e" s="T1775">öʔlugeʔ!" </ts>
               <ts e="T1777" id="Seg_1123" n="e" s="T1776">Dĭgəttə </ts>
               <ts e="T1778" id="Seg_1125" n="e" s="T1777">kalla </ts>
               <ts e="T1779" id="Seg_1127" n="e" s="T1778">dʼürbi. </ts>
               <ts e="T1780" id="Seg_1129" n="e" s="T1779">Dĭ </ts>
               <ts e="T1781" id="Seg_1131" n="e" s="T1780">volk </ts>
               <ts e="T1782" id="Seg_1133" n="e" s="T1781">šobi. </ts>
               <ts e="T1783" id="Seg_1135" n="e" s="T1782">Tădam </ts>
               <ts e="T1784" id="Seg_1137" n="e" s="T1783">golossiʔ </ts>
               <ts e="T1785" id="Seg_1139" n="e" s="T1784">dʼăbaktərlaʔbə. </ts>
               <ts e="T1786" id="Seg_1141" n="e" s="T1785">"Öʔlugeʔ </ts>
               <ts e="T1787" id="Seg_1143" n="e" s="T1786">măna. </ts>
               <ts e="T1788" id="Seg_1145" n="e" s="T1787">Miʔ </ts>
               <ts e="T1789" id="Seg_1147" n="e" s="T1788">šiʔnʼileʔ </ts>
               <ts e="T1790" id="Seg_1149" n="e" s="T1789">süt </ts>
               <ts e="T1791" id="Seg_1151" n="e" s="T1790">deʔpiem". </ts>
               <ts e="T1792" id="Seg_1153" n="e" s="T1791">Dĭgəttə </ts>
               <ts e="T1793" id="Seg_1155" n="e" s="T1792">dĭzeŋ </ts>
               <ts e="T1794" id="Seg_1157" n="e" s="T1793">öʔlubiʔi. </ts>
               <ts e="T1795" id="Seg_1159" n="e" s="T1794">Dĭzeŋ </ts>
               <ts e="T1796" id="Seg_1161" n="e" s="T1795">bar </ts>
               <ts e="T1797" id="Seg_1163" n="e" s="T1796">amnuʔpi, </ts>
               <ts e="T1798" id="Seg_1165" n="e" s="T1797">onʼiʔ </ts>
               <ts e="T1799" id="Seg_1167" n="e" s="T1798">pʼešdə </ts>
               <ts e="T1800" id="Seg_1169" n="e" s="T1799">(š-) </ts>
               <ts e="T1801" id="Seg_1171" n="e" s="T1800">sʼaluʔpi </ts>
               <ts e="T1802" id="Seg_1173" n="e" s="T1801">i </ts>
               <ts e="T1803" id="Seg_1175" n="e" s="T1802">dĭn </ts>
               <ts e="T1804" id="Seg_1177" n="e" s="T1803">amnolaʔpi. </ts>
               <ts e="T1805" id="Seg_1179" n="e" s="T1804">Iat </ts>
               <ts e="T1806" id="Seg_1181" n="e" s="T1805">šobi, </ts>
               <ts e="T1807" id="Seg_1183" n="e" s="T1806">kirgarbi, </ts>
               <ts e="T1808" id="Seg_1185" n="e" s="T1807">kirgarbi, </ts>
               <ts e="T1809" id="Seg_1187" n="e" s="T1808">naga. </ts>
               <ts e="T1810" id="Seg_1189" n="e" s="T1809">Dĭgəttə </ts>
               <ts e="T1811" id="Seg_1191" n="e" s="T1810">šobi, </ts>
               <ts e="T1812" id="Seg_1193" n="e" s="T1811">esseŋdə </ts>
               <ts e="T1813" id="Seg_1195" n="e" s="T1812">naga. </ts>
               <ts e="T1814" id="Seg_1197" n="e" s="T1813">Onʼiʔ </ts>
               <ts e="T1815" id="Seg_1199" n="e" s="T1814">kubi, </ts>
               <ts e="T1816" id="Seg_1201" n="e" s="T1815">dĭ </ts>
               <ts e="T1817" id="Seg_1203" n="e" s="T1816">măndə: </ts>
               <ts e="T1818" id="Seg_1205" n="e" s="T1817">"Šobi </ts>
               <ts e="T1819" id="Seg_1207" n="e" s="T1818">volk, </ts>
               <ts e="T1820" id="Seg_1209" n="e" s="T1819">dĭ </ts>
               <ts e="T1821" id="Seg_1211" n="e" s="T1820">bar </ts>
               <ts e="T1822" id="Seg_1213" n="e" s="T1821">amnuʔpi. </ts>
               <ts e="T1823" id="Seg_1215" n="e" s="T1822">Tolʼko </ts>
               <ts e="T1824" id="Seg_1217" n="e" s="T1823">măn </ts>
               <ts e="T1825" id="Seg_1219" n="e" s="T1824">unnʼa </ts>
               <ts e="T1826" id="Seg_1221" n="e" s="T1825">maluʔpiam". </ts>
               <ts e="T1827" id="Seg_1223" n="e" s="T1826">Dĭgəttə </ts>
               <ts e="T1828" id="Seg_1225" n="e" s="T1827">kambi </ts>
               <ts e="T1829" id="Seg_1227" n="e" s="T1828">nʼiʔdə, </ts>
               <ts e="T1830" id="Seg_1229" n="e" s="T1829">dĭ </ts>
               <ts e="T1831" id="Seg_1231" n="e" s="T1830">bar </ts>
               <ts e="T1832" id="Seg_1233" n="e" s="T1831">amnolaʔ </ts>
               <ts e="T1833" id="Seg_1235" n="e" s="T1832">dʼorlaʔbə. </ts>
               <ts e="T1834" id="Seg_1237" n="e" s="T1833">"Gijen </ts>
               <ts e="T1835" id="Seg_1239" n="e" s="T1834">măn </ts>
               <ts e="T1836" id="Seg_1241" n="e" s="T1835">(əj-) </ts>
               <ts e="T1837" id="Seg_1243" n="e" s="T1836">esseŋbə!" </ts>
               <ts e="T1838" id="Seg_1245" n="e" s="T1837">Dĭ </ts>
               <ts e="T1839" id="Seg_1247" n="e" s="T1838">volk </ts>
               <ts e="T1840" id="Seg_1249" n="e" s="T1839">šobi. </ts>
               <ts e="T1841" id="Seg_1251" n="e" s="T1840">"Ĭmbi </ts>
               <ts e="T1842" id="Seg_1253" n="e" s="T1841">dʼorlaʔbəl, </ts>
               <ts e="T1843" id="Seg_1255" n="e" s="T1842">dĭ </ts>
               <ts e="T1844" id="Seg_1257" n="e" s="T1843">ej </ts>
               <ts e="T1845" id="Seg_1259" n="e" s="T1844">măn </ts>
               <ts e="T1846" id="Seg_1261" n="e" s="T1845">ambiam". </ts>
               <ts e="T1847" id="Seg_1263" n="e" s="T1846">Kanžəbəj </ts>
               <ts e="T1848" id="Seg_1265" n="e" s="T1847">mănziʔ!" </ts>
               <ts e="T1849" id="Seg_1267" n="e" s="T1848">"Dʼok, </ts>
               <ts e="T1850" id="Seg_1269" n="e" s="T1849">em </ts>
               <ts e="T1851" id="Seg_1271" n="e" s="T1850">kanaʔ". </ts>
               <ts e="T1852" id="Seg_1273" n="e" s="T1851">"Kanžəbəj!" </ts>
               <ts e="T1853" id="Seg_1275" n="e" s="T1852">Kambiʔi. </ts>
               <ts e="T1854" id="Seg_1277" n="e" s="T1853">A </ts>
               <ts e="T1855" id="Seg_1279" n="e" s="T1854">dĭn </ts>
               <ts e="T1856" id="Seg_1281" n="e" s="T1855">kubiʔi </ts>
               <ts e="T1857" id="Seg_1283" n="e" s="T1856">urgo </ts>
               <ts e="T1858" id="Seg_1285" n="e" s="T1857">jama. </ts>
               <ts e="T1859" id="Seg_1287" n="e" s="T1858">Dĭn </ts>
               <ts e="T1860" id="Seg_1289" n="e" s="T1859">bar </ts>
               <ts e="T1861" id="Seg_1291" n="e" s="T1860">šü </ts>
               <ts e="T1862" id="Seg_1293" n="e" s="T1861">iʔbəlaʔbə. </ts>
               <ts e="T1863" id="Seg_1295" n="e" s="T1862">I </ts>
               <ts e="T1864" id="Seg_1297" n="e" s="T1863">kaša </ts>
               <ts e="T1865" id="Seg_1299" n="e" s="T1864">iʔbəlaʔbə. </ts>
               <ts e="T1866" id="Seg_1301" n="e" s="T1865">"No, </ts>
               <ts e="T1867" id="Seg_1303" n="e" s="T1866">suʔmiʔ!" </ts>
               <ts e="T1868" id="Seg_1305" n="e" s="T1867">Dĭ </ts>
               <ts e="T1869" id="Seg_1307" n="e" s="T1868">suʔmiluʔpi, </ts>
               <ts e="T1870" id="Seg_1309" n="e" s="T1869">a </ts>
               <ts e="T1871" id="Seg_1311" n="e" s="T1870">dĭn </ts>
               <ts e="T1872" id="Seg_1313" n="e" s="T1871">šü </ts>
               <ts e="T1873" id="Seg_1315" n="e" s="T1872">ibi. </ts>
               <ts e="T1874" id="Seg_1317" n="e" s="T1873">Dĭn </ts>
               <ts e="T1875" id="Seg_1319" n="e" s="T1874">nanət </ts>
               <ts e="T1876" id="Seg_1321" n="e" s="T1875">(münʼüʔluʔpi). </ts>
               <ts e="T1877" id="Seg_1323" n="e" s="T1876">A </ts>
               <ts e="T1878" id="Seg_1325" n="e" s="T1877">poʔtoʔi </ts>
               <ts e="T1879" id="Seg_1327" n="e" s="T1878">nanəndə </ts>
               <ts e="T1880" id="Seg_1329" n="e" s="T1879">supsoluʔpiʔi. </ts>
               <ts e="T1881" id="Seg_1331" n="e" s="T1880">I </ts>
               <ts e="T1882" id="Seg_1333" n="e" s="T1881">nuʔmiluʔpiʔi </ts>
               <ts e="T1883" id="Seg_1335" n="e" s="T1882">maːʔndə. </ts>
               <ts e="T1884" id="Seg_1337" n="e" s="T1883">Dĭgəttə </ts>
               <ts e="T1885" id="Seg_1339" n="e" s="T1884">dĭzeŋ </ts>
               <ts e="T1886" id="Seg_1341" n="e" s="T1885">kambiʔi, </ts>
               <ts e="T1887" id="Seg_1343" n="e" s="T1886">amnobiʔi. </ts>
               <ts e="T1888" id="Seg_1345" n="e" s="T1887">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T1672" id="Seg_1346" s="T1670">PKZ_196X_SevenGoatlings_flk.001 (001)</ta>
            <ta e="T1675" id="Seg_1347" s="T1672">PKZ_196X_SevenGoatlings_flk.002 (002)</ta>
            <ta e="T1677" id="Seg_1348" s="T1675">PKZ_196X_SevenGoatlings_flk.003 (003)</ta>
            <ta e="T1680" id="Seg_1349" s="T1677">PKZ_196X_SevenGoatlings_flk.004 (004)</ta>
            <ta e="T1683" id="Seg_1350" s="T1680">PKZ_196X_SevenGoatlings_flk.005 (005)</ta>
            <ta e="T1684" id="Seg_1351" s="T1683">PKZ_196X_SevenGoatlings_flk.006 (006)</ta>
            <ta e="T1686" id="Seg_1352" s="T1684">PKZ_196X_SevenGoatlings_flk.007 (007)</ta>
            <ta e="T1688" id="Seg_1353" s="T1686">PKZ_196X_SevenGoatlings_flk.008 (008)</ta>
            <ta e="T1691" id="Seg_1354" s="T1688">PKZ_196X_SevenGoatlings_flk.009 (009)</ta>
            <ta e="T1694" id="Seg_1355" s="T1691">PKZ_196X_SevenGoatlings_flk.010 (010)</ta>
            <ta e="T1697" id="Seg_1356" s="T1694">PKZ_196X_SevenGoatlings_flk.011 (011)</ta>
            <ta e="T1698" id="Seg_1357" s="T1697">PKZ_196X_SevenGoatlings_flk.012 (012)</ta>
            <ta e="T1699" id="Seg_1358" s="T1698">PKZ_196X_SevenGoatlings_flk.013 (013)</ta>
            <ta e="T1702" id="Seg_1359" s="T1699">PKZ_196X_SevenGoatlings_flk.014 (014)</ta>
            <ta e="T1709" id="Seg_1360" s="T1702">PKZ_196X_SevenGoatlings_flk.015 (015)</ta>
            <ta e="T1711" id="Seg_1361" s="T1709">PKZ_196X_SevenGoatlings_flk.016 (016)</ta>
            <ta e="T1715" id="Seg_1362" s="T1711">PKZ_196X_SevenGoatlings_flk.017 (017)</ta>
            <ta e="T1720" id="Seg_1363" s="T1715">PKZ_196X_SevenGoatlings_flk.018 (018)</ta>
            <ta e="T1723" id="Seg_1364" s="T1720">PKZ_196X_SevenGoatlings_flk.019 (019)</ta>
            <ta e="T1730" id="Seg_1365" s="T1723">PKZ_196X_SevenGoatlings_flk.020 (020)</ta>
            <ta e="T1737" id="Seg_1366" s="T1730">PKZ_196X_SevenGoatlings_flk.021 (022) </ta>
            <ta e="T1748" id="Seg_1367" s="T1737">PKZ_196X_SevenGoatlings_flk.022 (024)</ta>
            <ta e="T1752" id="Seg_1368" s="T1748">PKZ_196X_SevenGoatlings_flk.023 (025)</ta>
            <ta e="T1755" id="Seg_1369" s="T1752">PKZ_196X_SevenGoatlings_flk.024 (026)</ta>
            <ta e="T1757" id="Seg_1370" s="T1755">PKZ_196X_SevenGoatlings_flk.025 (027)</ta>
            <ta e="T1760" id="Seg_1371" s="T1757">PKZ_196X_SevenGoatlings_flk.026 (028)</ta>
            <ta e="T1762" id="Seg_1372" s="T1760">PKZ_196X_SevenGoatlings_flk.027 (029)</ta>
            <ta e="T1766" id="Seg_1373" s="T1762">PKZ_196X_SevenGoatlings_flk.028 (030)</ta>
            <ta e="T1771" id="Seg_1374" s="T1766">PKZ_196X_SevenGoatlings_flk.029 (031)</ta>
            <ta e="T1774" id="Seg_1375" s="T1771">PKZ_196X_SevenGoatlings_flk.030 (033)</ta>
            <ta e="T1776" id="Seg_1376" s="T1774">PKZ_196X_SevenGoatlings_flk.031 (034)</ta>
            <ta e="T1779" id="Seg_1377" s="T1776">PKZ_196X_SevenGoatlings_flk.032 (035)</ta>
            <ta e="T1782" id="Seg_1378" s="T1779">PKZ_196X_SevenGoatlings_flk.033 (036)</ta>
            <ta e="T1785" id="Seg_1379" s="T1782">PKZ_196X_SevenGoatlings_flk.034 (037)</ta>
            <ta e="T1787" id="Seg_1380" s="T1785">PKZ_196X_SevenGoatlings_flk.035 (038)</ta>
            <ta e="T1791" id="Seg_1381" s="T1787">PKZ_196X_SevenGoatlings_flk.036 (039)</ta>
            <ta e="T1794" id="Seg_1382" s="T1791">PKZ_196X_SevenGoatlings_flk.037 (040)</ta>
            <ta e="T1804" id="Seg_1383" s="T1794">PKZ_196X_SevenGoatlings_flk.038 (041)</ta>
            <ta e="T1809" id="Seg_1384" s="T1804">PKZ_196X_SevenGoatlings_flk.039 (042)</ta>
            <ta e="T1813" id="Seg_1385" s="T1809">PKZ_196X_SevenGoatlings_flk.040 (043)</ta>
            <ta e="T1822" id="Seg_1386" s="T1813">PKZ_196X_SevenGoatlings_flk.041 (044)</ta>
            <ta e="T1826" id="Seg_1387" s="T1822">PKZ_196X_SevenGoatlings_flk.042 (046)</ta>
            <ta e="T1833" id="Seg_1388" s="T1826">PKZ_196X_SevenGoatlings_flk.043 (047)</ta>
            <ta e="T1837" id="Seg_1389" s="T1833">PKZ_196X_SevenGoatlings_flk.044 (048)</ta>
            <ta e="T1840" id="Seg_1390" s="T1837">PKZ_196X_SevenGoatlings_flk.045 (049)</ta>
            <ta e="T1846" id="Seg_1391" s="T1840">PKZ_196X_SevenGoatlings_flk.046 (050)</ta>
            <ta e="T1848" id="Seg_1392" s="T1846">PKZ_196X_SevenGoatlings_flk.047 (051)</ta>
            <ta e="T1851" id="Seg_1393" s="T1848">PKZ_196X_SevenGoatlings_flk.048 (052)</ta>
            <ta e="T1852" id="Seg_1394" s="T1851">PKZ_196X_SevenGoatlings_flk.049 (053)</ta>
            <ta e="T1853" id="Seg_1395" s="T1852">PKZ_196X_SevenGoatlings_flk.050 (054)</ta>
            <ta e="T1858" id="Seg_1396" s="T1853">PKZ_196X_SevenGoatlings_flk.051 (055)</ta>
            <ta e="T1862" id="Seg_1397" s="T1858">PKZ_196X_SevenGoatlings_flk.052 (056)</ta>
            <ta e="T1865" id="Seg_1398" s="T1862">PKZ_196X_SevenGoatlings_flk.053 (057)</ta>
            <ta e="T1867" id="Seg_1399" s="T1865">PKZ_196X_SevenGoatlings_flk.054 (058)</ta>
            <ta e="T1873" id="Seg_1400" s="T1867">PKZ_196X_SevenGoatlings_flk.055 (059)</ta>
            <ta e="T1876" id="Seg_1401" s="T1873">PKZ_196X_SevenGoatlings_flk.056 (060)</ta>
            <ta e="T1880" id="Seg_1402" s="T1876">PKZ_196X_SevenGoatlings_flk.057 (061)</ta>
            <ta e="T1883" id="Seg_1403" s="T1880">PKZ_196X_SevenGoatlings_flk.058 (062)</ta>
            <ta e="T1887" id="Seg_1404" s="T1883">PKZ_196X_SevenGoatlings_flk.059 (063)</ta>
            <ta e="T1888" id="Seg_1405" s="T1887">PKZ_196X_SevenGoatlings_flk.060 (064)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T1672" id="Seg_1406" s="T1670">Amnobi poʔto. </ta>
            <ta e="T1675" id="Seg_1407" s="T1672">Dĭn turat ibi. </ta>
            <ta e="T1677" id="Seg_1408" s="T1675">Esseŋ deʔpi. </ta>
            <ta e="T1680" id="Seg_1409" s="T1677">Dĭgəttə kambi:" Amnogaʔ! </ta>
            <ta e="T1683" id="Seg_1410" s="T1680">Šindində iʔ öʔlugeʔ!" </ta>
            <ta e="T1684" id="Seg_1411" s="T1683">Kambi. </ta>
            <ta e="T1686" id="Seg_1412" s="T1684">Dĭgəttə šobi. </ta>
            <ta e="T1688" id="Seg_1413" s="T1686">"Kargaʔ ajə! </ta>
            <ta e="T1691" id="Seg_1414" s="T1688">Šiʔ ial šobi. </ta>
            <ta e="T1694" id="Seg_1415" s="T1691">Süt deʔpi šiʔnʼileʔ!" </ta>
            <ta e="T1697" id="Seg_1416" s="T1694">Dĭgəttə dĭ (kajbiʔi). </ta>
            <ta e="T1698" id="Seg_1417" s="T1697">(Kajbiʔi). </ta>
            <ta e="T1699" id="Seg_1418" s="T1698">Karbiʔi. </ta>
            <ta e="T1702" id="Seg_1419" s="T1699">Dĭgəttə dĭ šobi. </ta>
            <ta e="T1709" id="Seg_1420" s="T1702">A (m-) mĭbi (dĭʔzit-) dĭzeŋdə (m-) amzittə. </ta>
            <ta e="T1711" id="Seg_1421" s="T1709">Dĭzeŋ ambiʔi. </ta>
            <ta e="T1715" id="Seg_1422" s="T1711">Dĭgəttə bazoʔ kalla dʼürbi. </ta>
            <ta e="T1720" id="Seg_1423" s="T1715">"Amnogaʔ, šindində iʔ (öʔlu-) öʔlugeʔ!" </ta>
            <ta e="T1723" id="Seg_1424" s="T1720">Dĭgəttə šobi volk. </ta>
            <ta e="T1730" id="Seg_1425" s="T1723">Dĭ măndə: "Kargaʔ ajə, šiʔ ial šobi!" </ta>
            <ta e="T1737" id="Seg_1426" s="T1730">Dĭzeŋ: "Dʼok, dĭ (im-) ej miʔ iam. </ta>
            <ta e="T1748" id="Seg_1427" s="T1737">"(Măn=) Miʔ iam golosdə ej dĭrgit, ej nʼešpək, a tăn nʼešpək". </ta>
            <ta e="T1752" id="Seg_1428" s="T1748">Dĭgəttə dĭ kalla dʼürbi. </ta>
            <ta e="T1755" id="Seg_1429" s="T1752">Iat šobi dĭzeŋ. </ta>
            <ta e="T1757" id="Seg_1430" s="T1755">Bazoʔ kirgarlaʔbə. </ta>
            <ta e="T1760" id="Seg_1431" s="T1757">"(Karə-) Kargaʔ ajə!" </ta>
            <ta e="T1762" id="Seg_1432" s="T1760">Dĭ karluʔpiʔi. </ta>
            <ta e="T1766" id="Seg_1433" s="T1762">Dĭ dĭzeŋdə amzittə mĭbi. </ta>
            <ta e="T1771" id="Seg_1434" s="T1766">Dĭzeŋ măndəʔi: "Šobi döbər volk. </ta>
            <ta e="T1774" id="Seg_1435" s="T1771">Ugaːndə nʼešpək golosdə". </ta>
            <ta e="T1776" id="Seg_1436" s="T1774">"Iʔ öʔlugeʔ!" </ta>
            <ta e="T1779" id="Seg_1437" s="T1776">Dĭgəttə kalla dʼürbi. </ta>
            <ta e="T1782" id="Seg_1438" s="T1779">Dĭ volk šobi. </ta>
            <ta e="T1785" id="Seg_1439" s="T1782">Tădam golossiʔ dʼăbaktərlaʔbə. </ta>
            <ta e="T1787" id="Seg_1440" s="T1785">"Öʔlugeʔ măna. </ta>
            <ta e="T1791" id="Seg_1441" s="T1787">Miʔ šiʔnʼileʔ süt deʔpiem". </ta>
            <ta e="T1794" id="Seg_1442" s="T1791">Dĭgəttə dĭzeŋ öʔlubiʔi. </ta>
            <ta e="T1804" id="Seg_1443" s="T1794">Dĭzeŋ bar amnuʔpi, onʼiʔ pʼešdə (š-) sʼaluʔpi i dĭn amnolaʔpi. </ta>
            <ta e="T1809" id="Seg_1444" s="T1804">Iat šobi, kirgarbi, kirgarbi, naga. </ta>
            <ta e="T1813" id="Seg_1445" s="T1809">Dĭgəttə šobi, esseŋdə naga. </ta>
            <ta e="T1822" id="Seg_1446" s="T1813">Onʼiʔ kubi, dĭ măndə: "Šobi volk, dĭ bar amnuʔpi. </ta>
            <ta e="T1826" id="Seg_1447" s="T1822">Tolʼko măn unnʼa maluʔpiam". </ta>
            <ta e="T1833" id="Seg_1448" s="T1826">Dĭgəttə kambi nʼiʔdə, dĭ bar amnolaʔ dʼorlaʔbə. </ta>
            <ta e="T1837" id="Seg_1449" s="T1833">"Gijen măn (əj-) esseŋbə!" </ta>
            <ta e="T1840" id="Seg_1450" s="T1837">Dĭ volk šobi. </ta>
            <ta e="T1846" id="Seg_1451" s="T1840">"Ĭmbi dʼorlaʔbəl, dĭ ej măn ambiam". </ta>
            <ta e="T1848" id="Seg_1452" s="T1846">Kanžəbəj mănziʔ!" </ta>
            <ta e="T1851" id="Seg_1453" s="T1848">"Dʼok, em kanaʔ". </ta>
            <ta e="T1852" id="Seg_1454" s="T1851">"Kanžəbəj!" </ta>
            <ta e="T1853" id="Seg_1455" s="T1852">Kambiʔi. </ta>
            <ta e="T1858" id="Seg_1456" s="T1853">A dĭn kubiʔi urgo jama. </ta>
            <ta e="T1862" id="Seg_1457" s="T1858">Dĭn bar šü iʔbəlaʔbə. </ta>
            <ta e="T1865" id="Seg_1458" s="T1862">I kaša iʔbəlaʔbə. </ta>
            <ta e="T1867" id="Seg_1459" s="T1865">"No, suʔmiʔ!" </ta>
            <ta e="T1873" id="Seg_1460" s="T1867">Dĭ suʔmiluʔpi, a dĭn šü ibi. </ta>
            <ta e="T1876" id="Seg_1461" s="T1873">Dĭn nanət (münʼüʔluʔpi). </ta>
            <ta e="T1880" id="Seg_1462" s="T1876">A poʔtoʔi nanəndə supsoluʔpiʔi. </ta>
            <ta e="T1883" id="Seg_1463" s="T1880">I nuʔmiluʔpiʔi maːʔndə. </ta>
            <ta e="T1887" id="Seg_1464" s="T1883">Dĭgəttə dĭzeŋ kambiʔi, amnobiʔi. </ta>
            <ta e="T1888" id="Seg_1465" s="T1887">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1671" id="Seg_1466" s="T1670">amno-bi</ta>
            <ta e="T1672" id="Seg_1467" s="T1671">poʔto</ta>
            <ta e="T1673" id="Seg_1468" s="T1672">dĭ-n</ta>
            <ta e="T1674" id="Seg_1469" s="T1673">tura-t</ta>
            <ta e="T1675" id="Seg_1470" s="T1674">i-bi</ta>
            <ta e="T1676" id="Seg_1471" s="T1675">es-seŋ</ta>
            <ta e="T1677" id="Seg_1472" s="T1676">deʔ-pi</ta>
            <ta e="T1678" id="Seg_1473" s="T1677">dĭgəttə</ta>
            <ta e="T1679" id="Seg_1474" s="T1678">kam-bi</ta>
            <ta e="T1680" id="Seg_1475" s="T1679">amno-gaʔ</ta>
            <ta e="T1681" id="Seg_1476" s="T1680">šindi-ndə</ta>
            <ta e="T1682" id="Seg_1477" s="T1681">i-ʔ</ta>
            <ta e="T1683" id="Seg_1478" s="T1682">öʔlu-geʔ</ta>
            <ta e="T1684" id="Seg_1479" s="T1683">kam-bi</ta>
            <ta e="T1685" id="Seg_1480" s="T1684">dĭgəttə</ta>
            <ta e="T1686" id="Seg_1481" s="T1685">šo-bi</ta>
            <ta e="T1687" id="Seg_1482" s="T1686">kar-gaʔ</ta>
            <ta e="T1688" id="Seg_1483" s="T1687">ajə</ta>
            <ta e="T1689" id="Seg_1484" s="T1688">šiʔ</ta>
            <ta e="T1690" id="Seg_1485" s="T1689">ia-l</ta>
            <ta e="T1691" id="Seg_1486" s="T1690">šo-bi</ta>
            <ta e="T1692" id="Seg_1487" s="T1691">süt</ta>
            <ta e="T1693" id="Seg_1488" s="T1692">deʔ-pi</ta>
            <ta e="T1694" id="Seg_1489" s="T1693">šiʔnʼileʔ</ta>
            <ta e="T1695" id="Seg_1490" s="T1694">dĭgəttə</ta>
            <ta e="T1696" id="Seg_1491" s="T1695">dĭ</ta>
            <ta e="T1697" id="Seg_1492" s="T1696">kaj-bi-ʔi</ta>
            <ta e="T1698" id="Seg_1493" s="T1697">kaj-bi-ʔi</ta>
            <ta e="T1699" id="Seg_1494" s="T1698">kar-bi-ʔi</ta>
            <ta e="T1700" id="Seg_1495" s="T1699">dĭgəttə</ta>
            <ta e="T1701" id="Seg_1496" s="T1700">dĭ</ta>
            <ta e="T1702" id="Seg_1497" s="T1701">šo-bi</ta>
            <ta e="T1703" id="Seg_1498" s="T1702">a</ta>
            <ta e="T1705" id="Seg_1499" s="T1704">mĭ-bi</ta>
            <ta e="T1707" id="Seg_1500" s="T1706">dĭ-zeŋ-də</ta>
            <ta e="T1709" id="Seg_1501" s="T1708">am-zittə</ta>
            <ta e="T1710" id="Seg_1502" s="T1709">dĭ-zeŋ</ta>
            <ta e="T1711" id="Seg_1503" s="T1710">am-bi-ʔi</ta>
            <ta e="T1712" id="Seg_1504" s="T1711">dĭgəttə</ta>
            <ta e="T1713" id="Seg_1505" s="T1712">bazoʔ</ta>
            <ta e="T1714" id="Seg_1506" s="T1713">kal-la</ta>
            <ta e="T1715" id="Seg_1507" s="T1714">dʼür-bi</ta>
            <ta e="T1716" id="Seg_1508" s="T1715">amno-gaʔ</ta>
            <ta e="T1717" id="Seg_1509" s="T1716">šindi-ndə</ta>
            <ta e="T1718" id="Seg_1510" s="T1717">i-ʔ</ta>
            <ta e="T1720" id="Seg_1511" s="T1719">öʔlu-geʔ</ta>
            <ta e="T1721" id="Seg_1512" s="T1720">dĭgəttə</ta>
            <ta e="T1722" id="Seg_1513" s="T1721">šo-bi</ta>
            <ta e="T1723" id="Seg_1514" s="T1722">volk</ta>
            <ta e="T1724" id="Seg_1515" s="T1723">dĭ</ta>
            <ta e="T1725" id="Seg_1516" s="T1724">măn-də</ta>
            <ta e="T1726" id="Seg_1517" s="T1725">kar-gaʔ</ta>
            <ta e="T1727" id="Seg_1518" s="T1726">ajə</ta>
            <ta e="T1728" id="Seg_1519" s="T1727">šiʔ</ta>
            <ta e="T1729" id="Seg_1520" s="T1728">ia-l</ta>
            <ta e="T1730" id="Seg_1521" s="T1729">šo-bi</ta>
            <ta e="T1731" id="Seg_1522" s="T1730">dĭ-zeŋ</ta>
            <ta e="T1732" id="Seg_1523" s="T1731">dʼok</ta>
            <ta e="T1733" id="Seg_1524" s="T1732">dĭ</ta>
            <ta e="T1735" id="Seg_1525" s="T1734">ej</ta>
            <ta e="T1736" id="Seg_1526" s="T1735">miʔ</ta>
            <ta e="T1737" id="Seg_1527" s="T1736">ia-m</ta>
            <ta e="T1738" id="Seg_1528" s="T1737">măn</ta>
            <ta e="T1739" id="Seg_1529" s="T1738">miʔ</ta>
            <ta e="T1740" id="Seg_1530" s="T1739">ia-m</ta>
            <ta e="T1741" id="Seg_1531" s="T1740">golos-də</ta>
            <ta e="T1742" id="Seg_1532" s="T1741">ej</ta>
            <ta e="T1743" id="Seg_1533" s="T1742">dĭrgit</ta>
            <ta e="T1744" id="Seg_1534" s="T1743">ej</ta>
            <ta e="T1745" id="Seg_1535" s="T1744">nʼešpək</ta>
            <ta e="T1746" id="Seg_1536" s="T1745">a</ta>
            <ta e="T1747" id="Seg_1537" s="T1746">tăn</ta>
            <ta e="T1748" id="Seg_1538" s="T1747">nʼešpək</ta>
            <ta e="T1749" id="Seg_1539" s="T1748">dĭgəttə</ta>
            <ta e="T1750" id="Seg_1540" s="T1749">dĭ</ta>
            <ta e="T1751" id="Seg_1541" s="T1750">kal-la</ta>
            <ta e="T1752" id="Seg_1542" s="T1751">dʼür-bi</ta>
            <ta e="T1753" id="Seg_1543" s="T1752">ia-t</ta>
            <ta e="T1754" id="Seg_1544" s="T1753">šo-bi</ta>
            <ta e="T1755" id="Seg_1545" s="T1754">dĭ-zeŋ</ta>
            <ta e="T1756" id="Seg_1546" s="T1755">bazoʔ</ta>
            <ta e="T1757" id="Seg_1547" s="T1756">kirgar-laʔbə</ta>
            <ta e="T1759" id="Seg_1548" s="T1758">kar-gaʔ</ta>
            <ta e="T1760" id="Seg_1549" s="T1759">ajə</ta>
            <ta e="T1761" id="Seg_1550" s="T1760">dĭ</ta>
            <ta e="T1762" id="Seg_1551" s="T1761">kar-luʔ-pi-ʔi</ta>
            <ta e="T1763" id="Seg_1552" s="T1762">dĭ</ta>
            <ta e="T1764" id="Seg_1553" s="T1763">dĭ-zeŋ-də</ta>
            <ta e="T1765" id="Seg_1554" s="T1764">am-zittə</ta>
            <ta e="T1766" id="Seg_1555" s="T1765">mĭ-bi</ta>
            <ta e="T1767" id="Seg_1556" s="T1766">dĭ-zeŋ</ta>
            <ta e="T1768" id="Seg_1557" s="T1767">măn-də-ʔi</ta>
            <ta e="T1769" id="Seg_1558" s="T1768">šo-bi</ta>
            <ta e="T1770" id="Seg_1559" s="T1769">döbər</ta>
            <ta e="T1771" id="Seg_1560" s="T1770">volk</ta>
            <ta e="T1772" id="Seg_1561" s="T1771">ugaːndə</ta>
            <ta e="T1773" id="Seg_1562" s="T1772">nʼešpək</ta>
            <ta e="T1774" id="Seg_1563" s="T1773">golos-də</ta>
            <ta e="T1775" id="Seg_1564" s="T1774">i-ʔ</ta>
            <ta e="T1776" id="Seg_1565" s="T1775">öʔlu-geʔ</ta>
            <ta e="T1777" id="Seg_1566" s="T1776">dĭgəttə</ta>
            <ta e="T1778" id="Seg_1567" s="T1777">kal-la</ta>
            <ta e="T1779" id="Seg_1568" s="T1778">dʼür-bi</ta>
            <ta e="T1780" id="Seg_1569" s="T1779">dĭ</ta>
            <ta e="T1781" id="Seg_1570" s="T1780">volk</ta>
            <ta e="T1782" id="Seg_1571" s="T1781">šo-bi</ta>
            <ta e="T1783" id="Seg_1572" s="T1782">tădam</ta>
            <ta e="T1784" id="Seg_1573" s="T1783">golos-siʔ</ta>
            <ta e="T1785" id="Seg_1574" s="T1784">dʼăbaktər-laʔbə</ta>
            <ta e="T1786" id="Seg_1575" s="T1785">öʔlu-geʔ</ta>
            <ta e="T1787" id="Seg_1576" s="T1786">măna</ta>
            <ta e="T1788" id="Seg_1577" s="T1787">miʔ</ta>
            <ta e="T1789" id="Seg_1578" s="T1788">šiʔnʼileʔ</ta>
            <ta e="T1790" id="Seg_1579" s="T1789">süt</ta>
            <ta e="T1791" id="Seg_1580" s="T1790">deʔ-pie-m</ta>
            <ta e="T1792" id="Seg_1581" s="T1791">dĭgəttə</ta>
            <ta e="T1793" id="Seg_1582" s="T1792">dĭ-zeŋ</ta>
            <ta e="T1794" id="Seg_1583" s="T1793">öʔlu-bi-ʔi</ta>
            <ta e="T1795" id="Seg_1584" s="T1794">dĭ-zeŋ</ta>
            <ta e="T1796" id="Seg_1585" s="T1795">bar</ta>
            <ta e="T1797" id="Seg_1586" s="T1796">am-nuʔ-pi</ta>
            <ta e="T1798" id="Seg_1587" s="T1797">onʼiʔ</ta>
            <ta e="T1799" id="Seg_1588" s="T1798">pʼeš-də</ta>
            <ta e="T1801" id="Seg_1589" s="T1800">sʼa-luʔ-pi</ta>
            <ta e="T1802" id="Seg_1590" s="T1801">i</ta>
            <ta e="T1803" id="Seg_1591" s="T1802">dĭn</ta>
            <ta e="T1804" id="Seg_1592" s="T1803">amno-laʔ-pi</ta>
            <ta e="T1805" id="Seg_1593" s="T1804">ia-t</ta>
            <ta e="T1806" id="Seg_1594" s="T1805">šo-bi</ta>
            <ta e="T1807" id="Seg_1595" s="T1806">kirgar-bi</ta>
            <ta e="T1808" id="Seg_1596" s="T1807">kirgar-bi</ta>
            <ta e="T1809" id="Seg_1597" s="T1808">naga</ta>
            <ta e="T1810" id="Seg_1598" s="T1809">dĭgəttə</ta>
            <ta e="T1811" id="Seg_1599" s="T1810">šo-bi</ta>
            <ta e="T1812" id="Seg_1600" s="T1811">es-seŋ-də</ta>
            <ta e="T1813" id="Seg_1601" s="T1812">naga</ta>
            <ta e="T1814" id="Seg_1602" s="T1813">onʼiʔ</ta>
            <ta e="T1815" id="Seg_1603" s="T1814">ku-bi</ta>
            <ta e="T1816" id="Seg_1604" s="T1815">dĭ</ta>
            <ta e="T1817" id="Seg_1605" s="T1816">măn-də</ta>
            <ta e="T1818" id="Seg_1606" s="T1817">šo-bi</ta>
            <ta e="T1819" id="Seg_1607" s="T1818">volk</ta>
            <ta e="T1820" id="Seg_1608" s="T1819">dĭ</ta>
            <ta e="T1821" id="Seg_1609" s="T1820">bar</ta>
            <ta e="T1822" id="Seg_1610" s="T1821">am-nuʔ-pi</ta>
            <ta e="T1823" id="Seg_1611" s="T1822">tolʼko</ta>
            <ta e="T1824" id="Seg_1612" s="T1823">măn</ta>
            <ta e="T1825" id="Seg_1613" s="T1824">unnʼa</ta>
            <ta e="T1826" id="Seg_1614" s="T1825">ma-luʔ-pia-m</ta>
            <ta e="T1827" id="Seg_1615" s="T1826">dĭgəttə</ta>
            <ta e="T1828" id="Seg_1616" s="T1827">kam-bi</ta>
            <ta e="T1829" id="Seg_1617" s="T1828">nʼiʔdə</ta>
            <ta e="T1830" id="Seg_1618" s="T1829">dĭ</ta>
            <ta e="T1831" id="Seg_1619" s="T1830">bar</ta>
            <ta e="T1832" id="Seg_1620" s="T1831">amno-laʔ</ta>
            <ta e="T1833" id="Seg_1621" s="T1832">dʼor-laʔbə</ta>
            <ta e="T1834" id="Seg_1622" s="T1833">gijen</ta>
            <ta e="T1835" id="Seg_1623" s="T1834">măn</ta>
            <ta e="T1837" id="Seg_1624" s="T1836">es-seŋ-bə</ta>
            <ta e="T1838" id="Seg_1625" s="T1837">dĭ</ta>
            <ta e="T1839" id="Seg_1626" s="T1838">volk</ta>
            <ta e="T1840" id="Seg_1627" s="T1839">šo-bi</ta>
            <ta e="T1841" id="Seg_1628" s="T1840">ĭmbi</ta>
            <ta e="T1842" id="Seg_1629" s="T1841">dʼor-laʔbə-l</ta>
            <ta e="T1843" id="Seg_1630" s="T1842">dĭ</ta>
            <ta e="T1844" id="Seg_1631" s="T1843">ej</ta>
            <ta e="T1845" id="Seg_1632" s="T1844">măn</ta>
            <ta e="T1846" id="Seg_1633" s="T1845">am-bia-m</ta>
            <ta e="T1847" id="Seg_1634" s="T1846">kan-žə-bəj</ta>
            <ta e="T1848" id="Seg_1635" s="T1847">măn-ziʔ</ta>
            <ta e="T1849" id="Seg_1636" s="T1848">dʼok</ta>
            <ta e="T1850" id="Seg_1637" s="T1849">e-m</ta>
            <ta e="T1851" id="Seg_1638" s="T1850">kan-a-ʔ</ta>
            <ta e="T1852" id="Seg_1639" s="T1851">kan-žə-bəj</ta>
            <ta e="T1853" id="Seg_1640" s="T1852">kam-bi-ʔi</ta>
            <ta e="T1854" id="Seg_1641" s="T1853">a</ta>
            <ta e="T1855" id="Seg_1642" s="T1854">dĭn</ta>
            <ta e="T1856" id="Seg_1643" s="T1855">ku-bi-ʔi</ta>
            <ta e="T1857" id="Seg_1644" s="T1856">urgo</ta>
            <ta e="T1858" id="Seg_1645" s="T1857">jama</ta>
            <ta e="T1859" id="Seg_1646" s="T1858">dĭn</ta>
            <ta e="T1860" id="Seg_1647" s="T1859">bar</ta>
            <ta e="T1861" id="Seg_1648" s="T1860">šü</ta>
            <ta e="T1862" id="Seg_1649" s="T1861">iʔbə-laʔbə</ta>
            <ta e="T1863" id="Seg_1650" s="T1862">i</ta>
            <ta e="T1864" id="Seg_1651" s="T1863">kaša</ta>
            <ta e="T1865" id="Seg_1652" s="T1864">iʔbə-laʔbə</ta>
            <ta e="T1866" id="Seg_1653" s="T1865">no</ta>
            <ta e="T1867" id="Seg_1654" s="T1866">suʔmi-ʔ</ta>
            <ta e="T1868" id="Seg_1655" s="T1867">dĭ</ta>
            <ta e="T1869" id="Seg_1656" s="T1868">suʔmi-luʔ-pi</ta>
            <ta e="T1870" id="Seg_1657" s="T1869">a</ta>
            <ta e="T1871" id="Seg_1658" s="T1870">dĭn</ta>
            <ta e="T1872" id="Seg_1659" s="T1871">šü</ta>
            <ta e="T1873" id="Seg_1660" s="T1872">i-bi</ta>
            <ta e="T1874" id="Seg_1661" s="T1873">dĭn</ta>
            <ta e="T1875" id="Seg_1662" s="T1874">nanə-t</ta>
            <ta e="T1876" id="Seg_1663" s="T1875">münʼüʔ-luʔ-pi</ta>
            <ta e="T1877" id="Seg_1664" s="T1876">a</ta>
            <ta e="T1878" id="Seg_1665" s="T1877">poʔto-ʔi</ta>
            <ta e="T1879" id="Seg_1666" s="T1878">nanə-ndə</ta>
            <ta e="T1880" id="Seg_1667" s="T1879">supso-luʔ-pi-ʔi</ta>
            <ta e="T1881" id="Seg_1668" s="T1880">i</ta>
            <ta e="T1882" id="Seg_1669" s="T1881">nuʔmi-luʔ-pi-ʔi</ta>
            <ta e="T1883" id="Seg_1670" s="T1882">maːʔ-ndə</ta>
            <ta e="T1884" id="Seg_1671" s="T1883">dĭgəttə</ta>
            <ta e="T1885" id="Seg_1672" s="T1884">dĭ-zeŋ</ta>
            <ta e="T1886" id="Seg_1673" s="T1885">kam-bi-ʔi</ta>
            <ta e="T1887" id="Seg_1674" s="T1886">amno-bi-ʔi</ta>
            <ta e="T1888" id="Seg_1675" s="T1887">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1671" id="Seg_1676" s="T1670">amno-bi</ta>
            <ta e="T1672" id="Seg_1677" s="T1671">poʔto</ta>
            <ta e="T1673" id="Seg_1678" s="T1672">dĭ-n</ta>
            <ta e="T1674" id="Seg_1679" s="T1673">tura-t</ta>
            <ta e="T1675" id="Seg_1680" s="T1674">i-bi</ta>
            <ta e="T1676" id="Seg_1681" s="T1675">ešši-zAŋ</ta>
            <ta e="T1677" id="Seg_1682" s="T1676">det-bi</ta>
            <ta e="T1678" id="Seg_1683" s="T1677">dĭgəttə</ta>
            <ta e="T1679" id="Seg_1684" s="T1678">kan-bi</ta>
            <ta e="T1680" id="Seg_1685" s="T1679">amnə-KAʔ</ta>
            <ta e="T1681" id="Seg_1686" s="T1680">šində-gəndə</ta>
            <ta e="T1682" id="Seg_1687" s="T1681">e-ʔ</ta>
            <ta e="T1683" id="Seg_1688" s="T1682">öʔlu-KAʔ</ta>
            <ta e="T1684" id="Seg_1689" s="T1683">kan-bi</ta>
            <ta e="T1685" id="Seg_1690" s="T1684">dĭgəttə</ta>
            <ta e="T1686" id="Seg_1691" s="T1685">šo-bi</ta>
            <ta e="T1687" id="Seg_1692" s="T1686">kar-KAʔ</ta>
            <ta e="T1688" id="Seg_1693" s="T1687">ajə</ta>
            <ta e="T1689" id="Seg_1694" s="T1688">šiʔ</ta>
            <ta e="T1690" id="Seg_1695" s="T1689">ija-l</ta>
            <ta e="T1691" id="Seg_1696" s="T1690">šo-bi</ta>
            <ta e="T1692" id="Seg_1697" s="T1691">süt</ta>
            <ta e="T1693" id="Seg_1698" s="T1692">det-bi</ta>
            <ta e="T1694" id="Seg_1699" s="T1693">šiʔnʼileʔ</ta>
            <ta e="T1695" id="Seg_1700" s="T1694">dĭgəttə</ta>
            <ta e="T1696" id="Seg_1701" s="T1695">dĭ</ta>
            <ta e="T1697" id="Seg_1702" s="T1696">kaj-bi-jəʔ</ta>
            <ta e="T1698" id="Seg_1703" s="T1697">kaj-bi-jəʔ</ta>
            <ta e="T1699" id="Seg_1704" s="T1698">kar-bi-jəʔ</ta>
            <ta e="T1700" id="Seg_1705" s="T1699">dĭgəttə</ta>
            <ta e="T1701" id="Seg_1706" s="T1700">dĭ</ta>
            <ta e="T1702" id="Seg_1707" s="T1701">šo-bi</ta>
            <ta e="T1703" id="Seg_1708" s="T1702">a</ta>
            <ta e="T1705" id="Seg_1709" s="T1704">mĭ-bi</ta>
            <ta e="T1707" id="Seg_1710" s="T1706">dĭ-zAŋ-Tə</ta>
            <ta e="T1709" id="Seg_1711" s="T1708">am-zittə</ta>
            <ta e="T1710" id="Seg_1712" s="T1709">dĭ-zAŋ</ta>
            <ta e="T1711" id="Seg_1713" s="T1710">am-bi-jəʔ</ta>
            <ta e="T1712" id="Seg_1714" s="T1711">dĭgəttə</ta>
            <ta e="T1713" id="Seg_1715" s="T1712">bazoʔ</ta>
            <ta e="T1714" id="Seg_1716" s="T1713">kan-lAʔ</ta>
            <ta e="T1715" id="Seg_1717" s="T1714">tʼür-bi</ta>
            <ta e="T1716" id="Seg_1718" s="T1715">amno-KAʔ</ta>
            <ta e="T1717" id="Seg_1719" s="T1716">šində-gəndə</ta>
            <ta e="T1718" id="Seg_1720" s="T1717">e-ʔ</ta>
            <ta e="T1720" id="Seg_1721" s="T1719">öʔlu-KAʔ</ta>
            <ta e="T1721" id="Seg_1722" s="T1720">dĭgəttə</ta>
            <ta e="T1722" id="Seg_1723" s="T1721">šo-bi</ta>
            <ta e="T1723" id="Seg_1724" s="T1722">volk</ta>
            <ta e="T1724" id="Seg_1725" s="T1723">dĭ</ta>
            <ta e="T1725" id="Seg_1726" s="T1724">măn-ntə</ta>
            <ta e="T1726" id="Seg_1727" s="T1725">kar-KAʔ</ta>
            <ta e="T1727" id="Seg_1728" s="T1726">ajə</ta>
            <ta e="T1728" id="Seg_1729" s="T1727">šiʔ</ta>
            <ta e="T1729" id="Seg_1730" s="T1728">ija-l</ta>
            <ta e="T1730" id="Seg_1731" s="T1729">šo-bi</ta>
            <ta e="T1731" id="Seg_1732" s="T1730">dĭ-zAŋ</ta>
            <ta e="T1732" id="Seg_1733" s="T1731">dʼok</ta>
            <ta e="T1733" id="Seg_1734" s="T1732">dĭ</ta>
            <ta e="T1735" id="Seg_1735" s="T1734">ej</ta>
            <ta e="T1736" id="Seg_1736" s="T1735">miʔ</ta>
            <ta e="T1737" id="Seg_1737" s="T1736">ija-m</ta>
            <ta e="T1738" id="Seg_1738" s="T1737">măn</ta>
            <ta e="T1739" id="Seg_1739" s="T1738">miʔ</ta>
            <ta e="T1740" id="Seg_1740" s="T1739">ija-m</ta>
            <ta e="T1741" id="Seg_1741" s="T1740">golos-də</ta>
            <ta e="T1742" id="Seg_1742" s="T1741">ej</ta>
            <ta e="T1743" id="Seg_1743" s="T1742">dĭrgit</ta>
            <ta e="T1744" id="Seg_1744" s="T1743">ej</ta>
            <ta e="T1745" id="Seg_1745" s="T1744">nʼešpək</ta>
            <ta e="T1746" id="Seg_1746" s="T1745">a</ta>
            <ta e="T1747" id="Seg_1747" s="T1746">tăn</ta>
            <ta e="T1748" id="Seg_1748" s="T1747">nʼešpək</ta>
            <ta e="T1749" id="Seg_1749" s="T1748">dĭgəttə</ta>
            <ta e="T1750" id="Seg_1750" s="T1749">dĭ</ta>
            <ta e="T1751" id="Seg_1751" s="T1750">kan-lAʔ</ta>
            <ta e="T1752" id="Seg_1752" s="T1751">tʼür-bi</ta>
            <ta e="T1753" id="Seg_1753" s="T1752">ija-t</ta>
            <ta e="T1754" id="Seg_1754" s="T1753">šo-bi</ta>
            <ta e="T1755" id="Seg_1755" s="T1754">dĭ-zAŋ</ta>
            <ta e="T1756" id="Seg_1756" s="T1755">bazoʔ</ta>
            <ta e="T1757" id="Seg_1757" s="T1756">kirgaːr-laʔbə</ta>
            <ta e="T1759" id="Seg_1758" s="T1758">kar-KAʔ</ta>
            <ta e="T1760" id="Seg_1759" s="T1759">ajə</ta>
            <ta e="T1761" id="Seg_1760" s="T1760">dĭ</ta>
            <ta e="T1762" id="Seg_1761" s="T1761">kar-luʔbdə-bi-jəʔ</ta>
            <ta e="T1763" id="Seg_1762" s="T1762">dĭ</ta>
            <ta e="T1764" id="Seg_1763" s="T1763">dĭ-zAŋ-Tə</ta>
            <ta e="T1765" id="Seg_1764" s="T1764">am-zittə</ta>
            <ta e="T1766" id="Seg_1765" s="T1765">mĭ-bi</ta>
            <ta e="T1767" id="Seg_1766" s="T1766">dĭ-zAŋ</ta>
            <ta e="T1768" id="Seg_1767" s="T1767">măn-ntə-jəʔ</ta>
            <ta e="T1769" id="Seg_1768" s="T1768">šo-bi</ta>
            <ta e="T1770" id="Seg_1769" s="T1769">döbər</ta>
            <ta e="T1771" id="Seg_1770" s="T1770">volk</ta>
            <ta e="T1772" id="Seg_1771" s="T1771">ugaːndə</ta>
            <ta e="T1773" id="Seg_1772" s="T1772">nʼešpək</ta>
            <ta e="T1774" id="Seg_1773" s="T1773">golos-də</ta>
            <ta e="T1775" id="Seg_1774" s="T1774">e-ʔ</ta>
            <ta e="T1776" id="Seg_1775" s="T1775">öʔlu-KAʔ</ta>
            <ta e="T1777" id="Seg_1776" s="T1776">dĭgəttə</ta>
            <ta e="T1778" id="Seg_1777" s="T1777">kan-lAʔ</ta>
            <ta e="T1779" id="Seg_1778" s="T1778">tʼür-bi</ta>
            <ta e="T1780" id="Seg_1779" s="T1779">dĭ</ta>
            <ta e="T1781" id="Seg_1780" s="T1780">volk</ta>
            <ta e="T1782" id="Seg_1781" s="T1781">šo-bi</ta>
            <ta e="T1783" id="Seg_1782" s="T1782">tădam</ta>
            <ta e="T1784" id="Seg_1783" s="T1783">golos-ziʔ</ta>
            <ta e="T1785" id="Seg_1784" s="T1784">tʼăbaktər-laʔbə</ta>
            <ta e="T1786" id="Seg_1785" s="T1785">öʔlu-KAʔ</ta>
            <ta e="T1787" id="Seg_1786" s="T1786">măna</ta>
            <ta e="T1788" id="Seg_1787" s="T1787">miʔ</ta>
            <ta e="T1789" id="Seg_1788" s="T1788">šiʔnʼileʔ</ta>
            <ta e="T1790" id="Seg_1789" s="T1789">süt</ta>
            <ta e="T1791" id="Seg_1790" s="T1790">det-bi-m</ta>
            <ta e="T1792" id="Seg_1791" s="T1791">dĭgəttə</ta>
            <ta e="T1793" id="Seg_1792" s="T1792">dĭ-zAŋ</ta>
            <ta e="T1794" id="Seg_1793" s="T1793">öʔlu-bi-jəʔ</ta>
            <ta e="T1795" id="Seg_1794" s="T1794">dĭ-zAŋ</ta>
            <ta e="T1796" id="Seg_1795" s="T1795">bar</ta>
            <ta e="T1797" id="Seg_1796" s="T1796">am-luʔbdə-bi</ta>
            <ta e="T1798" id="Seg_1797" s="T1797">onʼiʔ</ta>
            <ta e="T1799" id="Seg_1798" s="T1798">pʼeːš-Tə</ta>
            <ta e="T1801" id="Seg_1799" s="T1800">sʼa-luʔbdə-bi</ta>
            <ta e="T1802" id="Seg_1800" s="T1801">i</ta>
            <ta e="T1803" id="Seg_1801" s="T1802">dĭn</ta>
            <ta e="T1804" id="Seg_1802" s="T1803">amnə-laʔbə-bi</ta>
            <ta e="T1805" id="Seg_1803" s="T1804">ija-t</ta>
            <ta e="T1806" id="Seg_1804" s="T1805">šo-bi</ta>
            <ta e="T1807" id="Seg_1805" s="T1806">kirgaːr-bi</ta>
            <ta e="T1808" id="Seg_1806" s="T1807">kirgaːr-bi</ta>
            <ta e="T1809" id="Seg_1807" s="T1808">naga</ta>
            <ta e="T1810" id="Seg_1808" s="T1809">dĭgəttə</ta>
            <ta e="T1811" id="Seg_1809" s="T1810">šo-bi</ta>
            <ta e="T1812" id="Seg_1810" s="T1811">ešši-zAŋ-Tə</ta>
            <ta e="T1813" id="Seg_1811" s="T1812">naga</ta>
            <ta e="T1814" id="Seg_1812" s="T1813">onʼiʔ</ta>
            <ta e="T1815" id="Seg_1813" s="T1814">ku-bi</ta>
            <ta e="T1816" id="Seg_1814" s="T1815">dĭ</ta>
            <ta e="T1817" id="Seg_1815" s="T1816">măn-ntə</ta>
            <ta e="T1818" id="Seg_1816" s="T1817">šo-bi</ta>
            <ta e="T1819" id="Seg_1817" s="T1818">volk</ta>
            <ta e="T1820" id="Seg_1818" s="T1819">dĭ</ta>
            <ta e="T1821" id="Seg_1819" s="T1820">bar</ta>
            <ta e="T1822" id="Seg_1820" s="T1821">am-luʔbdə-bi</ta>
            <ta e="T1823" id="Seg_1821" s="T1822">tolʼko</ta>
            <ta e="T1824" id="Seg_1822" s="T1823">măn</ta>
            <ta e="T1825" id="Seg_1823" s="T1824">unʼə</ta>
            <ta e="T1826" id="Seg_1824" s="T1825">ma-luʔbdə-bi-m</ta>
            <ta e="T1827" id="Seg_1825" s="T1826">dĭgəttə</ta>
            <ta e="T1828" id="Seg_1826" s="T1827">kan-bi</ta>
            <ta e="T1829" id="Seg_1827" s="T1828">nʼiʔdə</ta>
            <ta e="T1830" id="Seg_1828" s="T1829">dĭ</ta>
            <ta e="T1831" id="Seg_1829" s="T1830">bar</ta>
            <ta e="T1832" id="Seg_1830" s="T1831">amno-lAʔ</ta>
            <ta e="T1833" id="Seg_1831" s="T1832">tʼor-laʔbə</ta>
            <ta e="T1834" id="Seg_1832" s="T1833">gijen</ta>
            <ta e="T1835" id="Seg_1833" s="T1834">măn</ta>
            <ta e="T1837" id="Seg_1834" s="T1836">ešši-zAŋ-m</ta>
            <ta e="T1838" id="Seg_1835" s="T1837">dĭ</ta>
            <ta e="T1839" id="Seg_1836" s="T1838">volk</ta>
            <ta e="T1840" id="Seg_1837" s="T1839">šo-bi</ta>
            <ta e="T1841" id="Seg_1838" s="T1840">ĭmbi</ta>
            <ta e="T1842" id="Seg_1839" s="T1841">tʼor-laʔbə-l</ta>
            <ta e="T1843" id="Seg_1840" s="T1842">dĭ</ta>
            <ta e="T1844" id="Seg_1841" s="T1843">ej</ta>
            <ta e="T1845" id="Seg_1842" s="T1844">măn</ta>
            <ta e="T1846" id="Seg_1843" s="T1845">am-bi-m</ta>
            <ta e="T1847" id="Seg_1844" s="T1846">kan-žə-bəj</ta>
            <ta e="T1848" id="Seg_1845" s="T1847">măn-ziʔ</ta>
            <ta e="T1849" id="Seg_1846" s="T1848">dʼok</ta>
            <ta e="T1850" id="Seg_1847" s="T1849">e-m</ta>
            <ta e="T1851" id="Seg_1848" s="T1850">kan-ə-ʔ</ta>
            <ta e="T1852" id="Seg_1849" s="T1851">kan-žə-bəj</ta>
            <ta e="T1853" id="Seg_1850" s="T1852">kan-bi-jəʔ</ta>
            <ta e="T1854" id="Seg_1851" s="T1853">a</ta>
            <ta e="T1855" id="Seg_1852" s="T1854">dĭn</ta>
            <ta e="T1856" id="Seg_1853" s="T1855">ku-bi-jəʔ</ta>
            <ta e="T1857" id="Seg_1854" s="T1856">urgo</ta>
            <ta e="T1858" id="Seg_1855" s="T1857">jama</ta>
            <ta e="T1859" id="Seg_1856" s="T1858">dĭn</ta>
            <ta e="T1860" id="Seg_1857" s="T1859">bar</ta>
            <ta e="T1861" id="Seg_1858" s="T1860">šü</ta>
            <ta e="T1862" id="Seg_1859" s="T1861">iʔbö-laʔbə</ta>
            <ta e="T1863" id="Seg_1860" s="T1862">i</ta>
            <ta e="T1864" id="Seg_1861" s="T1863">kaša</ta>
            <ta e="T1865" id="Seg_1862" s="T1864">iʔbö-laʔbə</ta>
            <ta e="T1866" id="Seg_1863" s="T1865">no</ta>
            <ta e="T1867" id="Seg_1864" s="T1866">süʔmə-ʔ</ta>
            <ta e="T1868" id="Seg_1865" s="T1867">dĭ</ta>
            <ta e="T1869" id="Seg_1866" s="T1868">süʔmə-luʔbdə-bi</ta>
            <ta e="T1870" id="Seg_1867" s="T1869">a</ta>
            <ta e="T1871" id="Seg_1868" s="T1870">dĭn</ta>
            <ta e="T1872" id="Seg_1869" s="T1871">šü</ta>
            <ta e="T1873" id="Seg_1870" s="T1872">i-bi</ta>
            <ta e="T1874" id="Seg_1871" s="T1873">dĭn</ta>
            <ta e="T1875" id="Seg_1872" s="T1874">nanə-t</ta>
            <ta e="T1876" id="Seg_1873" s="T1875">münʼüʔ-luʔbdə-bi</ta>
            <ta e="T1877" id="Seg_1874" s="T1876">a</ta>
            <ta e="T1878" id="Seg_1875" s="T1877">poʔto-jəʔ</ta>
            <ta e="T1879" id="Seg_1876" s="T1878">nanə-gəndə</ta>
            <ta e="T1880" id="Seg_1877" s="T1879">supso-luʔbdə-bi-jəʔ</ta>
            <ta e="T1881" id="Seg_1878" s="T1880">i</ta>
            <ta e="T1882" id="Seg_1879" s="T1881">nuʔmə-luʔbdə-bi-jəʔ</ta>
            <ta e="T1883" id="Seg_1880" s="T1882">maʔ-gəndə</ta>
            <ta e="T1884" id="Seg_1881" s="T1883">dĭgəttə</ta>
            <ta e="T1885" id="Seg_1882" s="T1884">dĭ-zAŋ</ta>
            <ta e="T1886" id="Seg_1883" s="T1885">kan-bi-jəʔ</ta>
            <ta e="T1887" id="Seg_1884" s="T1886">amno-bi-jəʔ</ta>
            <ta e="T1888" id="Seg_1885" s="T1887">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1671" id="Seg_1886" s="T1670">live-PST.[3SG]</ta>
            <ta e="T1672" id="Seg_1887" s="T1671">goat.[NOM.SG]</ta>
            <ta e="T1673" id="Seg_1888" s="T1672">this-GEN</ta>
            <ta e="T1674" id="Seg_1889" s="T1673">house-NOM/GEN.3SG</ta>
            <ta e="T1675" id="Seg_1890" s="T1674">be-PST.[3SG]</ta>
            <ta e="T1676" id="Seg_1891" s="T1675">child-PL</ta>
            <ta e="T1677" id="Seg_1892" s="T1676">bring-PST.[3SG]</ta>
            <ta e="T1678" id="Seg_1893" s="T1677">then</ta>
            <ta e="T1679" id="Seg_1894" s="T1678">go-PST.[3SG]</ta>
            <ta e="T1680" id="Seg_1895" s="T1679">sit-IMP.2PL</ta>
            <ta e="T1681" id="Seg_1896" s="T1680">who-LAT/LOC.3SG</ta>
            <ta e="T1682" id="Seg_1897" s="T1681">NEG.AUX-IMP.2SG</ta>
            <ta e="T1683" id="Seg_1898" s="T1682">send-CNG</ta>
            <ta e="T1684" id="Seg_1899" s="T1683">go-PST.[3SG]</ta>
            <ta e="T1685" id="Seg_1900" s="T1684">then</ta>
            <ta e="T1686" id="Seg_1901" s="T1685">come-PST.[3SG]</ta>
            <ta e="T1687" id="Seg_1902" s="T1686">open-IMP.2PL</ta>
            <ta e="T1688" id="Seg_1903" s="T1687">door.[NOM.SG]</ta>
            <ta e="T1689" id="Seg_1904" s="T1688">you.PL.NOM</ta>
            <ta e="T1690" id="Seg_1905" s="T1689">mother-NOM/GEN/ACC.2SG</ta>
            <ta e="T1691" id="Seg_1906" s="T1690">come-PST.[3SG]</ta>
            <ta e="T1692" id="Seg_1907" s="T1691">milk.[NOM.SG]</ta>
            <ta e="T1693" id="Seg_1908" s="T1692">bring-PST.[3SG]</ta>
            <ta e="T1694" id="Seg_1909" s="T1693">you.PL.LAT</ta>
            <ta e="T1695" id="Seg_1910" s="T1694">then</ta>
            <ta e="T1696" id="Seg_1911" s="T1695">this.[NOM.SG]</ta>
            <ta e="T1697" id="Seg_1912" s="T1696">close-PST-3PL</ta>
            <ta e="T1698" id="Seg_1913" s="T1697">close-PST-3PL</ta>
            <ta e="T1699" id="Seg_1914" s="T1698">open-PST-3PL</ta>
            <ta e="T1700" id="Seg_1915" s="T1699">then</ta>
            <ta e="T1701" id="Seg_1916" s="T1700">this.[NOM.SG]</ta>
            <ta e="T1702" id="Seg_1917" s="T1701">come-PST.[3SG]</ta>
            <ta e="T1703" id="Seg_1918" s="T1702">and</ta>
            <ta e="T1705" id="Seg_1919" s="T1704">give-PST.[3SG]</ta>
            <ta e="T1707" id="Seg_1920" s="T1706">this-PL-LAT</ta>
            <ta e="T1709" id="Seg_1921" s="T1708">eat-INF.LAT</ta>
            <ta e="T1710" id="Seg_1922" s="T1709">this-PL</ta>
            <ta e="T1711" id="Seg_1923" s="T1710">eat-PST-3PL</ta>
            <ta e="T1712" id="Seg_1924" s="T1711">then</ta>
            <ta e="T1713" id="Seg_1925" s="T1712">again</ta>
            <ta e="T1714" id="Seg_1926" s="T1713">go-CVB</ta>
            <ta e="T1715" id="Seg_1927" s="T1714">disappear-PST.[3SG]</ta>
            <ta e="T1716" id="Seg_1928" s="T1715">sit-IMP.2PL</ta>
            <ta e="T1717" id="Seg_1929" s="T1716">who-LAT/LOC.3SG</ta>
            <ta e="T1718" id="Seg_1930" s="T1717">NEG.AUX-IMP.2SG</ta>
            <ta e="T1720" id="Seg_1931" s="T1719">send-CNG</ta>
            <ta e="T1721" id="Seg_1932" s="T1720">then</ta>
            <ta e="T1722" id="Seg_1933" s="T1721">come-PST.[3SG]</ta>
            <ta e="T1723" id="Seg_1934" s="T1722">wolf.[NOM.SG]</ta>
            <ta e="T1724" id="Seg_1935" s="T1723">this.[NOM.SG]</ta>
            <ta e="T1725" id="Seg_1936" s="T1724">say-IPFVZ.[3SG]</ta>
            <ta e="T1726" id="Seg_1937" s="T1725">open-IMP.2PL</ta>
            <ta e="T1727" id="Seg_1938" s="T1726">door.[NOM.SG]</ta>
            <ta e="T1728" id="Seg_1939" s="T1727">you.PL.NOM</ta>
            <ta e="T1729" id="Seg_1940" s="T1728">mother-NOM/GEN/ACC.2SG</ta>
            <ta e="T1730" id="Seg_1941" s="T1729">come-PST.[3SG]</ta>
            <ta e="T1731" id="Seg_1942" s="T1730">this-PL</ta>
            <ta e="T1732" id="Seg_1943" s="T1731">no</ta>
            <ta e="T1733" id="Seg_1944" s="T1732">this.[NOM.SG]</ta>
            <ta e="T1735" id="Seg_1945" s="T1734">NEG</ta>
            <ta e="T1736" id="Seg_1946" s="T1735">we.NOM</ta>
            <ta e="T1737" id="Seg_1947" s="T1736">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T1738" id="Seg_1948" s="T1737">I.NOM</ta>
            <ta e="T1739" id="Seg_1949" s="T1738">we.NOM</ta>
            <ta e="T1740" id="Seg_1950" s="T1739">mother-NOM/GEN/ACC.1SG</ta>
            <ta e="T1741" id="Seg_1951" s="T1740">voice-NOM/GEN/ACC.3SG</ta>
            <ta e="T1742" id="Seg_1952" s="T1741">NEG</ta>
            <ta e="T1743" id="Seg_1953" s="T1742">such.[NOM.SG]</ta>
            <ta e="T1744" id="Seg_1954" s="T1743">NEG</ta>
            <ta e="T1745" id="Seg_1955" s="T1744">thick.[NOM.SG]</ta>
            <ta e="T1746" id="Seg_1956" s="T1745">and</ta>
            <ta e="T1747" id="Seg_1957" s="T1746">you.GEN</ta>
            <ta e="T1748" id="Seg_1958" s="T1747">thick.[NOM.SG]</ta>
            <ta e="T1749" id="Seg_1959" s="T1748">then</ta>
            <ta e="T1750" id="Seg_1960" s="T1749">this.[NOM.SG]</ta>
            <ta e="T1751" id="Seg_1961" s="T1750">go-CVB</ta>
            <ta e="T1752" id="Seg_1962" s="T1751">disappear-PST.[3SG]</ta>
            <ta e="T1753" id="Seg_1963" s="T1752">mother-NOM/GEN.3SG</ta>
            <ta e="T1754" id="Seg_1964" s="T1753">come-PST.[3SG]</ta>
            <ta e="T1755" id="Seg_1965" s="T1754">this-PL</ta>
            <ta e="T1756" id="Seg_1966" s="T1755">again</ta>
            <ta e="T1757" id="Seg_1967" s="T1756">shout-DUR.[3SG]</ta>
            <ta e="T1759" id="Seg_1968" s="T1758">open-IMP.2PL</ta>
            <ta e="T1760" id="Seg_1969" s="T1759">door.[NOM.SG]</ta>
            <ta e="T1761" id="Seg_1970" s="T1760">this.[NOM.SG]</ta>
            <ta e="T1762" id="Seg_1971" s="T1761">open-MOM-PST-3PL</ta>
            <ta e="T1763" id="Seg_1972" s="T1762">this.[NOM.SG]</ta>
            <ta e="T1764" id="Seg_1973" s="T1763">this-PL-LAT</ta>
            <ta e="T1765" id="Seg_1974" s="T1764">eat-INF.LAT</ta>
            <ta e="T1766" id="Seg_1975" s="T1765">give-PST.[3SG]</ta>
            <ta e="T1767" id="Seg_1976" s="T1766">this-PL</ta>
            <ta e="T1768" id="Seg_1977" s="T1767">say-IPFVZ-3PL</ta>
            <ta e="T1769" id="Seg_1978" s="T1768">come-PST.[3SG]</ta>
            <ta e="T1770" id="Seg_1979" s="T1769">here</ta>
            <ta e="T1771" id="Seg_1980" s="T1770">wolf.[NOM.SG]</ta>
            <ta e="T1772" id="Seg_1981" s="T1771">very</ta>
            <ta e="T1773" id="Seg_1982" s="T1772">thick.[NOM.SG]</ta>
            <ta e="T1774" id="Seg_1983" s="T1773">voice-NOM/GEN/ACC.3SG</ta>
            <ta e="T1775" id="Seg_1984" s="T1774">NEG.AUX-IMP.2SG</ta>
            <ta e="T1776" id="Seg_1985" s="T1775">send-CNG</ta>
            <ta e="T1777" id="Seg_1986" s="T1776">then</ta>
            <ta e="T1778" id="Seg_1987" s="T1777">go-CVB</ta>
            <ta e="T1779" id="Seg_1988" s="T1778">disappear-PST.[3SG]</ta>
            <ta e="T1780" id="Seg_1989" s="T1779">this.[NOM.SG]</ta>
            <ta e="T1781" id="Seg_1990" s="T1780">wolf.[NOM.SG]</ta>
            <ta e="T1782" id="Seg_1991" s="T1781">come-PST.[3SG]</ta>
            <ta e="T1783" id="Seg_1992" s="T1782">high</ta>
            <ta e="T1784" id="Seg_1993" s="T1783">voice-INS</ta>
            <ta e="T1785" id="Seg_1994" s="T1784">speak-DUR.[3SG]</ta>
            <ta e="T1786" id="Seg_1995" s="T1785">send-IMP.2PL</ta>
            <ta e="T1787" id="Seg_1996" s="T1786">I.ACC</ta>
            <ta e="T1788" id="Seg_1997" s="T1787">we.GEN</ta>
            <ta e="T1789" id="Seg_1998" s="T1788">you.PL.ACC</ta>
            <ta e="T1790" id="Seg_1999" s="T1789">milk.[NOM.SG]</ta>
            <ta e="T1791" id="Seg_2000" s="T1790">bring-PST-1SG</ta>
            <ta e="T1792" id="Seg_2001" s="T1791">then</ta>
            <ta e="T1793" id="Seg_2002" s="T1792">this-PL</ta>
            <ta e="T1794" id="Seg_2003" s="T1793">send-PST-3PL</ta>
            <ta e="T1795" id="Seg_2004" s="T1794">this-PL</ta>
            <ta e="T1796" id="Seg_2005" s="T1795">PTCL</ta>
            <ta e="T1797" id="Seg_2006" s="T1796">eat-MOM-PST.[3SG]</ta>
            <ta e="T1798" id="Seg_2007" s="T1797">one.[NOM.SG]</ta>
            <ta e="T1799" id="Seg_2008" s="T1798">stove-LAT</ta>
            <ta e="T1801" id="Seg_2009" s="T1800">climb-MOM-PST</ta>
            <ta e="T1802" id="Seg_2010" s="T1801">and</ta>
            <ta e="T1803" id="Seg_2011" s="T1802">there</ta>
            <ta e="T1804" id="Seg_2012" s="T1803">sit-DUR-PST</ta>
            <ta e="T1805" id="Seg_2013" s="T1804">mother-NOM/GEN.3SG</ta>
            <ta e="T1806" id="Seg_2014" s="T1805">come-PST.[3SG]</ta>
            <ta e="T1807" id="Seg_2015" s="T1806">shout-PST.[3SG]</ta>
            <ta e="T1808" id="Seg_2016" s="T1807">shout-PST.[3SG]</ta>
            <ta e="T1809" id="Seg_2017" s="T1808">NEG.EX</ta>
            <ta e="T1810" id="Seg_2018" s="T1809">then</ta>
            <ta e="T1811" id="Seg_2019" s="T1810">come-PST.[3SG]</ta>
            <ta e="T1812" id="Seg_2020" s="T1811">child-PL-LAT</ta>
            <ta e="T1813" id="Seg_2021" s="T1812">NEG.EX</ta>
            <ta e="T1814" id="Seg_2022" s="T1813">one.[NOM.SG]</ta>
            <ta e="T1815" id="Seg_2023" s="T1814">find-PST.[3SG]</ta>
            <ta e="T1816" id="Seg_2024" s="T1815">this.[NOM.SG]</ta>
            <ta e="T1817" id="Seg_2025" s="T1816">say-IPFVZ.[3SG]</ta>
            <ta e="T1818" id="Seg_2026" s="T1817">come-PST.[3SG]</ta>
            <ta e="T1819" id="Seg_2027" s="T1818">wolf.[NOM.SG]</ta>
            <ta e="T1820" id="Seg_2028" s="T1819">this.[NOM.SG]</ta>
            <ta e="T1821" id="Seg_2029" s="T1820">all</ta>
            <ta e="T1822" id="Seg_2030" s="T1821">eat-MOM-PST.[3SG]</ta>
            <ta e="T1823" id="Seg_2031" s="T1822">only</ta>
            <ta e="T1824" id="Seg_2032" s="T1823">I.NOM</ta>
            <ta e="T1825" id="Seg_2033" s="T1824">alone</ta>
            <ta e="T1826" id="Seg_2034" s="T1825">remain-MOM-PST-1SG</ta>
            <ta e="T1827" id="Seg_2035" s="T1826">then</ta>
            <ta e="T1828" id="Seg_2036" s="T1827">go-PST.[3SG]</ta>
            <ta e="T1829" id="Seg_2037" s="T1828">outwards</ta>
            <ta e="T1830" id="Seg_2038" s="T1829">this.[NOM.SG]</ta>
            <ta e="T1831" id="Seg_2039" s="T1830">PTCL</ta>
            <ta e="T1832" id="Seg_2040" s="T1831">sit-CVB</ta>
            <ta e="T1833" id="Seg_2041" s="T1832">cry-DUR.[3SG]</ta>
            <ta e="T1834" id="Seg_2042" s="T1833">where</ta>
            <ta e="T1835" id="Seg_2043" s="T1834">I.GEN</ta>
            <ta e="T1837" id="Seg_2044" s="T1836">child-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T1838" id="Seg_2045" s="T1837">this.[NOM.SG]</ta>
            <ta e="T1839" id="Seg_2046" s="T1838">wolf.[NOM.SG]</ta>
            <ta e="T1840" id="Seg_2047" s="T1839">come-PST.[3SG]</ta>
            <ta e="T1841" id="Seg_2048" s="T1840">what.[NOM.SG]</ta>
            <ta e="T1842" id="Seg_2049" s="T1841">cry-DUR-2SG</ta>
            <ta e="T1843" id="Seg_2050" s="T1842">this.[NOM.SG]</ta>
            <ta e="T1844" id="Seg_2051" s="T1843">NEG</ta>
            <ta e="T1845" id="Seg_2052" s="T1844">I.NOM</ta>
            <ta e="T1846" id="Seg_2053" s="T1845">eat-PST-1SG</ta>
            <ta e="T1847" id="Seg_2054" s="T1846">go-OPT.DU/PL-1DU</ta>
            <ta e="T1848" id="Seg_2055" s="T1847">I-INS</ta>
            <ta e="T1849" id="Seg_2056" s="T1848">no</ta>
            <ta e="T1850" id="Seg_2057" s="T1849">NEG.AUX-1SG</ta>
            <ta e="T1851" id="Seg_2058" s="T1850">go-EP-CNG</ta>
            <ta e="T1852" id="Seg_2059" s="T1851">go-OPT.DU/PL-1DU</ta>
            <ta e="T1853" id="Seg_2060" s="T1852">go-PST-3PL</ta>
            <ta e="T1854" id="Seg_2061" s="T1853">and</ta>
            <ta e="T1855" id="Seg_2062" s="T1854">there</ta>
            <ta e="T1856" id="Seg_2063" s="T1855">see-PST-3PL</ta>
            <ta e="T1857" id="Seg_2064" s="T1856">big</ta>
            <ta e="T1858" id="Seg_2065" s="T1857">pit.[NOM.SG]</ta>
            <ta e="T1859" id="Seg_2066" s="T1858">there</ta>
            <ta e="T1860" id="Seg_2067" s="T1859">PTCL</ta>
            <ta e="T1861" id="Seg_2068" s="T1860">fire.[NOM.SG]</ta>
            <ta e="T1862" id="Seg_2069" s="T1861">lie-DUR.[3SG]</ta>
            <ta e="T1863" id="Seg_2070" s="T1862">and</ta>
            <ta e="T1864" id="Seg_2071" s="T1863">porridge.[NOM.SG]</ta>
            <ta e="T1865" id="Seg_2072" s="T1864">lie-DUR.[3SG]</ta>
            <ta e="T1866" id="Seg_2073" s="T1865">well</ta>
            <ta e="T1867" id="Seg_2074" s="T1866">jump-IMP.2SG</ta>
            <ta e="T1868" id="Seg_2075" s="T1867">this.[NOM.SG]</ta>
            <ta e="T1869" id="Seg_2076" s="T1868">jump-MOM-PST.[3SG]</ta>
            <ta e="T1870" id="Seg_2077" s="T1869">and</ta>
            <ta e="T1871" id="Seg_2078" s="T1870">there</ta>
            <ta e="T1872" id="Seg_2079" s="T1871">fire.[NOM.SG]</ta>
            <ta e="T1873" id="Seg_2080" s="T1872">be-PST.[3SG]</ta>
            <ta e="T1874" id="Seg_2081" s="T1873">there</ta>
            <ta e="T1875" id="Seg_2082" s="T1874">belly-NOM/GEN.3SG</ta>
            <ta e="T1876" id="Seg_2083" s="T1875">bend-MOM-PST.[3SG]</ta>
            <ta e="T1877" id="Seg_2084" s="T1876">and</ta>
            <ta e="T1878" id="Seg_2085" s="T1877">goat-PL</ta>
            <ta e="T1879" id="Seg_2086" s="T1878">belly-LAT/LOC.3SG</ta>
            <ta e="T1880" id="Seg_2087" s="T1879">depart-MOM-PST-3PL</ta>
            <ta e="T1881" id="Seg_2088" s="T1880">and</ta>
            <ta e="T1882" id="Seg_2089" s="T1881">run-MOM-PST-3PL</ta>
            <ta e="T1883" id="Seg_2090" s="T1882">tent-LAT/LOC.3SG</ta>
            <ta e="T1884" id="Seg_2091" s="T1883">then</ta>
            <ta e="T1885" id="Seg_2092" s="T1884">this-PL</ta>
            <ta e="T1886" id="Seg_2093" s="T1885">go-PST-3PL</ta>
            <ta e="T1887" id="Seg_2094" s="T1886">live-PST-3PL</ta>
            <ta e="T1888" id="Seg_2095" s="T1887">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1671" id="Seg_2096" s="T1670">жить-PST.[3SG]</ta>
            <ta e="T1672" id="Seg_2097" s="T1671">коза.[NOM.SG]</ta>
            <ta e="T1673" id="Seg_2098" s="T1672">этот-GEN</ta>
            <ta e="T1674" id="Seg_2099" s="T1673">дом-NOM/GEN.3SG</ta>
            <ta e="T1675" id="Seg_2100" s="T1674">быть-PST.[3SG]</ta>
            <ta e="T1676" id="Seg_2101" s="T1675">ребенок-PL</ta>
            <ta e="T1677" id="Seg_2102" s="T1676">принести-PST.[3SG]</ta>
            <ta e="T1678" id="Seg_2103" s="T1677">тогда</ta>
            <ta e="T1679" id="Seg_2104" s="T1678">пойти-PST.[3SG]</ta>
            <ta e="T1680" id="Seg_2105" s="T1679">сидеть-IMP.2PL</ta>
            <ta e="T1681" id="Seg_2106" s="T1680">кто-LAT/LOC.3SG</ta>
            <ta e="T1682" id="Seg_2107" s="T1681">NEG.AUX-IMP.2SG</ta>
            <ta e="T1683" id="Seg_2108" s="T1682">посылать-CNG</ta>
            <ta e="T1684" id="Seg_2109" s="T1683">пойти-PST.[3SG]</ta>
            <ta e="T1685" id="Seg_2110" s="T1684">тогда</ta>
            <ta e="T1686" id="Seg_2111" s="T1685">прийти-PST.[3SG]</ta>
            <ta e="T1687" id="Seg_2112" s="T1686">открыть-IMP.2PL</ta>
            <ta e="T1688" id="Seg_2113" s="T1687">дверь.[NOM.SG]</ta>
            <ta e="T1689" id="Seg_2114" s="T1688">вы.NOM</ta>
            <ta e="T1690" id="Seg_2115" s="T1689">мать-NOM/GEN/ACC.2SG</ta>
            <ta e="T1691" id="Seg_2116" s="T1690">прийти-PST.[3SG]</ta>
            <ta e="T1692" id="Seg_2117" s="T1691">молоко.[NOM.SG]</ta>
            <ta e="T1693" id="Seg_2118" s="T1692">принести-PST.[3SG]</ta>
            <ta e="T1694" id="Seg_2119" s="T1693">вы.LAT</ta>
            <ta e="T1695" id="Seg_2120" s="T1694">тогда</ta>
            <ta e="T1696" id="Seg_2121" s="T1695">этот.[NOM.SG]</ta>
            <ta e="T1697" id="Seg_2122" s="T1696">закрыть-PST-3PL</ta>
            <ta e="T1698" id="Seg_2123" s="T1697">закрыть-PST-3PL</ta>
            <ta e="T1699" id="Seg_2124" s="T1698">открыть-PST-3PL</ta>
            <ta e="T1700" id="Seg_2125" s="T1699">тогда</ta>
            <ta e="T1701" id="Seg_2126" s="T1700">этот.[NOM.SG]</ta>
            <ta e="T1702" id="Seg_2127" s="T1701">прийти-PST.[3SG]</ta>
            <ta e="T1703" id="Seg_2128" s="T1702">а</ta>
            <ta e="T1705" id="Seg_2129" s="T1704">дать-PST.[3SG]</ta>
            <ta e="T1707" id="Seg_2130" s="T1706">этот-PL-LAT</ta>
            <ta e="T1709" id="Seg_2131" s="T1708">съесть-INF.LAT</ta>
            <ta e="T1710" id="Seg_2132" s="T1709">этот-PL</ta>
            <ta e="T1711" id="Seg_2133" s="T1710">съесть-PST-3PL</ta>
            <ta e="T1712" id="Seg_2134" s="T1711">тогда</ta>
            <ta e="T1713" id="Seg_2135" s="T1712">опять</ta>
            <ta e="T1714" id="Seg_2136" s="T1713">пойти-CVB</ta>
            <ta e="T1715" id="Seg_2137" s="T1714">исчезнуть-PST.[3SG]</ta>
            <ta e="T1716" id="Seg_2138" s="T1715">сидеть-IMP.2PL</ta>
            <ta e="T1717" id="Seg_2139" s="T1716">кто-LAT/LOC.3SG</ta>
            <ta e="T1718" id="Seg_2140" s="T1717">NEG.AUX-IMP.2SG</ta>
            <ta e="T1720" id="Seg_2141" s="T1719">посылать-CNG</ta>
            <ta e="T1721" id="Seg_2142" s="T1720">тогда</ta>
            <ta e="T1722" id="Seg_2143" s="T1721">прийти-PST.[3SG]</ta>
            <ta e="T1723" id="Seg_2144" s="T1722">волк.[NOM.SG]</ta>
            <ta e="T1724" id="Seg_2145" s="T1723">этот.[NOM.SG]</ta>
            <ta e="T1725" id="Seg_2146" s="T1724">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1726" id="Seg_2147" s="T1725">открыть-IMP.2PL</ta>
            <ta e="T1727" id="Seg_2148" s="T1726">дверь.[NOM.SG]</ta>
            <ta e="T1728" id="Seg_2149" s="T1727">вы.NOM</ta>
            <ta e="T1729" id="Seg_2150" s="T1728">мать-NOM/GEN/ACC.2SG</ta>
            <ta e="T1730" id="Seg_2151" s="T1729">прийти-PST.[3SG]</ta>
            <ta e="T1731" id="Seg_2152" s="T1730">этот-PL</ta>
            <ta e="T1732" id="Seg_2153" s="T1731">нет</ta>
            <ta e="T1733" id="Seg_2154" s="T1732">этот.[NOM.SG]</ta>
            <ta e="T1735" id="Seg_2155" s="T1734">NEG</ta>
            <ta e="T1736" id="Seg_2156" s="T1735">мы.NOM</ta>
            <ta e="T1737" id="Seg_2157" s="T1736">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T1738" id="Seg_2158" s="T1737">я.NOM</ta>
            <ta e="T1739" id="Seg_2159" s="T1738">мы.NOM</ta>
            <ta e="T1740" id="Seg_2160" s="T1739">мать-NOM/GEN/ACC.1SG</ta>
            <ta e="T1741" id="Seg_2161" s="T1740">голос-NOM/GEN/ACC.3SG</ta>
            <ta e="T1742" id="Seg_2162" s="T1741">NEG</ta>
            <ta e="T1743" id="Seg_2163" s="T1742">такой.[NOM.SG]</ta>
            <ta e="T1744" id="Seg_2164" s="T1743">NEG</ta>
            <ta e="T1745" id="Seg_2165" s="T1744">толстый.[NOM.SG]</ta>
            <ta e="T1746" id="Seg_2166" s="T1745">а</ta>
            <ta e="T1747" id="Seg_2167" s="T1746">ты.GEN</ta>
            <ta e="T1748" id="Seg_2168" s="T1747">толстый.[NOM.SG]</ta>
            <ta e="T1749" id="Seg_2169" s="T1748">тогда</ta>
            <ta e="T1750" id="Seg_2170" s="T1749">этот.[NOM.SG]</ta>
            <ta e="T1751" id="Seg_2171" s="T1750">пойти-CVB</ta>
            <ta e="T1752" id="Seg_2172" s="T1751">исчезнуть-PST.[3SG]</ta>
            <ta e="T1753" id="Seg_2173" s="T1752">мать-NOM/GEN.3SG</ta>
            <ta e="T1754" id="Seg_2174" s="T1753">прийти-PST.[3SG]</ta>
            <ta e="T1755" id="Seg_2175" s="T1754">этот-PL</ta>
            <ta e="T1756" id="Seg_2176" s="T1755">опять</ta>
            <ta e="T1757" id="Seg_2177" s="T1756">кричать-DUR.[3SG]</ta>
            <ta e="T1759" id="Seg_2178" s="T1758">открыть-IMP.2PL</ta>
            <ta e="T1760" id="Seg_2179" s="T1759">дверь.[NOM.SG]</ta>
            <ta e="T1761" id="Seg_2180" s="T1760">этот.[NOM.SG]</ta>
            <ta e="T1762" id="Seg_2181" s="T1761">открыть-MOM-PST-3PL</ta>
            <ta e="T1763" id="Seg_2182" s="T1762">этот.[NOM.SG]</ta>
            <ta e="T1764" id="Seg_2183" s="T1763">этот-PL-LAT</ta>
            <ta e="T1765" id="Seg_2184" s="T1764">съесть-INF.LAT</ta>
            <ta e="T1766" id="Seg_2185" s="T1765">дать-PST.[3SG]</ta>
            <ta e="T1767" id="Seg_2186" s="T1766">этот-PL</ta>
            <ta e="T1768" id="Seg_2187" s="T1767">сказать-IPFVZ-3PL</ta>
            <ta e="T1769" id="Seg_2188" s="T1768">прийти-PST.[3SG]</ta>
            <ta e="T1770" id="Seg_2189" s="T1769">здесь</ta>
            <ta e="T1771" id="Seg_2190" s="T1770">волк.[NOM.SG]</ta>
            <ta e="T1772" id="Seg_2191" s="T1771">очень</ta>
            <ta e="T1773" id="Seg_2192" s="T1772">толстый.[NOM.SG]</ta>
            <ta e="T1774" id="Seg_2193" s="T1773">голос-NOM/GEN/ACC.3SG</ta>
            <ta e="T1775" id="Seg_2194" s="T1774">NEG.AUX-IMP.2SG</ta>
            <ta e="T1776" id="Seg_2195" s="T1775">посылать-CNG</ta>
            <ta e="T1777" id="Seg_2196" s="T1776">тогда</ta>
            <ta e="T1778" id="Seg_2197" s="T1777">пойти-CVB</ta>
            <ta e="T1779" id="Seg_2198" s="T1778">исчезнуть-PST.[3SG]</ta>
            <ta e="T1780" id="Seg_2199" s="T1779">этот.[NOM.SG]</ta>
            <ta e="T1781" id="Seg_2200" s="T1780">волк.[NOM.SG]</ta>
            <ta e="T1782" id="Seg_2201" s="T1781">прийти-PST.[3SG]</ta>
            <ta e="T1783" id="Seg_2202" s="T1782">высокий</ta>
            <ta e="T1784" id="Seg_2203" s="T1783">голос-INS</ta>
            <ta e="T1785" id="Seg_2204" s="T1784">говорить-DUR.[3SG]</ta>
            <ta e="T1786" id="Seg_2205" s="T1785">посылать-IMP.2PL</ta>
            <ta e="T1787" id="Seg_2206" s="T1786">я.ACC</ta>
            <ta e="T1788" id="Seg_2207" s="T1787">мы.GEN</ta>
            <ta e="T1789" id="Seg_2208" s="T1788">вы.ACC</ta>
            <ta e="T1790" id="Seg_2209" s="T1789">молоко.[NOM.SG]</ta>
            <ta e="T1791" id="Seg_2210" s="T1790">принести-PST-1SG</ta>
            <ta e="T1792" id="Seg_2211" s="T1791">тогда</ta>
            <ta e="T1793" id="Seg_2212" s="T1792">этот-PL</ta>
            <ta e="T1794" id="Seg_2213" s="T1793">посылать-PST-3PL</ta>
            <ta e="T1795" id="Seg_2214" s="T1794">этот-PL</ta>
            <ta e="T1796" id="Seg_2215" s="T1795">PTCL</ta>
            <ta e="T1797" id="Seg_2216" s="T1796">съесть-MOM-PST.[3SG]</ta>
            <ta e="T1798" id="Seg_2217" s="T1797">один.[NOM.SG]</ta>
            <ta e="T1799" id="Seg_2218" s="T1798">печь-LAT</ta>
            <ta e="T1801" id="Seg_2219" s="T1800">влезать-MOM-PST</ta>
            <ta e="T1802" id="Seg_2220" s="T1801">и</ta>
            <ta e="T1803" id="Seg_2221" s="T1802">там</ta>
            <ta e="T1804" id="Seg_2222" s="T1803">сидеть-DUR-PST</ta>
            <ta e="T1805" id="Seg_2223" s="T1804">мать-NOM/GEN.3SG</ta>
            <ta e="T1806" id="Seg_2224" s="T1805">прийти-PST.[3SG]</ta>
            <ta e="T1807" id="Seg_2225" s="T1806">кричать-PST.[3SG]</ta>
            <ta e="T1808" id="Seg_2226" s="T1807">кричать-PST.[3SG]</ta>
            <ta e="T1809" id="Seg_2227" s="T1808">NEG.EX</ta>
            <ta e="T1810" id="Seg_2228" s="T1809">тогда</ta>
            <ta e="T1811" id="Seg_2229" s="T1810">прийти-PST.[3SG]</ta>
            <ta e="T1812" id="Seg_2230" s="T1811">ребенок-PL-LAT</ta>
            <ta e="T1813" id="Seg_2231" s="T1812">NEG.EX</ta>
            <ta e="T1814" id="Seg_2232" s="T1813">один.[NOM.SG]</ta>
            <ta e="T1815" id="Seg_2233" s="T1814">найти-PST.[3SG]</ta>
            <ta e="T1816" id="Seg_2234" s="T1815">этот.[NOM.SG]</ta>
            <ta e="T1817" id="Seg_2235" s="T1816">сказать-IPFVZ.[3SG]</ta>
            <ta e="T1818" id="Seg_2236" s="T1817">прийти-PST.[3SG]</ta>
            <ta e="T1819" id="Seg_2237" s="T1818">волк.[NOM.SG]</ta>
            <ta e="T1820" id="Seg_2238" s="T1819">этот.[NOM.SG]</ta>
            <ta e="T1821" id="Seg_2239" s="T1820">весь</ta>
            <ta e="T1822" id="Seg_2240" s="T1821">съесть-MOM-PST.[3SG]</ta>
            <ta e="T1823" id="Seg_2241" s="T1822">только</ta>
            <ta e="T1824" id="Seg_2242" s="T1823">я.NOM</ta>
            <ta e="T1825" id="Seg_2243" s="T1824">один</ta>
            <ta e="T1826" id="Seg_2244" s="T1825">остаться-MOM-PST-1SG</ta>
            <ta e="T1827" id="Seg_2245" s="T1826">тогда</ta>
            <ta e="T1828" id="Seg_2246" s="T1827">пойти-PST.[3SG]</ta>
            <ta e="T1829" id="Seg_2247" s="T1828">наружу</ta>
            <ta e="T1830" id="Seg_2248" s="T1829">этот.[NOM.SG]</ta>
            <ta e="T1831" id="Seg_2249" s="T1830">PTCL</ta>
            <ta e="T1832" id="Seg_2250" s="T1831">сидеть-CVB</ta>
            <ta e="T1833" id="Seg_2251" s="T1832">плакать-DUR.[3SG]</ta>
            <ta e="T1834" id="Seg_2252" s="T1833">где</ta>
            <ta e="T1835" id="Seg_2253" s="T1834">я.GEN</ta>
            <ta e="T1837" id="Seg_2254" s="T1836">ребенок-PL-NOM/GEN/ACC.1SG</ta>
            <ta e="T1838" id="Seg_2255" s="T1837">этот.[NOM.SG]</ta>
            <ta e="T1839" id="Seg_2256" s="T1838">волк.[NOM.SG]</ta>
            <ta e="T1840" id="Seg_2257" s="T1839">прийти-PST.[3SG]</ta>
            <ta e="T1841" id="Seg_2258" s="T1840">что.[NOM.SG]</ta>
            <ta e="T1842" id="Seg_2259" s="T1841">плакать-DUR-2SG</ta>
            <ta e="T1843" id="Seg_2260" s="T1842">этот.[NOM.SG]</ta>
            <ta e="T1844" id="Seg_2261" s="T1843">NEG</ta>
            <ta e="T1845" id="Seg_2262" s="T1844">я.NOM</ta>
            <ta e="T1846" id="Seg_2263" s="T1845">съесть-PST-1SG</ta>
            <ta e="T1847" id="Seg_2264" s="T1846">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T1848" id="Seg_2265" s="T1847">я-INS</ta>
            <ta e="T1849" id="Seg_2266" s="T1848">нет</ta>
            <ta e="T1850" id="Seg_2267" s="T1849">NEG.AUX-1SG</ta>
            <ta e="T1851" id="Seg_2268" s="T1850">пойти-EP-CNG</ta>
            <ta e="T1852" id="Seg_2269" s="T1851">пойти-OPT.DU/PL-1DU</ta>
            <ta e="T1853" id="Seg_2270" s="T1852">пойти-PST-3PL</ta>
            <ta e="T1854" id="Seg_2271" s="T1853">а</ta>
            <ta e="T1855" id="Seg_2272" s="T1854">там</ta>
            <ta e="T1856" id="Seg_2273" s="T1855">видеть-PST-3PL</ta>
            <ta e="T1857" id="Seg_2274" s="T1856">большой</ta>
            <ta e="T1858" id="Seg_2275" s="T1857">яма.[NOM.SG]</ta>
            <ta e="T1859" id="Seg_2276" s="T1858">там</ta>
            <ta e="T1860" id="Seg_2277" s="T1859">PTCL</ta>
            <ta e="T1861" id="Seg_2278" s="T1860">огонь.[NOM.SG]</ta>
            <ta e="T1862" id="Seg_2279" s="T1861">лежать-DUR.[3SG]</ta>
            <ta e="T1863" id="Seg_2280" s="T1862">и</ta>
            <ta e="T1864" id="Seg_2281" s="T1863">каша.[NOM.SG]</ta>
            <ta e="T1865" id="Seg_2282" s="T1864">лежать-DUR.[3SG]</ta>
            <ta e="T1866" id="Seg_2283" s="T1865">ну</ta>
            <ta e="T1867" id="Seg_2284" s="T1866">прыгнуть-IMP.2SG</ta>
            <ta e="T1868" id="Seg_2285" s="T1867">этот.[NOM.SG]</ta>
            <ta e="T1869" id="Seg_2286" s="T1868">прыгнуть-MOM-PST.[3SG]</ta>
            <ta e="T1870" id="Seg_2287" s="T1869">а</ta>
            <ta e="T1871" id="Seg_2288" s="T1870">там</ta>
            <ta e="T1872" id="Seg_2289" s="T1871">огонь.[NOM.SG]</ta>
            <ta e="T1873" id="Seg_2290" s="T1872">быть-PST.[3SG]</ta>
            <ta e="T1874" id="Seg_2291" s="T1873">там</ta>
            <ta e="T1875" id="Seg_2292" s="T1874">живот-NOM/GEN.3SG</ta>
            <ta e="T1876" id="Seg_2293" s="T1875">сгибать-MOM-PST.[3SG]</ta>
            <ta e="T1877" id="Seg_2294" s="T1876">а</ta>
            <ta e="T1878" id="Seg_2295" s="T1877">коза-PL</ta>
            <ta e="T1879" id="Seg_2296" s="T1878">живот-LAT/LOC.3SG</ta>
            <ta e="T1880" id="Seg_2297" s="T1879">уйти-MOM-PST-3PL</ta>
            <ta e="T1881" id="Seg_2298" s="T1880">и</ta>
            <ta e="T1882" id="Seg_2299" s="T1881">бежать-MOM-PST-3PL</ta>
            <ta e="T1883" id="Seg_2300" s="T1882">чум-LAT/LOC.3SG</ta>
            <ta e="T1884" id="Seg_2301" s="T1883">тогда</ta>
            <ta e="T1885" id="Seg_2302" s="T1884">этот-PL</ta>
            <ta e="T1886" id="Seg_2303" s="T1885">пойти-PST-3PL</ta>
            <ta e="T1887" id="Seg_2304" s="T1886">жить-PST-3PL</ta>
            <ta e="T1888" id="Seg_2305" s="T1887">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1671" id="Seg_2306" s="T1670">v-v:tense-v:pn</ta>
            <ta e="T1672" id="Seg_2307" s="T1671">n-n:case</ta>
            <ta e="T1673" id="Seg_2308" s="T1672">dempro-n:case</ta>
            <ta e="T1674" id="Seg_2309" s="T1673">n-n:case.poss</ta>
            <ta e="T1675" id="Seg_2310" s="T1674">v-v:tense-v:pn</ta>
            <ta e="T1676" id="Seg_2311" s="T1675">n-n:num</ta>
            <ta e="T1677" id="Seg_2312" s="T1676">v-v:tense-v:pn</ta>
            <ta e="T1678" id="Seg_2313" s="T1677">adv</ta>
            <ta e="T1679" id="Seg_2314" s="T1678">v-v:tense-v:pn</ta>
            <ta e="T1680" id="Seg_2315" s="T1679">v-v:mood.pn</ta>
            <ta e="T1681" id="Seg_2316" s="T1680">que-n:case.poss</ta>
            <ta e="T1682" id="Seg_2317" s="T1681">aux-v:mood.pn</ta>
            <ta e="T1683" id="Seg_2318" s="T1682">v-v:mood.pn</ta>
            <ta e="T1684" id="Seg_2319" s="T1683">v-v:tense-v:pn</ta>
            <ta e="T1685" id="Seg_2320" s="T1684">adv</ta>
            <ta e="T1686" id="Seg_2321" s="T1685">v-v:tense-v:pn</ta>
            <ta e="T1687" id="Seg_2322" s="T1686">v-v:mood.pn</ta>
            <ta e="T1688" id="Seg_2323" s="T1687">n-n:case</ta>
            <ta e="T1689" id="Seg_2324" s="T1688">pers</ta>
            <ta e="T1690" id="Seg_2325" s="T1689">n-n:case.poss</ta>
            <ta e="T1691" id="Seg_2326" s="T1690">v-v:tense-v:pn</ta>
            <ta e="T1692" id="Seg_2327" s="T1691">n-n:case</ta>
            <ta e="T1693" id="Seg_2328" s="T1692">v-v:tense-v:pn</ta>
            <ta e="T1694" id="Seg_2329" s="T1693">pers</ta>
            <ta e="T1695" id="Seg_2330" s="T1694">adv</ta>
            <ta e="T1696" id="Seg_2331" s="T1695">dempro-n:case</ta>
            <ta e="T1697" id="Seg_2332" s="T1696">v-v:tense-v:pn</ta>
            <ta e="T1698" id="Seg_2333" s="T1697">v-v:tense-v:pn</ta>
            <ta e="T1699" id="Seg_2334" s="T1698">v-v:tense-v:pn</ta>
            <ta e="T1700" id="Seg_2335" s="T1699">adv</ta>
            <ta e="T1701" id="Seg_2336" s="T1700">dempro-n:case</ta>
            <ta e="T1702" id="Seg_2337" s="T1701">v-v:tense-v:pn</ta>
            <ta e="T1703" id="Seg_2338" s="T1702">conj</ta>
            <ta e="T1705" id="Seg_2339" s="T1704">v-v:tense-v:pn</ta>
            <ta e="T1707" id="Seg_2340" s="T1706">dempro-n:num-n:case</ta>
            <ta e="T1709" id="Seg_2341" s="T1708">v-v:n.fin</ta>
            <ta e="T1710" id="Seg_2342" s="T1709">dempro-n:num</ta>
            <ta e="T1711" id="Seg_2343" s="T1710">v-v:tense-v:pn</ta>
            <ta e="T1712" id="Seg_2344" s="T1711">adv</ta>
            <ta e="T1713" id="Seg_2345" s="T1712">adv</ta>
            <ta e="T1714" id="Seg_2346" s="T1713">v-v:n.fin</ta>
            <ta e="T1715" id="Seg_2347" s="T1714">v-v:tense-v:pn</ta>
            <ta e="T1716" id="Seg_2348" s="T1715">v-v:mood.pn</ta>
            <ta e="T1717" id="Seg_2349" s="T1716">que-n:case.poss</ta>
            <ta e="T1718" id="Seg_2350" s="T1717">aux-v:mood.pn</ta>
            <ta e="T1720" id="Seg_2351" s="T1719">v-v:mood.pn</ta>
            <ta e="T1721" id="Seg_2352" s="T1720">adv</ta>
            <ta e="T1722" id="Seg_2353" s="T1721">v-v:tense-v:pn</ta>
            <ta e="T1723" id="Seg_2354" s="T1722">n-n:case</ta>
            <ta e="T1724" id="Seg_2355" s="T1723">dempro-n:case</ta>
            <ta e="T1725" id="Seg_2356" s="T1724">v-v&gt;v-v:pn</ta>
            <ta e="T1726" id="Seg_2357" s="T1725">v-v:mood.pn</ta>
            <ta e="T1727" id="Seg_2358" s="T1726">n-n:case</ta>
            <ta e="T1728" id="Seg_2359" s="T1727">pers</ta>
            <ta e="T1729" id="Seg_2360" s="T1728">n-n:case.poss</ta>
            <ta e="T1730" id="Seg_2361" s="T1729">v-v:tense-v:pn</ta>
            <ta e="T1731" id="Seg_2362" s="T1730">dempro-n:num</ta>
            <ta e="T1732" id="Seg_2363" s="T1731">ptcl</ta>
            <ta e="T1733" id="Seg_2364" s="T1732">dempro-n:case</ta>
            <ta e="T1735" id="Seg_2365" s="T1734">ptcl</ta>
            <ta e="T1736" id="Seg_2366" s="T1735">pers</ta>
            <ta e="T1737" id="Seg_2367" s="T1736">n-n:case.poss</ta>
            <ta e="T1738" id="Seg_2368" s="T1737">pers</ta>
            <ta e="T1739" id="Seg_2369" s="T1738">pers</ta>
            <ta e="T1740" id="Seg_2370" s="T1739">n-n:case.poss</ta>
            <ta e="T1741" id="Seg_2371" s="T1740">n-n:case.poss</ta>
            <ta e="T1742" id="Seg_2372" s="T1741">ptcl</ta>
            <ta e="T1743" id="Seg_2373" s="T1742">adj-n:case</ta>
            <ta e="T1744" id="Seg_2374" s="T1743">ptcl</ta>
            <ta e="T1745" id="Seg_2375" s="T1744">adj-n:case</ta>
            <ta e="T1746" id="Seg_2376" s="T1745">conj</ta>
            <ta e="T1747" id="Seg_2377" s="T1746">pers</ta>
            <ta e="T1748" id="Seg_2378" s="T1747">adj-n:case</ta>
            <ta e="T1749" id="Seg_2379" s="T1748">adv</ta>
            <ta e="T1750" id="Seg_2380" s="T1749">dempro-n:case</ta>
            <ta e="T1751" id="Seg_2381" s="T1750">v-v:n.fin</ta>
            <ta e="T1752" id="Seg_2382" s="T1751">v-v:tense-v:pn</ta>
            <ta e="T1753" id="Seg_2383" s="T1752">n-n:case.poss</ta>
            <ta e="T1754" id="Seg_2384" s="T1753">v-v:tense-v:pn</ta>
            <ta e="T1755" id="Seg_2385" s="T1754">dempro-n:num</ta>
            <ta e="T1756" id="Seg_2386" s="T1755">adv</ta>
            <ta e="T1757" id="Seg_2387" s="T1756">v-v&gt;v-v:pn</ta>
            <ta e="T1759" id="Seg_2388" s="T1758">v-v:mood.pn</ta>
            <ta e="T1760" id="Seg_2389" s="T1759">n-n:case</ta>
            <ta e="T1761" id="Seg_2390" s="T1760">dempro-n:case</ta>
            <ta e="T1762" id="Seg_2391" s="T1761">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1763" id="Seg_2392" s="T1762">dempro-n:case</ta>
            <ta e="T1764" id="Seg_2393" s="T1763">dempro-n:num-n:case</ta>
            <ta e="T1765" id="Seg_2394" s="T1764">v-v:n.fin</ta>
            <ta e="T1766" id="Seg_2395" s="T1765">v-v:tense-v:pn</ta>
            <ta e="T1767" id="Seg_2396" s="T1766">dempro-n:num</ta>
            <ta e="T1768" id="Seg_2397" s="T1767">v-v&gt;v-v:pn</ta>
            <ta e="T1769" id="Seg_2398" s="T1768">v-v:tense-v:pn</ta>
            <ta e="T1770" id="Seg_2399" s="T1769">adv</ta>
            <ta e="T1771" id="Seg_2400" s="T1770">n-n:case</ta>
            <ta e="T1772" id="Seg_2401" s="T1771">adv</ta>
            <ta e="T1773" id="Seg_2402" s="T1772">adj-n:case</ta>
            <ta e="T1774" id="Seg_2403" s="T1773">n-n:case.poss</ta>
            <ta e="T1775" id="Seg_2404" s="T1774">aux-v:mood.pn</ta>
            <ta e="T1776" id="Seg_2405" s="T1775">v-v:mood.pn</ta>
            <ta e="T1777" id="Seg_2406" s="T1776">adv</ta>
            <ta e="T1778" id="Seg_2407" s="T1777">v-v:n.fin</ta>
            <ta e="T1779" id="Seg_2408" s="T1778">v-v:tense-v:pn</ta>
            <ta e="T1780" id="Seg_2409" s="T1779">dempro-n:case</ta>
            <ta e="T1781" id="Seg_2410" s="T1780">n-n:case</ta>
            <ta e="T1782" id="Seg_2411" s="T1781">v-v:tense-v:pn</ta>
            <ta e="T1783" id="Seg_2412" s="T1782">adj</ta>
            <ta e="T1784" id="Seg_2413" s="T1783">n-n:case</ta>
            <ta e="T1785" id="Seg_2414" s="T1784">v-v&gt;v-v:pn</ta>
            <ta e="T1786" id="Seg_2415" s="T1785">v-v:mood.pn</ta>
            <ta e="T1787" id="Seg_2416" s="T1786">pers</ta>
            <ta e="T1788" id="Seg_2417" s="T1787">pers</ta>
            <ta e="T1789" id="Seg_2418" s="T1788">pers</ta>
            <ta e="T1790" id="Seg_2419" s="T1789">n-n:case</ta>
            <ta e="T1791" id="Seg_2420" s="T1790">v-v:tense-v:pn</ta>
            <ta e="T1792" id="Seg_2421" s="T1791">adv</ta>
            <ta e="T1793" id="Seg_2422" s="T1792">dempro-n:num</ta>
            <ta e="T1794" id="Seg_2423" s="T1793">v-v:tense-v:pn</ta>
            <ta e="T1795" id="Seg_2424" s="T1794">dempro-n:num</ta>
            <ta e="T1796" id="Seg_2425" s="T1795">ptcl</ta>
            <ta e="T1797" id="Seg_2426" s="T1796">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1798" id="Seg_2427" s="T1797">num-n:case</ta>
            <ta e="T1799" id="Seg_2428" s="T1798">n-n:case</ta>
            <ta e="T1801" id="Seg_2429" s="T1800">v-v&gt;v-v:tense</ta>
            <ta e="T1802" id="Seg_2430" s="T1801">conj</ta>
            <ta e="T1803" id="Seg_2431" s="T1802">adv</ta>
            <ta e="T1804" id="Seg_2432" s="T1803">v-v&gt;v-v:tense</ta>
            <ta e="T1805" id="Seg_2433" s="T1804">n-n:case.poss</ta>
            <ta e="T1806" id="Seg_2434" s="T1805">v-v:tense-v:pn</ta>
            <ta e="T1807" id="Seg_2435" s="T1806">v-v:tense-v:pn</ta>
            <ta e="T1808" id="Seg_2436" s="T1807">v-v:tense-v:pn</ta>
            <ta e="T1809" id="Seg_2437" s="T1808">v</ta>
            <ta e="T1810" id="Seg_2438" s="T1809">adv</ta>
            <ta e="T1811" id="Seg_2439" s="T1810">v-v:tense-v:pn</ta>
            <ta e="T1812" id="Seg_2440" s="T1811">n-n:num-n:case</ta>
            <ta e="T1813" id="Seg_2441" s="T1812">v</ta>
            <ta e="T1814" id="Seg_2442" s="T1813">num-n:case</ta>
            <ta e="T1815" id="Seg_2443" s="T1814">v-v:tense-v:pn</ta>
            <ta e="T1816" id="Seg_2444" s="T1815">dempro-n:case</ta>
            <ta e="T1817" id="Seg_2445" s="T1816">v-v&gt;v-v:pn</ta>
            <ta e="T1818" id="Seg_2446" s="T1817">v-v:tense-v:pn</ta>
            <ta e="T1819" id="Seg_2447" s="T1818">n-n:case</ta>
            <ta e="T1820" id="Seg_2448" s="T1819">dempro-n:case</ta>
            <ta e="T1821" id="Seg_2449" s="T1820">quant</ta>
            <ta e="T1822" id="Seg_2450" s="T1821">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1823" id="Seg_2451" s="T1822">adv</ta>
            <ta e="T1824" id="Seg_2452" s="T1823">pers</ta>
            <ta e="T1825" id="Seg_2453" s="T1824">adv</ta>
            <ta e="T1826" id="Seg_2454" s="T1825">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1827" id="Seg_2455" s="T1826">adv</ta>
            <ta e="T1828" id="Seg_2456" s="T1827">v-v:tense-v:pn</ta>
            <ta e="T1829" id="Seg_2457" s="T1828">adv</ta>
            <ta e="T1830" id="Seg_2458" s="T1829">dempro-n:case</ta>
            <ta e="T1831" id="Seg_2459" s="T1830">ptcl</ta>
            <ta e="T1832" id="Seg_2460" s="T1831">v-v:n.fin</ta>
            <ta e="T1833" id="Seg_2461" s="T1832">v-v&gt;v-v:pn</ta>
            <ta e="T1834" id="Seg_2462" s="T1833">que</ta>
            <ta e="T1835" id="Seg_2463" s="T1834">pers</ta>
            <ta e="T1837" id="Seg_2464" s="T1836">n-n:num-n:case.poss</ta>
            <ta e="T1838" id="Seg_2465" s="T1837">dempro-n:case</ta>
            <ta e="T1839" id="Seg_2466" s="T1838">n-n:case</ta>
            <ta e="T1840" id="Seg_2467" s="T1839">v-v:tense-v:pn</ta>
            <ta e="T1841" id="Seg_2468" s="T1840">que-n:case</ta>
            <ta e="T1842" id="Seg_2469" s="T1841">v-v&gt;v-v:pn</ta>
            <ta e="T1843" id="Seg_2470" s="T1842">dempro-n:case</ta>
            <ta e="T1844" id="Seg_2471" s="T1843">ptcl</ta>
            <ta e="T1845" id="Seg_2472" s="T1844">pers</ta>
            <ta e="T1846" id="Seg_2473" s="T1845">v-v:tense-v:pn</ta>
            <ta e="T1847" id="Seg_2474" s="T1846">v-v:mood-v:pn</ta>
            <ta e="T1848" id="Seg_2475" s="T1847">pers-n:case</ta>
            <ta e="T1849" id="Seg_2476" s="T1848">ptcl</ta>
            <ta e="T1850" id="Seg_2477" s="T1849">aux-v:pn</ta>
            <ta e="T1851" id="Seg_2478" s="T1850">v-v:ins-v:n.fin</ta>
            <ta e="T1852" id="Seg_2479" s="T1851">v-v:mood-v:pn</ta>
            <ta e="T1853" id="Seg_2480" s="T1852">v-v:tense-v:pn</ta>
            <ta e="T1854" id="Seg_2481" s="T1853">conj</ta>
            <ta e="T1855" id="Seg_2482" s="T1854">adv</ta>
            <ta e="T1856" id="Seg_2483" s="T1855">v-v:tense-v:pn</ta>
            <ta e="T1857" id="Seg_2484" s="T1856">adj</ta>
            <ta e="T1858" id="Seg_2485" s="T1857">n-n:case</ta>
            <ta e="T1859" id="Seg_2486" s="T1858">adv</ta>
            <ta e="T1860" id="Seg_2487" s="T1859">ptcl</ta>
            <ta e="T1861" id="Seg_2488" s="T1860">n-n:case</ta>
            <ta e="T1862" id="Seg_2489" s="T1861">v-v&gt;v-v:pn</ta>
            <ta e="T1863" id="Seg_2490" s="T1862">conj</ta>
            <ta e="T1864" id="Seg_2491" s="T1863">n-n:case</ta>
            <ta e="T1865" id="Seg_2492" s="T1864">v-v&gt;v-v:pn</ta>
            <ta e="T1866" id="Seg_2493" s="T1865">ptcl</ta>
            <ta e="T1867" id="Seg_2494" s="T1866">v-v:mood.pn</ta>
            <ta e="T1868" id="Seg_2495" s="T1867">dempro-n:case</ta>
            <ta e="T1869" id="Seg_2496" s="T1868">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1870" id="Seg_2497" s="T1869">conj</ta>
            <ta e="T1871" id="Seg_2498" s="T1870">adv</ta>
            <ta e="T1872" id="Seg_2499" s="T1871">n-n:case</ta>
            <ta e="T1873" id="Seg_2500" s="T1872">v-v:tense-v:pn</ta>
            <ta e="T1874" id="Seg_2501" s="T1873">adv</ta>
            <ta e="T1875" id="Seg_2502" s="T1874">n-n:case.poss</ta>
            <ta e="T1876" id="Seg_2503" s="T1875">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1877" id="Seg_2504" s="T1876">conj</ta>
            <ta e="T1878" id="Seg_2505" s="T1877">n-n:num</ta>
            <ta e="T1879" id="Seg_2506" s="T1878">n-n:case.poss</ta>
            <ta e="T1880" id="Seg_2507" s="T1879">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1881" id="Seg_2508" s="T1880">conj</ta>
            <ta e="T1882" id="Seg_2509" s="T1881">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T1883" id="Seg_2510" s="T1882">n-n:case.poss</ta>
            <ta e="T1884" id="Seg_2511" s="T1883">adv</ta>
            <ta e="T1885" id="Seg_2512" s="T1884">dempro-n:num</ta>
            <ta e="T1886" id="Seg_2513" s="T1885">v-v:tense-v:pn</ta>
            <ta e="T1887" id="Seg_2514" s="T1886">v-v:tense-v:pn</ta>
            <ta e="T1888" id="Seg_2515" s="T1887">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1671" id="Seg_2516" s="T1670">v</ta>
            <ta e="T1672" id="Seg_2517" s="T1671">n</ta>
            <ta e="T1673" id="Seg_2518" s="T1672">dempro</ta>
            <ta e="T1674" id="Seg_2519" s="T1673">n</ta>
            <ta e="T1675" id="Seg_2520" s="T1674">v</ta>
            <ta e="T1676" id="Seg_2521" s="T1675">n</ta>
            <ta e="T1677" id="Seg_2522" s="T1676">v</ta>
            <ta e="T1678" id="Seg_2523" s="T1677">adv</ta>
            <ta e="T1679" id="Seg_2524" s="T1678">v</ta>
            <ta e="T1680" id="Seg_2525" s="T1679">v</ta>
            <ta e="T1681" id="Seg_2526" s="T1680">que</ta>
            <ta e="T1682" id="Seg_2527" s="T1681">aux</ta>
            <ta e="T1683" id="Seg_2528" s="T1682">v</ta>
            <ta e="T1684" id="Seg_2529" s="T1683">v</ta>
            <ta e="T1685" id="Seg_2530" s="T1684">adv</ta>
            <ta e="T1686" id="Seg_2531" s="T1685">v</ta>
            <ta e="T1687" id="Seg_2532" s="T1686">v</ta>
            <ta e="T1688" id="Seg_2533" s="T1687">n</ta>
            <ta e="T1689" id="Seg_2534" s="T1688">pers</ta>
            <ta e="T1690" id="Seg_2535" s="T1689">n</ta>
            <ta e="T1691" id="Seg_2536" s="T1690">v</ta>
            <ta e="T1692" id="Seg_2537" s="T1691">n</ta>
            <ta e="T1693" id="Seg_2538" s="T1692">v</ta>
            <ta e="T1694" id="Seg_2539" s="T1693">pers</ta>
            <ta e="T1695" id="Seg_2540" s="T1694">adv</ta>
            <ta e="T1696" id="Seg_2541" s="T1695">dempro</ta>
            <ta e="T1697" id="Seg_2542" s="T1696">v</ta>
            <ta e="T1698" id="Seg_2543" s="T1697">v</ta>
            <ta e="T1699" id="Seg_2544" s="T1698">v</ta>
            <ta e="T1700" id="Seg_2545" s="T1699">adv</ta>
            <ta e="T1701" id="Seg_2546" s="T1700">dempro</ta>
            <ta e="T1702" id="Seg_2547" s="T1701">v</ta>
            <ta e="T1703" id="Seg_2548" s="T1702">conj</ta>
            <ta e="T1705" id="Seg_2549" s="T1704">v</ta>
            <ta e="T1707" id="Seg_2550" s="T1706">dempro</ta>
            <ta e="T1709" id="Seg_2551" s="T1708">v</ta>
            <ta e="T1710" id="Seg_2552" s="T1709">dempro</ta>
            <ta e="T1711" id="Seg_2553" s="T1710">v</ta>
            <ta e="T1712" id="Seg_2554" s="T1711">adv</ta>
            <ta e="T1713" id="Seg_2555" s="T1712">adv</ta>
            <ta e="T1714" id="Seg_2556" s="T1713">v</ta>
            <ta e="T1715" id="Seg_2557" s="T1714">v</ta>
            <ta e="T1716" id="Seg_2558" s="T1715">v</ta>
            <ta e="T1717" id="Seg_2559" s="T1716">que</ta>
            <ta e="T1718" id="Seg_2560" s="T1717">aux</ta>
            <ta e="T1720" id="Seg_2561" s="T1719">v</ta>
            <ta e="T1721" id="Seg_2562" s="T1720">adv</ta>
            <ta e="T1722" id="Seg_2563" s="T1721">v</ta>
            <ta e="T1723" id="Seg_2564" s="T1722">n</ta>
            <ta e="T1724" id="Seg_2565" s="T1723">dempro</ta>
            <ta e="T1725" id="Seg_2566" s="T1724">v</ta>
            <ta e="T1726" id="Seg_2567" s="T1725">v</ta>
            <ta e="T1727" id="Seg_2568" s="T1726">n</ta>
            <ta e="T1728" id="Seg_2569" s="T1727">pers</ta>
            <ta e="T1729" id="Seg_2570" s="T1728">n</ta>
            <ta e="T1730" id="Seg_2571" s="T1729">v</ta>
            <ta e="T1731" id="Seg_2572" s="T1730">dempro</ta>
            <ta e="T1732" id="Seg_2573" s="T1731">ptcl</ta>
            <ta e="T1733" id="Seg_2574" s="T1732">dempro</ta>
            <ta e="T1735" id="Seg_2575" s="T1734">ptcl</ta>
            <ta e="T1736" id="Seg_2576" s="T1735">pers</ta>
            <ta e="T1737" id="Seg_2577" s="T1736">n</ta>
            <ta e="T1738" id="Seg_2578" s="T1737">pers</ta>
            <ta e="T1739" id="Seg_2579" s="T1738">pers</ta>
            <ta e="T1740" id="Seg_2580" s="T1739">n</ta>
            <ta e="T1741" id="Seg_2581" s="T1740">n</ta>
            <ta e="T1742" id="Seg_2582" s="T1741">ptcl</ta>
            <ta e="T1743" id="Seg_2583" s="T1742">adj</ta>
            <ta e="T1744" id="Seg_2584" s="T1743">ptcl</ta>
            <ta e="T1745" id="Seg_2585" s="T1744">adj</ta>
            <ta e="T1746" id="Seg_2586" s="T1745">conj</ta>
            <ta e="T1747" id="Seg_2587" s="T1746">pers</ta>
            <ta e="T1748" id="Seg_2588" s="T1747">adj</ta>
            <ta e="T1749" id="Seg_2589" s="T1748">adv</ta>
            <ta e="T1750" id="Seg_2590" s="T1749">dempro</ta>
            <ta e="T1751" id="Seg_2591" s="T1750">v</ta>
            <ta e="T1752" id="Seg_2592" s="T1751">v</ta>
            <ta e="T1753" id="Seg_2593" s="T1752">n</ta>
            <ta e="T1754" id="Seg_2594" s="T1753">v</ta>
            <ta e="T1755" id="Seg_2595" s="T1754">dempro</ta>
            <ta e="T1756" id="Seg_2596" s="T1755">adv</ta>
            <ta e="T1757" id="Seg_2597" s="T1756">v</ta>
            <ta e="T1759" id="Seg_2598" s="T1758">v</ta>
            <ta e="T1760" id="Seg_2599" s="T1759">n</ta>
            <ta e="T1761" id="Seg_2600" s="T1760">dempro</ta>
            <ta e="T1762" id="Seg_2601" s="T1761">v</ta>
            <ta e="T1763" id="Seg_2602" s="T1762">dempro</ta>
            <ta e="T1764" id="Seg_2603" s="T1763">dempro</ta>
            <ta e="T1765" id="Seg_2604" s="T1764">v</ta>
            <ta e="T1766" id="Seg_2605" s="T1765">v</ta>
            <ta e="T1767" id="Seg_2606" s="T1766">dempro</ta>
            <ta e="T1768" id="Seg_2607" s="T1767">v</ta>
            <ta e="T1769" id="Seg_2608" s="T1768">v</ta>
            <ta e="T1770" id="Seg_2609" s="T1769">adv</ta>
            <ta e="T1771" id="Seg_2610" s="T1770">n</ta>
            <ta e="T1772" id="Seg_2611" s="T1771">adv</ta>
            <ta e="T1773" id="Seg_2612" s="T1772">adj</ta>
            <ta e="T1774" id="Seg_2613" s="T1773">n</ta>
            <ta e="T1775" id="Seg_2614" s="T1774">aux</ta>
            <ta e="T1776" id="Seg_2615" s="T1775">v</ta>
            <ta e="T1777" id="Seg_2616" s="T1776">adv</ta>
            <ta e="T1778" id="Seg_2617" s="T1777">v</ta>
            <ta e="T1779" id="Seg_2618" s="T1778">v</ta>
            <ta e="T1780" id="Seg_2619" s="T1779">dempro</ta>
            <ta e="T1781" id="Seg_2620" s="T1780">n</ta>
            <ta e="T1782" id="Seg_2621" s="T1781">v</ta>
            <ta e="T1783" id="Seg_2622" s="T1782">adj</ta>
            <ta e="T1784" id="Seg_2623" s="T1783">n</ta>
            <ta e="T1785" id="Seg_2624" s="T1784">v</ta>
            <ta e="T1786" id="Seg_2625" s="T1785">v</ta>
            <ta e="T1787" id="Seg_2626" s="T1786">pers</ta>
            <ta e="T1788" id="Seg_2627" s="T1787">pers</ta>
            <ta e="T1789" id="Seg_2628" s="T1788">pers</ta>
            <ta e="T1790" id="Seg_2629" s="T1789">n</ta>
            <ta e="T1791" id="Seg_2630" s="T1790">v</ta>
            <ta e="T1792" id="Seg_2631" s="T1791">adv</ta>
            <ta e="T1793" id="Seg_2632" s="T1792">dempro</ta>
            <ta e="T1794" id="Seg_2633" s="T1793">v</ta>
            <ta e="T1795" id="Seg_2634" s="T1794">dempro</ta>
            <ta e="T1796" id="Seg_2635" s="T1795">ptcl</ta>
            <ta e="T1797" id="Seg_2636" s="T1796">v</ta>
            <ta e="T1798" id="Seg_2637" s="T1797">num</ta>
            <ta e="T1799" id="Seg_2638" s="T1798">n</ta>
            <ta e="T1801" id="Seg_2639" s="T1800">v</ta>
            <ta e="T1802" id="Seg_2640" s="T1801">conj</ta>
            <ta e="T1803" id="Seg_2641" s="T1802">adv</ta>
            <ta e="T1804" id="Seg_2642" s="T1803">v</ta>
            <ta e="T1805" id="Seg_2643" s="T1804">n</ta>
            <ta e="T1806" id="Seg_2644" s="T1805">v</ta>
            <ta e="T1807" id="Seg_2645" s="T1806">v</ta>
            <ta e="T1808" id="Seg_2646" s="T1807">v</ta>
            <ta e="T1809" id="Seg_2647" s="T1808">v</ta>
            <ta e="T1810" id="Seg_2648" s="T1809">adv</ta>
            <ta e="T1811" id="Seg_2649" s="T1810">v</ta>
            <ta e="T1812" id="Seg_2650" s="T1811">n</ta>
            <ta e="T1813" id="Seg_2651" s="T1812">v</ta>
            <ta e="T1814" id="Seg_2652" s="T1813">num</ta>
            <ta e="T1815" id="Seg_2653" s="T1814">v</ta>
            <ta e="T1816" id="Seg_2654" s="T1815">dempro</ta>
            <ta e="T1817" id="Seg_2655" s="T1816">v</ta>
            <ta e="T1818" id="Seg_2656" s="T1817">v</ta>
            <ta e="T1819" id="Seg_2657" s="T1818">n</ta>
            <ta e="T1820" id="Seg_2658" s="T1819">dempro</ta>
            <ta e="T1821" id="Seg_2659" s="T1820">quant</ta>
            <ta e="T1822" id="Seg_2660" s="T1821">v</ta>
            <ta e="T1823" id="Seg_2661" s="T1822">adv</ta>
            <ta e="T1824" id="Seg_2662" s="T1823">pers</ta>
            <ta e="T1825" id="Seg_2663" s="T1824">adv</ta>
            <ta e="T1826" id="Seg_2664" s="T1825">v</ta>
            <ta e="T1827" id="Seg_2665" s="T1826">adv</ta>
            <ta e="T1828" id="Seg_2666" s="T1827">v</ta>
            <ta e="T1829" id="Seg_2667" s="T1828">adv</ta>
            <ta e="T1830" id="Seg_2668" s="T1829">dempro</ta>
            <ta e="T1831" id="Seg_2669" s="T1830">ptcl</ta>
            <ta e="T1832" id="Seg_2670" s="T1831">v</ta>
            <ta e="T1833" id="Seg_2671" s="T1832">v</ta>
            <ta e="T1834" id="Seg_2672" s="T1833">que</ta>
            <ta e="T1835" id="Seg_2673" s="T1834">pers</ta>
            <ta e="T1837" id="Seg_2674" s="T1836">n</ta>
            <ta e="T1838" id="Seg_2675" s="T1837">dempro</ta>
            <ta e="T1839" id="Seg_2676" s="T1838">n</ta>
            <ta e="T1840" id="Seg_2677" s="T1839">v</ta>
            <ta e="T1841" id="Seg_2678" s="T1840">que</ta>
            <ta e="T1842" id="Seg_2679" s="T1841">v</ta>
            <ta e="T1843" id="Seg_2680" s="T1842">dempro</ta>
            <ta e="T1844" id="Seg_2681" s="T1843">ptcl</ta>
            <ta e="T1845" id="Seg_2682" s="T1844">pers</ta>
            <ta e="T1846" id="Seg_2683" s="T1845">v</ta>
            <ta e="T1847" id="Seg_2684" s="T1846">v</ta>
            <ta e="T1848" id="Seg_2685" s="T1847">pers</ta>
            <ta e="T1849" id="Seg_2686" s="T1848">ptcl</ta>
            <ta e="T1850" id="Seg_2687" s="T1849">aux</ta>
            <ta e="T1851" id="Seg_2688" s="T1850">v</ta>
            <ta e="T1852" id="Seg_2689" s="T1851">v</ta>
            <ta e="T1853" id="Seg_2690" s="T1852">v</ta>
            <ta e="T1854" id="Seg_2691" s="T1853">conj</ta>
            <ta e="T1855" id="Seg_2692" s="T1854">adv</ta>
            <ta e="T1856" id="Seg_2693" s="T1855">v</ta>
            <ta e="T1857" id="Seg_2694" s="T1856">adj</ta>
            <ta e="T1858" id="Seg_2695" s="T1857">n</ta>
            <ta e="T1859" id="Seg_2696" s="T1858">adv</ta>
            <ta e="T1860" id="Seg_2697" s="T1859">ptcl</ta>
            <ta e="T1861" id="Seg_2698" s="T1860">n</ta>
            <ta e="T1862" id="Seg_2699" s="T1861">v</ta>
            <ta e="T1863" id="Seg_2700" s="T1862">conj</ta>
            <ta e="T1864" id="Seg_2701" s="T1863">n</ta>
            <ta e="T1865" id="Seg_2702" s="T1864">v</ta>
            <ta e="T1866" id="Seg_2703" s="T1865">ptcl</ta>
            <ta e="T1867" id="Seg_2704" s="T1866">v</ta>
            <ta e="T1868" id="Seg_2705" s="T1867">dempro</ta>
            <ta e="T1869" id="Seg_2706" s="T1868">v</ta>
            <ta e="T1870" id="Seg_2707" s="T1869">conj</ta>
            <ta e="T1871" id="Seg_2708" s="T1870">adv</ta>
            <ta e="T1872" id="Seg_2709" s="T1871">n</ta>
            <ta e="T1873" id="Seg_2710" s="T1872">v</ta>
            <ta e="T1874" id="Seg_2711" s="T1873">adv</ta>
            <ta e="T1875" id="Seg_2712" s="T1874">n</ta>
            <ta e="T1876" id="Seg_2713" s="T1875">v</ta>
            <ta e="T1877" id="Seg_2714" s="T1876">conj</ta>
            <ta e="T1878" id="Seg_2715" s="T1877">n</ta>
            <ta e="T1879" id="Seg_2716" s="T1878">n</ta>
            <ta e="T1880" id="Seg_2717" s="T1879">v</ta>
            <ta e="T1881" id="Seg_2718" s="T1880">conj</ta>
            <ta e="T1882" id="Seg_2719" s="T1881">v</ta>
            <ta e="T1883" id="Seg_2720" s="T1882">n</ta>
            <ta e="T1884" id="Seg_2721" s="T1883">adv</ta>
            <ta e="T1885" id="Seg_2722" s="T1884">dempro</ta>
            <ta e="T1886" id="Seg_2723" s="T1885">v</ta>
            <ta e="T1887" id="Seg_2724" s="T1886">v</ta>
            <ta e="T1888" id="Seg_2725" s="T1887">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1672" id="Seg_2726" s="T1671">np.h:E</ta>
            <ta e="T1673" id="Seg_2727" s="T1672">pro.h:Poss</ta>
            <ta e="T1674" id="Seg_2728" s="T1673">np:Th</ta>
            <ta e="T1676" id="Seg_2729" s="T1675">np.h:Th</ta>
            <ta e="T1677" id="Seg_2730" s="T1676">0.3.h:A</ta>
            <ta e="T1678" id="Seg_2731" s="T1677">adv:Time</ta>
            <ta e="T1679" id="Seg_2732" s="T1678">0.3.h:A</ta>
            <ta e="T1680" id="Seg_2733" s="T1679">0.2.h:A</ta>
            <ta e="T1681" id="Seg_2734" s="T1680">pro.h:Th</ta>
            <ta e="T1682" id="Seg_2735" s="T1681">0.2.h:A</ta>
            <ta e="T1684" id="Seg_2736" s="T1683">0.3.h:A</ta>
            <ta e="T1685" id="Seg_2737" s="T1684">adv:Time</ta>
            <ta e="T1686" id="Seg_2738" s="T1685">0.3.h:A</ta>
            <ta e="T1687" id="Seg_2739" s="T1686">0.2.h:A</ta>
            <ta e="T1688" id="Seg_2740" s="T1687">np:Th</ta>
            <ta e="T1689" id="Seg_2741" s="T1688">pro.h:Poss</ta>
            <ta e="T1690" id="Seg_2742" s="T1689">np.h:A</ta>
            <ta e="T1692" id="Seg_2743" s="T1691">np:Th</ta>
            <ta e="T1693" id="Seg_2744" s="T1692">0.3.h:A</ta>
            <ta e="T1694" id="Seg_2745" s="T1693">pro.h:R</ta>
            <ta e="T1695" id="Seg_2746" s="T1694">adv:Time</ta>
            <ta e="T1696" id="Seg_2747" s="T1695">pro.h:A</ta>
            <ta e="T1698" id="Seg_2748" s="T1697">0.3.h:A</ta>
            <ta e="T1699" id="Seg_2749" s="T1698">0.3.h:A</ta>
            <ta e="T1700" id="Seg_2750" s="T1699">adv:Time</ta>
            <ta e="T1701" id="Seg_2751" s="T1700">0.2.h:A</ta>
            <ta e="T1705" id="Seg_2752" s="T1704">0.3.h:A</ta>
            <ta e="T1707" id="Seg_2753" s="T1706">pro.h:R</ta>
            <ta e="T1710" id="Seg_2754" s="T1709">pro.h:A</ta>
            <ta e="T1712" id="Seg_2755" s="T1711">adv:Time</ta>
            <ta e="T1715" id="Seg_2756" s="T1714">0.3.h:A</ta>
            <ta e="T1716" id="Seg_2757" s="T1715">0.2.h:E</ta>
            <ta e="T1717" id="Seg_2758" s="T1716">pro.h:Th</ta>
            <ta e="T1718" id="Seg_2759" s="T1717">0.2.h:A</ta>
            <ta e="T1721" id="Seg_2760" s="T1720">adv:Time</ta>
            <ta e="T1723" id="Seg_2761" s="T1722">np.h:A</ta>
            <ta e="T1724" id="Seg_2762" s="T1723">pro.h:A</ta>
            <ta e="T1726" id="Seg_2763" s="T1725">0.2.h:A</ta>
            <ta e="T1727" id="Seg_2764" s="T1726">np:Th</ta>
            <ta e="T1728" id="Seg_2765" s="T1727">pro.h:Poss</ta>
            <ta e="T1729" id="Seg_2766" s="T1728">np.h:A</ta>
            <ta e="T1731" id="Seg_2767" s="T1730">pro.h:A</ta>
            <ta e="T1733" id="Seg_2768" s="T1732">pro.h:Th</ta>
            <ta e="T1736" id="Seg_2769" s="T1735">pro.h:Poss</ta>
            <ta e="T1737" id="Seg_2770" s="T1736">np.h:Th</ta>
            <ta e="T1739" id="Seg_2771" s="T1738">pro.h:Poss</ta>
            <ta e="T1740" id="Seg_2772" s="T1739">np.h:Poss</ta>
            <ta e="T1741" id="Seg_2773" s="T1740">np:Th</ta>
            <ta e="T1747" id="Seg_2774" s="T1746">pro.h:Poss np:Th</ta>
            <ta e="T1749" id="Seg_2775" s="T1748">adv:Time</ta>
            <ta e="T1750" id="Seg_2776" s="T1749">pro.h:A</ta>
            <ta e="T1753" id="Seg_2777" s="T1752">np.h:A</ta>
            <ta e="T1755" id="Seg_2778" s="T1754">pro.h:Poss</ta>
            <ta e="T1757" id="Seg_2779" s="T1756">0.3.h:A</ta>
            <ta e="T1759" id="Seg_2780" s="T1758">0.2.h:A</ta>
            <ta e="T1760" id="Seg_2781" s="T1759">np:Th</ta>
            <ta e="T1761" id="Seg_2782" s="T1760">pro.h:A</ta>
            <ta e="T1763" id="Seg_2783" s="T1762">pro.h:A</ta>
            <ta e="T1764" id="Seg_2784" s="T1763">pro.h:R</ta>
            <ta e="T1767" id="Seg_2785" s="T1766">pro.h:A</ta>
            <ta e="T1770" id="Seg_2786" s="T1769">adv:L</ta>
            <ta e="T1771" id="Seg_2787" s="T1770">np.h:A</ta>
            <ta e="T1774" id="Seg_2788" s="T1773">np:Th</ta>
            <ta e="T1775" id="Seg_2789" s="T1774">0.2.h:A</ta>
            <ta e="T1777" id="Seg_2790" s="T1776">adv:Time</ta>
            <ta e="T1779" id="Seg_2791" s="T1778">0.3.h:A</ta>
            <ta e="T1781" id="Seg_2792" s="T1780">np.h:A</ta>
            <ta e="T1784" id="Seg_2793" s="T1783">np:Ins</ta>
            <ta e="T1785" id="Seg_2794" s="T1784">0.3.h:A</ta>
            <ta e="T1786" id="Seg_2795" s="T1785">0.2.h:A</ta>
            <ta e="T1787" id="Seg_2796" s="T1786">pro.h:Th</ta>
            <ta e="T1789" id="Seg_2797" s="T1788">pro.h:R</ta>
            <ta e="T1790" id="Seg_2798" s="T1789">np:Th</ta>
            <ta e="T1791" id="Seg_2799" s="T1790">0.1.h:A</ta>
            <ta e="T1792" id="Seg_2800" s="T1791">adv:Time</ta>
            <ta e="T1793" id="Seg_2801" s="T1792">pro.h:A</ta>
            <ta e="T1795" id="Seg_2802" s="T1794">pro.h:A</ta>
            <ta e="T1798" id="Seg_2803" s="T1797">np.h:A</ta>
            <ta e="T1799" id="Seg_2804" s="T1798">np:G</ta>
            <ta e="T1803" id="Seg_2805" s="T1802">adv:L</ta>
            <ta e="T1805" id="Seg_2806" s="T1804">np.h:A 0.3.h:Poss</ta>
            <ta e="T1807" id="Seg_2807" s="T1806">0.3.h:A</ta>
            <ta e="T1808" id="Seg_2808" s="T1807">0.3.h:A</ta>
            <ta e="T1810" id="Seg_2809" s="T1809">adv:Time</ta>
            <ta e="T1811" id="Seg_2810" s="T1810">0.3.h:A</ta>
            <ta e="T1812" id="Seg_2811" s="T1811">np.h:Th</ta>
            <ta e="T1814" id="Seg_2812" s="T1813">np.h:Th</ta>
            <ta e="T1815" id="Seg_2813" s="T1814">0.3.h:A</ta>
            <ta e="T1816" id="Seg_2814" s="T1815">pro.h:A</ta>
            <ta e="T1819" id="Seg_2815" s="T1818">np.h:A</ta>
            <ta e="T1820" id="Seg_2816" s="T1819">pro.h:A</ta>
            <ta e="T1821" id="Seg_2817" s="T1820">np.h:P</ta>
            <ta e="T1824" id="Seg_2818" s="T1823">pro.h:E</ta>
            <ta e="T1827" id="Seg_2819" s="T1826">adv:Time</ta>
            <ta e="T1828" id="Seg_2820" s="T1827">0.3.h:A</ta>
            <ta e="T1829" id="Seg_2821" s="T1828">adv:Pth</ta>
            <ta e="T1830" id="Seg_2822" s="T1829">pro.h:E</ta>
            <ta e="T1835" id="Seg_2823" s="T1834">pro.h:Poss</ta>
            <ta e="T1837" id="Seg_2824" s="T1836">np.h:Th</ta>
            <ta e="T1839" id="Seg_2825" s="T1838">np.h:A</ta>
            <ta e="T1842" id="Seg_2826" s="T1841">0.2.h:E</ta>
            <ta e="T1843" id="Seg_2827" s="T1842">pro.h:P</ta>
            <ta e="T1845" id="Seg_2828" s="T1844">pro.h:A</ta>
            <ta e="T1847" id="Seg_2829" s="T1846">0.1.h:A</ta>
            <ta e="T1848" id="Seg_2830" s="T1847">pro.h:Com</ta>
            <ta e="T1850" id="Seg_2831" s="T1849">0.1.h:A</ta>
            <ta e="T1852" id="Seg_2832" s="T1851">0.1.h:A</ta>
            <ta e="T1853" id="Seg_2833" s="T1852">0.3.h:A</ta>
            <ta e="T1855" id="Seg_2834" s="T1854">adv:L</ta>
            <ta e="T1856" id="Seg_2835" s="T1855">0.3.h:E</ta>
            <ta e="T1858" id="Seg_2836" s="T1857">np:Th</ta>
            <ta e="T1859" id="Seg_2837" s="T1858">adv:L</ta>
            <ta e="T1861" id="Seg_2838" s="T1860">np:Th</ta>
            <ta e="T1864" id="Seg_2839" s="T1863">np:Th</ta>
            <ta e="T1867" id="Seg_2840" s="T1866">0.2.h:A</ta>
            <ta e="T1868" id="Seg_2841" s="T1867">pro.h:A</ta>
            <ta e="T1871" id="Seg_2842" s="T1870">adv:L</ta>
            <ta e="T1872" id="Seg_2843" s="T1871">np:Th</ta>
            <ta e="T1874" id="Seg_2844" s="T1873">adv:L</ta>
            <ta e="T1875" id="Seg_2845" s="T1874">np:P 0.3.h:Poss</ta>
            <ta e="T1878" id="Seg_2846" s="T1877">np.h:A</ta>
            <ta e="T1879" id="Seg_2847" s="T1878">np:So</ta>
            <ta e="T1882" id="Seg_2848" s="T1881">0.3.h:A</ta>
            <ta e="T1883" id="Seg_2849" s="T1882">np:G</ta>
            <ta e="T1884" id="Seg_2850" s="T1883">adv:Time</ta>
            <ta e="T1885" id="Seg_2851" s="T1884">pro.h:A</ta>
            <ta e="T1887" id="Seg_2852" s="T1886">0.3.h:E</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1671" id="Seg_2853" s="T1670">v:pred</ta>
            <ta e="T1672" id="Seg_2854" s="T1671">np.h:S</ta>
            <ta e="T1674" id="Seg_2855" s="T1673">np:S</ta>
            <ta e="T1675" id="Seg_2856" s="T1674">v:pred</ta>
            <ta e="T1676" id="Seg_2857" s="T1675">np.h:O</ta>
            <ta e="T1677" id="Seg_2858" s="T1676">v:pred 0.3.h:S</ta>
            <ta e="T1679" id="Seg_2859" s="T1678">v:pred 0.3.h:S</ta>
            <ta e="T1680" id="Seg_2860" s="T1679">v:pred 0.2.h:S</ta>
            <ta e="T1681" id="Seg_2861" s="T1680">pro.h:O</ta>
            <ta e="T1682" id="Seg_2862" s="T1681">v:pred 0.2.h:S</ta>
            <ta e="T1684" id="Seg_2863" s="T1683">v:pred 0.3.h:S</ta>
            <ta e="T1686" id="Seg_2864" s="T1685">v:pred 0.3.h:S</ta>
            <ta e="T1687" id="Seg_2865" s="T1686">v:pred 0.2.h:S</ta>
            <ta e="T1688" id="Seg_2866" s="T1687">np:O</ta>
            <ta e="T1690" id="Seg_2867" s="T1689">np.h:S</ta>
            <ta e="T1691" id="Seg_2868" s="T1690">v:pred</ta>
            <ta e="T1692" id="Seg_2869" s="T1691">np:O</ta>
            <ta e="T1693" id="Seg_2870" s="T1692">v:pred 0.3.h:S</ta>
            <ta e="T1696" id="Seg_2871" s="T1695">pro.h:S</ta>
            <ta e="T1697" id="Seg_2872" s="T1696">v:pred</ta>
            <ta e="T1698" id="Seg_2873" s="T1697">v:pred 0.3.h:S</ta>
            <ta e="T1699" id="Seg_2874" s="T1698">v:pred 0.3.h:S</ta>
            <ta e="T1701" id="Seg_2875" s="T1700">pro.h:S</ta>
            <ta e="T1702" id="Seg_2876" s="T1701">v:pred</ta>
            <ta e="T1705" id="Seg_2877" s="T1704">v:pred 0.3.h:S</ta>
            <ta e="T1709" id="Seg_2878" s="T1708">s:purp</ta>
            <ta e="T1710" id="Seg_2879" s="T1709">pro.h:S</ta>
            <ta e="T1711" id="Seg_2880" s="T1710">v:pred</ta>
            <ta e="T1714" id="Seg_2881" s="T1713">conv:pred</ta>
            <ta e="T1715" id="Seg_2882" s="T1714">v:pred 0.3.h:S</ta>
            <ta e="T1716" id="Seg_2883" s="T1715">v:pred 0.2.h:S</ta>
            <ta e="T1717" id="Seg_2884" s="T1716">pro.h:O</ta>
            <ta e="T1718" id="Seg_2885" s="T1717">v:pred 0.2.h:S</ta>
            <ta e="T1722" id="Seg_2886" s="T1721">v:pred</ta>
            <ta e="T1723" id="Seg_2887" s="T1722">np.h:S</ta>
            <ta e="T1724" id="Seg_2888" s="T1723">pro.h:S</ta>
            <ta e="T1725" id="Seg_2889" s="T1724">v:pred</ta>
            <ta e="T1726" id="Seg_2890" s="T1725">v:pred 0.2.h:S</ta>
            <ta e="T1727" id="Seg_2891" s="T1726">np:O</ta>
            <ta e="T1729" id="Seg_2892" s="T1728">np.h:S</ta>
            <ta e="T1730" id="Seg_2893" s="T1729">v:pred</ta>
            <ta e="T1731" id="Seg_2894" s="T1730">pro.h:S</ta>
            <ta e="T1733" id="Seg_2895" s="T1732">pro.h:S</ta>
            <ta e="T1735" id="Seg_2896" s="T1734">ptcl.neg</ta>
            <ta e="T1737" id="Seg_2897" s="T1736">n:pred</ta>
            <ta e="T1741" id="Seg_2898" s="T1740">np:S</ta>
            <ta e="T1742" id="Seg_2899" s="T1741">ptcl.neg</ta>
            <ta e="T1744" id="Seg_2900" s="T1743">ptcl.neg</ta>
            <ta e="T1745" id="Seg_2901" s="T1744">adj:pred</ta>
            <ta e="T1747" id="Seg_2902" s="T1746">np:S</ta>
            <ta e="T1748" id="Seg_2903" s="T1747">adj:pred</ta>
            <ta e="T1750" id="Seg_2904" s="T1749">pro.h:S</ta>
            <ta e="T1751" id="Seg_2905" s="T1750">conv:pred</ta>
            <ta e="T1752" id="Seg_2906" s="T1751">v:pred</ta>
            <ta e="T1753" id="Seg_2907" s="T1752">np.h:S</ta>
            <ta e="T1754" id="Seg_2908" s="T1753">v:pred</ta>
            <ta e="T1757" id="Seg_2909" s="T1756">v:pred 0.3.h:S</ta>
            <ta e="T1759" id="Seg_2910" s="T1758">v:pred 0.2.h:S</ta>
            <ta e="T1760" id="Seg_2911" s="T1759">np:O</ta>
            <ta e="T1761" id="Seg_2912" s="T1760">pro.h:S</ta>
            <ta e="T1762" id="Seg_2913" s="T1761">v:pred</ta>
            <ta e="T1763" id="Seg_2914" s="T1762">pro.h:S</ta>
            <ta e="T1765" id="Seg_2915" s="T1764">s:purp</ta>
            <ta e="T1766" id="Seg_2916" s="T1765">v:pred</ta>
            <ta e="T1767" id="Seg_2917" s="T1766">pro.h:S</ta>
            <ta e="T1768" id="Seg_2918" s="T1767">v:pred</ta>
            <ta e="T1769" id="Seg_2919" s="T1768">v:pred</ta>
            <ta e="T1771" id="Seg_2920" s="T1770">np.h:S</ta>
            <ta e="T1773" id="Seg_2921" s="T1772">adj:pred</ta>
            <ta e="T1774" id="Seg_2922" s="T1773">np:S</ta>
            <ta e="T1775" id="Seg_2923" s="T1774">v:pred 0.2.h:S</ta>
            <ta e="T1778" id="Seg_2924" s="T1777">conv:pred</ta>
            <ta e="T1779" id="Seg_2925" s="T1778">v:pred 0.3.h:S</ta>
            <ta e="T1781" id="Seg_2926" s="T1780">np.h:S</ta>
            <ta e="T1782" id="Seg_2927" s="T1781">v:pred</ta>
            <ta e="T1785" id="Seg_2928" s="T1784">v:pred 0.3.h:S</ta>
            <ta e="T1786" id="Seg_2929" s="T1785">v:pred 0.2.h:S</ta>
            <ta e="T1787" id="Seg_2930" s="T1786">pro.h:O</ta>
            <ta e="T1790" id="Seg_2931" s="T1789">np:O</ta>
            <ta e="T1791" id="Seg_2932" s="T1790">v:pred 0.1.h:S</ta>
            <ta e="T1793" id="Seg_2933" s="T1792">pro.h:S</ta>
            <ta e="T1794" id="Seg_2934" s="T1793">v:pred</ta>
            <ta e="T1795" id="Seg_2935" s="T1794">pro.h:S</ta>
            <ta e="T1797" id="Seg_2936" s="T1796">v:pred</ta>
            <ta e="T1798" id="Seg_2937" s="T1797">np.h:S</ta>
            <ta e="T1801" id="Seg_2938" s="T1800">v:pred</ta>
            <ta e="T1804" id="Seg_2939" s="T1803">v:pred</ta>
            <ta e="T1805" id="Seg_2940" s="T1804">np.h:S</ta>
            <ta e="T1806" id="Seg_2941" s="T1805">v:pred</ta>
            <ta e="T1807" id="Seg_2942" s="T1806">v:pred 0.3.h:S</ta>
            <ta e="T1808" id="Seg_2943" s="T1807">v:pred 0.3.h:S</ta>
            <ta e="T1809" id="Seg_2944" s="T1808">v:pred</ta>
            <ta e="T1811" id="Seg_2945" s="T1810">v:pred 0.3.h:S</ta>
            <ta e="T1812" id="Seg_2946" s="T1811">np.h:S</ta>
            <ta e="T1813" id="Seg_2947" s="T1812">v:pred</ta>
            <ta e="T1814" id="Seg_2948" s="T1813">np.h:O</ta>
            <ta e="T1815" id="Seg_2949" s="T1814">v:pred 0.3.h:S</ta>
            <ta e="T1816" id="Seg_2950" s="T1815">pro.h:S</ta>
            <ta e="T1817" id="Seg_2951" s="T1816">v:pred</ta>
            <ta e="T1818" id="Seg_2952" s="T1817">v:pred</ta>
            <ta e="T1819" id="Seg_2953" s="T1818">np.h:S</ta>
            <ta e="T1820" id="Seg_2954" s="T1819">pro.h:S</ta>
            <ta e="T1821" id="Seg_2955" s="T1820">np.h:O</ta>
            <ta e="T1822" id="Seg_2956" s="T1821">v:pred</ta>
            <ta e="T1824" id="Seg_2957" s="T1823">pro.h:S</ta>
            <ta e="T1826" id="Seg_2958" s="T1825">v:pred</ta>
            <ta e="T1828" id="Seg_2959" s="T1827">v:pred 0.3.h:S</ta>
            <ta e="T1830" id="Seg_2960" s="T1829">pro.h:S</ta>
            <ta e="T1832" id="Seg_2961" s="T1831">conv:pred</ta>
            <ta e="T1833" id="Seg_2962" s="T1832">v:pred</ta>
            <ta e="T1837" id="Seg_2963" s="T1836">np.h:S</ta>
            <ta e="T1839" id="Seg_2964" s="T1838">np.h:S</ta>
            <ta e="T1840" id="Seg_2965" s="T1839">v:pred</ta>
            <ta e="T1842" id="Seg_2966" s="T1841">v:pred 0.2.h:S</ta>
            <ta e="T1843" id="Seg_2967" s="T1842">pro.h:O</ta>
            <ta e="T1844" id="Seg_2968" s="T1843">ptcl.neg</ta>
            <ta e="T1845" id="Seg_2969" s="T1844">pro.h:S</ta>
            <ta e="T1846" id="Seg_2970" s="T1845">v:pred</ta>
            <ta e="T1847" id="Seg_2971" s="T1846">v:pred 0.1.h:S</ta>
            <ta e="T1850" id="Seg_2972" s="T1849">v:pred 0.1.h:S</ta>
            <ta e="T1852" id="Seg_2973" s="T1851">v:pred 0.1.h:S</ta>
            <ta e="T1853" id="Seg_2974" s="T1852">v:pred 0.3.h:S</ta>
            <ta e="T1856" id="Seg_2975" s="T1855">v:pred 0.3.h:S</ta>
            <ta e="T1858" id="Seg_2976" s="T1857">np:O</ta>
            <ta e="T1861" id="Seg_2977" s="T1860">np:S</ta>
            <ta e="T1862" id="Seg_2978" s="T1861">v:pred</ta>
            <ta e="T1864" id="Seg_2979" s="T1863">np:S</ta>
            <ta e="T1865" id="Seg_2980" s="T1864">v:pred</ta>
            <ta e="T1867" id="Seg_2981" s="T1866">v:pred 0.2.h:S</ta>
            <ta e="T1868" id="Seg_2982" s="T1867">pro.h:S</ta>
            <ta e="T1869" id="Seg_2983" s="T1868">v:pred</ta>
            <ta e="T1872" id="Seg_2984" s="T1871">np:S</ta>
            <ta e="T1873" id="Seg_2985" s="T1872">v:pred</ta>
            <ta e="T1875" id="Seg_2986" s="T1874">np:S</ta>
            <ta e="T1876" id="Seg_2987" s="T1875">v:pred</ta>
            <ta e="T1878" id="Seg_2988" s="T1877">np.h:S</ta>
            <ta e="T1880" id="Seg_2989" s="T1879">v:pred</ta>
            <ta e="T1882" id="Seg_2990" s="T1881">v:pred 0.3.h:S</ta>
            <ta e="T1885" id="Seg_2991" s="T1884">pro.h:S</ta>
            <ta e="T1886" id="Seg_2992" s="T1885">v:pred</ta>
            <ta e="T1887" id="Seg_2993" s="T1886">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T1674" id="Seg_2994" s="T1673">TAT:cult</ta>
            <ta e="T1692" id="Seg_2995" s="T1691">TURK:cult</ta>
            <ta e="T1703" id="Seg_2996" s="T1702">RUS:gram</ta>
            <ta e="T1723" id="Seg_2997" s="T1722">RUS:cult</ta>
            <ta e="T1732" id="Seg_2998" s="T1731">TURK:disc</ta>
            <ta e="T1741" id="Seg_2999" s="T1740">RUS:core</ta>
            <ta e="T1746" id="Seg_3000" s="T1745">RUS:gram</ta>
            <ta e="T1771" id="Seg_3001" s="T1770">RUS:cult</ta>
            <ta e="T1774" id="Seg_3002" s="T1773">RUS:core</ta>
            <ta e="T1781" id="Seg_3003" s="T1780">RUS:cult</ta>
            <ta e="T1784" id="Seg_3004" s="T1783">RUS:core</ta>
            <ta e="T1785" id="Seg_3005" s="T1784">%TURK:core</ta>
            <ta e="T1790" id="Seg_3006" s="T1789">TURK:cult</ta>
            <ta e="T1796" id="Seg_3007" s="T1795">TURK:disc</ta>
            <ta e="T1799" id="Seg_3008" s="T1798">RUS:cult</ta>
            <ta e="T1802" id="Seg_3009" s="T1801">RUS:gram</ta>
            <ta e="T1819" id="Seg_3010" s="T1818">RUS:cult</ta>
            <ta e="T1821" id="Seg_3011" s="T1820">TURK:disc</ta>
            <ta e="T1823" id="Seg_3012" s="T1822">RUS:mod</ta>
            <ta e="T1831" id="Seg_3013" s="T1830">TURK:disc</ta>
            <ta e="T1839" id="Seg_3014" s="T1838">RUS:cult</ta>
            <ta e="T1849" id="Seg_3015" s="T1848">TURK:disc</ta>
            <ta e="T1854" id="Seg_3016" s="T1853">RUS:gram</ta>
            <ta e="T1858" id="Seg_3017" s="T1857">RUS:core</ta>
            <ta e="T1860" id="Seg_3018" s="T1859">TURK:disc</ta>
            <ta e="T1863" id="Seg_3019" s="T1862">RUS:gram</ta>
            <ta e="T1864" id="Seg_3020" s="T1863">RUS:cult</ta>
            <ta e="T1866" id="Seg_3021" s="T1865">RUS:disc</ta>
            <ta e="T1870" id="Seg_3022" s="T1869">RUS:gram</ta>
            <ta e="T1877" id="Seg_3023" s="T1876">RUS:gram</ta>
            <ta e="T1881" id="Seg_3024" s="T1880">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T1672" id="Seg_3025" s="T1670">Жила коза.</ta>
            <ta e="T1675" id="Seg_3026" s="T1672">У неё был дом.</ta>
            <ta e="T1677" id="Seg_3027" s="T1675">Привела детей [туда].</ta>
            <ta e="T1680" id="Seg_3028" s="T1677">Потом ушла [и говорит]: «Сидите здесь!</ta>
            <ta e="T1683" id="Seg_3029" s="T1680">Никого не пускайте!»</ta>
            <ta e="T1684" id="Seg_3030" s="T1683">Ушла.</ta>
            <ta e="T1686" id="Seg_3031" s="T1684">Потом пришла.</ta>
            <ta e="T1688" id="Seg_3032" s="T1686">«Откройте дверь!</ta>
            <ta e="T1691" id="Seg_3033" s="T1688">Ваша мать пришла.</ta>
            <ta e="T1694" id="Seg_3034" s="T1691">Молока вам принесла».</ta>
            <ta e="T1697" id="Seg_3035" s="T1694">Тогда он [=они] закрыли [=открыли].</ta>
            <ta e="T1698" id="Seg_3036" s="T1697">(Они закрыли.)</ta>
            <ta e="T1699" id="Seg_3037" s="T1698">Они открыли.</ta>
            <ta e="T1702" id="Seg_3038" s="T1699">Тогда она вошла.</ta>
            <ta e="T1709" id="Seg_3039" s="T1702">Дала им поесть.</ta>
            <ta e="T1711" id="Seg_3040" s="T1709">Они поели.</ta>
            <ta e="T1715" id="Seg_3041" s="T1711">Потом она снова ушла.</ta>
            <ta e="T1720" id="Seg_3042" s="T1715">«Сидите [здесь], никого не пускайте!»</ta>
            <ta e="T1723" id="Seg_3043" s="T1720">Потом пришёл волк.</ta>
            <ta e="T1725" id="Seg_3044" s="T1723">Он говорит:</ta>
            <ta e="T1730" id="Seg_3045" s="T1725">«Откройте, ваша мать пришла!»</ta>
            <ta e="T1731" id="Seg_3046" s="T1730">Они [говорят]:</ta>
            <ta e="T1737" id="Seg_3047" s="T1731">«Нет, это не наша мать.</ta>
            <ta e="T1748" id="Seg_3048" s="T1737">У нашей матери голос не такой, не грубый, а у тебя грубый».</ta>
            <ta e="T1752" id="Seg_3049" s="T1748">Потом он ушёл.</ta>
            <ta e="T1755" id="Seg_3050" s="T1752">Их мать пришла.</ta>
            <ta e="T1757" id="Seg_3051" s="T1755">Снова кричит:</ta>
            <ta e="T1760" id="Seg_3052" s="T1757">«Откройте дверь!»</ta>
            <ta e="T1762" id="Seg_3053" s="T1760">Они открыли.</ta>
            <ta e="T1766" id="Seg_3054" s="T1762">Она дала им поесть.</ta>
            <ta e="T1768" id="Seg_3055" s="T1766">Они говорят:</ta>
            <ta e="T1771" id="Seg_3056" s="T1768">«Сюда волк приходил.</ta>
            <ta e="T1774" id="Seg_3057" s="T1771">[У него] голос очень грубый».</ta>
            <ta e="T1776" id="Seg_3058" s="T1774">«Не пускайте [его]!»</ta>
            <ta e="T1779" id="Seg_3059" s="T1776">Потом она ушла.</ta>
            <ta e="T1782" id="Seg_3060" s="T1779">Этот волк пришёл.</ta>
            <ta e="T1785" id="Seg_3061" s="T1782">Он заговорил тонким голосом.</ta>
            <ta e="T1787" id="Seg_3062" s="T1785">«Пустите меня!</ta>
            <ta e="T1791" id="Seg_3063" s="T1787">Я вам молока принесла».</ta>
            <ta e="T1794" id="Seg_3064" s="T1791">Тогда они [его] впустили.</ta>
            <ta e="T1804" id="Seg_3065" s="T1794">Он их съел, а один [козлёнок] залез в печку и там сидел.</ta>
            <ta e="T1809" id="Seg_3066" s="T1804">Их мать пришла, зовёт, зовёт, ничего [не происходит].</ta>
            <ta e="T1813" id="Seg_3067" s="T1809">Потом вошла, а детей нет.</ta>
            <ta e="T1817" id="Seg_3068" s="T1813">Она нашла одного, он говорит:</ta>
            <ta e="T1822" id="Seg_3069" s="T1817">«Волк пришёл и всех съел.</ta>
            <ta e="T1826" id="Seg_3070" s="T1822">Только я один остался».</ta>
            <ta e="T1833" id="Seg_3071" s="T1826">Потом вышла наружу, сидит и плачет.</ta>
            <ta e="T1837" id="Seg_3072" s="T1833">«Где мои дети!»</ta>
            <ta e="T1840" id="Seg_3073" s="T1837">Пришёл тот волк.</ta>
            <ta e="T1846" id="Seg_3074" s="T1840">«Что плачешь, это не я их съел.</ta>
            <ta e="T1848" id="Seg_3075" s="T1846">Пойдём со мной!»</ta>
            <ta e="T1851" id="Seg_3076" s="T1848">«Нет, не пойду».</ta>
            <ta e="T1852" id="Seg_3077" s="T1851">«Пойдём!»</ta>
            <ta e="T1853" id="Seg_3078" s="T1852">Пошли.</ta>
            <ta e="T1858" id="Seg_3079" s="T1853">А там увидели большую яму.</ta>
            <ta e="T1862" id="Seg_3080" s="T1858">Там огонь лежит [=горит].</ta>
            <ta e="T1865" id="Seg_3081" s="T1862">И каша лежит.</ta>
            <ta e="T1867" id="Seg_3082" s="T1865">«Ну, прыгай!»</ta>
            <ta e="T1873" id="Seg_3083" s="T1867">Он прыгнул, а там огонь был.</ta>
            <ta e="T1876" id="Seg_3084" s="T1873">Его живот лопнул.</ta>
            <ta e="T1880" id="Seg_3085" s="T1876">И козы [=козлята] вылезли у него из живота.</ta>
            <ta e="T1883" id="Seg_3086" s="T1880">И побежали домой.</ta>
            <ta e="T1887" id="Seg_3087" s="T1883">Потом пришли и жили.</ta>
            <ta e="T1888" id="Seg_3088" s="T1887">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T1672" id="Seg_3089" s="T1670">[Once] there lived a goat.</ta>
            <ta e="T1675" id="Seg_3090" s="T1672">She had a house.</ta>
            <ta e="T1677" id="Seg_3091" s="T1675">She brought children [there].</ta>
            <ta e="T1680" id="Seg_3092" s="T1677">Then she went: "Sit there!</ta>
            <ta e="T1683" id="Seg_3093" s="T1680">Don’t let anyone [in]!"</ta>
            <ta e="T1684" id="Seg_3094" s="T1683">She went.</ta>
            <ta e="T1686" id="Seg_3095" s="T1684">Then she came.</ta>
            <ta e="T1688" id="Seg_3096" s="T1686">"Open the door!</ta>
            <ta e="T1691" id="Seg_3097" s="T1688">Your mother came.</ta>
            <ta e="T1694" id="Seg_3098" s="T1691">She brought you milk!"</ta>
            <ta e="T1697" id="Seg_3099" s="T1694">Then it [=they] closed [=opened].</ta>
            <ta e="T1698" id="Seg_3100" s="T1697">(They closed.)</ta>
            <ta e="T1699" id="Seg_3101" s="T1698">They opened.</ta>
            <ta e="T1702" id="Seg_3102" s="T1699">Then she came.</ta>
            <ta e="T1709" id="Seg_3103" s="T1702">She gave them [something] to eat.</ta>
            <ta e="T1711" id="Seg_3104" s="T1709">They ate.</ta>
            <ta e="T1715" id="Seg_3105" s="T1711">Then again she left.</ta>
            <ta e="T1720" id="Seg_3106" s="T1715">"Sit [here], don't let anyone in!"</ta>
            <ta e="T1723" id="Seg_3107" s="T1720">Then a wolf came.</ta>
            <ta e="T1725" id="Seg_3108" s="T1723">He says:</ta>
            <ta e="T1730" id="Seg_3109" s="T1725">"Open the door, your mother came!"</ta>
            <ta e="T1731" id="Seg_3110" s="T1730">They [say]:</ta>
            <ta e="T1737" id="Seg_3111" s="T1731">"No, it is not our mother.</ta>
            <ta e="T1748" id="Seg_3112" s="T1737">“Our mother's voice is not like that, not [so] coarse, and yours is coarse.”</ta>
            <ta e="T1752" id="Seg_3113" s="T1748">Then he left.</ta>
            <ta e="T1755" id="Seg_3114" s="T1752">Their mother came.</ta>
            <ta e="T1757" id="Seg_3115" s="T1755">Again it is calling.</ta>
            <ta e="T1760" id="Seg_3116" s="T1757">“Open the door!”</ta>
            <ta e="T1762" id="Seg_3117" s="T1760">They opened.</ta>
            <ta e="T1766" id="Seg_3118" s="T1762">She gave them [something] to eat.</ta>
            <ta e="T1768" id="Seg_3119" s="T1766">They say:</ta>
            <ta e="T1771" id="Seg_3120" s="T1768">"A wolf came here.</ta>
            <ta e="T1774" id="Seg_3121" s="T1771">His voice was very coarse."</ta>
            <ta e="T1776" id="Seg_3122" s="T1774">"Don't let [him] in!"</ta>
            <ta e="T1779" id="Seg_3123" s="T1776">Then she left.</ta>
            <ta e="T1782" id="Seg_3124" s="T1779">The wolf came.</ta>
            <ta e="T1785" id="Seg_3125" s="T1782">He spoke with a high voice.</ta>
            <ta e="T1787" id="Seg_3126" s="T1785">"Let me in!</ta>
            <ta e="T1791" id="Seg_3127" s="T1787">I brought you milk."</ta>
            <ta e="T1794" id="Seg_3128" s="T1791">Then they let [him] in.</ta>
            <ta e="T1804" id="Seg_3129" s="T1794">He ate them, one [little goat] climbed into the oven and sat there.</ta>
            <ta e="T1809" id="Seg_3130" s="T1804">Their mother came, called, called, there is nothing.</ta>
            <ta e="T1813" id="Seg_3131" s="T1809">Then she came, her children are gone.</ta>
            <ta e="T1817" id="Seg_3132" s="T1813">She found one, it says:</ta>
            <ta e="T1822" id="Seg_3133" s="T1817">"The wolf came, he ate everyone.</ta>
            <ta e="T1826" id="Seg_3134" s="T1822">Only I left alone."</ta>
            <ta e="T1833" id="Seg_3135" s="T1826">Then she went out, she is sitting crying.</ta>
            <ta e="T1837" id="Seg_3136" s="T1833">"Where are my children!"</ta>
            <ta e="T1840" id="Seg_3137" s="T1837">The wolf came.</ta>
            <ta e="T1846" id="Seg_3138" s="T1840">"Why are you crying, I did not eat it [them]."</ta>
            <ta e="T1848" id="Seg_3139" s="T1846">Let's go with me!"</ta>
            <ta e="T1851" id="Seg_3140" s="T1848">"No, I will not go."</ta>
            <ta e="T1852" id="Seg_3141" s="T1851">"Let's go!"</ta>
            <ta e="T1853" id="Seg_3142" s="T1852">They went.</ta>
            <ta e="T1858" id="Seg_3143" s="T1853">And there they saw a big pit.</ta>
            <ta e="T1862" id="Seg_3144" s="T1858">There is fire lying [= burning].</ta>
            <ta e="T1865" id="Seg_3145" s="T1862">And porridge is lying.</ta>
            <ta e="T1867" id="Seg_3146" s="T1865">"Well, jump!"</ta>
            <ta e="T1873" id="Seg_3147" s="T1867">He jumped but there was fire [below].</ta>
            <ta e="T1876" id="Seg_3148" s="T1873">Its stomach splitted [from the heat].</ta>
            <ta e="T1880" id="Seg_3149" s="T1876">And the goats [=goatlings] came out from its stomach.</ta>
            <ta e="T1883" id="Seg_3150" s="T1880">And ran home.</ta>
            <ta e="T1887" id="Seg_3151" s="T1883">Then they went, lived.</ta>
            <ta e="T1888" id="Seg_3152" s="T1887">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T1672" id="Seg_3153" s="T1670">Es lebte [einmal] eine Ziege.</ta>
            <ta e="T1675" id="Seg_3154" s="T1672">Sie hatte ein Haus.</ta>
            <ta e="T1677" id="Seg_3155" s="T1675">Sie brachte Kinder [dort].</ta>
            <ta e="T1680" id="Seg_3156" s="T1677">Dann ging sie: „Sitzt dort!</ta>
            <ta e="T1683" id="Seg_3157" s="T1680">Lass niemanden [rein]!“</ta>
            <ta e="T1684" id="Seg_3158" s="T1683">Sie ging.</ta>
            <ta e="T1686" id="Seg_3159" s="T1684">Dann kam sie.</ta>
            <ta e="T1688" id="Seg_3160" s="T1686">„Öffnet die Tür!</ta>
            <ta e="T1691" id="Seg_3161" s="T1688">Eure Mutter ist gekommen.</ta>
            <ta e="T1694" id="Seg_3162" s="T1691">Sie hat euch Milch gebracht!“</ta>
            <ta e="T1697" id="Seg_3163" s="T1694">Dann sie [=die Kinder] sperrten zu [=machten auf].</ta>
            <ta e="T1698" id="Seg_3164" s="T1697">(Sie sperrten zu.)</ta>
            <ta e="T1699" id="Seg_3165" s="T1698">Sie machten auf.</ta>
            <ta e="T1702" id="Seg_3166" s="T1699">Dann kam sie.</ta>
            <ta e="T1709" id="Seg_3167" s="T1702">Sie gab ihnen [etwas] zu essen.</ta>
            <ta e="T1711" id="Seg_3168" s="T1709">Sie aßen.</ta>
            <ta e="T1715" id="Seg_3169" s="T1711">Dann ging sie wieder.</ta>
            <ta e="T1720" id="Seg_3170" s="T1715">„Sitzt [hier], lasst niemanden rein!“</ta>
            <ta e="T1723" id="Seg_3171" s="T1720">Dann kam ein Wolf.</ta>
            <ta e="T1725" id="Seg_3172" s="T1723">Er sagt:</ta>
            <ta e="T1730" id="Seg_3173" s="T1725">„Öffnet die Tür, eure Mutter ist gekommen!“</ta>
            <ta e="T1731" id="Seg_3174" s="T1730">Sie [sagen]:</ta>
            <ta e="T1737" id="Seg_3175" s="T1731">„Nein, es ist nicht unsere Mutter.</ta>
            <ta e="T1748" id="Seg_3176" s="T1737">„Die Stimme unserer Mutter ist anders, nicht [so] rauh, und deine ist rauh.“</ta>
            <ta e="T1752" id="Seg_3177" s="T1748">Dann ging er.</ta>
            <ta e="T1755" id="Seg_3178" s="T1752">Ihre Mutter kam.</ta>
            <ta e="T1757" id="Seg_3179" s="T1755">Wieder ruft sie.</ta>
            <ta e="T1760" id="Seg_3180" s="T1757">„Öffnet die Tür!“</ta>
            <ta e="T1762" id="Seg_3181" s="T1760">Sie machten auf.</ta>
            <ta e="T1766" id="Seg_3182" s="T1762">Sie gab ihnen [etwas] zu essen.</ta>
            <ta e="T1768" id="Seg_3183" s="T1766">Sie sagen:</ta>
            <ta e="T1771" id="Seg_3184" s="T1768">„Ein Wolf kam her.</ta>
            <ta e="T1774" id="Seg_3185" s="T1771">Seine Stimme war sehr rauh.“</ta>
            <ta e="T1776" id="Seg_3186" s="T1774">„Lasst [ihn] nicht rein!“</ta>
            <ta e="T1779" id="Seg_3187" s="T1776">Dann ging sie.</ta>
            <ta e="T1782" id="Seg_3188" s="T1779">Der Wolf kam.</ta>
            <ta e="T1785" id="Seg_3189" s="T1782">Er sprach mit einer hohen Stimme.</ta>
            <ta e="T1787" id="Seg_3190" s="T1785">„Lasst mich rein!</ta>
            <ta e="T1791" id="Seg_3191" s="T1787">Ich habe euch Milch gebracht.“</ta>
            <ta e="T1794" id="Seg_3192" s="T1791">Dann ließen sie [ihn] rein.</ta>
            <ta e="T1804" id="Seg_3193" s="T1794">Er frass sie auf, eine [kleine Ziege] kletterte in den Ofen hinein und saß dort.</ta>
            <ta e="T1809" id="Seg_3194" s="T1804">Ihre Mutter kam, rief, es ist nichts.</ta>
            <ta e="T1813" id="Seg_3195" s="T1809">Dann kam sie, ihre Kinder sind weg.</ta>
            <ta e="T1817" id="Seg_3196" s="T1813">Sie fand eins, es sagte:</ta>
            <ta e="T1822" id="Seg_3197" s="T1817">„Der Wolf kam, er hat alle aufgefressen.</ta>
            <ta e="T1826" id="Seg_3198" s="T1822">Nur ich blieb allein.“</ta>
            <ta e="T1833" id="Seg_3199" s="T1826">Dann ging sie hinaus, sie sitzt und weint.</ta>
            <ta e="T1837" id="Seg_3200" s="T1833">„Wo sind meine Kinder!“</ta>
            <ta e="T1840" id="Seg_3201" s="T1837">Der Wolf kam.</ta>
            <ta e="T1846" id="Seg_3202" s="T1840">„Warum weinst du, ich habe [sie] nicht gegessen.“</ta>
            <ta e="T1848" id="Seg_3203" s="T1846">„Komm mit mir!“</ta>
            <ta e="T1851" id="Seg_3204" s="T1848">„Nein, ich werde nicht gehen.“</ta>
            <ta e="T1852" id="Seg_3205" s="T1851">“Gehen wir!”</ta>
            <ta e="T1853" id="Seg_3206" s="T1852">Sie gingen.</ta>
            <ta e="T1858" id="Seg_3207" s="T1853">Und da sahen sie eine große Grube.</ta>
            <ta e="T1862" id="Seg_3208" s="T1858">Es liegt ein Feuer [= brennt].</ta>
            <ta e="T1865" id="Seg_3209" s="T1862">Und brei liegt.</ta>
            <ta e="T1867" id="Seg_3210" s="T1865">“Also, spring!”</ta>
            <ta e="T1873" id="Seg_3211" s="T1867">Er sprang aber es war Feuer [unterhalb].</ta>
            <ta e="T1876" id="Seg_3212" s="T1873">Sein Bauch platzte [von der Hitze].</ta>
            <ta e="T1880" id="Seg_3213" s="T1876">Und die Ziegen [=Zieglein] kamen heraus von seinem Bauch.</ta>
            <ta e="T1883" id="Seg_3214" s="T1880">Und liefen nach Hause.</ta>
            <ta e="T1887" id="Seg_3215" s="T1883">Dann gingen sie, lebten.</ta>
            <ta e="T1888" id="Seg_3216" s="T1887">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T1672" id="Seg_3217" s="T1670">[GVY:] A well-known story about a wolf and seven goatlings. A version with a fire in a pit see e.g. http://vseskazki.su/narodnye-skazki/russkie-narodnie-skazki/volk-i-semero-kozlyat-chitat.html</ta>
            <ta e="T1683" id="Seg_3218" s="T1680">[GVY:] šindində = šindidə?</ta>
            <ta e="T1697" id="Seg_3219" s="T1694">[KlT:] kajbiʔi =karbiʔi 'opened', see further.</ta>
            <ta e="T1698" id="Seg_3220" s="T1697">She probably means 'opened'.</ta>
            <ta e="T1720" id="Seg_3221" s="T1715">[GVY:] or ölüʔlügəʔ?</ta>
            <ta e="T1755" id="Seg_3222" s="T1752">Ŋ instead of n?</ta>
            <ta e="T1762" id="Seg_3223" s="T1760">SG instead of PL.</ta>
            <ta e="T1865" id="Seg_3224" s="T1862">[GVY:] Why the porridge is mentioned, is not clear.</ta>
            <ta e="T1880" id="Seg_3225" s="T1876">[GVY:] nanəndə = nanəgəttə [stomach-ABL]?</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1670" />
            <conversion-tli id="T1671" />
            <conversion-tli id="T1672" />
            <conversion-tli id="T1673" />
            <conversion-tli id="T1674" />
            <conversion-tli id="T1675" />
            <conversion-tli id="T1676" />
            <conversion-tli id="T1677" />
            <conversion-tli id="T1678" />
            <conversion-tli id="T1679" />
            <conversion-tli id="T1680" />
            <conversion-tli id="T1681" />
            <conversion-tli id="T1682" />
            <conversion-tli id="T1683" />
            <conversion-tli id="T1684" />
            <conversion-tli id="T1685" />
            <conversion-tli id="T1686" />
            <conversion-tli id="T1687" />
            <conversion-tli id="T1688" />
            <conversion-tli id="T1689" />
            <conversion-tli id="T1690" />
            <conversion-tli id="T1691" />
            <conversion-tli id="T1692" />
            <conversion-tli id="T1693" />
            <conversion-tli id="T1694" />
            <conversion-tli id="T1695" />
            <conversion-tli id="T1696" />
            <conversion-tli id="T1697" />
            <conversion-tli id="T1698" />
            <conversion-tli id="T1699" />
            <conversion-tli id="T1700" />
            <conversion-tli id="T1701" />
            <conversion-tli id="T1702" />
            <conversion-tli id="T1703" />
            <conversion-tli id="T1704" />
            <conversion-tli id="T1705" />
            <conversion-tli id="T1706" />
            <conversion-tli id="T1707" />
            <conversion-tli id="T1708" />
            <conversion-tli id="T1709" />
            <conversion-tli id="T1710" />
            <conversion-tli id="T1711" />
            <conversion-tli id="T1712" />
            <conversion-tli id="T1713" />
            <conversion-tli id="T1714" />
            <conversion-tli id="T1715" />
            <conversion-tli id="T1716" />
            <conversion-tli id="T1717" />
            <conversion-tli id="T1718" />
            <conversion-tli id="T1719" />
            <conversion-tli id="T1720" />
            <conversion-tli id="T1721" />
            <conversion-tli id="T1722" />
            <conversion-tli id="T1723" />
            <conversion-tli id="T1724" />
            <conversion-tli id="T1725" />
            <conversion-tli id="T1726" />
            <conversion-tli id="T1727" />
            <conversion-tli id="T1728" />
            <conversion-tli id="T1729" />
            <conversion-tli id="T1730" />
            <conversion-tli id="T1731" />
            <conversion-tli id="T1732" />
            <conversion-tli id="T1733" />
            <conversion-tli id="T1734" />
            <conversion-tli id="T1735" />
            <conversion-tli id="T1736" />
            <conversion-tli id="T1737" />
            <conversion-tli id="T1738" />
            <conversion-tli id="T1739" />
            <conversion-tli id="T1740" />
            <conversion-tli id="T1741" />
            <conversion-tli id="T1742" />
            <conversion-tli id="T1743" />
            <conversion-tli id="T1744" />
            <conversion-tli id="T1745" />
            <conversion-tli id="T1746" />
            <conversion-tli id="T1747" />
            <conversion-tli id="T1748" />
            <conversion-tli id="T1749" />
            <conversion-tli id="T1750" />
            <conversion-tli id="T1751" />
            <conversion-tli id="T1752" />
            <conversion-tli id="T1753" />
            <conversion-tli id="T1754" />
            <conversion-tli id="T1755" />
            <conversion-tli id="T1756" />
            <conversion-tli id="T1757" />
            <conversion-tli id="T1758" />
            <conversion-tli id="T1759" />
            <conversion-tli id="T1760" />
            <conversion-tli id="T1761" />
            <conversion-tli id="T1762" />
            <conversion-tli id="T1763" />
            <conversion-tli id="T1764" />
            <conversion-tli id="T1765" />
            <conversion-tli id="T1766" />
            <conversion-tli id="T1767" />
            <conversion-tli id="T1768" />
            <conversion-tli id="T1769" />
            <conversion-tli id="T1770" />
            <conversion-tli id="T1771" />
            <conversion-tli id="T1772" />
            <conversion-tli id="T1773" />
            <conversion-tli id="T1774" />
            <conversion-tli id="T1775" />
            <conversion-tli id="T1776" />
            <conversion-tli id="T1777" />
            <conversion-tli id="T1778" />
            <conversion-tli id="T1779" />
            <conversion-tli id="T1780" />
            <conversion-tli id="T1781" />
            <conversion-tli id="T1782" />
            <conversion-tli id="T1783" />
            <conversion-tli id="T1784" />
            <conversion-tli id="T1785" />
            <conversion-tli id="T1786" />
            <conversion-tli id="T1787" />
            <conversion-tli id="T1788" />
            <conversion-tli id="T1789" />
            <conversion-tli id="T1790" />
            <conversion-tli id="T1791" />
            <conversion-tli id="T1792" />
            <conversion-tli id="T1793" />
            <conversion-tli id="T1794" />
            <conversion-tli id="T1795" />
            <conversion-tli id="T1796" />
            <conversion-tli id="T1797" />
            <conversion-tli id="T1798" />
            <conversion-tli id="T1799" />
            <conversion-tli id="T1800" />
            <conversion-tli id="T1801" />
            <conversion-tli id="T1802" />
            <conversion-tli id="T1803" />
            <conversion-tli id="T1804" />
            <conversion-tli id="T1805" />
            <conversion-tli id="T1806" />
            <conversion-tli id="T1807" />
            <conversion-tli id="T1808" />
            <conversion-tli id="T1809" />
            <conversion-tli id="T1810" />
            <conversion-tli id="T1811" />
            <conversion-tli id="T1812" />
            <conversion-tli id="T1813" />
            <conversion-tli id="T1814" />
            <conversion-tli id="T1815" />
            <conversion-tli id="T1816" />
            <conversion-tli id="T1817" />
            <conversion-tli id="T1818" />
            <conversion-tli id="T1819" />
            <conversion-tli id="T1820" />
            <conversion-tli id="T1821" />
            <conversion-tli id="T1822" />
            <conversion-tli id="T1823" />
            <conversion-tli id="T1824" />
            <conversion-tli id="T1825" />
            <conversion-tli id="T1826" />
            <conversion-tli id="T1827" />
            <conversion-tli id="T1828" />
            <conversion-tli id="T1829" />
            <conversion-tli id="T1830" />
            <conversion-tli id="T1831" />
            <conversion-tli id="T1832" />
            <conversion-tli id="T1833" />
            <conversion-tli id="T1834" />
            <conversion-tli id="T1835" />
            <conversion-tli id="T1836" />
            <conversion-tli id="T1837" />
            <conversion-tli id="T1838" />
            <conversion-tli id="T1839" />
            <conversion-tli id="T1840" />
            <conversion-tli id="T1841" />
            <conversion-tli id="T1842" />
            <conversion-tli id="T1843" />
            <conversion-tli id="T1844" />
            <conversion-tli id="T1845" />
            <conversion-tli id="T1846" />
            <conversion-tli id="T1847" />
            <conversion-tli id="T1848" />
            <conversion-tli id="T1849" />
            <conversion-tli id="T1850" />
            <conversion-tli id="T1851" />
            <conversion-tli id="T1852" />
            <conversion-tli id="T1853" />
            <conversion-tli id="T1854" />
            <conversion-tli id="T1855" />
            <conversion-tli id="T1856" />
            <conversion-tli id="T1857" />
            <conversion-tli id="T1858" />
            <conversion-tli id="T1859" />
            <conversion-tli id="T1860" />
            <conversion-tli id="T1861" />
            <conversion-tli id="T1862" />
            <conversion-tli id="T1863" />
            <conversion-tli id="T1864" />
            <conversion-tli id="T1865" />
            <conversion-tli id="T1866" />
            <conversion-tli id="T1867" />
            <conversion-tli id="T1868" />
            <conversion-tli id="T1869" />
            <conversion-tli id="T1870" />
            <conversion-tli id="T1871" />
            <conversion-tli id="T1872" />
            <conversion-tli id="T1873" />
            <conversion-tli id="T1874" />
            <conversion-tli id="T1875" />
            <conversion-tli id="T1876" />
            <conversion-tli id="T1877" />
            <conversion-tli id="T1878" />
            <conversion-tli id="T1879" />
            <conversion-tli id="T1880" />
            <conversion-tli id="T1881" />
            <conversion-tli id="T1882" />
            <conversion-tli id="T1883" />
            <conversion-tli id="T1884" />
            <conversion-tli id="T1885" />
            <conversion-tli id="T1886" />
            <conversion-tli id="T1887" />
            <conversion-tli id="T1888" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
