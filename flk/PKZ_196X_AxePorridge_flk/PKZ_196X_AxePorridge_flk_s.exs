<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID358FEAD5-FE3E-C610-6826-E764DC6E854E">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_AxePorridge_flk.wav" />
         <referenced-file url="PKZ_196X_AxePorridge_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_AxePorridge_flk\PKZ_196X_AxePorridge_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">127</ud-information>
            <ud-information attribute-name="# HIAT:w">79</ud-information>
            <ud-information attribute-name="# e">79</ud-information>
            <ud-information attribute-name="# HIAT:u">22</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.008" type="appl" />
         <tli id="T1" time="1.133" type="appl" />
         <tli id="T2" time="2.258" type="appl" />
         <tli id="T3" time="3.573069423929099" />
         <tli id="T4" time="4.189" type="appl" />
         <tli id="T5" time="4.809" type="appl" />
         <tli id="T6" time="5.472929098966027" />
         <tli id="T7" time="6.123" type="appl" />
         <tli id="T8" time="6.816" type="appl" />
         <tli id="T9" time="7.213" type="appl" />
         <tli id="T10" time="7.61" type="appl" />
         <tli id="T11" time="8.746020679468241" />
         <tli id="T12" time="9.429" type="appl" />
         <tli id="T13" time="9.929" type="appl" />
         <tli id="T14" time="10.428" type="appl" />
         <tli id="T15" time="10.928" type="appl" />
         <tli id="T16" time="11.427" type="appl" />
         <tli id="T17" time="12.115" type="appl" />
         <tli id="T18" time="12.802" type="appl" />
         <tli id="T19" time="14.398936484490399" />
         <tli id="T20" time="15.14" type="appl" />
         <tli id="T21" time="15.724671898079762" />
         <tli id="T22" time="16.418" type="appl" />
         <tli id="T23" time="17.039" type="appl" />
         <tli id="T24" time="17.659" type="appl" />
         <tli id="T25" time="18.328" type="appl" />
         <tli id="T26" time="18.997" type="appl" />
         <tli id="T27" time="19.666" type="appl" />
         <tli id="T28" time="21.171769571639587" />
         <tli id="T29" time="21.665" type="appl" />
         <tli id="T30" time="22.154" type="appl" />
         <tli id="T31" time="22.643" type="appl" />
         <tli id="T32" time="23.132" type="appl" />
         <tli id="T33" time="23.621" type="appl" />
         <tli id="T34" time="24.003341028751617" />
         <tli id="T35" time="24.736" type="appl" />
         <tli id="T36" time="25.361" type="appl" />
         <tli id="T37" time="25.987" type="appl" />
         <tli id="T38" time="26.408" type="appl" />
         <tli id="T39" time="26.829" type="appl" />
         <tli id="T40" time="27.25" type="appl" />
         <tli id="T41" time="28.114" type="appl" />
         <tli id="T42" time="29.051187592319057" />
         <tli id="T43" time="29.693" type="appl" />
         <tli id="T44" time="30.098" type="appl" />
         <tli id="T45" time="30.502" type="appl" />
         <tli id="T46" time="30.938" type="appl" />
         <tli id="T47" time="31.373" type="appl" />
         <tli id="T48" time="31.809" type="appl" />
         <tli id="T49" time="32.748" type="appl" />
         <tli id="T50" time="33.687" type="appl" />
         <tli id="T51" time="34.626" type="appl" />
         <tli id="T52" time="35.294" type="appl" />
         <tli id="T53" time="35.962" type="appl" />
         <tli id="T54" time="36.63" type="appl" />
         <tli id="T55" time="37.298" type="appl" />
         <tli id="T56" time="37.966" type="appl" />
         <tli id="T57" time="38.437" type="appl" />
         <tli id="T58" time="38.908" type="appl" />
         <tli id="T59" time="39.378" type="appl" />
         <tli id="T60" time="39.849" type="appl" />
         <tli id="T61" time="40.32" type="appl" />
         <tli id="T62" time="40.791" type="appl" />
         <tli id="T63" time="41.262" type="appl" />
         <tli id="T64" time="41.733" type="appl" />
         <tli id="T65" time="42.203" type="appl" />
         <tli id="T66" time="42.674" type="appl" />
         <tli id="T67" time="43.45012407680945" />
         <tli id="T68" time="44.261" type="appl" />
         <tli id="T69" time="44.916" type="appl" />
         <tli id="T78" time="45.35340303861574" type="intp" />
         <tli id="T70" time="45.936607090103394" />
         <tli id="T71" time="47.226" type="appl" />
         <tli id="T72" time="48.369" type="appl" />
         <tli id="T73" time="49.512" type="appl" />
         <tli id="T80" time="50.11424161215446" type="intp" />
         <tli id="T79" time="50.16524161215446" type="intp" />
         <tli id="T74" time="51.03623042836041" />
         <tli id="T75" time="52.413" type="appl" />
         <tli id="T76" time="53.284" type="appl" />
         <tli id="T77" time="54.156" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T77" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Šonəbi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">soldat</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">službagən</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T6" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Šobi</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">onʼiʔ</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">nükenə</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T8" id="Seg_26" n="HIAT:u" s="T6">
                  <nts id="Seg_27" n="HIAT:ip">"</nts>
                  <ts e="T7" id="Seg_29" n="HIAT:w" s="T6">Davaj</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_32" n="HIAT:w" s="T7">amorzittə</ts>
                  <nts id="Seg_33" n="HIAT:ip">"</nts>
                  <nts id="Seg_34" n="HIAT:ip">.</nts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_37" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_39" n="HIAT:w" s="T8">Dĭ</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_42" n="HIAT:w" s="T9">măndə:</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_44" n="HIAT:ip">"</nts>
                  <ts e="T11" id="Seg_46" n="HIAT:w" s="T10">Edəʔ</ts>
                  <nts id="Seg_47" n="HIAT:ip">!</nts>
                  <nts id="Seg_48" n="HIAT:ip">"</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_51" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_53" n="HIAT:w" s="T11">Dĭgəttə:</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_55" n="HIAT:ip">"</nts>
                  <ts e="T13" id="Seg_57" n="HIAT:w" s="T12">Tăn</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_60" n="HIAT:w" s="T13">ĭmbi</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_63" n="HIAT:w" s="T14">ej</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_66" n="HIAT:w" s="T15">nüngiel</ts>
                  <nts id="Seg_67" n="HIAT:ip">?</nts>
                  <nts id="Seg_68" n="HIAT:ip">"</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_71" n="HIAT:u" s="T16">
                  <nts id="Seg_72" n="HIAT:ip">"</nts>
                  <ts e="T17" id="Seg_74" n="HIAT:w" s="T16">No</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_77" n="HIAT:w" s="T17">gijen-nʼibudʼ</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_80" n="HIAT:w" s="T18">šaːləl</ts>
                  <nts id="Seg_81" n="HIAT:ip">"</nts>
                  <nts id="Seg_82" n="HIAT:ip">.</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_85" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_87" n="HIAT:w" s="T19">Davaj</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_90" n="HIAT:w" s="T20">amzittə</ts>
                  <nts id="Seg_91" n="HIAT:ip">.</nts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_94" n="HIAT:u" s="T21">
                  <nts id="Seg_95" n="HIAT:ip">"</nts>
                  <ts e="T22" id="Seg_97" n="HIAT:w" s="T21">Naga</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_100" n="HIAT:w" s="T22">ĭmbidə</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_103" n="HIAT:w" s="T23">amzittə</ts>
                  <nts id="Seg_104" n="HIAT:ip">"</nts>
                  <nts id="Seg_105" n="HIAT:ip">.</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_108" n="HIAT:u" s="T24">
                  <nts id="Seg_109" n="HIAT:ip">"</nts>
                  <ts e="T25" id="Seg_111" n="HIAT:w" s="T24">Davaj</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_114" n="HIAT:w" s="T25">baltu</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_117" n="HIAT:w" s="T26">măn</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_120" n="HIAT:w" s="T27">mĭnzərlem</ts>
                  <nts id="Seg_121" n="HIAT:ip">!</nts>
                  <nts id="Seg_122" n="HIAT:ip">"</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_125" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_127" n="HIAT:w" s="T28">Dĭ</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_130" n="HIAT:w" s="T29">mĭbi</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_133" n="HIAT:w" s="T30">baltu</ts>
                  <nts id="Seg_134" n="HIAT:ip">,</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_137" n="HIAT:w" s="T31">dĭ</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_140" n="HIAT:w" s="T32">mĭnzərbi</ts>
                  <nts id="Seg_141" n="HIAT:ip">,</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_144" n="HIAT:w" s="T33">mĭnzərbi</ts>
                  <nts id="Seg_145" n="HIAT:ip">.</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_148" n="HIAT:u" s="T34">
                  <nts id="Seg_149" n="HIAT:ip">"</nts>
                  <ts e="T35" id="Seg_151" n="HIAT:w" s="T34">Nada</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_154" n="HIAT:w" s="T35">idʼiʔeje</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_157" n="HIAT:w" s="T36">krupa</ts>
                  <nts id="Seg_158" n="HIAT:ip">"</nts>
                  <nts id="Seg_159" n="HIAT:ip">.</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_162" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_164" n="HIAT:w" s="T37">Dĭ</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_167" n="HIAT:w" s="T38">mĭbi</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_170" n="HIAT:w" s="T39">krupa</ts>
                  <nts id="Seg_171" n="HIAT:ip">.</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_174" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_176" n="HIAT:w" s="T40">Dĭgəttə</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_179" n="HIAT:w" s="T41">mĭnzərbi</ts>
                  <nts id="Seg_180" n="HIAT:ip">.</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_183" n="HIAT:u" s="T42">
                  <nts id="Seg_184" n="HIAT:ip">"</nts>
                  <ts e="T43" id="Seg_186" n="HIAT:w" s="T42">No</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_189" n="HIAT:w" s="T43">nada</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_192" n="HIAT:w" s="T44">kajaʔ</ts>
                  <nts id="Seg_193" n="HIAT:ip">"</nts>
                  <nts id="Seg_194" n="HIAT:ip">.</nts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_197" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_199" n="HIAT:w" s="T45">Dĭ</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_202" n="HIAT:w" s="T46">kajaʔ</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_205" n="HIAT:w" s="T47">deʔpi</ts>
                  <nts id="Seg_206" n="HIAT:ip">.</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_209" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_211" n="HIAT:w" s="T48">Dĭgəttə</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_214" n="HIAT:w" s="T49">amnobiʔi</ts>
                  <nts id="Seg_215" n="HIAT:ip">,</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_218" n="HIAT:w" s="T50">ambiʔi</ts>
                  <nts id="Seg_219" n="HIAT:ip">.</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_222" n="HIAT:u" s="T51">
                  <nts id="Seg_223" n="HIAT:ip">"</nts>
                  <ts e="T52" id="Seg_225" n="HIAT:w" s="T51">A</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_228" n="HIAT:w" s="T52">kamən</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_231" n="HIAT:w" s="T53">baltu</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_233" n="HIAT:ip">(</nts>
                  <ts e="T55" id="Seg_235" n="HIAT:w" s="T54">amnəbi-</ts>
                  <nts id="Seg_236" n="HIAT:ip">)</nts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_239" n="HIAT:w" s="T55">amnobaʔ</ts>
                  <nts id="Seg_240" n="HIAT:ip">?</nts>
                  <nts id="Seg_241" n="HIAT:ip">"</nts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_244" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_246" n="HIAT:w" s="T56">A</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_249" n="HIAT:w" s="T57">dĭ</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_252" n="HIAT:w" s="T58">măndə:</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_254" n="HIAT:ip">"</nts>
                  <ts e="T60" id="Seg_256" n="HIAT:w" s="T59">Dĭ</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_259" n="HIAT:w" s="T60">ej</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_262" n="HIAT:w" s="T61">mĭnzərbi</ts>
                  <nts id="Seg_263" n="HIAT:ip">,</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_266" n="HIAT:w" s="T62">măn</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_269" n="HIAT:w" s="T63">aʔtʼigən</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_272" n="HIAT:w" s="T64">mĭnzərlim</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_275" n="HIAT:w" s="T65">i</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_278" n="HIAT:w" s="T66">amnim</ts>
                  <nts id="Seg_279" n="HIAT:ip">"</nts>
                  <nts id="Seg_280" n="HIAT:ip">.</nts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_283" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_285" n="HIAT:w" s="T67">Dĭgəttə</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_288" n="HIAT:w" s="T68">dĭ</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_291" n="HIAT:w" s="T69">kalla</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_294" n="HIAT:w" s="T78">dʼürbi</ts>
                  <nts id="Seg_295" n="HIAT:ip">.</nts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_298" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_300" n="HIAT:w" s="T70">Baltut</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_303" n="HIAT:w" s="T71">ibi</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_306" n="HIAT:w" s="T72">i</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_309" n="HIAT:w" s="T73">kalla</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_312" n="HIAT:w" s="T79">dʼürbi</ts>
                  <nts id="Seg_313" n="HIAT:ip">.</nts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_316" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_318" n="HIAT:w" s="T74">Kabarləj</ts>
                  <nts id="Seg_319" n="HIAT:ip">.</nts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_322" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_324" n="HIAT:w" s="T75">Nüke</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_327" n="HIAT:w" s="T76">maluʔpi</ts>
                  <nts id="Seg_328" n="HIAT:ip">.</nts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T77" id="Seg_330" n="sc" s="T0">
               <ts e="T1" id="Seg_332" n="e" s="T0">Šonəbi </ts>
               <ts e="T2" id="Seg_334" n="e" s="T1">soldat </ts>
               <ts e="T3" id="Seg_336" n="e" s="T2">službagən. </ts>
               <ts e="T4" id="Seg_338" n="e" s="T3">Šobi </ts>
               <ts e="T5" id="Seg_340" n="e" s="T4">onʼiʔ </ts>
               <ts e="T6" id="Seg_342" n="e" s="T5">nükenə. </ts>
               <ts e="T7" id="Seg_344" n="e" s="T6">"Davaj </ts>
               <ts e="T8" id="Seg_346" n="e" s="T7">amorzittə". </ts>
               <ts e="T9" id="Seg_348" n="e" s="T8">Dĭ </ts>
               <ts e="T10" id="Seg_350" n="e" s="T9">măndə: </ts>
               <ts e="T11" id="Seg_352" n="e" s="T10">"Edəʔ!" </ts>
               <ts e="T12" id="Seg_354" n="e" s="T11">Dĭgəttə: </ts>
               <ts e="T13" id="Seg_356" n="e" s="T12">"Tăn </ts>
               <ts e="T14" id="Seg_358" n="e" s="T13">ĭmbi </ts>
               <ts e="T15" id="Seg_360" n="e" s="T14">ej </ts>
               <ts e="T16" id="Seg_362" n="e" s="T15">nüngiel?" </ts>
               <ts e="T17" id="Seg_364" n="e" s="T16">"No </ts>
               <ts e="T18" id="Seg_366" n="e" s="T17">gijen-nʼibudʼ </ts>
               <ts e="T19" id="Seg_368" n="e" s="T18">šaːləl". </ts>
               <ts e="T20" id="Seg_370" n="e" s="T19">Davaj </ts>
               <ts e="T21" id="Seg_372" n="e" s="T20">amzittə. </ts>
               <ts e="T22" id="Seg_374" n="e" s="T21">"Naga </ts>
               <ts e="T23" id="Seg_376" n="e" s="T22">ĭmbidə </ts>
               <ts e="T24" id="Seg_378" n="e" s="T23">amzittə". </ts>
               <ts e="T25" id="Seg_380" n="e" s="T24">"Davaj </ts>
               <ts e="T26" id="Seg_382" n="e" s="T25">baltu </ts>
               <ts e="T27" id="Seg_384" n="e" s="T26">măn </ts>
               <ts e="T28" id="Seg_386" n="e" s="T27">mĭnzərlem!" </ts>
               <ts e="T29" id="Seg_388" n="e" s="T28">Dĭ </ts>
               <ts e="T30" id="Seg_390" n="e" s="T29">mĭbi </ts>
               <ts e="T31" id="Seg_392" n="e" s="T30">baltu, </ts>
               <ts e="T32" id="Seg_394" n="e" s="T31">dĭ </ts>
               <ts e="T33" id="Seg_396" n="e" s="T32">mĭnzərbi, </ts>
               <ts e="T34" id="Seg_398" n="e" s="T33">mĭnzərbi. </ts>
               <ts e="T35" id="Seg_400" n="e" s="T34">"Nada </ts>
               <ts e="T36" id="Seg_402" n="e" s="T35">idʼiʔeje </ts>
               <ts e="T37" id="Seg_404" n="e" s="T36">krupa". </ts>
               <ts e="T38" id="Seg_406" n="e" s="T37">Dĭ </ts>
               <ts e="T39" id="Seg_408" n="e" s="T38">mĭbi </ts>
               <ts e="T40" id="Seg_410" n="e" s="T39">krupa. </ts>
               <ts e="T41" id="Seg_412" n="e" s="T40">Dĭgəttə </ts>
               <ts e="T42" id="Seg_414" n="e" s="T41">mĭnzərbi. </ts>
               <ts e="T43" id="Seg_416" n="e" s="T42">"No </ts>
               <ts e="T44" id="Seg_418" n="e" s="T43">nada </ts>
               <ts e="T45" id="Seg_420" n="e" s="T44">kajaʔ". </ts>
               <ts e="T46" id="Seg_422" n="e" s="T45">Dĭ </ts>
               <ts e="T47" id="Seg_424" n="e" s="T46">kajaʔ </ts>
               <ts e="T48" id="Seg_426" n="e" s="T47">deʔpi. </ts>
               <ts e="T49" id="Seg_428" n="e" s="T48">Dĭgəttə </ts>
               <ts e="T50" id="Seg_430" n="e" s="T49">amnobiʔi, </ts>
               <ts e="T51" id="Seg_432" n="e" s="T50">ambiʔi. </ts>
               <ts e="T52" id="Seg_434" n="e" s="T51">"A </ts>
               <ts e="T53" id="Seg_436" n="e" s="T52">kamən </ts>
               <ts e="T54" id="Seg_438" n="e" s="T53">baltu </ts>
               <ts e="T55" id="Seg_440" n="e" s="T54">(amnəbi-) </ts>
               <ts e="T56" id="Seg_442" n="e" s="T55">amnobaʔ?" </ts>
               <ts e="T57" id="Seg_444" n="e" s="T56">A </ts>
               <ts e="T58" id="Seg_446" n="e" s="T57">dĭ </ts>
               <ts e="T59" id="Seg_448" n="e" s="T58">măndə: </ts>
               <ts e="T60" id="Seg_450" n="e" s="T59">"Dĭ </ts>
               <ts e="T61" id="Seg_452" n="e" s="T60">ej </ts>
               <ts e="T62" id="Seg_454" n="e" s="T61">mĭnzərbi, </ts>
               <ts e="T63" id="Seg_456" n="e" s="T62">măn </ts>
               <ts e="T64" id="Seg_458" n="e" s="T63">aʔtʼigən </ts>
               <ts e="T65" id="Seg_460" n="e" s="T64">mĭnzərlim </ts>
               <ts e="T66" id="Seg_462" n="e" s="T65">i </ts>
               <ts e="T67" id="Seg_464" n="e" s="T66">amnim". </ts>
               <ts e="T68" id="Seg_466" n="e" s="T67">Dĭgəttə </ts>
               <ts e="T69" id="Seg_468" n="e" s="T68">dĭ </ts>
               <ts e="T78" id="Seg_470" n="e" s="T69">kalla </ts>
               <ts e="T70" id="Seg_472" n="e" s="T78">dʼürbi. </ts>
               <ts e="T71" id="Seg_474" n="e" s="T70">Baltut </ts>
               <ts e="T72" id="Seg_476" n="e" s="T71">ibi </ts>
               <ts e="T73" id="Seg_478" n="e" s="T72">i </ts>
               <ts e="T79" id="Seg_480" n="e" s="T73">kalla </ts>
               <ts e="T74" id="Seg_482" n="e" s="T79">dʼürbi. </ts>
               <ts e="T75" id="Seg_484" n="e" s="T74">Kabarləj. </ts>
               <ts e="T76" id="Seg_486" n="e" s="T75">Nüke </ts>
               <ts e="T77" id="Seg_488" n="e" s="T76">maluʔpi. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_489" s="T0">PKZ_196X_AxePorridge_flk.001 (001)</ta>
            <ta e="T6" id="Seg_490" s="T3">PKZ_196X_AxePorridge_flk.002 (002)</ta>
            <ta e="T8" id="Seg_491" s="T6">PKZ_196X_AxePorridge_flk.003 (003)</ta>
            <ta e="T11" id="Seg_492" s="T8">PKZ_196X_AxePorridge_flk.004 (004)</ta>
            <ta e="T16" id="Seg_493" s="T11">PKZ_196X_AxePorridge_flk.005 (005)</ta>
            <ta e="T19" id="Seg_494" s="T16">PKZ_196X_AxePorridge_flk.006 (006)</ta>
            <ta e="T21" id="Seg_495" s="T19">PKZ_196X_AxePorridge_flk.007 (007)</ta>
            <ta e="T24" id="Seg_496" s="T21">PKZ_196X_AxePorridge_flk.008 (008)</ta>
            <ta e="T28" id="Seg_497" s="T24">PKZ_196X_AxePorridge_flk.009 (009)</ta>
            <ta e="T34" id="Seg_498" s="T28">PKZ_196X_AxePorridge_flk.010 (010)</ta>
            <ta e="T37" id="Seg_499" s="T34">PKZ_196X_AxePorridge_flk.011 (011)</ta>
            <ta e="T40" id="Seg_500" s="T37">PKZ_196X_AxePorridge_flk.012 (012)</ta>
            <ta e="T42" id="Seg_501" s="T40">PKZ_196X_AxePorridge_flk.013 (013)</ta>
            <ta e="T45" id="Seg_502" s="T42">PKZ_196X_AxePorridge_flk.014 (014)</ta>
            <ta e="T48" id="Seg_503" s="T45">PKZ_196X_AxePorridge_flk.015 (015)</ta>
            <ta e="T51" id="Seg_504" s="T48">PKZ_196X_AxePorridge_flk.016 (016)</ta>
            <ta e="T56" id="Seg_505" s="T51">PKZ_196X_AxePorridge_flk.017 (017)</ta>
            <ta e="T67" id="Seg_506" s="T56">PKZ_196X_AxePorridge_flk.018 (018)</ta>
            <ta e="T70" id="Seg_507" s="T67">PKZ_196X_AxePorridge_flk.019 (019)</ta>
            <ta e="T74" id="Seg_508" s="T70">PKZ_196X_AxePorridge_flk.020 (020)</ta>
            <ta e="T75" id="Seg_509" s="T74">PKZ_196X_AxePorridge_flk.021 (021)</ta>
            <ta e="T77" id="Seg_510" s="T75">PKZ_196X_AxePorridge_flk.022 (022)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_511" s="T0">Šonəbi soldat službagən. </ta>
            <ta e="T6" id="Seg_512" s="T3">Šobi onʼiʔ nükenə. </ta>
            <ta e="T8" id="Seg_513" s="T6">"Davaj amorzittə". </ta>
            <ta e="T11" id="Seg_514" s="T8">Dĭ măndə: "Edəʔ!" </ta>
            <ta e="T16" id="Seg_515" s="T11">Dĭgəttə: "Tăn ĭmbi ej nüngiel?" </ta>
            <ta e="T19" id="Seg_516" s="T16">"No gijen-nʼibudʼ šaːləl". </ta>
            <ta e="T21" id="Seg_517" s="T19">Davaj amzittə. </ta>
            <ta e="T24" id="Seg_518" s="T21">"Naga ĭmbidə amzittə". </ta>
            <ta e="T28" id="Seg_519" s="T24">"Davaj baltu măn mĭnzərlem!" </ta>
            <ta e="T34" id="Seg_520" s="T28">Dĭ mĭbi baltu, dĭ mĭnzərbi, mĭnzərbi. </ta>
            <ta e="T37" id="Seg_521" s="T34">"Nada idʼiʔeje krupa". </ta>
            <ta e="T40" id="Seg_522" s="T37">Dĭ mĭbi krupa. </ta>
            <ta e="T42" id="Seg_523" s="T40">Dĭgəttə mĭnzərbi. </ta>
            <ta e="T45" id="Seg_524" s="T42">"No nada kajaʔ". </ta>
            <ta e="T48" id="Seg_525" s="T45">Dĭ kajaʔ deʔpi. </ta>
            <ta e="T51" id="Seg_526" s="T48">Dĭgəttə amnobiʔi, ambiʔi. </ta>
            <ta e="T56" id="Seg_527" s="T51">"A kamən baltu (amnəbi-) amnobaʔ?" </ta>
            <ta e="T67" id="Seg_528" s="T56">A dĭ măndə: "Dĭ ej mĭnzərbi, măn aʔtʼigən mĭnzərlim i amnim". </ta>
            <ta e="T70" id="Seg_529" s="T67">Dĭgəttə dĭ kalla dʼürbi. </ta>
            <ta e="T74" id="Seg_530" s="T70">Baltut ibi i kalla dʼürbi. </ta>
            <ta e="T75" id="Seg_531" s="T74">Kabarləj. </ta>
            <ta e="T77" id="Seg_532" s="T75">Nüke maluʔpi. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_533" s="T0">šonə-bi</ta>
            <ta e="T2" id="Seg_534" s="T1">soldat</ta>
            <ta e="T3" id="Seg_535" s="T2">služba-gən</ta>
            <ta e="T4" id="Seg_536" s="T3">šo-bi</ta>
            <ta e="T5" id="Seg_537" s="T4">onʼiʔ</ta>
            <ta e="T6" id="Seg_538" s="T5">nüke-nə</ta>
            <ta e="T7" id="Seg_539" s="T6">davaj</ta>
            <ta e="T8" id="Seg_540" s="T7">amor-zittə</ta>
            <ta e="T9" id="Seg_541" s="T8">dĭ</ta>
            <ta e="T10" id="Seg_542" s="T9">măn-də</ta>
            <ta e="T11" id="Seg_543" s="T10">edə-ʔ</ta>
            <ta e="T12" id="Seg_544" s="T11">dĭgəttə</ta>
            <ta e="T13" id="Seg_545" s="T12">tăn</ta>
            <ta e="T14" id="Seg_546" s="T13">ĭmbi</ta>
            <ta e="T15" id="Seg_547" s="T14">ej</ta>
            <ta e="T16" id="Seg_548" s="T15">nün-gie-l</ta>
            <ta e="T17" id="Seg_549" s="T16">no</ta>
            <ta e="T18" id="Seg_550" s="T17">gijen=nʼibudʼ</ta>
            <ta e="T19" id="Seg_551" s="T18">šaː-lə-l</ta>
            <ta e="T20" id="Seg_552" s="T19">davaj</ta>
            <ta e="T21" id="Seg_553" s="T20">am-zittə</ta>
            <ta e="T22" id="Seg_554" s="T21">naga</ta>
            <ta e="T23" id="Seg_555" s="T22">ĭmbi=də</ta>
            <ta e="T24" id="Seg_556" s="T23">am-zittə</ta>
            <ta e="T25" id="Seg_557" s="T24">davaj</ta>
            <ta e="T26" id="Seg_558" s="T25">baltu</ta>
            <ta e="T27" id="Seg_559" s="T26">măn</ta>
            <ta e="T28" id="Seg_560" s="T27">mĭnzər-le-m</ta>
            <ta e="T29" id="Seg_561" s="T28">dĭ</ta>
            <ta e="T30" id="Seg_562" s="T29">mĭ-bi</ta>
            <ta e="T31" id="Seg_563" s="T30">baltu</ta>
            <ta e="T32" id="Seg_564" s="T31">dĭ</ta>
            <ta e="T33" id="Seg_565" s="T32">mĭnzər-bi</ta>
            <ta e="T34" id="Seg_566" s="T33">mĭnzər-bi</ta>
            <ta e="T35" id="Seg_567" s="T34">nada</ta>
            <ta e="T36" id="Seg_568" s="T35">idʼiʔeje</ta>
            <ta e="T37" id="Seg_569" s="T36">krupa</ta>
            <ta e="T38" id="Seg_570" s="T37">dĭ</ta>
            <ta e="T39" id="Seg_571" s="T38">mĭ-bi</ta>
            <ta e="T40" id="Seg_572" s="T39">krupa</ta>
            <ta e="T41" id="Seg_573" s="T40">dĭgəttə</ta>
            <ta e="T42" id="Seg_574" s="T41">mĭnzər-bi</ta>
            <ta e="T43" id="Seg_575" s="T42">no</ta>
            <ta e="T44" id="Seg_576" s="T43">nada</ta>
            <ta e="T45" id="Seg_577" s="T44">kajaʔ</ta>
            <ta e="T46" id="Seg_578" s="T45">dĭ</ta>
            <ta e="T47" id="Seg_579" s="T46">kajaʔ</ta>
            <ta e="T48" id="Seg_580" s="T47">deʔ-pi</ta>
            <ta e="T49" id="Seg_581" s="T48">dĭgəttə</ta>
            <ta e="T50" id="Seg_582" s="T49">amno-bi-ʔi</ta>
            <ta e="T51" id="Seg_583" s="T50">am-bi-ʔi</ta>
            <ta e="T52" id="Seg_584" s="T51">a</ta>
            <ta e="T53" id="Seg_585" s="T52">kamən</ta>
            <ta e="T54" id="Seg_586" s="T53">baltu</ta>
            <ta e="T56" id="Seg_587" s="T55">am-no-baʔ</ta>
            <ta e="T57" id="Seg_588" s="T56">a</ta>
            <ta e="T58" id="Seg_589" s="T57">dĭ</ta>
            <ta e="T59" id="Seg_590" s="T58">măn-də</ta>
            <ta e="T60" id="Seg_591" s="T59">dĭ</ta>
            <ta e="T61" id="Seg_592" s="T60">ej</ta>
            <ta e="T62" id="Seg_593" s="T61">mĭnzər-bi</ta>
            <ta e="T63" id="Seg_594" s="T62">măn</ta>
            <ta e="T64" id="Seg_595" s="T63">aʔtʼi-gən</ta>
            <ta e="T65" id="Seg_596" s="T64">mĭnzər-li-m</ta>
            <ta e="T66" id="Seg_597" s="T65">i</ta>
            <ta e="T67" id="Seg_598" s="T66">am-ni-m</ta>
            <ta e="T68" id="Seg_599" s="T67">dĭgəttə</ta>
            <ta e="T69" id="Seg_600" s="T68">dĭ</ta>
            <ta e="T78" id="Seg_601" s="T69">kal-la</ta>
            <ta e="T70" id="Seg_602" s="T78">dʼür-bi</ta>
            <ta e="T71" id="Seg_603" s="T70">baltu-t</ta>
            <ta e="T72" id="Seg_604" s="T71">i-bi</ta>
            <ta e="T73" id="Seg_605" s="T72">i</ta>
            <ta e="T79" id="Seg_606" s="T73">kal-la</ta>
            <ta e="T74" id="Seg_607" s="T79">dʼür-bi</ta>
            <ta e="T75" id="Seg_608" s="T74">kabarləj</ta>
            <ta e="T76" id="Seg_609" s="T75">nüke</ta>
            <ta e="T77" id="Seg_610" s="T76">ma-luʔ-pi</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_611" s="T0">šonə-bi</ta>
            <ta e="T2" id="Seg_612" s="T1">soldat</ta>
            <ta e="T3" id="Seg_613" s="T2">služba-Kən</ta>
            <ta e="T4" id="Seg_614" s="T3">šo-bi</ta>
            <ta e="T5" id="Seg_615" s="T4">onʼiʔ</ta>
            <ta e="T6" id="Seg_616" s="T5">nüke-Tə</ta>
            <ta e="T7" id="Seg_617" s="T6">davaj</ta>
            <ta e="T8" id="Seg_618" s="T7">amor-zittə</ta>
            <ta e="T9" id="Seg_619" s="T8">dĭ</ta>
            <ta e="T10" id="Seg_620" s="T9">măn-ntə</ta>
            <ta e="T11" id="Seg_621" s="T10">edə-ʔ</ta>
            <ta e="T12" id="Seg_622" s="T11">dĭgəttə</ta>
            <ta e="T13" id="Seg_623" s="T12">tăn</ta>
            <ta e="T14" id="Seg_624" s="T13">ĭmbi</ta>
            <ta e="T15" id="Seg_625" s="T14">ej</ta>
            <ta e="T16" id="Seg_626" s="T15">nünə-gA-l</ta>
            <ta e="T17" id="Seg_627" s="T16">no</ta>
            <ta e="T18" id="Seg_628" s="T17">gijen=nʼibudʼ</ta>
            <ta e="T19" id="Seg_629" s="T18">šaː-lV-l</ta>
            <ta e="T20" id="Seg_630" s="T19">davaj</ta>
            <ta e="T21" id="Seg_631" s="T20">am-zittə</ta>
            <ta e="T22" id="Seg_632" s="T21">naga</ta>
            <ta e="T23" id="Seg_633" s="T22">ĭmbi=də</ta>
            <ta e="T24" id="Seg_634" s="T23">am-zittə</ta>
            <ta e="T25" id="Seg_635" s="T24">davaj</ta>
            <ta e="T26" id="Seg_636" s="T25">baltu</ta>
            <ta e="T27" id="Seg_637" s="T26">măn</ta>
            <ta e="T28" id="Seg_638" s="T27">mĭnzər-lV-m</ta>
            <ta e="T29" id="Seg_639" s="T28">dĭ</ta>
            <ta e="T30" id="Seg_640" s="T29">mĭ-bi</ta>
            <ta e="T31" id="Seg_641" s="T30">baltu</ta>
            <ta e="T32" id="Seg_642" s="T31">dĭ</ta>
            <ta e="T33" id="Seg_643" s="T32">mĭnzər-bi</ta>
            <ta e="T34" id="Seg_644" s="T33">mĭnzər-bi</ta>
            <ta e="T35" id="Seg_645" s="T34">nadə</ta>
            <ta e="T36" id="Seg_646" s="T35">idʼiʔeʔe</ta>
            <ta e="T37" id="Seg_647" s="T36">krupa</ta>
            <ta e="T38" id="Seg_648" s="T37">dĭ</ta>
            <ta e="T39" id="Seg_649" s="T38">mĭ-bi</ta>
            <ta e="T40" id="Seg_650" s="T39">krupa</ta>
            <ta e="T41" id="Seg_651" s="T40">dĭgəttə</ta>
            <ta e="T42" id="Seg_652" s="T41">mĭnzər-bi</ta>
            <ta e="T43" id="Seg_653" s="T42">no</ta>
            <ta e="T44" id="Seg_654" s="T43">nadə</ta>
            <ta e="T45" id="Seg_655" s="T44">kajaʔ</ta>
            <ta e="T46" id="Seg_656" s="T45">dĭ</ta>
            <ta e="T47" id="Seg_657" s="T46">kajaʔ</ta>
            <ta e="T48" id="Seg_658" s="T47">det-bi</ta>
            <ta e="T49" id="Seg_659" s="T48">dĭgəttə</ta>
            <ta e="T50" id="Seg_660" s="T49">amnə-bi-jəʔ</ta>
            <ta e="T51" id="Seg_661" s="T50">am-bi-jəʔ</ta>
            <ta e="T52" id="Seg_662" s="T51">a</ta>
            <ta e="T53" id="Seg_663" s="T52">kamən</ta>
            <ta e="T54" id="Seg_664" s="T53">baltu</ta>
            <ta e="T56" id="Seg_665" s="T55">am-lV-bAʔ</ta>
            <ta e="T57" id="Seg_666" s="T56">a</ta>
            <ta e="T58" id="Seg_667" s="T57">dĭ</ta>
            <ta e="T59" id="Seg_668" s="T58">măn-ntə</ta>
            <ta e="T60" id="Seg_669" s="T59">dĭ</ta>
            <ta e="T61" id="Seg_670" s="T60">ej</ta>
            <ta e="T62" id="Seg_671" s="T61">mĭnzər-bi</ta>
            <ta e="T63" id="Seg_672" s="T62">măn</ta>
            <ta e="T64" id="Seg_673" s="T63">aʔtʼi-Kən</ta>
            <ta e="T65" id="Seg_674" s="T64">mĭnzər-lV-m</ta>
            <ta e="T66" id="Seg_675" s="T65">i</ta>
            <ta e="T67" id="Seg_676" s="T66">am-lV-m</ta>
            <ta e="T68" id="Seg_677" s="T67">dĭgəttə</ta>
            <ta e="T69" id="Seg_678" s="T68">dĭ</ta>
            <ta e="T78" id="Seg_679" s="T69">kan-lAʔ</ta>
            <ta e="T70" id="Seg_680" s="T78">tʼür-bi</ta>
            <ta e="T71" id="Seg_681" s="T70">baltu-t</ta>
            <ta e="T72" id="Seg_682" s="T71">i-bi</ta>
            <ta e="T73" id="Seg_683" s="T72">i</ta>
            <ta e="T79" id="Seg_684" s="T73">kan-lAʔ</ta>
            <ta e="T74" id="Seg_685" s="T79">tʼür-bi</ta>
            <ta e="T75" id="Seg_686" s="T74">kabarləj</ta>
            <ta e="T76" id="Seg_687" s="T75">nüke</ta>
            <ta e="T77" id="Seg_688" s="T76">ma-luʔbdə-bi</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_689" s="T0">come-PST.[3SG]</ta>
            <ta e="T2" id="Seg_690" s="T1">soldier.[NOM.SG]</ta>
            <ta e="T3" id="Seg_691" s="T2">service-LOC</ta>
            <ta e="T4" id="Seg_692" s="T3">come-PST.[3SG]</ta>
            <ta e="T5" id="Seg_693" s="T4">one.[NOM.SG]</ta>
            <ta e="T6" id="Seg_694" s="T5">woman-LAT</ta>
            <ta e="T7" id="Seg_695" s="T6">HORT</ta>
            <ta e="T8" id="Seg_696" s="T7">eat-INF.LAT</ta>
            <ta e="T9" id="Seg_697" s="T8">this.[NOM.SG]</ta>
            <ta e="T10" id="Seg_698" s="T9">say-IPFVZ.[3SG]</ta>
            <ta e="T11" id="Seg_699" s="T10">hang.up-IMP.2SG</ta>
            <ta e="T12" id="Seg_700" s="T11">then</ta>
            <ta e="T13" id="Seg_701" s="T12">you.NOM</ta>
            <ta e="T14" id="Seg_702" s="T13">what.[NOM.SG]</ta>
            <ta e="T15" id="Seg_703" s="T14">NEG</ta>
            <ta e="T16" id="Seg_704" s="T15">hear-PRS-2SG</ta>
            <ta e="T17" id="Seg_705" s="T16">well</ta>
            <ta e="T18" id="Seg_706" s="T17">where=INDEF</ta>
            <ta e="T19" id="Seg_707" s="T18">spend.night-FUT-2SG</ta>
            <ta e="T20" id="Seg_708" s="T19">HORT</ta>
            <ta e="T21" id="Seg_709" s="T20">eat-INF.LAT</ta>
            <ta e="T22" id="Seg_710" s="T21">NEG.EX.[3SG]</ta>
            <ta e="T23" id="Seg_711" s="T22">what.[NOM.SG]=INDEF</ta>
            <ta e="T24" id="Seg_712" s="T23">eat-INF.LAT</ta>
            <ta e="T25" id="Seg_713" s="T24">HORT</ta>
            <ta e="T26" id="Seg_714" s="T25">axe.[NOM.SG]</ta>
            <ta e="T27" id="Seg_715" s="T26">I.NOM</ta>
            <ta e="T28" id="Seg_716" s="T27">boil-FUT-1SG</ta>
            <ta e="T29" id="Seg_717" s="T28">this.[NOM.SG]</ta>
            <ta e="T30" id="Seg_718" s="T29">give-PST.[3SG]</ta>
            <ta e="T31" id="Seg_719" s="T30">axe.[NOM.SG]</ta>
            <ta e="T32" id="Seg_720" s="T31">this.[NOM.SG]</ta>
            <ta e="T33" id="Seg_721" s="T32">boil-PST.[3SG]</ta>
            <ta e="T34" id="Seg_722" s="T33">boil-PST.[3SG]</ta>
            <ta e="T35" id="Seg_723" s="T34">one.should</ta>
            <ta e="T36" id="Seg_724" s="T35">a.few</ta>
            <ta e="T37" id="Seg_725" s="T36">groats.[NOM.SG]</ta>
            <ta e="T38" id="Seg_726" s="T37">this.[NOM.SG]</ta>
            <ta e="T39" id="Seg_727" s="T38">give-PST.[3SG]</ta>
            <ta e="T40" id="Seg_728" s="T39">groats.[NOM.SG]</ta>
            <ta e="T41" id="Seg_729" s="T40">then</ta>
            <ta e="T42" id="Seg_730" s="T41">boil-PST.[3SG]</ta>
            <ta e="T43" id="Seg_731" s="T42">well</ta>
            <ta e="T44" id="Seg_732" s="T43">one.should</ta>
            <ta e="T45" id="Seg_733" s="T44">butter.[NOM.SG]</ta>
            <ta e="T46" id="Seg_734" s="T45">this.[NOM.SG]</ta>
            <ta e="T47" id="Seg_735" s="T46">butter.[NOM.SG]</ta>
            <ta e="T48" id="Seg_736" s="T47">bring-PST.[3SG]</ta>
            <ta e="T49" id="Seg_737" s="T48">then</ta>
            <ta e="T50" id="Seg_738" s="T49">sit.down-PST-3PL</ta>
            <ta e="T51" id="Seg_739" s="T50">eat-PST-3PL</ta>
            <ta e="T52" id="Seg_740" s="T51">and</ta>
            <ta e="T53" id="Seg_741" s="T52">when</ta>
            <ta e="T54" id="Seg_742" s="T53">axe.[NOM.SG]</ta>
            <ta e="T56" id="Seg_743" s="T55">eat-FUT-1PL</ta>
            <ta e="T57" id="Seg_744" s="T56">and</ta>
            <ta e="T58" id="Seg_745" s="T57">this.[NOM.SG]</ta>
            <ta e="T59" id="Seg_746" s="T58">say-IPFVZ.[3SG]</ta>
            <ta e="T60" id="Seg_747" s="T59">this.[3SG]</ta>
            <ta e="T61" id="Seg_748" s="T60">NEG</ta>
            <ta e="T62" id="Seg_749" s="T61">boil-PST.[3SG]</ta>
            <ta e="T63" id="Seg_750" s="T62">I.NOM</ta>
            <ta e="T64" id="Seg_751" s="T63">road-LOC</ta>
            <ta e="T65" id="Seg_752" s="T64">boil-FUT-1SG</ta>
            <ta e="T66" id="Seg_753" s="T65">and</ta>
            <ta e="T67" id="Seg_754" s="T66">eat-FUT-1SG</ta>
            <ta e="T68" id="Seg_755" s="T67">then</ta>
            <ta e="T69" id="Seg_756" s="T68">this.[NOM.SG]</ta>
            <ta e="T78" id="Seg_757" s="T69">go-CVB</ta>
            <ta e="T70" id="Seg_758" s="T78">disappear-PST.[3SG]</ta>
            <ta e="T71" id="Seg_759" s="T70">axe-NOM/GEN.3SG</ta>
            <ta e="T72" id="Seg_760" s="T71">take-PST.[3SG]</ta>
            <ta e="T73" id="Seg_761" s="T72">and</ta>
            <ta e="T79" id="Seg_762" s="T73">go-CVB</ta>
            <ta e="T74" id="Seg_763" s="T79">disappear-PST.[3SG]</ta>
            <ta e="T75" id="Seg_764" s="T74">enough</ta>
            <ta e="T76" id="Seg_765" s="T75">woman.[NOM.SG]</ta>
            <ta e="T77" id="Seg_766" s="T76">remain-MOM-PST.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_767" s="T0">прийти-PST.[3SG]</ta>
            <ta e="T2" id="Seg_768" s="T1">солдат.[NOM.SG]</ta>
            <ta e="T3" id="Seg_769" s="T2">служба-LOC</ta>
            <ta e="T4" id="Seg_770" s="T3">прийти-PST.[3SG]</ta>
            <ta e="T5" id="Seg_771" s="T4">один.[NOM.SG]</ta>
            <ta e="T6" id="Seg_772" s="T5">женщина-LAT</ta>
            <ta e="T7" id="Seg_773" s="T6">HORT</ta>
            <ta e="T8" id="Seg_774" s="T7">есть-INF.LAT</ta>
            <ta e="T9" id="Seg_775" s="T8">этот.[NOM.SG]</ta>
            <ta e="T10" id="Seg_776" s="T9">сказать-IPFVZ.[3SG]</ta>
            <ta e="T11" id="Seg_777" s="T10">вешать-IMP.2SG</ta>
            <ta e="T12" id="Seg_778" s="T11">тогда</ta>
            <ta e="T13" id="Seg_779" s="T12">ты.NOM</ta>
            <ta e="T14" id="Seg_780" s="T13">что.[NOM.SG]</ta>
            <ta e="T15" id="Seg_781" s="T14">NEG</ta>
            <ta e="T16" id="Seg_782" s="T15">слышать-PRS-2SG</ta>
            <ta e="T17" id="Seg_783" s="T16">ну</ta>
            <ta e="T18" id="Seg_784" s="T17">где=INDEF</ta>
            <ta e="T19" id="Seg_785" s="T18">ночевать-FUT-2SG</ta>
            <ta e="T20" id="Seg_786" s="T19">HORT</ta>
            <ta e="T21" id="Seg_787" s="T20">съесть-INF.LAT</ta>
            <ta e="T22" id="Seg_788" s="T21">NEG.EX.[3SG]</ta>
            <ta e="T23" id="Seg_789" s="T22">что.[NOM.SG]=INDEF</ta>
            <ta e="T24" id="Seg_790" s="T23">съесть-INF.LAT</ta>
            <ta e="T25" id="Seg_791" s="T24">HORT</ta>
            <ta e="T26" id="Seg_792" s="T25">топор.[NOM.SG]</ta>
            <ta e="T27" id="Seg_793" s="T26">я.NOM</ta>
            <ta e="T28" id="Seg_794" s="T27">кипятить-FUT-1SG</ta>
            <ta e="T29" id="Seg_795" s="T28">этот.[NOM.SG]</ta>
            <ta e="T30" id="Seg_796" s="T29">дать-PST.[3SG]</ta>
            <ta e="T31" id="Seg_797" s="T30">топор.[NOM.SG]</ta>
            <ta e="T32" id="Seg_798" s="T31">этот.[NOM.SG]</ta>
            <ta e="T33" id="Seg_799" s="T32">кипятить-PST.[3SG]</ta>
            <ta e="T34" id="Seg_800" s="T33">кипятить-PST.[3SG]</ta>
            <ta e="T35" id="Seg_801" s="T34">надо</ta>
            <ta e="T36" id="Seg_802" s="T35">немного</ta>
            <ta e="T37" id="Seg_803" s="T36">крупа.[NOM.SG]</ta>
            <ta e="T38" id="Seg_804" s="T37">этот.[NOM.SG]</ta>
            <ta e="T39" id="Seg_805" s="T38">дать-PST.[3SG]</ta>
            <ta e="T40" id="Seg_806" s="T39">крупа.[NOM.SG]</ta>
            <ta e="T41" id="Seg_807" s="T40">тогда</ta>
            <ta e="T42" id="Seg_808" s="T41">кипятить-PST.[3SG]</ta>
            <ta e="T43" id="Seg_809" s="T42">ну</ta>
            <ta e="T44" id="Seg_810" s="T43">надо</ta>
            <ta e="T45" id="Seg_811" s="T44">масло.[NOM.SG]</ta>
            <ta e="T46" id="Seg_812" s="T45">этот.[NOM.SG]</ta>
            <ta e="T47" id="Seg_813" s="T46">масло.[NOM.SG]</ta>
            <ta e="T48" id="Seg_814" s="T47">принести-PST.[3SG]</ta>
            <ta e="T49" id="Seg_815" s="T48">тогда</ta>
            <ta e="T50" id="Seg_816" s="T49">сесть-PST-3PL</ta>
            <ta e="T51" id="Seg_817" s="T50">съесть-PST-3PL</ta>
            <ta e="T52" id="Seg_818" s="T51">а</ta>
            <ta e="T53" id="Seg_819" s="T52">когда</ta>
            <ta e="T54" id="Seg_820" s="T53">топор.[NOM.SG]</ta>
            <ta e="T56" id="Seg_821" s="T55">съесть-FUT-1PL</ta>
            <ta e="T57" id="Seg_822" s="T56">а</ta>
            <ta e="T58" id="Seg_823" s="T57">этот.[NOM.SG]</ta>
            <ta e="T59" id="Seg_824" s="T58">сказать-IPFVZ.[3SG]</ta>
            <ta e="T60" id="Seg_825" s="T59">этот.[3SG]</ta>
            <ta e="T61" id="Seg_826" s="T60">NEG</ta>
            <ta e="T62" id="Seg_827" s="T61">кипятить-PST.[3SG]</ta>
            <ta e="T63" id="Seg_828" s="T62">я.NOM</ta>
            <ta e="T64" id="Seg_829" s="T63">дорога-LOC</ta>
            <ta e="T65" id="Seg_830" s="T64">кипятить-FUT-1SG</ta>
            <ta e="T66" id="Seg_831" s="T65">и</ta>
            <ta e="T67" id="Seg_832" s="T66">съесть-FUT-1SG</ta>
            <ta e="T68" id="Seg_833" s="T67">тогда</ta>
            <ta e="T69" id="Seg_834" s="T68">этот.[NOM.SG]</ta>
            <ta e="T78" id="Seg_835" s="T69">пойти-CVB</ta>
            <ta e="T70" id="Seg_836" s="T78">исчезнуть-PST.[3SG]</ta>
            <ta e="T71" id="Seg_837" s="T70">топор-NOM/GEN.3SG</ta>
            <ta e="T72" id="Seg_838" s="T71">взять-PST.[3SG]</ta>
            <ta e="T73" id="Seg_839" s="T72">и</ta>
            <ta e="T79" id="Seg_840" s="T73">пойти-CVB</ta>
            <ta e="T74" id="Seg_841" s="T79">исчезнуть-PST.[3SG]</ta>
            <ta e="T75" id="Seg_842" s="T74">хватит</ta>
            <ta e="T76" id="Seg_843" s="T75">женщина.[NOM.SG]</ta>
            <ta e="T77" id="Seg_844" s="T76">остаться-MOM-PST.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_845" s="T0">v-v:tense-v:pn</ta>
            <ta e="T2" id="Seg_846" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_847" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_848" s="T3">v-v:tense-v:pn</ta>
            <ta e="T5" id="Seg_849" s="T4">num-n:case</ta>
            <ta e="T6" id="Seg_850" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_851" s="T6">ptcl</ta>
            <ta e="T8" id="Seg_852" s="T7">v-v:n.fin</ta>
            <ta e="T9" id="Seg_853" s="T8">dempro-n:case</ta>
            <ta e="T10" id="Seg_854" s="T9">v-v&gt;v-v:pn</ta>
            <ta e="T11" id="Seg_855" s="T10">v-v:mood.pn</ta>
            <ta e="T12" id="Seg_856" s="T11">adv</ta>
            <ta e="T13" id="Seg_857" s="T12">pers</ta>
            <ta e="T14" id="Seg_858" s="T13">que-n:case</ta>
            <ta e="T15" id="Seg_859" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_860" s="T15">v-v:tense-v:pn</ta>
            <ta e="T17" id="Seg_861" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_862" s="T17">que=ptcl</ta>
            <ta e="T19" id="Seg_863" s="T18">v-v:tense-v:pn</ta>
            <ta e="T20" id="Seg_864" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_865" s="T20">v-v:n.fin</ta>
            <ta e="T22" id="Seg_866" s="T21">v-v:pn</ta>
            <ta e="T23" id="Seg_867" s="T22">que-n:case=ptcl</ta>
            <ta e="T24" id="Seg_868" s="T23">v-v:n.fin</ta>
            <ta e="T25" id="Seg_869" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_870" s="T25">n-n:case</ta>
            <ta e="T27" id="Seg_871" s="T26">pers</ta>
            <ta e="T28" id="Seg_872" s="T27">v-v:tense-v:pn</ta>
            <ta e="T29" id="Seg_873" s="T28">dempro-n:case</ta>
            <ta e="T30" id="Seg_874" s="T29">v-v:tense-v:pn</ta>
            <ta e="T31" id="Seg_875" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_876" s="T31">dempro-n:case</ta>
            <ta e="T33" id="Seg_877" s="T32">v-v:tense-v:pn</ta>
            <ta e="T34" id="Seg_878" s="T33">v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_879" s="T34">ptcl</ta>
            <ta e="T36" id="Seg_880" s="T35">adv</ta>
            <ta e="T37" id="Seg_881" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_882" s="T37">dempro-n:case</ta>
            <ta e="T39" id="Seg_883" s="T38">v-v:tense-v:pn</ta>
            <ta e="T40" id="Seg_884" s="T39">n-n:case</ta>
            <ta e="T41" id="Seg_885" s="T40">adv</ta>
            <ta e="T42" id="Seg_886" s="T41">v-v:tense-v:pn</ta>
            <ta e="T43" id="Seg_887" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_888" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_889" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_890" s="T45">dempro-n:case</ta>
            <ta e="T47" id="Seg_891" s="T46">n-n:case</ta>
            <ta e="T48" id="Seg_892" s="T47">v-v:tense-v:pn</ta>
            <ta e="T49" id="Seg_893" s="T48">adv</ta>
            <ta e="T50" id="Seg_894" s="T49">v-v:tense-v:pn</ta>
            <ta e="T51" id="Seg_895" s="T50">v-v:tense-v:pn</ta>
            <ta e="T52" id="Seg_896" s="T51">conj</ta>
            <ta e="T53" id="Seg_897" s="T52">que</ta>
            <ta e="T54" id="Seg_898" s="T53">n-n:case</ta>
            <ta e="T56" id="Seg_899" s="T55">v-v:tense-v:pn</ta>
            <ta e="T57" id="Seg_900" s="T56">conj</ta>
            <ta e="T58" id="Seg_901" s="T57">dempro-n:case</ta>
            <ta e="T59" id="Seg_902" s="T58">v-v&gt;v-v:pn</ta>
            <ta e="T60" id="Seg_903" s="T59">dempro-v:pn</ta>
            <ta e="T61" id="Seg_904" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_905" s="T61">v-v:tense-v:pn</ta>
            <ta e="T63" id="Seg_906" s="T62">pers</ta>
            <ta e="T64" id="Seg_907" s="T63">n-n:case</ta>
            <ta e="T65" id="Seg_908" s="T64">v-v:tense-v:pn</ta>
            <ta e="T66" id="Seg_909" s="T65">conj</ta>
            <ta e="T67" id="Seg_910" s="T66">v-v:tense-v:pn</ta>
            <ta e="T68" id="Seg_911" s="T67">adv</ta>
            <ta e="T69" id="Seg_912" s="T68">dempro-n:case</ta>
            <ta e="T78" id="Seg_913" s="T69">v-v:n-fin</ta>
            <ta e="T70" id="Seg_914" s="T78">v-v:tense-v:pn</ta>
            <ta e="T71" id="Seg_915" s="T70">n-n:case.poss</ta>
            <ta e="T72" id="Seg_916" s="T71">v-v:tense-v:pn</ta>
            <ta e="T73" id="Seg_917" s="T72">conj</ta>
            <ta e="T79" id="Seg_918" s="T73">v-v:n-fin</ta>
            <ta e="T74" id="Seg_919" s="T79">v-v:tense-v:pn</ta>
            <ta e="T75" id="Seg_920" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_921" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_922" s="T76">v-v&gt;v-v:tense-v:pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_923" s="T0">v</ta>
            <ta e="T2" id="Seg_924" s="T1">n</ta>
            <ta e="T3" id="Seg_925" s="T2">n</ta>
            <ta e="T4" id="Seg_926" s="T3">v</ta>
            <ta e="T5" id="Seg_927" s="T4">num</ta>
            <ta e="T6" id="Seg_928" s="T5">n</ta>
            <ta e="T7" id="Seg_929" s="T6">ptcl</ta>
            <ta e="T8" id="Seg_930" s="T7">v</ta>
            <ta e="T9" id="Seg_931" s="T8">dempro</ta>
            <ta e="T10" id="Seg_932" s="T9">v</ta>
            <ta e="T11" id="Seg_933" s="T10">v</ta>
            <ta e="T12" id="Seg_934" s="T11">adv</ta>
            <ta e="T13" id="Seg_935" s="T12">pers</ta>
            <ta e="T14" id="Seg_936" s="T13">que</ta>
            <ta e="T15" id="Seg_937" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_938" s="T15">v</ta>
            <ta e="T17" id="Seg_939" s="T16">ptcl</ta>
            <ta e="T18" id="Seg_940" s="T17">que</ta>
            <ta e="T19" id="Seg_941" s="T18">v</ta>
            <ta e="T20" id="Seg_942" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_943" s="T20">v</ta>
            <ta e="T22" id="Seg_944" s="T21">v</ta>
            <ta e="T23" id="Seg_945" s="T22">que</ta>
            <ta e="T24" id="Seg_946" s="T23">v</ta>
            <ta e="T25" id="Seg_947" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_948" s="T25">n</ta>
            <ta e="T27" id="Seg_949" s="T26">pers</ta>
            <ta e="T28" id="Seg_950" s="T27">v</ta>
            <ta e="T29" id="Seg_951" s="T28">dempro</ta>
            <ta e="T30" id="Seg_952" s="T29">v</ta>
            <ta e="T31" id="Seg_953" s="T30">n</ta>
            <ta e="T32" id="Seg_954" s="T31">dempro</ta>
            <ta e="T33" id="Seg_955" s="T32">v</ta>
            <ta e="T34" id="Seg_956" s="T33">v</ta>
            <ta e="T35" id="Seg_957" s="T34">ptcl</ta>
            <ta e="T36" id="Seg_958" s="T35">adv</ta>
            <ta e="T37" id="Seg_959" s="T36">n</ta>
            <ta e="T38" id="Seg_960" s="T37">dempro</ta>
            <ta e="T39" id="Seg_961" s="T38">v</ta>
            <ta e="T40" id="Seg_962" s="T39">n</ta>
            <ta e="T41" id="Seg_963" s="T40">adv</ta>
            <ta e="T42" id="Seg_964" s="T41">v</ta>
            <ta e="T43" id="Seg_965" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_966" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_967" s="T44">n</ta>
            <ta e="T46" id="Seg_968" s="T45">dempro</ta>
            <ta e="T47" id="Seg_969" s="T46">n</ta>
            <ta e="T48" id="Seg_970" s="T47">v</ta>
            <ta e="T49" id="Seg_971" s="T48">adv</ta>
            <ta e="T50" id="Seg_972" s="T49">v</ta>
            <ta e="T51" id="Seg_973" s="T50">v</ta>
            <ta e="T52" id="Seg_974" s="T51">conj</ta>
            <ta e="T53" id="Seg_975" s="T52">conj</ta>
            <ta e="T54" id="Seg_976" s="T53">n</ta>
            <ta e="T56" id="Seg_977" s="T55">v</ta>
            <ta e="T57" id="Seg_978" s="T56">conj</ta>
            <ta e="T58" id="Seg_979" s="T57">dempro</ta>
            <ta e="T59" id="Seg_980" s="T58">v</ta>
            <ta e="T60" id="Seg_981" s="T59">dempro</ta>
            <ta e="T61" id="Seg_982" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_983" s="T61">v</ta>
            <ta e="T63" id="Seg_984" s="T62">pers</ta>
            <ta e="T64" id="Seg_985" s="T63">n</ta>
            <ta e="T65" id="Seg_986" s="T64">v</ta>
            <ta e="T66" id="Seg_987" s="T65">conj</ta>
            <ta e="T67" id="Seg_988" s="T66">v</ta>
            <ta e="T68" id="Seg_989" s="T67">adv</ta>
            <ta e="T69" id="Seg_990" s="T68">dempro</ta>
            <ta e="T78" id="Seg_991" s="T69">v</ta>
            <ta e="T70" id="Seg_992" s="T78">v</ta>
            <ta e="T71" id="Seg_993" s="T70">n</ta>
            <ta e="T72" id="Seg_994" s="T71">v</ta>
            <ta e="T73" id="Seg_995" s="T72">conj</ta>
            <ta e="T74" id="Seg_996" s="T73">vv</ta>
            <ta e="T75" id="Seg_997" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_998" s="T75">n</ta>
            <ta e="T77" id="Seg_999" s="T76">v</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_1000" s="T0">v:pred</ta>
            <ta e="T2" id="Seg_1001" s="T1">np.h:S</ta>
            <ta e="T4" id="Seg_1002" s="T3">0.3.h:S v:pred</ta>
            <ta e="T7" id="Seg_1003" s="T6">v:pred</ta>
            <ta e="T9" id="Seg_1004" s="T8">pro.h:S</ta>
            <ta e="T10" id="Seg_1005" s="T9">v:pred</ta>
            <ta e="T11" id="Seg_1006" s="T10">0.2.h:S v:pred</ta>
            <ta e="T13" id="Seg_1007" s="T12">pro.h:S</ta>
            <ta e="T14" id="Seg_1008" s="T13">pro:pred</ta>
            <ta e="T15" id="Seg_1009" s="T14">ptcl.neg</ta>
            <ta e="T16" id="Seg_1010" s="T15">0.2.h:S v:pred</ta>
            <ta e="T19" id="Seg_1011" s="T18">0.2.h:S v:pred</ta>
            <ta e="T20" id="Seg_1012" s="T19">v:pred</ta>
            <ta e="T22" id="Seg_1013" s="T21">v:pred</ta>
            <ta e="T25" id="Seg_1014" s="T24">ptcl:pred</ta>
            <ta e="T26" id="Seg_1015" s="T25">np:O</ta>
            <ta e="T29" id="Seg_1016" s="T28">pro.h:S</ta>
            <ta e="T30" id="Seg_1017" s="T29">v:pred</ta>
            <ta e="T31" id="Seg_1018" s="T30">np:O</ta>
            <ta e="T32" id="Seg_1019" s="T31">pro.h:S</ta>
            <ta e="T33" id="Seg_1020" s="T32">v:pred</ta>
            <ta e="T34" id="Seg_1021" s="T33">0.3.h:S v:pred</ta>
            <ta e="T35" id="Seg_1022" s="T34">ptcl:pred</ta>
            <ta e="T37" id="Seg_1023" s="T36">np:S</ta>
            <ta e="T38" id="Seg_1024" s="T37">pro.h:S</ta>
            <ta e="T39" id="Seg_1025" s="T38">v:pred</ta>
            <ta e="T40" id="Seg_1026" s="T39">np:O</ta>
            <ta e="T42" id="Seg_1027" s="T41">0.3.h:S v:pred</ta>
            <ta e="T44" id="Seg_1028" s="T43">ptcl:pred</ta>
            <ta e="T46" id="Seg_1029" s="T45">pro.h:S</ta>
            <ta e="T47" id="Seg_1030" s="T46">np:O</ta>
            <ta e="T48" id="Seg_1031" s="T47">v:pred</ta>
            <ta e="T50" id="Seg_1032" s="T49">0.3.h:S v:pred</ta>
            <ta e="T51" id="Seg_1033" s="T50">0.3.h:S v:pred</ta>
            <ta e="T54" id="Seg_1034" s="T53">np:O</ta>
            <ta e="T56" id="Seg_1035" s="T55">0.1.h:S v:pred</ta>
            <ta e="T58" id="Seg_1036" s="T57">pro.h:S</ta>
            <ta e="T59" id="Seg_1037" s="T58">v:pred</ta>
            <ta e="T60" id="Seg_1038" s="T59">pro.h:S</ta>
            <ta e="T61" id="Seg_1039" s="T60">ptcl.neg</ta>
            <ta e="T62" id="Seg_1040" s="T61">v:pred</ta>
            <ta e="T63" id="Seg_1041" s="T62">pro.h:S</ta>
            <ta e="T65" id="Seg_1042" s="T64">v:pred</ta>
            <ta e="T67" id="Seg_1043" s="T66">0.1.h:S v:pred</ta>
            <ta e="T69" id="Seg_1044" s="T68">pro.h:S</ta>
            <ta e="T78" id="Seg_1045" s="T69">conv:pred</ta>
            <ta e="T70" id="Seg_1046" s="T78">v:pred</ta>
            <ta e="T71" id="Seg_1047" s="T70">np:O</ta>
            <ta e="T72" id="Seg_1048" s="T71">0.3.h:S v:pred</ta>
            <ta e="T79" id="Seg_1049" s="T73">conv:pred</ta>
            <ta e="T74" id="Seg_1050" s="T79">0.3.h:S v:pred</ta>
            <ta e="T76" id="Seg_1051" s="T75">np.h:S</ta>
            <ta e="T77" id="Seg_1052" s="T76">v:pred</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_1053" s="T1">np.h:A</ta>
            <ta e="T3" id="Seg_1054" s="T2">np:So</ta>
            <ta e="T4" id="Seg_1055" s="T3">0.3.h:A</ta>
            <ta e="T6" id="Seg_1056" s="T5">np:G</ta>
            <ta e="T9" id="Seg_1057" s="T8">pro.h:A</ta>
            <ta e="T11" id="Seg_1058" s="T10">0.2.h:A</ta>
            <ta e="T12" id="Seg_1059" s="T11">adv:Time</ta>
            <ta e="T13" id="Seg_1060" s="T12">pro.h:E</ta>
            <ta e="T14" id="Seg_1061" s="T13">pro:Th</ta>
            <ta e="T16" id="Seg_1062" s="T15">0.2.h:E</ta>
            <ta e="T18" id="Seg_1063" s="T17">pro:L</ta>
            <ta e="T19" id="Seg_1064" s="T18">0.2.h:A</ta>
            <ta e="T23" id="Seg_1065" s="T22">pro:Th</ta>
            <ta e="T26" id="Seg_1066" s="T25">np:P</ta>
            <ta e="T27" id="Seg_1067" s="T26">pro.h:A</ta>
            <ta e="T29" id="Seg_1068" s="T28">pro.h:A</ta>
            <ta e="T31" id="Seg_1069" s="T30">np:Th</ta>
            <ta e="T32" id="Seg_1070" s="T31">pro.h:A</ta>
            <ta e="T34" id="Seg_1071" s="T33">0.3.h:A</ta>
            <ta e="T37" id="Seg_1072" s="T36">np:Th</ta>
            <ta e="T38" id="Seg_1073" s="T37">pro.h:A</ta>
            <ta e="T40" id="Seg_1074" s="T39">np:Th</ta>
            <ta e="T41" id="Seg_1075" s="T40">adv:Time</ta>
            <ta e="T42" id="Seg_1076" s="T41">0.3.h:A</ta>
            <ta e="T45" id="Seg_1077" s="T44">np:Th</ta>
            <ta e="T46" id="Seg_1078" s="T45">pro.h:A</ta>
            <ta e="T47" id="Seg_1079" s="T46">np:Th</ta>
            <ta e="T49" id="Seg_1080" s="T48">adv:Time</ta>
            <ta e="T50" id="Seg_1081" s="T49">0.3.h:A</ta>
            <ta e="T51" id="Seg_1082" s="T50">0.3.h:A</ta>
            <ta e="T54" id="Seg_1083" s="T53">np:P</ta>
            <ta e="T56" id="Seg_1084" s="T55">0.1.h:A</ta>
            <ta e="T58" id="Seg_1085" s="T57">pro.h:A</ta>
            <ta e="T60" id="Seg_1086" s="T59">pro.h:A</ta>
            <ta e="T63" id="Seg_1087" s="T62">pro.h:A</ta>
            <ta e="T64" id="Seg_1088" s="T63">np:L</ta>
            <ta e="T67" id="Seg_1089" s="T66">0.1.h:A</ta>
            <ta e="T68" id="Seg_1090" s="T67">adv:Time</ta>
            <ta e="T69" id="Seg_1091" s="T68">pro.h:A</ta>
            <ta e="T71" id="Seg_1092" s="T70">np:Th</ta>
            <ta e="T72" id="Seg_1093" s="T71">0.3.h:A</ta>
            <ta e="T74" id="Seg_1094" s="T79">0.3.h:A</ta>
            <ta e="T76" id="Seg_1095" s="T75">np.h:A</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T2" id="Seg_1096" s="T1">RUS:cult</ta>
            <ta e="T3" id="Seg_1097" s="T2">RUS:cult</ta>
            <ta e="T7" id="Seg_1098" s="T6">RUS:gram</ta>
            <ta e="T17" id="Seg_1099" s="T16">RUS:disc</ta>
            <ta e="T18" id="Seg_1100" s="T17">RUS:gram(INDEF)</ta>
            <ta e="T20" id="Seg_1101" s="T19">RUS:gram</ta>
            <ta e="T23" id="Seg_1102" s="T22">TURK:gram(INDEF)</ta>
            <ta e="T25" id="Seg_1103" s="T24">RUS:gram</ta>
            <ta e="T35" id="Seg_1104" s="T34">RUS:mod</ta>
            <ta e="T37" id="Seg_1105" s="T36">RUS:cult</ta>
            <ta e="T40" id="Seg_1106" s="T39">RUS:cult</ta>
            <ta e="T43" id="Seg_1107" s="T42">RUS:disc</ta>
            <ta e="T44" id="Seg_1108" s="T43">RUS:mod</ta>
            <ta e="T52" id="Seg_1109" s="T51">RUS:gram</ta>
            <ta e="T57" id="Seg_1110" s="T56">RUS:gram</ta>
            <ta e="T66" id="Seg_1111" s="T65">RUS:gram</ta>
            <ta e="T73" id="Seg_1112" s="T72">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_1113" s="T0">Солдат шел со службы.</ta>
            <ta e="T6" id="Seg_1114" s="T3">Пришел к одной женщине.</ta>
            <ta e="T8" id="Seg_1115" s="T6">"Давай поедим".</ta>
            <ta e="T11" id="Seg_1116" s="T8">Она говорит: "Вешай!"</ta>
            <ta e="T16" id="Seg_1117" s="T11">"Ты что, не слышишь?"</ta>
            <ta e="T19" id="Seg_1118" s="T16">"Ну, где-нибудь переночуешь".</ta>
            <ta e="T21" id="Seg_1119" s="T19">Давай есть!"</ta>
            <ta e="T24" id="Seg_1120" s="T21">"Нечего есть".</ta>
            <ta e="T28" id="Seg_1121" s="T24">"Давай я сварю топор!"</ta>
            <ta e="T34" id="Seg_1122" s="T28">Она дала [ему] топор, он [его] варил, варил.</ta>
            <ta e="T37" id="Seg_1123" s="T34">"Надо немножко крупы".</ta>
            <ta e="T40" id="Seg_1124" s="T37">Она дала крупу.</ta>
            <ta e="T42" id="Seg_1125" s="T40">Он поварил.</ta>
            <ta e="T45" id="Seg_1126" s="T42">"Ну, надо масло".</ta>
            <ta e="T48" id="Seg_1127" s="T45">Она принесла масло.</ta>
            <ta e="T51" id="Seg_1128" s="T48">Потом он сели, поели.</ta>
            <ta e="T56" id="Seg_1129" s="T51">"А когда мы будем есть топор?"</ta>
            <ta e="T67" id="Seg_1130" s="T56">А он говорит: "Он недоварился, я по дороге доварю и съем".</ta>
            <ta e="T70" id="Seg_1131" s="T67">Потом он ушел.</ta>
            <ta e="T74" id="Seg_1132" s="T70">Взял топор и ушел.</ta>
            <ta e="T75" id="Seg_1133" s="T74">Все!</ta>
            <ta e="T77" id="Seg_1134" s="T75">Женщина осталась.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_1135" s="T0">The soldier was going from the service.</ta>
            <ta e="T6" id="Seg_1136" s="T3">He came to one woman.</ta>
            <ta e="T8" id="Seg_1137" s="T6">"Let's eat!"</ta>
            <ta e="T11" id="Seg_1138" s="T8">She says: "Hang [there]!".</ta>
            <ta e="T16" id="Seg_1139" s="T11">"Don't you hear?"</ta>
            <ta e="T19" id="Seg_1140" s="T16">"Well, somewhere you will spend the night."</ta>
            <ta e="T21" id="Seg_1141" s="T19">"Let's eat!"</ta>
            <ta e="T24" id="Seg_1142" s="T21">"There is nothing to eat."</ta>
            <ta e="T28" id="Seg_1143" s="T24">"Let me cook an axe!"</ta>
            <ta e="T34" id="Seg_1144" s="T28">She gave [him] an axe, he boiled [it], boiled [it].</ta>
            <ta e="T37" id="Seg_1145" s="T34">"[I] need a bit of groats."</ta>
            <ta e="T40" id="Seg_1146" s="T37">She gave groats.</ta>
            <ta e="T42" id="Seg_1147" s="T40">Then he cooked.</ta>
            <ta e="T45" id="Seg_1148" s="T42">"Well, [I] need butter!"</ta>
            <ta e="T48" id="Seg_1149" s="T45">She brought butter.</ta>
            <ta e="T51" id="Seg_1150" s="T48">Then they sat down, ate.</ta>
            <ta e="T56" id="Seg_1151" s="T51">"But when do we eat the axe?"</ta>
            <ta e="T67" id="Seg_1152" s="T56">And he says: "It did not cook [well], I will cook it on the road and eat [it]."</ta>
            <ta e="T70" id="Seg_1153" s="T67">Then he left.</ta>
            <ta e="T74" id="Seg_1154" s="T70">He took the axe and left.</ta>
            <ta e="T75" id="Seg_1155" s="T74">Enough!</ta>
            <ta e="T77" id="Seg_1156" s="T75">The woman stayed.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_1157" s="T0">Der Soldat ging vom Dienst.</ta>
            <ta e="T6" id="Seg_1158" s="T3">Er kam zu einer Frau.</ta>
            <ta e="T8" id="Seg_1159" s="T6">"Lass uns essen!"</ta>
            <ta e="T11" id="Seg_1160" s="T8">Sie sagt. "Häng [es auf]!"</ta>
            <ta e="T16" id="Seg_1161" s="T11">"Hörst du nicht?"</ta>
            <ta e="T19" id="Seg_1162" s="T16">"Nun, irgendwo wirst du die Nacht verbringen."</ta>
            <ta e="T21" id="Seg_1163" s="T19">"Lass uns essen!"</ta>
            <ta e="T24" id="Seg_1164" s="T21">"Es gibt nichts zu essen."</ta>
            <ta e="T28" id="Seg_1165" s="T24">"Lass mich eine Axt kochen!"</ta>
            <ta e="T34" id="Seg_1166" s="T28">Sie gab [ihm] eine Axt, er kochte [sie], er kochte [sie].</ta>
            <ta e="T37" id="Seg_1167" s="T34">"[Ich] brauche etwas Grütze."</ta>
            <ta e="T40" id="Seg_1168" s="T37">Sie gab Grütze.</ta>
            <ta e="T42" id="Seg_1169" s="T40">Dann kochte er.</ta>
            <ta e="T45" id="Seg_1170" s="T42">"Nun, [ich] brauche Butter!"</ta>
            <ta e="T48" id="Seg_1171" s="T45">Sie brachte Butter.</ta>
            <ta e="T51" id="Seg_1172" s="T48">Dann setzten sie sich, aßen.</ta>
            <ta e="T56" id="Seg_1173" s="T51">"Aber wann essen wir die Axt?"</ta>
            <ta e="T67" id="Seg_1174" s="T56">Und er sagt: "Sie ist nicht [gut] gekocht, ich werde sie auf dem Weg kochen und essen."</ta>
            <ta e="T70" id="Seg_1175" s="T67">Dann ging er.</ta>
            <ta e="T74" id="Seg_1176" s="T70">Er nahm die Axt und ging.</ta>
            <ta e="T75" id="Seg_1177" s="T74">Genug!</ta>
            <ta e="T77" id="Seg_1178" s="T75">Die Frau blieb.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T3" id="Seg_1179" s="T0">[GVY:] The tale is well-known, but not all the versions mention the old woman's deafness. One of such versions see here: http://hyaenidae.narod.ru/story2/093.html. službagən = službagəʔs</ta>
            <ta e="T11" id="Seg_1180" s="T8">[GVY:] The old woman pretends to be deaf.</ta>
            <ta e="T16" id="Seg_1181" s="T11">[GVY:] nüngiel = nünniel</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T78" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T80" />
            <conversion-tli id="T79" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
