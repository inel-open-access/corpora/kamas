<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID3C0C6F73-ED97-E46F-9CE7-A60641C90134">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_HorseDied_flk.wav" />
         <referenced-file url="PKZ_196X_HorseDied_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_HorseDied_flk\PKZ_196X_HorseDied_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">76</ud-information>
            <ud-information attribute-name="# HIAT:w">49</ud-information>
            <ud-information attribute-name="# e">48</ud-information>
            <ud-information attribute-name="# HIAT:u">10</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T581" time="0.1" type="appl" />
         <tli id="T582" time="0.622" type="appl" />
         <tli id="T583" time="1.144" type="appl" />
         <tli id="T584" time="1.667" type="appl" />
         <tli id="T585" time="2.189" type="appl" />
         <tli id="T586" time="3.239860934138673" />
         <tli id="T587" time="3.932" type="appl" />
         <tli id="T588" time="4.819" type="appl" />
         <tli id="T589" time="5.706" type="appl" />
         <tli id="T590" time="6.592" type="appl" />
         <tli id="T591" time="7.714" type="appl" />
         <tli id="T592" time="8.837" type="appl" />
         <tli id="T593" time="9.959" type="appl" />
         <tli id="T594" time="11.081" type="appl" />
         <tli id="T595" time="12.204" type="appl" />
         <tli id="T596" time="13.326" type="appl" />
         <tli id="T597" time="13.898" type="appl" />
         <tli id="T598" time="14.471" type="appl" />
         <tli id="T599" time="15.044" type="appl" />
         <tli id="T600" time="15.839320122455732" />
         <tli id="T601" time="16.763" type="appl" />
         <tli id="T602" time="18.61920080051299" />
         <tli id="T603" time="19.459" type="appl" />
         <tli id="T604" time="20.226" type="appl" />
         <tli id="T605" time="20.993" type="appl" />
         <tli id="T606" time="21.76" type="appl" />
         <tli id="T607" time="22.939015379364555" />
         <tli id="T608" time="23.469" type="appl" />
         <tli id="T609" time="24.071" type="appl" />
         <tli id="T610" time="24.672" type="appl" />
         <tli id="T611" time="25.274" type="appl" />
         <tli id="T612" time="25.875" type="appl" />
         <tli id="T613" time="26.476" type="appl" />
         <tli id="T614" time="27.078" type="appl" />
         <tli id="T615" time="28.812096620056263" />
         <tli id="T616" time="29.484" type="appl" />
         <tli id="T617" time="30.168" type="appl" />
         <tli id="T618" time="30.852" type="appl" />
         <tli id="T619" time="31.536" type="appl" />
         <tli id="T620" time="32.22" type="appl" />
         <tli id="T621" time="33.102" type="appl" />
         <tli id="T622" time="33.983" type="appl" />
         <tli id="T623" time="34.864" type="appl" />
         <tli id="T624" time="35.746" type="appl" />
         <tli id="T625" time="36.628" type="appl" />
         <tli id="T626" time="37.509" type="appl" />
         <tli id="T627" time="38.39" type="appl" />
         <tli id="T628" time="39.272" type="appl" />
         <tli id="T629" time="40.285" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <timeline-fork end="T612" start="T611">
            <tli id="T611.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T629" id="Seg_0" n="sc" s="T581">
               <ts e="T586" id="Seg_2" n="HIAT:u" s="T581">
                  <ts e="T582" id="Seg_4" n="HIAT:w" s="T581">Onʼiʔ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_7" n="HIAT:w" s="T582">kuzan</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_9" n="HIAT:ip">(</nts>
                  <ts e="T584" id="Seg_11" n="HIAT:w" s="T583">bɨl-</ts>
                  <nts id="Seg_12" n="HIAT:ip">)</nts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_15" n="HIAT:w" s="T584">ibi</ts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_18" n="HIAT:w" s="T585">ine</ts>
                  <nts id="Seg_19" n="HIAT:ip">.</nts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T590" id="Seg_22" n="HIAT:u" s="T586">
                  <ts e="T587" id="Seg_24" n="HIAT:w" s="T586">Dĭ</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_26" n="HIAT:ip">(</nts>
                  <ts e="T588" id="Seg_28" n="HIAT:w" s="T587">măjanə=</ts>
                  <nts id="Seg_29" n="HIAT:ip">)</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_32" n="HIAT:w" s="T588">măjagən</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_35" n="HIAT:w" s="T589">mĭmbi</ts>
                  <nts id="Seg_36" n="HIAT:ip">.</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T596" id="Seg_39" n="HIAT:u" s="T590">
                  <ts e="T591" id="Seg_41" n="HIAT:w" s="T590">Dĭgəttə</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_44" n="HIAT:w" s="T591">saʔməluʔpi</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_47" n="HIAT:w" s="T592">dʼünə</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_49" n="HIAT:ip">(</nts>
                  <ts e="T594" id="Seg_51" n="HIAT:w" s="T593">i=</ts>
                  <nts id="Seg_52" n="HIAT:ip">)</nts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_55" n="HIAT:w" s="T594">i</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_58" n="HIAT:w" s="T595">külaːmbi</ts>
                  <nts id="Seg_59" n="HIAT:ip">.</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T600" id="Seg_62" n="HIAT:u" s="T596">
                  <ts e="T597" id="Seg_64" n="HIAT:w" s="T596">Dĭ</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_67" n="HIAT:w" s="T597">davaj</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_70" n="HIAT:w" s="T598">kirgarzittə</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_73" n="HIAT:w" s="T599">il</ts>
                  <nts id="Seg_74" n="HIAT:ip">.</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T602" id="Seg_77" n="HIAT:u" s="T600">
                  <ts e="T601" id="Seg_79" n="HIAT:w" s="T600">Nuʔməleʔ</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_82" n="HIAT:w" s="T601">šobiʔi</ts>
                  <nts id="Seg_83" n="HIAT:ip">.</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T607" id="Seg_86" n="HIAT:u" s="T602">
                  <nts id="Seg_87" n="HIAT:ip">(</nts>
                  <ts e="T603" id="Seg_89" n="HIAT:w" s="T602">Măn=</ts>
                  <nts id="Seg_90" n="HIAT:ip">)</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_93" n="HIAT:w" s="T603">Măn</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_96" n="HIAT:w" s="T604">inem</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_99" n="HIAT:w" s="T605">saʔməluʔpi</ts>
                  <nts id="Seg_100" n="HIAT:ip">,</nts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_103" n="HIAT:w" s="T606">külaːmbi</ts>
                  <nts id="Seg_104" n="HIAT:ip">.</nts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T615" id="Seg_107" n="HIAT:u" s="T607">
                  <ts e="T608" id="Seg_109" n="HIAT:w" s="T607">A</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_112" n="HIAT:w" s="T608">dĭzeŋ</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_115" n="HIAT:w" s="T609">măliaʔi:</ts>
                  <nts id="Seg_116" n="HIAT:ip">"</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_119" n="HIAT:w" s="T610">A</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611.tx.1" id="Seg_122" n="HIAT:w" s="T611">na</ts>
                  <nts id="Seg_123" n="HIAT:ip">_</nts>
                  <ts e="T612" id="Seg_125" n="HIAT:w" s="T611.tx.1">što</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_128" n="HIAT:w" s="T612">tăn</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_131" n="HIAT:w" s="T613">miʔnʼibeʔ</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_134" n="HIAT:w" s="T614">kăštəbial</ts>
                  <nts id="Seg_135" n="HIAT:ip">?</nts>
                  <nts id="Seg_136" n="HIAT:ip">"</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T620" id="Seg_139" n="HIAT:u" s="T615">
                  <ts e="T616" id="Seg_141" n="HIAT:w" s="T615">Nu</ts>
                  <nts id="Seg_142" n="HIAT:ip">,</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_145" n="HIAT:w" s="T616">dĭgəttə</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_148" n="HIAT:w" s="T617">šiʔ</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_151" n="HIAT:w" s="T618">surarbileʔ</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_154" n="HIAT:w" s="T619">bar</ts>
                  <nts id="Seg_155" n="HIAT:ip">.</nts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T628" id="Seg_158" n="HIAT:u" s="T620">
                  <ts e="T621" id="Seg_160" n="HIAT:w" s="T620">Tüj</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_162" n="HIAT:ip">(</nts>
                  <ts e="T622" id="Seg_164" n="HIAT:w" s="T621">boškə-</ts>
                  <nts id="Seg_165" n="HIAT:ip">)</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_168" n="HIAT:w" s="T622">bostənziʔ</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_170" n="HIAT:ip">(</nts>
                  <ts e="T624" id="Seg_172" n="HIAT:w" s="T623">kubi-</ts>
                  <nts id="Seg_173" n="HIAT:ip">)</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_176" n="HIAT:w" s="T624">kubilaʔ</ts>
                  <nts id="Seg_177" n="HIAT:ip">,</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_180" n="HIAT:w" s="T625">što</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_183" n="HIAT:w" s="T626">inem</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_186" n="HIAT:w" s="T627">külaːmbi</ts>
                  <nts id="Seg_187" n="HIAT:ip">.</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T629" id="Seg_190" n="HIAT:u" s="T628">
                  <ts e="T629" id="Seg_192" n="HIAT:w" s="T628">Kabarləj</ts>
                  <nts id="Seg_193" n="HIAT:ip">.</nts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T629" id="Seg_195" n="sc" s="T581">
               <ts e="T582" id="Seg_197" n="e" s="T581">Onʼiʔ </ts>
               <ts e="T583" id="Seg_199" n="e" s="T582">kuzan </ts>
               <ts e="T584" id="Seg_201" n="e" s="T583">(bɨl-) </ts>
               <ts e="T585" id="Seg_203" n="e" s="T584">ibi </ts>
               <ts e="T586" id="Seg_205" n="e" s="T585">ine. </ts>
               <ts e="T587" id="Seg_207" n="e" s="T586">Dĭ </ts>
               <ts e="T588" id="Seg_209" n="e" s="T587">(măjanə=) </ts>
               <ts e="T589" id="Seg_211" n="e" s="T588">măjagən </ts>
               <ts e="T590" id="Seg_213" n="e" s="T589">mĭmbi. </ts>
               <ts e="T591" id="Seg_215" n="e" s="T590">Dĭgəttə </ts>
               <ts e="T592" id="Seg_217" n="e" s="T591">saʔməluʔpi </ts>
               <ts e="T593" id="Seg_219" n="e" s="T592">dʼünə </ts>
               <ts e="T594" id="Seg_221" n="e" s="T593">(i=) </ts>
               <ts e="T595" id="Seg_223" n="e" s="T594">i </ts>
               <ts e="T596" id="Seg_225" n="e" s="T595">külaːmbi. </ts>
               <ts e="T597" id="Seg_227" n="e" s="T596">Dĭ </ts>
               <ts e="T598" id="Seg_229" n="e" s="T597">davaj </ts>
               <ts e="T599" id="Seg_231" n="e" s="T598">kirgarzittə </ts>
               <ts e="T600" id="Seg_233" n="e" s="T599">il. </ts>
               <ts e="T601" id="Seg_235" n="e" s="T600">Nuʔməleʔ </ts>
               <ts e="T602" id="Seg_237" n="e" s="T601">šobiʔi. </ts>
               <ts e="T603" id="Seg_239" n="e" s="T602">(Măn=) </ts>
               <ts e="T604" id="Seg_241" n="e" s="T603">Măn </ts>
               <ts e="T605" id="Seg_243" n="e" s="T604">inem </ts>
               <ts e="T606" id="Seg_245" n="e" s="T605">saʔməluʔpi, </ts>
               <ts e="T607" id="Seg_247" n="e" s="T606">külaːmbi. </ts>
               <ts e="T608" id="Seg_249" n="e" s="T607">A </ts>
               <ts e="T609" id="Seg_251" n="e" s="T608">dĭzeŋ </ts>
               <ts e="T610" id="Seg_253" n="e" s="T609">măliaʔi:" </ts>
               <ts e="T611" id="Seg_255" n="e" s="T610">A </ts>
               <ts e="T612" id="Seg_257" n="e" s="T611">na_što </ts>
               <ts e="T613" id="Seg_259" n="e" s="T612">tăn </ts>
               <ts e="T614" id="Seg_261" n="e" s="T613">miʔnʼibeʔ </ts>
               <ts e="T615" id="Seg_263" n="e" s="T614">kăštəbial?" </ts>
               <ts e="T616" id="Seg_265" n="e" s="T615">Nu, </ts>
               <ts e="T617" id="Seg_267" n="e" s="T616">dĭgəttə </ts>
               <ts e="T618" id="Seg_269" n="e" s="T617">šiʔ </ts>
               <ts e="T619" id="Seg_271" n="e" s="T618">surarbileʔ </ts>
               <ts e="T620" id="Seg_273" n="e" s="T619">bar. </ts>
               <ts e="T621" id="Seg_275" n="e" s="T620">Tüj </ts>
               <ts e="T622" id="Seg_277" n="e" s="T621">(boškə-) </ts>
               <ts e="T623" id="Seg_279" n="e" s="T622">bostənziʔ </ts>
               <ts e="T624" id="Seg_281" n="e" s="T623">(kubi-) </ts>
               <ts e="T625" id="Seg_283" n="e" s="T624">kubilaʔ, </ts>
               <ts e="T626" id="Seg_285" n="e" s="T625">što </ts>
               <ts e="T627" id="Seg_287" n="e" s="T626">inem </ts>
               <ts e="T628" id="Seg_289" n="e" s="T627">külaːmbi. </ts>
               <ts e="T629" id="Seg_291" n="e" s="T628">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T586" id="Seg_292" s="T581">PKZ_196X_HorseDied_flk.001 (001)</ta>
            <ta e="T590" id="Seg_293" s="T586">PKZ_196X_HorseDied_flk.002 (002)</ta>
            <ta e="T596" id="Seg_294" s="T590">PKZ_196X_HorseDied_flk.003 (003)</ta>
            <ta e="T600" id="Seg_295" s="T596">PKZ_196X_HorseDied_flk.004 (004)</ta>
            <ta e="T602" id="Seg_296" s="T600">PKZ_196X_HorseDied_flk.005 (005)</ta>
            <ta e="T607" id="Seg_297" s="T602">PKZ_196X_HorseDied_flk.006 (006)</ta>
            <ta e="T615" id="Seg_298" s="T607">PKZ_196X_HorseDied_flk.007 (007)</ta>
            <ta e="T620" id="Seg_299" s="T615">PKZ_196X_HorseDied_flk.008 (008)</ta>
            <ta e="T628" id="Seg_300" s="T620">PKZ_196X_HorseDied_flk.009 (009)</ta>
            <ta e="T629" id="Seg_301" s="T628">PKZ_196X_HorseDied_flk.010 (010)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T586" id="Seg_302" s="T581">Onʼiʔ kuzan (bɨl-) ibi ine. </ta>
            <ta e="T590" id="Seg_303" s="T586">Dĭ (măjanə=) măjagən mĭmbi. </ta>
            <ta e="T596" id="Seg_304" s="T590">Dĭgəttə saʔməluʔpi dʼünə (i=) i külaːmbi. </ta>
            <ta e="T600" id="Seg_305" s="T596">Dĭ davaj kirgarzittə il. </ta>
            <ta e="T602" id="Seg_306" s="T600">Nuʔməleʔ šobiʔi. </ta>
            <ta e="T607" id="Seg_307" s="T602">(Măn=) Măn inem saʔməluʔpi, külaːmbi. </ta>
            <ta e="T615" id="Seg_308" s="T607">A dĭzeŋ măliaʔi:" A na što tăn miʔnʼibeʔ kăštəbial?" </ta>
            <ta e="T620" id="Seg_309" s="T615">Nu, dĭgəttə šiʔ surarbileʔ bar. </ta>
            <ta e="T628" id="Seg_310" s="T620">Tüj (boškə-) bostənziʔ (kubi-) kubilaʔ, što inem külaːmbi. </ta>
            <ta e="T629" id="Seg_311" s="T628">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T582" id="Seg_312" s="T581">onʼiʔ</ta>
            <ta e="T583" id="Seg_313" s="T582">kuza-n</ta>
            <ta e="T585" id="Seg_314" s="T584">i-bi</ta>
            <ta e="T586" id="Seg_315" s="T585">ine</ta>
            <ta e="T587" id="Seg_316" s="T586">dĭ</ta>
            <ta e="T588" id="Seg_317" s="T587">măja-nə</ta>
            <ta e="T589" id="Seg_318" s="T588">măja-gən</ta>
            <ta e="T590" id="Seg_319" s="T589">mĭm-bi</ta>
            <ta e="T591" id="Seg_320" s="T590">dĭgəttə</ta>
            <ta e="T592" id="Seg_321" s="T591">saʔmə-luʔ-pi</ta>
            <ta e="T593" id="Seg_322" s="T592">dʼü-nə</ta>
            <ta e="T594" id="Seg_323" s="T593">i</ta>
            <ta e="T595" id="Seg_324" s="T594">i</ta>
            <ta e="T596" id="Seg_325" s="T595">kü-laːm-bi</ta>
            <ta e="T597" id="Seg_326" s="T596">dĭ</ta>
            <ta e="T598" id="Seg_327" s="T597">davaj</ta>
            <ta e="T599" id="Seg_328" s="T598">kirgar-zittə</ta>
            <ta e="T600" id="Seg_329" s="T599">il</ta>
            <ta e="T601" id="Seg_330" s="T600">nuʔmə-leʔ</ta>
            <ta e="T602" id="Seg_331" s="T601">šo-bi-ʔi</ta>
            <ta e="T603" id="Seg_332" s="T602">măn</ta>
            <ta e="T604" id="Seg_333" s="T603">măn</ta>
            <ta e="T605" id="Seg_334" s="T604">ine-m</ta>
            <ta e="T606" id="Seg_335" s="T605">saʔmə-luʔ-pi</ta>
            <ta e="T607" id="Seg_336" s="T606">kü-laːm-bi</ta>
            <ta e="T608" id="Seg_337" s="T607">a</ta>
            <ta e="T609" id="Seg_338" s="T608">dĭ-zeŋ</ta>
            <ta e="T610" id="Seg_339" s="T609">mă-lia-ʔi</ta>
            <ta e="T611" id="Seg_340" s="T610">a</ta>
            <ta e="T612" id="Seg_341" s="T611">našto</ta>
            <ta e="T613" id="Seg_342" s="T612">tăn</ta>
            <ta e="T614" id="Seg_343" s="T613">miʔnʼibeʔ</ta>
            <ta e="T615" id="Seg_344" s="T614">kăštə-bia-l</ta>
            <ta e="T616" id="Seg_345" s="T615">nu</ta>
            <ta e="T617" id="Seg_346" s="T616">dĭgəttə</ta>
            <ta e="T618" id="Seg_347" s="T617">šiʔ</ta>
            <ta e="T619" id="Seg_348" s="T618">surar-bi-leʔ</ta>
            <ta e="T620" id="Seg_349" s="T619">bar</ta>
            <ta e="T621" id="Seg_350" s="T620">tüj</ta>
            <ta e="T623" id="Seg_351" s="T622">bos-tən-ziʔ</ta>
            <ta e="T625" id="Seg_352" s="T624">ku-bi-laʔ</ta>
            <ta e="T626" id="Seg_353" s="T625">što</ta>
            <ta e="T627" id="Seg_354" s="T626">ine-m</ta>
            <ta e="T628" id="Seg_355" s="T627">kü-laːm-bi</ta>
            <ta e="T629" id="Seg_356" s="T628">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T582" id="Seg_357" s="T581">onʼiʔ</ta>
            <ta e="T583" id="Seg_358" s="T582">kuza-n</ta>
            <ta e="T585" id="Seg_359" s="T584">i-bi</ta>
            <ta e="T586" id="Seg_360" s="T585">ine</ta>
            <ta e="T587" id="Seg_361" s="T586">dĭ</ta>
            <ta e="T588" id="Seg_362" s="T587">măja-Tə</ta>
            <ta e="T589" id="Seg_363" s="T588">măja-Kən</ta>
            <ta e="T590" id="Seg_364" s="T589">mĭn-bi</ta>
            <ta e="T591" id="Seg_365" s="T590">dĭgəttə</ta>
            <ta e="T592" id="Seg_366" s="T591">saʔmə-luʔbdə-bi</ta>
            <ta e="T593" id="Seg_367" s="T592">tʼo-Tə</ta>
            <ta e="T594" id="Seg_368" s="T593">i</ta>
            <ta e="T595" id="Seg_369" s="T594">i</ta>
            <ta e="T596" id="Seg_370" s="T595">kü-laːm-bi</ta>
            <ta e="T597" id="Seg_371" s="T596">dĭ</ta>
            <ta e="T598" id="Seg_372" s="T597">davaj</ta>
            <ta e="T599" id="Seg_373" s="T598">kirgaːr-zittə</ta>
            <ta e="T600" id="Seg_374" s="T599">il</ta>
            <ta e="T601" id="Seg_375" s="T600">nuʔmə-lAʔ</ta>
            <ta e="T602" id="Seg_376" s="T601">šo-bi-jəʔ</ta>
            <ta e="T603" id="Seg_377" s="T602">măn</ta>
            <ta e="T604" id="Seg_378" s="T603">măn</ta>
            <ta e="T605" id="Seg_379" s="T604">ine-m</ta>
            <ta e="T606" id="Seg_380" s="T605">saʔmə-luʔbdə-bi</ta>
            <ta e="T607" id="Seg_381" s="T606">kü-laːm-bi</ta>
            <ta e="T608" id="Seg_382" s="T607">a</ta>
            <ta e="T609" id="Seg_383" s="T608">dĭ-zAŋ</ta>
            <ta e="T610" id="Seg_384" s="T609">măn-liA-jəʔ</ta>
            <ta e="T611" id="Seg_385" s="T610">a</ta>
            <ta e="T612" id="Seg_386" s="T611">našto</ta>
            <ta e="T613" id="Seg_387" s="T612">tăn</ta>
            <ta e="T614" id="Seg_388" s="T613">miʔnʼibeʔ</ta>
            <ta e="T615" id="Seg_389" s="T614">kăštə-bi-l</ta>
            <ta e="T616" id="Seg_390" s="T615">nu</ta>
            <ta e="T617" id="Seg_391" s="T616">dĭgəttə</ta>
            <ta e="T618" id="Seg_392" s="T617">šiʔ</ta>
            <ta e="T619" id="Seg_393" s="T618">surar-bi-lAʔ</ta>
            <ta e="T620" id="Seg_394" s="T619">bar</ta>
            <ta e="T621" id="Seg_395" s="T620">tüj</ta>
            <ta e="T623" id="Seg_396" s="T622">bos-dən-ziʔ</ta>
            <ta e="T625" id="Seg_397" s="T624">ku-bi-lAʔ</ta>
            <ta e="T626" id="Seg_398" s="T625">što</ta>
            <ta e="T627" id="Seg_399" s="T626">ine-m</ta>
            <ta e="T628" id="Seg_400" s="T627">kü-laːm-bi</ta>
            <ta e="T629" id="Seg_401" s="T628">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T582" id="Seg_402" s="T581">one.[NOM.SG]</ta>
            <ta e="T583" id="Seg_403" s="T582">man-GEN</ta>
            <ta e="T585" id="Seg_404" s="T584">be-PST.[3SG]</ta>
            <ta e="T586" id="Seg_405" s="T585">horse.[NOM.SG]</ta>
            <ta e="T587" id="Seg_406" s="T586">this.[NOM.SG]</ta>
            <ta e="T588" id="Seg_407" s="T587">mountain-LAT</ta>
            <ta e="T589" id="Seg_408" s="T588">mountain-LOC</ta>
            <ta e="T590" id="Seg_409" s="T589">go-PST.[3SG]</ta>
            <ta e="T591" id="Seg_410" s="T590">then</ta>
            <ta e="T592" id="Seg_411" s="T591">fall-MOM-PST.[3SG]</ta>
            <ta e="T593" id="Seg_412" s="T592">place-LAT</ta>
            <ta e="T594" id="Seg_413" s="T593">and</ta>
            <ta e="T595" id="Seg_414" s="T594">and</ta>
            <ta e="T596" id="Seg_415" s="T595">die-RES-PST.[3SG]</ta>
            <ta e="T597" id="Seg_416" s="T596">this.[NOM.SG]</ta>
            <ta e="T598" id="Seg_417" s="T597">INCH</ta>
            <ta e="T599" id="Seg_418" s="T598">shout-INF.LAT</ta>
            <ta e="T600" id="Seg_419" s="T599">people.[NOM.SG]</ta>
            <ta e="T601" id="Seg_420" s="T600">run-CVB</ta>
            <ta e="T602" id="Seg_421" s="T601">come-PST-3PL</ta>
            <ta e="T603" id="Seg_422" s="T602">I.GEN</ta>
            <ta e="T604" id="Seg_423" s="T603">I.GEN</ta>
            <ta e="T605" id="Seg_424" s="T604">horse-1SG</ta>
            <ta e="T606" id="Seg_425" s="T605">fall-MOM-PST.[3SG]</ta>
            <ta e="T607" id="Seg_426" s="T606">die-RES-PST.[3SG]</ta>
            <ta e="T608" id="Seg_427" s="T607">and</ta>
            <ta e="T609" id="Seg_428" s="T608">this-PL</ta>
            <ta e="T610" id="Seg_429" s="T609">say-PRS-3PL</ta>
            <ta e="T611" id="Seg_430" s="T610">and</ta>
            <ta e="T612" id="Seg_431" s="T611">what.for</ta>
            <ta e="T613" id="Seg_432" s="T612">you.NOM</ta>
            <ta e="T614" id="Seg_433" s="T613">we.LAT</ta>
            <ta e="T615" id="Seg_434" s="T614">call-PST-2SG</ta>
            <ta e="T616" id="Seg_435" s="T615">well</ta>
            <ta e="T617" id="Seg_436" s="T616">then</ta>
            <ta e="T618" id="Seg_437" s="T617">you.PL.NOM</ta>
            <ta e="T619" id="Seg_438" s="T618">ask-PST-2PL</ta>
            <ta e="T620" id="Seg_439" s="T619">all</ta>
            <ta e="T621" id="Seg_440" s="T620">now</ta>
            <ta e="T623" id="Seg_441" s="T622">self-NOM/GEN/ACC.3PL-INS</ta>
            <ta e="T625" id="Seg_442" s="T624">see-PST-CVB</ta>
            <ta e="T626" id="Seg_443" s="T625">that</ta>
            <ta e="T627" id="Seg_444" s="T626">horse-ACC</ta>
            <ta e="T628" id="Seg_445" s="T627">die-RES-PST.[3SG]</ta>
            <ta e="T629" id="Seg_446" s="T628">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T582" id="Seg_447" s="T581">один.[NOM.SG]</ta>
            <ta e="T583" id="Seg_448" s="T582">мужчина-GEN</ta>
            <ta e="T585" id="Seg_449" s="T584">быть-PST.[3SG]</ta>
            <ta e="T586" id="Seg_450" s="T585">лошадь.[NOM.SG]</ta>
            <ta e="T587" id="Seg_451" s="T586">этот.[NOM.SG]</ta>
            <ta e="T588" id="Seg_452" s="T587">гора-LAT</ta>
            <ta e="T589" id="Seg_453" s="T588">гора-LOC</ta>
            <ta e="T590" id="Seg_454" s="T589">идти-PST.[3SG]</ta>
            <ta e="T591" id="Seg_455" s="T590">тогда</ta>
            <ta e="T592" id="Seg_456" s="T591">упасть-MOM-PST.[3SG]</ta>
            <ta e="T593" id="Seg_457" s="T592">место-LAT</ta>
            <ta e="T594" id="Seg_458" s="T593">и</ta>
            <ta e="T595" id="Seg_459" s="T594">и</ta>
            <ta e="T596" id="Seg_460" s="T595">умереть-RES-PST.[3SG]</ta>
            <ta e="T597" id="Seg_461" s="T596">этот.[NOM.SG]</ta>
            <ta e="T598" id="Seg_462" s="T597">INCH</ta>
            <ta e="T599" id="Seg_463" s="T598">кричать-INF.LAT</ta>
            <ta e="T600" id="Seg_464" s="T599">люди.[NOM.SG]</ta>
            <ta e="T601" id="Seg_465" s="T600">бежать-CVB</ta>
            <ta e="T602" id="Seg_466" s="T601">прийти-PST-3PL</ta>
            <ta e="T603" id="Seg_467" s="T602">я.GEN</ta>
            <ta e="T604" id="Seg_468" s="T603">я.GEN</ta>
            <ta e="T605" id="Seg_469" s="T604">лошадь-1SG</ta>
            <ta e="T606" id="Seg_470" s="T605">упасть-MOM-PST.[3SG]</ta>
            <ta e="T607" id="Seg_471" s="T606">умереть-RES-PST.[3SG]</ta>
            <ta e="T608" id="Seg_472" s="T607">а</ta>
            <ta e="T609" id="Seg_473" s="T608">этот-PL</ta>
            <ta e="T610" id="Seg_474" s="T609">сказать-PRS-3PL</ta>
            <ta e="T611" id="Seg_475" s="T610">а</ta>
            <ta e="T612" id="Seg_476" s="T611">зачем</ta>
            <ta e="T613" id="Seg_477" s="T612">ты.NOM</ta>
            <ta e="T614" id="Seg_478" s="T613">мы.LAT</ta>
            <ta e="T615" id="Seg_479" s="T614">позвать-PST-2SG</ta>
            <ta e="T616" id="Seg_480" s="T615">ну</ta>
            <ta e="T617" id="Seg_481" s="T616">тогда</ta>
            <ta e="T618" id="Seg_482" s="T617">вы.NOM</ta>
            <ta e="T619" id="Seg_483" s="T618">спросить-PST-2PL</ta>
            <ta e="T620" id="Seg_484" s="T619">весь</ta>
            <ta e="T621" id="Seg_485" s="T620">сейчас</ta>
            <ta e="T623" id="Seg_486" s="T622">сам-NOM/GEN/ACC.3PL-INS</ta>
            <ta e="T625" id="Seg_487" s="T624">видеть-PST-CVB</ta>
            <ta e="T626" id="Seg_488" s="T625">что</ta>
            <ta e="T627" id="Seg_489" s="T626">лошадь-ACC</ta>
            <ta e="T628" id="Seg_490" s="T627">умереть-RES-PST.[3SG]</ta>
            <ta e="T629" id="Seg_491" s="T628">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T582" id="Seg_492" s="T581">num-n:case</ta>
            <ta e="T583" id="Seg_493" s="T582">n-n:case</ta>
            <ta e="T585" id="Seg_494" s="T584">v-v:tense-v:pn</ta>
            <ta e="T586" id="Seg_495" s="T585">n-n:case</ta>
            <ta e="T587" id="Seg_496" s="T586">dempro-n:case</ta>
            <ta e="T588" id="Seg_497" s="T587">n-n:case</ta>
            <ta e="T589" id="Seg_498" s="T588">n-n:case</ta>
            <ta e="T590" id="Seg_499" s="T589">v-v:tense-v:pn</ta>
            <ta e="T591" id="Seg_500" s="T590">adv</ta>
            <ta e="T592" id="Seg_501" s="T591">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T593" id="Seg_502" s="T592">n-n:case</ta>
            <ta e="T594" id="Seg_503" s="T593">conj</ta>
            <ta e="T595" id="Seg_504" s="T594">conj</ta>
            <ta e="T596" id="Seg_505" s="T595">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T597" id="Seg_506" s="T596">dempro-n:case</ta>
            <ta e="T598" id="Seg_507" s="T597">ptcl</ta>
            <ta e="T599" id="Seg_508" s="T598">v-v:n.fin</ta>
            <ta e="T600" id="Seg_509" s="T599">n-n:case</ta>
            <ta e="T601" id="Seg_510" s="T600">v-v:n.fin</ta>
            <ta e="T602" id="Seg_511" s="T601">v-v:tense-v:pn</ta>
            <ta e="T603" id="Seg_512" s="T602">pers</ta>
            <ta e="T604" id="Seg_513" s="T603">pers</ta>
            <ta e="T605" id="Seg_514" s="T604">n-n:poss</ta>
            <ta e="T606" id="Seg_515" s="T605">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T607" id="Seg_516" s="T606">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T608" id="Seg_517" s="T607">conj</ta>
            <ta e="T609" id="Seg_518" s="T608">dempro-n:num</ta>
            <ta e="T610" id="Seg_519" s="T609">v-v:tense-v:pn</ta>
            <ta e="T611" id="Seg_520" s="T610">conj</ta>
            <ta e="T612" id="Seg_521" s="T611">adv</ta>
            <ta e="T613" id="Seg_522" s="T612">pers</ta>
            <ta e="T614" id="Seg_523" s="T613">pers</ta>
            <ta e="T615" id="Seg_524" s="T614">v-v:tense-v:pn</ta>
            <ta e="T616" id="Seg_525" s="T615">ptcl</ta>
            <ta e="T617" id="Seg_526" s="T616">adv</ta>
            <ta e="T618" id="Seg_527" s="T617">pers</ta>
            <ta e="T619" id="Seg_528" s="T618">v-v:tense-v:pn</ta>
            <ta e="T620" id="Seg_529" s="T619">quant</ta>
            <ta e="T621" id="Seg_530" s="T620">adv</ta>
            <ta e="T623" id="Seg_531" s="T622">refl-n:case.poss-n:case</ta>
            <ta e="T625" id="Seg_532" s="T624">v-v:tense-v:n.fin</ta>
            <ta e="T626" id="Seg_533" s="T625">conj</ta>
            <ta e="T627" id="Seg_534" s="T626">n-n:case</ta>
            <ta e="T628" id="Seg_535" s="T627">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T629" id="Seg_536" s="T628">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T582" id="Seg_537" s="T581">num</ta>
            <ta e="T583" id="Seg_538" s="T582">n</ta>
            <ta e="T585" id="Seg_539" s="T584">v</ta>
            <ta e="T586" id="Seg_540" s="T585">n</ta>
            <ta e="T587" id="Seg_541" s="T586">dempro</ta>
            <ta e="T588" id="Seg_542" s="T587">n</ta>
            <ta e="T589" id="Seg_543" s="T588">n</ta>
            <ta e="T590" id="Seg_544" s="T589">v</ta>
            <ta e="T591" id="Seg_545" s="T590">adv</ta>
            <ta e="T592" id="Seg_546" s="T591">v</ta>
            <ta e="T593" id="Seg_547" s="T592">n</ta>
            <ta e="T594" id="Seg_548" s="T593">conj</ta>
            <ta e="T595" id="Seg_549" s="T594">conj</ta>
            <ta e="T596" id="Seg_550" s="T595">v</ta>
            <ta e="T597" id="Seg_551" s="T596">dempro</ta>
            <ta e="T598" id="Seg_552" s="T597">ptcl</ta>
            <ta e="T599" id="Seg_553" s="T598">v</ta>
            <ta e="T600" id="Seg_554" s="T599">n</ta>
            <ta e="T601" id="Seg_555" s="T600">v</ta>
            <ta e="T602" id="Seg_556" s="T601">v</ta>
            <ta e="T603" id="Seg_557" s="T602">pers</ta>
            <ta e="T604" id="Seg_558" s="T603">pers</ta>
            <ta e="T605" id="Seg_559" s="T604">n</ta>
            <ta e="T606" id="Seg_560" s="T605">v</ta>
            <ta e="T607" id="Seg_561" s="T606">v</ta>
            <ta e="T608" id="Seg_562" s="T607">conj</ta>
            <ta e="T609" id="Seg_563" s="T608">dempro</ta>
            <ta e="T610" id="Seg_564" s="T609">v</ta>
            <ta e="T611" id="Seg_565" s="T610">conj</ta>
            <ta e="T612" id="Seg_566" s="T611">adv</ta>
            <ta e="T613" id="Seg_567" s="T612">pers</ta>
            <ta e="T614" id="Seg_568" s="T613">pers</ta>
            <ta e="T615" id="Seg_569" s="T614">v</ta>
            <ta e="T616" id="Seg_570" s="T615">ptcl</ta>
            <ta e="T617" id="Seg_571" s="T616">adv</ta>
            <ta e="T618" id="Seg_572" s="T617">pers</ta>
            <ta e="T619" id="Seg_573" s="T618">v</ta>
            <ta e="T620" id="Seg_574" s="T619">quant</ta>
            <ta e="T621" id="Seg_575" s="T620">adv</ta>
            <ta e="T623" id="Seg_576" s="T622">refl</ta>
            <ta e="T625" id="Seg_577" s="T624">v</ta>
            <ta e="T626" id="Seg_578" s="T625">conj</ta>
            <ta e="T627" id="Seg_579" s="T626">n</ta>
            <ta e="T628" id="Seg_580" s="T627">v</ta>
            <ta e="T629" id="Seg_581" s="T628">ptcl</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T585" id="Seg_582" s="T584">v:pred</ta>
            <ta e="T586" id="Seg_583" s="T585">np:S</ta>
            <ta e="T587" id="Seg_584" s="T586">pro:S</ta>
            <ta e="T590" id="Seg_585" s="T589">v:pred</ta>
            <ta e="T592" id="Seg_586" s="T591">0.3:S v:pred</ta>
            <ta e="T596" id="Seg_587" s="T595">0.3:S v:pred</ta>
            <ta e="T598" id="Seg_588" s="T597">ptcl:pred</ta>
            <ta e="T601" id="Seg_589" s="T600">conv:pred</ta>
            <ta e="T602" id="Seg_590" s="T601">0.3.h:S v:pred</ta>
            <ta e="T605" id="Seg_591" s="T604">np:S</ta>
            <ta e="T606" id="Seg_592" s="T605">v:pred</ta>
            <ta e="T607" id="Seg_593" s="T606">0.3:S v:pred</ta>
            <ta e="T609" id="Seg_594" s="T608">pro.h:S</ta>
            <ta e="T610" id="Seg_595" s="T609">v:pred</ta>
            <ta e="T613" id="Seg_596" s="T612">pro.h:S</ta>
            <ta e="T615" id="Seg_597" s="T614">v:pred</ta>
            <ta e="T618" id="Seg_598" s="T617">pro.h:S</ta>
            <ta e="T619" id="Seg_599" s="T618">v:pred</ta>
            <ta e="T628" id="Seg_600" s="T625">s:compl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T583" id="Seg_601" s="T582">np.h:Poss</ta>
            <ta e="T586" id="Seg_602" s="T585">np:Th</ta>
            <ta e="T587" id="Seg_603" s="T586">pro.h:A</ta>
            <ta e="T589" id="Seg_604" s="T588">np:L</ta>
            <ta e="T591" id="Seg_605" s="T590">adv:Time</ta>
            <ta e="T592" id="Seg_606" s="T591">0.3:Th</ta>
            <ta e="T593" id="Seg_607" s="T592">np:G</ta>
            <ta e="T596" id="Seg_608" s="T595">0.3:P</ta>
            <ta e="T597" id="Seg_609" s="T596">pro.h:A</ta>
            <ta e="T600" id="Seg_610" s="T599">np.h:R</ta>
            <ta e="T602" id="Seg_611" s="T601">0.3.h:A</ta>
            <ta e="T604" id="Seg_612" s="T603">pro.h:Poss</ta>
            <ta e="T605" id="Seg_613" s="T604">np:Th</ta>
            <ta e="T607" id="Seg_614" s="T606">0.3:P</ta>
            <ta e="T609" id="Seg_615" s="T608">pro.h:A</ta>
            <ta e="T613" id="Seg_616" s="T612">pro.h:A</ta>
            <ta e="T614" id="Seg_617" s="T613">pro.h:R</ta>
            <ta e="T617" id="Seg_618" s="T616">adv:Time</ta>
            <ta e="T618" id="Seg_619" s="T617">pro.h:A</ta>
            <ta e="T621" id="Seg_620" s="T620">adv:Time</ta>
            <ta e="T623" id="Seg_621" s="T622">pro.h:Poss</ta>
            <ta e="T627" id="Seg_622" s="T626">np:P</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T594" id="Seg_623" s="T593">RUS:gram</ta>
            <ta e="T595" id="Seg_624" s="T594">RUS:gram</ta>
            <ta e="T598" id="Seg_625" s="T597">RUS:gram</ta>
            <ta e="T608" id="Seg_626" s="T607">RUS:gram</ta>
            <ta e="T611" id="Seg_627" s="T610">RUS:gram</ta>
            <ta e="T612" id="Seg_628" s="T611">RUS:mod</ta>
            <ta e="T616" id="Seg_629" s="T615">RUS:disc</ta>
            <ta e="T620" id="Seg_630" s="T619">TURK:disc</ta>
            <ta e="T626" id="Seg_631" s="T625">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fr" tierref="fr">
            <ta e="T586" id="Seg_632" s="T581">У одного человека была лошадь.</ta>
            <ta e="T590" id="Seg_633" s="T586">Он ходил по горе.</ta>
            <ta e="T596" id="Seg_634" s="T590">Потом она [лошадь] упала и умерла.</ta>
            <ta e="T600" id="Seg_635" s="T596">Он стал звать людей.</ta>
            <ta e="T602" id="Seg_636" s="T600">Они прибежали.</ta>
            <ta e="T607" id="Seg_637" s="T602">«Моя лошадь упала, умерла!»</ta>
            <ta e="T615" id="Seg_638" s="T607">А они говорят: «А зачем ты нас позвал?»</ta>
            <ta e="T620" id="Seg_639" s="T615">«Ну, тогда [=иначе] вы бы все стали спрашивать.</ta>
            <ta e="T628" id="Seg_640" s="T620">А теперь вы своими [глазами] видели, что лошадь умерла».</ta>
            <ta e="T629" id="Seg_641" s="T628">Хватит.</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T586" id="Seg_642" s="T581">One man had a horse.</ta>
            <ta e="T590" id="Seg_643" s="T586">He was going around on a mountain.</ta>
            <ta e="T596" id="Seg_644" s="T590">Then it [the horse] fell down and died.</ta>
            <ta e="T600" id="Seg_645" s="T596">He started to yell [for] people.</ta>
            <ta e="T602" id="Seg_646" s="T600">They came running.</ta>
            <ta e="T607" id="Seg_647" s="T602">"My horse fell down, died!"</ta>
            <ta e="T615" id="Seg_648" s="T607">But they say: "But why did you call us?"</ta>
            <ta e="T620" id="Seg_649" s="T615">"Well, then [=otherwise] you would all ask.</ta>
            <ta e="T628" id="Seg_650" s="T620">Now you with your own [eyes] saw that my horse died."</ta>
            <ta e="T629" id="Seg_651" s="T628">Enough.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T586" id="Seg_652" s="T581">Ein Mann hatte ein Pferd.</ta>
            <ta e="T590" id="Seg_653" s="T586">Er ritt umher an einem Berg.</ta>
            <ta e="T596" id="Seg_654" s="T590">Dann fiel es [das Pferd] hin und starb.</ta>
            <ta e="T600" id="Seg_655" s="T596">Er fing an, [nach] Leuten zu rufen.</ta>
            <ta e="T602" id="Seg_656" s="T600">Sie kamen angerannt.</ta>
            <ta e="T607" id="Seg_657" s="T602">„Mein Pferd fiel hin, starb!“</ta>
            <ta e="T615" id="Seg_658" s="T607">Aber sie sagen: „Aber warum hast du uns hergerufen?“</ta>
            <ta e="T620" id="Seg_659" s="T615">„Also, dann [=ansonsten] würdet ihr alle fragen.</ta>
            <ta e="T628" id="Seg_660" s="T620">Nun habt ihr mit euren eigenen [Augen] gesehen, dass mein Pferd gestorben ist.“</ta>
            <ta e="T629" id="Seg_661" s="T628">Genug.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T628" id="Seg_662" s="T620">[GVY:] bostənziʔ 3Pl instead of 2Pl? || 'своими глазами'.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
