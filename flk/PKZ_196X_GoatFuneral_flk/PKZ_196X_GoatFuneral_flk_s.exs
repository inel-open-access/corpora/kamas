<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID4BBF0B6F-7F82-8518-C009-9A27D34221D7">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_GoatFuneral_flk.wav" />
         <referenced-file url="PKZ_196X_GoatFuneral_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_GoatFuneral_flk\PKZ_196X_GoatFuneral_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">377</ud-information>
            <ud-information attribute-name="# HIAT:w">250</ud-information>
            <ud-information attribute-name="# e">250</ud-information>
            <ud-information attribute-name="# HIAT:u">52</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.045" type="appl" />
         <tli id="T1" time="0.761" type="appl" />
         <tli id="T2" time="1.476" type="appl" />
         <tli id="T3" time="2.192" type="appl" />
         <tli id="T4" time="2.83" type="appl" />
         <tli id="T5" time="3.467" type="appl" />
         <tli id="T6" time="4.733260219157362" />
         <tli id="T7" time="5.591" type="appl" />
         <tli id="T8" time="6.478" type="appl" />
         <tli id="T9" time="7.364" type="appl" />
         <tli id="T10" time="8.506535267105344" />
         <tli id="T11" time="9.032" type="appl" />
         <tli id="T12" time="9.639" type="appl" />
         <tli id="T13" time="10.247" type="appl" />
         <tli id="T14" time="10.854" type="appl" />
         <tli id="T15" time="11.461" type="appl" />
         <tli id="T16" time="12.183" type="appl" />
         <tli id="T17" time="12.905" type="appl" />
         <tli id="T18" time="13.627" type="appl" />
         <tli id="T19" time="14.349" type="appl" />
         <tli id="T20" time="15.071" type="appl" />
         <tli id="T21" time="15.66" type="appl" />
         <tli id="T22" time="16.249" type="appl" />
         <tli id="T23" time="16.837" type="appl" />
         <tli id="T24" time="17.426" type="appl" />
         <tli id="T25" time="18.015" type="appl" />
         <tli id="T26" time="18.707" type="appl" />
         <tli id="T27" time="19.388" type="appl" />
         <tli id="T28" time="20.53301616197842" />
         <tli id="T29" time="21.866" type="appl" />
         <tli id="T30" time="23.118" type="appl" />
         <tli id="T31" time="24.506288120595023" />
         <tli id="T32" time="25.696" type="appl" />
         <tli id="T33" time="27.01" type="appl" />
         <tli id="T34" time="28.50622633396744" />
         <tli id="T35" time="29.953" type="appl" />
         <tli id="T36" time="31.058" type="appl" />
         <tli id="T37" time="32.162" type="appl" />
         <tli id="T38" time="33.266" type="appl" />
         <tli id="T39" time="34.37" type="appl" />
         <tli id="T40" time="35.475" type="appl" />
         <tli id="T41" time="36.96609565525011" />
         <tli id="T42" time="38.132" type="appl" />
         <tli id="T43" time="39.223" type="appl" />
         <tli id="T44" time="40.313" type="appl" />
         <tli id="T45" time="41.403" type="appl" />
         <tli id="T46" time="42.493" type="appl" />
         <tli id="T47" time="43.584" type="appl" />
         <tli id="T48" time="44.77930829870423" />
         <tli id="T49" time="45.783" type="appl" />
         <tli id="T50" time="46.707" type="appl" />
         <tli id="T51" time="47.632" type="appl" />
         <tli id="T52" time="48.556" type="appl" />
         <tli id="T53" time="49.65923291901859" />
         <tli id="T54" time="50.115" type="appl" />
         <tli id="T55" time="50.751" type="appl" />
         <tli id="T56" time="51.386" type="appl" />
         <tli id="T57" time="52.021" type="appl" />
         <tli id="T58" time="52.657" type="appl" />
         <tli id="T59" time="53.292" type="appl" />
         <tli id="T60" time="53.927" type="appl" />
         <tli id="T61" time="54.563" type="appl" />
         <tli id="T62" time="55.57914147480977" />
         <tli id="T63" time="56.42" type="appl" />
         <tli id="T64" time="57.09" type="appl" />
         <tli id="T65" time="57.761" type="appl" />
         <tli id="T66" time="58.432" type="appl" />
         <tli id="T67" time="59.102" type="appl" />
         <tli id="T68" time="59.773" type="appl" />
         <tli id="T69" time="60.577" type="appl" />
         <tli id="T70" time="61.267" type="appl" />
         <tli id="T71" time="61.958" type="appl" />
         <tli id="T72" time="62.648" type="appl" />
         <tli id="T73" time="63.339" type="appl" />
         <tli id="T74" time="64.03" type="appl" />
         <tli id="T75" time="64.72" type="appl" />
         <tli id="T76" time="65.411" type="appl" />
         <tli id="T77" time="66.382" type="appl" />
         <tli id="T78" time="67.352" type="appl" />
         <tli id="T79" time="68.323" type="appl" />
         <tli id="T80" time="69.293" type="appl" />
         <tli id="T81" time="70.264" type="appl" />
         <tli id="T82" time="71.069" type="appl" />
         <tli id="T83" time="71.775" type="appl" />
         <tli id="T84" time="72.48" type="appl" />
         <tli id="T85" time="73.186" type="appl" />
         <tli id="T86" time="73.892" type="appl" />
         <tli id="T87" time="74.598" type="appl" />
         <tli id="T88" time="75.374" type="appl" />
         <tli id="T89" time="76.016" type="appl" />
         <tli id="T90" time="76.657" type="appl" />
         <tli id="T91" time="77.299" type="appl" />
         <tli id="T92" time="78.0" type="appl" />
         <tli id="T93" time="78.701" type="appl" />
         <tli id="T94" time="79.402" type="appl" />
         <tli id="T95" time="80.103" type="appl" />
         <tli id="T96" time="80.804" type="appl" />
         <tli id="T97" time="82.2587293580038" />
         <tli id="T98" time="83.253" type="appl" />
         <tli id="T99" time="84.16" type="appl" />
         <tli id="T100" time="85.068" type="appl" />
         <tli id="T101" time="85.976" type="appl" />
         <tli id="T102" time="86.968" type="appl" />
         <tli id="T103" time="87.959" type="appl" />
         <tli id="T104" time="88.951" type="appl" />
         <tli id="T105" time="89.943" type="appl" />
         <tli id="T106" time="90.934" type="appl" />
         <tli id="T107" time="91.926" type="appl" />
         <tli id="T108" time="93.174" type="appl" />
         <tli id="T109" time="94.252" type="appl" />
         <tli id="T110" time="95.329" type="appl" />
         <tli id="T111" time="96.248" type="appl" />
         <tli id="T112" time="96.852" type="appl" />
         <tli id="T113" time="97.455" type="appl" />
         <tli id="T114" time="98.059" type="appl" />
         <tli id="T115" time="98.662" type="appl" />
         <tli id="T116" time="99.528" type="appl" />
         <tli id="T117" time="101.13843772512162" />
         <tli id="T118" time="102.553" type="appl" />
         <tli id="T119" time="103.067" type="appl" />
         <tli id="T120" time="103.582" type="appl" />
         <tli id="T121" time="104.096" type="appl" />
         <tli id="T122" time="104.61" type="appl" />
         <tli id="T123" time="105.129" type="appl" />
         <tli id="T124" time="105.649" type="appl" />
         <tli id="T125" time="106.168" type="appl" />
         <tli id="T126" time="106.688" type="appl" />
         <tli id="T127" time="108.11166334376755" />
         <tli id="T128" time="108.968" type="appl" />
         <tli id="T129" time="109.697" type="appl" />
         <tli id="T130" time="110.426" type="appl" />
         <tli id="T131" time="111.155" type="appl" />
         <tli id="T132" time="112.2649325219859" />
         <tli id="T133" time="112.98" type="appl" />
         <tli id="T134" time="113.562" type="appl" />
         <tli id="T135" time="114.145" type="appl" />
         <tli id="T136" time="115.062" type="appl" />
         <tli id="T137" time="115.954" type="appl" />
         <tli id="T138" time="116.847" type="appl" />
         <tli id="T139" time="117.739" type="appl" />
         <tli id="T140" time="118.63767262460216" />
         <tli id="T141" time="119.271" type="appl" />
         <tli id="T142" time="119.911" type="appl" />
         <tli id="T143" time="120.551" type="appl" />
         <tli id="T144" time="121.191" type="appl" />
         <tli id="T145" time="121.83" type="appl" />
         <tli id="T146" time="122.47" type="appl" />
         <tli id="T147" time="123.11" type="appl" />
         <tli id="T148" time="123.75" type="appl" />
         <tli id="T149" time="124.39" type="appl" />
         <tli id="T150" time="124.941" type="appl" />
         <tli id="T151" time="125.492" type="appl" />
         <tli id="T152" time="126.043" type="appl" />
         <tli id="T153" time="126.594" type="appl" />
         <tli id="T154" time="127.11" type="appl" />
         <tli id="T155" time="127.625" type="appl" />
         <tli id="T156" time="128.141" type="appl" />
         <tli id="T157" time="128.657" type="appl" />
         <tli id="T158" time="129.172" type="appl" />
         <tli id="T159" time="129.85132753344664" />
         <tli id="T160" time="130.313" type="appl" />
         <tli id="T161" time="130.928" type="appl" />
         <tli id="T162" time="131.543" type="appl" />
         <tli id="T163" time="132.157" type="appl" />
         <tli id="T164" time="132.772" type="appl" />
         <tli id="T165" time="134.4245902240691" />
         <tli id="T166" time="135.177" type="appl" />
         <tli id="T167" time="136.188" type="appl" />
         <tli id="T168" time="137.2" type="appl" />
         <tli id="T169" time="138.211" type="appl" />
         <tli id="T170" time="139.222" type="appl" />
         <tli id="T171" time="140.52449599946203" />
         <tli id="T172" time="141.326" type="appl" />
         <tli id="T173" time="142.258" type="appl" />
         <tli id="T174" time="143.19" type="appl" />
         <tli id="T175" time="144.123" type="appl" />
         <tli id="T176" time="145.056" type="appl" />
         <tli id="T177" time="146.91106401348" />
         <tli id="T178" time="148.204" type="appl" />
         <tli id="T179" time="149.407" type="appl" />
         <tli id="T180" time="150.609" type="appl" />
         <tli id="T181" time="151.812" type="appl" />
         <tli id="T182" time="153.014" type="appl" />
         <tli id="T183" time="154.217" type="appl" />
         <tli id="T184" time="155.419" type="appl" />
         <tli id="T185" time="155.951" type="appl" />
         <tli id="T186" time="156.482" type="appl" />
         <tli id="T187" time="157.27757054980353" />
         <tli id="T188" time="157.804" type="appl" />
         <tli id="T189" time="158.226" type="appl" />
         <tli id="T190" time="158.647" type="appl" />
         <tli id="T191" time="159.069" type="appl" />
         <tli id="T192" time="159.49" type="appl" />
         <tli id="T193" time="160.218" type="appl" />
         <tli id="T194" time="160.936" type="appl" />
         <tli id="T195" time="161.653" type="appl" />
         <tli id="T196" time="162.73081964736792" />
         <tli id="T197" time="164.258" type="appl" />
         <tli id="T198" time="166.145" type="appl" />
         <tli id="T199" time="166.778" type="appl" />
         <tli id="T200" time="167.411" type="appl" />
         <tli id="T201" time="168.043" type="appl" />
         <tli id="T202" time="168.676" type="appl" />
         <tli id="T203" time="169.309" type="appl" />
         <tli id="T204" time="170.417" type="appl" />
         <tli id="T205" time="171.524" type="appl" />
         <tli id="T206" time="172.632" type="appl" />
         <tli id="T207" time="173.739" type="appl" />
         <tli id="T208" time="174.847" type="appl" />
         <tli id="T209" time="175.954" type="appl" />
         <tli id="T210" time="177.062" type="appl" />
         <tli id="T211" time="178.169" type="appl" />
         <tli id="T212" time="179.277" type="appl" />
         <tli id="T213" time="180.27" type="appl" />
         <tli id="T214" time="181.8505243072881" />
         <tli id="T215" time="182.743" type="appl" />
         <tli id="T216" time="184.224" type="appl" />
         <tli id="T217" time="185.704" type="appl" />
         <tli id="T218" time="186.585" type="appl" />
         <tli id="T219" time="187.352" type="appl" />
         <tli id="T220" time="188.30375795819558" />
         <tli id="T221" time="189.183" type="appl" />
         <tli id="T222" time="190.085" type="appl" />
         <tli id="T223" time="190.988" type="appl" />
         <tli id="T224" time="191.89" type="appl" />
         <tli id="T225" time="193.27034790646633" />
         <tli id="T226" time="193.949" type="appl" />
         <tli id="T227" time="194.561" type="appl" />
         <tli id="T228" time="195.174" type="appl" />
         <tli id="T229" time="195.786" type="appl" />
         <tli id="T230" time="196.398" type="appl" />
         <tli id="T231" time="197.01" type="appl" />
         <tli id="T232" time="198.074" type="appl" />
         <tli id="T233" time="198.979" type="appl" />
         <tli id="T234" time="199.885" type="appl" />
         <tli id="T235" time="200.79" type="appl" />
         <tli id="T236" time="201.876881628906" />
         <tli id="T237" time="202.383" type="appl" />
         <tli id="T238" time="202.941" type="appl" />
         <tli id="T239" time="203.499" type="appl" />
         <tli id="T240" time="204.057" type="appl" />
         <tli id="T241" time="205.056" type="appl" />
         <tli id="T242" time="205.9" type="appl" />
         <tli id="T243" time="206.744" type="appl" />
         <tli id="T244" time="208.31011558874664" />
         <tli id="T245" time="209.045" type="appl" />
         <tli id="T246" time="209.727" type="appl" />
         <tli id="T247" time="210.408" type="appl" />
         <tli id="T248" time="211.09" type="appl" />
         <tli id="T249" time="211.772" type="appl" />
         <tli id="T250" time="212.454" type="appl" />
         <tli id="T251" time="213.753" type="appl" />
         <tli id="T252" time="213.77" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T251" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Amnolaʔbi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">nüke</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">büzʼe</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T6" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Dĭzeŋ</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">ĭmbidə</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">nagobi</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_26" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">Dĭ</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">büzʼe</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">kürleʔpi</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">jamaʔi</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_41" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">Da</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">sadarbi</ts>
                  <nts id="Seg_47" n="HIAT:ip">,</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_50" n="HIAT:w" s="T12">dărəʔ</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_53" n="HIAT:w" s="T13">dĭzeŋ</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">amnobiʔi</ts>
                  <nts id="Seg_57" n="HIAT:ip">.</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_60" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_62" n="HIAT:w" s="T15">A</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_64" n="HIAT:ip">(</nts>
                  <ts e="T17" id="Seg_66" n="HIAT:w" s="T16">dĭ=</ts>
                  <nts id="Seg_67" n="HIAT:ip">)</nts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_70" n="HIAT:w" s="T17">dĭzeŋgən</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_73" n="HIAT:w" s="T18">ĭmbidə</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_76" n="HIAT:w" s="T19">nagobi</ts>
                  <nts id="Seg_77" n="HIAT:ip">.</nts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_80" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_82" n="HIAT:w" s="T20">Tolʼko</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_84" n="HIAT:ip">(</nts>
                  <ts e="T22" id="Seg_86" n="HIAT:w" s="T21">ădʼin</ts>
                  <nts id="Seg_87" n="HIAT:ip">)</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_90" n="HIAT:w" s="T22">onʼiʔ</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_93" n="HIAT:w" s="T23">poʔto</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_96" n="HIAT:w" s="T24">ibi</ts>
                  <nts id="Seg_97" n="HIAT:ip">.</nts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_100" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_102" n="HIAT:w" s="T25">Da</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_105" n="HIAT:w" s="T26">kambi</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_108" n="HIAT:w" s="T27">onʼiʔ</ts>
                  <nts id="Seg_109" n="HIAT:ip">.</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_112" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_114" n="HIAT:w" s="T28">Kambi</ts>
                  <nts id="Seg_115" n="HIAT:ip">,</nts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_118" n="HIAT:w" s="T29">dʼijenə</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_121" n="HIAT:w" s="T30">kürzittə</ts>
                  <nts id="Seg_122" n="HIAT:ip">.</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_125" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_127" n="HIAT:w" s="T31">Kubaʔi</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_130" n="HIAT:w" s="T32">kürbi</ts>
                  <nts id="Seg_131" n="HIAT:ip">,</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_134" n="HIAT:w" s="T33">kürbi</ts>
                  <nts id="Seg_135" n="HIAT:ip">.</nts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_138" n="HIAT:u" s="T34">
                  <nts id="Seg_139" n="HIAT:ip">(</nts>
                  <ts e="T35" id="Seg_141" n="HIAT:w" s="T34">A=</ts>
                  <nts id="Seg_142" n="HIAT:ip">)</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_145" n="HIAT:w" s="T35">A</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_148" n="HIAT:w" s="T36">poʔto</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_151" n="HIAT:w" s="T37">mĭlleʔbə</ts>
                  <nts id="Seg_152" n="HIAT:ip">,</nts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_155" n="HIAT:w" s="T38">dĭn</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_158" n="HIAT:w" s="T39">noʔ</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_161" n="HIAT:w" s="T40">amnaʔbə</ts>
                  <nts id="Seg_162" n="HIAT:ip">.</nts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_165" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_167" n="HIAT:w" s="T41">Dĭgəttə</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_170" n="HIAT:w" s="T42">mĭmbi</ts>
                  <nts id="Seg_171" n="HIAT:ip">,</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_174" n="HIAT:w" s="T43">mĭmbi</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_177" n="HIAT:w" s="T44">dĭ</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_180" n="HIAT:w" s="T45">ujutsiʔ</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_183" n="HIAT:w" s="T46">bar</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_186" n="HIAT:w" s="T47">saʔməluʔpi</ts>
                  <nts id="Seg_187" n="HIAT:ip">.</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_190" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_192" n="HIAT:w" s="T48">Kuliat</ts>
                  <nts id="Seg_193" n="HIAT:ip">,</nts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_196" n="HIAT:w" s="T49">dĭn</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_199" n="HIAT:w" s="T50">iʔbəlaʔbə</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_202" n="HIAT:w" s="T51">iʔgö</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_205" n="HIAT:w" s="T52">aktʼa</ts>
                  <nts id="Seg_206" n="HIAT:ip">.</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_209" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_211" n="HIAT:w" s="T53">Dĭgəttə</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_214" n="HIAT:w" s="T54">büzʼe</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_217" n="HIAT:w" s="T55">šobi</ts>
                  <nts id="Seg_218" n="HIAT:ip">,</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_220" n="HIAT:ip">(</nts>
                  <ts e="T57" id="Seg_222" n="HIAT:w" s="T56">kuliat=</ts>
                  <nts id="Seg_223" n="HIAT:ip">)</nts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_226" n="HIAT:w" s="T57">kubi</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_229" n="HIAT:w" s="T58">aktʼa</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_232" n="HIAT:w" s="T59">i</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_235" n="HIAT:w" s="T60">deʔpi</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_238" n="HIAT:w" s="T61">maːndə</ts>
                  <nts id="Seg_239" n="HIAT:ip">.</nts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_242" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_244" n="HIAT:w" s="T62">Dĭgəttə</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_247" n="HIAT:w" s="T63">măndə:</ts>
                  <nts id="Seg_248" n="HIAT:ip">"</nts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_251" n="HIAT:w" s="T64">No</ts>
                  <nts id="Seg_252" n="HIAT:ip">,</nts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_255" n="HIAT:w" s="T65">miʔnʼibeʔ</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_258" n="HIAT:w" s="T66">kudaj</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_261" n="HIAT:w" s="T67">mĭbi</ts>
                  <nts id="Seg_262" n="HIAT:ip">.</nts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_265" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_267" n="HIAT:w" s="T68">No</ts>
                  <nts id="Seg_268" n="HIAT:ip">,</nts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_271" n="HIAT:w" s="T69">kudaj</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_274" n="HIAT:w" s="T70">nʼe</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_277" n="HIAT:w" s="T71">kudaj</ts>
                  <nts id="Seg_278" n="HIAT:ip">,</nts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_281" n="HIAT:w" s="T72">dĭ</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_284" n="HIAT:w" s="T73">miʔnʼibeʔ</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_287" n="HIAT:w" s="T74">kubi</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_290" n="HIAT:w" s="T75">poʔto</ts>
                  <nts id="Seg_291" n="HIAT:ip">.</nts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_294" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_296" n="HIAT:w" s="T76">Dĭm</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_299" n="HIAT:w" s="T77">nada</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_302" n="HIAT:w" s="T78">tože</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_305" n="HIAT:w" s="T79">amzittə</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_308" n="HIAT:w" s="T80">mĭzittə</ts>
                  <nts id="Seg_309" n="HIAT:ip">.</nts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_312" n="HIAT:u" s="T81">
                  <nts id="Seg_313" n="HIAT:ip">(</nts>
                  <ts e="T82" id="Seg_315" n="HIAT:w" s="T81">Dĭzem-</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_318" n="HIAT:w" s="T82">dĭ-</ts>
                  <nts id="Seg_319" n="HIAT:ip">)</nts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_322" n="HIAT:w" s="T83">Dĭzeŋ</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_325" n="HIAT:w" s="T84">dĭm</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_328" n="HIAT:w" s="T85">tože</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_330" n="HIAT:ip">(</nts>
                  <ts e="T87" id="Seg_332" n="HIAT:w" s="T86">ajərbiʔi</ts>
                  <nts id="Seg_333" n="HIAT:ip">)</nts>
                  <nts id="Seg_334" n="HIAT:ip">.</nts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_337" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_339" n="HIAT:w" s="T87">I</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_342" n="HIAT:w" s="T88">bădəbiʔi</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_345" n="HIAT:w" s="T89">bostə</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_348" n="HIAT:w" s="T90">bar</ts>
                  <nts id="Seg_349" n="HIAT:ip">.</nts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_352" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_354" n="HIAT:w" s="T91">Dĭgəttə</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_357" n="HIAT:w" s="T92">ĭzemnuʔpi</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_360" n="HIAT:w" s="T93">dĭ</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_363" n="HIAT:w" s="T94">poʔto</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_366" n="HIAT:w" s="T95">i</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_369" n="HIAT:w" s="T96">külaːmbi</ts>
                  <nts id="Seg_370" n="HIAT:ip">.</nts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_373" n="HIAT:u" s="T97">
                  <nts id="Seg_374" n="HIAT:ip">"</nts>
                  <ts e="T98" id="Seg_376" n="HIAT:w" s="T97">Nada</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_378" n="HIAT:ip">(</nts>
                  <ts e="T99" id="Seg_380" n="HIAT:w" s="T98">dĭze-</ts>
                  <nts id="Seg_381" n="HIAT:ip">)</nts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_384" n="HIAT:w" s="T99">dĭm</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_387" n="HIAT:w" s="T100">barəʔsittə</ts>
                  <nts id="Seg_388" n="HIAT:ip">"</nts>
                  <nts id="Seg_389" n="HIAT:ip">.</nts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_392" n="HIAT:u" s="T101">
                  <nts id="Seg_393" n="HIAT:ip">"</nts>
                  <ts e="T102" id="Seg_395" n="HIAT:w" s="T101">Nʼe</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_398" n="HIAT:w" s="T102">nada</ts>
                  <nts id="Seg_399" n="HIAT:ip">,</nts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_402" n="HIAT:w" s="T103">nada</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_404" n="HIAT:ip">(</nts>
                  <ts e="T105" id="Seg_406" n="HIAT:w" s="T104">dĭm=</ts>
                  <nts id="Seg_407" n="HIAT:ip">)</nts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_410" n="HIAT:w" s="T105">kanzittə</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_413" n="HIAT:w" s="T106">abəstə</ts>
                  <nts id="Seg_414" n="HIAT:ip">.</nts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_417" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_419" n="HIAT:w" s="T107">Da</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_422" n="HIAT:w" s="T108">jakšəŋ</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_425" n="HIAT:w" s="T109">enzittə</ts>
                  <nts id="Seg_426" n="HIAT:ip">"</nts>
                  <nts id="Seg_427" n="HIAT:ip">.</nts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T117" id="Seg_430" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_432" n="HIAT:w" s="T110">Dĭgəttə</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_435" n="HIAT:w" s="T111">kambi</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_438" n="HIAT:w" s="T112">abəstə</ts>
                  <nts id="Seg_439" n="HIAT:ip">,</nts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_442" n="HIAT:w" s="T113">abəs</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_445" n="HIAT:w" s="T114">măndə:</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_447" n="HIAT:ip">"</nts>
                  <ts e="T116" id="Seg_449" n="HIAT:w" s="T115">Šindi</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_452" n="HIAT:w" s="T116">poʔto</ts>
                  <nts id="Seg_453" n="HIAT:ip">?</nts>
                  <nts id="Seg_454" n="HIAT:ip">"</nts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_457" n="HIAT:u" s="T117">
                  <ts e="T118" id="Seg_459" n="HIAT:w" s="T117">Nüjleʔbə</ts>
                  <nts id="Seg_460" n="HIAT:ip">.</nts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_463" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_465" n="HIAT:w" s="T118">I</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_468" n="HIAT:w" s="T119">davaj</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_471" n="HIAT:w" s="T120">dĭm</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_474" n="HIAT:w" s="T121">münörzittə</ts>
                  <nts id="Seg_475" n="HIAT:ip">.</nts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_478" n="HIAT:u" s="T122">
                  <nts id="Seg_479" n="HIAT:ip">"</nts>
                  <nts id="Seg_480" n="HIAT:ip">(</nts>
                  <ts e="T123" id="Seg_482" n="HIAT:w" s="T122">Da-</ts>
                  <nts id="Seg_483" n="HIAT:ip">)</nts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_486" n="HIAT:w" s="T123">Dĭm</ts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_489" n="HIAT:w" s="T125">jakšə</ts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_492" n="HIAT:w" s="T126">ibi</ts>
                  <nts id="Seg_493" n="HIAT:ip">.</nts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T132" id="Seg_496" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_498" n="HIAT:w" s="T127">Tănan</ts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_501" n="HIAT:w" s="T128">aktʼa</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_504" n="HIAT:w" s="T129">maːbi</ts>
                  <nts id="Seg_505" n="HIAT:ip">,</nts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_508" n="HIAT:w" s="T130">šide</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_511" n="HIAT:w" s="T131">bieʔ</ts>
                  <nts id="Seg_512" n="HIAT:ip">"</nts>
                  <nts id="Seg_513" n="HIAT:ip">.</nts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T135" id="Seg_516" n="HIAT:u" s="T132">
                  <ts e="T133" id="Seg_518" n="HIAT:w" s="T132">Dĭgəttə:</ts>
                  <nts id="Seg_519" n="HIAT:ip">"</nts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_522" n="HIAT:w" s="T133">No</ts>
                  <nts id="Seg_523" n="HIAT:ip">,</nts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_526" n="HIAT:w" s="T134">kanaʔ</ts>
                  <nts id="Seg_527" n="HIAT:ip">!</nts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_530" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_532" n="HIAT:w" s="T135">Ĭmbi</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_535" n="HIAT:w" s="T136">tăn</ts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_538" n="HIAT:w" s="T137">măna</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_541" n="HIAT:w" s="T138">ej</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_544" n="HIAT:w" s="T139">nörbəbiel</ts>
                  <nts id="Seg_545" n="HIAT:ip">?</nts>
                  <nts id="Seg_546" n="HIAT:ip">"</nts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_549" n="HIAT:u" s="T140">
                  <ts e="T141" id="Seg_551" n="HIAT:w" s="T140">Dĭ</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_554" n="HIAT:w" s="T141">kambi</ts>
                  <nts id="Seg_555" n="HIAT:ip">;</nts>
                  <nts id="Seg_556" n="HIAT:ip">"</nts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_559" n="HIAT:w" s="T142">Kanaʔ</ts>
                  <nts id="Seg_560" n="HIAT:ip">,</nts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_563" n="HIAT:w" s="T143">ami</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_565" n="HIAT:ip">(</nts>
                  <ts e="T145" id="Seg_567" n="HIAT:w" s="T144">a-</ts>
                  <nts id="Seg_568" n="HIAT:ip">)</nts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_571" n="HIAT:w" s="T145">baška</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_574" n="HIAT:w" s="T146">abəsdə</ts>
                  <nts id="Seg_575" n="HIAT:ip">"</nts>
                  <nts id="Seg_576" n="HIAT:ip">,</nts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_579" n="HIAT:w" s="T147">dibər</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_582" n="HIAT:w" s="T148">kambi</ts>
                  <nts id="Seg_583" n="HIAT:ip">.</nts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_586" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_588" n="HIAT:w" s="T149">Dĭʔnə</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_590" n="HIAT:ip">(</nts>
                  <ts e="T151" id="Seg_592" n="HIAT:w" s="T150">š-</ts>
                  <nts id="Seg_593" n="HIAT:ip">)</nts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_596" n="HIAT:w" s="T151">tože</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_599" n="HIAT:w" s="T152">nörbəbi</ts>
                  <nts id="Seg_600" n="HIAT:ip">.</nts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T159" id="Seg_603" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_605" n="HIAT:w" s="T153">Dĭ</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_608" n="HIAT:w" s="T154">tože</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_611" n="HIAT:w" s="T155">dĭm</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_614" n="HIAT:w" s="T156">kabarbi</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_617" n="HIAT:w" s="T157">da</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_620" n="HIAT:w" s="T158">münörluʔpi</ts>
                  <nts id="Seg_621" n="HIAT:ip">.</nts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T165" id="Seg_624" n="HIAT:u" s="T159">
                  <nts id="Seg_625" n="HIAT:ip">"</nts>
                  <ts e="T160" id="Seg_627" n="HIAT:w" s="T159">Iʔ</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_630" n="HIAT:w" s="T160">münöraʔ</ts>
                  <nts id="Seg_631" n="HIAT:ip">,</nts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_634" n="HIAT:w" s="T161">dĭ</ts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_637" n="HIAT:w" s="T162">tănan</ts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_640" n="HIAT:w" s="T163">aktʼa</ts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_643" n="HIAT:w" s="T164">maːbi</ts>
                  <nts id="Seg_644" n="HIAT:ip">.</nts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T171" id="Seg_647" n="HIAT:u" s="T165">
                  <nts id="Seg_648" n="HIAT:ip">(</nts>
                  <ts e="T166" id="Seg_650" n="HIAT:w" s="T165">Ši-</ts>
                  <nts id="Seg_651" n="HIAT:ip">)</nts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_654" n="HIAT:w" s="T166">Šide</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_657" n="HIAT:w" s="T167">bieʔ</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_660" n="HIAT:w" s="T168">aktʼa</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_663" n="HIAT:w" s="T169">tănan</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_666" n="HIAT:w" s="T170">maːbi</ts>
                  <nts id="Seg_667" n="HIAT:ip">"</nts>
                  <nts id="Seg_668" n="HIAT:ip">.</nts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T177" id="Seg_671" n="HIAT:u" s="T171">
                  <ts e="T172" id="Seg_673" n="HIAT:w" s="T171">Dĭgəttə</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_676" n="HIAT:w" s="T172">dĭ:</ts>
                  <nts id="Seg_677" n="HIAT:ip">"</nts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_680" n="HIAT:w" s="T173">Ĭmbi</ts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_683" n="HIAT:w" s="T174">tăn</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_686" n="HIAT:w" s="T175">ej</ts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_689" n="HIAT:w" s="T176">mămbial</ts>
                  <nts id="Seg_690" n="HIAT:ip">?</nts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_693" n="HIAT:u" s="T177">
                  <ts e="T178" id="Seg_695" n="HIAT:w" s="T177">Kanaʔ</ts>
                  <nts id="Seg_696" n="HIAT:ip">,</nts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_699" n="HIAT:w" s="T178">pušaj</ts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_701" n="HIAT:ip">(</nts>
                  <ts e="T180" id="Seg_703" n="HIAT:w" s="T179">tʼegermaʔdə</ts>
                  <nts id="Seg_704" n="HIAT:ip">)</nts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_707" n="HIAT:w" s="T180">sʼaləj</ts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_710" n="HIAT:w" s="T181">da</ts>
                  <nts id="Seg_711" n="HIAT:ip">,</nts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_714" n="HIAT:w" s="T182">küzürləj</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_717" n="HIAT:w" s="T183">koŋgoroziʔ</ts>
                  <nts id="Seg_718" n="HIAT:ip">"</nts>
                  <nts id="Seg_719" n="HIAT:ip">.</nts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T187" id="Seg_722" n="HIAT:u" s="T184">
                  <ts e="T185" id="Seg_724" n="HIAT:w" s="T184">Dĭ</ts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_727" n="HIAT:w" s="T185">dibər</ts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_730" n="HIAT:w" s="T186">šobi</ts>
                  <nts id="Seg_731" n="HIAT:ip">.</nts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T192" id="Seg_734" n="HIAT:u" s="T187">
                  <ts e="T188" id="Seg_736" n="HIAT:w" s="T187">Dĭ</ts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_739" n="HIAT:w" s="T188">tože</ts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_742" n="HIAT:w" s="T189">dĭm</ts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_745" n="HIAT:w" s="T190">davaj</ts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_747" n="HIAT:ip">(</nts>
                  <ts e="T192" id="Seg_749" n="HIAT:w" s="T191">münö-</ts>
                  <nts id="Seg_750" n="HIAT:ip">)</nts>
                  <nts id="Seg_751" n="HIAT:ip">.</nts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T196" id="Seg_754" n="HIAT:u" s="T192">
                  <nts id="Seg_755" n="HIAT:ip">"</nts>
                  <ts e="T193" id="Seg_757" n="HIAT:w" s="T192">Tănan</ts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_760" n="HIAT:w" s="T193">bieʔ</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_763" n="HIAT:w" s="T194">sumna</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_766" n="HIAT:w" s="T195">mabi</ts>
                  <nts id="Seg_767" n="HIAT:ip">.</nts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T198" id="Seg_770" n="HIAT:u" s="T196">
                  <ts e="T197" id="Seg_772" n="HIAT:w" s="T196">Dĭ</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_775" n="HIAT:w" s="T197">poʔto</ts>
                  <nts id="Seg_776" n="HIAT:ip">"</nts>
                  <nts id="Seg_777" n="HIAT:ip">.</nts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T203" id="Seg_780" n="HIAT:u" s="T198">
                  <nts id="Seg_781" n="HIAT:ip">"</nts>
                  <ts e="T199" id="Seg_783" n="HIAT:w" s="T198">Ĭmbi</ts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_786" n="HIAT:w" s="T199">tăn</ts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_789" n="HIAT:w" s="T200">măna</ts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_792" n="HIAT:w" s="T201">ej</ts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_795" n="HIAT:w" s="T202">nörbəbiel</ts>
                  <nts id="Seg_796" n="HIAT:ip">?</nts>
                  <nts id="Seg_797" n="HIAT:ip">"</nts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T212" id="Seg_800" n="HIAT:u" s="T203">
                  <ts e="T204" id="Seg_802" n="HIAT:w" s="T203">Dĭgəttə</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_805" n="HIAT:w" s="T204">suʔməluʔpi</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_808" n="HIAT:w" s="T205">tʼegermaʔdə</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_811" n="HIAT:w" s="T206">i</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_814" n="HIAT:w" s="T207">davaj</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_816" n="HIAT:ip">(</nts>
                  <ts e="T209" id="Seg_818" n="HIAT:w" s="T208">koŋgorosʼtə=</ts>
                  <nts id="Seg_819" n="HIAT:ip">)</nts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_822" n="HIAT:w" s="T209">koŋgorozʼiʔ</ts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_825" n="HIAT:w" s="T210">bar</ts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_828" n="HIAT:w" s="T211">küzürzittə</ts>
                  <nts id="Seg_829" n="HIAT:ip">.</nts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T214" id="Seg_832" n="HIAT:u" s="T212">
                  <ts e="T213" id="Seg_834" n="HIAT:w" s="T212">Dĭgəttə</ts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_837" n="HIAT:w" s="T213">kambiʔi</ts>
                  <nts id="Seg_838" n="HIAT:ip">.</nts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T217" id="Seg_841" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_843" n="HIAT:w" s="T214">Dĭ</ts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_846" n="HIAT:w" s="T215">poʔto</ts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_849" n="HIAT:w" s="T216">kumbiʔi</ts>
                  <nts id="Seg_850" n="HIAT:ip">.</nts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T220" id="Seg_853" n="HIAT:u" s="T217">
                  <ts e="T218" id="Seg_855" n="HIAT:w" s="T217">I</ts>
                  <nts id="Seg_856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_858" n="HIAT:w" s="T218">dʼünə</ts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_861" n="HIAT:w" s="T219">kămnəbiʔi</ts>
                  <nts id="Seg_862" n="HIAT:ip">.</nts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T225" id="Seg_865" n="HIAT:u" s="T220">
                  <ts e="T221" id="Seg_867" n="HIAT:w" s="T220">I</ts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_870" n="HIAT:w" s="T221">iššo</ts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_873" n="HIAT:w" s="T222">urgo</ts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_876" n="HIAT:w" s="T223">abəs</ts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_879" n="HIAT:w" s="T224">tĭmluʔpi</ts>
                  <nts id="Seg_880" n="HIAT:ip">.</nts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T231" id="Seg_883" n="HIAT:u" s="T225">
                  <ts e="T226" id="Seg_885" n="HIAT:w" s="T225">I</ts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_888" n="HIAT:w" s="T226">kăštəbi</ts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_891" n="HIAT:w" s="T227">dĭ</ts>
                  <nts id="Seg_892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_894" n="HIAT:w" s="T228">büzʼem</ts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_897" n="HIAT:w" s="T229">i</ts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_900" n="HIAT:w" s="T230">abəstə</ts>
                  <nts id="Seg_901" n="HIAT:ip">.</nts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T236" id="Seg_904" n="HIAT:u" s="T231">
                  <nts id="Seg_905" n="HIAT:ip">"</nts>
                  <ts e="T232" id="Seg_907" n="HIAT:w" s="T231">Ĭmbi</ts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_910" n="HIAT:w" s="T232">šiʔ</ts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_913" n="HIAT:w" s="T233">poʔto</ts>
                  <nts id="Seg_914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_916" n="HIAT:w" s="T234">embileʔ</ts>
                  <nts id="Seg_917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_919" n="HIAT:w" s="T235">dʼünə</ts>
                  <nts id="Seg_920" n="HIAT:ip">"</nts>
                  <nts id="Seg_921" n="HIAT:ip">.</nts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T240" id="Seg_924" n="HIAT:u" s="T236">
                  <nts id="Seg_925" n="HIAT:ip">"</nts>
                  <ts e="T237" id="Seg_927" n="HIAT:w" s="T236">Da</ts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_930" n="HIAT:w" s="T237">dĭ</ts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_933" n="HIAT:w" s="T238">jakšə</ts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_936" n="HIAT:w" s="T239">ibi</ts>
                  <nts id="Seg_937" n="HIAT:ip">.</nts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T244" id="Seg_940" n="HIAT:u" s="T240">
                  <ts e="T241" id="Seg_942" n="HIAT:w" s="T240">Tănan</ts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_945" n="HIAT:w" s="T241">iʔgö</ts>
                  <nts id="Seg_946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_948" n="HIAT:w" s="T242">aktʼa</ts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_951" n="HIAT:w" s="T243">maːluʔpi</ts>
                  <nts id="Seg_952" n="HIAT:ip">"</nts>
                  <nts id="Seg_953" n="HIAT:ip">.</nts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T250" id="Seg_956" n="HIAT:u" s="T244">
                  <ts e="T245" id="Seg_958" n="HIAT:w" s="T244">Dĭgəttə</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_961" n="HIAT:w" s="T245">ibi</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_964" n="HIAT:w" s="T246">aktʼabə</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_967" n="HIAT:w" s="T247">i</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_970" n="HIAT:w" s="T248">dĭzeŋ</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_973" n="HIAT:w" s="T249">öʔlubi</ts>
                  <nts id="Seg_974" n="HIAT:ip">.</nts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_977" n="HIAT:u" s="T250">
                  <ts e="T251" id="Seg_979" n="HIAT:w" s="T250">Kabarləj</ts>
                  <nts id="Seg_980" n="HIAT:ip">.</nts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T251" id="Seg_982" n="sc" s="T0">
               <ts e="T1" id="Seg_984" n="e" s="T0">Amnolaʔbi </ts>
               <ts e="T2" id="Seg_986" n="e" s="T1">nüke </ts>
               <ts e="T3" id="Seg_988" n="e" s="T2">büzʼe. </ts>
               <ts e="T4" id="Seg_990" n="e" s="T3">Dĭzeŋ </ts>
               <ts e="T5" id="Seg_992" n="e" s="T4">ĭmbidə </ts>
               <ts e="T6" id="Seg_994" n="e" s="T5">nagobi. </ts>
               <ts e="T7" id="Seg_996" n="e" s="T6">Dĭ </ts>
               <ts e="T8" id="Seg_998" n="e" s="T7">büzʼe </ts>
               <ts e="T9" id="Seg_1000" n="e" s="T8">kürleʔpi </ts>
               <ts e="T10" id="Seg_1002" n="e" s="T9">jamaʔi. </ts>
               <ts e="T11" id="Seg_1004" n="e" s="T10">Da </ts>
               <ts e="T12" id="Seg_1006" n="e" s="T11">sadarbi, </ts>
               <ts e="T13" id="Seg_1008" n="e" s="T12">dărəʔ </ts>
               <ts e="T14" id="Seg_1010" n="e" s="T13">dĭzeŋ </ts>
               <ts e="T15" id="Seg_1012" n="e" s="T14">amnobiʔi. </ts>
               <ts e="T16" id="Seg_1014" n="e" s="T15">A </ts>
               <ts e="T17" id="Seg_1016" n="e" s="T16">(dĭ=) </ts>
               <ts e="T18" id="Seg_1018" n="e" s="T17">dĭzeŋgən </ts>
               <ts e="T19" id="Seg_1020" n="e" s="T18">ĭmbidə </ts>
               <ts e="T20" id="Seg_1022" n="e" s="T19">nagobi. </ts>
               <ts e="T21" id="Seg_1024" n="e" s="T20">Tolʼko </ts>
               <ts e="T22" id="Seg_1026" n="e" s="T21">(ădʼin) </ts>
               <ts e="T23" id="Seg_1028" n="e" s="T22">onʼiʔ </ts>
               <ts e="T24" id="Seg_1030" n="e" s="T23">poʔto </ts>
               <ts e="T25" id="Seg_1032" n="e" s="T24">ibi. </ts>
               <ts e="T26" id="Seg_1034" n="e" s="T25">Da </ts>
               <ts e="T27" id="Seg_1036" n="e" s="T26">kambi </ts>
               <ts e="T28" id="Seg_1038" n="e" s="T27">onʼiʔ. </ts>
               <ts e="T29" id="Seg_1040" n="e" s="T28">Kambi, </ts>
               <ts e="T30" id="Seg_1042" n="e" s="T29">dʼijenə </ts>
               <ts e="T31" id="Seg_1044" n="e" s="T30">kürzittə. </ts>
               <ts e="T32" id="Seg_1046" n="e" s="T31">Kubaʔi </ts>
               <ts e="T33" id="Seg_1048" n="e" s="T32">kürbi, </ts>
               <ts e="T34" id="Seg_1050" n="e" s="T33">kürbi. </ts>
               <ts e="T35" id="Seg_1052" n="e" s="T34">(A=) </ts>
               <ts e="T36" id="Seg_1054" n="e" s="T35">A </ts>
               <ts e="T37" id="Seg_1056" n="e" s="T36">poʔto </ts>
               <ts e="T38" id="Seg_1058" n="e" s="T37">mĭlleʔbə, </ts>
               <ts e="T39" id="Seg_1060" n="e" s="T38">dĭn </ts>
               <ts e="T40" id="Seg_1062" n="e" s="T39">noʔ </ts>
               <ts e="T41" id="Seg_1064" n="e" s="T40">amnaʔbə. </ts>
               <ts e="T42" id="Seg_1066" n="e" s="T41">Dĭgəttə </ts>
               <ts e="T43" id="Seg_1068" n="e" s="T42">mĭmbi, </ts>
               <ts e="T44" id="Seg_1070" n="e" s="T43">mĭmbi </ts>
               <ts e="T45" id="Seg_1072" n="e" s="T44">dĭ </ts>
               <ts e="T46" id="Seg_1074" n="e" s="T45">ujutsiʔ </ts>
               <ts e="T47" id="Seg_1076" n="e" s="T46">bar </ts>
               <ts e="T48" id="Seg_1078" n="e" s="T47">saʔməluʔpi. </ts>
               <ts e="T49" id="Seg_1080" n="e" s="T48">Kuliat, </ts>
               <ts e="T50" id="Seg_1082" n="e" s="T49">dĭn </ts>
               <ts e="T51" id="Seg_1084" n="e" s="T50">iʔbəlaʔbə </ts>
               <ts e="T52" id="Seg_1086" n="e" s="T51">iʔgö </ts>
               <ts e="T53" id="Seg_1088" n="e" s="T52">aktʼa. </ts>
               <ts e="T54" id="Seg_1090" n="e" s="T53">Dĭgəttə </ts>
               <ts e="T55" id="Seg_1092" n="e" s="T54">büzʼe </ts>
               <ts e="T56" id="Seg_1094" n="e" s="T55">šobi, </ts>
               <ts e="T57" id="Seg_1096" n="e" s="T56">(kuliat=) </ts>
               <ts e="T58" id="Seg_1098" n="e" s="T57">kubi </ts>
               <ts e="T59" id="Seg_1100" n="e" s="T58">aktʼa </ts>
               <ts e="T60" id="Seg_1102" n="e" s="T59">i </ts>
               <ts e="T61" id="Seg_1104" n="e" s="T60">deʔpi </ts>
               <ts e="T62" id="Seg_1106" n="e" s="T61">maːndə. </ts>
               <ts e="T63" id="Seg_1108" n="e" s="T62">Dĭgəttə </ts>
               <ts e="T64" id="Seg_1110" n="e" s="T63">măndə:" </ts>
               <ts e="T65" id="Seg_1112" n="e" s="T64">No, </ts>
               <ts e="T66" id="Seg_1114" n="e" s="T65">miʔnʼibeʔ </ts>
               <ts e="T67" id="Seg_1116" n="e" s="T66">kudaj </ts>
               <ts e="T68" id="Seg_1118" n="e" s="T67">mĭbi. </ts>
               <ts e="T69" id="Seg_1120" n="e" s="T68">No, </ts>
               <ts e="T70" id="Seg_1122" n="e" s="T69">kudaj </ts>
               <ts e="T71" id="Seg_1124" n="e" s="T70">nʼe </ts>
               <ts e="T72" id="Seg_1126" n="e" s="T71">kudaj, </ts>
               <ts e="T73" id="Seg_1128" n="e" s="T72">dĭ </ts>
               <ts e="T74" id="Seg_1130" n="e" s="T73">miʔnʼibeʔ </ts>
               <ts e="T75" id="Seg_1132" n="e" s="T74">kubi </ts>
               <ts e="T76" id="Seg_1134" n="e" s="T75">poʔto. </ts>
               <ts e="T77" id="Seg_1136" n="e" s="T76">Dĭm </ts>
               <ts e="T78" id="Seg_1138" n="e" s="T77">nada </ts>
               <ts e="T79" id="Seg_1140" n="e" s="T78">tože </ts>
               <ts e="T80" id="Seg_1142" n="e" s="T79">amzittə </ts>
               <ts e="T81" id="Seg_1144" n="e" s="T80">mĭzittə. </ts>
               <ts e="T82" id="Seg_1146" n="e" s="T81">(Dĭzem- </ts>
               <ts e="T83" id="Seg_1148" n="e" s="T82">dĭ-) </ts>
               <ts e="T84" id="Seg_1150" n="e" s="T83">Dĭzeŋ </ts>
               <ts e="T85" id="Seg_1152" n="e" s="T84">dĭm </ts>
               <ts e="T86" id="Seg_1154" n="e" s="T85">tože </ts>
               <ts e="T87" id="Seg_1156" n="e" s="T86">(ajərbiʔi). </ts>
               <ts e="T88" id="Seg_1158" n="e" s="T87">I </ts>
               <ts e="T89" id="Seg_1160" n="e" s="T88">bădəbiʔi </ts>
               <ts e="T90" id="Seg_1162" n="e" s="T89">bostə </ts>
               <ts e="T91" id="Seg_1164" n="e" s="T90">bar. </ts>
               <ts e="T92" id="Seg_1166" n="e" s="T91">Dĭgəttə </ts>
               <ts e="T93" id="Seg_1168" n="e" s="T92">ĭzemnuʔpi </ts>
               <ts e="T94" id="Seg_1170" n="e" s="T93">dĭ </ts>
               <ts e="T95" id="Seg_1172" n="e" s="T94">poʔto </ts>
               <ts e="T96" id="Seg_1174" n="e" s="T95">i </ts>
               <ts e="T97" id="Seg_1176" n="e" s="T96">külaːmbi. </ts>
               <ts e="T98" id="Seg_1178" n="e" s="T97">"Nada </ts>
               <ts e="T99" id="Seg_1180" n="e" s="T98">(dĭze-) </ts>
               <ts e="T100" id="Seg_1182" n="e" s="T99">dĭm </ts>
               <ts e="T101" id="Seg_1184" n="e" s="T100">barəʔsittə". </ts>
               <ts e="T102" id="Seg_1186" n="e" s="T101">"Nʼe </ts>
               <ts e="T103" id="Seg_1188" n="e" s="T102">nada, </ts>
               <ts e="T104" id="Seg_1190" n="e" s="T103">nada </ts>
               <ts e="T105" id="Seg_1192" n="e" s="T104">(dĭm=) </ts>
               <ts e="T106" id="Seg_1194" n="e" s="T105">kanzittə </ts>
               <ts e="T107" id="Seg_1196" n="e" s="T106">abəstə. </ts>
               <ts e="T108" id="Seg_1198" n="e" s="T107">Da </ts>
               <ts e="T109" id="Seg_1200" n="e" s="T108">jakšəŋ </ts>
               <ts e="T110" id="Seg_1202" n="e" s="T109">enzittə". </ts>
               <ts e="T111" id="Seg_1204" n="e" s="T110">Dĭgəttə </ts>
               <ts e="T112" id="Seg_1206" n="e" s="T111">kambi </ts>
               <ts e="T113" id="Seg_1208" n="e" s="T112">abəstə, </ts>
               <ts e="T114" id="Seg_1210" n="e" s="T113">abəs </ts>
               <ts e="T115" id="Seg_1212" n="e" s="T114">măndə: </ts>
               <ts e="T116" id="Seg_1214" n="e" s="T115">"Šindi </ts>
               <ts e="T117" id="Seg_1216" n="e" s="T116">poʔto?" </ts>
               <ts e="T118" id="Seg_1218" n="e" s="T117">Nüjleʔbə. </ts>
               <ts e="T119" id="Seg_1220" n="e" s="T118">I </ts>
               <ts e="T120" id="Seg_1222" n="e" s="T119">davaj </ts>
               <ts e="T121" id="Seg_1224" n="e" s="T120">dĭm </ts>
               <ts e="T122" id="Seg_1226" n="e" s="T121">münörzittə. </ts>
               <ts e="T123" id="Seg_1228" n="e" s="T122">"(Da-) </ts>
               <ts e="T125" id="Seg_1230" n="e" s="T123">Dĭm </ts>
               <ts e="T126" id="Seg_1232" n="e" s="T125">jakšə </ts>
               <ts e="T127" id="Seg_1234" n="e" s="T126">ibi. </ts>
               <ts e="T128" id="Seg_1236" n="e" s="T127">Tănan </ts>
               <ts e="T129" id="Seg_1238" n="e" s="T128">aktʼa </ts>
               <ts e="T130" id="Seg_1240" n="e" s="T129">maːbi, </ts>
               <ts e="T131" id="Seg_1242" n="e" s="T130">šide </ts>
               <ts e="T132" id="Seg_1244" n="e" s="T131">bieʔ". </ts>
               <ts e="T133" id="Seg_1246" n="e" s="T132">Dĭgəttə:" </ts>
               <ts e="T134" id="Seg_1248" n="e" s="T133">No, </ts>
               <ts e="T135" id="Seg_1250" n="e" s="T134">kanaʔ! </ts>
               <ts e="T136" id="Seg_1252" n="e" s="T135">Ĭmbi </ts>
               <ts e="T137" id="Seg_1254" n="e" s="T136">tăn </ts>
               <ts e="T138" id="Seg_1256" n="e" s="T137">măna </ts>
               <ts e="T139" id="Seg_1258" n="e" s="T138">ej </ts>
               <ts e="T140" id="Seg_1260" n="e" s="T139">nörbəbiel?" </ts>
               <ts e="T141" id="Seg_1262" n="e" s="T140">Dĭ </ts>
               <ts e="T142" id="Seg_1264" n="e" s="T141">kambi;" </ts>
               <ts e="T143" id="Seg_1266" n="e" s="T142">Kanaʔ, </ts>
               <ts e="T144" id="Seg_1268" n="e" s="T143">ami </ts>
               <ts e="T145" id="Seg_1270" n="e" s="T144">(a-) </ts>
               <ts e="T146" id="Seg_1272" n="e" s="T145">baška </ts>
               <ts e="T147" id="Seg_1274" n="e" s="T146">abəsdə", </ts>
               <ts e="T148" id="Seg_1276" n="e" s="T147">dibər </ts>
               <ts e="T149" id="Seg_1278" n="e" s="T148">kambi. </ts>
               <ts e="T150" id="Seg_1280" n="e" s="T149">Dĭʔnə </ts>
               <ts e="T151" id="Seg_1282" n="e" s="T150">(š-) </ts>
               <ts e="T152" id="Seg_1284" n="e" s="T151">tože </ts>
               <ts e="T153" id="Seg_1286" n="e" s="T152">nörbəbi. </ts>
               <ts e="T154" id="Seg_1288" n="e" s="T153">Dĭ </ts>
               <ts e="T155" id="Seg_1290" n="e" s="T154">tože </ts>
               <ts e="T156" id="Seg_1292" n="e" s="T155">dĭm </ts>
               <ts e="T157" id="Seg_1294" n="e" s="T156">kabarbi </ts>
               <ts e="T158" id="Seg_1296" n="e" s="T157">da </ts>
               <ts e="T159" id="Seg_1298" n="e" s="T158">münörluʔpi. </ts>
               <ts e="T160" id="Seg_1300" n="e" s="T159">"Iʔ </ts>
               <ts e="T161" id="Seg_1302" n="e" s="T160">münöraʔ, </ts>
               <ts e="T162" id="Seg_1304" n="e" s="T161">dĭ </ts>
               <ts e="T163" id="Seg_1306" n="e" s="T162">tănan </ts>
               <ts e="T164" id="Seg_1308" n="e" s="T163">aktʼa </ts>
               <ts e="T165" id="Seg_1310" n="e" s="T164">maːbi. </ts>
               <ts e="T166" id="Seg_1312" n="e" s="T165">(Ši-) </ts>
               <ts e="T167" id="Seg_1314" n="e" s="T166">Šide </ts>
               <ts e="T168" id="Seg_1316" n="e" s="T167">bieʔ </ts>
               <ts e="T169" id="Seg_1318" n="e" s="T168">aktʼa </ts>
               <ts e="T170" id="Seg_1320" n="e" s="T169">tănan </ts>
               <ts e="T171" id="Seg_1322" n="e" s="T170">maːbi". </ts>
               <ts e="T172" id="Seg_1324" n="e" s="T171">Dĭgəttə </ts>
               <ts e="T173" id="Seg_1326" n="e" s="T172">dĭ:" </ts>
               <ts e="T174" id="Seg_1328" n="e" s="T173">Ĭmbi </ts>
               <ts e="T175" id="Seg_1330" n="e" s="T174">tăn </ts>
               <ts e="T176" id="Seg_1332" n="e" s="T175">ej </ts>
               <ts e="T177" id="Seg_1334" n="e" s="T176">mămbial? </ts>
               <ts e="T178" id="Seg_1336" n="e" s="T177">Kanaʔ, </ts>
               <ts e="T179" id="Seg_1338" n="e" s="T178">pušaj </ts>
               <ts e="T180" id="Seg_1340" n="e" s="T179">(tʼegermaʔdə) </ts>
               <ts e="T181" id="Seg_1342" n="e" s="T180">sʼaləj </ts>
               <ts e="T182" id="Seg_1344" n="e" s="T181">da, </ts>
               <ts e="T183" id="Seg_1346" n="e" s="T182">küzürləj </ts>
               <ts e="T184" id="Seg_1348" n="e" s="T183">koŋgoroziʔ". </ts>
               <ts e="T185" id="Seg_1350" n="e" s="T184">Dĭ </ts>
               <ts e="T186" id="Seg_1352" n="e" s="T185">dibər </ts>
               <ts e="T187" id="Seg_1354" n="e" s="T186">šobi. </ts>
               <ts e="T188" id="Seg_1356" n="e" s="T187">Dĭ </ts>
               <ts e="T189" id="Seg_1358" n="e" s="T188">tože </ts>
               <ts e="T190" id="Seg_1360" n="e" s="T189">dĭm </ts>
               <ts e="T191" id="Seg_1362" n="e" s="T190">davaj </ts>
               <ts e="T192" id="Seg_1364" n="e" s="T191">(münö-). </ts>
               <ts e="T193" id="Seg_1366" n="e" s="T192">"Tănan </ts>
               <ts e="T194" id="Seg_1368" n="e" s="T193">bieʔ </ts>
               <ts e="T195" id="Seg_1370" n="e" s="T194">sumna </ts>
               <ts e="T196" id="Seg_1372" n="e" s="T195">mabi. </ts>
               <ts e="T197" id="Seg_1374" n="e" s="T196">Dĭ </ts>
               <ts e="T198" id="Seg_1376" n="e" s="T197">poʔto". </ts>
               <ts e="T199" id="Seg_1378" n="e" s="T198">"Ĭmbi </ts>
               <ts e="T200" id="Seg_1380" n="e" s="T199">tăn </ts>
               <ts e="T201" id="Seg_1382" n="e" s="T200">măna </ts>
               <ts e="T202" id="Seg_1384" n="e" s="T201">ej </ts>
               <ts e="T203" id="Seg_1386" n="e" s="T202">nörbəbiel?" </ts>
               <ts e="T204" id="Seg_1388" n="e" s="T203">Dĭgəttə </ts>
               <ts e="T205" id="Seg_1390" n="e" s="T204">suʔməluʔpi </ts>
               <ts e="T206" id="Seg_1392" n="e" s="T205">tʼegermaʔdə </ts>
               <ts e="T207" id="Seg_1394" n="e" s="T206">i </ts>
               <ts e="T208" id="Seg_1396" n="e" s="T207">davaj </ts>
               <ts e="T209" id="Seg_1398" n="e" s="T208">(koŋgorosʼtə=) </ts>
               <ts e="T210" id="Seg_1400" n="e" s="T209">koŋgorozʼiʔ </ts>
               <ts e="T211" id="Seg_1402" n="e" s="T210">bar </ts>
               <ts e="T212" id="Seg_1404" n="e" s="T211">küzürzittə. </ts>
               <ts e="T213" id="Seg_1406" n="e" s="T212">Dĭgəttə </ts>
               <ts e="T214" id="Seg_1408" n="e" s="T213">kambiʔi. </ts>
               <ts e="T215" id="Seg_1410" n="e" s="T214">Dĭ </ts>
               <ts e="T216" id="Seg_1412" n="e" s="T215">poʔto </ts>
               <ts e="T217" id="Seg_1414" n="e" s="T216">kumbiʔi. </ts>
               <ts e="T218" id="Seg_1416" n="e" s="T217">I </ts>
               <ts e="T219" id="Seg_1418" n="e" s="T218">dʼünə </ts>
               <ts e="T220" id="Seg_1420" n="e" s="T219">kămnəbiʔi. </ts>
               <ts e="T221" id="Seg_1422" n="e" s="T220">I </ts>
               <ts e="T222" id="Seg_1424" n="e" s="T221">iššo </ts>
               <ts e="T223" id="Seg_1426" n="e" s="T222">urgo </ts>
               <ts e="T224" id="Seg_1428" n="e" s="T223">abəs </ts>
               <ts e="T225" id="Seg_1430" n="e" s="T224">tĭmluʔpi. </ts>
               <ts e="T226" id="Seg_1432" n="e" s="T225">I </ts>
               <ts e="T227" id="Seg_1434" n="e" s="T226">kăštəbi </ts>
               <ts e="T228" id="Seg_1436" n="e" s="T227">dĭ </ts>
               <ts e="T229" id="Seg_1438" n="e" s="T228">büzʼem </ts>
               <ts e="T230" id="Seg_1440" n="e" s="T229">i </ts>
               <ts e="T231" id="Seg_1442" n="e" s="T230">abəstə. </ts>
               <ts e="T232" id="Seg_1444" n="e" s="T231">"Ĭmbi </ts>
               <ts e="T233" id="Seg_1446" n="e" s="T232">šiʔ </ts>
               <ts e="T234" id="Seg_1448" n="e" s="T233">poʔto </ts>
               <ts e="T235" id="Seg_1450" n="e" s="T234">embileʔ </ts>
               <ts e="T236" id="Seg_1452" n="e" s="T235">dʼünə". </ts>
               <ts e="T237" id="Seg_1454" n="e" s="T236">"Da </ts>
               <ts e="T238" id="Seg_1456" n="e" s="T237">dĭ </ts>
               <ts e="T239" id="Seg_1458" n="e" s="T238">jakšə </ts>
               <ts e="T240" id="Seg_1460" n="e" s="T239">ibi. </ts>
               <ts e="T241" id="Seg_1462" n="e" s="T240">Tănan </ts>
               <ts e="T242" id="Seg_1464" n="e" s="T241">iʔgö </ts>
               <ts e="T243" id="Seg_1466" n="e" s="T242">aktʼa </ts>
               <ts e="T244" id="Seg_1468" n="e" s="T243">maːluʔpi". </ts>
               <ts e="T245" id="Seg_1470" n="e" s="T244">Dĭgəttə </ts>
               <ts e="T246" id="Seg_1472" n="e" s="T245">ibi </ts>
               <ts e="T247" id="Seg_1474" n="e" s="T246">aktʼabə </ts>
               <ts e="T248" id="Seg_1476" n="e" s="T247">i </ts>
               <ts e="T249" id="Seg_1478" n="e" s="T248">dĭzeŋ </ts>
               <ts e="T250" id="Seg_1480" n="e" s="T249">öʔlubi. </ts>
               <ts e="T251" id="Seg_1482" n="e" s="T250">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_1483" s="T0">PKZ_196X_GoatFuneral_flk.001 (001)</ta>
            <ta e="T6" id="Seg_1484" s="T3">PKZ_196X_GoatFuneral_flk.002 (002)</ta>
            <ta e="T10" id="Seg_1485" s="T6">PKZ_196X_GoatFuneral_flk.003 (003)</ta>
            <ta e="T15" id="Seg_1486" s="T10">PKZ_196X_GoatFuneral_flk.004 (004)</ta>
            <ta e="T20" id="Seg_1487" s="T15">PKZ_196X_GoatFuneral_flk.005 (005)</ta>
            <ta e="T25" id="Seg_1488" s="T20">PKZ_196X_GoatFuneral_flk.006 (006)</ta>
            <ta e="T28" id="Seg_1489" s="T25">PKZ_196X_GoatFuneral_flk.007 (007)</ta>
            <ta e="T31" id="Seg_1490" s="T28">PKZ_196X_GoatFuneral_flk.008 (008)</ta>
            <ta e="T34" id="Seg_1491" s="T31">PKZ_196X_GoatFuneral_flk.009 (009)</ta>
            <ta e="T41" id="Seg_1492" s="T34">PKZ_196X_GoatFuneral_flk.010 (010)</ta>
            <ta e="T48" id="Seg_1493" s="T41">PKZ_196X_GoatFuneral_flk.011 (011)</ta>
            <ta e="T53" id="Seg_1494" s="T48">PKZ_196X_GoatFuneral_flk.012 (012)</ta>
            <ta e="T62" id="Seg_1495" s="T53">PKZ_196X_GoatFuneral_flk.013 (013)</ta>
            <ta e="T68" id="Seg_1496" s="T62">PKZ_196X_GoatFuneral_flk.014 (014)</ta>
            <ta e="T76" id="Seg_1497" s="T68">PKZ_196X_GoatFuneral_flk.015 (015)</ta>
            <ta e="T81" id="Seg_1498" s="T76">PKZ_196X_GoatFuneral_flk.016 (016)</ta>
            <ta e="T87" id="Seg_1499" s="T81">PKZ_196X_GoatFuneral_flk.017 (017)</ta>
            <ta e="T91" id="Seg_1500" s="T87">PKZ_196X_GoatFuneral_flk.018 (018)</ta>
            <ta e="T97" id="Seg_1501" s="T91">PKZ_196X_GoatFuneral_flk.019 (019)</ta>
            <ta e="T101" id="Seg_1502" s="T97">PKZ_196X_GoatFuneral_flk.020 (020)</ta>
            <ta e="T107" id="Seg_1503" s="T101">PKZ_196X_GoatFuneral_flk.021 (021)</ta>
            <ta e="T110" id="Seg_1504" s="T107">PKZ_196X_GoatFuneral_flk.022 (022)</ta>
            <ta e="T117" id="Seg_1505" s="T110">PKZ_196X_GoatFuneral_flk.023 (023)</ta>
            <ta e="T118" id="Seg_1506" s="T117">PKZ_196X_GoatFuneral_flk.024 (025)</ta>
            <ta e="T122" id="Seg_1507" s="T118">PKZ_196X_GoatFuneral_flk.025 (026)</ta>
            <ta e="T127" id="Seg_1508" s="T122">PKZ_196X_GoatFuneral_flk.026 (027)</ta>
            <ta e="T132" id="Seg_1509" s="T127">PKZ_196X_GoatFuneral_flk.027 (028)</ta>
            <ta e="T135" id="Seg_1510" s="T132">PKZ_196X_GoatFuneral_flk.028 (029)</ta>
            <ta e="T140" id="Seg_1511" s="T135">PKZ_196X_GoatFuneral_flk.029 (030)</ta>
            <ta e="T149" id="Seg_1512" s="T140">PKZ_196X_GoatFuneral_flk.030 (031)</ta>
            <ta e="T153" id="Seg_1513" s="T149">PKZ_196X_GoatFuneral_flk.031 (032)</ta>
            <ta e="T159" id="Seg_1514" s="T153">PKZ_196X_GoatFuneral_flk.032 (033)</ta>
            <ta e="T165" id="Seg_1515" s="T159">PKZ_196X_GoatFuneral_flk.033 (034)</ta>
            <ta e="T171" id="Seg_1516" s="T165">PKZ_196X_GoatFuneral_flk.034 (035)</ta>
            <ta e="T177" id="Seg_1517" s="T171">PKZ_196X_GoatFuneral_flk.035 (036)</ta>
            <ta e="T184" id="Seg_1518" s="T177">PKZ_196X_GoatFuneral_flk.036 (037)</ta>
            <ta e="T187" id="Seg_1519" s="T184">PKZ_196X_GoatFuneral_flk.037 (038)</ta>
            <ta e="T192" id="Seg_1520" s="T187">PKZ_196X_GoatFuneral_flk.038 (039)</ta>
            <ta e="T196" id="Seg_1521" s="T192">PKZ_196X_GoatFuneral_flk.039 (040)</ta>
            <ta e="T198" id="Seg_1522" s="T196">PKZ_196X_GoatFuneral_flk.040 (041)</ta>
            <ta e="T203" id="Seg_1523" s="T198">PKZ_196X_GoatFuneral_flk.041 (042)</ta>
            <ta e="T212" id="Seg_1524" s="T203">PKZ_196X_GoatFuneral_flk.042 (043)</ta>
            <ta e="T214" id="Seg_1525" s="T212">PKZ_196X_GoatFuneral_flk.043 (044)</ta>
            <ta e="T217" id="Seg_1526" s="T214">PKZ_196X_GoatFuneral_flk.044 (045)</ta>
            <ta e="T220" id="Seg_1527" s="T217">PKZ_196X_GoatFuneral_flk.045 (046)</ta>
            <ta e="T225" id="Seg_1528" s="T220">PKZ_196X_GoatFuneral_flk.046 (047)</ta>
            <ta e="T231" id="Seg_1529" s="T225">PKZ_196X_GoatFuneral_flk.047 (048)</ta>
            <ta e="T236" id="Seg_1530" s="T231">PKZ_196X_GoatFuneral_flk.048 (049)</ta>
            <ta e="T240" id="Seg_1531" s="T236">PKZ_196X_GoatFuneral_flk.049 (050)</ta>
            <ta e="T244" id="Seg_1532" s="T240">PKZ_196X_GoatFuneral_flk.050 (051)</ta>
            <ta e="T250" id="Seg_1533" s="T244">PKZ_196X_GoatFuneral_flk.051 (052)</ta>
            <ta e="T251" id="Seg_1534" s="T250">PKZ_196X_GoatFuneral_flk.052 (053)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_1535" s="T0">Amnolaʔbi nüke büzʼe. </ta>
            <ta e="T6" id="Seg_1536" s="T3">Dĭzeŋ ĭmbidə nagobi. </ta>
            <ta e="T10" id="Seg_1537" s="T6">Dĭ büzʼe kürleʔpi jamaʔi. </ta>
            <ta e="T15" id="Seg_1538" s="T10">Da sadarbi, dărəʔ dĭzeŋ amnobiʔi. </ta>
            <ta e="T20" id="Seg_1539" s="T15">A (dĭ=) dĭzeŋgən ĭmbidə nagobi. </ta>
            <ta e="T25" id="Seg_1540" s="T20">Tolʼko (ădʼin) onʼiʔ poʔto ibi. </ta>
            <ta e="T28" id="Seg_1541" s="T25">Da kambi onʼiʔ. </ta>
            <ta e="T31" id="Seg_1542" s="T28">Kambi, dʼijenə kürzittə. </ta>
            <ta e="T34" id="Seg_1543" s="T31">Kubaʔi kürbi, kürbi. </ta>
            <ta e="T41" id="Seg_1544" s="T34">(A=) A poʔto mĭlleʔbə, dĭn noʔ amnaʔbə. </ta>
            <ta e="T48" id="Seg_1545" s="T41">Dĭgəttə mĭmbi, mĭmbi dĭ ujutsiʔ bar saʔməluʔpi. </ta>
            <ta e="T53" id="Seg_1546" s="T48">Kuliat, dĭn iʔbəlaʔbə iʔgö aktʼa. </ta>
            <ta e="T62" id="Seg_1547" s="T53">Dĭgəttə büzʼe šobi, (kuliat=) kubi aktʼa i deʔpi maːndə. </ta>
            <ta e="T68" id="Seg_1548" s="T62">Dĭgəttə măndə: "No, miʔnʼibeʔ kudaj mĭbi." </ta>
            <ta e="T76" id="Seg_1549" s="T68">"No, kudaj nʼe kudaj, dĭ miʔnʼibeʔ kubi poʔto. </ta>
            <ta e="T81" id="Seg_1550" s="T76">Dĭm nada tože amzittə mĭzittə." </ta>
            <ta e="T87" id="Seg_1551" s="T81">(Dĭzem- dĭ-) Dĭzeŋ dĭm tože (ajərbiʔi). </ta>
            <ta e="T91" id="Seg_1552" s="T87">I bădəbiʔi bostə bar. </ta>
            <ta e="T97" id="Seg_1553" s="T91">Dĭgəttə ĭzemnuʔpi dĭ poʔto i külaːmbi. </ta>
            <ta e="T101" id="Seg_1554" s="T97">"Nada (dĭze-) dĭm barəʔsittə". </ta>
            <ta e="T107" id="Seg_1555" s="T101">"Nʼe nada, nada (dĭm=) kanzittə abəstə. </ta>
            <ta e="T110" id="Seg_1556" s="T107">Da jakšəŋ enzittə". </ta>
            <ta e="T117" id="Seg_1557" s="T110">Dĭgəttə kambi abəstə, abəs măndə: "Šindi poʔto?" </ta>
            <ta e="T118" id="Seg_1558" s="T117">Nüjleʔbə. </ta>
            <ta e="T122" id="Seg_1559" s="T118">I davaj dĭm münörzittə. </ta>
            <ta e="T127" id="Seg_1560" s="T122">"(Da-) Dĭm ((PAUSE)) jakšə ibi. </ta>
            <ta e="T132" id="Seg_1561" s="T127">Tănan aktʼa maːbi, šide bieʔ". </ta>
            <ta e="T135" id="Seg_1562" s="T132">Dĭgəttə:" No, kanaʔ! </ta>
            <ta e="T140" id="Seg_1563" s="T135">Ĭmbi tăn măna ej nörbəbiel?" </ta>
            <ta e="T149" id="Seg_1564" s="T140">Dĭ kambi;" Kanaʔ, ami (a-) baška abəsdə", dibər kambi. </ta>
            <ta e="T153" id="Seg_1565" s="T149">Dĭʔnə (š-) tože nörbəbi. </ta>
            <ta e="T159" id="Seg_1566" s="T153">Dĭ tože dĭm kabarbi da münörluʔpi. </ta>
            <ta e="T165" id="Seg_1567" s="T159">"Iʔ münöraʔ, dĭ tănan aktʼa maːbi. </ta>
            <ta e="T171" id="Seg_1568" s="T165">(Ši-) Šide bieʔ aktʼa tănan maːbi". </ta>
            <ta e="T177" id="Seg_1569" s="T171">Dĭgəttə dĭ:" Ĭmbi tăn ej mămbial? </ta>
            <ta e="T184" id="Seg_1570" s="T177">Kanaʔ, pušaj (tʼegermaʔdə) sʼaləj da, küzürləj koŋgoroziʔ". </ta>
            <ta e="T187" id="Seg_1571" s="T184">Dĭ dibər šobi. </ta>
            <ta e="T192" id="Seg_1572" s="T187">Dĭ tože dĭm davaj (münö-). </ta>
            <ta e="T196" id="Seg_1573" s="T192">"Tănan bieʔ sumna mabi. </ta>
            <ta e="T198" id="Seg_1574" s="T196">Dĭ poʔto". </ta>
            <ta e="T203" id="Seg_1575" s="T198">"Ĭmbi tăn măna ej nörbəbiel?" </ta>
            <ta e="T212" id="Seg_1576" s="T203">Dĭgəttə suʔməluʔpi tʼegermaʔdə i davaj (koŋgorosʼtə=) koŋgorozʼiʔ bar küzürzittə. </ta>
            <ta e="T214" id="Seg_1577" s="T212">Dĭgəttə kambiʔi. </ta>
            <ta e="T217" id="Seg_1578" s="T214">Dĭ poʔto kumbiʔi. </ta>
            <ta e="T220" id="Seg_1579" s="T217">I dʼünə kămnəbiʔi. </ta>
            <ta e="T225" id="Seg_1580" s="T220">I iššo urgo abəs tĭmluʔpi. </ta>
            <ta e="T231" id="Seg_1581" s="T225">I kăštəbi dĭ büzʼem i abəstə. </ta>
            <ta e="T236" id="Seg_1582" s="T231">"Ĭmbi šiʔ poʔto embileʔ dʼünə". </ta>
            <ta e="T240" id="Seg_1583" s="T236">"Da dĭ jakšə ibi. </ta>
            <ta e="T244" id="Seg_1584" s="T240">Tănan iʔgö aktʼa maːluʔpi". </ta>
            <ta e="T250" id="Seg_1585" s="T244">Dĭgəttə ibi aktʼabə i dĭzeŋ öʔlubi. </ta>
            <ta e="T251" id="Seg_1586" s="T250">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1587" s="T0">amno-laʔ-bi</ta>
            <ta e="T2" id="Seg_1588" s="T1">nüke</ta>
            <ta e="T3" id="Seg_1589" s="T2">büzʼe</ta>
            <ta e="T4" id="Seg_1590" s="T3">dĭ-zeŋ</ta>
            <ta e="T5" id="Seg_1591" s="T4">ĭmbi=də</ta>
            <ta e="T6" id="Seg_1592" s="T5">nago-bi</ta>
            <ta e="T7" id="Seg_1593" s="T6">dĭ</ta>
            <ta e="T8" id="Seg_1594" s="T7">büzʼe</ta>
            <ta e="T9" id="Seg_1595" s="T8">kür-leʔ-pi</ta>
            <ta e="T10" id="Seg_1596" s="T9">jama-ʔi</ta>
            <ta e="T11" id="Seg_1597" s="T10">da</ta>
            <ta e="T12" id="Seg_1598" s="T11">sadar-bi</ta>
            <ta e="T13" id="Seg_1599" s="T12">dărəʔ</ta>
            <ta e="T14" id="Seg_1600" s="T13">dĭ-zeŋ</ta>
            <ta e="T15" id="Seg_1601" s="T14">amno-bi-ʔi</ta>
            <ta e="T16" id="Seg_1602" s="T15">a</ta>
            <ta e="T17" id="Seg_1603" s="T16">dĭ</ta>
            <ta e="T18" id="Seg_1604" s="T17">dĭ-zeŋ-gən</ta>
            <ta e="T19" id="Seg_1605" s="T18">ĭmbi=də</ta>
            <ta e="T20" id="Seg_1606" s="T19">nago-bi</ta>
            <ta e="T21" id="Seg_1607" s="T20">tolʼko</ta>
            <ta e="T23" id="Seg_1608" s="T22">onʼiʔ</ta>
            <ta e="T24" id="Seg_1609" s="T23">poʔto</ta>
            <ta e="T25" id="Seg_1610" s="T24">i-bi</ta>
            <ta e="T26" id="Seg_1611" s="T25">da</ta>
            <ta e="T27" id="Seg_1612" s="T26">kam-bi</ta>
            <ta e="T28" id="Seg_1613" s="T27">onʼiʔ</ta>
            <ta e="T29" id="Seg_1614" s="T28">kam-bi</ta>
            <ta e="T30" id="Seg_1615" s="T29">dʼije-nə</ta>
            <ta e="T31" id="Seg_1616" s="T30">kür-zittə</ta>
            <ta e="T32" id="Seg_1617" s="T31">kuba-ʔi</ta>
            <ta e="T33" id="Seg_1618" s="T32">kür-bi</ta>
            <ta e="T34" id="Seg_1619" s="T33">kür-bi</ta>
            <ta e="T35" id="Seg_1620" s="T34">a</ta>
            <ta e="T36" id="Seg_1621" s="T35">a</ta>
            <ta e="T37" id="Seg_1622" s="T36">poʔto</ta>
            <ta e="T38" id="Seg_1623" s="T37">mĭl-leʔbə</ta>
            <ta e="T39" id="Seg_1624" s="T38">dĭn</ta>
            <ta e="T40" id="Seg_1625" s="T39">noʔ</ta>
            <ta e="T41" id="Seg_1626" s="T40">am-naʔbə</ta>
            <ta e="T42" id="Seg_1627" s="T41">dĭgəttə</ta>
            <ta e="T43" id="Seg_1628" s="T42">mĭm-bi</ta>
            <ta e="T44" id="Seg_1629" s="T43">mĭm-bi</ta>
            <ta e="T45" id="Seg_1630" s="T44">dĭ</ta>
            <ta e="T46" id="Seg_1631" s="T45">uju-t-siʔ</ta>
            <ta e="T47" id="Seg_1632" s="T46">bar</ta>
            <ta e="T48" id="Seg_1633" s="T47">saʔmə-luʔ-pi</ta>
            <ta e="T49" id="Seg_1634" s="T48">ku-lia-t</ta>
            <ta e="T50" id="Seg_1635" s="T49">dĭn</ta>
            <ta e="T51" id="Seg_1636" s="T50">iʔbə-laʔbə</ta>
            <ta e="T52" id="Seg_1637" s="T51">iʔgö</ta>
            <ta e="T53" id="Seg_1638" s="T52">aktʼa</ta>
            <ta e="T54" id="Seg_1639" s="T53">dĭgəttə</ta>
            <ta e="T55" id="Seg_1640" s="T54">büzʼe</ta>
            <ta e="T56" id="Seg_1641" s="T55">šo-bi</ta>
            <ta e="T57" id="Seg_1642" s="T56">ku-lia-t</ta>
            <ta e="T58" id="Seg_1643" s="T57">ku-bi</ta>
            <ta e="T59" id="Seg_1644" s="T58">aktʼa</ta>
            <ta e="T60" id="Seg_1645" s="T59">i</ta>
            <ta e="T61" id="Seg_1646" s="T60">deʔ-pi</ta>
            <ta e="T62" id="Seg_1647" s="T61">ma-ndə</ta>
            <ta e="T63" id="Seg_1648" s="T62">dĭgəttə</ta>
            <ta e="T64" id="Seg_1649" s="T63">măn-də</ta>
            <ta e="T65" id="Seg_1650" s="T64">no</ta>
            <ta e="T66" id="Seg_1651" s="T65">miʔnʼibeʔ</ta>
            <ta e="T67" id="Seg_1652" s="T66">kudaj</ta>
            <ta e="T68" id="Seg_1653" s="T67">mĭ-bi</ta>
            <ta e="T69" id="Seg_1654" s="T68">no</ta>
            <ta e="T70" id="Seg_1655" s="T69">kudaj</ta>
            <ta e="T71" id="Seg_1656" s="T70">nʼe</ta>
            <ta e="T72" id="Seg_1657" s="T71">kudaj</ta>
            <ta e="T73" id="Seg_1658" s="T72">dĭ</ta>
            <ta e="T74" id="Seg_1659" s="T73">miʔnʼibeʔ</ta>
            <ta e="T75" id="Seg_1660" s="T74">ku-bi</ta>
            <ta e="T76" id="Seg_1661" s="T75">poʔto</ta>
            <ta e="T77" id="Seg_1662" s="T76">dĭ-m</ta>
            <ta e="T78" id="Seg_1663" s="T77">nada</ta>
            <ta e="T79" id="Seg_1664" s="T78">tože</ta>
            <ta e="T80" id="Seg_1665" s="T79">am-zittə</ta>
            <ta e="T81" id="Seg_1666" s="T80">mĭ-zittə</ta>
            <ta e="T84" id="Seg_1667" s="T83">dĭ-zeŋ</ta>
            <ta e="T85" id="Seg_1668" s="T84">dĭ-m</ta>
            <ta e="T86" id="Seg_1669" s="T85">tože</ta>
            <ta e="T87" id="Seg_1670" s="T86">ajər-bi-ʔi</ta>
            <ta e="T88" id="Seg_1671" s="T87">i</ta>
            <ta e="T89" id="Seg_1672" s="T88">bădə-bi-ʔi</ta>
            <ta e="T90" id="Seg_1673" s="T89">bos-tə</ta>
            <ta e="T91" id="Seg_1674" s="T90">bar</ta>
            <ta e="T92" id="Seg_1675" s="T91">dĭgəttə</ta>
            <ta e="T93" id="Seg_1676" s="T92">ĭzem-nuʔ-pi</ta>
            <ta e="T94" id="Seg_1677" s="T93">dĭ</ta>
            <ta e="T95" id="Seg_1678" s="T94">poʔto</ta>
            <ta e="T96" id="Seg_1679" s="T95">i</ta>
            <ta e="T97" id="Seg_1680" s="T96">kü-laːm-bi</ta>
            <ta e="T98" id="Seg_1681" s="T97">nada</ta>
            <ta e="T100" id="Seg_1682" s="T99">dĭ-m</ta>
            <ta e="T101" id="Seg_1683" s="T100">barəʔ-sittə</ta>
            <ta e="T102" id="Seg_1684" s="T101">nʼe</ta>
            <ta e="T103" id="Seg_1685" s="T102">nada</ta>
            <ta e="T104" id="Seg_1686" s="T103">nada</ta>
            <ta e="T105" id="Seg_1687" s="T104">dĭ-m</ta>
            <ta e="T106" id="Seg_1688" s="T105">kan-zittə</ta>
            <ta e="T107" id="Seg_1689" s="T106">abəs-tə</ta>
            <ta e="T108" id="Seg_1690" s="T107">da</ta>
            <ta e="T109" id="Seg_1691" s="T108">jakšə-ŋ</ta>
            <ta e="T110" id="Seg_1692" s="T109">en-zittə</ta>
            <ta e="T111" id="Seg_1693" s="T110">dĭgəttə</ta>
            <ta e="T112" id="Seg_1694" s="T111">kam-bi</ta>
            <ta e="T113" id="Seg_1695" s="T112">abəs-tə</ta>
            <ta e="T114" id="Seg_1696" s="T113">abəs</ta>
            <ta e="T115" id="Seg_1697" s="T114">măn-də</ta>
            <ta e="T116" id="Seg_1698" s="T115">šindi</ta>
            <ta e="T117" id="Seg_1699" s="T116">poʔto</ta>
            <ta e="T118" id="Seg_1700" s="T117">nüj-leʔbə</ta>
            <ta e="T119" id="Seg_1701" s="T118">i</ta>
            <ta e="T120" id="Seg_1702" s="T119">davaj</ta>
            <ta e="T121" id="Seg_1703" s="T120">dĭ-m</ta>
            <ta e="T122" id="Seg_1704" s="T121">münör-zittə</ta>
            <ta e="T125" id="Seg_1705" s="T123">dĭ-m</ta>
            <ta e="T126" id="Seg_1706" s="T125">jakšə</ta>
            <ta e="T127" id="Seg_1707" s="T126">i-bi</ta>
            <ta e="T128" id="Seg_1708" s="T127">tănan</ta>
            <ta e="T129" id="Seg_1709" s="T128">aktʼa</ta>
            <ta e="T130" id="Seg_1710" s="T129">ma-bi</ta>
            <ta e="T131" id="Seg_1711" s="T130">šide</ta>
            <ta e="T132" id="Seg_1712" s="T131">bieʔ</ta>
            <ta e="T133" id="Seg_1713" s="T132">dĭgəttə</ta>
            <ta e="T134" id="Seg_1714" s="T133">no</ta>
            <ta e="T135" id="Seg_1715" s="T134">kan-a-ʔ</ta>
            <ta e="T136" id="Seg_1716" s="T135">ĭmbi</ta>
            <ta e="T137" id="Seg_1717" s="T136">tăn</ta>
            <ta e="T138" id="Seg_1718" s="T137">măna</ta>
            <ta e="T139" id="Seg_1719" s="T138">ej</ta>
            <ta e="T140" id="Seg_1720" s="T139">nörbə-bie-l</ta>
            <ta e="T141" id="Seg_1721" s="T140">dĭ</ta>
            <ta e="T142" id="Seg_1722" s="T141">kam-bi</ta>
            <ta e="T143" id="Seg_1723" s="T142">kan-a-ʔ</ta>
            <ta e="T144" id="Seg_1724" s="T143">ami</ta>
            <ta e="T146" id="Seg_1725" s="T145">baška</ta>
            <ta e="T147" id="Seg_1726" s="T146">abəs-də</ta>
            <ta e="T148" id="Seg_1727" s="T147">dibər</ta>
            <ta e="T149" id="Seg_1728" s="T148">kam-bi</ta>
            <ta e="T150" id="Seg_1729" s="T149">dĭʔ-nə</ta>
            <ta e="T152" id="Seg_1730" s="T151">tože</ta>
            <ta e="T153" id="Seg_1731" s="T152">nörbə-bi</ta>
            <ta e="T154" id="Seg_1732" s="T153">dĭ</ta>
            <ta e="T155" id="Seg_1733" s="T154">tože</ta>
            <ta e="T156" id="Seg_1734" s="T155">dĭ-m</ta>
            <ta e="T157" id="Seg_1735" s="T156">kabar-bi</ta>
            <ta e="T158" id="Seg_1736" s="T157">da</ta>
            <ta e="T159" id="Seg_1737" s="T158">münör-luʔ-pi</ta>
            <ta e="T160" id="Seg_1738" s="T159">i-ʔ</ta>
            <ta e="T161" id="Seg_1739" s="T160">münör-a-ʔ</ta>
            <ta e="T162" id="Seg_1740" s="T161">dĭ</ta>
            <ta e="T163" id="Seg_1741" s="T162">tănan</ta>
            <ta e="T164" id="Seg_1742" s="T163">aktʼa</ta>
            <ta e="T165" id="Seg_1743" s="T164">ma-bi</ta>
            <ta e="T167" id="Seg_1744" s="T166">šide</ta>
            <ta e="T168" id="Seg_1745" s="T167">bieʔ</ta>
            <ta e="T169" id="Seg_1746" s="T168">aktʼa</ta>
            <ta e="T170" id="Seg_1747" s="T169">tănan</ta>
            <ta e="T171" id="Seg_1748" s="T170">ma-bi</ta>
            <ta e="T172" id="Seg_1749" s="T171">dĭgəttə</ta>
            <ta e="T173" id="Seg_1750" s="T172">dĭ</ta>
            <ta e="T174" id="Seg_1751" s="T173">ĭmbi</ta>
            <ta e="T175" id="Seg_1752" s="T174">tăn</ta>
            <ta e="T176" id="Seg_1753" s="T175">ej</ta>
            <ta e="T177" id="Seg_1754" s="T176">măm-bia-l</ta>
            <ta e="T178" id="Seg_1755" s="T177">kan-a-ʔ</ta>
            <ta e="T179" id="Seg_1756" s="T178">pušaj</ta>
            <ta e="T180" id="Seg_1757" s="T179">tʼegermaʔ-də</ta>
            <ta e="T181" id="Seg_1758" s="T180">sʼa-lə-j</ta>
            <ta e="T182" id="Seg_1759" s="T181">da</ta>
            <ta e="T183" id="Seg_1760" s="T182">küzür-lə-j</ta>
            <ta e="T184" id="Seg_1761" s="T183">koŋgoro-ziʔ</ta>
            <ta e="T185" id="Seg_1762" s="T184">dĭ</ta>
            <ta e="T186" id="Seg_1763" s="T185">dibər</ta>
            <ta e="T187" id="Seg_1764" s="T186">šo-bi</ta>
            <ta e="T188" id="Seg_1765" s="T187">dĭ</ta>
            <ta e="T189" id="Seg_1766" s="T188">tože</ta>
            <ta e="T190" id="Seg_1767" s="T189">dĭ-m</ta>
            <ta e="T191" id="Seg_1768" s="T190">davaj</ta>
            <ta e="T193" id="Seg_1769" s="T192">tănan</ta>
            <ta e="T194" id="Seg_1770" s="T193">bieʔ</ta>
            <ta e="T195" id="Seg_1771" s="T194">sumna</ta>
            <ta e="T196" id="Seg_1772" s="T195">ma-bi</ta>
            <ta e="T197" id="Seg_1773" s="T196">dĭ</ta>
            <ta e="T198" id="Seg_1774" s="T197">poʔto</ta>
            <ta e="T199" id="Seg_1775" s="T198">ĭmbi</ta>
            <ta e="T200" id="Seg_1776" s="T199">tăn</ta>
            <ta e="T201" id="Seg_1777" s="T200">măna</ta>
            <ta e="T202" id="Seg_1778" s="T201">ej</ta>
            <ta e="T203" id="Seg_1779" s="T202">nörbə-bie-l</ta>
            <ta e="T204" id="Seg_1780" s="T203">dĭgəttə</ta>
            <ta e="T205" id="Seg_1781" s="T204">suʔmə-luʔ-pi</ta>
            <ta e="T206" id="Seg_1782" s="T205">tʼegermaʔ-də</ta>
            <ta e="T207" id="Seg_1783" s="T206">i</ta>
            <ta e="T208" id="Seg_1784" s="T207">davaj</ta>
            <ta e="T209" id="Seg_1785" s="T208">koŋgoro-sʼtə</ta>
            <ta e="T210" id="Seg_1786" s="T209">koŋgoro-zʼiʔ</ta>
            <ta e="T211" id="Seg_1787" s="T210">bar</ta>
            <ta e="T212" id="Seg_1788" s="T211">küzür-zittə</ta>
            <ta e="T213" id="Seg_1789" s="T212">dĭgəttə</ta>
            <ta e="T214" id="Seg_1790" s="T213">kam-bi-ʔi</ta>
            <ta e="T215" id="Seg_1791" s="T214">dĭ</ta>
            <ta e="T216" id="Seg_1792" s="T215">poʔto</ta>
            <ta e="T217" id="Seg_1793" s="T216">kum-bi-ʔi</ta>
            <ta e="T218" id="Seg_1794" s="T217">i</ta>
            <ta e="T219" id="Seg_1795" s="T218">dʼü-nə</ta>
            <ta e="T220" id="Seg_1796" s="T219">kămnə-bi-ʔi</ta>
            <ta e="T221" id="Seg_1797" s="T220">i</ta>
            <ta e="T222" id="Seg_1798" s="T221">iššo</ta>
            <ta e="T223" id="Seg_1799" s="T222">urgo</ta>
            <ta e="T224" id="Seg_1800" s="T223">abəs</ta>
            <ta e="T225" id="Seg_1801" s="T224">tĭm-luʔ-pi</ta>
            <ta e="T226" id="Seg_1802" s="T225">i</ta>
            <ta e="T227" id="Seg_1803" s="T226">kăštə-bi</ta>
            <ta e="T228" id="Seg_1804" s="T227">dĭ</ta>
            <ta e="T229" id="Seg_1805" s="T228">büzʼe-m</ta>
            <ta e="T230" id="Seg_1806" s="T229">i</ta>
            <ta e="T231" id="Seg_1807" s="T230">abəs-tə</ta>
            <ta e="T232" id="Seg_1808" s="T231">ĭmbi</ta>
            <ta e="T233" id="Seg_1809" s="T232">šiʔ</ta>
            <ta e="T234" id="Seg_1810" s="T233">poʔto</ta>
            <ta e="T235" id="Seg_1811" s="T234">em-bi-leʔ</ta>
            <ta e="T236" id="Seg_1812" s="T235">dʼü-nə</ta>
            <ta e="T237" id="Seg_1813" s="T236">da</ta>
            <ta e="T238" id="Seg_1814" s="T237">dĭ</ta>
            <ta e="T239" id="Seg_1815" s="T238">jakšə</ta>
            <ta e="T240" id="Seg_1816" s="T239">i-bi</ta>
            <ta e="T241" id="Seg_1817" s="T240">tănan</ta>
            <ta e="T242" id="Seg_1818" s="T241">iʔgö</ta>
            <ta e="T243" id="Seg_1819" s="T242">aktʼa</ta>
            <ta e="T244" id="Seg_1820" s="T243">ma-luʔ-pi</ta>
            <ta e="T245" id="Seg_1821" s="T244">dĭgəttə</ta>
            <ta e="T246" id="Seg_1822" s="T245">i-bi</ta>
            <ta e="T247" id="Seg_1823" s="T246">aktʼa-bə</ta>
            <ta e="T248" id="Seg_1824" s="T247">i</ta>
            <ta e="T249" id="Seg_1825" s="T248">dĭ-zeŋ</ta>
            <ta e="T250" id="Seg_1826" s="T249">öʔlu-bi</ta>
            <ta e="T251" id="Seg_1827" s="T250">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1828" s="T0">amno-laʔbə-bi</ta>
            <ta e="T2" id="Seg_1829" s="T1">nüke</ta>
            <ta e="T3" id="Seg_1830" s="T2">büzʼe</ta>
            <ta e="T4" id="Seg_1831" s="T3">dĭ-zAŋ</ta>
            <ta e="T5" id="Seg_1832" s="T4">ĭmbi=də</ta>
            <ta e="T6" id="Seg_1833" s="T5">naga-bi</ta>
            <ta e="T7" id="Seg_1834" s="T6">dĭ</ta>
            <ta e="T8" id="Seg_1835" s="T7">büzʼe</ta>
            <ta e="T9" id="Seg_1836" s="T8">kür-laʔbə-bi</ta>
            <ta e="T10" id="Seg_1837" s="T9">jama-jəʔ</ta>
            <ta e="T11" id="Seg_1838" s="T10">da</ta>
            <ta e="T12" id="Seg_1839" s="T11">sădar-bi</ta>
            <ta e="T13" id="Seg_1840" s="T12">dărəʔ</ta>
            <ta e="T14" id="Seg_1841" s="T13">dĭ-zAŋ</ta>
            <ta e="T15" id="Seg_1842" s="T14">amnə-bi-jəʔ</ta>
            <ta e="T16" id="Seg_1843" s="T15">a</ta>
            <ta e="T17" id="Seg_1844" s="T16">dĭ</ta>
            <ta e="T18" id="Seg_1845" s="T17">dĭ-zAŋ-Kən</ta>
            <ta e="T19" id="Seg_1846" s="T18">ĭmbi=də</ta>
            <ta e="T20" id="Seg_1847" s="T19">naga-bi</ta>
            <ta e="T21" id="Seg_1848" s="T20">tolʼko</ta>
            <ta e="T23" id="Seg_1849" s="T22">onʼiʔ</ta>
            <ta e="T24" id="Seg_1850" s="T23">poʔto</ta>
            <ta e="T25" id="Seg_1851" s="T24">i-bi</ta>
            <ta e="T26" id="Seg_1852" s="T25">da</ta>
            <ta e="T27" id="Seg_1853" s="T26">kan-bi</ta>
            <ta e="T28" id="Seg_1854" s="T27">onʼiʔ</ta>
            <ta e="T29" id="Seg_1855" s="T28">kan-bi</ta>
            <ta e="T30" id="Seg_1856" s="T29">dʼije-Tə</ta>
            <ta e="T31" id="Seg_1857" s="T30">kür-zittə</ta>
            <ta e="T32" id="Seg_1858" s="T31">kuba-jəʔ</ta>
            <ta e="T33" id="Seg_1859" s="T32">kür-bi</ta>
            <ta e="T34" id="Seg_1860" s="T33">kür-bi</ta>
            <ta e="T35" id="Seg_1861" s="T34">a</ta>
            <ta e="T36" id="Seg_1862" s="T35">a</ta>
            <ta e="T37" id="Seg_1863" s="T36">poʔto</ta>
            <ta e="T38" id="Seg_1864" s="T37">mĭn-laʔbə</ta>
            <ta e="T39" id="Seg_1865" s="T38">dĭn</ta>
            <ta e="T40" id="Seg_1866" s="T39">noʔ</ta>
            <ta e="T41" id="Seg_1867" s="T40">am-laʔbə</ta>
            <ta e="T42" id="Seg_1868" s="T41">dĭgəttə</ta>
            <ta e="T43" id="Seg_1869" s="T42">mĭn-bi</ta>
            <ta e="T44" id="Seg_1870" s="T43">mĭn-bi</ta>
            <ta e="T45" id="Seg_1871" s="T44">dĭ</ta>
            <ta e="T46" id="Seg_1872" s="T45">üjü-t-ziʔ</ta>
            <ta e="T47" id="Seg_1873" s="T46">bar</ta>
            <ta e="T48" id="Seg_1874" s="T47">saʔmə-luʔbdə-bi</ta>
            <ta e="T49" id="Seg_1875" s="T48">ku-liA-t</ta>
            <ta e="T50" id="Seg_1876" s="T49">dĭn</ta>
            <ta e="T51" id="Seg_1877" s="T50">iʔbə-laʔbə</ta>
            <ta e="T52" id="Seg_1878" s="T51">iʔgö</ta>
            <ta e="T53" id="Seg_1879" s="T52">aktʼa</ta>
            <ta e="T54" id="Seg_1880" s="T53">dĭgəttə</ta>
            <ta e="T55" id="Seg_1881" s="T54">büzʼe</ta>
            <ta e="T56" id="Seg_1882" s="T55">šo-bi</ta>
            <ta e="T57" id="Seg_1883" s="T56">ku-liA-t</ta>
            <ta e="T58" id="Seg_1884" s="T57">ku-bi</ta>
            <ta e="T59" id="Seg_1885" s="T58">aktʼa</ta>
            <ta e="T60" id="Seg_1886" s="T59">i</ta>
            <ta e="T61" id="Seg_1887" s="T60">det-bi</ta>
            <ta e="T62" id="Seg_1888" s="T61">maʔ-gəndə</ta>
            <ta e="T63" id="Seg_1889" s="T62">dĭgəttə</ta>
            <ta e="T64" id="Seg_1890" s="T63">măn-ntə</ta>
            <ta e="T65" id="Seg_1891" s="T64">no</ta>
            <ta e="T66" id="Seg_1892" s="T65">miʔnʼibeʔ</ta>
            <ta e="T67" id="Seg_1893" s="T66">kudaj</ta>
            <ta e="T68" id="Seg_1894" s="T67">mĭ-bi</ta>
            <ta e="T69" id="Seg_1895" s="T68">no</ta>
            <ta e="T70" id="Seg_1896" s="T69">kudaj</ta>
            <ta e="T71" id="Seg_1897" s="T70">nʼe</ta>
            <ta e="T72" id="Seg_1898" s="T71">kudaj</ta>
            <ta e="T73" id="Seg_1899" s="T72">dĭ</ta>
            <ta e="T74" id="Seg_1900" s="T73">miʔnʼibeʔ</ta>
            <ta e="T75" id="Seg_1901" s="T74">ku-bi</ta>
            <ta e="T76" id="Seg_1902" s="T75">poʔto</ta>
            <ta e="T77" id="Seg_1903" s="T76">dĭ-m</ta>
            <ta e="T78" id="Seg_1904" s="T77">nadə</ta>
            <ta e="T79" id="Seg_1905" s="T78">tože</ta>
            <ta e="T80" id="Seg_1906" s="T79">am-zittə</ta>
            <ta e="T81" id="Seg_1907" s="T80">mĭ-zittə</ta>
            <ta e="T84" id="Seg_1908" s="T83">dĭ-zAŋ</ta>
            <ta e="T85" id="Seg_1909" s="T84">dĭ-m</ta>
            <ta e="T86" id="Seg_1910" s="T85">tože</ta>
            <ta e="T87" id="Seg_1911" s="T86">ajər-bi-jəʔ</ta>
            <ta e="T88" id="Seg_1912" s="T87">i</ta>
            <ta e="T89" id="Seg_1913" s="T88">bădə-bi-jəʔ</ta>
            <ta e="T90" id="Seg_1914" s="T89">bos-də</ta>
            <ta e="T91" id="Seg_1915" s="T90">bar</ta>
            <ta e="T92" id="Seg_1916" s="T91">dĭgəttə</ta>
            <ta e="T93" id="Seg_1917" s="T92">ĭzem-luʔbdə-bi</ta>
            <ta e="T94" id="Seg_1918" s="T93">dĭ</ta>
            <ta e="T95" id="Seg_1919" s="T94">poʔto</ta>
            <ta e="T96" id="Seg_1920" s="T95">i</ta>
            <ta e="T97" id="Seg_1921" s="T96">kü-laːm-bi</ta>
            <ta e="T98" id="Seg_1922" s="T97">nadə</ta>
            <ta e="T100" id="Seg_1923" s="T99">dĭ-m</ta>
            <ta e="T101" id="Seg_1924" s="T100">barəʔ-zittə</ta>
            <ta e="T102" id="Seg_1925" s="T101">nʼe</ta>
            <ta e="T103" id="Seg_1926" s="T102">nadə</ta>
            <ta e="T104" id="Seg_1927" s="T103">nadə</ta>
            <ta e="T105" id="Seg_1928" s="T104">dĭ-m</ta>
            <ta e="T106" id="Seg_1929" s="T105">kan-zittə</ta>
            <ta e="T107" id="Seg_1930" s="T106">abəs-Tə</ta>
            <ta e="T108" id="Seg_1931" s="T107">da</ta>
            <ta e="T109" id="Seg_1932" s="T108">jakšə-ŋ</ta>
            <ta e="T110" id="Seg_1933" s="T109">hen-zittə</ta>
            <ta e="T111" id="Seg_1934" s="T110">dĭgəttə</ta>
            <ta e="T112" id="Seg_1935" s="T111">kan-bi</ta>
            <ta e="T113" id="Seg_1936" s="T112">abəs-Tə</ta>
            <ta e="T114" id="Seg_1937" s="T113">abəs</ta>
            <ta e="T115" id="Seg_1938" s="T114">măn-ntə</ta>
            <ta e="T116" id="Seg_1939" s="T115">šində</ta>
            <ta e="T117" id="Seg_1940" s="T116">poʔto</ta>
            <ta e="T118" id="Seg_1941" s="T117">nüj-laʔbə</ta>
            <ta e="T119" id="Seg_1942" s="T118">i</ta>
            <ta e="T120" id="Seg_1943" s="T119">davaj</ta>
            <ta e="T121" id="Seg_1944" s="T120">dĭ-m</ta>
            <ta e="T122" id="Seg_1945" s="T121">münör-zittə</ta>
            <ta e="T125" id="Seg_1946" s="T123">dĭ-m</ta>
            <ta e="T126" id="Seg_1947" s="T125">jakšə</ta>
            <ta e="T127" id="Seg_1948" s="T126">i-bi</ta>
            <ta e="T128" id="Seg_1949" s="T127">tănan</ta>
            <ta e="T129" id="Seg_1950" s="T128">aktʼa</ta>
            <ta e="T130" id="Seg_1951" s="T129">ma-bi</ta>
            <ta e="T131" id="Seg_1952" s="T130">šide</ta>
            <ta e="T132" id="Seg_1953" s="T131">biəʔ</ta>
            <ta e="T133" id="Seg_1954" s="T132">dĭgəttə</ta>
            <ta e="T134" id="Seg_1955" s="T133">no</ta>
            <ta e="T135" id="Seg_1956" s="T134">kan-ə-ʔ</ta>
            <ta e="T136" id="Seg_1957" s="T135">ĭmbi</ta>
            <ta e="T137" id="Seg_1958" s="T136">tăn</ta>
            <ta e="T138" id="Seg_1959" s="T137">măna</ta>
            <ta e="T139" id="Seg_1960" s="T138">ej</ta>
            <ta e="T140" id="Seg_1961" s="T139">nörbə-bi-l</ta>
            <ta e="T141" id="Seg_1962" s="T140">dĭ</ta>
            <ta e="T142" id="Seg_1963" s="T141">kan-bi</ta>
            <ta e="T143" id="Seg_1964" s="T142">kan-ə-ʔ</ta>
            <ta e="T144" id="Seg_1965" s="T143">ami</ta>
            <ta e="T146" id="Seg_1966" s="T145">baška</ta>
            <ta e="T147" id="Seg_1967" s="T146">abəs-Tə</ta>
            <ta e="T148" id="Seg_1968" s="T147">dĭbər</ta>
            <ta e="T149" id="Seg_1969" s="T148">kan-bi</ta>
            <ta e="T150" id="Seg_1970" s="T149">dĭ-Tə</ta>
            <ta e="T152" id="Seg_1971" s="T151">tože</ta>
            <ta e="T153" id="Seg_1972" s="T152">nörbə-bi</ta>
            <ta e="T154" id="Seg_1973" s="T153">dĭ</ta>
            <ta e="T155" id="Seg_1974" s="T154">tože</ta>
            <ta e="T156" id="Seg_1975" s="T155">dĭ-m</ta>
            <ta e="T157" id="Seg_1976" s="T156">kabar-bi</ta>
            <ta e="T158" id="Seg_1977" s="T157">da</ta>
            <ta e="T159" id="Seg_1978" s="T158">münör-luʔbdə-bi</ta>
            <ta e="T160" id="Seg_1979" s="T159">e-ʔ</ta>
            <ta e="T161" id="Seg_1980" s="T160">münör-ə-ʔ</ta>
            <ta e="T162" id="Seg_1981" s="T161">dĭ</ta>
            <ta e="T163" id="Seg_1982" s="T162">tănan</ta>
            <ta e="T164" id="Seg_1983" s="T163">aktʼa</ta>
            <ta e="T165" id="Seg_1984" s="T164">ma-bi</ta>
            <ta e="T167" id="Seg_1985" s="T166">šide</ta>
            <ta e="T168" id="Seg_1986" s="T167">biəʔ</ta>
            <ta e="T169" id="Seg_1987" s="T168">aktʼa</ta>
            <ta e="T170" id="Seg_1988" s="T169">tănan</ta>
            <ta e="T171" id="Seg_1989" s="T170">ma-bi</ta>
            <ta e="T172" id="Seg_1990" s="T171">dĭgəttə</ta>
            <ta e="T173" id="Seg_1991" s="T172">dĭ</ta>
            <ta e="T174" id="Seg_1992" s="T173">ĭmbi</ta>
            <ta e="T175" id="Seg_1993" s="T174">tăn</ta>
            <ta e="T176" id="Seg_1994" s="T175">ej</ta>
            <ta e="T177" id="Seg_1995" s="T176">măn-bi-l</ta>
            <ta e="T178" id="Seg_1996" s="T177">kan-ə-ʔ</ta>
            <ta e="T179" id="Seg_1997" s="T178">pušaj</ta>
            <ta e="T180" id="Seg_1998" s="T179">tʼegermaʔ-Tə</ta>
            <ta e="T181" id="Seg_1999" s="T180">sʼa-lV-j</ta>
            <ta e="T182" id="Seg_2000" s="T181">da</ta>
            <ta e="T183" id="Seg_2001" s="T182">kuzur-lV-j</ta>
            <ta e="T184" id="Seg_2002" s="T183">koŋgoro-ziʔ</ta>
            <ta e="T185" id="Seg_2003" s="T184">dĭ</ta>
            <ta e="T186" id="Seg_2004" s="T185">dĭbər</ta>
            <ta e="T187" id="Seg_2005" s="T186">šo-bi</ta>
            <ta e="T188" id="Seg_2006" s="T187">dĭ</ta>
            <ta e="T189" id="Seg_2007" s="T188">tože</ta>
            <ta e="T190" id="Seg_2008" s="T189">dĭ-m</ta>
            <ta e="T191" id="Seg_2009" s="T190">davaj</ta>
            <ta e="T193" id="Seg_2010" s="T192">tănan</ta>
            <ta e="T194" id="Seg_2011" s="T193">biəʔ</ta>
            <ta e="T195" id="Seg_2012" s="T194">sumna</ta>
            <ta e="T196" id="Seg_2013" s="T195">ma-bi</ta>
            <ta e="T197" id="Seg_2014" s="T196">dĭ</ta>
            <ta e="T198" id="Seg_2015" s="T197">poʔto</ta>
            <ta e="T199" id="Seg_2016" s="T198">ĭmbi</ta>
            <ta e="T200" id="Seg_2017" s="T199">tăn</ta>
            <ta e="T201" id="Seg_2018" s="T200">măna</ta>
            <ta e="T202" id="Seg_2019" s="T201">ej</ta>
            <ta e="T203" id="Seg_2020" s="T202">nörbə-bi-l</ta>
            <ta e="T204" id="Seg_2021" s="T203">dĭgəttə</ta>
            <ta e="T205" id="Seg_2022" s="T204">süʔmə-luʔbdə-bi</ta>
            <ta e="T206" id="Seg_2023" s="T205">tʼegermaʔ-Tə</ta>
            <ta e="T207" id="Seg_2024" s="T206">i</ta>
            <ta e="T208" id="Seg_2025" s="T207">davaj</ta>
            <ta e="T209" id="Seg_2026" s="T208">koŋgoro-zittə</ta>
            <ta e="T210" id="Seg_2027" s="T209">koŋgoro-ziʔ</ta>
            <ta e="T211" id="Seg_2028" s="T210">bar</ta>
            <ta e="T212" id="Seg_2029" s="T211">kuzur-zittə</ta>
            <ta e="T213" id="Seg_2030" s="T212">dĭgəttə</ta>
            <ta e="T214" id="Seg_2031" s="T213">kan-bi-jəʔ</ta>
            <ta e="T215" id="Seg_2032" s="T214">dĭ</ta>
            <ta e="T216" id="Seg_2033" s="T215">poʔto</ta>
            <ta e="T217" id="Seg_2034" s="T216">kun-bi-jəʔ</ta>
            <ta e="T218" id="Seg_2035" s="T217">i</ta>
            <ta e="T219" id="Seg_2036" s="T218">tʼo-Tə</ta>
            <ta e="T220" id="Seg_2037" s="T219">kămnə-bi-jəʔ</ta>
            <ta e="T221" id="Seg_2038" s="T220">i</ta>
            <ta e="T222" id="Seg_2039" s="T221">ĭššo</ta>
            <ta e="T223" id="Seg_2040" s="T222">urgo</ta>
            <ta e="T224" id="Seg_2041" s="T223">abəs</ta>
            <ta e="T225" id="Seg_2042" s="T224">tĭm-luʔbdə-bi</ta>
            <ta e="T226" id="Seg_2043" s="T225">i</ta>
            <ta e="T227" id="Seg_2044" s="T226">kăštə-bi</ta>
            <ta e="T228" id="Seg_2045" s="T227">dĭ</ta>
            <ta e="T229" id="Seg_2046" s="T228">büzʼe-m</ta>
            <ta e="T230" id="Seg_2047" s="T229">i</ta>
            <ta e="T231" id="Seg_2048" s="T230">abəs-də</ta>
            <ta e="T232" id="Seg_2049" s="T231">ĭmbi</ta>
            <ta e="T233" id="Seg_2050" s="T232">šiʔ</ta>
            <ta e="T234" id="Seg_2051" s="T233">poʔto</ta>
            <ta e="T235" id="Seg_2052" s="T234">hen-bi-lAʔ</ta>
            <ta e="T236" id="Seg_2053" s="T235">tʼo-Tə</ta>
            <ta e="T237" id="Seg_2054" s="T236">da</ta>
            <ta e="T238" id="Seg_2055" s="T237">dĭ</ta>
            <ta e="T239" id="Seg_2056" s="T238">jakšə</ta>
            <ta e="T240" id="Seg_2057" s="T239">i-bi</ta>
            <ta e="T241" id="Seg_2058" s="T240">tănan</ta>
            <ta e="T242" id="Seg_2059" s="T241">iʔgö</ta>
            <ta e="T243" id="Seg_2060" s="T242">aktʼa</ta>
            <ta e="T244" id="Seg_2061" s="T243">ma-luʔbdə-bi</ta>
            <ta e="T245" id="Seg_2062" s="T244">dĭgəttə</ta>
            <ta e="T246" id="Seg_2063" s="T245">i-bi</ta>
            <ta e="T247" id="Seg_2064" s="T246">aktʼa-bə</ta>
            <ta e="T248" id="Seg_2065" s="T247">i</ta>
            <ta e="T249" id="Seg_2066" s="T248">dĭ-zAŋ</ta>
            <ta e="T250" id="Seg_2067" s="T249">öʔlu-bi</ta>
            <ta e="T251" id="Seg_2068" s="T250">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_2069" s="T0">live-DUR-PST.[3SG]</ta>
            <ta e="T2" id="Seg_2070" s="T1">woman.[NOM.SG]</ta>
            <ta e="T3" id="Seg_2071" s="T2">man.[NOM.SG]</ta>
            <ta e="T4" id="Seg_2072" s="T3">this-PL</ta>
            <ta e="T5" id="Seg_2073" s="T4">what.[NOM.SG]=INDEF</ta>
            <ta e="T6" id="Seg_2074" s="T5">NEG.EX-PST.[3SG]</ta>
            <ta e="T7" id="Seg_2075" s="T6">this.[NOM.SG]</ta>
            <ta e="T8" id="Seg_2076" s="T7">man.[NOM.SG]</ta>
            <ta e="T9" id="Seg_2077" s="T8">plait-DUR-PST.[3SG]</ta>
            <ta e="T10" id="Seg_2078" s="T9">boot-PL</ta>
            <ta e="T11" id="Seg_2079" s="T10">and</ta>
            <ta e="T12" id="Seg_2080" s="T11">sell-PST.[3SG]</ta>
            <ta e="T13" id="Seg_2081" s="T12">so</ta>
            <ta e="T14" id="Seg_2082" s="T13">this-PL</ta>
            <ta e="T15" id="Seg_2083" s="T14">live-PST-3PL</ta>
            <ta e="T16" id="Seg_2084" s="T15">and</ta>
            <ta e="T17" id="Seg_2085" s="T16">this.[NOM.SG]</ta>
            <ta e="T18" id="Seg_2086" s="T17">this-PL-LOC</ta>
            <ta e="T19" id="Seg_2087" s="T18">what.[NOM.SG]=INDEF</ta>
            <ta e="T20" id="Seg_2088" s="T19">NEG.EX-PST.[3SG]</ta>
            <ta e="T21" id="Seg_2089" s="T20">only</ta>
            <ta e="T23" id="Seg_2090" s="T22">one.[NOM.SG]</ta>
            <ta e="T24" id="Seg_2091" s="T23">goat.[NOM.SG]</ta>
            <ta e="T25" id="Seg_2092" s="T24">be-PST.[3SG]</ta>
            <ta e="T26" id="Seg_2093" s="T25">and</ta>
            <ta e="T27" id="Seg_2094" s="T26">go-PST.[3SG]</ta>
            <ta e="T28" id="Seg_2095" s="T27">one.[NOM.SG]</ta>
            <ta e="T29" id="Seg_2096" s="T28">go-PST.[3SG]</ta>
            <ta e="T30" id="Seg_2097" s="T29">forest-LAT</ta>
            <ta e="T31" id="Seg_2098" s="T30">peel.bark-INF.LAT</ta>
            <ta e="T32" id="Seg_2099" s="T31">skin-PL</ta>
            <ta e="T33" id="Seg_2100" s="T32">peel.bark-PST.[3SG]</ta>
            <ta e="T34" id="Seg_2101" s="T33">peel.bark-PST.[3SG]</ta>
            <ta e="T35" id="Seg_2102" s="T34">and</ta>
            <ta e="T36" id="Seg_2103" s="T35">and</ta>
            <ta e="T37" id="Seg_2104" s="T36">goat.[NOM.SG]</ta>
            <ta e="T38" id="Seg_2105" s="T37">go-DUR.[3SG]</ta>
            <ta e="T39" id="Seg_2106" s="T38">there</ta>
            <ta e="T40" id="Seg_2107" s="T39">grass.[NOM.SG]</ta>
            <ta e="T41" id="Seg_2108" s="T40">eat-DUR.[3SG]</ta>
            <ta e="T42" id="Seg_2109" s="T41">then</ta>
            <ta e="T43" id="Seg_2110" s="T42">go-PST.[3SG]</ta>
            <ta e="T44" id="Seg_2111" s="T43">go-PST.[3SG]</ta>
            <ta e="T45" id="Seg_2112" s="T44">this.[NOM.SG]</ta>
            <ta e="T46" id="Seg_2113" s="T45">foot-3SG-INS</ta>
            <ta e="T47" id="Seg_2114" s="T46">PTCL</ta>
            <ta e="T48" id="Seg_2115" s="T47">fall-MOM-PST.[3SG]</ta>
            <ta e="T49" id="Seg_2116" s="T48">see-PRS-3SG.O</ta>
            <ta e="T50" id="Seg_2117" s="T49">there</ta>
            <ta e="T51" id="Seg_2118" s="T50">lie.down-DUR.[3SG]</ta>
            <ta e="T52" id="Seg_2119" s="T51">many</ta>
            <ta e="T53" id="Seg_2120" s="T52">money.[NOM.SG]</ta>
            <ta e="T54" id="Seg_2121" s="T53">then</ta>
            <ta e="T55" id="Seg_2122" s="T54">man.[NOM.SG]</ta>
            <ta e="T56" id="Seg_2123" s="T55">come-PST.[3SG]</ta>
            <ta e="T57" id="Seg_2124" s="T56">see-PRS-3SG.O</ta>
            <ta e="T58" id="Seg_2125" s="T57">see-PST.[3SG]</ta>
            <ta e="T59" id="Seg_2126" s="T58">money.[NOM.SG]</ta>
            <ta e="T60" id="Seg_2127" s="T59">and</ta>
            <ta e="T61" id="Seg_2128" s="T60">bring-PST.[3SG]</ta>
            <ta e="T62" id="Seg_2129" s="T61">tent-LAT/LOC.3SG</ta>
            <ta e="T63" id="Seg_2130" s="T62">then</ta>
            <ta e="T64" id="Seg_2131" s="T63">say-IPFVZ.[3SG]</ta>
            <ta e="T65" id="Seg_2132" s="T64">well</ta>
            <ta e="T66" id="Seg_2133" s="T65">we.LAT</ta>
            <ta e="T67" id="Seg_2134" s="T66">God.[NOM.SG]</ta>
            <ta e="T68" id="Seg_2135" s="T67">give-PST.[3SG]</ta>
            <ta e="T69" id="Seg_2136" s="T68">well</ta>
            <ta e="T70" id="Seg_2137" s="T69">God.[NOM.SG]</ta>
            <ta e="T71" id="Seg_2138" s="T70">not</ta>
            <ta e="T72" id="Seg_2139" s="T71">God.[NOM.SG]</ta>
            <ta e="T73" id="Seg_2140" s="T72">this.[NOM.SG]</ta>
            <ta e="T74" id="Seg_2141" s="T73">we.LAT</ta>
            <ta e="T75" id="Seg_2142" s="T74">find-PST.[3SG]</ta>
            <ta e="T76" id="Seg_2143" s="T75">goat.[NOM.SG]</ta>
            <ta e="T77" id="Seg_2144" s="T76">this-ACC</ta>
            <ta e="T78" id="Seg_2145" s="T77">one.should</ta>
            <ta e="T79" id="Seg_2146" s="T78">also</ta>
            <ta e="T80" id="Seg_2147" s="T79">eat-INF.LAT</ta>
            <ta e="T81" id="Seg_2148" s="T80">give-INF.LAT</ta>
            <ta e="T84" id="Seg_2149" s="T83">this-PL</ta>
            <ta e="T85" id="Seg_2150" s="T84">this-ACC</ta>
            <ta e="T86" id="Seg_2151" s="T85">also</ta>
            <ta e="T87" id="Seg_2152" s="T86">feel.sorry-PST-3PL</ta>
            <ta e="T88" id="Seg_2153" s="T87">and</ta>
            <ta e="T89" id="Seg_2154" s="T88">feed-PST-3PL</ta>
            <ta e="T90" id="Seg_2155" s="T89">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T91" id="Seg_2156" s="T90">PTCL</ta>
            <ta e="T92" id="Seg_2157" s="T91">then</ta>
            <ta e="T93" id="Seg_2158" s="T92">hurt-MOM-PST.[3SG]</ta>
            <ta e="T94" id="Seg_2159" s="T93">this.[NOM.SG]</ta>
            <ta e="T95" id="Seg_2160" s="T94">goat.[NOM.SG]</ta>
            <ta e="T96" id="Seg_2161" s="T95">and</ta>
            <ta e="T97" id="Seg_2162" s="T96">die-RES-PST.[3SG]</ta>
            <ta e="T98" id="Seg_2163" s="T97">one.should</ta>
            <ta e="T100" id="Seg_2164" s="T99">this-ACC</ta>
            <ta e="T101" id="Seg_2165" s="T100">throw.away-INF.LAT</ta>
            <ta e="T102" id="Seg_2166" s="T101">not</ta>
            <ta e="T103" id="Seg_2167" s="T102">one.should</ta>
            <ta e="T104" id="Seg_2168" s="T103">one.should</ta>
            <ta e="T105" id="Seg_2169" s="T104">this-ACC</ta>
            <ta e="T106" id="Seg_2170" s="T105">go-INF.LAT</ta>
            <ta e="T107" id="Seg_2171" s="T106">priest-LAT</ta>
            <ta e="T108" id="Seg_2172" s="T107">and</ta>
            <ta e="T109" id="Seg_2173" s="T108">good-LAT.ADV</ta>
            <ta e="T110" id="Seg_2174" s="T109">put-INF.LAT</ta>
            <ta e="T111" id="Seg_2175" s="T110">then</ta>
            <ta e="T112" id="Seg_2176" s="T111">go-PST.[3SG]</ta>
            <ta e="T113" id="Seg_2177" s="T112">priest-LAT</ta>
            <ta e="T114" id="Seg_2178" s="T113">priest.[NOM.SG]</ta>
            <ta e="T115" id="Seg_2179" s="T114">say-IPFVZ.[3SG]</ta>
            <ta e="T116" id="Seg_2180" s="T115">who.[NOM.SG]</ta>
            <ta e="T117" id="Seg_2181" s="T116">goat.[NOM.SG]</ta>
            <ta e="T118" id="Seg_2182" s="T117">turn-DUR.[3SG]</ta>
            <ta e="T119" id="Seg_2183" s="T118">and</ta>
            <ta e="T120" id="Seg_2184" s="T119">INCH</ta>
            <ta e="T121" id="Seg_2185" s="T120">this-ACC</ta>
            <ta e="T122" id="Seg_2186" s="T121">beat-INF.LAT</ta>
            <ta e="T125" id="Seg_2187" s="T123">this-ACC</ta>
            <ta e="T126" id="Seg_2188" s="T125">good.[NOM.SG]</ta>
            <ta e="T127" id="Seg_2189" s="T126">be-PST.[3SG]</ta>
            <ta e="T128" id="Seg_2190" s="T127">you.DAT</ta>
            <ta e="T129" id="Seg_2191" s="T128">money.[NOM.SG]</ta>
            <ta e="T130" id="Seg_2192" s="T129">remain-PST.[3SG]</ta>
            <ta e="T131" id="Seg_2193" s="T130">two.[NOM.SG]</ta>
            <ta e="T132" id="Seg_2194" s="T131">ten.[NOM.SG]</ta>
            <ta e="T133" id="Seg_2195" s="T132">then</ta>
            <ta e="T134" id="Seg_2196" s="T133">well</ta>
            <ta e="T135" id="Seg_2197" s="T134">go-EP-IMP.2SG</ta>
            <ta e="T136" id="Seg_2198" s="T135">what.[NOM.SG]</ta>
            <ta e="T137" id="Seg_2199" s="T136">you.GEN</ta>
            <ta e="T138" id="Seg_2200" s="T137">I.LAT</ta>
            <ta e="T139" id="Seg_2201" s="T138">NEG</ta>
            <ta e="T140" id="Seg_2202" s="T139">tell-PST-2SG</ta>
            <ta e="T141" id="Seg_2203" s="T140">this.[NOM.SG]</ta>
            <ta e="T142" id="Seg_2204" s="T141">go-PST.[3SG]</ta>
            <ta e="T143" id="Seg_2205" s="T142">go-EP-IMP.2SG</ta>
            <ta e="T144" id="Seg_2206" s="T143">other.[NOM.SG]</ta>
            <ta e="T146" id="Seg_2207" s="T145">another.[NOM.SG]</ta>
            <ta e="T147" id="Seg_2208" s="T146">priest-LAT</ta>
            <ta e="T148" id="Seg_2209" s="T147">there</ta>
            <ta e="T149" id="Seg_2210" s="T148">go-PST.[3SG]</ta>
            <ta e="T150" id="Seg_2211" s="T149">this-LAT</ta>
            <ta e="T152" id="Seg_2212" s="T151">also</ta>
            <ta e="T153" id="Seg_2213" s="T152">tell-PST.[3SG]</ta>
            <ta e="T154" id="Seg_2214" s="T153">this.[NOM.SG]</ta>
            <ta e="T155" id="Seg_2215" s="T154">also</ta>
            <ta e="T156" id="Seg_2216" s="T155">this-ACC</ta>
            <ta e="T157" id="Seg_2217" s="T156">grab-PST.[3SG]</ta>
            <ta e="T158" id="Seg_2218" s="T157">and</ta>
            <ta e="T159" id="Seg_2219" s="T158">beat-MOM-PST.[3SG]</ta>
            <ta e="T160" id="Seg_2220" s="T159">NEG.AUX-IMP.2SG</ta>
            <ta e="T161" id="Seg_2221" s="T160">beat-EP-CNG</ta>
            <ta e="T162" id="Seg_2222" s="T161">this.[NOM.SG]</ta>
            <ta e="T163" id="Seg_2223" s="T162">you.DAT</ta>
            <ta e="T164" id="Seg_2224" s="T163">money.[NOM.SG]</ta>
            <ta e="T165" id="Seg_2225" s="T164">leave-PST.[3SG]</ta>
            <ta e="T167" id="Seg_2226" s="T166">two.[NOM.SG]</ta>
            <ta e="T168" id="Seg_2227" s="T167">ten.[NOM.SG]</ta>
            <ta e="T169" id="Seg_2228" s="T168">money.[NOM.SG]</ta>
            <ta e="T170" id="Seg_2229" s="T169">you.DAT</ta>
            <ta e="T171" id="Seg_2230" s="T170">leave-PST.[3SG]</ta>
            <ta e="T172" id="Seg_2231" s="T171">then</ta>
            <ta e="T173" id="Seg_2232" s="T172">this.[NOM.SG]</ta>
            <ta e="T174" id="Seg_2233" s="T173">what.[NOM.SG]</ta>
            <ta e="T175" id="Seg_2234" s="T174">you.NOM</ta>
            <ta e="T176" id="Seg_2235" s="T175">NEG</ta>
            <ta e="T177" id="Seg_2236" s="T176">say-PST-2SG</ta>
            <ta e="T178" id="Seg_2237" s="T177">go-EP-IMP.2SG</ta>
            <ta e="T179" id="Seg_2238" s="T178">JUSS</ta>
            <ta e="T180" id="Seg_2239" s="T179">bell.tower-LAT</ta>
            <ta e="T181" id="Seg_2240" s="T180">climb-FUT-3SG</ta>
            <ta e="T182" id="Seg_2241" s="T181">and</ta>
            <ta e="T183" id="Seg_2242" s="T182">rumble-FUT-3SG</ta>
            <ta e="T184" id="Seg_2243" s="T183">bell-INS</ta>
            <ta e="T185" id="Seg_2244" s="T184">this.[NOM.SG]</ta>
            <ta e="T186" id="Seg_2245" s="T185">there</ta>
            <ta e="T187" id="Seg_2246" s="T186">come-PST.[3SG]</ta>
            <ta e="T188" id="Seg_2247" s="T187">this.[NOM.SG]</ta>
            <ta e="T189" id="Seg_2248" s="T188">also</ta>
            <ta e="T190" id="Seg_2249" s="T189">this-ACC</ta>
            <ta e="T191" id="Seg_2250" s="T190">INCH</ta>
            <ta e="T193" id="Seg_2251" s="T192">you.DAT</ta>
            <ta e="T194" id="Seg_2252" s="T193">ten.[NOM.SG]</ta>
            <ta e="T195" id="Seg_2253" s="T194">five.[NOM.SG]</ta>
            <ta e="T196" id="Seg_2254" s="T195">leave-PST.[3SG]</ta>
            <ta e="T197" id="Seg_2255" s="T196">this.[NOM.SG]</ta>
            <ta e="T198" id="Seg_2256" s="T197">goat.[NOM.SG]</ta>
            <ta e="T199" id="Seg_2257" s="T198">what.[NOM.SG]</ta>
            <ta e="T200" id="Seg_2258" s="T199">you.GEN</ta>
            <ta e="T201" id="Seg_2259" s="T200">I.LAT</ta>
            <ta e="T202" id="Seg_2260" s="T201">NEG</ta>
            <ta e="T203" id="Seg_2261" s="T202">tell-PST-2SG</ta>
            <ta e="T204" id="Seg_2262" s="T203">then</ta>
            <ta e="T205" id="Seg_2263" s="T204">jump-MOM-PST.[3SG]</ta>
            <ta e="T206" id="Seg_2264" s="T205">bell.tower-LAT</ta>
            <ta e="T207" id="Seg_2265" s="T206">and</ta>
            <ta e="T208" id="Seg_2266" s="T207">INCH</ta>
            <ta e="T209" id="Seg_2267" s="T208">bell-INF.LAT</ta>
            <ta e="T210" id="Seg_2268" s="T209">bell-INS</ta>
            <ta e="T211" id="Seg_2269" s="T210">PTCL</ta>
            <ta e="T212" id="Seg_2270" s="T211">rumble-INF.LAT</ta>
            <ta e="T213" id="Seg_2271" s="T212">then</ta>
            <ta e="T214" id="Seg_2272" s="T213">go-PST-3PL</ta>
            <ta e="T215" id="Seg_2273" s="T214">this.[NOM.SG]</ta>
            <ta e="T216" id="Seg_2274" s="T215">goat.[NOM.SG]</ta>
            <ta e="T217" id="Seg_2275" s="T216">bring-PST-3PL</ta>
            <ta e="T218" id="Seg_2276" s="T217">and</ta>
            <ta e="T219" id="Seg_2277" s="T218">place-LAT</ta>
            <ta e="T220" id="Seg_2278" s="T219">pour-PST-3PL</ta>
            <ta e="T221" id="Seg_2279" s="T220">and</ta>
            <ta e="T222" id="Seg_2280" s="T221">more</ta>
            <ta e="T223" id="Seg_2281" s="T222">big.[NOM.SG]</ta>
            <ta e="T224" id="Seg_2282" s="T223">priest.[NOM.SG]</ta>
            <ta e="T225" id="Seg_2283" s="T224">know-MOM-PST.[3SG]</ta>
            <ta e="T226" id="Seg_2284" s="T225">and</ta>
            <ta e="T227" id="Seg_2285" s="T226">call-PST.[3SG]</ta>
            <ta e="T228" id="Seg_2286" s="T227">this.[NOM.SG]</ta>
            <ta e="T229" id="Seg_2287" s="T228">man-ACC</ta>
            <ta e="T230" id="Seg_2288" s="T229">and</ta>
            <ta e="T231" id="Seg_2289" s="T230">priest-NOM/GEN/ACC.3SG</ta>
            <ta e="T232" id="Seg_2290" s="T231">what.[NOM.SG]</ta>
            <ta e="T233" id="Seg_2291" s="T232">you.PL.NOM</ta>
            <ta e="T234" id="Seg_2292" s="T233">goat.[NOM.SG]</ta>
            <ta e="T235" id="Seg_2293" s="T234">put-PST-2PL</ta>
            <ta e="T236" id="Seg_2294" s="T235">place-LAT</ta>
            <ta e="T237" id="Seg_2295" s="T236">and</ta>
            <ta e="T238" id="Seg_2296" s="T237">this.[NOM.SG]</ta>
            <ta e="T239" id="Seg_2297" s="T238">good.[NOM.SG]</ta>
            <ta e="T240" id="Seg_2298" s="T239">be-PST.[3SG]</ta>
            <ta e="T241" id="Seg_2299" s="T240">you.DAT</ta>
            <ta e="T242" id="Seg_2300" s="T241">many</ta>
            <ta e="T243" id="Seg_2301" s="T242">money.[NOM.SG]</ta>
            <ta e="T244" id="Seg_2302" s="T243">leave-MOM-PST.[3SG]</ta>
            <ta e="T245" id="Seg_2303" s="T244">then</ta>
            <ta e="T246" id="Seg_2304" s="T245">take-PST.[3SG]</ta>
            <ta e="T247" id="Seg_2305" s="T246">money-ACC.3SG</ta>
            <ta e="T248" id="Seg_2306" s="T247">and</ta>
            <ta e="T249" id="Seg_2307" s="T248">this-PL</ta>
            <ta e="T250" id="Seg_2308" s="T249">send-PST.[3SG]</ta>
            <ta e="T251" id="Seg_2309" s="T250">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_2310" s="T0">жить-DUR-PST.[3SG]</ta>
            <ta e="T2" id="Seg_2311" s="T1">женщина.[NOM.SG]</ta>
            <ta e="T3" id="Seg_2312" s="T2">мужчина.[NOM.SG]</ta>
            <ta e="T4" id="Seg_2313" s="T3">этот-PL</ta>
            <ta e="T5" id="Seg_2314" s="T4">что.[NOM.SG]=INDEF</ta>
            <ta e="T6" id="Seg_2315" s="T5">NEG.EX-PST.[3SG]</ta>
            <ta e="T7" id="Seg_2316" s="T6">этот.[NOM.SG]</ta>
            <ta e="T8" id="Seg_2317" s="T7">мужчина.[NOM.SG]</ta>
            <ta e="T9" id="Seg_2318" s="T8">плести-DUR-PST.[3SG]</ta>
            <ta e="T10" id="Seg_2319" s="T9">сапог-PL</ta>
            <ta e="T11" id="Seg_2320" s="T10">и</ta>
            <ta e="T12" id="Seg_2321" s="T11">продавать-PST.[3SG]</ta>
            <ta e="T13" id="Seg_2322" s="T12">так</ta>
            <ta e="T14" id="Seg_2323" s="T13">этот-PL</ta>
            <ta e="T15" id="Seg_2324" s="T14">жить-PST-3PL</ta>
            <ta e="T16" id="Seg_2325" s="T15">а</ta>
            <ta e="T17" id="Seg_2326" s="T16">этот.[NOM.SG]</ta>
            <ta e="T18" id="Seg_2327" s="T17">этот-PL-LOC</ta>
            <ta e="T19" id="Seg_2328" s="T18">что.[NOM.SG]=INDEF</ta>
            <ta e="T20" id="Seg_2329" s="T19">NEG.EX-PST.[3SG]</ta>
            <ta e="T21" id="Seg_2330" s="T20">только</ta>
            <ta e="T23" id="Seg_2331" s="T22">один.[NOM.SG]</ta>
            <ta e="T24" id="Seg_2332" s="T23">коза.[NOM.SG]</ta>
            <ta e="T25" id="Seg_2333" s="T24">быть-PST.[3SG]</ta>
            <ta e="T26" id="Seg_2334" s="T25">и</ta>
            <ta e="T27" id="Seg_2335" s="T26">пойти-PST.[3SG]</ta>
            <ta e="T28" id="Seg_2336" s="T27">один.[NOM.SG]</ta>
            <ta e="T29" id="Seg_2337" s="T28">пойти-PST.[3SG]</ta>
            <ta e="T30" id="Seg_2338" s="T29">лес-LAT</ta>
            <ta e="T31" id="Seg_2339" s="T30">снимать.кору-INF.LAT</ta>
            <ta e="T32" id="Seg_2340" s="T31">кожа-PL</ta>
            <ta e="T33" id="Seg_2341" s="T32">снимать.кору-PST.[3SG]</ta>
            <ta e="T34" id="Seg_2342" s="T33">снимать.кору-PST.[3SG]</ta>
            <ta e="T35" id="Seg_2343" s="T34">а</ta>
            <ta e="T36" id="Seg_2344" s="T35">а</ta>
            <ta e="T37" id="Seg_2345" s="T36">коза.[NOM.SG]</ta>
            <ta e="T38" id="Seg_2346" s="T37">идти-DUR.[3SG]</ta>
            <ta e="T39" id="Seg_2347" s="T38">там</ta>
            <ta e="T40" id="Seg_2348" s="T39">трава.[NOM.SG]</ta>
            <ta e="T41" id="Seg_2349" s="T40">съесть-DUR.[3SG]</ta>
            <ta e="T42" id="Seg_2350" s="T41">тогда</ta>
            <ta e="T43" id="Seg_2351" s="T42">идти-PST.[3SG]</ta>
            <ta e="T44" id="Seg_2352" s="T43">идти-PST.[3SG]</ta>
            <ta e="T45" id="Seg_2353" s="T44">этот.[NOM.SG]</ta>
            <ta e="T46" id="Seg_2354" s="T45">нога-3SG-INS</ta>
            <ta e="T47" id="Seg_2355" s="T46">PTCL</ta>
            <ta e="T48" id="Seg_2356" s="T47">упасть-MOM-PST.[3SG]</ta>
            <ta e="T49" id="Seg_2357" s="T48">видеть-PRS-3SG.O</ta>
            <ta e="T50" id="Seg_2358" s="T49">там</ta>
            <ta e="T51" id="Seg_2359" s="T50">ложиться-DUR.[3SG]</ta>
            <ta e="T52" id="Seg_2360" s="T51">много</ta>
            <ta e="T53" id="Seg_2361" s="T52">деньги.[NOM.SG]</ta>
            <ta e="T54" id="Seg_2362" s="T53">тогда</ta>
            <ta e="T55" id="Seg_2363" s="T54">мужчина.[NOM.SG]</ta>
            <ta e="T56" id="Seg_2364" s="T55">прийти-PST.[3SG]</ta>
            <ta e="T57" id="Seg_2365" s="T56">видеть-PRS-3SG.O</ta>
            <ta e="T58" id="Seg_2366" s="T57">видеть-PST.[3SG]</ta>
            <ta e="T59" id="Seg_2367" s="T58">деньги.[NOM.SG]</ta>
            <ta e="T60" id="Seg_2368" s="T59">и</ta>
            <ta e="T61" id="Seg_2369" s="T60">принести-PST.[3SG]</ta>
            <ta e="T62" id="Seg_2370" s="T61">чум-LAT/LOC.3SG</ta>
            <ta e="T63" id="Seg_2371" s="T62">тогда</ta>
            <ta e="T64" id="Seg_2372" s="T63">сказать-IPFVZ.[3SG]</ta>
            <ta e="T65" id="Seg_2373" s="T64">ну</ta>
            <ta e="T66" id="Seg_2374" s="T65">мы.LAT</ta>
            <ta e="T67" id="Seg_2375" s="T66">бог.[NOM.SG]</ta>
            <ta e="T68" id="Seg_2376" s="T67">дать-PST.[3SG]</ta>
            <ta e="T69" id="Seg_2377" s="T68">ну</ta>
            <ta e="T70" id="Seg_2378" s="T69">бог.[NOM.SG]</ta>
            <ta e="T71" id="Seg_2379" s="T70">не</ta>
            <ta e="T72" id="Seg_2380" s="T71">бог.[NOM.SG]</ta>
            <ta e="T73" id="Seg_2381" s="T72">этот.[NOM.SG]</ta>
            <ta e="T74" id="Seg_2382" s="T73">мы.LAT</ta>
            <ta e="T75" id="Seg_2383" s="T74">найти-PST.[3SG]</ta>
            <ta e="T76" id="Seg_2384" s="T75">коза.[NOM.SG]</ta>
            <ta e="T77" id="Seg_2385" s="T76">этот-ACC</ta>
            <ta e="T78" id="Seg_2386" s="T77">надо</ta>
            <ta e="T79" id="Seg_2387" s="T78">тоже</ta>
            <ta e="T80" id="Seg_2388" s="T79">съесть-INF.LAT</ta>
            <ta e="T81" id="Seg_2389" s="T80">дать-INF.LAT</ta>
            <ta e="T84" id="Seg_2390" s="T83">этот-PL</ta>
            <ta e="T85" id="Seg_2391" s="T84">этот-ACC</ta>
            <ta e="T86" id="Seg_2392" s="T85">тоже</ta>
            <ta e="T87" id="Seg_2393" s="T86">жалеть-PST-3PL</ta>
            <ta e="T88" id="Seg_2394" s="T87">и</ta>
            <ta e="T89" id="Seg_2395" s="T88">кормить-PST-3PL</ta>
            <ta e="T90" id="Seg_2396" s="T89">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T91" id="Seg_2397" s="T90">PTCL</ta>
            <ta e="T92" id="Seg_2398" s="T91">тогда</ta>
            <ta e="T93" id="Seg_2399" s="T92">болеть-MOM-PST.[3SG]</ta>
            <ta e="T94" id="Seg_2400" s="T93">этот.[NOM.SG]</ta>
            <ta e="T95" id="Seg_2401" s="T94">коза.[NOM.SG]</ta>
            <ta e="T96" id="Seg_2402" s="T95">и</ta>
            <ta e="T97" id="Seg_2403" s="T96">умереть-RES-PST.[3SG]</ta>
            <ta e="T98" id="Seg_2404" s="T97">надо</ta>
            <ta e="T100" id="Seg_2405" s="T99">этот-ACC</ta>
            <ta e="T101" id="Seg_2406" s="T100">выбросить-INF.LAT</ta>
            <ta e="T102" id="Seg_2407" s="T101">не</ta>
            <ta e="T103" id="Seg_2408" s="T102">надо</ta>
            <ta e="T104" id="Seg_2409" s="T103">надо</ta>
            <ta e="T105" id="Seg_2410" s="T104">этот-ACC</ta>
            <ta e="T106" id="Seg_2411" s="T105">пойти-INF.LAT</ta>
            <ta e="T107" id="Seg_2412" s="T106">священник-LAT</ta>
            <ta e="T108" id="Seg_2413" s="T107">и</ta>
            <ta e="T109" id="Seg_2414" s="T108">хороший-LAT.ADV</ta>
            <ta e="T110" id="Seg_2415" s="T109">класть-INF.LAT</ta>
            <ta e="T111" id="Seg_2416" s="T110">тогда</ta>
            <ta e="T112" id="Seg_2417" s="T111">пойти-PST.[3SG]</ta>
            <ta e="T113" id="Seg_2418" s="T112">священник-LAT</ta>
            <ta e="T114" id="Seg_2419" s="T113">священник.[NOM.SG]</ta>
            <ta e="T115" id="Seg_2420" s="T114">сказать-IPFVZ.[3SG]</ta>
            <ta e="T116" id="Seg_2421" s="T115">кто.[NOM.SG]</ta>
            <ta e="T117" id="Seg_2422" s="T116">коза.[NOM.SG]</ta>
            <ta e="T118" id="Seg_2423" s="T117">крутить-DUR.[3SG]</ta>
            <ta e="T119" id="Seg_2424" s="T118">и</ta>
            <ta e="T120" id="Seg_2425" s="T119">INCH</ta>
            <ta e="T121" id="Seg_2426" s="T120">этот-ACC</ta>
            <ta e="T122" id="Seg_2427" s="T121">бить-INF.LAT</ta>
            <ta e="T125" id="Seg_2428" s="T123">этот-ACC</ta>
            <ta e="T126" id="Seg_2429" s="T125">хороший.[NOM.SG]</ta>
            <ta e="T127" id="Seg_2430" s="T126">быть-PST.[3SG]</ta>
            <ta e="T128" id="Seg_2431" s="T127">ты.DAT</ta>
            <ta e="T129" id="Seg_2432" s="T128">деньги.[NOM.SG]</ta>
            <ta e="T130" id="Seg_2433" s="T129">остаться-PST.[3SG]</ta>
            <ta e="T131" id="Seg_2434" s="T130">два.[NOM.SG]</ta>
            <ta e="T132" id="Seg_2435" s="T131">десять.[NOM.SG]</ta>
            <ta e="T133" id="Seg_2436" s="T132">тогда</ta>
            <ta e="T134" id="Seg_2437" s="T133">ну</ta>
            <ta e="T135" id="Seg_2438" s="T134">пойти-EP-IMP.2SG</ta>
            <ta e="T136" id="Seg_2439" s="T135">что.[NOM.SG]</ta>
            <ta e="T137" id="Seg_2440" s="T136">ты.GEN</ta>
            <ta e="T138" id="Seg_2441" s="T137">я.LAT</ta>
            <ta e="T139" id="Seg_2442" s="T138">NEG</ta>
            <ta e="T140" id="Seg_2443" s="T139">сказать-PST-2SG</ta>
            <ta e="T141" id="Seg_2444" s="T140">этот.[NOM.SG]</ta>
            <ta e="T142" id="Seg_2445" s="T141">пойти-PST.[3SG]</ta>
            <ta e="T143" id="Seg_2446" s="T142">пойти-EP-IMP.2SG</ta>
            <ta e="T144" id="Seg_2447" s="T143">другой.[NOM.SG]</ta>
            <ta e="T146" id="Seg_2448" s="T145">другой.[NOM.SG]</ta>
            <ta e="T147" id="Seg_2449" s="T146">священник-LAT</ta>
            <ta e="T148" id="Seg_2450" s="T147">там</ta>
            <ta e="T149" id="Seg_2451" s="T148">пойти-PST.[3SG]</ta>
            <ta e="T150" id="Seg_2452" s="T149">этот-LAT</ta>
            <ta e="T152" id="Seg_2453" s="T151">тоже</ta>
            <ta e="T153" id="Seg_2454" s="T152">сказать-PST.[3SG]</ta>
            <ta e="T154" id="Seg_2455" s="T153">этот.[NOM.SG]</ta>
            <ta e="T155" id="Seg_2456" s="T154">тоже</ta>
            <ta e="T156" id="Seg_2457" s="T155">этот-ACC</ta>
            <ta e="T157" id="Seg_2458" s="T156">хватать-PST.[3SG]</ta>
            <ta e="T158" id="Seg_2459" s="T157">и</ta>
            <ta e="T159" id="Seg_2460" s="T158">бить-MOM-PST.[3SG]</ta>
            <ta e="T160" id="Seg_2461" s="T159">NEG.AUX-IMP.2SG</ta>
            <ta e="T161" id="Seg_2462" s="T160">бить-EP-CNG</ta>
            <ta e="T162" id="Seg_2463" s="T161">этот.[NOM.SG]</ta>
            <ta e="T163" id="Seg_2464" s="T162">ты.DAT</ta>
            <ta e="T164" id="Seg_2465" s="T163">деньги.[NOM.SG]</ta>
            <ta e="T165" id="Seg_2466" s="T164">оставить-PST.[3SG]</ta>
            <ta e="T167" id="Seg_2467" s="T166">два.[NOM.SG]</ta>
            <ta e="T168" id="Seg_2468" s="T167">десять.[NOM.SG]</ta>
            <ta e="T169" id="Seg_2469" s="T168">деньги.[NOM.SG]</ta>
            <ta e="T170" id="Seg_2470" s="T169">ты.DAT</ta>
            <ta e="T171" id="Seg_2471" s="T170">оставить-PST.[3SG]</ta>
            <ta e="T172" id="Seg_2472" s="T171">тогда</ta>
            <ta e="T173" id="Seg_2473" s="T172">этот.[NOM.SG]</ta>
            <ta e="T174" id="Seg_2474" s="T173">что.[NOM.SG]</ta>
            <ta e="T175" id="Seg_2475" s="T174">ты.NOM</ta>
            <ta e="T176" id="Seg_2476" s="T175">NEG</ta>
            <ta e="T177" id="Seg_2477" s="T176">сказать-PST-2SG</ta>
            <ta e="T178" id="Seg_2478" s="T177">пойти-EP-IMP.2SG</ta>
            <ta e="T179" id="Seg_2479" s="T178">JUSS</ta>
            <ta e="T180" id="Seg_2480" s="T179">колокольня-LAT</ta>
            <ta e="T181" id="Seg_2481" s="T180">влезать-FUT-3SG</ta>
            <ta e="T182" id="Seg_2482" s="T181">и</ta>
            <ta e="T183" id="Seg_2483" s="T182">греметь-FUT-3SG</ta>
            <ta e="T184" id="Seg_2484" s="T183">колокол-INS</ta>
            <ta e="T185" id="Seg_2485" s="T184">этот.[NOM.SG]</ta>
            <ta e="T186" id="Seg_2486" s="T185">там</ta>
            <ta e="T187" id="Seg_2487" s="T186">прийти-PST.[3SG]</ta>
            <ta e="T188" id="Seg_2488" s="T187">этот.[NOM.SG]</ta>
            <ta e="T189" id="Seg_2489" s="T188">тоже</ta>
            <ta e="T190" id="Seg_2490" s="T189">этот-ACC</ta>
            <ta e="T191" id="Seg_2491" s="T190">INCH</ta>
            <ta e="T193" id="Seg_2492" s="T192">ты.DAT</ta>
            <ta e="T194" id="Seg_2493" s="T193">десять.[NOM.SG]</ta>
            <ta e="T195" id="Seg_2494" s="T194">пять.[NOM.SG]</ta>
            <ta e="T196" id="Seg_2495" s="T195">оставить-PST.[3SG]</ta>
            <ta e="T197" id="Seg_2496" s="T196">этот.[NOM.SG]</ta>
            <ta e="T198" id="Seg_2497" s="T197">коза.[NOM.SG]</ta>
            <ta e="T199" id="Seg_2498" s="T198">что.[NOM.SG]</ta>
            <ta e="T200" id="Seg_2499" s="T199">ты.GEN</ta>
            <ta e="T201" id="Seg_2500" s="T200">я.LAT</ta>
            <ta e="T202" id="Seg_2501" s="T201">NEG</ta>
            <ta e="T203" id="Seg_2502" s="T202">сказать-PST-2SG</ta>
            <ta e="T204" id="Seg_2503" s="T203">тогда</ta>
            <ta e="T205" id="Seg_2504" s="T204">прыгнуть-MOM-PST.[3SG]</ta>
            <ta e="T206" id="Seg_2505" s="T205">колокольня-LAT</ta>
            <ta e="T207" id="Seg_2506" s="T206">и</ta>
            <ta e="T208" id="Seg_2507" s="T207">INCH</ta>
            <ta e="T209" id="Seg_2508" s="T208">колокол-INF.LAT</ta>
            <ta e="T210" id="Seg_2509" s="T209">колокол-INS</ta>
            <ta e="T211" id="Seg_2510" s="T210">PTCL</ta>
            <ta e="T212" id="Seg_2511" s="T211">греметь-INF.LAT</ta>
            <ta e="T213" id="Seg_2512" s="T212">тогда</ta>
            <ta e="T214" id="Seg_2513" s="T213">пойти-PST-3PL</ta>
            <ta e="T215" id="Seg_2514" s="T214">этот.[NOM.SG]</ta>
            <ta e="T216" id="Seg_2515" s="T215">коза.[NOM.SG]</ta>
            <ta e="T217" id="Seg_2516" s="T216">нести-PST-3PL</ta>
            <ta e="T218" id="Seg_2517" s="T217">и</ta>
            <ta e="T219" id="Seg_2518" s="T218">место-LAT</ta>
            <ta e="T220" id="Seg_2519" s="T219">лить-PST-3PL</ta>
            <ta e="T221" id="Seg_2520" s="T220">и</ta>
            <ta e="T222" id="Seg_2521" s="T221">еще</ta>
            <ta e="T223" id="Seg_2522" s="T222">большой.[NOM.SG]</ta>
            <ta e="T224" id="Seg_2523" s="T223">священник.[NOM.SG]</ta>
            <ta e="T225" id="Seg_2524" s="T224">знать-MOM-PST.[3SG]</ta>
            <ta e="T226" id="Seg_2525" s="T225">и</ta>
            <ta e="T227" id="Seg_2526" s="T226">позвать-PST.[3SG]</ta>
            <ta e="T228" id="Seg_2527" s="T227">этот.[NOM.SG]</ta>
            <ta e="T229" id="Seg_2528" s="T228">мужчина-ACC</ta>
            <ta e="T230" id="Seg_2529" s="T229">и</ta>
            <ta e="T231" id="Seg_2530" s="T230">священник-NOM/GEN/ACC.3SG</ta>
            <ta e="T232" id="Seg_2531" s="T231">что.[NOM.SG]</ta>
            <ta e="T233" id="Seg_2532" s="T232">вы.NOM</ta>
            <ta e="T234" id="Seg_2533" s="T233">коза.[NOM.SG]</ta>
            <ta e="T235" id="Seg_2534" s="T234">класть-PST-2PL</ta>
            <ta e="T236" id="Seg_2535" s="T235">место-LAT</ta>
            <ta e="T237" id="Seg_2536" s="T236">и</ta>
            <ta e="T238" id="Seg_2537" s="T237">этот.[NOM.SG]</ta>
            <ta e="T239" id="Seg_2538" s="T238">хороший.[NOM.SG]</ta>
            <ta e="T240" id="Seg_2539" s="T239">быть-PST.[3SG]</ta>
            <ta e="T241" id="Seg_2540" s="T240">ты.DAT</ta>
            <ta e="T242" id="Seg_2541" s="T241">много</ta>
            <ta e="T243" id="Seg_2542" s="T242">деньги.[NOM.SG]</ta>
            <ta e="T244" id="Seg_2543" s="T243">оставить-MOM-PST.[3SG]</ta>
            <ta e="T245" id="Seg_2544" s="T244">тогда</ta>
            <ta e="T246" id="Seg_2545" s="T245">взять-PST.[3SG]</ta>
            <ta e="T247" id="Seg_2546" s="T246">деньги-ACC.3SG</ta>
            <ta e="T248" id="Seg_2547" s="T247">и</ta>
            <ta e="T249" id="Seg_2548" s="T248">этот-PL</ta>
            <ta e="T250" id="Seg_2549" s="T249">посылать-PST.[3SG]</ta>
            <ta e="T251" id="Seg_2550" s="T250">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_2551" s="T0">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T2" id="Seg_2552" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_2553" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_2554" s="T3">dempro-n:num</ta>
            <ta e="T5" id="Seg_2555" s="T4">que-n:case=ptcl</ta>
            <ta e="T6" id="Seg_2556" s="T5">v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_2557" s="T6">dempro-n:case</ta>
            <ta e="T8" id="Seg_2558" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_2559" s="T8">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T10" id="Seg_2560" s="T9">n-n:num</ta>
            <ta e="T11" id="Seg_2561" s="T10">conj</ta>
            <ta e="T12" id="Seg_2562" s="T11">v-v:tense-v:pn</ta>
            <ta e="T13" id="Seg_2563" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_2564" s="T13">dempro-n:num</ta>
            <ta e="T15" id="Seg_2565" s="T14">v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_2566" s="T15">conj</ta>
            <ta e="T17" id="Seg_2567" s="T16">dempro-n:case</ta>
            <ta e="T18" id="Seg_2568" s="T17">dempro-n:num-n:case</ta>
            <ta e="T19" id="Seg_2569" s="T18">que-n:case=ptcl</ta>
            <ta e="T20" id="Seg_2570" s="T19">v-v:tense-v:pn</ta>
            <ta e="T21" id="Seg_2571" s="T20">adv</ta>
            <ta e="T23" id="Seg_2572" s="T22">num-n:case</ta>
            <ta e="T24" id="Seg_2573" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_2574" s="T24">v-v:tense-v:pn</ta>
            <ta e="T26" id="Seg_2575" s="T25">conj</ta>
            <ta e="T27" id="Seg_2576" s="T26">v-v:tense-v:pn</ta>
            <ta e="T28" id="Seg_2577" s="T27">num-n:case</ta>
            <ta e="T29" id="Seg_2578" s="T28">v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_2579" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_2580" s="T30">v-v:n.fin</ta>
            <ta e="T32" id="Seg_2581" s="T31">n-n:num</ta>
            <ta e="T33" id="Seg_2582" s="T32">v-v:tense-v:pn</ta>
            <ta e="T34" id="Seg_2583" s="T33">v-v:tense-v:pn</ta>
            <ta e="T35" id="Seg_2584" s="T34">conj</ta>
            <ta e="T36" id="Seg_2585" s="T35">conj</ta>
            <ta e="T37" id="Seg_2586" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_2587" s="T37">v-v&gt;v-v:pn</ta>
            <ta e="T39" id="Seg_2588" s="T38">adv</ta>
            <ta e="T40" id="Seg_2589" s="T39">n-n:case</ta>
            <ta e="T41" id="Seg_2590" s="T40">v-v&gt;v-v:pn</ta>
            <ta e="T42" id="Seg_2591" s="T41">adv</ta>
            <ta e="T43" id="Seg_2592" s="T42">v-v:tense-v:pn</ta>
            <ta e="T44" id="Seg_2593" s="T43">v-v:tense-v:pn</ta>
            <ta e="T45" id="Seg_2594" s="T44">dempro-n:case</ta>
            <ta e="T46" id="Seg_2595" s="T45">n-n:case.poss-n:case</ta>
            <ta e="T47" id="Seg_2596" s="T46">ptcl</ta>
            <ta e="T48" id="Seg_2597" s="T47">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T49" id="Seg_2598" s="T48">v-v:tense-v:pn</ta>
            <ta e="T50" id="Seg_2599" s="T49">adv</ta>
            <ta e="T51" id="Seg_2600" s="T50">v-v&gt;v-v:pn</ta>
            <ta e="T52" id="Seg_2601" s="T51">quant</ta>
            <ta e="T53" id="Seg_2602" s="T52">n-n:case</ta>
            <ta e="T54" id="Seg_2603" s="T53">adv</ta>
            <ta e="T55" id="Seg_2604" s="T54">n-n:case</ta>
            <ta e="T56" id="Seg_2605" s="T55">v-v:tense-v:pn</ta>
            <ta e="T57" id="Seg_2606" s="T56">v-v:tense-v:pn</ta>
            <ta e="T58" id="Seg_2607" s="T57">v-v:tense-v:pn</ta>
            <ta e="T59" id="Seg_2608" s="T58">n-n:case</ta>
            <ta e="T60" id="Seg_2609" s="T59">conj</ta>
            <ta e="T61" id="Seg_2610" s="T60">v-v:tense-v:pn</ta>
            <ta e="T62" id="Seg_2611" s="T61">n-n:case.poss</ta>
            <ta e="T63" id="Seg_2612" s="T62">adv</ta>
            <ta e="T64" id="Seg_2613" s="T63">v-v&gt;v-v:pn</ta>
            <ta e="T65" id="Seg_2614" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_2615" s="T65">pers</ta>
            <ta e="T67" id="Seg_2616" s="T66">n-n:case</ta>
            <ta e="T68" id="Seg_2617" s="T67">v-v:tense-v:pn</ta>
            <ta e="T69" id="Seg_2618" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_2619" s="T69">n-n:case</ta>
            <ta e="T71" id="Seg_2620" s="T70">ptcl</ta>
            <ta e="T72" id="Seg_2621" s="T71">n-n:case</ta>
            <ta e="T73" id="Seg_2622" s="T72">dempro-n:case</ta>
            <ta e="T74" id="Seg_2623" s="T73">pers</ta>
            <ta e="T75" id="Seg_2624" s="T74">v-v:tense-v:pn</ta>
            <ta e="T76" id="Seg_2625" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_2626" s="T76">dempro-n:case</ta>
            <ta e="T78" id="Seg_2627" s="T77">ptcl</ta>
            <ta e="T79" id="Seg_2628" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_2629" s="T79">v-v:n.fin</ta>
            <ta e="T81" id="Seg_2630" s="T80">v-v:n.fin</ta>
            <ta e="T84" id="Seg_2631" s="T83">dempro-n:num</ta>
            <ta e="T85" id="Seg_2632" s="T84">dempro-n:case</ta>
            <ta e="T86" id="Seg_2633" s="T85">ptcl</ta>
            <ta e="T87" id="Seg_2634" s="T86">v-v:tense-v:pn</ta>
            <ta e="T88" id="Seg_2635" s="T87">conj</ta>
            <ta e="T89" id="Seg_2636" s="T88">v-v:tense-v:pn</ta>
            <ta e="T90" id="Seg_2637" s="T89">refl-n:case.poss</ta>
            <ta e="T91" id="Seg_2638" s="T90">ptcl</ta>
            <ta e="T92" id="Seg_2639" s="T91">adv</ta>
            <ta e="T93" id="Seg_2640" s="T92">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T94" id="Seg_2641" s="T93">dempro-n:case</ta>
            <ta e="T95" id="Seg_2642" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_2643" s="T95">conj</ta>
            <ta e="T97" id="Seg_2644" s="T96">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T98" id="Seg_2645" s="T97">ptcl</ta>
            <ta e="T100" id="Seg_2646" s="T99">dempro-n:case</ta>
            <ta e="T101" id="Seg_2647" s="T100">v-v:n.fin</ta>
            <ta e="T102" id="Seg_2648" s="T101">ptcl</ta>
            <ta e="T103" id="Seg_2649" s="T102">ptcl</ta>
            <ta e="T104" id="Seg_2650" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_2651" s="T104">dempro-n:case</ta>
            <ta e="T106" id="Seg_2652" s="T105">v-v:n.fin</ta>
            <ta e="T107" id="Seg_2653" s="T106">n-n:case</ta>
            <ta e="T108" id="Seg_2654" s="T107">conj</ta>
            <ta e="T109" id="Seg_2655" s="T108">adj-adj&gt;adv</ta>
            <ta e="T110" id="Seg_2656" s="T109">v-v:n.fin</ta>
            <ta e="T111" id="Seg_2657" s="T110">adv</ta>
            <ta e="T112" id="Seg_2658" s="T111">v-v:tense-v:pn</ta>
            <ta e="T113" id="Seg_2659" s="T112">n-n:case</ta>
            <ta e="T114" id="Seg_2660" s="T113">n-n:case</ta>
            <ta e="T115" id="Seg_2661" s="T114">v-v&gt;v-v:pn</ta>
            <ta e="T116" id="Seg_2662" s="T115">que-n:case</ta>
            <ta e="T117" id="Seg_2663" s="T116">n-n:case</ta>
            <ta e="T118" id="Seg_2664" s="T117">v-v&gt;v-v:pn</ta>
            <ta e="T119" id="Seg_2665" s="T118">conj</ta>
            <ta e="T120" id="Seg_2666" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_2667" s="T120">dempro-n:case</ta>
            <ta e="T122" id="Seg_2668" s="T121">v-v:n.fin</ta>
            <ta e="T125" id="Seg_2669" s="T123">dempro-n:case</ta>
            <ta e="T126" id="Seg_2670" s="T125">adj-n:case</ta>
            <ta e="T127" id="Seg_2671" s="T126">v-v:tense-v:pn</ta>
            <ta e="T128" id="Seg_2672" s="T127">pers</ta>
            <ta e="T129" id="Seg_2673" s="T128">n-n:case</ta>
            <ta e="T130" id="Seg_2674" s="T129">v-v:tense-v:pn</ta>
            <ta e="T131" id="Seg_2675" s="T130">num-n:case</ta>
            <ta e="T132" id="Seg_2676" s="T131">num-n:case</ta>
            <ta e="T133" id="Seg_2677" s="T132">adv</ta>
            <ta e="T134" id="Seg_2678" s="T133">ptcl</ta>
            <ta e="T135" id="Seg_2679" s="T134">v-v:ins-v:mood.pn</ta>
            <ta e="T136" id="Seg_2680" s="T135">que-n:case</ta>
            <ta e="T137" id="Seg_2681" s="T136">pers</ta>
            <ta e="T138" id="Seg_2682" s="T137">pers</ta>
            <ta e="T139" id="Seg_2683" s="T138">ptcl</ta>
            <ta e="T140" id="Seg_2684" s="T139">v-v:tense-v:pn</ta>
            <ta e="T141" id="Seg_2685" s="T140">dempro-n:case</ta>
            <ta e="T142" id="Seg_2686" s="T141">v-v:tense-v:pn</ta>
            <ta e="T143" id="Seg_2687" s="T142">v-v:ins-v:mood.pn</ta>
            <ta e="T144" id="Seg_2688" s="T143">adj-n:case</ta>
            <ta e="T146" id="Seg_2689" s="T145">adj-n:case</ta>
            <ta e="T147" id="Seg_2690" s="T146">n-n:case</ta>
            <ta e="T148" id="Seg_2691" s="T147">adv</ta>
            <ta e="T149" id="Seg_2692" s="T148">v-v:tense-v:pn</ta>
            <ta e="T150" id="Seg_2693" s="T149">dempro-n:case</ta>
            <ta e="T152" id="Seg_2694" s="T151">ptcl</ta>
            <ta e="T153" id="Seg_2695" s="T152">v-v:tense-v:pn</ta>
            <ta e="T154" id="Seg_2696" s="T153">dempro-n:case</ta>
            <ta e="T155" id="Seg_2697" s="T154">ptcl</ta>
            <ta e="T156" id="Seg_2698" s="T155">dempro-n:case</ta>
            <ta e="T157" id="Seg_2699" s="T156">v-v:tense-v:pn</ta>
            <ta e="T158" id="Seg_2700" s="T157">conj</ta>
            <ta e="T159" id="Seg_2701" s="T158">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T160" id="Seg_2702" s="T159">aux-v:mood.pn</ta>
            <ta e="T161" id="Seg_2703" s="T160">v-v:ins-v:n.fin</ta>
            <ta e="T162" id="Seg_2704" s="T161">dempro-n:case</ta>
            <ta e="T163" id="Seg_2705" s="T162">pers</ta>
            <ta e="T164" id="Seg_2706" s="T163">n-n:case</ta>
            <ta e="T165" id="Seg_2707" s="T164">v-v:tense-v:pn</ta>
            <ta e="T167" id="Seg_2708" s="T166">num-n:case</ta>
            <ta e="T168" id="Seg_2709" s="T167">num-n:case</ta>
            <ta e="T169" id="Seg_2710" s="T168">n-n:case</ta>
            <ta e="T170" id="Seg_2711" s="T169">pers</ta>
            <ta e="T171" id="Seg_2712" s="T170">v-v:tense-v:pn</ta>
            <ta e="T172" id="Seg_2713" s="T171">adv</ta>
            <ta e="T173" id="Seg_2714" s="T172">dempro-n:case</ta>
            <ta e="T174" id="Seg_2715" s="T173">que-n:case</ta>
            <ta e="T175" id="Seg_2716" s="T174">pers</ta>
            <ta e="T176" id="Seg_2717" s="T175">ptcl</ta>
            <ta e="T177" id="Seg_2718" s="T176">v-v:tense-v:pn</ta>
            <ta e="T178" id="Seg_2719" s="T177">v-v:ins-v:mood.pn</ta>
            <ta e="T179" id="Seg_2720" s="T178">ptcl</ta>
            <ta e="T180" id="Seg_2721" s="T179">n-n:case</ta>
            <ta e="T181" id="Seg_2722" s="T180">v-v:tense-v:pn</ta>
            <ta e="T182" id="Seg_2723" s="T181">conj</ta>
            <ta e="T183" id="Seg_2724" s="T182">v-v:tense-v:pn</ta>
            <ta e="T184" id="Seg_2725" s="T183">n-n:case</ta>
            <ta e="T185" id="Seg_2726" s="T184">dempro-n:case</ta>
            <ta e="T186" id="Seg_2727" s="T185">adv</ta>
            <ta e="T187" id="Seg_2728" s="T186">v-v:tense-v:pn</ta>
            <ta e="T188" id="Seg_2729" s="T187">dempro-n:case</ta>
            <ta e="T189" id="Seg_2730" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_2731" s="T189">dempro-n:case</ta>
            <ta e="T191" id="Seg_2732" s="T190">ptcl</ta>
            <ta e="T193" id="Seg_2733" s="T192">pers</ta>
            <ta e="T194" id="Seg_2734" s="T193">num-n:case</ta>
            <ta e="T195" id="Seg_2735" s="T194">num-n:case</ta>
            <ta e="T196" id="Seg_2736" s="T195">v-v:tense-v:pn</ta>
            <ta e="T197" id="Seg_2737" s="T196">dempro-n:case</ta>
            <ta e="T198" id="Seg_2738" s="T197">n-n:case</ta>
            <ta e="T199" id="Seg_2739" s="T198">que-n:case</ta>
            <ta e="T200" id="Seg_2740" s="T199">pers</ta>
            <ta e="T201" id="Seg_2741" s="T200">pers</ta>
            <ta e="T202" id="Seg_2742" s="T201">ptcl</ta>
            <ta e="T203" id="Seg_2743" s="T202">v-v:tense-v:pn</ta>
            <ta e="T204" id="Seg_2744" s="T203">adv</ta>
            <ta e="T205" id="Seg_2745" s="T204">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T206" id="Seg_2746" s="T205">n-n:case</ta>
            <ta e="T207" id="Seg_2747" s="T206">conj</ta>
            <ta e="T208" id="Seg_2748" s="T207">ptcl</ta>
            <ta e="T209" id="Seg_2749" s="T208">n-v:n.fin</ta>
            <ta e="T210" id="Seg_2750" s="T209">n-n:case</ta>
            <ta e="T211" id="Seg_2751" s="T210">ptcl</ta>
            <ta e="T212" id="Seg_2752" s="T211">v-v:n.fin</ta>
            <ta e="T213" id="Seg_2753" s="T212">adv</ta>
            <ta e="T214" id="Seg_2754" s="T213">v-v:tense-v:pn</ta>
            <ta e="T215" id="Seg_2755" s="T214">dempro-n:case</ta>
            <ta e="T216" id="Seg_2756" s="T215">n-n:case</ta>
            <ta e="T217" id="Seg_2757" s="T216">v-v:tense-v:pn</ta>
            <ta e="T218" id="Seg_2758" s="T217">conj</ta>
            <ta e="T219" id="Seg_2759" s="T218">n-n:case</ta>
            <ta e="T220" id="Seg_2760" s="T219">v-v:tense-v:pn</ta>
            <ta e="T221" id="Seg_2761" s="T220">conj</ta>
            <ta e="T222" id="Seg_2762" s="T221">adv</ta>
            <ta e="T223" id="Seg_2763" s="T222">adj-n:case</ta>
            <ta e="T224" id="Seg_2764" s="T223">n-n:case</ta>
            <ta e="T225" id="Seg_2765" s="T224">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T226" id="Seg_2766" s="T225">conj</ta>
            <ta e="T227" id="Seg_2767" s="T226">v-v:tense-v:pn</ta>
            <ta e="T228" id="Seg_2768" s="T227">dempro-n:case</ta>
            <ta e="T229" id="Seg_2769" s="T228">n-n:case</ta>
            <ta e="T230" id="Seg_2770" s="T229">conj</ta>
            <ta e="T231" id="Seg_2771" s="T230">n-n:case.poss</ta>
            <ta e="T232" id="Seg_2772" s="T231">que-n:case</ta>
            <ta e="T233" id="Seg_2773" s="T232">pers</ta>
            <ta e="T234" id="Seg_2774" s="T233">n-n:case</ta>
            <ta e="T235" id="Seg_2775" s="T234">v-v:tense-v:pn</ta>
            <ta e="T236" id="Seg_2776" s="T235">n-n:case</ta>
            <ta e="T237" id="Seg_2777" s="T236">conj</ta>
            <ta e="T238" id="Seg_2778" s="T237">dempro-n:case</ta>
            <ta e="T239" id="Seg_2779" s="T238">adj-n:case</ta>
            <ta e="T240" id="Seg_2780" s="T239">v-v:tense-v:pn</ta>
            <ta e="T241" id="Seg_2781" s="T240">pers</ta>
            <ta e="T242" id="Seg_2782" s="T241">quant</ta>
            <ta e="T243" id="Seg_2783" s="T242">n-n:case</ta>
            <ta e="T244" id="Seg_2784" s="T243">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T245" id="Seg_2785" s="T244">adv</ta>
            <ta e="T246" id="Seg_2786" s="T245">v-v:tense-v:pn</ta>
            <ta e="T247" id="Seg_2787" s="T246">n-n:case.poss</ta>
            <ta e="T248" id="Seg_2788" s="T247">conj</ta>
            <ta e="T249" id="Seg_2789" s="T248">dempro-n:num</ta>
            <ta e="T250" id="Seg_2790" s="T249">v-v:tense-v:pn</ta>
            <ta e="T251" id="Seg_2791" s="T250">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_2792" s="T0">v</ta>
            <ta e="T2" id="Seg_2793" s="T1">n</ta>
            <ta e="T3" id="Seg_2794" s="T2">n</ta>
            <ta e="T4" id="Seg_2795" s="T3">dempro</ta>
            <ta e="T5" id="Seg_2796" s="T4">que</ta>
            <ta e="T6" id="Seg_2797" s="T5">v</ta>
            <ta e="T7" id="Seg_2798" s="T6">dempro</ta>
            <ta e="T8" id="Seg_2799" s="T7">n</ta>
            <ta e="T9" id="Seg_2800" s="T8">v</ta>
            <ta e="T10" id="Seg_2801" s="T9">n</ta>
            <ta e="T11" id="Seg_2802" s="T10">conj</ta>
            <ta e="T12" id="Seg_2803" s="T11">v</ta>
            <ta e="T13" id="Seg_2804" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_2805" s="T13">dempro</ta>
            <ta e="T15" id="Seg_2806" s="T14">v</ta>
            <ta e="T16" id="Seg_2807" s="T15">conj</ta>
            <ta e="T17" id="Seg_2808" s="T16">dempro</ta>
            <ta e="T18" id="Seg_2809" s="T17">dempro</ta>
            <ta e="T19" id="Seg_2810" s="T18">que</ta>
            <ta e="T20" id="Seg_2811" s="T19">v</ta>
            <ta e="T21" id="Seg_2812" s="T20">adv</ta>
            <ta e="T23" id="Seg_2813" s="T22">num</ta>
            <ta e="T24" id="Seg_2814" s="T23">n</ta>
            <ta e="T25" id="Seg_2815" s="T24">v</ta>
            <ta e="T26" id="Seg_2816" s="T25">conj</ta>
            <ta e="T27" id="Seg_2817" s="T26">v</ta>
            <ta e="T28" id="Seg_2818" s="T27">num</ta>
            <ta e="T29" id="Seg_2819" s="T28">v</ta>
            <ta e="T30" id="Seg_2820" s="T29">n</ta>
            <ta e="T31" id="Seg_2821" s="T30">v</ta>
            <ta e="T32" id="Seg_2822" s="T31">n</ta>
            <ta e="T33" id="Seg_2823" s="T32">v</ta>
            <ta e="T34" id="Seg_2824" s="T33">v</ta>
            <ta e="T35" id="Seg_2825" s="T34">conj</ta>
            <ta e="T36" id="Seg_2826" s="T35">conj</ta>
            <ta e="T37" id="Seg_2827" s="T36">n</ta>
            <ta e="T38" id="Seg_2828" s="T37">v</ta>
            <ta e="T39" id="Seg_2829" s="T38">adv</ta>
            <ta e="T40" id="Seg_2830" s="T39">n</ta>
            <ta e="T41" id="Seg_2831" s="T40">v</ta>
            <ta e="T42" id="Seg_2832" s="T41">adv</ta>
            <ta e="T43" id="Seg_2833" s="T42">v</ta>
            <ta e="T44" id="Seg_2834" s="T43">v</ta>
            <ta e="T45" id="Seg_2835" s="T44">dempro</ta>
            <ta e="T46" id="Seg_2836" s="T45">n</ta>
            <ta e="T47" id="Seg_2837" s="T46">ptcl</ta>
            <ta e="T48" id="Seg_2838" s="T47">v</ta>
            <ta e="T49" id="Seg_2839" s="T48">v</ta>
            <ta e="T50" id="Seg_2840" s="T49">adv</ta>
            <ta e="T51" id="Seg_2841" s="T50">v</ta>
            <ta e="T52" id="Seg_2842" s="T51">quant</ta>
            <ta e="T53" id="Seg_2843" s="T52">n</ta>
            <ta e="T54" id="Seg_2844" s="T53">adv</ta>
            <ta e="T55" id="Seg_2845" s="T54">n</ta>
            <ta e="T56" id="Seg_2846" s="T55">v</ta>
            <ta e="T57" id="Seg_2847" s="T56">v</ta>
            <ta e="T58" id="Seg_2848" s="T57">v</ta>
            <ta e="T59" id="Seg_2849" s="T58">n</ta>
            <ta e="T60" id="Seg_2850" s="T59">conj</ta>
            <ta e="T61" id="Seg_2851" s="T60">v</ta>
            <ta e="T62" id="Seg_2852" s="T61">n</ta>
            <ta e="T63" id="Seg_2853" s="T62">adv</ta>
            <ta e="T64" id="Seg_2854" s="T63">v</ta>
            <ta e="T65" id="Seg_2855" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_2856" s="T65">pers</ta>
            <ta e="T67" id="Seg_2857" s="T66">n</ta>
            <ta e="T68" id="Seg_2858" s="T67">v</ta>
            <ta e="T69" id="Seg_2859" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_2860" s="T69">n</ta>
            <ta e="T71" id="Seg_2861" s="T70">ptcl</ta>
            <ta e="T72" id="Seg_2862" s="T71">n</ta>
            <ta e="T73" id="Seg_2863" s="T72">dempro</ta>
            <ta e="T74" id="Seg_2864" s="T73">pers</ta>
            <ta e="T75" id="Seg_2865" s="T74">v</ta>
            <ta e="T76" id="Seg_2866" s="T75">n</ta>
            <ta e="T77" id="Seg_2867" s="T76">dempro</ta>
            <ta e="T78" id="Seg_2868" s="T77">ptcl</ta>
            <ta e="T79" id="Seg_2869" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_2870" s="T79">v</ta>
            <ta e="T81" id="Seg_2871" s="T80">v</ta>
            <ta e="T84" id="Seg_2872" s="T83">dempro</ta>
            <ta e="T85" id="Seg_2873" s="T84">dempro</ta>
            <ta e="T86" id="Seg_2874" s="T85">ptcl</ta>
            <ta e="T87" id="Seg_2875" s="T86">v</ta>
            <ta e="T88" id="Seg_2876" s="T87">conj</ta>
            <ta e="T89" id="Seg_2877" s="T88">v</ta>
            <ta e="T90" id="Seg_2878" s="T89">refl</ta>
            <ta e="T91" id="Seg_2879" s="T90">ptcl</ta>
            <ta e="T92" id="Seg_2880" s="T91">adv</ta>
            <ta e="T93" id="Seg_2881" s="T92">v</ta>
            <ta e="T94" id="Seg_2882" s="T93">dempro</ta>
            <ta e="T95" id="Seg_2883" s="T94">n</ta>
            <ta e="T96" id="Seg_2884" s="T95">conj</ta>
            <ta e="T97" id="Seg_2885" s="T96">v</ta>
            <ta e="T98" id="Seg_2886" s="T97">ptcl</ta>
            <ta e="T100" id="Seg_2887" s="T99">dempro</ta>
            <ta e="T101" id="Seg_2888" s="T100">v</ta>
            <ta e="T102" id="Seg_2889" s="T101">ptcl</ta>
            <ta e="T103" id="Seg_2890" s="T102">ptcl</ta>
            <ta e="T104" id="Seg_2891" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_2892" s="T104">dempro</ta>
            <ta e="T106" id="Seg_2893" s="T105">v</ta>
            <ta e="T107" id="Seg_2894" s="T106">n</ta>
            <ta e="T108" id="Seg_2895" s="T107">conj</ta>
            <ta e="T109" id="Seg_2896" s="T108">adv</ta>
            <ta e="T110" id="Seg_2897" s="T109">v</ta>
            <ta e="T111" id="Seg_2898" s="T110">adv</ta>
            <ta e="T112" id="Seg_2899" s="T111">v</ta>
            <ta e="T113" id="Seg_2900" s="T112">n</ta>
            <ta e="T114" id="Seg_2901" s="T113">n</ta>
            <ta e="T115" id="Seg_2902" s="T114">v</ta>
            <ta e="T116" id="Seg_2903" s="T115">que</ta>
            <ta e="T117" id="Seg_2904" s="T116">n</ta>
            <ta e="T118" id="Seg_2905" s="T117">v</ta>
            <ta e="T119" id="Seg_2906" s="T118">conj</ta>
            <ta e="T120" id="Seg_2907" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_2908" s="T120">dempro</ta>
            <ta e="T122" id="Seg_2909" s="T121">v</ta>
            <ta e="T125" id="Seg_2910" s="T123">dempro</ta>
            <ta e="T126" id="Seg_2911" s="T125">adj</ta>
            <ta e="T127" id="Seg_2912" s="T126">v</ta>
            <ta e="T128" id="Seg_2913" s="T127">pers</ta>
            <ta e="T129" id="Seg_2914" s="T128">n</ta>
            <ta e="T130" id="Seg_2915" s="T129">v</ta>
            <ta e="T131" id="Seg_2916" s="T130">num</ta>
            <ta e="T132" id="Seg_2917" s="T131">num</ta>
            <ta e="T133" id="Seg_2918" s="T132">adv</ta>
            <ta e="T134" id="Seg_2919" s="T133">ptcl</ta>
            <ta e="T135" id="Seg_2920" s="T134">v</ta>
            <ta e="T136" id="Seg_2921" s="T135">que</ta>
            <ta e="T137" id="Seg_2922" s="T136">pers</ta>
            <ta e="T138" id="Seg_2923" s="T137">pers</ta>
            <ta e="T139" id="Seg_2924" s="T138">ptcl</ta>
            <ta e="T140" id="Seg_2925" s="T139">v</ta>
            <ta e="T141" id="Seg_2926" s="T140">dempro</ta>
            <ta e="T142" id="Seg_2927" s="T141">v</ta>
            <ta e="T143" id="Seg_2928" s="T142">v</ta>
            <ta e="T144" id="Seg_2929" s="T143">adj</ta>
            <ta e="T146" id="Seg_2930" s="T145">adj</ta>
            <ta e="T147" id="Seg_2931" s="T146">n</ta>
            <ta e="T148" id="Seg_2932" s="T147">adv</ta>
            <ta e="T149" id="Seg_2933" s="T148">v</ta>
            <ta e="T150" id="Seg_2934" s="T149">dempro</ta>
            <ta e="T152" id="Seg_2935" s="T151">ptcl</ta>
            <ta e="T153" id="Seg_2936" s="T152">v</ta>
            <ta e="T154" id="Seg_2937" s="T153">dempro</ta>
            <ta e="T155" id="Seg_2938" s="T154">ptcl</ta>
            <ta e="T156" id="Seg_2939" s="T155">dempro</ta>
            <ta e="T157" id="Seg_2940" s="T156">v</ta>
            <ta e="T158" id="Seg_2941" s="T157">conj</ta>
            <ta e="T159" id="Seg_2942" s="T158">v</ta>
            <ta e="T160" id="Seg_2943" s="T159">aux</ta>
            <ta e="T161" id="Seg_2944" s="T160">v</ta>
            <ta e="T162" id="Seg_2945" s="T161">dempro</ta>
            <ta e="T163" id="Seg_2946" s="T162">pers</ta>
            <ta e="T164" id="Seg_2947" s="T163">n</ta>
            <ta e="T165" id="Seg_2948" s="T164">v</ta>
            <ta e="T167" id="Seg_2949" s="T166">num</ta>
            <ta e="T168" id="Seg_2950" s="T167">num</ta>
            <ta e="T169" id="Seg_2951" s="T168">n</ta>
            <ta e="T170" id="Seg_2952" s="T169">pers</ta>
            <ta e="T171" id="Seg_2953" s="T170">v</ta>
            <ta e="T172" id="Seg_2954" s="T171">adv</ta>
            <ta e="T173" id="Seg_2955" s="T172">dempro</ta>
            <ta e="T174" id="Seg_2956" s="T173">que</ta>
            <ta e="T175" id="Seg_2957" s="T174">pers</ta>
            <ta e="T176" id="Seg_2958" s="T175">ptcl</ta>
            <ta e="T177" id="Seg_2959" s="T176">v</ta>
            <ta e="T178" id="Seg_2960" s="T177">v</ta>
            <ta e="T179" id="Seg_2961" s="T178">ptcl</ta>
            <ta e="T180" id="Seg_2962" s="T179">n</ta>
            <ta e="T181" id="Seg_2963" s="T180">v</ta>
            <ta e="T182" id="Seg_2964" s="T181">conj</ta>
            <ta e="T183" id="Seg_2965" s="T182">v</ta>
            <ta e="T184" id="Seg_2966" s="T183">n</ta>
            <ta e="T185" id="Seg_2967" s="T184">dempro</ta>
            <ta e="T186" id="Seg_2968" s="T185">adv</ta>
            <ta e="T187" id="Seg_2969" s="T186">v</ta>
            <ta e="T188" id="Seg_2970" s="T187">dempro</ta>
            <ta e="T189" id="Seg_2971" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_2972" s="T189">dempro</ta>
            <ta e="T191" id="Seg_2973" s="T190">ptcl</ta>
            <ta e="T193" id="Seg_2974" s="T192">pers</ta>
            <ta e="T194" id="Seg_2975" s="T193">num</ta>
            <ta e="T195" id="Seg_2976" s="T194">num</ta>
            <ta e="T196" id="Seg_2977" s="T195">v</ta>
            <ta e="T197" id="Seg_2978" s="T196">dempro</ta>
            <ta e="T198" id="Seg_2979" s="T197">n</ta>
            <ta e="T199" id="Seg_2980" s="T198">que</ta>
            <ta e="T200" id="Seg_2981" s="T199">pers</ta>
            <ta e="T201" id="Seg_2982" s="T200">pers</ta>
            <ta e="T202" id="Seg_2983" s="T201">ptcl</ta>
            <ta e="T203" id="Seg_2984" s="T202">v</ta>
            <ta e="T204" id="Seg_2985" s="T203">adv</ta>
            <ta e="T205" id="Seg_2986" s="T204">v</ta>
            <ta e="T206" id="Seg_2987" s="T205">n</ta>
            <ta e="T207" id="Seg_2988" s="T206">conj</ta>
            <ta e="T208" id="Seg_2989" s="T207">ptcl</ta>
            <ta e="T209" id="Seg_2990" s="T208">v</ta>
            <ta e="T210" id="Seg_2991" s="T209">n</ta>
            <ta e="T211" id="Seg_2992" s="T210">ptcl</ta>
            <ta e="T212" id="Seg_2993" s="T211">v</ta>
            <ta e="T213" id="Seg_2994" s="T212">adv</ta>
            <ta e="T214" id="Seg_2995" s="T213">v</ta>
            <ta e="T215" id="Seg_2996" s="T214">dempro</ta>
            <ta e="T216" id="Seg_2997" s="T215">n</ta>
            <ta e="T217" id="Seg_2998" s="T216">v</ta>
            <ta e="T218" id="Seg_2999" s="T217">conj</ta>
            <ta e="T219" id="Seg_3000" s="T218">n</ta>
            <ta e="T220" id="Seg_3001" s="T219">v</ta>
            <ta e="T221" id="Seg_3002" s="T220">conj</ta>
            <ta e="T222" id="Seg_3003" s="T221">adv</ta>
            <ta e="T223" id="Seg_3004" s="T222">adj</ta>
            <ta e="T224" id="Seg_3005" s="T223">n</ta>
            <ta e="T225" id="Seg_3006" s="T224">v</ta>
            <ta e="T226" id="Seg_3007" s="T225">conj</ta>
            <ta e="T227" id="Seg_3008" s="T226">v</ta>
            <ta e="T228" id="Seg_3009" s="T227">dempro</ta>
            <ta e="T229" id="Seg_3010" s="T228">n</ta>
            <ta e="T230" id="Seg_3011" s="T229">conj</ta>
            <ta e="T231" id="Seg_3012" s="T230">n</ta>
            <ta e="T232" id="Seg_3013" s="T231">que</ta>
            <ta e="T233" id="Seg_3014" s="T232">pers</ta>
            <ta e="T234" id="Seg_3015" s="T233">n</ta>
            <ta e="T235" id="Seg_3016" s="T234">v</ta>
            <ta e="T236" id="Seg_3017" s="T235">n</ta>
            <ta e="T237" id="Seg_3018" s="T236">conj</ta>
            <ta e="T238" id="Seg_3019" s="T237">dempro</ta>
            <ta e="T239" id="Seg_3020" s="T238">adj</ta>
            <ta e="T240" id="Seg_3021" s="T239">v</ta>
            <ta e="T241" id="Seg_3022" s="T240">pers</ta>
            <ta e="T242" id="Seg_3023" s="T241">quant</ta>
            <ta e="T243" id="Seg_3024" s="T242">n</ta>
            <ta e="T244" id="Seg_3025" s="T243">v</ta>
            <ta e="T245" id="Seg_3026" s="T244">adv</ta>
            <ta e="T246" id="Seg_3027" s="T245">v</ta>
            <ta e="T247" id="Seg_3028" s="T246">n</ta>
            <ta e="T248" id="Seg_3029" s="T247">conj</ta>
            <ta e="T249" id="Seg_3030" s="T248">dempro</ta>
            <ta e="T250" id="Seg_3031" s="T249">v</ta>
            <ta e="T251" id="Seg_3032" s="T250">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_3033" s="T1">np.h:E</ta>
            <ta e="T3" id="Seg_3034" s="T2">np.h:E</ta>
            <ta e="T4" id="Seg_3035" s="T3">pro.h:Poss</ta>
            <ta e="T5" id="Seg_3036" s="T4">pro:Th</ta>
            <ta e="T8" id="Seg_3037" s="T7">np.h:A</ta>
            <ta e="T10" id="Seg_3038" s="T9">np:P</ta>
            <ta e="T12" id="Seg_3039" s="T11">0.3.h:A</ta>
            <ta e="T14" id="Seg_3040" s="T13">pro.h:E</ta>
            <ta e="T18" id="Seg_3041" s="T17">pro.h:Poss</ta>
            <ta e="T19" id="Seg_3042" s="T18">pro:Th</ta>
            <ta e="T24" id="Seg_3043" s="T23">np:Th</ta>
            <ta e="T28" id="Seg_3044" s="T27">np.h:A</ta>
            <ta e="T29" id="Seg_3045" s="T28">0.3.h:A</ta>
            <ta e="T30" id="Seg_3046" s="T29">np:L</ta>
            <ta e="T32" id="Seg_3047" s="T31">np:P</ta>
            <ta e="T33" id="Seg_3048" s="T32">0.3.h:A</ta>
            <ta e="T34" id="Seg_3049" s="T33">0.3.h:A</ta>
            <ta e="T37" id="Seg_3050" s="T36">np:A</ta>
            <ta e="T39" id="Seg_3051" s="T38">adv:L</ta>
            <ta e="T40" id="Seg_3052" s="T39">np:P</ta>
            <ta e="T41" id="Seg_3053" s="T40">0.3:A</ta>
            <ta e="T42" id="Seg_3054" s="T41">adv:L</ta>
            <ta e="T43" id="Seg_3055" s="T42">0.3:A</ta>
            <ta e="T44" id="Seg_3056" s="T43">0.3:A</ta>
            <ta e="T46" id="Seg_3057" s="T45">np:Ins</ta>
            <ta e="T48" id="Seg_3058" s="T47">0.3:E</ta>
            <ta e="T49" id="Seg_3059" s="T48">0.3:E</ta>
            <ta e="T50" id="Seg_3060" s="T49">adv:L</ta>
            <ta e="T53" id="Seg_3061" s="T52">np:Th</ta>
            <ta e="T54" id="Seg_3062" s="T53">adv:Time</ta>
            <ta e="T55" id="Seg_3063" s="T54">np.h:A</ta>
            <ta e="T58" id="Seg_3064" s="T57">0.3.h:E</ta>
            <ta e="T59" id="Seg_3065" s="T58">np:Th</ta>
            <ta e="T61" id="Seg_3066" s="T60">0.3.h:A</ta>
            <ta e="T62" id="Seg_3067" s="T61">np:G</ta>
            <ta e="T63" id="Seg_3068" s="T62">adv:Time</ta>
            <ta e="T64" id="Seg_3069" s="T63">0.3.h:A</ta>
            <ta e="T66" id="Seg_3070" s="T65">pro.h:R</ta>
            <ta e="T67" id="Seg_3071" s="T66">np:A</ta>
            <ta e="T74" id="Seg_3072" s="T73">pro.h:B</ta>
            <ta e="T76" id="Seg_3073" s="T75">np:A</ta>
            <ta e="T77" id="Seg_3074" s="T76">pro:R</ta>
            <ta e="T84" id="Seg_3075" s="T83">pro.h:A</ta>
            <ta e="T85" id="Seg_3076" s="T84">pro:B</ta>
            <ta e="T89" id="Seg_3077" s="T88">0.3.h:A</ta>
            <ta e="T90" id="Seg_3078" s="T89">pro:B</ta>
            <ta e="T92" id="Seg_3079" s="T91">adv:Time</ta>
            <ta e="T95" id="Seg_3080" s="T94">np:E</ta>
            <ta e="T97" id="Seg_3081" s="T96">0.3:P</ta>
            <ta e="T100" id="Seg_3082" s="T99">pro:Th</ta>
            <ta e="T105" id="Seg_3083" s="T104">pro:Th</ta>
            <ta e="T107" id="Seg_3084" s="T106">np:G</ta>
            <ta e="T111" id="Seg_3085" s="T110">adv:Time</ta>
            <ta e="T112" id="Seg_3086" s="T111">0.3.h:A</ta>
            <ta e="T113" id="Seg_3087" s="T112">np:G</ta>
            <ta e="T114" id="Seg_3088" s="T113">np.h:A</ta>
            <ta e="T117" id="Seg_3089" s="T116">np:Th</ta>
            <ta e="T118" id="Seg_3090" s="T117">0.3.h:A</ta>
            <ta e="T121" id="Seg_3091" s="T120">pro.h:E</ta>
            <ta e="T125" id="Seg_3092" s="T123">pro.h:A</ta>
            <ta e="T127" id="Seg_3093" s="T126">0.3:Th</ta>
            <ta e="T128" id="Seg_3094" s="T127">pro.h:B</ta>
            <ta e="T129" id="Seg_3095" s="T128">np:Th</ta>
            <ta e="T133" id="Seg_3096" s="T132">adv:Time</ta>
            <ta e="T135" id="Seg_3097" s="T134">0.2.h:A</ta>
            <ta e="T138" id="Seg_3098" s="T137">pro.h:R</ta>
            <ta e="T140" id="Seg_3099" s="T139">0.2.h:A</ta>
            <ta e="T141" id="Seg_3100" s="T140">pro.h:A</ta>
            <ta e="T143" id="Seg_3101" s="T142">0.2.h:A</ta>
            <ta e="T147" id="Seg_3102" s="T146">np:G</ta>
            <ta e="T148" id="Seg_3103" s="T147">adv:G</ta>
            <ta e="T149" id="Seg_3104" s="T148">0.3.h:A</ta>
            <ta e="T150" id="Seg_3105" s="T149">pro.h:R</ta>
            <ta e="T153" id="Seg_3106" s="T152">0.3.h:A</ta>
            <ta e="T154" id="Seg_3107" s="T153">pro.h:A</ta>
            <ta e="T156" id="Seg_3108" s="T155">pro.h:Th</ta>
            <ta e="T159" id="Seg_3109" s="T158">0.3.h:A</ta>
            <ta e="T160" id="Seg_3110" s="T159">0.2.h:A</ta>
            <ta e="T162" id="Seg_3111" s="T161">pro:A</ta>
            <ta e="T163" id="Seg_3112" s="T162">pro.h:B</ta>
            <ta e="T164" id="Seg_3113" s="T163">np:Th</ta>
            <ta e="T169" id="Seg_3114" s="T168">np:Th</ta>
            <ta e="T170" id="Seg_3115" s="T169">pro.h:B</ta>
            <ta e="T171" id="Seg_3116" s="T170">0.3:A</ta>
            <ta e="T172" id="Seg_3117" s="T171">adv:Time</ta>
            <ta e="T173" id="Seg_3118" s="T172">pro.h:A</ta>
            <ta e="T175" id="Seg_3119" s="T174">pro.h:A</ta>
            <ta e="T178" id="Seg_3120" s="T177">0.2.h:A</ta>
            <ta e="T180" id="Seg_3121" s="T179">np:G</ta>
            <ta e="T181" id="Seg_3122" s="T180">0.3.h:A</ta>
            <ta e="T183" id="Seg_3123" s="T182">0.3.h:A</ta>
            <ta e="T184" id="Seg_3124" s="T183">np:Ins</ta>
            <ta e="T185" id="Seg_3125" s="T184">pro.h:A</ta>
            <ta e="T186" id="Seg_3126" s="T185">adv:L</ta>
            <ta e="T188" id="Seg_3127" s="T187">pro.h:A</ta>
            <ta e="T190" id="Seg_3128" s="T189">pro.h:E</ta>
            <ta e="T193" id="Seg_3129" s="T192">pro.h:B</ta>
            <ta e="T194" id="Seg_3130" s="T193">np:Th</ta>
            <ta e="T195" id="Seg_3131" s="T194">np:Th</ta>
            <ta e="T196" id="Seg_3132" s="T195">0.3:A</ta>
            <ta e="T201" id="Seg_3133" s="T200">pro.h:R</ta>
            <ta e="T203" id="Seg_3134" s="T202">0.2.h:A</ta>
            <ta e="T204" id="Seg_3135" s="T203">adv:Time</ta>
            <ta e="T205" id="Seg_3136" s="T204">0.3.h:A</ta>
            <ta e="T206" id="Seg_3137" s="T205">np:G</ta>
            <ta e="T210" id="Seg_3138" s="T209">np:Ins</ta>
            <ta e="T213" id="Seg_3139" s="T212">adv:Time</ta>
            <ta e="T214" id="Seg_3140" s="T213">0.3.h:A</ta>
            <ta e="T215" id="Seg_3141" s="T214">pro.h:A</ta>
            <ta e="T216" id="Seg_3142" s="T215">pro:Th</ta>
            <ta e="T219" id="Seg_3143" s="T218">np:G</ta>
            <ta e="T220" id="Seg_3144" s="T219">pro.h:A</ta>
            <ta e="T224" id="Seg_3145" s="T223">np.h:E</ta>
            <ta e="T227" id="Seg_3146" s="T226">0.3.h:A</ta>
            <ta e="T229" id="Seg_3147" s="T228">np.h:Th</ta>
            <ta e="T231" id="Seg_3148" s="T230">np.h:Th</ta>
            <ta e="T233" id="Seg_3149" s="T232">pro.h:A</ta>
            <ta e="T234" id="Seg_3150" s="T233">np:Th</ta>
            <ta e="T236" id="Seg_3151" s="T235">np:G</ta>
            <ta e="T238" id="Seg_3152" s="T237">pro:Th</ta>
            <ta e="T241" id="Seg_3153" s="T240">pro.h:B</ta>
            <ta e="T243" id="Seg_3154" s="T242">np:Th</ta>
            <ta e="T244" id="Seg_3155" s="T243">0.3:A</ta>
            <ta e="T245" id="Seg_3156" s="T244">adv:Time</ta>
            <ta e="T246" id="Seg_3157" s="T245">0.3.h:A</ta>
            <ta e="T247" id="Seg_3158" s="T246">np:Th</ta>
            <ta e="T249" id="Seg_3159" s="T248">pro.h:Th</ta>
            <ta e="T250" id="Seg_3160" s="T249">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_3161" s="T0">v:pred</ta>
            <ta e="T2" id="Seg_3162" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_3163" s="T2">np.h:S</ta>
            <ta e="T5" id="Seg_3164" s="T4">pro:S</ta>
            <ta e="T6" id="Seg_3165" s="T5">v:pred</ta>
            <ta e="T8" id="Seg_3166" s="T7">np.h:S</ta>
            <ta e="T9" id="Seg_3167" s="T8">v:pred</ta>
            <ta e="T10" id="Seg_3168" s="T9">np:O</ta>
            <ta e="T12" id="Seg_3169" s="T11">v:pred 0.3.h:S</ta>
            <ta e="T14" id="Seg_3170" s="T13">pro.h:S</ta>
            <ta e="T15" id="Seg_3171" s="T14">v:pred</ta>
            <ta e="T19" id="Seg_3172" s="T18">pro:S</ta>
            <ta e="T20" id="Seg_3173" s="T19">v:pred</ta>
            <ta e="T24" id="Seg_3174" s="T23">np:S</ta>
            <ta e="T25" id="Seg_3175" s="T24">v:pred</ta>
            <ta e="T27" id="Seg_3176" s="T26">v:pred </ta>
            <ta e="T28" id="Seg_3177" s="T27">np.h:S</ta>
            <ta e="T29" id="Seg_3178" s="T28">v:pred 0.3.h:S</ta>
            <ta e="T31" id="Seg_3179" s="T30">s:purp</ta>
            <ta e="T32" id="Seg_3180" s="T31">np:O</ta>
            <ta e="T33" id="Seg_3181" s="T32">v:pred 0.3.h:S</ta>
            <ta e="T34" id="Seg_3182" s="T33">v:pred 0.3.h:S</ta>
            <ta e="T37" id="Seg_3183" s="T36">np:S</ta>
            <ta e="T38" id="Seg_3184" s="T37">v:pred</ta>
            <ta e="T40" id="Seg_3185" s="T39">np:O</ta>
            <ta e="T41" id="Seg_3186" s="T40">v:pred 0.3:S</ta>
            <ta e="T43" id="Seg_3187" s="T42">v:pred 0.3:S</ta>
            <ta e="T44" id="Seg_3188" s="T43">v:pred 0.3:S</ta>
            <ta e="T48" id="Seg_3189" s="T47">v:pred 0.3:S</ta>
            <ta e="T49" id="Seg_3190" s="T48">v:pred 0.3:S</ta>
            <ta e="T51" id="Seg_3191" s="T50">v:pred</ta>
            <ta e="T53" id="Seg_3192" s="T52">np:S</ta>
            <ta e="T55" id="Seg_3193" s="T54">np.h:S</ta>
            <ta e="T56" id="Seg_3194" s="T55">v:pred</ta>
            <ta e="T58" id="Seg_3195" s="T57">v:pred 0.3.h:S</ta>
            <ta e="T59" id="Seg_3196" s="T58">np:O</ta>
            <ta e="T61" id="Seg_3197" s="T60">v:pred 0.3.h:S</ta>
            <ta e="T64" id="Seg_3198" s="T63">v:pred 0.3.h:S</ta>
            <ta e="T67" id="Seg_3199" s="T66">np:S</ta>
            <ta e="T68" id="Seg_3200" s="T67">v:pred</ta>
            <ta e="T75" id="Seg_3201" s="T74">v:pred</ta>
            <ta e="T76" id="Seg_3202" s="T75">np:S</ta>
            <ta e="T77" id="Seg_3203" s="T76">pro:O</ta>
            <ta e="T78" id="Seg_3204" s="T77">ptcl:pred</ta>
            <ta e="T80" id="Seg_3205" s="T79">s:purp</ta>
            <ta e="T84" id="Seg_3206" s="T83">pro.h:S</ta>
            <ta e="T85" id="Seg_3207" s="T84">pro:O</ta>
            <ta e="T87" id="Seg_3208" s="T86">v:pred</ta>
            <ta e="T89" id="Seg_3209" s="T88">v:pred 0.3.h:S</ta>
            <ta e="T90" id="Seg_3210" s="T89">pro:O</ta>
            <ta e="T93" id="Seg_3211" s="T92">v:pred</ta>
            <ta e="T95" id="Seg_3212" s="T94">np:S</ta>
            <ta e="T97" id="Seg_3213" s="T96">v:pred 0.3:S</ta>
            <ta e="T98" id="Seg_3214" s="T97">ptcl:pred</ta>
            <ta e="T100" id="Seg_3215" s="T99">pro:O</ta>
            <ta e="T103" id="Seg_3216" s="T102">ptcl:pred</ta>
            <ta e="T104" id="Seg_3217" s="T103">ptcl:pred</ta>
            <ta e="T105" id="Seg_3218" s="T104">pro:O</ta>
            <ta e="T110" id="Seg_3219" s="T109">v:pred</ta>
            <ta e="T112" id="Seg_3220" s="T111">v:pred 0.3.h:S</ta>
            <ta e="T114" id="Seg_3221" s="T113">np.h:S</ta>
            <ta e="T115" id="Seg_3222" s="T114">v:pred</ta>
            <ta e="T117" id="Seg_3223" s="T116">np:S</ta>
            <ta e="T118" id="Seg_3224" s="T117">v:pred 0.3.h:S</ta>
            <ta e="T120" id="Seg_3225" s="T119">ptcl:pred</ta>
            <ta e="T121" id="Seg_3226" s="T120">pro.h:O</ta>
            <ta e="T125" id="Seg_3227" s="T123">pro.h:S</ta>
            <ta e="T126" id="Seg_3228" s="T125">adj:pred</ta>
            <ta e="T127" id="Seg_3229" s="T126">cop 0.3:S</ta>
            <ta e="T129" id="Seg_3230" s="T128">np:S</ta>
            <ta e="T130" id="Seg_3231" s="T129">v:pred</ta>
            <ta e="T135" id="Seg_3232" s="T134">v:pred 0.2.h:S</ta>
            <ta e="T139" id="Seg_3233" s="T138">ptcl.neg</ta>
            <ta e="T140" id="Seg_3234" s="T139">v:pred 0.2.h:S</ta>
            <ta e="T141" id="Seg_3235" s="T140">pro.h:S</ta>
            <ta e="T142" id="Seg_3236" s="T141">v:pred</ta>
            <ta e="T143" id="Seg_3237" s="T142">v:pred 0.2.h:S</ta>
            <ta e="T149" id="Seg_3238" s="T148">v:pred 0.3.h:S</ta>
            <ta e="T153" id="Seg_3239" s="T152">v:pred 0.3.h:S</ta>
            <ta e="T154" id="Seg_3240" s="T153">pro.h:S</ta>
            <ta e="T156" id="Seg_3241" s="T155">pro.h:O</ta>
            <ta e="T157" id="Seg_3242" s="T156">v:pred</ta>
            <ta e="T159" id="Seg_3243" s="T158">v:pred 0.3.h:S</ta>
            <ta e="T160" id="Seg_3244" s="T159">v:pred 0.2.h:S</ta>
            <ta e="T162" id="Seg_3245" s="T161">pro:S</ta>
            <ta e="T164" id="Seg_3246" s="T163">np:O</ta>
            <ta e="T165" id="Seg_3247" s="T164">v:pred</ta>
            <ta e="T169" id="Seg_3248" s="T168">np:O</ta>
            <ta e="T171" id="Seg_3249" s="T170">v:pred 0.3:S</ta>
            <ta e="T173" id="Seg_3250" s="T172">pro.h:S</ta>
            <ta e="T175" id="Seg_3251" s="T174">pro.h:S</ta>
            <ta e="T176" id="Seg_3252" s="T175">ptcl.neg</ta>
            <ta e="T177" id="Seg_3253" s="T176">v:pred</ta>
            <ta e="T178" id="Seg_3254" s="T177">v:pred 0.2.h:S</ta>
            <ta e="T179" id="Seg_3255" s="T178">ptcl:pred</ta>
            <ta e="T183" id="Seg_3256" s="T182">v:pred 0.3.h:S</ta>
            <ta e="T185" id="Seg_3257" s="T184">pro.h:S</ta>
            <ta e="T187" id="Seg_3258" s="T186">v:pred</ta>
            <ta e="T190" id="Seg_3259" s="T189">pro.h:O</ta>
            <ta e="T191" id="Seg_3260" s="T190">ptcl:pred</ta>
            <ta e="T196" id="Seg_3261" s="T195">v:pred 0.3:S</ta>
            <ta e="T202" id="Seg_3262" s="T201">ptcl.neg</ta>
            <ta e="T203" id="Seg_3263" s="T202">v:pred 0.2.h:S</ta>
            <ta e="T205" id="Seg_3264" s="T204">v:pred 0.3.h:S</ta>
            <ta e="T208" id="Seg_3265" s="T207">ptcl:pred</ta>
            <ta e="T214" id="Seg_3266" s="T213">v:pred 0.3.h:S</ta>
            <ta e="T215" id="Seg_3267" s="T214">pro.h:S</ta>
            <ta e="T216" id="Seg_3268" s="T215">np:O</ta>
            <ta e="T217" id="Seg_3269" s="T216">v:pred</ta>
            <ta e="T220" id="Seg_3270" s="T219">v:pred pro.h:S</ta>
            <ta e="T224" id="Seg_3271" s="T223">np.h:S</ta>
            <ta e="T225" id="Seg_3272" s="T224">v:pred</ta>
            <ta e="T227" id="Seg_3273" s="T226">v:pred 0.3.h:S</ta>
            <ta e="T229" id="Seg_3274" s="T228">np.h:O</ta>
            <ta e="T231" id="Seg_3275" s="T230">np.h:O</ta>
            <ta e="T233" id="Seg_3276" s="T232">pro.h:S</ta>
            <ta e="T234" id="Seg_3277" s="T233">np:O</ta>
            <ta e="T235" id="Seg_3278" s="T234">v:pred</ta>
            <ta e="T238" id="Seg_3279" s="T237">pro:S</ta>
            <ta e="T239" id="Seg_3280" s="T238">adj:pred</ta>
            <ta e="T240" id="Seg_3281" s="T239">cop</ta>
            <ta e="T243" id="Seg_3282" s="T242">np:O</ta>
            <ta e="T244" id="Seg_3283" s="T243">v:pred 0.3:S</ta>
            <ta e="T246" id="Seg_3284" s="T245">v:pred 0.3.h:S</ta>
            <ta e="T247" id="Seg_3285" s="T246">np:O</ta>
            <ta e="T249" id="Seg_3286" s="T248">pro.h:O</ta>
            <ta e="T250" id="Seg_3287" s="T249">v:pred 0.3.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T5" id="Seg_3288" s="T4">TURK:gram(INDEF)</ta>
            <ta e="T11" id="Seg_3289" s="T10">RUS:gram</ta>
            <ta e="T12" id="Seg_3290" s="T11">TURK:cult</ta>
            <ta e="T16" id="Seg_3291" s="T15">RUS:gram</ta>
            <ta e="T19" id="Seg_3292" s="T18">TURK:gram(INDEF)</ta>
            <ta e="T21" id="Seg_3293" s="T20">RUS:mod</ta>
            <ta e="T26" id="Seg_3294" s="T25">RUS:gram</ta>
            <ta e="T35" id="Seg_3295" s="T34">RUS:gram</ta>
            <ta e="T36" id="Seg_3296" s="T35">RUS:gram</ta>
            <ta e="T47" id="Seg_3297" s="T46">TURK:disc</ta>
            <ta e="T53" id="Seg_3298" s="T52">TAT:cult</ta>
            <ta e="T59" id="Seg_3299" s="T58">TAT:cult</ta>
            <ta e="T60" id="Seg_3300" s="T59">RUS:gram</ta>
            <ta e="T65" id="Seg_3301" s="T64">RUS:disc</ta>
            <ta e="T69" id="Seg_3302" s="T68">RUS:disc</ta>
            <ta e="T71" id="Seg_3303" s="T70">RUS:gram</ta>
            <ta e="T78" id="Seg_3304" s="T77">RUS:mod</ta>
            <ta e="T79" id="Seg_3305" s="T78">RUS:mod</ta>
            <ta e="T86" id="Seg_3306" s="T85">RUS:mod</ta>
            <ta e="T88" id="Seg_3307" s="T87">RUS:gram</ta>
            <ta e="T91" id="Seg_3308" s="T90">TURK:disc</ta>
            <ta e="T96" id="Seg_3309" s="T95">RUS:gram</ta>
            <ta e="T98" id="Seg_3310" s="T97">RUS:mod</ta>
            <ta e="T102" id="Seg_3311" s="T101">RUS:gram</ta>
            <ta e="T103" id="Seg_3312" s="T102">RUS:mod</ta>
            <ta e="T104" id="Seg_3313" s="T103">RUS:mod</ta>
            <ta e="T107" id="Seg_3314" s="T106">TURK:cult</ta>
            <ta e="T108" id="Seg_3315" s="T107">RUS:gram</ta>
            <ta e="T109" id="Seg_3316" s="T108">TURK:core</ta>
            <ta e="T113" id="Seg_3317" s="T112">TURK:cult</ta>
            <ta e="T114" id="Seg_3318" s="T113">TURK:cult</ta>
            <ta e="T119" id="Seg_3319" s="T118">RUS:gram</ta>
            <ta e="T120" id="Seg_3320" s="T119">RUS:gram</ta>
            <ta e="T126" id="Seg_3321" s="T125">TURK:core</ta>
            <ta e="T129" id="Seg_3322" s="T128">TAT:cult</ta>
            <ta e="T134" id="Seg_3323" s="T133">RUS:disc</ta>
            <ta e="T146" id="Seg_3324" s="T145">TURK:core</ta>
            <ta e="T147" id="Seg_3325" s="T146">TURK:cult</ta>
            <ta e="T152" id="Seg_3326" s="T151">RUS:mod</ta>
            <ta e="T155" id="Seg_3327" s="T154">RUS:mod</ta>
            <ta e="T158" id="Seg_3328" s="T157">RUS:gram</ta>
            <ta e="T164" id="Seg_3329" s="T163">TAT:cult</ta>
            <ta e="T169" id="Seg_3330" s="T168">TAT:cult</ta>
            <ta e="T179" id="Seg_3331" s="T178">RUS:gram</ta>
            <ta e="T182" id="Seg_3332" s="T181">RUS:gram</ta>
            <ta e="T189" id="Seg_3333" s="T188">RUS:mod</ta>
            <ta e="T191" id="Seg_3334" s="T190">RUS:gram</ta>
            <ta e="T207" id="Seg_3335" s="T206">RUS:gram</ta>
            <ta e="T208" id="Seg_3336" s="T207">RUS:gram</ta>
            <ta e="T211" id="Seg_3337" s="T210">TURK:disc</ta>
            <ta e="T218" id="Seg_3338" s="T217">RUS:gram</ta>
            <ta e="T221" id="Seg_3339" s="T220">RUS:gram</ta>
            <ta e="T222" id="Seg_3340" s="T221">RUS:mod</ta>
            <ta e="T224" id="Seg_3341" s="T223">TURK:cult</ta>
            <ta e="T226" id="Seg_3342" s="T225">RUS:gram</ta>
            <ta e="T230" id="Seg_3343" s="T229">RUS:gram</ta>
            <ta e="T231" id="Seg_3344" s="T230">TURK:cult</ta>
            <ta e="T237" id="Seg_3345" s="T236">RUS:gram</ta>
            <ta e="T239" id="Seg_3346" s="T238">TURK:core</ta>
            <ta e="T243" id="Seg_3347" s="T242">TAT:cult</ta>
            <ta e="T247" id="Seg_3348" s="T246">TAT:cult</ta>
            <ta e="T248" id="Seg_3349" s="T247">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T53" id="Seg_3350" s="T52">dir:bare</ta>
            <ta e="T59" id="Seg_3351" s="T58">dir:bare</ta>
            <ta e="T129" id="Seg_3352" s="T128">dir:bare</ta>
            <ta e="T164" id="Seg_3353" s="T163">dir:bare</ta>
            <ta e="T169" id="Seg_3354" s="T168">dir:bare</ta>
            <ta e="T243" id="Seg_3355" s="T242">dir:bare</ta>
            <ta e="T247" id="Seg_3356" s="T246">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS">
            <ta e="T174" id="Seg_3357" s="T173">RUS:calq</ta>
            <ta e="T232" id="Seg_3358" s="T231">RUS:calq</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_3359" s="T0">Жили жена и муж.</ta>
            <ta e="T6" id="Seg_3360" s="T3">У них ничего не было.</ta>
            <ta e="T10" id="Seg_3361" s="T6">Муж плёл лапти.</ta>
            <ta e="T15" id="Seg_3362" s="T10">И продавал, так они и жили.</ta>
            <ta e="T20" id="Seg_3363" s="T15">Но у них ничего не было.</ta>
            <ta e="T25" id="Seg_3364" s="T20">Только один козёл был.</ta>
            <ta e="T28" id="Seg_3365" s="T25">И ушёл он [однажды?].</ta>
            <ta e="T31" id="Seg_3366" s="T28">Он пошёл в тайгу лыки драть.</ta>
            <ta e="T34" id="Seg_3367" s="T31">Лыки драл, драл.</ta>
            <ta e="T41" id="Seg_3368" s="T34">А козёл там ходит, траву ест.</ta>
            <ta e="T48" id="Seg_3369" s="T41">Потом он ходил, ходил, и провалился ногой [в яму].</ta>
            <ta e="T53" id="Seg_3370" s="T48">Увидел, что там много денег лежит.</ta>
            <ta e="T62" id="Seg_3371" s="T53">Потом пришёл мужик, увидел деньги и принёс домой.</ta>
            <ta e="T68" id="Seg_3372" s="T62">Потом [старуха] говорит: «Ну, нам Бог дал.»</ta>
            <ta e="T76" id="Seg_3373" s="T68">«Ну, Бог не Бог, а для нас козёл нашёл.</ta>
            <ta e="T81" id="Seg_3374" s="T76">Надо его тоже кормить».</ta>
            <ta e="T87" id="Seg_3375" s="T81">Они его тоже берегли.</ta>
            <ta e="T91" id="Seg_3376" s="T87">И кормили.</ta>
            <ta e="T97" id="Seg_3377" s="T91">Потом козёл заболел и умер.</ta>
            <ta e="T101" id="Seg_3378" s="T97">«Надо его выбросить».</ta>
            <ta e="T107" id="Seg_3379" s="T101">«Не надо, надо к священнику пойти.</ta>
            <ta e="T110" id="Seg_3380" s="T107">И похоронить его по-хорошему».</ta>
            <ta e="T115" id="Seg_3381" s="T110">Потом он пошёл к священнику, священник говорит:</ta>
            <ta e="T117" id="Seg_3382" s="T115">«Что за козёл?»</ta>
            <ta e="T118" id="Seg_3383" s="T117">Схватил мужика.</ta>
            <ta e="T122" id="Seg_3384" s="T118">И давай бить его.</ta>
            <ta e="T127" id="Seg_3385" s="T122">[Мужик гооврит:] «Он был хорошим.</ta>
            <ta e="T132" id="Seg_3386" s="T127">Он тебе денег оставил, двадцать [рублей].</ta>
            <ta e="T135" id="Seg_3387" s="T132">Потом [священник] говорит: «Ну, пойдём! </ta>
            <ta e="T140" id="Seg_3388" s="T135">Почему ты мне раньше не сказал?»</ta>
            <ta e="T149" id="Seg_3389" s="T140">Он пошёл; [священник сказал:] «Иди к другому… к другому священнику», он туда пошёл.</ta>
            <ta e="T153" id="Seg_3390" s="T149">Тому тоже сказал.</ta>
            <ta e="T159" id="Seg_3391" s="T153">Тот тоже схватил его и ударил.</ta>
            <ta e="T165" id="Seg_3392" s="T159">«Не бей [меня], он [козёл] тебе денег оставил!</ta>
            <ta e="T171" id="Seg_3393" s="T165">Двадцать [рублей] денег тебе оставил».</ta>
            <ta e="T177" id="Seg_3394" s="T171">Тогда тот [говорит]: «Что ты [раньше] не сказал?</ta>
            <ta e="T184" id="Seg_3395" s="T177">Иди [к звонарю], пусть лезет на (колокольню?) и звонит в колокол».</ta>
            <ta e="T187" id="Seg_3396" s="T184">Он туда пошёл.</ta>
            <ta e="T192" id="Seg_3397" s="T187">Тот тоже его начал [бить].</ta>
            <ta e="T196" id="Seg_3398" s="T192">«Он тебе пятнадцать рублей оставил.</ta>
            <ta e="T198" id="Seg_3399" s="T196">Этот козёл».</ta>
            <ta e="T203" id="Seg_3400" s="T198">«Что ты мне [раньше] не сказал?»</ta>
            <ta e="T212" id="Seg_3401" s="T203">Потом запрыгнул [=залез] на (колокольню?) и стал в колокол звонить.</ta>
            <ta e="T214" id="Seg_3402" s="T212">Потом они пришли.</ta>
            <ta e="T217" id="Seg_3403" s="T214">Принесли козла.</ta>
            <ta e="T220" id="Seg_3404" s="T217">И в землю (положили?).</ta>
            <ta e="T225" id="Seg_3405" s="T220">И ещё главный священник узнал [об этом].</ta>
            <ta e="T231" id="Seg_3406" s="T225">И вызвал этого человека и священника.</ta>
            <ta e="T236" id="Seg_3407" s="T231">«Почему вы козла похоронили?»</ta>
            <ta e="T240" id="Seg_3408" s="T236">«Да он был хороший!</ta>
            <ta e="T244" id="Seg_3409" s="T240">Тебе много денег оставил».</ta>
            <ta e="T250" id="Seg_3410" s="T244">Тогда он взял деньги и отпустил их.</ta>
            <ta e="T251" id="Seg_3411" s="T250">Останови [плёнку].</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_3412" s="T0">A woman [and] a man lived.</ta>
            <ta e="T6" id="Seg_3413" s="T3">They did not have anything.</ta>
            <ta e="T10" id="Seg_3414" s="T6">The husband made bast shoes.</ta>
            <ta e="T15" id="Seg_3415" s="T10">And sold [them], so they lived.</ta>
            <ta e="T20" id="Seg_3416" s="T15">But they had nothing.</ta>
            <ta e="T25" id="Seg_3417" s="T20">[They] had only one goat.</ta>
            <ta e="T28" id="Seg_3418" s="T25">And (once?) he went [away].</ta>
            <ta e="T31" id="Seg_3419" s="T28">He went to the taiga to bark [lime-trees].</ta>
            <ta e="T34" id="Seg_3420" s="T31">He barked and barked [lime-trees].</ta>
            <ta e="T41" id="Seg_3421" s="T34">And the goat is going around, eating grass there.</ta>
            <ta e="T48" id="Seg_3422" s="T41">Then it walked, it walked, and it felt [into a hole] with its feet [forward].</ta>
            <ta e="T53" id="Seg_3423" s="T48">It sees there is a lot of money lying.</ta>
            <ta e="T62" id="Seg_3424" s="T53">Then the man came, saw the money and brought [it] home.</ta>
            <ta e="T68" id="Seg_3425" s="T62">Then [the wife] says: "Well, God gave [it] to us."</ta>
            <ta e="T76" id="Seg_3426" s="T68">"Well, God or not God, the goat found it for us.</ta>
            <ta e="T81" id="Seg_3427" s="T76">[We] should give it [food] to eat."</ta>
            <ta e="T87" id="Seg_3428" s="T81">They cared for it, too.</ta>
            <ta e="T91" id="Seg_3429" s="T87">And fed it [itself].</ta>
            <ta e="T97" id="Seg_3430" s="T91">Then the goat got sick and died.</ta>
            <ta e="T101" id="Seg_3431" s="T97">"[We] should throw it away."</ta>
            <ta e="T107" id="Seg_3432" s="T101">"No, [we] shouldn't, [we] should go to the priest.</ta>
            <ta e="T110" id="Seg_3433" s="T107">And bury [it] well."</ta>
            <ta e="T115" id="Seg_3434" s="T110">Then he went to the priest, the priest says:</ta>
            <ta e="T117" id="Seg_3435" s="T115">"What for a goat?"</ta>
            <ta e="T118" id="Seg_3436" s="T117">[He] grabbed [the old (man]?).</ta>
            <ta e="T122" id="Seg_3437" s="T118">And he started to beat him.</ta>
            <ta e="T127" id="Seg_3438" s="T122">[The old man says]: "He was good.</ta>
            <ta e="T132" id="Seg_3439" s="T127">It left money for you, twenty [rubles]."</ta>
            <ta e="T135" id="Seg_3440" s="T132">Then [the priest says]: "Well, (go?)!</ta>
            <ta e="T140" id="Seg_3441" s="T135">Why did you not tell me [earlier]?"</ta>
            <ta e="T149" id="Seg_3442" s="T140">He went; [the priest said:] "Go, other… to another priest", there he went.</ta>
            <ta e="T153" id="Seg_3443" s="T149">He also told that one.</ta>
            <ta e="T159" id="Seg_3444" s="T153">He also grabbed him and beat [him].</ta>
            <ta e="T165" id="Seg_3445" s="T159">"Don't beat [me], it left money for you!</ta>
            <ta e="T171" id="Seg_3446" s="T165">He left for you twenty [rubles] money."</ta>
            <ta e="T177" id="Seg_3447" s="T171">Then he [said]: "Why didn't you say [it earlier]?</ta>
            <ta e="T184" id="Seg_3448" s="T177">Go [to the bell ringer], let him climb to the church (tower?), ring the bell."</ta>
            <ta e="T187" id="Seg_3449" s="T184">He came there.</ta>
            <ta e="T192" id="Seg_3450" s="T187">He started to [beat him], too.</ta>
            <ta e="T196" id="Seg_3451" s="T192">"It left you fifteen [rubles].</ta>
            <ta e="T198" id="Seg_3452" s="T196">This goat."</ta>
            <ta e="T203" id="Seg_3453" s="T198">"Why didn't you tell me [earlier]?"</ta>
            <ta e="T212" id="Seg_3454" s="T203">Then he jumped [=climbed up] to the church (tower?) and started to ring the bell.</ta>
            <ta e="T214" id="Seg_3455" s="T212">Then they came.</ta>
            <ta e="T217" id="Seg_3456" s="T214">They brought the goat.</ta>
            <ta e="T220" id="Seg_3457" s="T217">And dumped [it] into the ground.</ta>
            <ta e="T225" id="Seg_3458" s="T220">And the head priest also learned [the news].</ta>
            <ta e="T231" id="Seg_3459" s="T225">And called the man and his priest.</ta>
            <ta e="T236" id="Seg_3460" s="T231">"Why did you put the goat into the ground?"</ta>
            <ta e="T240" id="Seg_3461" s="T236">"But it was good!</ta>
            <ta e="T244" id="Seg_3462" s="T240">It left you a lot of money."</ta>
            <ta e="T250" id="Seg_3463" s="T244">Then he took the money and let them go.</ta>
            <ta e="T251" id="Seg_3464" s="T250">Stop [the tape].</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_3465" s="T0">Ein Mann [und] eine Frau lebten.</ta>
            <ta e="T6" id="Seg_3466" s="T3">Sie hatten nichts.</ta>
            <ta e="T10" id="Seg_3467" s="T6">Der Ehemann machte Bastschuhe.</ta>
            <ta e="T15" id="Seg_3468" s="T10">Und verkaufte [sie], so lebten sie.</ta>
            <ta e="T20" id="Seg_3469" s="T15">Aber sie hatten nichts.</ta>
            <ta e="T25" id="Seg_3470" s="T20">[Sie] hatten nur eine Ziege.</ta>
            <ta e="T28" id="Seg_3471" s="T25">Und ein (mal?), ging er [weg].</ta>
            <ta e="T31" id="Seg_3472" s="T28">Er ging in die Taiga um Bäume zu entrinden.</ta>
            <ta e="T34" id="Seg_3473" s="T31">Er enrindete und entrindete Bäume.</ta>
            <ta e="T41" id="Seg_3474" s="T34">Und die Ziege ging herum, aß das Gras dort.</ta>
            <ta e="T48" id="Seg_3475" s="T41">Dann lief sie, sie lief, und sie fiel [in ein Loch] mit den Füßen [nach vorne].</ta>
            <ta e="T53" id="Seg_3476" s="T48">Sie sieht, dass dort viel Geld liegt.</ta>
            <ta e="T62" id="Seg_3477" s="T53">Dann kam der Mann, sah das Geld und brachte [es] nach Hause.</ta>
            <ta e="T68" id="Seg_3478" s="T62">Dann sagte [die Frau]: "Nun, Gott hat [es] uns gegeben."</ta>
            <ta e="T76" id="Seg_3479" s="T68">"Nun, Gott oder kein Gott, die Ziege hat es für uns gefunden.</ta>
            <ta e="T81" id="Seg_3480" s="T76">[Wir] sollten ihr Essen geben."</ta>
            <ta e="T87" id="Seg_3481" s="T81">Sie kümmerten sie auch um sie.</ta>
            <ta e="T91" id="Seg_3482" s="T87">Und fütterten sie.</ta>
            <ta e="T97" id="Seg_3483" s="T91">Dann wurde die Ziege krank und starb.</ta>
            <ta e="T101" id="Seg_3484" s="T97">"[Wir] sollten sie wegwerfen."</ta>
            <ta e="T107" id="Seg_3485" s="T101">"Nein, [wir] sollten das nicht, [wir] sollten den Priester holen.</ta>
            <ta e="T110" id="Seg_3486" s="T107">Und [sie] gut beerdigen."</ta>
            <ta e="T115" id="Seg_3487" s="T110">Dann gingen sie zum Priester, der Priester sagte: </ta>
            <ta e="T117" id="Seg_3488" s="T115">"Was für eine Ziege?"</ta>
            <ta e="T118" id="Seg_3489" s="T117">[Er] packte [den alten (Mann]?).</ta>
            <ta e="T122" id="Seg_3490" s="T118">Und fing an ihn zu schlagen.</ta>
            <ta e="T127" id="Seg_3491" s="T122">[Der alte Mann sagte]: "Sie war gut.</ta>
            <ta e="T132" id="Seg_3492" s="T127">Sie hat Geld für dich da gelassen, zwanzig [Rubel]."</ta>
            <ta e="T135" id="Seg_3493" s="T132">Dann [sagt der Priester]: "Nun, (los?)!</ta>
            <ta e="T140" id="Seg_3494" s="T135">Warum hast du mir das nicht [früher] gesagt?"</ta>
            <ta e="T149" id="Seg_3495" s="T140">Er ging; [der Priester sagte:]: "Geh, anderer… zu einem anderen Priester", da ging er hin.</ta>
            <ta e="T153" id="Seg_3496" s="T149">Er erzählte es auch diesem.</ta>
            <ta e="T159" id="Seg_3497" s="T153">Er packte ihn auch und schlug [ihn].</ta>
            <ta e="T165" id="Seg_3498" s="T159">"Schlag [mich] nicht, sie hat Geld für dich dagelassen!</ta>
            <ta e="T171" id="Seg_3499" s="T165">Sie hat zwanzig Rubel für dich dagelassen."</ta>
            <ta e="T177" id="Seg_3500" s="T171">Dann [sagte] er: "Warum hast du das nicht [früher] gesagt?</ta>
            <ta e="T184" id="Seg_3501" s="T177">Geh [zum Glöckner], lass ihn auch den (Kirchturm?) klettern, die Glocke läuten."</ta>
            <ta e="T187" id="Seg_3502" s="T184">Er kam dorthin.</ta>
            <ta e="T192" id="Seg_3503" s="T187">Er fing auch an [ihn zu schlagen].</ta>
            <ta e="T196" id="Seg_3504" s="T192">"Sie hat fünfzehn [Rubel] für dich dagelassen.</ta>
            <ta e="T198" id="Seg_3505" s="T196">Diese Ziege."</ta>
            <ta e="T203" id="Seg_3506" s="T198">"Warum hast du mir das nicht [früher] gesagt?"</ta>
            <ta e="T212" id="Seg_3507" s="T203">Dann sprang [=kletterte hoch] er auf den (Kirchturm?) und begann die Glocke zu läuten.</ta>
            <ta e="T214" id="Seg_3508" s="T212">Dann kamen sie.</ta>
            <ta e="T217" id="Seg_3509" s="T214">Sie brachten die Ziege.</ta>
            <ta e="T220" id="Seg_3510" s="T217">Und warfen [sie] in die Erde.</ta>
            <ta e="T225" id="Seg_3511" s="T220">Und der oberste Priester erfuhr auch [davon].</ta>
            <ta e="T231" id="Seg_3512" s="T225">Und rief den Mann und seinen Priester.</ta>
            <ta e="T236" id="Seg_3513" s="T231">"Warum habt ihr die Ziege beerdigt?"</ta>
            <ta e="T240" id="Seg_3514" s="T236">"Aber sie war gut!</ta>
            <ta e="T244" id="Seg_3515" s="T240">Und sie hat dir viel Geld hinterlassen."</ta>
            <ta e="T250" id="Seg_3516" s="T244">Dann nahm er das Geld und lies sie gehen.</ta>
            <ta e="T251" id="Seg_3517" s="T250">Ende [der Aufnahme].</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T3" id="Seg_3518" s="T0">[GVY:] Tale 'A goat's funeral'; Russian text see e.g. http://hyaenidae.narod.ru/story3/124.html [AAV]: Tape SU0192</ta>
            <ta e="T10" id="Seg_3519" s="T6">[GVY:] The glottal stop heard at the end of kürleʔpi must be accidental.</ta>
            <ta e="T20" id="Seg_3520" s="T15">[KlT:] Interesting LOC pronoun form use.</ta>
            <ta e="T31" id="Seg_3521" s="T28">[GVY:] kör- 'to flay, to skin', here used for barking trees.</ta>
            <ta e="T34" id="Seg_3522" s="T31">[GVY:] Here kuba 'skin' used for 'bast'.</ta>
            <ta e="T87" id="Seg_3523" s="T81">[GVY:] ajər- 'feel pity' - Russian жалеть coll. 'love, be kind for someone'</ta>
            <ta e="T107" id="Seg_3524" s="T101">Abəs ’priest’.</ta>
            <ta e="T118" id="Seg_3525" s="T117">[GVY:] nɯilʼom 'reiben, drehen, winden' (D 46b)</ta>
            <ta e="T127" id="Seg_3526" s="T122">[KlT:] Dĭm ACC instead of NOM.</ta>
            <ta e="T132" id="Seg_3527" s="T127">[GVY:] made = left?</ta>
            <ta e="T149" id="Seg_3528" s="T140">[GVY:] to the deacon</ta>
            <ta e="T184" id="Seg_3529" s="T177">[KlT:] Küzər ’knock, murmur etc’. [GVY:] tʼegermaʔdə 'bell tower'? </ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
