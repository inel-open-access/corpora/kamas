<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID98D22F48-F256-F001-886A-7D2E2A568939">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\AA_1914_Brothers_flk\AA_1914_Brothers_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">424</ud-information>
            <ud-information attribute-name="# HIAT:w">299</ud-information>
            <ud-information attribute-name="# e">299</ud-information>
            <ud-information attribute-name="# HIAT:u">70</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="AA">
            <abbreviation>AA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" />
         <tli id="T1" />
         <tli id="T2" />
         <tli id="T3" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" />
         <tli id="T7" />
         <tli id="T8" />
         <tli id="T9" />
         <tli id="T10" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" />
         <tli id="T15" />
         <tli id="T16" />
         <tli id="T17" />
         <tli id="T18" />
         <tli id="T19" />
         <tli id="T20" />
         <tli id="T21" />
         <tli id="T22" />
         <tli id="T23" />
         <tli id="T24" />
         <tli id="T25" />
         <tli id="T26" />
         <tli id="T27" />
         <tli id="T28" />
         <tli id="T29" />
         <tli id="T30" />
         <tli id="T31" />
         <tli id="T32" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" />
         <tli id="T42" />
         <tli id="T43" />
         <tli id="T44" />
         <tli id="T45" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" />
         <tli id="T49" />
         <tli id="T50" />
         <tli id="T51" />
         <tli id="T52" />
         <tli id="T53" />
         <tli id="T54" />
         <tli id="T55" />
         <tli id="T56" />
         <tli id="T57" />
         <tli id="T58" />
         <tli id="T59" />
         <tli id="T60" />
         <tli id="T61" />
         <tli id="T62" />
         <tli id="T63" />
         <tli id="T64" />
         <tli id="T65" />
         <tli id="T66" />
         <tli id="T67" />
         <tli id="T68" />
         <tli id="T69" />
         <tli id="T70" />
         <tli id="T71" />
         <tli id="T72" />
         <tli id="T73" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" />
         <tli id="T83" />
         <tli id="T84" />
         <tli id="T85" />
         <tli id="T86" />
         <tli id="T87" />
         <tli id="T88" />
         <tli id="T89" />
         <tli id="T90" />
         <tli id="T91" />
         <tli id="T92" />
         <tli id="T93" />
         <tli id="T94" />
         <tli id="T95" />
         <tli id="T96" />
         <tli id="T97" />
         <tli id="T98" />
         <tli id="T99" />
         <tli id="T100" />
         <tli id="T101" />
         <tli id="T102" />
         <tli id="T103" />
         <tli id="T104" />
         <tli id="T105" />
         <tli id="T106" />
         <tli id="T107" />
         <tli id="T108" />
         <tli id="T109" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" />
         <tli id="T114" />
         <tli id="T115" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" />
         <tli id="T123" />
         <tli id="T124" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" />
         <tli id="T133" />
         <tli id="T134" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" />
         <tli id="T138" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" />
         <tli id="T145" />
         <tli id="T146" />
         <tli id="T147" />
         <tli id="T148" />
         <tli id="T149" />
         <tli id="T150" />
         <tli id="T151" />
         <tli id="T152" />
         <tli id="T153" />
         <tli id="T154" />
         <tli id="T155" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" />
         <tli id="T174" />
         <tli id="T175" />
         <tli id="T176" />
         <tli id="T177" />
         <tli id="T178" />
         <tli id="T179" />
         <tli id="T180" />
         <tli id="T181" />
         <tli id="T182" />
         <tli id="T183" />
         <tli id="T184" />
         <tli id="T185" />
         <tli id="T186" />
         <tli id="T187" />
         <tli id="T188" />
         <tli id="T189" />
         <tli id="T190" />
         <tli id="T191" />
         <tli id="T192" />
         <tli id="T193" />
         <tli id="T194" />
         <tli id="T195" />
         <tli id="T196" />
         <tli id="T197" />
         <tli id="T198" />
         <tli id="T199" />
         <tli id="T200" />
         <tli id="T201" />
         <tli id="T202" />
         <tli id="T203" />
         <tli id="T204" />
         <tli id="T205" />
         <tli id="T206" />
         <tli id="T207" />
         <tli id="T208" />
         <tli id="T209" />
         <tli id="T210" />
         <tli id="T211" />
         <tli id="T212" />
         <tli id="T213" />
         <tli id="T214" />
         <tli id="T215" />
         <tli id="T216" />
         <tli id="T217" />
         <tli id="T218" />
         <tli id="T219" />
         <tli id="T220" />
         <tli id="T221" />
         <tli id="T222" />
         <tli id="T223" />
         <tli id="T224" />
         <tli id="T225" />
         <tli id="T226" />
         <tli id="T227" />
         <tli id="T228" />
         <tli id="T229" />
         <tli id="T230" />
         <tli id="T231" />
         <tli id="T232" />
         <tli id="T233" />
         <tli id="T234" />
         <tli id="T235" />
         <tli id="T236" />
         <tli id="T237" />
         <tli id="T238" />
         <tli id="T239" />
         <tli id="T240" />
         <tli id="T241" />
         <tli id="T242" />
         <tli id="T243" />
         <tli id="T244" />
         <tli id="T245" />
         <tli id="T246" />
         <tli id="T247" />
         <tli id="T248" />
         <tli id="T249" />
         <tli id="T250" />
         <tli id="T251" />
         <tli id="T252" />
         <tli id="T253" />
         <tli id="T254" />
         <tli id="T255" />
         <tli id="T256" />
         <tli id="T257" />
         <tli id="T258" />
         <tli id="T259" />
         <tli id="T260" />
         <tli id="T261" />
         <tli id="T262" />
         <tli id="T263" />
         <tli id="T264" />
         <tli id="T265" />
         <tli id="T266" />
         <tli id="T267" />
         <tli id="T268" />
         <tli id="T269" />
         <tli id="T270" />
         <tli id="T271" />
         <tli id="T272" />
         <tli id="T273" />
         <tli id="T274" />
         <tli id="T275" />
         <tli id="T276" />
         <tli id="T277" />
         <tli id="T278" />
         <tli id="T279" />
         <tli id="T280" />
         <tli id="T281" />
         <tli id="T282" />
         <tli id="T283" />
         <tli id="T284" />
         <tli id="T285" />
         <tli id="T286" />
         <tli id="T287" />
         <tli id="T288" />
         <tli id="T289" />
         <tli id="T290" />
         <tli id="T291" />
         <tli id="T292" />
         <tli id="T293" />
         <tli id="T294" />
         <tli id="T295" />
         <tli id="T296" />
         <tli id="T297" />
         <tli id="T298" />
         <tli id="T299" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="AA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T299" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Nüke</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">büzʼezʼəʔ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">amnobi</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T6" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Šide</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">nʼidən</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">ibiiʔ</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_26" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">Dĭ</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">nʼizeŋdən</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">sʼarbindən</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">essen</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">udabə</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">saj</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">nʼeʔlədən</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_50" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">So</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">abiiʔ</ts>
                  <nts id="Seg_56" n="HIAT:ip">,</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">sonə</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_62" n="HIAT:w" s="T16">sarbiiʔ</ts>
                  <nts id="Seg_63" n="HIAT:ip">.</nts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_66" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_68" n="HIAT:w" s="T17">Üʔleʔ</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_71" n="HIAT:w" s="T18">mĭbiiʔ</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_74" n="HIAT:w" s="T19">bünə</ts>
                  <nts id="Seg_75" n="HIAT:ip">.</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_78" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_80" n="HIAT:w" s="T20">Bü</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_83" n="HIAT:w" s="T21">mʼaŋlaʔ</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_86" n="HIAT:w" s="T22">šonnamnaiʔ</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_89" n="HIAT:w" s="T23">nunan</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_92" n="HIAT:w" s="T24">jilgəndə</ts>
                  <nts id="Seg_93" n="HIAT:ip">.</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_96" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_98" n="HIAT:w" s="T25">Bü</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_101" n="HIAT:w" s="T26">păʔlaʔbə</ts>
                  <nts id="Seg_102" n="HIAT:ip">.</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T38" id="Seg_105" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_107" n="HIAT:w" s="T27">Dĭzeŋ</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_110" n="HIAT:w" s="T28">kegərerbiiʔ:</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_112" n="HIAT:ip">"</nts>
                  <ts e="T30" id="Seg_114" n="HIAT:w" s="T29">Kudajən</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_117" n="HIAT:w" s="T30">ijat</ts>
                  <nts id="Seg_118" n="HIAT:ip">,</nts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_121" n="HIAT:w" s="T31">ijabaʔ</ts>
                  <nts id="Seg_122" n="HIAT:ip">,</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_125" n="HIAT:w" s="T32">kudajən</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_128" n="HIAT:w" s="T33">ijat</ts>
                  <nts id="Seg_129" n="HIAT:ip">,</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_132" n="HIAT:w" s="T34">săbəjʔtə</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_135" n="HIAT:w" s="T35">miʔnʼibeʔ</ts>
                  <nts id="Seg_136" n="HIAT:ip">,</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_139" n="HIAT:w" s="T36">kudajən</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_142" n="HIAT:w" s="T37">ijat</ts>
                  <nts id="Seg_143" n="HIAT:ip">!</nts>
                  <nts id="Seg_144" n="HIAT:ip">"</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_147" n="HIAT:u" s="T38">
                  <ts e="T39" id="Seg_149" n="HIAT:w" s="T38">Solaj</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_152" n="HIAT:w" s="T39">eʔbdəbə</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_155" n="HIAT:w" s="T40">ködərbi</ts>
                  <nts id="Seg_156" n="HIAT:ip">,</nts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_159" n="HIAT:w" s="T41">öʔleʔ</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_162" n="HIAT:w" s="T42">mĭbi</ts>
                  <nts id="Seg_163" n="HIAT:ip">.</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_166" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_168" n="HIAT:w" s="T43">Esseŋdə</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_171" n="HIAT:w" s="T44">ej</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_174" n="HIAT:w" s="T45">tulaʔ</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_177" n="HIAT:w" s="T46">eʔbdət</ts>
                  <nts id="Seg_178" n="HIAT:ip">.</nts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_181" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_183" n="HIAT:w" s="T47">Mana</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_186" n="HIAT:w" s="T48">pʼeldə</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_189" n="HIAT:w" s="T49">ködərbi</ts>
                  <nts id="Seg_190" n="HIAT:ip">.</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_193" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_195" n="HIAT:w" s="T50">Mana</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_198" n="HIAT:w" s="T51">eʔbdət</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_201" n="HIAT:w" s="T52">tubi</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_204" n="HIAT:w" s="T53">esseŋdə</ts>
                  <nts id="Seg_205" n="HIAT:ip">.</nts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_208" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_210" n="HIAT:w" s="T54">Esseŋ</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_213" n="HIAT:w" s="T55">oʔbdəbi</ts>
                  <nts id="Seg_214" n="HIAT:ip">,</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_217" n="HIAT:w" s="T56">tʼabəlaʔ</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_220" n="HIAT:w" s="T57">supsobiiʔ</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_223" n="HIAT:w" s="T58">dĭgəʔ</ts>
                  <nts id="Seg_224" n="HIAT:ip">.</nts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_227" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_229" n="HIAT:w" s="T59">Nunan</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_232" n="HIAT:w" s="T60">nĭgəndə</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_235" n="HIAT:w" s="T61">uʔbdəlbi</ts>
                  <nts id="Seg_236" n="HIAT:ip">,</nts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_239" n="HIAT:w" s="T62">öʔleʔ</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_242" n="HIAT:w" s="T63">mĭbi</ts>
                  <nts id="Seg_243" n="HIAT:ip">.</nts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_246" n="HIAT:u" s="T64">
                  <nts id="Seg_247" n="HIAT:ip">"</nts>
                  <ts e="T65" id="Seg_249" n="HIAT:w" s="T64">Kaŋgaʔ</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_252" n="HIAT:w" s="T65">aʔtʼə</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_255" n="HIAT:w" s="T66">dăra</ts>
                  <nts id="Seg_256" n="HIAT:ip">!</nts>
                  <nts id="Seg_257" n="HIAT:ip">"</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_260" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_262" n="HIAT:w" s="T67">Mĭlloʔbdəbiiʔ</ts>
                  <nts id="Seg_263" n="HIAT:ip">.</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_266" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_268" n="HIAT:w" s="T68">Kandəbiiʔ</ts>
                  <nts id="Seg_269" n="HIAT:ip">.</nts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_272" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_274" n="HIAT:w" s="T69">Kandəbiiʔ</ts>
                  <nts id="Seg_275" n="HIAT:ip">,</nts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_278" n="HIAT:w" s="T70">urgu</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_281" n="HIAT:w" s="T71">aʔtʼə</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_284" n="HIAT:w" s="T72">toʔbdobi</ts>
                  <nts id="Seg_285" n="HIAT:ip">.</nts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_288" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_290" n="HIAT:w" s="T73">Dĭzeŋ</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_293" n="HIAT:w" s="T74">aʔtʼən</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_296" n="HIAT:w" s="T75">toʔgəndə</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_299" n="HIAT:w" s="T76">amnaiʔ</ts>
                  <nts id="Seg_300" n="HIAT:ip">.</nts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_303" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_305" n="HIAT:w" s="T77">Kubindən</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_308" n="HIAT:w" s="T78">bozeraʔ</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_311" n="HIAT:w" s="T79">inezəbi</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_314" n="HIAT:w" s="T80">aləp</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_317" n="HIAT:w" s="T81">šonnamna</ts>
                  <nts id="Seg_318" n="HIAT:ip">.</nts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_321" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_323" n="HIAT:w" s="T82">Dĭzeŋ</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_326" n="HIAT:w" s="T83">šündəbiiʔ</ts>
                  <nts id="Seg_327" n="HIAT:ip">.</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_330" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_332" n="HIAT:w" s="T84">Inet</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_335" n="HIAT:w" s="T85">sinəgəndə</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_338" n="HIAT:w" s="T86">šolaʔ</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_341" n="HIAT:w" s="T87">üzəbi</ts>
                  <nts id="Seg_342" n="HIAT:ip">.</nts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_345" n="HIAT:u" s="T88">
                  <nts id="Seg_346" n="HIAT:ip">"</nts>
                  <ts e="T89" id="Seg_348" n="HIAT:w" s="T88">Penzüt</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_351" n="HIAT:w" s="T89">enəidəne</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_354" n="HIAT:w" s="T90">šündəlie</ts>
                  <nts id="Seg_355" n="HIAT:ip">.</nts>
                  <nts id="Seg_356" n="HIAT:ip">"</nts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_359" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_361" n="HIAT:w" s="T91">Bostə</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_364" n="HIAT:w" s="T92">ei</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_367" n="HIAT:w" s="T93">dĭ</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_370" n="HIAT:w" s="T94">kambi</ts>
                  <nts id="Seg_371" n="HIAT:ip">,</nts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_374" n="HIAT:w" s="T95">mĭllüʔbi</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_377" n="HIAT:w" s="T96">menəj</ts>
                  <nts id="Seg_378" n="HIAT:ip">.</nts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_381" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_383" n="HIAT:w" s="T97">Bazoʔ</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_386" n="HIAT:w" s="T98">sagər</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_389" n="HIAT:w" s="T99">inezəbi</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_392" n="HIAT:w" s="T100">aləp</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_395" n="HIAT:w" s="T101">šonnamna</ts>
                  <nts id="Seg_396" n="HIAT:ip">.</nts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T104" id="Seg_399" n="HIAT:u" s="T102">
                  <ts e="T103" id="Seg_401" n="HIAT:w" s="T102">Bazoʔ</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_404" n="HIAT:w" s="T103">šündlüʔbiiʔ</ts>
                  <nts id="Seg_405" n="HIAT:ip">.</nts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_408" n="HIAT:u" s="T104">
                  <ts e="T105" id="Seg_410" n="HIAT:w" s="T104">Inet</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_413" n="HIAT:w" s="T105">bazoʔ</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_416" n="HIAT:w" s="T106">sinigəndə</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_419" n="HIAT:w" s="T107">šolaʔ</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_422" n="HIAT:w" s="T108">üzəbi</ts>
                  <nts id="Seg_423" n="HIAT:ip">.</nts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_426" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_428" n="HIAT:w" s="T109">Bazoʔ</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_431" n="HIAT:w" s="T110">penzüt</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_434" n="HIAT:w" s="T111">mălia</ts>
                  <nts id="Seg_435" n="HIAT:ip">.</nts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_438" n="HIAT:u" s="T112">
                  <nts id="Seg_439" n="HIAT:ip">"</nts>
                  <ts e="T113" id="Seg_441" n="HIAT:w" s="T112">Enəidəne</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_444" n="HIAT:w" s="T113">bostu</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_447" n="HIAT:w" s="T114">šündəlie</ts>
                  <nts id="Seg_448" n="HIAT:ip">.</nts>
                  <nts id="Seg_449" n="HIAT:ip">"</nts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_452" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_454" n="HIAT:w" s="T115">Bostə</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_457" n="HIAT:w" s="T116">bazoʔ</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_460" n="HIAT:w" s="T117">mellüʔbi</ts>
                  <nts id="Seg_461" n="HIAT:ip">.</nts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_464" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_466" n="HIAT:w" s="T118">Dĭ</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_469" n="HIAT:w" s="T119">esseŋ</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_472" n="HIAT:w" s="T120">kergierbiiʔ</ts>
                  <nts id="Seg_473" n="HIAT:ip">,</nts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_476" n="HIAT:w" s="T121">kergierieiʔ:</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_478" n="HIAT:ip">"</nts>
                  <ts e="T123" id="Seg_480" n="HIAT:w" s="T122">Kanda</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_483" n="HIAT:w" s="T123">aʔtʼəlaʔ</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_486" n="HIAT:w" s="T124">moguj</ts>
                  <nts id="Seg_487" n="HIAT:ip">,</nts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_490" n="HIAT:w" s="T125">šona</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_493" n="HIAT:w" s="T126">aʔtʼəlaʔ</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_496" n="HIAT:w" s="T127">nagoguj</ts>
                  <nts id="Seg_497" n="HIAT:ip">!</nts>
                  <nts id="Seg_498" n="HIAT:ip">"</nts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_501" n="HIAT:u" s="T128">
                  <ts e="T129" id="Seg_503" n="HIAT:w" s="T128">Bazoʔ</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_506" n="HIAT:w" s="T129">sĭri</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_509" n="HIAT:w" s="T130">inezəbi</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_512" n="HIAT:w" s="T131">aləp</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_515" n="HIAT:w" s="T132">šonnamna</ts>
                  <nts id="Seg_516" n="HIAT:ip">.</nts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T137" id="Seg_519" n="HIAT:u" s="T133">
                  <ts e="T134" id="Seg_521" n="HIAT:w" s="T133">Dĭzeŋ</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_524" n="HIAT:w" s="T134">esseŋ</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_527" n="HIAT:w" s="T135">bazoʔ</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_530" n="HIAT:w" s="T136">šündəbiiʔ</ts>
                  <nts id="Seg_531" n="HIAT:ip">.</nts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_534" n="HIAT:u" s="T137">
                  <ts e="T138" id="Seg_536" n="HIAT:w" s="T137">Inet</ts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_539" n="HIAT:w" s="T138">nulaːmbi</ts>
                  <nts id="Seg_540" n="HIAT:ip">.</nts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T142" id="Seg_543" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_545" n="HIAT:w" s="T139">Dĭgəttə</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_548" n="HIAT:w" s="T140">kallaʔ</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_551" n="HIAT:w" s="T141">kubi</ts>
                  <nts id="Seg_552" n="HIAT:ip">.</nts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T145" id="Seg_555" n="HIAT:u" s="T142">
                  <nts id="Seg_556" n="HIAT:ip">"</nts>
                  <ts e="T143" id="Seg_558" n="HIAT:w" s="T142">Kajət</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_561" n="HIAT:w" s="T143">esseŋ</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_564" n="HIAT:w" s="T144">igəleʔ</ts>
                  <nts id="Seg_565" n="HIAT:ip">?</nts>
                  <nts id="Seg_566" n="HIAT:ip">"</nts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T149" id="Seg_569" n="HIAT:u" s="T145">
                  <nts id="Seg_570" n="HIAT:ip">"</nts>
                  <ts e="T146" id="Seg_572" n="HIAT:w" s="T145">Dĭrgit</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_575" n="HIAT:w" s="T146">dĭrgit</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_578" n="HIAT:w" s="T147">esseŋ</ts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_581" n="HIAT:w" s="T148">igəbeʔ</ts>
                  <nts id="Seg_582" n="HIAT:ip">.</nts>
                  <nts id="Seg_583" n="HIAT:ip">"</nts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T155" id="Seg_586" n="HIAT:u" s="T149">
                  <ts e="T150" id="Seg_588" n="HIAT:w" s="T149">Esseŋdə</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_591" n="HIAT:w" s="T150">šü</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_594" n="HIAT:w" s="T151">embi</ts>
                  <nts id="Seg_595" n="HIAT:ip">,</nts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_598" n="HIAT:w" s="T152">kambi</ts>
                  <nts id="Seg_599" n="HIAT:ip">,</nts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_602" n="HIAT:w" s="T153">bulan</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_605" n="HIAT:w" s="T154">tʼippi</ts>
                  <nts id="Seg_606" n="HIAT:ip">.</nts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T162" id="Seg_609" n="HIAT:u" s="T155">
                  <ts e="T156" id="Seg_611" n="HIAT:w" s="T155">Teppi</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_614" n="HIAT:w" s="T156">esseŋdə</ts>
                  <nts id="Seg_615" n="HIAT:ip">,</nts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_618" n="HIAT:w" s="T157">bulanən</ts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_621" n="HIAT:w" s="T158">kubatsʼəʔ</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_624" n="HIAT:w" s="T159">esseŋdə</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_627" n="HIAT:w" s="T160">maʔ</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_630" n="HIAT:w" s="T161">abi</ts>
                  <nts id="Seg_631" n="HIAT:ip">.</nts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T164" id="Seg_634" n="HIAT:u" s="T162">
                  <nts id="Seg_635" n="HIAT:ip">"</nts>
                  <ts e="T163" id="Seg_637" n="HIAT:w" s="T162">Amnogaʔ</ts>
                  <nts id="Seg_638" n="HIAT:ip">,</nts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_641" n="HIAT:w" s="T163">esseŋ</ts>
                  <nts id="Seg_642" n="HIAT:ip">!</nts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T169" id="Seg_645" n="HIAT:u" s="T164">
                  <ts e="T165" id="Seg_647" n="HIAT:w" s="T164">Tʼili</ts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_650" n="HIAT:w" s="T165">ibinʼə</ts>
                  <nts id="Seg_651" n="HIAT:ip">,</nts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_654" n="HIAT:w" s="T166">parbinʼə</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_657" n="HIAT:w" s="T167">iləm</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_660" n="HIAT:w" s="T168">šiʔnʼileʔ</ts>
                  <nts id="Seg_661" n="HIAT:ip">.</nts>
                  <nts id="Seg_662" n="HIAT:ip">"</nts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T171" id="Seg_665" n="HIAT:u" s="T169">
                  <ts e="T170" id="Seg_667" n="HIAT:w" s="T169">Dărə</ts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_670" n="HIAT:w" s="T170">mămbi</ts>
                  <nts id="Seg_671" n="HIAT:ip">.</nts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T182" id="Seg_674" n="HIAT:u" s="T171">
                  <ts e="T172" id="Seg_676" n="HIAT:w" s="T171">Bostu</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_679" n="HIAT:w" s="T172">mĭllüʔbi</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_682" n="HIAT:w" s="T173">i</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_685" n="HIAT:w" s="T174">nagurgit</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_688" n="HIAT:w" s="T175">tʼalan</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_691" n="HIAT:w" s="T176">dĭ</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_694" n="HIAT:w" s="T177">šide</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_697" n="HIAT:w" s="T178">aləbən</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_700" n="HIAT:w" s="T179">boš</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_703" n="HIAT:w" s="T180">inezeŋdən</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_706" n="HIAT:w" s="T181">parlaːndəgaiʔ</ts>
                  <nts id="Seg_707" n="HIAT:ip">.</nts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_710" n="HIAT:u" s="T182">
                  <ts e="T183" id="Seg_712" n="HIAT:w" s="T182">Konzanzaŋdən</ts>
                  <nts id="Seg_713" n="HIAT:ip">,</nts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_716" n="HIAT:w" s="T183">pakzənzaŋdən</ts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_719" n="HIAT:w" s="T184">inen</ts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_722" n="HIAT:w" s="T185">nanəndə</ts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_725" n="HIAT:w" s="T186">lambərlaʔ</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_728" n="HIAT:w" s="T187">kandəgajəʔ</ts>
                  <nts id="Seg_729" n="HIAT:ip">.</nts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T193" id="Seg_732" n="HIAT:u" s="T188">
                  <ts e="T189" id="Seg_734" n="HIAT:w" s="T188">Dĭzen</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_737" n="HIAT:w" s="T189">piʔnettə</ts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_740" n="HIAT:w" s="T190">sĭri</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_743" n="HIAT:w" s="T191">inezəbi</ts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_746" n="HIAT:w" s="T192">parlamna</ts>
                  <nts id="Seg_747" n="HIAT:ip">.</nts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T200" id="Seg_750" n="HIAT:u" s="T193">
                  <ts e="T194" id="Seg_752" n="HIAT:w" s="T193">Šobi</ts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_755" n="HIAT:w" s="T194">esseŋgəndən</ts>
                  <nts id="Seg_756" n="HIAT:ip">,</nts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_759" n="HIAT:w" s="T195">ibi</ts>
                  <nts id="Seg_760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_762" n="HIAT:w" s="T196">essem</ts>
                  <nts id="Seg_763" n="HIAT:ip">,</nts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_766" n="HIAT:w" s="T197">maʔgəndə</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_769" n="HIAT:w" s="T198">kunnaʔ</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_772" n="HIAT:w" s="T199">kambi</ts>
                  <nts id="Seg_773" n="HIAT:ip">.</nts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T206" id="Seg_776" n="HIAT:u" s="T200">
                  <ts e="T201" id="Seg_778" n="HIAT:w" s="T200">Mălia:</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_780" n="HIAT:ip">"</nts>
                  <ts e="T202" id="Seg_782" n="HIAT:w" s="T201">Măn</ts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_785" n="HIAT:w" s="T202">amnollaʔbo</ts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_788" n="HIAT:w" s="T203">aľi</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_791" n="HIAT:w" s="T204">maʔgənʼilaʔ</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_794" n="HIAT:w" s="T205">kalləlaʔbo</ts>
                  <nts id="Seg_795" n="HIAT:ip">?</nts>
                  <nts id="Seg_796" n="HIAT:ip">"</nts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T210" id="Seg_799" n="HIAT:u" s="T206">
                  <ts e="T207" id="Seg_801" n="HIAT:w" s="T206">Oʔbdə</ts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_804" n="HIAT:w" s="T207">mălia:</ts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_806" n="HIAT:ip">"</nts>
                  <ts e="T209" id="Seg_808" n="HIAT:w" s="T208">Măn</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_811" n="HIAT:w" s="T209">kallam</ts>
                  <nts id="Seg_812" n="HIAT:ip">.</nts>
                  <nts id="Seg_813" n="HIAT:ip">"</nts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T213" id="Seg_816" n="HIAT:u" s="T210">
                  <ts e="T211" id="Seg_818" n="HIAT:w" s="T210">Oʔbdə</ts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_821" n="HIAT:w" s="T211">dĭgən</ts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_824" n="HIAT:w" s="T212">maʔbi</ts>
                  <nts id="Seg_825" n="HIAT:ip">.</nts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T217" id="Seg_828" n="HIAT:u" s="T213">
                  <ts e="T214" id="Seg_830" n="HIAT:w" s="T213">Aləbəj</ts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_833" n="HIAT:w" s="T214">ine</ts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_836" n="HIAT:w" s="T215">konzandəlaʔ</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_839" n="HIAT:w" s="T216">mĭbi</ts>
                  <nts id="Seg_840" n="HIAT:ip">.</nts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T220" id="Seg_843" n="HIAT:u" s="T217">
                  <ts e="T218" id="Seg_845" n="HIAT:w" s="T217">Mĭllüʔbi</ts>
                  <nts id="Seg_846" n="HIAT:ip">,</nts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_849" n="HIAT:w" s="T218">maʔgəndə</ts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_852" n="HIAT:w" s="T219">šobi</ts>
                  <nts id="Seg_853" n="HIAT:ip">.</nts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T223" id="Seg_856" n="HIAT:u" s="T220">
                  <ts e="T221" id="Seg_858" n="HIAT:w" s="T220">Onʼiʔ</ts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_861" n="HIAT:w" s="T221">maʔ</ts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_864" n="HIAT:w" s="T222">nuga</ts>
                  <nts id="Seg_865" n="HIAT:ip">.</nts>
                  <nts id="Seg_866" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T227" id="Seg_868" n="HIAT:u" s="T223">
                  <ts e="T224" id="Seg_870" n="HIAT:w" s="T223">Jadat</ts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_873" n="HIAT:w" s="T224">kak</ts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_876" n="HIAT:w" s="T225">tüjə</ts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_879" n="HIAT:w" s="T226">naga</ts>
                  <nts id="Seg_880" n="HIAT:ip">.</nts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T230" id="Seg_883" n="HIAT:u" s="T227">
                  <ts e="T228" id="Seg_885" n="HIAT:w" s="T227">Dĭ</ts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_888" n="HIAT:w" s="T228">maʔdə</ts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_891" n="HIAT:w" s="T229">šübi</ts>
                  <nts id="Seg_892" n="HIAT:ip">.</nts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T237" id="Seg_895" n="HIAT:u" s="T230">
                  <ts e="T231" id="Seg_897" n="HIAT:w" s="T230">Surariat:</ts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_899" n="HIAT:ip">"</nts>
                  <ts e="T232" id="Seg_901" n="HIAT:w" s="T231">Tʼaktə</ts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_904" n="HIAT:w" s="T232">nüke</ts>
                  <nts id="Seg_905" n="HIAT:ip">,</nts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_908" n="HIAT:w" s="T233">tʼaktə</ts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_911" n="HIAT:w" s="T234">büzʼe</ts>
                  <nts id="Seg_912" n="HIAT:ip">,</nts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_915" n="HIAT:w" s="T235">jadalaʔ</ts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_918" n="HIAT:w" s="T236">giraːmbi</ts>
                  <nts id="Seg_919" n="HIAT:ip">?</nts>
                  <nts id="Seg_920" n="HIAT:ip">"</nts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T242" id="Seg_923" n="HIAT:u" s="T237">
                  <ts e="T238" id="Seg_925" n="HIAT:w" s="T237">Dĭzeŋ</ts>
                  <nts id="Seg_926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_928" n="HIAT:w" s="T238">nörbəlieiʔ:</ts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_930" n="HIAT:ip">"</nts>
                  <ts e="T240" id="Seg_932" n="HIAT:w" s="T239">Šide</ts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_935" n="HIAT:w" s="T240">nʼibeʔ</ts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_938" n="HIAT:w" s="T241">ibi</ts>
                  <nts id="Seg_939" n="HIAT:ip">.</nts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T248" id="Seg_942" n="HIAT:u" s="T242">
                  <ts e="T243" id="Seg_944" n="HIAT:w" s="T242">Il</ts>
                  <nts id="Seg_945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_947" n="HIAT:w" s="T243">sonə</ts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_950" n="HIAT:w" s="T244">sarbize</ts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_953" n="HIAT:w" s="T245">öʔleʔ</ts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_956" n="HIAT:w" s="T246">mĭbi</ts>
                  <nts id="Seg_957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_959" n="HIAT:w" s="T247">bünə</ts>
                  <nts id="Seg_960" n="HIAT:ip">.</nts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T252" id="Seg_963" n="HIAT:u" s="T248">
                  <ts e="T249" id="Seg_965" n="HIAT:w" s="T248">Dĭzen</ts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_968" n="HIAT:w" s="T249">piʔneendə</ts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_971" n="HIAT:w" s="T250">koʔbdobaʔ</ts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_974" n="HIAT:w" s="T251">ibi</ts>
                  <nts id="Seg_975" n="HIAT:ip">.</nts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T257" id="Seg_978" n="HIAT:u" s="T252">
                  <ts e="T253" id="Seg_980" n="HIAT:w" s="T252">Dĭ</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_983" n="HIAT:w" s="T253">iləm</ts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_986" n="HIAT:w" s="T254">bar</ts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_989" n="HIAT:w" s="T255">kutlaʔ</ts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_992" n="HIAT:w" s="T256">kumbi</ts>
                  <nts id="Seg_993" n="HIAT:ip">.</nts>
                  <nts id="Seg_994" n="HIAT:ip">"</nts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T261" id="Seg_997" n="HIAT:u" s="T257">
                  <nts id="Seg_998" n="HIAT:ip">"</nts>
                  <ts e="T258" id="Seg_1000" n="HIAT:w" s="T257">A</ts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_1003" n="HIAT:w" s="T258">gijen</ts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_1006" n="HIAT:w" s="T259">koʔbdolaʔ</ts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_1009" n="HIAT:w" s="T260">giraːmbi</ts>
                  <nts id="Seg_1010" n="HIAT:ip">?</nts>
                  <nts id="Seg_1011" n="HIAT:ip">"</nts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T264" id="Seg_1014" n="HIAT:u" s="T261">
                  <nts id="Seg_1015" n="HIAT:ip">"</nts>
                  <ts e="T262" id="Seg_1017" n="HIAT:w" s="T261">Bospaʔ</ts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1020" n="HIAT:w" s="T262">ej</ts>
                  <nts id="Seg_1021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_1023" n="HIAT:w" s="T263">tĭmnebeʔ</ts>
                  <nts id="Seg_1024" n="HIAT:ip">.</nts>
                  <nts id="Seg_1025" n="HIAT:ip">"</nts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T271" id="Seg_1028" n="HIAT:u" s="T264">
                  <ts e="T265" id="Seg_1030" n="HIAT:w" s="T264">Dĭ</ts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1033" n="HIAT:w" s="T265">nʼi</ts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1036" n="HIAT:w" s="T266">inebə</ts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1039" n="HIAT:w" s="T267">šĭbi</ts>
                  <nts id="Seg_1040" n="HIAT:ip">,</nts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1043" n="HIAT:w" s="T268">pʼeleʔ</ts>
                  <nts id="Seg_1044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1046" n="HIAT:w" s="T269">kallaʔ</ts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1049" n="HIAT:w" s="T270">tʼürbi</ts>
                  <nts id="Seg_1050" n="HIAT:ip">.</nts>
                  <nts id="Seg_1051" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T274" id="Seg_1053" n="HIAT:u" s="T271">
                  <ts e="T272" id="Seg_1055" n="HIAT:w" s="T271">Kambi</ts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1058" n="HIAT:w" s="T272">urgo</ts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1061" n="HIAT:w" s="T273">karaʔdə</ts>
                  <nts id="Seg_1062" n="HIAT:ip">.</nts>
                  <nts id="Seg_1063" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T277" id="Seg_1065" n="HIAT:u" s="T274">
                  <ts e="T275" id="Seg_1067" n="HIAT:w" s="T274">Dĭgən</ts>
                  <nts id="Seg_1068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1070" n="HIAT:w" s="T275">tunoldəlaʔ</ts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1073" n="HIAT:w" s="T276">mĭlleʔbi</ts>
                  <nts id="Seg_1074" n="HIAT:ip">.</nts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T282" id="Seg_1077" n="HIAT:u" s="T277">
                  <ts e="T278" id="Seg_1079" n="HIAT:w" s="T277">Dĭ</ts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1082" n="HIAT:w" s="T278">nʼim</ts>
                  <nts id="Seg_1083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1085" n="HIAT:w" s="T279">kubiza</ts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1088" n="HIAT:w" s="T280">tunoldəlaʔ</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1091" n="HIAT:w" s="T281">šobi</ts>
                  <nts id="Seg_1092" n="HIAT:ip">.</nts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T286" id="Seg_1095" n="HIAT:u" s="T282">
                  <ts e="T283" id="Seg_1097" n="HIAT:w" s="T282">Süʔməleʔ</ts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1100" n="HIAT:w" s="T283">naŋbi</ts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1103" n="HIAT:w" s="T284">dĭ</ts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1106" n="HIAT:w" s="T285">nʼinə</ts>
                  <nts id="Seg_1107" n="HIAT:ip">.</nts>
                  <nts id="Seg_1108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T288" id="Seg_1110" n="HIAT:u" s="T286">
                  <ts e="T287" id="Seg_1112" n="HIAT:w" s="T286">Dĭgəttə</ts>
                  <nts id="Seg_1113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1115" n="HIAT:w" s="T287">tʼabərobiiʔ</ts>
                  <nts id="Seg_1116" n="HIAT:ip">.</nts>
                  <nts id="Seg_1117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T291" id="Seg_1119" n="HIAT:u" s="T288">
                  <ts e="T289" id="Seg_1121" n="HIAT:w" s="T288">Kagat</ts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1124" n="HIAT:w" s="T289">üštəlüʔbi</ts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1127" n="HIAT:w" s="T290">dĭm</ts>
                  <nts id="Seg_1128" n="HIAT:ip">.</nts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T297" id="Seg_1131" n="HIAT:u" s="T291">
                  <ts e="T292" id="Seg_1133" n="HIAT:w" s="T291">Tʼabolaʔbə</ts>
                  <nts id="Seg_1134" n="HIAT:ip">,</nts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1137" n="HIAT:w" s="T292">nörbəleʔbə:</ts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1139" n="HIAT:ip">"</nts>
                  <ts e="T294" id="Seg_1141" n="HIAT:w" s="T293">Măn</ts>
                  <nts id="Seg_1142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1144" n="HIAT:w" s="T294">tăn</ts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1147" n="HIAT:w" s="T295">kagal</ts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1150" n="HIAT:w" s="T296">igem</ts>
                  <nts id="Seg_1151" n="HIAT:ip">.</nts>
                  <nts id="Seg_1152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T299" id="Seg_1154" n="HIAT:u" s="T297">
                  <ts e="T298" id="Seg_1156" n="HIAT:w" s="T297">Ej</ts>
                  <nts id="Seg_1157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1159" n="HIAT:w" s="T298">tʼabəroʔ</ts>
                  <nts id="Seg_1160" n="HIAT:ip">!</nts>
                  <nts id="Seg_1161" n="HIAT:ip">"</nts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T299" id="Seg_1163" n="sc" s="T0">
               <ts e="T1" id="Seg_1165" n="e" s="T0">Nüke </ts>
               <ts e="T2" id="Seg_1167" n="e" s="T1">büzʼezʼəʔ </ts>
               <ts e="T3" id="Seg_1169" n="e" s="T2">amnobi. </ts>
               <ts e="T4" id="Seg_1171" n="e" s="T3">Šide </ts>
               <ts e="T5" id="Seg_1173" n="e" s="T4">nʼidən </ts>
               <ts e="T6" id="Seg_1175" n="e" s="T5">ibiiʔ. </ts>
               <ts e="T7" id="Seg_1177" n="e" s="T6">Dĭ </ts>
               <ts e="T8" id="Seg_1179" n="e" s="T7">nʼizeŋdən </ts>
               <ts e="T9" id="Seg_1181" n="e" s="T8">sʼarbindən </ts>
               <ts e="T10" id="Seg_1183" n="e" s="T9">essen </ts>
               <ts e="T11" id="Seg_1185" n="e" s="T10">udabə </ts>
               <ts e="T12" id="Seg_1187" n="e" s="T11">saj </ts>
               <ts e="T13" id="Seg_1189" n="e" s="T12">nʼeʔlədən. </ts>
               <ts e="T14" id="Seg_1191" n="e" s="T13">So </ts>
               <ts e="T15" id="Seg_1193" n="e" s="T14">abiiʔ, </ts>
               <ts e="T16" id="Seg_1195" n="e" s="T15">sonə </ts>
               <ts e="T17" id="Seg_1197" n="e" s="T16">sarbiiʔ. </ts>
               <ts e="T18" id="Seg_1199" n="e" s="T17">Üʔleʔ </ts>
               <ts e="T19" id="Seg_1201" n="e" s="T18">mĭbiiʔ </ts>
               <ts e="T20" id="Seg_1203" n="e" s="T19">bünə. </ts>
               <ts e="T21" id="Seg_1205" n="e" s="T20">Bü </ts>
               <ts e="T22" id="Seg_1207" n="e" s="T21">mʼaŋlaʔ </ts>
               <ts e="T23" id="Seg_1209" n="e" s="T22">šonnamnaiʔ </ts>
               <ts e="T24" id="Seg_1211" n="e" s="T23">nunan </ts>
               <ts e="T25" id="Seg_1213" n="e" s="T24">jilgəndə. </ts>
               <ts e="T26" id="Seg_1215" n="e" s="T25">Bü </ts>
               <ts e="T27" id="Seg_1217" n="e" s="T26">păʔlaʔbə. </ts>
               <ts e="T28" id="Seg_1219" n="e" s="T27">Dĭzeŋ </ts>
               <ts e="T29" id="Seg_1221" n="e" s="T28">kegərerbiiʔ: </ts>
               <ts e="T30" id="Seg_1223" n="e" s="T29">"Kudajən </ts>
               <ts e="T31" id="Seg_1225" n="e" s="T30">ijat, </ts>
               <ts e="T32" id="Seg_1227" n="e" s="T31">ijabaʔ, </ts>
               <ts e="T33" id="Seg_1229" n="e" s="T32">kudajən </ts>
               <ts e="T34" id="Seg_1231" n="e" s="T33">ijat, </ts>
               <ts e="T35" id="Seg_1233" n="e" s="T34">săbəjʔtə </ts>
               <ts e="T36" id="Seg_1235" n="e" s="T35">miʔnʼibeʔ, </ts>
               <ts e="T37" id="Seg_1237" n="e" s="T36">kudajən </ts>
               <ts e="T38" id="Seg_1239" n="e" s="T37">ijat!" </ts>
               <ts e="T39" id="Seg_1241" n="e" s="T38">Solaj </ts>
               <ts e="T40" id="Seg_1243" n="e" s="T39">eʔbdəbə </ts>
               <ts e="T41" id="Seg_1245" n="e" s="T40">ködərbi, </ts>
               <ts e="T42" id="Seg_1247" n="e" s="T41">öʔleʔ </ts>
               <ts e="T43" id="Seg_1249" n="e" s="T42">mĭbi. </ts>
               <ts e="T44" id="Seg_1251" n="e" s="T43">Esseŋdə </ts>
               <ts e="T45" id="Seg_1253" n="e" s="T44">ej </ts>
               <ts e="T46" id="Seg_1255" n="e" s="T45">tulaʔ </ts>
               <ts e="T47" id="Seg_1257" n="e" s="T46">eʔbdət. </ts>
               <ts e="T48" id="Seg_1259" n="e" s="T47">Mana </ts>
               <ts e="T49" id="Seg_1261" n="e" s="T48">pʼeldə </ts>
               <ts e="T50" id="Seg_1263" n="e" s="T49">ködərbi. </ts>
               <ts e="T51" id="Seg_1265" n="e" s="T50">Mana </ts>
               <ts e="T52" id="Seg_1267" n="e" s="T51">eʔbdət </ts>
               <ts e="T53" id="Seg_1269" n="e" s="T52">tubi </ts>
               <ts e="T54" id="Seg_1271" n="e" s="T53">esseŋdə. </ts>
               <ts e="T55" id="Seg_1273" n="e" s="T54">Esseŋ </ts>
               <ts e="T56" id="Seg_1275" n="e" s="T55">oʔbdəbi, </ts>
               <ts e="T57" id="Seg_1277" n="e" s="T56">tʼabəlaʔ </ts>
               <ts e="T58" id="Seg_1279" n="e" s="T57">supsobiiʔ </ts>
               <ts e="T59" id="Seg_1281" n="e" s="T58">dĭgəʔ. </ts>
               <ts e="T60" id="Seg_1283" n="e" s="T59">Nunan </ts>
               <ts e="T61" id="Seg_1285" n="e" s="T60">nĭgəndə </ts>
               <ts e="T62" id="Seg_1287" n="e" s="T61">uʔbdəlbi, </ts>
               <ts e="T63" id="Seg_1289" n="e" s="T62">öʔleʔ </ts>
               <ts e="T64" id="Seg_1291" n="e" s="T63">mĭbi. </ts>
               <ts e="T65" id="Seg_1293" n="e" s="T64">"Kaŋgaʔ </ts>
               <ts e="T66" id="Seg_1295" n="e" s="T65">aʔtʼə </ts>
               <ts e="T67" id="Seg_1297" n="e" s="T66">dăra!" </ts>
               <ts e="T68" id="Seg_1299" n="e" s="T67">Mĭlloʔbdəbiiʔ. </ts>
               <ts e="T69" id="Seg_1301" n="e" s="T68">Kandəbiiʔ. </ts>
               <ts e="T70" id="Seg_1303" n="e" s="T69">Kandəbiiʔ, </ts>
               <ts e="T71" id="Seg_1305" n="e" s="T70">urgu </ts>
               <ts e="T72" id="Seg_1307" n="e" s="T71">aʔtʼə </ts>
               <ts e="T73" id="Seg_1309" n="e" s="T72">toʔbdobi. </ts>
               <ts e="T74" id="Seg_1311" n="e" s="T73">Dĭzeŋ </ts>
               <ts e="T75" id="Seg_1313" n="e" s="T74">aʔtʼən </ts>
               <ts e="T76" id="Seg_1315" n="e" s="T75">toʔgəndə </ts>
               <ts e="T77" id="Seg_1317" n="e" s="T76">amnaiʔ. </ts>
               <ts e="T78" id="Seg_1319" n="e" s="T77">Kubindən </ts>
               <ts e="T79" id="Seg_1321" n="e" s="T78">bozeraʔ </ts>
               <ts e="T80" id="Seg_1323" n="e" s="T79">inezəbi </ts>
               <ts e="T81" id="Seg_1325" n="e" s="T80">aləp </ts>
               <ts e="T82" id="Seg_1327" n="e" s="T81">šonnamna. </ts>
               <ts e="T83" id="Seg_1329" n="e" s="T82">Dĭzeŋ </ts>
               <ts e="T84" id="Seg_1331" n="e" s="T83">šündəbiiʔ. </ts>
               <ts e="T85" id="Seg_1333" n="e" s="T84">Inet </ts>
               <ts e="T86" id="Seg_1335" n="e" s="T85">sinəgəndə </ts>
               <ts e="T87" id="Seg_1337" n="e" s="T86">šolaʔ </ts>
               <ts e="T88" id="Seg_1339" n="e" s="T87">üzəbi. </ts>
               <ts e="T89" id="Seg_1341" n="e" s="T88">"Penzüt </ts>
               <ts e="T90" id="Seg_1343" n="e" s="T89">enəidəne </ts>
               <ts e="T91" id="Seg_1345" n="e" s="T90">šündəlie." </ts>
               <ts e="T92" id="Seg_1347" n="e" s="T91">Bostə </ts>
               <ts e="T93" id="Seg_1349" n="e" s="T92">ei </ts>
               <ts e="T94" id="Seg_1351" n="e" s="T93">dĭ </ts>
               <ts e="T95" id="Seg_1353" n="e" s="T94">kambi, </ts>
               <ts e="T96" id="Seg_1355" n="e" s="T95">mĭllüʔbi </ts>
               <ts e="T97" id="Seg_1357" n="e" s="T96">menəj. </ts>
               <ts e="T98" id="Seg_1359" n="e" s="T97">Bazoʔ </ts>
               <ts e="T99" id="Seg_1361" n="e" s="T98">sagər </ts>
               <ts e="T100" id="Seg_1363" n="e" s="T99">inezəbi </ts>
               <ts e="T101" id="Seg_1365" n="e" s="T100">aləp </ts>
               <ts e="T102" id="Seg_1367" n="e" s="T101">šonnamna. </ts>
               <ts e="T103" id="Seg_1369" n="e" s="T102">Bazoʔ </ts>
               <ts e="T104" id="Seg_1371" n="e" s="T103">šündlüʔbiiʔ. </ts>
               <ts e="T105" id="Seg_1373" n="e" s="T104">Inet </ts>
               <ts e="T106" id="Seg_1375" n="e" s="T105">bazoʔ </ts>
               <ts e="T107" id="Seg_1377" n="e" s="T106">sinigəndə </ts>
               <ts e="T108" id="Seg_1379" n="e" s="T107">šolaʔ </ts>
               <ts e="T109" id="Seg_1381" n="e" s="T108">üzəbi. </ts>
               <ts e="T110" id="Seg_1383" n="e" s="T109">Bazoʔ </ts>
               <ts e="T111" id="Seg_1385" n="e" s="T110">penzüt </ts>
               <ts e="T112" id="Seg_1387" n="e" s="T111">mălia. </ts>
               <ts e="T113" id="Seg_1389" n="e" s="T112">"Enəidəne </ts>
               <ts e="T114" id="Seg_1391" n="e" s="T113">bostu </ts>
               <ts e="T115" id="Seg_1393" n="e" s="T114">šündəlie." </ts>
               <ts e="T116" id="Seg_1395" n="e" s="T115">Bostə </ts>
               <ts e="T117" id="Seg_1397" n="e" s="T116">bazoʔ </ts>
               <ts e="T118" id="Seg_1399" n="e" s="T117">mellüʔbi. </ts>
               <ts e="T119" id="Seg_1401" n="e" s="T118">Dĭ </ts>
               <ts e="T120" id="Seg_1403" n="e" s="T119">esseŋ </ts>
               <ts e="T121" id="Seg_1405" n="e" s="T120">kergierbiiʔ, </ts>
               <ts e="T122" id="Seg_1407" n="e" s="T121">kergierieiʔ: </ts>
               <ts e="T123" id="Seg_1409" n="e" s="T122">"Kanda </ts>
               <ts e="T124" id="Seg_1411" n="e" s="T123">aʔtʼəlaʔ </ts>
               <ts e="T125" id="Seg_1413" n="e" s="T124">moguj, </ts>
               <ts e="T126" id="Seg_1415" n="e" s="T125">šona </ts>
               <ts e="T127" id="Seg_1417" n="e" s="T126">aʔtʼəlaʔ </ts>
               <ts e="T128" id="Seg_1419" n="e" s="T127">nagoguj!" </ts>
               <ts e="T129" id="Seg_1421" n="e" s="T128">Bazoʔ </ts>
               <ts e="T130" id="Seg_1423" n="e" s="T129">sĭri </ts>
               <ts e="T131" id="Seg_1425" n="e" s="T130">inezəbi </ts>
               <ts e="T132" id="Seg_1427" n="e" s="T131">aləp </ts>
               <ts e="T133" id="Seg_1429" n="e" s="T132">šonnamna. </ts>
               <ts e="T134" id="Seg_1431" n="e" s="T133">Dĭzeŋ </ts>
               <ts e="T135" id="Seg_1433" n="e" s="T134">esseŋ </ts>
               <ts e="T136" id="Seg_1435" n="e" s="T135">bazoʔ </ts>
               <ts e="T137" id="Seg_1437" n="e" s="T136">šündəbiiʔ. </ts>
               <ts e="T138" id="Seg_1439" n="e" s="T137">Inet </ts>
               <ts e="T139" id="Seg_1441" n="e" s="T138">nulaːmbi. </ts>
               <ts e="T140" id="Seg_1443" n="e" s="T139">Dĭgəttə </ts>
               <ts e="T141" id="Seg_1445" n="e" s="T140">kallaʔ </ts>
               <ts e="T142" id="Seg_1447" n="e" s="T141">kubi. </ts>
               <ts e="T143" id="Seg_1449" n="e" s="T142">"Kajət </ts>
               <ts e="T144" id="Seg_1451" n="e" s="T143">esseŋ </ts>
               <ts e="T145" id="Seg_1453" n="e" s="T144">igəleʔ?" </ts>
               <ts e="T146" id="Seg_1455" n="e" s="T145">"Dĭrgit </ts>
               <ts e="T147" id="Seg_1457" n="e" s="T146">dĭrgit </ts>
               <ts e="T148" id="Seg_1459" n="e" s="T147">esseŋ </ts>
               <ts e="T149" id="Seg_1461" n="e" s="T148">igəbeʔ." </ts>
               <ts e="T150" id="Seg_1463" n="e" s="T149">Esseŋdə </ts>
               <ts e="T151" id="Seg_1465" n="e" s="T150">šü </ts>
               <ts e="T152" id="Seg_1467" n="e" s="T151">embi, </ts>
               <ts e="T153" id="Seg_1469" n="e" s="T152">kambi, </ts>
               <ts e="T154" id="Seg_1471" n="e" s="T153">bulan </ts>
               <ts e="T155" id="Seg_1473" n="e" s="T154">tʼippi. </ts>
               <ts e="T156" id="Seg_1475" n="e" s="T155">Teppi </ts>
               <ts e="T157" id="Seg_1477" n="e" s="T156">esseŋdə, </ts>
               <ts e="T158" id="Seg_1479" n="e" s="T157">bulanən </ts>
               <ts e="T159" id="Seg_1481" n="e" s="T158">kubatsʼəʔ </ts>
               <ts e="T160" id="Seg_1483" n="e" s="T159">esseŋdə </ts>
               <ts e="T161" id="Seg_1485" n="e" s="T160">maʔ </ts>
               <ts e="T162" id="Seg_1487" n="e" s="T161">abi. </ts>
               <ts e="T163" id="Seg_1489" n="e" s="T162">"Amnogaʔ, </ts>
               <ts e="T164" id="Seg_1491" n="e" s="T163">esseŋ! </ts>
               <ts e="T165" id="Seg_1493" n="e" s="T164">Tʼili </ts>
               <ts e="T166" id="Seg_1495" n="e" s="T165">ibinʼə, </ts>
               <ts e="T167" id="Seg_1497" n="e" s="T166">parbinʼə </ts>
               <ts e="T168" id="Seg_1499" n="e" s="T167">iləm </ts>
               <ts e="T169" id="Seg_1501" n="e" s="T168">šiʔnʼileʔ." </ts>
               <ts e="T170" id="Seg_1503" n="e" s="T169">Dărə </ts>
               <ts e="T171" id="Seg_1505" n="e" s="T170">mămbi. </ts>
               <ts e="T172" id="Seg_1507" n="e" s="T171">Bostu </ts>
               <ts e="T173" id="Seg_1509" n="e" s="T172">mĭllüʔbi </ts>
               <ts e="T174" id="Seg_1511" n="e" s="T173">i </ts>
               <ts e="T175" id="Seg_1513" n="e" s="T174">nagurgit </ts>
               <ts e="T176" id="Seg_1515" n="e" s="T175">tʼalan </ts>
               <ts e="T177" id="Seg_1517" n="e" s="T176">dĭ </ts>
               <ts e="T178" id="Seg_1519" n="e" s="T177">šide </ts>
               <ts e="T179" id="Seg_1521" n="e" s="T178">aləbən </ts>
               <ts e="T180" id="Seg_1523" n="e" s="T179">boš </ts>
               <ts e="T181" id="Seg_1525" n="e" s="T180">inezeŋdən </ts>
               <ts e="T182" id="Seg_1527" n="e" s="T181">parlaːndəgaiʔ. </ts>
               <ts e="T183" id="Seg_1529" n="e" s="T182">Konzanzaŋdən, </ts>
               <ts e="T184" id="Seg_1531" n="e" s="T183">pakzənzaŋdən </ts>
               <ts e="T185" id="Seg_1533" n="e" s="T184">inen </ts>
               <ts e="T186" id="Seg_1535" n="e" s="T185">nanəndə </ts>
               <ts e="T187" id="Seg_1537" n="e" s="T186">lambərlaʔ </ts>
               <ts e="T188" id="Seg_1539" n="e" s="T187">kandəgajəʔ. </ts>
               <ts e="T189" id="Seg_1541" n="e" s="T188">Dĭzen </ts>
               <ts e="T190" id="Seg_1543" n="e" s="T189">piʔnettə </ts>
               <ts e="T191" id="Seg_1545" n="e" s="T190">sĭri </ts>
               <ts e="T192" id="Seg_1547" n="e" s="T191">inezəbi </ts>
               <ts e="T193" id="Seg_1549" n="e" s="T192">parlamna. </ts>
               <ts e="T194" id="Seg_1551" n="e" s="T193">Šobi </ts>
               <ts e="T195" id="Seg_1553" n="e" s="T194">esseŋgəndən, </ts>
               <ts e="T196" id="Seg_1555" n="e" s="T195">ibi </ts>
               <ts e="T197" id="Seg_1557" n="e" s="T196">essem, </ts>
               <ts e="T198" id="Seg_1559" n="e" s="T197">maʔgəndə </ts>
               <ts e="T199" id="Seg_1561" n="e" s="T198">kunnaʔ </ts>
               <ts e="T200" id="Seg_1563" n="e" s="T199">kambi. </ts>
               <ts e="T201" id="Seg_1565" n="e" s="T200">Mălia: </ts>
               <ts e="T202" id="Seg_1567" n="e" s="T201">"Măn </ts>
               <ts e="T203" id="Seg_1569" n="e" s="T202">amnollaʔbo </ts>
               <ts e="T204" id="Seg_1571" n="e" s="T203">aľi </ts>
               <ts e="T205" id="Seg_1573" n="e" s="T204">maʔgənʼilaʔ </ts>
               <ts e="T206" id="Seg_1575" n="e" s="T205">kalləlaʔbo?" </ts>
               <ts e="T207" id="Seg_1577" n="e" s="T206">Oʔbdə </ts>
               <ts e="T208" id="Seg_1579" n="e" s="T207">mălia: </ts>
               <ts e="T209" id="Seg_1581" n="e" s="T208">"Măn </ts>
               <ts e="T210" id="Seg_1583" n="e" s="T209">kallam." </ts>
               <ts e="T211" id="Seg_1585" n="e" s="T210">Oʔbdə </ts>
               <ts e="T212" id="Seg_1587" n="e" s="T211">dĭgən </ts>
               <ts e="T213" id="Seg_1589" n="e" s="T212">maʔbi. </ts>
               <ts e="T214" id="Seg_1591" n="e" s="T213">Aləbəj </ts>
               <ts e="T215" id="Seg_1593" n="e" s="T214">ine </ts>
               <ts e="T216" id="Seg_1595" n="e" s="T215">konzandəlaʔ </ts>
               <ts e="T217" id="Seg_1597" n="e" s="T216">mĭbi. </ts>
               <ts e="T218" id="Seg_1599" n="e" s="T217">Mĭllüʔbi, </ts>
               <ts e="T219" id="Seg_1601" n="e" s="T218">maʔgəndə </ts>
               <ts e="T220" id="Seg_1603" n="e" s="T219">šobi. </ts>
               <ts e="T221" id="Seg_1605" n="e" s="T220">Onʼiʔ </ts>
               <ts e="T222" id="Seg_1607" n="e" s="T221">maʔ </ts>
               <ts e="T223" id="Seg_1609" n="e" s="T222">nuga. </ts>
               <ts e="T224" id="Seg_1611" n="e" s="T223">Jadat </ts>
               <ts e="T225" id="Seg_1613" n="e" s="T224">kak </ts>
               <ts e="T226" id="Seg_1615" n="e" s="T225">tüjə </ts>
               <ts e="T227" id="Seg_1617" n="e" s="T226">naga. </ts>
               <ts e="T228" id="Seg_1619" n="e" s="T227">Dĭ </ts>
               <ts e="T229" id="Seg_1621" n="e" s="T228">maʔdə </ts>
               <ts e="T230" id="Seg_1623" n="e" s="T229">šübi. </ts>
               <ts e="T231" id="Seg_1625" n="e" s="T230">Surariat: </ts>
               <ts e="T232" id="Seg_1627" n="e" s="T231">"Tʼaktə </ts>
               <ts e="T233" id="Seg_1629" n="e" s="T232">nüke, </ts>
               <ts e="T234" id="Seg_1631" n="e" s="T233">tʼaktə </ts>
               <ts e="T235" id="Seg_1633" n="e" s="T234">büzʼe, </ts>
               <ts e="T236" id="Seg_1635" n="e" s="T235">jadalaʔ </ts>
               <ts e="T237" id="Seg_1637" n="e" s="T236">giraːmbi?" </ts>
               <ts e="T238" id="Seg_1639" n="e" s="T237">Dĭzeŋ </ts>
               <ts e="T239" id="Seg_1641" n="e" s="T238">nörbəlieiʔ: </ts>
               <ts e="T240" id="Seg_1643" n="e" s="T239">"Šide </ts>
               <ts e="T241" id="Seg_1645" n="e" s="T240">nʼibeʔ </ts>
               <ts e="T242" id="Seg_1647" n="e" s="T241">ibi. </ts>
               <ts e="T243" id="Seg_1649" n="e" s="T242">Il </ts>
               <ts e="T244" id="Seg_1651" n="e" s="T243">sonə </ts>
               <ts e="T245" id="Seg_1653" n="e" s="T244">sarbize </ts>
               <ts e="T246" id="Seg_1655" n="e" s="T245">öʔleʔ </ts>
               <ts e="T247" id="Seg_1657" n="e" s="T246">mĭbi </ts>
               <ts e="T248" id="Seg_1659" n="e" s="T247">bünə. </ts>
               <ts e="T249" id="Seg_1661" n="e" s="T248">Dĭzen </ts>
               <ts e="T250" id="Seg_1663" n="e" s="T249">piʔneendə </ts>
               <ts e="T251" id="Seg_1665" n="e" s="T250">koʔbdobaʔ </ts>
               <ts e="T252" id="Seg_1667" n="e" s="T251">ibi. </ts>
               <ts e="T253" id="Seg_1669" n="e" s="T252">Dĭ </ts>
               <ts e="T254" id="Seg_1671" n="e" s="T253">iləm </ts>
               <ts e="T255" id="Seg_1673" n="e" s="T254">bar </ts>
               <ts e="T256" id="Seg_1675" n="e" s="T255">kutlaʔ </ts>
               <ts e="T257" id="Seg_1677" n="e" s="T256">kumbi." </ts>
               <ts e="T258" id="Seg_1679" n="e" s="T257">"A </ts>
               <ts e="T259" id="Seg_1681" n="e" s="T258">gijen </ts>
               <ts e="T260" id="Seg_1683" n="e" s="T259">koʔbdolaʔ </ts>
               <ts e="T261" id="Seg_1685" n="e" s="T260">giraːmbi?" </ts>
               <ts e="T262" id="Seg_1687" n="e" s="T261">"Bospaʔ </ts>
               <ts e="T263" id="Seg_1689" n="e" s="T262">ej </ts>
               <ts e="T264" id="Seg_1691" n="e" s="T263">tĭmnebeʔ." </ts>
               <ts e="T265" id="Seg_1693" n="e" s="T264">Dĭ </ts>
               <ts e="T266" id="Seg_1695" n="e" s="T265">nʼi </ts>
               <ts e="T267" id="Seg_1697" n="e" s="T266">inebə </ts>
               <ts e="T268" id="Seg_1699" n="e" s="T267">šĭbi, </ts>
               <ts e="T269" id="Seg_1701" n="e" s="T268">pʼeleʔ </ts>
               <ts e="T270" id="Seg_1703" n="e" s="T269">kallaʔ </ts>
               <ts e="T271" id="Seg_1705" n="e" s="T270">tʼürbi. </ts>
               <ts e="T272" id="Seg_1707" n="e" s="T271">Kambi </ts>
               <ts e="T273" id="Seg_1709" n="e" s="T272">urgo </ts>
               <ts e="T274" id="Seg_1711" n="e" s="T273">karaʔdə. </ts>
               <ts e="T275" id="Seg_1713" n="e" s="T274">Dĭgən </ts>
               <ts e="T276" id="Seg_1715" n="e" s="T275">tunoldəlaʔ </ts>
               <ts e="T277" id="Seg_1717" n="e" s="T276">mĭlleʔbi. </ts>
               <ts e="T278" id="Seg_1719" n="e" s="T277">Dĭ </ts>
               <ts e="T279" id="Seg_1721" n="e" s="T278">nʼim </ts>
               <ts e="T280" id="Seg_1723" n="e" s="T279">kubiza </ts>
               <ts e="T281" id="Seg_1725" n="e" s="T280">tunoldəlaʔ </ts>
               <ts e="T282" id="Seg_1727" n="e" s="T281">šobi. </ts>
               <ts e="T283" id="Seg_1729" n="e" s="T282">Süʔməleʔ </ts>
               <ts e="T284" id="Seg_1731" n="e" s="T283">naŋbi </ts>
               <ts e="T285" id="Seg_1733" n="e" s="T284">dĭ </ts>
               <ts e="T286" id="Seg_1735" n="e" s="T285">nʼinə. </ts>
               <ts e="T287" id="Seg_1737" n="e" s="T286">Dĭgəttə </ts>
               <ts e="T288" id="Seg_1739" n="e" s="T287">tʼabərobiiʔ. </ts>
               <ts e="T289" id="Seg_1741" n="e" s="T288">Kagat </ts>
               <ts e="T290" id="Seg_1743" n="e" s="T289">üštəlüʔbi </ts>
               <ts e="T291" id="Seg_1745" n="e" s="T290">dĭm. </ts>
               <ts e="T292" id="Seg_1747" n="e" s="T291">Tʼabolaʔbə, </ts>
               <ts e="T293" id="Seg_1749" n="e" s="T292">nörbəleʔbə: </ts>
               <ts e="T294" id="Seg_1751" n="e" s="T293">"Măn </ts>
               <ts e="T295" id="Seg_1753" n="e" s="T294">tăn </ts>
               <ts e="T296" id="Seg_1755" n="e" s="T295">kagal </ts>
               <ts e="T297" id="Seg_1757" n="e" s="T296">igem. </ts>
               <ts e="T298" id="Seg_1759" n="e" s="T297">Ej </ts>
               <ts e="T299" id="Seg_1761" n="e" s="T298">tʼabəroʔ!" </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_1762" s="T0">AA_1914_Brothers_flk.001 (001.001)</ta>
            <ta e="T6" id="Seg_1763" s="T3">AA_1914_Brothers_flk.002 (001.002)</ta>
            <ta e="T13" id="Seg_1764" s="T6">AA_1914_Brothers_flk.003 (001.003)</ta>
            <ta e="T17" id="Seg_1765" s="T13">AA_1914_Brothers_flk.004 (001.004)</ta>
            <ta e="T20" id="Seg_1766" s="T17">AA_1914_Brothers_flk.005 (001.005)</ta>
            <ta e="T25" id="Seg_1767" s="T20">AA_1914_Brothers_flk.006 (001.006)</ta>
            <ta e="T27" id="Seg_1768" s="T25">AA_1914_Brothers_flk.007 (001.007)</ta>
            <ta e="T38" id="Seg_1769" s="T27">AA_1914_Brothers_flk.008 (001.008)</ta>
            <ta e="T43" id="Seg_1770" s="T38">AA_1914_Brothers_flk.009 (001.010)</ta>
            <ta e="T47" id="Seg_1771" s="T43">AA_1914_Brothers_flk.010 (001.011)</ta>
            <ta e="T50" id="Seg_1772" s="T47">AA_1914_Brothers_flk.011 (001.012)</ta>
            <ta e="T54" id="Seg_1773" s="T50">AA_1914_Brothers_flk.012 (001.013)</ta>
            <ta e="T59" id="Seg_1774" s="T54">AA_1914_Brothers_flk.013 (001.014)</ta>
            <ta e="T64" id="Seg_1775" s="T59">AA_1914_Brothers_flk.014 (001.015)</ta>
            <ta e="T67" id="Seg_1776" s="T64">AA_1914_Brothers_flk.015 (001.016)</ta>
            <ta e="T68" id="Seg_1777" s="T67">AA_1914_Brothers_flk.016 (001.017)</ta>
            <ta e="T69" id="Seg_1778" s="T68">AA_1914_Brothers_flk.017 (002.001)</ta>
            <ta e="T73" id="Seg_1779" s="T69">AA_1914_Brothers_flk.018 (002.002)</ta>
            <ta e="T77" id="Seg_1780" s="T73">AA_1914_Brothers_flk.019 (002.003)</ta>
            <ta e="T82" id="Seg_1781" s="T77">AA_1914_Brothers_flk.020 (002.004)</ta>
            <ta e="T84" id="Seg_1782" s="T82">AA_1914_Brothers_flk.021 (002.005)</ta>
            <ta e="T88" id="Seg_1783" s="T84">AA_1914_Brothers_flk.022 (002.006)</ta>
            <ta e="T91" id="Seg_1784" s="T88">AA_1914_Brothers_flk.023 (002.007)</ta>
            <ta e="T97" id="Seg_1785" s="T91">AA_1914_Brothers_flk.024 (002.008)</ta>
            <ta e="T102" id="Seg_1786" s="T97">AA_1914_Brothers_flk.025 (002.009)</ta>
            <ta e="T104" id="Seg_1787" s="T102">AA_1914_Brothers_flk.026 (002.010)</ta>
            <ta e="T109" id="Seg_1788" s="T104">AA_1914_Brothers_flk.027 (002.011)</ta>
            <ta e="T112" id="Seg_1789" s="T109">AA_1914_Brothers_flk.028 (002.012)</ta>
            <ta e="T115" id="Seg_1790" s="T112">AA_1914_Brothers_flk.029 (002.013)</ta>
            <ta e="T118" id="Seg_1791" s="T115">AA_1914_Brothers_flk.030 (002.014)</ta>
            <ta e="T128" id="Seg_1792" s="T118">AA_1914_Brothers_flk.031 (002.015)</ta>
            <ta e="T133" id="Seg_1793" s="T128">AA_1914_Brothers_flk.032 (002.017)</ta>
            <ta e="T137" id="Seg_1794" s="T133">AA_1914_Brothers_flk.033 (002.018)</ta>
            <ta e="T139" id="Seg_1795" s="T137">AA_1914_Brothers_flk.034 (002.019)</ta>
            <ta e="T142" id="Seg_1796" s="T139">AA_1914_Brothers_flk.035 (002.020)</ta>
            <ta e="T145" id="Seg_1797" s="T142">AA_1914_Brothers_flk.036 (002.021)</ta>
            <ta e="T149" id="Seg_1798" s="T145">AA_1914_Brothers_flk.037 (002.022)</ta>
            <ta e="T155" id="Seg_1799" s="T149">AA_1914_Brothers_flk.038 (002.023)</ta>
            <ta e="T162" id="Seg_1800" s="T155">AA_1914_Brothers_flk.039 (002.024)</ta>
            <ta e="T164" id="Seg_1801" s="T162">AA_1914_Brothers_flk.040 (002.025)</ta>
            <ta e="T169" id="Seg_1802" s="T164">AA_1914_Brothers_flk.041 (002.026)</ta>
            <ta e="T171" id="Seg_1803" s="T169">AA_1914_Brothers_flk.042 (002.027)</ta>
            <ta e="T182" id="Seg_1804" s="T171">AA_1914_Brothers_flk.043 (002.028)</ta>
            <ta e="T188" id="Seg_1805" s="T182">AA_1914_Brothers_flk.044 (002.029)</ta>
            <ta e="T193" id="Seg_1806" s="T188">AA_1914_Brothers_flk.045 (002.030)</ta>
            <ta e="T200" id="Seg_1807" s="T193">AA_1914_Brothers_flk.046 (002.031)</ta>
            <ta e="T206" id="Seg_1808" s="T200">AA_1914_Brothers_flk.047 (002.032)</ta>
            <ta e="T210" id="Seg_1809" s="T206">AA_1914_Brothers_flk.048 (002.034)</ta>
            <ta e="T213" id="Seg_1810" s="T210">AA_1914_Brothers_flk.049 (002.036)</ta>
            <ta e="T217" id="Seg_1811" s="T213">AA_1914_Brothers_flk.050 (002.037)</ta>
            <ta e="T220" id="Seg_1812" s="T217">AA_1914_Brothers_flk.051 (003.001)</ta>
            <ta e="T223" id="Seg_1813" s="T220">AA_1914_Brothers_flk.052 (003.002)</ta>
            <ta e="T227" id="Seg_1814" s="T223">AA_1914_Brothers_flk.053 (003.003)</ta>
            <ta e="T230" id="Seg_1815" s="T227">AA_1914_Brothers_flk.054 (003.004)</ta>
            <ta e="T237" id="Seg_1816" s="T230">AA_1914_Brothers_flk.055 (003.005)</ta>
            <ta e="T242" id="Seg_1817" s="T237">AA_1914_Brothers_flk.056 (003.007)</ta>
            <ta e="T248" id="Seg_1818" s="T242">AA_1914_Brothers_flk.057 (003.009)</ta>
            <ta e="T252" id="Seg_1819" s="T248">AA_1914_Brothers_flk.058 (003.010)</ta>
            <ta e="T257" id="Seg_1820" s="T252">AA_1914_Brothers_flk.059 (003.011)</ta>
            <ta e="T261" id="Seg_1821" s="T257">AA_1914_Brothers_flk.060 (003.012)</ta>
            <ta e="T264" id="Seg_1822" s="T261">AA_1914_Brothers_flk.061 (003.013)</ta>
            <ta e="T271" id="Seg_1823" s="T264">AA_1914_Brothers_flk.062 (003.014)</ta>
            <ta e="T274" id="Seg_1824" s="T271">AA_1914_Brothers_flk.063 (004.001)</ta>
            <ta e="T277" id="Seg_1825" s="T274">AA_1914_Brothers_flk.064 (004.002)</ta>
            <ta e="T282" id="Seg_1826" s="T277">AA_1914_Brothers_flk.065 (004.003)</ta>
            <ta e="T286" id="Seg_1827" s="T282">AA_1914_Brothers_flk.066 (004.004)</ta>
            <ta e="T288" id="Seg_1828" s="T286">AA_1914_Brothers_flk.067 (004.005)</ta>
            <ta e="T291" id="Seg_1829" s="T288">AA_1914_Brothers_flk.068 (004.006)</ta>
            <ta e="T297" id="Seg_1830" s="T291">AA_1914_Brothers_flk.069 (004.007))</ta>
            <ta e="T299" id="Seg_1831" s="T297">AA_1914_Brothers_flk.070 (004.009)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_1832" s="T0">Nüke büzʼezʼəʔ amnobi. </ta>
            <ta e="T6" id="Seg_1833" s="T3">Šide nʼidən ibiiʔ. </ta>
            <ta e="T13" id="Seg_1834" s="T6">Dĭ nʼizeŋdən sʼarbindən essen udabə saj nʼeʔlədən. </ta>
            <ta e="T17" id="Seg_1835" s="T13">So abiiʔ, sonə sarbiiʔ. </ta>
            <ta e="T20" id="Seg_1836" s="T17">Üʔleʔ mĭbiiʔ bünə. </ta>
            <ta e="T25" id="Seg_1837" s="T20">Bü mʼaŋlaʔ šonnamnaiʔ nunan jilgəndə. </ta>
            <ta e="T27" id="Seg_1838" s="T25">Bü păʔlaʔbə. </ta>
            <ta e="T38" id="Seg_1839" s="T27">Dĭzeŋ kegərerbiiʔ: "Kudajən ijat, ijabaʔ, kudajən ijat, săbəjʔtə miʔnʼibeʔ, kudajən ijat!" </ta>
            <ta e="T43" id="Seg_1840" s="T38">Solaj eʔbdəbə ködərbi, öʔleʔ mĭbi. </ta>
            <ta e="T47" id="Seg_1841" s="T43">Esseŋdə ej tulaʔ eʔbdət. </ta>
            <ta e="T50" id="Seg_1842" s="T47">Mana pʼeldə ködərbi. </ta>
            <ta e="T54" id="Seg_1843" s="T50">Mana eʔbdət tubi esseŋdə. </ta>
            <ta e="T59" id="Seg_1844" s="T54">Esseŋ oʔbdəbi, tʼabəlaʔ supsobiiʔ dĭgəʔ. </ta>
            <ta e="T64" id="Seg_1845" s="T59">Nunan nĭgəndə uʔbdəlbi, öʔleʔ mĭbi. </ta>
            <ta e="T67" id="Seg_1846" s="T64">"Kaŋgaʔ aʔtʼə dăra!" </ta>
            <ta e="T68" id="Seg_1847" s="T67">Mĭlloʔbdəbiiʔ. </ta>
            <ta e="T69" id="Seg_1848" s="T68">Kandəbiiʔ. </ta>
            <ta e="T73" id="Seg_1849" s="T69">Kandəbiiʔ, urgu aʔtʼə toʔbdobi. </ta>
            <ta e="T77" id="Seg_1850" s="T73">Dĭzeŋ aʔtʼən toʔgəndə amnaiʔ. </ta>
            <ta e="T82" id="Seg_1851" s="T77">Kubindən bozeraʔ inezəbi aləp šonnamna. </ta>
            <ta e="T84" id="Seg_1852" s="T82">Dĭzeŋ šündəbiiʔ. </ta>
            <ta e="T88" id="Seg_1853" s="T84">Inet sinəgəndə šolaʔ üzəbi. </ta>
            <ta e="T91" id="Seg_1854" s="T88">"Penzüt enəidəne šündəlie." </ta>
            <ta e="T97" id="Seg_1855" s="T91">Bostə ei dĭ kambi, mĭllüʔbi menəj. </ta>
            <ta e="T102" id="Seg_1856" s="T97">Bazoʔ sagər inezəbi aləp šonnamna. </ta>
            <ta e="T104" id="Seg_1857" s="T102">Bazoʔ šündlüʔbiiʔ. </ta>
            <ta e="T109" id="Seg_1858" s="T104">Inet bazoʔ sinigəndə šolaʔ üzəbi. </ta>
            <ta e="T112" id="Seg_1859" s="T109">Bazoʔ "penzüt" mălia. </ta>
            <ta e="T115" id="Seg_1860" s="T112">"Enəidəne bostu šündəlie." </ta>
            <ta e="T118" id="Seg_1861" s="T115">Bostə bazoʔ mellüʔbi. </ta>
            <ta e="T128" id="Seg_1862" s="T118">Dĭ esseŋ kergierbiiʔ, kergierieiʔ: "Kanda aʔtʼəlaʔ moguj, šona aʔtʼəlaʔ nagoguj!" </ta>
            <ta e="T133" id="Seg_1863" s="T128">Bazoʔ sĭri inezəbi aləp šonnamna. </ta>
            <ta e="T137" id="Seg_1864" s="T133">Dĭzeŋ esseŋ bazoʔ šündəbiiʔ. </ta>
            <ta e="T139" id="Seg_1865" s="T137">Inet nulaːmbi. </ta>
            <ta e="T142" id="Seg_1866" s="T139">Dĭgəttə kallaʔ kubi. </ta>
            <ta e="T145" id="Seg_1867" s="T142">"Kajət esseŋ igəleʔ?" </ta>
            <ta e="T149" id="Seg_1868" s="T145">"Dĭrgit dĭrgit esseŋ igəbeʔ." </ta>
            <ta e="T155" id="Seg_1869" s="T149">Esseŋdə šü embi, kambi, bulan tʼippi. </ta>
            <ta e="T162" id="Seg_1870" s="T155">Teppi esseŋdə, bulanən kubatsʼəʔ esseŋdə maʔ abi. </ta>
            <ta e="T164" id="Seg_1871" s="T162">"Amnogaʔ, esseŋ! </ta>
            <ta e="T169" id="Seg_1872" s="T164">Tʼili ibinʼə, parbinʼə iləm šiʔnʼileʔ." </ta>
            <ta e="T171" id="Seg_1873" s="T169">Dărə mămbi. </ta>
            <ta e="T182" id="Seg_1874" s="T171">Bostu mĭllüʔbi i nagurgit tʼalan dĭ šide aləbən boš inezeŋdən parlaːndəgaiʔ. </ta>
            <ta e="T188" id="Seg_1875" s="T182">Konzanzaŋdən, pakzənzaŋdən inen nanəndə lambərlaʔ kandəgajəʔ. </ta>
            <ta e="T193" id="Seg_1876" s="T188">Dĭzen piʔnettə sĭri inezəbi parlamna. </ta>
            <ta e="T200" id="Seg_1877" s="T193">Šobi esseŋgəndən, ibi essem, maʔgəndə kunnaʔ kambi. </ta>
            <ta e="T206" id="Seg_1878" s="T200">Mălia: "Măn amnollaʔbo aľi maʔgənʼilaʔ kalləlaʔbo?" </ta>
            <ta e="T210" id="Seg_1879" s="T206">Oʔbdə mălia: "Măn kallam." </ta>
            <ta e="T213" id="Seg_1880" s="T210">Oʔbdə dĭgən maʔbi. </ta>
            <ta e="T217" id="Seg_1881" s="T213">Aləbəj ine konzandəlaʔ mĭbi. </ta>
            <ta e="T220" id="Seg_1882" s="T217">Mĭllüʔbi, maʔgəndə šobi. </ta>
            <ta e="T223" id="Seg_1883" s="T220">Onʼiʔ maʔ nuga. </ta>
            <ta e="T227" id="Seg_1884" s="T223">Jadat kak tüjə naga. </ta>
            <ta e="T230" id="Seg_1885" s="T227">Dĭ maʔdə šübi. </ta>
            <ta e="T237" id="Seg_1886" s="T230">Surariat: "Tʼaktə nüke, tʼaktə büzʼe, jadalaʔ giraːmbi?" </ta>
            <ta e="T242" id="Seg_1887" s="T237">Dĭzeŋ nörbəlieiʔ: "Šide nʼibeʔ ibi. </ta>
            <ta e="T248" id="Seg_1888" s="T242">Il sonə sarbize öʔleʔ mĭbi bünə. </ta>
            <ta e="T252" id="Seg_1889" s="T248">Dĭzen piʔneendə koʔbdobaʔ ibi. </ta>
            <ta e="T257" id="Seg_1890" s="T252">Dĭ iləm bar kutlaʔ kumbi." </ta>
            <ta e="T261" id="Seg_1891" s="T257">"A gijen koʔbdolaʔ giraːmbi?" </ta>
            <ta e="T264" id="Seg_1892" s="T261">"Bospaʔ ej tĭmnebeʔ." </ta>
            <ta e="T271" id="Seg_1893" s="T264">Dĭ nʼi inebə šĭbi, pʼeleʔ kallaʔ tʼürbi. </ta>
            <ta e="T274" id="Seg_1894" s="T271">Kambi urgo karaʔdə. </ta>
            <ta e="T277" id="Seg_1895" s="T274">Dĭgən tunoldəlaʔ mĭlleʔbi. </ta>
            <ta e="T282" id="Seg_1896" s="T277">Dĭ nʼim kubiza tunoldəlaʔ šobi. </ta>
            <ta e="T286" id="Seg_1897" s="T282">Süʔməleʔ naŋbi dĭ nʼinə. </ta>
            <ta e="T288" id="Seg_1898" s="T286">Dĭgəttə tʼabərobiiʔ. </ta>
            <ta e="T291" id="Seg_1899" s="T288">Kagat üštəlüʔbi dĭm. </ta>
            <ta e="T297" id="Seg_1900" s="T291">Tʼabolaʔbə, nörbəleʔbə: "Măn tăn kagal igem. </ta>
            <ta e="T299" id="Seg_1901" s="T297">Ej tʼabəroʔ!" </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1902" s="T0">nüke</ta>
            <ta e="T2" id="Seg_1903" s="T1">büzʼe-zʼəʔ</ta>
            <ta e="T3" id="Seg_1904" s="T2">amno-bi</ta>
            <ta e="T4" id="Seg_1905" s="T3">šide</ta>
            <ta e="T5" id="Seg_1906" s="T4">nʼi-dən</ta>
            <ta e="T6" id="Seg_1907" s="T5">i-bi-iʔ</ta>
            <ta e="T7" id="Seg_1908" s="T6">dĭ</ta>
            <ta e="T8" id="Seg_1909" s="T7">nʼi-zeŋ-dən</ta>
            <ta e="T9" id="Seg_1910" s="T8">sʼar-bi-ndən</ta>
            <ta e="T10" id="Seg_1911" s="T9">es-sen</ta>
            <ta e="T11" id="Seg_1912" s="T10">uda-bə</ta>
            <ta e="T12" id="Seg_1913" s="T11">saj</ta>
            <ta e="T13" id="Seg_1914" s="T12">nʼeʔ-lə-dən</ta>
            <ta e="T14" id="Seg_1915" s="T13">so</ta>
            <ta e="T15" id="Seg_1916" s="T14">a-bi-iʔ</ta>
            <ta e="T16" id="Seg_1917" s="T15">so-nə</ta>
            <ta e="T17" id="Seg_1918" s="T16">sar-bi-iʔ</ta>
            <ta e="T18" id="Seg_1919" s="T17">üʔ-leʔ</ta>
            <ta e="T19" id="Seg_1920" s="T18">mĭ-bi-iʔ</ta>
            <ta e="T20" id="Seg_1921" s="T19">bü-nə</ta>
            <ta e="T21" id="Seg_1922" s="T20">bü</ta>
            <ta e="T22" id="Seg_1923" s="T21">mʼaŋ-laʔ</ta>
            <ta e="T23" id="Seg_1924" s="T22">šon-namna-iʔ</ta>
            <ta e="T24" id="Seg_1925" s="T23">nuna-n</ta>
            <ta e="T25" id="Seg_1926" s="T24">jil-gəndə</ta>
            <ta e="T26" id="Seg_1927" s="T25">bü</ta>
            <ta e="T27" id="Seg_1928" s="T26">păʔ-laʔbə</ta>
            <ta e="T28" id="Seg_1929" s="T27">dĭ-zeŋ</ta>
            <ta e="T29" id="Seg_1930" s="T28">kegərer-bi-iʔ</ta>
            <ta e="T30" id="Seg_1931" s="T29">kudaj-ə-n</ta>
            <ta e="T31" id="Seg_1932" s="T30">ija-t</ta>
            <ta e="T32" id="Seg_1933" s="T31">ija-bAʔ</ta>
            <ta e="T33" id="Seg_1934" s="T32">kudaj-ə-n</ta>
            <ta e="T34" id="Seg_1935" s="T33">ija-t</ta>
            <ta e="T35" id="Seg_1936" s="T34">săbəjʔ-tə</ta>
            <ta e="T36" id="Seg_1937" s="T35">miʔnʼibeʔ</ta>
            <ta e="T37" id="Seg_1938" s="T36">kudaj-ə-n</ta>
            <ta e="T38" id="Seg_1939" s="T37">ija-t</ta>
            <ta e="T39" id="Seg_1940" s="T38">solaj</ta>
            <ta e="T40" id="Seg_1941" s="T39">eʔbdə-bə</ta>
            <ta e="T41" id="Seg_1942" s="T40">ködər-bi</ta>
            <ta e="T42" id="Seg_1943" s="T41">öʔ-leʔ</ta>
            <ta e="T43" id="Seg_1944" s="T42">mĭ-bi</ta>
            <ta e="T44" id="Seg_1945" s="T43">es-seŋ-də</ta>
            <ta e="T45" id="Seg_1946" s="T44">ej</ta>
            <ta e="T46" id="Seg_1947" s="T45">tu-laʔ</ta>
            <ta e="T47" id="Seg_1948" s="T46">eʔbdə-t</ta>
            <ta e="T48" id="Seg_1949" s="T47">mana</ta>
            <ta e="T49" id="Seg_1950" s="T48">pʼel-də</ta>
            <ta e="T50" id="Seg_1951" s="T49">ködər-bi</ta>
            <ta e="T51" id="Seg_1952" s="T50">mana</ta>
            <ta e="T52" id="Seg_1953" s="T51">eʔbdə-t</ta>
            <ta e="T53" id="Seg_1954" s="T52">tu-bi</ta>
            <ta e="T54" id="Seg_1955" s="T53">es-seŋ-də</ta>
            <ta e="T55" id="Seg_1956" s="T54">es-seŋ</ta>
            <ta e="T56" id="Seg_1957" s="T55">oʔbdə-bi</ta>
            <ta e="T57" id="Seg_1958" s="T56">tʼabə-laʔ</ta>
            <ta e="T58" id="Seg_1959" s="T57">supso-bi-iʔ</ta>
            <ta e="T59" id="Seg_1960" s="T58">dĭ-gəʔ</ta>
            <ta e="T60" id="Seg_1961" s="T59">nuna-n</ta>
            <ta e="T61" id="Seg_1962" s="T60">nĭ-gəndə</ta>
            <ta e="T62" id="Seg_1963" s="T61">uʔbdə-l-bi</ta>
            <ta e="T63" id="Seg_1964" s="T62">öʔ-leʔ</ta>
            <ta e="T64" id="Seg_1965" s="T63">mĭ-bi</ta>
            <ta e="T65" id="Seg_1966" s="T64">kaŋ-gaʔ</ta>
            <ta e="T66" id="Seg_1967" s="T65">aʔtʼə</ta>
            <ta e="T67" id="Seg_1968" s="T66">dăra</ta>
            <ta e="T68" id="Seg_1969" s="T67">mĭl-loʔbdə-bi-iʔ</ta>
            <ta e="T69" id="Seg_1970" s="T68">kandə-bi-iʔ</ta>
            <ta e="T70" id="Seg_1971" s="T69">kandə-bi-iʔ</ta>
            <ta e="T71" id="Seg_1972" s="T70">urgu</ta>
            <ta e="T72" id="Seg_1973" s="T71">aʔtʼə</ta>
            <ta e="T73" id="Seg_1974" s="T72">toʔbdo-bi</ta>
            <ta e="T74" id="Seg_1975" s="T73">dĭ-zeŋ</ta>
            <ta e="T75" id="Seg_1976" s="T74">aʔtʼə-n</ta>
            <ta e="T76" id="Seg_1977" s="T75">toʔ-gəndə</ta>
            <ta e="T77" id="Seg_1978" s="T76">amna-iʔ</ta>
            <ta e="T78" id="Seg_1979" s="T77">ku-bi-ndən</ta>
            <ta e="T79" id="Seg_1980" s="T78">bozeraʔ</ta>
            <ta e="T80" id="Seg_1981" s="T79">ine-zəbi</ta>
            <ta e="T81" id="Seg_1982" s="T80">aləp</ta>
            <ta e="T82" id="Seg_1983" s="T81">šon-namna</ta>
            <ta e="T83" id="Seg_1984" s="T82">dĭ-zeŋ</ta>
            <ta e="T84" id="Seg_1985" s="T83">šündə-bi-iʔ</ta>
            <ta e="T85" id="Seg_1986" s="T84">ine-t</ta>
            <ta e="T86" id="Seg_1987" s="T85">sinə-gəndə</ta>
            <ta e="T87" id="Seg_1988" s="T86">šo-laʔ</ta>
            <ta e="T88" id="Seg_1989" s="T87">üzə-bi</ta>
            <ta e="T89" id="Seg_1990" s="T88">penzüt</ta>
            <ta e="T90" id="Seg_1991" s="T89">enəidəne</ta>
            <ta e="T91" id="Seg_1992" s="T90">šündə-lie</ta>
            <ta e="T92" id="Seg_1993" s="T91">bos-tə</ta>
            <ta e="T93" id="Seg_1994" s="T92">ej</ta>
            <ta e="T94" id="Seg_1995" s="T93">dĭ</ta>
            <ta e="T95" id="Seg_1996" s="T94">kam-bi</ta>
            <ta e="T96" id="Seg_1997" s="T95">mĭl-lüʔ-bi</ta>
            <ta e="T97" id="Seg_1998" s="T96">menə-j</ta>
            <ta e="T98" id="Seg_1999" s="T97">bazoʔ</ta>
            <ta e="T99" id="Seg_2000" s="T98">sagər</ta>
            <ta e="T100" id="Seg_2001" s="T99">ine-zəbi</ta>
            <ta e="T101" id="Seg_2002" s="T100">aləp</ta>
            <ta e="T102" id="Seg_2003" s="T101">šon-namna</ta>
            <ta e="T103" id="Seg_2004" s="T102">bazoʔ</ta>
            <ta e="T104" id="Seg_2005" s="T103">šünd-lüʔ-bi-iʔ</ta>
            <ta e="T105" id="Seg_2006" s="T104">ine-t</ta>
            <ta e="T106" id="Seg_2007" s="T105">bazoʔ</ta>
            <ta e="T107" id="Seg_2008" s="T106">sini-gəndə</ta>
            <ta e="T108" id="Seg_2009" s="T107">šo-laʔ</ta>
            <ta e="T109" id="Seg_2010" s="T108">üzə-bi</ta>
            <ta e="T110" id="Seg_2011" s="T109">bazoʔ</ta>
            <ta e="T111" id="Seg_2012" s="T110">penzüt</ta>
            <ta e="T112" id="Seg_2013" s="T111">mă-lia</ta>
            <ta e="T113" id="Seg_2014" s="T112">enəidəne</ta>
            <ta e="T114" id="Seg_2015" s="T113">bos-tu</ta>
            <ta e="T115" id="Seg_2016" s="T114">šündə-lie</ta>
            <ta e="T116" id="Seg_2017" s="T115">bos-tə</ta>
            <ta e="T117" id="Seg_2018" s="T116">bazoʔ</ta>
            <ta e="T118" id="Seg_2019" s="T117">mel-lüʔ-bi</ta>
            <ta e="T119" id="Seg_2020" s="T118">dĭ</ta>
            <ta e="T120" id="Seg_2021" s="T119">es-seŋ</ta>
            <ta e="T121" id="Seg_2022" s="T120">kergier-bi-iʔ</ta>
            <ta e="T122" id="Seg_2023" s="T121">kergier-ie-iʔ</ta>
            <ta e="T123" id="Seg_2024" s="T122">kan-da</ta>
            <ta e="T124" id="Seg_2025" s="T123">aʔtʼə-laʔ</ta>
            <ta e="T125" id="Seg_2026" s="T124">mo-gu-j</ta>
            <ta e="T126" id="Seg_2027" s="T125">šo-na</ta>
            <ta e="T127" id="Seg_2028" s="T126">aʔtʼə-laʔ</ta>
            <ta e="T128" id="Seg_2029" s="T127">nago-gu-j</ta>
            <ta e="T129" id="Seg_2030" s="T128">bazoʔ</ta>
            <ta e="T130" id="Seg_2031" s="T129">sĭri</ta>
            <ta e="T131" id="Seg_2032" s="T130">ine-zəbi</ta>
            <ta e="T132" id="Seg_2033" s="T131">aləp</ta>
            <ta e="T133" id="Seg_2034" s="T132">šon-namna</ta>
            <ta e="T134" id="Seg_2035" s="T133">dĭ-zeŋ</ta>
            <ta e="T135" id="Seg_2036" s="T134">es-seŋ</ta>
            <ta e="T136" id="Seg_2037" s="T135">bazoʔ</ta>
            <ta e="T137" id="Seg_2038" s="T136">šündə-bi-iʔ</ta>
            <ta e="T138" id="Seg_2039" s="T137">ine-t</ta>
            <ta e="T139" id="Seg_2040" s="T138">nu-laːm-bi</ta>
            <ta e="T140" id="Seg_2041" s="T139">dĭgəttə</ta>
            <ta e="T141" id="Seg_2042" s="T140">kal-laʔ</ta>
            <ta e="T142" id="Seg_2043" s="T141">ku-bi</ta>
            <ta e="T143" id="Seg_2044" s="T142">Kajət</ta>
            <ta e="T144" id="Seg_2045" s="T143">es-seŋ</ta>
            <ta e="T145" id="Seg_2046" s="T144">i-gə-leʔ</ta>
            <ta e="T146" id="Seg_2047" s="T145">dĭrgit</ta>
            <ta e="T147" id="Seg_2048" s="T146">dĭrgit</ta>
            <ta e="T148" id="Seg_2049" s="T147">es-seŋ</ta>
            <ta e="T149" id="Seg_2050" s="T148">i-gə-beʔ</ta>
            <ta e="T150" id="Seg_2051" s="T149">es-seŋ-də</ta>
            <ta e="T151" id="Seg_2052" s="T150">šü</ta>
            <ta e="T152" id="Seg_2053" s="T151">em-bi</ta>
            <ta e="T153" id="Seg_2054" s="T152">kam-bi</ta>
            <ta e="T154" id="Seg_2055" s="T153">bulan</ta>
            <ta e="T155" id="Seg_2056" s="T154">tʼip-pi</ta>
            <ta e="T156" id="Seg_2057" s="T155">tep-pi</ta>
            <ta e="T157" id="Seg_2058" s="T156">es-seŋ-də</ta>
            <ta e="T158" id="Seg_2059" s="T157">bulan-ə-n</ta>
            <ta e="T159" id="Seg_2060" s="T158">kuba-t-sʼəʔ</ta>
            <ta e="T160" id="Seg_2061" s="T159">es-seŋ-də</ta>
            <ta e="T161" id="Seg_2062" s="T160">maʔ</ta>
            <ta e="T162" id="Seg_2063" s="T161">a-bi</ta>
            <ta e="T163" id="Seg_2064" s="T162">amno-gaʔ</ta>
            <ta e="T164" id="Seg_2065" s="T163">es-seŋ</ta>
            <ta e="T165" id="Seg_2066" s="T164">tʼili</ta>
            <ta e="T166" id="Seg_2067" s="T165">i-bi-nʼə</ta>
            <ta e="T167" id="Seg_2068" s="T166">par-bi-nʼə</ta>
            <ta e="T168" id="Seg_2069" s="T167">i-lə-m</ta>
            <ta e="T169" id="Seg_2070" s="T168">šiʔnʼileʔ</ta>
            <ta e="T170" id="Seg_2071" s="T169">dărə</ta>
            <ta e="T171" id="Seg_2072" s="T170">măm-bi</ta>
            <ta e="T172" id="Seg_2073" s="T171">bos-tu</ta>
            <ta e="T173" id="Seg_2074" s="T172">mĭl-lüʔ-bi</ta>
            <ta e="T174" id="Seg_2075" s="T173">i</ta>
            <ta e="T175" id="Seg_2076" s="T174">nagur-git</ta>
            <ta e="T176" id="Seg_2077" s="T175">tʼala-n</ta>
            <ta e="T177" id="Seg_2078" s="T176">dĭ</ta>
            <ta e="T178" id="Seg_2079" s="T177">šide</ta>
            <ta e="T179" id="Seg_2080" s="T178">aləb-ə-n</ta>
            <ta e="T180" id="Seg_2081" s="T179">boš</ta>
            <ta e="T181" id="Seg_2082" s="T180">ine-zeŋ-dən</ta>
            <ta e="T182" id="Seg_2083" s="T181">par-laːndə-ga-iʔ</ta>
            <ta e="T183" id="Seg_2084" s="T182">konzan-zaŋ-dən</ta>
            <ta e="T184" id="Seg_2085" s="T183">pakzən-zaŋ-dən</ta>
            <ta e="T185" id="Seg_2086" s="T184">ine-n</ta>
            <ta e="T186" id="Seg_2087" s="T185">nanə-ndə</ta>
            <ta e="T187" id="Seg_2088" s="T186">lambər-laʔ</ta>
            <ta e="T188" id="Seg_2089" s="T187">kandə-ga-jəʔ</ta>
            <ta e="T189" id="Seg_2090" s="T188">dĭ-zen</ta>
            <ta e="T190" id="Seg_2091" s="T189">piʔne-ttə</ta>
            <ta e="T191" id="Seg_2092" s="T190">sĭri</ta>
            <ta e="T192" id="Seg_2093" s="T191">ine-zəbi</ta>
            <ta e="T193" id="Seg_2094" s="T192">par-lamna</ta>
            <ta e="T194" id="Seg_2095" s="T193">šo-bi</ta>
            <ta e="T195" id="Seg_2096" s="T194">es-seŋ-gəndən</ta>
            <ta e="T196" id="Seg_2097" s="T195">i-bi</ta>
            <ta e="T197" id="Seg_2098" s="T196">esse-m</ta>
            <ta e="T198" id="Seg_2099" s="T197">maʔ-gəndə</ta>
            <ta e="T199" id="Seg_2100" s="T198">kun-naʔ</ta>
            <ta e="T200" id="Seg_2101" s="T199">kam-bi</ta>
            <ta e="T201" id="Seg_2102" s="T200">mă-lia</ta>
            <ta e="T202" id="Seg_2103" s="T201">măn</ta>
            <ta e="T203" id="Seg_2104" s="T202">amno-l-laʔ=bo</ta>
            <ta e="T204" id="Seg_2105" s="T203">aľi</ta>
            <ta e="T205" id="Seg_2106" s="T204">maʔ-gənʼilAʔ</ta>
            <ta e="T206" id="Seg_2107" s="T205">kal-lə-laʔ=bo</ta>
            <ta e="T207" id="Seg_2108" s="T206">oʔbdə</ta>
            <ta e="T208" id="Seg_2109" s="T207">mă-lia</ta>
            <ta e="T209" id="Seg_2110" s="T208">măn</ta>
            <ta e="T210" id="Seg_2111" s="T209">kal-la-m</ta>
            <ta e="T211" id="Seg_2112" s="T210">oʔbdə</ta>
            <ta e="T212" id="Seg_2113" s="T211">dĭgən</ta>
            <ta e="T213" id="Seg_2114" s="T212">maʔ-bi</ta>
            <ta e="T214" id="Seg_2115" s="T213">aləb-əj</ta>
            <ta e="T215" id="Seg_2116" s="T214">ine</ta>
            <ta e="T216" id="Seg_2117" s="T215">konzan-də-laʔ</ta>
            <ta e="T217" id="Seg_2118" s="T216">mĭ-bi</ta>
            <ta e="T218" id="Seg_2119" s="T217">mĭl-lüʔ-bi</ta>
            <ta e="T219" id="Seg_2120" s="T218">maʔ-gəndə</ta>
            <ta e="T220" id="Seg_2121" s="T219">šo-bi</ta>
            <ta e="T221" id="Seg_2122" s="T220">onʼiʔ</ta>
            <ta e="T222" id="Seg_2123" s="T221">maʔ</ta>
            <ta e="T223" id="Seg_2124" s="T222">nu-ga</ta>
            <ta e="T224" id="Seg_2125" s="T223">jada-t</ta>
            <ta e="T225" id="Seg_2126" s="T224">kak</ta>
            <ta e="T226" id="Seg_2127" s="T225">tüjə</ta>
            <ta e="T227" id="Seg_2128" s="T226">naga</ta>
            <ta e="T228" id="Seg_2129" s="T227">dĭ</ta>
            <ta e="T229" id="Seg_2130" s="T228">maʔ-də</ta>
            <ta e="T230" id="Seg_2131" s="T229">šü-bi</ta>
            <ta e="T231" id="Seg_2132" s="T230">surar-ia-t</ta>
            <ta e="T232" id="Seg_2133" s="T231">tʼaktə</ta>
            <ta e="T233" id="Seg_2134" s="T232">nüke</ta>
            <ta e="T234" id="Seg_2135" s="T233">tʼaktə</ta>
            <ta e="T235" id="Seg_2136" s="T234">büzʼe</ta>
            <ta e="T236" id="Seg_2137" s="T235">jada-laʔ</ta>
            <ta e="T237" id="Seg_2138" s="T236">giraːm-bi</ta>
            <ta e="T238" id="Seg_2139" s="T237">dĭ-zeŋ</ta>
            <ta e="T239" id="Seg_2140" s="T238">nörbə-lie-iʔ</ta>
            <ta e="T240" id="Seg_2141" s="T239">šide</ta>
            <ta e="T241" id="Seg_2142" s="T240">nʼi-beʔ</ta>
            <ta e="T242" id="Seg_2143" s="T241">i-bi</ta>
            <ta e="T243" id="Seg_2144" s="T242">il</ta>
            <ta e="T244" id="Seg_2145" s="T243">so-nə</ta>
            <ta e="T245" id="Seg_2146" s="T244">sar-bize</ta>
            <ta e="T246" id="Seg_2147" s="T245">öʔ-leʔ</ta>
            <ta e="T247" id="Seg_2148" s="T246">mĭ-bi</ta>
            <ta e="T248" id="Seg_2149" s="T247">bü-nə</ta>
            <ta e="T249" id="Seg_2150" s="T248">dĭ-zen</ta>
            <ta e="T250" id="Seg_2151" s="T249">piʔne-endə</ta>
            <ta e="T251" id="Seg_2152" s="T250">koʔbdo-bAʔ</ta>
            <ta e="T252" id="Seg_2153" s="T251">i-bi</ta>
            <ta e="T253" id="Seg_2154" s="T252">dĭ</ta>
            <ta e="T254" id="Seg_2155" s="T253">il-əm</ta>
            <ta e="T255" id="Seg_2156" s="T254">bar</ta>
            <ta e="T256" id="Seg_2157" s="T255">kut-laʔ</ta>
            <ta e="T257" id="Seg_2158" s="T256">kum-bi</ta>
            <ta e="T258" id="Seg_2159" s="T257">a</ta>
            <ta e="T259" id="Seg_2160" s="T258">gijen</ta>
            <ta e="T260" id="Seg_2161" s="T259">koʔbdo-laʔ</ta>
            <ta e="T261" id="Seg_2162" s="T260">giraːm-bi</ta>
            <ta e="T262" id="Seg_2163" s="T261">bos-paʔ</ta>
            <ta e="T263" id="Seg_2164" s="T262">ej</ta>
            <ta e="T264" id="Seg_2165" s="T263">tĭmne-beʔ</ta>
            <ta e="T265" id="Seg_2166" s="T264">dĭ</ta>
            <ta e="T266" id="Seg_2167" s="T265">nʼi</ta>
            <ta e="T267" id="Seg_2168" s="T266">ine-bə</ta>
            <ta e="T268" id="Seg_2169" s="T267">šĭ-bi</ta>
            <ta e="T269" id="Seg_2170" s="T268">pʼe-leʔ</ta>
            <ta e="T270" id="Seg_2171" s="T269">kal-laʔ</ta>
            <ta e="T271" id="Seg_2172" s="T270">tʼür-bi</ta>
            <ta e="T272" id="Seg_2173" s="T271">kam-bi</ta>
            <ta e="T273" id="Seg_2174" s="T272">urgo</ta>
            <ta e="T274" id="Seg_2175" s="T273">karaʔ-də</ta>
            <ta e="T275" id="Seg_2176" s="T274">dĭgən</ta>
            <ta e="T276" id="Seg_2177" s="T275">tuno-l-də-laʔ</ta>
            <ta e="T277" id="Seg_2178" s="T276">mĭl-leʔ-bi</ta>
            <ta e="T278" id="Seg_2179" s="T277">dĭ</ta>
            <ta e="T279" id="Seg_2180" s="T278">nʼi-m</ta>
            <ta e="T280" id="Seg_2181" s="T279">ku-biza</ta>
            <ta e="T281" id="Seg_2182" s="T280">tuno-l-də-laʔ</ta>
            <ta e="T282" id="Seg_2183" s="T281">šo-bi</ta>
            <ta e="T283" id="Seg_2184" s="T282">süʔmə-leʔ</ta>
            <ta e="T284" id="Seg_2185" s="T283">naŋ-bi</ta>
            <ta e="T285" id="Seg_2186" s="T284">dĭ</ta>
            <ta e="T286" id="Seg_2187" s="T285">nʼi-nə</ta>
            <ta e="T287" id="Seg_2188" s="T286">dĭgəttə</ta>
            <ta e="T288" id="Seg_2189" s="T287">tʼabəro-bi-iʔ</ta>
            <ta e="T289" id="Seg_2190" s="T288">kaga-t</ta>
            <ta e="T290" id="Seg_2191" s="T289">üštə-lüʔ-bi</ta>
            <ta e="T291" id="Seg_2192" s="T290">dĭ-m</ta>
            <ta e="T292" id="Seg_2193" s="T291">tʼabo-laʔbə</ta>
            <ta e="T293" id="Seg_2194" s="T292">nörbə-leʔbə</ta>
            <ta e="T294" id="Seg_2195" s="T293">măn</ta>
            <ta e="T295" id="Seg_2196" s="T294">tăn</ta>
            <ta e="T296" id="Seg_2197" s="T295">kaga-l</ta>
            <ta e="T297" id="Seg_2198" s="T296">i-ge-m</ta>
            <ta e="T298" id="Seg_2199" s="T297">ej</ta>
            <ta e="T299" id="Seg_2200" s="T298">tʼabəro-ʔ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_2201" s="T0">nüke</ta>
            <ta e="T2" id="Seg_2202" s="T1">büzʼe-ziʔ</ta>
            <ta e="T3" id="Seg_2203" s="T2">amno-bi</ta>
            <ta e="T4" id="Seg_2204" s="T3">šide</ta>
            <ta e="T5" id="Seg_2205" s="T4">nʼi-dən</ta>
            <ta e="T6" id="Seg_2206" s="T5">i-bi-jəʔ</ta>
            <ta e="T7" id="Seg_2207" s="T6">dĭ</ta>
            <ta e="T8" id="Seg_2208" s="T7">nʼi-zAŋ-dən</ta>
            <ta e="T9" id="Seg_2209" s="T8">sʼar-bi-gəndən</ta>
            <ta e="T10" id="Seg_2210" s="T9">ešši-zen</ta>
            <ta e="T11" id="Seg_2211" s="T10">uda-bə</ta>
            <ta e="T12" id="Seg_2212" s="T11">saj</ta>
            <ta e="T13" id="Seg_2213" s="T12">nʼeʔbdə-lV-dən</ta>
            <ta e="T14" id="Seg_2214" s="T13">so</ta>
            <ta e="T15" id="Seg_2215" s="T14">a-bi-jəʔ</ta>
            <ta e="T16" id="Seg_2216" s="T15">so-Tə</ta>
            <ta e="T17" id="Seg_2217" s="T16">sar-bi-jəʔ</ta>
            <ta e="T18" id="Seg_2218" s="T17">öʔ-lAʔ</ta>
            <ta e="T19" id="Seg_2219" s="T18">mĭ-bi-jəʔ</ta>
            <ta e="T20" id="Seg_2220" s="T19">bü-Tə</ta>
            <ta e="T21" id="Seg_2221" s="T20">bü</ta>
            <ta e="T22" id="Seg_2222" s="T21">mʼaŋ-lAʔ</ta>
            <ta e="T23" id="Seg_2223" s="T22">šonə-lamnə-jəʔ</ta>
            <ta e="T24" id="Seg_2224" s="T23">nuna-n</ta>
            <ta e="T25" id="Seg_2225" s="T24">il-gəndə</ta>
            <ta e="T26" id="Seg_2226" s="T25">bü</ta>
            <ta e="T27" id="Seg_2227" s="T26">păda-laʔbə</ta>
            <ta e="T28" id="Seg_2228" s="T27">dĭ-zAŋ</ta>
            <ta e="T29" id="Seg_2229" s="T28">kegərer-bi-jəʔ</ta>
            <ta e="T30" id="Seg_2230" s="T29">kudaj-ə-n</ta>
            <ta e="T31" id="Seg_2231" s="T30">ija-t</ta>
            <ta e="T32" id="Seg_2232" s="T31">ija-bAʔ</ta>
            <ta e="T33" id="Seg_2233" s="T32">kudaj-ə-n</ta>
            <ta e="T34" id="Seg_2234" s="T33">ija-t</ta>
            <ta e="T35" id="Seg_2235" s="T34">săbəj-t</ta>
            <ta e="T36" id="Seg_2236" s="T35">miʔnʼibeʔ</ta>
            <ta e="T37" id="Seg_2237" s="T36">kudaj-ə-n</ta>
            <ta e="T38" id="Seg_2238" s="T37">ija-t</ta>
            <ta e="T39" id="Seg_2239" s="T38">solaj</ta>
            <ta e="T40" id="Seg_2240" s="T39">eʔbdə-bə</ta>
            <ta e="T41" id="Seg_2241" s="T40">ködər-bi</ta>
            <ta e="T42" id="Seg_2242" s="T41">öʔ-lAʔ</ta>
            <ta e="T43" id="Seg_2243" s="T42">mĭ-bi</ta>
            <ta e="T44" id="Seg_2244" s="T43">ešši-zAŋ-Tə</ta>
            <ta e="T45" id="Seg_2245" s="T44">ej</ta>
            <ta e="T46" id="Seg_2246" s="T45">tu-lAʔ</ta>
            <ta e="T47" id="Seg_2247" s="T46">eʔbdə-t</ta>
            <ta e="T48" id="Seg_2248" s="T47">mana</ta>
            <ta e="T49" id="Seg_2249" s="T48">pʼel-də</ta>
            <ta e="T50" id="Seg_2250" s="T49">ködər-bi</ta>
            <ta e="T51" id="Seg_2251" s="T50">mana</ta>
            <ta e="T52" id="Seg_2252" s="T51">eʔbdə-t</ta>
            <ta e="T53" id="Seg_2253" s="T52">tu-bi</ta>
            <ta e="T54" id="Seg_2254" s="T53">ešši-zAŋ-Tə</ta>
            <ta e="T55" id="Seg_2255" s="T54">ešši-zAŋ</ta>
            <ta e="T56" id="Seg_2256" s="T55">oʔbdə-bi</ta>
            <ta e="T57" id="Seg_2257" s="T56">tʼabə-lAʔ</ta>
            <ta e="T58" id="Seg_2258" s="T57">supso-bi-jəʔ</ta>
            <ta e="T59" id="Seg_2259" s="T58">dĭ-gəʔ</ta>
            <ta e="T60" id="Seg_2260" s="T59">nuna-n</ta>
            <ta e="T61" id="Seg_2261" s="T60">nĭ-gəndə</ta>
            <ta e="T62" id="Seg_2262" s="T61">uʔbdə-lə-bi</ta>
            <ta e="T63" id="Seg_2263" s="T62">öʔ-lAʔ</ta>
            <ta e="T64" id="Seg_2264" s="T63">mĭ-bi</ta>
            <ta e="T65" id="Seg_2265" s="T64">kan-KAʔ</ta>
            <ta e="T66" id="Seg_2266" s="T65">aʔtʼi</ta>
            <ta e="T67" id="Seg_2267" s="T66">dăra</ta>
            <ta e="T68" id="Seg_2268" s="T67">mĭn-loʔbdə-bi-jəʔ</ta>
            <ta e="T69" id="Seg_2269" s="T68">kandə-bi-jəʔ</ta>
            <ta e="T70" id="Seg_2270" s="T69">kandə-bi-jəʔ</ta>
            <ta e="T71" id="Seg_2271" s="T70">urgo</ta>
            <ta e="T72" id="Seg_2272" s="T71">aʔtʼi</ta>
            <ta e="T73" id="Seg_2273" s="T72">toʔbdo-bi</ta>
            <ta e="T74" id="Seg_2274" s="T73">dĭ-zAŋ</ta>
            <ta e="T75" id="Seg_2275" s="T74">aʔtʼi-n</ta>
            <ta e="T76" id="Seg_2276" s="T75">toʔ-gəndə</ta>
            <ta e="T77" id="Seg_2277" s="T76">amnə-jəʔ</ta>
            <ta e="T78" id="Seg_2278" s="T77">ku-bi-gəndən</ta>
            <ta e="T79" id="Seg_2279" s="T78">bozeraʔ</ta>
            <ta e="T80" id="Seg_2280" s="T79">ine-zəbi</ta>
            <ta e="T81" id="Seg_2281" s="T80">aləp</ta>
            <ta e="T82" id="Seg_2282" s="T81">šonə-lamnə</ta>
            <ta e="T83" id="Seg_2283" s="T82">dĭ-zAŋ</ta>
            <ta e="T84" id="Seg_2284" s="T83">šündə-bi-jəʔ</ta>
            <ta e="T85" id="Seg_2285" s="T84">ine-t</ta>
            <ta e="T86" id="Seg_2286" s="T85">sini-gəndə</ta>
            <ta e="T87" id="Seg_2287" s="T86">šo-lAʔ</ta>
            <ta e="T88" id="Seg_2288" s="T87">üzə-bi</ta>
            <ta e="T89" id="Seg_2289" s="T88">penzüt</ta>
            <ta e="T90" id="Seg_2290" s="T89">eneidəne</ta>
            <ta e="T91" id="Seg_2291" s="T90">šündə-liA</ta>
            <ta e="T92" id="Seg_2292" s="T91">bos-də</ta>
            <ta e="T93" id="Seg_2293" s="T92">ej</ta>
            <ta e="T94" id="Seg_2294" s="T93">dĭ</ta>
            <ta e="T95" id="Seg_2295" s="T94">kan-bi</ta>
            <ta e="T96" id="Seg_2296" s="T95">mĭn-luʔbdə-bi</ta>
            <ta e="T97" id="Seg_2297" s="T96">menə-j</ta>
            <ta e="T98" id="Seg_2298" s="T97">bazoʔ</ta>
            <ta e="T99" id="Seg_2299" s="T98">sagər</ta>
            <ta e="T100" id="Seg_2300" s="T99">ine-zəbi</ta>
            <ta e="T101" id="Seg_2301" s="T100">aləp</ta>
            <ta e="T102" id="Seg_2302" s="T101">šonə-lamnə</ta>
            <ta e="T103" id="Seg_2303" s="T102">bazoʔ</ta>
            <ta e="T104" id="Seg_2304" s="T103">šündə-luʔbdə-bi-jəʔ</ta>
            <ta e="T105" id="Seg_2305" s="T104">ine-t</ta>
            <ta e="T106" id="Seg_2306" s="T105">bazoʔ</ta>
            <ta e="T107" id="Seg_2307" s="T106">sini-gəndə</ta>
            <ta e="T108" id="Seg_2308" s="T107">šo-lAʔ</ta>
            <ta e="T109" id="Seg_2309" s="T108">üzə-bi</ta>
            <ta e="T110" id="Seg_2310" s="T109">bazoʔ</ta>
            <ta e="T111" id="Seg_2311" s="T110">penzüt</ta>
            <ta e="T112" id="Seg_2312" s="T111">măn-liA</ta>
            <ta e="T113" id="Seg_2313" s="T112">eneidəne</ta>
            <ta e="T114" id="Seg_2314" s="T113">bos-də</ta>
            <ta e="T115" id="Seg_2315" s="T114">šündə-liA</ta>
            <ta e="T116" id="Seg_2316" s="T115">bos-də</ta>
            <ta e="T117" id="Seg_2317" s="T116">bazoʔ</ta>
            <ta e="T118" id="Seg_2318" s="T117">menə-luʔbdə-bi</ta>
            <ta e="T119" id="Seg_2319" s="T118">dĭ</ta>
            <ta e="T120" id="Seg_2320" s="T119">ešši-zAŋ</ta>
            <ta e="T121" id="Seg_2321" s="T120">kegərer-bi-jəʔ</ta>
            <ta e="T122" id="Seg_2322" s="T121">kegərer-liA-jəʔ</ta>
            <ta e="T123" id="Seg_2323" s="T122">kan-NTA</ta>
            <ta e="T124" id="Seg_2324" s="T123">aʔtʼi-lAʔ</ta>
            <ta e="T125" id="Seg_2325" s="T124">mo-KV-j</ta>
            <ta e="T126" id="Seg_2326" s="T125">šo-NTA</ta>
            <ta e="T127" id="Seg_2327" s="T126">aʔtʼi-lAʔ</ta>
            <ta e="T128" id="Seg_2328" s="T127">naga-KV-j</ta>
            <ta e="T129" id="Seg_2329" s="T128">bazoʔ</ta>
            <ta e="T130" id="Seg_2330" s="T129">sĭri</ta>
            <ta e="T131" id="Seg_2331" s="T130">ine-zəbi</ta>
            <ta e="T132" id="Seg_2332" s="T131">aləp</ta>
            <ta e="T133" id="Seg_2333" s="T132">šonə-lamnə</ta>
            <ta e="T134" id="Seg_2334" s="T133">dĭ-zAŋ</ta>
            <ta e="T135" id="Seg_2335" s="T134">ešši-zAŋ</ta>
            <ta e="T136" id="Seg_2336" s="T135">bazoʔ</ta>
            <ta e="T137" id="Seg_2337" s="T136">šündə-bi-jəʔ</ta>
            <ta e="T138" id="Seg_2338" s="T137">ine-t</ta>
            <ta e="T139" id="Seg_2339" s="T138">nu-laːm-bi</ta>
            <ta e="T140" id="Seg_2340" s="T139">dĭgəttə</ta>
            <ta e="T141" id="Seg_2341" s="T140">kan-lAʔ</ta>
            <ta e="T142" id="Seg_2342" s="T141">ku-bi</ta>
            <ta e="T143" id="Seg_2343" s="T142">kajət</ta>
            <ta e="T144" id="Seg_2344" s="T143">ešši-zAŋ</ta>
            <ta e="T145" id="Seg_2345" s="T144">i-gA-lAʔ</ta>
            <ta e="T146" id="Seg_2346" s="T145">dĭrgit</ta>
            <ta e="T147" id="Seg_2347" s="T146">dĭrgit</ta>
            <ta e="T148" id="Seg_2348" s="T147">ešši-zAŋ</ta>
            <ta e="T149" id="Seg_2349" s="T148">i-gA-bAʔ</ta>
            <ta e="T150" id="Seg_2350" s="T149">ešši-zAŋ-Tə</ta>
            <ta e="T151" id="Seg_2351" s="T150">šü</ta>
            <ta e="T152" id="Seg_2352" s="T151">hen-bi</ta>
            <ta e="T153" id="Seg_2353" s="T152">kan-bi</ta>
            <ta e="T154" id="Seg_2354" s="T153">bulan</ta>
            <ta e="T155" id="Seg_2355" s="T154">tʼit-bi</ta>
            <ta e="T156" id="Seg_2356" s="T155">det-bi</ta>
            <ta e="T157" id="Seg_2357" s="T156">ešši-zAŋ-Tə</ta>
            <ta e="T158" id="Seg_2358" s="T157">bulan-ə-n</ta>
            <ta e="T159" id="Seg_2359" s="T158">kuba-t-ziʔ</ta>
            <ta e="T160" id="Seg_2360" s="T159">ešši-zAŋ-Tə</ta>
            <ta e="T161" id="Seg_2361" s="T160">maʔ</ta>
            <ta e="T162" id="Seg_2362" s="T161">a-bi</ta>
            <ta e="T163" id="Seg_2363" s="T162">amno-KAʔ</ta>
            <ta e="T164" id="Seg_2364" s="T163">ešši-zAŋ</ta>
            <ta e="T165" id="Seg_2365" s="T164">tʼili</ta>
            <ta e="T166" id="Seg_2366" s="T165">i-bi-gənʼi</ta>
            <ta e="T167" id="Seg_2367" s="T166">par-bi-gənʼi</ta>
            <ta e="T168" id="Seg_2368" s="T167">i-lV-m</ta>
            <ta e="T169" id="Seg_2369" s="T168">šiʔnʼileʔ</ta>
            <ta e="T170" id="Seg_2370" s="T169">dărəʔ</ta>
            <ta e="T171" id="Seg_2371" s="T170">măn-bi</ta>
            <ta e="T172" id="Seg_2372" s="T171">bos-də</ta>
            <ta e="T173" id="Seg_2373" s="T172">mĭn-luʔbdə-bi</ta>
            <ta e="T174" id="Seg_2374" s="T173">i</ta>
            <ta e="T175" id="Seg_2375" s="T174">nagur-git</ta>
            <ta e="T176" id="Seg_2376" s="T175">tʼala-n</ta>
            <ta e="T177" id="Seg_2377" s="T176">dĭ</ta>
            <ta e="T178" id="Seg_2378" s="T177">šide</ta>
            <ta e="T179" id="Seg_2379" s="T178">aləp-ə-n</ta>
            <ta e="T180" id="Seg_2380" s="T179">bos</ta>
            <ta e="T181" id="Seg_2381" s="T180">ine-zAŋ-dən</ta>
            <ta e="T182" id="Seg_2382" s="T181">par-laːndə-gA-jəʔ</ta>
            <ta e="T183" id="Seg_2383" s="T182">konzan-zAŋ-dən</ta>
            <ta e="T184" id="Seg_2384" s="T183">pakzən-zAŋ-dən</ta>
            <ta e="T185" id="Seg_2385" s="T184">ine-n</ta>
            <ta e="T186" id="Seg_2386" s="T185">nanə-gəndə</ta>
            <ta e="T187" id="Seg_2387" s="T186">lambər-lAʔ</ta>
            <ta e="T188" id="Seg_2388" s="T187">kandə-gA-jəʔ</ta>
            <ta e="T189" id="Seg_2389" s="T188">dĭ-zen</ta>
            <ta e="T190" id="Seg_2390" s="T189">piʔne-ttə</ta>
            <ta e="T191" id="Seg_2391" s="T190">sĭri</ta>
            <ta e="T192" id="Seg_2392" s="T191">ine-zəbi</ta>
            <ta e="T193" id="Seg_2393" s="T192">par-lamnə</ta>
            <ta e="T194" id="Seg_2394" s="T193">šo-bi</ta>
            <ta e="T195" id="Seg_2395" s="T194">ešši-zAŋ-gəndən</ta>
            <ta e="T196" id="Seg_2396" s="T195">i-bi</ta>
            <ta e="T197" id="Seg_2397" s="T196">ešši-m</ta>
            <ta e="T198" id="Seg_2398" s="T197">maʔ-gəndə</ta>
            <ta e="T199" id="Seg_2399" s="T198">kun-lAʔ</ta>
            <ta e="T200" id="Seg_2400" s="T199">kan-bi</ta>
            <ta e="T201" id="Seg_2401" s="T200">măn-liA</ta>
            <ta e="T202" id="Seg_2402" s="T201">măn</ta>
            <ta e="T203" id="Seg_2403" s="T202">amno-l-lAʔ=bo</ta>
            <ta e="T204" id="Seg_2404" s="T203">aľi</ta>
            <ta e="T205" id="Seg_2405" s="T204">maʔ-gənʼilAʔ</ta>
            <ta e="T206" id="Seg_2406" s="T205">kan-lV-lAʔ=bo</ta>
            <ta e="T207" id="Seg_2407" s="T206">oʔbdə</ta>
            <ta e="T208" id="Seg_2408" s="T207">măn-liA</ta>
            <ta e="T209" id="Seg_2409" s="T208">măn</ta>
            <ta e="T210" id="Seg_2410" s="T209">kan-lV-m</ta>
            <ta e="T211" id="Seg_2411" s="T210">oʔbdə</ta>
            <ta e="T212" id="Seg_2412" s="T211">dĭgən</ta>
            <ta e="T213" id="Seg_2413" s="T212">ma-bi</ta>
            <ta e="T214" id="Seg_2414" s="T213">aləp-j</ta>
            <ta e="T215" id="Seg_2415" s="T214">ine</ta>
            <ta e="T216" id="Seg_2416" s="T215">konzan-də-lAʔ</ta>
            <ta e="T217" id="Seg_2417" s="T216">mĭ-bi</ta>
            <ta e="T218" id="Seg_2418" s="T217">mĭn-luʔbdə-bi</ta>
            <ta e="T219" id="Seg_2419" s="T218">maʔ-gəndə</ta>
            <ta e="T220" id="Seg_2420" s="T219">šo-bi</ta>
            <ta e="T221" id="Seg_2421" s="T220">onʼiʔ</ta>
            <ta e="T222" id="Seg_2422" s="T221">maʔ</ta>
            <ta e="T223" id="Seg_2423" s="T222">nu-gA</ta>
            <ta e="T224" id="Seg_2424" s="T223">jada-t</ta>
            <ta e="T225" id="Seg_2425" s="T224">kak</ta>
            <ta e="T226" id="Seg_2426" s="T225">tüjə</ta>
            <ta e="T227" id="Seg_2427" s="T226">naga</ta>
            <ta e="T228" id="Seg_2428" s="T227">dĭ</ta>
            <ta e="T229" id="Seg_2429" s="T228">maʔ-Tə</ta>
            <ta e="T230" id="Seg_2430" s="T229">šü-bi</ta>
            <ta e="T231" id="Seg_2431" s="T230">surar-liA-t</ta>
            <ta e="T232" id="Seg_2432" s="T231">tʼaktə</ta>
            <ta e="T233" id="Seg_2433" s="T232">nüke</ta>
            <ta e="T234" id="Seg_2434" s="T233">tʼaktə</ta>
            <ta e="T235" id="Seg_2435" s="T234">büzʼe</ta>
            <ta e="T236" id="Seg_2436" s="T235">jada-lAʔ</ta>
            <ta e="T237" id="Seg_2437" s="T236">giraːm-bi</ta>
            <ta e="T238" id="Seg_2438" s="T237">dĭ-zAŋ</ta>
            <ta e="T239" id="Seg_2439" s="T238">nörbə-liA-jəʔ</ta>
            <ta e="T240" id="Seg_2440" s="T239">šide</ta>
            <ta e="T241" id="Seg_2441" s="T240">nʼi-bAʔ</ta>
            <ta e="T242" id="Seg_2442" s="T241">i-bi</ta>
            <ta e="T243" id="Seg_2443" s="T242">il</ta>
            <ta e="T244" id="Seg_2444" s="T243">so-Tə</ta>
            <ta e="T245" id="Seg_2445" s="T244">sar-bizA</ta>
            <ta e="T246" id="Seg_2446" s="T245">öʔ-lAʔ</ta>
            <ta e="T247" id="Seg_2447" s="T246">mĭ-bi</ta>
            <ta e="T248" id="Seg_2448" s="T247">bü-Tə</ta>
            <ta e="T249" id="Seg_2449" s="T248">dĭ-zen</ta>
            <ta e="T250" id="Seg_2450" s="T249">piʔne-gəndə</ta>
            <ta e="T251" id="Seg_2451" s="T250">koʔbdo-bAʔ</ta>
            <ta e="T252" id="Seg_2452" s="T251">i-bi</ta>
            <ta e="T253" id="Seg_2453" s="T252">dĭ</ta>
            <ta e="T254" id="Seg_2454" s="T253">il-m</ta>
            <ta e="T255" id="Seg_2455" s="T254">bar</ta>
            <ta e="T256" id="Seg_2456" s="T255">kut-lAʔ</ta>
            <ta e="T257" id="Seg_2457" s="T256">kun-bi</ta>
            <ta e="T258" id="Seg_2458" s="T257">a</ta>
            <ta e="T259" id="Seg_2459" s="T258">gijen</ta>
            <ta e="T260" id="Seg_2460" s="T259">koʔbdo-lAʔ</ta>
            <ta e="T261" id="Seg_2461" s="T260">giraːm-bi</ta>
            <ta e="T262" id="Seg_2462" s="T261">bos-bAʔ</ta>
            <ta e="T263" id="Seg_2463" s="T262">ej</ta>
            <ta e="T264" id="Seg_2464" s="T263">tĭmne-bAʔ</ta>
            <ta e="T265" id="Seg_2465" s="T264">dĭ</ta>
            <ta e="T266" id="Seg_2466" s="T265">nʼi</ta>
            <ta e="T267" id="Seg_2467" s="T266">ine-bə</ta>
            <ta e="T268" id="Seg_2468" s="T267">šĭ-bi</ta>
            <ta e="T269" id="Seg_2469" s="T268">pi-lAʔ</ta>
            <ta e="T270" id="Seg_2470" s="T269">kan-lAʔ</ta>
            <ta e="T271" id="Seg_2471" s="T270">tʼür-bi</ta>
            <ta e="T272" id="Seg_2472" s="T271">kan-bi</ta>
            <ta e="T273" id="Seg_2473" s="T272">urgo</ta>
            <ta e="T274" id="Seg_2474" s="T273">karaʔ-Tə</ta>
            <ta e="T275" id="Seg_2475" s="T274">dĭgən</ta>
            <ta e="T276" id="Seg_2476" s="T275">tuno-l-də-lAʔ</ta>
            <ta e="T277" id="Seg_2477" s="T276">mĭn-laʔbə-bi</ta>
            <ta e="T278" id="Seg_2478" s="T277">dĭ</ta>
            <ta e="T279" id="Seg_2479" s="T278">nʼi-m</ta>
            <ta e="T280" id="Seg_2480" s="T279">ku-bizA</ta>
            <ta e="T281" id="Seg_2481" s="T280">tuno-l-də-lAʔ</ta>
            <ta e="T282" id="Seg_2482" s="T281">šo-bi</ta>
            <ta e="T283" id="Seg_2483" s="T282">süʔmə-lAʔ</ta>
            <ta e="T284" id="Seg_2484" s="T283">naŋ-bi</ta>
            <ta e="T285" id="Seg_2485" s="T284">dĭ</ta>
            <ta e="T286" id="Seg_2486" s="T285">nʼi-Tə</ta>
            <ta e="T287" id="Seg_2487" s="T286">dĭgəttə</ta>
            <ta e="T288" id="Seg_2488" s="T287">tʼabəro-bi-jəʔ</ta>
            <ta e="T289" id="Seg_2489" s="T288">kaga-t</ta>
            <ta e="T290" id="Seg_2490" s="T289">üštə-luʔbdə-bi</ta>
            <ta e="T291" id="Seg_2491" s="T290">dĭ-m</ta>
            <ta e="T292" id="Seg_2492" s="T291">tʼabo-laʔbə</ta>
            <ta e="T293" id="Seg_2493" s="T292">nörbə-laʔbə</ta>
            <ta e="T294" id="Seg_2494" s="T293">măn</ta>
            <ta e="T295" id="Seg_2495" s="T294">tăn</ta>
            <ta e="T296" id="Seg_2496" s="T295">kaga-l</ta>
            <ta e="T297" id="Seg_2497" s="T296">i-gA-m</ta>
            <ta e="T298" id="Seg_2498" s="T297">ej</ta>
            <ta e="T299" id="Seg_2499" s="T298">tʼabəro-ʔ</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_2500" s="T0">woman.[NOM.SG]</ta>
            <ta e="T2" id="Seg_2501" s="T1">man-INS</ta>
            <ta e="T3" id="Seg_2502" s="T2">live-PST.[3SG]</ta>
            <ta e="T4" id="Seg_2503" s="T3">two.[NOM.SG]</ta>
            <ta e="T5" id="Seg_2504" s="T4">son-NOM/GEN/ACC.3PL</ta>
            <ta e="T6" id="Seg_2505" s="T5">be-PST-3PL</ta>
            <ta e="T7" id="Seg_2506" s="T6">this.[NOM.SG]</ta>
            <ta e="T8" id="Seg_2507" s="T7">son-PL-NOM/GEN/ACC.3PL</ta>
            <ta e="T9" id="Seg_2508" s="T8">play-CVB.TEMP-LAT/LOC.3PL</ta>
            <ta e="T10" id="Seg_2509" s="T9">child-GEN.PL</ta>
            <ta e="T11" id="Seg_2510" s="T10">hand-ACC.3SG</ta>
            <ta e="T12" id="Seg_2511" s="T11">off</ta>
            <ta e="T13" id="Seg_2512" s="T12">pull-FUT-3PL.O</ta>
            <ta e="T14" id="Seg_2513" s="T13">raft.[NOM.SG]</ta>
            <ta e="T15" id="Seg_2514" s="T14">make-PST-3PL</ta>
            <ta e="T16" id="Seg_2515" s="T15">raft-LAT</ta>
            <ta e="T17" id="Seg_2516" s="T16">bind-PST-3PL</ta>
            <ta e="T18" id="Seg_2517" s="T17">let-CVB</ta>
            <ta e="T19" id="Seg_2518" s="T18">give-PST-3PL</ta>
            <ta e="T20" id="Seg_2519" s="T19">water-LAT</ta>
            <ta e="T21" id="Seg_2520" s="T20">water.[NOM.SG]</ta>
            <ta e="T22" id="Seg_2521" s="T21">flow-CVB</ta>
            <ta e="T23" id="Seg_2522" s="T22">come-DUR-3PL</ta>
            <ta e="T24" id="Seg_2523" s="T23">cliff-GEN</ta>
            <ta e="T25" id="Seg_2524" s="T24">underpart-LAT/LOC.3SG</ta>
            <ta e="T26" id="Seg_2525" s="T25">water.[NOM.SG]</ta>
            <ta e="T27" id="Seg_2526" s="T26">creep.into-DUR.[3SG]</ta>
            <ta e="T28" id="Seg_2527" s="T27">this-PL</ta>
            <ta e="T29" id="Seg_2528" s="T28">call.out-PST-3PL</ta>
            <ta e="T30" id="Seg_2529" s="T29">God-EP-GEN</ta>
            <ta e="T31" id="Seg_2530" s="T30">mother-NOM/GEN.3SG</ta>
            <ta e="T32" id="Seg_2531" s="T31">mother-NOM/GEN/ACC.1PL</ta>
            <ta e="T33" id="Seg_2532" s="T32">God-EP-GEN</ta>
            <ta e="T34" id="Seg_2533" s="T33">mother-NOM/GEN.3SG</ta>
            <ta e="T35" id="Seg_2534" s="T34">pull.out-IMP.2SG.O</ta>
            <ta e="T36" id="Seg_2535" s="T35">we.ACC</ta>
            <ta e="T37" id="Seg_2536" s="T36">God-EP-GEN</ta>
            <ta e="T38" id="Seg_2537" s="T37">mother-NOM/GEN.3SG</ta>
            <ta e="T39" id="Seg_2538" s="T38">left.[NOM.SG]</ta>
            <ta e="T40" id="Seg_2539" s="T39">hair-ACC.3SG</ta>
            <ta e="T41" id="Seg_2540" s="T40">untie-PST.[3SG]</ta>
            <ta e="T42" id="Seg_2541" s="T41">let-CVB</ta>
            <ta e="T43" id="Seg_2542" s="T42">give-PST.[3SG]</ta>
            <ta e="T44" id="Seg_2543" s="T43">child-PL-LAT</ta>
            <ta e="T45" id="Seg_2544" s="T44">NEG</ta>
            <ta e="T46" id="Seg_2545" s="T45">arrive-CVB</ta>
            <ta e="T47" id="Seg_2546" s="T46">hair-NOM/GEN.3SG</ta>
            <ta e="T48" id="Seg_2547" s="T47">right.[NOM.SG]</ta>
            <ta e="T49" id="Seg_2548" s="T48">half-NOM/GEN/ACC.3SG</ta>
            <ta e="T50" id="Seg_2549" s="T49">untie-PST.[3SG]</ta>
            <ta e="T51" id="Seg_2550" s="T50">right.[NOM.SG]</ta>
            <ta e="T52" id="Seg_2551" s="T51">hair-NOM/GEN.3SG</ta>
            <ta e="T53" id="Seg_2552" s="T52">arrive-PST</ta>
            <ta e="T54" id="Seg_2553" s="T53">child-PL-LAT</ta>
            <ta e="T55" id="Seg_2554" s="T54">child-PL</ta>
            <ta e="T56" id="Seg_2555" s="T55">collect-PST.[3SG]</ta>
            <ta e="T57" id="Seg_2556" s="T56">seize-CVB</ta>
            <ta e="T58" id="Seg_2557" s="T57">depart-PST-3PL</ta>
            <ta e="T59" id="Seg_2558" s="T58">this-ABL</ta>
            <ta e="T60" id="Seg_2559" s="T59">cliff-GEN</ta>
            <ta e="T61" id="Seg_2560" s="T60">top-LAT/LOC.3SG</ta>
            <ta e="T62" id="Seg_2561" s="T61">get.up-TR-PST.[3SG]</ta>
            <ta e="T63" id="Seg_2562" s="T62">let-CVB</ta>
            <ta e="T64" id="Seg_2563" s="T63">give-PST.[3SG]</ta>
            <ta e="T65" id="Seg_2564" s="T64">go-IMP.2PL</ta>
            <ta e="T66" id="Seg_2565" s="T65">road.[NOM.SG]</ta>
            <ta e="T67" id="Seg_2566" s="T66">along</ta>
            <ta e="T68" id="Seg_2567" s="T67">go-INCH-PST-3PL</ta>
            <ta e="T69" id="Seg_2568" s="T68">walk-PST-3PL</ta>
            <ta e="T70" id="Seg_2569" s="T69">walk-PST-3PL</ta>
            <ta e="T71" id="Seg_2570" s="T70">big</ta>
            <ta e="T72" id="Seg_2571" s="T71">road.[NOM.SG]</ta>
            <ta e="T73" id="Seg_2572" s="T72">come.up-PST.[3SG]</ta>
            <ta e="T74" id="Seg_2573" s="T73">this-PL</ta>
            <ta e="T75" id="Seg_2574" s="T74">road-GEN</ta>
            <ta e="T76" id="Seg_2575" s="T75">edge-LAT/LOC.3SG</ta>
            <ta e="T77" id="Seg_2576" s="T76">sit-3PL</ta>
            <ta e="T78" id="Seg_2577" s="T77">see-CVB.TEMP-LAT/LOC.3PL</ta>
            <ta e="T79" id="Seg_2578" s="T78">bay.[NOM.SG]</ta>
            <ta e="T80" id="Seg_2579" s="T79">horse-ADJZ</ta>
            <ta e="T81" id="Seg_2580" s="T80">hero.[NOM.SG]</ta>
            <ta e="T82" id="Seg_2581" s="T81">come-DUR.[3SG]</ta>
            <ta e="T83" id="Seg_2582" s="T82">this-PL</ta>
            <ta e="T84" id="Seg_2583" s="T83">whistle-PST-3PL</ta>
            <ta e="T85" id="Seg_2584" s="T84">horse-NOM/GEN.3SG</ta>
            <ta e="T86" id="Seg_2585" s="T85">knee-LAT/LOC.3SG</ta>
            <ta e="T87" id="Seg_2586" s="T86">come-CVB</ta>
            <ta e="T88" id="Seg_2587" s="T87">descend-PST.[3SG]</ta>
            <ta e="T89" id="Seg_2588" s="T88">helping.spirit.[NOM.SG]</ta>
            <ta e="T90" id="Seg_2589" s="T89">Eneidene.[NOM.SG]</ta>
            <ta e="T91" id="Seg_2590" s="T90">whistle-PRS.[3SG]</ta>
            <ta e="T92" id="Seg_2591" s="T91">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T93" id="Seg_2592" s="T92">NEG</ta>
            <ta e="T94" id="Seg_2593" s="T93">this.[NOM.SG]</ta>
            <ta e="T95" id="Seg_2594" s="T94">go-PST.[3SG]</ta>
            <ta e="T96" id="Seg_2595" s="T95">go-MOM-PST.[3SG]</ta>
            <ta e="T97" id="Seg_2596" s="T96">pass.by-CVB</ta>
            <ta e="T98" id="Seg_2597" s="T97">again</ta>
            <ta e="T99" id="Seg_2598" s="T98">black.[NOM.SG]</ta>
            <ta e="T100" id="Seg_2599" s="T99">horse-ADJZ</ta>
            <ta e="T101" id="Seg_2600" s="T100">hero.[NOM.SG]</ta>
            <ta e="T102" id="Seg_2601" s="T101">come-DUR.[3SG]</ta>
            <ta e="T103" id="Seg_2602" s="T102">again</ta>
            <ta e="T104" id="Seg_2603" s="T103">whistle-MOM-PST-3PL</ta>
            <ta e="T105" id="Seg_2604" s="T104">horse-NOM/GEN.3SG</ta>
            <ta e="T106" id="Seg_2605" s="T105">again</ta>
            <ta e="T107" id="Seg_2606" s="T106">knee-LAT/LOC.3SG</ta>
            <ta e="T108" id="Seg_2607" s="T107">come-CVB</ta>
            <ta e="T109" id="Seg_2608" s="T108">descend-PST.[3SG]</ta>
            <ta e="T110" id="Seg_2609" s="T109">again</ta>
            <ta e="T111" id="Seg_2610" s="T110">helping.spirit.[NOM.SG]</ta>
            <ta e="T112" id="Seg_2611" s="T111">say-PRS.[3SG]</ta>
            <ta e="T113" id="Seg_2612" s="T112">Eneidene.[NOM.SG]</ta>
            <ta e="T114" id="Seg_2613" s="T113">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T115" id="Seg_2614" s="T114">whistle-PRS.[3SG]</ta>
            <ta e="T116" id="Seg_2615" s="T115">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T117" id="Seg_2616" s="T116">again</ta>
            <ta e="T118" id="Seg_2617" s="T117">pass.by-MOM-PST.[3SG]</ta>
            <ta e="T119" id="Seg_2618" s="T118">this.[NOM.SG]</ta>
            <ta e="T120" id="Seg_2619" s="T119">child-PL</ta>
            <ta e="T121" id="Seg_2620" s="T120">call.out-PST-3PL</ta>
            <ta e="T122" id="Seg_2621" s="T121">call.out-PRS-3PL</ta>
            <ta e="T123" id="Seg_2622" s="T122">go-PTCP</ta>
            <ta e="T124" id="Seg_2623" s="T123">way-NOM/GEN/ACC.2PL</ta>
            <ta e="T125" id="Seg_2624" s="T124">become-IMP-3SG</ta>
            <ta e="T126" id="Seg_2625" s="T125">come-PTCP</ta>
            <ta e="T127" id="Seg_2626" s="T126">way-NOM/GEN/ACC.2PL</ta>
            <ta e="T128" id="Seg_2627" s="T127">NEG.EX-IMP-3SG</ta>
            <ta e="T129" id="Seg_2628" s="T128">again</ta>
            <ta e="T130" id="Seg_2629" s="T129">white.[NOM.SG]</ta>
            <ta e="T131" id="Seg_2630" s="T130">horse-ADJZ</ta>
            <ta e="T132" id="Seg_2631" s="T131">hero.[NOM.SG]</ta>
            <ta e="T133" id="Seg_2632" s="T132">come-DUR.[3SG]</ta>
            <ta e="T134" id="Seg_2633" s="T133">this-PL</ta>
            <ta e="T135" id="Seg_2634" s="T134">child-PL</ta>
            <ta e="T136" id="Seg_2635" s="T135">again</ta>
            <ta e="T137" id="Seg_2636" s="T136">whistle-PST-3PL</ta>
            <ta e="T138" id="Seg_2637" s="T137">horse-NOM/GEN.3SG</ta>
            <ta e="T139" id="Seg_2638" s="T138">stand-RES-PST.[3SG]</ta>
            <ta e="T140" id="Seg_2639" s="T139">then</ta>
            <ta e="T141" id="Seg_2640" s="T140">go-CVB</ta>
            <ta e="T142" id="Seg_2641" s="T141">see-PST.[3SG]</ta>
            <ta e="T143" id="Seg_2642" s="T142">what.kind</ta>
            <ta e="T144" id="Seg_2643" s="T143">child-PL</ta>
            <ta e="T145" id="Seg_2644" s="T144">be-PRS-2PL</ta>
            <ta e="T146" id="Seg_2645" s="T145">such.[NOM.SG]</ta>
            <ta e="T147" id="Seg_2646" s="T146">such.[NOM.SG]</ta>
            <ta e="T148" id="Seg_2647" s="T147">child-PL</ta>
            <ta e="T149" id="Seg_2648" s="T148">be-PRS-1PL</ta>
            <ta e="T150" id="Seg_2649" s="T149">child-PL-LAT</ta>
            <ta e="T151" id="Seg_2650" s="T150">fire.[NOM.SG]</ta>
            <ta e="T152" id="Seg_2651" s="T151">put-PST.[3SG]</ta>
            <ta e="T153" id="Seg_2652" s="T152">go-PST.[3SG]</ta>
            <ta e="T154" id="Seg_2653" s="T153">moose.[NOM.SG]</ta>
            <ta e="T155" id="Seg_2654" s="T154">shoot-PST.[3SG]</ta>
            <ta e="T156" id="Seg_2655" s="T155">bring-PST.[3SG]</ta>
            <ta e="T157" id="Seg_2656" s="T156">child-PL-LAT</ta>
            <ta e="T158" id="Seg_2657" s="T157">moose-EP-GEN</ta>
            <ta e="T159" id="Seg_2658" s="T158">skin-3SG-INS</ta>
            <ta e="T160" id="Seg_2659" s="T159">child-PL-LAT</ta>
            <ta e="T161" id="Seg_2660" s="T160">tent.[NOM.SG]</ta>
            <ta e="T162" id="Seg_2661" s="T161">make-PST.[3SG]</ta>
            <ta e="T163" id="Seg_2662" s="T162">live-IMP.2PL</ta>
            <ta e="T164" id="Seg_2663" s="T163">child-PL</ta>
            <ta e="T165" id="Seg_2664" s="T164">alive.[NOM.SG]</ta>
            <ta e="T166" id="Seg_2665" s="T165">be-CVB.TEMP-LAT/LOC.1SG</ta>
            <ta e="T167" id="Seg_2666" s="T166">return-CVB.TEMP-LAT/LOC.1SG</ta>
            <ta e="T168" id="Seg_2667" s="T167">take-FUT-1SG</ta>
            <ta e="T169" id="Seg_2668" s="T168">you.PL.ACC</ta>
            <ta e="T170" id="Seg_2669" s="T169">so</ta>
            <ta e="T171" id="Seg_2670" s="T170">say-PST.[3SG]</ta>
            <ta e="T172" id="Seg_2671" s="T171">self-NOM/GEN/ACC.3SG</ta>
            <ta e="T173" id="Seg_2672" s="T172">go-MOM-PST.[3SG]</ta>
            <ta e="T174" id="Seg_2673" s="T173">and</ta>
            <ta e="T175" id="Seg_2674" s="T174">three-ORD.[NOM.SG]</ta>
            <ta e="T176" id="Seg_2675" s="T175">day-GEN</ta>
            <ta e="T177" id="Seg_2676" s="T176">this.[NOM.SG]</ta>
            <ta e="T178" id="Seg_2677" s="T177">two.[NOM.SG]</ta>
            <ta e="T179" id="Seg_2678" s="T178">hero-EP-GEN</ta>
            <ta e="T180" id="Seg_2679" s="T179">self.[NOM.SG]</ta>
            <ta e="T181" id="Seg_2680" s="T180">horse-PL-NOM/GEN/ACC.3PL</ta>
            <ta e="T182" id="Seg_2681" s="T181">return-DUR-PRS-3PL</ta>
            <ta e="T183" id="Seg_2682" s="T182">saddle-PL-NOM/GEN/ACC.3PL</ta>
            <ta e="T184" id="Seg_2683" s="T183">stirrup-PL-NOM/GEN/ACC.3PL</ta>
            <ta e="T185" id="Seg_2684" s="T184">horse-GEN</ta>
            <ta e="T186" id="Seg_2685" s="T185">belly-LAT/LOC.3SG</ta>
            <ta e="T187" id="Seg_2686" s="T186">dangle-CVB</ta>
            <ta e="T188" id="Seg_2687" s="T187">walk-PRS-3PL</ta>
            <ta e="T189" id="Seg_2688" s="T188">this-GEN.PL</ta>
            <ta e="T190" id="Seg_2689" s="T189">behind-ABL.3SG</ta>
            <ta e="T191" id="Seg_2690" s="T190">white.[NOM.SG]</ta>
            <ta e="T192" id="Seg_2691" s="T191">horse-ADJZ</ta>
            <ta e="T193" id="Seg_2692" s="T192">return-DUR.[3SG]</ta>
            <ta e="T194" id="Seg_2693" s="T193">come-PST.[3SG]</ta>
            <ta e="T195" id="Seg_2694" s="T194">child-PL-LAT/LOC.3PL</ta>
            <ta e="T196" id="Seg_2695" s="T195">take-PST.[3SG]</ta>
            <ta e="T197" id="Seg_2696" s="T196">child-ACC</ta>
            <ta e="T198" id="Seg_2697" s="T197">tent-LAT/LOC.3SG</ta>
            <ta e="T199" id="Seg_2698" s="T198">bring-CVB</ta>
            <ta e="T200" id="Seg_2699" s="T199">go-PST.[3SG]</ta>
            <ta e="T201" id="Seg_2700" s="T200">say-PRS.[3SG]</ta>
            <ta e="T202" id="Seg_2701" s="T201">I.NOM</ta>
            <ta e="T203" id="Seg_2702" s="T202">live-FUT-2PL=QP</ta>
            <ta e="T204" id="Seg_2703" s="T203">or</ta>
            <ta e="T205" id="Seg_2704" s="T204">tent-LAT/LOC.2PL</ta>
            <ta e="T206" id="Seg_2705" s="T205">go-FUT-2PL=QP</ta>
            <ta e="T207" id="Seg_2706" s="T206">one.[NOM.SG]</ta>
            <ta e="T208" id="Seg_2707" s="T207">say-PRS.[3SG]</ta>
            <ta e="T209" id="Seg_2708" s="T208">I.NOM</ta>
            <ta e="T210" id="Seg_2709" s="T209">go-FUT-1SG</ta>
            <ta e="T211" id="Seg_2710" s="T210">one.[NOM.SG]</ta>
            <ta e="T212" id="Seg_2711" s="T211">there</ta>
            <ta e="T213" id="Seg_2712" s="T212">remain-PST.[3SG]</ta>
            <ta e="T214" id="Seg_2713" s="T213">hero-ADJZ.[NOM.SG]</ta>
            <ta e="T215" id="Seg_2714" s="T214">horse.[NOM.SG]</ta>
            <ta e="T216" id="Seg_2715" s="T215">saddle-TR-CVB</ta>
            <ta e="T217" id="Seg_2716" s="T216">give-PST.[3SG]</ta>
            <ta e="T218" id="Seg_2717" s="T217">go-MOM-PST.[3SG]</ta>
            <ta e="T219" id="Seg_2718" s="T218">tent-LAT/LOC.3SG</ta>
            <ta e="T220" id="Seg_2719" s="T219">come-PST.[3SG]</ta>
            <ta e="T221" id="Seg_2720" s="T220">single.[NOM.SG]</ta>
            <ta e="T222" id="Seg_2721" s="T221">tent.[NOM.SG]</ta>
            <ta e="T223" id="Seg_2722" s="T222">stand-PRS.[3SG]</ta>
            <ta e="T224" id="Seg_2723" s="T223">village-NOM/GEN.3SG</ta>
            <ta e="T225" id="Seg_2724" s="T224">like</ta>
            <ta e="T226" id="Seg_2725" s="T225">before</ta>
            <ta e="T227" id="Seg_2726" s="T226">NEG.EX.[3SG]</ta>
            <ta e="T228" id="Seg_2727" s="T227">this.[NOM.SG]</ta>
            <ta e="T229" id="Seg_2728" s="T228">tent-LAT</ta>
            <ta e="T230" id="Seg_2729" s="T229">enter-PST.[3SG]</ta>
            <ta e="T231" id="Seg_2730" s="T230">ask-PRS-3SG.O</ta>
            <ta e="T232" id="Seg_2731" s="T231">old.[NOM.SG]</ta>
            <ta e="T233" id="Seg_2732" s="T232">woman.[NOM.SG]</ta>
            <ta e="T234" id="Seg_2733" s="T233">old.[NOM.SG]</ta>
            <ta e="T235" id="Seg_2734" s="T234">man.[NOM.SG]</ta>
            <ta e="T236" id="Seg_2735" s="T235">village-NOM/GEN/ACC.2PL</ta>
            <ta e="T237" id="Seg_2736" s="T236">go.where-PST.[3SG]</ta>
            <ta e="T238" id="Seg_2737" s="T237">this-PL</ta>
            <ta e="T239" id="Seg_2738" s="T238">tell-PRS-3PL</ta>
            <ta e="T240" id="Seg_2739" s="T239">two.[NOM.SG]</ta>
            <ta e="T241" id="Seg_2740" s="T240">son-NOM/GEN/ACC.1PL</ta>
            <ta e="T242" id="Seg_2741" s="T241">be-PST.[3SG]</ta>
            <ta e="T243" id="Seg_2742" s="T242">people.[NOM.SG]</ta>
            <ta e="T244" id="Seg_2743" s="T243">raft-LAT</ta>
            <ta e="T245" id="Seg_2744" s="T244">bind-CVB.ANT</ta>
            <ta e="T246" id="Seg_2745" s="T245">let-CVB</ta>
            <ta e="T247" id="Seg_2746" s="T246">give-PST.[3SG]</ta>
            <ta e="T248" id="Seg_2747" s="T247">water-LAT</ta>
            <ta e="T249" id="Seg_2748" s="T248">this-GEN.PL</ta>
            <ta e="T250" id="Seg_2749" s="T249">after-LAT/LOC.3SG</ta>
            <ta e="T251" id="Seg_2750" s="T250">daughter-NOM/GEN/ACC.1PL</ta>
            <ta e="T252" id="Seg_2751" s="T251">be-PST.[3SG]</ta>
            <ta e="T253" id="Seg_2752" s="T252">this.[NOM.SG]</ta>
            <ta e="T254" id="Seg_2753" s="T253">people-ACC</ta>
            <ta e="T255" id="Seg_2754" s="T254">all</ta>
            <ta e="T256" id="Seg_2755" s="T255">kill-CVB</ta>
            <ta e="T257" id="Seg_2756" s="T256">bring-PST.[3SG]</ta>
            <ta e="T258" id="Seg_2757" s="T257">and</ta>
            <ta e="T259" id="Seg_2758" s="T258">where</ta>
            <ta e="T260" id="Seg_2759" s="T259">daughter-NOM/GEN/ACC.2PL</ta>
            <ta e="T261" id="Seg_2760" s="T260">go.where-PST.[3SG]</ta>
            <ta e="T262" id="Seg_2761" s="T261">self-NOM/GEN/ACC.1PL</ta>
            <ta e="T263" id="Seg_2762" s="T262">NEG</ta>
            <ta e="T264" id="Seg_2763" s="T263">know-1PL</ta>
            <ta e="T265" id="Seg_2764" s="T264">this.[NOM.SG]</ta>
            <ta e="T266" id="Seg_2765" s="T265">boy.[NOM.SG]</ta>
            <ta e="T267" id="Seg_2766" s="T266">horse-ACC.3SG</ta>
            <ta e="T268" id="Seg_2767" s="T267">mount-PST.[3SG]</ta>
            <ta e="T269" id="Seg_2768" s="T268">look.for-CVB</ta>
            <ta e="T270" id="Seg_2769" s="T269">go-CVB</ta>
            <ta e="T271" id="Seg_2770" s="T270">disappear-PST.[3SG]</ta>
            <ta e="T272" id="Seg_2771" s="T271">go-PST.[3SG]</ta>
            <ta e="T273" id="Seg_2772" s="T272">big.[NOM.SG]</ta>
            <ta e="T274" id="Seg_2773" s="T273">steppe-LAT</ta>
            <ta e="T275" id="Seg_2774" s="T274">there</ta>
            <ta e="T276" id="Seg_2775" s="T275">gallop-FRQ-TR-CVB</ta>
            <ta e="T277" id="Seg_2776" s="T276">go-DUR-PST.[3SG]</ta>
            <ta e="T278" id="Seg_2777" s="T277">this.[NOM.SG]</ta>
            <ta e="T279" id="Seg_2778" s="T278">boy-ACC</ta>
            <ta e="T280" id="Seg_2779" s="T279">see-CVB.ANT</ta>
            <ta e="T281" id="Seg_2780" s="T280">gallop-FRQ-TR-CVB</ta>
            <ta e="T282" id="Seg_2781" s="T281">come-PST.[3SG]</ta>
            <ta e="T283" id="Seg_2782" s="T282">jump-CVB</ta>
            <ta e="T284" id="Seg_2783" s="T283">cling-PST.[3SG]</ta>
            <ta e="T285" id="Seg_2784" s="T284">this.[NOM.SG]</ta>
            <ta e="T286" id="Seg_2785" s="T285">boy-LAT</ta>
            <ta e="T287" id="Seg_2786" s="T286">then</ta>
            <ta e="T288" id="Seg_2787" s="T287">fight-PST-3PL</ta>
            <ta e="T289" id="Seg_2788" s="T288">brother-NOM/GEN.3SG</ta>
            <ta e="T290" id="Seg_2789" s="T289">bring.down-MOM-PST</ta>
            <ta e="T291" id="Seg_2790" s="T290">this-ACC</ta>
            <ta e="T292" id="Seg_2791" s="T291">hold-DUR.[3SG]</ta>
            <ta e="T293" id="Seg_2792" s="T292">tell-DUR.[3SG]</ta>
            <ta e="T294" id="Seg_2793" s="T293">I.NOM</ta>
            <ta e="T295" id="Seg_2794" s="T294">you.GEN</ta>
            <ta e="T296" id="Seg_2795" s="T295">brother-NOM/GEN/ACC.2SG</ta>
            <ta e="T297" id="Seg_2796" s="T296">be-PRS-1SG</ta>
            <ta e="T298" id="Seg_2797" s="T297">NEG</ta>
            <ta e="T299" id="Seg_2798" s="T298">fight-IMP.2SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_2799" s="T0">женщина.[NOM.SG]</ta>
            <ta e="T2" id="Seg_2800" s="T1">мужчина-INS</ta>
            <ta e="T3" id="Seg_2801" s="T2">жить-PST.[3SG]</ta>
            <ta e="T4" id="Seg_2802" s="T3">два.[NOM.SG]</ta>
            <ta e="T5" id="Seg_2803" s="T4">сын-NOM/GEN/ACC.3PL</ta>
            <ta e="T6" id="Seg_2804" s="T5">быть-PST-3PL</ta>
            <ta e="T7" id="Seg_2805" s="T6">этот.[NOM.SG]</ta>
            <ta e="T8" id="Seg_2806" s="T7">сын-PL-NOM/GEN/ACC.3PL</ta>
            <ta e="T9" id="Seg_2807" s="T8">играть-CVB.TEMP-LAT/LOC.3PL</ta>
            <ta e="T10" id="Seg_2808" s="T9">ребенок-GEN.PL</ta>
            <ta e="T11" id="Seg_2809" s="T10">рука-ACC.3SG</ta>
            <ta e="T12" id="Seg_2810" s="T11">от</ta>
            <ta e="T13" id="Seg_2811" s="T12">тянуть-FUT-3PL.O</ta>
            <ta e="T14" id="Seg_2812" s="T13">плот.[NOM.SG]</ta>
            <ta e="T15" id="Seg_2813" s="T14">делать-PST-3PL</ta>
            <ta e="T16" id="Seg_2814" s="T15">плот-LAT</ta>
            <ta e="T17" id="Seg_2815" s="T16">завязать-PST-3PL</ta>
            <ta e="T18" id="Seg_2816" s="T17">пускать-CVB</ta>
            <ta e="T19" id="Seg_2817" s="T18">дать-PST-3PL</ta>
            <ta e="T20" id="Seg_2818" s="T19">вода-LAT</ta>
            <ta e="T21" id="Seg_2819" s="T20">вода.[NOM.SG]</ta>
            <ta e="T22" id="Seg_2820" s="T21">течь-CVB</ta>
            <ta e="T23" id="Seg_2821" s="T22">прийти-DUR-3PL</ta>
            <ta e="T24" id="Seg_2822" s="T23">утес-GEN</ta>
            <ta e="T25" id="Seg_2823" s="T24">низ-LAT/LOC.3SG</ta>
            <ta e="T26" id="Seg_2824" s="T25">вода.[NOM.SG]</ta>
            <ta e="T27" id="Seg_2825" s="T26">ползти-DUR.[3SG]</ta>
            <ta e="T28" id="Seg_2826" s="T27">этот-PL</ta>
            <ta e="T29" id="Seg_2827" s="T28">кричать-PST-3PL</ta>
            <ta e="T30" id="Seg_2828" s="T29">бог-EP-GEN</ta>
            <ta e="T31" id="Seg_2829" s="T30">мать-NOM/GEN.3SG</ta>
            <ta e="T32" id="Seg_2830" s="T31">мать-NOM/GEN/ACC.1PL</ta>
            <ta e="T33" id="Seg_2831" s="T32">бог-EP-GEN</ta>
            <ta e="T34" id="Seg_2832" s="T33">мать-NOM/GEN.3SG</ta>
            <ta e="T35" id="Seg_2833" s="T34">вынимать-IMP.2SG.O</ta>
            <ta e="T36" id="Seg_2834" s="T35">мы.ACC</ta>
            <ta e="T37" id="Seg_2835" s="T36">бог-EP-GEN</ta>
            <ta e="T38" id="Seg_2836" s="T37">мать-NOM/GEN.3SG</ta>
            <ta e="T39" id="Seg_2837" s="T38">левый.[NOM.SG]</ta>
            <ta e="T40" id="Seg_2838" s="T39">волосы-ACC.3SG</ta>
            <ta e="T41" id="Seg_2839" s="T40">развязать-PST.[3SG]</ta>
            <ta e="T42" id="Seg_2840" s="T41">пускать-CVB</ta>
            <ta e="T43" id="Seg_2841" s="T42">дать-PST.[3SG]</ta>
            <ta e="T44" id="Seg_2842" s="T43">ребенок-PL-LAT</ta>
            <ta e="T45" id="Seg_2843" s="T44">NEG</ta>
            <ta e="T46" id="Seg_2844" s="T45">прибыть-CVB</ta>
            <ta e="T47" id="Seg_2845" s="T46">волосы-NOM/GEN.3SG</ta>
            <ta e="T48" id="Seg_2846" s="T47">правый.[NOM.SG]</ta>
            <ta e="T49" id="Seg_2847" s="T48">половина-NOM/GEN/ACC.3SG</ta>
            <ta e="T50" id="Seg_2848" s="T49">развязать-PST.[3SG]</ta>
            <ta e="T51" id="Seg_2849" s="T50">правый.[NOM.SG]</ta>
            <ta e="T52" id="Seg_2850" s="T51">волосы-NOM/GEN.3SG</ta>
            <ta e="T53" id="Seg_2851" s="T52">прибыть-PST</ta>
            <ta e="T54" id="Seg_2852" s="T53">ребенок-PL-LAT</ta>
            <ta e="T55" id="Seg_2853" s="T54">ребенок-PL</ta>
            <ta e="T56" id="Seg_2854" s="T55">собирать-PST.[3SG]</ta>
            <ta e="T57" id="Seg_2855" s="T56">схватить-CVB</ta>
            <ta e="T58" id="Seg_2856" s="T57">уйти-PST-3PL</ta>
            <ta e="T59" id="Seg_2857" s="T58">этот-ABL</ta>
            <ta e="T60" id="Seg_2858" s="T59">утес-GEN</ta>
            <ta e="T61" id="Seg_2859" s="T60">верх-LAT/LOC.3SG</ta>
            <ta e="T62" id="Seg_2860" s="T61">встать-TR-PST.[3SG]</ta>
            <ta e="T63" id="Seg_2861" s="T62">пускать-CVB</ta>
            <ta e="T64" id="Seg_2862" s="T63">дать-PST.[3SG]</ta>
            <ta e="T65" id="Seg_2863" s="T64">пойти-IMP.2PL</ta>
            <ta e="T66" id="Seg_2864" s="T65">дорога.[NOM.SG]</ta>
            <ta e="T67" id="Seg_2865" s="T66">вдоль</ta>
            <ta e="T68" id="Seg_2866" s="T67">идти-INCH-PST-3PL</ta>
            <ta e="T69" id="Seg_2867" s="T68">идти-PST-3PL</ta>
            <ta e="T70" id="Seg_2868" s="T69">идти-PST-3PL</ta>
            <ta e="T71" id="Seg_2869" s="T70">большой</ta>
            <ta e="T72" id="Seg_2870" s="T71">дорога.[NOM.SG]</ta>
            <ta e="T73" id="Seg_2871" s="T72">попасть-PST.[3SG]</ta>
            <ta e="T74" id="Seg_2872" s="T73">этот-PL</ta>
            <ta e="T75" id="Seg_2873" s="T74">дорога-GEN</ta>
            <ta e="T76" id="Seg_2874" s="T75">край-LAT/LOC.3SG</ta>
            <ta e="T77" id="Seg_2875" s="T76">сидеть-3PL</ta>
            <ta e="T78" id="Seg_2876" s="T77">видеть-CVB.TEMP-LAT/LOC.3PL</ta>
            <ta e="T79" id="Seg_2877" s="T78">гнедой.[NOM.SG]</ta>
            <ta e="T80" id="Seg_2878" s="T79">лошадь-ADJZ</ta>
            <ta e="T81" id="Seg_2879" s="T80">герой.[NOM.SG]</ta>
            <ta e="T82" id="Seg_2880" s="T81">прийти-DUR.[3SG]</ta>
            <ta e="T83" id="Seg_2881" s="T82">этот-PL</ta>
            <ta e="T84" id="Seg_2882" s="T83">свистеть-PST-3PL</ta>
            <ta e="T85" id="Seg_2883" s="T84">лошадь-NOM/GEN.3SG</ta>
            <ta e="T86" id="Seg_2884" s="T85">колено-LAT/LOC.3SG</ta>
            <ta e="T87" id="Seg_2885" s="T86">прийти-CVB</ta>
            <ta e="T88" id="Seg_2886" s="T87">спуститься-PST.[3SG]</ta>
            <ta e="T89" id="Seg_2887" s="T88">дух.помощник.[NOM.SG]</ta>
            <ta e="T90" id="Seg_2888" s="T89">Энейдене.[NOM.SG]</ta>
            <ta e="T91" id="Seg_2889" s="T90">свистеть-PRS.[3SG]</ta>
            <ta e="T92" id="Seg_2890" s="T91">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T93" id="Seg_2891" s="T92">NEG</ta>
            <ta e="T94" id="Seg_2892" s="T93">этот.[NOM.SG]</ta>
            <ta e="T95" id="Seg_2893" s="T94">пойти-PST.[3SG]</ta>
            <ta e="T96" id="Seg_2894" s="T95">идти-MOM-PST.[3SG]</ta>
            <ta e="T97" id="Seg_2895" s="T96">пройти-CVB</ta>
            <ta e="T98" id="Seg_2896" s="T97">опять</ta>
            <ta e="T99" id="Seg_2897" s="T98">черный.[NOM.SG]</ta>
            <ta e="T100" id="Seg_2898" s="T99">лошадь-ADJZ</ta>
            <ta e="T101" id="Seg_2899" s="T100">герой.[NOM.SG]</ta>
            <ta e="T102" id="Seg_2900" s="T101">прийти-DUR.[3SG]</ta>
            <ta e="T103" id="Seg_2901" s="T102">опять</ta>
            <ta e="T104" id="Seg_2902" s="T103">свистеть-MOM-PST-3PL</ta>
            <ta e="T105" id="Seg_2903" s="T104">лошадь-NOM/GEN.3SG</ta>
            <ta e="T106" id="Seg_2904" s="T105">опять</ta>
            <ta e="T107" id="Seg_2905" s="T106">колено-LAT/LOC.3SG</ta>
            <ta e="T108" id="Seg_2906" s="T107">прийти-CVB</ta>
            <ta e="T109" id="Seg_2907" s="T108">спуститься-PST.[3SG]</ta>
            <ta e="T110" id="Seg_2908" s="T109">опять</ta>
            <ta e="T111" id="Seg_2909" s="T110">дух.помощник.[NOM.SG]</ta>
            <ta e="T112" id="Seg_2910" s="T111">сказать-PRS.[3SG]</ta>
            <ta e="T113" id="Seg_2911" s="T112">Энейдене.[NOM.SG]</ta>
            <ta e="T114" id="Seg_2912" s="T113">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T115" id="Seg_2913" s="T114">свистеть-PRS.[3SG]</ta>
            <ta e="T116" id="Seg_2914" s="T115">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T117" id="Seg_2915" s="T116">опять</ta>
            <ta e="T118" id="Seg_2916" s="T117">пройти-MOM-PST.[3SG]</ta>
            <ta e="T119" id="Seg_2917" s="T118">этот.[NOM.SG]</ta>
            <ta e="T120" id="Seg_2918" s="T119">ребенок-PL</ta>
            <ta e="T121" id="Seg_2919" s="T120">кричать-PST-3PL</ta>
            <ta e="T122" id="Seg_2920" s="T121">кричать-PRS-3PL</ta>
            <ta e="T123" id="Seg_2921" s="T122">пойти-PTCP</ta>
            <ta e="T124" id="Seg_2922" s="T123">путь-NOM/GEN/ACC.2PL</ta>
            <ta e="T125" id="Seg_2923" s="T124">стать-IMP-3SG</ta>
            <ta e="T126" id="Seg_2924" s="T125">прийти-PTCP</ta>
            <ta e="T127" id="Seg_2925" s="T126">путь-NOM/GEN/ACC.2PL</ta>
            <ta e="T128" id="Seg_2926" s="T127">NEG.EX-IMP-3SG</ta>
            <ta e="T129" id="Seg_2927" s="T128">опять</ta>
            <ta e="T130" id="Seg_2928" s="T129">белый.[NOM.SG]</ta>
            <ta e="T131" id="Seg_2929" s="T130">лошадь-ADJZ</ta>
            <ta e="T132" id="Seg_2930" s="T131">герой.[NOM.SG]</ta>
            <ta e="T133" id="Seg_2931" s="T132">прийти-DUR.[3SG]</ta>
            <ta e="T134" id="Seg_2932" s="T133">этот-PL</ta>
            <ta e="T135" id="Seg_2933" s="T134">ребенок-PL</ta>
            <ta e="T136" id="Seg_2934" s="T135">опять</ta>
            <ta e="T137" id="Seg_2935" s="T136">свистеть-PST-3PL</ta>
            <ta e="T138" id="Seg_2936" s="T137">лошадь-NOM/GEN.3SG</ta>
            <ta e="T139" id="Seg_2937" s="T138">стоять-RES-PST.[3SG]</ta>
            <ta e="T140" id="Seg_2938" s="T139">тогда</ta>
            <ta e="T141" id="Seg_2939" s="T140">пойти-CVB</ta>
            <ta e="T142" id="Seg_2940" s="T141">видеть-PST.[3SG]</ta>
            <ta e="T143" id="Seg_2941" s="T142">какой</ta>
            <ta e="T144" id="Seg_2942" s="T143">ребенок-PL</ta>
            <ta e="T145" id="Seg_2943" s="T144">быть-PRS-2PL</ta>
            <ta e="T146" id="Seg_2944" s="T145">такой.[NOM.SG]</ta>
            <ta e="T147" id="Seg_2945" s="T146">такой.[NOM.SG]</ta>
            <ta e="T148" id="Seg_2946" s="T147">ребенок-PL</ta>
            <ta e="T149" id="Seg_2947" s="T148">быть-PRS-1PL</ta>
            <ta e="T150" id="Seg_2948" s="T149">ребенок-PL-LAT</ta>
            <ta e="T151" id="Seg_2949" s="T150">огонь.[NOM.SG]</ta>
            <ta e="T152" id="Seg_2950" s="T151">класть-PST.[3SG]</ta>
            <ta e="T153" id="Seg_2951" s="T152">пойти-PST.[3SG]</ta>
            <ta e="T154" id="Seg_2952" s="T153">лось.[NOM.SG]</ta>
            <ta e="T155" id="Seg_2953" s="T154">стрелять-PST.[3SG]</ta>
            <ta e="T156" id="Seg_2954" s="T155">принести-PST.[3SG]</ta>
            <ta e="T157" id="Seg_2955" s="T156">ребенок-PL-LAT</ta>
            <ta e="T158" id="Seg_2956" s="T157">лось-EP-GEN</ta>
            <ta e="T159" id="Seg_2957" s="T158">кожа-3SG-INS</ta>
            <ta e="T160" id="Seg_2958" s="T159">ребенок-PL-LAT</ta>
            <ta e="T161" id="Seg_2959" s="T160">чум.[NOM.SG]</ta>
            <ta e="T162" id="Seg_2960" s="T161">делать-PST.[3SG]</ta>
            <ta e="T163" id="Seg_2961" s="T162">жить-IMP.2PL</ta>
            <ta e="T164" id="Seg_2962" s="T163">ребенок-PL</ta>
            <ta e="T165" id="Seg_2963" s="T164">живой.[NOM.SG]</ta>
            <ta e="T166" id="Seg_2964" s="T165">быть-CVB.TEMP-LAT/LOC.1SG</ta>
            <ta e="T167" id="Seg_2965" s="T166">вернуться-CVB.TEMP-LAT/LOC.1SG</ta>
            <ta e="T168" id="Seg_2966" s="T167">взять-FUT-1SG</ta>
            <ta e="T169" id="Seg_2967" s="T168">вы.ACC</ta>
            <ta e="T170" id="Seg_2968" s="T169">так</ta>
            <ta e="T171" id="Seg_2969" s="T170">сказать-PST.[3SG]</ta>
            <ta e="T172" id="Seg_2970" s="T171">сам-NOM/GEN/ACC.3SG</ta>
            <ta e="T173" id="Seg_2971" s="T172">идти-MOM-PST.[3SG]</ta>
            <ta e="T174" id="Seg_2972" s="T173">и</ta>
            <ta e="T175" id="Seg_2973" s="T174">три-ORD.[NOM.SG]</ta>
            <ta e="T176" id="Seg_2974" s="T175">день-GEN</ta>
            <ta e="T177" id="Seg_2975" s="T176">этот.[NOM.SG]</ta>
            <ta e="T178" id="Seg_2976" s="T177">два.[NOM.SG]</ta>
            <ta e="T179" id="Seg_2977" s="T178">герой-EP-GEN</ta>
            <ta e="T180" id="Seg_2978" s="T179">сам.[NOM.SG]</ta>
            <ta e="T181" id="Seg_2979" s="T180">лошадь-PL-NOM/GEN/ACC.3PL</ta>
            <ta e="T182" id="Seg_2980" s="T181">вернуться-DUR-PRS-3PL</ta>
            <ta e="T183" id="Seg_2981" s="T182">седло-PL-NOM/GEN/ACC.3PL</ta>
            <ta e="T184" id="Seg_2982" s="T183">стремя-PL-NOM/GEN/ACC.3PL</ta>
            <ta e="T185" id="Seg_2983" s="T184">лошадь-GEN</ta>
            <ta e="T186" id="Seg_2984" s="T185">живот-LAT/LOC.3SG</ta>
            <ta e="T187" id="Seg_2985" s="T186">болтаться-CVB</ta>
            <ta e="T188" id="Seg_2986" s="T187">идти-PRS-3PL</ta>
            <ta e="T189" id="Seg_2987" s="T188">этот-GEN.PL</ta>
            <ta e="T190" id="Seg_2988" s="T189">позади-ABL.3SG</ta>
            <ta e="T191" id="Seg_2989" s="T190">белый.[NOM.SG]</ta>
            <ta e="T192" id="Seg_2990" s="T191">лошадь-ADJZ</ta>
            <ta e="T193" id="Seg_2991" s="T192">вернуться-DUR.[3SG]</ta>
            <ta e="T194" id="Seg_2992" s="T193">прийти-PST.[3SG]</ta>
            <ta e="T195" id="Seg_2993" s="T194">ребенок-PL-LAT/LOC.3PL</ta>
            <ta e="T196" id="Seg_2994" s="T195">взять-PST.[3SG]</ta>
            <ta e="T197" id="Seg_2995" s="T196">ребенок-ACC</ta>
            <ta e="T198" id="Seg_2996" s="T197">чум-LAT/LOC.3SG</ta>
            <ta e="T199" id="Seg_2997" s="T198">нести-CVB</ta>
            <ta e="T200" id="Seg_2998" s="T199">пойти-PST.[3SG]</ta>
            <ta e="T201" id="Seg_2999" s="T200">сказать-PRS.[3SG]</ta>
            <ta e="T202" id="Seg_3000" s="T201">я.NOM</ta>
            <ta e="T203" id="Seg_3001" s="T202">жить-FUT-2PL=QP</ta>
            <ta e="T204" id="Seg_3002" s="T203">или</ta>
            <ta e="T205" id="Seg_3003" s="T204">чум-LAT/LOC.2PL</ta>
            <ta e="T206" id="Seg_3004" s="T205">пойти-FUT-2PL=QP</ta>
            <ta e="T207" id="Seg_3005" s="T206">один.[NOM.SG]</ta>
            <ta e="T208" id="Seg_3006" s="T207">сказать-PRS.[3SG]</ta>
            <ta e="T209" id="Seg_3007" s="T208">я.NOM</ta>
            <ta e="T210" id="Seg_3008" s="T209">пойти-FUT-1SG</ta>
            <ta e="T211" id="Seg_3009" s="T210">один.[NOM.SG]</ta>
            <ta e="T212" id="Seg_3010" s="T211">там</ta>
            <ta e="T213" id="Seg_3011" s="T212">остаться-PST.[3SG]</ta>
            <ta e="T214" id="Seg_3012" s="T213">герой-ADJZ.[NOM.SG]</ta>
            <ta e="T215" id="Seg_3013" s="T214">лошадь.[NOM.SG]</ta>
            <ta e="T216" id="Seg_3014" s="T215">седло-TR-CVB</ta>
            <ta e="T217" id="Seg_3015" s="T216">дать-PST.[3SG]</ta>
            <ta e="T218" id="Seg_3016" s="T217">идти-MOM-PST.[3SG]</ta>
            <ta e="T219" id="Seg_3017" s="T218">чум-LAT/LOC.3SG</ta>
            <ta e="T220" id="Seg_3018" s="T219">прийти-PST.[3SG]</ta>
            <ta e="T221" id="Seg_3019" s="T220">один.[NOM.SG]</ta>
            <ta e="T222" id="Seg_3020" s="T221">чум.[NOM.SG]</ta>
            <ta e="T223" id="Seg_3021" s="T222">стоять-PRS.[3SG]</ta>
            <ta e="T224" id="Seg_3022" s="T223">деревня-NOM/GEN.3SG</ta>
            <ta e="T225" id="Seg_3023" s="T224">как</ta>
            <ta e="T226" id="Seg_3024" s="T225">раньше</ta>
            <ta e="T227" id="Seg_3025" s="T226">NEG.EX.[3SG]</ta>
            <ta e="T228" id="Seg_3026" s="T227">этот.[NOM.SG]</ta>
            <ta e="T229" id="Seg_3027" s="T228">чум-LAT</ta>
            <ta e="T230" id="Seg_3028" s="T229">войти-PST.[3SG]</ta>
            <ta e="T231" id="Seg_3029" s="T230">спросить-PRS-3SG.O</ta>
            <ta e="T232" id="Seg_3030" s="T231">старый.[NOM.SG]</ta>
            <ta e="T233" id="Seg_3031" s="T232">женщина.[NOM.SG]</ta>
            <ta e="T234" id="Seg_3032" s="T233">старый.[NOM.SG]</ta>
            <ta e="T235" id="Seg_3033" s="T234">мужчина.[NOM.SG]</ta>
            <ta e="T236" id="Seg_3034" s="T235">деревня-NOM/GEN/ACC.2PL</ta>
            <ta e="T237" id="Seg_3035" s="T236">куда.идти-PST.[3SG]</ta>
            <ta e="T238" id="Seg_3036" s="T237">этот-PL</ta>
            <ta e="T239" id="Seg_3037" s="T238">сказать-PRS-3PL</ta>
            <ta e="T240" id="Seg_3038" s="T239">два.[NOM.SG]</ta>
            <ta e="T241" id="Seg_3039" s="T240">сын-NOM/GEN/ACC.1PL</ta>
            <ta e="T242" id="Seg_3040" s="T241">быть-PST.[3SG]</ta>
            <ta e="T243" id="Seg_3041" s="T242">люди.[NOM.SG]</ta>
            <ta e="T244" id="Seg_3042" s="T243">плот-LAT</ta>
            <ta e="T245" id="Seg_3043" s="T244">завязать-CVB.ANT</ta>
            <ta e="T246" id="Seg_3044" s="T245">пускать-CVB</ta>
            <ta e="T247" id="Seg_3045" s="T246">дать-PST.[3SG]</ta>
            <ta e="T248" id="Seg_3046" s="T247">вода-LAT</ta>
            <ta e="T249" id="Seg_3047" s="T248">этот-GEN.PL</ta>
            <ta e="T250" id="Seg_3048" s="T249">после-LAT/LOC.3SG</ta>
            <ta e="T251" id="Seg_3049" s="T250">дочь-NOM/GEN/ACC.1PL</ta>
            <ta e="T252" id="Seg_3050" s="T251">быть-PST.[3SG]</ta>
            <ta e="T253" id="Seg_3051" s="T252">этот.[NOM.SG]</ta>
            <ta e="T254" id="Seg_3052" s="T253">люди-ACC</ta>
            <ta e="T255" id="Seg_3053" s="T254">весь</ta>
            <ta e="T256" id="Seg_3054" s="T255">убить-CVB</ta>
            <ta e="T257" id="Seg_3055" s="T256">нести-PST.[3SG]</ta>
            <ta e="T258" id="Seg_3056" s="T257">а</ta>
            <ta e="T259" id="Seg_3057" s="T258">где</ta>
            <ta e="T260" id="Seg_3058" s="T259">дочь-NOM/GEN/ACC.2PL</ta>
            <ta e="T261" id="Seg_3059" s="T260">куда.идти-PST.[3SG]</ta>
            <ta e="T262" id="Seg_3060" s="T261">сам-NOM/GEN/ACC.1PL</ta>
            <ta e="T263" id="Seg_3061" s="T262">NEG</ta>
            <ta e="T264" id="Seg_3062" s="T263">знать-1PL</ta>
            <ta e="T265" id="Seg_3063" s="T264">этот.[NOM.SG]</ta>
            <ta e="T266" id="Seg_3064" s="T265">мальчик.[NOM.SG]</ta>
            <ta e="T267" id="Seg_3065" s="T266">лошадь-ACC.3SG</ta>
            <ta e="T268" id="Seg_3066" s="T267">садиться.на.лошадь-PST.[3SG]</ta>
            <ta e="T269" id="Seg_3067" s="T268">искать-CVB</ta>
            <ta e="T270" id="Seg_3068" s="T269">пойти-CVB</ta>
            <ta e="T271" id="Seg_3069" s="T270">исчезнуть-PST.[3SG]</ta>
            <ta e="T272" id="Seg_3070" s="T271">пойти-PST.[3SG]</ta>
            <ta e="T273" id="Seg_3071" s="T272">большой.[NOM.SG]</ta>
            <ta e="T274" id="Seg_3072" s="T273">степь-LAT</ta>
            <ta e="T275" id="Seg_3073" s="T274">там</ta>
            <ta e="T276" id="Seg_3074" s="T275">скакать-FRQ-TR-CVB</ta>
            <ta e="T277" id="Seg_3075" s="T276">идти-DUR-PST.[3SG]</ta>
            <ta e="T278" id="Seg_3076" s="T277">этот.[NOM.SG]</ta>
            <ta e="T279" id="Seg_3077" s="T278">мальчик-ACC</ta>
            <ta e="T280" id="Seg_3078" s="T279">видеть-CVB.ANT</ta>
            <ta e="T281" id="Seg_3079" s="T280">скакать-FRQ-TR-CVB</ta>
            <ta e="T282" id="Seg_3080" s="T281">прийти-PST.[3SG]</ta>
            <ta e="T283" id="Seg_3081" s="T282">прыгнуть-CVB</ta>
            <ta e="T284" id="Seg_3082" s="T283">схватить-PST.[3SG]</ta>
            <ta e="T285" id="Seg_3083" s="T284">этот.[NOM.SG]</ta>
            <ta e="T286" id="Seg_3084" s="T285">мальчик-LAT</ta>
            <ta e="T287" id="Seg_3085" s="T286">тогда</ta>
            <ta e="T288" id="Seg_3086" s="T287">бороться-PST-3PL</ta>
            <ta e="T289" id="Seg_3087" s="T288">брат-NOM/GEN.3SG</ta>
            <ta e="T290" id="Seg_3088" s="T289">спустить-MOM-PST</ta>
            <ta e="T291" id="Seg_3089" s="T290">этот-ACC</ta>
            <ta e="T292" id="Seg_3090" s="T291">держать-DUR.[3SG]</ta>
            <ta e="T293" id="Seg_3091" s="T292">сказать-DUR.[3SG]</ta>
            <ta e="T294" id="Seg_3092" s="T293">я.NOM</ta>
            <ta e="T295" id="Seg_3093" s="T294">ты.GEN</ta>
            <ta e="T296" id="Seg_3094" s="T295">брат-NOM/GEN/ACC.2SG</ta>
            <ta e="T297" id="Seg_3095" s="T296">быть-PRS-1SG</ta>
            <ta e="T298" id="Seg_3096" s="T297">NEG</ta>
            <ta e="T299" id="Seg_3097" s="T298">бороться-IMP.2SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_3098" s="T0">n-n:case</ta>
            <ta e="T2" id="Seg_3099" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_3100" s="T2">v-v:tense-v:pn</ta>
            <ta e="T4" id="Seg_3101" s="T3">num-n:case</ta>
            <ta e="T5" id="Seg_3102" s="T4">n-n:case.poss</ta>
            <ta e="T6" id="Seg_3103" s="T5">v-v:tense-v:pn</ta>
            <ta e="T7" id="Seg_3104" s="T6">dempro-n:case</ta>
            <ta e="T8" id="Seg_3105" s="T7">n-n:num-n:case.poss</ta>
            <ta e="T9" id="Seg_3106" s="T8">v-v:n.fin-v:(case.poss)</ta>
            <ta e="T10" id="Seg_3107" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_3108" s="T10">n-n:case.poss</ta>
            <ta e="T12" id="Seg_3109" s="T11">adv</ta>
            <ta e="T13" id="Seg_3110" s="T12">v-v:tense-v:pn</ta>
            <ta e="T14" id="Seg_3111" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_3112" s="T14">v-v:tense-v:pn</ta>
            <ta e="T16" id="Seg_3113" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_3114" s="T16">v-v:tense-v:pn</ta>
            <ta e="T18" id="Seg_3115" s="T17">v-v:n.fin</ta>
            <ta e="T19" id="Seg_3116" s="T18">v-v:tense-v:pn</ta>
            <ta e="T20" id="Seg_3117" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_3118" s="T20">n-n:case</ta>
            <ta e="T22" id="Seg_3119" s="T21">v-v:n.fin</ta>
            <ta e="T23" id="Seg_3120" s="T22">v-v&gt;v-v:pn</ta>
            <ta e="T24" id="Seg_3121" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_3122" s="T24">n-n:case.poss</ta>
            <ta e="T26" id="Seg_3123" s="T25">n-n:case</ta>
            <ta e="T27" id="Seg_3124" s="T26">v-v&gt;v-v:pn</ta>
            <ta e="T28" id="Seg_3125" s="T27">dempro-n:num</ta>
            <ta e="T29" id="Seg_3126" s="T28">v-v:tense-v:pn</ta>
            <ta e="T30" id="Seg_3127" s="T29">n-n:ins-n:case</ta>
            <ta e="T31" id="Seg_3128" s="T30">n-n:case.poss</ta>
            <ta e="T32" id="Seg_3129" s="T31">n-n:case.poss</ta>
            <ta e="T33" id="Seg_3130" s="T32">n-n:ins-n:case</ta>
            <ta e="T34" id="Seg_3131" s="T33">n-n:case.poss</ta>
            <ta e="T35" id="Seg_3132" s="T34">v-v:mood.pn</ta>
            <ta e="T36" id="Seg_3133" s="T35">pers</ta>
            <ta e="T37" id="Seg_3134" s="T36">n-n:ins-n:case</ta>
            <ta e="T38" id="Seg_3135" s="T37">n-n:case.poss</ta>
            <ta e="T39" id="Seg_3136" s="T38">adj-n:case</ta>
            <ta e="T40" id="Seg_3137" s="T39">n-n:case.poss</ta>
            <ta e="T41" id="Seg_3138" s="T40">v-v:tense-v:pn</ta>
            <ta e="T42" id="Seg_3139" s="T41">v-v:n.fin</ta>
            <ta e="T43" id="Seg_3140" s="T42">v-v:tense-v:pn</ta>
            <ta e="T44" id="Seg_3141" s="T43">n-n:num-n:case</ta>
            <ta e="T45" id="Seg_3142" s="T44">ptcl</ta>
            <ta e="T46" id="Seg_3143" s="T45">v-v:n.fin</ta>
            <ta e="T47" id="Seg_3144" s="T46">n-n:case.poss</ta>
            <ta e="T48" id="Seg_3145" s="T47">adj-n:case</ta>
            <ta e="T49" id="Seg_3146" s="T48">n-n:case.poss</ta>
            <ta e="T50" id="Seg_3147" s="T49">v-v:tense-v:pn</ta>
            <ta e="T51" id="Seg_3148" s="T50">adj-n:case</ta>
            <ta e="T52" id="Seg_3149" s="T51">n-n:case.poss</ta>
            <ta e="T53" id="Seg_3150" s="T52">v-v:tense</ta>
            <ta e="T54" id="Seg_3151" s="T53">n-n:num-n:case</ta>
            <ta e="T55" id="Seg_3152" s="T54">n-n:num</ta>
            <ta e="T56" id="Seg_3153" s="T55">v-v:tense-v:pn</ta>
            <ta e="T57" id="Seg_3154" s="T56">v-v:n.fin</ta>
            <ta e="T58" id="Seg_3155" s="T57">v-v:tense-v:pn</ta>
            <ta e="T59" id="Seg_3156" s="T58">dempro-n:case</ta>
            <ta e="T60" id="Seg_3157" s="T59">n-n:case</ta>
            <ta e="T61" id="Seg_3158" s="T60">n-n:case.poss</ta>
            <ta e="T62" id="Seg_3159" s="T61">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T63" id="Seg_3160" s="T62">v-v:n.fin</ta>
            <ta e="T64" id="Seg_3161" s="T63">v-v:tense-v:pn</ta>
            <ta e="T65" id="Seg_3162" s="T64">v-v:mood.pn</ta>
            <ta e="T66" id="Seg_3163" s="T65">n-n:case</ta>
            <ta e="T67" id="Seg_3164" s="T66">post</ta>
            <ta e="T68" id="Seg_3165" s="T67">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T69" id="Seg_3166" s="T68">v-v:tense-v:pn</ta>
            <ta e="T70" id="Seg_3167" s="T69">v-v:tense-v:pn</ta>
            <ta e="T71" id="Seg_3168" s="T70">adj</ta>
            <ta e="T72" id="Seg_3169" s="T71">n-n:case</ta>
            <ta e="T73" id="Seg_3170" s="T72">v-v:tense-v:pn</ta>
            <ta e="T74" id="Seg_3171" s="T73">dempro-n:num</ta>
            <ta e="T75" id="Seg_3172" s="T74">n-n:case</ta>
            <ta e="T76" id="Seg_3173" s="T75">n-n:case.poss</ta>
            <ta e="T77" id="Seg_3174" s="T76">v-v:pn</ta>
            <ta e="T78" id="Seg_3175" s="T77">v-v:n.fin-v:(case.poss)</ta>
            <ta e="T79" id="Seg_3176" s="T78">adj-n:case</ta>
            <ta e="T80" id="Seg_3177" s="T79">n-n&gt;adj</ta>
            <ta e="T81" id="Seg_3178" s="T80">n-n:case</ta>
            <ta e="T82" id="Seg_3179" s="T81">v-v&gt;v-v:pn</ta>
            <ta e="T83" id="Seg_3180" s="T82">dempro-n:num</ta>
            <ta e="T84" id="Seg_3181" s="T83">v-v:tense-v:pn</ta>
            <ta e="T85" id="Seg_3182" s="T84">n-n:case.poss</ta>
            <ta e="T86" id="Seg_3183" s="T85">n-n:case.poss</ta>
            <ta e="T87" id="Seg_3184" s="T86">v-v:n.fin</ta>
            <ta e="T88" id="Seg_3185" s="T87">v-v:tense-v:pn</ta>
            <ta e="T89" id="Seg_3186" s="T88">n-n:case</ta>
            <ta e="T90" id="Seg_3187" s="T89">propr-n:case</ta>
            <ta e="T91" id="Seg_3188" s="T90">v-v:tense-v:pn</ta>
            <ta e="T92" id="Seg_3189" s="T91">refl-n:case.poss</ta>
            <ta e="T93" id="Seg_3190" s="T92">ptcl</ta>
            <ta e="T94" id="Seg_3191" s="T93">dempro-n:case</ta>
            <ta e="T95" id="Seg_3192" s="T94">v-v:tense-v:pn</ta>
            <ta e="T96" id="Seg_3193" s="T95">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T97" id="Seg_3194" s="T96">v-v:n.fin</ta>
            <ta e="T98" id="Seg_3195" s="T97">adv</ta>
            <ta e="T99" id="Seg_3196" s="T98">adj-n:case</ta>
            <ta e="T100" id="Seg_3197" s="T99">n-n&gt;adj</ta>
            <ta e="T101" id="Seg_3198" s="T100">n-n:case</ta>
            <ta e="T102" id="Seg_3199" s="T101">v-v&gt;v-v:pn</ta>
            <ta e="T103" id="Seg_3200" s="T102">adv</ta>
            <ta e="T104" id="Seg_3201" s="T103">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T105" id="Seg_3202" s="T104">n-n:case.poss</ta>
            <ta e="T106" id="Seg_3203" s="T105">adv</ta>
            <ta e="T107" id="Seg_3204" s="T106">n-n:case.poss</ta>
            <ta e="T108" id="Seg_3205" s="T107">v-v:n.fin</ta>
            <ta e="T109" id="Seg_3206" s="T108">v-v:tense-v:pn</ta>
            <ta e="T110" id="Seg_3207" s="T109">adv</ta>
            <ta e="T111" id="Seg_3208" s="T110">n-n:case</ta>
            <ta e="T112" id="Seg_3209" s="T111">v-v:tense-v:pn</ta>
            <ta e="T113" id="Seg_3210" s="T112">propr-n:case</ta>
            <ta e="T114" id="Seg_3211" s="T113">refl-n:case.poss</ta>
            <ta e="T115" id="Seg_3212" s="T114">v-v:tense-v:pn</ta>
            <ta e="T116" id="Seg_3213" s="T115">refl-n:case.poss</ta>
            <ta e="T117" id="Seg_3214" s="T116">adv</ta>
            <ta e="T118" id="Seg_3215" s="T117">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T119" id="Seg_3216" s="T118">dempro-n:case</ta>
            <ta e="T120" id="Seg_3217" s="T119">n-n:num</ta>
            <ta e="T121" id="Seg_3218" s="T120">v-v:tense-v:pn</ta>
            <ta e="T122" id="Seg_3219" s="T121">v-v:tense-v:pn</ta>
            <ta e="T123" id="Seg_3220" s="T122">v-v:n.fin</ta>
            <ta e="T124" id="Seg_3221" s="T123">n-n:case.poss</ta>
            <ta e="T125" id="Seg_3222" s="T124">v-v:mood-v:pn</ta>
            <ta e="T126" id="Seg_3223" s="T125">v-v:n.fin</ta>
            <ta e="T127" id="Seg_3224" s="T126">n-n:case.poss</ta>
            <ta e="T128" id="Seg_3225" s="T127">v-v:mood-v:pn</ta>
            <ta e="T129" id="Seg_3226" s="T128">adv</ta>
            <ta e="T130" id="Seg_3227" s="T129">adj-n:case</ta>
            <ta e="T131" id="Seg_3228" s="T130">n-n&gt;adj</ta>
            <ta e="T132" id="Seg_3229" s="T131">n-n:case</ta>
            <ta e="T133" id="Seg_3230" s="T132">v-v&gt;v-v:pn</ta>
            <ta e="T134" id="Seg_3231" s="T133">dempro-n:num</ta>
            <ta e="T135" id="Seg_3232" s="T134">n-n:num</ta>
            <ta e="T136" id="Seg_3233" s="T135">adv</ta>
            <ta e="T137" id="Seg_3234" s="T136">v-v:tense-v:pn</ta>
            <ta e="T138" id="Seg_3235" s="T137">n-n:case.poss</ta>
            <ta e="T139" id="Seg_3236" s="T138">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T140" id="Seg_3237" s="T139">adv</ta>
            <ta e="T141" id="Seg_3238" s="T140">v-v:n.fin</ta>
            <ta e="T142" id="Seg_3239" s="T141">v-v:tense-v:pn</ta>
            <ta e="T143" id="Seg_3240" s="T142">que</ta>
            <ta e="T144" id="Seg_3241" s="T143">n-n:num</ta>
            <ta e="T145" id="Seg_3242" s="T144">v-v:tense-v:pn</ta>
            <ta e="T146" id="Seg_3243" s="T145">adj-n:case</ta>
            <ta e="T147" id="Seg_3244" s="T146">adj-n:case</ta>
            <ta e="T148" id="Seg_3245" s="T147">n-n:num</ta>
            <ta e="T149" id="Seg_3246" s="T148">v-v:tense-v:pn</ta>
            <ta e="T150" id="Seg_3247" s="T149">n-n:num-n:case</ta>
            <ta e="T151" id="Seg_3248" s="T150">n-n:case</ta>
            <ta e="T152" id="Seg_3249" s="T151">v-v:tense-v:pn</ta>
            <ta e="T153" id="Seg_3250" s="T152">v-v:tense-v:pn</ta>
            <ta e="T154" id="Seg_3251" s="T153">n-n:case</ta>
            <ta e="T155" id="Seg_3252" s="T154">v-v:tense-v:pn</ta>
            <ta e="T156" id="Seg_3253" s="T155">v-v:tense-v:pn</ta>
            <ta e="T157" id="Seg_3254" s="T156">n-n:num-n:case</ta>
            <ta e="T158" id="Seg_3255" s="T157">n-n:ins-n:case</ta>
            <ta e="T159" id="Seg_3256" s="T158">n-n:case.poss-n:case</ta>
            <ta e="T160" id="Seg_3257" s="T159">n-n:num-n:case</ta>
            <ta e="T161" id="Seg_3258" s="T160">n-n:case</ta>
            <ta e="T162" id="Seg_3259" s="T161">v-v:tense-v:pn</ta>
            <ta e="T163" id="Seg_3260" s="T162">v-v:mood.pn</ta>
            <ta e="T164" id="Seg_3261" s="T163">n-n:num</ta>
            <ta e="T165" id="Seg_3262" s="T164">adj-n:case</ta>
            <ta e="T166" id="Seg_3263" s="T165">v-v:n.fin-v:(case.poss)</ta>
            <ta e="T167" id="Seg_3264" s="T166">v-v:n.fin-v:(case.poss)</ta>
            <ta e="T168" id="Seg_3265" s="T167">v-v:tense-v:pn</ta>
            <ta e="T169" id="Seg_3266" s="T168">pers</ta>
            <ta e="T170" id="Seg_3267" s="T169">ptcl</ta>
            <ta e="T171" id="Seg_3268" s="T170">v-v:tense-v:pn</ta>
            <ta e="T172" id="Seg_3269" s="T171">refl-n:case.poss</ta>
            <ta e="T173" id="Seg_3270" s="T172">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T174" id="Seg_3271" s="T173">conj</ta>
            <ta e="T175" id="Seg_3272" s="T174">num-num&gt;num-n:case</ta>
            <ta e="T176" id="Seg_3273" s="T175">n-n:case</ta>
            <ta e="T177" id="Seg_3274" s="T176">dempro-n:case</ta>
            <ta e="T178" id="Seg_3275" s="T177">num-n:case</ta>
            <ta e="T179" id="Seg_3276" s="T178">n-n:ins-n:case</ta>
            <ta e="T180" id="Seg_3277" s="T179">refl-n:case</ta>
            <ta e="T181" id="Seg_3278" s="T180">n-n:num-n:case.poss</ta>
            <ta e="T182" id="Seg_3279" s="T181">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T183" id="Seg_3280" s="T182">n-n:num-n:case.poss</ta>
            <ta e="T184" id="Seg_3281" s="T183">n-n:num-n:case.poss</ta>
            <ta e="T185" id="Seg_3282" s="T184">n-n:case</ta>
            <ta e="T186" id="Seg_3283" s="T185">n-n:case.poss</ta>
            <ta e="T187" id="Seg_3284" s="T186">v-v:n.fin</ta>
            <ta e="T188" id="Seg_3285" s="T187">v-v:tense-v:pn</ta>
            <ta e="T189" id="Seg_3286" s="T188">dempro-n:case</ta>
            <ta e="T190" id="Seg_3287" s="T189">adv-n:case.poss</ta>
            <ta e="T191" id="Seg_3288" s="T190">adj-n:case</ta>
            <ta e="T192" id="Seg_3289" s="T191">n-n&gt;adj</ta>
            <ta e="T193" id="Seg_3290" s="T192">v-v&gt;v-v:pn</ta>
            <ta e="T194" id="Seg_3291" s="T193">v-v:tense-v:pn</ta>
            <ta e="T195" id="Seg_3292" s="T194">n-n:num-n:case.poss</ta>
            <ta e="T196" id="Seg_3293" s="T195">v-v:tense-v:pn</ta>
            <ta e="T197" id="Seg_3294" s="T196">n-n:case</ta>
            <ta e="T198" id="Seg_3295" s="T197">n-n:case.poss</ta>
            <ta e="T199" id="Seg_3296" s="T198">v-v:n.fin</ta>
            <ta e="T200" id="Seg_3297" s="T199">v-v:tense-v:pn</ta>
            <ta e="T201" id="Seg_3298" s="T200">v-v:tense-v:pn</ta>
            <ta e="T202" id="Seg_3299" s="T201">pers</ta>
            <ta e="T203" id="Seg_3300" s="T202">v-v:tense-v:pn=ptcl</ta>
            <ta e="T204" id="Seg_3301" s="T203">conj</ta>
            <ta e="T205" id="Seg_3302" s="T204">n-n:case.poss</ta>
            <ta e="T206" id="Seg_3303" s="T205">v-v:tense-v:pn=ptcl</ta>
            <ta e="T207" id="Seg_3304" s="T206">num-n:case</ta>
            <ta e="T208" id="Seg_3305" s="T207">v-v:tense-v:pn</ta>
            <ta e="T209" id="Seg_3306" s="T208">pers</ta>
            <ta e="T210" id="Seg_3307" s="T209">v-v:tense-v:pn</ta>
            <ta e="T211" id="Seg_3308" s="T210">num-n:case</ta>
            <ta e="T212" id="Seg_3309" s="T211">adv</ta>
            <ta e="T213" id="Seg_3310" s="T212">v-v:tense-v:pn</ta>
            <ta e="T214" id="Seg_3311" s="T213">n-n&gt;adj-n:case</ta>
            <ta e="T215" id="Seg_3312" s="T214">n-n:case</ta>
            <ta e="T216" id="Seg_3313" s="T215">n-n&gt;v-v:n.fin</ta>
            <ta e="T217" id="Seg_3314" s="T216">v-v:tense-v:pn</ta>
            <ta e="T218" id="Seg_3315" s="T217">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T219" id="Seg_3316" s="T218">n-n:case.poss</ta>
            <ta e="T220" id="Seg_3317" s="T219">v-v:tense-v:pn</ta>
            <ta e="T221" id="Seg_3318" s="T220">adj-n:case</ta>
            <ta e="T222" id="Seg_3319" s="T221">n-n:case</ta>
            <ta e="T223" id="Seg_3320" s="T222">v-v:tense-v:pn</ta>
            <ta e="T224" id="Seg_3321" s="T223">n-n:case.poss</ta>
            <ta e="T225" id="Seg_3322" s="T224">ptcl</ta>
            <ta e="T226" id="Seg_3323" s="T225">adv</ta>
            <ta e="T227" id="Seg_3324" s="T226">v-v:pn</ta>
            <ta e="T228" id="Seg_3325" s="T227">dempro-n:case</ta>
            <ta e="T229" id="Seg_3326" s="T228">n-n:case</ta>
            <ta e="T230" id="Seg_3327" s="T229">v-v:tense-v:pn</ta>
            <ta e="T231" id="Seg_3328" s="T230">v-v:tense-v:pn</ta>
            <ta e="T232" id="Seg_3329" s="T231">adj-n:case</ta>
            <ta e="T233" id="Seg_3330" s="T232">n-n:case</ta>
            <ta e="T234" id="Seg_3331" s="T233">adj-n:case</ta>
            <ta e="T235" id="Seg_3332" s="T234">n-n:case</ta>
            <ta e="T236" id="Seg_3333" s="T235">n-n:case.poss</ta>
            <ta e="T237" id="Seg_3334" s="T236">v-v:tense-v:pn</ta>
            <ta e="T238" id="Seg_3335" s="T237">dempro-n:num</ta>
            <ta e="T239" id="Seg_3336" s="T238">v-v:tense-v:pn</ta>
            <ta e="T240" id="Seg_3337" s="T239">num-n:case</ta>
            <ta e="T241" id="Seg_3338" s="T240">n-n:case.poss</ta>
            <ta e="T242" id="Seg_3339" s="T241">v-v:tense-v:pn</ta>
            <ta e="T243" id="Seg_3340" s="T242">n-n:case</ta>
            <ta e="T244" id="Seg_3341" s="T243">n-n:case</ta>
            <ta e="T245" id="Seg_3342" s="T244">v-v:n.fin</ta>
            <ta e="T246" id="Seg_3343" s="T245">v-v:n.fin</ta>
            <ta e="T247" id="Seg_3344" s="T246">v-v:tense-v:pn</ta>
            <ta e="T248" id="Seg_3345" s="T247">n-n:case</ta>
            <ta e="T249" id="Seg_3346" s="T248">dempro-n:case</ta>
            <ta e="T250" id="Seg_3347" s="T249">adv-n:case.poss</ta>
            <ta e="T251" id="Seg_3348" s="T250">n-n:case.poss</ta>
            <ta e="T252" id="Seg_3349" s="T251">v-v:tense-v:pn</ta>
            <ta e="T253" id="Seg_3350" s="T252">dempro-n:case</ta>
            <ta e="T254" id="Seg_3351" s="T253">n-n:case</ta>
            <ta e="T255" id="Seg_3352" s="T254">quant</ta>
            <ta e="T256" id="Seg_3353" s="T255">v-v:n.fin</ta>
            <ta e="T257" id="Seg_3354" s="T256">v-v:tense-v:pn</ta>
            <ta e="T258" id="Seg_3355" s="T257">conj</ta>
            <ta e="T259" id="Seg_3356" s="T258">que</ta>
            <ta e="T260" id="Seg_3357" s="T259">n-n:case.poss</ta>
            <ta e="T261" id="Seg_3358" s="T260">v-v:tense-v:pn</ta>
            <ta e="T262" id="Seg_3359" s="T261">refl-n:case.poss</ta>
            <ta e="T263" id="Seg_3360" s="T262">ptcl</ta>
            <ta e="T264" id="Seg_3361" s="T263">v-v:pn</ta>
            <ta e="T265" id="Seg_3362" s="T264">dempro-n:case</ta>
            <ta e="T266" id="Seg_3363" s="T265">n-n:case</ta>
            <ta e="T267" id="Seg_3364" s="T266">n-n:case.poss</ta>
            <ta e="T268" id="Seg_3365" s="T267">v-v:tense-v:pn</ta>
            <ta e="T269" id="Seg_3366" s="T268">v-v:n.fin</ta>
            <ta e="T270" id="Seg_3367" s="T269">v-v:n.fin</ta>
            <ta e="T271" id="Seg_3368" s="T270">v-v:tense-v:pn</ta>
            <ta e="T272" id="Seg_3369" s="T271">v-v:tense-v:pn</ta>
            <ta e="T273" id="Seg_3370" s="T272">adj-n:case</ta>
            <ta e="T274" id="Seg_3371" s="T273">n-n:case</ta>
            <ta e="T275" id="Seg_3372" s="T274">adv</ta>
            <ta e="T276" id="Seg_3373" s="T275">v-v&gt;v-v&gt;v-v:n.fin</ta>
            <ta e="T277" id="Seg_3374" s="T276">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T278" id="Seg_3375" s="T277">dempro-n:case</ta>
            <ta e="T279" id="Seg_3376" s="T278">n-n:case</ta>
            <ta e="T280" id="Seg_3377" s="T279">v-v:n.fin</ta>
            <ta e="T281" id="Seg_3378" s="T280">v-v&gt;v-v&gt;v-v:n.fin</ta>
            <ta e="T282" id="Seg_3379" s="T281">v-v:tense-v:pn</ta>
            <ta e="T283" id="Seg_3380" s="T282">v-v:n.fin</ta>
            <ta e="T284" id="Seg_3381" s="T283">v-v:tense-v:pn</ta>
            <ta e="T285" id="Seg_3382" s="T284">dempro-n:case</ta>
            <ta e="T286" id="Seg_3383" s="T285">n-n:case</ta>
            <ta e="T287" id="Seg_3384" s="T286">adv</ta>
            <ta e="T288" id="Seg_3385" s="T287">v-v:tense-v:pn</ta>
            <ta e="T289" id="Seg_3386" s="T288">n-n:case.poss</ta>
            <ta e="T290" id="Seg_3387" s="T289">v-v&gt;v-v:tense</ta>
            <ta e="T291" id="Seg_3388" s="T290">dempro-n:case</ta>
            <ta e="T292" id="Seg_3389" s="T291">v-v&gt;v-v:pn</ta>
            <ta e="T293" id="Seg_3390" s="T292">v-v&gt;v-v:pn</ta>
            <ta e="T294" id="Seg_3391" s="T293">pers</ta>
            <ta e="T295" id="Seg_3392" s="T294">pers</ta>
            <ta e="T296" id="Seg_3393" s="T295">n-n:case.poss</ta>
            <ta e="T297" id="Seg_3394" s="T296">v-v:tense-v:pn</ta>
            <ta e="T298" id="Seg_3395" s="T297">ptcl</ta>
            <ta e="T299" id="Seg_3396" s="T298">v-v:mood.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_3397" s="T0">n</ta>
            <ta e="T2" id="Seg_3398" s="T1">n</ta>
            <ta e="T3" id="Seg_3399" s="T2">v</ta>
            <ta e="T4" id="Seg_3400" s="T3">num</ta>
            <ta e="T5" id="Seg_3401" s="T4">n</ta>
            <ta e="T6" id="Seg_3402" s="T5">v</ta>
            <ta e="T7" id="Seg_3403" s="T6">dempro</ta>
            <ta e="T8" id="Seg_3404" s="T7">n</ta>
            <ta e="T9" id="Seg_3405" s="T8">v</ta>
            <ta e="T10" id="Seg_3406" s="T9">n</ta>
            <ta e="T11" id="Seg_3407" s="T10">n</ta>
            <ta e="T12" id="Seg_3408" s="T11">adv</ta>
            <ta e="T13" id="Seg_3409" s="T12">v</ta>
            <ta e="T14" id="Seg_3410" s="T13">n</ta>
            <ta e="T15" id="Seg_3411" s="T14">v</ta>
            <ta e="T16" id="Seg_3412" s="T15">n</ta>
            <ta e="T17" id="Seg_3413" s="T16">v</ta>
            <ta e="T18" id="Seg_3414" s="T17">v</ta>
            <ta e="T19" id="Seg_3415" s="T18">v</ta>
            <ta e="T20" id="Seg_3416" s="T19">n</ta>
            <ta e="T21" id="Seg_3417" s="T20">n</ta>
            <ta e="T22" id="Seg_3418" s="T21">v</ta>
            <ta e="T23" id="Seg_3419" s="T22">v</ta>
            <ta e="T24" id="Seg_3420" s="T23">n</ta>
            <ta e="T25" id="Seg_3421" s="T24">n</ta>
            <ta e="T26" id="Seg_3422" s="T25">n</ta>
            <ta e="T27" id="Seg_3423" s="T26">v</ta>
            <ta e="T28" id="Seg_3424" s="T27">dempro</ta>
            <ta e="T29" id="Seg_3425" s="T28">v</ta>
            <ta e="T30" id="Seg_3426" s="T29">n</ta>
            <ta e="T31" id="Seg_3427" s="T30">n</ta>
            <ta e="T32" id="Seg_3428" s="T31">n</ta>
            <ta e="T33" id="Seg_3429" s="T32">n</ta>
            <ta e="T34" id="Seg_3430" s="T33">n</ta>
            <ta e="T35" id="Seg_3431" s="T34">v</ta>
            <ta e="T36" id="Seg_3432" s="T35">pers</ta>
            <ta e="T37" id="Seg_3433" s="T36">n</ta>
            <ta e="T38" id="Seg_3434" s="T37">n</ta>
            <ta e="T39" id="Seg_3435" s="T38">adj</ta>
            <ta e="T40" id="Seg_3436" s="T39">n</ta>
            <ta e="T41" id="Seg_3437" s="T40">v</ta>
            <ta e="T42" id="Seg_3438" s="T41">v</ta>
            <ta e="T43" id="Seg_3439" s="T42">v</ta>
            <ta e="T44" id="Seg_3440" s="T43">n</ta>
            <ta e="T45" id="Seg_3441" s="T44">ptcl</ta>
            <ta e="T46" id="Seg_3442" s="T45">v</ta>
            <ta e="T47" id="Seg_3443" s="T46">n</ta>
            <ta e="T48" id="Seg_3444" s="T47">adj</ta>
            <ta e="T49" id="Seg_3445" s="T48">n</ta>
            <ta e="T50" id="Seg_3446" s="T49">v</ta>
            <ta e="T51" id="Seg_3447" s="T50">adj</ta>
            <ta e="T52" id="Seg_3448" s="T51">n</ta>
            <ta e="T53" id="Seg_3449" s="T52">v</ta>
            <ta e="T54" id="Seg_3450" s="T53">n</ta>
            <ta e="T55" id="Seg_3451" s="T54">n</ta>
            <ta e="T56" id="Seg_3452" s="T55">v</ta>
            <ta e="T57" id="Seg_3453" s="T56">v</ta>
            <ta e="T58" id="Seg_3454" s="T57">v</ta>
            <ta e="T59" id="Seg_3455" s="T58">dempro</ta>
            <ta e="T60" id="Seg_3456" s="T59">n</ta>
            <ta e="T61" id="Seg_3457" s="T60">n</ta>
            <ta e="T62" id="Seg_3458" s="T61">v</ta>
            <ta e="T63" id="Seg_3459" s="T62">v</ta>
            <ta e="T64" id="Seg_3460" s="T63">v</ta>
            <ta e="T65" id="Seg_3461" s="T64">v</ta>
            <ta e="T66" id="Seg_3462" s="T65">n</ta>
            <ta e="T67" id="Seg_3463" s="T66">post</ta>
            <ta e="T68" id="Seg_3464" s="T67">v</ta>
            <ta e="T69" id="Seg_3465" s="T68">v</ta>
            <ta e="T70" id="Seg_3466" s="T69">v</ta>
            <ta e="T71" id="Seg_3467" s="T70">adj</ta>
            <ta e="T72" id="Seg_3468" s="T71">n</ta>
            <ta e="T73" id="Seg_3469" s="T72">v</ta>
            <ta e="T74" id="Seg_3470" s="T73">dempro</ta>
            <ta e="T75" id="Seg_3471" s="T74">n</ta>
            <ta e="T76" id="Seg_3472" s="T75">n</ta>
            <ta e="T77" id="Seg_3473" s="T76">v</ta>
            <ta e="T78" id="Seg_3474" s="T77">v</ta>
            <ta e="T79" id="Seg_3475" s="T78">adj</ta>
            <ta e="T80" id="Seg_3476" s="T79">adj</ta>
            <ta e="T81" id="Seg_3477" s="T80">n</ta>
            <ta e="T82" id="Seg_3478" s="T81">v</ta>
            <ta e="T83" id="Seg_3479" s="T82">dempro</ta>
            <ta e="T84" id="Seg_3480" s="T83">v</ta>
            <ta e="T85" id="Seg_3481" s="T84">n</ta>
            <ta e="T86" id="Seg_3482" s="T85">n</ta>
            <ta e="T87" id="Seg_3483" s="T86">v</ta>
            <ta e="T88" id="Seg_3484" s="T87">v</ta>
            <ta e="T89" id="Seg_3485" s="T88">n</ta>
            <ta e="T90" id="Seg_3486" s="T89">propr</ta>
            <ta e="T91" id="Seg_3487" s="T90">v</ta>
            <ta e="T92" id="Seg_3488" s="T91">refl</ta>
            <ta e="T93" id="Seg_3489" s="T92">ptcl</ta>
            <ta e="T94" id="Seg_3490" s="T93">dempro</ta>
            <ta e="T95" id="Seg_3491" s="T94">v</ta>
            <ta e="T96" id="Seg_3492" s="T95">v</ta>
            <ta e="T97" id="Seg_3493" s="T96">v</ta>
            <ta e="T98" id="Seg_3494" s="T97">adv</ta>
            <ta e="T99" id="Seg_3495" s="T98">adj</ta>
            <ta e="T100" id="Seg_3496" s="T99">adj</ta>
            <ta e="T101" id="Seg_3497" s="T100">n</ta>
            <ta e="T102" id="Seg_3498" s="T101">v</ta>
            <ta e="T103" id="Seg_3499" s="T102">adv</ta>
            <ta e="T104" id="Seg_3500" s="T103">v</ta>
            <ta e="T105" id="Seg_3501" s="T104">n</ta>
            <ta e="T106" id="Seg_3502" s="T105">adv</ta>
            <ta e="T107" id="Seg_3503" s="T106">n</ta>
            <ta e="T108" id="Seg_3504" s="T107">v</ta>
            <ta e="T109" id="Seg_3505" s="T108">v</ta>
            <ta e="T110" id="Seg_3506" s="T109">adv</ta>
            <ta e="T111" id="Seg_3507" s="T110">n</ta>
            <ta e="T112" id="Seg_3508" s="T111">v</ta>
            <ta e="T113" id="Seg_3509" s="T112">propr</ta>
            <ta e="T114" id="Seg_3510" s="T113">refl</ta>
            <ta e="T115" id="Seg_3511" s="T114">v</ta>
            <ta e="T116" id="Seg_3512" s="T115">refl</ta>
            <ta e="T117" id="Seg_3513" s="T116">adv</ta>
            <ta e="T118" id="Seg_3514" s="T117">v</ta>
            <ta e="T119" id="Seg_3515" s="T118">dempro</ta>
            <ta e="T120" id="Seg_3516" s="T119">n</ta>
            <ta e="T121" id="Seg_3517" s="T120">v</ta>
            <ta e="T122" id="Seg_3518" s="T121">v</ta>
            <ta e="T123" id="Seg_3519" s="T122">adj</ta>
            <ta e="T124" id="Seg_3520" s="T123">n</ta>
            <ta e="T125" id="Seg_3521" s="T124">v</ta>
            <ta e="T126" id="Seg_3522" s="T125">adj</ta>
            <ta e="T127" id="Seg_3523" s="T126">n</ta>
            <ta e="T128" id="Seg_3524" s="T127">v</ta>
            <ta e="T129" id="Seg_3525" s="T128">adv</ta>
            <ta e="T130" id="Seg_3526" s="T129">adj</ta>
            <ta e="T131" id="Seg_3527" s="T130">adj</ta>
            <ta e="T132" id="Seg_3528" s="T131">n</ta>
            <ta e="T133" id="Seg_3529" s="T132">v</ta>
            <ta e="T134" id="Seg_3530" s="T133">dempro</ta>
            <ta e="T135" id="Seg_3531" s="T134">n</ta>
            <ta e="T136" id="Seg_3532" s="T135">adv</ta>
            <ta e="T137" id="Seg_3533" s="T136">v</ta>
            <ta e="T138" id="Seg_3534" s="T137">n</ta>
            <ta e="T139" id="Seg_3535" s="T138">v</ta>
            <ta e="T140" id="Seg_3536" s="T139">adv</ta>
            <ta e="T141" id="Seg_3537" s="T140">v</ta>
            <ta e="T142" id="Seg_3538" s="T141">v</ta>
            <ta e="T143" id="Seg_3539" s="T142">que</ta>
            <ta e="T144" id="Seg_3540" s="T143">n</ta>
            <ta e="T145" id="Seg_3541" s="T144">v</ta>
            <ta e="T146" id="Seg_3542" s="T145">adj</ta>
            <ta e="T147" id="Seg_3543" s="T146">adj</ta>
            <ta e="T148" id="Seg_3544" s="T147">n</ta>
            <ta e="T149" id="Seg_3545" s="T148">v</ta>
            <ta e="T150" id="Seg_3546" s="T149">n</ta>
            <ta e="T151" id="Seg_3547" s="T150">n</ta>
            <ta e="T152" id="Seg_3548" s="T151">v</ta>
            <ta e="T153" id="Seg_3549" s="T152">v</ta>
            <ta e="T154" id="Seg_3550" s="T153">n</ta>
            <ta e="T155" id="Seg_3551" s="T154">v</ta>
            <ta e="T156" id="Seg_3552" s="T155">v</ta>
            <ta e="T157" id="Seg_3553" s="T156">n</ta>
            <ta e="T158" id="Seg_3554" s="T157">n</ta>
            <ta e="T159" id="Seg_3555" s="T158">n</ta>
            <ta e="T160" id="Seg_3556" s="T159">n</ta>
            <ta e="T161" id="Seg_3557" s="T160">n</ta>
            <ta e="T162" id="Seg_3558" s="T161">v</ta>
            <ta e="T163" id="Seg_3559" s="T162">v</ta>
            <ta e="T164" id="Seg_3560" s="T163">n</ta>
            <ta e="T165" id="Seg_3561" s="T164">adj</ta>
            <ta e="T166" id="Seg_3562" s="T165">v</ta>
            <ta e="T167" id="Seg_3563" s="T166">v</ta>
            <ta e="T168" id="Seg_3564" s="T167">v</ta>
            <ta e="T169" id="Seg_3565" s="T168">pers</ta>
            <ta e="T170" id="Seg_3566" s="T169">ptcl</ta>
            <ta e="T171" id="Seg_3567" s="T170">v</ta>
            <ta e="T172" id="Seg_3568" s="T171">refl</ta>
            <ta e="T173" id="Seg_3569" s="T172">v</ta>
            <ta e="T174" id="Seg_3570" s="T173">conj</ta>
            <ta e="T175" id="Seg_3571" s="T174">num</ta>
            <ta e="T176" id="Seg_3572" s="T175">n</ta>
            <ta e="T177" id="Seg_3573" s="T176">dempro</ta>
            <ta e="T178" id="Seg_3574" s="T177">num</ta>
            <ta e="T179" id="Seg_3575" s="T178">n</ta>
            <ta e="T180" id="Seg_3576" s="T179">refl</ta>
            <ta e="T181" id="Seg_3577" s="T180">n</ta>
            <ta e="T182" id="Seg_3578" s="T181">v</ta>
            <ta e="T183" id="Seg_3579" s="T182">n</ta>
            <ta e="T184" id="Seg_3580" s="T183">n</ta>
            <ta e="T185" id="Seg_3581" s="T184">n</ta>
            <ta e="T186" id="Seg_3582" s="T185">n</ta>
            <ta e="T187" id="Seg_3583" s="T186">v</ta>
            <ta e="T188" id="Seg_3584" s="T187">v</ta>
            <ta e="T189" id="Seg_3585" s="T188">dempro</ta>
            <ta e="T190" id="Seg_3586" s="T189">adv</ta>
            <ta e="T191" id="Seg_3587" s="T190">adj</ta>
            <ta e="T192" id="Seg_3588" s="T191">adj</ta>
            <ta e="T193" id="Seg_3589" s="T192">v</ta>
            <ta e="T194" id="Seg_3590" s="T193">v</ta>
            <ta e="T195" id="Seg_3591" s="T194">n</ta>
            <ta e="T196" id="Seg_3592" s="T195">v</ta>
            <ta e="T197" id="Seg_3593" s="T196">n</ta>
            <ta e="T198" id="Seg_3594" s="T197">n</ta>
            <ta e="T199" id="Seg_3595" s="T198">v</ta>
            <ta e="T200" id="Seg_3596" s="T199">v</ta>
            <ta e="T201" id="Seg_3597" s="T200">v</ta>
            <ta e="T202" id="Seg_3598" s="T201">pers</ta>
            <ta e="T203" id="Seg_3599" s="T202">v</ta>
            <ta e="T204" id="Seg_3600" s="T203">conj</ta>
            <ta e="T205" id="Seg_3601" s="T204">n</ta>
            <ta e="T206" id="Seg_3602" s="T205">v</ta>
            <ta e="T207" id="Seg_3603" s="T206">num</ta>
            <ta e="T208" id="Seg_3604" s="T207">v</ta>
            <ta e="T209" id="Seg_3605" s="T208">pers</ta>
            <ta e="T210" id="Seg_3606" s="T209">v</ta>
            <ta e="T211" id="Seg_3607" s="T210">num</ta>
            <ta e="T212" id="Seg_3608" s="T211">adv</ta>
            <ta e="T213" id="Seg_3609" s="T212">v</ta>
            <ta e="T214" id="Seg_3610" s="T213">adj</ta>
            <ta e="T215" id="Seg_3611" s="T214">n</ta>
            <ta e="T216" id="Seg_3612" s="T215">v</ta>
            <ta e="T217" id="Seg_3613" s="T216">v</ta>
            <ta e="T218" id="Seg_3614" s="T217">v</ta>
            <ta e="T219" id="Seg_3615" s="T218">n</ta>
            <ta e="T220" id="Seg_3616" s="T219">v</ta>
            <ta e="T221" id="Seg_3617" s="T220">adj</ta>
            <ta e="T222" id="Seg_3618" s="T221">n</ta>
            <ta e="T223" id="Seg_3619" s="T222">v</ta>
            <ta e="T224" id="Seg_3620" s="T223">n</ta>
            <ta e="T225" id="Seg_3621" s="T224">ptcl</ta>
            <ta e="T226" id="Seg_3622" s="T225">adv</ta>
            <ta e="T227" id="Seg_3623" s="T226">v</ta>
            <ta e="T228" id="Seg_3624" s="T227">dempro</ta>
            <ta e="T229" id="Seg_3625" s="T228">n</ta>
            <ta e="T230" id="Seg_3626" s="T229">v</ta>
            <ta e="T231" id="Seg_3627" s="T230">v</ta>
            <ta e="T232" id="Seg_3628" s="T231">adj</ta>
            <ta e="T233" id="Seg_3629" s="T232">n</ta>
            <ta e="T234" id="Seg_3630" s="T233">adj</ta>
            <ta e="T235" id="Seg_3631" s="T234">n</ta>
            <ta e="T236" id="Seg_3632" s="T235">n</ta>
            <ta e="T237" id="Seg_3633" s="T236">v</ta>
            <ta e="T238" id="Seg_3634" s="T237">dempro</ta>
            <ta e="T239" id="Seg_3635" s="T238">v</ta>
            <ta e="T240" id="Seg_3636" s="T239">num</ta>
            <ta e="T241" id="Seg_3637" s="T240">n</ta>
            <ta e="T242" id="Seg_3638" s="T241">v</ta>
            <ta e="T243" id="Seg_3639" s="T242">n</ta>
            <ta e="T244" id="Seg_3640" s="T243">n</ta>
            <ta e="T245" id="Seg_3641" s="T244">v</ta>
            <ta e="T246" id="Seg_3642" s="T245">v</ta>
            <ta e="T247" id="Seg_3643" s="T246">v</ta>
            <ta e="T248" id="Seg_3644" s="T247">n</ta>
            <ta e="T249" id="Seg_3645" s="T248">dempro</ta>
            <ta e="T250" id="Seg_3646" s="T249">adv</ta>
            <ta e="T251" id="Seg_3647" s="T250">n</ta>
            <ta e="T252" id="Seg_3648" s="T251">v</ta>
            <ta e="T253" id="Seg_3649" s="T252">dempro</ta>
            <ta e="T254" id="Seg_3650" s="T253">n</ta>
            <ta e="T255" id="Seg_3651" s="T254">quant</ta>
            <ta e="T256" id="Seg_3652" s="T255">v</ta>
            <ta e="T257" id="Seg_3653" s="T256">v</ta>
            <ta e="T258" id="Seg_3654" s="T257">conj</ta>
            <ta e="T259" id="Seg_3655" s="T258">que</ta>
            <ta e="T260" id="Seg_3656" s="T259">n</ta>
            <ta e="T261" id="Seg_3657" s="T260">v</ta>
            <ta e="T262" id="Seg_3658" s="T261">refl</ta>
            <ta e="T263" id="Seg_3659" s="T262">ptcl</ta>
            <ta e="T264" id="Seg_3660" s="T263">v</ta>
            <ta e="T265" id="Seg_3661" s="T264">dempro</ta>
            <ta e="T266" id="Seg_3662" s="T265">n</ta>
            <ta e="T267" id="Seg_3663" s="T266">n</ta>
            <ta e="T268" id="Seg_3664" s="T267">v</ta>
            <ta e="T269" id="Seg_3665" s="T268">v</ta>
            <ta e="T270" id="Seg_3666" s="T269">v</ta>
            <ta e="T271" id="Seg_3667" s="T270">v</ta>
            <ta e="T272" id="Seg_3668" s="T271">v</ta>
            <ta e="T273" id="Seg_3669" s="T272">adj</ta>
            <ta e="T274" id="Seg_3670" s="T273">n</ta>
            <ta e="T275" id="Seg_3671" s="T274">adv</ta>
            <ta e="T276" id="Seg_3672" s="T275">v</ta>
            <ta e="T277" id="Seg_3673" s="T276">v</ta>
            <ta e="T278" id="Seg_3674" s="T277">dempro</ta>
            <ta e="T279" id="Seg_3675" s="T278">n</ta>
            <ta e="T280" id="Seg_3676" s="T279">v</ta>
            <ta e="T281" id="Seg_3677" s="T280">v</ta>
            <ta e="T282" id="Seg_3678" s="T281">v</ta>
            <ta e="T283" id="Seg_3679" s="T282">v</ta>
            <ta e="T284" id="Seg_3680" s="T283">v</ta>
            <ta e="T285" id="Seg_3681" s="T284">dempro</ta>
            <ta e="T286" id="Seg_3682" s="T285">n</ta>
            <ta e="T287" id="Seg_3683" s="T286">adv</ta>
            <ta e="T288" id="Seg_3684" s="T287">v</ta>
            <ta e="T289" id="Seg_3685" s="T288">n</ta>
            <ta e="T290" id="Seg_3686" s="T289">v</ta>
            <ta e="T291" id="Seg_3687" s="T290">dempro</ta>
            <ta e="T292" id="Seg_3688" s="T291">v</ta>
            <ta e="T293" id="Seg_3689" s="T292">v</ta>
            <ta e="T294" id="Seg_3690" s="T293">pers</ta>
            <ta e="T295" id="Seg_3691" s="T294">pers</ta>
            <ta e="T296" id="Seg_3692" s="T295">n</ta>
            <ta e="T297" id="Seg_3693" s="T296">v</ta>
            <ta e="T298" id="Seg_3694" s="T297">ptcl</ta>
            <ta e="T299" id="Seg_3695" s="T298">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_3696" s="T0">np.h:E</ta>
            <ta e="T2" id="Seg_3697" s="T1">np.h:Com</ta>
            <ta e="T5" id="Seg_3698" s="T4">np.h:Th 0.3.h:Poss</ta>
            <ta e="T8" id="Seg_3699" s="T7">np.h:A 0.3.h:Poss</ta>
            <ta e="T10" id="Seg_3700" s="T9">np.h:Poss</ta>
            <ta e="T11" id="Seg_3701" s="T10">np:P</ta>
            <ta e="T14" id="Seg_3702" s="T13">np:P</ta>
            <ta e="T15" id="Seg_3703" s="T14">0.3.h:A</ta>
            <ta e="T16" id="Seg_3704" s="T15">np:G</ta>
            <ta e="T17" id="Seg_3705" s="T16">0.3.h:A</ta>
            <ta e="T19" id="Seg_3706" s="T18">0.3.h:A</ta>
            <ta e="T20" id="Seg_3707" s="T19">np:G</ta>
            <ta e="T21" id="Seg_3708" s="T20">np:Th</ta>
            <ta e="T23" id="Seg_3709" s="T22">0.3.h:Th</ta>
            <ta e="T24" id="Seg_3710" s="T23">np:Poss</ta>
            <ta e="T25" id="Seg_3711" s="T24">np:L</ta>
            <ta e="T26" id="Seg_3712" s="T25">np:Th</ta>
            <ta e="T28" id="Seg_3713" s="T27">pro.h:A</ta>
            <ta e="T30" id="Seg_3714" s="T29">np.h:Poss</ta>
            <ta e="T32" id="Seg_3715" s="T31">0.1.h:Poss</ta>
            <ta e="T33" id="Seg_3716" s="T32">np.h:Poss</ta>
            <ta e="T37" id="Seg_3717" s="T36">np.h:Poss</ta>
            <ta e="T40" id="Seg_3718" s="T39">np:P 0.3.h:Poss</ta>
            <ta e="T41" id="Seg_3719" s="T40">0.3.h:A</ta>
            <ta e="T43" id="Seg_3720" s="T42">0.3.h:A</ta>
            <ta e="T44" id="Seg_3721" s="T43">np:G</ta>
            <ta e="T47" id="Seg_3722" s="T46">np:Th 0.3.h:Poss</ta>
            <ta e="T49" id="Seg_3723" s="T48">np:P 0.3.h:Poss</ta>
            <ta e="T50" id="Seg_3724" s="T49">0.3.h:A</ta>
            <ta e="T52" id="Seg_3725" s="T51">np:Th 0.3.h:Poss</ta>
            <ta e="T54" id="Seg_3726" s="T53">np:G</ta>
            <ta e="T55" id="Seg_3727" s="T54">np.h:A</ta>
            <ta e="T58" id="Seg_3728" s="T57">0.3.h:Th</ta>
            <ta e="T59" id="Seg_3729" s="T58">adv:So</ta>
            <ta e="T60" id="Seg_3730" s="T59">np:Poss</ta>
            <ta e="T61" id="Seg_3731" s="T60">np:G</ta>
            <ta e="T62" id="Seg_3732" s="T61">0.3.h:A </ta>
            <ta e="T64" id="Seg_3733" s="T63">0.3.h:A</ta>
            <ta e="T67" id="Seg_3734" s="T65">pp:Pth</ta>
            <ta e="T68" id="Seg_3735" s="T67">0.3.h:A</ta>
            <ta e="T69" id="Seg_3736" s="T68">0.3.h:A</ta>
            <ta e="T70" id="Seg_3737" s="T69">0.3.h:A</ta>
            <ta e="T72" id="Seg_3738" s="T71">np:Th</ta>
            <ta e="T74" id="Seg_3739" s="T73">pro.h:A</ta>
            <ta e="T75" id="Seg_3740" s="T74">np:Poss</ta>
            <ta e="T76" id="Seg_3741" s="T75">np:L</ta>
            <ta e="T78" id="Seg_3742" s="T77">0.3.h:E</ta>
            <ta e="T81" id="Seg_3743" s="T80">np.h:A</ta>
            <ta e="T83" id="Seg_3744" s="T82">pro.h:A</ta>
            <ta e="T85" id="Seg_3745" s="T84">np:A 0.3.h:Poss</ta>
            <ta e="T86" id="Seg_3746" s="T85">np:G</ta>
            <ta e="T89" id="Seg_3747" s="T88">np:A</ta>
            <ta e="T90" id="Seg_3748" s="T89">np:A</ta>
            <ta e="T94" id="Seg_3749" s="T93">pro.h:A</ta>
            <ta e="T96" id="Seg_3750" s="T95">0.3.h:A</ta>
            <ta e="T98" id="Seg_3751" s="T97">adv:Time</ta>
            <ta e="T101" id="Seg_3752" s="T100">np.h:A</ta>
            <ta e="T103" id="Seg_3753" s="T102">adv:Time</ta>
            <ta e="T104" id="Seg_3754" s="T103">0.3.h:A</ta>
            <ta e="T105" id="Seg_3755" s="T104">np:A 0.3.h:Poss</ta>
            <ta e="T106" id="Seg_3756" s="T105">adv:Time</ta>
            <ta e="T107" id="Seg_3757" s="T106">np:G 0.3:Poss</ta>
            <ta e="T110" id="Seg_3758" s="T109">adv:Time</ta>
            <ta e="T111" id="Seg_3759" s="T110">np:Th</ta>
            <ta e="T112" id="Seg_3760" s="T111">0.3.h:A</ta>
            <ta e="T113" id="Seg_3761" s="T112">np.h:A</ta>
            <ta e="T116" id="Seg_3762" s="T115">pro.h:A</ta>
            <ta e="T117" id="Seg_3763" s="T116">adv:Time</ta>
            <ta e="T120" id="Seg_3764" s="T119">np.h:A</ta>
            <ta e="T122" id="Seg_3765" s="T121">0.3.h:A</ta>
            <ta e="T124" id="Seg_3766" s="T123">np:Th 0.2.h:Poss</ta>
            <ta e="T127" id="Seg_3767" s="T126">np:Th 0.2.h:Poss</ta>
            <ta e="T129" id="Seg_3768" s="T128">adv:Time</ta>
            <ta e="T132" id="Seg_3769" s="T131">np.h:A</ta>
            <ta e="T134" id="Seg_3770" s="T133">pro.h:A</ta>
            <ta e="T135" id="Seg_3771" s="T134">np.h:A</ta>
            <ta e="T136" id="Seg_3772" s="T135">adv:Time</ta>
            <ta e="T138" id="Seg_3773" s="T137">np:A 0.3.h:Poss</ta>
            <ta e="T140" id="Seg_3774" s="T139">adv:Time</ta>
            <ta e="T142" id="Seg_3775" s="T141">0.3.h:E</ta>
            <ta e="T144" id="Seg_3776" s="T143">np.h:Th</ta>
            <ta e="T145" id="Seg_3777" s="T144">0.2.h:Th</ta>
            <ta e="T148" id="Seg_3778" s="T147">np.h:Th</ta>
            <ta e="T149" id="Seg_3779" s="T148">0.1.h:Th</ta>
            <ta e="T150" id="Seg_3780" s="T149">np.h:B</ta>
            <ta e="T151" id="Seg_3781" s="T150">np:P</ta>
            <ta e="T152" id="Seg_3782" s="T151">0.3.h:A</ta>
            <ta e="T153" id="Seg_3783" s="T152">0.3.h:A</ta>
            <ta e="T154" id="Seg_3784" s="T153">np:P</ta>
            <ta e="T155" id="Seg_3785" s="T154">0.3.h:A</ta>
            <ta e="T156" id="Seg_3786" s="T155">0.3.h:A</ta>
            <ta e="T157" id="Seg_3787" s="T156">np:G</ta>
            <ta e="T158" id="Seg_3788" s="T157">np:Poss</ta>
            <ta e="T159" id="Seg_3789" s="T158">np:Ins</ta>
            <ta e="T160" id="Seg_3790" s="T159">np.h:B</ta>
            <ta e="T161" id="Seg_3791" s="T160">np:P</ta>
            <ta e="T162" id="Seg_3792" s="T161">0.3.h:A</ta>
            <ta e="T163" id="Seg_3793" s="T162">0.2.h:E</ta>
            <ta e="T166" id="Seg_3794" s="T165">0.1.h:Th</ta>
            <ta e="T167" id="Seg_3795" s="T166">0.1.h:A</ta>
            <ta e="T168" id="Seg_3796" s="T167">0.1.h:A</ta>
            <ta e="T169" id="Seg_3797" s="T168">pro.h:Th</ta>
            <ta e="T171" id="Seg_3798" s="T170">0.3.h:A</ta>
            <ta e="T173" id="Seg_3799" s="T172">0.3.h:A</ta>
            <ta e="T176" id="Seg_3800" s="T175">n:Time</ta>
            <ta e="T179" id="Seg_3801" s="T178">np.h:Poss</ta>
            <ta e="T181" id="Seg_3802" s="T180">np:A</ta>
            <ta e="T183" id="Seg_3803" s="T182">np:Th 0.3:Poss</ta>
            <ta e="T184" id="Seg_3804" s="T183">np:Th 0.3:Poss</ta>
            <ta e="T185" id="Seg_3805" s="T184">np:Poss</ta>
            <ta e="T186" id="Seg_3806" s="T185">np:L</ta>
            <ta e="T190" id="Seg_3807" s="T188">pp:So</ta>
            <ta e="T193" id="Seg_3808" s="T192">0.3.h:A</ta>
            <ta e="T194" id="Seg_3809" s="T193">0.3.h:A</ta>
            <ta e="T195" id="Seg_3810" s="T194">np:G 0.3.h:Poss</ta>
            <ta e="T196" id="Seg_3811" s="T195">0.3.h:A</ta>
            <ta e="T197" id="Seg_3812" s="T196">np.h:Th</ta>
            <ta e="T198" id="Seg_3813" s="T197">np:G 0.3.h:Poss</ta>
            <ta e="T200" id="Seg_3814" s="T199">0.3.h:A</ta>
            <ta e="T201" id="Seg_3815" s="T200">0.3.h:A</ta>
            <ta e="T203" id="Seg_3816" s="T202">0.2.h:E</ta>
            <ta e="T205" id="Seg_3817" s="T204">np:G 0.2.h:Poss</ta>
            <ta e="T206" id="Seg_3818" s="T205">0.2.h:A</ta>
            <ta e="T207" id="Seg_3819" s="T206">np.h:A</ta>
            <ta e="T209" id="Seg_3820" s="T208">pro.h:A</ta>
            <ta e="T211" id="Seg_3821" s="T210">np.h:Th</ta>
            <ta e="T212" id="Seg_3822" s="T211">adv:L</ta>
            <ta e="T215" id="Seg_3823" s="T214">np:P</ta>
            <ta e="T217" id="Seg_3824" s="T216">0.3.h:A</ta>
            <ta e="T218" id="Seg_3825" s="T217">0.3.h:A</ta>
            <ta e="T219" id="Seg_3826" s="T218">np:G 0.3.h:Poss</ta>
            <ta e="T220" id="Seg_3827" s="T219">0.3.h:A</ta>
            <ta e="T222" id="Seg_3828" s="T221">np:Th</ta>
            <ta e="T224" id="Seg_3829" s="T223">np:Th 0.3.h:Poss</ta>
            <ta e="T228" id="Seg_3830" s="T227">pro.h:A</ta>
            <ta e="T229" id="Seg_3831" s="T228">np:G</ta>
            <ta e="T231" id="Seg_3832" s="T230">0.3.h:A</ta>
            <ta e="T236" id="Seg_3833" s="T235">np:Th 0.2.h:Poss</ta>
            <ta e="T238" id="Seg_3834" s="T237">pro.h:A</ta>
            <ta e="T241" id="Seg_3835" s="T240">np.h:Th 0.1.h:Poss</ta>
            <ta e="T243" id="Seg_3836" s="T242">np.h:A</ta>
            <ta e="T244" id="Seg_3837" s="T243">np:G</ta>
            <ta e="T245" id="Seg_3838" s="T244">0.3.h:A</ta>
            <ta e="T247" id="Seg_3839" s="T246">0.3.h:A</ta>
            <ta e="T248" id="Seg_3840" s="T247">np:G</ta>
            <ta e="T250" id="Seg_3841" s="T248">pp:Time</ta>
            <ta e="T251" id="Seg_3842" s="T250">np.h:Th 0.1.h:Poss</ta>
            <ta e="T253" id="Seg_3843" s="T252">pro.h:A</ta>
            <ta e="T254" id="Seg_3844" s="T253">np.h:P</ta>
            <ta e="T260" id="Seg_3845" s="T259">np.h:Th 0.2.h:Poss</ta>
            <ta e="T262" id="Seg_3846" s="T261">pro.h:E</ta>
            <ta e="T266" id="Seg_3847" s="T265">np:A</ta>
            <ta e="T267" id="Seg_3848" s="T266">np:Th</ta>
            <ta e="T271" id="Seg_3849" s="T270">0.3.h:A</ta>
            <ta e="T272" id="Seg_3850" s="T271">0.3.h:A</ta>
            <ta e="T274" id="Seg_3851" s="T273">np:G</ta>
            <ta e="T275" id="Seg_3852" s="T274">adv:L</ta>
            <ta e="T277" id="Seg_3853" s="T276">0.3.h:A</ta>
            <ta e="T278" id="Seg_3854" s="T277">pro.h:A</ta>
            <ta e="T279" id="Seg_3855" s="T278">np.h:Th</ta>
            <ta e="T280" id="Seg_3856" s="T279">0.3.h:E</ta>
            <ta e="T284" id="Seg_3857" s="T283">0.3.h:A</ta>
            <ta e="T286" id="Seg_3858" s="T285">np:G</ta>
            <ta e="T287" id="Seg_3859" s="T286">adv:Time</ta>
            <ta e="T288" id="Seg_3860" s="T287">0.3.h:A</ta>
            <ta e="T289" id="Seg_3861" s="T288">np.h:A 0.3.h:Poss</ta>
            <ta e="T291" id="Seg_3862" s="T290">pro.h:P</ta>
            <ta e="T292" id="Seg_3863" s="T291">0.3.h:A</ta>
            <ta e="T293" id="Seg_3864" s="T292">0.3.h:A</ta>
            <ta e="T294" id="Seg_3865" s="T293">pro.h:Th</ta>
            <ta e="T295" id="Seg_3866" s="T294">pro.h:Poss</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_3867" s="T0">np.h:S</ta>
            <ta e="T3" id="Seg_3868" s="T2">v:pred</ta>
            <ta e="T5" id="Seg_3869" s="T4">np.h:S</ta>
            <ta e="T6" id="Seg_3870" s="T5">v:pred</ta>
            <ta e="T8" id="Seg_3871" s="T7">np.h:S</ta>
            <ta e="T9" id="Seg_3872" s="T8">s:temp</ta>
            <ta e="T11" id="Seg_3873" s="T10">np:O</ta>
            <ta e="T13" id="Seg_3874" s="T12">v:pred</ta>
            <ta e="T14" id="Seg_3875" s="T13">np:O</ta>
            <ta e="T15" id="Seg_3876" s="T14">v:pred 0.3.h:S</ta>
            <ta e="T17" id="Seg_3877" s="T16">v:pred 0.3.h:S</ta>
            <ta e="T18" id="Seg_3878" s="T17">conv:pred</ta>
            <ta e="T19" id="Seg_3879" s="T18">v:pred 0.3.h:S</ta>
            <ta e="T22" id="Seg_3880" s="T20">s:adv</ta>
            <ta e="T23" id="Seg_3881" s="T22">v:pred 0.3.h:S</ta>
            <ta e="T26" id="Seg_3882" s="T25">np:S</ta>
            <ta e="T27" id="Seg_3883" s="T26">v:pred</ta>
            <ta e="T28" id="Seg_3884" s="T27">pro.h:S</ta>
            <ta e="T29" id="Seg_3885" s="T28">v:pred</ta>
            <ta e="T35" id="Seg_3886" s="T34">v:pred 0.2.h:S</ta>
            <ta e="T36" id="Seg_3887" s="T35">pro.h:O</ta>
            <ta e="T40" id="Seg_3888" s="T39">np:O</ta>
            <ta e="T41" id="Seg_3889" s="T40">v:pred 0.3.h:S</ta>
            <ta e="T42" id="Seg_3890" s="T41">conv:pred</ta>
            <ta e="T43" id="Seg_3891" s="T42">v:pred 0.3.h:S</ta>
            <ta e="T45" id="Seg_3892" s="T44">ptcl.neg</ta>
            <ta e="T46" id="Seg_3893" s="T45">conv:pred</ta>
            <ta e="T47" id="Seg_3894" s="T46">np:S</ta>
            <ta e="T49" id="Seg_3895" s="T48">np:O</ta>
            <ta e="T50" id="Seg_3896" s="T49">v:pred 0.3.h:S</ta>
            <ta e="T52" id="Seg_3897" s="T51">np:S</ta>
            <ta e="T53" id="Seg_3898" s="T52">v:pred</ta>
            <ta e="T55" id="Seg_3899" s="T54">np.h:S</ta>
            <ta e="T56" id="Seg_3900" s="T55">v:pred</ta>
            <ta e="T57" id="Seg_3901" s="T56">conv:pred</ta>
            <ta e="T58" id="Seg_3902" s="T57">v:pred 0.3.h:S</ta>
            <ta e="T62" id="Seg_3903" s="T61">v:pred 0.3.h:S</ta>
            <ta e="T63" id="Seg_3904" s="T62">conv:pred</ta>
            <ta e="T64" id="Seg_3905" s="T63">v:pred 0.3.h:S</ta>
            <ta e="T65" id="Seg_3906" s="T64">v:pred 0.2.h:S</ta>
            <ta e="T68" id="Seg_3907" s="T67">v:pred 0.3.h:S</ta>
            <ta e="T69" id="Seg_3908" s="T68">v:pred 0.3.h:S</ta>
            <ta e="T70" id="Seg_3909" s="T69">v:pred 0.3.h:S</ta>
            <ta e="T72" id="Seg_3910" s="T71">np:S</ta>
            <ta e="T73" id="Seg_3911" s="T72">v:pred</ta>
            <ta e="T74" id="Seg_3912" s="T73">pro.h:S</ta>
            <ta e="T77" id="Seg_3913" s="T76">v:pred</ta>
            <ta e="T78" id="Seg_3914" s="T77">s:temp conv:pred</ta>
            <ta e="T81" id="Seg_3915" s="T80">np.h:S</ta>
            <ta e="T82" id="Seg_3916" s="T81">v:pred</ta>
            <ta e="T83" id="Seg_3917" s="T82">pro.h:S</ta>
            <ta e="T84" id="Seg_3918" s="T83">v:pred</ta>
            <ta e="T85" id="Seg_3919" s="T84">np:S</ta>
            <ta e="T87" id="Seg_3920" s="T86">conv:pred</ta>
            <ta e="T88" id="Seg_3921" s="T87">v:pred</ta>
            <ta e="T89" id="Seg_3922" s="T88">np:S</ta>
            <ta e="T90" id="Seg_3923" s="T89">np:S</ta>
            <ta e="T91" id="Seg_3924" s="T90">v:pred</ta>
            <ta e="T93" id="Seg_3925" s="T92">ptcl.neg</ta>
            <ta e="T94" id="Seg_3926" s="T93">pro.h:S</ta>
            <ta e="T95" id="Seg_3927" s="T94">v:pred</ta>
            <ta e="T96" id="Seg_3928" s="T95">v:pred 0.3.h:S</ta>
            <ta e="T97" id="Seg_3929" s="T96">conv:pred</ta>
            <ta e="T101" id="Seg_3930" s="T100">np.h:S</ta>
            <ta e="T102" id="Seg_3931" s="T101">v:pred</ta>
            <ta e="T104" id="Seg_3932" s="T103">v:pred 0.3.h:S</ta>
            <ta e="T105" id="Seg_3933" s="T104">np:S</ta>
            <ta e="T109" id="Seg_3934" s="T108">v:pred</ta>
            <ta e="T112" id="Seg_3935" s="T111">v:pred 0.3.h:S</ta>
            <ta e="T113" id="Seg_3936" s="T112">np:S</ta>
            <ta e="T115" id="Seg_3937" s="T114">v:pred</ta>
            <ta e="T116" id="Seg_3938" s="T115">pro.h:S</ta>
            <ta e="T118" id="Seg_3939" s="T117">v:pred</ta>
            <ta e="T120" id="Seg_3940" s="T119">np.h:S</ta>
            <ta e="T121" id="Seg_3941" s="T120">v:pred</ta>
            <ta e="T122" id="Seg_3942" s="T121">v:pred</ta>
            <ta e="T124" id="Seg_3943" s="T123">np:S</ta>
            <ta e="T125" id="Seg_3944" s="T124">v:pred</ta>
            <ta e="T127" id="Seg_3945" s="T126">np:S</ta>
            <ta e="T128" id="Seg_3946" s="T127">v:pred</ta>
            <ta e="T132" id="Seg_3947" s="T131">np.h:S</ta>
            <ta e="T133" id="Seg_3948" s="T132">v:pred</ta>
            <ta e="T134" id="Seg_3949" s="T133">pro.h:S</ta>
            <ta e="T135" id="Seg_3950" s="T134">np.h:S</ta>
            <ta e="T137" id="Seg_3951" s="T136">v:pred</ta>
            <ta e="T138" id="Seg_3952" s="T137">np:S</ta>
            <ta e="T139" id="Seg_3953" s="T138">v:pred</ta>
            <ta e="T141" id="Seg_3954" s="T140">conv:pred</ta>
            <ta e="T142" id="Seg_3955" s="T141">v:pred 0.3.h:S</ta>
            <ta e="T144" id="Seg_3956" s="T143">n:pred</ta>
            <ta e="T145" id="Seg_3957" s="T144">cop 0.2.h:S</ta>
            <ta e="T148" id="Seg_3958" s="T147">n:pred</ta>
            <ta e="T149" id="Seg_3959" s="T148">cop 0.1.h:S</ta>
            <ta e="T151" id="Seg_3960" s="T150">np:O</ta>
            <ta e="T152" id="Seg_3961" s="T151">v:pred 0.3.h:S</ta>
            <ta e="T153" id="Seg_3962" s="T152">v:pred 0.3.h:S</ta>
            <ta e="T154" id="Seg_3963" s="T153">np:O</ta>
            <ta e="T155" id="Seg_3964" s="T154">v:pred 0.3.h:S</ta>
            <ta e="T156" id="Seg_3965" s="T155">v:pred 0.3.h:S</ta>
            <ta e="T161" id="Seg_3966" s="T160">np:O</ta>
            <ta e="T162" id="Seg_3967" s="T161">v:pred 0.3.h:S</ta>
            <ta e="T163" id="Seg_3968" s="T162">v:pred 0.2.h:S</ta>
            <ta e="T166" id="Seg_3969" s="T164">s:cond</ta>
            <ta e="T167" id="Seg_3970" s="T166">s:cond</ta>
            <ta e="T168" id="Seg_3971" s="T167">v:pred 0.1.h:S</ta>
            <ta e="T169" id="Seg_3972" s="T168">pro.h:O</ta>
            <ta e="T171" id="Seg_3973" s="T170">v:pred 0.3.h:S</ta>
            <ta e="T173" id="Seg_3974" s="T172">v:pred</ta>
            <ta e="T181" id="Seg_3975" s="T180">np:S</ta>
            <ta e="T182" id="Seg_3976" s="T181">v:pred</ta>
            <ta e="T183" id="Seg_3977" s="T182">np:S</ta>
            <ta e="T184" id="Seg_3978" s="T183">np:S</ta>
            <ta e="T187" id="Seg_3979" s="T186">conv:pred</ta>
            <ta e="T188" id="Seg_3980" s="T187">v:pred</ta>
            <ta e="T190" id="Seg_3981" s="T188">s:adv</ta>
            <ta e="T193" id="Seg_3982" s="T192">v:pred 0.3.h:S</ta>
            <ta e="T194" id="Seg_3983" s="T193">v:pred 0.3.h:S</ta>
            <ta e="T196" id="Seg_3984" s="T195">v:pred 0.3.h:S</ta>
            <ta e="T197" id="Seg_3985" s="T196">np.h:O</ta>
            <ta e="T199" id="Seg_3986" s="T198">conv:pred</ta>
            <ta e="T200" id="Seg_3987" s="T199">v:pred 0.3.h:S</ta>
            <ta e="T201" id="Seg_3988" s="T200">v:pred 0.3.h:S</ta>
            <ta e="T203" id="Seg_3989" s="T202">v:pred 0.2.h:S</ta>
            <ta e="T206" id="Seg_3990" s="T205">v:pred 0.2.h:S</ta>
            <ta e="T207" id="Seg_3991" s="T206">np.h:S</ta>
            <ta e="T208" id="Seg_3992" s="T207">v:pred</ta>
            <ta e="T209" id="Seg_3993" s="T208">pro.h:S</ta>
            <ta e="T210" id="Seg_3994" s="T209">v:pred</ta>
            <ta e="T211" id="Seg_3995" s="T210">np.h:S</ta>
            <ta e="T213" id="Seg_3996" s="T212">v:pred</ta>
            <ta e="T215" id="Seg_3997" s="T214">np:O</ta>
            <ta e="T216" id="Seg_3998" s="T215">conv:pred</ta>
            <ta e="T217" id="Seg_3999" s="T216">v:pred 0.3.h:S</ta>
            <ta e="T218" id="Seg_4000" s="T217">v:pred 0.3.h:S</ta>
            <ta e="T220" id="Seg_4001" s="T219">v:pred 0.3.h:S</ta>
            <ta e="T222" id="Seg_4002" s="T221">np:S</ta>
            <ta e="T223" id="Seg_4003" s="T222">v:pred</ta>
            <ta e="T224" id="Seg_4004" s="T223">np:S</ta>
            <ta e="T227" id="Seg_4005" s="T226">v:pred</ta>
            <ta e="T228" id="Seg_4006" s="T227">pro.h:S</ta>
            <ta e="T230" id="Seg_4007" s="T229">v:pred</ta>
            <ta e="T231" id="Seg_4008" s="T230">v:pred 0.3.h:S</ta>
            <ta e="T236" id="Seg_4009" s="T235">np:S</ta>
            <ta e="T237" id="Seg_4010" s="T236">v:pred</ta>
            <ta e="T238" id="Seg_4011" s="T237">pro.h:S</ta>
            <ta e="T239" id="Seg_4012" s="T238">v:pred</ta>
            <ta e="T241" id="Seg_4013" s="T240">np.h:S</ta>
            <ta e="T242" id="Seg_4014" s="T241">v:pred</ta>
            <ta e="T245" id="Seg_4015" s="T242">s:temp</ta>
            <ta e="T246" id="Seg_4016" s="T245">conv:pred</ta>
            <ta e="T247" id="Seg_4017" s="T246">v:pred 0.3.h:S</ta>
            <ta e="T251" id="Seg_4018" s="T250">np.h:S</ta>
            <ta e="T252" id="Seg_4019" s="T251">v:pred</ta>
            <ta e="T253" id="Seg_4020" s="T252">pro.h:S</ta>
            <ta e="T254" id="Seg_4021" s="T253">np.h:O</ta>
            <ta e="T256" id="Seg_4022" s="T255">conv:pred</ta>
            <ta e="T257" id="Seg_4023" s="T256">v:pred</ta>
            <ta e="T260" id="Seg_4024" s="T259">np.h:S</ta>
            <ta e="T261" id="Seg_4025" s="T260">v:pred</ta>
            <ta e="T262" id="Seg_4026" s="T261">pro.h:S</ta>
            <ta e="T263" id="Seg_4027" s="T262">ptcl.neg</ta>
            <ta e="T264" id="Seg_4028" s="T263">v:pred</ta>
            <ta e="T266" id="Seg_4029" s="T265">np.h:S</ta>
            <ta e="T267" id="Seg_4030" s="T266">np:O</ta>
            <ta e="T268" id="Seg_4031" s="T267">v:pred</ta>
            <ta e="T269" id="Seg_4032" s="T268">conv:pred</ta>
            <ta e="T270" id="Seg_4033" s="T269">conv:pred</ta>
            <ta e="T271" id="Seg_4034" s="T270">v:pred 0.3.h:S</ta>
            <ta e="T272" id="Seg_4035" s="T271">v:pred 0.3.h:S</ta>
            <ta e="T276" id="Seg_4036" s="T275">conv:pred</ta>
            <ta e="T277" id="Seg_4037" s="T276">v:pred 0.3.h:S</ta>
            <ta e="T280" id="Seg_4038" s="T277">s:temp</ta>
            <ta e="T281" id="Seg_4039" s="T280">conv:pred</ta>
            <ta e="T282" id="Seg_4040" s="T281">v:pred</ta>
            <ta e="T283" id="Seg_4041" s="T282">conv:pred</ta>
            <ta e="T284" id="Seg_4042" s="T283">v:pred 0.3.h:S</ta>
            <ta e="T288" id="Seg_4043" s="T287">v:pred 0.3.h:S</ta>
            <ta e="T289" id="Seg_4044" s="T288">np.h:S</ta>
            <ta e="T290" id="Seg_4045" s="T289">v:pred</ta>
            <ta e="T291" id="Seg_4046" s="T290">pro.h:O</ta>
            <ta e="T292" id="Seg_4047" s="T291">v:pred 0.3.h:S</ta>
            <ta e="T293" id="Seg_4048" s="T292">v:pred 0.3.h:S</ta>
            <ta e="T294" id="Seg_4049" s="T293">pro.h:S</ta>
            <ta e="T296" id="Seg_4050" s="T295">n:pred</ta>
            <ta e="T297" id="Seg_4051" s="T296">cop</ta>
            <ta e="T298" id="Seg_4052" s="T297">ptcl.neg</ta>
            <ta e="T299" id="Seg_4053" s="T298">v:pred 0.2.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T1" id="Seg_4054" s="T0">new</ta>
            <ta e="T2" id="Seg_4055" s="T1">new</ta>
            <ta e="T5" id="Seg_4056" s="T4">new</ta>
            <ta e="T8" id="Seg_4057" s="T7">giv-active</ta>
            <ta e="T11" id="Seg_4058" s="T10">accs-inf </ta>
            <ta e="T14" id="Seg_4059" s="T13">new</ta>
            <ta e="T15" id="Seg_4060" s="T14">0.giv-active </ta>
            <ta e="T16" id="Seg_4061" s="T15">giv-act</ta>
            <ta e="T17" id="Seg_4062" s="T16">0.giv-active 0.giv-active</ta>
            <ta e="T19" id="Seg_4063" s="T18">0.giv-active </ta>
            <ta e="T20" id="Seg_4064" s="T19">accs-sit </ta>
            <ta e="T21" id="Seg_4065" s="T20">giv-active </ta>
            <ta e="T23" id="Seg_4066" s="T22">0.giv-active </ta>
            <ta e="T25" id="Seg_4067" s="T24">new </ta>
            <ta e="T26" id="Seg_4068" s="T25">giv-active </ta>
            <ta e="T28" id="Seg_4069" s="T27">giv-inactive </ta>
            <ta e="T29" id="Seg_4070" s="T28">quot-sp </ta>
            <ta e="T31" id="Seg_4071" s="T30">accs-gen-Q </ta>
            <ta e="T32" id="Seg_4072" s="T31">giv-active-Q </ta>
            <ta e="T34" id="Seg_4073" s="T33">giv-active-Q </ta>
            <ta e="T36" id="Seg_4074" s="T35">giv-active-Q </ta>
            <ta e="T38" id="Seg_4075" s="T37">giv-active-Q </ta>
            <ta e="T40" id="Seg_4076" s="T39">accs-sit </ta>
            <ta e="T41" id="Seg_4077" s="T40">0.giv-active </ta>
            <ta e="T43" id="Seg_4078" s="T42">0.giv-active </ta>
            <ta e="T44" id="Seg_4079" s="T43">giv-active</ta>
            <ta e="T47" id="Seg_4080" s="T46">giv-active </ta>
            <ta e="T49" id="Seg_4081" s="T48">accs-sit</ta>
            <ta e="T50" id="Seg_4082" s="T49">0.giv-active</ta>
            <ta e="T52" id="Seg_4083" s="T51">giv-active</ta>
            <ta e="T54" id="Seg_4084" s="T53">giv-inactive</ta>
            <ta e="T55" id="Seg_4085" s="T54">giv-active</ta>
            <ta e="T59" id="Seg_4086" s="T58">giv-active</ta>
            <ta e="T61" id="Seg_4087" s="T60">accs-inf</ta>
            <ta e="T62" id="Seg_4088" s="T61">giv-inactive</ta>
            <ta e="T64" id="Seg_4089" s="T63">giv-active</ta>
            <ta e="T65" id="Seg_4090" s="T64">0.giv-active-Q</ta>
            <ta e="T66" id="Seg_4091" s="T65">new-Q</ta>
            <ta e="T68" id="Seg_4092" s="T67">0.giv-active</ta>
            <ta e="T69" id="Seg_4093" s="T68">0.giv-active</ta>
            <ta e="T70" id="Seg_4094" s="T69">0.giv-active</ta>
            <ta e="T72" id="Seg_4095" s="T71">new</ta>
            <ta e="T74" id="Seg_4096" s="T73">giv-active</ta>
            <ta e="T76" id="Seg_4097" s="T75">accs-inf</ta>
            <ta e="T78" id="Seg_4098" s="T77">0.giv-active</ta>
            <ta e="T81" id="Seg_4099" s="T80">new</ta>
            <ta e="T83" id="Seg_4100" s="T82">giv-active</ta>
            <ta e="T85" id="Seg_4101" s="T84">giv-active</ta>
            <ta e="T86" id="Seg_4102" s="T85">accs-inf</ta>
            <ta e="T89" id="Seg_4103" s="T88">new-Q</ta>
            <ta e="T90" id="Seg_4104" s="T89">new-Q</ta>
            <ta e="T92" id="Seg_4105" s="T91">giv-inactive</ta>
            <ta e="T94" id="Seg_4106" s="T93">giv-inactive</ta>
            <ta e="T96" id="Seg_4107" s="T95">0.giv-active</ta>
            <ta e="T101" id="Seg_4108" s="T100">new</ta>
            <ta e="T104" id="Seg_4109" s="T103">0.giv-inactive</ta>
            <ta e="T105" id="Seg_4110" s="T104">accs-sit</ta>
            <ta e="T107" id="Seg_4111" s="T106">accs-inf</ta>
            <ta e="T111" id="Seg_4112" s="T110">new-Q</ta>
            <ta e="T112" id="Seg_4113" s="T111">quot-sp</ta>
            <ta e="T113" id="Seg_4114" s="T112">accs-sit-Q</ta>
            <ta e="T114" id="Seg_4115" s="T113">accs-sit-Q</ta>
            <ta e="T116" id="Seg_4116" s="T115">giv-inactive</ta>
            <ta e="T120" id="Seg_4117" s="T119">giv-inactive</ta>
            <ta e="T121" id="Seg_4118" s="T120">quot-sp</ta>
            <ta e="T122" id="Seg_4119" s="T121">quot-sp</ta>
            <ta e="T124" id="Seg_4120" s="T123">accs-sit-Q</ta>
            <ta e="T125" id="Seg_4121" s="T124">0.giv-inactive</ta>
            <ta e="T127" id="Seg_4122" s="T126">accs-sit-Q</ta>
            <ta e="T132" id="Seg_4123" s="T131">new</ta>
            <ta e="T134" id="Seg_4124" s="T133">giv-inactive</ta>
            <ta e="T135" id="Seg_4125" s="T134">giv-inactive</ta>
            <ta e="T138" id="Seg_4126" s="T137">accs-sit</ta>
            <ta e="T142" id="Seg_4127" s="T141">0.giv-inactive</ta>
            <ta e="T144" id="Seg_4128" s="T143">accs-sit-Q</ta>
            <ta e="T145" id="Seg_4129" s="T144">0.giv-active-Q</ta>
            <ta e="T148" id="Seg_4130" s="T147">giv-active-Q</ta>
            <ta e="T149" id="Seg_4131" s="T148">0.giv-active-Q</ta>
            <ta e="T150" id="Seg_4132" s="T149">giv-active</ta>
            <ta e="T151" id="Seg_4133" s="T150">new</ta>
            <ta e="T152" id="Seg_4134" s="T151">0.giv-inactive</ta>
            <ta e="T153" id="Seg_4135" s="T152">0.giv-active</ta>
            <ta e="T154" id="Seg_4136" s="T153">new</ta>
            <ta e="T156" id="Seg_4137" s="T155">0.giv-active</ta>
            <ta e="T157" id="Seg_4138" s="T156">giv-active</ta>
            <ta e="T159" id="Seg_4139" s="T158">accs-inf</ta>
            <ta e="T160" id="Seg_4140" s="T159">giv-active</ta>
            <ta e="T161" id="Seg_4141" s="T160">new</ta>
            <ta e="T162" id="Seg_4142" s="T161">0.giv-active</ta>
            <ta e="T164" id="Seg_4143" s="T163">giv-active-Q</ta>
            <ta e="T166" id="Seg_4144" s="T165">0.giv-inactive-Q</ta>
            <ta e="T167" id="Seg_4145" s="T166">0.giv-inactive-Q</ta>
            <ta e="T168" id="Seg_4146" s="T167">0.giv-active-Q</ta>
            <ta e="T169" id="Seg_4147" s="T168">giv-active-Q</ta>
            <ta e="T171" id="Seg_4148" s="T170">quot-sp</ta>
            <ta e="T172" id="Seg_4149" s="T171">giv-active</ta>
            <ta e="T176" id="Seg_4150" s="T175">accs-sit</ta>
            <ta e="T181" id="Seg_4151" s="T180">giv-inactive</ta>
            <ta e="T183" id="Seg_4152" s="T182">accs-inf</ta>
            <ta e="T184" id="Seg_4153" s="T183">accs-inf</ta>
            <ta e="T186" id="Seg_4154" s="T185">accs-inf</ta>
            <ta e="T189" id="Seg_4155" s="T188">giv-inactive</ta>
            <ta e="T193" id="Seg_4156" s="T192">0.giv-inactive</ta>
            <ta e="T194" id="Seg_4157" s="T193">0.giv-active</ta>
            <ta e="T195" id="Seg_4158" s="T194">giv-inactive</ta>
            <ta e="T196" id="Seg_4159" s="T195">0.giv-active</ta>
            <ta e="T197" id="Seg_4160" s="T196">giv-active</ta>
            <ta e="T198" id="Seg_4161" s="T197">accs-sit</ta>
            <ta e="T200" id="Seg_4162" s="T199">0.giv-active</ta>
            <ta e="T201" id="Seg_4163" s="T200">0.giv-activ quot-sp</ta>
            <ta e="T202" id="Seg_4164" s="T201">giv-active-Q</ta>
            <ta e="T203" id="Seg_4165" s="T202">0.giv-inactive-Q</ta>
            <ta e="T205" id="Seg_4166" s="T204">accs-sit-Q</ta>
            <ta e="T206" id="Seg_4167" s="T205">0.giv-active</ta>
            <ta e="T207" id="Seg_4168" s="T206">giv-active</ta>
            <ta e="T208" id="Seg_4169" s="T207">quot-sp</ta>
            <ta e="T209" id="Seg_4170" s="T208">giv-active-Q</ta>
            <ta e="T211" id="Seg_4171" s="T210">giv-inactive</ta>
            <ta e="T212" id="Seg_4172" s="T211">accs-sit</ta>
            <ta e="T215" id="Seg_4173" s="T214">giv-inactive</ta>
            <ta e="T217" id="Seg_4174" s="T216">0.giv-inactive</ta>
            <ta e="T218" id="Seg_4175" s="T217">0.giv-active</ta>
            <ta e="T219" id="Seg_4176" s="T218">accs-sit</ta>
            <ta e="T220" id="Seg_4177" s="T219">0.giv-active</ta>
            <ta e="T222" id="Seg_4178" s="T221">new</ta>
            <ta e="T224" id="Seg_4179" s="T223">accs-sit</ta>
            <ta e="T228" id="Seg_4180" s="T227">giv-inactive</ta>
            <ta e="T229" id="Seg_4181" s="T228">giv-inactive</ta>
            <ta e="T231" id="Seg_4182" s="T230">0.giv-active quot-sp</ta>
            <ta e="T233" id="Seg_4183" s="T232">new-Q</ta>
            <ta e="T235" id="Seg_4184" s="T234">new-Q</ta>
            <ta e="T236" id="Seg_4185" s="T235">giv-inactive-Q</ta>
            <ta e="T238" id="Seg_4186" s="T237">accs-aggr</ta>
            <ta e="T239" id="Seg_4187" s="T238">quot-sp</ta>
            <ta e="T241" id="Seg_4188" s="T240">new-Q/0.giv-active-Q</ta>
            <ta e="T243" id="Seg_4189" s="T242">accs-gen-Q</ta>
            <ta e="T244" id="Seg_4190" s="T243">accs-sit-Q</ta>
            <ta e="T247" id="Seg_4191" s="T246">0.giv-active-Q</ta>
            <ta e="T248" id="Seg_4192" s="T247">accs-sit-Q</ta>
            <ta e="T249" id="Seg_4193" s="T248">giv-active-Q</ta>
            <ta e="T251" id="Seg_4194" s="T250">new-Q</ta>
            <ta e="T253" id="Seg_4195" s="T252">giv-active-Q</ta>
            <ta e="T254" id="Seg_4196" s="T253">giv-inactive-Q</ta>
            <ta e="T260" id="Seg_4197" s="T259">giv-active-Q</ta>
            <ta e="T262" id="Seg_4198" s="T261">giv-inactive-Q</ta>
            <ta e="T265" id="Seg_4199" s="T264">giv-inactive</ta>
            <ta e="T266" id="Seg_4200" s="T265">giv-inactive</ta>
            <ta e="T267" id="Seg_4201" s="T266">giv-inactive</ta>
            <ta e="T271" id="Seg_4202" s="T270">0.giv-active</ta>
            <ta e="T272" id="Seg_4203" s="T271">0.giv-active</ta>
            <ta e="T274" id="Seg_4204" s="T273">accs-gen</ta>
            <ta e="T275" id="Seg_4205" s="T274">giv-active</ta>
            <ta e="T277" id="Seg_4206" s="T276">0.giv-active</ta>
            <ta e="T278" id="Seg_4207" s="T277">giv-inactive</ta>
            <ta e="T279" id="Seg_4208" s="T278">giv-active</ta>
            <ta e="T282" id="Seg_4209" s="T281">0.giv-active</ta>
            <ta e="T284" id="Seg_4210" s="T283">0.giv-active</ta>
            <ta e="T286" id="Seg_4211" s="T285">giv-active</ta>
            <ta e="T288" id="Seg_4212" s="T287">0.giv-active</ta>
            <ta e="T289" id="Seg_4213" s="T288">giv-active</ta>
            <ta e="T291" id="Seg_4214" s="T290">giv-active</ta>
            <ta e="T292" id="Seg_4215" s="T291">0.giv-active</ta>
            <ta e="T293" id="Seg_4216" s="T292">0.giv-active/quot-sp</ta>
            <ta e="T294" id="Seg_4217" s="T293">giv-active-Q</ta>
            <ta e="T296" id="Seg_4218" s="T295">giv-active-Q</ta>
            <ta e="T299" id="Seg_4219" s="T298">0.giv-active-Q</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T174" id="Seg_4220" s="T173">RUS:gram</ta>
            <ta e="T204" id="Seg_4221" s="T203">RUS:gram</ta>
            <ta e="T225" id="Seg_4222" s="T224">RUS:gram</ta>
            <ta e="T255" id="Seg_4223" s="T254">TURK:disc</ta>
            <ta e="T258" id="Seg_4224" s="T257">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="fr" tierref="fr">
            <ta e="T3" id="Seg_4225" s="T0">Жили старуха со стариком.</ta>
            <ta e="T6" id="Seg_4226" s="T3">Было у них два сына.</ta>
            <ta e="T13" id="Seg_4227" s="T6">Сыновья их, играя, [другим] детям руки отрывают. </ta>
            <ta e="T17" id="Seg_4228" s="T13">[Люди] плот построили, к нему их привязали.</ta>
            <ta e="T20" id="Seg_4229" s="T17">В воду его пустили.</ta>
            <ta e="T25" id="Seg_4230" s="T20">Течением принесло их под утёс. </ta>
            <ta e="T27" id="Seg_4231" s="T25">Вода прибывает [в плот через брёвна?].</ta>
            <ta e="T38" id="Seg_4232" s="T27">Они закричали: "Матерь Божья, мать наша, матерь Божья, вытащи нас, матерь Божья!"</ta>
            <ta e="T43" id="Seg_4233" s="T38">Расплела она косу с левой стороны, детям сбросила.</ta>
            <ta e="T47" id="Seg_4234" s="T43">Не достали её волосы до детей.</ta>
            <ta e="T50" id="Seg_4235" s="T47">С правой стороны косу расплела.</ta>
            <ta e="T54" id="Seg_4236" s="T50">С правой стороны волосы достали до детей.</ta>
            <ta e="T59" id="Seg_4237" s="T54">Дети подобрали волосы, ухватились за них и выбрались оттуда. </ta>
            <ta e="T64" id="Seg_4238" s="T59">Вытащила она их на вершину утёса и отправила их:</ta>
            <ta e="T67" id="Seg_4239" s="T64">"Идите по дороге!"</ta>
            <ta e="T68" id="Seg_4240" s="T67">Они пошли.</ta>
            <ta e="T69" id="Seg_4241" s="T68">Шли они.</ta>
            <ta e="T73" id="Seg_4242" s="T69">Шли они, вышли на широкую дорогу. </ta>
            <ta e="T77" id="Seg_4243" s="T73">Сели у края дороги.</ta>
            <ta e="T82" id="Seg_4244" s="T77">Видят, герой на гнедом коне скачет.</ta>
            <ta e="T84" id="Seg_4245" s="T82">Они свистнули.</ta>
            <ta e="T88" id="Seg_4246" s="T84">Опустился его конь на колени.</ta>
            <ta e="T91" id="Seg_4247" s="T88">"Злой дух, леший это свистит."</ta>
            <ta e="T97" id="Seg_4248" s="T91">Сам он не пошёл [к ним], мимо прошёл.</ta>
            <ta e="T102" id="Seg_4249" s="T97">Снова скачет герой, на чёрном коне. </ta>
            <ta e="T104" id="Seg_4250" s="T102">Снова они засвистели.</ta>
            <ta e="T109" id="Seg_4251" s="T104">Снова его конь упал на колени.</ta>
            <ta e="T112" id="Seg_4252" s="T109">Снова он говорит: "Дух".</ta>
            <ta e="T115" id="Seg_4253" s="T112">"Сам леший это свистит".</ta>
            <ta e="T118" id="Seg_4254" s="T115">Сам снова мимо прошёл.</ta>
            <ta e="T122" id="Seg_4255" s="T118">Дети [ему] закричали, они кричат:</ta>
            <ta e="T128" id="Seg_4256" s="T122">"Будет у тебя дорога вперёд, назад дороги не будет!"</ta>
            <ta e="T133" id="Seg_4257" s="T128">Снова скачет герой, на белом коне.</ta>
            <ta e="T137" id="Seg_4258" s="T133">Дети снова засвистели.</ta>
            <ta e="T139" id="Seg_4259" s="T137">Конь его остановился.</ta>
            <ta e="T142" id="Seg_4260" s="T139">Тогда он пошёл и увидел [их]:</ta>
            <ta e="T145" id="Seg_4261" s="T142">"Что вы за дети?" </ta>
            <ta e="T149" id="Seg_4262" s="T145">"Мы такие-то такие-то дети."</ta>
            <ta e="T155" id="Seg_4263" s="T149">Разжег он детям огонь, пошёл, лося подстрелил.</ta>
            <ta e="T162" id="Seg_4264" s="T155">Принёс детям лося, из лосиной шкуры чум детям сделал.</ta>
            <ta e="T164" id="Seg_4265" s="T162">"Живите тут, дети!</ta>
            <ta e="T169" id="Seg_4266" s="T164">Если живым вернусь, возьму вас с собой".</ta>
            <ta e="T171" id="Seg_4267" s="T169">Так сказал.</ta>
            <ta e="T182" id="Seg_4268" s="T171">Сам ушёл, а на третий день две [первые] лошади без всадников возвращаются.</ta>
            <ta e="T188" id="Seg_4269" s="T182">Идут, сёдла и стремена на животе [у лошадей] болтаются.</ta>
            <ta e="T193" id="Seg_4270" s="T188">За ними [всадник] на белом коне возвращается.</ta>
            <ta e="T200" id="Seg_4271" s="T193">Приехал он к детям, взял их и привёл к себе домой.</ta>
            <ta e="T201" id="Seg_4272" s="T200">Говорит им: </ta>
            <ta e="T206" id="Seg_4273" s="T201">"Станете у меня жить или к себе домой пойдёте?"</ta>
            <ta e="T208" id="Seg_4274" s="T206">Один отвечает:</ta>
            <ta e="T210" id="Seg_4275" s="T208">"Я пойду [домой]".</ta>
            <ta e="T213" id="Seg_4276" s="T210">Другой там остался.</ta>
            <ta e="T217" id="Seg_4277" s="T213">[Герой для первого] запряг лошадь [одного из героев].</ta>
            <ta e="T220" id="Seg_4278" s="T217">Поехал он, домой приехал.</ta>
            <ta e="T223" id="Seg_4279" s="T220">Одинокий чум стоит.</ta>
            <ta e="T227" id="Seg_4280" s="T223">Деревни его, как была раньше, больше нет.</ta>
            <ta e="T230" id="Seg_4281" s="T227">Вошел он в этот чум.</ta>
            <ta e="T231" id="Seg_4282" s="T230">Спрашивает:</ta>
            <ta e="T237" id="Seg_4283" s="T231">"Старуха, старик, куда деревня подевалась?"</ta>
            <ta e="T239" id="Seg_4284" s="T237">Отвечают они:</ta>
            <ta e="T242" id="Seg_4285" s="T239">"Было у нас двое сыновей.</ta>
            <ta e="T248" id="Seg_4286" s="T242">Привязали их люди к плоту, пустили по реке.</ta>
            <ta e="T252" id="Seg_4287" s="T248">После них дочь у нас была.</ta>
            <ta e="T257" id="Seg_4288" s="T252">Она всех этих людей убила."</ta>
            <ta e="T261" id="Seg_4289" s="T257">"Куда же ушла ваша дочь?"</ta>
            <ta e="T264" id="Seg_4290" s="T261">"Сами не знаем."</ta>
            <ta e="T271" id="Seg_4291" s="T264">Сел парень на коня, уехал искать её.</ta>
            <ta e="T274" id="Seg_4292" s="T271">Поехал в широкую степь.</ta>
            <ta e="T277" id="Seg_4293" s="T274">Там галопом скакал.</ta>
            <ta e="T282" id="Seg_4294" s="T277">Увидев парня, [она к нему] галопом прискакала.</ta>
            <ta e="T286" id="Seg_4295" s="T282">На скаку на парня напала.</ta>
            <ta e="T288" id="Seg_4296" s="T286">Затем стали бороться.</ta>
            <ta e="T291" id="Seg_4297" s="T288">Брат её повалил.</ta>
            <ta e="T293" id="Seg_4298" s="T291">Сжал её крепко и говорит:</ta>
            <ta e="T297" id="Seg_4299" s="T293">"Я твой брат.</ta>
            <ta e="T299" id="Seg_4300" s="T297">Не борись со мной!"</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_4301" s="T0">There lived an old woman and an old man.</ta>
            <ta e="T6" id="Seg_4302" s="T3">They had two sons.</ta>
            <ta e="T13" id="Seg_4303" s="T6">While playing, these sons of them will tear off [other] children’s hands.</ta>
            <ta e="T17" id="Seg_4304" s="T13">[The people] made a raft, tied them to the raft.</ta>
            <ta e="T20" id="Seg_4305" s="T17">They let it into the river.</ta>
            <ta e="T25" id="Seg_4306" s="T20">With the river flowing, they came under a cliff.</ta>
            <ta e="T27" id="Seg_4307" s="T25">Water is coming in.</ta>
            <ta e="T38" id="Seg_4308" s="T27">They called out: "Mother of God, our mother, Mother of God, get us out of here, Mother of God!"</ta>
            <ta e="T43" id="Seg_4309" s="T38">She tied her left hair [plaid] open, let it [down] to the children.</ta>
            <ta e="T47" id="Seg_4310" s="T43">Her hair did not reach the children.</ta>
            <ta e="T50" id="Seg_4311" s="T47">She tied her right side open.</ta>
            <ta e="T54" id="Seg_4312" s="T50">Her right hair did reach to the children.</ta>
            <ta e="T59" id="Seg_4313" s="T54">The children reached for it, holding it they went off from there.</ta>
            <ta e="T64" id="Seg_4314" s="T59">She lifted them up on the cliff and sent them off:</ta>
            <ta e="T67" id="Seg_4315" s="T64">"Go along the road!"</ta>
            <ta e="T68" id="Seg_4316" s="T67">They got going.</ta>
            <ta e="T69" id="Seg_4317" s="T68">They walked.</ta>
            <ta e="T73" id="Seg_4318" s="T69">They walked, came upon a big road.</ta>
            <ta e="T77" id="Seg_4319" s="T73">They sit at the side of the road.</ta>
            <ta e="T82" id="Seg_4320" s="T77">As they saw, a hero with a foxy red horse was coming.</ta>
            <ta e="T84" id="Seg_4321" s="T82">They whistled.</ta>
            <ta e="T88" id="Seg_4322" s="T84">His horse sank on its knees.</ta>
            <ta e="T91" id="Seg_4323" s="T88">"That's a spirit, a puck whistling."</ta>
            <ta e="T97" id="Seg_4324" s="T91">He himself did not go [to them], he went off passing by.</ta>
            <ta e="T102" id="Seg_4325" s="T97">Again a hero is coming, one with a black horse.</ta>
            <ta e="T104" id="Seg_4326" s="T102">Again they whistled.</ta>
            <ta e="T109" id="Seg_4327" s="T104">Again, his horse came down on its knees.</ta>
            <ta e="T112" id="Seg_4328" s="T109">Again he says: "A spirit".</ta>
            <ta e="T115" id="Seg_4329" s="T112">"The puck himself is whistling."</ta>
            <ta e="T118" id="Seg_4330" s="T115">He himself again passed by.</ta>
            <ta e="T122" id="Seg_4331" s="T118">The children shouted, they shout:</ta>
            <ta e="T128" id="Seg_4332" s="T122">"You shall have a departure, but you shall not have an arrival!"</ta>
            <ta e="T133" id="Seg_4333" s="T128">Again a hero with a white horse is coming.</ta>
            <ta e="T137" id="Seg_4334" s="T133">The children whistled again.</ta>
            <ta e="T139" id="Seg_4335" s="T137">His horse stopped.</ta>
            <ta e="T142" id="Seg_4336" s="T139">Then he went and saw [them].</ta>
            <ta e="T145" id="Seg_4337" s="T142">"What kind of children are you?"</ta>
            <ta e="T149" id="Seg_4338" s="T145">"We are such and such children."</ta>
            <ta e="T155" id="Seg_4339" s="T149">He made a fire for the children, went and shot a moose.</ta>
            <ta e="T162" id="Seg_4340" s="T155">He brought it to the children, and from the moose’s skin he made a tent for the children.</ta>
            <ta e="T164" id="Seg_4341" s="T162">"Live here, children!</ta>
            <ta e="T169" id="Seg_4342" s="T164">If I’m alive and return, I will take you".</ta>
            <ta e="T171" id="Seg_4343" s="T169">So he said.</ta>
            <ta e="T182" id="Seg_4344" s="T171">He himself went off, and on the third day the two heroes’ horses are returning riderless.</ta>
            <ta e="T188" id="Seg_4345" s="T182">Their saddles and stirrups dangling at the horse’s bellies, they are walking.</ta>
            <ta e="T193" id="Seg_4346" s="T188">Behind these the one with the white horse is returning.</ta>
            <ta e="T200" id="Seg_4347" s="T193">He came to the children, took the children and led them to his home.</ta>
            <ta e="T201" id="Seg_4348" s="T200">He says:</ta>
            <ta e="T206" id="Seg_4349" s="T201">"Will you live [with] me, or will you go to your home?"</ta>
            <ta e="T208" id="Seg_4350" s="T206">One [of them] says:</ta>
            <ta e="T210" id="Seg_4351" s="T208">"I will go."</ta>
            <ta e="T213" id="Seg_4352" s="T210">One [other] stayed there.</ta>
            <ta e="T217" id="Seg_4353" s="T213">[The hero] saddled a hero’s horse [for the first one].</ta>
            <ta e="T220" id="Seg_4354" s="T217">He got going, came home.</ta>
            <ta e="T223" id="Seg_4355" s="T220">A single tent is standing.</ta>
            <ta e="T227" id="Seg_4356" s="T223">His village, as it used to be, is not there.</ta>
            <ta e="T230" id="Seg_4357" s="T227">He entered this tent.</ta>
            <ta e="T231" id="Seg_4358" s="T230">He asks:</ta>
            <ta e="T237" id="Seg_4359" s="T231">"Old woman, old man, where has your village moved to?"</ta>
            <ta e="T239" id="Seg_4360" s="T237">They say:</ta>
            <ta e="T242" id="Seg_4361" s="T239">"We had two sons.</ta>
            <ta e="T248" id="Seg_4362" s="T242">The people tied them onto a raft and committed them to the river.</ta>
            <ta e="T252" id="Seg_4363" s="T248">After them we had a daughter.</ta>
            <ta e="T257" id="Seg_4364" s="T252">She killed all these people."</ta>
            <ta e="T261" id="Seg_4365" s="T257">"But where did your daughter move to?"</ta>
            <ta e="T264" id="Seg_4366" s="T261">"We ourselves do not know."</ta>
            <ta e="T271" id="Seg_4367" s="T264">The boy mounted his horse and went away searching.</ta>
            <ta e="T274" id="Seg_4368" s="T271">He went into the big steppe.</ta>
            <ta e="T277" id="Seg_4369" s="T274">There he was galloping around.</ta>
            <ta e="T282" id="Seg_4370" s="T277">As she saw the boy, she came galloping [to him].</ta>
            <ta e="T286" id="Seg_4371" s="T282">She jumped and clinged to the boy.</ta>
            <ta e="T288" id="Seg_4372" s="T286">Then they fought.</ta>
            <ta e="T291" id="Seg_4373" s="T288">Her brother brought her down.</ta>
            <ta e="T293" id="Seg_4374" s="T291">He is holding her and says:</ta>
            <ta e="T297" id="Seg_4375" s="T293">I am your brother.</ta>
            <ta e="T299" id="Seg_4376" s="T297">Do not fight!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_4377" s="T0">Es lebten eine alte Frau und ein alter Mann.</ta>
            <ta e="T6" id="Seg_4378" s="T3">Sie hatten zwei Söhne. </ta>
            <ta e="T13" id="Seg_4379" s="T6">Diese Söhne reißen beim Spielen die Hände der [anderen] Kinder ab. </ta>
            <ta e="T17" id="Seg_4380" s="T13">[Die Leute] bauten ein Floß und banden sie auf das Floß.</ta>
            <ta e="T20" id="Seg_4381" s="T17">Sie ließen es zu Wasser.</ta>
            <ta e="T25" id="Seg_4382" s="T20">Mit der Flussströmung kommen sie an eine Felswand.</ta>
            <ta e="T27" id="Seg_4383" s="T25">Wasser dringt [durch die Stämme?].</ta>
            <ta e="T38" id="Seg_4384" s="T27">Sie schrien: „Mutter Gottes! Unsere Mutter! Mutter Gottes! Hol uns hier heraus, Mutter Gottes!“</ta>
            <ta e="T43" id="Seg_4385" s="T38">Sie flocht ihr Haar auf der linken Seite auf und ließ es hinab.</ta>
            <ta e="T47" id="Seg_4386" s="T43">Ihr Haar reichte nicht bis zu den Kindern.</ta>
            <ta e="T50" id="Seg_4387" s="T47">Sie flocht es auf der rechten Seite auf.</ta>
            <ta e="T54" id="Seg_4388" s="T50">Das Haar auf der rechten Seite reichte bis zu den Kindern.</ta>
            <ta e="T59" id="Seg_4389" s="T54">Die Kinder griffen danach, sie packten es und kamen von dort weg.</ta>
            <ta e="T64" id="Seg_4390" s="T59">Sie hob sie auf die Felswand und schickte sie fort: </ta>
            <ta e="T67" id="Seg_4391" s="T64">„Geht den Weg entlang!“</ta>
            <ta e="T68" id="Seg_4392" s="T67">Sie gingen los.</ta>
            <ta e="T69" id="Seg_4393" s="T68">Sie wanderten.</ta>
            <ta e="T73" id="Seg_4394" s="T69">Sie wanderten und kamen an eine große Straße.</ta>
            <ta e="T77" id="Seg_4395" s="T73">Sie setzen sich an den Straßenrand.</ta>
            <ta e="T82" id="Seg_4396" s="T77">Als sie sich umsahen, kam ein Held auf einem fuchsroten Pferd angeritten.</ta>
            <ta e="T84" id="Seg_4397" s="T82">Sie pfiffen.</ta>
            <ta e="T88" id="Seg_4398" s="T84">Sein Pferd sank auf die Knie.</ta>
            <ta e="T91" id="Seg_4399" s="T88">„Ein Geist, ein böser Geist pfeift da.“</ta>
            <ta e="T97" id="Seg_4400" s="T91">Er selbst ging nicht [zu ihnen], er ritt an ihnen vorbei.</ta>
            <ta e="T102" id="Seg_4401" s="T97">Wieder kommt ein Held angeritten, auf einem schwarzen Pferd.</ta>
            <ta e="T104" id="Seg_4402" s="T102">Wieder pfiffen sie.</ta>
            <ta e="T109" id="Seg_4403" s="T104">Wieder sank sein Pferd auf die Knie.</ta>
            <ta e="T112" id="Seg_4404" s="T109">Wieder sagt er: „Ein Geist.</ta>
            <ta e="T115" id="Seg_4405" s="T112">Der böse Geist selbst pfeift da.“</ta>
            <ta e="T118" id="Seg_4406" s="T115">Er selbst ritt wieder an ihnen vorbei.</ta>
            <ta e="T122" id="Seg_4407" s="T118">Die Kinder fluchten und fluchten:</ta>
            <ta e="T128" id="Seg_4408" s="T122">„Du magst fortgehen, doch wiederkehren sollst du nicht!“</ta>
            <ta e="T133" id="Seg_4409" s="T128">Wieder kommt ein Held angeritten, auf einem weißen Pferd.</ta>
            <ta e="T137" id="Seg_4410" s="T133">Wieder pfiffen die Kinder:</ta>
            <ta e="T139" id="Seg_4411" s="T137">Sein Pferd blieb stehen.</ta>
            <ta e="T142" id="Seg_4412" s="T139">Dann ging er hin und sah [sie]:</ta>
            <ta e="T145" id="Seg_4413" s="T142">„Was für Kinder seid ihr?“</ta>
            <ta e="T149" id="Seg_4414" s="T145">„Wir sind solche und solche Kinder.“</ta>
            <ta e="T155" id="Seg_4415" s="T149">Er machte den Kindern ein Feuer, ging und schoss einen Elch.</ta>
            <ta e="T162" id="Seg_4416" s="T155">Er brachte ihn den Kindern und machte den Kindern aus der Haut des Elches ein Zelt.</ta>
            <ta e="T164" id="Seg_4417" s="T162">„Bleibt hier, Kinder!</ta>
            <ta e="T169" id="Seg_4418" s="T164">Bleibe ich am Leben und kehre zurück, werde ich euch mitnehmen.“</ta>
            <ta e="T171" id="Seg_4419" s="T169">So sprach er.</ta>
            <ta e="T182" id="Seg_4420" s="T171">Er selbst ritt davon. Am dritten Tag kehrten die herrenlosen Pferde jener zwei Helden zurück.</ta>
            <ta e="T188" id="Seg_4421" s="T182">[Die Pferde] laufen, wobei die Sättel und Steigbügel am Bauch der Pferde baumeln.</ta>
            <ta e="T193" id="Seg_4422" s="T188">Nach ihnen kehrt der mit dem weißen Pferd zurück.</ta>
            <ta e="T200" id="Seg_4423" s="T193">Er kam zu den Kindern, nahm die Kinder und brachte sie zu sich nach Hause.</ta>
            <ta e="T201" id="Seg_4424" s="T200">Er sagt:</ta>
            <ta e="T206" id="Seg_4425" s="T201">„Werdet ihr bei mir wohnen oder werdet ihr nach Hause gehen?“</ta>
            <ta e="T208" id="Seg_4426" s="T206">Einer sagt:</ta>
            <ta e="T210" id="Seg_4427" s="T208">„Ich werde gehen.”</ta>
            <ta e="T213" id="Seg_4428" s="T210">Einer blieb dort.</ta>
            <ta e="T217" id="Seg_4429" s="T213">[Der Held] sattelte [ihm] eines der Heldenpferde.</ta>
            <ta e="T220" id="Seg_4430" s="T217">Er brach auf und kam nach Hause.</ta>
            <ta e="T223" id="Seg_4431" s="T220">Ein einzelnes Zelt stand da.</ta>
            <ta e="T227" id="Seg_4432" s="T223">Sein Dorf, wie es mal war, ist nicht mehr.</ta>
            <ta e="T230" id="Seg_4433" s="T227">Er betrat dieses Zelt.</ta>
            <ta e="T231" id="Seg_4434" s="T230">Er fragt:</ta>
            <ta e="T237" id="Seg_4435" s="T231">„Alte Frau, alter Mann, wo ist euer Dorf hin?“</ta>
            <ta e="T239" id="Seg_4436" s="T237">Sie antworten:</ta>
            <ta e="T242" id="Seg_4437" s="T239">„Wir hatten zwei Söhne.</ta>
            <ta e="T248" id="Seg_4438" s="T242">Nachdem die Leute sie auf ein Floß gebunden hatten, ließen sie es zu Wasser.</ta>
            <ta e="T252" id="Seg_4439" s="T248">Nach ihnen hatten wir ein Mädchen.</ta>
            <ta e="T257" id="Seg_4440" s="T252">Sie tötete all diese Leute.“</ta>
            <ta e="T261" id="Seg_4441" s="T257">„Aber, wo ist eure Tochter hin?“</ta>
            <ta e="T264" id="Seg_4442" s="T261">„Wir wissen es selbst nicht.“</ta>
            <ta e="T271" id="Seg_4443" s="T264">Der Junge setzte sich auf sein Pferd und machte sich auf die Suche.</ta>
            <ta e="T274" id="Seg_4444" s="T271">Er ritt in die große Steppe.</ta>
            <ta e="T277" id="Seg_4445" s="T274">Dort ritt er umher.</ta>
            <ta e="T282" id="Seg_4446" s="T277">Nachdem sie den Jungen gesehen hatte, kam sie angelaufen.</ta>
            <ta e="T286" id="Seg_4447" s="T282">Sie sprang den Jungen an und klammerte sich an ihn.</ta>
            <ta e="T288" id="Seg_4448" s="T286">Dann kämpften sie.</ta>
            <ta e="T291" id="Seg_4449" s="T288">Ihr Bruder brachte sie zu Fall.</ta>
            <ta e="T293" id="Seg_4450" s="T291">Er hält sie fest und sagt ihr:</ta>
            <ta e="T297" id="Seg_4451" s="T293">„Ich bin dein Bruder.</ta>
            <ta e="T299" id="Seg_4452" s="T297">Bekämpfe mich nicht!”</ta>
         </annotation>
         <annotation name="ltg" tierref="ltg">
            <ta e="T3" id="Seg_4453" s="T0">Ein altes Weib und ein alter Mann lebten.</ta>
            <ta e="T6" id="Seg_4454" s="T3">⌈lhre⌉ zwei Söhne hatten sie.</ta>
            <ta e="T13" id="Seg_4455" s="T6">Während diese ihre Söhne spielen, die Hände der Kinder reissen sie ab, die Füsse reissen sie ab.</ta>
            <ta e="T17" id="Seg_4456" s="T13">Ein Floss sie machten, auf das Floss sie [sie] banden,</ta>
            <ta e="T20" id="Seg_4457" s="T17">liessen auf das Wasser;</ta>
            <ta e="T25" id="Seg_4458" s="T20">beim Strömen des Wassers kommen sie ⌈sitzend⌉ unter eine steile Felswand.</ta>
            <ta e="T27" id="Seg_4459" s="T25">Das Wasser strömt.</ta>
            <ta e="T38" id="Seg_4460" s="T27">Sie schrien: »Gott[es] Tochter, Mutter-Gott[es] Tochter, nimm uns hinauf, Gott[es] Tochter!»</ta>
            <ta e="T43" id="Seg_4461" s="T38">Links [ihr] Haar flocht sie auf, liess es herab;</ta>
            <ta e="T47" id="Seg_4462" s="T43">zu den Kindern reichen nicht ihre Haare.</ta>
            <ta e="T50" id="Seg_4463" s="T47">Auf die rechte Seite wendete sie [sie],</ta>
            <ta e="T54" id="Seg_4464" s="T50">auf der rechten Seite reichte ihr Haar zu den Kindern.</ta>
            <ta e="T59" id="Seg_4465" s="T54">Hob die Kinder auf den Berg, festnehmend gingen sie fort.</ta>
            <ta e="T64" id="Seg_4466" s="T59">Zog [sie] auf die steile Felswand herauf, liess sie weg.</ta>
            <ta e="T67" id="Seg_4467" s="T64">»Gehet!» Den Weg entlang</ta>
            <ta e="T68" id="Seg_4468" s="T67">gingen sie.</ta>
            <ta e="T69" id="Seg_4469" s="T68">»Gehet!»</ta>
            <ta e="T73" id="Seg_4470" s="T69">Sie gingen. Ein grosser Weg kam [ihnen] entgegen.</ta>
            <ta e="T77" id="Seg_4471" s="T73">Sie setzen sich neben den Weg.</ta>
            <ta e="T82" id="Seg_4472" s="T77">Als sie sehen, kommt ein Held auf einem braunen Pferde.</ta>
            <ta e="T84" id="Seg_4473" s="T82">Sie pfiffen:</ta>
            <ta e="T88" id="Seg_4474" s="T84">sein Pferd fiel auf die Knie, sein Pferd legte sich lang hin.</ta>
            <ta e="T91" id="Seg_4475" s="T88">»Der böse Geist, der Teufel pfeift.»</ta>
            <ta e="T97" id="Seg_4476" s="T91">Er selbst ging nicht, ging fort.</ta>
            <ta e="T102" id="Seg_4477" s="T97">Wieder auf einem schwarzen Pferde kommt ein Held,</ta>
            <ta e="T104" id="Seg_4478" s="T102">wieder pfiffen sie,</ta>
            <ta e="T109" id="Seg_4479" s="T104">abermals sein Pferd auf die Knie ⌈kommend⌉ fiel.</ta>
            <ta e="T112" id="Seg_4480" s="T109">»Wieder der Teufel», sagt er,</ta>
            <ta e="T115" id="Seg_4481" s="T112">»pfeift, der böse Geist.»</ta>
            <ta e="T118" id="Seg_4482" s="T115">Er selbst wieder ging.</ta>
            <ta e="T122" id="Seg_4483" s="T118">Sie fluchten, fluchen:</ta>
            <ta e="T128" id="Seg_4484" s="T122">»Dein Hinweg möge sein, dein Herweg sei nicht!»</ta>
            <ta e="T133" id="Seg_4485" s="T128">Abermals auf einem weissen Pferd ein Held kommt.</ta>
            <ta e="T137" id="Seg_4486" s="T133">Diese Kinder pfiffen wiederum:</ta>
            <ta e="T139" id="Seg_4487" s="T137">sein Pferd blieb stehen.</ta>
            <ta e="T142" id="Seg_4488" s="T139">Dann gehend sah er:</ta>
            <ta e="T145" id="Seg_4489" s="T142">»Was für Kinder seid ihr?»</ta>
            <ta e="T149" id="Seg_4490" s="T145">»Solche, solche Kinder sind wir.»</ta>
            <ta e="T155" id="Seg_4491" s="T149">Den Kindern machte er Feuer, ging, schoss einen Elch,</ta>
            <ta e="T162" id="Seg_4492" s="T155">brachte den Kindern den Elch, machte aus der Haut den Kindern ein Zelt.</ta>
            <ta e="T164" id="Seg_4493" s="T162">»Setzt euch, Kinder!</ta>
            <ta e="T169" id="Seg_4494" s="T164">Wenn ich leben bleibe, bei meiner Rückkehr werde ich euch nehmen»,</ta>
            <ta e="T171" id="Seg_4495" s="T169">so sagte</ta>
            <ta e="T182" id="Seg_4496" s="T171">er selbst, ging fort. Am dritten Tage die leeren Pferde der zwei Helden zurückkehrend kommen,</ta>
            <ta e="T188" id="Seg_4497" s="T182">ihre Sättel, Steigbügel am Bauche des Pferdes hängend sie gehen.</ta>
            <ta e="T193" id="Seg_4498" s="T188">Hinter ihnen der mit dem weissen Pferde Versehene zurückkehrend sitzt;</ta>
            <ta e="T200" id="Seg_4499" s="T193">kam zu den Kindern, nahmen die Kinder, nach Hause bringend ging er,</ta>
            <ta e="T201" id="Seg_4500" s="T200">sagt:</ta>
            <ta e="T206" id="Seg_4501" s="T201">»Wohnt ihr bei mir oder geht ihr nach Hause zu den [eigenen] Leuten?»</ta>
            <ta e="T208" id="Seg_4502" s="T206">Der eine sagt:</ta>
            <ta e="T210" id="Seg_4503" s="T208">»Ich gehe».</ta>
            <ta e="T213" id="Seg_4504" s="T210">Der andere blieb da.</ta>
            <ta e="T217" id="Seg_4505" s="T213">Der Held liess das Pferd satteln.</ta>
            <ta e="T220" id="Seg_4506" s="T217">Ging, kam nach Hause.</ta>
            <ta e="T223" id="Seg_4507" s="T220">Einsam das Zelt steht,</ta>
            <ta e="T227" id="Seg_4508" s="T223">da das Dorf nicht ist.</ta>
            <ta e="T230" id="Seg_4509" s="T227">In dieses Zelt hinein ging er,</ta>
            <ta e="T231" id="Seg_4510" s="T230">fragt:</ta>
            <ta e="T237" id="Seg_4511" s="T231">»Alte, Alter, euer Dorf, wohin geriet es?»</ta>
            <ta e="T239" id="Seg_4512" s="T237">Sie antworten: </ta>
            <ta e="T242" id="Seg_4513" s="T239">»Zwei Knaben waren.</ta>
            <ta e="T248" id="Seg_4514" s="T242">Nachdem die Leute [sie] auf ein Floss gebunden hatten, liessen sie [sie] ins Wasser.</ta>
            <ta e="T252" id="Seg_4515" s="T248">Nach ihnen aber ein Mädchen war.</ta>
            <ta e="T257" id="Seg_4516" s="T252">Sie alles Volk erschlagend wanderte.»</ta>
            <ta e="T261" id="Seg_4517" s="T257">»Nun, wohin geriet eure Tochter?»</ta>
            <ta e="T264" id="Seg_4518" s="T261">»Selbst wissen wir [es] nicht.»</ta>
            <ta e="T271" id="Seg_4519" s="T264">Der Knabe setzte sich aufs Pferd, suchen ging er,</ta>
            <ta e="T274" id="Seg_4520" s="T271">ging auf die grosse Steppe.</ta>
            <ta e="T277" id="Seg_4521" s="T274">Dort trabend fand er.</ta>
            <ta e="T282" id="Seg_4522" s="T277">Nachdem sie den Knaben gesehen hatte, kam sie trabend,</ta>
            <ta e="T286" id="Seg_4523" s="T282">laufend ergriff sie den Knaben.</ta>
            <ta e="T288" id="Seg_4524" s="T286">Dann kämpften sie.</ta>
            <ta e="T291" id="Seg_4525" s="T288">Ihr Bruder brachte sie zu Fall,</ta>
            <ta e="T293" id="Seg_4526" s="T291">er hält sie fest, erzählt:</ta>
            <ta e="T297" id="Seg_4527" s="T293">»Ich bin dein Bruder,</ta>
            <ta e="T299" id="Seg_4528" s="T297">streite nicht!»</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T299" id="Seg_4529" s="T297">[AAV] NEG ptcl instead of NEG.AUX?</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltg"
                          display-name="ltg"
                          name="ltg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
