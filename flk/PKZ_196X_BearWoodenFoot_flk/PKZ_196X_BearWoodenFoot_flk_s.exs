<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID8F97247D-4430-185A-B9AE-51B463430072">
   <head>
      <meta-information>
         <project-name />
         <transcription-name />
         <referenced-file url="PKZ_196X_BearWoodenFoot_flk.wav" />
         <referenced-file url="PKZ_196X_BearWoodenFoot_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\KamasCorpus\flk\PKZ_196X_BearWoodenFoot_flk\PKZ_196X_BearWoodenFoot_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">225</ud-information>
            <ud-information attribute-name="# HIAT:w">147</ud-information>
            <ud-information attribute-name="# e">147</ud-information>
            <ud-information attribute-name="# HIAT:u">38</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PKZ">
            <abbreviation>PKZ</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T80" time="0.088" type="appl" />
         <tli id="T81" time="1.048" type="appl" />
         <tli id="T82" time="2.008" type="appl" />
         <tli id="T83" time="3.2532086316674675" />
         <tli id="T84" time="4.336" type="appl" />
         <tli id="T85" time="5.453124304721287" />
         <tli id="T86" time="6.277" type="appl" />
         <tli id="T87" time="7.011" type="appl" />
         <tli id="T88" time="7.744" type="appl" />
         <tli id="T89" time="8.592" type="appl" />
         <tli id="T90" time="9.28" type="appl" />
         <tli id="T91" time="9.968" type="appl" />
         <tli id="T92" time="10.656" type="appl" />
         <tli id="T93" time="12.224" type="appl" />
         <tli id="T94" time="13.008" type="appl" />
         <tli id="T95" time="13.793" type="appl" />
         <tli id="T96" time="14.577" type="appl" />
         <tli id="T97" time="15.442" type="appl" />
         <tli id="T98" time="16.186" type="appl" />
         <tli id="T99" time="16.929" type="appl" />
         <tli id="T100" time="17.673" type="appl" />
         <tli id="T101" time="18.417" type="appl" />
         <tli id="T102" time="18.996" type="appl" />
         <tli id="T103" time="19.574" type="appl" />
         <tli id="T104" time="20.153" type="appl" />
         <tli id="T105" time="20.732" type="appl" />
         <tli id="T106" time="21.311" type="appl" />
         <tli id="T107" time="21.89" type="appl" />
         <tli id="T108" time="22.468" type="appl" />
         <tli id="T109" time="23.047" type="appl" />
         <tli id="T110" time="24.215" type="appl" />
         <tli id="T111" time="25.383" type="appl" />
         <tli id="T112" time="26.55" type="appl" />
         <tli id="T113" time="27.718" type="appl" />
         <tli id="T114" time="28.886" type="appl" />
         <tli id="T115" time="29.442" type="appl" />
         <tli id="T116" time="29.963" type="appl" />
         <tli id="T117" time="30.484" type="appl" />
         <tli id="T118" time="31.005" type="appl" />
         <tli id="T119" time="31.683" type="appl" />
         <tli id="T120" time="32.361" type="appl" />
         <tli id="T121" time="33.039" type="appl" />
         <tli id="T122" time="33.986" type="appl" />
         <tli id="T123" time="35.15865230207833" />
         <tli id="T124" time="36.133" type="appl" />
         <tli id="T125" time="37.055" type="appl" />
         <tli id="T126" time="37.977" type="appl" />
         <tli id="T127" time="38.899" type="appl" />
         <tli id="T128" time="40.796" type="appl" />
         <tli id="T129" time="41.508" type="appl" />
         <tli id="T130" time="42.205" type="appl" />
         <tli id="T131" time="42.901" type="appl" />
         <tli id="T132" time="43.598" type="appl" />
         <tli id="T133" time="49.791424733451464" />
         <tli id="T134" time="52.51" type="appl" />
         <tli id="T135" time="54.979" type="appl" />
         <tli id="T136" time="57.448" type="appl" />
         <tli id="T137" time="58.186" type="appl" />
         <tli id="T138" time="58.924" type="appl" />
         <tli id="T139" time="59.662" type="appl" />
         <tli id="T140" time="60.4" type="appl" />
         <tli id="T141" time="61.027" type="appl" />
         <tli id="T142" time="61.654" type="appl" />
         <tli id="T143" time="62.281" type="appl" />
         <tli id="T144" time="63.172" type="appl" />
         <tli id="T145" time="63.934" type="appl" />
         <tli id="T146" time="64.696" type="appl" />
         <tli id="T147" time="65.39900354864442" />
         <tli id="T148" time="66.327" type="appl" />
         <tli id="T149" time="67.196" type="appl" />
         <tli id="T150" time="68.064" type="appl" />
         <tli id="T151" time="68.932" type="appl" />
         <tli id="T152" time="69.726" type="appl" />
         <tli id="T153" time="70.521" type="appl" />
         <tli id="T154" time="71.315" type="appl" />
         <tli id="T155" time="72.109" type="appl" />
         <tli id="T156" time="72.904" type="appl" />
         <tli id="T157" time="74.23048793774026" />
         <tli id="T158" time="75.293" type="appl" />
         <tli id="T159" time="76.077" type="appl" />
         <tli id="T160" time="76.861" type="appl" />
         <tli id="T161" time="77.805" type="appl" />
         <tli id="T162" time="78.749" type="appl" />
         <tli id="T163" time="79.693" type="appl" />
         <tli id="T164" time="80.637" type="appl" />
         <tli id="T165" time="81.581" type="appl" />
         <tli id="T166" time="82.525" type="appl" />
         <tli id="T167" time="83.469" type="appl" />
         <tli id="T168" time="84.71" type="appl" />
         <tli id="T169" time="85.896" type="appl" />
         <tli id="T170" time="86.722" type="appl" />
         <tli id="T171" time="87.547" type="appl" />
         <tli id="T172" time="88.373" type="appl" />
         <tli id="T173" time="89.91655332772706" />
         <tli id="T174" time="91.022" type="appl" />
         <tli id="T175" time="91.95" type="appl" />
         <tli id="T176" time="92.878" type="appl" />
         <tli id="T177" time="93.686" type="appl" />
         <tli id="T178" time="94.386" type="appl" />
         <tli id="T179" time="95.087" type="appl" />
         <tli id="T180" time="95.787" type="appl" />
         <tli id="T181" time="96.488" type="appl" />
         <tli id="T182" time="97.188" type="appl" />
         <tli id="T183" time="97.889" type="appl" />
         <tli id="T184" time="98.589" type="appl" />
         <tli id="T185" time="99.72951051177319" />
         <tli id="T186" time="100.718" type="appl" />
         <tli id="T187" time="102.10275287421914" />
         <tli id="T188" time="103.314" type="appl" />
         <tli id="T189" time="104.473" type="appl" />
         <tli id="T190" time="105.63" type="appl" />
         <tli id="T191" time="107.08922839980778" />
         <tli id="T192" time="107.735" type="appl" />
         <tli id="T193" time="108.379" type="appl" />
         <tli id="T194" time="109.022" type="appl" />
         <tli id="T195" time="109.634" type="appl" />
         <tli id="T196" time="110.245" type="appl" />
         <tli id="T197" time="110.857" type="appl" />
         <tli id="T198" time="111.37" type="appl" />
         <tli id="T199" time="111.882" type="appl" />
         <tli id="T200" time="112.395" type="appl" />
         <tli id="T201" time="112.908" type="appl" />
         <tli id="T202" time="113.421" type="appl" />
         <tli id="T203" time="113.933" type="appl" />
         <tli id="T204" time="114.446" type="appl" />
         <tli id="T205" time="115.824" type="appl" />
         <tli id="T206" time="117.201" type="appl" />
         <tli id="T207" time="119.48875310247476" />
         <tli id="T208" time="120.655" type="appl" />
         <tli id="T209" time="121.95733035222007" />
         <tli id="T210" time="122.816" type="appl" />
         <tli id="T211" time="123.708" type="appl" />
         <tli id="T212" time="124.599" type="appl" />
         <tli id="T213" time="125.491" type="appl" />
         <tli id="T214" time="126.383" type="appl" />
         <tli id="T215" time="127.426" type="appl" />
         <tli id="T216" time="128.323" type="appl" />
         <tli id="T217" time="129.556" type="appl" />
         <tli id="T218" time="130.788" type="appl" />
         <tli id="T219" time="132.02" type="appl" />
         <tli id="T220" time="133.1796736159875" />
         <tli id="T221" time="133.945" type="appl" />
         <tli id="T222" time="134.637" type="appl" />
         <tli id="T223" time="135.329" type="appl" />
         <tli id="T224" time="136.021" type="appl" />
         <tli id="T225" time="136.713" type="appl" />
         <tli id="T226" time="137.405" type="appl" />
         <tli id="T227" time="138.728" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PKZ"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T227" id="Seg_0" n="sc" s="T80">
               <ts e="T83" id="Seg_2" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_4" n="HIAT:w" s="T80">Amnobiʔi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_7" n="HIAT:w" s="T81">nüke</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_10" n="HIAT:w" s="T82">büzʼeziʔ</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_14" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_16" n="HIAT:w" s="T83">Kuʔpiʔi</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_19" n="HIAT:w" s="T84">rʼepa</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_23" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_25" n="HIAT:w" s="T85">Dĭ</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_28" n="HIAT:w" s="T86">urgo</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_31" n="HIAT:w" s="T87">özerbi</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_35" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_37" n="HIAT:w" s="T88">Urgaːba</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_40" n="HIAT:w" s="T89">šobi</ts>
                  <nts id="Seg_41" n="HIAT:ip">,</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_44" n="HIAT:w" s="T90">dĭ</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_47" n="HIAT:w" s="T91">nĭŋgəbi</ts>
                  <nts id="Seg_48" n="HIAT:ip">.</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_51" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_53" n="HIAT:w" s="T92">Barəʔluʔpi</ts>
                  <nts id="Seg_54" n="HIAT:ip">.</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_57" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_59" n="HIAT:w" s="T93">Büzʼe</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_62" n="HIAT:w" s="T94">kambi</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_65" n="HIAT:w" s="T95">măndəsʼtə</ts>
                  <nts id="Seg_66" n="HIAT:ip">.</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_69" n="HIAT:u" s="T96">
                  <nts id="Seg_70" n="HIAT:ip">"</nts>
                  <ts e="T97" id="Seg_72" n="HIAT:w" s="T96">Šindidə</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_75" n="HIAT:w" s="T97">bar</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_78" n="HIAT:w" s="T98">rʼepat</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_81" n="HIAT:w" s="T99">nĭŋgəbi</ts>
                  <nts id="Seg_82" n="HIAT:ip">,</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_84" n="HIAT:ip">(</nts>
                  <ts e="T101" id="Seg_86" n="HIAT:w" s="T100">barəʔluʔpi</ts>
                  <nts id="Seg_87" n="HIAT:ip">)</nts>
                  <nts id="Seg_88" n="HIAT:ip">"</nts>
                  <nts id="Seg_89" n="HIAT:ip">.</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_92" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_94" n="HIAT:w" s="T101">Nüke</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_97" n="HIAT:w" s="T102">măndə:</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_99" n="HIAT:ip">"</nts>
                  <ts e="T104" id="Seg_101" n="HIAT:w" s="T103">Dĭ</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_104" n="HIAT:w" s="T104">annaka</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_107" n="HIAT:w" s="T105">ej</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_109" n="HIAT:ip">(</nts>
                  <ts e="T107" id="Seg_111" n="HIAT:w" s="T106">iba</ts>
                  <nts id="Seg_112" n="HIAT:ip">)</nts>
                  <nts id="Seg_113" n="HIAT:ip">,</nts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_116" n="HIAT:w" s="T107">a</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_119" n="HIAT:w" s="T108">urgaːba</ts>
                  <nts id="Seg_120" n="HIAT:ip">.</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_123" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_125" n="HIAT:w" s="T109">Kanaʔ</ts>
                  <nts id="Seg_126" n="HIAT:ip">,</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_129" n="HIAT:w" s="T110">kanaʔ</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_131" n="HIAT:ip">(</nts>
                  <ts e="T112" id="Seg_133" n="HIAT:w" s="T111">dĭm=</ts>
                  <nts id="Seg_134" n="HIAT:ip">)</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_137" n="HIAT:w" s="T112">dibər</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_140" n="HIAT:w" s="T113">amnoʔ</ts>
                  <nts id="Seg_141" n="HIAT:ip">!</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_144" n="HIAT:u" s="T114">
                  <ts e="T115" id="Seg_146" n="HIAT:w" s="T114">Da</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_149" n="HIAT:w" s="T115">măndoʔ</ts>
                  <nts id="Seg_150" n="HIAT:ip">,</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_153" n="HIAT:w" s="T116">šində</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_156" n="HIAT:w" s="T117">mĭnge</ts>
                  <nts id="Seg_157" n="HIAT:ip">"</nts>
                  <nts id="Seg_158" n="HIAT:ip">.</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T121" id="Seg_161" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_163" n="HIAT:w" s="T118">Dĭ</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_166" n="HIAT:w" s="T119">kambi</ts>
                  <nts id="Seg_167" n="HIAT:ip">,</nts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_170" n="HIAT:w" s="T120">šaʔlaːmbi</ts>
                  <nts id="Seg_171" n="HIAT:ip">.</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_174" n="HIAT:u" s="T121">
                  <ts e="T122" id="Seg_176" n="HIAT:w" s="T121">Urgaːba</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_179" n="HIAT:w" s="T122">šobi</ts>
                  <nts id="Seg_180" n="HIAT:ip">.</nts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_183" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_185" n="HIAT:w" s="T123">Iʔgö</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_188" n="HIAT:w" s="T124">nĭŋgəbi</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_191" n="HIAT:w" s="T125">i</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_194" n="HIAT:w" s="T126">kandəga</ts>
                  <nts id="Seg_195" n="HIAT:ip">.</nts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_198" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_200" n="HIAT:w" s="T127">Zaplottə</ts>
                  <nts id="Seg_201" n="HIAT:ip">.</nts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_204" n="HIAT:u" s="T128">
                  <ts e="T129" id="Seg_206" n="HIAT:w" s="T128">Dĭgəttə</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_209" n="HIAT:w" s="T129">dĭ</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_212" n="HIAT:w" s="T130">baltuziʔ</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_215" n="HIAT:w" s="T131">băʔluʔpi</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_218" n="HIAT:w" s="T132">ujubə</ts>
                  <nts id="Seg_219" n="HIAT:ip">.</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_222" n="HIAT:u" s="T133">
                  <ts e="T134" id="Seg_224" n="HIAT:w" s="T133">Ujubə</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_227" n="HIAT:w" s="T134">dĭn</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_230" n="HIAT:w" s="T135">sajluʔpi</ts>
                  <nts id="Seg_231" n="HIAT:ip">.</nts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_234" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_236" n="HIAT:w" s="T136">Dĭgəttə</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_239" n="HIAT:w" s="T137">deʔpi</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_241" n="HIAT:ip">(</nts>
                  <ts e="T139" id="Seg_243" n="HIAT:w" s="T138">nügen-</ts>
                  <nts id="Seg_244" n="HIAT:ip">)</nts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_247" n="HIAT:w" s="T139">nükendə</ts>
                  <nts id="Seg_248" n="HIAT:ip">.</nts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_251" n="HIAT:u" s="T140">
                  <nts id="Seg_252" n="HIAT:ip">"</nts>
                  <ts e="T141" id="Seg_254" n="HIAT:w" s="T140">Na</ts>
                  <nts id="Seg_255" n="HIAT:ip">,</nts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_258" n="HIAT:w" s="T141">uju</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_261" n="HIAT:w" s="T142">deʔpiem</ts>
                  <nts id="Seg_262" n="HIAT:ip">"</nts>
                  <nts id="Seg_263" n="HIAT:ip">.</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T147" id="Seg_266" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_268" n="HIAT:w" s="T143">Dĭ</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_271" n="HIAT:w" s="T144">nüke</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_274" n="HIAT:w" s="T145">ujubə</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_277" n="HIAT:w" s="T146">kürbi</ts>
                  <nts id="Seg_278" n="HIAT:ip">.</nts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T151" id="Seg_281" n="HIAT:u" s="T147">
                  <ts e="T148" id="Seg_283" n="HIAT:w" s="T147">Ujabə</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_286" n="HIAT:w" s="T148">embi</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_289" n="HIAT:w" s="T149">aspaʔdə</ts>
                  <nts id="Seg_290" n="HIAT:ip">,</nts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_293" n="HIAT:w" s="T150">mĭnzərleʔbə</ts>
                  <nts id="Seg_294" n="HIAT:ip">.</nts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_297" n="HIAT:u" s="T151">
                  <ts e="T152" id="Seg_299" n="HIAT:w" s="T151">A</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_302" n="HIAT:w" s="T152">tărdə</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_305" n="HIAT:w" s="T153">bar</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_307" n="HIAT:ip">(</nts>
                  <ts e="T155" id="Seg_309" n="HIAT:w" s="T154">jerləʔ-</ts>
                  <nts id="Seg_310" n="HIAT:ip">)</nts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_313" n="HIAT:w" s="T155">ererzittə</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_316" n="HIAT:w" s="T156">načala</ts>
                  <nts id="Seg_317" n="HIAT:ip">.</nts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_320" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_322" n="HIAT:w" s="T157">A</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_325" n="HIAT:w" s="T158">kubagən</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_328" n="HIAT:w" s="T159">amnolaʔbə</ts>
                  <nts id="Seg_329" n="HIAT:ip">.</nts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_332" n="HIAT:u" s="T160">
                  <ts e="T161" id="Seg_334" n="HIAT:w" s="T160">A</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_337" n="HIAT:w" s="T161">urgaːba</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_340" n="HIAT:w" s="T162">uju</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_343" n="HIAT:w" s="T163">abi</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_346" n="HIAT:w" s="T164">lipagən</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_349" n="HIAT:w" s="T165">i</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_352" n="HIAT:w" s="T166">šonəga</ts>
                  <nts id="Seg_353" n="HIAT:ip">.</nts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T173" id="Seg_356" n="HIAT:u" s="T167">
                  <ts e="T168" id="Seg_358" n="HIAT:w" s="T167">Măndə:</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_360" n="HIAT:ip">"</nts>
                  <ts e="T169" id="Seg_362" n="HIAT:w" s="T168">Măn</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_365" n="HIAT:w" s="T169">ujum</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_367" n="HIAT:ip">(</nts>
                  <ts e="T171" id="Seg_369" n="HIAT:w" s="T170">pagəʔ</ts>
                  <nts id="Seg_370" n="HIAT:ip">)</nts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_372" n="HIAT:ip">(</nts>
                  <ts e="T172" id="Seg_374" n="HIAT:w" s="T171">anonabar</ts>
                  <nts id="Seg_375" n="HIAT:ip">)</nts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_378" n="HIAT:w" s="T172">bar</ts>
                  <nts id="Seg_379" n="HIAT:ip">.</nts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T176" id="Seg_382" n="HIAT:u" s="T173">
                  <ts e="T174" id="Seg_384" n="HIAT:w" s="T173">Bar</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_387" n="HIAT:w" s="T174">il</ts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_390" n="HIAT:w" s="T175">kunollaʔbəʔjə</ts>
                  <nts id="Seg_391" n="HIAT:ip">.</nts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T185" id="Seg_394" n="HIAT:u" s="T176">
                  <nts id="Seg_395" n="HIAT:ip">(</nts>
                  <ts e="T177" id="Seg_397" n="HIAT:w" s="T176">Odʼinʼ-</ts>
                  <nts id="Seg_398" n="HIAT:ip">)</nts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_401" n="HIAT:w" s="T177">Onʼiʔ</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_404" n="HIAT:w" s="T178">büzʼe</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_407" n="HIAT:w" s="T179">ej</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_410" n="HIAT:w" s="T180">kunollaʔbə</ts>
                  <nts id="Seg_411" n="HIAT:ip">,</nts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_414" n="HIAT:w" s="T181">onʼiʔ</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_417" n="HIAT:w" s="T182">nüke</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_420" n="HIAT:w" s="T183">ej</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_423" n="HIAT:w" s="T184">kunollaʔbə</ts>
                  <nts id="Seg_424" n="HIAT:ip">.</nts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T187" id="Seg_427" n="HIAT:u" s="T185">
                  <ts e="T186" id="Seg_429" n="HIAT:w" s="T185">Uja</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_432" n="HIAT:w" s="T186">mĭnzerleʔbə</ts>
                  <nts id="Seg_433" n="HIAT:ip">.</nts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T189" id="Seg_436" n="HIAT:u" s="T187">
                  <ts e="T188" id="Seg_438" n="HIAT:w" s="T187">Tăr</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_441" n="HIAT:w" s="T188">ererlaʔbə</ts>
                  <nts id="Seg_442" n="HIAT:ip">.</nts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T191" id="Seg_445" n="HIAT:u" s="T189">
                  <ts e="T190" id="Seg_447" n="HIAT:w" s="T189">Kubagən</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_450" n="HIAT:w" s="T190">amnolaʔbə</ts>
                  <nts id="Seg_451" n="HIAT:ip">"</nts>
                  <nts id="Seg_452" n="HIAT:ip">.</nts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T197" id="Seg_455" n="HIAT:u" s="T191">
                  <ts e="T192" id="Seg_457" n="HIAT:w" s="T191">Dĭgəttə</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_460" n="HIAT:w" s="T192">nüke</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_463" n="HIAT:w" s="T193">măndə:</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_465" n="HIAT:ip">"</nts>
                  <ts e="T195" id="Seg_467" n="HIAT:w" s="T194">Kanaʔ</ts>
                  <nts id="Seg_468" n="HIAT:ip">,</nts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_471" n="HIAT:w" s="T195">urgaːba</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_474" n="HIAT:w" s="T196">šonəga</ts>
                  <nts id="Seg_475" n="HIAT:ip">"</nts>
                  <nts id="Seg_476" n="HIAT:ip">.</nts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T204" id="Seg_479" n="HIAT:u" s="T197">
                  <ts e="T198" id="Seg_481" n="HIAT:w" s="T197">Dĭ</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_484" n="HIAT:w" s="T198">kambi</ts>
                  <nts id="Seg_485" n="HIAT:ip">,</nts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_488" n="HIAT:w" s="T199">a</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_491" n="HIAT:w" s="T200">dĭ</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_494" n="HIAT:w" s="T201">už</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_497" n="HIAT:w" s="T202">turanə</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_500" n="HIAT:w" s="T203">mĭlleʔbə</ts>
                  <nts id="Seg_501" n="HIAT:ip">.</nts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T207" id="Seg_504" n="HIAT:u" s="T204">
                  <ts e="T205" id="Seg_506" n="HIAT:w" s="T204">Dĭ</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_509" n="HIAT:w" s="T205">bar</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_512" n="HIAT:w" s="T206">sʼabi</ts>
                  <nts id="Seg_513" n="HIAT:ip">.</nts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T209" id="Seg_516" n="HIAT:u" s="T207">
                  <ts e="T208" id="Seg_518" n="HIAT:w" s="T207">Karɨtazʼiʔ</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_521" n="HIAT:w" s="T208">kajluʔpi</ts>
                  <nts id="Seg_522" n="HIAT:ip">.</nts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T214" id="Seg_525" n="HIAT:u" s="T209">
                  <ts e="T210" id="Seg_527" n="HIAT:w" s="T209">Nüke</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_530" n="HIAT:w" s="T210">pʼeːšdə</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_533" n="HIAT:w" s="T211">sʼaluʔpi</ts>
                  <nts id="Seg_534" n="HIAT:ip">,</nts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_537" n="HIAT:w" s="T212">oldʼanə</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_540" n="HIAT:w" s="T213">šaʔlaːmbi</ts>
                  <nts id="Seg_541" n="HIAT:ip">.</nts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T216" id="Seg_544" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_546" n="HIAT:w" s="T214">Urgaːba</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_549" n="HIAT:w" s="T215">šobi</ts>
                  <nts id="Seg_550" n="HIAT:ip">.</nts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T220" id="Seg_553" n="HIAT:u" s="T216">
                  <ts e="T217" id="Seg_555" n="HIAT:w" s="T216">Da</ts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_558" n="HIAT:w" s="T217">patpolʼlʼanə</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_560" n="HIAT:ip">(</nts>
                  <ts e="T219" id="Seg_562" n="HIAT:w" s="T218">b-</ts>
                  <nts id="Seg_563" n="HIAT:ip">)</nts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_566" n="HIAT:w" s="T219">păʔluʔpi</ts>
                  <nts id="Seg_567" n="HIAT:ip">.</nts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T226" id="Seg_570" n="HIAT:u" s="T220">
                  <ts e="T221" id="Seg_572" n="HIAT:w" s="T220">A</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_575" n="HIAT:w" s="T221">il</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_578" n="HIAT:w" s="T222">šobiʔi</ts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_581" n="HIAT:w" s="T223">da</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_584" n="HIAT:w" s="T224">urgaːbam</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_587" n="HIAT:w" s="T225">kutlaːmbiʔi</ts>
                  <nts id="Seg_588" n="HIAT:ip">.</nts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T227" id="Seg_591" n="HIAT:u" s="T226">
                  <ts e="T227" id="Seg_593" n="HIAT:w" s="T226">Kabarləj</ts>
                  <nts id="Seg_594" n="HIAT:ip">.</nts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T227" id="Seg_596" n="sc" s="T80">
               <ts e="T81" id="Seg_598" n="e" s="T80">Amnobiʔi </ts>
               <ts e="T82" id="Seg_600" n="e" s="T81">nüke </ts>
               <ts e="T83" id="Seg_602" n="e" s="T82">büzʼeziʔ. </ts>
               <ts e="T84" id="Seg_604" n="e" s="T83">Kuʔpiʔi </ts>
               <ts e="T85" id="Seg_606" n="e" s="T84">rʼepa. </ts>
               <ts e="T86" id="Seg_608" n="e" s="T85">Dĭ </ts>
               <ts e="T87" id="Seg_610" n="e" s="T86">urgo </ts>
               <ts e="T88" id="Seg_612" n="e" s="T87">özerbi. </ts>
               <ts e="T89" id="Seg_614" n="e" s="T88">Urgaːba </ts>
               <ts e="T90" id="Seg_616" n="e" s="T89">šobi, </ts>
               <ts e="T91" id="Seg_618" n="e" s="T90">dĭ </ts>
               <ts e="T92" id="Seg_620" n="e" s="T91">nĭŋgəbi. </ts>
               <ts e="T93" id="Seg_622" n="e" s="T92">Barəʔluʔpi. </ts>
               <ts e="T94" id="Seg_624" n="e" s="T93">Büzʼe </ts>
               <ts e="T95" id="Seg_626" n="e" s="T94">kambi </ts>
               <ts e="T96" id="Seg_628" n="e" s="T95">măndəsʼtə. </ts>
               <ts e="T97" id="Seg_630" n="e" s="T96">"Šindidə </ts>
               <ts e="T98" id="Seg_632" n="e" s="T97">bar </ts>
               <ts e="T99" id="Seg_634" n="e" s="T98">rʼepat </ts>
               <ts e="T100" id="Seg_636" n="e" s="T99">nĭŋgəbi, </ts>
               <ts e="T101" id="Seg_638" n="e" s="T100">(barəʔluʔpi)". </ts>
               <ts e="T102" id="Seg_640" n="e" s="T101">Nüke </ts>
               <ts e="T103" id="Seg_642" n="e" s="T102">măndə: </ts>
               <ts e="T104" id="Seg_644" n="e" s="T103">"Dĭ </ts>
               <ts e="T105" id="Seg_646" n="e" s="T104">annaka </ts>
               <ts e="T106" id="Seg_648" n="e" s="T105">ej </ts>
               <ts e="T107" id="Seg_650" n="e" s="T106">(iba), </ts>
               <ts e="T108" id="Seg_652" n="e" s="T107">a </ts>
               <ts e="T109" id="Seg_654" n="e" s="T108">urgaːba. </ts>
               <ts e="T110" id="Seg_656" n="e" s="T109">Kanaʔ, </ts>
               <ts e="T111" id="Seg_658" n="e" s="T110">kanaʔ </ts>
               <ts e="T112" id="Seg_660" n="e" s="T111">(dĭm=) </ts>
               <ts e="T113" id="Seg_662" n="e" s="T112">dibər </ts>
               <ts e="T114" id="Seg_664" n="e" s="T113">amnoʔ! </ts>
               <ts e="T115" id="Seg_666" n="e" s="T114">Da </ts>
               <ts e="T116" id="Seg_668" n="e" s="T115">măndoʔ, </ts>
               <ts e="T117" id="Seg_670" n="e" s="T116">šində </ts>
               <ts e="T118" id="Seg_672" n="e" s="T117">mĭnge". </ts>
               <ts e="T119" id="Seg_674" n="e" s="T118">Dĭ </ts>
               <ts e="T120" id="Seg_676" n="e" s="T119">kambi, </ts>
               <ts e="T121" id="Seg_678" n="e" s="T120">šaʔlaːmbi. </ts>
               <ts e="T122" id="Seg_680" n="e" s="T121">Urgaːba </ts>
               <ts e="T123" id="Seg_682" n="e" s="T122">šobi. </ts>
               <ts e="T124" id="Seg_684" n="e" s="T123">Iʔgö </ts>
               <ts e="T125" id="Seg_686" n="e" s="T124">nĭŋgəbi </ts>
               <ts e="T126" id="Seg_688" n="e" s="T125">i </ts>
               <ts e="T127" id="Seg_690" n="e" s="T126">kandəga. </ts>
               <ts e="T128" id="Seg_692" n="e" s="T127">Zaplottə. </ts>
               <ts e="T129" id="Seg_694" n="e" s="T128">Dĭgəttə </ts>
               <ts e="T130" id="Seg_696" n="e" s="T129">dĭ </ts>
               <ts e="T131" id="Seg_698" n="e" s="T130">baltuziʔ </ts>
               <ts e="T132" id="Seg_700" n="e" s="T131">băʔluʔpi </ts>
               <ts e="T133" id="Seg_702" n="e" s="T132">ujubə. </ts>
               <ts e="T134" id="Seg_704" n="e" s="T133">Ujubə </ts>
               <ts e="T135" id="Seg_706" n="e" s="T134">dĭn </ts>
               <ts e="T136" id="Seg_708" n="e" s="T135">sajluʔpi. </ts>
               <ts e="T137" id="Seg_710" n="e" s="T136">Dĭgəttə </ts>
               <ts e="T138" id="Seg_712" n="e" s="T137">deʔpi </ts>
               <ts e="T139" id="Seg_714" n="e" s="T138">(nügen-) </ts>
               <ts e="T140" id="Seg_716" n="e" s="T139">nükendə. </ts>
               <ts e="T141" id="Seg_718" n="e" s="T140">"Na, </ts>
               <ts e="T142" id="Seg_720" n="e" s="T141">uju </ts>
               <ts e="T143" id="Seg_722" n="e" s="T142">deʔpiem". </ts>
               <ts e="T144" id="Seg_724" n="e" s="T143">Dĭ </ts>
               <ts e="T145" id="Seg_726" n="e" s="T144">nüke </ts>
               <ts e="T146" id="Seg_728" n="e" s="T145">ujubə </ts>
               <ts e="T147" id="Seg_730" n="e" s="T146">kürbi. </ts>
               <ts e="T148" id="Seg_732" n="e" s="T147">Ujabə </ts>
               <ts e="T149" id="Seg_734" n="e" s="T148">embi </ts>
               <ts e="T150" id="Seg_736" n="e" s="T149">aspaʔdə, </ts>
               <ts e="T151" id="Seg_738" n="e" s="T150">mĭnzərleʔbə. </ts>
               <ts e="T152" id="Seg_740" n="e" s="T151">A </ts>
               <ts e="T153" id="Seg_742" n="e" s="T152">tărdə </ts>
               <ts e="T154" id="Seg_744" n="e" s="T153">bar </ts>
               <ts e="T155" id="Seg_746" n="e" s="T154">(jerləʔ-) </ts>
               <ts e="T156" id="Seg_748" n="e" s="T155">ererzittə </ts>
               <ts e="T157" id="Seg_750" n="e" s="T156">načala. </ts>
               <ts e="T158" id="Seg_752" n="e" s="T157">A </ts>
               <ts e="T159" id="Seg_754" n="e" s="T158">kubagən </ts>
               <ts e="T160" id="Seg_756" n="e" s="T159">amnolaʔbə. </ts>
               <ts e="T161" id="Seg_758" n="e" s="T160">A </ts>
               <ts e="T162" id="Seg_760" n="e" s="T161">urgaːba </ts>
               <ts e="T163" id="Seg_762" n="e" s="T162">uju </ts>
               <ts e="T164" id="Seg_764" n="e" s="T163">abi </ts>
               <ts e="T165" id="Seg_766" n="e" s="T164">lipagən </ts>
               <ts e="T166" id="Seg_768" n="e" s="T165">i </ts>
               <ts e="T167" id="Seg_770" n="e" s="T166">šonəga. </ts>
               <ts e="T168" id="Seg_772" n="e" s="T167">Măndə: </ts>
               <ts e="T169" id="Seg_774" n="e" s="T168">"Măn </ts>
               <ts e="T170" id="Seg_776" n="e" s="T169">ujum </ts>
               <ts e="T171" id="Seg_778" n="e" s="T170">(pagəʔ) </ts>
               <ts e="T172" id="Seg_780" n="e" s="T171">(anonabar) </ts>
               <ts e="T173" id="Seg_782" n="e" s="T172">bar. </ts>
               <ts e="T174" id="Seg_784" n="e" s="T173">Bar </ts>
               <ts e="T175" id="Seg_786" n="e" s="T174">il </ts>
               <ts e="T176" id="Seg_788" n="e" s="T175">kunollaʔbəʔjə. </ts>
               <ts e="T177" id="Seg_790" n="e" s="T176">(Odʼinʼ-) </ts>
               <ts e="T178" id="Seg_792" n="e" s="T177">Onʼiʔ </ts>
               <ts e="T179" id="Seg_794" n="e" s="T178">büzʼe </ts>
               <ts e="T180" id="Seg_796" n="e" s="T179">ej </ts>
               <ts e="T181" id="Seg_798" n="e" s="T180">kunollaʔbə, </ts>
               <ts e="T182" id="Seg_800" n="e" s="T181">onʼiʔ </ts>
               <ts e="T183" id="Seg_802" n="e" s="T182">nüke </ts>
               <ts e="T184" id="Seg_804" n="e" s="T183">ej </ts>
               <ts e="T185" id="Seg_806" n="e" s="T184">kunollaʔbə. </ts>
               <ts e="T186" id="Seg_808" n="e" s="T185">Uja </ts>
               <ts e="T187" id="Seg_810" n="e" s="T186">mĭnzerleʔbə. </ts>
               <ts e="T188" id="Seg_812" n="e" s="T187">Tăr </ts>
               <ts e="T189" id="Seg_814" n="e" s="T188">ererlaʔbə. </ts>
               <ts e="T190" id="Seg_816" n="e" s="T189">Kubagən </ts>
               <ts e="T191" id="Seg_818" n="e" s="T190">amnolaʔbə". </ts>
               <ts e="T192" id="Seg_820" n="e" s="T191">Dĭgəttə </ts>
               <ts e="T193" id="Seg_822" n="e" s="T192">nüke </ts>
               <ts e="T194" id="Seg_824" n="e" s="T193">măndə: </ts>
               <ts e="T195" id="Seg_826" n="e" s="T194">"Kanaʔ, </ts>
               <ts e="T196" id="Seg_828" n="e" s="T195">urgaːba </ts>
               <ts e="T197" id="Seg_830" n="e" s="T196">šonəga". </ts>
               <ts e="T198" id="Seg_832" n="e" s="T197">Dĭ </ts>
               <ts e="T199" id="Seg_834" n="e" s="T198">kambi, </ts>
               <ts e="T200" id="Seg_836" n="e" s="T199">a </ts>
               <ts e="T201" id="Seg_838" n="e" s="T200">dĭ </ts>
               <ts e="T202" id="Seg_840" n="e" s="T201">už </ts>
               <ts e="T203" id="Seg_842" n="e" s="T202">turanə </ts>
               <ts e="T204" id="Seg_844" n="e" s="T203">mĭlleʔbə. </ts>
               <ts e="T205" id="Seg_846" n="e" s="T204">Dĭ </ts>
               <ts e="T206" id="Seg_848" n="e" s="T205">bar </ts>
               <ts e="T207" id="Seg_850" n="e" s="T206">sʼabi. </ts>
               <ts e="T208" id="Seg_852" n="e" s="T207">Karɨtazʼiʔ </ts>
               <ts e="T209" id="Seg_854" n="e" s="T208">kajluʔpi. </ts>
               <ts e="T210" id="Seg_856" n="e" s="T209">Nüke </ts>
               <ts e="T211" id="Seg_858" n="e" s="T210">pʼeːšdə </ts>
               <ts e="T212" id="Seg_860" n="e" s="T211">sʼaluʔpi, </ts>
               <ts e="T213" id="Seg_862" n="e" s="T212">oldʼanə </ts>
               <ts e="T214" id="Seg_864" n="e" s="T213">šaʔlaːmbi. </ts>
               <ts e="T215" id="Seg_866" n="e" s="T214">Urgaːba </ts>
               <ts e="T216" id="Seg_868" n="e" s="T215">šobi. </ts>
               <ts e="T217" id="Seg_870" n="e" s="T216">Da </ts>
               <ts e="T218" id="Seg_872" n="e" s="T217">patpolʼlʼanə </ts>
               <ts e="T219" id="Seg_874" n="e" s="T218">(b-) </ts>
               <ts e="T220" id="Seg_876" n="e" s="T219">păʔluʔpi. </ts>
               <ts e="T221" id="Seg_878" n="e" s="T220">A </ts>
               <ts e="T222" id="Seg_880" n="e" s="T221">il </ts>
               <ts e="T223" id="Seg_882" n="e" s="T222">šobiʔi </ts>
               <ts e="T224" id="Seg_884" n="e" s="T223">da </ts>
               <ts e="T225" id="Seg_886" n="e" s="T224">urgaːbam </ts>
               <ts e="T226" id="Seg_888" n="e" s="T225">kutlaːmbiʔi. </ts>
               <ts e="T227" id="Seg_890" n="e" s="T226">Kabarləj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T83" id="Seg_891" s="T80">PKZ_196X_BearWoodenFoot_flk.001 (001)</ta>
            <ta e="T85" id="Seg_892" s="T83">PKZ_196X_BearWoodenFoot_flk.002 (002)</ta>
            <ta e="T88" id="Seg_893" s="T85">PKZ_196X_BearWoodenFoot_flk.003 (003)</ta>
            <ta e="T92" id="Seg_894" s="T88">PKZ_196X_BearWoodenFoot_flk.004 (004)</ta>
            <ta e="T93" id="Seg_895" s="T92">PKZ_196X_BearWoodenFoot_flk.005 (005)</ta>
            <ta e="T96" id="Seg_896" s="T93">PKZ_196X_BearWoodenFoot_flk.006 (006)</ta>
            <ta e="T101" id="Seg_897" s="T96">PKZ_196X_BearWoodenFoot_flk.007 (007)</ta>
            <ta e="T109" id="Seg_898" s="T101">PKZ_196X_BearWoodenFoot_flk.008 (008)</ta>
            <ta e="T114" id="Seg_899" s="T109">PKZ_196X_BearWoodenFoot_flk.009 (009)</ta>
            <ta e="T118" id="Seg_900" s="T114">PKZ_196X_BearWoodenFoot_flk.010 (010)</ta>
            <ta e="T121" id="Seg_901" s="T118">PKZ_196X_BearWoodenFoot_flk.011 (011)</ta>
            <ta e="T123" id="Seg_902" s="T121">PKZ_196X_BearWoodenFoot_flk.012 (012)</ta>
            <ta e="T127" id="Seg_903" s="T123">PKZ_196X_BearWoodenFoot_flk.013 (013)</ta>
            <ta e="T128" id="Seg_904" s="T127">PKZ_196X_BearWoodenFoot_flk.014 (014)</ta>
            <ta e="T133" id="Seg_905" s="T128">PKZ_196X_BearWoodenFoot_flk.015 (015)</ta>
            <ta e="T136" id="Seg_906" s="T133">PKZ_196X_BearWoodenFoot_flk.016 (016)</ta>
            <ta e="T140" id="Seg_907" s="T136">PKZ_196X_BearWoodenFoot_flk.017 (017)</ta>
            <ta e="T143" id="Seg_908" s="T140">PKZ_196X_BearWoodenFoot_flk.018 (018)</ta>
            <ta e="T147" id="Seg_909" s="T143">PKZ_196X_BearWoodenFoot_flk.019 (019)</ta>
            <ta e="T151" id="Seg_910" s="T147">PKZ_196X_BearWoodenFoot_flk.020 (020)</ta>
            <ta e="T157" id="Seg_911" s="T151">PKZ_196X_BearWoodenFoot_flk.021 (021)</ta>
            <ta e="T160" id="Seg_912" s="T157">PKZ_196X_BearWoodenFoot_flk.022 (022)</ta>
            <ta e="T167" id="Seg_913" s="T160">PKZ_196X_BearWoodenFoot_flk.023 (023)</ta>
            <ta e="T173" id="Seg_914" s="T167">PKZ_196X_BearWoodenFoot_flk.024 (024) </ta>
            <ta e="T176" id="Seg_915" s="T173">PKZ_196X_BearWoodenFoot_flk.025 (026)</ta>
            <ta e="T185" id="Seg_916" s="T176">PKZ_196X_BearWoodenFoot_flk.026 (027)</ta>
            <ta e="T187" id="Seg_917" s="T185">PKZ_196X_BearWoodenFoot_flk.027 (028)</ta>
            <ta e="T189" id="Seg_918" s="T187">PKZ_196X_BearWoodenFoot_flk.028 (029)</ta>
            <ta e="T191" id="Seg_919" s="T189">PKZ_196X_BearWoodenFoot_flk.029 (030)</ta>
            <ta e="T197" id="Seg_920" s="T191">PKZ_196X_BearWoodenFoot_flk.030 (031)</ta>
            <ta e="T204" id="Seg_921" s="T197">PKZ_196X_BearWoodenFoot_flk.031 (033)</ta>
            <ta e="T207" id="Seg_922" s="T204">PKZ_196X_BearWoodenFoot_flk.032 (034)</ta>
            <ta e="T209" id="Seg_923" s="T207">PKZ_196X_BearWoodenFoot_flk.033 (035)</ta>
            <ta e="T214" id="Seg_924" s="T209">PKZ_196X_BearWoodenFoot_flk.034 (036)</ta>
            <ta e="T216" id="Seg_925" s="T214">PKZ_196X_BearWoodenFoot_flk.035 (037)</ta>
            <ta e="T220" id="Seg_926" s="T216">PKZ_196X_BearWoodenFoot_flk.036 (038)</ta>
            <ta e="T226" id="Seg_927" s="T220">PKZ_196X_BearWoodenFoot_flk.037 (039)</ta>
            <ta e="T227" id="Seg_928" s="T226">PKZ_196X_BearWoodenFoot_flk.038 (040)</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T83" id="Seg_929" s="T80">Amnobiʔi nüke büzʼeziʔ. </ta>
            <ta e="T85" id="Seg_930" s="T83">Kuʔpiʔi rʼepa. </ta>
            <ta e="T88" id="Seg_931" s="T85">Dĭ urgo özerbi. </ta>
            <ta e="T92" id="Seg_932" s="T88">Urgaːba šobi, dĭ nĭŋgəbi. </ta>
            <ta e="T93" id="Seg_933" s="T92">Barəʔluʔpi. </ta>
            <ta e="T96" id="Seg_934" s="T93">Büzʼe kambi măndəsʼtə. </ta>
            <ta e="T101" id="Seg_935" s="T96">"Šindidə bar rʼepat nĭŋgəbi, (barəʔluʔpi)". </ta>
            <ta e="T109" id="Seg_936" s="T101">Nüke măndə: "Dĭ annaka ej (iba), a urgaːba. </ta>
            <ta e="T114" id="Seg_937" s="T109">Kanaʔ, kanaʔ (dĭm=) dibər amnoʔ! </ta>
            <ta e="T118" id="Seg_938" s="T114">Da măndoʔ, šində mĭnge". </ta>
            <ta e="T121" id="Seg_939" s="T118">Dĭ kambi, šaʔlaːmbi. </ta>
            <ta e="T123" id="Seg_940" s="T121">Urgaːba šobi. </ta>
            <ta e="T127" id="Seg_941" s="T123">Iʔgö nĭŋgəbi i kandəga. </ta>
            <ta e="T128" id="Seg_942" s="T127">Zaplottə. </ta>
            <ta e="T133" id="Seg_943" s="T128">Dĭgəttə dĭ baltuziʔ băʔluʔpi ujubə. </ta>
            <ta e="T136" id="Seg_944" s="T133">Ujubə dĭn sajluʔpi. </ta>
            <ta e="T140" id="Seg_945" s="T136">Dĭgəttə deʔpi (nügen-) nükendə. </ta>
            <ta e="T143" id="Seg_946" s="T140">"Na, uju deʔpiem". </ta>
            <ta e="T147" id="Seg_947" s="T143">Dĭ nüke ujubə kürbi. </ta>
            <ta e="T151" id="Seg_948" s="T147">Ujabə embi aspaʔdə, mĭnzərleʔbə. </ta>
            <ta e="T157" id="Seg_949" s="T151">A tărdə bar (jerləʔ-) ererzittə načala. </ta>
            <ta e="T160" id="Seg_950" s="T157">A kubagən amnolaʔbə. </ta>
            <ta e="T167" id="Seg_951" s="T160">A urgaːba uju abi lipagən i šonəga. </ta>
            <ta e="T173" id="Seg_952" s="T167">Măndə: "Măn ujum (pagəʔ) (anonabar) bar. </ta>
            <ta e="T176" id="Seg_953" s="T173">Bar il kunollaʔbəʔjə. </ta>
            <ta e="T185" id="Seg_954" s="T176">(Odʼinʼ-) Onʼiʔ büzʼe ej kunollaʔbə, onʼiʔ nüke ej kunollaʔbə. </ta>
            <ta e="T187" id="Seg_955" s="T185">Uja mĭnzerleʔbə. </ta>
            <ta e="T189" id="Seg_956" s="T187">Tăr ererlaʔbə. </ta>
            <ta e="T191" id="Seg_957" s="T189">Kubagən amnolaʔbə". </ta>
            <ta e="T197" id="Seg_958" s="T191">Dĭgəttə nüke măndə: "Kanaʔ, urgaːba šonəga". </ta>
            <ta e="T204" id="Seg_959" s="T197">Dĭ kambi, a dĭ už turanə mĭlleʔbə. </ta>
            <ta e="T207" id="Seg_960" s="T204">Dĭ bar sʼabi. </ta>
            <ta e="T209" id="Seg_961" s="T207">Karɨtazʼiʔ kajluʔpi. </ta>
            <ta e="T214" id="Seg_962" s="T209">Nüke pʼeːšdə sʼaluʔpi, oldʼanə šaʔlaːmbi. </ta>
            <ta e="T216" id="Seg_963" s="T214">Urgaːba šobi. </ta>
            <ta e="T220" id="Seg_964" s="T216">Da patpolʼlʼanə (b-) păʔluʔpi. </ta>
            <ta e="T226" id="Seg_965" s="T220">A il šobiʔi da urgaːbam kutlaːmbiʔi. </ta>
            <ta e="T227" id="Seg_966" s="T226">Kabarləj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T81" id="Seg_967" s="T80">amno-bi-ʔi</ta>
            <ta e="T82" id="Seg_968" s="T81">nüke</ta>
            <ta e="T83" id="Seg_969" s="T82">büzʼe-ziʔ</ta>
            <ta e="T84" id="Seg_970" s="T83">kuʔ-pi-ʔi</ta>
            <ta e="T85" id="Seg_971" s="T84">rʼepa</ta>
            <ta e="T86" id="Seg_972" s="T85">dĭ</ta>
            <ta e="T87" id="Seg_973" s="T86">urgo</ta>
            <ta e="T88" id="Seg_974" s="T87">özer-bi</ta>
            <ta e="T89" id="Seg_975" s="T88">urgaːba</ta>
            <ta e="T90" id="Seg_976" s="T89">šo-bi</ta>
            <ta e="T91" id="Seg_977" s="T90">dĭ</ta>
            <ta e="T92" id="Seg_978" s="T91">nĭŋgə-bi</ta>
            <ta e="T93" id="Seg_979" s="T92">barəʔ-luʔ-pi</ta>
            <ta e="T94" id="Seg_980" s="T93">büzʼe</ta>
            <ta e="T95" id="Seg_981" s="T94">kam-bi</ta>
            <ta e="T96" id="Seg_982" s="T95">măndə-sʼtə</ta>
            <ta e="T97" id="Seg_983" s="T96">šindi=də</ta>
            <ta e="T98" id="Seg_984" s="T97">bar</ta>
            <ta e="T99" id="Seg_985" s="T98">rʼepa-t</ta>
            <ta e="T100" id="Seg_986" s="T99">nĭŋgə-bi</ta>
            <ta e="T101" id="Seg_987" s="T100">barəʔ-luʔ-pi</ta>
            <ta e="T102" id="Seg_988" s="T101">nüke</ta>
            <ta e="T103" id="Seg_989" s="T102">măn-də</ta>
            <ta e="T104" id="Seg_990" s="T103">dĭ</ta>
            <ta e="T105" id="Seg_991" s="T104">annaka</ta>
            <ta e="T106" id="Seg_992" s="T105">ej</ta>
            <ta e="T107" id="Seg_993" s="T106">iba</ta>
            <ta e="T108" id="Seg_994" s="T107">a</ta>
            <ta e="T109" id="Seg_995" s="T108">urgaːba</ta>
            <ta e="T110" id="Seg_996" s="T109">kan-a-ʔ</ta>
            <ta e="T111" id="Seg_997" s="T110">kan-a-ʔ</ta>
            <ta e="T112" id="Seg_998" s="T111">dĭ-m</ta>
            <ta e="T113" id="Seg_999" s="T112">dibər</ta>
            <ta e="T114" id="Seg_1000" s="T113">amno-ʔ</ta>
            <ta e="T115" id="Seg_1001" s="T114">da</ta>
            <ta e="T116" id="Seg_1002" s="T115">măndo-ʔ</ta>
            <ta e="T117" id="Seg_1003" s="T116">šində</ta>
            <ta e="T118" id="Seg_1004" s="T117">mĭn-ge</ta>
            <ta e="T119" id="Seg_1005" s="T118">dĭ</ta>
            <ta e="T120" id="Seg_1006" s="T119">kam-bi</ta>
            <ta e="T121" id="Seg_1007" s="T120">šaʔ-laːm-bi</ta>
            <ta e="T122" id="Seg_1008" s="T121">urgaːba</ta>
            <ta e="T123" id="Seg_1009" s="T122">šo-bi</ta>
            <ta e="T124" id="Seg_1010" s="T123">iʔgö</ta>
            <ta e="T125" id="Seg_1011" s="T124">nĭŋgə-bi</ta>
            <ta e="T126" id="Seg_1012" s="T125">i</ta>
            <ta e="T127" id="Seg_1013" s="T126">kandə-ga</ta>
            <ta e="T128" id="Seg_1014" s="T127">zaplot-tə</ta>
            <ta e="T129" id="Seg_1015" s="T128">dĭgəttə</ta>
            <ta e="T130" id="Seg_1016" s="T129">dĭ</ta>
            <ta e="T131" id="Seg_1017" s="T130">baltu-ziʔ</ta>
            <ta e="T132" id="Seg_1018" s="T131">băʔ-luʔ-pi</ta>
            <ta e="T133" id="Seg_1019" s="T132">uju-bə</ta>
            <ta e="T134" id="Seg_1020" s="T133">uju-bə</ta>
            <ta e="T135" id="Seg_1021" s="T134">dĭ-n</ta>
            <ta e="T136" id="Seg_1022" s="T135">saj-luʔ-pi</ta>
            <ta e="T137" id="Seg_1023" s="T136">dĭgəttə</ta>
            <ta e="T138" id="Seg_1024" s="T137">deʔ-pi</ta>
            <ta e="T140" id="Seg_1025" s="T139">nüke-ndə</ta>
            <ta e="T141" id="Seg_1026" s="T140">na</ta>
            <ta e="T142" id="Seg_1027" s="T141">uju</ta>
            <ta e="T143" id="Seg_1028" s="T142">deʔ-pie-m</ta>
            <ta e="T144" id="Seg_1029" s="T143">dĭ</ta>
            <ta e="T145" id="Seg_1030" s="T144">nüke</ta>
            <ta e="T146" id="Seg_1031" s="T145">uju-bə</ta>
            <ta e="T147" id="Seg_1032" s="T146">kür-bi</ta>
            <ta e="T148" id="Seg_1033" s="T147">uja-bə</ta>
            <ta e="T149" id="Seg_1034" s="T148">em-bi</ta>
            <ta e="T150" id="Seg_1035" s="T149">aspaʔ-də</ta>
            <ta e="T151" id="Seg_1036" s="T150">mĭnzər-leʔbə</ta>
            <ta e="T152" id="Seg_1037" s="T151">a</ta>
            <ta e="T153" id="Seg_1038" s="T152">tăr-də</ta>
            <ta e="T154" id="Seg_1039" s="T153">bar</ta>
            <ta e="T156" id="Seg_1040" s="T155">erer-zittə</ta>
            <ta e="T158" id="Seg_1041" s="T157">a</ta>
            <ta e="T159" id="Seg_1042" s="T158">kuba-gən</ta>
            <ta e="T160" id="Seg_1043" s="T159">amno-laʔbə</ta>
            <ta e="T161" id="Seg_1044" s="T160">a</ta>
            <ta e="T162" id="Seg_1045" s="T161">urgaːba</ta>
            <ta e="T163" id="Seg_1046" s="T162">uju</ta>
            <ta e="T164" id="Seg_1047" s="T163">a-bi</ta>
            <ta e="T165" id="Seg_1048" s="T164">lipa-gən</ta>
            <ta e="T166" id="Seg_1049" s="T165">i</ta>
            <ta e="T167" id="Seg_1050" s="T166">šonə-ga</ta>
            <ta e="T168" id="Seg_1051" s="T167">măn-də</ta>
            <ta e="T169" id="Seg_1052" s="T168">măn</ta>
            <ta e="T170" id="Seg_1053" s="T169">uju-m</ta>
            <ta e="T171" id="Seg_1054" s="T170">pa-gəʔ</ta>
            <ta e="T172" id="Seg_1055" s="T171">anonabar</ta>
            <ta e="T173" id="Seg_1056" s="T172">bar</ta>
            <ta e="T174" id="Seg_1057" s="T173">bar</ta>
            <ta e="T175" id="Seg_1058" s="T174">il</ta>
            <ta e="T176" id="Seg_1059" s="T175">kunol-laʔbə-ʔjə</ta>
            <ta e="T178" id="Seg_1060" s="T177">onʼiʔ</ta>
            <ta e="T179" id="Seg_1061" s="T178">büzʼe</ta>
            <ta e="T180" id="Seg_1062" s="T179">ej</ta>
            <ta e="T181" id="Seg_1063" s="T180">kunol-laʔbə</ta>
            <ta e="T182" id="Seg_1064" s="T181">onʼiʔ</ta>
            <ta e="T183" id="Seg_1065" s="T182">nüke</ta>
            <ta e="T184" id="Seg_1066" s="T183">ej</ta>
            <ta e="T185" id="Seg_1067" s="T184">kunol-laʔbə</ta>
            <ta e="T186" id="Seg_1068" s="T185">uja</ta>
            <ta e="T187" id="Seg_1069" s="T186">mĭnzer-leʔbə</ta>
            <ta e="T188" id="Seg_1070" s="T187">tăr</ta>
            <ta e="T189" id="Seg_1071" s="T188">erer-laʔbə</ta>
            <ta e="T190" id="Seg_1072" s="T189">kuba-gən</ta>
            <ta e="T191" id="Seg_1073" s="T190">amno-laʔbə</ta>
            <ta e="T192" id="Seg_1074" s="T191">dĭgəttə</ta>
            <ta e="T193" id="Seg_1075" s="T192">nüke</ta>
            <ta e="T194" id="Seg_1076" s="T193">măn-də</ta>
            <ta e="T195" id="Seg_1077" s="T194">kan-a-ʔ</ta>
            <ta e="T196" id="Seg_1078" s="T195">urgaːba</ta>
            <ta e="T197" id="Seg_1079" s="T196">šonə-ga</ta>
            <ta e="T198" id="Seg_1080" s="T197">dĭ</ta>
            <ta e="T199" id="Seg_1081" s="T198">kam-bi</ta>
            <ta e="T200" id="Seg_1082" s="T199">a</ta>
            <ta e="T201" id="Seg_1083" s="T200">dĭ</ta>
            <ta e="T202" id="Seg_1084" s="T201">už</ta>
            <ta e="T203" id="Seg_1085" s="T202">tura-nə</ta>
            <ta e="T204" id="Seg_1086" s="T203">mĭl-leʔbə</ta>
            <ta e="T205" id="Seg_1087" s="T204">dĭ</ta>
            <ta e="T206" id="Seg_1088" s="T205">bar</ta>
            <ta e="T207" id="Seg_1089" s="T206">sʼa-bi</ta>
            <ta e="T208" id="Seg_1090" s="T207">karɨta-zʼiʔ</ta>
            <ta e="T209" id="Seg_1091" s="T208">kaj-luʔ-pi</ta>
            <ta e="T210" id="Seg_1092" s="T209">nüke</ta>
            <ta e="T211" id="Seg_1093" s="T210">pʼeːš-də</ta>
            <ta e="T212" id="Seg_1094" s="T211">sʼa-luʔ-pi</ta>
            <ta e="T213" id="Seg_1095" s="T212">oldʼa-nə</ta>
            <ta e="T214" id="Seg_1096" s="T213">šaʔ-laːm-bi</ta>
            <ta e="T215" id="Seg_1097" s="T214">urgaːba</ta>
            <ta e="T216" id="Seg_1098" s="T215">šo-bi</ta>
            <ta e="T217" id="Seg_1099" s="T216">da</ta>
            <ta e="T218" id="Seg_1100" s="T217">patpolʼlʼa-nə</ta>
            <ta e="T220" id="Seg_1101" s="T219">păʔ-luʔ-pi</ta>
            <ta e="T221" id="Seg_1102" s="T220">a</ta>
            <ta e="T222" id="Seg_1103" s="T221">il</ta>
            <ta e="T223" id="Seg_1104" s="T222">šo-bi-ʔi</ta>
            <ta e="T224" id="Seg_1105" s="T223">da</ta>
            <ta e="T225" id="Seg_1106" s="T224">urgaːba-m</ta>
            <ta e="T226" id="Seg_1107" s="T225">kut-laːm-bi-ʔi</ta>
            <ta e="T227" id="Seg_1108" s="T226">kabarləj</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T81" id="Seg_1109" s="T80">amno-bi-jəʔ</ta>
            <ta e="T82" id="Seg_1110" s="T81">nüke</ta>
            <ta e="T83" id="Seg_1111" s="T82">büzʼe-ziʔ</ta>
            <ta e="T84" id="Seg_1112" s="T83">kuʔ-bi-jəʔ</ta>
            <ta e="T85" id="Seg_1113" s="T84">rʼepa</ta>
            <ta e="T86" id="Seg_1114" s="T85">dĭ</ta>
            <ta e="T87" id="Seg_1115" s="T86">urgo</ta>
            <ta e="T88" id="Seg_1116" s="T87">özer-bi</ta>
            <ta e="T89" id="Seg_1117" s="T88">urgaːba</ta>
            <ta e="T90" id="Seg_1118" s="T89">šo-bi</ta>
            <ta e="T91" id="Seg_1119" s="T90">dĭ</ta>
            <ta e="T92" id="Seg_1120" s="T91">nĭŋgə-bi</ta>
            <ta e="T93" id="Seg_1121" s="T92">barəʔ-luʔbdə-bi</ta>
            <ta e="T94" id="Seg_1122" s="T93">büzʼe</ta>
            <ta e="T95" id="Seg_1123" s="T94">kan-bi</ta>
            <ta e="T96" id="Seg_1124" s="T95">măndo-zittə</ta>
            <ta e="T97" id="Seg_1125" s="T96">šində=də</ta>
            <ta e="T98" id="Seg_1126" s="T97">bar</ta>
            <ta e="T99" id="Seg_1127" s="T98">rʼepa-t</ta>
            <ta e="T100" id="Seg_1128" s="T99">nĭŋgə-bi</ta>
            <ta e="T101" id="Seg_1129" s="T100">barəʔ-luʔbdə-bi</ta>
            <ta e="T102" id="Seg_1130" s="T101">nüke</ta>
            <ta e="T103" id="Seg_1131" s="T102">măn-ntə</ta>
            <ta e="T104" id="Seg_1132" s="T103">dĭ</ta>
            <ta e="T105" id="Seg_1133" s="T104">adnaka</ta>
            <ta e="T106" id="Seg_1134" s="T105">ej</ta>
            <ta e="T107" id="Seg_1135" s="T106">iba</ta>
            <ta e="T108" id="Seg_1136" s="T107">a</ta>
            <ta e="T109" id="Seg_1137" s="T108">urgaːba</ta>
            <ta e="T110" id="Seg_1138" s="T109">kan-ə-ʔ</ta>
            <ta e="T111" id="Seg_1139" s="T110">kan-ə-ʔ</ta>
            <ta e="T112" id="Seg_1140" s="T111">dĭ-m</ta>
            <ta e="T113" id="Seg_1141" s="T112">dĭbər</ta>
            <ta e="T114" id="Seg_1142" s="T113">amno-ʔ</ta>
            <ta e="T115" id="Seg_1143" s="T114">da</ta>
            <ta e="T116" id="Seg_1144" s="T115">măndo-ʔ</ta>
            <ta e="T117" id="Seg_1145" s="T116">šində</ta>
            <ta e="T118" id="Seg_1146" s="T117">mĭn-gA</ta>
            <ta e="T119" id="Seg_1147" s="T118">dĭ</ta>
            <ta e="T120" id="Seg_1148" s="T119">kan-bi</ta>
            <ta e="T121" id="Seg_1149" s="T120">šaʔ-laːm-bi</ta>
            <ta e="T122" id="Seg_1150" s="T121">urgaːba</ta>
            <ta e="T123" id="Seg_1151" s="T122">šo-bi</ta>
            <ta e="T124" id="Seg_1152" s="T123">iʔgö</ta>
            <ta e="T125" id="Seg_1153" s="T124">nĭŋgə-bi</ta>
            <ta e="T126" id="Seg_1154" s="T125">i</ta>
            <ta e="T127" id="Seg_1155" s="T126">kandə-gA</ta>
            <ta e="T128" id="Seg_1156" s="T127">zaplot-Tə</ta>
            <ta e="T129" id="Seg_1157" s="T128">dĭgəttə</ta>
            <ta e="T130" id="Seg_1158" s="T129">dĭ</ta>
            <ta e="T131" id="Seg_1159" s="T130">baltu-ziʔ</ta>
            <ta e="T132" id="Seg_1160" s="T131">băt-luʔbdə-bi</ta>
            <ta e="T133" id="Seg_1161" s="T132">üjü-bə</ta>
            <ta e="T134" id="Seg_1162" s="T133">üjü-bə</ta>
            <ta e="T135" id="Seg_1163" s="T134">dĭ-n</ta>
            <ta e="T136" id="Seg_1164" s="T135">saj-luʔbdə-bi</ta>
            <ta e="T137" id="Seg_1165" s="T136">dĭgəttə</ta>
            <ta e="T138" id="Seg_1166" s="T137">det-bi</ta>
            <ta e="T140" id="Seg_1167" s="T139">nüke-gəndə</ta>
            <ta e="T141" id="Seg_1168" s="T140">na</ta>
            <ta e="T142" id="Seg_1169" s="T141">üjü</ta>
            <ta e="T143" id="Seg_1170" s="T142">det-bi-m</ta>
            <ta e="T144" id="Seg_1171" s="T143">dĭ</ta>
            <ta e="T145" id="Seg_1172" s="T144">nüke</ta>
            <ta e="T146" id="Seg_1173" s="T145">üjü-bə</ta>
            <ta e="T147" id="Seg_1174" s="T146">kür-bi</ta>
            <ta e="T148" id="Seg_1175" s="T147">uja-bə</ta>
            <ta e="T149" id="Seg_1176" s="T148">hen-bi</ta>
            <ta e="T150" id="Seg_1177" s="T149">aspaʔ-də</ta>
            <ta e="T151" id="Seg_1178" s="T150">mĭnzər-laʔbə</ta>
            <ta e="T152" id="Seg_1179" s="T151">a</ta>
            <ta e="T153" id="Seg_1180" s="T152">tar-də</ta>
            <ta e="T154" id="Seg_1181" s="T153">bar</ta>
            <ta e="T156" id="Seg_1182" s="T155">erer-zittə</ta>
            <ta e="T158" id="Seg_1183" s="T157">a</ta>
            <ta e="T159" id="Seg_1184" s="T158">kuba-Kən</ta>
            <ta e="T160" id="Seg_1185" s="T159">amnə-laʔbə</ta>
            <ta e="T161" id="Seg_1186" s="T160">a</ta>
            <ta e="T162" id="Seg_1187" s="T161">urgaːba</ta>
            <ta e="T163" id="Seg_1188" s="T162">üjü</ta>
            <ta e="T164" id="Seg_1189" s="T163">a-bi</ta>
            <ta e="T165" id="Seg_1190" s="T164">lipa-Kən</ta>
            <ta e="T166" id="Seg_1191" s="T165">i</ta>
            <ta e="T167" id="Seg_1192" s="T166">šonə-gA</ta>
            <ta e="T168" id="Seg_1193" s="T167">măn-ntə</ta>
            <ta e="T169" id="Seg_1194" s="T168">măn</ta>
            <ta e="T170" id="Seg_1195" s="T169">üjü-m</ta>
            <ta e="T171" id="Seg_1196" s="T170">pa-gəʔ</ta>
            <ta e="T172" id="Seg_1197" s="T171">anonabar</ta>
            <ta e="T173" id="Seg_1198" s="T172">bar</ta>
            <ta e="T174" id="Seg_1199" s="T173">bar</ta>
            <ta e="T175" id="Seg_1200" s="T174">il</ta>
            <ta e="T176" id="Seg_1201" s="T175">kunol-laʔbə-jəʔ</ta>
            <ta e="T178" id="Seg_1202" s="T177">onʼiʔ</ta>
            <ta e="T179" id="Seg_1203" s="T178">büzʼe</ta>
            <ta e="T180" id="Seg_1204" s="T179">ej</ta>
            <ta e="T181" id="Seg_1205" s="T180">kunol-laʔbə</ta>
            <ta e="T182" id="Seg_1206" s="T181">onʼiʔ</ta>
            <ta e="T183" id="Seg_1207" s="T182">nüke</ta>
            <ta e="T184" id="Seg_1208" s="T183">ej</ta>
            <ta e="T185" id="Seg_1209" s="T184">kunol-laʔbə</ta>
            <ta e="T186" id="Seg_1210" s="T185">uja</ta>
            <ta e="T187" id="Seg_1211" s="T186">mĭnzər-laʔbə</ta>
            <ta e="T188" id="Seg_1212" s="T187">tar</ta>
            <ta e="T189" id="Seg_1213" s="T188">erer-laʔbə</ta>
            <ta e="T190" id="Seg_1214" s="T189">kuba-Kən</ta>
            <ta e="T191" id="Seg_1215" s="T190">amno-laʔbə</ta>
            <ta e="T192" id="Seg_1216" s="T191">dĭgəttə</ta>
            <ta e="T193" id="Seg_1217" s="T192">nüke</ta>
            <ta e="T194" id="Seg_1218" s="T193">măn-ntə</ta>
            <ta e="T195" id="Seg_1219" s="T194">kan-ə-ʔ</ta>
            <ta e="T196" id="Seg_1220" s="T195">urgaːba</ta>
            <ta e="T197" id="Seg_1221" s="T196">šonə-gA</ta>
            <ta e="T198" id="Seg_1222" s="T197">dĭ</ta>
            <ta e="T199" id="Seg_1223" s="T198">kan-bi</ta>
            <ta e="T200" id="Seg_1224" s="T199">a</ta>
            <ta e="T201" id="Seg_1225" s="T200">dĭ</ta>
            <ta e="T202" id="Seg_1226" s="T201">už</ta>
            <ta e="T203" id="Seg_1227" s="T202">tura-Tə</ta>
            <ta e="T204" id="Seg_1228" s="T203">mĭn-laʔbə</ta>
            <ta e="T205" id="Seg_1229" s="T204">dĭ</ta>
            <ta e="T206" id="Seg_1230" s="T205">bar</ta>
            <ta e="T207" id="Seg_1231" s="T206">sʼa-bi</ta>
            <ta e="T208" id="Seg_1232" s="T207">karɨta-ziʔ</ta>
            <ta e="T209" id="Seg_1233" s="T208">kaj-luʔbdə-bi</ta>
            <ta e="T210" id="Seg_1234" s="T209">nüke</ta>
            <ta e="T211" id="Seg_1235" s="T210">pʼeːš-Tə</ta>
            <ta e="T212" id="Seg_1236" s="T211">sʼa-luʔbdə-bi</ta>
            <ta e="T213" id="Seg_1237" s="T212">oldʼa-Tə</ta>
            <ta e="T214" id="Seg_1238" s="T213">šaʔ-laːm-bi</ta>
            <ta e="T215" id="Seg_1239" s="T214">urgaːba</ta>
            <ta e="T216" id="Seg_1240" s="T215">šo-bi</ta>
            <ta e="T217" id="Seg_1241" s="T216">da</ta>
            <ta e="T218" id="Seg_1242" s="T217">patpolʼlʼa-Tə</ta>
            <ta e="T220" id="Seg_1243" s="T219">păda-luʔbdə-bi</ta>
            <ta e="T221" id="Seg_1244" s="T220">a</ta>
            <ta e="T222" id="Seg_1245" s="T221">il</ta>
            <ta e="T223" id="Seg_1246" s="T222">šo-bi-jəʔ</ta>
            <ta e="T224" id="Seg_1247" s="T223">da</ta>
            <ta e="T225" id="Seg_1248" s="T224">urgaːba-m</ta>
            <ta e="T226" id="Seg_1249" s="T225">kut-laːm-bi-jəʔ</ta>
            <ta e="T227" id="Seg_1250" s="T226">kabarləj</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T81" id="Seg_1251" s="T80">live-PST-3PL</ta>
            <ta e="T82" id="Seg_1252" s="T81">woman.[NOM.SG]</ta>
            <ta e="T83" id="Seg_1253" s="T82">man-INS</ta>
            <ta e="T84" id="Seg_1254" s="T83">sow-PST-3PL</ta>
            <ta e="T85" id="Seg_1255" s="T84">turnip.[NOM.SG]</ta>
            <ta e="T86" id="Seg_1256" s="T85">this.[NOM.SG]</ta>
            <ta e="T87" id="Seg_1257" s="T86">big.[NOM.SG]</ta>
            <ta e="T88" id="Seg_1258" s="T87">grow-PST.[3SG]</ta>
            <ta e="T89" id="Seg_1259" s="T88">bear.[NOM.SG]</ta>
            <ta e="T90" id="Seg_1260" s="T89">come-PST.[3SG]</ta>
            <ta e="T91" id="Seg_1261" s="T90">this.[NOM.SG]</ta>
            <ta e="T92" id="Seg_1262" s="T91">tear-PST.[3SG]</ta>
            <ta e="T93" id="Seg_1263" s="T92">throw.away-MOM-PST.[3SG]</ta>
            <ta e="T94" id="Seg_1264" s="T93">man.[NOM.SG]</ta>
            <ta e="T95" id="Seg_1265" s="T94">go-PST.[3SG]</ta>
            <ta e="T96" id="Seg_1266" s="T95">look-INF.LAT</ta>
            <ta e="T97" id="Seg_1267" s="T96">who.[NOM.SG]=INDEF</ta>
            <ta e="T98" id="Seg_1268" s="T97">PTCL</ta>
            <ta e="T99" id="Seg_1269" s="T98">turnip-NOM/GEN.3SG</ta>
            <ta e="T100" id="Seg_1270" s="T99">tear-PST.[3SG]</ta>
            <ta e="T101" id="Seg_1271" s="T100">throw.away-MOM-PST.[3SG]</ta>
            <ta e="T102" id="Seg_1272" s="T101">woman.[NOM.SG]</ta>
            <ta e="T103" id="Seg_1273" s="T102">say-IPFVZ.[3SG]</ta>
            <ta e="T104" id="Seg_1274" s="T103">this</ta>
            <ta e="T105" id="Seg_1275" s="T104">however</ta>
            <ta e="T106" id="Seg_1276" s="T105">NEG</ta>
            <ta e="T107" id="Seg_1277" s="T106">%%</ta>
            <ta e="T108" id="Seg_1278" s="T107">and</ta>
            <ta e="T109" id="Seg_1279" s="T108">bear.[NOM.SG]</ta>
            <ta e="T110" id="Seg_1280" s="T109">go-EP-IMP.2SG</ta>
            <ta e="T111" id="Seg_1281" s="T110">go-EP-IMP.2SG</ta>
            <ta e="T112" id="Seg_1282" s="T111">this-ACC</ta>
            <ta e="T113" id="Seg_1283" s="T112">there</ta>
            <ta e="T114" id="Seg_1284" s="T113">sit-IMP.2SG</ta>
            <ta e="T115" id="Seg_1285" s="T114">and</ta>
            <ta e="T116" id="Seg_1286" s="T115">look-IMP.2SG</ta>
            <ta e="T117" id="Seg_1287" s="T116">who.[NOM.SG]</ta>
            <ta e="T118" id="Seg_1288" s="T117">go-PRS.[3SG]</ta>
            <ta e="T119" id="Seg_1289" s="T118">this.[NOM.SG]</ta>
            <ta e="T120" id="Seg_1290" s="T119">go-PST.[3SG]</ta>
            <ta e="T121" id="Seg_1291" s="T120">hide-RES-PST.[3SG]</ta>
            <ta e="T122" id="Seg_1292" s="T121">bear.[NOM.SG]</ta>
            <ta e="T123" id="Seg_1293" s="T122">come-PST.[3SG]</ta>
            <ta e="T124" id="Seg_1294" s="T123">many</ta>
            <ta e="T125" id="Seg_1295" s="T124">tear-PST.[3SG]</ta>
            <ta e="T126" id="Seg_1296" s="T125">and</ta>
            <ta e="T127" id="Seg_1297" s="T126">walk-PRS.[3SG]</ta>
            <ta e="T128" id="Seg_1298" s="T127">fence-LAT</ta>
            <ta e="T129" id="Seg_1299" s="T128">then</ta>
            <ta e="T130" id="Seg_1300" s="T129">this.[NOM.SG]</ta>
            <ta e="T131" id="Seg_1301" s="T130">axe-INS</ta>
            <ta e="T132" id="Seg_1302" s="T131">cut-MOM-PST.[3SG]</ta>
            <ta e="T133" id="Seg_1303" s="T132">foot-ACC.3SG</ta>
            <ta e="T134" id="Seg_1304" s="T133">foot-ACC.3SG</ta>
            <ta e="T135" id="Seg_1305" s="T134">this-GEN</ta>
            <ta e="T136" id="Seg_1306" s="T135">cut.off-MOM-PST.[3SG]</ta>
            <ta e="T137" id="Seg_1307" s="T136">then</ta>
            <ta e="T138" id="Seg_1308" s="T137">bring-PST.[3SG]</ta>
            <ta e="T140" id="Seg_1309" s="T139">woman-LAT/LOC.3SG</ta>
            <ta e="T141" id="Seg_1310" s="T140">take.it</ta>
            <ta e="T142" id="Seg_1311" s="T141">foot.[NOM.SG]</ta>
            <ta e="T143" id="Seg_1312" s="T142">bring-PST-1SG</ta>
            <ta e="T144" id="Seg_1313" s="T143">this.[NOM.SG]</ta>
            <ta e="T145" id="Seg_1314" s="T144">woman.[NOM.SG]</ta>
            <ta e="T146" id="Seg_1315" s="T145">foot-ACC.3SG</ta>
            <ta e="T147" id="Seg_1316" s="T146">peel.bark-PST.[3SG]</ta>
            <ta e="T148" id="Seg_1317" s="T147">meat-ACC.3SG</ta>
            <ta e="T149" id="Seg_1318" s="T148">put-PST.[3SG]</ta>
            <ta e="T150" id="Seg_1319" s="T149">cauldron-NOM/GEN/ACC.3SG</ta>
            <ta e="T151" id="Seg_1320" s="T150">boil-DUR.[3SG]</ta>
            <ta e="T152" id="Seg_1321" s="T151">and</ta>
            <ta e="T153" id="Seg_1322" s="T152">hair-NOM/GEN/ACC.3SG</ta>
            <ta e="T154" id="Seg_1323" s="T153">PTCL</ta>
            <ta e="T156" id="Seg_1324" s="T155">spin-INF.LAT</ta>
            <ta e="T158" id="Seg_1325" s="T157">and</ta>
            <ta e="T159" id="Seg_1326" s="T158">skin-LOC</ta>
            <ta e="T160" id="Seg_1327" s="T159">sit-DUR.[3SG]</ta>
            <ta e="T161" id="Seg_1328" s="T160">and</ta>
            <ta e="T162" id="Seg_1329" s="T161">bear.[NOM.SG]</ta>
            <ta e="T163" id="Seg_1330" s="T162">foot.[NOM.SG]</ta>
            <ta e="T164" id="Seg_1331" s="T163">make-PST.[3SG]</ta>
            <ta e="T165" id="Seg_1332" s="T164">linden-LOC</ta>
            <ta e="T166" id="Seg_1333" s="T165">and</ta>
            <ta e="T167" id="Seg_1334" s="T166">come-PRS.[3SG]</ta>
            <ta e="T168" id="Seg_1335" s="T167">say-IPFVZ.[3SG]</ta>
            <ta e="T169" id="Seg_1336" s="T168">I.GEN</ta>
            <ta e="T170" id="Seg_1337" s="T169">foot-NOM/GEN/ACC.1SG</ta>
            <ta e="T171" id="Seg_1338" s="T170">tree-ABL</ta>
            <ta e="T172" id="Seg_1339" s="T171">%%</ta>
            <ta e="T173" id="Seg_1340" s="T172">PTCL</ta>
            <ta e="T174" id="Seg_1341" s="T173">all</ta>
            <ta e="T175" id="Seg_1342" s="T174">people.[NOM.SG]</ta>
            <ta e="T176" id="Seg_1343" s="T175">sleep-DUR-3PL</ta>
            <ta e="T178" id="Seg_1344" s="T177">one.[NOM.SG]</ta>
            <ta e="T179" id="Seg_1345" s="T178">man.[NOM.SG]</ta>
            <ta e="T180" id="Seg_1346" s="T179">NEG</ta>
            <ta e="T181" id="Seg_1347" s="T180">sleep-DUR.[3SG]</ta>
            <ta e="T182" id="Seg_1348" s="T181">one.[NOM.SG]</ta>
            <ta e="T183" id="Seg_1349" s="T182">woman.[NOM.SG]</ta>
            <ta e="T184" id="Seg_1350" s="T183">NEG</ta>
            <ta e="T185" id="Seg_1351" s="T184">sleep-DUR.[3SG]</ta>
            <ta e="T186" id="Seg_1352" s="T185">meat.[NOM.SG]</ta>
            <ta e="T187" id="Seg_1353" s="T186">boil-DUR.[3SG]</ta>
            <ta e="T188" id="Seg_1354" s="T187">hair</ta>
            <ta e="T189" id="Seg_1355" s="T188">spin-DUR.[3SG]</ta>
            <ta e="T190" id="Seg_1356" s="T189">skin-LOC</ta>
            <ta e="T191" id="Seg_1357" s="T190">sit-DUR.[3SG]</ta>
            <ta e="T192" id="Seg_1358" s="T191">then</ta>
            <ta e="T193" id="Seg_1359" s="T192">woman.[NOM.SG]</ta>
            <ta e="T194" id="Seg_1360" s="T193">say-IPFVZ.[3SG]</ta>
            <ta e="T195" id="Seg_1361" s="T194">go-EP-IMP.2SG</ta>
            <ta e="T196" id="Seg_1362" s="T195">bear.[NOM.SG]</ta>
            <ta e="T197" id="Seg_1363" s="T196">come-PRS.[3SG]</ta>
            <ta e="T198" id="Seg_1364" s="T197">this.[NOM.SG]</ta>
            <ta e="T199" id="Seg_1365" s="T198">go-PST.[3SG]</ta>
            <ta e="T200" id="Seg_1366" s="T199">and</ta>
            <ta e="T201" id="Seg_1367" s="T200">this.[NOM.SG]</ta>
            <ta e="T202" id="Seg_1368" s="T201">PTCL</ta>
            <ta e="T203" id="Seg_1369" s="T202">house-LAT</ta>
            <ta e="T204" id="Seg_1370" s="T203">go-DUR.[3SG]</ta>
            <ta e="T205" id="Seg_1371" s="T204">this.[NOM.SG]</ta>
            <ta e="T206" id="Seg_1372" s="T205">PTCL</ta>
            <ta e="T207" id="Seg_1373" s="T206">climb-PST.[3SG]</ta>
            <ta e="T208" id="Seg_1374" s="T207">trough-INS</ta>
            <ta e="T209" id="Seg_1375" s="T208">close-MOM-PST.[3SG]</ta>
            <ta e="T210" id="Seg_1376" s="T209">woman.[NOM.SG]</ta>
            <ta e="T211" id="Seg_1377" s="T210">stove-LAT</ta>
            <ta e="T212" id="Seg_1378" s="T211">climb-MOM-PST</ta>
            <ta e="T213" id="Seg_1379" s="T212">clothing-LAT</ta>
            <ta e="T214" id="Seg_1380" s="T213">hide-RES-PST.[3SG]</ta>
            <ta e="T215" id="Seg_1381" s="T214">bear.[NOM.SG]</ta>
            <ta e="T216" id="Seg_1382" s="T215">come-PST.[3SG]</ta>
            <ta e="T217" id="Seg_1383" s="T216">and</ta>
            <ta e="T218" id="Seg_1384" s="T217">cellar-LAT</ta>
            <ta e="T220" id="Seg_1385" s="T219">creep.into-MOM-PST.[3SG]</ta>
            <ta e="T221" id="Seg_1386" s="T220">and</ta>
            <ta e="T222" id="Seg_1387" s="T221">people.[NOM.SG]</ta>
            <ta e="T223" id="Seg_1388" s="T222">come-PST-3PL</ta>
            <ta e="T224" id="Seg_1389" s="T223">and</ta>
            <ta e="T225" id="Seg_1390" s="T224">bear-ACC</ta>
            <ta e="T226" id="Seg_1391" s="T225">kill-RES-PST-3PL</ta>
            <ta e="T227" id="Seg_1392" s="T226">enough</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T81" id="Seg_1393" s="T80">жить-PST-3PL</ta>
            <ta e="T82" id="Seg_1394" s="T81">женщина.[NOM.SG]</ta>
            <ta e="T83" id="Seg_1395" s="T82">мужчина-INS</ta>
            <ta e="T84" id="Seg_1396" s="T83">сеять-PST-3PL</ta>
            <ta e="T85" id="Seg_1397" s="T84">репа.[NOM.SG]</ta>
            <ta e="T86" id="Seg_1398" s="T85">этот.[NOM.SG]</ta>
            <ta e="T87" id="Seg_1399" s="T86">большой.[NOM.SG]</ta>
            <ta e="T88" id="Seg_1400" s="T87">расти-PST.[3SG]</ta>
            <ta e="T89" id="Seg_1401" s="T88">медведь.[NOM.SG]</ta>
            <ta e="T90" id="Seg_1402" s="T89">прийти-PST.[3SG]</ta>
            <ta e="T91" id="Seg_1403" s="T90">этот.[NOM.SG]</ta>
            <ta e="T92" id="Seg_1404" s="T91">рвать-PST.[3SG]</ta>
            <ta e="T93" id="Seg_1405" s="T92">выбросить-MOM-PST.[3SG]</ta>
            <ta e="T94" id="Seg_1406" s="T93">мужчина.[NOM.SG]</ta>
            <ta e="T95" id="Seg_1407" s="T94">пойти-PST.[3SG]</ta>
            <ta e="T96" id="Seg_1408" s="T95">смотреть-INF.LAT</ta>
            <ta e="T97" id="Seg_1409" s="T96">кто.[NOM.SG]=INDEF</ta>
            <ta e="T98" id="Seg_1410" s="T97">PTCL</ta>
            <ta e="T99" id="Seg_1411" s="T98">репа-NOM/GEN.3SG</ta>
            <ta e="T100" id="Seg_1412" s="T99">рвать-PST.[3SG]</ta>
            <ta e="T101" id="Seg_1413" s="T100">выбросить-MOM-PST.[3SG]</ta>
            <ta e="T102" id="Seg_1414" s="T101">женщина.[NOM.SG]</ta>
            <ta e="T103" id="Seg_1415" s="T102">сказать-IPFVZ.[3SG]</ta>
            <ta e="T104" id="Seg_1416" s="T103">этот</ta>
            <ta e="T105" id="Seg_1417" s="T104">однако</ta>
            <ta e="T106" id="Seg_1418" s="T105">NEG</ta>
            <ta e="T108" id="Seg_1419" s="T107">а</ta>
            <ta e="T109" id="Seg_1420" s="T108">медведь.[NOM.SG]</ta>
            <ta e="T110" id="Seg_1421" s="T109">пойти-EP-IMP.2SG</ta>
            <ta e="T111" id="Seg_1422" s="T110">пойти-EP-IMP.2SG</ta>
            <ta e="T112" id="Seg_1423" s="T111">этот-ACC</ta>
            <ta e="T113" id="Seg_1424" s="T112">там</ta>
            <ta e="T114" id="Seg_1425" s="T113">сидеть-IMP.2SG</ta>
            <ta e="T115" id="Seg_1426" s="T114">и</ta>
            <ta e="T116" id="Seg_1427" s="T115">смотреть-IMP.2SG</ta>
            <ta e="T117" id="Seg_1428" s="T116">кто.[NOM.SG]</ta>
            <ta e="T118" id="Seg_1429" s="T117">идти-PRS.[3SG]</ta>
            <ta e="T119" id="Seg_1430" s="T118">этот.[NOM.SG]</ta>
            <ta e="T120" id="Seg_1431" s="T119">пойти-PST.[3SG]</ta>
            <ta e="T121" id="Seg_1432" s="T120">спрятаться-RES-PST.[3SG]</ta>
            <ta e="T122" id="Seg_1433" s="T121">медведь.[NOM.SG]</ta>
            <ta e="T123" id="Seg_1434" s="T122">прийти-PST.[3SG]</ta>
            <ta e="T124" id="Seg_1435" s="T123">много</ta>
            <ta e="T125" id="Seg_1436" s="T124">рвать-PST.[3SG]</ta>
            <ta e="T126" id="Seg_1437" s="T125">и</ta>
            <ta e="T127" id="Seg_1438" s="T126">идти-PRS.[3SG]</ta>
            <ta e="T128" id="Seg_1439" s="T127">забор-LAT</ta>
            <ta e="T129" id="Seg_1440" s="T128">тогда</ta>
            <ta e="T130" id="Seg_1441" s="T129">этот.[NOM.SG]</ta>
            <ta e="T131" id="Seg_1442" s="T130">топор-INS</ta>
            <ta e="T132" id="Seg_1443" s="T131">резать-MOM-PST.[3SG]</ta>
            <ta e="T133" id="Seg_1444" s="T132">нога-ACC.3SG</ta>
            <ta e="T134" id="Seg_1445" s="T133">нога-ACC.3SG</ta>
            <ta e="T135" id="Seg_1446" s="T134">этот-GEN</ta>
            <ta e="T136" id="Seg_1447" s="T135">отрезать-MOM-PST.[3SG]</ta>
            <ta e="T137" id="Seg_1448" s="T136">тогда</ta>
            <ta e="T138" id="Seg_1449" s="T137">принести-PST.[3SG]</ta>
            <ta e="T140" id="Seg_1450" s="T139">женщина-LAT/LOC.3SG</ta>
            <ta e="T141" id="Seg_1451" s="T140">возьми</ta>
            <ta e="T142" id="Seg_1452" s="T141">нога.[NOM.SG]</ta>
            <ta e="T143" id="Seg_1453" s="T142">принести-PST-1SG</ta>
            <ta e="T144" id="Seg_1454" s="T143">этот.[NOM.SG]</ta>
            <ta e="T145" id="Seg_1455" s="T144">женщина.[NOM.SG]</ta>
            <ta e="T146" id="Seg_1456" s="T145">нога-ACC.3SG</ta>
            <ta e="T147" id="Seg_1457" s="T146">снимать.кору-PST.[3SG]</ta>
            <ta e="T148" id="Seg_1458" s="T147">мясо-ACC.3SG</ta>
            <ta e="T149" id="Seg_1459" s="T148">класть-PST.[3SG]</ta>
            <ta e="T150" id="Seg_1460" s="T149">котел-NOM/GEN/ACC.3SG</ta>
            <ta e="T151" id="Seg_1461" s="T150">кипятить-DUR.[3SG]</ta>
            <ta e="T152" id="Seg_1462" s="T151">а</ta>
            <ta e="T153" id="Seg_1463" s="T152">шерсть-NOM/GEN/ACC.3SG</ta>
            <ta e="T154" id="Seg_1464" s="T153">PTCL</ta>
            <ta e="T156" id="Seg_1465" s="T155">прясть-INF.LAT</ta>
            <ta e="T158" id="Seg_1466" s="T157">а</ta>
            <ta e="T159" id="Seg_1467" s="T158">кожа-LOC</ta>
            <ta e="T160" id="Seg_1468" s="T159">сидеть-DUR.[3SG]</ta>
            <ta e="T161" id="Seg_1469" s="T160">а</ta>
            <ta e="T162" id="Seg_1470" s="T161">медведь.[NOM.SG]</ta>
            <ta e="T163" id="Seg_1471" s="T162">нога.[NOM.SG]</ta>
            <ta e="T164" id="Seg_1472" s="T163">делать-PST.[3SG]</ta>
            <ta e="T165" id="Seg_1473" s="T164">липа-LOC</ta>
            <ta e="T166" id="Seg_1474" s="T165">и</ta>
            <ta e="T167" id="Seg_1475" s="T166">прийти-PRS.[3SG]</ta>
            <ta e="T168" id="Seg_1476" s="T167">сказать-IPFVZ.[3SG]</ta>
            <ta e="T169" id="Seg_1477" s="T168">я.GEN</ta>
            <ta e="T170" id="Seg_1478" s="T169">нога-NOM/GEN/ACC.1SG</ta>
            <ta e="T171" id="Seg_1479" s="T170">дерево-ABL</ta>
            <ta e="T173" id="Seg_1480" s="T172">PTCL</ta>
            <ta e="T174" id="Seg_1481" s="T173">весь</ta>
            <ta e="T175" id="Seg_1482" s="T174">люди.[NOM.SG]</ta>
            <ta e="T176" id="Seg_1483" s="T175">спать-DUR-3PL</ta>
            <ta e="T178" id="Seg_1484" s="T177">один.[NOM.SG]</ta>
            <ta e="T179" id="Seg_1485" s="T178">мужчина.[NOM.SG]</ta>
            <ta e="T180" id="Seg_1486" s="T179">NEG</ta>
            <ta e="T181" id="Seg_1487" s="T180">спать-DUR.[3SG]</ta>
            <ta e="T182" id="Seg_1488" s="T181">один.[NOM.SG]</ta>
            <ta e="T183" id="Seg_1489" s="T182">женщина.[NOM.SG]</ta>
            <ta e="T184" id="Seg_1490" s="T183">NEG</ta>
            <ta e="T185" id="Seg_1491" s="T184">спать-DUR.[3SG]</ta>
            <ta e="T186" id="Seg_1492" s="T185">мясо.[NOM.SG]</ta>
            <ta e="T187" id="Seg_1493" s="T186">кипятить-DUR.[3SG]</ta>
            <ta e="T188" id="Seg_1494" s="T187">шерсть</ta>
            <ta e="T189" id="Seg_1495" s="T188">прясть-DUR.[3SG]</ta>
            <ta e="T190" id="Seg_1496" s="T189">кожа-LOC</ta>
            <ta e="T191" id="Seg_1497" s="T190">сидеть-DUR.[3SG]</ta>
            <ta e="T192" id="Seg_1498" s="T191">тогда</ta>
            <ta e="T193" id="Seg_1499" s="T192">женщина.[NOM.SG]</ta>
            <ta e="T194" id="Seg_1500" s="T193">сказать-IPFVZ.[3SG]</ta>
            <ta e="T195" id="Seg_1501" s="T194">пойти-EP-IMP.2SG</ta>
            <ta e="T196" id="Seg_1502" s="T195">медведь.[NOM.SG]</ta>
            <ta e="T197" id="Seg_1503" s="T196">прийти-PRS.[3SG]</ta>
            <ta e="T198" id="Seg_1504" s="T197">этот.[NOM.SG]</ta>
            <ta e="T199" id="Seg_1505" s="T198">пойти-PST.[3SG]</ta>
            <ta e="T200" id="Seg_1506" s="T199">а</ta>
            <ta e="T201" id="Seg_1507" s="T200">этот.[NOM.SG]</ta>
            <ta e="T202" id="Seg_1508" s="T201">PTCL</ta>
            <ta e="T203" id="Seg_1509" s="T202">дом-LAT</ta>
            <ta e="T204" id="Seg_1510" s="T203">идти-DUR.[3SG]</ta>
            <ta e="T205" id="Seg_1511" s="T204">этот.[NOM.SG]</ta>
            <ta e="T206" id="Seg_1512" s="T205">PTCL</ta>
            <ta e="T207" id="Seg_1513" s="T206">влезать-PST.[3SG]</ta>
            <ta e="T208" id="Seg_1514" s="T207">корыто-INS</ta>
            <ta e="T209" id="Seg_1515" s="T208">закрыть-MOM-PST.[3SG]</ta>
            <ta e="T210" id="Seg_1516" s="T209">женщина.[NOM.SG]</ta>
            <ta e="T211" id="Seg_1517" s="T210">печь-LAT</ta>
            <ta e="T212" id="Seg_1518" s="T211">влезать-MOM-PST</ta>
            <ta e="T213" id="Seg_1519" s="T212">одежда-LAT</ta>
            <ta e="T214" id="Seg_1520" s="T213">спрятаться-RES-PST.[3SG]</ta>
            <ta e="T215" id="Seg_1521" s="T214">медведь.[NOM.SG]</ta>
            <ta e="T216" id="Seg_1522" s="T215">прийти-PST.[3SG]</ta>
            <ta e="T217" id="Seg_1523" s="T216">и</ta>
            <ta e="T218" id="Seg_1524" s="T217">подвал-LAT</ta>
            <ta e="T220" id="Seg_1525" s="T219">ползти-MOM-PST.[3SG]</ta>
            <ta e="T221" id="Seg_1526" s="T220">а</ta>
            <ta e="T222" id="Seg_1527" s="T221">люди.[NOM.SG]</ta>
            <ta e="T223" id="Seg_1528" s="T222">прийти-PST-3PL</ta>
            <ta e="T224" id="Seg_1529" s="T223">и</ta>
            <ta e="T225" id="Seg_1530" s="T224">медведь-ACC</ta>
            <ta e="T226" id="Seg_1531" s="T225">убить-RES-PST-3PL</ta>
            <ta e="T227" id="Seg_1532" s="T226">хватит</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T81" id="Seg_1533" s="T80">v-v:tense-v:pn</ta>
            <ta e="T82" id="Seg_1534" s="T81">n-n:case</ta>
            <ta e="T83" id="Seg_1535" s="T82">n-n:case</ta>
            <ta e="T84" id="Seg_1536" s="T83">v-v:tense-v:pn</ta>
            <ta e="T85" id="Seg_1537" s="T84">n-n:case</ta>
            <ta e="T86" id="Seg_1538" s="T85">dempro-n:case</ta>
            <ta e="T87" id="Seg_1539" s="T86">adj-n:case</ta>
            <ta e="T88" id="Seg_1540" s="T87">v-v:tense-v:pn</ta>
            <ta e="T89" id="Seg_1541" s="T88">n-n:case</ta>
            <ta e="T90" id="Seg_1542" s="T89">v-v:tense-v:pn</ta>
            <ta e="T91" id="Seg_1543" s="T90">dempro-n:case</ta>
            <ta e="T92" id="Seg_1544" s="T91">v-v:tense-v:pn</ta>
            <ta e="T93" id="Seg_1545" s="T92">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T94" id="Seg_1546" s="T93">n-n:case</ta>
            <ta e="T95" id="Seg_1547" s="T94">v-v:tense-v:pn</ta>
            <ta e="T96" id="Seg_1548" s="T95">v-v:n.fin</ta>
            <ta e="T97" id="Seg_1549" s="T96">que-n:case=ptcl</ta>
            <ta e="T98" id="Seg_1550" s="T97">ptcl</ta>
            <ta e="T99" id="Seg_1551" s="T98">n-n:case.poss</ta>
            <ta e="T100" id="Seg_1552" s="T99">v-v:tense-v:pn</ta>
            <ta e="T101" id="Seg_1553" s="T100">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T102" id="Seg_1554" s="T101">n-n:case</ta>
            <ta e="T103" id="Seg_1555" s="T102">v-v&gt;v-v:pn</ta>
            <ta e="T104" id="Seg_1556" s="T103">dempro</ta>
            <ta e="T105" id="Seg_1557" s="T104">adv</ta>
            <ta e="T106" id="Seg_1558" s="T105">ptcl</ta>
            <ta e="T107" id="Seg_1559" s="T106">%%</ta>
            <ta e="T108" id="Seg_1560" s="T107">conj</ta>
            <ta e="T109" id="Seg_1561" s="T108">n-n:case</ta>
            <ta e="T110" id="Seg_1562" s="T109">v-v:ins-v:mood.pn</ta>
            <ta e="T111" id="Seg_1563" s="T110">v-v:ins-v:mood.pn</ta>
            <ta e="T112" id="Seg_1564" s="T111">dempro-n:case</ta>
            <ta e="T113" id="Seg_1565" s="T112">adv</ta>
            <ta e="T114" id="Seg_1566" s="T113">v-v:mood.pn</ta>
            <ta e="T115" id="Seg_1567" s="T114">conj</ta>
            <ta e="T116" id="Seg_1568" s="T115">v-v:mood.pn</ta>
            <ta e="T117" id="Seg_1569" s="T116">que</ta>
            <ta e="T118" id="Seg_1570" s="T117">v-v:tense-v:pn</ta>
            <ta e="T119" id="Seg_1571" s="T118">dempro-n:case</ta>
            <ta e="T120" id="Seg_1572" s="T119">v-v:tense-v:pn</ta>
            <ta e="T121" id="Seg_1573" s="T120">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T122" id="Seg_1574" s="T121">n-n:case</ta>
            <ta e="T123" id="Seg_1575" s="T122">v-v:tense-v:pn</ta>
            <ta e="T124" id="Seg_1576" s="T123">quant</ta>
            <ta e="T125" id="Seg_1577" s="T124">v-v:tense-v:pn</ta>
            <ta e="T126" id="Seg_1578" s="T125">conj</ta>
            <ta e="T127" id="Seg_1579" s="T126">v-v:tense-v:pn</ta>
            <ta e="T128" id="Seg_1580" s="T127">n-n:case</ta>
            <ta e="T129" id="Seg_1581" s="T128">adv</ta>
            <ta e="T130" id="Seg_1582" s="T129">dempro-n:case</ta>
            <ta e="T131" id="Seg_1583" s="T130">n-n:case</ta>
            <ta e="T132" id="Seg_1584" s="T131">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T133" id="Seg_1585" s="T132">n-n:case.poss</ta>
            <ta e="T134" id="Seg_1586" s="T133">n-n:case.poss</ta>
            <ta e="T135" id="Seg_1587" s="T134">dempro-n:case</ta>
            <ta e="T136" id="Seg_1588" s="T135">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T137" id="Seg_1589" s="T136">adv</ta>
            <ta e="T138" id="Seg_1590" s="T137">v-v:tense-v:pn</ta>
            <ta e="T140" id="Seg_1591" s="T139">n-n:case.poss</ta>
            <ta e="T141" id="Seg_1592" s="T140">ptcl</ta>
            <ta e="T142" id="Seg_1593" s="T141">n-n:case</ta>
            <ta e="T143" id="Seg_1594" s="T142">v-v:tense-v:pn</ta>
            <ta e="T144" id="Seg_1595" s="T143">dempro-n:case</ta>
            <ta e="T145" id="Seg_1596" s="T144">n-n:case</ta>
            <ta e="T146" id="Seg_1597" s="T145">n-n:case.poss</ta>
            <ta e="T147" id="Seg_1598" s="T146">v-v:tense-v:pn</ta>
            <ta e="T148" id="Seg_1599" s="T147">n-n:case.poss</ta>
            <ta e="T149" id="Seg_1600" s="T148">v-v:tense-v:pn</ta>
            <ta e="T150" id="Seg_1601" s="T149">n-n:case.poss</ta>
            <ta e="T151" id="Seg_1602" s="T150">v-v&gt;v-v:pn</ta>
            <ta e="T152" id="Seg_1603" s="T151">conj</ta>
            <ta e="T153" id="Seg_1604" s="T152">n-n:case.poss</ta>
            <ta e="T154" id="Seg_1605" s="T153">ptcl</ta>
            <ta e="T156" id="Seg_1606" s="T155">v-v:n.fin</ta>
            <ta e="T158" id="Seg_1607" s="T157">conj</ta>
            <ta e="T159" id="Seg_1608" s="T158">n-n:case</ta>
            <ta e="T160" id="Seg_1609" s="T159">v-v&gt;v-v:pn</ta>
            <ta e="T161" id="Seg_1610" s="T160">conj</ta>
            <ta e="T162" id="Seg_1611" s="T161">n-n:case</ta>
            <ta e="T163" id="Seg_1612" s="T162">n-n:case</ta>
            <ta e="T164" id="Seg_1613" s="T163">v-v:tense-v:pn</ta>
            <ta e="T165" id="Seg_1614" s="T164">n-n:case</ta>
            <ta e="T166" id="Seg_1615" s="T165">conj</ta>
            <ta e="T167" id="Seg_1616" s="T166">v-v:tense-v:pn</ta>
            <ta e="T168" id="Seg_1617" s="T167">v-v&gt;v-v:pn</ta>
            <ta e="T169" id="Seg_1618" s="T168">pers</ta>
            <ta e="T170" id="Seg_1619" s="T169">n-n:case.poss</ta>
            <ta e="T171" id="Seg_1620" s="T170">n-n:case</ta>
            <ta e="T172" id="Seg_1621" s="T171">%%</ta>
            <ta e="T173" id="Seg_1622" s="T172">ptcl</ta>
            <ta e="T174" id="Seg_1623" s="T173">quant</ta>
            <ta e="T175" id="Seg_1624" s="T174">n-n:case</ta>
            <ta e="T176" id="Seg_1625" s="T175">v-v&gt;v-v:pn</ta>
            <ta e="T178" id="Seg_1626" s="T177">num-n:case</ta>
            <ta e="T179" id="Seg_1627" s="T178">n-n:case</ta>
            <ta e="T180" id="Seg_1628" s="T179">ptcl</ta>
            <ta e="T181" id="Seg_1629" s="T180">v-v&gt;v-v:pn</ta>
            <ta e="T182" id="Seg_1630" s="T181">num-n:case</ta>
            <ta e="T183" id="Seg_1631" s="T182">n-n:case</ta>
            <ta e="T184" id="Seg_1632" s="T183">ptcl</ta>
            <ta e="T185" id="Seg_1633" s="T184">v-v&gt;v-v:pn</ta>
            <ta e="T186" id="Seg_1634" s="T185">n-n:case</ta>
            <ta e="T187" id="Seg_1635" s="T186">v-v&gt;v-v:pn</ta>
            <ta e="T188" id="Seg_1636" s="T187">n</ta>
            <ta e="T189" id="Seg_1637" s="T188">v-v&gt;v-v:pn</ta>
            <ta e="T190" id="Seg_1638" s="T189">n-n:case</ta>
            <ta e="T191" id="Seg_1639" s="T190">v-v&gt;v-v:pn</ta>
            <ta e="T192" id="Seg_1640" s="T191">adv</ta>
            <ta e="T193" id="Seg_1641" s="T192">n-n:case</ta>
            <ta e="T194" id="Seg_1642" s="T193">v-v&gt;v-v:pn</ta>
            <ta e="T195" id="Seg_1643" s="T194">v-v:ins-v:mood.pn</ta>
            <ta e="T196" id="Seg_1644" s="T195">n-n:case</ta>
            <ta e="T197" id="Seg_1645" s="T196">v-v:tense-v:pn</ta>
            <ta e="T198" id="Seg_1646" s="T197">dempro-n:case</ta>
            <ta e="T199" id="Seg_1647" s="T198">v-v:tense-v:pn</ta>
            <ta e="T200" id="Seg_1648" s="T199">conj</ta>
            <ta e="T201" id="Seg_1649" s="T200">dempro-n:case</ta>
            <ta e="T202" id="Seg_1650" s="T201">ptcl</ta>
            <ta e="T203" id="Seg_1651" s="T202">n-n:case</ta>
            <ta e="T204" id="Seg_1652" s="T203">v-v&gt;v-v:pn</ta>
            <ta e="T205" id="Seg_1653" s="T204">dempro-n:case</ta>
            <ta e="T206" id="Seg_1654" s="T205">ptcl</ta>
            <ta e="T207" id="Seg_1655" s="T206">v-v:tense-v:pn</ta>
            <ta e="T208" id="Seg_1656" s="T207">n-n:case</ta>
            <ta e="T209" id="Seg_1657" s="T208">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T210" id="Seg_1658" s="T209">n-n:case</ta>
            <ta e="T211" id="Seg_1659" s="T210">n-n:case</ta>
            <ta e="T212" id="Seg_1660" s="T211">v-v&gt;v-v:tense</ta>
            <ta e="T213" id="Seg_1661" s="T212">n-n:case</ta>
            <ta e="T214" id="Seg_1662" s="T213">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T215" id="Seg_1663" s="T214">n-n:case</ta>
            <ta e="T216" id="Seg_1664" s="T215">v-v:tense-v:pn</ta>
            <ta e="T217" id="Seg_1665" s="T216">conj</ta>
            <ta e="T218" id="Seg_1666" s="T217">n-n:case</ta>
            <ta e="T220" id="Seg_1667" s="T219">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T221" id="Seg_1668" s="T220">conj</ta>
            <ta e="T222" id="Seg_1669" s="T221">n-n:case</ta>
            <ta e="T223" id="Seg_1670" s="T222">v-v:tense-v:pn</ta>
            <ta e="T224" id="Seg_1671" s="T223">conj</ta>
            <ta e="T225" id="Seg_1672" s="T224">n-n:case</ta>
            <ta e="T226" id="Seg_1673" s="T225">v-v&gt;v-v:tense-v:pn</ta>
            <ta e="T227" id="Seg_1674" s="T226">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T81" id="Seg_1675" s="T80">v</ta>
            <ta e="T82" id="Seg_1676" s="T81">n</ta>
            <ta e="T83" id="Seg_1677" s="T82">n</ta>
            <ta e="T84" id="Seg_1678" s="T83">v</ta>
            <ta e="T85" id="Seg_1679" s="T84">n</ta>
            <ta e="T86" id="Seg_1680" s="T85">dempro</ta>
            <ta e="T87" id="Seg_1681" s="T86">adj</ta>
            <ta e="T88" id="Seg_1682" s="T87">v</ta>
            <ta e="T89" id="Seg_1683" s="T88">n</ta>
            <ta e="T90" id="Seg_1684" s="T89">v</ta>
            <ta e="T91" id="Seg_1685" s="T90">dempro</ta>
            <ta e="T92" id="Seg_1686" s="T91">v</ta>
            <ta e="T93" id="Seg_1687" s="T92">v</ta>
            <ta e="T94" id="Seg_1688" s="T93">n</ta>
            <ta e="T95" id="Seg_1689" s="T94">v</ta>
            <ta e="T96" id="Seg_1690" s="T95">v</ta>
            <ta e="T97" id="Seg_1691" s="T96">que</ta>
            <ta e="T98" id="Seg_1692" s="T97">ptcl</ta>
            <ta e="T99" id="Seg_1693" s="T98">n</ta>
            <ta e="T100" id="Seg_1694" s="T99">v</ta>
            <ta e="T101" id="Seg_1695" s="T100">v</ta>
            <ta e="T102" id="Seg_1696" s="T101">n</ta>
            <ta e="T103" id="Seg_1697" s="T102">v</ta>
            <ta e="T104" id="Seg_1698" s="T103">dempro</ta>
            <ta e="T105" id="Seg_1699" s="T104">adv</ta>
            <ta e="T106" id="Seg_1700" s="T105">ptcl</ta>
            <ta e="T108" id="Seg_1701" s="T107">conj</ta>
            <ta e="T109" id="Seg_1702" s="T108">n</ta>
            <ta e="T110" id="Seg_1703" s="T109">v</ta>
            <ta e="T111" id="Seg_1704" s="T110">v</ta>
            <ta e="T112" id="Seg_1705" s="T111">dempro</ta>
            <ta e="T113" id="Seg_1706" s="T112">adv</ta>
            <ta e="T114" id="Seg_1707" s="T113">v</ta>
            <ta e="T115" id="Seg_1708" s="T114">conj</ta>
            <ta e="T116" id="Seg_1709" s="T115">v</ta>
            <ta e="T117" id="Seg_1710" s="T116">que</ta>
            <ta e="T118" id="Seg_1711" s="T117">v</ta>
            <ta e="T119" id="Seg_1712" s="T118">dempro</ta>
            <ta e="T120" id="Seg_1713" s="T119">v</ta>
            <ta e="T121" id="Seg_1714" s="T120">v</ta>
            <ta e="T122" id="Seg_1715" s="T121">n</ta>
            <ta e="T123" id="Seg_1716" s="T122">v</ta>
            <ta e="T124" id="Seg_1717" s="T123">quant</ta>
            <ta e="T125" id="Seg_1718" s="T124">v</ta>
            <ta e="T126" id="Seg_1719" s="T125">conj</ta>
            <ta e="T127" id="Seg_1720" s="T126">v</ta>
            <ta e="T128" id="Seg_1721" s="T127">n</ta>
            <ta e="T129" id="Seg_1722" s="T128">adv</ta>
            <ta e="T130" id="Seg_1723" s="T129">dempro</ta>
            <ta e="T131" id="Seg_1724" s="T130">n</ta>
            <ta e="T132" id="Seg_1725" s="T131">v</ta>
            <ta e="T133" id="Seg_1726" s="T132">n</ta>
            <ta e="T134" id="Seg_1727" s="T133">n</ta>
            <ta e="T135" id="Seg_1728" s="T134">dempro</ta>
            <ta e="T136" id="Seg_1729" s="T135">v</ta>
            <ta e="T137" id="Seg_1730" s="T136">adv</ta>
            <ta e="T138" id="Seg_1731" s="T137">v</ta>
            <ta e="T140" id="Seg_1732" s="T139">n</ta>
            <ta e="T141" id="Seg_1733" s="T140">ptcl</ta>
            <ta e="T142" id="Seg_1734" s="T141">n</ta>
            <ta e="T143" id="Seg_1735" s="T142">v</ta>
            <ta e="T144" id="Seg_1736" s="T143">dempro</ta>
            <ta e="T145" id="Seg_1737" s="T144">n</ta>
            <ta e="T146" id="Seg_1738" s="T145">n</ta>
            <ta e="T147" id="Seg_1739" s="T146">v</ta>
            <ta e="T148" id="Seg_1740" s="T147">n</ta>
            <ta e="T149" id="Seg_1741" s="T148">v</ta>
            <ta e="T150" id="Seg_1742" s="T149">n</ta>
            <ta e="T151" id="Seg_1743" s="T150">v</ta>
            <ta e="T152" id="Seg_1744" s="T151">conj</ta>
            <ta e="T153" id="Seg_1745" s="T152">n</ta>
            <ta e="T154" id="Seg_1746" s="T153">ptcl</ta>
            <ta e="T156" id="Seg_1747" s="T155">v</ta>
            <ta e="T158" id="Seg_1748" s="T157">conj</ta>
            <ta e="T159" id="Seg_1749" s="T158">n</ta>
            <ta e="T160" id="Seg_1750" s="T159">v</ta>
            <ta e="T161" id="Seg_1751" s="T160">conj</ta>
            <ta e="T162" id="Seg_1752" s="T161">n</ta>
            <ta e="T163" id="Seg_1753" s="T162">n</ta>
            <ta e="T164" id="Seg_1754" s="T163">v</ta>
            <ta e="T165" id="Seg_1755" s="T164">n</ta>
            <ta e="T166" id="Seg_1756" s="T165">conj</ta>
            <ta e="T167" id="Seg_1757" s="T166">v</ta>
            <ta e="T168" id="Seg_1758" s="T167">v</ta>
            <ta e="T169" id="Seg_1759" s="T168">pers</ta>
            <ta e="T170" id="Seg_1760" s="T169">n</ta>
            <ta e="T171" id="Seg_1761" s="T170">n</ta>
            <ta e="T173" id="Seg_1762" s="T172">ptcl</ta>
            <ta e="T174" id="Seg_1763" s="T173">quant</ta>
            <ta e="T175" id="Seg_1764" s="T174">n</ta>
            <ta e="T176" id="Seg_1765" s="T175">v</ta>
            <ta e="T178" id="Seg_1766" s="T177">num</ta>
            <ta e="T179" id="Seg_1767" s="T178">n</ta>
            <ta e="T180" id="Seg_1768" s="T179">ptcl</ta>
            <ta e="T181" id="Seg_1769" s="T180">v</ta>
            <ta e="T182" id="Seg_1770" s="T181">num</ta>
            <ta e="T183" id="Seg_1771" s="T182">n</ta>
            <ta e="T184" id="Seg_1772" s="T183">ptcl</ta>
            <ta e="T185" id="Seg_1773" s="T184">v</ta>
            <ta e="T186" id="Seg_1774" s="T185">n</ta>
            <ta e="T187" id="Seg_1775" s="T186">v</ta>
            <ta e="T188" id="Seg_1776" s="T187">n</ta>
            <ta e="T189" id="Seg_1777" s="T188">v</ta>
            <ta e="T190" id="Seg_1778" s="T189">n</ta>
            <ta e="T191" id="Seg_1779" s="T190">v</ta>
            <ta e="T192" id="Seg_1780" s="T191">adv</ta>
            <ta e="T193" id="Seg_1781" s="T192">n</ta>
            <ta e="T194" id="Seg_1782" s="T193">v</ta>
            <ta e="T195" id="Seg_1783" s="T194">v</ta>
            <ta e="T196" id="Seg_1784" s="T195">n</ta>
            <ta e="T197" id="Seg_1785" s="T196">v</ta>
            <ta e="T198" id="Seg_1786" s="T197">dempro</ta>
            <ta e="T199" id="Seg_1787" s="T198">v</ta>
            <ta e="T200" id="Seg_1788" s="T199">conj</ta>
            <ta e="T201" id="Seg_1789" s="T200">dempro</ta>
            <ta e="T202" id="Seg_1790" s="T201">ptcl</ta>
            <ta e="T203" id="Seg_1791" s="T202">n</ta>
            <ta e="T204" id="Seg_1792" s="T203">v</ta>
            <ta e="T205" id="Seg_1793" s="T204">dempro</ta>
            <ta e="T206" id="Seg_1794" s="T205">ptcl</ta>
            <ta e="T207" id="Seg_1795" s="T206">v</ta>
            <ta e="T208" id="Seg_1796" s="T207">n</ta>
            <ta e="T209" id="Seg_1797" s="T208">v</ta>
            <ta e="T210" id="Seg_1798" s="T209">n</ta>
            <ta e="T211" id="Seg_1799" s="T210">n</ta>
            <ta e="T212" id="Seg_1800" s="T211">v</ta>
            <ta e="T213" id="Seg_1801" s="T212">n</ta>
            <ta e="T214" id="Seg_1802" s="T213">v</ta>
            <ta e="T215" id="Seg_1803" s="T214">n</ta>
            <ta e="T216" id="Seg_1804" s="T215">v</ta>
            <ta e="T217" id="Seg_1805" s="T216">conj</ta>
            <ta e="T218" id="Seg_1806" s="T217">n</ta>
            <ta e="T220" id="Seg_1807" s="T219">v</ta>
            <ta e="T221" id="Seg_1808" s="T220">conj</ta>
            <ta e="T222" id="Seg_1809" s="T221">n</ta>
            <ta e="T223" id="Seg_1810" s="T222">v</ta>
            <ta e="T224" id="Seg_1811" s="T223">conj</ta>
            <ta e="T225" id="Seg_1812" s="T224">n</ta>
            <ta e="T226" id="Seg_1813" s="T225">v</ta>
            <ta e="T227" id="Seg_1814" s="T226">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T82" id="Seg_1815" s="T81">np.h:E</ta>
            <ta e="T83" id="Seg_1816" s="T82">np.h:Com</ta>
            <ta e="T84" id="Seg_1817" s="T83">0.3.h:A</ta>
            <ta e="T85" id="Seg_1818" s="T84">np:Th</ta>
            <ta e="T86" id="Seg_1819" s="T85">pro:Th</ta>
            <ta e="T89" id="Seg_1820" s="T88">np.h:A</ta>
            <ta e="T91" id="Seg_1821" s="T90">pro.h:A</ta>
            <ta e="T93" id="Seg_1822" s="T92">0.3.h:A</ta>
            <ta e="T94" id="Seg_1823" s="T93">np.h:A</ta>
            <ta e="T97" id="Seg_1824" s="T96">pro.h:A</ta>
            <ta e="T99" id="Seg_1825" s="T98">np:P</ta>
            <ta e="T101" id="Seg_1826" s="T100">0.3.h:A</ta>
            <ta e="T102" id="Seg_1827" s="T101">np.h:A</ta>
            <ta e="T104" id="Seg_1828" s="T103">pro.h:Th</ta>
            <ta e="T109" id="Seg_1829" s="T108">np:Th</ta>
            <ta e="T110" id="Seg_1830" s="T109">0.2.h:A</ta>
            <ta e="T111" id="Seg_1831" s="T110">0.2.h:A</ta>
            <ta e="T113" id="Seg_1832" s="T112">adv:L</ta>
            <ta e="T114" id="Seg_1833" s="T113">0.2.h:A</ta>
            <ta e="T116" id="Seg_1834" s="T115">0.2.h:A</ta>
            <ta e="T117" id="Seg_1835" s="T116">pro.h:A</ta>
            <ta e="T119" id="Seg_1836" s="T118">pro.h:A</ta>
            <ta e="T121" id="Seg_1837" s="T120">0.3.h:A</ta>
            <ta e="T122" id="Seg_1838" s="T121">np.h:A</ta>
            <ta e="T125" id="Seg_1839" s="T124">0.3.h:A</ta>
            <ta e="T127" id="Seg_1840" s="T126">0.3.h:A</ta>
            <ta e="T129" id="Seg_1841" s="T128">adv:Time</ta>
            <ta e="T130" id="Seg_1842" s="T129">pro.h:A</ta>
            <ta e="T131" id="Seg_1843" s="T130">np:Ins</ta>
            <ta e="T133" id="Seg_1844" s="T132">np:Th</ta>
            <ta e="T134" id="Seg_1845" s="T133">np:P</ta>
            <ta e="T135" id="Seg_1846" s="T134">pro.h:Poss</ta>
            <ta e="T136" id="Seg_1847" s="T135">0.3.h:A</ta>
            <ta e="T137" id="Seg_1848" s="T136">adv:Time</ta>
            <ta e="T138" id="Seg_1849" s="T137">0.3.h:A</ta>
            <ta e="T140" id="Seg_1850" s="T139">np.h:R</ta>
            <ta e="T142" id="Seg_1851" s="T141">np:Th</ta>
            <ta e="T143" id="Seg_1852" s="T142">0.1.h:A</ta>
            <ta e="T145" id="Seg_1853" s="T144">np.h:A</ta>
            <ta e="T146" id="Seg_1854" s="T145">np:P</ta>
            <ta e="T148" id="Seg_1855" s="T147">np:Th</ta>
            <ta e="T149" id="Seg_1856" s="T148">0.3.h:A</ta>
            <ta e="T150" id="Seg_1857" s="T149">np:L</ta>
            <ta e="T151" id="Seg_1858" s="T150">0.3.h:A</ta>
            <ta e="T153" id="Seg_1859" s="T152">np:P</ta>
            <ta e="T159" id="Seg_1860" s="T158">np:L</ta>
            <ta e="T160" id="Seg_1861" s="T159">0.3.h:E</ta>
            <ta e="T162" id="Seg_1862" s="T161">np.h:A</ta>
            <ta e="T163" id="Seg_1863" s="T162">np:P</ta>
            <ta e="T165" id="Seg_1864" s="T164">np:Ins</ta>
            <ta e="T167" id="Seg_1865" s="T166">0.3.h:A</ta>
            <ta e="T168" id="Seg_1866" s="T167">0.3.h:A</ta>
            <ta e="T169" id="Seg_1867" s="T168">pro.h:Poss</ta>
            <ta e="T170" id="Seg_1868" s="T169">np:Th</ta>
            <ta e="T175" id="Seg_1869" s="T174">np.h:E</ta>
            <ta e="T179" id="Seg_1870" s="T178">np.h:E</ta>
            <ta e="T183" id="Seg_1871" s="T182">np.h:E</ta>
            <ta e="T186" id="Seg_1872" s="T185">np:P</ta>
            <ta e="T187" id="Seg_1873" s="T186">0.3.h:A</ta>
            <ta e="T188" id="Seg_1874" s="T187">np:P</ta>
            <ta e="T189" id="Seg_1875" s="T188">0.3.h:A</ta>
            <ta e="T190" id="Seg_1876" s="T189">np:L</ta>
            <ta e="T191" id="Seg_1877" s="T190">0.3.h:E</ta>
            <ta e="T192" id="Seg_1878" s="T191">adv:Time</ta>
            <ta e="T193" id="Seg_1879" s="T192">np.h:A</ta>
            <ta e="T195" id="Seg_1880" s="T194">0.2.h:A</ta>
            <ta e="T196" id="Seg_1881" s="T195">np.h:A</ta>
            <ta e="T198" id="Seg_1882" s="T197">pro.h:A</ta>
            <ta e="T201" id="Seg_1883" s="T200">pro.h:A</ta>
            <ta e="T203" id="Seg_1884" s="T202">np:G</ta>
            <ta e="T205" id="Seg_1885" s="T204">pro.h:A</ta>
            <ta e="T208" id="Seg_1886" s="T207">np:Ins</ta>
            <ta e="T209" id="Seg_1887" s="T208">0.3.h:A</ta>
            <ta e="T210" id="Seg_1888" s="T209">np.h:A</ta>
            <ta e="T211" id="Seg_1889" s="T210">np:G</ta>
            <ta e="T213" id="Seg_1890" s="T212">np:G</ta>
            <ta e="T214" id="Seg_1891" s="T213">0.3.h:A</ta>
            <ta e="T215" id="Seg_1892" s="T214">np.h:A</ta>
            <ta e="T218" id="Seg_1893" s="T217">np:G</ta>
            <ta e="T220" id="Seg_1894" s="T219">0.3.h:E</ta>
            <ta e="T222" id="Seg_1895" s="T221">np.h:A</ta>
            <ta e="T225" id="Seg_1896" s="T224">np.h:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T81" id="Seg_1897" s="T80">v:pred</ta>
            <ta e="T82" id="Seg_1898" s="T81">np.h:S</ta>
            <ta e="T84" id="Seg_1899" s="T83">v:pred 0.3.h:S</ta>
            <ta e="T85" id="Seg_1900" s="T84">np:O</ta>
            <ta e="T86" id="Seg_1901" s="T85">pro:S</ta>
            <ta e="T88" id="Seg_1902" s="T87">v:pred</ta>
            <ta e="T89" id="Seg_1903" s="T88">np.h:S</ta>
            <ta e="T90" id="Seg_1904" s="T89">v:pred</ta>
            <ta e="T91" id="Seg_1905" s="T90">pro.h:S</ta>
            <ta e="T92" id="Seg_1906" s="T91">v:pred</ta>
            <ta e="T93" id="Seg_1907" s="T92">v:pred 0.3.h:S</ta>
            <ta e="T94" id="Seg_1908" s="T93">np.h:S</ta>
            <ta e="T95" id="Seg_1909" s="T94">v:pred</ta>
            <ta e="T96" id="Seg_1910" s="T95">s:purp</ta>
            <ta e="T97" id="Seg_1911" s="T96">pro.h:S</ta>
            <ta e="T99" id="Seg_1912" s="T98">np:O</ta>
            <ta e="T100" id="Seg_1913" s="T99">v:pred</ta>
            <ta e="T101" id="Seg_1914" s="T100">v:pred 0.3.h:S</ta>
            <ta e="T102" id="Seg_1915" s="T101">np.h:S</ta>
            <ta e="T103" id="Seg_1916" s="T102">v:pred</ta>
            <ta e="T104" id="Seg_1917" s="T103">pro.h:S</ta>
            <ta e="T106" id="Seg_1918" s="T105">ptcl.neg</ta>
            <ta e="T109" id="Seg_1919" s="T108">n:pred</ta>
            <ta e="T110" id="Seg_1920" s="T109">v:pred 0.2.h:S</ta>
            <ta e="T111" id="Seg_1921" s="T110">v:pred 0.2.h:S</ta>
            <ta e="T114" id="Seg_1922" s="T113">v:pred 0.2.h:S</ta>
            <ta e="T116" id="Seg_1923" s="T115">v:pred 0.2.h:S</ta>
            <ta e="T117" id="Seg_1924" s="T116">pro.h:S</ta>
            <ta e="T118" id="Seg_1925" s="T117">v:pred</ta>
            <ta e="T119" id="Seg_1926" s="T118">pro.h:S</ta>
            <ta e="T120" id="Seg_1927" s="T119">v:pred</ta>
            <ta e="T121" id="Seg_1928" s="T120">v:pred 0.3.h:S</ta>
            <ta e="T122" id="Seg_1929" s="T121">np.h:S</ta>
            <ta e="T123" id="Seg_1930" s="T122">v:pred</ta>
            <ta e="T125" id="Seg_1931" s="T124">v:pred 0.3.h:S</ta>
            <ta e="T127" id="Seg_1932" s="T126">v:pred 0.3.h:S</ta>
            <ta e="T130" id="Seg_1933" s="T129">pro.h:S</ta>
            <ta e="T132" id="Seg_1934" s="T131">v:pred</ta>
            <ta e="T133" id="Seg_1935" s="T132">np:O</ta>
            <ta e="T134" id="Seg_1936" s="T133">np:O</ta>
            <ta e="T136" id="Seg_1937" s="T135">v:pred 0.3.h:S</ta>
            <ta e="T138" id="Seg_1938" s="T137">v:pred 0.3.h:S</ta>
            <ta e="T141" id="Seg_1939" s="T140">ptcl:pred</ta>
            <ta e="T142" id="Seg_1940" s="T141">np:O</ta>
            <ta e="T143" id="Seg_1941" s="T142">v:pred 0.1.h:S</ta>
            <ta e="T145" id="Seg_1942" s="T144">np.h:S</ta>
            <ta e="T146" id="Seg_1943" s="T145">np:O</ta>
            <ta e="T147" id="Seg_1944" s="T146">v:pred</ta>
            <ta e="T148" id="Seg_1945" s="T147">np:O</ta>
            <ta e="T149" id="Seg_1946" s="T148">v:pred 0.3.h:S</ta>
            <ta e="T151" id="Seg_1947" s="T150">v:pred 0.3.h:S</ta>
            <ta e="T153" id="Seg_1948" s="T152">np:O</ta>
            <ta e="T156" id="Seg_1949" s="T155">v:pred</ta>
            <ta e="T160" id="Seg_1950" s="T159">v:pred 0.3.h:S</ta>
            <ta e="T162" id="Seg_1951" s="T161">np.h:S</ta>
            <ta e="T163" id="Seg_1952" s="T162">np:O</ta>
            <ta e="T164" id="Seg_1953" s="T163">v:pred</ta>
            <ta e="T167" id="Seg_1954" s="T166">v:pred 0.3.h:S</ta>
            <ta e="T168" id="Seg_1955" s="T167">v:pred 0.3.h:S</ta>
            <ta e="T170" id="Seg_1956" s="T169">np:S</ta>
            <ta e="T171" id="Seg_1957" s="T170">n:pred</ta>
            <ta e="T175" id="Seg_1958" s="T174">np.h:S</ta>
            <ta e="T176" id="Seg_1959" s="T175">v:pred</ta>
            <ta e="T179" id="Seg_1960" s="T178">np.h:S</ta>
            <ta e="T180" id="Seg_1961" s="T179">ptcl.neg</ta>
            <ta e="T181" id="Seg_1962" s="T180">v:pred</ta>
            <ta e="T183" id="Seg_1963" s="T182">np.h:S</ta>
            <ta e="T184" id="Seg_1964" s="T183">ptcl.neg</ta>
            <ta e="T185" id="Seg_1965" s="T184">v:pred</ta>
            <ta e="T186" id="Seg_1966" s="T185">np:O</ta>
            <ta e="T187" id="Seg_1967" s="T186">v:pred 0.3.h:S</ta>
            <ta e="T188" id="Seg_1968" s="T187">np:O</ta>
            <ta e="T189" id="Seg_1969" s="T188">v:pred 0.3.h:S</ta>
            <ta e="T191" id="Seg_1970" s="T190">v:pred 0.3.h:S</ta>
            <ta e="T193" id="Seg_1971" s="T192">np.h:S</ta>
            <ta e="T194" id="Seg_1972" s="T193">v:pred</ta>
            <ta e="T195" id="Seg_1973" s="T194">0.2.h:S v:pred</ta>
            <ta e="T196" id="Seg_1974" s="T195">np.h:S</ta>
            <ta e="T197" id="Seg_1975" s="T196">v:pred</ta>
            <ta e="T198" id="Seg_1976" s="T197">pro.h:S</ta>
            <ta e="T199" id="Seg_1977" s="T198">v:pred</ta>
            <ta e="T201" id="Seg_1978" s="T200">pro.h:S</ta>
            <ta e="T204" id="Seg_1979" s="T203">v:pred</ta>
            <ta e="T205" id="Seg_1980" s="T204">pro.h:S</ta>
            <ta e="T207" id="Seg_1981" s="T206">v:pred</ta>
            <ta e="T209" id="Seg_1982" s="T208">v:pred 0.3.h:S</ta>
            <ta e="T210" id="Seg_1983" s="T209">np.h:S</ta>
            <ta e="T212" id="Seg_1984" s="T211">v:pred</ta>
            <ta e="T214" id="Seg_1985" s="T213">v:pred 0.3.h:S</ta>
            <ta e="T215" id="Seg_1986" s="T214">np.h:S</ta>
            <ta e="T216" id="Seg_1987" s="T215">v:pred</ta>
            <ta e="T220" id="Seg_1988" s="T219">v:pred 0.3.h:S</ta>
            <ta e="T222" id="Seg_1989" s="T221">np.h:S</ta>
            <ta e="T223" id="Seg_1990" s="T222">v:pred</ta>
            <ta e="T225" id="Seg_1991" s="T224">np.h:O</ta>
            <ta e="T226" id="Seg_1992" s="T225">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T85" id="Seg_1993" s="T84">RUS:cult</ta>
            <ta e="T97" id="Seg_1994" s="T96">TURK:gram(INDEF)</ta>
            <ta e="T98" id="Seg_1995" s="T97">TURK:disc</ta>
            <ta e="T99" id="Seg_1996" s="T98">RUS:cult</ta>
            <ta e="T105" id="Seg_1997" s="T104">RUS:mod</ta>
            <ta e="T108" id="Seg_1998" s="T107">RUS:gram</ta>
            <ta e="T115" id="Seg_1999" s="T114">RUS:gram</ta>
            <ta e="T126" id="Seg_2000" s="T125">RUS:gram</ta>
            <ta e="T128" id="Seg_2001" s="T127">RUS:cult</ta>
            <ta e="T141" id="Seg_2002" s="T140">RUS:disc</ta>
            <ta e="T152" id="Seg_2003" s="T151">RUS:gram</ta>
            <ta e="T154" id="Seg_2004" s="T153">TURK:disc</ta>
            <ta e="T158" id="Seg_2005" s="T157">RUS:gram</ta>
            <ta e="T161" id="Seg_2006" s="T160">RUS:gram</ta>
            <ta e="T165" id="Seg_2007" s="T164">RUS:cult</ta>
            <ta e="T166" id="Seg_2008" s="T165">RUS:gram</ta>
            <ta e="T173" id="Seg_2009" s="T172">TURK:disc</ta>
            <ta e="T174" id="Seg_2010" s="T173">TURK:disc</ta>
            <ta e="T200" id="Seg_2011" s="T199">RUS:gram</ta>
            <ta e="T202" id="Seg_2012" s="T201">RUS:disc</ta>
            <ta e="T203" id="Seg_2013" s="T202">TAT:cult</ta>
            <ta e="T206" id="Seg_2014" s="T205">TURK:disc</ta>
            <ta e="T208" id="Seg_2015" s="T207">RUS:cult</ta>
            <ta e="T211" id="Seg_2016" s="T210">RUS:cult</ta>
            <ta e="T217" id="Seg_2017" s="T216">RUS:gram</ta>
            <ta e="T218" id="Seg_2018" s="T217">RUS:cult</ta>
            <ta e="T221" id="Seg_2019" s="T220">RUS:gram</ta>
            <ta e="T224" id="Seg_2020" s="T223">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T157" id="Seg_2021" s="T156">RUS:int.ins</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T83" id="Seg_2022" s="T80">Жили жена с мужем.</ta>
            <ta e="T85" id="Seg_2023" s="T83">Посеяли они репу.</ta>
            <ta e="T88" id="Seg_2024" s="T85">Выросла она большая.</ta>
            <ta e="T92" id="Seg_2025" s="T88">Пришёл медведь, вырвал её.</ta>
            <ta e="T93" id="Seg_2026" s="T92">Выбросил прочь.</ta>
            <ta e="T96" id="Seg_2027" s="T93">Пришёл муж посмотреть.</ta>
            <ta e="T101" id="Seg_2028" s="T96">"Кто-то вырвал репу, выбросил".</ta>
            <ta e="T109" id="Seg_2029" s="T101">Жена говорит: " Это, наверное, не (человек?), а медведь.</ta>
            <ta e="T114" id="Seg_2030" s="T109">Иди, иди, сядь там!</ta>
            <ta e="T118" id="Seg_2031" s="T114">Да смотри, кто придёт".</ta>
            <ta e="T121" id="Seg_2032" s="T118">Он пошёл, спрятался.</ta>
            <ta e="T123" id="Seg_2033" s="T121">Пришёл медведь.</ta>
            <ta e="T127" id="Seg_2034" s="T123">Нарвал много [репы] и ушёл.</ta>
            <ta e="T128" id="Seg_2035" s="T127">На забор [полез].</ta>
            <ta e="T133" id="Seg_2036" s="T128">Тогда [муж] отрубил ему ногу топором.</ta>
            <ta e="T136" id="Seg_2037" s="T133">Ногу ему отрубил.</ta>
            <ta e="T140" id="Seg_2038" s="T136">Потом принёс [её] жене.</ta>
            <ta e="T143" id="Seg_2039" s="T140">"Возьми, я ногу принёс".</ta>
            <ta e="T147" id="Seg_2040" s="T143">Женщина ободрала ногу.</ta>
            <ta e="T151" id="Seg_2041" s="T147">Положила мясо в котёл, варит.</ta>
            <ta e="T157" id="Seg_2042" s="T151">И начала шерсть прясть.</ta>
            <ta e="T160" id="Seg_2043" s="T157">А сама на шкуре [с ноги] сидит.</ta>
            <ta e="T167" id="Seg_2044" s="T160">А медведь сделал себе ногу из липы и идёт [к ним].</ta>
            <ta e="T168" id="Seg_2045" s="T167">Говорит:</ta>
            <ta e="T173" id="Seg_2046" s="T168">"У меня нога из (дерева?). [?]</ta>
            <ta e="T176" id="Seg_2047" s="T173">Все люди спят.</ta>
            <ta e="T185" id="Seg_2048" s="T176">Один мужик не спит, одна баба не спит.</ta>
            <ta e="T187" id="Seg_2049" s="T185">Она мясо варит.</ta>
            <ta e="T189" id="Seg_2050" s="T187">Шерсть прядёт.</ta>
            <ta e="T191" id="Seg_2051" s="T189">Сидит на шкуре".</ta>
            <ta e="T194" id="Seg_2052" s="T191">Тогда женщина говорит:</ta>
            <ta e="T197" id="Seg_2053" s="T194">"Иди [закрой дверь], медведь идёт".</ta>
            <ta e="T204" id="Seg_2054" s="T197">Он пошёл, а тот уже в дом входит.</ta>
            <ta e="T207" id="Seg_2055" s="T204">Влез [в сени].</ta>
            <ta e="T209" id="Seg_2056" s="T207">[Муж] корытом накрылся.</ta>
            <ta e="T214" id="Seg_2057" s="T209">Жена на печь залезла, под одеждой спряталась.</ta>
            <ta e="T216" id="Seg_2058" s="T214">Медведь пришёл.</ta>
            <ta e="T220" id="Seg_2059" s="T216">И в подпол свалился.</ta>
            <ta e="T226" id="Seg_2060" s="T220">А люди пришли и убили медведя.</ta>
            <ta e="T227" id="Seg_2061" s="T226">Хватит!</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T83" id="Seg_2062" s="T80">There lived a woman with a man.</ta>
            <ta e="T85" id="Seg_2063" s="T83">They sowed turnip.</ta>
            <ta e="T88" id="Seg_2064" s="T85">It grew big.</ta>
            <ta e="T92" id="Seg_2065" s="T88">A bear came, tore it.</ta>
            <ta e="T93" id="Seg_2066" s="T92">Threw it away.</ta>
            <ta e="T96" id="Seg_2067" s="T93">The man came to look.</ta>
            <ta e="T101" id="Seg_2068" s="T96">"Someone had torn the turnip, had thrown it away."</ta>
            <ta e="T109" id="Seg_2069" s="T101">The woman says: "It was not a (human?), but a bear.</ta>
            <ta e="T114" id="Seg_2070" s="T109">Go, go, sit there!</ta>
            <ta e="T118" id="Seg_2071" s="T114">And look, who is going."</ta>
            <ta e="T121" id="Seg_2072" s="T118">He went, hid.</ta>
            <ta e="T123" id="Seg_2073" s="T121">The bear came.</ta>
            <ta e="T127" id="Seg_2074" s="T123">It tore a lot [of turnip] and went [away].</ta>
            <ta e="T128" id="Seg_2075" s="T127">[It climbed] on the fence.</ta>
            <ta e="T133" id="Seg_2076" s="T128">Then [the man] cut its foot with an axe.</ta>
            <ta e="T136" id="Seg_2077" s="T133">He cut off its foot.</ta>
            <ta e="T140" id="Seg_2078" s="T136">Then brought [it] to his wife.</ta>
            <ta e="T143" id="Seg_2079" s="T140">"Take it, I brought a foot."</ta>
            <ta e="T147" id="Seg_2080" s="T143">The woman skinned the foot.</ta>
            <ta e="T151" id="Seg_2081" s="T147">She put the meat into a cauldron, is cooking.</ta>
            <ta e="T157" id="Seg_2082" s="T151">And she started to spin its hair.</ta>
            <ta e="T160" id="Seg_2083" s="T157">And she is sitting on the skin [from the foot].</ta>
            <ta e="T167" id="Seg_2084" s="T160">And the bear made a foot from the linden [wood] and comes [to them].</ta>
            <ta e="T168" id="Seg_2085" s="T167">It says:</ta>
            <ta e="T173" id="Seg_2086" s="T168">"My foot is from (wood?). [?]</ta>
            <ta e="T176" id="Seg_2087" s="T173">All the people are sleeping.</ta>
            <ta e="T185" id="Seg_2088" s="T176">One man is not sleeping, one woman is not sleeping.</ta>
            <ta e="T187" id="Seg_2089" s="T185">She is cooking meat.</ta>
            <ta e="T189" id="Seg_2090" s="T187">She is spinning.</ta>
            <ta e="T191" id="Seg_2091" s="T189">She is sitting on the skin."</ta>
            <ta e="T194" id="Seg_2092" s="T191">Then the woman says:</ta>
            <ta e="T197" id="Seg_2093" s="T194">"Go [close the door], the bear is coming."</ta>
            <ta e="T204" id="Seg_2094" s="T197">[The man] went [to close the door], but [the bear] is already entering the house.</ta>
            <ta e="T207" id="Seg_2095" s="T204">He climbed [on the loft].</ta>
            <ta e="T209" id="Seg_2096" s="T207">[The man] covered [himself] with a trough.</ta>
            <ta e="T214" id="Seg_2097" s="T209">Then woman climbed into the oven, hid in the clothes.</ta>
            <ta e="T216" id="Seg_2098" s="T214">The bear came.</ta>
            <ta e="T220" id="Seg_2099" s="T216">And fell into the cellar.</ta>
            <ta e="T226" id="Seg_2100" s="T220">But the people came and killed the bear.</ta>
            <ta e="T227" id="Seg_2101" s="T226">Enough!</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T83" id="Seg_2102" s="T80">Es lebte eine Frau mit einem Mann.</ta>
            <ta e="T85" id="Seg_2103" s="T83">Sie säten Weißrüben.</ta>
            <ta e="T88" id="Seg_2104" s="T85">Es wuchs groß.</ta>
            <ta e="T92" id="Seg_2105" s="T88">Ein Bär kam, zerriss es.</ta>
            <ta e="T93" id="Seg_2106" s="T92">Wurf es weg.</ta>
            <ta e="T96" id="Seg_2107" s="T93">Der Mann kam schauen.</ta>
            <ta e="T101" id="Seg_2108" s="T96">„Jemand hatte die Weißrüben zerrissen, hatte sie weggeworfen.“ </ta>
            <ta e="T109" id="Seg_2109" s="T101">Die Frau sagt : „Es war kein Mensch, sondern ein Bär.</ta>
            <ta e="T114" id="Seg_2110" s="T109">Geh, geh, sitz dort!</ta>
            <ta e="T118" id="Seg_2111" s="T114">Und schau, wer geht.“</ta>
            <ta e="T121" id="Seg_2112" s="T118">Er ging, versteckte sich.</ta>
            <ta e="T123" id="Seg_2113" s="T121">Der Bär kam.</ta>
            <ta e="T127" id="Seg_2114" s="T123">Er zerriss viel [an Weißrüben] und ging [weg].</ta>
            <ta e="T128" id="Seg_2115" s="T127">[Er kletterte] auf den Zaun.</ta>
            <ta e="T133" id="Seg_2116" s="T128">Dann schnitt er ihn an die Pfote mit einer Axt.</ta>
            <ta e="T136" id="Seg_2117" s="T133">Er schnitt seinen Fuß ab.</ta>
            <ta e="T140" id="Seg_2118" s="T136">Dan brachte [ihn] seiner Frau.</ta>
            <ta e="T143" id="Seg_2119" s="T140">„Nimm es, ich habe einen Fuß gebracht.“</ta>
            <ta e="T147" id="Seg_2120" s="T143">Die Frau häutete den Fuß.</ta>
            <ta e="T151" id="Seg_2121" s="T147">Sie legt das Fleisch in einen Kessel, es kocht.</ta>
            <ta e="T157" id="Seg_2122" s="T151">Und sie fängt an, sein Haar zu spinnen.</ta>
            <ta e="T160" id="Seg_2123" s="T157">Und sie sitzt auf der Haut.</ta>
            <ta e="T167" id="Seg_2124" s="T160">Und der Bär machte ein Fuß aus Linden [-holz] und kommt.</ta>
            <ta e="T168" id="Seg_2125" s="T167">Er sagt:</ta>
            <ta e="T173" id="Seg_2126" s="T168">„Meine Tatze ist aus (Holz?). [?]</ta>
            <ta e="T176" id="Seg_2127" s="T173">Alle Leute schlafen.</ta>
            <ta e="T185" id="Seg_2128" s="T176">Ein Mann schläft, eine Frau schläft nicht.</ta>
            <ta e="T187" id="Seg_2129" s="T185">Sie kocht Fleisch.</ta>
            <ta e="T189" id="Seg_2130" s="T187">Sie spinnt.</ta>
            <ta e="T191" id="Seg_2131" s="T189">Sie sitzt auf der Haut.</ta>
            <ta e="T194" id="Seg_2132" s="T191">Dann sagt die Frau:</ta>
            <ta e="T197" id="Seg_2133" s="T194">„Geh [mach die Tür zu], der Bär kommt.“</ta>
            <ta e="T204" id="Seg_2134" s="T197">Er ging, aber er tretet schon ins Haus ein.</ta>
            <ta e="T207" id="Seg_2135" s="T204">Er klettert auf den Dachboden.</ta>
            <ta e="T209" id="Seg_2136" s="T207">Er bedeckt [sich] mit einem Trog.</ta>
            <ta e="T214" id="Seg_2137" s="T209">Dann kletterte die Frau in den Ofen, versteckte die Kleider.</ta>
            <ta e="T216" id="Seg_2138" s="T214">Der Bär kam.</ta>
            <ta e="T220" id="Seg_2139" s="T216">Und fiel in den Keller.</ta>
            <ta e="T226" id="Seg_2140" s="T220">Aber die Leute kamen und töteten den Bären.</ta>
            <ta e="T227" id="Seg_2141" s="T226">Genug!</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T83" id="Seg_2142" s="T80">http://hyaenidae.narod.ru/story1/037.html</ta>
            <ta e="T109" id="Seg_2143" s="T101">[GVY:] Here unclear. In the tale, the woman says: "If it have been humans, they would have taken the turnip away, so it should have been a bear".</ta>
            <ta e="T128" id="Seg_2144" s="T127">[GVY:] In the Russian original the bear "полез через плетень" 'climbed over the (wicked) fence'. Заплот is a sort of a fence. Another possibility is "za plotə" '[excaped] over the fence', where плот is a dialectal word for плетень, but PKZ seems not to use            Russian prepositons. </ta>
            <ta e="T173" id="Seg_2145" s="T168">[GVY:] pagad? anoːna (bar) 'made'?</ta>
            <ta e="T189" id="Seg_2146" s="T187">[GVY:] erlerlaʔbə?</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
